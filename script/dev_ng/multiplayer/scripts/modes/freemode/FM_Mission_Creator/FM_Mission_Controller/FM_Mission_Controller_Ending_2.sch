
USING "FM_Mission_Controller_USING.sch"
USING "FM_Mission_Controller_Utility.sch"
USING "FM_Mission_Controller_Entities.sch"
USING "FM_Mission_Controller_ObjectiveCompletion.sch"
USING "FM_Mission_Controller_SpecialCase.sch"
USING "FM_Mission_Controller_Rules.sch"
USING "FM_Mission_Controller_Doors.sch"
USING "FM_Mission_Controller_Cutscene.sch"
USING "FM_Mission_Controller_Debug.sch"
USING "FM_Mission_Controller_Locations.sch"
USING "FM_Mission_Controller_Vehicles.sch"
USING "FM_Mission_Controller_Peds_Brain.sch"
USING "FM_Mission_Controller_Peds_Body.sch"
USING "FM_Mission_Controller_Peds.sch"
USING "FM_Mission_Controller_GangChase.sch"
USING "FM_Mission_Controller_Minigame.sch"
USING "FM_Mission_Controller_Objects.sch"
USING "FM_Mission_Controller_Props.sch"
USING "FM_Mission_Controller_Audio.sch"
USING "FM_Mission_Controller_HUD.sch"
USING "FM_Mission_Controller_Players.sch"
USING "FM_Mission_Controller_Zones.sch"
USING "FM_Mission_Controller_Bounds.sch"
USING "FM_Mission_Controller_Events.sch"
USING "FM_Mission_Controller_Ending.sch"
USING "FM_Mission_Controller_Cutscene_2.sch"
USING "FM_Mission_Controller_Main.sch"
USING "net_private_yacht_private.sch"



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: SCRIPT CLEANUP !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************

PROC CLEAR_WATER_CALMING_QUADS()
	
	INT iQuad
	
	FOR iQuad = 0 TO (FMMC_MAX_WATER_CALMING_QUADS - 1)
		IF iWaterCalmingQuad[iQuad] != -1
			REMOVE_EXTRA_CALMING_QUAD(iWaterCalmingQuad[iQuad])
			iWaterCalmingQuad[iQuad] = -1
		ENDIF
	ENDFOR
	
ENDPROC

PROC RESET_HEIST_BONUS_TICKERS()
	g_bDoHeistBonusFlowOrderBrokenTicker = FALSE
	g_bDoHeistBonusSameTeamBrokenTicker = FALSE
	g_bDoHeistBonusUltimateChallengeBrokenTicker = FALSE
ENDPROC

//PURPOSE: Clears the render targets to their blank start states
PROC CLEANUP_RENDERTARGETS()
	PRINTLN("RC MISSION Cleanup_RenderTargets() called")
	//WIPE_RENDERTARGETS(FALSE)
	IF IS_NAMED_RENDERTARGET_REGISTERED("tvscreen")
		RELEASE_NAMED_RENDERTARGET("tvscreen")
		PRINTLN("RC MISSION Cleanup_RenderTargets() named render target tvscreen found & released")
	ENDIF
ENDPROC

PROC CLEAN_UP_SPAWN_VEHICLE_BLIPS()
	CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION][SPAWN] Cleaning up spawn vehicle blips.")

	// Remove spawn vehicle blips.
	IF DOES_BLIP_EXIST(biSpawnVehicle)
		REMOVE_BLIP(biSpawnVehicle)
		CDEBUG2LN(DEBUG_MC_BLIP, "[RCC MISSION][SPAWN] Spawn vehicle blip deleted.")
	ENDIF
	
	// Dereference our vehicle index.
	vehSpawnVehicle = NULL
	CDEBUG2LN(DEBUG_MC_BLIP, "[RCC MISSION][SPAWN] Spawn vehicle index dereferenced.")
	
	// Reset the flag to maintain the blip.
	bManageSpawnVehicleBlip = FALSE
	CDEBUG2LN(DEBUG_MC_BLIP, "[RCC MISSION][SPAWN] Spawn vehicle management flag cleared.")
ENDPROC

PROC CACHE_STAT_VARIABLES_FOR_STRAND_MISSION()

	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
		IF IS_A_STRAND_MISSION_BEING_INITIALISED()
			IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE()
				g_TransitionSessionNonResetVars.iTotalMissionEndTime += MC_serverBD.iTotalMissionEndTime
				PRINTLN("[RCC MISSION] CACHE_STAT_VARIABLES_FOR_STRAND_MISSION iTotalMissionEndTime (CASINO HEIST) set time as ",g_TransitionSessionNonResetVars.iTotalMissionEndTime)
			ELSE
				g_TransitionSessionNonResetVars.iTotalMissionEndTime = MC_serverBD.iTotalMissionEndTime
				PRINTLN("[RCC MISSION] CACHE_STAT_VARIABLES_FOR_STRAND_MISSION g_TransitionSessionNonResetVars.iTotalMissionEndTime set time as ",g_TransitionSessionNonResetVars.iTotalMissionEndTime)
				//If not grabbed server time, use local time
				IF g_TransitionSessionNonResetVars.iTotalMissionEndTime <= 0
					g_TransitionSessionNonResetVars.iTotalMissionEndTime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( MC_serverBD.tdMissionLengthTimer )
					PRINTLN("[RCC MISSION] g_TransitionSessionNonResetVars.iTotalMissionEndTime <= 0 taking local time as ",g_TransitionSessionNonResetVars.iTotalMissionEndTime)
				ENDIF
			ENDIF
			g_TransitionSessionNonResetVars.fHeistVehDamage = MC_serverBD_2.fHeistVehDamage
			g_TransitionSessionNonResetVars.fVIPDamage = MC_serverBD_2.fVIPDamage
			g_TransitionSessionNonResetVars.bSwatInvolved = IS_BIT_SET(MC_serverBD.iServerBitSet2,SBBOOL2_HEIST_REWARD_SWAT_INVOLVED)
			g_TransitionSessionNonResetVars.bHasBeatCashGrab = bHasBeatCashGrab
			g_TransitionSessionNonResetVars.iPlayerMostHealthLost = MC_serverBD.iPlayerMostHealthLost
			g_TransitionSessionNonResetVars.iHeistRewardMMTimer = MC_serverBD.iHeistRewardMMTimer
			
			g_TransitionSessionNonResetVars.iNumPedKills = MC_Playerbd[iPartToUse].iNumPedKills
			g_TransitionSessionNonResetVars.iAmbientCopsKilled = MC_Playerbd[iPartToUse].iAmbientCopsKilled
			g_TransitionSessionNonResetVars.iNumHeadshots = MC_Playerbd[iPartToUse].iNumHeadshots
			g_TransitionSessionNonResetVars.veh_damage_time = MC_Playerbd[iPartToUse].veh_damage_time
			g_TransitionSessionNonResetVars.veh_damage_percentage = MC_Playerbd[iPartToUse].veh_damage_percentage
			g_TransitionSessionNonResetVars.iStartHits = MC_Playerbd[iPartToUse].iStartHits
			g_TransitionSessionNonResetVars.iStartShots = MC_Playerbd[iPartToUse].iStartShots
			g_TransitionSessionNonResetVars.iCivilianKills = MC_Playerbd[iPartToUse].iCivilianKills
			g_TransitionSessionNonResetVars.iNumPlayerDeaths = MC_Playerbd[iPartToUse].iNumPlayerDeaths
			g_TransitionSessionNonResetVars.iHackTime = MC_Playerbd[iPartToUse].iHackTime
			g_TransitionSessionNonResetVars.inumHacks = MC_Playerbd[iPartToUse].inumHacks
			g_TransitionSessionNonResetVars.iWantedTime = MC_Playerbd[iPartToUse].iWantedTime
			g_TransitionSessionNonResetVars.iNumWantedLose = MC_Playerbd[iPartToUse].iNumWantedLose
			g_TransitionSessionNonResetVars.medal_objective_completed_count = MC_Playerbd[iPartToUse].medal_objective_completed_count
			g_TransitionSessionNonResetVars.iOffRuleMinigameScore = MC_playerBD_1[iPartToUse].iOffRuleMinigameScoreForMedal
			
			g_TransitionSessionNonResetVars.iStrandMissionsComplete = MC_serverBD_1.iStrandMissionsComplete + 1
			g_TransitionSessionNonResetVars.iEntranceUsed = MC_serverBD_1.iEntranceUsed
			g_TransitionSessionNonResetVars.iVaultTake = MC_serverBD_1.iVaultTake
			g_TransitionSessionNonResetVars.iDailyCashRoomTake = MC_serverBD_1.iDailyCashRoomTake
			g_TransitionSessionNonResetVars.iDepositBoxTake = MC_serverBD_1.iDepositBoxTake
			
			INT iTeamCounter
			REPEAT FMMC_MAX_TEAMS iTeamCounter
				g_TransitionSessionNonResetVars.iTeamKills[iTeamCounter] = MC_serverBD.iTeamKills[iTeamCounter]
				g_TransitionSessionNonResetVars.iTotalCoopDeaths[iTeamCounter] = MC_serverBD.iTeamDeaths[iTeamCounter]	
			ENDREPEAT
			
			g_TransitionSessionNonResetVars.iTripSkipCost = MC_playerBD[NATIVE_TO_INT(LocalPlayer)].TSPlayerData.iCost
			PRINTLN("[MJM] - TRIP SKIP - g_TransitionSessionNonResetVars.iTripSkipCost = TSPlayerData.iCost $", MC_playerBD[NATIVE_TO_INT(LocalPlayer)].TSPlayerData.iCost)
			
			PRINTLN("[MJM] - CACHE_STAT_VARIABLES_FOR_STRAND_MISSION - Stored.")
		ENDIF
	ENDIF
	
ENDPROC

PROC CACHE_MISSION_CONTINUITY_VARS()
	
	IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		EXIT
	ENDIF
	
	IF IS_CORONA_INITIALISING_A_QUICK_RESTART()
		EXIT
	ENDIF
	
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - On a strand mission, caching continuity vars")
	g_TransitionSessionNonResetVars.sLEGACYMissionContinuityVars = MC_serverBD_1.sLEGACYMissionContinuityVars
	g_TransitionSessionNonResetVars.sLEGACYMissionLocalContinuityVars = sLEGACYMissionLocalContinuityVars
	
#IF IS_DEBUG_BUILD	
	//Server Continuity
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iGenericTrackingBitset = ", g_TransitionSessionNonResetVars.sLEGACYMissionContinuityVars.iGenericTrackingBitset)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iLocationTrackingBitset = ", g_TransitionSessionNonResetVars.sLEGACYMissionContinuityVars.iLocationTrackingBitset)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iVehicleDestroyedTrackingBitset = ", g_TransitionSessionNonResetVars.sLEGACYMissionContinuityVars.iVehicleDestroyedTrackingBitset)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iDynoPropDestroyedTrackingBitset = ", g_TransitionSessionNonResetVars.sLEGACYMissionContinuityVars.iDynoPropDestroyedTrackingBitset)
	INT i
	FOR i = 0 TO FMMC_MAX_PEDS_BITSET - 1
		PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iPedDeathBitset[",i,"] = ", g_TransitionSessionNonResetVars.sLEGACYMissionContinuityVars.iPedDeathBitset[i])
		PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iPedSpecialBitset[",i,"] = ", g_TransitionSessionNonResetVars.sLEGACYMissionContinuityVars.iPedSpecialBitset[i])
	ENDFOR
	FOR i = 0 TO FMMC_MAX_OBJ_CONTINUITY_BITSET - 1
		PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iObjectTrackingBitset[",i,"] = ", g_TransitionSessionNonResetVars.sLEGACYMissionContinuityVars.iObjectTrackingBitset[i])
		PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iObjectDestroyedBitset[",i,"] = ", g_TransitionSessionNonResetVars.sLEGACYMissionContinuityVars.iObjectDestroyedBitset[i])
		PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iObjectOffRuleTrackingBitset[",i,"] = ", g_TransitionSessionNonResetVars.sLEGACYMissionContinuityVars.iObjectOffRuleTrackingBitset[i])
	ENDFOR
	FOR i = 0 TO FMMC_MAX_DOOR_CONTINUITY_BITSET - 1
		PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iDoorsUseAltConfigBitset[",i,"] = ", g_TransitionSessionNonResetVars.sLEGACYMissionContinuityVars.iDoorsUseAltConfigBitset[i])
	ENDFOR
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iDailyVaultCashGrabbed = ", g_TransitionSessionNonResetVars.sLEGACYMissionContinuityVars.iDailyVaultCashGrabbed)
	
	//Local Continuity
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iEndVehicleContinuityId = ", g_TransitionSessionNonResetVars.sLEGACYMissionLocalContinuityVars.iEndVehicleContinuityId)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - eEndVehicleSeat = ", ENUM_TO_INT(g_TransitionSessionNonResetVars.sLEGACYMissionLocalContinuityVars.eEndVehicleSeat))
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iGeneralLocalTrackingBitset = ", g_TransitionSessionNonResetVars.sLEGACYMissionLocalContinuityVars.iGeneralLocalTrackingBitset)
	FOR i = 0 TO FMMC_MAX_PICKUP_CONTINUITY_BITSET - 1
		PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iPickupCollectedTrackingBitset[",i,"] = ", g_TransitionSessionNonResetVars.sLEGACYMissionLocalContinuityVars.iPickupCollectedTrackingBitset[i])
	ENDFOR
	FOR i = 0 TO ciMAX_DIALOGUE_BIT_SETS_LEGACY - 1
		PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iDialogueTrackingBitset[",i,"] = ", g_TransitionSessionNonResetVars.sLEGACYMissionLocalContinuityVars.iDialogueTrackingBitset[i])
	ENDFOR
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iHeistBag = ", g_TransitionSessionNonResetVars.sLEGACYMissionLocalContinuityVars.iHeistBag)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iOutfit = ", g_TransitionSessionNonResetVars.sLEGACYMissionLocalContinuityVars.iOutfit)
	
#ENDIF	

ENDPROC

PROC REMOVE_PLAYER_CUSTOM_BLIP_SPRITE()

	INT iParticipant

	
	FOR iParticipant = 0 TO MAX_NUM_MC_PLAYERS -1
	
		IF iParticipant < MAX_NUM_MC_PLAYERS
			PARTICIPANT_INDEX currentParticipant = INT_TO_PARTICIPANTINDEX(iParticipant)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(currentParticipant)
				PLAYER_INDEX currentPlayer = NETWORK_GET_PLAYER_INDEX(currentParticipant)
		
				IF g_FMMC_STRUCT.iTeamBlipType[MC_playerBD[iParticipant].iteam] = TEAM_BLIP_RED_SKULL					
					SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(currentPlayer, RADAR_TRACE_BOUNTY_HIT, FALSE)
					FORCE_BLIP_PLAYER(localPlayer, FALSE, FALSE)
					PRINTLN("CLEARING CUSTOM BLIP SPRITE FOR PLAYER - SKULL | iParticipant: ", iParticipant, " | iTeam: ", MC_playerBD[iParticipant].iteam)
				ELIF g_FMMC_STRUCT.iTeamBlipType[MC_playerBD[iParticipant].iteam] = TEAM_BLIP_HOLDING_PACKAGE	
					SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(currentPlayer, RADAR_TRACE_RACEFLAG, FALSE)
					FORCE_BLIP_PLAYER(localPlayer, FALSE, FALSE)
					PRINTLN("CLEARING CUSTOM BLIP SPRITE FOR PLAYER - PACKAGE | iParticipant: ", iParticipant, " | iTeam: ", MC_playerBD[iParticipant].iteam)
				ELIF g_FMMC_STRUCT.iTeamBlipType[MC_playerBD[iParticipant].iteam] = TEAM_BLIP_BEAST	
					SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(currentPlayer, RADAR_TRACE_BEAST, FALSE)
					FORCE_BLIP_PLAYER(localPlayer, FALSE, FALSE)
					PRINTLN("CLEARING CUSTOM BLIP SPRITE FOR PLAYER - BEAST | iParticipant: ", iParticipant, " | iTeam: ", MC_playerBD[iParticipant].iteam)
				ELIF g_FMMC_STRUCT.iTeamBlipType[MC_playerBD[iParticipant].iteam] = TEAM_BLIP_JUGGERNAUT
					SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(currentPlayer, RADAR_TRACE_JUGG, FALSE)
					FORCE_BLIP_PLAYER(localPlayer, FALSE, FALSE)
					PRINTLN("CLEARING CUSTOM BLIP SPRITE FOR PLAYER - JUGGERNAUT | iParticipant: ", iParticipant, " | iTeam: ", MC_playerBD[iParticipant].iteam)
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

PROC CLEARUP_RIOTVAN_PTFX()
	INT iPtfx = 0
	
	REPEAT FMMC_MAX_VEHICLES iPtfx
		IF DOES_PARTICLE_FX_LOOPED_EXIST(MC_serverBD_4.ptfxVehicleFireFX_Front[iPtfx])
			REMOVE_PARTICLE_FX(MC_serverBD_4.ptfxVehicleFireFX_Front[iPtfx])
			PRINTLN("[TMS] Clearing up riot van front PTFX for vehicle ", iPtfx)
		ENDIF
		
		IF DOES_PARTICLE_FX_LOOPED_EXIST(MC_serverBD_4.ptfxVehicleFireFX_Back[iPtfx])
			REMOVE_PARTICLE_FX(MC_serverBD_4.ptfxVehicleFireFX_Back[iPtfx])
			PRINTLN("[TMS] Clearing up riot van back PTFX for vehicle ", iPtfx)
		ENDIF
		
		IF IS_DECAL_ALIVE(di_RiotVanDecals[iPtfx])
			PRINTLN("[TMS] Clearing up decal for vehicle ", iPtfx)
			REMOVE_DECAL(di_RiotVanDecals[iPtfx])
		ENDIF
	ENDREPEAT
ENDPROC

PROC CLEANUP_OVERTIME_MODE_LEAVERS_VEHICLE()
	// We left the game...
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
	AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet,SBBOOL_MISSION_OVER)	
	AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		IF iOTPartToMove = iLocalPart
			PRINTLN("[LM][OVERTIME][RENDERPHASE_PAUSE_EVENT] MC_SCRIPT_CLEANUP - Local Player Leaving and it's our turn - Calling to pause renderphases.")
			BROADCAST_FMMC_OVERTIME_PAUSE_RENDERPHASE(MC_ServerBD.iSessionScriptEventKey, TRUE)
		ENDIF
		
		IF NOT IS_PED_INJURED(localPlayerPed)
			IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)				
				VEHICLE_INDEX vehIndex = GET_VEHICLE_PED_IS_IN(localPlayerPed)
				CLEAR_PED_TASKS_IMMEDIATELY(localPlayerPed)
				
				IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehIndex, FALSE)
				AND IS_ENTITY_A_MISSION_ENTITY(vehIndex)				
					DELETE_VEHICLE(vehIndex)
				ENDIF
			ENDIF
		ENDIF
		
		PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - Calling SET_PLAYER_LEAVE_PED_BEHIND(FALSE) & NETWORK_DISABLE_LEAVE_REMOTE_PED_BEHIND(TRUE)")
		SET_PLAYER_LEAVE_PED_BEHIND(LocalPlayer, FALSE)
		NETWORK_DISABLE_LEAVE_REMOTE_PED_BEHIND(TRUE)
	ENDIF
ENDPROC

PROC CLEANUP_BOMBUSHKA_MODE_LEAVERS()
	// We left the game...
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMBUSHKA(g_FMMC_STRUCT.iAdversaryModeType)
	AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet,SBBOOL_MISSION_OVER)	
	AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()		
		IF NOT IS_PED_INJURED(localPlayerPed)
			IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)				
				VEHICLE_INDEX vehIndex = GET_VEHICLE_PED_IS_IN(localPlayerPed)
				CLEAR_PED_TASKS_IMMEDIATELY(localPlayerPed)
				
				IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehIndex, FALSE)
				AND IS_ENTITY_A_MISSION_ENTITY(vehIndex)
				AND GET_ENTITY_MODEL(vehIndex) != BOMBUSHKA
					DELETE_VEHICLE(vehIndex)
				ENDIF
			ENDIF
			
			IF NETWORK_HAS_CONTROL_OF_ENTITY(localPlayerPed)
				SET_ENTITY_VISIBLE(localPlayerPed, FALSE)
			ELSE
				SET_ENTITY_ALPHA(localPlayerPed, 0, FALSE)
			ENDIF
		ENDIF
		
		PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - Calling SET_PLAYER_LEAVE_PED_BEHIND(FALSE) & NETWORK_DISABLE_LEAVE_REMOTE_PED_BEHIND(TRUE)")
		SET_PLAYER_LEAVE_PED_BEHIND(LocalPlayer, FALSE)
		NETWORK_DISABLE_LEAVE_REMOTE_PED_BEHIND(TRUE)
	ENDIF
ENDPROC

PROC CLEANUP_GHOST_AND_ALPHA_EFFECTS()
	//For tron invulnerbility
	IF NOT IS_PED_INJURED(LocalPlayerPed)
		PRINTLN("[PRON CLEANUP] - MC_SCRIPT_CLEANUP - Clearing Alpha effect")
		RESET_ENTITY_ALPHA(LocalPlayerPed)
		SET_LOCAL_PLAYER_AS_GHOST(FALSE)
	ENDIF
	IF IS_ENTITY_A_GHOST(localPlayerPed)
		PRINTLN("[GHOST CLEANUP] - MC_SCRIPT_CLEANUP - Clearing Ghost effect")
		VEHICULAR_VENDETTA_SET_LOCAL_PLAYER_AS_GHOST(FALSE)
	ENDIF	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		BROADCAST_FMMC_SPAWN_PROTECTION_ALPHA(FALSE, iLocalPart, GET_FRAME_COUNT())
	ENDIF
	IF IS_ENTITY_A_GHOST(LocalPlayerPed)
		SET_LOCAL_PLAYER_AS_GHOST(FALSE)
	ENDIF
ENDPROC

PROC CLEANUP_TRON_MODE()
	INT i
	//clean up tron cams
	IF DOES_CAM_EXIST(g_birdsEyeCam)
		IF IS_CAM_ACTIVE(g_birdsEyeCam)
			SET_CAM_ACTIVE(g_birdsEyeCam,FALSE)
		ENDIF
		DESTROY_CAM(g_birdsEyeCam)
		PRINTLN("[PRON CLEANUP] - MC_SCRIPT_CLEANUP - Killing cams")
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciPRON_MODE_ENABLED)
		IF NETWORK_IS_GAME_IN_PROGRESS()
			FOR i = 0 TO (MAX_NUM_MC_PLAYERS - 1)
				PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(i)
				
				IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
					PLAYER_INDEX playerIndex = NETWORK_GET_PLAYER_INDEX(tempPart)
					
					CLEAR_PRON_LINES(MC_playerBD[i].iTeam, playerIndex)
				ENDIF
			ENDFOR
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(viPlayerPronVehicle)
		IF IS_VEHICLE_EMPTY(viPlayerPronVehicle)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(viPlayerPronVehicle)
				SET_ENTITY_VISIBLE(viPlayerPronVehicle, FALSE )
				DELETE_VEHICLE(viPlayerPronVehicle)
				CLEAR_BIT(iLocalBoolCheck20, LBOOL20_MARK_PRON_BIKE_FOR_CLEANUP)
				PRINTLN("[RCC MISSION] - cleaning up pron bike - 1")
			ENDIF
		ENDIF
	ELSE
		CLEAR_BIT(iLocalBoolCheck20, LBOOL20_MARK_PRON_BIKE_FOR_CLEANUP)
	ENDIF
ENDPROC

PROC CLEANUP_TLAD_FEET_EFFECTS()
	//The Lost and Damned particles
	CLEAR_BIT(iLocalBoolCheck18, LBOOL19_TLAD_DEVIL_FLAME_FEET_ACTIVE)
	CLEAR_BIT(iLocalBoolCheck18, LBOOL19_TLAD_ANGEL_GLOW_ACTIVE)
	IF DOES_ENTITY_EXIST(localPlayerPed)
		IF GET_PED_CONFIG_FLAG(LocalPlayerPed,PCF_UseOverrideFootstepPtFx)
			SET_PED_CONFIG_FLAG(LocalPlayerPed,PCF_UseOverrideFootstepPtFx, FALSE)
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_TINY_RACERS_SETTINGS()	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)	
		SET_FORCE_MOTIONBLUR(FALSE)
		OVERRIDE_MICROPHONE_SETTINGS(HASH("SR_TR_MIC"), FALSE)
		SET_SKIDMARK_RANGE_SCALE(1.0)
		SET_PARTICLE_FX_SLIPSTREAM_LODRANGE_SCALE(1.0)
		CASCADE_SHADOWS_SET_BOUND_POSITION(0.0)
		IF DOES_CAM_EXIST(camTRIntro)
			DESTROY_CAM(camTRIntro)
		ENDIF
		CASCADE_SHADOWS_ENABLE_FREEZER(FALSE)
	ENDIF
ENDPROC

PROC CLEANUP_JUGGERNAUT_AUDIO_MIX_GROUP()
	INT iPart
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
		FOR iPart = 0 TO (MAX_NUM_MC_PLAYERS - 1)
			IF iPart != iPartToUse
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPart].iTeam].iTeamBitset2, ciBS2_JUGGERNAUTS)
					IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
						PED_INDEX tempPed = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart)))
						IF DOES_ENTITY_EXIST(tempPed)
							REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(tempPed)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

PROC CLEANUP_SHOWDOWN_POINTS()
	IF USING_SHOWDOWN_POINTS()
		SET_HEALTH_HUD_DISPLAY_VALUES(-1, -1)
		
		//Turns their Gamer Tag healthbars back to normal
		INT iShowdownBarResetIndex = 0
		FOR iShowdownBarResetIndex = 0 TO (MAX_NUM_MC_PLAYERS - 1)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iShowdownBarResetIndex))
				INT iPlayerNum = GET_PLAYER_INDEX_NUMBER_FROM_PARTICIPANT_INDEX_NUMBER(iShowdownBarResetIndex)
				
				IF IS_MP_GAMER_TAG_ACTIVE(iPlayerNum)
					SET_MP_GAMER_TAGS_SHOULD_USE_POINTS_HEALTH(iPlayerNum, FALSE)
					PRINTLN("[SHOWDOWN] Resetting gamertag settings for player ", iPlayerNum)
				ENDIF
			ENDIF
		ENDFOR
	ENDIF	
ENDPROC

PROC CLENANUP_POINTLESS_MODE_DATA()
	INT i
	//Pointless
	FOR i = 0 TO FMMC_MAX_NUM_OBJECTS - 1
		IF HAS_NET_TIMER_STARTED(stPickupTimer[i])
			RESET_NET_TIMER(stPickupTimer[i])
			PRINTLN("[JT POINTLESS] MC_SCRIPT_CLEANUP Resetting timer in clean up: ", i)
		ENDIF
		g_FMMC_STRUCT.iPTLOwnedPickUps[i] = 0
		MC_serverBD_1.iPTLDroppedPackageOwner[i] = -1
		MC_serverBD_1.iPTLDroppedPackageLastTeam[i] = -1
	ENDFOR
ENDPROC

PROC CLEANUP_ALTERED_RADIO_FOR_MC()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_KEEP_RADIO_STATION_ON_RESPAWN)
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMBUSHKA(g_FMMC_STRUCT.iAdversaryModeType)
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_STOCKPILE(g_FMMC_STRUCT.iAdversaryModeType)
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_STUNTING_PACK(g_FMMC_STRUCT.iAdversaryModeType)
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUNNING_BACK_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMB_FOOTBALL(g_FMMC_STRUCT.iAdversaryModeType)
		PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - Cleaning up altered radio.")		
		SET_MOBILE_RADIO_ENABLED_DURING_GAMEPLAY(FALSE)
		SET_MOBILE_PHONE_RADIO_STATE(FALSE)
		SET_AUDIO_FLAG("MobileRadioInGame", FALSE)
		SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)		
		SET_RADIO_TO_STATION_INDEX(255)		
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			IF DOES_PLAYER_VEH_HAVE_RADIO()
				SET_VEH_RADIO_STATION(GET_VEHICLE_PED_IS_IN(localPlayerPed), GET_RADIO_STATION_NAME(255))
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC UNLOAD_MISSION_SPECIFIC_POSTFX()
	PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - UNLOAD_MISSION_SPECIFIC_POSTFX ################################")
	CLEANUP_PLAYER_VISUAL_AIDS()
	IF ANIMPOSTFX_IS_RUNNING("InchOrange")
		PRINTLN("[JJT] - MC_SCRIPT_CLEANUP - Killing InchOrange FX")
		ANIMPOSTFX_STOP("InchOrange")
	ENDIF
	
	IF ANIMPOSTFX_IS_RUNNING("InchPurple")
		PRINTLN("[JJT] - MC_SCRIPT_CLEANUP - Killing InchPurple FX")
		ANIMPOSTFX_STOP("InchPurple")
	ENDIF
	
	IF ANIMPOSTFX_IS_RUNNING("TinyRacerPink")
		PRINTLN("[JS] - MC_SCRIPT_CLEANUP - Killing TinyRacerPink FX")
		ANIMPOSTFX_STOP("TinyRacerPink")
	ENDIF
	
	IF ANIMPOSTFX_IS_RUNNING("TinyRacerGreen")
		PRINTLN("[JS] - MC_SCRIPT_CLEANUP - Killing TinyRacerGreen FX")
		ANIMPOSTFX_STOP("TinyRacerGreen")
	ENDIF
	
	ANIMPOSTFX_STOP("MP_Celeb_Win")
	ANIMPOSTFX_STOP("MP_Celeb_Lose")
	
	IF GET_JOINING_GAMEMODE() = GAMEMODE_SP
	OR GET_JOINING_GAMEMODE() = GAMEMODE_CREATOR
		ANIMPOSTFX_STOP("HeistCelebFail")
		ANIMPOSTFX_STOP("HeistCelebFailBW")
		ANIMPOSTFX_STOP("HeistCelebPass")
		ANIMPOSTFX_STOP("HeistCelebPassBW")
		PRINTLN("[RCC MISSION][NET_CELEBRATION] - Changing to gamemode other than freemode - Clearing all celebration postfx.")
	ENDIF
	TURN_OFF_ACTIVE_FILTERS()
	
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("Deadline")	
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPHotwire")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPBeamhack")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("helicopterhud")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPBeamhackFG")
			
	IF ANIMPOSTFX_IS_RUNNING("CrossLine")
		PRINTLN("[MMacK][LocVFX] SCRIPT CLEAN")
		ANIMPOSTFX_STOP("CrossLine")
		ANIMPOSTFX_PLAY("CrossLineOut", 0, FALSE)
	ENDIF
ENDPROC

PROC CLEANUP_PLACED_PTFX()
	INT iPTFX
	FOR iPTFX = 0 TO FMMC_MAX_PLACED_PTFX-1	
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sPlacedPtfx[iPTFX].vStartCoord)
			IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sPlacedPtfx[iPTFX].tl63AssetName)
				PRINTLN("[LM][Placed_PTFX] MC_SCRIPT_CLEANUP - CLEANUP_PLACED_PTFX - iPTFX: ", iPTFX, " removing named asset: ", g_FMMC_STRUCT.sPlacedPtfx[iPTFX].tl63AssetName)
				REMOVE_NAMED_PTFX_ASSET(g_FMMC_STRUCT.sPlacedPtfx[iPTFX].tl63AssetName)
			ENDIF
			
			IF DOES_PARTICLE_FX_LOOPED_EXIST(sPlacedPtfxLocal[iPTFX].ptfxID)
				PRINTLN("[LM][Placed_PTFX] MC_SCRIPT_CLEANUP - CLEANUP_PLACED_PTFX - iPTFX: ", iPTFX, " Stopping PTFX: ", g_FMMC_STRUCT.sPlacedPtfx[iPTFX].tl63EffectName)
				STOP_PARTICLE_FX_LOOPED(sPlacedPtfxLocal[iPTFX].ptfxID)
			ENDIF
			
			//If option is on RESET_PARTICLE_FX_OVERRIDE
		ENDIF
		CLEANUP_PLACED_PTFX_ASSOCIATED_SOUND_FX(iPTFX)
	ENDFOR
ENDPROC

PROC UNLOAD_MISSION_SPECIFIC_PTFX()
	PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - UNLOAD_MISSION_SPECIFIC_PTFX ################################")
	INT i
	IF IS_BIT_SET(iLocalBoolCheck27, LBOOL27_OVERRIDDEN_MINE_PTFX)
		RESET_PARTICLE_FX_OVERRIDE("exp_underwater_mine")
		CLEAR_BIT(iLocalBoolCheck27, LBOOL27_OVERRIDDEN_MINE_PTFX)
	ENDIF
	IF IS_BIT_SET(iLocalBoolCheck21, LBOOL21_USING_YACHT_APARMENT_ASSETS)
		REMOVE_NAMED_PTFX_ASSET("scr_apartment_mp")
	ENDIF
	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(StockpileSmokeTrail)
		REMOVE_PARTICLE_FX(StockpileSmokeTrail)
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(TLADptfxLensDirt)
		STOP_PARTICLE_FX_LOOPED(TLADptfxLensDirt)
	ENDIF
	SET_PARTICLE_FX_FOOT_OVERRIDE_NAME("")
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciENABLE_NEW_SLIPSTREAM_FX)
		RESET_SLIPSTREAM_VFX()
	ENDIF
	
	FOR i = 0 TO  (FMMC_MAX_TEAMS-1)
		IF DOES_PARTICLE_FX_LOOPED_EXIST(LGHTCYCLE_PLAYER[i].ptfxPronTrail)
			STOP_PARTICLE_FX_LOOPED(LGHTCYCLE_PLAYER[i].ptfxPronTrail)
		ENDIF
	ENDFOR
	
	IF iRadioEmitterPropCheck != -1
		SET_STATIC_EMITTER_ENABLED("SE_Script_Placed_Prop_Emitter_Boombox", FALSE)		
		iRadioEmitterPropCheck = -1
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciDRIVEBY_SHOOTOUT_CAR_PARK_RADIO)
		SET_STATIC_EMITTER_ENABLED("SE_LR_Car_Park_Radio_01", FALSE)
	ENDIF
	
	//SVM Technical 2
	IF IS_BIT_SET(iLocalBoolCheck20, LBOOL20_PARTICLE_FX_OVERRIDE_COCAINE)
		PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP RESET_PARTICLE_FX_OVERRIDE \"ent_dst_polystyrene\"")
		RESET_PARTICLE_FX_OVERRIDE("ent_dst_polystyrene")
		CLEAR_BIT(iLocalBoolCheck20, LBOOL20_PARTICLE_FX_OVERRIDE_COCAINE)
	ENDIF
	
	//Box Pile
	IF IS_BIT_SET(iLocalBoolCheck21, LBOOL21_PARTICLE_FX_OVERRIDE_BOX_PILE)
		PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP RESET_PARTICLE_FX_OVERRIDE \"ent_dst_gen_cardboard\"")
		RESET_PARTICLE_FX_OVERRIDE("ent_dst_gen_cardboard")
		CLEAR_BIT(iLocalBoolCheck21, LBOOL21_PARTICLE_FX_OVERRIDE_BOX_PILE)
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRQUOTA(g_FMMC_STRUCT.iAdversaryModeType)
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxAirQuotaLastKillEffect)
			REMOVE_PARTICLE_FX(ptfxAirQuotaLastKillEffect)
			PRINTLN("[AIRQUOTA_PTFX] MC_SCRIPT_CLEANUP - Clearing up the effect because the mission is ending!")
		ENDIF
		
		IF DOES_PARTICLE_FX_LOOPED_EXIST(sVehicleSwap.ptfxID)
			REMOVE_PARTICLE_FX(sVehicleSwap.ptfxID)
			PRINTLN("[AIRQUOTA_PTFX] MC_SCRIPT_CLEANUP - Clearing up the smokey effect because the mission is ending!")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciCASH_EXPLOSION_ON_TEAM_SWAP)
		IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
		AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TRADING_PLACES_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
			REMOVE_NAMED_PTFX_ASSET("scr_tplaces")
		ELSE
			REMOVE_NAMED_PTFX_ASSET("scr_impexp_jug")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TRANSFORM_ASSETS)
		REMOVE_NAMED_PTFX_ASSET("scr_impexp_jug")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciCONDEMNED_MODE_ENABLED)
		REMOVE_NAMED_PTFX_ASSET("scr_sm_trans")
		REMOVE_NAMED_PTFX_ASSET("scr_bike_adversary")
		SET_PARTICLE_FX_FOOT_OVERRIDE_NAME("")		
	ENDIF
	
	IF iTitanRotorParticlesBitset != 0
		REMOVE_NAMED_PTFX_ASSET("scr_gr_def")
	ENDIF
	
	IF IS_THIS_BOMB_FOOTBALL()
		REMOVE_NAMED_PTFX_ASSET("scr_xs_props")
	ENDIF
	
	INT iPropEF
	FOR iPropEF = 0 TO FMMC_MAX_NUM_FADEOUT_PROPS_EVERY_FRAME-1
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_PropFadeoutEveryFrameIndex[iPropEF])
			REMOVE_PARTICLE_FX(ptfx_PropFadeoutEveryFrameIndex[iPropEF], TRUE)
		ENDIF
	ENDFOR
	
	FOR i = 0 TO (MAX_NUM_MC_PLAYERS - 1)
		IF DOES_PARTICLE_FX_LOOPED_EXIST(TLAD_AngelLight[i])
			REMOVE_PARTICLE_FX(TLAD_AngelLight[i])
			PRINTLN("[JT PFX] Removing the light from the Angels.")
		ENDIF
		IF DOES_PARTICLE_FX_LOOPED_EXIST(TLAD_DevilLight[i])
			REMOVE_PARTICLE_FX(TLAD_DevilLight[i])
			PRINTLN("[JT PFX] Removing the light from the Devils.")
		ENDIF
		IF DOES_PARTICLE_FX_LOOPED_EXIST(TLADAngelGlow[i])
			REMOVE_PARTICLE_FX(TLADAngelGlow[i])
		ENDIF
	ENDFOR
	
	BOOL bUnloadSeaMine = FALSE
	FOR i = 0 TO (MC_serverBD.iNumObjCreated - 1)
		IF IS_MODEL_A_SEA_MINE(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn)
			bUnloadSeaMine = TRUE
		ENDIF
	ENDFOR
	If bUnloadSeaMine
		IF HAS_NAMED_PTFX_ASSET_LOADED("scr_xm_submarine") // Unloads seamine effect
			PRINTLN("[TMS] Unloading seamine particle effects")
			REMOVE_NAMED_PTFX_ASSET("scr_xm_submarine")
		ENDIF	
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
		REMOVE_NAMED_PTFX_ASSET("scr_sr_tr")
		IF DOES_PARTICLE_FX_LOOPED_EXIST(TRPfx)
			STOP_PARTICLE_FX_LOOPED(TRPfx)
			TRPfx = NULL
		ENDIF
	ENDIF
	IF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_INTERIOR_AUDIO_SUBMARINE)
		SET_STATIC_EMITTER_ENABLED("se_xm_x17dlc_int_sub_stream", FALSE)
	ENDIF
	IF players_underwater_flare != NULL
		IF DOES_PARTICLE_FX_LOOPED_EXIST( players_underwater_flare )
			STOP_PARTICLE_FX_LOOPED(players_underwater_flare)
		ENDIF
		players_underwater_flare= NULL
	ENDIF
	IF ptfxThermite[0] != NULL
		IF DOES_PARTICLE_FX_LOOPED_EXIST( ptfxThermite[0] )
			STOP_PARTICLE_FX_LOOPED(ptfxThermite[0])
		ENDIF
		ptfxThermite[0]= NULL
	ENDIF
	IF ptfxThermite[1] != NULL
		IF DOES_PARTICLE_FX_LOOPED_EXIST( ptfxThermite[1] )
			STOP_PARTICLE_FX_LOOPED(ptfxThermite[1])
		ENDIF
		ptfxThermite[1]= NULL
	ENDIF
	i = 0
	REPEAT ciMAX_LOCAL_THERMITE_PTFX i
		IF ptfxThermiteLocalDrip[i] != NULL
			IF DOES_PARTICLE_FX_LOOPED_EXIST( ptfxThermiteLocalDrip[i] )
				STOP_PARTICLE_FX_LOOPED(ptfxThermiteLocalDrip[i])
			ENDIF
			ptfxThermiteLocalDrip[i]= NULL
		ENDIF
		IF ptfxThermiteLocalSpark[i] != NULL
			IF DOES_PARTICLE_FX_LOOPED_EXIST( ptfxThermiteLocalSpark[i] )
				STOP_PARTICLE_FX_LOOPED(ptfxThermiteLocalSpark[i])
			ENDIF
			ptfxThermiteLocalSpark[i]= NULL
		ENDIF
	ENDREPEAT
	IF DrillptfxIndex != NULL
		IF DOES_PARTICLE_FX_LOOPED_EXIST( DrillptfxIndex )
			STOP_PARTICLE_FX_LOOPED(DrillptfxIndex)
		ENDIF
		DrillptfxIndex= NULL
	ENDIF
	IF OverheatptfxIndex != NULL
		IF DOES_PARTICLE_FX_LOOPED_EXIST( OverheatptfxIndex )
			STOP_PARTICLE_FX_LOOPED(OverheatptfxIndex)
		ENDIF
		OverheatptfxIndex= NULL
	ENDIF
	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(trans_lights_ptfx)
		STOP_PARTICLE_FX_LOOPED(trans_lights_ptfx)
	ENDIF
	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_LoopedGunsmithLastKill)
		REMOVE_PARTICLE_FX(ptfx_LoopedGunsmithLastKill)
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck23, LBOOL23_TEAM_SWAP_IN_MISSION)
		REMOVE_NAMED_PTFX_ASSET("scr_as_trans")
	ENDIF
			
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciLOAD_POWER_PLAY_ASSETS)
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciOPTION_VEHICLE_WEAPON_ENABLED)
	OR IS_BIT_SET(iLocalBoolCheck23, LBOOL23_TEAM_SWAP_IN_MISSION)
		REMOVE_NAMED_PTFX_ASSET("scr_powerplay")
	ENDIF
	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxHeliDamageSmoke1)
		REMOVE_PARTICLE_FX(ptfxHeliDamageSmoke1)
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxHeliDamageSmoke2)
		REMOVE_PARTICLE_FX(ptfxHeliDamageSmoke2)
	ENDIF
	
	BOOL bUnloadHeliSmoke = FALSE
	PRINTLN("[MJL] iNumVehCreated = ", MC_serverBD.iNumVehCreated)
	FOR i = 0 TO (MC_serverBD.iNumVehCreated - 1)
		IF IS_THIS_MODEL_A_HELI(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
		AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetEight, ciFMMC_VEHICLE8_ENGINE_SMOKE_ON_LOW_BODY_HEALTH)
			bUnloadHeliSmoke = TRUE	
		ENDIF
	ENDFOR
	If bUnloadHeliSmoke
		IF HAS_NAMED_PTFX_ASSET_LOADED("scr_vw_finale")
			PRINTLN("[ML] Unloading helicopter smoke particle effects")
			REMOVE_NAMED_PTFX_ASSET("scr_vw_finale")
		ENDIF	
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_UseBigExplosions)
		RESET_PARTICLE_FX_OVERRIDE("exp_grd_plane")
		PRINTLN("[ML] Unloading big explosion override effect")
		REMOVE_NAMED_PTFX_ASSET("scr_vw_oil")
	ENDIF
	
	FOR i = 0 TO FMMC_MAX_NUM_DYNOPROPS - 1
	
		IF i < FMMC_MAX_NUM_OBJECTS
			IF DOES_PARTICLE_FX_LOOPED_EXIST(sObjTaserVars.ptLoopingTaserFX[i])
				REMOVE_PARTICLE_FX(sObjTaserVars.ptLoopingTaserFX[i], TRUE)
			ENDIF
		ENDIF
		
		IF DOES_PARTICLE_FX_LOOPED_EXIST(sDynoPropTaserVars.ptLoopingTaserFX[i])
			REMOVE_PARTICLE_FX(sDynoPropTaserVars.ptLoopingTaserFX[i], TRUE)
		ENDIF
	ENDFOR
	
	IF USING_POISON_GAS()
		CLEANUP_POISON_GAS()
	ENDIF
	
	IF SHOULD_LOAD_ELECTRONIC_ASSETS()
		REMOVE_NAMED_PTFX_ASSET("scr_ch_finale")
	ENDIF
	
	CLEANUP_ALL_TRAP_PTFX(MC_serverBD_2.sTrapInfo_Host)
	
	CLEANUP_PLACED_PTFX()

ENDPROC

PROC UNLOAD_MISSION_SPECIFIC_AUDIO(BOOL bFinalCleanup)
	PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - UNLOAD_MISSION_SPECIFIC_AUDIO ################################")
	INT iPart, i
	IF IS_SOUND_ID_VALID(iHardTargetCountdownTimerSoundID)
		PRINTLN("MC_SCRIPT_CLEANUP - Clearing 5s Timer for Hard Target Change.")
		iHardTargetCountdownTimerSoundID = -1
		STOP_SOUND(iHardTargetCountdownTimerSoundID)
	ENDIF
	
	IF sIWInfo.iInteractWith_AudioLoopID > -1
		IF NOT HAS_SOUND_FINISHED(sIWInfo.iInteractWith_AudioLoopID)
			STOP_SOUND(sIWInfo.iInteractWith_AudioLoopID)
		ENDIF

		PRINTLN("MC_SCRIPT_CLEANUP - Releasing sIWInfo.iInteractWith_AudioLoopID")
		SCRIPT_RELEASE_SOUND_ID(sIWInfo.iInteractWith_AudioLoopID)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TINY_RACERS_SOUNDS)
		IF IS_AUDIO_SCENE_ACTIVE("DLC_SR_TR_Winner_Screen_Scene")
			STOP_AUDIO_SCENE("DLC_SR_TR_Winner_Screen_Scene")
		ENDIF
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
		IF IS_PARTICIPANT_A_JUGGERNAUT(iPartToUse)
			STOP_AUDIO_SCENE("DLC_IE_JN_Player_Is_JN_Scene")
		ELSE
			STOP_AUDIO_SCENE("DLC_IE_JN_Player_Is_Attacker_Scene")
		ENDIF
	ENDIF
	
	IF iHalloweenSoundID > -1
	AND NOT HAS_SOUND_FINISHED(iHalloweenSoundID)
		STOP_SOUND(iHalloweenSoundID)
	ENDIF
	
	IF iElevatorDescendSound > -1
		PRINTLN("MC_SCRIPT_CLEANUP iElevatorDescendSound > -1 Cleaning up")
		STOP_SOUND(iElevatorDescendSound)
		SCRIPT_RELEASE_SOUND_ID(iElevatorDescendSound)
	ENDIF
	
	IF iSoundIDVehBeacon > -1
		STOP_SOUND(iSoundIDVehBeacon)
		PRINTLN("MC_SCRIPT_CLEANUP - Releasing iSoundIDVehBeacon")
		SCRIPT_RELEASE_SOUND_ID(iSoundIDVehBeacon)
	ENDIF
	
	IF iSiloAlarmLoopSound > -1
		STOP_SOUND(iSiloAlarmLoopSound)
		PRINTLN("MC_SCRIPT_CLEANUP - Releasing iSiloAlarmLoopSound")
		SCRIPT_RELEASE_SOUND_ID(iSiloAlarmLoopSound)
	ENDIF
	
	IF iPTLCountdownSoundID > -1
		STOP_POINTLESS_SOUND(iPTLCountdownSoundID)
		iPTLCountdownSoundID = -1
		PRINTLN("[JT AUDIO] MC_SCRIPT_CLEANUP STOPPING COUNTDOWN SOUND")
	ENDIF
	
	IF IS_SOUND_ID_VALID(iCrashSound)
	AND NOT HAS_SOUND_FINISHED(iCrashSound)
		STOP_SOUND(iCrashSound)
	ENDIF
	
	IF iOTSoundNewRound > -1 
		PRINTLN("MC_SCRIPT_CLEANUP - Releasing iOTSoundNewRound")
		SCRIPT_RELEASE_SOUND_ID(iOTSoundNewRound)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciCONDEMNED_MODE_ENABLED)
		PRINTLN("MC_SCRIPT_CLEANUP - Releasing iCondemnedSoundID")
		SCRIPT_RELEASE_SOUND_ID(iCondemnedSoundID)
	ENDIF
	
	IF IS_SOUND_ID_VALID(iBoundsTimerSound)
	AND NOT HAS_SOUND_FINISHED(iBoundsTimerSound)
		STOP_SOUND(iBoundsTimerSound)
	ENDIF
	
	IF iStartCargoBobSound != - 1
		STOP_SOUND(SoundIDCargoBob)
		IF NOT IS_STRING_NULL_OR_EMPTY(sCargoBobAudioScene)
			IF IS_AUDIO_SCENE_ACTIVE(sCargoBobAudioScene)
				STOP_AUDIO_SCENE(sCargoBobAudioScene) 
			ENDIF
		ENDIF
		PRINTLN("[CARGO_AUDIO]STOP_AUDIO_SCENE = DLC_Apartments_Drop_Zone_Helicopter_Scen")
		iStartCargoBobSound = - 1
	ENDIF
	
	IF iDroneCutsceneSound != -1
		PRINTLN("[PROCESS_MUSIC] Ending audio scene for camera drone cutscene")
		STOP_SOUND(iDroneCutsceneSound)
		STOP_AUDIO_SCENE("DLC_GR_WVM_APC_Sounds")
		PRINTLN("MC_SCRIPT_CLEANUP - Releasing iDroneCutsceneSound")
		SCRIPT_RELEASE_SOUND_ID(iDroneCutsceneSound)
		iDroneCutsceneSound = -1
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciENABLE_POWER_MAD_SOUNDS)
		FOR iPart = 0 TO (MAX_NUM_MC_PLAYERS - 1)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
				PED_INDEX tempPed = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart)))
				IF DOES_ENTITY_EXIST(tempPed)
				AND IS_ENTITY_ALIVE(tempPed)
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(tempPed)
					USE_FOOTSTEP_SCRIPT_SWEETENERS(tempPed, FALSE, HASH("DLC_GR_PM_Juggernaut_Footstep_Sounds"))
				ENDIF
			ENDIF
		ENDFOR
	
		IF IS_PARTICIPANT_A_JUGGERNAUT(iPartToUse)
			STOP_AUDIO_SCENE("DLC_GR_PM_JN_Player_Is_JN_Scene")
		ELSE
			STOP_AUDIO_SCENE("DLC_GR_PM_JN_Player_Is_Attacker_Scene")
		ENDIF
		IF IS_AUDIO_SCENE_ACTIVE("DLC_GR_PM_Beast_Gameplay_Scene")
			PRINTLN("[LM] MC_SCRIPT_CLEANUP ENDING_2 (\"DLC_GR_PM_Beast_Gameplay_Scene\") Stopping Scene.")
			STOP_AUDIO_SCENE("DLC_GR_PM_Beast_Gameplay_Scene")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC)
	AND NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_KILLED_APT_COUNTDOWN_MUSIC)
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciUSE_FREEMODE_30S_COUNTDOWN)
			CANCEL_MUSIC_EVENT("FM_COUNTDOWN_30S_KILL")
		ELSE
			CANCEL_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck15, LBOOL15_HAS_STARTED_VAL_COUNTDOWN_MUSIC)
	AND NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL15_HAS_KILLED_VAL_COUNTDOWN_MUSIC)
		CANCEL_MUSIC_EVENT("VAL2_COUNTDOWN_30S_KILL")
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck20, LBOOL20_HAS_STARTED_IE_COUNTDOWN_MUSIC)
	AND NOT IS_BIT_SET(iLocalBoolCheck20, LBOOL20_HAS_KILLED_IE_COUNTDOWN_MUSIC)
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciUSE_IE_30S_COUNTDOWN)
			CANCEL_MUSIC_EVENT("IE_COUNTDOWN_30S_KILL")
			PRINTLN("CANCEL_MUSIC_EVENT : IE_COUNTDOWN_30S_KILL")
		ENDIF
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("MP_Reduce_Score_For_Emitters_Scene")
		STOP_AUDIO_SCENE("MP_Reduce_Score_For_Emitters_Scene")
		PRINTLN("STOP_AUDIO_SCENE =  MC_SCRIPT_CLEANUP MP_Reduce_Score_For_Emitters_Scene")
	ENDIF
	
	SET_AUDIO_FLAG("AllowScriptedSpeechInSlowMo", FALSE) 
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciDRIVEBY_SHOOTOUT_CAR_PARK_RADIO)
		IF IS_AUDIO_SCENE_ACTIVE("MP_Reduce_Score_For_Emitters_Scene")
			STOP_AUDIO_SCENE("MP_Reduce_Score_For_Emitters_Scene")
			PRINTLN("STOP_AUDIO_SCENE = MP_Reduce_Score_For_Emitters_Scene / DISABLE SE_LR_Car_Park_Radio_01")
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("MP_POSITIONED_RADIO_MUTE_SCENE")
			SET_AUDIO_SCENE_VARIABLE("MP_POSITIONED_RADIO_MUTE_SCENE", "apply", 1)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[0][0].iSpawnBitSet, ci_SpawnBS_AirSpawn)
		PRINTLN("[JJT] MC_SCRIPT_CLEANUP - Releasing iDZCargobobRampSoundID")
		SCRIPT_RELEASE_SOUND_ID(iDZCargobobRampSoundID)
	ENDIF
	
	//Halloween cleanup
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciHWN_BREATHING_ENABLED)
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciPRE_GAME_EMP)
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_SLASHERS_AUDIO)
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_HALLOWEEN/FVJ_01")
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciHWN_BREATHING_ENABLED)
			SCRIPT_RELEASE_SOUND_ID(iHunterBreathingSoundID)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_ENABLE_TRAP_DOOR_SOUNDS)
		PRINTLN("[RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK Cleaning up audio banks - DLC_ASSAULT/AS_TRP_01 & DLC_ASSAULT/AS_TRP_02 ")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_ASSAULT/AS_TRP_01")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_ASSAULT/AS_TRP_02")
		IF IS_AUDIO_SCENE_ACTIVE("DLC_AS_TRP_Scene")
			STOP_AUDIO_SCENE("DLC_AS_TRP_Scene")
			PRINTLN("[LM][TRAPDOOR] STOP_AUDIO_SCENE(\"DLC_AS_TRP_Scene\")")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_SUMO_RUN_SOUNDS)
		PRINTLN("[RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK Cleaning up audio banks - DLC_BATTLE/DLC_BTL_SM_REMIX_01")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_BATTLE/DLC_BTL_SM_REMIX_01")
		IF IS_AUDIO_SCENE_ACTIVE("DLC_BTL_SM_Remix_General_Scene")
			STOP_AUDIO_SCENE("DLC_BTL_SM_Remix_General_Scene")
			PRINTLN("[LM][TRAPDOOR] STOP_AUDIO_SCENE(\"DLC_BTL_SM_Remix_General_Scene\")")
		ENDIF
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_SUBMARINE_FINALE)
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_CHRISTMAS2017/XM_SUBMARINE")
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_SERVERFARM)
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_CHRISTMAS2017/XM_FARM")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_ENABLE_ALTERNATE_LIGHTS_OFF_AUDIO)
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_CHRISTMAS2017/XM_STEALAVG")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciENABLE_GO_AIRHORN)
	OR g_bIsRunningBackMission = TRUE
		IF NOT CONTENT_IS_USING_ARENA()
			PRINTLN("[RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_HALLOWEEN/TG_01) within MC_SCRIPT_CLEANUP")
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_HALLOWEEN/TG_01")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciENABLE_SUMO_SOUNDS)
		PRINTLN("[RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_LOW2/SUMO_01) within MC_SCRIPT_CLEANUP")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_LOW2/SUMO_01") //B* 2666917
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_RUGBY_SOUNDS)
		PRINTLN("[RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_LOW2/IBI_01) within MC_SCRIPT_CLEANUP")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_LOW2/IBI_01")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciENABLE_LOST_AND_DAMNED_SOUNDS)
		PRINTLN("[RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_BIKER/L_AND_D_01) within MC_SCRIPT_CLEANUP")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_BIKER/L_AND_D_01")
		
		PRINTLN("[RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_BIKER/L_AND_D_02) within MC_SCRIPT_CLEANUP")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_BIKER/L_AND_D_02")
	ENDIF
	
	// Taken out deliberately. Leave it here.
	IF IS_AUDIO_SCENE_ACTIVE("DLC_BTL_HP_Remix_General_Scene")
		STOP_AUDIO_SCENE("DLC_BTL_HP_Remix_General_Scene")
		PRINTLN("[TMS] STOP_AUDIO_SCENE(\"DLC_BTL_HP_Remix_General_Scene\")")
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_HUNTING_PACK_REMIX_SOUNDS)
		PRINTLN("[TMS][RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_STUNT/STUNT_RACE_01) within MC_SCRIPT_CLEANUP")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_STUNT/STUNT_RACE_01")
		
		PRINTLN("[TMS][RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_STUNT/STUNT_RACE_02) within MC_SCRIPT_CLEANUP")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_STUNT/STUNT_RACE_02")
		
		PRINTLN("[TMS][RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_STUNT/STUNT_RACE_03) within MC_SCRIPT_CLEANUP")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_STUNT/STUNT_RACE_03")
	ENDIF
	
	IF USING_SHOWDOWN_POINTS() // Commented out as the audio for this has been removed/moved to another pack
//		IF IS_AUDIO_SCENE_ACTIVE("DLC_AS_SHW_Scene")
//			STOP_AUDIO_SCENE("DLC_AS_SHW_Scene")
//			PRINTLN("[TMS][SHOWDOWN] STOP_AUDIO_SCENE(\"DLC_AS_SHW_Scene\")")
//		ENDIF
//		
//		PRINTLN("[TMS][SHOWDOWN][RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_ASSAULT/AS_SHW_01) within MC_SCRIPT_CLEANUP")
//		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_ASSAULT/AS_SHW_01")
//		
//		PRINTLN("[TMS][SHOWDOWN][RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_ASSAULT/AS_SHW_02) within MC_SCRIPT_CLEANUP")
//		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_ASSAULT/AS_SHW_02")
	ENDIF

	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_POINTLESS_SOUNDS)
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_IMPORTEXPORT/IE_PL_01")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_IMPORTEXPORT/IE_PL_02")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_IMPORTEXPORT/IE_PL_03")
		PRINTLN("[RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK() ALL POINTLESS SOUND BANKS")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_LAND_GRAB_SOUNDS)	
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_SPECIALRACES/SR_LG")
		IF IS_AUDIO_SCENE_ACTIVE("DLC_SR_LG_General_Scene")
			PRINTLN("[LM] STOP_AUDIO_SCENE(DLC_SR_LG_General_Scene)")
			STOP_AUDIO_SCENE("DLC_SR_LG_General_Scene")
		ENDIF
		PRINTLN("[RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK() ALL LAND GRAB SOUND BANKS")
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_MISSILE_SILO_FINALE_P2)
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_CHRISTMAS2017/XM_SILO")
		PRINTLN("[TMS][SiloAlarm] Releasing audio bank now!")
	ENDIF	
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_LIONS_DEN_SOUNDS)
//		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_GUNRUNNING/GR_LD")
//		IF IS_AUDIO_SCENE_ACTIVE("DLC_GR_LD_General_Scene")
//			PRINTLN("[LM] STOP_AUDIO_SCENE(DLC_GR_LD_General_Scene)")
//			STOP_AUDIO_SCENE("DLC_GR_LD_General_Scene")
//		ENDIF
//		PRINTLN("[RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(\"DLC_GUNRUNNING/GR_LD\") ALL LIONS DEN SOUND BANKS")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_OVERTIME_SOUNDS)	
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_GUNRUNNING/GR_OT")
		IF IS_AUDIO_SCENE_ACTIVE("DLC_GR_OT_General_Scene")
			PRINTLN("[LM] STOP_AUDIO_SCENE(DLC_GR_OT_General_Scene)")
			STOP_AUDIO_SCENE("DLC_GR_OT_General_Scene")
		ENDIF
		PRINTLN("[RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(\"DLC_GUNRUNNING/GR_OT\") ALL OVERTIME SOUND BANKS")
	ENDIF	
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciENABLE_POWER_MAD_SOUNDS)
		PRINTLN("[RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_GUNRUNNING/GR_PM_01) within MC_SCRIPT_CLEANUP")
		PRINTLN("[RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_GUNRUNNING/GR_PM_02) within MC_SCRIPT_CLEANUP")
		PRINTLN("[RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_GUNRUNNING/GR_PM_03) within MC_SCRIPT_CLEANUP")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_GUNRUNNING/GR_PM_01")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_GUNRUNNING/GR_PM_02")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_GUNRUNNING/GR_PM_03")
		IF IS_AUDIO_SCENE_ACTIVE("DLC_GR_PM_General_Scene")
			PRINTLN("[LM] STOP_AUDIO_SCENE(DLC_GR_PM_General_Scene)")
			STOP_AUDIO_SCENE("DLC_GR_PM_General_Scene")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_VEHICULAR_WARFARE_SOUNDS)
		PRINTLN("[RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_SMUGGLER/SM_VEHWA_01) within MC_SCRIPT_CLEANUP")
		PRINTLN("[RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_SMUGGLER/SM_VEHWA_02) within MC_SCRIPT_CLEANUP")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_SMUGGLER/SM_VEHWA_01")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_SMUGGLER/SM_VEHWA_02")
		IF IS_AUDIO_SCENE_ACTIVE("DLC_SM_VEHWA_General_Scene")
			PRINTLN("STOP_AUDIO_SCENE(DLC_SM_VEHWA_General_Scene)")
			STOP_AUDIO_SCENE("DLC_SM_VEHWA_General_Scene")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_CONDEMNED_SOUNDS)
		PRINTLN("[RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_SMUGGLER/SM_CND_01) within MC_SCRIPT_CLEANUP")
		PRINTLN("[RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_SMUGGLER/SM_CND_02) within MC_SCRIPT_CLEANUP")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_SMUGGLER/SM_CND_01")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_SMUGGLER/SM_CND_02")
		IF IS_AUDIO_SCENE_ACTIVE("DLC_SM_CND_General_Scene")
			PRINTLN("STOP_AUDIO_SCENE(DLC_SM_CND_General_Scene)")
			STOP_AUDIO_SCENE("DLC_SM_CND_General_Scene")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_HOSTILE_TAKEOVER_SOUNDS)
		PRINTLN("[RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_CHRISTMAS2017/XM_HOTA_01) within MC_SCRIPT_CLEANUP")
		PRINTLN("[RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_CHRISTMAS2017/XM_HOTA_02) within MC_SCRIPT_CLEANUP")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_CHRISTMAS2017/XM_HOTA_01")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_CHRISTMAS2017/XM_HOTA_02")
		IF IS_AUDIO_SCENE_ACTIVE("DLC_XM_HOTA_Scene")
			PRINTLN("STOP_AUDIO_SCENE(DLC_XM_HOTA_Scene)")
			STOP_AUDIO_SCENE("DLC_XM_HOTA_Scene")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_SLASHERS_AUDIO)
		PRINTLN("[RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_CHRISTMAS2017/XM_SLS_01) within MC_SCRIPT_CLEANUP")
		PRINTLN("[RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_CHRISTMAS2017/XM_SLS_02) within MC_SCRIPT_CLEANUP")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_CHRISTMAS2017/XM_SLS_01")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_CHRISTMAS2017/XM_SLS_02")
		IF IS_AUDIO_SCENE_ACTIVE("DLC_XM_SLS_Scene")
			PRINTLN("STOP_AUDIO_SCENE(DLC_XM_SLS_Scene)")
			STOP_AUDIO_SCENE("DLC_XM_SLS_Scene")
		ENDIF
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_AIR_QUOTA_AUDIO)
		PRINTLN("[RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_CHRISTMAS2017/XM_AQO_01) within MC_SCRIPT_CLEANUP")
		PRINTLN("[RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_CHRISTMAS2017/XM_AQO_02) within MC_SCRIPT_CLEANUP")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_CHRISTMAS2017/XM_AQO_01")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_CHRISTMAS2017/XM_AQO_02")
		IF IS_AUDIO_SCENE_ACTIVE("DLC_XM_AQO_Scene")
			PRINTLN("STOP_AUDIO_SCENE(DLC_XM_AQO_Scene)")
			STOP_AUDIO_SCENE("DLC_XM_AQO_Scene")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_HARD_TARGET_AUDIO)
		PRINTLN("[RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_CHRISTMAS2017/XM_HATA_01) within MC_SCRIPT_CLEANUP")
		PRINTLN("[RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_CHRISTMAS2017/XM_HATA_02) within MC_SCRIPT_CLEANUP")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_CHRISTMAS2017/XM_HATA_01")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_CHRISTMAS2017/XM_HATA_02")
		IF IS_AUDIO_SCENE_ACTIVE("DLC_XM_HATA_Scene")
			PRINTLN("STOP_AUDIO_SCENE(DLC_XM_HATA_Scene)")
			STOP_AUDIO_SCENE("DLC_XM_HATA_Scene")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_STOCKPILE_SOUNDS)
		PRINTLN("[RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_SMUGGLER/SM_STPI_01) within MC_SCRIPT_CLEANUP")
		PRINTLN("[RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_SMUGGLER/SM_STPI_02) within MC_SCRIPT_CLEANUP")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_SMUGGLER/SM_STPI_01")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_SMUGGLER/SM_STPI_02")
		IF IS_AUDIO_SCENE_ACTIVE("DLC_SM_STPI_General_Scene")
			PRINTLN("STOP_AUDIO_SCENE(DLC_SM_STPI_General_Scene)")
			STOP_AUDIO_SCENE("DLC_SM_STPI_General_Scene")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_AIR_SHOOUTOUT_SOUNDS)
		PRINTLN("[RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_SMUGGLER/SM_AS_01) within MC_SCRIPT_CLEANUP")
		PRINTLN("[RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_SMUGGLER/SM_AS_02) within MC_SCRIPT_CLEANUP")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_SMUGGLER/SM_AS_01")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_SMUGGLER/SM_AS_02")
		IF IS_AUDIO_SCENE_ACTIVE("DLC_SM_AS_General_Scene")
			PRINTLN("STOP_AUDIO_SCENE(DLC_SM_AS_General_Scene)")
			STOP_AUDIO_SCENE("DLC_SM_AS_General_Scene")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_BOMBUSHKA_SOUNDS)
		PRINTLN("[RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_SMUGGLER/SM_BR_01) within MC_SCRIPT_CLEANUP")
		PRINTLN("[RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_SMUGGLER/SM_BR_02) within MC_SCRIPT_CLEANUP")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_SMUGGLER/SM_BR_01")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_SMUGGLER/SM_BR_02")
		IF IS_AUDIO_SCENE_ACTIVE("DLC_SM_BR_General_Scene")
			PRINTLN("STOP_AUDIO_SCENE(DLC_SM_BR_General_Scene)")
			STOP_AUDIO_SCENE("DLC_SM_BR_General_Scene")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_RESURRECTION_SOUNDS)
		STOP_AUDIO_SCENE("DLC_SR_RS_General_Scene")
		PRINTLN("[RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_SPECIALRACES/SR_RS) within MC_SCRIPT_CLEANUP")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_SPECIALRACES/SR_RS")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TINY_RACERS_SOUNDS)
		STOP_AUDIO_SCENE("DLC_SR_TR_General_Scene")
		PRINTLN("[RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_SPECIALRACES/SR_TR) within MC_SCRIPT_CLEANUP")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_SPECIALRACES/SR_TR")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_JUGGERNAUT_SOUNDS)
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TRANSFORM_ASSETS)
		PRINTLN("[RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_IMPORTEXPORT/IE_JN_01) within MC_SCRIPT_CLEANUP")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_IMPORTEXPORT/IE_JN_01")
		
		PRINTLN("[RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_IMPORTEXPORT/IE_JN_02) within MC_SCRIPT_CLEANUP")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_IMPORTEXPORT/IE_JN_02")
		
		PRINTLN("[RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_IMPORTEXPORT/IE_JN_03) within MC_SCRIPT_CLEANUP")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_IMPORTEXPORT/IE_JN_03")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_TRADING_PLACES_SOUNDS)
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_BATTLE/DLC_BTL_TP_REMIX_01")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_BATTLE/DLC_BTL_TP_REMIX_02")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_BATTLE/DLC_BTL_TP_REMIX_03")
		PRINTLN("[RCC MISSION] Releasing Trading palces audio banks: DLC_BTL_TP_REMIX_01, DLC_BTL_TP_REMIX_02, and DLC_BTL_TP_REMIX_03")
		
		IF IS_AUDIO_SCENE_ACTIVE("DLC_BTL_TP_Remix_General_Scene")
			STOP_AUDIO_SCENE("DLC_BTL_TP_Remix_General_Scene")
			PRINTLN("STOP_AUDIO_SCENE(\"DLC_BTL_TP_Remix_General_Scene\")")
		ENDIF
		
		FOR iPart = 0 TO (MAX_NUM_MC_PLAYERS - 1)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
				PED_INDEX tempPed = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart)))
				IF DOES_ENTITY_EXIST(tempPed)
				AND IS_ENTITY_ALIVE(tempPed)
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(tempPed)
					USE_FOOTSTEP_SCRIPT_SWEETENERS(tempPed, FALSE, HASH("DLC_GR_PM_Juggernaut_Footstep_Sounds"))
				ENDIF
			ENDIF
		ENDFOR
	ENDIF

	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARS(g_FMMC_STRUCT.iAdversaryModeType)
	OR (NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_LAND_GRAB_SOUNDS) AND IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARZONE(g_FMMC_STRUCT.iAdversaryModeType))
		PRINTLN("[RCC MISSION][LM] - RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_IMPORTEXPORT/IE_TW_01) within MC_SCRIPT_CLEANUP.")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_IMPORTEXPORT/IE_TW_01")
		
		PRINTLN("[RCC MISSION][LM] - RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_IMPORTEXPORT/IE_TW_02) within MC_SCRIPT_CLEANUP.")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_IMPORTEXPORT/IE_TW_02")
		
		PRINTLN("[RCC MISSION][LM] - RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_IMPORTEXPORT/IE_TW_03) within MC_SCRIPT_CLEANUP.")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_IMPORTEXPORT/IE_TW_03")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciDAWN_RAID_ENABLE_MODE)
		IF IS_AUDIO_SCENE_ACTIVE("DLC_GR_DR_General_Scene")
			STOP_AUDIO_SCENE("DLC_GR_DR_General_Scene")
			PRINTLN("STOP_AUDIO_SCENE(\"DLC_GR_DR_General_Scene\")")
		ENDIF
		
		PRINTLN("[RCC MISSION] - RELEASE_NAMED_SCRIPT_AUDIO_BANK(\"DLC_GUNRUNNING/GR_DR\") within MC_SCRIPT_CLEANUP.")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_GUNRUNNING/GR_DR")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_ENABLE_VENETIAN_JOB_SOUNDS)
		IF IS_AUDIO_SCENE_ACTIVE("DLC_AS_VNT_Scene")
			STOP_AUDIO_SCENE("DLC_AS_VNT_Scene")
			PRINTLN("STOP_AUDIO_SCENE(\"DLC_AS_VNT_Scene\")")
		ENDIF
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_ASSAULT/AS_VNT_01")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_ASSAULT/AS_VNT_02")
		PRINTLN("[RCC MISSION] - RELEASE_NAMED_SCRIPT_AUDIO_BANK DLC_ASSAULT/AS_VNT_01 & DLC_ASSAULT/AS_VNT_02 within MC_SCRIPT_CLEANUP.")
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARS(g_FMMC_STRUCT.iAdversaryModeType)
		IF IS_BIT_SET(iLocalBoolCheck, LBOOL20_PARTICLE_FX_OVERRIDE_RPG)
			PRINTLN("[RCC MISSION] RESET_PARTICLE_FX_OVERRIDE \"proj_rpg_trail\"")
			RESET_PARTICLE_FX_OVERRIDE("proj_rpg_trail")
			CLEAR_BIT(iLocalBoolCheck, LBOOL20_PARTICLE_FX_OVERRIDE_RPG)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_INTERIOR_AUDIO_MOUNTAINBASE)
		PRINTLN("[LM][RCC MISSION] - [Audio] - [MountainBase] - clearing mountain base Audio (MC_SCRIPT_CLEANUP)")
		SET_AMBIENT_ZONE_LIST_STATE("dlc_xm_int_base_zones", false, false)
		STOP_AUDIO_SCENE("dlc_xm_mountain_base_scene")
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_INTERIOR_AUDIO_FACILITY_PROPERTY)
		PRINTLN("[LM][RCC MISSION] - [Audio] - [Facility Proper] - clearing Player Facility Audio Scene (MC_SCRIPT_CLEANUP)")
		STOP_AUDIO_SCENE("dlc_xm_facility_property_scene")
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_INTERIOR_AUDIO_SUBMARINE)
		PRINTLN("[LM][RCC MISSION] - [Audio] - [Submarine] - clearing Player Facility Audio (MC_SCRIPT_CLEANUP)")
		SET_AMBIENT_ZONE_LIST_STATE("azl_xm_int_sub_zones", FALSE, FALSE)
		STOP_AUDIO_SCENE("dlc_xm_submarine_finale_sub_interior_scene")
	ENDIF
	
	IF NOT IS_THIS_IS_A_STRAND_MISSION()
	AND (IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
	OR IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_IN_BMBFB_SUDDEN_DEATH))
		TRIGGER_MUSIC_EVENT( "FM_SUDDEN_DEATH_STOP_MUSIC" )  
	ENDIF
	
	SET_AUDIO_FLAG("EnableHeadsetBeep",FALSE)	
		
	IF g_FMMC_STRUCT.iRootContentIDHash = g_sMPTunables.iroot_id_HASH_Prison_Break_Station
		SET_AMBIENT_ZONE_STATE( "AZ_DLC_HEIST_POLICE_STATION_BOOST", FALSE, TRUE )
	ENDIF

	IF IS_AUDIO_SCENE_ACTIVE("DLC_HEIST_PACIFIC_BANK_HACK_PASSWORD_SCENE")
		STOP_AUDIO_SCENE("DLC_HEIST_PACIFIC_BANK_HACK_PASSWORD_SCENE" )
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("DLC_HEIST_BIOLAB_ENEMY_GUN_BOOST_SCENE")
		STOP_AUDIO_SCENE("DLC_HEIST_BIOLAB_ENEMY_GUN_BOOST_SCENE")		
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("DLC_HEIST_SERIES_A_BIKERS_TRAILERPARK_SCENE")
		STOP_AUDIO_SCENE("DLC_HEIST_SERIES_A_BIKERS_TRAILERPARK_SCENE")
	ENDIF
	
	IF g_HeistGarageDropoffPanStarted
		TRIGGER_MUSIC_EVENT("GLOBAL_KILL_MUSIC_FADEIN_RADIO")
	ENDIF
	
	#IF IS_NEXTGEN_BUILD
		IF IS_BIT_SET( iLocalBoolCheck10, LBOOL10_TRIGGERED_DANGER_ZONE_FADE_OUT )
			TRIGGER_MUSIC_EVENT("MP_MC_DZ_FIRA")
		ENDIF
	#ENDIF
	
	//Stop any audio mixes
	STOP_ANY_CURRENTLY_PLAYING_AUDIO_MIX()
	
	IF IS_SOUND_ID_VALID(iSoundIDCountdown)
		PRINTLN("[MC_SCRIPT_CLEANUP] STOP_SOUND(iSoundIDCountdown")
		STOP_SOUND(iSoundIDCountdown)
		RELEASE_SOUND_ID(iSoundIDCountdown)
		iSoundIDCountdown = -1
	ENDIF
				
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_RUNNING_BACK_REMIX_SOUNDS)
		PRINTLN("[LM][RCC MISSION] - [Audio] - Releasing Audio Banks: DLC_STUNT/STUNT_RACE_01, DLC_STUNT/STUNT_RACE_02, DLC_STUNT/STUNT_RACE_03.")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_STUNT/STUNT_RACE_01")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_STUNT/STUNT_RACE_02")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_STUNT/STUNT_RACE_03")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_BATTLE/DLC_BTL_RB_REMIX_01")
		
		IF IS_AUDIO_SCENE_ACTIVE("DLC_BTL_RB_Remix_General_Scene")
			PRINTLN("[LM] START_AUDIO_SCENE(\"DLC_BTL_RB_Remix_General_Scene\") Stopping Scene.")
			STOP_AUDIO_SCENE("DLC_BTL_RB_Remix_General_Scene")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_COME_OUT_TO_PLAY_REMIX_SOUNDS)
		IF IS_AUDIO_SCENE_ACTIVE("DLC_BTL_COTP_Remix_General_Scene")
			PRINTLN("[LM] START_AUDIO_SCENE UNLOAD_MISSION_SPECIFIC_AUDIO(\"DLC_BTL_COTP_Remix_General_Scene\") Stopping Scene.")
			STOP_AUDIO_SCENE("DLC_BTL_COTP_Remix_General_Scene")
		ENDIF
	ENDIF
	
	IF GET_VCM_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash) = eVCM_BRAWL
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_VINEWOOD/VW_Casino_HK")
		STOP_AUDIO_SCENE("dlc_vw_casino_brawl_casino_interior_scene")
		STOP_AUDIO_SCENE("dlc_vw_casino_brawl_fight_scene")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_EnableCasinoHeistAudioBank)
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_HEIST3\CASINO_HEIST_FINALE_GENERAL_01")
		PRINTLN("[LM] RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_HEIST3/CASINO_HEIST_FINALE_GENERAL_01) - Cleaned up successfully.")
	ENDIF
	
	g_bCasinoShockedAmbientPedDialogue = FALSE
	
	IF IS_SOUND_ID_VALID(iBoundsExplodeSound)
	AND NOT HAS_SOUND_FINISHED(iBoundsExplodeSound)
		STOP_SOUND(iBoundsExplodeSound)
	ENDIF
	
	IF IS_SOUND_ID_VALID(iSoundIDDawnRaidPickup)
	AND NOT HAS_SOUND_FINISHED(iSoundIDDawnRaidPickup)
		PRINTLN("[MC_SCRIPT_CLEANUP] STOP_SOUND(iSoundIDDawnRaidPickup")
		STOP_SOUND(iSoundIDDawnRaidPickup)
	ENDIF
	
	IF IS_SOUND_ID_VALID(iSoundIDVehBomb)
	AND NOT HAS_SOUND_FINISHED(iSoundIDVehBomb)
		PRINTLN("[TMS] Stopping iSoundIDVehBomb")
		STOP_SOUND(iSoundIDVehBomb)
	ENDIF
	IF IS_SOUND_ID_VALID(iSoundIDVehBomb2)
	AND NOT HAS_SOUND_FINISHED(iSoundIDVehBomb2)
		PRINTLN("[TMS] Stopping iSoundIDVehBomb2")
		STOP_SOUND(iSoundIDVehBomb2)
	ENDIF
	
	IF IS_SOUND_ID_VALID(iSoundIDVehBombCountdown)
	AND NOT HAS_SOUND_FINISHED(iSoundIDVehBombCountdown)
		PRINTLN("[TMS] Stopping iSoundIDVehBombCountdown")
		STOP_SOUND(iSoundIDVehBombCountdown)
	ENDIF
	
	SCRIPT_RELEASE_SOUND_ID(iSoundIDDawnRaidPickup)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iSoundIDDawnRaidPickup")
	SCRIPT_RELEASE_SOUND_ID(iSoundIDVehBomb)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iSoundIDVehBomb")
	SCRIPT_RELEASE_SOUND_ID(iSoundIDVehBomb2)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iSoundIDVehBomb2")
	SCRIPT_RELEASE_SOUND_ID(iSlipStreamSoundId)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iSlipStreamSoundId")
	SCRIPT_RELEASE_SOUND_ID(iGarageDoorSoundID)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iGarageDoorSoundID")
	SCRIPT_RELEASE_SOUND_ID(iEmpSoundId)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iEmpSoundId")
	SCRIPT_RELEASE_SOUND_ID(soundFxLift)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID soundFxLift")
	SCRIPT_RELEASE_SOUND_ID(SoundIDCargoBob)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID SoundIDCargoBob")
	SCRIPT_RELEASE_SOUND_ID(monkey_sound)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID monkey_sound")
	SCRIPT_RELEASE_SOUND_ID(iCrashSound)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iCrashSound")
	SCRIPT_RELEASE_SOUND_ID(iSoundIDCamBackground)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iSoundIDCamBackground")
	SCRIPT_RELEASE_SOUND_ID(iSoundIDCountdown)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iSoundIDCountdown")
	SCRIPT_RELEASE_SOUND_ID(iSoundPan)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iSoundPan")
	SCRIPT_RELEASE_SOUND_ID(iSoundZoom)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iSoundZoom")
	SCRIPT_RELEASE_SOUND_ID(iCamSound)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iCamSound")
	SCRIPT_RELEASE_SOUND_ID(garage_door_sound_id)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID garage_door_sound_id")
	SCRIPT_RELEASE_SOUND_ID(iBeastAttackSoundID)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iBeastAttackSoundID")
	SCRIPT_RELEASE_SOUND_ID(iBeastSprintSoundID)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iBeastSprintSoundID")
	SCRIPT_RELEASE_SOUND_ID(iThermalSoundID)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iThermalSoundID")
	SCRIPT_RELEASE_SOUND_ID(iNightVisSoundID)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iNightVisSoundID")
	SCRIPT_RELEASE_SOUND_ID(iBoundsExplodeSound)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iBoundsExplodeSound")
	SCRIPT_RELEASE_SOUND_ID(iBoundsTimerSound)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iBoundsTimerSound")
	SCRIPT_RELEASE_SOUND_ID(iRugbyCarryingBallSoundID)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iRugbyCarryingBallSoundID")
	SCRIPT_RELEASE_SOUND_ID(iSoundIDLastAlive)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iSoundIDLastAlive")
	SCRIPT_RELEASE_SOUND_ID(iTrailSound0)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iTrailSound0")
	SCRIPT_RELEASE_SOUND_ID(iTrailSound1)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iTrailSound1")
	SCRIPT_RELEASE_SOUND_ID(iTrailSound2)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iTrailSound2")
	SCRIPT_RELEASE_SOUND_ID(iTrailSound3)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iTrailSound3")
	SCRIPT_RELEASE_SOUND_ID(iStealthSoundID)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iStealthSoundID")	
	SCRIPT_RELEASE_SOUND_ID(iSlipStreamLeaderSoundId)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iSlipStreamLeaderSoundId")
	STOP_ALL_POWER_PLAY_PICKUP_AUDIO()
	
	CLEAN_UP_SERVER_FARM_ALARM_SOUND()
	CLEAN_UP_SUBMARINE_ALARM_SOUND()	
	CLEAR_LAST_ALIVE_SOUND_EFFECT()
	CLEANUP_MISSION_FLARE_PTFX()
	
	IF bFinalCleanup
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciTURN_OFF_PRISON_YARD_AMB)
			SET_AMBIENT_ZONE_STATE("AZ_COUNTRYSIDE_PRISON_01_ANNOUNCER_ALARM",TRUE,TRUE)
			SET_AMBIENT_ZONE_STATE("AZ_COUNTRYSIDE_PRISON_01_ANNOUNCER_WARNING",TRUE,TRUE)
			SET_AMBIENT_ZONE_STATE("AZ_COUNTRYSIDE_PRISON_01_ANNOUNCER_GENERAL",TRUE,TRUE)
			PRINTLN("TURNING BACK ON PRISON AMBIENCE")
		ENDIF
	ENDIF
	
	IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		IF NOT g_bCelebrationScreenIsActive
		AND NOT IS_THIS_AN_ADVERSARY_MODE_MISSION(0, g_FMMC_STRUCT.iAdversaryModeType)
			SCRIPT_TRIGGER_MUSIC_EVENT_STOP()
		ENDIF
	ENDIF
	
	STOP_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
	
	IF CONTENT_IS_USING_ARENA()
		IF IS_AUDIO_SCENE_ACTIVE("MP_CELEB_SCREEN_ARENA_SCENE")
			STOP_AUDIO_SCENE("MP_CELEB_SCREEN_ARENA_SCENE")
		ENDIF
	ELIF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciPLAY_POWERPLAY_INTRO_ANNOUNCER)
		STOP_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
	ELSE
		STOP_AUDIO_SCENE("MP_CELEB_SCREEN_PP_SCENE")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("MP_LEADERBOARD_SCENE")
		STOP_AUDIO_SCENE("MP_LEADERBOARD_SCENE")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MP_LEADERBOARD_PP_SCENE")
		STOP_AUDIO_SCENE("MP_LEADERBOARD_PP_SCENE")
	ENDIF
	STOP_AUDIO_SCENE("DLC_HEISTS_ALARM_INITIAL_BOOST_SCENE" )
	SET_AUDIO_FLAG("DisableFlightMusic", FALSE) 
	SET_AUDIO_FLAG("WantedMusicDisabled", FALSE)
	SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE)
	SET_AUDIO_FLAG("IsPlayerOnMissionForSpeech",FALSE)
	REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(PLAYER_PED_ID(), 0.0)
	SET_PED_INTERIOR_WALLA_DENSITY(1,0)
	SET_AMBIENT_ZONE_STATE("AZ_DLC_HEISTS_BIOLAB", FALSE, FALSE)
	
	IF NOT g_bCelebrationScreenIsActive
		IF NOT CONTENT_IS_USING_ARENA()
			TRIGGER_MUSIC_EVENT("MP_GLOBAL_RADIO_FADE_IN")
		ELSE
			TRIGGER_MUSIC_EVENT("AW_FADE_IN_RADIO")
		ENDIF
	ENDIF
	
	STOP_LBD_AUDIO_SCENE()
	TOGGLE_STRIPPER_AUDIO(TRUE)
	
	STOP_STREAM()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_POWERPLAY_PICKUP_SOUNDS)
		ALLOW_DIALOGUE_IN_WATER(FALSE)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciMUTE_AMBIENT_CITY_SOUNDS)
		IF IS_AUDIO_SCENE_ACTIVE("GTAO_Mute_Traffic_Ambience_Scene")
			STOP_AUDIO_SCENE("GTAO_Mute_Traffic_Ambience_Scene")
			PRINTLN("[LM] - MC_SCRIPT_CLEANUP - STOP_AUDIO_SCENE: GTAO_Mute_Traffic_Ambience_Scene")
		ENDIF
	ENDIF
	
	RELEASE_MISSION_AUDIO_BANK()
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("SCRIPT\\LIFTS")
	
	PRINTLN("iOutAreaSound = ", iOutAreaSound)
	//stop penned in countdown sounds
	IF IS_SOUND_ID_VALID(iOutAreaSound)
		IF NOT HAS_SOUND_FINISHED(iOutAreaSound)
			STOP_SOUND(iOutAreaSound)
		ENDIF
	ENDIF
	
	SCRIPT_RELEASE_SOUND_ID(iOutAreaSound)
	
	IF IS_SOUND_ID_VALID(iRugbyCarryingBallSoundID)
		IF NOT HAS_SOUND_FINISHED(iRugbyCarryingBallSoundID)
			STOP_SOUND(iRugbyCarryingBallSoundID)
		ENDIF
	ENDIF
	
	IF iDoorAlarmSoundID != -1
		STOP_SOUND(iDoorAlarmSoundID)
		SCRIPT_RELEASE_SOUND_ID(iDoorAlarmSoundID)
	ENDIF
	
	IF iFakeAmmoRechargeSoundID > -1
		STOP_SOUND(iFakeAmmoRechargeSoundID)
		RELEASE_SOUND_ID(iFakeAmmoRechargeSoundID)
		iFakeAmmoRechargeSoundID = -1
	ENDIF
	
	SCRIPT_RELEASE_SOUND_ID(iRugbyCarryingBallSoundID)
	
	CLEAR_AMBIENT_ZONE_STATE( "IZ_V_FIB01_V_FIB01_JAN_COMM1", TRUE )
	CLEAR_AMBIENT_ZONE_STATE( "IZ_v_fib01_V_FIB01_jan_comm2", TRUE )
	CLEAR_AMBIENT_ZONE_STATE( "IZ_V_FIB01_V_FIB01_JAN_OFFC1", TRUE )
	CLEAR_AMBIENT_ZONE_STATE( "IZ_v_fib01_V_FIB01_jan_offc2", TRUE )
	CLEAR_AMBIENT_ZONE_STATE( "IZ_V_FIB01_V_FIB01_JAN_OFFC3", TRUE )
	CLEAR_AMBIENT_ZONE_STATE( "IZ_V_FIB01_V_FIB01_JAN_PNTRY", TRUE )
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TRADING_PLACES_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
		PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - UNLOAD_MISSION_SPECIFIC_AUDIO - Killing 30s Countdown again.")
		TRIGGER_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
	ENDIF
	
	CLEANUP_JUGGERNAUT_AUDIO_MIX_GROUP()
	
	STOP_AUDIO_SCENE_FOR_ARENA_VIP_TRANSITION(TRUE)
	
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_AWXM2018/Arena_Traps")
	
	IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfProps-1)
			//IF NOT SHOULD_THIS_SOUND_PERSIST_FOR_STRAND_MISSION(isoundid[i])
			    IF isoundid[i] <> -1
					PRINTLN("[RCC MISSION] [ALARM] STOP_SOUND(isoundid[", i, "]")
		            STOP_SOUND(isoundid[i])
		        ENDIF
			//ENDIF
		ENDFOR
		
		// turn off the casino alarm zones
		PRINTLN("[JS][ALARM] - UNLOAD_MISSION_SPECIFIC_AUDIO - Clearing Casino Alarm Zones")
		SET_AMBIENT_ZONE_STATE_PERSISTENT("AZ_H3_Casino_Alarm_Zone_01_Exterior", FALSE, TRUE)
		SET_AMBIENT_ZONE_STATE_PERSISTENT("AZ_H3_Casino_Alarm_Zone_02_Interior", FALSE, TRUE)
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_CCTVAlertSpottedSounds)
		INT iSnd = 0
		FOR iSnd = 0 TO MAX_NUM_CCTV_CAM-1
			IF iSoundAlarmCCTV[iSnd] > -1
				SCRIPT_RELEASE_SOUND_ID(iSoundAlarmCCTV[iSnd])
			ENDIF
		ENDFOR
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("SCRIPT/ALARM_BELL_01") 
		PRINTLN("[RCC MISSION][MC_SCRIPT_CLEANUP] Releasing SCRIPT/ALARM_BELL_01")
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_UseCasinoCCTVAudio)
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_VINEWOOD/VW_CASINO_FINALE") 
			PRINTLN("[RCC MISSION][MC_SCRIPT_CLEANUP] Releasing DLC_VINEWOOD/VW_CASINO_FINALE")	
		ELSE	
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("SCRIPT/ALARM_KLAXON_04") 
			PRINTLN("[RCC MISSION][MC_SCRIPT_CLEANUP] Releasing SCRIPT/ALARM_KLAXON_04")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_AmbientCasinoFloorZone)
		PRINTLN("MC_SCRIPT_CLEANUP | Turning off casino floor ambient zone")
		SET_AMBIENT_ZONE_STATE("IZ_ch_dlc_casino_heist_rm_GamingFloor_01", FALSE, TRUE)
		SET_AMBIENT_ZONE_STATE("IZ_ch_dlc_casino_heist_rm_GamingFloor_02", FALSE, TRUE)
	ENDIF	
ENDPROC

PROC CLEAR_ALL_LARGE_PICKUP_COPIES()
	INT i
	
	FOR i = 0 TO FMMC_MAX_WEAPONS - 1
		IF DOES_ENTITY_EXIST(oiLargePowerups[i])
			DELETE_OBJECT(oiLargePowerups[i])
		ENDIF
	ENDFOR
ENDPROC

PROC UNLOAD_MISSION_SPECIFIC_MODELS_AND_ENTITIES()
	PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - UNLOAD_MISSION_SPECIFIC_MODELS_AND_ENTITIES ################################")
	INT i
	SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("w_lr_rpg_rocket")))
	IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_VEHICLE_ROCKET_ASSET_REQUEST)
		REMOVE_WEAPON_ASSET(g_wtPickupRocketType)
	ENDIF
	IF IS_BIT_SET(iLocalBoolCheck5,LBOOL5_RESTRICTION_ROCKET_LOADED)
		REMOVE_WEAPON_ASSET(WEAPONTYPE_RPG)
		CLEAR_BIT(iLocalBoolCheck5,LBOOL5_RESTRICTION_ROCKET_LOADED)
	ENDIF
	
	IF NETWORK_IS_PLAYER_ACTIVE(LocalPlayer)
		SET_LOCAL_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_WITH_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_SWAP")),FALSE)
		SET_LOCAL_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_WITH_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_BMD")),FALSE)
		SET_LOCAL_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_WITH_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_SHARK")),FALSE)
		SET_LOCAL_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_WITH_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MK_THERMAL")),FALSE)
		SET_LOCAL_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_WITH_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_WEED")),FALSE)
		SET_LOCAL_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_WITH_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MK_S_TIME")),FALSE)
		SET_LOCAL_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_WITH_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_TIME")),FALSE)
		SET_LOCAL_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_WITH_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_HIDDEN")),FALSE)
		SET_LOCAL_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_WITH_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MK_RANDOM")),FALSE)
		SET_LOCAL_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_WITH_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_SWAP_P")),FALSE)
		SET_LOCAL_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_WITH_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_BMD_P")),FALSE)
		SET_LOCAL_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_WITH_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_SHARK_P")),FALSE)
		SET_LOCAL_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_WITH_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_WEED_P")),FALSE)
		SET_LOCAL_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_WITH_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_TIME_P")),FALSE)
		SET_LOCAL_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_WITH_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_HIDDEN_P")),FALSE)
		SET_LOCAL_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_WITH_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_RANDOM_P")),FALSE)
		//SET_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_OF_TYPE(LocalPlayer, PICKUP_CUSTOM_SCRIPT, TRUE)
	ENDIF
	
	INT iObj
	FOR iObj = 0 TO FMMC_MAX_NUM_OBJECTS - 1
		IF NOT IS_STRING_NULL_OR_EMPTY(GET_INTERACT_WITH_PLAYER_ANIM_DICT(iObj))
			REMOVE_ANIM_DICT(GET_INTERACT_WITH_PLAYER_ANIM_DICT(iObj))
		ENDIF
		IF NOT IS_STRING_NULL_OR_EMPTY(GET_INTERACT_WITH_PLAYER_FAILSAFE_ANIM_DICT(iObj))
			REMOVE_ANIM_DICT(GET_INTERACT_WITH_PLAYER_FAILSAFE_ANIM_DICT(iObj))
		ENDIF
		
		IF GET_INTERACT_WITH_PROP_MODEL_TO_SPAWN(iObj) != DUMMY_MODEL_FOR_SCRIPT
			SET_MODEL_AS_NO_LONGER_NEEDED(GET_INTERACT_WITH_PROP_MODEL_TO_SPAWN(iObj))
			PRINTLN("[InteractWith] Setting prop model for iObj ", iObj, " as no longer needed")
		ENDIF
		
		IF iObj < ciInteractWith__Max_Persistent_Props
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_PlayerBD_1[iLocalPart].niInteractWith_PersistentProps[iObj])
				DELETE_NET_ID(MC_PlayerBD_1[iLocalPart].niInteractWith_PersistentProps[iObj])
				PRINTLN("[InteractWith] Deleting MC_PlayerBD_1[iLocalPart].niInteractWith_PersistentProps[", iObj, "]")
			ENDIF
		ENDIF
	ENDFOR
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciLOAD_POWER_PLAY_ASSETS)
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciOPTION_VEHICLE_WEAPON_ENABLED)
	OR IS_BIT_SET(iLocalBoolCheck23, LBOOL23_TEAM_SWAP_IN_MISSION)
		RAGE_PICKUPS_CLEANUP(TRUE)
	ENDIF
	
	FOR i = 0 TO ciInteractWith__Max_Spawned_Props - 1
		DELETE_INTERACT_WITH_SPAWNED_PROP(i)
	ENDFOR
	
	CLEAR_ALL_CARGO_BOMBS()
	CLEAR_ALL_LARGE_PICKUP_COPIES()
		
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA(g_FMMC_STRUCT.iAdversaryModeType)
		FOR i = 0 TO 3
			IF NETWORK_DOES_NETWORK_ID_EXIST(oBmbBombs[i].bombObject)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(oBmbBombs[i].bombObject)
					OBJECT_INDEX objBomb = NET_TO_OBJ(oBmbBombs[i].bombObject)
					IF DOES_ENTITY_EXIST(objBomb)
						DELETE_OBJECT(objBomb)
					ENDIF
				ENDIF
			ENDIF	
		ENDFOR
	ENDIF
ENDPROC

PROC UNLOAD_IN_GAME_MISSION_SPECIFIC_SCALEFORM()
	PRINTLN("[RCC MISSION] UNLOAD_IN_GAME_MISSION_SPECIFIC_SCALEFORM")
	IF HAS_SCALEFORM_MOVIE_LOADED(sfiPPGenericMovie)
		PRINTLN("[RCC MISSION] - Cleanup - Unloading sfiPPGenericMovie - POWER_PLAY_GENERIC")
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfiPPGenericMovie)
	ENDIF
	
	RELEASE_COUNTDOWN_UI(cduiIntro)
ENDPROC

PROC UNLOAD_MISSION_SPECIFIC_SCALEFORM()
	PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - UNLOAD_MISSION_SPECIFIC_SCALEFORM ################################")
	
	UNLOAD_IN_GAME_MISSION_SPECIFIC_SCALEFORM()
	
	IF HAS_SCALEFORM_MOVIE_LOADED(sfiDownloadScreen)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfiDownloadScreen)
	ENDIF			

	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iTeamBitset2, ciBS2_STOP_GO_HUD)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(cduiIntro.uiCountdown)
	ENDIF
	IF HAS_SCALEFORM_MOVIE_LOADED(scLB_scaleformID)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(scLB_scaleformID)
		PRINTLN("CLEANUP_SOCIAL_CLUB_LEADERBOARD marking SCLB scaleform movie as no longer needed (FM_mission_controler)") 
	ENDIF
	
	IF HAS_SCALEFORM_MOVIE_LOADED(scaleformDpadMovie)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(scaleformDpadMovie)
	ENDIF
	
	// url:bugstar:2210585
	CLEANUP_ARROW_MOVIE(siRallyArrow)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciHARD_TARGET_ENABLE_MODE)
		SET_ICON_VISABILITY(NATIVE_TO_INT(PLAYER_ID()), MP_TAG_BIKER_ARROW, FALSE)
		SET_ICON_VISABILITY(NATIVE_TO_INT(PLAYER_ID()), MP_TAG_ARROW, FALSE)
	ENDIF
ENDPROC

PROC UNLOAD_MISSION_SPECIFIC_INTERIORS_IPLS()
	PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - UNLOAD_MISSION_SPECIFIC_INTERIORS_IPLS ################################")
	IF IS_NEW_LOAD_SCENE_ACTIVE()
		NEW_LOAD_SCENE_STOP()
	ENDIF
	
	IF IPL_GROUP_SWAP_IS_ACTIVE()
		IPL_GROUP_SWAP_CANCEL()
	ENDIF
	
	CLEANUP_INTERIOR_COLLISION_FIXES(sInteriorFixStruct)
	
	CLEANUP_CASINO_APARTMENT_SETUP(interiorCasinoApartmentIndex)
	
	IF NOT (IS_CORONA_INITIALISING_A_QUICK_RESTART()
	AND NOT IS_THIS_A_ROUNDS_RESTART())
	OR IS_FORCE_ABANDON_CURRENT_MP_MISSION_SET()
		PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - Removing IPLs as not a quick restart")
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_FORT_ZANCUDO_GATES)
			REQUEST_IPL("cs3_07_mpgates")
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - adding ford zancudo gates ipl")
		ELSE
			REMOVE_IPL("cs3_07_mpgates")
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - removing ford zancudo gates ipl")
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_FARMHOUSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_FARMHOUSE, TRUE)
			REMOVE_IPL("farmint")
			REQUEST_IPL("farmint_cap")
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_ABATTOIR)	
			SET_INTERIOR_CAPPED(INTERIOR_V_ABATTOIR, TRUE)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_CORONER)	
			CLEAN_UP_INTERIOR(INTERIOR_V_CORONER)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_RECYCLEPLANT)				
			SET_INTERIOR_CAPPED(INTERIOR_V_RECYCLE, TRUE)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_UNION_DEPOSITORY)	
			CLEAN_UP_INTERIOR(INTERIOR_DT1_03_CARPARK)
		ENDIF	
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_BUNKER)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_BUNKER)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)

			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_STANDARD_SET)
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Standard_Bunker_Set")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Standard_Bunker_Set")
					PRINTLN("DEACTIVATE_INTERIOR_ENTITY_SET - \"Standard_Bunker_Set\"")
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_UPGRADE_SET)
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Upgrade_Bunker_Set")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Upgrade_Bunker_Set")
					PRINTLN("DEACTIVATE_INTERIOR_ENTITY_SET - \"Upgrade_Bunker_Set\"")
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_STYLE_A)
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Bunker_Style_A")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Bunker_Style_A")
					PRINTLN("DEACTIVATE_INTERIOR_ENTITY_SET - \"Bunker_Style_A\"")
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_STYLE_B)
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Bunker_Style_B")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Bunker_Style_B")
					PRINTLN("DEACTIVATE_INTERIOR_ENTITY_SET - \"Bunker_Style_B\"")
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_STYLE_C)
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Bunker_Style_C")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Bunker_Style_C")
					PRINTLN("DEACTIVATE_INTERIOR_ENTITY_SET - \"Bunker_Style_C\"")
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_OFFICE_UPGRADE_SET)
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Office_Upgrade_Set")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Office_Upgrade_Set")
					PRINTLN("DEACTIVATE_INTERIOR_ENTITY_SET - \"Office_Upgrade_Set\"")
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_OFFICE_BLOCKER_SET)
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Office_Blocker_Set")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Office_Blocker_Set")
					PRINTLN("DEACTIVATE_INTERIOR_ENTITY_SET - \"Office_Blocker_Set\"")
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_GUN_RANGE_BLOCKER_SET)
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Gun_Range_Blocker_Set")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Gun_Range_Blocker_Set")
					PRINTLN("DEACTIVATE_INTERIOR_ENTITY_SET - \"Gun_Range_Blocker_Set\"")
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_GUN_LOCKER_UPGRADE)
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Gun_locker_upgrade")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Gun_locker_upgrade")
					PRINTLN("DEACTIVATE_INTERIOR_ENTITY_SET - \"Gun_locker_upgrade\"")
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_STANDARD_SECURITY_SET)
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Standard_Security_Set")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Standard_Security_Set")
					PRINTLN("DEACTIVATE_INTERIOR_ENTITY_SET - \"Standard_Security_Set\"")
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_SECURITY_UPGRADE)
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Security_upgrade")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Security_upgrade")
					PRINTLN("DEACTIVATE_INTERIOR_ENTITY_SET - \"Security_upgrade\"")
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_GUN_WALL_BLOCKER)
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Gun_wall_blocker")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Gun_wall_blocker")
					PRINTLN("DEACTIVATE_INTERIOR_ENTITY_SET - \"Gun_wall_blocker\"")
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciBUNKER_STANDARD_SET_MORE)
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Standard_Bunker_Set_More")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Standard_Bunker_Set_More")
					PRINTLN("DEACTIVATE_INTERIOR_ENTITY_SET - \"Standard_Bunker_Set_More\"")
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciBUNKER_UPGRADE_SET_MORE)
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "Upgrade_Bunker_Set_More")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "Upgrade_Bunker_Set_More")
					PRINTLN("DEACTIVATE_INTERIOR_ENTITY_SET - \"Upgrade_Bunker_Set_More\"")
				ENDIF
			ENDIF
			
			SET_INTERIOR_DISABLED(INTERIOR_V_BUNKER, TRUE)
			UNPIN_INTERIOR(iInteriorIndex)
			
			//Audio Scene
			IF IS_AUDIO_SCENE_ACTIVE("DLC_GR_Bunker_Interior")
				STOP_AUDIO_SCENE("DLC_GR_Bunker_Interior")
				PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - STOP_AUDIO_SCENE(\"DLC_GR_Bunker_Interior\"")
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SUB)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_SUB)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_SUB, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_IAA)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_IAA)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_IAA, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_IAA_COVER_POINTS)
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "IAA_CoverPoints_01")
					DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "IAA_CoverPoints_01")
					PRINTLN("ACTIVATE_INTERIOR_ENTITY_SET - IAA_CoverPoints_01")
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_LIFEINVADER)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_LIFEINVADER)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_LIFEINVADER, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
			
			REMOVE_IPL("facelobby")
			REQUEST_IPL("facelobbyfake")
			
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_FOUNDRY)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_FOUNDRY)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_CAPPED(INTERIOR_V_FOUNDRY, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_01)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_SILO_1)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_SILO_1, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_02)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_SILO_2)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_SILO_2, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_03)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_SILO_3)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_SILO_3, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SERVER_FARM)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_SERVER_FARM)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_SERVER_FARM, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_TUNNEL)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_SILO_TUNNEL)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_SILO_TUNNEL, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_LOOP)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_SILO_LOOP)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_SILO_LOOP, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_ENTRANCE)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_SILO_ENTRANCE)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_SILO_ENTRANCE, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_BASE)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_SILO_BASE)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_SILO_BASE, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_OSPREY)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_OSPREY)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_OSPREY, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_HANGAR)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_HANGAR)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_HANGAR, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_IMPORT_WAREHOUSE)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_IMPORT_WAREHOUSE)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_IMPORT_WAREHOUSE, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_WAREHOUSE_UNDRGRND_FACILITY)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_WAREHOUSE_UNDRGRND_FACILITY)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_WAREHOUSE_UNDRGRND_FACILITY, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SPECIAL_CARGO_WAREHOUSE_S)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_SPECIAL_CARGO_WAREHOUSE_S)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_SPECIAL_CARGO_WAREHOUSE_S, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SPECIAL_CARGO_WAREHOUSE_M)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_SPECIAL_CARGO_WAREHOUSE_M)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_SPECIAL_CARGO_WAREHOUSE_M, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SPECIAL_CARGO_WAREHOUSE_L)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_SPECIAL_CARGO_WAREHOUSE_L)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_SPECIAL_CARGO_WAREHOUSE_L, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_STILT_APARTMENT)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_STILT_APARTMENT)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_STILT_APARTMENT, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_HIGH_END_APARTMENT)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_HIGH_END_APARTMENT)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_HIGH_END_APARTMENT, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_MEDIUM_END_APARTMENT)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_MEDIUM_END_APARTMENT)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_MEDIUM_END_APARTMENT, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_LOW_END_APARTMENT)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_LOW_END_APARTMENT)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_LOW_END_APARTMENT, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_FIB_OFFICE)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_FIB_OFFICE)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_FIB_OFFICE, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_FIB_OFFICE_2)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_FIB_OFFICE_2)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_FIB_OFFICE_2, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR_FIB_LOBBY)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_FIB_LOBBY)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_FIB_LOBBY, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR_HIGH_END_GARAGE)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_HIGH_END_GARAGE)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_HIGH_END_GARAGE, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR_MEDIUM_GARAGE)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_GARAGEM)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_GARAGEM, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR_SMALL_GARAGE)
			INTERIOR_INSTANCE_INDEX iInteriorIndex		
			INTERIOR_DATA_STRUCT structInteriorData	
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_GARAGES)			
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_GARAGES, TRUE)
			IF IS_VALID_INTERIOR(iInteriorIndex)
				UNPIN_INTERIOR(iInteriorIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_TUNNEL_ENTRANCE)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_ENTRY)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_TUNNEL_STRAIGHT)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_STRAIGHT_0)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_STRAIGHT_1)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_STRAIGHT_2)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_TUNNEL_FLAT_SLOPE)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_SLOPE_FLAT_0)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_SLOPE_FLAT_1)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_SLOPE_FLAT_2)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_TUNNEL_FLAT_SLOPE_2)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_FLAT_SLOPE_0)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_FLAT_SLOPE_1)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_FLAT_SLOPE_2)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_TUNNEL_30D_R)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_30D_RIGHT_0)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_30D_RIGHT_1)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_30D_RIGHT_2)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_30D_RIGHT_3)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_30D_RIGHT_4)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_TUNNEL_30D_L)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_30D_LEFT_0)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_30D_LEFT_1)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_30D_LEFT_2)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_30D_LEFT_3)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_30D_LEFT_4)
			CLEAN_UP_INTERIOR(INTERIOR_V_TUNNEL_30D_LEFT_5)
		ENDIF
		

		ARENA_CLEANUP(DEFAULT, DEFAULT, TRUE)
		CLEANUP_ARENA_TRAPS(MC_serverBD_2.sTrapInfo_Host, sTrapInfo_Local)
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_WEED_FARM)
			CLEAN_UP_INTERIOR(INTERIOR_V_WEED_FARM)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_COCAINE_FACTORY)
			CLEAN_UP_INTERIOR(INTERIOR_V_COCAINE_FACTORY)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_METH_FACTORY)
			CLEAN_UP_INTERIOR(INTERIOR_V_METH_FACTORY)
		ENDIF
		
		IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - CLEANUP_CASINO_INTERIORS")
			g_TransitionSessionNonResetVars.iFMMCiInteriorBS2 = 0
			CLEANUP_STRAND_INTERIORS()
			CLEANUP_CASINO_HEIST_MANTRAP_DOORS_FROM_TRANSITION()
			
			// Casino Vault Door cleanup // // // // // // // // // // // // // // 
			CLEANUP_CASINO_VAULT_DOOR(interiorCasinoVaultIndex)
			REMOVE_ANIM_DICT("anim_heist@hs3f@ig8_vault_explosive_react@left@male@")
			REMOVE_ANIM_DICT("anim_heist@hs3f@ig8_vault_explosive_react@right@male@")
			IF DOES_ENTITY_EXIST(oiSpawnedVaultDoor_Animated)
				DELETE_OBJECT(oiSpawnedVaultDoor_Animated)
				PRINTLN("[CasinoVaultDoor] Deleting oiSpawnedVaultDoor_Animated")
			ENDIF
			IF DOES_ENTITY_EXIST(oiSpawnedVaultDoor_StaticCollision)
				DELETE_OBJECT(oiSpawnedVaultDoor_StaticCollision)
				PRINTLN("[CasinoVaultDoor] Deleting oiSpawnedVaultDoor_StaticCollision")
			ENDIF
			IF DOES_ENTITY_EXIST(oiVaultDoorDebrisObj)
				DELETE_OBJECT(oiVaultDoorDebrisObj)
				PRINTLN("[CasinoVaultDoor] Deleting oiVaultDoorDebrisObj")
			ENDIF
			// // // // // // // // // // // // // // // // // // // // // // // //
		ELSE
			g_TransitionSessionNonResetVars.iFMMCiInteriorBS2 = g_FMMC_STRUCT.iInteriorBS2
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - iFMMCiInteriorBS2 = ", g_TransitionSessionNonResetVars.iFMMCiInteriorBS2)
		ENDIF
				
		g_bMissionIPLsRequested = FALSE
		
	#IF IS_DEBUG_BUILD
	ELSE
		IF IS_CORONA_INITIALISING_A_QUICK_RESTART()
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - Keeping IPLs as this is a quick restart")
		ENDIF
		IF IS_THIS_A_ROUNDS_RESTART()
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - Keeping IPLs as we're going into another round")
		ENDIF
	#ENDIF
	ENDIF
	
	REQUEST_IPL("cs1_02_cf_offmission")
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		REMOVE_IPL("cs1_02_cf_onmission1")
		REMOVE_IPL("cs1_02_cf_onmission2")
		REMOVE_IPL("cs1_02_cf_onmission3")
		REMOVE_IPL("cs1_02_cf_onmission4")
	ENDIF
	
	IF NOT IS_IPL_ACTIVE("chemgrill_grp1")
		REQUEST_IPL("chemgrill_grp1")
	ENDIF
	
	CLEAR_WATER_CALMING_QUADS()
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		IF IS_BIT_SET(iLocalBoolCheck19, LBOOL19_YACHT_AQUARIS_ASSETS_REQUESTED)
			YACHT_DATA YachtData    
			YachtData.iLocation = 34            // Paleto Bay
			YachtData.Appearance.iOption = 2    // The Aquaris
			YachtData.Appearance.iTint = 12     // Vintage
			YachtData.Appearance.iLighting = 5 // Blue Vivacious
			YachtData.Appearance.iRailing = 1   // Gold Fittings
			YachtData.Appearance.iFlag = -1     // no flag 
			
			DELETE_AND_CLEANUP_ASSETS_FOR_MISSION_CREATOR_YACHT(YachtData)
			
			CLEAR_BIT(iLocalBoolCheck19, LBOOL19_YACHT_AQUARIS_ASSETS_REQUESTED)
			CLEAR_BIT(iLocalBoolCheck19, LBOOL19_YACHT_AQUARIS_ASSETS_LOADED)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck11, LBOOL11_BIG_MAP_WAS_ZOOMED_IN_FOR_CARRIER)
		SET_RADAR_ZOOM_PRECISE(0)
		CLEAR_BIT(iLocalBoolCheck11, LBOOL11_BIG_MAP_WAS_ZOOMED_IN_FOR_CARRIER)
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck33, LBOOL33_ZOOMED_IN_FOR_CASINO_ROOF)
		SET_RADAR_ZOOM_PRECISE(0)
		CLEAR_BIT(iLocalBoolCheck33, LBOOL33_ZOOMED_IN_FOR_CASINO_ROOF)
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck33, LBOOL33_ZOOMED_IN_FOR_CASINO_HEIST)
		SET_RADAR_ZOOM_PRECISE(0)
		CLEAR_BIT(iLocalBoolCheck33, LBOOL33_ZOOMED_IN_FOR_CASINO_HEIST)
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("DLC_HEISTS_BIOLAB_STEAL_EMP_FLYOVER_SCENE")
		STOP_AUDIO_SCENE("DLC_HEISTS_BIOLAB_STEAL_EMP_FLYOVER_SCENE")
	ENDIF
	
	CLEAR_ALL_TCMODIFIER_OVERRIDES("lab_none_exit")
	CLEAR_ALL_TCMODIFIER_OVERRIDES("lab_none_dark")
	CLEAR_ALL_TCMODIFIER_OVERRIDES("morgue_dark")
	CLEAR_TIMECYCLE_MODIFIER()
	IF DOES_ENTITY_EXIST(vehPrizedVehicle)
		DELETE_VEHICLE(vehPrizedVehicle)
	ENDIF
	
	CLEANUP_MISSION_FACTORIES(sMissionFactories)
	DELETE_AND_CLEANUP_ASSETS_FOR_MISSION_CREATOR_YACHT(sMissionYachtData)	
		
	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_CASINO_AIR_CONDITIONING_UNIT)
		PRINTLN("[RCC MISSION] REQUEST_IPL - ch_dlc_casino_aircon_broken")
		IF NOT IS_IPL_ACTIVE("ch_dlc_casino_aircon_broken")
			REQUEST_IPL("ch_dlc_casino_aircon_broken")	
		ENDIF
	ENDIF
ENDPROC

PROC UNLOAD_MISSION_SPECIFIC_ANIMS()
	PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - UNLOAD_MISSION_SPECIFIC_ANIMS ################################")
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TRANSFORM_ASSETS)
		REMOVE_ANIM_SET("ANIM_GROUP_MOVE_BALLISTIC")    
	    REMOVE_ANIM_SET("MOVE_STRAFE_BALLISTIC")
	    REMOVE_CLIP_SET("move_ballistic_2h")
		IF IS_BIT_SET(iLocalBoolCheck20, LBOOL20_JUGGERNAUT_SUIT_MOVEMENT_SET)
		OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_TRADING_PLACES_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
			RESET_PED_MOVEMENT_CLIPSET(LocalPlayerPed)
			RESET_PED_STRAFE_CLIPSET(LocalPlayerPed)
			SET_WEAPON_ANIMATION_OVERRIDE(LocalPlayerPed, HASH("Default"))
			CLEAR_BIT(iLocalBoolCheck20, LBOOL20_JUGGERNAUT_SUIT_MOVEMENT_SET)
		ENDIF
		IF NOT IS_PED_INJURED(localPlayerPed)
			SET_PLAYER_CAN_USE_COVER(LocalPlayer, TRUE)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck8, LBOOL8_TREVOR_COVER_CLIPSET_LOADED)
	OR IS_BIT_SET(iLocalBoolCheck8, LBOOL8_TREVOR_COVER_CLIPSET_LOADING)
		REMOVE_CLIP_SET("trevor_heist_cover_2h")
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck11, LBOOL11_TREVOR_VAN_IDLE_CLIPSET_LOADED)
		REMOVE_CLIP_SET("anim@heists@narcotics@finale@ig_5_trevor_in_truck")
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck12, LBOOL12_TREVOR_COVER_ANIMS_LOADED)
		REMOVE_ANIM_DICT("anim@heists@narcotics@finale@trevor_cover_idles")
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck9, LBOOL9_ACTION_MODE_OVERRIDDEN_TO_STOP_ARMOUR_CLIPPING)
	AND NOT IS_PED_FEMALE(PLAYER_PED_ID())
		PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - Was wearing armour in heist mission, setting movement mode override back to NULL")
		SET_MOVEMENT_MODE_OVERRIDE(PLAYER_PED_ID())
		CLEAR_BIT(iLocalBoolCheck9, LBOOL9_ACTION_MODE_OVERRIDDEN_TO_STOP_ARMOUR_CLIPPING)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_PhoneEMPAvailable_CasinoPrep)
		REMOVE_ANIM_DICT("AMB@CODE_HUMAN_POLICE_INVESTIGATE@idle_a")
		REMOVE_ANIM_DICT("AMB@CODE_HUMAN_POLICE_INVESTIGATE@idle_b")
		REMOVE_ANIM_DICT("anim@weapons@flashlight@stealth")
	ENDIF
	
	IF IS_BIT_SET(sIWInfo.iInteractWith_Bitset, ciInteractWith_Bitset__HidPlayerWeaponForEndCutscene)
		SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed, TRUE, FALSE)
		PRINTLN("[InteractWith] Setting player's weapon to be visible in cleanup due to ciInteractWith_Bitset__HidPlayerWeaponForEndCutscene")
	ENDIF
ENDPROC
PROC RESET_VEHICLE_RELATED_CONFIGS()	
	PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - RESET_VEHICLE_RELATED_CONFIGS ################################")
	ENTITY_INDEX vehTemp
	//Cleanup Don't Fall Off Bike
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciBIKES_DONT_WIPE_OUT) 
	AND NOT IS_PED_INJURED(LocalPlayerPed)
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, TRUE)
			IF IS_PED_ON_ANY_BIKE(LocalPlayerPed)
				SET_BIKE_EASY_TO_LAND(GET_VEHICLE_PED_IS_IN(LocalPlayerPed), FALSE)
				PRINTLN("MC - CLEARING EASY TO LAND")
			ENDIF
		ENDIF
	ENDIF
	
	SET_VEHICLE_SLIPSTREAMING_SHOULD_TIME_OUT(TRUE)
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableAutoEquipHelmetsInBikes, GET_MP_INT_CHARACTER_STAT(MP_STAT_AUTO_EQUIP_HELMET)=0)
			CPRINTLN(DEBUG_AMBIENT, "[RCC MC_SCRIPT_CLEANUP] default PCF_DisableAutoEquipHelmetsInBikes=",GET_STRING_FROM_BOOL(GET_MP_INT_CHARACTER_STAT(MP_STAT_AUTO_EQUIP_HELMET)=0))
		
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableAutoEquipHelmetsInAicraft, GET_MP_INT_CHARACTER_STAT(MP_STAT_AUTO_EQUIP_HELMET)=0)
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iTeamBitset3, ciBS3_TEAM_PLAYER_PREVENT_FALLS_OUT_WHEN_KILLED)
				IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_SAVING_FALLOUT_FLAG)
					SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_FallsOutOfVehicleWhenKilled, TRUE)
					CLEAR_BIT(iLocalBoolCheck29, LBOOL29_SAVING_FALLOUT_FLAG)
					SET_BIT(iLocalBoolCheck29, LBOOL29_SAVING_FALLOUT_FLAG)
				ELSE
					SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_FallsOutOfVehicleWhenKilled, FALSE)
					CLEAR_BIT(iLocalBoolCheck29, LBOOL29_SAVING_FALLOUT_FLAG)
				ENDIF
			ENDIF
						
			//g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset2,  ciBS2_BLOCK_WEAPON_SWAP_ON_PICKUP
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_BlockAutoSwapOnWeaponPickups, TRUE)
		ENDIF
	ENDIF
	
	CLEAR_BIT(iLocalBoolCheck19, LBOOL19_LOADED_TOUR_DE_FORCE_VFX_SLIPSTREAM_OVERRIDE)
	
	IF DOES_ENTITY_EXIST(LocalPlayerPed)
		IF NOT IS_PED_INJURED(LocalPlayerPed)
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_GetOutUndriveableVehicle, TRUE)
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_GetOutBurningVehicle, TRUE)
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_RunFromFiresAndExplosions, TRUE)
			SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisableAutoForceOutWhenBlowingUpCar, FALSE)
	
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_PhoneDisableTextingAnimations, TRUE)
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_PhoneDisableTalkingAnimations, TRUE)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_PlayersDontDragMeOutOfCar, FALSE)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciMAKE_VEH_SUITABLE_FOR_SUMO)
		INT iVehLoop
		FOR iVehLoop = 0 TO (MAX_NUM_MC_PLAYERS-1)
			IF NETWORK_DOES_NETWORK_ID_EXIST(niBlendVeh[iVehLoop])
				IF LocalPlayer != INT_TO_PLAYERINDEX(iVehLoop)
					IF niBlendVeh[iVehLoop] != NULL
						IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(niBlendVeh[iVehLoop])
							vehTemp = NETWORK_GET_ENTITY_FROM_NETWORK_ID(niBlendVeh[iVehLoop])
							IF NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(vehTemp)
								IF NETWORK_IS_NETWORK_ID_REMOTELY_CONTROLLED(niBlendVeh[iVehLoop])
									NETWORK_ENABLE_EXTRA_VEHICLE_ORIENTATION_BLEND_CHECKS(niBlendVeh[iVehLoop], FALSE)
									PRINTLN("[LH][BLEND_VEH] NETWORK_ENABLE_EXTRA_VEHICLE_ORIENTATION_BLEND_CHECKS disabled for player ",iVehLoop)
								ELSE
									PRINTLN("[KH][BLEND_VEH] NETWORK_ENABLE_EXTRA_VEHICLE_ORIENTATION_BLEND_CHECKS NOT disabled for player as the vehicle is not a clone on local machine ",iVehLoop)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
		
	IF NOT IS_PED_DEAD_OR_DYING(PLAYER_PED_ID())
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND DOES_ENTITY_EXIST(localPlayerPed)
		SET_PED_CONFIG_FLAG(localPlayerPed, PCF_GetOutUndriveableVehicle, TRUE)
		SET_PED_CONFIG_FLAG(localPlayerPed, PCF_GetOutBurningVehicle, TRUE)
	ENDIF
	
	//3873051
	IF (g_FMMC_STRUCT.iTeamVehicleModCountermeasure_AmmoPerLife[MC_playerBD[iLocalPart].iTeam] > 0) OR (g_FMMC_STRUCT.iTeamVehicleModCountermeasure_Cooldown[MC_playerBD[iLocalPart].iTeam] > 0)
		RESET_COUNTERMEASURE_OVERRIDES()
	ENDIF	
			
	IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_VEHICLE_WEAPONS_HOMING_DISABLED)
		SET_PLAYER_HOMING_DISABLED_FOR_ALL_VEHICLE_WEAPONS(localPlayer, FALSE)
		CLEAR_BIT(iLocalBoolCheck25, LBOOL25_VEHICLE_WEAPONS_HOMING_DISABLED)
		
		PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP -  Clearing Blocked Homing Missiles.")
	ENDIF	
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iTeamBitset3, ciBS3_PREFER_FRONT_SEATS)
		PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - ciBS3_PREFER_FRONT_SEATS is set. Resetting: PCF_PlayerPreferFrontSeatMP")
		
		IF DOES_ENTITY_EXIST(LocalPlayerPed)
			IF  NOT IS_PED_INJURED(LocalPlayerPed)
				SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_PlayerPreferFrontSeatMP, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck21, LBOOL21_VEHICLE_SWAP_RETRACTABLE_WHEEL_BUTTON)
		SET_VEHICLE_USE_BOOST_BUTTON_FOR_WHEEL_RETRACT(FALSE)
	ENDIF	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_VEHICLE_TARGETTING_RETICULE)
		USE_VEHICLE_TARGETING_RETICULE(FALSE)
	ENDIF
	CLEAR_VALID_VEHICLE_HIT_HASHES()
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_SUMO_ZONE(g_FMMC_STRUCT.iAdversaryModeType)
		SET_DISABLE_COLLISIONS_BETWEEN_CARS_AND_CAR_PARACHUTE(FALSE)
	ENDIF
	
	CLEANUP_ALTERED_RADIO_FOR_MC()
ENDPROC

PROC RESET_PLAYER_RELATED_CONFIGS()
	PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - RESET_PLAYER_RELATED_CONFIGS ################################")
	
	IF NOT IS_PED_INJURED(LocalPlayerPed)
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iTeamBitset3, ciBS3_TEAM_DISABLE_HELMET_ARMOUR_FROM_OUTFIT)
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - ciBS3_TEAM_DISABLE_HELMET_ARMOUR_FROM_OUTFIT is set. Resetting: PCF_DisableHelmetArmor")
			
			IF DOES_ENTITY_EXIST(LocalPlayerPed)
				SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DisableHelmetArmor, FALSE)
			ENDIF
		ENDIF		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciALLOW_SPRINTING_IN_INTERIORS)
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_IgnoreInteriorCheckForSprinting, FALSE)")
			IF DOES_ENTITY_EXIST(localPlayerPed)
				SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_IgnoreInteriorCheckForSprinting, FALSE)
			ENDIF		
		ENDIF
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(tdTeamSwapPassiveModeTimer)
	AND g_MultiplayerSettings.g_bTempPassiveModeSetting
		PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - g_bTempPassiveModeSetting was set. Cleaning up passive mode.")
		CLEANUP_TEMP_PASSIVE_MODE()
		TURN_OFF_PASSIVE_MODE_OPTION(FALSE)		
		SET_ENTITY_ALPHA(LocalPlayerPed, 255, FALSE)
		IF NOT IS_PED_INJURED(LocalPlayerPed)
			SET_PED_RESET_FLAG(LocalPlayerPed, PRF_BlockWeaponFire, FALSE)
		ENDIF
	ENDIF
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_POWER_PLAY(g_FMMC_STRUCT.iAdversaryModeType)
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_BVS1(g_FMMC_STRUCT.iRootContentIDHash)
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TRANSFORM_ASSETS)
	OR IS_BIT_SET(iLocalBoolCheck28, LBOOL28_TPR_APPEARANCE_UPDATED)
	OR IS_BIT_SET(iLocalBoolCheck14, LBOOL14_IN_BEAST_MODE)
		PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - cleanup beast.")
		CLEAN_UP_BEAST_MODE(TRUE, DEFAULT, TRUE)
	ENDIF	
	
	IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
	AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TRADING_PLACES_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
	AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TRANSFORM_ASSETS)
		PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - CALLING APPLY_OUTFIT_TO_LOCAL_PLAYER. GET_LOCAL_DEFAULT_OUTFIT")
		APPLY_OUTFIT_TO_LOCAL_PLAYER(INT_TO_ENUM(MP_OUTFIT_ENUM, GET_LOCAL_DEFAULT_OUTFIT(MC_playerBD[iLocalPart].iTeam)))
	ENDIF
	
	IF NOT IS_PED_INJURED(LocalPlayerPed)
		IF HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_MINIGUN)
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - SET_PED_INFINITE_AMMO = false.")
			SET_PED_INFINITE_AMMO(LocalPlayerPed, FALSE, WEAPONTYPE_MINIGUN)
		ENDIF
		
		RESET_PLAYER_HEALTH()
		
		IF DOES_ENTITY_EXIST(GET_PLAYER_PED(PLAYER_ID()))
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - resetting SET_PLAYER_HEALTH_RECHARGE_MULTIPLIER")
			SET_PLAYER_HEALTH_RECHARGE_MULTIPLIER(PLAYER_ID(), 1.0)
		ENDIF
		
		IF DOES_ENTITY_EXIST(GET_PLAYER_PED(PLAYER_ID()))
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - resetting SET_PLAYER_WEAPON_DEFENSE_MODIFIER")
			SET_PLAYER_WEAPON_DEFENSE_MODIFIER( PLAYER_ID(), 1.0 ) // Reset any defense modifiers
		ENDIF
		
		IF( bWasKnockOffSettingChangedForTrash )
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - resetting KNOCKOFFVEHICLE_DEFAULT")
			SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE( LocalPlayerPed, KNOCKOFFVEHICLE_DEFAULT )
		ENDIF
		
		CLEAR_PED_PROP(PLAYER_PED_ID(),ANCHOR_RIGHT_HAND)
		CLEAR_PED_PROP(PLAYER_PED_ID(),ANCHOR_LEFT_HAND)
		
		PRINTLN("[RCC MISSION] - Cleanup - Set PCF_PreventAutoShuffleToDriversSeat, false")
		SET_PED_CONFIG_FLAG(LocalPlayerPed,PCF_PreventAutoShuffleToDriversSeat,FALSE)
		
		PRINTLN("[RCC MISSION] - Cleanup - Set PCF_DisableLadderClimbing, false")
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DisableLadderClimbing, FALSE)
		
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_GetOutUndriveableVehicle, TRUE)
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_FallsOutOfVehicleWhenKilled, TRUE)
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_GetOutBurningVehicle, TRUE)
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_RunFromFiresAndExplosions, TRUE)
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_BlockDispatchedHelicoptersFromLanding, FALSE)
		
		SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed, TRUE)
		
		SET_FLASH_LIGHT_ACTIVE_HISTORY(LocalPlayerPed, FALSE)
		
	ENDIF
	
	IF LocalPlayerPed != NULL
		IF NETWORK_GET_ENTITY_IS_NETWORKED(LocalPlayerPed)
			IF NETWORK_IS_PLAYER_CONCEALED(LocalPlayer)
			AND NOT IS_PLAYER_SCTV(LocalPlayer)
				PRINTLN("[RCC MISSION] - Cleanup - Set NETWORK_CONCEAL_PLAYER, false")
				NETWORK_CONCEAL_PLAYER(LocalPlayer, FALSE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC RESET_MISSION_SPECIFIC_GLOBALS(BOOL bFinalCleanup, BOOL bStrand)
	PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - RESET_MISSION_SPECIFIC_GLOBALS ################################")
	UNUSED_PARAMETER(bFinalCleanup)
	INT i
		
	IF IS_FORCE_ABANDON_CURRENT_MP_MISSION_SET()
	AND CONTENT_IS_USING_ARENA()
	AND NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
		HANDLE_END_OF_ARENA_EVENT_PLAYER_CAREER_REWARDS(0, (MC_serverBD.iNumberOfTeams > 1), TRUE, DEFAULT, DEFAULT, DEFAULT, MC_serverBD.iNumberOfTeams)
	ENDIF
	
	#IF IS_DEBUG_BUILD 
	g_iDialogueProgressLastPlayed = -1
	#ENDIF
	
	g_bMissionInstantRespawn = FALSE
	g_bAppliedMissionOutfit = FALSE
	g_bBlockInfiniteRangeCheck = FALSE
	g_bMissionPlacedOrbitalCannon = FALSE
	g_iMissionObjectHasSpawnedBitset = 0
	g_bMissionHasPlacedAvengerVehicle = FALSE
	g_bLeavingVehInteriorForMCEnd = FALSE
	g_bMissionHideRespawnBar = FALSE 
	g_bMissionHideRespawnBarForRestart = FALSE 
	g_bBlockSeatShufflePlacedVeh = FALSE
	g_bPerformingBuildingSwap = FALSE
	g_bInMissionControllerCutscene = FALSE
	g_SpawnData.VehicleWeapon = WEAPONTYPE_INVALID
	g_mnOfLastVehicleWeWasIn = DUMMY_MODEL_FOR_SCRIPT
	g_iFMMCCurrentStaticCam = -1	
	g_LastPlayerAliveTinyRacers = FALSE
	g_bMCForceThroughRestartState = FALSE	
	g_iDawnRaidPickupCarrierPart = -1
	bUsingBirdsEyeCam = FALSE
	g_bMissionSeatSwapping = FALSE
	g_bAbortPropertyMenus = FALSE
	MPGlobalsAmbience.bEnablePassengerBombing = FALSE
	g_SpawnData.bCargobobUseBehaviour = FALSE
	g_SpawnData.bRememberVehicleWeaponOnSpawn = FALSE	
	iTotalObjectsToDefend = -1	
	g_b_UseBottomRightTitleColour = FALSE
	iStoreLastTeam0Score = 0
	iStoreLastTeam1Score = 0
	bShownScoreShardInSuddenDeath = FALSE
	bsHasObjExploded = - 1
	iExplodeCountdownSound = - 1
	iExplodeLocalTimer = - 1
	iStoredNoWeaponZone = - 1
	iSniperBallObj = - 1
	oiSniperBallPackage = NULL	
	iManagePowerPlayUI =  PPH_REQUEST_ASSETS
	DISABLE_VEHICLE_MINES(FALSE)
	bFMMCHealthBoosted = FALSE	
	iHackStage = ciSECUROSERV_HACK_STAGE_NO_SIGNAL
	iHackPercentage = 0
	tl15HackComplete = ""
	iTrackifyProgress = 0
	iBeastTrackifyProgress = 0
	g_VehicleRocketInfo.bIsCollected = FALSE
	g_VehicleBoostInfo.bCollected = FALSE
	g_VehicleSpikeInfo.bIsCollected = FALSE
	g_VehicleGunInfo.bCollected = FALSE
	g_vehStandbyVehicle = NULL
	//clean up for - 2646657
	g_bNoDeathTickers = FALSE
	g_bTeamColoursOverridden = FALSE
	g_ForceRedWastedKillerShard = FALSE
	
	SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY(FALSE)
		
	// Cleanup assists
	g_MissionControllerserverBD_LB.sleaderboard[iPartToUse].iAssists += g_iMyAssists
	g_iMyAssists = 0
	
	//For altering vehicle objective text based on the player who is on the vehicles colouring.
	iVehCapTeam =  -1	
	bsRepositionCheckpoint = 0
	bsCheckPointCreated = 0	
	bsPlayersWithBombActive = 0
	
	//Clean up passed through globals
	MC_PlayerBD_VARS_STRUCT sEmptyPlayerBDVars
	MC_serverBD_VARS_STRUCT sEmptyServerBDVars
	MC_LocalVariables_VARS_STRUCT sEmptyLocalVars
	gMC_PlayerBD_VARS = sEmptyPlayerBDVars
	gBG_MC_serverBD_VARS = sEmptyServerBDVars
	gMC_LocalVariables_VARS = sEmptyLocalVars
	
	//Target Location of the Dawn Raid Pickup 
	vTargetObj = <<0,0,0>>
	
	#IF IS_DEBUG_BUILD
	g_debugDisplayState = DEBUG_DISP_OFF
	#ENDIF
		
	IF bFinalCleanup AND CONTENT_IS_USING_ARENA()
	OR CONTENT_IS_USING_ARENA()
		IF g_mnMyRaceModel != DUMMY_MODEL_FOR_SCRIPT
			g_mnMyRaceModel = DUMMY_MODEL_FOR_SCRIPT
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - Clearing g_mnMyRaceModel")
		ENDIF
	ENDIF	
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciALLOW_MISSION_AIRSTRIKES)
		g_vAirstrikeCoords = <<0,0,0>>
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_WVM_OPPRESSOR_2(g_FMMC_STRUCT.iRootContentIDHash)
		IF g_sMagnateOutfitStruct.iBossStyle != ENUM_TO_INT(GB_BOSS_STYLE_NONE)
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - Setting bPerformGeneralRefresh")
			g_sMagnateOutfitStruct.bPerformGeneralRefresh = TRUE
		ENDIF
	ENDIF
	
	FOR i = 0 TO FMMC_MAX_NUM_OBJECTS - 1
		gBG_MC_serverBD_VARS.oiObjects[i] = NULL
	ENDFOR
	FOR i = 0 TO FMMC_MAX_VEHICLES - 1
		gBG_MC_serverBD_VARS.viVehicles[i] = NULL
	ENDFOR
	FOR i = 0 TO FMMC_MAX_PEDS - 1
		gBG_MC_serverBD_VARS.piPeds[i] = NULL
	ENDFOR
	
	CLEAR_CACHED_AVAILABLE_VEHICLE_COUNTS()
	
	CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iMissionModeBit, ciMISSION_USING_ORBITAL_CANNON)
	
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree,PROPERTY_BROADCAST_BS3_SVM_WALKOUT_COMPLETE)
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree,PROPERTY_BROADCAST_BS3_SVM_WALKOUT_READY)
	PRINTLN("MC_SCRIPT_CLEANUP PROPERTY_BROADCAST_BS2_SVM_WALKOUT_READY: Cleared")
	CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_FADE_HIDE)
	
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS() = LocalPlayer
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].SimpleInteriorBD.iBSTwo,  BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_EMPTY_ARMORY_TRUCK_TRAILER) 
			SET_EMPTY_ARMORY_TRUCK_TRAILER(FALSE)
			PRINTLN("[RCC MISSION] - MC_SCRIPT_CLEANUP - Setting BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_EMPTY_ARMORY_TRUCK_TRAILER")
		ENDIF
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].SimpleInteriorBD.iBSTwo,  BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_LEAVE_ARMORY_TRUCK_TRAILER_ON_DELIVER)
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].SimpleInteriorBD.iBSTwo,  BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_LEAVE_ARMORY_TRUCK_TRAILER_ON_DELIVER)
		ENDIF
	ENDIF
	#IF IS_DEBUG_BUILD
		bDrawMenu = FALSE

		PRINTLN("MC_SCRIPT_CLEANUP Powerplay ended. Strings played were: tl15PPPreIntroRoot = ", tl15PPPreIntroRoot, 
				", tl15PPTeamIntroRoot = ", tl15PPTeamIntroRoot, ", tl15PPSuddendeathRoot", tl15PPSuddendeathRoot,
				", tl15PPWinnerRoot = ", tl15PPWinnerRoot, ", tl15PPExitRoot = ", tl15PPExitRoot)
	#ENDIF
	IF NOT IS_THIS_A_ROUNDS_MISSION()
		PRINTLN("[MMacK][RoundsTimeBonus] MC_SCRIPT_CLEANUP g_iRoundsCombinedTime cleaned up!")
		g_iRoundsCombinedTime = 0
		g_iCachedMatchBonus = 0
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciALLOW_MOC_ENTRY_THIS_MISSION)
		SET_ALLOW_INTERIOR_SCRIPT_FOR_MISSION(FALSE)		
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSTwo,  BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_LEAVE_ARMORY_TRUCK_TRAILER_ON_DELIVER)
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSTwo,  BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_LEAVE_ARMORY_TRUCK_TRAILER_ON_DELIVER) 
			CDEBUG1LN(DEBUG_SPAWNING, "MC_SCRIPT_CLEANUP Clearing BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_LEAVE_ARMORY_TRUCK_TRAILER_ON_DELIVER ENDING")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen,ciENABLE_AVENGER_INTERIOR_ENTRY)		
		SET_ALLOW_INTERIOR_SCRIPT_FOR_MISSION(FALSE)
	ENDIF
	
	CLEAR_BIT(g_FMMC_STRUCT.bsRaceMission, ciDidSpeedBoost)
	REENABLE_AMBIENT_NIGHT_VISION()
	CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_BLOCK_TOGGLING_NIGHTVISION_FILTER)
	CLEAR_BIT(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_READY_FOR_LEADERBOARD)
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitSet3, ciBS3_ENABLE_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN)
		SET_ON_TEAM_RACE_RACE_GLOBAL(FALSE)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetTwelve, ciENABLE_TINY_RACERS_CHECKPOINT_LOGIC)
		RELEASE_COUNTDOWN_UI(cduiIntro)
		CLEAN_COUNTDOWN(cduiIntro)
	ENDIF
	
	// cleanup our saved drivers and partners
	PRINTLN("[LM][MULTI_VEH_RESPAWN][MC_SCRIPT_CLEANUP] - Cleaning up the variables for the next mission/round.")
	INT iii = 0
	FOR iii = 0 TO MAX_VEHICLE_PARTNERS-1
		SET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(iii, -1)
	ENDFOR
	SET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER(0)
	SET_PLAYER_RESPAWN_VEHICLE_SEAT_PREFERENCE(-3)
	CLEAR_SPECIFIC_VEHICLE_TO_RESPAWN_IN()	
	CLEAR_PLAYER_NEXT_RESPAWN_LOCATION()

	FOR i = 0 TO 3
		g_SpawnData.vCargobobSpawnCoord[i] = <<-1,-1,-1>>
		g_SpawnData.fCargobobSpawnHeading[i] = -1
	ENDFOR	
	
	g_SpawnData.iClosestCBPlayerIndex = -1
	g_SpawnData.fClosestCBPlayerDist = -1	
	MPGlobalsHud.b_HideCrossTheLineUI = FALSE
	MPGlobalsAmbience.bWantedStarsHidden = FALSE
	
	ACTIVATE_KILL_TICKERS( TRUE )
	g_bNoDeathTickers = FALSE
	g_bIsRunningBackMission = FALSE
	g_bTriggerCrossTheLineAudio = FALSE
	
	SET_USE_RACE_RESPOT_AFTER_RESPAWNS(FALSE)	
	CLEANUP_DYNAMIC_FLARES()	
	RESET_GLOBAL_SPECTATOR_STRUCT()	
	CLEAR_IDLE_KICK_TIME_OVERRIDDEN()	
	RESET_WARP_TO_SPAWN_LOCATION_GLOBALS() 
	RESET_NET_WARP_TO_COORD_GLOBALS() //kill any active warps
	CLEAR_TINY_RACERS_GLOBALS()
	CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_GENERIC_TEXT)	
	SET_SPECTATOR_CAN_QUIT(TRUE)
	
	g_bEndOfMissionCleanUpHUD = FALSE
	g_FMMC_STRUCT.bRemoveUnwantedCamPhoneOptions = FALSE	
	g_bHeistQuickRestart = FALSE
	//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].g_bIsHacking = FALSE
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bIsHacking)
	
	CLEAR_IM_WATCHING_A_MOCAP()
	
	//Hacking Scaleform
	sfHacking = NULL
	
	//Render target clean up.
	MC_serverBD_1.iPlayerForHackingMG = -1
	CLEANUP_RENDERTARGETS()
	IF ARE_EMERGENCY_CALLS_SUPPRESSED()
		RELEASE_SUPPRESSED_EMERGENCY_CALLS()
	ENDIF	
	
	g_FinalRoundsLbd = FALSE
	g_b_PopulateReady = FALSE
	g_iApartmentDropoffWarpRadius = iDEFAULT_WARP_TO_APARTMENT_DISTANCE
	g_bDelayUnlockScreenAfterHeists = TRUE // added by Kevin as Imran wanted unlocks delayed by 10 secs bug 2172240 6/1/2014
	MPGlobalsHud_TitleUpdate.unlock_delay_heists_ticker_main_timer = 0
	
	SET_ON_JOB_INTRO(FALSE)	
	SET_PLAYER_WILL_SPAWN_USING_SAME_NODE_TYPE_AS_TARGET(FALSE)	
	IF g_MultiplayerSettings.g_bTempPassiveModeSetting
		CLEANUP_TEMP_PASSIVE_MODE()
		TURN_OFF_PASSIVE_MODE_OPTION(FALSE)
	ENDIF	
	Clear_Any_Objective_Text()	
	CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_REMOVE_SNAPMATIC_GRID)
	
	//sync the checkpoints
	IF IS_BIT_SET(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_1)
		FMMC_CHECKPOINTS_SET_RETRY_START_POINT(0)
		PRINTLN("[MMacK][Checkpoint] Setting bRetryStartPoint = TRUE")
	ENDIF
	
	IF IS_BIT_SET(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_2)
		FMMC_CHECKPOINTS_SET_RETRY_START_POINT(1)
		PRINTLN("[MMacK][Checkpoint] Setting bRetrySecondStartPoint = TRUE")
	ENDIF
	
	IF IS_BIT_SET(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_3)
		FMMC_CHECKPOINTS_SET_RETRY_START_POINT(2)
		PRINTLN("[MMacK][Checkpoint] Setting bRetryThirdStartPoint = TRUE")
	ENDIF
	
	IF IS_BIT_SET(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_4)
		FMMC_CHECKPOINTS_SET_RETRY_START_POINT(3)
		PRINTLN("[MMacK][Checkpoint] Setting bRetryFourthStartPoint = TRUE")
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_SVM_PHANTOM2(g_FMMC_STRUCT.iRootContentIDHash)
		INT iNumTrolleysFirstCheckpoint = 2
		INT iNumTrolleysSecondCheckpoint = 1
		IF GET_NUMBER_OF_ACTIVE_PARTICIPANTS() = 3
			PRINTLN("[KH][COKEGRAB] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - 3 players reducing trolleys grabbed")
			iNumTrolleysFirstCheckpoint--
		ENDIF
		IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(0)
		AND !FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(1)
			PRINTLN("[KH][COKEGRAB] MC_SCRIPT_CLEANUP - Setting to 2/1 coke grab complete")
			g_TransitionSessionNonResetVars.iNumCokeGrabsCompleted = iNumTrolleysFirstCheckpoint
		ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(1)
			PRINTLN("[KH][COKEGRAB] MC_SCRIPT_CLEANUP - Setting to 3/2 coke grabs complete")
			g_TransitionSessionNonResetVars.iNumCokeGrabsCompleted = iNumTrolleysFirstCheckpoint + iNumTrolleysSecondCheckpoint
		ELSE
			PRINTLN("[KH][COKEGRAB] MC_SCRIPT_CLEANUP - Setting to 0 coke grabs complete")
			g_TransitionSessionNonResetVars.iNumCokeGrabsCompleted = 0
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_AllowCashTakeToPersistThroughTransition)
		IF IS_BIT_SET(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_STRAND_1)
			SET_BIT(g_TransitionSessionNonResetVars.iRestartBitset, FMMC_RESTART_BITSET_RETRY_STRAND_PASS_START_POINT)
			PRINTLN("[MMacK][Checkpoint] Setting FMMC_RESTART_BITSET_RETRY_STRAND_PASS_START_POINT = TRUE")
			
			IF g_TransitionSessionNonResetVars.iTotalCashGrabTake != MC_ServerBD.iCashGrabTotalTake
			OR g_TransitionSessionNonResetVars.iTotalCashGrabDrop != MC_ServerBD.iCashGrabTotalDrop
				//write new value of cash grab take and drop to the global variables when hitting a checkpoint
				PRINTLN("[RCC MISSION] Cash grab take at checkpoint is ", MC_ServerBD.iCashGrabTotalTake)
			
				g_TransitionSessionNonResetVars.iTotalCashGrabTake = MC_ServerBD.iCashGrabTotalTake
				PRINTLN("[RCC MISSION] POST_PARTICIPANT_PROCESSING Storing cash grab total take for checkpoint g_TransitionSessionNonResetVars.iTotalCashGrabTake = ", g_TransitionSessionNonResetVars.iTotalCashGrabTake, ".")
			
				g_TransitionSessionNonResetVars.iTotalCashGrabDrop = MC_ServerBD.iCashGrabTotalDrop
				PRINTLN("[RCC MISSION] POST_PARTICIPANT_PROCESSING Storing cash grab total drop for checkpoint g_TransitionSessionNonResetVars.iTotalCashGrabDrop = ", g_TransitionSessionNonResetVars.iTotalCashGrabDrop, ".")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_BuildSpawnGroupsDuringPlay)
		IF bStrand
		AND (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciOPTIONS4_MAINTAIN_SPAWN_GROUPS_OVER_STRAND)	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_MaintainSpawnGroupOverStrand_SaveOnly))
			PRINTLN("[RCC MISSION][ran] MC_SCRIPT_CLEANUP - Copying rsgSpawnSeed data into g_TransitionSessionNonResetVars, as this is a strand")
			g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed = MC_serverBD_4.rsgSpawnSeed
		ELSE
			IF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciOPTIONS4_MAINTAIN_SPAWN_GROUPS_ON_RESTART) AND IS_CORONA_INITIALISING_A_QUICK_RESTART())
			AND SHOULD_WE_START_FROM_CHECKPOINT()
				PRINTLN("[RCC MISSION][ran] MC_SCRIPT_CLEANUP - Copying rsgSpawnSeed data into g_TransitionSessionNonResetVars, as this is a quick restart to checkpoint w ciOptionsBS18_BuildSpawnGroupsDuringPlay,  MC_serverBD_4.rsgSpawnSeed: ",  MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS)
				g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed = MC_serverBD_4.rsgSpawnSeed
			ELSE
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_MaintainSpawnGroupOverStrand_DontClearSaved)
					CLEAR_SPAWN_GROUP_STRUCT(g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed)
				ELSE
					PRINTLN("[RCC MISSION][ran] MC_SCRIPT_CLEANUP - NOT CLEARING g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed: ", g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed.iEntitySpawnSeedBS, " as ciOptionsBS25_MaintainSpawnGroupOverStrand_DontClearSaved is set.")
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciOPTIONS4_MAINTAIN_SPAWN_GROUPS_ON_RESTART) AND IS_CORONA_INITIALISING_A_QUICK_RESTART())
		OR bStrand
		AND (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciOPTIONS4_MAINTAIN_SPAWN_GROUPS_OVER_STRAND)	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_MaintainSpawnGroupOverStrand_SaveOnly))
			PRINTLN("[RCC MISSION][ran] MC_SCRIPT_CLEANUP - Copying rsgSpawnSeed data into g_TransitionSessionNonResetVars, as this is a quick restart / strand,  MC_serverBD_4.rsgSpawnSeed: ",  MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS)
			g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed = MC_serverBD_4.rsgSpawnSeed
		ELSE
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_MaintainSpawnGroupOverStrand_DontClearSaved)
				CLEAR_SPAWN_GROUP_STRUCT(g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed)
			ELSE
				PRINTLN("[RCC MISSION][ran] MC_SCRIPT_CLEANUP - NOT CLEARING g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed: ", g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed.iEntitySpawnSeedBS, " as ciOptionsBS25_MaintainSpawnGroupOverStrand_DontClearSaved is set.")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(BitSet_CellphoneTU, g_BSTU_ENABLE_MP_PRES_EXTRACTION_APP)
		PRINTLN("[RCC MISSION][EXTRACTION] Turning off Extraction App!")
		CLEAR_BIT(BitSet_CellphoneTU, g_BSTU_ENABLE_MP_PRES_EXTRACTION_APP)
	ENDIF
	
	IF IS_BIT_SET(BitSet_CellphoneTU, g_BSTU_ENABLE_MP_SECUROSERV_HACK_APP)
	OR IS_BIT_SET(BitSet_CellphoneTU, g_BSTU_LAUNCH_CELLPHONE_MP_APPDUMMYAPP0)
		PRINTLN("[RCC MISSION][SECUROHACK] Turning off SecuroServ Hack App!")
		CLEAR_BIT(BitSet_CellphoneTU, g_BSTU_ENABLE_MP_SECUROSERV_HACK_APP)
		CLEAR_BIT(BitSet_CellphoneTU, g_BSTU_LAUNCH_CELLPHONE_MP_APPDUMMYAPP0)
		CLEAR_BIT(BitSet_CellphoneTU, g_BSTU_SET_CELLPHONE_CONTROLLER_LAUNCH)
	ENDIF
	
	IF g_bDisablePropertyAccessForLoseCopMissionObj
		g_bDisablePropertyAccessForLoseCopMissionObj = FALSE
		PRINTLN("g_bDisablePropertyAccessForLoseCopMissionObj set to FALSE - 2")
	ENDIF
	g_bReducedApartmentCreationTurnedOn = FALSE
	PRINTLN("g_bReducedApartmentCreationTurnedOn set to: ",g_bReducedApartmentCreationTurnedOn, " reason: ",999)
	PRINTLN("Script has disabled Trackify ")
	ENABLE_MULTIPLAYER_TRACKIFY_APP(FALSE)
	iTrackifyProgress = 0
	iBeastTrackifyProgress = 0
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_AllowCashTakeToPersistThroughTransition)
		IF HAS_TEAM_PASSED_MISSION(MC_playerBD[iLocalPart].iteam)
			g_TransitionSessionNonResetVars.iTotalCashGrabTake = MC_ServerBD.iCashGrabTotalTake
			g_TransitionSessionNonResetVars.iTotalCashGrabDrop = MC_ServerBD.iCashGrabTotalDrop
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - Allow Cash Take To Persist Through Transition")
		ENDIF
	ELSE	
		//write the server info into the global vars if this is needed,
		//when failing only store take if have no value stored, which should be when reaching the bank exit checkpoint
		//when reaching other checkpoints later on in the mission, we'll have to manually update global vars when checkpoint is reached
		IF ( g_TransitionSessionNonResetVars.iTotalCashGrabTake = 0 )
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP g_TransitionSessionNonResetVars.iTotalCashGrabTake = ", g_TransitionSessionNonResetVars.iTotalCashGrabTake, " need to update.")
			g_TransitionSessionNonResetVars.iTotalCashGrabTake = MC_ServerBD.iCashGrabTotalTake
			
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP g_TransitionSessionNonResetVars.iTotalCashGrabDrop = ", g_TransitionSessionNonResetVars.iTotalCashGrabDrop, " need to update.")
			g_TransitionSessionNonResetVars.iTotalCashGrabDrop = MC_ServerBD.iCashGrabTotalDrop
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP g_TransitionSessionNonResetVars.iTotalCashGrabTake = ", g_TransitionSessionNonResetVars.iTotalCashGrabTake, " no need to update.")
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP g_TransitionSessionNonResetVars.iTotalCashGrabDrop = ", g_TransitionSessionNonResetVars.iTotalCashGrabDrop, " no need to update.")
		#ENDIF
		ENDIF
	ENDIF
	
	PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP g_TransitionSessionNonResetVars.iTotalCashGrabTake = ", g_TransitionSessionNonResetVars.iTotalCashGrabTake)
	PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP g_TransitionSessionNonResetVars.iTotalCashGrabDrop = ", g_TransitionSessionNonResetVars.iTotalCashGrabDrop)
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		IF iPartToUse > -1
		AND iPartToUse < NUM_NETWORK_PLAYERS
		AND NETWORK_IS_GAME_IN_PROGRESS()
			CLEAR_BIT( MC_playerBD[ iPartToUse ].iClientBitSet, PBBOOL_HEIST_HOST )
			INT iHeistLeaderPlayer =  NATIVE_TO_INT( NETWORK_GET_PLAYER_INDEX( INT_TO_PARTICIPANTINDEX( iPartToUse ) ) )
			
			IF iHeistLeaderPlayer > -1
			AND iHeistLeaderPlayer < NUM_NETWORK_PLAYERS					
				GlobalplayerBD_FM_HeistPlanning[iHeistLeaderPlayer].iPreviousPropertyIndex = INVALID_HEIST_DATA
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(LocalPlayerPed)
		CLEAR_ALL_PED_VEHICLE_FORCED_SEAT_USAGE(LocalPlayerPed)
	ENDIF
	
	IF IS_AMBIENT_ZONE_ENABLED("AZ_DLC_HEIST_BIOLAB_GARAGE")
		SET_AMBIENT_ZONE_STATE("AZ_DLC_HEIST_BIOLAB_GARAGE",FALSE,TRUE)
	ENDIF
	
	g_sTransitionSessionData.bMissionCutsceneInProgress = FALSE
	PRINTLN("[RCC MISSION][AMEC] Setting bMissionCutsceneInProgress to FALSE- 2.")
	
	SET_SHOP_IGNORES_KICKING_OFF_CHECKS(CARMOD_SHOP_01_AP, FALSE)
	SET_SHOP_IGNORES_KICKING_OFF_CHECKS(CARMOD_SHOP_05_ID2, FALSE)
	SET_SHOP_IGNORES_KICKING_OFF_CHECKS(CARMOD_SHOP_06_BT1, FALSE)
	SET_SHOP_IGNORES_KICKING_OFF_CHECKS(CARMOD_SHOP_07_CS1, FALSE)
	SET_SHOP_IGNORES_KICKING_OFF_CHECKS(CARMOD_SHOP_08_CS6, FALSE)
	SET_SHOP_IGNORES_KICKING_OFF_CHECKS(CARMOD_SHOP_SUPERMOD, FALSE) 
	
	g_bAllowClothesShopInMission = FALSE
	g_bGarageDropOff = FALSE
	g_bPropertyDropOff = FALSE
	g_HeistGarageDropoffPanComplete = FALSE
	g_bMissionEnding = FALSE
	CLEAR_MP_DECORATOR_BIT(PLAYER_ID(), MP_DECORATOR_BS_MISSION_ENDING)
	PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - MP_DECORATOR_BS_MISSION_ENDING - CLEARED")
	g_bVSMission = FALSE
	g_bOnCoopMission = FALSE
	g_b_OnRaceIntro = FALSE
	g_bDeactivateRestrictedAreas = FALSE
	g_bMissionOver = FALSE	
	g_i_IgnoreSuicideTicker_PlayerBS = 0	
	g_bFMMCLightsTurnedOff = FALSE	
	g_bCleanupMultiplayerMissionInteriors = true 
	//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].g_bInMissionCreatorApartment = FALSE
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bInMissionCreatorApartment)
	CLEAR_BIT(GlobalplayerBD_FM[NETWORK_PLAYER_ID_TO_INT()].iXPBitset, ciGLOBAL_XP_BIT_ON_JOB)	
	CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM0_TARGET)
	CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM1_TARGET)
	CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM2_TARGET)
	CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM3_TARGET)	
	Delete_MP_Objective_Text()
	BYPASS_CELLPHONE_CAMERA_DEFAULT_SAVE_ROUTINE(FALSE)
	g_FMMC_STRUCT.g_b_QuitTest = FALSE
	IF NOT IS_CORONA_INITIALISING_A_QUICK_RESTART() // We don't want the IPLs to clean up if we're just coming back in
		PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - Not Quick Restarting, Blocking Building Controller FALSE")
		BLOCK_BUILDING_CONTROLLER_ON_MISSION(FALSE)
	ENDIF
	
	MPGlobalsInteractions.ePickupState = INT_TO_ENUM(eSpecialPickupState, 0)
	
	IF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_MISSILE_SILO_FINALE_P2)
		
		IF g_sTransitionSessionData.sStrandMissionData.iAlarmFrontendSoundId > -1
			PRINTLN("[TMS][SiloAlarm] Setting flag to stop power-up sound")
			SET_BIT(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_STRAND_MISSION_SILO_DEACTIVATE_ALARM)
		ENDIF		
		
		// As the leader, check what the vehicle choice was ahead of selling the vehicle that wasn't used
		IF HAS_TEAM_PASSED_MISSION(MC_playerBD[iLocalPart].iteam)

			IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
				IF IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iGangOpsMissionChoiceTrackingBitSet, ENUM_TO_INT(GANG_OPS_MISSION_CHOICE_KHANJALI))
				
					PRINTLN("[RCC MISSION] Leader of heist chose Khanjali")
					SET_MP_INT_CHARACTER_STAT(MP_STAT_FINALE_VEHICLE_CHOICE, 1)				
				
				ELIF IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iGangOpsMissionChoiceTrackingBitSet, ENUM_TO_INT(GANG_OPS_MISSION_CHOICE_BARRAGE))
					
					PRINTLN("[RCC MISSION] Leader of heist chose Barrage")
					SET_MP_INT_CHARACTER_STAT(MP_STAT_FINALE_VEHICLE_CHOICE, 2)
				ENDIF
			
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciALLOW_NON_HEIST_REBREATHERS)
		IF (NOT IS_ENTITY_DEAD(LocalPlayerPed))
		AND IS_MP_HEIST_GEAR_EQUIPPED(LocalPlayerPed, HEIST_GEAR_REBREATHER)
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - Have an equipped rebreather on since we were using them during mission. Removing for going back into freemode.")
			REMOVE_MP_HEIST_GEAR(LocalPlayerPed, HEIST_GEAR_REBREATHER)
		ENDIF
		IF iNumRebreathersFromFreemode > 0
			SET_MP_INT_CHARACTER_STAT(MP_STAT_BREATHING_APPAR_BOUGHT, iNumRebreathersFromFreemode)
			PRINTLN("[RCC MISSION] PROCESS_MISSION_END_STAGE - Resetting number of rebreathers to: ", iNumRebreathersFromFreemode)
		ENDIF
		g_bResetRebreatherAir = FALSE
	ENDIF
	
	IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		IF (NOT IS_ENTITY_DEAD(LocalPlayerPed))
		AND IS_MP_HEIST_GEAR_EQUIPPED(LocalPlayerPed, MC_playerBD_1[iLocalPart].eHeistGearBagType)
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - The player has an equipped duffel bag on since we were using them during mission. Removing for going back into freemode.")
			REMOVE_DUFFEL_BAG_HEIST_GEAR(LocalPlayerPed, iLocalPart)
		ENDIF
	ENDIF
	
	PREVENT_GANG_OUTFIT_CHANGING(FALSE)	
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		IF NOT IS_FORCED_WEAPON_STRAND_BEING_INITIALISED()
			CONFIGURE_CUSTOM_WEAPON_LOADOUT()
			PRINTLN("MC_SCRIPT_CLEANUP - CONFIGURE_CUSTOM_WEAPON_LOADOUT()")
		ENDIF
	ENDIF
			
	IF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_SCUBA_GEAR_ON)
		SET_PED_ABLE_TO_DROWN(LocalPlayerPed)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciRESPAWN_WITH_PARACHUTE_EQUIPPED)
		MPGlobalsAmbience.bDisableTakeOffChute = FALSE
	ENDIF	
	DISABLE_SPECTATOR_FILTER_OPTION(FALSE)
	
	INT iPlayers = 0		
	FOR iPlayers = 0 TO MAX_NUM_MC_PLAYERS-1
		CLEAR_BIT(g_iMissionPlayerLeftBS, iPlayers)
	ENDFOR
	g_iCurrentSpectatorTrapSpawned = 0
	g_bMissionClientGameStateRunning = FALSE
	SET_PLAYER_USING_ARENA_BOX_SEATID(-1)
	g_bPlayerFullySeated = FALSE
	SPAWN_LOCAL_PLAYER_IN_ARENA_BOX_SEAT(FALSE)
	LAUNCH_ARENA_SPECTATOR_ACTIVITIES(FALSE)
	
	g_iLastDamagerPlayer = -1
	g_iLastDamagerTimeStamp = -1
	g_iLastPlayerIDamaged = -1
	g_iLastPlayerIDamagedTimeStamp = -1
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].iLastDamagerPlayer = -1
	ENDIF
	
	SET_MC_PLAYER_MINI_ROUND_RESTART(FALSE)
	g_iFailedTrapCamSwitchBlackList = 0
	g_bArenaWarsUseForcedStockImperator = FALSE
	g_bArenaWarsUseForcedRaceOutfit = FALSE
	g_bMission321Done = FALSE
	CLEANUP_ARENA_ANNOUNCER_VARIABLES()
	g_iArenaBigScreenBinkControllerBS = 0
	g_bArenaBigScreenBinkPlaying = FALSE
	g_bIntroJobShardPlayed = FALSE
	g_fTimeSpentInSpectatorMode = 0
	g_fTimeSpentInSpectatorPowerup = 0
	g_iKillsInSpectatorMode = 0
	g_iKillsInSpectatorPowerup = 0
	g_iSpecialSpectatorTelemetryBS = 0
	RESET_NET_TIMER(g_tdTelemetryDelay)
	RESET_NET_TIMER(g_tdTelemetryDelay2)
	SET_TIME_LEFT_FOR_INSTANCED_CONTENT_IN_MILISECONDS(-1)
	g_iBS1_Mission = 0
	g_iBSPlayerLeftDoSomething = 0
	g_bUsingTurretHeliCam = FALSE
	g_bCelebrationEarlyDeathIsActive = FALSE
	g_bInInteriorThatRequiresInstantFadeout = FALSE
	g_bCasinoShockedAmbientPedDialogue = FALSE
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_EnableWeaponsInFreemodeShops)
		ALLOW_WEAPONS_IN_SHOPS(FALSE)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_DisablePlayerPropertiesInMission)
		INT iMaxProperties = ENUM_TO_INT(PPAG_ARENA_GARAGE)
		INT iProperty
		FOR iProperty = 0 TO iMaxProperties
			PRINTLN("[RCC MISSION] SET_PLAYER_BLOCK_FROM_PROPERTY_FLAG with iProperty: ", iProperty)
			CLEAR_PLAYER_BLOCK_FROM_PROPERTY_FLAG(INT_TO_ENUM(PREVENT_PROPERTY_ACCESS_FLAGS, iProperty))
		ENDFOR
	ENDIF
	CLEAR_BIT(GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_ARENA_FAILED)
	PRINTLN("MC_SCRIPT_CLEANUP - Cleaning up g_bArenaWarsUseForcedStockImperator & g_bArenaWarsUseForcedRaceOutfit")
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_EnableCasino_InteriorScripts)
		SET_ALLOW_INTERIOR_SCRIPT_FOR_MISSION(FALSE)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_EnableDroneDeployment)
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_AllowDroneAfterBranch)
		PRINTLN("[LM][RCC MISSION] MC_SCRIPT_CLEANUP, ciOptionsBS24_EnableDroneDeployment enabled. Disabling Drone Deployment for mission.") 
		SET_ENABLE_DRONE_USE_FROM_PI_MENU_IN_MISSION(FALSE)
	ENDIF
	CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_BLOCK_MASKS_AND_GEAR)	
	
	IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		g_bSlowingArmour = FALSE
	ENDIF
	
	g_bAutoMaskEquip = FALSE
ENDPROC

PROC CLEANUP_PHONE_BOOK()
	INT i
	FOR i = 0 TO FMMC_MAX_DIALOGUES_LEGACY-1
		IF g_FMMC_STRUCT.sDialogueTriggers[i].iCallCharacter = -1
		OR g_FMMC_STRUCT.sDialogueTriggers[i].iCallCharacter >= 32
			PRINTLN("[RCC MISSION] CLEANUP_PHONE_BOOK - Skipping Dialogue Trigger ", i, " because g_FMMC_STRUCT.sDialogueTriggers[", i, "].iCallCharacter is ", g_FMMC_STRUCT.sDialogueTriggers[i].iCallCharacter)
			RELOOP
		ENDIF
		
		IF IS_BIT_SET(iCharacterAddedToPhoneBook, g_FMMC_STRUCT.sDialogueTriggers[i].iCallCharacter)
			REMOVE_CONTACT_FROM_INDIVIDUAL_PHONEBOOK(GET_INCOMING_CONTACT_CHARACTER_FROM_CREATOR(g_FMMC_STRUCT.sDialogueTriggers[i].iCallCharacter), MULTIPLAYER_BOOK)
			CLEAR_BIT(iCharacterAddedToPhoneBook, g_FMMC_STRUCT.sDialogueTriggers[i].iCallCharacter)
			PRINTLN("[RCC MISSION] CLEANUP_PHONE_BOOK - Removing temporary contact from phone book for Dialogue Trigger ", i)
		ENDIF
	ENDFOR
ENDPROC

PROC CLEANUP_RAPPEL_ROPE()
	IF DOES_ROPE_EXIST(sWallRappel.RappelRopeID)
		DELETE_ROPE(sWallRappel.RappelRopeID)
	ENDIF
ENDPROC

PROC PROCESS_FINAL_CLEANUP_HANDHELD_DRILL_MINIGAME(STRUCT_HANDHELD_DRILL_MINI_GAME &sHandHeldDrillPassedIn)

	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_HandheldDrillMGMaintainSelectedSpawngroupOverStrand)
		IF IS_A_STRAND_MISSION_BEING_INITIALISED()
			g_TransitionSessionNonResetVars.iSubSpawnGroupHandheldHackBS = MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[g_FMMC_STRUCT.iHandheldDrillingMinigameSpawnGroup]
			PRINTLN("[LM][HandHeldDrill][iSubSpawnGroupHandheldHackBS] - g_TransitionSessionNonResetVars.iSubSpawnGroupHandheldHackBS assigned from server data: ", g_TransitionSessionNonResetVars.iSubSpawnGroupHandheldHackBS)
		ELSE
			PRINTLN("[LM][HandHeldDrill][iSubSpawnGroupHandheldHackBS] - g_TransitionSessionNonResetVars.iSubSpawnGroupHandheldHackBS = 0 - Strand not being init.")
			g_TransitionSessionNonResetVars.iSubSpawnGroupHandheldHackBS = 0
		ENDIF
	ELSE
		PRINTLN("[LM][HandHeldDrill][iSubSpawnGroupHandheldHackBS] - g_TransitionSessionNonResetVars.iSubSpawnGroupHandheldHackBS = 0 - Option not on..")
		g_TransitionSessionNonResetVars.iSubSpawnGroupHandheldHackBS = 0
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_EnableHandheldDrillMinigame)
		EXIT
	ENDIF
	
	CLEANUP_HANDHELD_MINIGAME_ANIM_DICTS()
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_CABINET_MODEL_FOR_PAINTING())
	MODEL_NAMES mnDrill = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Vault_Drill_01a"))	
	SET_MODEL_AS_NO_LONGER_NEEDED(mnDrill)	
	MODEL_NAMES mnCabinet
	INT i, ii
	FOR i = 0 TO 2
		FOR ii = 0 TO 2
			mnCabinet = GET_HANDHELD_DRILL_MINIGAME_CABINET_MODEL_FROM_INT(i, ii)
			SET_MODEL_AS_NO_LONGER_NEEDED(mnCabinet)
		ENDFOR
	ENDFOR
	FOR i = 0 TO 2
		FOR ii = 0 TO 2
			mnCabinet = GET_HANDHELD_DRILL_MINIGAME_CABINET_MODEL_FROM_INT(i, ii)
			SET_MODEL_AS_NO_LONGER_NEEDED(mnCabinet)
		ENDFOR
	ENDFOR
	IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		IF IS_VALID_INTERIOR(interiorCasinoVaultIndex)
			IF IS_INTERIOR_ENTITY_SET_ACTIVE(interiorCasinoVaultIndex, "Set_Spawn_Group1")		
				DEACTIVATE_INTERIOR_ENTITY_SET(interiorCasinoVaultIndex, "Set_Spawn_Group1")
			ENDIF
			IF IS_INTERIOR_ENTITY_SET_ACTIVE(interiorCasinoVaultIndex, "Set_Spawn_Group2")
				DEACTIVATE_INTERIOR_ENTITY_SET(interiorCasinoVaultIndex, "Set_Spawn_Group2")
			ENDIF
			IF IS_INTERIOR_ENTITY_SET_ACTIVE(interiorCasinoVaultIndex, "set_vault_lock_boxes1")
				DEACTIVATE_INTERIOR_ENTITY_SET(interiorCasinoVaultIndex, "set_vault_lock_boxes1")
			ENDIF
		ENDIF
	ENDIF
	
	CLEANUP_HANDHELD_DRILL_MINIGAME_REMOTE_FUNCTIONALITY(sHandHeldDrillPassedIn)
	
	STOP_HANDHELD_DRILL_MINIGAME_PARTICLE_EFFECT_FOR_DRILL(sHandHeldDrillPassedIn)
	STOP_HANDHELD_DRILL_MINIGAME_SOUND_EFFECT(sHandHeldDrillPassedIn)

	IF DOES_ENTITY_EXIST(sHandHeldDrillPassedIn.oiDrill)
		DELETE_OBJECT(sHandHeldDrillPassedIn.oiDrill)
	ENDIF
	IF DOES_ENTITY_EXIST(sHandHeldDrillPassedIn.oiDrillBag)
		DELETE_OBJECT(sHandHeldDrillPassedIn.oiDrillBag)
	ENDIF
	IF DOES_ENTITY_EXIST(sHandHeldDrillPassedIn.oiMoneyBag)
		DELETE_OBJECT(sHandHeldDrillPassedIn.oiMoneyBag)
	ENDIF
	IF DOES_ENTITY_EXIST(sHandHeldDrillPassedIn.oiCabinet)
		sHandHeldDrillPassedIn.oiCabinet = NULL
	ENDIF
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		IF (g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps - ciMAX_NUMBER_OF_HANDHELD_DRILL_LOCKBOX_CABINETS_DYNAMIC) >= 0
			g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps -= ciMAX_NUMBER_OF_HANDHELD_DRILL_LOCKBOX_CABINETS_DYNAMIC
		ENDIF
	ENDIF
	
	REMOVE_NAMED_PTFX_ASSET("scr_ch_finale")
ENDPROC

PROC CLEANUP_CCTV_CONES()		
	INT iObj
	FOR iObj = 0 TO FMMC_MAX_NUM_OBJECTS-1
		REMOVE_CCTV_VISION_CONE(biObjBlip[iobj], iObj, FMMC_ELECTRONIC_ENTITY_TYPE__OBJECT)
		REMOVE_CCTV_VISION_CONE(biDynoPropBlips[iobj], iObj, FMMC_ELECTRONIC_ENTITY_TYPE__DYNOPROP)
	ENDFOR		
ENDPROC

PROC CLEANUP_COVER_POINTS()	
	INT i = 0
	FOR i = 0 TO (FMMC_MAX_NUM_COVER-1)
		IF DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].vPos)
			PRINTLN("CLEANUP_COVER_POINTS | Cleaning up Cover Point: ", i)
			REMOVE_COVER_POINT(cpPlacedCoverPoints[i])
		ENDIF
	ENDFOR	
ENDPROC

FUNC BOOL DOES_CONTENT_HAVE_UDS_FUNCTIONALITY()
	
	IF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_BUNKER_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
	OR CONTENT_IS_USING_ARENA()
	OR CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_ADVERSARY_SERIES_JOB(g_FMMC_STRUCT.iRootContentIDHash)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MC_SCRIPT_CLEANUP(BOOL bFinalCleanUp = TRUE,BOOL bQuit =FALSE)
	
	INT i, iDoorHash
	INT iPart = -1
	BOOL bStrand = IS_A_STRAND_MISSION_BEING_INITIALISED()
	IF bQuit
	ENDIF	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		iPart = PARTICIPANT_ID_TO_INT()
	ENDIF		
	IF iPart = -1
	AND (iPartToUse != -1)
		iPart = iPartToUse
	ENDIF
	
	PRINTLN( "[RCC MISSION] **************MC_SCRIPT_CLEANUP*************** " )
	PRINTLN( "[RCC MISSION] MC_SCRIPT_CLEANUP - With iPart = ",iPart )
	PRINTLN( "[RCC MISSION] MC_SCRIPT_CLEANUP - bFinalCleanUp: ",bFinalCleanUp )
	PRINTLN( "[RCC MISSION] MC_SCRIPT_CLEANUP -  bQuit: ",bQuit )
	DEBUG_PRINTCALLSTACK()
		
	IF NOT DOES_ENTITY_EXIST(LocalPlayerPed)
		LocalPlayerPed = PLAYER_PED_ID()
	ENDIF
	
	PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - STARTING ################################")
	
	// FUNCTIONALISING THIS IS A WIP AT THE MOMENT - PLEASE TRY TO ADD TO, OR ADD NEW RELEVANT FUNCTIONS. THANK YOU.
	
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF DOES_CONTENT_HAVE_UDS_FUNCTIONALITY()
		END_SERIES_UDS_ACTIVITY(g_FMMC_STRUCT.iRootContentIDHash)
	ENDIF
	#ENDIF
	
	// Handles Screen Blur and other Post FX.
	UNLOAD_MISSION_SPECIFIC_POSTFX()
	
	// Handles the cleaning up of PTFX, Particles, Effects, etc.
	UNLOAD_MISSION_SPECIFIC_PTFX()
	
	// Audio Scenes, Sound Banks, and Sound IDs are all destroyed/released here.
	UNLOAD_MISSION_SPECIFIC_AUDIO(bFinalCleanUp)
		
	// Scaleform Movies, UI textures, all unloaded here.
	UNLOAD_MISSION_SPECIFIC_SCALEFORM()
	
	// Cleaning up Handheld Drill Minigame
	PROCESS_FINAL_CLEANUP_HANDHELD_DRILL_MINIGAME(sHandHeldDrillMiniGame)
	
	// Interiors and IPLS are unpinned/released in here.	
	UNLOAD_MISSION_SPECIFIC_INTERIORS_IPLS()
	
	// Special Models, Pickups or objects etc are all unloaded here.
	UNLOAD_MISSION_SPECIFIC_MODELS_AND_ENTITIES()
	
	// Any Anims that were specially loaded in are unloaded here.
	UNLOAD_MISSION_SPECIFIC_ANIMS()
	
	// Config Flags and other settings that were applied to vehicles during the mission are cleaned up
	RESET_VEHICLE_RELATED_CONFIGS()
	
	// Config Flags and other setting that were applied to players during the mission are cleaned up
	RESET_PLAYER_RELATED_CONFIGS()
	
	// General or External globals are reset here.
	RESET_MISSION_SPECIFIC_GLOBALS(bFinalCleanUp, bStrand)
	
	CLEANUP_PHONE_BOOK()
		
	PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - Resetting DISABLE_CUSTOM_VEHICLE_NODES_FOR_ALL_PROPERTIES to false.")
	DISABLE_CUSTOM_VEHICLE_NODES_FOR_ALL_PROPERTIES(FALSE)
	CLEAR_PLAYER_BLOCK_FROM_PROPERTY_FLAG(PPAF_GEN)	
	Make_Ped_Sober(localPlayerPed)
	Quit_Drunk_Camera_Immediately()		
	CLEANUP_OVERTIME_MODE_LEAVERS_VEHICLE()
	CLEANUP_BOMBUSHKA_MODE_LEAVERS()
	PRINTLN("Calling CLEANUP_MODE_LEAVERS_ON_OPTION for Mission")
	CLEANUP_MODE_LEAVERS_ON_OPTION()
	CLEANUP_TRON_MODE()	
	CLEANUP_GHOST_AND_ALPHA_EFFECTS()	
	CLEANUP_TLAD_FEET_EFFECTS()	
	CLEANUP_BLIMP(sBlimpSign)
	DO_BLIMP_SIGNS(sBlimpSign, TRUE)	
	CLEANUP_SHOWDOWN_POINTS()
	CLENANUP_POINTLESS_MODE_DATA()	
	CLEANUP_TINY_RACERS_SETTINGS()	
	RESET_WEAPON_DAMAGE_MODIFIERS()
	RESET_TEAM_HEALTH_REGEN()
	CLEANUP_RAPPEL_ROPE()
	CLEANUP_CCTV_CONES()
	CLEANUP_COVER_POINTS()	
	
	SET_LOCK_ADAPTIVE_DOF_DISTANCE(FALSE)	
	
	SET_IN_ARENA_MODE(FALSE)
	SET_VEHICLE_COMBAT_MODE(FALSE)
	SET_IN_STUNT_MODE(FALSE)
	ALLOW_PLAYER_HEIGHTS(FALSE)
	
	IF IS_BIT_SET(iLocalBoolCheck21,LBOOL21_TINY_RACERS_LAST_PLAYER_PROCESSED)
		CLEAR_BIT(iLocalBoolCheck21,LBOOL21_TINY_RACERS_LAST_PLAYER_PROCESSED)
	ENDIF
			
	//Clears Slashers weather
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_SLASHERS(g_FMMC_STRUCT.iAdversaryModeType)
		CLEAR_OVERRIDE_WEATHER()
		PRINTLN("[TMS] MC_SCRIPT_CLEANUP Clearing Slashers weather")
		SET_RAIN(-10)
	ENDIF

	IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_STROMBERG_FORCE_GPS_ACTIVE)
		PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - Setting Force show GPS back to false")
		SET_FORCE_SHOW_GPS(FALSE)
	ENDIF	
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRQUOTA(g_FMMC_STRUCT.iAdversaryModeType)
		RESTART_LOGIC_TURN_PHONE_RADIO_OFF()
		SET_MOBILE_PHONE_RADIO_STATE(FALSE)
		PRINTLN("[TMS] MC_SCRIPT_CLEANUP Turning off phone radio (Air Quota)")
	ENDIF
	
	IF CONTENT_IS_USING_ARENA()
		IF NETWORK_IS_GAME_IN_PROGRESS()
			NETWORK_CLEAR_CLOCK_TIME_OVERRIDE()
		ENDIF		
		CLEAR_OVERRIDE_WEATHER()
	ENDIF
		
	IF fLightIntesityScale >= 0
		SET_LIGHT_OVERRIDE_MAX_INTENSITY_SCALE(fLightIntesityScale)
	ENDIF
	
	IF HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_GRENADE)
		PRINTLN("[RCC MISSION] TURNING OFF INFINITE AMMO")
		SET_PED_INFINITE_AMMO(LocalPlayerPed, FALSE, WEAPONTYPE_GRENADE)
	ENDIF
		
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		TOGGLE_PAUSED_RENDERPHASES(TRUE)
		TOGGLE_PAUSED_RENDERPHASES(TRUE)
	ENDIF
		
	IF GET_IDLE_KICK_TIME_OVERRIDE_MS() != -1
		CLEAR_IDLE_KICK_TIME_OVERRIDDEN()
	ENDIF	
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_TEAM_SCORE_ON_BLIPS)
		INT iPlayBlip
		FOR iPlayBlip = 0 TO (MAX_NUM_MC_PLAYERS - 1)
			SET_CUSTOM_BLIP_NUMBER_FOR_PLAYER(INT_TO_PLAYERINDEX(i),0, FALSE)
		ENDFOR
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_SET_STUN_GUN_AS_LIMITED)
		SET_PED_STUN_GUN_FINITE_AMMO(LocalPlayerPed, FALSE)
		PRINTLN("[StunGun] Resetting SET_PED_STUN_GUN_FINITE_AMMO")
	ENDIF
	
	// B* 2654550: Script was ending without deactivating the spectator camera
	IF DOES_THIS_SPECTATOR_CAM_WANT_TO_LEAVE(g_BossSpecData.specCamData)
		IF IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData)
			CPRINTLN(DEBUG_SPECTATOR, "MC_SCRIPT_CLEANUP - DOES_THIS_SPECTATOR_CAM_WANT_TO_LEAVE() = TRUE")
						
			TOGGLE_RENDERPHASES( FALSE ) //NOTE(Owain): Fix for B* 2601452
			DEACTIVATE_SPECTATOR_CAM(g_BossSpecData, DSCF_BAIL_FOR_TRANSITION)
			NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE)
		ENDIF
	ENDIF
	CLEAR_DOES_THIS_SPECTATOR_CAM_WANT_TO_LEAVE(g_BossSpecData.specCamData)	
	
	IF NOT IS_PED_INJURED( LocalPlayerPed )
		SET_PED_DIES_INSTANTLY_IN_WATER( LocalPlayerPed, FALSE )
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck16, LBOOL16_DIVE_SCORE_RENDERPAUSE_ACTIVE)
		TOGGLE_PAUSED_RENDERPHASES(TRUE)
	ENDIF
	
	SET_CREATE_RANDOM_COPS( TRUE )
	SET_PED_MODEL_IS_SUPPRESSED(S_M_M_Security_01, FALSE)
	
	IF HAS_NET_TIMER_STARTED(tdRaceMissionTime)
		RESET_NET_TIMER(tdRaceMissionTime)
	ENDIF
	
	IF SHOULD_CLEAR_QUEUED_JOIN_REQUESTS_BASED_ON_EOJ_VOTE_STATUS()
		NETWORK_REMOVE_ALL_QUEUED_JOIN_REQUESTS()
		PRINTLN("[RCC MISSION] NETWORK_REMOVE_ALL_QUEUED_JOIN_REQUESTS called")
	ENDIF
	
	// Implemented to fix bug: url:bugstar:3077617 & changed for url:bugstar:4467866
	//IF IS_PED_INJURED(localPlayerPed)
		CLEAR_ENTITY_LAST_DAMAGE_ENTITY(localPlayerPed)
		PRINTLN("[RCC MISSION][ASSISTS][LM] Clearing the last damage event on localPlayerPed due to round end.")
	//ENDIF
		
	//Reset lowered heatmaps for friendly players
	INT iParticipant	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_DAMAGE_TRACKER_ACTIVE_ON_PLAYER(LocalPlayer)
			ACTIVATE_DAMAGE_TRACKER_ON_PLAYER(LocalPlayer,FALSE)
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(LocalPlayerPed)
			PRINTLN("[RCC MISSION][ASSISTS]Clearing Damage tracker on Local Player")
		ELSE
			PRINTLN("[RCC MISSION][ASSISTS]Damage tracker isn't active on Local Player")
		ENDIF
		
		IF IS_DAMAGE_TRACKER_ACTIVE_ON_PLAYER(LocalPlayer)
			ACTIVATE_DAMAGE_TRACKER_ON_PLAYER(LocalPlayer,TRUE)
			PRINTLN("[RCC MISSION][ASSISTS] Reactivating damage tracker on local player")
		ENDIF
		
		FOR iParticipant = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() -1
			IF iParticipant < MAX_NUM_MC_PLAYERS
				PARTICIPANT_INDEX participantIndex = INT_TO_PARTICIPANTINDEX(iParticipant)
				
				IF NETWORK_IS_PARTICIPANT_ACTIVE(participantIndex)
					PLAYER_INDEX 	playerIndex = NETWORK_GET_PLAYER_INDEX(participantIndex)
					PED_INDEX		playerPedIndex = GET_PLAYER_PED( playerIndex )
					
					IF NOT IS_PED_INJURED(playerPedIndex)
						DISABLE_PED_HEATSCALE_OVERRIDE(playerPedIndex)
						
						PRINTLN("[ThermalVision] DISABLE_PED_HEATSCALE_OVERRIDE ", iParticipant)
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		
		REMOVE_PLAYER_CUSTOM_BLIP_SPRITE()
	ENDIF
		
	IF IS_PLAYSTATION_PLATFORM()
		CLEAR_CONTROL_LIGHT_EFFECT(PLAYER_CONTROL)
	ENDIF
	
	//Check if it needs updating
	IF DOES_BLIP_EXIST(DestinationBlip)	
		REMOVE_BLIP(DestinationBlip)
		PRINTLN( "MC_SCRIPT_CLEANUP [RCC MISSION] - VIP MARKER REMOVED  ")
	ENDIF	
	INT iplayerloop
	FOR iplayerloop = 0 TO (NUM_NETWORK_PLAYERS-1)
		//turn off flashing player blips if they are still flashing
		PLAYER_INDEX tempPlayer = INT_TO_PLAYERINDEX(iplayerloop)
		FLASH_BLIP_FOR_PLAYER(tempPlayer, FALSE)

		//turn on any hidden health bars
		IF IS_BIT_SET(MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.iHideHealthBarsBitset[0], iplayerloop)
			CLEAR_BIT(MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.iHideHealthBarsBitset[0], iplayerloop)
		ENDIF
	ENDFOR	
	INT iloc	
	FOR i = 0 TO  i_NUMBER_OF_GRADIENT_BLIPS -1
		FOR iloc = 0 TO FMMC_MAX_GO_TO_LOCATIONS - 1
			IF DOES_BLIP_EXIST(LineGradientBlips[ i ][ iloc ])
				REMOVE_BLIP(LineGradientBlips[ i ][ iloc ])
			ENDIF
		ENDFOR
	ENDFOR
	IF DOES_BLIP_EXIST(biRegenBlip)
		REMOVE_BLIP(biRegenBlip)
		PRINTLN("[JS][TEAMREGEN] - MC_SCRIPT_CLEANUP removing blip")
	ENDIF
		
	//Reset Pickup Generation Range
	SET_PICKUP_GENERATION_RANGE_MULTIPLIER(1.0)
	
	// url:bugstar:2169152
	CLEAR_GPS_MULTI_ROUTE()
	SET_GPS_MULTI_ROUTE_RENDER(FALSE)
	
	// url:bugstar:2087082
	SET_IGNORE_NO_GPS_FLAG(FALSE)
	
	// url:bugstar:2239709
	IF IS_BIT_SET(iLocalBoolCheck11, LBOOL11_PAC_FIN_LOW_BUDGETS)
		PAC_FIN_SET_LOW_BUDGETS(FALSE)
	ENDIF
	
	IF MP_FORCE_TERMINATE_INTERNET_ACTIVE()
		MP_FORCE_TERMINATE_INTERNET_CLEAR()
		PRINTLN("[RCC MISSION] MP_FORCE_TERMINATE_INTERNET_ACTIVE - MP_FORCE_TERMINATE_INTERNET_CLEAR() called by MC_SCRIPT_CLEANUP")
	ENDIF
	
	IF IS_NAVMESH_REQUIRED_REGION_IN_USE()
		REMOVE_NAVMESH_REQUIRED_REGIONS()
	ENDIF		
	
	IF NOT g_bCelebrationScreenIsActive // So we won't unpause the renderphases while the celebration is active. Need the screen to be frozen for them. 
		IF NOT SHOULD_LAUNCH_HEIST_TUTORIAL_MID_STRAND_CUT_SCENE()
			IF IS_BIT_SET( iLocalBoolCheck7, LBOOL7_TOGGLED_RENDERPHASES_FOR_DROPOFF )
				TOGGLE_RENDERPHASES( TRUE )
				CLEAR_BIT( iLocalBoolCheck7, LBOOL7_TOGGLED_RENDERPHASES_FOR_DROPOFF )
				PRINTLN("[RCC MISSION] - LBOOL7_TOGGLED_RENDERPHASES_FOR_DROPOFF set false- 1")
			ENDIF
		ENDIF
		CLEAR_APARTMENT_DROPOFF_SPINNER(1)
	ENDIF
	
	//Clearing up the blocks which are placed in order to fix bug 3729570
	INT psWallBlockersLoopVar = 0
	REPEAT ciPacificStandardWallBlockers psWallBlockersLoopVar
		IF DOES_ENTITY_EXIST(oi_pacificStandardWallBlockers[psWallBlockersLoopVar])
			DELETE_OBJECT(oi_pacificStandardWallBlockers[psWallBlockersLoopVar])
			PRINTLN("[TMS] DESTROYING pacific standard wall blocker")
		ENDIF
	ENDREPEAT	
	
	INT iBargeProp = 0
	REPEAT 2 iBargeProp
		IF DOES_ENTITY_EXIST(g_FMMC_STRUCT_ENTITIES.oiBargeCollisionProps[iBargeProp])
			DELETE_OBJECT(g_FMMC_STRUCT_ENTITIES.oiBargeCollisionProps[iBargeProp])
			PRINTLN("[TMS] Clearing up g_FMMC_STRUCT_ENTITIES.oiBargeCollisionProps[", iBargeProp, "]")
		ENDIF
	ENDREPEAT

	IF IS_A_STRAND_MISSION_BEING_INITIALISED()
		MAINTAIN_ALL_ENTITIES_WHEN_ON_STRAND_CUTSCENE(TRUE)
	ENDIF
	
	IF (iPart != -1)
	AND (NOT IS_BIT_SET(MC_playerBD[ iPart ].iClientBitSet,PBBOOL_START_SPECTATOR))
		IF NOT IS_PLAYER_SCTV(PLAYER_ID())
			//-- DOn't remove weapons if there's another mission coming
			IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
			AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
				DEAL_WITH_DLC_WEAPONS(iDlcWeaponBitSet, FALSE)
			ELSE
				PRINTLN("[RCC MISSION] [dsw] Cleanup not removing DLC weapons as strand mission or a test mission")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_ALL_SHOPS_LOCKED)
		SET_ALL_SHOP_LOCATES_ARE_BLOCKED(FALSE)
		SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)
		HIDE_ALL_SHOP_BLIPS(FALSE)
		PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - SET_ALL_SHOP_LOCATES_ARE_BLOCKED updated to FALSE ")
		CLEAR_BIT(iLocalBoolCheck10, LBOOL10_ALL_SHOPS_LOCKED)
	ENDIF
		
	RESET_SPECIAL_PICKUP( TRUE )
	g_bEnableHackingApp = FALSE
	g_HackingAppHasBeenLaunched = FALSE	
	g_FMMC_STRUCT.bDisablePhoneInstructions = FALSE
	g_FMMC_STRUCT.bRememberToCleanUpHacking = FALSE
	IF NETWORK_IS_GAME_IN_PROGRESS()
		CLEAR_FOOTSTEP_SWEETENERS_FOR_ALL_PLAYER_PEDS(TRUE)
	ENDIF
	REMOVE_SPECIAL_PICKUP_ASSETS()
	#IF IS_NEXTGEN_BUILD
	MANAGE_FIRST_PERSON_BINBAG( sCurrentSpecialPickupState.obj, 
								sCurrentSpecialPickupState.objFirstPerson, 
								LocalPlayerPed, 
								TRUE )
	#ENDIF
	
	iVehDamageTrackerBS = 0
	
	IF HAS_SCALEFORM_MOVIE_LOADED(uiGoStopDisplay)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(uiGoStopDisplay)
	ENDIF
	
	IF NOT IS_THIS_A_ROUNDS_MISSION()
	OR SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
	OR NOT bFinalCleanUp
	OR bQuit
		SET_RANDOM_TRAINS( TRUE )
		CPRINTLN( DEBUG_SIMON, "MC_SCRIPT_CLEANUP - Enabling SET_RANDOM_TRAINS" )
	ENDIF
		
	
	RESET_DROPOFF_GLOBALS(5)
	
	g_iApartmentDropoffObjectsBitset = 0
	
	g_bFailedMission = FALSE
	
	CLEANUP_TRAIN()
	
	CLEANUP_SVM_BJXL()
	
	IF IS_PV_DISABLED()
		DISABLE_PERSONAL_VEHICLE_CREATION_FOR_MISSION( FALSE ) 
	ENDIF
	
	SET_DO_VISIBILITY_CHECKS_FOR_SPAWNING(TRUE)
	
	RESET_CARMOD_MENU_OPTION_FOR_CREATOR_MISSION()
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		IF Is_Player_Currently_On_MP_Heist(PLAYER_ID())
		OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
			IF NOT IS_CORONA_INITIALISING_A_QUICK_RESTART()
			AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
				SET_HEIST_AM_I_HEIST_LEADER_GLOBAL(FALSE)
				g_TransitionSessionNonResetVars.iHeistCutPercent = 0				
				PRINTLN("[RCC MISSION] Outfits - Clearing iHeistOutfit because not a quick restart")
			ENDIF
			RESET_ALL_HUD_COLOURS() // 1948497
			RESET_HEIST_BONUS_TICKERS()
			
		ENDIF
		
		IF NOT IS_CORONA_INITIALISING_A_QUICK_RESTART()
			IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
				g_TransitionSessionNonResetVars.iHeistPresistantPropertyID = -1
				PRINTLN("[RCC MISSION] - [PMC] - IS_A_STRAND_MISSION_BEING_INITIALISED() = FALSE and IS_CORONA_INITIALISING_A_QUICK_RESTART() = FALSE, resetting g_TransitionSessionNonResetVars.iHeistPresistantPropertyID to value ", g_TransitionSessionNonResetVars.iHeistPresistantPropertyID)
			ELSE
				g_TransitionSessionNonResetVars.iHeistPresistantPropertyID = MC_serverBD.iHeistPresistantPropertyID
				PRINTLN("[RCC MISSION] - [PMC] - IS_A_STRAND_MISSION_BEING_INITIALISED() = TRUE, keeping g_TransitionSessionNonResetVars.iHeistPresistantPropertyID at value ", g_TransitionSessionNonResetVars.iHeistPresistantPropertyID)
			ENDIF
		ELSE
			g_TransitionSessionNonResetVars.iHeistPresistantPropertyID = MC_serverBD.iHeistPresistantPropertyID
			PRINTLN("[RCC MISSION] - [PMC] - IS_THIS_A_QUICK_RESTART_JOB() = TRUE, keeping g_TransitionSessionNonResetVars.iHeistPresistantPropertyID at value ", g_TransitionSessionNonResetVars.iHeistPresistantPropertyID)
		ENDIf
	ENDIF
	
	IF IS_A_STRAND_MISSION_BEING_INITIALISED()
	AND NOT IS_THIS_A_QUICK_RESTART_JOB()
	AND NOT bQuit
		IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE()
			//Variable lives shouldn't take deaths into account
			g_TransitionSessionNonResetVars.iTeamLives = GET_FMMC_MISSION_LIVES_FOR_COOP_TEAMS()
		ELSE
			g_TransitionSessionNonResetVars.iTeamLives = GET_FMMC_MISSION_LIVES_FOR_COOP_TEAMS() - MC_serverBD.iTotalCoopDeaths[MC_playerBD[NATIVE_TO_INT(LocalPlayer)].iteam]
		ENDIF
		PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - Storing TeamLives in g_TransitionSessionNonResetVars.iTeamLives = ", g_TransitionSessionNonResetVars.iTeamLives)
	ELSE
		PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - Resetting g_TransitionSessionNonResetVars.iTeamLives = -1")
		g_TransitionSessionNonResetVars.iTeamLives = -1
	ENDIF

	CHECK_AND_AWARD_IF_PLAYER_HAS_PERFORMED_ALL_HEIST_ROLES()
	
	IF IS_LOADED_MISSION_LOW_RIDER_FLOW_MISSION()
	AND IS_LOADED_MISSION_LOW_FLOW_MISSION(ciLOW_FLOW_MISSION_LOWRIDER_FINALE)
		CACHE_STAT_VARIABLES_FOR_STRAND_MISSION()
	ENDIF
	
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
	OR Is_Player_Currently_On_MP_Heist(PLAYER_ID())
	OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
	OR CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE()
		CACHE_STAT_VARIABLES_FOR_STRAND_MISSION()
		
		IF HAS_TEAM_PASSED_MISSION(MC_playerBD[iLocalPart].iteam)
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - Setting strand mission team deaths: team 0 as ",MC_serverBD.iTeamDeaths[0],", team 1 as ",MC_serverBD.iTeamDeaths[1],", team 2 as ",MC_serverBD.iTeamDeaths[2],", team 3 as ",MC_serverBD.iTeamDeaths[3])
			SET_STRAND_MISSION_TEAM_DEATHS(MC_serverBD.iTeamDeaths[0], MC_serverBD.iTeamDeaths[1], MC_serverBD.iTeamDeaths[2], MC_serverBD.iTeamDeaths[3])
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iTeamBitset4, ciBS4_TEAM_PER_PLAYER_THERMAL_CHARGES)
				PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - Setting strand mission player thermal charges remaining: ", MC_playerBD_1[iLocalPart].iThermitePerPlayerRemaining)
				SET_STRAND_MISSION_PLAYER_THERMAL_CHARGES_REMAINING(MC_playerBD_1[iLocalPart].iThermitePerPlayerRemaining)
			ELSE 
				PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - Setting strand mission team thermal charges remaining: team 0 as ",MC_serverBD_1.iTeamThermalCharges[0],", team 1 as ",MC_serverBD_1.iTeamThermalCharges[1],", team 2 as ",MC_serverBD_1.iTeamThermalCharges[2],", team 3 as ",MC_serverBD_1.iTeamThermalCharges[3])
				SET_STRAND_MISSION_TEAM_THERMAL_CHARGES_REMAINING(MC_serverBD_1.iTeamThermalCharges[0], MC_serverBD_1.iTeamThermalCharges[1], MC_serverBD_1.iTeamThermalCharges[2], MC_serverBD_1.iTeamThermalCharges[3])
			ENDIF
		ENDIF
	ENDIF
	
	IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE()
		IF IS_A_STRAND_MISSION_BEING_INITIALISED()
		AND NOT IS_CORONA_INITIALISING_A_QUICK_RESTART()
			sLEGACYMissionLocalContinuityVars.iOutfit = MC_playerBD[iLocalPart].iOutfit
			PRINTLN("[CONTINUITY] sLEGACYMissionLocalContinuityVars.iOutfit set to: ", sLEGACYMissionLocalContinuityVars.iOutfit)
		ENDIF
	ENDIF
	
	CACHE_MISSION_CONTINUITY_VARS()
	
	IF Is_Player_Currently_On_MP_Heist(PLAYER_ID())
	OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
	
		IF IS_A_STRAND_MISSION_BEING_INITIALISED()
			//Prevent weapon switching between strand missions
			g_TransitionSessionNonResetVars.previousMissionWeapon = GET_PLAYER_CURRENT_HELD_WEAPON()
		ENDIF
		/*IF IS_CORONA_INITIALISING_A_QUICK_RESTART()
			g_TransitionSessionNonResetVars.iTripSkipCost = MC_playerBD[NATIVE_TO_INT(LocalPlayer)].TSPlayerData.iCost
			PRINTLN("[RCC MISSION] TRIP SKIP - g_TransitionSessionNonResetVars.iTripSkipCost = TSPlayerData.iCost $", MC_playerBD[NATIVE_TO_INT(LocalPlayer)].TSPlayerData.iCost)
		ENDIF*/
		
		IF SHOULD_WE_START_FROM_CHECKPOINT()
		OR IS_MISSION_START_CHECKPOINT_SET(1)
		OR HAS_FIRST_STRAND_MISSION_BEEN_PASSED()
			g_TransitionSessionNonResetVars.iTripSkipCost = MC_playerBD[NATIVE_TO_INT(LocalPlayer)].TSPlayerData.iCost
			#IF IS_DEBUG_BUILD
			IF SHOULD_WE_START_FROM_CHECKPOINT()
				PRINTLN("MC_SCRIPT_CLEANUP - TRIP SKIP - SHOULD_WE_START_FROM_CHECKPOINT = TRUE")
			ENDIF
			IF IS_MISSION_START_CHECKPOINT_SET(1)
				PRINTLN("MC_SCRIPT_CLEANUP - TRIP SKIP - IS_MISSION_START_CHECKPOINT_SET(1) = TRUE")
			ENDIF
			IF HAS_FIRST_STRAND_MISSION_BEEN_PASSED()
				PRINTLN("MC_SCRIPT_CLEANUP - TRIP SKIP - HAS_FIRST_STRAND_MISSION_BEEN_PASSED() = TRUE")
			ENDIF
			PRINTLN("MC_SCRIPT_CLEANUP - TRIP SKIP - g_TransitionSessionNonResetVars.iTripSkipCost SET TO TSPlayerData.iCost $", g_TransitionSessionNonResetVars.iTripSkipCost)
			#ENDIF
		ENDIF
		
		//Remove all remote players from heist audio mix group
		INT iPlayer
		REPEAT NUM_NETWORK_PLAYERS iPlayer
			PLAYER_INDEX piCurrent = INT_TO_PLAYERINDEX(iPlayer)
			IF IS_NET_PLAYER_OK(piCurrent)
				IF piCurrent != LocalPlayer
					PED_INDEX pedCurrent = GET_PLAYER_PED(piCurrent)
					IF NOT IS_PED_INJURED(pedCurrent)
						REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(pedCurrent, 0.0)
						PRINTLN("[MJM] MC_SCRIPT_CLEANUP - Removed player: ",iPlayer," from heist audio group")
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
	ENDIF
	
	// FAKE PHONE FOR HACKING
	IF DOES_ENTITY_EXIST(e_FakePlayerPhone)
		DELETE_OBJECT(e_FakePlayerPhone)
		PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - Deleted Fake Hacking Phone object.")
		
		IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPCircuitHack2")
			SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPCircuitHack2")
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - Unloaded Hacking Render to Phone Sprite Dict.")
		ENDIF
	ENDIF
	
	IF NOT IS_CORONA_INITIALISING_A_QUICK_RESTART()
	AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
	AND AM_I_ON_A_HEIST()
		g_iHeistLeaderPlayerID = -1
		CLEAR_HEIST_MISSION_STARTING_APARTMENT()
		PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - Clearing heist starting apartment")
	ENDIF
	
	CLEAR_RESTRICTED_AREA_BLOCKING()
	
	// Return weapons at the end
    IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		
		IF IS_JOB_FORCED_WEAPON_ONLY()		// Return weapons if we've been in a deathmatch
	    OR IS_JOB_FORCED_WEAPON_PLUS_PICKUPS()
		OR ((iPart != -1) AND SHOULD_CURRENT_PLAYER_HAVE_WEAPONS_REMOVED( MC_playerBD[ iPart ].iteam ))	// Or if the mission requested the weapons to be removed
			IF NOT IS_FORCED_WEAPON_STRAND_BEING_INITIALISED()
				PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP calling GIVE_MP_REWARD_WEAPON_IN_FREEMODE")
				GIVE_MP_REWARD_WEAPON_IN_FREEMODE( TRUE, FALSE, WEAPONINHAND_FIRSTSPAWN_HOLSTERED, FALSE, FALSE, FALSE, SHOULD_GIVE_WEAPONS_BACK_THROUGH_GIVE_MP_REWARD_WEAPON(MC_playerBD[iPart].iteam))
		        IF wtWeaponToRemove <> WEAPONTYPE_INVALID
		            IF HAS_PED_GOT_WEAPON( PLAYER_PED_ID(), wtWeaponToRemove )
		                REMOVE_WEAPON_FROM_PED( PLAYER_PED_ID(), wtWeaponToRemove )
		                PRINTLN("[RCC MISSION] - REMOVING WEAPON LOANED AT START")
		            ENDIF
		    	ENDIF
			ENDIF
		ENDIF
		
		IF ((iPart != -1) AND DOES_TEAM_HAVE_AN_FMMC_GIVEN_INVENTORY(MC_playerBD[iPart].iTeam) )
		OR IS_BIT_SET( g_FMMC_STRUCT.iOptionsMenuBitSet, ciREMOVE_WEAPONS )	// Or if the mission requested the weapons to be removed
		OR IS_BIT_SET( g_FMMC_STRUCT.iWepRestBS, REST_TEAM_1 ) // Or if we removed a particular team's weapons
		OR IS_BIT_SET( g_FMMC_STRUCT.iWepRestBS, REST_TEAM_2 )
		OR IS_BIT_SET( g_FMMC_STRUCT.iWepRestBS, REST_TEAM_3 )
		OR IS_BIT_SET( g_FMMC_STRUCT.iWepRestBS, REST_TEAM_4 )
			
			IF (g_bMissionInventory OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciMISSION_OVERRIDE_AMMUNATION_FORCE_CLOSE))
			AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
			OR NOT bFinalCleanUp
				PRINTLN("[RCC MISSION] DOES_TEAM_HAVE_AN_FMMC_GIVEN_INVENTORY OR ciREMOVE_WEAPONS set - Resetting weapons...")
				REMOVE_ALL_WEAPONS()
				RESET_PLAYERS_WEAPONS_OBTAINED()
				IF GET_PED_ARMOUR(PLAYER_PED_ID()) > 0
					SET_PED_ARMOUR(PLAYER_PED_ID(), 0)
				ENDIF
				g_bMissionInventory = FALSE
				IF SHOULD_GIVE_WEAPONS_BACK_THROUGH_GIVE_MP_REWARD_WEAPON(MC_playerBD[iPart].iteam)
					g_GIVE_PLAYER_ALL_UNLOCKED_AND_EQUIPED_WEAPONS_FM_CHECK = TRUE
					PRINTLN("[RCC MISSION] Setting g_GIVE_PLAYER_ALL_UNLOCKED_AND_EQUIPED_WEAPONS_FM_CHECK to TRUE")
				ENDIF
			ENDIF
			
		ENDIF
		
		IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		OR NOT bFinalCleanUp
			g_bMissionInventory = FALSE
		ENDIF
		
		//Restore Parachute Tints
		IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		AND NOT IS_BIT_SET(iLocalBoolCheck5, LBOOL5_SET_UP_STRAND_DETAILS_AT_END_OF_MISSION)
			EQUIP_STORED_MP_PARACHUTE(PLAYER_ID())
		ENDIF		
	ENDIF
	
	//If the cut scene has
	IF bFinalCleanUp
		CLEANUP_SCRIPTED_CUTSCENE(bFinalCleanUp)
	ENDIF
	
	IF NETWORK_IS_IN_MP_CUTSCENE()
		SET_NETWORK_CUTSCENE_ENTITIES(FALSE)
	ENDIF
	
	SET_ENABLE_VEHICLE_SLIPSTREAMING(FALSE)
	
	SET_HIDOF_OVERRIDE(FALSE,FALSE,0,0,0,0)
	
	//gdisablerankupmessage = FALSE
	SET_DISABLE_RANK_UP_MESSAGE(FALSE)
	g_bAllowJobReplay = FALSE
	g_MaskObjectiveActive = FALSE
	
	SET_USE_DLC_DIALOGUE(FALSE)
	
	IF DOES_PLAYER_HAVE_A_BAG( PLAYER_PED_ID() )
		REMOVE_PLAYER_BAG()
	ENDIF
	
	//Clear the prop spawn bitsets
	INT iTeamLoop
	FOR iTeamLoop = 0 TO 3
		LocalRandomSpawnBitset[iTeamLoop] = 0
		IF NETWORK_IS_GAME_IN_PROGRESS()
		AND bIsLocalPlayerHost
			MC_ServerBD.iSpawnPointsUsed[iTeamLoop] = 0
		ENDIF
		IF IS_BIT_SET(iTeamPropSpawns, iTeamLoop)
			CLEAR_BIT(iTeamPropSpawns, iTeamLoop)
		ENDIF
	ENDFOR
	
	//Reseting Melee Damage to Normal
	IF bTakedownDamageModifier
		IF NETWORK_IS_GAME_IN_PROGRESS()
			INT iDamageModLoop
			FOR iDamageModLoop = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() -1
				PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iDamageModLoop)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
					PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
					SET_PLAYER_WEAPON_TAKEDOWN_DEFENSE_MODIFIER(tempPlayer, 1.0)
				ENDIF
			ENDFOR
		ENDIF
	ENDIF

	// Spectator cam
    IF NOT IS_THIS_SPECTATOR_CAM_OFF(g_BossSpecData.specCamData)
		FORCE_CLEANUP_SPECTATOR_CAM(g_BossSpecData, TRUE)
    ENDIF
	
	CLEAR_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_ALLOW_SPECTATING_RIVAL_TEAMS)
	MPSpecGlobals.iPreferredSpectatorPlayerID = -1
	
	CLEANUP_END_JOB_VOTING(nextJobStruct)
	
	SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		SET_ENTITY_PROOFS(PLAYER_PED_ID(),FALSE,FALSE,FALSE,FALSE,FALSE)
	ENDIF
	
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
		
	SET_STORE_ENABLED(TRUE)
	
	CLEAR_TIMECYCLE_MODIFIER()
	
	OVERRIDE_INTERIOR_SMOKE_END()
	
	CLEAR_CTF_MUSIC_GLOBAL()
	
	UNLOAD_ALL_COMMON_LEADERBOARD_ELEMENTS(LBPlacement)
	
	SHOW_ALL_PLAYER_BLIPS(FALSE)
	SET_ALL_PLAYER_BLIPS_LONG_RANGE(FALSE)
	
	HIDE_SPECTATOR_HUD(FALSE)
	
	IF (iPart != -1)
		SET_BIT(MC_PlayerBD[iPart].iClientBitSet,PBBOOL_TERMINATED_SCRIPT)
	ENDIF
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		SET_AUTO_GIVE_PARACHUTE_WHEN_ENTER_PLANE(PLAYER_ID(),FALSE)
	ENDIF
	
	CLEAR_ALL_SPECTATOR_TARGET_LISTS(g_BossSpecData.specHUDData)	
		
	//Clear Radio Station Over Score (Final Round Only)
	IF NOT (IS_THIS_A_ROUNDS_MISSION() AND NOT SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD())
		g_SpawnData.bRadioStationOverScore = FALSE
		PRINTLN("MC_SCRIPT_CLEANUP - g_SpawnData.bRadioStationOverScore = FALSE")
	ENDIF
	
	INT iSpawnGroup
	
	IF (IS_THIS_A_ROUNDS_MISSION() AND NOT SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD())
		g_TransitionSessionNonResetVars.iSpawnGroupLastBitset = MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS
		PRINTLN("MC_SCRIPT_CLEANUP - COPY iSpawnGroupLastBitset = ", g_TransitionSessionNonResetVars.iSpawnGroupLastBitset)
		REPEAT ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS iSpawnGroup
			g_TransitionSessionNonResetVars.iSpawnSubGroupLastBitset[iSpawnGroup] = MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[iSpawnGroup]
			PRINTLN("MC_SCRIPT_CLEANUP - COPY iSpawnSubGroupLastBitset[", iSpawnGroup, "] = ", g_TransitionSessionNonResetVars.iSpawnSubGroupLastBitset[iSpawnGroup])
		ENDREPEAT
	ELSE
		g_TransitionSessionNonResetVars.iSpawnGroupLastBitset = 0
		PRINTLN("MC_SCRIPT_CLEANUP - CLEAR iSpawnGroupLastBitset = 0")
		REPEAT ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS iSpawnGroup
			g_TransitionSessionNonResetVars.iSpawnSubGroupLastBitset[iSpawnGroup] = 0
			PRINTLN("MC_SCRIPT_CLEANUP - CLEAR iSpawnSubGroupLastBitset[", iSpawnGroup, "] = 0")
		ENDREPEAT
	ENDIF
	
	FOR i = 0 TO (FMMC_MAX_TEAMS-1)
		NETWORK_OVERRIDE_TEAM_RESTRICTIONS(i, FALSE)
		PRINTLN("[MMacK][VoiceChat] Setting All Teams NETWORK_OVERRIDE_TEAM_RESTRICTIONS to FALSE ... Team = ", i)
		IF NETWORK_IS_GAME_IN_PROGRESS()
		AND NETWORK_IS_HOST_OF_THIS_SCRIPT()
			MC_serverBD_3.iAdditionalTeamLives[i] = 0
		ENDIF
	ENDFOR
	MC_playerBD[iLocalPart].iAdditionalPlayerLives = 0
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		FOR i = 0 TO (MAX_NUM_MC_PLAYERS - 1)
			PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(i)
			
			IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				PLAYER_INDEX playerIndex = NETWORK_GET_PLAYER_INDEX(tempPart)
				
				NETWORK_OVERRIDE_SEND_RESTRICTIONS(playerIndex, FALSE)
				PRINTLN("[VoiceChat] Setting All Players NETWORK_OVERRIDE_SEND_RESTRICTIONS to FALSE... Name = ", GET_PLAYER_NAME(playerIndex))
				NETWORK_OVERRIDE_RECEIVE_RESTRICTIONS(playerIndex, FALSE)
				PRINTLN("[VoiceChat] Setting All Players NETWORK_OVERRIDE_RECEIVE_RESTRICTIONS to FALSE... Name = ", GET_PLAYER_NAME(playerIndex))
			ENDIF
		ENDFOR
	ENDIF
	
	NETWORK_OVERRIDE_SEND_RESTRICTIONS_ALL(FALSE)
	NETWORK_OVERRIDE_RECEIVE_RESTRICTIONS_ALL(FALSE)
	
	NETWORK_SET_OVERRIDE_SPECTATOR_MODE(FALSE)
	
	g_iOverheadNamesState = -1
	
	g_bMissionReadyForAM_VEHICLE_SPAWN = FALSE
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== Mission === g_bMissionReadyForAM_VEHICLE_SPAWN = FALSE")
	#ENDIF
	
	SET_MAX_NUM_PORTABLE_PICKUPS_CARRIED_BY_PLAYER(DUMMY_MODEL_FOR_SCRIPT,-1)
	
	CLEAR_A_INVENTORY_BOX_CONTENT(ci_Ped_Inventory)
	CLEAR_A_INVENTORY_BOX_CONTENT(ci_veh_Inventory)
	CLEAR_A_INVENTORY_BOX_CONTENT(ci_obj_Inventory)
	CLEAR_INVENTORY_BOX_CONTENT()
	
	CLEANUP_ALL_AI_PED_BLIPS(biHostilePedBlip)

	CLEANUP_SOCIAL_CLUB_LEADERBOARD(scLB_control)
	CLEAR_RANK_REDICTION_DETAILS() 
		
	PRINTLN("[RCC MISSION] - [ARTIFICIAL LIGHTS] - [CLIENT] - LBOOL5_TURNED_OFF_ARTIFICIAL_LIGHTS, doing mission cleanup, turning lights back on with SET_ARTIFICIAL_LIGHTS_STATE(FALSE).")
	SET_ARTIFICIAL_LIGHTS_STATE(FALSE)

	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		IF GET_JOINING_GAMEMODE() = GAMEMODE_CREATOR
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
				PRINTLN("[RCC MISSION][JJT] MC_SCRIPT_CLEANUP - Heading into the creator, calling SET_PLAYER_CLOTHES_FOR_RETURN_TO_FREEMODE")
				Clear_All_Generated_MP_Headshots(FALSE)
				IF NOT GB_OUTFITS_IS_PLAYER_WEARING_GANG_OUTFIT(PLAYER_ID())
					SET_PLAYER_CLOTHES_FOR_RETURN_TO_FREEMODE()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//if garage_model_hide_set
	if enum_to_int(garage_door_model) != 0
		remove_model_hide(<<-29.32, -1086.63, 26.95>>, 1.0, garage_door_model)
	endif 
	
	IF DOES_CAM_EXIST(camEndScreen)
		DESTROY_CAM(camEndScreen)
	ENDIF
	
	if does_cam_exist(cutscenecam)
		destroy_cam(cutscenecam)
	endif 
	
	NETWORK_SET_VOICE_ACTIVE(true)
	
	PRINTLN("[dsw] [RCC MISSION] Deleteing mission text messages")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("MIS_CUST_TXT2")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("MIS_CUST_TXT3")
	
	FOR i = 0 TO (FMMC_MAX_RULES-1)
	
		//If any doors were swapped then reset them here.
		IF IS_BIT_SET(iLocalHackDoorThermiteBitset, i)
			PRINTLN("[RCC MISSION] [OPEN_HACK_DOOR] Resetting damaged door. Rule: ", i, " on door: ", iHackDoorID[i])
			REMOVE_MODEL_SWAP(GET_FMMC_DOOR_COORDS(iHackDoorID[i]), 3.0, GET_FMMC_DOOR_MODEL(iHackDoorID[i], FALSE), GET_FMMC_DOOR_MODEL(iHackDoorID[i], TRUE))
		ENDIF
	
		IF iHackDoorID[i] != -1
			GET_NEW_HACK_DOOR_HASH(i,GET_FMMC_DOOR_MODEL(iHackDoorID[i]),GET_FMMC_DOOR_COORDS(iHackDoorID[i]),iDoorHash)
			IF CAN_ACCESS_CODE_DOOR(iDoorHash)				
				DOOR_SYSTEM_SET_OPEN_RATIO(iDoorHash,0,FALSE,TRUE)
				DOOR_SYSTEM_SET_DOOR_STATE(iDoorHash, DOORSTATE_FORCE_LOCKED_THIS_FRAME,FALSE, FALSE)
     			REMOVE_DOOR_FROM_SYSTEM(iDoorHash)
			ENDIF
		ENDIF
		
		IF iHackDoorID2[i] != -1
			GET_NEW_HACK_DOOR_HASH(i,GET_FMMC_DOOR_MODEL(iHackDoorID2[i]),GET_FMMC_DOOR_COORDS(iHackDoorID2[i]),iDoorHash)
			IF CAN_ACCESS_CODE_DOOR(iDoorHash)
				DOOR_SYSTEM_SET_OPEN_RATIO(iDoorHash,0,FALSE,TRUE)
				DOOR_SYSTEM_SET_DOOR_STATE(iDoorHash, DOORSTATE_FORCE_LOCKED_THIS_FRAME,FALSE, FALSE)
     			REMOVE_DOOR_FROM_SYSTEM(iDoorHash)
			ENDIF
		ENDIF
		
	ENDFOR
	
	// Door Cleanup
	FOR i = 0 TO (FMMC_MAX_NUM_DOORS-1)
		IF iDoorID[i] != -1
			GET_NEW_DOOR_HASH(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[i].mnDoorModel,g_FMMC_STRUCT_ENTITIES.sSelectedDoor[i].vPos,iDoorHash, iCachedDoorHashes[i])
			IF CAN_ACCESS_CODE_DOOR(iDoorHash)
				DOOR_SYSTEM_SET_OPEN_RATIO(iDoorHash,0,FALSE,TRUE)
				DOOR_SYSTEM_SET_DOOR_STATE(iDoorHash, DOORSTATE_FORCE_LOCKED_THIS_FRAME,FALSE, FALSE)
     			REMOVE_DOOR_FROM_SYSTEM(iDoorHash)
				#IF IS_DEBUG_BUILD
				PRINTLN("[Doors] Removing door at vPos: ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[i].vPos, " door number: ", i, " - iDoorHash: ", iDoorHash, " / g_FMMC_STRUCT_ENTITIES.sSelectedDoor[",i, "].mn: ", GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[i].mnDoorModel), " mn as int: ", ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[i].mnDoorModel))
				#ENDIF
			ENDIF
			iDoorID[i] = -1
		ENDIF
	ENDFOR
	
	IF IS_BIT_SET(g_TransitionSessionNonResetVars.iFMMCMissionBS, iFMMCMissionBS__MISSION_CONTROLLER_IS_CONTROLLING_DOORS)
		CLEAR_BIT(g_TransitionSessionNonResetVars.iFMMCMissionBS, iFMMCMissionBS__MISSION_CONTROLLER_IS_CONTROLLING_DOORS)
		PRINTLN("[RCC MISSION][Doors] Clearing iFMMCMissionBS__MISSION_CONTROLLER_IS_CONTROLLING_DOORS")
	ENDIF
	
	CLEAR_ALL_MISSION_CONTROL_NET_DOOR_OVERRIDE() //clears all conor's networked doors.
	
	//2059824 - Reset any of the doors that were swapped for damaged versions.		
	iLocalHackDoorThermiteBitset = 0
	iLocalHackDoorThermiteFailedBitset = 0
	
	//cleaning up doors from MAINTAIN_SETTING_MAP_OBJECT_DOOR_RATIOS
	FOR I = 0 TO COUNT_OF(map_door) - 1
		IF DOES_ENTITY_EXIST(map_door[I])
			CPRINTLN( DEBUG_CONTROLLER, "SETTING MAP_DOOR ", I , " AS NO LONGER NEEDED." )
			SET_OBJECT_AS_NO_LONGER_NEEDED(map_door[I])
		ENDIF
	ENDFOR
	 
 	FOR i = 0 TO (FMMC_MAX_RULES-1)
		IF DOES_ENTITY_EXIST(Vault_door[i])
			SET_ENTITY_HEADING(Vault_door[i],fHackDoorStartHeading[i])
			SET_OBJECT_AS_NO_LONGER_NEEDED(Vault_door[i])
		ENDIF
	ENDFOR
	
	if does_entity_exist(zip_tie_obj)
		DELETE_OBJECT(zip_tie_obj)
	endif
	
	set_model_as_no_longer_needed(HEI_PROP_ZIP_TIE_POSITIONED)
	
	for i = 0 to count_of(monkey_ped) - 1
		
		if does_entity_exist(monkey_ped[i])
			DELETE_PED(monkey_ped[i])
		endif 
		
		set_model_as_no_longer_needed(A_C_Rhesus)
		
	endfor 
	
	LEGACY_CLEANUP_WORLD_PROPS(sRuntimeWorldPropData)	
	
	FOR i = 0 TO (FMMC_MAX_PEDS-1)
		IF IS_BIT_SET(iPedTagCreated[GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i))
	        REMOVE_PED_OVERHEAD_ICONS(iPedTag[i], i, iPedTagCreated, iPedTagInitalised)
	    ENDIF
		IF DOES_BLIP_EXIST(biPedBlip[i])
			REMOVE_BLIP(biPedBlip[i])
		ENDIF
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[i])
			CLEANUP_COLLISION_ON_PED(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[i]), i)
		ELSE
			CLEANUP_COLLISION_ON_PED(NULL, i)
		ENDIF
	ENDFOR
	
	FOR i = 0 TO FMMC_MAX_PEDS - 1
		IF DOES_BLIP_EXIST(biCaptureRangeBlip[i])
			REMOVE_BLIP(biCaptureRangeBlip[i])
		ENDIF
	ENDFOR

	FOR i = 0 TO (FMMC_MAX_VEHICLES-1)
		IF DOES_BLIP_EXIST(biVehBlip[i])
			REMOVE_VEHICLE_BLIP(i)
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP removing veh: ",i)
		ENDIF
		
		IF DOES_BLIP_EXIST(biVehCaptureRangeBlip[i])
			REMOVE_BLIP(biVehCaptureRangeBlip[i])
		ENDIF
	ENDFOR
	
	FOR i = 0 TO (FMMC_MAX_NUM_OBJECTS-1)
		REMOVE_OBJECT_BLIP(i)
	ENDFOR
	CLEAR_FAKE_CONE_ARRAY()
	
	// Remove pickup blips
	FOR i = 0 TO (FMMC_MAX_WEAPONS-1)
		IF DOES_BLIP_EXIST(biPickup[i])
			REMOVE_BLIP(biPickup[i])
		ENDIF
		IF DOES_PICKUP_EXIST(pipickup[i])
			REMOVE_PICKUP(pipickup[i])
		ENDIF
	ENDFOR
	
	CLEAN_UP_SPAWN_VEHICLE_BLIPS()
	
	IF NOT IS_PED_INJURED(LocalPlayerPed)
		IF bStopDurationBlockSeatShuffle
			IF GET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_PreventAutoShuffleToDriversSeat)
				SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_PreventAutoShuffleToDriversSeat, FALSE)
				
				bStopDurationBlockSeatShuffle = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableVoiceDrivenMouthMovement , FALSE)
		
		IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED() // Fixed 2051968
			CLEAR_PED_TASKS(LocalPlayerPed)
			RESET_PED_MOVEMENT_CLIPSET(LocalPlayerPed)
			RESET_PED_WEAPON_MOVEMENT_CLIPSET(LocalPlayerPed)
			CLEAR_PED_FALL_UPPER_BODY_CLIPSET_OVERRIDE(LocalPlayerPed)
			//Don't remove the players wanted level in a strand mission
			g_bMissionRemovedWanted = TRUE
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(),0)
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			PRINTLN("MC_SCRIPT_CLEANUP - Cleaning up player's wanted level now")			
		ENDIF
		
		IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE()
		AND NOT IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING()
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			PRINTLN("MC_SCRIPT_CLEANUP - (Casino) Cleaning up player's wanted level now")
		ENDIF
	ENDIF
	
	IF bFinalCleanUp
		
		PRINTLN("MC_SCRIPT_CLEANUP - bFinalCleanup is TRUE! bStrand = ",bStrand,", MC_serverBD.iEndCutscene = ",MC_serverBD.iEndCutscene)
		
		IF IS_NET_PLAYER_OK(PLAYER_ID())
		AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		AND NOT IS_BIT_SET(iLocalBoolCheck5, LBOOL5_SET_UP_STRAND_DETAILS_AT_END_OF_MISSION)
			IF NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
			OR IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			ENDIF
			// REMOVE_PED_HELMET(PLAYER_PED_ID(),TRUE) url:bugstar:5161046   (Commented out because it was causing that bug - we'll see how it goes, timelapsed but couldn't get to the bottom of why this is necessary.)
		ENDIF
		
		IF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_USING_PV)
			CLEANUP_MP_SAVED_VEHICLE(TRUE)
		ENDIF
		
		IF IS_NET_PLAYER_OK(PLAYER_ID(),FALSE)
		AND (NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_END_CUTSCENE_STARTED) AND NOT IS_BIT_SET(iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE_OVER))
			IF (NOT bStrand AND NOT IS_BIT_SET( iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE ))
			OR (MC_serverBD.iEndCutscene = ciMISSION_CUTSCENE_PLACE_HOLDER)
				VEHICLE_INDEX LastVeh = GET_LAST_DRIVEN_VEHICLE()
				
				IF DOES_ENTITY_EXIST(LastVeh) 					
					PRINTLN("MC_SCRIPT_CLEANUP - LastVeh = ", NETWORK_ENTITY_GET_OBJECT_ID(LastVeh))
					
					IF NETWORK_HAS_CONTROL_OF_ENTITY(LastVeh)
						PRINTLN("MC_SCRIPT_CLEANUP - LastVeh - We Have Control.")
						
						IF CAN_REGISTER_MISSION_VEHICLES(1)
							PRINTLN("MC_SCRIPT_CLEANUP - LastVeh - Can Register Additional Vehicles")
							IF NOT IS_VEHICLE_A_PERSONAL_VEHICLE(LastVeh) // let neil's pv system manage themselves.
							OR (NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(LastVeh) AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(LastVeh))
								PRINTLN("MC_SCRIPT_CLEANUP - LastVeh - Registered with the Mission Controller")
								IF IS_VEHICLE_DRIVEABLE(LastVeh)
									IF IS_ENTITY_ATTACHED(LastVeh)
										DETACH_ENTITY(LastVeh) //for the magnet stuff
									ENDIF
								ENDIF
								
								IF NOT NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(LastVeh) 
									PRINTLN("[MC_SCRIPT_CLEANUP][RH] Network has control of LastVeh but it is not registered with the Mission Controller. Registering now")
									SET_ENTITY_AS_MISSION_ENTITY(LastVeh, NETWORK_IS_HOST_OF_THIS_SCRIPT(), TRUE) 
								ENDIF
								
								IF NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(LastVeh)
									PRINTLN("MC_SCRIPT_CLEANUP - LastVeh - Deleting Vehicle")
								
									IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
										CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
									ENDIF
																		
									IF IS_VEHICLE_A_CARGOBOB(LastVeh)
										ENTITY_INDEX vehcargo = GET_VEHICLE_ATTACHED_TO_CARGOBOB(LastVeh)
										IF DOES_ENTITY_EXIST(vehcargo)
											IF IS_ENTITY_A_VEHICLE(vehCargo)
												VEHICLE_INDEX vehCargoIndex = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(vehCargo)
												DETACH_VEHICLE_FROM_ANY_CARGOBOB(vehCargoIndex)
											ENDIF
										ENDIF
									ENDIF
									
									DELETE_VEHICLE(LastVeh)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						PRINTLN("MC_SCRIPT_CLEANUP - LastVeh - We Do Not Have Control.")
						
						IF NOT IS_VEHICLE_A_PERSONAL_VEHICLE(LastVeh) // let neil's pv system manage themselves.
						AND (NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(LastVeh) AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(LastVeh))
							PRINTLN("MC_SCRIPT_CLEANUP - LastVeh - Registered with the Mission Controller")
							
							IF NETWORK_IS_GAME_IN_PROGRESS()
							//AND NETWORK_IS_HOST_OF_THIS_SCRIPT() //can only set mission entities if we are host
								PRINTLN("MC_SCRIPT_CLEANUP - LastVeh - Can Reigster Additional Vehicles")
								
								IF NETWORK_IS_HOST_OF_THIS_SCRIPT() 
									IF NOT NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(LastVeh)
										PRINTLN("[MC_SCRIPT_CLEANUP][RH] Network does not control of LastVeh but it is not registered with the Mission Controller. Registering now")
										SET_ENTITY_AS_MISSION_ENTITY(LastVeh, TRUE, TRUE)
									ENDIF
								ENDIF
								
								// If these fail then whatever made this vehicle should be the thing that cleans it up.
								IF NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(LastVeh)
								AND IS_ENTITY_A_MISSION_ENTITY(LastVeh)								
									PRINTLN("MC_SCRIPT_CLEANUP - LastVeh - Setting as No Longer Needed")
									SET_VEHICLE_AS_NO_LONGER_NEEDED(LastVeh)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					PRINTLN("MC_SCRIPT_CLEANUP - LastVeh = NULL")
				ENDIF
				CLEAR_LAST_DRIVEN_VEHICLE()
			ENDIF
		ENDIF
		
		g_iNumPlayersOnMyHeistTeam = -1
		
		FOR i = 0 TO (MAX_BACKUP_VEHICLES-1)
			DELETE_BACKUP_UNIT(i)
		ENDFOR
		
		IF (NOT bStrand OR (MC_serverBD.iEndCutscene = ciMISSION_CUTSCENE_PLACE_HOLDER))
		AND (NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_END_CUTSCENE_STARTED) AND NOT IS_BIT_SET(iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE_OVER)) // url:bugstar:5830986 || (Cutscene Entities were being cleaned up by remote clients finishing it early...)
			CLEANUP_ALL_FMMC_ENTITIES(MC_serverBD_1.sFMMC_SBD)
			FOR i = 0 TO (GET_FMMC_MAX_NUM_PROPS()-1)
				IF DOES_ENTITY_EXIST(oiProps[i])
					DELETE_OBJECT(oiProps[i])
				ENDIF
			ENDFOR
			FOR i = 0 TO (FMMC_MAX_NUM_PROP_CHILDREN-1)
				IF DOES_ENTITY_EXIST(oiPropsChildren[i])
				   DELETE_OBJECT(oiPropsChildren[i])
				ENDIF
			ENDFOR 
				
			// cleanup respawn vehicle
			IF NETWORK_DOES_NETWORK_ID_EXIST(netRespawnVehicle)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(netRespawnVehicle)
					DELETE_NET_ID(netRespawnVehicle)
				ELSE
					CLEANUP_NET_ID(netRespawnVehicle)
				ENDIF
			ENDIF
		ENDIF

		IF NOT bStrand
		OR (MC_serverBD.iEndCutscene = ciMISSION_CUTSCENE_PLACE_HOLDER)
			FOR i = 0 TO (FMMC_MAX_VEHICLES-1)
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[i])
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[i])
						DELETE_NET_ID(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[i])
					ELSE
						CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[i])
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
		
		//jt bug - add cleanup for titans here!!!
		
		IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_END_CUTSCENE_STARTED) AND NOT IS_BIT_SET(iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE_OVER)
			FOR i = 0 to (total_number_of_cutscene_vehicles-1)
				IF DOES_ENTITY_EXIST(cutscene_vehicle[i])
					IF NETWORK_HAS_CONTROL_OF_ENTITY(cutscene_vehicle[i])
						IF CAN_REGISTER_MISSION_VEHICLES(1)
						AND ((NOT bStrand) OR (MC_serverBD.iEndCutscene = ciMISSION_CUTSCENE_PLACE_HOLDER))
							SET_ENTITY_AS_MISSION_ENTITY(cutscene_vehicle[i], FALSE, TRUE)
							DELETE_VEHICLE(cutscene_vehicle[i])
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
		
		FOR i = 0 TO (FMMC_MAX_NUM_DYNOPROPS-1)
			
			REMOVE_DYNOPROP_BLIP(i)
			
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niDynoProps[i])
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niDynoProps[i])
					DELETE_NET_ID(MC_serverBD_1.sFMMC_SBD.niDynoProps[i])
				ELSE
					CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niDynoProps[i])
				ENDIF
			ENDIF
		ENDFOR
		
		REPEAT COUNT_OF(niMinigameObjects) i
			IF NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[i])
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(niMinigameObjects[i])
					DELETE_NET_ID(niMinigameObjects[i])
				ELSE
					CLEANUP_NET_ID(niMinigameObjects[i])
				ENDIF
			ENDIF
		ENDREPEAT
	
		IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		AND NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_END_CUTSCENE_STARTED) AND NOT IS_BIT_SET(iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE_OVER)
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				CLEAR_AREA_LEAVE_VEHICLE_HEALTH(GET_ENTITY_COORDS(PLAYER_PED_ID(),FALSE),5000,TRUE, DEFAULT, DEFAULT, TRUE)
			ENDIF
			CLEAR_AREA_OF_OBJECTS(GET_ENTITY_COORDS(PLAYER_PED_ID(),FALSE),5000,CLEAROBJ_FLAG_FORCE) 
			PRINTLN("[RCC MISSION] CLEAR_AREA_OF_OBJECTS called with force flag ")
		ENDIF
		
	ENDIF
	
	IF NETWORK_IS_SIGNED_ONLINE() 
		MC_CLEAR_PED_AND_TRAFFIC_DENSITIES(iPopulationHandle, bVehicleGenerator)
		iPopulationHandle = -1
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
		AND NETWORK_IS_HOST_OF_THIS_SCRIPT()
			IF MC_serverBD.fPedDensity = 0.0 
			AND MC_serverBD.fVehicleDensity = 0.0
				SERVER_CLEAR_BLOCKING_ALL_SCENARIOS(MC_serverBD_3.scenBlockServer)
			ENDIF
			
			IF NETWORK_ENTITY_AREA_DOES_EXIST(MC_serverBD.iSpawnArea)
				NETWORK_REMOVE_ENTITY_AREA(MC_serverBD.iSpawnArea)
			ENDIF
			MC_serverBD.iSpawnArea = -1
		ENDIF
	ENDIF
	
	FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfZones-1)
		IF iPopMultiArea[i] != -1
			IF DOES_POP_MULTIPLIER_AREA_EXIST(iPopMultiArea[i])
				REMOVE_POP_MULTIPLIER_AREA(iPopMultiArea[i], TRUE)
				PRINTLN("[RCC MISSION] iPopMultiArea cleaned up: ",i," native area id: ",iPopMultiArea[i])
				iPopulationHandle = -1
			ENDIF
		ENDIF
		IF bipopScenarioArea[i] != NULL
			REMOVE_SCENARIO_BLOCKING_AREA(bipopScenarioArea[i])
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP bipopScenarioArea cleaned up: ",i," native area id: ",NATIVE_TO_INT(bipopScenarioArea[i]))
		ENDIF
		
		IF sbiPacificHeliBlocker != NULL
			REMOVE_SCENARIO_BLOCKING_AREA(sbiPacificHeliBlocker)
		ENDIF
		
		IF sbiCasinoHeistHeliBlocker != NULL
			REMOVE_SCENARIO_BLOCKING_AREA(sbiCasinoHeistHeliBlocker)
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iType = ciFMMC_ZONE_TYPE__CLEAR_ALL
		OR g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iType = ciFMMC_ZONE_TYPE__CLEAR_VEHICLES
		OR g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iType = ciFMMC_ZONE_TYPE__ACTIVATE_ROADS
			SET_VEHICLE_GENERATORS_AND_ROADS_INACTIVE_FOR_ZONE_LEGACY(i, FALSE, TRUE, TRUE)
		ENDIF
		
		IF iGPSDisabledZones[i] != -1
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP clearing GPS disabled zone at index ", iGPSDisabledZones[i])
			CLEAR_GPS_DISABLED_ZONE_AT_INDEX(iGPSDisabledZones[i])
			iGPSDisabledZones[i] = -1
		ENDIF
		IF iDispatchBlockZone[i] != -1
			PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP removing dispatch block zone at index: ", iDispatchBlockZone[i])
			REMOVE_DISPATCH_SPAWN_BLOCKING_AREA(iDispatchBlockZone[i])
		ENDIF
		IF iSpawnOcclusionIndex[i] != -1
			CLEAR_MISSION_SPAWN_OCCLUSION_AREA(iSpawnOcclusionIndex[i])
		ENDIF
		IF iAirDefenseSphereMission[i] != -1
			REMOVE_AIR_DEFENCE_SPHERE(iAirDefenseSphereMission[i])
		ENDIF
		IF iNavBlockerZone[i] != -1
			IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iNavBlockerZone[i])
				REMOVE_NAVMESH_BLOCKING_OBJECT(iNavBlockerZone[i])
			ENDIF
		ENDIF
	ENDFOR
	
	REMOVE_ALL_COVER_BLOCKING_AREAS()
	
	FOR i = 0 TO (FMMC_MAX_VEHICLES-1)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iPriority[MC_playerBD[iLocalPart].iteam] < FMMC_MAX_RULES
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn != DUMMY_MODEL_FOR_SCRIPT
			AND IS_MODEL_VALID(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
				PRINTLN("[RCC MISSION] suppressing vehicle model FALSE: ",ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn))
				SET_VEHICLE_MODEL_IS_SUPPRESSED(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn,FALSE)				
				VALIDATE_NUMBER_MODELS_SUPRESSED(-1)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn = Technical
					PRINTLN("[RCC MISSION] suppressing vehicle model FALSE: ",ENUM_TO_INT(REBEL))
					SET_VEHICLE_MODEL_IS_SUPPRESSED(Rebel,FALSE)
					VALIDATE_NUMBER_MODELS_SUPRESSED(-1)
				ENDIF
				CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetTwo, ciFMMC_VEHICLE2_TriggerAudioOnVehicle) 
			ENDIF
		ENDIF
	ENDFOR
	FOR i = 0 TO FMMC_MAX_TEAMS - 1
		IF g_FMMC_STRUCT.mnVehicleModel[i] != DUMMY_MODEL_FOR_SCRIPT
		AND IS_MODEL_VALID(g_FMMC_STRUCT.mnVehicleModel[i])
			PRINTLN("[RCC MISSION] suppressing vehicle model FALSE: ",ENUM_TO_INT(g_FMMC_STRUCT.mnVehicleModel[i]))
			SET_VEHICLE_MODEL_IS_SUPPRESSED(g_FMMC_STRUCT.mnVehicleModel[i], FALSE)
			VALIDATE_NUMBER_MODELS_SUPRESSED(-1)
		ENDIF
	ENDFOR
	
	FOR i = 0 TO (FMMC_MAX_NUM_STUNTJUMPS - 1)
		IF IS_BIT_SET(iStuntJumpBitSet, i)
			DELETE_STUNT_JUMP(sjsID[i])
			CLEAR_BIT(iStuntJumpBitSet, i)
		ENDIF
	ENDFOR
	
	//CHECK_AND_GIVE_MISS_RACE_DM_DAILY_XP()
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		IF NETWORK_IS_SIGNED_ONLINE() 
		
			NET_NL()NET_PRINT("[RCC MISSION] [BCCHEAT] MC_SCRIPT_CLEANUP  ")NET_PRINT(" g_FMMC_STRUCT.iMissionType = ")NET_PRINT_INT(g_FMMC_STRUCT.iMissionType)
			NET_PRINT("[RCC MISSION]  g_FMMC_STRUCT.iMissionSubType = ")NET_PRINT_INT(g_FMMC_STRUCT.iMissionSubType)

			IF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_CTF
				INT IncrementBy = INCREMENT_ENDING_ACTIVITY_CHEAT_STAT_BY()
				NET_NL()NET_PRINT("[RCC MISSION] [BCCHEAT] MPPLY_CAP_CHEAT_END Being Incremented in MC_SCRIPT_CLEANUP. ")
				INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_CAPTURE_END)
				INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_CAP_CHEAT_END, IncrementBy)
				IF IncrementBy > 0
					INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_ACTIVITY_ENDED, 1)
				ENDIF
				IF IS_CURRENT_CONTENT_MADE_BY_A_FRIEND()
					REQUEST_SYSTEM_ACTIVITY_TYPE_PLAYED_FRIENDS_PUBLISHED_CAPTURE(GET_CURRENT_CONTENT_MADE_BY_A_FRIEND_NAME())
					TEXT_LABEL_63 EmptyTL = ""
					SET_CURRENT_CONTENT_MADE_BY_A_FRIEND(FALSE, EmptyTL)
				ENDIF
			ELIF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_LTS
				INT IncrementBy = INCREMENT_ENDING_ACTIVITY_CHEAT_STAT_BY()
				NET_NL()NET_PRINT("[RCC MISSION] [BCCHEAT] MPPLY_LTS_CHEAT_END Being Incremented in MC_SCRIPT_CLEANUP. ")
				INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_LTS_END)
				INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_LTS_CHEAT_END,IncrementBy )
				IF IncrementBy > 0
					INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_ACTIVITY_ENDED, 1)
				ENDIF
			ELIF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_HEIST
				INT IncrementBy = INCREMENT_ENDING_ACTIVITY_CHEAT_STAT_BY()
				NET_NL()NET_PRINT("[RCC MISSION] [BCCHEAT] MPPLY_HEIST_CHEAT_END Being Incremented in MC_SCRIPT_CLEANUP. ")
				INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_END)
				INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_HEIST_CHEAT_END,IncrementBy)
				IF IncrementBy > 0
					INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_ACTIVITY_ENDED, 1)
				ENDIF
			ELSE
				NET_NL()NET_PRINT("[RCC MISSION] [BCCHEAT] MPPLY_MC_CHEAT_END Being Incremented in MC_SCRIPT_CLEANUP. ")
				INT IncrementBy = INCREMENT_ENDING_ACTIVITY_CHEAT_STAT_BY()
				INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_MC_OVER)
				INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_MC_CHEAT_END, IncrementBy)
				IF IncrementBy > 0
					INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_ACTIVITY_ENDED, 1)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
	AND iTeamSlotOvertimeCache > -1
		CLEAR_PLAYER_NEXT_RESPAWN_LOCATION()
		CLEAR_SPECIFIC_SPAWN_LOCATION() 
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TRADING_PLACES_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
	AND bQuit
		PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - Player quit, broadcast to update their appearance.")
		BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_PLAYER_CHANGED_APPEARANCE, ALL_PLAYERS())
	ENDIF
	
	g_bFM_ON_TEAM_MISSION = FALSE
//	g_b_ShowCircleExit = FALSE
	g_i_ActualLeaderboardPlayerSelected = 0
	//CLEAR_MP_DECORATOR_BIT(PLAYER_ID(), MP_DECORATOR_BS_INVALID_TO_SPECTATE)
	SET_BIG_MESSAGE_ENABLED_WHEN_SPECTATING(FALSE)
	
	SET_PLAYER_LOCKON(PLAYER_ID(), TRUE)
	
	SET_EVERYONE_IGNORE_PLAYER(PLAYER_ID(),FALSE)
	
	//CLEANUP_NETWORK_CHAT()
	
	//1611432
	//DELETE_ALL_STUNT_JUMPS()
	IF NETWORK_IS_GAME_IN_PROGRESS()
		CLEAR_BIT(GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_ON_SECOND_PART_OF_STRAND)
		//Force the player to respawn
		IF g_sFMMCEOM.iVoteStatus =  ciFMMC_EOM_VOTE_STATUS_ROUNDS_MISSION
		OR IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
			IF IS_PED_INJURED(PLAYER_PED_ID())		
			OR NOT IS_NET_PLAYER_OK(PLAYER_ID())
			OR IS_PLAYER_RESPAWNING(PLAYER_ID())
				IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
				AND GET_INTERIOR_FROM_ENTITY(LocalPlayerPed) = NULL
					SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_AUTOMATIC)
					PRINTLN("[TS] [MSROUND] - RESET_FMMC_MISSION_VARIABLES - SPAWN_LOCATION_AUTOMATIC - SET_PLAYER_NEXT_RESPAWN_LOCATION - FORCE_RESPAWN(TRUE)")
				ELSE
	  				SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_AT_CURRENT_POSITION)
					PRINTLN("[TS] [MSROUND] - RESET_FMMC_MISSION_VARIABLES - SPAWN_LOCATION_AT_CURRENT_POSITION - SET_PLAYER_NEXT_RESPAWN_LOCATION - FORCE_RESPAWN(TRUE)")
				ENDIF
				FORCE_RESPAWN(TRUE)
			#IF IS_DEBUG_BUILD
			ELSE 
				PRINTLN("[TS] [MSROUND] - RESET_FMMC_MISSION_VARIABLES -  IS_PED_INJURED(PLAYER_PED_ID()) = FALSE")
				#ENDIF
			ENDIF
		ENDIF
	#IF IS_DEBUG_BUILD
	ELSE
		IF IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
		AND g_sFMMCEOM.iVoteStatus != ciFMMC_EOM_VOTE_STATUS_ROUNDS_MISSION
		AND g_sFMMCEOM.iVoteStatus != ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART
			PRINTLN("[TS] [MSROUND] - RESET_FMMC_MISSION_VARIABLES - SPAWN_LOCATION_AT_CURRENT_POSITION - SET_PLAYER_NEXT_RESPAWN_LOCATION - FORCE_RESPAWN(TRUE)")
		ENDIF
	#ENDIF
	ENDIF	
	
	BOOL bJoinSpectator = DID_I_JOIN_MISSION_AS_SPECTATOR()//url:bugstar:4744458
	
	RESET_FMMC_MISSION_VARIABLES(TRUE,FALSE,iMissionResult)	
	
	// Crowd control cleanup and data reset
	CLEANUP_CROWD_CONTROL_MINIGAME( sCCLocalData, sCCLocalPedData )
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND NETWORK_IS_HOST_OF_THIS_SCRIPT()
		RESET_CROWD_CONTROL_SERVER_DATA( MC_serverBD_3.sCCServerData )
	ENDIF
	
	RESET_CROWD_CONTROL_PED_DECOR_DATA( sCCLocalPedDecorCopy )
	RESET_CROWD_CONTROL_LOCAL_DATA( sCCLocalData )
	RESET_CROWD_CONTROL_LOCAL_PED_DATA( sCCLocalPedData )
	
	//If the player is dead and on a playlist then force them to respawn
	IF NETWORK_IS_GAME_IN_PROGRESS()
		
		IF NOT bStrand
			PRINTLN("[TS] [MSRAND] - NETWORK_SET_IN_MP_CUTSCENE(FALSE, FALSE)")
			NETWORK_SET_IN_MP_CUTSCENE(FALSE, FALSE)
		ENDIF
		
		IF IS_PLAYER_ON_A_PLAYLIST_INT(NATIVE_TO_INT(PLAYER_ID()))
			IF NOT IS_NET_PLAYER_OK(PLAYER_ID())	
			OR IS_PLAYER_RESPAWNING(PLAYER_ID())
		  		SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_AT_CURRENT_POSITION)
				FORCE_RESPAWN(TRUE)
			ENDIF
		ENDIF	
		//Force the player to respawn
		IF g_sFMMCEOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART
		OR g_sFMMCEOM.iVoteStatus =  ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART_SWAP
			IF IS_PED_INJURED(PLAYER_PED_ID())		
			OR NOT IS_NET_PLAYER_OK(PLAYER_ID())
			OR IS_PLAYER_RESPAWNING(PLAYER_ID())
		  		SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_AT_CURRENT_POSITION)
				FORCE_RESPAWN(TRUE)
			ENDIF
		ENDIF
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[TS] [MSROUND] - RESET_FMMC_MISSION_VARIABLES - SPAWN_LOCATION_AT_CURRENT_POSITION - SET_PLAYER_NEXT_RESPAWN_LOCATION - FORCE_RESPAWN(TRUE)")
	#ENDIF
	ENDIF	
	
	CLEANUP_POLICE()
	
	SET_FAKE_WANTED_LEVEL(0)
	
	SET_BLOCK_WANTED_FLASH(FALSE)
	FORCE_OFF_WANTED_STAR_FLASH(FALSE)
	SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_ForceBlipSecurityPedsIfPlayerIsWanted, FALSE)
	
	ALLOW_EVASION_HUD_IF_DISABLING_HIDDEN_EVASION_THIS_FRAME(PLAYER_ID(), -1.0)	//Reset Cop Report Delay
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet,ciTRIGGER_ALARMS_ON_WANTED)
		IF IS_BIT_SET(iLocalBoolCheck4,LBOOL4_TRIGGER_ALARMS)
			CLEAR_BIT(iLocalBoolCheck4,LBOOL4_TRIGGER_ALARMS)
			STOP_ALARM("PRISON_ALARMS",TRUE)
			SET_AMBIENT_ZONE_STATE("AZ_COUNTRYSIDE_PRISON_01_ANNOUNCER_GENERAL", TRUE, TRUE)
			SET_AMBIENT_ZONE_STATE("AZ_COUNTRYSIDE_PRISON_01_ANNOUNCER_ALARM" , FALSE, TRUE)
		ENDIF
	ENDIF
	
	IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
	AND NOT IS_CORONA_INITIALISING_A_QUICK_RESTART()
	AND NOT IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
		SET_TIME_OF_DAY(TIME_OFF, TRUE)
	ENDIF
	
	IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		CLEAR_OVERRIDE_WEATHER()
	ENDIF
		
	// Reset afterlife state (411389)
	RESET_GAME_STATE_ON_DEATH() 

	// Player	
	IF NOT IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING()
		PRINTLN("[RCC MISSION] Cleaning up team and friendly fire")
		
		FOR i = 0 TO (FMMC_MAX_TEAMS-1)
			IF NETWORK_IS_GAME_IN_PROGRESS()
	        	SET_PED_CAN_BE_TARGETTED_BY_TEAM(PLAYER_PED_ID(), i, TRUE)
			ENDIF
			g_sMission_TeamName[i] = NULL_STRING()
	    ENDFOR
		
		// Returns FALSE by the cleanup stage
		// Cleanup team targetting
		PRINTLN("[RCC MISSION] NETWORK_SET_FRIENDLY_FIRE_OPTION(TRUE)")
		//NETWORK_SET_FRIENDLY_FIRE_OPTION(TRUE)
		SET_PLAYER_DEFAULT_FRIENDLY_FIRE_OPTION()	
		//NETWORK_SET_LOCAL_PLAYER_INVINCIBLE_TIME(0)
		
		IF NOT IS_PLAYER_SCTV(PLAYER_ID())
			PRINTLN("[RCC MISSION] SET_PLAYER_TEAM(PLAYER_ID(), -1)")
			SET_PLAYER_TEAM(PLAYER_ID(), -1)
		ENDIF
		
		//changed by Brenda 05/03/2012
		REFRESH_ALL_OVERHEAD_DISPLAY()
		g_bDoMCTeamCleanup = FALSE
	ELSE
		g_bDoMCTeamCleanup = TRUE
		PRINTLN("[RCC MISSION] Not Cleaning up team and friendly fire due to strand")
	ENDIF
	
	SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_WillFlyThroughWindscreen, TRUE) 
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	//	ELSE
		//IF bFinalCleanUp = FALSE
		//	NET_SET_PLAYER_CONTROL(player_ID(),FALSE,TRUE,TRUE,TRUE,FALSE,FALSE)
	//	ENDIF
	ENDIF
	
//	IF IS_BIT_SET(iLocalBoolCheck,LBOOL_WEAPONS_REMOVED)
//		REMOVE_ALL_WEAPONS()
//		SET_PED_WEAPONS_MP(PLAYER_PED_ID(), structWeaponInfo)
//	ENDIF
	
	RESET_SAFE_CRACK( SafeCrackData, FALSE )
	CLEAR_SAFE_CRACK_SEQUENCES(SafeCrackData)
	
	// Emergency services
	SET_MAX_WANTED_LEVEL(5)
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	//SET_FAKE_WANTED_LEVEL(0)
	RESET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID())
	
	IF NOT (IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING() AND CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE())
		PRINTLN("[RCC MISSION] Turning dispatch services back on.")
		ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)			
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)	
		SET_DISPATCH_SERVICES(TRUE, FALSE)
		g_bDoMCDispatchCleanup = FALSE
	ELSE
		PRINTLN("[RCC MISSION] Blocking dispatch for casino airlock")
		SET_DISPATCH_SERVICES(FALSE, TRUE)
		g_bDoMCDispatchCleanup = TRUE
	ENDIF
	
	// HUD
	IF NOT IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING()
		SET_WIDESCREEN_BORDERS(FALSE, -1)
		DISPLAY_RADAR(TRUE)
		DISPLAY_HUD(TRUE)
		UNLOCK_MINIMAP_ANGLE()
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck33, LBOOL33_SET_IGNORED_WHEN_WANTED)
		SET_PED_COMBAT_ATTRIBUTES(LocalPlayerPed, CA_IGNORED_BY_OTHER_PEDS_WHEN_WANTED, FALSE)
		CLEAR_BIT(iLocalBoolCheck33, LBOOL33_SET_IGNORED_WHEN_WANTED)
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck33, LBOOL33_SET_TIME_TO_LOSE_COPS_CLEAR_MODE)
		RESET_WANTED_LEVEL_HIDDEN_ESCAPE_TIME(LocalPlayer)
		CLEAR_BIT(iLocalBoolCheck33, LBOOL33_SET_TIME_TO_LOSE_COPS_CLEAR_MODE)
	ENDIF
	
	SET_KILLSTRIP_AUTO_RESPAWN(FALSE)
	
	CLEAR_CELEBRATION_MISC_POST_FX()
	CLEAR_CELEBRATION_FAIL_POST_FX()
	IF NOT DOES_SCRIPT_EXIST("Celebrations")
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Celebrations")) = 0
			CLEAR_CELEBRATION_PASS_POST_FX()
			SET_CELEB_BLOCK_RESURRECT_RENDERPHASE_UNPAUSE(FALSE)
		ENDIF
	ENDIF
	
	// (414073)
	IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
		Enable_MP_Comms()
	ENDIF
	ENABLE_SELECTOR()
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()	
		CLEAR_ADDITIONAL_TEXT(MISSION_TEXT_SLOT,TRUE)
		CLEAR_ADDITIONAL_TEXT(DLC_MISSION_DIALOGUE_TEXT_SLOT,TRUE)
		CLEAR_ADDITIONAL_TEXT(MINIGAME_TEXT_SLOT,TRUE)
	ELSE
		PRINTLN("[RCC MISSION] NOT CALLING CLEAR_ADDITIONAL_TEXT(MISSION_TEXT_SLOT)")
	ENDIF
	DISABLE_SCRIPT_HUD(HUDPART_RANKBAR, FALSE)
	ENABLE_ALL_MP_HUD()
	
	IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		IF NOT SHOULD_LAUNCH_HEIST_TUTORIAL_MID_STRAND_CUT_SCENE() 
			IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerHeistwinnerCutscene) // If we have trigged a stand alone celebration, we don't want to cleanup here, the stand alone celebration script will handle that.
				PRINTLN("[NETCELEBRATION] - [RCC MISSION] - CLEANUP_CELEBRATION_SCREEN call 0.")
				CLEANUP_CELEBRATION_SCREEN(sCelebrationData, TRUE, "SUMMARY", bJoinSpectator)
				RESET_WEATHER_AND_TIME_AFTER_CELEBRATION_WINNER() //In case the script terminated before the winner screen cleaned up.
				SET_CELEBRATION_SCREEN_AS_ACTIVE(FALSE)
				SET_CHAT_ENABLED_FOR_CELEBRATION_SCREEN(FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	// Stop the players being invisible
	IF IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
		IF NOT NETWORK_IS_TUTORIAL_SESSION_CHANGE_PENDING()
			IF NETWORK_IS_IN_TUTORIAL_SESSION()
				NETWORK_END_TUTORIAL_SESSION()
				#IF IS_DEBUG_BUILD
					PRINTSTRING("[RCC MISSION]  - NETWORK_END_TUTORIAL_SESSION")PRINTNL()	
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF bFinalCleanUp
			NET_PRINT_TIME() NET_PRINT_STRINGS("[RCC MISSION]     MC_SCRIPT_CLEANUP  bFinalCleanUp = TRUE   ", tl31ScriptName) NET_NL()
		ELSE
			NET_PRINT_TIME() NET_PRINT_STRINGS("[RCC MISSION]     MC_SCRIPT_CLEANUP  bFinalCleanUp = FALSE   ", tl31ScriptName) NET_NL()
		ENDIF

	#ENDIF	
	
	IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_HIDE_RADAR)
		DISPLAY_RADAR(TRUE)
		CLEAR_BIT(iLocalBoolCheck6, LBOOL6_HIDE_RADAR)
	ENDIF
		
	IF interiorWarp[g_FMMC_STRUCT.iBuildingToWarpTo] != NULL
		UNPIN_INTERIOR(interiorWarp[g_FMMC_STRUCT.iBuildingToWarpTo])
		interiorWarp[g_FMMC_STRUCT.iBuildingToWarpTo] = NULL
		
		IF  NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD.niAptDoor)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD.niAptDoor)
				DELETE_NET_ID(MC_serverBD.niAptDoor)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(objDoorBlocker)
			DELETE_OBJECT(objDoorBlocker)
		ENDIF
	ENDIF
	
	CLEAR_GPS_FLAGS()
	
	//Clear the Quick GPS
	CLEAN_QUICK_GPS()
	//Force Clean all waypoints
	SET_WAYPOINT_OFF()
	//Clear Hint Cam
	CLEAN_HINT_CAM()
	
	//CLEANING DISPLACED INTERIOR DATA
	IF bIsDisplacedInteriorSet
		CLEAR_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA()
		bIsDisplacedInteriorSet = FALSE
	ENDIF

	ENABLE_SPECTATOR_FADES()
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
		CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_SCTV_IN_MOCAP)
		PRINTLN("[RCC MISSION] GLOBAL_SPEC_BS_SCTV_IN_MOCAP CLEARED")
	ENDIF
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bBlockSpawnFadeInForMissionFail = FALSE
	PRINTLN("[RCC MISSION] - [SAC] - [spawning] - setting bBlockSpawnFadeInForMissionFail = FALSE.")
	
	RESET_PHOTO_DATA()
	//CLEANUP_PHOTO_TRACKED_POINT()
	
	SET_PLAYER_BLOCKED_FROM_EXITING_VEHICLE_IN_MISSION_FLAG(FALSE)
	
	ENABLE_MULTIPLAYER_TRACKIFY_APP(FALSE)
	
	IF bFinalCleanUp
		g_sTransitionSessionData.bJustDoneMission = TRUE // Dave W
		PRINTLN("[RCC MISSION] [dsw] Mission controller cleanup set bJustDoneMission")
	ENDIF
	
	SET_PED_DROPS_WEAPONS_WHEN_DEAD(PLAYER_PED_ID(), TRUE)
	
	//reset wasted text to original after sudden death penned in
	CHANGE_WASTED_SHARD_TEXT(FALSE)
	SET_BIG_MESSAGE_SUPPRESS_WASTED(FALSE)
		
	IF IS_BIT_SET(iLocalBoolCheck19, LBOOL19_RACE_SPLIT_TIMES_STARTED)
		SCRIPT_RACE_SHUTDOWN()
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_POWER_PLAY(g_FMMC_STRUCT.iAdversaryModeType)
		IF NOT IS_BIT_SET(iLocalBoolCheck17, LBOOL17_POWER_PLAY_HUD_HAS_BEEN_CLEARED_UP)
			IF bPowerPlayScaleFormActive
				SET_POWER_PLAY_SHARD_VISIBLE(FALSE)
				
				SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(SF_PPH_MovieIndex)
				SET_BIT(iLocalBoolCheck17, LBOOL17_POWER_PLAY_HUD_HAS_BEEN_CLEARED_UP)
			ENDIF
		ENDIF
		g_iVersusOutfitStyleChoice = -2
		g_iVersusOutfitStyleSetup = -2
	ENDIF
	
	g_bFinishedExitingOfSMPLinteriorOnHeist = FALSE
	PRINTLN("[RCC MISSION] Cleaning up g_bFinishedExitingOfSMPLinteriorOnHeist")
	
	ALLOW_PLAYER_TO_PICK_UP_RAGE_COLLECTABLES(TRUE)
	
	g_bFMMCLightsTurnedOff = FALSE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciOPTION_VEHICLE_WEAPON_ENABLED)
		CLEANUP_VEHICLE_WEAPONS()
	ENDIF
	
	SET_SPEED_BOOST_EFFECT_DISABLED(FALSE)	
		
	INT iObj
	FOR iObj = 0 TO FMMC_MAX_NUM_OBJECTS - 1
		CLEAN_UP_BEAM_HACK_MINIGAME(sBeamhack[iobj], TRUE)
		CLEAN_UP_HOTWIRE_MINIGAME(sHotwire[iobj], TRUE)
		CLEAN_UP_FINGERPRINT_CLONE(sFingerprintCloneGameplay, sFingerprintClone[iobj], TRUE, HASH("heist"), g_HeistTelemetryData.iHeistPosixTime)
		CLEAN_UP_ORDER_UNLOCK(sOrderUnlockGameplay, sOrderUnlock[iobj], TRUE, HASH("heist"), g_HeistTelemetryData.iHeistPosixTime)
	ENDFOR
	
	IF NOT IS_PED_INJURED(LocalPlayerPed)
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DisableVehicleCombat, FALSE)
		SET_PED_CAN_SWITCH_WEAPON(LocalPlayerPed, TRUE)
	ENDIF
	
	SET_MISSION_CONTROLLER_RACE_TYPE(-1)
	
	SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisableShallowWaterBikeJumpOutThisFrame, FALSE)
	
	IF bShouldProcessCokeGrabHud
		REMOVE_PLAYER_BAG()
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciDISABLE_AMBIENT_VFX)
		DISABLE_REGION_VFX(FALSE)
	ENDIF
	
	IF IS_NET_PLAYER_OK(LocalPlayer)
		SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(LocalPlayer, DRAG_RESET)
	ENDIF
	
	IF IS_THIS_TRANSITION_SESSION_A_PLAYLIST()
	AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciDISGUISE_TEAM_SELECTION)
		PRINTLN("[KH][RCC MISSION] MC_SCRIPT_CLEANUP - Setting player back into freemode outfit as we're on a playlist and ciDISGUISE_TEAM_SELECTION is set")
		SET_PLAYER_CLOTHES_FOR_RETURN_TO_FREEMODE()
	ENDIF
	
	IF WVM_FLOW_IS_CURRENT_MISSION_WVM_MISSION()
		g_bLastMissionWVM = TRUE
	ELSE
		g_bLastMissionWVM = FALSE
	ENDIF
	
	SET_FAKE_MINIMAP_MAX_ALTIMETER_HEIGHT(0)
	
	MPGlobalsAmbience.bIgnoreInvalidToSpectateBail = FALSE
	MPGlobalsAmbience.bTeamTurnsSpectateCheck		= FALSE
	
	SET_LOCAL_PLAYER_ARROW_TO_CURRENT_HUD_COLOUR(FALSE)
	
	g_iFMMCScriptedCutscenePlaying = -1
	
	g_bReBlockSeatSwapping = FALSE
	
	g_EnableKersHelp = FALSE
	
	IF DOES_RELATIONSHIP_GROUP_EXIST(relDecoyPed)
		REMOVE_RELATIONSHIP_GROUP(relDecoyPed)
	ENDIF
	
	RESET_DISPATCH_SPAWN_LOCATION()
	
	IF IS_NET_PLAYER_OK(LocalPlayer)
		SET_PLAYER_PARACHUTE_SMOKE_TRAIL_COLOR(LocalPlayer, iCachedParachuteSmokeRValue, iCachedParachuteSmokeGValue, iCachedParachuteSmokeBValue)
	ENDIF
	
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS() = LocalPlayer
		IF IS_CREATOR_AIRCRAFT_ENTRY_BLOCKED(LocalPlayer)
			BLOCK_CREATOR_AIRCRAFT_INTERIOR_ENTRY(FALSE)
		ENDIF
	ENDIF
	
	IF IS_SIMPLE_INTERIOR_EXIT_BLOCKED()
		BLOCK_SIMPLE_INTERIOR_EXIT(FALSE)
	ENDIF
	
	SET_ARMORY_AIRCRAFT_AUTOPILOT_ACTIVE(FALSE)
	
	IF DOES_ENTITY_EXIST(oiHangarWayfinding)
		DELETE_OBJECT(oiHangarWayfinding)
	ENDIF
	
	SET_VEHICLE_DETONATION_MODE(FALSE)
	SET_VEHICLE_SHUNT_ON_STICK(FALSE)
	
	IF IS_ARENA_WARS_JOB()
	AND NOT IS_PLAYER_SPECTATING(LocalPlayer)
		ARENA_CONTESTANT_TURRET_CLEANUP(arenaTurretContext)
	ENDIF
	ARENA_CONTESTANT_TURRET_STACK_CLEAR()
	
	PLAYSTATS_STOP_TRACKING_STUNTS()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Enable_Spectator_Arena_Roaming)
		SET_SPECIAL_SPECTATOR_MODES_FOR_INSTANCE_CONTENT_STATE(SSS_CLEANUP)
	ENDIF
	
	g_bBlockHintCamUsageInMPMission = FALSE
	
	IF IS_ARENA_WARS_JOB()
		CLEAR_AREA_OF_VEHICLES(<<2800.0, -3800.5, 150.0>>, 200.0)
		PRINTLN("[ARENA] MC_SCRIPT_CLEANUP - Doing huge clear area with radius of 200")
		g_TransitionSessionNonResetVars.sPostMissionCleanupData.bArenaPostMissionSpawn = TRUE
		PRINTLN("[ARENA][MC] g_TransitionSessionNonResetVars.sPostMissionCleanupData.bArenaPostMissionSpawn - TRUE")
	ENDIF
	
	// For Arena modes, clear vehicle winner name
	RELEASE_REMOTE_PLAYER_ARENA_VEHICLE_NAME_DATA()
	
	TOGGLE_HIDE_CASINO_FRONT_DOOR_IPL(FALSE)
	
	CLEAN_UP_ELEVATOR_CAMERA()
	
	PROCESS_RESET_AIRLOCK_HUD_VARS()
	g_iAirlockHudDrawnFrame = GET_FRAME_COUNT() //Stop it running again this frame
	g_TransitionSessionNonResetVars.iMissionControllerTerminateTime = (GET_GAME_TIMER() / 1000)
		
	#IF IS_DEBUG_BUILD
	PASS_OVER_PREVIOUS_DEBUG_WINDOW_INFORMATION(ciCONTENT_OVERVIEW_DEBUG_WINDOW_SCRIPT__MC, sContentOverviewDebug.eDebugWindow, sContentOverviewDebug.eDebugWindowPrevious)	
	#ENDIF
		
	TERMINATE_THIS_MULTIPLAYER_THREAD(MC_serverBD.TerminationTimer)

ENDPROC



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: CELEBRATION SCREEN SETUP !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************

FUNC BOOL CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST(INT iParticipant, INT iLbdCount, PLAYER_INDEX &playerId)
	
	BOOL bDoStandardChecks = TRUE
	playerId = INVALID_PLAYER_INDEX()
	
	IF IS_THIS_A_ROUNDS_MISSION()
		IF SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
			PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - IS_THIS_A_ROUNDS_MISSION = TRUE, SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD = TRUE")
			IF g_sTransitionSessionData.sMissionRoundData.piWinner != INVALID_PLAYER_INDEX()
			AND NETWORK_IS_PLAYER_ACTIVE(g_sTransitionSessionData.sMissionRoundData.piWinner)
				INT iWinnerTeam = GET_PLAYER_TEAM(g_sTransitionSessionData.sMissionRoundData.piWinner)
				PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - g_sTransitionSessionData.sMissionRoundData.piWinner != IVALID_PLAYER_INDEX()")
				PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - g_sTransitionSessionData.sMissionRoundData.piWinner = ", NATIVE_TO_INT(g_sTransitionSessionData.sMissionRoundData.piWinner))
				PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - g_sTransitionSessionData.sMissionRoundData.piWinner team = ", iWinnerTeam)
				bDoStandardChecks = FALSE
				IF MC_Playerbd[iParticipant].iteam = iWinnerTeam
					PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - MC_Playerbd[", iParticipant, "].iteam = ", MC_Playerbd[iParticipant].iteam)
					PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - teams are the same - returning TRUE.")
					playerId = NETWORK_GET_PLAYER_INDEX(INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant))
					RETURN TRUE
				ENDIF
				IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_CONDEMNED(g_FMMC_STRUCT.iAdversaryModeType)
				AND DOES_TEAM_LIKE_TEAM(MC_Playerbd[iParticipant].iteam, iWinnerTeam)
					PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - MC_Playerbd[", iParticipant, "].iteam = ", MC_Playerbd[iParticipant].iteam)
					PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - DOES_TEAM_LIKE_TEAM = TRUE - returning TRUE.")
					playerId = NETWORK_GET_PLAYER_INDEX(INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant))
					RETURN TRUE
				ENDIF
				PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - MC_Playerbd[", iParticipant, "].iteam is not same team as winner and does not like team of winner, not adding.")
			ELSE
				IF NOT SHOULD_END_ROUNDS_MISSION_DUE_TO_TEAM_LEAVING()
					SCRIPT_ASSERT("At end of rounds mission not ended for team leaving and sMissionRoundData.piWinner = invalid player")
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD = FALSE")
		ENDIF
	ELSE
		PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - IS_THIS_A_ROUNDS_MISSION = FALSE")
	ENDIF
	
	IF bDoStandardChecks
		PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - bDoStandardChecks is still true, doing standard checks...")
		IF MC_Playerbd[iParticipant].iteam > -1
			PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - MC_Playerbd[", iParticipant, "].iteam = ", MC_Playerbd[iParticipant].iteam)
			IF MC_Playerbd[iParticipant].iteam = MC_serverBD.iWinningTeam
				PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - MC_Playerbd[", iParticipant, "].iteam = ", MC_Playerbd[iParticipant].iteam)
				PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - teams are the same")
				IF HAS_TEAM_PASSED_MISSION(MC_Playerbd[iParticipant].iteam) //1643001 - Don't include top scorers if they failed the mission.
				OR MC_serverBD.eCurrentTeamFail[MC_PlayerBD[iParticipant].iTeam] = mFail_ALL_TEAMS_FAIL // used only in special cases when no winner is declared url:bugstar:3588936
					PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - MC_Playerbd[", iParticipant, "].iteam - HAS_TEAM_PASSED_MISSION is TRUE, returning TRUE")
					playerId = GET_PLAYER_IN_LEADERBOARD_POS(iLbdCount)
					RETURN TRUE
				ELSE
					PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - MC_Playerbd[", iParticipant, "].iteam has not passed mission")
				ENDIF
			ELSE
				PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - MC_Playerbd[", iParticipant, "].iteam is not same team as winner")
			ENDIF
			IF DOES_TEAM_LIKE_TEAM(MC_Playerbd[iParticipant].iteam, MC_serverBD.iWinningTeam)
				PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - MC_Playerbd[", iParticipant, "].iteam = ", MC_Playerbd[iParticipant].iteam)
				PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - DOES_TEAM_LIKE_TEAM = TRUE")
				IF HAS_TEAM_PASSED_MISSION(MC_Playerbd[iParticipant].iteam) //1643001 - Don't include top scorers if they failed the mission.
				OR MC_serverBD.eCurrentTeamFail[MC_PlayerBD[iParticipant].iTeam] = mFail_ALL_TEAMS_FAIL // used only in special cases when no winner is declared url:bugstar:3588936
					PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - MC_Playerbd[", iParticipant, "].iteam - HAS_TEAM_PASSED_MISSION is TRUE, returning TRUE")
					playerId = GET_PLAYER_IN_LEADERBOARD_POS(iLbdCount)
					RETURN TRUE
				ELSE
					PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - MC_Playerbd[", iParticipant, "].iteam has not passed mission")
				ENDIF
			ELSE
				PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - MC_Playerbd[", iParticipant, "].iteam does not like winning team")
			ENDIF
		ENDIF
	ENDIF
	
	PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST - participant ", iParticipant, " has not passed checks, not adding to list of winners")
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Creates a list of players that were on the winning team, in order of their position (if it's not a team event then just one player is stored).
FUNC BOOL GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN(PLAYER_INDEX &winningPlayers[MAX_NUM_CELEBRATION_PEDS], INT &iNumberPlayersFound)
	
	BOOL bArenaAnims

	IF CONTENT_IS_USING_ARENA()
//		IF ( GET_CELEB_TYPE(serverBDpassed) = CELEBRATION_ARENA_ANIM_PODIUM )
//		OR ( GET_CELEB_TYPE(serverBDpassed) = CELEBRATION_ARENA_ANIM_FLAT )
			bArenaAnims = TRUE
//		ENDIF
	ENDIF
	
	INT iNumPlayersFound = 0
	INT i = 0
	
	IF IS_A_STRAND_MISSION_BEING_INITIALISED()
		RETURN FALSE
	ENDIF
	
	//First wipe the list (default value is zero which is a valid player index).
	REPEAT COUNT_OF(winningPlayers) i
		winningPlayers[i] = INVALID_PLAYER_INDEX()
	ENDREPEAT
	
	IF Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)
	OR Is_Player_Currently_On_MP_Versus_Mission(LocalPlayer)		
	OR Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer)
	OR IS_LOCAL_PLAYER_ON_A_NEW_AVERSAY()
	OR bArenaAnims
		PLAYER_INDEX currentPlayer
		REPEAT COUNT_OF(g_MissionControllerserverBD_LB.sleaderboard) i
			IF iNumPlayersFound < COUNT_OF(winningPlayers)
				INT iParticipant = g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant
				
				#IF IS_DEBUG_BUILD
				INT iPlayer = NATIVE_TO_INT(g_MissionControllerserverBD_LB.sleaderboard[i].playerID)
				STRING strPlayerName
				IF g_MissionControllerserverBD_LB.sleaderboard[i].playerID != INVALID_PLAYER_INDEX()
					IF NETWORK_IS_PLAYER_ACTIVE(g_MissionControllerserverBD_LB.sleaderboard[i].playerID)
						strPlayerName = GET_PLAYER_NAME(g_MissionControllerserverBD_LB.sleaderboard[i].playerID)
					ENDIF
				ENDIF
				#IF IS_DEBUG_BUILD
				IF (GET_FRAME_COUNT() % 100) = 0
					PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - **************************************** ", i)
					PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - leaderboard index: ", i)
					PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - participant at index: ", iParticipant)
					PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - player ID at index: ", iPlayer)
					PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - player name at index: ", strPlayerName)
				ENDIF
				#ENDIF
				IF iParticipant > (-1)
					#IF IS_DEBUG_BUILD
					IF (GET_FRAME_COUNT() % 100) = 0
						PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - MC_Playerbd[iParticipant].iteam: ", MC_Playerbd[iParticipant].iteam)
					ENDIF
					#ENDIF
				ENDIF
				#IF IS_DEBUG_BUILD
				IF (GET_FRAME_COUNT() % 100) = 0
					PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - MC_serverBD.iWinningTeam: ", MC_serverBD.iWinningTeam)
				ENDIF
				#ENDIF
				#ENDIF
				
				IF iParticipant > -1
					IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
						currentPlayer = INVALID_PLAYER_INDEX()
						IF CELEBRATION_SHOULD_ADD_TO_WINNING_PLAYERS_LIST(iParticipant, i, currentPlayer)
							IF NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_START_SPECTATOR)
							AND NOT IS_PLAYER_SCTV(currentPlayer)
							AND NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(currentPlayer)
					
								IF IS_NET_PLAYER_OK(currentPlayer, FALSE)
								
									#IF IS_DEBUG_BUILD
									IF (GET_FRAME_COUNT() % 100) = 0
										TEXT_LABEL_63 tlName = GET_PLAYER_NAME(currentPlayer)
										PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - Player being added to list of winners:")
										PRINTLN("[NETCELEBRATION] Name: ", tlName)
										PRINTLN("[NETCELEBRATION] Array index: ", i)
										PRINTLN("[NETCELEBRATION] iParticipant: ", iParticipant)
										PRINTLN("[NETCELEBRATION] player id: ", NATIVE_TO_INT(currentPlayer))
										PRINTLN("[NETCELEBRATION] MC_Playerbd[iParticipant].iteam: ", MC_Playerbd[iParticipant].iteam)
										PRINTLN("[NETCELEBRATION] MC_serverBD.iWinningTeam: ", MC_serverBD.iWinningTeam)
									ENDIF
									#ENDIF
									winningPlayers[iNumPlayersFound] = currentPlayer
									iNumPlayersFound++
									
									#IF IS_DEBUG_BUILD
									IF (GET_FRAME_COUNT() % 100) = 0
										PRINTLN("[NETCELEBRATION] iNumPlayersFound: ", iNumPlayersFound)
									ENDIF
									#ENDIF
									
								ELSE
									#IF IS_DEBUG_BUILD
									IF (GET_FRAME_COUNT() % 100) = 0
										PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - not grabbing player. IS_NET_PLAYER_OK = FALSE.")
									ENDIF
									#ENDIF
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
								IF (GET_FRAME_COUNT() % 100) = 0
									IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_START_SPECTATOR)
										PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - not grabbing player. IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_START_SPECTATOR) = TRUE.")
									ENDIF
									IF IS_PLAYER_SCTV(currentPlayer)
										PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - not grabbing player. IS_PLAYER_SCTV = TRUE.")
									ENDIF
									IF DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(currentPlayer)
										PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - not grabbing player. DID_PLAYER_JOIN_MISSION_AS_SPECTATOR = TRUE.")
									ENDIF
								ENDIF
								#ENDIf
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF bArenaAnims
			// Failsafe, Only 1 player remains so set them as winner
			IF iNumPlayersFound = 0
			AND NETWORK_GET_NUM_PARTICIPANTS() = 1
				winningPlayers[0] = PLAYER_ID()
				iNumPlayersFound++
				CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] bArenaAnims, Only 1 player remains so set them as winner, iNumPlayersFound = ", iNumPlayersFound)
			ENDIF
		ENDIF
		
	ELSE
		#IF IS_DEBUG_BUILD
		IF (GET_FRAME_COUNT() % 100) = 0
			PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - Not a team event, just grabbing the top player.")
		ENDIF
		#ENDIf
		IF g_MissionControllerserverBD_LB.sleaderboard[0].iParticipant > -1
			winningPlayers[0] = GET_PLAYER_IN_LEADERBOARD_POS(0)
			
			IF IS_NET_PLAYER_OK(winningPlayers[0], FALSE)
				iNumPlayersFound++
			ENDIF
		ENDIF
	ENDIF
	
	iNumberPlayersFound = iNumPlayersFound
	
	IF iNumPlayersFound > 0
		
		RETURN TRUE
		
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF (GET_FRAME_COUNT() % 100) = 0
		PRINTLN("[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - No valid players found.")
	ENDIF
	#ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL HAS_CELEBRATION_WINNER_FINISHED(BOOL bKeepDrawingAfterFinished = FALSE)	
	
	IF IS_A_STRAND_MISSION_BEING_INITIALISED()
		RETURN TRUE
	ENDIF
	
	//Don't do the celebration screen if the player is just spectating.
	IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_START_SPECTATOR)
	AND NOT IS_PLAYER_SCTV(LocalPlayer) // So we can see the winner scene on the live feed.
		RETURN TRUE
	ENDIF
	
	HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
	
	UNLOAD_IN_GAME_MISSION_SPECIFIC_SCALEFORM()
	
	BOOL bArenaAnims

	IF CONTENT_IS_USING_ARENA()
//		IF ( GET_CELEB_TYPE(serverBDpassed) = CELEBRATION_ARENA_ANIM_PODIUM )
//		OR ( GET_CELEB_TYPE(serverBDpassed) = CELEBRATION_ARENA_ANIM_FLAT )
			bArenaAnims = TRUE
//		ENDIF
	ENDIF
	
	PLAYER_INDEX winningPlayers[MAX_NUM_CELEBRATION_PEDS]
	BOOL bSuccessfullyGrabbedWinners = GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN(winningPlayers, sCelebrationData.iNumPlayerFoundForWinnerScene)	
	INT iWinnerLBDIndex = GET_LEADERBOARD_INDEX_OF_TOP_WINNER()
	INT iWinningPlayerID = NATIVE_TO_INT(GET_PLAYER_IN_LEADERBOARD_POS(iWinnerLBDIndex))
	PRINTLN("[RCC MISSION] - [NETCELEBRATION] - Grabbing winning player details from top player")
	PRINTLN("[RCC MISSION] - [NETCELEBRATION] - iWinnerLBDIndex: ", iWinnerLBDIndex, " iWinningPlayerID: ", iWinningPlayerID)
	
	TEXT_LABEL_63 tl63_AnimDict, tl63_AnimName
	TEXT_LABEL_63 tl63_IdleAnimDict, tl63_IdleAnimName
	
	IF ( NOT bSuccessfullyGrabbedWinners OR iWinningPlayerID <= (-1) )
		winningPlayers[0] = LocalPlayer
		iWinningPlayerID = NATIVE_TO_INT(LocalPlayer)
		IF NOT sCelebrationData.bCreateWinnerSceneEntities
			sCelebrationData.iDisplayingLocalPlayerAsLoser = 1
			PRINTLN("[RCC MISSION] - [NETCELEBRATION] - could not grab list of winners, setting flag to indicate we are displaying fallback loser screen.")
		ENDIF
	ELSE
		IF NOT sCelebrationData.bCreateWinnerSceneEntities
			sCelebrationData.iDisplayingLocalPlayerAsLoser = 0
			PRINTLN("[RCC MISSION] - [NETCELEBRATION] - grabbed list of winners, setting flag to indicate we are not displaying fallback loser screen.")
		ENDIF
	ENDIF
	
	IF sCelebrationData.bAllowPlayerNameToggles
		
		DRAW_THE_PLAYER_LIST(celebdpadVars)
		MAINTAIN_CELEBRATION_INSTRUCTIONAL_BUTTONS(sCelebrationData)
		CELEB_SET_PLAYERS_INVISIBLE_THIS_FRAME()
				
		IF IS_XBOX360_VERSION()
		OR IS_PS3_VERSION()
			DISPLAY_CELEBRATION_PLAYER_NAMES(sCelebrationData.pedWinnerClones, sCelebrationData.tl31_pedWinnerClonesNames, sCelebrationData.eCurrentStage, sCelebrationData.sfCelebration)
		ELSE
			DRAW_NG_CELEBRATION_PLAYER_NAMES(	sCelebrationData, sCelebrationData.iDrawNamesStage, sCelebrationData.iNumNamesToDisplay, 
												sCelebrationData.pedWinnerClones, sCelebrationData.tl31_pedWinnerClonesNames, 
												sCelebrationData.eCurrentStage, sCelebrationData.sfCelebration,
												sCelebrationData.playerNameMovies, sCelebrationData.bToggleNames)
		ENDIF
	ENDIF
	
	HIDE_PLAYERS_FOR_CELEBRATION_WINNER_SCENE(sCelebrationData) // Fix for B* 2278693 - stop player models being seen during winner scene, only want ot see ped clones.
	
	BOOL bJuggernaut
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(INT_TO_NATIVE(PLAYER_INDEX, iWinningPlayerID)))].iTeam].iTeamBitset2, ciBS2_JUGGERNAUTS)
	OR IS_BIT_SET(MC_playerBD[NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(INT_TO_NATIVE(PLAYER_INDEX, iWinningPlayerID)))].iClientBitSet3, PBBOOL3_SUDDEN_DEATH_JUGGERNAUT)
		PRINTLN("[RCC MISSION] - [NETCELEBRATION] - [JS] ciBS2_JUGGERNAUTS or PBBOOL3_SUDDEN_DEATH_JUGGERNAUT bits are set. bJuggernaut = TRUE")
		bJuggernaut = TRUE
	ELSE
		PRINTLN("[RCC MISSION] - [NETCELEBRATION] - [JS] Winning player: ", NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(INT_TO_NATIVE(PLAYER_INDEX, iWinningPlayerID))), " on team ",MC_playerBD[NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(INT_TO_NATIVE(PLAYER_INDEX, iWinningPlayerID)))].iTeam," is not the juggernaut???")
		PED_INDEX tempWinnerPed = GET_PLAYER_PED(INT_TO_NATIVE(PLAYER_INDEX, iWinningPlayerID))
		IF (NOT IS_ENTITY_DEAD(tempWinnerPed)
		AND INT_TO_ENUM(MASK_TYPES, GET_PED_DRAWABLE_VARIATION(tempWinnerPed, PED_COMP_BERD)) = MASK_TYPE_IMPORT_EXPORT_1)
		OR IS_PED_WEARING_JUGGERNAUT_SUIT(tempWinnerPed)
			bJuggernaut = TRUE
		ENDIF
	ENDIF
	
	SWITCH sCelebrationData.eCurrentStage
		CASE CELEBRATION_STAGE_SETUP
			
			CELEBRATION_SCREEN_TYPE eCelebType
			
			IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
			OR Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
			
				eCelebType = CELEBRATION_HEIST
				PRINTLN("[RCC MISSION] - [NETCELEBRATION] - USING HEIST CELEBRATION SCREEN 3")	
				

				ELIF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
				
					eCelebType = CELEBRATION_GANGOPS
					PRINTLN("[RCC MISSION] - [NETCELEBRATION] - USING GANGOPS CELEBRATION SCREEN 3")
				
				
				ELSE
			
					eCelebType = CELEBRATION_STANDARD
					PRINTLN("[RCC MISSION] - [NETCELEBRATION] - USING STANDARD CELEBRATION SCREEN 3")
				
				ENDIF
			
			REQUEST_CELEBRATION_SCREEN(sCelebrationData, eCelebType)
			
			IF LOAD_WINNER_SCENE_INTERIOR(sCelebrationData)
			
				IF bArenaAnims
				AND NOT HAS_NG_FAILSAFE_TIMER_EXPIRED(sCelebrationData)
					
					IF NOT ARENA_ANIMS_READY(MC_serverBD_2.sCelebServer)
						PRINTLN("[NETCELEBRATION] [ARENA] HAS_RACE_CELEBRATION_WINNER_FINISHED  - ARENA_ANIMS_READY = FALSE, iNumberOfRounds = ", g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds)
						PRINTLN("[NETCELEBRATION] [ARENA] HAS_RACE_CELEBRATION_WINNER_FINISHED  - ARENA_ANIMS_READY = FALSE, iNumPlayerFoundForWinnerScene = ", sCelebrationData.iNumPlayerFoundForWinnerScene)
						RETURN FALSE
					ENDIF
				ENDIF
				
				IF HAS_CELEBRATION_SCREEN_LOADED(sCelebrationData)
				AND NOT ANIMPOSTFX_IS_RUNNING("DeathFailMPIn")
				AND CREATE_WINNER_SCENE_ENTITIES(MC_serverBD_2.sCelebServer, sCelebrationData, winningPlayers, DEFAULT, NOT bSuccessfullyGrabbedWinners, NULL, FALSE, FALSE, FALSE, FALSE, DUMMY_MODEL_FOR_SCRIPT, GlobalplayerBD[iWinningPlayerID].iInteractionAnim, bJuggernaut)				
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Enable_Turn_Rotor_Tag_Team)
						IF DOES_ENTITY_EXIST(g_vehStandbyVehicle)
							PRINTLN("[RCC MISSION] - [NET_CELEBRATION] - UN GHOSTING OUR STANDBY VEHICLE!")
							/*IF NOT NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(g_vehStandbyVehicle)
								SET_ENTITY_AS_MISSION_ENTITY(g_vehStandbyVehicle, FALSE, TRUE)
							ENDIF*/
							NETWORK_INDEX niVeh
							niVeh = VEH_TO_NET(g_vehStandbyVehicle)
							INT iNetIndex
							iNetIndex = NATIVE_TO_INT(niVeh)						
							BROADCAST_FMMC_ALPHA_CHANGE(255, 255, PARTICIPANT_ID_TO_INT(), iNetIndex, GET_FRAME_COUNT())
						ELSE
							PRINTLN("[RCC MISSION] - [NET_CELEBRATION] - Standby Vehicle does not exist.")
						ENDIF
					ENDIF
						
					IF bArenaAnims
			
						GET_ARENA_CELEB_ANIMS(MC_serverBD_2.sCelebServer, tl63_AnimDict, tl63_AnimName, 0)
						
						PRINTLN("[RCC MISSION] - [NET_CELEBRATION] - initial anim loading - GET_ARENA_CELEB_ANIMS ")
						PRINTLN("[RCC MISSION] - [NETCELEBRATION] - tl63_AnimDict = tl63_IdleAnimDict = ", tl63_AnimDict)	
						PRINTLN("[RCC MISSION] - [NETCELEBRATION] - tl63_AnimName = tl63_IdleAnimName = ", tl63_AnimName)	
					ELSE
						GET_CELEBRATION_ANIM_TO_PLAY(GlobalplayerBD[iWinningPlayerID].iInteractionAnim, IS_PED_MALE(sCelebrationData.pedWinnerClones[0]), FALSE, tl63_AnimDict, tl63_AnimName, INT_TO_ENUM(INTERACTION_ANIM_TYPES, GlobalplayerBD[iWinningPlayerID].iInteractionType), INT_TO_ENUM(CREW_INTERACTIONS, GlobalplayerBD[iWinningPlayerID].iCrewAnim), bJuggernaut)
					ENDIF
					GET_CELEBRATION_IDLE_ANIM_TO_USE(sCelebrationData, tl63_IdleAnimDict, tl63_IdleAnimName, IS_PED_MALE(sCelebrationData.pedWinnerClones[0]))
					
					IF bJuggernaut
						//tl63_AnimDict = tl63_IdleAnimDict
						//tl63_AnimName = tl63_IdleAnimName
						#IF IS_DEBUG_BUILD
						
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(INT_TO_NATIVE(PLAYER_INDEX, iWinningPlayerID)))].iTeam].iTeamBitset2, ciBS2_JUGGERNAUTS)
							PRINTLN("[RCC MISSION] - [NETCELEBRATION] - IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[MC_playerBD[NETWORK_GET_PARTICIPANT_INDEX(INT_TO_NATIVE(PLAYER_INDEX, iWinningPlayerID))].iTeam].iTeamBitset2, ciBS2_JUGGERNAUTS) = TRUE")	
						ENDIF
						IF IS_BIT_SET(MC_playerBD[NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(INT_TO_NATIVE(PLAYER_INDEX, iWinningPlayerID)))].iClientBitSet3, PBBOOL3_SUDDEN_DEATH_JUGGERNAUT)
							PRINTLN("[RCC MISSION] - [NETCELEBRATION] - IS_BIT_SET(MC_playerBD[MC_playerBD[NETWORK_GET_PARTICIPANT_INDEX(INT_TO_NATIVE(PLAYER_INDEX, iWinningPlayerID))].iClientBitSet3, PBBOOL3_SUDDEN_DEATH_JUGGERNAUT) = TRUE")
						ENDIF
						PRINTLN("[RCC MISSION] - [NETCELEBRATION] - tl63_AnimDict = tl63_IdleAnimDict = ", tl63_AnimDict)	
						PRINTLN("[RCC MISSION] - [NETCELEBRATION] - tl63_AnimName = tl63_IdleAnimName = ", tl63_AnimName)	
						#ENDIF	
					ENDIF
					
					REQUEST_ANIM_DICT(tl63_AnimDict)
					REQUEST_ANIM_DICT(tl63_IdleAnimDict)
					
					IF (HAS_ANIM_DICT_LOADED(tl63_AnimDict) AND HAS_ANIM_DICT_LOADED(tl63_IdleAnimDict))
					OR HAS_NG_FAILSAFE_TIMER_EXPIRED(sCelebrationData)
						sCelebrationData.fAnimLength = GET_ANIM_DURATION(tl63_AnimDict, tl63_AnimName)
						PRINTLN("[NETCELEBRATION] - HAS_CELEBRATION_WINNER_FINISHED - fAnimLength = ", sCelebrationData.fAnimLength)
						RESET_NET_TIMER(sCelebrationData.sCelebrationTimer)
						START_NET_TIMER(sCelebrationData.sCelebrationTimer)
						RESET_NET_TIMER(sCelebrationData.sCelebrationTransitionTimer)
						START_NET_TIMER(sCelebrationData.sCelebrationTransitionTimer)
						
						STOP_CELEBRATION_SCREEN_ANIM(sCelebrationData, "SUMMARY")
						STOP_CELEBRATION_SCREEN_ANIM(sCelebrationData, "EARLYDEATH")
						FLASH_CELEBRATION_SCREEN(sCelebrationData, 0.33, 0.15, 0.33)
						DRAW_CELEBRATION_SCREEN_THIS_FRAME(sCelebrationData)

						//Cache the winner ID for later use in the anims, so that if the player leaves we still have the clone and anim array index.
						IF bSuccessfullyGrabbedWinners
						AND iWinningPlayerID > -1
							sCelebrationData.iWinnerPlayerID = iWinningPlayerID
						ENDIF

						IF IS_THIS_A_MISSION()
						AND CONTENT_IS_USING_ARENA()
						
							PRINTLN("[NETCELEBRATION] - LOAD_WINNER_SCENE_INTERIOR - sCelebrationData.iWinnerPlayerID: ", sCelebrationData.iWinnerPlayerID)
							
							INT iWinnerTeam
							PLAYER_INDEX piPlayerWinner
							piPlayerWinner = INT_TO_NATIVE(PLAYER_INDEX, sCelebrationData.iWinnerPlayerID)
							
							IF piPlayerWinner != INVALID_PLAYER_INDEX()
							AND IS_NET_PLAYER_OK(piPlayerWinner, FALSE, FALSE)
								iWinnerTeam = GET_PLAYER_TEAM(piPlayerWinner)
								PRINTLN("[NETCELEBRATION] - LOAD_WINNER_SCENE_INTERIOR - piPlayerWinner: ", GET_PLAYER_NAME(piPlayerWinner))
								PRINTLN("[NETCELEBRATION] - LOAD_WINNER_SCENE_INTERIOR - sCelebrationData.iWinnerTeam: ", iWinnerTeam)
							ELSE
								PRINTLN("[NETCELEBRATION] - LOAD_WINNER_SCENE_INTERIOR - INVALID PLAYER INDEX...")
							ENDIF	
							
							IF iWinnerTeam > -1
							AND iWinnerTeam < FMMC_MAX_TEAMS
							AND IS_VALID_INTERIOR(sCelebrationData.winnerSceneInterior)
								INT iColour
								STRING sEntitySetName
								INT i						
								FOR i = 0 TO 3
									iColour = g_FMMC_STRUCT.sArenaInfo.iArena_CrowdColour[iWinnerTeam]
									sEntitySetName = ""
									SWITCH i
										CASE 0
											sEntitySetName = "Set_Crowd_A"
										BREAK
										CASE 1
											sEntitySetName = "Set_Crowd_B"
										BREAK
										CASE 2
											sEntitySetName = "Set_Crowd_C"
										BREAK
										CASE 3
											sEntitySetName = "Set_Crowd_D"
										BREAK
									ENDSWITCH
									SET_INTERIOR_ENTITY_SET_TINT_INDEX(sCelebrationData.winnerSceneInterior, sEntitySetName, iColour)
									
									iColour = g_FMMC_STRUCT.sArenaInfo.iArena_BandColour[iWinnerTeam]
									sEntitySetName = ""
									SWITCH i
										CASE 0
											sEntitySetName = "Set_Team_Band_A"
										BREAK
										CASE 1
											sEntitySetName = "Set_Team_Band_B"
										BREAK
										CASE 2
											sEntitySetName = "Set_Team_Band_C"
										BREAK
										CASE 3
											sEntitySetName = "Set_Team_Band_D"
										BREAK
									ENDSWITCH							
									SET_INTERIOR_ENTITY_SET_TINT_INDEX(sCelebrationData.winnerSceneInterior, sEntitySetName, iColour)
									
									PRINTLN("[NETCELEBRATION] - LOAD_WINNER_SCENE_INTERIOR - Setting colour of ", sEntitySetName, " to ", iColour)
								ENDFOR
								REFRESH_INTERIOR(sCelebrationData.winnerSceneInterior)
							ENDIF
						ENDIF						
						
						PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - Started flash.")						
						
						SET_BIT(iLocalBoolCheck7, LBOOL7_BLOCK_TOD_EVERY_FRAME)						
						
						sCelebrationData.eCurrentStage = CELEBRATION_STAGE_DOING_FLASH
					ENDIF
					
				ELSE
					
					IF ANIMPOSTFX_IS_RUNNING("DeathFailMPIn")
						IF NOT HAS_NET_TIMER_STARTED(sCelebrationData.timerDeathPostFxDelay)
							START_NET_TIMER(sCelebrationData.timerDeathPostFxDelay)
							PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - DeathFailMPIn is running, started timer timerDeathPostFxDelay.")
						ELSE
							IF HAS_NET_TIMER_EXPIRED(sCelebrationData.timerDeathPostFxDelay, 1000)
								PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - DeathFailMPIn is running, timer timerDeathPostFxDelay >= 1000. Calling CLEAR_KILL_STRIP_DEATH_EFFECTS.")
								CLEAR_KILL_STRIP_DEATH_EFFECTS()
							ENDIF
						ENDIF	
					ENDIF
					
				ENDIF
				
			ENDIF
			
			//In case the transition from the previous screen is tight: keep drawing to prevent pops.
			IF HAS_CELEBRATION_SCREEN_LOADED(sCelebrationData)
				DRAW_CELEBRATION_SCREEN_THIS_FRAME(sCelebrationData)
			ENDIF
		BREAK
		
		CASE CELEBRATION_STAGE_DOING_FLASH
			DRAW_CELEBRATION_SCREEN_THIS_FRAME(sCelebrationData)
			
			IF (HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTransitionTimer, 375) OR IS_SCREEN_FADED_OUT())
				RESET_NET_TIMER(sCelebrationData.sCelebrationTimer)
				START_NET_TIMER(sCelebrationData.sCelebrationTimer)
				
				SET_SKYFREEZE_CLEAR(TRUE)
				SET_SKYBLUR_CLEAR()
				ANIMPOSTFX_STOP_ALL()
				
				IF bLocalPlayerOK
					SET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, WEAPONTYPE_VEHICLE_PLAYER_BUZZARD)
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitset2, ciBS2_JUGGERNAUTS)
					AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_SUDDEN_DEATH_JUGGERNAUT)
					AND NOT (IS_JOB_IN_CASINO() AND	IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType))
						REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE) // url:bugstar:2244903 - remove helmet mid-flash.
					ENDIF
				ENDIF
				
				IF CONTENT_IS_USING_ARENA()
					IF NOT IS_AUDIO_SCENE_ACTIVE("MP_CELEB_SCREEN_ARENA_SCENE")
						START_AUDIO_SCENE("MP_CELEB_SCREEN_ARENA_SCENE")
					ENDIF
				ELIF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciPLAY_POWERPLAY_INTRO_ANNOUNCER)
					START_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
				ELSE
					START_AUDIO_SCENE("MP_CELEB_SCREEN_PP_SCENE")
				ENDIF
				
				sCelebrationData.bUseFullBodyWinnerAnim = SHOULD_WINNER_SCREEN_USE_FULL_BODY_ANIM(sCelebrationData, iWinningPlayerID)
				
				PLACE_WINNER_SCENE_CAMERA(MC_serverBD_2.sCelebServer, sCelebrationData, camEndScreen, DEFAULT, iWinningPlayerID, sCelebrationData.bUseFullBodyWinnerAnim)
				FORCE_WEATHER_AND_TIME_FOR_CELEBRATION_WINNER()
				MC_serverBD.iTimeOfDay = TIME_OFF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciPRE_GAME_EMP)
				OR g_bFMMCLightsTurnedOff
					SET_ARTIFICIAL_LIGHTS_STATE(FALSE)
					g_bFMMCLightsTurnedOff = FALSE
					#IF IS_DEBUG_BUILD
						PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - called SET_ARTIFICIAL_LIGHTS_STATE(FALSE) for celebration winner screen.")
						DEBUG_PRINTCALLSTACK()
					#ENDIF
				ENDIF
				
				TURN_OFF_ACTIVE_FILTERS()
			
				IF GET_REQUESTINGNIGHTVISION()
				OR GET_USINGNIGHTVISION()
					ENABLE_NIGHTVISION(VISUALAID_OFF)
					DISABLE_NIGHT_VISION_CONTROLLER(TRUE) // Added under instruction of Alwyn for B*2196822
				ENDIF
		
				PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - Transitioning to winner screen.")

				sCelebrationData.eCurrentStage = CELEBRATION_STAGE_TRANSITIONING
			ENDIF
		BREAK
		
		CASE CELEBRATION_STAGE_TRANSITIONING
			
			DRAW_CELEBRATION_SCREEN_THIS_FRAME(sCelebrationData)
			HIDE_PROBLEM_MODELS_FOR_CELEBRATION_SCREEN()
			
			IF HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTimer, 375)
				SET_HIDE_PLAYERS_FOR_CELEBRATION_WINNER_SCENE(sCelebrationData)
			ENDIF
			
			IF HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTimer, 750)
				STRING strScreenName, strBackgroundColour
				TEXT_LABEL_63 strWinnerName
				TEXT_LABEL_63 strCrewName
				JOB_WIN_STATUS eJobWinStatus
				
				//Retrieve all the required stats for the race end screen.
				strScreenName = "WINNER"
				strWinnerName = ""
				strCrewName = ""
				
				IF winningPlayers[0] != INVALID_PLAYER_INDEX()
				AND g_MissionControllerserverBD_LB.sleaderboard[iWinnerLBDIndex].iParticipant > -1
				AND bSuccessfullyGrabbedWinners
					strWinnerName = GET_PLAYER_NAME(winningPlayers[0]) //MC_serverBD_2.tParticipantNames[g_MissionControllerserverBD_LB.sleaderboard[iWinnerLBDIndex].iParticipant]
					strCrewName = GET_CREW_NAME_FOR_CELEBRATION_SCREEN(winningPlayers[0])
				ENDIF
				PRINTLN("[HAS_CELEBRATION_WINNER_FINISHED][RH]1 The winning player name is ", strWinnerName)
				sCelebrationData.iEstimatedScreenDuration = CELEBRATION_SCREEN_STAT_WIPE_TIME * 2 
				
				IF sCelebrationStats.bPassedMission
					eJobWinStatus = JOB_STATUS_WIN
					strBackgroundColour = "HUD_COLOUR_FRIENDLY"
					
					PLAY_MISSION_COMPLETE_AUDIO("FRANKLIN_BIG_01")
				ELSE
					//Use the winner status in all cases except if the winner ped couldn't be retrieved.
					IF bSuccessfullyGrabbedWinners
						eJobWinStatus = JOB_STATUS_WIN
					ELSE
						eJobWinStatus = JOB_STATUS_LOSE
					ENDIF
					strBackgroundColour = "HUD_COLOUR_NET_PLAYER1"
					PLAY_MISSION_COMPLETE_AUDIO("GENERIC_FAILED")
				ENDIF
				
				//Build the screen
				CREATE_CELEBRATION_SCREEN(sCelebrationData, strScreenName, strBackgroundColour)
				SET_CELEBRATION_SCREEN_STAT_DISPLAY_TIME(sCelebrationData, CELEBRATION_SCREEN_WINNER_STAT_DISPLAY_TIME - CELEBRATION_SCREEN_STAT_WIPE_TIME)
				
				IF CONTENT_IS_USING_ARENA() 
				
					TEXT_LABEL_15 tl15Skill 
					STRING sSkillLevelTitle
					BOOL bWinnerNameIsTextLabel
					
					tl15Skill = GET_ARENA_SKILL_LEVEL_TITLE(GET_PLAYERS_CURRENT_ARENA_WARS_SKILL_LEVEL(winningPlayers[0]))	
					sSkillLevelTitle = TEXT_LABEL_TO_STRING(tl15Skill)				
					
					IF IS_STRING_NULL_OR_EMPTY(tl23WinnerVehicle)
						IF iWinnerLBDIndex != -1
							IF IS_MODEL_VALID(g_MissionControllerserverBD_LB.sleaderboard[iWinnerLBDIndex].mnVehModel)
								tl23WinnerVehicle = GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_MissionControllerserverBD_LB.sleaderboard[iWinnerLBDIndex].mnVehModel)	
								PRINTLN("ADD_ARENA_WINNER_TO_CELEBRATION_SCREEN, 5456221, tl23WinnerVehicle IS_STRING_NULL_OR_EMPTY, using default = ",tl23WinnerVehicle)
								tl23WinnerVehicle = GET_FILENAME_FOR_AUDIO_CONVERSATION(tl23WinnerVehicle)
								PRINTLN("ADD_ARENA_WINNER_TO_CELEBRATION_SCREEN, 5456221, tl23WinnerVehicle IS_STRING_NULL_OR_EMPTY, convert ",tl23WinnerVehicle)
							ENDIF
						ENDIF
					ENDIF
					
					IF MC_serverBD.iWinningTeam > -1
						strWinnerName = GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN(MC_serverBD.iWinningTeam, FALSE, FALSE, TRUE) // g_sMission_TeamName[MC_serverBD.iWinningTeam]
					ENDIF
				
					PRINTLN("ADD_ARENA_WINNER_TO_CELEBRATION_SCREEN, 5456221, tl15Skill = ", tl15Skill, " sSkillLevelTitle = ", sSkillLevelTitle, " tl23WinnerVehicle = ", tl23WinnerVehicle, " strWinnerName = ", strWinnerName)
				
					ADD_ARENA_WINNER_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, strWinnerName, (eJobWinStatus = JOB_STATUS_WIN), tl23WinnerVehicle, sSkillLevelTitle, bWinnerNameIsTextLabel, GET_PLAYERS_CURRENT_ARENA_WARS_SKILL_LEVEL(winningPlayers[0]))
				ELSE
				
					IF winningPlayers[1] != INVALID_PLAYER_INDEX() //1637254 - If multiple players won then don't show specifics on the winner display.
						strWinnerName = ""
						
						IF MC_serverBD.iWinningTeam > -1
							strWinnerName = GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN(MC_serverBD.iWinningTeam, FALSE, FALSE, TRUE) // g_sMission_TeamName[MC_serverBD.iWinningTeam]
						ENDIF
						
						PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - Adding team name: ", strWinnerName)
					
						ADD_WINNER_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, eJobWinStatus, strWinnerName, "", "", sCelebrationStats.iLocalPlayerBetWinnings, DEFAULT, TRUE, FALSE, IS_BIT_SET(iCelebrationTeamNameIsNotLiteral, MC_serverBD.iWinningTeam))
					ELSE
						ADD_WINNER_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, eJobWinStatus, "", strCrewName, strWinnerName, sCelebrationStats.iLocalPlayerBetWinnings)
					ENDIF 
					
				ENDIF
				
				
				PRINTLN("[HAS_CELEBRATION_WINNER_FINISHED][RH]2 The winning player name is ", strWinnerName)
				sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_WINNER_STAT_DISPLAY_TIME //Winner screen is guaranteed.
				
				//Add extra time if betting is shown.
				IF sCelebrationStats.iLocalPlayerBetWinnings != 0
					sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_WINNER_STAT_DISPLAY_TIME
				ENDIF
				
				sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_WIPE_TIME * 2
				
				ADD_BACKGROUND_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName)
				SHOW_CELEBRATION_SCREEN(sCelebrationData, strScreenName)
				
				ADJUST_ESTIMATED_TIME_BASED_ON_ANIM_LENGTH(sCelebrationData)
				
				RESET_NET_TIMER(sCelebrationData.sCelebrationTimer)
				START_NET_TIMER(sCelebrationData.sCelebrationTimer)
				
				RESET_CELEBRATION_PRE_LOAD(sCelebrationData)
				
				PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - Starting playback of winner screen.")
				MP_TEXT_CHAT_DISABLE(TRUE)
				sCelebrationData.eCurrentStage = CELEBRATION_STAGE_PLAYING
			ENDIF
		BREAK
		
		CASE CELEBRATION_STAGE_PLAYING			
			VECTOR vCamRot
			vCamRot = GET_FINAL_RENDERED_CAM_ROT()
		
			//Don't draw the screen if we're in skycam.
			IF ABSF(vCamRot.x) < 45.0
				
				DRAW_CELEBRATION_SCREEN_USING_3D_BACKGROUND(sCelebrationData, <<0.0, -0.5, 0.0>>, <<0.0, 0.0, 0.0>>, <<10.0, 5.0, 5.0>>)
				
				DRAW_BLACK_RECT_FOR_WINNER_SCREEN_END_TRANSITION(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sCelebrationData.sCelebrationTimer.Timer), 
																 sCelebrationData.iEstimatedScreenDuration)
																 
				//Block the switch PostFX as it comes on during the winner screen.
				ANIMPOSTFX_STOP_ALL()
				RESET_ADAPTATION(1)
			ENDIF
			
			//Once the timer finishes then progress: this depends on how many elements were added to the screen.
			IF HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTimer, sCelebrationData.iEstimatedScreenDuration)
				
				IF NOT bKeepDrawingAfterFinished
					sCelebrationData.eCurrentStage = CELEBRATION_STAGE_FINISHED
				ENDIF
				
				RESET_WEATHER_AND_TIME_AFTER_CELEBRATION_WINNER()
				
				IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
				AND NOT bArenaAnims
					VECTOR vCamCoords
					vCamCoords = GET_CAM_COORD(camEndScreen)
					vCamCoords.z = 1000.0
					SET_CAM_COORD(camEndScreen, vCamCoords)
					SET_ENTITY_COORDS(LocalPlayerPed, vCamCoords, FALSE)
					FREEZE_ENTITY_POSITION(LocalPlayerPed, TRUE)
					PRINTLN("[NETCELEBRATION] - put winner cam and player ped at ", vCamCoords)
				ENDIF
				
				sCelebrationData.bAllowPlayerNameToggles = FALSE
				MP_TEXT_CHAT_DISABLE(FALSE)
				RETURN TRUE
			
			ELSE
			
				sCelebrationData.bAllowPlayerNameToggles = TRUE
				
			ENDIF
		BREAK
		
		CASE CELEBRATION_STAGE_FINISHED
			MP_TEXT_CHAT_DISABLE(FALSE)
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	//1645969 - The anim needs to work if the winning player leaves half way through, their ID will be cached so we only need to check the following:
	// - The clone was created successfully.
	// - The clone is not the player.
	// - The clone is the player and the player won the race (i.e. we're not doing the fallback "Loser" screen).
	IF sCelebrationData.eCurrentStage > CELEBRATION_STAGE_SETUP
	AND sCelebrationData.eCurrentStage < CELEBRATION_STAGE_FINISHED
		IF sCelebrationData.iDisplayingLocalPlayerAsLoser = 0
		
			IF bArenaAnims
				// Repeat through players the podium to play their anims
				INT iLoop
				REPEAT MAX_CELEB_PLAYERS_TO_ANIMATE(MC_serverBD_2.sCelebServer) iLoop
					IF NOT IS_PED_INJURED(sCelebrationData.pedWinnerClones[iLoop])

						IF NATIVE_TO_INT(winningPlayers[iLoop]) > -1
						AND NATIVE_TO_INT(winningPlayers[iLoop]) < NUM_NETWORK_PLAYERS
							PRINTLN("[Paired_Anims] - bArenaAnims, calling UPDATE_CELEBRATION_WINNER_ANIMS for winner ", iLoop)
							UPDATE_CELEBRATION_WINNER_ANIMS_ARENA(MC_serverBD_2.sCelebServer, 
																	sCelebrationData, 
																	sCelebrationData.pedWinnerClones[iLoop], 
																	NATIVE_TO_INT(winningPlayers[iLoop]), iLoop)

						ELSE
							#IF IS_DEBUG_BUILD
							INT iTemp = NATIVE_TO_INT(winningPlayers[iLoop])
							PRINTLN("[Paired_Anims] - not calling UPDATE_CELEBRATION_WINNER_ANIMS for winner, iLoop = ", iLoop, ". winningPlayers[iLoop] = ", iTemp)
							#ENDIF
						ENDIF

					ENDIF
				ENDREPEAT
		
			ELIF DOES_ENTITY_EXIST(sCelebrationData.pedWinnerClones[0])
				IF NOT IS_PED_INJURED(sCelebrationData.pedWinnerClones[0])
					IF sCelebrationData.pedWinnerClones[0] != LocalPlayerPed
					OR (sCelebrationData.pedWinnerClones[0] = LocalPlayerPed AND bSuccessfullyGrabbedWinners)
						IF DOES_CAM_EXIST(camEndScreen)
							IF IS_CAM_RENDERING(camEndScreen)
								UPDATE_CELEBRATION_WINNER_ANIMS(sCelebrationData, sCelebrationData.pedWinnerClones[0], sCelebrationData.iWinnerPlayerID, 0, FALSE, FALSE, FALSE, PAIRED_CELEBRATION_GENDER_COMBO_NOT_SET, -1, -1, 200, FALSE, bJuggernaut)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

INT iFreezeCamForPlaceDcamStage

PROC FREEZE_SCREEN_FOR_PLACED_CAMERA()
	
	BOOL bMoveOntoNextStage
	
	SWITCH iFreezeCamForPlaceDcamStage
		
		CASE 0
			IF IS_BIT_SET(iLocalBoolCheck13, LBOOL13_NOT_SAFE_FOR_ALTERNATE_CAM)
				PRINTLN("[RCC MISSION] - FREEZE_SCREEN_FOR_PLACED_CAMERA - LBOOL13_NOT_SAFE_FOR_ALTERNATE_CAM.")
				bMoveOntoNextStage = TRUE
			ENDIF
			IF DOES_CAM_EXIST(camEndScreen)
				PRINTLN("[RCC MISSION] - FREEZE_SCREEN_FOR_PLACED_CAMERA - camEndScreen exists.")
				IF bCreatedCustomCelebrationCam
					PRINTLN("[RCC MISSION] - FREEZE_SCREEN_FOR_PLACED_CAMERA - bCreatedCustomCelebrationCam = TRUE.")
					IF IS_CAM_ACTIVE(camEndScreen)
						PRINTLN("[RCC MISSION] - FREEZE_SCREEN_FOR_PLACED_CAMERA - camEndScreen is active.")
						IF IS_CAM_RENDERING(camEndScreen)
							PRINTLN("[RCC MISSION] - FREEZE_SCREEN_FOR_PLACED_CAMERA - is rendering.")
							bMoveOntoNextStage = TRUE
						ELSE
							IF CONTENT_IS_USING_ARENA()
								RENDER_SCRIPT_CAMS(TRUE, FALSE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION] - FREEZE_SCREEN_FOR_PLACED_CAMERA - camEndScreen DOES NOT exist.")
			ENDIF
			IF bMoveOntoNextStage
				PRINTLN("[RCC MISSION] - FREEZE_SCREEN_FOR_PLACED_CAMERA - bMoveOntoNextStage = TRUE.")
				IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_RESPAWNING_DURING_FAIL)
					iFreezeCamForPlaceDcamStage++
				ELSE
					iFreezeCamForPlaceDcamStage = 2
				ENDIF
				PRINTLN("[RCC MISSION] - FREEZE_SCREEN_FOR_PLACED_CAMERA - set iFreezeCamForPlaceDcamStage = ", iFreezeCamForPlaceDcamStage)
			ENDIF
		BREAK
		
		CASE 1
			IF IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
				PRINTLN("[LM][SPEC_SPEC][RCC MISSION] - [SAC] - FREEZE_SCREEN_FOR_PLACED_CAMERA - Setting g_ciSpecial_Spectator_BS_Request_Cleanup")
				SET_BIT(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Cleanup)
				SET_BIT(g_iSpecialSpectatorBS2, g_ciSpecial_Spectator_BS2_Dont_Cleanup_Arena_Activities)
			ENDIF
			IF NOT IS_SCREEN_FADED_IN()
				IF NOT IS_PLAYER_RESPAWNING(LocalPlayer)
					IF NOT IS_SCREEN_FADING_IN()
						DO_SCREEN_FADE_IN(500) 
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION] - FREEZE_SCREEN_FOR_PLACED_CAMERA - We are still respawning! Do not fade in!")
				ENDIF
			ELSE
				iFreezeCamForPlaceDcamStage++
				PRINTLN("[RCC MISSION] - FREEZE_SCREEN_FOR_PLACED_CAMERA - set iFreezeCamForPlaceDcamStage = ", iFreezeCamForPlaceDcamStage)
			ENDIF
		BREAK
		
		CASE 2
			DO_RENDER_PHASE_PAUSE_FOR_CELEBRATION_SCREEN()
			SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_FrozenRenderPhasesForFail)
			PRINTLN("[RCC MISSION] - frozen screen on cut to placed camera. Set bit ciCELEBRATION_BIT_SET_FrozenRenderPhasesForFail.")
			iFreezeCamForPlaceDcamStage++
			PRINTLN("[RCC MISSION] - FREEZE_SCREEN_FOR_PLACED_CAMERA - set iFreezeCamForPlaceDcamStage = ", iFreezeCamForPlaceDcamStage)
		BREAK
		
		CASE 3
			// Done. 
		BREAK
		
	ENDSWITCH
	
ENDPROC

FUNC BOOL IS_CS_WAITING_FOR_PULSE()
	IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
	OR Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
		IF NOT HAS_NET_TIMER_EXPIRED(CSMinPulseTimer,CS_MIN_PULSE_TIME)
			PRINTLN("[RCC MISSION] - [NETCELEBRATION] - IS_CS_WAITING_FOR_PULSE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL OK_TO_DO_MISSION_OUTRO()
	
	IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
	OR USING_SHOWDOWN_POINTS()
		RETURN FALSE
	ENDIF
	
	IF Is_Player_Currently_On_MP_Versus_Mission(LocalPlayer)
	OR Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)
	OR Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer)
	
		RETURN TRUE
	ENDIF	
	
	RETURN FALSE
ENDFUNC

PROC ADD_ARENA_CELEBRATION_SHARDS(STRING strScreenName)

	INT iFinishPos 
	INT iNumLBDTeams = MC_serverBD.iNumActiveTeams		
	iFinishPos = GET_TEAM_FINISH_POSITION_FOR_LBD(MC_PlayerBD[iLocalPart].iTeam, iNumLBDTeams)
	
	INT iNumPlayers 	= (MC_serverBD.iNumberOfLBPlayers[0] + MC_serverBD.iNumberOfLBPlayers[1] + MC_serverBD.iNumberOfLBPlayers[2] + MC_serverBD.iNumberOfLBPlayers[3])
	INT iNumTeams		= MC_serverBD.iNumberOfTeams
	BOOL bPlayedThisMatchWithFriend 
	
	PRINTLN("ADD_ARENA_CELEBRATION_SHARDS, iNumTeams = ", iNumTeams)
	PRINTLN("ADD_ARENA_CELEBRATION_SHARDS, iFinishPos = ", iFinishPos)
	PRINTLN("ADD_ARENA_CELEBRATION_SHARDS, iNumPlayers = ", iNumPlayers)
	PRINTLN("ADD_ARENA_CELEBRATION_SHARDS, iNumLBDTeams = ", iNumLBDTeams)
	PRINTLN("ADD_ARENA_CELEBRATION_SHARDS, MC_PlayerBD[iLocalPart].iTeam = ", MC_PlayerBD[iLocalPart].iTeam)

	// Populate Arena points struct
	FILL_ARENA_POINTS_STRUCT(MC_playerBD_1[iLocalPart].sArenaPoints, (iNumTeams  > 1), iFinishPos, iNumPlayers)
	
	bPlayedThisMatchWithFriend = (MC_playerBD_1[iLocalPart].sArenaPoints.iNumFriends > 0)
	
	// Technical calculate Arena points
	IF NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
		HANDLE_END_OF_ARENA_EVENT_PLAYER_CAREER_REWARDS(iFinishPos, (iNumTeams > 1), IS_FORCE_ABANDON_CURRENT_MP_MISSION_SET(), IS_PLAYER_SELECTING_CUSTOM_VEHICLE(LocalPlayer, TRUE), iNumPlayers, bPlayedThisMatchWithFriend, iNumTeams)
	ENDIF
	
	// Populate Arena points
	FEED_ARENA_POINTS_TO_CELEBRATION_SCREEN(sCelebrationData, MC_playerBD_1[iLocalPart].sArenaPoints)
	
	// Display Arena points bar
	ADD_ARENA_POINTS_BAR_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName)
	
	// Display Reward unlocks if appropriate
	MC_playerBD_1[iLocalPart].sArenaPoints.iNumRewardsUnlocked = GET_NUM_ITEMS_UNLOCKED_AFTER_LAST_ARENA_EVENT()   
	
	ARENA_CAREER_UNLOCK_ITEMS eUnlock
	TEXT_LABEL_15 tl15Reward	
	TEXT_LABEL_15 tl15RewardSubString
	INT iValuePassed 

	IF WERE_ANY_ITEMS_UNLOCKED_AFTER_LAST_ARENA_EVENT()
	AND MC_playerBD_1[iLocalPart].sArenaPoints.iNumRewardsUnlocked > 0
	
		INT i
		REPEAT MC_playerBD_1[iLocalPart].sArenaPoints.iNumRewardsUnlocked i
		
			eUnlock = GET_ARENA_CARRER_ITEM_UNLOCKED(i)
			
			tl15Reward = GET_ARENA_CAREER_UNLOCK_TEXT_LABEL(eUnlock, tl15RewardSubString)
			
			iValuePassed = GET_ARENA_CARRER_ITEM_UNLOCKED_AMMOUNT(i)
			
			#IF IS_DEBUG_BUILD
				IF IS_STRING_NULL_OR_EMPTY(tl15RewardSubString)
					PRINTLN("[NETCELEBRATION] ADD_ARENA_CELEBRATION_SHARDS, sRewardName = ", tl15Reward, " i = ", i, " iNumRewardsUnlocked = ", MC_playerBD_1[iLocalPart].sArenaPoints.iNumRewardsUnlocked, " iValuePassed = ", iValuePassed)
				ELSE
					PRINTLN("[NETCELEBRATION] ADD_ARENA_CELEBRATION_SHARDS, sRewardName = ", tl15RewardSubString, " i = ", i, " iNumRewardsUnlocked = ", MC_playerBD_1[iLocalPart].sArenaPoints.iNumRewardsUnlocked, " iValuePassed = ", iValuePassed)
				ENDIF
			#ENDIF
			
			ADD_ARENA_REWARDS_CELEBRATION_SCREEN(sCelebrationData, strScreenName, tl15Reward, tl15RewardSubString, iValuePassed)
	
		ENDREPEAT
		
		// Increase display time to allow these to show
		sCelebrationData.iEstimatedScreenDuration += ( CELEBRATION_SCREEN_STAT_DISPLAY_TIME * MC_playerBD_1[iLocalPart].sArenaPoints.iNumRewardsUnlocked )
	ELSE
		sCelebrationData.iEstimatedScreenDuration += 1000
	ENDIF
	
	// To show on the leaderboard
	MC_playerBD_1[iLocalPart].iApEarned = GET_ARENA_EVENT_ARENA_CAREER_POINTS_REWARD((iNumTeams  > 1), iFinishPos, FALSE, iNumPlayers, FALSE, FALSE)
	
	SET_BIT(iLocalBoolCheck30, LBOOL30_FILLED_ARENA_POINTS)

	PRINTLN("[NETCELEBRATION] ADD_ARENA_CELEBRATION_SHARDS, MC_playerBD_1[iLocalPart].iApEarned = ", MC_playerBD_1[iLocalPart].iApEarned)

ENDPROC

FUNC BOOL HAS_CELEBRATION_SUMMARY_FINISHED(BOOL bDoEarlyDeathScreen = FALSE)	
	
	IF IS_A_STRAND_MISSION_BEING_INITIALISED()
		RETURN TRUE
	ENDIF
	
	IF MC_serverBD.iNextMission < FMMC_MAX_STRAND_MISSIONS
	AND MC_serverBD.iNextMission >= 0
	AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl23NextContentID[MC_serverBD.iNextMission])
		//Strand mission is going to be initialised soon
		IF g_FMMC_STRUCT.eBranchingTransitionType[MC_serverBD.iNextMission] != FMMC_BRANCHING_MISSION_TRANSITION_TYPE_MOCAP
		AND sCelebrationStats.bPassedMission
			PRINTLN("[RCC MISSION] - [NETCELEBRATION][MSRAND] - HAS_CELEBRATION_SUMMARY_FINISHED - TRUE (Airlock or Fade Out strand transition")
			RETURN TRUE
		ENDIF
	ENDIF
	
	//Don't do a celebration screen if the player died early on an LTS (just go straight to spectator).
	IF bDoEarlyDeathScreen
		IF (Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer))
			PRINTLN("[RCC MISSION] - [NETCELEBRATION] - Early death screen - This is an LTS mission, heading straight to spectator cam")
			sCelebrationData.eCurrentStage = CELEBRATION_STAGE_WAITING_FOR_SPECTATOR_CAM
			RETURN TRUE
		ELIF (Is_Player_Currently_On_MP_Versus_Mission(LocalPlayer))
			PRINTLN("[RCC MISSION] - [NETCELEBRATION] - Early death screen - This is a VS mission, heading straight to spectator cam")
			sCelebrationData.eCurrentStage = CELEBRATION_STAGE_WAITING_FOR_SPECTATOR_CAM
			RETURN TRUE
		ELIF (Is_Player_Currently_On_MP_Heist(LocalPlayer) OR Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer))
			PRINTLN("[RCC MISSION] - [NETCELEBRATION] - Early death screen - This is a Heist??? Heading straight to spectator cam")
			sCelebrationData.eCurrentStage = CELEBRATION_STAGE_WAITING_FOR_SPECTATOR_CAM
			RETURN TRUE
		ELIF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
			PRINTLN("[RCC MISSION] - [NETCELEBRATION] - Early death screen - This is a Gang Ops mission. Heading straight to spectator cam")
			sCelebrationData.eCurrentStage = CELEBRATION_STAGE_WAITING_FOR_SPECTATOR_CAM
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF (bDoEarlyDeathScreen AND sCelebrationData.eCurrentStage > CELEBRATION_STAGE_SETUP)
	OR NOT bDoEarlyDeathScreen
		HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
		PRINTLN("[RCC MISSION] - [NETCELEBRATION] - HAS_CELEBRATION_SUMMARY_FINISHED - HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME")
	ENDIF
	
	IF sCelebrationData.eCurrentStage = CELEBRATION_STAGE_SETUP
		IF IS_THIS_A_ROUNDS_MISSION()
		AND NOT AM_I_AT_END_OF_ROUNDS_MISSION()
		AND NOT SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
			PRINTLN("[RCC MISSION] - [NETCELEBRATION] - IS_THIS_A_ROUNDS_MISSION = TRUE, AM_I_AT_END_OF_ROUNDS_MISSION = FALSE, SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD = FALSE. Waiting until we can validly call SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	//Don't do the celebration screen if the player is just spectating.
	IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_START_SPECTATOR)
		PRINTLN("[RCC MISSION] - [NETCELEBRATION] - HAS_CELEBRATION_SUMMARY_FINISHED - PBBOOL_START_SPECTATOR")
		RETURN TRUE
	ENDIF
	
	BOOL bArena
	IF CONTENT_IS_USING_ARENA()
		bArena = TRUE
	ENDIF
	
	NET_SET_PLAYER_CONTROL_FLAGS NSPC_CLEAR_TASKS_NO_RAGDOLL = INT_TO_ENUM(NET_SET_PLAYER_CONTROL_FLAGS, PICK_INT(IS_PED_RAGDOLL(LocalPlayerPed), 0, ENUM_TO_INT(NSPC_CLEAR_TASKS)))
		
	IF IS_PLAYER_IN_CREATOR_AIRCRAFT(GET_PLAYER_INDEX())
	AND g_iInteriorTurretSeat != -1
		PRINTLN("[RCC MISSION] - [NETCELEBRATION] - DO NOT CLEAR TASKS as we are in an aircraft seat.")
		NSPC_CLEAR_TASKS_NO_RAGDOLL = INT_TO_ENUM(NET_SET_PLAYER_CONTROL_FLAGS, 0)
	ENDIF
	
	SWITCH sCelebrationData.eCurrentStage
		CASE CELEBRATION_STAGE_SETUP
		
			CELEBRATION_SCREEN_TYPE eCelebType
			
			//If it's a round mission and we need to display the LB the make sure we add the JPs
			IF IS_THIS_A_ROUNDS_MISSION()
			AND SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
			AND	IS_BIT_SET(iLocalBoolCheck6, LBOOL6_HIDE_JOB_POINTS_CELEB_SCREEN)
				PRINTLN("[RCC MISSION] - [NETCELEBRATION] - CLEAR_BIT(iLocalBoolCheck6, LBOOL6_HIDE_JOB_POINTS_CELEB_SCREEN)")	
				CLEAR_BIT(iLocalBoolCheck6, LBOOL6_HIDE_JOB_POINTS_CELEB_SCREEN)
				PRINTLN("[RCC MISSION] - [NETCELEBRATION] - ADD_TO_JOB_POINTS_TOTAL - sCelebrationStats.iLocalPlayerJobPoints")	
				ADD_TO_JOB_POINTS_TOTAL(sCelebrationStats.iLocalPlayerJobPoints)
			ENDIF
			
			IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
			OR Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
			
				eCelebType = CELEBRATION_HEIST
				PRINTLN("[RCC MISSION] - [NETCELEBRATION] - USING HEIST CELEBRATION SCREEN 2")	
				

			ELIF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
			
				eCelebType = CELEBRATION_GANGOPS
				PRINTLN("[RCC MISSION] - [NETCELEBRATION] - USING GANGOPS CELEBRATION SCREEN 2")
			
			
			ELSE
		
				eCelebType = CELEBRATION_STANDARD
				PRINTLN("[RCC MISSION] - [NETCELEBRATION] - USING STANDARD CELEBRATION SCREEN 2")
			
			ENDIF
				
			IF IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
			AND NOT IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Spectator_Wait_For_Cleanup)
				PRINTLN("[LM][MISSION][SPEC_SPEC] - Setting g_ciSpecial_Spectator_BS_Request_Spectator_Wait_For_Cleanup (1)")
				SET_BIT(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Spectator_Wait_For_Cleanup)
			ENDIF
			
			REQUEST_CELEBRATION_SCREEN(sCelebrationData, eCelebType)
			
			INT iWinnerLBDIndex
			iWinnerLBDIndex = -1
			BOOL bFetchedStatsFromServer, bPlayerInSameCrewAsWinner, bDelayShard
			GAMER_HANDLE sWinnerHandle
			
			IF IS_THIS_A_ROUNDS_MISSION()
			AND SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
			AND g_sTransitionSessionData.sMissionRoundData.piWinner != INVALID_PLAYER_INDEX()
				iWinnerLBDIndex = GET_LEADERBOARD_INDEX_OF_PLAYER(g_sTransitionSessionData.sMissionRoundData.piWinner)
				PRINTLN("[RCC MISSION] - [NETCELEBRATION] - IS_THIS_A_ROUNDS_MISSION = TRUE")	
				PRINTLN("[RCC MISSION] - [NETCELEBRATION] - SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD = TRUE")	
				PRINTLN("[RCC MISSION] - [NETCELEBRATION] - sTransitionSessionData.sMissionRoundData.piWinner != INVALID_PLAYER_INDEX()")	
				PRINTLN("[RCC MISSION] - [NETCELEBRATION] - set iWinnerLBDIndex = GET_LEADERBOARD_INDEX_OF_PLAYER(g_sTransitionSessionData.sMissionRoundData.piWinner) = ", iWinnerLBDIndex) 	
			ELSE
				#IF IS_DEBUG_BUILD
				IF NOT IS_THIS_A_ROUNDS_MISSION()
					PRINTLN("[RCC MISSION] - [NETCELEBRATION] - IS_THIS_A_ROUNDS_MISSION = FALSE")	
				ENDIF
				IF NOT SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
					PRINTLN("[RCC MISSION] - [NETCELEBRATION] - SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD = FALSE")	
				ENDIF
				IF g_sTransitionSessionData.sMissionRoundData.piWinner = INVALID_PLAYER_INDEX()
					PRINTLN("[RCC MISSION] - [NETCELEBRATION] - sTransitionSessionData.sMissionRoundData.piWinner = INVALID_PLAYER_INDEX()")	
				ENDIF
				#ENDIF
				iWinnerLBDIndex = GET_LEADERBOARD_INDEX_OF_TOP_WINNER()
				PRINTLN("[RCC MISSION] - [NETCELEBRATION] - set iWinnerLBDIndex = GET_LEADERBOARD_INDEX_OF_TOP_WINNER()", iWinnerLBDIndex) 	
			ENDIF
			
			IF bDoEarlyDeathScreen
				bFetchedStatsFromServer = TRUE
				bPlayerInSameCrewAsWinner = FALSE
			ELSE
				IF iWinnerLBDIndex > -1
					IF g_MissionControllerserverBD_LB.sleaderboard[iWinnerLBDIndex].iParticipant > -1
						sWinnerHandle = lbdVars.aGamer[g_MissionControllerserverBD_LB.sleaderboard[iWinnerLBDIndex].iParticipant] //This should be able to grab handles for players that have left already left the game.
					ENDIF
				ENDIF
				bPlayerInSameCrewAsWinner = IS_PLAYER_IN_SAME_CREW_AS_ME(sWinnerHandle, bFetchedStatsFromServer)
			ENDIF
	
			//1633762 - If passing a mission then have a delay between the flash and the shard.
			IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bStartedCelebrationBeforeCutsceneEnded)
			AND NOT bDoEarlyDeathScreen
			AND sCelebrationStats.bPassedMission
				IF HAS_NET_TIMER_STARTED(sCelebrationData.sCelebrationTransitionTimer)
					IF NOT HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTransitionTimer, 650)
						bDelayShard = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			// This might cause issues where we see under the world but we need this here so players are not stuck in a Black Screen.
			// Normally happens because we're sent to spectator and before we get a target we end the mission.
			IF IS_SCREEN_FADED_OUT()
				PRINTLN("[LM][url:bugstar:4372431 - HAS_CELEBRATION_SCREEN_FINISHED - Emergancy Screen Fade In. Celebration Screen should start.")
				DO_SCREEN_FADE_IN_FOR_TRANSITION(250)
			ENDIF
				
			IF HAS_CELEBRATION_SCREEN_LOADED(sCelebrationData)
			AND bFetchedStatsFromServer
			AND (bDoEarlyDeathScreen OR CONTROL_UPDATING_CHALLENGES())
			AND NOT bDelayShard
			AND (IS_SCREEN_FADED_IN() AND NOT IS_SCREEN_FADING_OUT() AND NOT IS_SCREEN_FADED_OUT())
			AND NOT ANIMPOSTFX_IS_RUNNING("DeathFailMPIn")
			AND NOT IS_CS_WAITING_FOR_PULSE()
			
				STRING strScreenName, strBackgroundColour
				INT iCurrentRP, iCurrentLvl, iNextLvl, iRPToReachCurrentLvl, iRPToReachNextLvl, iChallengePart, iTotalChallengeParts
				BOOL bWonChallengePart
			
				//Retrieve all the required stats for the race end screen.
				strBackgroundColour = "HUD_COLOUR_BLACK"
				
				IF bDoEarlyDeathScreen
					strScreenName = "EARLYDEATH"
				ELSE
					strScreenName = "SUMMARY"
					iCurrentRP = GET_PLAYER_FM_XP(NETWORK_GET_PLAYER_INDEX(PARTICIPANT_ID())) - sCelebrationStats.iLocalPlayerXP
					iCurrentLvl = GET_FM_RANK_FROM_XP_VALUE(iCurrentRP) //GET_PLAYER_GLOBAL_RANK(NETWORK_GET_PLAYER_INDEX(PARTICIPANT_ID())) //We don't want the current level, but their level before the XP was given.
					iNextLvl = iCurrentLvl + 1
					PRINTLN("[RCC MISSION] - [NETCELEBRATION] - iCurrentRP = ", iCurrentRP, ", iCurrentLvl = ", iCurrentLvl, ", iNextLvl = ", iNextLvl)	
					iRPToReachCurrentLvl = GET_XP_NEEDED_FOR_FM_RANK(iCurrentLvl)
					iRPToReachNextLvl = GET_XP_NEEDED_FOR_FM_RANK(iNextLvl)
					iChallengePart = g_sCurrentPlayListDetails.iPlaylistProgress
					iTotalChallengeParts = g_sCurrentPlayListDetails.iLength
					sCelebrationData.iEstimatedScreenDuration = CELEBRATION_SCREEN_STAT_WIPE_TIME * 2
					
					IF IS_THIS_A_ROUNDS_MISSION_FOR_CELEBRATION()
					#IF IS_DEBUG_BUILD
					OR g_bFakeRound
					#ENDIF
						
						IF g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iStartRp = 0
							g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iStartRp = iCurrentRP 
						ENDIF
						
						IF g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iStartLevel = 0
							g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iStartLevel = GET_FM_RANK_FROM_XP_VALUE(g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iStartRp)
						ENDIF
						
						g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iRpGained += sCelebrationStats.iLocalPlayerXP
						g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iJobPoints = sCelebrationStats.iLocalPlayerJobPoints
						g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iCash += sCelebrationStats.iLocalPlayerCash
						
						IF SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
						#IF IS_DEBUG_BUILD
						OR g_bFakeEndOfMatch
						#ENDIF
							
							g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iEndLvl = GET_FM_RANK_FROM_XP_VALUE(GET_PLAYER_FM_XP(NETWORK_GET_PLAYER_INDEX(PARTICIPANT_ID())))
							g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iRPToReachStartLvl = GET_XP_NEEDED_FOR_FM_RANK(g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iStartLevel)
							g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iRPToReachEndLvl = GET_XP_NEEDED_FOR_FM_RANK(g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iEndLvl)
							
							g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iNextLvl = (g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iStartLevel + 1)
							g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iRptoReachNextLvl = GET_XP_NEEDED_FOR_FM_RANK(g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iNextLvl)
							
						ENDIF
					ENDIF
					
				ENDIF				
				
				BOOL bBlackOverride
				INT iCreateStatWallAudioType
				IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
				OR Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
				OR GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
					strBackgroundColour = "HUD_COLOUR_HSHARD"
					bBlackOverride = FALSE
					IF NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bPassedMission
						iCreateStatWallAudioType = 2
						IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
							PRINTLN("[RCC MISSION] - [NETCELEBRATION] - failed mission - set strBackgroundColour = HUD_COLOUR_TECH_RED ")
							strBackgroundColour = "HUD_COLOUR_TECH_RED"
						ENDIF
						PRINTLN("[RCC MISSION] - [NETCELEBRATION] - failed mission - set iCreateStatWallAudioType = ", iCreateStatWallAudioType)
					ELSE
						IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.iHeistType = ciCELEBRATION_HEIST_TYPE__PREP
							iCreateStatWallAudioType = 1
							
							PRINTLN("[RCC MISSION] - [NETCELEBRATION] - failed mission - set iCreateStatWallAudioType = ", iCreateStatWallAudioType)
						ELIF g_TransitionSessionNonResetVars.sGlobalCelebrationData.iHeistType = ciCELEBRATION_HEIST_TYPE__FINALE
							iCreateStatWallAudioType = 3
							PRINTLN("[RCC MISSION] - [NETCELEBRATION] - failed mission - set iCreateStatWallAudioType = ", iCreateStatWallAudioType)
						ENDIF
					ENDIF
				ENDIF
				
				//Build the screen
				CREATE_CELEBRATION_SCREEN(sCelebrationData, strScreenName, strBackgroundColour, bBlackOverride, iCreateStatWallAudioType)
				
				IF bDoEarlyDeathScreen
				
					//Early death screen is for missions where you can run out of lives: single screen indicating fail then transition to spectator cam.
					PRINTLN("[NETCELEBRATION] - [RCC MISSION] - call 0 to ADD_MISSION_RESULT_TO_CELEBRATION_SCREEN.")
					ADD_MISSION_RESULT_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, CELEBRATION_JOB_TYPE_MISSION_EARLY_FAIL, sCelebrationStats.bPassedMission, 
															 sCelebrationStats.strFailReason, sCelebrationStats.strFailLiteral, sCelebrationStats.strFailLiteral2)
					
					sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME
				ELSE
					IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
					OR Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
					OR GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()	
					OR CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
						ADD_END_HEIST_DATA_TO_CELEBRATION_SCREEN(sCelebrationData)
						IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
						OR Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
							SET_APARTMENT_LOCATION()
						ENDIF
						
					ELSE
						IF Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)
							PRINTLN("[NETCELEBRATION] - [RCC MISSION] - call 1 to ADD_MISSION_RESULT_TO_CELEBRATION_SCREEN.")
							ADD_MISSION_RESULT_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, CELEBRATION_JOB_TYPE_LTS, sCelebrationStats.bPassedMission, 
																 	"", "", "")
						ELSE
							IF Is_Player_Currently_On_MP_Versus_Mission(LocalPlayer)
							OR Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer)
								PRINTLN("[NETCELEBRATION] - [RCC MISSION] - call 2 to ADD_MISSION_RESULT_TO_CELEBRATION_SCREEN.")
								ADD_MISSION_RESULT_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, CELEBRATION_JOB_TYPE_VS_MISSION, sCelebrationStats.bPassedMission, 
																	 "", "", "")
							ELSE
								PRINTLN("[NETCELEBRATION] - [RCC MISSION] - call 3 to ADD_MISSION_RESULT_TO_CELEBRATION_SCREEN.")
								ADD_MISSION_RESULT_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, CELEBRATION_JOB_TYPE_MISSION, sCelebrationStats.bPassedMission, 
																		sCelebrationStats.strFailReason, sCelebrationStats.strFailLiteral, sCelebrationStats.strFailLiteral2)
							ENDIF
						ENDIF
						
						sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME
						
						#IF IS_DEBUG_BUILD
						BOOL bRoundsMissionForCelTemp, bIsRoundsMission, bDisplayRoundsLb
						bRoundsMissionForCelTemp = IS_THIS_A_ROUNDS_MISSION_FOR_CELEBRATION()
						bIsRoundsMission = IS_THIS_A_ROUNDS_MISSION()
						bDisplayRoundsLb = SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
						PRINTLN("[NETCELEBRATION] - IS_THIS_A_ROUNDS_MISSION_FOR_CELEBRATION = ", bRoundsMissionForCelTemp)
						PRINTLN("[NETCELEBRATION] - IS_THIS_A_ROUNDS_MISSION = ", bIsRoundsMission)
						PRINTLN("[NETCELEBRATION] - SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD = ", bDisplayRoundsLb)
						#ENDIF
						
						IF NOT IS_THIS_A_ROUNDS_MISSION_FOR_CELEBRATION()
						#IF IS_DEBUG_BUILD
						AND NOT g_bFakeRound
						#ENDIF
						
							IF IS_PLAYLIST_DOING_HEAD_TO_HEAD()
								ADD_CHALLENGE_PART_RESULT_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, CHALLENGE_PART_HEAD_TO_HEAD, iChallengePart, iTotalChallengeParts, bPlayerInSameCrewAsWinner)
								sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME
							ELIF IS_PLAYLIST_DOING_CHALLENGE()
								IF sCelebrationStats.iLocalPlayerScore >= g_sCurrentPlayListDetails.sLoadedMissionDetails[g_sCurrentPlayListDetails.iCurrentPlayListPosition].iBestScore
									bWonChallengePart = TRUE
								ENDIF
							
								ADD_CHALLENGE_PART_RESULT_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, CHALLENGE_PART_CREW, iChallengePart, iTotalChallengeParts, bWonChallengePart)
								sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME
							ENDIF
							
							IF sCelebrationStats.iLocalPlayerCash != 0
								PRINTLN("[NETCELEBRATION] - HAS_CELEBRATION_SUMMARY_FINISHED - calling: ADD_CASH_TO_CELEBRATION_SCREEN, passing: $", sCelebrationStats.iLocalPlayerCash)
								ADD_CASH_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, sCelebrationStats.iLocalPlayerCash)
								sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME //If displaying cash do an extra screen.
								IF bArena
									INCREMENT_ARENA_CASH_EARNED_FOR_AWARD(sCelebrationStats.iLocalPlayerCash)
									PRINTLN("[MJL] MP_AWARD_ARENA_WAGEWORKER = ", GET_MP_INT_CHARACTER_AWARD(MP_AWARD_ARENA_WAGEWORKER))
								ENDIF
							ENDIF
							
							IF bArena
							
								ADD_ARENA_CELEBRATION_SHARDS(strScreenName)
							
							ELSE
							
								IF NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_HIDE_JOB_POINTS_CELEB_SCREEN)
									IF sCelebrationStats.iLocalPlayerCash != 0
										PRINTLN("[NETCELEBRATION] - HAS_CELEBRATION_SUMMARY_FINISHED - calling: ADD_JOB_POINTS_TO_CELEBRATION_SCREEN, passing: JP", sCelebrationStats.iLocalPlayerJobPoints)
										ADD_JOB_POINTS_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, sCelebrationStats.iLocalPlayerJobPoints)
									ELSE
										PRINTLN("[NETCELEBRATION] - HAS_CELEBRATION_SUMMARY_FINISHED - calling: ADD_JOB_POINTS_TO_CELEBRATION_SCREEN, passing: JP", sCelebrationStats.iLocalPlayerJobPoints)
										ADD_JOB_POINTS_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, sCelebrationStats.iLocalPlayerJobPoints, TRUE, FALSE)
									ENDIF
								ENDIF
								
								PRINTLN("[NETCELEBRATION] - HAS_CELEBRATION_SUMMARY_FINISHED - calling: ADD_REP_POINTS_AND_RANK_BAR_TO_CELEBRATION_SCREEN, passing: RP", sCelebrationStats.iLocalPlayerXP)
								ADD_REP_POINTS_AND_RANK_BAR_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, sCelebrationStats.iLocalPlayerXP, iCurrentRP, 
															   	   iRPToReachCurrentLvl, iRPToReachNextLvl, iCurrentLvl, iNextLvl)
								sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME * 2 //Two screens are guaranteed.
								
								IF iCurrentRP + sCelebrationStats.iLocalPlayerXP > iRPToReachNextLvl
									sCelebrationData.iEstimatedScreenDuration += ((CELEBRATION_SCREEN_STAT_DISPLAY_TIME / 2) + (CELEBRATION_SCREEN_STAT_DISPLAY_TIME / 4))
								ENDIF
							ENDIF
						
						ELSE
							
							IF SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
							#IF IS_DEBUG_BUILD
							OR g_bFakeEndOfMatch
							#ENDIF
							
								IF g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iCash != 0
									PRINTLN("[NETCELEBRATION] - HAS_CELEBRATION_SUMMARY_FINISHED - ROUNDS MISSION - calling: ADD_CASH_TO_CELEBRATION_SCREEN, passing: $", g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iCash)
									ADD_CASH_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iCash)
									sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME //If displaying cash do an extra screen.
									IF bArena
										INCREMENT_ARENA_CASH_EARNED_FOR_AWARD(g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iCash)
										PRINTLN("[MJL] MP_AWARD_ARENA_WAGEWORKER = ", GET_MP_INT_CHARACTER_AWARD(MP_AWARD_ARENA_WAGEWORKER))
									ENDIF
								ENDIF
							
								IF bArena
								
									ADD_ARENA_CELEBRATION_SHARDS(strScreenName)
								
								ELSE
									
									IF g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iCash != 0
										PRINTLN("[NETCELEBRATION] - HAS_CELEBRATION_SUMMARY_FINISHED - ROUNDS MISSION - calling: ADD_JOB_POINTS_TO_CELEBRATION_SCREEN, passing: JP", g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iJobPoints)
										ADD_JOB_POINTS_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iJobPoints)
									ELSE
										PRINTLN("[NETCELEBRATION] - HAS_CELEBRATION_SUMMARY_FINISHED - ROUNDS MISSION - calling: ADD_JOB_POINTS_TO_CELEBRATION_SCREEN, passing: JP", g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iJobPoints)
										ADD_JOB_POINTS_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iJobPoints, TRUE, FALSE)
									ENDIF
									
									PRINTLN("[NETCELEBRATION] - HAS_CELEBRATION_SUMMARY_FINISHED - ROUNDS MISSION - calling: ADD_REP_POINTS_AND_RANK_BAR_TO_CELEBRATION_SCREEN, passing: RP", g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iRpGained)
									ADD_REP_POINTS_AND_RANK_BAR_TO_CELEBRATION_SCREEN(	sCelebrationData, 
																						strScreenName, 
																						g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iRpGained,
																						g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iStartRp,
																						g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iRPToReachStartLvl,
																						g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iRptoReachNextLvl,
																						g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iStartLevel,
																						g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iNextLvl)
									sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME * 2 //Two screens are guaranteed.
									
									IF g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iEndLvl > g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iStartLevel
										sCelebrationData.iEstimatedScreenDuration += ((CELEBRATION_SCREEN_STAT_DISPLAY_TIME / 2) + (CELEBRATION_SCREEN_STAT_DISPLAY_TIME / 4))
									ENDIF
								ENDIF
								
							ENDIF
							
						ENDIF
						
					ENDIF
				ENDIF
				
				INT iMoneyTexture
				
				IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
				OR Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
					iMoneyTexture = 2
				ELIF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
					iMoneyTexture = 2 // same but will likely change so set up for its own bit
				ENDIF
				
				ADD_BACKGROUND_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, -1, iMoneyTexture)
				SHOW_CELEBRATION_SCREEN(sCelebrationData, strScreenName)
				
				SET_CELEBRATION_SCREEN_AS_ACTIVE(TRUE)
				
				RESET_NET_TIMER(sCelebrationData.sCelebrationTimer)
				START_NET_TIMER(sCelebrationData.sCelebrationTimer)
			
				IF NOT bDoEarlyDeathScreen
					START_AUDIO_SCENE("MP_JOB_CHANGE_RADIO_MUTE")
					PRINTLN("[WJK] - called START_AUDIO_SCENE(MP_JOB_CHANGE_RADIO_MUTE).")
					
					SET_CHAT_ENABLED_FOR_CELEBRATION_SCREEN(TRUE)
				ELSE
					SET_BIT(sSaveOutVars.iBitSet,ciRATINGS_PLAYED)
				
					//If this is an early death screen then keep everything on screen indefinitely.
					ADD_PAUSE_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, 15000)
				ENDIF
								
				//Freeze the screen: don't freeze for passing an LTS on foot as it uses the deathmatch cams. Some situations will use a delayed freeze to synch with the screen coming down.
//				IF Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)
				IF OK_TO_DO_MISSION_OUTRO()
					IF NOT sCelebrationStats.bPassedMission
					OR (bLocalPlayerOK AND IS_PED_IN_ANY_VEHICLE(LocalPlayerPed))
						SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bDoDelayedCelebrationFreeze)
					ENDIF
				ELSE
					IF bDoEarlyDeathScreen
						SET_SKYFREEZE_FROZEN()
						
						//These aren't called in advance when dying (so the WASTED message can display).
						KILL_UI_FOR_CELEBRATION_SCREEN()
						HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
					ELSE
						SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bDoDelayedCelebrationFreeze)
					ENDIF
				ENDIF
				
				//Audio scenes: see B*1642903 for implementation notes.
				IF NOT Is_Player_Currently_On_MP_Heist(LocalPlayer)
				AND NOT Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
				AND NOT GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
					IF CONTENT_IS_USING_ARENA()
						IF NOT IS_AUDIO_SCENE_ACTIVE("MP_CELEB_SCREEN_ARENA_SCENE")
							START_AUDIO_SCENE("MP_CELEB_SCREEN_ARENA_SCENE")
						ENDIF
					ELIF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciPLAY_POWERPLAY_INTRO_ANNOUNCER)
						IF NOT IS_AUDIO_SCENE_ACTIVE("MP_CELEB_SCREEN_SCENE")
							START_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
						ENDIF
					ELSE
						IF NOT IS_AUDIO_SCENE_ACTIVE("MP_CELEB_SCREEN_PP_SCENE")
							START_AUDIO_SCENE("MP_CELEB_SCREEN_PP_SCENE")
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_AUDIO_SCENE_ACTIVE("MP_LEADERBOARD_SCENE")
					STOP_AUDIO_SCENE("MP_LEADERBOARD_SCENE")
				ENDIF
				
				IF IS_AUDIO_SCENE_ACTIVE("MP_LEADERBOARD_PP_SCENE")
					STOP_AUDIO_SCENE("MP_LEADERBOARD_PP_SCENE")
				ENDIF
				
				IF bLocalPlayerOK
					g_bMissionRemovedWanted = TRUE
					SET_EVERYONE_IGNORE_PLAYER(LocalPlayer,TRUE)
					SET_PLAYER_WANTED_LEVEL(LocalPlayer,0)
					SET_PLAYER_WANTED_LEVEL_NOW(LocalPlayer)
					
					//Don't turn player control off if we didn't do the freeze, otherwise there will be a visible pop.
					IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bDoDelayedCelebrationFreeze)
					AND NOT OK_TO_DO_MISSION_OUTRO()
						IF DID_SPEC_BAILED_FOR_TRANSITION()							
							NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_SET_INVISIBLE | NSPC_CLEAR_TASKS_NO_RAGDOLL | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
						ELSE
							IF Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)
								NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_CLEAR_TASKS_NO_RAGDOLL | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
							ELSE
								NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_CLEAR_TASKS_NO_RAGDOLL | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
							ENDIF
						ENDIF
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed, TRUE)
					ENDIF
				ENDIF
				
				CLEAR_HELP()
				
				PRINTLN("[NETCELEBRATION] Adding stats ---------------------------")
				PRINTLN("strScreenName ", strScreenName)
				PRINTLN("iCurrentRP ", iCurrentRP)
				PRINTLN("iCurrentLvl ", iCurrentLvl)
				PRINTLN("iNextLvl ", iNextLvl)
				PRINTLN("iRPToReachCurrentLvl ", iRPToReachCurrentLvl)
				PRINTLN("iRPToReachNextLvl ", iRPToReachNextLvl)
				PRINTLN("iChallengePart ", iChallengePart)
				PRINTLN("iTotalChallengeParts ", iTotalChallengeParts)
				PRINTLN("sCelebrationData.iEstimatedScreenDuration ", sCelebrationData.iEstimatedScreenDuration)
				PRINTLN("---------------------------------------------------------")
				
				RESET_CELEBRATION_PRE_LOAD(sCelebrationData)
				
//				IF MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_HIDDEN_IE_JN_TARGET_0)
//				OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_HIDDEN_IE_JN4_ORANGE_TARGET_0)
//				OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_HIDDEN_IE_JN4_PURPLE_TARGET_0)
//					#IF IS_DEBUG_BUILD
//					IF MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_HIDDEN_IE_JN_TARGET_0)
//						PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_SUMMARY_FINISHED - MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_HIDDEN_IE_JN_TARGET_0)")
//					ENDIF
//					IF MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_HIDDEN_IE_JN4_ORANGE_TARGET_0)
//						PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_SUMMARY_FINISHED - MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_HIDDEN_IE_JN4_ORANGE_TARGET_0)")
//					ENDIF
//					IF MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_HIDDEN_IE_JN4_PURPLE_TARGET_0)
//						PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_SUMMARY_FINISHED - MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_HIDDEN_IE_JN4_PURPLE_TARGET_0)")
//					ENDIF
//					PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_SUMMARY_FINISHED - Clear_All_Generated_MP_Headshots(FALSE)")
//					PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_SUMMARY_FINISHED - SET_PLAYER_CLOTHES_FOR_RETURN_TO_FREEMODE()")
//					#ENDIF
//					Clear_All_Generated_MP_Headshots(FALSE)
//					SET_PLAYER_CLOTHES_FOR_RETURN_TO_FREEMODE()
//				ENDIF
			
				sCelebrationData.eCurrentStage = CELEBRATION_STAGE_PLAYING
				
			ELSE
				
				PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_SUMMARY_FINISHED - cannot process celebration screen yet.")
				
				#IF IS_DEBUG_BUILD
					
					IF NOT HAS_CELEBRATION_SCREEN_LOADED(sCelebrationData)
						PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_SUMMARY_FINISHED - Waiting on HAS_CELEBRATION_SCREEN_LOADED")
					ENDIF
					
					IF NOT bFetchedStatsFromServer
						PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_SUMMARY_FINISHED - Waiting on stats to be fetched.")
					ENDIF
					
					IF NOT (bDoEarlyDeathScreen OR CONTROL_UPDATING_CHALLENGES())
						PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_SUMMARY_FINISHED - (bDoEarlyDeathScreen OR CONTROL_UPDATING_CHALLENGES()) = FALSE.")
						IF NOT bDoEarlyDeathScreen
							PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_SUMMARY_FINISHED - bDoEarlyDeathScreen = FALSE.")
						ENDIF
						IF NOT CONTROL_UPDATING_CHALLENGES()
							PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_SUMMARY_FINISHED - Waiting on CONTROL_UPDATING_CHALLENGES")
						ENDIF
					ENDIF
					
					IF bDelayShard
						PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_SUMMARY_FINISHED - bDelayShard = TRUE.")
					ENDIF
					
					IF NOT IS_SCREEN_FADED_IN()
						PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_SUMMARY_FINISHED - IS_SCREEN_FADED_IN() = FALSE.")
					ENDIF
					
				#ENDIF
				
				IF ANIMPOSTFX_IS_RUNNING("DeathFailMPIn")
					IF NOT HAS_NET_TIMER_STARTED(sCelebrationData.timerDeathPostFxDelay)
						START_NET_TIMER(sCelebrationData.timerDeathPostFxDelay)
						PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - DeathFailMPIn is running, started timer timerDeathPostFxDelay.")
					ELSE
						IF HAS_NET_TIMER_EXPIRED(sCelebrationData.timerDeathPostFxDelay, 1000)
							PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - DeathFailMPIn is running, timer timerDeathPostFxDelay >= 1000. Calling CLEAR_KILL_STRIP_DEATH_EFFECTS.")
							CLEAR_KILL_STRIP_DEATH_EFFECTS()
						ENDIF
					ENDIF
				ENDIF
					
			ENDIF
		BREAK
		
		CASE CELEBRATION_STAGE_PLAYING
		
			IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
			OR Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
			OR GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
				PRINTLN("[RCC MISSION] - [NETCELEBRATION] - ON HEIST)")	
			
				IF HAS_CELEBRATION_SHARD_BEGAN_TO_FALL_IN()
					PRINTLN("[RCC MISSION] - [NETCELEBRATION] - HAS_CELEBRATION_SHARD_BEGAN_TO_FALL_IN")	
					IF NOT ANIMPOSTFX_IS_RUNNING("HeistCelebFailBW")
						PLAY_HEIST_FAIL_FX("HeistCelebFailBW")
						PRINTLN("[RCC MISSION] - [NETCELEBRATION] - ANIMPOSTFX_PLAY(HeistCelebFailBW, 0, TRUE)")	
					ENDIF
					IF NOT IS_BIT_SET(iLocalBoolCheck9, LBOOL9_SET_STATS_SCREEN_START_AUDIO)
						SET_CELEBRATION_AUDIO_STAGE(eCAS_CELEB_SCREEN_SCENE)
						DEACTIVATE_CELEBRATION_BLUR_PRE_CELEB_SCENE()
						SCRIPT_TRIGGER_MUSIC_EVENT_STOP()
						PRINTLN("[RCC MISSION] - [NETCELEBRATION] - triggered music event MP_MC_STOP.")
						TRIGGER_MUSIC_EVENT("HEIST_STATS_SCREEN_START")
						PRINTLN("[RCC MISSION] - [NETCELEBRATION] - triggered music event HEIST_STATS_SCREEN_START.")
						SET_BIT(iLocalBoolCheck9, LBOOL9_SET_STATS_SCREEN_START_AUDIO)
					ENDIF
					
				ELIF HAS_CELEBRATION_SHARD_FINISHED_FALL_IN()
					
					// b* 2214446 - moved from HAS_CELEBRATION_SHARD_BEGAN_TO_FALL_IN
					KILL_ALARM_SOUNDS()
					KILL_FLEECA_ALARMS()
					PRINTLN("[RCC MISSION] - [NETCELEBRATION] - HAS_CELEBRATION_SHARD_FINISHED_FALL_IN - Killing alarm sounds. ")
					
				ELIF HAS_CELEBRATION_SHARD_BEGAN_TO_FALL_AWAY()
					PRINTLN("[RCC MISSION] - [NETCELEBRATION] - HAS_CELEBRATION_SHARD_FINISHED_FALL_AWAY")	
					IF NOT ANIMPOSTFX_IS_RUNNING("HeistCelebEnd")
						PLAY_HEIST_FAIL_FX("HeistCelebEnd")
						REINIT_NET_TIMER(CSFailEndTimer)
						PRINTLN("[RCC MISSION] - [NETCELEBRATION] - ANIMPOSTFX_PLAY(HeistCelebEnd, 0, TRUE)")	
					ENDIF
					IF NOT IS_BIT_SET(iLocalBoolCheck9, LBOOL9_SET_STATS_SCREEN_STOP_AUDIO)
						
						SET_CELEBRATION_AUDIO_STAGE(eCAS_OFF)
						TRIGGER_MUSIC_EVENT("HEIST_STATS_SCREEN_STOP")
						PRINTLN("[RCC MISSION] - [NETCELEBRATION] - triggered music event HEIST_STATS_SCREEN_STOP.")
						SET_BIT(iLocalBoolCheck9, LBOOL9_SET_STATS_SCREEN_STOP_AUDIO)
					ENDIF
				ENDIF
			ENDIF
									
			DRAW_CELEBRATION_SCREEN_THIS_FRAME(sCelebrationData, FALSE)
			
			//1636875 - Delay the freeze so it syncs with the screen coming down.
			IF IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bDoDelayedCelebrationFreeze)
				PRINTLN("[RCC MISSION] - Doing ciCELEBRATION_BIT_SET_bDoDelayedCelebrationFreeze")
				IF HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTimer, CELEBRATION_SCREEN_STAT_WIPE_TIME)
					SET_SKYFREEZE_FROZEN()
					CLEAR_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bDoDelayedCelebrationFreeze)
					
					VEHICLE_INDEX PlayerVehicle 
					
					IF PLAYER_IS_THE_DRIVER_IN_A_CAR()
						PlayerVehicle = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(PlayerVehicle)
							SET_VEHICLE_STOP_INSTANTLY_WHEN_PLAYER_INACTIVE(PlayerVehicle, TRUE)
						ENDIF
					ENDIF
					
					IF DID_SPEC_BAILED_FOR_TRANSITION()
						NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_SET_INVISIBLE | NSPC_CLEAR_TASKS_NO_RAGDOLL | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
					ELSE
						IF Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)
							NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_NO_COLLISION | NSPC_CAN_BE_TARGETTED)
						ELSE
							NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_CLEAR_TASKS_NO_RAGDOLL | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
						ENDIF
					ENDIF
					FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed, TRUE)
					
					IF bLocalPlayerOK
						IF DOES_ENTITY_EXIST(PlayerVehicle)
							IF NOT IS_ENTITY_DEAD(PlayerVehicle)
								IF NETWORK_HAS_CONTROL_OF_ENTITY(PlayerVehicle)
									SET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, WEAPONTYPE_VEHICLE_PLAYER_BUZZARD)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF bDoEarlyDeathScreen
				//Allow enough time for the screen to come down before trying to preload the spectator cam (to avoid framerate issues).
				IF HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTimer, CELEBRATION_SCREEN_STAT_WIPE_TIME * 2)
					PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_SUMMARY_FINISHED - Early death screen is active, end early to allow spectator transition.")
					
					RETURN TRUE
				ENDIF
			ELSE
				//If we're going straight from the summary to a winner screen then do an early flash to help the transition.
				IF NOT sCelebrationData.bTriggeredEarlyFlash
					IF NOT bDoEarlyDeathScreen
						IF Is_Player_Currently_On_MP_Versus_Mission(LocalPlayer)
						OR Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)
						OR Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer)
							//Detach the cam on the player if they're stationary to stop the weird swings if the player turns slightly.
							IF bLocalPlayerOK
							AND DOES_CAM_EXIST(camEndScreen)
								IF GET_ENTITY_SPEED(LocalPlayerPed) < 0.1
									DETACH_CAM(camEndScreen)
									STOP_CAM_POINTING(camEndScreen)
								ENDIF
							ENDIF
						
							IF HAS_CELEBRATION_SHARD_FINISHED_FALL_AWAY()
								IF SHOULD_POSTFX_BE_WINNER_VERSION()
									PLAY_CELEB_WIN_POST_FX()
								ELSE
									PLAY_CELEB_LOSE_POST_FX()
								ENDIF
								
								START_CELEBRATION_CAMERA_ZOOM_TRANSITION(camEndScreen)
								sCelebrationData.bTriggeredEarlyFlash = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				SCRIPT_TIMER celebrationEndTimer
				INT celebrationEndTime
				
				IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
				OR Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
				OR (GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
				AND NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_RESPAWNING_DURING_FAIL))  //TEMP FIX
					celebrationEndTimer = CSFailEndTimer
					celebrationEndTime = CS_MIN_FAIL_END_TIME
				ELSE
					celebrationEndTimer = sCelebrationData.sCelebrationTimer
					celebrationEndTime = sCelebrationData.iEstimatedScreenDuration
				ENDIF
				
				// B* 2167120 - stop player falling through world - it's messing with the streaming.
				IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_ClearedTasksForOutOfVehicle)
					IF HAS_NET_TIMER_STARTED(celebrationEndTimer)
						IF HAS_NET_TIMER_EXPIRED(celebrationEndTimer, 6000)
							IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
								IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									IF NOT GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()
										SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
										PRINTLN("[RCC MISSION] - SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE), so other players don't see player pop out of vehicle when tasks are cleared immediately.")
										CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
										FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE)
										PRINTLN("[RCC MISSION] - screen frozen, safe, clearing ped tasks immediately so we can freeze the ped position. Call 0.")
										SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_ClearedTasksForOutOfVehicle)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//Once the timer finishes then progress: this depends on how many elements were added to the screen.
				IF (HAS_NET_TIMER_STARTED(celebrationEndTimer)
				AND HAS_NET_TIMER_EXPIRED(celebrationEndTimer, celebrationEndTime)
				AND HAS_CELEBRATION_SHARD_FINISHED_FALL_AWAY()) 
				OR (IS_BIT_SET(iLocalBoolCheck10, LBOOL10_RESPAWNING_DURING_FAIL) 
				AND GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
				AND HAS_CELEBRATION_SHARD_FINISHED_FALL_AWAY()) //TEMP FIX
					PRINTLN("[RCC MISSION] - [NETCELEBRATION] - Sending to sCelebrationData.eCurrentStage = CELEBRATION_STAGE_FINISHED")
					sCelebrationData.eCurrentStage = CELEBRATION_STAGE_FINISHED					
				ENDIF
			ENDIF
		BREAK
		
		CASE CELEBRATION_STAGE_FINISHED
			
			// 	Kill cinematic mode
			PRINTLN("[NETCELEBRATION] - [SAC] - [RCC MISSION] - SET_CINEMATIC_MODE_ACTIVE(FALSE) - deactivating cinematic camera at end of celebration")
			SET_CINEMATIC_MODE_ACTIVE(FALSE)
			
			IF IS_THIS_A_ROUNDS_MISSION_FOR_CELEBRATION()
			#IF IS_DEBUG_BUILD
			OR g_bFakeRound
			#ENDIF
				IF SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
				#IF IS_DEBUG_BUILD
				OR g_bFakeEndOfMatch
				#ENDIF
					
					RESET_GLOBAL_CELEBRATION_ROUNDS_DATA()
				
				ENDIF
			ENDIF
			
			IF NOT HAS_TEAM_PASSED_MISSION(MC_playerBD[iLocalPart].iteam)
				PRINTLN("[NETCELEBRATION] - [SAC] - [RCC MISSION] - team failed mission. Print 1.")
				IF IS_A_STRAND_MISSION_BEING_INITIALISED()
					PRINTLN("[NETCELEBRATION] - [SAC] - [RCC MISSION] - IS_A_STRAND_MISSION_BEING_INITIALISED() = TRUE. Print 1.")
					IF HAS_FIRST_STRAND_MISSION_BEEN_PASSED()
						PRINTLN("[NETCELEBRATION] - [SAC] - [RCC MISSION] - HAS_FIRST_STRAND_MISSION_BEEN_PASSED() = TRUE. Print 1.")
						CLEANUP_STAND_ALONE_CELEBRATION_SCREEN_GLOBALS()
					ENDIF
				ELSE
					PRINTLN("[NETCELEBRATION] - [SAC] - [RCC MISSION] - IS_A_STRAND_MISSION_BEING_INITIALISED() = FALSE. Print 1.")
					CLEANUP_STAND_ALONE_CELEBRATION_SCREEN_GLOBALS()
				ENDIF
			ENDIF
					
			RETURN TRUE
			
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC SET_DO_MISSION_OUTRO_ANIM()

	IF OK_TO_DO_MISSION_OUTRO()
		PRINTLN("[RCC MISSION] DO_OUTROS, LBOOL8_OUTRO_OK_TO_DO ")
		SET_BIT(ilocalboolcheck8, LBOOL8_OUTRO_OK_TO_DO)
	ENDIF
ENDPROC

PROC DO_OUTROS()	
	IF NOT IS_BIT_SET(ilocalboolcheck8, LBOOL8_OUTRO_DONE)
		
		IF GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
			ANIM_OUTRO_PREP(jobIntroData)
			REQUEST_ANIMATIONS_FOR_INTRO(jobIntroData)

			IF IS_BIT_SET(ilocalboolcheck8, LBOOL8_OUTRO_OK_TO_DO)

				IF HAS_STARTED_NEW_OUTRO(jobIntroData)
					PRINTLN("[CS_ANIM] [RCC MISSION] DO_OUTROS, YES")
					SET_BIT(ilocalboolcheck8, LBOOL8_OUTRO_DONE)
				ENDIF
			ENDIF
		ENDIF
	ELIF IS_BIT_SET(ilocalboolcheck8, LBOOL8_OUTRO_DONE)
		CLEANUP_OUTRO_ANIM_IF_NOT_SUITABLE(jobIntroData)	
	ENDIF
ENDPROC

FUNC BOOL SHOULD_DO_POST_MISSION_SCENE_CELEBRATION()
	
	IF NOT IS_LOADED_MISSION_LOW_RIDER_FLOW_MISSION()
	AND NOT SVM_FLOW_IS_CURRENT_MISSION_SVM_MISSION()
	AND NOT GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
	AND NOT VCM_FLOW_IS_THIS_MISSION_VCM_FLOW(g_FMMC_STRUCT.iRootContentIDHash)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_TEAM_PASSED_MISSION(MC_playerBD[iPartToUse].iteam)	
		PRINTLN("[SHOULD_DO_POST_MISSION_SCENE_CELEBRATION][RH] Not passed. iPartToUse ", iPartToUse, " MC_playerBD[iPartToUse].iteam) ", MC_playerBD[iPartToUse].iteam)
		RETURN FALSE
	ENDIF
	PRINTLN("[SHOULD_DO_POST_MISSION_SCENE_CELEBRATION][RH] Has passed. iPartToUse ", iPartToUse, " MC_playerBD[iPartToUse].iteam) ", MC_playerBD[iPartToUse].iteam)
	RETURN TRUE
	
ENDFUNC

FUNc BOOL SHOULD_PARTICIPANT_BE_ADDED_TO_MY_POST_MISSION_SCENE_HASH_ARRAY(INT iPart)
	
	IF SVM_FLOW_IS_CURRENT_MISSION_SVM_MISSION()
		
		PRINTLN("[RCC MISSION] - [PMC] - SHOULD_PARTICIPANT_BE_ADDED_TO_MY_POST_MISSION_SCENE_HASH_ARRAY = SVM_FLOW_IS_CURRENT_MISSION_SVM_MISSION = TRUE, returning TRUE")
		RETURN TRUE
		
	ELIF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
	
		PRINTLN("[RCC MISSION] - [PMC] - SHOULD_PARTICIPANT_BE_ADDED_TO_MY_POST_MISSION_SCENE_HASH_ARRAY = GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION = TRUE, returning TRUE")
		RETURN TRUE
		
	ELIF VCM_FLOW_IS_THIS_MISSION_VCM_FLOW(g_FMMC_STRUCT.iRootContentIDHash)
	
		PRINTLN("[RCC MISSION] - [PMC] - SHOULD_PARTICIPANT_BE_ADDED_TO_MY_POST_MISSION_SCENE_HASH_ARRAY = VCM_FLOW_IS_THIS_MISSION_VCM_FLOW = TRUE, returning TRUE")
		RETURN TRUE
					
	ELIF IS_LOADED_MISSION_LOW_RIDER_FLOW_MISSION()
		
		PRINTLN("[RCC MISSION] - [PMC] - SHOULD_PARTICIPANT_BE_ADDED_TO_MY_POST_MISSION_SCENE_HASH_ARRAY = IS_LOADED_MISSION_LOW_RIDER_FLOW_MISSION = TRUE")
		
		IF NOT IS_LOADED_MISSION_LOW_FLOW_MISSION(ciLOW_FLOW_MISSION_DRIVE_BY_SHOOTOUT)
			PRINTLN("[RCC MISSION] - [PMC] - SHOULD_PARTICIPANT_BE_ADDED_TO_MY_POST_MISSION_SCENE_HASH_ARRAY = IS_LOADED_MISSION_LOW_FLOW_MISSION(ciLOW_FLOW_MISSION_DRIVE_BY_SHOOTOUT) = FALSE, returning TRUE")
			RETURN TRUE
		ENDIF
		
		IF IS_BIT_SET(MC_playerBD[NATIVE_TO_INT(PARTICIPANT_ID())].iClientBitSet2, PBBOOL2_I_HAVE_SEEN_BENNY_GARAGE_POST_MISSION_SCENE)
			
			PRINTLN("[RCC MISSION] - [PMC] - SHOULD_PARTICIPANT_BE_ADDED_TO_MY_POST_MISSION_SCENE_HASH_ARRAY = PBBOOL2_I_HAVE_SEEN_BENNY_GARAGE_POST_MISSION_SCENE is set for me")
			
			IF IS_BIT_SET(MC_playerBD[ipart].iClientBitSet2, PBBOOL2_I_HAVE_SEEN_BENNY_GARAGE_POST_MISSION_SCENE)
				PRINTLN("[RCC MISSION] - [PMC] - SHOULD_PARTICIPANT_BE_ADDED_TO_MY_POST_MISSION_SCENE_HASH_ARRAY = PBBOOL2_I_HAVE_SEEN_BENNY_GARAGE_POST_MISSION_SCENE is set for participant ", ipart, ", returning TRUE")
				RETURN TRUE
			ENDIF
			
		ELSE
			
			PRINTLN("[RCC MISSION] - [PMC] - SHOULD_PARTICIPANT_BE_ADDED_TO_MY_POST_MISSION_SCENE_HASH_ARRAY = PBBOOL2_I_HAVE_SEEN_BENNY_GARAGE_POST_MISSION_SCENE is not set for me")
			
			IF NOT IS_BIT_SET(MC_playerBD[ipart].iClientBitSet2, PBBOOL2_I_HAVE_SEEN_BENNY_GARAGE_POST_MISSION_SCENE)
				PRINTLN("[RCC MISSION] - [PMC] - SHOULD_PARTICIPANT_BE_ADDED_TO_MY_POST_MISSION_SCENE_HASH_ARRAY = PBBOOL2_I_HAVE_SEEN_BENNY_GARAGE_POST_MISSION_SCENE is not set for participant ", ipart, ", returning TRUE")
				RETURN TRUE
			ENDIF
			
		ENDIF
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_RESURRECT_PLAYER_AT_END()

	IF USING_SHOWDOWN_POINTS()
	OR g_FMMC_STRUCT.iAdversaryModeType > 0
	OR (Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer) OR Is_Player_Currently_On_MP_Versus_Mission(LocalPlayer))
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL HAS_CELEBRATION_SCREEN_FINISHED()
	
	INT iPlayerNameCount, iPart
	
	#IF IS_DEBUG_BUILD
	IF g_SkipCelebAndLbd
		SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bCelebrationSummaryComplete)
		SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bCelebrationUnfrozeSkycam)
		SET_BIT(iLocalBoolCheck3,LBOOL3_SHOWN_RESULTS)
		SET_BIT(iLocalBoolCheck2,LBOOL2_LB_CAM_READY)
		PRINTLN("HAS_CELEBRATION_SCREEN_FINISHED() - SKIPPING DUE TO g_SkipCelebAndLbd")
		RETURN TRUE
	ENDIF
	#ENDIF
	
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_STUNTING_PACK(g_FMMC_STRUCT.iAdversaryModeType)
		IF HAS_TEAM_PASSED_MISSION(MC_playerBD[iLocalPart].iTeam) 
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_WON_HUNTING_PACK)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_WON_HUNTING_PACK, TRUE)
				PRINTLN("PACKED_MP_BOOL_WON_HUNTING_PACK set to true in FUNC BOOL HAS_CELEBRATION_SCREEN_FINISHED() ")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
		IF NOT IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Cleanup)
			PRINTLN("[LM][SPEC_SPEC][HAS_CELEBRATION_SCREEN_FINISHED] - We are in Roaming Spectator Mode. Requesting Cleanup!")
			SET_BIT(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Cleanup)
			SET_BIT(g_iSpecialSpectatorBS2, g_ciSpecial_Spectator_BS2_Dont_Cleanup_Arena_Activities)
		ENDIF
	ENDIF
	
	IF IS_ENTITY_A_GHOST(localPlayerPed)
		PRINTLN("[GHOST CLEANUP] - HAS_CELEBRATION_SCREEN_FINISHED - Clearing Ghost effect")
		VEHICULAR_VENDETTA_SET_LOCAL_PLAYER_AS_GHOST(FALSE)	
	ENDIF
	
	IF NOT IS_PED_INJURED(LocalPlayerPed)
	AND NOT IS_BIT_SET(iLocalBoolCheck28, LBOOL28_ALPHA_RESET_DONE)
		IF GET_ENTITY_ALPHA(localPlayerPed) < 255
		OR NOT IS_ENTITY_VISIBLE(LocalPlayerPed)		
			PRINTLN("[GHOST CLEANUP] - HAS_CELEBRATION_SCREEN_FINISHED - Clearing SPAWN_PROTECTION_ALPHA. Current alpha is: ", GET_ENTITY_ALPHA(localPlayerPed))
			
			IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
				VEHICLE_INDEX vehPlayerIsIn = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)	
				IF NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(vehPlayerIsIn)
					IF NETWORK_DOES_NETWORK_ID_EXIST(VEH_TO_NET(vehPlayerIsIn))
						SET_NETWORK_VEHICLE_RESPOT_TIMER(VEH_TO_NET(vehPlayerIsIn), 0, FALSE, FALSE)
						NETWORK_SET_LOCAL_PLAYER_INVINCIBLE_TIME(0)
						PRINTLN("[GHOST CLEANUP] - HAS_CELEBRATION_SCREEN_FINISHED - Clearing Spawn Protection Flash.")
					ENDIF
				ENDIF
			ENDIF
			
			BROADCAST_FMMC_SPAWN_PROTECTION_ALPHA(FALSE, iLocalPart, GET_FRAME_COUNT())
			RESET_ENTITY_ALPHA(LocalPlayerPed)
			
			IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
				IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				AND DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
					RESET_ENTITY_ALPHA(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
				ENDIF
			ENDIF
			iAlphaResetAttempts++
			PRINTLN("[HAS_CELEBRATION_SCREEN_FINISHED] - iAlphaResetAttempts = ", iAlphaResetAttempts)
			IF iAlphaResetAttempts >= 10
				SET_BIT(iLocalBoolCheck28, LBOOL28_ALPHA_RESET_DONE)
				PRINTLN("[HAS_CELEBRATION_SCREEN_FINISHED] - iAlphaResetAttempts >= 10! Setting LBOOL28_ALPHA_RESET_DONE")
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(iLocalBoolCheck28, LBOOL28_ALPHA_RESET_DONE)
				SET_BIT(iLocalBoolCheck28, LBOOL28_ALPHA_RESET_DONE)
				PRINTLN("[HAS_CELEBRATION_SCREEN_FINISHED] - setting LBOOL28_ALPHA_RESET_DONE Entity is visible.")
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(iLocalBoolCheck28, LBOOL28_ALPHA_RESET_DONE)
			SET_BIT(iLocalBoolCheck28, LBOOL28_ALPHA_RESET_DONE)
			PRINTLN("[HAS_CELEBRATION_SCREEN_FINISHED] - setting LBOOL28_ALPHA_RESET_DONE Don't Care.")
		ENDIF
	ENDIF
	
	IF( IS_BIT_SET( iLocalBoolCheck11, LBOOL11_TOGGLE_RENDER_PHASE_NEXT_FRAME ) )
		PRINTLN("[RCC MISSION][HAS_CELEBRATION_SCREEN_FINISHED] Renderphase Paused. 1")
		TOGGLE_RENDERPHASES(FALSE, TRUE)		
		CLEAR_BIT(iLocalBoolCheck11, LBOOL11_TOGGLE_RENDER_PHASE_NEXT_FRAME)
	ENDIF
	
	IF (Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer) OR Is_Player_Currently_On_MP_Versus_Mission(LocalPlayer))
	AND NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_WINNER_LOSER_SHARD_RENDERPHASE_INITIAL_PAUSE)
	AND NOT IS_PED_GETTING_UP(LocalPlayerPed)
	AND IS_BIT_SET(iLocalBoolCheck28, LBOOL28_ALPHA_RESET_DONE)
	AND NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bCelebrationSummaryComplete)
		// Twice due to a weird Graphics code bug.
		TOGGLE_PAUSED_RENDERPHASES(FALSE)
		TOGGLE_PAUSED_RENDERPHASES(FALSE)
		SET_BIT(iLocalBoolCheck27, LBOOL27_WINNER_LOSER_SHARD_RENDERPHASE_INITIAL_PAUSE)
		PRINTLN("[RCC MISSION][HAS_CELEBRATION_SCREEN_FINISHED] Renderphase Paused. 2")
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_SLASHERS(g_FMMC_STRUCT.iAdversaryModeType)
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_RESET_TO_DEFAULT_OUTFITS_ON_MISSION_END)
		IF GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()
		AND NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_CELEBRATION_SCREEN_OUTFIT_CHANGE)
			TOGGLE_RENDERPHASES(FALSE, TRUE)
			PRINTLN("[RCC MISSION][HAS_CELEBRATION_SCREEN_FINISHED] Renderphase Paused. 3")
			IF NOT IS_XBOX_PLATFORM() //Added for url:bugstar:4521637 - Render phases are fucked on xbox one.
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF

	IF NOT GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()
		// For modes which change an outfit mid-game.
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_SLASHERS(g_FMMC_STRUCT.iAdversaryModeType)
		OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_RESET_TO_DEFAULT_OUTFITS_ON_MISSION_END)
		OR (IS_THIS_ROCKSTAR_MISSION_NEW_VS_TRADING_PLACES_REMIX(g_FMMC_STRUCT.iAdversaryModeType) AND IS_LOCAL_PLAYER_ANY_SPECTATOR())
			IF NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_CELEBRATION_SCREEN_OUTFIT_CHANGE)
				PRINTLN("[RCC MISSION][HAS_CELEBRATION_SCREEN_FINISHED] Changing outfit back to default.")
				
				sApplyOutfitData.pedID 		= LocalPlayerPed
				sApplyOutfitData.eOutfit 	= INT_TO_ENUM(MP_OUTFIT_ENUM, GET_LOCAL_DEFAULT_OUTFIT(MC_playerBD[iPartToUse].iteam))
				sApplyOutfitData.eMask   	= INT_TO_ENUM(MP_OUTFIT_MASK_ENUM, GET_LOCAL_DEFAULT_MASK(MC_playerBD[iPartToUse].iteam))
			
				IF SET_PED_MP_OUTFIT(sApplyOutfitData)
				AND NOT IS_PED_INJURED(localPlayerPed)
				AND NOT IS_PED_DEAD_OR_DYING(localPlayerPed)
					PRINTLN("[RCC MISSION][HAS_CELEBRATION_SCREEN_FINISHED] Outfit changed back to the default. We can now progress.")
					SET_BIT(iLocalBoolCheck26, LBOOL26_CELEBRATION_SCREEN_OUTFIT_CHANGE)
					BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_PLAYER_CHANGED_APPEARANCE, ALL_PLAYERS())
				ELSE
					PRINTLN("[RCC MISSION][HAS_CELEBRATION_SCREEN_FINISHED] Returning False. Our outfit is still wrong.")
					RETURN FALSE
				ENDIF		
			ENDIF
		ENDIF	
	ENDIF
		
	SET_DO_MISSION_OUTRO_ANIM()
	GET_HEIST_COMPLETION_RANKINGS()
	POPULATE_RESULTS()
	COPY_CELEBRATION_DATA_TO_GLOBALS()
	
	// For lamar missions, lowrider pack.
	IF SHOULD_DO_POST_MISSION_SCENE_CELEBRATION()
		IF NOT IS_PLAYER_SCTV(PLAYER_ID())
		AND NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(PLAYER_ID())
		AND IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
			IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId = (-1)
				PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - [PMC] - SHOULD_DO_POST_MISSION_SCENE_CELEBRATION = TRUE, saving out post mission scene data.")
				
				// Setup the post mission scene ID.
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId = g_FMMC_STRUCT.iPostMissionSceneId[MC_playerBD[iPartToUse].iteam]
				
				IF IS_LOADED_MISSION_LOW_RIDER_FLOW_MISSION()
					IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId < SIS_POST_LOWRIDER_MISSION_1
					OR g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId > SIS_POST_LOWRIDER_MISSION_8
						PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - [PMC] - g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId, ", not a valid lowrider location, setting to SIS_POST_LOWRIDER_MISSION_2.")
						g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId = SIS_POST_LOWRIDER_MISSION_2 // g_FMMC_STRUCT.iPostMissionSceneId[MC_playerBD[iPartToUse].iteam]
					ENDIF
				ELIF SVM_FLOW_IS_CURRENT_MISSION_SVM_MISSION()
					IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId < SIS_SVM_BLAZER5
					OR g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId > SIS_SVM_WASTELANDER
						PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - [PMC] - g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId, ", not a valid SVM location, setting to SIS_SVM_DUNE4.")
						g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId = SIS_SVM_DUNE4
					ENDIF
				ELIF VCM_FLOW_IS_THIS_MISSION_VCM_FLOW(g_FMMC_STRUCT.iRootContentIDHash)
					IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId < SIS_POST_CASINO_MISSION_1
					OR g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId > SIS_POST_CASINO_MISSION_6
						PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - [PMC] - g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId, ", not a valid SVM location, setting to SIS_SVM_DUNE4.")
						g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId = SIS_POST_CASINO_MISSION_2
					ENDIF
				ENDIF
				PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - [PMC] - g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId)
				
				IF IS_LOADED_MISSION_LOW_FLOW_MISSION(ciLOW_FLOW_MISSION_DRIVE_BY_SHOOTOUT)		
//					IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId = SIS_POST_LOWRIDER_MISSION_1
						IF NOT GET_MP_BOOL_CHARACTER_STAT(MP_STAT_HAS_WATCHED_BENNY_CUTSCE) 
							g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlayBennyPreScene = TRUE
							PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - [PMC] - g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId = SIS_POST_LOWRIDER_MISSION_1")
							PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - [PMC] - GET_MP_BOOL_CHARACTER_STAT(MP_STAT_HAS_WATCHED_BENNY_CUTSCE) = FALSE, setting g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlayBennyPreScene = TRUE")
						ENDIF
//					ENDIF
				ENDIF
				
				// Get the players to be in the scene.
				REPEAT NUM_NETWORK_PLAYERS iPart
					PARTICIPANT_INDEX partId = INT_TO_NATIVE(PARTICIPANT_INDEX, iPart)
					IF NETWORK_IS_PARTICIPANT_ACTIVE(partId)
						PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - [PMC] - participant ", iPart, " is active.")
						PLAYER_INDEX playerId = NETWORK_GET_PLAYER_INDEX(partId)
						IF NETWORK_IS_PLAYER_ACTIVE(playerId)
						AND NOT IS_PLAYER_SCTV(playerId)
						AND NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(playerId)
							PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - [PMC] - NETWORK_IS_PLAYER_ACTIVE = TRUE")						
							IF SHOULD_PARTICIPANT_BE_ADDED_TO_MY_POST_MISSION_SCENE_HASH_ARRAY(iPart)
								PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - [PMC] - SHOULD_PARTICIPANT_BE_ADDED_TO_MY_POST_MISSION_SCENE_HASH_ARRAY(", iPart, ") = TRUE.")
								g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPlayerNameHash[iPlayerNameCount] = NETWORK_HASH_FROM_PLAYER_HANDLE(playerId)
								PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - [PMC] - participant ", iPart, " added NETWORK_HASH_FROM_PLAYER_HANDLE to g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPlayerNameHash[", iPlayerNameCount, "]")
								iPlayerNameCount++
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
				
			ELSE
				
				// Move the player to the scene coords as soon as we've paused the screen.
				IF NETWORK_IS_PLAYER_ACTIVE(PLAYER_ID())
					IF NOT IS_PLAYER_SCTV(PLAYER_ID())
						IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_PlacedPlayerForLowriderPostScene)
							IF NOT GET_TOGGLE_PAUSED_RENDERPHASES_STATUS() // sCelebrationData.eCurrentStage >= CELEBRATION_STAGE_PLAYING
								IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
									VECTOR vSceneCoords
									vSceneCoords = GET_SYNCED_INTERACTION_SCENE_COORDS(g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId)
									CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
									SET_ENTITY_COORDS(PLAYER_PED_ID(), vSceneCoords, FALSE)
									FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
									IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
										Clear_All_Generated_MP_Headshots(FALSE)
										SET_PLAYER_CLOTHES_FOR_RETURN_TO_FREEMODE()
										PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - [PMC] - reset clothes for freemode and cleared generated headshots.")
									ENDIF
									SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_PlacedPlayerForLowriderPostScene)
									PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - [PMC] - we are doing a lamar lowrider post mission scene, renderpahses are paused, put player at GET_SYNCED_INTERACTION_SCENE_COORDS(", g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId, ") = ", vSceneCoords)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - [PMC] - player is SCTV, not saving out lowrider post mission scene data.")
		ENDIF
	ELSE
		PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - [PMC] - SHOULD_DO_POST_MISSION_SCENE_CELEBRATION = FALSE, not saving out lowrider post mission scene data.")
//		IF HAS_TEAM_PASSED_MISSION(MC_playerBD[iPartToUse].iteam)
//			PRINTLN("[RCC MISSION] - [PMC] - HAS_TEAM_PASSED_MISSION(MC_playerBD[iPartToUse].iteam) = TRUE")
//			IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId = (-1) // B* url:bugstar:2508765
//				g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId = g_FMMC_STRUCT.iPostMissionSceneId[MC_playerBD[iPartToUse].iteam]
//				PRINTLN("[RCC MISSION] - [PMC] - saved iPostMissionSceneId = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId)
//			ENDIF
//		ELSE
//			PRINTLN("[RCC MISSION] - [PMC] - HAS_TEAM_PASSED_MISSION(MC_playerBD[iPartToUse].iteam) = FALSE, leaving g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostMissionSceneId = (-1)")
//		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_POWERPLAY_PICKUP_SOUNDS)
		IF NETWORK_IS_GAME_IN_PROGRESS()
		AND NETWORK_IS_HOST_OF_THIS_SCRIPT()
			IF NOT IS_BIT_SET(iLocalBoolCheck17, LBOOL17_HAS_PICKED_PP_WINNER_ANNOUNCER)
				PICK_POWERPLAY_WINNER_ANNOUNCER()
			ENDIF
			IF NOT IS_BIT_SET(iLocalBoolCheck17, LBOOL17_HAS_PICKED_PP_EXIT_ANNOUNCER)
				PICK_POWERPLAY_EXIT_ANNOUNCER()
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(iLocalBoolCheck17, LBOOL17_KILL_CONVERSATIONS_BEFORE_WINNER_ANNOUNCER)
			SET_BIT(iLocalBoolCheck17, LBOOL17_KILL_CONVERSATIONS_BEFORE_WINNER_ANNOUNCER)
			PRINTLN("[RCC MISSION] HAS_CELEBRATION_SCREEN_FINISHED - Killing conversations so winner announcer is ready to speak.")
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		ENDIF
	
		IF NOT IS_BIT_SET(iLocalBoolCheck17, LBOOL17_HAS_PLAYED_PP_WINNER_ANNOUNCER)
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				PLAY_POWERPLAY_WINNER_ANNOUNCER()
			ENDIF
		ELIF NOT IS_BIT_SET(iLocalBoolCheck17, LBOOL17_HAS_PLAYED_PP_EXIT_ANNOUNCER)
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				PLAY_POWERPLAY_EXIT_ANNOUNCER()
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
	OR DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(PLAYER_ID())
		IF IS_BIT_SET(iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE_OVER)
			SET_SKYSWOOP_UP()
			PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - [PMC] - IS_PLAYER_SCTV OR DID_PLAYER_JOIN_MISSION_AS_SPECTATOR Calling SET_SKYSWOOP_UP")
		ENDIF
	ENDIF
	
	// Fix for 2684556
	SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed, TRUE)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciMISSION_POST_MISSION_SCENE) 
		PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - [PMC] - IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciMISSION_POST_MISSION_SCENE) = TRUE, set flag g_TransitionSessionNonResetVars.sPostMissionCleanupData.bBeachAfterSumoSpawnFlag")
		g_TransitionSessionNonResetVars.sPostMissionCleanupData.bBeachAfterSumoSpawnFlag = TRUE
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - [PMC] - IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciMISSION_POST_MISSION_SCENE) = FALSE")
	#ENDIF
	ENDIF
	
	IF IS_ARENA_WARS_JOB()
		g_TransitionSessionNonResetVars.sPostMissionCleanupData.bArenaPostMissionSpawn = TRUE
		PRINTLN("[ARENA][MC] g_TransitionSessionNonResetVars.sPostMissionCleanupData.bArenaPostMissionSpawn - TRUE")
	ENDIF
	
	// No pausing the celebration screen.
	IF IS_PAUSE_MENU_ACTIVE()
		SET_FRONTEND_ACTIVE(FALSE)
		PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - [SAC] - called SET_FRONTEND_ACTIVE(FALSE) to remove pause menu for celebration screen.")
	ENDIF
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE)
	
	IF IS_A_STRAND_MISSION_BEING_INITIALISED()
		SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bCelebrationSummaryComplete)
		SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bCelebrationUnfrozeSkycam)
		SET_BIT(iLocalBoolCheck3,LBOOL3_SHOWN_RESULTS)
		PRINTLN("[RCC MISSION] HAS_CELEBRATION_SCREEN_FINISHED - Returning true IS_A_STRAND_MISSION_BEING_INITIALISED.")
		RETURN TRUE
	ENDIF
	
	// If this causes any issues let me know please [LM] we may have to remove it.
	// url:bugstar:5400867
	IF IS_PLAYER_TELEPORT_ACTIVE()
	AND (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPGlobals.g_NetWarpTime) < 10000)
		PRINTLN("[LM][RCC MISSION] HAS_CELEBRATION_SCREEN_FINISHED - IS_PLAYER_TELEPORT_ACTIVE = TRUE AND Time Spent Teleporting: ", (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPGlobals.g_NetWarpTime)), " Returning FALSE to stall so we can finish before cam freeze.")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bCelebrationSummaryComplete)
	
		CELEBRATION_SCREEN_TYPE eCelebType
		IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
		OR Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
		
			eCelebType = CELEBRATION_HEIST
			PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - [NETCELEBRATION] - USING HEIST CELEBRATION SCREEN 1")	
			

		ELIF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		
			eCelebType = CELEBRATION_GANGOPS
			PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - [NETCELEBRATION] - USING GANGOPS CELEBRATION SCREEN 1")
		
		
		ELSE
	
			eCelebType = CELEBRATION_STANDARD
			PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - [NETCELEBRATION] - USING STANDARD CELEBRATION SCREEN 1")
		
		ENDIF
		
		IF Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)
			UPDATE_PLAYER_IN_AIR_VEHICLE_DURING_CELEBRATION_SCREEN()
		ENDIF
		
		IF NOT g_bCelebrationScreenIsActive

			BOOL bTimeToStartScreen = FALSE
			
			IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_PLACED_REVERSE_ANGLE_CAMERA)
				bCreatedCustomCelebrationCam = FALSE
			ENDIF
			
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
			OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_WARFARE(g_FMMC_STRUCT.iAdversaryModeType)
			OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_SLASHERS(g_FMMC_STRUCT.iAdversaryModeType)
			OR USING_SHOWDOWN_POINTS()
				bTimeToStartScreen = TRUE
			ENDIF
			
			//Do a deathmatch-style cam.
			IF NOT IS_PLAYER_SPECTATING(LocalPlayer)
			AND NOT IS_BIT_SET(iLocalBoolCheck11, LBOOL11_PLAYER_IS_RESPAWNING)
				IF (Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer) OR Is_Player_Currently_On_MP_Versus_Mission(LocalPlayer))
				AND HAS_TEAM_PASSED_MISSION(MC_playerBD[iPartToUse].iteam)
					
					IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_RESPAWNING_DURING_FAIL)
						IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_PLACED_REVERSE_ANGLE_CAMERA)
							bTimeToStartScreen = TRUE
						ENDIF
					ELSE
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF ACTIVATE_CINEMATIC_CAM_FOR_IN_VEHICLE_CELEBRATION_SCREEN() // 2229861
								PRINTLN("[RCC MISSION], HAS_CELEBRATION_SCREEN_FINISHED - ACTIVATE_CINEMATIC_CAM_FOR_IN_VEHICLE_CELEBRATION_SCREEN = TRUE 1")
								bTimeToStartScreen = TRUE
							ELSE
								PRINTLN("[RCC MISSION], HAS_CELEBRATION_SCREEN_FINISHED - ACTIVATE_CINEMATIC_CAM_FOR_IN_VEHICLE_CELEBRATION_SCREEN = FALSE 1")
							ENDIF
						ELSE
							IF (IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUGBY(g_FMMC_STRUCT.iRootContentIDHash)
							AND IS_BIT_SET(iLocalBoolCheck16, LBOOL16_YOU_ARE_THE_LAST_DELIVERER))
							OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
							OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_WARFARE(g_FMMC_STRUCT.iAdversaryModeType)
							OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRQUOTA(g_FMMC_STRUCT.iAdversaryModeType)
							OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_SLASHERS(g_FMMC_STRUCT.iAdversaryModeType)
							OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
								PRINTLN("[RCC MISSION], HAS_CELEBRATION_SCREEN_FINISHED - PLAYING RUGBY, SKIPPING PLACE_CAMERA_FOR_CELEBRATION_SCREEN ")
								bTimeToStartScreen = TRUE
							ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TRADING_PLACES_REMIX(g_FMMC_STRUCT.iAdversaryModeType) //url:bugstar:4978075
								bTimeToStartScreen = TRUE
								TOGGLE_RENDERPHASES(FALSE)
								PRINTLN("[RCC MISSION], HAS_CELEBRATION_SCREEN_FINISHED - SKIPPING PLACE_CAMERA_FOR_CELEBRATION_SCREEN and clearing renderphase pause.")
							ELSE
								IF PLACE_CAMERA_FOR_CELEBRATION_SCREEN(sCelebrationData, camEndScreen, bCreatedCustomCelebrationCam)
									bTimeToStartScreen = TRUE
								ELSE
									PRINTLN("[RCC MISSION], HAS_CELEBRATION_SCREEN_FINISHED - PLACE_CAMERA_FOR_CELEBRATION_SCREEN ")
								ENDIF
							ENDIF
						ENDIF
							
					ENDIF
					
				//ELIF (Is_Player_Currently_On_MP_Heist(LocalPlayer) OR Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer))
				ELIF eCelebType = CELEBRATION_HEIST
				OR eCelebType = CELEBRATION_GANGOPS
					
					IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_RESPAWNING_DURING_FAIL)
						IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_PLACED_REVERSE_ANGLE_CAMERA)
							bTimeToStartScreen = TRUE
						ENDIF
					ELSE
						IF NOT HAS_TEAM_PASSED_MISSION(MC_playerBD[iPartToUse].iteam)
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) // 2198645
								IF ACTIVATE_CINEMATIC_CAM_FOR_IN_VEHICLE_CELEBRATION_SCREEN( FALSE ) // 2229861
									SET_BIT( iLocalBoolCheck11, LBOOL11_TOGGLE_RENDER_PHASE_NEXT_FRAME )
									PRINTLN("[RCC MISSION], HAS_CELEBRATION_SCREEN_FINISHED - ACTIVATE_CINEMATIC_CAM_FOR_IN_VEHICLE_CELEBRATION_SCREEN = TRUE 2")
									bTimeToStartScreen = TRUE
								ELSE
									PRINTLN("[RCC MISSION], HAS_CELEBRATION_SCREEN_FINISHED - ACTIVATE_CINEMATIC_CAM_FOR_IN_VEHICLE_CELEBRATION_SCREEN = FALSE 2")
								ENDIF
							ELSE
								IF PLACE_CAMERA_FOR_CELEBRATION_SCREEN(sCelebrationData, camEndScreen, bCreatedCustomCelebrationCam)
									PRINTLN("[RCC MISSION], HAS_CELEBRATION_SCREEN_FINISHED - HEISTS PLACE_CAMERA_FOR_CELEBRATION_SCREEN = TRUE ")
									bTimeToStartScreen = TRUE
								ELSE
									PRINTLN("[RCC MISSION], HAS_CELEBRATION_SCREEN_FINISHED - HEISTS PLACE_CAMERA_FOR_CELEBRATION_SCREEN =  FALSE ")
								ENDIF
							ENDIF
							
						ELSE
							bTimeToStartScreen = TRUE
						ENDIF
					ENDIF
				ELIF (Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer) OR Is_Player_Currently_On_MP_Versus_Mission(LocalPlayer))
				AND NOT HAS_TEAM_PASSED_MISSION(MC_playerBD[iPartToUse].iteam)
					IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_RESPAWNING_DURING_FAIL)
						IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_PLACED_REVERSE_ANGLE_CAMERA)
							bTimeToStartScreen = TRUE
						ENDIF
					ELSE
						IF IS_NET_PLAYER_OK(PLAYER_ID())
							IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
								IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUGBY(g_FMMC_STRUCT.iRootContentIDHash)
								AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
								AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_CONDEMNED(g_FMMC_STRUCT.iAdversaryModeType)
								AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_WARFARE(g_FMMC_STRUCT.iAdversaryModeType)
								AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRQUOTA(g_FMMC_STRUCT.iAdversaryModeType)
								AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_SLASHERS(g_FMMC_STRUCT.iAdversaryModeType)
								AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
								AND NOT USING_SHOWDOWN_POINTS()
								AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TRADING_PLACES_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
								AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_ARENA_CTF(g_FMMC_STRUCT.iAdversaryModeType)
									TOGGLE_RENDERPHASES(TRUE)
								ELSE
									IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
									AND NOT IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_IN_BMBFB_SUDDEN_DEATH)
										TOGGLE_RENDERPHASES(TRUE)
									ENDIF
								ENDIF
								
								IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
								OR USING_SHOWDOWN_POINTS()
								OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_TRADING_PLACES_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
									bTimeToStartScreen = TRUE
									TOGGLE_RENDERPHASES(FALSE)
									PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - Setting bTimeToStartScreen to TRUE because this is Overtime or Showdown")
								ELSE
									PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - TOGGLE_RENDERPHASES TRUE")
									IF NOT NETWORK_IS_IN_SPECTATOR_MODE()
										//SET_ENTITY_VISIBLE(LocalPlayerPed, TRUE)
										//CLEAR_BIT(iLocalBoolCheck10, LBOOL10_PLACED_REVERSE_ANGLE_CAMERA)
									ENDIF
									PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - SET_ENTITY_VISIBLE TRUE")
									IF PLACE_CAMERA_FOR_CELEBRATION_SCREEN(sCelebrationData, camEndScreen, bCreatedCustomCelebrationCam)
										bTimeToStartScreen = TRUE
									ELSE
										PRINTLN("[RCC MISSION], HAS_CELEBRATION_SCREEN_FINISHED - PLACE_CAMERA_FOR_CELEBRATION_SCREEN ")
									ENDIF
								ENDIF
							ELSE
								PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - PLAYER IS SPECTATING - skipping PLACE_CAMERA_FOR_CELEBRATION_SCREEN. Setting bTimeToStartScreen = TRUE.")
																
								IF (NOT CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
								AND CONTENT_IS_USING_CASINO_HEIST_INTERIOR()) // These two checks are to catch all the adversary modes in the casino.
									TOGGLE_RENDERPHASES(FALSE)
								ELIF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUGBY(g_FMMC_STRUCT.iRootContentIDHash)
								AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_CONDEMNED(g_FMMC_STRUCT.iAdversaryModeType)
								AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRQUOTA(g_FMMC_STRUCT.iAdversaryModeType)
								AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_SLASHERS(g_FMMC_STRUCT.iAdversaryModeType)
								AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
								AND NOT USING_SHOWDOWN_POINTS()
								AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TRADING_PLACES_REMIX(g_FMMC_STRUCT.iAdversaryModeType)								
									TOGGLE_RENDERPHASES(TRUE) // This should actually be false but too volatile to change now in 2020...
								ELSE
									IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
									AND NOT IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_IN_BMBFB_SUDDEN_DEATH)
										TOGGLE_RENDERPHASES(TRUE)
									ENDIF
								ENDIF
								
								bTimeToStartScreen = TRUE
							ENDIF
						ELSE
							bTimeToStartScreen = TRUE
							PRINTLN("[RCC MISSION], HAS_CELEBRATION_SCREEN_FINISHED - skipping PLACE_CAMERA_FOR_CELEBRATION_SCREEN, player is dead. Setting bTimeToStartScreen = TRUE.")
						ENDIF					
					ENDIF					
				ELSE
					bTimeToStartScreen = TRUE
				ENDIF
			ELSE
				bTimeToStartScreen = TRUE
			ENDIF
			
			IF bTimeToStartScreen
			
				PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - bTimeToStartScreen: is TRUE")
							
				POPULATE_RESULTS()
				
				IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bStartedCelebrationBeforeCutsceneEnded)
					INITIALISE_CELEBRATION_SCREEN_DATA(sCelebrationData)
				ENDIF
				
				REQUEST_CELEBRATION_SCREEN(sCelebrationData, eCelebType)
				
				SET_CELEBRATION_SCREEN_AS_ACTIVE(TRUE)
				KILL_UI_FOR_CELEBRATION_SCREEN(IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_POWERPLAY_PICKUP_SOUNDS))
				HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
				PRINTLN("[JS][url:bugstar:3998944] - HAS_CELEBRATION_SCREEN_FINISHED - 4")
				
				// This might cause issues where we see under the world but we need this here so players are not stuck in a Black Screen.
				// Normally happens because we're sent to spectator and before we get a target we end the mission.
				IF IS_SCREEN_FADED_OUT()
					PRINTLN("[LM][url:bugstar:4372431 - HAS_CELEBRATION_SCREEN_FINISHED - Emergancy Screen Fade In. Celebration Screen should start.")
					DO_SCREEN_FADE_IN_FOR_TRANSITION(250)
				ENDIF
				
				TOGGLE_PLAYER_DAMAGE_OVERLAY(FALSE)
				
				IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bStartedCelebrationBeforeCutsceneEnded)
					IF bCreatedCustomCelebrationCam
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
						PRINTLN("[RCC MISSION], HAS_CELEBRATION_SCREEN_FINISHED - PLACE_CAMERA_FOR_CELEBRATION_SCREEN RENDER_SCRIPT_CAMS(TRUE, FALSE) ")
						
						IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(g_FMMC_STRUCT.iAdversaryModeType)
							IF GET_REQUESTINGNIGHTVISION()
							OR GET_USINGNIGHTVISION()
								ENABLE_NIGHTVISION(VISUALAID_OFF, VISUALAID_SOUND_ON)
								DISABLE_NIGHT_VISION_CONTROLLER(TRUE)
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT ANIMPOSTFX_IS_RUNNING("MP_Celeb_Win")
					AND NOT ANIMPOSTFX_IS_RUNNING("MP_Celeb_Lose")
					AND NOT ANIMPOSTFX_IS_RUNNING("HeistCelebFail")
																			
						IF SHOULD_POSTFX_BE_WINNER_VERSION()
							PLAY_CELEB_WIN_POST_FX()
						ELSE
							
							IF eCelebType = CELEBRATION_HEIST
							OR eCelebType = CELEBRATION_GANGOPS
								PLAY_HEIST_FAIL_FX("HeistCelebFail")
								IF NOT bCreatedCustomCelebrationCam
									IF( NOT IS_BIT_SET( iLocalBoolCheck11, LBOOL11_TOGGLE_RENDER_PHASE_NEXT_FRAME ) )
										TOGGLE_RENDERPHASES(FALSE, TRUE)
									ENDIF
								ELSE
									CLEAR_BIT( iLocalBoolCheck11, LBOOL11_TOGGLE_RENDER_PHASE_NEXT_FRAME )
								ENDIF
								REINIT_NET_TIMER(CSMinPulseTimer)
								PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - [NETCELEBRATION] - ANIMPOSTFX_PLAY(HeistCelebFail, 0, TRUE) 2")
							ELSE
								PLAY_CELEB_LOSE_POST_FX()
							ENDIF
							
							
							IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_BVS1(g_FMMC_STRUCT.iRootContentIDHash)
								IF GET_REQUESTINGNIGHTVISION()
								OR GET_USINGNIGHTVISION()
									ENABLE_NIGHTVISION(VISUALAID_OFF)
									DISABLE_NIGHT_VISION_CONTROLLER(TRUE) // Added under instruction of Alwyn for B*2196822
								ENDIF
							ENDIF
							
							/*
							IF GET_REQUESTINGNIGHTVISION()
							OR GET_USINGNIGHTVISION()
								ENABLE_NIGHTVISION(VISUALAID_OFF)
								DISABLE_NIGHT_VISION_CONTROLLER(TRUE) // Added under instruction of Alwyn for B*2196822
							ENDIF
							*/
						ENDIF
					
					ENDIF
					
					RESET_NET_TIMER(sCelebrationData.sCelebrationTransitionTimer)
					START_NET_TIMER(sCelebrationData.sCelebrationTransitionTimer)
					
					PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS", FALSE)
					
					PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - [NETCELEBRATION] - STARTING CELEBRATION TIMER")
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - bTimeToStartScreen: is FALSE")
			ENDIF

		ELSE
			IF HAS_CELEBRATION_SUMMARY_FINISHED()
				RESET_CELEBRATION_SCREEN_STATE(sCelebrationData)
				
				RESET_NET_TIMER(sCelebrationData.sCelebrationTransitionTimer)
				START_NET_TIMER(sCelebrationData.sCelebrationTransitionTimer)
				
				CLEAR_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bCelebrationUnfrozeSkycam)
				SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bCelebrationSummaryComplete)
				
				PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - [NETCELEBRATION] - HAS_CELEBRATION_SUMMARY_FINISHED returning true... Setting ciCELEBRATION_BIT_SET_bCelebrationSummaryComplete")
			ELSE
				PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - [NETCELEBRATION] - HAS_CELEBRATION_SUMMARY_FINISHED returning false...")
			ENDIF
			
		ENDIF
	
	ELSE
		
		HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
				
		IF g_bCelebrationScreenIsActive
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_SLASHERS(g_FMMC_STRUCT.iAdversaryModeType)
			OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_RESET_TO_DEFAULT_OUTFITS_ON_MISSION_END)
			OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_TRADING_PLACES_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
				IF NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_CELEBRATION_SCREEN_OUTFIT_CHANGE_BROADCAST)
					PRINTLN("[RCC MISSION][LBOOL26_CELEBRATION_SCREEN_OUTFIT_CHANGE_BROADCAST] - HAS_CELEBRATION_SCREEN_FINISHED - CALLING REFRESH...")
					SET_BIT(iLocalBoolCheck26, LBOOL26_CELEBRATION_SCREEN_OUTFIT_CHANGE_BROADCAST)
					BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_PLAYER_CHANGED_APPEARANCE, ALL_PLAYERS())
				ENDIF
			ENDIF
		
			BOOL bSafeToContinue
			PRINTLN("[JS][url:bugstar:3998944] - HAS_CELEBRATION_SCREEN_FINISHED - 1")
			//Only display the winner screen if the player passed the mission.
			IF SHOULD_DO_POST_MISSION_SCENE_CELEBRATION()
				PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - [PMC] - IS_LOADED_MISSION_LOW_RIDER_FLOW_MISSION = TRUE")
				bSafeToContinue = TRUE
			ELIF (g_bVSMission
			OR Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)
			OR Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer) )
				IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
				AND IS_CELEBRATION_FOR_NON_MATCH_OR_LAST_ROUND_OF_MATCH()
					IF HAS_CELEBRATION_WINNER_FINISHED(TRUE)
						bSafeToContinue = TRUE
						PRINTLN("[HAS_CELEBRATION_SCREEN_FINISHED][RH] bSafeToContinue = TRUE 1")
					ELSE
						PRINTLN("[JS][url:bugstar:3998944] - HAS_CELEBRATION_SCREEN_FINISHED - 2")
					ENDIF
				ELSE
					bSafeToContinue = TRUE
					PRINTLN("[HAS_CELEBRATION_SCREEN_FINISHED][RH] bSafeToContinue = TRUE 2")
				ENDIF
			ELSE
				//In case the transition from the previous screen is tight: keep drawing to prevent pops.
				IF HAS_CELEBRATION_SCREEN_LOADED(sCelebrationData)
					DRAW_CELEBRATION_SCREEN_THIS_FRAME(sCelebrationData, FALSE)
				ENDIF
				bSafeToContinue = TRUE
				PRINTLN("[HAS_CELEBRATION_SCREEN_FINISHED][RH] bSafeToContinue = TRUE 3")
			ENDIF
			PRINTLN("[HAS_CELEBRATION_SCREEN_FINISHED][RH]2 bSafeToContinue ", BOOL_TO_INT(bSafeToContinue))
			//Need to wait for the player to play their gesture before starting the swoop cam (just ignore if we skipped the winner screen).
			IF bSafeToContinue	
				IF CONTENT_IS_USING_ARENA()
					SET_ARENA_LOBBY_BACKGROUND_TO_DRAW_AFTER_MISSION()
				ENDIF
				
				TOGGLE_PLAYER_DAMAGE_OVERLAY(TRUE)
				
				IF SHOULD_DO_POST_MISSION_SCENE_CELEBRATION()
					
					IF NOT IS_SCREEN_FADED_OUT()
						IF NOT IS_SCREEN_FADING_OUT()
							DO_SCREEN_FADE_OUT(1000)
						ENDIF
					ELSE
						RE_CACHE_LOCAL_PLAYER_PED_ID()
						ANIMPOSTFX_STOP("MP_Celeb_Win")
						ANIMPOSTFX_STOP("MP_Celeb_Lose")
						REMOVE_MP_HEIST_GEAR(PLAYER_PED_ID(), HEIST_GEAR_NIGHTVISION) // Remove night vision for celebration screen.
						IF IS_AUDIO_SCENE_ACTIVE("MP_LEADERBOARD_SCENE")
							STOP_AUDIO_SCENE("MP_LEADERBOARD_SCENE")
						ENDIF
						IF IS_AUDIO_SCENE_ACTIVE("MP_LEADERBOARD_PP_SCENE")
							STOP_AUDIO_SCENE("MP_LEADERBOARD_PP_SCENE")
						ENDIF
						CLEAR_ALL_BIG_MESSAGES()
						IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
							PRINTLN("[NETCELEBRATION] - [RCC MISSION] - CLEANUP_CELEBRATION_SCREEN call 2.")
							STOP_CELEBRATION_SCREEN_ANIM(sCelebrationData, "WINNER")
							STOP_CELEBRATION_SCREEN_ANIM(sCelebrationData, "SUMMARY")
							STOP_CELEBRATION_SCREEN_ANIM(sCelebrationData, "EARLYDEATH")
							CLEANUP_CELEBRATION_SCREEN(sCelebrationData, TRUE, "WINNER")
							CLEANUP_CELEBRATION_SCREEN(sCelebrationData, TRUE, "SUMMARY")
							CLEANUP_CELEBRATION_SCREEN(sCelebrationData, TRUE, "EARLYDEATH")
						ENDIF
						SET_CELEBRATION_SCREEN_AS_ACTIVE(FALSE)
						REINIT_NET_TIMER(tdresultstimer)
						IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_StoppedAllPostFxForSwitch)
							CLEAR_ALL_CELEBRATION_POST_FX()
							SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_StoppedAllPostFxForSwitch)
						ENDIF
						IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bCelebrationUnfrozeSkycam)
							SET_DRAW_BLACK_RECTANGLE_FOR_FADED_MOCAP(FALSE)
							SET_SKYFREEZE_CLEAR(TRUE)
							SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bCelebrationUnfrozeSkycam)
						ENDIF
						SET_BIT(iLocalBoolCheck3,LBOOL3_SHOWN_RESULTS)
						RETURN TRUE
					ENDIF
					
				ELSE
					PRINTLN("[JS][url:bugstar:3998944] - HAS_CELEBRATION_SCREEN_FINISHED - 3")
					
					IF SET_SKYSWOOP_UP(FALSE, TRUE, FALSE, DEFAULT, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE, FALSE, FALSE, TRUE) //This skyswoop needs to keep the screen frozen until the first jump.
						
						IF IS_SCREEN_FADED_OUT()
							PRINTLN("[LM][url:bugstar:4372431 - HAS_CELEBRATION_SCREEN_FINISHED - Emergancy Screen Fade In. Celebration Screen should start.")
							DO_SCREEN_FADE_IN_FOR_TRANSITION(250)
						ENDIF
					
						IF CONTENT_IS_USING_ARENA()
							IF IS_AUDIO_SCENE_ACTIVE("MP_CELEB_SCREEN_ARENA_SCENE")
								STOP_AUDIO_SCENE("MP_CELEB_SCREEN_ARENA_SCENE")
							ENDIF
						ELIF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciPLAY_POWERPLAY_INTRO_ANNOUNCER)						
							//Cleanup audio scenes: see B*1642903 for implementation notes ||| moved baed on nick gozems comments via: url:bugstar:4029840
							IF IS_AUDIO_SCENE_ACTIVE("MP_CELEB_SCREEN_SCENE")
								STOP_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
							ENDIF
						ELSE
							IF IS_AUDIO_SCENE_ACTIVE("MP_CELEB_SCREEN_PP_SCENE")
								STOP_AUDIO_SCENE("MP_CELEB_SCREEN_PP_SCENE")
							ENDIF
						ENDIF
						
						RE_CACHE_LOCAL_PLAYER_PED_ID()

						ANIMPOSTFX_STOP("MP_Celeb_Win")
						ANIMPOSTFX_STOP("MP_Celeb_Lose")
						
						REMOVE_MP_HEIST_GEAR(PLAYER_PED_ID(), HEIST_GEAR_NIGHTVISION) // Remove night vision for celebration screen.
						
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_POWERPLAY_PICKUP_SOUNDS)
							IF NOT IS_AUDIO_SCENE_ACTIVE("MP_LEADERBOARD_SCENE")
								START_AUDIO_SCENE("MP_LEADERBOARD_SCENE")
							ENDIF
						ELSE
							IF NOT IS_AUDIO_SCENE_ACTIVE("MP_LEADERBOARD_PP_SCENE")
								START_AUDIO_SCENE("MP_LEADERBOARD_PP_SCENE")
							ENDIF
						ENDIF
						
						IF SHOULD_DO_TRANSITION_MENU_AUDIO()
							//1665147 - Start the pause menu music as soon as the celebration screen ends.
							RUN_TRANSITION_MENU_AUDIO(TRUE)
						ENDIF
						
						CLEAR_ALL_BIG_MESSAGES()
						IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
							PRINTLN("HAS_CELEBRATION_SCREEN_FINISHED - [NETCELEBRATION] - [RCC MISSION] - CLEANUP_CELEBRATION_SCREEN call 2.")
							STOP_CELEBRATION_SCREEN_ANIM(sCelebrationData, "WINNER")
							STOP_CELEBRATION_SCREEN_ANIM(sCelebrationData, "SUMMARY")
							STOP_CELEBRATION_SCREEN_ANIM(sCelebrationData, "EARLYDEATH")
							CLEANUP_CELEBRATION_SCREEN(sCelebrationData, TRUE, "WINNER")
							CLEANUP_CELEBRATION_SCREEN(sCelebrationData, TRUE, "SUMMARY")
							CLEANUP_CELEBRATION_SCREEN(sCelebrationData, TRUE, "EARLYDEATH")
						ENDIF
						SET_CELEBRATION_SCREEN_AS_ACTIVE(FALSE)
						REINIT_NET_TIMER(tdresultstimer)
						SET_BIT(iLocalBoolCheck3,LBOOL3_SHOWN_RESULTS)
						
						RETURN TRUE
					ENDIF
					
				ENDIF
				
				RE_CACHE_LOCAL_PLAYER_PED_ID()
				
			ENDIF
			
			IF NOT SHOULD_DO_POST_MISSION_SCENE_CELEBRATION()
				// Stop post fx on first switch cut.
				IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_StoppedAllPostFxForSwitch)
					IF IS_PLAYER_SWITCH_IN_PROGRESS()
						IF (GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_JUMPCUT_ASCENT)
							IF GET_REQUESTINGNIGHTVISION()
							OR GET_USINGNIGHTVISION()
								ENABLE_NIGHTVISION(VISUALAID_OFF)
								DISABLE_NIGHT_VISION_CONTROLLER(TRUE)
							ENDIF
							CLEAR_ALL_CELEBRATION_POST_FX()
							PRINTLN("[RCC MISSION] - HAS_CELEBRATION_SCREEN_FINISHED - called ANIMPOSTFX_STOP_ALL, switch state is in SWITCH_STATE_JUMPCUT_ASCENT.")
							SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_StoppedAllPostFxForSwitch)
						ENDIF
					ENDIF
				ENDIF
				
				//Make sure the screen is unfrozen when the sky cam starts jumping up.
				IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bCelebrationUnfrozeSkycam)
					IF IS_SWITCH_TO_MULTI_FIRSTPART_FINISHED()
						SET_DRAW_BLACK_RECTANGLE_FOR_FADED_MOCAP(FALSE)
						SET_SKYFREEZE_CLEAR(TRUE)
						SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bCelebrationUnfrozeSkycam)
					ENDIF
				ENDIF
			ENDIF
			
		ELSE

			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_INTERNAL_CELEBRATION_SCREEN()
	
	BOOL bPreLoadEffectBeenPlayingLongEnough = TRUE
	BOOL bSortedCamera = TRUE
	
	// If we have started the celebration pre load effect, make sure it's had time to play properly.
	IF HAS_NET_TIMER_STARTED(sCelebrationData.timerCelebPreLoadPostFxBeenPlaying)		
		IF NOT HAS_NET_TIMER_EXPIRED(sCelebrationData.timerCelebPreLoadPostFxBeenPlaying, ciMissionEndEffectDelay)
			bPreLoadEffectBeenPlayingLongEnough = FALSE
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_AcceptedInvite)
		
		// Moved this here to make transtion between death and celeb screen more buttery smooth.
		IF IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration)
			IF NOT IS_PLAYER_SCTV(PLAYER_ID())
			AND NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(PLAYER_ID())
				IF IS_PED_DEAD_OR_DYING(PLAYER_PED_ID())
				OR IS_PED_INJURED(PLAYER_PED_ID())
				
					IF SHOULD_RESURRECT_PLAYER_AT_END()
						IF NOT IS_SCREEN_FADING_OUT()
							DO_SCREEN_FADE_OUT(150)
						ENDIF
						
						IF IS_SCREEN_FADED_OUT()
							NETWORK_RESURRECT_LOCAL_PLAYER(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_HEADING(PLAYER_PED_ID()), 0, FALSE, TRUE)
							CACHE_LOCAL_PLAYER_VARIABLES()
						ENDIF
						
						g_TransitionSessionNonResetVars.sGlobalCelebrationData.bBlockSpawnFadeInForMissionFail = TRUE
						SET_BIT(iLocalBoolCheck10, LBOOL10_RESPAWNING_DURING_FAIL)
						PRINTLN("[RCC MISSION][HAS_CELEBRATION_SCREEN_FINISHED] - Resurrecting Player.")
					ENDIF
					
				ELSE
					IF (IS_PLAYER_RESPAWNING(PLAYER_ID()) OR IS_LOCAL_PLAYER_ANY_SPECTATOR())
					AND NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_RESPAWNING_DURING_FAIL)
						SET_BIT(iLocalBoolCheck10, LBOOL10_RESPAWNING_DURING_FAIL)
						FORCE_RESPAWN(TRUE)
						FORCE_PlAYER_BACK_INTO_PLAYING_STATE(TRUE, TRUE)
						SET_MANUAL_RESPAWN_STATE(MRS_NULL)
						RESET_GAME_STATE_ON_DEATH()
						PRINTLN("[RCC MISSION][HAS_CELEBRATION_SCREEN_FINISHED] - Force Respawning Player.")
					ENDIF
					
					SET_PED_CONFIG_FLAG(localPlayerPed, PCF_GetOutUndriveableVehicle, FALSE)
					SET_PED_CONFIG_FLAG(localPlayerPed, PCF_GetOutBurningVehicle, FALSE)
				ENDIF
			ENDIF
		
			IF NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_DESTROY_BIRDSEYE_CAM_FOR_CELEB_SCREEN)	
				PRINTLN("[RCC MISSION] - [SAC] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - Calling CLEANUP_BIRDSEYE_CAM the once...")
				SET_BIT(iLocalBoolCheck30, LBOOL30_DESTROY_BIRDSEYE_CAM_FOR_CELEB_SCREEN)
				CLEANUP_BIRDSEYE_CAM()
			ENDIF
		ENDIF
		
		// Place the reverse camera behind faded out screen if respawning timing issue occurs when the mission ends.
		IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_PLACED_REVERSE_ANGLE_CAMERA)
			IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_RESPAWNING_DURING_FAIL)
			OR g_bCelebrationEarlyDeathIsActive
				PRINTLN("[RCC MISSION] - [SAC] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - LBOOL10_RESPAWNING_DURING_FAIL = TRUE.")
				IF IS_NET_PLAYER_OK(PLAYER_ID())
					
					//IF NET_WARP_TO_COORD() url:bugstar:5658884 - Missile Silo Series - Player sees under the world throughout the Loser shard upon dying about 6 seconds before the end of a round.
					
					PRINTLN("[RCC MISSION] - [SAC] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - IS_NET_PLAYER_OK = TRUE.")
					IF NOT IS_PLAYER_RESPAWNING(PLAYER_ID())
						PRINTLN("[RCC MISSION] - [SAC] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - IS_PLAYER_RESPAWNING = FALSE.")
						IF g_KillCamStruct.eCurrentStage = KILL_CAM_STAGE_OFF
							PRINTLN("[RCC MISSION] - [SAC] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - g_KillCamStruct.eCurrentStage = KILL_CAM_STAGE_OFF.")
							IF NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
								PRINTLN("[RCC MISSION] - [SAC] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - IS_ANY_BIG_MESSAGE_BEING_DISPLAYED = FALSE.")
//								IF IS_GAMEPLAY_CAM_RENDERING()
									PRINTLN("[RCC MISSION] - [SAC] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - IS_GAMEPLAY_CAM_RENDERING = TRUE.")
									IF PLACE_CAMERA_FOR_CELEBRATION_SCREEN(sCelebrationData, camEndScreen, bCreatedCustomCelebrationCam)
										PRINTLN("[RCC MISSION] - [SAC] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - PLACE_CAMERA_FOR_CELEBRATION_SCREEN = TRUE.")
										IF bCreatedCustomCelebrationCam
											PRINTLN("[RCC MISSION] - [SAC] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - bCreatedCustomCelebrationCam = TRUE.")
											IF DOES_CAM_EXIST(camEndScreen)
												PRINTLN("[RCC MISSION] - [SAC] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - DOES_CAM_EXIST(camEndScreen) = TRUE.")
												IF NOT IS_CAM_RENDERING(camEndScreen)
													PRINTLN("[RCC MISSION] - [SAC] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - IS_CAM_RENDERING(camEndScreen) = FALSE.")
													IF g_bCelebrationScreenIsActive
														PRINTLN("[RCC MISSION] - [SAC] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - Killing the early death screen to make way for the SUMMARY!")
														STOP_CELEBRATION_SCREEN_ANIM(sCelebrationData, "EARLYDEATH")
														CLEANUP_CELEBRATION_SCREEN(sCelebrationData, TRUE, "EARLYDEATH")
														INITIALISE_CELEBRATION_SCREEN_DATA(sCelebrationData)
													ENDIF
													SET_CAM_ACTIVE(camEndScreen, TRUE)													
													RENDER_SCRIPT_CAMS(TRUE, FALSE)
													PRINTLN("[RCC MISSION] - [SAC] - rendered cam camEndScreen.")
												ENDIF
											ENDIF
										ELSE
											PRINTLN("[RCC MISSION] - [SAC] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - bCreatedCustomCelebrationCam = FALSE, set bit LBOOL13_NOT_SAFE_FOR_ALTERNATE_CAM.")
											SET_BIT(iLocalBoolCheck13, LBOOL13_NOT_SAFE_FOR_ALTERNATE_CAM)
										ENDIF
										
										PRINTLN("[RCC MISSION] - [SAC] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - set bit LBOOL10_PLACED_REVERSE_ANGLE_CAMERA.")
										SET_BIT(iLocalBoolCheck10, LBOOL10_PLACED_REVERSE_ANGLE_CAMERA)
										
										IF HAS_NET_TIMER_STARTED(sCelebrationData.sCelebrationTransitionTimer)
											RESET_NET_TIMER(sCelebrationData.sCelebrationTransitionTimer)
											START_NET_TIMER(sCelebrationData.sCelebrationTransitionTimer)
											PRINTLN("[RCC MISSION] - [SAC] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - sCelebrationTransitionTimer had already started, reinitializing it.")
										ENDIF
									ENDIF
//								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
				
		FREEZE_SCREEN_FOR_PLACED_CAMERA()
		
		IF NOT IS_PLAYER_SCTV(PLAYER_ID())
		AND NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(PLAYER_ID())
			IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_RESPAWNING_DURING_FAIL)			
				IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_PLACED_REVERSE_ANGLE_CAMERA)
					PRINTLN("[RCC MISSION] - [SAC] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - LBOOL10_PLACED_REVERSE_ANGLE_CAMERA bit not set, setting bSortedCamera = FALSE.")
					bSortedCamera = FALSE
				ENDIF
				IF iFreezeCamForPlaceDcamStage < 3
					PRINTLN("[RCC MISSION] - [SAC] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - iFreezeCamForPlaceDcamStage = ", iFreezeCamForPlaceDcamStage, ", setting bSortedCamera = FALSE.")
					bSortedCamera = FALSE
				ENDIF
			ENDIF
		ENDIF
					
		// Run the main celebration logic.
		IF (bPreLoadEffectBeenPlayingLongEnough	AND bSortedCamera)
		#IF IS_DEBUG_BUILD
		OR g_SkipCelebAndLbd
		#ENDIF
			IF IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration)
				IF NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_OUTFIT_REMOVED_FOR_CELEB_SCREEN)
					IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TRANSFORMED_INTO_JUGGERNAUT)
					OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TRANSFORMED_INTO_BEAST)
						PRINTLN("[RCC MISSION] - [SAC] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - REMOVING TRANSFORMATION OUTFIT")
						SET_BIT(iLocalBoolCheck24, LBOOL24_OUTFIT_REMOVED_FOR_CELEB_SCREEN)
						APPLY_OUTFIT_TO_LOCAL_PLAYER(INT_TO_ENUM(MP_OUTFIT_ENUM, GET_LOCAL_DEFAULT_OUTFIT(MC_playerBD[iLocalPart].iTeam)))
					ENDIF
				ENDIF
				IF HAS_CELEBRATION_SCREEN_FINISHED()
					PRINTLN("[RCC MISSION] - [SAC] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - HAS_CELEBRATION_SCREEN_FINISHED = TRUE Setting ciCELEBRATION_BIT_SET_EndCelebrationFinished")
					SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_EndCelebrationFinished)
				ELSE
					PRINTLN("[RCC MISSION] - [SAC] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - HAS_CELEBRATION_SCREEN_FINISHED = FALSE")
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION] - [SAC] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - ciCELEBRATION_BIT_SET_TriggerEndCelebration = FALSE")
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION] - [SAC] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - bPreLoadEffectBeenPlayingLongEnough = ", bPreLoadEffectBeenPlayingLongEnough)
			PRINTLN("[RCC MISSION] - [SAC] - MAINTAIN_INTERNAL_CELEBRATION_SCREEN - bSortedCamera = ", bSortedCamera)
		ENDIF
		
	ENDIF
	
ENDPROC


FUNC BOOL SET_PLAYER_VARIATIONS_FOR_HEIST_CELEBRATION()

	INT i
	
	BOOL bAllStreamingRequestsComplete = TRUE
	
	SWITCH iCelebrationStreamingRequestStage
		
		CASE 0
			
			IF Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
			
				PRINTLN("[RCC MISSION] - [PMC] - [NETCELEBRATION] - [SAC] - SET_PLAYER_VARIATIONS_FOR_HEIST_CELEBRATION - Is_Player_Currently_On_MP_Heist_Planning = TRUE, RETURNING TRUE.")
				iCelebrationStreamingRequestStage = 4
				
			ELIF Is_Player_Currently_On_MP_Heist(LocalPlayer)
			OR CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
				
				IF IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggeredCelebrationStreamingRequests)
					
					SET_PLAYER_CLOTHES_FOR_RETURN_TO_FREEMODE()
					PRINTLN("[RCC MISSION] - [PMC] - [NETCELEBRATION] - [SAC] - SET_PLAYER_VARIATIONS_FOR_HEIST_CELEBRATION - called SET_PED_VARIATIONS.")
					
					iCelebrationStreamingRequestStage++
					PRINTLN("[RCC MISSION] - [PMC] - [NETCELEBRATION] - [SAC] - SET_PLAYER_VARIATIONS_FOR_HEIST_CELEBRATION - player set variations. Going to stage ", iCelebrationStreamingRequestStage)
				
				ENDIF
				
			ENDIF
			
		BREAK
		
		CASE 1
			
			IF PARTICIPANT_ID_TO_INT() = -1
				SCRIPT_ASSERT("[RCC MISSION] - [NETCELEBRATION] - [SAC] - PARTICIPANT_ID_TO_INT() = -1, even though misison controller is still running.")
			ELSE
				IF NOT IS_BIT_SET(MC_playerBD[PARTICIPANT_ID_TO_INT()].iClientBitset2, PBBOOL2_COMPLETED_CELEBRATION_STREAMING_REQUESTS)
					IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(LocalPlayerPed)
						SET_BIT(MC_playerBD[PARTICIPANT_ID_TO_INT()].iClientBitset2, PBBOOL2_COMPLETED_CELEBRATION_STREAMING_REQUESTS)
						PRINTLN("[RCC MISSION] - [PMC] - [NETCELEBRATION] - [SAC] - SET_PLAYER_VARIATIONS_FOR_HEIST_CELEBRATION - HAVE_ALL_SREAMING_REQUESTS_COMPLETED = TRUE, setting PBBOOL2_COMPLETED_CELEBRATION_STREAMING_REQUESTS.")
						iCelebrationStreamingRequestStage++
						PRINTLN("[RCC MISSION] - [PMC] - [NETCELEBRATION] - [SAC] - SET_PLAYER_VARIATIONS_FOR_HEIST_CELEBRATION - Going to stage ", iCelebrationStreamingRequestStage)
					ENDIF
				ENDIf
			ENDIF
			
		BREAK
		
		CASE 2
			
			REPEAT NUM_NETWORK_PLAYERS i
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
					IF NOT IS_PLAYER_SCTV(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i)))
						IF NOT IS_BIT_SET(MC_playerBD[i].iClientBitset2, PBBOOL2_COMPLETED_CELEBRATION_STREAMING_REQUESTS)
							bAllStreamingRequestsComplete = FALSE
							PRINTLN("[RCC MISSION] - [PMC] - [NETCELEBRATION] - [SAC] - SET_PLAYER_VARIATIONS_FOR_HEIST_CELEBRATION - player ", i, " not finished streaming requests.")
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF bAllStreamingRequestsComplete
				iCelebrationStreamingRequestStage++
				PRINTLN("[RCC MISSION] - [PMC] - [NETCELEBRATION] - [SAC] - SET_PLAYER_VARIATIONS_FOR_HEIST_CELEBRATION - Going to stage ", iCelebrationStreamingRequestStage)
			ENDIf
			
		BREAK
		
		CASE 3
		
			IF NOT HAS_NET_TIMER_STARTED(timerCelebReceivedAllPlayerVariationUpdates)
				START_NET_TIMER(timerCelebReceivedAllPlayerVariationUpdates)
				PRINTLN("[RCC MISSION] - [PMC] - [NETCELEBRATION] - [SAC] - SET_PLAYER_VARIATIONS_FOR_HEIST_CELEBRATION - player bAllStreamingRequestsComplete = TRUE, started timer timerCelebReceivedAllPlayerVariationUpdates.")
			ELSE
				IF HAS_NET_TIMER_EXPIRED(timerCelebReceivedAllPlayerVariationUpdates, 3000)
					iCelebrationStreamingRequestStage++
					PRINTLN("[RCC MISSION] - [PMC] - [NETCELEBRATION] - [SAC] - SET_PLAYER_VARIATIONS_FOR_HEIST_CELEBRATION - player bAllStreamingRequestsComplete = TRUE and timerCelebReceivedAllPlayerVariationUpdates >= 3000. Going to stage ", iCelebrationStreamingRequestStage)
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 4
			
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bStreamingRequestsComplete = TRUE
			RETURN TRUE
				
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL HAVE_ALL_PLAYERS_FINISHED_MOCAP()

	INT iPart
		
	FOR iPart = 0 TO (MAX_NUM_MC_PLAYERS - 1)
		PARTICIPANT_INDEX temppart = INT_TO_PARTICIPANTINDEX(iPart)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(temppart)
			PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(temppart)
			IF IS_NET_PLAYER_OK( tempPlayer, FALSE )
			AND NOT IS_BIT_SET( MC_playerBD[ iPart ].iClientBitset, PBBOOL_PLAYER_OUT_OF_LIVES )
			AND NOT IS_BIT_SET( MC_playerBD[ iPart ].iClientBitset, PBBOOL_PLAYER_FAIL )
				IF IS_BIT_SET( MC_playerBD[ iPart ].iClientBitset2, PBBOOL2_PLAYING_MOCAP_CUTSCENE )
					PRINTLN("[RCC MISSION] - HAVE_ALL_PLAYERS_FINISHED_MOCAP FALSE for ipart: ", iPart)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN TRUE

ENDFUNC

FUNC BOOL HAVE_ALL_PARTICIPANTS_COMPLETED_POPULATION_OF_CELEBRATION_GLOBALS()
	
	INT i
	
	REPEAT NUM_NETWORK_PLAYERS i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			IF NOT IS_BIT_SET(MC_playerBD[i].iClientBitset2, PBBOOL2_COMPLETED_POPULATION_OF_CELEB_GLOBALS)
				PRINTLN("[RCC MISSION] - [PMC] - [NETCELEBRATION] - [SAC] - HAVE_ALL_PARTICIPANTS_COMPLETED_POPULATION_OF_CELEBRATION_GLOBALS - participant ", i, " not finished populating globals.")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL HAVE_ALL_PARTICIPANTS_LAUNCHED_CELEBRATION_SCRIPT()
	
	INT i
	
	REPEAT NUM_NETWORK_PLAYERS i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			IF NOT IS_BIT_SET(MC_playerBD[i].iClientBitset2, PBBOOL2_ALL_PARTICIPANTS_LAUNCHED_CELEB_SCRIPT)
				PRINTLN("[RCC MISSION] - [PMC] - [NETCELEBRATION] - [SAC] - HAVE_ALL_PARTICIPANTS_LAUNCHED_CELEBRATION_SCRIPT - participant ", i, " not launched celebration script yet.")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL HAS_SCTV_PLAYER_POPULATED_CELEBRATION_GLOBALS()
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
	OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator
	
		IF IS_BIT_SET(iLocalBoolCheck9, LBOOL9_COPIED_CELEBRATION_DATA_EVENT_1)
		AND IS_BIT_SET(iLocalBoolCheck9, LBOOL9_COPIED_CELEBRATION_DATA_EVENT_2)
			PRINTLN("[SAC] - HAS_SCTV_PLAYER_POPULATED_CELEBRATION_GLOBALS - both populate bits are set")
			RETURN TRUE
		ENDIF
		
		IF NOT HAS_NET_TIMER_STARTED(sCelebrationData.SpecFillGlobalsTimeout)
			START_NET_TIMER(sCelebrationData.SpecFillGlobalsTimeout)
		ELSE
			IF HAS_NET_TIMER_EXPIRED(sCelebrationData.SpecFillGlobalsTimeout, 60000)
				PRINTLN("[SAC] - HAS_SCTV_PLAYER_POPULATED_CELEBRATION_GLOBALS - hitting spectator fill celebration globals timeout.")
				RETURN TRUE
			ENDIF
		ENDIF
	
	ELSE
		
		PRINTLN("[SAC] - HAS_SCTV_PLAYER_POPULATED_CELEBRATION_GLOBALS - I am not SCTV")
		RETURN TRUE
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC MAINTAIN_STAND_ALONE_CELEBRATION_SCREEN()
	
	BOOL bAllFinishedMocap
	BOOL bAllFilledCelebrationGlobals
	BOOL bPreLoadEffectBeenPlayingLongEnough = TRUE
	BOOL bAllParticipantsLaunchedCelebScript
	
	// If we have started the celebration pre load effect, make sure it's had time to play properly.
	IF HAS_NET_TIMER_STARTED(sCelebrationData.timerCelebPreLoadPostFxBeenPlaying)
		IF NOT HAS_NET_TIMER_EXPIRED(sCelebrationData.timerCelebPreLoadPostFxBeenPlaying, ciMissionEndEffectDelay)
			bPreLoadEffectBeenPlayingLongEnough = FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerHeistwinnerCutscene)
		
		SET_CELEB_BLOCK_RESURRECT_RENDERPHASE_UNPAUSE(TRUE)
		
		SET_PLAYER_VARIATIONS_FOR_HEIST_CELEBRATION()
			
		IF HAS_SCTV_PLAYER_POPULATED_CELEBRATION_GLOBALS()
			
			IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggeredHeistwinnerCutscene)
				
				IF bPreLoadEffectBeenPlayingLongEnough
					
					IF NOT IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_AcceptedInvite)
						
						IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
						OR Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
						OR GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
						OR CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
							// Calculate results.
							GET_HEIST_COMPLETION_RANKINGS()
							POPULATE_RESULTS()
							COPY_CELEBRATION_DATA_TO_GLOBALS()
								
							REQUEST_SCRIPT("Celebrations")
							IF HAS_SCRIPT_LOADED("Celebrations")
								
								PRINTLN("[RCC MISSION] - [NETCELEBRATION] - [SAC] - MAINTAIN_STAND_ALONE_CELEBRATION_SCREEN - HAS_SCRIPT_LOADED('Celebrations') = TRUE.") 
								
								IF SHOULD_LAUNCH_HEIST_TUTORIAL_MID_STRAND_CUT_SCENE() 
									SET_POST_MISSION_PLACE_IN_APARTMENT_FLAG(TRUE)
									PRINTLN("[RCC MISSION] - [PMC] - [SAC] - MAINTAIN_STAND_ALONE_CELEBRATION_SCREEN - SHOULD_LAUNCH_HEIST_TUTORIAL_MID_STRAND_CUT_SCENE() = TRUE, set bPlaceInApartment = TRUE.")
								ENDIF
								
								IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
								OR CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
									PRINTLN("[RCC MISSION] - [NETCELEBRATION] - [SAC] - MAINTAIN_STAND_ALONE_CELEBRATION_SCREEN - Is_Player_Currently_On_MP_Heist = TRUE, calling SET_DOING_HEIST_END_WINNER_SCENE(TRUE).") 
									SET_DOING_HEIST_END_WINNER_SCENE(TRUE)
								ELSE
									g_TransitionSessionNonResetVars.sGlobalCelebrationData.bCloningComplete = TRUE
									PRINTLN("[RCC MISSION] - [NETCELEBRATION] - [SAC] - MAINTAIN_STAND_ALONE_CELEBRATION_SCREEN - Is_Player_Currently_On_MP_Heist = FALSE, forcing bCloningComplete = TRUE so script can commence.")
									#IF IS_DEBUG_BUILD
									IF Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
										PRINTLN("[RCC MISSION] - [NETCELEBRATION] - [SAC] - MAINTAIN_STAND_ALONE_CELEBRATION_SCREEN - player is on a heist prep.")
									ELSE
										PRINTLN("[RCC MISSION] - [NETCELEBRATION] - [SAC] - MAINTAIN_STAND_ALONE_CELEBRATION_SCREEN - player is not on a heist mission - no prep or finale.")
									ENDIF
									#ENDIF
								ENDIF
								
								SET_DOING_HEIST_CELEBRATION_SCREEN(TRUE)
								
								// Set where in the apartment I'm going to.
								IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlaceInApartment
									PRINTLN("[RCC MISSION] - [PMC] - [SAC] - MAINTAIN_STAND_ALONE_CELEBRATION_SCREEN - bPlaceInApartment = TRUE, calling SET_APARTMENT_LOCATION(). 2") 
									SET_APARTMENT_LOCATION()
								ELSE
									PRINTLN("[RCC MISSION] - [PMC] - [SAC] - MAINTAIN_STAND_ALONE_CELEBRATION_SCREEN - bPlaceInApartment = FALSE, not calling SET_APARTMENT_LOCATION(). 2") 
								ENDIF
								
								// Transition and post mission.
								SET_TRANSITION_SESSIONS_SKIP_LB_AND_NJVS()
								g_TransitionSessionNonResetVars.sPostMissionCleanupData.bDoingCelebrationTransition = TRUE
								
								PRINTLN("[RCC MISSION] - [PMC] - [SAC] - MAINTAIN_STAND_ALONE_CELEBRATION_SCREEN - Grabbing ", NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS()), " as the leader.") 
								CELEBRATION_SCRIPT_DATA sLaunchData
								
								IF GB_GET_LOCAL_PLAYER_GANG_BOSS() != INVALID_PLAYER_INDEX()
									sLaunchData.leaderHandle = GET_GAMER_HANDLE_PLAYER(GB_GET_LOCAL_PLAYER_GANG_BOSS())
								ENDIF
								
								// Launch standalone celebrations script.
								START_NEW_SCRIPT_WITH_ARGS("Celebrations", sLaunchData.leaderHandle, SIZE_OF(sLaunchData), MULTIPLAYER_MISSION_STACK_SIZE)
								SET_SCRIPT_AS_NO_LONGER_NEEDED("Celebrations")
								
								PRINTLN("[RCC MISSION] - [NETCELEBRATION] - [SAC] - MAINTAIN_STAND_ALONE_CELEBRATION_SCREEN - PBBOOL2_ALL_PARTICIPANTS_LAUNCHED_CELEB_SCRIPT bit now set.") 
								SET_BIT(MC_playerBD[PARTICIPANT_ID_TO_INT()].iClientBitset2, PBBOOL2_ALL_PARTICIPANTS_LAUNCHED_CELEB_SCRIPT)
								
								SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggeredHeistwinnerCutscene)
								
							ELSE
								
								PRINTLN("[RCC MISSION] - [NETCELEBRATION] - [SAC] - MAINTAIN_STAND_ALONE_CELEBRATION_SCREEN - HAS_SCRIPT_LOADED('Celebrations') = FALSE.") 
								
							ENDIF
							
						ENDIF
						
					ELSE
						
						
						
					ENDIF
					
				ENDIF
				
			ELSE
			
				bAllFinishedMocap = HAVE_ALL_PLAYERS_FINISHED_MOCAP()
				bAllFilledCelebrationGlobals = HAVE_ALL_PARTICIPANTS_COMPLETED_POPULATION_OF_CELEBRATION_GLOBALS()
				bAllParticipantsLaunchedCelebScript = HAVE_ALL_PARTICIPANTS_LAUNCHED_CELEBRATION_SCRIPT()
				
				IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bCloningComplete
				AND bAllFinishedMocap
				AND bAllFilledCelebrationGlobals
				AND bAllParticipantsLaunchedCelebScript
					
					// Set flags mission controller script needs to fall through and complete script.
					SET_BIT(iLocalBoolCheck2,LBOOL2_LB_CAM_READY)
					SET_BIT(iLocalBoolCheck3,LBOOL3_SHOWN_RESULTS)
				
					PRINTLN("[RCC MISSION] - [NETCELEBRATION] - [SAC] - MAINTAIN_STAND_ALONE_CELEBRATION_SCREEN - set LBOOL2_LB_CAM_READY.") 
					PRINTLN("[RCC MISSION] - [NETCELEBRATION] - [SAC] - MAINTAIN_STAND_ALONE_CELEBRATION_SCREEN - set LBOOL3_SHOWN_RESULTS.") 
					
				ELSE
					
					#IF IS_DEBUG_BUILD
					IF NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bCloningComplete
						PRINTLN("[RCC MISSION] - [NETCELEBRATION] - MAINTAIN_STAND_ALONE_CELEBRATION_SCREEN - waiting on bCloningComplete.") 
					ENDIF
					IF NOT bAllFinishedMocap
						PRINTLN("[RCC MISSION] - [NETCELEBRATION] - MAINTAIN_STAND_ALONE_CELEBRATION_SCREEN - waiting on bAllFinishedMocap.") 
					ENDIF
					IF NOT bAllFilledCelebrationGlobals
						PRINTLN("[RCC MISSION] - [NETCELEBRATION] - MAINTAIN_STAND_ALONE_CELEBRATION_SCREEN - waiting on bAllFilledCelebrationGlobals.") 
					ENDIF
					IF NOT bAllParticipantsLaunchedCelebScript
						PRINTLN("[RCC MISSION] - [NETCELEBRATION] - MAINTAIN_STAND_ALONE_CELEBRATION_SCREEN - waiting on bAllParticipantsLaunchedCelebScript.") 
					ENDIF
					#ENDIF
				
				ENDIF
				
			ENDIF
			
		ENDIF
		
	ENDIF
	
ENDPROC


FUNC BOOL SHOULD_MISSION_CONTROLLER_CELEBRATION_PRE_LOAD_FX_BE_TRIGGERED()
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[RCC MISSION] - SHOULD_MISSION_CONTROLLER_CELEBRATION_PRE_LOAD_FX_BE_TRIGGERED - been called:")
	DEBUG_PRINTCALLSTACK()
	#ENDIF

	IF IS_THIS_IS_A_STRAND_MISSION()
		PRINTLN("[RCC MISSION] - SHOULD_MISSION_CONTROLLER_CELEBRATION_PRE_LOAD_FX_BE_TRIGGERED - IS_THIS_IS_A_STRAND_MISSION = TRUE.")
		IF NOT IS_THIS_THE_LAST_IN_A_SERIES_OF_STRAND_MISSIONS()
			PRINTLN("[RCC MISSION] - SHOULD_MISSION_CONTROLLER_CELEBRATION_PRE_LOAD_FX_BE_TRIGGERED - IS_THIS_THE_LAST_IN_A_SERIES_OF_STRAND_MISSIONS = FALSE.")
			IF IS_CUTSCENE_ACTIVE()
			AND MC_serverBD_4.iCurrentHighestPriority[ MC_playerBD[iPartToUse].iTeam ] >= MC_serverBD.iMaxObjectives[ MC_playerBD[iPartToUse].iTeam ]
				PRINTLN("[RCC MISSION] - SHOULD_MISSION_CONTROLLER_CELEBRATION_PRE_LOAD_FX_BE_TRIGGERED - IS_CUTSCENE_ACTIVE = TRUE.")
				PRINTLN("[RCC MISSION] - SHOULD_MISSION_CONTROLLER_CELEBRATION_PRE_LOAD_FX_BE_TRIGGERED - returning FALSE")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF MC_serverBD.iNextMission < FMMC_MAX_STRAND_MISSIONS
	AND MC_serverBD.iNextMission >= 0
	AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl23NextContentID[MC_serverBD.iNextMission])
		//Strand mission is going to be initialised soon
		IF g_FMMC_STRUCT.eBranchingTransitionType[MC_serverBD.iNextMission] != FMMC_BRANCHING_MISSION_TRANSITION_TYPE_MOCAP
			PRINTLN("[RCC MISSION] - SHOULD_MISSION_CONTROLLER_CELEBRATION_PRE_LOAD_FX_BE_TRIGGERED - returning FALSE (Airlock or Fade Out strand transition")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF MC_ServerBD.iEndCutscene > 0
	AND IS_THIS_IS_A_STRAND_MISSION()	
		PRINTLN("[RCC MISSION] - SHOULD_MISSION_CONTROLLER_CELEBRATION_PRE_LOAD_FX_BE_TRIGGERED - MC_ServerBD.iEndCutscene is set to: ", MC_ServerBD.iEndCutscene)
		RETURN FALSE
	ENDIF
				
	IF IS_CUTSCENE_PLAYING()
		PRINTLN("[RCC MISSION] - SHOULD_MISSION_CONTROLLER_CELEBRATION_PRE_LOAD_FX_BE_TRIGGERED - IS_CUTSCENE_PLAYING() = TRUE - returning FALSE")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerHeistwinnerCutscene)
		PRINTLN("[RCC MISSION] - SHOULD_MISSION_CONTROLLER_CELEBRATION_PRE_LOAD_FX_BE_TRIGGERED - ciCELEBRATION_BIT_SET_TriggerHeistwinnerCutscene is set - returning FALSE")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration)
		PRINTLN("[RCC MISSION] - SHOULD_MISSION_CONTROLLER_CELEBRATION_PRE_LOAD_FX_BE_TRIGGERED - ciCELEBRATION_BIT_SET_TriggerEndCelebration is set - returning FALSE")
		RETURN FALSE
	ENDIF
	
	IF g_bCelebrationScreenIsActive
		PRINTLN("[RCC MISSION] - SHOULD_MISSION_CONTROLLER_CELEBRATION_PRE_LOAD_FX_BE_TRIGGERED - g_bCelebrationScreenIsActive = TRUE - returning FALSE")
		RETURN FALSE
	ENDIF
		
	//no longer needed, we need to keep nightvision on in dawn raid
//	IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
//		IF GET_REQUESTINGNIGHTVISION()
//		OR GET_USINGNIGHTVISION()
//			PRINTLN("[RCC MISSION] - SHOULD_MISSION_CONTROLLER_CELEBRATION_PRE_LOAD_FX_BE_TRIGGERED - Nightvision is still on - returning FALSE")
//			RETURN FALSE
//		ENDIF
//	ENDIF
	
	PRINTLN("[RCC MISSION] - SHOULD_MISSION_CONTROLLER_CELEBRATION_PRE_LOAD_FX_BE_TRIGGERED - returning TRUE")
	RETURN TRUE
	
ENDFUNC

PROC MAINTAIN_CELEB_PRE_LOAD_FX(BOOL &bShownPreloadFX, SCRIPT_TIMER timerCelebPreLoadPostFxBeenPlaying)
	
	IF g_bEndOfMissionCleanUpHUD
		IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_PLAYED_CELEB_PRE_LOAD_FOR_HUD_CLEAR)
			IF SHOULD_MISSION_CONTROLLER_CELEBRATION_PRE_LOAD_FX_BE_TRIGGERED()
				IF IS_NET_PLAYER_OK(PLAYER_ID())
					PLAY_MP_CELEB_PRELOAD_POST_FX(bShownPreloadFX,timerCelebPreLoadPostFxBeenPlaying)
				ELSE
					PRINTLN("[NETCELEBRATION] - MAINTAIN_CELEB_PRE_LOAD_FX - player not net ok, not doing preload")
				ENDIF
				SET_BIT(iLocalBoolCheck10, LBOOL10_PLAYED_CELEB_PRE_LOAD_FOR_HUD_CLEAR)
				PRINTLN("[RCC MISSION] - [SAC] - played pre load fx, setting bit LBOOL10_PLAYED_CELEB_PRE_LOAD_FOR_HUD_CLEAR.")
			ELSE
				SET_BIT(iLocalBoolCheck10, LBOOL10_PLAYED_CELEB_PRE_LOAD_FOR_HUD_CLEAR)
				PRINTLN("[RCC MISSION] - [SAC] - SHOULD_MISSION_CONTROLLER_CELEBRATION_PRE_LOAD_FX_BE_TRIGGERED = FALSE, setting bit LBOOL10_PLAYED_CELEB_PRE_LOAD_FOR_HUD_CLEAR but haven't actually played fx.")
			ENDIF
		ENDIF
	ENDIF

ENDPROC



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: END STAGE AND LEADERBOARDS - ending logic relying on Cutscene_2 !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: EARLY END, GOING TO SPECTATE !
//
//************************************************************************************************************************************************************

PROC CLEANUP_EARLY_END_AND_SPECTATE()
	PRINTLN("[NETCELEBRATION] Out of lives celebration screen has finished.")
	IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
	AND NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
		NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_SET_INVISIBLE | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
	ENDIF
	
	IF GET_PED_PARACHUTE_STATE(LocalPlayerPed) <> PPS_INVALID
		CLEAR_PED_TASKS(LocalPlayerPed)
	ENDIF
			
	DETACH_ANY_PORTABLE_PICKUPS()
	
	INT i
	
	FOR i = 0 TO (MAX_NUM_MC_PLAYERS - 1)
		PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(i)
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			PLAYER_INDEX playerIndex = NETWORK_GET_PLAYER_INDEX(tempPart)
			
			NETWORK_OVERRIDE_SEND_RESTRICTIONS(playerIndex, FALSE)
			PRINTLN("[VoiceChat] CLEANUP_EARLY_END_AND_SPECTATE - Setting All Players NETWORK_OVERRIDE_SEND_RESTRICTIONS to FALSE... Name = ", GET_PLAYER_NAME(playerIndex))
			NETWORK_OVERRIDE_RECEIVE_RESTRICTIONS(playerIndex, FALSE)
			PRINTLN("[VoiceChat] CLEANUP_EARLY_END_AND_SPECTATE - Setting All Players NETWORK_OVERRIDE_RECEIVE_RESTRICTIONS to FALSE... Name = ", GET_PLAYER_NAME(playerIndex))
		ENDIF
	ENDFOR
	
	//Don't clean up the screen just yet, this will be done during the spectator transition.
	PRINTLN("[NETCELEBRATION] - [RCC MISSION] - CLEANUP_CELEBRATION_SCREEN call 4.")

	g_bEndOfMissionCleanUpHUD = FALSE
	SET_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_EARLY_END_SPECTATOR)
	HIDE_SPECTATOR_BUTTONS(FALSE)
	HIDE_SPECTATOR_HUD(FALSE)
	
	SET_MP_DECORATOR_BIT(LocalPlayer, MP_DECORATOR_BS_INVALID_TO_SPECTATE)
	REPLAY_PREVENT_RECORDING_THIS_FRAME()
	SET_GAME_STATE_ON_DEATH(AFTERLIFE_SET_SPECTATORCAM)
	SET_BIG_MESSAGE_ENABLED_WHEN_SPECTATING(TRUE)
	SET_BIT(iLocalBoolCheck3,LBOOL3_WAIT_END_SPECTATE)
	CLEAR_BIT(iLocalBoolCheck3,LBOOL3_MUSIC_PLAYING)
	
ENDPROC

FUNC BOOL SHOULD_MOVE_ON_TO_SPECTATE_AFTER_DEATH()
	
	IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdwastedtimer) > 3000
		
		PRINTLN("[RCC MISSION] SHOULD_MOVE_ON_TO_SPECTATE_AFTER_DEATH - Move on after 3 seconds of wasted timer")
		RETURN TRUE
		
		/*IF Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)
		OR Is_Player_Currently_On_MP_Versus_Mission(LocalPlayer)
		OR Is_Player_Currently_On_MP_Heist(LocalPlayer)
		OR Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
			IF IS_SCREEN_FADED_OUT()
				PRINTLN("[RCC MISSION] SHOULD_MOVE_ON_TO_SPECTATE_AFTER_DEATH - This IS an LTS/VS/Heist, move on after 3 seconds and faded out!")
				RETURN TRUE
			ELIF (NOT IS_SCREEN_FADING_OUT())
				DO_SCREEN_FADE_OUT(500)
				PRINTLN("[RCC MISSION] SHOULD_MOVE_ON_TO_SPECTATE_AFTER_DEATH - This IS an LTS/VS/Heist, after 3 seconds so start fading out")
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION] SHOULD_MOVE_ON_TO_SPECTATE_AFTER_DEATH - This isn't an LTS/VS/Heist, move on after 3 seconds")
			RETURN TRUE
		ENDIF*/
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC MANAGE_EARLY_END_AND_SPECTATE()
	
	IF IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()	
		EXIT
	ENDIF

	STRING sNull = NULL

	IF NOT HAS_NET_TIMER_STARTED(tdovertimer)

		IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_PLAYER_OUT_OF_LIVES)
		OR IS_BIT_SET(iLocalBoolCheck14, LBOOL14_OUT_OF_BOUNDS_DEATHFAIL) // This is set when the player is failed for being out of lives, but we also kill them!
			
			IF NOT HAS_NET_TIMER_STARTED(tdwastedtimer)
				IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
					INITIALISE_CELEBRATION_SCREEN_DATA(sCelebrationData)
					
					CELEBRATION_SCREEN_TYPE eCelebType
					IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
					OR Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
					
						eCelebType = CELEBRATION_HEIST
						PRINTLN("[RCC MISSION][MANAGE_EARLY_END_AND_SPECTATE] - [NETCELEBRATION] - USING HEIST CELEBRATION SCREEN 5")	
						

					ELIF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
					
						eCelebType = CELEBRATION_GANGOPS
						PRINTLN("[RCC MISSION][MANAGE_EARLY_END_AND_SPECTATE] - [NETCELEBRATION] - USING GANGOPS CELEBRATION SCREEN 5")
					
					
					ELSE
				
						eCelebType = CELEBRATION_STANDARD
						PRINTLN("[RCC MISSION][MANAGE_EARLY_END_AND_SPECTATE] - [NETCELEBRATION] - USING STANDARD CELEBRATION SCREEN 5")
					
					ENDIF
					
					REQUEST_CELEBRATION_SCREEN(sCelebrationData, eCelebType)

				ENDIF
				
				IF IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData)
					SET_MANUAL_RESPAWN_STATE(MRS_EXIT_SPECTATOR_AFTERLIFE)
					DEACTIVATE_SPECTATOR_CAM(g_BossSpecData, DSCF_BAIL_FOR_TRANSITION)
					RESET_GLOBAL_SPECTATOR_STRUCT()
				ENDIF
				
				PRINTLN("[RCC MISSION][MANAGE_EARLY_END_AND_SPECTATE] [NETCELEBRATION] Player out of lives, starting celebration screen")
				REINIT_NET_TIMER(tdwastedtimer)
				
				FIND_PREFERRED_SPECTATOR_TARGET()
				
				SET_CELEB_BLOCK_RESURRECT_RENDERPHASE_UNPAUSE(TRUE)
				
			ELSE
				IF SHOULD_MOVE_ON_TO_SPECTATE_AFTER_DEATH()
					IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
						g_bCelebrationEarlyDeathIsActive = TRUE
						
						IF HAS_CELEBRATION_SUMMARY_FINISHED(g_bCelebrationEarlyDeathIsActive)
							PRINTLN("[RCC MISSION][MANAGE_EARLY_END_AND_SPECTATE] celebration summary finished, cleaning up" )
							
							IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_FAIL)
							AND IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES)
								PRINTLN("[RCC MISSION][MANAGE_EARLY_END_AND_SPECTATE] starting tdovertimer" )
								REINIT_NET_TIMER(tdovertimer)
							ENDIF
							
							CLEANUP_EARLY_END_AND_SPECTATE()
						ELSE
							PRINTLN("[RCC MISSION][MANAGE_EARLY_END_AND_SPECTATE] HAS_CELEBRATION_SUMMARY_FINISHED FALSE" )
						ENDIF
					ELSE
						PRINTLN("[RCC MISSION][MANAGE_EARLY_END_AND_SPECTATE] strand mission cleanup" )
						CLEANUP_EARLY_END_AND_SPECTATE()
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION][MANAGE_EARLY_END_AND_SPECTATE] waiting for tdwastedtimer - fading out = ",IS_SCREEN_FADING_OUT(),", fade out = ",IS_SCREEN_FADED_OUT(),"; tdwastedtimer = ",GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdwastedtimer))
				ENDIF
			ENDIF
		ELSE
			CLEANUP_KILL_STRIP()
			CLEAR_KILL_STRIP_DEATH_EFFECTS()
			CLEAR_ALL_BIG_MESSAGES()
			
			VEHICULAR_VENDETTA_SET_LOCAL_PLAYER_AS_GHOST(FALSE)
			BROADCAST_FMMC_SPAWN_PROTECTION_ALPHA(FALSE, iLocalPart, GET_FRAME_COUNT())			
			
			IF HAS_LOCAL_PLAYER_FAILED()
				IF Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)
					sNull = "LTS_OVER"
				ENDIF
				// CMcM #2122025 - Removed shard for Out of Bounds fail.
				IF NOT (Is_Player_Currently_On_MP_Heist(LocalPlayer) OR Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer))
					IF MC_playerBD[iPartToUse].iReasonForPlayerFail != PLAYER_FAIL_REASON_BOUNDS
					AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH)
						IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUGBY(g_FMMC_STRUCT.iRootContentIdHash)
						OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(g_FMMC_STRUCT.iAdversaryModeType)
						OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_DCTL(g_FMMC_STRUCT.iAdversaryModeType)
						OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciCONDEMNED_MODE_ENABLED)
							PRINTLN("[KH][1][MANAGE_EARLY_END_AND_SPECTATE] - BLOCKING MISSION OVER IN RUGBY/DAWN RAID/DEADLINE MODE")
						ELSE
							SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_MISSION_OVER,  -1, GET_REASON_FOR_PLAYER_FAIL(MC_playerBD[iPartToUse].iReasonForPlayerFail),sNull,HUD_COLOUR_WHITE)
						ENDIF
					ENDIF
				ENDIF
				PRINTLN("[RCC MISSION][MANAGE_EARLY_END_AND_SPECTATE] game end mission over - player fail") NET_NL()
			ELSE
				IF HAS_TEAM_FAILED(MC_playerBD[iPartToUse].iteam)
					PRINTLN("[RCC MISSION][MANAGE_EARLY_END_AND_SPECTATE] game end mission over - team failed") NET_NL()
					
					IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()	//B*2658001 - Stops players getting fail shards after jipping in
						
						// CMcM #2122025 - Removed shard for Out of Bounds fail.
						IF NOT (Is_Player_Currently_On_MP_Heist(LocalPlayer) OR Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer))
							
							POPULATE_FAIL_RESULTS()
							
							// g_sJobHeistInfo.m_failureReason is the fail reason that POPULATE_FAIL_RESULTS comes up with, so check that here:
							IF g_sJobHeistInfo.m_failureReason != ENUM_TO_INT(mFail_OUT_OF_BOUNDS)
								
								IF NOT IS_STRING_NULL_OR_EMPTY(sCelebrationStats.strFailLiteral2)
									CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION][MANAGE_EARLY_END_AND_SPECTATE] Early end fail results - Trying to create a big message with two literal strings!")
									PRINTLN("[RCC MISSION][MANAGE_EARLY_END_AND_SPECTATE] Early end fail results - Trying to create a big message with two literal strings! strFailLiteral2 = ",sCelebrationStats.strFailLiteral2)
								ENDIF
								
								IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH)
									IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUGBY(g_FMMC_STRUCT.iRootContentIdHash)
									OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(g_FMMC_STRUCT.iAdversaryModeType)
									OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_DCTL(g_FMMC_STRUCT.iAdversaryModeType)
									OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciCONDEMNED_MODE_ENABLED)
										PRINTLN("[KH][2][MANAGE_EARLY_END_AND_SPECTATE] - BLOCKING MISSION OVER IN RUGBY/DAWN RAID/DEADLINE MODE")
									ELSE
										SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_MISSION_OVER, sObjEndText, sCelebrationStats.strFailLiteral, HUD_COLOUR_WHITE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION][MANAGE_EARLY_END_AND_SPECTATE] game end mission over - results pending") 
					
					IF NOT (Is_Player_Currently_On_MP_Heist(LocalPlayer) OR Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer))
						IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_playerBD[iLocalPart].tdMissionTime) > 3500
						OR GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[MC_playerBD[iPartToUse].iteam]) > 3500
							IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH)
							AND NOT (IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUGBY(g_FMMC_STRUCT.iRootContentIdHash)
							OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(g_FMMC_STRUCT.iAdversaryModeType)
							OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_DCTL(g_FMMC_STRUCT.iAdversaryModeType))
								SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_MISSION_OVER, -1, "MC_END_PEND",sNull,HUD_COLOUR_WHITE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData)
				SET_MANUAL_RESPAWN_STATE(MRS_EXIT_SPECTATOR_AFTERLIFE)
				DEACTIVATE_SPECTATOR_CAM(g_BossSpecData, DSCF_BAIL_FOR_TRANSITION)
				RESET_GLOBAL_SPECTATOR_STRUCT() 
			ENDIF
					
			FIND_PREFERRED_SPECTATOR_TARGET()
			SET_PED_DIES_IN_WATER(PLAYER_PED_ID(), FALSE)
			
		//	NET_SET_PLAYER_CONTROL(LocalPlayer,FALSE)
			DETACH_ANY_PORTABLE_PICKUPS()
			REINIT_NET_TIMER(tdovertimer)
			
		ENDIF
	ELSE		
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdovertimer) > 4000
		OR NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED() // Skip timer if no message displayed
			PRINTLN("[RCC MISSION][MANAGE_EARLY_END_AND_SPECTATE] Timer: Expired")			
			IF NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
				IF NOT IS_SCREEN_FADING_OUT()
				AND NOT IS_SCREEN_FADED_OUT()
					PRINTLN("[RCC MISSION][MANAGE_EARLY_END_AND_SPECTATE] moving to spectator (but fading out first...)" )
					DO_SCREEN_FADE_OUT(500)
				ELSE
					IF IS_SCREEN_FADED_OUT()
						PRINTLN("[RCC MISSION][MANAGE_EARLY_END_AND_SPECTATE] moving to spectator" )
						CLEANUP_EARLY_END_AND_SPECTATE()
					ENDIF
				ENDIF
			ELSE
				CLEAR_ALL_BIG_MESSAGES()
				PRINTLN("[RCC MISSION][MANAGE_EARLY_END_AND_SPECTATE] stuck before spectate - waiting for big message to stop")
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION][MANAGE_EARLY_END_AND_SPECTATE] Timer: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdovertimer), " / ", " 4000")
		ENDIF
	ENDIF

ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: SETTING UP END LEADERBOARD !
//
//************************************************************************************************************************************************************



PROC DO_RESTART_HELP_MISSION()
	STRING sRestarthelp
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		IF SHOULD_SHOW_QUICK_RESTART_OPTION()
			IF IS_BIT_SET(iLocalBoolCheck6,LBOOL6_SHOW_RETRY_HELP)
				DRAW_HUD_OVER_FADE_THIS_FRAME()
			ELSE
					IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
						sRestarthelp = "HQRHELP"
					ENDIF
					IF Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
						sRestarthelp = "HQRHELP1"
					ENDIF
					IF g_bAllowJobReplay
						IF NOT IS_STRING_NULL_OR_EMPTY(sRestarthelp)
							DRAW_HUD_OVER_FADE_THIS_FRAME()
							PRINTLN("[RCC MISSION] PRINTING FAIL RETRY HELP") 
							PRINT_HELP(sRestarthelp)
							SET_BIT(iLocalBoolCheck6,LBOOL6_SHOW_RETRY_HELP)
						ENDIF
					ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	UNUSED_PARAMETER( sRestarthelp )
ENDPROC

#IF IS_DEBUG_BUILD
PROC PRINT_MISSION_DETAILS_BEFORE_LEADERBOARD_STAGE()

	PRINTLN("START PRINT_MISSION_DETAILS_BEFORE_LEADERBOARD_STAGE -------------------------------- ")
	
	INT i
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF (g_MissionControllerserverBD_LB.sleaderboard[i].playerID <> INVALID_PLAYER_INDEX())
			PRINTLN("[LBD_NAMES] i = ", i, "  Name = ", GET_PLAYER_NAME(g_MissionControllerserverBD_LB.sleaderboard[i].playerID))
		ENDIF
		IF (g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant <> -1)
			PRINTLN("[LBD_NAMES] iParticipant = ", g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant)
			PRINTLN("[LBD_NAMES] serverBDpassed i = ", i, "  tl63_ParticipantNames = ", MC_ServerBD_2.tParticipantNames[g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant])
		ENDIF
	ENDREPEAT
	
	INT iPlayerCount = (MC_serverBD.iNumberOfLBPlayers[0] + MC_serverBD.iNumberOfLBPlayers[1] + MC_serverBD.iNumberOfLBPlayers[2] +MC_serverBD.iNumberOfLBPlayers[3])
	INT iNumLBDTeams = MC_serverBD.iNumActiveTeams
	
	PRINTLN("PRINT_MISSION_DETAILS_BEFORE_LEADERBOARD_STAGE, iPlayerCount = ",iPlayerCount)
	PRINTLN("PRINT_MISSION_DETAILS_BEFORE_LEADERBOARD_STAGE, iNumLBDTeams = ",iNumLBDTeams)
	PRINTLN("PRINT_MISSION_DETAILS_BEFORE_LEADERBOARD_STAGE, MC_serverBD.iWinningTeam  = ",MC_serverBD.iWinningTeam)
	PRINTLN("PRINT_MISSION_DETAILS_BEFORE_LEADERBOARD_STAGE, MC_serverBD.iSecondTeam   = ",MC_serverBD.iSecondTeam)
	PRINTLN("PRINT_MISSION_DETAILS_BEFORE_LEADERBOARD_STAGE, MC_serverBD.iThirdTeam    = ",MC_serverBD.iThirdTeam)
	PRINTLN("PRINT_MISSION_DETAILS_BEFORE_LEADERBOARD_STAGE, MC_serverBD.iLosingTeam   = ",MC_serverBD.iLosingTeam)
	
	
	IF NOT IS_SAFE_TO_DRAW_ON_SCREEN()
		PRINTLN("[2056544] PRINT_MISSION_DETAILS_BEFORE_LEADERBOARD_STAGE, NOT IS_SAFE_TO_DRAW_ON_SCREEN() ")
	ENDIF
	
	IF IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
		PRINTLN("[2056544] PRINT_MISSION_DETAILS_BEFORE_LEADERBOARD_STAGE, IS_SCRIPT_HUD_DISPLAYING ")
	ENDIF
	
	IF g_b_PopulateReady = FALSE
		PRINTLN("[2056544] PRINT_MISSION_DETAILS_BEFORE_LEADERBOARD_STAGE, bCheckReady ")
	ENDIF

	IF IS_WARNING_MESSAGE_ACTIVE()
	
		PRINTLN("[2056544] PRINT_MISSION_DETAILS_BEFORE_LEADERBOARD_STAGE, IS_WARNING_MESSAGE_ACTIVE() ")
	ENDIF
	
	//If we should get out because the NJVS is being skipped
	IF SHOULD_TRANSITION_SESSIONS_SKIP_LB_AND_NJVS()
		PRINTLN("[2056544] PRINT_MISSION_DETAILS_BEFORE_LEADERBOARD_STAGE, SHOULD_TRANSITION_SESSIONS_SKIP_LB_AND_NJVS ")
	ENDIF

ENDPROC
#ENDIF

PROC MANAGE_LEADERBOARD_DISPLAY(BOOL bRounds = FALSE)
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		EXIT
	ENDIF

	IF g_i_ActualLeaderboardPlayerSelected > -1
	AND g_i_ActualLeaderboardPlayerSelected < 32
	AND IS_BIT_SET(iSpectatorBit, g_i_ActualLeaderboardPlayerSelected)
		HANDLE_SPECTATOR_PLAYER_CARDS(lbdVars, iSavedSpectatorRow)
	ELSE
		BOOL bJobSpectating = (GET_SPECTATOR_HUD_STAGE(g_BossSpecData.specHUDData) = SPEC_HUD_STAGE_LEADERBOARD)
		HANDLE_THE_PLAYER_CARDS(g_MissionControllerserverBD_LB.sleaderboard, lbdVars, iSavedRow, bJobSpectating)
	ENDIF
	
	IF g_i_ActualLeaderboardPlayerSelected = -1
		#IF IS_DEBUG_BUILD
		PRINT_MISSION_DETAILS_BEFORE_LEADERBOARD_STAGE()
		#ENDIF
		g_i_ActualLeaderboardPlayerSelected = 0
	ENDIF
	
	IF NOT sEndOfMission.bActiveSCLeaderboard	
		DISPLAY_LEADERBOARD()
		PRINTLN("[2184572] MANAGE_LEADERBOARD_DISPLAY, DISPLAY_LEADERBOARD ")
		IF bRounds = FALSE
			DO_RESTART_HELP_MISSION()
		ENDIF
	ELSE
		IF NOT IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
			DRAW_SC_SCALEFORM_LEADERBOARD(scLB_scaleformID,scLB_control)
		ENDIF
	ENDIF
ENDPROC

PROC WRITE_TO_MISSION_SC_LB(BOOL bJustSum = FALSE)
	TEXT_LABEL_31 categoryNames[1] 
   	TEXT_LABEL_23 uniqueIdentifiers[1]
	INT iPosition
	// Don't write if spectating
	IF IS_PLAYER_SCTV(LocalPlayer)
	
		PRINTLN("WRITE_TO_MISSION_SC_LB bypassed because IS_PLAYER_SCTV")
		
		EXIT
	ENDIF
	
	INT i
	
	PRINTLN("WRITE_TO_MISSION_SC_LB ") 
	IF NOT ARE_STRINGS_EQUAL(g_FMMC_STRUCT.tl31LoadedContentID,"")
		categoryNames[0] = "Mission" 
  		uniqueIdentifiers[0] = g_FMMC_STRUCT.tl31LoadedContentID
		FOR i = 0 TO (MAX_NUM_DM_PLAYERS - 1)
	        IF g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant = iLocalPart
				iPosition = i	
				i = MAX_NUM_DM_PLAYERS
	        ENDIF
		ENDFOR
		
		
		IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
		OR Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer) 
			
			IF INIT_LEADERBOARD_WRITE(LEADERBOARD_FREEMODE_HEIST_MISSION,uniqueIdentifiers,categoryNames,1)
				
				// Score.
				IF MC_PlayerBD[iLocalPart].iRowanCSuperHighScore <= g_sMPtunables.iGLOBAL_LEADERBOARD_CAP_HEISTS
					WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_FREEMODE_HEIST_MISSION),LB_INPUT_COL_SCORE,MC_PlayerBD[iLocalPart].iRowanCSuperHighScore,0)
					PRINTLN("[RCC MISSION] - WRITE_TO_MISSION_SC_LB - written score to social club board - ", MC_PlayerBD[iLocalPart].iRowanCSuperHighScore)
				ELSE
					PRINTLN("[RCC MISSION] - WRITE_TO_MISSION_SC_LB - NOT written score to social club board - ", MC_PlayerBD[iLocalPart].iRowanCSuperHighScore)
				ENDIF
				// Time.
				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_FREEMODE_HEIST_MISSION),LB_INPUT_COL_TOTAL_TIME,MC_serverBD.iTotalMissionEndTime,0)
				PRINTLN("[RCC MISSION] - WRITE_TO_MISSION_SC_LB - written time to social club board - ", MC_serverBD.iTotalMissionEndTime)
				
				// Shots fired.
				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_FREEMODE_HEIST_MISSION),LB_INPUT_COL_SHOTS_FIRED,iTotalShotsFired,0)
				PRINTLN("[RCC MISSION] - WRITE_TO_MISSION_SC_LB - written shots fired to social club board - ", iTotalShotsFired)
				
				// Shots hit.
				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_FREEMODE_HEIST_MISSION),LB_INPUT_COL_SHOTS_HIT,iTotalShotsHit,0)
				PRINTLN("[RCC MISSION] - WRITE_TO_MISSION_SC_LB - written shots hit to social club board - ", iTotalShotsHit)
				
				// Headshots.
				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_FREEMODE_HEIST_MISSION),LB_INPUT_COL_HEADSHOTS,MC_Playerbd[iLocalPart].iNumHeadshots,0)
				PRINTLN("[RCC MISSION] - WRITE_TO_MISSION_SC_LB - written headshots to social club board - ", MC_PlayerBD[iLocalPart].iNumHeadshots)
				
				// Kills.
				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_FREEMODE_HEIST_MISSION),LB_INPUT_COL_KILLS,MC_Playerbd[iLocalPart].iNumPedKills,0)
				PRINTLN("[RCC MISSION] - WRITE_TO_MISSION_SC_LB - written enemy kills to social club board - ", MC_PlayerBD[iLocalPart].iNumPedKills)
				
				//1061136 UN_COMMENT WHEN 1098154           
        		LEADERBOARDS_WRITE_ADD_COLUMN_LONG(LB_INPUT_COL_MATCH_ID,g_MissionControllerserverBD_LB.iHashMAC ,g_MissionControllerserverBD_LB.iMatchTimeID)
				PRINTLN("[RCC MISSION] - WRITE_TO_MISSION_SC_LB - written ,atch details to social club board - iHashMAC = ", g_MissionControllerserverBD_LB.iHashMAC, ", iMatchTimeID = ", g_MissionControllerserverBD_LB.iMatchTimeID)
				
			ENDIF
			
		ELSE
		

			IF IS_ARENA_WARS_JOB(TRUE)
				PRINTLN("[ARENA][SC LDB] - WRITE_TO_MISSION_SC_LB - Arena Wars")
				ARENA_WARS_SC_LDB_SET_STAT_1(MC_playerBD_1[iLocalPart].sArenaSCLBStats, ARENA_WARS_SC_LDB_GET_MODE_STAT_1())
				ARENA_WARS_SC_LDB_SET_STAT_2(MC_playerBD_1[iLocalPart].sArenaSCLBStats, ARENA_WARS_SC_LDB_GET_MODE_STAT_2())
				BOOL bWinner = (MC_playerBD[iLocalPart].iTeam = MC_serverBD.iWinningTeam)
				PRINTLN("[ARENA][SC LDB] - WRITE_TO_MISSION_SC_LB - Did my team win: ", BOOL_TO_STRING(bWinner), " Winning team: ", MC_serverBD.iWinningTeam)
				MC_playerBD_1[iLocalPart].sArenaSCLBStats.iFavVehicle = GET_ARENA_FAV_VEH(PLAYER_ID())
				MC_playerBD_1[iLocalPart].sArenaSCLBStats.iSkillLevel = GET_PLAYERS_CURRENT_ARENA_WARS_SKILL_LEVEL(PLAYER_ID())
				
				PRINTLN("[ARENA][SC LDB] - WRITE_TO_MISSION_SC_LB - iFavVeh: ", MC_playerBD_1[iLocalPart].sArenaSCLBStats.iFavVehicle, " iSkillLevel: ", MC_playerBD_1[iLocalPart].sArenaSCLBStats.iSkillLevel)
				ARENA_WARS_SC_LDB_WRTE_TO_LEADERBOARD_DATA(bWinner, MC_playerBD_1[iLocalPart].sArenaPoints, MC_playerBD_1[iLocalPart].sArenaSCLBStats)
			ELIF g_FMMC_STRUCT.iMissionEndType =  ciMISSION_SCORING_TYPE_TIME
				
				// Begin setting the leaderboard data types
				IF INIT_LEADERBOARD_WRITE(LEADERBOARD_FREEMODE_MISSIONS_BY_TIME,uniqueIdentifiers,categoryNames,1)
					
					//*SCORE_COLUMN( AGG_MAX ) : COLUMN_ID_LB_SCORE - LBCOLUMNTYPE_INT64
				   	//KILLS( AGG_LAST ) : COLUMN_ID_LB_KILLS - LBCOLUMNTYPE_INT32
				   	//DEATHS( AGG_LAST ) : COLUMN_ID_LB_DEATHS - LBCOLUMNTYPE_INT32
				    //KILL_DEATH_RATIO( AGG_LAST ) : COLUMN_ID_LB_KILL_DEATH_RATIO - LBCOLUMNTYPE_DOUBLE
				    //TOTAL_TIME( AGG_SUM ) : COLUMN_ID_LB_TOTAL_TIME - LBCOLUMNTYPE_INT64
				    //TOTAL_KILLS( AGG_SUM ) : COLUMN_ID_LB_KILLS - LBCOLUMNTYPE_INT32
				    //TOTAL_DEATHS( AGG_SUM ) : COLUMN_ID_LB_DEATHS - LBCOLUMNTYPE_INT32
				    //TOTAL_KILL_DEATH_RATIO( AGG_SUM ) : COLUMN_ID_LB_KILL_DEATH_RATIO - LBCOLUMNTYPE_DOUBLE
					IF NOT  bJustSum 

						LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE,-g_MissionControllerserverBD_LB.sleaderboard[iPosition].iMissionTime,0)
						
						// Write kills
						LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_KILLS,g_MissionControllerserverBD_LB.sleaderboard[iPosition].iKills,0)
						
						// Write death
						LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_DEATHS,g_MissionControllerserverBD_LB.sleaderboard[iPosition].iDeaths,0)
						
						
					ELSE
						//LEADERBOARDS_WRITE_SET_INT(0,0)
						
						// Write kills
						//LEADERBOARDS_WRITE_SET_INT(1,0)
						
						// Write death
						//LEADERBOARDS_WRITE_SET_INT(2,0)
					ENDIF
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_TOTAL_TIME,g_MissionControllerserverBD_LB.sleaderboard[iPosition].iMissionTime,0)
						
						// Write kills
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_KILLS,g_MissionControllerserverBD_LB.sleaderboard[iPosition].iKills,0)

					// Write death
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_DEATHS,g_MissionControllerserverBD_LB.sleaderboard[iPosition].iDeaths,0)
					
					//1061136 UN_COMMENT WHEN 1098154 
		        	LEADERBOARDS_WRITE_ADD_COLUMN_LONG(LB_INPUT_COL_MATCH_ID,g_MissionControllerserverBD_LB.iHashMAC ,g_MissionControllerserverBD_LB.iMatchTimeID)
				ENDIF
			ELSE
				//LEADERBOARD_FREEMODE_MISSIONS_BY_SCORE
				//*SCORE_COLUMN( AGG_SUM ) : COLUMN_ID_LB_SCORE - LBCOLUMNTYPE_INT64
			   	// KILLS( AGG_SUM ) : COLUMN_ID_LB_KILLS - LBCOLUMNTYPE_INT32
			   	// DEATHS( AGG_SUM ) : COLUMN_ID_LB_DEATHS - LBCOLUMNTYPE_INT32
			   	// KILL_DEATH_RATIO( AGG_LAST ) : COLUMN_ID_LB_KILL_DEATH_RATIO - LBCOLUMNTYPE_DOUBLE
				
				//SUM BOARD
				//INIT_LEADERBOARD_WRITE(LEADERBOARD_FREEMODE_MISSIONS_BY_SCORE,groupHandle)
//				IF INIT_LEADERBOARD_WRITE(LEADERBOARD_FREEMODE_MISSIONS_BY_SCORE,uniqueIdentifiers,categoryNames,1)
//				
//					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE,MC_PlayerBD[iLocalPart].iRowanCSuperHighScore,0)
//						
//					// Write kills
//					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_KILLS,g_MissionControllerserverBD_LB.sleaderboard[iPosition].iKills,0)
//							
//					// Write death
//					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_DEATHS,g_MissionControllerserverBD_LB.sleaderboard[iPosition].iDeaths,0)
//						
//					LEADERBOARDS_WRITE_ADD_COLUMN_LONG(LB_INPUT_COL_MATCH_ID,g_MissionControllerserverBD_LB.iHashMAC ,g_MissionControllerserverBD_LB.iMatchTimeID)
					
					//LEADERBOARD_FREEMODE_MISSIONS_BY_BEST_SCORE
					///*SCORE_COLUMN( AGG_MAX ) : COLUMN_ID_LB_SCORE - LBCOLUMNTYPE_INT64
				   	// KILLS( AGG_LAST ) : COLUMN_ID_LB_KILLS - LBCOLUMNTYPE_INT32
				   	// DEATHS( AGG_LAST ) : COLUMN_ID_LB_DEATHS - LBCOLUMNTYPE_INT32
				    //KILL_DEATH_RATIO( AGG_LAST ) : COLUMN_ID_LB_KILL_DEATH_RATIO - LBCOLUMNTYPE_DOUBLE

					//BEST RUN BOARD
					IF NOT bJustSum 
						IF INIT_LEADERBOARD_WRITE(LEADERBOARD_FREEMODE_MISSIONS_BY_BEST_SCORE,uniqueIdentifiers,categoryNames,1,-1,TRUE)
						
							INT iScoreCap = g_sMPtunables.iGLOBAL_LEADERBOARD_CAP_CONTACT_MISSIONS
							IF IS_THIS_AN_ADVERSARY_MODE_MISSION(g_FMMC_STRUCT.iRootContentIDHash, g_FMMC_STRUCT.iAdversaryModeType)
								iScoreCap = g_sMPtunables.iGLOBAL_LEADERBOARD_CAP_ADVERSARY_MODES
							ELIF IS_THIS_A_CAPTURE()
								iScoreCap = g_sMPtunables.iGLOBAL_LEADERBOARD_CAP_CAPTURES 
							ELIF IS_THIS_A_LTS()
								iScoreCap = g_sMPtunables.iGLOBAL_LEADERBOARD_CAP_LTSS 
							ENDIF
							IF MC_PlayerBD[iLocalPart].iRowanCSuperHighScore <= iScoreCap	
							OR iScoreCap = 0
								WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_FREEMODE_MISSIONS_BY_BEST_SCORE),LB_INPUT_COL_SCORE,MC_PlayerBD[iLocalPart].iRowanCSuperHighScore,0)
							ENDIF
							
							// Write kills
							WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_FREEMODE_MISSIONS_BY_BEST_SCORE),LB_INPUT_COL_KILLS,g_MissionControllerserverBD_LB.sleaderboard[iPosition].iKills,0)
									
							// Write death
							WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_FREEMODE_MISSIONS_BY_BEST_SCORE),LB_INPUT_COL_DEATHS,g_MissionControllerserverBD_LB.sleaderboard[iPosition].iDeaths,0)
							//1061136 UN_COMMENT WHEN 1098154           
			        		LEADERBOARDS_WRITE_ADD_COLUMN_LONG(LB_INPUT_COL_MATCH_ID,g_MissionControllerserverBD_LB.iHashMAC ,g_MissionControllerserverBD_LB.iMatchTimeID)
						ENDIF
					ENDIF
//				ENDIF
			ENDIF
			
		
		ENDIF
		
		
		// NOW PASS THE DATA IN FOR SUBMISSION
		PRINTLN("[RCC MISSION]  -<>- WRITE_TO_MISSION_SC_LB: Score: ",g_MissionControllerserverBD_LB.sleaderboard[iPosition].iPlayerScore) 
		PRINTLN("[RCC MISSION]  -<>- WRITE_TO_MISSION_SC_LB: Kills: ",g_MissionControllerserverBD_LB.sleaderboard[iPosition].iKills) 
		PRINTLN("[RCC MISSION]  -<>- WRITE_TO_MISSION_SC_LB: Deaths: ",g_MissionControllerserverBD_LB.sleaderboard[iPosition].iDeaths) 
		
		//mpGlobals.g_bFlushLBWrites = TRUE
	ELSE
		PRINTLN("[RCC MISSION] WRITE_TO_MISSION_SC_LB: failed g_FMMC_STRUCT.tl31LoadedContentID = ",g_FMMC_STRUCT.tl31LoadedContentID) 
		PRINTLN("[RCC MISSION] WRITE_TO_MISSION_SC_LB: failed GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].tl23CurrentMissionOwner = ", GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].tl23CurrentMissionOwner)
		SCRIPT_ASSERT("Mission trying to write to leaderboard but mission name or creator name not set. See conor")
	ENDIF
ENDPROC

SCRIPT_TIMER timerSocialClubReadTime

FUNC BOOL DONE_RANK_PREDICTION_READ()
	
	INt iSubtype = -1
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET() 
	AND NOT IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
	AND IS_THIS_A_RSTAR_ACTIVITY()
		
		IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
			iSubtype = FMMC_MISSION_TYPE_HEIST
		ELIF  Is_Player_Currently_On_MP_Heist_planning(LocalPlayer)
			iSubtype = FMMC_MISSION_TYPE_PLANNING
		ENDIF
		TEXT_LABEL_23 modeName
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_ARENA_CTF(g_FMMC_STRUCT.iAdversaryModeType)
			modeName = GET_ARENA_WARS_MODE_NAME(SCLB_TYPE_ARENA_MODE_FLAG_WAR)
			SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA(scLB_control,
												SCLB_TYPE_ARENA_WARS_MODES,
												"ArenaMode",
												modeName,
												SCLB_TYPE_ARENA_MODE_FLAG_WAR )
		ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMB_FOOTBALL(g_FMMC_STRUCT.iAdversaryModeType)
			modeName = GET_ARENA_WARS_MODE_NAME(SCLB_TYPE_ARENA_MODE_BOMB_BALL)
			SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA(scLB_control,
												SCLB_TYPE_ARENA_WARS_MODES,
												"ArenaMode",
												modeName,
												SCLB_TYPE_ARENA_MODE_BOMB_BALL)
		ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_GAMES_MASTERS(g_FMMC_STRUCT.iAdversaryModeType)
			modeName = GET_ARENA_WARS_MODE_NAME(SCLB_TYPE_ARENA_MODE_GAMES_MASTERS)
			SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA(scLB_control,
												SCLB_TYPE_ARENA_WARS_MODES,
												"ArenaMode",
												modeName,
												SCLB_TYPE_ARENA_MODE_GAMES_MASTERS)
		ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_MONSTER_JAM(g_FMMC_STRUCT.iAdversaryModeType)
			modeName = GET_ARENA_WARS_MODE_NAME(SCLB_TYPE_ARENA_MODE_MONSTERS)
			SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA(scLB_control,
												SCLB_TYPE_ARENA_WARS_MODES,
												"ArenaMode",
												modeName,
												SCLB_TYPE_ARENA_MODE_MONSTERS)
		ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TAG_TEAM(g_FMMC_STRUCT.iAdversaryModeType)
			modeName = GET_ARENA_WARS_MODE_NAME(SCLB_TYPE_ARENA_MODE_TAG_TEAM)
			SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA(scLB_control,
												SCLB_TYPE_ARENA_WARS_MODES,
												"ArenaMode",
												modeName,
												SCLB_TYPE_ARENA_MODE_TAG_TEAM)
		ELSE
		SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA(scLB_control,GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].iCurrentMissionType,
			g_FMMC_STRUCT.tl31LoadedContentID,
			g_FMMC_STRUCT.tl63MissionName,iSubtype)//GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].tl31MissionName)
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	// 2012085
	IF NOT HAS_NET_TIMER_STARTED(timerSocialClubReadTime)
		START_NET_TIMER( timerSocialClubReadTime )
	ELSE
		IF HAS_NET_TIMER_EXPIRED(timerSocialClubReadTime, RANK_PREDICTION_TIMEOUT)
			WRITE_TO_MISSION_SC_LB(FALSE)
			#IF IS_DEBUG_BUILD
				PRINTLN("DONE_RANK_PREDICTION_READ, WRITE_LEADERBOARD_DATA (RANK_PREDICTION_TIMEOUT) " ) 
			#ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF ( scLB_rank_predict.bFinishedRead AND NOT scLB_rank_predict.bFinishedWrite )
		IF IS_BIT_SET(iLocalBoolCheck2,LBOOL2_PASSED_MISSION)
			WRITE_TO_MISSION_SC_LB(FALSE)
		ELSE
			WRITE_TO_MISSION_SC_LB(FALSE)
		ENDIF
		scLB_rank_predict.bFinishedWrite = TRUE
	ENDIF
	
	IF GET_RANK_PREDICTION_DETAILS(scLB_control)
		sclb_useRankPrediction = TRUE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: PROCESS MISSION END STAGE !
//
//************************************************************************************************************************************************************



FUNC BOOL HAVE_ALL_PLAYERS_FINISHED()

INT iparticipant
PARTICIPANT_INDEX tempPart
PLAYER_INDEX tempPlayer

	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iparticipant
		tempPart = INT_TO_PARTICIPANTINDEX(iparticipant)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
			
			IF NOT IS_PLAYER_SCTV(tempPlayer)
			AND GET_PLAYER_TEAM(tempPlayer) != -1
				IF GET_MC_CLIENT_MISSION_STAGE(iparticipant) < CLIENT_MISSION_STAGE_RESULTS
					PRINTLN("[RCC MISSION] HAVE_ALL_PLAYERS_FINISHED, returning FALSE - GET_MC_CLIENT_MISSION_STAGE < CLIENT_MISSION_STAGE_RESULTS (", GET_MC_CLIENT_MISSION_STAGE(iparticipant),") for part = ",iparticipant)
					RETURN FALSE
				ELSE
					PRINTLN("[RCC MISSION] HAVE_ALL_PLAYERS_FINISHED - GET_MC_CLIENT_MISSION_STAGE >= CLIENT_MISSION_STAGE_RESULTS (", GET_MC_CLIENT_MISSION_STAGE(iparticipant),") for part = ",iparticipant)
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION] HAVE_ALL_PLAYERS_FINISHED - Part ",iparticipant," is an SCTV spectator")
			ENDIF
			
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
	
ENDFUNC

PROC MAINTAIN_PLAYER_CONTROL_AT_END_OF_STRAND_CUTSCENE()
	
	IF IS_BIT_SET(iLocalBoolCheck7, LBOOL7_SET_PLAYER_CONTROL_FOR_BRIDGE_MOCAP)
		EXIT
	ENDIF
	
	IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		EXIT
	ENDIF
	
	IF GET_STRAND_MISSION_TRANSITION_TYPE() != FMMC_BRANCHING_MISSION_TRANSITION_TYPE_MOCAP
		EXIT
	ENDIF
	
	IF MC_serverBD.iNextMission < FMMC_MAX_STRAND_MISSIONS
	AND MC_serverBD.iNextMission >= 0
	AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl23NextContentID[MC_serverBD.iNextMission])
		IF g_FMMC_STRUCT.eBranchingTransitionType[MC_serverBD.iNextMission] != FMMC_BRANCHING_MISSION_TRANSITION_TYPE_MOCAP
			EXIT
		ENDIF
	ENDIF
	
	IF NOT IS_CUTSCENE_PLAYING()
		NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
		SET_BIT(iLocalBoolCheck7, LBOOL7_SET_PLAYER_CONTROL_FOR_BRIDGE_MOCAP)
		PRINTLN("[RCC MISSION][MSRAND] - MAINTAIN_PLAYER_CONTROL_AT_END_OF_STRAND_CUTSCENE")
	ENDIF
	
ENDPROC

FUNC BOOL MISSION_HAS_VALID_MOCAP()

	IF MC_ServerBD.iEndCutscene != ciMISSION_CUTSCENE_DEFAULT
	OR IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()	
		RETURN TRUE
	ENDIF

	RETURN FALSE
	
ENDFUNC 

PROC SELECT_ENDING_TYPE()

	INT iMyTeam = MC_playerBD[iPartToUse].iteam

	IF NOT IS_BIT_SET( iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE )
	AND NOT IS_BIT_SET( iLocalBoolCheck2, LBOOL2_NORMAL_END )
		IF HAS_TEAM_PASSED_MISSION( iMyTeam )
		AND HAS_TEAM_FINISHED( iMyTeam )
#IF IS_DEBUG_BUILD
		OR iPlayerPressedS != -1 AND !g_bVSMission // NOTE(Owain): show end cutscene if all teams are friendly and someone S passes
#ENDIF			
			
			IF MISSION_HAS_VALID_MOCAP()			
				IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()	
					MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_GERALD
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][SELECT_ENDING_TYPE] Player is in tutorial, setting the end cutscene to ciMISSION_CUTSCENE_GERALD" )
					SET_BIT( iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE )
					SCRIPT_TRIGGER_MUSIC_EVENT_STOP()
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][SELECT_ENDING_TYPE] MUSIC - TRIGGER_MUSIC_EVENT(MP_MC_STOP) - G")
					IF NOT AM_I_ON_A_HEIST()
						IF bLocalPlayerPedOk
							TASK_GO_TO_COORD_ANY_MEANS( LocalPlayerPed, GET_CUTSCENE_COORDS(-1), PEDMOVEBLENDRATIO_WALK, NULL )
						ENDIF
					ENDIF
					SET_BIT( MPGlobals.iWatchedMissionMocapBitset, WATCHED_TUTORIAL_MISSION_MOCAP )
					SET_BIT( MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_REQUEST_CUTSCENE_PLAYERS )
					BROADCAST_CUTSCENE_PLAYER_REQUEST(iMyTeam, FMMCCUT_ENDMOCAP)
					SET_BIT(iLocalBoolCheck31, LBOOL31_END_CUTSCENE_STARTED)
					PRINTLN("[LM][url:bugstar:5609784][SELECT_ENDING_TYPE] - Setting PBBOOL_REQUEST_CUTSCENE_PLAYERS (3)")
				ELSE
					IF SHOULD_PLAY_MOCAP()
						IF bLocalPlayerPedOk AND NOT IS_PLAYER_SCTV( LocalPlayer ) AND NOT AM_I_ON_A_HEIST()
							IF SHOULD_WALK_TO_MOCAP_COORDS()
								TASK_GO_TO_COORD_ANY_MEANS( LocalPlayerPed, GET_CUTSCENE_COORDS(-1), PEDMOVEBLENDRATIO_WALK, NULL )
							ENDIF
						ENDIF
						SET_BIT( iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE )
						CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][SELECT_ENDING_TYPE] end mission mocap server number: ", MC_serverBD.iEndCutsceneNum[ iMyTeam ] )
						//2081710 - Humane Labs | Keycodes: Music during Karen arrival cutscene needs updating
						IF NOT IS_STRING_EMPTY( sMocapCutscene)
							IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED() AND NOT IS_THIS_IS_A_STRAND_MISSION()
							OR ( HAS_FIRST_STRAND_MISSION_BEEN_PASSED() AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED() )
								IF NOT ARE_STRINGS_EQUAL( sMocapCutscene, "MPH_TUT_EXT" ) //Stop music cutting out too early in Fleeca cutscene
									SCRIPT_TRIGGER_MUSIC_EVENT_STOP()
									CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][SELECT_ENDING_TYPE] MUSIC - TRIGGER_MUSIC_EVENT(MP_MC_STOP) - H" )
								ENDIF
							ENDIF
						ENDIF
						SET_BIT( MPGlobals.iWatchedMissionMocapBitset, WATCHED_NON_TUTORIAL_MISSION_MOCAP )
						SET_BIT( MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_REQUEST_CUTSCENE_PLAYERS )
						BROADCAST_CUTSCENE_PLAYER_REQUEST(iMyTeam, FMMCCUT_ENDMOCAP)
						SET_BIT(iLocalBoolCheck31, LBOOL31_END_CUTSCENE_STARTED)
						PRINTLN("[LM][url:bugstar:5609784][SELECT_ENDING_TYPE] - Setting PBBOOL_REQUEST_CUTSCENE_PLAYERS (4)")
					ELSE
						SET_BIT( iLocalBoolCheck2, LBOOL2_NORMAL_END )
						CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][SELECT_ENDING_TYPE] normal end show results - pass & should not play mocap" )
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_BIT_SET( ilocalboolcheck, LBOOL_TRIGGER_CTF_END_MUSIC )
				AND NOT IS_BIT_SET( iLocalBoolCheck6, LBOOL6_TRIGGER_LTS_END_MUSIC )
				AND NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC)
				AND NOT IS_BIT_SET(iLocalBoolCheck20, LBOOL20_HAS_STARTED_IE_COUNTDOWN_MUSIC)
				AND NOT (IS_THIS_IS_A_STRAND_MISSION() AND NOT IS_THIS_THE_LAST_IN_A_SERIES_OF_STRAND_MISSIONS())
					SCRIPT_TRIGGER_MUSIC_EVENT_STOP()
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][SELECT_ENDING_TYPE] MUSIC - TRIGGER_MUSIC_EVENT(MP_MC_STOP) - J" )
				ENDIF
				SET_BIT( iLocalBoolCheck2, LBOOL2_NORMAL_END )
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][SELECT_ENDING_TYPE] normal end show results - pass no valid mocap" )
			ENDIF
		ELSE // The team has not finished the mission
			// Stop music that's playing if we're at the end of a match that isn't CTF/LTS
			IF NOT IS_BIT_SET( ilocalboolcheck, LBOOL_TRIGGER_CTF_END_MUSIC )
			AND NOT IS_BIT_SET( iLocalBoolCheck6, LBOOL6_TRIGGER_LTS_END_MUSIC )
			AND NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC)
			AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA(g_FMMC_STRUCT.iAdversaryModeType) // url:bugstar:3184780
			AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_SLASHERS(g_FMMC_STRUCT.iAdversaryModeType)
			AND NOT CONTENT_IS_USING_ARENA()
			AND NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
				SCRIPT_TRIGGER_MUSIC_EVENT_FAIL()
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][SELECT_ENDING_TYPE] MUSIC - TRIGGER_MUSIC_EVENT(MP_MC_FAIL) Set1" )
			ENDIF
			SET_BIT( iLocalBoolCheck2, LBOOL2_NORMAL_END )
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][SELECT_ENDING_TYPE] normal end show results - fail" )
		ENDIF
	ELSE
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][SELECT_ENDING_TYPE] normal end show results - NOT LBOOL2_ACTIVATE_MOCAP_CUTSCENE" )
	ENDIF

ENDPROC

FUNC BOOL IS_MISSION_OVER_REASON_DONE()
	
	IF HAS_NET_TIMER_STARTED(tdovertimer)
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdovertimer) > GET_TIME_FOR_BIG_MESSAGE(BIG_MESSAGE_MISSION_OVER) 
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_PLAYER_IN_A_FLYING_VEHICLE()

	IF IS_PED_IN_ANY_HELI( LocalPlayerPed )
	OR IS_PED_IN_ANY_PLANE( LocalPlayerPed )
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC GIVE_PLAYER_AI_FLY_TASK()

	VECTOR vTarget = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS( LocalPlayerPed, << 0, 200, 100 >> )
	VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN( LocalPlayerPed )
	FLOAT fSpeed = GET_ENTITY_SPEED(LocalPlayerPed)
	//Max cruise speed is 120, having this at 120 still asserts...
	IF fSpeed > 119.0
		fSpeed = 119.0
	ENDIF
	IF IS_PED_IN_ANY_HELI( LocalPlayerPed )
		IF GET_SEAT_PED_IS_IN( LocalPlayerPed ) = VS_DRIVER //Only return true if ped is the pilot.
			TASK_HELI_MISSION( LocalPlayerPed, vehPlayer, NULL, NULL, vTarget, MISSION_GOTO, fSpeed, 1.0, 0.0, 50, 50 )
		ENDIF
	ELIF IS_PED_IN_ANY_PLANE( LocalPlayerPed )
		IF GET_SEAT_PED_IS_IN( LocalPlayerPed ) = VS_DRIVER //Only return true if ped is the pilot.
			TASK_PLANE_MISSION( LocalPlayerPed, vehPlayer, NULL, NULL, vTarget, MISSION_GOTO, fSpeed, 1.0, 0.0, 50, 50 )
		ENDIF
	ENDIF
	
ENDPROC

///PURPOSE: This function checks through all active participants and sees if any team
///    is currently empty: if so, we know that the mission is over due to a team not
///    having any participants any longer, and this calls a flow function of Bobby's
PROC CHECK_LTS_OVER_DUE_TO_TEAM_LEAVING()
	IF IS_THIS_A_ROUNDS_MISSION()
		PRINTLN("[2847407] IF IS_THIS_A_ROUNDS_MISSION()")
		INT iPart = 0
		PARTICIPANT_INDEX tempPart = NULL
		INT iActiveParticipantsPerTeam[ FMMC_MAX_TEAMS ]
		
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iPart
			tempPart = INT_TO_PARTICIPANTINDEX( iPart )
			
			IF NETWORK_IS_PARTICIPANT_ACTIVE( tempPart )
				iActiveParticipantsPerTeam[ MC_playerBD[ iPart ].iteam ]++
			ENDIF
			
		ENDREPEAT
		
		INT iTeamLoop = 0
		
		FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS - 1
			PRINTLN("[2847407] iTeamLoop = ", iTeamLoop, " IF WAS_TEAM_EVER_ACTIVE( iTeamLoop ) = ", WAS_TEAM_EVER_ACTIVE( iTeamLoop ))
			PRINTLN("[2847407] iTeamLoop = ", iTeamLoop, " AND iActiveParticipantsPerTeam[ iTeamLoop ] = ", iActiveParticipantsPerTeam[ iTeamLoop ])
			IF WAS_TEAM_EVER_ACTIVE( iTeamLoop )
			AND iActiveParticipantsPerTeam[ iTeamLoop ] = 0
			AND (iTeamLoop != 0 OR !IS_THIS_ROCKSTAR_MISSION_NEW_VS_GUARDIAN(g_FMMC_STRUCT.iAdversaryModeType)) 
				SET_END_ROUNDS_MISSION_DUE_TO_TEAM_LEAVING()
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

FUNC BOOL DID_PLAYER_PASS_HEIST()
	IF AM_I_ON_A_HEIST()
	OR GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
	OR CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
		IF HAS_TEAM_PASSED_MISSION(MC_playerBD[iLocalPart].iteam)
		
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

// (2013525) Kick spectators out of heists so the rest can restart
PROC DEAL_WITH_HEIST_SPECTATORS_QUICK_RESTART()
	IF IS_PLAYER_PLAYING_OR_PLANNING_HEIST(LocalPlayer)
	OR IS_LOADED_MISSION_TYPE_FLOW_MISSION()
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciOVERTIME_WAIT_YOUR_TURN)
		IF NOT IS_BIT_SET(ilocalboolcheck8, LBOOL8_DEALT_WITH_HEIST_RESTART)
			IF NOT HAS_TEAM_PASSED_MISSION(0)
				IF DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(LocalPlayer)
				AND IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_ACTIVE)
				AND NOT IS_PLAYER_SCTV(PLAYER_ID())
					PRINTLN("[RCC MISSION] [2013525], DEAL_WITH_HEIST_SPECTATORS_QUICK_RESTART, (YES) ")
					
					SET_EMERGENCY_SKYCAM_UP_SPECTATOR_INVITE(TRUE)
					
					SET_BIT(ilocalboolcheck8, LBOOL8_DEALT_WITH_HEIST_RESTART)
					
					IF IS_PLAYER_SCTV(LocalPlayer)
						BAIL_FROM_SCTV_FOLLOW(TRUE)
					ENDIF
					
					MC_SCRIPT_CLEANUP()
					
				ELSE
					PRINTLN("[RCC MISSION] [2013525], DEAL_WITH_HEIST_SPECTATORS_QUICK_RESTART, IS_PLAYER_SPECTATING, (NO) ")
					IF ENOUGH_FOR_QUICK_RESTART()
						PRINTLN("[RCC MISSION] [QUICK_RESTART], DEAL_WITH_HEIST_SPECTATORS_QUICK_RESTART, SET_SHOW_QUICK_RESTART_OPTION,(YES) ")
						SET_SHOW_QUICK_RESTART_OPTION()
						SET_BIT(ilocalboolcheck8, LBOOL8_DEALT_WITH_HEIST_RESTART)
					ELSE
						PRINTLN("[RCC MISSION] [QUICK_RESTART], DEAL_WITH_HEIST_SPECTATORS_QUICK_RESTART, ENOUGH_FOR_QUICK_RESTART, (NO) ")
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION] [2013525], DEAL_WITH_HEIST_SPECTATORS_QUICK_RESTART, HAS_TEAM_PASSED_MISSION, (YES)  ")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_REQUESTED_MC_ASSETS()
	REMOVE_PTFX_ASSET() // Clear up the PTFX assets used by default
	
	REMOVE_INTRO_ANIM(jobIntroData)
				
	IF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_HEIST_FLARE_INDOORS_VFX_REQUEST)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_HEIST_FLARE_UNDERWATER_VFX_REQUEST)
		REMOVE_NAMED_PTFX_ASSET("scr_biolab_heist") //This is to load the underwater flare object for Biolabs B* 1907419
	ENDIF
	
	CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Cleaning up requested assets before we do the end cutscene" )
ENDPROC

FUNC BOOL IS_PLAYER_RESPAWNING_BEFORE_FADE_IN()
	
	IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState < RESPAWN_STATE_LOAD_SCENE
		PRINTLN("[RCC MISSION] - IS_PLAYER_RESPAWNING_BEFORE_FADE_IN - iRespawnState < RESPAWN_STATE_LOAD_SCENE, returning TRUE")
		RETURN TRUE
	ELIF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState = RESPAWN_STATE_LOAD_SCENE
		IF (g_SpawnData.bIsDoingLoadScene)
			PRINTLN("[RCC MISSION] - IS_PLAYER_RESPAWNING_BEFORE_FADE_IN - iRespawnState = RESPAWN_STATE_LOAD_SCENE, and is still doing load scene")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
 
ENDFUNC


PROC PROCESS_HEIST_END_OF_MISSION()
	
	IF  IS_PLAYER_SCTV(PLAYER_ID())
		PRINTLN("[AMEC][HEIST_MISC] - IS_PLAYER_SCTV = TRUE, exit PROCESS_HEIST_END_OF_MISSION ")
		EXIT
	ENDIF
	
	INT iRootContentID = GET_STRAND_ROOT_CONTENT_ID_HASH()
	
	IF iRootContentID = 0
		PRINTLN("[AMEC][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - RootContentID is 0, using g_FMMC_STRUCT data: ", g_FMMC_STRUCT.iRootContentIdHash)
		iRootContentID = g_FMMC_STRUCT.iRootContentIdHash
	ELSE
		PRINTLN("[AMEC][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - GET_STRAND_ROOT_CONTENT_ID_HASH() = ", iRootContentID)
	ENDIF
	
	CLEAR_HEIST_JIP_TO_PLANNING_BOARD()
	
	IF SHOULD_END_OF_MISSION_HEIST_LOGIC_BE_PROCESSED_BECAUSE_OF_STRAND_MISSION() // AKA "No mission is chained AFTER this one"
	
		PRINTLN("[AMEC][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - Saved root contentID hash NOT found (reported: ",iRootContentID,"), this must be a stand-alone mission. Using FMMC_STRUCT root contID: ", g_FMMC_STRUCT.iRootContentIdHash)
		
		SET_HEIST_SUPPRESS_HEIST_BOARD(FALSE)
		
		//Set the stat for the last time that I played a heist Finale
		IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
			PRINTLN("[TS] AM_I_ABLE_TO_PLAY_ANOTHER_HEIST_FINALE_PLEASE SET_MP_INT_CHARACTER_STAT(MP_STAT_CHALLENGE_LAUNCH_TIME, ", GET_CLOUD_TIME_AS_INT(), " + ", g_sMPTunables.iHeistOnCallCoolDown, ")")
			SET_MP_INT_CHARACTER_STAT(MP_STAT_HESIT_LAUNCH_TIME, GET_CLOUD_TIME_AS_INT() + g_sMPTunables.iHeistOnCallCoolDown)
		ELIF GANGOPS_FLOW_IS_CURRENT_MISSION_A_GANGOPS_FINALE()
			PRINTLN("[TS] AM_I_ABLE_TO_PLAY_ANOTHER_HEIST_FINALE_PLEASE MC_serverBD.iTotalNumStartingPlayers =  ", GET_CLOUD_TIME_AS_INT())
			INT iTime
			SWITCH MC_serverBD.iTotalNumStartingPlayers
				CASE 2  
					iTime = g_sMPTunables.iHeist2OnCallCoolDown2
				BREAK
				CASE 3  
					iTime = g_sMPTunables.iHeist2OnCallCoolDown3	
				BREAK
				CASE 4  
				DEFAULT
					iTime = g_sMPTunables.iHeist2OnCallCoolDown4	
				BREAK
			ENDSWITCH
			PRINTLN("[TS] AM_I_ABLE_TO_PLAY_ANOTHER_HEIST_FINALE_PLEASE SET_MP_INT_CHARACTER_STAT(MP_STAT_GANGOPS_LAUNCH_TIME, ", GET_CLOUD_TIME_AS_INT(), " + ", iTime, ")")
			SET_MP_INT_CHARACTER_STAT(MP_STAT_GANGOPS_LAUNCH_TIME, GET_CLOUD_TIME_AS_INT() + iTime)
		ENDIF
		 
		IF HAS_TEAM_PASSED_MISSION(MC_playerBD[iLocalPart].iteam)

			
			PRINTLN("[AMEC][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - Mission passed, checking if it was a HEIST type.")
			
			IF AM_I_ON_A_HEIST()
				
				IF GET_HEIST_AM_I_HEIST_LEADER_GLOBAL()
				OR IS_BIT_SET(MC_playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, PBBOOL_HEIST_HOST)
					
					PRINTLN("[RCC MISSION][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - IS_BIT_SET(PBBOOL_HEIST_HOST) = ", IS_BIT_SET(MC_playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, PBBOOL_HEIST_HOST))
					
					IF IS_HEIST_IN_PROLOG_BITS(iRootContentID)
						PROFILE_SETTING_FREEMODE_PROLOGUE mission
						mission = GET_HEIST_FREEMODE_PROLOGUE_SETTING(iRootContentID)
						SET_FREEMODE_PROLOGUE_PROFILE_SETTINGS(mission, DEFAULT)
						PRINTLN("[RCC MISSION][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - Mission passed, PROFILE_SETTING_FREEMODE_PROLOGUE: ", ENUM_TO_INT(mission))
					ELSE
						PROFILE_SETTING_JOB_ACTIVITY mission
						mission = GET_HEIST_JOB_ACTIVITY_SETTING(iRootContentID)
						SET_JOB_ACTIVITY_PROFILE_SETTINGS(mission, DEFAULT)
						PRINTLN("[RCC MISSION][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - Mission passed, PROFILE_SETTING_JOB_ACTIVITY: ", ENUM_TO_INT(mission))
					ENDIF
					
				ENDIF

				CLEAR_HEIST_CHEATER_BITSETS()
			ENDIF
			
			IF Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
			
				INT index
				
				//If we launched this via the Z menu then don't do this
				#IF IS_DEBUG_BUILD
				IF NOT g_bDebugLaunchedMission
				#ENDIF
					PRINTLN("[AMEC][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - Pre-requisite check: IS heist set-up mission.")
					
					INCREMENT_MP_INT_CHARACTER_AWARD(MP_AWARD_FINISH_HEIST_SETUP_JOB)
					SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_COMPLETE_HEIST_SETUP)
					PRINTLN("[MJL][DAILY OBJECTIVE] This was a Heist Setup")
					IF SHOULD_END_OF_MISSION_HEIST_LOGIC_BE_PROCESSED_BECAUSE_OF_STRAND_MISSION()
						IF IS_THIS_THE_LAST_IN_A_SERIES_OF_STRAND_MISSIONS() OR NOT (IS_A_STRAND_MISSION_BEING_INITIALISED() OR IS_FAKE_MULTIPLAYER_MODE_SET())
							SET_HEIST_ACHIEVEMENT_ID(HEISTACH_H1_DONE)
						ENDIF
					ENDIF
	
					IF iRootContentID != g_sMPTunables.iroot_id_HASH_The_Flecca_Job
					AND iRootContentID != g_sMPTunables.iroot_id_HASH_Tutorial_Car
						TEXT_LABEL_31 tlTeam = BUILD_UNIQUE_HEIST_TEAM_ID(iRootContentID, MC_Playerbd[iLocalPart].iteam)
						SAVE_HEIST_ROLE_DATA_TO_STATS(GET_HASH_KEY(tlTeam))
					ENDIF
					
					// Add check for this being a heist setup type mission when support is available.
					// If this IS a heist type mission, we need to make sure the leader actually finished.
					IF GET_HEIST_AM_I_HEIST_LEADER_GLOBAL()
					OR IS_BIT_SET(MC_playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, PBBOOL_HEIST_HOST)
					
						PRINTLN("[AMEC][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - Pre-requisite check: I AM leader.")
						
						#IF IS_DEBUG_BUILD
						IF NOT GET_MP_BOOL_CHARACTER_STAT(MP_STAT_HEIST_STRAND_INTRO_DONE)
							PRINTLN("[AMEC][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - ERROR! MP_STAT_HEIST_STRAND_INTRO_DONE = FALSE.")
						ENDIF
						#ENDIF
						
						PRINTLN("[AMEC][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - Checking if reported rContID: ",iRootContentID," exists in player stats.")
						INT iMissionIndex = GET_HEIST_MISSION_INDEX_FROM_CONTENTID(iRootContentID)
						
						// Make sure that there aren't any trailing members left in the stats.
						CLEAR_SAVED_MEMBERS_OF_SPECIFIC_HEIST_MISSION(iMissionIndex)
						
						REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() index
							IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(index))
								PLAYER_INDEX playerIndex = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(index))
								IF IS_NET_PLAYER_OK(playerIndex)
									PRINTLN("[AMEC][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION -  IS_NET_PLAYER_OK(",index,") = TRUE")
									IF NOT IS_PLAYER_SCTV(playerIndex)
										SAVE_SPECIFIC_HEIST_PARTICIPANT_GAMERTAG_IN_ORDER(iMissionIndex, GET_HEIST_COMPLETION_RATING(index), playerIndex)
									ELSE
										PRINTLN("[AMEC][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - IS_PLAYER_SCTV(playerIndex) = ",IS_PLAYER_SCTV(playerIndex))
									ENDIF
								ELSE
									PRINTLN("[AMEC][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION -  IS_NET_PLAYER_OK(",index,") = FALSE")
								ENDIF
							ENDIF
						ENDREPEAT
					
						INT iTempHeistBitset = GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_PLANNING_STAGE)
						
						IF iMissionIndex >= 0
						
							INT iBitRange = GET_BIT_RANGE_FROM_MISSION_ORDER(iMissionIndex)
						
							PRINTLN("[AMEC][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - Returned BitRange: ", iBitRange)
						
							// TODO: Iterate through the bitrange for branches. Branching currently not supported.
							IF NOT IS_BIT_SET(iTempHeistBitset, iBitRange)
								PRINTLN("[AMEC][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - Setting a bit for BitRange: ", iBitRange)
								SET_BIT(iTempHeistBitset, iBitRange)
							ENDIF
							SET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_PLANNING_STAGE, iTempHeistBitset)
						ELSE
							PRINTLN("[AMEC][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - ERROR! Returned mission index was < 0, value: ", iMissionIndex, ". Cannot progress MP progress stat!")
						ENDIF
										
#IF FEATURE_GEN9_STANDALONE
						IF GET_HEIST_AM_I_HEIST_LEADER_GLOBAL()
							// If we are the heist leader, make the relevant UDS calls
							HEIST_UDS_ACTIVITY_SETUP_MISSION_COMPLETED(iRootContentID)
						ENDIF
#ENDIF // FEATURE_GEN9_STANDALONE

						UPDATE_HEIST_PROGRESS_STATS()
														
						CLEAR_STRAND_ROOT_CONTENT_ID_HASH()
						
						IF NOT GET_MP_BOOL_CHARACTER_STAT(MP_STAT_HEIST_AWARD_DONE_PREP)
							#IF IS_DEBUG_BUILD
								PRINTLN("[AMEC][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - Setting HEIST_AWARD_DONE_PREP stat.")
							#ENDIF
							
							
							SET_MP_BOOL_CHARACTER_STAT(MP_STAT_HEIST_AWARD_DONE_PREP, TRUE)
						ENDIF
						
						GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].iCurrentHeistSetupIndex = INVALID_HEIST_DATA
					
					ENDIF
					
					PRINTLN("[AMEC][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - Saving PREP HASH: ", iRootContentID, " in stat MP_STAT_BEND_PROGRESS_HASH for B* 2045775.")
					SET_MP_INT_CHARACTER_STAT(MP_STAT_BEND_PROGRESS_HASH, iRootContentID)
					
					//As above, but player specific
					SET_MP_INT_PLAYER_STAT(MPPLY_HEIST_PROGRESS_HASH,iRootContentID)
					
					IF GET_HEIST_STARTED_TUTORIAL_THIS_SESSION()
						PRINTLN("[AMEC][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - SET_HEIST_STARTED_TUTORIAL_THIS_SESSION = FALSE.")
						SET_HEIST_STARTED_TUTORIAL_THIS_SESSION(FALSE)
					ENDIF
					
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[AMEC][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - Completed mission is SETUP but g_bDebugLaunchedMission = TRUE; not saving stats.")
				ENDIF
				#ENDIF
				
			ELIF Is_Player_Currently_On_MP_Heist(LocalPlayer)
			//If we launched this via the Z menu then don't do this
			
				#IF IS_DEBUG_BUILD
				IF NOT g_bDebugLaunchedMission
				#ENDIF
				
					INT index
					TEXT_LABEL_23 tlRContID
	
					IF GET_HEIST_AM_I_HEIST_LEADER_GLOBAL()
					OR IS_BIT_SET(MC_playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, PBBOOL_HEIST_HOST)
					
						tlRContID = GET_HEIST_FINALE_STAT_DATA()
						IF NOT IS_STRING_NULL_OR_EMPTY(tlRContID)
						AND NOT ARE_STRINGS_EQUAL(tlRContID, ".")
							g_HeistTelemetryData.iRootContentIdHash = GET_HASH_KEY(tlRContID)
							PRINTLN("[AMEC][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - LEADER - g_HeistTelemetryData.iRootContentIdHash = ", g_HeistTelemetryData.iRootContentIdHash)
						ELSE
							PRINTLN("[AMEC][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - LEADER - Not saving tlRContID value: ", tlRContID)
						ENDIF
					ELSE
						REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() index
							IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(index))
								IF IS_BIT_SET(MC_playerBD[index].iClientBitSet, PBBOOL_HEIST_HOST)
									PLAYER_INDEX playerIndex = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(index))
									IF IS_NET_PLAYER_OK(playerIndex)
									
										tlRContID = GlobalplayerBD_FM_HeistPlanning[NATIVE_TO_INT(playerIndex)].tlHeistFinaleRootContID
									
										IF NOT IS_STRING_NULL_OR_EMPTY(tlRContID)
										AND NOT ARE_STRINGS_EQUAL(tlRContID, ".")
											g_HeistTelemetryData.iRootContentIdHash = GET_HASH_KEY(tlRContID)
											PRINTLN("[AMEC][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - MEMBER - g_HeistTelemetryData.iRootContentIdHash = ", g_HeistTelemetryData.iRootContentIdHash)
										ELSE
											PRINTLN("[AMEC][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - MEMBER - Not saving tlRContID value: ", tlRContID)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDREPEAT
					ENDIF
					
					PRINTLN("[AMEC][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - Completed mission is a HEIST FINALE.")
					
					IF iRootContentID != g_sMPTunables.iroot_id_HASH_The_Flecca_Job
					AND iRootContentID != g_sMPTunables.iroot_id_HASH_Tutorial_Car
						TEXT_LABEL_31 tlTeam = BUILD_UNIQUE_HEIST_TEAM_ID(iRootContentID, MC_Playerbd[iLocalPart].iteam)
						SAVE_HEIST_ROLE_DATA_TO_STATS(GET_HASH_KEY(tlTeam))
					ENDIF
					
					INCREMENT_MP_INT_CHARACTER_AWARD(MP_AWARD_FINISH_HEISTS)
					SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_COMPLETE_HEIST_FINALE)
					PRINTLN("[MJL][DAILY OBJECTIVE] This was a Heist Finale")
					
					IF (MC_Playerbd[iLocalPart].iNumPlayerDeaths <= 0)
						INCREMENT_MP_INT_CHARACTER_AWARD(MP_AWARD_COMPLETE_HEIST_NOT_DIE)										
						IF (MC_Playerbd[iLocalPart].iPlayerDamage <= 0)					
							IF NOT IS_THIS_A_QUICK_RESTART_JOB() AND NOT IS_CORONA_INITIALISING_A_QUICK_RESTART()
								SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_FINISH_HEIST_NO_DAMAGE, TRUE)
								SET_HEIST_ACHIEVEMENT_ID(HEISTACH_H8_DONE)
							ENDIF
						ENDIF					
					ENDIF
					
					IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_HEIST_HOST)
						IF (g_TransitionSessionNonResetVars.iHeistCutPercent >= 100)
							//SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_TAKE_ALL_OF_HEIST_TAKE, TRUE)
						ENDIF
					ENDIF
				
					CPRINTLN(DEBUG_ACHIEVEMENT, "[ACHIEVEMENT] - FOUR WAY - NUMBER OF PLAYERS:", (MC_serverBD.iNumberOfLBPlayers[0] + MC_serverBD.iNumberOfLBPlayers[1] + MC_serverBD.iNumberOfLBPlayers[2] + MC_serverBD.iNumberOfLBPlayers[3]), " AM I LEADER?:", GET_HEIST_AM_I_HEIST_LEADER_GLOBAL())
					
					IF g_TransitionSessionNonResetVars.bAllHeistCutsEven

						IF ((MC_serverBD.iNumberOfLBPlayers[0] + MC_serverBD.iNumberOfLBPlayers[1] + MC_serverBD.iNumberOfLBPlayers[2] + MC_serverBD.iNumberOfLBPlayers[3]) = 4)
							
							IF (GET_HEIST_AM_I_HEIST_LEADER_GLOBAL())
							OR IS_BIT_SET(MC_playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, PBBOOL_HEIST_HOST)
								SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_SPLIT_HEIST_TAKE_EVENLY, TRUE)
								SET_HEIST_ACHIEVEMENT_ID(HEISTACH_H6_DONE)
							ENDIF
						ENDIF
					ENDIF
					
					PRINTLN("[AMEC][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - Saving FINALE HASH: ", iRootContentID, " in stat MP_STAT_BEND_PROGRESS_HASH for B* 2045775.")
					SET_MP_INT_CHARACTER_STAT(MP_STAT_BEND_PROGRESS_HASH, iRootContentID)
					
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[AMEC][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - Completed mission is FINALE but g_bDebugLaunchedMission = TRUE; not saving stats.")
				ENDIF
				#ENDIF
				
				BOOL bIsPlayerMale = (GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE) = 0)
				IF iRootContentID = g_sMPTunables.iroot_id_HASH_The_Flecca_Job
					// Unlock Ski Mask , Gas Mask and LCD
					SET_PACKED_STAT_BOOL(PACKED_MP_STAT_UNLOCKED_SKI_MASK, TRUE)
					SET_PACKED_STAT_BOOL(PACKED_MP_STAT_UNLOCKED_GAS_MASK, TRUE)
					SET_PACKED_STAT_BOOL(PACKED_MP_STAT_UNLOCKED_LCD_EARPIECE, TRUE)
					PRINTLN("[AMEC][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - Unlock SKI MASK, GAS MASK and LCD EARPIECE.")
					IF bIsPlayerMale
						CLEAR_BIT_SHOP_PED_APPAREL_SCRIPT(ENUM_TO_INT(DLC_MP_HEIST_M_EAR2_0), PED_COMPONENT_USED_SLOT)  //LCD
						CLEAR_BIT_SHOP_PED_APPAREL_SCRIPT(ENUM_TO_INT(DLC_MP_HEIST_M_BERD_3_0) , PED_COMPONENT_USED_SLOT)// gas mask
					ELSE
						CLEAR_BIT_SHOP_PED_APPAREL_SCRIPT(ENUM_TO_INT(DLC_MP_HEIST_F_EAR2_0), PED_COMPONENT_USED_SLOT)  //LCD
						CLEAR_BIT_SHOP_PED_APPAREL_SCRIPT(ENUM_TO_INT(DLC_MP_HEIST_F_BERD_3_0) , PED_COMPONENT_USED_SLOT)// gas mask
					ENDIF
				ENDIF
				
				IF iRootContentID = g_sMPTunables.iroot_id_HASH_The_Humane_Labs_Raid
					// Flaregun, rebreather and night vision
					SET_PACKED_STAT_BOOL(PACKED_MP_STAT_UNLOCKED_FLAREGUN, TRUE)
					SET_PACKED_STAT_BOOL(PACKED_MP_STAT_UNLOCKED_REBREATHER, TRUE)
					SET_PACKED_STAT_BOOL(PACKED_MP_STAT_UNLOCKED_NIGHTVISION, TRUE)
					
					
					IF bIsPlayerMale
						CLEAR_BIT_SHOP_PED_APPAREL_SCRIPT(ENUM_TO_INT(DLC_MP_HEIST_M_BERD_1_0), PED_COMPONENT_USED_SLOT) // rebreather
						CLEAR_BIT_SHOP_PED_APPAREL_SCRIPT(ENUM_TO_INT(DLC_MP_HEIST_M_BERD_0_0), PED_COMPONENT_USED_SLOT) // night vision
					
					ELSE
						CLEAR_BIT_SHOP_PED_APPAREL_SCRIPT(ENUM_TO_INT(DLC_MP_HEIST_F_BERD_1_0), PED_COMPONENT_USED_SLOT) // rebreather
						CLEAR_BIT_SHOP_PED_APPAREL_SCRIPT(ENUM_TO_INT(DLC_MP_HEIST_F_BERD_0_0), PED_COMPONENT_USED_SLOT) // night vision
					ENDIF
					PRINTLN("[AMEC][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - Unlock PACKED_MP_STAT_UNLOCKED_FLAREGUN, PACKED_MP_STAT_UNLOCKED_REBREATHER and PACKED_MP_STAT_UNLOCKED_NIGHTVISION.")
				ENDIF
				
				// KGM 21/11/14 [BUG 2139001]: The Heist strand 'finish' should only get called if the player is leader and this is a one part Finale OR part two of a two part Finale
				BOOL shouldFinishCurrentHeistStrand = HAS_TEAM_PASSED_MISSION(MC_playerBD[iLocalPart].iteam)
				
				IF (shouldFinishCurrentHeistStrand)
					IF NOT (GET_HEIST_AM_I_HEIST_LEADER_GLOBAL())
					AND NOT IS_BIT_SET(MC_playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, PBBOOL_HEIST_HOST)
						shouldFinishCurrentHeistStrand = FALSE
					ENDIF
				ENDIF
				
				// ...if TRUE, player is leader
				IF (shouldFinishCurrentHeistStrand)
					// Is this a Strand Mission? If not, finish. If yes, only finish if the last part.
					IF (IS_THIS_IS_A_STRAND_MISSION())
						// ...Strand mission, so clear the 'should finish' flag if only part one
						IF NOT (HAS_FIRST_STRAND_MISSION_BEEN_PASSED())
							shouldFinishCurrentHeistStrand = FALSE
						ENDIF
					ENDIF
				ENDIF
			
				IF (shouldFinishCurrentHeistStrand)
					CPRINTLN(DEBUG_ACHIEVEMENT, "[ACHIEVEMENT] - HEIST: FINISH_CURRENT_HEIST_STRAND - WILL BE CALLED HERE. I AM LEADER AND THIS IS THE ONLY, OR FINAL, PART OF THE FINALE.")
					PRINTLN("[AMEC][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - Completed mission is FINALE and I AM leader, clean up heist strand.")
					FINISH_CURRENT_HEIST_STRAND()
				ELSE
					#IF IS_DEBUG_BUILD
						IF (HAS_TEAM_PASSED_MISSION(MC_playerBD[iLocalPart].iteam))
							IF GET_HEIST_AM_I_HEIST_LEADER_GLOBAL()
							OR IS_BIT_SET(MC_playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, PBBOOL_HEIST_HOST)
								PRINTLN("[AMEC][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - Completed mission is FINALE and I am leader, BUT this is Part One of a Strand Mission, do NOT clean up heist strand.")
							ELSE
								PRINTLN("[AMEC][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - Completed mission is FINALE and I am NOT leader, do NOT clean up heist strand.")
							ENDIF
						ELSE
							PRINTLN("[AMEC][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - Failed mission, do NOT clean up heist strand.")
						ENDIF
					#ENDIF
				ENDIF
				
			ELSE
			
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - Completed mission is not FINALE or SETUP, do nothing.")
				#ENDIF
				
			ENDIF
			IF Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
			OR Is_Player_Currently_On_MP_Heist(LocalPlayer)
				IF (GET_HEIST_RATING_HUD_COLOUR(iLocalPart) = HUD_COLOUR_PLATINUM)
					INCREMENT_MP_INT_CHARACTER_AWARD(MP_AWARD_WIN_GOLD_MEDAL_HEISTS)
					INCREMENT_MP_INT_PLAYER_STAT(MPPLY_WIN_GOLD_MEDAL_HEISTS)

					IF (GET_MP_INT_PLAYER_STAT(MPPLY_WIN_GOLD_MEDAL_HEISTS) >= 25) AND NOT HAS_ACHIEVEMENT_BEEN_PASSED(ENUM_TO_INT(ACHH10))
						SET_HEIST_ACHIEVEMENT_ID(HEISTACH_H10_DONE)
					ENDIF							
				ENDIF
			ENDIF
		
		ELIF NOT HAS_TEAM_PASSED_MISSION(MC_playerBD[iLocalPart].iteam)
		AND NOT g_bVSMission
		
			IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
			AND g_bHeistCheaterIncremented
				DECREMENT_CURRENT_HEIST_CHEATER_BITSET()
			ENDIF
			
			IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FINALE()
			AND g_bGangopsCheaterIncremented
				DECREMENT_CURRENT_GANGOPS_CHEATER_BITSET()
			ENDIF
			
			IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
			AND g_bCasinoHeistCheaterIncremented
				DECREMENT_CURRENT_CASINO_HEIST_CHEATER_BITSET()
			ENDIF
		
			#IF IS_DEBUG_BUILD
				TEXT_LABEL_23 tlTemp = GET_STRAND_MISSION_CONTENT_ID()
				PRINTLN("[AMEC][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - Using strand ContentID to get mission index. ContentID: ", tlTemp)
			#ENDIF
			
			INT iMissionIndex = GET_HEIST_MISSION_INDEX_FROM_CONTENTID(iRootContentID)
			
			//B* 2068264, if planning mission was previously completed as leader, don't wipe stats.
			
			IF iMissionIndex != -1
				IF iMissionIndex != ci_HEIST_CONTENT_INDEX_FINALE AND GlobalplayerBD_FM_HeistPlanning[NATIVE_TO_INT(LocalPlayer)].iSelectedSetupMissions[iMissionIndex] = 0
					PRINTLN("[AMEC][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - Planning mission ",iMissionIndex," has previously been completed, not clearing stats")
				ELSE				
					PRINTLN("[AMEC][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - Returned index: ", iMissionIndex)
					CLEAR_SAVED_HEIST_PARTICIPANT_GAMERTAGS(iMissionIndex)
					
					IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
						PRINTLN("[AMEC][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - Heist finale FAILED, saving done cutscene stats to allow skipping from this point onwards.")
						SET_MP_BOOL_CHARACTER_STAT(MP_STAT_HEIST_CUTS_DONE_FINALE, TRUE)
					ENDIF					
				ENDIF
			ENDIF
			
		ENDIF
		
	ELSE
	
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - IS_A_STRAND_MISSION_BEING_INITIALISED() = TRUE, don't do heist end-mission processing yet.")
			PRINTLN("[AMEC][HEIST_MISC] - PROCESS_HEIST_END_OF_MISSION - Saved root contentID hash found, this must be a chained mission. Use saved hash: ", iRootContentID)
		#ENDIF
		
		IF HAS_TEAM_PASSED_MISSION(MC_playerBD[iLocalPart].iteam)
			PRINTLN("[RCC MISSION] POPULATE RESULTS - Setting strand mission team deaths: team 0 as ",MC_serverBD.iTeamDeaths[0],", team 1 as ",MC_serverBD.iTeamDeaths[1],", team 2 as ",MC_serverBD.iTeamDeaths[2],", team 3 as ",MC_serverBD.iTeamDeaths[3])
			SET_STRAND_MISSION_TEAM_DEATHS(MC_serverBD.iTeamDeaths[0], MC_serverBD.iTeamDeaths[1], MC_serverBD.iTeamDeaths[2], MC_serverBD.iTeamDeaths[3])
		ENDIF
		
	ENDIF
	
ENDPROC


//BUGFIX(Owain): Hacky fix for B* 2310234 - always untoggle renderphases if we're on the leaderboard
PROC UNTOGGLE_RENDERPHASES_FOR_LEADERBOARD()

	IF NOT IS_BIT_SET( iLocalBoolCheck13, LBOOL13_UNTOGGLED_RENDERPHASES_BEFORE_LEADERBOARD )
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] We've not untoggled the renderphases yet - untoggle them" )
		TOGGLE_RENDERPHASES( TRUE )
		SET_BIT( iLocalBoolCheck13, LBOOL13_UNTOGGLED_RENDERPHASES_BEFORE_LEADERBOARD )
	ENDIF

ENDPROC

//Returns true if a none SCTV player is on the LB
FUNC BOOL ARE_OTHER_PLAYERS_ON_LB()
	
	INT iPartLoop
	PLAYER_INDEX playerID
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iPartLoop
		//If they are active
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartLoop))
			//Get player ID
			playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartLoop))	
			
			IF NOT IS_PLAYER_SCTV(playerID)
			AND IS_PLAYER_ON_MISSION_LB(playerID)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL HAS_LEADERBOARD_TIMER_EXPIRED()	
	
	IF NOT HAS_NET_TIMER_STARTED(tdForceEndTimer)
		//If we're SCTV on a hest and not on our own
		IF IS_PLAYER_SCTV(LocalPlayer)
		AND (Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer) OR Is_Player_Currently_On_MP_Heist(LocalPlayer))
		AND NETWORK_GET_NUM_PARTICIPANTS() > 1
			IF ARE_OTHER_PLAYERS_ON_LB()
				IF NOT IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING() 
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_POWERPLAY_PICKUP_SOUNDS)
						START_AUDIO_SCENE("MP_LEADERBOARD_SCENE")
					ELSE
						START_AUDIO_SCENE("MP_LEADERBOARD_PP_SCENE")
					ENDIF
				ENDIF
				
				REINIT_NET_TIMER(tdForceEndTimer)
			ENDIF
		ELSE
			IF NOT IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING() 
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_POWERPLAY_PICKUP_SOUNDS)
					START_AUDIO_SCENE("MP_LEADERBOARD_SCENE")
				ELSE
					START_AUDIO_SCENE("MP_LEADERBOARD_PP_SCENE")
				ENDIF
			ENDIF
			
			REINIT_NET_TIMER(tdForceEndTimer)
		ENDIF
	ELSE
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdForceEndTimer) > VIEW_LEADERBOARD_TIME
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_CONTINUE_TO_MISSION_END(INT iEndDelay)

	IF IS_BIT_SET(iLocalBoolCheck2,LBOOL2_PLAY_END_MUSIC)
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdresultdelay) > 2000
			RETURN TRUE
		ENDIF
	ENDIF
	IF HAS_NET_TIMER_STARTED(sCelebrationData.timerCelebPreLoadPostFxBeenPlaying)
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sCelebrationData.timerCelebPreLoadPostFxBeenPlaying) > ciMissionEndEffectDelay
			RETURN TRUE
		ENDIF
	ELSE
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdresultdelay) > iEndDelay
		OR IS_BIT_SET(iLocalBoolCheck2,LBOOL2_ACTIVATE_MOCAP_CUTSCENE)
			RETURN TRUE
		ENDIF
	ENDIF	

	RETURN FALSE
	
ENDFUNC

PROC MAKE_PLAYER_INVINCIBLE_ONCE_MISSION_IS_OVER()

	SET_ENTITY_INVINCIBLE( PLAYER_PED_ID(), TRUE )
	SET_ENTITY_PROOFS( PLAYER_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE, TRUE )
	
ENDPROC

INT iClearDeathVisualsStage

//Check that there's enough players for this round
FUNC BOOL ARE_TEAM_NUMBERS_TOO_LOW_FOR_NEXT_ROUND()
	PRINTLN("[TS] - ARE_TEAM_NUMBERS_TOO_LOW_FOR_NEXT_ROUND ")
	//If it's a rounds mission
	IF g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds != 0
	
		INT iTeamCount[FMMC_MAX_TEAMS], iPartLoop
		PLAYER_INDEX playerID
				
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iPartLoop
			//If they are active
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartLoop))
				//Get player ID
				playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartLoop))	
				IF playerID != INVALID_PLAYER_INDEX()
					INT iTeam = GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].sClientCoronaData.iTeamChosen
					//Not an array over run
					IF iTeam != -1
					AND iTeam < FMMC_MAX_TEAMS
					AND NOT IS_PLAYER_SCTV(playerID)
						//Incrament the count of that team
						iTeamCount[iTeam]++
						PRINTLN("[TS] - ARE_TEAM_NUMBERS_TOO_LOW_FOR_NEXT_ROUND iTeamCount[", iTeam, "] = ", iTeamCount[iTeam])
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		//Loop the number of teams
		REPEAT g_FMMC_STRUCT.iNumberOfTeams iPartLoop
			PRINTLN("[TS] - ARE_TEAM_NUMBERS_TOO_LOW_FOR_NEXT_ROUND iTeamCount[", iPartLoop, "] = ", iTeamCount[iPartLoop])
			PRINTLN("[TS] - ARE_TEAM_NUMBERS_TOO_LOW_FOR_NEXT_ROUND g_FMMC_STRUCT.iNumPlayersPerTeam[", iPartLoop, "] = ", g_FMMC_STRUCT.iNumPlayersPerTeam[iPartLoop])
			//If the team count is less than the min
			IF iTeamCount[iPartLoop] < g_FMMC_STRUCT.iNumPlayersPerTeam[iPartLoop]
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_GUARDIAN(g_FMMC_STRUCT.iAdversaryModeType)
				AND iPartLoop = 0
				AND iTeamCount[iPartLoop] = 0
					PRINTLN("[TS] * - ARE_TEAM_NUMBERS_TOO_LOW_FOR_NEXT_ROUND() - Skipping team 0 as IS_THIS_ROCKSTAR_MISSION_NEW_VS_GUARDIAN")
				ELSE
					PRINTLN("[TS] * - ARE_TEAM_NUMBERS_TOO_LOW_FOR_NEXT_ROUND() - iTeamCount[", iPartLoop, "] < g_FMMC_STRUCT.iNumPlayersPerTeam[", iPartLoop, "]")
					#IF USE_FINAL_PRINTS PRINTLN_FINAL("[TS] * - ARE_TEAM_NUMBERS_TOO_LOW_FOR_NEXT_ROUND() - iTeamCount[", iPartLoop, "] < g_FMMC_STRUCT.iNumPlayersPerTeam[", iPartLoop, "]")#ENDIF
					SET_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_TOO_FEW_PLAYERS_FOR_NEXT_ROUND)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN FALSE
ENDFUNC



PROC PROCESS_MISSION_END_STAGE()

	INT iEndDelay,iteam, i
	
	BOOL bDelayEnd = FALSE
	IF IS_THIS_ROCKSTAR_MISSION_WVM_TRAILERLARGE(g_FMMC_STRUCT.iRootContentIDHash)
		IF IS_PLAYER_IN_CREATOR_TRAILER(LocalPlayer)
			IF GB_GET_LOCAL_PLAYER_GANG_BOSS() = INVALID_PLAYER_INDEX()
			OR (GB_GET_LOCAL_PLAYER_GANG_BOSS() != INVALID_PLAYER_INDEX() AND NOT NETWORK_IS_PLAYER_A_PARTICIPANT(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
				TRIGGER_EXIT_FROM_SIMPLE_INTERIOR_NOW()
				PRINTLN("[RCC MISSION] - PROCESS_MISSION_END_STAGE - Attempting to leave trailer")
			ENDIF
			PRINTLN("[RCC MISSION] - PROCESS_MISSION_END_STAGE - Delaying mission end due to player in trailer")
			bDelayEnd = TRUE
		ENDIF
		IF GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
			IF GB_GET_LOCAL_PLAYER_GANG_BOSS() = LocalPlayer
				IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].SimpleInteriorBD.iBSTwo,  BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_EMPTY_ARMORY_TRUCK_TRAILER) 
					SET_EMPTY_ARMORY_TRUCK_TRAILER(TRUE)
					PRINTLN("[RCC MISSION] - PROCESS_MISSION_END_STAGE - Setting BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_EMPTY_ARMORY_TRUCK_TRAILER")
				ENDIF
			ENDIF
		ENDIF
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_AVENGER_INTERIOR_ENTRY)
	AND MC_serverBD.iEndCutsceneNum[MC_playerBD[iPartToUse].iteam] = -1
	
		IF IS_PLAYER_IN_CREATOR_AIRCRAFT(LocalPlayer)
			IF GB_GET_LOCAL_PLAYER_GANG_BOSS() = INVALID_PLAYER_INDEX()
			OR (GB_GET_LOCAL_PLAYER_GANG_BOSS() != INVALID_PLAYER_INDEX() AND NOT NETWORK_IS_PLAYER_A_PARTICIPANT(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
				TRIGGER_EXIT_FROM_SIMPLE_INTERIOR_NOW()
				PRINTLN("[RCC MISSION] - PROCESS_MISSION_END_STAGE - Attempting to leave avenger")
			ENDIF
			PRINTLN("[RCC MISSION] - PROCESS_MISSION_END_STAGE - Delaying mission end due to player in avenger")
			bDelayEnd = TRUE
			IF NOT g_bLeavingVehInteriorForMCEnd
				g_bLeavingVehInteriorForMCEnd = TRUE
			ENDIF
		ENDIF
		IF GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
			IF GB_GET_LOCAL_PLAYER_GANG_BOSS() = LocalPlayer
				IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].SimpleInteriorBD.iBSThree,  BS_SIMPLE_INTERIOR_GLOBAL_BS_THREE_KICK_EVERY_ONE_OUT_OF_ARMORY_AIRCRAFT) 
					SET_EMPTY_AVENGER_HOLD(TRUE)
					PRINTLN("[RCC MISSION] - PROCESS_MISSION_END_STAGE - Setting BS_SIMPLE_INTERIOR_GLOBAL_BS_THREE_KICK_EVERY_ONE_OUT_OF_ARMORY_AIRCRAFT")
				ENDIF
			ENDIF
		ENDIF
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_COME_OUT_TO_PLAY_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
	 	IF manualRespawnState = eManualRespawnState_RESPAWNING
			bDelayEnd = TRUE
			PRINTLN("[RCC MISSION] PROCESS_MISSION_END_STAGE - Delaying mission end due to manualRespawnState = eManualRespawnState_RESPAWNING being set.")
		ENDIF
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_STUNTING_PACK(g_FMMC_STRUCT.iAdversaryModeType)
		IF manualRespawnState = eManualRespawnState_RESPAWNING
			bDelayEnd = TRUE
			PRINTLN("[RCC MISSION] PROCESS_MISSION_END_STAGE - Delaying mission end due to manualRespawnState = eManualRespawnState_RESPAWNING being set.")
		ENDIF
		
		IF MC_serverBD_4.iCurrentHighestPriority[ciHuntingPackRemix_RUNNER] = 1 //If the runner is on rule 1 as the game is ending, that must mean that they exploded/died
		AND GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
			IF HAS_NET_TIMER_STARTED(tdStuntingPackDelay)
				IF NOT HAS_NET_TIMER_EXPIRED(tdStuntingPackDelay, 3500)
					bDelayEnd = TRUE
					PRINTLN("[RCC MISSION] - [HPEND] PROCESS_MISSION_END_STAGE - Delaying mission end to allow time to see the Stunting Pack explosion")
				ENDIF
			ELSE
				START_NET_TIMER(tdStuntingPackDelay)
				bDelayEnd = TRUE
				PRINTLN("[RCC MISSION] - [HPEND] PROCESS_MISSION_END_STAGE - Delaying mission end to start the timer to see the Stunting Pack explosion")
			ENDIF
		ENDIF
	ENDIF
	
	IF GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
	AND NOT bDelayEnd
		g_bLeavingVehInteriorForMCEnd = FALSE
		g_bMissionOver = TRUE
		
		IF NOT IS_STRAND_MISSION_READY_TO_START_DOWNLOAD()
		AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_DelayNextMissionPreload)
			SET_STRAND_MISSION_READY_TO_START_DOWNLOAD(MC_serverBD.iNextMission)
			PRINTLN("[RCC MISSION][MSRAND] - PROCESS_MISSION_END_STAGE - SET_STRAND_MISSION_READY_TO_START_DOWNLOAD TRUE ")
		ENDIF
		
		IF MC_playerBD[iLocalPart].iRowanCSuperHighScore = 0
		AND CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
			//Populate early so it can be used by other players
			PRINTLN("[RCC MISSION] - PROCESS_MISSION_END_STAGE - Calling GET_MISSION_COMPLETION_HIGH_SCORE")
			MC_playerBD[iLocalPart].iRowanCSuperHighScore = GET_MISSION_COMPLETION_HIGH_SCORE()
		ENDIF
		
		IF NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE() OR IS_BIT_SET(MC_serverBD.iServerBitSet,SBBOOL_MISSION_OVER)
			g_bMissionOver = TRUE
			PRINTLN("[RCC MISSION] [MJM] PROCESS_MISSION_END_STAGE() - g_bMissionOver - SET")
		ELSE
			IF g_bMissionOver
				g_bMissionOver = FALSE
			ENDIF
		ENDIF	
		
		MP_FORCE_TERMINATE_INTERNET() 

		IF g_bIsRunningBackMission = TRUE
			IF NOT g_bTriggerCrossTheLineAudio
				//Team 0 is the defending team
				IF MC_playerBD[iPartToUse].iteam != 0
					IF HAS_TEAM_PASSED_MISSION(MC_playerBD[iPartToUse].iteam)
						PLAY_SOUND_FRONTEND(-1, "Cheers", "DLC_TG_Running_Back_Sounds" , FALSE)
						PRINTLN("[RCC MISSION] - PROCESS_MISSION_END_STAGE() - CROSS THE LINE AUDIO - Cheer")
					ENDIF
				ENDIF
				PLAY_SOUND_FRONTEND(-1, "Whistle", "DLC_TG_Running_Back_Sounds" , FALSE)
				PRINTLN("[RCC MISSION] - PROCESS_MISSION_END_STAGE() - CROSS THE LINE AUDIO - Whistle")
				g_bTriggerCrossTheLineAudio = TRUE
			ELSE
				PRINTLN("[RCC MISSION] - PROCESS_MISSION_END_STAGE() - We already triggered the cross the line audio")
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION] - PROCESS_MISSION_END_STAGE() - CROSS THE LINE AUDIO - g_bIsRunningBackMission = FALSE")
		ENDIF
		
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		AND GET_SEAT_PED_IS_IN(LocalPlayerPed) = VS_DRIVER
			IF NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(LocalPlayerPed, FALSE))
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRQUOTA(g_FMMC_STRUCT.iAdversaryModeType)
				OR GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(LocalPlayerPed, FALSE)) = DELUXO
					PRINTLN("[RCC MISSION] - PROCESS_MISSION_END_STAGE() - SET_VEHICLE_ENGINE_ON = FALSE (Not doing this as it's AirQuota/Deluxo.)") 	
				ELSE
					PRINTLN("[RCC MISSION] - PROCESS_MISSION_END_STAGE() - SET_VEHICLE_ENGINE_ON = FALSE")
					SET_VEHICLE_ENGINE_ON(GET_VEHICLE_PED_IS_IN(LocalPlayerPed, FALSE), FALSE, TRUE, TRUE)
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_UNLOCK)
			CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_UNLOCK)
			PRINTLN("[RCC MISSION] - PROCESS_MISSION_END_STAGE() - BIG_MESSAGE_UNLOCK") 
		ENDIF
		
		//Clearing up a screen effect
		IF DOES_PARTICLE_FX_LOOPED_EXIST(TLADptfxLensDirt)
			STOP_PARTICLE_FX_LOOPED(TLADptfxLensDirt)
		ENDIF
		
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TOUR_DE_FORCE (g_FMMC_STRUCT.iAdversaryModeType)
			IF MC_serverBD.iNumLocCreated > 0
			AND MC_serverBD.iNumLocCreated <= FMMC_MAX_GO_TO_LOCATIONS
				FOR i = 0 TO (MC_serverBD.iNumLocCreated-1)
					HIDE_LOCATES(i)
				ENDFOR
			ENDIF
		ENDIF

		//If the player is still alive and not spectating
		IF iSpectatorTarget = -1
			IF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciENABLE_SUMO_SOUNDS) OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_SUMO_RUN_SOUNDS))
			AND NOT IS_BIT_SET(iLocalBoolCheck15, LBOOL15_HAS_PLAYED_SUMO_END_SOUND)
				//If the player is on the winning team
				IF GET_TEAM_FINISH_POSITION(MC_playerBD[iLocalPart].iteam) = 1
				
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_SUMO_RUN_SOUNDS)
						PLAY_SOUND_FRONTEND(-1, "Round_End", "DLC_BTL_SM_Remix_Soundset", FALSE)
					ELSE
						PLAY_SOUND_FRONTEND(-1, "Round_End", "DLC_LOW2_Sumo_Soundset", FALSE)
					ENDIF
					
					SET_BIT(iLocalBoolCheck15, LBOOL15_HAS_PLAYED_SUMO_END_SOUND)
					PRINTLN("[RCC MISSION] - PROCESS_MISSION_END_STAGE() - SUMO AUDIO - Round End")
				ENDIF
			ENDIF
		ENDIF

		IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC)
		AND NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_KILLED_APT_COUNTDOWN_MUSIC)
		
			IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_COUNTDOWN_EARLY_MISSION_END)
				PRINTLN("[JJT] PROCESS_MISSION_END_STAGE - Killed Countdown music due to early end")
				
				IF IS_ARENA_WARS_JOB()
					TRIGGER_MUSIC_EVENT("AW_COUNTDOWN_30S_KILL")
				ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciUSE_FREEMODE_30S_COUNTDOWN)
					TRIGGER_MUSIC_EVENT("FM_COUNTDOWN_30S_KILL")
				ELSE
					TRIGGER_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
				ENDIF
			ENDIF
			
			SET_BIT(iLocalBoolCheck14, LBOOL14_HAS_KILLED_APT_COUNTDOWN_MUSIC)
			PRINTLN("[JJT] PROCESS_MISSION_END_STAGE - Triggering TRIGGER_MUSIC_EVENT('APT_FADE_IN_RADIO')")
			TRIGGER_MUSIC_EVENT("APT_FADE_IN_RADIO")
			
			STOP_ALL_POWER_PLAY_PICKUP_AUDIO()
		ENDIF
		
		IF IS_BIT_SET(iLocalBoolCheck15, LBOOL15_HAS_STARTED_VAL_COUNTDOWN_MUSIC)
		AND NOT IS_BIT_SET(iLocalBoolCheck15, LBOOL15_HAS_KILLED_VAL_COUNTDOWN_MUSIC)

			IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_COUNTDOWN_EARLY_MISSION_END)
				PRINTLN("[JJT] PROCESS_MISSION_END_STAGE - Killed Countdown music due to early end")
				TRIGGER_MUSIC_EVENT("VAL2_COUNTDOWN_30S_KILL")
			ENDIF
			
			SET_BIT(iLocalBoolCheck15, LBOOL15_HAS_KILLED_VAL_COUNTDOWN_MUSIC)
			PRINTLN("[JJT] PROCESS_MISSION_END_STAGE - Triggering TRIGGER_MUSIC_EVENT('VAL2_FADE_IN_RADIO')")
			TRIGGER_MUSIC_EVENT("VAL2_FADE_IN_RADIO")
		ENDIF
		
		IF IS_ARENA_WARS_JOB()
			TRIGGER_MUSIC_EVENT("AW_FADE_IN_RADIO")
		ENDIF
		
		IF NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_KILLED_APT_SUDDEN_DEATH_MUSIC)
		AND ( IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_SUDDEN_DEATH_MUSIC)
			  OR IS_BIT_SET(iLocalBoolCheck15, LBOOL15_HAS_STARTED_APT_SUDDEN_DEATH_END) )
			SET_BIT(iLocalBoolCheck14, LBOOL14_HAS_KILLED_APT_SUDDEN_DEATH_MUSIC)
			IF CONTENT_IS_USING_ARENA()
				TRIGGER_MUSIC_EVENT( "DLC_AWXM2018_SUDDEN_DEATH_MUSIC_KILL" )
				CPRINTLN(DEBUG_CONTROLLER, "[JJT] PROCESS_MISSION_END_STAGE - TRIGGER_MUSIC_EVENT('DLC_AWXM2018_SUDDEN_DEATH_MUSIC_KILL')")
			ELSE
				PRINTLN("[JJT] PROCESS_MISSION_END_STAGE - TRIGGER_MUSIC_EVENT('APT_SUDDEN_DEATH_MUSIC_KILL')")
				TRIGGER_MUSIC_EVENT("APT_SUDDEN_DEATH_MUSIC_KILL")
			ENDIF
			
			sudden_death_music_state = eSuddenDeathState_END
		ENDIF
		
		IF NOT IS_BIT_SET(iLocalBoolCheck20, LBOOL20_HAS_KILLED_IE_COUNTDOWN_MUSIC)
		AND IS_BIT_SET(iLocalBoolCheck20, LBOOL20_HAS_STARTED_IE_COUNTDOWN_MUSIC)

			SET_BIT(iLocalBoolCheck14, LBOOL14_HAS_KILLED_APT_SUDDEN_DEATH_MUSIC)
			SET_BIT(iLocalBoolCheck15, LBOOL15_HAS_KILLED_VAL_COUNTDOWN_MUSIC)
			SET_BIT(iLocalBoolCheck20, LBOOL20_HAS_KILLED_IE_COUNTDOWN_MUSIC)
			
			IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_COUNTDOWN_EARLY_MISSION_END)
				PRINTLN("[JJT] PROCESS_MISSION_END_STAGE - Killed Countdown music due to early end")
				TRIGGER_MUSIC_EVENT("IE_COUNTDOWN_30S_KILL")								
			ENDIF
			
			PRINTLN("[JJT] PROCESS_MISSION_END_STAGE - Triggering TRIGGER_MUSIC_EVENT('IE_FADE_IN_RADIO')")
			TRIGGER_MUSIC_EVENT("IE_FADE_IN_RADIO")
		ENDIF		
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciTURN_OFF_VEH_SPAWN)
			PRINTLN("[RCC MISSION] suppressing vehicle model FALSE SUPPRESS_ALL_HELIS")
			VALIDATE_NUMBER_MODELS_SUPRESSED(SUPPRESS_ALL_HELIS(FALSE)*-1)
		ENDIF
		
		SET_MISSION_OVER_AND_ON_LB_FOR_SCTV()
		
		DEAL_WITH_HEIST_SPECTATORS_QUICK_RESTART()

		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour,ciLOUD_PLANES)
			IF IS_AUDIO_SCENE_ACTIVE("Speed_Race_Airport_Scene")
				STOP_AUDIO_SCENE("Speed_Race_Airport_Scene")
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(iLocalBoolCheck11, LBOOL11_REGEN_PLAYER_HEADSHOTS)
			IF AM_I_ON_A_HEIST()
				IF NOT HAS_TEAM_PASSED_MISSION(MC_playerBD[iPartToUse].iteam)
					BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_PLAYER_CHANGED_APPEARANCE, ALL_PLAYERS())
					SET_BIT(iLocalBoolCheck11, LBOOL11_REGEN_PLAYER_HEADSHOTS)
				ENDIF
			ENDIF
		ENDIF
	
		IF IS_BIT_SET( iLocalBoolCheck11, LBOOL11_STOP_MISSION_FAILING_DUE_TO_EARLY_CELEBRATION )
		AND NOT IS_NET_PLAYER_OK( PLAYER_ID() )
		AND NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDidPlayerStartWinnerCelebrationScreenDead
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDidPlayerStartWinnerCelebrationScreenDead = TRUE
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Setting g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDidPlayerStartWinnerCelebrationScreenDead = TRUE" )
		ENDIF
		
		IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_RESPAWNING_DURING_FAIL)
			IF IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_OUT()
				IF IS_PLAYER_RESPAWNING_BEFORE_FADE_IN()
					IF NOT HAS_TEAM_PASSED_MISSION(MC_playerBD[iLocalPart].iteam)
						PRINTLN("[RCC MISSION] - IS_PLAYER_RESPAWNING_BEFORE_FADE_IN setting bit LBOOL10_RESPAWNING_DURING_FAIL: do fail celebration early ")
						g_TransitionSessionNonResetVars.sGlobalCelebrationData.bBlockSpawnFadeInForMissionFail = TRUE
						PRINTLN("[RCC MISSION] - [SPAWNING] - [NETCELEBRATION] - set flag g_TransitionSessionNonResetVars.sGlobalCelebrationData.bBlockSpawnFadeInForMissionFail")
						SET_BIT(iLocalBoolCheck10, LBOOL10_RESPAWNING_DURING_FAIL)
					ELSE
						PRINTLN("[RCC MISSION] - IS_PLAYER_RESPAWNING_BEFORE_FADE_IN HAS_TEAM_PASSED_MISSION = TRUE")
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION] - IS_PLAYER_RESPAWNING_BEFORE_FADE_IN = FALSE")
				ENDIF
			ENDIF
		ENDIF
		
		POPULATE_VEHICLE_FOR_LBD()
		
		IF iLocalPart!= -1
			IF NOT IS_BIT_SET(iLocalBoolCheck15, LBOOL15_ROUNDS_MISSION_END_SET_UP)
				IF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_MISSION_ENDING_BECAUSE_PLAYERS_LEFT)
				OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_TOO_FEW_PLAYERS_FOR_NEXT_ROUND)
				#IF IS_DEBUG_BUILD
				OR iPlayerPressedF != -1
				OR iPlayerPressedS != -1
				#ENDIF
					
					PRINTLN("[RCC MISSION] Clearing rounds data - SBBOOL2_MISSION_ENDING_BECAUSE_PLAYERS_LEFT = ",IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_MISSION_ENDING_BECAUSE_PLAYERS_LEFT),", iPlayerPressedF = ",iPlayerPressedF,", iPlayerPressedS = ",iPlayerPressedS)
					
					#IF IS_DEBUG_BUILD
					IF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_MISSION_ENDING_BECAUSE_PLAYERS_LEFT)
					#ENDIF
					
					IF g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds != 0
						IF NOT IS_BIT_SET(iLocalBoolCheck8, LBOOL8_HANDLE_PLAYLIST_POSITION_DATA_WRITE_CALLED)
							SET_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
							IF IS_PLAYER_ON_A_PLAYLIST(LocalPlayer)
								HANDLE_PLAYLIST_POSITION_DATA_WRITE()
							ENDIF
							SET_BIT(iLocalBoolCheck8, LBOOL8_HANDLE_PLAYLIST_POSITION_DATA_WRITE_CALLED)
						ENDIF
						RESET_ROUND_MISSION_DATA(DEFAULT, DEFAULT, FALSE)
					ENDIF
					
					#IF IS_DEBUG_BUILD
					ENDIF
					#ENDIF
					
					CLEAR_USE_BACKED_UP_MENU_SELECTION_ON_RESTART()
				ENDIF
				PRINTLN("[RCC MISSION] SET_BIT(iLocalBoolCheck15, LBOOL15_ROUNDS_MISSION_END_SET_UP)")
				SET_BIT(iLocalBoolCheck15, LBOOL15_ROUNDS_MISSION_END_SET_UP)
					
				PRINTLN("[LM][SPEC_SPEC][JIP] - BoxCheck: ", SHOULD_LOCAL_PLAYER_STAY_IN_ARENA_BOX_BETWEEN_ROUNDS(), " ArenaCheck: ", CONTENT_IS_USING_ARENA(),
									" SpecCheck: ", DID_I_JOIN_MISSION_AS_SPECTATOR(), " RestartCheck: ", IS_THIS_A_ROUNDS_MISSION(), " end due to play leave: ", 
									IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_MISSION_ENDING_BECAUSE_PLAYERS_LEFT), " too few players: ", IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_TOO_FEW_PLAYERS_FOR_NEXT_ROUND))
				
				IF SHOULD_LOCAL_PLAYER_STAY_IN_ARENA_BOX_BETWEEN_ROUNDS()
				AND CONTENT_IS_USING_ARENA()
				AND DID_I_JOIN_MISSION_AS_SPECTATOR()
				AND IS_THIS_A_ROUNDS_MISSION()
				AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_MISSION_ENDING_BECAUSE_PLAYERS_LEFT)
				AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_TOO_FEW_PLAYERS_FOR_NEXT_ROUND)
					PRINTLN("[LM][SPEC_SPEC][JIP] - Setting that we want to stay as a special spectator so we can double check.")
					SET_JIP_SHOW_STAY_AS_SPECTATOR_SCREEN_BETWEEN_ROUNDS(TRUE)
				ENDIF
			ENDIF
			
			IF SET_UP_ROUNDS_DETAILS_AT_END_OF_MISSION(MC_serverBD.iWinningTeam, MC_playerBD[iLocalPart].iteam)
				PRINTLN("[RCC MISSION] SET_BIT(iLocalBoolCheck15, LBOOL15_CALL_SWAP_TEAM_LOGIC)")
				SET_BIT(iLocalBoolCheck15, LBOOL15_CALL_SWAP_TEAM_LOGIC)
			ENDIF
					
			IF IS_BIT_SET(iLocalBoolCheck, ciPOPULATE_RESULTS)
			AND IS_BIT_SET(iLocalBoolCheck15, LBOOL15_CALL_SWAP_TEAM_LOGIC)
				CLEAR_BIT(iLocalBoolCheck15, LBOOL15_CALL_SWAP_TEAM_LOGIC)
				PRINTLN("[RCC MISSION] CLEAR_BIT(iLocalBoolCheck15, LBOOL15_CALL_SWAP_TEAM_LOGIC)")
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_BVS1(g_FMMC_STRUCT.iRootContentIDHash)
					PRINTLN("[SST] * - IS_THIS_ROCKSTAR_MISSION_NEW_VS_BVS1")
					SET_VARS_AND_SWAP_TO_SMALLEST_BEAST_TEAM(MC_Playerbd[iPartToUse].iNumPlayerKills > 0)
				ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciALWAYS_SWAP_SMALL_TEAM)
					PRINTLN("[SST] * - IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciALWAYS_SWAP_SMALL_TEAM)")
					IF SHOULD_I_JOIN_SMALLEST_TEAM(iTeamLBPositionStore, TRUE)
						PRINTLN("[SST] * - SHOULD_I_JOIN_SMALLEST_TEAM")
						SET_VARS_AND_END_OF_ALWAYS_SWAP_SMALLEST_TEAM_MISSION(TRUE)
					ELSE
						SET_VARS_AND_END_OF_ALWAYS_SWAP_SMALLEST_TEAM_MISSION(FALSE)
					ENDIF
				ELSE
					IF SHOULD_I_JOIN_SMALLEST_TEAM(iLBPositionStore)
						PRINTLN("[SST] * - SHOULD_I_JOIN_SMALLEST_TEAM")
						SET_VARS_AND_END_OF_SWAP_TO_SMALLEST_TEAM_MISSION(TRUE)
					ELSE
						SET_VARS_AND_END_OF_SWAP_TO_SMALLEST_TEAM_MISSION(FALSE)
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_SAVED_ROUNDS_LBD_DATAa)
				// Write kills etc. rewards are written in (POPULATE_RESULTS)
				PRINTLN("[RCC MISSION] [TS] [MSROUND] - WRITE_TO_ROUNDS_MISSION_END_LB_DATA 1a) ")
				INT iScore, iKills, iDeaths
				IF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_SiegeMentality)
					iScore = MC_serverBD.iKillScore[iLocalPart]
				ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciLBD_SHOW_SLIPSTREAM_TIME)
					IF MC_playerBD[iLocalPart].iFastestLapOfMission < GlobalplayerBD_FM_2[iLocalPart].sJobRoundData.iScore
					OR GlobalplayerBD_FM_2[iLocalPart].sJobRoundData.iScore = 0
						iScore = MC_playerBD[iLocalPart].iFastestLapOfMission
					ENDIF
				ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
					iScore = ROUND((TO_FLOAT(MC_playerBD[iLocalPart].iDamageToJuggernaut) / TO_FLOAT(GET_TOTAL_JUGGERNAUT_DAMAGE_FOR_TEAM(MC_playerBD[iLocalPart].iTeam))) * 100)
				ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_STOCKPILE(g_FMMC_STRUCT.iAdversaryModeType)
					iScore = MC_PlayerBD[iLocalPart].iPackagesAtHolding
				ELIF USING_SHOWDOWN_POINTS()
					PRINTLN("[SHOWDOWN] Setting leaderboard points to fShowdown_PointsStolenTotal which is ", MC_playerBD_1[iLocalPart].fShowdown_PointsStolenTotal)
					iScore = ROUND(MC_playerBD_1[iLocalPart].fShowdown_PointsStolenTotal)
				ELSE
					iScore = MC_serverBD.iPlayerScore[iLocalPart]
				ENDIF
				
				INT iTime = 0
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciSUMO_ZONE_CAPTURE_BAR_WINNER)
				OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
					iTime = ROUND(MC_playerBD[iLocalPart].fCaptureTime * 1000)
					PRINTLN("4251852, iTime = ", iTime)
				ENDIF
				
				//bugstar:3026870 - Kills on Leaderboard taking suicides into account
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciSHOW_ONLY_KILLS_ON_LEADERBOARD)
					iKills = MC_playerBD[iPartToUse].iPureKills
				ELSE
					iKills = MC_PlayerBD[iLocalPart].iNumPlayerKills + MC_PlayerBD[iLocalPart].iNumPedKills
				ENDIF
								
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_STOCKPILE(g_FMMC_STRUCT.iAdversaryModeType)
					iKills = MC_PlayerBD[iLocalPart].iNumberOfDeliveries
				ENDIF
								
				IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_STOCKPILE(g_FMMC_STRUCT.iAdversaryModeType)
					iDeaths = MC_PlayerBD[iLocalPart].iNumPlayerDeaths
				ELSE
					iDeaths = MC_PlayerBD[iLocalPart].iNumPlayerKills 
				ENDIF				
					
				IF ROUND(MC_playerBD_1[iLocalPart].fRunnerBestTime*1000) > 0
				AND (ROUND(MC_playerBD_1[iLocalPart].fRunnerBestTime*1000) < GlobalplayerBD_FM_2[iLocalPart].sJobRoundData.iKills OR GlobalplayerBD_FM_2[iLocalPart].sJobRoundData.iKills = 0)
					PRINTLN("[RUNNING_BACK] Setting leaderBoard[iLocalPart].iKills to ", ROUND(MC_playerBD_1[iLocalPart].fRunnerBestTime*1000))
					iKills = ROUND(MC_playerBD_1[iLocalPart].fRunnerBestTime*1000)
				ENDIF
				
				WRITE_TO_ROUNDS_MISSION_END_LB_DATA(iScore,
										iKills,
										iDeaths,	
										0,
										0,
										MC_PlayerBD[iLocalPart].iAssists,
										iTime,
										MC_playerBD_1[iLocalPart].iApEarned)
				SET_BIT(iLocalBoolCheck6, LBOOL6_SAVED_ROUNDS_LBD_DATAa)
			ENDIF
			
			//Write to the playlist LB
			IF IS_PLAYER_ON_A_PLAYLIST(LocalPlayer)
				IF NOT IS_BIT_SET(iLocalBoolCheck8, LBOOL8_HANDLE_PLAYLIST_POSITION_DATA_WRITE_CALLED)
				AND SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
					HANDLE_PLAYLIST_POSITION_DATA_WRITE()
					SET_BIT(iLocalBoolCheck8, LBOOL8_HANDLE_PLAYLIST_POSITION_DATA_WRITE_CALLED)
				ENDIF
			ENDIF
			
			//If it's a rounds mission then call DEAL_WITH_FM_MATCH_END
			IF iRoundsLBPosition != -1
			AND (AM_I_AT_END_OF_ROUNDS_MISSION()
			OR SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD())
				PRINTLN("[RCC MISSION] [TEL] * iRoundsLBPosition = ", iRoundsLBPosition)
				DEAL_WITH_FM_MATCH_END(FMMC_TYPE_MISSION,																				//Matchtype									
										g_MissionControllerserverBD_LB.iMatchTimeID,													//posixTime
										0,																								//Variation
										iMissionResult,																					//PassFail
										g_MissionControllerserverBD_LB.sleaderboard[iRoundsLBPosition].iRank,							//LDBD Pos
										g_MissionControllerserverBD_LB.sleaderboard[iRoundsLBPosition].iKills,							//Kills
										g_MissionControllerserverBD_LB.sleaderboard[iRoundsLBPosition].iDeaths,							//Deaths
										iRoundsSuicides,																				//Suicides
										iHighKillStreak,																				//Highest killstreak
										g_MissionControllerserverBD_LB.sleaderboard[iRoundsLBPosition].iTeam,							//Team
										g_MissionControllerserverBD_LB.sleaderboard[iRoundsLBPosition].iHeadshots, 						//Headshots
										g_mnMyRaceModel,																				//Vehicle ID
										DEFAULT,																						//Waves survived
										IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_MISSION_ENDING_BECAUSE_PLAYERS_LEFT),			//Forced round end
										MC_serverBD.iDifficulty,																		//Difficulty
										GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( MC_serverBD.tdMissionLengthTimer ),					//Time
										DEFAULT)																						//DNF
				iRoundsLBPosition = -1																									
			ENDIF 
			
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[TS] [MSROUND] - SET_UP_ROUNDS_DETAILS_AT_END_OF_MISSION - not being called from the mission controller, iLocalPart is -1")
		#ENDIF	
		ENDIF
		
		IF NOT IS_HEIST_SPECTATE_DISABLED()
			PRINTLN("[RCC MISSION] PIM - HEIST SPECTATE - DISABLE_HEIST_SPECTATE - C")
			DISABLE_HEIST_SPECTATE(TRUE)
		ENDIF

		IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_PROCESSED_HEIST_END_OF_MISSION)
			PRINTLN("[2847407] IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_PROCESSED_HEIST_END_OF_MISSION)")
			//If we've not moved on to the leaderboard (I'm hijacking LBOOL10_PROCESSED_HEIST_END_OF_MISSION here so this only gets called once)
			IF NOT MC_serverBD.sServerFMMC_EOM.bAllowedToVote
				PRINTLN("[2847407] IF NOT MC_serverBD.sServerFMMC_EOM.bAllowedToVote")
				CHECK_LTS_OVER_DUE_TO_TEAM_LEAVING()
			ENDIF
			
			PROCESS_HEIST_END_OF_MISSION()
			SET_BIT(iLocalBoolCheck10, LBOOL10_PROCESSED_HEIST_END_OF_MISSION)
		ENDIF
		
		
		IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DCTL(g_FMMC_STRUCT.iAdversaryModeType)
				PRINTLN("[LH][PRON] Checking for Shotaro unlock")
				IF NOT IS_MP_VEHICLE_UNLOCKED(PLAYER_VEHICLE_SHOTARO)	
					PRINTLN("[LH][PRON] Unlocking Shotaro")
					SET_MP_VEHICLE_UNLOCKED(PLAYER_VEHICLE_SHOTARO, TRUE, FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	MAINTAIN_CELEBRATION_PRE_LOAD(sCelebrationData)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFIve, ciBEAST_SFX)
		IF HAS_CELEBRATION_SCREEN_LOADED(sCelebrationData)
		AND NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_BEAST_CUSTOM_CELEB_SOUNDS_ON)
			PRINTLN("[JS] [BEASTMODE] Setting custom celebration screen sounds")
			
			STRING sSoundSet = GET_BEAST_MODE_SOUNDSET()
			
			BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "SET_CUSTOM_SOUND")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING("Blade_Appear")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(sSoundSet)
			END_SCALEFORM_MOVIE_METHOD()
			
			BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "SET_CUSTOM_SOUND")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(7)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING("Blade_Beasts_Fail")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(sSoundSet)
			END_SCALEFORM_MOVIE_METHOD()
			
			BEGIN_SCALEFORM_MOVIE_METHOD(sCelebrationData.sfCelebration, "SET_CUSTOM_SOUND")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(8)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING("Blade_Beasts_Win")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(sSoundSet)
			END_SCALEFORM_MOVIE_METHOD()

			SET_BIT(iLocalBoolCheck14, LBOOL14_BEAST_CUSTOM_CELEB_SOUNDS_ON)
		ENDIF
	ENDIF
	
	IF iSpectatorTarget != -1
	AND NOT IS_PLAYER_SCTV(LocalPlayer)
	AND NOT USING_HEIST_SPECTATE()
		EXIT
	ENDIF
	
	// Don't show HUD if the celebration screen has been requested/is being shown
	IF g_bCelebrationScreenIsActive
		IF NOT (IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUGBY(g_FMMC_STRUCT.iRootContentIdHash)
		AND IS_BIT_SET(iLocalBoolCheck16, LBOOL16_SUDDEN_DEATH_REASON_SHARD_SENT))
			IF NOT HAS_NET_TIMER_STARTED(tdresultdelay) OR (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdresultdelay) > iEndDelay)
				DISABLE_HUD()
				HIDE_SPECTATOR_HUD( TRUE )	
			ENDIF
		ENDIF
	ENDIF

	IF NOT IS_BIT_SET(iLocalBoolCheck3,LBOOL3_SHOWN_RESULTS)
		IF ((HAS_TEAM_FINISHED(MC_playerBD[iPartToUse].iteam) AND NOT HAVE_ALL_TEAMS_FINISHED() AND NOT AM_I_ON_A_HEIST() )
		OR (HAS_LOCAL_PLAYER_FAILED() AND NOT HAVE_ALL_TEAMS_FINISHED() AND NOT AM_I_ON_A_HEIST())
		OR GET_MC_SERVER_GAME_STATE() = GAME_STATE_END)
		AND NOT bDelayEnd
		AND (NOT IS_ACTIVE_ROAMING_SPECTATOR_MODE_ENABLED() OR (IS_ACTIVE_ROAMING_SPECTATOR_MODE_ENABLED() AND (NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE() OR IS_BIT_SET(MC_serverBD.iServerBitSet,SBBOOL_MISSION_OVER))))
		
			IF NOT g_bEndOfMissionCleanUpHUD
				IF NOT (IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUGBY(g_FMMC_STRUCT.iRootContentIdHash)
				AND IS_BIT_SET(iLocalBoolCheck16, LBOOL16_SUDDEN_DEATH_REASON_SHARD_SENT))
					IF NOT HAS_NET_TIMER_STARTED(tdresultdelay) OR (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdresultdelay) > iEndDelay)
					g_bEndOfMissionCleanUpHUD = TRUE
					ENDIF
				ENDIF
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Disabling HUD cos PROCESS_MISSION_END_STAGE" )
			ENDIF
			
			IF USING_HEIST_SPECTATE()
				CLEANUP_HEIST_SPECTATE()
				IF NOT AM_I_ON_A_HEIST()
				AND NOT IS_LOADED_MISSION_TYPE_FLOW_MISSION()
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] CLEANUP_HEIST_SPECTATE_FOR_NON_HEISTS" )
					CLEANUP_HEIST_SPECTATE_FOR_NON_HEISTS()
				ENDIF
			ENDIF
			
			IF g_bMissionEnding
			AND NOT GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()
				PRINTLN("[RCC MISSION] PROCESS_MISSION_END_STAGE - CLEANING UP BirdsEyeCam")
				bUsingBirdsEyeCam = FALSE
				IF DOES_CAM_EXIST(g_birdsEyeCam)
					IF IS_CAM_ACTIVE(g_birdsEyeCam)
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
						SET_CAM_CONTROLS_MINI_MAP_HEADING(g_birdsEyeCam, FALSE)
					ENDIF
				ENDIF
			ENDIF
			
			MAKE_PLAYER_INVINCIBLE_ONCE_MISSION_IS_OVER()
			
			STOP_PLAYER_LEAVING_VEHICLE_THIS_FRAME()
			
			IF IS_SOUND_ID_VALID(iRugbyCarryingBallSoundID)
				IF NOT HAS_SOUND_FINISHED(iRugbyCarryingBallSoundID)
					STOP_SOUND(iRugbyCarryingBallSoundID)
				ENDIF
			ENDIF
	
			SCRIPT_RELEASE_SOUND_ID(iRugbyCarryingBallSoundID)
			
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
				IF IS_TEMP_PASSIVE_MODE_ENABLED()
					CLEANUP_TEMP_PASSIVE_MODE()
				ENDIF
			ENDIF
			
			IF MC_serverBD_4.iCurrentHighestPriority[ MC_playerBD[iPartToUse].iteam ]	> FMMC_MAX_RULES
			AND NOT IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING()
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
					CLEAR_HELP()
				ENDIF
				HUD_SUPPRESS_WEAPON_WHEEL_RESULTS_THIS_FRAME()
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				DISABLE_SELECTOR_THIS_FRAME() 
				CLEAR_INVENTORY_BOX_CONTENT()
				DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_ALL_OVERHEADS)
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				HIDE_SPECTATOR_HUD( TRUE )
				
				IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
					IF SHOULD_TURN_OFF_CONTROL_AT_END_OF_MISSION()
						NET_SET_PLAYER_CONTROL( LocalPlayer, FALSE, NSPC_PREVENT_FROZEN_CHANGES | NSPC_PREVENT_VISIBILITY_CHANGES | NSPC_PREVENT_COLLISION_CHANGES )
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_RUGBY_SOUNDS)
				IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_HAS_PLAYED_RUGBY_END_SOUND)
					SET_BIT(iLocalBoolCheck16, LBOOL16_HAS_PLAYED_RUGBY_END_SOUND)
					PLAY_SOUND_FRONTEND(-1, "Match_End", "DLC_Low2_Ibi_Sounds", FALSE)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET( iLocalBoolCheck7, LBOOL7_PRISON_BREAK_CARGO_SCENE_TRIGGERED )
				RENDER_SCRIPT_CAMS( FALSE, FALSE )
				DESTROY_CAM( cutscenecam )
				CLEAR_BIT( iLocalBoolCheck7, LBOOL7_PRISON_BREAK_CARGO_SCENE_TRIGGERED )
			ENDIF
											
			g_bMissionRemovedWanted = TRUE
			
			IF IS_PAUSE_MENU_ACTIVE()
				PRINTLN("[RCC MISSION] PROCESS_MISSION_END_STAGE - SET_FRONTEND_ACTIVE(FALSE) - 1")				
				SET_FRONTEND_ACTIVE(FALSE)				
				PRINTLN("[RCC MISSION] PROCESS_MISSION_END_STAGE - SET_FRONTEND_ACTIVE(FALSE) - 2")
			ENDIF
			
			IF IS_SOCIAL_CLUB_ACTIVE()
				IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bClosedScPage)
					CLOSE_SOCIAL_CLUB_MENU()
					PRINTLN("[RCC MISSION] PROCESS_MISSION_END_STAGE - IS_SOCIAL_CLUB_ACTIVE - CLOSE_SOCIAL_CLUB_MENU")				
					SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bClosedScPage)
				ENDIF
				PRINTLN("[RCC MISSION] PROCESS_MISSION_END_STAGE - IS_SOCIAL_CLUB_ACTIVE")				
			ENDIF
			
			IF NETWORK_IS_GAME_IN_PROGRESS()
			AND NETWORK_IS_HOST_OF_THIS_SCRIPT()
				IF MC_serverBD_1.iPlayerForHackingMG != -1
					MC_serverBD_1.iPlayerForHackingMG = -1
					PRINTLN("[RCC MISSION] [MJM] PROCESS_MISSION_END_STAGE() - MC_serverBD_1.iPlayerForHackingMG = -1")
				ENDIF
			ENDIF
			
			IF IS_CIRCUIT_HACKING_MINIGAME_RUNNING(hackingMinigameData)
				CLEANUP_AND_QUIT_CIRCUIT_HACKING_MINIGAME(hackingMinigameData, IS_BIT_SET(MC_serverBD_1.iServerHackMGBitset,SBBOOL_HACK_DO_HARD))
				IF bLocalPlayerPedOk
					SET_PED_CONFIG_FLAG(LocalPlayerPed,PCF_PreventAutoShuffleToDriversSeat,FALSE)
				ENDIF
				PRINTLN("[RCC MISSION] [MJM] PROCESS_MISSION_END_STAGE() - CLEANUP_AND_QUIT_CIRCUIT_HACKING_MINIGAME")
			ENDIF
			
			// Clear up manual respawn
			IF (manualRespawnState != eManualRespawnState_RESPAWNING)
				IF manualRespawnState != eManualRespawnState_OKAY_TO_SPAWN
				OR manualRespawnState != eManualRespawnState_WAITING_AFTER_RESPAWN
					CLEAN_UP_MANUAL_RESPAWN()
				ENDIF
			ENDIF
			
			IF g_bVSMission
				IF IS_PLAYER_SCTV(LocalPlayer)
					SET_SCTV_TICKER_SCORE_OFF()
				ENDIF
			ENDIF
			
			SET_MP_DECORATOR_BIT(LocalPlayer, MP_DECORATOR_BS_INVALID_TO_SPECTATE)
			
			SET_GLOBAL_CLIENT_FINISHED_JOB(TRUE) 
			
			IF IS_BIT_SET(iLocalBoolCheck4, LBOOL4_SCRIPTED_CUT_NEEDS_CLEANED_UP)
				//Clear up any scripted cutscene!
				CLEAR_BIT( MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYING_CUTSCENE )
				IF g_iFMMCScriptedCutscenePlaying!= -1
					SET_BIT(MC_playerBD[iLocalPart].iCutsceneFinishedBitset, g_iFMMCScriptedCutscenePlaying)
				ENDIF
				SET_BIT( MC_playerBD[ iLocalPart].iClientBitSet2, PBBOOL2_FINISHED_CUTSCENE )
				//Stop any active load scenes
				IF IS_NEW_LOAD_SCENE_ACTIVE()
					NEW_LOAD_SCENE_STOP()
				ENDIF
				CLEANUP_SCRIPTED_CUTSCENE(TRUE)
			ENDIF
			
			SET_DAILY_OBJECTIVE_TICKERS_ENABLED_ON_MISSION(FALSE)
			
			IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_BVS1(g_FMMC_STRUCT.iRootContentIDHash)
			AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(g_FMMC_STRUCT.iAdversaryModeType)
			AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()	//Fix for bug 3470340 - mission controller and spectator cam fighting over nightvision
				ENABLE_NIGHTVISION(VISUALAID_OFF,VISUALAID_SOUND_OFF)
			ENDIF
			
			CLEANUP_PLAYER_VISUAL_AIDS()
			IF Is_Player_Currently_On_MP_Contact_Mission(PLAYER_ID())
				LEGACY_CLEANUP_WORLD_PROPS(sRuntimeWorldPropData)
			ENDIF
			
			IF IS_SOUND_ID_VALID(iBoundsTimerSound)
			AND NOT HAS_SOUND_FINISHED(iBoundsTimerSound)
				STOP_SOUND(iBoundsTimerSound)
			ENDIF
			
			IF (NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE() AND IS_ACTIVE_ROAMING_SPECTATOR_MODE_ENABLED())
			OR IS_BIT_SET(MC_serverBD.iServerBitSet,SBBOOL_MISSION_OVER)
				g_bMissionEnding = TRUE
				SET_MP_DECORATOR_BIT(PLAYER_ID(), MP_DECORATOR_BS_MISSION_ENDING)
				PRINTLN("[RCC MISSION] [MJM] PROCESS_MISSION_END_STAGE() - MP_DECORATOR_BS_MISSION_ENDING - g_bMissionEnding - SET")
			ELSE
				IF g_bMissionEnding
					g_bMissionEnding = FALSE
				ENDIF
			ENDIF	
			
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_POWERPLAY_PICKUP_SOUNDS)
			AND NOT IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING()	
				PRINTLN("[RCC MISSION] Calling KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE 1")
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			ELSE
				PRINTLN("[RCC MISSION] Call to KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE blocked")
			ENDIF
			
			IF IS_SKYSWOOP_IN_SKY()
				IF NOT IS_BIT_SET(iLocalBoolCheck2,LBOOL2_FLUSH_FEED)
					THEFEED_FLUSH_QUEUE()
					 g_b_ReapplyStickySaveFailedFeed = FALSE
					SET_BIT(iLocalBoolCheck2,LBOOL2_FLUSH_FEED)
				ENDIF
				THEFEED_HIDE_THIS_FRAME() 
			ENDIF
			
			IF HAS_NET_TIMER_STARTED(MC_Playerbd[iPartToUse].tdMissionTime)
				MC_Playerbd[iPartToUse].iMissionEndTime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_Playerbd[iPartToUse].tdMissionTime)
				PRINTLN("[RCC MISSION] Setting end time for part: ",iPartToUse," to time: ",MC_Playerbd[iPartToUse].iMissionEndTime)
				RESET_NET_TIMER(MC_Playerbd[iPartToUse].tdMissionTime)
			ENDIF
			
			IF ANIMPOSTFX_IS_RUNNING("CrossLine")
				PRINTLN("[MMacK][LocVFX] End of Mission")
				ANIMPOSTFX_STOP("CrossLine")
				ANIMPOSTFX_PLAY("CrossLineOut", 0, FALSE)
			ENDIF
			
			IF NOT IS_BIT_SET(iLocalBoolCheck7, LBOOL7_SET_END_MISSION_WEAPON)
				WEAPON_TYPE wtWeapon
				GET_CURRENT_PED_WEAPON(LocalPlayerPed, wtWeapon)
				SET_PLAYER_CURRENT_HELD_WEAPON(wtWeapon)
				
				#IF IS_DEBUG_BUILD
					PRINTLN("[RCC MISSION] [dsw] PROCESS_MISSION_END_STAGE - store weapon = ", GET_WEAPON_NAME(INT_TO_ENUM(WEAPON_TYPE, wtWeapon)))
				#ENDIF
						
				SET_BIT(iLocalBoolCheck7, LBOOL7_SET_END_MISSION_WEAPON)
			ENDIF
			
			IF NOT HAS_NET_TIMER_STARTED(tdresultdelay)
				
				SEND_EVENT_IF_PLAYER_COMPLETES_JOB_IN_UNDER_TEN_SECONDS(MC_serverBD.tdMissionLengthTimer, IS_PLAYER_SPECTATOR_ONLY(PLAYER_ID()), IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bQuitJob))

				CALCULATE_PLAYER_ACCURACY()
				
				START_NET_TIMER(tdresultdelay)
				
				REMOVE_REQUESTED_MC_ASSETS()

				SET_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_FINISHED)
				PRINTLN("[RCC MISSION] PROCESS_MISSION_END_STAGE, PBBOOL_OBJECTIVE_BLOCKER")
				SET_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_OBJECTIVE_BLOCKER)
				
				IF IS_PAUSE_MENU_ACTIVE()
					PRINTLN("[RCC MISSION] PROCESS_MISSION_END_STAGE - SET_FRONTEND_ACTIVE(FALSE) - 3")				
					SET_FRONTEND_ACTIVE(FALSE)
				ENDIF
				
				CLEANUP_DPAD_LBD(scaleformDpadMovie)
				SELECT_ENDING_TYPE()
				
				SAVE_PLAYER_END_JOB_POSITION_FOR_POST_MISSION_CLEANUP()
				IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
					SET_PERSONAL_VEHICLE_SPAWN_OVERRIDE_VECTOR(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE))
				ENDIF
				
				IF GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
					IF NOT HAS_TEAM_PASSED_MISSION(MC_playerBD[iPartToUse].iteam)
						g_bFailedMission = TRUE
						IF IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_InitDone)
							SET_BIT(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_BailFromInvite)
							PRINTLN("[RCC MISSION] - biITA_BailFromInvite enabled mission failed abort invite to apartment")
						ENDIF
						PRINTLN("[RCC MISSION] - g_bFailedMission = TRUE")
					ENDIF
				ENDIF
				
				IF NOT g_bFailedMission
					
					CACHE_VEHICLE_SEAT_FOR_CONTINUITY()
				
					IF MC_serverBD.iNextMission < FMMC_MAX_STRAND_MISSIONS
					AND MC_serverBD.iNextMission >= 0
					AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl23NextContentID[MC_serverBD.iNextMission])
						//Strand mission is going to be initialised soon
						IF g_FMMC_STRUCT.eBranchingTransitionType[MC_serverBD.iNextMission] = FMMC_BRANCHING_MISSION_TRANSITION_TYPE_FADEOUT
							PRINTLN("[RCC MISSION][MSRAND] - PROCESS_MISSION_END_STAGE - Fading out")
							DO_SCREEN_FADE_OUT(1000)
							IF NOT IS_PED_INJURED(localPlayerPed)
							AND GET_INTERIOR_FROM_ENTITY(LocalPlayerPed) = NULL
							AND NOT IS_ENTITY_IN_WATER(localPlayerPed)
								SetPlayerOnGroundProperly(FALSE, TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				TRIGGER_CELEBRATION_PRE_LOAD(sCelebrationData, SHOULD_MISSION_CONTROLLER_CELEBRATION_PRE_LOAD_FX_BE_TRIGGERED())
				
				IF CONTENT_IS_USING_ARENA()			
					IF GET_PLAYER_INDEX() = MC_serverBD_3.iWinningPlayerForRoundsId
					OR GET_TEAM_FINISH_POSITION(MC_playerBD[iLocalPart].iteam) = 1
						PRINTLN("[MJL] This player is the winner, or is on the winning team with PlayerIndex : ", NATIVE_TO_INT(GET_PLAYER_INDEX()))
						PLAY_SOUND_FRONTEND(-1, "Finish_Win", "DLC_AW_Frontend_Sounds")
					ELSE 
						PRINTLN("[MJL] This player is NOT the winner or NOT on the winning team with PlayerIndex : ", NATIVE_TO_INT(GET_PLAYER_INDEX()))
						PLAY_SOUND_FRONTEND(-1, "Finish_Default", "DLC_AW_Frontend_Sounds")
					ENDIF
				ENDIF
				
				IF g_bFailedMission
					IF g_HeistApartmentDropoffPanStarted
					OR g_HeistGarageDropoffPanStarted
						IF IS_SCREEN_FADED_OUT()
							DO_SCREEN_FADE_IN( 500 )
						ENDIF
					ENDIF
				ENDIF
				
				IF bPlayerToUseOK
				AND NOT IS_BIT_SET(iLocalBoolCheck3,LBOOL3_LATE_SPECTATE)
				AND NOT IS_BIT_SET(iLocalBoolCheck3,LBOOL3_WAIT_END_SPECTATE)
					g_bMissionRemovedWanted = TRUE
					IF NOT IS_PLAYER_SCTV(LocalPlayer)
						SET_EVERYONE_IGNORE_PLAYER(LocalPlayer,TRUE)
						IF NOT IS_THIS_IS_A_STRAND_MISSION()
						OR (IS_THIS_IS_A_STRAND_MISSION() AND IS_BIT_SET( iLocalBoolCheck5, LBOOL5_SET_UP_STRAND_DETAILS_AT_END_OF_MISSION) AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED())
							SET_PLAYER_WANTED_LEVEL(LocalPlayer,0)
							SET_PLAYER_WANTED_LEVEL_NOW(LocalPlayer)
						ENDIF
						IF PLAYER_IS_THE_DRIVER_IN_A_CAR()
							VEHICLE_INDEX PlayerVehicle 
							PlayerVehicle = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
							IF NETWORK_HAS_CONTROL_OF_ENTITY(PlayerVehicle)
								SET_VEHICLE_STOP_INSTANTLY_WHEN_PLAYER_INACTIVE(PlayerVehicle, FALSE)
							ENDIF
						ENDIF
						
						IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
							SET_ENTITY_ALPHA(LocalPlayerPed, 255, FALSE)
						ENDIF
						
						IF SHOULD_TURN_OFF_CONTROL_AT_END_OF_MISSION()
						AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
							NET_SET_PLAYER_CONTROL( LocalPlayer, FALSE, NSPC_PREVENT_FROZEN_CHANGES | NSPC_PREVENT_VISIBILITY_CHANGES | NSPC_PREVENT_COLLISION_CHANGES )
						ENDIF
						
						SET_PED_UNABLE_TO_DROWN( LocalPlayerPed )
					ENDIF
					
					// Gives the player an AI task to try and avoid obstacles whilst they have no control
					IF MC_ServerBD.iEndCutscene <> ciMISSION_CUTSCENE_HUM_EMP_EXT // url:bugstar:2188907 - [Humane Labs - EMP] Plane looked like it was taking off when transitioning into the end of mission cutscene.
						IF SHOULD_TURN_OFF_CONTROL_AT_END_OF_MISSION()
							IF IS_PLAYER_IN_A_FLYING_VEHICLE()
							 	IF IS_ENTITY_IN_AIR( GET_VEHICLE_PED_IS_IN(LocalPlayerPed ))
									GIVE_PLAYER_AI_FLY_TASK()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDELAY_MISSION_END) 
					AND MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_DEFAULT
					AND NOT Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)
					AND NOT AM_I_ON_A_HEIST()
						IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
						AND IS_ENTITY_ALIVE(LocalPlayerPed)
							TASK_WANDER_STANDARD(LocalPlayerPed)
						ENDIF
					ENDIF
				ENDIF
			ELSE // Otherwise the end timer has started
				IF IS_BIT_SET(iLocalBoolCheck3,LBOOL3_LATE_SPECTATE)	//jip, want to get out straight away
					iEndDelay = 0
				ELSE
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDELAY_MISSION_END) 
						IF HAS_TEAM_PASSED_MISSION(MC_playerBD[iPartToUse].iteam)
							iEndDelay = 1000
						ENDIF
					ELSE
						IF Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)
							iEndDelay = 0
						ELIF IS_BIT_SET(iLocalBoolCheck2, LBOOL2_USE_CRATE_DELIVERY_DELAY)	//delay when detaching crates from cargobob, TODO B*1622362
							iEndDelay = 0
						ELIF MPGlobalsAmbience.R2Pdata.bOnRaceToPoint = TRUE	//DELAY IF RACE OT POINT IS ACTIVE
							iEndDelay = R2P_MISSION_END_DELAY
							MPGlobalsAmbience.R2Pdata.bForceEnd = TRUE
							PRINTLN("[RCC MISSION]     ---------->     RACE TO POINT - bForceEnd - MISSION")
						ELIF (IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUGBY(g_FMMC_STRUCT.iRootContentIdHash)
						AND IS_BIT_SET(iLocalBoolCheck16, LBOOL16_SUDDEN_DEATH_REASON_SHARD_SENT))
						OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_TRADING_PLACES_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
							iEndDelay = 3000
						ELIF IS_THIS_ROCKSTAR_MISSION_WVM_TRAILERLARGE(g_FMMC_STRUCT.iRootContentIdHash)
						AND NOT HAS_TEAM_PASSED_MISSION(MC_playerBD[iPartToUse].iteam)
							iEndDelay = 4000 //Sync shard with kicking players out of the hauler
						ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_ENTOURAGE(g_FMMC_STRUCT.iRootContentIdHash)
						OR IS_THIS_ROCKSTAR_MISSION_SVM_VOLTIC2(g_FMMC_STRUCT.iRootContentIDHash)
						OR (WVM_FLOW_IS_CURRENT_MISSION_WVM_MISSION() AND NOT IS_THIS_ROCKSTAR_MISSION_WVM_TRAILERSMALL(g_FMMC_STRUCT.iRootContentIDHash))
						OR SVM_FLOW_IS_CURRENT_MISSION_SVM_MISSION()
						OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
						OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_POWER_MAD(g_FMMC_STRUCT.iAdversaryModeType)
						OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
						OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_WARFARE(g_FMMC_STRUCT.iAdversaryModeType)
						OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRQUOTA(g_FMMC_STRUCT.iAdversaryModeType)
						OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_SLASHERS(g_FMMC_STRUCT.iAdversaryModeType) // Modes where we can end and be transitioned into spectator on approximately the same frames.
						OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
							iEndDelay = 2000
						ELSE
							iEndDelay = 1000
						ENDIF
					ENDIF
				ENDIF
				
				IF USING_SHOWDOWN_POINTS()
					iEndDelay = 0
				ENDIF
				
				IF IS_BIT_SET(iLocalBoolCheck9, LBOOL9_PRE_MOCAP_CINE_TRIGGERED)
					START_PRE_MOCAP_SPLASH(sMocapCutscene, TRUE)
				ENDIF

				IF SHOULD_CONTINUE_TO_MISSION_END(iendDelay)
				AND NOT bDelayEnd					
					IF IS_THIS_SPECTATOR_CAM_RUNNING(g_BossSpecData.specCamData)
						PRINTLN("[RCC MISSION] spectator is running") 
					ENDIF
					IF GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
						PRINTLN("[RCC MISSION] game state is not end") 
					ENDIF
					
					//-- Stop any alarms
					//If it's not a strand mission
					IF NOT IS_THIS_IS_A_STRAND_MISSION()
					//or a strand mission is not being started
					OR (IS_THIS_IS_A_STRAND_MISSION() AND IS_BIT_SET( iLocalBoolCheck5, LBOOL5_SET_UP_STRAND_DETAILS_AT_END_OF_MISSION) AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED())
						INT iAlarm
						FOR iAlarm = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfProps-1)
						    IF isoundid[iAlarm] <> -1
								PRINTLN("[RCC MISSION] [ALARM] Stopping alarm ", iAlarm)
						        STOP_SOUND(isoundid[iAlarm])
					        ENDIF
						ENDFOR
						
						// turn off the casino alarm zones
						IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
							PRINTLN("[JS][ALARM] - PROCESS_MISSION_END_STAGE - Clearing Casino Alarm Zones - FreemodeLite")
							SET_AMBIENT_ZONE_STATE_PERSISTENT("AZ_H3_Casino_Alarm_Zone_01_Exterior", FALSE, TRUE)
							SET_AMBIENT_ZONE_STATE_PERSISTENT("AZ_H3_Casino_Alarm_Zone_02_Interior", FALSE, TRUE)
						ENDIF
					ENDIF
					
					IF GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
					AND NOT IS_THIS_SPECTATOR_CAM_RUNNING(g_BossSpecData.specCamData)
					AND IS_MISSION_OVER_REASON_DONE()
						SET_BIT(iLocalBoolCheck5, LBOOL5_BLOCK_TURNING_OFF_ARTIFICIAL_LIGHTS_EOM)
						
						IF NOT IS_BIT_SET( MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH )
							SET_GAME_STATE_ON_DEATH(AFTERLIFE_JUST_REJOIN)
						ENDIF
						IF NOT IS_BIT_SET(iLocalBoolCheck2,LBOOL2_CLEAR_CELEBRATION_SCREEN)
						OR IS_FAKE_MULTIPLAYER_MODE_SET()
							IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
								//Don't clean up the screen just yet, this will be done during the spectator transition.
								IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration)
								AND NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerHeistwinnerCutscene)
										SWITCH iClearDeathVisualsStage
											CASE 0
												CLEAR_ALL_BIG_MESSAGES()
												iClearDeathVisualsStage++
											BREAK
											CASE 1
												PRINTLN("[NETCELEBRATION] - [RCC MISSION] - CLEANUP_CELEBRATION_SCREEN call 3.")
												STOP_CELEBRATION_SCREEN_ANIM(sCelebrationData, "EARLYDEATH")
												CLEANUP_CELEBRATION_SCREEN(sCelebrationData, TRUE, "EARLYDEATH")
												SET_CELEBRATION_SCREEN_AS_ACTIVE(FALSE)
												PRINTLN("[RCC MISSION] clear celebrations early death") 
												SET_BIT(iLocalBoolCheck2,LBOOL2_CLEAR_CELEBRATION_SCREEN)
												IF NOT IS_NET_PLAYER_OK(LocalPlayer)
													IF NOT IS_CUTSCENE_PLAYING()
													AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYING_CUTSCENE)
														DO_RENDER_PHASE_PAUSE_FOR_CELEBRATION_SCREEN()
													ENDIF
												ENDIF
											BREAK
										ENDSWITCH
								ELSE
									PRINTLN("[RCC MISSION] - ciCELEBRATION_BIT_SET_TriggerEndCelebration OR ciCELEBRATION_BIT_SET_TriggerHeistwinnerCutscene bit is set, not clearing celebration screen.") 
									SET_BIT(iLocalBoolCheck2,LBOOL2_CLEAR_CELEBRATION_SCREEN)
								ENDIF
							ENDIF
						ENDIF
						
						// Make the player everything-proof before we do a cutscene
						IF NOT Is_Player_Currently_On_MP_Heist(LocalPlayer)
						OR NOT Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
							IF g_bFMMCLightsTurnedOff
								//block the lights coming back on at this stage in this mode
								IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(g_FMMC_STRUCT.iAdversaryModeType)
								AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_SLASHERS(g_FMMC_STRUCT.iAdversaryModeType)
									SET_ARTIFICIAL_LIGHTS_STATE(FALSE)
									g_bFMMCLightsTurnedOff = FALSE
								ENDIF
							ENDIF
						ENDIF
						SET_ENTITY_INVINCIBLE( PLAYER_PED_ID(), TRUE )
						SET_ENTITY_PROOFS( PLAYER_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE )
							
						IF NOT IS_BIT_SET( iLocalBoolCheck2, LBOOL2_LB_CAM_READY )
							MANAGE_END_CUTSCENE()
						ELSE
							PRINTLN("[RCC MISSION][PROCESS_MISSION_END_STAGE] limbo ")
						ENDIF
					ELSE
						PRINTLN("[RCC MISSION][PROCESS_MISSION_END_STAGE] calling spectator stuff")
						IF NOT IS_BIT_SET(iLocalBoolCheck3, LBOOL3_WAIT_END_SPECTATE)
							BOOL bDelaySpectator = FALSE
							
							IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet2, PBBOOL2_PLAYER_LEAVE_AREA_FAIL)
							OR IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet2, PBBOOL2_PLAYER_OUT_BOUNDS_FAIL)
							OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_LIONS_DEN(g_FMMC_STRUCT.iAdversaryModeType) // url:bugstar:3492549
								IF NOT HAS_NET_TIMER_STARTED(timerSpectatorDelay)
									REINIT_NET_TIMER(timerSpectatorDelay)
								ENDIF
								
								IF HAS_NET_TIMER_STARTED(timerSpectatorDelay)
									IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(timerSpectatorDelay) <= ciTimerSpectatorDelay
										PRINTLN("[PROCESS_MISSION_END_STAGE] bDelaySpectator = TRUE")
										bDelaySpectator = TRUE
									ENDIF
								ENDIF
							ENDIF
							
							IF NOT bDelaySpectator
								PRINTLN("[PROCESS_MISSION_END_STAGE] Calling MANAGE_EARLY_END_AND_SPECTATE")
								MANAGE_EARLY_END_AND_SPECTATE()
							ENDIF
						ENDIF
					ENDIF
					
				ELSE
					IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
						IF Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)
							UPDATE_PLAYER_IN_AIR_VEHICLE_DURING_CELEBRATION_SCREEN()
						ENDIF
					ENDIF
					PRINTLN("[RCC MISSION] waiting for results dealy to finish")
				ENDIF
			ENDIF
		ELSE
			IF (IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE() AND IS_ACTIVE_ROAMING_SPECTATOR_MODE_ENABLED())
			AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet,SBBOOL_MISSION_OVER)
				IF g_bMissionEnding
					g_bMissionEnding = FALSE
					CLEAR_MP_DECORATOR_BIT(PLAYER_ID(), MP_DECORATOR_BS_MISSION_ENDING)
					PRINTLN("[RCC MISSION] [MJM] PROCESS_MISSION_END_STAGE() - MP_DECORATOR_BS_MISSION_ENDING - g_bMissionEnding - CLEAR")
				ENDIF								
			ENDIF	
		ENDIF

	ELSE // Else IS_BIT_SET(iLocalBoolCheck3,LBOOL3_SHOWN_RESULTS) is true
		
		IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
			HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
		ENDIF
		
		IF bLocalPlayerPedOk
			SET_PED_RESET_FLAG(LocalPlayerPed, PRF_CannotBeTargetedByAI, TRUE)
		ENDIF
		
		//MANAGE_VOTING()
		PRINTLN("[RCC MISSION] results true") 
		
		IF NOT IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet,PBBOOL_ON_LEADERBOARD)
			#IF IS_DEBUG_BUILD
				IF g_SkipCelebAndLbd
					SET_BIT(MC_PlayerBD[iLocalPart].iClientBitSet,PBBOOL_ON_LEADERBOARD)
				ENDIF
			#ENDIF
			IF NOT IS_END_OF_ACTIVITY_HUD_ACTIVE()
			OR IS_A_STRAND_MISSION_BEING_INITIALISED()
				IF DONE_RANK_PREDICTION_READ()
					CLEANUP_THUMBS()
					NETWORK_SET_OVERRIDE_SPECTATOR_MODE(TRUE)
					FOR iteam = 0 TO (FMMC_MAX_TEAMS-1)
						NETWORK_OVERRIDE_TEAM_RESTRICTIONS(iteam,TRUE)
					ENDFOR
					CLIENT_SET_UP_EVERYONE_CHAT()
					CLEAR_ALL_BIG_MESSAGES()
					
					IF NOT IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING()	
						START_LBD_AUDIO_SCENE()
					ENDIF
					
					if bLocalPlayerPedOk
					and Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)
						INVISIBLE_FOR_LBD()
					endif
					
					IF DID_I_JOIN_MISSION_AS_SPECTATOR()
						IF IS_SCREEN_FADED_OUT()
						OR IS_SCREEN_FADING_OUT() 
							DO_SCREEN_FADE_IN(1)
							PRINTLN("[RCC MISSION] FADING IN FOR LEADERBOARD")
						ENDIF
					ENDIF
					//Kill face to face converstaion
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_POWERPLAY_PICKUP_SOUNDS)
					AND NOT IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING()	
						KILL_FACE_TO_FACE_CONVERSATION()
						PRINTLN("[RCC MISSION] KILL_FACE_TO_FACE_CONVERSATION called")
					ELSE
						PRINTLN("[RCC MISSION] KILL_FACE_TO_FACE_CONVERSATION blocked bc ciENABLE_POWERPLAY_PICKUP_SOUNDS")
					ENDIF
					SET_BIT(MC_PlayerBD[iLocalPart].iClientBitSet,PBBOOL_ON_LEADERBOARD)
				ELSE
					PRINTLN("[RCC MISSION] waiting for rank prediction")
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(iLocalBoolCheck2,LBOOL2_CLEAR_END_BIG_MESSAGE)
					CLEAR_ALL_BIG_MESSAGES()
					PRINTLN("[RCC MISSION] clear end of activity hud of big messages") 
					SET_BIT(iLocalBoolCheck2,LBOOL2_CLEAR_END_BIG_MESSAGE)
				ENDIF
				PRINTLN("[RCC MISSION] waiting for awards to clear") 
			ENDIF
		ELSE
		
			// Don't draw the normal leaderboard if we are drawing the Rounds one.
			IF NOT SHOULD_DRAW_ROUNDS_LBD()
				IF NOT IS_BIT_SET(iLocalBoolCheck3,LBOOL3_NEW_JOB_SCREENS)
				AND NOT DID_PLAYER_PASS_HEIST() 
					MANAGE_LEADERBOARD_DISPLAY()
					
					IF (NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
					OR NOT GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION())
					AND NOT CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
						UNTOGGLE_RENDERPHASES_FOR_LEADERBOARD()
					ENDIF
				ELSE
					SET_ON_LBD_GLOBAL(FALSE)
				ENDIF
			ENDIF
			
			IF IS_PLAYER_ON_A_PLAYLIST(LocalPlayer)
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdresultstimer) > 6000
					IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
						IF SHOULD_ALLOW_THUMB_VOTE()
						AND NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
							SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
							PRINTLN("[RCC MISSION] SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)")
						ELIF IS_THIS_A_ROUNDS_MISSION()
							SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
							PRINTLN("[RCC MISSION] SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)IS_PLAYER_ON_A_PLAYLIST ")
						ENDIF
					ELSE
						DO_THUMB_VOTING(sSaveOutVars, sEndOfMission, FALSE, HAS_LEADERBOARD_TIMER_EXPIRED(), TRUE)
						//If I joined as a spectator
						IF IS_PLAYER_SPECTATOR_ONLY(LocalPlayer)
						//and the timer expired
						AND HAS_LEADERBOARD_TIMER_EXPIRED()
						//and i've not voted
						AND NOT FM_PLAY_LIST_HAVE_I_VOTED()
							//Set me as
							FM_PLAY_LIST_I_WANT_TO_PLAY_NEXT()
						ENDIF
					ENDIF
					
					BOOL bSpectate		= FALSE
					BOOL bSkipLbd		= FALSE
					BOOL bNotUsedRemove = FALSE
					BOOL bRaceTimeout 	= HAS_LEADERBOARD_TIMER_EXPIRED()
					
					IF CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS(iPlaylistProgress, 
																		sEndOfMission, 
																		tdresultstimer.Timer, 
																		VIEW_LEADERBOARD_TIME, 
																		"END_LBD_CONTN", 
																		bSpectate, 
																		bSkipLbd, 
																		bNotUsedRemove,
																		bRaceTimeout)
																		
						SET_MC_CLIENT_GAME_STATE(GAME_STATE_MISSION_OVER)
						SET_BIT(iLocalBoolCheck,LBOOL_LB_FINISHED)
						
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_POWERPLAY_PICKUP_SOUNDS)
							STOP_AUDIO_SCENE("MP_LEADERBOARD_SCENE")
						ELSE
							IF IS_AUDIO_SCENE_ACTIVE("MP_LEADERBOARD_PP_SCENE")
								STOP_AUDIO_SCENE("MP_LEADERBOARD_PP_SCENE")
							ENDIF
						ENDIF
						
						#IF IS_DEBUG_BUILD
						IF HAS_LEADERBOARD_TIMER_EXPIRED()
							PRINTLN("[PLY_LBD] CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS, HAS_LEADERBOARD_TIMER_EXPIRED() TRUE ")
						ELSE
							PRINTLN("[PLY_LBD] CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS, HAS_LEADERBOARD_TIMER_EXPIRED() FALSE")
						ENDIF
						IF bSkipLbd
							PRINTLN("[PLY_LBD] CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS, bSkipLbd TRUE ")
						ELSE
							PRINTLN("[PLY_LBD] CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS, bSkipLbd FALSE")
						ENDIF
						IF FM_PLAY_LIST_HAVE_I_VOTED()
							PRINTLN("[PLY_LBD] CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS, FM_PLAY_LIST_HAVE_I_VOTED TRUE")
						ELSE
							PRINTLN("[PLY_LBD] CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS, FM_PLAY_LIST_HAVE_I_VOTED FALSE")
						ENDIF
						IF HAS_MISSION_END_CONTINUE_BEEN_PRESSED()
							PRINTLN("[PLY_LBD] CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS, HAS_MISSION_END_CONTINUE_BEEN_PRESSED TRUE")
						ELSE
							PRINTLN("[PLY_LBD] CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS, HAS_MISSION_END_CONTINUE_BEEN_PRESSED FALSE")
						ENDIF
						
						PRINTLN("[PLY_LBD] CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS, RETURN TRUE ")
						#ENDIF
						
					ELSE
						PRINTLN("[RCC MISSION] waiting for CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS ")
					ENDIF
				ENDIF
			ELSE
				IF GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
					IF SHOULD_DO_POST_MISSION_SCENE_CELEBRATION()
						TEXT_LABEL_23 tl23Twmp
						SET_VARS_ON_FMMC_EOM_VOTE_PASSING(ciFMMC_EOM_VOTE_STATUS_CONTINUE, <<0.0, 0.0, 0.0>>, tl23Twmp)
						SET_BIT(iLocalBoolCheck3,LBOOL3_MOVE_TO_END)
						PRINTLN("[RCC MISSION] SHOULD_DO_POST_MISSION_SCENE_CELEBRATION() = TRUE, setting LBOOL3_MOVE_TO_END since we don't need lb votes.")
					ENDIF
					IF IS_BIT_SET(iLocalBoolCheck3,LBOOL3_MOVE_TO_END)
						THEFEED_FORCE_RENDER_OFF()
						SET_MC_CLIENT_GAME_STATE(GAME_STATE_MISSION_OVER)
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_POWERPLAY_PICKUP_SOUNDS)
							STOP_AUDIO_SCENE("MP_LEADERBOARD_SCENE")
						ELSE
							IF IS_AUDIO_SCENE_ACTIVE("MP_LEADERBOARD_PP_SCENE")
								STOP_AUDIO_SCENE("MP_LEADERBOARD_PP_SCENE")
							ENDIF
						ENDIF
						IF IS_FAKE_MULTIPLAYER_MODE_SET()
							DO_SCREEN_FADE_OUT(500)
						ELSE
							SET_BIT(iLocalBoolCheck,LBOOL_LB_FINISHED)
						ENDIF
					ELSE
						IF NOT IS_BIT_SET(iLocalBoolCheck3,LBOOL3_NEW_JOB_SCREENS)
							BOOL bSCLeaderboardAvailable
							IF IS_THIS_A_RSTAR_ACTIVITY()
							AND NOT IS_PLAYER_SCTV(LocalPlayer)	
								bSCLeaderboardAvailable = TRUE
							ELSE
								bSCLeaderboardAvailable = FALSE
							ENDIF
							#IF IS_DEBUG_BUILD
							IF g_SkipCelebAndLbd
								sCelebrationStats.bPassedMission = FALSE
								SET_MY_EMO_VOTE_STATUS(ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART) 
								FM_PLAY_LIST_SET_ME_AS_VOTED()
							ENDIF
							#ENDIF
														
							IF MAINTAIN_END_OF_MISSION_SCREEN(	lbdVars,
																sEndOfMission, 
																MC_serverBD.sServerFMMC_EOM,
																HAS_LEADERBOARD_TIMER_EXPIRED(),
																TRUE,FALSE,TRUE,TRUE,
																bSCLeaderboardAvailable,tdresultstimer.Timer,VIEW_LEADERBOARD_TIME,"END_LBD_CONTN", FALSE,TRUE, SHOULD_RUN_NEW_VOTING_SYSTEM(sEndOfMission.iQuitProgress, MC_serverBD.sServerFMMC_EOM.iVoteStatus), sCelebrationStats.bPassedMission)
							#IF IS_DEBUG_BUILD
							AND NOT bLBDHold
							#ENDIF
								
								// Show a final accumulative Rounds leaderboard (see DISPLAY_ROUNDS_LBD)
								IF (SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
								OR ROUNDS_MISSION_OVER_FOR_JIP_LOCAL_PLAYER(PLAYER_ID()))
								AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bQuitJob)//GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].bQuitJob
								#IF IS_DEBUG_BUILD
								AND NOT g_SkipCelebAndLbd
								#ENDIF
									IF NETWORK_IS_GAME_IN_PROGRESS()
									AND NETWORK_IS_HOST_OF_THIS_SCRIPT()
										IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_ROUNDS_LBD_DO)
											SET_BIT(MC_serverBD.iServerBitSet2, SBBOOL2_ROUNDS_LBD_DO)
											PRINTLN("2460167 [CS_RNDS] [HOST] SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD, LBOOL6_SAVED_ROUNDS_DO_ROUNDS_LBD ")
										ENDIF
									ENDIF
								ELSE
									IF SHOULD_RUN_NEW_VOTING_SYSTEM(sEndOfMission.iQuitProgress, MC_serverBD.sServerFMMC_EOM.iVoteStatus)
									AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bQuitJob)//GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].bQuitJob
										SET_READY_FOR_NEXT_JOBS(nextJobStruct)
										SET_BIT(iLocalBoolCheck3,LBOOL3_NEW_JOB_SCREENS)
									ELSE
										SET_BIT(iLocalBoolCheck3,LBOOL3_MOVE_TO_END)
									ENDIF
								ENDIF
							ELSE
								DO_THUMB_VOTING(sSaveOutVars, sEndOfMission, FALSE, HAS_LEADERBOARD_TIMER_EXPIRED())
								PRINTLN("[RCC MISSION] waiting for LB votes ")
							ENDIF
						ELSE
							IF DO_END_JOB_VOTING_SCREEN(g_sMC_serverBDEndJob, nextJobStruct, g_sMC_serverBDEndJob.timerVotingEnds.Timer)
								SET_BIT(iLocalBoolCheck3,LBOOL3_MOVE_TO_END)
							ELSE
								PRINTLN("[RCC MISSION] waiting for EOJ votes ")
							ENDIF
						ENDIF
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION] waiting for GAME_STATE_END ")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

SCRIPT_TIMER stLeaderboardRounds
CONST_INT ciROUNDS_LBD_TIMEOUT 15000
INT iTimerProgressLbd
SCALEFORM_INDEX siLbdHelp

PROC DO_ROUND_LBD_TIMER(TIME_DATATYPE iTimer)

	IF ON_NEW_JOB_VOTE_SCREEN()
		EXIT
	ENDIF

	INT ms = ciROUNDS_LBD_TIMEOUT - ABSI(GET_TIME_DIFFERENCE(iTimer, GET_NETWORK_TIME()))
	INT sec = ROUND(ms * 0.001)

	STRING stTimer = "END_LBD_CONT"
	SPRITE_PLACEMENT aSprite = GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
	STRING sProfile = GET_PROFILE_BUTTON_NAME()
	
	STRING sInvalidButtonTime //= GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_Y)//ICON_INVALID
//	STRING sBackButtonProfile = GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)//ICON_BUTTON_BACK
	STRING sProfileButtonName = GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT)
	
	SWITCH iTimerProgressLbd
	
		CASE 0		
			IF sec > 0
				PRINTLN("FMMC EOM - DO_ROUND_LBD_TIMER, iTimerProgressLbd = 1 ")					
				iTimerProgressLbd = 1
			ENDIF
		BREAK
		
		CASE 1
			IF LOAD_MENU_ASSETS()
				REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(nextJobStruct.scaleformStruct)
				ADD_SCALEFORM_INSTRUCTIONAL_BUTTON_WITH_INT_AS_TIME(sInvalidButtonTime, stTimer, ms, nextJobStruct.scaleformStruct) 	// time
				ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(sProfileButtonName, sProfile, nextJobStruct.scaleformStruct)							// profile

				nextJobStruct.iUpdateTimer = sec
				PRINTLN("FMMC EOM - DO_ROUND_LBD_TIMER, iTimerProgressLbd = 2 ")					
				iTimerProgressLbd = 2
			ENDIF
		BREAK
		
		CASE 2
			IF sec >= 0
				IF nextJobStruct.iUpdateTimer <> sec
					UPDATE_SCALEFORM_INSTRUCTION_BUTTON_SPINNER(siLbdHelp, ms, nextJobStruct.scaleformStruct)
					nextJobStruct.iUpdateTimer = sec
				ENDIF
			ELSE
				PRINTLN("FMMC EOM - DO_ROUND_LBD_TIMER, iTimerProgressLbd = 3 ")	
				iTimerProgressLbd = 1
			ENDIF
			
			RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(siLbdHelp, aSprite, nextJobStruct.scaleformStruct, SHOULD_REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(nextJobStruct.scaleformStruct)) 
		BREAK
		
	ENDSWITCH
ENDPROC

PROC DISPLAY_ROUNDS_LBD()

	IF NOT IS_BIT_SET(iLocalBoolCheck3,LBOOL3_NEW_JOB_SCREENS)
		DO_ROUND_LBD_TIMER(stLeaderboardRounds.Timer)
	ENDIF
	
	IF SHOULD_DRAW_ROUNDS_LBD()
		IF NOT g_FinalRoundsLbd
			g_FinalRoundsLbd = TRUE
			PRINTLN("[2460167] [CS_RNDS] DISPLAY_ROUNDS_LBD, g_FinalRoundsLbd = TRUE  ")
		ENDIF
	ENDIF

	// CLIENT LOGIC
	SWITCH iRoundsLbdStage
		CASE 0
			IF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_ROUNDS_LBD_GO)
				IF SHOULD_END_ROUNDS_MISSION_DUE_TO_TEAM_LEAVING()
					MC_playerBD[iLocalPart].iBet = BROADCAST_BETTING_MISSION_FINISHED_TIED()
				ELSE
					IF MC_serverBD_3.iWinningPlayerForRounds > -1
						MC_playerBD[iLocalPart].iBet = BROADCAST_BETTING_MISSION_FINISHED(MC_serverBD_3.iWinningPlayerForRoundsId)
					ELSE
						PRINTLN("[2460167] [CS_RNDS] DISPLAY_ROUNDS_LBD, MC_serverBD_3.iWinningPlayerForRounds =  ", MC_serverBD_3.iWinningPlayerForRounds)
						EXIT
					ENDIF	
				ENDIF	
				LBPlacement.bHudScreenInitialised = FALSE
				lbdVars.bPlayerRowGrabbed = FALSE
				iRoundsLbdStage++
				iTeamRowBit = 0
				START_NET_TIMER(stLeaderboardRounds)
				PRINTLN("[2460167] [CS_RNDS] DISPLAY_ROUNDS_LBD, iRoundsLbdStage =  ", iRoundsLbdStage)
			ENDIF
		BREAK
		
		CASE 1
			IF HAS_NET_TIMER_EXPIRED(stLeaderboardRounds, ciROUNDS_LBD_TIMEOUT)
				iRoundsLbdStage++
				PRINTLN("[2460167] [CS_RNDS] DISPLAY_ROUNDS_LBD, iRoundsLbdStage =  ", iRoundsLbdStage)
			ELSE
				UNTOGGLE_RENDERPHASES_FOR_LEADERBOARD()
				MANAGE_LEADERBOARD_DISPLAY(TRUE)
			ENDIF
		BREAK
		
		CASE 2
			IF SHOULD_RUN_NEW_VOTING_SYSTEM(sEndOfMission.iQuitProgress, MC_serverBD.sServerFMMC_EOM.iVoteStatus)
			AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bQuitJob)//GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].bQuitJob
				SET_READY_FOR_NEXT_JOBS(nextJobStruct)
				IF NOT IS_BIT_SET(iLocalBoolCheck3,LBOOL3_NEW_JOB_SCREENS)
					BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("FMMC_PLYLOAD")
					END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_LOADING))
				ENDIF
				SET_BIT(iLocalBoolCheck3,LBOOL3_NEW_JOB_SCREENS)
				PRINTLN("[2460167] [CS_RNDS] DISPLAY_ROUNDS_LBD, SHOULD_RUN_NEW_VOTING_SYSTEM, LBOOL3_NEW_JOB_SCREENS ")
			ELSE
				SET_BIT(iLocalBoolCheck3,LBOOL3_MOVE_TO_END)
				PRINTLN("[2460167] [CS_RNDS] DISPLAY_ROUNDS_LBD, LBOOL3_MOVE_TO_END ")
			ENDIF
			SET_ON_LBD_GLOBAL(FALSE)
		BREAK
	ENDSWITCH
	
ENDPROC

PROC PROCESS_TRADING_PLACES_END_TEAM_SWAP()
	IF MC_serverBD_1.piTimeBarWinners[0] != INVALID_PLAYER_INDEX()	//Score set by server
		IF NOT IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesFinalTeamSwap)
			PRINTLN("[PROCESS_TRADING_PLACES_TIME_BAR] - IF MC_serverBD_1.piTimeBarWinners[0] != INVALID_PLAYER_INDEX()")
			INT i
			PRINTLN("[PROCESS_TRADING_PLACES_TIME_BAR] - MC_playerBD[iLocalPart].iTeam = ", MC_playerBD[iLocalPart].iTeam)
			REPEAT COUNT_OF(MC_serverBD_1.piTimeBarWinners) i
				IF MC_serverBD_1.piTimeBarWinners[i] != INVALID_PLAYER_INDEX()
					IF LocalPlayer = MC_serverBD_1.piTimeBarWinners[i]
						PRINTLN("[PROCESS_TRADING_PLACES_TIME_BAR] [", i, "] LocalPlayer = ", GET_PLAYER_NAME(MC_serverBD_1.piTimeBarWinners[i]))
						IF MC_playerBD[iLocalPart].iTeam != 1
							CHANGE_PLAYER_TEAM(1, TRUE)	//Winners Team
							
							PRINTLN("[PROCESS_TRADING_PLACES_TIME_BAR] Moved to winning team!")
						ENDIF
						
						SET_BIT(iTradingPlacesBitSet, ciTradingPlacesFinalTeamSwap)
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("[PROCESS_TRADING_PLACES_TIME_BAR] [", i, "] ELSE = ", GET_PLAYER_NAME(MC_serverBD_1.piTimeBarWinners[i]))
					#ENDIF
					ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[PROCESS_TRADING_PLACES_TIME_BAR] [", i, "] ELSE = INVALID_PLAYER_INDEX()")
				#ENDIF
				ENDIF
			ENDREPEAT
			
			IF NOT IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesFinalTeamSwap)
				PRINTLN("[PROCESS_TRADING_PLACES_TIME_BAR] IF NOT IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesFinalTeamSwap)")
				IF MC_playerBD[iLocalPart].iTeam != 0
					CHANGE_PLAYER_TEAM(0, TRUE)	//Losers Team
					
					PRINTLN("[PROCESS_TRADING_PLACES_TIME_BAR] Moved to losing team.")
				ENDIF
				
				SET_BIT(iTradingPlacesBitSet, ciTradingPlacesFinalTeamSwap)
			ENDIF
		ENDIF
	ENDIF
ENDPROC
