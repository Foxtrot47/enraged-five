USING "globals.sch"
USING "shared_hud_displays.sch"
USING "net_spawn.sch"
USING "net_objective_text.sch"
USING "freemode_header.sch"
USING "FMMC_HEADER.sch"
USING "screen_gang_on_mission.sch"
USING "leader_board_common.sch"
USING "net_spectator_cam.sch"
USING "weapons_public.sch" 
USING "Thumbs.sch"
USING "Cheat_Handler.sch"
USING "net_rank_rewards.sch"
USING "fmmc_request_to_play_mission.sch"
USING "fm_playlist_header.sch"
USING "net_corona_position.sch"
USING "menu_public.sch"
USING "FMMC_Restart_Header.sch"
USING "socialclub_leaderboard.sch"
USING "net_leaderboards.sch"
USING "hacking_public.sch"
USING "net_safe_crack.sch"
USING "social_feed_controller.sch"
USING "net_script_tunables.sch"
USING "net_job_intro.sch"
USING "net_save_create_eom_vehicle.sch"
USING "FM_Post_Mission_Cleanup_Public.sch"
USING "net_interactions.sch"
USING "FMMC_next_job_screen.sch"
USING "net_celebration_screen.sch"
USING "net_wait_zero.sch"
USING "heist_bags.sch"
USING "net_SCTV_common.sch"
USING "crowd_control.sch"
USING "FMMC_Bot_Controller.sch"
USING "net_mission_controler_public.sch"
USING "emergency_call.sch"
USING "asset_management_public.sch"
USING "net_doors.sch"
USING "SVM_MISSION_FLOW.sch"
USING "freemode_events_header.sch"

USING "net_synced_ambient_pickups.sch"

//USING "dancing_public.sch"
	USING "charm_minigame_public.sch"
USING "ped_component_public.sch"
USING "net_heists_cutscenes.sch"
USING "cellphone_public.sch"
USING "drill_minigame.sch"
USING "net_vaultDrill.sch"
USING "net_private_yacht_private.sch"
USING "net_beam_hack.sch"
USING "net_hotwire.sch"
	USING "net_heists_common.sch"
#IF IS_DEBUG_BUILD
	USING "profiler.sch"
	USING "FMMC_ContentOverviewDebug.sch"
#ENDIF
USING "net_orbital_cannon.sch"
USING "Maintain_Asassination_Mission_Locates.sch"

USING "net_arena_wars_career.sch"

USING "net_ArenaAnnouncer_Header.sch"
USING "net_hacking_fingerprint_minigame.sch"
USING "net_hacking_order_unlock.sch"

#IF FEATURE_GEN9_STANDALONE
USING "net_heists_public.sch"
USING "net_heists_common.sch"
#ENDIF // FEATURE_GEN9_STANDALONE

const_int total_number_of_cutscene_vehicles	ciTOTAL_NUMBER_OF_MC_CUT_VEHCILES 

CONST_INT ciINVALID_CUTSCENE_WARP_HEADING		-666

BOOL bCutsceneAreaClearedLate
BOOL bLocalHandBrakeOnFlag
bool render_underwater_tunnel_on_radar = false
BOOL bTriggerSuspense = FALSE

SCRIPT_TIMER stLbdStop
SCRIPT_TIMER streamCSFailsafeTimer
SCRIPT_TIMER CSMinPulseTimer
CONST_INT CS_MIN_PULSE_TIME	1000
SCRIPT_TIMER CSFailEndTimer
CONST_INT CS_MIN_FAIL_END_TIME	2000
SCRIPT_TIMER timerCelebReceivedAllPlayerVariationUpdates
SCRIPT_TIMER tdRaceMissionTime

CONST_INT OUT_OF_MAP_BOUNDS_TIMER_LIMIT	10000
CONST_INT ciPP_INTRO_ANNOUNCER_GENERIC_CHANCE		60

#IF IS_DEBUG_BUILD
INT iCaptureTeamDebug
INT iNumberofCaptureTeamsDebug
BOOL bTeamInLocDebug[FMMC_MAX_TEAMS]
#endif

INT waittimezap = 700
INT iOpenAnimStarted = -1

INT iKilledByHealthDrainBS
INT iNotKilledByHealthDrainOverrideBS
SCRIPT_TIMER tdNotKilledByHealthDrainTimer
FLOAT fDrainHealPercentage = 0.0

MASK_ANIM_DATA 						maskAnimData
//MP_OUTFITS_APPLY_DATA   			sMPOutfitApplyData

//BOOL bPreloadedMaskOutfitData 	= 	FALSE
BOOL bAnimationPlaying 			= 	FALSE

// ----------------------------------- MISSION CREATOR VARIABLES ------------------------------------------------------------

#IF IS_DEBUG_BUILD
	INT iPlayerPressedF = -1
	INT iPlayerPressedS = -1
	
	MissionStageMenuTextStruct sTeamSkipStruct[FMMC_MAX_RULES + 2]
	INT iSkipTeam = -1
	BOOL bUsedDebugMenu
	BOOL bSyncScenePedDebugDisplay
	
	//For prints
	TEXT_LABEL_31 tl31ScriptName				= "FM_Mission_Controler.sc"
	
	CONST_INT LDEBUGBS_FIRST_DONTFAILONDEATH_CHECK	0
	CONST_INT LDEBUGBS_TEAM_CHANGED_WAIT_FOR_OBJTXT	1
	
	INT iLocalDebugBS
	
	SCRIPT_TIMER tdDontFailOnDeathDelayTimer
	
	BOOL boutputdebugspam = TRUE
	BOOL bAddToGroupPrints = FALSE
	BOOL bGotoLocPrints = FALSE
	BOOL bStopVehDeadCleanup = FALSE
	BOOL bPlaneDropoffPrints = FALSE
	BOOL bDoPedCountPrints
	BOOL bPlayerInDropOffPrints
	BOOL bEnableDamageTrackerOnNetworkID
	BOOL b4517649Debug = FALSE
	BOOL bHideObjects = FALSE
	
	BOOL bLBDHold
	
	BOOL bDebugCircuitHack
	
	INT iDebugRewardCash
	INT iDebugPPRewardCashTune
	INT iDebugCashPickup
	INT iDebugXPDelVeh
	INT iDebugXPDelObj
	INT iAllKillXPBonus
	INT iAllHeadshotXPPlayer
	INT iAllHeadshotXPTeam
	INT iDebugTotalRewardXP
	INT iDebugXPLivesBonus
	INT iDebugXPGiven
	INT iDebugXPInvite
	INT iDebugXPDailyWin
	INT iDebugXP5Plays
	INT iDebugDrillOutfitCheckOverride = -1
	
	BOOL bOutputCopMissingBlipDebug
	BOOL bDebugIsThermiteDebuggingEnabled
	BOOL bDebugEnableThermiteReplay
	
	INT iPedGivenDebugName[FMMC_MAX_PEDS_BITSET]
	
	vector widget_bl
	vector widget_br
	vector widget_offset_bl
	vector widget_offset_br
	float widget_angled_length
	float widget_heading
	FLOAT fDebugDrillCamNearDofStart = -1.0
	FLOAT fDebugDrillCamNearDofEnd = -1.0
	FLOAT fDebugDrillCamFarDofStart = -1.0
	FLOAT fDebugDrillCamFarDofEnd = -1.0
	
	BOOL bDisableDeathsCount
	
	BOOL bActivateArtificialLightsDebug
	BOOL bPrintArticificalLightTimerValue
	INT iDebugArtificialLightsOffDuration = -1
	
	BOOL bWdGangChaseDebug
	BOOL bWdLoseGangBackupDebug
	BOOL bWdLocDebug
	BOOL bWdDropOffMarker = FALSE
	BOOL bWdMultiTimer
	BOOL bWdSeatPreferenceDebug
	
	BOOL bWdPauseGameTimerRag
	BOOL bWdPauseGameTimer
	BOOL bWdPauseGameTimerStarted
	
	BOOL bWdIncrementTimer
	BOOL bWdDecrementTimer
	
	//casco widget variables
	bool widget_activate_ptfx = false
	vector widget_ptfx_pos
	vector widget_ptfx_rot
	ptfx_id widget_ptfx
	
	BOOL bDebugCrossTheLineHUDPrints = FALSE
	
	INT iStealthAlpha = 0
	
	VECTOR vIWCamOffsetOverride
	VECTOR vIWCamRotOverride
	
	VECTOR vRappelCam
	VECTOR vRappelRot
	FLOAT fRappelFOV
	BOOL bUseRappelCamWidget
	INT iRappelParticipant = -1
	
	FLOAT fMinimapXOffset = 0.0
	FLOAT fMinimapYOffset = 0.0
#ENDIF

INT iteamstartPlayerJoinBitset
INT iPlayerIndexForCutscene
TEXT_LABEL_23 tlPlayerSceneHandle
//For forcing the host to not be a spectator
INT iManageSpechost = 0
INT iDesiredHost = -1

INT iDelayedDeliveryXPToAward
INT iXPExtraRewardGained
INT iTeamRowBit
INT iTeamNameRowBit 		


INT iTotalShotsFired
INT iTotalShotsHit

INT iActualTake // For end heist escreen.

INT iBinBagLocallyAttachedLastFrame
VECTOR vLastBinBagAttachRot

//current heist mission
INT iCurrentHeistMissionIndex = -1


BOOL bWasKnockOffSettingChangedForTrash = FALSE

INT iCTFMissionHelpProgress

BOOL bDelayedClientCheck = FALSE
BOOL bHasTripSkipJustFinished = FALSE

INT iBiolabBoostingGun[2]

SCRIPT_TIMER tdTextDelay[FMMC_MAX_NUM_SMS]
INT iMyLocalSentTextMsgBitset

INT iDummyBlipProgress
INT iDummyBlipToReach = -1 // Do a loop up to this int when a new priority is reached
INT iDummyBlipCurrentOverride = -1

INT iLocalDialoguePriority = -1
INT iDialogueProgress
INT iDialogueProgressLastPlayed = -1
INT iDialogueProgressCached
INT iLocalDialoguePending[ciMAX_DIALOGUE_BIT_SETS_LEGACY]
INT iLocalDialoguePlayedBS[ciMAX_DIALOGUE_BIT_SETS_LEGACY] //Bitset that stores whether dialogue has been played or not locally (added for 2193702)
INT iLocalDialoguePlayingBS[ciMAX_DIALOGUE_BIT_SETS_LEGACY]
INT iDialogueKilledBS[ciMAX_DIALOGUE_BIT_SETS_LEGACY]
INT iDialogueInterruptedBS[ciMAX_DIALOGUE_BIT_SETS_LEGACY]
INT iOldDialoguePoints[FMMC_MAX_TEAMS]
INT iDialogueVehicleOwnedBS[ciMAX_DIALOGUE_BIT_SETS_LEGACY]
INT iDialogueVehicleNotOwnedBS[ciMAX_DIALOGUE_BIT_SETS_LEGACY]
INT iDialogueProgressSFX1Played[ciMAX_DIALOGUE_BIT_SETS_LEGACY]
INT iDialogueProgressSFX2Played[ciMAX_DIALOGUE_BIT_SETS_LEGACY]
INT iLocalDialogueBlockedBS[ciMAX_DIALOGUE_BIT_SETS_LEGACY]
#IF IS_DEBUG_BUILD
INT iLocalDialogueForcePlay[ciMAX_DIALOGUE_BIT_SETS]
#ENDIF

CONST_INT ciDT_Blocked_PedHasBeenTased			0
CONST_INT ciDT_Blocked_PedHasBeenTranquilized	1
INT iDialogueBlockedConditionBS
INT iDialogueKillsThisRule[FMMC_MAX_RULES]
INT iDialogueKillsThisRuleSet

SCRIPT_TIMER tdDialogueTimer[FMMC_MAX_DIALOGUES_LEGACY]
structPedsForConversation speechPedStruct
INT iCurrentSpeaker

CONST_INT ciNUM_CACHED_ANNOUNCEMENTS			2
STRUCT STRUCT_CACHED_PP_ANNOUNCEMENT
	INT iPowerupType = -1
	INT iSenderTeam = -1
	PLAYER_INDEX piSenderPlayer = NULL
	INT iFrameTimeAdded = -1	
ENDSTRUCT

structPedsForConversation powerplayAnnouncer
STRUCT_CACHED_PP_ANNOUNCEMENT sCachedAnnouncement
SCRIPT_TIMER stPowerupAnnouncerDelayTimer

SCRIPT_TIMER RoundCutsceneTimeOut

//Random Next Objective
INT iRandomNextObjectiveSeed[FMMC_MAX_TEAMS]

	INT iTrackifyProgress

JOB_INTRO_CUT_DATA jobIntroData
SCRIPT_TIMER tdIntroTimer
SCRIPT_TIMER tdSafetyTimer

VECTOR vBuildingLocations
VECTOR vPlayerWarpLocation

INT iEntityRespawnType = ci_TARGET_NONE
INT iEntityRespawnID = 0
VECTOR vEntityRespawnLoc
INT iEntityRespawnInterior = -1
INT iEntityRespawnRandomSelected
INTERIOR_INSTANCE_INDEX SpawnInterior = NULL

BOOL bApartmentIntroReady = FALSE

INT iFreezeBinbagTracker[ 2 ]

INT iWaterCalmingQuad[FMMC_MAX_WATER_CALMING_QUADS]

LBD_VARS lbdVars
INT iRoundsLbdStage 

INT iEmpSoundId = -1
INT iSoundIDCountdown = -1
INT iSoundIDVehBomb = GET_SOUND_ID()
INT iSoundIDVehBomb2 = GET_SOUND_ID()
INT iSoundIDVehBombCountdown = GET_SOUND_ID()
INT iSoundIDVehBeacon = GET_SOUND_ID()
INT iSlipStreamSoundId = -1
INT iSlipStreamLeaderSoundId = GET_SOUND_ID()

INT iRadioEmitterPropCheck = -1

script_timer safe_drive_stat_time

int break_off_container_lock_system_status = 0
int apply_force_to_caso_system_status
vehicle_index casco_vehicle
ptfx_id casco_lock_ptfx

STRING sMyRadioStation
INT iTeams[4]

FLOAT	fGateOpenRatio = 0
BOOL 	bOpenGate = FALSE
INT iPhoneIntroDoneForShot = -1
INT iDoorCoverAreaBlocked

STRUCT STRUCT_RETASK_DIRTY_FLAG_DATA
	INT iDirtyFlagPedNeedsRetask[FMMC_MAX_PEDS_BITSET]
	INT iDirtyFlagPedRetaskHandshake[FMMC_MAX_PEDS_BITSET]
	INT iPedSpookedBitsetCopy[FMMC_MAX_PEDS_BITSET]
	INT iPedAggroedBitsetCopy[FMMC_MAX_PEDS_BITSET]
	INT iPedGotoProgressCopy[FMMC_MAX_PEDS]
ENDSTRUCT

STRING sPTFXThermalAsset = "scr_ornate_heist"

STRUCT_RETASK_DIRTY_FLAG_DATA sRetaskDirtyFlagData
INT iPedGivenSpookedGotoTaskBS[FMMC_MAX_PEDS_BITSET]
INT iPedTaskedGotoProgress[FMMC_MAX_PEDS]
INT iPedMovementClipsetOverrideBitset1[FMMC_MAX_PEDS_BITSET]
INT iPedMovementClipsetOverrideBitset2[FMMC_MAX_PEDS_BITSET]
INT iPedSpecBitset[FMMC_MAX_PEDS_BITSET]
INT iPedHasTrailerBitset[FMMC_MAX_PEDS_BITSET]
INT iPedTrailerDetachedBitset[FMMC_MAX_PEDS_BITSET]
INT iPedStartedInCoverBitset[FMMC_MAX_PEDS_BITSET]

INT iPedEMPRandomAnimationIndex[FMMC_MAX_PEDS]
CONST_INT ciPhoneEMP_PedAnims_Flashlight	1
CONST_INT ciPhoneEMP_PedAnims_NoFlashlight	2
CONST_INT ciPhoneEMP_PedAnims_Unassigned	0

SAVE_OUT_UGC_PLAYER_DATA_VARS sSaveOutVars //Ratings data cloud struct
//SCRIPT_TIMER timeThumbSafe

//BOOL bfriendcomparison
INT iSpectatorBit

INT iPlaylistProgress

CONST_INT PROCESSED_PRE_GAME_FAILSAFE_TIMEOUT 20000

INT iSpectatorTarget = -1
PLAYER_INDEX PlayerToUse
PLAYER_INDEX LocalPlayer
PED_INDEX PlayerPedToUse
PED_INDEX LocalPlayerPed
INT iPartToUse
INT iLocalPart
BOOL bPlayerToUseOK
BOOL bPedToUseOk
BOOL bLocalPlayerOK
BOOL bLocalPlayerPedOk
BOOL bIsLocalPlayerHost
INT iOTPartToMove = -1

CONST_INT ciTimerSpectatorDelay	1000
SCRIPT_TIMER timerSpectatorDelay

SCRIPT_TIMER tdresultstimer
SCRIPT_TIMER tdsafejointimer
SCRIPT_TIMER tdeomwaittimer
SCRIPT_TIMER tdSafeJoinVariations

BOOL bIsInMagnetRange = FALSE
BOOL bMegaMagnetOn = FALSE

INT ioldwanted
INT iWantedbeforeDeath
SCRIPT_TIMER tdWantedDifficultyTimer
INT ioldCarryLimit = -2
INT iOldLoc = -1
INT iOldVeh = -1

INT iIntroState = -1

INT iClearedTasksForHover[FMMC_MAX_PEDS_BITSET]

WEAPON_TYPE wtWeaponToRemove
INT iDlcWeaponBitSet

INT SoundIDCargoBob = -1
INT iStartCargoBobSound = -1
STRING sCargoBobAudioScene

SCRIPT_TIMER maxAccelerationUpdateTimers[FMMC_MAX_PEDS]
FLOAT maxAccelerationCurrentSpeed[FMMC_MAX_PEDS]

// Artificial lights.
ENUM eARTIFICIAL_LIGHTS_STATE
	eARTIFICALLIGHTSSTATE_ON = 0,
	eARTIFICALLIGHTSSTATE_WAITING_TO_TURN_OFF,
	eARTIFICALLIGHTSSTATE_OFF
ENDENUM

STRUCT STRUCT_ATRICIFICIAL_LIGHTS_DATA
	eARTIFICIAL_LIGHTS_STATE eState
	SCRIPT_TIMER sDelayTimer
	INT iDelayDuration
ENDSTRUCT

ENUM eAUDIO_MIXES
	AUDIO_MIX_DEFAULT = 0,
	AUDIO_MIX_GUNFIGHT,
	AUDIO_MIX_CAR_CHASE,
	AUDIO_MIX_DRIVING,
	AUDIO_MIX_NONE,
	
	MAX_AUDIO_MIXES
ENDENUM

CONST_INT ciRUGBY_SOUND_COLLECTED_BALL		0
CONST_INT ciRUGBY_SOUND_DROPPED_BALL		1
CONST_INT ciRUGBY_SOUND_SCORED				2

INT iRugbyCarryingBallSoundID = -1

CONST_INT ciIN_AND_OUT_SOUND_PICKUP			0
CONST_INT ciIN_AND_OUT_SOUND_DROP			1
CONST_INT ciIN_AND_OUT_SOUND_DELIVERED		2

CONST_INT ciSUDDEN_DEATH_REASON_KILLS		0
CONST_INT ciSUDDEN_DEATH_REASON_POINTS		1
CONST_INT ciSUDDEN_DEATH_REASON_DEATHS		2
CONST_INT ciSUDDEN_DEATH_REASON_HSHOTS		3

CONST_INT ciRANDOM_INDEX_ACCURACY			1001

INT iVehCapTeam = -1

INT iLocalAudioMixPriority = -1
INT iLocalCurrentAudioMix = -1

INT iLocalSpectateTarget = -1	

INT iPPBullsharkSoundID = -1
INT iPPBeastmodeSoundID = -1
INT iPPThermalSoundID = -1
INT iPPSwapSoundID = -1
INT iPPFilterSoundID = -1
INT iPPBulletTimeSoundID = -1
INT iPPHideBlipsSoundID = -1

SCRIPT_TIMER stPPBullsharkTimer
SCRIPT_TIMER stPPBeastmodeTimer
SCRIPT_TIMER stPPThermalTimer
SCRIPT_TIMER stPPSwapTimer
SCRIPT_TIMER stPPFilterTimer
SCRIPT_TIMER stPPBulletTimeTimer
SCRIPT_TIMER stPPHideBlipTimer

STRING sPPBullsharkSoundSet
STRING sPPBeastmodeSoundSet
STRING sPPThermalSoundSet
STRING sPPSwapSoundSet
STRING sPPFilterSoundSet
STRING sPPBulletTimeSoundSet
STRING sPPHideBlipsSoundSet

STRING sPPBullsharkSceneSoundSet
STRING sPPBeastmodeSceneSoundSet
STRING sPPThermalSceneSoundSet
STRING sPPSwapSceneSoundSet
STRING sPPFilterSceneSoundSet
STRING sPPBulletTimeSceneSoundSet
STRING sPPHideBlipsSceneSoundSet

STRING sVVRocketsSoundSet
STRING sVVBeastmodeSoundSet
STRING sVVBeastmodeSoundSet1
STRING sVVGhostSoundSet
STRING sVVGhostSoundSet1
STRING sVVForcedAccSoundSet
STRING sVVForcedAccSoundSet1
STRING sVVFlippedSoundSet
STRING sVVFlippedSoundSet1
STRING sVVFlippedSoundSet2
STRING sVVFlippedSoundSet3
STRING sVVZonedSoundSet
STRING sVVDetonateSoundSet
STRING sVVBombSoundSet
STRING sVVPronSoundSet
STRING sVVPronSoundSet1
STRING sVVPronSoundSet2
STRING sVVPronSoundSet3
STRING sVVRepairSoundSet
STRING sVVSpeedBoostSoundSet
STRING sVVRuinerSoundSet
STRING sVVRuinerSoundSet1
STRING sVVRuinerSoundSet2
STRING sVVRuinerSoundSet3
STRING sVVRampSoundSet
STRING sVVRampSoundSet1
STRING sVVRampSoundSet2
STRING sVVRampSoundSet3

STRING sVVRocketsSceneSoundSet
STRING sVVBeastmodeSceneSoundSet
STRING sVVGhostSceneSoundSet
STRING sVVForcedAccSceneSoundSet
STRING sVVFlippedSceneSoundSet
STRING sVVZonedSceneSoundSet
STRING sVVDetonateSceneSoundSet
STRING sVVBombSceneSoundSet
STRING sVVPronSceneSoundSet
STRING sVVRepairSceneSoundSet
STRING sVVSpeedBoostSceneSoundSet
/*STRING sVVRuinerSceneSoundSet
STRING sVVRampSceneSoundSet*/

//INT bsPlayersWithDetonateActive
INT bsPlayersWithBombActive

//STRING sVVBeastmodeSceneSoundSet1
//STRING sVVGhostSceneSoundSet1
//STRING sVVForcedAccSceneSoundSet1
//STRING sVVFlippedSceneSoundSet1
//STRING sVVPronSceneSoundSet1

INT iVVFlippedPartSender = -1
INT iVVFlippedPartSender1 = -1
INT iVVFlippedPartSender2 = -1
INT iVVFlippedPartSender3 = -1
INT iVVPronPartSender = -1
INT iVVPronPartSender1 = -1
INT iVVPronPartSender2 = -1
INT iVVPronPartSender3 = -1
INT iVVRuinerPartSender = -1
INT iVVRuinerPartSender1 = -1
INT iVVRuinerPartSender2 = -1
INT iVVRuinerPartSender3 = -1
INT iVVRampPartSender = -1
INT iVVRampPartSender1 = -1
INT iVVRampPartSender2 = -1
INT iVVRampPartSender3 = -1

INT iVVRocketsSoundID = -1
INT iVVBeastmodeSoundID = -1
INT iVVBeastmodeSoundID1 = -1
INT iVVGhostSoundID = -1
INT iVVGhostSoundID1 = -1
INT iVVForcedAccSoundID = -1
INT iVVForcedAccSoundID1 = -1
INT iVVFlippedSoundID = -1
INT iVVFlippedSoundID1 = -1
INT iVVFlippedSoundID2 = -1
INT iVVFlippedSoundID3 = -1
INT iVVZonedSoundID = -1
INT iVVDetonateSoundID = -1
INT iVVBombSoundID = -1
INT iVVPronSoundID = -1
INT iVVPronSoundID1 = -1
INT iVVPronSoundID2 = -1
INT iVVPronSoundID3 = -1
INT iVVRepairSoundID = -1
INT iVVSpeedBoostID = -1
INT iDTBBombLength = -1
INT iDTBBombMax = -1
INT iDTBExtraLife = -1
/*INT iVVRuinerSoundID = -1
INT iVVRuinerSoundID1 = -1
INT iVVRuinerSoundID2 = -1
INT iVVRuinerSoundID3 = -1
INT iVVRampSoundID = -1
INT iVVRampSoundID1 = -1
INT iVVRampSoundID2 = -1
INT iVVRampSoundID3 = -1*/

TEXT_LABEL_15 tl15PPTeamIntroRoot
TEXT_LABEL_15 tl15PPPreIntroRoot
TEXT_LABEL_15 tl15PPSuddendeathRoot
TEXT_LABEL_15 tl15PPWinnerRoot
TEXT_LABEL_15 tl15PPExitRoot

CONST_INT ciPP_SUDDDEATH_WAIT_FOR_HOST_DELAY	30
INT iPPSuddenDeathWaitForEvent = 0

CONST_INT	ciPP_PRE_INTRO_ROOT_NEUT1			0
CONST_INT	ciPP_PRE_INTRO_ROOT_NEUT2			1
CONST_INT	ciPP_PRE_INTRO_ROOT_NEUT3			2
CONST_INT	ciPP_PRE_INTRO_ROOT_NEUT4			3
CONST_INT	ciPP_PRE_INTRO_ROOT_NEUT5			4
CONST_INT	ciPP_PRE_INTRO_ROOT_NEUT6			5
CONST_INT	ciPP_PRE_INTRO_ROOT_NEUT7			6
CONST_INT	ciPP_PRE_INTRO_ROOT_NEUT8			8
CONST_INT	ciPP_PRE_INTRO_ROOT_NEUT9			9
CONST_INT	ciPP_PRE_INTRO_ROOT_NEUT10			10
												
CONST_INT	ciPP_TEAM_INTRO_ROOT_NCVB			11
CONST_INT	ciPP_TEAM_INTRO_ROOT_NBVC			12
CONST_INT	ciPP_TEAM_INTRO_ROOT_TVK			13
CONST_INT	ciPP_TEAM_INTRO_ROOT_KVT			14
CONST_INT	ciPP_TEAM_INTRO_ROOT_RVS			15
CONST_INT	ciPP_TEAM_INTRO_ROOT_SVR			16
CONST_INT	ciPP_TEAM_INTRO_ROOT_MVA			17
CONST_INT	ciPP_TEAM_INTRO_ROOT_AVM			18	

CONST_INT	ciPP_TEAM_INTRO_ROOT_STARG1			19
CONST_INT	ciPP_TEAM_INTRO_ROOT_STARG2			20
CONST_INT	ciPP_TEAM_INTRO_ROOT_STARG3			21
CONST_INT	ciPP_TEAM_INTRO_ROOT_STARG4			22
CONST_INT	ciPP_TEAM_INTRO_ROOT_STARG5			23	

CONST_INT	ciPP_SUDDEN_DEATH_ROOT_1			24
CONST_INT	ciPP_SUDDEN_DEATH_ROOT_2			25
CONST_INT	ciPP_SUDDEN_DEATH_ROOT_3			26
CONST_INT	ciPP_SUDDEN_DEATH_ROOT_4			27	

CONST_INT	ciPP_WINNER_ROOT_COCKS_1			28
CONST_INT	ciPP_WINNER_ROOT_COCKS_2			29
CONST_INT	ciPP_WINNER_ROOT_COCKS_3			30

CONST_INT	ciPP_WINNER_ROOT_BOARS_1			31
CONST_INT	ciPP_WINNER_ROOT_BOARS_2			32
CONST_INT	ciPP_WINNER_ROOT_BOARS_3			33	

CONST_INT	ciPP_WINNER_ROOT_TOXIC_1			34
CONST_INT	ciPP_WINNER_ROOT_TOXIC_2			35
CONST_INT	ciPP_WINNER_ROOT_TOXIC_3			36	

CONST_INT	ciPP_WINNER_ROOT_KILLER_1			37
CONST_INT	ciPP_WINNER_ROOT_KILLER_2			38
CONST_INT	ciPP_WINNER_ROOT_KILLER_3			39	

CONST_INT	ciPP_WINNER_ROOT_MUNCHIES_1			40
CONST_INT	ciPP_WINNER_ROOT_MUNCHIES_2			41
CONST_INT	ciPP_WINNER_ROOT_MUNCHIES_3			42

CONST_INT	ciPP_WINNER_ROOT_ANIMALS_1			43
CONST_INT	ciPP_WINNER_ROOT_ANIMALS_2			44
CONST_INT	ciPP_WINNER_ROOT_ANIMALS_3			45

CONST_INT	ciPP_WINNER_ROOT_RAINBOW_1			46
CONST_INT	ciPP_WINNER_ROOT_RAINBOW_2			47
CONST_INT	ciPP_WINNER_ROOT_RAINBOW_3			48

CONST_INT	ciPP_WINNER_ROOT_SHADE_1			49
CONST_INT	ciPP_WINNER_ROOT_SHADE_2			50
CONST_INT	ciPP_WINNER_ROOT_SHADE_3			51

CONST_INT	ciPP_WINNER_ROOT_GENERAL_1			52
CONST_INT	ciPP_WINNER_ROOT_GENERAL_2			53
CONST_INT	ciPP_WINNER_ROOT_GENERAL_3			54
CONST_INT	ciPP_WINNER_ROOT_GENERAL_4			55
CONST_INT	ciPP_WINNER_ROOT_GENERAL_5			56

CONST_INT	ciPP_TIE_ROOT_1						57				
CONST_INT	ciPP_TIE_ROOT_2						58
CONST_INT	ciPP_TIE_ROOT_3						59
CONST_INT	ciPP_TIE_ROOT_4						60
CONST_INT	ciPP_TIE_ROOT_5						61

CONST_INT	ciPP_EXIT_ROOT_1					62
CONST_INT	ciPP_EXIT_ROOT_2					63
CONST_INT	ciPP_EXIT_ROOT_3					64
CONST_INT	ciPP_EXIT_ROOT_4					65
CONST_INT	ciPP_EXIT_ROOT_5					66
CONST_INT	ciPP_EXIT_ROOT_6					67
CONST_INT	ciPP_EXIT_ROOT_7					68
CONST_INT	ciPP_EXIT_ROOT_8					69
CONST_INT	ciPP_EXIT_ROOT_9					70
CONST_INT	ciPP_EXIT_ROOT_10					71
CONST_INT	ciPP_EXIT_ROOT_11					72
CONST_INT	ciPP_EXIT_ROOT_12					73
CONST_INT	ciPP_EXIT_ROOT_13					74


//crate pickup info
INT iLocalCrateBrokenBitset
SAP_PICKUP_ID piCratePickup[FMMC_MAX_NUM_OBJECTS][ciMAX_CRATE_PICKUPS]
//INT iCratePickupMoveCounter[FMMC_MAX_NUM_OBJECTS]

SC_LEADERBOARD_CONTROL_STRUCT scLB_control
//SC_LEADERBOARD_CONTROL_STRUCT scLB_controlFriend
//UGCStateUpdate_Data FriendStateUpdateData
//BOOL bHaveFriends
SCALEFORM_INDEX	scLB_scaleformID
DPAD_VARS dpadVars
DPAD_VARS celebdpadVars
SCALEFORM_INDEX scaleformDpadMovie
//TWEAK_FLOAT SDPAD_X 							0.18
//TWEAK_FLOAT SDPAD_Y 							0.377
//TWEAK_FLOAT SDPAD_H 							0.6
//TWEAK_FLOAT SDPAD_W 							0.28
 
SCRIPT_TIMER tdovertimer
SCRIPT_TIMER tdForceEndTimer
SCRIPT_TIMER tdwastedtimer
SCRIPT_TIMER tHintCam

SCRIPT_TIMER timerManualRespawn
SCRIPT_TIMER tdForceVehicleRespawnTimer
SCRIPT_TIMER tdForceVehicleRespawnInWaterTimer
SCRIPT_TIMER tdForceUndriveableVehicleRespawnTimer
NETWORK_INDEX netRespawnVehicle

SCRIPT_TIMER tdRespawnTimer
SCRIPT_TIMER tdRespawnFailTimer

SCRIPT_TIMER timerHumLabDoors
SCRIPT_TIMER stSumoMessageDelay 
SCRIPT_TIMER stSuddenDeathDelay 

CONST_INT iHumLabDoorDelay 1000

CONST_INT iVelumDoorOpenDelay 500
CONST_INT iVelumDoorCloseDelay 2000
SCRIPT_TIMER tdVelumDoorDelayTimer

INT iMissionResult = ciFMMC_END_OF_MISSION_STATUS_NOT_SET

STUNTJUMP_ID sjsID[FMMC_MAX_NUM_STUNTJUMPS]
INT iStuntJumpBitSet

INT iHighKillStreak
INT iStartSuicides

BOOL bIgnoreInteriorCheckForSprinting = FALSE

//PED_WEAPONS_STRUCT structWeaponInfo
INT iPedTagCreated[FMMC_MAX_PEDS_BITSET]
INT iPedTagInitalised[FMMC_MAX_PEDS_BITSET]
INT iPedTag[FMMC_MAX_PEDS]

INT iPedVehWeaponsBlockedBS[FMMC_MAX_PEDS_BITSET]
INT iPedSetupDisguiseOverride[FMMC_MAX_PEDS_BITSET]

INT iPedTrackingDamageBS[FMMC_MAX_PEDS_BITSET]


SCRIPT_TIMER 	pedDronePerceptionBufferTimer
INT 			iPedDroneSpotter 			= -1

INT iPedHitByStunGun[ciFMMC_PED_BITSET_SIZE]

BLIP_INDEX biPedBlip[FMMC_MAX_PEDS]
INT iPedBlipCachedNamePriority[FMMC_MAX_PEDS]
AI_BLIP_STRUCT biHostilePedBlip[FMMC_MAX_PEDS]

BLIP_INDEX biVehBlip[FMMC_MAX_VEHICLES]
INT iVehBlipCachedNamePriority[FMMC_MAX_VEHICLES]
INT iVehBlipCreatedThisFrameBitset
INT iVehPhotoChecksBS

CONST_INT	BIT_FLASH_VEH_BLIP_RESYNC_REQUESTED		0
CONST_INT	BIT_FLASH_VEH_BLIP_FLASHING_STOPPED		1
INT iVehBlipFlashingBitset
INT iVehBlipFlashingState

BLIP_INDEX biObjBlip[FMMC_MAX_NUM_OBJECTS]
INT iObjBlipCachedNamePriority[FMMC_MAX_NUM_OBJECTS]

float vehicle_previous_damage_percentage[FMMC_MAX_VEHICLES]
float vehicle_damage_time[FMMC_MAX_VEHICLES]
int vehicle_safe_damage_bit_set
INT iLocalVehInit
INT iDoorUnlockBitset
INT iCachedDoorHashes[FMMC_MAX_NUM_DOORS]
VECTOR vCachedDoorUnlockPositions[FMMC_MAX_NUM_DOORS]
VECTOR vCachedDoorMiddlePositions[FMMC_MAX_NUM_DOORS]
INT iDirectionalDoorLocallyUnlockedBS
INT iDirectionalDoorShouldUpdateBS
INT iPedsInDirectionalDoorsBS[FMMC_MAX_NUM_DOORS][ciFMMC_PED_BITSET_SIZE]
FLOAT fDirectionalDoorAutoDistance[FMMC_MAX_NUM_DOORS]
SCRIPT_TIMER stDirectionalDoorEventTimer
CONST_INT ciDirectionalDoorEventTimerDuration	350
object_index map_door[FMMC_MAX_RULES]
int door_index_to_update_rotation_on = -1
//model_names fleeca_bank_door_model = HEI_PROP_HEIST_SEC_DOOR

INT	ibootbitset
HUD_COLOURS DeliverColour[FMMC_MAX_NUM_OBJECTS]
INT ipackageSlot
INT iPackageHUDBitset
BLIP_INDEX biPickup[FMMC_MAX_WEAPONS]
BLIP_INDEX LocBlip[FMMC_MAX_GO_TO_LOCATIONS]
BLIP_INDEX LocBlip1[FMMC_MAX_GO_TO_LOCATIONS]
INT ioldLocOwner[FMMC_MAX_GO_TO_LOCATIONS]
INT iBlipTeam[FMMC_MAX_GO_TO_LOCATIONS]

INT iSuddenDeath_PlayerLives = -1

CHECKPOINT_INDEX ciLocCheckpoint[FMMC_MAX_GO_TO_LOCATIONS]
SHAPETEST_INDEX stiCheckpoint[FMMC_MAX_GO_TO_LOCATIONS]
INT bsRepositionCheckpoint = 0
INT bsCheckPointCreated = 0
INT iCheckpointBS

BLIP_INDEX biModshopBlip
BLIP_INDEX DeliveryBlip
INT iDeliveryBlip_veh = -1
BLIP_INDEX HomeBlip[FMMC_MAX_TEAMS]
INT iDelblipPriority

BOOL bonImpromptuRace
//INT iOldCarryCount

INT iCustomPlayerBlipSetBS

GROUP_INDEX giPlayerGroup 
VECTOR vWarpPos
FLOAT ftempStartHeading

VECTOR vCrashSoundPos

INT FlashingPlayerBlip
BOOL bHasSeenExtractionAppHelpText
VECTOR vExtracAppTargetPosition
VECTOR vExtracAppLocalPosition
PLAYER_INDEX vExtracAppClosestPlayer
FLOAT fExtracClosestDistance = 999999999

INT LocalRandomSpawnBitset[4]
INT iTeamPropSpawns

BOOL bFilterIsOn
INT iFilterBS = 0
INT iActiveFilter
INT iCachedFilter

BLIP_INDEX DestinationBlip

//Sniperball
INT bsHasObjExploded
INT iExplodeCountdownSound
INT iExplodeLocalTimer

INT iStoredNoWeaponZone
INT iSniperBallObj
OBJECT_INDEX oiSniperBallPackage

//Trading Places Local Variables
STRUCT sTimeBarDisplay
	INT iTimeBarCurrent = -1
	PLAYER_INDEX playerIndex
ENDSTRUCT
sTimeBarDisplay sTimeBarSorted[8]	//4 is max...
INT iTradingPlacesTimeBarLastTeam = -1
INT iWinningPlayerHUD
INT iTradingPlacesBitSet
INT iNumberOfActivePlayersLast = -1
CONST_INT ciTradingPlacesTimeBarSet				0
CONST_INT ciTradingPlacesWinningPlayerFound		1
CONST_INT ciTradingPlacesWinningPlayerFoundLast	2
CONST_INT ciTradingPlacesDisplayToggle			3
CONST_INT ciTradingPlacesFinalTeamSwap			4
CONST_INT ciTradingPlacesFirstTeamSwapHelp		5
CONST_INT ciTradingPlacesHelpStartPart1			6
CONST_INT ciTradingPlacesHelpStartPart2			7
CONST_INT ciTradingPlacesHelpStartPart3			8
CONST_INT ciTradingPlacesHelpWinningTeamTrigger	9
CONST_INT ciTradingPlacesHelpWinningTeam		10
CONST_INT ciTradingPlacesHelpLosingTeamTrigger	11
CONST_INT ciTradingPlacesHelpLosingTeam			12
CONST_INT ciTradingPlacesHelpSuicideTrigger		13
CONST_INT ciTradingPlacesHelpRemixAbilities		14
CONST_INT ciTradingPlacesHelpRemixKnockBack		15

BOOL bTakedownDamageModifier = FALSE

// This is used to keep track of which sounds have triggered in the mission
INT iSoundCompletedBitset

INT isoundid[FMMC_MAX_NUM_PROPS]
SCRIPT_TIMER AlarmTimer[FMMC_MAX_NUM_PROPS]
PICKUP_INDEX pipickup[FMMC_MAX_WEAPONS]
OBJECT_INDEX oiProps[FMMC_MAX_NUM_PROPS]
OBJECT_INDEX oiPropsChildren[FMMC_MAX_NUM_PROP_CHILDREN]
OBJECT_INDEX oiStairCollision
BOOL bRAGStopDrain
INT iGetDeliverMax = -1
#IF IS_DEBUG_BUILD
BOOL bSlipstreamRespawnDebug
#ENDIF
INT TeamChangeTracker[MAX_NUM_MC_PLAYERS]
INT iTeamLivesHUD[4]

//Bitset for any one off or special case bools you might need
CONST_INT HasSeenMaxCapHelptext		0
CONST_INT HasBeastHelpFinished		1
CONST_INT AreAllObjectsSorted		2

INT AssortedBoolBitset

INT iObjPaintingForceLeftHandBS
INT iObjPaintingForceRightHandBS

CONST_INT NumberOfObjs		12
INT NeedToMoveObj
INT iCapPointChecked
INT iNumObjsChecked
INT iNeedsShapeTest
INT iCheckObjs
SHAPETEST_INDEX	siObjLOS[NumberOfObjs]

NETWORK_INDEX niBlendVeh[MAX_NUM_MC_PLAYERS]
INT	BlendBoolBitset

ped_index monkey_ped[ciMAX_MONKEYPEDS]
ped_index monkey_sound_ped
sequence_index monkey_seq
INT iMonkeyPedCount
int monkey_sound = get_sound_id()
script_timer monkey_time

enum monkey_anims_enum
	play_normal_anims = 0,
	play_freaked_out_anims, 
	blend_back_to_normal_anims
endenum

monkey_anims_enum monkey_anim_status[ciMAX_MONKEYPEDS]
	
INT iCrashSound = GET_SOUND_ID()
INT iCrashSoundProp = -1

INT iPropCleanedupBS[FMMC_MAX_PROP_BITSET]
INT iPropRespawnNowBS[FMMC_MAX_PROP_BITSET]
INT iPropCleanedupTriggeredBS[FMMC_MAX_PROP_BITSET]
INT iPropDetonateTriggeredBS[FMMC_MAX_PROP_BITSET]
INT iGarageDoorSoundID = -1
LEGACY_RUNTIME_WORLD_PROP_DATA sRuntimeWorldPropData
object_index zip_tie_obj

INT iLocalDriverPart[FMMC_MAX_VEHICLES]
INT inumfreevehicles
INT inumfreeseats
INT iAHighPriorityVehicle = -1//number of some high priority vehicle
INT iFirstHighPriorityVehicleThisRule[FMMC_MAX_TEAMS]
INT iOldClientStageObjText
INT iVehJustIn = -1

INT iOldVehDestroyBS
INT iOldHackTargetsRemaining
INT iOldVehFollowing
INT iOldHackingVehInRange

FLOAT fDist2ToObjective[FMMC_MAX_TEAMS]
//FLOAT fDist2FromLastSpawn[FMMC_MAX_TEAMS]

INT iRolePlayers[FMMC_MAX_TEAMS][FMMC_MAX_ROLES]

INT iPedSpawnFailCount[FMMC_MAX_PEDS]
INT iPedSpawnFailDelay[FMMC_MAX_PEDS]
INT iPedVehStoppedTimer[FMMC_MAX_PEDS]
INT iPedScratchVehTimer[FMMC_MAX_PEDS]
INT iPedBlockedBitset[FMMC_MAX_PEDS_BITSET]

INT iVehSpawnFailCount[FMMC_MAX_VEHICLES]
INT iVehSpawnFailDelay[FMMC_MAX_VEHICLES]

//For use with DRAW_FMMC_PLAYERS_REMAINING_HUD, caches last frames amount of alive players
INT iLastHUDPlayerLivesCount[FMMC_MAX_TEAMS]

VEHICLE_INDEX DeliveryVeh
NETWORK_INDEX OverwriteObjectiveNetID
SCRIPT_TIMER tdDhandlerTimer
SEQUENCE_INDEX temp_sequence

INT iLocalLeaderPart[FMMC_MAX_PEDS]
INT inumfreePeds
SCRIPT_TIMER tdparkTimer[FMMC_MAX_PEDS]
SCRIPT_TIMER tdPathFailCombat[FMMC_MAX_PEDS]
VEHICLE_SEAT targetseat[FMMC_MAX_PEDS]


INT iLocalCarrierPart[FMMC_MAX_NUM_OBJECTS]
INT iLocalPackageHolding[FMMC_MAX_NUM_OBJECTS]
INT iObjectBlipAlpha[FMMC_MAX_NUM_OBJECTS]
INT iCantCollectHelpDisp
INT inumfreeObjects
INT iBusySyncLockMinigamesBS
INT iAICarryBitset
INT iAIAttachedBitset
//Store for player female feet
INT iPedFeetDrawable = -1
INT iPedFeetTexture = - 1

INT iLastSpectatedTarget = -1
INT iLastPartToUse = -1

// the minimum time a mission must last for to get any xp/cash
CONST_INT ciMIN_REWARD_TIME 30000
CONST_INT ciMIN_REWARD_TIME_1_MIN 60000
CONST_INT ciMIN_REWARD_TIME_2_MIN 120000
CONST_INT ciMIN_REWARD_TIME_3_MIN 180000
CONST_INT ciMIN_REWARD_TIME_4_MIN 240000
CONST_INT ciMIN_REWARD_TIME_5_MIN 300000
CONST_INT ciMIN_REWARD_TIME_6_MIN 360000
CONST_INT ciMIN_REWARD_TIME_7_MIN 420000
CONST_INT ciMIN_REWARD_TIME_750_MIN 450000
CONST_INT ciMIN_REWARD_TIME_8_MIN  480000
CONST_INT ciMIN_REWARD_TIME_10_MIN 600000
CONST_INT ciMIN_REWARD_TIME_12_MIN 720000
CONST_INT ciMIN_REWARD_TIME_15_MIN 900000

CONST_INT ciFMMC_UNLIMITED_LIVES -1


CONST_INT 	ciNUM_PLAYERS_PER_AIRCRAFT_SPAWN		4
CONST_FLOAT cfDISTANCE_BETWEEN_PLAYERS				1.4

CONST_INT	ciCARGOBOB_RETRY_TEST_LIMIT				100
INT			iTestCargobobExistsCount = 1

//ped logic variables
INT iPedArrivedBitset[FMMC_MAX_PEDS_BITSET]
INT iPedChangeTaskBitset[FMMC_MAX_PEDS_BITSET]
CONST_INT ciPED_ARRIVE_GOTO_RANGE 80
CONST_INT ciPED_LEAVE_GOTO_RANGE 140
CONST_INT ciPED_BOAT_HELI_ARRIVE_GOTO_RANGE 150
CONST_INT ciPED_BOAT_HELI_LEAVE_GOTO_RANGE 250
CONST_INT ciPED_PLANE_ARRIVE_GOTO_RANGE 250
CONST_INT ciPED_PLANE_LEAVE_GOTO_RANGE 299

INT iObjectiveMaxPickup
INT	iHUDPriority = -1
INT iOldSpawnPriority

// photo rule variables
INT iPhotoTrackedPoint = -1

INT iVehPhotoTrackedPoint[FMMC_MAX_VEHICLES]
INT iVehPhotoTrackedPoint2[FMMC_MAX_VEHICLES]
VECTOR vVehPhotoCoords[FMMC_MAX_VEHICLES]

VECTOR vLocalPhotoCoords
CONST_INT ci_PHOTO_RANGE  40
CONST_INT ci_CHARM_RANGE  10
CONST_INT ci_CHARM_EXIT_RANGE  12
CONST_INT ci_PHOTO_RANGE_VEH 40
CONST_INT ci_PHOTO_RANGE_VEH_NUMPLATE 30
CONST_FLOAT cf_PHOTO_NUMPLATE_MAXANGLE 70.0

//dancing data
//S_DANCING_DATA sDancingData
CROWD_CONTROL_LOCAL_DATA 			sCCLocalData									// Local minigame data
CROWD_CONTROL_LOCAL_PED_DATA 		sCCLocalPedData[FMMC_MAX_CROWD_PEDS]			// Local per ped data
CROWD_CONTROL_PED_DECORATOR_DATA	sCCLocalPedDecorCopy[FMMC_MAX_CROWD_PEDS]		// Per ped data (local version)
CUTSCENE_PED_VARIATION				sCrowdControlCashierVariation[2]
PED_COMP_NAME_ENUM eHairToRestore

	S_CHARM_MINIGAME_DATA sCharmMinigameData

ENUM SUMO_SUDDEN_DEATH_STAGE_BD
	eSSDS_TIMER_GET_INSIDE_SPHERE,
	eSSDS_INSIDE_SPHERE,
	eSSDS_PUSHED_OUT_OF_SPHERE
ENDENUM

//Arena Spawn Fix
VECTOR vArenaSpawnVec
FLOAT fArenaSpawnHead
ENUM eArenaSpawnFixStage
	eArenaSpawnFix_Idle = 0,
	eArenaSpawnFix_FadeOut = 1,
	eArenaSpawnFix_Warp = 2,
	eArenaSpawnFix_Wait = 3,
	eArenaSpawnFix_Sync = 4,
	eArenaSpawnFix_FadeIn = 5,
	eArenaSpawnFix_Progress = 6
ENDENUM
SCRIPT_TIMER tdArenaSpawnFixTimer
SCRIPT_TIMER tdArenaSpawnFixSafety
SCRIPT_TIMER tdArenaSpawnFadeInTimer

//hacking data
S_HACKING_DATA sHackingData

	S_DRILL_DATA sDrillData
	S_VAULT_DRILL_DATA sVaultDrillData
	S_CASH_GRAB_DATA sCashGrabData
	//INT iSyncSceneThermite = -1
	INT iTimeStoppedCoverEyesAnim = 0
	INT iTimeWeaponReturnedAfterThermite = 0
	INT iThermiteBitset = 0
	
	CONST_INT ciMAX_LOCAL_THERMITE_PTFX  4
	INT iThermiteLocalBitset[ciMAX_LOCAL_THERMITE_PTFX]
	INT iThermiteLocalSparkTimer[ciMAX_LOCAL_THERMITE_PTFX]
	INT iThermiteLocalDripTimer[ciMAX_LOCAL_THERMITE_PTFX]
	INT iThermiteRemoteBagFadeTimer[ciMAX_LOCAL_THERMITE_PTFX]
	
	INT iThermiteSmokeEffectTimer = 0
	INT iLastThermiteDoorRule = 0
	INT iLastThermiteDoorID = 0
	INT iThermiteBagStage = 0
	INT iThermiteBagReturnStage = 0
	INT iThermiteBagFadeTimer = 0
	INT iThermiteKeypadInRangeBS = 0
	INT iThermiteKeypadWasLastTeamCharge[FMMC_MAX_TEAMS]
	INT iThermiteKeypadWasLastPlayerCharge
	INT iThermiteKeypadIsProcessing
	
	MP_OUTFITS_APPLY_DATA   sApplyDataPacificBankCut1
	MP_OUTFITS_APPLY_DATA   sApplyDataPacificBankCut2
	MP_OUTFITS_APPLY_DATA   sApplyDataPacificBankCut3
	MP_OUTFITS_APPLY_DATA   sApplyDataPacificBankCut4
	MP_OUTFITS_APPLY_DATA   sApplyDataPacificBankCut5
	
	BOOL bDrawCashGrabTakeRed
	INT iDrawCashGrabTakeRedTime
	  
	CONST_INT THERMITE_STATE_INITIALISE				0
	CONST_INT THERMITE_STATE_WAITING_FOR_ASSETS		1
	CONST_INT THERMITE_STATE_START_ANIM				2
	CONST_INT THERMITE_STATE_REMOVE_BAG				3
	CONST_INT THERMITE_STATE_WAIT_FOR_ANIM			4
	CONST_INT THERMITE_STATE_DO_BAG_SWAP			5
	CONST_INT THERMITE_STATE_DO_DOOR_SWAP			6
	CONST_INT THERMITE_STATE_WAIT_FOR_EXPLOSION		7
	CONST_INT THERMITE_STATE_CLEANUP				8
	CONST_INT THERMITE_STATE_FAIL_CLEANUP			9
	CONST_INT THERMAL_CHARGE_DURATION				12000
	
	CONST_INT THERMITE_BITSET_IS_COVER_EYES_ANIM_PLAYING		0
	CONST_INT THERMITE_BITSET_HAS_DOOR_SOUND_TRIGGERED			1
	CONST_INT THERMITE_BITSET_IS_THERMITE_BURNING				2
	CONST_INT THERMITE_BITSET_WALK_TASK_GIVEN_EARLY				3
	CONST_INT THERMITE_BITSET_TRIGGERED_ANIMS_EARLY				4
	CONST_INT THERMITE_BITSET_SET_THERMITE_OBJECT_VISIBLE		5
	CONST_INT THERMITE_BITSET_SET_THERMITE_OBJECT_TO_FLASH		6
	CONST_INT THERMITE_BITSET_MOCAP_TRIGGERED_DURING_ANIM		7
	CONST_INT THERMITE_BITSET_IS_COVER_EYES_INTRO_PLAYING		8
	CONST_INT THERMITE_BITSET_FORCED_ANIM_UPDATE_LAST_FRAME		9

	CONST_INT THERMITE_LOCAL_BITSET_CREATE_LOCAL_THERMITE_PTFX		0
	CONST_INT THERMITE_LOCAL_BITSET_START_LOCAL_SPARK_DIE_OFF_EVO	1
	CONST_INT THERMITE_LOCAL_BITSET_REMOTE_BAG_OBJECT_HIDDEN		2
	CONST_INT THERMITE_LOCAL_BITSET_REMOTE_BAG_COMPONENT_REMOVED	3
	CONST_INT THERMITE_LOCAL_BITSET_REMOTE_BAG_OBJECT_REVEALED		4
	CONST_INT THERMITE_LOCAL_BITSET_REMOTE_BAG_COMPONENT_RESTORED	5
	CONST_INT THERMITE_LOCAL_BITSET_IS_THERMITE_BURNING				6
	CONST_INT THERMITE_LOCAL_BITSET_IS_ANY_THERMITE_PROCESSING		7
	
	CONST_INT THERMITE_BAG_STAGE_ANIM_NOT_STARTED			0
	CONST_INT THERMITE_BAG_STAGE_PLAYERS_BAG_VISIBLE		1
	CONST_INT THERMITE_BAG_STAGE_OBJECT_BAG_INVISIBLE		2
	CONST_INT THERMITE_BAG_STAGE_OBJECT_BAG_VISIBLE			3
	
	CONST_INT THERMITE_BAG_RETURN_STAGE_ANIM_NOT_STARTED			0
	CONST_INT THERMITE_BAG_RETURN_STAGE_WAITING_FOR_ANIM			1
	CONST_INT THERMITE_BAG_RETURN_STAGE_WAITING_FOR_COMPONENT_BAG	2
	CONST_INT THERMITE_BAG_RETURN_STAGE_SWAP_COMPLETE				3
	
	CONST_INT THERMITE_DRIP_EFFECT_INDEX					0
	CONST_INT THERMITE_SPARK_EFFECT_INDEX					1
	
	VECTOR vThermitePlaybackPos = <<0.0, 0.0, 0.0>>
	VECTOR vThermiteGotoPoint = <<0.0, 0.0, 0.0>>
	VECTOR vThermiteSceneOffsetFromObj = <<-0.02, -0.05, -0.08>>
	VECTOR vThermiteSparkEffectOffset = vThermiteSceneOffsetFromObj + <<0.0, 1.0, 0.0>> //2099588 - To fix rendering issues with the effect it has an offset applied that needs to be accounted for in the script.
	VECTOR vThermiteDripEffectOffset = vThermiteSceneOffsetFromObj
	VECTOR vThermitePosWhenPlaced = <<0.0, 0.0, 0.0>>
	VECTOR vThermiteRotWhenPlaced = <<0.0, 0.0, 0.0>>
	VECTOR vThermiteLocalDripOffset[ciMAX_LOCAL_THERMITE_PTFX]
	VECTOR vThermiteLocalSparkOffset[ciMAX_LOCAL_THERMITE_PTFX]

	OBJECT_INDEX oiLocalThermiteTarget[ciMAX_LOCAL_THERMITE_PTFX]

	ENUM ePropertyDropoffType
		eApartmentBuzzerEntrance = 0,
		eGarageExterior
	ENDENUM
	
	INT i_Pri_Sta_IG2_AudioStreamPedID = -1
	
INT iHackProgress
INT ilocalHackdoorbitset
INT iLocalHackDoorBitset2
INT iLocalHackDoorThermiteBitset
INT iLocalHackDoorThermiteFailedBitset
int iLocal_unlock_thermite_door_bitset
int iLocal_force_open_hack_door_thermite_bitset
INT iHackDoorID[FMMC_MAX_RULES]
INT iHackDoorID2[FMMC_MAX_RULES]
INT iDoor1Swingfree
INT iDoor2Swingfree
INT iHackLimitTimer
FLOAT fHackDoorOpenRatio[FMMC_MAX_RULES]
FLOAT fHackDoor2OpenRatio[FMMC_MAX_RULES]
FLOAT fHackDoorStartHeading[FMMC_MAX_RULES]
float current_door_heading_addition[FMMC_MAX_RULES]

WEAPON_TYPE eWeaponBeforeMinigame = WEAPONTYPE_UNARMED

//Hacking variables moved here from FM_Mission_Controller_Minigame
BOOL b_cibsLEGACY_FMMC_MINI_GAME_IGNORE_CONNECT
BOOL b_cibsLEGACY_FMMC_MINI_GAME_IGNORE_BRUTE
BOOL b_cibsLEGACY_FMMC_MINI_GAME_DIFF_EASY
BOOL b_cibsLEGACY_FMMC_MINI_GAME_DIFF_HARD
VECTOR vHackOffset = <<0.0,-0.55,0>>
VECTOR vHackAAOffset = <<0.0,-1.8,0>>
VECTOR vHackLaptopGotoOffset = <<-0.060,-0.7,0>> //<<-0.39,-0.7,0>>
VECTOR vHackKeypadGotoOffset = <<-0.02, -0.445, 0>> //<<-0.130, -0.290, -0.64>>
VECTOR vHackKeycardGotoOffset = <<0,-0.75,0>>//<<-0.39,-0.7,0>>
VECTOR vHackLaptopScenePlayback = << -0.100, -0.800, 0.020 >>
VECTOR vHackKeypadScenePlayback = <<-0.02, -0.445, 0>> //<< -0.100, -0.350, -0.600 >>
	VECTOR vHackKeycardScenePlayback = <<0,-0.75,0>>
FLOAT fThisTweakValue1 =  2.0
FLOAT fThisTweakValue2 =  1.5
FLOAT fUseGoToDistance = 0.2
INT iTimeOutTimer
INT iWallPaper
#IF IS_DEBUG_BUILD
	VECTOR vOffsetFromTerminal 
	BOOL bSetUpHackWidgets
#ENDIF
STRING sKeyPadAnim = "anim@heists@keypad@"
STRING sBagAnim
STRING sLaptopHackDict

CONST_FLOAT cfHumaneSyncLock_AreaLength		0.8
CONST_FLOAT cfHumaneSyncLock_AreaWidth		0.65
CONST_FLOAT cfHumaneSyncLock_AreaHeight		3.0

//Sync lock variables moved here from FM_Mission_Controller_Minigame
CONST_INT ci_HUMANE_SYNC_LOCK_TIME 2200
INT iStoreAmountOfFails
VECTOR vThisOffset = <<0,-0.75,0>>
VECTOR vScenePlayback = <<0,-0.75,0>>
STRING sKeyCardDict
STRING sKeyCardAnim
STRING sHSLEnter
STRING sHSLEnterLoop
STRING sHSLExit
STRING sHSLIntro
STRING sHSLLoop
STRING sHSLPass
STRING sHSLFail
VECTOR vHumaneSyncPlayback
FLOAT fZAmount = 0.1

//Drilling/other minigame variables moved here from FM_Mission_Controller_Minigame

OBJECT_INDEX oiCurrentContainer //Stores the container being investigated
BOOL bIsCaseInContainer //If it containers the case obj
INT iThisContainerNumBeingInvestigated = -1 //local object loop value stored
INT iBSContainerBeingInvestigated //Local bit set for player investiging the container
INT iInvestigateContainerState = 0 //Local - player can only investigate one container at a time.

//Bitset for Cleaning Up Vehicles based on rule & distance
INT bsCleanupVehicleRule = 0
INT bsCleanupVehicleDistance = 0

INT iDisconnectedDawnRaidPart = -1

//The Case object
OBJECT_INDEX objDawnPickUp
INT iSoundIDDawnRaidPickup = GET_SOUND_ID()
INT iSpawnCargobobGhostedID = -1

CONST_INT ciHICInit 0
CONST_INT ciHICHaveAssetsLoaded 1
CONST_INT ciHICIsPlayerPlace 2
CONST_INT ciHICHasAnimFinished 3
CONST_INT ciHICCleanUp 4

VECTOR vContainerPlaybackPos
VECTOR vContainerGoToPoint
VECTOR vContainerInitialRot

//might need to work out offset in game
VECTOR vContainerSceneOffsetFromObj = << 0,0,0 >>

CONST_INT MAX_TRACKIFY_TARGETS 20

BOOL bDawnRaidGroundHeightCheckFinished
INT iDawnRaidReset = -1
OBJECT_INDEX objDawnRaidContainer
INT iDawnRaidContainerPart = -1
BOOL bDawnRaidContainerFound

#IF IS_DEBUG_BUILD
INT debug_iDawnRaidPickupCarrierPart = -1
#ENDIF

//INT iTrackifyProgress
INT iTrackifyTargets[MAX_TRACKIFY_TARGETS]
INT iTrackifyTargetProcessed = 0
VECTOR vTrackifyVector[MAX_TRACKIFY_TARGETS]
VEHICLE_INDEX vehTrackifyTarget[MAX_TRACKIFY_TARGETS]
OBJECT_INDEX objTrackifyTarget[MAX_TRACKIFY_TARGETS]

INT iNumberOfTrackifyTargets = 0
INT iAcquiredTargets
INT iTrackifyUpdateTimer

BOOL bTrackObjects 
BOOL bTrackifyDisabled

WEAPON_TYPE wtTrackifyCache = WEAPONTYPE_UNARMED

VECTOR vTargetObj
VECTOR vTrackifyPlayerCoords

BOOL bDawnRaidCratesLightsOn

REL_GROUP_HASH 						relMyFmGroup

VECTOR vGoToPoint
VECTOR vGoToPointOffset
VECTOR vPlayAnimAdvancedPlayback

INT iSafetyTimer
INT iServerSafetyTimer[MAX_TRACKIFY_TARGETS]
BOOL bServerSafetyTimer[MAX_TRACKIFY_TARGETS]

FLOAT fObjRot

OBJECT_INDEX Vault_door[FMMC_MAX_RULES]
OBJECT_INDEX FleecaTellerDoor

vehicle_index prison_break_plane

INT iMapObjectDoorCount
INT iMapObjectDoorRatioSetBitset
INT iMapObjectDoorRatioFirstSetBitset

INT iAudioBankFails

CONST_INT HACK_DOOR_ID1  HASH("RowanCHackDoor1")
CONST_INT HACK_DOOR_ID2  HASH("RowanCHackDoor2")
CONST_INT HACK_DOOR_ID3  HASH("RowanCHackDoor3")
CONST_INT HACK_DOOR_ID4  HASH("RowanCHackDoor4")
CONST_INT HACK_DOOR_ID5  HASH("RowanCHackDoor5")
CONST_INT HACK_DOOR_ID6  HASH("RowanCHackDoor6")
CONST_INT HACK_DOOR_ID7  HASH("RowanCHackDoor7")
CONST_INT HACK_DOOR_ID8  HASH("RowanCHackDoor8")
CONST_INT HACK_DOOR_ID9  HASH("RowanCHackDoor9")
CONST_INT HACK_DOOR_ID10  HASH("RowanCHackDoor10")
CONST_INT HACK_DOOR_ID11  HASH("RowanCHackDoor11")
CONST_INT HACK_DOOR_ID12  HASH("RowanCHackDoor12")
CONST_INT HACK_DOOR_ID13  HASH("RowanCHackDoor13")
CONST_INT HACK_DOOR_ID14  HASH("RowanCHackDoor14")
CONST_INT HACK_DOOR_ID15  HASH("RowanCHackDoor15")
CONST_INT HACK_DOOR_ID16  HASH("RowanCHackDoor16")
CONST_INT HACK_DOOR_ID17  HASH("RowanCHackDoor17")
CONST_INT HACK_DOOR_ID18  HASH("RowanCHackDoor18")

CONST_INT HACK_DOOR2_ID1  HASH("RowanCHack2Door1")
CONST_INT HACK_DOOR2_ID2  HASH("RowanCHack2Door2")
CONST_INT HACK_DOOR2_ID3  HASH("RowanCHack2Door3")
CONST_INT HACK_DOOR2_ID4  HASH("RowanCHack2Door4")
CONST_INT HACK_DOOR2_ID5  HASH("RowanCHack2Door5")
CONST_INT HACK_DOOR2_ID6  HASH("RowanCHack2Door6")
CONST_INT HACK_DOOR2_ID7  HASH("RowanCHack2Door7")
CONST_INT HACK_DOOR2_ID8  HASH("RowanCHack2Door8")
CONST_INT HACK_DOOR2_ID9  HASH("RowanCHack2Door9")
CONST_INT HACK_DOOR2_ID10  HASH("RowanCHack2Door10")
CONST_INT HACK_DOOR2_ID11  HASH("RowanCHack2Door11")
CONST_INT HACK_DOOR2_ID12  HASH("RowanCHack2Door12")
CONST_INT HACK_DOOR2_ID13  HASH("RowanCHack2Door13")
CONST_INT HACK_DOOR2_ID14  HASH("RowanCHack2Door14")
CONST_INT HACK_DOOR2_ID15  HASH("RowanCHack2Door15")
CONST_INT HACK_DOOR2_ID16  HASH("RowanCHack2Door16")
CONST_INT HACK_DOOR2_ID17  HASH("RowanCHack2Door17")
CONST_INT HACK_DOOR2_ID18  HASH("RowanCHack2Door18")


//ambient door data
BOOL bDoorsInited = FALSE
INT iDoorID[FMMC_MAX_NUM_DOORS]
CONST_INT DOOR_ID1  HASH("RowanCDoor1")
CONST_INT DOOR_ID2  HASH("RowanCDoor2")
CONST_INT DOOR_ID3  HASH("RowanCDoor3")
CONST_INT DOOR_ID4  HASH("RowanCDoor4")
CONST_INT DOOR_ID5  HASH("RowanCDoor5")
CONST_INT DOOR_ID6  HASH("RowanCDoor6")
CONST_INT DOOR_ID7  HASH("RowanCDoor7")
CONST_INT DOOR_ID8  HASH("RowanCDoor8")
CONST_INT DOOR_ID9  HASH("RowanCDoor9")
CONST_INT DOOR_ID10 HASH("RowanCDoor10")
CONST_INT DOOR_ID11 HASH("RowanCDoor11")
CONST_INT DOOR_ID12 HASH("RowanCDoor12")
CONST_INT DOOR_ID13 HASH("RowanCDoor13")
CONST_INT DOOR_ID14 HASH("RowanCDoor14")
CONST_INT DOOR_ID15 HASH("RowanCDoor15")

INT iDoorUpdatedBS

//safe cracking data
SAFE_CRACK_STRUCT SafeCrackData

//pop areas to turn off ambients
INT iPopMultiArea[FMMC_MAX_NUM_ZONES]

//Blocking Area Bitsets
INT iBitsetCoverBlockingArea
INT iBitsetVehicleGeneratorsActiveArea
INT iBitsetSetRoadsArea

//Dispatch Blocking zones
INT iDispatchBlockZone[FMMC_MAX_NUM_ZONES]
INT iZoneWaterCalmingQuadID[FMMC_MAX_NUM_ZONES]
INT iGPSDisabledZones[FMMC_MAX_NUM_ZONES]
FLOAT fZoneRadiusOverride[FMMC_MAX_NUM_ZONES]
FLOAT fZoneGrowth[FMMC_MAX_NUM_ZONES]
SCRIPT_TIMER td_ZoneRadiusOverrideTimer[FMMC_MAX_NUM_ZONES]
BLIP_INDEX biZoneBlips[FMMC_MAX_NUM_ZONES]
INT iLocalZoneBitset
INT iLocalZoneRemovedBitset
INT iLocalZoneCheckBitset //If we need to run processing on any of the zones more than just at the start
INT iLocalZoneFailedCreationBitset
INT iWaveDampingZoneBitset
INT iBlockJumpClimbZoneBitset
INT iBlockRunningZoneBitset
INT iBlockWeaponsZoneBitset
INT iPlayerCantExitVehicleZoneBitset
INT iPlayerVehicleSlowZoneBitset
INT iPlayerVehicleMaxSpeedBitset
INT iAlertPedsZoneBitSet
INT iMetalDetectorZoneHasBeenAlertedBS
INT iCurrentMetalDetectorZone
INT iMetalDetectorZoneDisabledBitset
INT iSpawnOcclusionIndex[FMMC_MAX_NUM_ZONES]
INT iBlockVTOLZoneBitset
INT iBlockElectrocutionZoneBitset
INT iBlockPlayerControlZoneBitset
INT iPlayerCantDriveVehicleZoneBitset
//INT iLocalGPSZonesCurrentlyOccupied
INT iZoneStaggeredLoop
SCENARIO_BLOCKING_INDEX bipopScenarioArea[FMMC_MAX_NUM_ZONES]
INT iLocalGearZoneNoneSet

SCENARIO_BLOCKING_INDEX sbiPacificHeliBlocker
SCENARIO_BLOCKING_INDEX sbiCasinoHeistHeliBlocker

WEAPON_TYPE wtWeaponBlockZonePreviousWeapon = WEAPONTYPE_INVALID

SCRIPT_TIMER tdRocketTimer

FLOAT fRealExplodeProgress
INT iExplodeProgressUpdate
INT iExplodeDriverPart
SCRIPT_TIMER tdExplodeSpeedUpdateTimer
BOOL bExplodeSpeedUpdateThisFrame
FLOAT fDisplayExplodeProgress
FLOAT fVehSpeedForAudioTrigger
INT iLastMinSpeed[FMMC_MAX_TEAMS]
INT iLastMeterMax[FMMC_MAX_TEAMS]
FLOAT fMinSpeedLerpProgress[FMMC_MAX_TEAMS]

SCRIPT_TIMER tdSpeedRaceCheckpointTimer

CONST_FLOAT cfSpeedBarDangerZone	0.2


eFMMC_SCRIPTED_CUTSCENE_PROGRESS eScriptedCutsceneProgress
eFMMC_MOCAP_PROGRESS eMocapManageCutsceneProgress
eFMMC_MOCAP_RUNNING_PROGRESS eMocapRunningCutsceneProgress

INT iScriptedCutsceneProgress
INT iMocapCutsceneProgress
INT iCamShot
INT iScriptedCutsceneTeam = -1
INT iCutsceneStreamingTeam = -1
BOOL bWarpPlayerAfterScriptCameaSetup = FALSE
INT iCamShotLoaded = -1
INT iTODh,iTODm,iTODs
SCRIPT_TIMER tdPhoneCutSafetyTimer
SCRIPT_TIMER tdScriptedCutsceneTimer
SCRIPT_TIMER tdStartWarpSafetyTimer
CAMERA_INDEX cutscenecam
INT iMocapCutsceneConcatParts = -1
WEAPON_TYPE weapMocap = WEAPONTYPE_INVALID
INT iScriptedCutsceneCannedSyncedScene = -1
SCRIPT_TIMER stCannedTVOL

CONST_INT ciScriptedCutsceneBitset_RenderingSyncSceneCam 		0
CONST_INT ciScriptedCutsceneBitset_ForceLocalUseofSyncSceneCam  1
CONST_INT ciScriptedCutsceneBitset_PlayedCutsceneSyncScene  	2

INT iScriptedCutsceneSyncSceneBS

INT iCutsceneBSEnteredCover
INT iCutsceneBSFinishedCover
SCRIPT_TIMER tdEnterCoverEndingCutsceneTimeOut

ENUM SCRIPTED_CUTSCENE_END_WARP_LOCAL_STATE
	SCUT_EWLS_INIT,
	SCUT_EWLS_WARP,
	SCUT_EWLS_WAIT_FOR_VEHICLE,
	SCUT_EWLS_WAIT_FOR_INTERIOR,
	SCUT_EWLS_END
ENDENUM

STRUCT SCRIPTED_CUTSCENE_END_WARP_LOCAL
	SCRIPTED_CUTSCENE_END_WARP_LOCAL_STATE eState
	VECTOR vWarpCoord
	FLOAT fWarpHeading
	VEHICLE_INDEX vehTarget
	VEHICLE_SEAT eCurrentSeat
ENDSTRUCT
SCRIPTED_CUTSCENE_END_WARP_LOCAL sScriptedCutsceneEndWarp

INT iSoundIDCamBackground = GET_SOUND_ID()
INT iSoundPan = GET_SOUND_ID()
INT iSoundZoom = GET_SOUND_ID()
INT iCamSound = GET_SOUND_ID()

INT iElevatorDescendSound = -1

//1 Background
//2 Pan
//3 Zoom
//4 Change_Cam
INT iCamSoundBitSet
INT iStoreLastCam
INT iLocalCamSoundTimer

SCALEFORM_INDEX SI_SecurityCam

camera_index 	ScriptedCatchupCam

CONST_INT MAX_NUM_SUPRESSED_MODELS 24
INT g_iNumSupressed = 0

BOOL bShardShown

BOOL bInBlockedJumpAndClimbZone
BOOL bInBlockedRunningZone
BOOL bInBlockedRunningZoneStart
FLOAT fBlockedRunningZoneSpeed
BOOL bInBlockedWeaponsZone
BOOL bInPlayerCantExitVehicleZone
BOOL bInBlockVTOLZone
BOOL bInPlayerCantDriveVehicleZone

INT iDropZoneMusicProgress = 0
INT iDropZoneMusicEvent = -1
//Drop Zone Music Events
CONST_INT ciDROPZONE_HELI		 	0
CONST_INT ciDROPZONE_JUMP 		 	1
CONST_INT ciDROPZONE_LAND 		 	2
CONST_INT ciDROPZONE_ACTION 	 	3
CONST_INT ciDROPZONE_ACTION_HIGH 	4

//hint cam variables
INT	iNearestTarget = -1
INT	iNearestTargetTemp
INT	iNearestTargetType = ci_TARGET_NONE
INT	iNearestTargetTypeTemp
FLOAT	fNearestTargetDist = 999999
FLOAT	fNearestTargetDistTemp
CHASE_HINT_CAM_STRUCT sHintCam
CHASE_HINT_CAM_STRUCT sIntroHintCam

INT iLastFramesNearestTarget = -1
INT iLastFramesTargetType = ci_TARGET_NONE

//radar Zone variables
FLOAT fFurthestTargetDistTemp
FLOAT fFurthestTargetDist
FLOAT fOldFurthestTargetDist

//Gang backup bitset
CONST_INT SBBBOOL_RESERVATION_COMPLETE			0
CONST_INT SBBBOOL_BACKUP_CREATED				1
CONST_INT SBBBOOL_BACKUP_SPAWNING_FORWARDS		2
CONST_INT SBBBOOL_BACKUP_MAXPEDS_BIT_RANGE_1	3
CONST_INT SBBBOOL_BACKUP_MAXPEDS_BIT_RANGE_2	4
CONST_INT SBBBOOL_BACKUP_MAXPEDS_BIT_RANGE_3	5
CONST_INT SBBOOL_BACKUP_USES_COP_BLIPS			6
CONST_INT SBBBOOL_BACKUP_AIR_VEHICLE_IS_FROZEN	7
CONST_INT SBBBOOL_BACKUP_ON_GOTO_TASK			8
CONST_INT SBBBOOL_BACKUP_DESPAWN_ON_NEW_TYPE	9

//last vehicle radio for manual respawning 2370609
//INT iManRespawnLastRadioStation = 255

//loop iterators

INT iPropIterator
INT iWorldPropIterator
//INT iDynoPropIterator
INT iWeaponIterator
INT iPartIterator
INT iStaggeredIterator
INT iWeaponSpawnIterator
INT iSpawnedWeapons[2]
INT iActiveWeapons[2]

BLIP_INDEX biDynoPropBlips[FMMC_MAX_NUM_DYNOPROPS]

//Hacking phone minigame data
S_HACKING_DATA hackingMinigameData

// gang backup data
CONST_INT MAX_BACKUP_PEDS_PER_VEHICLE 4
CONST_INT MAX_BACKUP_VEHICLES 5
CONST_INT MAX_BACKUP_PEDS 20

INT iLocalVehicleExplosiveBitset

// 0 and above are part indices
CONST_INT ciBACKUP_TARGET_NONE				-1
CONST_INT ciBACKUP_TARGET_AVENGER_DUMMY_PED	-2

STRUCT GangBackupData
	NETWORK_INDEX BackupVeh
	NETWORK_INDEX BackupPed[MAX_BACKUP_PEDS_PER_VEHICLE]
	INT iGangBackupProgress
	//INT iteam[MAX_BACKUP_PEDS_PER_VEHICLE] //This doesn't seem to get used?
	INT iTargetPart = ciBACKUP_TARGET_NONE
	INT iBackSpawnTeam = -1
	INT iBackupBitset
	INT iBackupType = ciBACKUP_TYPE_NONE
	INT iFleeDist = 200
	INT iMaxPeds = 0
	SCRIPT_TIMER tdCleanupTimer //Cleanup immediately when outside 300m, or within 10s if outside 200m (if on a rule that uses the new forward spawning system)
ENDSTRUCT

INT iGangBackupVehIterator
AI_BLIP_STRUCT biChasePedBlip[MAX_BACKUP_PEDS]
VECTOR vBackupSpawnDirection
INT iBackupForwardSpawnAttempts
INT iAirChase_StandardSpawnAttempts
SCRIPT_TIMER tdbackupdelay[FMMC_MAX_TEAMS]
SCRIPT_TIMER tdbackuptargettimer[MAX_BACKUP_PEDS]
SCRIPT_TIMER timeBetweenTeamBackup[FMMC_MAX_TEAMS]
INT iOldbackuptimerPriority[FMMC_MAX_TEAMS]
CONST_FLOAT cfGangBackup_AirSpawnZMinOffset	20.0
CONST_FLOAT cfGangBackup_RotationAmountOnFail	20.0
INT iBackupPedCollisionBitset
INT iBackupPedNavmeshCheckedBS
INT iBackupPedNavmeshLoadedBS

//carry counts
INT iMyPedCarryCount
INT iMyObjectCarryCount
INT iMyVehicleCarryCount
//INT iMyOldPedCarryCount
//INT iMyOldVehCarryCount
INT iMyOldObjCarryCount
INT iLowestLeaveLocationPriority = FMMC_PRIORITY_IGNORE

INT iObjBlipIterator

INT iInAnyLocTemp = -1

INT itempPartNear = -1

INT iPartEscorting
INT iTempPartEscorting

INT iTotalObjectsToDefend = -1

INT tempiNumberOfPart[FMMC_MAX_TEAMS]
INT tempiNumOfWanted[FMMC_MAX_TEAMS]
INT tempiNumOfFakeWanted[FMMC_MAX_TEAMS]
INT tempiNumOfMasks[FMMC_MAX_TEAMS]
INT tempiNumCutscenePlayers[FMMC_MAX_TEAMS]
INT tempiNumCutscenePlayersFinished[ FMMC_MAX_TEAMS ]
INT tempiNumCutsceneSpactatorPlayers[ FMMC_MAX_TEAMS ]
INT tempiNumPartPlayingAndFinished[ FMMC_MAX_TEAMS ]
//INT tempiNumCutsceneStartedPlayers[FMMC_MAX_TEAMS]
INT itempChaseTargets[FMMC_MAX_TEAMS]
INT iTempTotalMissionTime[FMMC_MAX_TEAMS]
INT	tempiNumberOfPlayers[FMMC_MAX_TEAMS]
INT	tempiNumberOfKnockedOutPlayers[FMMC_MAX_TEAMS]
INT itempTeamCriticalAmmo[FMMC_MAX_TEAMS]
INT tempiNumberOfPlayersInRole[FMMC_MAX_TEAMS][FMMC_MAX_ROLES]

INT iTempGranularCurrentPoints[FMMC_MAX_TEAMS]

INT tempiHighestPriority[FMMC_MAX_TEAMS]
INT iCurrentHighPriority[FMMC_MAX_TEAMS]
INT iOldHighPriority[FMMC_MAX_TEAMS]

INT iDoorPriority[FMMC_MAX_TEAMS]

BLIP_INDEX DummyBlip[FMMC_MAX_DUMMY_BLIPS]

SCRIPT_TIMER tdNightVisionDelayTimer

//Custom Text Shard on Team Scoring
INT iTeamScoreLast[FMMC_MAX_TEAMS]

//wanted variables
INT iLocalWantedHighestPriority = -1
INT iLocalFakeWantedHighestPriority = -1
SCRIPT_TIMER tdWantedDelayTimer
SCRIPT_TIMER tdFakeWantedDelayTimer
INT iWantedVehBitset

INT iNumHighPriorityPlayerRule[FMMC_MAX_TEAMS]
INT iNumHighPriorityLoc[FMMC_MAX_TEAMS]
INT	iNumHighPriorityPed[FMMC_MAX_TEAMS]
INT iNumHighPriorityDeadPed[FMMC_MAX_TEAMS]
INT	iNumHighPriorityVeh[FMMC_MAX_TEAMS]
INT	iNumHighPriorityObj[FMMC_MAX_TEAMS]
INT	iNumHighPriorityPedHeld[FMMC_MAX_TEAMS]
INT iTempNumVehHighestPriorityHeld[FMMC_MAX_TEAMS]
INT	iNumHighPriorityObjHeld[FMMC_MAX_TEAMS]

INT iTempTeamVehNumPlayers[FMMC_MAX_TEAMS]
INT iVehHeldBS[FMMC_MAX_TEAMS]
INT iVehHeld_NotMe_BS[FMMC_MAX_TEAMS] // Excludes any vehicle I'm holding

INT iUniqueVehicleBS[FMMC_MAX_TEAMS]
INT iUniqueVehiclesHeld[FMMC_MAX_TEAMS]
INT iUniqueVehiclesInDropOff[FMMC_MAX_TEAMS]

INT iPlayersInSeparateVehicles[FMMC_MAX_TEAMS]
INT iPlayersInAnyLocate
INT iTempHackingFails
INT iOvertimeScoreToGive[FMMC_MAX_TEAMS]

INT	iTempNumPriorityRespawnPed[FMMC_MAX_TEAMS] 
INT	iTempNumPriorityRespawnVeh[FMMC_MAX_TEAMS] 
INT	iTempNumPriorityRespawnObj[FMMC_MAX_TEAMS] 
INT itempPlayerRuleMissionLogic[FMMC_MAX_TEAMS]
INT itempLocMissionLogic[FMMC_MAX_TEAMS]
INT iTempPriorityLocation[FMMC_MAX_TEAMS]
INT itempPedMissionLogic[FMMC_MAX_TEAMS]
INT itempVehMissionLogic[FMMC_MAX_TEAMS]
INT itempObjMissionLogic[FMMC_MAX_TEAMS]
INT itempObjMissionSubLogic[FMMC_MAX_TEAMS]
INT itempCurrentPlayerRule[FMMC_MAX_TEAMS]
INT iTempCaptureLocation

INT iLine1iLoc = -1
INT iLine2iLoc = -1

SCRIPT_TIMER tdtimesincelastupdate[FMMC_MAX_GO_TO_LOCATIONS][FMMC_MAX_TEAMS]
INT tempNumberOfTeamInArea[FMMC_MAX_GO_TO_LOCATIONS][FMMC_MAX_TEAMS]
INT tempNumberOfTeamInAnyArea[FMMC_MAX_GO_TO_LOCATIONS][FMMC_MAX_TEAMS]
INT tempNumberOfTeamInLeaveArea[FMMC_MAX_GO_TO_LOCATIONS][FMMC_MAX_TEAMS]
SCRIPT_TIMER tdcontolHUDFlash

CONST_INT ci_Ped_Inventory               1
CONST_INT ci_Veh_Inventory               2
CONST_INT ci_Obj_Inventory               3

CONST_INT MC_PICKUP_AMMO_AMOUNT 				200
CONST_INT DEFAULT_GOTO_RADIUS                   10

CONST_INT ci_TIME_PER_POINT                     5

// Game States
CONST_INT GAME_STATE_INI 						0
CONST_INT GAME_STATE_TUTORIAL_CUTSCENE			1
CONST_INT GAME_STATE_INTRO_CUTSCENE             2
CONST_INT GAME_STATE_FORCED_TRIP_SKIP			3
CONST_INT GAME_STATE_HEIST_INTRO				4
CONST_INT GAME_STATE_APARTMENT_WALKOUT			5
CONST_INT GAME_STATE_SOLO_PLAY		            6
CONST_INT GAME_STATE_TEAM_PLAY		            7
CONST_INT GAME_STATE_CAMERA_BLEND	            8
CONST_INT GAME_STATE_RUNNING					9
CONST_INT GAME_STATE_MISSION_OVER				10
CONST_INT GAME_STATE_LEAVE						11
CONST_INT GAME_STATE_END						12

//Objective end variables
STRING sObjEndText = ""

INT ireasonObjEnd[FMMC_MAX_TEAMS]
INT iObjEndPriority[FMMC_MAX_TEAMS]
//INT iObjEndMostRecentTeam = -1

//INT iObjEndPriorityBackup = -1
//INT ireasonObjEndBackup = -1
//INT iObjEndTeamBackup = -1

//high score variables
INT istartStealthKills
SCRIPT_TIMER tdHackTimer

INT tempiTeamCivillianKills[FMMC_MAX_TEAMS]

SCRIPT_TIMER tdSyncTimer[FMMC_MAX_TEAMS]
INT iSyncLockCount[FMMC_MAX_TEAMS]

ENUM SYNC_LOCK_TYPE
	SYNC_LOCK_TYPE__ORIGINAL,
	SYNC_LOCK_TYPE__CASINO_KEYPAD
ENDENUM

SCRIPT_TIMER tdWantedTimer

script_timer door_time[FMMC_max_num_doors]

//Participant rankings for Heists end screen
INT first_place_participant = -1
INT second_place_participant = -1
INT third_place_participant = -1
int forth_place_participant = -1

const_int ci_HEISTPLYRRATING_PLATINUM 	0
CONST_INT ci_HEISTPLYRRATING_GOLD		1
CONST_INT ci_HEISTPLYRRATING_SILVER		2
CONST_INT ci_HEISTPLYRRATING_BRONZE		3
CONST_INT ci_HEISTPLYRRATING_BAD		4
CONST_INT ci_HEISTPLYRRATING_NONE		5

CONST_INT ciMAX_CUT_PLAYERS 4
//Struct for the players weapon details
STRUCT PLAYER_WEAPON_DETAILS_FOR_CUTSCENE
	BOOL bHasThisWeapon
	BOOL bSet
	WEAPON_INFO sWeaponInfo
	INT iParticipant
ENDSTRUCT
PLAYER_WEAPON_DETAILS_FOR_CUTSCENE sPlayerWeaponDetailsForCutscene[ciMAX_CUT_PLAYERS]

//Objective end reasons
CONST_INT OBJ_END_REASON_LOC_CAPTURED				0
CONST_INT OBJ_END_REASON_PED_CAPTURED				1	
CONST_INT OBJ_END_REASON_VEH_CAPTURED				2
CONST_INT OBJ_END_REASON_OBJ_CAPTURED				3
CONST_INT OBJ_END_REASON_PED_DELIVERED				4	
CONST_INT OBJ_END_REASON_VEH_DELIVERED				5
CONST_INT OBJ_END_REASON_OBJ_DELIVERED				6
CONST_INT OBJ_END_REASON_PED_DEAD					7	
CONST_INT OBJ_END_REASON_VEH_DEAD					8
CONST_INT OBJ_END_REASON_OBJ_DEAD					9
CONST_INT OBJ_END_REASON_PLAYERS_KILLED				10
CONST_INT OBJ_END_REASON_PROTECTED_PED              11
CONST_INT OBJ_END_REASON_PROTECTED_VEH              12
CONST_INT OBJ_END_REASON_PROTECTED_OBJ              13
CONST_INT OBJ_END_REASON_ARV_LOC             		14
CONST_INT OBJ_END_REASON_ARV_PED             		15
CONST_INT OBJ_END_REASON_ARV_VEH             		16
CONST_INT OBJ_END_REASON_ARV_OBJ             		17
CONST_INT OBJ_END_REASON_TIME_EXPIRED             	18
CONST_INT OBJ_END_REASON_TARGET_SCORE               19
CONST_INT OBJ_END_REASON_PHOTO_LOC					20
CONST_INT OBJ_END_REASON_PHOTO_PED					21
CONST_INT OBJ_END_REASON_PHOTO_VEH					22
CONST_INT OBJ_END_REASON_PHOTO_OBJ					23
CONST_INT OBJ_END_REASON_HACK_OBJ                   24
CONST_INT OBJ_END_REASON_OUT_OF_LIVES               25
CONST_INT OBJ_END_REASON_PED_ARRIVED                26
CONST_INT OBJ_END_REASON_TEAM_GONE                  27
CONST_INT OBJ_END_REASON_LOST_COPS                  28
CONST_INT OBJ_END_REASON_PLAYERS_GOT_MASKS          29
CONST_INT OBJ_END_REASON_CHARM_PED                  30
CONST_INT OBJ_END_REASON_PED_AGRO                   31
CONST_INT OBJ_END_REASON_PED_FOUND_BODY				32
CONST_INT OBJ_END_REASON_PLAYERS_ARRESTED           33
CONST_INT OBJ_END_REASON_HEIST_LEADER_LEFT          34
CONST_INT OBJ_END_REASON_HEIST_TEAM_GONE			35
CONST_INT OBJ_END_REASON_HEIST_TEAM_GONE_T0         36
CONST_INT OBJ_END_REASON_HEIST_TEAM_GONE_T1			37
CONST_INT OBJ_END_REASON_HEIST_TEAM_GONE_T2			38
CONST_INT OBJ_END_REASON_HEIST_TEAM_GONE_T3			39
CONST_INT OBJ_END_REASON_CC_PED_DEAD				40
CONST_INT OBJ_END_REASON_CC_PED_ESCAPED				41
CONST_INT OBJ_END_REASON_CC_PED_HERO_GUN			42
CONST_INT OBJ_END_REASON_CC_PED_HERO_PHONE			43
CONST_INT OBJ_END_REASON_LVE_LOC             		44
CONST_INT OBJ_END_REASON_GAINED_WANTED 				45
CONST_INT OBJ_END_REASON_NO_AMMO                    46
CONST_INT OBJ_END_REASON_LOCATE_WRONG_ANGLE         47
CONST_INT OBJ_END_REASON_MOD_SHOP_CLOSED			48
CONST_INT OBJ_END_REASON_PLAYERS_GONE_TO			49


enum eFailMissionEnum 
	 mFail_none 					= 0
	,mFail_LOC_CAPTURED				
	,mFail_PED_CAPTURED		
	,mFail_VEH_CAPTURED		
	,mFail_OBJ_CAPTURED		
	,mFail_PED_DELIVERED		
	,mFail_VEH_DELIVERED		
	,mFail_OBJ_DELIVERED		
	,mFail_PED_DEAD			
	,mFail_VEH_DEAD			
	,mFail_OBJ_DEAD					//10		
	,mFail_PLAYERS_KILLED		
	,mFail_PROTECTED_PED     
	,mFail_PROTECTED_VEH     
	,mFail_PROTECTED_OBJ     
	,mFail_ARV_LOC           
	,mFail_ARV_PED           
	,mFail_ARV_VEH           
	,mFail_ARV_OBJ           
	,mFail_TIME_EXPIRED   
	,mFail_MULTI_TIME_EXPIRED		//20 
	,mFail_TARGET_SCORE     
	,mFail_PHOTO_LOC			
	,mFail_PHOTO_PED			
	,mFail_PHOTO_VEH			
	,mFail_PHOTO_OBJ			
	,mFail_HACK_OBJ          
	,mFail_OUT_OF_LIVES      
	,mFail_PED_ARRIVED       	
	,mFail_TEAM_GONE         
	,mFail_LOST_COPS         		//30
	,mFail_PLAYERS_GOT_MASKS 
	,mFail_CHARM_PED         
	,mFail_PED_AGRO_T0   
	,mFail_PED_AGRO_T1 
	,mFail_PED_AGRO_T2 
	,mFail_PED_AGRO_T3 
	,mFail_PED_FOUND_BODY		
	,mFail_PLAYERS_ARRESTED  
	,mFail_HEIST_LEADER_LEFT 
	,mFail_HEIST_TEAM_GONE			//40
	,mFail_HEIST_TEAM_GONE_T0 	 
	,mFail_HEIST_TEAM_GONE_T1
	,mFail_HEIST_TEAM_GONE_T2
	,mFail_HEIST_TEAM_GONE_T3
	,mFail_YOU_LEFT
	,mFail_CC_PED_DEAD		
	,mFail_CC_PEDS_DEAD	
	,mFail_CC_PED_HERO_GUN
	,mFail_CC_PED_HERO_PHONE
	,mFail_CC_PED_HERO_ALARM		//50
	,mFail_LVE_LOC     
	,mFail_OUT_OF_BOUNDS 
	,mFail_GAINED_WANTED 		
	,mFail_NO_AMMO           
	,mFail_LOCATE_WRONG_ANGLE  
	,mFail_T0MEMBER_DIED
	,mFail_T1MEMBER_DIED
	,mFail_T2MEMBER_DIED
	,mFail_T3MEMBER_DIED
	,mFail_HEIST_TEAM_T0_FAILED 	//60
	,mFail_HEIST_TEAM_T1_FAILED 
	,mFail_HEIST_TEAM_T2_FAILED 
	,mFail_HEIST_TEAM_T3_FAILED
	,mFail_MOD_SHOP_CLOSED
	,mFail_GAPS_IN_TEAMS
	,mFail_CEO_GONE
	,mFail_MC_PRESIDENT_GONE
	,mFail_SCORE_CANT_BE_MATCHED
	,mFail_ALL_TEAMS_FAIL
	,mFail_OTHER_TEAM_HAS_ALL_OBJECTS
	,mFail_TOWERS_ARE_DESTROYED
	,max_MissionFails //64
endenum
//eFailMissionEnum eCurrentTeamFail[FMMC_MAX_TEAMS] //moved to broadcast data 3

//player fail reasons to end before team
CONST_INT PLAYER_FAIL_REASON_BOUNDS             0
CONST_INT PLAYER_FAIL_OUT_OF_LIVES              1



CONST_INT ciINITIAL_GOTO                        999
CONST_INT ciSECONDARY_GOTO                      1000


// Jobs the player can complete
CONST_INT JOB_COMP_DELIVER_PED                  0
CONST_INT JOB_COMP_DELIVER_VEH 					1
CONST_INT JOB_COMP_DELIVER_OBJ 					2
CONST_INT JOB_COMP_ARRIVE_LOC 					3
CONST_INT JOB_COMP_ARRIVE_VEH					4
CONST_INT JOB_COMP_ARRIVE_OBJ					5
CONST_INT JOB_COMP_ARRIVE_PED 					6
CONST_INT JOB_COMP_HELD_AREA					7 
CONST_INT JOB_COMP_KILL_PLAYER 					8
CONST_INT JOB_COMP_KILL_PED 					9
CONST_INT JOB_COMP_KILL_VEH 					10
CONST_INT JOB_COMP_KILL_OBJ 					11
CONST_INT JOB_COMP_HOLD_PED						12
CONST_INT JOB_COMP_HOLD_VEH 					13
CONST_INT JOB_COMP_HOLD_OBJ 					14
CONST_INT JOB_COMP_PHOTO_LOC 					15
CONST_INT JOB_COMP_PHOTO_VEH					16
CONST_INT JOB_COMP_PHOTO_OBJ					17
CONST_INT JOB_COMP_PHOTO_PED 					18
CONST_INT JOB_COMP_HACK_OBJ                     19
CONST_INT JOB_COMP_CHARM_PED                    20
CONST_INT JOB_COMP_CROWD_CONTROL                21

VECTOR vPedTargetCoords[FMMC_MAX_PEDS]
PED_INDEX piOldTargetPed[FMMC_MAX_PEDS]

SCRIPT_TIMER tdPlayerBlipsTimer
SCRIPT_TIMER tdeventsafetytimer
SCRIPT_TIMER tdVehDropoffNetIDRegTimer

SCRIPT_TIMER tddoorsafteytimer
SCRIPT_TIMER tdresultdelay
SCRIPT_TIMER tdDeliverTimer
SCRIPT_TIMER tdDeliverBackupTimer
SCRIPT_TIMER tdForceEveryoneOutBackupTimer

FMMC_EOM_DETAILS                sEndOfMission
PLAYER_CURRENT_STATUS_COUNTS    sPlayerCounts



// Local mission stages
CONST_INT	CLIENT_MISSION_STAGE_SETUP					0
CONST_INT	CLIENT_MISSION_STAGE_KILL_PED				1
CONST_INT	CLIENT_MISSION_STAGE_PROTECT_PED			2
CONST_INT	CLIENT_MISSION_STAGE_COLLECT_PED			3
CONST_INT	CLIENT_MISSION_STAGE_DELIVER_PED			4
CONST_INT	CLIENT_MISSION_STAGE_KILL_VEH				5
CONST_INT	CLIENT_MISSION_STAGE_PROTECT_VEH			6
CONST_INT	CLIENT_MISSION_STAGE_COLLECT_VEH			7
CONST_INT	CLIENT_MISSION_STAGE_DELIVER_VEH			8 
CONST_INT	CLIENT_MISSION_STAGE_KILL_OBJ				9
CONST_INT	CLIENT_MISSION_STAGE_PROTECT_OBJ			10
CONST_INT	CLIENT_MISSION_STAGE_COLLECT_OBJ			11
CONST_INT	CLIENT_MISSION_STAGE_DELIVER_OBJ			12
CONST_INT   CLIENT_MISSION_STAGE_GOTO_LOC				13
CONST_INT   CLIENT_MISSION_STAGE_CONTROL_AREA  			14
CONST_INT   CLIENT_MISSION_STAGE_CONTROL_PED   			15
CONST_INT   CLIENT_MISSION_STAGE_CONTROL_VEH   			16
CONST_INT   CLIENT_MISSION_STAGE_CONTROL_OBJ   			17
CONST_INT   CLIENT_MISSION_STAGE_GOTO_PED				18
CONST_INT   CLIENT_MISSION_STAGE_GOTO_VEH				19
CONST_INT   CLIENT_MISSION_STAGE_GOTO_OBJ				20
CONST_INT   CLIENT_MISSION_STAGE_KILL_PLAYERS  			21
CONST_INT   CLIENT_MISSION_STAGE_KILL_TEAM0   	 		22
CONST_INT   CLIENT_MISSION_STAGE_KILL_TEAM1				23
CONST_INT   CLIENT_MISSION_STAGE_KILL_TEAM2				24
CONST_INT   CLIENT_MISSION_STAGE_KILL_TEAM3				25
CONST_INT   CLIENT_MISSION_STAGE_PHOTO_LOC				26
CONST_INT   CLIENT_MISSION_STAGE_PHOTO_PED   			27
CONST_INT   CLIENT_MISSION_STAGE_PHOTO_VEH    			28
CONST_INT   CLIENT_MISSION_STAGE_PHOTO_OBJ    			29
CONST_INT   CLIENT_MISSION_STAGE_HACK_COMP    	 		30
CONST_INT   CLIENT_MISSION_STAGE_HACK_SAFE    	  		31
CONST_INT   CLIENT_MISSION_STAGE_HACK_SYNC_LOCK 		32
CONST_INT	CLIENT_MISSION_STAGE_HACK_OPEN_CASE			33
CONST_INT	CLIENT_MISSION_STAGE_INTERACT_WITH			34
CONST_INT	CLIENT_MISSION_STAGE_CASH_GRAB				35
CONST_INT   CLIENT_MISSION_STAGE_BLOW_DOOR      		36
CONST_INT   CLIENT_MISSION_STAGE_GET_MASK       		37
CONST_INT   CLIENT_MISSION_STAGE_SCRIPTED_CUTSCENE 		38
CONST_INT   CLIENT_MISSION_STAGE_CROWD_CONTROL 			39
CONST_INT   CLIENT_MISSION_STAGE_CHARM_PED     			40
CONST_INT   CLIENT_MISSION_STAGE_ARREST_ALL    			41
CONST_INT   CLIENT_MISSION_STAGE_ARREST_TEAM0  			42
CONST_INT   CLIENT_MISSION_STAGE_ARREST_TEAM1  			43
CONST_INT   CLIENT_MISSION_STAGE_ARREST_TEAM2  			44
CONST_INT   CLIENT_MISSION_STAGE_ARREST_TEAM3  			45
CONST_INT   CLIENT_MISSION_STAGE_LEAVE_LOC     			46
CONST_INT	CLIENT_MISSION_STAGE_PHONE_BULLSHARK 		47
CONST_INT	CLIENT_MISSION_STAGE_PHONE_AIRSTRIKE 		48
CONST_INT	CLIENT_MISSION_STAGE_HACK_DRILL      		49
CONST_INT	CLIENT_MISSION_STAGE_GOTO_ANY_PLAYER		50
CONST_INT	CLIENT_MISSION_STAGE_GOTO_TEAM0				51
CONST_INT	CLIENT_MISSION_STAGE_GOTO_TEAM1				52
CONST_INT	CLIENT_MISSION_STAGE_GOTO_TEAM2				53
CONST_INT	CLIENT_MISSION_STAGE_GOTO_TEAM3				54
CONST_INT 	CLIENT_MISSION_STAGE_OPEN_CONTAINER			55
CONST_INT 	CLIENT_MISSION_STAGE_ENTER_SCRIPTED_TURRET	56
CONST_INT 	CLIENT_MISSION_STAGE_HACKING				57
CONST_INT	CLIENT_MISSION_STAGE_RESULTS				58
CONST_INT	CLIENT_MISSION_STAGE_TERMINATE_DELAY 		59
CONST_INT  	CLIENT_MISSION_STAGE_HACK_FINGERPRINT_KEYPAD 60
CONST_INT  	CLIENT_MISSION_STAGE_HACK_BEAMHACK_VEH 		61
CONST_INT	CLIENT_MISSION_STAGE_END					62

LEADERBOARD_PLACEMENT_TOOLS LBPlacement 
//TEXT_LABEL_23 tParticipantNames[MAX_NUM_MC_PLAYERS]
INT iSavedRow[NUM_NETWORK_PLAYERS]
INT iSavedSpectatorRow[NUM_NETWORK_PLAYERS]
INT iLBPlayerSelection = -1
TIME_DATATYPE timeScrollDelay 

SCRIPT_TIMER mocapFailsafe

SCRIPT_TIMER mocapPreCinematic

INTERIOR_INSTANCE_INDEX iCurrentInterior 

//INTERIOR_INSTANCE_INDEX iMocapInterior //used for 2104332

INTERIOR_INSTANCE_INDEX iCutInterior  //Used for non-apartment interiors

FMMC_INTERIOR_FIX_STRUCT sInteriorFixStruct

CONST_INT BS_SCC_MANUAL_CCTV 0
CONST_INT BS_SCC_ADD_MANUAL_CCTV_HELP 1
CONST_INT BS_SCC_RESET_ADAPTATION 2

INT bsSpecialCaseCutscene
INT iCutsceneID_GotoShot = -1
INT iCoolDownTimerForManualCCTV

CONST_INT ciPOPULATE_RESULTS					0
CONST_INT LBOOL_HIT_MOCAP_FAIL_SAFE				1
CONST_INT PLAYER_DEATH_TOGGLE   				2
CONST_INT LBOOL_COUNTED_FINAL_DEATH   			3
CONST_INT TEMP_TEAM_0_ACTIVE					4
CONST_INT TEMP_TEAM_1_ACTIVE					5
CONST_INT TEMP_TEAM_2_ACTIVE    6
CONST_INT TEMP_TEAM_3_ACTIVE    7
CONST_INT WAS_NOT_THE_HOST      8
CONST_INT LBOOL_TRIGGER_CTF_END_MUSIC 9
CONST_INT LBOOL_TRIGGER_CTF_PRE_END_MUSIC 10
CONST_INT CHECK_SPEC_STATUS     11
CONST_INT LBOOL_SPAWN_AREA_SET  12
CONST_INT LBOOL_LB_FINISHED   13
CONST_INT LBOOL_MISSION_SET_NOT_JOINABLE  14
CONST_INT LBOOL_BLOCK_OBJECTIVE 15
CONST_INT LBOOL_HAD_MORE_THAN_1_LIFE 16
CONST_INT LBOOL_TDF_PLAY_IN_RANGE	17
CONST_INT LBOOL_ON_LAST_LIFE    18
CONST_INT LBOOL_CARGOBOB_DROP   19
CONST_INT LBOOL_MUSIC_CHANGED   20
CONST_INT LBOOL_HAD_MORE_THAN_1_PACKAGE 21
CONST_INT LBOOL_DPAD_HELP_DISP  22
CONST_INT LBOOL_WITH_CARRIER_TEMP    23
CONST_INT LBOOL_SHOW_ALL_PLAYER_BLIPS 24
CONST_INT LBOOL_MISSION_START_POSITION_SET 25
CONST_INT LBOOL_SETTING_AFTER_DEATH_WANTED 26
CONST_INT LBOOL_ON_CONTACT_MISSION 27
CONST_INT LBOOL_IN_GROUP_PED_VEH_TEMP 28
CONST_INT LBOOL_MADE_WANTED_EASY    29
CONST_INT LBOOL_SAVED_EOM_VEHICLE    30
CONST_INT LBOOL_WITH_CARRIER_FLAG 31

INT iLocalBoolCheck

CONST_INT  LBOOL2_CLEAR_CELEBRATION_SCREEN 		0
CONST_INT  LBOOL2_LB_CAM_READY           		1
CONST_INT  LBOOL2_HANDLER_HELP_TEXT_NEEDED 		2
CONST_INT  LBOOL2_ACTIVATE_MOCAP_CUTSCENE 		3
CONST_INT  LBOOL2_ACTIVATE_MOCAP_CUTSCENE_OVER 	4
CONST_INT  LBOOL2_HACKING_TICKER_SENT     		5
CONST_INT  LBOOL2_MOCAP_MALE      				6
CONST_INT  LBOOL2_MOCAP_FEMALE    				7
CONST_INT  LBOOL2_CHECKED_RESTRICTION_ZONE 		8
CONST_INT  LBOOL2_MOCAP_PLAYER_PLAYING_ANIM 	9 
CONST_INT  LBOOL2_STARTED_UPDATE       			10
CONST_INT  LBOOL2_WHOLE_UPDATE_DONE    			11
CONST_INT  LBOOL2_REQUEST_UPDATE       			12
CONST_INT  LBOOL2_ACTIVATE_MOCAP_CUTSCENE_WARP 	13
CONST_INT  LBOOL2_CUTSCENE_WARP_DONE     		14
CONST_INT  LBOOL2_CUTSCENE_STREAMED      		15
CONST_INT  LBOOL2_MISSION_START_STAY_STILL 		16
CONST_INT  LBOOL2_IN_RESTRICTION_ZONE      		17
CONST_INT  LBOOL2_PLAY_END_MUSIC           		18
CONST_INT  LBOOL2_FIRST_PART_LOOP_DONE    		19
CONST_INT  LBOOL2_CLEAR_END_BIG_MESSAGE			20
CONST_INT  LBOOL2_NORMAL_END            		21
CONST_INT  LBOOL2_WEATHER_SET            		22
CONST_INT  LBOOL2_FLUSH_FEED             		23
CONST_INT  LBOOL2_MY_TEAM_NAME_IS_CREW    		24
CONST_INT  LBOOL2_PED_DROPPED_OFF        		25
CONST_INT  LBOOL2_PASSED_MISSION          		26
CONST_INT  LBOOL2_SPAWN_SAFETY_FALLBACK  		27
CONST_INT  LBOOL2_MISSION_START_WARP_DONE		28
CONST_INT  LBOOL2_IN_RANGE_FOR_SAFE_CRACK       29
CONST_INT  LBOOL2_USE_CRATE_DELIVERY_DELAY 		30
CONST_INT  LBOOL2_BRING_VEHICLE_TO_HALT_CALLED 	31

INT iLocalBoolCheck2

CONST_INT  LBOOL3_MUSIC_PLAYING				0
CONST_INT  LBOOL3_TEAM_INTRO				1
CONST_INT  LBOOL3_RADAR_INIT				2
CONST_INT  LBOOL3_UPDATE_OBJECT_OBJECTIVE	3
CONST_INT  LBOOL3_UPDATE_VEHICLE_OBJECTIVE	4
CONST_INT  LBOOL3_UPDATE_PED_OBJECTIVE		5
CONST_INT  LBOOL3_SHOWN_RESULTS				6
CONST_INT  LBOOL3_LATE_SPECTATE				7
CONST_INT  LBOOL3_WAIT_END_SPECTATE			8
CONST_INT  LBOOL3_KILL_SPECTATE				9
CONST_INT  LBOOL3_NEW_JOB_SCREENS			10
CONST_INT  LBOOL3_MOVE_TO_END				11
CONST_INT  LBOOL3_EXPLOSIVE_ZONE_ACTIVE		12
CONST_INT  LBOOL3_CLEANED_UP_CAM			13
CONST_INT  LBOOL3_FAKE_CAM_MADE				14
CONST_INT  LBOOL3_ORIGINAL_SYNC_LOCK_PLAYER	15
CONST_INT  LBOOL3_GAMER_HANDLE_SET			16
CONST_INT  LBOOL3_USING_SYNC_LOCK			17
CONST_INT  LBOOL3_DISPLAY_SUDDEN_DEATH		18
CONST_INT  LBOOL3_DISABLE_GPSANDHINTCAM		19
CONST_INT  LBOOL3_IN_BOUNDS1				20
CONST_INT  LBOOL3_IN_BOUNDS2				21
CONST_INT  LBOOL3_CTFLOC_UNDERWATER			22
CONST_INT  LBOOL3_CTF_OBJECT_IS_BAG			23
CONST_INT  LBOOL3_CTF_OBJECT_IS_CASE		24
CONST_INT  LBOOL3_ADDED_DIALOGUE_PED		25
CONST_INT  LBOOL3_PHOTOTAKEN				26
CONST_INT  LBOOL3_SET_RESPAWN_WEAPON		27
CONST_INT  LBOOL3_WANTED_BOUNDS1_TOGGLE		28
CONST_INT  LBOOL3_WANTED_BOUNDS2_TOGGLE		29
CONST_INT  LBOOL3_OUTOFBOUNDS_TESTMODE		30
CONST_INT  LBOOL3_VELUM2_DOOR_MOVING		31

INT iLocalBoolCheck3

CONST_INT  LBOOL4_TRIGGER_ALARMS						0
CONST_INT  LBOOL4_DELIVERYDONE							1
CONST_INT  LBOOL4_T0GOTWANTED							2
CONST_INT  LBOOL4_T1GOTWANTED							3
CONST_INT  LBOOL4_T2GOTWANTED							4
CONST_INT  LBOOL4_T3GOTWANTED							5
CONST_INT  LBOOL4_T0LOSTWANTED							6
CONST_INT  LBOOL4_T1LOSTWANTED							7
CONST_INT  LBOOL4_T2LOSTWANTED							8
CONST_INT  LBOOL4_T3LOSTWANTED							9
CONST_INT  LBOOL4_DELIVERY_WAIT							10
CONST_INT  LBOOL4_I_HAVE_PUSHED_SYNC_LOCK				11
CONST_INT  LBOOL4_OWN_MASK								12
CONST_INT  LBOOL4_IN_MASK_SHOP							13
CONST_INT  LBOOL4_BROWSING_SHOP							14
CONST_INT  LBOOL4_HEIST_HELP_TEXT_1						15
CONST_INT  LBOOL4_APARTMENT_LOADED						16
CONST_INT  LBOOL4_IN_APARTMENT							17
CONST_INT  LBOOL4_ANIMSCENESTAGE1						18
CONST_INT  LBOOL4_ANIMSCENESTAGE2						19
CONST_INT  LBOOL4_APARTMENTMENU_LOADED					20
CONST_INT  LBOOL4_AWARD_LAST_ALIVE						21
CONST_INT  LBOOL4_ANIMSCENESTAGE0						22
CONST_INT  LBOOL4_APTWARP_REQUESTSENT					23
CONST_INT  LBOOL4_MOCAP_START_FADE_IN_DONE				24
CONST_INT  LBOOL4_SCRIPTED_CUT_NEEDS_CLEANED_UP			25
CONST_INT  LBOOL4_APARTMENTMENU_DISPLAYING				26
CONST_INT  LBOOL4_DROPOFF_TO_CARGOBOB					27
CONST_INT  LBOOL4_BEEN_GIVEN_MIDMISSION_INVENTORY		28
CONST_INT  LBOOL4_WAIT_FOR_RESPAWN_IN_VEH				29
CONST_INT  LBOOL4_INSIDE_RESTRICTED_AIRSPACE			30
CONST_INT  LBOOL4_RESPAWN_SHOULD_FORCE_AREA_CHECK		31

INT iLocalBoolCheck4


CONST_INT 	LBOOL5_MOCAP_END_FADE_OUT_DONE					0
CONST_INT 	LBOOL5_SAFE_CRACK_DATA_INITIALISED				1
CONST_INT 	LBOOL5_HEIST_HELP_TEXT_NIGHTVISION 				2
CONST_INT	LBOOL5_SPAWN_TEAM_IN_VEHICLE					3
CONST_INT	LBOOL5_BANKMANAGER_POSCHECK						4
CONST_INT	LBOOL5_BANKMANAGER_POSHANDLING					5
CONST_INT	LBOOL5_IS_MANUALLY_RESPAWNING_NOW				6
CONST_INT	LBOOL5_TEMP_ANYONE_IN_APPARTMENT			    7
CONST_INT	LBOOL5_GPS_DELIVER_ROUTE_SET					8
CONST_INT	LBOOL5_CTF_OBJECT_IS_FLAG						9
CONST_INT	LBOOL5_PED_INVOLVED_IN_CUTSCENE			    	10
CONST_INT	LBOOL5_TURNED_OFF_ARTIFICIAL_LIGHTS		    	11
CONST_INT	LBOOL5_SET_UP_STRAND_DETAILS_AT_END_OF_MISSION	12
CONST_INT	LBOOL5_MOCAP_REMOVED_LOCAL_PLAYER_MASK			13
CONST_INT	LBOOL5_MID_MISSION_MOCAP      			    	14
CONST_INT	LBOOL5_SHOWN_APARTMENT_GET_AND_DELIVER_HELP		15
CONST_INT	LBOOL5_PED_ACTION_MODE							16
CONST_INT	LBOOL5_TRIGGERED_EMP_AUDIO						17
CONST_INT	LBOOL5_KEEP_FORCE_EVERYONE_FROM_MY_CAR			18
CONST_INT	LBOOL5_CTF_OBJECT_IS_TARGET						19
CONST_INT	LBOOL5_DISPATCHOFF								20
CONST_INT	LBOOL5_OBJCOMP_KILLCHASEHINTCAM_1				21
CONST_INT	LBOOL5_OBJCOMP_KILLCHASEHINTCAM_2				22
CONST_INT	LBOOL5_WAS_IN_DROP_OFF							23
CONST_INT	LBOOL5_BLOCK_TURNING_OFF_ARTIFICIAL_LIGHTS_EOM	24
CONST_INT	LBOOL5_TDF_PLAY_LOSE_FIRST						25
CONST_INT	LBOOL5_HUD_COLOUR_RESET							26
CONST_INT	LBOOL5_GIVEN_SPAWN_VEHICLE_STARTING_VELOCITY	27
CONST_INT	LBOOL5_SHOW_CARRIER_IPL_MAP						28
CONST_INT	LBOOL5_RESTRICTION_ROCKET_LOADED				29
CONST_INT	LBOOL5_TDF_PLAY_OUT_OF_RANGE					30
CONST_INT	LBOOL5_STARTED_COUNTDOWN_TIMER_SOUNDS			31

INT iLocalBoolCheck5

CONST_INT	LBOOL6_SHOW_RETRY_HELP      					0
CONST_INT	LBOOL6_SOMEONE_NEEDS_RESTRICTION_ROCKETS		1
CONST_INT	LBOOL6_DISABLECONTROLTIMER_COMPLETE				2
CONST_INT	LBOOL6_IN_A_LOCATE								3
CONST_INT   LBOOL6_SCRIPTED_CUT_DONE_PHONE_INTRO			4
CONST_INT   LBOOL6_SCRIPTED_CUT_START_PHONE_INTRO           5
CONST_INT	LBOOL6_NEED_TO_CALCULATE_POSITION_AHEAD			6
CONST_INT	LBOOL6_DIALOGUE_ANIM_NEEDED						7
CONST_INT	LBOOL6_IN_MY_PRIORITY_LEAVE_LOC					8
CONST_INT 	LBOOL6_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC 		9
CONST_INT 	LBOOL6_TEAM_1_NEEDS_TO_WAIT_FOR_LEAVE_LOC 		10
CONST_INT 	LBOOL6_TEAM_2_NEEDS_TO_WAIT_FOR_LEAVE_LOC 		11
CONST_INT 	LBOOL6_TEAM_3_NEEDS_TO_WAIT_FOR_LEAVE_LOC 		12
CONST_INT	LBOOL6_SHOW_YACHT2_IPL_MAP						13
CONST_INT	LBOOL6_SAVED_ROUNDS_LBD_DATAa					14
CONST_INT	LBOOL6_SAVED_ROUNDS_LBD_DATAb					15
CONST_INT	LBOOL6_TRIGGER_LTS_END_MUSIC					16
CONST_INT	LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME		17
CONST_INT	LBOOL6_TEAM_1_HAS_NEW_PRIORITY_THIS_FRAME		18
CONST_INT	LBOOL6_TEAM_2_HAS_NEW_PRIORITY_THIS_FRAME		19
CONST_INT	LBOOL6_TEAM_3_HAS_NEW_PRIORITY_THIS_FRAME		20
CONST_INT	LBOOL6_TEAM_0_HAS_NEW_MIDPOINT_THIS_FRAME		21
CONST_INT	LBOOL6_TEAM_1_HAS_NEW_MIDPOINT_THIS_FRAME		22
CONST_INT	LBOOL6_TEAM_2_HAS_NEW_MIDPOINT_THIS_FRAME		23
CONST_INT	LBOOL6_TEAM_3_HAS_NEW_MIDPOINT_THIS_FRAME		24
CONST_INT	LBOOL6_BLOCK_SAVE_EOM_CAR_WAS_DELIVERED			25
CONST_INT	LBOOL6_SCRIPTED_CUT_READY_FOR_PHONE_INTRO		26
CONST_INT	LBOOL6_HIDE_JOB_POINTS_CELEB_SCREEN				27
CONST_INT	LBOOL6_USING_NIGHTVISION_WHEN_ENTERING_CUTSCENE	28
CONST_INT	LBOOL6_HIDE_RADAR								29

CONST_INT	LBOOL6_SCRIPTED_CUT_DONE_FOCUS_INTRO			30
CONST_INT	LBOOL6_SCRIPTED_CUT_START_FOCUS_INTRO			31

INT iLocalBoolCheck6 

CONST_INT 	LBOOL7_SCRIPTED_CUT_INIT_FOCUS_INTRO			0
CONST_INT 	LBOOL7_ADDED_JP									1
CONST_INT	LBOOL7_CURRENT_SCRIPTED_CAM_AREA_LOADED			2
CONST_INT	LBOOL7_BLOCK_TOD_EVERY_FRAME					3
CONST_INT	LBOOL7_PROPERTY_DROP_OFF_OWNER_HELP				4
CONST_INT	LBOOL7_MOCAP_LOAD_SCENE_STARTED					5
CONST_INT	LBOOL7_SECURITY_CAM_FILTER_USED					6
CONST_INT	LBOOL7_OUTFIT_ON_RULE_PROCESSING				7
CONST_INT	LBOOL7_OUTFIT_ON_RULE_LOADED					8
CONST_INT	LBOOL7_SLIPSTREAM_SOUND_ACTIVE					9
CONST_INT	LBOOL7_SLIPSTREAM_ACTIVE					    10
CONST_INT	LBOOL7_HIDE_MOCAP_PLAYERS						11
CONST_INT	LBOOL7_HOST_APPLIED_NEW_PRIORITY_MIDFRAME		12 // This gets set on the host when the iCurrentHighestPriority is updated - used to stop things from spawning as it's a new rule, and things should clean up first
CONST_INT	LBOOL7_SHOULD_HOLD_UP_MOCAP_FOR_CUTSCENE		13
CONST_INT	LBOOL7_TOGGLED_RENDERPHASES_FOR_DROPOFF			14
CONST_INT	LBOOL7_SET_END_MISSION_WEAPON					15
CONST_INT	LBOOL7_PRE_MAIN_WAS_A_SPECTATOR_CACHE			16
CONST_INT	LBOOL7_CURRENTLY_ON_APARTMENT_GOTO				17
CONST_INT	LBOOL7_WAS_VEH_RADIO_ENABLED_AT_CS_START		18
CONST_INT	LBOOL7_CUTSCENE_PHONE_SAFETY_TRIGGERED			19
CONST_INT	LBOOL7_PRISON_BREAK_CARGO_SCENE_TRIGGERED		20
CONST_INT 	LBOOL7_PRISON_BREAK_CARGO_SCENE_INIT			21
CONST_INT 	LBOOL7_DRAW_BLACK_RECTANGLE						22
CONST_INT	LBOOL7_IGNORE_RESPAWN_VISIBILITY_CHECKS			23
CONST_INT	LBOOL7_WATER_CALMING_QUAD_YACHT_CREATED			24
CONST_INT	LBOOL7_BIOLAB_EMP_SETTINGS_DONE			        25
CONST_INT	LBOOL7_SET_PLAYER_CONTROL_FOR_BRIDGE_MOCAP		26
CONST_INT	LBOOL7_SHOULD_CUTSCENE_DISABLE_PHONE_THIS_FRAME	27
CONST_INT	LBOOL7_DONE_CUTSCENE_HACKING_INTRO				29
CONST_INT	LBOOL7_DONE_CUTSCENE_HACKING_INTRO_INIT			30
CONST_INT	LBOOL7_HACKING_MINIGAME_COMPLETE				31

INT iLocalBoolCheck7

CONST_INT	LBOOL8_SKIPPED_CUTSCENE_BECAUSE_TOO_FAR				0
CONST_INT	LBOOL8_HANDLE_PLAYLIST_POSITION_DATA_WRITE_CALLED	1
CONST_INT	LBOOL8_INITIAL_IPL_SET_UP_COMPLETE					2
CONST_INT	LBOOL8_vault_door_sound								3
CONST_INT	LBOOL8_DEALT_WITH_HEIST_RESTART						4
CONST_INT	LBOOL8_CLEARED_HEADSHOTS							5
CONST_INT 	LBOOL8_DONE_HEIST_AWARD_HELP_CHECK					6
CONST_INT	LBOOL8_CAMERA_EXIT_STATE_CHECK						7
CONST_INT	LBOOL8_forces_applied_to_casco_vehicle				8
CONST_INT	LBOOL8_SHOULD_CELEBRATION_BE_SHOWN_PREMATURELY		9
CONST_INT	LBOOL8_REQUEST_LOAD_FMMC_MODELS_STAGGERED_DONE		10
CONST_INT	LBOOL8_skipped_to_mid_mission_cutscene_stage_5		11
CONST_INT	LBOOL8_TUT_EXT_SRL_STARTED							12
CONST_INT	LBOOL8_TREVOR_COVER_CLIPSET_LOADING					13
CONST_INT	LBOOL8_TREVOR_COVER_CLIPSET_LOADED					14
CONST_INT	LBOOL8_TREVOR_COVER_CLIPSET_ACTIVE					15
CONST_INT	LBOOL8_MANUAL_PLAYER_EXIT_STATE_DONE				16
CONST_INT	LBOOL8_PLAYER_EXIT_STATE_CALLED						17
CONST_INT	LBOOL8_HEIST_DRILL_ASSET_LOADED						18
CONST_INT	LBOOL8_LOC_LEAVE_TEXT_CHECK							19
CONST_INT	LBOOL8_OUTRO_DONE									20
CONST_INT	LBOOL8_OUTRO_OK_TO_DO								21
CONST_INT	LBOOL8_ENTITIES_UNFROZEN_AT_START_OF_MISSION		22
CONST_INT	LBOOL8_HUMANE_DELIVER_EMP_PER_SHOT_FLAG				23
CONST_INT	LBOOL8_MANUAL_PLACE_PED_IN_VEH						24
CONST_INT	LBOOL8_NO_MOCAP_AFTER_THIS_DROPOFF					25
CONST_INT	LBOOL8_HAS_QUICK_RESTARTED_IN_PV					26
CONST_INT	LBOOL8_HAS_START_POSITION_WARPED					27
const_int   LBOOL8_local_player_warped_into_casco				28
CONST_INT	LBOOL8_CONVERSATION_INTERRUPTED						29
CONST_INT	tracked_vehicle_damage_health_percentage_cached		30
CONST_INT	LBOOL8_NAVMESH_REGION_REQUESTED						31

INT iLocalBoolCheck8

CONST_INT	LBOOL9_FORCE_STREAM_CUTSCENE        				0
CONST_INT	LBOOL9_TIME_SET        								1
CONST_INT	LBOOL9_GPS_MULTI_ROUTE_ACTIVE						2
CONST_INT	LBOOL9_COPIED_CELEBRATION_DATA_EVENT_1				3
CONST_INT	LBOOL9_COPIED_CELEBRATION_DATA_EVENT_2				4
CONST_INT	LBOOL9_PLAYER_WARPED_INTO_PREDATOR					5
CONST_INT	LBOOL9_STARTING_PHONE_ANIM							6
CONST_INT	LBOOL9_PREDATOR_RADIO_OFF							7
CONST_INT	LBOOL9_CUTSCENE_SNOW_OFF							8
CONST_INT	LBOOL9_DO_INSTANT_DIALOGUE_LOOP						9
CONST_INT	LBOOL9_MIDPOINT_AUDIO_MIX_PROCESSED					10
CONST_INT	LBOOL9_MASK_REMOVE_MID_CUT							11
CONST_INT 	LBOOL9_HAS_FIRST_OBJECTIVE_TEXT_BEEN_SHOWN			12
CONST_INT	LBOOL9_MUSIC_STOPPED_TUT_EXT						13
CONST_INT   LBOOL9_CACHED_WEAPON_DATA							14
CONST_INT	LBOOL9_HEADSHOTS_REGENERATED						15
CONST_INT	LBOOL9_PRE_MOCAP_CINE_TRIGGERED						16
CONST_INT	LBOOL9_CHECK_FOR_TRIGGERING_PRE_MOCAP				17
CONST_INT	LBOOL9_SET_IGNORE_NO_GPS_FLAG						18
CONST_INT 	LBOOL9_SHOULD_STOP_VEHICLE							19
CONST_INT 	LBOOL9_AT_LEAST_ONE_GANG_CHASE_UNIT_SPAWNED				20
CONST_INT 	LBOOL9_SET_STATS_SCREEN_START_AUDIO					21
CONST_INT 	LBOOL9_SET_STATS_SCREEN_STOP_AUDIO					22
CONST_INT	LBOOL9_CAN_SHOW_MANUAL_RESPAWN_HELP_TEXT			23
// Free
CONST_INT   LBOOL9_fleeca_bank_door_anim_triggered				25
CONST_INT	LBOOL9_PREPARE_DANGER_ZONE							26
CONST_INT	LBOOL9_DANGER_ZONE_TRIGGERED						27
CONST_INT	LBOOL9_SCTV_LOADDED_FMMC_BLOCK						28
CONST_INT	LBOOL9_MASK_REPLACED_WITH_HELMET					29
CONST_INT	LBOOL9_ACTION_MODE_OVERRIDDEN_TO_STOP_ARMOUR_CLIPPING	30
CONST_INT	LBOOL9_KNOCKING_QUIETED								31

INT iLocalBoolCheck9

CONST_INT	LBOOL10_CURRENTLY_ON_APARTMENT_GO_TO				0
CONST_INT	LBOOL10_TRIGGERED_DANGER_ZONE_FADE_OUT				1
CONST_INT	LBOOL10_container_lock_shot_audio_loaded			2
CONST_INT	LBOOL10_ALL_SHOPS_LOCKED							3
CONST_INT	LBOOL10_RELEASE_LOCK_SHOT_OFF_AUDIO_BANK			4
CONST_INT	LBOOL10_PLAYER_IN_TURRET_SEAT						5
CONST_INT	LBOOL10_PLAYER_WEARING_MASK							6
CONST_INT	LBOOL10_TUT_CAR_EXT_PLAYER_TOO_FAR					7
CONST_INT	LBOOL10_PAC_FIN_PARACHUTE_GIVEN						8
CONST_INT	LBOOL10_PLAYER_ACTION_MODE_RESET					9
CONST_INT	LBOOL10_VEH_SPAWN_AT_SECOND_POSITION				10
CONST_INT	LBOOL10_PED_SPAWN_AT_SECOND_POSITION				11
CONST_INT	LBOOL10_OBJ_SPAWN_AT_SECOND_POSITION				12
CONST_INT	LBOOL10_IN_MAP_BOUNDS								13
CONST_INT	LBOOL10_DROP_OFF_LOC_FADE_OUT						14
CONST_INT	LBOOL10_DROP_OFF_LOC_NO_DISPLAY						15
CONST_INT 	LBOOL10_DROP_OFF_LOC_DISPLAY						16
CONST_INT	LBOOL10_GOTO_LOC_POSTFX								17
CONST_INT	LBOOL10_HUMANE_FLARE_PROCESSING_DONE				18
CONST_INT	LBOOL10_JUST_DONE_DROPOFF_POSTFX					19
CONST_INT 	LBOOL10_STOP_CAR_FOREVER							20
CONST_INT 	LBOOL10_DRIVING_AND_CARRYING_PED					21
CONST_INT	LBOOL10_REGENERATED_LEADERBOARD_HEADSHOTS			22
CONST_INT	LBOOL10_HEIST_HELP_TEXT_AVI_RASHKOVSKY				23
CONST_INT	LBOOL10_LOCAL_PLAYER_IN_CUTSCENE					24
CONST_INT	LBOOL10_HUMANE_FLARE_CREATED						25
CONST_INT	LBOOL10_PLAYED_CELEB_PRE_LOAD_FOR_HUD_CLEAR			26
CONST_INT	LBOOL10_HEIST_HELP_TEXT_INSURGENT_TECHNICAL			27
CONST_INT	LBOOL10_RESPAWNING_DURING_FAIL						28
CONST_INT	LBOOL10_PLACED_REVERSE_ANGLE_CAMERA					29
CONST_INT	LBOOL10_PROCESSED_HEIST_END_OF_MISSION				30
CONST_INT	LBOOL10_NEED_TO_CHECK_IN_VEH_FOR_DUMMY_BLIPS		31

INT iLocalBoolCheck10

CONST_INT	LBOOL11_WAS_IN_VEH_FOR_DUMMY_BLIPS					0
CONST_INT	LBOOL11_NEED_TO_CHECK_IN_VEH_FOR_DUMMY_BLIPS_TEMP	1
CONST_INT	LBOOL11_FLEECA_SCOPEOUT_CUTSCENE_INTRO_ANIM_TASKED	2
CONST_INT	LBOOL11_FLEECA_SCOPEOUT_CUTSCENE_LOOP_ANIM_TASKED	3
CONST_INT	LBOOL11_FLEECA_SCOPEOUT_CUTSCENE_OUTRO_ANIM_TASKED	4
CONST_INT	LBOOL11_FLEECA_SCOPEOUT_CUTSCENE_ANIM_REMOVED		5
CONST_INT	LBOOL11_PRISON_FINALE_PLANE_TASK					6
CONST_INT	LBOOL11_STOP_MISSION_FAILING_DUE_TO_EARLY_CELEBRATION	7
CONST_INT	LBOOL11_FORCE_CAMERA_EXIT_STATE						8
CONST_INT	LBOOL11_BIG_MAP_WAS_ZOOMED_IN_FOR_CARRIER			9
CONST_INT	LBOOL11_TREVOR_TATTOO_APPLIED						10
CONST_INT	LBOOL11_TREVOR_VAN_IDLE_CLIPSET_LOADED				11
CONST_INT	LBOOL11_TREVOR_VAN_CLIPSET_APPLIED					12
CONST_INT	LBOOL11_SET_SHOULD_STOP_IN_DROP_OFF					13
CONST_INT	LBOOL11_REGEN_PLAYER_HEADSHOTS                      14
CONST_INT 	LBOOL11_WAS_ATTACHED_TO_CARGOBOB					15
CONST_INT	LBOOL11_BIG_MAP_DO_ZOOM_IN_FOR_CARRIER				16
const_int	LBOOL11_fleeca_finale_warp_player_for_streaming		17
CONST_INT 	LBOOL11_IN_VALID_VEHICLE_FOR_WANTED_BOUNDS1			18
CONST_INT 	LBOOL11_IN_VALID_VEHICLE_FOR_WANTED_BOUNDS2			19
CONST_INT	LBOOL11_RETRY_CHECKPOINT_3							20
CONST_INT	LBOOL11_RETRY_CHECKPOINT_4							21
CONST_INT	LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME			22 // This gets set on the host when the midpoint is updated - used to stop things from spawning as it's a new midpoint, and things should clean up first
CONST_INT	LBOOL11_MASK_EQUIP_ON_QUICK_RESTART			        23
CONST_INT	LBOOL11_BLOCK_LOOK_AT_PLAYER_TASK			        24
CONST_INT	LBOOL11_PAC_FIN_LOW_BUDGETS							25
CONST_INT   LBOOL11_CLIENT_GETS_HOST_CUTSCENE_TIMER             26
CONST_INT	LBOOL11_CARRIER_LIGHTS_PRELOADED					27
CONST_INT	LBOOL11_TOGGLE_RENDER_PHASE_NEXT_FRAME				28
CONST_INT	LBOOL11_PAC_FIN_GIVEN_WEAPON						29
CONST_INT	LBOOL11_PLAYER_IS_RESPAWNING						30
const_int	LBOOL11_fleeca_finale_end_mocap_fade_in				31

INT iLocalBoolCheck11

CONST_INT	LBOOL12_OBJ_RES_LAPTOP				                0
CONST_INT	LBOOL12_OBJ_RES_BAG 				                1
CONST_INT	LBOOL12_OBJ_RES_HACK_CARD 				            2
CONST_INT	LBOOL12_OBJ_RES_SYNC_LOCK_CARD 				        3
CONST_INT	LBOOL12_OBJ_RES_MOBILE_PHONE 				        4
CONST_INT	LBOOL12_OBJ_RES_DRILL_WALL_AND_DOOR 				5
CONST_INT	LBOOL12_OBJ_RES_KEYCARD 				            6
CONST_INT	LBOOL12_OBJ_RES_UNDERWATER_FLARE 				    7
CONST_INT	LBOOL12_OBJ_RES_APARTMENT_DOOR 				        8
CONST_INT	LBOOL12_OBJ_RES_DRILL            			        9
CONST_INT	LBOOL12_OBJ_RES_DRILL_BAG            			    10
CONST_INT	LBOOL12_OBJ_RES_DRILL_BOX            			    11
CONST_INT	LBOOL12_OBJ_RES_DRILL_HOLE            			    12
CONST_INT	LBOOL12_OBJ_RES_THERMITE_CHARGE           			13
CONST_INT	LBOOL12_OBJ_RES_THERMITE_CHARGE_FLASH               14
CONST_INT	LBOOL12_OBJ_RES_MONEY                               15
CONST_INT	LBOOL12_RETRY_CHECKPOINT_1							16
CONST_INT	LBOOL12_RETRY_CHECKPOINT_2							17
CONST_INT	LBOOL12_DONE_HELMET_VISOR_UP_ON_INIT				18
CONST_INT	LBOOL12_FINALISE_HEADBLEND                          19
CONST_INT	LBOOL12_TREVOR_COVER_ANIMS_LOADED					20
CONST_INT	LBOOL12_HEIST_LEADER_CHEAT_CHECK_DONE				21
CONST_INT 	LBOOL12_LOADING_SPINNER_FOR_DROPOFF					22
CONST_INT	LBOOL12_STATION_DRUNK_PED_STREAM_STARTED            23
CONST_INT   LBOOL12_SERVER_STILL_THINKS_IM_HACKING				24
CONST_INT   LBOOL12_SERVER_STILL_THINKS_IM_HACKING_TEMP			25
CONST_INT	LBOOL12_SERVER_TEAM_0_IS_HACKING_TEMP				26
CONST_INT	LBOOL12_SERVER_TEAM_1_IS_HACKING_TEMP				27
CONST_INT	LBOOL12_SERVER_TEAM_2_IS_HACKING_TEMP				28
CONST_INT	LBOOL12_SERVER_TEAM_3_IS_HACKING_TEMP				29
CONST_INT	LBOOL12_EMP_AIRCRAFT_CHECKPOINT_2_DOOR_OPEN			30
CONST_INT	LBOOL12_EMP_AIRCRAFT_CHECKPOINT_1_DOOR_OPEN			31

INT iLocalBoolCheck12

CONST_INT	LBOOL13_IN_BLOCK_WANTED_LEVEL_ZONE					0
CONST_INT	LBOOL13_IN_BLOCK_AND_CLEAR_WANTED_LEVEL_ZONE		1
CONST_INT	LBOOL13_IN_BLOCK_WANTED_LEVEL_ZONE_TEMP				2
CONST_INT	LBOOL13_NOT_SAFE_FOR_ALTERNATE_CAM					3
CONST_INT	LBOOL13_UNTOGGLED_RENDERPHASES_BEFORE_LEADERBOARD	4
CONST_INT	LBOOL13_DISPLAYED_INTRO_SHARD						5
CONST_INT	LBOOL13_CONTROL_PAD_COLOUR_SHOULD_BE_RESET			6
CONST_INT   LBOOL13_FIRST_STAGGERED_LOOP_DONE_CLIENTSIDE        7
CONST_INT	LBOOL13_RESPAWNBOUNDS1_CHOSEN_IN_FRONT				8
CONST_INT	LBOOL13_RESPAWNBOUNDS1_CHOSEN_NOT_IN_FRONT			9
CONST_INT	LBOOL13_RESPAWNBOUNDS2_CHOSEN_IN_FRONT				8
CONST_INT	LBOOL13_RESPAWNBOUNDS2_CHOSEN_NOT_IN_FRONT			9
CONST_INT	LBOOL13_DRAW_DISTANCE_TO_CROSS_LINE					10
CONST_INT	LBOOL13_HIDE_CROSS_THE_LINE_HELP_PROMPT				11
CONST_INT	LBOOL13_MIN_SPEED_EXPLODE_BAR_INCREASING			12
CONST_INT	LBOOL13_EARLY_STREAM_RESPAWN_BAR					13
CONST_INT	LBOOL13_DONE_DIALOGUE_LOOP_AFTER_COPS_SPOTTED		14
CONST_INT	LBOOL13_SHOW_HEIST_SPECTATE_HELP					15
CONST_INT	LBOOL13_PRIORTISED_FVJ_HELPTEXT						16
CONST_INT	LBOOL13_PRIORTISED_THANKSGIVING_HELPTEXT			17
CONST_INT	LBOOL13_SHOWN_TEAM_SHARD							18
CONST_INT	LBOOL13_PLAYED_STOP_HUD_SFX							19
CONST_INT	LBOOL13_PLAYERINPEDBLOCKINGZONE						20
CONST_INT	LBOOL13_PLAYERINPEDBLOCKINGZONE_NEWMAXWANTEDSET		21
CONST_INT	LBOOL13_CANADDPEDTOGROUP_JOINGROUPFROMVAN_DOORSBLOWN	22
CONST_INT	LBOOL13_CANADDPEDTOGROUP_JOINGROUPFROMVAN_STANDARDCHECK	23
CONST_INT  	LBOOL13_BEEN_GIVEN_MIDMISSION_INVENTORY_2			24
CONST_INT	LBOOL13_REINITTED_MUSIC_FOR_ENTERING_SPECTATE		25
CONST_INT	LBOOL13_HWN_SWITCHOVER_COUNTDOWN_PLAYED				26
CONST_INT	LBOOL13_HWN_ENDGAME_COUNTDOWN_PLAYED				27
CONST_INT	LBOOL13_HWN_COUNTDOWN_SWITCHED						28
CONST_INT 	LBOOL13_ARMING_COUNTDOWN_CALLED						29
CONST_INT	LBOOL13_BOMB_ARMED_AFTER_COUNTDOWN					30
CONST_INT	LBOOL13_MY_TEAM_HAS_A_NEW_LAP						31

INT iLocalBoolCheck13

CONST_INT	LBOOL14_OTHER_TEAM_IN_BEAST_MODE					0
CONST_INT	LBOOL14_IN_BEAST_MODE								1
CONST_INT	LBOOL14_BEAST_STEALTH_TOGGLE						2
CONST_INT	LBOOL14_IN_BEAST_COSTUME							3
CONST_INT	LBOOL14_BEAST_IS_STEALTHED							4
CONST_INT	LBOOL14_ALLOW_LESTER_SNAKE_MINIGAME					5
CONST_INT	LBOOL14_HAS_FORCED_OFF_LOCK_ON						6
CONST_INT	LBOOL14_CAN_SHOW_BEAST_POWERS_UNAVAILABLE_HELP_TEXT	7
CONST_INT 	LBOOL14_BLIP_ON_LOCATE								8
CONST_INT	LBOOL14_DAMAGED_BY_SUPER_JUMP						9
CONST_INT	LBOOL14_VFX_CAPTURE_OWNER							10
CONST_INT	LBOOL14_BEAST_SFX_LOADED							11
CONST_INT	LBOOL14_BEAST_CUSTOM_CELEB_SOUNDS_ON				12
CONST_INT	LBOOL14_DZ_HAS_SET_CARGOBOB_RAMP_TO_LOWER			13
CONST_INT	LBOOL14_DONE_BEAST_LANDING_SOUND					14
CONST_INT	LBOOL14_INSURGENT_GUN_SEAT_HELP						15
CONST_INT	LBOOL14_HAS_STARTED_APT_SUDDEN_DEATH_MUSIC			16
CONST_INT	LBOOL14_HAS_KILLED_APT_SUDDEN_DEATH_MUSIC			17
CONST_INT	LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC				18
CONST_INT	LBOOL14_HAS_KILLED_APT_COUNTDOWN_MUSIC				19
CONST_INT	LBOOL14_COUNTDOWN_EARLY_MISSION_END					20
CONST_INT	LBOOL14_APT_STARTED_PRE_COUNTDOWN					21
CONST_INT	LBOOL14_DZ_STARTED_CARGOBOB_SOUNDS					22
CONST_INT	LBOOL14_PLAYED_5S_COUNTDOWN_ON_30S_SETTING_CD		23
CONST_INT	LBOOL14_PLAYED_5S_COUNTDOWN_ON_30S_SETTING_SD		24 //countdown version
CONST_INT	LBOOL14_OUT_OF_BOUNDS_DEATHFAIL						25 //suddendeath version
CONST_INT	LBOOL14_VEHICLE_ROCKET_ASSET_REQUEST				26
CONST_INT 	LBOOL14_THERMALVISION_INITIALISED					27

INT iLocalBoolCheck14

CONST_INT	LBOOL15_HAS_STARTED_APT_SUDDEN_DEATH_END			0
CONST_INT	LBOOL15_LAUNCHED_FROM_ON_YACHT						1
CONST_INT	LBOOL15_IS_OUT_OF_BOUNDS_SHARD_DISPLAYED			2
CONST_INT	LBOOL15_VEHICLE_MENU_WEAPON_ACTIVE_ROCKETS			3
CONST_INT	LBOOL15_VEHICLE_MENU_WEAPON_ACTIVE_BOOST			4
CONST_INT	LBOOL15_VEHICLE_MENU_WEAPON_ACTIVE_REPAIR			5
CONST_INT	LBOOL15_VEHICLE_MENU_WEAPON_ACTIVE_SPIKES			6
CONST_INT  	LBOOL15_IS_SUDDEN_DEATH_SOUND_PLAYING				7
CONST_INT   LBOOL15_HAS_ELIMINATION_COUNTDOWN_FINISHED			8
CONST_INT	LBOOL15_IS_SUDDEN_DEATH_BLIP_SETUP					9
CONST_INT	LBOOL15_SSDS_SUDDEN_DEATH_STARTED					10
CONST_INT	LBOOL15_HAS_STARTED_VAL_COUNTDOWN_MUSIC				11
CONST_INT	LBOOL15_HAS_KILLED_VAL_COUNTDOWN_MUSIC				12
CONST_INT	LBOOL15_TURN_BACK_ON_RADIO_CONTROL					13
CONST_INT	LBOOL15_HAS_PLAYED_SUMO_END_SOUND					14
CONST_INT	LBOOL15_SUMO_HUD_SOMEONE_DIED						15
CONST_INT	LBOOL15_ROUNDS_MISSION_END_SET_UP					16
CONST_INT	LBOOL15_DRAIN_HEALTH_INITIALISED					17
CONST_INT	LBOOL15_SUDDEN_DEATH_VALENTINES_ONE_HIT_KILL_INIT	18
CONST_INT	LBOOL15_CALL_SWAP_TEAM_LOGIC						19
CONST_INT	LBOOL15_HAS_STARTED_VAL_SUDDEN_DEATH_END			20
CONST_INT	LBOOL15_HAS_STARTED_VAL_SUDDEN_DEATH_FADE			21
CONST_INT	LBOOL15_HAS_LOWERED_CB_RAMP_1						22
CONST_INT	LBOOL15_HAS_LOWERED_CB_RAMP_2						23
CONST_INT	LBOOL15_HAS_LOWERED_CB_RAMP_3						24
CONST_INT	LBOOL15_HAS_LOWERED_CB_RAMP_4						25
CONST_INT	LBOOL15_HAS_LOWERED_CB_RAMP_5						26
CONST_INT	LBOOL15_HAS_LOWERED_CB_RAMP_6						27
CONST_INT	LBOOL15_HAS_LOWERED_CB_RAMP_7						28
CONST_INT	LBOOL15_HAS_LOWERED_CB_RAMP_8						29
CONST_INT	LBOOL15_RESET_30S_CD_BOOLS_NEXT_RULE				30
CONST_INT	LBOOL15_CALLED_POPULATE_FAIL_RESULTS				31

INT iLocalBoolCheck15

CONST_INT	LBOOL16_PLAYED_10S_SD_SFX							0
CONST_INT	LBOOL16_PLAYED_5S_SD_SFX							1
CONST_INT	LBOOL16_HAVE_RAGE_PICKUPS_BEEN_CLEANED_UP			2
CONST_INT	LBOOL16_TDF_PLAY_TEAM_TAKES_LEAD					3
CONST_INT	LBOOL16_BLIP_ON_COLLECT								4
CONST_INT	LBOOL16_HAS_STOPPED_MUSIC_ON_LEADERBOARD			5
CONST_INT	LBOOL16_TEAM_0_IS_PAST_THEIR_FIRST_RULE				6
CONST_INT	LBOOL16_TEAM_1_IS_PAST_THEIR_FIRST_RULE				7
CONST_INT	LBOOL16_TEAM_2_IS_PAST_THEIR_FIRST_RULE				8
CONST_INT	LBOOL16_TEAM_3_IS_PAST_THEIR_FIRST_RULE				9
CONST_INT	LBOOL16_HAS_SETUP_PP_ANNOUNCER						10
CONST_INT	LBOOL16_HAS_PLAYED_RUGBY_END_SOUND					11
CONST_INT	LBOOL16_HAS_SENT_RUGBY_PICKUP_SOUND_EVENT			12
CONST_INT 	LBOOL16_HAS_STARTED_BALL_CARRIER_LOOP_SOUND			13
CONST_INT	LBOOL16_HAS_PLAYED_IAO_PICKUP_SOUND					14
CONST_INT	LBOOL16_IS_DIVE_SCORE_ANIM_IN_PROGRESS				15
CONST_INT	LBOOL16_DIVE_SCORE_RENDERPAUSE_ACTIVE				16
CONST_INT	LBOOL16_DIVE_SCORE_SCREEN_FX_ACTIVE					17
CONST_INT	LBOOL16_IAO_BLOCK_PACKAGE_SOUNDS					18
CONST_INT	LBOOL16_SETTING_TEAM_AS_DRUNK_THIS_RULE				19
CONST_INT	LBOOL16_TDF_PLAY_PLAYER_ENTERS_CHECKPOINT			20
CONST_INT	LBOOL16_WEAPON_PICKUPS_BLOCKED						21
CONST_INT	LBOOL16_SWAN_DIVE_ANIM_VALIDATED					22
CONST_INT	LBOOL16_SUDDEN_DEATH_REASON_SHARD_SENT				23
CONST_INT	LBOOL16_HAS_RUGBY_SD_HELP_TEXT_FIRED				24
CONST_INT 	LBOOL16_YOU_ARE_THE_LAST_DELIVERER					25
CONST_INT	LBOOL16_INVINCIBLE_DURING_POINT_CAM					26
CONST_INT	LBOOL16_SUDDEN_DEATH_RUN_PLAYER_SCORE_UPDATE		27
CONST_INT	LBOOL16_CHANGE_PLAYER_TEAM_ALIVE_VARS_SETTER		28
CONST_INT	LBOOL16_SHOULD_SHOW_TEAM_SWITCH_SHARD				29
CONST_INT	LBOOL16_DELETED_PICKUPS_ON_TEAM_SWAP				30
CONST_INT	LBOOL16_VEHICLE_MARKED_FOR_CLEAN_UP					31

INT iLocalBoolCheck16

CONST_INT 	LBOOL17_PREVENT_VEHICLE_MANUAL_RESPAWN				0
CONST_INT 	LBOOL17_PREVENT_PICKUP_OF_PICKUP				    1
CONST_INT	LBOOL17_CAN_FORCE_PLAYER_TO_JUMP_FROM_CARGOBOB		2
CONST_INT	LBOOL17_HAS_PLAYED_PP_INTRO_SFX						3
CONST_INT 	LBOOL17_OCCUPANT_EXPLOSION_DMG						4
CONST_INT 	LBOOL17_SHOULD_CLEAN_UP_BEAST_MODE					5
CONST_INT	LBOOL17_HAS_GRABBED_STARTING_POINTLESS_PACKAGE		7
CONST_INT	LBOOL17_HAS_PLAYED_PP_INTRO_ANNOUNCER				8
CONST_INT	LBOOL17_HAS_PLAYED_PP_SUDDEN_DEATH_ANNOUNCER		9
CONST_INT	LBOOL17_HAS_PLAYED_PP_WINNER_ANNOUNCER				10
CONST_INT	LBOOL17_HAS_PLAYED_PP_PRE_INTRO_ANNOUNCER			11
CONST_INT	LBOOL17_HAS_PLAYED_PP_EXIT_ANNOUNCER				12
CONST_INT	LBOOL17_PROPS_HAVE_BEEN_HIDDEN						13
CONST_INT	LBOOL17_HUD_HIDDEN_AT_START_ONCE					14
CONST_INT	LBOOL17_POWER_PLAY_HUD_HAS_BEEN_CLEARED_UP			15
CONST_INT	LBOOL17_HAS_PICKED_PP_INTRO_ANNOUNCER				16
CONST_INT	LBOOL17_HAS_PICKED_PP_PRE_INTRO_ANNOUNCER			17
CONST_INT	LBOOL17_HAS_PICKED_PP_SUDDDEATH_ANNOUNCER			18
CONST_INT	LBOOL17_HAS_PICKED_PP_WINNER_ANNOUNCER				19
CONST_INT	LBOOL17_HAS_PICKED_PP_EXIT_ANNOUNCER				20
CONST_INT	LBOOL17_KILL_CONVERSATIONS_BEFORE_WINNER_ANNOUNCER	21
CONST_INT	LBOOL17_CLEANUP_BEAST_MODE_ON_RESPAWN				22
CONST_INT	LBOOL17_IS_PLAY_OUT_OF_MAIN_BOUNDS					23
CONST_INT	LBOOL17_MAX_BEASTS_ACTIVE							24
CONST_INT	LBOOL17_IS_POWER_PLAY_SHARD_VISIBLE					25
CONST_INT	LBOOL17_TEAM_SWAP_PICKUP_PROCESS					26
CONST_INT	LBOOL17_WEAPON_PICKUP_STAGGERED_LOOP_INDEX_SET		27
CONST_INT	LBOOL17_IS_VW_SHARD_VISIBLE							28
CONST_INT	LBOOL17_VV_HUD_HAS_BEEN_CLEARED_UP					29
CONST_INT	LBOOL17_DR_REQUESTED_ANIM							30
CONST_INT	LBOOL17_KILLED_CONVERSATIONS_FOR_PP_SD_ANN			31

INT iLocalBoolCheck17

CONST_INT	LBOOL18_GUNSMITH_NEW_WEAPON_READY					0
CONST_INT	LBOOL18_RULE_CHANGE_SHARD_PLAYED_TEAM_0				1
CONST_INT	LBOOL18_RULE_CHANGE_SHARD_PLAYED_TEAM_1				2
CONST_INT	LBOOL18_RULE_CHANGE_SHARD_PLAYED_TEAM_2				3
CONST_INT	LBOOL18_RULE_CHANGE_SHARD_PLAYED_TEAM_3				4
CONST_INT	LBOOL18_WEAPON_LOADOUT_READY						5
CONST_INT	LBOOL18_TLAD_SUDDEN_DEATH_WEAPONS					6
CONST_INT	LBOOL18_RULE_CHANGE_SHARD_FIRST_RULE				7
CONST_INT	LBOOL18_IS_SPEED_BOOST_FILTER_DISABLED				8
CONST_INT	LBOOL18_GUNSMITH_REPLENISH_AMMO_REQUIRED			9
CONST_INT	LBOOL18_MELEE_DAMAGE_MODIFIER_RESPAWN				10
CONST_INT	LBOOL18_GUNSMITH_EQUIPPING_WEAPON					11
CONST_INT	LBOOL18_TDF_PLAY_BOOST_SOUND						12
CONST_INT	LBOOL18_ENABLE_PRON_AUDIO_SCENE						13
CONST_INT	LBOOL18_ENABLE_PRON_BIKE_SOUNDS						14
CONST_INT 	LBOOL18_TIMECYCLE_SET								15
CONST_INT	LBOOL18_SD_TIMECYCLE_SET							16
CONST_INT	LBOOL18_TLAD_LIGHTNING								17
CONST_INT	LBOOL18_SPEED_BOOST_AVAILABLE						18
CONST_INT	LBOOL18_SPEED_BOOST_SUCCESS							19
CONST_INT	LBOOL18_TEAM_MEMBER_REQDIST_CHECKED_THIS_FRAME		20
CONST_INT	LBOOL18_TEAM_MEMBER_REQDIST_PASSED_THIS_FRAME		21
CONST_INT	LBOOL18_TLAD_SD_UI_CLEARED							22
CONST_INT	LBOOL18_TLAD_HUD_HAS_BEEN_CLEARED_UP				23
CONST_INT	LBOOL18_PRON_AUDIO_LOADED							24
CONST_INT	LBOOL18_TLAD_JUDGEMENT_DAY_PTFX_ACTIVE				25
CONST_INT	LBOOL18_TEAM_MEMBER_REQDIST_PASSED_LAST_FRAME		26 
CONST_INT	LBOOL18_ENABLE_GUNSMITH_AUDIO_SOUNDS				27
CONST_INT	LBOOL18_WEAPONS_ENABLED_SHARD_PLAY					28
CONST_INT	LBOOL18_CHECK_FOR_DEATH_OBJ_TEXT_REPRINT			29
CONST_INT	LBOOL18_DIED_AND_REQUIRES_OBJ_TEXT_REPRINT			30
CONST_INT	LBOOL18_LOADED_TOUR_DE_FORCE_SOUNDS					31

INT iLocalBoolCheck18

CONST_INT	LBOOL19_LOADED_TOUR_DE_FORCE_VFX_SLIPSTREAM_OVERRIDE	0
CONST_INT	LBOOL19_TLAD_ANGEL_GLOW_ACTIVE							1
CONST_INT	LBOOL19_TLAD_DEVIL_FLAME_FEET_ACTIVE					2
CONST_INT	LBOOL19_PLAY_LEADER_SLIPSTREAM_AUDIO					3
CONST_INT	LBOOL19_PLAY_FOLLOWER_SLIPSTREAM_AUDIO					4
CONST_INT	LBOOL19_PLAYER_SHOULD_NOT_GO_TO_SUMO_SUDDEN_DEATH		5
CONST_INT	LBOOL19_GUNSMITH_WEAPON_EQUIP_EFFECTS_BLOCKER			6
CONST_INT	LBOOL19_EQUIP_REQUIRES_PTFX								7
CONST_INT	LBOOL19_NEED_TO_SHOW_LAP_SHARD							8
CONST_INT	LBOOL19_SHOULD_EXTRA_HUD_BE_SHOWING						9
CONST_INT	LBOOL19_RACE_SPLIT_TIMES_STARTED						10
CONST_INT	LBOOL19_CHECKED_DROPOFF_CUTSCENE						11
CONST_INT	LBOOL19_SET_UP_VEHICLE_FOR_STUNT_RACE					12
CONST_INT	LBOOL19_TLAD_BLOCK_WEAPON_PFX							13
CONST_INT	LBOOL19_TURFWAR_PROPS_RESET_SUDDEN_DEATH				14
CONST_INT	LBOOL19_SHOW_LAST_LAP_HELP_TEXT							15
CONST_INT	LBOOL19_SLIPSTREAM_PASSED_CHECKPOINT					16
CONST_INT	LBOOL19_MUSIC_COUNTDOWN_INITIATED						17
CONST_INT	LBOOL19_TURFWAR_END_OF_ROUND_POINTS						18
CONST_INT	LBOOL19_LOST_DAMNED_TIME_SET_TO_NIGHT					19
CONST_INT	LBOOL19_ALL_POINTLESS_PACKAGES_GRABBED					20
CONST_INT 	LBOOL19_PTL_WIN_TIMER_STARTED							21
CONST_INT 	LBOOL19_IN_PRON_OUTFIT									22
CONST_INT	LBOOL19_CLEANUP_PRON_OUTFIT_ON_RESPAWN					23
CONST_INT 	LBOOL19_YACHT_AQUARIS_ASSETS_REQUESTED					24
CONST_INT 	LBOOL19_YACHT_AQUARIS_ASSETS_LOADED						25
CONST_INT 	LBOOL19_AUTO_PARACHUTE_ZONE_ACTIVATED					26
CONST_INT 	LBOOL19_VEHICLE_WEAPON_PICKUPS_CURRENTLY_BLOCKED		27
CONST_INT 	LBOOL19_CACHE_PLAYER_POS_FOR_GPS_TRACKING				28
CONST_INT	LBOOL19_TW_HUD_HAS_BEEN_CLEARED_UP						29
CONST_INT 	LBOOL19_TRIGGERED_CONTAINER_INVESTIGATION				31

INT iLocalBoolCheck19

CONST_INT	LBOOL20_INITIAL_SCORE_COUNT_DONE						0
CONST_INT	LBOOL20_MARK_PRON_BIKE_FOR_CLEANUP						1
CONST_INT	LBOOL20_JUGGERNAUT_LEAVING_SWAP_DONE					2
CONST_INT	LBOOL20_OBJ_RES_CONTAINER_PICK_UP				        3
CONST_INT	LBOOL20_OBJ_CRE_CONTAINER_PICK_UP				        4
CONST_INT	LBOOL20_POINTLESS_10S_COUNTDOWN_STARTED					5
CONST_INT	LBOOL20_POINTLESS_COUNTDOWN_SHARD						6
CONST_INT	LBOOL20_TURF_WAR_DISABLE_PROP_CLAIM_LOCALLY				7
CONST_INT	LBOOL20_TURF_WAR_DISABLE_PROP_ROUND_END					8
CONST_INT	LBOOL20_ROCKETS_FIRED_ONCE								9
CONST_INT	LBOOL20_HACKING_LOOP_STARTED							10
CONST_INT	LBOOL20_HAS_STARTED_IE_COUNTDOWN_MUSIC					11
CONST_INT 	LBOOL20_HAS_KILLED_IE_COUNTDOWN_MUSIC					12
CONST_INT 	LBOOL20_HAS_TURF_WARS_SD_HELP_TEXT_FIRED				13
CONST_INT	LBOOL20_PLAYER_PROOFS_ALTERED							14
CONST_INT	LBOOL20_JUGGERNAUT_SUIT_MOVEMENT_SET					15
CONST_INT	LBOOL20_START_TURFWAR_TIMER_COUNTDOWN					16
CONST_INT	LBOOL20_CARGOBOB_POSITIONED								17
CONST_INT	LBOOL20_CARGOBOB_CATCH_FINISHED							18
CONST_INT	LBOOL20_TURFWAR_ICON_SHOULD_PULSE_0						19
CONST_INT	LBOOL20_TURFWAR_ICON_SHOULD_PULSE_1						20
CONST_INT	LBOOL20_TURFWAR_ICON_SHOULD_PULSE_2						21
CONST_INT	LBOOL20_TURFWAR_ICON_SHOULD_PULSE_3						22
CONST_INT	LBOOL20_TURFWAR_ICON_START_PULSE						23
CONST_INT	LBOOL20_TURNED_OFF_RADIO_FOR_END_CUTSCENE				24
CONST_INT	LBOOL20_CASH_BAG_IS_FULL								25
CONST_INT	LBOOL20_PARTICLE_FX_OVERRIDE_COCAINE					26
CONST_INT	LBOOL20_SKIP_HELP_TEXT									27
CONST_INT	LBOOL20_USED_VEHICLE_WEAPON_ON_THIS_PRESS				28
CONST_INT	LBOOL20_PLAY_GHOST_AUDIO_EVENT							29
CONST_INT	LBOOL20_SHOWN_BAG_FULL_HELP								30
CONST_INT	LBOOL20_PARTICLE_FX_OVERRIDE_RPG						31

INT iLocalBoolCheck20

CONST_INT	LBOOL21_SET_CAR_HIGHER_JUMP								0
CONST_INT	LBOOL21_PRON_PUSH_FORWARD_ACTIVE						1
CONST_INT	LBOOL21_TOGGLE_CAN_COLLECT_PICKUP						2
CONST_INT	LBOOL21_USING_CUSTOM_RESPAWN_POINTS						3
CONST_INT	LBOOL21_SHOULD_CLEAR_CUSTOM_RESPAWN_POINTS				4
CONST_INT	LBOOL21_USING_YACHT_APARMENT_ASSETS						5
CONST_INT	LBOOL21_RUINER_2000_WEAPON_SET							6
CONST_INT	LBOOL21_DUNE4_RAMP_BUGGY_SIDE_IMPULSE_ACTIVE			7
CONST_INT	LBOOL21_TINY_RACERS_LAST_PLAYER_PROCESSED				8
CONST_INT	LBOOL21_SPECTATOR_RESPAWN_REQUESTED						9
CONST_INT 	LBOOL21_SPECTATOR_RESPAWN_WAIT_IN_SPECTATE				10
CONST_INT 	LBOOL21_SPECTATOR_RESPAWN_FORCE_RESPAWN					11
CONST_INT	LBOOL21_TINY_RACERS_ONE_PLAYER_LEFT						12
CONST_INT	LBOOL21_TINY_RACERS_SCORE_INCREMENTED					13
CONST_INT	LBOOL21_MY_DRIVER_IS_MANUALLY_RESPAWNING				14
CONST_INT	LBOOL21_TINY_RACERS_SPAWN_POINT_SET						15
// FREEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
CONST_INT	LBOOL21_PREVENT_COUNTDOWN_FROM_STARTING					17
CONST_INT	LBOOL21_TINY_RACERS_ROUND_SHARD_SHOWN					18
CONST_INT	LBOOL21_TINY_RACERS_LOOK_BACK_SET						19
CONST_INT	LBOOL21_TINY_RACERS_PREVENT_321_CONTROL_DISABLE			20
CONST_INT	LBOOL21_TINY_RACERS_SHRINKING_BOUNDS_SIZE_SET			21
CONST_INT	LBOOL21_TINY_RACERS_INITIAL_COUNTDOWN_OVER				22
CONST_INT	LBOOL21_VEHICLE_SWAP_ENGAGED							23
CONST_INT	LBOOL21_VEHICLE_SWAP_RETRACTABLE_WHEEL_BUTTON			24
CONST_INT	LBOOL21_VEHICLE_DAMAGE_MODIFIER_OUTPUT_RESPAWN			25
CONST_INT	LBOOL21_VEHICLE_DAMAGE_MODIFIER_INTAKE_RESPAWN			26
CONST_INT	LBOOL21_RESURRECTION_HUD_SHOW_ENEMY_TEAM				27
CONST_INT	LBOOL21_TOP_DOWN_CONTROLS_BEING_USED					28
CONST_INT	LBOOL21_VEHICLE_HEALTH_UDPATED							29
CONST_INT	LBOOL21_PARTICLE_FX_OVERRIDE_BOX_PILE					30
CONST_INT	LBOOL21_TINY_RACERS_ALLOW_ANCHOR						31

INT iLocalBoolCheck21

CONST_INT	LBOOL22_SWITCHED_ROCKET_TYPE							0
CONST_INT	LBOOL22_SWITCHED_ROCKET_AMMO							1
CONST_INT	LBOOL22_VEHICLE_AMMO_RELOAD_ENGAGED						2
CONST_INT	LBOOL22_USING_CUSTOM_VEHICLE_AMMO						3
CONST_INT	LBOOL22_TOP_DOWN_CAM_BLOCK_WEAPON_TOGGLE				4
CONST_INT	LBOOL22_START_PAUSE_CAM_FOR_RESET						5
CONST_INT	LBOOL22_SWAPPED_TO_POWERUP_VEHICLE						6
CONST_INT	LBOOL22_TOGGLE_PROP_CLAIM_COVER_TARGET					7
CONST_INT	LBOOL22_TOGGLE_PROP_FRICTION_LEVEL						8
CONST_INT	LBOOL22_TINY_RACERS_CAMERA_IN_POSITION					9
CONST_INT	LBOOL22_TINY_RACERS_INTRO_CAM_FINISHED					10
CONST_INT	LBOOL22_OVERTIME_ROUND_START_SHARD						11
CONST_INT	LBOOL22_OVERTIME_VEHICLE_DESTROYED						12
CONST_INT	LBOOL22_OVERTIME_LANDING_FAILED							13
CONST_INT	LBOOL22_NEW_OT_ROUND_THIS_FRAME							14
CONST_INT	LBOOL22_TINY_RACERS_FREEZE_CAMERA_ROTATION				15
CONST_INT	LBOOL22_TINY_RACERS_EASE_CAMERA_ROTATION				16
CONST_INT	LBOOL22_TINY_RACERS_OK_TO_PROCESS_BOUNDS				17
CONST_INT	LBOOL22_OVERTIME_NEW_TURN_SHARD							18
CONST_INT	LBOOL22_OVERTIME_WAIT_A_FRAME_FOR_TIMER_EXPIRE			19
CONST_INT	LBOOL22_OVERTIME_MY_TURN								20
CONST_INT	LBOOL22_TINY_RACERS_SPECTATOR_DISABLING_CAM_CHANGE		21
CONST_INT	LBOOL22_TINY_RACERS_RENDER_PAUSE_TARGET_DEAD			22
CONST_INT	LBOOL22_INSIDE_PREVIOUS_CHECKPOINT						23
CONST_INT	LBOOL22_INSIDE_NEXT_CHECKPOINT							24
CONST_INT	LBOOL22_LAST_PLAYER_ALIVE_SOUND_EFFECT					25
CONST_INT	LBOOL22_TOGGLE_PROP_CLAIM_COVER_TARGET_DIRECTION		26
CONST_INT	LBOOL22_TINY_RACERS_PLAY_DRAW_SHARD						27
CONST_INT	LBOOL22_VEHICLE_PETROL_TANK_DAMAGE_DISABLED				28
CONST_INT	LBOOL22_TINY_RACERS_CAM_SPECTATOR_MOTION_BLUR_OFF		29
CONST_INT	LBOOL22_OVERTIME_DELAYED_SCORE_SHARD					30
CONST_INT	LBOOL22_OVERTIME_CLEARED_SPECTATOR_ON_RESTART			31

INT iLocalBoolCheck22

CONST_INT	LBOOL23_OVERITME_SHOULD_PLAY_DELAYED_SCORE_SHARD_RUMBLE 0
CONST_INT	LBOOL23_FIRST_TURN_OF_NEW_ROUND							1
CONST_INT	LBOOL23_OVERTIME_SCORE_SHARD_ON_SCREEN					2
CONST_INT	LBOOL23_OVERTIME_ENTERED_BOUNDS							3
CONST_INT	LBOOL23_OVERTIME_RUMBLE_DELAY_FOR_MINI_ROUND_END	 	4
CONST_INT	LBOOL23_OVERTIME_INITIAL_SPECTATOR_DONE				 	5
CONST_INT	LBOOL23_VEHICLE_SET_EXPLOSION_PROOF					 	6
CONST_INT	LBOOL23_CLEANUP_OUTFIT_ON_RESPAWN					 	7
CONST_INT	LBOOL23_OVERTIME_SUDDEN_DEATH_LOCATES_CHANGED			8
CONST_INT	LBOOL23_OVERTIME_SUDDEN_DEATH_REACHED_FIRST_RULE		9
CONST_INT	LBOOL23_OVERTIME_SCORE_INCREMENTED_THIS_RULE			10
CONST_INT	LBOOL23_SOMEONE_IS_HACKING_A_VEHICLE					11
CONST_INT	LBOOL23_HAS_PLAYER_BEEN_GIVEN_A_REBREATHER				12
CONST_INT	LBOOL23_LOCATE_TEAM_SWAP_REQUESTED						13
CONST_INT	LBOOL23_LOCATE_TEAM_SWAP_INITIALISED					14
CONST_INT	LBOOL23_BLIP_FORCED_ON_BY_ZONE							15
CONST_INT	LBOOL23_OVERTIME_MODE_SWITCH_OBJ_BLOCK					16
CONST_INT	LBOOL23_OVERTIME_HELP_MESSAGE_PLAYED_THIS_TURN			17
CONST_INT	LBOOL23_OVERTIME_IGNORE_RESET_AT_START_DELAY			18
CONST_INT	LBOOL23_OVERTIME_SUDDEN_DEATH_SHARD						19
CONST_INT	LBOOL23_OVERTIME_SUDDEN_DEATH_HELP_SHOWN				20
CONST_INT	LBOOL23_OVERTIME_READY_TO_GHOST							21
CONST_INT	LBOOL23_ALERT_PED_ZONE_ACTIVATED						22
CONST_INT	LBOOL23_OVERTIME_INITIAL_GHOSTED						23
CONST_INT	LBOOL23_TEAM_SWAP_IN_MISSION							24
CONST_INT	LBOOL23_OVERTIME_VEHICLE_STUCK_FORCE_PROGRESS			25
CONST_INT	LBOOL23_SPEED_BAR_AIRSTRIKE_CALLED						26
CONST_INT	LBOOL23_AIRSTRIKE_COORDS_SET							27
CONST_INT	LBOOL23_AIRSTRIKE_SPEED_BAR_FIRED						28
CONST_INT	LBOOL23_OVERTIME_CHECK_SCORE_AFTER_SHARD				29
CONST_INT	LBOOL23_AIRSTRIKE_FIFTY_PERCENT_STRIKE					30
CONST_INT	LBOOL23_DIED_BEFORE_SUDDEN_DEATH						31

INT iLocalBoolCheck23

CONST_INT	LBOOL24_TINY_RACERS_TOD_RESET							0
CONST_INT	LBOOL24_WAIT_FOR_SCORE_SHARD							1
CONST_INT	LBOOL24_PREVENT_MANUAL_RESPAWN_HELP						2
CONST_INT	LBOOL24_MANUAL_RESPAWN_HELP_CLEARED_ONCE				3
CONST_INT	LBOOL24_OUTFIT_REMOVED_FOR_CELEB_SCREEN					4
CONST_INT	LBOOL24_FINAL_RP_REWARD_GIVEN							5
CONST_INT	LBOOL24_SPECTATOR_RENDERPHASE_PAUSED_VIA_EVENT			6
CONST_INT	LBOOL24_RENDERPHASE_PAUSED_VIA_RUMBLE_DEATH				7
CONST_INT	LBOOL24_OVERTIME_MISSED_THROUGH_DEATH_THIS_TURN			8
CONST_INT	LBOOL24_AIRSTRIKE_IN_PROGRESS							9
CONST_INT	LBOOL24_LAST_PLAYER_ALIVE								10
CONST_INT 	LBOOL24_PROP_CLAIM_TERTIARY_SELECTION					11
CONST_INT	LBOOL24_AIRSTRIKE_FINAL_SHOT_FIRED						12
CONST_INT	LBOOL24_VEHICLE_ALLOW_MOVEMENT_WHILE_INACTIVE			13
CONST_INT 	LBOOL24_SPECIAL_ABILITY_REFILLED						14
CONST_INT 	LBOOL24_IN_CREATOR_TRAILER								15
CONST_INT	LBOOL24_AIRSTRIKE_SAFETY_RANGE_STOP						16
CONST_INT 	LBOOL24_SET_PED_MOC_FIRING_PATTERN						17
CONST_INT	LBOOL24_AIRSTRIKE_DIALOGUE_TRIGGERED					18
CONST_INT	LBOOL24_BLOCK_OBJECTIVE_TEXT_UNTIL_RULE_CHANGE			19
CONST_INT	LBOOL24_BLOCK_VEHICLE_KERS_ON_RULE_CHECK				20
CONST_INT	LBOOL24_SHOW_VEHICLE_RESPAWN_TEXT						21
CONST_INT	LBOOL24_BUNKER_SLASHER_TIME_CYCLE_ACTIVE				22
//CONST_INT	FREE
CONST_INT	LBOOL24_FLASH_VEHICLE_BLIP_EXPIRED						24
CONST_INT	LBOOL24_FLASH_PED_BLIP_ACTIVE							25
CONST_INT	LBOOL24_FLASH_PED_BLIP_EXPIRED							26
CONST_INT	LBOOL24_SUDDEN_DEATH_DROP_PICKUPS						27
CONST_INT	LBOOL24_SUDDEN_DEATH_NO_PACKAGES_ARE_HELD				28
CONST_INT	LBOOL24_PULSED_CIRCLE_HUD_TIMER							29
CONST_INT	LBOOL24_SMOKE_TRAIL_ACTIVE								30
CONST_INT	LBOOL24_MANUAL_RESPAWN_DROP_PICKUPS						31

INT iLocalBoolCheck24

CONST_INT	LBOOL25_ALTERNATIVE_GD_SCORE_ADDED						0
CONST_INT	LBOOL25_VEHICLE_WEAPONS_HOMING_DISABLED					1
CONST_INT	LBOOL25_SUDDEN_DEATH_SPAWN_PROTECTION_DISABLED			2
CONST_INT	LBOOL25_LEFT_OLD_PARTNER_VEHICLE						3
CONST_INT	LBOOL25_FORCED_INTO_HEIST_SPECTATE						4
CONST_INT	LBOOL25_DRIVER_DIED_OR_RESPAWNED_INTO_NEW_VEH			5
CONST_INT	LBOOL25_SAVED_ACTUAL_VEHICLE_SEAT_TO_MULTI_VEH_DATA		6
CONST_INT	LBOOL25_INITIALIZED_SEATING_DATA_FOR_MULTI_VEH			7
CONST_INT	LBOOL25_STARTED_TURNS_FADE								8
CONST_INT	LBOOL25_COL_DAMAGE_AGAINST_ENTITY_DISABLED				9
CONST_INT	LBOOL25_MANAGE_LOCAL_PLAYER_INIT_CALLED					10
CONST_INT	LBOOL25_CLEARED_AND_RESET_WEAPONRY_RESTART				11
CONST_INT	LBOOL25_BLOCK_MANUAL_RESPAWN_DUE_TO_VEHICLE				12
CONST_INT	LBOOL25_CANNOT_BE_DAMAGED_DUE_TO_VEHICLE				13
CONST_INT	LBOOL25_RESET_ALL_PROPS_ON_RENDERPHASE_PAUSE			14
CONST_INT	LBOOL25_SUMO_RUN_TIMER_DELAY							15
CONST_INT	LBOOL25_SHOULD_PROCESS_CANT_DAMAGE_ENTITY				16
CONST_INT	LBOOL25_HOLDING_CONTROL									17
CONST_INT	LBOOL25_PASSENGER_MANUALLY_RESPAWNING					18
CONST_INT	LBOOL25_CLEANUP_RESPAWN_SPAWN_AT_START_NO_VEH			19
CONST_INT	LBOOL25_PLAYED_RULE_EXPIRED_SOUND						20
CONST_INT	LBOOL25_PLAYER_SET_DRUNK_THIS_RULE						21
CONST_INT	LBOOL25_PLAYER_SET_FORCED_STEALTH_THIS_RULE				22
CONST_INT	LBOOL25_USING_CAMERA_SHAKING_ON_RULE					23
CONST_INT	LBOOL25_FINISHED_CAMERA_SHAKING_ON_RULE					24
CONST_INT	LBOOL25_JUMPED_FROM_OPPRESSOR_SKY_DIVE					25
CONST_INT	LBOOL25_SHOULD_WAIT_TO_PLAY_AQ_POINT_GAINED				26
CONST_INT	LBOOL25_SHOULD_WAIT_TO_PLAY_AQ_GOAL_REACHED				27
CONST_INT	LBOOL25_A_PLAYER_HAS_SUPER_ALERTED_METAL_DETECTOR		28
CONST_INT	LBOOL25_STROMBERG_FORCE_GPS_ACTIVE						29
CONST_INT	LBOOL25_CLEAR_FAKE_CONE_ARRAY_CALLED					30 
CONST_INT	LBOOL25_ORBITAL_CANNON_DIALOGUE_AIRSTRIKE				31

INT iLocalBoolCheck25

CONST_INT	LBOOL26_INTERIOR_AUDIO_MOUNTAINBASE						0
CONST_INT	LBOOL26_INTERIOR_AUDIO_FACILITY_PROPERTY				1
CONST_INT	LBOOL26_INTERIOR_AUDIO_SUBMARINE						2
CONST_INT	LBOOL26_SCUBA_GEAR_ON									3
CONST_INT	LBOOL26_FIRST_STAGGERED_LOOP_OF_VEH_BODY				4
CONST_INT	LBOOL26_HARD_TARGET_OUTFIT_CHANGED						5
CONST_INT	LBOOL26_HARD_TARGET_CHANGED_SOUND						6
CONST_INT	LBOOL26_HARD_TARGET_FIRST_TARGET						7
CONST_INT	LBOOL26_OOB_ON_FOOT_EXPLODE_DONE						8
CONST_INT	LBOOL26_STARTED_HOTWIRE_HACK							9
CONST_INT	LBOOL26_STARTED_BEAM_HACK								10
CONST_INT	LBOOL26_PUT_BACK_INTO_STEALTH_MODE_WEP_CHANGE			11
CONST_INT	LBOOL26_PUT_BACK_INTO_STEALTH_MODE_OUTFIT_CHANGE		12
CONST_INT	LBOOL26_PLAYED_OUTFIT_CHANGE_PTFX						13
CONST_INT	LBOOL26_PARACHUTE_OVERRIDDEN_TO_TEAM_COLOUR				14
CONST_INT  	LBOOL26_FORCED_SEARCH_WANTED_AND_SEEN					15
CONST_INT	LBOOL26_CELEBRATION_SCREEN_OUTFIT_CHANGE				16
CONST_INT	LBOOL26_SPECTATOR_LOADED_INTERIOR						17
CONST_INT	LBOOL26_ENDING_HUD_UPDATED								18
CONST_INT	LBOOL26_DOWNLOADING_STARTED								19
CONST_INT  	LBOOL26_DISABLED_LAW_DISPATCH_FOR_FORCED_SEARCH			20
CONST_INT	LBOOL26_CELEBRATION_SCREEN_OUTFIT_CHANGE_BROADCAST		21
CONST_INT	LBOOL26_SERVUROSERV_LOSING_SIGNAL						22
CONST_INT	LBOOL26_MINIGUN_DEFENCE_MODIFIED						23
CONST_INT	LBOOL26_OVERRIDING_SEAT_PREF_SPECIAL_VEH				24
CONST_INT	LBOOL26_FIRST_HARD_TARGET_SHARD_PLAYED					25
CONST_INT	LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE			26
CONST_INT	LBOOL26_GANGOPS_LEADER_CHEAT_CHECK_DONE					27
CONST_INT	LBOOL26_DOWNLOAD_PAUSE_STARTED							28
CONST_INT	LBOOL26_DUMMY_BLIP_OVERRIDE								29
CONST_INT	LBOOL26_BEEN_UNDERWATER									30
CONST_INT	LBOOL26_STARTED_ANIMATION_FOR_BEAM_HACK					31


INT iLocalBoolCheck26

CONST_INT	LBOOL27_HOSTILE_TAKEOVER_EARLY_SD_COUNTDOWN_STOP		0
CONST_INT	LBOOL27_PV_REQUESTED_FACILITY_LOCATION					1
CONST_INT	LBOOL27_WINNER_LOSER_SHARD_RENDERPHASE_INITIAL_PAUSE	2
CONST_INT	LBOOL27_IN_CREATOR_TRAILER_OLD_VALUE					3
CONST_INT	LBOOL27_JUST_REACHED_QUICK_RESTART_POINT_1				4
CONST_INT	LBOOL27_JUST_REACHED_QUICK_RESTART_POINT_2				5
CONST_INT	LBOOL27_JUST_REACHED_QUICK_RESTART_POINT_3				6
CONST_INT	LBOOL27_JUST_REACHED_QUICK_RESTART_POINT_4				7
CONST_INT	LBOOL27_HARD_TARGET_LAST_TIME							8
CONST_INT	LBOOL27_PROCESS_BURNING_VANS							9
CONST_INT	LBOOL27_STOPPED_MP_MC_MUSIC_FOR_SUDDEN_DEATH			10
CONST_INT	LBOOL27_STARTED_SUB_SCAN								11
CONST_INT	LBOOL27_STROMBERG_TRANSFORM_SWEETENED					12
CONST_INT	LBOOL27_STORED_NIGHTVISION_STATE						13
CONST_INT	LBOOL27_SCRIPTED_CUTSCENE_LOCALLY_HIDE_PLAYERS			14
CONST_INT	LBOOL27_SCRIPTED_CUTSCENE_SYNCSCENE_STARTED				15
CONST_INT	LBOOL27_PLAYING_SERVER_FARM_ALARM						16
CONST_INT	LBOOL27_PLAYING_SUBMARINE_ALARM							17
CONST_INT	LBOOL27_FLYING_TOO_HIGH									18
CONST_INT	LBOOL27_OVERRIDDEN_MINE_PTFX							19
CONST_INT	LBOOL27_WAS_HARD_TARGET									20
CONST_INT	LBOOL27_BOMB_BAY_HELP_TEXT_PLAYED						21
CONST_INT	LBOOL27_WEAPON_HAS_BEEN_HIDDEN_BY_INTERACTION			22
CONST_INT	LBOOL27_AVENGER_EXIT_BLOCKED							23
CONST_INT	LBOOL27_PLAYED_DOWNLOAD_SOUND_THIS_RULE					24
CONST_INT	LBOOL27_BOMB_BAY_HELP_TEXT_PLAYED_OPEN					25
CONST_INT	LBOOL27_DELUXO_SOUND_UNLOCK_HOVER_PLAYED				26
CONST_INT	LBOOL27_DELUXO_SOUND_UNLOCK_FLIGHT_PLAYED				27
CONST_INT	LBOOL27_HAVE_HIDDEN_DUMMY_BLIPS							28
CONST_INT	LBOOL27_CARGO_EXHAUST_OVERRIDE							29
CONST_INT	LBOOL27_RED_FILTER_SOUND_PLAYED							30
CONST_INT	LBOOL27_OVERRIDDEN_CCTV_PTFX							31

INT iLocalBoolCheck27

CONST_INT LBOOL28_UPDATE_LOSE_COPS_OBJECTIVE						0
CONST_INT LBOOL28_CLEARED_PERSONAL_VEHICLE_INITIAL_HEIST2_NODES		1
CONST_INT LBOOL28_SUPPRESS_SUBMARINE_ALARM_FOR_CUTSCENE				2
CONST_INT LBOOL28_STARTED_DOWNLOAD_SND								3
CONST_INT LBOOL28_EXPEDITE_DIALOGUE_TRIGGERS_ENABLED				4
CONST_INT LBOOL28_PRELOAD_OUTFIT_CHANGE_AT_END_OF_CUTSCENE			5
CONST_INT LBOOL28_PREV_AVENGER_PILOT_STATUS							6
CONST_INT LBOOL28_PLAYERS_CONCEALED_DUE_TO_BANK_VAULT_SHOT			7
CONST_INT LBOOL28_END_MOCAP_TRIED_TURNING_OFF_VOLATOL				8
CONST_INT LBOOL28_USING_PROP_CLEANUP_FUNCTIONALITY					9
CONST_INT LBOOL28_SHOWDOWN_TAKEN_MANUAL_RESPAWN_POINTS				10
CONST_INT LBOOL28_SIREN_ACTIVE_MANUAL_RESPAWN						11
CONST_INT LBOOL28_FORCED_MANUAL_RESPAWN								12
CONST_INT LBOOL28_SHOWDOWN_DEATH_HANDLED							13
CONST_INT LBOOL28_CTF_OBJECT_IS_DRUGS								14
CONST_INT LBOOL28_CTF_OBJECT_IS_LAPTOP								15
CONST_INT LBOOL28_TRAP_DOOR_LEAVE_AREA_SHARD						16
CONST_INT LBOOL28_LAST_TIMER_RESET_FOR_SD							17
CONST_INT LBOOL28_USING_PV											18
CONST_INT LBOOL28_ALPHA_RESET_DONE									19
CONST_INT LBOOL28_BOUNDS_SHARD_SHOWN								20
CONST_INT LBOOL28_MANUALLY_HIDDEN_HUD								21
CONST_INT LBOOL28_STARTING_SPECTATOR_IN_TOP_DOWN					22
CONST_INT LBOOL28_CAN_TEAMS_ACTIVATE_OTHER_TEAMS_RESPAWN_POINTS		23
CONST_INT LBOOL28_OTHER_TEAM_ACTIVATED_MY_RESPAWN_POINTS			24
CONST_INT LBBOOL28_HAS_DONE_SUDDEN_DEATH_BOUNDS_CHECK				25
CONST_INT LBBOOL28_HIDE_RADAR_THIS_FRAME							26
CONST_INT LBBOOL28_BEAST_HELP_SHOWN									27
CONST_INT LBOOL28_TPR_APPEARANCE_UPDATED							28
CONST_INT LBBOOL28_LADDERS_DISABLED_BY_RULE_OPTION					29
CONST_INT LBBOOL28_REACHED_END_OF_STUNTING_PACK						30
CONST_INT LBOOL28_DELIVERED_THIS_RULE								31
INT iLocalBoolCheck28

CONST_INT LBOOL29_TP_JUGGERNAUT_HELP_SHOWN							0
CONST_INT LBOOL29_TP_KNOCKBACK_HELP_SHOWN							1
CONST_INT LBOOL29_TP_FIRST_JUGG_SWITCH_DONE							2
CONST_INT LBOOL29_SPECTATORS_RENDERPHASE_PAUSE_ON_RESTART			3
CONST_INT LBOOL29_RESTART_USING_POSTFX_REQUIRES_CLEANUP				4
CONST_INT LBOOL29_TEAM_SWAP_UPDATE_SPAWN_LOCATION					5
CONST_INT LBOOL29_MANUAL_RESPAWN_FADOUT								6
CONST_INT LBOOL29_CACHED_TEAM_ORDER_PARTICIPANTS					7
CONST_INT LBOOL29_READY_TO_PROCESS_GOTO_LOCATIONS					8
CONST_INT LBOOL29_SAVING_FALLOUT_FLAG								9
CONST_INT LBOOL29_PLAYED_RUNNING_BACK_FLASH_SFX						10
CONST_INT LBOOL29_UPDATE_LEAVE_LOC									11
CONST_INT LBOOL29_SHOULD_TURN_RADIO_BACK_ON_AFTER_ROUND_RESTART		12
CONST_INT LBOOL29_BLOCK_RUNNING_BACK_SHARD							13
CONST_INT LBOOL29_DONE_STUNTING_PACK_ENDING_FIREWORKS				14
CONST_INT LBOOL29_SET_REMOTE_PLAYER_SYNC_SCENE_FOR_SEC_ANIM			15
CONST_INT LBOOL29_GIVEN_BFOOTBALL_LOCAL_SCORE_THIS_MINIROUND		16
CONST_INT LBOOL29_MAX_NUM_OBJECTS_BEING_CARRIED						17
CONST_INT LBOOL29_WAITING_FOR_OBJ_TIMER_AND_FINAL_FOOTBALL_POINTS	18
CONST_INT LBOOL29_GIVEN_FINAL_FOOTBALL_POINTS						19
CONST_INT LBOOL29_MARKED_FOR_SHUNT_OBJ_DROP							20
CONST_INT LBOOL29_ARENA_VEHICLE_WEAPON_SET							21
CONST_INT LBOOL29_POST_SHARD_SCORE_UPDATE							22
CONST_INT LBOOL29_SENT_CAPTURED_FLAG_SHARD							23
CONST_INT LBOOL29_DELAY_BLOCK_MISSION_BOUNDS_1						24
CONST_INT LBOOL29_DELAY_BLOCK_MISSION_BOUNDS_2						25
CONST_INT LBOOL29_DO_DELIVER_FX										26
CONST_INT LBOOL29_HAS_LAST_VEHICLE_BEEN_STORED						27
CONST_INT LBOOL29_STARTED_SEAT_WARP_SPECTATOR_BOOTH					28
CONST_INT LBOOL29_TAG_TEAM_MODE_TAGGED_IN_ON_THIS_FRAME				29
CONST_INT LBOOL29_ARENA_SPAWN_FIX_HAS_BEEN_DONE						30
CONST_INT LBOOL29_SET_BLIP_IN_BLOCK_WANTED_CONE_RESPONSE_VEHICLE	31
INT iLocalBoolCheck29

INT iLocalBoolCheck30
CONST_INT LBOOL30_EXCLUDE_FROM_3_2_1_GO_COUNTDOWN					0
CONST_INT LBOOL30_HIDDEN_RESPAWN_BAR								1
CONST_INT LBOOL30_REJECTED_FROM_ARENA_SEAT							2
CONST_INT LBOOL30_CONTESTANT_TURRET_BLOCKED							3
CONST_INT LBOOL30_SETUP_NEW_INTRO_COORDS							4
CONST_INT LBOOL30_PLAYED_ARENA_WARS_AUDIO_TRACK						5
CONST_INT LBOOL30_PLAYED_30s_COUNTDOWN_FOR_ARENA_WARS				6
CONST_INT LBOOL30_SHOULD_RESET_VEHICLE_MINES_BLOCK_1				7
CONST_INT LBOOL30_FIRST_CONTENDER_DESTROYED							8
CONST_INT LBOOL30_SPEC_SPEC_FAKE_MINIROUND_TRANSITION_ACTIVATED		9
CONST_INT LBOOL30_RETURNING_A_FLAG									10
CONST_INT LBOOL30_A_FLAG_HAS_BEEN_DELIVERED							11
CONST_INT LBOOL30_TIME_SET_FOR_ARENA								12
CONST_INT LBOOL30_INIT_LAUNCH_SPEC_ACTIVITIES						13
CONST_INT LBOOL30_BMBFB_SD_OFFICIALLY_STARTED						14
CONST_INT LBOOL30_STOPPED_ARENA_MUSIC_FOR_JIP						15
CONST_INT LBOOL30_PAID_PREMIUM_ARENA_EVENT_FEE						16
CONST_INT LBOOL30_FILLED_ARENA_POINTS								17
CONST_INT LBOOL30_UPPED_SD_TELEMETRY								18
CONST_INT LBOOL30_LAST_CONTENDER_MONSTER_JAM						19
CONST_INT LBOOL30_BMBFB_BALLS_HAVE_BEEN_UNEVEN						20
CONST_INT LBOOL30_PROCESSED_LEAVE_VEHICLE_FOR_CUTSCENE				21
CONST_INT LBOOL30_DUMMY_BLIPS_NEED_CONSTANTLY_UPDATING				22
CONST_INT LBOOL30_CUTSCENE_PLAYED_CLOTHING_ANIM						23
CONST_INT LBOOL30_MOD_SHOP_CAR_NEEDS_REPAIR							24
CONST_INT LBOOL30_UPDATE_OBJECTIVE_TEXT_WITH_VERY_LONG_DELAY		25
CONST_INT LBOOL30_DESTROY_BIRDSEYE_CAM_FOR_CELEB_SCREEN				26
CONST_INT LBOOL30_SHOW_CUSTOM_WAIT_TEXT								27
CONST_INT LBOOL30_SET_PLAYER_INTO_COVER_START_POSITION				28
CONST_INT LBOOL30_HAS_BEEN_SET_INTO_COVER_START_POSITION			29
CONST_INT LBOOL30_HAS_CORRECTED_COVER_START_CAMERA_HEADING			30
CONST_INT LBOOL30_HAS_CORRECTED_COVER_START_PLAYER_HEADING			31

INT iLocalBoolCheck31
CONST_INT LBOOL31_ARE_ANY_TEAM_PLAYERS_IN_VALID_DELIVERY_VEH		0
CONST_INT LBOOL31_PRIZED_VEHICLE_FROZEN								1
CONST_INT LBOOL31_IS_CURRENT_OBJECTIVE_REPAIR_CAR					2
CONST_INT LBOOL31_LOCK_CASINO_DOORS									3
CONST_INT LBOOL31_PRIZED_VEHICLE_HIDDEN								4
CONST_INT LBOOL31_AGGRO_HELPTEXT_PLAYED								5
CONST_INT LBOOL31_END_CUTSCENE_STARTED								6
CONST_INT LBOOL31_USING_NEAREST_PLACED_VEHICLE_WARP					7
CONST_INT LBOOL31_CASINO_MISSION_CONTROLLER_BLIPS_HIDDEN			8
CONST_INT LBOOL31_AGGROD_SOMEONE_INTO_COMBAT_AT_SOME_POINT			9
CONST_INT LBOOL31_HAS_TRIGGERED_ALL_CCTV_CAMERAS					10
CONST_INT LBOOL31_APPLIED_INTRO_GAIT								11
CONST_INT LBOOL31_PLAYER_RESURRECTED_FOR_END_CUTSCENE				12
CONST_INT LBOOL31_PRIZED_VEHICLE_SHOULD_HIDE						13
CONST_INT LBOOL31_NOT_STREAMING_CUTSCENE							14
CONST_INT LBOOL31_CANCEL_LOAD_SCENE_FOR_END_CUTSCENE_WARP			15
CONST_INT LBOOL31_PLAYED_BRAWL_START_ONESHOT						16
CONST_INT LBOOL31_STARTED_LOAD_SCENE_FOR_SCRIPT_INIT				17
CONST_INT LBOOL31_FINISHED_LOAD_SCENE_FOR_SCRIPT_INIT				18
CONST_INT LBOOL31_ENDED_SKY_CAM_FOR_LOAD_SCENE_FOR_SCRIPT_INIT		19
CONST_INT LBOOL31_SHOULD_USE_SKY_CAM_FADE_LOAD_SCENE				20
CONST_INT LBOOL31_REQUEST_CASINO_INTERIOR_SCRIPT					21
CONST_INT LBOOL31_CLEARED_INTERIOR_FLAG_FOR_ENTITY					22
CONST_INT LBOOL31_CARGOBOB_CARRYING_OBJ_VEHICLE						23
CONST_INT LBOOL31_CARGOBOB_CARRYING_OBJ_VEHICLE_TRIGGERED			24
CONST_INT LBOOL31_SOMEONE_IS_RAPPELLING								25
CONST_INT LBOOL31_PHONE_EMP_IS_ACTIVE								26
CONST_INT LBOOL31_PLACED_PEDS_PERFORMED_ROLLING_START				27
CONST_INT LBOOL31_HEAVY_STOLEN_GOODS_CASINO_HEIST					28
CONST_INT LBOOL31_OUTFIT_CHANGED_THROUGH_INTERACT					29
CONST_INT LBOOL31_READY_TO_USE_DAMAGED_VAULT_DOOR					30
CONST_INT LBOOL31_STARTED_ANIM_FOR_FINGERPRINT_KEYPAD_HACK			31

INT iLocalBoolCheck32
CONST_INT LBOOL32_HAS_CUT_PAINTING_ANIMATION_STARTED				0
CONST_INT LBOOL32_HAS_CUT_PAINTING_EXITED_EARLY						1
CONST_INT LBOOL32_NO_NEED_FOR_DIALOGUE_EVERY_FRAME_PROCESSING		2
CONST_INT LBOOL32_BLOCKING_OBJECTIVE_FROM_DIALOGUE_TRIGGER			3
CONST_INT LBOOL32_HAS_CUT_PAINTING_GOT_LOCAL_COPY					4
CONST_INT LBOOL32_SET_CASINO_EXTRA_ENTITY_SETS						5
CONST_INT LBOOL32_SET_STUN_GUN_AS_LIMITED							6
CONST_INT LBOOL32_LIMITED_STUN_GUN_FULLY_INITIALISED				7
CONST_INT LBOOL32_MINIGAME_HELP_TEXT_SHOWING_THIS_FRAME				8
CONST_INT LBOOL32_ELEVATOR_GOTO_TASK_ACTIVE							9
CONST_INT LBOOL32_TURN_OFF_VISUAL_AIDS_ASAP							10
CONST_INT LBOOL32_DONE_FIRST_COUGH								11
CONST_INT LBOOL32_STARTED_FINGERPRINT_CLONE							12
CONST_INT LBOOL32_STARTED_ORDER_UNLOCK								13
CONST_INT LBOOL32_CUT_PAINTING_IS_CUT_ANIMATION_IN_PROGRESS			14
CONST_INT LBOOL32_CUT_PAINTING_IS_IDLE_ANIMATION_IN_PROGRESS		15
CONST_INT LBOOL32_CASINO_ALARM_STARTED								16
CONST_INT LBOOL32_UNLOCKED_DOOR_WITH_SWIPECARD						17
CONST_INT LBOOL32_FINGERPRINT_HACK_ANIM_USE_ALT						18
CONST_INT LBOOL32_USED_ELEVATOR										19
CONST_INT LBOOL32_USING_DRONE										20
CONST_INT LBOOL32_ELEVATOR_FILTER_ACTIVE							21
CONST_INT LBOOL32_ELEVATOR_REMOVED_PLAYER_CONTROL					22
CONST_INT LBOOL32_HANDLING_CASINO_VAULT_EXPLOSION					23
CONST_INT LBOOL32_COMPLETED_CASINO_VAULT_DOOR_BOMB_PLANT			24
CONST_INT LBOOL32_QUIT_KEYPAD_HACK_MANUALLY							25
CONST_INT LBOOL32_VAULT_DRILL_ASSET_LOADED							26
CONST_INT LBOOL32_USING_HANDHELD_DRILL								27
CONST_INT LBOOL32_SHOULD_USE_CUSTOM_HELP_WAIT_TEXT_TAXI				28 
CONST_INT LBOOL32_CASINO_HEIST_MISSION_CONTROLLER_BLIPS_HIDDEN		29 
CONST_INT LBOOL32_SCRIPTED_CUTSCENE_PLAYING_ANIM					30
CONST_INT LBOOL32_PLAYER_REL_GROUP_CACHED_FOR_DRONE					31

INT iLocalBoolCheck33
CONST_INT LBOOL33_PLAYED_COUGH_SOUND_EFFECT					0
CONST_INT LBOOL33_DRONE_PICKUP_SPAWNED								1
CONST_INT LBOOL33_ELEVATOR_FADED									2
CONST_INT LBOOL33_ZOOMED_IN_FOR_CASINO_ROOF							3
CONST_INT LBOOL33_ZOOMED_IN_FOR_CASINO_HEIST						4
CONST_INT LBOOL33_DRONE_TRANQUILISER_AMMO_ASSIGNED					5
CONST_INT LBOOL33_MELEE_PCF_IS_RULE_BLOCKED							6
CONST_INT LBOOL33_SET_PLAYER_INTO_COVER_START_POSITION_FORCE_LEFT	7
CONST_INT LBOOL33_HEAVY_ARMOUR_CHECK								8
CONST_INT LBOOL33_DAILY_CASH_ROOM_DOOR_OPEN_SOUND_PLAYED			9
CONST_INT LBOOL33_EMP_BLOCKING_DRONE_ACTIVE							10
CONST_INT LBOOL33_DRONE_PICKUP_COLLECTED							11
CONST_INT LBOOL33_SHOULD_CREATE_PICKUP								12
CONST_INT LBOOL33_USING_ACTION_MODE									13
CONST_INT LBOOL33_USING_ACTION_MODE_FORCE_OFF						14
CONST_INT LBOOL33_TURNED_ON_CASINO_FLOOR_AMBIENT_ZONE				15
CONST_INT LBOOL33_WEAPON_FORCED_EQUIPPED_THIS_RULE					16
CONST_INT LBOOL33_SET_IGNORED_WHEN_WANTED							17
CONST_INT LBOOL33_FLASHLIGHT_NEEDS_TO_BE_TURNED_ON					18
CONST_INT LBOOL33_PLAYER_DISABLED_THEIR_OWN_FLASHLIGHT				19
CONST_INT LBOOL33_RESPAWN_SET_FROM_INTERIOR_WARP					20
CONST_INT LBOOL33_HIDE_HEIST_BAGS_AT_END_MISSION_CUTSCENE			21
CONST_INT LBOOL33_SET_TIME_TO_LOSE_COPS_CLEAR_MODE					22
CONST_INT LBOOL33_CASINO_HEIST_LEADER_CHEAT_CHECK_DONE				23

INT iVehDeliveryStoppedCancelled

INT iCharacterAddedToPhoneBook

CONST_INT LBOOL_RESET1_SHOWING_VEH_HEALTH_BARS						0
CONST_INT LBOOL_RESET1_HUD_SHOULD_BE_HIDDEN_THIS_FRAME_CHECKED		1
CONST_INT LBOOL_RESET1_HUD_SHOULD_BE_HIDDEN_THIS_FRAME				2
INT iLocalBoolResetCheck1

INT iSPHBBS_PedHasHadBlipCheckedBS[ciFMMC_PED_BITSET_SIZE]
INT iSPHBBS_PedShouldHaveBlipBS[ciFMMC_PED_BITSET_SIZE]

INT iRespawnPointIndexToKeep = -1
INT iBSRespawnActiveFromRadius[FMMC_MAX_TEAMS][3]
INT iBSRespawnDeactivated[FMMC_MAX_TEAMS][3]
INT iBSRespawnActiveFromRadiusRemoteToggle[FMMC_MAX_TEAMS][3]

INT iRunningBackCustomBlipBS


INT iPlayerPedIgnoredWhenWanted

//ADD ANY NEW iLocalBoolCheckXs TO MAINTAIN_BG_GLOBALS_PASS_OVER

SCRIPT_TIMER stBlockPackageSoundsTimer

INT iSuddenDeathTargetInitStage
INT iSuddenDeathTargetRoundLast
BOOL bIncrementSuddenDeathTargetRound
INT iSuddenDeathTargetProp
OBJECT_INDEX objSuddenDeathTarget
INT iSuddenDeathTargetMoveStyle = 0
VECTOR vSuddenDeathTargetCentre = <<-0.1, 0.0, 6.0>>
VECTOR vSuddenDeathTargetBounds = <<0.0, 2.1, 2.3>>
VECTOR vSuddenDeathTargetOffset = <<0.0, 0.0, 0.0>>
VECTOR vSuddenDeathTargetInitialVelocity = <<0.0, 3.000, 3.125>>
VECTOR vSuddenDeathTargetVelocity = <<0.0, 3.000, 3.125>>
FLOAT fSuddenDeathTargetSinCosScale = 2.0
FLOAT fSuddenDeathTargetSinCosSpeed = 0.150

//Target Practice
SCRIPT_TIMER stTargetPropShot[FMMC_MAX_NUM_PROPS]

INT iNonRepeatingAlarmPlayedBS

INT iCachedLapCount = 0

INT iCountdownBleepTime = 5

INT iTurretPlayerCounter = 0

//Heli Rotor Health Cache
INT iHeliRotorHealthCacheBitset
FLOAT fHeliMainRotorHealthCache[FMMC_MAX_VEHICLES]
FLOAT fHeliTailRotorHealthCache[FMMC_MAX_VEHICLES]

INT	iTitanRotorParticlesBitset
PTFX_ID ptfxTitanRotorParticles[FMMC_MAX_VEHICLES][4]

//To check whether ped has said hi already
INT iDialogueHiBS[ciMAX_DIALOGUE_BIT_SETS_LEGACY]

//Staggered Loop Full Loop Check
INT iWeaponPickupStaggeredLoopIndex = -1

//For peds looking at other peds
INT iPedLookAtBS[FMMC_MAX_PEDS_BITSET]

INT iSeatPreferenceSlot = 0

INT iMidCountdownRuleNum
INT iMidCountdownLastScore

INT iArrowBitset
SCRIPT_TIMER stRallyArrowTimer
SCRIPT_TIMER stRallyHelpTimer

SCALEFORM_INDEX siRallyArrow

BOOL bHelpTextArrows = FALSE

CONST_INT OBJECTIVE_TEXT_DELAY				1250
CONST_INT OBJECTIVE_TEXT_DELAY_LONG			2000
CONST_INT OBJECTIVE_TEXT_DELAY_VERY_LONG	3000
INT iLocalBoolCheckObjTextDelay
CONST_INT ciOBJ_TEXT_DELAY_MAX_TIMERS 6
SCRIPT_TIMER stObjTextDelayTimerGroup[ciOBJ_TEXT_DELAY_MAX_TIMERS]	//Size of total different flags checked.
CONST_INT OBJ_TEXT_DELAY_BS_GROUP_0		0
CONST_INT OBJ_TEXT_DELAY_BS_GROUP_1		1

INT iPrevWantedForHUD = 0

INT iCurrentRequestStaggerForStrandMission
INT iKilledThisManyFrmTeam[FMMC_MAX_TEAMS]

INT iRelGroupStaggerVars[ 5 ]
INT iNumTimesCalledRelGroupFunc

//INT iTotalCashForEveryone = 0
BOOL bHasBeatCashGrab = FALSE
INT iCashStacksGrabbed
INT iLowCashStacksGrabbed
CONST_INT ciOffRuleScore_Low 1
CONST_INT ciOffRuleScore_Med 3
CONST_INT ciOffRuleScore_High 5

INT iCocaineParticleEffectBitset	//FMMC_MAX_NUM_OBJECTS

INT iDeliveryVehForcingOut = -1
INT iPartSending_DeliveryVehForcingOut = -1
INT iLocHit = -1
INT iOldLocate = -1
INT iSecondaryAnimPlaying = -1
INT iSecondaryAnimHP = 0
INT iLocalTrevorBodhiBreakoutSynchSceneID = -1
Bool bFinalizePedpBus
INT iLocalGOFoundryHostageSynchSceneID = -1
INT iGOFoundryHostagePerpResetFlagPed = -1

FLOAT fTeamMembers_ReqDist = 0.0
INT iTeamMembers_WithinReqDist

SKYSWOOP iSyncRoundLastFrame

int iSyncBusSchedule

INT iTempTeamVehicle[FMMC_MAX_TEAMS]

SHAPETEST_INDEX stLOSCheck = NULL

INT iOldRule = -1
INT iOldRule2[FMMC_MAX_TEAMS]
INT iNewRule2[FMMC_MAX_TEAMS]

FLOAT fLastFrameTime = 0.0333 // Time since last frame (in s) - initialise as if 30fps

INT iWasTeamWantedBS
INT iWasPlayerWantedBS

CONST_INT ciTCWBS_T0GOT		0
CONST_INT ciTCWBS_T1GOT		1
CONST_INT ciTCWBS_T2GOT		2
CONST_INT ciTCWBS_T3GOT		3
CONST_INT ciTCWBS_T0LOST	4
CONST_INT ciTCWBS_T1LOST	5
CONST_INT ciTCWBS_T2LOST	6
CONST_INT ciTCWBS_T3LOST	7

INT iTeamCarrierWantedBS

INT iContBitSet
INT iDropContBitSet

INT iEnemyVehBitSet
INT iEscortingBitSet[FMMC_MAX_PEDS_BITSET]
INT iEscortSpeedUpdatedBitset[FMMC_MAX_PEDS_BITSET]

INT iGotoObjectBitSet[FMMC_MAX_PEDS_BITSET]
INT iBSTaskUsingCoordTarget[FMMC_MAX_PEDS_BITSET]
INT iBSTaskAchieveHeadingWait[FMMC_MAX_PEDS_BITSET]
INT iBSTaskUsingIgnoreCombatChecks[FMMC_MAX_PEDS_BITSET]

INT iVehTargetingCheckedThisRuleBS
INT iVehSeatPreferenceCheckBS
//INT iVehSeatClearedPrefBS

SCRIPT_TIMER tdPhotoTakenTimer
SCRIPT_TIMER tdOutOfBoundsWarpTimer
SCRIPT_TIMER tdDisableControlTimer

//Lukasz: comment this out for B*2214154, no longer need these anims to play, but leave it commented in case we need to add it again
//actually need this back for B*2225816
INT iTrevorIdleAnimTimer // Timer for Trevor's idle animations in Narcotics Heist Finale
INT iTrevorAnimPerforming = ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_B // Which Anim is being performed 
INT iTrevorAnimRandomNo = 2000
INT iTrevorAnimsRandom = 0
INT iTrevorAnimsCount = 0
INT iPedAnimBitset[FMMC_MAX_PEDS_BITSET]
INT iPedSpecAnimBitset[FMMC_MAX_PEDS_BITSET]
INT iPedSecondaryAnimCleanupBitset[FMMC_MAX_PEDS_BITSET]

INT iPedInventoryCleanedUpBitset[FMMC_MAX_PEDS_BITSET]
SCRIPT_TIMER tdAssociatedGOTOWaitIdleAnimTimer[FMMC_MAX_PEDS]

NETWORK_INDEX niBriefcaseForPedTask
NETWORK_INDEX niPhoneforPedtask
NETWORK_INDEX niMinigameObjects[4]
//NETWORK_INDEX niClipboardForPedTask
//NETWORK_INDEX niPencilForPedTask
NETWORK_INDEX niWeldTorch			
//NETWORK_INDEX niBottleforPedtask		
PED_VARIATION_STRUCT sPedVariations

BOOL bSafeFromWantedZone
BOOL bSafeFromWantedZone2

//new way to apply outfits stuct within one call
MP_OUTFITS_APPLY_DATA sApplyOutfitData

INT iRuleOutfitLoading = -1
INT iLastOutfit = -1

VECTOR vPropertyDropoff
//INT iHeistLeaderPart = -1

BLIP_INDEX MaskShopBlip

INT iOldPriority[FMMC_MAX_TEAMS]
INT iOldMidpoint[FMMC_MAX_TEAMS]
INT iOldLaps

CONST_INT 	BVTH_TIME_TO_STOP_FOR 5
CONST_FLOAT BVTH_STOPPING_DISTANCE 10.0

SCRIPT_TIMER PVsafetyTimer
SCRIPT_TIMER tdCrateFailTimer[FMMC_MAX_NUM_OBJECTS]

//music variables
SCRIPT_TIMER tdMusicTimer

//Capture point variables

FLOAT iPrevCapTime[FMMC_MAX_GO_TO_LOCATIONS][FMMC_MAX_TEAMS]
FLOAT iCurrCapTime[FMMC_MAX_GO_TO_LOCATIONS][FMMC_MAX_TEAMS]
INT iStoredVehCaptureMS[FMMC_MAX_TEAMS][FMMC_MAX_VEHICLES] 


//end cutscene variables
INT iCutRegisterPlayerNum
INT iCutComponentPlayerNum
CONST_INT ci_INTRO_CUTSCENE 99
STRING sMocapCutscene
TEXT_LABEL_63 tl63_CrowdControlCutscene
STRING strPreviousMocapCutsceneName
PED_INDEX MocapPlayerPed[FMMC_MAX_CUTSCENE_PLAYERS]
INT iMocapPlayerPedMask[FMMC_MAX_CUTSCENE_PLAYERS]
OBJECT_INDEX MocapPlayerWeaponObjects[FMMC_MAX_CUTSCENE_PLAYERS]
OBJECT_INDEX playersWeaponObject
PLAYER_INDEX LocalMainCutscenePlayer[FMMC_MAX_TEAMS]
//OBJECT_INDEX oiHUM_FIN_MCS1_PC, oiHUM_FIN_MCS1_MOUSE, oiHUM_FIN_MCS1_KEYBOARD	
PED_INDEX ClonePlayerForCutscene
SCRIPT_TIMER tdCutsceneMidpoint
WEAPON_TYPE playerCutsceneWeap = WEAPONTYPE_INVALID

ENUM eLiftMovementState
	eLMS_WaitForStart,
	eLMS_AttachClones,
	eLMS_CloseDoors,
	eLMS_MoveLiftUp,
	eLMS_MoveLiftDown,
	eLMS_OpenDoors,
	eLMS_Complete
ENDENUM

OBJECT_INDEX objCutsceneLift_Body // The main bulk of the lift
OBJECT_INDEX objCutsceneLift_DoorL // On the left when inside the lift, trying to leave through the doors
OBJECT_INDEX objCutsceneLift_DoorR // On the right when inside the lift, trying to leave through the doors
OBJECT_INDEX objCutsceneLift_DoorLO // Outer door, stays on the floor and does not move with the lift like the other doors
OBJECT_INDEX objCutsceneLift_DoorRO // Outer door, stays on the floor and does not move with the lift like the other doors
FLOAT fCutsceneLift_OriginZ
FLOAT fCutsceneLift_DoorLOpenHeading
FLOAT fCutsceneLift_DoorROpenHeading
FLOAT fCutsceneLift_DoorsClosedHeading
eLiftMovementState eLiftState
eCutsceneLift eCSLift

INT indexLocalPlayer = -1
INT indexMp2 = -1
INT indexMp3 = -1
INT indexMp4 = -1
INT iTutMissionIntroMocapProg
INTERIOR_INSTANCE_INDEX interiorMocap247	// [NG-INTEGRATE] Next-gen only.

//for garge door sound fx during mocaps
int garage_door_sound_id = get_sound_id()
int garage_door_audio_system_status = 0

//-- For tutorial mission
INT iTutorialMissionCutBitset
CONST_INT biTutCut_WArpedPlayerIntoCar			0
CONST_INT biTutCut_SetMissVehColours			1
CONST_INT biTut_DoneEnemyBlipHelp				2
CONST_INT biTut_FoundEnemyBlip					3
CONST_INT biTut_MocapFinished					4
CONST_INT biTut_StartScriptedSceneSetup			5
CONST_INT biTut_StartScriptCam					6
CONST_INT biTut_GivenDriveTask					7
CONST_INT biTut_CleanedUpLamar					8
CONST_INT biTut_SetRadioStation					9
CONST_INT biTut_NeedToSetRadioStation			10
CONST_INT biTUt_StartEnterVehicle				11
CONST_INT biTut_PedIntoCar0						12
CONST_INT biTut_PedIntoCar1						14
CONST_INT biTut_PedIntoCar2						15
CONST_INT biTut_PedIntoCar3						16
CONST_INT biTut_SetExitState0					17
CONST_INT biTut_SetExitState1					18
CONST_INT biTut_SetExitState2					19
CONST_INT biTut_SetExitState3					20
CONST_INT biTut_StartFixingAudio				21
CONST_INT biTut_StartAudioScene					22
CONST_INT biTut_StartCutsceneOverrideSound		23
CONST_INT biTut_SToppedAudioScene				24
CONST_INT biTut_CleanupPostFx					25
CONST_INT biTut_UnmuteRadio						26
CONST_INT biTut_RadioOff						27

SCRIPT_TIMER timeFixAudio
SCRIPT_TIMER timeFixPan
//for collect vehicle ticker
INT ivehcolBoolCheck
//for collect object ticker
INT iobjcolBoolCheck
INT iobjCollectedBoolCheck
INT iPackageDelXPCount
INT iPackageColXPCount
INT iPedDeliveryRPCap

INT iLastRule = 0

//for goto ped bitset, stops them from leaving your group after collection.
INT iKeepFollowPedBitset[FMMC_MAX_PEDS_BITSET]

INT iCelebrationStreamingRequestStage

INT iObjDeliveredBitset
INT iPedDeliveredBitset[FMMC_MAX_PEDS_BITSET]

INT iObjHeldBS //Indicates that this object has been held by someone at some point (if cibsOBJ_Reset_If_Dropped_Out_Of_Bounds is set)

INT iBroadcastPriority = -1
INT iBroadcastRevalidate = -1
INT iBroadcastVehNear = -1

// For winner scene. 
INT iCelebrationTeamNameIsNotLiteral

CONST_INT ciDIVE_SCORE_TIME_BEFORE_RESET			2500//ms
CONST_INT ciDIVE_SCORE_TIME_BEFORE_FREEZEFRAME		800//ms
CONST_FLOAT cfDIVE_SCORE_EXTRA_DROP_OFF_RADIUS		1.3//m
CONST_INT ciDIVE_SCORE_DIVEABLE_AREA				7//m
CONST_FLOAT cfDIVE_SCORE_GET_UP_ANIM_CUTOFF			0.64//%
CONST_FLOAT cfDIVE_SCORE_DIVE_ANIM_CUTOFF			0.9//%
CONST_INT ciDIVE_SCORE_VALIDITY_CHECK				250//ms
CONST_INT ciDIVE_SCORE_STALL_CHECK					6000//ms

SCRIPT_TIMER stWaitForScoreUpdate

ENUM PlayerSwanDiveAnimState
	PSDAS_WaitingToPlay,
	PSDAS_PlayDive,
	PSDAS_ValidatingDiveAnim,
	PSDAS_WaitingForDiveFinish,
	PSDAS_PlayGetUp,
	PSDAS_WaitingForGetUpFinish,
	PSDAS_Reset
ENDENUM

ENUM SwanDiveScoreCameraState
	SDSCS_WaitingToPlay,
	SDSCS_FindCameraSlot,
	SDSCS_InitCamera,
	SDSCS_WaitForDiveFinish,
	SDSCS_Reset,
	
	SDSCS_FindRemoteCamSlot,
	SDSCS_InitNonScorerCam,
	SDSCS_WaitForNonScoreCamFinish
ENDENUM

PlayerSwanDiveAnimState currentSwanDiveState = PSDAS_WaitingToPlay
SCRIPT_TIMER stTimeSinceDived
SCRIPT_TIMER stDiveScoreStallTimer

SwanDiveScoreCameraState currentSwanDiveCamState = SDSCS_WaitingToPlay
SCRIPT_TIMER stDiveCamStallTimer
CAMERA_INDEX swanDiveCamera

SHAPETEST_INDEX siSwanDiveCamRay
INT iSwanCamRayHitSomething
VECTOR vSwanCamRayPos
VECTOR vSwanCamRayNormal
ENTITY_INDEX eiSwanCamRayEntity

INT iSwanCamAngleTestSelection = 1
VECTOR vSwanCamOffset
VECTOR vSwanCamCached

ENUM PlayerRespawnBackAtStartState
	PRBSS_PlayingNormally,
	PRBSS_Init,
	PRBSS_Prep,
	PRBSS_Warp,
	PRBSS_Sync,
	PRBSS_Reset,
	PRBSS_Invalid
ENDENUM

// ***************************
//
// Apartment Warping
//
// ***************************

CONST_INT ciAS_Outside		0
CONST_INT ciAS_WarpingIn	1
CONST_INT ciAS_Inside		2
CONST_INT ciAS_WarpingOut	3


//INT iApartmentState - moved this to MC_playerBD[iPartToUse].iApartmentState, so the server can see the player's apartment state!
INT iWarpProgress
INTERIOR_INSTANCE_INDEX interiorWarp[MAX_BUILDING_WARPS]
BLIP_INDEX AptWarpBlip
OBJECT_INDEX objDoorBlocker
CAMERA_INDEX ciWarpCam
SCRIPT_TIMER WarpCSTimer
BOOL bHideHud
BOOL bCamera

INT iAptSynchScene
INT iDoorSoundStage


//BITSET USED TO TRIGGER PED SPEECH
INT iPedSpeechTriggeredBitset[FMMC_MAX_PEDS_BITSET]
INT iPedSpeechAgroBitset[FMMC_MAX_PEDS_BITSET]
INT iPedDialogueRangeActive[FMMC_MAX_PEDS_BITSET]
INT iPedInitSpeechBitset[FMMC_MAX_PEDS_BITSET]
INT iPedSpeechAimBitset[FMMC_MAX_PEDS_BITSET]
INT iPedHasCharged[FMMC_MAX_PEDS_BITSET]

INT iPedAgroStageZero[FMMC_MAX_PEDS_BITSET]
INT iPedAgroStageOne[FMMC_MAX_PEDS_BITSET]
INT iPedAgroStageTwo[FMMC_MAX_PEDS_BITSET]

SCRIPT_TIMER PedAgroTimer[FMMC_MAX_PEDS]
SCRIPT_TIMER PedDelayDialogueTimer[FMMC_MAX_PEDS]
PED_INDEX DialoguePedToLookAt[FMMC_MAX_PEDS]
//used to swap between Idle Animation and Special Animation tasks
INT iPedPerformingSpecialAnimBS[FMMC_MAX_PEDS_BITSET]
//used to say if a ped has played their breakout animation (and probably shouldn't go back)
//INT ipedAnimBreakoutBS[FMMC_MAX_PEDS_BITSET] - now replaced by g_iFMMCPedAnimBreakoutBS
//used to make peds more perceptive during combat
INT iPedPerceptionBitset[FMMC_MAX_PEDS_BITSET]
//used to make peds less likely to warp into seats.
INT iPedSeatPriorityBitset[FMMC_MAX_PEDS_BITSET]
//used to task peds to turn towards the player when they hear them
INT iPedHeardBitset[FMMC_MAX_PEDS_BITSET]
//used to store whether a ped has seen a dead body or not
INT iPedSeenDeadBodyBitset[FMMC_MAX_PEDS_BITSET]
INT iDeadPedIDSeen[FMMC_MAX_PEDS] //Stores the PedID of the dead ped that this ped (in the array) has seen

//Set once a ped doing a cover task has gone into combat, so we know once this combat is finished we can go back to the cover task
INT iPedCoverTask_ReachedCombat[FMMC_MAX_PEDS_BITSET]

INT iPedGotoWaitBitset[FMMC_MAX_PEDS_BITSET]
INT iPedGotoAssCompletedWaitBitset[FMMC_MAX_PEDS_BITSET]

INT iPedLocalSpookedBitset[FMMC_MAX_PEDS_BITSET]
INT iPedLocalAggroedBitset[FMMC_MAX_PEDS_BITSET]
INT iPedLocalCancelTasksBitset[FMMC_MAX_PEDS_BITSET]
INT iPedLocalCheckedSpawnedAggroed[FMMC_MAX_PEDS_BITSET]
INT iPedZoneAggroed[FMMC_MAX_PEDS_BITSET]

INT iPedLocalBoatSpeedSetEnteredOverrideRangeBoat[FMMC_MAX_PEDS_BITSET]
INT iPedLocalBoatSpeedSetOverrideSpeedBoat[FMMC_MAX_PEDS_BITSET]
INT iPedLocalBoatSpeedSetRegularSpeedBoat[FMMC_MAX_PEDS_BITSET]

//Events for spooked checks (spooked checks happen in a staggered loop, so check for these events every frame and then check this bitset in the staggered loop check):
INT iPedLocalReceivedEvent_ShotFiredORThreatResponse[FMMC_MAX_PEDS_BITSET]
INT iPedLocalReceivedEvent_SeenMeleeActionOrPedKilled[FMMC_MAX_PEDS_BITSET]
INT iPedLocalReceivedEvent_HurtOrKilled[FMMC_MAX_PEDS_BITSET]
INT iPedLocalReceivedEvent_VehicleDamageFromPlayer[FMMC_MAX_PEDS_BITSET]

CONST_INT ciMAX_PED_SPOOK_COORDS		5
CONST_INT ciSpookCoordID_GENERIC				0
CONST_INT ciSpookCoordID_THERMITE_PLACED		1
CONST_INT ciSpookCoordID_OBJECT_INTERACT_WITH	2
STRUCT PED_SPOOK_AT_COORD_STRUCT
	SCRIPT_TIMER tdPedSpookAtCoordTimer
	VECTOR vPedSpookCoord
	FLOAT fPedSpookRadius
	ENTITY_INDEX entPedSpookTarget
	INT iPedSpookCoordID
ENDSTRUCT
PED_SPOOK_AT_COORD_STRUCT sPedSpookAtCoord[ciMAX_PED_SPOOK_COORDS]

//Ped aggro reasons:
CONST_INT ciSPOOK_NONE							0
CONST_INT ciSPOOK_PLAYER_WITH_WEAPON			1
CONST_INT ciSPOOK_PLAYER_BLOCKED_VEHICLE		2
CONST_INT ciSPOOK_PLAYER_CAUSED_EXPLOSION		3
CONST_INT ciSPOOK_PLAYER_POTENTIAL_RUN_OVER		4
CONST_INT ciSPOOK_PLAYER_AIMED_AT				5
CONST_INT ciSPOOK_SHOTS_FIRED					6
CONST_INT ciSPOOK_DAMAGED						7
CONST_INT ciSPOOK_PLAYER_TOUCHING_ME			8
CONST_INT ciSPOOK_PLAYER_CAR_CRASH				9
CONST_INT ciSPOOK_PLAYER_TOUCHING_MY_VEHICLE	10
CONST_INT ciSPOOK_PLAYER_HEARD					11
CONST_INT ciSPOOK_SEEN_BODY						12
CONST_INT ciSPOOK_SEEN_PLAYER_NOT_IN_SECONDARY_ANIM	13
CONST_INT ciSPOOK_VEHICLE_ATTACHED				14

SEQUENCE_INDEX PedDropOffSequence

CONST_INT ciCOLLISION_STREAMING_MAX	15
INT iCollisionStreamingLimit
INT iPedCollisionBitset[FMMC_MAX_PEDS_BITSET]
INT iPedNavmeshCheckedBS[FMMC_MAX_PEDS_BITSET]
INT iPedNavmeshLoadedBS[FMMC_MAX_PEDS_BITSET]
INT iVehCollisionBitset

INT iPedFixationStateCleanedUpBS[FMMC_MAX_PEDS_BITSET]

INT iEarlyCelebrationTeamPoints[ FMMC_MAX_TEAMS ]

//Boss Type (CEO & MC President etc.)
INT iBossType = -1

CONST_INT ciBOSSTYPE_CEO			0
CONST_INT ciBOSSTYPE_MC_PRESIDENT	1

//Beast Mode
INT iHealthBeforeStealth
INT iBeastPowersUnavailableDisplayCount = 0
SCRIPT_TIMER tdStealthDamageTimer
SCRIPT_TIMER tdLocationBlipTimer
SCRIPT_TIMER tdStealthToggleTimer
INT iBeastAttackSoundID = -1
INT iBeastSprintSoundID = -1
INT iThermalSoundID = -1
INT iNightVisSoundID = -1
INT iStealthSoundID = -1
BOOL bBeastBeenDamaged = FALSE
BOOL bBeastKilledPlayer = FALSE

//Collect blip timer
SCRIPT_TIMER tdCollectBlipTimer
//Health drain
SCRIPT_TIMER tdPerSecondDrainTimer
SCRIPT_TIMER tdStopDrainTimer
SCRIPT_TIMER tdDamageDrainRateTimer
SCRIPT_TIMER tdIsShooting
INT iStopDrainTime

//Rage against the campers
//SCRIPT_TIMER tdBeastMode

INT iManagePowerPlayUI
CONST_INT PPH_REQUEST_ASSETS 0
CONST_INT PPH_CHECK_IF_ASSETS_READY 1
CONST_INT PPH_BEGIN_SCALEFORM 2
CONST_INT PPH_INITIALISE 3
CONST_INT PPH_MAINSTATE 4

INT iPowerPlayPickupStarted

CONST_INT citeam0_BULLSHARK_TEST 	0
CONST_INT citeam1_BULLSHARK_TEST 	1
CONST_INT citeam0_BEAST_MODE 		2
CONST_INT citeam1_BEAST_MODE 		3
CONST_INT citeam0_THERMAL_VISION 	4
CONST_INT citeam1_THERMAL_VISION 	5
CONST_INT citeam0_SWAP 				6
CONST_INT citeam1_SWAP 				7
CONST_INT citeam0_FILTER 			8
CONST_INT citeam1_FILTER 			9
CONST_INT citeam0_SLOW_DOWN 		10
CONST_INT citeam1_SLOW_DOWN 		11
CONST_INT citeam0_BULLET_TIME 		12
CONST_INT citeam1_BULLET_TIME 		13
CONST_INT citeam0_HIDE_BLIPS		14
CONST_INT citeam1_HIDE_BLIPS		15
CONST_INT citeam0_BEAST_1			16
CONST_INT citeam0_BEAST_2			17
CONST_INT citeam1_BEAST_1			18
CONST_INT citeam1_BEAST_2			19

SCALEFORM_INDEX SF_PPH_MovieIndex

INT iPowerPlayEffectBS

CONST_INT ciPOWERPLAY_REASON_DEAD		0
CONST_INT ciPOWERPLAY_REASON_EXPIRED	1

//team 0
SCRIPT_TIMER tdUIBullSharkT0
SCRIPT_TIMER tdUIBeastModeT0
SCRIPT_TIMER tdUISwapT0
SCRIPT_TIMER tdUIFilterT0
SCRIPT_TIMER tdUIBulletTimeT0
SCRIPT_TIMER tdUIHideBlipsT0

//team 1
SCRIPT_TIMER tdUIBullSharkT1
SCRIPT_TIMER tdUIBeastModeT1
SCRIPT_TIMER tdUISwapT1
SCRIPT_TIMER tdUIFilterT1
SCRIPT_TIMER tdUIHideBlipsT1

BOOL bPowerPlayScaleFormActive 

INT iPowerUpJustPickedUp = -1
INT iThisPlayerPickedItUp = -1

CONST_INT DEFAULT_PICKUP_SHARD_TIME 3000

OBJECT_INDEX oiDronePickup
BLIP_INDEX   biDronePickup
VECTOR 		 vDronePosCache


//MAINTAIN_AIR_DRAG_CATCH_UP - Catch Up (Slows Down Top 3 Racers)
INT iDragPosition = -1

CONST_FLOAT DRAG_RESET							1.0

CONST_INT FIRST_PLACE_DRAG 						1
CONST_INT SECOND_PLACE_DRAG 					2
CONST_INT THIRD_PLACE_DRAG 						3

ENUM DRAG_STATE
	DRAG_STATE_NULL = 0,
	DRAG_STATE_TWO,
	DRAG_STATE_THREE,
	DRAG_STATE_DEFAULT
ENDENUM

CONST_FLOAT HIGH_DRAG  	2.0
CONST_FLOAT MED_DRAG  	1.55
CONST_FLOAT LOW_DRAG  	1.3
CONST_FLOAT ZERO_DRAG  	1.0


//Scaleform references to VV pickups
CONST_INT ciVVHUDZonedT0		0
CONST_INT ciVVHUDInverseT0		1
CONST_INT ciVVHUDDetonatorT0	2
CONST_INT ciVVHUDBombT0			3
CONST_INT ciVVHUDArmouredT0		4
CONST_INT ciVVHUDAcceleratorT0	5
CONST_INT ciVVHUDRepairT0		6
CONST_INT ciVVHUDGhostT0		7
CONST_INT ciVVHUDRocketsT0		8
CONST_INT ciVVHUDPronT0			9
//
CONST_INT ciVVHUDZonedT1		10
CONST_INT ciVVHUDInverseT1		11
CONST_INT ciVVHUDDetonatorT1	12
CONST_INT ciVVHUDBombT1			13
CONST_INT ciVVHUDArmouredT1		14
CONST_INT ciVVHUDAcceleratorT1	15
CONST_INT ciVVHUDRepairT1		16
CONST_INT ciVVHUDGhostT1		17
CONST_INT ciVVHUDRocketsT1		18
CONST_INT ciVVHUDPronT1			19

//

//Vehicle Vendetta HUD timers
SCRIPT_TIMER tdVehZoned
//Team 0
SCRIPT_TIMER tdVehFlipped0
SCRIPT_TIMER tdVehGhost0
SCRIPT_TIMER tdVehAccelerate0
SCRIPT_TIMER tdVehBeast0
SCRIPT_TIMER tdVehPron0
//Team 1
SCRIPT_TIMER tdVehFlipped1
SCRIPT_TIMER tdVehGhost1
SCRIPT_TIMER tdVehAccelerate1
SCRIPT_TIMER tdVehBeast1
SCRIPT_TIMER tdVehPron1

// Team 2 (added for Tiny Racers)
SCRIPT_TIMER tdVehPron2
SCRIPT_TIMER tdVehFlipped2

// Team 3 (added for Tiny Racers)
SCRIPT_TIMER tdVehPron3
SCRIPT_TIMER tdVehFlipped3

//Audio Timers

//Team 0
SCRIPT_TIMER tdVehAudBeast0
SCRIPT_TIMER tdVehAudPron0
SCRIPT_TIMER tdVehAudGhost0
SCRIPT_TIMER tdVehAudAccelerate0
SCRIPT_TIMER tdVehAudFlipped0
SCRIPT_TIMER tdVehAudRuiner0
SCRIPT_TIMER tdVehAudRamp0

//Team 1
SCRIPT_TIMER tdVehAudBeast1
SCRIPT_TIMER tdVehAudPron1
SCRIPT_TIMER tdVehAudGhost1
SCRIPT_TIMER tdVehAudAccelerate1
SCRIPT_TIMER tdVehAudFlipped1
SCRIPT_TIMER tdVehAudRuiner1
SCRIPT_TIMER tdVehAudRamp1

// Team 2 (added for Tiny Racers)
SCRIPT_TIMER tdVehAudPron2
SCRIPT_TIMER tdVehAudFlipped2
SCRIPT_TIMER tdVehAudRuiner2
SCRIPT_TIMER tdVehAudRamp2

// Team 3 (added for Tiny Racers)
SCRIPT_TIMER tdVehAudPron3
SCRIPT_TIMER tdVehAudFlipped3
SCRIPT_TIMER tdVehAudRuiner3
SCRIPT_TIMER tdVehAudRamp3

//Generic
SCRIPT_TIMER tdVehAudZoned
//SCRIPT_TIMER tdVehAudBomb

SCALEFORM_INDEX SF_VVH_MovieIndex

ENUM VehicleVendettaHUDState
	VV_REQUEST_ASSETS = 0,
	VV_INIT,
	VV_RUNNING
ENDENUM

VehicleVendettaHUDState eVVHUDState

BOOL bVehicleVendettaScaleFormActive 

INT iVehPowerUpJustPickedUp = -1
INT iVehThisPlayerPickedItUp = -1

SCALEFORM_INDEX sfiPPGenericMovie
ENUM PPGenericState
	PPG_REQUEST_ASSETS = 0,
	PPG_INIT,
	PPG_RUNNING
ENDENUM
PPGenericState ePPGenericState

CONST_INT ciPPGICON_BLANK			0
CONST_INT ciPPGICON_ACCELERATOR		1
CONST_INT ciPPGICON_ARMOURED		2
CONST_INT ciPPGICON_BEAST			3
CONST_INT ciPPGICON_BOMB			4
CONST_INT ciPPGICON_BOOST			5
CONST_INT ciPPGICON_DAY				6
CONST_INT ciPPGICON_DEADLINE		7
CONST_INT ciPPGICON_DETONATOR		8
CONST_INT ciPPGICON_DOPE			9
CONST_INT ciPPGICON_GHOST			10
CONST_INT ciPPGICON_INVERSE			11
CONST_INT ciPPGICON_JUMP			12
CONST_INT ciPPGICON_MACHINE_GUN		13
CONST_INT ciPPGICON_NIGHT			14
CONST_INT ciPPGICON_OFF_RADAR		15
CONST_INT ciPPGICON_RAGE			16
CONST_INT ciPPGICON_RAMP_BUGGY		17
CONST_INT ciPPGICON_REPAIR			18
CONST_INT ciPPGICON_ROCKETS			19
CONST_INT ciPPGICON_RUINER			20
CONST_INT ciPPGICON_SCORE			21
CONST_INT ciPPGICON_ZONED			22
CONST_INT ciPPGICON_SCORE_RING		23
CONST_INT ciPPGICON_TAG_OUT			24

FLOAT fPowerMadCaptureProgress[FMMC_MAX_TEAMS]

WEAPON_TYPE wtWeaponBeforeBeastmode = WEAPONTYPE_INVALID
//The Lost and Damned Scaleform
//Team 0
//SCRIPT_TIMER stTLADDay0
//SCRIPT_TIMER stTLADNight0

INT iCurrentTimeCycle = -1

SCALEFORM_INDEX SF_TLAD_MovieIndex

ENUM LostAndDamnedHUDState
	TLAD_REQUEST_ASSETS = 0,
	TLAD_INIT,
	TLAD_RUNNING
ENDENUM

CONST_INT ciDAY_TIMER		60000
CONST_INT ciNIGHT_TIMER		60000

LostAndDamnedHUDState eTLADHUDState

BOOL bLostAndDamnedScaleformActive

//Scaleform references to time of day - The Lost and the Damned
CONST_INT ciTLADHUDDayT0		0
CONST_INT ciTLADHUDNightT0		1
CONST_INT ciTLADHUDDayT1		2
CONST_INT ciTLADHUDNightT1		3

CONST_FLOAT cfCCTVTurnSpeedScalar	12.5
CONST_FLOAT cfCCTVTurnSpeedSlowdownPhase	0.7
CONST_FLOAT cfCCTVRotationTargetBuffer		5.0
CONST_INT MAX_NUM_CCTV_CAM 32
FLOAT fCCTVCamInitialHeading[MAX_NUM_CCTV_CAM]
CONST_INT ci_CCTV_FINISHED_TURN_CAM_0 0
ENUM CCTVCameraTurnState
	CamState_Wait, 		//0
	CamState_Turning 	//1
ENDENUM
SCRIPT_TIMER CCTVCameraTurnTimer[MAX_NUM_CCTV_CAM]
SCRIPT_TIMER CCTVSpottedTimer[MAX_NUM_CCTV_CAM]
SCRIPT_TIMER CCTVSpottedBodyTimer[MAX_NUM_CCTV_CAM]
SCRIPT_TIMER CCTVSpottedResetTimer
SCRIPT_TIMER CCTVTaserReEnablingTimer[MAX_NUM_CCTV_CAM]
INT iCCTVReachedDestinationBS

STRUCT FMMC_TASERED_ENTITY_VARS
	INT iHitByTaserBS // Set by the damage event when an object is hit by a taser & cleared in PROCESS_OBJECTS_EVERY_FRAME
	INT iReactedToTaserBS // Set & cleared by object processing functions on a per-system basis
	SCRIPT_TIMER stHitByTaserTimers[FMMC_MAX_NUM_DYNOPROPS]
	PTFX_ID ptLoopingTaserFX[FMMC_MAX_NUM_DYNOPROPS]
ENDSTRUCT
FMMC_TASERED_ENTITY_VARS sObjTaserVars
FMMC_TASERED_ENTITY_VARS sDynoPropTaserVars
INT iFuseBoxesCurrentlyTasered = 0
INT iCCTVCamsCurrentlyTasered = 0

CONST_INT ci_CCTV_TASER_DISABLE_TIME	30000
INT iStaggeredCCTVPedToCheck[MAX_NUM_CCTV_CAM]
INT iCCTVConeBitSet
INT iCCTVBrokenBS
CONST_INT ci_CCTV_CONE_BLIP_ADDED_0		0

CONST_FLOAT cf_CCTV_BaseLightIntensity		2.0
CONST_FLOAT cf_CCTV_FlickerLightIntensity	14.0
CONST_FLOAT cf_CCTV_TaserFlashIntensity		29.0
CONST_FLOAT cf_CCTV_LightLerpSpeed			10.0
CONST_FLOAT cf_CCTV_TaserLightFadeSpeed		31.50
CONST_FLOAT cf_CCTV_ReenableFlickerTime		0.500
FLOAT fCCTV_CurrentLightIntensity[MAX_NUM_CCTV_CAM]
FLOAT fCCTV_CurrentTargetLightIntensity[MAX_NUM_CCTV_CAM]

INT iObjCachedCamIndexes[FMMC_MAX_NUM_OBJECTS]
INT iDynoPropCachedCamIndexes[FMMC_MAX_NUM_DYNOPROPS]

//Team Regen
BLIP_INDEX biRegenBlip

BLIP_INDEX biLocateRadiusBlip
SCRIPT_TIMER tdRadiusBlipAlphaTimer
INT iCurrentLocateBlipped
SCRIPT_TIMER tdPassOutOfRangeTimer

BLIP_INDEX bounds_blip
BLIP_INDEX sec_bounds_blip
BLIP_INDEX map_bounds_blip
CONST_INT ciBOUNDS_FAIL_TIME 60000

VECTOR vBoundsSpawnPos_Temp
VECTOR vBoundsSpawnPos_TempSecondary
VECTOR vBoundsSpawnPos_TempSecondary2
SCRIPT_TIMER tdBoundsSpawnDelayTimer

CONST_INT ciMax_MINIGAME_CRATES		 1							// The total number of minigame crates
CONST_INT ciMAX_MINIGAME_CRATE_DOORS ciMax_MINIGAME_CRATES * 3 	// We always want a multiple of 3 (two doors and one lock)

//Celebration Screen
STRUCT CELEBRATION_STATS
	PLAYER_INDEX winningPlayer			//The index of the overall winner (or the top player from the winning team).
	INT iLocalPlayerCash				
	INT iLocalPlayerScore				//This is the score used to determine if this player beat a challenge for this mission. It may be a time value rather than score, but either can be put in here.
	INT iLocalPlayerJobPoints			
	INT iLocalPlayerXP					
	INT iLocalPlayerBetWinnings			//How much money the player won/lost on bets
	INT iLocalPlayerPosition			//If in teams this is just the position of the player's team, otherwise it's the player's leaderboard position.
	BOOL bPassedMission					
	BOOL bIsDraw						//I don't know if you can draw a mission but there's a screen for it so just in case you can then this variable covers it.
	STRING strFailReason
	TEXT_LABEL_63 strFailLiteral
	TEXT_LABEL_63 strFailLiteral2
ENDSTRUCT

BOOL bCreatedCustomCelebrationCam

CONST_INT ciCELEBRATION_BIT_SET_SCRIPTED_CUT_NEEDS_CLEANED_UP 			0
CONST_INT ciCELEBRATION_BIT_SET_bCelebrationSummaryComplete				1
CONST_INT ciCELEBRATION_BIT_SET_bStartedCelebrationBeforeCutsceneEnded	2
CONST_INT ciCELEBRATION_BIT_SET_bCelebrationUnfrozeSkycam				3
CONST_INT ciCELEBRATION_BIT_SET_bDoDelayedCelebrationFreeze				4
CONST_INT ciCELEBRATION_BIT_SET_bClosedScPage							5
CONST_INT ciCELEBRATION_BIT_SET_TriggerHeistwinnerCutscene				6
CONST_INT ciCELEBRATION_BIT_SET_TriggeredHeistwinnerCutscene			7
CONST_INT ciCELEBRATION_BIT_SET_TriggerEndCelebration					8
CONST_INT ciCELEBRATION_BIT_SET_EndCelebrationFinished					9
CONST_INT ciCELEBRATION_BIT_SET_TriggeredCelebrationStreamingRequests	10
CONST_INT ciCELEBRATION_BIT_SET_StoppedAllPostFxForSwitch				11
CONST_INT ciCELEBRATION_BIT_SET_TriggeredRenderPhasePause				12
CONST_INT ciCELEBRATION_BIT_SET_ClearedTasksForOutOfVehicle				13
CONST_INT ciCELEBRATION_BIT_SET_NeedToCheckForApartmentWalkInComplete	15
CONST_INT ciCELEBRATION_BIT_SET_NotDoingApartmentInvite					16
CONST_INT ciCELEBRATION_BIT_SET_FrozenRenderPhasesForFail				17
CONST_INT ciCELEBRATION_BIT_SET_PlacedPlayerForLowriderPostScene		18

#IF IS_DEBUG_BUILD

FUNC STRING GET_CELEBRATION_BIT_SET_NAME(INT iBit)
	SWITCH iBit
		CASE ciCELEBRATION_BIT_SET_SCRIPTED_CUT_NEEDS_CLEANED_UP 			RETURN "SCRIPTED_CUT_NEEDS_CLEANED_UP"
		CASE ciCELEBRATION_BIT_SET_bCelebrationSummaryComplete 				RETURN "bCelebrationSummaryComplete"
		CASE ciCELEBRATION_BIT_SET_bStartedCelebrationBeforeCutsceneEnded 	RETURN "bStartedCelebrationBeforeCutsceneEnded"
		CASE ciCELEBRATION_BIT_SET_bCelebrationUnfrozeSkycam 				RETURN "bCelebrationUnfrozeSkycam"
		CASE ciCELEBRATION_BIT_SET_bDoDelayedCelebrationFreeze 				RETURN "bDoDelayedCelebrationFreeze"
		CASE ciCELEBRATION_BIT_SET_bClosedScPage 							RETURN "bClosedScPage"
		CASE ciCELEBRATION_BIT_SET_TriggerHeistwinnerCutscene 				RETURN "TriggerHeistwinnerCutscene"
		CASE ciCELEBRATION_BIT_SET_TriggeredHeistwinnerCutscene 			RETURN "TriggeredHeistwinnerCutscene"
		CASE ciCELEBRATION_BIT_SET_TriggerEndCelebration 					RETURN "TriggerEndCelebration"
		CASE ciCELEBRATION_BIT_SET_EndCelebrationFinished 					RETURN "EndCelebrationFinished"
		CASE ciCELEBRATION_BIT_SET_TriggeredCelebrationStreamingRequests 	RETURN "TriggeredCelebrationStreamingRequests"
		CASE ciCELEBRATION_BIT_SET_StoppedAllPostFxForSwitch 				RETURN "StoppedAllPostFxForSwitch"
		CASE ciCELEBRATION_BIT_SET_TriggeredRenderPhasePause 				RETURN "TriggeredRenderPhasePause"
		CASE ciCELEBRATION_BIT_SET_ClearedTasksForOutOfVehicle 				RETURN "ClearedTasksForOutOfVehicle"
		CASE ciCELEBRATION_BIT_SET_NeedToCheckForApartmentWalkInComplete 	RETURN "NeedToCheckForApartmentWalkInComplete"
		CASE ciCELEBRATION_BIT_SET_NotDoingApartmentInvite 					RETURN "NotDoingApartmentInvite"
		CASE ciCELEBRATION_BIT_SET_FrozenRenderPhasesForFail 				RETURN "FrozenRenderPhasesForFail"
		CASE ciCELEBRATION_BIT_SET_PlacedPlayerForLowriderPostScene 		RETURN "PlacedPlayerForLowriderPostScene"
		DEFAULT RETURN "invalid" 
	ENDSWITCH
ENDFUNC

#ENDIF

INT iCelebrationBitSet

CELEBRATION_STATS sCelebrationStats
CELEBRATION_SCREEN_DATA sCelebrationData
CAMERA_INDEX camEndScreen

// entity killed bitsets

// -----------------------------------	SERVER BROADCAST DATA ----------------------------------------------------------------------
// 					Everyone can read this data, only the server can update it. 
//server bitset consts
CONST_INT SBBOOL_TEAM_0_ACTIVE			 0
CONST_INT SBBOOL_TEAM_1_ACTIVE			 1
CONST_INT SBBOOL_TEAM_2_ACTIVE			 2
CONST_INT SBBOOL_TEAM_3_ACTIVE			 3
CONST_INT SBBOOL_SKIP_TEAM_0_UPDATE		 4
CONST_INT SBBOOL_SKIP_TEAM_1_UPDATE		 5
CONST_INT SBBOOL_SKIP_TEAM_2_UPDATE		 6
CONST_INT SBBOOL_SKIP_TEAM_3_UPDATE		 7
//FREE									 8
CONST_INT SSBOOL_TEAM0_FINISHED 9
CONST_INT SSBOOL_TEAM1_FINISHED 10
CONST_INT SSBOOL_TEAM2_FINISHED 11
CONST_INT SSBOOL_TEAM3_FINISHED 12
CONST_INT SBBOOL_SKIP_PARTICIPANT_UPDATE 13
CONST_INT SBBOOL_FIRST_UPDATE_DONE 14
CONST_INT SBBOOL_POINTLESS_OBJECTS_LOADED	15
CONST_INT SBBOOL_MISSION_OVER 16
CONST_INT SBBOOL_ALL_JOINED_START 17
CONST_INT SBBOOL_SET_NOT_JOINABLE 18
CONST_INT SBBOOL_EVERYONE_RUNNING 19
CONST_INT SSBOOL_TEAM0_FAILED	  20
CONST_INT SSBOOL_TEAM1_FAILED	  21
CONST_INT SSBOOL_TEAM2_FAILED	  22
CONST_INT SSBOOL_TEAM3_FAILED	  23
CONST_INT SSBOOL_ALL_READY_INTRO_CUT  24
CONST_INT SBBOOL_CUTSCENE_TEAM0_PLAYERS_SELECTED 25
CONST_INT SBBOOL_CUTSCENE_TEAM1_PLAYERS_SELECTED 26
CONST_INT SBBOOL_CUTSCENE_TEAM2_PLAYERS_SELECTED 27
CONST_INT SBBOOL_CUTSCENE_TEAM3_PLAYERS_SELECTED 28
CONST_INT SBBOOL_HEIST_LEADER_LEFT 29
//FREE								30
CONST_INT SBBOOL_HEIST_CASH_VFX_REQUEST 31


//server team fail check bitsets
CONST_INT SBBOOL1_TEAM0_NEEDS 	   0
CONST_INT SBBOOL1_TEAM1_NEEDS      1
CONST_INT SBBOOL1_TEAM2_NEEDS      2
CONST_INT SBBOOL1_TEAM3_NEEDS      3
CONST_INT SBBOOL1_TEAM0_NEEDS_KILL 4
CONST_INT SBBOOL1_TEAM1_NEEDS_KILL 5
CONST_INT SBBOOL1_TEAM2_NEEDS_KILL 6
CONST_INT SBBOOL1_TEAM3_NEEDS_KILL 7
CONST_INT SBBOOL1_TEAM_0_WAS_ACTIVE 8
CONST_INT SBBOOL1_TEAM_1_WAS_ACTIVE 9
CONST_INT SBBOOL1_TEAM_2_WAS_ACTIVE 10
CONST_INT SBBOOL1_TEAM_3_WAS_ACTIVE 11
CONST_INT SBBOOL1_POINTLESS_IGNORE_PROTECT_OBJ 12
CONST_INT SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_0 13
CONST_INT SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_1 14
CONST_INT SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_2 15
CONST_INT SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_3 16
CONST_INT SBBOOL1_PROGRESS_OBJECTIVE_FOR_TEAM_0 17
CONST_INT SBBOOL1_PROGRESS_OBJECTIVE_FOR_TEAM_1 18
CONST_INT SBBOOL1_PROGRESS_OBJECTIVE_FOR_TEAM_2 19
CONST_INT SBBOOL1_PROGRESS_OBJECTIVE_FOR_TEAM_3 20
CONST_INT SBBOOL1_SUDDEN_DEATH         21
//FREE									22			
CONST_INT SBBOOL1_HEIST_ANY_PARTICIPANT_INSIDE_ANY_MP_PROPERTY 23
CONST_INT SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_0	24
CONST_INT SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_1	25
CONST_INT SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_2	26
CONST_INT SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_3	27
CONST_INT SBBOOL1_AGGRO_WILL_FAIL_FOR_TEAM_0	28//If set, the bits above mean it'll fail soon
CONST_INT SBBOOL1_AGGRO_WILL_FAIL_FOR_TEAM_1	29
CONST_INT SBBOOL1_AGGRO_WILL_FAIL_FOR_TEAM_2	30
CONST_INT SBBOOL1_AGGRO_WILL_FAIL_FOR_TEAM_3	31

CONST_INT SBBOOL2_AGGRO_FAIL_DUE_TO_BODY_FOUND_TEAM_0	0
CONST_INT SBBOOL2_AGGRO_FAIL_DUE_TO_BODY_FOUND_TEAM_1	1
CONST_INT SBBOOL2_AGGRO_FAIL_DUE_TO_BODY_FOUND_TEAM_2	2
CONST_INT SBBOOL2_AGGRO_FAIL_DUE_TO_BODY_FOUND_TEAM_3	3
CONST_INT SBBOOL2_FIRST_UPDATE_STARTED       			4
CONST_INT SBBOOL2_NEW_PARTICIPANT_LOOP_STARTED       	5
CONST_INT SBBOOL2_NEW_PARTICIPANT_LOOP_FINISHED      	6
CONST_INT SBBOOL2_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC 	7
CONST_INT SBBOOL2_TEAM_1_NEEDS_TO_WAIT_FOR_LEAVE_LOC 	8
CONST_INT SBBOOL2_TEAM_2_NEEDS_TO_WAIT_FOR_LEAVE_LOC 	9
CONST_INT SBBOOL2_TEAM_3_NEEDS_TO_WAIT_FOR_LEAVE_LOC 	10
CONST_INT SBBOOL2_HEIST_REWARD_SWAT_INVOLVED  			11
CONST_INT SBBOOL2_ROUNDS_PAUSE_LBD_SORT					12
CONST_INT SBBOOL2_ROUNDS_LBD_GO							13		
CONST_INT SBBOOL2_ROUNDS_LBD_DO							14
CONST_INT SBBOOL2_HEIST_ANY_PARTICIPANT_OUTSIDE_ANY_MP_PROPERTY 15
CONST_INT SBBOOL2_HEIST_FLARE_OUTDOORS_VFX_REQUEST		16
CONST_INT SBBOOL2_HEIST_DRILL_ASSET_REQUEST 			17
CONST_INT SBBOOL2_CUTSCENE_ON_FOOT_VISIBLE_CHECK		18
CONST_INT SBBOOL2_HEIST_FLARE_INDOORS_VFX_REQUEST 		19
CONST_INT SBBOOL2_HEIST_FLARE_UNDERWATER_VFX_REQUEST 	20
CONST_INT SBBOOL2_SERVER_INITIALISATION_TIMED_OUT		21
CONST_INT SBBOOL2_TEAM_0_IS_HACKING						22
CONST_INT SBBOOL2_TEAM_1_IS_HACKING						23
CONST_INT SBBOOL2_TEAM_2_IS_HACKING						24
CONST_INT SBBOOL2_TEAM_3_IS_HACKING						25
//FREE 													26
CONST_INT SBBOOL2_STARTED_COUNTDOWN						27
CONST_INT SBBOOL2_FINISHED_COUNTDOWN					28
CONST_INT SBBOOL2_SUDDEN_DEATH_MOREINLOC				29
CONST_INT SBBOOL2_SUDDEN_DEATH_KILLALLENEMIES			30
CONST_INT SBBOOL2_MISSION_ENDING_BECAUSE_PLAYERS_LEFT	31

CONST_INT SBBOOL3_RSGDATA_INITIALISED						0
CONST_INT SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN				1
CONST_INT SBBOOL3_SUDDEN_DEATH_BE_MY_VALENTINES				2
CONST_INT SBBOOL3_SUDDEN_DEATH_IN_AND_OUT					3
CONST_INT SBBOOL3_TOO_FEW_PLAYERS_FOR_NEXT_ROUND			4
CONST_INT SBBOOL3_TEAM_0_SHOULD_RESPAWN_BACK_AT_START_NOW	5
CONST_INT SBBOOL3_TEAM_1_SHOULD_RESPAWN_BACK_AT_START_NOW	6
CONST_INT SBBOOL3_TEAM_2_SHOULD_RESPAWN_BACK_AT_START_NOW	7
CONST_INT SBBOOL3_TEAM_3_SHOULD_RESPAWN_BACK_AT_START_NOW	8
CONST_INT SBBOOL3_SUDDEN_DEATH_POWER_PLAY					9
CONST_INT SBBOOL3_SUDDEN_DEATH_TARGET						10
CONST_INT SBBOOL3_MULTIRULE_TIMER_ENDED_GAME				11
CONST_INT SBBOOL3_SUDDEN_DEATH_PACKAGE_HELD					12
CONST_INT SBBOOL3_LBD_DISPLAYED								13
CONST_INT SBBOOL3_LBD_STOP									14
CONST_INT SBBOOL3_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN		15
//FREE 													  	16
//FREE 													  	17
//FREE 													  	18
//FREE 													  	19
//FREE 														20
CONST_INT SBBOOL3_MINIGAME_OBJ_TARGET_BEING_INVESTIGATED    21
CONST_INT SBBOOL3_MINIGAME_OBJ_0_FOUND                      22
CONST_INT SBBOOL3_MINIGAME_OBJ_1_FOUND                      23
CONST_INT SBBOOL3_MINIGAME_OBJ_2_FOUND                      24
CONST_INT SBBOOL3_MINIGAME_OBJ_3_FOUND                      25
CONST_INT SBBOOL3_MINIGAME_TARGET_FOUND						26
CONST_INT SBBOOL3_MINIGAME_SHOULD_SHOW_COLLECTABLE			27
//FREE 														28
CONST_INT SBBOOL3_TRADING_PLACES_TIMER_BAR_HUD_MODE			29
CONST_INT SBBOOL3_SUDDEN_DEATH_LOST_AND_DAMNED				30
CONST_INT SBBOOL3_SUDDEN_DEATH_GUNSMITH						31

CONST_INT SBBOOL4_PRON_DEATHMATCH_TOGGLE_CORONA				0
CONST_INT SBBOOL4_TLAD_HIGHSCORE_TOGGLE_CORONA				1
CONST_INT SBBOOL4_TEAM_0_CHECKED_CUTSCENE_DROPOFF			2
CONST_INT SBBOOL4_TEAM_1_CHECKED_CUTSCENE_DROPOFF			3
CONST_INT SBBOOL4_TEAM_2_CHECKED_CUTSCENE_DROPOFF			4
CONST_INT SBBOOL4_TEAM_3_CHECKED_CUTSCENE_DROPOFF			5
CONST_INT SBBOOL4_TEAM_0_APARTMENT_GET_AND_DELIVER			6
CONST_INT SBBOOL4_TEAM_1_APARTMENT_GET_AND_DELIVER			7
CONST_INT SBBOOL4_TEAM_2_APARTMENT_GET_AND_DELIVER			8
CONST_INT SBBOOL4_TEAM_3_APARTMENT_GET_AND_DELIVER			9
CONST_INT SBBOOL4_TEAM_0_GARAGE_GET_AND_DELIVER				10
CONST_INT SBBOOL4_TEAM_1_GARAGE_GET_AND_DELIVER				11
CONST_INT SBBOOL4_TEAM_2_GARAGE_GET_AND_DELIVER				12
CONST_INT SBBOOL4_TEAM_3_GARAGE_GET_AND_DELIVER				13
CONST_INT SBBOOL4_TEAM_0_SET_LOCATE_DROPOFF					14
CONST_INT SBBOOL4_TEAM_1_SET_LOCATE_DROPOFF					15
CONST_INT SBBOOL4_TEAM_2_SET_LOCATE_DROPOFF					16
CONST_INT SBBOOL4_TEAM_3_SET_LOCATE_DROPOFF					17
CONST_INT SBBOOL4_SUDDEN_DEATH_TURFWAR						18
CONST_INT SBBOOL4_DEADLINE_POWER_UP_ACTIVE_FOR_TEAM_0		19
CONST_INT SBBOOL4_DEADLINE_POWER_UP_ACTIVE_FOR_TEAM_1		20
CONST_INT SBBOOL4_DEADLINE_POWER_UP_ACTIVE_FOR_TEAM_2		21
CONST_INT SBBOOL4_DEADLINE_POWER_UP_ACTIVE_FOR_TEAM_3		22
CONST_INT SBBOOL4_FORCE_SUDDEN_DEATH_TIMER_START			23
CONST_INT SBBOOL4_TW_SHOULD_RESET_ALL_PROPS					24
CONST_INT SBBOOL4_TW_RESET_ALL_PROPS						25
CONST_INT SBBOOL4_TW_SHOULD_GIVE_POINTS						26
CONST_INT SBBOOL4_TW_GIVE_POINTS							27
CONST_INT SBBOOL4_TW_SD_END_POINTS_ALLOCATED				28
CONST_INT SBBOOL4_TR_SHOULD_PROGRESS_THROUGH_RESET			29
CONST_INT SBBOOL4_ALL_PLAYERS_IN_ANY_LOCATE_PASSED			30
CONST_INT SBBOOL4_SUDDEN_DEATH_JUGGERNAUT					31

CONST_INT SBBOOL5_BEAST_POWER_UP_ACTIVE_FOR_TEAM_0			0
CONST_INT SBBOOL5_BEAST_POWER_UP_ACTIVE_FOR_TEAM_1			1
CONST_INT SBBOOL5_BEAST_POWER_UP_ACTIVE_FOR_TEAM_2			2
CONST_INT SBBOOL5_BEAST_POWER_UP_ACTIVE_FOR_TEAM_3			3
CONST_INT SBBOOL5_RETRY_CURRENT_RULE_TEAM_0					4
CONST_INT SBBOOL5_RETRY_CURRENT_RULE_TEAM_1					5
CONST_INT SBBOOL5_RETRY_CURRENT_RULE_TEAM_2					6
CONST_INT SBBOOL5_RETRY_CURRENT_RULE_TEAM_3					7
CONST_INT SBBOOL5_RESET_TIMER_ON_RESTART_TEAM_0				8
CONST_INT SBBOOL5_RESET_TIMER_ON_RESTART_TEAM_1				9
CONST_INT SBBOOL5_RESET_TIMER_ON_RESTART_TEAM_2				10
CONST_INT SBBOOL5_RESET_TIMER_ON_RESTART_TEAM_3				11
CONST_INT SBBOOL5_ALL_RESPAWN_RULE_PEDS_READY				12
CONST_INT SBBOOL5_ALL_RESPAWN_RULE_VEHS_READY				13
CONST_INT SBBOOL5_ALL_RESPAWN_RULE_OBJS_READY				14
CONST_INT SBBOOL5_ALL_RESPAWN_RULE_DPROPS_READY				15
CONST_INT SBBOOL5_ENTITIES_CAN_RESPAWN_ON_RULE_CHANGE		16 //If changed update SBBOOL5_ENTITIES_CAN_RESPAWN_ON_RULE_CHANGE_COPY in fmmc_creation
CONST_INT SBBOOL5_ALL_PLAYERS_RENDERPHASES_PAUSED			17
CONST_INT SBBOOL5_TINY_RACERS_RERUN_OBJECTIVE_PROGRESSION	18
CONST_INT SBBOOL5_TIME_EXTENDED_FOR_TIEBREAKER_TEAM_0		19
CONST_INT SBBOOL5_TIME_EXTENDED_FOR_TIEBREAKER_TEAM_1		20
CONST_INT SBBOOL5_TIME_EXTENDED_FOR_TIEBREAKER_TEAM_2		21
CONST_INT SBBOOL5_TIME_EXTENDED_FOR_TIEBREAKER_TEAM_3		22
CONST_INT SBBOOL5_ELIMINATION_SCORE_GIVEN_THIS_RULE_TEAM_0	23
CONST_INT SBBOOL5_ELIMINATION_SCORE_GIVEN_THIS_RULE_TEAM_1	24
CONST_INT SBBOOL5_ELIMINATION_SCORE_GIVEN_THIS_RULE_TEAM_2	25
CONST_INT SBBOOL5_ELIMINATION_SCORE_GIVEN_THIS_RULE_TEAM_3	26
CONST_INT SBBOOL5_OVERTIME_SUDDEN_DEATH_ROUNDS				27
CONST_INT SBBOOL5_OVERTIME_PROGRESSED_DUE_TO_DEATH			28
CONST_INT SBBOOL5_TINY_RACERS_ROUND_WAS_A_DRAW				29
CONST_INT SBBOOL5_OVERTIME_WAIT_FOR_RESET					30
CONST_INT SBBOOL5_SUDDEN_DEATH_OVERTIME_RUMBLE				31

CONST_INT SBBOOL6_CONTROL_TIME_STOPPED_T0					0
CONST_INT SBBOOL6_CONTROL_TIME_STOPPED_T1					1
CONST_INT SBBOOL6_CONTROL_TIME_STOPPED_T2					2
CONST_INT SBBOOL6_CONTROL_TIME_STOPPED_T3					3
CONST_INT SBBOOL6_SUDDEN_DEATH_GUARDIAN						4
CONST_INT SBBOOL6_PLAY_COUNT_SET							5
CONST_INT SBBOOL6_DELETED_VEHICLES_OK_TO_PROGRESS			6
CONST_INT SBBOOL6_OVERTIME_PROGRESSED_DUE_TO_UPSIDEDOWN		7
CONST_INT SBBOOL6_SUDDEN_DEATH_STOCKPILE					8
CONST_INT SBBOOL6_BOMBUSHKA_EARLY_END_CHECKED				9
CONST_INT SBBOOL6_CACHED_SCORES_CHECK_FORCE_RULE_UPDATED	10
CONST_INT SBBOOL6_SUDDEN_DEATH_TRIGGERED_EARLY				11
CONST_INT SBBOOL6_AIRQUOTA_SEQUENCE_TOGGLE					12
CONST_INT SBBOOL6_AIRQUOTA_SEQUENCE_TOGGLE_CORONA			13
CONST_INT SBBOOL6_SUDDEN_DEATH_RUNNING_BACK_REMIX			14
CONST_INT SBBOOL6_LAP_PASSED								15
CONST_INT SBBOOL6_LAP_BOMBUSHKA_PROGRESSED					16
CONST_INT SBBOOL6_BOMBUSHKA_NEW_ROUND_SHARD					17
CONST_INT SBBOOL6_PLAYER_HAS_BEEN_SPOTTED_CCTV				18
CONST_INT SBBOOL6_HARD_TARGET_TIMER_BIT						19
CONST_INT SBBOOL6_USING_SYNCED_SCORE_TIMER					20
CONST_INT SBBOOL6_ALLOW_GANGCHASE_AFTER_SPEED_FAIL			21
//FREE														22
CONST_INT SBBOOL6_SUB_EXPLOSION_STARTED						23
CONST_INT SBBOOL6_SUB_EXPLOSION_BIGONE						24
CONST_INT SBBOOL6_COUNTED_VEH_HACK_TARGETS					25
//FREE														26
CONST_INT SBBOOL6_UPDATED_WEATHER_AND_TIME_FROM_CORONA		27
CONST_INT SBBOOL6_DEDUCTED_TIME_MISSING_PLAYERS_VJ_T0		28
CONST_INT SBBOOL6_DEDUCTED_TIME_MISSING_PLAYERS_VJ_T1		29
CONST_INT SBBOOL6_TIME_DEDUCTED_INTO_30S					30
CONST_INT SBBOOL6_USING_CYCLE_VEHICLE_PLACEMENT				31
	
CONST_INT SBBOOL7_SET_ALL_PLACED_PEDS_INTO_COVER					0
CONST_INT SBBOOL7_LOAD_AND_CREATE_ALL_FMMC_ENTITIES_RC				1
CONST_INT SBBOOL7_SHOULD_PLAY_RUNNING_BACK_REMIX_SCORED				2
CONST_INT SBBOOL7_SHOULD_PLAY_RUNNING_BACK_REMIX_BLOCKED			3
CONST_INT SBBOOL7_SHOULD_PLAY_RUNNING_BACK_REMIX_DESTROYED			4
CONST_INT SBBOOL7_BLOCK_RULE_TIMER_START_DUE_TO_SCORE_FOR_TEAM_0	5
CONST_INT SBBOOL7_BLOCK_RULE_TIMER_START_DUE_TO_SCORE_FOR_TEAM_1	6
CONST_INT SBBOOL7_BLOCK_RULE_TIMER_START_DUE_TO_SCORE_FOR_TEAM_2	7
CONST_INT SBBOOL7_BLOCK_RULE_TIMER_START_DUE_TO_SCORE_FOR_TEAM_3	8
CONST_INT SBBOOL7_BLOCK_MULTI_TIMER_START_DUE_TO_SCORE_FOR_TEAM_0	9
CONST_INT SBBOOL7_BLOCK_MULTI_TIMER_START_DUE_TO_SCORE_FOR_TEAM_1	10
CONST_INT SBBOOL7_BLOCK_MULTI_TIMER_START_DUE_TO_SCORE_FOR_TEAM_2	11
CONST_INT SBBOOL7_BLOCK_MULTI_TIMER_START_DUE_TO_SCORE_FOR_TEAM_3	12
CONST_INT SBBOOL7_THIS_IS_NOT_ROUND_ONE								13
CONST_INT SBBOOL7_SUDDEN_DEATH_BOMB_FOOTBALL						14
CONST_INT SBBOOL7_MULTI_RULE_TIMER_ROUND_RESTART_ENDED				15
CONST_INT SBBOOL7_USING_LIMITED_PLAYERS_IN_BOUNDS					16
CONST_INT SBBOOL7_SUDDEN_DEATH_TAG_TEAM								17
CONST_INT SBBOOL7_IN_BMBFB_SUDDEN_DEATH								18
CONST_INT SBBOOL7_GAME_MASTERS_TEAM_SWAPPED_ROLES_TEAM_0			19
CONST_INT SBBOOL7_GAME_MASTERS_TEAM_SWAPPED_ROLES_TEAM_1			20
CONST_INT SBBOOL7_GAME_MASTERS_TEAM_SWAPPED_ROLES_TEAM_2			21
CONST_INT SBBOOL7_GAME_MASTERS_TEAM_SWAPPED_ROLES_TEAM_3			22
CONST_INT SBBOOL7_GAME_MASTERS_END_MISSION_COUNT_SCORES				23
CONST_INT SBBOOL7_TAG_TEAM_INITIAL_TIMER							24
CONST_INT SBBOOL7_DEDUCTED_TIME_MISSING_PLAYERS_MJ_T0				25
CONST_INT SBBOOL7_DEDUCTED_TIME_MISSING_PLAYERS_MJ_T1				26
CONST_INT SBBOOL7_MULTIRULE_TIMER_REINIT							27
CONST_INT SBBOOL7_YACHT_CUTSCENE_RUNNING							28
CONST_INT SBBOOL7_YACHT_MOVED_TO_DESTINATION						29
CONST_INT SBBOOL7_CCTV_SPOTTED_DEAD_PED_BODY						30
CONST_INT SBBOOL7_CCTV_TEAM_AGGROED_PEDS							31

CONST_INT SBBOOL8_ALLOW_RAPPELLING_FROM_HELICOPTERS					0
CONST_INT SBBOOL8_COP_DECOY_STARTED									1
CONST_INT SBBOOL8_COP_DECOY_EXPIRED									2
CONST_INT SBBOOL8_COP_DECOY_FLEEING									3
CONST_INT SBBOOL8_COP_DECOY_DEDUCT_2_STAR							4
CONST_INT SBBOOL8_HOST_MISSION_VARIATION_DATA_APPLIED				5
CONST_INT SBBOOL8_A_PED_HAS_BEEN_STUNNED							6
CONST_INT SBBOOL8_A_PED_HAS_BEEN_TRANQUILIZED						7
CONST_INT SBBOOL8_VAULT_DRILL_ASSET_REQUEST 						8
CONST_INT SBBOOL8_VAULT_DRILL_ASSET_REQUEST_LASER					9
CONST_INT SBBOOL8_APPLIED_SCRIPT_CLOUD_CONTENT_FIXES				10

//Respawn BS
CONST_INT SRBS_RESPAWN_FROM_SPECTATOR_NOW_TEAM_0			0
CONST_INT SRBS_RESPAWN_FROM_SPECTATOR_NOW_TEAM_1			1
CONST_INT SRBS_RESPAWN_FROM_SPECTATOR_NOW_TEAM_2			2
CONST_INT SRBS_RESPAWN_FROM_SPECTATOR_NOW_TEAM_3			3
CONST_INT SRBS_RESPAWN_CHECK_ALL_DEAD_NOW_TEAM_0			4
CONST_INT SRBS_RESPAWN_CHECK_ALL_DEAD_NOW_TEAM_1			5
CONST_INT SRBS_RESPAWN_CHECK_ALL_DEAD_NOW_TEAM_2			6
CONST_INT SRBS_RESPAWN_CHECK_ALL_DEAD_NOW_TEAM_3			7
		
CONST_INT SBBOOL_SPECIAL_FIX_UNFREEZE_VEH_18				0
CONST_INT SBBOOL_SPECIAL_FIX_UNFREEZE_VEH_16				1
CONST_INT SBBOOL_SPECIAL_FIX_UNFREEZE_VEH_4					2
CONST_INT SBBOOL_SPECIAL_FIX_UNFREEZE_VEH_3					3
CONST_INT SBBOOL_SPECIAL_FIX_UNFREEZE_VEH_2					4
		
CONST_INT ciMAX_SYNCHRONIZED_ENTITY 12

CONST_INT SBBOOL_HACK_FIND_PLAYER			0
CONST_INT SBBOOL_HACK_DONE					1
CONST_INT SBBOOL_HACK_FAIL_ON_FAIL			2
CONST_INT SBBOOL_HACK_DO_HARD				3
//CONST_INT SBBOOL_HACK_HOLD_UP_PROGRESSION 	4 //Free!
CONST_INT SBBOOL_HACK_PASSES_RULE			5
CONST_INT SBBOOL_HACK_FAILED				6

#IF IS_DEBUG_BUILD
CONST_INT SBDEBUG_DONTFAILWHENOUTOFLIVES	0
#ENDIF

CONST_INT SBOOL_MISC_PLACED_VEHICLE_PLAYER_DATA_READY				0
CONST_INT SBOOL_MISC_PLACED_VEHICLE_PLAYER_DATA_READY_RESTART_LOGIC 1
//iPlacedVehiclePlayerPartBS

//-------------------------------------------------------
// 

ENUM FMMC_ELEVATOR_SERVER_STATE
	FMMC_ELEVATOR_SERVER_STATE_IDLE = 0,
	FMMC_ELEVATOR_SERVER_STATE_READY,
	FMMC_ELEVATOR_SERVER_STATE_ACTIVATED,
	FMMC_ELEVATOR_SERVER_STATE_WARPING,
	FMMC_ELEVATOR_SERVER_STATE_WARPED,
	FMMC_ELEVATOR_SERVER_STATE_DISABLED
ENDENUM
#IF IS_DEBUG_BUILD
FUNC STRING DEBUG_GET_FMMC_ELEVATOR_SERVER_STATE_STRING(FMMC_ELEVATOR_SERVER_STATE eState)
	SWITCH eState
		CASE FMMC_ELEVATOR_SERVER_STATE_IDLE 		RETURN "FMMC_ELEVATOR_SERVER_STATE_IDLE"
		CASE FMMC_ELEVATOR_SERVER_STATE_READY 		RETURN "FMMC_ELEVATOR_SERVER_STATE_READY"
		CASE FMMC_ELEVATOR_SERVER_STATE_ACTIVATED 	RETURN "FMMC_ELEVATOR_SERVER_STATE_ACTIVATED"
		CASE FMMC_ELEVATOR_SERVER_STATE_WARPING 	RETURN "FMMC_ELEVATOR_SERVER_STATE_WARPING"
		CASE FMMC_ELEVATOR_SERVER_STATE_WARPED 		RETURN "FMMC_ELEVATOR_SERVER_STATE_WARPED"
		CASE FMMC_ELEVATOR_SERVER_STATE_DISABLED 	RETURN "FMMC_ELEVATOR_SERVER_STATE_DISABLED"
	ENDSWITCH
	RETURN "INVALID FMMC_ELEVATOR_SERVER_STATE"
ENDFUNC
#ENDIF

ENUM ePropClaimingState
	ePropClaimingState_Unclaimed			= 0,
	ePropClaimingState_Claimed_Team_0 		= 1,
	ePropClaimingState_Claimed_Team_1 		= 2,
	ePropClaimingState_Claimed_Team_2 		= 3,
	ePropClaimingState_Claimed_Team_3 		= 4,
	ePropClaimingState_Contested			= 5,
	ePropClaimingState_Disabled				= 6
ENDENUM

CONST_INT ciTEAM0_CUTSCENE_SEEN_BY_TEAM0 			0
CONST_INT ciTEAM0_CUTSCENE_SEEN_BY_TEAM1 			1
CONST_INT ciTEAM0_CUTSCENE_SEEN_BY_TEAM2 			2
CONST_INT ciTEAM0_CUTSCENE_SEEN_BY_TEAM3 			3
 
CONST_INT ciTEAM1_CUTSCENE_SEEN_BY_TEAM0 			4
CONST_INT ciTEAM1_CUTSCENE_SEEN_BY_TEAM1 			5
CONST_INT ciTEAM1_CUTSCENE_SEEN_BY_TEAM2 			6
CONST_INT ciTEAM1_CUTSCENE_SEEN_BY_TEAM3 			7

CONST_INT ciTEAM2_CUTSCENE_SEEN_BY_TEAM0 			8
CONST_INT ciTEAM2_CUTSCENE_SEEN_BY_TEAM1 			9
CONST_INT ciTEAM2_CUTSCENE_SEEN_BY_TEAM2 			10
CONST_INT ciTEAM2_CUTSCENE_SEEN_BY_TEAM3 			11
								  
CONST_INT ciTEAM3_CUTSCENE_SEEN_BY_TEAM0 			12
CONST_INT ciTEAM3_CUTSCENE_SEEN_BY_TEAM1 			13
CONST_INT ciTEAM3_CUTSCENE_SEEN_BY_TEAM2 			14
CONST_INT ciTEAM3_CUTSCENE_SEEN_BY_TEAM3 			15

//-------------------------------------------------------

CONST_INT SB_CHECKPOINT_STRAND_1					0
CONST_INT SB_CHECKPOINT_1							1
CONST_INT SB_CHECKPOINT_2							2
CONST_INT SB_CHECKPOINT_3							3
CONST_INT SB_CHECKPOINT_4							4
CONST_INT SB_CP1_MRT_1								5
CONST_INT SB_CP1_MRT_2								6
CONST_INT SB_CP1_MRT_3								7
CONST_INT SB_CP1_MRT_4								8
CONST_INT SB_CP2_MRT_1								9
CONST_INT SB_CP2_MRT_2								10
CONST_INT SB_CP2_MRT_3								11
CONST_INT SB_CP2_MRT_4								12
CONST_INT SB_CP3_MRT_1								13
CONST_INT SB_CP3_MRT_2								14
CONST_INT SB_CP3_MRT_3								15
CONST_INT SB_CP3_MRT_4								16
CONST_INT SB_CP4_MRT_1								17
CONST_INT SB_CP4_MRT_2								18
CONST_INT SB_CP4_MRT_3								19
CONST_INT SB_CP4_MRT_4								20
CONST_INT SB_CP_QUICK_RESTART_MRT_FOR_TEAM_0		21
CONST_INT SB_CP_QUICK_RESTART_MRT_FOR_TEAM_1		22
CONST_INT SB_CP_QUICK_RESTART_MRT_FOR_TEAM_2		23
CONST_INT SB_CP_QUICK_RESTART_MRT_FOR_TEAM_3		24

CONST_INT ciFMMCSpawnAreaTimeout	10000

CONST_INT ciMAX_BURNING_VEHICLE_HEALTH 50000

ENUM CUT_PAINTING_STATE
	CP_STATE_WAITING_ON_INPUT,
	CP_STATE_ANIMATING_CUT,
	CP_STATE_EARLY_EXIT,
	CP_STATE_ROLL_UP_PAINTING
ENDENUM

CUT_PAINTING_STATE ecpsAnimationState[ciFMMC_MAX_PAINTING_INDEX_LEGACY]

CONST_INT ciCutPaintingTopLeft 		0
CONST_INT ciCutPaintingTopRight		1
CONST_INT ciCutPaintingBottomRight	2
CONST_INT ciCutPaintingBottomLeft	3
CONST_INT ciCutPaintingComplete		4

STRUCT MC_ServerBroadcastData
//DO NOT EVER ADD ANYTHING TO THIS. DO NOT ADJUST THE SIZES OF ANYTHING IN HERE.
//SPEAK TO JACK THALLON AND/OR JOHN SCOTT AND/OR MARTIN MACKINNON IF YOU WANT TO MAKE ANY MODIFICATION HERE.
	INT iServerGameState
	INT iServerBitSet
	INT iServerBitSet1
	INT iServerBitSet2
	INT iServerBitSet3
	INT iServerBitSet4
	INT iServerBitSet5
	INT iServerBitSet6
	INT iServerBitSet7
	INT iServerBitSet8
	
	INT iServerObjectBeingInvestigatedBitSet
	
	INT iServerPassBitSet[FMMC_MAX_TEAMS]
	
	INT iNumPlayerRuleHighestPriority[FMMC_MAX_TEAMS]
	INT iNumLocHighestPriority[FMMC_MAX_TEAMS]
	INT iNumPedHighestPriority[FMMC_MAX_TEAMS]
	INT iNumDeadPedHighestPriority[FMMC_MAX_TEAMS]
	INT iNumVehHighestPriority[FMMC_MAX_TEAMS]
	INT iNumVehSeatsHighestPriority[FMMC_MAX_TEAMS]
	INT iNumObjHighestPriority[FMMC_MAX_TEAMS]
	INT iCurrentPlayerRule[FMMC_MAX_TEAMS]
	
	INT iRequiredDeliveries[FMMC_MAX_TEAMS]
	
	INT iNumPedHighestPriorityHeld[FMMC_MAX_TEAMS]
	INT iNumVehHighestPriorityHeld[FMMC_MAX_TEAMS]
	INT iDriverPart[FMMC_MAX_VEHICLES]
	INT iNumObjHighestPriorityHeld[FMMC_MAX_TEAMS]
	
	INT iNumPriorityRespawnPed[FMMC_MAX_TEAMS]
	INT iNumPriorityRespawnVeh[FMMC_MAX_TEAMS]
	INT iNumPriorityRespawnObj[FMMC_MAX_TEAMS]
	
	INT iNumObjDelAtPriority[FMMC_MAX_TEAMS][FMMC_MAX_RULES]
	INT iAnyColObjRespawnAtPriority[FMMC_MAX_TEAMS]
	INT iObjDelTeam[FMMC_MAX_NUM_OBJECTS]
	
	INT iTimeObjectTeamControlBarCache[FMMC_MAX_TEAMS]
	
	INT iMaxLoopSize
	INT iNumPlayerRuleCreated
	INT iNumLocCreated
	INT iNumPedCreated
	INT iNumVehCreated
	INT iNumObjCreated
	INT iNumPedsSpawned //Includes respawns and gang backup

	INT iTargetType[FMMC_MAX_RULES][FMMC_MAX_TEAMS] 
	INT iMissionCriticalObj[FMMC_MAX_TEAMS]

	INT iLocteamFailBitset[FMMC_MAX_GO_TO_LOCATIONS]
	INT iLocOwner[FMMC_MAX_GO_TO_LOCATIONS]
	INT iPedAtYourHolding[FMMC_MAX_TEAMS][FMMC_MAX_PEDS_BITSET]
	INT iPedHoldPartPoints
	INT iPedteamFailBitset[FMMC_MAX_PEDS]
	INT	iMissionCriticalPed[FMMC_MAX_TEAMS][FMMC_MAX_PEDS_BITSET]
	
	INT iDeadPedPhotoBitset[FMMC_MAX_TEAMS][FMMC_MAX_PEDS_BITSET]
	INT iCopPed[FMMC_MAX_PEDS_BITSET]
	INT iVehAtYourHolding[FMMC_MAX_TEAMS]
	INT iVehHoldPartPoints
	INT iVehteamFailBitset[FMMC_MAX_VEHICLES]
	INT iMissionCriticalVeh[FMMC_MAX_TEAMS]
	INT iAmbientOverrideVehicle
	INT iAmbientOverrideVehicleSetup
	
	INT iObjAtYourHolding[FMMC_MAX_TEAMS]
	INT iObjHoldPartPoints[FMMC_MAX_NUM_OBJECTS]
	INT iObjteamFailBitset[FMMC_MAX_NUM_OBJECTS]
	
	INT iPlayerRuleLimit[FMMC_MAX_RULES][FMMC_MAX_TEAMS] 
	
	INT isearchingforPed = -1 
	INT isearchingforObj = -1 
	INT isearchingforVeh = -1 
	INT iSpawnArea = -1
	SCRIPT_TIMER tdSpawnAreaTimeout
	INT iVehSpawnBitset
	INT iPedRespawnVehBitset
	INT iPedFirstSpawnVehBitset
	INT iObjSpawnPedBitset[FMMC_MAX_PEDS_BITSET]
	INT iPedDelayRespawnBitset[FMMC_MAX_PEDS_BITSET]
	INT iVehDelayRespawnBitset
	
	INT iPedCleanup_NeedOwnershipBS[FMMC_MAX_PEDS_BITSET]
	INT iVehCleanup_NeedOwnershipBS // For server to request control when needing to cleanup a vehicle
	INT iObjCleanup_NeedOwnershipBS
	INT iDynoPropCleanup_NeedOwnershipBS
	INT iVehAboutToCleanup_BS // For server to request control when needing to cleanup a vehicle
	INT iRuleReCapture[FMMC_MAX_TEAMS][FMMC_MAX_RULES]
	
	NETWORK_INDEX niTargetID[FMMC_MAX_PEDS]
	PLAYER_INDEX TargetPlayerID[FMMC_MAX_PEDS]

	INT iPartWarpingIn = -1 //The participant currently doing the apartment warp door exit animation
	NETWORK_INDEX niAptDoor
	
	SCRIPT_TIMER tdJoinTimer
	
	SCRIPT_TIMER tdMissionLengthTimer
	INT iTotalMissionEndTime
	
	SCRIPT_TIMER tdHeistRewardMMTimer
	INT iHeistRewardMMTimer
	
	SCRIPT_TIMER timeServerCountdown
	
	//INT iBalancedTeam[MAX_NUM_MC_PLAYERS]
	
	INT iNumberOfTeams
	INT iNumActiveTeams
	INT iNumberOfPlayingTeams
	INT iFailTeam = FMMC_MAX_TEAMS
	INT iNumSpectators
	INT iNumberOfPart[FMMC_MAX_TEAMS]
	INT iTotalNumPart
	INT iNumberOfPlayingPlayers[FMMC_MAX_TEAMS]
	INT iNumberOfKnockedOutPlayers[FMMC_MAX_TEAMS]
	INT iTotalPlayingCoopPlayers[FMMC_MAX_TEAMS]
	INT iNumberOfLBPlayers[FMMC_MAX_TEAMS]
	INT iNumOfWanted[FMMC_MAX_TEAMS]
	INT iNumOfFakeWanted[FMMC_MAX_TEAMS]
	INT iNumOfMasks[FMMC_MAX_TEAMS]
	INT iTeamVehNumPlayers[FMMC_MAX_TEAMS]
	INT iTeamUniqueVehHeldNum[FMMC_MAX_TEAMS]
	INT iPriorityLocation[FMMC_MAX_TEAMS]
	
	INT iPartDeathEventBS // These two bits are used in server processing to get players swapping between teams on kill/death
	INT iPartNotAliveBS
	
	INT iCheckpointBitset
	
	INT iNextObjective[FMMC_MAX_TEAMS]
	INT iNextMission
	INT iEndMocapVariation = -1
	INT iCurrentExtraObjective[MAX_NUM_EXTRA_OBJECTIVE_ENTITIES][FMMC_MAX_TEAMS]
	
	INT iPedNearDropOff[FMMC_MAX_TEAMS][FMMC_MAX_PEDS_BITSET]
	INT iTeamScore[FMMC_MAX_TEAMS]
	INT iScoreOnThisRule[FMMC_MAX_TEAMS]
	INT iPlayerScore[MAX_NUM_MC_PLAYERS]
	INT iNumPlayerKills[MAX_NUM_MC_PLAYERS] 
	INT iNumPedKills[MAX_NUM_MC_PLAYERS]
	INT iNumPlayerDeaths[MAX_NUM_MC_PLAYERS]
	INT iNumHeadshots[MAX_NUM_MC_PLAYERS]
	INT iTeamPlayerKills[FMMC_MAX_TEAMS][FMMC_MAX_TEAMS]
	INT iKillScore[MAX_NUM_MC_PLAYERS]
	INT iTeamArrested[FMMC_MAX_TEAMS]
	INT iTeamCriticalAmmo[FMMC_MAX_TEAMS]
	
	INT iHostRefreshDpadValue
	INT iTotalScores
	
	INT iPedDelivered[FMMC_MAX_PEDS_BITSET]
	INT iPedSpookedBitset[FMMC_MAX_PEDS_BITSET]
	INT iPedAggroedBitset[FMMC_MAX_PEDS_BITSET]
	INT iPedAggroedFromEventBitset[FMMC_MAX_PEDS_BITSET]
	INT iPedSpookedByDeadBodyBitset[FMMC_MAX_PEDS_BITSET]
	//INT iPedTaskRangeTrigger[FMMC_MAX_PEDS_BITSET] - this is the same check as taskactive and activationrange != 0, so I've just replaced them with that
	INT iPedTaskActive[FMMC_MAX_PEDS_BITSET]
	INT iPedTaskStopped[FMMC_MAX_PEDS_BITSET]
	INT iPedSecondaryTaskActive[FMMC_MAX_PEDS_BITSET]
	INT iPedSecondaryTaskCompleted[FMMC_MAX_PEDS_BITSET]
	INT iPedAtGotoLocBitset[FMMC_MAX_PEDS_BITSET]
	INT iPedTasksArrivalResultsBS[FMMC_MAX_PEDS_BITSET]
	INT iPedCancelTasksBitset[FMMC_MAX_PEDS_BITSET]
	INT iPedAtSecondaryGotoLocBitset[FMMC_MAX_PEDS_BITSET]
	INT iPedShouldFleeDropOff[FMMC_MAX_PEDS_BITSET]
	INT iPedFirstSpawnBitset[FMMC_MAX_PEDS_BITSET]
	INT	iPedGunOnRuleGiven[FMMC_MAX_PEDS_BITSET] //Used so that we can check if a dead ped had been given a GunOnRule gun (on death their inventory clears, so we can't check if they had the gun or not)
	INT iPedCombatStyleChanged[FMMC_MAX_PEDS_BITSET]
	INT iPedDoneDefensiveAreaGoto[FMMC_MAX_PEDS_BITSET]
	
	INT iPedInvolvedInMiniGameBitSet[FMMC_MAX_PEDS_BITSET]
	
	INT iObjectiveMidPointBitset[FMMC_MAX_TEAMS]
	INT iObjectivePedAggroedBitset[FMMC_MAX_TEAMS] //Bitset to say if a ped has been aggroed for a certain team on a certain rule - the rule is the bitset index
	
	INT iProcessJobCompBitset
	
	INT iAverageMissionTime[FMMC_MAX_TEAMS]
	INT iMaxObjectives[FMMC_MAX_TEAMS]
	
	SCRIPT_TIMER tdMidpointFailTimer[FMMC_MAX_TEAMS]
	
	INT iObjCarrier[FMMC_MAX_NUM_OBJECTS]
	
	INT iVehRequestPart[FMMC_MAX_VEHICLES]
	INT iObjHackPart[FMMC_MAX_NUM_OBJECTS]
	
	INT iObjVaultDrillIDs[FMMC_MAX_VAULT_DRILL_GAMES]
	INT iObjVaultDrillDiscs[FMMC_MAX_VAULT_DRILL_GAMES]
	INT iObjVaultDrillProgress[FMMC_MAX_VAULT_DRILL_GAMES]
		
	// Termination timer
	SCRIPT_TIMER TerminationTimer
	
	#IF IS_DEBUG_BUILD	
		INT iDebugBitSet
		INT ijSkipBitset
	#ENDIF
	
//	// For thumb votes
//	INT iThumbBitset
//	INT iThumbUpCount
//	INT iThumbDownCount
	
	INT iReasonForObjEnd[FMMC_MAX_TEAMS]
	INT iPartCausingFail[FMMC_MAX_TEAMS]
	INT iEntityCausingFail[FMMC_MAX_TEAMS]
	SCRIPT_TIMER tdAggroCountdownTimer[FMMC_MAX_TEAMS]
	SCRIPT_TIMER tdCopSpottedFailTimer
	INT iAggroCountdownTimerSlack[FMMC_MAX_TEAMS] //add on some more time if they've got more people to kill before it fails
	
	INT iTeamCivillianKills[FMMC_MAX_TEAMS]
	INT iTeamKills[FMMC_MAX_TEAMS]
	INT iTeamDeaths[FMMC_MAX_TEAMS]
	INT iTotalCoopDeaths[FMMC_MAX_TEAMS]
	INT iteamHeadshots[FMMC_MAX_TEAMS]
	INT iTeamKillScore[FMMC_MAX_TEAMS]
	
	INT iCaptureBarPercentage[FMMC_MAX_TEAMS]
	INT iSpawnPointsUsed[FMMC_MAX_TEAMS]
	
	INT iWinningTeam = TEAM_INVALID	
	INT iSecondTeam = TEAM_INVALID	
	INT iThirdTeam = TEAM_INVALID	
	INT iLosingTeam = TEAM_INVALID	
	
	INT iLastTeamAlive = -1
		
	INT iVariablePlayerLives[FMMC_MAX_TEAMS]
	INT iVariableEnemyRespawns
	INT iDifficulty
	INT iNumStartingPlayers[FMMC_MAX_TEAMS]
	INT iTotalNumStartingPlayers
	SCRIPT_TIMER tdInitialisationTimeout
	SCRIPT_TIMER tdProcessedPreGameTimeout
	
	INT iWeather	= -1
	INT iTimeOfDay	= -1
	FLOAT fVehicleDensity  = -1.0
	FLOAT fPedDensity	  = -1.0
	INT iPolice
	INT iOptionsMenuBitSet
	INT iHackDoorOpenBitset
	INT iHackDoorBlownBitset
	INT iHackDoorThermiteDamageBitset
	INT iWasHackObj
	INT iIsHackContainer
	INT iOpenHackContainer
	
	INT iCrateBrokenBitset
	INT iCrateDestructionProcessed
	INT iFragableCrate
	INT iFragableCrateCreated
	
	//
	// CUTSCENES
	//
	INT iLastCompletedGoToLocation = -1
	INT iCutsceneID[FMMC_MAX_TEAMS]
	INT iCutsceneStreamingIndex[FMMC_MAX_TEAMS]
	INT iCutsceneStarted[FMMC_MAX_TEAMS]
	INT iCutsceneFinished[FMMC_MAX_TEAMS]
	INT iCutscenePlayerBitset[FMMC_MAX_TEAMS]
	PLAYER_INDEX piCutscenePlayerIDs[FMMC_MAX_TEAMS][FMMC_MAX_CUTSCENE_PLAYERS]

	// End Cutscene
	INT iEndCutsceneNum[FMMC_MAX_TEAMS]
	INT iEndCutsceneEntityType[FMMC_MAX_TEAMS]

	// ????
	INT iNumCutscenePlayers[FMMC_MAX_TEAMS]
	INT iMaxNumCutscenePlayers[FMMC_MAX_TEAMS]
	//INT iNumCutsceneStartedPlayers[FMMC_MAX_TEAMS]
	INT iNumCutscenePlayersFinished[FMMC_MAX_TEAMS]
	INT iNumFinishedCutsceneEventsReceived[FMMC_MAX_TEAMS]
	INT iNumOtherTeamsConsideredForTeamCutscene // This stores which teams should view another team's cutscene
	
	PLAYER_INDEX dropOffAtApartmentPlayer
	SCRIPT_TIMER tdCutsceneServerCheckDelay[FMMC_MAX_TEAMS]

	VECTOR vTeamCentreStartPoint[FMMC_MAX_TEAMS]
	
	SERVER_EOM_VARS sServerFMMC_EOM
	
	SCRIPT_TIMER tdAreaTimer[FMMC_MAX_GO_TO_LOCATIONS][FMMC_MAX_TEAMS]
	SCRIPT_TIMER tdHoldTimer[FMMC_MAX_GO_TO_LOCATIONS][FMMC_MAX_TEAMS]
	SCRIPT_TIMER tdCapturePointsTimer
	
	SCRIPT_TIMER tdCutsceneStartTime[FMMC_MAX_TEAMS]
	
	INT tdMultiTimerStartBitset[FMMC_MAX_TEAMS]

	INT	iGotoLocationDataTakeoverTime[FMMC_MAX_TEAMS][FMMC_MAX_RULES]
	INT	iPedTakeoverTime[FMMC_MAX_TEAMS][FMMC_MAX_RULES]
	INT	iVehTakeoverTime[FMMC_MAX_TEAMS][FMMC_MAX_RULES]
	INT	iObjTakeoverTime[FMMC_MAX_TEAMS][FMMC_MAX_RULES]
	
	INT cutscene_vehicle_count = 0
	INT iSpawnScene = -1
	INT iSpawnShot = -1
	
	INT iVehicleRappelSeatsBitset[FMMC_MAX_VEHICLES]
	
	INT iCashGrabTotal[FMMC_MAX_NUM_OBJECTS]
	INT iCashGrabBonusPilesGrabbed[FMMC_MAX_NUM_OBJECTS]
	INT iCashGrabTotalDrop
	INT iCashGrabTotalTake
	
	INT iEndCutscene	
	
	BOOL bPlayerHealthFailed
	INT iPlayerMostHealthLost = 0
	
	INT iHeistPresistantPropertyID
	
	eFailMissionEnum 			eCurrentTeamFail[FMMC_MAX_TEAMS]	
	
	SCRIPT_TIMER iBetweenRoundsTimer
	
	INT iBinbagOwners[MAX_NUM_MC_PLAYERS]
	INT iBinbagRuleActive[FMMC_MAX_TEAMS]
	
	INT iCashGrabRuleActive[FMMC_MAX_TEAMS]
	INT iThermiteRuleActive[FMMC_MAX_TEAMS]
	
	INT iNumHeistPlays = -1	
	
	INT iObjectCaptureOffset[FMMC_MAX_TEAMS]
	
	INT iTeamTurnScore[FMMC_MAX_TEAMS][MAX_PENALTY_PLAYERS]
	
	INT iSessionScriptEventKey
	
	INT iPointsGivenToPass[FMMC_MAX_TEAMS]
	INT iServerOTPartToMove = -1
	
	INT iGranularCurrentPoints[FMMC_MAX_TEAMS]
	
	INT iUndeliveredVehCleanupBS[FMMC_MAX_TEAMS]
	INT iCleanedUpUndeliveredVehiclesBS
	
	INT iVehicleRespawnPool
	INT iUsableMOCIndex = -1
	
	INT iVehicleNotSpawningDueToRandomBS
	INT iAllPlayersInUniqueVehiclesBS[FMMC_MAX_TEAMS]
	
	SCRIPT_TIMER tdSuddenDeath_ShrinkingBoundsHUDTimer[FMMC_MAX_TEAMS]
	
	INT iCachedMissionScoreLimit
	INT iCachedInitialPoints
	
	INT iCachedBestRuleScore[FMMC_MAX_TEAMS]
	INT iCachedRuleScore[FMMC_MAX_TEAMS]
	
	PLAYER_INDEX piTurnsActivePlayer[FMMC_MAX_TEAMS]
	
	INT iServerBS_PassRuleVehAfterRespawn
	
	INT iHardTargetPointer[FMMC_MAX_TEAMS]
	INT iHardTargetParticipant[FMMC_MAX_TEAMS]
	
	CCTVCameraTurnState eCCTVCamState[MAX_NUM_CCTV_CAM]
	INT iCameraNumber[MAX_NUM_CCTV_CAM]
	FMMC_ELECTRONIC_ENTITY_TYPE eCameraType[MAX_NUM_CCTV_CAM]
	INT CCTV_TURNED_BITSET
	
	SCRIPT_TIMER tdControlPedTimerOffset[FMMC_MAX_PEDS]
	SCRIPT_TIMER tdControlVehTimerOffset[FMMC_MAX_VEHICLES]
	SCRIPT_TIMER tdTurfWarScoreAddTimer
	
	INT iAllPlayersInSeparateVehiclesBS[FMMC_MAX_TEAMS]
	
	SCRIPT_TIMER stCBwaitTimer
	
	INT iDynoPropShouldRespawnNowBS
	
	INT iTRReadyToFadeTime = -1
	INT iTRReadyForControlTime = -1
	SCRIPT_TIMER tdTRSpawnTimer
	PARTICIPANT_INDEX piTRLastPartStanding
	INT iTRLastParticipantToUse = -1

	INT iNumTeamRuleAttempts[FMMC_MAX_TEAMS]
	
	SCRIPT_TIMER stVehicleDoorsOpenTimer
	INT iOpenVehicleDoorsBitset = 0
	
	INT iCargobobAttachedBitset = 0
	INT iCargobobDettachedBitset = 0
	INT iCargobobShouldDetachBitset = 0
	
	SCRIPT_TIMER tdControlObjTeamTimer[FMMC_MAX_TEAMS]
	
	INT iObjTeamCarrier
	INT iObjCaptured
	
	INT iOverriddenScriptedCutsceneRule[FMMC_MAX_TEAMS]
	INT iOverriddenScriptedCutsceneIndex[FMMC_MAX_TEAMS]
	
	//DO NOT EVER ADD ANYTHING TO THIS. DO NOT ADJUST THE SIZES OF ANYTHING IN HERE.
	//SPEAK TO JACK THALLON AND/OR JOHN SCOTT AND/OR MARTIN MACKINNON IF YOU WANT TO MAKE ANY MODIFICATION HERE.
ENDSTRUCT

MC_ServerBroadcastData MC_serverBD

STRUCT MC_ServerBroadcastData1

	SCRIPT_TIMER tdControlVehTimer[FMMC_MAX_VEHICLES]
	SCRIPT_TIMER tdControlObjTimer[FMMC_MAX_NUM_OBJECTS]
	SCRIPT_TIMER tdControlPedTimer[FMMC_MAX_PEDS]
	
	INT inumberOfTeamInArea[FMMC_MAX_GO_TO_LOCATIONS][FMMC_MAX_TEAMS]
	INT iNumberOfTeamInAllAreas[FMMC_MAX_TEAMS]
	INT inumberOfTeamInAnyArea[FMMC_MAX_GO_TO_LOCATIONS][FMMC_MAX_TEAMS]
	INT inumberOfTeamInLeaveArea[FMMC_MAX_GO_TO_LOCATIONS][FMMC_MAX_TEAMS]
	INT	iNumTeamControlKills[FMMC_MAX_GO_TO_LOCATIONS][FMMC_MAX_TEAMS]
	INT	iOldTeamControlkills[FMMC_MAX_GO_TO_LOCATIONS][FMMC_MAX_TEAMS]
	INT iNumControlKills[MAX_NUM_MC_PLAYERS]
	
	FMMC_SERVER_DATA_STRUCT sFMMC_SBD
	NETWORK_INDEX niMinigameCrateDoors[ ciMAX_MINIGAME_CRATE_DOORS ]	// This holds the network indexes for the doors on the minigame crate objects
	
	sCreatedCountData sCreatedCount
	
	INT iNumBackupChaseCandidates[FMMC_MAX_TEAMS]
	INT iNumBackupChasing[FMMC_MAX_TEAMS]
	INT iNumBackupSpawned[FMMC_MAX_TEAMS][FMMC_MAX_RULES]
	INT iChaseAreaBitset[FMMC_MAX_TEAMS]
	INT iCurrentBackupSpawn = -1
	INT iVariableNumBackupAllowed[FMMC_MAX_TEAMS]
	GangBackupData GangBackup[MAX_BACKUP_VEHICLES]
	INT iBackupTargetAvenger = -1
	
	INT iPlayerForHackingMG = -1
	INT iPlayerHacking = -1
	INT iVehicleForHackingMG = -1
	INT iServerHackMGBitset	
	
	INT iTotalPackages
	
	INT iPTLDroppedPackageOwner[FMMC_MAX_NUM_OBJECTS] //Also used for Stockpile
	INT iPTLDroppedPackageLastTeam[FMMC_MAX_NUM_OBJECTS] //Used as last captor of package in stockpile
	INT iPTLDroppedPackageLastOwner[FMMC_MAX_NUM_OBJECTS] //Also used for Stockpile
	SCRIPT_TIMER stPTLWinTimer
	SCRIPT_TIMER stTRLastPlayerTimer
	SCRIPT_TIMER stTRBoundsShrinkTimer
	
	INT iNumberOfPlayingPlayersInRole[FMMC_MAX_TEAMS][FMMC_MAX_ROLES]
	
	INT iTrevorBodhiBreakoutSynchSceneID = -1
	INT iGOFoundryHostageSynchSceneID = -1
	
	INT iObjDeliveredOnceBS // Bits to say if an object has been delivered
	
	INT iHackDialogueCompletedBS[ciMAX_DIALOGUE_BIT_SETS_LEGACY]
	
	INT iObjectiveProgressionHeldUpBitset[FMMC_MAX_TEAMS] // Bitset to say if a rule should be held up for a certain team (e.g. for circuit hack minigame)
	
	INT iNumCutsceneSpectatorPlayers[ FMMC_MAX_TEAMS ]	
	
	NETWORK_INDEX niDZSpawnVehicle[FMMC_MAX_TEAMS*2] //Aerial spawn vehicle, 2 per team
	NETWORK_INDEX niDZSpawnVehPed[FMMC_MAX_TEAMS*2] //Ped to sit in each spawn veh
	
	INT iTeamWinningCapturePoint = -1
	
	PLAYER_INDEX piTimeBarWinners[5]	//5 players max
	
	INT iCoronaHighestRankPlayer = -1
	
	INT iCoronaTeamLives[FMMC_MAX_TEAMS]
	
	INT iRespawnQueue[FMMC_MAX_TEAMS][FMMC_MAX_REZQ_PARTS]
	INT iRespawnMultiKillQueue[FMMC_MAX_TEAMS]
	INT	iServerRespawnBitset //Need this to come through at the same time as the above 2
	
	INT iPartToMove[FMMC_MAX_OVERTIME_TEAMS]
	INT iTeamToMove
	INT iCurrentRound
	
	INT iBSVehicleStopForever = 0
	INT iBSVehicleStopDuration = 0
	SCRIPT_TIMER timeVehicleStop
	INT iVehStopTimeToWait
	
	FLOAT fCarnageBar_BarFilledHeliTarget = -1.0 									// Add fCarnageBar_FillRate incrementally
	FLOAT fCarnageBar_BarFilledCarTarget = -1.0									// Add fCarnageBar_FillRate incrementally
	FLOAT fCarnageBar_FillRate													// Added onto BarFilledTarget every X amount of seconds, in order to prevent BD spam.
	FLOAT fCarnageBar_PedKilledForFill											// Cleared when added to fCarnageBar_BarFilledTarget
	FLOAT fCarnageBar_VehDestroyedForFill										// Cleared when added to fCarnageBar_BarFilledTarget
	FLOAT fCarnageBar_StuntPerformedForFill										// Cleared when added to fCarnageBar_BarFilledTarget
	INT iPartLookingAtCar = 0
	
	INT iPedFixationTaskEnded[FMMC_MAX_PEDS_BITSET]
	INT iPedFixationTaskStarted[FMMC_MAX_PEDS_BITSET]
	INT iVehicleTheftWantedTriggered
	
	INT iMissionVariationChoicesBS[FMMC_MAX_MC_VAR_CHOICE_BS]
	
	INT iVehicleBlockedWantedConeResponseBS
	INT iVehicleBlockedWantedConeResponseSeenIllegalBS
	INT iDoorUpdateOffRuleBS
	
	FMMC_LEGACY_MISSION_CONTINUITY_VARS sLEGACYMissionContinuityVars
	
	INT iOffRuleMinigameCompleted //Bitset to track off rule completed minigames
	
	SCRIPT_TIMER stCopDecoyActiveTimer
	
	INT iTeamThermalCharges[FMMC_MAX_TEAMS]
	
	INT iCurrentCutPaintingState[ciFMMC_MAX_PAINTING_INDEX_LEGACY]
	
	INT iCurrentVaultDoorPlantedExplosives_LeftSide = 0
	INT iCurrentVaultDoorPlantedExplosives_RightSide = 0
	
#IF IS_DEBUG_BUILD
	INT iCurrentContentIdHash = -1
#ENDIF
	
	FMMC_ELEVATOR_SERVER_STATE eElevatorState[FMMC_MAX_ELEVATORS]
	SCRIPT_TIMER tdElevatorTimer[FMMC_MAX_ELEVATORS]
	
	INT iStrandMissionsComplete = 0
	INT iVaultTake = 0
	INT iDailyCashRoomTake = 0
	INT iDepositBoxTake = 0
	INT iEntranceUsed = -1
	
	INT iScriptedCutsceneSyncScene = -1
ENDSTRUCT

MC_ServerBroadcastData1 MC_serverBD_1

STRUCT MC_ServerBroadcastData2
	TEXT_LABEL_63 tParticipantNames[MAX_NUM_MC_PLAYERS]
	INT iPedGotoProgress[FMMC_MAX_PEDS]
	INT iPartPedFollows[FMMC_MAX_PEDS]
	INT iOldPartPedFollows[FMMC_MAX_PEDS]
	INT iPedState[FMMC_MAX_PEDS]
	INT iOldPedState[FMMC_MAX_PEDS]
	INT iAssociatedAction[FMMC_MAX_PEDS]
	INT iTargetID[FMMC_MAX_PEDS]
	
	INT iCurrentPedRespawnLives[FMMC_MAX_PEDS]
	INT iCurrentVehRespawnLives[FMMC_MAX_VEHICLES]
	INT iCurrentObjRespawnLives[FMMC_MAX_NUM_OBJECTS]
	
	//Can use iObjSpawnPedBitset for peds
	INT iVehSpawnedOnce
	INT iObjSpawnedOnce
	INT iVehIsDestroyed
	
	FLOAT fVIPDamage
	FLOAT fHeistVehDamage
	INT iHeistVehRepairCost
	
	INT iPedOldGoto[FMMC_MAX_PEDS]
	STRUCT_ATRICIFICIAL_LIGHTS_DATA sArtificialLightsData
	
	INT iVipOriginalHealth
	
	INT iClobberedScore[ FMMC_MAX_TEAMS ]
	
	INT iNumPartPlayingAndFinished[ FMMC_MAX_TEAMS ]
	
	INT iCamObjDestroyed
	
	NETWORK_INDEX niDrillMinigameWall
	NETWORK_INDEX niDrillMinigameDoor
	
	PLAYER_INDEX playerLocOwner[FMMC_MAX_GO_TO_LOCATIONS][FMMC_MAX_TEAMS]
	
	//ARENA TRAPS
	FMMC_ARENA_TRAPS_HOST_INFO sTrapInfo_Host
	
		
	INT iArenaAudioScore = -1
	CELEB_SERVER_DATA sCelebServer
		
	INT iAggroText_PedSpooked		= -1
	INT iAggroText_PartCausingSpook = -1
	INT iAggroText_SpookReason		= -1
	
	INT iSafetyDepositCabinetOccupiedUsedBS
	INT iSafetyDepositBoxDrilledBS[ciMAX_NUMBER_OF_HANDHELD_DRILL_LOCKBOXES]
	INT iSafetyDepositBoxMiniGameConfiguration = -1
	INT iSafetyDepositBoxMiniGamePatternPreset = -1
ENDSTRUCT
MC_ServerBroadcastData2 MC_serverBD_2

INT iPopulationHandle = -1
BOOL bVehicleGenerator = FALSE
 
STRUCT MC_ServerBroadcastData3
	
	CROWD_CONTROL_SERVER_DATA sCCServerData
	INT iRoundLbdProgress
	INT iWinningPlayerForRounds = -1
	INT iCrowdpedbitset[ciFMMC_PED_BITSET_SIZE]
	PLAYER_INDEX iWinningPlayerForRoundsId
	SCENARIO_BLOCKING_INDEX 	scenBlockServer = NULL
	
	SCRIPT_TIMER tdPedSpawnDelay[FMMC_MAX_PEDS]
	SCRIPT_TIMER tdVehSpawnDelay[FMMC_MAX_VEHICLES]
	SCRIPT_TIMER tdObjSpawnDelay[FMMC_MAX_NUM_OBJECTS]
	
	FLOAT fSumoSuddenDeathRadius
	INT fSumoSuddenDeathTimeToGetIn
	
	//This will need upped to [4] if we ever do a powerup mode with 4 teams...
	SCRIPT_TIMER tdBullShark[2]
	SCRIPT_TIMER tdThermalVision[2]
	SCRIPT_TIMER tdSwap[2]
	SCRIPT_TIMER tdFilter[2]
	SCRIPT_TIMER tdSlowDown[2]
	SCRIPT_TIMER tdBulletTime //Applies to everyone no need to have seperate timer for each team
	SCRIPT_TIMER tdHideBlips[2]
	SCRIPT_TIMER tdBeastMode[2][2]//[team][beast]
	
	INT iBeastPart[2][2]//[team][beast]
	
	INT iTeamToScoreLast = -1
	VECTOR vVIPMarker
	
	INT iPronPlayerIsDrawingLines	//Bitset to track which PRON players have started drawing their lines
	
	SCRIPT_TIMER stTLADDay
	SCRIPT_TIMER stTLADNight
	
	INT iClobberedLaps[FMMC_MAX_TEAMS]
	
	INT iPropDestroyedBS[FMMC_MAX_PROP_BITSET]	//CEIL(FMMC_MAX_NUM_PROPS / 32)
	
	INT iNumSVMPlays = -1
	
	FLOAT f1stDragAmount = 1.0
	FLOAT f2ndDragAmount = 1.0
	FLOAT f3rdDragAmount = 1.0
	
	DRAG_STATE eDragState = DRAG_STATE_NULL
	
	INT iTeamTurnsRoundWonBS[2]
	INT iTaggedEntityBitset
	INT iTaggedEntityType[FMMC_MAX_TAGGED_ENTITIES]
	INT iTaggedEntityIndex[FMMC_MAX_TAGGED_ENTITIES]
	
	INT iTDTowersRemaining[FMMC_MAX_TEAMS]
	INT iTDTowersMaxStart[FMMC_MAX_TEAMS]	
	
	INT iBombFB_ExplosionPointsThisMiniRound[2]
	
	#IF IS_DEBUG_BUILD	
		INT iMultiObjectiveTimeLimitDebugCache[FMMC_MAX_TEAMS]
		INT iRuleObjectiveTimeLimitDebugCache[FMMC_MAX_TEAMS]
		INT iMissionLengthTimerDebugCache
	#ENDIF
	INT iRuleObjectiveTimeLimitKeepSyncCache[FMMC_MAX_TEAMS]
	
	INT iPlacedVehiclePlayerPart[FMMC_MAX_TEAMS][FMMC_MAX_TEAMSPAWNPOINTS]
	INT iPlacedVehiclePlayerPartBS
	
	INT iMissionSpecialFixBS
	
	SCRIPT_TIMER tdObjectiveLimitTimer[FMMC_MAX_TEAMS]
	SCRIPT_TIMER tdMultiObjectiveLimitTimer[FMMC_MAX_TEAMS]
	SCRIPT_TIMER tdLimitTimer
	INT iTimerPenalty[FMMC_MAX_TEAMS]
	INT iMultiObjectiveTimeLimit[FMMC_MAX_TEAMS]	
	
	INT iNumberOfPlayersInBounds1[FMMC_MAX_TEAMS]
	INT iNumberOfPlayersInBounds2[FMMC_MAX_TEAMS]
	INT iPlayerBoundAllowedBitset1
	INT iPlayerBoundAllowedBitset2
	SCRIPT_TIMER tdStolenPackageCooldownTimer
	
	SCRIPT_TIMER tdTagOutCooldown[FMMC_MAX_TEAMS]
	INT iServerTagTeamPartTurn[FMMC_MAX_TEAMS]
	INT iServerTagTeamPartTimeTagged[NUM_NETWORK_PLAYERS]
	
	INT iBombFB_ExplodedBS
	INT iBombFB_GoalProcessedBS
	INT iBombFB_OfficialRuleIndex = -1
	
	INT iGameMasterCurrentTeamActive = -1
	
	INT iArena_Lighting = -1
	INT iGameMasterTeamRoleSwapCounter = 0
	
	ARENA_CONTESTANT_SERVER_DATA arenaContestantTurretServer
	
	VECTOR vCratePickupCoords[FMMC_MAX_NUM_OBJECTS][ciMAX_CRATE_PICKUPS]
	
	//Trip Skip Data
	TRIP_SKIP_SERVER_BD TSServerData[FMMC_MAX_TEAMS]
	TRIP_SKIP_SERVER_BD FTSServerData
	
	INT iSuddenDeathTargetRound
	
	SCRIPT_TIMER tdTeamMissionTime[FMMC_MAX_TEAMS] // A timer counting how long this team has been playing
	INT iTeamMissionTime[FMMC_MAX_TEAMS]
	
	INT iVersusOutfitStyleSetup = -2
	INT iVersusOutfitStyleChoice = -2
	
	INT iAdditionalTeamLives[FMMC_MAX_TEAMS]
	
	// Condemned mode
	INT iCurrentCondemnedTeam = FMMC_MAX_TEAMS
	FLOAT fCondemnedTeamScore[FMMC_MAX_TEAMS]
	INT iCondemnedScoreUpdate = 0
	
	INT iGhostingPlayerVehiclesVeh = -1

ENDSTRUCT
MC_ServerBroadcastData3 MC_serverBD_3

NEXT_JOB_STRUCT nextJobStruct

STRUCT VEHICLE_SWAP_STRUCT
	MODEL_NAMES 		mnVehicleModelSwap
	INT 				iVehicleModelSwap
	INT 				iVehicleHealthSwap
	
	INT 				iVehicleTurretSwap
	INT 				iVehicleArmourSwap
	INT 				iVehicleAirCounterSwap
	INT 				iVehicleBombBaySwap
	INT 				iVehicleExhaustSwap
	
	INT 				iVehicleAirCounterCooldown = -1
ENDSTRUCT

//DO NOT EVER ADD ANYTHING TO THIS. DO NOT ADJUST THE SIZES OF ANYTHING IN HERE.
//SPEAK TO JACK THALLON AND/OR JOHN SCOTT AND/OR MARTIN MACKINNON IF YOU WANT TO MAKE ANY MODIFICATION HERE.
STRUCT MC_ServerBroadcastData4
	INT iCurrentHighestPriority[FMMC_MAX_TEAMS]
	INT iTimesRevalidatedObjectives[FMMC_MAX_TEAMS] // This is so that we don't get job comp events that move a mission back to the start, but then the same event from a different player completing that objective again in the future
	INT iPlayerRule[FMMC_MAX_RULES][FMMC_MAX_TEAMS] 
	INT iPlayerRulePriority[FMMC_MAX_RULES][FMMC_MAX_TEAMS] 
	INT	iGotoLocationDataRule[FMMC_MAX_GO_TO_LOCATIONS][FMMC_MAX_TEAMS]
	INT iGotoLocationDataPriority[FMMC_MAX_GO_TO_LOCATIONS][FMMC_MAX_TEAMS] 
	INT	iPedRule[FMMC_MAX_PEDS][FMMC_MAX_TEAMS]
	INT iPedPriority[FMMC_MAX_PEDS][FMMC_MAX_TEAMS]
	INT	iVehRule[FMMC_MAX_VEHICLES][FMMC_MAX_TEAMS]
	INT iVehPriority[FMMC_MAX_VEHICLES][FMMC_MAX_TEAMS]
	INT	iObjRule[FMMC_MAX_NUM_OBJECTS][FMMC_MAX_TEAMS]
	INT iObjPriority[FMMC_MAX_NUM_OBJECTS][FMMC_MAX_TEAMS]
	
	INT iPlayerRuleMissionLogic[FMMC_MAX_TEAMS]
	INT iLocMissionLogic[FMMC_MAX_TEAMS]
	INT iPedMissionLogic[FMMC_MAX_TEAMS]
	INT iVehMissionLogic[FMMC_MAX_TEAMS]
	INT iObjMissionLogic[FMMC_MAX_TEAMS]
	INT iObjMissionSubLogic[FMMC_MAX_TEAMS]
	
	INT iTeamLapsCompleted[FMMC_MAX_TEAMS]
	
	INT iTeamSwapLockFlag
	INT iLastHighestPriority[FMMC_MAX_TEAMS]
	INT iTargetScoreMultiplierSetting
	INT iGunsmithLoadoutSetting
	SCRIPT_TIMER tdLapTimer[FMMC_MAX_TEAMS]
	
	sFMMC_RandomSpawnGroupStruct rsgSpawnSeed
	INT iSpawnGroupsBuiltInPlay_TempSpawnSeedBS
	
	ePropClaimingState		ePropClaimState[FMMC_MAX_NUM_PROPS]
	INT	iPropOwnedByPart[FMMC_MAX_NUM_PROPS]
	
	INT	iPropAmountOwnedByPart[NUM_NETWORK_PLAYERS]
	INT iPropsAmountOwnedByTeam[FMMC_MAX_TEAMS]
	
	INT iCapturePedRequestPartBS[ciFMMC_PED_BITSET_SIZE]
	INT iCapturePedBeingCapturedBS[ciFMMC_PED_BITSET_SIZE]
	INT iPedExplodeKillOnCaptureBS[ciFMMC_PED_BITSET_SIZE]
	INT iPedClearFailOnGoTo[ciFMMC_PED_BITSET_SIZE]
	INT iPedSpeedOverride[ciFMMC_PED_BITSET_SIZE]
	INT iPedHeightOverride[ciFMMC_PED_BITSET_SIZE]
	INT iCaptureTimestamp
	
	INT iCaptureVehRequestPartBS
	INT iCaptureVehBeingCapturedBS
	INT iVehExplodeKillOnCaptureBS
	INT iVehUnlockOnCapture
	
	INT iFirstCheckpointRule[FMMC_MAX_TEAMS]
	INT iSecondCheckpointRule[FMMC_MAX_TEAMS]
	INT iThirdCheckpointRule[FMMC_MAX_TEAMS]
	INT iFourthCheckpointRule[FMMC_MAX_TEAMS]
	
	NETWORK_INDEX niBJXL
	SCRIPT_TIMER tdPedRuleSkipTimer
	INT iDialogueRangeBitset[ciFMMC_PED_BITSET_SIZE]
	
	PTFX_ID ptfxVehicleFireFX_Front[FMMC_MAX_VEHICLES]
	PTFX_ID ptfxVehicleFireFX_Back[FMMC_MAX_VEHICLES]
	FLOAT fFireStrength[FMMC_MAX_VEHICLES]
	FLOAT fBurningVehicleHealth[FMMC_MAX_VEHICLES]
	INT iBurningVehiclePTFXBitset
	
	FLOAT fTotalBurningVehFireDamage[FMMC_MAX_VEHICLES]
	INT iNumberOfFiresExtinguished[FMMC_MAX_TEAMS]
	
	INT iHardTargetMiniRound
	
	VEHICLE_SWAP_STRUCT sVehicleSwaps[FMMC_MAX_TEAMS][FMMC_MAX_RULES]
	
	//DO NOT EVER ADD ANYTHING TO THIS. DO NOT ADJUST THE SIZES OF ANYTHING IN HERE.
	//SPEAK TO JACK THALLON AND/OR JOHN SCOTT AND/OR MARTIN MACKINNON IF YOU WANT TO MAKE ANY MODIFICATION HERE.
	
	INT iGangChasePedsKilledThisRule
	INT iPlayThisAudioTriggerASAP_SERVER = -1
	
	INT iVehHackedReadyForDeathBS
	INT iVehHackeDeathCountedBS
	INT iHackingTargetsRemaining
	
	INT iHackingFails
	INT iFacilityPosVehSpawned
	
	INT iPedKilledBS[ciFMMC_PED_BITSET_SIZE]
	INT iObjectDoNotBlipBS
	INT iObjectsDestroyedForSpecialHUDBitset[FMMC_MAX_TEAMS]
	INT iObjectsDestroyedForSpecialHUD[FMMC_MAX_TEAMS]
	INT iObjectsRequiredForSpecialHUD[FMMC_MAX_TEAMS]

	INT iAvengerHoldTransitionBlockedCount[FMMC_MAX_TEAMS]
	PLAYER_INDEX piGangBossID
	
	SCRIPT_TIMER tdIncrementalRuleFailTimer[FMMC_MAX_TEAMS]
	INT iRuleTimerFailElapsedTime[FMMC_MAX_TEAMS]
	
	INT iDestroyedTurretBitSet
	INT iTransitionSessionMultirule_TimeStamp[FMMC_MAX_TEAMS]
	INT iTransitionSessionMultirule_Checkpoint[FMMC_MAX_TEAMS]
	
	FLOAT fVehSpeed
	INT iExplodeUpdateIter
	FLOAT fExplodeProgress
	
	INT iHostDirectionalDoorUnlockedBS
	
	INT iInteractWith_BombPlantParticipant = -1
	
	INT iDailyCashVaultStackValue = -1

ENDSTRUCT
MC_ServerBroadcastData4 MC_serverBD_4
//DO NOT EVER ADD ANYTHING TO THIS. DO NOT ADJUST THE SIZES OF ANYTHING IN HERE.
//SPEAK TO JACK THALLON AND/OR JOHN SCOTT AND/OR MARTIN MACKINNON IF YOU WANT TO MAKE ANY MODIFICATION HERE.

// -----------------------------------	CLIENT BROADCAST DATA ----------------------------------------------------------------------
// 					Everyone can read this data, only the local player can update it. 

CONST_INT 	PBBOOL_PRESSED_F 0
CONST_INT	PBBOOL_PRESSED_S 1
CONST_INT	PBBOOL_PRESSED_J 2
CONST_INT	PBBOOL_CLR_SRV_OBJ_BIT 3
CONST_INT	PBBOOL_OBJECTIVE_BLOCKER 4
CONST_INT   PBBOOL_FINISHED       5
CONST_INT   PBBOOL_START_SPECTATOR 6
CONST_INT 	PBBOOL_PLAYER_FAIL 	7
CONST_INT 	PBBOOL_PLAYER_OUT_OF_LIVES 8
CONST_INT 	PBBOOL_CHASE_TARGET    9
CONST_INT   PBBOOL_ALL_HEADSHOTS   10
CONST_INT   PBBOOL_WITH_CARRIER   11
CONST_INT   PBBOOL_TERMINATED_SCRIPT 12
//FREE								 13
CONST_INT   PBBOOL_IN_GROUP_PED_VEH   14
CONST_INT   PBBOOL_ON_LEADERBOARD     15
CONST_INT   PBBOOL_REQUEST_CUTSCENE_PLAYERS 16
CONST_INT   PBBOOL_LAST_LIFE             17
CONST_INT   PBBOOL_EARLY_END_SPECTATOR   18
CONST_INT   PBBOOL_WEARING_MASK          19
CONST_INT   PBBOOL_HEIST_HOST            20
CONST_INT	PBBOOL_HEIST_I_AM_INSIDE_ANY_MP_PROPERTY 21
CONST_INT   PBBOOL_ANY_SPECTATOR         			22
CONST_INT   PBBOOL_CC_FAILCUT_FINISH     			23
CONST_INT   PBBOOL_LOCK_SYNC_STARTED     			24
CONST_INT	PBBOOL_I_AM_DROWNING		 			25
CONST_INT	PBBOOL_DROPPED_OUT_OF_SECONDARY_ANIM	26
CONST_INT	PBBOOL_RESTRICTION_ZONE_ROCKETS_REQUIRED	27
CONST_INT	PBBOOL_HEIST_SPECTATOR 28
CONST_INT	PBBOOL_PLAYING_CUTSCENE 29
//FREE 								30
CONST_INT   PBBOOL_SYNC_LOCK_FORCE_CLEANUP 31


CONST_INT	PBBOOL2_REACHED_GAME_STATE_RUNNING					0
CONST_INT	PBBOOL2_COMPLETED_CELEBRATION_STREAMING_REQUESTS	1
CONST_INT	PBBOOL2_BLOCK_SECONDARY_AND_IDLE_ANIMS				2
CONST_INT	PBBOOL2_HACKING_MINIGAME_COMPLETED					3
CONST_INT	PBBOOL2_HACKING_INTRO_COMPLETED						4
CONST_INT	PBBOOL2_PLAYING_MOCAP_CUTSCENE						5
CONST_INT	PBBOOL2_STARTED_CUTSCENE							6
CONST_INT	PBBOOL2_FINISHED_CUTSCENE                           7
CONST_INT	PBBOOL2_HACKING_MINIGAME_FAILED						8
CONST_INT	PBBOOL2_PLAYED_CONTAINER_CUTSCENE					9
CONST_INT	PBBOOL2_PLAYER_DEATH_CAUSED_FAIL					10
CONST_INT	PBBOOL2_REACHED_GAME_STATE_INI						11
CONST_INT	PBBOOL2_I_AM_SPECTATOR_WATCHING_MOCAP				12
CONST_INT	PBBOOL2_COMPLETED_POPULATION_OF_CELEB_GLOBALS		13
CONST_INT	PBBOOL2_PLAYER_OUT_BOUNDS_FAIL						14
CONST_INT	PBBOOL2_PLAYER_LEAVE_AREA_FAIL						15
CONST_INT	PBBOOL2_CC_FAILCUT_START							16
CONST_INT	PBBOOL2_CASCO_LOCK_SHOT_OFF							17
CONST_INT	PBBOOL2_CASCO_LOCK_CLEAR_OF_CONTAINER_DOORS			18	
CONST_INT	PBBOOL2_IN_MISSION_BOUNDS							19
CONST_INT	PBBOOL2_IN_MISSION_BOUNDS2							20
CONST_INT	PBBOOL2_LOSE_GANG_CHASE								21
CONST_INT 	PBBOOL2_RELEASE_LOCK_SHOT_OFF_AUDIO_BANK			22
CONST_INT	PBBOOL2_USE_FAKE_PHONE								23
CONST_INT	PBBOOL2_TASK_PLAYER_CUTANIM_INTRO					24
CONST_INT	PBBOOL2_TASK_PLAYER_CUTANIM_OUTRO					25
CONST_INT	PBBOOL2_IN_DROP_OFF									26
CONST_INT	PBBOOL2_DROPPING_OFF								27
CONST_INT	PBBOOL2_IN_MID_MISSION_CUTSCENE						28
CONST_INT	PBBOOL2_ALL_PARTICIPANTS_LAUNCHED_CELEB_SCRIPT		29
CONST_INT	PBBOOL2_I_HAVE_SEEN_BENNY_GARAGE_POST_MISSION_SCENE	30

//iClientBitSet3

CONST_INT	PBBOOL3_RUGBY_GOAL_TARGET_MISSED					0
CONST_INT	PBBOOL3_RUGBY_GOAL_TARGET_HIT						1
CONST_INT	PBBOOL3_SPAWN_PROTECTION_ON							2
CONST_INT	PBBOOL3_ALL_TEAM_MEMBERS_WITHIN_REQDIST				3
CONST_INT	PBBOOL3_SUDDEN_DEATH_JUGGERNAUT						4
CONST_INT	PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN			5
CONST_INT	PBBOOL3_TINY_RACERS_LAST_PLAYER_WARPED				6
CONST_INT	PBBOOL3_TINY_RACERS_SCREEN_PAUSED					7
CONST_INT	PBBOOL3_TINY_RACERS_PLAYER_WARPED					8
CONST_INT	PBBOOL3_RESTART_RENDERPHASE_PAUSED					9
CONST_INT	PBBOOL3_TINY_RACERS_321_FINISHED					10
CONST_INT	PBBOOL3_TAKEN_A_TURN								11
CONST_INT	PBBOOL3_TINY_RACERS_PLAYER_HAS_PROGRESSED			12
CONST_INT	PBBOOL3_OVERTIME_LANDED_SUCCESSFULLY				13
CONST_INT	PBBOOL3_TRANSFORMED_INTO_JUGGERNAUT					14
CONST_INT	PBBOOL3_TRANSFORMED_INTO_BEAST						15
CONST_INT	PBBOOL3_UPGRADED_WEP_VEHICLE						16
CONST_INT	PBBOOL3_HAS_OBJECT_INVENTORY						17
CONST_INT	PBBOOL3_PROCESSED_PRE_GAME							18
CONST_INT	PBBOOL3_HAS_DELETED_OLD_VEHICLE						19
CONST_INT	PBBOOL3_WAS_JUGGERNAUT_ON_DEATH						20
//FREE 															21
//FREE 															22
CONST_INT	PBBOOL3_INITIATED_VEHICLE_SEAT_SWAP					23
CONST_INT	PBBOOL3_DATA_CHANGE_READY_TO_MOVE_VEHICLE			24
CONST_INT	PBBOOL3_DATA_CHANGING_SEAT_FINAL					25
CONST_INT	PBBOOL3_DATA_CHANGING_SEAT_DONE						26
CONST_INT	PBBOOL3_DATA_CHANGING_SEAT_GAVE_CONSENT				27
//FREE															28
CONST_INT	PBBOOL3_TEAM_TURNS_SET_ASSIGNED						29
CONST_INT	PBBOOL3_PLAYER_IS_A_HARD_TARGET						30
CONST_INT	PBBOOL3_PLAYER_USED_NEAREST_VEH_BAIL				31

//iClientBitSet4
CONST_INT 	PBBOOL4_PLAYER_IS_USING_SCRIPTED_GUN_CAM			0
CONST_INT	PBBOOL4_PLAYER_IS_AIMING_AT_PROTECT_PED				1
//FREE
CONST_INT	PBBOOL4_TREAT_AS_WANTED								3
CONST_INT   PBBOOL4_SPEED_FAIL_TRIGGERED						4
CONST_INT   PBBOOL4_REQUEST_AVENGER_HOLD_TRANSITION_BLOCK		5
CONST_INT   PBBOOL4_BLOCKING_AVENGER_HOLD_TRANSITION			6
CONST_INT	PBBOOL4_SPAWNED_IN_A_PLACED_VEHICLE					7
CONST_INT	PBBOOL4_INSIDE_A_PLAY_AREA_BOUND_1					8
CONST_INT	PBBOOL4_INSIDE_A_PLAY_AREA_BOUND_2					9
CONST_INT	PBBOOL4_ROAMING_SPECTATOR							10
CONST_INT	PBBOOL4_REQUEST_TAG_OUT								11
CONST_INT 	PBBOOL4_ASSIGNED_PLAYER_TEAM						12
CONST_INT 	PBBOOL4_START_POSITION_SET							13
CONST_INT 	PBBOOL4_FMMC_YACHTS_READY							14
CONST_INT 	PBBOOL4_SYNC_PLAYERS_FOR_END_OF_CUTSCENE			15
CONST_INT	PBBOOL4_RUNNING_WALL_RAPPEL_NOT_ON_RAPPEL_RULE		16
CONST_INT 	PBBOOL4_REACHED_CUT_PROCESS_SHOTS					17
CONST_INT	PBBOOL4_RAPELLING_FROM_HELI							18
CONST_INT	PBBOOL4_IS_USING_THE_HANDHELD_DRILL					19
CONST_INT	PBBOOL4_READY_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE		20
CONST_INT	PBBOOL4_REGISTERED_ENTITIES_FOR_END_MOCAP_PASS_OVER	21

CONST_INT	TUTORIAL_PLAYER_FINISHED_TUTORIAL_CUT	0

//Assuming that MAX_BACKUP_VEHICLES = 5
CONST_INT	PBGANGSPAWN_UNIT_0						0
CONST_INT	PBGANGSPAWN_UNIT_1						1
CONST_INT	PBGANGSPAWN_UNIT_2						2
CONST_INT	PBGANGSPAWN_UNIT_3						3
CONST_INT	PBGANGSPAWN_UNIT_4						4
CONST_INT	PBGANGSPAWN_UNIT_0_TRY_FORWARDS			5
CONST_INT	PBGANGSPAWN_UNIT_1_TRY_FORWARDS			6
CONST_INT	PBGANGSPAWN_UNIT_2_TRY_FORWARDS			7
CONST_INT	PBGANGSPAWN_UNIT_3_TRY_FORWARDS			8
CONST_INT	PBGANGSPAWN_UNIT_4_TRY_FORWARDS			9
CONST_INT	PBGANGSPAWN_UNIT_0_SUCCEED_FORWARDS		10
CONST_INT	PBGANGSPAWN_UNIT_1_SUCCEED_FORWARDS		11
CONST_INT	PBGANGSPAWN_UNIT_2_SUCCEED_FORWARDS		12
CONST_INT	PBGANGSPAWN_UNIT_3_SUCCEED_FORWARDS		13
CONST_INT	PBGANGSPAWN_UNIT_4_SUCCEED_FORWARDS		14
CONST_INT	PBGANGSPAWN_UNIT_0_AIR_FAILED_HELISPAWN	15
CONST_INT	PBGANGSPAWN_UNIT_1_AIR_FAILED_HELISPAWN	16
CONST_INT	PBGANGSPAWN_UNIT_2_AIR_FAILED_HELISPAWN	17
CONST_INT	PBGANGSPAWN_UNIT_3_AIR_FAILED_HELISPAWN	18
CONST_INT	PBGANGSPAWN_UNIT_4_AIR_FAILED_HELISPAWN	19

CONST_INT 	GOTO_HIT_SOUND_DEFAULT					0
CONST_INT	GOTO_HIT_SOUND_ALT						1
CONST_INT	GOTO_HIT_SOUND_OFF						2
CONST_INT	GOTO_HIT_SOUND_BOMBDISARM				3

CONST_INT	ciBS_MAN_RESPAWN_STARTED								0
CONST_INT	ciBS_MAN_RESPAWN_ENTERED_VEHICLE						1
CONST_INT	ciBS_MAN_RESPAWN_PASSENGERS_LEFT_VEHICLE				2
CONST_INT	ciBS_MAN_RESPAWN_FINISHED								3
CONST_INT	ciBS_MAN_RESPAWN_FORCED									4

CONST_INT 	ci_CARNAGEBAR_BS_LOOKING_AT_CAR					0
CONST_INT 	ci_CARNAGEBAR_BS_IS_USING_HELI_CAM				1
CONST_INT 	ci_CARNAGEBAR_BS_IS_DRIVER_OF_HELI				2
CONST_INT 	ci_CARNAGEBAR_BS_IS_DRIVER_OF_CAR				3

CONST_INT	ci_HUMANE_SYNC_LOCK__INACTIVE	-1
CONST_INT	ci_HUMANE_SYNC_LOCK__WORKING	0
CONST_INT	ci_HUMANE_SYNC_LOCK__FAILED		1
CONST_INT	ci_HUMANE_SYNC_LOCK__SUCCESS	2

ENUM FMMC_ELEVATOR_CLIENT_STATE
	FMMC_ELEVATOR_CLIENT_STATE_IDLE = 0,
	FMMC_ELEVATOR_CLIENT_STATE_IN_POSITION,
	FMMC_ELEVATOR_CLIENT_STATE_WARPED,
	FMMC_ELEVATOR_CLIENT_STATE_CAMERA_SHOT,
	FMMC_ELEVATOR_CLIENT_STATE_CAMERA_FINISHED
ENDENUM
#IF IS_DEBUG_BUILD
FUNC STRING DEBUG_GET_FMMC_ELEVATOR_CLIENT_STATE_STRING(FMMC_ELEVATOR_CLIENT_STATE eState)
	SWITCH eState
		CASE FMMC_ELEVATOR_CLIENT_STATE_IDLE 			RETURN "FMMC_ELEVATOR_CLIENT_STATE_IDLE"
		CASE FMMC_ELEVATOR_CLIENT_STATE_IN_POSITION 			RETURN "FMMC_ELEVATOR_CLIENT_STATE_IN_POSITION"
		CASE FMMC_ELEVATOR_CLIENT_STATE_WARPED 			RETURN "FMMC_ELEVATOR_CLIENT_STATE_WARPED"
		CASE FMMC_ELEVATOR_CLIENT_STATE_CAMERA_SHOT 		RETURN "FMMC_ELEVATOR_CLIENT_STATE_CAMERA_SHOT"
		CASE FMMC_ELEVATOR_CLIENT_STATE_CAMERA_FINISHED RETURN "FMMC_ELEVATOR_CLIENT_STATE_CAMERA_FINISHED"
	ENDSWITCH
	RETURN "INVALID FMMC_ELEVATOR_CLIENT_STATE"
ENDFUNC
#ENDIF

STRUCT MC_PlayerBroadcastData
 
	// Clients start here
	INT iClientLogicStage = CLIENT_MISSION_STAGE_SETUP
	INT iteam

	INT iGameState

	INT iCurrentLoc =-1
	INT iInAnyLoc = -1
	INT iLeaveLoc = -1
	INT iPedNear = -1
	INT iVehNear = -1
	INT iObjNear = -1
	INT iPartNear = -1
	
	INT iPedCrowdControlBits[FMMC_MAX_PEDS_BITSET]
	// iThreateningCrowdMember[] - This is now being used as a bit field to store each crowd members threat type
	// Threat type is represented by 4 bits (as it has 10 possible states) (this can go upto 16 states before we need more bits)
	// Per crowd ped (32 * 2 )/4 = 16 so we can get max 16 peds in here)
	// author - Mike Wadelin
	INT iThreateningCrowdMember[FMMC_MAX_PEDS_BITSET]
	
	INT iNearDialogueTrigger = -1
	
	INT iLocPhoto =-1
	INT iPedPhoto = -1
	INT iVehPhoto = -1
	INT iObjPhoto = -1
	
	INT iPedCharming = -1
	INT iPedCharmed  = -1
	
	INT iVehRequest = -1 
	
	INT iObjHacked = -1
	INT iObjHacking = -1
	INT iHackFailBitset
	INT iBeginHackFailBitset
	INT iSafeSyncSceneID[FMMC_MAX_NUM_OBJECTS]
	INT iRequestHackAnimBitset
	
	INT iPedCarryCount
	INT iVehCarryCount
	INT iObjCarryCount
	
	INT iVehDelCount
	
	INT iPlayerScore
	INT iKillScore
	INT iRowanCSuperHighScore
	
	SCRIPT_TIMER tdMissionTime
	INT iMissionEndTime
	
	SCRIPT_TIMER tdBoundstimer
	INT iBoundsBitSet
	SCRIPT_TIMER tdBounds2timer
	INT iBounds2BitSet
	SCRIPT_TIMER tdMapBoundsTimer
	
	INT iReasonForPlayerFail
	
	INT iVehDeliveryId = -1

	INT iNumPlayerKills
	INT iNumHeadshots
	INT iStartHits
	INT iStartShots
	INT iNumPlayerDeaths
	INT iNumControlKills
	INT iNumPedKills
	INT iCivilianKills
	INT iVehDamage
	float veh_damage_percentage
	float veh_damage_time
	INT iPlayerDamage
	INT iVehDestroyed
	INT iPrisonersKilled
	INT iChoppersDestroyed
	INT iAmbientCopsKilled
	INT iEndAccuracy
	FLOAT fKillDeathRatio
	FLOAT fDamageDealt
	FLOAT fDamageTaken
	INT iCashReward
	INT iRewardXP
	INT iWanted
	INT iFakeWanted
	INT iBet
	INT iHackTime
	INT iNumHacks
	INT iNumWantedLose
	INT iWantedTime
	INT iNumWantedStars
	INT iLatestHits
	INT iLatestShots
	INT iApartmentState
	INT iCriticalWeaponAmmo
	INT iAssists //2627125
	INT iPureKills //Kills without minus points
	
	//INT iCCDefaultStartTimer
	
	INT iObjectiveTypeCompleted =-1
	
	INT iPedFollowingBitset[FMMC_MAX_PEDS_BITSET]
	INT iVehFollowing = -1
	INT iVehDestroyBitset
	
	INT iClientBitSet
	INT iClientBitSet2
	INT iClientBitSet3
	INT iClientBitSet4
	
	INT iTutorialBitset
	
	VECTOR vBackupSpawnCoords
	INT ibackupSpawnBitset
	
	INT iCutsceneFinishedBitset
	//INT iCashDropped // this player's total dropped
	
	int medal_objective_completed_count = 0
	
	FLOAT fContainerOpenRatio = 0.0
	
	INT iSynchSceneID = -1
	NETWORK_INDEX netMiniGameObj
	NETWORK_INDEX netMiniGameObj2
	NETWORK_INDEX netMiniGameObj3
	
	INT iSyncAnimProgress


	ANIM_NEW_STAGE iSyncRoundCutsceneStage
	SKYSWOOP iSyncRoundPostCutsceneStage
	JOB_TEAM_CUT_STAGE iSyncRoundTeamCutsceneStage
	JOB_INTRO_CUT_STAGE iSyncRoundIntroCutsceneStage
	BOOL bSyncCutsceneDone

	#IF IS_DEBUG_BUILD
		BOOL bPressedF					
		BOOL bPressedS
	#ENDIF
	
	INT iOutfit = -1
	INT iStartingOutfit = -1 
	
	INT iHackMGDialogue = -1 // Dialogue that triggers the currently playing snake minigame for this player
	
	INT iRestartInPV = -1
	
	//Trip Skip Data
	TRIP_SKIP_PLAYER_BD TSPlayerData
	TRIP_SKIP_PLAYER_BD FTSPlayerData

	INT iDialoguePlayedBS[ciMAX_DIALOGUE_BIT_SETS_LEGACY]	 //Bitset that stores whether dialogue has been played or not
	
	INT iBinBagSyncScene

	INT iPartIAmSpectating = -1
	
	INT iSyncLockSuccessBitSet = ci_HUMANE_SYNC_LOCK__INACTIVE
	
	INT iCoronaRole = -1

	VECTOR vPositionAhead // Saves a safe position in front of the player (along their GPS route) for things to spawn
	
	INT iMusicState
	
	INT iBeastAlpha = -1
	
	PlayerRespawnBackAtStartState ePlayerRespawnBackAtStartState = PRBSS_PlayingNormally
	
	TinyRacersRoundState ePlayerTinyRacersRoundState = TR_Racing
//	INT iTRLastSpawnPointUsed

	INT iSuddenDeathBS
	SUMO_SUDDEN_DEATH_STAGE_BD eSumoSuddenDeathStage
	
	INT iRagePickupBS
	
	BOOL bIsOverSpeedThreshold
	BOOL bHasPlayerReceivedBattleEvent = FALSE
	BOOL bCelebrationScreenIsActive = FALSE
	BOOL bPlayedFirstDialogue
	
	BOOL bSwapTeamStarted = FALSE
	BOOL bSwapTeamFinished = FALSE
	
	FLOAT fCaptureTime
	
	INT iKillAllEnemiesPennedInUpdateTimer
	INT iKillAllEnemiesPennedInBS
	
	INT iTradingPlacesTimeBarCurrent
	SCRIPT_TIMER iTradingPlacesTimeBarTimer
	
	INT iVehicleWeaponCollectedBS
	INT iVehicleWeaponEffectActiveBS
	INT iVehicleWeaponEffectUsedBS
	INT iQuantityOfBombs
	INT iWeaponDetonatorIndex
	
	INT iQuantityOfBounces
	
	INT iAdditionalPlayerLives
	
	INT iSlipstreamTime
	INT iCheckpointsPassed
	INT iBestLapTime
	INT iFastestLapOfMission
	
	INT iDamageToJuggernaut
	
	SCRIPT_TIMER tdTimeAlivePron
	INT iGeneralUseBitSet

	INT iCurrentPropHit[ci_PLAYER_CLAIM_PROP_MAX]
	INT iPropsClaimedCurrent = 0
	INT iPropsClaimedCounter = 0
	INT iKSPowerupCollectedBS
	INT iKSPowerupActiveBS
	INT iKSPowerupGivenBS
	
	INT iRequestCapturePedBS[FMMC_MAX_PEDS_BITSET]
	INT iAlreadyBeenCapturingPedBS[FMMC_MAX_PEDS_BITSET]
	INT iRequestCaptureVehBS
	INT iAlreadyBeenCapturingVehBS
	
	BOOL bIsFiringMachineGun
	
	INT iManualRespawnBitset
	INT iScoreOffset
	INT iCheckPointForPosition = -1
	INT iPreviousCheckPointForPosition = -1
	INT iMyTurnScore[MAX_PENALTY_PLAYERS]
	INT iCurrentOTZoneBD
	INT iLapOffset = 0
	
	INT iResurrections
	INT iOvertimePoints
	
	INT iGranularObjCapturingBS
	INT iGranularCurrentPoints
	INT iGranularTotalPoints
	
	FLOAT fBombVehicleSpeed
	INT iMinSpeed_ExplodeProgress
	INT iLastVeh = -1
	
	INT iPackagesAtHolding = 0
	INT iNumberOfDeliveries = 0
	INT iPersonalLocatePoints = 0
	
	INT iVehicleCurrentForRestart = -3
	INT iVehicleCurrentSeatForRestart = -3
	
	INT iPlayThisAudioTriggerASAP_CLIENT = -1
	
	INT iHackingFails
	
	FLOAT fFireDamageSoFar[FMMC_MAX_VEHICLES]
	INT iBurningVehFlashBS
	
	INT iCurrentHeistCompletionStat = -1
	INT iSpecificHeistCompletionStatBS[HBCA_BS_MISSION_CONTROLLER_MAX_LONG]
ENDSTRUCT

MC_PlayerBroadcastData MC_playerBD[MAX_NUM_MC_PLAYERS]

CONST_INT ciInteractWith__Max_Spawned_Props			3
CONST_INT ciInteractWith__Max_Persistent_Props		7

ENUM INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE
	INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE__INIT,
	INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE__IDLE,
	INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE__PLANTING,
	INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE__EXITING
ENDENUM

STRUCT MC_PlayerBroadcastData_1
	NETWORK_INDEX niInteractWith_CurrentSpawnedProps[ciInteractWith__Max_Spawned_Props]
	NETWORK_INDEX niInteractWith_PersistentProps[ciInteractWith__Max_Persistent_Props]
	INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE eiInteractWith_PlantExplosivesState
	INT iInteractWith_NumSpawnedProps = 0
	INT iInteractWith_NumPersistentProps = 0
	
	INT iTimeSpentPhoneHacking
	INT iGunCameraKills
	INT iDamageToPedsForMedal
	INT iDamageToVehsForMedal
	INT iOffRuleMinigameScoreForMedal
	
	FLOAT fShowdownPoints = 50.0
	FLOAT fShowdown_PointsStolenTotal = 0.0
	INT iShowdownEliminations = 0
	INT iShowdown_LastDamageType = -1
	
	INT iBmbCurrentRadius		 = 1
	INT iBmbCurrentAmountOfBombs = 1	//Value for looping through max available bombs

	INT iBmbAvailableBombs		 = 1	//Value used for limiting amount placed and UI prints
	
	FLOAT fRunnerBestTime = 0.0
	
	INT iLocBitset
	
	INT iBmbFb_Goals = 0
	INT iLastBombFootballHitTimestamp[FMMC_MAX_NUM_OBJECTS] //A timestamp of the last time this player touched the ball
	
	INT iArenaTrialTotalTimeGained = 0	// Variable tracking amount of time gained from checkpoints in Arena Trials
	
	//ARENA TRAPS
	INT iArenaTraps_PressurePadBS
	SCRIPT_TIMER tdComboTimer
	INT iStuntComboMultiplier=1
	
	INT iApEarned
	ARENA_POINTS sArenaPoints
	ARENA_WARS_SC_LDB_STATS sArenaSCLBStats
	MODEL_NAMES mnCoronaVehicle
	INT iDisplaySlot
	
	INT iTaggedOutCounter = 0
	INT iTaggedInCounter = 0
	INT iCheckpointsCollected = 0
	
	INT iObjectsReturned = 0
	eArenaSpawnFixStage eArenaSpawnStage
	INT iCarnageBarBS
	INT iVehicleOwnerNeededToRepair				= -1
	INT iVehicleNeededToRepair					= -1
	
	INT iExplosionDangerZoneInsideBS = 0
	
	INT iCurrentHoldInteract = -1
	
	INT iKeypadThermiteInRangeBS = 0
	INT iThermitePerPlayerRemaining = -1
	
	INT iCurrentElevator = -1
	INT iCurrentElevatorCall = -1
	FMMC_ELEVATOR_CLIENT_STATE eCurrentElevatorState
	
	MP_HEIST_GEAR_ENUM eHeistGearBagType = HEIST_GEAR_SPORTS_BAG
	MODEL_NAMES mnHeistGearBag = HEI_P_M_BAG_VAR22_ARM_S	
	
ENDSTRUCT

MC_PlayerBroadcastData_1 MC_playerBD_1[MAX_NUM_MC_PLAYERS]

SERVER_EOM_VARS TempFMMC_EOM

//Trip Skip Data
TRIP_SKIP_LOCAL_DATA TSLocalData
TRIP_SKIP_LOCAL_DATA FTSLocalData

INT iLocalQuickMaskBitSet

INT iStoreLastTeam0Score 
INT iStoreLastTeam1Score
BOOL bShownScoreShardInSuddenDeath = FALSE

struct object_struct 
	
	object_index obj
	vector pos 
	vector rot
	float heading
	string scene_handle
	bool been_created
endstruct

object_struct envelope

ENUM FLARE_STATE
	FLARE_STATE_INIT=0,
	FLARE_STATE_NEUTRAL,
	FLARE_STATE_TEAM_0,
	FLARE_STATE_TEAM_1,
	FLARE_STATE_TEAM_2,
	FLARE_STATE_TEAM_3,
	FLARE_STATE_TRANSITION
ENDENUM

STRUCT prop_flare_data
	
	FLARE_STATE iFlareState
	FLARE_STATE iNewState
	FLOAT fNextRed
	FLOAT fNextGreen
	FLOAT fNextBlue
	FLOAT fRed
	FLOAT fGreen
	FLOAT fBlue
	INT   iPTFXval = - 1
	FLOAT fTransitionProgress
	INT   iWinningTeam
	INT		iConnectedLocate = -1

ENDSTRUCT

CONST_INT ciTOTAL_PTFX 			20 // This cannot be greater than 32

prop_flare_data flare_data [ciTOTAL_PTFX] // Previously FMMC_MAX_DYNAMIC_FLARES
prop_flare_data corona_flare_data [ciTOTAL_PTFX] // Previously FMMC_MAX_DYNAMIC_FLARES

INT iMaxCaptureAmount
INT iWinningState = -1

INT iNumCreatedDynamicFlarePTFX

BOOL bShowroomInteriorEntitySetChecked = FALSE

model_names garage_door_model

INTERIOR_INSTANCE_INDEX interiorID = NULL

vehicle_index cutscene_vehicle[total_number_of_cutscene_vehicles]

camera_index camera_a

// KGM_CTF_MARKER: 5/13/12: Control variables required to load, draw, and remove the ground projection
// NOTE: We used to have to delete the checkpoint then wait a frame before unpatching the decal - I'm pretty sure we can do this on the same frame now (we can - confirmed) which removes some complexity
ENUM m_enumStageMarker
	MARKER_REQUEST_TXD,				// The Ground Projection TXD is being loaded
	MARKER_ON_DISPLAY,				// The Ground Projection TXD has loaded, the checkpoint displayed, the decal overwritten - hanging around now until no longer required
	// Leave this at the bottom
	MARKER_NOT_ACTIVE				// The ground projection isn't currently required, loading, or cleaning up
ENDENUM

STRUCT m_structMarkerCTF
	BOOL				mctfKeepActiveThisFrame		= FALSE					// The 'draw_locate_marker' function will set this to TRUE every frame - while TRUE the projection will be loaded and drawn, FALSE will auto-cleanup
	m_enumStageMarker	mctfStage					= MARKER_NOT_ACTIVE		// The current processing stage of the marker
	CHECKPOINT_INDEX	mctfCheckPointIndex			= NULL					// Stores the Index of the created Checkpoint so it can be deleted when no longer needed
	VECTOR				mctfPosition										// The 'draw_locate_marker' function will store this - it shouldn't dynamically change so it will only be used on the frame the projection gets drawn
ENDSTRUCT

m_structMarkerCTF	m_sMarkerCTF

INT iNumCreatedFlarePTFX
INT iCreatedFlaresBitset[(FMMC_MAX_NUM_PROPS / 32) + 1]

INT iCreatedFireworkBitset[(FMMC_MAX_NUM_PROPS / 32) + 1]

OBJECT_INDEX objFlare
PTFX_ID players_underwater_flare
SCRIPT_TIMER stFlarePTFXDelayTimer
CONST_INT ciFlarePTFXDelayTimerLength	2000

PTFX_ID ptfxFlareIndex[ciTOTAL_PTFX]
PTFX_ID ptfxThermite[2]
PTFX_ID ptfxThermiteLocalDrip[ciMAX_LOCAL_THERMITE_PTFX]
PTFX_ID ptfxThermiteLocalSpark[ciMAX_LOCAL_THERMITE_PTFX]

INT FlareSoundID[ciTOTAL_PTFX]
INT FlareSoundIDPropNum[ciTOTAL_PTFX]

PTFX_ID trans_lights_ptfx

PTFX_ID DrillptfxIndex
PTFX_ID OverheatptfxIndex

//Spawn vehicle blips.
VEHICLE_INDEX vehSpawnVehicle
BLIP_INDEX biSpawnVehicle
BOOL bManageSpawnVehicleBlip = FALSE

//cash grab


FLOAT	fCashGrabDropRadius				= 0.175
FLOAT	fCashGrabAngledAreaWidth		= 1.750
VECTOR	vCashGrabDropOffset1			= << -0.180, -0.190, -0.150 >>
VECTOR	vCashGrabDropOffset2			= << -0.130, -0.190, 0.0000 >>
VECTOR	vCashGrabDropOffset3			= << -0.090, -0.170, 0.1600 >>
VECTOR 	vCashGrabCamAttachOffsetStart 	= <<0.6902, 0.0902, 0.895>>			//<<0.6902, 0.0902, 0.6794>>
#IF IS_DEBUG_BUILD
VECTOR 	vCashGrabCamPointOffsetStart 	= <<-2.2007, -0.1576, -0.0830>>
#ENDIF
VECTOR 	vCashGrabCamRotationStart		= <<-14.7232, 0.0033, -118.3009>>
VECTOR	vCashGrabAngledAreaOffset1		= << 0.0, 0.0, -0.450 >>
VECTOR	vCashGrabAngledAreaOffset2		= << 0.0, 1.300, 1.50 >>

VECTOR 	vCashGrabCamAttachOffsetEnd 	= <<0.3845, -0.4765, 0.725>>		//<<0.3845, -0.4765, 0.6170>>
VECTOR 	vCashGrabCamPointOffsetEnd 		= <<-0.5548, 0.750, 0.400>>			//<<-0.5548, 0.7236, 0.6954>>
VECTOR 	vCashGrabCamRotationEnd			= <<2.8980, -0.0000, 52.4134>>


BOOL bIsSmoker = FALSE
NETWORK_INDEX niNetworkObj

TEXT_LABEL_63 tl23WinnerVehicle

//Storing and setting vehicle colour/livery when using manual respawn
STRUCT ManualRespawnLastVehicleDetails
	MODEL_NAMES mnModel				= DUMMY_MODEL_FOR_SCRIPT
	INT iVehicleLivery				= -1
	INT iVehicleColour1				= -1
	INT iVehicleColour2				= -1
	INT iVehicleExtraColour1		= -1
	INT iVehicleExtraColour2		= -1
ENDSTRUCT

ManualRespawnLastVehicleDetails sLastVehicleInformation

INT iLastOccupiedRespawnListVehicle = -1
VEHICLE_SEAT vsLastOccupiedVehicleSeat = VS_DRIVER

BOOL bStopDurationBlockSeatShuffle

INT iVehDamageTrackerBS = 0

// url:bugstar:2208851
INT iValkyrieDesign = -1

// RENDERING HACKING SPRITE TO HACKING PLAYER'S PHONE
OBJECT_INDEX e_DefaultPlayerPhone
OBJECT_INDEX e_FakePlayerPhone

ENUM FAKE_PLAYER_PHONE_STATES
	FPPS_INIT,
	FPPS_UPDATE
ENDENUM
FAKE_PLAYER_PHONE_STATES e_FakePhoneState = FPPS_INIT

STRUCT MISSION_INTRO_COUNTDOWN_UI
	SCALEFORM_INDEX uiCountdown
	INT iBitFlags
	INT iFrameCount
	structTimer CountdownTimer
ENDSTRUCT

MISSION_INTRO_COUNTDOWN_UI cduiIntro

ENUM COUNTDOWN_UI_FLAGS_RACE
	CNTDWN_UI_SoundGo  		= BIT4,
	CNTDWN_UI2_Played_3 	= BIT0,
	CNTDWN_UI2_Played_2 	= BIT1,
	CNTDWN_UI2_Played_1 	= BIT2,
	CNTDWN_UI2_Played_Go	= BIT3
ENDENUM

// Bounds Timer Bleep
//For old timer sound
//INT iStoredBoundsTimerBleep
INT iBoundsExplodeSound = GET_SOUND_ID()
INT iBoundsTimerSound = GET_SOUND_ID()

INT iDroneCutsceneSound = -1

// url:bugstar:2214705
INT iOverridePositionCount

INT iZoneDelays[ FMMC_MAX_NUM_ZONES ]
INT iZonePendingBits
BOOL bWasPedEligibleForBinBagThrow = FALSE
INT iNavBlockerZone[FMMC_MAX_NUM_ZONES]

INT SyncCameraAndSkyswoopDownStages

SCALEFORM_INDEX uiGoStopDisplay
SCRIPT_TIMER tmrGoStopDisplay

INT iZoomDelayGameTime

ENUM BIN_BAG_STAGE
	BBS_DORMANT,
	BBS_INIT,
	BBS_SETUP,
	BBS_ORIENTING	
ENDENUM

STRUCT sSpecialPickupState
	eSpecialPickupState 		ePickupState 			= eSpecialPickupState_BEFORE_PICKUP
	WEAPON_TYPE					wtStoredWeapon 			= WEAPONTYPE_UNARMED
	FLOAT						fGroundHeightForPickup 	= -999999.0
	OBJECT_INDEX				obj
	VECTOR						vPlayerTargetPos
	VECTOR						vPlayerTestPos
	FLOAT 						fPlayerTargetHeading
	SHAPETEST_INDEX				sti
	SHAPETEST_INDEX				stiGroundCheck[ FMMC_MAX_NUM_OBJECTS ]
	SHAPETEST_INDEX				stiPickup
	BOOL						bObstructionExists
	BIN_BAG_STAGE				eBinBagStage[ FMMC_MAX_NUM_OBJECTS ]
	INT							iBinBagFallTimer[ FMMC_MAX_NUM_OBJECTS ]
	INT							iTimer					= -1
	BOOL						bAssetsLoaded			= FALSE
	STRING						sClipSet
	STRING						sAnimDict
	STRING						sWaveBank
	STRING						sAnimName
	INT							iContext				= -1
	INT							iStage					= 0
	INT							iSweetenerEnabled
	INT							iHelpTextBits
	INT							iPosition				= -1
	INT							iCurrentlyOwnedBinbag	= -1
	INT							iCurrentlyRequestedObj = -1
	INT							iSyncScene
	INT							iObjSyncScene
	INT							iTrashDriveEnable		= -1
	BOOL						bRightHandedThrow
	#IF IS_NEXTGEN_BUILD
	OBJECT_INDEX				objFirstPerson
	#ENDIF
ENDSTRUCT

sSpecialPickupState sCurrentSpecialPickupState

#IF IS_DEBUG_BUILD
BOOL bEnableMagnetTuning = FALSE
FLOAT fMagnetRange = 8.0
FLOAT fMagnetMin = 0.3
FLOAT fMagnetMax = 1.3
#ENDIF

//Controller heartbeat logic - "Hrtb"
//Controller friendly vibrate logic "Frnd"

CONST_INT ciMAX_SCARED_HALLOWEEN_PEDS		8

PED_INDEX piHrtbScaryPedRef

CONST_INT ciHRTB_PULSE_INCREASE_RADIUS		25		//METERS
CONST_INT ciHRTB_PULSE_MINIMUM_RADIUS		5		//OVER 20 METERS WILL GO FROM CALMEST TO FASTEST
CONST_INT ciHRTB_SECOND_BEAT_BASE_TIME		300		//MILLISECONDS //100 MINIMUM
CONST_INT ciHRTB_RELOOP_PULSE_BASE_TIME		3000	//MILLISECONDS //1000 MINIMUM
CONST_INT ciHRTB_MAX_PLAYER_CHECK			8


SCRIPT_TIMER stHrtbVibrationTimer
SCRIPT_TIMER stFrndVibrationTimer
SCRIPT_TIMER stFrndVibrationCDTimer[ciMAX_SCARED_HALLOWEEN_PEDS]

INT iHrtbBitSet

CONST_INT ciHRTB_MODE_NOT_ACTIVE			0
CONST_INT ciHRTB_DONE_SECOND_HEARTBEAT		1
CONST_INT ciHRTB_HAS_HANDLE_TO_SCARY_PED	2
CONST_INT ciHRTB_LOCAL_PED_IS_SCARY_PED		3
CONST_INT ciHRTB_IS_IN_SIGHT_OF_SCARY_PED	4
CONST_INT ciHRTB_HIT_NOTHING				5

INT iFrndVibCacheBitSet

FLOAT fHrtbDistanceFromScaryPed

INT iHrtbDistanceUpdateTimer = 10

INT iHunterBreathingSoundID
TEXT_LABEL_15 tlBreathingTrack


//Stop players camping variables
VECTOR vLastCachedPosition
CONST_INT ciIDLE_BLIP_THRESHOLD		4500 //Time to test against to confirm a player is camping
CONST_INT ciIDLE_BLIP_CAMP_RADIUS	2 //meters

SCRIPT_TIMER tIdleTimer
INT iIdleBlipExitRetry

INT iPlayerThermalRefreshBitset

VECTOR vDZPlayerSpawnLoc
FLOAT fDZPlayerVehHeading
INT iDZVehicleIndex

INT iDZCargobobRampSoundID = -1

//[KH] START Sudden death sumo penned in variables 
VECTOR vSphereSpawnPoint
FLOAT fSphereStartRadius
FLOAT fSphereEndRadius
FLOAT fCurrentSphereRadius
FLOAT fCurrentSphereRadius_VisualOffset = 0.0
SCRIPT_TIMER stSumoSDBubbleUpdateTimer
SCRIPT_TIMER stSumoSDCountdownUpdateTimer
CONST_INT ciSUMO_SD_BUBBLE_UPDATE_INTERVAL			1000
CONST_FLOAT ciSUMO_SD_SIZE_OFFSET_TOPDOWN	-7.5

INT fSphereShrinkTime
INT fTimeToGetInsideSphere

INT iOutAreaSound = GET_SOUND_ID()
INT iDoorAlarmSoundID = -1

CONST_INT ciPBDBOOL_SSDS_SUDDEN_DEATH_STARTED		0
CONST_INT ciPBDBOOL_SSDS_IS_INSIDE_SPHERE			1
CONST_INT ciPBDBOOL_SSDS_COUNTDOWN_TIMER_FINISHED	2
CONST_INT ciPBDBOOL_SSDS_COUNTDOWN_SOUND_PLAYING	3
CONST_INT ciPBDBOOL_SSDS_HAS_COUNTDOWN_FINISHED		4

SCRIPT_TIMER stTimeToGetInSphere

//[KH] END

SCRIPT_TIMER biBlipOnSpeedTimer[NUM_NETWORK_PLAYERS]
INT iBlippingPlayerBS

//Sets the time that the player can collect the pickup again after just dropping it
SCRIPT_TIMER stPickupTimeoutTimer
INT iPickupTimeoutTime = 3000

SCRIPT_TIMER stIconRemovalDelayTimer
INT iIconRemovalTime = 1500

#IF IS_DEBUG_BUILD
BOOL bBlockingObjectLastFrame = FALSE
#ENDIF

INT iNumObjsHeldLastFrame = 0

CONST_INT ciSTART_CUSTOM_LEVEL_SHARDS_WITH_STRAPLINES	4
CONST_INT ciEND_CUSTOM_LEVEL_SHARDS_WITH_STRAPLINES		12
CONST_INT ciSTART_CUSTOM_TLAD_SHARD_WITH_STRAPLINE		20
CONST_INT ciEND_CUSTOM_TLAD_SHARD_WITH_STRAPLINE		29
CONST_INT ciEND_CUSTOM_LEVEL_SHARD_TEXT					21
CONST_INT ci10_CUSTOM_LEVEL_SHARD_TEXT					26
CONST_INT ciVEHICLE_SWAP_SHARD_TEXT						27

INT iPartTeammateWasKilledBy = -2

///////////////////////////////////////////////////////////
//===============================Bomber Mission Logic START
//===============================Author: Jake Thorne
///////////////////////////////////////////////////////////

INT iBmbArenaZoneIndex 	= -1 				//Index of the BomberArena zone placed
FLOAT fBmbArenaAngle						//Stored angle at which to calculate explosion lines


STRUCT BmbObject

	NETWORK_INDEX 			bombObject
	SCRIPT_TIMER 			bombTimer
	SHAPETEST_INDEX 		explosionRay[4]			//0-up, 1-right, 2-down, 3-left
	
	VECTOR 					vExplosionPoint
	INT 					iCurrentExplosion
	FLOAT 					fBeepTimer
	
	INT						iRayBitSet
	INT						iHitSomething
	VECTOR					vHitPos[4]
	VECTOR					vHitNormal
	ENTITY_INDEX			hitEntity[4]
	INT						hitEntityCreatorIndex[4]
	BMB_POWERUP_TYPES		hitPowerupType[4]
	PTFX_ID					ptfx_BombFuse

ENDSTRUCT

STRUCT BmbNetObject
	NETWORK_INDEX 	objectID
	BLIP_INDEX		objectBlip
	BLIP_INDEX		radiusBlip
	
	SCRIPT_TIMER	bombTimer
	FLOAT			fBeepTimer
	INT 			iBombRadius

ENDSTRUCT

CONST_INT ciBOMB_DELAY_TIME 1500
SCRIPT_TIMER tdMultiBombDelay

BmbObject 		oBmbBombs[ciBMB_MAX_TOTAL_BOMBS_PER_PLAYER]
BmbNetObject	oBmbNetBombs[ciBMB_MAX_MAX_ACTIVE_BOMBS]

INT				iBmbPowerupTypeToLoad[ciBMB_MAX_POWERUPS]
PICKUP_INDEX	pBmbPowerups[ciBMB_MAX_POWERUPS]
INT				iBmbPowerupsLoadingBitSet
VECTOR			vBmbPowerupPositions[ciBMB_MAX_POWERUPS]

INT iVVBombTimerSoundID[ciBMB_MAX_TOTAL_BOMBS_PER_PLAYER]

INT iActiveBombBlipsBS
INT iShouldCleanupBombBS	
CONST_INT ciDROPTHEBOMB_Max_Bomb_Length		10

#IF IS_DEBUG_BUILD
	SCRIPT_TIMER 	persistantLine[32]
	INT				pLineDuration[32]
	VECTOR			pLineStart[32]
	BOOL			pLineFailed[32]
	VECTOR			pLineEnd[32]
#ENDIF
INT iBmbPlacementFlags = 0

INT iBmbPlacedBitSet

//iBmbPlacedBitSet
CONST_INT BMB_SLOT_1_HIT_TESTED				0
CONST_INT BMB_SLOT_2_HIT_TESTED				1	
CONST_INT BMB_SLOT_3_HIT_TESTED				2	
CONST_INT BMB_SLOT_4_HIT_TESTED				3	
CONST_INT BMB_SLOT_5_HIT_TESTED				4	
CONST_INT BMB_SLOT_6_HIT_TESTED				5	
CONST_INT BMB_SLOT_7_HIT_TESTED				6	
CONST_INT BMB_SLOT_8_HIT_TESTED				7	

CONST_INT BMB_SLOT_1						8
CONST_INT BMB_SLOT_2						9
CONST_INT BMB_SLOT_3						10
CONST_INT BMB_SLOT_4						11
CONST_INT BMB_SLOT_5						12
CONST_INT BMB_SLOT_6						13
CONST_INT BMB_SLOT_7						14
CONST_INT BMB_SLOT_8						15

CONST_INT BMB_SLOT_1_ARMING 				16	
CONST_INT BMB_SLOT_2_ARMING					17	
CONST_INT BMB_SLOT_3_ARMING					18	
CONST_INT BMB_SLOT_4_ARMING					19	
CONST_INT BMB_SLOT_5_ARMING					20	
CONST_INT BMB_SLOT_6_ARMING					21	
CONST_INT BMB_SLOT_7_ARMING					22	
CONST_INT BMB_SLOT_8_ARMING					23

INT iBmbPlayerBitSet

CONST_INT BMB_PLAYER_HAS_ARMOUR				0
CONST_INT BMB_STARTED_ARMOUR_REMOVE_TIMER	1
CONST_INT BMB_STARTED_DEATH_BOMB_FADE		2
CONST_INT BMB_STARTED_DEATH_BOMB_DELETE		3
CONST_INT BMB_CLEARED_NET_OBJECTS			4

INT iBmbArmourRemoveTimer

//iRayBitSet
CONST_INT BMB_HIT_TESTED_DIR_U				0
CONST_INT BMB_HIT_TESTED_DIR_R				1
CONST_INT BMB_HIT_TESTED_DIR_D				2
CONST_INT BMB_HIT_TESTED_DIR_L				3

CONST_INT BMB_DO_EXPLODE_PROP_U				4
CONST_INT BMB_DO_EXPLODE_PROP_R				5
CONST_INT BMB_DO_EXPLODE_PROP_D				6
CONST_INT BMB_DO_EXPLODE_PROP_L				7

CONST_INT BMB_HIT_POWERUP_PROP_U			8
CONST_INT BMB_HIT_POWERUP_PROP_R			9
CONST_INT BMB_HIT_POWERUP_PROP_D			10
CONST_INT BMB_HIT_POWERUP_PROP_L			11

CONST_INT BMB_HAS_HIT_FLOOR					12
CONST_INT BMB_SET_IGNORE_COLLISION			13


///////////////////////////////////////////////////////////
//===============================Bomber Mission Logic END
///////////////////////////////////////////////////////////

//Vehicle Menu
SCRIPT_TIMER VehicleDelayTimer

//Vehicle Rockets Ammo
INT iVehicleRockets

//Vehicle Boost
INT iVehicleBoost

//Vehicle Repair
INT iVehicleRepair

//Vehicle Spikes
INT iVehicleSpikes

//Equip Last Weapon after Parachute
WEAPON_TYPE wtLastWeaponParachute
BOOL bParachuteInProgress

//======HYDRAULIC SEQUENCING VARIABLES START

INT iPerformHydraulicsAsPriorityBitSet[FMMC_MAX_PEDS_BITSET]

INT iHydraulicDanceTimer
INT iHydraulicDanceDowntimeTimer
INT iHydraulicDanceCurrentLoop = 1
INT iHydraulicDanceCurrentRandomEvent

INT iHydraulicDanceBitSet
CONST_INT HDSA_ACTION_TRIGGERED_1		0
CONST_INT HDSA_ACTION_TRIGGERED_2		1
CONST_INT HDSA_ACTION_TRIGGERED_3		2
CONST_INT HDSA_ACTION_TRIGGERED_4		3
CONST_INT HDSA_ACTION_TRIGGERED_5		4
CONST_INT HDSA_ACTION_TRIGGERED_6		5
CONST_INT HDSA_ACTION_TRIGGERED_7		6
CONST_INT HDSA_ACTION_TRIGGERED_8		7
CONST_INT HDSA_ACTION_TRIGGERED_9		8

CONST_INT HDSA_ACTION_DOWNTIME			9
INT iHydraulicDowntimerVariance

//HYDRAULIC DANCE CONFIG

// +1 TO THE AMOUNT
CONST_INT HDCFG_MAX_RANDOM_EVENTS		8
CONST_INT HDCFG_REENTER_EVENT_CHANCE	250
CONST_INT HDCFG_ENTER_EVENT_CHANCE		450

//For storing out the mission results
INT iLBPositionStore
INT iTeamLBPositionStore

//======HYDRAULIC SEQUENCING VARIABLES END

//======TOUR DE FORCE VARIABLES START

SCRIPT_TIMER 		tdTourDeForceBoost

//======TOUR DE FORCE VARIABLES END

/////////////////////////////////////////////////
///    PRON							//////////
///    Lorcan Henry				//////////
////////////////////////////////////

CONST_INT MAX_SEGMENTS			150
CONST_INT MAX_SEGMENTS_PER_BOX	10
CONST_INT NUM_COLLISION_BOXES 	MAX_SEGMENTS/MAX_SEGMENTS_PER_BOX
CONST_INT STARTUP_TIME			5000
CONST_FLOAT MAX_LEAN_ANGLE		38.0

CONST_FLOAT MAX_SEGMENT_LENGTH	1.25

INT iSegmentsLimit		= MAX_SEGMENTS-1
INT iNumberOfPronTeams 	= 0

STRUCT LIGHTCYCLE_SEGMENT
	
	VECTOR vPointTop
	VECTOR vPointBottom
	
ENDSTRUCT

STRUCT LINE_AABB

	VECTOR vLowest
	VECTOR vHighest
	
	INT iStartSegment
	INT iEndSegment
	
ENDSTRUCT

STRUCT LIGHTCYCLE_PLAYER

	VEHICLE_INDEX LightBike
	PLAYER_INDEX PlayerID

	LIGHTCYCLE_SEGMENT LGHTCYCLE_SEGMENT[MAX_SEGMENTS]
	
	VECTOR vCurrentPos
	VECTOR vLastPoint
	VECTOR vLiveTrailOffset
	
	VECTOR vEndTop
	VECTOR vEndBottom
		
	INT iCurrentNumberOfSegments = 2
	
	FLOAT fDistanceFromLastPoint
	
	BOOL bIsInitialised
	BOOL bIsStartingUp = TRUE
	
	INT iFrontOfLine = 1
		
	INT iCurrentCollisionBox
	INT iSegmentCounter = 1
	BOOL bLoopActive
	LINE_AABB CollisionBox[NUM_COLLISION_BOXES]
	LINE_AABB EndCollisionBox
	
	INT iLastClosest = -1
	FLOAT fCachedDotProductBike
	FLOAT fCachedDotProductPed
	
	//VFX
	PTFX_ID ptfxPronTrail
	
	INT iAlpha = 50
	INT iAlphaLoopCounter = 1

ENDSTRUCT

LIGHTCYCLE_PLAYER 	LGHTCYCLE_PLAYER[4]
SCRIPT_TIMER 		tdBikeStartupTimer
SCRIPT_TIMER		tdBikeLandedTimer
SCRIPT_TIMER 		tdPronStationaryTimer
FLOAT 				fPronExplodeProgress

FLOAT				fWheelRadius = 0.342
FLOAT				fWheelOffset = fWheelRadius
FLOAT				fLineDistanceOffset = -0.972
FLOAT				fLineHeight = 0.623
FLOAT				Sy, Sz

VECTOR				vNormalVelocity
VECTOR				vLastFramePos

FLOAT fLOCKVEHSPEED = 16
CONST_INT			ciVEHICLE_ACCELERATE_DELAY			275

INT iLocalPronBitSet
CONST_INT ciLOCAL_PLAYER_END_OF_MODE_CLEANUP			1
CONST_INT ciDRAW_DEBUG_POLYS							2
CONST_INT ciDRAW_POLYS									3
CONST_INT ciUSE_COLLISION								4
CONST_INT ciBLOW_UP_IF_STATIONARY						5
CONST_INT ciNEEDS_TO_START_STARTUP						6
CONST_INT ciLOCAL_PLAYER_IS_INVULNERABLE				7
CONST_INT ciWARNING_SCREEN_IS_ACTIVE					8
CONST_INT ciALREADY_COLLIDED_THIS_PLAYER				9

INT A_Segment1 = 75
INT A_Segment2 = 200

INT A_SegmentB = 175

FLOAT fBloomScale = 0

FLOAT BoostPower = 10
SCRIPT_TIMER stBoostInAirTimer
BOOL bUseHighBounce

FLOAT fMagnitudeLimit = 6
INT iMagnitudeTimer = 300

INT iWarningMessageCount

INT iBroadcastOnTime
INT iBroadcastOnTimeFalse
INT iActivePronTeams

INT 	iStartupVibration = 255
INT 	iMain_A = 255

FLOAT	fLineTop = 0.005
OBJECT_INDEX oiTrailBottom

INT iTrailSound0 = -1 
INT iTrailSound1 = -1
INT iTrailSound2 = -1
INT iTrailSound3 = -1

INT iPronAudioBS 

INT iTrailR
INT iTrailG
INT iTrailB
INT iBike1
INT iBike2
INT iBike4

//COLOURS
#IF IS_DEBUG_BUILD
INT		iMain_R = 253
INT 	iMain_G = 132
INT 	iMain_B = 10
#ENDIF

/////////////////////////////
///    END PRON	  ///////
/////////////////////


/////////////////////////////////////////////////
///    OLD SCHOOL GTA				//////////
///    Lorcan Henry				//////////
////////////////////////////////////

BOOL bPlayerToggle
BOOL bUsingBirdsEyeCam
BOOL bUseBirdsEyeCamOnRespawn
BOOL bLockNorth
BOOL bSnapCam

VECTOR BirdsEyeTarget
FLOAT BirdsEyeTargetRot

VECTOR BirdsEyeCurrent
FLOAT BirdsEyeCurrentRot

VECTOR BirdsEyeCurrentRotVector

FLOAT fBEBaseHeight = 10
FLOAT fBEMinHeight = 25
FLOAT fSpeedMultiplier = 0.75

//FLOAT fLookAtDistanceMultiplier = 0.5
//FLOAT fPosInterpRate = 10.0
//FLOAT fLookAtInterpRate = 0.075
//FLOAT SphereSize

FLOAT fDistanceMultiplier = 0.5
FLOAT fBasePosOffset

FLOAT fDirectionalOffsetMultiplier = 10.0

FLOAT fBirdsEyeAngle = -89.9

BOOL bUseFixedCameraOffset

////////////////////
//Tiny Racers Camera
////////////////////
//Height

FLOAT fMinHeightTR = 35.150
FLOAT fBEBaseHeightTR = 95.0
FLOAT fSpeedMultiplierTR = 0

//Offset
FLOAT fDistanceMultiplierTR = 0.0
BOOL bTRAllowOffsetChange
FLOAT fBasePosOffsetTR = -1
CONST_FLOAT cf_BasePosOffsetTR -1.0 //Update this with the above, this is for the look back cam resetting
CONST_FLOAT cf_LookBackCamPosition -33.0
FLOAT fDirectionalOffsetMultiplierTR = 0
//Angle
FLOAT fBirdsEyeAngleTR = -80

FLOAT fTRMotionBlurStrength = 0.225
FLOAT fTRDOF = 1
FLOAT fBirdsEyeTRFOV = 35
//Tilt shift
FLOAT fTRFocalLength = 10.0
FLOAT fTRNumLens =  2.8
FLOAT fTRBlendLevelNearIn = 1.0

//FLOAT fTRPointAtX = 0.25
//FLOAT fTRPointAtY = 17.0
//FLOAT fTRPointAtZ = 0
//BOOL bTRRelativeCam = TRUE


//IntroCam
FLOAT fPostFxPhase = 0.85
FLOAT fBlendCamPhase = 0.91
FLOAT fMotionBlurStrength = 1.0
INT	iTRBoneIndex = -1
INT iTRInterpTime = 1700
BOOL bDeactivate
FLOAT fBirdsEyeInterpSpeed = 0.075
FLOAT fBirdsEyeRotInterpSpeed = 0.3 //Also used by normal camera settings
FLOAT BirdsEyeTWFOV = 45
FLOAT BirdsEyeFOV = 65

CAMERA_INDEX camScriptedRaceCam
VECTOR vRaceCamPos
VECTOR vLastRaceCam
VECTOR vNextRaceCam
//VECTOR vLastCheckpoint
FLOAT fPercentToNextPoint



//////////////////////////////////////
///    END OLD SCHOOL GTA	  ///////
////////////////////////////////////
///    

//Rugby
WEAPON_TYPE wtRugbyRespawnWeapon = WEAPONTYPE_INVALID
PLAYER_INDEX piLastDeliverer = INVALID_PLAYER_INDEX()

SCRIPT_TIMER tdRugby_TryShardTimer

SCRIPT_TIMER tdAttemptFailedTimer

WEAPON_TYPE wtCachedLastWeaponHeld
BOOL bHasObjectJustBeenDropped

BOOL bAppliedVehicleDrag = FALSE

PLAYER_INDEX piPlayerCarryingBall = INVALID_PLAYER_INDEX()
VECTOR vBallCollectionPoint
VECTOR vBallDropPoint
INT iObjBeingCarried = -1

FLOAT fDistFromScoringTry

CONST_INT 	ciTIMESTAMP_FOR_TEAM_SWAP		2000

FLOAT fTempCaptureTime = 0

SCRIPT_TIMER tdTeamSwapPassiveModeTimer
SCRIPT_TIMER tdTeamSwapFailSafe

NETWORK_INDEX netVehicleToCleanUp
SCRIPT_TIMER vehicleCleanUpTimer
CONST_INT ciCLEAN_UP_VEHICLE_TIME		5000

//Kill all enemies penned in sudden death
SCRIPT_TIMER timeToBeInsideSphere
SCRIPT_TIMER timeToBeInsideSphereUpdateTimer
INT iTimeToBeInsideSphere
VECTOR vSphereSpawnPosition
SCRIPT_TIMER suddenDeathOutOfAreaTimer
CONST_INT ciOUT_OF_AREA_TIME		5000
CONST_INT ciKAE_PENNED_IN_INSIDE_SPHERE		0
CONST_INT ciKAE_ELIMINATION_TIMER_FINISHED	1
BLIP_INDEX SuddenDeathAreaBlip
BLIP_INDEX SuddenDeathAreaRadiusBlip

INT iPartLastDamager = -1
INT iPartToShootPlayerLast = -1

SCRIPT_TIMER tdLastDamagerTimer
PED_INDEX piPlayerPedLastDamager

//Objective Text Delay Timer & Bitset
SCRIPT_TIMER objectiveTextDelay
INT iObjectiveTextDelayBitset

CONST_INT ciObjectiveText_Primary 			0
CONST_INT ciObjectiveText_Secondary			1
CONST_INT ciObjectiveText_AltSecondary		2

CONST_INT ciObjectiveTextDelayDawnRaidBit	0

CONST_INT ciObjectiveTextDelayDuration	2000

CONST_INT ciSumoMessageDelay 1500
CONST_INT ciSumoTeamsLeftTimerLength 500
SCRIPT_TIMER tdSumoTeamsLeftTimer

//Lost vs Damned
INT iRuleHour
INT iRuleMinute
INT iTargetHour = -1
INT iPreviousTargetHour = -1
CONST_INT iSuddenDeathHour	 19 //DO NOT put this above 23. 0-23 only
PTFX_ID TLADptfxLensDirt

INT iTLADCurrentPlayer
PTFX_ID TLAD_DevilLight[MAX_NUM_MC_PLAYERS]
PTFX_ID TLAD_AngelLight[MAX_NUM_MC_PLAYERS]
PTFX_ID TLAD_AngelWeapon
PTFX_ID TLAD_DevilWeapon
PTFX_ID	TLADAngelGlow[MAX_NUM_MC_PLAYERS]

CONST_INT iNightAnimFXDura 1500
CONST_INT iDayAnimFXDura 1500
CONST_INT iWepFXStartTime 2500
SCRIPT_TIMER stWeaponPFXtimer
SCRIPT_TIMER stWeaponPFXActivation

WEAPON_TYPE wtLastWeapon[MAX_NUM_MC_PLAYERS]

//Pointless
SCRIPT_TIMER stPickupTimer[FMMC_MAX_NUM_OBJECTS]
CONST_INT iPickupTimerLength 5000
INT iPTLTeamScore[FMMC_MAX_TEAMS]
INT iPTLTeamMateScore[MAX_NUM_MC_PLAYERS]
//Arrow bitset
CONST_INT ciARROW_BS_0		0
CONST_INT ciARROW_BS_1		1
CONST_INT ciARROW_BS_2		2
CONST_INT ciARROW_BS_3		3
CONST_INT ciARROW_BS_4		4
CONST_INT ciARROW_BS_5		5
CONST_INT ciARROW_BS_6		6
CONST_INT ciARROW_BS_7		7
CONST_INT ciARROW_BS_8		8
CONST_INT ciARROW_BS_9		9
CONST_INT ciARROW_BS_10		10
CONST_INT ciARROW_BS_11		11
CONST_INT ciARROW_BS_12		12
CONST_INT ciARROW_BS_13		13
CONST_INT ciARROW_BS_14		14
CONST_INT ciARROW_BS_15		15
CONST_INT ciDROP_SOUND_0	16
CONST_INT ciDROP_SOUND_1	17
CONST_INT ciDROP_SOUND_2	18
CONST_INT ciDROP_SOUND_3	19
CONST_INT ciDROP_SOUND_4	20
CONST_INT ciDROP_SOUND_5	21
CONST_INT ciDROP_SOUND_6	22
CONST_INT ciDROP_SOUND_7	23
CONST_INT ciDROP_SOUND_8	24
CONST_INT ciDROP_SOUND_9	25
CONST_INT ciDROP_SOUND_10	26
CONST_INT ciDROP_SOUND_11	27
CONST_INT ciDROP_SOUND_12	28
CONST_INT ciDROP_SOUND_13	29
CONST_INT ciDROP_SOUND_14	30
CONST_INT ciDROP_SOUND_15	31

INT ilocalPLBit

CONST_INT ciPTLCOL_BS_0		0
CONST_INT ciPTLCOL_BS_1		1
CONST_INT ciPTLCOL_BS_2		2
CONST_INT ciPTLCOL_BS_3		3
CONST_INT ciPTLCOL_BS_4		4
CONST_INT ciPTLCOL_BS_5		5
CONST_INT ciPTLCOL_BS_6		6
CONST_INT ciPTLCOL_BS_7		7
CONST_INT ciPTLCOL_BS_8		8
CONST_INT ciPTLCOL_BS_9		9
CONST_INT ciPTLCOL_BS_10	10
CONST_INT ciPTLCOL_BS_11	11
CONST_INT ciPTLCOL_BS_12	12
CONST_INT ciPTLCOL_BS_13	13
CONST_INT ciPTLCOL_BS_14	14
CONST_INT ciPTLCOL_BS_15	15
CONST_INT ciARROW_OFF_16	16
CONST_INT ciARROW_OFF_17	17
CONST_INT ciARROW_OFF_18	18
CONST_INT ciARROW_OFF_19	19
CONST_INT ciARROW_OFF_20	20
CONST_INT ciARROW_OFF_21	21
CONST_INT ciARROW_OFF_22	22
CONST_INT ciARROW_OFF_23	23
CONST_INT ciARROW_OFF_24	24
CONST_INT ciARROW_OFF_25	25
CONST_INT ciARROW_OFF_26	26
CONST_INT ciARROW_OFF_27	27
CONST_INT ciARROW_OFF_28	28
CONST_INT ciARROW_OFF_29	29
CONST_INT ciARROW_OFF_30	30
CONST_INT ciARROW_OFF_31	31
INT iPTLLocalCollisionBit

CONST_INT ciPTLAudio_COLLECT	0
CONST_INT ciPTLAudio_DROP		1
CONST_INT ciPTLAudio_SCORE_UP	2
CONST_INT ciPTLAudio_SCORE_DOWN	3
CONST_INT ciPTLAudio_COUNTDOWN	4
INT iPTLCountdownSoundID = -1

//Tiny Racers
SCRIPT_TIMER td_TR_WarpWait
CONST_INT ci_iWarpWaitTimer	1500

//SVM Ruiner
SCRIPT_TIMER stCargobob
VEHICLE_INDEX viCargobob, viAttachedVeh
BOOL bDone, bDone2, bDone3
CONST_INT ciCARGOBOB_INIT				0
CONST_INT ciCARGOBOB_HELI_SETUP			1
CONST_INT ciCARGOBOB_VEH_SET_UP			2
CONST_INT ciCARGOBOB_MAGNET_ACTIVATE	3
CONST_INT ciCARGOBOB_MAGNET_DEACTIVATE	4
CONST_INT ciCARGOBOB_RESET				5
INT iCargobobTimerLength, iCargobobAltitude
INT iCargobobCatchState = 0

TEXT_LABEL_23 sTLToSkip


CONST_INT DUNE_MAX_FLIPS 		4
SCRIPT_TIMER stFlipTimer[DUNE_MAX_FLIPS]
VEHICLE_INDEX viFlippedVeh[DUNE_MAX_FLIPS]

CONST_INT ciVeh_up_0		0
CONST_INT ciVeh_up_1		1
CONST_INT ciVeh_up_2		2
CONST_INT ciVeh_up_3		3
CONST_INT ciVeh_up_4		4
INT iLocalVehicleExplosionBitSet

CONST_INT ciOBJECT_LIGHT_UPDATED_0		0
CONST_INT ciOBJECT_LIGHT_UPDATED_1		1
CONST_INT ciOBJECT_LIGHT_UPDATED_2		2
CONST_INT ciOBJECT_LIGHT_UPDATED_3		3
CONST_INT ciOBJECT_LIGHT_UPDATED_4		4
CONST_INT ciOBJECT_LIGHT_UPDATED_5		5
CONST_INT ciOBJECT_LIGHT_UPDATED_6		6
CONST_INT ciOBJECT_LIGHT_UPDATED_7		7
CONST_INT ciOBJECT_LIGHT_UPDATED_8		8
CONST_INT ciOBJECT_LIGHT_UPDATED_9		9
CONST_INT ciOBJECT_LIGHT_UPDATED_10		10
CONST_INT ciOBJECT_LIGHT_UPDATED_11		11
CONST_INT ciOBJECT_LIGHT_UPDATED_12		12
CONST_INT ciOBJECT_LIGHT_UPDATED_13		13
CONST_INT ciOBJECT_LIGHT_UPDATED_14		14
CONST_INT ciOBJECT_LIGHT_UPDATED_15		15
CONST_INT ciOBJECT_LIGHT_UPDATED_16		16
CONST_INT ciOBJECT_LIGHT_UPDATED_17		17
CONST_INT ciOBJECT_LIGHT_UPDATED_18		18
CONST_INT ciOBJECT_LIGHT_UPDATED_19		19

INT iLocalObjectLightUpdated

CONST_INT ciOBJECT_LIGHT_READY_0		0
CONST_INT ciOBJECT_LIGHT_READY_1		1
CONST_INT ciOBJECT_LIGHT_READY_2		2
CONST_INT ciOBJECT_LIGHT_READY_3		3
CONST_INT ciOBJECT_LIGHT_READY_4		4
CONST_INT ciOBJECT_LIGHT_READY_5		5
CONST_INT ciOBJECT_LIGHT_READY_6		6
CONST_INT ciOBJECT_LIGHT_READY_7		7
CONST_INT ciOBJECT_LIGHT_READY_8		8
CONST_INT ciOBJECT_LIGHT_READY_9		9
CONST_INT ciOBJECT_LIGHT_READY_10		10
CONST_INT ciOBJECT_LIGHT_READY_11		11
CONST_INT ciOBJECT_LIGHT_READY_12		12
CONST_INT ciOBJECT_LIGHT_READY_13		13
CONST_INT ciOBJECT_LIGHT_READY_14		14
CONST_INT ciOBJECT_LIGHT_READY_15		15
CONST_INT ciOBJECT_LIGHT_READY_16		16
CONST_INT ciOBJECT_LIGHT_READY_17		17
CONST_INT ciOBJECT_LIGHT_READY_18		18
CONST_INT ciOBJECT_LIGHT_READY_19		19
INT iLocalObjectLightReadyForUpdate


SCRIPT_TIMER timerForAllowingRespawn

ENUM eManualRespawnState
	eManualRespawnState_OKAY_TO_SPAWN = 0,
	eManualRespawnState_FADING_OUT,
	eManualRespawnState_RESPAWNING,
	eManualRespawnState_FADING_IN,
	eManualRespawnState_WAITING_AFTER_RESPAWN
ENDENUM

CONST_INT iTWNeutralColour 6 //Grey
CONST_INT iTWContestedColour 1 //Red

eManualRespawnState manualRespawnState = eManualRespawnState_OKAY_TO_SPAWN
PLAYER_SPAWN_LOCATION eManualRespawnState_SpawnLocation

BOOL bPowerUpActive
SCRIPT_TIMER tdBeastModeBackupTimer

CONST_INT ciMAX_PLAYER_BARS			6

CONST_INT DEFAULT_SPLASH_SHARD_TIME 3 //in whole seconds

VEHICLE_INDEX viOldPlayerVeh
VEHICLE_INDEX viPowerupTank
ENUM eVehicleBeastTransformState
	eVehicleBeastTransformState_INITIALISE,
	eVehicleBeastTransformState_CREATE,
	eVehicleBeastTransformState_CLEANUP,
	eVehicleBeastTransformState_WAITFORRADIO,
	eVehicleBeastTransformState_IDLE
ENDENUM
eVehicleBeastTransformState vehicleBeastState = eVehicleBeastTransformState_INITIALISE
VECTOR vVehicleCreationPosition
FLOAT fVehicleCreationHeading
FLOAT fPrevVehForwardSpeed
STRING sCurrentRadioStation
VECTOR vPrevVehRotation

SCRIPT_TIMER tdSwapTimer
SCRIPT_TIMER tdSwapTimerRampT0
SCRIPT_TIMER tdSwapTimerRampT1
SCRIPT_TIMER tdSwapTimerRampT2
SCRIPT_TIMER tdSwapTimerRampT3
SCRIPT_TIMER tdSwapTimerRuinerT0
SCRIPT_TIMER tdSwapTimerRuinerT1
SCRIPT_TIMER tdSwapTimerRuinerT2
SCRIPT_TIMER tdSwapTimerRuinerT3
CONST_INT ciSWAP_TIME_DEFAULT 20000

MODEL_NAMES mnOldVehicleCached
MODEL_NAMES mnOldVehicle
VEHICLE_INDEX viPlayerSwapVehicle
MODEL_NAMES mnPlayerSwapVehicleRespawn = DUMMY_MODEL_FOR_SCRIPT
ENUM eVehicleSwapRule
	eVehicleSwapRuleTransformState_INITIALISE,
	eVehicleSwapRuleTransformState_CREATE,
	eVehicleSwapRuleTransformState_CLEANUP,
	eVehicleSwapRuleTransformState_WAITFORRADIO,
	eVehicleSwapRuleTransformState_IDLE
ENDENUM
eVehicleSwapRule vehicleSwapState = eVehicleSwapRuleTransformState_INITIALISE

SCRIPT_TIMER tdWaitForRadioSwitch

MP_OUTFITS_APPLY_DATA sApplyPreloadData
BOOL bPronOutfitReady
VEHICLE_INDEX viPlayerPronVehicle
ENUM eVehiclePronTransformState
	eVehiclePronTransformState_INITIALISE,
	eVehiclePronTransformState_CREATE,
	eVehiclePronTransformState_CLEANUP,
	eVehiclePronTransformState_WAITFORRADIO,
	eVehiclePronTransformState_IDLE
ENDENUM
eVehiclePronTransformState vehiclePronState = eVehiclePronTransformState_INITIALISE

SCRIPT_TIMER stVehWepBeastTimer
SCRIPT_TIMER stVehWepPronTimer
SCRIPT_TIMER stVehWepPronEffectTimer
SCRIPT_TIMER stVehWepForceAccelerateTimer
SCRIPT_TIMER stVehWepForceAccelerateTimerSpeedIncrement
SCRIPT_TIMER stVehWepGhostingTimer
SCRIPT_TIMER stVehWepFlippedTimer
SCRIPT_TIMER stVehWepJammedUsedTimer
SCRIPT_TIMER stVehWepFlippedUsedTimer
SCRIPT_TIMER stVehWepZonedTimer

SCRIPT_TIMER stVehWepPronInvTimer

CONST_INT ciVEH_SPD_INC							125

CONST_INT ci_FLASH_TIMER_INTERVAL 				500
SCRIPT_TIMER tdFlashPropTimer

FLOAT fForceAccelerateSpeedIntensity = 0.0

MODEL_NAMES mnPlayerVehModel

CONST_INT iPickupQuantity_Bombs			3



INT iCoronaLives = -1

SCRIPT_TIMER tdMissionStartWeaponsDisabled

INT iActualWeaponIndexUICached[FMMC_MAX_TEAMS]
INT iRequiredScoreUICached[FMMC_MAX_TEAMS]
INT iScoreUICached[FMMC_MAX_TEAMS]

TEXT_LABEL_23 sLabelObjHelpReprint
CONST_INT ci_OBJ_HELP_DEATH_CHECK_TIME 		4000
SCRIPT_TIMER tdObjHelpDeathCheckTimer

PTFX_ID ptfx_LoopedGunsmithLastKill
CONST_INT tdLastKillPTFXDelay 		1250
SCRIPT_TIMER tdLastKillPTFXTimer

SCRIPT_TIMER tdGunsmithWeaponEquipTimeoutFail
WEAPON_TYPE wtGunsmithPreviousWeapon

INT iSlipstreamTime
BOOL bVehicleIsSlipstreaming
INT iLastLapTime

INT iCurrentSplitCheckpoint[FMMC_MAX_TEAMS]

INT iTeamUniqueLapID[FMMC_MAX_TEAMS]
	
BOOL bTimecycleTransitionComplete = TRUE

CONST_INT ciPBD_SHOW_PRON_TIME_SINCE_DEATH			0

// ===== SEA MINES ===== //
INT iSeaMine_HasExploded = 0

// ===== Interact-With ===== //
ENUM INTERACT_WITH_STATE
	IW_STATE_IDLE,
	IW_STATE_INIT,
	IW_STATE_TURNING,
	IW_STATE_STARTING_ANIMATION,
	IW_STATE_ANIMATING,
	IW_STATE_DOWNLOADING,
	IW_STATE_CUT_PAINTING,
	IW_STATE_PLANT_VAULT_DOOR_EXPLOSIVES,
	IW_STATE_YACHT_CUTSCENE,
	IW_STATE_WALKING,
	IW_STATE_EXIT_ANIMATION,
	IW_STATE_FINISHED,
	IW_STATE_HOLD_INTERACT
ENDENUM // IF YOU ADD A NEW STATE, ADD IT TO GET_IW_STATE_NAME

CONST_INT ciINTERACT_WITH_DOWNLOAD_PAUSE_AT_PERCENT		99
CONST_INT ciINTERACT_WITH_DOWNLOAD_PAUSE_TIME			1656
CONST_INT ciINTERACT_WITH_DOWNLOAD_STUTTER_AMOUNT		40
CONST_INT ciINTERACT_WITH_DOWNLOAD_START_STUTTERING_AT	80
CONST_INT ciINTERACT_WITH_DOWNLOAD_SPEED 				25
CONST_INT ciINTERACT_WITH_DOWNLOAD_DELAY_WHEN_DONE 		20 

INT iInteractWithAssetLoadStagger = 0
INT iInteractWithAssetLoadStartedBS
CONST_FLOAT cfInteractWithAssetLoadDistance_Offrule		15.0

FLOAT fInteractWith_DownloadPercentage = 0
INT iInteractWith_DownloadSndID
INT iInteractWith_DownloadSndID_Remote

INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE eiInteractWith_PlantExplosivesState_LOCAL

INT iPaintingBeingCutLocalState = 0
INT iPaintingBeingCutAnimVariation = 0

FLOAT fPaintingCamBlendoutPhaseStart = 1.0
FLOAT fPaintingCamBlendoutPhaseEnd = 1.0

INT iPaintingKnifeAnimation[ciFMMC_MAX_PAINTING_INDEX_LEGACY]

CONST_INT ciInteractWith_Painting_EnterTopLeft				0
CONST_INT ciInteractWith_Painting_EnterTopRight				1
CONST_INT ciInteractWith_Painting_EnterBottomRight			2
CONST_INT ciInteractWith_Painting_EnterBottomLeft			3
CONST_INT ciInteractWith_Painting_IdleTopLeft				4
CONST_INT ciInteractWith_Painting_IdleTopRight				5
CONST_INT ciInteractWith_Painting_IdleBottomRight			6
CONST_INT ciInteractWith_Painting_IdleBottomLeft			7
CONST_INT ciInteractWith_Painting_CutTopLeftTopRight		8
CONST_INT ciInteractWith_Painting_CutTopRightBottomRight	9
CONST_INT ciInteractWith_Painting_CutBottomRightBottomLeft	10
CONST_INT ciInteractWith_Painting_CutBottomLeftTopLeft		11
CONST_INT ciInteractWith_Painting_ExitTopLeft				12
CONST_INT ciInteractWith_Painting_ExitTopRight				13
CONST_INT ciInteractWith_Painting_ExitBottomRight			14
CONST_INT ciInteractWith_Painting_ExitBottomLeft			15
CONST_INT ciInteractWith_Painting_ExitRollUp				16
CONST_INT ciInteractWith_Painting_V2EnterTopLeft			17
CONST_INT ciInteractWith_Painting_V2EnterTopRight			18
CONST_INT ciInteractWith_Painting_V2EnterBottomRight		19
CONST_INT ciInteractWith_Painting_V2EnterBottomLeft			20
CONST_INT ciInteractWith_Painting_V2IdleTopLeft				21
CONST_INT ciInteractWith_Painting_V2IdleTopRight			22
CONST_INT ciInteractWith_Painting_V2IdleBottomRight			23
CONST_INT ciInteractWith_Painting_V2IdleBottomLeft			24
CONST_INT ciInteractWith_Painting_V2CutTopLeftTopRight		25
CONST_INT ciInteractWith_Painting_V2CutTopRightBottomRight	26
CONST_INT ciInteractWith_Painting_V2CutBottomRightBottomLeft 27
CONST_INT ciInteractWith_Painting_V2CutBottomLeftTopLeft	28
CONST_INT ciInteractWith_Painting_V2ExitTopLeft				29
CONST_INT ciInteractWith_Painting_V2ExitTopRight			30
CONST_INT ciInteractWith_Painting_V2ExitBottomRight			31
CONST_INT ciInteractWith_Painting_V2ExitBottomLeft			32
CONST_INT ciInteractWith_Painting_V2ExitRollUp				33

CONST_INT ciInteractWith_Painting_Enter_TLE_CAM			0
CONST_INT ciInteractWith_Painting_Enter_TRE_CAM			1
CONST_INT ciInteractWith_Painting_Enter_BRE_CAM			2
CONST_INT ciInteractWith_Painting_Enter_BLE_CAM			3
CONST_INT ciInteractWith_Painting_Enter_RE_CAM			4

CONST_INT ciInteractWith_BailTime						25500
CONST_INT ciInteractWith_PostAnimResetFrameDelay		7
CONST_INT ciInteractWith_MeterFillDuration 				6000
TWEAK_FLOAT cfInteractWith_OccupiedAreaCheckSize 	 	0.01

CONST_INT ciInteractWith_LaptopAnim_IDLE	1
CONST_INT ciInteractWith_LaptopAnim_EXIT	2

ENUM INTERACT_WITH_CAMERA_STATE
	IW_CAM_NONE,
	IW_CAM_CREATE,
	IW_CAM_HOLD,
	IW_CAM_CLEAN_UP
ENDENUM
STRUCT INTERACT_WITH_CAMERA_VARS
	VECTOR vRot				//Initial rotation of the camera, gets offset by player heading
	VECTOR vOffsetCoords	//Position relative to the player
	FLOAT fPhaseEnd = 1.0	//When during the animation we want to clean up the camera
	FLOAT fFOV				//FOV for camera
	INT iDuration			//Animation duration, only needed for non sync scenes
	INT iLerpIn = 1000		//Lerp Time, set to 0 for a cut
	INT iLerpOut = 1000		//Lerp Time, set to 0 for a cut
	BOOL bIsAnimated = FALSE //Does this interact with use an animated camera
	STRING sCamAnimName //Cam anim to play
	STRING sCamAnimDict //Cam anim dict
	BOOL bProcess
	BOOL bStartTagHit
ENDSTRUCT
ENUM HOLD_BUTTON_INTERACT_STATE
	HOLD_BUTTON_STATE_WAITING,
	HOLD_BUTTON_STATE_REACT_START,
	HOLD_BUTTON_STATE_REACT_LOOP,
	HOLD_BUTTON_STATE_RETURN_LOOP
ENDENUM

CONST_INT ciInteractWith_Bitset__QuickBlend							0
CONST_INT ciInteractWith_Bitset__BrokeOut							1
CONST_INT ciInteractWith_Bitset__SlowBlendIn						2
CONST_INT ciInteractWith_Bitset__SlowBlendOut						3
CONST_INT ciInteractWith_Bitset__SkippedTransition					4
CONST_INT ciInteractWith_Bitset__HidPlayerWeaponForEndCutscene		5

STRUCT INTERACT_WITH_VARS
	
	INTERACT_WITH_STATE eInteractWith_CurrentState = IW_STATE_IDLE
	INT iInteractWith_LastFrameProcessed = -1
	
	TEXT_LABEL_15 tlInteractWith_Object
	INT iInteractWith_HelptextGiverIndex = -1
	INT iInteractWith_LastFrameHelptextGiverIndex = -1
	INT iInteractWith_CachedIndex = -1
	OBJECT_INDEX oiInteractWith_CachedObj
	
	INT iInteractWith_Bitset
	
	INT iInteractWith_AudioLoopID = -1
	INT iInteractWith_AudioBS = 0
	
	WEAPON_TYPE wtInteractWith_StoredWeapon = WEAPONTYPE_PISTOL
	
	SCRIPT_TIMER tdInteractWith_AnimationTimer
	SCRIPT_TIMER stInteractWith_BailTimer
	INT iInteractWith_StartInteractionImmediately = -1
	
	ENTITY_INDEX eiInteractWith_CachedSceneAlignmentEntities[FMMC_MAX_NUM_OBJECTS]
	ENTITY_INDEX eiInteractWith_CurrentSceneAlignmentEntity_AttachedEntity
	
	INT iInteractWith_ObjectiveTextToUse = ciObjectiveText_Primary
	
	// Camera
	CAMERA_INDEX camInteractWith_Camera
	INTERACT_WITH_CAMERA_STATE eInteractWith_CameraState
	INTERACT_WITH_CAMERA_VARS sInteractWith_CameraVars
	
	// Temp Vars
	STRING sInteractWith_Dictionary
	STRING sInteractWith_AnimName
	STRING sInteractWith_FailsafeDictionary
	STRING sInteractWith_FailsafeAnimName
	STRING sInteractWith_AudioBankName
	
	// Sync Scene vars
	VECTOR vInteractWith_TargetPos
	VECTOR vInteractWith_TargetRot
	FLOAT fInteractWith_RequiredHeading
	
	// Misc
	FLOAT fInteractWith_DistanceAllowed
	FLOAT fInteractWith_StartPhase = 0.15
	BOOL bInteractWith_Occupied
	SCRIPT_TIMER stInteractWith_DownloadTimer[FMMC_MAX_NUM_OBJECTS]
	SCRIPT_TIMER stInteractWith_DownloadStutterTimer[FMMC_MAX_NUM_OBJECTS]
	INT iInteractWith_ObjectsPlacedLocalCounter
	
	// Debug
	BOOL bInteractWith_ExtraDebug = FALSE
	
	//Hold Interact
	HOLD_BUTTON_INTERACT_STATE eHoldInteractState
	SCRIPT_TIMER stHoldInteractReactTimer
	SCRIPT_TIMER stHoldInteractReactDuration
	INT iHoldInteractReactTimerLength
	INT iHoldInteractReactAnim
	BOOL bDoneLongHoldReact
ENDSTRUCT
INTERACT_WITH_VARS sIWInfo

//Yacht
STRUCT_NET_YACHT_SCENE sMoveScene
CAMERA_INDEX cYachtCam1, cYachtCam2
INT iYachtCutStage = -1
INT iYachtScene = -1

// ===== Beam Hack Anim ===== //
ENUM BEAMHACKANIM_STATE
	BHA_STATE_NONE,
	BHA_STATE_TURNING,
	BHA_STATE_ENTER,
	BHA_STATE_LOOPING,
	BHA_STATE_EXIT,
	BHA_STATE_WALKING
ENDENUM

BEAMHACKANIM_STATE bhaBeamHackAnimState = BHA_STATE_NONE

// ===== Fingerprint Keypad Hack Anim ===== //
ENUM FINGERPRINTKEYPADHACKANIM_STATE
	FKHA_STATE_NONE,
	FKHA_STATE_TURNING,
	FKHA_STATE_ENTER,
	FKHA_STATE_LOOPING,
	FKHA_STATE_BACK_OUT,
	FKHA_STATE_LOOP_PRE_EXIT,
	FKHA_STATE_REATTEMPT,
	FKHA_STATE_QUICK_EXIT,
	FKHA_STATE_SUCCESS_EXIT,
	FKHA_STATE_EXIT,
	FKHA_STATE_WALKING
ENDENUM
FINGERPRINTKEYPADHACKANIM_STATE fkhaFingerprintKeypadHackAnimState = FKHA_STATE_NONE
VECTOR	vFingerprintKeypadHackAnimLastPos
VECTOR 	vFingerprintKeypadHackAnimLastRot
INT     iFingerprintKeypadForCurrentAnim = -1

// ====================== TURF WAR START =========================
SCALEFORM_INDEX SF_TWH_MovieIndex

ENUM TurfWarHUDState
	TW_REQUEST_ASSETS = 0,
	TW_INIT,
	TW_RUNNING
ENDENUM

TurfWarHUDState eTWHUDState

CONST_INT ci_TURF_WAR_STAGGERED_LOOP_COUNT					5
CONST_INT ci_TURF_WAR_STAGGERED_LOOP_ITERATIONS				30
CONST_INT ci_TURF_WAR_STAGGERED_LOOP_TIMER_CAP				1250
SCRIPT_TIMER tdTurfWarStaggeredLoopTimerCap

//INT	iLocalPropAmountOwnedByPart[NUM_NETWORK_PLAYERS]
BOOL bTurfWarScaleFormActive 
INT iPropsHighestTeamForHUD = -1
//FLOAT fPropsTaggedInSuccession
//CONST_INT ci_TURF_WAR_PROP_TAGGED_SOUND_CHAIN_LENGTH		1000
//SCRIPT_TIMER tdTurfWarPropTagChainTimer

SCRIPT_TIMER tdTurfWarTimerPropClaimLocal
CONST_INT ci_TURF_WAR_PROP_CLAIM_LOCAL						150
INT iLocalPropLastBroadcast									=-1

CONST_INT ci_TURF_WAR_BLOCK_PULSE_ON_RESET					500
SCRIPT_TIMER tdTurfWarBlockPulseOnReset

INT iTurfWarTotalClaimableProps
INT iTurfWarStaggeredLoopIterations = 0
CONST_INT ci_TURF_WAR_SCORE_ADD_TIME 						1000
CONST_INT ci_TURF_WAR_LOCAL_PROP_CLAIM_TIME_IN_FLASHES		10
SHAPETEST_INDEX stiPropClaiming

SCRIPT_TIMER tdTurfWarCollisionSpectatorFixTimer			
CONST_INT ci_TURF_WAR_COLLISION_SPECTATOR_TIME				2000

SCRIPT_TIMER tdTurfWarMarkerRingFlash						
SCRIPT_TIMER tdTurfWarMarkerRingFlashDuration
CONST_INT ci_TURF_WAR_MARKER_RING_FLASH						250
CONST_INT ci_TURF_WAR_MARKER_RING_FLASH_DURATION			2250

SCRIPT_TIMER tdTurfWarGrowTimer
CONST_INT ci_TURF_WAR_MARKER_GROW_TIME						40
FLOAT fTurfWarMarkerSize = 0.0
FLOAT iTurfWarChangeMarkerSize = 0.05

SCRIPT_TIMER tdPropResetClearBubble	
CONST_INT ci_Prop_Reset_Clear_Bubble						1250

#IF IS_DEBUG_BUILD
BOOL bEnableWidgetPropClaimTable = FALSE
BOOL bEnableWidgetPropClaimSimple = FALSE
BOOL bEnableWidgetPropClaimLine = FALSE
BOOL bEnableWidgetPropClaimNumberCount = FALSE
BOOL bEnableWidgetPrintsFor3137595bug = FALSE
#ENDIF
// ====================== TURF WAR END =========================

#IF IS_DEBUG_BUILD
BOOL bEnableWidgetOvertimeZoneMarkers = FALSE
BOOL bWasEnableWidgetOvertimeZoneMarkers = FALSE
BOOL bPutMeInSuddenDeath = FALSE
BOOL bTestOvertimeZones = FALSE
#ENDIF

SCRIPT_TIMER tdLostDamnedFlashScoreTimer
CONST_INT ci_Lost_Damned_Flash_Score_Time					3000

INT iPreviousLocToDraw

INT iTeamsFailedForSDTimeExpired
INT iVehicleWepModelsToLoadBS

BOOL bShouldStartControl = FALSE
BOOL bShouldProcessCokeGrabHud = FALSE

SCRIPT_TIMER tdHackingComplete
CONST_INT ciHACKING_COMPLETE_TIME 5000
INT iHackingPedHelpBitSet[FMMC_MAX_PEDS_BITSET]
INT iHackingPedInRangeBitSet[FMMC_MAX_PEDS_BITSET]
INT iHackingVehHelpBitSet
INT iHackingVehInRangeBitSet
INT iHackingLoopSoundID = -1

SCRIPT_TIMER tdJuggernautVFX
TWEAK_INT ciJUGGERNAUT_VFX_INTERVAL	4000
VECTOR vJuggernautVFXPos = <<0.1, 0.18, 0.0>>
VECTOR vJuggernautVFXRot = <<0.0, 0.0, 28.0>>

VECTOR vJuggernautVFXFire1Pos = <<0.1, 0.18, 0.0>>
VECTOR vJuggernautVFXFire1Rot = <<0.0, 47.0, 28.0>>
VECTOR vJuggernautVFXFire2Pos = <<0.1, 0.18, 0.0>>
VECTOR vJuggernautVFXFire2Rot = <<0.0, -47.0, 28.0>>

INT iSoundsSetForJuggernaut

BLIP_INDEX biBJXL

#IF IS_DEBUG_BUILD
BOOL bEnableScriptAudioSpammyPrints = FALSE
#ENDIF
// ==================== Killstreak Powerups START ===============================
INT iKSPowerupCounterTurfBomb 		= 1
INT iKSPowerupCounterTurfMissile 	= 1
INT iKSPowerupCounterTurfTrade 		= 1
INT iKSPowerupCounterFreezePos 		= 1

//KS Bomb
VECTOR vTurfBombCachedPoint

//KS Missile
VECTOR vTurfMissileCachedPoint
VECTOR vTurfMissileCachedDirection
INT iTurfMissileIncrements									= 0
SCRIPT_TIMER tdKillStreakTurfMissileTimer
CONST_INT ci_Turf_War_Missile_Interval						300

SCRIPT_TIMER tdKillStreakFreezePosition
CONST_INT ci_KillStreak_Freeze_Position						5000

// ==================== Killstreak Powerups END ===============================

BLIP_INDEX biCaptureRangeBlip[FMMC_MAX_PEDS]
BLIP_INDEX biVehCaptureRangeBlip[FMMC_MAX_VEHICLES]

CONST_INT ciVW_MACHINE_GUN_FIRERATE					6 //how many per second
SCRIPT_TIMER tdMachineGunOverheatTimer[MAX_NUM_MC_PLAYERS]
INT iBulletFiredTimeStamp[MAX_NUM_MC_PLAYERS]
INT iTimeSinceLastBullet[MAX_NUM_MC_PLAYERS]
INT iBulletSoundID = -1
BOOL bTriggerMGReload = FALSE

ENUM eVehicleWeaponMachineGunFireState
	eVehicleWeaponMachineGunFireState_FIRE,
	eVehicleWeaponMachineGunFireState_COOLDOWN,
	eVehicleWeaponMachineGunFireState_OVERHEAT,
	eVehicleWeaponMachineGunFireState_RELOAD
ENDENUM

eVehicleWeaponMachineGunFireState eMGFireState
SPECIAL_VEHICLE_PICKUP eLastVehiclePickup

INT iTimeFiring
INT iTimeJustFired

PTFX_ID ptfxVehDamageSmoke
PTFX_ID ptfxHeliDamageSmoke1
PTFX_ID ptfxHeliDamageSmoke2

INT iMaxCashLocalPlayerCanCarry					

CONST_INT ciNET_STATISTICS_AND_SIGNALLING_INFO 1
#IF IS_DEBUG_BUILD
// following used for displaying player lists
CONST_FLOAT PlayerListStartY  0.079
CONST_FLOAT PlayerListYSpace  0.014
CONST_FLOAT PlayerListWindowTopX  0.363
CONST_FLOAT PlayerListWindowTopY  0.0
CONST_FLOAT PlayerListWindowWidth  0.637
CONST_FLOAT PlayerListWindowHeight  1.0

CONST_FLOAT PlayerListVoiceColumnX  0.364
CONST_FLOAT PlayerListCountColumnX  0.372
CONST_FLOAT PlayerListNameColumnX  0.379
CONST_FLOAT PlayerListCodeNameColumnX  0.453
CONST_FLOAT PlayerListNetIDColumnX  0.501
CONST_FLOAT PlayerListGlobalIDColumnX  0.636
CONST_FLOAT PlayerListParticipantIDColumnX  0.650
CONST_FLOAT	PlayerListCashX  0.670
CONST_FLOAT PlayerListRankColumnX  0.697
CONST_FLOAT PlayerListWantedColumnX  0.715
CONST_FLOAT PlayerListInstanceColumnX  0.742
CONST_FLOAT PlayerListMissionColumnX  0.757
CONST_FLOAT PlayerListMissionNameColumnX  0.8
CONST_FLOAT PlayerListTeamColumnX  0.613
CONST_FLOAT PlayerListPassiveColumnX  0.585
CONST_FLOAT PlayerListMentalColumnX  0.550

//// FM event F9s
//FLOAT PlayerListParticipationRangeColumnX = 0.510
//FLOAT PlayerListResWithPassiveColumnX = 0.565
//FLOAT PlayerListResWithHideColumnX = 0.623
//FLOAT PlayerListWaitToStartColumnX = 0.680
//FLOAT PlayerListPermanentColumnX = 0.747
//FLOAT PlayerListCriticalColumnX = 0.815
//FLOAT PlayerListBlockedColumnX = 0.882

CONST_FLOAT PlayerListSessionIDX  0.374
CONST_FLOAT	PlayerListMyNameY	0.028
CONST_FLOAT PlayerListSessionInfoY  0.045 
CONST_FLOAT PlayerListHeadersY  0.06

CONST_FLOAT PlayerListJoinableX  0.72
CONST_FLOAT PlayerListJoinableY  0.429
CONST_FLOAT PlayerListColumnWidth  0.038

CONST_FLOAT PlayerListHistoryX  0.72
CONST_FLOAT PlayerListHistoryY  0.223
CONST_FLOAT PlayerListHistoryWidth  0.098
CONST_FLOAT PlayerListHistoryPlayedAddX  0.04

CONST_FLOAT PlayerListHistoryCopsX  0.72
CONST_FLOAT PlayerListHistoryCopsY  0.125
CONST_FLOAT PlayerListHistoryCopsWidth  0.098
CONST_FLOAT PlayerListHistoryJobListX  0.79

CONST_FLOAT PlayerListHistoryJobListMissionX  0.85
CONST_FLOAT PlayerListHistoryJobListY  0.608
CONST_FLOAT PlayerListHistoryJobListWidth  0.098

CONST_FLOAT PlayerListHistoryRejectedX  0.72
CONST_FLOAT PlayerListHistoryRejectedY  0.72
CONST_FLOAT PlayerListHistoryRejectedWidth  0.098

CONST_FLOAT	PlayerListAmbientScriptNameX	0.72
CONST_FLOAT	PlayerListAmbientDetailsX		0.78
CONST_FLOAT	PlayerListAmbientY				0.788

CONST_FLOAT PlayerListTeamNumbersX  0.369
CONST_FLOAT PlayerListTeamNumbersY  0.796
CONST_FLOAT PlayerListTeamCountX  0.459
CONST_FLOAT	PlayerListTeamNumbers2X	0.484
CONST_FLOAT	PlayerListTeamCount2X	0.530

CONST_FLOAT	PlayerListFlagsX		0.875
CONST_FLOAT	PlayerListFlagsStartY	0.100
CONST_FLOAT	PlayerListFlagsAddY		0.015
CONST_FLOAT	PlayerListFlagsAddY_LARGE 0.02
CONST_FLOAT	PlayerListFlagsAddX		0.25

CONST_FLOAT ScriptList_ScriptNameX 			0.366
CONST_FLOAT ScriptList_ScriptNameY 			0.530
CONST_FLOAT ScriptList_ScriptInstanceX 		0.448
CONST_FLOAT ScriptList_ScriptHostX 			0.528
CONST_FLOAT ScriptList_ScriptParticipantsX 	0.483
CONST_FLOAT ScriptList_ScriptNetPedX		0.600
CONST_FLOAT ScriptList_ScriptNetVehX		0.630
CONST_FLOAT ScriptList_ScriptNetObjX		0.660
CONST_FLOAT ScriptList_ScriptUpdateTimeX 	0.688
CONST_FLOAT ScriptList_BarWidthPerMS		0.005
CONST_FLOAT ScriptList_UpdateTimeBarX 		0.730
CONST_FLOAT ScriptList_BarWidthHeight 		0.01
CONST_FLOAT ScriptList_PeakTextSpace		0.019

CONST_FLOAT PropertyDetailsNameX	0.840
CONST_FLOAT	PropertyDetailsNameY	0.900
CONST_FLOAT	PropertyDetailsStartX	0.840
CONST_FLOAT	PropertyDetailsStartY 	0.880

CONST_FLOAT TranPlayerListCountColumnX 		0.070
CONST_FLOAT TranPlayerListCountColumnReadX 	0.180
CONST_FLOAT TranPlayerListCountColumnTalkX 	0.060
CONST_FLOAT TranPlayerListCountColumnHostX 	0.210
CONST_FLOAT TranPlayerListHeadingY 			0.060
CONST_FLOAT TranPlaylistX 					0.040
CONST_FLOAT TranPlaylistY 					0.420
CONST_FLOAT TranPlaylistShufX 				0.200
CONST_FLOAT TranPlayerListStartY 			0.080
CONST_FLOAT TranPlayerListTopX 				0.0
CONST_FLOAT TranPlayerListTopY 				0.000
CONST_FLOAT TranPlayerListTopWidth			0.366
CONST_FLOAT TranPlayerListTopHeight 		0.484

CONST_FLOAT ContactMissionX  				0.365
CONST_FLOAT ContactMissionY  				0.758
CONST_FLOAT	ContactMissionAddX				0.055
//TWEAK_INT	ContactMissionRows				10


CONST_FLOAT g_fPlayerListTextScale_CONST 0.23

PROC SET_UP_F9_BAR_TEXT()
	//Display the menu titles		
	SET_TEXT_SCALE(0.330, 0.330)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	SET_TEXT_DROPSHADOW(0, 0, 0, 0, 255)
	SET_TEXT_EDGE(0, 0, 0, 0, 255)
ENDPROC
FUNC TEXT_LABEL_63 FLOAT_TO_STRING_F9(FLOAT fPos)
	TEXT_LABEL_63 tl63
	tl63 += ROUND(fPos)
	tl63 += "."
	FLOAT fTemp = fPos%1
	fTemp *= 10
	INT iTemp = ROUND(fTemp)
	//Make it +ve
	IF iTemp < 0
		iTemp *=-1
	ENDIF
	tl63 += iTemp
	RETURN tl63
ENDFUNC
FUNC TEXT_LABEL_63 VECTOR_TO_STRING_F9(VECTOR vPos)
	TEXT_LABEL_63 tl63, tl63Temp
	tl63Temp = FLOAT_TO_STRING_F9(vPos.x)
	tl63 += tl63Temp
	tl63 += ","
	tl63Temp = FLOAT_TO_STRING_F9(vPos.y)
	tl63 += tl63Temp
	tl63 += ","
	tl63Temp = FLOAT_TO_STRING_F9(vPos.z)
	tl63 += tl63Temp
	RETURN tl63
ENDFUNC	
PROC DISPLAY_TEXT_WITH_LITERAL_STRING_FOR_RELEASE_F9(FLOAT DisplayAtX, FLOAT DisplayAtY, STRING pTextLabel, STRING pLiteralString)
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(pTextLabel)
		ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(pLiteralString)
	END_TEXT_COMMAND_DISPLAY_TEXT(DisplayAtX, DisplayAtY)
ENDPROC
//PROC DISPLAY_TEXT_WITH_LITERAL_STRING_FOR_RELEASE_F9(FLOAT DisplayAtX, FLOAT DisplayAtY, STRING pTextLabel, STRING pLiteralString)
//	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(pTextLabel)
//		ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(pLiteralString)
//	END_TEXT_COMMAND_DISPLAY_TEXT(DisplayAtX, DisplayAtY)
//ENDPROC
PROC DISPLAY_TEXT_WITH_LITERAL_STRING_AND_INT_FOR_RELEASE_F9(FLOAT DisplayAtX, FLOAT DisplayAtY, STRING pLiteralString, INT iPassed)
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT("GEN_BIGM_NUM")
		TEXT_LABEL_63 tl63 = pLiteralString
		tl63 += " = "
		ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(tl63)
		ADD_TEXT_COMPONENT_INTEGER(iPassed)
	END_TEXT_COMMAND_DISPLAY_TEXT(DisplayAtX, DisplayAtY)
ENDPROC

PROC DISPLAY_TEXT_WITH_LITERAL_STRING_AND_TIME_FOR_RELEASE_F9(FLOAT DisplayAtX, FLOAT DisplayAtY, STRING pLiteralString, INT iPassed)
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT("TWOSTRINGS")
		TEXT_LABEL_63 tl63 = pLiteralString
		tl63 += " = "
		ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(tl63)
		ADD_TEXT_COMPONENT_SUBSTRING_TIME(iPassed, TIME_FORMAT_HOURS | TIME_FORMAT_MINUTES | TIME_FORMAT_SECONDS)
	END_TEXT_COMMAND_DISPLAY_TEXT(DisplayAtX, DisplayAtY)
ENDPROC
PROC DISPLAY_TEXT_WITH_LITERAL_STRING_AND_FLOAT_FOR_RELEASE_F9(FLOAT DisplayAtX, FLOAT DisplayAtY, STRING pLiteralString, FLOAT fPassed)
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT("GEN_BIGM_NUM")
		ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(pLiteralString)
		ADD_TEXT_COMPONENT_INTEGER(fPassed)
	END_TEXT_COMMAND_DISPLAY_TEXT(DisplayAtX, DisplayAtY)
ENDPROC
PROC DO_F9_BAR()
	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE_PRIORITY_HIGH)
	DRAW_RECT(0.5, 0.971, 1.0, 0.068, 0, 0, 0, 255)
	TEXT_LABEL_63 tl63, tl63Temp
	VECTOR vPos = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
	tl63 = "P: "
	tl63Temp = VECTOR_TO_STRING_F9(vPos)
	tl63 += tl63Temp
	SET_UP_F9_BAR_TEXT()
	DISPLAY_TEXT_WITH_LITERAL_STRING_FOR_RELEASE_F9(0.040, 0.943, "STRING", tl63)
	vPos = GET_FINAL_RENDERED_CAM_COORD()
	tl63 = "C: "
	tl63Temp = VECTOR_TO_STRING_F9(vPos)
	tl63 += tl63Temp
	SET_UP_F9_BAR_TEXT()
	DISPLAY_TEXT_WITH_LITERAL_STRING_FOR_RELEASE_F9(0.166, 0.943, "STRING", tl63)
	tl63 = "FPS: "
	tl63Temp =FLOAT_TO_STRING_F9(1.0/GET_FRAME_TIME())
	tl63 += tl63Temp
	SET_UP_F9_BAR_TEXT()
	DISPLAY_TEXT_WITH_LITERAL_STRING_FOR_RELEASE_F9(0.316, 0.943, "STRING", tl63)
	tl63 = "F:"
	tl63 += GET_FRAME_COUNT()
	SET_UP_F9_BAR_TEXT()
	DISPLAY_TEXT_WITH_LITERAL_STRING_FOR_RELEASE_F9(0.377, 0.943, "STRING", tl63)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		tl63 = "NET TIME:"
		tl63 += GET_TIME_AS_STRING(GET_NETWORK_TIME())
		SET_UP_F9_BAR_TEXT()
		DISPLAY_TEXT_WITH_LITERAL_STRING_FOR_RELEASE_F9(0.468, 0.943, "STRING", tl63)
	ENDIF
	
	tl63 = "POSIX:"
	tl63 += GET_CLOUD_TIME_AS_INT()
	SET_UP_F9_BAR_TEXT()
	DISPLAY_TEXT_WITH_LITERAL_STRING_FOR_RELEASE_F9(0.658, 0.943, "STRING", tl63)
	
	tl63 = "T: "
	tl63 += GET_CLOCK_HOURS()
	tl63 += ":"
	IF GET_CLOCK_MINUTES() < 10
		tl63 += "0"
	ENDIF
	tl63 += GET_CLOCK_MINUTES()
	SET_UP_F9_BAR_TEXT()
	DISPLAY_TEXT_WITH_LITERAL_STRING_FOR_RELEASE_F9(0.925, 0.943, "STRING", tl63)
	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(FALSE)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
ENDPROC

PROC SETUP_NET_ENTITY_TEXT(INT iCre, INT iRes, INT iMaxRes = -1)
	SET_TEXT_SCALE(0.0000, g_fPlayerListTextScale_CONST)

	IF iCre > iRes										//More created than reserved
	OR ((iMaxRes <> -1) AND (iMaxRes - iRes < 10))		//or we only have ten total left to reserve
		SET_TEXT_COLOUR(255,0,0, 255)					//red
	ELIF iRes > 0
		IF iRes - iCre > 5								//More than five reserved but not used
			SET_TEXT_COLOUR(255,128,0, 255)				//orange
		ELIF iRes > iCre								//More reserved than used
			SET_TEXT_COLOUR(255,255,0, 255)				//yellow
		ELSE											//All used that are reserved
			SET_TEXT_COLOUR(0,255,0, 255)				//green
		ENDIF
	ELSE												//None reserved
		SET_TEXT_COLOUR(128,128,128, 255)				//grey
	ENDIF
ENDPROC


PROC DISPLAY_BOOL_FOR_F9(BOOL bPassed, STRING stPassed, FLOAT &theX, FLOAT &theY, BOOL bAlwaysDisplays = FALSE)
	IF bPassed
	OR bAlwaysDisplays
		SET_TEXT_SCALE(0.0000, g_fPlayerListTextScale_CONST+0.1)
		SET_TEXT_COLOUR(20, 255, 0, 255)
		DISPLAY_TEXT_WITH_LITERAL_STRING_FOR_RELEASE_F9(theX, theY, "STRING", stPassed)
		theY += PlayerListFlagsAddY_LARGE
		IF theY > 0.9
			theX += PlayerListFlagsAddX
			theY = PlayerListFlagsStartY
		ENDIF 
	ENDIF
ENDPROC

PROC DISPLAY_INT_FOR_F9(INT iPassed, STRING stPassed, FLOAT &theX, FLOAT &theY, BOOL bAlwaysDisplays = FALSE)
	IF iPassed != 0
	OR bAlwaysDisplays
		SET_TEXT_SCALE(0.0000, g_fPlayerListTextScale_CONST+0.1)
		SET_TEXT_COLOUR(20, 255, 0, 255)
		DISPLAY_TEXT_WITH_LITERAL_STRING_AND_INT_FOR_RELEASE_F9(theX, theY, stPassed, iPassed)
		theY += PlayerListFlagsAddY_LARGE		
		IF theY > 0.9
			theX += PlayerListFlagsAddX
			theY = PlayerListFlagsStartY
		ENDIF 	
	ENDIF 	
ENDPROC

PROC DISPLAY_LT_STRING_FOR_F9(STRING stPassed, FLOAT &theX, FLOAT &theY, BOOL bAlwaysDisplays = FALSE)
	IF bAlwaysDisplays
		SET_TEXT_SCALE(0.0000, g_fPlayerListTextScale_CONST+0.1)
		SET_TEXT_COLOUR(20, 255, 0, 255)
		DISPLAY_TEXT_WITH_LITERAL_STRING_FOR_RELEASE_F9(theX, theY, "STRING", stPassed)
		theY += PlayerListFlagsAddY_LARGE		
		IF theY > 0.9
			theX += PlayerListFlagsAddX
			theY = PlayerListFlagsStartY
		ENDIF 	
	ENDIF 	
ENDPROC
PROC DISPLAY_FLOAT_FOR_F9(FLOAT fPassed, STRING stPassed, FLOAT &theX, FLOAT &theY, BOOL bAlwaysDisplays = FALSE)
	IF fPassed != 0.0
	OR bAlwaysDisplays
		SET_TEXT_SCALE(0.0000, g_fPlayerListTextScale_CONST+0.1)
		SET_TEXT_COLOUR(20, 255, 0, 255)
		DISPLAY_TEXT_WITH_LITERAL_STRING_AND_FLOAT_FOR_RELEASE_F9(theX, theY, stPassed, fPassed)
		theY += PlayerListFlagsAddY_LARGE		
		IF theY > 0.9
			theX += PlayerListFlagsAddX
			theY = PlayerListFlagsStartY
		ENDIF 	
	ENDIF 	
ENDPROC
PROC DISPLAY_STRING_FOR_F9(STRING stToPrint, STRING StName, FLOAT &theX, FLOAT &theY)
	IF NOT IS_STRING_NULL_OR_EMPTY(stToPrint)
		IF NOT IS_STRING_NULL_OR_EMPTY(StName)
			SET_TEXT_SCALE(0.0000, g_fPlayerListTextScale_CONST+0.1)
			SET_TEXT_COLOUR(20, 255, 0, 255)
			TEXT_LABEL_63 tl63 = StName
			tl63 += " = "
			DISPLAY_TEXT_WITH_LITERAL_STRING_FOR_RELEASE_F9(theX, theY, "STRING", tl63)
			theY += PlayerListFlagsAddY_LARGE	
		ENDIF
		SET_TEXT_SCALE(0.0000, g_fPlayerListTextScale_CONST+0.1)
		SET_TEXT_COLOUR(20, 255, 0, 255)	
		DISPLAY_TEXT_WITH_LITERAL_STRING_FOR_RELEASE_F9(theX, theY, "STRING", stToPrint)
		theY += PlayerListFlagsAddY_LARGE		
		IF theY > 0.9
			theX += PlayerListFlagsAddX
			theY = PlayerListFlagsStartY
		ENDIF 	
	ENDIF 	
ENDPROC
PROC DISPLAY_TWO_STRINGS_FOR_F9(STRING stToPrint, STRING stToPrint2, FLOAT &theX, FLOAT &theY)
	SET_TEXT_SCALE(0.0000, g_fPlayerListTextScale_CONST+0.1)
	SET_TEXT_COLOUR(20, 255, 0, 255)
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT("BRIEF_STRING2")
		ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(stToPrint)
		ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(stToPrint2)
	END_TEXT_COMMAND_DISPLAY_TEXT(theX, theY)
	theY += PlayerListFlagsAddY_LARGE	
	IF theY > 0.9
		theX += PlayerListFlagsAddX
	ENDIF 		
ENDPROC
PROC DISPLAY_VECTOR_FOR_F9(VECTOR vPos, STRING stPassed, FLOAT &theX, FLOAT &theY)
	IF vPos.x != 0.0
	OR vPos.y != 0.0
	OR vPos.z != 0.0
		TEXT_LABEL_63 tl63, tl63Temp
		tl63 = stPassed
		tl63Temp = VECTOR_TO_STRING_F9(vPos)
		tl63 += tl63Temp
		SET_TEXT_SCALE(0.0000, g_fPlayerListTextScale_CONST+0.1)
		SET_TEXT_COLOUR(20, 255, 0, 255)
		DISPLAY_TEXT_WITH_LITERAL_STRING_FOR_RELEASE_F9(theX, theY, "STRING", tl63)
		theY += PlayerListFlagsAddY_LARGE	
		IF theY > 0.9
			theX += PlayerListFlagsAddX
		ENDIF 	
	ENDIF
ENDPROC	
PROC UPDATE_MP_RELEASE_F9_DisplayRunningScripts()

	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE_PRIORITY_HIGH)
	DRAW_RECT_FROM_CORNER(0, 0, 1,  1, 0,0,0,150)
	SET_TEXT_SCALE(0.0000, g_fPlayerListTextScale_CONST)
	SET_TEXT_COLOUR(182, 255, 0, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING_FOR_RELEASE_F9(PlayerListSessionIDX, PlayerListMyNameY, "STRING", "RunningScripts")
	SET_TEXT_SCALE(0.0000, g_fPlayerListTextScale_CONST)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_PLAYER_NAME(PlayerListNameColumnX + 0.06, PlayerListMyNameY, "STRING", GET_PLAYER_NAME(PLAYER_ID()), GET_PLAYER_HUD_COLOUR(PLAYER_ID()))
	
	FLOAT theY	= PlayerListFlagsStartY
	FLOAT theX	= 0.1
	TEXT_LABEL_63 tl63 //, tl632
	INT iLoop
	DISPLAY_INT_FOR_F9(GET_NUM_RESERVED_MISSION_OBJECTS(TRUE), "RESERVED_MISSION_OBJECTS", theX, theY)	
	DISPLAY_INT_FOR_F9(GET_NUM_CREATED_MISSION_OBJECTS(TRUE), "CREATED_MISSION_OBJECTS", theX, theY)	
	DISPLAY_INT_FOR_F9(GET_MAX_NUM_NETWORK_OBJECTS(), "NUM_NETWORK_OBJECTS", theX, theY)	
	SCRIPT_THREAD_ITERATOR_RESET()
	//Get the next thread
	THREADID tIdTemp = SCRIPT_THREAD_ITERATOR_GET_NEXT_THREAD_ID()
	WHILE tIdTemp != NULL
		tl63 = ""
		IF iLoop < 10
			tl63 = "0"
		ENDIF
		tl63 +=iLoop
		tl63 += " - "
		tl63 +=NATIVE_TO_INT(tIdTemp)
		tl63 += " - "
//		IF NETWORK_IS_THREAD_A_NETWORK_SCRIPT(tIdTemp)
//			tl63 += " [N]	"
//			tl63 += GET_NAME_OF_SCRIPT_WITH_THIS_ID(tIdTemp)
//			tl632 = " -	Host = "
//			tl632 += "cant get this, native is missing" //GET_PLAYER_NAME(NETWORK_GET_HOST_OF_THREAD(tIdTemp))
//			DISPLAY_TWO_STRINGS_FOR_F9(tl63, tl632, theX, theY)
//		ELSE
//			tl63 += "[NS]"
			tl63 += GET_NAME_OF_SCRIPT_WITH_THIS_ID(tIdTemp)
			DISPLAY_STRING_FOR_F9(tl63, "", theX, theY)
//		ENDIF
		tIdTemp = SCRIPT_THREAD_ITERATOR_GET_NEXT_THREAD_ID()
		iLoop++
	ENDWHILE
ENDPROC

PROC UPDATE_MP_RELEASE_F9()

	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE_PRIORITY_HIGH)
	DRAW_RECT_FROM_CORNER(0, 0, 1,  1, 0,0,0,150)
	SET_TEXT_SCALE(0.0000, g_fPlayerListTextScale_CONST)
	SET_TEXT_COLOUR(182, 255, 0, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING_FOR_RELEASE_F9(PlayerListSessionIDX, PlayerListMyNameY, "STRING", "APARTMENTS")
	SET_TEXT_SCALE(0.0000, g_fPlayerListTextScale_CONST)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_PLAYER_NAME(PlayerListNameColumnX + 0.09, PlayerListMyNameY, "STRING", GET_PLAYER_NAME(PLAYER_ID()), GET_PLAYER_HUD_COLOUR(PLAYER_ID()))

	FLOAT theY	= PlayerListFlagsStartY
	FLOAT theX  = 0.1	
	//INT iBitSet = FMMC_TYPE_CUSTOM_CAR_GARAGE
	//INT iArrayIndex = iBitSet/32
	DISPLAY_BOOL_FOR_F9(g_bLauncherWillControlMiniMap, "g_bLauncherWillControlMiniMap", theX, theY)
	DISPLAY_INT_FOR_F9(MPGlobalsHud.HUDDISPLAYINGNOW, "HUDDISPLAYINGNOW", theX, theY)
//	DISPLAY_FLOAT_FOR_F9(MPGlobalsHud.HUDDISPLAYINGNOW, "HUDDISPLAYINGNOW", theX, theY)
//	DISPLAY_STRING_FOR_F9(MPGlobalsHud.HUDDISPLAYINGNOW, "HUDDISPLAYINGNOW", theX, theY)

ENDPROC
#ENDIF
CONST_INT ci_TOTAL_DISTANCE_CHECK_BARS 		5

INT iGPSTrackingMeterTextLabel[ci_TOTAL_DISTANCE_CHECK_BARS]
INT iGPSTrackingMeterTextLabelIndex = 0

PROC ASSIGN_GPS_TRACKING_METER_TEXT_LABEL_TO_PED_INDEX(INT iPed)
	INT i
	
	REPEAT ci_TOTAL_DISTANCE_CHECK_BARS i
		IF iGPSTrackingMeterTextLabel[i] = iPed
			EXIT
		ENDIF
	ENDREPEAT
	
	IF iGPSTrackingMeterTextLabelIndex >= 0 AND iGPSTrackingMeterTextLabelIndex < ci_TOTAL_DISTANCE_CHECK_BARS
		iGPSTrackingMeterTextLabel[iGPSTrackingMeterTextLabelIndex] = iPed
		iGPSTrackingMeterTextLabelIndex++
	ENDIF
ENDPROC

INT iGPSTrackingMeterRegisteredPedBitset[FMMC_MAX_PEDS_BITSET]	//CEIL(FMMX_MAX_PEDS / 32)
INT iGPSTrackingMeterPedIndex[ci_TOTAL_DISTANCE_CHECK_BARS]
VECTOR vGPSTrackingMeterStart[ci_TOTAL_DISTANCE_CHECK_BARS]
VECTOR vGPSTrackingMeterEnd[ci_TOTAL_DISTANCE_CHECK_BARS]
INT iGPSTrackingMeterDistCache[ci_TOTAL_DISTANCE_CHECK_BARS]
CONST_INT ciTimerDistanceBarDelay	1000
SCRIPT_TIMER timerDistanceBarDelay

CONST_INT ci_WAIT_FOR_PED_RULE_SPAWN			2500
SCRIPT_TIMER tdWaitForPedRuleSpawn

INT iPartForTeamHealthBar[FMMC_MAX_TEAMS]

SCRIPT_TIMER tdPronJammedStationaryTimer
VECTOR vPronJammedStationaryPos
BOOL bPrintSecondaryCokeGrabText

//SHAPETEST_INDEX siBombPlaceShapeTest
//BOOL bRequestedToPlaceBomb
//BOOL bCanPlaceBomb
//BOOL bPlaceCloserBomb

// SVM Audio Scene Stuff
INT iLocalAudioSceneSVM = -1
CONST_INT ci_SVM_AUDSCENE_TECHNICAL2				0
CONST_INT ci_SVM_AUDSCENE_BOXVILLE5					1
CONST_INT ci_SVM_AUDSCENE_WASTELANDER				2
CONST_INT ci_SVM_AUDSCENE_PHANTOM2					3
CONST_INT ci_SVM_AUDSCENE_VOLTIC2					4
CONST_INT ci_SVM_AUDSCENE_DUNE4						5
CONST_INT ci_SVM_AUDSCENE_RUINER2					6
CONST_INT ci_SVM_AUDSCENE_BLAZER5					7

SCRIPT_TIMER tdSpookRangeTimer[FMMC_MAX_PEDS]

INT iCountThisVehAsDestroyedBS = 0

ENUM SPAWN_IN_PLACED_VEH_STAGE
	eRESTARTVEH_INIT = 0,
	eRESTARTVEH_ENTER_VEH,
	eRESTARTVEH_FINISHED
ENDENUM
SPAWN_IN_PLACED_VEH_STAGE eRestartVehStage

INT iVehToSpawnIn

SCRIPT_TIMER tdVehWarpBailTime
CONST_INT ciVEH_WARP_TIMEOUT 5000

SCRIPT_TIMER tdJipNoTeamTimer
CONST_INT ciJIP_NO_TEAM_TIMEOUT 15000

SCRIPT_TIMER tdGangNoBossTimer
CONST_INT ciGANG_NO_BOSS_TIMOUT 10000

SCRIPT_TIMER tdSVMIntroSafetyTimer

STRUCT UI_SLOT
	STRING sName
	INT iValue = -3
ENDSTRUCT

STRUCT UI_SLOT_REZ
	PLAYER_INDEX piID
	INT iValue = -3
ENDSTRUCT

STRUCT PLAYER_STATE_LIST_RESURRECTION_DATA
	UI_SLOT_REZ uiSlot[MAX_NUM_MC_PLAYERS]
	INT iTeam[MAX_NUM_MC_PLAYERS]
	HUD_COLOURS hcTeam[FMMC_MAX_TEAMS]
ENDSTRUCT

INT iRezQTeamIterator = 0

SCRIPT_TIMER tdManualRespawnTeamVehicle_FailTimedOut
CONST_INT ci_MAN_RESP_FAIL_TIMED_OUT		8000

CONST_INT ci_FPC_DIRECTION_NORTH	0
CONST_INT ci_FPC_DIRECTION_SOUTH	1
CONST_INT ci_FPC_DIRECTION_EAST		2
CONST_INT ci_FPC_DIRECTION_WEST		3

SCRIPT_TIMER tdTopDownBlockDelay
CONST_INT ci_TD_DELAY				1150

SCRIPT_TIMER tdTopDownBlockAiming
CONST_INT ci_TD_BLOCK_AIMING		350


WEAPON_TYPE wtCurrentRuinerWeaponCache
INT iRuinerRocketsAmmo			= -1
INT iRuinerRocketsHomingAmmo	= -1
INT iRuinerMGAmmo				= -1
INT iRuinerRocketsAmmoMax		= -1
INT iRuinerRocketsHomingAmmoMax	= -1
INT iRuinerMGAmmoMax			= -1

#IF IS_DEBUG_BUILD
BOOL bDebugBlipPrints
BOOL bDebugBoundsPrints
BOOL bDebugRezHUDPrints

BOOL bSingleItemCashGrab
BOOL bDisableIWCam

//Commandlines
BOOL bInVehicleDebug
BOOL bPlacedMissionVehiclesDebugHealth
BOOL bFMMCVehBodyNetVehIDprints
BOOL bobjectiveVehicleOnCargobobDebug
BOOL bextrabombfootballdebug
BOOL bRunningBackDiveCamLine
BOOL bParticipantLoopPrints
BOOL bAllowAllMissionsWithTwoPlayers
BOOL bTeamColourDebug
BOOL bFMMCOverrideMultiRuleTimer
BOOL bTimeLimit
BOOL bBailTimerMc
BOOL bShowMeTagTeam
BOOL bWantedChange
BOOL bJS3226522
BOOL bAllowGangOpsWithoutBoss
BOOL bHandcuff
BOOL bLMhelpfulScriptAIPrints
BOOL bExpProjDebug
BOOL bObjectGotoDebug
BOOL bDebugPrintContainer
BOOL bDawnRaidWarpToObject
BOOL bShowVehSpawnNumberCycle
BOOL bbmbfbRadioDebug
BOOL bbombfootballdebug
BOOL bMultiPersonVehiclePrints
BOOL bDoNotPauseRenderphases
BOOL bSkipSVMIntro
BOOL bAllowTripSkipNoPlay
BOOL bIdleKickPrints
BOOL bDebugPrintCustomText
BOOL bHUDHidingReasons
BOOL bGangChaseCleanup
BOOL bOverrideCCTVRotDebug
BOOL bsc3378817
BOOL bHeistHashedMac
BOOL bAutoForceRestartNoCorona
BOOL bPhoneEMPDebug
BOOL bMissionYachtDebug
BOOL bVaultPoisonGasDebug
BOOL bCasinoVaultDoorDebug
BOOL bObjectTaserDebug
BOOL bWallRappelDebug
BOOL bZoneTimerDebug
BOOL bDisableWallRappelCam
BOOL bInteractWithDebug
BOOL bHandheldDrillMinigameDebug
BOOL bCCTVDebug
BOOL bCasinoHeistBlockBranchingToDirect
BOOL bDirectionalDoorDebug
BOOL bByPassMissionVarSyncBail
BOOL bDynoPropSpam
BOOL bObjDebug
BOOL bHumaneSyncLockDebug
BOOL bCasinoTunnelRubbleDebug
BOOL bDoorCoverBlockZones
#ENDIF

// Overtime Variables.
INT iCurrentOTZone
INT iCurrentOTZoneScoreToGive = -1

INT iAirDefenseSphereMission[FMMC_MAX_NUM_ZONES]

SCRIPT_TIMER tdDefenseSphereInvulnerabilityTimer
SCRIPT_TIMER tdDefenseSphereAlphaTimer

BLIP_INDEX biPropShapeBlip[FMMC_MAX_NUM_PROPS]

CONST_INT MAX_NUM_FAKE_DOOR_PROPS 10

ENUM FAKE_DOOR_SOUND_STAGES
	FAKE_DOOR_SOUND_STAGE_LOAD,
	FAKE_DOOR_SOUND_STAGE_PLAYING,
	FAKE_DOOR_SOUND_STAGE_COMPLETE
ENDENUM

STRUCT FAKE_DOOR_PROP_STRUCT 
	INT iPropIndex = -1
	INT iStartTime = 0
	FAKE_DOOR_SOUND_STAGES eSoundStage = FAKE_DOOR_SOUND_STAGE_LOAD
ENDSTRUCT

FAKE_DOOR_PROP_STRUCT sFakeDoorProp[MAX_NUM_FAKE_DOOR_PROPS]

INT iDoorHasLinkedEntityBS

CONST_INT	LBOOLZONE_PLAYER_JUST_LEFT_DEFENSE_SPHERE					0 // + iEveryFrameZoneLoop
INT iLocalBoolCheckZone

INT iCachedResurrector = -1

CONST_INT ciCHECK_ALL_DEAD_TIME 3000
SCRIPT_TIMER tdCheckAllDeadTimer[FMMC_MAX_TEAMS]

INT iValidStaticCams = 0

CONST_INT ci_BOMB_SOUND_RELEASE					0

SCRIPT_TIMER tdOvertimeTimerDelayRuleProgression
VECTOR vCachedCheckpointEntry
CONST_INT ciOVERTIME_END_ACTIVE_TURN_TIME 1750

INT iLocalTeamTurnsRoundWonBS[2]
SCRIPT_TIMER tdTopDownSprintButtonMash
CONST_INT ci_TOP_DOWN_SPRINT_TIME			400

SCRIPT_TIMER tdLastAliveVibrationTimer
CONST_INT ciLAST_ALIVE_VIBRATE_TIME	1000
CONST_INT ciLAST_ALIVE_VIBRATE_FREQ	70
CONST_INT ciLAST_ALIVE_VIBRATE_DURATION 70

SCRIPT_TIMER tdPauseOnPlayerRuleDelay[FMMC_MAX_TEAMS]
CONST_INT ciPAUSE_ON_PLAYER_DELAY	1000

INT iPickupCollected[2]
//INT iPickupRecreated[2]

CONST_INT ciMAX_PTFX_FOR_WEAPONS 			10
PTFX_ID ptfxWeaponGlow[ciMAX_PTFX_FOR_WEAPONS]

INT iSoundIDLastAlive = -1

OBJECT_INDEX objTopDownCamCompass

VECTOR vPlayerTRPosSpec
VECTOR vPlayerTRVelSpec

INT iDelayedOvertimeShardScore
INT iDelayedOvertimeShardTeam
INT iDelayedOvertimeShardRule
INT iDelayedOvertimePartToMove
INT iDelayedOvertimeCurrentRound

SCRIPT_TIMER tdTRBigMessageStuckTimer
CONST_INT ciTRBIG_MESSAGE_STUCK_TIME 2500
SCRIPT_TIMER tdTR321StuckTimer
CONST_INT ciTR321_STUCK_TIME 7500

CONST_INT ciOVERTIME_IDLE_KICK 900000 //15 min timeout

CONST_INT ci_OVERTIME_SCORED_SHARD_TIME					3000
SCRIPT_TIMER tdOvertimeScoredShardTimer

CONST_INT ci_OVERTIME_POINTS_NEEDED_TIME				3500
SCRIPT_TIMER tdOvertimePointsNeededToStayInTheGame

CONST_INT ciOVERTIME_CAR_MID_FRONT 		0
CONST_INT ciOVERTIME_CAR_MID_BACK 		1
CONST_INT ciOVERTIME_CAR_MID_LEFT		2
CONST_INT ciOVERTIME_CAR_MID_RIGHT 		3
CONST_INT ciOVERTIME_CAR_FRONT_LEFT		4
CONST_INT ciOVERTIME_CAR_FRONT_RIGHT	5
CONST_INT ciOVERTIME_CAR_BACK_LEFT	 	6
CONST_INT ciOVERTIME_CAR_BACK_RIGHT 	7
CONST_INT ciOVERTIME_CAR_MAX	 		8

VECTOR vOvertimeCarOffsets[ciOVERTIME_CAR_MAX]
MODEL_NAMES mnOvertimeModelForOffsets = DUMMY_MODEL_FOR_SCRIPT
INT iSortedOvertimeZones[ci_OVERTIME_ZONE_MAX]

CONST_INT ci_OVERTIME_LOCATE_DELAY	2000
SCRIPT_TIMER tdOvertimeLocateDelayTimer

//SCRIPT_TIMER tdObjectControlTimerPause
INT iTimeObjectTeamControlBarCached[FMMC_MAX_TEAMS]
INT iTransformationObjectIndex = -1
INT iInventoryObjectIndex = -1
INT iTeamSlotOvertimeCache = -1

INT iOvertimeScoreCacheForRounds = 1
INT iOTPreviousPartToMove = -1
INT iOTPreviousPartChangeCounter = 0

INT iLastUsedPlacedVeh = -1

INT iOTSoundNewRound = -1

CONST_INT ci_OVERTIME_STUCK_VEHICLE 4000

SCRIPT_TIMER tdOvertimeLocateDelayForUpsideDown

SCRIPT_TIMER tdAirstrikeExplodeTimer
VECTOR vAirstrikeOffset, vAirstrikeTargetCoords
BOOL bDisableExplosionBarKill, bUseRAGValues, bAirstrikeSpheres, bAirstrikeSphereMin, bAirstrikeSphereMax
FLOAT fRAGAirstrikeMin, fRAGAirstrikeMax
SCRIPT_TIMER tdAirstrikeRocketTimer, tdAirstrikeStartTimer, tdAirstrikeSafetyTimer
INT iAirstrikeExplosionCount
INT iAirstrikeRocketFiredBitSet
CONST_INT ci_iAirstrikeStartDelay 2000
VECTOR vDialogueAirstrikeCoords

SCRIPT_TIMER tdTransformDelayTimer
CONST_INT ciTRANSFORM_DELAY_TIME 2000

SCRIPT_TIMER tdOvertimeStationaryTimer
CONST_INT ciOVERTIME_STATIONARY_TIME 1000

SCRIPT_TIMER tdWaitForScoreShardTimeOut
CONST_INT ciWAIT_FOR_SCORE_SHARD_TIME 2500

INT iMyParachuteMC = 0

INT iVehCargoCollisionDoneBS

FLOAT fLightIntesityScale = -1
INT iTimerCachedOTPartToMove = -1


CONST_INT ciGRANULAR_CAPTURE_TIME 1000
SCRIPT_TIMER tdGranularCaptureTimer

INT iGranularPointsThisFrame

CONST_INT ciWARP_DELAY_TIME 500
SCRIPT_TIMER tdInitWarpDelay

CONST_INT ciPLAYER_TELEPORT_FAIL_SAFE_TIME 3000
SCRIPT_TIMER tdPlayerTeleportFailSafe

CONST_INT ciOVERTIME_RENDERPHASE_UNPAUSE_FOR_SPECTATORS 5000
SCRIPT_TIMER tdOvertimeRenderphaseUnpauseForSpectators
SCRIPT_TIMER tdOvertimeRenderphaseUnpauseForRumbleDeath

CONST_INT ciOVERTIME_RENDERPHASE_PAUSE_START_SPEC 450
CONST_INT ciOVERTIME_RENDERPHASE_PAUSE_START 1000
SCRIPT_TIMER tdOvertimeRenderphasePauseStart

CONST_INT ciOVERTIME_CLEAR_AREA_TIME					1000
SCRIPT_TIMER tdOvertimeClearAreaTimer
INT iOvertimeClearAreaCounter =0 

INT iDisableVehicleWeaponBitset = 0
INT iDisableVehiclColdmgBitset = 0
INT iShouldProcessVehiclColdmgBitset = 0


CONST_INT ciPOWERMAD_SUDDENDEATH_SCORE_TOLERANCE 3

#IF IS_DEBUG_BUILD
	SCRIPT_TIMER tdLoadAndCreateTimer
	BOOL bPrintedLoadTime = FALSE
#ENDIF

INT iBonusXP

VEHICLE_INDEX spawnVehicleCache

//CONST_INT BLIMP_ASSIGN_RENDER_TARGETS 	0
//CONST_INT BLIMP_LOAD_SCALEFORM			1
//CONST_INT BLIMP_INIT					2
//CONST_INT BLIMP_DRAW					3
//CONST_INT BLIMP_CLEANUP					4
//
//STRUCT BLIMP_SIGN
//	INT iRenderTarget = -1
//	INT iState = BLIMP_ASSIGN_RENDER_TARGETS
//	SCALEFORM_INDEX siBlimp
//	BOOL bBlimpExists
//	INT iOTPlayerNamePart = -1
//ENDSTRUCT
//
//FLOAT fBlimpCentreX = 0.0
//FLOAT fBlimpCentreY = -0.08
//FLOAT fBlimpWidth = 1
//FLOAT fBlimpHeight = 1.7
//
//BLIMP_SIGN sBlimpSign

INT iVehConsideredBS

SCRIPT_TIMER tdOvertimeFlowPlayStuckTimer
CONST_INT ciOVERTIME_FLOW_PLAY_STUCK_TIME 7500

SCRIPT_TIMER tdCutsceneEventDelay
CONST_INT ciCUTSCENE_EVENT_DELAY 500

INT iIndexOfVehPlayerIsIn = -1

CONST_INT ciMOC_COUNTDOWN_STOP_TIME		500
SCRIPT_TIMER tdMOCCountdownStopTimer

SCRIPT_TIMER tdCargobobDoorBlockTimer
CONST_INT ciCARGOBOB_DOOR_BLOCK_TIME		3000

INT iBlimpScoreTeam1
INT iBlimpScoreTeam2

CONST_INT ciCOVERTIME_VEHICLE_REMOVAL_TIME		2000
SCRIPT_TIMER tdOvertimeVehicleRemovalTimer

INT iCachedParachuteSmokeRValue
INT iCachedParachuteSmokeGValue
INT iCachedParachuteSmokeBValue

SCRIPT_TIMER tdVehicleBlipFlash[FMMC_MAX_VEHICLES]
SCRIPT_TIMER tdPedBlipFlash

CONST_INT ci_RUNNING_BACK_REMIX_DELAY_RESTART			1500
SCRIPT_TIMER tdRunningBackRemixDelayRestartTimer

CONST_INT ci_OVERTIME_DELAY_RESTART						2500
SCRIPT_TIMER tdOvertimeDelayRestartTimer

CONST_INT ci_OVERTIME_FAIL_SAFE_RENDERPHASE_FORCE_THROUGH 10000
SCRIPT_TIMER tdOvertimeFailSafeRenderphaseForceThrough

CONST_INT ci_OVERTIME_FAIL_SAFE_VEHICLE_FORCE_THROUGH 10000
SCRIPT_TIMER tdOvertimeFailSafeVehicleForceThrough

FLOAT fCachedHealthRechargeMaxPercent

INT iLocalObjAtHolding

VEHICLE_SWAP_DATA sVehicleSwap

SCRIPT_TIMER tdCircleHUDPulseTimer
CONST_INT ci_CIRCLE_HUD_PULSE_TIME 1000

PTFX_ID StockpileSmokeTrail
SCRIPT_TIMER SmokeTrailTimer
CONST_INT ci_SMOKE_TRAIL_TIMER	500

FLOAT fCondemnedTeamScore[FMMC_MAX_TEAMS]
INT iCondemnedScoreUpdate = 0
INT iCondemnedTeamCache = -1
SCRIPT_TIMER ptCondemnedTeamSwitchTimer
SCRIPT_TIMER ptCondemnedTeamBarSyncTimer
SCRIPT_TIMER ptCondemnedBlipFlashTimer
SCRIPT_TIMER ptCondemnedStatusDelayTimer
SCRIPT_TIMER ptCondemnedControllerVibrationTimer
SCRIPT_TIMER ptCondemnedWeaponSwitchTimer
BOOL bCondemnedLocalOutfitNeedsUpdate = FALSE
BOOL bCondemnedLocalWeaponNeedsUpdate = TRUE
INT iCondemnedSoundID = -1
BOOL bCondemnedHeartbeatPlaying = FALSE
CONST_INT ci_CONDEMNED_TEAM_BAR_SYNC_PERIOD 500
#IF IS_DEBUG_BUILD
	BOOL bDebugCondemnedNextAlwaysFlash = FALSE
#ENDIF


CONST_INT ci_VEHICLE_BLIP_RULE_DEFAULT -2
CONST_INT ci_VEHICLE_BLIP_RULE_ALWAYS_OFF -1
CONST_INT ci_VEHICLE_BLIP_RULE_ALWAYS_ON 0

CONST_INT ci_DISABLE_LOCATE_RECAPTURE_TIME		2500
SCRIPT_TIMER tdDisableLocateRecaptureTimer[NUM_NETWORK_PLAYERS]
INT iLocateFromPreviousFrame = -1

VEHICLE_INDEX vehOldTeamMultiRespawnVehicle
VEHICLE_INDEX vehOurMultiSwitchVehicle

INT iCachedPartOnTeamVehicleRespawn = -1
//INT iCachedNewVehicleDriver = -1
INT iCachedNewVehSeatPref = -3
INT iCurrentRoundCache = -1
CONST_INT ci_VEH_SEAT_SWAP_CONSENT_TIME		7000
SCRIPT_TIMER tdVehicleSeatSwapConsentTimer
CONST_INT ci_VEH_SEAT_SWAP_INVALID_TIME		5000

CONST_INT ci_VEH_SEAT_SWAP_SAFETY_TIME		3500
SCRIPT_TIMER tdVehicleSeatSwapSafetyTimer

CONST_FLOAT cfMIN_MOVING_VEH_SPEED 5.0

CONST_INT ci_VEH_SEAT_SWAP_COOLDOWN_TIME	4000
SCRIPT_TIMER tdVehicleSeatSwapCooldownTimer

INT iNearestVehStartSeatCached = -3
BLIP_INDEX biTaggedEntity[FMMC_MAX_TAGGED_ENTITIES]
SHAPETEST_INDEX stiTagShapeTest
INT iTagShapeHitSomething
ENTITY_INDEX TagTestEntity
//INT iLocalRespawnVehReloop

CONST_INT ciStockpile_Sound_Collect			 0
CONST_INT ciStockpile_Sound_Delivery		 1
CONST_INT ciStockpile_Sound_Dropped			 2

//Props to fix bug 3729570
CONST_INT ciPacificStandardWallBlockers		4
OBJECT_INDEX oi_pacificStandardWallBlockers[ciPacificStandardWallBlockers]
INT iManRespawnLastRadioStation = 255

INT iRespVehCannotColDmgEntity

SCRIPT_TIMER tdButtonHeldTimer

INT iVehLastInsideGrantedInvulnability = -1
INT iDetachedObjectSuccessfully
INT iFailedAttemptsSNPV = 0
INT iFailedFailSafeAttemptsSNPV = 0

CONST_INT ci_FAIL_SAFE_ATTEMPTS_MAX				100
CONST_INT ci_FAIL_SAFE_ATTEMPTS_RESETS_MAX		3

INT iNumberLocsChecked
INT iCapturePointBroadcastBitSet

SCRIPT_TIMER td_MissedShotTakeDamageTimer
SCRIPT_TIMER td_CameraShakingOnRuleTimer

INT iNumRebreathersFromFreemode

VEHICLE_INDEX vehAirQuotaLocalVehicle
PTFX_ID ptfxAirQuotaLastKillEffect
INT iTransformRadioStation = -1
BOOL bReadyToSwap = FALSE

PTFX_ID ptfxFlickeringInvisiblePedEffects[FMMC_MAX_PEDS]

INT iColourTeamCacheGotoLocateOld[FMMC_MAX_GO_TO_LOCATIONS]
INT iCaptureLocateRed[FMMC_MAX_GO_TO_LOCATIONS]
INT iCaptureLocateGreen[FMMC_MAX_GO_TO_LOCATIONS]
INT iCaptureLocateBlue[FMMC_MAX_GO_TO_LOCATIONS]
INT iCaptureLocateAlpha[FMMC_MAX_GO_TO_LOCATIONS]

ENUM eCaptureLocateStateEnum
	eCapture_neutral,
	eCapture_captured,
	eCapture_contested
ENDENUM

PTFX_ID ptfxCapScanEffect
INT iCapScanSfx = -1

eCaptureLocateStateEnum eCaptureLocateState[FMMC_MAX_GO_TO_LOCATIONS]
eCaptureLocateStateEnum eCaptureLocateStateLastFrame[FMMC_MAX_GO_TO_LOCATIONS]
INT iLocPreContestOwnerTeam[FMMC_MAX_GO_TO_LOCATIONS]

CONST_INT ciPED_AGGRO_RULE_SKIP_DELAY 3500

HACKING_CONTROLLER_STRUCT sBeamHack[FMMC_MAX_NUM_OBJECTS]

HACKING_CONTROLLER_STRUCT sHotwire[FMMC_MAX_NUM_OBJECTS]

HG_CONTROL_STRUCT sFingerprintClone[FMMC_MAX_NUM_OBJECTS]
FINGERPRINT_MINIGAME_GAMEPLAY_DATA sFingerprintCloneGameplay

HG_CONTROL_STRUCT sOrderUnlock[FMMC_MAX_NUM_OBJECTS]
ORDER_UNLOCK_GAMEPLAY_DATA	sOrderUnlockGameplay

#IF IS_DEBUG_BUILD
WIDGET_GROUP_ID FMMC_WIDGET_GROUP
#ENDIF
INT iFlightModeEnabledForVeh
INT iHalloweenSoundID = -1

INT iHostileTakeoverInZoneCount[FMMC_MAX_GO_TO_LOCATIONS][FMMC_MAX_TEAMS]
SCRIPT_TIMER tdHostileTakeoverEndDelay
CONST_INT ciHTEndDelay 1500
INT iHostileTakeoverContestedBS

CONST_INT ciKeepVehicleInBoundsTimer 15000
SCRIPT_TIMER tdKeepVehicleInBounds[FMMC_MAX_VEHICLES]

INT iPlayerHardTargetBit
INT iSpawnRandomHeadingToSelect
INT iStaggeredVehLoopCounterPostCreationCalls

INT iPreviousVehHack = -1

CONST_INT ci_ChangeOutfitAppearance 2000
SCRIPT_TIMER tdChangeOutfitAppearance

BOOL bHasLaunchedFakeMissionOrbitalCannon = FALSE

//INT iObjTooCloseToFireBitSet

//SCRIPT_TIMER tdPersonVehicleWarp
SCRIPT_TIMER tdHardTargetSwitchDelay
CONST_INT ci_HARD_TARGET_SWITCH_DELAY 350

INT iInteractObject_AttachedVehicle[FMMC_MAX_NUM_OBJECTS]

INT iExplosionDestroyedObjectBitSet

SCRIPT_TIMER tdEndingTimer
CONST_INT ciEndingTimerLength 1000

CONST_INT ci_SubExplosion_BigExplosionTime			9150
CONST_INT ci_SubExplosion_RandomExplosionStartTime	11111
CONST_INT ci_SubExplosion_Explosion1Time			3840
CONST_INT ci_SubExplosion_Explosion2Time			4620
CONST_INT ci_SubExplosion_Explosion3Time			5640
CONST_INT ci_SubExplosion_Explosion4Time			6620
CONST_INT ci_SubExplosion_TimeBeforeExploding		9999

CONST_FLOAT cf_SubExplosion_Range_Forward	44.0
CONST_FLOAT cf_SubExplosion_Range_Sideways	10.0
SCRIPT_TIMER stSubExplosion_Timer

SCRIPT_TIMER stSubExplosion_RandomExplosions_Timer
CONST_INT ci_SubExplosion_MaxTime	1500

SCRIPT_TIMER stSubExplosion_Preexplosion_Timer
FLOAT fCurSubSpeed = 0.05
ENTITY_INDEX eiSubmarineProp
INT iSubExplosionBS = 0
PTFX_ID ptfx_SubmarineSplashes
CONST_INT iSE_Explosion1			0
CONST_INT iSE_Explosion2			1
CONST_INT iSE_Explosion3			2
CONST_INT iSE_Explosion4			3
CONST_INT iSE_BigExplosion			4
CONST_INT iSE_DoRandomExplosions	5
CONST_INT iSE_SequenceStarted		6
CONST_INT iSE_SequenceDone			7
CONST_INT iSE_PlayedStream			8
CONST_INT iSE_SwappedModel			9

SCALEFORM_INDEX sfiDownloadScreen

SCRIPT_TIMER timerResetWantedLevelInStealth

INT iVehStealthMode_MC_BS
CONST_INT	ci_VehStealthMode_HasDisabledLockOn			0
CONST_INT	ci_VehStealthMode_ActiveBeforeManualRespawn	1

PTFX_ID ptfx_PropFadeoutEveryFrameIndex[FMMC_MAX_NUM_FADEOUT_PROPS_EVERY_FRAME]
INT iPropFadeoutEveryFrameIndex[FMMC_MAX_NUM_FADEOUT_PROPS_EVERY_FRAME]
INT iFlashToggle[FMMC_MAX_NUM_FADEOUT_PROPS_EVERY_FRAME]
INT iFlashToggleTime[FMMC_MAX_NUM_FADEOUT_PROPS_EVERY_FRAME]
INT iHardTargetCountdownTimerSoundID = -1
SCRIPT_TIMER tdFlashToggleDissolvePTFX[FMMC_MAX_NUM_FADEOUT_PROPS_EVERY_FRAME]

CONST_INT ci_CleanUpPropEF_BS_ColourNotSet					0
INT iCleanUpPropEF_BS[FMMC_MAX_NUM_FADEOUT_PROPS_EVERY_FRAME]

CONST_INT ci_FlashToggleDissolveTime			1000
CONST_INT ci_FlashToggleDissolveTimeCleanup		3000

PTFX_ID ptfx_cargoplane_moving_clouds
PTFX_ID ptfx_cargoplane_wind_and_smoke

OBJECT_INDEX oiHangarWayfinding

SCRIPT_TIMER stHackPhoneTimer

BOOL bShowingAltitudeLimit = FALSE

#IF IS_DEBUG_BUILD
FLOAT fWingDebug = 0.5
VECTOR vRagPlacementVector
VECTOR vRagRotationVector
#ENDIF

STRUCT MISSION_DEFUNCT_BASE_BODY_SCANNER_DATA
	VECTOR vAudioSource
	VECTOR vMin
	VECTOR vMax
	FLOAT fWidth	  	 	= 1.75
ENDSTRUCT
INT iTempHackingTargetsRemaining

FLOAT fTempBurningVehFireDamageThisLoop[FMMC_MAX_VEHICLES]
FLOAT fLocalFireDamageDealt[FMMC_MAX_VEHICLES]
FLOAT fLocalFireDamageDealtLastFrame[FMMC_MAX_VEHICLES]
FLOAT fCurrentMaxFireDamage[FMMC_MAX_VEHICLES]
CONST_FLOAT cfMAX_FIRE_DAMAGE_UNTIL_APPLICATION 1.0
DECAL_ID di_RiotVanDecals[FMMC_MAX_VEHICLES]

MISSION_DEFUNCT_BASE_BODY_SCANNER_DATA sMissionBodyScannerData
SCRIPT_TIMER tdMissionBodyScannerTimer
INT iPlayersInsideSeatPrefOverrideVeh
INT iVehSeatPrefOverrideVeh = -1
INT iPlayersInsideSeatPref = -3

CONST_INT ciFULL_TEAM_BITSET 15

VECTOR vSubmarineInteriorCoords = <<2047.0, 2942.0, -61.9>>
VECTOR vServerFarmInteriorCoords = <<2168, 2920, -84>>
VECTOR vSiloTunnelEntranceInteriorCoords = <<1262.4, 4821.7, -39.1>>
VECTOR vHangarInteriorCoords = <<-1266.8022, -3014.8491, -49.4903>>

STRING sLastShardText

BOOL bAvengerHoldTransitionBlocked
INT iAvengerHoldTransitionBlockedCount[FMMC_MAX_TEAMS]
SCRIPT_TIMER tdAvengerHoldUnlockTimer

INT iEmailBS = 0

INT iFTTimeElapsedLastFrame
INT iPedAliveChecked[ciFMMC_PED_BITSET_SIZE]
INT iCurrentPlacedPedsAlive
INT iModifiedTimeElapsed[FMMC_MAX_TEAMS]
INT iFTCurrentTimerProgress[FMMC_MAX_TEAMS]
SCRIPT_TIMER tdUpdateProgressBarChoppy

INT iPlayedDialogueForVehHackBS = 0

// IAAJ_EXT cutscene vars
BOOL bIAAJ_EXTDrawProbabilityGraphic = FALSE
BOOL bIAAJ_EXTDrawBogdanGraphics = FALSE
BOOL bIAAJ_EXTInited = FALSE
INT iIAAJ_EXTRenderTargetIDs[2]

INT iSoundIDServerFarmAlarm = -1
INT iSoundIDSubmarineAlarm = -1

INT iScriptedEntitiesRegsitered
CONST_INT SCRIPTED_CUTSCENE_END_FADE_DURATION 	500
CONST_INT MOCAP_END_FADE_DURATION 				500

CONST_INT MOCAP_START_FADE_IN_DURATION_QUICK				500
CONST_INT MOCAP_START_FADE_IN_DURATION_NORMAL				1500

//Juggernaut Intro Cutscene
CONST_INT ci_JuggernautsInCutscene	3
ENTITY_INDEX piCutsceneJuggs[ci_JuggernautsInCutscene]
ENTITY_INDEX piCutsceneJuggs_Guns[ci_JuggernautsInCutscene]

ENUM JUGGERNAUT_CUTSCENE_STATE
	JCS_NONE,
	JCS_FLICKER,
	JCS_FADEOUT,
	JCS_DONE
ENDENUM
JUGGERNAUT_CUTSCENE_STATE jcs_CurCutsceneState[ci_JuggernautsInCutscene]
FLOAT fCutsceneJugg_MaxOpacity[ci_JuggernautsInCutscene]
TWEAK_FLOAT cf_CutsceneJugg_OpacityFadeSpeed 	180.0

//End of Juggernaut Intro Cutscene

CONST_INT ciTimeBeforeDownloadSound		6000
SCRIPT_TIMER stDownloadSoundTimer

INT iBeamHack_PrevPacketBS = 0
INT iBeamHack_PrevLivesRemaining = 0
INT iFingerprintClone_PrevPatternDone = 0
INT iFingerprintClone_PrevLivesLost = 0
INT iOrderUnlock_PrevPatternDone = 0
INT iOrderUnlock_PrevLivesLost = 0
INT iFingerprintKeypadFailedOnce = 0
INT iFingerprintKeypadFailedTwice = 0
INT iFingerprintKeypadQuitOnce = 0

SCRIPT_TIMER tdTimeHackingTimer
INT iSiloAlarmLoopSound = -1

BOOL bIsDisplacedInteriorSet = FALSE
INT iCommonDisplacementInterior = -1
SCRIPT_TIMER tdEmptyAvengerTimer
CONST_INT ciForceAvengerCrash 5000

INT iPrevCurrentLoc = -1

INT iZoneRocketClearToFireBS = 0

INT iHackSuccessSoundPlayedBS = 0
INT iHackSuccessSoundProgress = 0

VECTOR vBarragePlacementVector = <<0,0,0>>

SCRIPT_TIMER tdTimer_ClearCustomVehicleNodesHeist2

SCRIPT_TIMER stSoundBankBail
CONST_INT	ciSoundBankBailTime		10000

SCRIPT_TIMER tdRestartRenderPhaseBailTimer
CONST_INT ciRESTART_RENDERPHASE_BAIL_TIME 10000

SCRIPT_TIMER tdEveryFrameDropOffDelay[FMMC_MAX_NUM_OBJECTS]
CONST_INT ciEVERY_FRAME_DROP_OFF_DELAY_TIME 2000
SCRIPT_TIMER tdDropOff
CONST_INT iDropTimer 300

WEAPON_TYPE wtPreCutsceneWeapon


//SHOWDOWN
FLOAT fShowdownPointDrainCounter = 0
INT iShowdown_LastDamagerPart = -1
FLOAT fShowdown_DisplayPoints = 0.0 //The number of points used in the HUD - this is separate from the value in playerBD so that this can be smoothed and used to figure out if you are currently losing or gaining points

SCRIPT_TIMER stShowdown_BarSwitchTimer
HUD_COLOURS hcShowdown_BarColour

SCRIPT_TIMER stShowdown_FirstFifteenSecondsTimer
CONST_INT ciShowdown_TimeBeforeDrain	15000

SCRIPT_TIMER stShowdown_IdleTimer
CONST_INT ciShowdown_IdleTimeAllowed	5000
CONST_FLOAT cfShowdown_IdleSpeed		3.50

//VENETIAN JOB
FLOAT fRedTimerTime = 0.0
CONST_INT ciMinimumTimeToReduceTo 30000

VEHICLE_INDEX viVespucciRemixVehicle = NULL

//Widgets
#IF IS_DEBUG_BUILD
BOOL bShowdown_TeamDebugActive = FALSE
BOOL bShowdown_DisableDrain = FALSE
BOOL bShowdown_DisableIdleDrain = FALSE
VECTOR vShowdownDEBUG_StartPos = <<0.01, 0.140, 0.0>>
VECTOR vShowdownDEBUG_Offsets = <<0.2, 0.02, 0.0>>
#ENDIF

ENUM SHOWDOWN_HUD_STATE
	SHOWDOWN_HUD_OFF,
	SHOWDOWN_HUD_LOADING,
	SHOWDOWN_HUD_SETUP,
	SHOWDOWN_HUD_RUNNING,
	SHOWDOWN_HUD_CLEANUP
ENDENUM
SHOWDOWN_HUD_STATE eShowdownHUDState

SCRIPT_TIMER tdPropColourChangeDelayTimer
SCRIPT_TIMER tdClearCoverTaskProp
INT iRadialPulseCounter = 0
FLOAT fRadialPulsTimePassed
FLOAT fRadialPulsTimeMax
BOOL bShowRadialPuse = FALSE
SCRIPT_TIMER tdFallingTimer
CONST_INT ci_COVER_TIME_FOR_SHARD	 1500
SCRIPT_TIMER tdCoverTimerForShard

BLIMP_SIGN sBlimpSign

INT iSafeBlippedProp = -1
BLIP_INDEX blipSafeProp

INT iAlphaResetAttempts

SCRIPT_TIMER stMinSpeedSndCooldown

CONST_INT ciMax_Number_Of_Visible_Bombs		10
NETWORK_INDEX niMyCargoBombs[ciMax_Number_Of_Visible_Bombs]
OBJECT_INDEX oiLargePowerups[FMMC_MAX_WEAPONS]

CONST_INT ciHUDHIDDEN_NOT_HIDDEN					-1
CONST_INT ciHUDHIDDEN_SPECTATORHUDHIDDEN			0
CONST_INT ciHUDHIDDEN_DIVE_SCORE					1
CONST_INT ciHUDHIDDEN_RENDERPHASE_PAUSED			2
CONST_INT ciHUDHIDDEN_RESPAWN_BACK_AT_START_LOGIC	3
CONST_INT ciHUDHIDDEN_MANUALLY_HIDDEN_HUD			4
CONST_INT ciHUDHIDDEN_INTERACT_WITH_DOWNLOAD		5
CONST_INT ciHUDHIDDEN_BEAMHACK						6
CONST_INT ciHUDHIDDEN_HOTWIRE						7
CONST_INT ciHUDHIDDEN_GENERAL_CHECKS_1				8
CONST_INT ciHUDHIDDEN_GENERAL_CHECKS_2				9
CONST_INT ciHUDHIDDEN_BOMBFB_TRANSITIONING			10
CONST_INT ciHUDHIDDEN_FINGERPRINT_CLONE				11
CONST_INT ciHUDHIDDEN_ORDER_UNLOCK					12
CONST_INT ciHUDHIDDEN_ELEVATOR						13

CONST_INT ciHuntingPackRemix_RUNNER					2

INT iHUDHiddenReason
INT iRadarHiddenFrame
PED_WEAPONS_MP_STRUCT pwsCachedPedWeapons
INT iMK2ShotgunAmmo
AMMO_TYPE aAmmoType
SCRIPT_TIMER tdTPRAppearanceUpdateDelay
SCRIPT_TIMER tdRBSuddenDeathActivation
INT iVisualAidCachedTeam[MAX_NUM_MC_PLAYERS]
INT iPartNumbersArrayedByTeamOrder[FMMC_MAX_TEAMS][NUM_NETWORK_PLAYERS]

#IF IS_DEBUG_BUILD
VECTOR vDebugForDiveCamStartPos
VECTOR vDebugForDiveCamStartEndPos
#ENDIF

CONST_INT ci_PLACED_VEHICLE_CYCLE_WAIT_TIMEOUT			5000
SCRIPT_TIMER tdPlacedVehicleCycleWaitTimeout
CONST_INT ci_SCORE_AND_LAP_SYNC_TIMER			3500
SCRIPT_TIMER tdScoreAndLapSyncTimer

CONST_INT ci_TIMEOUT_FOR_PLACED_VEHICLE_WAIT_RESTART	5000
SCRIPT_TIMER tdTimeoutForPlacedVehicleWaitRestart
SCRIPT_TIMER tdRunningBestTimer
CONST_INT ci_EXPLODE_VEHICLE_FOR_RUNNING_BACK		5000
SCRIPT_TIMER tdExplodeVehicleForRunningBack

CONST_INT ci_MultiRuleTimerResyncLimit 		60000
CONST_INT ci_MaxVehicleExtras 				5
INT iCurrentRunnerPart = -1
INT iCurrentRunnerPlayer = -1
TEXT_LABEL_63 tlCurrentRunnerPlayerName = ""

SCRIPT_TIMER stWantedDeathCooldownTimer
CONST_INT ciWantedDeathCooldownTimer_Length		5000

INT iPlayerBlipRangeStaggeredIndex = 0
INT iRoundRestartLogicRadio = 255

SCRIPT_TIMER stStopLocationContinueTimer
CONST_INT ciStopLocationContinue_Time		3000

//Smash Force Vars
SCRIPT_TIMER timerSmashForce
SCRIPT_TIMER timerSmashForceHit
CONST_INT ciTimeToBeHitAgain 1000
CONST_FLOAT cfDeathZoneSmashInput 0.8
BOOL bUsingSmashPower = FALSE
CONST_FLOAT cfMinAngleToForwardSmash 25.0
INT iSmashPickup = -1

SCRIPT_TIMER timerExtraVehDamageForceHitting	// Me hitting someone
CONST_INT ciTimeExtraVehDamageToBeHittingAgain 1000

MODEL_NAMES mnSuppressedVehicles[MAX_NUM_SUPRESSED_MODELS]
INT iCamRenderingChecks = 0


SCRIPT_TIMER tdStuntingPackDelay

INT iPrevValidRule

INT iStuntRPEarned = 0
SCRIPT_TIMER tdEffectDelayBeforeCamTimer

TARGET_FLOATING_SCORE sFloatingScore[TARGET_MAX_FS]
INT iLastGoToLocateHit = -1

INT iLocalGhostRemotePlayerBitSet
INT iObjectTransformVehicle = -1

CONST_INT ci_TAG_OUT_COOLDOWN_TIME							30000

CONST_INT ci_START_WARNING_FOR_TOO_MANY_PLAYERS_INBOUNDS	700
CONST_INT ci_EXPLODE_FOR_TOO_MANY_PLAYERS_INBOUNDS			6000
SCRIPT_TIMER tdTooManyPlayersInBoundsTimer
INT iPlacedVehicleIAmIn = -1
CONST_INT ci_REGEN_VEHICLE_AFTER_THIS_TIME					1500
SCRIPT_TIMER tdVehicleRegenTimer
INT iRegenCapFromBounds

SCRIPT_TIMER stBmbFB_ExplosionDelayTimer
INT iBombStaggeredIndex
INT iTeamGoalsThisMiniRound[FMMC_MAX_TEAMS]
//INT iTeamExplosionsThisMiniRound[FMMC_MAX_TEAMS]
INT iTeamPotentialExplosionsThisMiniRound[FMMC_MAX_TEAMS]
INT iBmbFBCurrentMiniRound = 0

INT iGoalA, iGoalB

//TRAPS
FMMC_ARENA_TRAPS_LOCAL_INFO sTrapInfo_Local

ARENA_SOUNDS_STRUCT sArenaSoundsInfo

CONST_INT ci_STOLEN_VEH_SWAP_COOLDOWN_TIME					3000
//SCRIPT_TIMER tdStolenVehSwapCooldownTimer
//INT iObjectJustHadStolen = -1

INTERIOR_INSTANCE_INDEX /*interiorArena,*/ iArenaInterior_VIPLoungeIndex
CONST_INT ciExtraArenaTime 3000
SCRIPT_TIMER tdExtraArenaTimer

INT iServerTagTeamPartTimeTaggedNonServerCache[FMMC_MAX_TEAMS]

//BLIP_INDEX biEnemyFlagCarrierBlip
//PED_INDEX piEnemyFlagCarrier
SCRIPT_TIMER tdFlagCaptureShardTimer
	
INT iShuntObjToDrop = -1
OBJECT_INDEX oiShuntObjToDrop
ENUM eSHUNT_DROP_STATE
	eShuntDrop_IDLE,
	eShuntDrop_DROP,
	eShuntDrop_RETURN,
	eShuntDrop_REVALIDATEPICKUP,
	eShuntDrop_CLEANUP
ENDENUM
eSHUNT_DROP_STATE eShuntDropState = eShuntDrop_IDLE
SCRIPT_TIMER tdShuntObjDropTimer
CONST_INT ciShuntObjDropTimeLength 3000

SCRIPT_TIMER tdBoundsTimerDelay1
SCRIPT_TIMER tdBoundsTimerDelay2

SCRIPT_TIMER tdTagTeamFailSafeAreaOccupied
SCRIPT_TIMER tdTagTeamFailSafeAreaNotOccupied

CONST_INT ciTHPSTimer 20000
INT iStuntScoreBitset
BOOL bStuntLanded
ENUM eStuntTrick
	eStuntTrick_Frontflip,
	eStuntTrick_Backflip,
	eStuntTrick_Spin360,
	eStuntTrick_Spin540,
	eStuntTrick_Spin720,
	eStuntTrick_Spin900,
	eStuntTrick_Roll,
	eStuntTrick_Wheelie,
	eStuntTrick_Hoop
ENDENUM

SCRIPT_TIMER tdExplodePlayerZoneTimer[FMMC_MAX_NUM_ZONES]

CONST_INT ci_MaxTagTeamBlips	8
BLIP_INDEX biTagTeamGarageBlip[ci_MaxTagTeamBlips]
INT iGarageLocLink[ci_MaxTagTeamBlips]

ARENA_CONTESTANT_TURRET_CONTEXT arenaTurretContext

SCRIPT_TIMER tdLocalLocateDelayTimer

INT iTDTowersRemainingLocal[FMMC_MAX_TEAMS]
INT iTDTowersHealthRemainingLocal[FMMC_MAX_TEAMS]
INT iTDTowersHealthRemainingMaxLocal[FMMC_MAX_TEAMS]

FLOAT fVehicleWeaponShootingTime = 2.5
FLOAT fVehicleWeaponTimeSinceLastfired = 2.0
BOOL bRegenVehicleWeapons = FALSE

PLAYER_INDEX piTaggedInPlayer
BLIP_INDEX biTrapCamBlip[FMMC_MAX_NUM_DYNOPROPS]
INT iCTFLastLosingTeam = -1
SCRIPT_TIMER td_ArenaWarsBigAir
INT iFakeAmmoRechargeSoundID = -1
SCRIPT_TIMER tdDogfightSafety
SCRIPT_TIMER td_RestartClearAreaTimer
SCRIPT_TIMER td_BoundsTimerResetDelay1
SCRIPT_TIMER td_BoundsTimerResetDelay2

INT iInteriorInitializingExtrasBS[INTERIOR_BITSETS]
MISSION_FACTORIES sMissionFactories
WARP_PORTAL_CLIENT_STRUCT sWarpPortal

YACHT_DATA sMissionYachtData
FMMC_YACHT_VARS sMissionYachtVars

TIME_DATATYPE timeNewsFilterNextUpdate
SCALEFORM_INDEX sfFilter
CONST_INT ci_MCFILTER_BIT_REFRESH_FILTER 0
CONST_INT ci_MCFILTER_BIT_REFRESH_NEWS_FILTER 1

FLOAT fNewsFilterScrollEntry
INT iFilterChangeTime
INT iFilterBitSet
INT iFilterStage = -1
SPEC_HUD_FILTER_LIST eCurrentFilter
INT screenRT
SPEC_HUD_FILTER_LIST eDesiredFilter
INT iSoundChangeFilter
SCRIPT_TIMER tdRandomClothingAnimDelay

// Carnage Bar Local Vars - Start
SCRIPT_TIMER tdCarnageBar_LookingAtCarResetTimer
SCRIPT_TIMER tdCarnageBar_FillIncrementTimer
SCRIPT_TIMER tdCarnageBar_MoveOnRuleTimer
SCRIPT_TIMER tdCarnageBar_BigAirTimer
FLOAT fCarnageBar_BarFilledHeliHUD 											// Interp to fCarnageBar_BarFilledHeliTarget
FLOAT fCarnageBar_BarFilledCarHUD 											// Interp to fCarnageBar_BarFilledCarTarget
// Carnage Bar Local Vars - End

INT iZoneBS_ActivatedRemotely
INT iZoneBS_PlayerPedEntered
INT iObjectHiddenBS[FMMC_MAX_NUM_ZONES]
INT iVehBlipFlashActiveBS
INT iVehBlipFlashExpiredBS
INT iVehHasBeenDamagedOrHarmedBS

SCRIPT_TIMER tdEndMissionStopMusicDelay

INT iCCTVCameraSpottedPlayerHelpTextBS
INT iCCTVCameraSpottedPlayerBS
INT iCCTVCameraSpottedBodyBS
INT iCCTVCameraSpottedPlayer[MAX_NUM_CCTV_CAM]
INT iSoundAlarmCCTV[MAX_NUM_CCTV_CAM]
INT iSoundAlarmType[MAX_NUM_CCTV_CAM]

CONST_INT ci_IPL_INTERIOR_LOAD_SAFETY_TIMEOUT 20000
CONST_INT ci_MAX_INTERIORS_TO_LOAD 10
INT iInteriorInitializedBS = 0

SCRIPT_TIMER tdVehicleWantedDelayTimer
SCRIPT_TIMER tdInitialIPLSetupTimer
SCRIPT_TIMER tdInitialIPLSetupSafetyTimeout
INT iVehicleHasBeenUnlockedBS
INT iVehicleHasBeenLockedBS

INT iObjectiveVehicleAttachedToCargobobBS
INT iVehicleShouldWarpThisRuleStart
INT iVehicleWarpedOnThisRule
INT iOldMissionVehicleNeededToRepair = -1

INT iExplodeNearbyRocketsIterator = 0
CONST_INT ciNearbyRocketsListSize		15
ENTITY_INDEX eiNearbyRocketsList[ciNearbyRocketsListSize]
//ENTITY_INDEX eiPreviousNearbyRocketAddedToList
INT iExplodeNearbyRocketsListIndex = 0

INT iRenderPhasePauseDelay = 0
INT iRenderPhaseUnPauseDelay = 0
SCRIPT_TIMER tdRenderPhaseUnPauseDelay

INT iPedCompletedSecondaryGotoBS[FMMC_MAX_PEDS_BITSET]

FLOAT fPrizedVehicleHeading = 0.0
VEHICLE_INDEX vehPrizedVehicle
OBJECT_INDEX objPrizedPodium

COVERPOINT_INDEX cpPlacedCoverPoints[FMMC_MAX_NUM_COVER]

CONST_INT FMMC_MAX_PED_SCENE_SYNCED_PEDS 6

ENUM PED_SYNC_SCENE_STATE
	PED_SYNC_SCENE_STATE_INIT = 0,
	PED_SYNC_SCENE_STATE_PROCESS_START,
	PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT,	
	PED_SYNC_SCENE_STATE_CLEANUP,
	PED_SYNC_SCENE_STATE_PROCESS_END
ENDENUM


CONST_INT ci_MissionPedBS_iSyncSceneAnimationFinished			0
// free
CONST_INT ci_MissionPedBS_iSyncSceneLoadedBreakoutDialogue		2
CONST_INT ci_MissionPedBS_iSyncScenePlayedBreakoutDialogue		3
CONST_INT ci_MissionPedBS_iSyncSceneLoadedIntroDialogue			4
CONST_INT ci_MissionPedBS_iSyncScenePlayedIntroDialogue			5
CONST_INT ci_MissionPedBS_iSyncSceneLoadedEndDialogue			6
CONST_INT ci_MissionPedBS_iSyncScenePlayedEndDialogue			7
CONST_INT ci_MissionPedBS_iSyncSceneRequestRuleProgress			8
CONST_INT ci_MissionPedBS_PerformingGrappelFromHeli				9
CONST_INT ci_MissionPedBS_iSyncSceneAnimationReady 				10
CONST_INT ci_MissionPedBS_iSyncSceneAnimationNeedResync			11
CONST_INT ci_MissionPedBS_RollingStart							12
CONST_INT ci_MissionPedBS_ForcedBlip							13

STRUCT structMissionPedsLocalVars
	INT iSyncPedPartOwner = -1
	INT iSyncScene = -1
	PED_SYNC_SCENE_STATE ePedSyncSceneState = PED_SYNC_SCENE_STATE_INIT
	INT iSyncedPeds[FMMC_MAX_PED_SCENE_SYNCED_PEDS]
	INT iPedBS
ENDSTRUCT
structMissionPedsLocalVars sMissionPedsLocalVars[FMMC_MAX_PEDS]
//structPedsForConversation sMissionPedSpecialConversation

// Casino Brawl "Conversation Fight" Variables.
OBJECT_INDEX oiCasinoBrawlBottleObject
INT iCasinoBrawlBottleOutcome = -1
INT iCasinoBrawlCashierOutcome = -1

INT iCasinoBrawl_LoopingBrawlSndID = -1
INT iCasinoBrawl_PedInMixGroupBS[ciFMMC_PED_BITSET_SIZE]
CONST_FLOAT cfCasinoBrawl_DistanceBuffer	10.5

ENUM FMMC_CUTSCENE_TYPE
	FMMCCUT_MOCAP,
	FMMCCUT_ENDMOCAP,
	FMMCCUT_GENERIC_MOCAP,
	FMMCCUT_SCRIPTED
ENDENUM

FMMC_CUTSCENE_TYPE eCutsceneTypePlaying
INT iCutsceneIndexPlaying = -1

ENUM MOCAP_STREAMING_STAGE
	MOCAP_STREAMING_STAGE_INIT = 0,
	MOCAP_STREAMING_STAGE_REGISTER_ENTITIES,
	MOCAP_STREAMING_STAGE_REQUEST_MODELS,
	MOCAP_STREAMING_STAGE_ENDING
ENDENUM
MOCAP_STREAMING_STAGE eMocapStreamingStage

INT iPropHiddenForCutsceneBS[FMMC_MAX_PROP_BITSET]

INT iVehicleModShopOptionsBlockedBS

CONST_INT ciCUTSCENE_WAIT_FOR_ENTITIES_TIME			10000
SCRIPT_TIMER tdCutsceneWaitForEntitiesTimer
CONST_INT ciPROCESS_SCRIPT_INIT_LOAD_SCENE_TIMEOUT 	7500
SCRIPT_TIMER tdProcessScriptInitLoadSceneTimeout

INT iPedBlipStaggeredLoop = 0 
INT iPedAlwaysShowOneBlipCache = -1
INT iPedAlwaysShowOneBlipChosen = -1
FLOAT fPedAlwaysShowOneBlipDistCache = 9999999
VECTOR vPlayerPosCached
VECTOR vStartMicrophonePositionOverride

SCRIPT_TIMER CasinoHouseKeepDialogueTimer
SCRIPT_CONTROL_HOLD_TIMER sRappelButtonHold
SCRIPT_TIMER tdRappelStabilizeTimer

//Poison Gas // // // // // // // // // // // // // // // //
ENUM POISON_GAS_CHAMBER_STATE
	POISON_GAS_CHAMBER_STATE__INIT,
	POISON_GAS_CHAMBER_STATE__WAITING,
	POISON_GAS_CHAMBER_STATE__DEPLOYING,
	POISON_GAS_CHAMBER_STATE__RELEASED,
	POISON_GAS_CHAMBER_STATE__DISABLED
ENDENUM
POISON_GAS_CHAMBER_STATE eCurrentPoisonGasChamberState
INT iCachedPoisonGasZoneIndex = -1
CONST_INT ciPoisonGasVents									3
INT iPlayerWithinPoisonGasZoneBS
PTFX_ID pPoisonGasMainPTFX
PTFX_ID pPoisonGasPTFX[ciPoisonGasVents]
FLOAT fPoisonGasDamageBuildup = 0.0
FLOAT fPoisonGasFillBuildup = 0.0
FLOAT fPoisonGasFillBuildup_DamageRampSpeedMultiplier = 2.0
SCRIPT_TIMER stPoisonGasRunningTimer
SCRIPT_TIMER stPoisonGasCoughCooldownTimer
INT iPoisonGasCoughAnim = -1
CONST_INT ciPoisonGasCoughCooldownLength_NoGas					4500
CONST_INT ciPoisonGasCoughCooldownLength_FullGas				3350
CONST_INT ciPoisonGasBlockMinigameTime							30000
CONST_FLOAT cfPoisonGasCoughChance_NoGas						0.095
CONST_FLOAT cfPoisonGasCoughChance_FullGas						0.175
CONST_FLOAT cfPoisonGasDamageBuildupRate						2.0
CONST_FLOAT cfPoisonGasFillBuildupRate							0.01
CONST_FLOAT cfPoisonGasDamagePerTick_Start						1.0
CONST_FLOAT cfPoisonGasDamagePerTick_End						12.0
CONST_FLOAT cfPoisonGasFXTransitionTime							10.0
CONST_FLOAT cfPoisonGasBuildupRequiredBeforeDamaging			0.07

CONST_INT ciPOISONGASZONEBS_TriggerGas			0
CONST_INT ciPOISONGASZONEBS_AppliedEffects		1
INT iPoisonGasZoneBS

#IF IS_DEBUG_BUILD
INT iPedSpookFrameDebugWindow[FMMC_MAX_PEDS]
INT iPedsSpookedDebugWindow
INT iPedSpookOrderDebugWindow[FMMC_MAX_PEDS]
INT iPedSpookReasonDebugWindow[FMMC_MAX_PEDS]

INT iPedAggroFrameDebugWindow[FMMC_MAX_PEDS]
INT iPedAggroOrderDebugWindow[FMMC_MAX_PEDS]
INT iPedsAggroedDebugWindow

INT iPedBSDebugWindow_AggroedFromAll[ciFMMC_PED_BITSET_SIZE]
INT iPedBSDebugWindow_SpookedFromAll[ciFMMC_PED_BITSET_SIZE]
#ENDIF

//Metal Detector // // // // // // // // // // // // // // // //
FLOAT fPedHeadingBeforeMetalDetector[FMMC_MAX_PEDS]
SCRIPT_TIMER stPedMetalDetectorInvestigationTimer[FMMC_MAX_PEDS]
CONST_INT ciPedMetalDetectorInvestigationTime		5500

//Phone EMP // // // // // // // // // // // // // // // //
ENUM PHONE_EMP_STATE
	PHONE_EMP_STATE__INIT,
	PHONE_EMP_STATE__LOADING_ASSETS,
	PHONE_EMP_STATE__WAITING,
	PHONE_EMP_STATE__HACKING,
	PHONE_EMP_STATE__TRIGGERING,
	PHONE_EMP_STATE__WAITING_TO_HANG_UP,
	PHONE_EMP_STATE__WAITING_TO_DEACTIVATE,
	PHONE_EMP_STATE__DEACTIVATING,
	PHONE_EMP_STATE__FINISHED
ENDENUM
PHONE_EMP_STATE ePhoneEMPState = PHONE_EMP_STATE__INIT
FLOAT fPhoneEMPCharge = 0.0
CONST_FLOAT cfPhoneEMP_ChargeTimeInSeconds		3.0
SCRIPT_TIMER stPhoneEMP_ActivationDelayTimer
SCRIPT_TIMER stPhoneEMP_DeactivationTimer

CONST_INT ciPhoneEMP_EMPActivateDelay	2000
CONST_INT ciPhoneEMP_HangupTime			3500

COMBAT_RANGE eCachedPedCombatRanges[FMMC_MAX_PEDS]

INT iPhoneEMPPedReactedBS[ciFMMC_PED_BITSET_SIZE]

//Casino Vault Door // // // // // // // // // // // // // // // //
ENUM CASINO_VAULT_DOOR_STATE
	CASINO_VAULT_DOOR_STATE__INIT,
	CASINO_VAULT_DOOR_STATE__CREATING_DOOR_FOR_ANIM,
	CASINO_VAULT_DOOR_STATE__WAITING,
	CASINO_VAULT_DOOR_STATE__OPENING,
	CASINO_VAULT_DOOR_STATE__OPENED,
	CASINO_VAULT_DOOR_STATE__HIDDEN,
	CASINO_VAULT_DOOR_STATE__BROKEN,
	CASINO_VAULT_DOOR_STATE__INACTIVE_SYSTEM
ENDENUM
CASINO_VAULT_DOOR_STATE eCasinoVaultDoorState = CASINO_VAULT_DOOR_STATE__INIT
INTERIOR_INSTANCE_INDEX interiorCasinoVaultIndex
CONST_INT ciVaultDoorRoomKey		1929398810
OBJECT_INDEX oiRealVaultDoor

CONST_INT ciCasinoVaultDoorExplosionBS__StartedSequence		0
CONST_INT ciCasinoVaultDoorExplosionBS__PlayedDoorAnimation	1
CONST_INT ciCasinoVaultDoorExplosionBS__PlayedPedAnim		2
CONST_INT ciCasinoVaultDoorExplosionBS__PlayedCamShake		3
CONST_INT ciCasinoVaultDoorExplosionBS__ShownDecal			4
CONST_INT ciCasinoVaultDoorExplosionBS__HiddenExplosives	5
INT iCasinoVaultDoorExplosionBS

CONST_INT ciCasinoVaultDoorExplosionTiming_PlayDoorAnim		0
CONST_INT ciCasinoVaultDoorExplosionTiming_PlayPedAnim		195
CONST_INT ciCasinoVaultDoorExplosionTiming_PlayCamShake		315
CONST_INT ciCasinoVaultDoorExplosionTiming_ShowDecal		220
CONST_INT ciCasinoVaultDoorExplosionTiming_HiddenExplosives	200

SCRIPT_TIMER stCasinoVaultDoorExplosionTimer
OBJECT_INDEX oiSpawnedVaultDoor_Animated
OBJECT_INDEX oiSpawnedVaultDoor_StaticCollision
OBJECT_INDEX oiVaultDoorDebrisObj
INT iVaultExplosionSyncScene = -1

FLOAT fCasinoVaultDoor_CurrentOpenRatio = 0.0
FLOAT fCasinoVaultDoor_CurrentSpeed = 0.0
CONST_FLOAT cfCasinoVaultDoor_DoorOpenAcceleration					0.033
CONST_FLOAT cfCasinoVaultDoor_DoorOpenDeceleration					0.04
CONST_FLOAT cfCasinoVaultDoor_DoorOpenSpeedBuildupMultiplier		0.25
CONST_FLOAT cfCasinoVaultDoor_DoorOpenMaxSpeed			3.5
CONST_FLOAT cfCasinoVaultDoor_DoorOpenMaxRatio			1.0

// Zone Timers // // // // // // // // //
SCRIPT_TIMER stZoneTimers[FMMC_MAX_NUM_ZONES]
INT iZoneTimersCompletedBS
INT iZoneTimersHiddenBS

// Casino Tunnel Debris // // // //
OBJECT_INDEX oiCasinoTunnelDebris

//Casino Daily Cash Room Door // // // // // // // // // // // // // // // //
ENUM CASINO_DAILY_CASH_DOOR_STATE
	CASINO_DAILY_CASH_STATE__INIT,
	CASINO_DAILY_CASH_STATE__WAITING,
	CASINO_DAILY_CASH_STATE__OPENING,
	CASINO_DAILY_CASH_STATE__CLOSING,
	CASINO_DAILY_CASH_STATE__TRANSITION_CLOSE,
	CASINO_DAILY_CASH_STATE__TRANSITION_OPEN
	
ENDENUM
CASINO_DAILY_CASH_DOOR_STATE eCasinoDailyCashState
INT iCasinoDailyDoorHash
FLOAT fCasinoDailyDoor_CurrentOpenRatio = 0.0
FLOAT fCasinoDailyDoor_CurrentSpeed = 0.0
CONST_FLOAT cfCasinoDailyDoor_DoorOpenAcceleration					0.033
CONST_FLOAT cfCasinoDailyDoor_DoorOpenDecelerationTransition		0.04
CONST_FLOAT cfCasinoDailyDoor_DoorOpenDeceleration					0.08
CONST_FLOAT cfCasinoDailyDoor_DoorOpenSpeedBuildupMultiplier		0.45
CONST_FLOAT cfCasinoDailyDoor_DoorOpenMaxSpeed			0.3
CONST_FLOAT cfCasinoDailyDoor_DoorOpenMinSpeed			0.01
CONST_FLOAT cfCasinoDailyDoor_DoorOpenMaxRatio			1.0
CONST_FLOAT cfCasinoDailyDoor_DecelerationRatio			0.75

SCRIPT_TIMER tdUpdateTimedDoorTimer[FMMC_MAX_NUM_DOORS]
CONST_INT ciTIMED_DOOR_UPDATE 10000

ENUM RAPPEL_CAM_STATE
	RAPPEL_CAM_CREATE,
	RAPPEL_CAM_MASTER,
	RAPPEL_CAM_CLEANUP
ENDENUM
CONST_INT ciRappel_LookBehindCam 			0
CONST_INT ciRappel_TakenOutOfFirstPerson	1
CONST_INT ciRappel_CameraHelpTextShown		2
CONST_INT ciRappel_MovementHelpTextShown	3
CONST_INT ciRappel_PreCamSetup				4
STRUCT WALL_RAPPEL_STRUCT
	ROPE_INDEX RappelRopeID
	RAPPEL_CAM_STATE eRappelCamState
	CAMERA_INDEX camRappelCam
	BOOL bRappelLookBehindCam
	INT iRappelBitset
	SCRIPT_TIMER tdFadeTimer
ENDSTRUCT
WALL_RAPPEL_STRUCT sWallRappel


INT iGrabbedStolenGoods

CONST_INT ci_PLACED_PTFX_Loaded								0
CONST_INT ci_PLACED_PTFX_Created							1
CONST_INT ci_PLACED_PTFX_SFX								2
CONST_INT ci_PLACED_PTFX_CleanedUp							3
STRUCT PLACED_PTFX_CLIENT_STRUCT 
	PTFX_ID ptfxID
	INT iPtfxBS
	SCRIPT_TIMER tdPtfxStarted
	INT iPTFXSoundID = -1
ENDSTRUCT
PLACED_PTFX_CLIENT_STRUCT sPlacedPtfxLocal[FMMC_MAX_PLACED_PTFX]

INTERIOR_INSTANCE_INDEX interiorCasinoApartmentIndex

//Cop Decoy
//------------
CONST_INT ci_COP_DECOY_CREATED_BLIP 			0
CONST_INT ci_COP_DECOY_DEDUCT_1_STAR 			1
CONST_INT ci_COP_DECOY_DEDUCT_2_STAR 			2
CONST_INT ci_COP_DECOY_DEDUCTED_STAR_1 			3
CONST_INT ci_COP_DECOY_DEDUCTED_STAR_2 			4
CONST_INT ci_COP_DECOY_ENTERED_AREA 			5
CONST_INT ci_COP_DECOY_FORCE_5_STAR_ENDED 		6
CONST_INT ci_COP_DECOY_SHOWN_HELP_1 			7
CONST_INT ci_COP_DECOY_SHOWN_HELP_2 			8
CONST_INT ci_COP_DECOY_DEDUCTED_STAR_THIS_FRAME	9
CONST_INT ci_COP_DECOY_NEAR_END_DIA_TRIGGER		10

CONST_INT ci_COP_DECOY_TIME_LEFT_FOR_DIA_TRIGGER 15000
INT iCopDecoyBS
INT iCopDecoyDeductFirstStarTime			  = 0
INT iCopDecoyDeductSecondStarTime			  = 0
INT iCopDecoyCurrentFakeHealth				  = 100
INT iCopDecoyFakeHealthState				  = 0
REL_GROUP_HASH relDecoyPed
SCRIPT_TIMER tdRegisterDecoyAsTargetTimer
SCRIPT_TIMER tdCopDecoy5StarForceTimer
SCRIPT_TIMER tdBlockWantedConeResponseStationaryTimer
BLIP_INDEX biCopDecoyBlip

CONST_INT ciZoneBS_PlayersInside_1				0
CONST_INT ciZoneBS_PlayersInside_All			31
INT iZoneBS_PlayersInside[FMMC_MAX_NUM_ZONES]

INTERIOR_INSTANCE_INDEX iInteriorIndexCasinoMainCH
INTERIOR_INSTANCE_INDEX iInteriorIndexCasinoTunnel
INTERIOR_INSTANCE_INDEX iInteriorIndexCasinoBackArea
INTERIOR_INSTANCE_INDEX iInteriorIndexCasinoHotelFloor
INTERIOR_INSTANCE_INDEX iInteriorIndexCasinoLoadingBay
INTERIOR_INSTANCE_INDEX iInteriorIndexCasinoVault
INTERIOR_INSTANCE_INDEX iInteriorIndexCasinoUtilityLift
INTERIOR_INSTANCE_INDEX iInteriorIndexCasinoUtilityShaft

// Might want to move this to globals so it can be reused? Mission Controller Functionality will be written to support it being used outside of MC.
// #################################### HAND HELD DRILL MINI GAME ####################################
CONST_INT ciTIME_TO_DRILL_LOCKBOX							8000
CONST_INT ciTIME_FOR_DRILL_ANIM_TO_ENTER_REST				3000
CONST_INT ciMAX_HANDHELD_DRILLERS							4
CONST_INT ciTIME_FOR_DRILL_ANIM_TOGGLE_DLEAY				1000

CONST_INT ciDrill_Bitset_HoldingDrillButton					0
CONST_INT ciDrill_Bitset_RequestedAssets					1
CONST_INT ciDrill_Bitset_StartedPositioningTask				2
CONST_INT ciDrill_Bitset_CheckLockboxHasAReward				3
CONST_INT ciDrill_Bitset_LockboxHasAReward					4
CONST_INT ciDrill_Bitset_InitializedSettings				5
CONST_INT ciDrill_Bitset_InteriorInitialized				6
CONST_INT ciDrill_Bitset_LockboxGivenReward					7
CONST_INT ciDrill_Bitset_LockboxExittingFinished			8
CONST_INT ciDrill_Bitset_InitializedDrillingSettings		9
CONST_INT ciDrill_Bitset_PlayedMissingDrillHelpText			10
CONST_INT ciDrill_Bitset_ShowDuffelBag						11
CONST_INT ciDrill_Bitset_HideDuffelBag						12
CONST_INT ciDrill_Bitset_ShowAnimDrill						13
CONST_INT ciDrill_Bitset_HideAnimDrill						14
CONST_INT ciDrill_Bitset_CheckForBreakoutAndAnimHides		15

ENUM HANDHELD_DRILL_MINIGAME_DRILLING_STATE
	HANDHELD_DRILL_MINIGAME_DRILLING_STATE_WAITING = 0,
	HANDHELD_DRILL_MINIGAME_DRILLING_STATE_SELECTING,
	HANDHELD_DRILL_MINIGAME_DRILLING_STATE_DRILLING,	
	HANDHELD_DRILL_MINIGAME_DRILLING_STATE_FINISHING,
	HANDHELD_DRILL_MINIGAME_DRILLING_STATE_EXITTING,
	HANDHELD_DRILL_MINIGAME_DRILLING_STATE_CLEANUP
ENDENUM
ENUM HANDHELD_DRILL_MINIGAME_STATE
	HANDHELD_DRILL_MINIGAME_STATE_IDLE = 0,
	HANDHELD_DRILL_MINIGAME_STATE_INIT,
	HANDHELD_DRILL_MINIGAME_STATE_WAITING,
	HANDHELD_DRILL_MINIGAME_DRILL_POSITIONING,
	HANDHELD_DRILL_MINIGAME_DRILL_READY_TO_DRILL,
	HANDHELD_DRILL_MINIGAME_DRILL_FINISHED,
	HANDHELD_DRILL_MINIGAME_DRILL_CLEANUP
ENDENUM

STRUCT STRUCT_HANDHELD_DRILL_MINI_GAME
	PED_INDEX pedDrill
	CAMERA_INDEX camDrillCam
	OBJECT_INDEX oiLockboxes[ciMAX_NUMBER_OF_HANDHELD_DRILL_LOCKBOX_CABINETS_DYNAMIC]
	OBJECT_INDEX oiLockboxDrill
	OBJECT_INDEX oiDrill
	OBJECT_INDEX oiCabinet
	OBJECT_INDEX oiDrillBag
	OBJECT_INDEX oiMoneyBag
	PTFX_ID ptfxDrillSparks
	
	OBJECT_INDEX oiRemoteDrills[ciMAX_HANDHELD_DRILLERS]
	PTFX_ID ptfxRemoteDrillSparks[ciMAX_HANDHELD_DRILLERS]
	
	HANDHELD_DRILL_MINIGAME_DRILLING_STATE eDrillingState
	HANDHELD_DRILL_MINIGAME_STATE eDrillMinigameState
	
	SCRIPT_TIMER tdDrillingTimer
	SCRIPT_TIMER tdDrillCameraSwitchTimer
	SCRIPT_TIMER tdMinigameSafetyTimer
	SCRIPT_TIMER tdMinigamePositionTimer
	SCRIPT_TIMER tdMinigameIdleTimer
	SCRIPT_TIMER tdMinigameDrillToggleDelay
	
	INT iDrillMinigameBS
	
	// Also in ServerBD and NonResetVars (iLockboxConfiguration, iLockboxPatternPreset)
	INT iLockboxConfiguration
	INT iLockboxPatternPreset	
	
	INT iLockboxCabinetSelected			// Out of all 42 potential cabinets		ciMAX_NUMBER_OF_HANDHELD_DRILL_LOCKBOX_CABINETS_TOTAL
	INT iLockboxDynamicCabinetSelected	// Out of the 18 scripted ones.			ciMAX_NUMBER_OF_HANDHELD_DRILL_LOCKBOX_CABINETS_DYNAMIC
	INT iLockboxCabinetPatternSelected	// Out of the 9 pre-defined prop patterns
	INT iLockboxSelected				// The actual Lockbox in the list of 4.	
	
	FLOAT fLockboxCompletionPercent[ciMAX_NUMBER_OF_HANDHELD_DRILL_LOCKBOX_CABINETS_DYNAMIC][ciMAX_NUMBER_OF_HANDHELD_DRILL_LOCKBOXES]	
	INT iLockboxCabinetPattern[ciMAX_NUMBER_OF_HANDHELD_DRILL_LOCKBOX_CABINETS_DYNAMIC]
	INT iLockboxCabinetSubPattern[ciMAX_NUMBER_OF_HANDHELD_DRILL_LOCKBOX_CABINETS_DYNAMIC]
	
	INT iSyncScene = -1
	TEXT_LABEL_63 tl63_AnimPlaying
	INT iDrillSoundID = -1
	FLOAT fDrillCamBlendoutPhaseStart = 1.0
	FLOAT fDrillCamBlendoutPhaseEnd = 1.0
ENDSTRUCT
STRUCT_HANDHELD_DRILL_MINI_GAME sHandHeldDrillMiniGame

// #################################### HAND HELD DRILL MINI GAME ####################################

CONST_INT ciMAX_ELEVATOR_DOORS 2
STRUCT ELEVATOR_ASSOCIATED_ENTITIES
	INT iDoors[ciMAX_ELEVATOR_DOORS]
	INT iObject = -1
ENDSTRUCT
ELEVATOR_ASSOCIATED_ENTITIES sElevatorAssEntities[FMMC_MAX_ELEVATORS]
CAMERA_INDEX elevatorCam
SCRIPT_TIMER tdElevatorCamTimer
CONST_INT ciELEVATOR_COOLDOWN	3000

SCRIPT_TIMER tdLocationProcDelayTimer[FMMC_MAX_GO_TO_LOCATIONS]
INT iDoorBS_HiddenForCutscene

FMMC_LEGACY_MISSION_LOCAL_CONTINUITY_VARS sLEGACYMissionLocalContinuityVars

INT iVehicleSwapSwitchUnBlocked
INT iVehicleBuyerLocationSpawned

SCRIPT_TIMER stMantrapDoorDelayTimer
CONST_INT ciMantrapDoorDelayTime		2500

CONST_INT ciWAIT_FOR_INTERIOR_SCRIPTS_TIMEOUT	15000
SCRIPT_TIMER tdWaitForInteriorScriptsTimeout
INT iNumCashTrollies
SCRIPT_TIMER tdVehicleRadioSequenceDelayTimer[FMMC_MAX_VEHICLES]
INT iVehicleRadioSequenceBitset
SCRIPT_TIMER tdScriptedCutsceneSyncSceneSafetyTimer
SCRIPT_TIMER tdScriptedCutsceneSyncSceneSafetyTimer2

SCRIPT_TIMER stHintCamTemporarilyDisableTimer
CONST_INT ciHintCamTemporarilyDisableTimer_Length	750

INT iLocalOffRuleMinigameCompletedBS = 0

INT iVehicleDoorSetupSetToCreatorSettings

SCRIPT_TIMER tdResurrectionStuckTimer
CONST_INT ciResurrectionStuckTimerLength	5000
