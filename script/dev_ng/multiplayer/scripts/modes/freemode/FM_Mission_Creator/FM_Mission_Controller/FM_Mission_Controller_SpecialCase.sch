
USING "FM_Mission_Controller_USING.sch"
USING "FM_Mission_Controller_Utility.sch"
USING "FM_Mission_Controller_Entities.sch"
USING "FM_Mission_Controller_ObjectiveCompletion.sch"



//B*-2139031 If leader, delay set time until camera is on planning board. avoids pop
FUNC BOOL DELAY_SET_TOD()
	IF NOT IS_BIT_SET(iLocalBoolCheck9,LBOOL9_TIME_SET)
	AND Is_Player_Currently_On_MP_Heist(PLAYER_ID())
	AND IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
	AND ENUM_TO_INT(g_HeistPlanningClient.eHeistFlowState) <= ENUM_TO_INT(HEIST_FLOW_UPDATE_BACKGROUND)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: HEIST ANTI-CHEAT STUFF (to prevent reset abuse?) !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************



PROC INCREMENT_CURRENT_HEIST_CHEATER_BITSET()

	IF NOT IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_HEIST_FINALE_CHANCE_1, DEFAULT)
		SET_FREEMODE_PROLOGUE_PROFILE_SETTINGS(PSFP_HEIST_FINALE_CHANCE_1, DEFAULT)
		g_bHeistCheaterIncremented = TRUE
		PRINTLN("[RCC MISSION] PSFP_HEIST_FINALE_CHANCE_1 SET")
	ELIF NOT IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_HEIST_FINALE_CHANCE_2, DEFAULT)
		SET_FREEMODE_PROLOGUE_PROFILE_SETTINGS(PSFP_HEIST_FINALE_CHANCE_2, DEFAULT)
		g_bHeistCheaterIncremented = TRUE
		PRINTLN("[RCC MISSION] PSFP_HEIST_FINALE_CHANCE_2 SET")
	ELIF NOT IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_HEIST_FINALE_CHANCE_3, DEFAULT)
		SET_FREEMODE_PROLOGUE_PROFILE_SETTINGS(PSFP_HEIST_FINALE_CHANCE_3, DEFAULT)
		g_bHeistCheaterIncremented = TRUE
		PRINTLN("[RCC MISSION] PSFP_HEIST_FINALE_CHANCE_3 SET")
	ENDIF
	
ENDPROC

PROC DECREMENT_CURRENT_HEIST_CHEATER_BITSET()

	IF IS_BIT_SET(MC_playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PBBOOL_HEIST_HOST)
		IF IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_HEIST_FINALE_CHANCE_3, DEFAULT)
			CLEAR_FREEMODE_PROLOGUE_PROFILE_SETTINGS(PSFP_HEIST_FINALE_CHANCE_3, DEFAULT)
			PRINTLN("[RCC MISSION] PSFP_HEIST_FINALE_CHANCE_3 CLEAR")
		ELIF IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_HEIST_FINALE_CHANCE_2, DEFAULT)
			CLEAR_FREEMODE_PROLOGUE_PROFILE_SETTINGS(PSFP_HEIST_FINALE_CHANCE_2, DEFAULT)
			PRINTLN("[RCC MISSION] PSFP_HEIST_FINALE_CHANCE_2 CLEAR")
		ELIF IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_HEIST_FINALE_CHANCE_1, DEFAULT)
			CLEAR_FREEMODE_PROLOGUE_PROFILE_SETTINGS(PSFP_HEIST_FINALE_CHANCE_1, DEFAULT)
			PRINTLN("[RCC MISSION] PSFP_HEIST_FINALE_CHANCE_1 CLEAR")
		ENDIF
	ENDIF
	
ENDPROC

PROC CLEAR_HEIST_CHEATER_BITSETS()

	IF IS_BIT_SET(MC_playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PBBOOL_HEIST_HOST)
		CLEAR_HEIST_FINALE_ANTI_CHEAT_CHANCES()
		PRINTLN("[RCC MISSION] CLEAR_HEIST_CHEATER_BITSETS CALLED")
	ENDIF
	
ENDPROC


PROC MAINTAIN_HEIST_LEADER_ANTI_CHEAT()

INT iTeam

	IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_PROCESSED_HEIST_END_OF_MISSION)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(PARTICIPANT_ID())
		AND IS_BIT_SET(MC_playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PBBOOL_HEIST_HOST)
			IF Is_Player_Currently_On_MP_Heist(PLAYER_ID())
				FOR iTeam = 0 TO (MC_ServerBD.iNumActiveTeams-1)
					IF NOT IS_BIT_SET(iLocalBoolCheck12, LBOOL12_HEIST_LEADER_CHEAT_CHECK_DONE)
						IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] >= (MC_serverBD.iMaxObjectives[ iTeam ]-1)
						AND MC_ServerBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
							IF SHOULD_END_OF_MISSION_HEIST_LOGIC_BE_PROCESSED_BECAUSE_OF_STRAND_MISSION()
								INCREMENT_CURRENT_HEIST_CHEATER_BITSET()
							ENDIF
							SET_BIT(iLocalBoolCheck12, LBOOL12_HEIST_LEADER_CHEAT_CHECK_DONE)
							PRINTLN("[RCC MISSION] MAINTAIN_HEIST_LEADER_ANTI_CHEAT CALLED because team on final objective: ",iTeam)
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
	ENDIF

ENDPROC

//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: GANGOPS ANTI-CHEAT STUFF (to prevent reset abuse?) !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************

PROC INCREMENT_CURRENT_GANGOPS_CHEATER_BITSET()

	IF NOT IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_GANGOPS_FINALE_CHANCE_1, DEFAULT)
		SET_FREEMODE_PROLOGUE_PROFILE_SETTINGS(PSFP_GANGOPS_FINALE_CHANCE_1, DEFAULT)
		g_bGangopsCheaterIncremented = TRUE
		PRINTLN("[RCC MISSION][GO_ANTI] PSFP_GANGOPS_FINALE_CHANCE_1 SET")
	ELIF NOT IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_GANGOPS_FINALE_CHANCE_2, DEFAULT)
		SET_FREEMODE_PROLOGUE_PROFILE_SETTINGS(PSFP_GANGOPS_FINALE_CHANCE_2, DEFAULT)
		g_bGangopsCheaterIncremented = TRUE
		PRINTLN("[RCC MISSION][GO_ANTI] PSFP_GANGOPS_FINALE_CHANCE_2 SET")
	ELIF NOT IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_GANGOPS_FINALE_CHANCE_3, DEFAULT)
		SET_FREEMODE_PROLOGUE_PROFILE_SETTINGS(PSFP_GANGOPS_FINALE_CHANCE_3, DEFAULT)
		g_bGangopsCheaterIncremented = TRUE
		PRINTLN("[RCC MISSION][GO_ANTI] PSFP_GANGOPS_FINALE_CHANCE_3 SET")
	ENDIF
	
ENDPROC

PROC DECREMENT_CURRENT_GANGOPS_CHEATER_BITSET()

	IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
		IF IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_GANGOPS_FINALE_CHANCE_3, DEFAULT)
			CLEAR_FREEMODE_PROLOGUE_PROFILE_SETTINGS(PSFP_GANGOPS_FINALE_CHANCE_3, DEFAULT)
			PRINTLN("[RCC MISSION][GO_ANTI] PSFP_GANGOPS_FINALE_CHANCE_3 CLEAR")
		ELIF IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_GANGOPS_FINALE_CHANCE_2, DEFAULT)
			CLEAR_FREEMODE_PROLOGUE_PROFILE_SETTINGS(PSFP_GANGOPS_FINALE_CHANCE_2, DEFAULT)
			PRINTLN("[RCC MISSION][GO_ANTI] PSFP_GANGOPS_FINALE_CHANCE_2 CLEAR")
		ELIF IS_FREEMODE_PROLOGUE_PROFILE_SETTINGS_PASSED(PSFP_GANGOPS_FINALE_CHANCE_1, DEFAULT)
			CLEAR_FREEMODE_PROLOGUE_PROFILE_SETTINGS(PSFP_GANGOPS_FINALE_CHANCE_1, DEFAULT)
			PRINTLN("[RCC MISSION][GO_ANTI] PSFP_GANGOPS_FINALE_CHANCE_1 CLEAR")
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_GANGOPS_LEADER_ANTI_CHEAT()

INT iTeam

	IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_PROCESSED_HEIST_END_OF_MISSION)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(PARTICIPANT_ID())
		AND GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
			IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FINALE()
				FOR iTeam = 0 TO (MC_ServerBD.iNumActiveTeams-1)
					IF NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_GANGOPS_LEADER_CHEAT_CHECK_DONE)
						IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] >= (MC_serverBD.iMaxObjectives[ iTeam ]-1)
						AND MC_ServerBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
							IF SHOULD_END_OF_MISSION_HEIST_LOGIC_BE_PROCESSED_BECAUSE_OF_STRAND_MISSION()
								INCREMENT_CURRENT_GANGOPS_CHEATER_BITSET()
							ENDIF
							SET_BIT(iLocalBoolCheck26, LBOOL26_GANGOPS_LEADER_CHEAT_CHECK_DONE)
							PRINTLN("[RCC MISSION][GO_ANTI] MAINTAIN_GANGOPS_LEADER_ANTI_CHEAT CALLED because team on final objective: ",iTeam)
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
	ENDIF

ENDPROC


//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: CASINO HEIST ANTI-CHEAT STUFF (to prevent reset abuse?) !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************

PROC INCREMENT_CURRENT_CASINO_HEIST_CHEATER_BITSET()

	IF NOT IS_STRAND_PROGRESSION_STATUS_PROFILE_SETTINGS_PASSED(PSSPS_CASINO_HEIST_FINALE_CHANCE_1, DEFAULT)
		SET_STRAND_PROGRESSION_STATUS_PROFILE_SETTINGS(PSSPS_CASINO_HEIST_FINALE_CHANCE_1, DEFAULT)
		g_bCasinoHeistCheaterIncremented = TRUE
		PRINTLN("[RCC MISSION][CASINO_HEIST_ANTICHEAT] PSSPS_CASINO_HEIST_FINALE_CHANCE_1 SET")
	ELIF NOT IS_STRAND_PROGRESSION_STATUS_PROFILE_SETTINGS_PASSED(PSSPS_CASINO_HEIST_FINALE_CHANCE_2, DEFAULT)
		SET_STRAND_PROGRESSION_STATUS_PROFILE_SETTINGS(PSSPS_CASINO_HEIST_FINALE_CHANCE_2, DEFAULT)
		g_bCasinoHeistCheaterIncremented = TRUE
		PRINTLN("[RCC MISSION][CASINO_HEIST_ANTICHEAT] PSSPS_CASINO_HEIST_FINALE_CHANCE_2 SET")
	ELIF NOT IS_STRAND_PROGRESSION_STATUS_PROFILE_SETTINGS_PASSED(PSSPS_CASINO_HEIST_FINALE_CHANCE_3, DEFAULT)
		SET_STRAND_PROGRESSION_STATUS_PROFILE_SETTINGS(PSSPS_CASINO_HEIST_FINALE_CHANCE_3, DEFAULT)
		g_bCasinoHeistCheaterIncremented = TRUE
		PRINTLN("[RCC MISSION][CASINO_HEIST_ANTICHEAT] PSSPS_CASINO_HEIST_FINALE_CHANCE_3 SET")
	ENDIF
	
ENDPROC

PROC DECREMENT_CURRENT_CASINO_HEIST_CHEATER_BITSET()

	IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
		IF IS_STRAND_PROGRESSION_STATUS_PROFILE_SETTINGS_PASSED(PSSPS_CASINO_HEIST_FINALE_CHANCE_3, DEFAULT)
			CLEAR_STRAND_PROGRESSION_STATUS_PROFILE_SETTINGS(PSSPS_CASINO_HEIST_FINALE_CHANCE_3, DEFAULT)
			PRINTLN("[RCC MISSION][CASINO_HEIST_ANTICHEAT] PSSPS_CASINO_HEIST_FINALE_CHANCE_3 CLEAR")
		ELIF IS_STRAND_PROGRESSION_STATUS_PROFILE_SETTINGS_PASSED(PSSPS_CASINO_HEIST_FINALE_CHANCE_2, DEFAULT)
			CLEAR_STRAND_PROGRESSION_STATUS_PROFILE_SETTINGS(PSSPS_CASINO_HEIST_FINALE_CHANCE_2, DEFAULT)
			PRINTLN("[RCC MISSION][CASINO_HEIST_ANTICHEAT] PSSPS_CASINO_HEIST_FINALE_CHANCE_2 CLEAR")
		ELIF IS_STRAND_PROGRESSION_STATUS_PROFILE_SETTINGS_PASSED(PSSPS_CASINO_HEIST_FINALE_CHANCE_1, DEFAULT)
			CLEAR_STRAND_PROGRESSION_STATUS_PROFILE_SETTINGS(PSSPS_CASINO_HEIST_FINALE_CHANCE_1, DEFAULT)
			PRINTLN("[RCC MISSION][CASINO_HEIST_ANTICHEAT] PSSPS_CASINO_HEIST_FINALE_CHANCE_1 CLEAR")
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_CASINO_HEIST_LEADER_ANTI_CHEAT()

	INT iTeam
	IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_PROCESSED_HEIST_END_OF_MISSION)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(PARTICIPANT_ID())
		AND GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
			IF IS_THIS_CASINO_HEIST_MISSION_THE_FINAL_STAGE(GET_CASINO_HEIST_MISSION_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash))
				FOR iTeam = 0 TO (MC_ServerBD.iNumActiveTeams-1)
					IF NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_CASINO_HEIST_LEADER_CHEAT_CHECK_DONE)
						IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] >= (MC_serverBD.iMaxObjectives[ iTeam ]-1)
						AND MC_ServerBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
							IF SHOULD_END_OF_MISSION_HEIST_LOGIC_BE_PROCESSED_BECAUSE_OF_STRAND_MISSION()
								INCREMENT_CURRENT_CASINO_HEIST_CHEATER_BITSET()
							ENDIF
							SET_BIT(iLocalBoolCheck33, LBOOL33_CASINO_HEIST_LEADER_CHEAT_CHECK_DONE)
							PRINTLN("[RCC MISSION][CASINO_HEIST_ANTICHEAT] MAINTAIN_CASINO_HEIST_LEADER_ANTI_CHEAT CALLED because team on final objective: ",iTeam)
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
	ENDIF

ENDPROC


//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: CASCO CONTAINER OPENING /CAR DETACHING (from The Prison Break Station prep) !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************



FUNC BOOL IS_CONTAINER_VEHICLE_DETACHED( INT iContainerIndex )
	
	BOOL bDetach = TRUE
	VEHICLE_INDEX carToDetach = NULL
	INT iLoop
	
	FOR iLoop = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles - 1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[ iLoop ].iAttachParent = iContainerIndex
			carToDetach = NET_TO_VEH( MC_serverBD_1.sFMMC_SBD.niVehicle[iLoop] )
			iLoop = g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles
		ENDIF
	ENDFOR
	
	IF NOT IS_VEHICLE_FUCKED( carToDetach )
		IF IS_ENTITY_ATTACHED( carToDetach )
			bDetach = FALSE
			PRINTLN("[RCC MISSION] vehicle attached ")
		ENDIF
	ENDIF
	
	PRINTLN("[RCC MISSION] vehicle attached test 0")
	
	RETURN bDetach
	
ENDFUNC

func vehicle_index get_attached_car_from_container(INT iContainerIndex)
	
	VEHICLE_INDEX carToDetach = NULL
	INT iLoop
	
	FOR iLoop = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles - 1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[ iLoop ].iAttachParent = iContainerIndex
			carToDetach = NET_TO_VEH( MC_serverBD_1.sFMMC_SBD.niVehicle[iLoop] )
			iLoop = g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles
		ENDIF
	ENDFOR
	
	return carToDetach
	
endfunc 

FUNC BOOL DETACH_ATTACHED_CAR_FROM_CONTAINER( INT iContainerIndex )
	
	BOOL bDetach = TRUE
	
	VEHICLE_INDEX carToDetach = NULL
	
	carToDetach = get_attached_car_from_container(iContainerIndex)
	
	IF NOT IS_VEHICLE_FUCKED( carToDetach )
		
		NETWORK_REQUEST_CONTROL_OF_ENTITY( carToDetach )
		
		IF NETWORK_HAS_CONTROL_OF_ENTITY( carToDetach )
			IF IS_ENTITY_ATTACHED(carToDetach)
//				SET_VEHICLE_DOORS_LOCKED(carToDetach, VEHICLELOCK_UNLOCKED)
//				SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(carToDetach, FALSE)
//				SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(carToDetach,FALSE)
				SET_ENTITY_INVINCIBLE( carToDetach, FALSE ) 
				SET_ENTITY_PROOFS(carToDetach, FALSE,TRUE,TRUE,TRUE,FALSE) //moving till after the cutscene as the explosion via sticky bombs can cause the mission to fail. 
				RESET_VEHICLE_STUCK_TIMER(carToDetach,VEH_STUCK_ON_ROOF)
				RESET_VEHICLE_STUCK_TIMER(carToDetach,VEH_STUCK_ON_SIDE)
				RESET_VEHICLE_STUCK_TIMER(carToDetach,VEH_STUCK_HUNG_UP)
				RESET_VEHICLE_STUCK_TIMER(carToDetach,VEH_STUCK_JAMMED)
				
				VECTOR vCarCoords = GET_ENTITY_COORDS(carToDetach)
				
				DETACH_ENTITY( carToDetach , DEFAULT, FALSE)
				SET_ENTITY_COORDS(carToDetach,vCarCoords + <<0, 0, 0.05>>)
				set_entity_collision(carToDetach, true)
				
			ELSE
				#IF IS_DEBUG_BUILD
				PRINTLN("[RCC MISSION] [dsw] [DETACH_ATTACHED_CAR_FROM_CONTAINER] OPEN_CONTAINER_HACK_DOORSl not detaching as already detached ")
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			PRINTLN("[RCC MISSION] [dsw] [DETACH_ATTACHED_CAR_FROM_CONTAINER] OPEN_CONTAINER_HACK_DOORSl Failed to detach is don't have control ")
			#ENDIF
			NETWORK_REQUEST_CONTROL_OF_ENTITY( carToDetach )
			bDetach = FALSE
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		PRINTLN("[RCC MISSION] [dsw] [DETACH_ATTACHED_CAR_FROM_CONTAINER] OPEN_CONTAINER_HACK_DOORSl Failed to detach is vehicle broke ")
		#ENDIF
	ENDIF
	
	RETURN bDetach
	
ENDFUNC

FUNC BOOL IS_THIS_A_HACK_CONTAINER( INT iObjIndex )
	RETURN IS_BIT_SET( MC_ServerBD.iIsHackContainer, iObjIndex )
ENDFUNC

FUNC BOOL IS_CONTAINER_DOOR_OPEN(INT iMaxOpenAngle, BOOL bMocap )
	BOOL bReturn = FALSE
	
	printstring("MC_playerBD[ iLocalPart].fContainerOpenRatio")
	printfloat(MC_playerBD[ iLocalPart].fContainerOpenRatio)
	printnl()

	IF MC_playerBD[ iLocalPart].fContainerOpenRatio >= iMaxOpenAngle
	and bMocap //has cutscene ever been triggered
		bReturn = TRUE
	ENDIF
	
	RETURN bReturn
ENDFUNC

///purpose: cycle through participant list. If the local player is first on the list return true else return false
FUNC BOOL HAS_CASCO_CUTSCENE_PLAYED() 

	INT iParticipant 
	
	IF IS_BIT_SET(MC_playerBD[ iLocalPart].iClientBitSet2, PBBOOL2_PLAYED_CONTAINER_CUTSCENE)
		RETURN TRUE
	ENDIF
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant 
		IF IS_BIT_SET(MC_playerBD[ iParticipant ].iClientBitSet2, PBBOOL2_PLAYED_CONTAINER_CUTSCENE)
			SET_BIT(MC_playerBD[ iLocalPart].iClientBitSet2, PBBOOL2_PLAYED_CONTAINER_CUTSCENE)
			RETURN TRUE
		ENDIF
	ENDREPEAT

	RETURN FALSE

ENDFUNC

FUNC BOOL OPEN_CONTAINER_HACK_DOORS( INT iContainerIndex, INT iAngleToOpenTo, BOOL bMocap )
	
	INT left 	= 0
	INT right 	= 1
	INT lock 	= 2
	
	BOOL bReturn = FALSE
	
	VECTOR vLeftDoorOffset 	= << 1.295, 6.075, -1.4 >>
	VECTOR vRightDoorOffset = << -1.295, 6.075, -1.4 >>
	
	NETWORK_INDEX niContainer = MC_serverBD_1.sFMMC_SBD.niObject[ iContainerIndex ]
	NETWORK_INDEX niLeftDoor = MC_serverBD_1.niMinigameCrateDoors[ left ]
	NETWORK_INDEX niRightDoor = MC_serverBD_1.niMinigameCrateDoors[ right ]
	NETWORK_INDEX niLock = MC_serverBD_1.niMinigameCrateDoors[ lock ]
	
	IF NETWORK_HAS_CONTROL_OF_NETWORK_ID( niContainer )
	
		vehicle_index carToDetach = get_attached_car_from_container(iContainerIndex)

		SET_NETWORK_ID_CAN_MIGRATE(niContainer, false)
		
		NETWORK_REQUEST_CONTROL_OF_NETWORK_ID( niLeftDoor )
		NETWORK_REQUEST_CONTROL_OF_NETWORK_ID( niRightDoor )
		
		if is_entity_attached(carToDetach)
			NETWORK_REQUEST_CONTROL_OF_ENTITY(carToDetach)
		endif 
	
		PRINTLN("[RCC MISSION] OPEN_CONTAINER_HACK_DOORS I have control of container ")
		
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID( niLeftDoor )
		AND NETWORK_HAS_CONTROL_OF_NETWORK_ID( niRightDoor )
		and (NETWORK_HAS_CONTROL_OF_entity(carToDetach) or (not is_entity_attached(carToDetach)))
			
			PRINTLN("[RCC MISSION] OPEN_CONTAINER_HACK_DOORS I have control of doors ")
			
			IF NOT (NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niLock) and IS_ENTITY_ATTACHED(NET_TO_ENT(niLock))) //Lock has come off, start to open doors:
				PRINTLN("[RCC MISSION] OPEN_CONTAINER_HACK_DOORS Lock has been opened ")
				
				FLOAT fAngleToOpenEveryFrame = 1//10 //burst open door when lock is sot off.
				
				IF bMocap
					fAngleToOpenEveryFrame = 2
				ENDIF
				
				printstring("fAngleToOpenEveryFrame ")
				printfloat(fAngleToOpenEveryFrame)
				printnl()
				
				//-- Dave W, try detaching the car here. Waiting until the doors are open takes too long and causes bugs like 2067439
				if is_entity_attached(carToDetach)
					DETACH_ATTACHED_CAR_FROM_CONTAINER(iContainerIndex)
				endif 

				// If the doors are now open, return true - passing in bMocap as FALSE so it doesn't keep opening the doors
				IF IS_CONTAINER_DOOR_OPEN(iAngleToOpenTo, bMocap)
					PRINTLN("[RCC MISSION] OPEN_CONTAINER_HACK_DOORS Container door has been opened ")
					
					IF NOT IS_CUTSCENE_PLAYING()
					AND DETACH_ATTACHED_CAR_FROM_CONTAINER( iContainerIndex )
						
						//detach_attached_car_from_container() used to set the vehicle unlocked before the cutscene
						//started. The player could enter the vehicle thus they were warped into the vehicle. 
						//We only unlock the car once the cutscene is not playing 
						SET_VEHICLE_DOORS_LOCKED(carToDetach, VEHICLELOCK_UNLOCKED)
						SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(carToDetach, FALSE)
						SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(carToDetach,FALSE)
						
						SET_NETWORK_ID_CAN_MIGRATE(niContainer, true)
						bReturn = TRUE
					#IF IS_DEBUG_BUILD
					ELSE
						IF IS_CUTSCENE_PLAYING()
							PRINTLN("[RCC MISSION] OPEN_CONTAINER_HACK_DOORS Trying to detach but cutscene playing! ")
						ELSE
							PRINTLN("[RCC MISSION] OPEN_CONTAINER_HACK_DOORS Trying to detach but failed DETACH_ATTACHED_CAR_FROM_CONTAINER ")
						ENDIF
					#ENDIF
					ENDIF
					
				ELSE // Else keep opening them
					PRINTLN("[RCC MISSION] OPEN_CONTAINER_HACK_DOORS Still trying to open doors ")
					
					if not bMocap
					
						//MC_playerBD[ iLocalPart].fContainerOpenRatio = fAngleToOpenEveryFrame
						if MC_playerBD[ iLocalPart].fContainerOpenRatio < iAngleToOpenTo
							MC_playerBD[ iLocalPart].fContainerOpenRatio += fAngleToOpenEveryFrame
						endif 
					
					else 
					
						MC_playerBD[ iLocalPart].fContainerOpenRatio += fAngleToOpenEveryFrame
						
					endif 
					
					ATTACH_ENTITY_TO_ENTITY( NET_TO_OBJ( niLeftDoor ), 
											NET_TO_OBJ( niContainer ),
											-1,
											-vLeftDoorOffset,
											<<0,0, -MC_playerBD[ iLocalPart].fContainerOpenRatio>>,
											FALSE,
											FALSE, 
											TRUE )
					
					ATTACH_ENTITY_TO_ENTITY( NET_TO_OBJ( niRightDoor ),
											NET_TO_OBJ( niContainer ),
											-1,
											-vRightDoorOffset,
											<<0,0 ,MC_playerBD[ iLocalPart].fContainerOpenRatio>>,
											FALSE,
											FALSE, 
											TRUE )
					
				ENDIF
				
				PRINTLN("[RCC MISSION] OPEN_CONTAINER_HACK_DOORS current door ratio  = ", MC_playerBD[ iLocalPart].fContainerOpenRatio)					
				
			ENDIF
			
		#IF IS_DEBUG_BUILD
		else
			IF not NETWORK_HAS_CONTROL_OF_NETWORK_ID(niLeftDoor)
				PRINTLN("[RCC MISSION] NOT got control of assets -  Request LEFT door")
			endif
			
			if NETWORK_HAS_CONTROL_OF_NETWORK_ID(niRightDoor)
				PRINTLN("[RCC MISSION] NOT got control of assets -  Request RIGHT door")
			endif 
			
			if (NETWORK_HAS_CONTROL_OF_entity(carToDetach) or (not is_entity_attached(carToDetach)))
				PRINTLN("[RCC MISSION] got control of car or not is_entity_attached")
			else
				PRINTLN("[RCC MISSION] not got control of car or not is_entity_attached")
			endif
		#ENDIF
		ENDIF
	
	ENDIF

	RETURN bReturn
ENDFUNC



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: PLAYER HANDCUFF / SECONDARY ANIMATIONS (used in The Prison Break Finale heist) !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************



FUNC STRING GET_PLAYER_SECONDARY_ANIM_DICT(INT iAnim)
	
	STRING animDict = ""
	
	SWITCH iAnim
		CASE ciSECONDARY_ANIM_NONE
		
		BREAK
		CASE ciSECONDARY_ANIM_HANDCUFFED
			//Placeholder
			animDict = "mp_uncuff_paired"
			CPRINTLN(DEBUG_MISSION,"[pBUS] GET_PLAYER_SECONDARY_ANIM_DICT animDict = mp_uncuff_paired")

		BREAK
		
		CASE ciSECONDARY_ANIM_HANDCUFFED_ALT
			animDict =  "ANIM@HEISTS@PRISON_HEISTIG_2_P1_EXIT_BUS"
		BREAK
	ENDSWITCH
	
	RETURN animDict
	
ENDFUNC

FUNC STRING GET_PLAYER_SECONDARY_ANIM_NAME(INT iAnim)
	
	STRING animDict = ""
	
	SWITCH iAnim
		CASE ciSECONDARY_ANIM_NONE
			
		BREAK
		CASE ciSECONDARY_ANIM_HANDCUFFED
			//Placeholder
			animDict = "crook_02_p1_0"
		BREAK
	ENDSWITCH
	
	RETURN animDict
	
ENDFUNC

FUNC BOOL SHOULD_PLAYER_BREAK_OUT_OF_SECONDARY_ANIM(PLAYER_INDEX Player,bool bCheckWeaponChange = false)
	
	PED_INDEX tempPed = GET_PLAYER_PED(Player)
	
	if IS_PED_RUNNING(tempPed)
	or IS_PED_SPRINTING(tempPed)
		PRINTLN("[RCC MISSION] SHOULD_PLAYER_BREAK_OUT_OF_SECONDARY_ANIM, TRUE - Moving too quickly")
		RETURN TRUE
	ENDIF
	
	IF IS_PED_JUMPING(tempPed)
	OR IS_PED_VAULTING(tempPed)
	OR IS_PED_CLIMBING(tempPed)
		PRINTLN("[RCC MISSION] SHOULD_PLAYER_BREAK_OUT_OF_SECONDARY_ANIM, TRUE - Jumping/vaulting/climbing")
		RETURN TRUE
	ENDIF
	
	IF iSecondaryAnimHP > GET_ENTITY_HEALTH(LocalPlayerPed) + GET_PED_ARMOUR(LocalPlayerPed)
		PRINTLN("[RCC MISSION] SHOULD_PLAYER_BREAK_OUT_OF_SECONDARY_ANIM, TRUE - Taking damage")
		RETURN TRUE
	ENDIF
	
	if bCheckWeaponChange
		if IS_PLAYER_PRESSING_WEAPON_SWAP_BUTTON()
			PRINTLN("[RCC MISSION] SHOULD_PLAYER_BREAK_OUT_OF_SECONDARY_ANIM, TRUE - Weapon is swapped")
			RETURN TRUE
		ENDIF	
	else
		WEAPON_TYPE currentWeapon
		GET_CURRENT_PED_WEAPON(LocalPlayerPed,currentWeapon)
		IF NOT ((currentWeapon = WEAPONTYPE_UNARMED) OR (currentWeapon = WEAPONTYPE_OBJECT))
			PRINTLN("[RCC MISSION] SHOULD_PLAYER_BREAK_OUT_OF_SECONDARY_ANIM, TRUE - Weapon is out")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF GET_PED_STEALTH_MOVEMENT(tempPed)
		PRINTLN("[RCC MISSION] SHOULD_PLAYER_BREAK_OUT_OF_SECONDARY_ANIM, TRUE - Gone into stealth mode")
		RETURN TRUE
	ENDIF
	
	IF IS_PED_SWIMMING(tempPed)
		PRINTLN("[RCC MISSION] SHOULD_PLAYER_BREAK_OUT_OF_SECONDARY_ANIM, TRUE - Swimming")
		RETURN TRUE
	ENDIF
	
	IF IS_PED_IN_MELEE_COMBAT(tempPed)
	OR IS_PED_PERFORMING_MELEE_ACTION(tempPed)
		PRINTLN("[RCC MISSION] SHOULD_PLAYER_BREAK_OUT_OF_SECONDARY_ANIM, TRUE - In melee combat")
		RETURN TRUE
	ENDIF
	
	IF IS_PED_GOING_INTO_COVER(tempPed)
	OR IS_PED_IN_COVER(tempPed)
		PRINTLN("[RCC MISSION] SHOULD_PLAYER_BREAK_OUT_OF_SECONDARY_ANIM, TRUE - Using cover")
		RETURN TRUE
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(tempPed,TRUE)
		PRINTLN("[RCC MISSION] SHOULD_PLAYER_BREAK_OUT_OF_SECONDARY_ANIM, TRUE - Using vehicle")
		RETURN TRUE
	ENDIF
	
	IF IS_ENTITY_ON_FIRE(tempPed)
		PRINTLN("[RCC MISSION] SHOULD_PLAYER_BREAK_OUT_OF_SECONDARY_ANIM, TRUE - On fire")
		RETURN TRUE
	ENDIF
	
	IF IS_ANY_INTERACTION_ANIM_PLAYING()
		PRINTLN("[RCC MISSION] SHOULD_PLAYER_BREAK_OUT_OF_SECONDARY_ANIM, TRUE - Interaction anim playing")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_PLAYER_SECONDARY_ANIMS()
		
	IF iSpectatorTarget = -1
	AND bPlayerToUseOK
		IF MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
			
			IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iPartToUse].iteam)
				CPRINTLN(DEBUG_MISSION,"[pBUS] Team has new priority this frame clear bit PBBOOL_DROPPED_OUT_OF_SECONDARY_ANIM")
				CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_DROPPED_OUT_OF_SECONDARY_ANIM)
			ENDIF
			
			IF (g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iSecondaryAnim[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] != ciSECONDARY_ANIM_NONE)
			OR iSecondaryAnimPlaying != -1
				IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_DROPPED_OUT_OF_SECONDARY_ANIM)
										
					INT iAnim = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iSecondaryAnim[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]]
					
					CPRINTLN(DEBUG_MISSION,"[pBUS][PROCESS_PLAYER_SECONDARY_ANIMS] iAnim: ", iAnim, " iSecondaryAnimPlaying: ", iSecondaryAnimPlaying)
					
					IF iAnim = ciSECONDARY_ANIM_HANDCUFFED_ALT
						SET_BIT(iLocalBoolCheck29, LBOOL29_SET_REMOTE_PLAYER_SYNC_SCENE_FOR_SEC_ANIM)
						NETWORK_ALLOW_REMOTE_SYNCED_SCENE_LOCAL_PLAYER_REQUESTS(TRUE)
					ELSE
						NETWORK_ALLOW_REMOTE_SYNCED_SCENE_LOCAL_PLAYER_REQUESTS(FALSE)
					ENDIF
					IF iSecondaryAnimPlaying != -1
						IF iAnim != -1
							IF iSecondaryAnimPlaying != iAnim
								//New secondary anim:
								CPRINTLN(DEBUG_MISSION,"[pBUS] New rule has a different secondary anim")
								RESET_PED_MOVEMENT_CLIPSET(LocalPlayerPed)
								RESET_PED_WEAPON_MOVEMENT_CLIPSET(LocalPlayerPed)
								if HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_HANDCUFFS)
									REMOVE_WEAPON_FROM_PED(LocalPlayerPed,WEAPONTYPE_DLC_HANDCUFFS)
								endif
								CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet2, PBBOOL2_BLOCK_SECONDARY_AND_IDLE_ANIMS)
								if iAnim != ciSECONDARY_ANIM_HANDCUFFED_ALT
									if IS_ENTITY_PLAYING_ANIM(LocalPlayerPed,GET_PLAYER_SECONDARY_ANIM_DICT(iSecondaryAnimPlaying),GET_PLAYER_SECONDARY_ANIM_NAME(iSecondaryAnimPlaying))
										STOP_ANIM_TASK(LocalPlayerPed,GET_PLAYER_SECONDARY_ANIM_DICT(iSecondaryAnimPlaying),GET_PLAYER_SECONDARY_ANIM_NAME(iSecondaryAnimPlaying),SLOW_BLEND_OUT)
									endif
								endif
								DISPLAY_RADAR(true)	
								iSecondaryAnimPlaying = -1
							ENDIF
						ELSE
							iAnim = iSecondaryAnimPlaying
						ENDIF
					ENDIF
					
					STRING animDict = GET_PLAYER_SECONDARY_ANIM_DICT(iAnim)
					STRING animStr = GET_PLAYER_SECONDARY_ANIM_NAME(iAnim)
					
					bool bDoBusExit = true
					if iAnim != ciSECONDARY_ANIM_HANDCUFFED_ALT
						bDoBusExit = false							
					endif
					
					IF iSecondaryAnimPlaying = -1
												
						if bDoBusExit
							REQUEST_ANIM_DICT(animDict)	
							if GET_PLAYER_WANTED_LEVEL(LocalPlayer) = 0
								DISABLE_PLAYER_CONTROLS_THIS_FRAME()
								DISABLE_CELLPHONE_THIS_FRAME_ONLY()
								REQUEST_CLIP_SET("MOVE_M@PRISON_GAURD")
//								REQUEST_CLIP_SET("MOVE_M@PRISONER_CUFFED")
								
								IF HAS_ANIM_DICT_LOADED(animDict)
								and HAS_CLIP_SET_LOADED("MOVE_M@PRISON_GAURD")
//								and HAS_CLIP_SET_LOADED("MOVE_M@PRISONER_CUFFED")
								
									iSecondaryAnimHP = GET_ENTITY_HEALTH(LocalPlayerPed) + GET_PED_ARMOUR(LocalPlayerPed)
																		
									//check if in a vehicle 
									IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
									
										VEHICLE_INDEX vehPrisonBus = GET_VEHICLE_PED_IS_IN(LocalPlayerPed,true)									
										//if prison bus
										if GET_ENTITY_MODEL(vehPrisonBus) = pbus
											if IS_VEHICLE_STOPPED(vehPrisonBus)
											
												if not IS_SYNC_SCENE_RUNNING(MC_playerBD[iPartToUse].iSynchSceneID)	
												and GET_SEAT_PED_IS_IN(LocalPlayerPed) = VS_DRIVER												
													PED_INDEX pedBackSeat 	= GET_PED_IN_VEHICLE_SEAT(vehPrisonBus,VS_FRONT_RIGHT)	
													CLEAR_PED_TASKS(LocalPlayerPed)
													CLEAR_PED_TASKS(pedBackSeat)
													
													MC_playerBD[iPartToUse].iSynchSceneID  = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_ENTITY_COORDS(vehPrisonBus), GET_ENTITY_ROTATION(vehPrisonBus))
													NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(LocalPlayerPed,MC_playerBD[iPartToUse].iSynchSceneID,"ANIM@HEISTS@PRISON_HEISTIG_2_P1_EXIT_BUS","exit_bus_player_guard",INSTANT_BLEND_IN,REALLY_SLOW_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_SET_PED_OUT_OF_VEHICLE_AT_START)
													NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedBackSeat,MC_playerBD[iPartToUse].iSynchSceneID,"ANIM@HEISTS@PRISON_HEISTIG_2_P1_EXIT_BUS","exit_bus_player_prisoner",INSTANT_BLEND_IN,REALLY_SLOW_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_SET_PED_OUT_OF_VEHICLE_AT_START)
													NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(vehPrisonBus,MC_playerBD[iPartToUse].iSynchSceneID,"ANIM@HEISTS@PRISON_HEISTIG_2_P1_EXIT_BUS","exit_bus_bus",INSTANT_BLEND_IN,NORMAL_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT)
													NETWORK_ADD_SYNCHRONISED_SCENE_CAMERA(MC_playerBD[iPartToUse].iSynchSceneID,"ANIM@HEISTS@PRISON_HEISTIG_2_P1_EXIT_BUS","exit_bus_cam")
													NETWORK_FORCE_LOCAL_USE_OF_SYNCED_SCENE_CAMERA(MC_playerBD[iPartToUse].iSynchSceneID)	
													NETWORK_START_SYNCHRONISED_SCENE(MC_playerBD[iPartToUse].iSynchSceneID)
													
													if HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_SPECIALCARBINE)
														SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_SPECIALCARBINE, TRUE)	
													ENDIF
																																				
													SET_PED_MOVEMENT_CLIPSET(LocalPlayerPed,"MOVE_M@PRISON_GAURD")
													SET_PED_WEAPON_MOVEMENT_CLIPSET(LocalPlayerPed,"MOVE_M@PRISON_GAURD")
													DISPLAY_RADAR(false)
													
													iSecondaryAnimPlaying = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iSecondaryAnim[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]]
													FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed)
													
													CPRINTLN(DEBUG_MISSION,"[pBUS] VS_DRIVER - Starting secondary anim: ", iSecondaryAnimPlaying)
												ENDIF											
											ELSE
												if GET_SEAT_PED_IS_IN(LocalPlayerPed) = VS_DRIVER	//only driver can stop the bus
												and NETWORK_HAS_CONTROL_OF_ENTITY(vehPrisonBus)
													BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehPrisonBus,0.5,6)
												endif
											ENDIF
										endif
									else	
										
										int iOtherParts									
										for iOtherParts = 0 to GET_MAXIMUM_PLAYERS_ON_MISSION()-1
											if iOtherParts != iPartToUse
												if IS_SYNC_SCENE_RUNNING(MC_playerBD[iOtherParts].iSynchSceneID)
													MC_playerBD[iPartToUse].iSynchSceneID = MC_playerBD[iOtherParts].iSynchSceneID
													CPRINTLN(DEBUG_MISSION,"[pBUS] IS_SYNC_SCENE_RUNNING - participant running scene:",iOtherParts," sceneID: ",MC_playerBD[iPartToUse].iSynchSceneID)
												endif
											endif
										ENDFOR
										
										IF IS_SYNC_SCENE_RUNNING(MC_playerBD[iPartToUse].iSynchSceneID)
											DISABLE_CELLPHONE_THIS_FRAME_ONLY()
											DISPLAY_RADAR(false)
											CPRINTLN(DEBUG_MISSION,"[pBUS] IS_PED_IN_ANY_VEHICLE - not in veh")
											if IS_ENTITY_PLAYING_ANIM(LocalPlayerPed,"ANIM@HEISTS@PRISON_HEISTIG_2_P1_EXIT_BUS","exit_bus_player_prisoner")
												CPRINTLN(DEBUG_MISSION,"[pBUS]Playing anim exit_bus_player_prisoner :1 ")
												if HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_HANDCUFFS)
													WEAPON_TYPE wt = WEAPONTYPE_DLC_HANDCUFFS
													if not GET_CURRENT_PED_WEAPON(LocalPlayerPed,wt)									
														SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_HANDCUFFS, TRUE)	
													ENDIF
												else
													GIVE_WEAPON_TO_PED(LocalPlayerPed, WEAPONTYPE_DLC_HANDCUFFS,0,true)
												endif
											endif
											//Apply handcuffs now to the ped
											scrShopPedComponent componentItem												
											if GET_ENTITY_MODEL(LocalPlayerPed) = MP_M_FREEMODE_01	
												GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TEETH_5_0), componentItem)
											elif GET_ENTITY_MODEL(LocalPlayerPed) = MP_F_FREEMODE_01
												GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TEETH_6_0), componentItem)
											ENDIF		
											
											INT icurrentDrawID = GET_PED_DRAWABLE_VARIATION(LocalPlayerPed,PED_COMP_TEETH)												
											CPRINTLN(DEBUG_MISSION,"[pBUS]icurrentDrawID teeth: ",icurrentDrawID)
											CPRINTLN(DEBUG_MISSION,"[pBUS]componentItem.m_drawableIndex teeth: ",componentItem.m_drawableIndex)
											IF icurrentDrawID != componentItem.m_drawableIndex
												bFinalizePedpBus = true
												SET_PED_COMPONENT_VARIATION(LocalPlayerPed,PED_COMP_TEETH,componentItem.m_drawableIndex,componentItem.m_textureIndex)
												CPRINTLN(DEBUG_MISSION,"[pBUS]Adding handcuff Comps here 1 ")
											ENDIF
											
											iSecondaryAnimPlaying = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iSecondaryAnim[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]]
											CPRINTLN(DEBUG_MISSION,"[pBUS]IS_PED_IN_ANY_VEHICLE - not in veh Starting secondary anim: ", iSecondaryAnimPlaying)
										ENDIF
									endif
								endif
							ELSE
								SET_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_DROPPED_OUT_OF_SECONDARY_ANIM)
							ENDIF
						else
							if not IS_STRING_NULL_OR_EMPTY(animDict)
								REQUEST_ANIM_DICT(animDict)
								IF HAS_ANIM_DICT_LOADED(animDict)
									IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
										iSecondaryAnimHP = GET_ENTITY_HEALTH(LocalPlayerPed) + GET_PED_ARMOUR(LocalPlayerPed)
										SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED, TRUE)									
										TASK_PLAY_ANIM(LocalPlayerPed,animDict,animStr,SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_SECONDARY | AF_UPPERBODY | AF_EXIT_AFTER_INTERRUPTED | AF_ABORT_ON_WEAPON_DAMAGE | AF_LOOPING)
										REMOVE_ANIM_DICT(animDict)
										iSecondaryAnimPlaying = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iSecondaryAnim[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]]
										CPRINTLN(DEBUG_MISSION,"[pBUS] PROCESS_PLAYER_SECONDARY_ANIMS - Starting secondary anim", iSecondaryAnimPlaying)
									ENDIF
								ENDIF
							endif
						endif
					ELSE
						
						if bDoBusExit
							IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
								DISABLE_PLAYER_CONTROLS_THIS_FRAME()
							endif
							
							IF  IS_SYNC_SCENE_RUNNING(MC_playerBD[iPartToUse].iSynchSceneID)
								DISABLE_PLAYER_CONTROLS_THIS_FRAME()
								DISABLE_CELLPHONE_THIS_FRAME_ONLY()
								DISABLE_INTERACTION_MENU()
								
								//Force gun into the guards hand every frame to make sure the gun gets equiped
								if IS_ENTITY_PLAYING_ANIM(LocalPlayerPed,"ANIM@HEISTS@PRISON_HEISTIG_2_P1_EXIT_BUS","exit_bus_player_guard")
									if HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_SPECIALCARBINE)
										WEAPON_TYPE wt = WEAPONTYPE_DLC_SPECIALCARBINE
										if not GET_CURRENT_PED_WEAPON(LocalPlayerPed,wt)									
											SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_SPECIALCARBINE, TRUE)	
										ENDIF
									else
										GIVE_WEAPON_TO_PED(LocalPlayerPed, WEAPONTYPE_DLC_SPECIALCARBINE,25,true)
									endif
								endif								
								//force weapon type handcuffs
								if IS_ENTITY_PLAYING_ANIM(LocalPlayerPed,"ANIM@HEISTS@PRISON_HEISTIG_2_P1_EXIT_BUS","exit_bus_player_prisoner")
									CPRINTLN(DEBUG_MISSION,"[pBUS]Playing anim exit_bus_player_prisoner:2 ")
									if HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_HANDCUFFS)
										WEAPON_TYPE wt = WEAPONTYPE_DLC_HANDCUFFS
										if not GET_CURRENT_PED_WEAPON(LocalPlayerPed,wt)									
											SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_HANDCUFFS, TRUE)	
										ENDIF
									else
										GIVE_WEAPON_TO_PED(LocalPlayerPed, WEAPONTYPE_DLC_HANDCUFFS,0,true)
									endif
									//Apply handcuffs now to the ped if not already
									scrShopPedComponent componentItem												
									if GET_ENTITY_MODEL(LocalPlayerPed) = MP_M_FREEMODE_01													
										GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TEETH_5_0), componentItem)
									elif GET_ENTITY_MODEL(LocalPlayerPed) = MP_F_FREEMODE_01
										GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TEETH_6_0), componentItem)
									ENDIF	
									INT icurrentDrawID = GET_PED_DRAWABLE_VARIATION(LocalPlayerPed,PED_COMP_TEETH)												
									IF icurrentDrawID != componentItem.m_drawableIndex
										bFinalizePedpbus = true
										SET_PED_COMPONENT_VARIATION(LocalPlayerPed,PED_COMP_TEETH,componentItem.m_drawableIndex,componentItem.m_textureIndex)
										CPRINTLN(DEBUG_MISSION,"[pBUS]Adding handcuff Comps here :2 ")
									ENDIF
								endif
								
								// catch up cam setup
								IF DOES_CAM_EXIST(ScriptedCatchupCam)
									SET_CAM_PARAMS(ScriptedCatchupCam, GET_FINAL_RENDERED_CAM_COORD(), GET_FINAL_RENDERED_CAM_ROT(), GET_FINAL_RENDERED_CAM_FOV())										
								endif							
								int iThisLocalsync = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID)									
								IF iThisLocalsync != -1
									if GET_SYNCHRONIZED_SCENE_PHASE(iThisLocalsync) >= 0.9										
										SET_CAM_ACTIVE(ScriptedCatchupCam, TRUE)										
										RENDER_SCRIPT_CAMS(TRUE, FALSE)	
										CPRINTLN(DEBUG_MISSION,"[pBUS] SET_CAM_ACTIVE(ScriptedCatchupCam, TRUE)")
									else
										IF not DOES_CAM_EXIST(ScriptedCatchupCam)
											ScriptedCatchupCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE) 		
											CPRINTLN(DEBUG_MISSION,"[pBUS] ScriptedCatchupCam CREATE_CAM")
										endif									
									endif
								endif
								
							else
								IF( MC_playerBD[iPartToUse].iSynchSceneID <> -1 )
									//Reset syncScene ID
									MC_playerBD[iPartToUse].iSynchSceneID = -1
									
									//kill cam if it exists
									IF DOES_CAM_EXIST(ScriptedCatchupCam)	
										DISPLAY_RADAR(true)
										if GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
											SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)	
											SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)	
										ENDIF
										if !g_bCelebrationScreenIsActive
											STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP(TRUE)
										else
											RENDER_SCRIPT_CAMS(FALSE, FALSE)	
										endif
										DESTROY_CAM(ScriptedCatchupCam)			
										ENABLE_INTERACTION_MENU()
										CPRINTLN(DEBUG_MISSION,"[pBUS] DESTROY_CAM from sync scene")
									ENDIF	
								ENDIF
								
								if bFinalizePedpBus
									If	HAVE_ALL_STREAMING_REQUESTS_COMPLETED(LocalPlayerPed)
									and HAS_PED_HEAD_BLEND_FINISHED(LocalPlayerPed)
										FINALIZE_HEAD_BLEND(LocalPlayerPed)
										bFinalizePedpBus = false
										CPRINTLN(DEBUG_MISSION,"[pBUS] FINALIZE_HEAD_BLEND")
									endif
								endif
								
								// if player wants to break out of clipset
								IF SHOULD_PLAYER_BREAK_OUT_OF_SECONDARY_ANIM(LocalPlayer,true)
									SET_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_DROPPED_OUT_OF_SECONDARY_ANIM)
									CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet2, PBBOOL2_BLOCK_SECONDARY_AND_IDLE_ANIMS)
									RESET_PED_WEAPON_MOVEMENT_CLIPSET(LocalPlayerPed)
									RESET_PED_MOVEMENT_CLIPSET(LocalPlayerPed)	
									if HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_HANDCUFFS)
										REMOVE_WEAPON_FROM_PED(LocalPlayerPed,WEAPONTYPE_DLC_HANDCUFFS)
									endif
									DISPLAY_RADAR(true)
									iSecondaryAnimPlaying = -1		
									CPRINTLN(DEBUG_MISSION,"[pBUS] SHOULD_PLAYER_BREAK_OUT_OF_SECONDARY_ANIM - Player has dropped out of anim!")								
								ELSE
									IF not IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SPRINT)
										SET_PED_MAX_MOVE_BLEND_RATIO(LocalPlayerPed,PEDMOVE_WALK)
									ENDIF
									SET_BIT(MC_playerBD[iPartToUse].iClientBitSet2, PBBOOL2_BLOCK_SECONDARY_AND_IDLE_ANIMS)
									// Disable pointing - url:bugstar:2144848
									DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_SECONDARY)
								ENDIF
							endif
						
						else // not bus related
							IF IS_ENTITY_PLAYING_ANIM(LocalPlayerPed,animDict,animStr)						
								IF SHOULD_PLAYER_BREAK_OUT_OF_SECONDARY_ANIM(LocalPlayer)
									STOP_ANIM_TASK(LocalPlayerPed,animDict,animStr,SLOW_BLEND_OUT)	
									RESET_PED_MOVEMENT_CLIPSET(LocalPlayerPed)
									RESET_PED_WEAPON_MOVEMENT_CLIPSET(LocalPlayerPed)
									SET_PED_CAN_PLAY_AMBIENT_ANIMS(LocalPlayerPed,true) 
									SET_PED_FLEE_ATTRIBUTES(LocalPlayerPed, FA_DISABLE_AMBIENT_CLIPS, false) 
									
									CPRINTLN(DEBUG_MISSION,"[pBUS] PROCESS_PLAYER_SECONDARY_ANIMS - set stopping anim and clipset!")
								ENDIF
							else								
								CPRINTLN(DEBUG_MISSION,"[pBUS] PROCESS_PLAYER_SECONDARY_ANIMS - set stopping anim and clipset!")
								SET_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_DROPPED_OUT_OF_SECONDARY_ANIM)
								iSecondaryAnimPlaying = -1	
							endif
						endif						
					ENDIF					
				ELSE
					IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_SET_REMOTE_PLAYER_SYNC_SCENE_FOR_SEC_ANIM)
						CLEAR_BIT(iLocalBoolCheck29, LBOOL29_SET_REMOTE_PLAYER_SYNC_SCENE_FOR_SEC_ANIM)
						NETWORK_ALLOW_REMOTE_SYNCED_SCENE_LOCAL_PLAYER_REQUESTS(FALSE)
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_SET_REMOTE_PLAYER_SYNC_SCENE_FOR_SEC_ANIM)
					CLEAR_BIT(iLocalBoolCheck29, LBOOL29_SET_REMOTE_PLAYER_SYNC_SCENE_FOR_SEC_ANIM)
					NETWORK_ALLOW_REMOTE_SYNCED_SCENE_LOCAL_PLAYER_REQUESTS(FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: ARTIFICIAL LIGHTS / EMP EFFECT (from The Humane Labs Raid) !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************



PROC RESET_ARTIFICIAL_LIGHTS_SERVER_DATA()
	
	STRUCT_ATRICIFICIAL_LIGHTS_DATA sTempData
	MC_serverBD_2.sArtificialLightsData = sTempData
	
	#IF IS_DEBUG_BUILD
		bActivateArtificialLightsDebug = FALSE
		bPrintArticificalLightTimerValue = FALSE
		iDebugArtificialLightsOffDuration = -1
	#ENDIF
	
	PRINTLN("[RCC MISSION] - [ARTIFICIAL LIGHTS] - [SERVER] - reset articificial lights data.")
	
ENDPROC

PROC PROCESS_ARTIFICIAL_LIGHTS_SERVER(INT iTeam, INT iRule)
	
	INT iTime = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iArtificalLightTimer[iRule]
	
	#IF IS_DEBUG_BUILD
		IF bPrintArticificalLightTimerValue
			PRINTLN("[RCC MISSION] - [ARTIFICIAL LIGHTS] - [SERVER] - iArtificalLightTimer value = ", iTime)
		ENDIF
		IF bActivateArtificialLightsDebug
			iTime = iDebugArtificialLightsOffDuration
		ENDIF
	#ENDIF
	
	IF NOT Is_Player_Currently_On_MP_Heist(PLAYER_ID())
		IF iTime = FMMC_ARTIFICIAL_LIGHTS_ALWAYS_ON // Always On	
			RESET_NET_TIMER(MC_serverBD_2.sArtificialLightsData.sDelayTimer)
			MC_serverBD_2.sArtificialLightsData.eState = eARTIFICALLIGHTSSTATE_ON
			PRINTLN("[RCC MISSION] - [ARTIFICIAL LIGHTS] - [SERVER] - Turning Lights back on!")
		ELSE
			PRINTLN("[RCC MISSION] - [ARTIFICIAL LIGHTS] - [SERVER] - Getting ready to turn Lights off.")
		ENDIF
	ENDIF
	
	SWITCH MC_serverBD_2.sArtificialLightsData.eState
		
		CASE eARTIFICALLIGHTSSTATE_ON
			
			IF NOT HAS_NET_TIMER_STARTED(MC_serverBD_2.sArtificialLightsData.sDelayTimer)
				IF iTime != FMMC_ARTIFICIAL_LIGHTS_ALWAYS_ON
				AND iTime > 0
					MC_serverBD_2.sArtificialLightsData.iDelayDuration = iTime
					START_NET_TIMER(MC_serverBD_2.sArtificialLightsData.sDelayTimer)
					MC_serverBD_2.sArtificialLightsData.eState = eARTIFICALLIGHTSSTATE_WAITING_TO_TURN_OFF
					
					PRINTLN("[RCC MISSION] - [ARTIFICIAL LIGHTS] - [SERVER] - team ", iTeam, ", rule ", iRule, ", iDelayDuration = ", MC_serverBD_2.sArtificialLightsData.iDelayDuration)
					PRINTLN("[RCC MISSION] - [ARTIFICIAL LIGHTS] - [SERVER] - started delay timer.")
					PRINTLN("[RCC MISSION] - [ARTIFICIAL LIGHTS] - [SERVER] - going to waiting to turn off.")
				ENDIF	
			ENDIF
			
		BREAK
		
		CASE eARTIFICALLIGHTSSTATE_WAITING_TO_TURN_OFF
			
			IF HAS_NET_TIMER_EXPIRED(MC_serverBD_2.sArtificialLightsData.sDelayTimer, MC_serverBD_2.sArtificialLightsData.iDelayDuration)
				MC_serverBD_2.sArtificialLightsData.eState = eARTIFICALLIGHTSSTATE_OFF
				PRINTLN("[RCC MISSION] - [ARTIFICIAL LIGHTS] - [SERVER] - iDelayDuration = ", MC_serverBD_2.sArtificialLightsData.iDelayDuration)
				PRINTLN("[RCC MISSION] - [ARTIFICIAL LIGHTS] - [SERVER] - going to state off.")
			ENDIF
			
		BREAK
		
		CASE eARTIFICALLIGHTSSTATE_OFF
			// In state off. 
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC PROCESS_ARTIFICIAL_LIGHTS_CLIENT()
	
	IF IS_BIT_SET(iLocalBoolCheck5, LBOOL5_BLOCK_TURNING_OFF_ARTIFICIAL_LIGHTS_EOM)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciTURN_OFF_LIGHTS_AT_START)
		IF NOT IS_BIT_SET(iLocalBoolCheck5, LBOOL5_TURNED_OFF_ARTIFICIAL_LIGHTS)
			SET_ARTIFICIAL_LIGHTS_STATE(TRUE)
			
			IF NOT IS_PHONE_EMP_CURRENTLY_ACTIVE()
				SET_TIMECYCLE_MODIFIER("NoPedLight")
				SET_TIMECYCLE_MODIFIER_STRENGTH(1.0)
			ENDIF
			
			IF CONTENT_IS_USING_CASINO_HEIST_INTERIOR()
				SET_TIMECYCLE_MODIFIER("Casino_Lightsoff")
				SET_TIMECYCLE_MODIFIER_STRENGTH(1.0)
				PRINTLN("[RCC MISSION] - [ARTIFICIAL LIGHTS] - [CLIENT] - Turning on Casino_Lightsoff timecycle modifier.")
			ENDIF
			
			g_bFMMCLightsTurnedOff = TRUE
			SET_BIT(iLocalBoolCheck5, LBOOL5_TURNED_OFF_ARTIFICIAL_LIGHTS)
			PRINTLN("[RCC MISSION] - [ARTIFICIAL LIGHTS] - [CLIENT] - Dawn Raid - Turning off lights.")
		ENDIF

		EXIT
	ENDIF
	
	// Maintain lights going on/off.
	SWITCH MC_serverBD_2.sArtificialLightsData.eState
		
		CASE eARTIFICALLIGHTSSTATE_ON
			IF IS_BIT_SET(iLocalBoolCheck5, LBOOL5_TURNED_OFF_ARTIFICIAL_LIGHTS)
				SET_ARTIFICIAL_LIGHTS_STATE(FALSE)
				g_bFMMCLightsTurnedOff = FALSE
				OVERRIDE_INTERIOR_SMOKE_END()
				CLEAR_TIMECYCLE_MODIFIER()
				CLEAR_ALL_TCMODIFIER_OVERRIDES("lab_none_dark")
				CLEAR_ALL_TCMODIFIER_OVERRIDES("lab_none_exit")
				CLEAR_ALL_TCMODIFIER_OVERRIDES("morgue_dark")
				CLEAR_BIT(iLocalBoolCheck5, LBOOL5_TURNED_OFF_ARTIFICIAL_LIGHTS)
				PRINTLN("[RCC MISSION] - [ARTIFICIAL LIGHTS] - [CLIENT] - server says artificial lights state is on. Turning on lights.")
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_ENABLE_ALTERNATE_LIGHTS_OFF_AUDIO)
					IF REQUEST_SCRIPT_AUDIO_BANK("DLC_CHRISTMAS2017/XM_STEALAVG")
						PLAY_SOUND_FRONTEND(-1, "lights_on", "dlc_xm_stealavg_sounds", TRUE)
					ELSE
						PRINTLN("[RCC MISSION] - [ARTIFICIAL LIGHTS] - [CLIENT] - REQUEST_SCRIPT_AUDIO_BANK(DLC_HALLOWEEN/FVJ_01) = FALSE")	
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE eARTIFICALLIGHTSSTATE_WAITING_TO_TURN_OFF
			IF NOT IS_BIT_SET(iLocalBoolCheck5, LBOOL5_TRIGGERED_EMP_AUDIO)
				IF( FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(1) ) // B* 2222484
					SET_BIT(iLocalBoolCheck5, LBOOL5_TRIGGERED_EMP_AUDIO)
				ELSE
					IF AM_I_ON_A_HEIST()
						IF REQUEST_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_EMP_BOMB")
							IF HAS_NET_TIMER_EXPIRED(MC_serverBD_2.sArtificialLightsData.sDelayTimer, MC_serverBD_2.sArtificialLightsData.iDelayDuration - 3000) // Launch audio 3 seconds before the emp is going to fire. 
								iEmpSoundId = GET_SOUND_ID()
								START_AUDIO_SCENE("DLC_HEISTS_BIOLAB_EMP_BLAST_SCENE")
								PLAY_SOUND_FRONTEND(iEmpSoundId, "EMP_Blast", "DLC_HEISTS_BIOLAB_FINALE_SOUNDS", FALSE)
								SET_BIT(iLocalBoolCheck5, LBOOL5_TRIGGERED_EMP_AUDIO)
							ENDIF
						ELSE
							PRINTLN("[RCC MISSION] - [ARTIFICIAL LIGHTS] - [CLIENT] - REQUEST_SCRIPT_AUDIO_BANK(DLC_MPHEIST/HEIST_EMP_BOMB) = FALSE")
						ENDIF
					ELSE
						//	On freddy versus jason MP mission						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_SLASHERS_AUDIO)
							
						ELSE
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_ENABLE_ALTERNATE_LIGHTS_OFF_AUDIO)
								IF REQUEST_SCRIPT_AUDIO_BANK("DLC_HALLOWEEN/FVJ_01")
									PRINTLN( "[RCC MISSION] - PROCESS_ARTIFICIAL_LIGHTS_CLIENT - eARTIFICALLIGHTSSTATE_WAITING_TO_TURN_OFF - Not on heist.")
									PRINTLN( "[RCC MISSION] - PLAY_SOUND_FRONTEND(-1, EMP, DLC_HALLOWEEN_FVJ_Sounds, FALSE)")
									iEmpSoundId = GET_SOUND_ID()
									PLAY_SOUND_FRONTEND(iEmpSoundId, "EMP", "DLC_HALLOWEEN_FVJ_Sounds", FALSE)
									SET_BIT(iLocalBoolCheck5, LBOOL5_TRIGGERED_EMP_AUDIO)
								ELSE
									PRINTLN("[RCC MISSION] - [ARTIFICIAL LIGHTS] - [CLIENT] - REQUEST_SCRIPT_AUDIO_BANK(DLC_HALLOWEEN/FVJ_01) = FALSE")	
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE eARTIFICALLIGHTSSTATE_OFF		
			IF NOT IS_BIT_SET(iLocalBoolCheck5, LBOOL5_TURNED_OFF_ARTIFICIAL_LIGHTS)
				SET_ARTIFICIAL_LIGHTS_STATE(TRUE)
				g_bFMMCLightsTurnedOff = TRUE
				SET_BIT(iLocalBoolCheck5, LBOOL5_TURNED_OFF_ARTIFICIAL_LIGHTS)
				PRINTLN("[RCC MISSION] - [ARTIFICIAL LIGHTS] - [CLIENT] - server says artificial lights state is off. Turning off lights. Will be setting timecycle modifier NoPedLight from now on.")
			ENDIF
			
			INTERIOR_INSTANCE_INDEX playerInterior 
			playerInterior = GET_INTERIOR_FROM_ENTITY(LocalPlayerPed)
			VECTOR vPos
			INT interiorNameHash 
			interiorNameHash = -1
			IF IS_VALID_INTERIOR(playerInterior)
				GET_INTERIOR_LOCATION_AND_NAMEHASH(playerInterior, vPos, interiorNameHash)
			ELSE
				playerInterior = NULL
			ENDIF
			UNUSED_PARAMETER(vPos)
			
			IF (IS_THIS_ROCKSTAR_MISSION_NEW_VS_SLASHER(g_FMMC_STRUCT.iAdversaryModeType) OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_SLASHERS(g_FMMC_STRUCT.iAdversaryModeType))
			AND (IS_PLAYER_IN_BUNKER(PLAYER_ID()) OR interiorNameHash = HASH("gr_grdlc_int_02"))
				SET_TIMECYCLE_MODIFIER("grdlc_int_02")
				SET_TIMECYCLE_MODIFIER_STRENGTH(1.0)
				SET_EXTRA_TCMODIFIER("NoPedLight")
				ADD_TCMODIFIER_OVERRIDE("lab_none_exit","lab_none_exit_OVR")
				ADD_TCMODIFIER_OVERRIDE("lab_none_dark","lab_none_dark_OVR")
				
			ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_SLASHERS(g_FMMC_STRUCT.iAdversaryModeType)
				PRINTLN("[RCC MISSION] - [ARTIFICIAL LIGHTS] - [CLIENT] - (SLASHERS)")
				SET_TIMECYCLE_MODIFIER("grdlc_int_02")
				SET_EXTRA_TCMODIFIER("NoPedLight")
				SET_TIMECYCLE_MODIFIER_STRENGTH(1.0)
				
			ELSE
				IF NOT IS_PHONE_EMP_CURRENTLY_ACTIVE()
					PRINTLN("[RCC MISSION] - [ARTIFICIAL LIGHTS] - [CLIENT] - (Default)")
					SET_TIMECYCLE_MODIFIER("NoPedLight")
					SET_TIMECYCLE_MODIFIER_STRENGTH(1.0)
				ENDIF
			ENDIF
			
			IF CONTENT_IS_USING_CASINO_HEIST_INTERIOR()
				SET_TIMECYCLE_MODIFIER("Casino_Lightsoff")
				SET_TIMECYCLE_MODIFIER_STRENGTH(1.0)
				PRINTLN("[RCC MISSION] - [ARTIFICIAL LIGHTS] - [CLIENT] - Turning on Casino_Lightsoff timecycle modifier. (2)")
			ENDIF
			
			IF NOT IS_BIT_SET(iLocalBoolCheck5, LBOOL5_TRIGGERED_EMP_AUDIO)
			AND NOT FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(1)
			AND NOT AM_I_ON_A_HEIST()
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_SLASHERS_AUDIO)
					PRINTLN( "[RCC MISSION] - PROCESS_ARTIFICIAL_LIGHTS_CLIENT - eARTIFICALLIGHTSSTATE_WAITING_TO_TURN_OFF - Not on heist.")
					PRINTLN( "[RCC MISSION] - PLAY_SOUND_FRONTEND(-1, EMP, dlc_xm_sls_Sounds, FALSE)")
					iEmpSoundId = GET_SOUND_ID()
					STRING sSoundSet 
					sSoundSet = "dlc_xm_sls_Sounds"
					PLAY_SOUND_FRONTEND(iEmpSoundId, "EMP", sSoundSet, FALSE)
					SET_BIT(iLocalBoolCheck5, LBOOL5_TRIGGERED_EMP_AUDIO)
				ELSE				
					STRING sAudioBank, sSoundSet, sSoundName
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_ENABLE_ALTERNATE_LIGHTS_OFF_AUDIO)
						sAudiobank = "DLC_CHRISTMAS2017/XM_STEALAVG"
						sSoundSet = "dlc_xm_stealavg_sounds"
						sSoundName = "lights_off"
					ELSE
						sAudioBank = "DLC_HALLOWEEN/FVJ_01"
						sSoundSet = "DLC_HALLOWEEN_FVJ_Sounds"
						sSoundName = "EMP"
					ENDIF
					IF REQUEST_SCRIPT_AUDIO_BANK(sAudiobank)
						PRINTLN( "[RCC MISSION] - PROCESS_ARTIFICIAL_LIGHTS_CLIENT - eARTIFICALLIGHTSSTATE_OFF - Not on heist.")
						PRINTLN( "[RCC MISSION] - PLAY_SOUND_FRONTEND(-1, ",sSoundName,", ",sSoundSet,", FALSE)")
						iEmpSoundId = GET_SOUND_ID()
						PLAY_SOUND_FRONTEND(iEmpSoundId, sSoundName, sSoundSet, IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_ENABLE_ALTERNATE_LIGHTS_OFF_AUDIO))
						SET_BIT(iLocalBoolCheck5, LBOOL5_TRIGGERED_EMP_AUDIO)
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
	
	ENDSWITCH
	
	IF AM_I_ON_A_HEIST()
		// If we have started playing the emp audio and it has finished, unload because we don't need it anymore.
		IF iEmpSoundId != -1
			IF HAS_SOUND_FINISHED(iEmpSoundId)
				RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_EMP_BOMB")
				iEmpSoundId = -1
				PRINTLN("[RCC MISSION] - [ARTIFICIAL LIGHTS] - [CLIENT] - emp HAS_SOUND_FINISHED = TRUE, iEmpSoundId = ", iEmpSoundId)
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck5, LBOOL5_TRIGGERED_EMP_AUDIO)
		AND iEmpSoundID != -1
			IF HAS_SOUND_FINISHED(iEmpSoundId)
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_ENABLE_ALTERNATE_LIGHTS_OFF_AUDIO)
					RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_CHRISTMAS2017/XM_STEALAVG")
				ELSE
					RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_HALLOWEEN/FVJ_01")
				ENDIF
				iEmpSoundId = -1
				PRINTLN("[RCC MISSION] - [ARTIFICIAL LIGHTS] - [CLIENT] - non-heist emp HAS_SOUND_FINISHED = TRUE, iEmpSoundId = ", iEmpSoundId)
			ENDIF
		ENDIF
	ENDIF
					
ENDPROC



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: BIOLAB LIFT (the elevator used by the Ground Team in The Humane Labs Raid) !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************



INT iWarpStage
VECTOR vLiftPosition
FLOAT fLiftHeading
SCRIPT_TIMER timerBiolabLiftCooldown
INT iBioLiftBitset

CONST_INT BIOLAB_BIT_SHOWN_HELP							0
CONST_INT BIOLAB_BIT_RECEIVED_LOWER_LIFT_WARP_EVENT		1
CONST_INT BIOLAB_BIT_RECEIVED_UPPER_LIFT_WARP_EVENT		2
CONST_INT BIOLAB_BIT_LOADED_LIFT_AUDIO					3
CONST_INT BIOLAB_BIT_LOADED_ACTION_MODE_ASSETS			4

ENUM eMY_BIOLAB_LIFT_STATE
	eMYBIOLABLIFTSTATE_NOT_WARPING = 0,
	eMYBIOLABLIFTSTATE_WARPING
ENDENUM
eMY_BIOLAB_LIFT_STATE eMyBiolabLiftState

#IF IS_DEBUG_BUILD
FUNC STRING GET_MY_BIOLAB_LIFT_STATE_NAME(eMY_BIOLAB_LIFT_STATE eState)
	SWITCH eState
		CASE eMYBIOLABLIFTSTATE_NOT_WARPING		RETURN "NOT_WARPING"
		CASE eMYBIOLABLIFTSTATE_WARPING			RETURN "WARPING"
	ENDSWITCH
	RETURN "NOT_IN_SWITCH"
ENDFUNC
#ENDIF

FUNC eMY_BIOLAB_LIFT_STATE GET_MY_BIOLAB_LIFT_STATE()
	RETURN eMyBiolabLiftState
ENDFUNC

PROC SET_MY_BIOLAB_LIFT_STATE(eMY_BIOLAB_LIFT_STATE eState)
	IF eMyBiolabLiftState != eState
		eMyBiolabLiftState = eState
		#IF IS_DEBUG_BUILD
			STRING strTemp = GET_MY_BIOLAB_LIFT_STATE_NAME(eMyBiolabLiftState)
			PRINTLN("[RCC MISSION] setting my biolab state to ", strTemp)
		#ENDIF
	ENDIF
ENDPROC

PROC SAVE_MY_BIOLAB_LIFT_DESTINATION_DATA(BOOL bInLower)
	IF DOES_ENTITY_EXIST(LocalPlayerPed)
		IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
			NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_CLEAR_TASKS )
			vLiftPosition = GET_ENTITY_COORDS(LocalPlayerPed)
			fLiftHeading = GET_ENTITY_HEADING(LocalPlayerPed)
			IF bInLower
				vLiftPosition.z = 28.0
			ELSE
				vLiftPosition.z = 21.0
			ENDIF
			PRINTLN("[RCC MISSION] my biolab destination data saved as:")
			PRINTLN("[RCC MISSION] vLiftPosition = ", vLiftPosition)
			PRINTLN("[RCC MISSION] fLiftHeading = ", fLiftHeading)
		ENDIF
	ENDIF
ENDPROC

INT soundFxLift = GET_SOUND_ID()

FUNc BOOL DO_BIOLAB_LIFT_WARP()
	
	SWITCH iWarpStage
		CASE 0
			IF NOT IS_SCREEN_FADED_OUT()
				IF NOT IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_OUT(2000)
					PRINTLN("[RCC MISSION]  [LIFT] doing screen fade out.")
				ENDIF
			ELSE
				// Looping lift warp sound
				PLAY_SOUND_FROM_ENTITY(soundFxLift, "Move", LocalPlayerPed, "LIFT_NORMAL_SOUNDSET")
				iWarpStage++
				PRINTLN("[RCC MISSION]  [LIFT] competed screen fade out. Going to warp stage", iWarpStage)
			ENDIF
		BREAK
		CASE 1
			IF DOES_ENTITY_EXIST(LocalPlayerPed)
				IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
					IF NET_WARP_TO_COORD(vLiftPosition, fLiftHeading, FALSE, FALSE)
						iWarpStage++
						PRINTLN("[RCC MISSION] [LIFT]  competed warp to other lift. Going to warp stage", iWarpStage)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF NOT IS_SCREEN_FADED_IN()
				IF NOT IS_SCREEN_FADING_IN()
					DO_SCREEN_FADE_IN(2000)
				ENDIF
			ELSE
				START_NET_TIMER(timerBiolabLiftCooldown)
				IF DOES_ENTITY_EXIST(LocalPlayerPed)
					IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
						NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
					ENDIF
				ENDIF
				iWarpStage++
				PRINTLN("[RCC MISSION]  [LIFT] competed screen fade in. Going to warp stage", iWarpStage)
				// Stop lift sound
				STOP_SOUND(soundFxLift)
				// Play bell sound when arriving
				PLAY_SOUND_FROM_ENTITY(-1, "Bell", LocalPlayerPed, "LIFT_NORMAL_SOUNDSET")
			ENDIF
		BREAK
		CASE 3
			IF HAS_NET_TIMER_EXPIRED(timerBiolabLiftCooldown, 5000)
				RESET_NET_TIMER(timerBiolabLiftCooldown)
				iWarpStage = 0
				vLiftPosition = << 0.0, 0.0, 0.0 >>
				fLiftHeading = 0.0
				iBioLiftBitset = 0
				PRINTLN("[RCC MISSION] [LIFT]  biolab lift cooldown timer expired, resetting all data.")
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

PROC UNLOCK_ELEVATOR_DOORS()
	DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_BIOLAB_LOWER_LIFT_L, DOORSTATE_UNLOCKED,FALSE)
	DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_BIOLAB_LOWER_LIFT_R, DOORSTATE_UNLOCKED,FALSE)
ENDPROC

PROC LOCK_ELEVATOR_DOORS()
	DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_BIOLAB_LOWER_LIFT_L, DOORSTATE_LOCKED,FALSE)
	DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_BIOLAB_LOWER_LIFT_R, DOORSTATE_LOCKED,FALSE)
ENDPROC

PROC FORCE_OPEN_ELEVATOR_DOORS()
	DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_BIOLAB_LOWER_LIFT_L, DOORSTATE_FORCE_OPEN_THIS_FRAME,FALSE)
	DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_BIOLAB_LOWER_LIFT_R, DOORSTATE_FORCE_OPEN_THIS_FRAME,FALSE)
	CPRINTLN(DEBUG_PAUSE_MENU, "DO_BIOLAB_LIFT_CUTSCENE FORCE_OPEN_ELEVATOR_DOORS")
ENDPROC

PED_INDEX pedTempClone
INT iStoredButtonPresser = -1

PROC CREATE_BIOLAB_LIFT_CLONE(INT iParticipant)
	PED_INDEX pedTemp
	IF iParticipant > -1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			pedTemp = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant)))
		ENDIF
	
		IF iParticipant != iStoredButtonPresser
			IF IS_PED_INJURED(pedTempClone)
				pedTempClone = CLONE_PED(pedTemp, FALSE, FALSE, FALSE)
				FREEZE_ENTITY_POSITION(pedTempClone, TRUE)
				SET_ENTITY_COLLISION(pedTempClone, FALSE)
			ENDIF
			SET_ENTITY_HEADING(pedTempClone, 168.3219)
			SET_ENTITY_COORDS(pedTempClone, <<3541.2388, 3674.7527, 19.9918>>)
			IF IS_PED_ARMED(pedTemp, WF_INCLUDE_PROJECTILE|WF_INCLUDE_GUN|WF_INCLUDE_MELEE)
				GIVE_WEAPON_TO_PED(pedTempClone, GET_PEDS_CURRENT_WEAPON(pedTemp), 90, TRUE)
			ENDIF
			CPRINTLN(DEBUG_PAUSE_MENU, "CREATE_BIOLAB_LIFT_CLONE pedTemp hits the switch.")
		ELSE
			IF IS_PED_INJURED(pedTempClone)
				pedTempClone = CLONE_PED(pedTemp, FALSE, FALSE, FALSE)
				FREEZE_ENTITY_POSITION(pedTempClone, TRUE)
				SET_ENTITY_COLLISION(pedTempClone, FALSE)
			ENDIF
			SET_ENTITY_HEADING(pedTempClone, 168.3219)
			SET_ENTITY_COORDS(pedTempClone, <<3540.2175, 3675.3281, 19.9918>>)
			IF IS_PED_ARMED(pedTemp, WF_INCLUDE_PROJECTILE|WF_INCLUDE_GUN|WF_INCLUDE_MELEE)
				GIVE_WEAPON_TO_PED(pedTempClone, GET_PEDS_CURRENT_WEAPON(pedTemp), 90, TRUE)
			ENDIF
			CPRINTLN(DEBUG_PAUSE_MENU, "CREATE_BIOLAB_LIFT_CLONE LocalPlayerPed hits the switch.")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DO_BIOLAB_LIFT_CUTSCENE()
	VECTOR vCamPos = <<3541.6360, 3673.5957, 22.3640>>
	VECTOR vCamRot = <<-44.4380, -0.1210, 35.6351>>
	FLOAT fCamFOV = 62.2881
	INT iCutTime = 3000
	PED_INDEX pedTemp
	
	INT iPartLoop, iPart = -1
	PLAYER_INDEX tempPlayer
	
	FOR iPartLoop = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF iPartLoop != iPartToUse
		AND MC_playerBD[iPartLoop].iteam = MC_playerBD[iPartToUse].iteam
			tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartLoop))
				
			IF IS_NET_PLAYER_OK(tempPlayer)
				iPart = iPartLoop
				iPartLoop = NUM_NETWORK_PLAYERS //Break out!
			ENDIF

		ENDIF
	ENDFOR
	
	IF iPart > -1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
			pedTemp = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart)))
			IF NOT IS_PED_INJURED(pedTemp)
				IF iScriptedCutsceneProgress < 4
					SET_ENTITY_LOCALLY_INVISIBLE(pedTemp)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF iScriptedCutsceneProgress = -2
		iStoredButtonPresser = iPartToUse
		
		iScriptedCutsceneProgress = -1
	ELIF iScriptedCutsceneProgress = -1 // Cutscene Setup

		CPRINTLN(DEBUG_PAUSE_MENU, "DO_BIOLAB_LIFT_CUTSCENE iStoredButtonPresser = ", iStoredButtonPresser)
		CPRINTLN(DEBUG_PAUSE_MENU, "DO_BIOLAB_LIFT_CUTSCENE Other guy iPart = ", iPart)
		
		IF iPart > -1
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
				pedTemp = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart)))
		
				IF NOT IS_PED_INJURED(pedTemp)
					IF iPartToUse != iStoredButtonPresser
						CREATE_BIOLAB_LIFT_CLONE(iPartToUse)
						SET_ENTITY_VISIBLE(pedTempClone, TRUE)
						SET_PED_USING_ACTION_MODE(pedTempClone, TRUE, -1, "")
						IF !IS_PED_FEMALE(pedTempClone)
							SET_PED_USING_ACTION_MODE(pedTempClone, TRUE, -1, "DEFAULT_ACTION")
						ELSE
							SET_PED_USING_ACTION_MODE(pedTempClone, TRUE, -1, "MP_FEMALE_ACTION")
						ENDIF
						FORCE_PED_MOTION_STATE(pedTempClone, MS_ACTIONMODE_IDLE) 
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedTempClone)
						
						SET_ENTITY_HEADING(LocalPlayerPed, 168.3219)
						SET_ENTITY_COORDS(LocalPlayerPed, <<3540.2175, 3675.3281, 19.9918>>)
						SET_PED_USING_ACTION_MODE(LocalPlayerPed, TRUE)
						FORCE_PED_MOTION_STATE(LocalPlayerPed, MS_ACTIONMODE_IDLE) 
						FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed)
						
						CPRINTLN(DEBUG_PAUSE_MENU, "DO_BIOLAB_LIFT_CUTSCENE pedTemp hits the switch.")
					ELSE
						SET_ENTITY_HEADING(LocalPlayerPed, 168.3219)
						SET_ENTITY_COORDS(LocalPlayerPed, <<3541.2388, 3674.7527, 19.9918>>)
						SET_PED_USING_ACTION_MODE(LocalPlayerPed, TRUE)
						FORCE_PED_MOTION_STATE(LocalPlayerPed, MS_ACTIONMODE_IDLE)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed)
						
						CREATE_BIOLAB_LIFT_CLONE(iPartToUse)
						SET_ENTITY_VISIBLE(pedTempClone, TRUE)
						IF !IS_PED_FEMALE(pedTempClone)
							SET_PED_USING_ACTION_MODE(pedTempClone, TRUE, -1, "DEFAULT_ACTION")
						ELSE
							SET_PED_USING_ACTION_MODE(pedTempClone, TRUE, -1, "MP_FEMALE_ACTION")
						ENDIF
						FORCE_PED_MOTION_STATE(pedTempClone, MS_ACTIONMODE_IDLE) 
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedTempClone)
						CPRINTLN(DEBUG_PAUSE_MENU, "DO_BIOLAB_LIFT_CUTSCENE LocalPlayerPed hits the switch.")
					ENDIF
				ENDIF
			ELSE
				SET_ENTITY_HEADING(LocalPlayerPed, 168.3219)
				SET_ENTITY_COORDS(LocalPlayerPed, <<3541.2388, 3674.7527, 19.9918>>)
				TASK_LOOK_AT_COORD(LocalPlayerPed, <<3540.4111, 3673.7197, 22.3434>>, iCutTime)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed)
				
				CPRINTLN(DEBUG_PAUSE_MENU, "DO_BIOLAB_LIFT_CUTSCENE LocalPlayerPed is on their own. 1")
			ENDIF
		ELSE
			SET_ENTITY_HEADING(LocalPlayerPed, 168.3219)
			SET_ENTITY_COORDS(LocalPlayerPed, <<3541.2388, 3674.7527, 19.9918>>)
			TASK_LOOK_AT_COORD(LocalPlayerPed, <<3540.4111, 3673.7197, 22.3434>>, iCutTime)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed)
			
			CPRINTLN(DEBUG_PAUSE_MENU, "DO_BIOLAB_LIFT_CUTSCENE LocalPlayerPed is on their own. 2")
		ENDIF
		
		cutscenecam = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", vCamPos, vCamRot, fCamFOV, TRUE)
		
		SHAKE_CAM(cutscenecam, "hand_shake", 0.2)
		
		REQUEST_EARLY_LIGHT_CHECK()
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
		
		ANIMPOSTFX_PLAY("DeathFailMPDark", 0, FALSE)
		
		SET_PLAYER_CONTROL(LocalPlayer, FALSE)
		
		DISABLE_ALL_MP_HUD_THIS_FRAME()
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		LOCK_ELEVATOR_DOORS()
		
		START_NET_TIMER(timerBiolabLiftCooldown)
		SETTIMERA(0)
		iScriptedCutsceneProgress++
		
	ELIF iScriptedCutsceneProgress = 0 // Cutscene Running	
		
		IF HAS_NET_TIMER_EXPIRED(timerBiolabLiftCooldown, iCutTime)
		 
			//STOP_SOUND(lift_sound)
			CPRINTLN(DEBUG_PAUSE_MENU, "DO_BIOLAB_LIFT_CUTSCENE Time up")
			DESTROY_ALL_CAMS()
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			CPRINTLN(DEBUG_PAUSE_MENU, "DO_BIOLAB_LIFT_CUTSCENE SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)")
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			UNLOCK_ELEVATOR_DOORS()
			
			IF DOES_ENTITY_EXIST(pedTempClone)
				DELETE_PED(pedTempClone)
			ENDIF
			
			IF NOT IS_PED_INJURED(pedTemp)
				SET_ENTITY_LOCALLY_VISIBLE(pedTemp)
			ENDIF
			IF ANIMPOSTFX_IS_RUNNING("DeathFailMPDark")
				ANIMPOSTFX_STOP("DeathFailMPDark")
			ENDIF
			SET_PLAYER_CONTROL(LocalPlayer, TRUE)
			//stop_sound(lift_sound)
			iScriptedCutsceneProgress = 4
		ELSE
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
		ENDIF 
	ELIF iScriptedCutsceneProgress = 1	
	ELIF iScriptedCutsceneProgress = 2
	ELIF iScriptedCutsceneProgress = 3
	ELIF iScriptedCutsceneProgress = 4
	
		//FORCE_OPEN_ELEVATOR_DOORS()
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_IN_BIOLAB_LIFT(BOOL bCheckLower)
	IF DOES_ENTITY_EXIST(LocalPlayerPed)
		IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
			VECTOR v1, v2
			FLOAT fWidth
			IF bCheckLower
				v1		= <<3540.308838,3673.561523,19.991785>>
				v2		= <<3540.981445,3677.466553,22.491785>>
				fWidth	= 3.250000
			ELSE
				v1 		= <<3540.303223,3673.522217,27.121149>>
				v2 		= <<3540.991455,3677.459717,29.621149>> 
				fWidth 	= 3.250000
			ENDIF
			RETURN IS_ENTITY_IN_ANGLED_AREA(LocalPlayerPed, v1, v2, fWidth)
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC DISPLAY_USE_BIOLAB_LIFT_HELP()
	IF NOT IS_BIT_SET(iBioLiftBitset, BIOLAB_BIT_SHOWN_HELP)
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			PRINT_HELP_FOREVER("BIOLAB_LIFT")
			SET_BIT(iBioLiftBitset, BIOLAB_BIT_SHOWN_HELP)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_NEARBY_LIFT(BOOL bCheckLower)
	IF DOES_ENTITY_EXIST(LocalPlayerPed)
		IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
			IF bCheckLower
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(LocalPlayerPed), <<3540.308838,3673.561523,19.991785>>) <= 10.0
					RETURN TRUE
				ENDIF
			ELSE
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(LocalPlayerPed), <<3540.303223,3673.522217,27.121149>>) <= 10.0
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_BIOLAB_LIFT()
	
	BOOL bInLowerLift, bInUpperLift, bNearbyLowerLift, bNearbyUpperLift, bLoadedMovementAsset
	INT iPartLoop, iPart = -1
	PLAYER_INDEX tempPlayer
	PED_INDEX pedTemp
	SWITCH GET_MY_BIOLAB_LIFT_STATE()
		
		CASE eMYBIOLABLIFTSTATE_NOT_WARPING
			
			// Work out if nearby or in either lift.
			//bInLowerLift = IS_LOCAL_PLAYER_IN_BIOLAB_LIFT(TRUE)
			bInUpperLift = IS_LOCAL_PLAYER_IN_BIOLAB_LIFT(FALSE)
			
			// Make sure both players are going together.
			IF bInUpperLift
				
				
				FOR iPartLoop = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
					IF iPartLoop != iPartToUse
					AND MC_playerBD[iPartLoop].iteam = MC_playerBD[iPartToUse].iteam
						tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartLoop))
							
						IF IS_NET_PLAYER_OK(tempPlayer)
							iPart = iPartLoop
							iPartLoop = NUM_NETWORK_PLAYERS //Break out!
						ENDIF
	
					ENDIF
				ENDFOR	
				IF iPart > -1
					IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
						pedTemp = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart)))

						IF !IS_ENTITY_IN_ANGLED_AREA(pedTemp, <<3540.303223,3673.522217,27.121149>>, <<3540.991455,3677.459717,29.621149>> , 3.25)
	//					IF !GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedTemp), <<3540.303223,3673.522217,27.121149>>) <= 10.0
							bInUpperLift = FALSE
						ENDIF
					ENDIF	
				ENDIF
			ENDIF
			
			IF NOT bInLowerLift
			AND NOT bInUpperLift
				bNearbyLowerLift = IS_PLAYER_NEARBY_LIFT(TRUE)
				bNearbyUpperLift = IS_PLAYER_NEARBY_LIFT(FALSE)
			ENDIF
			
			// Manage loading and unloading audio.
			IF (bInLowerLift
			OR bInUpperLift
			OR bNearbyLowerLift
			OR bNearbyUpperLift)
				IF NOT IS_BIT_SET(iBioLiftBitset, BIOLAB_BIT_LOADED_LIFT_AUDIO)
					IF REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\LIFTS")
						PRINTLN("[RCC MISSION] [LIFT] MAINTAIN_BIOLAB_LIFT, loaded audio ")
						SET_BIT(iBioLiftBitset, BIOLAB_BIT_LOADED_LIFT_AUDIO)
					ENDIF
				ENDIF
				CREATE_BIOLAB_LIFT_CLONE(iPart)
				IF NOT IS_PED_INJURED(pedTempClone)
					IF IS_ENTITY_VISIBLE(pedTempClone)
						SET_ENTITY_VISIBLE(pedTempClone, FALSE)
					ENDIF
				ENDIF
				
				IF NOT IS_BIT_SET(iBioLiftBitset, BIOLAB_BIT_LOADED_ACTION_MODE_ASSETS)
					IF NOT IS_PED_INJURED(pedTempClone)
						IF !IS_PED_FEMALE(pedTempClone)
							REQUEST_ACTION_MODE_ASSET("DEFAULT_ACTION")
							IF HAS_ACTION_MODE_ASSET_LOADED("DEFAULT_ACTION")
								bLoadedMovementAsset = TRUE
							ENDIF
						ELSE
							REQUEST_ACTION_MODE_ASSET("MP_FEMALE_ACTION")
							IF HAS_ACTION_MODE_ASSET_LOADED("MP_FEMALE_ACTION")
								bLoadedMovementAsset = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					
					IF bLoadedMovementAsset	
						PRINTLN("[RCC MISSION] [LIFT] MAINTAIN_BIOLAB_LIFT, loaded action mode assets ")
						SET_BIT(iBioLiftBitset, BIOLAB_BIT_LOADED_ACTION_MODE_ASSETS)	
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(iBioLiftBitset, BIOLAB_BIT_LOADED_LIFT_AUDIO)
					RELEASE_NAMED_SCRIPT_AUDIO_BANK("SCRIPT\\LIFTS")
					PRINTLN("[RCC MISSION] [LIFT] MAINTAIN_BIOLAB_LIFT, unloaded audio ")
					CLEAR_BIT(iBioLiftBitset, BIOLAB_BIT_LOADED_LIFT_AUDIO)
				ENDIF
				IF IS_BIT_SET(iBioLiftBitset, BIOLAB_BIT_LOADED_ACTION_MODE_ASSETS)
					IF NOT IS_PED_INJURED(pedTempClone)
						IF !IS_PED_FEMALE(pedTempClone)
							REMOVE_ACTION_MODE_ASSET("DEFAULT_ACTION")
						ELSE
							REMOVE_ACTION_MODE_ASSET("MP_FEMALE_ACTION")
						ENDIF
						PRINTLN("[RCC MISSION] [LIFT] MAINTAIN_BIOLAB_LIFT, unloaded action mode assets ")
						CLEAR_BIT(iBioLiftBitset, BIOLAB_BIT_LOADED_ACTION_MODE_ASSETS)
					ENDIF
				ENDIF
			ENDIF
			
			// Handle using lift.
			IF IS_BIT_SET(iBioLiftBitset, BIOLAB_BIT_LOADED_LIFT_AUDIO)
			
				IF bInLowerLift
				
					DISPLAY_USE_BIOLAB_LIFT_HELP()
					
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
						iScriptedCutsceneProgress = -1
						BROADCAST_BIOLAB_LIFT_WARP_EVENT(TRUE)
						PRINTLN("[RCC MISSION] [LIFT] player has pressed context button to activate lift. Lower to upper.")
						SAVE_MY_BIOLAB_LIFT_DESTINATION_DATA(TRUE)
						SET_MY_BIOLAB_LIFT_STATE(eMYBIOLABLIFTSTATE_WARPING)
						// Play sound when lift button pushed
						PLAY_SOUND_FROM_ENTITY(-1, "Tone", LocalPlayerPed, "LIFT_NORMAL_SOUNDSET")
					ENDIF
					
					IF IS_BIT_SET(iBioLiftBitset, BIOLAB_BIT_RECEIVED_LOWER_LIFT_WARP_EVENT)
						SAVE_MY_BIOLAB_LIFT_DESTINATION_DATA(TRUE)
						SET_MY_BIOLAB_LIFT_STATE(eMYBIOLABLIFTSTATE_WARPING)
					ENDIF
					
				ELSE
					
					CLEAR_BIT(iBioLiftBitset, BIOLAB_BIT_RECEIVED_LOWER_LIFT_WARP_EVENT)
					
					IF NOT bInUpperLift
						CLEAR_BIT(iBioLiftBitset, BIOLAB_BIT_SHOWN_HELP)
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BIOLAB_LIFT")
							CLEAR_HELP()
						ENDIF
					ENDIF
					
				ENDIF
				
				IF bInUpperLift
				OR IS_BIT_SET(iBioLiftBitset, BIOLAB_BIT_RECEIVED_UPPER_LIFT_WARP_EVENT)
					DISPLAY_USE_BIOLAB_LIFT_HELP()
					
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
						CPRINTLN(DEBUG_PAUSE_MENU, "MAINTAIN_BIOLAB_LIFT Local Player Pressed the Button ")
						iScriptedCutsceneProgress = -2
						BROADCAST_BIOLAB_LIFT_WARP_EVENT(FALSE)
						PRINTLN("[RCC MISSION][LIFT] player has pressed context button to activate lift. Upper to lower.")
						SAVE_MY_BIOLAB_LIFT_DESTINATION_DATA(FALSE)
						SET_MY_BIOLAB_LIFT_STATE(eMYBIOLABLIFTSTATE_WARPING)
						REQUEST_EARLY_LIGHT_CHECK()
						// Play sound when lift button pushed
						PLAY_SOUND_FROM_ENTITY(-1, "Tone", LocalPlayerPed, "LIFT_NORMAL_SOUNDSET")
					ENDIF
					
					IF IS_BIT_SET(iBioLiftBitset, BIOLAB_BIT_RECEIVED_UPPER_LIFT_WARP_EVENT)
						iScriptedCutsceneProgress = -1
						SAVE_MY_BIOLAB_LIFT_DESTINATION_DATA(FALSE)
						SET_MY_BIOLAB_LIFT_STATE(eMYBIOLABLIFTSTATE_WARPING)
					ENDIF
					
				ELSE
					
					CLEAR_BIT(iBioLiftBitset, BIOLAB_BIT_RECEIVED_UPPER_LIFT_WARP_EVENT)
					
					IF NOT bInLowerLift
						CLEAR_BIT(iBioLiftBitset, BIOLAB_BIT_SHOWN_HELP)
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BIOLAB_LIFT")
							CLEAR_HELP()
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF			
		BREAK
		
		CASE eMYBIOLABLIFTSTATE_WARPING
		
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BIOLAB_LIFT")
				CLEAR_HELP()
			ENDIF
			
			//IF DO_BIOLAB_LIFT_WARP()
			IF DO_BIOLAB_LIFT_CUTSCENE()
				PRINTLN("[RCC MISSION] [LIFT] biolab lift warp complete.")
				CLEAR_BIT(iBioLiftBitset, BIOLAB_BIT_RECEIVED_UPPER_LIFT_WARP_EVENT)
				//SET_MY_BIOLAB_LIFT_STATE(eMYBIOLABLIFTSTATE_NOT_WARPING)
			ENDIF
			
		BREAK
		
	ENDSWITCH	
	
ENDPROC



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: APARTMENT WARPING (used in the canned Booty Call mission) !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************



/// Server-only function, creates the apartment door
FUNC BOOL CREATE_APARTMENTWARP_DOOR()
	
	BOOL bDoorMade = FALSE
	
	IF IS_VECTOR_ZERO(g_FMMC_STRUCT.vBuildingWarp)
		bDoorMade = TRUE
	ELSE
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD.niAptDoor)
			VECTOR vDoorLoc, vDoorRot
			vDoorLoc = <<345.8695,-1003.0790,-99.1045>>
			vDoorRot = <<0,0,180>>
			
			MODEL_NAMES mnDoor
			mnDoor = V_ILEV_MP_MID_FRONTDOOR
			
			REQUEST_MODEL(mnDoor)
			IF NOT IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_APARTMENT_DOOR)
				MC_RESERVE_NETWORK_MISSION_OBJECTS(1)
				SET_BIT(iLocalBoolCheck12, LBOOL12_OBJ_RES_APARTMENT_DOOR)
			ENDIF
			IF IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_APARTMENT_DOOR)
				IF HAS_MODEL_LOADED(mnDoor)
				AND CAN_REGISTER_MISSION_OBJECTS(1)
					IF CREATE_NET_OBJ(MC_serverBD.niAptDoor,mnDoor,vDoorLoc - <<0,0,1.09>>,bIsLocalPlayerHost,DEFAULT,TRUE)
						SET_ENTITY_ROTATION(NET_TO_ENT(MC_serverBD.niAptDoor),vDoorRot)
						bDoorMade = TRUE
						//I disable collisions to stop players being pushed by this door into the hallway (through the other invisible objDoorBlocker door)
						SET_ENTITY_COLLISION(NET_TO_ENT(MC_serverBD.niAptDoor),FALSE)
						CLEAR_BIT(iLocalBoolCheck12, LBOOL12_OBJ_RES_APARTMENT_DOOR)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			bDoorMade = TRUE
		ENDIF
	ENDIF
	
	RETURN bDoorMade
	
ENDFUNC

//This only checks the bank manager's apartment for now!
FUNC BOOL IS_LOCATION_IN_FMMC_APARTMENT(VECTOR vLoc)
	
	BOOL bInApartment
	
	//The apartment needs to have a warp point for it to exist
	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vBuildingWarp)
		IF ( (vLoc.x > 335) AND (vLoc.x < 355) )
		AND ( (vLoc.y > -1016) AND (vLoc.y < -992) )
		AND ( (vLoc.z > -102) AND (vLoc.z < -95) )
			bInApartment = TRUE
		ENDIF
	ENDIF
	
	RETURN bInApartment
	
ENDFUNC

//Runs the apartment control actions limiter - stops equipping weapons / jumping / etc
PROC RUN_APARTMENT_PLAYER_CONTROL_ACTIONS_LIMITER()
	
	SET_MINIMAP_BLOCK_WAYPOINT(TRUE)
	IF IS_WAYPOINT_ACTIVE()
		SET_WAYPOINT_OFF()
	ENDIF
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_UNARMED)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_MELEE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_HANDGUN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SHOTGUN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SMG)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_AUTO_RIFLE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SNIPER)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_HEAVY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SPECIAL)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
	
	IF bLocalPlayerOK
		
		WEAPON_TYPE currentWeapon
		GET_CURRENT_PED_WEAPON(LocalPlayerPed,currentWeapon)
		
		IF currentWeapon = WEAPONTYPE_UNARMED
		OR currentWeapon = WEAPONTYPE_OBJECT
			//do nothing
		ELSE
			SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED, TRUE)
			PRINTLN("[RCC MISSION] RUN_APARTMENT_PLAYER_CONTROL_ACTIONS_LIMITER: Setting current weapontype to unarmed")
		ENDIF
		
		SET_PED_RESET_FLAG(LocalPlayerPed,PRF_DisablePlayerAutoVaulting,TRUE)
		//SET_PED_RESET_FLAG(LocalPlayerPed, PRF_CannotBeTargetedByAI, TRUE)
		SET_PED_RESET_FLAG(LocalPlayerPed, PRF_UseInteriorCapsuleSettings, TRUE) //Stops player getting onto couches, chairs, etc
		
	ENDIF
	
ENDPROC

PROC PROCESS_APARTMENT_WARPING()
	
	IF IS_PLAYER_SPECTATING(LocalPlayer)
		EXIT
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vBuildingWarp)
	AND g_FMMC_STRUCT.iBuildingWarpFirstRule[MC_playerBD[iPartToUse].iteam] != -1
		INT iteam = MC_playerBD[iPartToUse].iteam
		
		//Bool to check if we're at the right point to be running the warp stuff
		BOOL bRightPoint
		
		IF IS_BIT_SET(iLocalBoolCheck4,LBOOL4_IN_APARTMENT)
			bRightPoint = TRUE
		ELSE
			IF g_FMMC_STRUCT.iBuildingWarpFirstRule[MC_playerBD[iPartToUse].iteam] != -1
				IF g_FMMC_STRUCT.iBuildingWarpLastRule[MC_playerBD[iPartToUse].iteam] != -1
					IF (MC_serverBD_4.iCurrentHighestPriority[iteam] >= g_FMMC_STRUCT.iBuildingWarpFirstRule[iteam]
					AND MC_serverBD_4.iCurrentHighestPriority[iteam] <= g_FMMC_STRUCT.iBuildingWarpLastRule[iteam])
						bRightPoint = TRUE
					ENDIF
				ELSE
					IF (MC_serverBD_4.iCurrentHighestPriority[iteam] >= g_FMMC_STRUCT.iBuildingWarpFirstRule[iteam])
						bRightPoint = TRUE
					ENDIF
				ENDIF
			ELSE
				bRightPoint = TRUE
			ENDIF
		ENDIF
		
		IF bRightPoint
			
			VECTOR vBuildingWarpLoc = g_FMMC_STRUCT.vBuildingWarp
			VECTOR vWarpTo = <<0,0,0>>
			
			POPULATE_WARP_LOCATIONS(vBuildingLocations, vWarpTo, g_FMMC_STRUCT.iBuildingToWarpTo)
			
			/*NEEDS UPDATING FOR NEW INTERIORS*/
			//Front door:
			VECTOR vDoorLoc, vDoorRot
			vDoorLoc = <<345.8695,-1003.0790,-99.1045>>
			vDoorRot = <<0,0,180>>
			
			FLOAT fWarpRadius = 2.6 //Warp size
			FLOAT fWarpLeftRadius = 3.1 //Distance from warp to activate it again (it gets deactivated after coming through from inside the apartment)
			FLOAT fLoadRadius = 20 //Distance from warp to load the apartment interior
			FLOAT fUnloadRadius = 30 //Distance from warp to unload the apartment interior
			FLOAT fExitRadius = 1.7 //Distance from door for Exit Apartment menu to appear
			
			//Camera pan bits:
			VECTOR vStartPos,vStartRot
			VECTOR vEndPos, vEndRot	
			
			//Interp time
			INT iDuration = 5000
			INT iTimeCam = 20
			
			
			INT iR, iG, iB, iA 
	
			BOOL bIsReady = FALSE

			SWITCH MC_PlayerBD[iPartToUse].iApartmentState
			
				CASE ciAS_Outside
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.iBuildingWarpBS,ciAPW_HideMarker)
						GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
						iA = 200
						DRAW_MARKER(MARKER_CYLINDER,vBuildingWarpLoc,<<0.0,0.0,0.0>>,<<0.0,0.0,0.0>>,<<fWarpRadius,fWarpRadius,0.2>>,iR, iG, iB, iA)
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iBuildingWarpBS,ciAPW_DrawBlip)
						IF NOT DOES_BLIP_EXIST(AptWarpBlip)
							AptWarpBlip = CREATE_BLIP_FOR_COORD(vBuildingWarpLoc, IS_BIT_SET(g_FMMC_STRUCT.iBuildingWarpBS,ciAPW_DrawGPSRouteToWarp))
							SET_BLIP_COLOUR_FROM_HUD_COLOUR(AptWarpBlip,HUD_COLOUR_YELLOW)
						ENDIF
					ENDIF
					
					IF NOT IS_BIT_SET(iLocalBoolCheck4,LBOOL4_APARTMENT_LOADED)
						
						IF VDIST2(GET_PLAYER_COORDS(LocalPlayer),vBuildingWarpLoc) <= (fLoadRadius * fLoadRadius)
							PRINTLN("[RCC MISSION] PROCESS_APARTMENT_WARPING - OUTSIDE - Local player has entered Apartment load in radius - loading in apartment...")

							IF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_WARP_APARTMENT
								SET_INTERIOR_DISABLED(INTERIOR_V_APART_MIDSPAZ, FALSE)
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_WARP_APARTMENT], vBuildingLocations, "v_apart_midspaz")
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_WARP_APARTMENT])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_WARP_CORONER
								SET_BUILDING_STATE(BUILDINGNAME_IPL_CORONER_INTERIOR, BUILDINGSTATE_DESTROYED) 
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_WARP_CORONER], vBuildingLocations, "v_coroner")
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_WARP_CORONER])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_WARP_RECYCLE
								CLEANUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_WARP_RECYCLE])
								
								SET_INTERIOR_DISABLED(INTERIOR_V_RECYCLE, FALSE)
								SET_INTERIOR_CAPPED(INTERIOR_V_RECYCLE, FALSE)
								
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_WARP_RECYCLE], vBuildingLocations, "v_recycle")
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_WARP_RECYCLE])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_WARP_ABATTOIR
								CLEANUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_WARP_ABATTOIR])								
								SET_INTERIOR_DISABLED(INTERIOR_V_ABATTOIR, FALSE) 
								SET_INTERIOR_CAPPED(INTERIOR_V_ABATTOIR, FALSE)
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_WARP_ABATTOIR], vBuildingLocations, "v_abattoir")
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_WARP_ABATTOIR])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_WARP_FARMHOUSE
								CLEANUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_WARP_FARMHOUSE])								
								SET_INTERIOR_DISABLED(INTERIOR_V_FARMHOUSE, FALSE) 
								SET_INTERIOR_CAPPED(INTERIOR_V_FARMHOUSE, FALSE)
								SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE, BUILDINGSTATE_NORMAL)
								SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_CLNUP1, BUILDINGSTATE_NORMAL)
								SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_CLNUP2, BUILDINGSTATE_NORMAL)
								SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_CLNUP3, BUILDINGSTATE_NORMAL)
								SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_INTERIOR, BUILDINGSTATE_DESTROYED)
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_WARP_FARMHOUSE], vBuildingLocations, "v_farmhouse")
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_WARP_FARMHOUSE])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_BUNKER
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_BUNKER], vBuildingLocations, "gr_grdlc_int_02")
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_BUNKER], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_BUNKER], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_BUNKER])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_SUB
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_SUB], vBuildingLocations, "xm_x17dlc_int_sub")
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_SUB], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_SUB], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_SUB])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_IAA
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_IAA], vBuildingLocations, "xm_x17dlc_int_facility")
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_IAA], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_IAA], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_IAA])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_LIFEINVADER
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_LIFEINVADER], vBuildingLocations, "v_faceoffice")
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_LIFEINVADER], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_LIFEINVADER], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_LIFEINVADER])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_FOUNDRY
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_FOUNDRY], vBuildingLocations, "v_foundry")
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_FOUNDRY], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_FOUNDRY], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_FOUNDRY])							
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_SILO_01
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_SILO_01], vBuildingLocations, "xm_x17dlc_int_silo_01")
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_SILO_01], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_SILO_01], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_SILO_01])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_SILO_02
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_SILO_02], vBuildingLocations, "xm_x17dlc_int_silo_02")
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_SILO_02], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_SILO_02], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_SILO_02])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_SILO_03
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_SILO_03], vBuildingLocations, "xm_x17dlc_int_lab")
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_SILO_03], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_SILO_03], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_SILO_03])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_SERVER_FARM
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_SERVER_FARM], vBuildingLocations, "xm_x17dlc_int_facility2")
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_SERVER_FARM], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_SERVER_FARM], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_SERVER_FARM])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_SILO_TUNNEL
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_SILO_TUNNEL], vBuildingLocations, "xm_x17dlc_int_bse_tun")
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_SILO_TUNNEL], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_SILO_TUNNEL], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_SILO_TUNNEL])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_SILO_LOOP
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_SILO_LOOP], vBuildingLocations, "xm_x17dlc_int_base_loop")
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_SILO_LOOP], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_SILO_LOOP], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_SILO_LOOP])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_SILO_ENTRANCE
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_SILO_ENTRANCE], vBuildingLocations, "xm_x17dlc_int_base_ent")
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_SILO_ENTRANCE], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_SILO_ENTRANCE], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_SILO_ENTRANCE])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_SILO_BASE
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_SILO_BASE], vBuildingLocations, "xm_x17dlc_int_base")
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_SILO_BASE], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_SILO_BASE], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_SILO_BASE])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_OSPREY
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_OSPREY], vBuildingLocations, "xm_x17dlc_int_01")
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_OSPREY], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_OSPREY], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_OSPREY])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_HANGAR
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_HANGAR], vBuildingLocations, "sm_smugdlc_int_01")
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_HANGAR], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_HANGAR], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_HANGAR])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_IMPORT_WAREHOUSE
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_IMPORT_WAREHOUSE], vBuildingLocations, "imp_impexp_intwaremed")
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_IMPORT_WAREHOUSE], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_IMPORT_WAREHOUSE], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_IMPORT_WAREHOUSE])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_WAREHOUSE_UNDRGRND_FACILITY
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_WAREHOUSE_UNDRGRND_FACILITY], vBuildingLocations, "imp_impexp_int_02")
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_WAREHOUSE_UNDRGRND_FACILITY], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_WAREHOUSE_UNDRGRND_FACILITY], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_WAREHOUSE_UNDRGRND_FACILITY])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_SPECIAL_CARGO_WAREHOUSE_S
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_SPECIAL_CARGO_WAREHOUSE_S], vBuildingLocations, "ex_int_warehouse_s_dlc")
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_SPECIAL_CARGO_WAREHOUSE_S], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_SPECIAL_CARGO_WAREHOUSE_S], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_SPECIAL_CARGO_WAREHOUSE_S])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_SPECIAL_CARGO_WAREHOUSE_M
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_SPECIAL_CARGO_WAREHOUSE_M], vBuildingLocations, "ex_int_warehouse_m_dlc")
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_SPECIAL_CARGO_WAREHOUSE_M], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_SPECIAL_CARGO_WAREHOUSE_M], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_SPECIAL_CARGO_WAREHOUSE_M])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_SPECIAL_CARGO_WAREHOUSE_L
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_SPECIAL_CARGO_WAREHOUSE_L], vBuildingLocations, "ex_int_warehouse_l_dlc")
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_SPECIAL_CARGO_WAREHOUSE_L], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_SPECIAL_CARGO_WAREHOUSE_L], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_SPECIAL_CARGO_WAREHOUSE_L])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_SPECIAL_CARGO_WAREHOUSE_L
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_STILT_APARTMENT], vBuildingLocations, "apa_v_mp_stilts_a")
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_STILT_APARTMENT], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_STILT_APARTMENT], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_STILT_APARTMENT])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_HIGH_END_APARTMENT
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_HIGH_END_APARTMENT], vBuildingLocations, "hei_dlc_apart_high_new")
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_HIGH_END_APARTMENT], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_HIGH_END_APARTMENT], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_HIGH_END_APARTMENT])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_MEDIUM_END_APARTMENT
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_MEDIUM_END_APARTMENT], vBuildingLocations, "v_apart_midspaz")
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_MEDIUM_END_APARTMENT], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_MEDIUM_END_APARTMENT], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_MEDIUM_END_APARTMENT])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_LOW_END_APARTMENT
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_LOW_END_APARTMENT], vBuildingLocations, "v_studio_lo")
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_LOW_END_APARTMENT], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_LOW_END_APARTMENT], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_LOW_END_APARTMENT])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_FIB_OFFICE
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_FIB_OFFICE], vBuildingLocations, "v_fib01")
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_FIB_OFFICE], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_FIB_OFFICE], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_FIB_OFFICE])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_FIB_OFFICE_2
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_FIB_OFFICE_2], vBuildingLocations, "v_fib03")
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_FIB_OFFICE_2], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_FIB_OFFICE_2], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_FIB_OFFICE_2])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_FIB_LOBBY
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_FIB_LOBBY], vBuildingLocations, "v_office_lobby")
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_FIB_LOBBY], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_FIB_LOBBY], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_FIB_LOBBY])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_HIGH_END_GARAGE
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_HIGH_END_GARAGE], vBuildingLocations, "heist_dlc_garage_high_new")
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_HIGH_END_GARAGE], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_HIGH_END_GARAGE], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_HIGH_END_GARAGE])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_MEDIUM_GARAGE
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_MEDIUM_GARAGE], vBuildingLocations, "v_garagem")
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_MEDIUM_GARAGE], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_MEDIUM_GARAGE], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_MEDIUM_GARAGE])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_SMALL_GARAGE
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_SMALL_GARAGE], vBuildingLocations, "v_garages")
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_SMALL_GARAGE], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_SMALL_GARAGE], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_SMALL_GARAGE])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_TUNNEL_ENTRANCE
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_TUNNEL_ENTRANCE], vBuildingLocations, GET_INTERIOR_NAME(FMMC_BUILDING_TUNNEL_ENTRANCE))
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_TUNNEL_ENTRANCE], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_TUNNEL_ENTRANCE], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_TUNNEL_STRAIGHT])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_TUNNEL_STRAIGHT
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_TUNNEL_STRAIGHT], vBuildingLocations, GET_INTERIOR_NAME(FMMC_BUILDING_TUNNEL_STRAIGHT))
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_TUNNEL_STRAIGHT], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_TUNNEL_STRAIGHT], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_TUNNEL_FLAT_SLOPE_1])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_TUNNEL_FLAT_SLOPE_1
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_TUNNEL_FLAT_SLOPE_1], vBuildingLocations, GET_INTERIOR_NAME(FMMC_BUILDING_TUNNEL_FLAT_SLOPE_1))
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_TUNNEL_FLAT_SLOPE_1], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_TUNNEL_FLAT_SLOPE_1], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_TUNNEL_FLAT_SLOPE_2])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_TUNNEL_FLAT_SLOPE_2
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_TUNNEL_FLAT_SLOPE_2], vBuildingLocations, GET_INTERIOR_NAME(FMMC_BUILDING_TUNNEL_FLAT_SLOPE_2))
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_TUNNEL_FLAT_SLOPE_2], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_TUNNEL_FLAT_SLOPE_2], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_TUNNEL_FLAT_SLOPE_2])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_TUNNEL_30D_RIGHT_1
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_TUNNEL_30D_RIGHT_1], vBuildingLocations, GET_INTERIOR_NAME(FMMC_BUILDING_TUNNEL_30D_RIGHT_1))
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_TUNNEL_30D_RIGHT_1], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_TUNNEL_30D_RIGHT_1], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_TUNNEL_30D_RIGHT_1])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_TUNNEL_30D_LEFT_1
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_TUNNEL_30D_LEFT_1], vBuildingLocations, GET_INTERIOR_NAME(FMMC_BUILDING_TUNNEL_30D_LEFT_1))
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_TUNNEL_30D_LEFT_1], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_TUNNEL_30D_LEFT_1], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_TUNNEL_30D_LEFT_1])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_ARENA
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_ARENA], vBuildingLocations, GET_INTERIOR_NAME(FMMC_BUILDING_ARENA))
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_ARENA], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_ARENA], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_ARENA])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_CASINO_MAIN
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_CASINO_MAIN], vBuildingLocations, GET_INTERIOR_NAME(FMMC_BUILDING_CASINO_MAIN))
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_CASINO_MAIN], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_CASINO_MAIN], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_CASINO_MAIN])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_CASINO_ARCADE
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_CASINO_ARCADE], vBuildingLocations, GET_INTERIOR_NAME(FMMC_BUILDING_CASINO_ARCADE))
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_CASINO_ARCADE], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_CASINO_ARCADE], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_CASINO_ARCADE])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_CASINO_HEIST_PLANNING
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_CASINO_HEIST_PLANNING], vBuildingLocations, GET_INTERIOR_NAME(FMMC_BUILDING_CASINO_HEIST_PLANNING))
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_CASINO_HEIST_PLANNING], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_CASINO_HEIST_PLANNING], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_CASINO_HEIST_PLANNING])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_CASINO_TUNNEL
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_CASINO_TUNNEL], vBuildingLocations, GET_INTERIOR_NAME(FMMC_BUILDING_CASINO_TUNNEL))
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_CASINO_TUNNEL], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_CASINO_TUNNEL], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_CASINO_TUNNEL])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_CASINO_BACK_AREA
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_CASINO_BACK_AREA], vBuildingLocations, GET_INTERIOR_NAME(FMMC_BUILDING_CASINO_BACK_AREA))
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_CASINO_BACK_AREA], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_CASINO_BACK_AREA], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_CASINO_BACK_AREA])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_CASINO_HOTEL_FLOOR
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_CASINO_HOTEL_FLOOR], vBuildingLocations, GET_INTERIOR_NAME(FMMC_BUILDING_CASINO_HOTEL_FLOOR))
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_CASINO_HOTEL_FLOOR], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_CASINO_HOTEL_FLOOR], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_CASINO_HOTEL_FLOOR])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_CASINO_LOADING_BAY
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_CASINO_LOADING_BAY], vBuildingLocations, GET_INTERIOR_NAME(FMMC_BUILDING_CASINO_LOADING_BAY))
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_CASINO_LOADING_BAY], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_CASINO_LOADING_BAY], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_CASINO_LOADING_BAY])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_CASINO_VAULT
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_CASINO_VAULT], vBuildingLocations, GET_INTERIOR_NAME(FMMC_BUILDING_CASINO_VAULT))
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_CASINO_VAULT], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_CASINO_VAULT], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_CASINO_VAULT])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_CASINO_UTILITY_LIFT
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_CASINO_UTILITY_LIFT], vBuildingLocations, GET_INTERIOR_NAME(FMMC_BUILDING_CASINO_UTILITY_LIFT))
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_CASINO_UTILITY_LIFT], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_CASINO_UTILITY_LIFT], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_CASINO_UTILITY_LIFT])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_CASINO_LIFT_SHAFT
								SETUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_CASINO_UTILITY_LIFT], vBuildingLocations, GET_INTERIOR_NAME(FMMC_BUILDING_CASINO_LIFT_SHAFT))
								CAP_INTERIOR(interiorWarp[FMMC_BUILDING_CASINO_LIFT_SHAFT], FALSE)
								DISABLE_INTERIOR(interiorWarp[FMMC_BUILDING_CASINO_LIFT_SHAFT], FALSE)
								bIsReady = IS_INTERIOR_READY(interiorWarp[FMMC_BUILDING_CASINO_LIFT_SHAFT])
							ENDIF
							
							IF bIsReady
								SET_BIT(iLocalBoolCheck4,LBOOL4_APARTMENT_LOADED)
								PRINTLN("[RCC MISSION] PROCESS_APARTMENT_WARPING - OUTSIDE - Local player has entered Building load in radius - building load SUCCESS.")
							ELSE
								PRINTLN("[RCC MISSION] PROCESS_APARTMENT_WARPING - OUTSIDE - Local player has entered Building load in radius - building load FAIL")
							ENDIF
						ELSE
							IF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_WARP_APARTMENT
								CLEANUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_WARP_APARTMENT])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_WARP_CORONER
								CLEANUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_WARP_CORONER])
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_WARP_RECYCLE
								CLEANUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_WARP_RECYCLE])
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_RECYCLE, TRUE)
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_WARP_ABATTOIR
								CLEANUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_WARP_ABATTOIR])
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_ABATTOIR, TRUE)
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_WARP_FARMHOUSE
								CLEANUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_WARP_FARMHOUSE])
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_FARMHOUSE, TRUE)
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_SUB
								CLEANUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_SUB])
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_SUB, TRUE)
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_IAA
								CLEANUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_IAA])
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_IAA, TRUE)
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_LIFEINVADER
								CLEANUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_LIFEINVADER])
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_LIFEINVADER, TRUE)
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_FOUNDRY
								CLEANUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_FOUNDRY])
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_FOUNDRY, TRUE)
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_SILO_01
								CLEANUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_SILO_01])
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_SILO_1, TRUE)
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_SILO_02
								CLEANUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_SILO_02])
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_SILO_2, TRUE)
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_SILO_03
								CLEANUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_SILO_03])
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_SILO_3, TRUE)
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_SERVER_FARM
								CLEANUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_SERVER_FARM])
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_SERVER_FARM, TRUE)								
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_SILO_TUNNEL
								CLEANUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_SILO_TUNNEL])
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_SILO_TUNNEL, TRUE)								
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_SILO_LOOP
								CLEANUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_SILO_LOOP])
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_SILO_LOOP, TRUE)								
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_SILO_ENTRANCE
								CLEANUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_SILO_ENTRANCE])
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_SILO_ENTRANCE, TRUE)								
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_SILO_BASE
								CLEANUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_SILO_BASE])
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_SILO_BASE, TRUE)								
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_OSPREY
								CLEANUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_OSPREY])
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_OSPREY, TRUE)
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_TUNNEL_ENTRANCE
								CLEANUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_TUNNEL_ENTRANCE])
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_TUNNEL_ENTRY, TRUE)	
								
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_TUNNEL_STRAIGHT
								CLEANUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_TUNNEL_STRAIGHT])
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_TUNNEL_STRAIGHT_0, TRUE)
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_TUNNEL_STRAIGHT_1, TRUE)
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_TUNNEL_STRAIGHT_2, TRUE)
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_TUNNEL_FLAT_SLOPE_1
								CLEANUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_TUNNEL_FLAT_SLOPE_1])
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_TUNNEL_SLOPE_FLAT_0, TRUE)
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_TUNNEL_SLOPE_FLAT_1, TRUE)
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_TUNNEL_SLOPE_FLAT_2, TRUE)
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_TUNNEL_FLAT_SLOPE_2
								CLEANUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_TUNNEL_FLAT_SLOPE_2])
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_TUNNEL_FLAT_SLOPE_0, TRUE)
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_TUNNEL_FLAT_SLOPE_1, TRUE)
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_TUNNEL_FLAT_SLOPE_2, TRUE)
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_TUNNEL_30D_RIGHT_1
								CLEANUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_TUNNEL_30D_RIGHT_1])
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_TUNNEL_30D_RIGHT_0, TRUE)
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_TUNNEL_30D_RIGHT_1, TRUE)	
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_TUNNEL_30D_RIGHT_2, TRUE)
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_TUNNEL_30D_RIGHT_3, TRUE)
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_TUNNEL_30D_RIGHT_4, TRUE)
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_TUNNEL_30D_LEFT_1
								CLEANUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_TUNNEL_30D_LEFT_1])
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_TUNNEL_30D_LEFT_0, TRUE)
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_TUNNEL_30D_LEFT_1, TRUE)
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_TUNNEL_30D_LEFT_2, TRUE)
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_TUNNEL_30D_LEFT_3, TRUE)
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_TUNNEL_30D_LEFT_4, TRUE)
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_TUNNEL_30D_LEFT_5, TRUE)
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_ARENA
								CLEANUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_ARENA])
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_ARENA, TRUE)
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_CASINO_MAIN
								CLEANUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_CASINO_MAIN])
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_CASINO_MAIN, TRUE)
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_CASINO_ARCADE
								CLEANUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_CASINO_ARCADE])
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_CASINO_ARCADE, TRUE)
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_CASINO_HEIST_PLANNING
								CLEANUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_CASINO_HEIST_PLANNING])
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_CASINO_HEIST_PLANNING, TRUE)
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_CASINO_TUNNEL
								CLEANUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_CASINO_TUNNEL])
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_CASINO_TUNNEL, TRUE)
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_CASINO_BACK_AREA
								CLEANUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_CASINO_BACK_AREA])
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_CASINO_BACK_AREA, TRUE)
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_CASINO_HOTEL_FLOOR
								CLEANUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_CASINO_HOTEL_FLOOR])
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_CASINO_HOTEL_FLOOR, TRUE)
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_CASINO_LOADING_BAY
								CLEANUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_CASINO_LOADING_BAY])
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_CASINO_LOADING_BAY, TRUE)
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_CASINO_VAULT
								CLEANUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_CASINO_VAULT])
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_CASINO_VAULT, TRUE)
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_CASINO_UTILITY_LIFT
								CLEANUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_CASINO_UTILITY_LIFT])
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_CASINO_UTILITY_LIFT, TRUE)
							ELIF g_FMMC_STRUCT.iBuildingToWarpTo = FMMC_BUILDING_CASINO_LIFT_SHAFT
								CLEANUP_LOCATION_FOR_WARP(interiorWarp[FMMC_BUILDING_CASINO_LIFT_SHAFT])
								SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_CASINO_LIFT_SHAFT, TRUE)
							ENDIF
							
							IF DOES_ENTITY_EXIST(objDoorBlocker)
								DELETE_OBJECT(objDoorBlocker)
							ENDIF
						ENDIF
					ELSE
						//Check against (fWarpRadius - 1) because then it looks like they're actually in the locate, otherwise you just get near it and it fires
						IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed,vBuildingWarpLoc,FALSE) < (fWarpRadius - 1)
							IF IS_PED_ON_FOOT(LocalPlayerPed)
								IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
									PRINTLN("[RCC MISSION] PROCESS_APARTMENT_WARPING - OUTSIDE - Local player has entered Apartment warp!")
									SET_BIT(iLocalBoolCheck4,LBOOL4_IN_APARTMENT) //Set bit as soon as they are definitely starting to enter the apartment - otherwise they might get stuck in the cutscene if the objective moves on
									MC_playerBD[iPartToUse].iApartmentState = ciAS_WarpingIn
									iWarpProgress = 0
									//GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].g_bInMissionCreatorApartment = TRUE
									SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bInMissionCreatorApartment)
									IF IS_PHONE_ONSCREEN()
										HANG_UP_AND_PUT_AWAY_PHONE(FALSE)
									ENDIF
								ENDIF
							ENDIF
						ELIF VDIST2(GET_PLAYER_COORDS(LocalPlayer),vBuildingWarpLoc) >= (fUnloadRadius * fUnloadRadius)
							CLEAR_BIT(iLocalBoolCheck4,LBOOL4_APARTMENT_LOADED)
							PRINTLN("[RCC MISSION] PROCESS_APARTMENT_WARPING - OUTSIDE - Local player has left Apartment unload radius - unloading apartment...")
							UNPIN_INTERIOR(interiorWarp[g_FMMC_STRUCT.iBuildingToWarpTo])
							interiorWarp[g_FMMC_STRUCT.iBuildingToWarpTo] = NULL
							IF DOES_ENTITY_EXIST(objDoorBlocker)
								DELETE_OBJECT(objDoorBlocker)
							ENDIF
						ENDIF
					ENDIF
					
				BREAK
				
				CASE ciAS_WarpingIn
					
					SETUP_CAMERA_FOR_WARP(vStartPos, vStartRot, vEndPos, vEndRot, iDuration, g_FMMC_STRUCT.iBuildingToWarpTo)
					
					SET_CONTROL_SHAKE(PLAYER_CONTROL,0,0)
					
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.iBuildingWarpBS,ciAPW_HideMarker)
						GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
						iA = 200
						DRAW_MARKER(MARKER_CYLINDER,vBuildingWarpLoc,<<0.0,0.0,0.0>>,<<0.0,0.0,0.0>>,<<fWarpRadius,fWarpRadius,0.2>>,iR, iG, iB, iA)
					ENDIF
					
					IF NOT DOES_ENTITY_EXIST(objDoorBlocker)
						MODEL_NAMES mnDoor
						mnDoor = V_ILEV_MP_MID_FRONTDOOR
						
						REQUEST_MODEL(mnDoor)
						IF HAS_MODEL_LOADED(mnDoor)
							objDoorBlocker = CREATE_OBJECT(mnDoor,vDoorLoc,FALSE,FALSE,TRUE)
							SET_ENTITY_ROTATION(objDoorBlocker,vDoorRot)
							SET_ENTITY_INVINCIBLE(objDoorBlocker,TRUE)
							SET_ENTITY_VISIBLE(objDoorBlocker,FALSE)
							FREEZE_ENTITY_POSITION(objDoorBlocker,TRUE)
						ENDIF
					ENDIF
					
					SWITCH iWarpProgress
						
						CASE 0
							
							NET_SET_PLAYER_CONTROL(LocalPlayer,FALSE, NSPC_PREVENT_EVERYBODY_BACKOFF | NSPC_REENABLE_CONTROL_ON_DEATH)
							bHideHud = TRUE
							START_MP_CUTSCENE(FALSE)
							
							IF NEW_LOAD_SCENE_START_SPHERE(vWarpTo,30)
								ciWarpCam = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA",vStartPos,vStartRot,40)
								SET_CAM_ACTIVE(ciWarpCam,TRUE)
								RENDER_SCRIPT_CAMS(TRUE, FALSE)
								REINIT_NET_TIMER(WarpCSTimer)
								REQUEST_ANIM_DICT("mp_doorbell")
								PRINTLN("[RCC MISSION] PROCESS_APARTMENT_WARPING - WARP IN - iWarpProgress 0 >> 1 , camera cut out.")
								iWarpProgress++
							ENDIF
							
						BREAK
						
						CASE 1
							IF (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(WarpCSTimer) >= iTimeCam)
								SET_CAM_PARAMS(ciWarpCam,vEndPos,vEndRot,40,iDuration)
								RESET_NET_TIMER(WarpCSTimer)
								REINIT_NET_TIMER(WarpCSTimer)
								PRINTLN("[RCC MISSION] PROCESS_APARTMENT_WARPING - WARP IN - iWarpProgress 1 >> 2 , camera pan up.")
								iWarpProgress++
							ENDIF
							
						BREAK
						
						CASE 2
							
							//Set camera as focussed outside to keep it non-LOD
							IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
								SET_FOCUS_POS_AND_VEL(vEndPos,<<0,0,0>>)
							ENDIF
							
							IF (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(WarpCSTimer) >= 3500)
								SET_ENTITY_VISIBLE(LocalPlayerPed,FALSE)
							ENDIF
							
							IF IS_NEW_LOAD_SCENE_LOADED()
								IF (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(WarpCSTimer) >= iDuration)
									RESET_NET_TIMER(WarpCSTimer)
									SET_ENTITY_COORDS(LocalPlayerPed, vWarpTo)
									SET_ENTITY_HEADING(LocalPlayerPed,	-4.077)
									PRINTLN("[RCC MISSION] PROCESS_APARTMENT_WARPING - WARP IN - iWarpProgress 2 >> 3 , player has loaded in.")
									iWarpProgress++
								ENDIF
							ELSE
								IF (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(WarpCSTimer) >= iDuration + 2000)
									RESET_NET_TIMER(WarpCSTimer)
									SET_ENTITY_COORDS(LocalPlayerPed, vWarpTo)
									SET_ENTITY_HEADING(LocalPlayerPed,	-4.077)
									PRINTLN("[RCC MISSION] PROCESS_APARTMENT_WARPING - WARP IN - iWarpProgress 2 >> 3 , player has warped in - loading took too long!")
									iWarpProgress++
								ENDIF
							ENDIF
							
							//TODO - REMOVE THIS FOR NON APARTMENT SCENES - SHOULD BE A SKIP CASE
							IF iWarpProgress = 3 
								IF (g_FMMC_STRUCT.iBuildingToWarpTo > FMMC_BUILDING_WARP_APARTMENT)
									IF DOES_BLIP_EXIST(AptWarpBlip)
										REMOVE_BLIP(AptWarpBlip)
									ENDIF
									
									SET_ENTITY_VISIBLE(LocalPlayerPed,TRUE)
									
									SET_BIT(iLocalBoolCheck4,LBOOL4_IN_APARTMENT)
									
									ciWarpCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE) 
									SET_CAM_ACTIVE(ciWarpCam, TRUE)
									
									SETUP_POST_WARP_CAMERA(ciWarpCam, g_FMMC_STRUCT.iBuildingToWarpTo)
									
									RENDER_SCRIPT_CAMS(TRUE, FALSE)
									
									iWarpProgress = 5
								ENDIF
							ENDIF
						
						BREAK
						
						CASE 3
							
							IF MC_serverBD.iPartWarpingIn = -1
								IF NOT bIsLocalPlayerHost
									IF NOT IS_BIT_SET(iLocalBoolCheck4,LBOOL4_APTWARP_REQUESTSENT)
										BROADCAST_FMMC_APARTMENTWARP_CS_REQUEST(iPartToUse)
										SET_BIT(iLocalBoolCheck4,LBOOL4_APTWARP_REQUESTSENT)
									ENDIF
								ELSE
									MC_serverBD.iPartWarpingIn = iPartToUse
								ENDIF
							ELSE
								IF MC_serverBD.iPartWarpingIn = iPartToUse
									//We have the go ahead:
									IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD.niAptDoor)
									//Check that we have control of the bank manager if we need to reset his position
									OR ( (MC_playerBD[iPartToUse].iteam = 0 AND NOT IS_BIT_SET(iLocalBoolCheck5,LBOOL5_BANKMANAGER_POSCHECK)) 
									   AND (NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[9])) )
										NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD.niAptDoor)
										//Request the bank manager:
										IF (MC_playerBD[iPartToUse].iteam = 0 AND NOT IS_BIT_SET(iLocalBoolCheck5,LBOOL5_BANKMANAGER_POSCHECK))
											NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[9])
										ENDIF
									ELSE
										IF MC_playerBD[iPartToUse].iteam = 0
										AND NOT IS_BIT_SET(iLocalBoolCheck5,LBOOL5_BANKMANAGER_POSCHECK)
											//Ugly hack for getting the bank manager to stay in the apartment:
											INT iBankManagerID
											iBankManagerID = 9
											IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[ iBankManagerID ])
												IF NOT IS_PED_INJURED(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[ iBankManagerID ]))
													IF IS_LOCATION_IN_FMMC_APARTMENT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iBankManagerID ].vpos)
														SET_ENTITY_COORDS(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[ iBankManagerID ]),g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iBankManagerID ].vpos + <<0,0,-0.0002>>)
														FORCE_ROOM_FOR_ENTITY(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[ iBankManagerID ]), interiorWarp[g_FMMC_STRUCT.iBuildingToWarpTo], -908149619)
														FREEZE_ENTITY_POSITION(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[ iBankManagerID ]),TRUE)
														SET_BIT(iLocalBoolCheck5,LBOOL5_BANKMANAGER_POSHANDLING)
														PRINTLN("[RCC MISSION] PROCESS_APARTMENT_WARPING - Bank Manager spawn position successfully reset")
													ELSE
														PRINTLN("[RCC MISSION] PROCESS_APARTMENT_WARPING - Bank Manager spawn position not in the apartment!")
													ENDIF
												ELSE
													PRINTLN("[RCC MISSION] PROCESS_APARTMENT_WARPING - Bank Manager is injured!")
												ENDIF
											ELSE
												PRINTLN("[RCC MISSION] PROCESS_APARTMENT_WARPING - Bank Manager doesn't exist!")
											ENDIF
										ENDIF
										
										SET_BIT(iLocalBoolCheck5,LBOOL5_BANKMANAGER_POSCHECK)
										
										FORCE_ROOM_FOR_ENTITY(NET_TO_ENT(MC_serverBD.niAptDoor), interiorWarp[g_FMMC_STRUCT.iBuildingToWarpTo], -908149619)
										FORCE_ROOM_FOR_ENTITY(objDoorBlocker,interiorWarp[g_FMMC_STRUCT.iBuildingToWarpTo], -908149619)
										PRINTLN("[RCC MISSION] PROCESS_APARTMENT_WARPING - WARP IN - iWarpProgress 3 >> 4 , local player has go ahead for entry cutscene.")
										iWarpProgress++
									ENDIF
								ELSE
									//Wait for the next free cutscene
									CLEAR_BIT(iLocalBoolCheck4,LBOOL4_APTWARP_REQUESTSENT)
								ENDIF
							ENDIF
							
						BREAK
						
						CASE 4
							
							IF IS_BIT_SET(iLocalBoolCheck5,LBOOL5_BANKMANAGER_POSHANDLING)
								INT iBankManagerID
								iBankManagerID = 9
								
								IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[ iBankManagerID ])
									IF NOT IS_PED_INJURED(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[ iBankManagerID ]))
										IF IS_LOCATION_IN_FMMC_APARTMENT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iBankManagerID ].vpos)
											SET_ENTITY_COORDS(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[ iBankManagerID ]),g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iBankManagerID ].vpos + <<0,0,-0.0002>>)
											FORCE_ROOM_FOR_ENTITY(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[ iBankManagerID ]), interiorWarp[g_FMMC_STRUCT.iBuildingToWarpTo], -908149619)
											FREEZE_ENTITY_POSITION(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[ iBankManagerID ]),TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							IF DOES_BLIP_EXIST(AptWarpBlip)
								REMOVE_BLIP(AptWarpBlip)
							ENDIF
							
							IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD.niAptDoor)
								NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD.niAptDoor)
							ENDIF
							
							IF HAS_ANIM_DICT_LOADED("mp_doorbell")
							AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD.niAptDoor)
								
								SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED, TRUE)
								SET_ENTITY_VISIBLE(LocalPlayerPed,TRUE)
								
								iAptSynchScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vDoorLoc,vDoorRot)
								NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(LocalPlayerPed,iAptSynchScene,"mp_doorbell","PLAYER_EXIT_L_PEDA",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_TAG_SYNC_OUT|SYNCED_SCENE_USE_PHYSICS,RBF_NONE,WALK_BLEND_IN)
								NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_ENT(MC_serverBD.niAptDoor),iAptSynchScene,"mp_doorbell","PLAYER_EXIT_L_DOOR",INSTANT_BLEND_IN,INSTANT_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_USE_PHYSICS)
								//NETWORK_ADD_SYNCHRONISED_SCENE_CAMERA(iAptSynchScene,"mp_doorbell","PLAYER_EXIT_L_CAM")
								NETWORK_START_SYNCHRONISED_SCENE(iAptSynchScene)
								
								bCamera = FALSE
								iDoorSoundStage = 0
								
								PRINTLN("[RCC MISSION] PROCESS_APARTMENT_WARPING - WARP IN - iWarpProgress 4 >> 5 , open door sync scene has begun.")
								
								iWarpProgress++
							ENDIF
							
						BREAK
						
						CASE 5
							
							//Cutscene is playing:
							SWITCH iDoorSoundStage
								CASE 0
									
									IF NOT bCamera
										IF DOES_CAM_EXIST(ciWarpCam)
											DESTROY_CAM(ciWarpCam)
										ENDIF
										CLEAR_FOCUS()
										PRINTLN("[RCC MISSION] PROCESS_APARTMENT_WARPING - CLEAR_FOCUS")
										ciWarpCam = CREATE_CAM("DEFAULT_ANIMATED_CAMERA",TRUE)
										PLAY_SYNCHRONIZED_CAM_ANIM(ciWarpCam,NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iAptSynchScene),"PLAYER_EXIT_L_CAM","mp_doorbell")
										bCamera = TRUE
									ENDIF
									
									IF (GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iAptSynchScene)) >= 0.04)
										PLAY_SOUND_FRONTEND(-1, "WOODEN_DOOR_OPEN_HANDLE_AT", DEFAULT, FALSE)
										SET_PORTAL_SETTINGS_OVERRIDE("V_DLC_HEIST_APARTMENT_DOOR_CLOSED", "V_DLC_HEIST_APARTMENT_DOOR_OPEN")
										
										IF IS_BIT_SET(iLocalBoolCheck5,LBOOL5_BANKMANAGER_POSHANDLING)
											INT iBankManagerID
											iBankManagerID = 9
											
											IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[ iBankManagerID ])
												IF NOT IS_PED_INJURED(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[ iBankManagerID ]))
													IF IS_LOCATION_IN_FMMC_APARTMENT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iBankManagerID ].vpos)
														CLEAR_PED_TASKS_IMMEDIATELY(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[ iBankManagerID ]))
														SET_ENTITY_COORDS(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[ iBankManagerID ]),g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iBankManagerID ].vpos + <<0,0,-0.0002>>)
														FORCE_ROOM_FOR_ENTITY(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[ iBankManagerID ]), interiorWarp[g_FMMC_STRUCT.iBuildingToWarpTo], -908149619)
														FREEZE_ENTITY_POSITION(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[ iBankManagerID ]),FALSE)
														//Make sure he's not falling with any speed
														SET_ENTITY_VELOCITY(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[ iBankManagerID ]),<<0,0,0>>)
													ENDIF
												ENDIF
											ENDIF
											
											CLEAR_BIT(iLocalBoolCheck5,LBOOL5_BANKMANAGER_POSHANDLING)
										ENDIF
										
										//Set up the gameplay cam for cutting back to it later
										SET_GAMEPLAY_CAM_RELATIVE_HEADING(-8.6)
										SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
										
										iDoorSoundStage++
									ELSE
										
										IF IS_BIT_SET(iLocalBoolCheck5,LBOOL5_BANKMANAGER_POSHANDLING)
											INT iBankManagerID
											iBankManagerID = 9
											
											IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[ iBankManagerID ])
												IF NOT IS_PED_INJURED(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[ iBankManagerID ]))
													IF IS_LOCATION_IN_FMMC_APARTMENT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iBankManagerID ].vpos)
														CLEAR_PED_TASKS_IMMEDIATELY(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[ iBankManagerID ]))
														SET_ENTITY_COORDS(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[ iBankManagerID ]),g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iBankManagerID ].vpos + <<0,0,-0.0002>>)
														FORCE_ROOM_FOR_ENTITY(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[ iBankManagerID ]), interiorWarp[g_FMMC_STRUCT.iBuildingToWarpTo], -908149619)
														FREEZE_ENTITY_POSITION(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[ iBankManagerID ]),FALSE)
														//Make sure he's not falling with any speed
														SET_ENTITY_VELOCITY(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[ iBankManagerID ]),<<0,0,0>>)
													ENDIF
												ENDIF
											ENDIF
											
											CLEAR_BIT(iLocalBoolCheck5,LBOOL5_BANKMANAGER_POSHANDLING)
										ENDIF
										
									ENDIF
								BREAK
								
								CASE 1
									IF (GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iAptSynchScene)) >= 0.175)
										PLAY_SOUND_FRONTEND(-1, "WOODEN_DOOR_CLOSING_AT", DEFAULT, FALSE)
										iDoorSoundStage++
									ENDIF
								BREAK
								
								CASE 2
									IF (GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iAptSynchScene)) >= 0.2)
										
										RENDER_SCRIPT_CAMS(FALSE,TRUE,2500)
										
										IF DOES_CAM_EXIST(ciWarpCam)
											IF IS_CAM_ACTIVE(ciWarpCam)
												SET_CAM_ACTIVE(ciWarpCam, FALSE)
											ENDIF
											DESTROY_CAM(ciWarpCam)
										ENDIF
										
										iDoorSoundStage++
									ENDIF
								BREAK
								
								CASE 3
									IF (GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iAptSynchScene)) >= 0.25)
										PLAY_SOUND_FRONTEND(-1, "WOODEN_DOOR_CLOSED_AT", DEFAULT, FALSE)
										REMOVE_PORTAL_SETTINGS_OVERRIDE("V_DLC_HEIST_APARTMENT_DOOR_CLOSED")

										iDoorSoundStage++
									ENDIF
								BREAK
							ENDSWITCH
							
							IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD.niAptDoor)
								NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD.niAptDoor)
							ENDIF
							
							IF NOT IS_PED_PERFORMING_TASK(LocalPlayerPed, SCRIPT_TASK_SYNCHRONIZED_SCENE)
								
								PRINTLN("[RCC MISSION] PROCESS_APARTMENT_WARPING - WARP IN COMPLETE - iWarpProgress 5 >> 0 , sync scene has finished.")
								
								bHideHud = FALSE
								CLEANUP_MP_CUTSCENE(DEFAULT,FALSE)
								NET_SET_PLAYER_CONTROL(LocalPlayer,TRUE)
								
								RUN_APARTMENT_PLAYER_CONTROL_ACTIONS_LIMITER()
								
								IF DOES_CAM_EXIST(ciWarpCam)
									//The camera won't exist at this point, it's just a backup
									IF IS_CAM_ACTIVE(ciWarpCam)
										SET_CAM_ACTIVE(ciWarpCam, FALSE)
									ENDIF
									
									DESTROY_CAM(ciWarpCam)
								ENDIF
								
								iWarpProgress = 0
								MC_playerBD[iPartToUse].iApartmentState = ciAS_Inside
							ELSE
								
								IF (GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iAptSynchScene)) >= 0.27)
									IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD.niAptDoor)
										NETWORK_STOP_SYNCHRONISED_SCENE(iAptSynchScene)
										FREEZE_ENTITY_POSITION(NET_TO_ENT(MC_serverBD.niAptDoor),TRUE)
										
										NEW_LOAD_SCENE_STOP()
										
										PRINTLN("[RCC MISSION] PROCESS_APARTMENT_WARPING - WARP IN COMPLETE - iWarpProgress 5 , sync scene stopping...")
										
										IF IS_BIT_SET(iLocalBoolCheck4,LBOOL4_APTWARP_REQUESTSENT)
											CLEAR_BIT(iLocalBoolCheck4,LBOOL4_APTWARP_REQUESTSENT)
											IF NOT bIsLocalPlayerHost
												BROADCAST_FMMC_APARTMENTWARP_CS_RELEASE(iPartToUse)
											ELSE
												MC_serverBD.iPartWarpingIn = -1
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
							ENDIF
						BREAK
						
					ENDSWITCH
					
				BREAK
				
				CASE ciAS_Inside
					
					RUN_APARTMENT_PLAYER_CONTROL_ACTIONS_LIMITER()
					
					IF (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed,vDoorLoc,FALSE) <= fExitRadius)
						IF NOT IS_PHONE_ONSCREEN()
						AND LOAD_MENU_ASSETS()
							
							IF NOT IS_BIT_SET(iLocalBoolCheck4,LBOOL4_APARTMENTMENU_LOADED)
								CLEAR_MENU_DATA()
								SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT)
								SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT)
								SET_MENU_ITEM_TOGGLEABLE(FALSE)
								SET_MENU_TITLE("MP_PROP_GEN2a")
								ADD_MENU_ITEM_TEXT(0,"MP_PROP_MENU2d",0,TRUE)
								SET_CURRENT_MENU_ITEM(0)
								ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_ACCEPT, "BB_SELECT")
								SET_BIT(iLocalBoolCheck4,LBOOL4_APARTMENTMENU_LOADED)
							ENDIF
							
							IF MC_serverBD.iPartWarpingIn = -1
								DRAW_MENU()
								
								IF NOT IS_BIT_SET(iLocalBoolCheck4,LBOOL4_APARTMENTMENU_DISPLAYING)
									PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_MP_SOUNDSET", FALSE)
									SET_BIT(iLocalBoolCheck4,LBOOL4_APARTMENTMENU_DISPLAYING)
								ENDIF
								
								IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
									PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_MP_SOUNDSET", FALSE)
									IF NOT bIsLocalPlayerHost
										IF NOT IS_BIT_SET(iLocalBoolCheck4,LBOOL4_APTWARP_REQUESTSENT)
											BROADCAST_FMMC_APARTMENTWARP_CS_REQUEST(iPartToUse)
											SET_BIT(iLocalBoolCheck4,LBOOL4_APTWARP_REQUESTSENT)
										ENDIF
									ELSE
										MC_serverBD.iPartWarpingIn = iPartToUse
									ENDIF
								ENDIF
							ELSE
								CLEAR_BIT(iLocalBoolCheck4,LBOOL4_APTWARP_REQUESTSENT)
								CLEAR_BIT(iLocalBoolCheck4,LBOOL4_APARTMENTMENU_DISPLAYING)
								IF MC_serverBD.iPartWarpingIn = iPartToUse
									//We have the go ahead:
									IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD.niAptDoor)
										NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD.niAptDoor)
									ELSE
										PRINTLN("[RCC MISSION] PROCESS_APARTMENT_WARPING - INSIDE - Local player has interacted with the exit door!")
										MC_playerBD[iPartToUse].iApartmentState = ciAS_WarpingOut
										iWarpProgress = 0
									ENDIF
									
								ENDIF
							ENDIF
							
						ENDIF
					ELSE
						CLEAR_BIT(iLocalBoolCheck4,LBOOL4_APTWARP_REQUESTSENT)
						CLEAR_BIT(iLocalBoolCheck4,LBOOL4_APARTMENTMENU_DISPLAYING)
						
						IF MC_serverBD.iPartWarpingIn = iPartToUse
							IF NOT bIsLocalPlayerHost
								BROADCAST_FMMC_APARTMENTWARP_CS_RELEASE(iPartToUse)
							ELSE
								MC_serverBD.iPartWarpingIn = -1
							ENDIF
						ENDIF
						
						IF IS_BIT_SET(iLocalBoolCheck4,LBOOL4_APARTMENTMENU_LOADED)
							CLEANUP_MENU_ASSETS()
							CLEAR_BIT(iLocalBoolCheck4,LBOOL4_APARTMENTMENU_LOADED)
						ENDIF
					ENDIF
					
				BREAK
				
				CASE ciAS_WarpingOut
					
					SETUP_CAMERA_FOR_WARP(vStartPos, vStartRot, vEndPos, vEndRot, iDuration, g_FMMC_STRUCT.iBuildingToWarpTo,FALSE)
					
					SET_CONTROL_SHAKE(PLAYER_CONTROL,0,0)
					
					IF IS_BIT_SET(iLocalBoolCheck4,LBOOL4_APARTMENTMENU_LOADED)
						CLEANUP_MENU_ASSETS()
						CLEAR_BIT(iLocalBoolCheck4,LBOOL4_APARTMENTMENU_DISPLAYING)
						CLEAR_BIT(iLocalBoolCheck4,LBOOL4_APARTMENTMENU_LOADED)
					ENDIF
					
					SWITCH iWarpProgress
					
						CASE 0
							
							IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD.niAptDoor)
								NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD.niAptDoor)
							ENDIF
							
							RUN_APARTMENT_PLAYER_CONTROL_ACTIONS_LIMITER()
							
							IF NEW_LOAD_SCENE_START_SPHERE(vBuildingWarpLoc,50)
								REQUEST_ANIM_DICT("mp_doorbell")
								IF HAS_ANIM_DICT_LOADED("mp_doorbell")
								AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD.niAptDoor)
									START_MP_CUTSCENE(FALSE)
									bHideHud = TRUE
									NET_SET_PLAYER_CONTROL(LocalPlayer,FALSE, NSPC_PREVENT_EVERYBODY_BACKOFF | NSPC_REENABLE_CONTROL_ON_DEATH)
									
									iAptSynchScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vDoorLoc,vDoorRot)
									NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(LocalPlayerPed,iAptSynchScene,"mp_doorbell","PLAYER_EXIT_R_PEDA",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_TAG_SYNC_OUT|SYNCED_SCENE_USE_PHYSICS,RBF_NONE,WALK_BLEND_IN)
									NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_ENT(MC_serverBD.niAptDoor),iAptSynchScene,"mp_doorbell","PLAYER_EXIT_R_DOOR",INSTANT_BLEND_IN,INSTANT_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_USE_PHYSICS)
									NETWORK_ADD_SYNCHRONISED_SCENE_CAMERA(iAptSynchScene,"mp_doorbell","PLAYER_EXIT_R_CAM")
									NETWORK_START_SYNCHRONISED_SCENE(iAptSynchScene)
									
									PRINTLN("[RCC MISSION] PROCESS_APARTMENT_WARPING - WARP OUT - iWarpProgress 0 >> 1 , synch scene starting...")
									iWarpProgress++
								ENDIF
							ENDIF
							
						BREAK
						
						CASE 1
							
							IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD.niAptDoor)
								NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD.niAptDoor)
							ENDIF
							
							IF (GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iAptSynchScene)) >= 0.01)
								REINIT_NET_TIMER(WarpCSTimer)
								PRINTLN("[RCC MISSION] PROCESS_APARTMENT_WARPING - WARP OUT - iWarpProgress 1 >> 2 , synch scene begun.")
								iDoorSoundStage = 0
								iWarpProgress++
							ENDIF
							
						BREAK
						
						CASE 2
							
							//Cutscene is playing:
							SWITCH iDoorSoundStage
								CASE 0
									IF (GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iAptSynchScene)) >= 0.04)
										PLAY_SOUND_FRONTEND(-1, "WOODEN_DOOR_OPEN_HANDLE_AT", DEFAULT, FALSE)
										iDoorSoundStage++
									ENDIF
								BREAK
								
								CASE 1
									IF (GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iAptSynchScene)) >= 0.175)
										PLAY_SOUND_FRONTEND(-1, "WOODEN_DOOR_CLOSING_AT", DEFAULT, FALSE)
										iDoorSoundStage++
									ENDIF
								BREAK
								
								CASE 2
									IF (GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iAptSynchScene)) >= 0.25)
										PLAY_SOUND_FRONTEND(-1, "WOODEN_DOOR_CLOSED_AT", DEFAULT, FALSE)
										iDoorSoundStage++
									ENDIF
								BREAK
							ENDSWITCH
							
							IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD.niAptDoor)
								NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD.niAptDoor)
							ENDIF
							
							IF (GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iAptSynchScene)) >= 0.35)
								IF IS_NEW_LOAD_SCENE_LOADED()
								OR (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(WarpCSTimer) >= 5000)
									IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD.niAptDoor)
										NETWORK_STOP_SYNCHRONISED_SCENE(iAptSynchScene)
										
										FREEZE_ENTITY_POSITION(NET_TO_ENT(MC_serverBD.niAptDoor),TRUE)
										
										CLEAR_BIT(iLocalBoolCheck4,LBOOL4_APTWARP_REQUESTSENT)
										IF NOT bIsLocalPlayerHost
											BROADCAST_FMMC_APARTMENTWARP_CS_RELEASE(iPartToUse)
										ELSE
											MC_serverBD.iPartWarpingIn = -1
										ENDIF
										
										SET_ENTITY_VISIBLE(LocalPlayerPed,FALSE)
										
										ciWarpCam = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA",vEndPos,vEndRot,40)
										SET_CAM_ACTIVE(ciWarpCam,TRUE)
										RENDER_SCRIPT_CAMS(TRUE, FALSE)
										REINIT_NET_TIMER(WarpCSTimer)
										PRINTLN("[RCC MISSION] PROCESS_APARTMENT_WARPING - WARP OUT - iWarpProgress 2 >> 3 , synch scene ended.")
										iWarpProgress++
									ENDIF
								ENDIF
							ENDIF
							
						BREAK
						
						CASE 3
							
							IF (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(WarpCSTimer) >= 20)
								SET_ENTITY_COORDS(LocalPlayerPed, vBuildingWarpLoc)
								SET_ENTITY_HEADING(LocalPlayerPed,	-0.33)
								//SET_ENTITY_VISIBLE(LocalPlayerPed,TRUE)
								SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
								SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
								
								SET_CAM_PARAMS(ciWarpCam,vStartPos,vStartRot,40,iDuration)
								RESET_NET_TIMER(WarpCSTimer)
								REINIT_NET_TIMER(WarpCSTimer)
								PRINTLN("[RCC MISSION] PROCESS_APARTMENT_WARPING - WARP OUT - iWarpProgress 3 >> 4 , camera pan down.")
								iWarpProgress++
							ENDIF
							
						BREAK
						
						
						CASE 4
							
							IF (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(WarpCSTimer) < 3000 + 1400)
								
								IF (GET_INTERIOR_FROM_ENTITY(LocalPlayerPed) = interiorWarp[g_FMMC_STRUCT.iBuildingToWarpTo])
								OR NOT HAS_COLLISION_LOADED_AROUND_ENTITY(LocalPlayerPed)
									SET_ENTITY_COORDS(LocalPlayerPed, vBuildingWarpLoc)
									SET_ENTITY_HEADING(LocalPlayerPed,	-0.33)
								ELSE
									IF (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(WarpCSTimer) >= 3000)
										//GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].g_bInMissionCreatorApartment = FALSE
										CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bInMissionCreatorApartment)
										SET_ENTITY_VISIBLE(LocalPlayerPed,TRUE)
										SIMULATE_PLAYER_INPUT_GAIT(LocalPlayer,PEDMOVEBLENDRATIO_WALK,4000,47.91,FALSE)
									ENDIF
								ENDIF
								
							ELSE
								//GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].g_bInMissionCreatorApartment = FALSE
								CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bInMissionCreatorApartment)
								SET_ENTITY_VISIBLE(LocalPlayerPed,TRUE)
								
								IF (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(WarpCSTimer) >= (iDuration+150))
									
									REINIT_NET_TIMER(WarpCSTimer)
									
									PRINTLN("[RCC MISSION] PROCESS_APARTMENT_WARPING - WARP OUT - iWarpProgress 4 >> 5 , camera pan finished.")
									iWarpProgress++
								ENDIF
							ENDIF
							
						BREAK
						
						CASE 5
							
							IF (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(WarpCSTimer) >= 2000)
								//We're outside!
								CLEAR_BIT(iLocalBoolCheck4,LBOOL4_IN_APARTMENT)
								NEW_LOAD_SCENE_STOP()
								PRINTLN("[RCC MISSION] PROCESS_APARTMENT_WARPING - WARP OUT - iWarpProgress 5 >> 6 , player about to resume control.")
								
								RENDER_SCRIPT_CAMS(FALSE,FALSE)
								
								DESTROY_CAM(ciWarpCam)
								
								iWarpProgress++
								REINIT_NET_TIMER(WarpCSTimer)
							ENDIF
							
						BREAK
						
						CASE 6
							
							IF (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(WarpCSTimer) >= 250)
								bHideHud = FALSE
								CLEANUP_MP_CUTSCENE()
								SET_RADAR_AS_EXTERIOR_THIS_FRAME()
								NET_SET_PLAYER_CONTROL(LocalPlayer,TRUE)
								
								SET_MINIMAP_BLOCK_WAYPOINT(FALSE)
								
								PRINTLN("[RCC MISSION] PROCESS_APARTMENT_WARPING - WARP OUT - iWarpProgress 6 >> 7 , player has resumed control.")
								RESET_NET_TIMER(WarpCSTimer)
								iWarpProgress++
							ELSE
								SET_RADAR_AS_EXTERIOR_THIS_FRAME()
							ENDIF
						BREAK
						
						CASE 7
							IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed,vBuildingWarpLoc,FALSE) >= fWarpLeftRadius
								iWarpProgress = 0
								MC_playerBD[iPartToUse].iApartmentState = ciAS_Outside
								PRINTLN("[RCC MISSION] PROCESS_APARTMENT_WARPING - WARP OUT COMPLETE - iWarpProgress 7 >> 0 , player has left Apartment warp.")
							ENDIF
							
						BREAK
						
					ENDSWITCH
					
				BREAK
			
			ENDSWITCH
		
		ELSE
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iBuildingWarpBS,ciAPW_DrawBlip)
				IF DOES_BLIP_EXIST(AptWarpBlip)
					REMOVE_BLIP(AptWarpBlip)
				ENDIF
			ENDIF
			
			IF interiorWarp[g_FMMC_STRUCT.iBuildingToWarpTo] != NULL
				UNPIN_INTERIOR(interiorWarp[g_FMMC_STRUCT.iBuildingToWarpTo])
				interiorWarp[g_FMMC_STRUCT.iBuildingToWarpTo] = NULL
				IF DOES_ENTITY_EXIST(objDoorBlocker)
					DELETE_OBJECT(objDoorBlocker)
				ENDIF
			ENDIF
			
			IF bHideHud
				bHideHud = FALSE
				CLEANUP_MP_CUTSCENE()
			ENDIF
			
		ENDIF

	ENDIF

ENDPROC
										
PROC private_RAISE_WHEEL( PED_INDEX &tempPed, SC_WHEEL_LIST wheel, INT bit = -1 )
	SET_HYDRAULIC_WHEEL_STATE( GET_VEHICLE_PED_IS_IN( tempPed ), wheel, HS_WHEEL_FREE, 1.0, 1.0 )
	IF bit >= 0 AND bit < 32
		SET_BIT( iHydraulicDanceBitSet, bit )
	ENDIF
ENDPROC

PROC private_LOWER_WHEEL( PED_INDEX &tempPed, SC_WHEEL_LIST wheel, INT bit = -1 )
	SET_HYDRAULIC_WHEEL_STATE( GET_VEHICLE_PED_IS_IN( tempPed ), wheel, HS_WHEEL_FREE, 0.0, 1.0 )
	IF bit >= 0 AND bit < 32
		SET_BIT( iHydraulicDanceBitSet, bit )
	ENDIF
ENDPROC

PROC private_RAISE_ALL_WHEELS( PED_INDEX &tempPed, INT bit = -1 )
	SET_HYDRAULIC_VEHICLE_STATE( GET_VEHICLE_PED_IS_IN( tempPed ), HS_ALL_BOUNCE )
	
	IF bit >= 0 AND bit < 32
		SET_BIT( iHydraulicDanceBitSet, bit )
	ENDIF
ENDPROC

PROC private_LOWER_ALL_WHEELS( PED_INDEX &tempPed, INT bit = -1 )
	SET_HYDRAULIC_VEHICLE_STATE( GET_VEHICLE_PED_IS_IN( tempPed ), HS_ALL_BOUNCE )
	
	IF bit >= 0 AND bit < 32
		SET_BIT( iHydraulicDanceBitSet, bit )
	ENDIF
ENDPROC

PROC private_RAISE_FRONT_WHEELS( PED_INDEX &tempPed, INT bit = -1 )
	SET_HYDRAULIC_WHEEL_STATE( GET_VEHICLE_PED_IS_IN( tempPed ), SC_WHEEL_CAR_FRONT_LEFT, HS_WHEEL_FREE, 1.0, 1.0 )
	SET_HYDRAULIC_WHEEL_STATE( GET_VEHICLE_PED_IS_IN( tempPed ), SC_WHEEL_CAR_FRONT_RIGHT, HS_WHEEL_FREE, 1.0, 1.0 )
	
	IF bit >= 0 AND bit < 32
		SET_BIT( iHydraulicDanceBitSet, bit )
	ENDIF
ENDPROC

PROC private_LOWER_FRONT_WHEELS( PED_INDEX &tempPed, INT bit = -1 )
	SET_HYDRAULIC_WHEEL_STATE( GET_VEHICLE_PED_IS_IN( tempPed ), SC_WHEEL_CAR_FRONT_LEFT, HS_WHEEL_FREE, 0.0, 1.0 )
	SET_HYDRAULIC_WHEEL_STATE( GET_VEHICLE_PED_IS_IN( tempPed ), SC_WHEEL_CAR_FRONT_RIGHT, HS_WHEEL_FREE, 0.0, 1.0 )
	IF bit >= 0 AND bit < 32
		SET_BIT( iHydraulicDanceBitSet, bit )
	ENDIF
ENDPROC

PROC private_RAISE_BACK_WHEELS( PED_INDEX &tempPed, INT bit = -1 )
	SET_HYDRAULIC_WHEEL_STATE( GET_VEHICLE_PED_IS_IN( tempPed ), SC_WHEEL_CAR_REAR_LEFT, HS_WHEEL_FREE, 1.0, 1.0 )
	SET_HYDRAULIC_WHEEL_STATE( GET_VEHICLE_PED_IS_IN( tempPed ), SC_WHEEL_CAR_REAR_RIGHT, HS_WHEEL_FREE, 1.0, 1.0 )
	
	IF bit >= 0 AND bit < 32
		SET_BIT( iHydraulicDanceBitSet, bit )
	ENDIF
ENDPROC

PROC private_LOWER_BACK_WHEELS( PED_INDEX &tempPed, INT bit = -1 )
	SET_HYDRAULIC_WHEEL_STATE( GET_VEHICLE_PED_IS_IN( tempPed ), SC_WHEEL_CAR_REAR_LEFT, HS_WHEEL_FREE, 0.0, 1.0 )
	SET_HYDRAULIC_WHEEL_STATE( GET_VEHICLE_PED_IS_IN( tempPed ), SC_WHEEL_CAR_REAR_RIGHT, HS_WHEEL_FREE, 0.0, 1.0 )
	
	IF bit >= 0 AND bit < 32
		SET_BIT( iHydraulicDanceBitSet, bit )
	ENDIF
ENDPROC

PROC private_CLEAR_HYDRAULIC_DATA()
	iHydraulicDanceBitSet = 0
	iHydraulicDanceCurrentRandomEvent = 0
	iHydraulicDanceTimer = 0
ENDPROC

PROC private_TEST_REENTER_HYDRAULIC_EVENT()
	IF GET_RANDOM_INT_IN_RANGE( 0, 1000 ) < HDCFG_REENTER_EVENT_CHANCE
		iHydraulicDanceCurrentRandomEvent = GET_RANDOM_INT_IN_RANGE( 1, HDCFG_MAX_RANDOM_EVENTS )
	ENDIF
ENDPROC

PROC private_PROCESS_RANDOM_HYDRAULIC_DANCING_EVENT( PED_INDEX &tempPed )
	
	PRINTLN( "[JJT][Hydraulics] private_PROCESS_RANDOM_HYDRAULIC_DANCING_EVENT. Event = ", iHydraulicDanceCurrentRandomEvent )
	
	SWITCH( iHydraulicDanceCurrentRandomEvent )
	
		//RANDOM MODIFIER 1 - ANTI-CLOCKWISE WAVE
		CASE 1
			IF iHydraulicDanceTimer > 7
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_1 )
					private_LOWER_BACK_WHEELS( tempPed )
					private_LOWER_FRONT_WHEELS( tempPed )
					
					private_RAISE_WHEEL( tempPed, SC_WHEEL_CAR_FRONT_LEFT, HDSA_ACTION_TRIGGERED_1 )
					private_LOWER_WHEEL( tempPed, SC_WHEEL_CAR_FRONT_RIGHT )
				ENDIF
			ENDIF
			
			IF iHydraulicDanceTimer > 14
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_2 )
					private_RAISE_WHEEL( tempPed, SC_WHEEL_CAR_REAR_LEFT, HDSA_ACTION_TRIGGERED_2 )
					private_LOWER_WHEEL( tempPed, SC_WHEEL_CAR_FRONT_LEFT )
				ENDIF
			ENDIF
			
			IF iHydraulicDanceTimer > 21
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_3 )
					private_RAISE_WHEEL( tempPed, SC_WHEEL_CAR_REAR_RIGHT, HDSA_ACTION_TRIGGERED_3 )
					private_LOWER_WHEEL( tempPed, SC_WHEEL_CAR_REAR_LEFT )
				ENDIF
			ENDIF
			
			IF iHydraulicDanceTimer > 28
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_4 )
					private_RAISE_WHEEL( tempPed, SC_WHEEL_CAR_FRONT_RIGHT, HDSA_ACTION_TRIGGERED_4 )
					private_LOWER_WHEEL( tempPed, SC_WHEEL_CAR_REAR_RIGHT )				
				ENDIF
			ENDIF
			
			IF iHydraulicDanceTimer > 35
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_5 )
					private_RAISE_WHEEL( tempPed, SC_WHEEL_CAR_FRONT_LEFT, HDSA_ACTION_TRIGGERED_5 )
					private_LOWER_WHEEL( tempPed, SC_WHEEL_CAR_FRONT_RIGHT )
				ENDIF
			ENDIF
			
			IF iHydraulicDanceTimer > 42
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_6 )
					private_RAISE_WHEEL( tempPed, SC_WHEEL_CAR_REAR_LEFT, HDSA_ACTION_TRIGGERED_6 )
					private_LOWER_WHEEL( tempPed, SC_WHEEL_CAR_FRONT_LEFT )
				ENDIF
			ENDIF
			
			IF iHydraulicDanceTimer > 49
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_7 )
					private_RAISE_WHEEL( tempPed, SC_WHEEL_CAR_REAR_RIGHT, HDSA_ACTION_TRIGGERED_7 )
					private_LOWER_WHEEL( tempPed, SC_WHEEL_CAR_REAR_LEFT )
				ENDIF
			ENDIF
			
			IF iHydraulicDanceTimer > 56
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_8)
					private_RAISE_WHEEL( tempPed, SC_WHEEL_CAR_FRONT_RIGHT, HDSA_ACTION_TRIGGERED_8 )
					private_LOWER_WHEEL( tempPed, SC_WHEEL_CAR_REAR_RIGHT )
					private_CLEAR_HYDRAULIC_DATA()
					private_TEST_REENTER_HYDRAULIC_EVENT()					
				ENDIF
			ENDIF
		BREAK
		//RANDOM MODIFIER 2 - CLOCKWISE WAVE
		CASE 2
			IF iHydraulicDanceTimer > 7
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_1 )
					private_LOWER_BACK_WHEELS( tempPed )
					private_LOWER_FRONT_WHEELS( tempPed )
					
					private_RAISE_WHEEL( tempPed, SC_WHEEL_CAR_FRONT_RIGHT, HDSA_ACTION_TRIGGERED_1 )
					private_LOWER_WHEEL( tempPed, SC_WHEEL_CAR_FRONT_LEFT )
				ENDIF
			ENDIF
			
			IF iHydraulicDanceTimer > 14
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_2 )
					private_RAISE_WHEEL( tempPed, SC_WHEEL_CAR_REAR_RIGHT, HDSA_ACTION_TRIGGERED_2 )
					private_LOWER_WHEEL( tempPed, SC_WHEEL_CAR_FRONT_RIGHT )
				ENDIF
			ENDIF
			
			IF iHydraulicDanceTimer > 21
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_3 )
					private_RAISE_WHEEL( tempPed, SC_WHEEL_CAR_REAR_LEFT, HDSA_ACTION_TRIGGERED_3 )
					private_LOWER_WHEEL( tempPed, SC_WHEEL_CAR_REAR_RIGHT )
				ENDIF
			ENDIF
			
			IF iHydraulicDanceTimer > 28
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_4 )
					private_RAISE_WHEEL( tempPed, SC_WHEEL_CAR_FRONT_LEFT, HDSA_ACTION_TRIGGERED_4 )
					private_LOWER_WHEEL( tempPed, SC_WHEEL_CAR_REAR_LEFT )
				ENDIF
			ENDIF
			
			IF iHydraulicDanceTimer > 35
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_5 )
					private_RAISE_WHEEL( tempPed, SC_WHEEL_CAR_FRONT_RIGHT, HDSA_ACTION_TRIGGERED_5 )
					private_LOWER_WHEEL( tempPed, SC_WHEEL_CAR_FRONT_LEFT )
				ENDIF
			ENDIF
			
			IF iHydraulicDanceTimer > 42
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_6 )
					private_RAISE_WHEEL( tempPed, SC_WHEEL_CAR_REAR_RIGHT, HDSA_ACTION_TRIGGERED_6 )
					private_LOWER_WHEEL( tempPed, SC_WHEEL_CAR_FRONT_RIGHT )
				ENDIF
			ENDIF
			
			IF iHydraulicDanceTimer > 49
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_7 )
					private_RAISE_WHEEL( tempPed, SC_WHEEL_CAR_REAR_LEFT, HDSA_ACTION_TRIGGERED_7 )
					private_LOWER_WHEEL( tempPed, SC_WHEEL_CAR_REAR_RIGHT )
				ENDIF
			ENDIF
			
			IF iHydraulicDanceTimer > 56
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_8 )
					private_RAISE_WHEEL( tempPed, SC_WHEEL_CAR_FRONT_LEFT, HDSA_ACTION_TRIGGERED_8 )
					private_LOWER_WHEEL( tempPed, SC_WHEEL_CAR_REAR_LEFT )
					private_CLEAR_HYDRAULIC_DATA()
					private_TEST_REENTER_HYDRAULIC_EVENT()
				ENDIF
			ENDIF
		BREAK
		//RANDOM MODIFIER 3 - TWO BUNNY HOPS
		CASE 3
			IF iHydraulicDanceTimer > 0
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_1 )
					private_LOWER_BACK_WHEELS( tempPed )
					private_LOWER_FRONT_WHEELS( tempPed )
					
					private_RAISE_ALL_WHEELS( tempPed, HDSA_ACTION_TRIGGERED_1 )
				ENDIF
			ENDIF
			
			IF iHydraulicDanceTimer > 10
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_2 )
					private_LOWER_ALL_WHEELS( tempPed, HDSA_ACTION_TRIGGERED_2 )
				ENDIF
			ENDIF
			
			IF iHydraulicDanceTimer > 50
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_3 )
					private_RAISE_ALL_WHEELS( tempPed, HDSA_ACTION_TRIGGERED_3 )
				ENDIF
			ENDIF
			
			IF iHydraulicDanceTimer > 60
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_4 )
					private_LOWER_ALL_WHEELS( tempPed, HDSA_ACTION_TRIGGERED_4 )
				ENDIF
			ENDIF
			
			IF iHydraulicDanceTimer > 100
				private_CLEAR_HYDRAULIC_DATA()
				private_TEST_REENTER_HYDRAULIC_EVENT()
			ENDIF
		BREAK
		//RANDOM MODIFIER 4 - BACK OFFSET BUNNY HOP
		CASE 4
			IF iHydraulicDanceTimer > 0
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_1 )
					private_LOWER_BACK_WHEELS( tempPed )
					private_LOWER_FRONT_WHEELS( tempPed )
					
					private_RAISE_BACK_WHEELS( tempPed, HDSA_ACTION_TRIGGERED_1 )
				ENDIF
			ENDIF
			
			IF iHydraulicDanceTimer > 10
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_2 )
					private_RAISE_FRONT_WHEELS( tempPed, HDSA_ACTION_TRIGGERED_2 )
					private_LOWER_BACK_WHEELS( tempPed )
				ENDIF
			ENDIF
			
			IF iHydraulicDanceTimer > 20
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_3 )
					private_LOWER_FRONT_WHEELS( tempPed, HDSA_ACTION_TRIGGERED_3 )
				ENDIF
			ENDIF
			
			IF iHydraulicDanceTimer > 30
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_4 )
					private_RAISE_BACK_WHEELS( tempPed, HDSA_ACTION_TRIGGERED_4 )
				ENDIF
			ENDIF
			
			IF iHydraulicDanceTimer > 40
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_5 )
					private_RAISE_FRONT_WHEELS( tempPed, HDSA_ACTION_TRIGGERED_5 )
					private_LOWER_BACK_WHEELS( tempPed )
				ENDIF
			ENDIF
			
			IF iHydraulicDanceTimer > 50
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_6 )
					private_LOWER_FRONT_WHEELS( tempPed, HDSA_ACTION_TRIGGERED_6 )
				ENDIF
			ENDIF
						
			IF iHydraulicDanceTimer > 55
				private_CLEAR_HYDRAULIC_DATA()
				private_TEST_REENTER_HYDRAULIC_EVENT()
			ENDIF
		BREAK
		//RANDOM MODIFIER 5 - BACK OFFSET BUNNY HOP
		CASE 5
			IF iHydraulicDanceTimer > 0
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_1 )
					private_LOWER_BACK_WHEELS( tempPed )
					private_LOWER_FRONT_WHEELS( tempPed )
					
					private_RAISE_FRONT_WHEELS( tempPed, HDSA_ACTION_TRIGGERED_1 )
				ENDIF
			ENDIF
			
			IF iHydraulicDanceTimer > 10
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_2 )
					private_RAISE_BACK_WHEELS( tempPed, HDSA_ACTION_TRIGGERED_2 )
					private_LOWER_FRONT_WHEELS( tempPed )
				ENDIF
			ENDIF
			
			IF iHydraulicDanceTimer > 20
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_3 )
					private_LOWER_BACK_WHEELS( tempPed, HDSA_ACTION_TRIGGERED_3 )
				ENDIF
			ENDIF
			
			IF iHydraulicDanceTimer > 30
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_4 )
					private_RAISE_FRONT_WHEELS( tempPed, HDSA_ACTION_TRIGGERED_4 )
				ENDIF
			ENDIF
			
			IF iHydraulicDanceTimer > 40
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_5 )
					private_RAISE_BACK_WHEELS( tempPed, HDSA_ACTION_TRIGGERED_5 )
					private_LOWER_FRONT_WHEELS( tempPed )
				ENDIF
			ENDIF
			
			IF iHydraulicDanceTimer > 50
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_6 )
					private_LOWER_BACK_WHEELS( tempPed, HDSA_ACTION_TRIGGERED_6 )
				ENDIF
			ENDIF
						
			IF iHydraulicDanceTimer > 55
				private_CLEAR_HYDRAULIC_DATA()
				private_TEST_REENTER_HYDRAULIC_EVENT()
			ENDIF
		BREAK
		//RANDOM MODIFIER 6 - LEFT CORNER BUMP COUNTER
		CASE 6
			IF iHydraulicDanceTimer > 0
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_1 )
					private_LOWER_BACK_WHEELS( tempPed )
					private_LOWER_FRONT_WHEELS( tempPed )
					
					private_RAISE_WHEEL( tempPed, SC_WHEEL_CAR_FRONT_LEFT, HDSA_ACTION_TRIGGERED_1 )
					private_RAISE_WHEEL( tempPed, SC_WHEEL_CAR_REAR_RIGHT )
				ENDIF
			ENDIF
			
			IF iHydraulicDanceTimer > 15
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_2 )
					private_RAISE_WHEEL( tempPed, SC_WHEEL_CAR_FRONT_RIGHT, HDSA_ACTION_TRIGGERED_2 )
					private_LOWER_WHEEL( tempPed, SC_WHEEL_CAR_REAR_LEFT )
				ENDIF
			ENDIF
			
			IF iHydraulicDanceTimer > 30
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_3 )
					private_RAISE_WHEEL( tempPed, SC_WHEEL_CAR_REAR_LEFT, HDSA_ACTION_TRIGGERED_3 )
					private_LOWER_WHEEL( tempPed, SC_WHEEL_CAR_FRONT_RIGHT )
				ENDIF
			ENDIF
			
			IF iHydraulicDanceTimer > 45
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_4 )
					private_RAISE_WHEEL( tempPed, SC_WHEEL_CAR_FRONT_RIGHT, HDSA_ACTION_TRIGGERED_4 )
					private_LOWER_WHEEL( tempPed, SC_WHEEL_CAR_REAR_LEFT )
				ENDIF
			ENDIF
			
			IF iHydraulicDanceTimer > 60
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_5 )
					private_RAISE_WHEEL( tempPed, SC_WHEEL_CAR_REAR_LEFT, HDSA_ACTION_TRIGGERED_5 )
					private_LOWER_WHEEL( tempPed, SC_WHEEL_CAR_FRONT_RIGHT )
				ENDIF
			ENDIF
		
			IF iHydraulicDanceTimer > 75
				private_LOWER_BACK_WHEELS( tempPed )
				private_LOWER_FRONT_WHEELS( tempPed )
				private_CLEAR_HYDRAULIC_DATA()
				private_TEST_REENTER_HYDRAULIC_EVENT()
			ENDIF
		BREAK
		
		//RANDOM MODIFIER 7 - RIGHT CORNER BUMP COUNTER
		CASE 7
			IF iHydraulicDanceTimer > 0
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_1 )
					private_LOWER_BACK_WHEELS( tempPed )
					private_LOWER_FRONT_WHEELS( tempPed )
					
					private_RAISE_WHEEL( tempPed, SC_WHEEL_CAR_FRONT_RIGHT, HDSA_ACTION_TRIGGERED_1 )
					private_RAISE_WHEEL( tempPed, SC_WHEEL_CAR_REAR_LEFT )
				ENDIF
			ENDIF
			
			IF iHydraulicDanceTimer > 15
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_2 )
					private_RAISE_WHEEL( tempPed, SC_WHEEL_CAR_FRONT_LEFT, HDSA_ACTION_TRIGGERED_2 )
					private_LOWER_WHEEL( tempPed, SC_WHEEL_CAR_REAR_RIGHT )
				ENDIF
			ENDIF
			
			IF iHydraulicDanceTimer > 30
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_3 )
					private_RAISE_WHEEL( tempPed, SC_WHEEL_CAR_REAR_RIGHT, HDSA_ACTION_TRIGGERED_3 )
					private_LOWER_WHEEL( tempPed, SC_WHEEL_CAR_FRONT_LEFT )
				ENDIF
			ENDIF
			
			IF iHydraulicDanceTimer > 45
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_4 )
					private_RAISE_WHEEL( tempPed, SC_WHEEL_CAR_FRONT_LEFT, HDSA_ACTION_TRIGGERED_4 )
					private_LOWER_WHEEL( tempPed, SC_WHEEL_CAR_REAR_RIGHT )
				ENDIF
			ENDIF
			
			IF iHydraulicDanceTimer > 60
				IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_5 )
					private_RAISE_WHEEL( tempPed, SC_WHEEL_CAR_REAR_RIGHT, HDSA_ACTION_TRIGGERED_5 )
					private_LOWER_WHEEL( tempPed, SC_WHEEL_CAR_FRONT_LEFT )
				ENDIF
			ENDIF
		
			IF iHydraulicDanceTimer > 75
				private_LOWER_BACK_WHEELS( tempPed )
				private_LOWER_FRONT_WHEELS( tempPed )
				private_CLEAR_HYDRAULIC_DATA()
				private_TEST_REENTER_HYDRAULIC_EVENT()
			ENDIF
		BREAK
	
	ENDSWITCH
	
ENDPROC

PROC PROCESS_HYDRAULIC_DANCING( PED_INDEX &tempPed )
	
	VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN( tempPed )
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST( VEH_TO_NET( tempVeh ) )
		EXIT
	ENDIF
	
	IF NOT NETWORK_HAS_CONTROL_OF_ENTITY( tempVeh )
		EXIT
	ENDIF

	IF iHydraulicDanceDowntimeTimer > 240 + iHydraulicDowntimerVariance
		IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_DOWNTIME )
			SET_BIT( iHydraulicDanceBitSet, HDSA_ACTION_DOWNTIME )
			private_LOWER_BACK_WHEELS( tempPed )
			private_LOWER_FRONT_WHEELS( tempPed )
		ENDIF
		
		IF iHydraulicDanceDowntimeTimer > 330 + iHydraulicDowntimerVariance
			CLEAR_BIT( iHydraulicDanceBitSet, HDSA_ACTION_DOWNTIME )
			iHydraulicDanceDowntimeTimer = 0
			iHydraulicDowntimerVariance = GET_RANDOM_INT_IN_RANGE( -35, 35 )
			private_CLEAR_HYDRAULIC_DATA()
		ENDIF
		
		#IF IS_DEBUG_BUILD
			PRINTLN( "[JJT][Hydraulics] In downtime." )
		#ENDIF
		
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF (GET_FRAME_COUNT() % 10) = 0
			PRINTLN( "[JJT][Hydraulics] Processing Hydraulics Animation. iHydraulicDanceCurrentLoop = ", iHydraulicDanceCurrentLoop,
																	  ", iHydraulicDanceCurrentRandomEvent = ", iHydraulicDanceCurrentRandomEvent, 
																	  ", iHydraulicDanceTimer = ", iHydraulicDanceTimer )
		ENDIF
	#ENDIF

	SWITCH( iHydraulicDanceCurrentLoop )
		//BASE LOOP - FRONT POP UP
		CASE 1 
			//IF WE WANT TO DO THE BASE LOOP
			IF iHydraulicDanceCurrentRandomEvent = 0
				IF iHydraulicDanceTimer > 25
					IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_1 )
						private_RAISE_FRONT_WHEELS( tempPed, HDSA_ACTION_TRIGGERED_1 )
					ENDIF
				ENDIF
				
				IF iHydraulicDanceTimer > 40
					IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_2 )
						private_LOWER_FRONT_WHEELS( tempPed )
						private_CLEAR_HYDRAULIC_DATA()
						
						//CHANCE OF GOING INTO A RANDOM EVENT
						IF GET_RANDOM_INT_IN_RANGE( 0, 1000 ) < HDCFG_ENTER_EVENT_CHANCE
							iHydraulicDanceCurrentRandomEvent = GET_RANDOM_INT_IN_RANGE( 1, HDCFG_MAX_RANDOM_EVENTS )
							PRINTLN( "[JJT][Hydraulics] Going into random event: ", iHydraulicDanceCurrentRandomEvent )
						ENDIF
						
						//5% CHANCE OF SWITCHING BASE LOOP
						IF GET_RANDOM_INT_IN_RANGE( 0, 1000 ) < 100
							PRINTLN( "[JJT][Hydraulics] Switching to base loop 2" )
							iHydraulicDanceCurrentLoop = 2
						ENDIF
					ENDIF
				ENDIF
			ELSE	
				private_PROCESS_RANDOM_HYDRAULIC_DANCING_EVENT( tempPed )
			ENDIF
		BREAK
		//BASE LOOP - BACK POP UP
		CASE 2 
			//IF WE WANT TO DO THE BASE LOOP
			IF iHydraulicDanceCurrentRandomEvent = 0
				IF iHydraulicDanceTimer > 25
					IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_1 )
						private_RAISE_BACK_WHEELS( tempPed, HDSA_ACTION_TRIGGERED_1 )
					ENDIF
				ENDIF
				
				IF iHydraulicDanceTimer > 40
					IF NOT IS_BIT_SET( iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_2 )
						private_LOWER_BACK_WHEELS( tempPed )
						private_CLEAR_HYDRAULIC_DATA()
						
						//CHANCE OF GOING INTO A RANDOM EVENT
						IF GET_RANDOM_INT_IN_RANGE( 0, 1000 ) < HDCFG_ENTER_EVENT_CHANCE
							iHydraulicDanceCurrentRandomEvent = GET_RANDOM_INT_IN_RANGE( 1, HDCFG_MAX_RANDOM_EVENTS )
						ENDIF
						
						//5% CHANCE OF SWITCHING BASE LOOP
						IF GET_RANDOM_INT_IN_RANGE( 0, 1000 ) < 100
							PRINTLN( "[JJT][Hydraulics] Switching to base loop 1" )
							iHydraulicDanceCurrentLoop = 1
						ENDIF
					ENDIF
				ENDIF
			ELSE	
				private_PROCESS_RANDOM_HYDRAULIC_DANCING_EVENT( tempPed )
			ENDIF
		BREAK
		
		CASE 3
			
		BREAK
		
	ENDSWITCH
ENDPROC

//=========================
//	HALLOWEEN specific logic
//	- Hunted player: heartbeat controller vibration
// 	- Hunted player: friendly vibration (in radius)
//  - Hunter player: heartbeat sounds from hunted players
//=========================

FUNC BOOL private_DO_RUN_HALLOWEEN_LOGIC()

	IF NOT IS_BIT_SET(MC_serverbd.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
		RETURN FALSE
	ENDIF
	
	//Hard block, lets us print a message once and then not come into here again
	IF IS_BIT_SET( iHrtbBitSet, ciHRTB_MODE_NOT_ACTIVE )
		RETURN FALSE
	ELSE	
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciHWN_HEARTBEAT_VIBRATE_ENABLED) 
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciHWN_FRIENDLY_VIBRATE_ENABLED)
			PRINTLN("[JJT] Mission is not a halloween mission.")
			SET_BIT( iHrtbBitSet, ciHRTB_MODE_NOT_ACTIVE )
			RETURN FALSE
		ENDIF
	ENDIF

	IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
		IF iPartToUse >-1
			IF GET_MC_CLIENT_GAME_STATE( iPartToUse ) != GAME_STATE_RUNNING
			OR GET_MC_CLIENT_GAME_STATE( iPartToUse ) = GAME_STATE_END
				PRINTLN("[JJT] PROCESS_HALLOWEEN_MODE_LOGIC mission over (state=", GET_MC_CLIENT_GAME_STATE( iLocalPart ), 
						",missionending=", g_bMissionEnding, " 2)")
				RETURN FALSE
			ENDIF
		ENDIF
	ELSE
		//Stop running if the game has ended
		IF GET_MC_CLIENT_GAME_STATE( iLocalPart ) = GAME_STATE_END
		OR GET_MC_CLIENT_GAME_STATE( iLocalPart ) != GAME_STATE_RUNNING
		OR g_bMissionEnding
			#IF IS_DEBUG_BUILD
				PRINTLN("[JJT] PROCESS_HALLOWEEN_MODE_LOGIC mission over (state=", GET_MC_CLIENT_GAME_STATE( iLocalPart ), 
						",missionending=", g_bMissionEnding, " 1)")
			#ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	
	//Exit if spectating the jason
	IF iSpectatorTarget != -1
		IF iSpectatorTarget < MAX_NUM_MC_PLAYERS
		AND iSpectatorTarget >= 0
			IF MC_PlayerBD[iSpectatorTarget].iteam = 0
			AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_SLASHERS(g_FMMC_STRUCT.iAdversaryModeType)
				PRINTLN("[JJT] Spectating the scary ped so not running halloween logic.")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC FLOAT private_GET_SECOND_HEARTBEAT_TIME(FLOAT distToScaredPed = -1.0)
	FLOAT mod = fHrtbDistanceFromScaryPed
	
	//So the scary ped can use this as a guage to scared peds
	IF distToScaredPed != -1.0
		mod = distToScaredPed
	ENDIF
	
	IF mod < 5.5
		mod = 5.5
	ENDIF	
	RETURN ciHRTB_SECOND_BEAT_BASE_TIME - ( (ciHRTB_PULSE_INCREASE_RADIUS + ( ciHRTB_PULSE_MINIMUM_RADIUS / 2 )
										- mod) * 10 )
ENDFUNC

FUNC FLOAT private_GET_RELOOP_HEARTBEAT_TIME(FLOAT distToScaredPed = -1.0)
	FLOAT mod = fHrtbDistanceFromScaryPed
	
	//So the scary ped can use this as a guage to scared peds
	IF distToScaredPed != -1.0
		mod = distToScaredPed
	ENDIF
	
	IF mod < 5.5
		mod = 5.5
	ENDIF
	RETURN ciHRTB_RELOOP_PULSE_BASE_TIME - ( (ciHRTB_PULSE_INCREASE_RADIUS + ( ciHRTB_PULSE_MINIMUM_RADIUS / 2 )
										 - mod) * 125 )
ENDFUNC

PROC private_HALLOWEEN_INIT_FIND_SCARY_PED()
	//Get the scary ped which should always be on team 0
	IF MC_PlayerBD[iPartToUse].iteam != 0
		piHrtbScaryPedRef = GET_PLAYER_PED( 
								INT_TO_PLAYERINDEX( 
									GET_NEAREST_PLAYER_TO_ENTITY_ON_TEAM( GET_PLAYER_PED( PlayerToUse ), 0 )) )							
		
		IF NOT DOES_ENTITY_EXIST( piHrtbScaryPedRef )
			#IF IS_DEBUG_BUILD
				PRINTLN( "[JJT][Hrtb] Found scary ped, but they don't exist!" )
			#ENDIF
			EXIT
		ENDIF
		
		#IF IS_DEBUG_BUILD
			PRINTLN( "[JJT][Hrtb] Ped isn't scary, and has got a handle to the scary ped." )
		#ENDIF
		
		SET_BIT( iHrtbBitSet, ciHRTB_HAS_HANDLE_TO_SCARY_PED )
	ELSE
		#IF IS_DEBUG_BUILD
			PRINTLN( "[JJT][Hrtb] This local player is the scary ped." )
		#ENDIF
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciHWN_ENABLE_HEARTBEAT_BOTH_TEAMS)		
			SET_BIT( iHrtbBitSet, ciHRTB_LOCAL_PED_IS_SCARY_PED )
		ENDIF
		
		EXIT
	ENDIF
ENDPROC

PROC private_FIND_OTHER_TEAM_FOR_HEARTBEAT()
	IF MC_PlayerBD[iPartToUse].iteam = 0
		piHrtbScaryPedRef = GET_PLAYER_PED(INT_TO_PLAYERINDEX(GET_NEAREST_PLAYER_TO_ENTITY_ON_TEAM( GET_PLAYER_PED( PlayerToUse ), 1)))
		SET_BIT( iHrtbBitSet, ciHRTB_HAS_HANDLE_TO_SCARY_PED )
		IF IS_BIT_SET(iHrtbBitSet, ciHRTB_LOCAL_PED_IS_SCARY_PED)
			PRINTLN("[JT HEART] YOU ARE SCARY, NO VIBRATIONS FOR YOU!")
		ENDIF
		PRINTLN("[JT HEART] Team 0 Scary heartbeat")
		IF NOT DOES_ENTITY_EXIST( piHrtbScaryPedRef )
			#IF IS_DEBUG_BUILD
				PRINTLN( "[JT HEART] Found scary ped, but they don't exist!" )
			#ENDIF
			EXIT
		ENDIF
	ELIF MC_PlayerBD[iPartToUse].iteam = 1
		piHrtbScaryPedRef = GET_PLAYER_PED(INT_TO_PLAYERINDEX(GET_NEAREST_PLAYER_TO_ENTITY_ON_TEAM( GET_PLAYER_PED( PlayerToUse ), 0)))
		SET_BIT( iHrtbBitSet, ciHRTB_HAS_HANDLE_TO_SCARY_PED )
		PRINTLN("[JT HEART] Team 1 Scary heartbeat")
		IF IS_BIT_SET(iHrtbBitSet, ciHRTB_LOCAL_PED_IS_SCARY_PED)
			PRINTLN("[JT HEART] YOU ARE SCARY, NO VIBRATIONS FOR YOU!")
		ENDIF
		IF NOT DOES_ENTITY_EXIST( piHrtbScaryPedRef )
			#IF IS_DEBUG_BUILD
				PRINTLN( "[JT][Hrtb] Found scary ped, but they don't exist!" )
			#ENDIF
			EXIT
		ENDIF
	ELSE
		PRINTLN("[JT HEART] Player is not on a valid team")
	ENDIF
ENDPROC

/// PURPOSE:
///    Hunted player logic
PROC private_HALLOWEEN_PROCESS_SCARED_PED()
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]

	IF iRule < FMMC_MAX_RULES 
	AND iTeam < FMMC_MAX_TEAMS
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciHWN_HEARTBEAT_VIBRATE_ENABLED)
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFive[iRule], ciBS_RULE5_DISABLE_HWN_HEARTBEAT)
			
			PED_INDEX tempPedToUse
			IF iSpectatorTarget != -1 //Is a spectator
				tempPedToUse = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iSpectatorTarget)))
			ELSE
				tempPedToUse = LocalPlayerPed
			ENDIF
			
			IF DOES_ENTITY_EXIST( tempPedToUse )
			AND DOES_ENTITY_EXIST( piHrtbScaryPedRef )
			
				IF NOT IS_PED_INJURED( tempPedToUse )
				AND NOT IS_PED_INJURED( piHrtbScaryPedRef )

					//Distance update every 1/6th of a second. For framerate easing
					iHrtbDistanceUpdateTimer++
					IF iHrtbDistanceUpdateTimer > 10
						fHrtbDistanceFromScaryPed = VDIST( GET_ENTITY_COORDS( piHrtbScaryPedRef ), 
														   GET_ENTITY_COORDS( tempPedToUse ) )
														   
						PRINTLN("[JJT] Distance to scary ped is: ", fHrtbDistanceFromScaryPed )								   				   
						iHrtbDistanceUpdateTimer = 0
					ENDIF

					IF fHrtbDistanceFromScaryPed < g_FMMC_STRUCT.iFvjHeartbeatDistance
						FLOAT mod = fHrtbDistanceFromScaryPed
						IF mod < g_FMMC_STRUCT.iFvjHeartbeatMinDistance
							mod = TO_FLOAT( g_FMMC_STRUCT.iFvjHeartbeatMinDistance )
						ENDIF

						IF NOT HAS_NET_TIMER_STARTED( stHrtbVibrationTimer )
							START_NET_TIMER( stHrtbVibrationTimer )
							CLEAR_BIT( iHrtbBitSet, ciHRTB_DONE_SECOND_HEARTBEAT )		
							
							PRINTLN( "[JJT] Heartbeat 1" )
							SET_CONTROL_SHAKE( PLAYER_CONTROL, 
											   100 - FLOOR(ciHRTB_PULSE_INCREASE_RADIUS - mod), 
											   200 )
						ELSE
							IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( stHrtbVibrationTimer ) > private_GET_SECOND_HEARTBEAT_TIME()
							AND NOT IS_BIT_SET( iHrtbBitSet, ciHRTB_DONE_SECOND_HEARTBEAT )
							
								PRINTLN( "[JJT] Heartbeat 2" )
								SET_CONTROL_SHAKE( PLAYER_CONTROL, 
												   100 - FLOOR(ciHRTB_PULSE_INCREASE_RADIUS - mod), 
												   200 )
								SET_BIT( iHrtbBitSet, ciHRTB_DONE_SECOND_HEARTBEAT )			
							ENDIF

							IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( stHrtbVibrationTimer ) > private_GET_RELOOP_HEARTBEAT_TIME()
								PRINTLN( "[JJT] Heartbeat reset" )
								RESET_NET_TIMER( stHrtbVibrationTimer )
							ENDIF
						ENDIF
					ENDIF
				ELSE
					PRINTLN("[JJT] Ped injured! : ", (IS_PED_INJURED( tempPedToUse )), 
							", ", (IS_PED_INJURED( piHrtbScaryPedRef )) )
				ENDIF
			ELSE
				PRINTLN("[JJT] Error: entity doesn't exist: ", (DOES_ENTITY_EXIST(tempPedToUse)), 
							", ", (DOES_ENTITY_EXIST( piHrtbScaryPedRef )) )
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciHWN_FRIENDLY_VIBRATE_ENABLED)	
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFive[iRule], ciBS_RULE5_DISABLE_HWN_FRIENDLY_VIBRATION)
			IF NOT HAS_NET_TIMER_STARTED( stFrndVibrationTimer )
				START_NET_TIMER( stFrndVibrationTimer )
			ELSE
				//Roughly ten checks for new players every second to lessen computational impact
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( stFrndVibrationTimer ) > 200
					
//					INT tempPedInt = GET_NEAREST_PLAYER_TO_ENTITY_ON_TEAM( GET_PLAYER_PED( PlayerToUse ), MC_PlayerBD[iPartToUse].iteam ) 
//					PLAYER_INDEX tempPedIndex = INT_TO_PLAYERINDEX( tempPedInt )
//					PED_INDEX tempPed = GET_PLAYER_PED( tempPedIndex )
					
					//LOOP THROUGH ALL MISSION PLAYERS
					INT iParticipant
					FOR iParticipant = 0 TO (ciHRTB_MAX_PLAYER_CHECK - 1)
						PARTICIPANT_INDEX participantIndex = INT_TO_PARTICIPANTINDEX(iParticipant)
						
						IF iParticipant < 8
							IF NETWORK_IS_PARTICIPANT_ACTIVE(participantIndex)
								
								PLAYER_INDEX 	playerIndex = NETWORK_GET_PLAYER_INDEX(participantIndex)
								PED_INDEX		playerPedIndex = GET_PLAYER_PED( playerIndex )
								
								IF DOES_ENTITY_EXIST( playerPedIndex )
								AND DOES_ENTITY_EXIST( GET_PLAYER_PED( PlayerToUse ) )
								
									IF NOT IS_PED_INJURED( playerPedIndex )
									AND NOT IS_PED_INJURED( GET_PLAYER_PED( PlayerToUse ) )

										IF VDIST( GET_ENTITY_COORDS( playerPedIndex ), 
										  	      GET_ENTITY_COORDS( GET_PLAYER_PED( PlayerToUse ) ) ) < g_FMMC_STRUCT.iFvjFriendlyVibrDistance
										
											IF NOT IS_BIT_SET( iFrndVibCacheBitSet, iParticipant )
												
												PRINTLN("[JJT] Entered radius of participant ", iParticipant, ". Radius = ", VDIST( GET_ENTITY_COORDS( playerPedIndex ), 
										  	      GET_ENTITY_COORDS( GET_PLAYER_PED( PlayerToUse ) ) ))
												
												SET_CONTROL_SHAKE( PLAYER_CONTROL, 69, 50 )
												RESET_NET_TIMER( stFrndVibrationCDTimer[iParticipant] )

												SET_BIT( iFrndVibCacheBitSet, iParticipant )
											ENDIF
										ELSE
											IF IS_BIT_SET( iFrndVibCacheBitSet, iParticipant )
												IF NOT HAS_NET_TIMER_STARTED( stFrndVibrationCDTimer[iParticipant] )
													START_NET_TIMER( stFrndVibrationCDTimer[iParticipant] )
												ELSE
													IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( stFrndVibrationCDTimer[iParticipant] ) 
													> g_FMMC_STRUCT.iFvjFriendlyVibrCooldown
														
														PRINTLN("[JJT] Left radius of participant ", iParticipant, ". Radius = ", 
																VDIST( GET_ENTITY_COORDS( playerPedIndex ), 
										  	      				GET_ENTITY_COORDS( GET_PLAYER_PED( PlayerToUse ) ) ))
														
														CLEAR_BIT( iFrndVibCacheBitSet, iParticipant )
														RESET_NET_TIMER( stFrndVibrationCDTimer[iParticipant] )
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ELSE
										PRINTLN("[JJT] Ped injured! : ", (IS_PED_INJURED( playerPedIndex )), 
												", ", (IS_PED_INJURED( GET_PLAYER_PED( PlayerToUse ) )) )
									ENDIF
								ELSE
									PRINTLN("[JJT] Error: entity doesn't exist: ", (DOES_ENTITY_EXIST(playerPedIndex)), 
												", ", (DOES_ENTITY_EXIST( GET_PLAYER_PED( PlayerToUse ) )) )
								ENDIF
							ENDIF
						ELSE
							PRINTLN( "[JJT] participantIndex IS MORE THAN 7: ", iParticipant )
						ENDIF
					ENDFOR
					
					REINIT_NET_TIMER( stFrndVibrationTimer )
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC FLOAT private_HALLOWEEN_GET_HEARTBEAT_SOUND_INTENSITY(FLOAT distToPed)
	FLOAT intensity = 1.1 - ((distToPed / 100) * 4 )				
	intensity = CLAMP( intensity, 0.0, 1.0 )

	RETURN intensity
ENDFUNC

PROC private_PROCESS_HALLOWEEN_COUNTDOWN_SOUND()

	INT iTeam = MC_playerBD[iPartToUse].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule < FMMC_MAX_RULES 
	AND iTeam < FMMC_MAX_TEAMS
	AND (HAS_NET_TIMER_STARTED(MC_serverBD_3.tdObjectiveLimitTimer[iTeam]) OR IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH))
		INT iTimeSetting = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveTimeLimitRule[iRule]
		INT iMaxMissionTime = GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(iTimeSetting)
		
		SCRIPT_TIMER stObjectiveStartTime = MC_serverBD_3.tdObjectiveLimitTimer[iTeam]
		INT iMissionTimePassed = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stObjectiveStartTime)
		
		INT iTimeRemaining = iMaxMissionTime - iMissionTimePassed
		
		IF iTimeRemaining <= 2000
			INT iMyTeam = MC_playerBD[iPartToUse].iteam
			INT iMyRule = MC_serverBD_4.iCurrentHighestPriority[iMyTeam]
			
			iTimeRemaining = ((GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iMyTeam].iObjectiveTimeLimitRule[iMyRule]) + GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iMyTeam].iObjectiveSuddenDeathTimeLimit[iMyRule])) - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdObjectiveLimitTimer[MC_playerBD[iPartToUse].iteam]) - MC_serverBD_3.iTimerPenalty[MC_playerBD[iPartToUse].iteam])
			
			IF IS_BIT_SET( iLocalBoolCheck13, LBOOL13_HWN_SWITCHOVER_COUNTDOWN_PLAYED )
				SET_BIT( iLocalBoolCheck13, LBOOL13_HWN_COUNTDOWN_SWITCHED )
				PRINTLN("private_PROCESS_HALLOWEEN_COUNTDOWN_SOUND - Clearing LBOOL13_HWN_SWITCHOVER_COUNTDOWN_PLAYED so it can be played again")
				CLEAR_BIT(iLocalBoolCheck13, LBOOL13_HWN_SWITCHOVER_COUNTDOWN_PLAYED)
			ENDIF
		ENDIF
		
		STRING sSoundSet = "DLC_HALLOWEEN_FVJ_Sounds"
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_SLASHERS_AUDIO)
			sSoundSet = "dlc_xm_sls_Sounds"
		ENDIF
		
		IF iTimeRemaining < 10001
		AND iTimeRemaining > 2000
			IF NOT IS_BIT_SET( iLocalBoolCheck13, LBOOL13_HWN_SWITCHOVER_COUNTDOWN_PLAYED )
				iHalloweenSoundID = GET_SOUND_ID()
				PLAY_SOUND_FRONTEND(iHalloweenSoundID,"Timer_10s",sSoundSet,FALSE)
				PRINTLN("[JJT] PLAY_SOUND_FRONTEND(Timer_10s, ", sSoundSet, ") Halloween switchover.")
				
				SET_BIT( iLocalBoolCheck13, LBOOL13_HWN_SWITCHOVER_COUNTDOWN_PLAYED )
			ELIF NOT IS_BIT_SET( iLocalBoolCheck13, LBOOL13_HWN_ENDGAME_COUNTDOWN_PLAYED )
			AND IS_BIT_SET( iLocalBoolCheck13, LBOOL13_HWN_COUNTDOWN_SWITCHED )
				iHalloweenSoundID = GET_SOUND_ID()
				PLAY_SOUND_FRONTEND(iHalloweenSoundID,"Timer_10s",sSoundSet,FALSE)
				PRINTLN("[JJT] PLAY_SOUND_FRONTEND(Timer_10s, ", sSoundSet, ") Halloween end.")
				
				SET_BIT( iLocalBoolCheck13, LBOOL13_HWN_ENDGAME_COUNTDOWN_PLAYED )
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

PROC PROCESS_HALLOWEEN_MODE_LOGIC()
	
	// Mission is Over. We Don't want vibrations or whatever playing.
	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		EXIT
	ENDIF
	
	IF NOT private_DO_RUN_HALLOWEEN_LOGIC()
		EXIT
	ENDIF
	
	IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_LOST_DAMNED(g_FMMC_STRUCT.iAdversaryModeType)
		private_PROCESS_HALLOWEEN_COUNTDOWN_SOUND()
	ENDIF
	
	//Don't run if this player is a scary ped
	IF IS_BIT_SET( iHrtbBitSet, ciHRTB_LOCAL_PED_IS_SCARY_PED )	
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET( iHrtbBitSet, ciHRTB_HAS_HANDLE_TO_SCARY_PED )
	AND NOT IS_BIT_SET( iHrtbBitSet, ciHRTB_LOCAL_PED_IS_SCARY_PED )	
		private_HALLOWEEN_INIT_FIND_SCARY_PED()
	ELSE
		private_HALLOWEEN_PROCESS_SCARED_PED()
	ENDIF
ENDPROC

PROC PROCESS_INCH_BY_INCH_HEARTBEAT()

	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]

	IF iRule < FMMC_MAX_RULES 
	AND iTeam < FMMC_MAX_TEAMS
	AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciHWN_HEARTBEAT_VIBRATE_ENABLED)
		AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciRUGBY_HEARTBEAT)
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFive[iRule], ciBS_RULE5_DISABLE_HWN_HEARTBEAT)
		
			IF MC_serverBD.iNumObjHighestPriorityHeld[iTeam] > 0
			
				PED_INDEX tempPedToUse
				IF iSpectatorTarget != -1 //Is a spectator
					tempPedToUse = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iSpectatorTarget)))
				ELSE
					tempPedToUse = LocalPlayerPed
				ENDIF
				
				IF DOES_ENTITY_EXIST( tempPedToUse )
				
					IF NOT IS_PED_INJURED( tempPedToUse )
					
						IF NOT CAN_I_SWAN_DIVE(iTeam, iRule)
						OR MC_PlayerBD[iLocalPart].iObjCarryCount = 0

							//Distance update every 1/6th of a second. For framerate easing
							iHrtbDistanceUpdateTimer++
							IF iHrtbDistanceUpdateTimer > 10
								VECTOR vDropPointCentre
								VECTOR vDropOffVec1 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[iRule]
								VECTOR vDropOffVec2 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff2[iRule]
								
								VECTOR vPoint1 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos1
								VECTOR vPoint2 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos2
						
								vDropPointCentre.x = ((vDropOffVec1.x + vDropOffVec2.x) / 2)
								vDropPointCentre.y = ((vDropOffVec1.y + vDropOffVec2.y) / 2)
								vDropPointCentre.z = ((vDropOffVec1.z + vDropOffVec2.z) / 2)
								
								VECTOR vClosestPointToDropCentre = GET_CLOSEST_POINT_ON_LINE(vDropPointCentre, vPoint1, vPoint2)
								VECTOR vClosestPointToPlayer = GET_CLOSEST_POINT_ON_LINE(GET_ENTITY_COORDS( tempPedToUse ), vPoint1, vPoint2)
								
								fDistFromScoringTry = VDIST( vClosestPointToDropCentre, 
																   vClosestPointToPlayer )
								fDistFromScoringTry = fDistFromScoringTry - (g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffRadius[iRule] / 2)
																   					   				   
								iHrtbDistanceUpdateTimer = 0
							ENDIF

							IF fDistFromScoringTry < g_FMMC_STRUCT.iFvjHeartbeatDistance
								FLOAT mod = fDistFromScoringTry
								IF mod < g_FMMC_STRUCT.iFvjHeartbeatMinDistance
									mod = TO_FLOAT( g_FMMC_STRUCT.iFvjHeartbeatMinDistance )
								ENDIF

								IF NOT HAS_NET_TIMER_STARTED( stHrtbVibrationTimer )
									START_NET_TIMER( stHrtbVibrationTimer )
									CLEAR_BIT( iHrtbBitSet, ciHRTB_DONE_SECOND_HEARTBEAT )		
									
									PRINTLN( "[JJT] Heartbeat 1" )
									//stronger heartbeat for player carrying object
									IF MC_PlayerBD[iLocalPart].iObjCarryCount > 0
										SET_CONTROL_SHAKE( PLAYER_CONTROL, 
													   100 - FLOOR(ciHRTB_PULSE_INCREASE_RADIUS - mod), 
													   200 )
									ELSE
										SET_CONTROL_SHAKE( PLAYER_CONTROL, 
													   100 - FLOOR(ciHRTB_PULSE_INCREASE_RADIUS - mod), 
													   50 )
									ENDIF
									
								ELSE
									IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( stHrtbVibrationTimer ) > private_GET_SECOND_HEARTBEAT_TIME(fDistFromScoringTry)
									AND NOT IS_BIT_SET( iHrtbBitSet, ciHRTB_DONE_SECOND_HEARTBEAT )
									
										PRINTLN( "[JJT] Heartbeat 2" )
										IF MC_PlayerBD[iLocalPart].iObjCarryCount > 0
											SET_CONTROL_SHAKE( PLAYER_CONTROL, 
													  	 100 - FLOOR(ciHRTB_PULSE_INCREASE_RADIUS - mod), 
													  	 200 )
										ELSE
											SET_CONTROL_SHAKE( PLAYER_CONTROL, 
													   	100 - FLOOR(ciHRTB_PULSE_INCREASE_RADIUS - mod), 
													  	 50 )
										ENDIF
									
										SET_BIT( iHrtbBitSet, ciHRTB_DONE_SECOND_HEARTBEAT )			
									ENDIF

									IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( stHrtbVibrationTimer ) > private_GET_RELOOP_HEARTBEAT_TIME(fDistFromScoringTry)
										PRINTLN( "[JJT] Heartbeat reset" )
										RESET_NET_TIMER( stHrtbVibrationTimer )
									ENDIF
								ENDIF
							ENDIF
						ELSE
							//if you can swan dive, solid vibration
							IF MC_PlayerBD[iLocalPart].iObjCarryCount > 0
								SET_CONTROL_SHAKE( PLAYER_CONTROL, 
												10000, 
												200 )
							ENDIF
						ENDIF
					ELSE
						PRINTLN("[JJT] Ped injured! : ", (IS_PED_INJURED( tempPedToUse )), 
								", ", (IS_PED_INJURED( piHrtbScaryPedRef )) )
					ENDIF
				ELSE
					PRINTLN("[JJT] Error: entity doesn't exist: ", (DOES_ENTITY_EXIST(tempPedToUse)), 
								", ", (DOES_ENTITY_EXIST( piHrtbScaryPedRef )) )
				ENDIF
				
			ENDIF
			
		ENDIF
		
	ENDIF
ENDPROC

PROC PROCESS_BULLSHARK_HEARTBEAT(BOOL bSlowHeartbeat)

	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]

	IF iRule < FMMC_MAX_RULES 
	AND iTeam < FMMC_MAX_TEAMS
	AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciBULLSHARK_HEARTBEAT_ENABLED)
			
			PED_INDEX tempPedToUse
			IF iSpectatorTarget != -1 //Is a spectator
				tempPedToUse = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iSpectatorTarget)))
			ELSE
				tempPedToUse = LocalPlayerPed
			ENDIF
			
			IF DOES_ENTITY_EXIST( tempPedToUse )
			
				IF NOT IS_PED_INJURED( tempPedToUse )

					//Distance update every 1/6th of a second. For framerate easing
					iHrtbDistanceUpdateTimer++
					IF iHrtbDistanceUpdateTimer > 10					   					   				   
						iHrtbDistanceUpdateTimer = 0
					ENDIF

					//IF fDistFromScoringTry < g_FMMC_STRUCT.iFvjHeartbeatDistance
						FLOAT mod
						IF bSlowHeartbeat
							mod = 16
						ELSE
							mod = 8
						ENDIF

						IF NOT HAS_NET_TIMER_STARTED( stHrtbVibrationTimer )
							START_NET_TIMER( stHrtbVibrationTimer )
							CLEAR_BIT( iHrtbBitSet, ciHRTB_DONE_SECOND_HEARTBEAT )		
							
							PRINTLN( "[JJT] Heartbeat 1" )
							SET_CONTROL_SHAKE( PLAYER_CONTROL, 
										   100 - FLOOR(ciHRTB_PULSE_INCREASE_RADIUS - mod), 
										   200 )
							
						ELSE
							IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( stHrtbVibrationTimer ) > private_GET_SECOND_HEARTBEAT_TIME(mod)
							AND NOT IS_BIT_SET( iHrtbBitSet, ciHRTB_DONE_SECOND_HEARTBEAT )
							
								PRINTLN( "[JJT] Heartbeat 2" )
								SET_CONTROL_SHAKE( PLAYER_CONTROL, 
										  	 100 - FLOOR(ciHRTB_PULSE_INCREASE_RADIUS - mod), 
										  	 200 )
							
								SET_BIT( iHrtbBitSet, ciHRTB_DONE_SECOND_HEARTBEAT )			
							ENDIF

							IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( stHrtbVibrationTimer ) > private_GET_RELOOP_HEARTBEAT_TIME(mod)
								PRINTLN( "[JJT] Heartbeat reset" )
								RESET_NET_TIMER( stHrtbVibrationTimer )
							ENDIF
						ENDIF
					//ENDIF
				ELSE
					PRINTLN("[JJT] Ped injured! : ", (IS_PED_INJURED( tempPedToUse )), 
							", ", (IS_PED_INJURED( piHrtbScaryPedRef )) )
				ENDIF
			ELSE
				PRINTLN("[JJT] Error: entity doesn't exist: ", (DOES_ENTITY_EXIST(tempPedToUse)), 
							", ", (DOES_ENTITY_EXIST( piHrtbScaryPedRef )) )
			ENDIF
			
		ENDIF
		
	ENDIF
ENDPROC



///=====================================
///    
///    Bomber Mission Logic START
///    Author: Jake Thorne
///    
///=====================================

#IF IS_DEBUG_BUILD
PROC private_ADD_PERSISTANT_LINE( VECTOR startPos, VECTOR endPos, INT duration, BOOL failed = FALSE )
	INT i
	FOR i = 0 TO 31	
		IF pLineDuration[i] = 0
			START_NET_TIMER( persistantLine[i] )
			pLineDuration[i] = duration
			pLineStart[i] = startPos
			pLineEnd[i] = endPos
			pLineFailed[i] = failed
			EXIT
		ENDIF
	ENDFOR
ENDPROC

PROC private_DRAW_PERSISTANT_LINES()
	INT i
	FOR i = 0 TO 31
		IF pLineDuration[i] > 0
			IF ( GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( persistantLine[i] ) > pLineDuration[i] )
				RESET_NET_TIMER( persistantLine[i] )
				pLineDuration[i] = 0
				pLineFailed[i] = FALSE
			ELSE
				IF !pLineFailed[i]
					DRAW_LINE( pLineStart[i], pLineEnd[i], 0, 255, 0 )
				ELSE
					DRAW_LINE( pLineStart[i], pLineEnd[i], 255, 0, 0 )
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
ENDPROC
#ENDIF

PROC private_GET_BMB_ARENA_ANGLE()

	INT i = 0
	FLOAT fDeltaX, fDeltaY
	
	//Get the bomber zone
	FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfZones-1)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iType = ciFMMC_ZONE_TYPE__BOMBER_ARENA
			iBmbArenaZoneIndex = i
		ENDIF
	ENDFOR
	
	//Give an assert if there isn't a bomber arena placed but set an angle so play can continue.
	IF iBmbArenaZoneIndex = -1
		SCRIPT_ASSERT( "iBmbArenaZoneIndex is invalid. No Bomber Arena has been set with Bomber Mode enabled.
						Talk to Jake Thorne." )
		fBmbArenaAngle = 0
		
		IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
			MC_playerBD_1[iLocalPart].iBmbCurrentRadius = g_FMMC_STRUCT.iBomberModeExplosionRadius
		ENDIF
		EXIT
	ENDIF
	
	FLOAT point1X = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iBmbArenaZoneIndex].vPos[0].x
	FLOAT point1Y = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iBmbArenaZoneIndex].vPos[0].y
	FLOAT point2X = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iBmbArenaZoneIndex].vPos[1].x
	FLOAT point2Y = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iBmbArenaZoneIndex].vPos[1].y
	
	IF point1X < 0 point1X = -point1X ENDIF
	IF point1Y < 0 point1Y = -point1Y ENDIF
	IF point2X < 0 point2X = -point2X ENDIF
	IF point2Y < 0 point2Y = -point2Y ENDIF
	
	fDeltaY = point2Y - point1Y
	fDeltaX = point2X - point1X
	
	IF fDeltaY < 0 fDeltaY = -fDeltaY ENDIF
	IF fDeltaX < 0 fDeltaX = -fDeltaX ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
		fBmbArenaAngle = GET_HEADING_BETWEEN_VECTORS(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iBmbArenaZoneIndex].vPos[0], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iBmbArenaZoneIndex].vPos[1])
	ELSE
		fBmbArenaAngle = ATAN2(fDeltaY, fDeltaX)
	ENDIF
	
	IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
		MC_playerBD_1[iLocalPart].iBmbCurrentRadius = g_FMMC_STRUCT.iBomberModeExplosionRadius
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA(g_FMMC_STRUCT.iAdversaryModeType)
		MC_playerBD_1[iLocalPart].iBmbCurrentRadius = 2
	ENDIF
	
	PRINTLN( "[JJT][BMB] ANGLE ATAN2(slope): ", fBmbArenaAngle )
	PRINTLN( "[JJT][BMB] MAX EXPLOSION RADIUS: ", g_FMMC_STRUCT.iBomberModeExplosionRadius )
	
	FOR i = 0 TO ciBMB_MAX_TOTAL_BOMBS_PER_PLAYER-1
		oBmbBombs[i].bombObject = NULL
	ENDFOR
		
	FOR i = 0 TO ciBMB_MAX_MAX_ACTIVE_BOMBS-1
		oBmbNetBombs[i].objectID = NULL
	ENDFOR
	
ENDPROC

PROC private_CLEAR_BMB_BOMB( int bmbIndex )
	
	PRINTLN("[BMB] private_CLEAR_BMB_BOMB - Called on bomb ", bmbIndex)

	RESET_NET_TIMER( oBmbBombs[bmbIndex].bombTimer )
	CLEAR_BIT( iBmbPlacedBitSet, BMB_SLOT_1 			+ bmbIndex )
	CLEAR_BIT( iBmbPlacedBitSet, BMB_SLOT_1_ARMING 		+ bmbIndex )
	CLEAR_BIT( iBmbPlacedBitSet, BMB_SLOT_1_HIT_TESTED 	+ bmbIndex )
	CLEAR_BIT( oBmbBombs[bmbIndex].iRayBitSet, BMB_HAS_HIT_FLOOR )
	CLEAR_BIT( oBmbBombs[bmbIndex].iRayBitSet, BMB_SET_IGNORE_COLLISION )
	
	INT i
	FOR i = 0 TO 3
		CLEAR_BIT( oBmbBombs[bmbIndex].iRayBitSet, BMB_HIT_TESTED_DIR_U + i )
		CLEAR_BIT( oBmbBombs[bmbIndex].iRayBitSet, BMB_DO_EXPLODE_PROP_U + i )
		oBmbBombs[bmbIndex].explosionRay[i] = NULL
		oBmbBombs[bmbIndex].hitEntityCreatorIndex[i] = -1
		oBmbBombs[bmbIndex].vHitPos[i] = << 0, 0, 0 >>
	ENDFOR
ENDPROC

PROC private_DELETE_NET_BOMB_BLIPS( INT bmbIndex )
	IF DOES_BLIP_EXIST( oBmbNetBombs[bmbIndex].radiusBlip )
		PRINTLN("[BMB] private_DELETE_NET_BOMB_BLIPS - Cleaning up radiusBlip for oBmbNetBombs[",bmbIndex,"]")
		REMOVE_BLIP( oBmbNetBombs[bmbIndex].radiusBlip )
	ENDIF
	
	IF DOES_BLIP_EXIST( oBmbNetBombs[bmbIndex].objectBlip )
		PRINTLN("[BMB] private_DELETE_NET_BOMB_BLIPS - Cleaning up objectBlip for oBmbNetBombs[",bmbIndex,"]")
		REMOVE_BLIP( oBmbNetBombs[bmbIndex].objectBlip )
	ENDIF
	
	oBmbNetBombs[bmbIndex].objectID = NULL
ENDPROC

PROC private_PROCESS_NET_BMB_ARMING_SOUND_AND_LIGHT()

	INT i
	FOR i = 0 TO ciBMB_MAX_MAX_ACTIVE_BOMBS-1
		
		IF oBmbNetBombs[i].objectID != NULL
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID( oBmbNetBombs[i].objectID )
				INT ms = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( oBmbNetBombs[i].bombTimer )
					
				IF DOES_BLIP_EXIST( oBmbNetBombs[i].radiusBlip )
					SET_BLIP_COORDS( oBmbNetBombs[i].radiusBlip, GET_ENTITY_COORDS( NET_TO_OBJ( oBmbNetBombs[i].objectID ) ) )
				ENDIF
				
				//Incremental and faster lights (and sound) as the timer progresses
				IF ms > ( g_FMMC_STRUCT.iBomberModeExplosionTimer - oBmbNetBombs[i].fBeepTimer )
					
					IF ms < ( g_FMMC_STRUCT.iBomberModeExplosionTimer - oBmbNetBombs[i].fBeepTimer ) + ( oBmbNetBombs[i].fBeepTimer * 0.25 )
						DRAW_LIGHT_WITH_RANGEEX( GET_ENTITY_COORDS( NET_TO_ENT( oBmbNetBombs[i].objectID ) ), 255, 0, 0, 5, 500, 1 )
						//PLACEHOLDER: Temporary SFX for the bomb detonation.
						//PLAY_SOUND_FROM_ENTITY( -1, "Crate_Beacon_Oneshot", NET_TO_ENT( oBmbNetBombs[i].objectID ), "GTAO_FM_Events_Soundset" )
					ELSE
						//Linear beep increase
						oBmbNetBombs[i].fBeepTimer /= 2
					ENDIF
				ENDIF
			ELIF NOT IS_BIT_SET(iActiveBombBlipsBS, i)
				private_DELETE_NET_BOMB_BLIPS( i )
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

/// PURPOSE: Processes the staggered explosion of the bomb
PROC private_PROCESS_BMB_STAGGERED_EXPLOSION( int bmbIndex )

	OBJECT_INDEX objBomb

	IF NOT HAS_NET_TIMER_STARTED( oBmbBombs[bmbIndex].bombTimer )
		START_NET_TIMER( oBmbBombs[bmbIndex].bombTimer )
		
		oBmbBombs[bmbIndex].iCurrentExplosion = 0
		oBmbBombs[bmbIndex].vExplosionPoint = GET_ENTITY_COORDS( NET_TO_OBJ( oBmbBombs[bmbIndex].bombObject ) )
		
		PRINTLN("[BMB] private_PROCESS_BMB_STAGGERED_EXPLOSION - Bomb ", bmbIndex , " starting bombtimer. vExplosionPoint = ", oBmbBombs[bmbIndex].vExplosionPoint)
	ELSE
		IF NETWORK_DOES_NETWORK_ID_EXIST( oBmbBombs[bmbIndex].bombObject )
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(oBmbBombs[bmbIndex].bombObject)
				PRINTLN("[BMB] private_PROCESS_BMB_STAGGERED_EXPLOSION - Cleaning up oBmbBombs[",bmbIndex,"].bombObject")
				CLEAR_BIT(iShouldCleanupBombBS, bmbIndex)
				DELETE_NET_ID( oBmbBombs[bmbIndex].bombObject )
			ELSE
				IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
					PRINTLN("[BMB] private_PROCESS_BMB_STAGGERED_EXPLOSION - Requesting control to clean up oBmbBombs[",bmbIndex,"].bombObject")
					SET_BIT(iShouldCleanupBombBS, bmbIndex)
					IF NOT IS_PLAYER_RESPAWNING(LocalPlayer)
						NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(oBmbBombs[bmbIndex].bombObject)
					ENDIF
				ELSE
					PRINTLN("[BMB] private_PROCESS_BMB_STAGGERED_EXPLOSION - Spectator can't request control!!")
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[BMB] private_PROCESS_BMB_STAGGERED_EXPLOSION - Bomb ", bmbIndex , " bombObject doesn't exist")
		ENDIF
		
		IF DOES_PARTICLE_FX_LOOPED_EXIST(oBmbBombs[bmbIndex].ptfx_BombFuse)
			REMOVE_PARTICLE_FX(oBmbBombs[bmbIndex].ptfx_BombFuse)
		ENDIF
		
		//End the explosion sequence if it has reached the radius of the bomb
		IF oBmbBombs[bmbIndex].iCurrentExplosion >= MC_playerBD_1[iLocalPart].iBmbCurrentRadius
			PRINTLN( "[JJT][BMB] BombNum: ", bmbIndex, " - ","Explosion complete. Clearing flags and rearming the bomb as ready" )
			private_CLEAR_BMB_BOMB( bmbIndex )
			MC_playerBD_1[iLocalPart].iBmbAvailableBombs++
		ENDIF
		
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( oBmbBombs[bmbIndex].bombTimer ) 
			> g_FMMC_STRUCT.iBomberModeExplosionStaggerInterval * oBmbBombs[bmbIndex].iCurrentExplosion
			PRINTLN("[BMB] private_PROCESS_BMB_STAGGERED_EXPLOSION - Timer Expired")
			
			VECTOR lines[ciBMB_DEFAULT_EXPLOSION_LINES]
			FLOAT increment = g_FMMC_STRUCT.fBomberModeExplosionDistanceIncrement * oBmbBombs[bmbIndex].iCurrentExplosion
			
			IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_GRENADE, oBmbBombs[bmbIndex].vExplosionPoint, increment)
				EXIT
			ENDIF
			
			lines[0] = << 0,g_FMMC_STRUCT.fBomberModeExplosionInitialOffset + increment,0 >>  //UP
			lines[1] = << g_FMMC_STRUCT.fBomberModeExplosionInitialOffset + increment,0,0 >>  //RIGHT
			lines[2] = << 0,-g_FMMC_STRUCT.fBomberModeExplosionInitialOffset - increment,0 >> //DOWN
			lines[3] = << -g_FMMC_STRUCT.fBomberModeExplosionInitialOffset - increment,0,0 >> //LEFT
		
			INT i
			FOR i = 0 TO ciBMB_DEFAULT_EXPLOSION_LINES-1
			
				IF  VDIST2( oBmbBombs[bmbIndex].vExplosionPoint, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS( oBmbBombs[bmbIndex].vExplosionPoint, fBmbArenaAngle, lines[i] ) ) <
				   	VDIST2( oBmbBombs[bmbIndex].vExplosionPoint, oBmbBombs[bmbIndex].vHitPos[i] )
					
					PRINTLN( "[BMB] private_PROCESS_BMB_STAGGERED_EXPLOSION Bomb: ", bmbIndex, " distance check passed" )
					
					IF NOT IS_STRING_EMPTY(sVVBombSoundSet)
						IF IS_SOUND_ID_VALID(iVVBombTimerSoundID[bmbIndex])
							IF NOT HAS_SOUND_FINISHED(iVVBombTimerSoundID[bmbIndex])
								STOP_SOUND(iVVBombTimerSoundID[bmbIndex])
								iVVBombTimerSoundID[bmbIndex] = -1
								objBomb = NET_TO_OBJ(oBmbBombs[bmbIndex].bombObject)
								PLAY_SOUND_FROM_ENTITY(-1, "Bomb_Detonate" ,objBomb, sVVBombSoundSet, FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					BOOL bUseSound = TRUE
					
					IF i >= ciBMB_DEFAULT_EXPLOSION_LINES-3
						bUseSound = FALSE
					ENDIF
					
					EXPLOSION_TAG exp = EXP_TAG_GRENADE
					
					IF oBmbBombs[bmbIndex].iCurrentExplosion = 0
						exp = EXP_TAG_GAS_TANK
					ENDIF
					
					IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
						exp = EXP_TAG_HI_OCTANE
					ENDIF
					
					ADD_OWNED_EXPLOSION( LocalPlayerPed, 
								     GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS( oBmbBombs[bmbIndex].vExplosionPoint, fBmbArenaAngle, lines[i] ), 
								     exp,
								     2,
								     bUseSound,
								     FALSE,
								     0.05 )
					private_DELETE_NET_BOMB_BLIPS(bmbIndex)
				ELSE
					IF IS_BIT_SET( oBmbBombs[bmbIndex].iRayBitSet, BMB_DO_EXPLODE_PROP_U + i )
						
						PRINTLN( "[JJT][BMB] BombNum: ", bmbIndex, " - ","Exploding prop: ", i )
						
						IF DOES_ENTITY_EXIST( oBmbBombs[bmbIndex].hitEntity[i] )
							BROADCAST_BMB_PROP_DESTROY_MESSAGE( oBmbBombs[bmbIndex].hitEntityCreatorIndex[i] )	
							
							IF IS_BIT_SET( oBmbBombs[bmbIndex].iRayBitSet, BMB_HIT_POWERUP_PROP_U + i )
								//See BMB_CREATE_PICKUP
								BROADCAST_BMB_PICKUP_CREATED_MESSAGE( GET_RANDOM_INT_IN_RANGE( 0, 1000 ),
																	  GET_RANDOM_INT_IN_RANGE( 0, 1000 ),
																	  GET_ENTITY_COORDS( oBmbBombs[bmbIndex].hitEntity[i] ),
																	  oBmbBombs[bmbIndex].hitPowerupType[i],
																	  i ) //dummy data
																	  
								CLEAR_BIT( oBmbBombs[bmbIndex].iRayBitSet, BMB_HIT_POWERUP_PROP_U + i )
							ENDIF
							
							ADD_OWNED_EXPLOSION( LocalPlayerPed, 
												 oBmbBombs[bmbIndex].vHitPos[i], 
												 EXP_TAG_GAS_TANK, 
												 1.0, TRUE, FALSE, 0.1 )	
												 
						ENDIF
												 
						
						IF oBmbBombs[bmbIndex].hitEntity[i] != NULL
							RELEASE_SCRIPT_GUID_FROM_ENTITY( oBmbBombs[bmbIndex].hitEntity[i] )
							oBmbBombs[bmbIndex].hitEntity[i] = NULL
						ENDIF
						
						CLEAR_BIT( oBmbBombs[bmbIndex].iRayBitSet, BMB_DO_EXPLODE_PROP_U + i )
					ENDIF
				ENDIF
			ENDFOR
			
			oBmbBombs[bmbIndex].iCurrentExplosion++

		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Will send a ray out to see if we need to shorten a line explosion radius. Also sees 
///    if the prop being hit needs to be destroyed by the bomb.
/// PARAMS:
///    direction - 0=up, 1=right, 2=down, 3=left
PROC private_PROCESS_BMB_EXPLOSION_HIT_TEST( INT bmbIndex, INT rayIndex, INT direction )
	IF ( oBmbBombs[bmbIndex].explosionRay[rayIndex] = NULL)

		VECTOR heightOffset	
		FLOAT pointOffset
		VECTOR endPointRaw
		VECTOR endPointOrientated
		
		heightOffset = << 0, 0, 0.3 >>
		pointOffset = g_FMMC_STRUCT.fBomberModeExplosionInitialOffset + 
					  g_FMMC_STRUCT.fBomberModeExplosionDistanceIncrement * MC_playerBD_1[iLocalPart].iBmbCurrentRadius
		
		SWITCH direction
			CASE 0 endPointRaw = << 0, pointOffset, 0 >> BREAK //UP
			CASE 1 endPointRaw = << pointOffset, 0, 0 >> BREAK //RIGHT
			CASE 2 endPointRaw = << 0, -pointOffset, 0 >> BREAK //DOWN
			CASE 3 endPointRaw = << -pointOffset, 0, 0 >> BREAK //LEFT
			DEFAULT
				PRINTLN( "[JJT][BMB] Direction is invalid, set to: ", direction )
				SCRIPT_ASSERT( "Raycast direction is invalid! Speak to Jake Thorne.")
			BREAK
		ENDSWITCH
		
		endPointRaw += heightOffset
		endPointOrientated = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS( GET_ENTITY_COORDS( 
																					NET_TO_OBJ (oBmbBombs[bmbIndex].bombObject ) ), 
																			   	fBmbArenaAngle, 
																				endPointRaw )																				
		oBmbBombs[bmbIndex].explosionRay[rayIndex] = 
			START_SHAPE_TEST_LOS_PROBE( GET_ENTITY_COORDS( NET_TO_OBJ( oBmbBombs[bmbIndex].bombObject ) ) + heightOffset, 
										endPointOrientated,
										SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_OBJECT )
										
		IF oBmbBombs[bmbIndex].hitEntity[rayIndex] != NULL
			RELEASE_SCRIPT_GUID_FROM_ENTITY( oBmbBombs[bmbIndex].hitEntity[rayIndex] )
		ENDIF

		CLEAR_BIT( oBmbBombs[bmbIndex].iRayBitSet, BMB_DO_EXPLODE_PROP_U + rayIndex )
		
		PRINTLN( "[JJT][BMB] BombNum: ", bmbIndex, " - ","Sent out ray: ", rayIndex )
	ELSE
		IF ( GET_SHAPE_TEST_RESULT( oBmbBombs[bmbIndex].explosionRay[rayIndex], 
									oBmbBombs[bmbIndex].iHitSomething, 
									oBmbBombs[bmbIndex].vHitPos[rayIndex], 
									oBmbBombs[bmbIndex].vHitNormal, 
									oBmbBombs[bmbIndex].hitEntity[rayIndex] ) = SHAPETEST_STATUS_RESULTS_READY )
							
			IF direction > 3 AND direction < 0
				PRINTLN( "[JJT][BMB] Direction is invalid, it is set to: ", direction )
				SCRIPT_ASSERT( "Raycast direction is invalid! Speak to Jake Thorne.")
			ENDIF
			
			PRINTLN( "[JJT][BMB] BombNum: ", bmbIndex, " - ","Ray complete: ", rayIndex )
			IF NOT ARE_VECTORS_EQUAL( oBmbBombs[bmbIndex].vHitPos[rayIndex], << 0,0,0 >> )
				
				BOOL found
				
				//Check for Creator props that are supposed to be destructable and flag them as so
				IF DOES_ENTITY_EXIST( oBmbBombs[bmbIndex].hitEntity[rayIndex] )
					PRINTLN( "[JJT][BMB] BombNum: ", bmbIndex, " - ","DOES_ENTITY_EXIST" )
					IF IS_ENTITY_AN_OBJECT( oBmbBombs[bmbIndex].hitEntity[rayIndex] )
						PRINTLN( "[JJT][BMB] BombNum: ", bmbIndex, " - ","IS_ENTITY_AN_OBJECT" )
						IF IS_ENTITY_VISIBLE_TO_SCRIPT( oBmbBombs[bmbIndex].hitEntity[rayIndex] )
							PRINTLN( "[JJT][BMB] BombNum: ", bmbIndex, " - ","IS_ENTITY_VISIBLE_TO_SCRIPT" )
							IF IS_ENTITY_A_MISSION_ENTITY( oBmbBombs[bmbIndex].hitEntity[rayIndex] )
								PRINTLN( "[JJT][BMB] BombNum: ", bmbIndex, " - ","IS_ENTITY_A_MISSION_ENTITY" )
								INT i = 0
								FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfProps-1
									IF i < GET_FMMC_MAX_NUM_PROPS()
									AND !found
										IF oiProps[i] = GET_OBJECT_INDEX_FROM_ENTITY_INDEX( oBmbBombs[bmbIndex].hitEntity[rayIndex] )
											IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].bBmbIsDestructible
												PRINTLN( "[JJT][BMB] BombNum: ", bmbIndex, " - ","Creator Prop is a destructable. Flagging." )
												SET_BIT( oBmbBombs[bmbIndex].iRayBitSet, BMB_DO_EXPLODE_PROP_U + rayIndex )
												
												IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].bBmbIsPowerup
													SET_BIT( oBmbBombs[bmbIndex].iRayBitSet, BMB_HIT_POWERUP_PROP_U + rayIndex )
													oBmbBombs[bmbIndex].hitPowerupType[rayIndex] = g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].eBmbPowerupType
													PRINTLN( "[JJT][BMB] BombNum: ", bmbIndex, " - ","Creator Prop: ", rayIndex,", is a powerup. Flagging." )
												ELSE
													CLEAR_BIT( oBmbBombs[bmbIndex].iRayBitSet, BMB_HIT_POWERUP_PROP_U + rayIndex )
												ENDIF
												
												oBmbBombs[bmbIndex].hitEntityCreatorIndex[rayIndex] = i
												found = TRUE
												BREAKLOOP
											ENDIF
										ENDIF
									ENDIF
								ENDFOR	
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//If not we delete the reference to it
				IF !found
					PRINTLN( "[JJT][BMB] BombNum: ", bmbIndex, " - ","Destructable Creator Prop not hit" )
					RELEASE_SCRIPT_GUID_FROM_ENTITY( oBmbBombs[bmbIndex].hitEntity[rayIndex] )
					CLEAR_BIT( oBmbBombs[bmbIndex].iRayBitSet, BMB_DO_EXPLODE_PROP_U + rayIndex )
					oBmbBombs[bmbIndex].hitEntity[rayIndex] = NULL
				ENDIF
				
			ELSE
				PRINTLN( "[JJT][BMB] BombNum: ", bmbIndex, " - ","Hit nothing." )
			ENDIF
			
			SET_BIT( oBmbBombs[bmbIndex].iRayBitSet, BMB_HIT_TESTED_DIR_U + rayIndex)
			
		ENDIF	
	ENDIF	
ENDPROC

FUNC INT GET_TEAM_COLOUR_FOR_BOMB(INT iTeam)
	SWITCH iTeam
		CASE 0
			RETURN 0
		BREAK
		CASE 1
			RETURN 1
		BREAK
		CASE 2
			RETURN 3
		BREAK
		CASE 3
			RETURN 2
		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

/// PURPOSE: Sets up the bomb and starts the bomb arming process (lights and sounds)
PROC private_PROCESS_BMB_BOMB_CREATION_AND_ARMING( int bmbIndex )
	
	IF NOT HAS_NET_TIMER_STARTED( oBmbBombs[bmbIndex].bombTimer ) 
	AND NOT NETWORK_DOES_NETWORK_ID_EXIST( oBmbBombs[bmbIndex].bombObject ) 
		
		PRINTLN("[BMB] private_PROCESS_BMB_BOMB_ARMING - Bomb ", bmbIndex , " bombObject does not exist and bombTimer hasn't started")
		
		IF IS_PED_IN_ANY_VEHICLE( LocalPlayerPed )
			MODEL_NAMES mnBomb = (INT_TO_ENUM(MODEL_NAMES, HASH("imp_Prop_Bomb_Ball")))
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
				mnBomb = (INT_TO_ENUM(MODEL_NAMES, HASH("ba_Prop_Battle_Bomb")))
			ENDIF

			REQUEST_MODEL(mnBomb)
			REQUEST_NAMED_PTFX_ASSET("scr_stunts")
						
			IF HAS_MODEL_LOADED(mnBomb)
			AND HAS_NAMED_PTFX_ASSET_LOADED("scr_stunts")
				IF CAN_REGISTER_MISSION_OBJECTS( 1 )
				
					VECTOR vehDimMin, vehDimMax, rotPos
					
					GET_MODEL_DIMENSIONS( GET_ENTITY_MODEL( GET_VEHICLE_PED_IS_IN( LocalPlayerPed ) ), vehDimMin, vehDimMax )
					
						rotPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS( 
										GET_VEHICLE_PED_IS_IN( LocalPlayerPed ),
										<< vehDimMax.x - ((vehDimMax.x - vehDimMin.x) / 2), vehDimMin.y - 0.75, vehDimMax.z - (((vehDimMax.z - vehDimMin.z) / 2) - 0.15)>> )
											
					IF CREATE_NET_OBJ( oBmbBombs[bmbIndex].bombObject,
									   mnBomb,
									   rotPos, 
									   FALSE, 
									   TRUE )
						
						IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(oBmbBombs[bmbIndex].ptfx_BombFuse)
							USE_PARTICLE_FX_ASSET("scr_stunts")
							oBmbBombs[bmbIndex].ptfx_BombFuse = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_stunts_bomb_fuse",
																				NET_TO_OBJ(oBmbBombs[bmbIndex].bombObject),
																											<<0.0,0.0,1.25>>,
																											<<0.0,0.0,0.0>>)
						ENDIF
						
						SET_ENTITY_GHOSTED_FOR_GHOST_PLAYERS( NET_TO_OBJ(oBmbBombs[bmbIndex].bombObject), TRUE)
						
						
						VECTOR vForwardVector = -GET_ENTITY_FORWARD_VECTOR( NET_TO_OBJ(oBmbBombs[bmbIndex].bombObject))
						
						SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES( oBmbBombs[bmbIndex].bombObject, TRUE ) 
						APPLY_FORCE_TO_ENTITY( NET_TO_OBJ( oBmbBombs[bmbIndex].bombObject ), 
											   APPLY_TYPE_IMPULSE, 
											   vForwardVector, 
											   << 0,0,0 >>, 0, 
											   TRUE, TRUE, TRUE )
											   
						//SET_ENTITY_CAN_BE_DAMAGED( NET_TO_OBJ( oBmbBombs[bmbIndex].bombObject ), FALSE )
						SET_ENTITY_HEALTH(NET_TO_ENT(oBmbBombs[bmbIndex].bombObject), 150)
						
						IF NETWORK_DOES_NETWORK_ID_EXIST( oBmbBombs[bmbIndex].bombObject )
							PRINTLN("[BMB] private_PROCESS_BMB_BOMB_ARMING - Bomb ", bmbIndex , " is valid")
						ELSE
							PRINTLN("[BMB] private_PROCESS_BMB_BOMB_ARMING - Bomb ", bmbIndex , "is invalid")
						ENDIF
						
						IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
							SET_OBJECT_TINT_INDEX(NET_TO_OBJ(oBmbBombs[bmbIndex].bombObject), GET_TEAM_COLOUR_FOR_BOMB(MC_playerBD[iLocalPart].iteam))
							SET_ENTITY_PROOFS(NET_TO_OBJ(oBmbBombs[bmbIndex].bombObject), TRUE, TRUE, TRUE, TRUE, TRUE, TRUE)
						ENDIF
						
						BROADCAST_BMB_BOMB_CREATED_MESSAGE( oBmbBombs[bmbIndex].bombObject, MC_playerBD_1[iLocalPart].iBmbCurrentRadius )
						
						START_NET_TIMER( oBmbBombs[bmbIndex].bombTimer )
						oBmbBombs[bmbIndex].fBeepTimer = TO_FLOAT( g_FMMC_STRUCT.iBomberModeExplosionTimer )
						
						IF NOT IS_STRING_EMPTY(sVVBombSoundSet)
							IF iVVBombTimerSoundID[bmbIndex] = -1
								PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND - Playing Bomb_Timer_Loop")
								OBJECT_INDEX objBomb = NET_TO_OBJ(oBmbBombs[bmbIndex].bombObject)
								iVVBombTimerSoundID[bmbIndex] = GET_SOUND_ID()
								PLAY_SOUND_FROM_ENTITY(iVVBombTimerSoundID[bmbIndex], "Bomb_Timer_Loop",objBomb, sVVBombSoundSet, TRUE)
							ENDIF
						ENDIF
						
						PRINTLN( "[JJT][BMB] BombNum: ", bmbIndex, " - ","Created bomb" )	
					ELSE
						PRINTLN("[BMB] private_PROCESS_BMB_BOMB_ARMING - Bomb ", bmbIndex , " failed to create object")
					ENDIF
				ELSE
					PRINTLN("[BMB] private_PROCESS_BMB_BOMB_ARMING - Bomb ", bmbIndex , " can't register anymore objects")
				ENDIF
			ELSE
				PRINTLN("[BMB] private_PROCESS_BMB_BOMB_ARMING - Bomb ", bmbIndex , " awaiting assets loading")
			ENDIF
		ELSE
			//If ped isn't in a vehicle between pressing the bomb button and getting in here
			//cancel the bomb placement
			PRINTLN( "[JJT][BMB] BombNum: ", bmbIndex, " - ","Ped registered as not in a vehicle between button press and spawning the prop!" )
			private_CLEAR_BMB_BOMB( bmbIndex )
		ENDIF
	ELSE
		IF NETWORK_DOES_NETWORK_ID_EXIST( oBmbBombs[bmbIndex].bombObject )
			INT ms = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( oBmbBombs[bmbIndex].bombTimer )
			
			PRINTLN("[BMB] private_PROCESS_BMB_BOMB_ARMING - Bomb ", bmbIndex , " bombObject exists and bombTimer has started (", ms, ")")
			
			//Check for obstacles right before the bomb explodes
			IF ms > g_FMMC_STRUCT.iBomberModeExplosionTimer * 0.85
				PRINTLN("[BMB] private_PROCESS_BMB_BOMB_ARMING - Bomb ", bmbIndex , " bombTimer is 85% done(", ms, ") doing hit tests")
				
				IF NOT IS_BIT_SET( iBmbPlacedBitSet, BMB_SLOT_1_HIT_TESTED + bmbIndex )
					INT i
					FOR i = 0 TO 3
						IF NOT IS_BIT_SET( oBmbBombs[bmbIndex].iRayBitSet, BMB_HIT_TESTED_DIR_U + i )
							private_PROCESS_BMB_EXPLOSION_HIT_TEST( bmbIndex, i, i )
						ENDIF
					ENDFOR
					
					IF  IS_BIT_SET( oBmbBombs[bmbIndex].iRayBitSet, BMB_HIT_TESTED_DIR_U )
					AND IS_BIT_SET( oBmbBombs[bmbIndex].iRayBitSet, BMB_HIT_TESTED_DIR_R )
					AND IS_BIT_SET( oBmbBombs[bmbIndex].iRayBitSet, BMB_HIT_TESTED_DIR_D )
					AND IS_BIT_SET( oBmbBombs[bmbIndex].iRayBitSet, BMB_HIT_TESTED_DIR_L )
						
						PRINTLN( "[JJT][BMB] BombNum: ", bmbIndex, " - ","Sucessfully completed object hit testing" )
						SET_BIT( iBmbPlacedBitSet, BMB_SLOT_1_HIT_TESTED + bmbIndex )
					ENDIF
				ENDIF
			ENDIF

			//Continue to the bomb explosion logic
			IF ms > g_FMMC_STRUCT.iBomberModeExplosionTimer
			OR GET_ENTITY_HEALTH(NET_TO_ENT(oBmbBombs[bmbIndex].bombObject)) < 100 
				PRINTLN( "[JJT][BMB] BombNum: ", bmbIndex, " - ","Arming proc complete, moving to staggered explosion." )
				
				OBJECT_INDEX objBomb
				IF NOT IS_STRING_EMPTY(sVVBombSoundSet)
					objBomb = NET_TO_OBJ(oBmbBombs[bmbIndex].bombObject)
					STOP_SOUND(iVVBombTimerSoundID[bmbIndex])
					iVVBombTimerSoundID[bmbIndex] = -1
					
					IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
						PLAY_SOUND_FROM_ENTITY(-1, "Bomb_Detonate" ,objBomb, sVVBombSoundSet, TRUE)
						PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND - Bomb_Detonate")
					ENDIF
				ENDIF
				
				RESET_NET_TIMER( oBmbBombs[bmbIndex].bombTimer )
				CLEAR_BIT( iBmbPlacedBitSet, BMB_SLOT_1_ARMING + bmbIndex )	
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC private_BMB_LOADING_PICKUP()
	INT i = 0
	FOR i = 0 TO ciBMB_MAX_POWERUPS-1
		IF IS_BIT_SET( iBmbPowerupsLoadingBitSet, i )
			SWITCH( iBmbPowerupTypeToLoad[i] )
				CASE 1 //ciBMB_POWERUP_DROP_CHANCE_SIZE
					REQUEST_MODEL(PROP_MP_BOOST_01)
					
					IF HAS_MODEL_LOADED(PROP_MP_BOOST_01)
						
						PRINTLN( "[JJT] NET PICKUP - Making PROP_MP_BOOST_01" )
						iBmbPlacementFlags = 0
						SET_BIT(iBmbPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_MAP))
						SET_BIT(iBmbPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
						SET_BIT(iBmbPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FACEPLAYER))
						
						pBmbPowerups[i] = CREATE_PICKUP_ROTATE (PICKUP_VEHICLE_CUSTOM_SCRIPT_LOW_GLOW, 
																vBmbPowerupPositions[i] + << 0,0,1.5 >>, 
																<<0.0,0.0,0>>, 
																iBmbPlacementFlags, 
																0, 
																DEFAULT,  
																DEFAULT, 
																PROP_MP_BOOST_01 )						
						CLEAR_BIT( iBmbPowerupsLoadingBitSet, i )
					ENDIF
				BREAK
				CASE 2 //ciBMB_POWERUP_DROP_CHANCE_AMOUNT
					REQUEST_MODEL(PROP_MP_ROCKET_01)
					
					IF HAS_MODEL_LOADED(PROP_MP_ROCKET_01)
						
						PRINTLN( "[JJT] NET PICKUP - Making PROP_MP_ROCKET_01" )
						iBmbPlacementFlags = 0
						SET_BIT(iBmbPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_MAP))
						SET_BIT(iBmbPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
						SET_BIT(iBmbPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FACEPLAYER))
						
						pBmbPowerups[i] = CREATE_PICKUP_ROTATE (PICKUP_VEHICLE_CUSTOM_SCRIPT_LOW_GLOW, 
																vBmbPowerupPositions[i] + << 0,0,1.5 >>, 
																<<0.0,0.0,0>>, 
																iBmbPlacementFlags, 
																0, 
																DEFAULT,  
																DEFAULT, 
																PROP_MP_ROCKET_01 )	
						
						CLEAR_BIT( iBmbPowerupsLoadingBitSet, i )
					ENDIF
				BREAK
				CASE 3 //ciBMB_POWERUP_DROP_CHANCE_STINGER
					REQUEST_MODEL(PROP_MP_REPAIR)
					
					IF HAS_MODEL_LOADED(PROP_MP_REPAIR)
						
						PRINTLN( "[JJT] NET PICKUP - Making PROP_MP_ROCKET_01" )
						iBmbPlacementFlags = 0
						SET_BIT(iBmbPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_MAP))
						SET_BIT(iBmbPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
						SET_BIT(iBmbPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FACEPLAYER))
						
						pBmbPowerups[i] = CREATE_PICKUP_ROTATE (PICKUP_VEHICLE_CUSTOM_SCRIPT_LOW_GLOW, 
																vBmbPowerupPositions[i] + << 0,0,1.5 >>, 
																<<0.0,0.0,0>>, 
																iBmbPlacementFlags, 
																0, 
																DEFAULT,  
																DEFAULT, 
																PROP_MP_REPAIR )						
						CLEAR_BIT( iBmbPowerupsLoadingBitSet, i )
					ENDIF
					
				BREAK
			ENDSWITCH
		ENDIF
	ENDFOR
ENDPROC

PROC BMB_SETUP_BOMB(INT i)
	IF DOES_BLIP_EXIST( oBmbNetBombs[i].objectBlip )
		REMOVE_BLIP( oBmbNetBombs[i].objectBlip )
	ENDIF
	
	IF DOES_BLIP_EXIST( oBmbNetBombs[i].radiusBlip )
		REMOVE_BLIP( oBmbNetBombs[i].radiusBlip )
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
		oBmbNetBombs[i].iBombRadius += 2
		PRINTLN("BMB_SETUP_BOMB - Increased blip radius for Drop the Bomb: ", oBmbNetBombs[i].iBombRadius)
	ENDIF
				
	oBmbNetBombs[i].objectBlip = CREATE_BLIP_FOR_ENTITY( NET_TO_OBJ( oBmbNetBombs[i].objectID ), TRUE )
	oBmbNetBombs[i].radiusBlip = ADD_BLIP_FOR_RADIUS( GET_ENTITY_COORDS( NET_TO_OBJ( oBmbNetBombs[i].objectID ) ) , 
													  TO_FLOAT( oBmbNetBombs[i].iBombRadius ) * 6 )
	
	SET_BLIP_COLOUR( oBmbNetBombs[i].objectBlip, BLIP_COLOUR_RED )
	SET_BLIP_COLOUR( oBmbNetBombs[i].radiusBlip, BLIP_COLOUR_RED )
	SET_BLIP_ALPHA( oBmbNetBombs[i].radiusBlip, 120 )
	SET_BLIP_NAME_FROM_TEXT_FILE(oBmbNetBombs[i].objectBlip, "BLIP_PBOMB")
	
	SET_BLIP_PRIORITY( oBmbNetBombs[i].objectBlip, BLIPPRIORITY_OVER_CENTRE_BLIP )
	oBmbNetBombs[i].fBeepTimer = TO_FLOAT( g_FMMC_STRUCT.iBomberModeExplosionTimer )
	
	CLEAR_BIT(iActiveBombBlipsBS, i)
ENDPROC

PROC private_BMB_HANDLE_BOMB_BLIPS_AND_CLEANUP()
	
	INT i
	FOR i = 0 TO ciBMB_MAX_MAX_ACTIVE_BOMBS-1
		IF IS_BIT_SET(iActiveBombBlipsBS, i)
			IF NETWORK_DOES_NETWORK_ID_EXIST( oBmbNetBombs[i].objectID )
				BMB_SETUP_BOMB(i)
				PRINTLN( "[BMB][KH] private_BMB_HANDLE_BOMB_BLIPS_AND_CLEANUP - SETUP BOMB" )
			ELSE
				PRINTLN( "[BMB][KH] private_BMB_HANDLE_BOMB_BLIPS_AND_CLEANUP - Wanting to create blip but net id doesn't exist" )
			ENDIF
		ENDIF
		
		IF i < ciBMB_MAX_TOTAL_BOMBS_PER_PLAYER
			IF NETWORK_DOES_NETWORK_ID_EXIST( oBmbBombs[i].bombObject )
				IF IS_BIT_SET(iShouldCleanupBombBS, i)
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID( oBmbBombs[i].bombObject )
						PRINTLN( "[BMB][KH] private_BMB_HANDLE_BOMB_BLIPS_AND_CLEANUP - Cleaning up bombObject" )
						DELETE_NET_ID(oBmbBombs[i].bombObject)
						CLEAR_BIT(iShouldCleanupBombBS, i)
					ELSE
						PRINTLN( "[BMB][KH] private_BMB_HANDLE_BOMB_BLIPS_AND_CLEANUP - Requesting control of bombObject to clean it up" )
						NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(oBmbBombs[i].bombObject)
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(iShouldCleanupBombBS, i)
					PRINTLN( "[BMB][KH] private_BMB_HANDLE_BOMB_BLIPS_AND_CLEANUP - bombObject cleaned up" )
					CLEAR_BIT(iShouldCleanupBombBS, i)
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

//////////////////////////
//Public Bomber Mode Procs
//////////////////////////


PROC CLEAR_ALL_CARGO_BOMBS()
	INT i
	
	FOR i = 0 TO (ciMax_Number_Of_Visible_Bombs - 1)
		IF NETWORK_DOES_NETWORK_ID_EXIST(niMyCargoBombs[i])
			DELETE_NET_ID(niMyCargoBombs[i])
		ENDIF
	ENDFOR
ENDPROC

FUNC NETWORK_INDEX CREATE_CARGO_BOMB(INT iIndex, MODEL_NAMES mnBombModel)
	NETWORK_INDEX niTemp
	UNUSED_PARAMETER(iIndex)
	
	VECTOR vOffset, vVehPos
	vOffset.x = PICK_FLOAT((iIndex % 2 = 0), -0.860, 0.860)
	vOffset.x *= GET_RANDOM_FLOAT_IN_RANGE(0.65, 1.1)
	
	vOffset.y = -3.540 + (TO_FLOAT(iIndex) * 0.55)
	vOffset.z = 1.40
	vVehPos = GET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
	vVehPos.z += 10
	IF CREATE_NET_OBJ(niTemp, mnBombModel, vVehPos, FALSE)
		OBJECT_INDEX oiTemp = NET_TO_OBJ(niTemp)
		ATTACH_ENTITY_TO_ENTITY(oiTemp, GET_VEHICLE_PED_IS_IN(LocalPlayerPed), 0, vOffset, <<0.0,0.0, GET_RANDOM_FLOAT_IN_RANGE(-180.0, 180)>>, FALSE)
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
			SET_ENTITY_LOD_DIST(oiTemp, 500)
		ENDIF
		
		SET_ENTITY_PROOFS(oiTemp, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE)
		
		SET_MODEL_AS_NO_LONGER_NEEDED(mnBombModel)
		RETURN niTemp
	ELSE
		RETURN NULL
	ENDIF
ENDFUNC

PROC PROCESS_CARGO_BOMBS()

	INT i
	MODEL_NAMES mnBombModel = INT_TO_ENUM(MODEL_NAMES, HASH("imp_Prop_Bomb_Ball"))
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
		mnBombModel = INT_TO_ENUM(MODEL_NAMES, HASH("ba_Prop_Battle_Bomb_Small")) 
	ENDIF
	
	IF IS_PED_DEAD_OR_DYING(LocalPlayerPed)
	OR IS_PLAYER_RESPAWNING(LocalPlayer)
	OR manualRespawnState > eManualRespawnState_OKAY_TO_SPAWN
		CLEAR_ALL_CARGO_BOMBS()
		EXIT
	ENDIF
	
	FOR i = 0 TO (ciMax_Number_Of_Visible_Bombs - 1)
		IF i < MC_playerBD_1[iLocalPart].iBmbAvailableBombs //If this bomb should be shown...
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niMyCargoBombs[i]) //And it doesn't already exist...
				REQUEST_MODEL(mnBombModel)
				REQUEST_NAMED_PTFX_ASSET("scr_stunts")
							
				IF HAS_MODEL_LOADED(mnBombModel)
					niMyCargoBombs[i] = CREATE_CARGO_BOMB(i, mnBombModel) //Create the bomb for slot i!
					
					IF NETWORK_DOES_NETWORK_ID_EXIST(niMyCargoBombs[i])
						SET_OBJECT_TINT_INDEX(NET_TO_OBJ(niMyCargoBombs[i]), GET_TEAM_COLOUR_FOR_BOMB(MC_playerBD[iLocalPart].iteam))
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NETWORK_DOES_NETWORK_ID_EXIST(niMyCargoBombs[i]) //If it's not meant to be shown and it currently is...
				DELETE_NET_ID(niMyCargoBombs[i]) //Destroy it
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC GET_DTB_BOMB_COLOURS(HUD_COLOURS& dotArray[])
	INT iDot
	
	FOR iDot = 0 TO 7
		IF MC_playerBD_1[iPartToUse].iBmbAvailableBombs > iDot
			dotArray[iDot] = HUD_COLOUR_WHITE
		ELSE
			dotArray[iDot] = HUD_COLOUR_BLACK
		ENDIF
	ENDFOR
ENDPROC

/// PURPOSE: Increments the amount of bombs the player can place after picking up a powerup
PROC BMB_INCREMENT_AVAILABLE_BOMBS()
		
	IF MC_playerBD_1[iLocalPart].iBmbCurrentAmountOfBombs < (g_FMMC_STRUCT.iBomberModeTotalBombsPerPlayer)
	OR (IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType) AND MC_playerBD_1[iLocalPart].iBmbCurrentAmountOfBombs < ciBMB_MAX_TOTAL_BOMBS_PER_PLAYER)
		MC_playerBD_1[iLocalPart].iBmbCurrentAmountOfBombs++
		MC_playerBD_1[iLocalPart].iBmbAvailableBombs++
		PRINTLN( "[JJT][BMB] Player collected a bomb. Incrementing available bombs: ", MC_playerBD_1[iLocalPart].iBmbCurrentAmountOfBombs, ", ", MC_playerBD_1[iLocalPart].iBmbAvailableBombs )
	ELSE
		PRINTLN( "[JJT][BMB] Player already has max bombs and is requesting more." )
	ENDIF

ENDPROC

PROC BMB_INCREMENT_BOMB_SIZE()
	
	IF MC_playerBD_1[iLocalPart].iBmbCurrentRadius < g_FMMC_STRUCT.iBomberModeExplosionRadius
	OR (IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType) AND MC_playerBD_1[iLocalPart].iBmbCurrentRadius < ciDROPTHEBOMB_Max_Bomb_Length)
		MC_playerBD_1[iLocalPart].iBmbCurrentRadius++
		PRINTLN( "[JJT][BMB] Player collected bomb size increase powerup. iBmbCurrentRadius is now: ", MC_playerBD_1[iLocalPart].iBmbCurrentRadius)
	ELSE
		PRINTLN( "[JJT][BMB] Player already has max bomb size and is requesting it to increase." )
	ENDIF

ENDPROC

/// PURPOSE: Main Update loop for Bomber Mode
PROC PROCESS_BMB_UPDATE()
	
	#IF IS_DEBUG_BUILD
	IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
		TEXT_LABEL_63 tlDebugTextLeft
		tlDebugTextLeft += "C_Bombs: "
		tlDebugTextLeft += MC_playerBD_1[iLocalPart].iBmbCurrentAmountOfBombs
		tlDebugTextLeft += " || Radius: "
		tlDebugTextLeft += MC_playerBD_1[iLocalPart].iBmbCurrentRadius
		tlDebugTextLeft += " || A_Bombs: "
		tlDebugTextLeft += MC_playerBD_1[iLocalPart].iBmbAvailableBombs
		DRAW_DEBUG_TEXT_2D(tlDebugTextLeft, <<0.25, 0.5, 0.5>>)
		
		HIDE_RADAR_THIS_FRAME()
	ENDIF
	#ENDIF

	//Handles hiding the bomb-specific HUD elements
	BOOL bHideBombHud = (IS_BIT_SET(iLocalBoolCheck28, LBOOL28_MANUALLY_HIDDEN_HUD) AND bUsingBirdsEyeCam) OR IS_PLAYER_RESPAWNING(LocalPlayer)
	BOOL bIsDropTheBomb = IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
	
	IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
		bHideBombHud = FALSE
	ENDIF

	IF GET_MC_CLIENT_GAME_STATE( iLocalPart ) != GAME_STATE_END
		IF GET_MC_CLIENT_GAME_STATE( iLocalPart ) = GAME_STATE_RUNNING
		AND !g_bMissionEnding
		
			private_PROCESS_NET_BMB_ARMING_SOUND_AND_LIGHT()
			private_BMB_LOADING_PICKUP()
			private_BMB_HANDLE_BOMB_BLIPS_AND_CLEANUP()
			
			#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD3)
					IF MC_playerBD_1[iLocalPart].iBmbAvailableBombs < ciBMB_MAX_TOTAL_BOMBS_PER_PLAYER - 1
						BMB_INCREMENT_AVAILABLE_BOMBS()
						BMB_INCREMENT_BOMB_SIZE()
					ENDIF
				ENDIF
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD1)
					IF MC_playerBD_1[iLocalPart].iBmbAvailableBombs > 0
						MC_playerBD_1[iLocalPart].iBmbAvailableBombs--
						MC_playerBD_1[iLocalPart].iBmbCurrentAmountOfBombs--
						MC_playerBD[iLocalPart].iQuantityOfBombs--
					ENDIF
					
					MC_playerBD_1[iLocalPart].iBmbCurrentRadius--
				ENDIF
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD2)
					CLEAR_ALL_CARGO_BOMBS()
				ENDIF
			ENDIF
			#ENDIF
			
			INT i
			FOR i = 0 TO MC_playerBD_1[iLocalPart].iBmbCurrentAmountOfBombs - 1
				IF IS_BIT_SET( iBmbPlacedBitSet, BMB_SLOT_1 + i )
					PRINTLN("[BMB] PROCESS_BMB_UPDATE - Active bomb found: ", i)
					IF IS_BIT_SET( iBmbPlacedBitSet, BMB_SLOT_1_ARMING + i )
						PRINTLN("[BMB] PROCESS_BMB_UPDATE - Bomb ", i , " is arming")
						private_PROCESS_BMB_BOMB_CREATION_AND_ARMING( i )
					ELSE
						PRINTLN("[BMB] PROCESS_BMB_UPDATE - Bomb ", i , " is armed, doing staggered explosion")
						private_PROCESS_BMB_STAGGERED_EXPLOSION( i )
					ENDIF
				ENDIF
			ENDFOR
			
			IF NOT bHideBombHud
				IF bIsDropTheBomb
					DRAW_GENERIC_METER(MC_playerBD_1[iPartToUse].iBmbCurrentRadius, ciDROPTHEBOMB_Max_Bomb_Length, "DTB_BLAST", DEFAULT, DEFAULT, HUDORDER_THIRDBOTTOM)
				ELSE
					IF MC_playerBD_1[iLocalPart].iBmbCurrentRadius > 2
					AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciOPTION_VEHICLE_WEAPON_ENABLED)
						//DRAW_BIG_SINGLE_SCORE_HUD( iBmbCurrentRadius,  "BMB_UI_CR" )
						
						
							DRAW_GENERIC_SCORE( MC_playerBD_1[iLocalPart].iBmbCurrentRadius - 2, 
												"BLAST", 
												DEFAULT, 
												DEFAULT,  
												HUDORDER_NINETHBOTTOM, 
												TRUE )
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT bHideBombHud
				IF bIsDropTheBomb
				AND MC_playerBD_1[iPartToUse].iBmbAvailableBombs <= 8
					HUD_COLOURS hcColours[8]
					GET_DTB_BOMB_COLOURS(hcColours)
					DRAW_ONE_PACKAGES_EIGHT_HUD(MC_playerBD_1[iPartToUse].iBmbCurrentAmountOfBombs, "DTB_BOMBS", FALSE, hcColours[0], hcColours[1], hcColours[2], hcColours[3], hcColours[4], hcColours[5], hcColours[6], hcColours[7])
				ELSE
					IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA(g_FMMC_STRUCT.iAdversaryModeType)
					AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciOPTION_VEHICLE_WEAPON_ENABLED)
						DRAW_BIG_SINGLE_SCORE_HUD(MC_playerBD_1[iLocalPart].iBmbAvailableBombs, "FMMC_VWP8")
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_PED_IN_ANY_VEHICLE( LocalPlayerPed )
			AND bLocalPlayerPedOK
				//BOMB PLACEMENT
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciOPTION_VEHICLE_WEAPON_ENABLED)
				AND NOT bIsDropTheBomb
					IF IS_BIT_SET(MC_playerBD[iLocalPart].iVehicleWeaponEffectActiveBS, ciVEH_WEP_BOMB)
						IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_LS) 
							OR (IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_VEH_HORN) 
							AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType))
						OR (IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType) 
							AND IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_VEH_AIM))
						OR (IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType) 
							AND IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_AIM)))
						AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciOPTION_VEHICLE_WEAPON_ENABLED)
							IF MC_playerBD[iLocalPart].iQuantityOfBombs > 0
								PRINTLN("[BMB] PROCESS_BMB_UPDATE - Dropping a bomb - button pressed")
								IF NOT HAS_NET_TIMER_STARTED(tdMultiBombDelay)
								OR HAS_NET_TIMER_STARTED(tdMultiBombDelay) AND HAS_NET_TIMER_EXPIRED(tdMultiBombDelay, ciBOMB_DELAY_TIME)
									FOR i = 0 TO MC_playerBD_1[iLocalPart].iBmbCurrentAmountOfBombs - 1
										IF NOT IS_BIT_SET( iBmbPlacedBitSet, BMB_SLOT_1 + i )
											PRINTLN("[BMB] PROCESS_BMB_UPDATE - Free bomb slot found: ", i)
											SET_BIT( 	iBmbPlacedBitSet, BMB_SLOT_1 			+ i )
											SET_BIT( 	iBmbPlacedBitSet, BMB_SLOT_1_ARMING 	+ i )
											CLEAR_BIT( 	iBmbPlacedBitSet, BMB_SLOT_1_HIT_TESTED + i )
											SET_BIT(MC_playerBD[iLocalPart].iVehicleWeaponEffectUsedBS, ciVEH_WEP_BOMB)
											MC_playerBD[iLocalPart].iQuantityOfBombs --
											REINIT_NET_TIMER(tdMultiBombDelay)
											
											IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TINY_RACERS_SOUNDS)
												BROADCAST_FMMC_VEHICLE_WEAPON_SOUND(MC_PlayerBD[ilocalPart].iteam, ci_BOMB_SOUND_RELEASE, GET_ENTITY_COORDS(localPlayerPed))
											ENDIF
											
											EXIT
										ENDIF
									ENDFOR
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, GET_BOMBDROP_BUTTON()) 
						FOR i = 0 TO MC_playerBD_1[iLocalPart].iBmbCurrentAmountOfBombs - 1
							IF NOT IS_BIT_SET( iBmbPlacedBitSet, BMB_SLOT_1 + i )
								PRINTLN( "[JJT][BMB] PLACING A BOMB IN SLOT: ", i )
								
								SET_BIT( 	iBmbPlacedBitSet, BMB_SLOT_1 			+ i )
								SET_BIT( 	iBmbPlacedBitSet, BMB_SLOT_1_ARMING 	+ i )
								CLEAR_BIT( 	iBmbPlacedBitSet, BMB_SLOT_1_HIT_TESTED + i )
								MC_playerBD_1[iLocalPart].iBmbAvailableBombs--
								EXIT
							ENDIF
						ENDFOR
					ENDIF
				ENDIF
				
				//ARMOUR REMOVAL
				IF IS_BIT_SET( iBmbPlayerBitSet, BMB_PLAYER_HAS_ARMOUR )
				AND NOT bHideBombHud
					
					DRAW_GENERIC_SCORE( 0, 
										"ARMOR", 
										DEFAULT, 
										DEFAULT,  
										HUDORDER_TENTHBOTTOM, 
										TRUE, 
										DEFAULT, 
										DEFAULT, 
										DEFAULT, 
										DEFAULT, 
										DEFAULT, 
										DEFAULT, 
										DEFAULT, 
										DEFAULT, 
										DEFAULT, 
										ACTIVITY_POWERUP_TICK )					
					
					IF IS_BIT_SET( iBmbPlayerBitSet, BMB_STARTED_ARMOUR_REMOVE_TIMER )
						iBmbArmourRemoveTimer++
						
						IF iBmbArmourRemoveTimer > 60
							PRINT_TICKER("BMB_PU_REMARM") 
							
							SET_ENTITY_PROOFS( GET_VEHICLE_PED_IS_IN( LocalPlayerPed ), 
									   FALSE, FALSE, FALSE, FALSE, FALSE )
							
							CLEAR_BIT( iBmbPlayerBitSet, BMB_STARTED_ARMOUR_REMOVE_TIMER )
							CLEAR_BIT( iBmbPlayerBitSet, BMB_PLAYER_HAS_ARMOUR )
						ENDIF
					ELSE
						IF IS_EXPLOSION_IN_SPHERE( EXP_TAG_GRENADE, GET_ENTITY_COORDS( LocalPlayerPed ), 6.0 )
							SET_BIT( iBmbPlayerBitSet, BMB_STARTED_ARMOUR_REMOVE_TIMER )
							iBmbArmourRemoveTimer = 0
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELIF g_bMissionEnding OR IS_PLAYER_SPECTATING( LocalPlayer )
			
			MC_playerBD_1[iLocalPart].iBmbAvailableBombs = 0
			
			//Here we deal with clearing up of player bombs and clearing the net bomb references
			IF NOT IS_BIT_SET(iBmbPlayerBitSet, BMB_STARTED_DEATH_BOMB_FADE)
				INT i
				FOR i = 0 TO MC_playerBD_1[iLocalPart].iBmbCurrentAmountOfBombs - 1
					
					IF IS_BIT_SET(iBmbPlacedBitSet, BMB_SLOT_1 + i)
					
						INT y
						FOR y = 0 TO ciBMB_MAX_MAX_ACTIVE_BOMBS-1
							
							IF oBmbNetBombs[i].objectID != NULL
								IF oBmbNetBombs[i].objectID = oBmbBombs[i].bombObject
									PRINTLN("[BMB] - PROCESS_BMB_UPDATE - oBmbNetBombs[",i,"].objectID = oBmbBombs[",i,"].bombObject so setting obejctID to NULL")
									oBmbNetBombs[i].objectID = NULL
								ENDIF
							ENDIF
						ENDFOR
						
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(oBmbBombs[i].bombObject)
							IF DOES_ENTITY_EXIST(NET_TO_OBJ(oBmbBombs[i].bombObject))
								SET_ENTITY_ALPHA(NET_TO_OBJ(oBmbBombs[i].bombObject), 0, TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
				
				SET_BIT( iBmbPlayerBitSet, BMB_STARTED_DEATH_BOMB_FADE )
			ELSE
				IF NOT IS_BIT_SET( iBmbPlayerBitSet, BMB_STARTED_DEATH_BOMB_DELETE )
					INT i
					BOOL bNoBombsLeft = TRUE
					FOR i = 0 TO MC_playerBD_1[iLocalPart].iBmbCurrentAmountOfBombs - 1
						IF IS_BIT_SET( iBmbPlacedBitSet, BMB_SLOT_1 + i )
							bNoBombsLeft = FALSE
						
							IF NETWORK_DOES_NETWORK_ID_EXIST( oBmbBombs[i].bombObject )
								IF NETWORK_HAS_CONTROL_OF_ENTITY( NET_TO_OBJ( oBmbBombs[i].bombObject ) )
									PRINTLN( "[BMB] - PROCESS_BMB_UPDATE - has control deleting")
									DELETE_NET_ID( oBmbBombs[i].bombObject )
									CLEAR_BIT( iBmbPlacedBitSet, BMB_SLOT_1 + i )
								ELSE
									IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
										NETWORK_REQUEST_CONTROL_OF_ENTITY( NET_TO_OBJ( oBmbBombs[i].bombObject ) )
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
					
					IF bNoBombsLeft
						SET_BIT( iBmbPlayerBitSet, BMB_STARTED_DEATH_BOMB_DELETE )
					ENDIF
				ELSE
					private_PROCESS_NET_BMB_ARMING_SOUND_AND_LIGHT()			
				ENDIF
			ENDIF

			private_BMB_LOADING_PICKUP()
		ENDIF
	ENDIF
ENDPROC

PROC BMB_APPLY_CAR_ARMOUR()
	IF NOT IS_BIT_SET( iBmbPlayerBitSet, BMB_PLAYER_HAS_ARMOUR )
		IF bLocalPlayerPedOK
			IF IS_PED_IN_ANY_VEHICLE( LocalPlayerPed )
				IF IS_VEHICLE_DRIVEABLE( GET_VEHICLE_PED_IS_IN( LocalPlayerPed ) )
				
					SET_ENTITY_PROOFS( GET_VEHICLE_PED_IS_IN( LocalPlayerPed ), 
									   FALSE, TRUE, TRUE, FALSE, FALSE )
					
					SET_BIT( iBmbPlayerBitSet, BMB_PLAYER_HAS_ARMOUR )
					CLEAR_BIT( iBmbPlayerBitSet, BMB_STARTED_ARMOUR_REMOVE_TIMER )
					iBmbArmourRemoveTimer = 0
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC BMB_SET_UP_ARMING_BOMB( NETWORK_INDEX placedBomb, INT radius, BOOL bLocalPlayer )
	
	PRINTLN( "[BMB] NET ARMING - BMB_SET_UP_ARMING_BOMB LocalPlayer: ", bLocalPlayer)
	INT iFirstBombSpot = 0
	INT i
	IF bLocalPlayer
		iFirstBombSpot = ciBMB_MAX_TOTAL_BOMBS_PER_PLAYER
	ENDIF
	FOR i = iFirstBombSpot TO ciBMB_MAX_MAX_ACTIVE_BOMBS-1
		IF oBmbNetBombs[i].objectID = NULL
			PRINTLN( "[BMB] NET ARMING - BMB_SET_UP_ARMING_BOMB Bomb index: ", i)
			oBmbNetBombs[i].objectID = placedBomb
			oBmbNetBombs[i].iBombRadius = radius
			REINIT_NET_TIMER( oBmbNetBombs[i].bombTimer )
			IF NETWORK_DOES_NETWORK_ID_EXIST( placedBomb )
				BMB_SETUP_BOMB(i)
			ELSE
				PRINTLN( "[BMB] NET ARMING - BMB_SET_UP_ARMING_BOMB NET ID DOESN'T EXIST")
				//Adding this to the active bombs list as when the event is received the net id does not always exist...
				SET_BIT(iActiveBombBlipsBS, i)
			ENDIF
			BREAKLOOP
		ENDIF
	ENDFOR
ENDPROC

PROC BMB_DESTROY_DESTRUCTIBLE_PROP( INT creatorPropIdx )

	PRINTLN( "[JJT][BMB] BMB_DESTROY_DESTRUCTIBLE_PROP" )
											
	IF creatorPropIdx < g_FMMC_STRUCT_ENTITIES.iNumberOfProps
	AND creatorPropIdx < GET_FMMC_MAX_NUM_PROPS()
		IF DOES_ENTITY_EXIST( oiProps[creatorPropIdx] )
			PRINTLN( "[JJT][BMB] Deleting prop number: ", creatorPropIdx )
			DELETE_OBJECT( oiProps[creatorPropIdx] )
		ENDIF
	ENDIF

ENDPROC

FUNC INT private_BMB_FIND_FREE_PICKUP_SLOT()
	INT i = 0
	FOR i = 0 TO ciBMB_MAX_POWERUPS-1
		IF pBmbPowerups[i] = NULL 
			RETURN i
		ENDIF
	ENDFOR
	RETURN -1
ENDFUNC

PROC private_BMB_SETUP_PICKUP_LOADING( INT type, VECTOR pos )
	
	INT i = private_BMB_FIND_FREE_PICKUP_SLOT()
	
	IF i < 0 OR i >= ciBMB_MAX_POWERUPS
		PRINTLN( "[JJT] NET PICKUP - Could not find a free pickup slot." )
		EXIT
	ENDIF
	
	//Set the slot to currently loading
	PRINTLN( "[JJT][BMB] NET PICKUP - private_BMB_SETUP_PICKUP_LOADING: ", i )
	SET_BIT( iBmbPowerupsLoadingBitSet, i )
	iBmbPowerupTypeToLoad[i] = type
	vBmbPowerupPositions[i] = pos
ENDPROC


PROC BMB_CREATE_PICKUP( INT chance1, INT chance2, VECTOR pos, BMB_POWERUP_TYPES type )

	PRINTLN( "[JJT][BMB] NET PICKUP - BMB_CREATE_PICKUP: ", chance1, ", ", chance2, ". PosX: ", pos.x, ", PosY: ", pos.y )
	
	SWITCH( type )
		CASE BPT_BIGGER_BOMB
			PRINTLN( "[JJT][BMB] NET PICKUP - BPT_BIGGER_BOMB" )
			private_BMB_SETUP_PICKUP_LOADING(1, pos)
		BREAK
		CASE BPT_EXTRA_BOMB
			PRINTLN( "[JJT][BMB] NET PICKUP - BPT_EXTRA_BOMB" )
			private_BMB_SETUP_PICKUP_LOADING(2, pos)
		BREAK
		CASE BPT_ARMOUR
			PRINTLN( "[JJT][BMB] NET PICKUP - BPT_ARMOUR" )
			private_BMB_SETUP_PICKUP_LOADING(3, pos)
		BREAK
		CASE BPT_RANDOM
			//If we get the chance to try for a powerup
			IF chance1 <= ciBMB_POWERUP_DROP_CHANCE
				PRINTLN( "[JJT][BMB] NET PICKUP - chance1 <= ciBMB_POWERUP_DROP_CHANCE" )
				//We don't do anything
				IF chance2 >= 0 
				AND chance2 <= ciBMB_POWERUP_DROP_CHANCE_SIZE
					PRINTLN( "[JJT][BMB] NET PICKUP - ciBMB_POWERUP_DROP_CHANCE_SIZE" )
					private_BMB_SETUP_PICKUP_LOADING(1, pos)
				//Else if we drop an additional bomb
				ELIF chance2 > ciBMB_POWERUP_DROP_CHANCE_SIZE
				AND chance2 <= ciBMB_POWERUP_DROP_CHANCE_SIZE + ciBMB_POWERUP_DROP_CHANCE_AMOUNT
					PRINTLN( "[JJT][BMB] NET PICKUP - ciBMB_POWERUP_DROP_CHANCE_AMOUNT" )
					private_BMB_SETUP_PICKUP_LOADING(2, pos)
				//Else if we drop armour
				ELIF chance2 > ciBMB_POWERUP_DROP_CHANCE_SIZE + ciBMB_POWERUP_DROP_CHANCE_AMOUNT
				AND chance2 <= ciBMB_POWERUP_DROP_CHANCE_SIZE + ciBMB_POWERUP_DROP_CHANCE_AMOUNT + ciBMB_POWERUP_DROP_CHANCE_ARMOUR
					PRINTLN( "[JJT][BMB] NET PICKUP - ciBMB_POWERUP_DROP_CHANCE_STINGER" )
					private_BMB_SETUP_PICKUP_LOADING(3, pos)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC
///=====================================
///    
///    Bomber Mission Logic END
///    Author: Jake Thorne
///    
///=====================================

PROC BOOST_AT_START_OF_RULE_CHANGE()
	INT iTeam = MC_PlayerBD[iLocalPart].iTeam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	
	FLOAT fSpeed
	
	VEHICLE_INDEX viLocalPlayerBike
	
	IF DOES_ENTITY_EXIST(LocalPlayerPed)
		IF NOT IS_PED_DEAD_OR_DYING(LocalPlayerPed)
			IF IS_PED_ON_ANY_BIKE(LocalPlayerPed)
				viLocalPlayerBike = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
			ENDIF
		ENDIF
	ENDIF
	
	IF iRule < FMMC_MAX_RULES
		IF DOES_ENTITY_EXIST(viLocalPlayerBike)
		AND IS_VEHICLE_DRIVEABLE(viLocalPlayerBike)
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_BOOST_AT_START_OF_OBJ )
			AND NOT IS_PARTICIPANT_A_SPECTATOR(iLocalPart)
			AND (NOT HAS_NET_TIMER_STARTED(g_FMMC_STRUCT.stBoostTimer)
				OR HAS_NET_TIMER_EXPIRED(g_FMMC_STRUCT.stBoostTimer, 1000))
				// The Rule has changed - enable the boost if below the threshold
				IF IS_BIT_SET(iLocalBoolCheck6, (LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam))
					
					PRINTLN("[TOUR BOOST][BOOST_AT_START_OF_RULE_CHANGE] - New rule shold have a boost, iRule: ", iRule)
					
					fSpeed = GET_ENTITY_SPEED(viLocalPlayerBike)
					IF fSpeed < TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iBoostThreshold[iRule])
						PRINTLN("[TOUR BOOST][BOOST_AT_START_OF_RULE_CHANGE] - Player is going slow enough to enable a boost, ",fSpeed,", iBoostThreshold = ",g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iBoostThreshold[iRule])
						PRINT_HELP("TDF_BOOST")
						REINIT_NET_TIMER(tdTourDeForceBoost)
						SET_BIT(iLocalBoolCheck18, LBOOL18_SPEED_BOOST_AVAILABLE)
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("[TOUR BOOST][BOOST_AT_START_OF_RULE_CHANGE] - Player is going too slow to boost, fSpeed = ",fSpeed,", iBoostThreshold = ",g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iBoostThreshold[iRule])
					#ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF HAS_NET_TIMER_STARTED(tdTourDeForceBoost)
				IF IS_BIT_SET(iLocalBoolCheck18, LBOOL18_SPEED_BOOST_AVAILABLE)
					//Once the boost is available await input
					IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdTourDeForceBoost) < 3000 // Give players a few seconds to hit the boost
						IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
						AND IS_VEHICLE_ON_ALL_WHEELS(viLocalPlayerBike)
							PRINTLN("[TOUR BOOST][BOOST_AT_START_OF_RULE_CHANGE] Player accelerated in time, it's time to get boosting!")
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TDF_BOOST")
								CLEAR_HELP()
							ENDIF
							REINIT_NET_TIMER(tdTourDeForceBoost)
							ANIMPOSTFX_PLAY("RaceTurbo", 0, FALSE)
							SET_BIT(iLocalBoolCheck18, LBOOL18_SPEED_BOOST_SUCCESS)
							CLEAR_BIT(iLocalBoolCheck18, LBOOL18_SPEED_BOOST_AVAILABLE)
						ENDIF
					ELSE
						PRINTLN("[TOUR BOOST][BOOST_AT_START_OF_RULE_CHANGE] Took too long to press accelerate, reset!")
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TDF_BOOST")
							CLEAR_HELP()
						ENDIF
						RESET_NET_TIMER(tdTourDeForceBoost)
						CLEAR_BIT(iLocalBoolCheck18, LBOOL18_SPEED_BOOST_SUCCESS)
						CLEAR_BIT(iLocalBoolCheck18, LBOOL18_SPEED_BOOST_AVAILABLE)
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(iLocalBoolCheck18, LBOOL18_SPEED_BOOST_SUCCESS)
					//Do the boost
					IF IS_VEHICLE_ON_ALL_WHEELS(viLocalPlayerBike)
					AND (GET_ENTITY_SPEED(viLocalPlayerBike) < GET_VEHICLE_MODEL_ESTIMATED_MAX_SPEED(GET_ENTITY_MODEL(viLocalPlayerBike)))
						fSpeed = GET_ENTITY_SPEED(viLocalPlayerBike)
						PRINTLN("[TOUR BOOST][BOOST_AT_START_OF_RULE_CHANGE] applying boost! extra speed of ",(0.015 * fLastFrameTime)," (fLastFrameTime = ",fLastFrameTime,") on top of current speed ",fSpeed)
						fSpeed = fSpeed + (60 * fLastFrameTime) // Approximately +2 per frame (assuming 30 fps)
						IF IS_VEHICLE_ON_ALL_WHEELS(viLocalPlayerBike)
							SET_VEHICLE_FORWARD_SPEED(viLocalPlayerBike,fSpeed)
						ENDIF
					ENDIF
					
					IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdTourDeForceBoost) >= 250
						PRINTLN("[TOUR BOOST][BOOST_AT_START_OF_RULE_CHANGE] timer has finished, stop boosting")
						CLEAR_BIT(iLocalBoolCheck18, LBOOL18_SPEED_BOOST_SUCCESS)
						CLEAR_BIT(iLocalBoolCheck18, LBOOL18_TDF_PLAY_BOOST_SOUND)
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
		
	ENDIF
	
ENDPROC
///PURPOSE:
///    Adding particles for night rules
PROC TLAD_ADD_NIGHT_PARTICLES(INT iTeam)
	SET_PARTICLE_FX_FOOT_OVERRIDE_NAME("scr_adversary_foot_flames")
	IF iTeam = 0
		IF NOT IS_BIT_SET(iLocalBoolCheck19, LBOOL19_TLAD_DEVIL_FLAME_FEET_ACTIVE)
			SET_PED_CONFIG_FLAG(LocalPlayerPed,PCF_UseOverrideFootstepPtFx, TRUE)
			SET_BIT(iLocalBoolCheck19, LBOOL19_TLAD_DEVIL_FLAME_FEET_ACTIVE)
			PRINTLN("[JT PFX] Started Flaming feet")
		ENDIF
		IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(TLAD_DevilLight[PARTICIPANT_ID_TO_INT()])
			STRING ptfxName = "scr_adversary_ped_light_bad"
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciCONDEMNED_MODE_ENABLED)
				ptfxName = "scr_sm_con_ped_light"
				USE_PARTICLE_FX_ASSET("scr_sm")
			ELSE
				USE_PARTICLE_FX_ASSET("scr_bike_adversary")
			ENDIF
			TLAD_DevilLight[PARTICIPANT_ID_TO_INT()] = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY_BONE(ptfxName,localPlayerPed,<<0,0,0>>,<<0,0,0>>,GET_PED_BONE_INDEX(localPlayerPed, BONETAG_ROOT))
			PRINTLN("[JT PFX] Started TLAD_DevilLight")
		ENDIF
	ENDIF
ENDPROC
///PURPOSE:
///    Removing particles added during the night rules
PROC TLAD_REMOVE_NIGHT_PARTICLES()
	IF DOES_PARTICLE_FX_LOOPED_EXIST(TLAD_DevilLight[PARTICIPANT_ID_TO_INT()])
		STOP_PARTICLE_FX_LOOPED(TLAD_DevilLight[PARTICIPANT_ID_TO_INT()])
		PRINTLN("[JT PFX] Stopping TLAD_DevilLight")
	ENDIF
	IF IS_BIT_SET(iLocalBoolCheck19, LBOOL19_TLAD_DEVIL_FLAME_FEET_ACTIVE)
		SET_PED_CONFIG_FLAG(LocalPlayerPed,PCF_UseOverrideFootstepPtFx, FALSE)
		CLEAR_BIT(iLocalBoolCheck19, LBOOL19_TLAD_DEVIL_FLAME_FEET_ACTIVE)
		PRINTLN("[JT PFX] Stopping Flaming feet")
	ENDIF
ENDPROC
///PURPOSE:
///    Adding particles for day rules
PROC TLAD_ADD_DAY_PARTICLES()
	IF MC_playerBD[iPartToUse].iteam = 1
	AND NOT IS_LOCAL_PLAYER_SPECTATOR()
		IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(TLADAngelGlow[PARTICIPANT_ID_TO_INT()])
			USE_PARTICLE_FX_ASSET("scr_bike_adversary")
			TLADAngelGlow[PARTICIPANT_ID_TO_INT()] = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY_BONE("scr_adversary_ped_glow",localPlayerPed,<<0,0,0>>,<<0,0,0>>,GET_PED_BONE_INDEX(localPlayerPed, BONETAG_ROOT))
			PRINTLN("[JT PFX] Started TLADAngelGlow")
		ENDIF
	ENDIF
	
//	IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(TLAD_AngelLight[PARTICIPANT_ID_TO_INT()])
//		USE_PARTICLE_FX_ASSET("scr_bike_adversary")
//		TLAD_AngelLight[PARTICIPANT_ID_TO_INT()] = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY_BONE("scr_adversary_ped_light_good",localPlayerPed,<<0,0,0>>,<<0,0,0>>,GET_PED_BONE_INDEX(localPlayerPed, BONETAG_ROOT))
//		PRINTLN("[JT PFX] Started TLAD_AngelLight")
//	ENDIF
	IF MC_playerBD[iTLADCurrentPlayer].iteam = 1
		PRINTLN("[JT PFX] Correct team, player: ", iTLADCurrentPlayer)
		PRINTLN("[JT PFX] team: ", MC_playerBD[iTLADCurrentPlayer].iteam)
		IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(TLAD_AngelLight[iTLADCurrentPlayer])
		AND NOT IS_PARTICIPANT_A_SPECTATOR(iTLADCurrentPlayer)
			PRINTLN("[JT PFX] Particle doesn't already exist, player: ", iTLADCurrentPlayer)
			USE_PARTICLE_FX_ASSET("scr_bike_adversary")
			
			IF DOES_ENTITY_EXIST(GET_PLAYER_PED(INT_TO_PLAYERINDEX(iTLADCurrentPlayer)))
				TLAD_AngelLight[iTLADCurrentPlayer] = START_PARTICLE_FX_LOOPED_ON_ENTITY_BONE("scr_adversary_ped_light_good",
														GET_PLAYER_PED(INT_TO_PLAYERINDEX(iTLADCurrentPlayer)),
														<<0,0,0>>,<<0,0,0>>,
														GET_PED_BONE_INDEX(GET_PLAYER_PED(INT_TO_PLAYERINDEX(iTLADCurrentPlayer)), BONETAG_ROOT))
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[JT PFX] GET_PLAYER_PED(INT_TO_PLAYERINDEX(iTLADCurrentPlayer)) does not exist!")
			#ENDIF
			ENDIF
			
			IF DOES_PARTICLE_FX_LOOPED_EXIST(TLAD_AngelLight[iTLADCurrentPlayer])
				PRINTLN("[JT PFX Complete] Started TLAD_AngelLight on player ", iTLADCurrentPlayer)
			ENDIF
		ENDIF
	ENDIF
ENDPROC
///PURPOSE:
///    Removing particles added during the day rules
PROC TLAD_REMOVE_DAY_PARTICLES(BOOL bIsLocalOnly = FALSE)
	
//	IF DOES_PARTICLE_FX_LOOPED_EXIST(TLADAngelGlow[PARTICIPANT_ID_TO_INT()])
//		STOP_PARTICLE_FX_LOOPED(TLADAngelGlow[PARTICIPANT_ID_TO_INT()]) 
//		PRINTLN("[JT PFX] Stopping TLADAngelGlow")
//	ENDIF

//	IF DOES_PARTICLE_FX_LOOPED_EXIST(TLAD_AngelLight[PARTICIPANT_ID_TO_INT()])
//		REMOVE_PARTICLE_FX(TLAD_AngelLight[PARTICIPANT_ID_TO_INT()]) //Using remove here to stop the light from persisting through into night
//		PRINTLN("[JT PFX] Stopping TLADAngelLight")
//	ENDIF
	
	IF NOT bIsLocalOnly
		IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iSpecificTODHour[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] = 0
			IF DOES_PARTICLE_FX_LOOPED_EXIST(TLADAngelGlow[PARTICIPANT_ID_TO_INT()])
				STOP_PARTICLE_FX_LOOPED(TLADAngelGlow[PARTICIPANT_ID_TO_INT()]) 
				PRINTLN("[JT PFX] Stopping TLADAngelGlow, local only")
			ENDIF
		ENDIF
		IF MC_playerBD[iTLADCurrentPlayer].iteam = 1
			IF DOES_PARTICLE_FX_LOOPED_EXIST(TLAD_AngelLight[iTLADCurrentPlayer])
				REMOVE_PARTICLE_FX(TLAD_AngelLight[iTLADCurrentPlayer]) //Using remove here to stop the light from persisting through into night
				PRINTLN("[JT PFX] Stopping TLADAngelLight on player: ", iTLADCurrentPlayer)
			ENDIF
		ENDIF
	ELSE
		IF DOES_PARTICLE_FX_LOOPED_EXIST(TLADAngelGlow[PARTICIPANT_ID_TO_INT()])
			STOP_PARTICLE_FX_LOOPED(TLADAngelGlow[PARTICIPANT_ID_TO_INT()]) 
			PRINTLN("[JT PFX] Stopping TLADAngelGlow, local only")
		ENDIF
		IF DOES_PARTICLE_FX_LOOPED_EXIST(TLAD_AngelLight[PARTICIPANT_ID_TO_INT()])
			REMOVE_PARTICLE_FX(TLAD_AngelLight[PARTICIPANT_ID_TO_INT()]) //Using remove here to stop the light from persisting through into night
			PRINTLN("[JT PFX] Stopping TLADAngelLight, local only")
		ENDIF
	ENDIF
	
ENDPROC

///PURPOSE:
///    Logic for the Lost and Damned particles
PROC MANAGE_TLAD_PARTICLES(INT iTeam, INT iRule)
	
	iTLADCurrentPlayer++
	IF iTLADCurrentPlayer >= NETWORK_GET_MAX_NUM_PARTICIPANTS()
		iTLADCurrentPlayer = 0
		PRINTLN("[JT CP] ", iTLADCurrentPlayer)
	ENDIF
		
	IF NOT IS_LOCAL_PLAYER_SPECTATOR()
		//Staggered loop for checking if player has particle
		IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH)
			IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSpecificTODHour[iRule] = 0
				IF NOT IS_PED_INJURED(LocalPlayerPed)
					TLAD_REMOVE_DAY_PARTICLES()
					TLAD_ADD_NIGHT_PARTICLES(iTeam)
				ELSE
					TLAD_REMOVE_NIGHT_PARTICLES()
				ENDIF
			ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSpecificTODHour[iRule] = 12
				IF NOT IS_PED_INJURED(LocalPlayerPed)
					TLAD_REMOVE_NIGHT_PARTICLES()
					TLAD_ADD_DAY_PARTICLES()
				ELSE
					TLAD_REMOVE_DAY_PARTICLES(TRUE)
				ENDIF
				IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(iTLADCurrentPlayer),FALSE)
					IF IS_PED_INJURED(GET_PLAYER_PED(INT_TO_PLAYERINDEX(iTLADCurrentPlayer)))
						TLAD_REMOVE_DAY_PARTICLES()
					ENDIF
				ENDIF
			ENDIF
		ELSE 
			PRINTLN("[JT PFX] IN SD")
			IF NOT IS_PED_INJURED(localPlayerPed)
				TLAD_ADD_NIGHT_PARTICLES(iTeam)
				TLAD_ADD_DAY_PARTICLES()
			ELSE
				TLAD_REMOVE_NIGHT_PARTICLES()
				TLAD_REMOVE_DAY_PARTICLES()
			ENDIF	
		ENDIF
	ELSE
		//Handling particles for spectators
		IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH)
			IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSpecificTODHour[iRule] = 0
				SET_PARTICLE_FX_FOOT_OVERRIDE_NAME("scr_adversary_foot_flames")
				TLAD_REMOVE_DAY_PARTICLES()
			ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSpecificTODHour[iRule] = 12
				TLAD_ADD_DAY_PARTICLES()
				//Removing lights from players when they are injured
				IF IS_PED_INJURED(GET_PLAYER_PED(INT_TO_PLAYERINDEX(iTLADCurrentPlayer)))
					TLAD_REMOVE_DAY_PARTICLES()
				ENDIF
			ENDIF
		ELSE
			TLAD_ADD_DAY_PARTICLES()
		ENDIF
	ENDIF
ENDPROC

///PURPOSE:
///    Activates the transition anim post fx for Lost and Damned
PROC ACTIVATE_TLAD_ANIM_POST_FX(INT iTeam, INT iRule)
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSpecificTODHour[iRule] = 0
		PRINTLN("[JT ANIMPOST] NIGHT Time is 0")
		IF NOT ANIMPOSTFX_IS_RUNNING("LostTimeNight") AND iRule != 0
			ANIMPOSTFX_PLAY("LostTimeNight", iNightAnimFXDura, FALSE)									
			PRINTLN("[JT ANIMPOST] NIGHT Started playing animFX for 2seconds")
		ENDIF
	ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSpecificTODHour[iRule] = 12
	PRINTLN("[JT ANIMPOST] DAY time is 12")
		IF NOT ANIMPOSTFX_IS_RUNNING("LostTimeDay") AND iRule != 0
			ANIMPOSTFX_PLAY("LostTimeDay", iDayAnimFXDura, FALSE)
			PRINTLN("[JT ANIMPOST] DAY Active")
		ENDIF
	ENDIF
ENDPROC


///PURPOSE:
///    Contains logic specific to Lost and Damned
PROC MANAGE_LOST_AND_DAMNED(INT iTeam, INT iRule)
	
	IF IS_BIT_SET(iLocalBoolCheck6, (LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam))
		//Starting timers for the lost and damned scaleform icons
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSpecificTODHour[iRule] = 0 //Night Time
			iCurrentTimeCycle = 1
			
			IF NOT IS_LOCAL_PLAYER_SPECTATOR()
				IF NOT HAS_NET_TIMER_STARTED(MC_serverBD_3.stTLADNight)
					START_NET_TIMER(MC_serverBD_3.stTLADNight)
				ELSE
					RESET_NET_TIMER(MC_serverBD_3.stTLADNight)
					START_NET_TIMER(MC_serverBD_3.stTLADNight)
				ENDIF
			ENDIF	
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSpecificTODHour[iRule] = 12 //Day Time
			iCurrentTimeCycle = 0
			
			IF NOT IS_LOCAL_PLAYER_SPECTATOR()
				IF NOT HAS_NET_TIMER_STARTED(MC_serverBD_3.stTLADDay)
					START_NET_TIMER(MC_serverBD_3.stTLADDay)
				ELSE
					RESET_NET_TIMER(MC_serverBD_3.stTLADDay)
					START_NET_TIMER(MC_serverBD_3.stTLADDay)
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[TLAD HUD] iTargetHour is not valid")
		ENDIF
		
		//Setting wetness, fix for Angels being too bright during the day.
		INT i
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSpecificTODHour[iRule] = 12
			FOR i = 0 TO NETWORK_GET_NUM_PARTICIPANTS() - 1
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
					IF IS_NET_PLAYER_OK(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i)), TRUE)
						IF MC_playerBD[i].iTeam = 1
							SET_PED_WETNESS_HEIGHT(GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))), 1.0)
							PRINTLN("[JT WET] This player is wet.")
						ENDIF
					ENDIF
				ENDIF
			ENDFOR	
		ENDIF
		
		//Setting the current timecycle based on the time set on this rule
		CLEAR_BIT(iLocalBoolCheck18, LBOOL18_TLAD_LIGHTNING)
		
		IF IS_BIT_SET(iLocalBoolCheck19, LBOOL19_TLAD_BLOCK_WEAPON_PFX)
			CLEAR_BIT(iLocalBoolCheck19, LBOOL19_TLAD_BLOCK_WEAPON_PFX)
		ENDIF
		
	ENDIF
	
	//Disabling cellphone camera if we are in Lost and damned
	DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
	
	//Particles
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciTLAD_USE_PARTICLE_EFFECTS)
	AND (GET_TIME_REMAINING_ON_MULTIRULE_TIMER() > 1000 OR IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH))
		MANAGE_TLAD_PARTICLES(iTeam, iRule)
	ENDIF
	
ENDPROC

PROC SET_START_TIME_BASED_ON_RULE()
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		
	IF iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_ENABLE_IS_START_TIME)
			iTargetHour = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSpecificTODStartHour[iRule]
			IF iRuleHour != iTargetHour
				iRuleHour = iTargetHour
			ENDIF
			IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSpecificTODStartHour[iRule] = 0
				iCurrentTimeCycle = 1
			ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSpecificTODStartHour[iRule] = 12
				iCurrentTimeCycle = 0
			ENDIF
			
			NETWORK_OVERRIDE_CLOCK_TIME(iRuleHour,  0, 0)
			PRINTLN("[JT][TIME] Starting time is: ", iRuleHour)
			
		ENDIF
	ENDIF
ENDPROC

///PURPOSE:
///    Adjusts the time to creator specified values
///    Created for Lost and Damned, has some mode specific functionality
PROC SET_TIME_BASED_ON_RULE()
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	INT iSpeed = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSpecificTODSpeed[iRule]
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_LOST_DAMNED(g_FMMC_STRUCT.iAdversaryModeType)
		MANAGE_LOST_AND_DAMNED(iTeam, iRule)
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck6, (LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam))
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciTLAD_USE_TOD_ANIM_POST_FX)
		AND GET_TIME_REMAINING_ON_MULTIRULE_TIMER() > 1000
			ACTIVATE_TLAD_ANIM_POST_FX(iTeam, iRule)
		ENDIF
	ENDIF
	
	IF iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_ENABLE_SPECIFIC_TOD)
		AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH)
			IF iRuleHour != iTargetHour
				SWITCH iSpeed
					CASE 0
						IF iRuleMinute != 59
							iRuleMinute++
						ELIF iRuleMinute >= 59
							IF iRuleHour >= 23 AND iRuleMinute >= 59
								iRuleHour = 0
								iRuleMinute = 0
							ELSE
								iRuleHour++
								iRuleMinute = 0
							ENDIF
						ENDIF	
					BREAK
					CASE 1
						IF iRuleMinute != 57
							iRuleMinute+=3
						ELIF iRuleMinute >= 57
							IF iRuleHour >= 23 AND iRuleMinute >= 57
								iRuleHour = 0
								iRuleMinute = 0
							ELSE
								iRuleHour++
								iRuleMinute = 0
							ENDIF
						ENDIF
					BREAK
					CASE 2
						IF iRuleMinute != 55
							iRuleMinute+=5
						ELIF iRuleMinute >= 55
							IF iRuleHour >= 23 AND iRuleMinute >= 55
								iRuleHour = 0
								iRuleMinute = 0
							ELSE
								iRuleHour++
								iRuleMinute = 0
							ENDIF
						ENDIF
					BREAK
					CASE 3
						IF iRuleMinute != 56
							iRuleMinute+=8
						ELIF iRuleMinute >= 56
							IF iRuleHour >= 23 AND iRuleMinute >= 56
								iRuleHour = 0
								iRuleMinute = 0
							ELSE
								iRuleHour++
								iRuleMinute = 0
							ENDIF
						ENDIF
					BREAK
					CASE 4
						IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_LOST_DAMNED(g_FMMC_STRUCT.iAdversaryModeType)
							RESET_ADAPTATION(15)
							IF ANIMPOSTFX_IS_RUNNING("LostTimeNight") OR ANIMPOSTFX_IS_RUNNING("LostTimeDay")
								IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSpecificTODHour[iRule] = 0
									FORCE_LIGHTNING_FLASH()
								ENDIF
								iRuleHour = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSpecificTODHour[iRule]
								iRuleMinute = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSpecificTODMinute[iRule]
							ENDIF
						ELSE
							RESET_ADAPTATION(10)
							iRuleHour = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSpecificTODHour[iRule]
							iRuleMinute = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSpecificTODMinute[iRule]
						ENDIF
						
					BREAK
				ENDSWITCH
			ELIF iRuleHour = iTargetHour
			AND (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_ENABLE_CHANGE_TOD_ON_RULE)
			OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_ALLOW_TOD_CHANGE))
					
				iTargetHour = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSpecificTODHour[iRule]
				PRINTLN("[JT][TLADHUD] Current time Cycle: ", iCurrentTimeCycle)
				PRINTLN("[JT TIMECYCLE] Setting the target hour to: ", iTargetHour)
				
				// Used for Flashing the Score
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_LOST_DAMNED(g_FMMC_STRUCT.iAdversaryModeType)
					IF iTargetHour = 0
					AND NOT IS_BIT_SET(iLocalBoolCheck, LBOOL19_LOST_DAMNED_TIME_SET_TO_NIGHT)
						RESET_NET_TIMER(tdLostDamnedFlashScoreTimer)
						START_NET_TIMER(tdLostDamnedFlashScoreTimer)
						
						SET_BIT(iLocalBoolCheck, LBOOL19_LOST_DAMNED_TIME_SET_TO_NIGHT)
						
						PRINTLN("[LM] Time check: Setting the bit to Night LBOOL19_LOST_DAMNED_TIME_SET_TO_NIGHT and resetting timer")
					ELIF iTargetHour = 12
					AND IS_BIT_SET(iLocalBoolCheck, LBOOL19_LOST_DAMNED_TIME_SET_TO_NIGHT)
						RESET_NET_TIMER(tdLostDamnedFlashScoreTimer)
						START_NET_TIMER(tdLostDamnedFlashScoreTimer)
						
						CLEAR_BIT(iLocalBoolCheck, LBOOL19_LOST_DAMNED_TIME_SET_TO_NIGHT)
						
						PRINTLN("[LM] Time check: Clearing the bit to Day LBOOL19_LOST_DAMNED_TIME_SET_TO_NIGHT and resetting timer")
					ENDIF
				ENDIF
							
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciENABLE_LOST_AND_DAMNED_SOUNDS)
				AND	GET_TIME_REMAINING_ON_MULTIRULE_TIMER() > 1000 //To stop sounds from playing when the round is shorter then 10 minutes
					//Day/night transition sounds
					IF iTargetHour != iPreviousTargetHour
						IF iTargetHour = 0
							PRINTLN("[KH][TIME] Playing transition to night sound...")
							PLAY_SOUND_FRONTEND(-1, "NightFall_Stinger", "DLC_Biker_LostAndDamned_Sounds")
						ELIF iTargetHour = 12
							PRINTLN("[KH][TIME] Playing transition to day sound...")
							PLAY_SOUND_FRONTEND(-1, "DayBreak_Stinger", "DLC_Biker_LostAndDamned_Sounds")
						ENDIF
					ENDIF
				ENDIF
				
				iPreviousTargetHour = iTargetHour
				
			ELIF iTargetHour = -1
				iTargetHour = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSpecificTODHour[iRule]
				iPreviousTargetHour = iTargetHour
			ENDIF
			
			NETWORK_OVERRIDE_CLOCK_TIME(iRuleHour,  iRuleMinute, 0)
			
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_LOST_DAMNED(g_FMMC_STRUCT.iAdversaryModeType)
				IF ANIMPOSTFX_IS_RUNNING("LostTimeNight")
					IF ANIMPOSTFX_GET_CURRENT_TIME("LostTimeNight") <= 0.5 AND NOT IS_BIT_SET(iLocalBoolCheck18, LBOOL18_TLAD_LIGHTNING)
						PRINTLN("[JT ANIMPOST] NIGHT half way point has been reached")
						FORCE_LIGHTNING_FLASH()
						SET_BIT(iLocalBoolCheck18, LBOOL18_TLAD_LIGHTNING)
					ENDIF
				ENDIF
			ENDIF
			
			PRINTLN("[JT][TIME] Current time for team ", iTeam, " is: ", iRuleHour, ":", iRuleMinute," target hour is: ", iTargetHour)
		ELIF IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH)
		AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciENABLE_SUDDEN_DEATH_TIME_OF_DAY_CHANGE)
			IF iRuleHour != iSuddenDeathHour
				IF IS_BIT_SET(iLocalBoolCheck18, LBOOL18_SD_TIMECYCLE_SET)
					CLEAR_BIT(iLocalBoolCheck18, LBOOL18_SD_TIMECYCLE_SET)
				ENDIF

				PRINTLN("[JT ANIMPOST] NIGHT Time is 0")
				IF NOT ANIMPOSTFX_IS_RUNNING("LostTimeDay")
					ANIMPOSTFX_PLAY("LostTimeDay", iDayAnimFXDura, FALSE)									
					iRuleHour = iSuddenDeathHour
					PRINTLN("[JT ANIMPOST] JUDGEMENT DAY Started playing animFX for 2 seconds")
				ENDIF
			ENDIF
			//Clearing Scaleform Icon
			IF NOT IS_BIT_SET(iLocalBoolCheck18, LBOOL18_TLAD_SD_UI_CLEARED)
				SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(SF_TLAD_MovieIndex)
				SET_BIT(iLocalBoolCheck18, LBOOL18_TLAD_SD_UI_CLEARED)
			ENDIF
			
			IF iRuleHour = iSuddenDeathHour
				//Timecycle
				IF NOT IS_BIT_SET(iLocalBoolCheck18, LBOOL18_SD_TIMECYCLE_SET)
					CLEAR_TIMECYCLE_MODIFIER()
					SET_TIMECYCLE_MODIFIER("mp_lad_judgment")
					
					SET_BIT(iLocalBoolCheck18, LBOOL18_SD_TIMECYCLE_SET)
					PRINTLN("[JT TIMECYCLE] SUDDEN DEATH TIMECYCLE ACTIVE!")
				ENDIF
				
				//Particles
				IF NOT IS_PED_INJURED(localPlayerPed)
					IF NOT IS_BIT_SET(iLocalBoolCheck18, LBOOL18_TLAD_JUDGEMENT_DAY_PTFX_ACTIVE)
						USE_PARTICLE_FX_ASSET("scr_bike_adversary")
						START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_adversary_judgement_ash", localPlayerPed, <<0,0,0>>, <<0,0,0>>)
						USE_PARTICLE_FX_ASSET("scr_bike_adversary")
						TLADptfxLensDirt = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_adversary_judgement_lens_dirt", localPlayerPed, <<0,0,0>>, <<0,0,0>>)
						SET_PARTICLE_FX_LOOPED_EVOLUTION(TLADptfxLensDirt,"level", 1.0, TRUE)
						PRINTLN("[JT PFX] PARTICLES ON SHOW")
						SET_BIT(iLocalBoolCheck18, LBOOL18_TLAD_JUDGEMENT_DAY_PTFX_ACTIVE)
					ENDIF
				ELSE
					CLEAR_BIT(iLocalBoolCheck18, LBOOL18_TLAD_JUDGEMENT_DAY_PTFX_ACTIVE)
				ENDIF
			ENDIF
			
			NETWORK_OVERRIDE_CLOCK_TIME(iRuleHour,  0, 0)
			
			PRINTLN("[JT][TIME] SUDDEN DEATH Current time for team ", iTeam, " is: ", iRuleHour, " target hour is: ", iTargetHour)
		ENDIF
	ENDIF
ENDPROC

PROC CHANGE_PROP_COLOUR(INT iPropNumber, INT iTeam)
	INT iColour, iColourSwitch
	iColourSwitch = g_FMMC_STRUCT.iTeamColourOverride[iTeam]
	//If we want to check for team colour overrides
	SWITCH iColourSwitch
		CASE -1
			//If left on default it will set the colour based on team colour
			SWITCH iTeam
				CASE 0
					iColour = 8 //Orange
				BREAK
				CASE 1
					iColour = 3 //Purple
				BREAK
				CASE 2
					iColour = 10 //Pink
				BREAK
				CASE 3
					iColour = 9	//Green
				BREAK
			ENDSWITCH
		BREAK
		CASE 0
		CASE 15
		CASE 16
		CASE 17
			iColour = 8 //Oranges
			PRINTLN("[JT PROP] set to: Orange") 
		BREAK
		CASE 1
		CASE 18
		CASE 19
		CASE 20
			iColour = 9 //Greens
			PRINTLN("[JT PROP] set to: Green") 
		BREAK
		CASE 2
			iColour = 10 //Pinks
			PRINTLN("[JT PROP] set to: Pink") 
		BREAK
		CASE 3
		CASE 5
			iColour = 6 //Greys
			PRINTLN("[JT PROP] set to: Grey") 
		BREAK
		CASE 4
		CASE 21
		CASE 22
		CASE 23
			iColour = 3 //Purples
			PRINTLN("[JT PROP] set to: Purple") 
		BREAK
		CASE 6
		CASE 7
		CASE 8
			iColour = 1 //Reds
			PRINTLN("[JT PROP] set to: ") 
		BREAK
		CASE 9
		CASE 10
		CASE 11
			iColour = 2 //Blues
			PRINTLN("[JT PROP] set to: Blue") 
		BREAK
		CASE 12
		CASE 13
		CASE 14
			iColour = 7 //Yellows
			PRINTLN("[JT PROP] set to: Yellow") 
		BREAK
		DEFAULT
			SWITCH iTeam
				CASE 0
					iColour = 8
				BREAK
				CASE 1
					iColour = 3
				BREAK
				CASE 2
					iColour = 10
				BREAK
				CASE 3
					iColour = 9
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	PRINTLN("[JT PROP] We are in, iColourSwitch = ", iColourSwitch, " iColour = ", iColour)
	SET_OBJECT_TINT_INDEX(oiProps[iPropNumber], iColour)
	PRINTLN("[JT PROP] Set the new tint")
ENDPROC

/////////////////////////////////////////////////
///    Biker Formations				//////////
///    Adam Westwood			//////////
////////////////////////////////////
///    
///
///    
///    

PROC OVERRIDE_SLIPSTREAM_VFX()

	IF HAS_NAMED_PTFX_ASSET_LOADED("scr_bike_adversary")
		IF NOT IS_BIT_SET(iLocalBoolCheck19, LBOOL19_LOADED_TOUR_DE_FORCE_VFX_SLIPSTREAM_OVERRIDE)
			SET_BIT(iLocalBoolCheck19, LBOOL19_LOADED_TOUR_DE_FORCE_VFX_SLIPSTREAM_OVERRIDE)
			SET_PARTICLE_FX_OVERRIDE("veh_slipstream", "scr_adversary_slipstream")
			PRINTLN("OVERRIDE_SLIPSTREAM_VFX - overridden slipstream vfx")
		ENDIF
	ENDIF
ENDPROC

PROC RESET_SLIPSTREAM_VFX()
	IF IS_BIT_SET(iLocalBoolCheck19, LBOOL19_LOADED_TOUR_DE_FORCE_VFX_SLIPSTREAM_OVERRIDE)
		RESET_PARTICLE_FX_OVERRIDE("veh_slipstream")
		CLEAR_BIT(iLocalBoolCheck19, LBOOL19_LOADED_TOUR_DE_FORCE_VFX_SLIPSTREAM_OVERRIDE)
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_BIKER_LEADER(PLAYER_INDEX PlayerIndex)
	
	IF DOES_ENTITY_EXIST(PlayerIndex)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC MANAGE_BIKER_FORMATIONS_MENU(PLAYER_INDEX PlayerIndex)
	IF IS_PLAYER_BIKER_LEADER(PlayerIndex)
		
	ENDIF
ENDPROC



FUNC BOOL IS_PLAYER_IN_BIKER_FORMATION(PLAYER_INDEX PlayerIndex)
	IF DOES_ENTITY_EXIST(PlayerIndex)
	
	ENDIF
	RETURN FALSE
ENDFUNC

PROC DRAW_BIKER_FORMATION_MARKER()
	
	VECTOR vWorld
	FLOAT fMarkerScale = 4.5
	INT iR, iG, iB, iA, iDivisor 

	IF IS_PLAYER_IN_BIKER_FORMATION(PLAYER_ID())  
		iDivisor = 8 
	ELSE 
		iDivisor = 2 
	ENDIF 

	GET_HUD_COLOUR(GET_PLAYER_HUD_COLOUR(PLAYER_ID()), iR, iG, iB, iA) 
	GET_GROUND_Z_FOR_3D_COORD(vWorld, vWorld.z) 
	vWorld.z += 0.15 
	DRAW_MARKER(MARKER_RING, vWorld, <<0,0,1>>, <<0,0,0>>, <<fMarkerScale, fMarkerScale, fMarkerScale>>, iR, iG, iB, iA/iDivisor, FALSE, FALSE)
                     
ENDPROC


PROC APPLY_FORMATION_BONUSES()

ENDPROC

PROC REMOVE_FORMATION_BONUSES()

ENDPROC

PROC MANAGE_FORMATION_BONUSES()

ENDPROC

PROC MANAGE_BIKER_FORMATIONS()

ENDPROC


/////////////////////////////////////////////////
///    PRON							//////////
///    Lorcan Henry				//////////
////////////////////////////////////

PROC GET_PRON_VEHICLE_COLOURS(INT iPlayer, INT &iColour1, INT &iColour2, INT &iColour4)
	
	SWITCH iPlayer
		CASE 0
			//Orange
			iColour1 = 124
			iColour2 = 41
			iColour4 = 124
		BREAK
		CASE 1
			//Purple
			iColour1 = 145
			iColour2 = 81
			iColour4 = 148
		BREAK
		CASE 2
			//Pink
			iColour1 = 137
			iColour2 = 136
			iColour4 = 136
		BREAK
		CASE 3
			//Green
			iColour1 = 53
			iColour2 = 55
			iColour4 = 55
		BREAK
	ENDSWITCH
	
ENDPROC

PROC SETUP_PRON_PLAYER_COLOUR(INT iPlayer)

	SWITCH iPlayer
		CASE 0
			//Orange
			iTrailR = 253
			iTrailG = 132
			iTrailB = 10
		BREAK
		CASE 1
			//Purple
			iTrailR = 123
			iTrailG = 64
			iTrailB = 225
		BREAK
		CASE 2
			//Pink
			iTrailR = 252
			iTrailG = 143
			iTrailB = 230
		BREAK
		CASE 3
			//Green
			iTrailR = 110
			iTrailG = 213
			iTrailB = 31
		BREAK
	ENDSWITCH
	
	GET_PRON_VEHICLE_COLOURS(iPlayer, iBike1, iBike2, iBike4)
			
ENDPROC

FUNC BOOL SHOULD_WE_USE_PRON_BIKE_COLOURS(MODEL_NAMES mnVeh)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciPRON_MODE_ENABLED)
		IF mnVeh = SHOTARO
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC SETUP_PRON_PLAYER(INT iPlayer)
	
	PRINTLN("[[LH]][PRON] - SETUP_PRON_PLAYER")
	
	LGHTCYCLE_PLAYER[iPlayer].vLastPoint = LGHTCYCLE_PLAYER[iPlayer].vCurrentPos
	
	IF iNumberOfPronTeams < iPlayer
		iNumberOfPronTeams = iPlayer
	ENDIF
	
	PRINTLN("[PRON][PTFX]SETUP_PRON_PLAYER Colour - iTrailR ",iTrailR)
	PRINTLN("[PRON][PTFX]SETUP_PRON_PLAYER Colour - iTrailG ",iTrailG)
	PRINTLN("[PRON][PTFX]SETUP_PRON_PLAYER Colour - iTrailB ",iTrailB)
	
	SET_ENTITY_COORDS(oiTrailBottom, GET_ENTITY_BONE_POSTION(LGHTCYCLE_PLAYER[iPlayer].LightBike, GET_ENTITY_BONE_INDEX_BY_NAME(LGHTCYCLE_PLAYER[iPlayer].LightBike,"WHEEL_LR")))
	SET_ENTITY_ROTATION(oiTrailBottom, GET_ENTITY_ROTATION(LGHTCYCLE_PLAYER[iPlayer].LightBike))
	VECTOR vTop = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(LGHTCYCLE_PLAYER[iPlayer].LightBike,<<0.0, fLineDistanceOffset, fLineHeight>>)
	VECTOR vBottom = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oiTrailBottom, <<0.0,-fWheelOffset,-fWheelRadius>>)
	
	LGHTCYCLE_PLAYER[iPlayer].LGHTCYCLE_SEGMENT[0].vPointTop = vTop
	LGHTCYCLE_PLAYER[iPlayer].LGHTCYCLE_SEGMENT[0].vPointBottom = vBottom
	
	LGHTCYCLE_PLAYER[iPlayer].vEndTop = vTop
	LGHTCYCLE_PLAYER[iPlayer].vEndBottom = vBottom
	
	LGHTCYCLE_PLAYER[iPlayer].LGHTCYCLE_SEGMENT[149].vPointTop = vTop
	LGHTCYCLE_PLAYER[iPlayer].LGHTCYCLE_SEGMENT[149].vPointBottom = vBottom
	
	LGHTCYCLE_PLAYER[iPlayer].CollisionBox[LGHTCYCLE_PLAYER[iPlayer].iCurrentCollisionBox].vLowest = LGHTCYCLE_PLAYER[iPlayer].vCurrentPos - <<MAX_SEGMENT_LENGTH,MAX_SEGMENT_LENGTH,MAX_SEGMENT_LENGTH>>
	LGHTCYCLE_PLAYER[iPlayer].CollisionBox[LGHTCYCLE_PLAYER[iPlayer].iCurrentCollisionBox].vHighest = LGHTCYCLE_PLAYER[iPlayer].vCurrentPos + <<MAX_SEGMENT_LENGTH,MAX_SEGMENT_LENGTH,MAX_SEGMENT_LENGTH>>
	
ENDPROC

PROC CREATE_LATEST_PRON_LINE(INT iPlayer)

	PRINTLN("[[LH]][PRON] - CREATE_LATEST_PRON_LINE, LGHTCYCLE_PLAYER[",iPlayer,"]iFrontOfLine = ", LGHTCYCLE_PLAYER[iPlayer].iFrontOfLine)

	//Calculate the length, midpoint and rotation of the currently active line
	LGHTCYCLE_PLAYER[iPlayer].LGHTCYCLE_SEGMENT[LGHTCYCLE_PLAYER[iPlayer].iFrontOfLine].vPointTop = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(LGHTCYCLE_PLAYER[iPlayer].LightBike,<<0.0, fLineDistanceOffset, fLineHeight>>)
	
	SET_ENTITY_COORDS(oiTrailBottom, GET_ENTITY_BONE_POSTION(LGHTCYCLE_PLAYER[iPlayer].LightBike, GET_ENTITY_BONE_INDEX_BY_NAME(LGHTCYCLE_PLAYER[iPlayer].LightBike,"WHEEL_LR")))
	SET_ENTITY_ROTATION(oiTrailBottom, GET_ENTITY_ROTATION(LGHTCYCLE_PLAYER[iPlayer].LightBike))
	LGHTCYCLE_PLAYER[iPlayer].LGHTCYCLE_SEGMENT[LGHTCYCLE_PLAYER[iPlayer].iFrontOfLine].vPointBottom = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oiTrailBottom, <<0.0,-fWheelOffset,-fWheelRadius>>)

	LGHTCYCLE_PLAYER[iPlayer].vLiveTrailOffset = LGHTCYCLE_PLAYER[iPlayer].LGHTCYCLE_SEGMENT[LGHTCYCLE_PLAYER[iPlayer].iFrontOfLine].vPointBottom
	
ENDPROC

FUNC INT GET_NEXT_POINT_IN_LINE(INT iPlayer, INT iSegment, INT iSteps = 0, BOOL bIgnoreLimit = FALSE)

	INT iMaxSegments
	IF bIgnoreLimit
		iMaxSegments = MAX_SEGMENTS
	ELSE
		iMaxSegments = LGHTCYCLE_PLAYER[iPlayer].iCurrentNumberOfSegments
	ENDIF

	INT i
	
	FOR i = 0 TO iSteps
		iSegment = iSegment+1
		
		IF iSegment = LGHTCYCLE_PLAYER[iPlayer].iFrontOfLine
			BREAKLOOP
		ENDIF
		
		IF iSegment > iMaxSegments
		OR iSegment > LGHTCYCLE_PLAYER[iPlayer].iCurrentNumberOfSegments
			iSegment = 0
		ENDIF
	ENDFOR

	RETURN iSegment

ENDFUNC

FUNC INT GET_NEXT_POINT_BACK_IN_LINE(INT iPlayer, INT iSegment, INT iSteps = 0)

	INT i
	
	FOR i = 0 TO iSteps
		iSegment = iSegment-1
		IF iSegment = LGHTCYCLE_PLAYER[iPlayer].iFrontOfLine
			iSegment = iSegment+1
			BREAKLOOP
		ENDIF
		IF iSegment <= -1
			iSegment = LGHTCYCLE_PLAYER[iPlayer].iCurrentNumberOfSegments
		ENDIF
	ENDFOR

	RETURN iSegment

ENDFUNC

PROC INTERP_BACK_OF_PRON_TRAIL(INT iPlayer, FLOAT fDist)

	VECTOR v1,v2,v3,v4
	FLOAT fDivDist = fDist/MAX_SEGMENT_LENGTH
	
	IF fDivDist < 1
		PRINTLN("[[LH]][PRON] - fDivDist ", fDivDist)
		
		INT iBackPoint = LGHTCYCLE_PLAYER[iPlayer].iFrontOfLine+1
		IF iBackPoint >= iSegmentsLimit
			iBackPoint = 0
		ENDIF
		INT iNextPoint = iBackPoint+1
		IF iNextPoint >= iSegmentsLimit
			iNextPoint = 0
		ENDIF
		
		v1 = LGHTCYCLE_PLAYER[iPlayer].vEndTop
		v2 = LGHTCYCLE_PLAYER[iPlayer].LGHTCYCLE_SEGMENT[iNextPoint].vPointTop
		v3 = LGHTCYCLE_PLAYER[iPlayer].vEndBottom
		v4 = LGHTCYCLE_PLAYER[iPlayer].LGHTCYCLE_SEGMENT[iNextPoint].vPointBottom
		
		LGHTCYCLE_PLAYER[iPlayer].LGHTCYCLE_SEGMENT[iBackPoint].vPointTop = INTERP_VECTOR(v1, v2, fDivDist)
		LGHTCYCLE_PLAYER[iPlayer].LGHTCYCLE_SEGMENT[iBackPoint].vPointBottom = INTERP_VECTOR(v3, v4, fDivDist)
	ENDIF
	
ENDPROC

FUNC INT GET_BACK_OF_LINE(INT iPlayer)

	IF LGHTCYCLE_PLAYER[iPlayer].iCurrentNumberOfSegments >= iSegmentsLimit
		INT iReturn = LGHTCYCLE_PLAYER[iPlayer].iFrontOfLine+1
		IF iReturn >= iSegmentsLimit
			iReturn = 0
		ENDIF
		RETURN iReturn
	ENDIF

	RETURN 0

ENDFUNC

PROC CLEAR_COLLISION_BOX(LINE_AABB &CollisionBox)

	CollisionBox.iEndSegment = 0
	CollisionBox.iStartSegment = 0
	CollisionBox.vLowest = <<0.0, 0.0, 0.0>>
	CollisionBox.vHighest = <<0.0, 0.0, 0.0>>
	
ENDPROC

PROC ADJUST_COLLISION_BOX(INT iPlayer)

	INT iCurrentBox = LGHTCYCLE_PLAYER[iPlayer].iCurrentCollisionBox
	
	PRINTLN("[LH][PRON][BOX!] Checking box ",iCurrentBox," segment ", LGHTCYCLE_PLAYER[iPlayer].iSegmentCounter, " Front - ", LGHTCYCLE_PLAYER[iPlayer].iFrontOfLine)
	
	PRINTLN("[LH][PRON][BOX!] LGHTCYCLE_PLAYER[",iPlayer,"].CollisionBox[", iCurrentBox, "].iStartSegment = ", LGHTCYCLE_PLAYER[iPlayer].CollisionBox[iCurrentBox].iStartSegment)
	FLOAT fExtraLine = MAX_SEGMENT_LENGTH*2
		
	//Get the furthest/nearest X
	LGHTCYCLE_PLAYER[iPlayer].CollisionBox[iCurrentBox].vLowest.x = FMIN(LGHTCYCLE_PLAYER[iPlayer].vLastPoint.x - fExtraLine, LGHTCYCLE_PLAYER[iPlayer].CollisionBox[iCurrentBox].vLowest.x)
	LGHTCYCLE_PLAYER[iPlayer].CollisionBox[iCurrentBox].vHighest.x = FMAX(LGHTCYCLE_PLAYER[iPlayer].vLastPoint.x + fExtraLine, LGHTCYCLE_PLAYER[iPlayer].CollisionBox[iCurrentBox].vHighest.x)
	
	//Get the furthest/nearest Y
	LGHTCYCLE_PLAYER[iPlayer].CollisionBox[iCurrentBox].vLowest.y = FMIN(LGHTCYCLE_PLAYER[iPlayer].vLastPoint.y - fExtraLine, LGHTCYCLE_PLAYER[iPlayer].CollisionBox[iCurrentBox].vLowest.y)
	LGHTCYCLE_PLAYER[iPlayer].CollisionBox[iCurrentBox].vHighest.y = FMAX(LGHTCYCLE_PLAYER[iPlayer].vLastPoint.y + fExtraLine, LGHTCYCLE_PLAYER[iPlayer].CollisionBox[iCurrentBox].vHighest.y)
	
	//Get the furthest/nearest Z
	LGHTCYCLE_PLAYER[iPlayer].CollisionBox[iCurrentBox].vLowest.z = FMIN(LGHTCYCLE_PLAYER[iPlayer].vLastPoint.z - fExtraLine, LGHTCYCLE_PLAYER[iPlayer].CollisionBox[iCurrentBox].vLowest.z)
	LGHTCYCLE_PLAYER[iPlayer].CollisionBox[iCurrentBox].vHighest.z = FMAX(LGHTCYCLE_PLAYER[iPlayer].vLastPoint.z + fExtraLine, LGHTCYCLE_PLAYER[iPlayer].CollisionBox[iCurrentBox].vHighest.z)
	
	LGHTCYCLE_PLAYER[iPlayer].CollisionBox[iCurrentBox].iEndSegment = LGHTCYCLE_PLAYER[iPlayer].iFrontOfLine
	
	//Check the segments and create a new box if needed
	LGHTCYCLE_PLAYER[iPlayer].iSegmentCounter++
	IF LGHTCYCLE_PLAYER[iPlayer].iSegmentCounter = MAX_SEGMENTS_PER_BOX
		
		LGHTCYCLE_PLAYER[iPlayer].iSegmentCounter = 0
		
		LGHTCYCLE_PLAYER[iPlayer].iCurrentCollisionBox++
		IF LGHTCYCLE_PLAYER[iPlayer].iCurrentCollisionBox = NUM_COLLISION_BOXES
			LGHTCYCLE_PLAYER[iPlayer].iCurrentCollisionBox = 0
			LGHTCYCLE_PLAYER[iPlayer].bLoopActive = TRUE
		ENDIF
		iCurrentBox = LGHTCYCLE_PLAYER[iPlayer].iCurrentCollisionBox
		
		IF LGHTCYCLE_PLAYER[iPlayer].bLoopActive
			//Copy the old box into the temp box to make sure it's segments are still checked.
			LGHTCYCLE_PLAYER[iPlayer].EndCollisionBox = LGHTCYCLE_PLAYER[iPlayer].CollisionBox[iCurrentBox]
			LGHTCYCLE_PLAYER[iPlayer].EndCollisionBox = LGHTCYCLE_PLAYER[iPlayer].CollisionBox[iCurrentBox]
			CLEAR_COLLISION_BOX(LGHTCYCLE_PLAYER[iPlayer].CollisionBox[iCurrentBox])
		ENDIF
		
		LGHTCYCLE_PLAYER[iPlayer].CollisionBox[iCurrentBox].vLowest = LGHTCYCLE_PLAYER[iPlayer].vCurrentPos - <<MAX_SEGMENT_LENGTH,MAX_SEGMENT_LENGTH,MAX_SEGMENT_LENGTH>>
		LGHTCYCLE_PLAYER[iPlayer].CollisionBox[iCurrentBox].vHighest = LGHTCYCLE_PLAYER[iPlayer].vCurrentPos + <<MAX_SEGMENT_LENGTH,MAX_SEGMENT_LENGTH,MAX_SEGMENT_LENGTH>>
		
		LGHTCYCLE_PLAYER[iPlayer].CollisionBox[iCurrentBox].iStartSegment = GET_NEXT_POINT_IN_LINE(iPlayer, LGHTCYCLE_PLAYER[iPlayer].iFrontOfLine, 0, TRUE)
		
		PRINTLN("[LH][PRON][BOX] LGHTCYCLE_PLAYER[iPlayer].CollisionBox[", iCurrentBox, "].iStartSegment = ", LGHTCYCLE_PLAYER[iPlayer].CollisionBox[iCurrentBox].iStartSegment)
		
	ENDIF

ENDPROC

PROC DROP_PRON_LINE_POINT(INT iPlayer)

	PRINTLN("[[LH]][PRON] - DROP_PRON_LINE_POINT")
	LGHTCYCLE_PLAYER[iPlayer].fDistanceFromLastPoint = ABSF(VDIST(LGHTCYCLE_PLAYER[iPlayer].vLiveTrailOffset,LGHTCYCLE_PLAYER[iPlayer].vLastPoint))
	IF LGHTCYCLE_PLAYER[iPlayer].fDistanceFromLastPoint > MAX_SEGMENT_LENGTH
		IF LGHTCYCLE_PLAYER[iPlayer].iCurrentNumberOfSegments < iSegmentsLimit
			LGHTCYCLE_PLAYER[iPlayer].iCurrentNumberOfSegments++
		ENDIF
		
		LGHTCYCLE_PLAYER[iPlayer].vLastPoint = LGHTCYCLE_PLAYER[iPlayer].vLiveTrailOffset
		
		IF LGHTCYCLE_PLAYER[iPlayer].bLoopActive
			INT iBackofLine = GET_BACK_OF_LINE(iPlayer)
			LGHTCYCLE_PLAYER[iPlayer].vEndTop = LGHTCYCLE_PLAYER[iPlayer].LGHTCYCLE_SEGMENT[iBackofLine].vPointTop
			LGHTCYCLE_PLAYER[iPlayer].vEndBottom = LGHTCYCLE_PLAYER[iPlayer].LGHTCYCLE_SEGMENT[iBackofLine].vPointBottom
		
			LGHTCYCLE_PLAYER[iPlayer].EndCollisionBox.iStartSegment++
		ENDIF
		ADJUST_COLLISION_BOX(iPlayer)
		
		LGHTCYCLE_PLAYER[iPlayer].iFrontOfLine++
		IF LGHTCYCLE_PLAYER[iPlayer].iFrontOfLine > iSegmentsLimit
			LGHTCYCLE_PLAYER[iPlayer].iFrontOfLine = 0
		ENDIF
		
		CREATE_LATEST_PRON_LINE(iPlayer)
		
	ENDIF
	
	IF LGHTCYCLE_PLAYER[iPlayer].iCurrentNumberOfSegments = iSegmentsLimit
		INTERP_BACK_OF_PRON_TRAIL(iPlayer, LGHTCYCLE_PLAYER[iPlayer].fDistanceFromLastPoint)
	ENDIF
	
	PRINTLN("[[LH]][PRON] - CREATE_LATEST_PRON_LINE - vLastPoin = ", LGHTCYCLE_PLAYER[iPlayer].vLastPoint)

ENDPROC

PROC DRAW_RECT_POLY(BOOL bLocalPlayer, VECTOR v1, VECTOR v2, VECTOR v3, VECTOR v4, INT R, INT G, INT B, INT LeftA, INT RightA = -1, FLOAT uVLeft = 0.0, FLOAT uVRight = 1.0)
//
//	DRAW_DEBUG_TEXT(GET_STRING_FROM_INT(LeftA), v1+<<0,0,1>>, 255,0,0)
//	DRAW_DEBUG_TEXT(GET_STRING_FROM_INT(RightA), v2+<<0,0,0.5>>, 0,255,0)
//	DRAW_DEBUG_TEXT(GET_STRING_FROM_FLOAT(uVLeft), v1+<<0,0,0.15>>, 0,255,0)
//	DRAW_DEBUG_TEXT(GET_STRING_FROM_FLOAT(uVRight), v2+<<0,0,0.25>>, 0,255,0)
	
	IF RightA > -1
		DRAW_TEXTURED_POLY_WITH_THREE_COLOURS(v1,v3,v2, <<R, G, B>>, LeftA, <<R, G, B>>, LeftA, <<R, G, B>>, RightA, "Deadline", "Deadline_Trail_01", <<uVLeft,1,1>> , <<uVLeft, 0, 1>> , <<uVRight, 1, 1>>)
		DRAW_TEXTURED_POLY_WITH_THREE_COLOURS(v3,v4,v2, <<R, G, B>>, LeftA, <<R, G, B>>, RightA, <<R, G, B>>, RightA, "Deadline", "Deadline_Trail_01", <<uVLeft, 0, 1>>, <<uVRight, 0, 1>> , <<uVRight, 1, 1>>)
		IF bLocalPlayer
			DRAW_TEXTURED_POLY_WITH_THREE_COLOURS(v1+<<fLineTop,fLineTop,0>>,v1+<<-fLineTop,fLineTop,0>>,v2+<<fLineTop,-fLineTop,0>>, <<R, G, B>>, LeftA, <<R, G, B>>, LeftA, <<R, G, B>>, RightA, "Deadline", "Deadline_Trail_01", <<uVLeft,0.55,1>> , <<uVLeft, 0.5, 1>> , <<uVRight, 0.55, 1>>)
			DRAW_TEXTURED_POLY_WITH_THREE_COLOURS(v1+<<fLineTop,fLineTop,0>>,v2+<<-fLineTop,-fLineTop,0>>,v2+<<fLineTop,-fLineTop,0>>, <<R, G, B>>, LeftA, <<R, G, B>>, RightA, <<R, G, B>>, RightA, "Deadline", "Deadline_Trail_01", <<uVLeft, 0.5, 1>>, <<uVRight, 0.5, 1>> , <<uVRight, 0.55, 1>>)
		ENDIF
	ELSE
		DRAW_TEXTURED_POLY(v1,v3,v2,R,G,B,LeftA, "Deadline", "Deadline_Trail_01",<<0,1,1>>,<<0,0,1>>,<<1,1,1>>)
		DRAW_TEXTURED_POLY(v3,v4,v2,R,G,B,LeftA, "Deadline", "Deadline_Trail_01",<<0,0,1>>,<<1,0,1>>,<<1,1,1>>)
	ENDIF
	
	IF NOT g_bMissionEnding
		IF DOES_CAM_EXIST(g_birdsEyeCam)
			IF IS_INTERPOLATING_TO_SCRIPT_CAMS()
			OR IS_INTERPOLATING_FROM_SCRIPT_CAMS()
			OR bUsingBirdsEyeCam
				DRAW_LINE(v1,v2,R,G,B,LeftA)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC DRAW_POLY_PRON_LINES(INT iPlayer, BOOL bLocalPlayer)
	PRINTLN("[[LH]][PRON] - DRAWING THE LINE")
	
	//Setup the variables
	VECTOR v1,v2,v3,v4,v5,v6
	
	FLOAT fDivDist = LGHTCYCLE_PLAYER[iPlayer].fDistanceFromLastPoint/MAX_SEGMENT_LENGTH
	
	SET_BACKFACECULLING(FALSE)
	SET_DEPTHWRITING(TRUE)
	
	SETUP_PRON_PLAYER_COLOUR(iPlayer)
	
	#IF IS_DEBUG_BUILD
	//Setup the colours
	IF iPlayer = 0
		iTrailR = iMain_R			//Widget colours - To be removed
		iTrailG = iMain_G			//Widget colours - To be removed
		iTrailB = iMain_B			//Widget colours - To be removed
	ENDIF
	#ENDIF
	
	//Make sure the texture is loaded because it sometimes seems not to..
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("Deadline")
		REQUEST_STREAMED_TEXTURE_DICT("Deadline", TRUE)
	ENDIF
	
	INT LeftA = A_Segment1
	INT RightA = 0
	
	FLOAT fInterpValue = fDivDist
	
	IF fInterpValue < 0
		fInterpValue = 0
	ELIF fInterpValue > 1
		fInterpValue = 1
	ENDIF
	
	INT SInt12 = FLOOR(INTERP_FLOAT(TO_FLOAT(A_Segment1), TO_FLOAT(A_Segment2), fInterpValue))
	INT SInt2M = FLOOR(INTERP_FLOAT(TO_FLOAT(A_Segment2), TO_FLOAT(iMain_A), fInterpValue))
	INT SInt0B = FLOOR(INTERP_FLOAT(TO_FLOAT(A_SegmentB), 0.0, fInterpValue))
	INT SIntBM = FLOOR(INTERP_FLOAT(TO_FLOAT(iMain_A), TO_FLOAT(A_SegmentB), fInterpValue))
	
	SET_ENTITY_COORDS(oiTrailBottom, GET_ENTITY_BONE_POSTION(LGHTCYCLE_PLAYER[iPlayer].LightBike, GET_ENTITY_BONE_INDEX_BY_NAME(LGHTCYCLE_PLAYER[iPlayer].LightBike,"WHEEL_LR")))
	SET_ENTITY_ROTATION(oiTrailBottom, GET_ENTITY_ROTATION(LGHTCYCLE_PLAYER[iPlayer].LightBike))
	v1 = LGHTCYCLE_PLAYER[iPlayer].LGHTCYCLE_SEGMENT[LGHTCYCLE_PLAYER[iPlayer].iFrontOfLine].vPointTop
	v2 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(LGHTCYCLE_PLAYER[iPlayer].LightBike,<<0.0, Sy, Sz>>)
	v3 = LGHTCYCLE_PLAYER[iPlayer].LGHTCYCLE_SEGMENT[LGHTCYCLE_PLAYER[iPlayer].iFrontOfLine].vPointBottom
	v4 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oiTrailBottom, <<0.0,0.0,-fWheelRadius>>)
		
	DRAW_RECT_POLY(bLocalPlayer,v1,v2,v3,v4,iTrailR,iTrailG,iTrailB,LeftA,RightA)
	
	INT iBackOfLine
	IF LGHTCYCLE_PLAYER[iPlayer].iCurrentNumberOfSegments < iSegmentsLimit
		iBackOfLine = 0
	ELSE
		iBackOfLine = GET_NEXT_POINT_IN_LINE(iPlayer, LGHTCYCLE_PLAYER[iPlayer].iFrontOfLine)
	ENDIF
	INT	iSecondBackFromLine = GET_NEXT_POINT_IN_LINE(iPlayer, iBackOfLine)
	INT iThirdBackFromLine = GET_NEXT_POINT_IN_LINE(iPlayer, iBackOfLine, 1)
	INT iFirstBackFromFront = GET_NEXT_POINT_BACK_IN_LINE(iPlayer, LGHTCYCLE_PLAYER[iPlayer].iFrontOfLine)
	INT iSecondBackFromFront = GET_NEXT_POINT_BACK_IN_LINE(iPlayer, LGHTCYCLE_PLAYER[iPlayer].iFrontOfLine, 1)
	INT iThirdBackFromFront = GET_NEXT_POINT_BACK_IN_LINE(iPlayer, LGHTCYCLE_PLAYER[iPlayer].iFrontOfLine, 2)
	
	BOOL bHitLimit
	INT iLoop, iNextPoint, iDrawLimit
	IF LGHTCYCLE_PLAYER[iPlayer].iCurrentNumberOfSegments < iSegmentsLimit
		iDrawLimit = LGHTCYCLE_PLAYER[iPlayer].iCurrentNumberOfSegments-1
	ELSE
		iDrawLimit = LGHTCYCLE_PLAYER[iPlayer].iCurrentNumberOfSegments
		bHitLimit = TRUE
	ENDIF
	FOR iLoop = 0 TO iDrawLimit
		
		IF iLoop <> LGHTCYCLE_PLAYER[iPlayer].iFrontOfLine
			iNextPoint = GET_NEXT_POINT_IN_LINE(iPlayer, iLoop)
			
			IF iNextPoint > -1
				
				v1 = LGHTCYCLE_PLAYER[iPlayer].LGHTCYCLE_SEGMENT[iLoop].vPointTop
				v2 = LGHTCYCLE_PLAYER[iPlayer].LGHTCYCLE_SEGMENT[iNextPoint].vPointTop
				v3 = LGHTCYCLE_PLAYER[iPlayer].LGHTCYCLE_SEGMENT[iLoop].vPointBottom
				v4 = LGHTCYCLE_PLAYER[iPlayer].LGHTCYCLE_SEGMENT[iNextPoint].vPointBottom
				
				IF LGHTCYCLE_PLAYER[iPlayer].iCurrentNumberOfSegments > 5
					
					IF iLoop = iFirstBackFromFront
						
						//FRONT segment
						RightA = A_Segment1
						LeftA = SInt12
												
						DRAW_RECT_POLY(bLocalPlayer,v1,v2,v3,v4,iTrailR,iTrailG,iTrailB,LeftA,RightA, DEFAULT, fInterpValue)
						
					ELIF iLoop = iSecondBackFromFront
						
						//SECOND segment
						IF fDivDist < 1
							v5 = INTERP_VECTOR(v1, v2, fInterpValue)
							v6 = INTERP_VECTOR(v3, v4, fInterpValue)
							
							LeftA = SInt2M
							RightA = A_Segment2
							DRAW_RECT_POLY(bLocalPlayer,v1,v5,v3,v6,iTrailR,iTrailG,iTrailB,LeftA,RightA, DEFAULT, fInterpValue)
						
							LeftA = A_Segment2
							RightA = SInt12
							DRAW_RECT_POLY(bLocalPlayer,v5,v2,v6,v4,iTrailR,iTrailG,iTrailB,LeftA,RightA,fInterpValue)
							
						ELSE
						
							LeftA = A_Segment2
							RightA = A_Segment1
							
							DRAW_RECT_POLY(bLocalPlayer,v1,v2,v3,v4,iTrailR,iTrailG,iTrailB,LeftA,RightA)
							
						ENDIF
					ELIF iLoop = iThirdBackFromFront
						
						//THIRD segment
						IF fDivDist < 1
							v5 = INTERP_VECTOR(v1, v2, fInterpValue)
							v6 = INTERP_VECTOR(v3, v4, fInterpValue)
							
							LeftA = iMain_A
							DRAW_RECT_POLY(bLocalPlayer,v1,v5,v3,v6,iTrailR,iTrailG,iTrailB,LeftA, DEFAULT, DEFAULT, fInterpValue)
							
							RightA = SInt2M
							DRAW_RECT_POLY(bLocalPlayer,v5,v2,v6,v4,iTrailR,iTrailG,iTrailB,LeftA,RightA, fInterpValue)
							
						ELSE
							
							RightA = A_Segment2
							LeftA = iMain_A
							DRAW_RECT_POLY(bLocalPlayer,v1,v2,v3,v4,iTrailR,iTrailG,iTrailB,LeftA,RightA)
													
						ENDIF
						
					ELIF iLoop = iBackOfLine
						
						//BACK segment
						IF fDivDist < 1
						AND bHitLimit
							LeftA = 0
							RightA = SInt0B
						ELSE
							LeftA = 0
							RightA = A_SegmentB
						ENDIF
						
						IF bHitLimit
							DRAW_RECT_POLY(bLocalPlayer,v1,v2,v3,v4,iTrailR,iTrailG,iTrailB,LeftA,RightA,fInterpValue)
						ELSE
							DRAW_RECT_POLY(bLocalPlayer,v1,v2,v3,v4,iTrailR,iTrailG,iTrailB,LeftA,RightA)
						ENDIF
						
					ELIF iLoop = iSecondBackFromLine
						
						//SECOND FROM BACK segment
						IF fDivDist < 1
						AND bHitLimit
							v5 = INTERP_VECTOR(v1, v2, fInterpValue)
							v6 = INTERP_VECTOR(v3, v4, fInterpValue)
							
							LeftA = SInt0B
							RightA = A_SegmentB
							DRAW_RECT_POLY(bLocalPlayer,v1,v5,v3,v6,iTrailR,iTrailG,iTrailB,LeftA,RightA,DEFAULT,fInterpValue)
							
							LeftA = A_SegmentB
							RightA = SIntBM
							DRAW_RECT_POLY(bLocalPlayer,v5,v2,v6,v4,iTrailR,iTrailG,iTrailB,LeftA,RightA,fInterpValue)
							
						ELSE
							
							LeftA = A_SegmentB
							RightA = iMain_A
							
							DRAW_RECT_POLY(bLocalPlayer,v1,v2,v3,v4,iTrailR,iTrailG,iTrailB,LeftA,RightA)
							
						ENDIF
						
					ELIF iLoop = iThirdBackFromLine
						
						//THIRD FROM BACK segment
						IF fDivDist < 1
						AND bHitLimit
							v5 = INTERP_VECTOR(v1, v2, fDivDist)
							v6 = INTERP_VECTOR(v3, v4, fDivDist)
							
							LeftA = FLOOR(INTERP_FLOAT(TO_FLOAT(iMain_A), TO_FLOAT(A_SegmentB), fInterpValue))
							RightA = iMain_A
							DRAW_RECT_POLY(bLocalPlayer,v1,v5,v3,v6,iTrailR,iTrailG,iTrailB,LeftA,RightA,DEFAULT,fInterpValue)
							
							DRAW_RECT_POLY(bLocalPlayer,v5,v2,v6,v4,iTrailR,iTrailG,iTrailB,iMain_A,DEFAULT,fInterpValue)
							
						ELSE
							
							DRAW_RECT_POLY(bLocalPlayer,v1,v2,v3,v4,iTrailR,iTrailG,iTrailB,iMain_A)
													
						ENDIF
						
					ELSE
						
						//Normal Segments
						DRAW_RECT_POLY(bLocalPlayer,v1,v2,v3,v4,iTrailR,iTrailG,iTrailB,iMain_A)
						
					ENDIF
				
				ELSE
				
					
					IF iLoop = 0
						
						//BACK segment
						IF LGHTCYCLE_PLAYER[iPlayer].iCurrentNumberOfSegments = 2
							RightA = A_Segment1
						ELIF LGHTCYCLE_PLAYER[iPlayer].iCurrentNumberOfSegments = 3
							IF fDivDist < 1	
								RightA = FLOOR(INTERP_FLOAT(TO_FLOAT(A_Segment1), TO_FLOAT(A_SegmentB), fInterpValue))
							ELSE
								RightA = A_Segment1
							ENDIF
						ELSE
							RightA = A_SegmentB
						ENDIF
						LeftA = 0
						
						DRAW_RECT_POLY(bLocalPlayer,v1,v2,v3,v4,iTrailR,iTrailG,iTrailB,LeftA,RightA)
						
					ELIF iLoop = 1	
						
						//SECOND FROM BACK segment
						IF LGHTCYCLE_PLAYER[iPlayer].iCurrentNumberOfSegments = 3
							RightA = A_Segment1
							IF fDivDist < 1		
								LeftA = FLOOR(INTERP_FLOAT(TO_FLOAT(A_Segment1), TO_FLOAT(A_SegmentB), fInterpValue))
							ELSE
								LeftA = A_Segment1
							ENDIF
						ELIF LGHTCYCLE_PLAYER[iPlayer].iCurrentNumberOfSegments = 4
							IF fDivDist < 1
								RightA = FLOOR(INTERP_FLOAT(TO_FLOAT(A_Segment1), TO_FLOAT(A_Segment2), fInterpValue))
							ELSE
								RightA = A_Segment1
							ENDIF
							LeftA = A_SegmentB
						ELSE
							IF fDivDist < 1
								RightA = FLOOR(INTERP_FLOAT(TO_FLOAT(A_Segment2), TO_FLOAT(iMain_A), fInterpValue))
							ELSE
								RightA = A_Segment2
							ENDIF
							LeftA = A_SegmentB
						ENDIF
						
						DRAW_RECT_POLY(bLocalPlayer,v1,v2,v3,v4,iTrailR,iTrailG,iTrailB,LeftA,RightA)
					
					ELIF iLoop = 2	
						
						//SECOND FROM BACK segment
						IF LGHTCYCLE_PLAYER[iPlayer].iCurrentNumberOfSegments = 4
							RightA = A_Segment1
							IF fDivDist < 1
								LeftA = FLOOR(INTERP_FLOAT(TO_FLOAT(A_Segment1), TO_FLOAT(A_Segment2), fInterpValue))
							ELSE
								LeftA = A_Segment1
							ENDIF
						ELSE LGHTCYCLE_PLAYER[iPlayer].iCurrentNumberOfSegments = 5
							IF fDivDist < 1
								RightA = FLOOR(INTERP_FLOAT(TO_FLOAT(A_Segment1), TO_FLOAT(A_Segment2), fInterpValue))
								LeftA = FLOOR(INTERP_FLOAT(TO_FLOAT(A_Segment2), TO_FLOAT(iMain_A), fInterpValue))
							ELSE
								RightA = A_Segment1
								LeftA = A_Segment2
							ENDIF
						ENDIF
						
						DRAW_RECT_POLY(bLocalPlayer,v1,v2,v3,v4,iTrailR,iTrailG,iTrailB,LeftA,RightA)
					
					ELIF iLoop = 3	
						
						RightA = A_Segment1
						IF fDivDist < 1
							LeftA = FLOOR(INTERP_FLOAT(TO_FLOAT(A_Segment1), TO_FLOAT(A_Segment2), fInterpValue))
						ELSE
							LeftA = A_Segment1
						ENDIF
						
						DRAW_RECT_POLY(bLocalPlayer,v1,v2,v3,v4,iTrailR,iTrailG,iTrailB,LeftA,RightA)
					
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
		
	ENDFOR
	
	SET_BACKFACECULLING(TRUE)
	SET_DEPTHWRITING(FALSE)
	
ENDPROC

PROC CLEANUP_BIKE_AUDIO(INT iPlayer)

	IF iPlayer = 0 
		IF iTrailSound0 != -1
			IF IS_BIT_SET(iPronAudioBS,0) 
				IF NOT HAS_SOUND_FINISHED(iTrailSound0)
					STOP_SOUND(iTrailSound0)
					RELEASE_SOUND_ID(iTrailSound0)
					iTrailSound0 = -1
					CLEAR_BIT(iPronAudioBS,0) 
					PRINTLN("[RCC MISSION] [PRON AUDIO]- [HANDLE_PRON_AUDIO] STOPPING SOUND iTrailSound0")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF iPlayer = 1
		IF iTrailSound1 != -1
			IF IS_BIT_SET(iPronAudioBS,1) 
				IF NOT HAS_SOUND_FINISHED(iTrailSound1)
					STOP_SOUND(iTrailSound1)
					RELEASE_SOUND_ID(iTrailSound1)
					iTrailSound1 = -1
					CLEAR_BIT(iPronAudioBS,1) 
					PRINTLN("[RCC MISSION] [PRON AUDIO]- [HANDLE_PRON_AUDIO] STOPPING SOUND iTrailSound1")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF iPlayer = 2 
		IF iTrailSound2 != -1
			IF IS_BIT_SET(iPronAudioBS,2) 
				IF NOT HAS_SOUND_FINISHED(iTrailSound2)
					STOP_SOUND(iTrailSound2)
					RELEASE_SOUND_ID(iTrailSound2)
					iTrailSound2 = -1
					CLEAR_BIT(iPronAudioBS,2) 
					PRINTLN("[RCC MISSION] [PRON AUDIO]- [HANDLE_PRON_AUDIO] STOPPING SOUND iTrailSound2")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF iPlayer = 3 
		IF iTrailSound3 != -1
			IF IS_BIT_SET(iPronAudioBS,3) 
				IF NOT HAS_SOUND_FINISHED(iTrailSound3)
					STOP_SOUND(iTrailSound3)
					RELEASE_SOUND_ID(iTrailSound3)
					iTrailSound3 = -1
					CLEAR_BIT(iPronAudioBS,3) 
					PRINTLN("[RCC MISSION] [PRON AUDIO]- [HANDLE_PRON_AUDIO] STOPPING SOUND iTrailSound3")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC HANDLE_PRON_AUDIO()

	STRING sAudioBank1
	STRING sAudioBank2
	
	STRING sSoundSet
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA(g_FMMC_STRUCT.iAdversaryModeType)
		sAudioBank1 = "DLC_IMPORTEXPORT/IE_VV_01"
		sAudioBank2 = "DLC_IMPORTEXPORT/IE_VV_02"
		sSoundSet = sVVPronSoundSet
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
		// coming back to this.
		sAudioBank1 = ""
		sAudioBank2 = ""
		sSoundSet = sVVPronSoundSet
		SET_BIT(iLocalBoolCheck18, LBOOL18_PRON_AUDIO_LOADED)
	ELSE
		sAudioBank1 = "DLC_BIKER/BKR_DL_01"
		sAudioBank2 = "DLC_BIKER/BKR_DL_02"
		sSoundSet = "DLC_Biker_DL_Sounds"
	ENDIF
	
	IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA(g_FMMC_STRUCT.iAdversaryModeType)	
	AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
		IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_Biker_DL_Scene")
			START_AUDIO_SCENE("DLC_Biker_DL_Scene")
			SET_BIT(iLocalBoolCheck18,LBOOL18_ENABLE_PRON_AUDIO_SCENE)
			PRINTLN("[PRON AUDIO] - HANDLE_PRON_AUDIO - audio scene started")
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck18, LBOOL18_PRON_AUDIO_LOADED)
		BOOL bLoadedAudio = FALSE
		
		IF NOT IS_STRING_NULL_OR_EMPTY(sAudioBank1)
		AND NOT IS_STRING_NULL_OR_EMPTY(sAudioBank2)
			IF REQUEST_SCRIPT_AUDIO_BANK(sAudioBank1)
				bLoadedAudio = TRUE
			ELSE
				bLoadedAudio = FALSE
			ENDIF
			IF REQUEST_SCRIPT_AUDIO_BANK(sAudioBank2)
				bLoadedAudio = TRUE
			ELSE
				bLoadedAudio = FALSE
			ENDIF
			
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA(g_FMMC_STRUCT.iAdversaryModeType)
				IF REQUEST_SCRIPT_AUDIO_BANK("DLC_IMPORTEXPORT/IE_VV_03")
					bLoadedAudio = TRUE
				ELSE
					bLoadedAudio = FALSE
				ENDIF
			ENDIF

			IF bLoadedAudio
				PRINTLN("[PRON AUDIO] - HANDLE_PRON_AUDIO - LBOOL18_PRON_AUDIO_LOADED")
				SET_BIT(iLocalBoolCheck18, LBOOL18_PRON_AUDIO_LOADED)
			ENDIF
		ENDIF
	ENDIF
	
	VECTOR vBikeFront
	VECTOR vBikeSide
	VECTOR vBikePos
	VECTOR vBikeUp
	FLOAT fDotProduct
	FLOAT fLeanAngle
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sSoundSet)
		IF IS_BIT_SET(iLocalBoolCheck18, LBOOL18_PRON_AUDIO_LOADED)
			IF NOT IS_BIT_SET(iPronAudioBS,0) 
				IF DOES_ENTITY_EXIST(LGHTCYCLE_PLAYER[0].LightBike)
					IF IS_VEHICLE_DRIVEABLE(LGHTCYCLE_PLAYER[0].LightBike)
						PRINTLN("[PRON AUDIO] iTrailSound0: ", iTrailSound0)
						iTrailSound0 = GET_SOUND_ID()
						IF iTrailSound0 != -1
							STRING sSoundName = "Trail_00"
							IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
								sSoundName = "Trail_Loop_00"
							ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA(g_FMMC_STRUCT.iAdversaryModeType)
								sSoundName = "Trail_Loop_01"
							ENDIF
							
							PLAY_SOUND_FROM_ENTITY(iTrailSound0,sSoundName,LGHTCYCLE_PLAYER[0].LightBike,sSoundSet)
							
							PRINTLN("[PRON AUDIO] - HANDLE_PRON_AUDIO - start bike sound 00")
							SET_BIT(iPronAudioBS,0) 
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF iTrailSound0 != -1
					IF IS_ENTITY_ALIVE(LGHTCYCLE_PLAYER[0].LightBike)
						GET_ENTITY_MATRIX(LGHTCYCLE_PLAYER[0].LightBike, vBikeFront, vBikeSide, vBikeUp, vBikePos)
						fDotProduct = DOT_PRODUCT(NORMALISE_VECTOR(vBikeUp), NORMALISE_VECTOR(<<0,0,1>>))
						fLeanAngle = CLAMP(((ACOS(CLAMP(fDotProduct, -1.0, 1.0))) / MAX_LEAN_ANGLE), 0.0, 1.0)
						PRINTLN("[KH] HANDLE_PRON_AUDIO(0) - fDotProduct:", fDotProduct, " - fLeanAngle: ", fLeanAngle)
						SET_VARIABLE_ON_SOUND(iTrailSound0, "Mod", fLeanAngle)
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(iPronAudioBS,1) 
				IF DOES_ENTITY_EXIST(LGHTCYCLE_PLAYER[1].LightBike)
					IF IS_VEHICLE_DRIVEABLE(LGHTCYCLE_PLAYER[1].LightBike)
						PRINTLN("[PRON AUDIO] iTrailSound1: ", iTrailSound1)
						iTrailSound1 = GET_SOUND_ID()
						IF iTrailSound1 != -1
							STRING sSoundName = "Trail_01"
							IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
								sSoundName = "Trail_Loop_01"
							ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA(g_FMMC_STRUCT.iAdversaryModeType)
								sSoundName = "Trail_Loop_02"
							ENDIF
							
							PLAY_SOUND_FROM_ENTITY(iTrailSound1,sSoundName,LGHTCYCLE_PLAYER[1].LightBike,sSoundSet)
								
							PRINTLN("[PRON AUDIO] - HANDLE_PRON_AUDIO - start bike sound 01")
							SET_BIT(iPronAudioBS,1) 
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF iTrailSound1 != -1
					IF IS_ENTITY_ALIVE(LGHTCYCLE_PLAYER[1].LightBike)
						GET_ENTITY_MATRIX(LGHTCYCLE_PLAYER[1].LightBike, vBikeFront, vBikeSide, vBikeUp, vBikePos)
						fDotProduct = DOT_PRODUCT(NORMALISE_VECTOR(vBikeUp), NORMALISE_VECTOR(<<0,0,1>>))
						fLeanAngle = CLAMP(((ACOS(CLAMP(fDotProduct, -1.0, 1.0))) / MAX_LEAN_ANGLE), 0.0, 1.0)
						PRINTLN("[KH] HANDLE_PRON_AUDIO(1) - fDotProduct:", fDotProduct, " - fLeanAngle: ", fLeanAngle)
						SET_VARIABLE_ON_SOUND(iTrailSound1, "Mod", fLeanAngle)
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(iPronAudioBS,2) 
				IF DOES_ENTITY_EXIST(LGHTCYCLE_PLAYER[2].LightBike)
					IF IS_VEHICLE_DRIVEABLE(LGHTCYCLE_PLAYER[2].LightBike)
						PRINTLN("[PRON AUDIO] iTrailSound2: ", iTrailSound2)
						iTrailSound2 = GET_SOUND_ID()
						IF iTrailSound2 != -1
							STRING sSoundName = "Trail_02"
							IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
								sSoundName = "Trail_Loop_02"
							ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA(g_FMMC_STRUCT.iAdversaryModeType)
								sSoundName = "Trail_Loop_03"
							ENDIF
							
							PLAY_SOUND_FROM_ENTITY(iTrailSound2,sSoundName,LGHTCYCLE_PLAYER[2].LightBike,sSoundSet)
							
							PRINTLN("[PRON AUDIO] - HANDLE_PRON_AUDIO - start bike sound 02")
							SET_BIT(iPronAudioBS,2) 
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF iTrailSound2 != -1
					IF IS_ENTITY_ALIVE(LGHTCYCLE_PLAYER[2].LightBike)
						GET_ENTITY_MATRIX(LGHTCYCLE_PLAYER[2].LightBike, vBikeFront, vBikeSide, vBikeUp, vBikePos)
						fDotProduct = DOT_PRODUCT(NORMALISE_VECTOR(vBikeUp), NORMALISE_VECTOR(<<0,0,1>>))
						fLeanAngle = CLAMP(((ACOS(CLAMP(fDotProduct, -1.0, 1.0))) / MAX_LEAN_ANGLE), 0.0, 1.0)
						PRINTLN("[KH] HANDLE_PRON_AUDIO(2) - fDotProduct:", fDotProduct, " - fLeanAngle: ", fLeanAngle)
						SET_VARIABLE_ON_SOUND(iTrailSound2, "Mod", fLeanAngle)
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(iPronAudioBS,3) 
				IF DOES_ENTITY_EXIST(LGHTCYCLE_PLAYER[3].LightBike)
					IF IS_VEHICLE_DRIVEABLE(LGHTCYCLE_PLAYER[3].LightBike)
						PRINTLN("[PRON AUDIO] iTrailSound3: ", iTrailSound3)
						iTrailSound3 = GET_SOUND_ID()
						IF iTrailSound3 != -1
							
							STRING sSoundName = "Trail_03"
							IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
								sSoundName = "Trail_Loop_04"
							ENDIF
							
							PLAY_SOUND_FROM_ENTITY(iTrailSound3,sSoundName,LGHTCYCLE_PLAYER[3].LightBike,sSoundSet)
							PRINTLN("[PRON AUDIO] - HANDLE_PRON_AUDIO - start bike sound 03")
							SET_BIT(iPronAudioBS,3) 
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF iTrailSound3 != -1
					IF IS_ENTITY_ALIVE(LGHTCYCLE_PLAYER[3].LightBike)
						GET_ENTITY_MATRIX(LGHTCYCLE_PLAYER[3].LightBike, vBikeFront, vBikeSide, vBikeUp, vBikePos)
						fDotProduct = DOT_PRODUCT(NORMALISE_VECTOR(vBikeUp), NORMALISE_VECTOR(<<0,0,1>>))
						fLeanAngle = CLAMP(((ACOS(CLAMP(fDotProduct, -1.0, 1.0))) / MAX_LEAN_ANGLE), 0.0, 1.0)
						PRINTLN("[KH] HANDLE_PRON_AUDIO(3) - fDotProduct:", fDotProduct, " - fLeanAngle: ", fLeanAngle)
						SET_VARIABLE_ON_SOUND(iTrailSound3, "Mod", fLeanAngle)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC PROCESS_PRON_COLLISION_VIBRATION(FLOAT fDistToCollision)
	FLOAT fMaxDist = 6.0
	FLOAT fMinDist = 0.75
	
	IF fDistToCollision < fMaxDist
		FLOAT fNormalisedDist =  1.0 - ((fDistToCollision - fMinDist) / (fMaxDist - fMinDist))
		INT iVibrationFrequency = CLAMP_INT(ROUND(fNormalisedDist * 256), 30, 256)
		
		PRINTLN("[KH] PROCESS_PRON_COLLISION_VIBRATION - fDistToCollision: ", fDistToCollision, " - fNormalisedDist: ", fNormalisedDist, " - iVibrationFrequency: ", iVibrationFrequency)
		
		SET_CONTROL_SHAKE(PLAYER_CONTROL, 130, iVibrationFrequency)
	ENDIF
ENDPROC

FUNC BOOL CAN_SEGMENT_BE_CHECKED(INT iPlayer, INT iSegment, BOOL bIsPlayer = FALSE)

	IF iSegment = GET_NEXT_POINT_IN_LINE(iPlayer, LGHTCYCLE_PLAYER[iPlayer].iFrontOfLine)
	OR iSegment = GET_NEXT_POINT_IN_LINE(iPlayer, LGHTCYCLE_PLAYER[iPlayer].iFrontOfLine, 1)
	OR iSegment = LGHTCYCLE_PLAYER[iPlayer].iFrontOfLine
		PRINTLN("[LH][PRONCOL][NewPrints] iSegment ", iSegment," is the back of the line! Not checking.")
		RETURN FALSE
	ENDIF

	IF NOT bIsPlayer
		RETURN TRUE
	ENDIF
	
	PRINTLN("[LH][PRONCOL][NewPrints] iSegment = ", iSegment)
	PRINTLN("[LH][PRONCOL][NewPrints] LGHTCYCLE_PLAYER[",iPlayer,"].iFrontOfLine = ", LGHTCYCLE_PLAYER[iPlayer].iFrontOfLine)
	
	IF iSegment = GET_NEXT_POINT_BACK_IN_LINE(iPlayer, LGHTCYCLE_PLAYER[iPlayer].iFrontOfLine)
	OR iSegment = GET_NEXT_POINT_BACK_IN_LINE(iPlayer, LGHTCYCLE_PLAYER[iPlayer].iFrontOfLine, 1)
	OR iSegment = GET_NEXT_POINT_BACK_IN_LINE(iPlayer, LGHTCYCLE_PLAYER[iPlayer].iFrontOfLine, 2)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE	

ENDFUNC

PROC CHECK_COLLISION_BOX_FOR_CLOSE_SEGMENTS(LINE_AABB CollisionBox, INT iPlayer, VECTOR vFrontofBike, INT &SEGMENT[], INT &iTempSegments, BOOL &bSegmentsToCheck, BOOL bIsPlayer = FALSE)
	IF IS_POINT_IN_AXIS_ALIGNED_AREA(vFrontofBike, CollisionBox.vLowest, CollisionBox.vHighest)
		// Get a list of the closest segments
		INT iSegment
		FLOAT fExtra = MAX_SEGMENT_LENGTH*3
		FOR iSegment = CollisionBox.iStartSegment TO CollisionBox.iEndSegment STEP 2
			IF CAN_SEGMENT_BE_CHECKED(iPlayer, iSegment, bIsPlayer)
				VECTOR v1 = LGHTCYCLE_PLAYER[iPlayer].LGHTCYCLE_SEGMENT[iSegment].vPointTop
				VECTOR v2 = LGHTCYCLE_PLAYER[iPlayer].LGHTCYCLE_SEGMENT[GET_NEXT_POINT_IN_LINE(iPlayer,iSegment,1)].vPointTop
				IF IS_POINT_IN_AXIS_ALIGNED_AREA(vFrontofBike, <<FMIN(v1.x, v2.x)-fExtra,FMIN(v1.y, v2.y)-fExtra,0.0>>, <<FMAX(v1.x, v2.x)+fExtra,FMAX(v1.y, v2.y)+fExtra,100.0>>, FALSE)
					SEGMENT[iTempSegments] = iSegment
					iTempSegments++
					SEGMENT[iTempSegments] = GET_NEXT_POINT_IN_LINE(iPlayer,iSegment)
					iTempSegments++
					bSegmentsToCheck = TRUE
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

FUNC FLOAT GET_DOT_PRODUCT_FROM_LINE(INT iPlayer, INT iPoint, VECTOR vBikePos)
	
	INT iNextPoint = GET_NEXT_POINT_IN_LINE(iPlayer, iPoint, 1)		
	VECTOR PlayerNormal = vBikePos - LGHTCYCLE_PLAYER[iPlayer].LGHTCYCLE_SEGMENT[iPoint].vPointBottom
	VECTOR LineNormal = LGHTCYCLE_PLAYER[iPlayer].LGHTCYCLE_SEGMENT[iNextPoint].vPointBottom - LGHTCYCLE_PLAYER[iPlayer].LGHTCYCLE_SEGMENT[iPoint].vPointBottom
	VECTOR LineCross = LGHTCYCLE_PLAYER[iPlayer].LGHTCYCLE_SEGMENT[iPoint].vPointTop - LGHTCYCLE_PLAYER[iPlayer].LGHTCYCLE_SEGMENT[iPoint].vPointBottom
	LineNormal = CROSS_PRODUCT(LineNormal, LineCross)
	NORMALISE_VECTOR(LineNormal)
	NORMALISE_VECTOR(PlayerNormal)
	RETURN DOT_PRODUCT(LineNormal, PlayerNormal)
	
ENDFUNC

FUNC BOOL HAS_POINT_CROSSED_THE_PRON_LINE(INT iPlayer, INT iClosestPoint, VECTOR vCollisionPoint, VECTOR vSecondPoint)

	BOOL bHasCrossedLine

	PRINTLN("vCollisionPoint = ",vCollisionPoint," vSecondPoint = ",vSecondPoint)
	//DRAW_DEBUG_SPHERE(vCollisionPoint, 0.2, 255, 0, 0, 150)
	//DRAW_DEBUG_SPHERE(vSecondPoint, 0.2, 0, 0, 255, 150)
	
	FLOAT fDotProduct = GET_DOT_PRODUCT_FROM_LINE(iPlayer, iClosestPoint, vCollisionPoint)
	FLOAT fSecondDotProduct = GET_DOT_PRODUCT_FROM_LINE(iPlayer, iClosestPoint, vSecondPoint)
	PRINTLN("[LH][PRONCOL] fDotProduct = ",fDotProduct," fSecondDotProduct = ",fSecondDotProduct)
	
	IF (fDotProduct >= 0 AND fSecondDotProduct < 0)
	OR (fDotProduct <= 0 AND fSecondDotProduct > 0)
		bHasCrossedLine = TRUE
	ENDIF
	
	RETURN bHasCrossedLine
	
ENDFUNC

FUNC BOOL IS_CROSS_POINT_WITHIN_LINE(INT iPlayer, INT iClosest, VECTOR vCollisionPoint)

	INT iNextPoint = GET_NEXT_POINT_IN_LINE(iPlayer, iClosest, 1)
	
	VECTOR vClosestPointTop = GET_CLOSEST_POINT_ON_LINE(vCollisionPoint,LGHTCYCLE_PLAYER[iPlayer].LGHTCYCLE_SEGMENT[iClosest].vPointTop,LGHTCYCLE_PLAYER[iPlayer].LGHTCYCLE_SEGMENT[iNextPoint].vPointTop)
	VECTOR vClosestPointBottom = GET_CLOSEST_POINT_ON_LINE(vCollisionPoint,LGHTCYCLE_PLAYER[iPlayer].LGHTCYCLE_SEGMENT[iClosest].vPointBottom,LGHTCYCLE_PLAYER[iPlayer].LGHTCYCLE_SEGMENT[iNextPoint].vPointBottom)
	
	FLOAT fRatioLeftRight = GET_RATIO_OF_CLOSEST_POINT_ON_LINE(vCollisionPoint,LGHTCYCLE_PLAYER[iPlayer].LGHTCYCLE_SEGMENT[iClosest].vPointBottom,LGHTCYCLE_PLAYER[iPlayer].LGHTCYCLE_SEGMENT[iNextPoint].vPointBottom, FALSE)
	FLOAT fRatioUpDown = GET_RATIO_OF_CLOSEST_POINT_ON_LINE(vCollisionPoint, vClosestPointTop, vClosestPointBottom, FALSE)
	PRINTLN("[LH][PRONCOL][NewPrints] fRatioLeftRight = ", fRatioLeftRight, " fRatioUpDown = ", fRatioUpDown)
	//DRAW_DEBUG_LINE(vCollisionPoint, GET_CLOSEST_POINT_ON_LINE(vCollisionPoint, vClosestPointTop, vClosestPointBottom, FALSE), 0,255,0,80)
	
	IF fRatioUpDown >= 0 AND fRatioUpDown <= 1
	AND fRatioLeftRight > 0 AND fRatioLeftRight < 1
		//DRAW_DEBUG_LINE(vCollisionPoint, GET_CLOSEST_POINT_ON_LINE(vCollisionPoint, vClosestPointTop, vClosestPointBottom, FALSE), 255,0,0)
		PRINTLN("[LH][PRON][CROSSPOINT] Player IS WITHIN LINE BOUNDS")
		RETURN TRUE
	ENDIF
	
	PRINTLN("[LH][PRON][CROSSPOINT] Player IS NOT within line bounds")
	RETURN FALSE

ENDFUNC



PROC COLLIDE_WITH_LINE(INT iPlayer, VEHICLE_INDEX viLightBike, PLAYER_INDEX CurrentPlayerId, BOOL bIsPlayer = FALSE)
	
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA(g_FMMC_STRUCT.iAdversaryModeType)
		IF NETWORK_IS_LOCAL_PLAYER_INVINCIBLE()
			PRINTLN("[VV] - COLLIDE_WITH_LINE - exit because respawn invin ")
			EXIT
		ENDIF
		
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iVehicleWeaponEffectActiveBS, ciVEH_WEP_PRON)
			IF HAS_NET_TIMER_STARTED(stVehWepPronInvTimer)
			AND (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stVehWepPronInvTimer)) < 1000
				PRINTLN("[VV] - COLLIDE_WITH_LINE - exit because power up invin ")
				EXIT
			ENDIF
		ENDIF
		
	ENDIF
	
	IF NETWORK_HAS_CONTROL_OF_ENTITY(LocalPlayerPed)
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA(g_FMMC_STRUCT.iAdversaryModeType)
			IF NOT NETWORK_IS_LOCAL_PLAYER_INVINCIBLE()
				SET_ENTITY_INVINCIBLE(LocalPlayerPed, FALSE)
			ENDIF
		ELSE
			SET_ENTITY_INVINCIBLE(LocalPlayerPed, FALSE)
		ENDIF
		PRINTLN("[VV] - COLLIDE_WITH_LINE - Setting invincibilty off ")
	ELSE
		NETWORK_REQUEST_CONTROL_OF_ENTITY(LocalPlayerPed)
	ENDIF
	
	IF NETWORK_HAS_CONTROL_OF_ENTITY(viLightBike)
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA(g_FMMC_STRUCT.iAdversaryModeType)
			IF NOT NETWORK_IS_LOCAL_PLAYER_INVINCIBLE()
				SET_ENTITY_INVINCIBLE(viLightBike, FALSE)
			ENDIF
		ELSE
			SET_ENTITY_INVINCIBLE(viLightBike, FALSE)
		ENDIF
	ELSE
		NETWORK_REQUEST_CONTROL_OF_ENTITY(viLightBike)
	ENDIF
	SET_BIT(iLocalPronBitSet, ciNEEDS_TO_START_STARTUP)
	IF iBroadcastOnTimeFalse = 0
	OR (GET_GAME_TIMER() - iBroadcastOnTimeFalse) >= 250
		BROADCAST_PRON_PLAYERS_DRAWING_TRAILS( MC_playerBD[iPartToUse].iTeam, FALSE )
		iBroadcastOnTimeFalse = GET_GAME_TIMER()
	ENDIF
	SET_ENTITY_HEALTH(LocalPlayerPed, 125)
	
	IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA(g_FMMC_STRUCT.iAdversaryModeType)
		IF IS_VEHICLE_DRIVEABLE(viLightBike)
			NETWORK_EXPLODE_VEHICLE(viLightBike, DEFAULT, DEFAULT, NATIVE_TO_INT(CurrentPlayerID))
		ENDIF
		SET_BIT(iLocalBoolCheck20, LBOOL20_MARK_PRON_BIKE_FOR_CLEANUP)
	ELSE
		IF NOT IS_BIT_SET(iLocalPronBitSet,ciALREADY_COLLIDED_THIS_PLAYER)
			IF IS_VEHICLE_DRIVEABLE(viLightBike)
				IF bIsPlayer
					IF NOT NETWORK_IS_LOCAL_PLAYER_INVINCIBLE()
						NETWORK_EXPLODE_VEHICLE(viLightBike, DEFAULT, DEFAULT,NATIVE_TO_INT(LocalPlayer))
						SET_BIT(iLocalPronBitSet,ciALREADY_COLLIDED_THIS_PLAYER)
					ENDIF
				ELSE
					NETWORK_EXPLODE_VEHICLE(viLightBike, DEFAULT, DEFAULT, NATIVE_TO_INT(CurrentPlayerID))
					SET_BIT(iLocalPronBitSet,ciALREADY_COLLIDED_THIS_PLAYER)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	SET_KILL_STRIP_OVER_RIDE_PLAYER_ID(CurrentPlayerID)
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciCLEAR_UP_VEHICLE_ON_DEATH)
		IF NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(viLightBike)
			NETWORK_INDEX niVeh = VEH_TO_NET(viLightBike)
			START_NET_TIMER(vehicleCleanUpTimer)
			netVehicleToCleanUp = niVeh
		ENDIF
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sVVPronSoundSet)
		//IF CurrentPlayerID = LocalPlayer
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
			PRINTLN("[PRON AUDIO] - VV COLLIDE_WITH_LINE - crash sound TR")
			PLAY_SOUND_FROM_COORD(-1, "Crash", GET_ENTITY_COORDS(viLightBike,FALSE), sVVPronSoundSet)
			CLEANUP_BIKE_AUDIO(iPlayer)
		ELIF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA(g_FMMC_STRUCT.iAdversaryModeType)
			IF REQUEST_SCRIPT_AUDIO_BANK("DLC_BIKER/BKR_DL_01")
			OR REQUEST_SCRIPT_AUDIO_BANK("DLC_BIKER/BKR_DL_02")
				PRINTLN("[PRON AUDIO] - COLLIDE_WITH_LINE - crash sound other")
				PLAY_SOUND_FROM_COORD(-1, "Crash", GET_ENTITY_COORDS(viLightBike,FALSE),"DLC_Biker_DL_Sounds")
				CLEANUP_BIKE_AUDIO(iPlayer)
			ENDIF
		ELSE
			PRINTLN("[PRON AUDIO] - VV COLLIDE_WITH_LINE - crash sound VV")
			PLAY_SOUND_FROM_COORD(-1, "Crash", GET_ENTITY_COORDS(viLightBike,FALSE),sVVPronSoundSet)
			CLEANUP_BIKE_AUDIO(iPlayer)
		ENDIF
		//ENDIF
	ENDIF


ENDPROC

FUNC BOOL IS_THE_BIKE_WITHIN_THE_SEGMENT_LIMITS(INT iPlayer, INT iClosestPoint, VECTOR vFrontofBike, VECTOR vBackofBike, VECTOR vPedChest, BOOL &bNextLastCanCheck)
	
	IF IS_CROSS_POINT_WITHIN_LINE(iPlayer, iClosestPoint, vLastFramePos)
		bNextLastCanCheck = TRUE
	ENDIF
	
	IF bNextLastCanCheck
	OR IS_CROSS_POINT_WITHIN_LINE(iPlayer, iClosestPoint, vFrontofBike)
	OR IS_CROSS_POINT_WITHIN_LINE(iPlayer, iClosestPoint, vBackofBike)
	OR IS_CROSS_POINT_WITHIN_LINE(iPlayer, iClosestPoint, vPedChest)
	OR IS_CROSS_POINT_WITHIN_LINE(iPlayer, iClosestPoint, vFrontofBike+vNormalVelocity)
	OR IS_CROSS_POINT_WITHIN_LINE(iPlayer, iClosestPoint, vBackofBike+vNormalVelocity)
	OR IS_CROSS_POINT_WITHIN_LINE(iPlayer, iClosestPoint, vPedChest+vNormalVelocity)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC CHECK_PRON_LINE_COLLISION(INT iPlayer, VEHICLE_INDEX viLightBike, BOOL bIsPlayer, PLAYER_INDEX CurrentPlayerId, FLOAT fOffsetMultiplier = 0.0)
	
	INT iTempSegments 
	INT iTempSegments2
	INT iClosestPoint
	BOOL bSegmentsToCheck
	FLOAT fClosestDistance = 600.0
	VECTOR vFrontofBike, vBackofBike, vPedChest
	INT SEGMENT[MAX_SEGMENTS-1]
	VECTOR vMax,vMin

	IF DOES_ENTITY_EXIST(viLightBike)
		MODEL_NAMES mnBikeModel = GET_ENTITY_MODEL(viLightBike)
		GET_MODEL_DIMENSIONS(mnBikeModel,vMin,vMax)
		IF fOffsetMultiplier <> 0
			PRINTLN("[LH][PRONCOL] Checking for car! Offset: ",fOffsetMultiplier)
			vFrontofBike = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viLightBike,<<(vMax.x/4)*fOffsetMultiplier*3,(vMax.y/4)*3,0.0>>)
			vBackofBike = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viLightBike,<<(vMax.x/4)*fOffsetMultiplier*3,-(vMax.y/4)*3,0.0>>) 
			vPedChest = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viLightBike,<<(vMax.x/2)*fOffsetMultiplier,0.0,(vMax.z/4)*3>>)
		ELSE
			vFrontofBike = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viLightBike,<<0.0,vMax.y/2,0.0>>)
			vBackofBike = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viLightBike,<<0.0,-(vMax.y/2),0.0>>) 
			vPedChest = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viLightBike,<<0.0,0.0,(vMax.z/4)*3>>)
		ENDIF
	ELSE
		EXIT
	ENDIF
	
	INT i
	FOR i = 0 TO NUM_COLLISION_BOXES - 1
		CHECK_COLLISION_BOX_FOR_CLOSE_SEGMENTS(LGHTCYCLE_PLAYER[iPlayer].CollisionBox[i], iPlayer, vFrontofBike, SEGMENT, iTempSegments, bSegmentsToCheck, bIsPlayer)
	ENDFOR
	CHECK_COLLISION_BOX_FOR_CLOSE_SEGMENTS(LGHTCYCLE_PLAYER[iPlayer].EndCollisionBox, iPlayer, vFrontofBike, SEGMENT, iTempSegments, bSegmentsToCheck, bIsPlayer)
	
	//Check for the closest segment if we have any to check
	IF bSegmentsToCheck

		FLOAT fTempDistance
		BOOL bFoundPoint
		
		//Find the closest point of the closest segments
		FOR iTempSegments2 = 0 TO iTempSegments
			fTempDistance = VDIST2(vFrontofBike,LGHTCYCLE_PLAYER[iPlayer].LGHTCYCLE_SEGMENT[SEGMENT[iTempSegments2]].vPointTop)
			IF fTempDistance < fClosestDistance
				fClosestDistance = fTempDistance
				iClosestPoint = SEGMENT[iTempSegments2]
				bFoundPoint = TRUE
			ENDIF
		ENDFOR
		
		//Check for actual colision if we have a point close enough
		IF bFoundPoint
			
			IF CAN_SEGMENT_BE_CHECKED(iPlayer, iClosestPoint, bIsPlayer)
				
				iClosestPoint = GET_NEXT_POINT_BACK_IN_LINE(iPlayer, iClosestPoint)
				
				PRINTLN("[LH][PRONCOL] iClosestPoint = ", iClosestPoint)
				
				FLOAT fDistToLine = VDIST2(LGHTCYCLE_PLAYER[iPlayer].LGHTCYCLE_SEGMENT[iClosestPoint].vPointTop, vFrontofBike)
				PRINTLN("[LH][PRONCOL] fDistToLine Bike = ", fDistToLine)
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciPRON_MODE_PROXIMITY_VIBRATION)
					PROCESS_PRON_COLLISION_VIBRATION(fDistToLine)
				ENDIF
				
				BOOL bNextLastCanCheck
				BOOL bIsPlayerWithinSegment = IS_THE_BIKE_WITHIN_THE_SEGMENT_LIMITS(iPlayer, iClosestPoint, vFrontofBike, vBackofBike, vPedChest, bNextLastCanCheck)
				
				IF bIsPlayerWithinSegment
					IF HAS_POINT_CROSSED_THE_PRON_LINE(iPlayer, iClosestPoint, vFrontofBike, vFrontofBike+vNormalVelocity)
						PRINTLN("[LH][PRONCOL] Bike blowing up! Front of bike velocity")
						COLLIDE_WITH_LINE(iPlayer, viLightBike, CurrentPlayerId, bIsPlayer)
					ELIF HAS_POINT_CROSSED_THE_PRON_LINE(iPlayer, iClosestPoint, vPedChest, vPedChest+vNormalVelocity)
						PRINTLN("[LH][PRONCOL] Bike blowing up! Ped velocity ")
						COLLIDE_WITH_LINE(iPlayer, viLightBike, CurrentPlayerId,bIsPlayer)
					ELIF HAS_POINT_CROSSED_THE_PRON_LINE(iPlayer, iClosestPoint, vBackofBike, vBackofBike+vNormalVelocity)
						PRINTLN("[LH][PRONCOL] Bike blowing up! Back of bike velocity")
						COLLIDE_WITH_LINE(iPlayer, viLightBike, CurrentPlayerId)
					ELIF HAS_POINT_CROSSED_THE_PRON_LINE(iPlayer, iClosestPoint, vFrontofBike, vBackofBike)
						PRINTLN("[LH][PRONCOL] Bike blowing up! Between back and front wheel")
						COLLIDE_WITH_LINE(iPlayer, viLightBike, CurrentPlayerId,bIsPlayer)
					ELIF bNextLastCanCheck AND HAS_POINT_CROSSED_THE_PRON_LINE(iPlayer, iClosestPoint, GET_ENTITY_COORDS(viLightbike), vLastFramePos)
						PRINTLN("[LH][PRONCOL] Bike blowing up! Between old and new positions")
						COLLIDE_WITH_LINE(iPlayer, viLightBike, CurrentPlayerId,bIsPlayer)
					ENDIF
					
					IF bIsPlayer
						VECTOR vFrontofBikeLeft = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viLightBike,<<-vMax.x/2.8,vMax.y/2,0.0>>)
						VECTOR vFrontofBikeRight = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viLightBike,<<vMax.x/2.8,vMax.y/2,0.0>>)
						IF HAS_POINT_CROSSED_THE_PRON_LINE(iPlayer, iClosestPoint, vFrontofBikeLeft, vFrontofBikeRight)
							PRINTLN("[LH][PRONCOL] Bike blowing up! Bike Left/Right")
							COLLIDE_WITH_LINE(iPlayer, viLightBike, CurrentPlayerId,bIsPlayer)
						ENDIF
					ELIF fOffsetMultiplier = 1.0
						VECTOR vCarLeft = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viLightBike,<<-((vMax.x/4)*3),vMax.y/2,0.0>>)
						VECTOR vCarRight = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viLightBike,<<(vMax.x/4)*3,vMax.y/2,0.0>>)
						IF HAS_POINT_CROSSED_THE_PRON_LINE(iPlayer, iClosestPoint, vCarLeft, vCarRight)
							PRINTLN("[LH][PRONCOL] Car blowing up! Car Left/Right")
							COLLIDE_WITH_LINE(iPlayer, viLightBike, CurrentPlayerId,bIsPlayer)
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
		CLEAR_BIT(iLocalPronBitSet,ciALREADY_COLLIDED_THIS_PLAYER)
	ENDIF
	
ENDPROC

PROC PRON_STATIONARY_CHECK(VEHICLE_INDEX viLightBike)

	IF NOT HAS_NET_TIMER_STARTED(tdPronStationaryTimer)
		REINIT_NET_TIMER(tdPronStationaryTimer)
		fPronExplodeProgress = 0
	ENDIF
	
	IF IS_BIT_SET(iLocalPronBitSet, ciWARNING_SCREEN_IS_ACTIVE)
	AND NOT IS_WARNING_MESSAGE_ACTIVE()
		IF iWarningMessageCount < 2
			REINIT_NET_TIMER(tdPronStationaryTimer)
			fPronExplodeProgress = 0
			iWarningMessageCount++
		ENDIF
		CLEAR_BIT(iLocalPronBitSet, ciWARNING_SCREEN_IS_ACTIVE)
	ENDIF
	
	FLOAT fBikeMag = ABSF(VMAG(GET_ENTITY_VELOCITY(viLightBike)))
	//DRAW_DEBUG_TEXT(GET_STRING_FROM_FLOAT(fBikeMag), GET_ENTITY_COORDS(viLightBike), 255)
	PRINTLN("[KH][PRON] Bike Magnitude = ", fBikeMag)
	IF fBikeMag < fMagnitudeLimit
		
		fPronExplodeProgress = CLAMP( fPronExplodeProgress + (fLastFrameTime / TO_FLOAT(5) * 100), 0, 100)
		SET_CONTROL_SHAKE(PLAYER_CONTROL, 130, ROUND(fPronExplodeProgress + 80))
		
		INT iStoppedTimeLimit = iMagnitudeTimer
		
		IF IS_WARNING_MESSAGE_ACTIVE()
			SET_BIT(iLocalPronBitSet, ciWARNING_SCREEN_IS_ACTIVE)
			iStoppedTimeLimit = 10000
		ENDIF
		
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdPronStationaryTimer) > iStoppedTimeLimit
			PRINTLN("[KH][PRON] Exploding for not moving")
			IF NETWORK_HAS_CONTROL_OF_ENTITY(LocalPlayerPed)
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA(g_FMMC_STRUCT.iAdversaryModeType)
					IF NOT NETWORK_IS_LOCAL_PLAYER_INVINCIBLE()
						SET_ENTITY_INVINCIBLE(LocalPlayerPed, FALSE)
					ENDIF
				ELSE
					SET_ENTITY_INVINCIBLE(LocalPlayerPed, FALSE)
				ENDIF
				PRINTLN("[VV] - PRON_STATIONARY_CHECK - Setting invincibilty off ")
			ELSE
				NETWORK_REQUEST_CONTROL_OF_ENTITY(LocalPlayerPed)
			ENDIF
			IF NETWORK_HAS_CONTROL_OF_ENTITY(viLightBike)
				SET_ENTITY_INVINCIBLE(viLightBike, FALSE)
			ELSE
				NETWORK_REQUEST_CONTROL_OF_ENTITY(viLightBike)
			ENDIF
			SET_BIT(iLocalPronBitSet, ciNEEDS_TO_START_STARTUP)
			NETWORK_EXPLODE_VEHICLE(viLightBike, DEFAULT, DEFAULT, NATIVE_TO_INT(LocalPlayer))
			START_NET_TIMER(vehicleCleanUpTimer)
			IF NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(viLightBike)	
				netVehicleToCleanUp = VEH_TO_NET(viLightBike)
			ENDIF
			SET_KILL_STRIP_OVER_RIDE_PLAYER_ID(LocalPlayer)
		ENDIF
	ELSE
		REINIT_NET_TIMER(tdPronStationaryTimer)
		fPronExplodeProgress = 0
	ENDIF
	
ENDPROC

PROC SETUP_PRON_LINE_BASED_ON_BIKE(INT iPlayer)

	MODEL_NAMES BikeModel = GET_ENTITY_MODEL(LGHTCYCLE_PLAYER[iPlayer].LightBike)
	SWITCH BikeModel
		
		CASE BATI
		CASE BATI2
			
			fWheelOffset = 0.192
			fWheelRadius = 0.342
			fLineDistanceOffset = -0.972
			fLineHeight = 0.623
			Sy = 0.0
			Sz = 0.380
			
		BREAK
			
		CASE DAEMON
			
			fWheelOffset = 0.135
			fWheelRadius = 0.315
			fLineDistanceOffset = -0.890
			fLineHeight = 0.250
			Sy = -0.272
			Sz = 0.108
			
		BREAK
		
		CASE FAGGIO2
			
			fWheelOffset = 0.135
			fWheelRadius = 0.204
			fLineDistanceOffset = -0.682
			fLineHeight = 0.300
			Sy = -0.308
			Sz = 0.000
			
		BREAK
			
		CASE HEXER
		
			fWheelOffset = 0.081
			fWheelRadius = 0.300
			fLineDistanceOffset = -0.828
			fLineHeight = 0.214
			Sy = -0.256
			Sz = -0.108
			
		BREAK
		
		CASE NEMESIS
		CASE SANCHEZ
			
			fWheelOffset = 0.192
			fWheelRadius = 0.300
			fLineDistanceOffset = -0.846
			fLineHeight = 0.436
			Sy = 0.036
			Sz = 0.200
			
		BREAK
		
		CASE VADER
		CASE THRUST
			
			fWheelOffset = 0.231
			fWheelRadius = 0.300
			fLineDistanceOffset = -1.010
			fLineHeight = 0.436
			Sy = -0.092
			Sz = 0.092
			
		BREAK
		
		CASE SOVEREIGN
			
			fWheelOffset = 0.231
			fWheelRadius = 0.300
			fLineDistanceOffset = -1.010
			fLineHeight = 0.459
			Sy = -0.256
			Sz = -0.020
			
		BREAK
		
		CASE ENDURO
			
			fWheelOffset = 0.231
			fWheelRadius = 0.315
			fLineDistanceOffset = -0.964
			fLineHeight = 0.391
			Sy = 0.000
			Sz = 0.072
			
		BREAK
		
		CASE INNOVATION
		
			fWheelOffset = 0.135
			fWheelRadius = 0.300
			fLineDistanceOffset = -0.872
			fLineHeight = 0.232
			Sy = 0.000
			Sz = -0.020
			
		BREAK
		
		CASE HAKUCHOU
			
			fWheelOffset = 0.135
			fWheelRadius = 0.300
			fLineDistanceOffset = -0.872
			fLineHeight = 0.382
			Sy = 0.000
			Sz = -0.020
			
		BREAK
		
		CASE LECTRO
			
			fWheelOffset = 0.000
			fWheelRadius = 0.300
			fLineDistanceOffset = -0.710
			fLineHeight = 0.482
			Sy = 0.000
			Sz = -0.020
			
		BREAK
		
		CASE VINDICATOR
			
			fWheelOffset = 0.096
			fWheelRadius = 0.327
			fLineDistanceOffset = -0.890
			fLineHeight = 0.418
			Sy = -0.308
			Sz = 0.180
			
		BREAK
		
		CASE AKUMA
			
			fWheelOffset = 0.150
			fWheelRadius = 0.315
			fLineDistanceOffset = -0.836
			fLineHeight = 0.400
			Sy = 0.000
			Sz = 0.128
			
		BREAK
		
		CASE BF400
			
			fWheelOffset = 0.150
			fWheelRadius = 0.315
			fLineDistanceOffset = -0.836
			fLineHeight = 0.427
			Sy = 0.000
			Sz = 0.128
			
		BREAK
		
		CASE GARGOYLE
		CASE CLIFFHANGER
			
			fWheelOffset = 0.150
			fWheelRadius = 0.315
			fLineDistanceOffset = -0.836
			fLineHeight = 0.264
			Sy = -0.144
			Sz = -0.072
			
		BREAK
		
		CASE ESSKEY
			
			fWheelOffset = 0.081
			fWheelRadius = 0.315
			fLineDistanceOffset = -0.610
			fLineHeight = 0.341
			Sy = -0.092
			Sz = 0.236
			
		BREAK
		
		CASE NIGHTBLADE
			
			fWheelOffset = 0.108
			fWheelRadius = 0.315
			fLineDistanceOffset = -0.982
			fLineHeight = 0.159
			Sy = -0.328
			Sz = -0.056
			
		BREAK
		
		CASE DEFILER
			
			fWheelOffset = 0.000
			fWheelRadius = 0.315
			fLineDistanceOffset = -0.672
			fLineHeight = 0.386
			Sy = -0.036
			Sz = 0.144
			
		BREAK
		
		CASE AVARUS
			
			fWheelOffset = 0.000
			fWheelRadius = 0.315
			fLineDistanceOffset = -0.754
			fLineHeight = 0.182
			Sy = -0.308
			Sz = -0.056
			
		BREAK
		
		CASE ZOMBIEA
			
			fWheelOffset = 0.042
			fWheelRadius = 0.315
			fLineDistanceOffset = -0.790
			fLineHeight = 0.177
			Sy = -0.308
			Sz = -0.056
			
		BREAK
				
		CASE ZOMBIEB
			
			fWheelOffset = 0.150
			fWheelRadius = 0.315
			fLineDistanceOffset = -0.890
			fLineHeight = 0.205
			Sy = -0.308
			Sz = -0.056
			
		BREAK
		
		CASE RATBIKE
			
			fWheelOffset = 0.150
			fWheelRadius = 0.315
			fLineDistanceOffset = -0.854
			fLineHeight = 0.227
			Sy = -0.308
			Sz = -0.056
			
		BREAK
		
		CASE DAEMON2
			
			fWheelOffset = 0.150
			fWheelRadius = 0.315
			fLineDistanceOffset = -0.854
			fLineHeight = 0.250
			Sy = -0.308
			Sz = -0.056
			
		BREAK
		
	ENDSWITCH

	IF BikeModel = INT_TO_ENUM(MODEL_NAMES, HASH("SHOTARO"))
			
		fWheelOffset = 0.000
		fWheelRadius = 0.315
		fLineDistanceOffset = -0.764
		fLineHeight = 0.423
		Sy = -0.328
		Sz = 0.220
			
	ENDIF
		

ENDPROC

FUNC INT GET_RANDOM_PRON_BIKE_HORN()
	INT iReturnHorn = GET_RANDOM_INT_IN_RANGE(0,8)
	PRINTLN("[KH] GET_RANDOM_PRON_BIKE_HORN - Getting random horn: ", iReturnHorn)
	SWITCH iReturnHorn
		CASE 0
			RETURN HASH("DLC_Biker_Trailblazers_Horns_00")
		CASE 1
			RETURN HASH("DLC_Biker_Trailblazers_Horns_01")
		CASE 2
			RETURN HASH("DLC_Biker_Trailblazers_Horns_02")
		CASE 3
			RETURN HASH("DLC_Biker_Trailblazers_Horns_03")
		CASE 4
			RETURN HASH("DLC_Biker_Trailblazers_Horns_04")
		CASE 5
			RETURN HASH("DLC_Biker_Trailblazers_Horns_05")
		CASE 6
			RETURN HASH("DLC_Biker_Trailblazers_Horns_06")
		CASE 7
			RETURN HASH("DLC_Biker_Trailblazers_Horns_07")
	ENDSWITCH
	
	PRINTLN("[KH] GET_RANDOM_PRON_BIKE_HORN - Didn't return a correct value...")
	RETURN 0
ENDFUNC

PROC SET_PRON_PLAYER_ALPHA(PED_INDEX CurrentPlayerPed, INT iAlpha, INT iPlayer)

	SET_ENTITY_ALPHA(CurrentPlayerPed,iAlpha,FALSE)
	IF DOES_ENTITY_EXIST(LGHTCYCLE_PLAYER[iPlayer].LightBike)
		SET_ENTITY_ALPHA(LGHTCYCLE_PLAYER[iPlayer].LightBike,iAlpha,FALSE)
	ENDIF
	
	PRINTLN("[PRON][RESPAWNING]SET_PRON_PLAYER_ALPHA iAlpha : ", iAlpha)
ENDPROC

PROC RESET_PRON_PLAYER_ALPHA(PLAYER_INDEX CurrentPlayerId, INT iPlayer)

	PED_INDEX CurrentPlayerPed = GET_PLAYER_PED(CurrentPlayerId)
	
	RESET_ENTITY_ALPHA(CurrentPlayerPed)
	IF DOES_ENTITY_EXIST(LGHTCYCLE_PLAYER[iPlayer].LightBike)
		RESET_ENTITY_ALPHA(LGHTCYCLE_PLAYER[iPlayer].LightBike)
	ENDIF
	PRINTLN("[PRON][RESPAWNING] RESET_PRON_PLAYER_ALPHA")
ENDPROC


PROC FLASH_PRON_PLAYER_ALPHA(PLAYER_INDEX CurrentPlayerId, INT iPlayer)

	PED_INDEX CurrentPlayerPed = GET_PLAYER_PED(CurrentPlayerId)
	
	IF LGHTCYCLE_PLAYER[iPlayer].iAlphaLoopCounter%2 = 0
		LGHTCYCLE_PLAYER[iPlayer].iAlpha -= ((10+(LGHTCYCLE_PLAYER[iPlayer].iAlphaLoopCounter*2))/4)*5
	ELSE
		LGHTCYCLE_PLAYER[iPlayer].iAlpha += ((10+(LGHTCYCLE_PLAYER[iPlayer].iAlphaLoopCounter*2))/4)*5
	ENDIF
	
	IF LGHTCYCLE_PLAYER[iPlayer].iAlpha >= 255
		LGHTCYCLE_PLAYER[iPlayer].iAlpha = 255
		LGHTCYCLE_PLAYER[iPlayer].iAlphaLoopCounter++
	ELIF LGHTCYCLE_PLAYER[iPlayer].iAlpha <= 50
		LGHTCYCLE_PLAYER[iPlayer].iAlpha = 50
		LGHTCYCLE_PLAYER[iPlayer].iAlphaLoopCounter++
	ENDIF
	
	SET_PRON_PLAYER_ALPHA(CurrentPlayerPed, LGHTCYCLE_PLAYER[iPlayer].iAlpha, iPlayer)

	PRINTLN("[PRON][RESPAWNING]FLASH_PRON_PLAYER_ALPHA   : ", LGHTCYCLE_PLAYER[iPlayer].iAlpha)
	
ENDPROC

PROC CLEANUP_PRON_VFX(INT iPlayer)
	IF DOES_PARTICLE_FX_LOOPED_EXIST(LGHTCYCLE_PLAYER[iPlayer].ptfxPronTrail)
		STOP_PARTICLE_FX_LOOPED(LGHTCYCLE_PLAYER[iPlayer].ptfxPronTrail)
	ENDIF
ENDPROC

PROC HANDLE_PRON_VFX(INT iPlayer, PLAYER_INDEX CurrentPlayerID)

	FLOAT fEvoRatio
	FLOAT fSpeed
	INT   iMaxSpeed

	SETUP_PRON_PLAYER_COLOUR(iPlayer)
	
	PED_INDEX PlayerPed = GET_PLAYER_PED(CurrentPlayerId)

	FLOAT fR
	FLOAT fG
	FLOAT fB
	
	IF DOES_ENTITY_EXIST(LGHTCYCLE_PLAYER[iPlayer].LightBike)
		REQUEST_NAMED_PTFX_ASSET("scr_bike_adversary")
		IF HAS_NAMED_PTFX_ASSET_LOADED("scr_bike_adversary")		
			//USE_PARTICLE_FX_ASSET("scr_bike_adversary")
			
			iMaxSpeed = ROUND(GET_VEHICLE_ESTIMATED_MAX_SPEED(LGHTCYCLE_PLAYER[iPlayer].LightBike))
			
			fSpeed = GET_ENTITY_SPEED(LGHTCYCLE_PLAYER[iPlayer].LightBike)
			
			fEvoRatio =  fSpeed / iMaxSpeed
			
			IF fEvoRatio > 1
				fEvoRatio = 1
			ENDIF		
			
			fR = TO_FLOAT(iTrailR)
			fG = TO_FLOAT(iTrailG)
			fB = TO_FLOAT(iTrailB)
			
			fR = fR / 255
			fG = fG / 255
			fB = fB / 255
			
			IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(LGHTCYCLE_PLAYER[iPlayer].ptfxPronTrail)
				USE_PARTICLE_FX_ASSET("scr_bike_adversary")
				LGHTCYCLE_PLAYER[iPlayer].ptfxPronTrail = START_PARTICLE_FX_LOOPED_ON_ENTITY_BONE("scr_adversary_trail_lightning", PlayerPed, <<0,0,0>>, <<0,0,0>> , GET_ENTITY_BONE_INDEX_BY_NAME(PlayerPed,"SKEL_ROOT"))
				PRINTLN("[PRON][PTFX]HANDLE_PRON_VFX Colour - fR",fR)
				PRINTLN("[PRON][PTFX]HANDLE_PRON_VFX Colour - fG",fG)
				PRINTLN("[PRON][PTFX]HANDLE_PRON_VFX Colour - fB",fB)
			ELSE
				PRINTLN("[PRON][PTFX]HANDLE_PRON_VFX fEvoRatio",fEvoRatio)
				SET_PARTICLE_FX_LOOPED_EVOLUTION(LGHTCYCLE_PLAYER[iPlayer].ptfxPronTrail, "speed", fEvoRatio, TRUE)
				SET_PARTICLE_FX_LOOPED_COLOUR(LGHTCYCLE_PLAYER[iPlayer].ptfxPronTrail, fR , fG , fB, TRUE)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_STARTUP_VIBRATION(INT iPlayer, PLAYER_INDEX CurrentPlayerID)

	INT iTimer = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdBikeStartupTimer)
	
	IF (iTimer > STARTUP_TIME - 2100 AND iTimer < STARTUP_TIME - 2000)
	OR (iTimer > STARTUP_TIME - 1100 AND iTimer < STARTUP_TIME - 1000)
	OR (iTimer > STARTUP_TIME - 500 AND iTimer < STARTUP_TIME - 400)
	OR (iTimer > STARTUP_TIME - 250 AND iTimer < STARTUP_TIME - 200)
	OR (iTimer > STARTUP_TIME - 150 AND iTimer < STARTUP_TIME - 100)
		SET_CONTROL_SHAKE(PLAYER_CONTROL, 130, 50)
		HANDLE_PRON_VFX(iPlayer, CurrentPlayerID)
	ELIF iTimer < STARTUP_TIME
		CLEANUP_PRON_VFX(iPlayer)
	ELIF iTimer > STARTUP_TIME AND iTimer < STARTUP_TIME + 2000
	AND iStartupVibration >= 0
		SET_CONTROL_SHAKE(PLAYER_CONTROL, 130, iStartupVibration)
		iStartupVibration -= 10
	ENDIF

ENDPROC



PROC CREATE_AND_DRAW_PRON_LINE(INT iPlayer, PLAYER_INDEX CurrentPlayerID)
	
	BOOL bLocalPlayer
	
	//Do Local Checks
	IF CurrentPlayerID = LocalPlayer
		//Get the player's velocity
		vNormalVelocity = GET_ENTITY_VELOCITY(LGHTCYCLE_PLAYER[iPlayer].LightBike)*GET_FRAME_TIME()
		
		HANDLE_PRON_AUDIO()
		
		bLocalPlayer = TRUE
		
		IF NOT IS_BIT_SET(MC_serverBD_3.iPronPlayerIsDrawingLines, iPlayer)
			IF iBroadcastOnTime = 0
			OR (GET_GAME_TIMER() - iBroadcastOnTime) >= 250
				BROADCAST_PRON_PLAYERS_DRAWING_TRAILS( iPlayer, TRUE )
				iBroadcastOnTime = GET_GAME_TIMER()
			ENDIF
		ELSE
			iBroadcastOnTime = 0
		ENDIF
	ELSE
		//Clean up for non player bikes - local is done when "crash" is played
		IF DOES_ENTITY_EXIST(LGHTCYCLE_PLAYER[iPlayer].LightBike)
			IF NOT IS_VEHICLE_DRIVEABLE(LGHTCYCLE_PLAYER[iPlayer].LightBike)
				CLEANUP_BIKE_AUDIO(iPlayer)
			ENDIF
		ENDIF
	ENDIF
	
	//Set the offset points for the bike to draw from
	SETUP_PRON_LINE_BASED_ON_BIKE(iPlayer)
	
	//Set initial segment points
	IF IS_VECTOR_ZERO(LGHTCYCLE_PLAYER[iPlayer].vLastPoint)
		SETUP_PRON_PLAYER(iPlayer)
	ENDIF
	
	//Get the latest position for the points on the line
	CREATE_LATEST_PRON_LINE(iPlayer)
	
	//Check if a point should be dropped
	DROP_PRON_LINE_POINT(iPlayer)
	
	//Draw the lines 
	DRAW_POLY_PRON_LINES(iPlayer, bLocalPlayer)
	
	//Add the VFX
	HANDLE_PRON_VFX(iPlayer, CurrentPlayerID)
	
ENDPROC

PROC PROCESS_LIGHTBIKE_STARTUP(INT iPlayer, PLAYER_INDEX CurrentPlayerID)

	//Track how long the player has been invincible for the local player
	IF CurrentPlayerID = LocalPlayer
		INT iTimer = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdBikeStartupTimer)
		IF iTimer > STARTUP_TIME
		OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciUSE_PRON_POWERUP_LOGIC)
			IF iBroadcastOnTime = 0
			OR (GET_GAME_TIMER() - iBroadcastOnTime) >= 250
				BROADCAST_PRON_PLAYERS_DRAWING_TRAILS( iPlayer, TRUE )
				iBroadcastOnTime = GET_GAME_TIMER()
			ENDIF
		ENDIF
	ENDIF
	
	//Handle the flashing and spluttering of the trail powering up
	IF NOT IS_BIT_SET(MC_serverBD_3.iPronPlayerIsDrawingLines, iPlayer)
		IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_EVERYONE_RUNNING)
			FLASH_PRON_PLAYER_ALPHA(CurrentPlayerID, iPlayer)
		ENDIF
	ELSE
		iBroadcastOnTime = 0
		RESET_PRON_PLAYER_ALPHA(CurrentPlayerID, iPlayer)
		IF CurrentPlayerID = LocalPlayer
			CLEAR_BIT(iLocalPronBitSet, ciLOCAL_PLAYER_IS_INVULNERABLE)
		ENDIF
		LGHTCYCLE_PLAYER[iPlayer].bIsStartingUp = FALSE
	ENDIF

ENDPROC

PROC HANDLE_PRON_LIGHTBIKE(INT iPlayer, PLAYER_INDEX CurrentPlayerID)

	VEHICLE_INDEX viLocalPlayerLightBike
	
	IF DOES_ENTITY_EXIST(LocalPlayerPed)
		IF NOT IS_PED_DEAD_OR_DYING(LocalPlayerPed)
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				viLocalPlayerLightBike = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
				
				IF IS_VEHICLE_DRIVEABLE(viLocalPlayerLightBike)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(viLocalPlayerLightBike)
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciUSE_PRON_POWERUP_LOGIC)
							SET_ENTITY_INVINCIBLE(viLocalPlayerLightBike, TRUE)
						ENDIF
						
						IF NOT GET_IS_VEHICLE_ENGINE_RUNNING(viLocalPlayerLightBike)
							SET_VEHICLE_ENGINE_ON(viLocalPlayerLightBike, TRUE, TRUE)
						ENDIF
					ELSE
						NETWORK_REQUEST_CONTROL_OF_ENTITY(viLocalPlayerLightBike)
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
	
	IF LGHTCYCLE_PLAYER[iPlayer].bIsStartingUp
	
		PROCESS_LIGHTBIKE_STARTUP(iPlayer, CurrentPlayerID)
	
	ELSE
	
		CREATE_AND_DRAW_PRON_LINE(iPlayer, CurrentPlayerID)
		
	ENDIF
	
	IF DOES_ENTITY_EXIST(LocalPlayerPed)
		IF NOT IS_PED_DEAD_OR_DYING(LocalPlayerPed)
		AND GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iRespawnState = RESPAWN_STATE_PLAYING
		
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				viLocalPlayerLightBike = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
			ELSE
				CLEANUP_PRON_VFX(iPlayer)
			ENDIF
			FLOAT fMinSpeed
			
			IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
				//Limit the local player's speed
				IF CurrentPlayerId = localPlayer
					IF DOES_ENTITY_EXIST(viLocalPlayerLightBike)
						IF IS_VEHICLE_DRIVEABLE(viLocalPlayerLightBike)
							IF NOT IS_ENTITY_IN_AIR(viLocalPlayerLightBike)
								IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
								OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType) AND TINY_RACERS_GET_ROUND_STATE(iLocalPart) = TR_Racing
								
									IF IS_BIT_SET(iLocalPronBitSet, ciBLOW_UP_IF_STATIONARY)
									AND NOT LGHTCYCLE_PLAYER[iPlayer].bIsStartingUp
									AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciUSE_PRON_POWERUP_LOGIC)
									AND fLOCKVEHSPEED > 0
										PRON_STATIONARY_CHECK(viLocalPlayerLightBike)
									ENDIF
									IF IS_BIT_SET(iLocalPronBitSet, ciLOCAL_PLAYER_IS_INVULNERABLE)
										FLOAT fSpeedIncrease = (fLOCKVEHSPEED/2)/STARTUP_TIME
										IF fMinSpeed < fLOCKVEHSPEED
											fMinSpeed = (fLOCKVEHSPEED/2 + (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdBikeStartupTimer)*fSpeedIncrease))
										ELSE
											fMinSpeed = fLOCKVEHSPEED
										ENDIF
									ELSE
										fMinSpeed = fLOCKVEHSPEED
									ENDIF
									
									//Stops the use of the brake if they are below this speed because we don't want them sticking to the inside of tubes...
									IF GET_ENTITY_SPEED(viLocalPlayerLightBike) < fLOCKVEHSPEED + 1
										IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA(g_FMMC_STRUCT.iAdversaryModeType)
											IF IS_BIT_SET(MC_playerBD[iPartToUse].iVehicleWeaponEffectActiveBS, ciVEH_WEP_FLIPPED_CONTROLS)
												DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
											ELSE
												DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
											ENDIF
										ELSE
											DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
										ENDIF
									ENDIF
									IF NOT IS_WARNING_MESSAGE_ACTIVE()
										IF GET_ENTITY_SPEED(viLocalPlayerLightBike) < fMinSpeed
											IF HAS_NET_TIMER_STARTED(tdBikeLandedTimer)
												IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdBikeLandedTimer) > ciVEHICLE_ACCELERATE_DELAY
													IF IS_VEHICLE_DRIVEABLE(viLocalPlayerLightBike)
														SET_VEHICLE_FORWARD_SPEED_XY(viLocalPlayerLightBike,fMinSpeed)
													ENDIF
												ENDIF
											ELSE
												START_NET_TIMER(tdBikeLandedTimer)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF HAS_NET_TIMER_STARTED(tdBikeLandedTimer)
									RESET_NET_TIMER(tdBikeLandedTimer)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisableShallowWaterBikeJumpOutThisFrame, TRUE)
					
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciUSE_PRON_POWERUP_LOGIC)
						PROCESS_STARTUP_VIBRATION(iPlayer, CurrentPlayerID)
					ENDIF
						
					IF IS_PLAYSTATION_PLATFORM()
						SET_CONTROL_LIGHT_EFFECT_COLOR(PLAYER_CONTROL, iTrailR, iTrailG, iTrailB)
						SET_BIT(iLocalBoolCheck13, LBOOL13_CONTROL_PAD_COLOUR_SHOULD_BE_RESET) // Resets the pad colour in PROCESS_PRE_MAIN_CHECKS (it's already done in MC_SCRIPT_CLEANUP)
					ENDIF
						
					IF IS_BIT_SET(iLocalPronBitSet, ciUSE_COLLISION)
					AND NOT IS_BIT_SET(iLocalPronBitSet, ciLOCAL_PLAYER_IS_INVULNERABLE)
						//Check if the player has run into the line
						CHECK_PRON_LINE_COLLISION(iPlayer, viLocalPlayerLightBike, TRUE, CurrentPlayerID)
					ENDIF
				ELSE
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciUSE_PRON_POWERUP_LOGIC)
						IF MC_PlayerBD[iPartToUse].iTeam != iPlayer
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven , ciENABLE_VEHICLE_ENABLE_PRON_MOVEMENT)
								
								IF DOES_ENTITY_EXIST(viLocalPlayerLightBike)
									IF IS_VEHICLE_DRIVEABLE(viLocalPlayerLightBike)
										IF NOT IS_ENTITY_IN_AIR(viLocalPlayerLightBike)
											fMinSpeed = fLOCKVEHSPEED/2
											//Stops the use of the brake if they are below this speed because we don't want them sticking to the inside of tubes...
											IF NOT IS_WARNING_MESSAGE_ACTIVE()
												IF GET_ENTITY_SPEED(viLocalPlayerLightBike) < fMinSpeed
													IF IS_BIT_SET(MC_playerBD[iPartToUse].iVehicleWeaponEffectActiveBS, ciVEH_WEP_FLIPPED_CONTROLS)
														DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
													ELSE
														DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
													ENDIF
													
													//PRON_STATIONARY_CHECK(viLocalPlayerLightBike)
													IF HAS_NET_TIMER_STARTED(tdBikeLandedTimer)
														IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdBikeLandedTimer) > ciVEHICLE_ACCELERATE_DELAY
															IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA(g_FMMC_STRUCT.iAdversaryModeType)
																IF IS_VEHICLE_ON_ALL_WHEELS(viLocalPlayerLightBike)
																	IF IS_VEHICLE_DRIVEABLE(viLocalPlayerLightBike)
																		SET_BIT(iLocalBoolCheck21, LBOOL21_PRON_PUSH_FORWARD_ACTIVE)
																		SET_VEHICLE_FORWARD_SPEED_XY(viLocalPlayerLightBike,fMinSpeed)
																	ENDIF
																ENDIF
															ELSE
																IF IS_VEHICLE_DRIVEABLE(viLocalPlayerLightBike)
																	SET_VEHICLE_FORWARD_SPEED_XY(viLocalPlayerLightBike,fMinSpeed)
																ENDIF
															ENDIF
														ENDIF
													ELSE
														START_NET_TIMER(tdBikeLandedTimer)
													ENDIF
												ENDIF
											ENDIF
										ELSE
											IF HAS_NET_TIMER_STARTED(tdBikeLandedTimer)
												RESET_NET_TIMER(tdBikeLandedTimer)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							IF DOES_ENTITY_EXIST(viLocalPlayerLightBike)
							AND NOT IS_BIT_SET(MC_playerBD[iPartToUse].iVehicleWeaponEffectActiveBS, ciVEH_WEP_GHOST)
								CHECK_PRON_LINE_COLLISION(iPlayer, viLocalPlayerLightBike, FALSE, CurrentPlayerID, -1.0)
								CHECK_PRON_LINE_COLLISION(iPlayer, viLocalPlayerLightBike, FALSE, CurrentPlayerID, 1.0)
							ENDIF
						ENDIF
					ELIF IS_BIT_SET(iLocalPronBitSet, ciUSE_COLLISION)
						IF NOT IS_BIT_SET(iLocalPronBitSet, ciLOCAL_PLAYER_IS_INVULNERABLE)
							//Check if the player has run into the line
							CHECK_PRON_LINE_COLLISION(iPlayer, viLocalPlayerLightBike, FALSE, CurrentPlayerID)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC CLEAR_PRON_LINES(INT iPlayer, PLAYER_INDEX CurrentPlayerId)
	PRINTLN("[PRON][RESPAWNING] CLEAR_PRON_LINES")
	
	IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA(g_FMMC_STRUCT.iAdversaryModeType)
		RESET_PRON_PLAYER_ALPHA(CurrentPlayerId, iPlayer)
	ENDIF
	
	LGHTCYCLE_PLAYER[iPlayer].vCurrentPos = <<0.0, 0.0, 0.0>>
	LGHTCYCLE_PLAYER[iPlayer].vLastPoint = <<0.0, 0.0, 0.0>>
	LGHTCYCLE_PLAYER[iPlayer].vEndTop = <<0.0, 0.0, 0.0>>
	LGHTCYCLE_PLAYER[iPlayer].vEndBottom = <<0.0, 0.0, 0.0>>
	LGHTCYCLE_PLAYER[iPlayer].iCurrentNumberOfSegments = 2
	LGHTCYCLE_PLAYER[iPlayer].fDistanceFromLastPoint = 0.0
	LGHTCYCLE_PLAYER[iPlayer].iFrontOfLine = 1
	
	INT iLoop
	FOR iLoop = 0 TO LGHTCYCLE_PLAYER[iPlayer].iCurrentNumberOfSegments-1
		LGHTCYCLE_PLAYER[iPlayer].LGHTCYCLE_SEGMENT[iLoop].vPointTop = <<0.0, 0.0, 0.0>>
		LGHTCYCLE_PLAYER[iPlayer].LGHTCYCLE_SEGMENT[iLoop].vPointBottom = <<0.0, 0.0, 0.0>>
	ENDFOR
	
	LGHTCYCLE_PLAYER[iPlayer].iAlphaLoopCounter = 1
	LGHTCYCLE_PLAYER[iPlayer].iAlpha = 50
	
	LGHTCYCLE_PLAYER[iPlayer].iCurrentCollisionBox = 0
	LGHTCYCLE_PLAYER[iPlayer].iSegmentCounter = 1
	FOR iLoop = 0 TO NUM_COLLISION_BOXES-1
		CLEAR_COLLISION_BOX(LGHTCYCLE_PLAYER[iPlayer].CollisionBox[iLoop])
	ENDFOR
	CLEAR_COLLISION_BOX(LGHTCYCLE_PLAYER[iPlayer].EndCollisionBox)
	LGHTCYCLE_PLAYER[iPlayer].bLoopActive = FALSE

	LGHTCYCLE_PLAYER[iPlayer].fCachedDotProductBike = 0
	LGHTCYCLE_PLAYER[iPlayer].fCachedDotProductPed = 0
	
	CLEANUP_PRON_VFX(iPlayer)
	
ENDPROC

FUNC BOOL SETUP_PLAYER_BIKE(INT iPlayer, BOOL bIsLocalPlayer, PED_INDEX CurrentPlayerPedId)

	IF IS_PED_INJURED(CurrentPlayerPedId)
		RETURN FALSE
	ENDIF

	IF IS_PED_ON_ANY_BIKE(CurrentPlayerPedId)
		
		LGHTCYCLE_PLAYER[iPlayer].LightBike = GET_VEHICLE_PED_IS_IN(CurrentPlayerPedId)
		
		IF IS_VEHICLE_DRIVEABLE(LGHTCYCLE_PLAYER[iPlayer].LightBike)
			
			SETUP_PRON_PLAYER_COLOUR(iPlayer)
			SET_NETWORK_VEHICLE_MAX_POSITION_DELTA_MULTIPLIER(LGHTCYCLE_PLAYER[iPlayer].LightBike, 2)
			SET_NETWORK_ENABLE_HIGH_SPEED_EDGE_FALL_DETECTION(LGHTCYCLE_PLAYER[iPlayer].LightBike, TRUE)
			
			IF bIsLocalPlayer
				IF IS_BIT_SET(iLocalPronBitSet, ciNEEDS_TO_START_STARTUP)
					IF bPlayerToUseOK	
						PRINTLN("[KH][PRON][RESPAWNING] Resetting local player")
						
						IF NETWORK_HAS_CONTROL_OF_ENTITY(LocalPlayerPed)
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciUSE_PRON_POWERUP_LOGIC)	
								SET_ENTITY_INVINCIBLE(LocalPlayerPed, TRUE)
							ENDIF
						ELSE
							NETWORK_REQUEST_CONTROL_OF_ENTITY(LocalPlayerPed)
							RETURN FALSE
						ENDIF
						
						IF DOES_ENTITY_EXIST(LGHTCYCLE_PLAYER[iPlayer].LightBike)
							IF NETWORK_HAS_CONTROL_OF_ENTITY(LGHTCYCLE_PLAYER[iPlayer].LightBike)
								IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciUSE_PRON_POWERUP_LOGIC)
									SET_ENTITY_INVINCIBLE(LGHTCYCLE_PLAYER[iPlayer].LightBike, TRUE)
								ENDIF
								PRINTLN("[LH][PRON] Setting vehicle colours! iBike1 = ",iBike1," iBike2 = ",iBike2," iBike4 = ",iBike4)
								SET_VEHICLE_COLOURS(LGHTCYCLE_PLAYER[iPlayer].LightBike, iBike1, iBike2)
								SET_VEHICLE_EXTRA_COLOURS(LGHTCYCLE_PLAYER[iPlayer].LightBike, 0, iBike4)
						
								IF NOT GET_IS_VEHICLE_ENGINE_RUNNING(LGHTCYCLE_PLAYER[iPlayer].LightBike)
									SET_VEHICLE_ENGINE_ON(LGHTCYCLE_PLAYER[iPlayer].LightBike, TRUE, TRUE)
								ENDIF
								
								CLEAR_BIT(iLocalPronBitSet, ciNEEDS_TO_START_STARTUP)
								
							ELSE
								NETWORK_REQUEST_CONTROL_OF_ENTITY(LGHTCYCLE_PLAYER[iPlayer].LightBike)
								RETURN FALSE
							ENDIF
						ELSE
							RETURN FALSE
						ENDIF
						
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciUSE_PRON_POWERUP_LOGIC)
							SET_BIT(iLocalPronBitSet, ciLOCAL_PLAYER_IS_INVULNERABLE)
							LGHTCYCLE_PLAYER[iPlayer].bIsStartingUp = TRUE
						ENDIF
						
						REINIT_NET_TIMER(tdBikeStartupTimer)
						iStartupVibration = 255
						
						IF bUsingBirdsEyeCam = TRUE
							bUseBirdsEyeCamOnRespawn = TRUE
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciPRON_SHOW_TIME_SINCE_LAST_DEATH)
							REINIT_NET_TIMER(MC_playerBD[iLocalPart].tdTimeAlivePron)
						ENDIF
					
						RETURN TRUE
					
					ENDIF
						
				ENDIF
			ELSE
				
				RETURN TRUE
			
			ENDIF
		
		ENDIF
	
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_PRON_LOGIC_BE_PROCESSED_ON_PLAYER(PED_INDEX CurrentPlayerPedId)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciUSE_PRON_POWERUP_LOGIC)
		RETURN TRUE
	ELSE
		IF IS_PED_ON_ANY_BIKE(CurrentPlayerPedId)
			IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(CurrentPlayerPedId)) = SHOTARO
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC BOOL SHOULD_WE_CLEAR_PRON_TRAILS_FOR_NON_ACTIVE_PRON_TEAM(INT iPlayer)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciUSE_PRON_POWERUP_LOGIC)
		RETURN TRUE
	ELSE
		IF NOT IS_BIT_SET(iActivePronTeams, iPlayer)
			PRINTLN("[PRON][VV] Clearing lines as no players on bikes for team: ", iPlayer)
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE

ENDFUNC

PROC RUN_PRON_MODE(INT iPart, INT iPlayer)
	
	PRINTLN("[LH][PRONCOL] 	===========================================================")	
	PRINTLN("[LH][PRONCOL] 	=====                                               =======")	
	PRINTLN("[LH][PRONCOL] 	=====       PRON PLAYER - ",iPlayer,"                          =======")	
	PRINTLN("[LH][PRONCOL] 	=====                                               =======")	
	PRINTLN("[LH][PRONCOL] 	===========================================================")	
	
	//Set the player's start position
	PLAYER_INDEX CurrentPlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart))
	PED_INDEX CurrentPlayerPedId = GET_PLAYER_PED(CurrentPlayerId)
	
	IF NOT DOES_ENTITY_EXIST(oiTrailBottom)
		REQUEST_MODEL(PROP_LD_TEST_01)
		IF HAS_MODEL_LOADED(PROP_LD_TEST_01)
			oiTrailBottom = CREATE_OBJECT(PROP_LD_TEST_01, GET_ENTITY_COORDS(CurrentPlayerPedId), FALSE)
			SET_ENTITY_VISIBLE(oiTrailBottom, FALSE)
			SET_ENTITY_COLLISION(oiTrailBottom, FALSE)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciUSE_PRON_POWERUP_LOGIC)
		CLEAR_BIT(iLocalPronBitSet, ciLOCAL_PLAYER_IS_INVULNERABLE)
		LGHTCYCLE_PLAYER[iPlayer].bIsStartingUp = FALSE
	ENDIF
	
	IF GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
	AND GET_SKYSWOOP_STAGE() < SKYSWOOP_GOINGUP
		
		IF IS_TEAM_ACTIVE(iPlayer)
		AND SHOULD_PRON_LOGIC_BE_PROCESSED_ON_PLAYER(CurrentPlayerPedId)
		
			//If they are the local player, prepare them for starting up
			
			IF NOT LGHTCYCLE_PLAYER[iPlayer].bIsInitialised
				IF SETUP_PLAYER_BIKE(iPlayer, (CurrentPlayerPedId = LocalPlayerPed), CurrentPlayerPedId)
					LGHTCYCLE_PLAYER[iPlayer].bIsInitialised = TRUE
				ENDIF
			ENDIF
						
			BOOL bShouldCleanup
			
			IF IS_PLAYER_DOING_WARP_TO_SPAWN_LOCATION(CurrentPlayerId)
				bShouldCleanup = TRUE
				PRINTLN("[LH][PRON] Cleanup - Player is warping to spawn")
			ELIF NOT IS_ENTITY_ALIVE(LGHTCYCLE_PLAYER[iPlayer].LightBike)
				bShouldCleanup = TRUE
				PRINTLN("[LH][PRON] Cleanup - Bike ",iPlayer," is not alive")
			ELIF NOT IS_ENTITY_ALIVE(CurrentPlayerPedId)
				bShouldCleanup = TRUE
				PRINTLN("[LH][PRON] Cleanup - Player ",iPlayer," is not alive")
			ELSE
				PRINTLN("[LH][PRON] Cleanup - Bike and player are alive and not spawning")
			ENDIF
			
			//Clean up the player's line if they respawn but haven't died.
			IF bShouldCleanup
				PRINTLN("[LH][PRON] Player is warping, or their bike is dead, or the player ped is dead")
				CLEAR_PRON_LINES(iPlayer, CurrentPlayerId)
				LGHTCYCLE_PLAYER[iPlayer].bIsStartingUp = TRUE
				LGHTCYCLE_PLAYER[iPlayer].bIsInitialised = FALSE
				CLEANUP_BIKE_AUDIO(iPlayer)
				IF CurrentPlayerPedId = LocalPlayerPed
					iStartupVibration = 255
					REINIT_NET_TIMER(tdBikeStartupTimer)
					SET_BIT(iLocalPronBitSet, ciLOCAL_PLAYER_IS_INVULNERABLE)
					SET_BIT(iLocalPronBitSet, ciNEEDS_TO_START_STARTUP)
					IF IS_BIT_SET(MC_serverBD_3.iPronPlayerIsDrawingLines, iPlayer)
						IF iBroadcastOnTimeFalse = 0
						OR (GET_GAME_TIMER() - iBroadcastOnTimeFalse) >= 250
							BROADCAST_PRON_PLAYERS_DRAWING_TRAILS( MC_playerBD[iPartToUse].iteam, FALSE )
							iBroadcastOnTimeFalse = GET_GAME_TIMER()
						ENDIF
					ENDIF
					PRINTLN("[PRON][RESPAWNING] Resetting 4")
				ENDIF
			ENDIF
			
			IF IS_PLAYER_DOING_WARP_TO_SPAWN_LOCATION(CurrentPlayerId)
				IF DOES_ENTITY_EXIST(LGHTCYCLE_PLAYER[iPlayer].LightBike)
					IF NOT IS_ENTITY_ALIVE(LGHTCYCLE_PLAYER[iPlayer].LightBike)
						PRINTLN("[LH][PRON] Player is respawning and their bike still exists. Deleting.")
						DELETE_VEHICLE(LGHTCYCLE_PLAYER[iPlayer].LightBike)					
					ENDIF
				ENDIF
			ENDIF
			
			//Handle the bike, drawing the line and collision against it
			IF IS_ENTITY_ALIVE(LGHTCYCLE_PLAYER[iPlayer].LightBike)			
				LGHTCYCLE_PLAYER[iPlayer].vCurrentPos = GET_ENTITY_COORDS(LGHTCYCLE_PLAYER[iPlayer].LightBike)
				IF IS_VEHICLE_DRIVEABLE(LGHTCYCLE_PLAYER[iPlayer].LightBike)
				AND NOT IS_PLAYER_DOING_WARP_TO_SPAWN_LOCATION(CurrentPlayerId)
				AND NOT IS_PED_INJURED(CurrentPlayerPedID)
					HANDLE_PRON_LIGHTBIKE(iPlayer, CurrentPlayerId)
				ENDIF
			ENDIF
			
		ELSE
			IF SHOULD_WE_CLEAR_PRON_TRAILS_FOR_NON_ACTIVE_PRON_TEAM(iPlayer)
				PRINTLN("[LH][PRON] Team not active - Clearing")
				CLEAR_PRON_LINES(iPlayer, CurrentPlayerId)
				LGHTCYCLE_PLAYER[iPlayer].bIsStartingUp = TRUE
				LGHTCYCLE_PLAYER[iPlayer].bIsInitialised = FALSE
				CLEANUP_BIKE_AUDIO(iPlayer)
				IF CurrentPlayerPedId = LocalPlayerPed
					iStartupVibration = 255
					REINIT_NET_TIMER(tdBikeStartupTimer)
					SET_BIT(iLocalPronBitSet, ciLOCAL_PLAYER_IS_INVULNERABLE)
					SET_BIT(iLocalPronBitSet, ciNEEDS_TO_START_STARTUP)
					IF IS_BIT_SET(MC_serverBD_3.iPronPlayerIsDrawingLines, iPlayer)						
						IF iBroadcastOnTimeFalse = 0
						OR (GET_GAME_TIMER() - iBroadcastOnTimeFalse) >= 250
							BROADCAST_PRON_PLAYERS_DRAWING_TRAILS( MC_playerBD[iPartToUse].iteam, FALSE )
							iBroadcastOnTimeFalse = GET_GAME_TIMER()
						ENDIF
					ENDIF
					PRINTLN("[PRON][RESPAWNING] Resetting 5")
				ENDIF
			ENDIF
		ENDIF
			
	ELSE
		//Clears up the lines at the end of the round
		IF NOT IS_BIT_SET(iLocalPronBitSet, ciLOCAL_PLAYER_END_OF_MODE_CLEANUP)
			PRINTLN("[KH][PRON] RUN_PRON_MODE - Clearing up as the round has ended")
			INT iPlayerCount
			FOR iPlayerCount = 0 TO 3
				CLEAR_PRON_LINES(iPlayerCount, CurrentPlayerId)
				CLEANUP_BIKE_AUDIO(iPlayerCount)
			ENDFOR
			SET_BIT(iLocalPronBitSet, ciLOCAL_PLAYER_END_OF_MODE_CLEANUP)
		ENDIF
		
	ENDIF
	
ENDPROC

/////////////////////////////
///    END PRON	  ///////
/////////////////////

FUNC FLOAT GET_SCALE_SIZE_FOR_TURF_WAR_MARKER(BOOL bReduced)
	IF NOT HAS_NET_TIMER_STARTED(tdTurfWarGrowTimer)
		START_NET_TIMER(tdTurfWarGrowTimer)
	ELSE
		IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdTurfWarGrowTimer, ci_TURF_WAR_MARKER_GROW_TIME)
			RESET_NET_TIMER(tdTurfWarGrowTimer)
			START_NET_TIMER(tdTurfWarGrowTimer)			
			
			IF NOT bReduced
				IF fTurfWarMarkerSize > 1.5
					iTurfWarChangeMarkerSize = -0.225
				ELIF fTurfWarMarkerSize < -0.5
					iTurfWarChangeMarkerSize = 0.175
				ENDIF
			ELSE
				IF fTurfWarMarkerSize > 0.75
					iTurfWarChangeMarkerSize = -0.175
				ELIF fTurfWarMarkerSize < 0.0
					iTurfWarChangeMarkerSize = 0.135
				ENDIF
			ENDIF
			
			fTurfWarMarkerSize += iTurfWarChangeMarkerSize
		ENDIF
	ENDIF
	
	RETURN fTurfWarMarkerSize
ENDFUNC

PROC TURF_WAR_MAINTAIN_LOCAL_PLAYER_MARKER_FOR_TOPDOWN_CAM()
	
	PED_INDEX pedFocus = localPlayerPed
	
	IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
	OR IS_LOCAL_PLAYER_ANY_SPECTATOR()
		IF iSpectatorTarget != -1
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iSpectatorTarget))
				pedFocus = GET_PLAYER_PED(INT_TO_PLAYERINDEX(iSpectatorTarget))
			ENDIF
		ENDIF	
	ENDIF
	
	IF NOT IS_PED_INJURED(pedFocus)	
		VEHICLE_INDEX vehPlayer
		VECTOR vPlayerRot
		VECTOR vPlayerDir
		VECTOR vPlayerPos
		VECTOR vCamPos
		VECTOR vMarkerPos
		
		vCamPos = BirdsEyeTarget
		
		IF IS_PED_IN_ANY_VEHICLE(pedFocus)
			vehPlayer = GET_VEHICLE_PED_IS_IN(pedFocus) 			
			vPlayerPos = GET_ENTITY_COORDS(vehPlayer)
		ELSE
			vPlayerPos = GET_ENTITY_COORDS(pedFocus)
		ENDIF
		
		IF NOT HAS_NET_TIMER_STARTED(tdTurfWarMarkerRingFlash)
			START_NET_TIMER(tdTurfWarMarkerRingFlash)
		ENDIF
		IF NOT HAS_NET_TIMER_STARTED(tdTurfWarMarkerRingFlashDuration)
			START_NET_TIMER(tdTurfWarMarkerRingFlashDuration)
		ENDIF
				
		vMarkerPos = vCamPos + <<((vPlayerPos.x - vCamPos.x) / 2), ((vPlayerPos.y - vCamPos.y) / 2), ((vPlayerPos.z - vCamPos.z) / 2)>>
		vPlayerDir = (vPlayerPos - vCamPos)
		
		FLOAT fBaseSizeMod = 0
		FLOAT fDistance = VDIST2(vPlayerPos, vCamPos)
		IF fDistance < POW(170, 2)
			fBaseSizeMod = 0.9
		ENDIF
		
		FLOAT fSizeMod = GET_SCALE_SIZE_FOR_TURF_WAR_MARKER(fBaseSizeMod > 0)
				
		// No need to flash anymore...
		IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdTurfWarMarkerRingFlashDuration, ci_TURF_WAR_MARKER_RING_FLASH_DURATION)
			IF DOES_ENTITY_EXIST(vehPlayer)
				IF IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(vehPlayer))
					DRAW_MARKER(MARKER_RING, vMarkerPos, vPlayerDir, vPlayerRot, <<5.5+fSizeMod, 0.25, 5.5+fSizeMod>>, 255, 255, 255, 255)
					DRAW_MARKER(MARKER_RING, vMarkerPos+<<0.0,0.0,-0.1>>, vPlayerDir, vPlayerRot, <<6.5+fSizeMod, 0.25, 6.5+fSizeMod>>, 0, 0, 0, 255)
				ELSE
					DRAW_MARKER(MARKER_RING, vMarkerPos, vPlayerDir, vPlayerRot, <<4.0+fSizeMod, 0.25, 4.0+fSizeMod>>, 255, 255, 255, 255)
					DRAW_MARKER(MARKER_RING, vMarkerPos+<<0.0,0.0,-0.1>>, vPlayerDir, vPlayerRot, <<5.0+fSizeMod, 0.25, 5.0+fSizeMod>>, 0, 0, 0, 255)
				ENDIF
			ELSE
				DRAW_MARKER(MARKER_RING, vMarkerPos, vPlayerDir, vPlayerRot, <<(fBaseSizeMod/2)-3.0+fSizeMod, 0.25, (fBaseSizeMod/2)-3.0+fSizeMod>>, 255, 255, 255, 255)
				DRAW_MARKER(MARKER_RING, vMarkerPos+<<0.0,0.0,-0.1>>, vPlayerDir, vPlayerRot, <<fBaseSizeMod-4.0+fSizeMod, 0.25, fBaseSizeMod-4.0+fSizeMod>>, 0, 0, 0, 255)
			ENDIF
		ELSE
			// Flash
			IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdTurfWarMarkerRingFlash, ci_TURF_WAR_MARKER_RING_FLASH)
				IF DOES_ENTITY_EXIST(vehPlayer)
					IF IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(vehPlayer))
						DRAW_MARKER(MARKER_RING, vMarkerPos, vPlayerDir, vPlayerRot, <<5.5+fSizeMod, 0.25, 5.5+fSizeMod>>, 255, 255, 255, 255)
						DRAW_MARKER(MARKER_RING, vMarkerPos+<<0.0,0.0,-0.1>>, vPlayerDir, vPlayerRot, <<6.5+fSizeMod, 0.25, 6.5+fSizeMod>>, 0, 0, 0, 255)
					ELSE
						DRAW_MARKER(MARKER_RING, vMarkerPos, vPlayerDir, vPlayerRot, <<4.0+fSizeMod, 0.25, 4.0+fSizeMod>>, 255, 255, 255, 255)
						DRAW_MARKER(MARKER_RING, vMarkerPos+<<0.0,0.0,-0.1>>, vPlayerDir, vPlayerRot, <<5.0+fSizeMod, 0.25, 5.0+fSizeMod>>, 0, 0, 0, 255)
					ENDIF
				ELSE
					DRAW_MARKER(MARKER_RING, vMarkerPos, vPlayerDir, vPlayerRot, <<(fBaseSizeMod/2)-3.0+fSizeMod, 0.25, (fBaseSizeMod/2)-3.0+fSizeMod>>, 255, 255, 255, 255)
					DRAW_MARKER(MARKER_RING, vMarkerPos+<<0.0,0.0,-0.1>>, vPlayerDir, vPlayerRot, <<fBaseSizeMod-4.0+fSizeMod, 0.25, fBaseSizeMod-4.0+fSizeMod>>, 0, 0, 0, 255)
				ENDIF
				IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdTurfWarMarkerRingFlash, ci_TURF_WAR_MARKER_RING_FLASH+250)
					RESET_NET_TIMER(tdTurfWarMarkerRingFlash)
					START_NET_TIMER(tdTurfWarMarkerRingFlash)
				ENDIF
			ENDIF
		ENDIF		
	ENDIF
ENDPROC


//////////////////////////////
///    FUNCTIONS FOR POINTLESS
///    

PROC STOP_POINTLESS_SOUND(INT SoundID)
	IF IS_SOUND_ID_VALID(SoundID)
		STOP_SOUND(SoundID)
		IF SoundID > -1
			RELEASE_SOUND_ID(SoundID)
		ENDIF
		PRINTLN("[JT AUDIO POINTLESS] Stopping Sound ", SoundID)
	ENDIF
ENDPROC

PROC SET_UP_TIMER_FOR_POINTLESS_ENDING()
	IF DOES_ANY_TEAM_HAVE_MAX_POINTS()
		IF NOT IS_BIT_SET(iLocalBoolCheck19, LBOOL19_PTL_WIN_TIMER_STARTED)
			PRINTLN("[JT POINTLESS] STARTING TIMER WITH: ", g_FMMC_STRUCT.iPTLWinTimeLimit * 1000, " duration.")
			REINIT_NET_TIMER(MC_serverBD_1.stPTLWinTimer)
			SET_BIT(iLocalBoolCheck19, LBOOL19_PTL_WIN_TIMER_STARTED)
		ELSE
			PRINTLN("[JT POINTLESS] Timer bit is set")
			IF NOT HAS_NET_TIMER_STARTED(MC_serverBD_1.stPTLWinTimer)
				CLEAR_BIT(iLocalBoolCheck19, LBOOL19_PTL_WIN_TIMER_STARTED)
				RESET_NET_TIMER(MC_serverBD_1.stPTLWinTimer)
			ELSE
				PRINTLN("[JT POINTLESS] Timer has already started, Has the bit been set?: ", IS_BIT_SET(iLocalBoolCheck19, LBOOL19_PTL_WIN_TIMER_STARTED))
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck19, LBOOL19_PTL_WIN_TIMER_STARTED)
			CLEAR_BIT(iLocalBoolCheck19, LBOOL19_PTL_WIN_TIMER_STARTED)
			RESET_NET_TIMER(MC_serverBD_1.stPTLWinTimer)
			CLEAR_BIT(iLocalBoolCheck20,LBOOL20_POINTLESS_COUNTDOWN_SHARD)
			STOP_POINTLESS_SOUND(iPTLCountdownSoundID)
			CLEAR_BIT(iLocalBoolCheck20, LBOOL20_POINTLESS_10S_COUNTDOWN_STARTED)
			iPTLCountdownSoundID = -1
			PRINTLN("[JT POINTLESS] Bit is set without max points. Resetting Bit and Timer")
		ENDIF
	ENDIF
ENDPROC

PROC PTL_ENDING_SHARD()
	INT iTeamToShow, i
	TEXT_LABEL_31 tl31
	FOR i = 0 TO FMMC_MAX_TEAMS - 1
		IF MC_serverBD.iTeamScore[i] >= g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionScoreLimit
			iTeamToShow = i
		ENDIF
	ENDFOR
	IF MC_serverBD.iTeamScore[MC_playerBD[iPartToUse].iteam] >= g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionScoreLimit
		SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, "PTL_END_TITLE","PTL_WIN_SUB",HUD_COLOUR_WHITE, 3500)
		PRINTLN("[JT SHARD POINTLESS] Shard showing for winning team")
		SET_BIT(iLocalBoolCheck20,LBOOL20_POINTLESS_COUNTDOWN_SHARD)
	ELSE
		tl31 = "PTL_LOSE_SUB_"
		tl31 += iTeamToShow
		SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, "PTL_END_TITLE", tl31,HUD_COLOUR_WHITE, 3500)
		PRINTLN("[JT SHARD POINTLESS] Shard showing for trailing team")
		SET_BIT(iLocalBoolCheck20,LBOOL20_POINTLESS_COUNTDOWN_SHARD)
	ENDIF
ENDPROC

FUNC BOOL ANY_PACKAGES_DROPPED()
	INT i
	FOR i = 0 TO MC_serverBD.iNumObjCreated - 1
		IF MC_serverBD_1.iPTLDroppedPackageOwner[i] != -1
			PRINTLN("[JT POINTLESS] ANY_PACKAGES_DROPPED Package: ", i, " is dropped. iPTLDroppedPackageOwner: ", MC_serverBD_1.iPTLDroppedPackageOwner[i])
			RETURN TRUE
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

PROC SET_POINTLESS_OWNED_PACKAGE_NUMBER()
	INT i
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			IF g_FMMC_STRUCT.iPTLOwnedPickUps[i] != MC_serverBD.iPlayerScore[i]
				PRINTLN("[JT OVERHEAD] Updating overhead score from: ", g_FMMC_STRUCT.iPTLOwnedPickUps[i], " for participant ", i)
				g_FMMC_STRUCT.iPTLOwnedPickUps[i] = MC_serverBD.iPlayerScore[i]
				PRINTLN("[JT OVERHEAD] Updating overhead score to: ", g_FMMC_STRUCT.iPTLOwnedPickUps[i], " for participant ", i)
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC PLAY_POINTLESS_SOUND(INT iSoundToPlay,INT iPart, INT iTeam, ENTITY_INDEX entityToPlayFrom = NULL)
	PRINTLN("[JT AUDIO POINTLESS] ***** INFO DUMP START *****")
	PRINTLN("[JT AUDIO POINTLESS] iSoundToPlay: ", iSoundToPlay)
	PRINTLN("[JT AUDIO POINTLESS] iPart: ", iPart)
	PRINTLN("[JT AUDIO POINTLESS] iTeam: ", iTeam)
	PRINTLN("[JT AUDIO POINTLESS] ***** INFO DUMP END *****")
	SWITCH iSoundToPlay
		CASE ciPTLAudio_COLLECT

			IF iTeam = MC_playerBD[iPartToUse].iteam
				IF iPart = iPartToUse
					PLAY_SOUND_FRONTEND(-1,"Collect_Pickup","DLC_IE_PL_Player_Sounds", FALSE)
					PRINTLN("[JT AUDIO POINTLESS] Playing: Collect_Pickup - player")
				ELSE
				//	PLAY_SOUND_FRONTEND(-1,"Collect_Pickup","DLC_IE_PL_Team_Sounds", FALSE)
					PLAY_SOUND_FROM_ENTITY(-1,"Collect_Pickup",entityToPlayFrom,"DLC_IE_PL_Team_Sounds",FALSE)
					PRINTLN("[JT AUDIO POINTLESS] Playing: Collect_Pickup - team")
				ENDIF
			ELSE
				//PLAY_SOUND_FRONTEND(-1,"Collect_Pickup","DLC_IE_PL_Enemy_Sounds", FALSE)
				PLAY_SOUND_FROM_ENTITY(-1,"Collect_Pickup",entityToPlayFrom,"DLC_IE_PL_Enemy_Sounds",FALSE)
				PRINTLN("[JT AUDIO POINTLESS] Playing: Collect_Pickup - enemy")
			ENDIF
		BREAK
		CASE ciPTLAudio_DROP
			IF iTeam = MC_playerBD[iPartToUse].iteam
				IF iPart = iPartToUse
					PLAY_SOUND_FRONTEND(-1,"Drop_Pickup","DLC_IE_PL_Player_Sounds", FALSE)
					PRINTLN("[JT AUDIO POINTLESS] Playing: Drop_Pickup - player")
				ELSE
					PLAY_SOUND_FRONTEND(-1,"Drop_Pickup","DLC_IE_PL_Team_Sounds", FALSE)
					PRINTLN("[JT AUDIO POINTLESS] Playing: Drop_Pickup - team")
				ENDIF
			ELSE
				PLAY_SOUND_FRONTEND(-1,"Drop_Pickup","DLC_IE_PL_Enemy_Sounds", FALSE)
				PRINTLN("[JT AUDIO POINTLESS] Playing: Drop_Pickup - enemy")
			ENDIF
		BREAK
		CASE ciPTLAudio_SCORE_UP
			IF iTeam = MC_playerBD[iPartToUse].iteam
				IF iPart = iPartToUse
					PLAY_SOUND_FRONTEND(-1,"Score_Up","DLC_IE_PL_Player_Sounds", FALSE)
					PRINTLN("[JT AUDIO POINTLESS] Playing: Score_Up - player")
				ELSE
					
					PLAY_SOUND_FRONTEND(-1,"Score_Up","DLC_IE_PL_Team_Sounds", FALSE)
					PRINTLN("[JT AUDIO POINTLESS] Playing: Score_Up - team")
				ENDIF
			ELSE
				PLAY_SOUND_FRONTEND(-1,"Score_Up","DLC_IE_PL_Enemy_Sounds", FALSE)
				PRINTLN("[JT AUDIO POINTLESS] Playing: Score_Up - enemy")
			ENDIF
		BREAK
		CASE ciPTLAudio_SCORE_DOWN
			IF iTeam = MC_playerBD[iPartToUse].iteam
				IF iPart = iPartToUse
					PLAY_SOUND_FRONTEND(-1,"Score_Down","DLC_IE_PL_Player_Sounds", FALSE)
					PRINTLN("[JT AUDIO POINTLESS] Playing: Score_Down - player")
				ELSE
					PLAY_SOUND_FRONTEND(-1,"Score_Down","DLC_IE_PL_Team_Sounds", FALSE)
					PRINTLN("[JT AUDIO POINTLESS] Playing: Score_Down - team")
				ENDIF
			ELSE
				PLAY_SOUND_FRONTEND(-1,"Score_Down","DLC_IE_PL_Enemy_Sounds", FALSE)
				PRINTLN("[JT AUDIO POINTLESS] Playing: Score_Down - enemy")
			ENDIF
		BREAK
		CASE ciPTLAudio_COUNTDOWN
			IF iTeam = MC_playerBD[iPartToUse].iteam
				IF iPart = iPartToUse
					IF iPTLCountdownSoundID = -1
						iPTLCountdownSoundID = GET_SOUND_ID()
						PLAY_SOUND_FRONTEND(iPTLCountdownSoundID,"Countdown_To_Win","DLC_IE_PL_Player_Sounds", FALSE)
						PRINTLN("[JT AUDIO POINTLESS] Playing: Countdown_To_Win - Player - Sound ID: ", iPTLCountdownSoundID)
					ENDIF
				ELSE
					IF iPTLCountdownSoundID = -1
						iPTLCountdownSoundID = GET_SOUND_ID()
						PLAY_SOUND_FRONTEND(iPTLCountdownSoundID,"Countdown_To_Win","DLC_IE_PL_Team_Sounds", FALSE)
						PRINTLN("[JT AUDIO POINTLESS] Playing: Countdown_To_Win - Team - Sound ID: ", iPTLCountdownSoundID)
					ENDIF			
				ENDIF
			ELSE
				IF iPTLCountdownSoundID = -1
					iPTLCountdownSoundID = GET_SOUND_ID()
					PLAY_SOUND_FRONTEND(iPTLCountdownSoundID,"Countdown_To_Win","DLC_IE_PL_Player_Sounds", FALSE)
					PRINTLN("[JT AUDIO POINTLESS] Playing: Countdown_To_Win - Enemy - Sound ID: ", iPTLCountdownSoundID)
				ENDIF			
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC



PROC PLAY_POINTLESS_SOUND_ON_TEAM_SCORE_CHANGE()
	INT i
	FOR i = 0 TO FMMC_MAX_TEAMS - 1
		IF iPTLTeamScore[i] != MC_serverBD.iTeamScore[i]
			INT iChange = MC_serverBD.iTeamScore[i] - iPTLTeamScore[i]
			IF iChange > 0
				PLAY_POINTLESS_SOUND(ciPTLAudio_SCORE_UP, iPartToUse, i)
				iPTLTeamScore[i] = MC_serverBD.iTeamScore[i]
			ELIF iChange < 0
				PLAY_POINTLESS_SOUND(ciPTLAudio_SCORE_DOWN, iPartToUse, i)
				iPTLTeamScore[i] = MC_serverBD.iTeamScore[i]
			ENDIF
		ELSE
			iPTLTeamScore[i] = MC_serverBD.iTeamScore[i]
		ENDIF
	ENDFOR
ENDPROC
PROC PLAY_POINTLESS_COUNTDOWN_SOUND()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciPOINTLESS_MODE_ENABLED)
		IF HAS_NET_TIMER_STARTED(MC_serverBD_1.stPTLWinTimer)
		AND NOT IS_BIT_SET(iLocalBoolCheck20, LBOOL20_POINTLESS_10S_COUNTDOWN_STARTED)
			INT iTimeLeft = (g_FMMC_STRUCT.iPTLWinTimeLimit * 1000) - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.stPTLWinTimer)
			
			IF iTimeLeft <= 10000
			
				INT i, iWinningTeam
				FOR i = 0 TO FMMC_MAX_TEAMS - 1
					IF MC_serverBD.iTeamScore[i] >= g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionScoreLimit
						iWinningTeam = i
					ENDIF
				ENDFOR
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_POINTLESS_SOUNDS)
				AND iTimeLeft > 0
				AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
					PLAY_POINTLESS_SOUND(ciPTLAudio_COUNTDOWN, iPartToUse, iWinningTeam)
				ENDIF
				
				SET_BIT(iLocalBoolCheck20, LBOOL20_POINTLESS_10S_COUNTDOWN_STARTED)
				PRINTLN("[JT AUDIO POINTLESS] - PLAY_POINTLESS_SOUND ciPTLAudio_COUNTDOWN")
			ENDIF
		ENDIF
	ENDIF
ENDPROC
PROC SHOW_POINTS_ON_TEAM_BLIP()
	INT i
	FOR i = 0 TO NETWORK_GET_NUM_PARTICIPANTS() - 1
		IF MC_playerBD[i].iteam = MC_playerBD[iPartToUse].iteam
			IF iPTLTeamMateScore[i] != MC_playerBD[i].iPlayerScore
				SET_CUSTOM_BLIP_NUMBER_FOR_PLAYER(INT_TO_PLAYERINDEX(i),MC_playerBD[i].iPlayerScore, TRUE)
				iPTLTeamMateScore[i] = MC_playerBD[i].iPlayerScore
			ENDIF
		ENDIF
	ENDFOR
ENDPROC


///    
///
///    END OF POINTLESS
//////////////////////////////

/////////////////////////////
///    TINY RACERS
///



FUNC FLOAT TINY_RACERS_GET_CURRENT_BOUNDS_SIZE(INT iTeam, INT iRule)
	IF ARE_ALL_PARTICIPANTS_ALIVE()
		RETURN g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fRadius
	ELSE
		RETURN fCurrentSphereRadius
	ENDIF
ENDFUNC

PROC TINY_RACERS_SET_FRONT_OF_CAR_POS(VECTOR& vPlayerPos)
	IF IS_ENTITY_ALIVE(localPlayerPed)
	AND IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		IF IS_ENTITY_ALIVE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
			
			VECTOR vMin, vMax
			GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)),vMin, vMax)
			
			vPlayerPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(GET_VEHICLE_PED_IS_IN(LocalPlayerPed),<<0,vMax.y,0>>)
		ENDIF
	ELSE
		vPlayerPos = GET_ENTITY_COORDS(LocalPlayerPed, FALSE)
	ENDIF
ENDPROC

FUNC BOOL TINY_RACERS_CHECK_REAR_OF_CAR(VECTOR vBoundsPos, FLOAT fBoundsRadius)
	VECTOR vMin, vMax, vPlayerPos
	IF IS_ENTITY_ALIVE(localPlayerPed)
	AND IS_PED_IN_ANY_VEHICLE(localPlayerPed)
		IF IS_ENTITY_ALIVE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))			
				
			GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)),vMin, vMax)
				
			vPlayerPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(GET_VEHICLE_PED_IS_IN(LocalPlayerPed),<<0,vMin.y,0>>)
		ENDIF
		
		IF VDIST2(vPlayerPos, vBoundsPos) < POW(fBoundsRadius,2)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC TINY_RACERS_SHRINK_BOUNDS(INT iTeam, INT iRule)
	
	fSphereStartRadius = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fRadius
	
	IF NOT IS_BIT_SET(iLocalBoolCheck21, LBOOL21_TINY_RACERS_SHRINKING_BOUNDS_SIZE_SET)
		fCurrentSphereRadius = fSphereStartRadius
		SET_BIT(iLocalBoolCheck21, LBOOL21_TINY_RACERS_SHRINKING_BOUNDS_SIZE_SET)
	ENDIF
	
	fSphereEndRadius = g_FMMC_STRUCT.fTREndRadius
	FLOAT fDiff = (ABSF(fSphereStartRadius - fSphereEndRadius) * 1000)
	
	FLOAT fDiffThisFrame = (fDiff / (g_FMMC_STRUCT.iTRShrinkingTimer * 1000)) * GET_FRAME_TIME()
	PRINTLN("[JT BOUNDS] TINY_RACERS_SHRINK_BOUNDS - fDiffThisFrame: ", fDiffThisFrame)
	IF fDiffThisFrame < 0.0025
		fDiffThisFrame = 0.0025
	ENDIF
	
	IF fCurrentSphereRadius > fSphereEndRadius
		fCurrentSphereRadius -= fDiffThisFrame
		IF fCurrentSphereRadius < fSphereEndRadius
			fCurrentSphereRadius = fSphereEndRadius
		ENDIF
		PRINTLN("[JT BOUNDS] TINY_RACERS_SHRINK_BOUNDS - New fCurrentSphereRadius: ", fCurrentSphereRadius)
	ELSE
		fCurrentSphereRadius = fSphereEndRadius
	ENDIF

ENDPROC

FUNC BOOL TINY_RACERS_IS_ONE_PLAYER_LEFT_ALIVE()
	IF GET_NUMBER_OF_PLAYERS() > 1
		IF GET_LAST_PARTICIPANT_ALIVE() != INVALID_PARTICIPANT_INDEX()
			PRINTLN("[JT TR] TINY_RACERS_IS_ONE_PLAYER_LEFT_ALIVE - Returning True")
			g_LastPlayerAliveTinyRacers = TRUE
			RETURN TRUE
		ENDIF	
	ENDIF
	g_LastPlayerAliveTinyRacers = FALSE
	RETURN FALSE
ENDFUNC

FUNC BOOL TR_REQUEST_COUNTDOWN_UI(MISSION_INTRO_COUNTDOWN_UI &uiToRequest)
	IF HAS_SCALEFORM_MOVIE_LOADED(uiToRequest.uiCountdown)
		PRINTLN("[JT CD] Loaded countdown UI")
		RETURN TRUE
	ELSE
		uiToRequest.uiCountdown = REQUEST_SCALEFORM_MOVIE("COUNTDOWN")
		PRINTLN("[JT CD] Requesting countdown UI")
		RETURN FALSE
	ENDIF
ENDFUNC

PROC TR_CLEAR_COUNTDOWN_DATA(MISSION_INTRO_COUNTDOWN_UI &cduiToUse)

	cduiToUse.iBitFlags = 0	
	CANCEL_TIMER(cduiToUse.CountdownTimer)
	
	CLEAR_BIT(iLocalBoolCheck13, LBOOL13_DISPLAYED_INTRO_SHARD)
	CLEAR_BIT(g_FMMC_STRUCT.bsRaceMission, ciDidSpeedBoost)
	RESET_NET_TIMER(g_FMMC_STRUCT.stBoostTimer)

	PRINTLN("[JT CD] Clearing Countdown Data")
ENDPROC

PROC TINY_RACERS_PROCESS_INTRO_CAM_BLEND()
	IF IS_SYNCHRONIZED_SCENE_RUNNING(iTRCameraSyncScene)
		
		IF GET_SYNCHRONIZED_SCENE_PHASE(iTRCameraSyncScene) > 0.98
			RENDER_SCRIPT_CAMS(TRUE, TRUE, 5000)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_TINY_RACERS_INTRO_OVER()
	IF IS_SYNCHRONIZED_SCENE_RUNNING(iTRCameraSyncScene)
		IF GET_SYNCHRONIZED_SCENE_PHASE(iTRCameraSyncScene) > 0.98
			SET_BIT(iLocalBoolCheck22, LBOOL22_TINY_RACERS_INTRO_CAM_FINISHED)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//bForControl checks if players should be given back control, setting this to false checks if players should fade in
FUNC BOOL TINY_RACERS_ARE_ALL_RACERS_READY(BOOL bForControl = FALSE)
	
	INT iPart
	INT iPlayersChecked
	INT iPlayersToCheck = GET_NUMBER_OF_PLAYING_PLAYERS()
	INT iPlayersReady
	
	IF bIsLocalPlayerHost
		FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
			IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)
			AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_PLAYER_FAIL)
			AND IS_BIT_SET(MC_playerBD[iPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
			AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_FINISHED)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
				AND IS_NET_PLAYER_OK(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart)))
				AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
					iPlayersChecked++
					PED_INDEX piTemp = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart)))
					IF IS_PED_SITTING_IN_ANY_VEHICLE(piTemp)
					AND IS_ENTITY_VISIBLE(piTemp)
						IF bForControl
							IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet3, PBBOOL3_TINY_RACERS_SCREEN_PAUSED)
								iPlayersReady++
							ENDIF
						ELSE
							IF IS_BIT_SET(MC_playerBD[iPart].iClientBitSet3, PBBOOL3_TINY_RACERS_PLAYER_WARPED)
								iPlayersReady++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF iPlayersReady >= iPlayersToCheck
				IF bForControl
					PRINTLN("[JT TR] TINY_RACERS_ARE_ALL_RACERS_READY - Setting SBBOOL, players ready for control")
					IF MC_serverBD.iTRReadyForControlTime = -1
						MC_serverBD.iTRReadyForControlTime = NATIVE_TO_INT(GET_NETWORK_TIME()) + 500
					ENDIF
				ELSE
					PRINTLN("[JT TR] TINY_RACERS_ARE_ALL_RACERS_READY - Setting SBBOOL, players ready for fade")
					IF MC_serverBD.iTRReadyToFadeTime = -1
						MC_serverBD.iTRReadyToFadeTime = NATIVE_TO_INT(GET_NETWORK_TIME()) + 1500
					ENDIF
				ENDIF
			ELSE
				IF (GET_FRAME_COUNT() % 30) = 0
					PRINTLN("[JT TR] TINY_RACERS_ARE_ALL_RACERS_READY - iPlayersReady: ", iPlayersReady, " not >= iPlayersToCheck: ", iPlayersToCheck)
				ENDIF
			ENDIF
			IF iPlayersChecked >= iPlayersToCheck
				BREAKLOOP
			ENDIF
		ENDFOR
	ENDIF
	
	IF bForControl			
		IF MC_serverBD.iTRReadyForControlTime != -1 
		AND NATIVE_TO_INT(GET_NETWORK_TIME()) > MC_serverBD.iTRReadyForControlTime
			RETURN TRUE
		ENDIF
	ELSE
		IF MC_serverBD.iTRReadyToFadeTime != -1 
		AND NATIVE_TO_INT(GET_NETWORK_TIME()) > MC_serverBD.iTRReadyToFadeTime
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL TINY_RACERS_WARP_TO_SPAWN_LOCATION(INT iSpawnPointIndex)

	IF iSpawnPointIndex != -1
		VECTOR vCoord = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[MC_playerBD[iLocalPart].iteam][iSpawnPointIndex].vPos
		FLOAT fHeading = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[MC_playerBD[iLocalPart].iteam][iSpawnPointIndex].fHead
		CLEAR_AREA(vCoord, 20.0, TRUE)
		IF NET_WARP_TO_COORD(vCoord, fHeading, TRUE, FALSE)
			PRINTLN("[JT TR] TINY_RACERS_WARP_TO_SPAWN_LOCATION - Successful warp to spawn point ", iSpawnPointIndex)
			RETURN TRUE
		ELSE
			PRINTLN("[JT TR] TINY_RACERS_WARP_TO_SPAWN_LOCATION - Failed warp to spawn point ", iSpawnPointIndex)
		ENDIF
	ELSE
		PRINTLN("[JT TR] TINY_RACERS_WARP_TO_SPAWN_LOCATION - iSpawnPointIndex = -1")
	ENDIF

	RETURN FALSE
ENDFUNC

PROC TINY_RACERS_SET_PLAYER_ROUND_STATE(TinyRacersRoundState eState)
	MC_playerBD[iLocalPart].ePlayerTinyRacersRoundState = eState
	PRINTLN("[JT] TINY_RACERS_SET_PLAYER_ROUND_STATE - Set player state to ", GET_TR_ROUND_STATE_AS_STRING())
ENDPROC

FUNC BOOL TINY_RACERS_PROCESS_ROUND_RESPAWN(INT iTeam, INT iRule)	
	IF (NOT IS_LOCAL_PLAYER_ANY_SPECTATOR())
	OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
		IF iRule < FMMC_MAX_RULES
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[iRule], ciBS_RULE8_RESPAWN_SPECTATORS)
			AND IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
				IF NOT bLocalPlayerPedOk
					PRINTLN("[JS] [SPECREZ] - TINY_RACERS_PROCESS_ROUND_RESPAWN - FORCE RESPAWN SET")
					IF IS_WARNING_MESSAGE_ACTIVE()
					    FORCE_PLAYER_OFF_WARNING_SCREEN(TRUE)
					ENDIF 
					SET_BIT(iLocalBoolCheck21, LBOOL21_SPECTATOR_RESPAWN_FORCE_RESPAWN) //To bring out of spectator
					PRINTLN("[JT TR] TINY_RACERS_PROCESS_ROUND_RESPAWN Setting LBOOL21_SPECTATOR_RESPAWN_FORCE_RESPAWN")
				ELSE
					DISABLE_HEIST_SPECTATE(TRUE)
					CLEANUP_HEIST_SPECTATE_FOR_NON_HEISTS()
					MPGlobalsAmbience.piHeistSpectateTarget = INVALID_PLAYER_INDEX()
					CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
					CLEAR_BIT(iLocalBoolCheck21, LBOOL21_SPECTATOR_RESPAWN_REQUESTED)
					PRINTLN("[SPECREZ] - LBOOL21_SPECTATOR_RESPAWN_REQUESTED - Clear 1")
					CLEAR_BIT(iLocalBoolCheck21, LBOOL21_SPECTATOR_RESPAWN_WAIT_IN_SPECTATE)
					CLEAR_MP_DECORATOR_BIT(LocalPlayer, MP_DECORATOR_BS_INVALID_TO_SPECTATE)
					CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_HEIST_SPECTATE_WHILE_DEAD_ENABLED)
					RESET_NET_TIMER(tdSpectatorDelayTimer)
					CLEAR_BIT(iLocalBoolCheck21, LBOOL21_SPECTATOR_RESPAWN_FORCE_RESPAWN)
					PRINTLN("[JT TR] Clearing LBOOL21_SPECTATOR_RESPAWN_FORCE_RESPAWN")
					SET_SPECTATOR_CAN_QUIT(TRUE)
					RESET_SPECTATOR_HUD_DATA_STRUCT()
					SET_ON_LBD_GLOBAL(FALSE)
					PRINTLN("[JT TR] TINY_RACERS_PROCESS_ROUND_RESPAWN Stuck in heist spectator but not injured, calling CLEANUP_HEIST_SPECTATE_FOR_NON_HEISTS")
				ENDIF
				PRINTLN("[JT TR] TINY_RACERS_PROCESS_ROUND_RESPAWN - Bringing player back from spectator")
			ELIF NOT bLocalPlayerPedOk
				IF IS_WARNING_MESSAGE_ACTIVE()
				    FORCE_PLAYER_OFF_WARNING_SCREEN(TRUE)
				ENDIF 
				FORCE_RESPAWN()
				PRINTLN("[JT TR] From just injured, should be using spawn point: ", GET_CURRENT_ACTIVE_TEAM_SPAWN_POINT())
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TINY_RACERS_PLAYER_WARPED)
		OR (NOT IS_PED_INJURED(LocalPlayerPed)
		AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN))
			RETURN TRUE
		ENDIF
	ELSE
		PRINTLN("[JT TR] TINY_RACERS_PROCESS_ROUND_RESPAWN - We are a real spectator dont respawn")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC TINY_RACERS_SET_MOTION_BLUR_ON_VEHICLES(BOOL bAllowMotionBlur)
	INT iPart
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
			PED_INDEX piTemp = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart)))
			IF IS_PED_IN_ANY_VEHICLE(piTemp)
				IF IS_ENTITY_ALIVE(GET_VEHICLE_PED_IS_IN(piTemp))
					SET_ENTITY_MOTION_BLUR(GET_VEHICLE_PED_IS_IN(piTemp),bAllowMotionBlur)
					PRINTLN("[JT TR] Setting motion blur on participant: ", iPart)
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

FUNC BOOL TINY_RACERS_ARE_ALL_RACERS_ON_SAME_RULE()
	INT iTeam, iOtherTeam
	BOOL bAllOnSameRule = TRUE
	FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
		IF IS_TEAM_ACTIVE(iTeam)
		AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet,SSBOOL_TEAM0_FAILED + iTeam)
			FOR iOtherTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
				IF IS_TEAM_ACTIVE(iOtherTeam)
				AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet,SSBOOL_TEAM0_FAILED + iTeam)
					IF MC_serverBD_4.iCurrentHighestPriority[iTeam] != MC_serverBD_4.iCurrentHighestPriority[iOtherTeam]
					OR MC_serverBD_4.iTeamLapsCompleted[iTeam] != MC_serverBD_4.iTeamLapsCompleted[iOtherTeam]
						PRINTLN("[JT TR] TINY_RACERS_ARE_ALL_RACERS_ON_SAME_RULE - Team ", iTeam, " is not on the same rule or lap as ", iOtherTeam)
						bAllOnSameRule = FALSE
						BREAKLOOP
					ENDIF
				ENDIF
			ENDFOR
			IF NOT bAllOnSameRule
				BREAKLOOP
			ENDIF
		ENDIF
	ENDFOR
	RETURN bAllOnSameRule
ENDFUNC

FUNC BOOL TINY_RACERS_IS_ON_SAME_RULE_AND_LAP_AS_WINNER()
	IF MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam] = MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[MC_serverBD.iTRLastParticipantToUse].iteam]
	AND MC_ServerBD_4.iTeamLapsCompleted[MC_playerBD[iLocalPart].iteam] = MC_ServerBD_4.iTeamLapsCompleted[MC_playerBD[MC_serverBD.iTRLastParticipantToUse].iteam]
	AND NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iLocalPart].iteam)
		PRINTLN("[JT TR] TINY_RACERS_IS_ON_SAME_RULE_AND_LAP_AS_WINNER - Returning true, at same priority as the previous winner: ", MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam])
		RETURN TRUE
	ENDIF
	PRINTLN("[JT TR] TINY_RACERS_IS_ON_SAME_RULE_AND_LAP_AS_WINNER - Not on same rule/lap as previous winner, waiting to catch up")
	PRINTLN("[JT TR] Local Rule: ", MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam], " Previous Winner Rule: ", MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[MC_serverBD.iTRLastParticipantToUse].iteam])
	PRINTLN("[JT TR] Local Lap: ", MC_ServerBD_4.iTeamLapsCompleted[MC_playerBD[iLocalPart].iteam]," Previous Winner Lap: " ,MC_ServerBD_4.iTeamLapsCompleted[MC_playerBD[MC_serverBD.iTRLastParticipantToUse].iteam])
	RETURN FALSE
ENDFUNC

PROC TINY_RACERS_ADD_PARTICLE_EFFECT()
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(TRPfx)
			USE_PARTICLE_FX_ASSET("scr_sr_tr")
			TRPfx = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_sr_tr_player_glow", GET_VEHICLE_PED_IS_IN(LocalPlayerPed), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0)
			
			PRINTLN("[JT PFX] Started TRPfx")
		ELSE
			INT iR, iG, iB, iA
			
			GET_HUD_COLOUR(GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_playerBD[iLocalPart].iTeam, PLAYER_ID()), iR, iG, iB, iA)
			
			FLOAT fR = TO_FLOAT(iR)
			FLOAT fG = TO_FLOAT(iG)
			FLOAT fB = TO_FLOAT(iB)
			
			fR = fR / 255
			fG = fG / 255
			fB = fB / 255
			
			SET_PARTICLE_FX_LOOPED_COLOUR(TRPfx,fR,fG,fB)
			
			IF NOT HAS_NET_TIMER_STARTED(tdTRPfxTimer)
				REINIT_NET_TIMER(tdTRPfxTimer)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CLEAN_UP_ALL_WEAPONS(BOOL bCleanUpWeaponEffects = FALSE)
	IF TINY_RACERS_GET_ROUND_STATE(iLocalPart) != TR_Racing
		INT i
		//Clearing up placed bombs
		FOR i = 0 TO ciBMB_MAX_MAX_ACTIVE_BOMBS - 1
			IF NETWORK_DOES_NETWORK_ID_EXIST( oBmbNetBombs[i].objectID)
				
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(oBmbNetBombs[i].objectID)
					DELETE_NET_ID(oBmbNetBombs[i].objectID)
					PRINTLN("[JT TR] Removing bomb ", i)
				ELSE
					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(oBmbNetBombs[i].objectID)
				ENDIF
			ENDIF
		ENDFOR
		
		IF bCleanUpWeaponEffects
			IF IS_BIT_SET(MC_playerBD[iLocalPart].iVehicleWeaponEffectActiveBS, ciVEH_WEP_FLIPPED_CONTROLS)
				CLEAR_BIT(MC_playerBD[iLocalPart].iVehicleWeaponEffectActiveBS, ciVEH_WEP_FLIPPED_CONTROLS)
			ENDIF
			IF HAS_NET_TIMER_STARTED(stVehWepFlippedTimer)
				RESET_NET_TIMER(stVehWepFlippedTimer)
			ENDIF
			
		ENDIF
		
		//Clearing up shot rockets
		REMOVE_ALL_PROJECTILES_OF_TYPE(g_wtPickupRocketType, FALSE)
		
		MC_playerBD[iLocalPart].iQuantityOfBombs = 0
		
		//Clearing all picked up weapons
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
			IF TINY_RACERS_GET_ROUND_STATE(iLocalPart) != TR_Racing
				MC_playerBD[iLocalPart].iVehicleWeaponCollectedBS = 0
				MC_playerBD[iPartToUse].bIsFiringMachineGun = FALSE
				IF IS_SOUND_ID_VALID(iBulletSoundID)
					STOP_SOUND(iBulletSoundID)
					RELEASE_SOUND_ID(iBulletSoundID)
					iBulletSoundID = -1
				ENDIF
			ENDIF
		ENDIF
	ELSE
		PRINTLN("[JT TR] Attempting to clear all weapons outside of TR_Racing")
	ENDIF
ENDPROC

PROC TINY_RACERS_DISABLE_VEHICLE_CONTROL()
	IF TINY_RACERS_GET_ROUND_STATE(iLocalPart) = TR_MoveLastPlayer
	OR TINY_RACERS_GET_ROUND_STATE(iLocalPart) = TR_Respawn
	OR TINY_RACERS_GET_ROUND_STATE(iLocalPart) = TR_Warp
	OR TINY_RACERS_GET_ROUND_STATE(iLocalPart) = TR_Sync
		DISABLE_PLAYER_VEHICLE_CONTROLS_THIS_FRAME()
	ENDIF
	
ENDPROC

FUNC BOOL TINY_RACERS_IS_VEHICLE_IN_WATER(VEHICLE_INDEX vehToUse)
	IF DOES_ENTITY_EXIST(vehToUse)
		IF NOT IS_PED_INJURED(LocalPlayerPed)
			IF IS_SPECIAL_VEHICLE_A_BOAT(GET_ENTITY_MODEL(vehToUse))
				IF IS_VEHICLE_IN_WATER(vehToUse)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC TINY_RACERS_TEMP_ANCHOR(VEHICLE_INDEX vehToUse)
	
	FLOAT fHeading
	VECTOR vPos

	IF (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_VEH_ACCELERATE)
		OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_VEH_BRAKE))
	AND (TINY_RACERS_GET_ROUND_STATE(iLocalPart) = TR_Racing
		OR TINY_RACERS_GET_ROUND_STATE(iLocalPart) = TR_Reset
		OR  TINY_RACERS_GET_ROUND_STATE(iLocalPart) = TR_Init
		OR  TINY_RACERS_GET_ROUND_STATE(iLocalPart) = TR_LastManTimer)
		IF GET_CURRENT_ACTIVE_TEAM_SPAWN_POINT() != -1
			IF DOES_ENTITY_EXIST(vehToUse)
				IF NOT IS_PED_INJURED(LocalPlayerPed)
					IF IS_SPECIAL_VEHICLE_A_BOAT(GET_ENTITY_MODEL(vehToUse))
						PRINTLN("[JT TR] Anchor up")
						CLEAR_BIT(iLocalBoolCheck21,LBOOL21_TINY_RACERS_ALLOW_ANCHOR)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF TINY_RACERS_IS_VEHICLE_IN_WATER(vehToUse)
			PRINTLN("[JT TR] Anchors away!")
			IF GET_CURRENT_ACTIVE_TEAM_SPAWN_POINT() != -1
				fHeading = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[MC_playerBD[iLocalPart].iteam][GET_CURRENT_ACTIVE_TEAM_SPAWN_POINT()].fHead
				vPos = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[MC_playerBD[iLocalPart].iteam][GET_CURRENT_ACTIVE_TEAM_SPAWN_POINT()].vPos
				PRINTLN("[JT TR] (1) setting heading to ", fHeading)
				SET_ENTITY_HEADING(vehToUse, fHeading)
			ENDIF
		ENDIF

	ENDIF
	IF TINY_RACERS_GET_ROUND_STATE(iLocalPart) = TR_Warp
	OR TINY_RACERS_GET_ROUND_STATE(iLocalPart) = TR_Sync
	OR TINY_RACERS_GET_ROUND_STATE(iLocalPart) = TR_321
		IF TINY_RACERS_IS_VEHICLE_IN_WATER(vehToUse)
			IF GET_CURRENT_ACTIVE_TEAM_SPAWN_POINT() != -1
				vPos = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[MC_playerBD[iLocalPart].iteam][GET_CURRENT_ACTIVE_TEAM_SPAWN_POINT()].vPos
				IF NOT ARE_VECTORS_EQUAL(GET_ENTITY_COORDS(vehToUse), vPos)
					fHeading = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[MC_playerBD[iLocalPart].iteam][GET_CURRENT_ACTIVE_TEAM_SPAWN_POINT()].fHead		
					PRINTLN("[JT TR] (2) setting heading to ", fHeading)
					SET_ENTITY_HEADING(vehToUse, fHeading)
					SET_ENTITY_COORDS(vehToUse, vPos)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC TINY_RACERS_RENDER_PHASE_PAUSE(BOOL bUnpause = FALSE)
	IF NOT bUnpause
		IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TINY_RACERS_SCREEN_PAUSED)
			PRINTLN("[JT TR] Pausing renderphase")
			SET_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TINY_RACERS_SCREEN_PAUSED)
			PRINTLN("[JT TR] PBBOOL3_TINY_RACERS_SCREEN_PAUSED ON")
			THEFEED_HIDE_THIS_FRAME()
			THEFEED_FLUSH_QUEUE()
			IF IS_BROWSER_OPEN()
				MP_FORCE_TERMINATE_INTERNET()
			ENDIF
			SET_CELEB_BLOCK_RESURRECT_RENDERPHASE_UNPAUSE(TRUE)
			TOGGLE_PAUSED_RENDERPHASES(FALSE)
		ENDIF
	ELSE
		CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TINY_RACERS_SCREEN_PAUSED)
		PRINTLN("[JT TR] PBBOOL3_TINY_RACERS_SCREEN_PAUSED OFF")
		SET_CELEB_BLOCK_RESURRECT_RENDERPHASE_UNPAUSE(FALSE)
		TOGGLE_PAUSED_RENDERPHASES(TRUE)
	ENDIF
ENDPROC

FUNC BOOL IS_TINY_RACERS_CAMERA_IN_POSITION()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_TINY_RACERS_CAMERA)
		IF BirdsEyeCurrent.z >= BirdsEyeTarget.z - 0.2
		OR IS_BIT_SET(iLocalBoolCheck22, LBOOL22_TINY_RACERS_CAMERA_IN_POSITION)
			SET_BIT(iLocalBoolCheck22, LBOOL22_TINY_RACERS_CAMERA_IN_POSITION)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL TINY_RACERS_SHOULD_FORCE_PROGRESSION()
	RETURN IS_BIT_SET(MC_serverBD.iServerBitSet4,SBBOOL4_TR_SHOULD_PROGRESS_THROUGH_RESET)
ENDFUNC

PROC TINY_RACERS_BOUNDS_DELAY_AT_START()
	IF NOT IS_BIT_SET(iLocalBoolCheck22, LBOOL22_TINY_RACERS_OK_TO_PROCESS_BOUNDS)
		IF NOT HAS_NET_TIMER_STARTED(tdTRBoundsDelay)
			REINIT_NET_TIMER(tdTRBoundsDelay)
		ELSE
			IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdTRBoundsDelay) >= 1000
				SET_BIT(iLocalBoolCheck22, LBOOL22_TINY_RACERS_OK_TO_PROCESS_BOUNDS)
				PRINTLN("[JT TR] TINY_RACERS_BOUNDS_DELAY_AT_START Setting LBOOL22_TINY_RACERS_OK_TO_PROCESS_BOUNDS")
				RESET_NET_TIMER(tdTRBoundsDelay)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL TINY_RACERS_IS_CAMERA_READY_FOR_PAUSE()
	IF IS_BIT_SET(iLocalBoolCheck22, LBOOL22_TINY_RACERS_RENDER_PAUSE_TARGET_DEAD)
		RETURN TRUE
	ELIF DOES_CAM_EXIST(swanDiveCamera) AND DOES_CAM_EXIST(g_birdsEyeCam)
		IF (IS_CAM_RENDERING(swanDiveCamera) AND (NOT IS_CAM_ACTIVE(g_birdsEyeCam)))
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC TINY_RACERS_WINNER_CAM_POST_FX(INT iPart, BOOL bClear = FALSE)
	IF NOT bClear
		IF iPart != -1
			IF MC_playerBD[iPart].iTeam = 0
				//ORANGE FILTER
				IF NOT ANIMPOSTFX_IS_RUNNING("InchOrange")
					PRINTLN("[JT TR] PROCESS_PLAYER_RESPAWN_BACK_AT_START - Playing InchOrange FX")
					ANIMPOSTFX_PLAY("InchOrange", 0, TRUE)
					SET_BIT(iLocalBoolCheck16, LBOOL16_DIVE_SCORE_SCREEN_FX_ACTIVE)
				ENDIF
			ELIF MC_playerBD[iPart].iTeam = 1
				//PURPLE FILTER
				IF NOT ANIMPOSTFX_IS_RUNNING("InchPurple")
					PRINTLN("[JT TR] PROCESS_PLAYER_RESPAWN_BACK_AT_START - Playing InchPurple FX")
					ANIMPOSTFX_PLAY("InchPurple", 0, TRUE)
					SET_BIT(iLocalBoolCheck16, LBOOL16_DIVE_SCORE_SCREEN_FX_ACTIVE)
				ENDIF
			ELIF MC_playerBD[iPart].iTeam = 2
				//PINK FILTER
				IF NOT ANIMPOSTFX_IS_RUNNING("TinyRacerPink")
					PRINTLN("[JT TR] PROCESS_PLAYER_RESPAWN_BACK_AT_START - Playing TinyRacerPink FX")
					ANIMPOSTFX_PLAY("TinyRacerPink", 0, TRUE)
					SET_BIT(iLocalBoolCheck16, LBOOL16_DIVE_SCORE_SCREEN_FX_ACTIVE)
				ENDIF
			ELIF MC_playerBD[iPart].iTeam = 3
				//GREEN FILTER
				IF NOT ANIMPOSTFX_IS_RUNNING("TinyRacerGreen")
					PRINTLN("[JT TR] PROCESS_PLAYER_RESPAWN_BACK_AT_START - Playing TinyRacerGreen FX")
					ANIMPOSTFX_PLAY("TinyRacerGreen", 0, TRUE)
					SET_BIT(iLocalBoolCheck16, LBOOL16_DIVE_SCORE_SCREEN_FX_ACTIVE)
				ENDIF
			ENDIF	
		ENDIF
	ELSE
		IF ANIMPOSTFX_IS_RUNNING("TinyRacerGreen")
			ANIMPOSTFX_STOP("TinyRacerGreen")
			ANIMPOSTFX_PLAY("TinyRacerGreenOut", 0,TRUE)
		ELIF ANIMPOSTFX_IS_RUNNING("TinyRacerPink")
			ANIMPOSTFX_STOP("TinyRacerPink")
			ANIMPOSTFX_PLAY("TinyRacerPinkOut", 0,TRUE)
		ENDIF
		ANIMPOSTFX_STOP_ALL()
	ENDIF
ENDPROC

PROC TINY_RACERS_PLAYER_LOOP_CHECKS(BOOL &bStatePassed, BOOL &bAllWarpedPassed, BOOL &bClear321Passed,
									BOOL &bAllDeadPassed, BOOL &bClearedProgressionPassed, BOOL &bShouldForceProgressPassed,
									BOOL bCheckState = FALSE,TinyRacersRoundState eTRRoundState = TR_Invalid, BOOL bCheckAllWarped = FALSE, BOOL bCheckClear321 = FALSE,
									BOOL bCheckAllDead = FALSE, BOOL bCheckClearedProgression = FALSE, BOOL bCheckShouldForceProgress = FALSE)
	INT iPart
	INT iPlayersChecked
	INT iPlayersToCheck = GET_NUMBER_OF_PLAYING_PLAYERS()
	INT iPlayersPassedState, iPlayersPassedWarped, iPlayersPassed321, iPlayersPassedDead, iPlayersPassedProgression
	BOOL bAllAlive 
	
	IF bCheckClear321
	OR bCheckAllWarped
		bAllAlive = ARE_ALL_PARTICIPANTS_ALIVE()
	ENDIF
	
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)
		AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_PLAYER_FAIL)
		AND IS_BIT_SET(MC_playerBD[iPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
		AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_FINISHED)
			PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iPart)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
				IF IS_NET_PLAYER_OK(tempPlayer, FALSE)
					iPlayersChecked++
					
					IF bCheckState
						IF TINY_RACERS_GET_ROUND_STATE(iPart) = eTRRoundState
							iPlayersPassedState++
							IF iPlayersPassedState >= iPlayersToCheck
								PRINTLN("[JT TR] TINY_RACERS_PLAYER_LOOP_CHECKS - ALL IN SAME STATE - Returning TRUE")
								bStatePassed = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					IF bCheckAllWarped
						IF bAllAlive
							IF IS_BIT_SET(MC_playerBD[iPart].iClientBitSet3, PBBOOL3_TINY_RACERS_PLAYER_WARPED)
								iPlayersPassedWarped++
								IF iPlayersPassedWarped >= iPlayersToCheck
									PRINTLN("[JT TR] TINY_RACERS_PLAYER_LOOP_CHECKS - ALL WARPED - Returning TRUE")
									bAllWarpedPassed = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF bCheckClear321
						IF bAllAlive
							IF IS_BIT_SET(MC_playerBD[iPart].iClientBitSet3, PBBOOL3_TINY_RACERS_321_FINISHED)
								iPlayersPassed321++
								IF iPlayersPassed321 >= GET_NUMBER_OF_PLAYING_PLAYERS()
									PRINTLN("[JT TR] TINY_RACERS_PLAYER_LOOP_CHECKS - ALL 321 CLEAR - Returning TRUE")
									bClear321Passed = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF bCheckAllDead
						IF NOT IS_NET_PLAYER_OK(tempPlayer, TRUE)
						OR IS_BIT_SET(MC_playerBD[iPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
							iPlayersPassedDead++
							PRINTLN("[JT TR] TINY_RACERS_PLAYER_LOOP_CHECKS - ALL DEAD - Participant ", iPart, " is dead")
							IF iPlayersPassedDead >= iPlayersToCheck
								PRINTLN("[JT TR] TINY_RACERS_PLAYER_LOOP_CHECKS - ALL DEAD - Returning TRUE")
								bAllDeadPassed = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					IF bCheckClearedProgression
						IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet3, PBBOOL3_TINY_RACERS_PLAYER_HAS_PROGRESSED)
							iPlayersPassedProgression++
							IF iPlayersPassedProgression >= iPlayersToCheck
								PRINTLN("[JT TR] TINY_RACERS_PLAYER_LOOP_CHECKS - ALL CLEARED PROGRESSION - Returning TRUE")
								bClearedProgressionPassed = TRUE
							ENDIF
						ELSE
							PRINTLN("[JT TR] TINY_RACERS_HAVE_ALL_PLAYERS_CLEARED_PROGRESSION - Participant ", iPart," still has PBBOOL3_TINY_RACERS_PLAYER_HAS_PROGRESSED set!")
						ENDIF
					ENDIF
					
					IF bCheckShouldForceProgress
						IF bIsLocalPlayerHost
							IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet4,SBBOOL4_TR_SHOULD_PROGRESS_THROUGH_RESET)
								IF IS_BIT_SET(MC_playerBD[iPart].iClientBitSet3, PBBOOL3_TINY_RACERS_PLAYER_HAS_PROGRESSED)
									SET_BIT(MC_serverBD.iServerBitSet4,SBBOOL4_TR_SHOULD_PROGRESS_THROUGH_RESET)
									PRINTLN("[JT TR] Setting iServerBitSet4,SBBOOL4_TR_SHOULD_PROGRESS_THROUGH_RESET")
								ENDIF
							ELSE
								PRINTLN("[JT TR] TINY_RACERS_PLAYER_LOOP_CHECKS - ALL IN SAME STATE - Returning TRUE")
								bShouldForceProgressPassed = TRUE
							ENDIF
						ELSE
							PRINTLN("[JT TR] TINY_RACERS_SERVER_SHOULD_EVERYONE_PROGRESS - Returning false as being called on non-host")
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
		IF iPlayersChecked >= iPlayersToCheck
			BREAKLOOP
		ENDIF
	ENDFOR
ENDPROC

PROC TINY_RACERS_SERVER_CHECK_FOR_DRAW()
	IF bIsLocalPlayerHost
		IF MC_serverBD.iTRLastParticipantToUse = -1
		OR IS_PED_INJURED(GET_PLAYER_PED(INT_TO_PLAYERINDEX(MC_serverBD.iTRLastParticipantToUse)))
			SET_BIT(MC_serverBD.iServerBitSet5, SBBOOL5_TINY_RACERS_ROUND_WAS_A_DRAW)
		ENDIF
	ENDIF
ENDPROC

PROC TINY_RACERS_PROCESS_MINI_ROUND()
	INT iTeam = MC_playerBD[iLocalPart].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	VEHICLE_INDEX vehTemp
	VECTOR vTemp
	BOOL bAllSameState, bAllWarped, bAllClear321, bAllDead, bAllClearedProgression, bShouldForceProgress
	
	IF g_bTDForceThroughCurrentState
		PRINTLN("[JT TR] FORCING PLAYER THROUGH CURRENT STATE")
		IF TINY_RACERS_GET_ROUND_STATE(iLocalPart) < TR_Reset
			TINY_RACERS_SET_PLAYER_ROUND_STATE(INT_TO_ENUM(TinyRacersRoundState, ENUM_TO_INT(TINY_RACERS_GET_ROUND_STATE(iLocalPart)) + 1))
		ENDIF
		g_bTDForceThroughCurrentState = FALSE
	ENDIF
	
	IF g_bTDHostForceProgressAllPlayers
		IF bIsLocalPlayerHost
			IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet4,SBBOOL4_TR_SHOULD_PROGRESS_THROUGH_RESET)
				SET_BIT(MC_serverBD.iServerBitSet4,SBBOOL4_TR_SHOULD_PROGRESS_THROUGH_RESET)
				PRINTLN("[JT TR] FORCING ALL PLAYERS TO PROGRESS")
			ENDIF
		ENDIF
		g_bTDHostForceProgressAllPlayers = FALSE
	ENDIF
	
	IF g_bTDForceRespawn
		IF TINY_RACERS_PROCESS_ROUND_RESPAWN(iTeam, iRule)
			g_bTDForceRespawn = FALSE
		ENDIF
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
			vehTemp = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		ENDIF
	ENDIF
	
	TINY_RACERS_DISABLE_VEHICLE_CONTROL()
	
	IF IS_BIT_SET(iLocalBoolCheck21,LBOOL21_TINY_RACERS_ALLOW_ANCHOR)
		TINY_RACERS_TEMP_ANCHOR(vehTemp)
	ENDIF
	PROCESS_SWAN_DIVE_SCORE_CAMERA(TRUE)
	IF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_FINISHED_COUNTDOWN)
		TINY_RACERS_BOUNDS_DELAY_AT_START()
	ENDIF
	SET_LIGHT_OVERRIDE_MAX_INTENSITY_SCALE(fBloomScale)
	
	IF TINY_RACERS_GET_ROUND_STATE(iLocalPart) != TR_Racing
		REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
	ENDIF
		
	IF NOT IS_BIT_SET(iLocalBoolCheck22, LBOOL22_VEHICLE_PETROL_TANK_DAMAGE_DISABLED)
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			IF IS_ENTITY_A_MISSION_ENTITY(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
			AND NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
				SET_DISABLE_VEHICLE_PETROL_TANK_DAMAGE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed), TRUE)
				SET_BIT(iLocalBoolCheck22, LBOOL22_VEHICLE_PETROL_TANK_DAMAGE_DISABLED)
			ENDIF
		ENDIF
	ENDIF
			
	IF iRule < FMMC_MAX_RULES
		SWITCH TINY_RACERS_GET_ROUND_STATE(iLocalPart)
			CASE TR_Racing
				IF NOT IS_PLAYER_SPECTATING(LocalPlayer)
					IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
						IF NOT IS_PED_INJURED(LocalPlayerPed)
							IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
								SET_ENTITY_HEALTH(LocalPlayerPed, 0)
							ELSE
								IF DOES_ENTITY_EXIST(vehTemp)
									IF NOT IS_VEHICLE_OK(vehTemp)
										IF NETWORK_HAS_CONTROL_OF_ENTITY(vehTemp)
											NETWORK_EXPLODE_VEHICLE(vehTemp)
										ELSE
											NETWORK_REQUEST_CONTROL_OF_ENTITY(vehTemp)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF IS_LOCAL_PLAYER_SPECTATOR()
						IF NOT GET_ENTITY_COLLISION_DISABLED(LocalPlayerPed)
							SET_ENTITY_COLLISION(LocalPlayerPed,FALSE)
						ENDIF
					ENDIF
				ENDIF
				
//				IF IS_PED_INJURED(LocalPlayerPed)
//				AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
//					STOP_CAM_POINTING(g_birdsEyeCam)
//				ENDIF
				
				//Blazer wheel UI
				IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
						vehTemp = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
						IF GET_HAS_RETRACTABLE_WHEELS(vehTemp)
						AND GET_IS_WHEELS_RETRACTED(vehTemp)
							HUD_FORCE_SPECIAL_VEHICLE_WEAPON_WHEEL()
						ENDIF
					ENDIF
				ENDIF
				
				IF bIsLocalPlayerHost
					TINY_RACERS_PLAYER_LOOP_CHECKS(bAllSameState, bAllWarped, bAllClear321, bAllDead, bAllClearedProgression, bShouldForceProgress, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE, DEFAULT, TRUE)
				ELSE
					TINY_RACERS_PLAYER_LOOP_CHECKS(bAllSameState, bAllWarped, bAllClear321, bAllDead, bAllClearedProgression, bShouldForceProgress, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
				ENDIF
				
				IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
				AND NOT IS_BIT_SET(iLocalBoolCheck22, LBOOL22_TINY_RACERS_SPECTATOR_DISABLING_CAM_CHANGE)
					SET_BIT(iLocalBoolCheck22, LBOOL22_TINY_RACERS_SPECTATOR_DISABLING_CAM_CHANGE)
					SET_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_TURF_WARS_CAMER_ACTIVE)
					REQUEST_SPEC_INSTRUCTIONAL_BUTTON_UPDATE()
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciTINY_RACERS_ENABLE_FLASHING_BLIPS)
					FLASH_OUT_OF_BOUNDS_PLAYER_BLIP()
				ENDIF
				
				IF TINY_RACERS_IS_ONE_PLAYER_LEFT_ALIVE()
				OR bAllDead
				OR TINY_RACERS_SHOULD_FORCE_PROGRESSION()
					#IF IS_DEBUG_BUILD
					IF TINY_RACERS_SHOULD_FORCE_PROGRESSION()
					AND NOT TINY_RACERS_IS_ONE_PLAYER_LEFT_ALIVE()
					AND NOT bAllDead
						PRINTLN("[JT TR] Progressing due to TINY_RACERS_SHOULD_FORCE_PROGRESSION")
					ENDIF
					#ENDIF
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TINY_RACERS_SOUNDS)
						IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_SR_TR_Winner_Screen_Scene")
							START_AUDIO_SCENE("DLC_SR_TR_Winner_Screen_Scene")
						ENDIF
					ENDIF
					TINY_RACERS_SET_PLAYER_ROUND_STATE(TR_Init)
				ELSE
					//Regular Racing stuff
					IF NOT ARE_ALL_PARTICIPANTS_ALIVE()
						//Shrinking
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciTINY_RACERS_SHRINK_BOUNDS)
							IF iRule < FMMC_MAX_RULES
								IF NOT HAS_NET_TIMER_STARTED(MC_serverBD_1.stTRBoundsShrinkTimer)
									IF bIsLocalPlayerHost
										REINIT_NET_TIMER(MC_serverBD_1.stTRBoundsShrinkTimer)
										PRINTLN("[JT TR] Starting shrink timer")
									ENDIF
								ELIF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.stTRBoundsShrinkTimer) > (g_FMMC_STRUCT.iTRShrinkStartTimer * 1000)
									#IF IS_DEBUG_BUILD
									IF (GET_FRAME_COUNT() % 30) = 0
										PRINTLN("[JT TR] Shrink Timer passed")
									ENDIF
									#ENDIF
									TINY_RACERS_SHRINK_BOUNDS(iTeam, iRule)
								ELSE
									fCurrentSphereRadius = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fRadius
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					NETWORK_SET_LOCAL_PLAYER_INVINCIBLE_TIME(0)	
					
					IF HAS_NET_TIMER_STARTED(tdTRPfxTimer)
						IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdTRPfxTimer) > ciTR_PFX_TIMER
							IF DOES_PARTICLE_FX_LOOPED_EXIST(TRPfx)
								STOP_PARTICLE_FX_LOOPED(TRPfx)
								RESET_NET_TIMER(tdTRPfxTimer)
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
			BREAK
			
			CASE TR_Init
				//Initialise all the things
				IF bIsLocalPlayerHost
		

					MC_serverBD.iTRReadyToFadeTime = -1 
					MC_serverBD.iTRReadyForControlTime = -1
					REINIT_NET_TIMER(MC_serverBD.tdTRSpawnTimer)
					TINY_RACERS_PLAYER_LOOP_CHECKS(bAllSameState, bAllWarped, bAllClear321, bAllDead, bAllClearedProgression, bShouldForceProgress,DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
					IF NOT bAllDead
						MC_serverBD.piTRLastPartStanding = GET_LAST_PARTICIPANT_ALIVE()
						MC_serverBD.iTRLastParticipantToUse = NATIVE_TO_INT(MC_serverBD.piTRLastPartStanding)
						PRINTLN("[JT TR] TR_Init - Setting MC_serverBD.piTRLastPartStanding & MC_serverBD.iTRLastParticipantToUse: ", MC_serverBD.iTRLastParticipantToUse)
					ENDIF
					
					RESET_NET_TIMER(MC_serverBD_1.stTRBoundsShrinkTimer)
										
					REINIT_NET_TIMER(MC_serverBD_1.stTRLastPlayerTimer)
					PRINTLN("[JT TR] TR_Init - STARTING TIMER")
					
				ENDIF
				
				CLEAR_BIT(iLocalBoolCheck22, LBOOL22_TINY_RACERS_SPECTATOR_DISABLING_CAM_CHANGE)

				IF DOES_CAM_EXIST(camTRIntro)
					DESTROY_CAM(camTRIntro)
					PRINTLN("[JT TR] Destroying Intro Cam")
				ENDIF
				
				IF NOT HAS_NET_TIMER_STARTED(tdStuckInitTimer)
					REINIT_NET_TIMER(tdStuckInitTimer)
				ENDIF
				
				CLEAR_BIT(iLocalBoolCheck21, LBOOL21_TINY_RACERS_SCORE_INCREMENTED)
				CLEAR_BIT(iLocalBoolCheck21, LBOOL21_TINY_RACERS_ROUND_SHARD_SHOWN)
				CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TINY_RACERS_PLAYER_WARPED)
				CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TINY_RACERS_321_FINISHED)
				
				
				IF (bLocalPlayerPedOk
				OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN))
				AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
					PRINTLN("[JT TR] TR_Init - Setting PBBOOL3_TINY_RACERS_PLAYER_HAS_PROGRESSED")
					SET_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TINY_RACERS_PLAYER_HAS_PROGRESSED)
				ENDIF
				
				IF NOT IS_LOCAL_PLAYER_SPECTATOR()
					CLEAN_UP_ALL_WEAPONS()
				ENDIF
				
				HIDE_DEAD_PLAYER_BLIPS(FALSE)
				
				IF IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
					CLEAR_ALL_BIG_MESSAGES()
					CLEAR_HELP()
				ENDIF
				
				TINY_RACERS_PLAYER_LOOP_CHECKS(bAllSameState, bAllWarped, bAllClear321, bAllDead, bAllClearedProgression, bShouldForceProgress,DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
				IF MC_serverBD.iTRLastParticipantToUse != -1
					TINY_RACERS_SET_PLAYER_ROUND_STATE(TR_LastManTimer)
				ELSE 
					IF bAllDead
					AND HAS_NET_TIMER_STARTED(MC_serverBD.tdTRSpawnTimer)
						PRINTLN("[JT TR] Progressing through to Init all dead")
						TINY_RACERS_SET_PLAYER_ROUND_STATE(TR_Respawn)
					ELSE
						IF HAS_NET_TIMER_STARTED(tdStuckInitTimer)
							IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdStuckInitTimer) >= 3000
								PRINTLN("[JT TR] Progressing because of stuck timer!")
								TINY_RACERS_SET_PLAYER_ROUND_STATE(TR_Respawn)
								RESET_NET_TIMER(tdStuckInitTimer)
							ENDIF
						ENDIF
						PRINTLN("[JT TR] Stuck in the else: All dead =  ", bAllDead, " HAS stuck spawn timer started ", HAS_NET_TIMER_STARTED(MC_serverBD.tdTRSpawnTimer))
					ENDIF
				ENDIF
			
			BREAK
			
			CASE TR_LastManTimer
				IF NOT IS_LOCAL_PLAYER_SPECTATOR()
					CLEAN_UP_ALL_WEAPONS()
				ENDIF
				
				TINY_RACERS_SERVER_CHECK_FOR_DRAW()
				
				IF HAS_NET_TIMER_STARTED(MC_serverBD_1.stTRLastPlayerTimer)
					REINIT_NET_TIMER(stDiveCamStallTimer)
					IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.stTRLastPlayerTimer) > (g_FMMC_STRUCT.iTRResetTimer * 1000)
					AND TINY_RACERS_IS_CAMERA_READY_FOR_PAUSE()
						
						Clear_Any_Objective_Text()
						CLEAR_HELP()
						HIDE_DEAD_PLAYER_BLIPS(TRUE)
						IF DOES_ENTITY_EXIST(vehTemp)
							CLEAR_AREA(GET_ENTITY_COORDS(vehTemp), 1000.0,FALSE)
						ENDIF
						
						IF NOT IS_BIT_SET(iLocalBoolCheck22, LBOOL22_TINY_RACERS_RENDER_PAUSE_TARGET_DEAD)
							IF MC_serverBD.iTRLastParticipantToUse != -1
								TINY_RACERS_WINNER_CAM_POST_FX(MC_serverBD.iTRLastParticipantToUse)
							ENDIF
							g_SpawnData.bIsSwoopedUp = TRUE
							
							TINY_RACERS_RENDER_PHASE_PAUSE()
						ENDIF
						REINIT_NET_TIMER(tdTRWarpDelay)
						PRINTLN("[JT TR] Reset timer passed, moving state")
						TINY_RACERS_SET_PLAYER_ROUND_STATE(TR_MoveLastPlayer)
						
					ELIF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.stTRLastPlayerTimer) > (g_FMMC_STRUCT.iTRResetTimer * 1000)/2
						IF MC_serverBD.iTRLastParticipantToUse != -1
						AND NOT IS_PED_INJURED(GET_PLAYER_PED(INT_TO_PLAYERINDEX(MC_serverBD.iTRLastParticipantToUse)))
							IF NOT IS_BIT_SET(iLocalBoolCheck21, LBOOL21_TINY_RACERS_ROUND_SHARD_SHOWN)
								IF iLocalPart = MC_serverBD.iTRLastParticipantToUse
									IF SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GENERIC_WINNER, NETWORK_GET_PLAYER_INDEX(MC_serverBD.piTRLastPartStanding), DEFAULT, "TR_RWINSL", "TR_RWIN",  GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_playerBD[MC_serverBD.iTRLastParticipantToUse].iTeam, PLAYER_ID()))
										SET_BIT(iLocalBoolCheck21, LBOOL21_TINY_RACERS_ROUND_SHARD_SHOWN)
										PRINTLN("[JT TR] TR_LastManTimer - Setting up Shard")
									ENDIF
									IF DOES_ENTITY_EXIST(vehTemp)
										IF GET_HAS_RETRACTABLE_WHEELS(vehTemp)
											IF GET_IS_WHEELS_RETRACTED(vehTemp)
											AND NOT IS_ENTITY_IN_WATER(vehTemp)
												SET_WHEELS_EXTENDED_INSTANTLY(vehTemp)
											ELIF NOT GET_IS_WHEELS_RETRACTED(vehTemp)
											AND IS_ENTITY_IN_WATER(vehTemp)
												SET_WHEELS_RETRACTED_INSTANTLY(vehTemp)
											ENDIF
										ENDIF
									ENDIF
								ELSE
									IF SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GENERIC_LOSER, NETWORK_GET_PLAYER_INDEX(MC_serverBD.piTRLastPartStanding), DEFAULT, "TR_RWINSL", "TR_RWIN", GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_playerBD[MC_serverBD.iTRLastParticipantToUse].iTeam, PLAYER_ID()))
										SET_BIT(iLocalBoolCheck21, LBOOL21_TINY_RACERS_ROUND_SHARD_SHOWN)
										PRINTLN("[JT TR] TR_LastManTimer - Setting up Shard")
									ENDIF
								ENDIF
							ELSE
								PRINTLN("[JT TR] TR_LastManTimer - Shard bit is set")
							ENDIF
						ENDIF
					ENDIF
				ELSE
					PRINTLN("[JT TR] TR_LastManTimer - Timer not started. Host will be starting it shortly")
					IF bIsLocalPlayerHost
						REINIT_NET_TIMER(MC_serverBD_1.stTRLastPlayerTimer)
					ENDIF
				ENDIF
				
				IF MC_serverBD.piTRLastPartStanding != INVALID_PARTICIPANT_INDEX()
					IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(MC_serverBD.piTRLastPartStanding)
						PRINTLN("[JT TR] TR_LastManTimer - Last alive is now invalid, move onto respawning")
						TINY_RACERS_SET_PLAYER_ROUND_STATE(TR_Respawn)
					ENDIF
				ENDIF
				
			BREAK
			
			CASE TR_MoveLastPlayer
				
				TINY_RACERS_SERVER_CHECK_FOR_DRAW()
				
				SET_BIT(iLocalBoolCheck21,LBOOL21_TINY_RACERS_ALLOW_ANCHOR)
			
				IF NOT IS_LOCAL_PLAYER_SPECTATOR()
					CLEAN_UP_ALL_WEAPONS(TRUE)
				ENDIF
				
				IF DOES_CAM_EXIST(g_birdsEyeCam)
					IF NOT IS_CAM_ACTIVE(g_birdsEyeCam)
						SET_CAM_ACTIVE(g_birdsEyeCam, TRUE)
					ENDIF
				ENDIF
								
				//Increase the score of the remaining player
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(MC_serverBD.iTRLastParticipantToUse))
					IF iLocalPart = MC_serverBD.iTRLastParticipantToUse
						PRINTLN("[JT TR] TR_MoveLastPlayer - Local player was last alive")
						IF bLocalPlayerPedOk
						AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
							IF HAS_NET_TIMER_STARTED(tdTRWarpDelay)
	//						IF ((NOT GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()) OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciDISABLE_TINY_RACERS_PAUSE))
	//						OR NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TINY_RACERS_SCREEN_PAUSED)
								IF HAS_NET_TIMER_EXPIRED(tdTRWarpDelay, ciTR_WARP_DELAY)
									PRINTLN("[JT TR] TR_MoveLastPlayer - Through renderphase checks, now attempting to warp")
									IF TINY_RACERS_WARP_TO_SPAWN_LOCATION(GET_CURRENT_ACTIVE_TEAM_SPAWN_POINT())
										IF TINY_RACERS_IS_VEHICLE_IN_WATER(vehTemp)
											vTemp = GET_ENTITY_COORDS(vehTemp)
											vTemp.z += 1
											NET_WARP_TO_COORD(vTemp, GET_ENTITY_HEADING(vehTemp), TRUE, FALSE)
										ENDIF
										SET_BIT(MC_playerBD[MC_serverBD.iTRLastParticipantToUse].iClientBitSet3, PBBOOL3_TINY_RACERS_LAST_PLAYER_WARPED)
										PRINTLN("[JT TR] TR_MoveLastPlayer - Setting PBBOOL3_TINY_RACERS_LAST_PLAYER_WARPED")
										
										IF NOT IS_PED_INJURED(LocalPlayerPed)
										AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_TINY_RACERS_ROUND_WAS_A_DRAW)
											IF NOT IS_BIT_SET(iLocalBoolCheck21, LBOOL21_TINY_RACERS_SCORE_INCREMENTED)
												MC_playerBD[iLocalPart].iPlayerScore++
												PRINTLN("[JT TR] TINY_RACERS_PROCESS_LAST_PLAYER_STANDING - Incrementing score")
												SET_BIT(iLocalBoolCheck21, LBOOL21_TINY_RACERS_SCORE_INCREMENTED)
											ENDIF
										ENDIF
										
										TINY_RACERS_SET_PLAYER_ROUND_STATE(TR_Respawn)
									ENDIF
								ENDIF
							ELSE
								REINIT_NET_TIMER(tdTRWarpDelay)
							ENDIF
//							ENDIF
						ELSE
							TINY_RACERS_PROCESS_ROUND_RESPAWN(iTeam, iRule)
						ENDIF
					ELSE
						PRINTLN("[JT TR] TR_MoveLastPlayer - Local player was not last alive, waiting for bit to be set. LocalPart: ", iLocalPart, " Last alive: ", MC_serverBD.iTRLastParticipantToUse)
						IF IS_BIT_SET(MC_playerBD[MC_serverBD.iTRLastParticipantToUse].iClientBitSet3, PBBOOL3_TINY_RACERS_LAST_PLAYER_WARPED)
							TINY_RACERS_SET_PLAYER_ROUND_STATE(TR_Respawn)
						ENDIF
						
						IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
							IF HAS_NET_TIMER_STARTED(tdAllDeadTimer)
								IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdAllDeadTimer) > ciTR_ALL_DEAD_TIMER
									PRINTLN("[JT TR] TR_MoveLastPlayer - Spectator Timer Passed")
									RESET_NET_TIMER(tdAllDeadTimer)
									TINY_RACERS_SET_PLAYER_ROUND_STATE(TR_Respawn)
								ENDIF
							ELSE
								REINIT_NET_TIMER(tdAllDeadTimer)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					PRINTLN("[JT TR] TR_MoveLastPlayer - Last alive is now invalid, move onto respawning")
					TINY_RACERS_SET_PLAYER_ROUND_STATE(TR_Respawn)
				ENDIF
				
			BREAK
			
			CASE TR_Respawn
			
				IF HAS_NET_TIMER_STARTED(MC_serverBD.tdTRSpawnTimer)
					IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdTRSpawnTimer) > 250
						IF HAS_NET_TIMER_STARTED(tdSwapTimer)
							RESET_NET_TIMER(tdSwapTimer)
						ENDIF
			
						//Respawning and warping
						IF iRule < FMMC_MAX_RULES
							
							TINY_RACERS_PROCESS_ROUND_RESPAWN(iTeam, iRule)
							
							IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
								IF HAS_NET_TIMER_STARTED(tdAllDeadTimer)
									IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdAllDeadTimer) > ciTR_ALL_DEAD_TIMER
										IF MPGlobalsAmbience.piHeistSpectateTarget = INVALID_PLAYER_INDEX()
											SET_BIT(iLocalBoolCheck21, LBOOL21_SPECTATOR_RESPAWN_FORCE_RESPAWN)
										ENDIF
									ENDIF
								ELSE
									REINIT_NET_TIMER(tdAllDeadTimer)
								ENDIF
							ENDIF
							
						ENDIF
						
						IF ARE_ALL_PARTICIPANTS_ALIVE()
							RESET_NET_TIMER(tdAllDeadTimer)
							TINY_RACERS_SET_PLAYER_ROUND_STATE(TR_Warp)
							DO_CORONA_POLICE_SETTINGS()
						ENDIF
					ENDIF
				ELSE
					IF bIsLocalPlayerHost
						REINIT_NET_TIMER(MC_serverBD.tdTRSpawnTimer)
					ENDIF
				ENDIF
				
			BREAK
			
			CASE TR_Warp
				IF (NOT IS_LOCAL_PLAYER_ANY_SPECTATOR())
				OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
					SET_BIT(iLocalBoolCheck3, LBOOL3_IN_BOUNDS1)
					SET_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_MISSION_BOUNDS)
										
					IF MC_serverBD.iTRLastParticipantToUse != -1
						IF HAS_NET_TIMER_STARTED(td_TR_WarpWait)
							PRINTLN("[JT TR] Timer td_TR_WarpWait is running")
							IF HAS_NET_TIMER_EXPIRED(td_TR_WarpWait, ci_iWarpWaitTimer)
								PRINTLN("[JT TR] td_TR_WarpWait has expired")
								IF TINY_RACERS_IS_ON_SAME_RULE_AND_LAP_AS_WINNER()	
									IF TINY_RACERS_WARP_TO_SPAWN_LOCATION(GET_CURRENT_ACTIVE_TEAM_SPAWN_POINT())
										PRINTLN("[JT TR] Readjusting racer's position to spawn point: ", GET_CURRENT_ACTIVE_TEAM_SPAWN_POINT())
										SET_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TINY_RACERS_PLAYER_WARPED)
										PRINTLN("[JT TR] Setting PBBOOL3_TINY_RACERS_PLAYER_WARPED")
									ENDIF
								ENDIF
							ENDIF
						ELSE
							REINIT_NET_TIMER(td_TR_WarpWait)
							PRINTLN("[JT TR] Starting td_TR_WarpWait")
						ENDIF
					ELSE
						IF TINY_RACERS_WARP_TO_SPAWN_LOCATION(GET_CURRENT_ACTIVE_TEAM_SPAWN_POINT())
							PRINTLN("******** [JT TR] MC_serverBD.iTRLastParticipantToUse = -1 - POTENTIALLY SPAWNING AT INCORRECT POSITION ********")
							PRINTLN("[JT TR] Readjusting racer's position to spawn point: ", GET_CURRENT_ACTIVE_TEAM_SPAWN_POINT())
							
							SET_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TINY_RACERS_PLAYER_WARPED)
							PRINTLN("[JT TR] Setting PBBOOL3_TINY_RACERS_PLAYER_WARPED")
						ENDIF
					ENDIF
					
					IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
						CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TINY_RACERS_PLAYER_WARPED)
						TINY_RACERS_SET_PLAYER_ROUND_STATE(TR_Respawn)
					ENDIF 
				ENDIF
					
				IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TINY_RACERS_PLAYER_WARPED)
				OR (IS_LOCAL_PLAYER_ANY_SPECTATOR() 
					AND (NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN))
					AND IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet3, PBBOOL3_TINY_RACERS_PLAYER_WARPED))
					
					SET_BIT(iLocalBoolCheck21, LBOOL21_PREVENT_COUNTDOWN_FROM_STARTING)
					TR_CLEAR_COUNTDOWN_DATA(cduiIntro)
					IF currentSwanDiveCamState != SDSCS_WaitingToPlay
						SET_SWAN_DIVE_CAM_STATE(SDSCS_Reset)
					ENDIF
					TINY_RACERS_SET_PLAYER_ROUND_STATE(TR_Sync)
				ENDIF
			BREAK
			
			CASE TR_Sync
				//Host
				IF bIsLocalPlayerHost
					IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_TINY_RACERS_RERUN_OBJECTIVE_PROGRESSION)
						IF NOT TINY_RACERS_ARE_ALL_RACERS_ON_SAME_RULE()
							PRINTLN("[JT TR] Setting SBBOOL5_TINY_RACERS_RERUN_OBJECTIVE_PROGRESSION")
							SET_BIT(MC_serverBD.iServerBitSet5, SBBOOL5_TINY_RACERS_RERUN_OBJECTIVE_PROGRESSION)
						ENDIF
					ENDIF
					
					IF IS_BIT_SET(MC_serverBD.iServerBitSet4,SBBOOL4_TR_SHOULD_PROGRESS_THROUGH_RESET)
						CLEAR_BIT(MC_serverBD.iServerBitSet4,SBBOOL4_TR_SHOULD_PROGRESS_THROUGH_RESET)
						PRINTLN("[JT TR] Clearing iServerBitSet4,SBBOOL4_TR_SHOULD_PROGRESS_THROUGH_RESET")
					ENDIF
					
					//Resetting countdown server bits
					IF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_FINISHED_COUNTDOWN)
						CLEAR_BIT(MC_serverBD.iServerBitSet2, SBBOOL2_FINISHED_COUNTDOWN)
						PRINTLN("[JT TR] Clearing SBBOOL2_FINISHED_COUNTDOWN")
					ENDIF
					
					IF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_STARTED_COUNTDOWN)
						CLEAR_BIT(MC_serverBD.iServerBitSet2, SBBOOL2_STARTED_COUNTDOWN)
						PRINTLN("[JT TR] Clearing SBBOOL2_STARTED_COUNTDOWN")
					ENDIF
				ENDIF
				
				IF NOT IS_LOCAL_PLAYER_SPECTATOR()
					CLEAN_UP_ALL_WEAPONS(TRUE)
				ENDIF
				
//				IF DOES_CAM_EXIST(g_birdsEyeCam)
//					STOP_CAM_POINTING(g_birdsEyeCam)
//				ENDIF
								
				//Reset player has progressed
				IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TINY_RACERS_PLAYER_HAS_PROGRESSED)
					CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TINY_RACERS_PLAYER_HAS_PROGRESSED)
					PRINTLN("[JT TR] TR_Sync - Clearing iClientBitSet3, PBBOOL3_TINY_RACERS_PLAYER_HAS_PROGRESSED")
				ENDIF
				
				//Reset Warped
				IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
				OR USING_HEIST_SPECTATE()
					CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TINY_RACERS_PLAYER_WARPED)
					PRINTLN("[JT TR] Going back to TR_Respawn")
					TINY_RACERS_SET_PLAYER_ROUND_STATE(TR_Respawn)
				ENDIF

				SET_BIT(iLocalBoolCheck3, LBOOL3_IN_BOUNDS1)
				SET_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_MISSION_BOUNDS)
				
				TINY_RACERS_ADD_PARTICLE_EFFECT()
				
				CLEAN_UP_VEHICLE_SWAP()
				
				SET_BIT(iLocalBoolCheck21, LBOOL21_PREVENT_COUNTDOWN_FROM_STARTING)
				
				TINY_RACERS_PLAYER_LOOP_CHECKS(bAllSameState, bAllWarped, bAllClear321, bAllDead, bAllClearedProgression, bShouldForceProgress,DEFAULT, DEFAULT, TRUE, FALSE, FALSE, TRUE)
				
				IF TINY_RACERS_ARE_ALL_RACERS_READY()
					PRINTLN("[JT TR] Passed ready chec k1")
					IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TINY_RACERS_SCREEN_PAUSED)
					OR IS_BIT_SET(iLocalBoolCheck22, LBOOL22_TINY_RACERS_RENDER_PAUSE_TARGET_DEAD)
					OR MC_serverBD.iTRLastParticipantToUse = -1
					OR NOT NETWORK_IS_PARTICIPANT_ACTIVE(MC_serverBD.piTRLastPartStanding)
						PRINTLN("[JT TR] Passed check 2")
						IF bAllWarped
							PRINTLN("[JT TR] Passed check 3")
							IF DOES_CAM_EXIST(g_birdsEyeCam)
								SET_CAM_MOTION_BLUR_STRENGTH(g_birdsEyeCam, fTRMotionBlurStrength)
								TINY_RACERS_SET_MOTION_BLUR_ON_VEHICLES(FALSE)
								SET_CAM_DOF_FNUMBER_OF_LENS(g_birdsEyeCam, fTRNumLens)
								SET_CAM_DOF_FOCAL_LENGTH_MULTIPLIER(g_birdsEyeCam, fTRFocalLength)
								SET_CAM_DOF_MAX_NEAR_IN_FOCUS_DISTANCE(g_birdsEyeCam, BirdsEyeTarget.z)
								SET_CAM_DOF_MAX_NEAR_IN_FOCUS_DISTANCE_BLEND_LEVEL(g_birdsEyeCam, fTRBlendLevelNearIn)
								SET_CAM_DOF_STRENGTH(g_birdsEyeCam, fTRDOF)
							ENDIF
							TINY_RACERS_WINNER_CAM_POST_FX(iLocalPart, TRUE)
							TINY_RACERS_RENDER_PHASE_PAUSE(TRUE)
						ENDIF
					ENDIF	
				ENDIF			
				
				
				IF IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_TINY_RACERS_ROUND_WAS_A_DRAW)
					IF NOT IS_BIT_SET(iLocalBoolCheck21, LBOOL21_TINY_RACERS_ROUND_SHARD_SHOWN)
						IF SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_LOSER,"TR_DRAW", "TR_DRAWSL")
							SET_BIT(iLocalBoolCheck21, LBOOL21_TINY_RACERS_ROUND_SHARD_SHOWN)
							PRINTLN("[JT TR] TR_LastManTimer - Setting up Shard")
						ENDIF
					ENDIF
				ENDIF
				
				TR_CLEAR_COUNTDOWN_DATA(cduiIntro)
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciUSE_MORE_ACCURATE_RACE_POSITION)
					MC_playerBD[iLocalPart].iCheckPointForPosition = -1
					MC_playerBD[iLocalPart].iPreviousCheckPointForPosition = -1
					INT iPreviousCheckpointRule
					iPreviousCheckpointRule =  MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam] - 1
					IF iPreviousCheckpointRule <= -1
						iPreviousCheckpointRule = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iNumberOfTeamRules - 1
					ENDIF
					MC_playerBD[iLocalPart].iCheckPointForPosition = GET_LOCATION_FROM_RULE(iPreviousCheckpointRule, MC_playerBD[iLocalPart].iTeam)
					PRINTLN("[JS] - PROCESS_ACCURATE_RACE_POSITION - current loc: ", MC_playerBD[iLocalPart].iCheckPointForPosition)
					iPreviousCheckpointRule--
					IF iPreviousCheckpointRule <= -1
						iPreviousCheckpointRule = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iNumberOfTeamRules - 1
					ENDIF
					MC_playerBD[iLocalPart].iPreviousCheckPointForPosition = GET_LOCATION_FROM_RULE(iPreviousCheckpointRule, MC_playerBD[iLocalPart].iTeam)
					PRINTLN("[JS] - PROCESS_ACCURATE_RACE_POSITION - previous loc: ", MC_playerBD[iLocalPart].iCheckPointForPosition)
					CLEAR_BIT(iLocalBoolCheck22, LBOOL22_INSIDE_PREVIOUS_CHECKPOINT)
					CLEAR_BIT(iLocalBoolCheck22, LBOOL22_INSIDE_NEXT_CHECKPOINT)
					MC_playerBD[iLocalPart].iLapOffset = 0
					vCachedCheckpointEntry = <<0,0,0>>
				ENDIF
				
				IF NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_TINY_RACERS_TOD_RESET)
					SET_TIME_OF_DAY(MC_serverBD.iTimeOfDay, TRUE)
					IF ANIMPOSTFX_IS_RUNNING("DeadlineNeon")
						ANIMPOSTFX_STOP("DeadlineNeon")
					ENDIF
					SET_BIT(iLocalBoolCheck24, LBOOL24_TINY_RACERS_TOD_RESET)
				ENDIF
				
				//Sorting out wheels for the Blazer
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iTeamBitset2, ciBS2_START_WITH_WHEELS_UP)
					IF GET_ENTITY_MODEL( GET_VEHICLE_PED_IS_IN(LocalPlayerPed)) = BLAZER5
						IF IS_VEHICLE_IN_WATER(vehTemp)
							SET_WHEELS_RETRACTED_INSTANTLY(vehTemp)
							PRINTLN("[JT][RCC MISSION] Retracting wheels - TR_Sync")
						ELSE
							SET_WHEELS_EXTENDED_INSTANTLY(vehTemp)
							PRINTLN("[JT][RCC MISSION] Extending wheels - TR_Sync")
						ENDIF
					ENDIF
				ENDIF
				//Making sure vehicle is fixed up
				IF IS_VEHICLE_DRIVEABLE(vehTemp)
					SET_VEHICLE_ENGINE_ON(vehTemp,TRUE,TRUE)
					SET_VEHICLE_ENGINE_HEALTH(vehTemp, 1000.0)
					SET_VEHICLE_PETROL_TANK_HEALTH(vehTemp, 1000.0)
					SET_VEHICLE_FIXED(vehTemp)
					SET_ENTITY_HEALTH(vehTemp,GET_ENTITY_MAX_HEALTH(vehTemp))
				ENDIF
				
				IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
					CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TINY_RACERS_PLAYER_WARPED)
					TINY_RACERS_SET_PLAYER_ROUND_STATE(TR_Respawn)
				ENDIF
				
				
				
				IF TINY_RACERS_ARE_ALL_RACERS_READY(TRUE)
					PRINTLN("[JT TR] TR_Sync - All racers are ready for control")
					IF bAllWarped
						PRINTLN("[JT TR] 1")
						IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet4,SBBOOL4_TR_SHOULD_PROGRESS_THROUGH_RESET)
						AND bAllClearedProgression
						AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_STARTED_COUNTDOWN)
							IF NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
							OR (HAS_NET_TIMER_STARTED(tdTRBigMessageStuckTimer) AND HAS_NET_TIMER_EXPIRED(tdTRBigMessageStuckTimer, ciTRBIG_MESSAGE_STUCK_TIME))
								PRINTLN("[JT TR] TR_Sync - All racers are warped")	
								TINY_RACERS_SET_PLAYER_ROUND_STATE(TR_321)
								RESET_NET_TIMER(tdTRBigMessageStuckTimer)
							ELSE
								IF NOT HAS_NET_TIMER_STARTED(tdTRBigMessageStuckTimer)
									REINIT_NET_TIMER(tdTRBigMessageStuckTimer)
								ENDIF
							ENDIF
						ELSE
							IF IS_BIT_SET(MC_serverBD.iServerBitSet4,SBBOOL4_TR_SHOULD_PROGRESS_THROUGH_RESET)
								PRINTLN("[JT TR] TR_Sync - server force progression not reset")
							ENDIF
							IF NOT bAllClearedProgression
								PRINTLN("[JT TR] TR_Sync - Someone still has progression set.")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			BREAK
			
			CASE TR_321
				
				IF NOT HAS_NET_TIMER_STARTED(tdTR321StuckTimer)
					REINIT_NET_TIMER(tdTR321StuckTimer)
				ELSE
					IF HAS_NET_TIMER_EXPIRED(tdTR321StuckTimer, ciTR321_STUCK_TIME)
						g_bTDForceThroughCurrentState = TRUE
					ENDIF
				ENDIF
				// This needs to be stopped before the 321_go sounds.
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TINY_RACERS_SOUNDS)
					IF IS_AUDIO_SCENE_ACTIVE("DLC_SR_TR_Winner_Screen_Scene")
						STOP_AUDIO_SCENE("DLC_SR_TR_Winner_Screen_Scene")
					ENDIF
				ENDIF
				
				//Countdown
				IF bIsLocalPlayerHost
					CLEAR_BIT(MC_serverBD.iServerBitSet4,SBBOOL4_TR_SHOULD_PROGRESS_THROUGH_RESET)
					PRINTLN("[JT TR] TR_321 Clearing iServerBitSet4,SBBOOL4_TR_SHOULD_PROGRESS_THROUGH_RESET")
				ENDIF
				
				IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TINY_RACERS_PLAYER_HAS_PROGRESSED)
					CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TINY_RACERS_PLAYER_HAS_PROGRESSED)
					PRINTLN("[JT TR] TR_321 Clearing PBBOOL3_TINY_RACERS_PLAYER_HAS_PROGRESSED")
				ENDIF
				
				IF g_SpawnData.bIsSwoopedUp
					g_SpawnData.bIsSwoopedUp = FALSE
				ENDIF
					
				IF IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
					CLEAR_ALL_BIG_MESSAGES()
				ENDIF
				
				TINY_RACERS_PLAYER_LOOP_CHECKS(bAllSameState, bAllWarped, bAllClear321, bAllDead, bAllClearedProgression, bShouldForceProgress,TRUE, TR_321, FALSE, TRUE)
				
				IF IS_BIT_SET(iLocalBoolCheck21, LBOOL21_PREVENT_COUNTDOWN_FROM_STARTING)
					IF bAllSameState
						PRINTLN("[JT TR] TR_321 Clearing LBOOL21_PREVENT_COUNTDOWN_FROM_STARTING")
						CLEAR_BIT(iLocalBoolCheck21, LBOOL21_PREVENT_COUNTDOWN_FROM_STARTING)
					ENDIF
				ENDIF
				
				IF NOT IS_BITMASK_AS_ENUM_SET(cduiIntro.iBitFlags, CNTDWN_UI2_Played_Go)
					DISABLE_PLAYER_VEHICLE_CONTROLS_THIS_FRAME()
				ENDIF

				
				IF NOT IS_BIT_SET(iLocalBoolCheck21, LBOOL21_PREVENT_COUNTDOWN_FROM_STARTING)
					IF TR_REQUEST_COUNTDOWN_UI(cduiIntro)
						IF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_FINISHED_COUNTDOWN)
							SET_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TINY_RACERS_321_FINISHED)
						ENDIF						
					ENDIF 
				ENDIF
				
				IF bAllClear321
				AND NOT IS_BIT_SET(iLocalBoolCheck21, LBOOL21_PREVENT_COUNTDOWN_FROM_STARTING)
					TINY_RACERS_SET_PLAYER_ROUND_STATE(TR_Reset)
				ENDIF
				
			BREAK
			
			CASE TR_Reset
			
				IF bIsLocalPlayerHost
					//Reset Timers
					IF HAS_NET_TIMER_STARTED(MC_serverBD_1.stTRBoundsShrinkTimer)
						RESET_NET_TIMER(MC_serverBD_1.stTRBoundsShrinkTimer)
						PRINTLN("[JT TR] TR_Reset - Resetting shrink timer")
					ENDIF
					
					IF HAS_NET_TIMER_STARTED(MC_serverBD_1.stTRLastPlayerTimer)
						RESET_NET_TIMER(MC_serverBD_1.stTRLastPlayerTimer)
						PRINTLN("[JT TR] TR_Reset - Resetting stTRLastPlayerTimer")
					ENDIF
					
					PRINTLN("[JT TR] TR_Reset - Clearing LBOOL21_TINY_RACERS_ROUND_SHARD_SHOWN")
					CLEAR_BIT(MC_serverBD.iServerBitSet5, SBBOOL5_TINY_RACERS_ROUND_WAS_A_DRAW)
					
					CLEAR_BIT(iLocalBoolCheck21, LBOOL21_TINY_RACERS_SPAWN_POINT_SET)
					MC_serverBD.iTRLastParticipantToUse = -1
					MC_serverBD.piTRLastPartStanding =  INVALID_PARTICIPANT_INDEX()
					RESET_NET_TIMER(MC_serverBD.tdTRSpawnTimer)
				ENDIF
				
				//Score
				CLEAR_BIT(iLocalBoolCheck21, LBOOL21_TINY_RACERS_SCORE_INCREMENTED)
				PRINTLN("[JT TR] TR_Reset - Clearing LBOOL21_TINY_RACERS_SCORE_INCREMENTED")
				//Shards
				CLEAR_BIT(iLocalBoolCheck21, LBOOL21_TINY_RACERS_ROUND_SHARD_SHOWN)
				
				PRINTLN("[JT TR] TR_Reset - Clearing LBOOL22_TINY_RACERS_PLAY_DRAW_SHARD")
				//Camera is in position
				CLEAR_BIT(iLocalBoolCheck22, LBOOL22_TINY_RACERS_CAMERA_IN_POSITION)
				PRINTLN("[JT TR] TR_Reset - Clearing LBOOL22_TINY_RACERS_CAMERA_IN_POSITION")
				//Visual Bounds
				CLEAR_BIT(iLocalBoolCheck22, LBOOL22_TINY_RACERS_OK_TO_PROCESS_BOUNDS)
				PRINTLN("[JT TR] TR_Reset - Clearing LBOOL22_TINY_RACERS_OK_TO_PROCESS_BOUNDS")
				//Petrol Tank Health
				CLEAR_BIT(iLocalBoolCheck22, LBOOL22_VEHICLE_PETROL_TANK_DAMAGE_DISABLED)
				PRINTLN("[JT TR] TR_Reset - Clearing LBOOL22_VEHICLE_PETROL_TANK_DAMAGE_DISABLED")
				//Resetting Time of Day and TRON FX
				CLEAR_BIT(iLocalBoolCheck24, LBOOL24_TINY_RACERS_TOD_RESET)
				PRINTLN("[JT TR] TR_Reset - Clearing LBOOL24_TINY_RACERS_TOD_RESET")
				//Allowing to pass through pause with dead play
				IF IS_BIT_SET(iLocalBoolCheck22, LBOOL22_TINY_RACERS_RENDER_PAUSE_TARGET_DEAD)
					CLEAR_BIT(iLocalBoolCheck22, LBOOL22_TINY_RACERS_RENDER_PAUSE_TARGET_DEAD)
					PRINTLN("[JT TR] TR_Reset - Clearing LBOOL22_TINY_RACERS_RENDER_PAUSE_TARGET_DEAD")
				ENDIF
				//Clearing spectator motion blur
				CLEAR_BIT(iLocalBoolCheck22, LBOOL22_TINY_RACERS_CAM_SPECTATOR_MOTION_BLUR_OFF)
				PRINTLN("[JT TR] TR_Reset - Clearing LBOOL22_TINY_RACERS_CAM_SPECTATOR_MOTION_BLUR_OFF")
				
				//Shrinking bounds
				CLEAR_BIT(iLocalBoolCheck21, LBOOL21_TINY_RACERS_SHRINKING_BOUNDS_SIZE_SET)
				fCurrentSphereRadius = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fRadius
				PRINTLN("[JT TR] TR_Reset - Clearing LBOOL21_TINY_RACERS_SHRINKING_BOUNDS_SIZE_SET")
				RESET_NET_TIMER(MC_playerBD[iLocalPart].tdBoundstimer)
				RESET_NET_TIMER(MC_playerBD[iLocalPart].tdBounds2timer)
				RESET_NET_TIMER(MC_playerBD[iLocalPart].tdMapBoundsTimer)
				
				//Respawning
				CLEAR_BIT(iLocalBoolCheck21, LBOOL21_SPECTATOR_RESPAWN_FORCE_RESPAWN)
				PRINTLN("[JS] [SPECREZ] - TR_Reset - FORCE RESPAWN CLEARED")
				
				IF HAS_NET_TIMER_STARTED(td_TR_WarpWait)
					PRINTLN("[JT TR] TR_Reset - Resetting td_TR_WarpWait")
					RESET_NET_TIMER(td_TR_WarpWait)
				ENDIF
				
				IF HAS_NET_TIMER_STARTED(tdStuckInitTimer)
					PRINTLN("[JT TR] TR_Reset - Resetting tdStuckInitTimer")
					RESET_NET_TIMER(tdStuckInitTimer)
				ENDIF
				RESET_NET_TIMER(tdTRWarpDelay)
				PRINTLN("[JT TR] TR_Reset - Resetting tdTRWarpDelay")
				
				RESET_NET_TIMER(tdTR321StuckTimer)
				PRINTLN("[JT TR] TR_Reset - Resetting tdTR321StuckTimer")
				
				TINY_RACERS_SET_PLAYER_ROUND_STATE(TR_Racing)
			BREAK
			
			CASE TR_Invalid
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

///
///    END OF TINY RACERS
//////////////////////////////

/////////////////////////////////////////////////
///    OLD SCHOOL GTA				//////////
///    Lorcan Henry				//////////
////////////////////////////////////
///    

PROC SKIP_SKIPPABLE_HELP_TEXT()
	IF NOT IS_STRING_EMPTY(sTLToSkip)
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sTLToSkip)
			PRINTLN("[JT HELP] HELP TEXT BEING CLEARED")
			CLEAR_HELP(TRUE)
		ENDIF
	ENDIF
	IF IS_THIS_ROCKSTAR_MISSION_SVM_RUINER2(g_FMMC_STRUCT.iRootContentIDHash)
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SVM_RUINER2_HELP5")
			PRINTLN("[JT HELP] SVM_RUINER2_HELP5 BEING CLEARED in ruiner mission")
			CLEAR_HELP(TRUE)
		ENDIF
	ENDIF
ENDPROC

PROC DISABLE_MOVEMENT_CONTROLS()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD, TRUE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR, TRUE)
ENDPROC

PROC DISABLE_CONTROLS()

	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK, TRUE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2, TRUE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM, TRUE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL, TRUE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO, TRUE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_THROW_GRENADE, TRUE)
	
	DISABLE_PLAYER_THROW_GRENADE_WHILE_USING_GUN()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK, TRUE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE, TRUE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PICKUP, TRUE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK, TRUE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2, TRUE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM, TRUE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK, TRUE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_ATTACK, TRUE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON, TRUE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_NEXT_WEAPON, TRUE)
ENDPROC
PROC BLOCK_ACTIONS_FOR_TOP_DOWN_MODE_ON_FOOT()
	IF NOT IS_PED_INJURED(localPlayerPed)
		IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			
			DISABLE_CONTROLS()

			WEAPON_TYPE wt = WEAPONTYPE_INVALID
			GET_CURRENT_PED_WEAPON(LocalPlayerPed, wt)
			
			IF wt != WEAPONTYPE_UNARMED
				SET_PED_RESET_FLAG(LocalPlayerPed, PRF_BlockWeaponFire, TRUE)
				DISABLE_PLAYER_FIRING(localPlayer, TRUE)
				
				IF IS_THROWN_WEAPON(wt)
					SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED, TRUE)
					REMOVE_ALL_PROJECTILES_OF_TYPE(WEAPONTYPE_GRENADE, FALSE)
					REMOVE_ALL_PROJECTILES_OF_TYPE(WEAPONTYPE_STICKYBOMB, FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC BIRDSEYE_PLAYER_TOGGLE()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciPRON_MODE_ENABLED)
	AND LGHTCYCLE_PLAYER[MC_playerBD[iPartToUse].iteam ].bIsStartingUp AND NOT bUsingBirdsEyeCam
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_TURF_WAR_TOP_DOWN_CAMERA)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_TINY_RACERS_CAMERA)
		SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
		PRINTLN("[JS] Can't switch to Birdseye cam until after the boost")
		EXIT
	ENDIF
	
	IF NOT IS_INTERPOLATING_TO_SCRIPT_CAMS()
	AND NOT IS_INTERPOLATING_FROM_SCRIPT_CAMS()
		bSnapCam = FALSE
	ENDIF	
	
	IF NOT IS_PED_IN_ANY_VEHICLE(localPlayerPed)
	AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_TURF_WAR_TOP_DOWN_CAMERA)
		
	ELSE
		SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
	ENDIF
	
	BOOL bSpectatorCheck = IS_LOCAL_PLAYER_ANY_SPECTATOR()
	PRINTLN("[BIRDSEYE_PLAYER_TOGGLE] Calling function")
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_TURF_WAR_TOP_DOWN_CAMERA)
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_TINY_RACERS_CAMERA)
	OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciFORCE_STATIC_CAMERA_LOCATIONS) AND (IS_LOCAL_PLAYER_ANY_SPECTATOR() OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)))
	OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TOP_DOWN_CAM_FOR_RESPAWN_SPECTATORS) AND IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN))
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
		bSpectatorCheck = FALSE
	ENDIF
	//Don't let the player toggle between states if the phone or menus are on screen
	IF NOT IS_PHONE_ONSCREEN()
	AND NOT IS_CUSTOM_MENU_ON_SCREEN()
	AND NOT IS_PAUSE_MENU_ACTIVE()
	AND NOT bSpectatorCheck
	AND NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CHARACTER_WHEEL)
	AND NOT IS_WARNING_MESSAGE_ACTIVE()
	AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appinternet")) = 0
	AND ((NOT IS_PED_INJURED(LocalPlayerPed)) OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN) OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType))
		PRINTLN("[BIRDSEYE_PLAYER_TOGGLE] Passing checks")
		BOOL bSwitchButtonPressed = FALSE
		// If we're playing Turf Wars and we are a spectator then listen for the following input. Else use the Default.
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_TURF_WAR_TOP_DOWN_CAMERA)
		OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciFORCE_STATIC_CAMERA_LOCATIONS) AND (IS_LOCAL_PLAYER_ANY_SPECTATOR() OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)))
			IF IS_LOCAL_PLAYER_ANY_SPECTATOR() OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
				IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RS))
					bSwitchButtonPressed = TRUE
				ENDIF
			ELSE
				IF NOT IS_PED_IN_ANY_VEHICLE(localPlayerPed)
					IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_ENTER)
						IF NOT HAS_NET_TIMER_STARTED(tdTopDownBlockDelay) OR HAS_NET_TIMER_EXPIRED_READ_ONLY(tdTopDownBlockDelay, ci_TD_DELAY)
							RESET_NET_TIMER(tdTopDownBlockDelay)
							START_NET_TIMER(tdTopDownBlockDelay)
							bSwitchButtonPressed = TRUE
							
							IF IS_PLAYER_FREE_AIMING(LocalPlayer)							
								CLEAR_PED_TASKS(LocalPlayerPed)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
						bSwitchButtonPressed = TRUE
					ENDIF
				ENDIF
			ENDIF
		ELIF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_TINY_RACERS_CAMERA)
			AND IS_LOCAL_PLAYER_ANY_SPECTATOR())
		OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_TINY_RACERS_CAMERA) 
			AND IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN))
		OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TOP_DOWN_CAM_FOR_RESPAWN_SPECTATORS) AND IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN))
			PRINTLN("[BIRDSEYE_PLAYER_TOGGLE] In Look behind button press")
			IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RS))
				bSwitchButtonPressed = TRUE
			ENDIF
		ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
			IF IS_LOCAL_PLAYER_ANY_SPECTATOR() OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
				IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RS))
					bSwitchButtonPressed = TRUE
				ENDIF
			ELSE
				IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
					bSwitchButtonPressed = TRUE
				ENDIF
			ENDIF
		ELSE
			IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
				bSwitchButtonPressed = TRUE
			ENDIF
		ENDIF
		
		IF bSwitchButtonPressed
			PRINTLN("[BIRDSEYE_PLAYER_TOGGLE] Button pressed set")
			IF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciFORCE_STATIC_CAMERA_LOCATIONS) AND (IS_LOCAL_PLAYER_ANY_SPECTATOR() OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)))
				PRINTLN("[LM][ASSIGN_NUMBER_OF_VALID_FORCE_STATIC_CAMERA_LOCATIONS] - g_iFMMCCurrentStaticCam: ", g_iFMMCCurrentStaticCam)
				g_iFMMCCurrentStaticCam++
				
				IF g_iFMMCCurrentStaticCam < iValidStaticCams
				AND bUsingBirdsEyeCam
					EXIT
				ENDIF
			ENDIF
			
			BOOL bAllowButtonCheck = FALSE
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_TURF_WAR_TOP_DOWN_CAMERA)
			OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_TINY_RACERS_CAMERA)
			OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciFORCE_STATIC_CAMERA_LOCATIONS) AND (IS_LOCAL_PLAYER_ANY_SPECTATOR() OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)))
			OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TOP_DOWN_CAM_FOR_RESPAWN_SPECTATORS) AND IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN))
			OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
				bAllowButtonCheck = TRUE
			ENDIF
		
			IF NOT IS_INTERPOLATING_TO_SCRIPT_CAMS()
			AND NOT IS_INTERPOLATING_FROM_SCRIPT_CAMS()
			OR bAllowButtonCheck		
				PRINTLN("[BIRDSEYE_PLAYER_TOGGLE] Able to change camera")
				IF bUsingBirdsEyeCam = TRUE
					IF bAllowButtonCheck
						PRINTLN("[LM][BIRDSEYE_PLAYER_TOGGLE] Setting Spectator Birdseye Cam FALSE")
						CLEAR_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_TURF_WARS_CAMER_ACTIVE)
						REQUEST_SPEC_INSTRUCTIONAL_BUTTON_UPDATE()
					ENDIF
					PRINTLN("[BIRDSEYE_PLAYER_TOGGLE] Using Birds eye cam false")
					bUsingBirdsEyeCam = FALSE
					
					IF g_iFMMCCurrentStaticCam >= iValidStaticCams
						g_iFMMCCurrentStaticCam = -1
					ENDIF
				ELSE
					IF bAllowButtonCheck
						PRINTLN("[LM][BIRDSEYE_PLAYER_TOGGLE] Setting Spectator Birdseye Cam TRUE")
						SET_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_TURF_WARS_CAMER_ACTIVE)
						REQUEST_SPEC_INSTRUCTIONAL_BUTTON_UPDATE()
					ENDIF
					PRINTLN("[BIRDSEYE_PLAYER_TOGGLE] Using birds eye true")
					bUsingBirdsEyeCam = TRUE
				ENDIF
			ELSE
				bSnapCam = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) = CAM_VIEW_MODE_FIRST_PERSON
		bSnapCam = TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_TURF_WAR_TOP_DOWN_CAMERA)
	OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciFORCE_STATIC_CAMERA_LOCATIONS) AND (IS_LOCAL_PLAYER_ANY_SPECTATOR() OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)))
		bSnapCam = TRUE
	ENDIF
ENDPROC

PROC SET_BIRDSEYE_CAM_TARGETS()

	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_TURF_WAR_TOP_DOWN_CAMERA)
	AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vTWCamStaticPosition)
		BirdsEyeTarget = g_FMMC_STRUCT.vTWCamStaticPosition
		
		IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARS(g_FMMC_STRUCT.iAdversaryModeType)
			BirdsEyeCurrentRotVector = g_FMMC_STRUCT.vTWCamStaticRotation
		ELSE
			BirdsEyeTargetRot = g_FMMC_STRUCT.vTWCamStaticRotation.z
		ENDIF
	ELIF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciFORCE_STATIC_CAMERA_LOCATIONS) AND (IS_LOCAL_PLAYER_ANY_SPECTATOR() OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)))
		IF g_iFMMCCurrentStaticCam > -1
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vBEVCamFSLPos[g_iFMMCCurrentStaticCam])
				BirdsEyeTarget = g_FMMC_STRUCT.vBEVCamFSLPos[g_iFMMCCurrentStaticCam]
				BirdsEyeCurrentRotVector = g_FMMC_STRUCT.vBEVCamFSLRotation[g_iFMMCCurrentStaticCam]
			ENDIF
		ENDIF
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_TINY_RACERS_CAMERA)
	OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TOP_DOWN_CAM_FOR_RESPAWN_SPECTATORS) AND IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN))
		VECTOR PlayerPos, vVelocity

		//TARGET X & Y
		PED_INDEX tempPed = LocalPlayerPed
		
		IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
		OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
			IF iSpectatorTarget != -1
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iSpectatorTarget))
					tempPed = GET_PLAYER_PED(INT_TO_PLAYERINDEX(iSpectatorTarget))
				ENDIF
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(tempPed)
			
			IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
			OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
				
				IF IS_PED_IN_ANY_VEHICLE(tempPed)
					vPlayerTRPosSpec = GET_ENTITY_COORDS(tempPed, FALSE)
					vPlayerTRVelSpec = GET_ENTITY_VELOCITY(tempPed)
				ELSE
					vPlayerTRPosSpec += <<vPlayerTRVelSpec.x * 0.03, vPlayerTRVelSpec.y * 0.03, vPlayerTRVelSpec.z * 0.03>>
				ENDIF
				
				PlayerPos = vPlayerTRPosSpec
				vVelocity = vPlayerTRVelSpec
			ELSE
				PlayerPos = GET_ENTITY_COORDS(tempPed, FALSE)
				vVelocity = GET_ENTITY_VELOCITY(tempPed)
			ENDIF
			
			FLOAT fBESpeedHeight = 0
			
			VECTOR vForwardOffset = GET_ENTITY_FORWARD_VECTOR(tempPed) * fBasePosOffsetTR
			BirdsEyeTarget.x = PlayerPos.x + ((vVelocity.x * fDistanceMultiplierTR) + vForwardOffset.x)
			BirdsEyeTarget.y = PlayerPos.y + ((vVelocity.y * fDistanceMultiplierTR) + vForwardOffset.y)
			
			//TARGET Z & ROT
			IF IS_PED_IN_ANY_VEHICLE(tempPed)
				VEHICLE_INDEX viPlayerVeh = GET_VEHICLE_PED_IS_IN(tempPed)
				fBESpeedHeight = (GET_ENTITY_SPEED(viPlayerVeh)*fSpeedMultiplierTR)
				
				IF fBESpeedHeight > fMinHeightTR
					fBESpeedHeight = fMinHeightTR
				ENDIF
				
				IF IS_ENTITY_IN_AIR(viPlayerVeh)
					IF GET_ENTITY_HEIGHT_ABOVE_GROUND(viPlayerVeh) > 10.0
						SET_BIT(iLocalBoolCheck22, LBOOL22_TINY_RACERS_FREEZE_CAMERA_ROTATION)
						SET_BIT(iLocalBoolCheck22, LBOOL22_TINY_RACERS_EASE_CAMERA_ROTATION)
					ENDIF
				ELSE
					CLEAR_BIT(iLocalBoolCheck22, LBOOL22_TINY_RACERS_FREEZE_CAMERA_ROTATION)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciBIRDS_EYE_LOCK_NORTH)
			OR bLockNorth
				BirdsEyeTargetRot = 0.0
			ELSE
				BirdsEyeTarget.z = PlayerPos.z + fBEBaseHeightTR + fBESpeedHeight
				IF NOT IS_PED_DEAD_OR_DYING(tempPed)
				AND NOT IS_BIT_SET(iLocalBoolCheck22, LBOOL22_TINY_RACERS_FREEZE_CAMERA_ROTATION)
					
					VECTOR vPlayerRot = GET_ENTITY_ROTATION(tempPed)
					BirdsEyeTargetRot = vPlayerRot.z
				ENDIF
			ENDIF
		ENDIF
	ELSE
		// TARGET X & Y
		VECTOR PlayerPos = GET_ENTITY_COORDS(LocalPlayerPed)
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			VECTOR vVelocity = GET_ENTITY_VELOCITY(LocalPlayerPed)
			IF bUseFixedCameraOffset
				fDistanceMultiplier = 0
				fDirectionalOffsetMultiplier = 0
				VECTOR vForwardOffset = (GET_ENTITY_FORWARD_VECTOR(LocalPlayerPed) * fBasePosOffset)
				BirdsEyeTarget.x = PlayerPos.x + ((vVelocity.x * fDistanceMultiplier) + vForwardOffset.x)
				BirdsEyeTarget.y = PlayerPos.y + ((vVelocity.y * fDistanceMultiplier) + vForwardOffset.y)
			ELSE
				BirdsEyeTarget.x = PlayerPos.x + (vVelocity.x * fDistanceMultiplier)
				BirdsEyeTarget.y = PlayerPos.y + (vVelocity.y * fDistanceMultiplier)
			ENDIF
		ELSE
			BirdsEyeTarget.x = PlayerPos.x
			BirdsEyeTarget.y = PlayerPos.y
		ENDIF
		
		// TARGET Z
		FLOAT fBESpeedHeight = 0
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			VEHICLE_INDEX viPlayerVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
			fBESpeedHeight = (GET_ENTITY_SPEED(viPlayerVeh)*fSpeedMultiplier)
			IF fBESpeedHeight > fBEMinHeight
				fBESpeedHeight = fBEMinHeight
			ENDIF
		ENDIF
		
		BirdsEyeTarget.z = PlayerPos.z + fBEBaseHeight + fBESpeedHeight
		
		// TARGET ROT
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciBIRDS_EYE_LOCK_NORTH)
		OR bLockNorth
			BirdsEyeTargetRot = 0.0
		ELSE
			IF NOT IS_PED_DEAD_OR_DYING(LocalPlayerPed)
				VECTOR vPlayerRot = GET_ENTITY_ROTATION(LocalPlayerPed)
				BirdsEyeTargetRot = vPlayerRot.z
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC TINY_RACERS_TOP_DOWN_LOOK_BACK()
	VEHICLE_INDEX vehToCheck
	
	IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_LOOK_BEHIND)
	AND NATIVE_TO_INT(GET_NETWORK_TIME()) > iTRLookBackTimer
	AND IS_BIT_SET(iLocalBoolCheck21, LBOOL21_TINY_RACERS_LOOK_BACK_SET)
		fBasePosOffsetTR = INTERP_FLOAT(fBasePosOffsetTR, cf_LookBackCamPosition, 0.5)
		
	ELIF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_LOOK_BEHIND)
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			vehToCheck = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
			IF IS_ENTITY_ALIVE(vehToCheck)
				IF NOT IS_ENTITY_IN_AIR(vehToCheck)
					IF NOT IS_BIT_SET(iLocalBoolCheck21, LBOOL21_TINY_RACERS_LOOK_BACK_SET)
						SET_BIT(iLocalBoolCheck21, LBOOL21_TINY_RACERS_LOOK_BACK_SET)
						iTRLookBackTimer = NATIVE_TO_INT(GET_NETWORK_TIME()) + 30
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT bTRAllowOffsetChange
			IF fBasePosOffsetTR != cf_BasePosOffsetTR
				fBasePosOffsetTR = INTERP_FLOAT(fBasePosOffsetTR, cf_BasePosOffsetTR, 0.5)
			ENDIF
		ENDIF
		
		CLEAR_BIT(iLocalBoolCheck21, LBOOL21_TINY_RACERS_LOOK_BACK_SET)
	ENDIF
ENDPROC

PROC INTERP_BIRDSEYE_POS()

	//Flatten Vectors
	VECTOR FlatCurrent = <<BirdsEyeCurrent.x, BirdsEyeCurrent.y, 0.0>>
	VECTOR FlatTarget = <<BirdsEyeTarget.x, BirdsEyeTarget.y, 0.0>>

	VECTOR vDirectionalVector = FlatCurrent - FlatTarget	//Backwards directional vector so we can shift away from the Target
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_TINY_RACERS_CAMERA)
		vDirectionalVector = NORMALISE_VECTOR(vDirectionalVector)
		vDirectionalVector = FlatTarget + (vDirectionalVector * fDirectionalOffsetMultiplierTR)
	ELSE
		vDirectionalVector = NORMALISE_VECTOR(vDirectionalVector)
		
		vDirectionalVector = FlatTarget + (vDirectionalVector * fDirectionalOffsetMultiplier)
		
	ENDIF
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_TINY_RACERS_CAMERA)
	OR IS_TINY_RACERS_CAMERA_IN_POSITION()
		BirdsEyeCurrent = <<vDirectionalVector.X, vDirectionalVector.Y, BirdsEyeCurrent.Z>>
	ENDIF
	
	IF BirdsEyeTargetRot > 170 AND BirdsEyeCurrentRot < 0
		BirdsEyeCurrentRot += 360
	ENDIF
	IF BirdsEyeTargetRot < -170 AND BirdsEyeCurrentRot > 0
		BirdsEyeCurrentRot -= 360
	ENDIF
	
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_TINY_RACERS_CAMERA)
		IF NOT IS_TINY_RACERS_CAMERA_IN_POSITION()
			BirdsEyeCurrent.x = INTERP_FLOAT(BirdsEyeCurrent.x, BirdsEyeTarget.x, fBirdsEyeInterpSpeed/*g_FMMC_STRUCT.fBEVCamHeightDampening*/)
			BirdsEyeCurrent.y = INTERP_FLOAT(BirdsEyeCurrent.y, BirdsEyeTarget.y, fBirdsEyeInterpSpeed/*g_FMMC_STRUCT.fBEVCamHeightDampening*/)
			BirdsEyeCurrent.z = INTERP_FLOAT(BirdsEyeCurrent.z, BirdsEyeTarget.z, fBirdsEyeInterpSpeed/*g_FMMC_STRUCT.fBEVCamHeightDampening*/)
		ELSE
			BirdsEyeCurrent.z = INTERP_FLOAT(BirdsEyeCurrent.z, BirdsEyeTarget.z, g_FMMC_STRUCT.fBEVCamHeightDampening)
		ENDIF
		
		FLOAT fTRCamRot
		
		PED_INDEX tempPed
		IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
		OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
			IF iSpectatorTarget != -1
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iSpectatorTarget))
					tempPed = GET_PLAYER_PED(INT_TO_PLAYERINDEX(iSpectatorTarget))
				ENDIF
			ENDIF
		ELSE
			tempPed = LocalPlayerPed
		ENDIF
		
		IF NOT IS_PED_INJURED(tempPed)
			IF IS_PED_IN_ANY_VEHICLE(tempPed)
				VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(tempPed)
				IF DOES_ENTITY_EXIST(tempVeh)
					IF NOT IS_ENTITY_IN_AIR(tempVeh)
						
						fTRCamRot = fBirdsEyeRotInterpSpeed
					ELSE
						bUseBirdsEyeCamOnRespawn = FALSE
						IF fBirdsEyeRotInterpSpeed != 0
							fTRCamRot = (fBirdsEyeRotInterpSpeed / 2)
						ELSE
							fTRCamRot = fBirdsEyeRotInterpSpeed
						ENDIF
					ENDIF
					
					IF IS_BIT_SET(iLocalBoolCheck22, LBOOL22_TINY_RACERS_EASE_CAMERA_ROTATION)
						fTRCamRot = (fBirdsEyeRotInterpSpeed / 5)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
		BirdsEyeCurrentRot = INTERP_FLOAT(BirdsEyeCurrentRot, BirdsEyeTargetRot, fTRCamRot)

		IF NOT IS_BIT_SET(iLocalBoolCheck22, LBOOL22_TINY_RACERS_FREEZE_CAMERA_ROTATION)
			IF IS_ANGLE_WITHIN_RANGE_OF_ANGLE_WRAPPED(BirdsEyeCurrentRot, BirdsEyeTargetRot, 1)
				CLEAR_BIT(iLocalBoolCheck22, LBOOL22_TINY_RACERS_EASE_CAMERA_ROTATION)
			ENDIF
		ENDIF
		
	ENDIF
	
	IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
		BirdsEyeCurrentRot = INTERP_FLOAT(BirdsEyeCurrentRot, BirdsEyeTargetRot, g_FMMC_STRUCT.fBEVCamHeadDampening)
		BirdsEyeCurrent.z = INTERP_FLOAT(BirdsEyeCurrent.z, BirdsEyeTarget.z, g_FMMC_STRUCT.fBEVCamHeightDampening)
	ENDIF
ENDPROC

FUNC BOOL CAN_PLAYER_DIRECT_PED()

	IF IS_PED_CLIMBING(LocalPlayerPed)
	OR IS_PED_JACKING(LocalPlayerPed)
	OR GET_SCRIPT_TASK_STATUS(LocalPlayerPed, SCRIPT_TASK_ANY) = WAITING_TO_START_TASK
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC

PROC FACE_PED_CONTROL()
	
	IF NOT INITIALIZE_DUMMY_COMPASS_OBJECT()
		EXIT
	ENDIF
	
	IF NOT IS_PED_INJURED(localPlayerPed)
		IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			
			DISABLE_MOVEMENT_CONTROLS()
			
			IF NOT IS_ANY_FIRST_PERSON_CAM_ACTIVE(TRUE, TRUE)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
			ENDIF
			
			IF GET_SCRIPT_TASK_STATUS(LocalPlayerPed, SCRIPT_TASK_JUMP) != WAITING_TO_START_TASK
			AND GET_SCRIPT_TASK_STATUS(LocalPlayerPed, SCRIPT_TASK_JUMP) != PERFORMING_TASK
				
				//Disable the normal controls
				INT iLX,iLY,iRX,iRY
				GET_CONTROL_VALUE_OF_ANALOGUE_STICKS_UNBOUND(iLX, iLY, iRX, iRY, TRUE)
				
				SET_ENTITY_ROTATION(objTopDownCamCompass, BirdsEyeCurrentRotVector)
				
				SET_ENTITY_COORDS(objTopDownCamCompass, GET_ENTITY_COORDS(localPlayerPed))
				
				VECTOR vNewPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(LocalPlayerPed), BirdsEyeCurrentRotVector.z, <<iLX, -iLY, 0.0>>)
				
				SET_ENTITY_COORDS(objTopDownCamCompass, vNewPos)
				
				IF iLX <> 0
				OR iLY <> 0
					//Get where the player should go
					VECTOR vGoTo

					vGoTo = GET_ENTITY_COORDS(objTopDownCamCompass)
					
					//Get their movement speed
					FLOAT fMovementSpeed
					
					BOOL bTryingToSprint = FALSE
					
					IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SPRINT)
						bTryingToSprint = TRUE
					ENDIF
											
					IF NOT HAS_NET_TIMER_STARTED(tdTopDownSprintButtonMash)
					OR bTryingToSprint
						RESET_NET_TIMER(tdTopDownSprintButtonMash)
						START_NET_TIMER(tdTopDownSprintButtonMash)
					ENDIF
					
					IF NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(tdTopDownSprintButtonMash, ci_TOP_DOWN_SPRINT_TIME)
						bTryingToSprint = TRUE
					ENDIF
					
					IF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SPRINT)
					OR bTryingToSprint
						IF bTryingToSprint
							fMovementSpeed = PEDMOVEBLENDRATIO_SPRINT
						ELSE
							fMovementSpeed = PEDMOVEBLENDRATIO_RUN
						ENDIF
					ELSE
						fMovementSpeed = PEDMOVEBLENDRATIO_WALK
					ENDIF
					
					//Tell the ped to go to there
					IF CAN_PLAYER_DIRECT_PED()
						TASK_GO_STRAIGHT_TO_COORD(LocalPlayerPed, vGoTo, fMovementSpeed)
					ENDIF
				ELSE
					IF GET_SCRIPT_TASK_STATUS(LocalPlayerPed, SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = WAITING_TO_START_TASK
					OR GET_SCRIPT_TASK_STATUS(LocalPlayerPed, SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = PERFORMING_TASK
						CLEAR_PED_TASKS(LocalPlayerPed)
					ENDIF
				ENDIF
								
				IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_ENTER)
					IF GET_SCRIPT_TASK_STATUS(LocalPlayerPed, SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = WAITING_TO_START_TASK
					OR GET_SCRIPT_TASK_STATUS(LocalPlayerPed, SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = PERFORMING_TASK
						CLEAR_PED_TASKS(LocalPlayerPed)
					ENDIF
				ENDIF
				
				IF NOT IS_BIT_SET(iLocalBoolCheck21, LBOOL21_TOP_DOWN_CONTROLS_BEING_USED)
					SET_BIT(iLocalBoolCheck21, LBOOL21_TOP_DOWN_CONTROLS_BEING_USED)
				ENDIF
				
				IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_JUMP)
					CLEAR_PED_TASKS(LocalPlayerPed)
					TASK_JUMP(LocalPlayerPed, TRUE, FALSE, FALSE)
				ENDIF
			ELSE
				IF IS_PED_LANDING(LocalPlayerPed)
					CLEAR_PED_TASKS(LocalPlayerPed)
				ENDIF
			ENDIF			
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SPECIAL_CASE_DUNE3_TITAN_HEALTHBAR(VEHICLE_INDEX tempVeh, INT iVeh, MODEL_NAMES tempModel)
	// [LM] I'VE MOVED THIS AS IT CAUSED NETSYNC BUGS - NEVER USE THIS FOR ANYTHING ELSE OTHER THAN THE SPECIFIC CASE IT WAS ADDED FOR.
	// We should NOT be calling the below functions like we are, especially not every frame. 
	// Wrapping this in a check and not removing it was added in old content that never got fixed verified.
	IF IS_THIS_ROCKSTAR_MISSION_WVM_DUNE3(g_FMMC_STRUCT.iRootContentIDHash)
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix, ciFMMC_VEHICLE6_DISABLE_BREAKING)
			FLOAT engineHealth = GET_VEHICLE_ENGINE_HEALTH(tempVeh)
			IF IS_THIS_MODEL_A_PLANE(tempModel)
				SET_PLANE_ENGINE_HEALTH(tempVeh, engineHealth)
			ELSE
				SET_VEHICLE_ENGINE_HEALTH(tempVeh, engineHealth)
			ENDIF
		ENDIF
	ENDIF
ENDPROC
		
FUNC BOOL IS_BIRDS_EYE_CAM_ACTIVE()
	IF bUsingBirdsEyeCam
		IF (bLocalPlayerOK
		AND (NOT IS_LOCAL_PLAYER_ANY_SPECTATOR() OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)))
		OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_TURF_WAR_TOP_DOWN_CAMERA)
		OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_TINY_RACERS_CAMERA)
		OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciFORCE_STATIC_CAMERA_LOCATIONS) AND (IS_LOCAL_PLAYER_ANY_SPECTATOR() OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)))
		OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TOP_DOWN_CAM_FOR_RESPAWN_SPECTATORS) AND IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN))
			RETURN TRUE
		ELSE
			//bUsingBirdsEyeCam = FALSE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_TINY_RACERS_CAMERA_EFFECTS(CAMERA_INDEX camToUse)
	IF DOES_CAM_EXIST(camToUse)
		SET_CAM_MOTION_BLUR_STRENGTH(camToUse, fTRMotionBlurStrength)
		TINY_RACERS_SET_MOTION_BLUR_ON_VEHICLES(FALSE)
		//'Tilt Shift' settings
		SET_CAM_DOF_FNUMBER_OF_LENS(camToUse, fTRNumLens)
		SET_CAM_DOF_FOCAL_LENGTH_MULTIPLIER(camToUse, fTRFocalLength)
		SET_CAM_DOF_MAX_NEAR_IN_FOCUS_DISTANCE(camToUse, BirdsEyeTarget.z)
		SET_CAM_DOF_MAX_NEAR_IN_FOCUS_DISTANCE_BLEND_LEVEL(camToUse, fTRBlendLevelNearIn)
		SET_CAM_DOF_STRENGTH(camToUse, fTRDOF)
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_CAMS()
	
	IF currentSwanDiveCamState != SDSCS_WaitingToPlay
	AND currentSwanDiveCamState != SDSCS_FindCameraSlot
	AND NOT (currentSwanDiveCamState = SDSCS_InitCamera AND NOT (HAS_NET_TIMER_STARTED(MC_serverBD_1.stTRLastPlayerTimer) AND GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.stTRLastPlayerTimer) > (g_FMMC_STRUCT.iTRResetTimer * 1000)))
	AND IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType) AND TINY_RACERS_GET_ROUND_STATE(iLocalPart) = TR_LastManTimer
		PRINTLN("[PROCESS_SCRIPT_CAMS] Exiting")
		EXIT
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
	AND g_bMissionEnding
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciBIRDS_EYE_ENABLED)
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_TURF_WAR_TOP_DOWN_CAMERA)
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciFORCE_STATIC_CAMERA_LOCATIONS)
	OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TOP_DOWN_CAM_FOR_RESPAWN_SPECTATORS) AND IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN))
		IF NOT g_bMissionEnding
		OR IS_LOCAL_PLAYER_ANY_SPECTATOR()
			IF IS_BIT_SET(iLocalBoolCheck21, LBOOL21_TOP_DOWN_CONTROLS_BEING_USED)
			AND NOT bUsingBirdsEyeCam
				PRINTLN("[PROCESS_SCRIPT_CAMS][TOP DOWN] - We are no longer using Top Down Cam.")
				IF GET_SCRIPT_TASK_STATUS(LocalPlayerPed, SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = WAITING_TO_START_TASK
				OR GET_SCRIPT_TASK_STATUS(LocalPlayerPed, SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = PERFORMING_TASK
					PRINTLN("[PROCESS_SCRIPT_CAMS][TOP DOWN] - Ped still performing GOTO task. Clear Ped Task.")
					CLEAR_PED_TASKS(LocalPlayerPed)
				ENDIF
				CLEAR_BIT(iLocalBoolCheck21, LBOOL21_TOP_DOWN_CONTROLS_BEING_USED)
			ENDIF
			
			IF NOT DOES_CAM_EXIST(g_birdsEyeCam)
				g_birdsEyeCam = CREATE_CAM_WITH_PARAMS("default_scripted_camera", GET_ENTITY_COORDS(LocalPlayerPed), <<0.0,0.0,0.0>>)
				SET_BIRDSEYE_CAM_TARGETS()
				
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_TINY_RACERS_CAMERA)
				OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType) AND IS_FAKE_MULTIPLAYER_MODE_SET()
					PRINTLN("[PROCESS_SCRIPT_CAMS] - Assigned BirdseyeTarget Pos/Rot 1")
					BirdsEyeCurrentRot = BirdsEyeTargetRot
					BirdsEyeCurrent = BirdsEyeTarget 
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_TINY_RACERS_CAMERA)
						SET_TINY_RACERS_CAMERA_EFFECTS(g_birdsEyeCam)
					ENDIF
				ELSE
					PRINTLN("[PROCESS_SCRIPT_CAMS] - Assigned BirdseyeTarget Pos/Rot 2")
					IF DOES_CAM_EXIST(camTRIntro)
						VECTOR tempVec = GET_CAM_ROT(camTRIntro)	
						BirdsEyeCurrentRot = tempVec.z
						BirdsEyeCurrent = GET_CAM_COORD(camTRIntro)
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_TINY_RACERS_CAMERA)
							SET_TINY_RACERS_CAMERA_EFFECTS(g_birdsEyeCam)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciALLOW_PLAYER_TOGGLE)
			OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_TURF_WAR_TOP_DOWN_CAMERA)
			OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciFORCE_STATIC_CAMERA_LOCATIONS) AND (IS_LOCAL_PLAYER_ANY_SPECTATOR() OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)))
			OR bPlayerToggle
			OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_TINY_RACERS_CAMERA) 
				AND (IS_LOCAL_PLAYER_ANY_SPECTATOR() OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)))
			OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TOP_DOWN_CAM_FOR_RESPAWN_SPECTATORS) AND IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN))
				BIRDSEYE_PLAYER_TOGGLE()
			ELSE
				bUsingBirdsEyeCam = TRUE
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciFORCE_STATIC_CAMERA_LOCATIONS) AND NOT (IS_LOCAL_PLAYER_ANY_SPECTATOR() OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN))
				bUsingBirdsEyeCam = FALSE
			ENDIF
			
			SET_BIRDSEYE_CAM_TARGETS()
		
			IF (bUseBirdsEyeCamOnRespawn
			OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_TURF_WAR_TOP_DOWN_CAMERA))
			AND TINY_RACERS_GET_ROUND_STATE(iLocalPart) != TR_Racing
			OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciFORCE_STATIC_CAMERA_LOCATIONS) AND (IS_LOCAL_PLAYER_ANY_SPECTATOR() OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)))
				BirdsEyeCurrent = BirdsEyeTarget
				BirdsEyeCurrentRot = BirdsEyeTargetRot
			ELSE
				INTERP_BIRDSEYE_POS()
			ENDIF
			
			IF bUsingBirdsEyeCam
				IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
				AND IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARZONE(g_FMMC_STRUCT.iAdversaryModeType)
					
					PRINTLN("LM CHECK BirdsEyeCurrentRot: ", BirdsEyeCurrentRot)
	
					// A way of finding out the orientation of the Camera (Roll Axis) needs to be ascertained so the correct direction const can be used in the below function...
					FACE_PED_CONTROL()
				ENDIF
			ENDIF
			
			IF IS_BIRDS_EYE_CAM_ACTIVE()
				IF IS_CAM_ACTIVE(g_birdsEyeCam)
					IF NOT bSnapCam
					AND NOT bUseBirdsEyeCamOnRespawn
						IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
							RENDER_SCRIPT_CAMS(TRUE, TRUE, iTRInterpTime)
						ELSE
							RENDER_SCRIPT_CAMS(TRUE, TRUE, 1700)
						ENDIF
					ELSE
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_TINY_RACERS_CAMERA)
					
					TINY_RACERS_TOP_DOWN_LOOK_BACK()
					
					IF IS_TINY_RACERS_CAMERA_IN_POSITION()
						bUseBirdsEyeCamOnRespawn = TRUE
					ENDIF
					
					IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
						IF NOT IS_BIT_SET(iLocalBoolCheck22, LBOOL22_TINY_RACERS_CAM_SPECTATOR_MOTION_BLUR_OFF)
							TINY_RACERS_SET_MOTION_BLUR_ON_VEHICLES(FALSE)
							SET_BIT(iLocalBoolCheck22, LBOOL22_TINY_RACERS_CAM_SPECTATOR_MOTION_BLUR_OFF)
						ENDIF
					ENDIF
					
					//Put this back in for the mic override stuff
//					PRINTLN("[JT SPECTA] TR Option on")
//					IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
//					OR IS_LOCAL_PLAYER_SPECTATOR()
//						PRINTLN("[JT SPECTA] spectating in some form")
//						IF iSpectatorTarget > -1
//							IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iSpectatorTarget))
//								PED_INDEX tempPed = GET_PLAYER_PED(INT_TO_PLAYERINDEX(iSpectatorTarget))
//								#IF IS_DEBUG_BUILD
//								IF (GET_FRAME_COUNT() % 30) = 0
//									PRINTLN("[JT SPECTA] Spectator target: ", iSpectatorTarget)
//								ENDIF
//								#ENDIF
//								VEHICLE_INDEX tempVeh
//								IF IS_PED_IN_ANY_VEHICLE(tempPed)
//									tempVeh = GET_VEHICLE_PED_IS_IN(tempPed)
//								ENDIF
//								
//								IF NOT IS_PED_INJURED(tempPed)
//									VEHICLE_INDEX tempVeh
//									IF IS_PED_IN_ANY_VEHICLE(tempPed)
//										tempVeh = GET_VEHICLE_PED_IS_IN(tempPed)
//										IF IS_VEHICLE_OK(tempVeh)
//											POINT_CAM_AT_ENTITY(g_birdsEyeCam, tempVeh, <<fTRPointAtX,fTRPointAtY,fTRPointAtZ>>, bTRRelativeCam)
//										ENDIF
//									ELSE
//										POINT_CAM_AT_ENTITY(g_birdsEyeCam, tempPed, <<fTRPointAtX,fTRPointAtY,fTRPointAtZ>>, bTRRelativeCam)
//									ENDIF										
//								ELSE
//									PRINTLN("[JT SPECTA] STOP_CAM_POINTING - Target is dead/vehicle broken")
//									STOP_CAM_POINTING(g_birdsEyeCam)
//								ENDIF
//								
//							ELSE
//								STOP_CAM_POINTING(g_birdsEyeCam)
//							ENDIF
//						ELSE
//							STOP_CAM_POINTING(g_birdsEyeCam)
//						ENDIF
//					ENDIF
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_TINY_RACERS_CAMERA) AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TINY_RACERS_SCREEN_PAUSED)
				OR NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_TINY_RACERS_CAMERA)
					IF NOT g_bMissionEnding
					AND NOT DOES_CAM_EXIST(camEndScreen)
						SET_CAM_ACTIVE(g_birdsEyeCam, TRUE)
						PRINTLN("[DODGYCELEBRATIONSCREEN] SET_CAM_ACTIVE(g_birdsEyeCam, TRUE) - 11800")
					ELSE
						SET_CAM_ACTIVE(g_birdsEyeCam, FALSE)
					ENDIF
				ENDIF
				
				
				SET_CAM_COORD(g_birdsEyeCam, BirdsEyeCurrent)

				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_TINY_RACERS_CAMERA)
					SET_CAM_ROT(g_birdsEyeCam, <<fBirdsEyeAngleTR, 0.0, BirdsEyeCurrentRot>>)
				ELIF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciFORCE_STATIC_CAMERA_LOCATIONS) AND (IS_LOCAL_PLAYER_ANY_SPECTATOR() OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)))
				OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_TURF_WAR_TOP_DOWN_CAMERA) AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARS(g_FMMC_STRUCT.iAdversaryModeType))					
					SET_CAM_ROT(g_birdsEyeCam, BirdsEyeCurrentRotVector)
				ELSE
					SET_CAM_ROT(g_birdsEyeCam, <<fBirdsEyeAngle, 0.0, BirdsEyeCurrentRot>>)
				ENDIF
								
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_TURF_WAR_TOP_DOWN_CAMERA)
					SET_CAM_FOV(g_birdsEyeCam, BirdsEyeTWFOV)
				ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_TINY_RACERS_CAMERA)
					SET_CAM_FOV(g_birdsEyeCam, fBirdsEyeTRFOV)
				ELSE
					SET_CAM_FOV(g_birdsEyeCam, BirdsEyeFOV)
				ENDIF
				
				
				
				SET_CAM_CONTROLS_MINI_MAP_HEADING(g_birdsEyeCam, TRUE)
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					OVERRIDE_MICROPHONE_SETTINGS(HASH("SR_TR_MIC"), TRUE)
					PRINTLN("[BIRDS EYE CAM] - SR_TR_MIC override active")
				ELSE
					OVERRIDE_MICROPHONE_SETTINGS(HASH("FOLLOW_VEHICLE_MIC"), TRUE)
					PRINTLN("[BIRDS EYE CAM] - FOLLOW_VEHICLE_MIC override active")
				ENDIF
				
				DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
				SET_BIT(MPSpecGlobals.iBitSet, SCTV_BIT_GLOBAL_REFRESH_BUTTONS)
				IF bUseBirdsEyeCamOnRespawn
				AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_TINY_RACERS_CAMERA)
					bUseBirdsEyeCamOnRespawn = FALSE
				ENDIF
			ELSE
				IF IS_CAM_ACTIVE(g_birdsEyeCam)					
					IF NOT bSnapCam	
						RENDER_SCRIPT_CAMS(FALSE, TRUE, 1700)
						SET_CAM_CONTROLS_MINI_MAP_HEADING(g_birdsEyeCam, FALSE)
						IF NOT IS_INTERPOLATING_FROM_SCRIPT_CAMS()
							IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
								OVERRIDE_MICROPHONE_SETTINGS(HASH("SR_TR_MIC"), TRUE)
								PRINTLN("[BIRDS EYE CAM] - SR_TR_MIC override deactivated - returning to normal camera")
							ELSE
								OVERRIDE_MICROPHONE_SETTINGS(HASH("FOLLOW_VEHICLE_MIC"), FALSE)
								PRINTLN("[BIRDS EYE CAM] - FOLLOW_VEHICLE_MIC override deactivated - returning to normal camera")
							ENDIF
						ENDIF
					ELSE	
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
						SET_CAM_CONTROLS_MINI_MAP_HEADING(g_birdsEyeCam, FALSE)
					ENDIF
					SET_BIT(MPSpecGlobals.iBitSet, SCTV_BIT_GLOBAL_REFRESH_BUTTONS)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_TURF_WAR_TOP_DOWN_CAMERA)
				IF NOT bUsingBirdsEyeCam
				OR IS_PED_INJURED(localPlayerPed)
					IF HAS_NET_TIMER_STARTED(tdTurfWarMarkerRingFlash)
						RESET_NET_TIMER(tdTurfWarMarkerRingFlash)
					ENDIF
					IF HAS_NET_TIMER_STARTED(tdTurfWarMarkerRingFlashDuration)
						RESET_NET_TIMER(tdTurfWarMarkerRingFlashDuration)
					ENDIF
				ELSE
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_TURF_WAR_CUSTOM_MARKER)
						TURF_WAR_MAINTAIN_LOCAL_PLAYER_MARKER_FOR_TOPDOWN_CAM()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT DOES_CAM_EXIST(camScriptedRaceCam)
			camScriptedRaceCam = CREATE_CAM_WITH_PARAMS("default_scripted_camera", GET_ENTITY_COORDS(LocalPlayerPed), <<0.0,0.0,0.0>>)
		ENDIF
		
		vRaceCamPos = INTERP_VECTOR(vLastRaceCam, vNextRaceCam, fPercentToNextPoint)
		
		RENDER_SCRIPT_CAMS(TRUE, TRUE, 2000)
		SET_CAM_ACTIVE(camScriptedRaceCam, TRUE)
		SET_CAM_COORD(camScriptedRaceCam, vRaceCamPos)
		POINT_CAM_AT_ENTITY(camScriptedRaceCam, LocalPlayerPed, EMPTY_VEC())
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARZONE(g_FMMC_STRUCT.iAdversaryModeType)
		IF NOT bUsingBirdsEyeCam
			IF IS_BIT_SET(iLocalBoolCheck22, LBOOL22_TOP_DOWN_CAM_BLOCK_WEAPON_TOGGLE)
				RESET_NET_TIMER(tdTopDownBlockAiming)
				START_NET_TIMER(tdTopDownBlockAiming)
				PRINTLN("[BIRDS EYE CAM][WAF] - Clear the LBOOL22_TOP_DOWN_CAM_BLOCK_WEAPON_TOGGLE")
				CLEAR_BIT(iLocalBoolCheck22, LBOOL22_TOP_DOWN_CAM_BLOCK_WEAPON_TOGGLE)				
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(iLocalBoolCheck22, LBOOL22_TOP_DOWN_CAM_BLOCK_WEAPON_TOGGLE)
				PRINTLN("[BIRDS EYE CAM][WAF] - Set the LBOOL22_TOP_DOWN_CAM_BLOCK_WEAPON_TOGGLE")
				SET_BIT(iLocalBoolCheck22, LBOOL22_TOP_DOWN_CAM_BLOCK_WEAPON_TOGGLE)			
			ENDIF
		ENDIF
	ENDIF
ENDPROC


//////////////////////////////////////
///    END OLD SCHOOL GTA	  ///////
////////////////////////////////////

//For starting in topdown camera
PROC SET_UP_TINY_RACERS_CAMERA()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_TINY_RACERS_CAMERA)
		IF HAS_ANIM_DICT_LOADED("anim@mp_tiny_racers@")
			PRINTLN("[JT CAM] Cam anim loaded")
			
			IF NOT DOES_CAM_EXIST(camTRIntro)
				camTRIntro = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", TRUE)
				iTRCameraSyncScene = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
				ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iTRCameraSyncScene,LocalPlayerPed, iTRBoneIndex)
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciUSE_TINY_RACERS_SMALL_VEH_INTRO_CAM)
					PLAY_SYNCHRONIZED_CAM_ANIM(camTRIntro, iTRCameraSyncScene, "BUGGY_INTRO_CAM", "anim@mp_tiny_racers@")
					PRINTLN("[JT CAM] Playing BUGGY_INTRO_CAM")
				ELSE
					PLAY_SYNCHRONIZED_CAM_ANIM(camTRIntro, iTRCameraSyncScene, "CAR_INTRO_CAM", "anim@mp_tiny_racers@")
					PRINTLN("[JT CAM] Playing CAR_INTRO_CAM")
				ENDIF
				PRINTLN("[JT CAM] Camera set up")
			ENDIF
			
			IF DOES_CAM_EXIST(camTRIntro)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
			ENDIF
		ENDIF
		
	ENDIF
ENDPROC

FUNC VECTOR GET_AIRSTIKE_OFFSET(INT iOffsetNum)
	SWITCH iOffsetNum
		CASE 0		RETURN <<0,0,0>>
		CASE 1		RETURN <<5,5,0>>
		CASE 2		RETURN <<-5,-5,0>>
		CASE 3		RETURN <<-5,5,0>>
		CASE 4		RETURN <<5,-5,0>>
		CASE 5		RETURN <<10,5,0>>
		CASE 6		RETURN <<10,10,0>>
		CASE 7		RETURN <<-10, -10,0>>
		CASE 8		RETURN <<-10, 10, 0>>
		CASE 9		RETURN <<10, -10, 0>>
	ENDSWITCH
	RETURN <<0,0,0>>
ENDFUNC

PROC DO_SCRIPTED_AIRSTRIKE(VECTOR vTargetCoords, INT iNumRockets)

	VECTOR vStartPos = vTargetCoords
	VECTOR vEndPos = vTargetCoords
	
	INT iLongTimer
	INT iShortTimer
	
	IF IS_BIT_SET(iLocalBoolCheck23, LBOOL23_AIRSTRIKE_SPEED_BAR_FIRED)
		iLongTimer = 250
		iShortTimer = 150
	ELSE
		iLongTimer = 750
		iShortTimer = 500
	ENDIF
	
	vStartPos.z += 40
	
	IF iAirstrikeExplosionCount >= iNumRockets
		PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Exiting due to rocket count")
		SET_BIT(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_FINAL_SHOT_FIRED)
		EXIT
	ENDIF
		
	SWITCH iAirstrikeExplosionCount
		CASE 0
			IF NOT HAS_NET_TIMER_STARTED(tdAirstrikeRocketTimer)
				IF NOT IS_BIT_SET(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
					CLEAR_BIT(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_FINAL_SHOT_FIRED)
					GET_GROUND_Z_FOR_3D_COORD(vEndPos+<<0,0,10>>, vEndPos.z)
					vEndPos = vEndPos + <<0.1, 0.1, 0.0>>
					START_NET_TIMER(tdAirstrikeRocketTimer)
					SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vStartPos,vEndPos, 200, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NULL, TRUE, FALSE)
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " launched.")
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " Starting Coord: ", vStartPos," Ending Coord: ", vEndPos)
					SET_BIT(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
					SET_BIT(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_IN_PROGRESS)
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(tdAirstrikeRocketTimer, iLongTimer)
					RESET_NET_TIMER(tdAirstrikeRocketTimer)
					iAirstrikeExplosionCount++
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Incrementing iAirstrikeExplosionCount to: ", iAirstrikeExplosionCount)
				ENDIF
			ENDIF
		BREAK
		CASE 1
			IF NOT HAS_NET_TIMER_STARTED(tdAirstrikeRocketTimer)
				IF NOT IS_BIT_SET(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
					vStartPos = vTargetCoords + GET_AIRSTIKE_OFFSET(iAirstrikeExplosionCount)
					vEndPos = vTargetCoords + GET_AIRSTIKE_OFFSET(iAirstrikeExplosionCount)
					vEndPos = vEndPos + <<0.1, 0.1, 0.0>>
					GET_GROUND_Z_FOR_3D_COORD(vEndPos+<<0,0,10>>, vEndPos.z)
					ADD_EXPLOSION(vEndPos,EXP_TAG_PLANE_ROCKET,1,FALSE) //Using add explosion with no sound as sfx was dropping out
//					SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vStartPos,vEndPos, 200, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NULL, TRUE, FALSE)
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " launched.")
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " Starting Coord: ", vStartPos," Ending Coord: ", vEndPos)
					START_NET_TIMER(tdAirstrikeRocketTimer)
					SET_BIT(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(tdAirstrikeRocketTimer, iShortTimer)
					RESET_NET_TIMER(tdAirstrikeRocketTimer)
					iAirstrikeExplosionCount++
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Incrementing iAirstrikeExplosionCount to: ", iAirstrikeExplosionCount)
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF NOT HAS_NET_TIMER_STARTED(tdAirstrikeRocketTimer)
				IF NOT IS_BIT_SET(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
					vStartPos = vTargetCoords + GET_AIRSTIKE_OFFSET(iAirstrikeExplosionCount)
					vStartPos.z += 40
					vEndPos = vTargetCoords + GET_AIRSTIKE_OFFSET(iAirstrikeExplosionCount)
					vEndPos = vEndPos + <<0.1, 0.1, 0.0>>
					GET_GROUND_Z_FOR_3D_COORD(vEndPos+<<0,0,10>>, vEndPos.z)
					SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vStartPos,vEndPos, 200, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NULL, TRUE, FALSE)
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " launched.")
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " Starting Coord: ", vStartPos," Ending Coord: ", vEndPos)
					START_NET_TIMER(tdAirstrikeRocketTimer)
					SET_BIT(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(tdAirstrikeRocketTimer, iShortTimer)
					RESET_NET_TIMER(tdAirstrikeRocketTimer)
					iAirstrikeExplosionCount++
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Incrementing iAirstrikeExplosionCount to: ", iAirstrikeExplosionCount)
				ENDIF
			ENDIF
		BREAK
		CASE 3
			IF NOT HAS_NET_TIMER_STARTED(tdAirstrikeRocketTimer)
				IF NOT IS_BIT_SET(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
					vStartPos = vTargetCoords + GET_AIRSTIKE_OFFSET(iAirstrikeExplosionCount)
					vStartPos.z += 40
					vEndPos = vTargetCoords + GET_AIRSTIKE_OFFSET(iAirstrikeExplosionCount)
					vEndPos = vEndPos + <<0.1, 0.1, 0.0>>
					GET_GROUND_Z_FOR_3D_COORD(vEndPos+<<0,0,10>>, vEndPos.z)
					ADD_EXPLOSION(vEndPos,EXP_TAG_PLANE_ROCKET,1,FALSE) //Using add explosion with no sound as sfx was dropping out
//					SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vStartPos,vEndPos, 200, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NULL, TRUE, FALSE)
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " launched.")
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " Starting Coord: ", vStartPos," Ending Coord: ", vEndPos)
					START_NET_TIMER(tdAirstrikeRocketTimer)
					SET_BIT(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(tdAirstrikeRocketTimer, iLongTimer)
					RESET_NET_TIMER(tdAirstrikeRocketTimer)
					iAirstrikeExplosionCount++
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Incrementing iAirstrikeExplosionCount to: ", iAirstrikeExplosionCount)
				ENDIF
			ENDIF
		BREAK
		CASE 4
			IF NOT HAS_NET_TIMER_STARTED(tdAirstrikeRocketTimer)
				IF NOT IS_BIT_SET(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
					vStartPos = vTargetCoords + GET_AIRSTIKE_OFFSET(iAirstrikeExplosionCount)
					vStartPos.z += 40
					vEndPos = vTargetCoords + GET_AIRSTIKE_OFFSET(iAirstrikeExplosionCount)
					vEndPos = vEndPos + <<0.1, 0.1, 0.0>>
					GET_GROUND_Z_FOR_3D_COORD(vEndPos+<<0,0,10>>, vEndPos.z)
					SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vStartPos,vEndPos, 200, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NULL, TRUE, FALSE)
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " launched.")
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " Starting Coord: ", vStartPos," Ending Coord: ", vEndPos)
					START_NET_TIMER(tdAirstrikeRocketTimer)
					SET_BIT(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(tdAirstrikeRocketTimer, iShortTimer)
					RESET_NET_TIMER(tdAirstrikeRocketTimer)
					iAirstrikeExplosionCount++
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Incrementing iAirstrikeExplosionCount to: ", iAirstrikeExplosionCount)
				ENDIF
			ENDIF
		BREAK
		CASE 5
			IF NOT HAS_NET_TIMER_STARTED(tdAirstrikeRocketTimer)
				IF NOT IS_BIT_SET(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
					vStartPos = vTargetCoords + GET_AIRSTIKE_OFFSET(iAirstrikeExplosionCount)
					vStartPos.z += 40
					vEndPos = vTargetCoords + GET_AIRSTIKE_OFFSET(iAirstrikeExplosionCount)
					vEndPos = vEndPos + <<0.1, 0.1, 0.0>>
					GET_GROUND_Z_FOR_3D_COORD(vEndPos+<<0,0,10>>, vEndPos.z)
					ADD_EXPLOSION(vEndPos,EXP_TAG_PLANE_ROCKET,1,FALSE) //Using add explosion with no sound as sfx was dropping out
//					SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vStartPos,vEndPos, 200, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NULL, TRUE, FALSE)
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " launched.")
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " Starting Coord: ", vStartPos," Ending Coord: ", vEndPos)
					START_NET_TIMER(tdAirstrikeRocketTimer)
					SET_BIT(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(tdAirstrikeRocketTimer, iLongTimer)
					RESET_NET_TIMER(tdAirstrikeRocketTimer)
					iAirstrikeExplosionCount++
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Incrementing iAirstrikeExplosionCount to: ", iAirstrikeExplosionCount)
				ENDIF
			ENDIF
		BREAK
		CASE 6
			IF NOT HAS_NET_TIMER_STARTED(tdAirstrikeRocketTimer)
				IF NOT IS_BIT_SET(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
					vStartPos = vTargetCoords + GET_AIRSTIKE_OFFSET(iAirstrikeExplosionCount)
					vStartPos.z += 40
					vEndPos = vTargetCoords + GET_AIRSTIKE_OFFSET(iAirstrikeExplosionCount)
					vEndPos = vEndPos + <<0.1, 0.1, 0.0>>
					GET_GROUND_Z_FOR_3D_COORD(vEndPos+<<0,0,10>>, vEndPos.z)
					SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vStartPos,vEndPos, 200, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NULL, TRUE, FALSE)
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " launched.")
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " Starting Coord: ", vStartPos," Ending Coord: ", vEndPos)
					START_NET_TIMER(tdAirstrikeRocketTimer)
					SET_BIT(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(tdAirstrikeRocketTimer, iShortTimer)
					RESET_NET_TIMER(tdAirstrikeRocketTimer)
					iAirstrikeExplosionCount++
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Incrementing iAirstrikeExplosionCount to: ", iAirstrikeExplosionCount)
				ENDIF
			ENDIF
		BREAK
		CASE 7
			IF NOT HAS_NET_TIMER_STARTED(tdAirstrikeRocketTimer)
				IF NOT IS_BIT_SET(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
					vStartPos = vTargetCoords + GET_AIRSTIKE_OFFSET(iAirstrikeExplosionCount)
					vStartPos.z += 40
					vEndPos = vTargetCoords + GET_AIRSTIKE_OFFSET(iAirstrikeExplosionCount)
					vEndPos = vEndPos + <<0.1, 0.1, 0.0>>
					GET_GROUND_Z_FOR_3D_COORD(vEndPos+<<0,0,10>>, vEndPos.z)
					ADD_EXPLOSION(vEndPos,EXP_TAG_PLANE_ROCKET,1,FALSE) //Using add explosion with no sound as sfx was dropping out
//					SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vStartPos,vEndPos, 200, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NULL, TRUE, FALSE)
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " launched.")
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " Starting Coord: ", vStartPos," Ending Coord: ", vEndPos)
					START_NET_TIMER(tdAirstrikeRocketTimer)
					SET_BIT(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(tdAirstrikeRocketTimer, iShortTimer)
					RESET_NET_TIMER(tdAirstrikeRocketTimer)
					iAirstrikeExplosionCount++
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Incrementing iAirstrikeExplosionCount to: ", iAirstrikeExplosionCount)
				ENDIF
			ENDIF
		BREAK
		CASE 8
			IF NOT HAS_NET_TIMER_STARTED(tdAirstrikeRocketTimer)
				IF NOT IS_BIT_SET(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
					vStartPos = vTargetCoords + GET_AIRSTIKE_OFFSET(iAirstrikeExplosionCount)
					vStartPos.z += 40
					vEndPos = vTargetCoords + GET_AIRSTIKE_OFFSET(iAirstrikeExplosionCount)
					vEndPos = vEndPos + <<0.1, 0.1, 0.0>>
					GET_GROUND_Z_FOR_3D_COORD(vEndPos+<<0,0,10>>, vEndPos.z)
					SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vStartPos,vEndPos, 200, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NULL, TRUE, FALSE)
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " launched.")
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " Starting Coord: ", vStartPos," Ending Coord: ", vEndPos)
					START_NET_TIMER(tdAirstrikeRocketTimer)
					SET_BIT(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(tdAirstrikeRocketTimer, iShortTimer)
					RESET_NET_TIMER(tdAirstrikeRocketTimer)
					iAirstrikeExplosionCount++
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Incrementing iAirstrikeExplosionCount to: ", iAirstrikeExplosionCount)
				ENDIF
			ENDIF
		BREAK
		CASE 9
			IF NOT HAS_NET_TIMER_STARTED(tdAirstrikeRocketTimer)
				IF NOT IS_BIT_SET(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
					vStartPos = vTargetCoords + GET_AIRSTIKE_OFFSET(iAirstrikeExplosionCount)
					vStartPos.z += 40
					vEndPos = vTargetCoords + GET_AIRSTIKE_OFFSET(iAirstrikeExplosionCount)
					vEndPos = vEndPos + <<0.1, 0.1, 0.0>>
					GET_GROUND_Z_FOR_3D_COORD(vEndPos+<<0,0,10>>, vEndPos.z)
					ADD_EXPLOSION(vEndPos,EXP_TAG_PLANE_ROCKET,1,FALSE) //Using add explosion with no sound as sfx was dropping out
//					SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vStartPos,vEndPos, 200, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NULL, TRUE, FALSE)
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " launched.")
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " Starting Coord: ", vStartPos," Ending Coord: ", vEndPos)
					SET_BIT(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
					iAirstrikeExplosionCount++
					SET_BIT(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_FINAL_SHOT_FIRED)
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Incrementing iAirstrikeExplosionCount to: ", iAirstrikeExplosionCount)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

PROC ACTIVATE_AIRSTRIKE_AND_SET_COORDS(FLOAT fXOffset = 0.0, FLOAT fYOffset = 0.0, BOOL bSpecificCoords = FALSE)

	PRINTLN("[RCC MISSION][AIRSTRIKE] Calling ACTIVATE_AIRSTRIKE_AND_SET_COORDS")
		
	VECTOR tempTargetCoords
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam] < FMMC_MAX_RULES
		IF bSpecificCoords
			tempTargetCoords = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].sAirstrikeStruct[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]].vTargetVector
		ELSE	
			GET_AIRSTRIKE_TARGET_COORDS(tempTargetCoords, MC_playerBD[iPartToUse].iTeam, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam])
		ENDIF
	ELSE
		tempTargetCoords = <<0,0,0>>
	ENDIF
	PRINTLN("[RCC MISSION][AIRSTRIKE][OFFSET] ACTIVATE_AIRSTRIKE_AND_SET_COORDS - tempTargetCoords Pre offset: ", tempTargetCoords)
	PRINTLN("[RCC MISSION][AIRSTRIKE][OFFSET] ACTIVATE_AIRSTRIKE_AND_SET_COORDS - fXOffset: ", fXOffset, " Y Offset: ", fYOffset)
	tempTargetCoords.x += fXOffset
	tempTargetCoords.y += fYOffset
	PRINTLN("[RCC MISSION][AIRSTRIKE][OFFSET] ACTIVATE_AIRSTRIKE_AND_SET_COORDS - tempTargetCoords post offset: ", tempTargetCoords)
	SET_GLOBAL_AIRSTRIKE_COORDS(tempTargetCoords)
	PRINTLN("[RCC MISSION][AIRSTRIKE][CALL] Calling DO_SCRIPTED_AIRSTRIKE from: Activate airstrike")
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam] < FMMC_MAX_RULES
		IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].sAirstrikeStruct[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]].iAirstrikeType = ciAIRSTRIKE_TYPE_MISSILES
			PRINTLN("[RCC MISSION][AIRSTRIKE][OFFSET] ACTIVATE_AIRSTRIKE_AND_SET_COORDS CALLING DO_SCRIPTED_AIRSTRIKE - tempTargetCoords: ", tempTargetCoords)
			DO_SCRIPTED_AIRSTRIKE(tempTargetCoords, 10)
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].sAirstrikeStruct[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]].iAirstrikeType = ciAIRSTRIKE_TYPE_ORBITAL_CANNON
			PRINTLN("[RCC MISSION][AIRSTRIKE][OFFSET] ACTIVATE_AIRSTRIKE_AND_SET_COORDS CALLING FIRE_ORBITAL_CANNON - tempTargetCoords: ", tempTargetCoords)
			FIRE_ORBITAL_CANNON(tempTargetCoords)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_SMOKE_SHOW()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_SmokeTrailRequiresMovement)
		RETURN GET_ENTITY_SPEED(LocalPlayerPed) > 1.0
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_JET_SMOKE_TRAIL()
	IF NOT IS_PED_INJURED(LocalPlayerPed)
	AND (MC_playerBD[iLocalPart].iObjCarryCount != 0
	OR IS_PED_CARRYING_ANY_OBJECTS(LocalPlayerPed))
	AND manualRespawnState = eManualRespawnState_OKAY_TO_SPAWN
	AND NOT g_bMissionEnding
	AND NOT IS_MULTI_TEAM_VEHICLE_SEAT_SWAP_PAST_INITIATED()
	AND SHOULD_SMOKE_SHOW()
		IF HAS_NET_TIMER_STARTED(SmokeTrailTimer)
			IF HAS_NET_TIMER_EXPIRED(SmokeTrailTimer, ci_SMOKE_TRAIL_TIMER)
				IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(StockpileSmokeTrail)
					USE_PARTICLE_FX_ASSET("scr_ar_planes")
					VECTOR vPFXOffset = <<0,0,0>>
					VECTOR vPFXRot = <<0,0,0>>
					STRING sPFXName = "scr_ar_trail_smoke"
					IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
						VECTOR vMin, vMax
						GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)),vMin, vMax)
						vPFXOffset = <<0,vMin.y,0>>
						IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)) = HYDRA
						OR GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)) = LAZER
							vPFXOffset.y -= 4
						ENDIF
					ENDIF
					
					INT iR, iG, iB, iA
					GET_HUD_COLOUR(GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_playerBD[iLocalPart].iTeam, PLAYER_ID()), iR, iG, iB, iA)
					FLOAT fR = TO_FLOAT(iR)
					FLOAT fG = TO_FLOAT(iG)
					FLOAT fB = TO_FLOAT(iB)
					
					fR = fR / 255
					fG = fG / 255
					fB = fB / 255
					
					ENTITY_INDEX entityToUse
					
					IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
						entityToUse = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
						IF GET_ENTITY_MODEL(entityToUse) = HUNTER
						OR GET_ENTITY_MODEL(entityToUse) = NOKOTA
							sPFXName = "scr_ar_trail_smoke_slow"
						ENDIF
					ELSE
						entityToUse = LocalPlayerPed
					ENDIF
					
					PRINTLN("[RCC MISSION] PROCESS_JET_SMOKE_TRAIL - Colour: RGB: ", fR,", ", fG,", ", fB)
					
					StockpileSmokeTrail = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY_BONE(sPFXName,entityToUse,vPFXOffset,vPFXRot ,DEFAULT, g_FMMC_STRUCT.fSmokeTrailScale, DEFAULT, DEFAULT, DEFAULT, fR, fG, fB)
				ENDIF
			ENDIF
		ELSE
			REINIT_NET_TIMER(SmokeTrailTimer)
			PRINTLN("[RCC MISSION] PROCESS_JET_SMOKE_TRAIL - Starting time delay")
		ENDIF
	ELSE
		IF NOT IS_MULTI_TEAM_VEHICLE_SEAT_SWAP_PAST_INITIATED() //Not stopping particle effect whilst switching seats
			IF DOES_PARTICLE_FX_LOOPED_EXIST(StockpileSmokeTrail)
				PRINTLN("[RCC MISSION] PROCESS_JET_SMOKE_TRAIL - Stopping StockpileSmokeTrail")
				STOP_PARTICLE_FX_LOOPED(StockpileSmokeTrail)
			ENDIF
		ENDIF
		IF HAS_NET_TIMER_STARTED(SmokeTrailTimer)
			RESET_NET_TIMER(SmokeTrailTimer)
		ENDIF
	ENDIF
		
ENDPROC

FUNC INT GET_TAG_BLIP_COLOUR(INT iEntType, INT iEntIndex)
	SWITCH iEntType
		CASE 1 RETURN GET_BLIP_COLOUR_FROM_CREATOR(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEntIndex].iTagColour)
		CASE 2 RETURN GET_BLIP_COLOUR_FROM_CREATOR(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntIndex].iTagColour)
		CASE 3 RETURN GET_BLIP_COLOUR_FROM_CREATOR(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iEntIndex].iTagColour)
		CASE 4 RETURN GET_BLIP_COLOUR_FROM_CREATOR(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntIndex].iTagColour)
	ENDSWITCH
	RETURN BLIP_COLOUR_RED
ENDFUNC

FUNC HUD_COLOURS GET_TAG_MARKER_COLOUR(INT iEntType, INT iEntIndex)
	SWITCH iEntType
		CASE 1 RETURN GET_HUD_COLOUR_FROM_CREATOR_BLIP_COLOUR(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEntIndex].iTagColour)
		CASE 2 RETURN GET_HUD_COLOUR_FROM_CREATOR_BLIP_COLOUR(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntIndex].iTagColour)
		CASE 3 RETURN GET_HUD_COLOUR_FROM_CREATOR_BLIP_COLOUR(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iEntIndex].iTagColour)
		CASE 4 RETURN GET_HUD_COLOUR_FROM_CREATOR_BLIP_COLOUR(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntIndex].iTagColour)
	ENDSWITCH
	RETURN HUD_COLOUR_RED
ENDFUNC

PROC DRAW_TAGGED_MARKER(VECTOR vTaggedEntityPos, ENTITY_INDEX EntityToUse, INT iEntType, INT iEntIndex)
	VECTOR vPlayerPos = GET_ENTITY_COORDS(LocalPlayerPed)
	
	INT r, g, b, a
	GET_HUD_COLOUR(GET_TAG_MARKER_COLOUR(iEntType, iEntIndex), r, g, b, a)
	
	IF (vPlayerPos.z - vTaggedEntityPos.z) >= 30
		DRAW_MARKER(MARKER_RING, vTaggedEntityPos, <<0,0,1>>, <<0,0,0>>, <<5,5,5>>, r, g, b, 100, TRUE)
	ELSE
		VECTOR returnMin, returnMax
		FLOAT fOffset
		
		fOffset = 0.5 
		
		GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(EntityToUse), returnMin, returnMax)

		FLOAT fCentreHeight = (returnMax.z - returnMin.z)/2
		FLOAT fZdiff = returnMax.z - fCentreHeight
		
		IF fOffset <= (fZdiff + 0.1)
			fOffset = fZdiff + 0.4
		ENDIF
		VECTOR vScale = <<1,1,1>>
		IF VDIST2(vPlayerPos, vTaggedEntityPos) > POW(75,2)
			vScale = <<1.5,1.5,1.5>>
		ENDIF
		
		vTaggedEntityPos.z += ((returnMax.z - returnMin.z)/2) + fOffset

		DRAW_MARKER(MARKER_ARROW, vTaggedEntityPos, <<0,0,0>>, <<180,0,0>>,vScale, r, g, b, 100, TRUE, TRUE)
	ENDIF
ENDPROC

FUNC BOOL IS_ENTITY_ALREADY_TAGGED(INT iEntityType, INT iEntIndexToUse)
	INT iTagged
	FOR iTagged = 0 TO FMMC_MAX_TAGGED_ENTITIES-1
		IF MC_serverBD_3.iTaggedEntityType[iTagged] = iEntityType
			IF MC_serverBD_3.iTaggedEntityIndex[iTagged] = iEntIndexToUse
				PRINTLN("[RCC MISSION][TAGGING] - Tag Check - This Entity is already tagged. Tag index: ", iTagged)
				RETURN TRUE
				BREAKLOOP
			ENDIF
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC


PROC ADD_TAGGED_BLIP(ENTITY_INDEX eiEntityToUse, INT iEntityType, INT iEntIndexToUse, INT iTagIndex)
	IF NOT DOES_BLIP_EXIST(biTaggedEntity[iTagIndex])
	
		REMOVE_BLIP(biTaggedEntity[iTagIndex])
		
		biTaggedEntity[iTagIndex] = ADD_BLIP_FOR_ENTITY(eiEntityToUse)
		SET_BLIP_COLOUR(biTaggedEntity[iTagIndex], GET_TAG_BLIP_COLOUR(iEntityType, iEntIndexToUse))
		
		IF DOES_BLIP_EXIST(biTaggedEntity[iTagIndex])
			PRINTLN("[RCC MISSION][TAGGING] - Blip ",iTagIndex, " Added")
		ENDIF
	ENDIF	
ENDPROC

PROC REMOVE_TAGGED_BLIP(INT iEntityType, INT iEntIndexToUse)
	INT iTagged
	FOR iTagged = 0 TO FMMC_MAX_TAGGED_ENTITIES-1
		IF DOES_BLIP_EXIST(biTaggedEntity[iTagged])
			IF MC_serverBD_3.iTaggedEntityType[iTagged] = iEntityType
				IF MC_serverBD_3.iTaggedEntityIndex[iTagged] = iEntIndexToUse
					IF DOES_BLIP_EXIST(biTaggedEntity[iTagged])
						REMOVE_BLIP(biTaggedEntity[iTagged])
						IF NOT DOES_BLIP_EXIST(biTaggedEntity[iTagged])
							PRINTLN("[RCC MISSION][TAGGING] - Blip ",iTagged, " removed")
						ENDIF
						IF bIsLocalPlayerHost
							CLEAR_BIT(MC_serverBD_3.iTaggedEntityBitset, iTagged)
							MC_serverBD_3.iTaggedEntityType[iTagged] = 0
							MC_serverBD_3.iTaggedEntityType[iTagged] = -1
						ENDIF
						BREAKLOOP
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

FUNC BOOL IS_TAGGING_ENABLED_THIS_RULE()
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule > -1
	AND iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[iRule], ciBS_RULE10_ENABLE_TAGGING_THIS_RULE)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_IT_SAFE_TO_ADD_TAG_BLIP(INT iRule)
	IF iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iRuleBitsetTen[iRule], ciBS_RULE10_TAGGING_HIDE_NOT_ACTIVE) AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iRuleBitsetTen[iRule], ciBS_RULE10_ENABLE_TAGGING_THIS_RULE)
			PRINTLN("[RCC MISSION] IS_IT_SAFE_TO_ADD_TAG_BLIP - RETURNING FALSE - Option Set")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SET_STOCKPILE_LAST_PICKUP_CAPTOR(INT iPickup, INT iParticipant)
	IF bIsLocalPlayerHost
		PRINTLN("[RCC MISSION] SET_STOCKPILE_LAST_PICKUP_CAPTOR - Setting MC_serverBD_1.iPTLDroppedPackageLastTeam[",iPickup,"] to: ", iparticipant)
		MC_serverBD_1.iPTLDroppedPackageLastTeam[iPickup] = iParticipant
	ENDIF
ENDPROC

FUNC INT GET_STOCKPILE_LAST_PICKUP_CAPTOR(INT iPickup)
	PRINTLN("[RCC MISSION] GET_STOCKPILE_LAST_PICKUP_CAPTOR - Returning MC_serverBD_1.iPTLDroppedPackageLastTeam[",iPickup,"] to: ", MC_serverBD_1.iPTLDroppedPackageLastTeam[iPickup])
	RETURN MC_serverBD_1.iPTLDroppedPackageLastTeam[iPickup]
ENDFUNC

PROC CALCULATE_CAPTURE_LOCATION_STATE_AND_COLOUR(INT iLoc)
	INT iA
	IF iLoc > -1
		eCaptureLocateStateLastFrame[iLoc] = eCaptureLocateState[iLoc]

		IF MC_serverBD.iLocOwner[iLoc] != -1
			IF IS_ZONE_BEING_CONTESTED(iLoc)
				iCaptureLocateRed[iLoc] = 255
				iCaptureLocateGreen[iLoc] = 0
				iCaptureLocateBlue[iLoc] = 0
				iCaptureLocateAlpha[iLoc] = 200
				
				eCaptureLocateState[iLoc] = eCapture_contested
			ELSE
				GET_HUD_COLOUR(GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_serverBD.iLocOwner[iLoc], PlayerToUse), iCaptureLocateRed[iLoc], iCaptureLocateGreen[iLoc], iCaptureLocateBlue[iLoc], iA)
				eCaptureLocateState[iLoc] = eCapture_captured
			ENDIF
		ELSE
			IF IS_ZONE_BEING_CONTESTED(iLoc, FALSE, TRUE)
				iCaptureLocateRed[iLoc] = 255
				iCaptureLocateGreen[iLoc] = 0
				iCaptureLocateBlue[iLoc] = 0
				iCaptureLocateAlpha[iLoc] = 200
				
				eCaptureLocateState[iLoc] = eCapture_contested
			ELSE
				iCaptureLocateRed[iLoc] = 255
				iCaptureLocateGreen[iLoc] = 255
				iCaptureLocateBlue[iLoc] = 0
				iCaptureLocateAlpha[iLoc] = 200
				
				eCaptureLocateState[iLoc] = eCapture_neutral
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_THERE_ENOUGH_TIME_LEFT_TO_DECREMENT_REMAINING_TIME(INT iRule, INT iTeam)
	IF iRule < FMMC_MAX_RULES
		IF HAS_NET_TIMER_STARTED(MC_serverBD_3.tdObjectiveLimitTimer[iTeam])
			IF GET_REMAINING_TIME_ON_RULE(iTeam, iRule) < ((g_FMMC_STRUCT.fManualRespawnTimeLose[iTeam]*1000) * 1.5)
				RETURN FALSE
			ENDIF
		ELIF HAS_NET_TIMER_STARTED(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeam])
			IF GET_TIME_REMAINING_ON_MULTIRULE_TIMER() < ((g_FMMC_STRUCT.fManualRespawnTimeLose[iTeam]*1000) * 1.5)
				RETURN FALSE
			ENDIF
		ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THERE_ENOUGH_TIME_LEFT_TO_DECREMENT_REMAINING_TIME_FOR_FORCED_RESPAWN(INT iRule, INT iTeam)
	IF iRule < FMMC_MAX_RULES
		IF HAS_NET_TIMER_STARTED(MC_serverBD_3.tdObjectiveLimitTimer[iTeam])
			IF GET_REMAINING_TIME_ON_RULE(iTeam, iRule) < ((g_FMMC_STRUCT.fForcedRespawnTimeLose[iTeam]*1000) * 1.5)
				RETURN FALSE
			ENDIF
		ELIF HAS_NET_TIMER_STARTED(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeam])
			IF GET_TIME_REMAINING_ON_MULTIRULE_TIMER() < ((g_FMMC_STRUCT.fForcedRespawnTimeLose[iTeam]*1000) * 1.5)
				RETURN FALSE
			ENDIF
		ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC INT VENETIAN_JOB_COUNT_MISSING_INTERCEPTORS()
	INT i, iPlayerCount
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(i)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
		AND NOT IS_PLAYER_SPECTATING(NETWORK_GET_PLAYER_INDEX(tempPart))
			IF MC_playerBD[i].iteam = 0
				iPlayerCount++
				PRINTLN("[RCC MISSION] VENETIAN_JOB_COUNT_MISSING_INTERCEPTORS - Participant ",i," is active on team 0. iPlayerCount: ", iPlayerCount)
				IF iPlayerCount = g_FMMC_STRUCT.iMaxNumPlayersPerTeam[0]
					BREAKLOOP
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	PRINTLN("[RCC MISSION] VENETIAN_JOB_COUNT_MISSING_INTERCEPTORS - Returning: ", g_FMMC_STRUCT.iMaxNumPlayersPerTeam[0] - iPlayerCount)
	RETURN g_FMMC_STRUCT.iMaxNumPlayersPerTeam[0] - iPlayerCount
ENDFUNC

FUNC INT MONSTER_JAM_COUNT_MISSING_CONTENDERS()
	INT i, iPlayerCount
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			IF MC_playerBD[i].iteam = 0
				iPlayerCount++
				PRINTLN("[MJL][MONSTER JAM] MONSTER_JAM_COUNT_MISSING_CONTENDERS - Participant ",i," is active on team 0. iPlayerCount: ", iPlayerCount)
				IF iPlayerCount = g_FMMC_STRUCT.iMaxNumPlayersPerTeam[0]
					BREAKLOOP
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	PRINTLN("[MJL] MONSTER_JAM_COUNT_MISSING_CONTENDERS - Returning: ", ciMONSTER_JAM_MAX_CONTENDERS - iPlayerCount)
	
	RETURN ciMONSTER_JAM_MAX_CONTENDERS - iPlayerCount
ENDFUNC

FUNC STRING GET_CASINO_HOUSE_KEEPING_VOICE()
	INT iReturnVoice = GET_RANDOM_INT_IN_RANGE(0,44)
	
	SWITCH iReturnVoice
		CASE 0
			RETURN "A_F_M_Beach_01_White_FULL_01"
		CASE 1
			RETURN "A_F_M_BevHills_02_BLACK_FULL_01"
		CASE 2
			RETURN "A_F_M_Bodybuild_01_Black_FULL_01"
		CASE 3
			RETURN "A_F_M_Bodybuild_01_White_FULL_01"
		CASE 4
			RETURN "A_F_M_FatWhite_01_White_FULL_01"
		CASE 5
			RETURN "A_F_M_Ktown_02_KOREAN_FULL_01"
		CASE 6
			RETURN "A_F_M_SALTON_01_WHITE_FULL_02"
		CASE 7
			RETURN "A_F_M_SALTON_01_WHITE_FULL_03"
		CASE 8
			RETURN "A_F_M_SouCent_01_Black_FULL_01"
		CASE 9
			RETURN "A_F_O_SouCent_02_Black_FULL_01"
		CASE 10
			RETURN "A_F_Y_BevHills_01_White_FULL_01"
		CASE 11
			RETURN "A_F_Y_Business_03_Chinese_FULL_01"
		CASE 12
			RETURN "A_F_Y_Business_03_Latino_FULL_01"
		CASE 13
			RETURN "A_F_Y_Business_04_Black_FULL_01"
		CASE 14
			RETURN "A_F_Y_EastSA_01_Latino_FULL_01"
		CASE 15
			RETURN "A_F_Y_SouCent_01_Black_FULL_01"		
		CASE 16
			RETURN "A_F_Y_SouCent_02_Black_FULL_01"
		CASE 17
			RETURN "A_F_Y_SouCent_03_Latino_FULL_01"
		CASE 18
			RETURN "A_F_Y_TOURIST_01_LATINO_FULL_01"
		CASE 19
			RETURN "A_F_Y_Vinewood_03_Chinese_FULL_01"
		CASE 20
			RETURN "A_F_Y_Vinewood_04_White_FULL_01"
		CASE 21
			RETURN "A_M_M_AfriAmer_01_Black_FULL_01"
		CASE 22
			RETURN "A_M_M_Beach_01_Latino_FULL_01"
		CASE 23
			RETURN "A_M_M_Beach_01_White_FULL_01"	
		CASE 24
			RETURN "A_M_M_Beach_02_Black_FULL_01"
		CASE 25
			RETURN "A_M_M_Beach_02_White_FULL_01"
		CASE 26
			RETURN "A_M_M_BevHills_02_Black_FULL_01"
		CASE 27
			RETURN "A_M_M_BevHills_02_WHITE_FULL_01"
		CASE 28
			RETURN "A_M_M_Business_01_Black_FULL_01"
		CASE 29
			RETURN "A_M_M_EastSA_01_Latino_FULL_01"
		CASE 30
			RETURN "A_M_M_EastSA_02_Latino_FULL_01"
		CASE 31
			RETURN "A_M_M_FatLatin_01_Latino_FULL_01"
		CASE 32
			RETURN "A_M_M_Malibu_01_Black_FULL_01"
		CASE 33
			RETURN "A_M_M_Malibu_01_Latino_FULL_01"
		CASE 34
			RETURN "A_M_M_Malibu_01_White_FULL_01"
		CASE 35
			RETURN "A_M_Y_BeachVesp_01_CHINESE_FULL_01"
		CASE 36
			RETURN "A_M_Y_Business_01_BLACK_FULL_01"
		CASE 37
			RETURN "A_M_Y_Ktown_01_Korean_FULL_01"
		CASE 38
			RETURN "A_M_Y_KTown_02_Korean_FULL_01"
		CASE 39
			RETURN "A_M_Y_MusclBeac_02_Chinese_FULL_01"
		CASE 40
			RETURN "A_M_Y_StWhi_01_White_FULL_01"
		CASE 41
			RETURN "A_M_Y_StWhi_02_White_FULL_01"
		CASE 42
			RETURN "A_M_Y_Vinewood_02_White_FULL_01"
		CASE 43
			RETURN "A_M_Y_Vinewood_03_White_FULL_01"					
	ENDSWITCH

	RETURN "A_F_M_Beach_01_White_FULL_01"
ENDFUNC

PROC PLAY_CASINO_HOUSE_KEEPING_AMBIENT_DIALOGUE()
	IF g_bCasinoShockedAmbientPedDialogue = TRUE
		IF NOT HAS_NET_TIMER_STARTED(CasinoHouseKeepDialogueTimer)
			PLAY_AMBIENT_SPEECH_FROM_POSITION("GENERIC_SHOCKED_MED", GET_CASINO_HOUSE_KEEPING_VOICE(), GET_ENTITY_COORDS(LocalPlayerPed) + << 10, 10, 0 >>, SPEECH_PARAMS_FORCE)
			START_NET_TIMER(CasinoHouseKeepDialogueTimer)
		ELSE
			IF HAS_NET_TIMER_EXPIRED(CasinoHouseKeepDialogueTimer, 5000)
				RESET_NET_TIMER(CasinoHouseKeepDialogueTimer)
			ENDIF
		ENDIF
	ENDIF
ENDPROC


// url:bugstar:6961622 - [BG SCRIPT][PUBLIC][REPORTED] Prison Break - Finale: Players are reporting that Rashkovsky is not spawning or is invisible during the Finale
PROC FIX_FOR_6961622()
	IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
		IF NOT IS_CUTSCENE_PLAYING()
			#IF IS_DEBUG_BUILD
			VECTOR vTemp			
			vTemp = GET_ENTITY_COORDS(localPlayerPed)
			PRINTLN("[LM][FIX_FOR_5255181] - Our V Location: ", vTemp)
			#ENDIF
			
			NETWORK_INDEX niRash = MC_serverBD_1.sFMMC_SBD.niPed[4]
			
			IF NETWORK_DOES_NETWORK_ID_EXIST(niRash)
				PED_INDEX pedRash = NET_TO_PED(niRash)
				IF DOES_ENTITY_EXIST(pedRash)
					IF GET_ENTITY_MODEL(pedRash) = IG_RASHCOSVKI // CSB_RASHCOSVKI
						VECTOR vPos = GET_ENTITY_COORDS(pedRash)
						PRINTLN("[LM][FIX_FOR_5255181] - We've found Rashkovsky vPos: ", vPos)
						
						IF ARE_VECTORS_ALMOST_EQUAL(vPos, <<1707, 2520, 45>>, 5.0)
							IF vPos.z <= 45.0
								IF NETWORK_HAS_CONTROL_OF_ENTITY(pedRash)
									CLEAR_PED_TASKS_IMMEDIATELY(pedRash)
									FLOAT fNewZ = 0										
									GET_GROUND_Z_FOR_3D_COORD(vPos, fNewZ)
									vPos.z = fNewZ
									SET_ENTITY_COORDS(pedRash, vPos)										
									FORCE_PED_AI_AND_ANIMATION_UPDATE(pedRash)
									
									#IF IS_DEBUG_BUILD
									vTemp = GET_ENTITY_COORDS(pedRash)
									PRINTLN("[LM][FIX_FOR_5255181] - Shove him down! NewvPos: ", vTemp)
									#ENDIF
									
									//SET_PED_TO_RAGDOLL(pedRash, 0, 250, TASK_RELAX) might try this next.
								ELSE
									PRINTLN("[LM][FIX_FOR_5255181] - We do not have control of rashkovsky. ")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[LM][FIX_FOR_5255181] - Waiting for Cutscene to finish...")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///   For when we have to port BG fixes into the main script file.
PROC PROCESS_PRE_CONTROLLER_LOGIC_FIXES()
	FIX_FOR_6961622()
ENDPROC



























