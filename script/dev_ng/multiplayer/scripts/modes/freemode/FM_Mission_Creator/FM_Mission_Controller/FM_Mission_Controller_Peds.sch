
USING "FM_Mission_Controller_USING.sch"
USING "FM_Mission_Controller_Utility.sch"
USING "FM_Mission_Controller_Entities.sch"
USING "FM_Mission_Controller_ObjectiveCompletion.sch"
USING "FM_Mission_Controller_SpecialCase.sch"
USING "FM_Mission_Controller_Rules.sch"
USING "FM_Mission_Controller_Doors.sch"
USING "FM_Mission_Controller_Cutscene.sch"
USING "FM_Mission_Controller_Debug.sch"
USING "FM_Mission_Controller_Locations.sch"
USING "FM_Mission_Controller_Vehicles.sch"
USING "FM_Mission_Controller_Peds_Brain.sch"
USING "FM_Mission_Controller_Peds_Body.sch"


//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: EVERY FRAME PED CHECKS / PED HUD AND BLIPS !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************

PROC PROCESS_HIGH_PRIORITY_PEDS()
	//These peds get processed every frame:
	INT i 
	REPEAT FMMC_MAX_PRIORITY_PEDS i
		IF g_FMMC_STRUCT.iHighPriorityPed[i] >= 0
		AND g_FMMC_STRUCT.iHighPriorityPed[i] < MC_serverBD.iNumPedCreated
		AND iStaggeredIterator != g_FMMC_STRUCT.iHighPriorityPed[i]
			PROCESS_PED_BRAIN(g_FMMC_STRUCT.iHighPriorityPed[i], TRUE)
			PROCESS_PED_BODY(g_FMMC_STRUCT.iHighPriorityPed[i], TRUE)
		ENDIF
	ENDREPEAT
ENDPROC

PROC RUN_CC_PED_HUD(INT iped, PED_INDEX tempPed)
	
	IF NOT IS_PED_INJURED(tempPed)
		
		IF NOT DOES_BLIP_EXIST(biPedBlip[iped])
		
			#IF IS_DEBUG_BUILD
				IF biHostilePedBlip[iped].PedID != NULL
					CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][AI] Ped ", iped, " AI blip cleaning up due to crowd control blip being created.")
				ENDIF
			#ENDIF
			CLEANUP_AI_PED_BLIP(biHostilePedBlip[iped])
			SET_PED_HAS_AI_BLIP(TempPed, FALSE)
			
			CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION] Adding crowd control script blip for ped ", iped, ".")
			biPedBlip[iped] = ADD_BLIP_FOR_ENTITY(TempPed)
			
			SET_BLIP_SPRITE(biPedBlip[iped],RADAR_TRACE_AI)
			SET_BLIP_COLOUR(biPedBlip[iped],BLIP_COLOUR_BLUE)
			SET_BLIP_PRIORITY(biPedBlip[iped],BLIPPRIORITY_HIGHEST)
			SET_BLIP_CUSTOM_NAME(biPedBlip[iped],MC_playerBD[iPartToUse].iteam,MC_serverBD_4.iPedPriority[iped][MC_playerBD[iPartToUse].iteam], iPedBlipCachedNamePriority[iped])
		
		// Should we show the height indicator?
		ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_ShowBlipHeightIndicator)
			SHOW_HEIGHT_ON_BLIP(biPedBlip[iped], TRUE) 
		ENDIF
		
	ELSE // Else this ped is injured
		#IF IS_DEBUG_BUILD
			IF biHostilePedBlip[iped].PedID != NULL
				CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][AI] Ped ", iped, " AI blip cleaning up due to crowd control ped being injured.")
			ENDIF
		#ENDIF
		CLEANUP_AI_PED_BLIP(biHostilePedBlip[iped])
		
		IF DOES_BLIP_EXIST(biPedBlip[iped])
			CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] Removing script blip for ped ", iped, " due to crowd control ped being injured.")
			REMOVE_BLIP(biPedBlip[iped])
		ENDIF
		
		IF IS_BIT_SET(iPedTagCreated[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		    REMOVE_PED_OVERHEAD_ICONS(iPedTag[iped], iPed, iPedTagCreated,iPedTagInitalised)
		ENDIF
	ENDIF
	
ENDPROC

PROC RUN_CHARM_PED_LOGIC(INT iped, PED_INDEX tempPed)
	
		IF NOT DOES_BLIP_EXIST(biPedBlip[iped])
		
			#IF IS_DEBUG_BUILD
				IF biHostilePedBlip[iped].PedID != NULL
					CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][AI] Ped ", iped, " AI blip cleaning up due to charm blip being created.")
				ENDIF
			#ENDIF
			CLEANUP_AI_PED_BLIP(biHostilePedBlip[iped])
			SET_PED_HAS_AI_BLIP(TempPed, FALSE)
			
			CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION] Adding charm script blip for ped ", iped, ".")
			biPedBlip[iped] = ADD_BLIP_FOR_ENTITY(TempPed)
			SET_BLIP_SPRITE(biPedBlip[iped],RADAR_TRACE_AI)
			SET_BLIP_COLOUR(biPedBlip[iped],BLIP_COLOUR_BLUE)
			SET_BLIP_PRIORITY(biPedBlip[iped],BLIPPRIORITY_HIGHEST)
			SET_BLIP_CUSTOM_NAME(biPedBlip[iped],MC_playerBD[iPartToUse].iteam,MC_serverBD_4.iPedPriority[iped][MC_playerBD[iPartToUse].iteam], iPedBlipCachedNamePriority[iped])
		ENDIF
		
		IF MC_playerBD[iPartToUse].iPedCharming = iped OR MC_playerBD[iPartToUse].iPedCharming = -1
			IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed,tempPed) < ci_CHARM_RANGE
				IF iSpectatorTarget = -1
					PRINTLN("[RCC MISSION] Charm ped triggered")
					IF bIsLocalPlayerHost
	//					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MC_CHARM_1")
	//						PRINT_HELP("MC_CHARM_1")
	//					ENDIF
						SET_BIT(MC_serverBD.iPedInvolvedInMiniGameBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					ELSE
						BROADCAST_FMMC_PED_ADDED_TO_CHARM(iped)
					ENDIF
					MC_playerBD[iPartToUse].iPedCharming = iped
				ENDIF
			ENDIF
			
			IF MC_playerBD[iPartToUse].iPedCharming = iped
				IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed,tempPed) > ci_CHARM_EXIT_RANGE
					IF iSpectatorTarget = -1
						IF bIsLocalPlayerHost
							CLEAR_BIT(MC_serverBD.iPedInvolvedInMiniGameBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						ELSE
							BROADCAST_FMMC_PED_REMOVED_FROM_CHARM(iped)
						ENDIF
						MC_playerBD[iPartToUse].iPedCharming = -1
					ENDIF
				ELSE
					IF NOT HAS_PLAYER_CHARMED_PED(sCharmMinigameData)
						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iped])
							RUN_CHARM_PED_MINIGAME(tempPed,sCharmMinigameData)
						ELSE
							NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iped])
						ENDIF
					ELSE
						IF bIsLocalPlayerHost
							CLEAR_BIT(MC_serverBD.iPedInvolvedInMiniGameBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						ELSE
							BROADCAST_FMMC_PED_REMOVED_FROM_CHARM(iped)
						ENDIF
						MC_playerBD[iPartToUse].iPedCharmed  = iped
					ENDIF
				ENDIF
			ENDIF
		ENDIF
ENDPROC

FUNC BOOL IS_PED_STATE_OK_FOR_PHOTO(INT iped)

	IF IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
		IF IS_BIT_SET(MC_serverBD.iDeadPedPhotoBitset[MC_playerBD[iPartToUse].iteam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			RETURN TRUE
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(MC_serverBD.iDeadPedPhotoBitset[MC_playerBD[iPartToUse].iteam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
	
ENDFUNC

FUNC INT GET_PED_PHOTO_RANGE_SQUARED(INT iped)

	INT irange = ci_PHOTO_RANGE
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedPhotoRange > 0
		irange = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedPhotoRange 
	ENDIF

	RETURN (irange * irange)
	
ENDFUNC

PROC PROCESS_CLEANING_UP_PED_ANIM_OBJECTS(PED_INDEX tempPed, INT iped)
	
	IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim = ciPED_IDLE_ANIM__UNFSHDBIZ_JUDGE_IDLE)
		IF IS_BIT_SET(iPedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			IF (IS_PED_INJURED(tempPed)
				OR SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM,TRUE)
				OR IS_ENTITY_PLAYING_ANIM(tempPed,"anim@heists@prison_heistunfinished_biztarget_idle","target_breakout")
				OR ( IS_ENTITY_PLAYING_ANIM(tempPed,"anim@heists@prison_heistunfinished_biz@popov_react","popov_react")
					 AND (GET_ENTITY_ANIM_CURRENT_TIME(tempPed,"anim@heists@prison_heistunfinished_biz@popov_react","popov_react") >= 0.023188) )
				)
				VECTOR vCoords = <<-1806.5146, 427.7325, 131.7362>>
				OBJECT_INDEX oiTablet
				oiTablet = GET_CLOSEST_OBJECT_OF_TYPE(vCoords,5,PROP_CS_TABLET, false, false, false)
				
				IF DOES_ENTITY_EXIST(oiTablet)
				AND IS_ENTITY_ATTACHED_TO_ENTITY(oiTablet,NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niPed[iped]))
					IF NETWORK_HAS_CONTROL_OF_ENTITY(oiTablet)
						DETACH_ENTITY(oiTablet,FALSE)
						SET_OBJECT_AS_NO_LONGER_NEEDED(oiTablet)
					ELSE
						NETWORK_REQUEST_CONTROL_OF_ENTITY(oiTablet)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELIF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim = ciPED_IDLE_ANIM__WITSEC_CHAIR_SNOOZE)
	OR (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim = ciPED_IDLE_ANIM__WITSEC_CHAIR_STAND_UP)
		IF IS_BIT_SET(iPedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			IF IS_ENTITY_ALIVE(tempPed) 
			AND (IS_PED_INJURED(tempPed) OR SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_SYNCHRONIZED_SCENE,TRUE))
				VECTOR vCoords = GET_ENTITY_COORDS(tempPed, FALSE)
				
				OBJECT_INDEX oiChair
				oiChair = GET_CLOSEST_OBJECT_OF_TYPE(vCoords,5,HEI_PROP_HEI_SKID_CHAIR, false, false, false)
				
				IF DOES_ENTITY_EXIST(oiChair)
				AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(oiChair, FALSE)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(oiChair)	
						ONLY_CLEAN_UP_OBJECT_WHEN_OUT_OF_RANGE(oiChair)
						SET_OBJECT_AS_NO_LONGER_NEEDED(oiChair)
					ELSE
						NETWORK_REQUEST_CONTROL_OF_ENTITY(oiChair)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnim = ciPED_IDLE_ANIM__CLIPBOARD

		IF IS_BIT_SET(iPedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			
			if iped = 10
			
				PRINTLN("[RCC MISSION] clipboard ped 10 test 0")
			
				if does_entity_exist(tempPed)
				
					PRINTLN("[RCC MISSION] clipboard ped 10 exists")
					
					IF not IS_PED_INJURED(tempPed)
					
						printstring("[RCC MISSION] clipboard ped 10 coords 0 = ")
						printvector(get_entity_coords(tempPed,FALSE))
						printnl() 

						printstring("temp_ped ped_index = ")
						printint(native_to_int(tempPed))
						printnl()
					
						PRINTLN("[RCC MISSION] clipboard ped 10 NOT injured")
						
					else 
					
						PRINTLN("[RCC MISSION] clipboard ped 10 injured")
					
					endif 
					
				else 
				
					PRINTLN("[RCC MISSION] clipboard ped 10 does NOT exist")
				
				endif
				
			endif 
			
			PRINTLN("[RCC MISSION] clipboard ped 10 TEST PRINT")
							
			
			IF does_entity_exist(tempPed)
				IF IS_PED_INJURED(tempPed)
				OR SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PERFORM_SEQUENCE,TRUE)
					
					OBJECT_INDEX ObjClipboard		
					ObjClipboard =  GET_CLOSEST_OBJECT_OF_TYPE(get_entity_coords(tempPed,FALSE),5,P_CS_Clipboard, false, false, false)					
					IF DOES_ENTITY_EXIST(ObjClipboard)		
					AND IS_ENTITY_ATTACHED(ObjClipboard)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(ObjClipboard)
							PRINTLN("[RCC MISSION][PROCESS_CLEANING_UP_PED_ANIM_OBJECTS] ObjClipboard iPed: ", iPed, " Setting as no longer needed")
							DETACH_ENTITY(ObjClipboard)
							ACTIVATE_PHYSICS(ObjClipboard)
							SET_OBJECT_AS_NO_LONGER_NEEDED(ObjClipboard)
						else
							PRINTLN("[RCC MISSION][PROCESS_CLEANING_UP_PED_ANIM_OBJECTS] ObjClipboard iPed: ", iPed, " Grabbing Ownership")
							NETWORK_REQUEST_CONTROL_OF_ENTITY(ObjClipboard)
						endif
					endif
					
					OBJECT_INDEX Objpencil	
					Objpencil =  GET_CLOSEST_OBJECT_OF_TYPE(get_entity_coords(tempPed,FALSE),5,prop_pencil_01, false, false, false)					
					IF DOES_ENTITY_EXIST(Objpencil)		
					AND IS_ENTITY_ATTACHED(Objpencil)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(Objpencil)
							PRINTLN("[RCC MISSION][PROCESS_CLEANING_UP_PED_ANIM_OBJECTS] Objpencil iPed: ", iPed, " Setting as no longer needed")
							DETACH_ENTITY(Objpencil)
							ACTIVATE_PHYSICS(Objpencil)
							SET_OBJECT_AS_NO_LONGER_NEEDED(Objpencil)				
						else
							PRINTLN("[RCC MISSION][PROCESS_CLEANING_UP_PED_ANIM_OBJECTS] Objpencil iPed: ", iPed, " Grabbing Ownership")
							NETWORK_REQUEST_CONTROL_OF_ENTITY(Objpencil)
						endif
					endif
					
				endif	
			ENDIF
		endif	
		
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnim = ciPED_IDLE_ANIM__SIT_DOWN_DRINKING
		IF IS_BIT_SET(iPedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			IF IS_PED_INJURED(tempPed)
			OR SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM,TRUE)
				IF NOT IS_PED_INJURED(tempPed)
					SET_PED_LEG_IK_MODE(tempPed,LEG_IK_FULL)	
				ENDIF
				OBJECT_INDEX Objbottle	
				Objbottle =  GET_CLOSEST_OBJECT_OF_TYPE(get_entity_coords(tempPed,FALSE),5,prop_cs_beer_bot_01, false, false, false)					
				IF DOES_ENTITY_EXIST(Objbottle)		
				AND IS_ENTITY_ATTACHED(Objbottle)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(Objbottle)
						DETACH_ENTITY(Objbottle)
						SET_OBJECT_AS_NO_LONGER_NEEDED(Objbottle)					
					else
						NETWORK_REQUEST_CONTROL_OF_ENTITY(Objbottle)
					endif
				endif
				
			endif		
		endif	
	//Unfreeze anim peds and remove ragdoll blocking 
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnim = ciPED_IDLE_ANIM__PRISONBREAK_RASHKOVSKY_IDLE
	or g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnim = ciPED_IDLE_ANIM__STATION_DRUNK_LADY_IDLE
	or g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnim = ciPED_IDLE_ANIM__STATION_DRUNK_LADY_REACT
		if DOES_ENTITY_EXIST(tempPed)
			IF NOT IS_PED_INJURED(tempPed)
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM)
					FREEZE_ENTITY_POSITION(tempPed,false)
					CLEAR_RAGDOLL_BLOCKING_FLAGS(tempPed,RBF_PED_RAGDOLL_BUMP|RBF_PLAYER_BUMP|RBF_PLAYER_IMPACT|RBF_PLAYER_RAGDOLL_BUMP)
				ENDIF
			ENDIF
		endif			
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnim = ciPED_IDLE_ANIM__TREVOR_NARCFIN_BODHI_LOOP
		IF IS_BIT_SET(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			IF 	NOT IS_ENTITY_PLAYING_ANIM(tempPed,"ANIM@HEISTS@NARCOTICS@FINALE@IG_4_CAR_INTRO","TREVOR_LOOP")
			AND NOT IS_ENTITY_PLAYING_ANIM(tempPed,"ANIM@HEISTS@NARCOTICS@FINALE@IG_4_CAR_INTRO","TREVOR_OUTRO")
			
				//remove the phone object once the bodhi scene is over, phone gets placed under the map by the anim so look under the map
				OBJECT_INDEX oiPhone = GET_CLOSEST_OBJECT_OF_TYPE(<< 639, -442, 9>>, 3, PROP_PHONE_ING, false, false, false)
				IF DOES_ENTITY_EXIST(oiPhone)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(oiPhone)
						NETWORK_INDEX niPhone
						niPhone = OBJ_TO_NET(oiPhone)
						DELETE_NET_ID(niPhone)
						PRINTLN("[RCC MISSION] PROCESS_CLEANING_UP_PED_ANIM_OBJECTS - Deleting Trevor's phone.")
					ELSE
						NETWORK_REQUEST_CONTROL_OF_ENTITY(oiPhone)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnim = ciPED_IDLE_ANIM__DOOR_KNOCK 
		IF NOT IS_BIT_SET(iLocalBoolCheck9,LBOOL9_KNOCKING_QUIETED)	
			IF IS_PED_INJURED(tempPed)
				BROADCAST_HEIST_QUIET_KNOCKING(ALL_PLAYERS(),FALSE)
			ENDIF
		ENDIF
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnim = ciPED_IDLE_ANIM__DRINK
		IF IS_BIT_SET(iPedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			IF DOES_ENTITY_EXIST(tempPed)
				IF NOT IS_PED_INJURED(tempPed)
					IF NOT IS_PED_USING_SCENARIO(tempPed, "WORLD_HUMAN_DRINKING")
						DROP_AMBIENT_PROP(tempPed)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC RUN_PED_SPOOKED_EVENT_CHECKS(PED_INDEX tempPed, INT iped)
	
	IF NOT IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
	AND NOT IS_BIT_SET(iPedLocalSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
	AND NOT IS_PED_INJURED(tempPed)
		
		IF IS_BIT_SET( iPedHitByStunGun[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed) )
			EXIT
		ENDIF

		IF ( HAS_PED_RECEIVED_EVENT(tempPed, EVENT_INVALID) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_INVALID) ) // Has the ped received any events?
			
			CPRINTLN( DEBUG_OWAIN, "RUN_PED_SPOOKED_EVENT_CHECKS for ped", iPed )
			
			IF NOT IS_BIT_SET(iPedLocalReceivedEvent_ShotFiredORThreatResponse[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwelve, ciPed_BSTwelve_BlockThreatenEvents)
			AND (NOT IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
			OR HAVE_CLEAR_LOS_TO_ANY_PLAYER_NEARBY(tempPed))
				IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,ciPED_BSThree_StopShotFiredEventsAggroing)
					
					IF HAS_PED_RECEIVED_EVENT(tempPed, EVENT_SHOT_FIRED) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_SHOT_FIRED)
					OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_SHOT_FIRED_WHIZZED_BY) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_SHOT_FIRED_WHIZZED_BY)
					OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_SHOT_FIRED_BULLET_IMPACT) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_SHOT_FIRED_BULLET_IMPACT)
						PRINTLN("[RCC MISSION] RUN_PED_SPOOKED_EVENT_CHECKS - ped to be spooked by shot fired: ",iped)
						#IF IS_DEBUG_BUILD
							EVENT_NAMES eventReceived
							
							IF HAS_PED_RECEIVED_EVENT(tempPed, EVENT_SHOT_FIRED) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_SHOT_FIRED)
								eventReceived = EVENT_SHOT_FIRED
								PRINTLN("[RCC MISSION] RUN_PED_SPOOKED_EVENT_CHECKS - ped to be spooked by EVENT_SHOT_FIRED: ",iped)
							ELIF HAS_PED_RECEIVED_EVENT(tempPed, EVENT_SHOT_FIRED_WHIZZED_BY) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_SHOT_FIRED_WHIZZED_BY)
								eventReceived = EVENT_SHOT_FIRED_WHIZZED_BY
								PRINTLN("[RCC MISSION] RUN_PED_SPOOKED_EVENT_CHECKS - ped to be spooked by EVENT_SHOT_FIRED_WHIZZED_BY: ",iped)
							ELIF HAS_PED_RECEIVED_EVENT(tempPed, EVENT_SHOT_FIRED_BULLET_IMPACT) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_SHOT_FIRED_BULLET_IMPACT)
								eventReceived = EVENT_SHOT_FIRED_BULLET_IMPACT
								PRINTLN("[RCC MISSION] RUN_PED_SPOOKED_EVENT_CHECKS - ped to be spooked by EVENT_SHOT_FIRED_BULLET_IMPACT: ",iped)
							ENDIF
							
							VECTOR vGunshot
							IF GET_POS_FROM_FIRED_EVENT(tempPed, eventReceived, vGunshot)
								PRINTLN("[RCC MISSION] RUN_PED_SPOOKED_EVENT_CHECKS - gunshot from coord ", vGunshot)
							ELSE
								PRINTLN("[RCC MISSION] RUN_PED_SPOOKED_EVENT_CHECKS - GET_POS_FROM_FIRED_EVENT returned false, unable to find where shot came from")
							ENDIF
							PRINTLN("[RCC MISSION] RUN_PED_SPOOKED_EVENT_CHECKS - GET_PED_INDEX_FROM_FIRED_EVENT = ",GET_PED_INDEX_FROM_FIRED_EVENT(tempPed, eventReceived))
						#ENDIF
						SET_BIT(iPedLocalReceivedEvent_ShotFiredORThreatResponse[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					ENDIF
				ELSE
					IF HAS_PED_RECEIVED_EVENT(tempPed, EVENT_RESPONDED_TO_THREAT) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_RESPONDED_TO_THREAT)
					OR ( HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(tempPed,PLAYER_PED_ID()) AND ( IS_PLAYER_AIMING_AT_PED(tempPed) OR IS_PLAYER_AIMING_AT_PED_IN_VEHICLE(tempPed, TRUE) ) ) //if the player is seen aiming at the ped or vehicle the ped is in					
						PRINTLN("[RCC MISSION] RUN_PED_SPOOKED_EVENT_CHECKS - ped to be spooked by receiving event_responded_to_threat or being aimed at: ",iped)
						SET_BIT(iPedLocalReceivedEvent_ShotFiredORThreatResponse[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					ENDIF
				ENDIF		
			ENDIF
			
			//Added event for ped in non interruptible animation to be set to spooked, see B*2150182
			IF ( NOT IS_ANIMATION_INTERRUPTIBLE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim) AND DOES_ANIMATION_NEED_SPECIAL_BREAKOUT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim) )
			OR ( MC_serverBD_2.iPedState[iped] = ciTASK_GOTO_COORDS AND NOT IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)) )
				IF NOT IS_BIT_SET(iPedLocalReceivedEvent_SeenMeleeActionOrPedKilled[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					IF HAS_PED_RECEIVED_EVENT(tempPed, EVENT_RESPONDED_TO_THREAT) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_RESPONDED_TO_THREAT)
					OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_INJURED_CRY_FOR_HELP) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_INJURED_CRY_FOR_HELP)
					OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_SHOCKING_SEEN_PED_KILLED) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_SHOCKING_SEEN_PED_KILLED)
					OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_SHOCKING_SEEN_MELEE_ACTION) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_SHOCKING_SEEN_MELEE_ACTION)
						IF NOT IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
						OR HAVE_CLEAR_LOS_TO_ANY_PLAYER_NEARBY(tempPed)
							PRINTLN("[RCC MISSION] RUN_PED_SPOOKED_EVENT_CHECKS - ped with non interruptible idle animation or active go to is to be spooked by melee action seen: ",iped)
							SET_BIT(iPedLocalReceivedEvent_SeenMeleeActionOrPedKilled[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						ELSE
							PRINTLN("[RCC MISSION] RUN_PED_SPOOKED_EVENT_CHECKS - ped has received a code even for melee attacks but does not have Line of Sight of the melee action iPed: ",iped)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//Lukasz: Added event for peds in vehicles to be spooked by player vehicle collision, fix for B*2193459
			IF NOT IS_BIT_SET(iPedLocalReceivedEvent_VehicleDamageFromPlayer[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				IF IS_PED_SITTING_IN_ANY_VEHICLE(tempPed)
		
					VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_USING(tempPed)
					//PED_INDEX ClosestPlayerPed = GET_PLAYER_PED(INT_TO_PLAYERINDEX(GET_NEAREST_PLAYER_TO_ENTITY(tempPed)))
					//commenting out closest player checks as they might be expensive
					//EVENT_VEHICLE_DAMAGE_WEAPON already checks code side for collisions with player vehicles					
					
					IF DOES_ENTITY_EXIST(tempVeh) AND NOT IS_ENTITY_DEAD(tempVeh)
						//IF NOT IS_PED_INJURED(ClosestPlayerPed) AND IS_PED_IN_ANY_VEHICLE(ClosestPlayerPed)
							IF (HAS_PED_RECEIVED_EVENT(tempPed, EVENT_VEHICLE_DAMAGE_WEAPON) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_VEHICLE_DAMAGE_WEAPON))
							AND GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), tempPed) < 5
							//OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(tempVeh,ClosestPlayerPed) OR IS_ENTITY_TOUCHING_ENTITY(tempVeh,ClosestPlayerPed)
								PRINTLN("[RCC MISSION] RUN_PED_SPOOKED_EVENT_CHECKS - ped in vehicle to be spooked by event EVENT_VEHICLE_DAMAGE_WEAPON for crashing with player vehicle: ",iped)
								SET_BIT(iPedLocalReceivedEvent_VehicleDamageFromPlayer[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							ENDIF
						//ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//Add any other events that need checking every frame here
			
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_AggroWhenHurtOrKilled)
		AND NOT IS_BIT_SET(iPedLocalReceivedEvent_HurtOrKilled[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			IF IS_PED_RAGDOLL(tempPed)
			OR IS_PED_GETTING_UP(tempPed)
			OR IS_PED_IN_WRITHE(tempPed)
				PRINTLN("[RCC MISSION] RUN_PED_SPOOKED_EVENT_CHECKS - ped to be spooked by hurt or killed (ragdoll/getting up/writhe): ",iped)
				
				SET_BIT(iPedLocalReceivedEvent_HurtOrKilled[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				
				#IF IS_DEBUG_BUILD
				IF IS_PED_RAGDOLL(tempPed)
					PRINTLN("[RCC MISSION] RUN_PED_SPOOKED_EVENT_CHECKS - ped to be spooked by ragdolling: ",iped)
				ENDIF
				IF IS_PED_GETTING_UP(tempPed)
					PRINTLN("[RCC MISSION] RUN_PED_SPOOKED_EVENT_CHECKS - ped to be spooked by getting up: ",iped)
				ENDIF
				IF IS_PED_IN_WRITHE(tempPed)
					PRINTLN("[RCC MISSION] RUN_PED_SPOOKED_EVENT_CHECKS - ped to be spooked by writhing: ",iped)
				ENDIF
				#ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

PROC PROCESS_PED_BLOCKING_ZONE(INT iPed, PED_INDEX piPed)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetSeven, ciPED_BSSeven_BlockWantedAroundPed) 
	AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].fBlockRange > 0)
	AND NOT IS_PED_INJURED(piPed)
		
		VECTOR vAreaCoord = GET_ENTITY_COORDS(piPed)
		VECTOR vPlayerCoord = GET_ENTITY_COORDS(LocalPlayerPed)
		FLOAT fRadius = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].fBlockRange
		
		IF (fRadius * fRadius) > VDIST2(vPlayerCoord, vAreaCoord)
			PRINTLN("[JS] PROCESS_PED_BLOCKING_ZONE - Player is within the blocking zone (", fRadius, "m) of ped ", iPed,", setting max wanted level to the player's current wanted level (",GET_PLAYER_WANTED_LEVEL(LocalPlayer),")") 
			SET_BIT(iLocalBoolCheck13, LBOOL13_PLAYERINPEDBLOCKINGZONE)
		ELSE
			PRINTLN("[JS] PROCESS_PED_BLOCKING_ZONE - Player is not within zone (", fRadius, "m) of ped ", iPed,", setting max wanted level back to default")
		ENDIF
	ENDIF
	
ENDPROC

PROC CLEANUP_PED_INVENTORY(PED_INDEX tempPed, INT iped)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iInventoryBS,ciPED_INV_Briefcase_prop_ld_case_01)
		
		OBJECT_INDEX oiBrief = GET_CLOSEST_OBJECT_OF_TYPE(GET_PED_BONE_COORDS(tempPed,BONETAG_PH_R_HAND,<<0.094,0.020,-0.005>>),0.5,PROP_LD_CASE_01, false, false, false)
		
		IF DOES_ENTITY_EXIST(oiBrief)
		AND IS_ENTITY_ATTACHED_TO_ENTITY(oiBrief,tempPed)
			
			//Need to check that this briefcase isn't a pickup the ped is carrying, but the actual briefcase in his/her hand:
			BOOL bDelete = TRUE
			INT iObj
			
			FOR iObj = 0 TO (MC_serverBD.iNumObjCreated-1)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iAttachParent = iped
					IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iObj])
					AND oiBrief = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iObj])
						PRINTLN("[RCC MISSION] CLEANUP_PED_INVENTORY - Closest briefcase to hand prop bone returned as the ped's held pickup, aborting cleanup...")
						bDelete = FALSE
					ENDIF
					iObj = MC_serverBD.iNumObjCreated //Break out!
				ENDIF
			ENDFOR
			
			IF bDelete
				
				IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(oiBrief)
					NETWORK_REQUEST_CONTROL_OF_ENTITY(oiBrief)
				ELSE
					IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,ciPED_BSThree_DropInventoryOnDeath)
						PRINTLN("[RCC MISSION] CLEANUP_PED_INVENTORY - Deleting briefcase")
						DELETE_OBJECT(oiBrief)
					ELSE
						DETACH_ENTITY(oiBrief)
						SET_OBJECT_AS_NO_LONGER_NEEDED(oiBrief)
						PRINTLN("[RCC MISSION] CLEANUP_PED_INVENTORY - Dropping briefcase")
					ENDIF
					SET_BIT(iPedInventoryCleanedUpBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					BROADCAST_FMMC_PED_INVENTORY_CLEANUP(iped)
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_PED_INVENTORY(PED_INDEX tempPed, INT iped)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iInventoryBS,ciPED_INV_Briefcase_prop_ld_case_01)
		
		OBJECT_INDEX oiBrief = GET_CLOSEST_OBJECT_OF_TYPE(GET_PED_BONE_COORDS(tempPed,BONETAG_PH_R_HAND,<<0.094,0.020,-0.005>>),0.5,PROP_LD_CASE_01, false, false, false)
		
		IF DOES_ENTITY_EXIST(oiBrief)
		AND IS_ENTITY_ATTACHED_TO_ENTITY(oiBrief,tempPed)
			
			IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(oiBrief)
				NETWORK_REQUEST_CONTROL_OF_ENTITY(oiBrief)
			ELSE
				//No need to check if this is the pickup (as in CLEANUP_PED_INVENTORY), as we can't change its visibility like this anyway
				
				BOOL bVis = IS_ENTITY_VISIBLE(oiBrief)
				BOOL bVeh = IS_PED_IN_ANY_VEHICLE(tempPed)
				
				IF bVeh
					IF bVis
						PRINTLN("[RCC MISSION] PROCESS_PED_INVENTORY - Setting briefcase as invisible")
						SET_ENTITY_VISIBLE(oiBrief,FALSE)
					ENDIF
				ELSE
					IF NOT bVis
						PRINTLN("[RCC MISSION] PROCESS_PED_INVENTORY - Setting briefcase as visible")
						SET_ENTITY_VISIBLE(oiBrief,TRUE)
					ENDIF
				ENDIF
				
			ENDIF
			
		ENDIF
		
	ENDIF
	
ENDPROC

FUNC BOOL WAS_THIS_VEHICLE_ON_A_KILL_RULE(INT iVeh, INT iTeam)
	
	BOOL bWasOnKillRule = FALSE
	BOOl bCheckExtraObjectives = FALSE
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iPriority[iTeam] < FMMC_MAX_RULES
	AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iRule[iTeam] != FMMC_OBJECTIVE_LOGIC_NONE
		IF (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iPriority[iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam])
			IF (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iRule[iTeam] = FMMC_OBJECTIVE_LOGIC_KILL)
				bWasOnKillRule = TRUE
			ELSE
				bCheckExtraObjectives = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF bCheckExtraObjectives
		INT iextraObjectiveNum
		// Loop through all potential entites that have multiple rules
		FOR iextraObjectiveNum = 0 TO ( MAX_NUM_EXTRA_OBJECTIVE_ENTITIES-1 )
			// If the type matches the one passed
			IF g_FMMC_STRUCT.iExtraObjectiveEntityType[iextraObjectiveNum] = ciRULE_TYPE_VEHICLE
			AND g_FMMC_STRUCT.iExtraObjectiveEntityId[iextraObjectiveNum] = iVeh
				INT iextraObjectiveLoop
				
				FOR iextraObjectiveLoop = 0 TO (MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY - 1)
					IF g_FMMC_STRUCT.iExtraObjectivePriority[iextraObjectiveNum][iextraObjectiveLoop][iTeam] < FMMC_MAX_RULES
					AND (GET_FMMC_RULE_FROM_CREATOR_RULE(g_FMMC_STRUCT.iExtraObjectiveRule[iextraObjectiveNum][iextraObjectiveLoop][iTeam]) != FMMC_OBJECTIVE_LOGIC_NONE)
						
						IF (g_FMMC_STRUCT.iExtraObjectivePriority[iextraObjectiveNum][iextraObjectiveLoop][iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam])
							IF (GET_FMMC_RULE_FROM_CREATOR_RULE(g_FMMC_STRUCT.iExtraObjectiveRule[iextraObjectiveNum][iextraObjectiveLoop][iTeam]) = FMMC_OBJECTIVE_LOGIC_KILL)
								bWasOnKillRule = TRUE
								iextraObjectiveLoop = MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY //Break out
							ENDIF
						ELSE
							iextraObjectiveLoop = MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY //Break out
						ENDIF
						
					ENDIF
				ENDFOR
				//We found the multi-rule number for this entity, there won't be any more so break out of the loop:
				iextraObjectiveNum = MAX_NUM_EXTRA_OBJECTIVE_ENTITIES
			ENDIF
		ENDFOR
	ENDIF
	
	RETURN bWasOnKillRule
	
ENDFUNC

FUNC BOOL HAS_PED_SPAWN_DELAY_EXPIRED(INT iped)
	
	BOOL bExpired
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iRespawnDelay != 0
	AND IS_BIT_SET( MC_serverBD.iObjSpawnPedBitset[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
		IF NOT HAS_NET_TIMER_STARTED(MC_serverBD_3.tdPedSpawnDelay[iped])
			REINIT_NET_TIMER(MC_serverBD_3.tdPedSpawnDelay[iped])
		ELSE
			INT iTimeScale = 1
			IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
				iTimeScale = 1000
			ENDIF
			
			IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdPedSpawnDelay[iped]) >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iRespawnDelay*iTimeScale
				bExpired = TRUE
			ENDIF
		ENDIF
	ELSE
		bExpired = TRUE
	ENDIF
	
	RETURN bExpired
	
ENDFUNC

PROC MAINTAIN_RUSSIAN_CLOAK_VISIBILITY(INT iPed)
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(mc_serverBD_1.sFMMC_SBD.niPed[iPed])
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iBecomeCloakedOnRule < 0
		EXIT
	ENDIF
	
	PED_INDEX piCurPed = NET_TO_PED(mc_serverBD_1.sFMMC_SBD.niPed[iPed])
	BOOL bCurrentlyInvisible = IS_PED_INVISIBLE(iPed)
	//PRINTLN("[TMS] ped ", iPed, " currently invisible = ", bCurrentlyInvisible, " for player ", iLocalPart)
	
	IF IS_PED_INJURED(piCurPed)
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxFlickeringInvisiblePedEffects[iPed])
			REMOVE_PARTICLE_FX(ptfxFlickeringInvisiblePedEffects[iPed])
			PRINTLN("[FlickeringPeds] - removing particles for ped ", iPed)
		ENDIF
	ENDIF

	IF bCurrentlyInvisible
	
		IF IS_PED_INJURED(piCurPed) //Sets ped to be visible upon death
			RESET_ENTITY_ALPHA(piCurPed)
			EXIT
		ENDIF
	
		IF GET_USINGSEETHROUGH()
			SET_ENTITY_ALPHA(piCurPed, 50, FALSE)
			IF NOT IS_PED_INJURED(piCurPed)
				SET_PED_CAN_BE_TARGETTED(piCurPed, TRUE)
			ENDIF
		ELSE
			SET_ENTITY_ALPHA(piCurPed, 0, FALSE)
			
			IF NOT IS_PED_INJURED(piCurPed)
				SET_PED_CAN_BE_TARGETTED(piCurPed, FALSE)
			ENDIF
		ENDIF
		
		IF SHOULD_PED_FLICKER(iPed)
			PRINTLN("[TMS][FlickeringPeds] Flickering ped ", iPed)
			SET_ENTITY_ALPHA(piCurPed, 50, FALSE)
		ENDIF
	ELSE
		IF NOT IS_PED_INJURED(piCurPed)
		AND GET_ENTITY_ALPHA(piCurPed) < 100
			SET_PED_CAN_BE_TARGETTED(piCurPed, TRUE)
		ENDIF
		
		RESET_ENTITY_ALPHA(piCurPed)
	ENDIF
ENDPROC

PROC PROCESS_COP_DECOY_PED_BLIP_AND_HUD(INT iPed, PED_INDEX piPed)
	UNUSED_PARAMETER(iPed)
	
	IF NOT IS_BIT_SET(iCopDecoyBS, ci_COP_DECOY_CREATED_BLIP)
		IF IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_COP_DECOY_STARTED)
			IF NOT DOES_BLIP_EXIST(biCopDecoyBlip)
			AND IS_ENTITY_ALIVE(piPed)
				VECTOR vPos = GET_ENTITY_COORDS(piPed)
				biCopDecoyBlip = ADD_BLIP_FOR_RADIUS(vPos, 100.0)
				SET_BLIP_COLOUR(biCopDecoyBlip, BLIP_COLOUR_RED)
				SET_BLIP_ALPHA(biCopDecoyBlip, 120)
				SET_BIT(iCopDecoyBS, ci_COP_DECOY_CREATED_BLIP)
				PRINTLN("[CopDecoy] Created Decoy Blip at: << ", vPos.x, ", ", vPos.y, ", ", vPos.z, " >>." )
			ENDIF
		ENDIF	
	ELIF IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_COP_DECOY_EXPIRED)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_COP_DECOY_FLEEING)
		IF DOES_BLIP_EXIST(biCopDecoyBlip)
			REMOVE_BLIP(biCopDecoyBlip)
			PRINTLN("[CopDecoy] Removing Decoy Blip.")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_COP_DECOY_STARTED)
	AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_COP_DECOY_EXPIRED)
	AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_COP_DECOY_FLEEING)
		IF NOT IS_BIT_SET(iCopDecoyBS, ci_COP_DECOY_SHOWN_HELP_1)
			PRINT_HELP("FMMC_CSH_CDEC1")
			SET_BIT(iCopDecoyBS, ci_COP_DECOY_SHOWN_HELP_1)
		ENDIF
		
		IF NOT IS_BIT_SET(iCopDecoyBS, ci_COP_DECOY_SHOWN_HELP_2)
		AND bLocalPlayerPedOk
		AND IS_ENTITY_IN_RANGE_COORDS(LocalPlayerPed, GET_ENTITY_COORDS(piPed, FALSE), 200.0)
			PRINT_HELP("FMMC_CSH_CDEC2")
			SET_BIT(iCopDecoyBS, ci_COP_DECOY_SHOWN_HELP_2)
		ENDIF
	
		//Fake health bar HUD
		BOOL isLiteralString = TRUE
		TEXT_LABEL_63 tl63_Temp = "Test"
		HUDORDER hudOrderHB = HUDORDER_DONTCARE
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCustomPedName > -1
			tl63_Temp = CONVERT_STRING_TO_UPPERCASE(g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCustomPedName])
		ELSE
			tl63_Temp = CONVERT_STRING_TO_UPPERCASE(GET_CREATOR_NAME_FOR_PED_MODEL(GET_ENTITY_MODEL(piPed)))
			isLiteralString = FALSE
		ENDIF
		
		FLOAT fTotalDecoyTime = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedCopDecoyActiveTime * 1000)
		
		IF HAS_NET_TIMER_STARTED(MC_serverBD_1.stCopDecoyActiveTimer)
			IF iCopDecoyFakeHealthState = 0
				IF HAS_NET_TIMER_EXPIRED(MC_serverBD_1.stCopDecoyActiveTimer, ROUND(fTotalDecoyTime * 0.22))   //22% through time
					iCopDecoyCurrentFakeHealth = 92
					iCopDecoyFakeHealthState++
				ENDIF
			ELIF iCopDecoyFakeHealthState = 1
				IF HAS_NET_TIMER_EXPIRED(MC_serverBD_1.stCopDecoyActiveTimer, ROUND(fTotalDecoyTime * 0.29))   //29% through time
					iCopDecoyCurrentFakeHealth = 81
					iCopDecoyFakeHealthState++
				ENDIF
			ELIF iCopDecoyFakeHealthState = 2
				IF HAS_NET_TIMER_EXPIRED(MC_serverBD_1.stCopDecoyActiveTimer, ROUND(fTotalDecoyTime * 0.36))   //36% through time
					iCopDecoyCurrentFakeHealth = 76
					iCopDecoyFakeHealthState++
				ENDIF
			ELIF iCopDecoyFakeHealthState = 3
				IF HAS_NET_TIMER_EXPIRED(MC_serverBD_1.stCopDecoyActiveTimer, ROUND(fTotalDecoyTime * 0.45))   //45% through time
					iCopDecoyCurrentFakeHealth = 72
					iCopDecoyFakeHealthState++
				ENDIF
			ELIF iCopDecoyFakeHealthState = 4
				IF HAS_NET_TIMER_EXPIRED(MC_serverBD_1.stCopDecoyActiveTimer, ROUND(fTotalDecoyTime * 0.52))   //52% through time
					iCopDecoyCurrentFakeHealth = 64
					iCopDecoyFakeHealthState++
				ENDIF
			ELIF iCopDecoyFakeHealthState = 5
				IF HAS_NET_TIMER_EXPIRED(MC_serverBD_1.stCopDecoyActiveTimer, ROUND(fTotalDecoyTime * 0.60))   //60% through time
					iCopDecoyCurrentFakeHealth = 54
					iCopDecoyFakeHealthState++
				ENDIF
			ELIF iCopDecoyFakeHealthState = 6
				IF HAS_NET_TIMER_EXPIRED(MC_serverBD_1.stCopDecoyActiveTimer, ROUND(fTotalDecoyTime * 0.68))   //68% through time
					iCopDecoyCurrentFakeHealth = 42
					iCopDecoyFakeHealthState++
				ENDIF
			ELIF iCopDecoyFakeHealthState = 7
				IF HAS_NET_TIMER_EXPIRED(MC_serverBD_1.stCopDecoyActiveTimer, ROUND(fTotalDecoyTime * 0.73))   //73% through time
					iCopDecoyCurrentFakeHealth = 36
					iCopDecoyFakeHealthState++
				ENDIF
			ELIF iCopDecoyFakeHealthState = 8
				IF HAS_NET_TIMER_EXPIRED(MC_serverBD_1.stCopDecoyActiveTimer, ROUND(fTotalDecoyTime * 0.80))   //80% through time
					iCopDecoyCurrentFakeHealth = 24
					iCopDecoyFakeHealthState++
				ENDIF
			ELIF iCopDecoyFakeHealthState = 9
				IF HAS_NET_TIMER_EXPIRED(MC_serverBD_1.stCopDecoyActiveTimer, ROUND(fTotalDecoyTime * 0.89))   //89% through time
					iCopDecoyCurrentFakeHealth = 12
					iCopDecoyFakeHealthState++
				ENDIF
			ELIF iCopDecoyFakeHealthState = 10
				IF HAS_NET_TIMER_EXPIRED(MC_serverBD_1.stCopDecoyActiveTimer, ROUND(fTotalDecoyTime * 0.95))   //95% through time
					iCopDecoyCurrentFakeHealth = 6
					iCopDecoyFakeHealthState++
				ENDIF
			ENDIF
		ENDIF
		
		HUD_COLOURS eHudColour = HUD_COLOUR_GREEN
		IF iCopDecoyFakeHealthState >= 9
			eHudColour = HUD_COLOUR_RED
		ENDIF
		
		DRAW_GENERIC_METER(iCopDecoyCurrentFakeHealth, 100, tl63_Temp, eHudColour, -1, hudOrderHB, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, isLiteralString)
	ENDIF
	
ENDPROC

PROC PROCESS_INVISIBLE_PED_BLIPS_EVERY_FRAME(INT iPed, PED_INDEX tempPed, INT iBlipColour, BOOL bCustomBlip)
	UNUSED_PARAMETER(tempPed)
	IF DOES_BLIP_EXIST(biPedBlip[iPed])
	AND IS_PED_INVISIBLE(iPed)
		IF NOT SHOULD_PED_HAVE_BLIP(iped,iBlipColour,bCustomBlip)
			CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION] PROCESS_PED_BLIPS_EVERY_FRAME - Removing blip for ped ", iped," as SHOULD_PED_HAVE_BLIP was negative.")
			REMOVE_BLIP(biPedBlip[iped])
		ENDIF
	ENDIF
ENDPROC

//Note(Tom): Moved this down here so we can call ped_brain and ped_body on the same frame as a ped spawns
PROC DISPLAY_PED_HUD_AND_BLIPS(INT iped)

	VECTOR 	vPedCoords
	PED_INDEX tempPed
	BOOL bHasGoToRange
	INT i
	//These two variables are just so that SHOULD_PED_HAVE_BLIP has enough arguments to compile:
	BOOL bCustomBlip
	INT iBlipColour
	
	#IF IS_DEBUG_BUILD
	IF bOutputCopMissingBlipDebug
		PRINTLN("[RCC MISSION] - DISPLAY_PED_HUD_AND_BLIPS - ********************* PED ", iped, " *********************")
	ENDIF
	#ENDIF
	
	IF bIsLocalPlayerHost
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iped])
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iRespawnOnRuleChangeBS > 0
				IF IS_BIT_SET(MC_serverBD.iPedCleanup_NeedOwnershipBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					
					tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
					
					IF DOES_ENTITY_EXIST(tempPed)
						IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(tempPed)
							NETWORK_REQUEST_CONTROL_OF_ENTITY(tempPed)
							PRINTLN("[RCC MISSION] DISPLAY_PED_HUD_AND_BLIPS - Requesting control of ped ",iped," for cleanup")
						ELSE
							IF (IS_ANY_TEAM_BEING_WARPED_TO_START_POINT()
							AND IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_ALL_PLAYERS_RENDERPHASES_PAUSED))
							OR NOT IS_ANY_TEAM_BEING_WARPED_TO_START_POINT()
								PRINTLN("[RCC MISSION] DISPLAY_PED_HUD_AND_BLIPS - Deleting ped ",iped)
								DELETE_FMMC_PED(iped)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// Need every Frame calls for sync scenes else it is garbage.
	PROCESS_EVERY_FRAME_OWNER_PLAYER_SYNC_SCENE_PEDS(iPed)
	PROCESS_EVERY_FRAME_LOCAL_PLAYER_SYNC_SCENE_PEDS(iPed)
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iped])
		
		RESET_NET_TIMER(maxAccelerationUpdateTimers[iPed])
		CLEANUP_COLLISION_ON_PED(NULL,iped)
		CLEAR_BIT(iEscortingBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		CLEAR_BIT(iGotoObjectBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		CLEAR_BIT(iPedSpecBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		IF DOES_BLIP_EXIST(biPedBlip[iped])
			CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] DISPLAY_PED_HUD_AND_BLIPS - Removing blip for ped ", iped, " as their networkID doesn't exist.")
			REMOVE_BLIP(biPedBlip[iped])
		ENDIF
		IF IS_BIT_SET(iPedTagCreated[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
       	 	REMOVE_PED_OVERHEAD_ICONS(iPedTag[iped], iPed, iPedTagCreated,iPedTagInitalised)
   		ENDIF
		#IF IS_DEBUG_BUILD
		CLEAR_BIT(iPedGivenDebugName[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
		#ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim = ciPED_IDLE_ANIM__GO_FOUNDRY_HOSTAGE_PERP_IDLE
		AND iGOFoundryHostagePerpResetFlagPed = iped
			iGOFoundryHostagePerpResetFlagPed = -1
		ENDIF
		
		IF bIsLocalPlayerHost
			IF IS_BIT_SET(MC_serverBD.iPedCleanup_NeedOwnershipBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iRespawnOnRuleChangeBS = 0
					CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION] DISPLAY_PED_HUD_AND_BLIPS - Ped ",iped," we were trying to request ownership of for cleanup no longer exists!")
				ENDIF
				PRINTLN("[RCC MISSION] DISPLAY_PED_HUD_AND_BLIPS - Ped ",iped," we were trying to request ownership of for cleanup no longer exists!")
				CLEAR_BIT(MC_serverBD.iPedCleanup_NeedOwnershipBS[GET_LONG_BITSET_INDEX(iped)],GET_LONG_BITSET_BIT(iped))
			ENDIF
			
			IF MC_serverBD_2.iCurrentPedRespawnLives[iped] > 0
				IF SHOULD_PED_RESPAWN_NOW(iped)
					IF HAS_PED_SPAWN_DELAY_EXPIRED(iped)
						IF RESPAWN_MISSION_PED(iped)
							MC_serverBD_2.iPedState[iped] = ciTASK_CHOOSE_NEW_TASK
							
							//Do a quick run-through of ped processing & tasking to shunt them into the right stuff quickly
							PROCESS_PED_BRAIN(iped, TRUE)
							PROCESS_PED_BODY(iped, TRUE)
							
							RESET_NET_TIMER(MC_serverBD_3.tdPedSpawnDelay[iped])
							
							IF DOES_PED_HAVE_PLAYER_VARIABLE_SETTING(iped)
								IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedObjective > -1
								AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedTeam > -1
									IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedTeam] < FMMC_MAX_RULES
										MC_serverBD_1.sCreatedCount.iNumPedCreated[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedObjective]++
										PRINTLN("[LM][DISPLAY_PED_HUD_AND_BLIPS] - Added to iNumPedCreated, is now: ", MC_serverBD_1.sCreatedCount.iNumPedCreated[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedObjective], " for iRule: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedObjective)
									ENDIF
								ENDIF
							ENDIF
							IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iRespawnOnRuleChangeBS > 0
								INT iTeam
								FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
									IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iRule[iTeam] != FMMC_OBJECTIVE_LOGIC_NONE
										SET_BIT(MC_serverBD.iPedteamFailBitset[iped],iteam)
										BREAKLOOP
									ENDIF
								ENDFOR
							ENDIF
							IF MC_serverBD_2.iCurrentPedRespawnLives[iped] != UNLIMITED_RESPAWNS
								MC_serverBD_2.iCurrentPedRespawnLives[iped]--
							ENDIF
						ENDIF
					ELSE
						PRINTLN("[LM][DISPLAY_PED_HUD_AND_BLIPS] - SHOULD_PED_RESPAWN_NOW - iPed: ", iPed, " But we are waiting for the DELAY...")
					ENDIF
				ELSE
					IF MC_serverBD.isearchingforPed = iped
						MC_serverBD.isearchingforPed = -1
					ENDIF
					
					IF DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME()
					AND (NOT CAN_PED_STILL_SPAWN_IN_MISSION(iped))
						
						MC_serverBD.iPedteamFailBitset[iped] = 0
						
						INT iTeam
						
						FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
							MC_serverBD_4.iPedPriority[iped][iTeam] = FMMC_PRIORITY_IGNORE
							MC_serverBD_4.iPedRule[iped][iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
							CLEAR_BIT(MC_serverBD.iMissionCriticalPed[iTeam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						ENDFOR
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		EXIT
	ELSE
		tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
		
		// Cleanup ped AI blip only when the ped is injured, not just when
		// the netID gets dereferenced. This lets us clean up peds without losing
		// their combat blips.
		IF biHostilePedBlip[iped].PedID != NULL
			IF IS_PED_INJURED(tempPed)
				CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][DISPLAY_PED_HUD_AND_BLIPS] Ped ", iped, " AI blip cleaning up as the ped is injured.")
				CLEANUP_AI_PED_BLIP(biHostilePedBlip[iped])	
			ENDIF
		ENDIF
				
		IF IS_BIT_SET(iPedStartedInCoverBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			IF NOT IS_PED_IN_COVER(tempPed)
				SET_NETWORK_ID_CAN_MIGRATE(MC_serverBD_1.sFMMC_SBD.niped[iped], TRUE)
				CLEAR_BIT(iPedStartedInCoverBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][DISPLAY_PED_HUD_AND_BLIPS] Ped ", iped, " No longer in cover, can now Migrate, clearing iPedStartedInCoverBitset.")
			ELSE
				CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][DISPLAY_PED_HUD_AND_BLIPS] Ped ", iped, " Started in cover...")
			ENDIF
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedInvincibleOnRules >= 0
			INT currentRule = MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedInvincibleOnRulesTeam]
			IF currentRule+1 < FMMC_MAX_RULES
				BOOL bInvincible = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedInvincibleOnRules, currentRule+1)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(tempPed)
					IF IS_ENTITY_ALIVE(tempPed)
						IF IS_PED_IN_ANY_VEHICLE(tempPed) AND IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(tempPed))
						OR NOT IS_PED_IN_ANY_VEHICLE(tempPed)
							SET_ENTITY_INVINCIBLE(tempPed, bInvincible)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
				
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_EVERY_FRAME_INVISIBLE_PED_BLIPS_ENABLED)
			PROCESS_INVISIBLE_PED_BLIPS_EVERY_FRAME(iPed, tempPed, iBlipColour, bCustomBlip)
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedHealthbarOnRulesTeam > -1
		AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedHealthbarOnRules > -1
		AND IS_ENTITY_ALIVE(tempPed)
		AND NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_FINISHED)
		AND MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedHealthbarOnRulesTeam] < FMMC_MAX_RULES

			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedHealthbarOnRules, MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedHealthbarOnRulesTeam] + 1)
				BOOL isLiteralString = TRUE
				TEXT_LABEL_63 tl63_Temp = "Test"
				HUDORDER hudOrderHB = HUDORDER_DONTCARE
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCustomPedName > -1
					tl63_Temp = CONVERT_STRING_TO_UPPERCASE(g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCustomPedName])
				ELSE
					tl63_Temp = CONVERT_STRING_TO_UPPERCASE(GET_CREATOR_NAME_FOR_PED_MODEL(GET_ENTITY_MODEL(tempPed)))
					isLiteralString = FALSE
				ENDIF
				
				DRAW_GENERIC_METER(GET_ENTITY_HEALTH(tempPed) - 100, GET_PED_MAX_HEALTH(tempPed) - 100, tl63_Temp, HUD_COLOUR_WHITE, -1, hudOrderHB, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, isLiteralString)
			ENDIF
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedFlashBlipRule != -1
		AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedFlashBlipRule < FMMC_MAX_RULES
		AND NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_FLASH_PED_BLIP_EXPIRED)
			IF NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_FLASH_PED_BLIP_ACTIVE)
				INT iTeam
				FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
					IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
					AND MC_serverBD_4.iCurrentHighestPriority[iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedFlashBlipRule
						IF DOES_BLIP_EXIST(biPedBlip[iped])
							SET_BLIP_FLASHES(biPedBlip[iped], TRUE)
							SET_BIT(iLocalBoolCheck24, LBOOL24_FLASH_PED_BLIP_ACTIVE)
							PRINTLN("[RCC MISSION][DISPLAY_PED_HUD_AND_BLIPS] Starting flashing blip. Rule: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedFlashBlipRule, " Duration: ", (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedFlashBlipDuration * 1000))
						ENDIF
					ENDIF
				ENDFOR
			ELSE
				IF HAS_NET_TIMER_STARTED(tdPedBlipFlash)
					IF HAS_NET_TIMER_EXPIRED(tdPedBlipFlash, (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedFlashBlipDuration * 1000))
						IF DOES_BLIP_EXIST(biPedBlip[iped])
							SET_BLIP_FLASHES(biPedBlip[iped], FALSE)
							RESET_NET_TIMER(tdPedBlipFlash)
							SET_BIT(iLocalBoolCheck24, LBOOL24_FLASH_PED_BLIP_EXPIRED)
							PRINTLN("[RCC MISSION][DISPLAY_PED_HUD_AND_BLIPS] Ending flashing blip")
						ELSE
							RESET_NET_TIMER(tdPedBlipFlash)
							SET_BIT(iLocalBoolCheck24, LBOOL24_FLASH_PED_BLIP_EXPIRED)
							PRINTLN("[RCC MISSION][DISPLAY_PED_HUD_AND_BLIPS] Ending flashing blip")
						ENDIF
					ENDIF
				ELSE
					REINIT_NET_TIMER(tdPedBlipFlash)
				ENDIF
			ENDIF
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_TAGGING_TARGETS)
			IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam] < FMMC_MAX_RULES
				IF IS_IT_SAFE_TO_ADD_TAG_BLIP(MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam])
					INT iTagged
					FOR iTagged = 0 TO FMMC_MAX_TAGGED_ENTITIES-1
						IF MC_serverBD_3.iTaggedEntityType[iTagged] = ENUM_TO_INT(GET_ENTITY_TYPE(tempPed))
							IF MC_serverBD_3.iTaggedEntityIndex[iTagged] = iPed
								IF IS_ENTITY_ALIVE(tempPed)
									VECTOR vEntityCoords = GET_ENTITY_COORDS(tempPed)
									vEntityCoords.z = (vEntityCoords.z + 1.0)
									DRAW_TAGGED_MARKER(vEntityCoords, tempPed, ENUM_TO_INT(GET_ENTITY_TYPE(tempPed)), iPed)
									
									IF NOT DOES_BLIP_EXIST(biTaggedEntity[iTagged])
									AND NOT DOES_BLIP_EXIST(GET_BLIP_FROM_ENTITY(tempPed))
										ADD_TAGGED_BLIP(tempPed, ENUM_TO_INT(GET_ENTITY_TYPE(tempPed)), iPed, iTagged)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
		ENDIF
		
		PROCESS_PED_PHONE_EMP_REACTIONS(iPed, tempPed)
		
		IF IS_PED_INJURED(tempPed)
			IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(tempPed)
				IF IS_PED_IN_ANY_VEHICLE(tempPed)
					VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(tempPed)
					DOES_PED_IN_GOTO_TASK_HAVE_TO_WAIT(tempPed, tempVeh, iped, TRUE, FALSE)
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	IF bIsLocalPlayerHost
		IF IS_BIT_SET(MC_serverBD.iPedCleanup_NeedOwnershipBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			
			IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(tempPed)
				NETWORK_REQUEST_CONTROL_OF_ENTITY(tempPed)
				PRINTLN("[RCC MISSION] DISPLAY_PED_HUD_AND_BLIPS - Requesting control of ped ",iped," for cleanup")
			ELSE
				IF (IS_ANY_TEAM_BEING_WARPED_TO_START_POINT()
				AND IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_ALL_PLAYERS_RENDERPHASES_PAUSED))
				OR NOT IS_ANY_TEAM_BEING_WARPED_TO_START_POINT()
					PRINTLN("[RCC MISSION] DISPLAY_PED_HUD_AND_BLIPS - Deleting ped ",iped)
					DELETE_FMMC_PED(iped)
				ENDIF
				EXIT
			ENDIF
			
		ELIF DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME(TRUE)
			
			INT iTeam
			
			IF MC_serverBD.iPedteamFailBitset[iped] > 0
				FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
					IF IS_BIT_SET(MC_serverBD.iPedteamFailBitset[iped], iTeam)
					AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iMissionCriticalRemovalRule[iTeam] > 0)
					AND MC_serverBD_4.iCurrentHighestPriority[iTeam] >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iMissionCriticalRemovalRule[iTeam]
						CLEAR_BIT(MC_serverBD.iMissionCriticalPed[iTeam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						CLEAR_BIT(MC_serverBD.iPedteamFailBitset[iped], iTeam)
						PRINTLN("[RCC MISSION] DISPLAY_PED_HUD_AND_BLIPS - Ped ",iped," set as no longer critical for team ",iteam," as they have reached iMissionCriticalRemovalRule ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iMissionCriticalRemovalRule[iTeam])
					ENDIF
				ENDFOR
			ENDIF
			
			IF CLEANUP_PED_EARLY(iped,tempPed)
				PRINTLN("[RCC MISSION] DISPLAY_PED_HUD_AND_BLIPS - Cleaning up ped ",iped," early, new priority/midpoint check")
				EXIT
			ENDIF
		ENDIF
	ENDIF
	
	IF NETWORK_HAS_CONTROL_OF_ENTITY(tempPed)
	
		//must be called regardless if ped is injured or not. 
		//Must be called every frame instead of within PROCESS_PED_BODY() as the ped could be cleaned up instantly
		//and by the time PROCESS_PED_BODY()processes the ped it may be many frames later and the ped won't exist.
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim != ciPED_IDLE_ANIM__NONE
			
			PROCESS_CLEANING_UP_PED_ANIM_OBJECTS(tempPed, iped)
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim = ciPED_IDLE_ANIM__GO_FOUNDRY_HOSTAGE_PERP_IDLE
			AND iGOFoundryHostagePerpResetFlagPed = iped
				IF NOT IS_PED_INJURED(tempPed)
				AND NOT IS_PED_IN_COMBAT(tempPed)
					SET_PED_RESET_FLAG(tempPed, PRF_InstantBlendToAim, TRUE)
				ELSE
					iGOFoundryHostagePerpResetFlagPed = -1
				ENDIF
			ENDIF
			
		ENDIF
		
		IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iInventoryBS > 0)
		AND NOT IS_BIT_SET(iPedInventoryCleanedUpBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			IF IS_PED_INJURED(tempPed)
				CLEANUP_PED_INVENTORY(tempPed,iped)
			ELSE
				PROCESS_PED_INVENTORY(tempPed, iped)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour, ciPED_BSFour_ReactToFriendlyGunfire)
		AND NOT IS_PED_INJURED(tempPed)
		AND NOT IS_PED_IN_COMBAT(tempPed)
			SET_PED_RESET_FLAG(tempPed, PRF_CanBePinnedByFriendlyBullets, TRUE)
		ENDIF	
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPed_BSEight_DisableSpookedAudio)
		AND NOT IS_PED_INJURED(tempPed)
			SET_PED_RESET_FLAG(tempPed, PRF_DisableFriendlyGunReactAudio, TRUE)
			PRINTLN("[RCC MISSION] FRIENDLY REACTION - Setting PRF_DisableFriendlyGunReactAudio")
		ENDIF
		
		IF DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME(TRUE)
			//Giving weapons on certain rules:
			IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iGunOnRuleTeam != -1)
			AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iGunOnRuleRule != -1)
			AND ((g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].wtGunOnRule != WEAPONTYPE_UNARMED) AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].wtGunOnRule != WEAPONTYPE_INVALID))
				PROCESS_GIVING_PED_WEAPON_ON_RULE(tempPed,iped)
			ENDIF
			
			// Changing combat style on a new rule
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_Team != -1
			AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_Rule != -1
			AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_NewStyle != -1
				PROCESS_CHANGING_PED_COMBAT_STYLE_ON_RULE(tempPed, iped)
			ENDIF
		ENDIF
		
		#IF IS_NEXTGEN_BUILD
		IF( GET_ENTITY_MODEL( tempPed ) = IG_MONEY ) // Avi Schwartzman B* 2172537
			IF( IS_PED_IN_ANY_BOAT( tempPed ) )
				SET_PED_RESET_FLAG( tempPed, PRF_PlayAgitatedAnimsInVehicle, TRUE )
			ENDIF
		ENDIF
		#ENDIF
	ENDIF
	
	//url:bugstar:6201476 - Casino Heist - NOOSE outfit doesn't work on placed cop peds if we're giving the players a Wanted Level - make sure rel group is overridden on spawn
	OVERRIDE_PED_RELATIONSHIP_TO_PLAYER_IF_IN_VALID_DISGUISE(tempPed, iPed)
	
	//Some events don't stick around long enough to be picked up in the staggered loop, check for those ones here:
	RUN_PED_SPOOKED_EVENT_CHECKS(tempPed, iped)
	
	PROCESS_PED_BLOCKING_ZONE(iped, tempPed)
	
	//If the vehicle we're in is blipped hide any kind of AI blip including cop blips.
	//First of all detect whether we want to hide or not.
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_AiBlipsOnly)
		IF NOT IS_BIT_SET(biHostilePedBlip[iped].iStateBitset, BIT_AI_BLIP_STATE_HIDDEN)
			IF IS_PED_IN_ANY_VEHICLE(tempPed)
				VEHICLE_INDEX VehicleID = GET_VEHICLE_PED_IS_IN(tempPed)
				IF DOES_ENTITY_EXIST(VehicleID)
					BLIP_INDEX BlipID = GET_BLIP_FROM_ENTITY(VehicleID)
					IF DOES_BLIP_EXIST(BlipID)
						#IF IS_DEBUG_BUILD
							IF (GET_FRAME_COUNT()%100) = 0
								CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][AI] Hiding AI blip as the ped is in a blipped vehicle.")
							ENDIF
						#ENDIF
						SET_BIT(biHostilePedBlip[iped].iStateBitset, BIT_AI_BLIP_STATE_HIDDEN)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_PED_IN_ANY_VEHICLE(tempPed)
				CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][AI] Unhiding AI blip as the ped is no longer in a vehicle.")
				CLEAR_BIT(biHostilePedBlip[iped].iStateBitset, BIT_AI_BLIP_STATE_HIDDEN)
			ELSE
				VEHICLE_INDEX VehicleID = GET_VEHICLE_PED_IS_IN(tempPed)
				IF NOT DOES_ENTITY_EXIST(VehicleID)
					CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][AI] Unhiding AI blip as the peds vehicle no longer exists.")
					CLEAR_BIT(biHostilePedBlip[iped].iStateBitset, BIT_AI_BLIP_STATE_HIDDEN)
				ELSE
					BLIP_INDEX BlipID = GET_BLIP_FROM_ENTITY(VehicleID)
					IF NOT DOES_BLIP_EXIST(BlipID)
						CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][AI] Unhiding AI blip as the peds vehicle is no longer blipped.")
						CLEAR_BIT(biHostilePedBlip[iped].iStateBitset, BIT_AI_BLIP_STATE_HIDDEN)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFourteen, ciPED_BSFourteen_RemovePedBlipWhenAggroed)
	AND IS_BIT_SET(MC_serverBD.iPedAggroedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))		
		IF IS_BIT_SET(biHostilePedBlip[iped].iStateBitset, BIT_AI_BLIP_STATE_HIDDEN)
			CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][AI] DISPLAY_PED_HUD_AND_BLIPS - Setting blip to hidden.")
			SET_BIT(biHostilePedBlip[iped].iStateBitset, BIT_AI_BLIP_STATE_HIDDEN)
		ENDIF
	ENDIF

	IF NOT DOES_PED_HAVE_DUMMY_BLIP(iped)
	AND ( NOT SHOULD_PED_HAVE_BLIP(iped,iBlipColour,bCustomBlip)
	OR SUPPRESS_SCRIPT_PED_BLIP_TILL_COMBAT(iped, tempPed, TRUE) )

		#IF IS_DEBUG_BUILD
			IF bOutputCopMissingBlipDebug
				BOOL bPedBlipOff = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_BlipOff)
				PRINTLN("[RCC MISSION] - DISPLAY_PED_HUD_AND_BLIPS - IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_BlipOff) = ", bPedBlipOff, ". Ped = ", iped)
				PRINTLN("[RCC MISSION] - DISPLAY_PED_HUD_AND_BLIPS - g_bDeactivateRestrictedAreas = ", g_bDeactivateRestrictedAreas, ". Ped = ", iped)
				PRINTLN("[RCC MISSION] - DISPLAY_PED_HUD_AND_BLIPS - MC_playerBD[iPartToUse].iWanted = ", MC_playerBD[iPartToUse].iWanted, ". Ped = ", iped)
			ENDIF
		#ENDIF
		
		INT iTemp
		BOOL bTemp
		
		
		
		
		IF NOT IS_BIT_SET(MC_serverBD.iPedInvolvedInMiniGameBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)) 
			
			IF (NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_BlipOff) OR SHOULD_PED_BE_BLIPPED_FOR_CURRENT_RULE(iped, iTemp, bTemp))	// Added override check for 2195776
				
				IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iTeam[MC_playerBD[iPartToUse].iteam] = ciPED_RELATION_SHIP_DISLIKE OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iTeam[MC_playerBD[iPartToUse].iteam] = ciPED_RELATION_SHIP_HATE)
					
					IF ((IS_PED_A_COP_MC(iped) AND MC_playerBD[iPartToUse].iWanted = 0 AND MC_playerBD[iPartToUse].iFakeWanted = 0)
					OR NOT IS_PED_A_COP_MC(iped))
					
						 //Or cop blips are off (is an objective for a team) but is not an objective for me. 
						 //OR (IS_OBJECTIVE_PED_FOR_ANY_TEAM(iped) AND NOT IS_OBJECTIVE_PED_FOR_TEAM(iped, MC_playerBD[iPartToUse].iteam)) )
						//They automatically get a blip if a cop and the player has a wanted/fake wanted level, so stop updating this one - Bug 1810579
						//AND NOT ( IS_BIT_SET(MC_serverBD.iNonObjectiveCopBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed)) AND ( (MC_playerBD[iPartToUse].iWanted > 0) OR (MC_playerBD[iPartToUse].iFakeWanted > 0) ) )
						
						//Fix for bug 1929440 - the ped AI blip is adding a heli blip while helis on a kill rule crash...
						BOOL bHeliBypass = FALSE
						
						IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle != -1)
						AND IS_PED_IN_ANY_HELI(tempPed)
							
							VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(tempPed)
							
							INT iVeh = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle
							
							IF (NOT IS_VEHICLE_DRIVEABLE(tempVeh))
							AND (NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]))
							AND (GET_ENTITY_MODEL(tempVeh) = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn)
							AND WAS_THIS_VEHICLE_ON_A_KILL_RULE(iVeh,MC_playerBD[iPartToUse].iteam)
								bHeliBypass = TRUE
							ENDIF
						ENDIF
						
						// has this ped explicitly been told not to blip?
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_DisableHelicopterBlip)
							bHeliBypass = TRUE
						ENDIF
						
						IF NOT bHeliBypass

							//Is this ped's team rule less than the current mission highest rule?
							//If so blip the ped constantly. Otherwise let AI code fade the blip in
							//an out based on combat.
							BOOL bForceOn = FALSE
							
							IF SHOULD_PED_BE_BLIPPED_FOR_CURRENT_RULE(iped, iTemp, bTemp)
							AND NOT SUPPRESS_SCRIPT_PED_BLIP_TILL_COMBAT(iped)
								bForceOn = TRUE
							ENDIF

							INT iTeam = MC_playerBD[iPartToUse].iteam
							INT iPedPriority = MC_serverBD_4.iPedPriority[iped][MC_playerBD[iPartToUse].iteam]
						
							TEXT_LABEL_23 tl23BlipName = GET_BLIP_LITERAL_STRING_NAME(iTeam, iPedPriority, iPedBlipCachedNamePriority[iped])

							BLIP_SPRITE thisBlipSprite

//							PRINTLN("[NF BUG] iped = ", iped)
//							PRINTLN("[NF BUG] SHOULD_PED_HAVE_BLIP(iped,iBlipColour,bCustomBlip) = ", SHOULD_PED_HAVE_BLIP(iped,iBlipColour,bCustomBlip))
//							PRINTLN("[NF BUG] bForceOn = ", bForceOn)
//							PRINTLN("[NF BUG] SHOULD_PED_BE_BLIPPED_FOR_CURRENT_RULE(iped, iTemp, bTemp) = ", SHOULD_PED_BE_BLIPPED_FOR_CURRENT_RULE(iped, iTemp, bTemp))
//							PRINTLN("[NF BUG] SUPPRESS_SCRIPT_PED_BLIP_TILL_COMBAT(iped) = ", SUPPRESS_SCRIPT_PED_BLIP_TILL_COMBAT(iped))
//							PRINTLN("[NF BUG] IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour, ciPED_BSFour_SolidBlip) = ", IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour, ciPED_BSFour_SolidBlip))
//							PRINTLN("[NF BUG] IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_AiBlipsOnly) = ", IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_AiBlipsOnly))
							
							IF IS_BIT_SET(biHostilePedBlip[iped].iStateBitset, BIT_AI_BLIP_STATE_HIDDEN)
							AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPed_BSEight_ForceBoatBlipSprite)
								#IF IS_DEBUG_BUILD
									IF (GET_FRAME_COUNT()%100) = 0
										CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][AI] Unhiding AI blip as the ped is having it's AI blip managed this frame.")
									ENDIF
								#ENDIF
								CLEAR_BIT(biHostilePedBlip[iped].iStateBitset, BIT_AI_BLIP_STATE_HIDDEN)
							ENDIF
							
//							#IF IS_DEBUG_BUILD
//								TEXT_LABEL_15 tlDebugName = "PlacedPed["
//								tlDebugName += iPed
//								tlDebugName += "]"
//							#ENDIF
							BOOL bHideBlipBecauseInvisible = IS_PED_INVISIBLE(iped) AND NOT GET_USINGSEETHROUGH()
							
							BOOL bAggressiveAIBlipCleanUp = FALSE
							
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFourteen, ciPED_BSFourteen_AIBlipCleanup)
							OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_AIBlipCleanupAllPeds)
								IF !SHOULD_PED_HAVE_BLIP(iPed, iTemp, bTemp)
									bAggressiveAIBlipCleanUp = TRUE
									PRINTLN("[RCC MISSION] - DISPLAY_PED_HUD_AND_BLIPS - CLEANING UP PED ", iPed, " AI BLIP. One of the options to do so is set on.")
								ENDIF
							ENDIF
							
							IF NOT bHideBlipBecauseInvisible
							AND NOT bAggressiveAIBlipCleanUp
								UPDATE_ENEMY_NET_PED_BLIP( 	MC_serverBD_1.sFMMC_SBD.niPed[iped],
															biHostilePedBlip[iped],
															TO_FLOAT( g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iped ].iBlipNoticeRange ), // fNoticableRadius
															DEFAULT,	// Professional, DEFAULT = FALSE
															bForceOn,	
															SHOULD_DISPLAY_CONE_ON_THIS_PED( iPed ), 
															tl23BlipName
															)
							ELSE
								CLEANUP_AI_PED_BLIP(biHostilePedBlip[iped])
							ENDIF
							//PRINTLN("[RCC MISSION] - biHostilePedBlip[iped].BlipID  blip does not exists ", iped)
								
							// deal with police jets - make sure they are red and rotate
							IF DOES_BLIP_EXIST(biHostilePedBlip[iped].VehicleBlipID)						
								thisBlipSprite = GET_BLIP_SPRITE(biHostilePedBlip[iped].VehicleBlipID)							
								IF (thisBlipSprite = RADAR_TRACE_POLICE_PLANE_MOVE)
									SET_BLIP_ROTATION(biHostilePedBlip[iped].VehicleBlipID, ROUND(GET_ENTITY_HEADING_FROM_EULERS(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped]))))
									SET_BLIP_COLOUR(biHostilePedBlip[iped].VehicleBlipID, BLIP_COLOUR_RED )
								ENDIF													
							ENDIF
								
							
							
						ELSE
							#IF IS_DEBUG_BUILD
								IF biHostilePedBlip[iped].PedID != NULL
									CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][AI] Ped ", iped, " AI blip cleaning up due to heli bypass setting.")
								ENDIF
							#ENDIF
							CLEANUP_AI_PED_BLIP(biHostilePedBlip[iped])
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							IF biHostilePedBlip[iped].PedID != NULL
								CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][AI] Ped ", iped, " AI blip cleaning up due to cop check.")
							ENDIF
						#ENDIF
						CLEANUP_AI_PED_BLIP(biHostilePedBlip[iped])
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						IF biHostilePedBlip[iped].PedID != NULL
							CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][AI] Ped ", iped, " AI blip cleaning up due to relationship not being dislike or hate.")
						ENDIF
					#ENDIF
					CLEANUP_AI_PED_BLIP(biHostilePedBlip[iped])
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					IF biHostilePedBlip[iped].PedID != NULL
						CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][AI] Ped ", iped, " AI blip cleaning up due to not being active on the current rule or BlipOff setting.")
					ENDIF
				#ENDIF
				CLEANUP_AI_PED_BLIP(biHostilePedBlip[iped])
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				IF biHostilePedBlip[iped].PedID != NULL
					CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][AI] Ped ", iped, " AI blip cleaning up due to ped being flagged as part of a minigame.")
				ENDIF
			#ENDIF
			CLEANUP_AI_PED_BLIP(biHostilePedBlip[iped])
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			IF biHostilePedBlip[iped].PedID != NULL
				CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][AI] Ped ", iped, " AI blip cleaning up due to should ped have blip or blip suppression check.")
			ENDIF
		#ENDIF
		CLEANUP_AI_PED_BLIP(biHostilePedBlip[iped])
	ENDIF
	
	//Act on the decision to hide or not.
	IF IS_BIT_SET(biHostilePedBlip[iped].iStateBitset, BIT_AI_BLIP_STATE_HIDDEN)
		#IF IS_DEBUG_BUILD
			IF biHostilePedBlip[iped].PedID != NULL
				CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][AI] Ped ", iped, " AI blip cleaning up due to hide setting being flagged.")
			ENDIF
		#ENDIF
		CLEANUP_AI_PED_BLIP(biHostilePedBlip[iped])
		IF IS_PED_A_COP_MC(iped)
			REMOVE_COP_BLIP_FROM_PED(tempPed)
		ENDIF
	ENDIF
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
		IF DOES_BLIP_EXIST(biCaptureRangeBlip[iPed])
			SET_BLIP_COORDS(biCaptureRangeBlip[iPed], GET_ENTITY_COORDS(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPed]), FALSE))
		ENDIF
	ENDIF
	
	INT iMultiRuleNo 
	
	//3944005
	MAINTAIN_RUSSIAN_CLOAK_VISIBILITY(iped)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetThirteen, ciPED_BSThirteen_SetAsCopDecoy)
		PROCESS_COP_DECOY_PED_BLIP_AND_HUD(iPed, tempPed)
	ENDIF
	
	//Ped priority for my team is <= my current highest priority:
	IF (MC_serverBD_4.iPedPriority[iped][MC_playerBD[iPartToUse].iteam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
		INT iHackingPart = -1
		BOOL bInHackingVehicle = FALSE
		INT iTempHackPercentage = 0
		VEHICLE_INDEX viCurrentVeh
		PED_INDEX piPedInVeh
		INT iPartsChecked = 0
		IF MC_playerBD[iLocalPart].iPedNear = iped 
			IF MC_serverBD_4.iPedRule[iped][MC_playerBD[iLocalPart].iteam] != FMMC_OBJECTIVE_LOGIC_PHOTO
				RESET_PHOTO_DATA()
				MC_playerBD[iLocalPart].iPedNear = -1
			ENDIF
		ENDIF
		
		SWITCH  MC_serverBD_4.iPedRule[iped][MC_playerBD[iPartToUse].iteam] 
			
			//AW - Charm Ped Logic
			CASE FMMC_OBJECTIVE_LOGIC_CHARM
				IF MC_playerBD[iPartToUse].iPedCharmed = -1
				
						RUN_CHARM_PED_LOGIC(iped, tempPed)
				ENDIF
			BREAK
			
			//AW - Crowd Control Ped Logic - now mostly moved to ped brain/body
			CASE FMMC_OBJECTIVE_LOGIC_CROWD_CONTROL
			
				RUN_CC_PED_HUD(iped, tempPed)
				
			BREAK
			
			CASE FMMC_OBJECTIVE_LOGIC_PHOTO
			
				IF MC_playerBD[iPartToUse].iPedNear = iped OR MC_playerBD[iPartToUse].iPedNear = -1
					IF IS_PED_STATE_OK_FOR_PHOTO(iped)
						
						IF VDIST2(GET_ENTITY_COORDS(tempPed,FALSE),GET_ENTITY_COORDS(LocalPlayerPed,FALSE)) < GET_PED_PHOTO_RANGE_SQUARED(iped)
							
							IF iSpectatorTarget = -1
								MC_playerBD[iPartToUse].iPedNear = iped
							ENDIF
							
							vPedCoords = GET_PED_BONE_COORDS(tempPed, BONETAG_HEAD, << 0.0, 0.0, 0.0 >>)
							
							IF NOT DOES_PHOTO_TRACKED_POINT_EXIST(iPhotoTrackedPoint)
								//create new tracked point for current target coords
								CREATE_PHOTO_TRACKED_POINT_FOR_COORD(iPhotoTrackedPoint, vPedCoords, PICK_FLOAT(IS_PED_IN_ANY_VEHICLE(tempPed), 1.0, 0.5))
								vLocalPhotoCoords = vPedCoords
							ELSE
								IF NOT ARE_VECTORS_ALMOST_EQUAL(vPedCoords, vLocalPhotoCoords, 0.05) //If the ped's moved
									vLocalPhotoCoords = vPedCoords
									SET_TRACKED_POINT_INFO(iPhotoTrackedPoint,vLocalPhotoCoords,PICK_FLOAT(IS_PED_IN_ANY_VEHICLE(tempPed), 1.0, 0.5))
								ENDIF
							ENDIF
							
							IF DOES_PHOTO_TRACKED_POINT_EXIST(iPhotoTrackedPoint) AND IS_PHOTO_TRACKED_POINT_VISIBLE(iPhotoTrackedPoint)
								IF IS_POINT_IN_CELLPHONE_CAMERA_VIEW(vPedCoords, 0.2)
									IF HAS_CELLPHONE_CAM_JUST_TAKEN_PIC()
										RESET_PHOTO_DATA()
										//CLEANUP_PHOTO_TRACKED_POINT()
										MC_playerBD[iPartToUse].iPedNear 	= -1
										MC_playerBD[iPartToUse].iPedPhoto 	= iPed
										
										IF SHOULD_PUT_AWAY_PHONE_AFTER_PHOTO(MC_serverBD_4.iPedPriority[iped][MC_playerBD[iPartToUse].iTeam], ci_TARGET_PED)
											SET_BIT(iLocalBoolCheck3,LBOOL3_PHOTOTAKEN)
											REINIT_NET_TIMER(tdPhotoTakenTimer)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
						ELSE
							IF(MC_playerBD[iPartToUse].iPedNear = iPed)
								RESET_PHOTO_DATA()
								MC_playerBD[iPartToUse].iPedNear = -1
							ENDIF
						ENDIF
					ELSE // Else this ped is not okay for a photo
						IF(MC_playerBD[iPartToUse].iPedNear = iPed)
							RESET_PHOTO_DATA()
							MC_playerBD[iPartToUse].iPedNear = -1
						ENDIF
					ENDIF
				
				ENDIF
			BREAK	
			
			CASE FMMC_OBJECTIVE_LOGIC_GO_TO
			
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iGotoRange != 0
					
					iMultiRuleNo = GET_ENTITY_MULTIRULE_NUMBER(iped,MC_playerBD[iPartToUse].iteam,ci_TARGET_PED)
					
					IF (iMultiRuleNo != -1)
					AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour, ciPED_BSFour_IgnoreProximityPrimaryRule + iMultiRuleNo)
						
						bHasGoToRange = TRUE
						
						IF (MC_playerBD[iPartToUse].iPedNear = -1)
						AND VDIST2(GET_ENTITY_COORDS(tempPed,FALSE),GET_ENTITY_COORDS(LocalPlayerPed,FALSE)) <= (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iGotoRange * g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iGotoRange)
							IF iSpectatorTarget = -1
								MC_playerBD[iPartToUse].iPedNear = iPed
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				

			BREAK		
			
			CASE FMMC_OBJECTIVE_LOGIC_CAPTURE
			
			
				IF IS_BIT_SET(MC_playerBD[iPartToUse].iPedFollowingBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				OR IS_ANY_PED_BEING_CAPTURED()
					IF HAS_NET_TIMER_STARTED(MC_serverBD_1.tdControlPedTimer[iped])
						IF bPlayerToUseOK
							IF MC_serverBD_4.iPedPriority[iped][MC_playerBD[iPartToUse].iteam] > -1
							AND MC_serverBD_4.iPedPriority[iped][MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
								IF NOT IS_SPECTATOR_HUD_HIDDEN()
								AND NOT IS_SPECTATOR_SHOWING_NEWS_HUD()
								AND NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_OBJECTIVE_BLOCKER)	
								AND ((NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitset[MC_serverBD_4.iPedPriority[iped][MC_playerBD[iPartToUse].iteam]], ciBS_RULE_HIDE_HUD)
								AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPED_BSEight_DisableCaptureBar))
								OR IS_FAKE_MULTIPLAYER_MODE_SET())
									DRAW_GENERIC_METER(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.tdControlPedTimer[iped]) + MC_serverBD_4.iCaptureTimestamp,  MC_serverBD.iPedTakeoverTime[MC_playerBD[iPartToUse].iteam][MC_serverBD_4.iPedPriority[iped][MC_playerBD[iPartToUse].iteam]],"CON_TIME",HUD_COLOUR_GREEN,-1)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING)
					IF HAS_NET_TIMER_STARTED(MC_serverBD_1.tdControlPedTimer[iped])
						IF IS_BIT_SET(MC_playerBD[iPartToUse].iPedFollowingBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
							//We are the hacker
							iHackingPart = iPartToUse
							bInHackingVehicle = TRUE
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciSECUROSERV_DONT_FORCE_ON_SCREEN)
								DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_PHONE)
								DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CELLPHONE_DOWN)
								DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CELLPHONE_CANCEL)
							ENDIF
						ELSE
							IF IS_BIT_SET(iHackingPedInRangeBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
							//In range of the ped
								FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
									IF MC_playerBD[i].iTeam = MC_playerBD[iPartToUse].iTeam
									AND i != iPartToUse
										iPartsChecked++
										IF IS_BIT_SET(MC_playerBD[i].iPedFollowingBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
											IF IS_PED_IN_ANY_VEHICLE(PlayerPedToUse)
												viCurrentVeh = GET_VEHICLE_PED_IS_USING(PlayerPedToUse)
												piPedInVeh = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i)))
												IF IS_PED_IN_VEHICLE(piPedInVeh, viCurrentVeh)
													PRINTLN("[JS][SECUROHACK] Hacker is in our vehicle, ", iHackingPart)
													bInHackingVehicle = TRUE
												ENDIF
											ENDIF
											iHackingPart = i
											PRINTLN("[JS][SECUROHACK] Hacker is, ", iHackingPart)
											BREAKLOOP
										ENDIF
									ENDIF
									IF iPartsChecked >= MC_serverBD.iNumberOfPlayingPlayers[MC_playerBD[iPartToUse].iteam] - 1
										BREAKLOOP
									ENDIF
								ENDFOR
							ENDIF
						ENDIF
						IF iHackingPart != -1
							IF MC_serverBD_4.iPedPriority[iped][MC_playerBD[iHackingPart].iteam] > -1
							AND MC_serverBD_4.iPedPriority[iped][MC_playerBD[iHackingPart].iteam] < FMMC_MAX_RULES
								IF bInHackingVehicle
									IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH("appSecuroHack")) > 0
									AND IS_PHONE_ONSCREEN(TRUE)
										IF NOT IS_BIT_SET(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
											IF iHackingLoopSoundID = -1
												iHackingLoopSoundID = GET_SOUND_ID()
											ENDIF
											PLAY_SOUND_FRONTEND(iHackingLoopSoundID, "Hack_Loop", "DLC_IE_SVM_Voltic2_Hacking_Sounds")
										SET_BIT(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
										ENDIF
										SET_VARIABLE_ON_SOUND(iHackingLoopSoundID, "percentageComplete", TO_FLOAT(iHackingLoopSoundID) / 100)
									ENDIF
									iTempHackPercentage = ROUND(TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.tdControlPedTimer[iped])) / MC_serverBD.iPedTakeoverTime[MC_playerBD[iHackingPart].iteam][MC_serverBD_4.iPedPriority[iped][MC_playerBD[iHackingPart].iteam]] * 100)
									IF iTempHackPercentage >= iHackPercentage
									OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPED_BSEight_ResetCaptureOnOutOfRange)
										iHackPercentage = iTempHackPercentage
									ELSE
										PRINTLN("[JS][SECUROHACK] iTempHackPercentage is less that we think it should be ",iTempHackPercentage," vs ",iHackPercentage," keeping higher value until we catch up")
									ENDIF
									
									IF IS_THIS_ROCKSTAR_MISSION_WVM_HALFTRACK(g_FMMC_STRUCT.iRootContentIDHash)
									AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH("appSecuroHack")) = 0
										DRAW_GENERIC_METER(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.tdControlPedTimer[iped]), MC_serverBD.iPedTakeoverTime[MC_playerBD[iHackingPart].iteam][MC_serverBD_4.iPedPriority[iped][MC_playerBD[iHackingPart].iteam]], "HACK_PROG", DEFAULT, DEFAULT, HUDORDER_TOP, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT)
									ENDIF
									
									IF iHackPercentage < 100
										IF iHackStage != ciSECUROSERV_HACK_STAGE_HACKING
										AND iHackStage != ciSECUROSERV_HACK_STAGE_LOSING_SIGNAL
										
											IF NOT IS_HACK_START_SOUND_BLOCKED()
												IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
													PLAY_SOUND_FRONTEND(-1, "Hack_Start", "dlc_xm_deluxos_hacking_Hacking_Sounds")
												ELSE
													PLAY_SOUND_FRONTEND(-1, "Hack_Start", "DLC_IE_SVM_Voltic2_Hacking_Sounds")
												ENDIF
											ENDIF
											
											iHackStage = ciSECUROSERV_HACK_STAGE_HACKING
										ENDIF
									ENDIF
								ELSE
									iHackStage = ciSECUROSERV_HACK_STAGE_HACK_IN_PROGRESS
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			
			BREAK

		ENDSWITCH
		
		
		IF IS_BIT_SET(MC_playerBD[iPartToUse].iPedFollowingBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			IF NOT IS_PED_INJURED(tempPed)
				IF IS_PED_GROUP_MEMBER(tempPed, giPlayerGroup)
				AND NETWORK_HAS_CONTROL_OF_ENTITY(tempPed)
					
					SET_PED_RESET_FLAG(tempPed, PRF_IfLeaderStopsSeekCover,TRUE)
					
					IF bLocalPlayerPedOk
						
						IF NOT IS_BIT_SET(iPedMovementClipsetOverrideBitset2[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
						AND NOT IS_BIT_SET(iPedMovementClipsetOverrideBitset1[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
							// (1909144) Ped action mode
							IF IS_PLAYER_BATTLE_AWARE(LocalPlayer)
								IF NOT IS_BIT_SET(iLocalBoolCheck5, LBOOL5_PED_ACTION_MODE)
									IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour, ciPED_BSFour_AlwaysUseActionModeWhenInGroup)
										SET_PED_USING_ACTION_MODE(tempPed, TRUE, -1, "DEFAULT_ACTION")
										PRINTLN("[CS_ACT] SET_PED_USING_ACTION_MODE, PED, TRUE) ")
									ENDIF
									SET_BIT(iLocalBoolCheck5, LBOOL5_PED_ACTION_MODE)
								ENDIF
							ELSE
								IF IS_BIT_SET(iLocalBoolCheck5, LBOOL5_PED_ACTION_MODE)
									IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour, ciPED_BSFour_AlwaysUseActionModeWhenInGroup)
										SET_PED_USING_ACTION_MODE(tempPed, FALSE, -1, "DEFAULT_ACTION")	
										PRINTLN("[CS_ACT] SET_PED_USING_ACTION_MODE, PED, FALSE) ")
									ENDIF
									CLEAR_BIT(iLocalBoolCheck5, LBOOL5_PED_ACTION_MODE)
								ENDIF
							ENDIF
						ENDIF
						
						IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
							IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_AssActionWalk)
							OR IS_PED_IN_COMBAT(LocalPlayerPed)
								VEHICLE_SEAT tempSeat
								IF GET_FREE_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(LocalPlayerPed),tempSeat)
									SET_PED_MIN_MOVE_BLEND_RATIO(tempPed,PEDMOVEBLENDRATIO_RUN)
								ENDIF
							ENDIF
						ELSE
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset,ciPED_BS_GroupAlongSide)
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_AssActionWalk)
									SET_PED_MAX_MOVE_BLEND_RATIO(tempPed,PEDMOVEBLENDRATIO_WALK)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF iSpectatorTarget = -1	
				IF MC_playerBD[iPartToUse].iPedNear = -1
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFollow = ciPED_FOLLOW_ON
						MC_playerBD[iPartToUse].iPedNear = iped
					ENDIF
				ENDIF
				iMyPedCarryCount++
			ENDIF

			
		ELSE
			IF iSpectatorTarget = -1
			AND MC_playerBD[iPartToUse].iPedNear = -1
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFollow = ciPED_FOLLOW_OFF
				AND NOT bHasGoToRange
					IF VDIST2(GET_ENTITY_COORDS(tempPed,FALSE),GET_ENTITY_COORDS(LocalPlayerPed,FALSE)) < (4 * 4)
						MC_playerBD[iPartToUse].iPedNear = iped
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

PROC PROCESS_PEDS_EVERY_FRAME()
	IF MC_serverBD.iNumPedCreated > 0
	AND MC_serverBD.iNumPedCreated <= FMMC_MAX_PEDS
		INT i = 0
		FOR i = 0 TO (MC_serverBD.iNumPedCreated-1)
			
			NETWORK_INDEX netPed
			PED_INDEX tempPed
			
			netPed = MC_serverBD_1.sFMMC_SBD.niPed[i]
			
			IF NETWORK_DOES_NETWORK_ID_EXIST(netPed)
				tempPed = NET_TO_PED(netPed)
			ENDIF
						
			DISPLAY_PED_HUD_AND_BLIPS(i)
			
			IF bIsLocalPlayerHost
				MANAGE_SERVER_CAPTURE_BY_DISTANCE_EVERY_FRAME(i)	
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iRespawnOnRuleChangeBS > 0
					INT iTeam
					FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
						IF (MC_serverBD_4.iCurrentHighestPriority[iTeam] > 0
						AND IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam))
						OR IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_RETRY_CURRENT_RULE_TEAM_0 + iTeam)
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iRespawnOnRuleChangeBS, iTeam)
								SET_BIT(MC_serverBD.iPedCleanup_NeedOwnershipBS[GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i))
								SET_BIT(MC_serverBD.iPedFirstSpawnBitset[GET_LONG_BITSET_INDEX(i)],GET_LONG_BITSET_BIT(i))
								CLEAR_BIT(MC_serverBD.iPedAggroedBitset[GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i))
								CLEAR_BIT(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i))
								
								MC_serverBD_2.iPedState[i] = ciTASK_CHOOSE_NEW_TASK
								MC_serverBD_2.iOldPedState[i] = ciTASK_CHOOSE_NEW_TASK
								IF MC_serverBD_2.iCurrentPedRespawnLives[i] < 1
									MC_serverBD_2.iCurrentPedRespawnLives[i] = 1
								ENDIF
								PRINTLN( "[JS] Cleaning up ped to respawn on rule change ", i )
								BREAKLOOP
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
			IF (MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam] > 0
			AND IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iLocalPart].iTeam))
			OR IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_RETRY_CURRENT_RULE_TEAM_0 + MC_playerBD[iLocalPart].iTeam)
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iRespawnOnRuleChangeBS, MC_playerBD[iLocalPart].iTeam)
					IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[i])
						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[i])
							CLEAR_PED_TASKS_IMMEDIATELY( NET_TO_PED( MC_serverBD_1.sFMMC_SBD.niPed[ i ] ))
						ENDIF
					ENDIF
					CLEAR_BIT(sRetaskDirtyFlagData.iPedAggroedBitsetCopy[GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i))
					CLEAR_BIT(sRetaskDirtyFlagData.iPedSpookedBitsetCopy[GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i))
					CLEAR_BIT(sRetaskDirtyFlagData.iDirtyFlagPedNeedsRetask[GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i))
					CLEAR_BIT(sRetaskDirtyFlagData.iDirtyFlagPedRetaskHandshake[GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i))
					CLEAR_BIT(iPedLocalSpookedBitset[GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i))
					CLEAR_BIT(iPedLocalAggroedBitset[GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i))
				ENDIF
			ENDIF
			
			IF IS_BIT_SET( iPerformHydraulicsAsPriorityBitSet[GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i) )
				IF IS_PED_IN_ANY_VEHICLE( tempPed )
					IF IS_VEHICLE_DRIVEABLE( GET_VEHICLE_PED_IS_IN( tempPed ) )
						IF IS_VEHICLE_A_SUPERMOD_MODEL( GET_ENTITY_MODEL( GET_VEHICLE_PED_IS_IN( tempPed ) ), SUPERMOD_FLAG_HAS_HYDRAULICS)
							#IF IS_DEBUG_BUILD
								IF (GET_FRAME_COUNT() % 5) = 0
									PRINTLN( "[JJT] PROCESS_HYDRAULIC_DANCING on ped: ", i )
								ENDIF
							#ENDIF
							
							iHydraulicDanceTimer++
							iHydraulicDanceDowntimeTimer++
							PROCESS_HYDRAULIC_DANCING( tempPed )
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			CLEAR_BIT(sMissionPedsLocalVars[i].iPedBS, ci_MissionPedBS_ForcedBlip)
			
			IF IS_ENTITY_ALIVE(tempPed)
			AND NETWORK_HAS_CONTROL_OF_ENTITY(tempPed)
				IF IS_ENTITY_PLAYING_ANIM(tempPed, "move_aim_strafe_crouch_2h", "idle") 
					SET_PED_RESET_FLAG(tempPed, PRF_InstantBlendToAim, TRUE)
					SET_PED_DUCKING(tempPed, TRUE)
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck32, LBOOL32_PLAYER_REL_GROUP_CACHED_FOR_DRONE)
		relMyFmGroup = GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID())
		SET_BIT(iLocalBoolCheck32, LBOOL32_PLAYER_REL_GROUP_CACHED_FOR_DRONE)
	ENDIF
	
	RESET_SPOOK_AT_COORD_IN_RADIUS()
	#IF IS_DEBUG_BUILD		
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_PEDS_EVERY_FRAME")
	#ENDIF
	#ENDIF
ENDPROC
