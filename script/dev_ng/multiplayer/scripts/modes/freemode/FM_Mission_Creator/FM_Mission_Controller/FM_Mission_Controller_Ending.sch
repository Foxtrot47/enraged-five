
USING "FM_Mission_Controller_USING.sch"
USING "FM_Mission_Controller_Utility.sch"
USING "FM_Mission_Controller_Entities.sch"
USING "FM_Mission_Controller_ObjectiveCompletion.sch"
USING "FM_Mission_Controller_SpecialCase.sch"
USING "FM_Mission_Controller_Rules.sch"
USING "FM_Mission_Controller_Doors.sch"
USING "FM_Mission_Controller_Cutscene.sch"
USING "FM_Mission_Controller_Debug.sch"
USING "FM_Mission_Controller_Locations.sch"
USING "FM_Mission_Controller_Vehicles.sch"
USING "FM_Mission_Controller_Peds_Brain.sch"
USING "FM_Mission_Controller_Peds_Body.sch"
USING "FM_Mission_Controller_Peds.sch"
USING "FM_Mission_Controller_GangChase.sch"
USING "FM_Mission_Controller_Minigame.sch"
USING "FM_Mission_Controller_Objects.sch"
USING "FM_Mission_Controller_Props.sch"
USING "FM_Mission_Controller_Audio.sch"
USING "FM_Mission_Controller_HUD.sch"
USING "FM_Mission_Controller_Players.sch"
USING "FM_Mission_Controller_Zones.sch"
USING "FM_Mission_Controller_Bounds.sch"
USING "FM_Mission_Controller_Events.sch"



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: POPULATE RESULTS !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************

// Note(Owain):
//
// This section is mostly to do with PROCESS_MISSION_END_STAGE and related features
// like leaderboards, stats, fail reasons,a few celebration screen things, and suchlike

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: POPULATE RESULTS !
//
//************************************************************************************************************************************************************



///purpose: calculates the total number of hits. Using many stats
func int calculate_hits()

	int total_number_of_hits
	
	total_number_of_hits = (GET_MP_INT_CHARACTER_STAT(MP_STAT_HITS_PEDS_VEHICLES) 
							+ GET_MP_INT_CHARACTER_STAT(MP_STAT_INSURGENT_TURR_HITS)
							+ GET_MP_INT_CHARACTER_STAT(MP_STAT_VALKYRIE_CANNON_KILLS)
							+ GET_MP_INT_CHARACTER_STAT(MP_STAT_VALKYRIE_TURR_HITS)) 

	return total_number_of_hits

endfunc  

///purpose: calculates the total number of shots. Uses multiple stats. We can not change mp_stat_shots to include 
///    		all weapons e.g. insurgent turret gun as this will have an effect on social club stat accuracy.
///    	    MP_STAT_SHOTS only includes player hand held weapons. Any news vehicle weapons stst enums will have to
///    		be added here. 
///    		check : hydra, technikal vehicle stats
func int calculate_shots()

	int total_number_of_shots
	
	total_number_of_shots = (GET_MP_INT_CHARACTER_STAT(MP_STAT_SHOTS) 
							+ GET_MP_INT_CHARACTER_STAT(MP_STAT_INSURGENT_TURR_SHOTS)
							+ GET_MP_INT_CHARACTER_STAT(MP_STAT_VALKYRIE_CANNON_SHOTS)
							+ GET_MP_INT_CHARACTER_STAT(MP_STAT_VALKYRIE_TURR_SHOTS))

	return total_number_of_shots

endfunc  

///purpose: calculates the players accuracy. 
PROC CALCULATE_PLAYER_ACCURACY()

	INT iEndHits = calculate_hits()
	INT iEndShots = calculate_shots()
	
	PRINTLN("[LK MISSION] iEndHits: ", iEndHits)
	PRINTLN("[LK MISSION] iEndShots: ", iEndShots)
	
	IF (iEndShots - MC_playerBD[iPartToUse].iStartShots) >= 1
		FLOAT frecentaccuracy = (TO_FLOAT(iEndHits - MC_playerBD[iPartToUse].iStartHits) / TO_FLOAT(iEndShots - MC_playerBD[iPartToUse].iStartShots))
		
		MC_playerBD[iPartToUse].iEndAccuracy = ROUND(frecentaccuracy * 100.0)
		
		PRINTLN("[LK MISSION] accuracy: ", MC_playerBD[iPartToUse].iEndAccuracy)
	ENDIF
	
ENDPROC

FUNC INT GET_SCORE_CONTRIBUTION_TIME_START_VALUE()
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		RETURN g_sMPTunables.IH2_medal_time_start_value
	ENDIF
	IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
		RETURN g_sMPTunables.iCH_medal_time_start_value
	ENDIF
	RETURN 5000
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_TIME_MULTIPLIER()
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		RETURN g_sMPTunables.IH2_medal_time_multiplier 
	ENDIF
	IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
		RETURN g_sMPTunables.iCH_medal_time_multiplier
	ENDIF
	RETURN 2
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_KILLS_MULTIPLIER()
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		RETURN g_sMPTunables.IH2_medal_kills_multiplier
	ENDIF
	IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
		RETURN g_sMPTunables.iCH_medal_kills_multiplier
	ENDIF
	RETURN 100
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_KILLS_CAP()
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		RETURN g_sMPTunables.IH2_medal_max_kills
	ENDIF
	IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
		RETURN g_sMPTunables.iCH_medal_max_kills
	ENDIF
	RETURN 100
ENDFUNC

FUNC FLOAT GET_SCORE_CONTRIBUTION_AMBIENT_COP_KILL_MULTIPLIER()
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		RETURN g_sMPTunables.fH2_medal_ambient_cop_kills_multiplier
	ENDIF
	IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
		RETURN g_sMPTunables.fCH_medal_ambient_cop_kills_multiplier
	ENDIF
	RETURN 1.0
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_AMBIENT_COP_KILL_CAP()
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		RETURN g_sMPTunables.IH2_medal_max_ambient_cop_kills
	ENDIF
	IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
		RETURN g_sMPTunables.ICH_medal_max_ambient_cop_kills
	ENDIF
	RETURN HIGHEST_INT
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_HEADSHOT_MULTIPLIER()
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		RETURN g_sMPTunables.IH2_medal_head_shot_multiplier
	ENDIF
	IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
		RETURN g_sMPTunables.iCH_medal_head_shot_multiplier
	ENDIF
	RETURN 200
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_HEADSHOT_CAP()
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		RETURN g_sMPTunables.IH2_medal_max_headshots
	ENDIF
	IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
		RETURN g_sMPTunables.iCH_medal_max_headshots
	ENDIF
	RETURN 25
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_SAFE_DRIVE_CAP()
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		RETURN g_sMPTunables.IH2_medal_safe_drive_multiplier
	ENDIF
	RETURN 2000
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_SAFE_DRIVE_MULTIPLIER()
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		RETURN g_sMPTunables.IH2_medal_safe_drive_time_multiplier
	ENDIF
	RETURN 1
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_ACCURACY_MULTIPLIER()
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		RETURN g_sMPTunables.IH2_medal_accuracy_multiplier
	ENDIF
	IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
		RETURN g_sMPTunables.iCH_medal_accuracy_multiplier
	ENDIF
	RETURN 5000
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_CIV_KILLS_START()
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		RETURN g_sMPTunables.IH2_medal_civillian_kills_start_value
	ENDIF
	IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
		RETURN g_sMPTunables.iCH_medal_civillian_kills_start_value
	ENDIF
	RETURN 2000
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_CIV_KILLS_MULTIPLIER()
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		RETURN g_sMPTunables.IH2_medal_civillian_kills_multiplier
	ENDIF
	IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
		RETURN g_sMPTunables.iCH_medal_civillian_kills_multiplier
	ENDIF
	RETURN 100
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_CIV_KILLS_CAP()
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		RETURN g_sMPTunables.IH2_medal_max_civillian_kills
	ENDIF
	IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
		RETURN g_sMPTunables.iCH_medal_max_civillian_kills
	ENDIF
	RETURN HIGHEST_INT
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_HEALTH_START()
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		RETURN g_sMPTunables.IH2_medal_health_score_start_value
	ENDIF
	IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
		RETURN g_sMPTunables.iCH_medal_health_score_start_value
	ENDIF
	RETURN 3000
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_HEALTH_MULTIPLIER_0()
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		RETURN g_sMPTunables.IH2_medal_health_score_multiplier_0 
	ENDIF
	IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
		RETURN g_sMPTunables.iCH_medal_health_score_multiplier_0
	ENDIF
	RETURN 1000
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_HEALTH_MULTIPLIER_1()
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		RETURN g_sMPTunables.IH2_medal_health_score_multiplier_1
	ENDIF
	IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
		RETURN g_sMPTunables.iCH_medal_health_score_multiplier_1
	ENDIF
	RETURN 5
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_HACK_SCORE_START_VALUE()
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		RETURN g_sMPTunables.IH2_medal_hack_score_start_value
	ENDIF
	IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
		RETURN g_sMPTunables.iCH_medal_hack_score_start_value
	ENDIF
	RETURN 500
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_HACK_SCORE_MULTIPLIER()
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		RETURN g_sMPTunables.IH2_medal_hack_score_multiplier
	ENDIF
	IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
		RETURN g_sMPTunables.iCH_medal_hack_score_multiplier
	ENDIF
	RETURN 100
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_HACK_SCORE_CAP()
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		RETURN g_sMPTunables.IH2_medal_max_hacks
	ENDIF
	IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
		RETURN g_sMPTunables.iCH_medal_max_hacks
	ENDIF
	RETURN 5
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_WANTED_LEVEL_MAX()
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		RETURN g_sMPTunables.IH2_medal_max_wanted_level
	ENDIF
	IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
		RETURN g_sMPTunables.iCH_medal_max_wanted_level
	ENDIF
	RETURN 10
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_WANTED_LEVEL_MULTIPLIER()
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		RETURN g_sMPTunables.IH2_medal_wanted_level_multiplier
	ENDIF
	IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
		RETURN g_sMPTunables.iCH_medal_wanted_level_multiplier
	ENDIF
	RETURN 500
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_WANTED_LEVEL_TIME_MULTIPLIER()
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		RETURN g_sMPTunables.IH2_medal_wanted_level_time_multiplier
	ENDIF
	IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
		RETURN g_sMPTunables.iCH_medal_wanted_level_time_multiplier
	ENDIF
	RETURN 1
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_PLAYER_SCORE_MULTIPLIER()
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		RETURN g_sMPTunables.IH2_medal_objective_multiplier
	ENDIF
	IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
		RETURN g_sMPTunables.iCH_medal_objective_multiplier
	ENDIF
	RETURN 2000
ENDFUNC

FUNC INT GET_SCORE_CONTRIBUTION_PLAYER_SCORE_CAP()
	IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
		RETURN g_sMPTunables.iCH_medal_objective_cap
	ENDIF
	RETURN 10000
ENDFUNC

FUNC INT GET_MISSION_COMPLETION_HIGH_SCORE()

	INT iHighScore
	INT ibonus

	IF MC_Playerbd[iPartToUse].iPlayerScore > 0
		INT iPlayerScore = MC_Playerbd[iPartToUse].iPlayerScore
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS, ciFMMC_USE_SCORE_OFF_RULE_MINIGAMES)
			iPlayerScore += MC_playerBD_1[iPartToUse].iOffRuleMinigameScoreForMedal
			PRINTLN("[RCC MISSION] HIGH SCORE - adding off rule minigame points (", MC_playerBD_1[iLocalPart].iOffRuleMinigameScoreForMedal,") to playerscore. iPlayerScore now: ", iPlayerScore)
		ENDIF
		
		ibonus = (GET_SCORE_CONTRIBUTION_PLAYER_SCORE_MULTIPLIER() * iPlayerScore)
		IF ibonus > 0
			iBonus = CLAMP_INT(iBonus, 0, GET_SCORE_CONTRIBUTION_PLAYER_SCORE_CAP())
			iHighScore = iHighScore + ibonus
			PRINTLN("[RCC MISSION] HIGH SCORE - adding points bonus of: ",ibonus)
			PRINTLN("[RCC MISSION] HIGH SCORE - Total: ",iHighScore)
		ENDIF
	ENDIF

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS,ciFMMC_IGNORE_SCORE_TIME)
		FLOAT ftimesecs = TO_FLOAT(MC_Playerbd[iPartToUse].iMissionEndTime/1000)
		INT itimesecs = ROUND(ftimesecs)
		ibonus = (GET_SCORE_CONTRIBUTION_TIME_START_VALUE() - (itimesecs * GET_SCORE_CONTRIBUTION_TIME_MULTIPLIER()))
		IF ibonus > 0
			iHighScore = iHighScore + ibonus
			PRINTLN("[RCC MISSION] HIGH SCORE - adding time bonus of: ",ibonus)
			PRINTLN("[RCC MISSION] HIGH SCORE - Total: ",iHighScore)
		ENDIF
	ENDIF

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS,ciFMMC_IGNORE_SCORE_ENEMIES)
		INT ikills = (MC_Playerbd[iPartToUse].iNumPlayerKills + MC_Playerbd[iPartToUse].iNumPedKills + FLOOR(TO_FLOAT(CLAMP_INT(MC_playerBD[iPartToUse].iAmbientCopsKilled, 0, GET_SCORE_CONTRIBUTION_AMBIENT_COP_KILL_CAP())) * GET_SCORE_CONTRIBUTION_AMBIENT_COP_KILL_MULTIPLIER()))
		ibonus = (ikills * GET_SCORE_CONTRIBUTION_KILLS_MULTIPLIER())
		IF ibonus > 0
			iBonus = CLAMP_INT(iBonus, 0, (GET_SCORE_CONTRIBUTION_KILLS_MULTIPLIER() * GET_SCORE_CONTRIBUTION_KILLS_CAP()))
			iHighScore = iHighScore + ibonus
			PRINTLN("[RCC MISSION] HIGH SCORE - adding kill bonus of: ",ibonus)
			PRINTLN("[RCC MISSION] HIGH SCORE - Total: ",iHighScore)
		ENDIF
	ENDIF

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS,ciFMMC_IGNORE_SCORE_HEADSHOTS)
		INT iheadshots = CLAMP_INT(MC_Playerbd[iPartToUse].iNumHeadshots, 0, GET_SCORE_CONTRIBUTION_HEADSHOT_CAP())
		ibonus = (iheadshots * GET_SCORE_CONTRIBUTION_HEADSHOT_MULTIPLIER())
		IF ibonus > 0
			iHighScore = iHighScore + ibonus
			PRINTLN("[RCC MISSION] HIGH SCORE - adding headshot bonus of: ",ibonus)
			PRINTLN("[RCC MISSION] HIGH SCORE - Total: ",iHighScore)
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS,ciFMMC_IGNORE_SCORE_STEALTH_KILLS)
		ibonus = ((GET_MP_INT_CHARACTER_STAT(MP_STAT_KILLS_STEALTH)-istartStealthKills)* 500)
		IF ibonus > 0
			iBonus = CLAMP_INT(iBonus, 0, 5000)
			iHighScore = iHighScore + ibonus
			PRINTLN("[RCC MISSION] HIGH SCORE - adding stealth kill bonus of: ",ibonus)
			PRINTLN("[RCC MISSION] HIGH SCORE - Total: ",iHighScore)
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS,ciFMMC_IGNORE_SCORE_SAFE_DRIVE)
		ibonus = (GET_SCORE_CONTRIBUTION_SAFE_DRIVE_CAP() - (MC_Playerbd[iPartToUse].iVehDamage * GET_SCORE_CONTRIBUTION_SAFE_DRIVE_MULTIPLIER()))
		IF ibonus > 0
			iHighScore = iHighScore + ibonus
			PRINTLN("[RCC MISSION] HIGH SCORE - adding vehicle damage bonus of: ",ibonus)
			PRINTLN("[RCC MISSION] HIGH SCORE - Total: ",iHighScore)
		ENDIF
	ENDIF

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS,ciFMMC_IGNORE_SCORE_ACCURACY)

		INT iEndHits =  calculate_hits()
		INT iEndShots = calculate_shots()
		
		PRINTLN("[RCC MISSION] iEndHits: ",iEndHits)
		PRINTLN("[RCC MISSION] iEndShots: ",iEndHits)
		
		IF (iEndShots - MC_playerBD[iPartToUse].iStartShots) >= 1
			FLOAT frecentaccuracy = (TO_FLOAT(iEndHits - MC_playerBD[iPartToUse].iStartHits) / TO_FLOAT(iEndShots - MC_playerBD[iPartToUse].iStartShots))
			
			MC_playerBD[iPartToUse].iEndAccuracy = ROUND(frecentaccuracy * 100.0)
			
			ibonus = ROUND(frecentaccuracy * GET_SCORE_CONTRIBUTION_ACCURACY_MULTIPLIER())
			IF ibonus > 0
				iHighScore = iHighScore + ibonus
				PRINTLN("[RCC MISSION] HIGH SCORE - adding accuracy bonus of: ",ibonus)
				PRINTLN("[RCC MISSION] HIGH SCORE - Total: ",iHighScore)
			ENDIF
		ENDIF
	
	ELSE
		PRINTLN("[RCC MISSION] g_FMMC_STRUCT.iPointAwardsBS,ciFMMC_IGNORE_SCORE_ACCURACY is set - not calculating accuracy stat")
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS,ciFMMC_IGNORE_SCORE_CIVILIANS)
		ibonus = (GET_SCORE_CONTRIBUTION_CIV_KILLS_START() - (CLAMP_INT(MC_playerBD[iPartToUse].iCivilianKills, 0, GET_SCORE_CONTRIBUTION_CIV_KILLS_CAP()) * GET_SCORE_CONTRIBUTION_CIV_KILLS_MULTIPLIER()))
		IF ibonus > 0
			iHighScore = iHighScore + ibonus
			PRINTLN("[RCC MISSION] HIGH SCORE - adding low civ kill bonus of: ",ibonus)
			PRINTLN("[RCC MISSION] HIGH SCORE - Total: ",iHighScore)
		ENDIF
	
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS,ciFMMC_IGNORE_SCORE_HEALTH)

		ibonus = (GET_SCORE_CONTRIBUTION_HEALTH_START() - (MC_PlayerBD[iPartToUse].iNumPlayerDeaths * GET_SCORE_CONTRIBUTION_HEALTH_MULTIPLIER_0()))
		IF bLocalPlayerPedOk
			IF (GET_PED_MAX_HEALTH(LocalPlayerPed)-GET_ENTITY_HEALTH(LocalPlayerPed)) > 0
				ibonus = ibonus - (GET_SCORE_CONTRIBUTION_HEALTH_MULTIPLIER_1() * (GET_PED_MAX_HEALTH(LocalPlayerPed)-GET_ENTITY_HEALTH(LocalPlayerPed)))
			ENDIF
		ENDIF
		IF ibonus > 0
			iHighScore = iHighScore + ibonus
			PRINTLN("[RCC MISSION] HIGH SCORE - adding health bonus of: ",ibonus)
			PRINTLN("[RCC MISSION] HIGH SCORE - Total: ",iHighScore)
		ENDIF

	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS,ciFMMC_IGNORE_SCORE_HACK_SPEED)
		IF MC_playerBD[iPartToUse].iNumHacks > 0
			FLOAT faverageHackTime = TO_FLOAT(MC_playerBD[iPartToUse].iHackTime)/TO_FLOAT(MC_playerBD[iPartToUse].iNumHacks)
			faverageHackTime = (faverageHackTime/1000)
			INT iaverageHackTime = ROUND(faverageHackTime)
			ibonus = (GET_SCORE_CONTRIBUTION_HACK_SCORE_START_VALUE() - iaverageHackTime)
			INT iNumHacks = CLAMP_INT(MC_playerBD[iPartToUse].iNumHacks, 0, GET_SCORE_CONTRIBUTION_HACK_SCORE_CAP())
			ibonus += (iNumHacks * GET_SCORE_CONTRIBUTION_HACK_SCORE_MULTIPLIER())
			IF ibonus > 0
				iBonus = CLAMP_INT(iBonus, 0, GET_SCORE_CONTRIBUTION_HACK_SCORE_START_VALUE() * 2)
				iHighScore = iHighScore + ibonus
				PRINTLN("[RCC MISSION] HIGH SCORE - adding hack bonus of: ",ibonus)
				PRINTLN("[RCC MISSION] HIGH SCORE - Total: ",iHighScore)
			ENDIF
		ENDIF
	ENDIF

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS,ciFMMC_IGNORE_SCORE_WANTED_EVADE_SPEED)
		IF MC_playerBD[iPartToUse].iNumWantedLose > 0
			FLOAT faverageWantedLoseTime = TO_FLOAT(MC_playerBD[iPartToUse].iWantedTime)/TO_FLOAT(MC_playerBD[iPartToUse].iNumWantedLose)
			faverageWantedLoseTime = (faverageWantedLoseTime/1000)
			INT iaverageWantedLoseTime = ROUND(faverageWantedLoseTime)

			PRINTLN("[RCC MISSION] iWantedLoseTime - GET_MISSION_COMPLETION_HIGH_SCORE - TO_FLOAT(MC_playerBD[", iPartToUse, "].iWantedTime)/TO_FLOAT(MC_playerBD[", iPartToUse, "].iNumWantedLose) = TO_FLOAT(", MC_playerBD[iPartToUse].iWantedTime, ")/TO_FLOAT(", MC_playerBD[iPartToUse].iNumWantedLose, ") = ", TO_FLOAT(MC_playerBD[iPartToUse].iWantedTime)/TO_FLOAT(MC_playerBD[iPartToUse].iNumWantedLose))
			MC_playerBD[iPartToUse].iNumWantedStars = CLAMP_INT(MC_playerBD[iPartToUse].iNumWantedStars, 0, GET_SCORE_CONTRIBUTION_WANTED_LEVEL_MAX())
			ibonus = (MC_playerBD[iPartToUse].iNumWantedStars * GET_SCORE_CONTRIBUTION_WANTED_LEVEL_MULTIPLIER()) - (iaverageWantedLoseTime / GET_SCORE_CONTRIBUTION_WANTED_LEVEL_TIME_MULTIPLIER())
			IF ibonus > 0
				iBonus = CLAMP_INT(iBonus, 0, 5000)
				iHighScore = iHighScore + ibonus
				PRINTLN("[RCC MISSION] HIGH SCORE - adding wanted bonus of: ",ibonus)
				PRINTLN("[RCC MISSION] HIGH SCORE - Total: ",iHighScore)
			ENDIF
		ENDIF
	ELSE
		PRINTLN("[RCC MISSION] g_FMMC_STRUCT.iPointAwardsBS,ciFMMC_IGNORE_SCORE_WANTED_EVADE_SPEED is set - not calculating wanted lose time stat")
	ENDIF
	
	//BELOW OPTIONS ARE TURNED ON NOT OFF (Added for Gangops)
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		IF IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS, ciFMMC_USE_SCORE_PHONE_HACKING)
			ibonus = ((MC_playerBD_1[iLocalPart].iTimeSpentPhoneHacking / 1000) * g_sMPTunables.IH2_medal_phone_hacking_multiplier)
			IF ibonus > 0
				iBonus = CLAMP_INT(iBonus, 0, g_sMPTunables.IH2_medal_phone_hacking_max)
				iHighScore = iHighScore + ibonus
				PRINTLN("[RCC MISSION] HIGH SCORE - adding Time Spent Phone Hacking bonus of: ",ibonus)
				PRINTLN("[RCC MISSION] HIGH SCORE - Total: ",iHighScore)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS, ciFMMC_USE_SCORE_GUN_CAMERA_KILLS)
			ibonus = (MC_playerBD_1[iPartToUse].iGunCameraKills * g_sMPTunables.IH2_medal_gun_camera_kills_multiplier)
			IF ibonus > 0
				iBonus = CLAMP_INT(iBonus, 0, g_sMPTunables.IH2_medal_gun_camera_kills_max)
				iHighScore = iHighScore + ibonus
				PRINTLN("[RCC MISSION] HIGH SCORE - adding gun camera kill bonus of: ",ibonus)
				PRINTLN("[RCC MISSION] HIGH SCORE - Total: ",iHighScore)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS, ciFMMC_USE_SCORE_DAMAGE_TO_PEDS)
			ibonus = (MC_playerBD_1[iLocalPart].iDamageToPedsForMedal * g_sMPTunables.IH2_medal_damage_to_peds_multiplier)
			IF ibonus > 0
				iBonus = CLAMP_INT(iBonus, 0, g_sMPTunables.IH2_medal_damage_to_peds_max)
				iHighScore = iHighScore + ibonus
				PRINTLN("[RCC MISSION] HIGH SCORE - adding specific ped damage bonus of: ",ibonus)
				PRINTLN("[RCC MISSION] HIGH SCORE - Total: ",iHighScore)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS, ciFMMC_USE_SCORE_DAMAGE_TO_VEHS)
			ibonus = (MC_playerBD_1[iLocalPart].iDamageToVehsForMedal * g_sMPTunables.IH2_medal_damage_to_vehs_multiplier)
			IF ibonus > 0
				iBonus = CLAMP_INT(iBonus, 0, g_sMPTunables.IH2_medal_damage_to_vehs_max)
				iHighScore = iHighScore + ibonus
				PRINTLN("[RCC MISSION] HIGH SCORE - adding specific veh damage bonus of: ",ibonus)
				PRINTLN("[RCC MISSION] HIGH SCORE - Total: ",iHighScore)
			ENDIF
		ENDIF
	ENDIF
	
	INT iScoreCap = g_sMPtunables.iGLOBAL_LEADERBOARD_CAP_CONTACT_MISSIONS
	IF IS_THIS_AN_ADVERSARY_MODE_MISSION(g_FMMC_STRUCT.iRootContentIDHash, g_FMMC_STRUCT.iAdversaryModeType)
		iScoreCap = g_sMPtunables.iGLOBAL_LEADERBOARD_CAP_ADVERSARY_MODES
	ELIF IS_THIS_A_CAPTURE()
		iScoreCap = g_sMPtunables.iGLOBAL_LEADERBOARD_CAP_CAPTURES 
	ELIF IS_THIS_A_LTS()
		iScoreCap = g_sMPtunables.iGLOBAL_LEADERBOARD_CAP_LTSS 
	ELIF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		iScoreCap = g_sMPtunables.iGLOBAL_LEADERBOARD_CAP_H2
	ENDIF
	
	IF iScoreCap != 0
		PRINTLN("[RCC MISSION] CLAMPING - iScoreCap is ", iScoreCap)
		iHighScore = CLAMP_INT(iHighScore, 0, iScoreCap)
	ENDIF
	
	RETURN iHighScore
	
ENDFUNC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: HEIST SPECIAL CASE RESULTS LOGIC (achievements, telemetry, vehicle unlocks, score and medals) !
//
//************************************************************************************************************************************************************



PROC UPDATE_HEIST_ACHIEVEMENT_CHECK()
	// because the awards are on a character basis and these should be on player so we loop through awards 
	// and set bits on the stored stats
	
	// before i was checking the player awards (IF GET_MP_BOOL_PLAYER_AWARD(MPPLY_AWD_PACIFIC_FIN_INDEX))
	// don't check the awards here - check the heist completion index as that causes 2269737
	// the awards are given once and will always be valid even if we delete trophies
	CPRINTLN(DEBUG_ACHIEVEMENT, "[ACHIEVEMENT] - UPDATE_HEIST_ACHIEVEMENT_CHECK - iCurrentHeistMissionIndex:", iCurrentHeistMissionIndex)
	
	
	
	IF iCurrentHeistMissionIndex = HBCA_BS_PACIFIC_STANDARD_FINALE2
		SET_HEIST_ACHIEVEMENT_ID(HEISTACH_PACIFIC_DONE)
	ENDIF
	
	IF iCurrentHeistMissionIndex = HBCA_BS_HUMANE_LABS_FINALE
		SET_HEIST_ACHIEVEMENT_ID(HEISTACH_HUMANELAB_DONE)	
	ENDIF
	
	IF iCurrentHeistMissionIndex = HBCA_BS_PRISON_BREAK_FINALE
		SET_HEIST_ACHIEVEMENT_ID(HEISTACH_PRISON_DONE)
	ELSE
		SET_HEIST_ACHIEVEMENT_ID(HEISTACH_PRISON_DONE, FALSE)
	ENDIF
	
	IF iCurrentHeistMissionIndex = HBCA_BS_SERIES_A_FINALE
		SET_HEIST_ACHIEVEMENT_ID(HEISTACH_SERIESA_DONE)
	ENDIF
	
	IF iCurrentHeistMissionIndex = HBCA_BS_TUTORIAL_FINALE
		SET_HEIST_ACHIEVEMENT_ID(HEISTACH_FLECCAJOB_DONE)
	ENDIF	
ENDPROC

///Heist finale achievements:
PROC HANDLE_HEIST_ACHIEVEMENTS()
	
	// MAKE SURE THIS IS CALLED MARTIN'S HANDLE_HEIST_AWARDS()
	CPRINTLN(DEBUG_ACHIEVEMENT, "[ACHIEVEMENT]: HANDLE HEIST ACHIEVEMENTS START")
	UPDATE_HEIST_ACHIEVEMENT_CHECK()

	// check for achievement ach_52 - In the name of science - Human Labs Clear
	IF IS_HEIST_ACHIEVEMENT_ID_SET(HEISTACH_HUMANELAB_DONE) AND IS_HEIST_ACHIEVEMENT_ID_SET(HEISTACH_SERIESA_DONE)
		CPRINTLN(DEBUG_ACHIEVEMENT, "AWARD_ACHIEVEMENT(ACHH2) - ", DEBUG_GET_ACHIEVEMENT_TITLE(ACHH2), " - ACHIEVEMENT ID SET")
		SET_HEIST_ACHIEVEMENT_ID(HEISTACH_H2_DONE)
		// AWARD_ACHIEVEMENT(ACHH2) - We now set the achievement to be given when back in free mode (B*2090918)
	ENDIF

	// check for achievement ach_53 - Dead Presidents - Pacific Standard Clear
	IF IS_HEIST_ACHIEVEMENT_ID_SET(HEISTACH_FLECCAJOB_DONE) AND IS_HEIST_ACHIEVEMENT_ID_SET(HEISTACH_PACIFIC_DONE)
		CPRINTLN(DEBUG_ACHIEVEMENT, "AWARD_ACHIEVEMENT(ACHH3) - ", DEBUG_GET_ACHIEVEMENT_TITLE(ACHH3), " - ACHIEVEMENT ID SET")
		SET_HEIST_ACHIEVEMENT_ID(HEISTACH_H3_DONE)
		// AWARD_ACHIEVEMENT(ACHH3) - We now set the achievement to be given when back in free mode (B*2090918)
	ENDIF

	// check for achievement ach_54 - Parole Day - Prison Break Clear
	IF IS_HEIST_ACHIEVEMENT_ID_SET(HEISTACH_PRISON_DONE)
		CPRINTLN(DEBUG_ACHIEVEMENT, "AWARD_ACHIEVEMENT(ACHH4) - ", DEBUG_GET_ACHIEVEMENT_TITLE(ACHH4), " - ACHIEVEMENT ID SET")
		SET_HEIST_ACHIEVEMENT_ID(HEISTACH_H4_DONE)
		// AWARD_ACHIEVEMENT(ACHH4) - We now set the achievement to be given when back in free mode (B*2090918)
	ENDIF

	CPRINTLN(DEBUG_ACHIEVEMENT, "[ACHIEVEMENT]: HEIST CREW CUT ACHIEVEMENT - SANITY CHECK START - BEFORE BIT SET ATTEMPT")
	CPRINTLN(DEBUG_ACHIEVEMENT, "[ACHIEVEMENT]:    IS LOCAL PLAYER IN ACTIVE CLAN:", IS_LOCAL_PLAYER_IN_ACTIVE_CLAN())
	CPRINTLN(DEBUG_ACHIEVEMENT, "[ACHIEVEMENT]:    ACTIVE PLAYERS IN PLAYERS CREW:", GET_NUMBER_OF_ACTIVE_PLAYERS_IN_PLAYERS_CREW())
	CPRINTLN(DEBUG_ACHIEVEMENT, "[ACHIEVEMENT]:    IS CREW CUT BIT SET:", IS_HEIST_ACHIEVEMENT_ID_SET(HEISTACH_CREW_CUT))
	CPRINTLN(DEBUG_ACHIEVEMENT, "[ACHIEVEMENT]:    IS CREW CUT SANITY BIT SET:", IS_HEIST_ACHIEVEMENT_ID_SET(HEISTACH_CREW_CUT_SANCHECK))
	CPRINTLN(DEBUG_ACHIEVEMENT, "[ACHIEVEMENT]: HEIST CREW CUT ACHIEVEMENT - SANITY CHECK END")
	
	// Crew Cut
	IF NOT IS_HEIST_ACHIEVEMENT_ID_SET(HEISTACH_CREW_CUT)	
        IF IS_LOCAL_PLAYER_IN_ACTIVE_CLAN() AND (GET_NUMBER_OF_ACTIVE_PLAYERS_IN_PLAYERS_CREW() >= 2)
            SET_HEIST_ACHIEVEMENT_ID(HEISTACH_CREW_CUT)    
			SET_HEIST_ACHIEVEMENT_ID(HEISTACH_CREW_CUT_SANCHECK)      
        ENDIF   
    ENDIF
	
	CPRINTLN(DEBUG_ACHIEVEMENT, "[ACHIEVEMENT]: HEIST CREW CUT ACHIEVEMENT - SANITY CHECK START - AFTER BIT SET ATTEMPT")
	CPRINTLN(DEBUG_ACHIEVEMENT, "[ACHIEVEMENT]:    IS LOCAL PLAYER IN ACTIVE CLAN:", IS_LOCAL_PLAYER_IN_ACTIVE_CLAN())
	CPRINTLN(DEBUG_ACHIEVEMENT, "[ACHIEVEMENT]:    ACTIVE PLAYERS IN PLAYERS CREW:", GET_NUMBER_OF_ACTIVE_PLAYERS_IN_PLAYERS_CREW())
	CPRINTLN(DEBUG_ACHIEVEMENT, "[ACHIEVEMENT]:    IS CREW CUT BIT SET:", IS_HEIST_ACHIEVEMENT_ID_SET(HEISTACH_CREW_CUT))
	CPRINTLN(DEBUG_ACHIEVEMENT, "[ACHIEVEMENT]:    IS CREW CUT SANITY BIT SET:", IS_HEIST_ACHIEVEMENT_ID_SET(HEISTACH_CREW_CUT_SANCHECK))
	CPRINTLN(DEBUG_ACHIEVEMENT, "[ACHIEVEMENT]: HEIST CREW CUT ACHIEVEMENT - SANITY CHECK END")
ENDPROC

PROC INCREMENT_HEIST_FLOW_ORDER_AWARD_PROGRESS()
	INCREMENT_MP_INT_PLAYER_STAT(MPPLY_HEISTFLOWORDERPROGRESS)
	PRINTLN("[MJM] AWARD STEP ACHIEVED - Flow Order - Step ",GET_MP_INT_PLAYER_STAT(MPPLY_HEISTFLOWORDERPROGRESS))
ENDPROC

FUNC BOOL IS_HEIST_AWARD_BITSET_COMPLETE(INT iAwardBitset)

	INT iTempInt = ROUND(POW(2,HBCA_BS_MAX)) - 1
	FLOAT fTempFloat = POW(2,HBCA_BS_MAX) - 1

	PRINTLN("[MJM] IS_HEIST_AWARD_BITSET_COMPLETE: ",iAwardBitset, " = ",iTempInt," ?")
	PRINTLN("[MJM] IS_HEIST_AWARD_BITSET_COMPLETE: POW(2,HBCA_BS_MAX)     = ", POW(2,HBCA_BS_MAX))
	PRINTLN("[MJM] IS_HEIST_AWARD_BITSET_COMPLETE: POW(2.0,28.0)          = ", POW(2.0,28.0))
	PRINTLN("[MJM] IS_HEIST_AWARD_BITSET_COMPLETE: POW(2,HBCA_BS_MAX) - 1 = ", POW(2,HBCA_BS_MAX) - 1)
	PRINTLN("[MJM] IS_HEIST_AWARD_BITSET_COMPLETE: POW(2.0,28.0) - 1      = ", POW(2.0,28.0) - 1)
	PRINTLN("[MJM] IS_HEIST_AWARD_BITSET_COMPLETE: POW(2.0,28.0) - 1.0    = ", POW(2.0,28.0) - 1.0)
	INT iPower = ROUND(POW(2.0,28.0))
	PRINTLN("[MJM] IS_HEIST_AWARD_BITSET_COMPLETE: ", iPower, " - 1.0       = ", iPower - 1.0)
	PRINTLN("[MJM] IS_HEIST_AWARD_BITSET_COMPLETE: ", iPower, " - 1       = ", iPower - 1)
	PRINTLN("[MJM] IS_HEIST_AWARD_BITSET_COMPLETE: iAwardBitset = POW(2,HBCA_BS_MAX) - 1      = ",iAwardBitset = POW(2,HBCA_BS_MAX) - 1)
	PRINTLN("[MJM] IS_HEIST_AWARD_BITSET_COMPLETE: (iAwardBitset = POW(2,HBCA_BS_MAX) - 1)      = ",(iAwardBitset = POW(2,HBCA_BS_MAX) - 1))
	PRINTLN("[MJM] IS_HEIST_AWARD_BITSET_COMPLETE: (iAwardBitset = (POW(2,HBCA_BS_MAX) - 1))      = ",(iAwardBitset = (POW(2,HBCA_BS_MAX) - 1)))
	PRINTLN("[MJM] IS_HEIST_AWARD_BITSET_COMPLETE: (iAwardBitset = (ROUND(POW(2,HBCA_BS_MAX)) - 1)      = ",(iAwardBitset = (ROUND(POW(2,HBCA_BS_MAX)) - 1)))
	PRINTLN("[MJM] IS_HEIST_AWARD_BITSET_COMPLETE: (iAwardBitset = fTempFloat)      = ",(iAwardBitset = fTempFloat))
	PRINTLN("[MJM] IS_HEIST_AWARD_BITSET_COMPLETE: (iAwardBitset = iTempInt)      = ",(iAwardBitset = iTempInt))
	
	RETURN (iAwardBitset = iTempInt) //2^28(-1))

ENDFUNC

PROC HANDLE_HEIST_FINALE_COMPLETION_AWARDS(MPPLY_BOOL_AWARD_INDEX award)
	INT iScriptTransactionIndex
	PRINTLN("[MJM] HANDLE_HEIST_FINALE_COMPLETION_AWARDS() - Mission: ",iCurrentHeistMissionIndex)
	
	IF NOT GET_MP_BOOL_PLAYER_AWARD(award)
		BOOL bComplete
		STRING sAwardLiteral

		
		PRINTLN("[MJM] HANDLE_HEIST_FINALE_COMPLETION_AWARDS() - GET_MP_BOOL_CHARACTER_AWARD(award)")
		
		SWITCH(award)
		
			CASE MPPLY_AWD_PACIFIC_FIN_INDEX
				PRINTLN("[MJM] HANDLE_HEIST_FINALE_COMPLETION_AWARDS() - AWARD: PACIFIC")
				bComplete = TRUE
				sAwardLiteral = "PACIFIC"
			BREAK
			CASE MPPLY_AWD_HUMANE_FIN_INDEX
				PRINTLN("[MJM] HANDLE_HEIST_FINALE_COMPLETION_AWARDS() - AWARD: HUMANE")
				bComplete = TRUE
				sAwardLiteral = "HUMANE"
			BREAK
			CASE MPPLY_AWD_PRISON_FIN_INDEX
				PRINTLN("[MJM] HANDLE_HEIST_FINALE_COMPLETION_AWARDS() - AWARD: PRISON")
				bComplete = TRUE
				sAwardLiteral = "PRISON"
			BREAK
			CASE MPPLY_AWD_SERIESA_FIN_INDEX
				PRINTLN("[MJM] HANDLE_HEIST_FINALE_COMPLETION_AWARDS() - AWARD: SERIES A")
				bComplete = TRUE
				sAwardLiteral = "FUNDING"
			BREAK
			CASE MPPLY_AWD_FLEECA_FIN_INDEX
				PRINTLN("[MJM] HANDLE_HEIST_FINALE_COMPLETION_AWARDS() - AWARD: FLEECA")
				bComplete = TRUE
				sAwardLiteral = "FLEECA"
			BREAK
			DEFAULT
				PRINTLN("[MJM] HANDLE_HEIST_FINALE_COMPLETION_AWARDS() - NOT A HEIST FINALE")
			BREAK
		ENDSWITCH
		
		IF bComplete
			SET_MP_BOOL_PLAYER_AWARDS(award,TRUE) 
			g_award_event_heist_complete_strand_bonus = TRUE
			TEXT_LABEL_23 challengeString = "First_Time_" 
			challengeString += sAwardLiteral
			
			IF USE_SERVER_TRANSACTIONS()
				TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_JOB_BONUS_FIRST_TIME_BONUS,g_sMPTunables.iheist_First_Time_Bonus, iScriptTransactionIndex,FALSE,TRUE)
				g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl31ContenID = g_FMMC_STRUCT.tl31LoadedContentID
				g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl23Challenge = challengeString
			ELSE
				NETWORK_EARN_FIRST_TIME_BONUS(g_sMPTunables.iheist_First_Time_Bonus,g_FMMC_STRUCT.tl31LoadedContentID,challengeString)
			ENDIF
			#IF IS_DEBUG_BUILD
				PRINTLN("[MJM] AWARD GIVEN - ",sAwardLiteral)
				g_DEBUG_Heist_First_Time_Fleeca = GET_MP_BOOL_PLAYER_AWARD(MPPLY_AWD_FLEECA_FIN_INDEX)
				g_DEBUG_Heist_First_Time_Prison = GET_MP_BOOL_PLAYER_AWARD(MPPLY_AWD_PRISON_FIN_INDEX)
				g_DEBUG_Heist_First_Time_Humane = GET_MP_BOOL_PLAYER_AWARD(MPPLY_AWD_HUMANE_FIN_INDEX)
				g_DEBUG_Heist_First_Time_Series = GET_MP_BOOL_PLAYER_AWARD(MPPLY_AWD_SERIESA_FIN_INDEX)
				g_DEBUG_Heist_First_Time_Pacific = GET_MP_BOOL_PLAYER_AWARD(MPPLY_AWD_PACIFIC_FIN_INDEX)
			#ENDIF
		
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bFirstTimeBonusShouldBeShown = TRUE
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iFirstTimeBonusValue = g_sMPTunables.iheist_First_Time_Bonus

			// check for achivement achh8 - Can't Touch This - Complete a Heist finale without taking any damage
			IF (MC_Playerbd[iLocalPart].iPlayerDamage <= 0) AND (MC_Playerbd[iLocalPart].iNumPlayerDeaths <= 0)
				IF NOT IS_THIS_A_QUICK_RESTART_JOB() AND NOT IS_CORONA_INITIALISING_A_QUICK_RESTART()
					CPRINTLN(DEBUG_ACHIEVEMENT, "AWARD_ACHIEVEMENT(ACHH8) - ", DEBUG_GET_ACHIEVEMENT_TITLE(ACHH8), " - ACHIEVEMENT ID SET")
					SET_HEIST_ACHIEVEMENT_ID(HEISTACH_H8_DONE)
				ENDIF
				// AWARD_ACHIEVEMENT(ACHH8)  - We now set the achievement to be given when back in free mode (B*2090918)
			ENDIF
		ENDIF
		
	ENDIF

ENDPROC

PROC HANDLE_HEIST_FLOW_ORDER_AWARD()
	INT iScriptTransactionIndex
	IF NOT GET_MP_BOOL_PLAYER_AWARD(MPPLY_AWD_HST_ORDER_INDEX)
				
		//Set bit for mission
		INT iTempBitset = GET_MP_INT_PLAYER_STAT(MPPLY_HEISTFLOWORDERPROGRESS)
		
		IF iTempBitset != 0
		OR iCurrentHeistMissionIndex = 0 //Don't start marking progress unless on first mission
		
			SET_BIT(iTempBitset, iCurrentHeistMissionIndex)
			
			SET_MP_INT_PLAYER_STAT(MPPLY_HEISTFLOWORDERPROGRESS,iTempBitset)
			PRINTLN("[MJM] AWARD PROGRESS - Flow Order - Mission Complete: ",iCurrentHeistMissionIndex," Stat is now: ",iTempBitset)
				
			IF IS_HEIST_AWARD_BITSET_COMPLETE(iTempBitset)
				SET_MP_BOOL_PLAYER_AWARDS(MPPLY_AWD_HST_ORDER_INDEX,TRUE)
				g_award_event_heist_flow_order = TRUE
				IF USE_SERVER_TRANSACTIONS()
					TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_JOB_BONUS_HEIST_AWARD,g_sMPTunables.iheist_Order_Bonus, iScriptTransactionIndex, FALSE,TRUE)
					g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl31ContenID = g_FMMC_STRUCT.tl31LoadedContentID
					g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl23Challenge = "Flow_Order"
				ELSE
					NETWORK_EARN_HEIST_AWARD(g_sMPTunables.iheist_Order_Bonus,g_FMMC_STRUCT.tl31LoadedContentID,"Flow_Order")
				ENDIF
				
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.bOrderBonusShouldBeShown = TRUE
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.iOrderBonusValue = g_sMPTunables.iheist_Order_Bonus
				PRINTLN("[MJM] AWARD GIVEN - Flow Order")
			ENDIF	
			
			#IF IS_DEBUG_BUILD
				g_DEBUG_Heist_Flow_Order_BS	= GET_MP_INT_PLAYER_STAT(MPPLY_HEISTFLOWORDERPROGRESS)
			#ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC

PROC HANDLE_HEIST_SAME_TEAM_AWARD()
	INT iScriptTransactionIndex
	IF NOT GET_MP_BOOL_PLAYER_AWARD(MPPLY_AWD_HST_SAME_TEAM_INDEX)

		//Set bit for mission
		INT iTempBitset = GET_MP_INT_PLAYER_STAT(MPPLY_HEISTTEAMPROGRESSBITSET)
		
		SET_BIT(iTempBitset, iCurrentHeistMissionIndex)
		
		//For checking if we have made progress
		INCREMENT_MP_INT_PLAYER_STAT(MPPLY_HEISTTEAMPROGRESSCOUNTER) 
		
		SET_MP_INT_PLAYER_STAT(MPPLY_HEISTTEAMPROGRESSBITSET,iTempBitset)
		PRINTLN("[MJM] AWARD PROGRESS - Same Team - Mission Complete: ",iCurrentHeistMissionIndex," Stat is now: ",iTempBitset)
		
		//If bitset = full, award
		IF IS_HEIST_AWARD_BITSET_COMPLETE(iTempBitset)
			SET_MP_BOOL_PLAYER_AWARDS(MPPLY_AWD_HST_SAME_TEAM_INDEX,TRUE)
			g_award_event_heist_same_team = TRUE
			
			IF USE_SERVER_TRANSACTIONS()
				TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_JOB_BONUS_HEIST_AWARD,g_sMPTunables.iheist_Same_Team_Bonus, iScriptTransactionIndex,FALSE,TRUE)
				g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl31ContenID = g_FMMC_STRUCT.tl31LoadedContentID
				g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl23Challenge = "Same_Team"
			ELSE
				NETWORK_EARN_HEIST_AWARD(g_sMPTunables.iheist_Same_Team_Bonus,g_FMMC_STRUCT.tl31LoadedContentID,"Same_Team")
			ENDIF
			
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bSameTeamBonusShouldBeShown = TRUE
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iSameTeamBonusValue = g_sMPTunables.iheist_Same_Team_Bonus
			PRINTLN("[MJM] AWARD GIVEN - Same Team")
		ENDIF
		
		#IF IS_DEBUG_BUILD
			g_DEBUG_Heist_Same_Team_BS	= GET_MP_INT_PLAYER_STAT(MPPLY_HEISTTEAMPROGRESSBITSET)
		#ENDIF
		
	ENDIF
	
ENDPROC

PROC HANDLE_HEIST_ULTIMATE_CHALLENGE_AWARD()
	INT iScriptTransactionIndex
	IF NOT GET_MP_BOOL_PLAYER_AWARD(MPPLY_AWD_HST_ULT_CHAL_INDEX)		
		IF (g_FMMC_STRUCT.iDifficulity = DIFF_HARD)
			IF NOT HAVE_ANY_PLAYERS_DIED_ON_CURRENT_HEIST_MISSION()
					
				//Set no death bit
				INT iTempBitset = GET_MP_INT_PLAYER_STAT(MPPLY_HEISTNODEATHPROGREITSET)
				
				IF iTempBitset != 0
				OR iCurrentHeistMissionIndex = 0 //Don't start marking progress unless on first mission
				
					SET_BIT(iTempBitset, iCurrentHeistMissionIndex)
					SET_MP_INT_PLAYER_STAT(MPPLY_HEISTNODEATHPROGREITSET,iTempBitset)
					
					PRINTLN("[MJM] AWARD PROGRESS - Ultimate Challenge - Mission Complete: ",iCurrentHeistMissionIndex," Stat is now: ",iTempBitset)
					
					IF IS_HEIST_AWARD_BITSET_COMPLETE(iTempBitset)
						// Award
						SET_MP_BOOL_PLAYER_AWARDS(MPPLY_AWD_HST_ULT_CHAL_INDEX,TRUE)
						g_award_event_heist_ultimate_challenge = TRUE
						IF USE_SERVER_TRANSACTIONS()
							TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_JOB_BONUS_CRIMINAL_MASTERMIND, g_sMPTunables.iheist_Ultimate_Challenge, iScriptTransactionIndex, FALSE, TRUE)
							g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl31ContenID = g_FMMC_STRUCT.tl31LoadedContentID
							g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl23Challenge = "Ultimate_Challenge"
						ELSE
							NETWORK_EARN_FROM_CRIMINAL_MASTERMIND(g_sMPTunables.iheist_Ultimate_Challenge,g_FMMC_STRUCT.tl31LoadedContentID,"Ultimate_Challenge")
						ENDIF
						g_TransitionSessionNonResetVars.sGlobalCelebrationData.bUltimateBonusShouldBeShown = TRUE
						g_TransitionSessionNonResetVars.sGlobalCelebrationData.iUltimateBonusValue = g_sMPTunables.iheist_Ultimate_Challenge
						PRINTLN("[MJM] AWARD GIVEN - Ultimate Challenge")
					ENDIF
				ENDIF
				
				#IF IS_DEBUG_BUILD
					g_DEBUG_Heist_No_Deaths_BS	= GET_MP_INT_PLAYER_STAT(MPPLY_HEISTNODEATHPROGREITSET)
				#ENDIF
			ELSE
				PRINTLN("[MJM] Ultimate Challenge Fail - Players died.")
			ENDIF
		ELSE
			PRINTLN("[MJM] Ultimate Challenge Fail - Not on hard.")
		ENDIF
	ENDIF


ENDPROC

PROC HANDLE_HEIST_MEMBER_AWARD()
	INT iScriptTransactionIndex				
	IF NOT GET_HEIST_AM_I_HEIST_LEADER_GLOBAL()
	AND NOT IS_BIT_SET(MC_playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, PBBOOL_HEIST_HOST)
	
		//Set bit for mission
		INT iTempBitset = GET_MP_INT_PLAYER_STAT(MPPLY_HEISTMEMBERPROGRESSBITSET)

		SET_BIT(iTempBitset, iCurrentHeistMissionIndex)

		SET_MP_INT_PLAYER_STAT(MPPLY_HEISTMEMBERPROGRESSBITSET,iTempBitset)
		PRINTLN("[MJM] AWARD PROGRESS - Member - Mission Complete: ",iCurrentHeistMissionIndex," Stat is now: ",iTempBitset)
			
		IF IS_HEIST_AWARD_BITSET_COMPLETE(iTempBitset)
			IF NOT GET_MP_BOOL_PLAYER_AWARD(MPPLY_AWD_COMPLET_HEIST_MEM_INDEX)
				SET_MP_BOOL_PLAYER_AWARDS(MPPLY_AWD_COMPLET_HEIST_MEM_INDEX,TRUE)
				IF USE_SERVER_TRANSACTIONS()
					TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_JOB_BONUS_HEIST_AWARD,g_sMPTunables.iamount_heist_members_bonus, iScriptTransactionIndex,FALSE,TRUE)
					g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl31ContenID = g_FMMC_STRUCT.tl31LoadedContentID
					g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl23Challenge = "Member"
				ELSE
					NETWORK_EARN_HEIST_AWARD(g_sMPTunables.iamount_heist_members_bonus ,g_FMMC_STRUCT.tl31LoadedContentID,"Member") 
				ENDIF
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.bMemberBonusShouldBeShown = TRUE
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMemberBonusValue = g_sMPTunables.iamount_heist_members_bonus
				PRINTLN("[MJM] AWARD GIVEN - Member")
			ENDIF
		ENDIF	

		#IF IS_DEBUG_BUILD
			g_DEBUG_Heist_Member_BS = GET_MP_INT_PLAYER_STAT(MPPLY_HEISTMEMBERPROGRESSBITSET)
		#ENDIF
	ENDIF
		

ENDPROC

PROC HANDLE_HEIST_FIRST_PERSON_AWARD()
	INT iScriptTransactionIndex			
	IF g_FMMC_STRUCT.iFixedCamera = 1
	
		//Set bit for mission
		INT iTempBitset = GET_MP_INT_PLAYER_STAT(MPPLY_HEIST_1STPERSON_PROG)

		SET_BIT(iTempBitset, iCurrentHeistMissionIndex)

		SET_MP_INT_PLAYER_STAT(MPPLY_HEIST_1STPERSON_PROG,iTempBitset)
		PRINTLN("[MJM] AWARD PROGRESS - First Person - Mission Complete: ",iCurrentHeistMissionIndex)
			
		IF IS_HEIST_AWARD_BITSET_COMPLETE(iTempBitset)
			IF NOT GET_MP_BOOL_PLAYER_AWARD(MPPLY_AWD_COMPLET_HEIST_1STPER_INDEX)
				SET_MP_BOOL_PLAYER_AWARDS(MPPLY_AWD_COMPLET_HEIST_1STPER_INDEX,TRUE)
				IF USE_SERVER_TRANSACTIONS()
					TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_JOB_BONUS_HEIST_AWARD,g_sMPTunables.iHeist_First_Person_Bonus, iScriptTransactionIndex,FALSE,TRUE)
					g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl31ContenID = g_FMMC_STRUCT.tl31LoadedContentID
					g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl23Challenge = "First Person"
				ELSE
					NETWORK_EARN_HEIST_AWARD(g_sMPTunables.iHeist_First_Person_Bonus,g_FMMC_STRUCT.tl31LoadedContentID,"First Person")
				ENDIF
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.bFirstPersonBonusShouldBeShown = TRUE
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.iFirstPersonBonusValue = g_sMPTunables.iHeist_First_Person_Bonus
				PRINTLN("[MJM] AWARD GIVEN - First Person")
			ENDIF
		ENDIF	

		#IF IS_DEBUG_BUILD
			g_DEBUG_Heist_First_Person_BS	= GET_MP_INT_PLAYER_STAT(MPPLY_HEIST_1STPERSON_PROG)
		#ENDIF
	ENDIF
		

ENDPROC

FUNC SUM22_EVENT_REWARDS GET_HEIST_SUM22_EVENT_REWARD_ENUM(INT iHeistMissionIndex)
	
	SWITCH iHeistMissionIndex
		CASE HBCA_BS_PACIFIC_STANDARD_FINALE2 RETURN SUM22_EVENT_REWARD_COMPLETE_PACIFIC_STANDARD_FINALE
		CASE HBCA_BS_HUMANE_LABS_FINALE RETURN SUM22_EVENT_REWARD_COMPLETE_HUMANE_LABS_RAID_FINALE
		CASE HBCA_BS_PRISON_BREAK_FINALE RETURN SUM22_EVENT_REWARD_COMPLETE_PRISON_BREAK_FINALE
		CASE HBCA_BS_SERIES_A_FINALE RETURN SUM22_EVENT_REWARD_COMPLETE_SERIES_A_FUNDING_FINALE
		CASE HBCA_BS_TUTORIAL_FINALE RETURN SUM22_EVENT_REWARD_COMPLETE_FLEECA_JOB_FINALE
	ENDSWITCH
	
	RETURN SUM22_EVENT_REWARD_INVALID
ENDFUNC

PROC HANDLE_HEIST_AWARDS()
	PRINTLN("[MJM] HANDLE_HEIST_AWARDS() - Mission: ",iCurrentHeistMissionIndex)
	
	IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
		//First Time: One off
		IF iCurrentHeistMissionIndex = HBCA_BS_PACIFIC_STANDARD_FINALE2		
			HANDLE_HEIST_FINALE_COMPLETION_AWARDS(MPPLY_AWD_PACIFIC_FIN_INDEX)
		ELIF iCurrentHeistMissionIndex = HBCA_BS_HUMANE_LABS_FINALE
			HANDLE_HEIST_FINALE_COMPLETION_AWARDS(MPPLY_AWD_HUMANE_FIN_INDEX)
		ELIF iCurrentHeistMissionIndex = HBCA_BS_PRISON_BREAK_FINALE
			HANDLE_HEIST_FINALE_COMPLETION_AWARDS(MPPLY_AWD_PRISON_FIN_INDEX)
		ELIF iCurrentHeistMissionIndex = HBCA_BS_SERIES_A_FINALE
			HANDLE_HEIST_FINALE_COMPLETION_AWARDS(MPPLY_AWD_SERIESA_FIN_INDEX)
		ELIF iCurrentHeistMissionIndex = HBCA_BS_TUTORIAL_FINALE
			HANDLE_HEIST_FINALE_COMPLETION_AWARDS(MPPLY_AWD_FLEECA_FIN_INDEX)
		ENDIF
		
		IF GET_HEIST_SUM22_EVENT_REWARD_ENUM(iCurrentHeistMissionIndex) != SUM22_EVENT_REWARD_INVALID
			IF IS_SUM22_EVENT_REWARD_ACTIVE(GET_HEIST_SUM22_EVENT_REWARD_ENUM(iCurrentHeistMissionIndex))
			AND NOT DOES_LOCAL_PLAYER_OWN_SUM22_EVENT_REWARD(GET_HEIST_SUM22_EVENT_REWARD_ENUM(iCurrentHeistMissionIndex))
				GIVE_LOCAL_PLAYER_SUM22_EVENT_REWARD(GET_HEIST_SUM22_EVENT_REWARD_ENUM(iCurrentHeistMissionIndex))
			ENDIF
		ENDIF
	ENDIF
	
	//Flow Order: One off
	//Fleeca -> Prison -> Humane -> Series A -> Pacific
	IF NOT g_smptunables.bDisableHeistAwardFlowOrder
		HANDLE_HEIST_FLOW_ORDER_AWARD()
	ENDIF
	
	//Same Team: One off
	IF NOT g_smptunables.bDisableHeistAwardSameTeam
		HANDLE_HEIST_SAME_TEAM_AWARD()
	ENDIF
	
	//Ultimate Challenge: One off
	IF NOT g_smptunables.bDisableHeistAwardUltimate
		HANDLE_HEIST_ULTIMATE_CHALLENGE_AWARD()
	ENDIF
	
	//Member: One off
	IF NOT g_sMPTunables.bdisable_heist_bonus
		HANDLE_HEIST_MEMBER_AWARD()
	ENDIF

	//First Person: One off
	IF NOT g_sMPTunables.bDisableHeistAwardFirstPerson
		HANDLE_HEIST_FIRST_PERSON_AWARD()
	ENDIF
	
ENDPROC


FUNC BOOL HAVE_PASSED_MC_CONDITIONS_FOR_GANGOPS_FLOW_AWARD(MPPLY_BOOL_AWARD_INDEX award)

 	IF IS_GANGOPS_FLOW_AWARD_A_MASTERMIND_AWARD(award)
		IF (g_FMMC_STRUCT.iDifficulity = DIFF_HARD)
			IF NOT g_TransitionSessionNonResetVars.bAnyPlayerDiedDuringMission
				RETURN TRUE
			ENDIF
		ENDIF
		RETURN FALSE
	ENDIF

	RETURN TRUE

ENDFUNC

FUNC GANGOPS_AWARD_TYPE GET_GANGOPS_AWARD_EARN_TYPE(MPPLY_BOOL_AWARD_INDEX award)

	SWITCH award	
		// First Time
		CASE MPPLY_AWD_GANGOPS_IAA_INDEX			RETURN GANGOPS_AWARD_FIRST_TIME_XM_BASE
		CASE MPPLY_AWD_GANGOPS_SUBMARINE_INDEX		RETURN GANGOPS_AWARD_FIRST_TIME_XM_SUBMARINE
		CASE MPPLY_AWD_GANGOPS_MISSILE_INDEX		RETURN GANGOPS_AWARD_FIRST_TIME_XM_SILO
		
		// Flow Awards
		CASE MPPLY_AWD_GANGOPS_ALLINORDER_INDEX		RETURN GANGOPS_AWARD_ORDER
		CASE MPPLY_AWD_GANGOPS_LOYALTY_INDEX		RETURN GANGOPS_AWARD_LOYALTY_AWARD_4
		CASE MPPLY_AWD_GANGOPS_LOYALTY2_INDEX		RETURN GANGOPS_AWARD_LOYALTY_AWARD_2
		CASE MPPLY_AWD_GANGOPS_LOYALTY3_INDEX		RETURN GANGOPS_AWARD_LOYALTY_AWARD_3
		CASE MPPLY_AWD_GANGOPS_CRIMMASMD_INDEX		RETURN GANGOPS_AWARD_MASTERMIND_4
		CASE MPPLY_AWD_GANGOPS_CRIMMASMD2_INDEX		RETURN GANGOPS_AWARD_MASTERMIND_2
		CASE MPPLY_AWD_GANGOPS_CRIMMASMD3_INDEX		RETURN GANGOPS_AWARD_MASTERMIND_3
		CASE MPPLY_AWD_GANGOPS_SUPPORT_INDEX		RETURN GANGOPS_AWARD_SUPPORTING
	ENDSWITCH

	SCRIPT_ASSERT("[MJM][GO_AWARDS] GET_GANGOPS_AWARD_EARN_TYPE - Unknown award passed, this value will be incorrect.")
	PRINTLN("[MJM][GO_AWARDS] GET_GANGOPS_AWARD_EARN_TYPE - Unknown award passed, this value will be incorrect.")
	RETURN GANGOPS_AWARD_ORDER

ENDFUNC

FUNC TRANSACTION_SERVICES GET_GANGOPS_FLOW_AWARD_TRANSACTION_SERVICE(MPPLY_BOOL_AWARD_INDEX award)
	
	SWITCH award	
		// First Time
		CASE MPPLY_AWD_GANGOPS_IAA_INDEX			RETURN SERVICE_EARN_GANGOPS_AWARD_FIRST_TIME_XM_BASE
		CASE MPPLY_AWD_GANGOPS_SUBMARINE_INDEX		RETURN SERVICE_EARN_GANGOPS_AWARD_FIRST_TIME_XM_SUBMARINE
		CASE MPPLY_AWD_GANGOPS_MISSILE_INDEX		RETURN SERVICE_EARN_GANGOPS_AWARD_FIRST_TIME_XM_SILO
		
		// Flow Awards
		CASE MPPLY_AWD_GANGOPS_ALLINORDER_INDEX		RETURN SERVICE_EARN_GANGOPS_AWARD_ORDER
		CASE MPPLY_AWD_GANGOPS_LOYALTY_INDEX		RETURN SERVICE_EARN_GANGOPS_AWARD_LOYALTY_AWARD_4
		CASE MPPLY_AWD_GANGOPS_LOYALTY2_INDEX		RETURN SERVICE_EARN_GANGOPS_AWARD_LOYALTY_AWARD_2
		CASE MPPLY_AWD_GANGOPS_LOYALTY3_INDEX		RETURN SERVICE_EARN_GANGOPS_AWARD_LOYALTY_AWARD_3
		CASE MPPLY_AWD_GANGOPS_CRIMMASMD_INDEX		RETURN SERVICE_EARN_GANGOPS_AWARD_MASTERMIND_4
		CASE MPPLY_AWD_GANGOPS_CRIMMASMD2_INDEX		RETURN SERVICE_EARN_GANGOPS_AWARD_MASTERMIND_2
		CASE MPPLY_AWD_GANGOPS_CRIMMASMD3_INDEX		RETURN SERVICE_EARN_GANGOPS_AWARD_MASTERMIND_3
		CASE MPPLY_AWD_GANGOPS_SUPPORT_INDEX		RETURN SERVICE_EARN_GANGOPS_AWARD_SUPPORTING
	ENDSWITCH
	
	SCRIPT_ASSERT("[MJM][GO_AWARDS] GET_GANGOPS_FLOW_AWARD_TRANSACTION_SERVICE - Unknown award passed, this value will be incorrect.")
	PRINTLN("[MJM][GO_AWARDS] GET_GANGOPS_FLOW_AWARD_TRANSACTION_SERVICE - Unknown award passed, this value will be incorrect.")
	RETURN SERVICE_EARN_JOB_BONUS_HEIST_AWARD

ENDFUNC

PROC EARN_GANGOPS_FLOW_AWARD(MPPLY_BOOL_AWARD_INDEX award)

	NETWORK_EARN_GANGOPS_AWARD(GET_AWARD_VALUE_FOR_GANGOPS_AWARD(award),g_FMMC_STRUCT.tl31LoadedContentID, GET_GANGOPS_AWARD_EARN_TYPE(award))
	
ENDPROC

PROC HANDLE_GANGOPS_FLOW_AWARD(MPPLY_BOOL_AWARD_INDEX award, GANGOPS_FLOW_AWARD_MISSION eMission)
		
	IF SHOULD_ALLOW_GANGOPS_FLOW_AWARD(award, eMission)
	AND HAVE_PASSED_MC_CONDITIONS_FOR_GANGOPS_FLOW_AWARD(award)
	
		// Set mission complete
		SET_GANGOPS_FLOW_AWARD_COMPLETE_FOR_MISSION(award, eMission)
		
		// Check if all missions complete
		IF IS_GANGOPS_FLOW_AWARD_COMPLETE(award)
			SET_MP_BOOL_PLAYER_AWARDS(award,TRUE)

			IF USE_SERVER_TRANSACTIONS()
				INT iScriptTransactionIndex
				TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(GET_GANGOPS_FLOW_AWARD_TRANSACTION_SERVICE(award),GET_AWARD_VALUE_FOR_GANGOPS_AWARD(award), iScriptTransactionIndex, FALSE,TRUE)
				g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl31ContenID = g_FMMC_STRUCT.tl31LoadedContentID
				g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl23Challenge = GET_GANGOPS_AWARD_STRING(award)
			ELSE
				EARN_GANGOPS_FLOW_AWARD(award)
			ENDIF
			
			SET_BIT(g_TransitionSessionNonResetVars.sGlobalCelebrationData.iGangopsAwardBS,ENUM_TO_INT(award))
			
			PRINTLN("[MJM][GO_AWARDS] HANDLE_GANGOPS_FLOW_AWARD - Given award - ",GET_GANGOPS_AWARD_STRING(award))
		ENDIF	
	ELSE
		PRINTLN("[MJM][GO_AWARDS] HANDLE_GANGOPS_FLOW_AWARD - Failed conditions for award - ",GET_GANGOPS_AWARD_STRING(award))
	ENDIF

ENDPROC

PROC HANDLE_GANGOPS_FINALE_COMPLETION_AWARDS(MPPLY_BOOL_AWARD_INDEX award)
	
	PRINTLN("[MJM][GO_AWARDS] HANDLE_GANGOPS_FINALE_COMPLETION_AWARDS - Award: ",GET_GANGOPS_AWARD_STRING(award))
	
	IF NOT IS_GANGOPS_FIRST_TIME_AWARD_DISABLED()
		IF NOT GET_MP_BOOL_PLAYER_AWARD(award)

			PRINTLN("[MJM][GO_AWARDS] HANDLE_GANGOPS_FINALE_COMPLETION_AWARDS - First time, giving award.")
			
			// Give award
			SET_MP_BOOL_PLAYER_AWARDS(award,TRUE) 
			
			// Telemetry
			TEXT_LABEL_23 challengeString = "First_Time_" 
			challengeString += GET_GANGOPS_AWARD_STRING(award)
			
			// Give cash
			IF USE_SERVER_TRANSACTIONS()
				INT iScriptTransactionIndex
				TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(GET_GANGOPS_FLOW_AWARD_TRANSACTION_SERVICE(award),GET_AWARD_VALUE_FOR_GANGOPS_AWARD(award), iScriptTransactionIndex,FALSE,TRUE)
				g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl31ContenID = g_FMMC_STRUCT.tl31LoadedContentID
				g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl23Challenge = challengeString
			ELSE
				NETWORK_EARN_GANGOPS_AWARD(GET_AWARD_VALUE_FOR_GANGOPS_AWARD(award),g_FMMC_STRUCT.tl31LoadedContentID, GET_GANGOPS_AWARD_EARN_TYPE(award))
			ENDIF
		
			// Celebration screen
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bFirstTimeBonusShouldBeShown = TRUE
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iFirstTimeBonusValue = GET_AWARD_VALUE_FOR_GANGOPS_AWARD(award)
			
		ENDIF
	ENDIF

ENDPROC


/// PURPOSE:
///    Checks and awards GangOps mission completion achievements
/// PARAMS:
///    eMission - Mission that was just completed
PROC HANDLE_GANGOPS_MISSION_ACHIEVEMENTS(GANGOPS_FLOW_AWARD_MISSION eMission)
	PRINTLN("[MJM][GO_AWARDS] HANDLE_GANGOPS_MISSION_ACHIEVEMENTS - Checking mission ",eMission,", Last Strand: ",CHECK_TO_SEE_IF_THIS_IS_THE_LAST_STRAND_MISSION())

	IF CHECK_TO_SEE_IF_THIS_IS_THE_LAST_STRAND_MISSION() //B* 4151814 - Missile Silo is 2-part
	AND (eMission = GFAM_MC_IAA_FINALE OR eMission = GFAM_MC_SUBMARINE_FINALE OR eMission = GFAM_MC_FINALE)
		
		BOOL doneIAA 		= HAS_ACHIEVEMENT_BEEN_AWARDED(ACHGO2)
		BOOL doneSubJob 	= HAS_ACHIEVEMENT_BEEN_AWARDED(ACHGO3)
		BOOL doneMissSilo 	= HAS_ACHIEVEMENT_BEEN_AWARDED(ACHGO4)		
	
		PRINTLN("[MJM][GO_AWARDS] HANDLE_GANGOPS_MISSION_ACHIEVEMENTS - This is a finale mission. Checking achievements already obtained ",
		"IAA-",doneIAA,", Submarine-",doneSubJob,", Missile Silo-",doneMissSilo)
	
		//Finale completion achievements
		IF (eMission = GFAM_MC_IAA_FINALE AND NOT doneIAA)
			//Give IAA achievement
			SET_GANGOPS_ACHIEVEMENT_DONE(GOPS_ACH_2_DONE)
			doneIAA = TRUE
		ENDIF
		
		IF (eMission = GFAM_MC_SUBMARINE_FINALE AND NOT doneSubJob)
			//Give Submarine Job achievement
			SET_GANGOPS_ACHIEVEMENT_DONE(GOPS_ACH_3_DONE)
			doneSubJob = TRUE
		ENDIF
		
		IF (eMission = GFAM_MC_FINALE AND NOT doneMissSilo)
			//Give Missile Silo achievement
			SET_GANGOPS_ACHIEVEMENT_DONE(GOPS_ACH_4_DONE)
			doneMissSilo = TRUE
		ENDIF
		
		IF (doneIAA AND doneSubJob AND doneMissSilo AND NOT HAS_ACHIEVEMENT_BEEN_AWARDED(ACHGO5))
			//Give World Worth Saving achievement
			SET_GANGOPS_ACHIEVEMENT_DONE(GOPS_ACH_5_DONE)
		ENDIF
	ENDIF
	
	IF 	GET_MP_BOOL_PLAYER_AWARD(MPPLY_AWD_GANGOPS_CRIMMASMD_INDEX) 
	AND	GET_MP_BOOL_PLAYER_AWARD(MPPLY_AWD_GANGOPS_CRIMMASMD2_INDEX)
	AND	GET_MP_BOOL_PLAYER_AWARD(MPPLY_AWD_GANGOPS_CRIMMASMD3_INDEX)
	AND NOT HAS_ACHIEVEMENT_BEEN_AWARDED(ACHGO8)
		//Give MASTERMINDS achievement
		SET_GANGOPS_ACHIEVEMENT_DONE(GOPS_ACH_8_DONE)
	ENDIF
ENDPROC	
	
FUNC SUM22_EVENT_REWARDS GET_GANGOPS_SUM22_EVENT_REWARD_ENUM(INT iHeistMissionIndex)
	
	SWITCH iHeistMissionIndex
		CASE ciGANGOPS_FLOW_MISSION_IAABASE_FINALE RETURN SUM22_EVENT_REWARD_COMPLETE_DATA_BREACHES_FINALE
		CASE ciGANGOPS_FLOW_MISSION_SUBMARINE_FINALE RETURN SUM22_EVENT_REWARD_COMPLETE_BOGDAN_PROBLEM_FINALE
		CASE ciGANGOPS_FLOW_MISSION_MISSILE_SILO_FINALE_P2 RETURN SUM22_EVENT_REWARD_COMPLETE_DOOMSDAY_SCENARIO_FINALE
	ENDSWITCH
	
	RETURN SUM22_EVENT_REWARD_INVALID
ENDFUNC

PROC HANDLE_GANGOPS_AWARDS()

	INT iCurrentMission = GANGOPS_FLOW_GET_CURRENT_GANGOPS_MISSION()
	PRINTLN("[MJM][GO_AWARDS] HANDLE_GANGOPS_AWARDS - Mission: ",iCurrentMission)

	// Convert
	GANGOPS_FLOW_AWARD_MISSION eMission = GET_GANGOPS_FLOW_AWARD_MISSION_FROM_GANGOPS_FLOW_MISSION(iCurrentMission)

	IF CHECK_TO_SEE_IF_THIS_IS_THE_LAST_STRAND_MISSION()
		IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION_FINALE()
			//First Time: One off
			HANDLE_GANGOPS_FINALE_COMPLETION_AWARDS(GET_AWARD_FOR_GANGOPS_FINALE(iCurrentMission))
		ENDIF
		
		HANDLE_GANGOPS_FLOW_AWARD(MPPLY_AWD_GANGOPS_ALLINORDER_INDEX, eMission)
		HANDLE_GANGOPS_FLOW_AWARD(MPPLY_AWD_GANGOPS_LOYALTY_INDEX, eMission)
		HANDLE_GANGOPS_FLOW_AWARD(MPPLY_AWD_GANGOPS_LOYALTY2_INDEX, eMission)
		HANDLE_GANGOPS_FLOW_AWARD(MPPLY_AWD_GANGOPS_LOYALTY3_INDEX, eMission)
		HANDLE_GANGOPS_FLOW_AWARD(MPPLY_AWD_GANGOPS_CRIMMASMD_INDEX, eMission)
		HANDLE_GANGOPS_FLOW_AWARD(MPPLY_AWD_GANGOPS_CRIMMASMD2_INDEX, eMission)
		HANDLE_GANGOPS_FLOW_AWARD(MPPLY_AWD_GANGOPS_CRIMMASMD3_INDEX, eMission)
		HANDLE_GANGOPS_FLOW_AWARD(MPPLY_AWD_GANGOPS_SUPPORT_INDEX, eMission)
		
		DO_CREW_CUT_ACHIEVEMENT()
	ENDIF

	IF GET_GANGOPS_SUM22_EVENT_REWARD_ENUM(iCurrentMission) != SUM22_EVENT_REWARD_INVALID
		IF IS_SUM22_EVENT_REWARD_ACTIVE(GET_GANGOPS_SUM22_EVENT_REWARD_ENUM(iCurrentMission))
		AND NOT DOES_LOCAL_PLAYER_OWN_SUM22_EVENT_REWARD(GET_GANGOPS_SUM22_EVENT_REWARD_ENUM(iCurrentMission))
			GIVE_LOCAL_PLAYER_SUM22_EVENT_REWARD(GET_GANGOPS_SUM22_EVENT_REWARD_ENUM(iCurrentMission))
		ENDIF
	ENDIF
	
	HANDLE_GANGOPS_MISSION_ACHIEVEMENTS(eMission)

ENDPROC

PROC HANDLE_CASINO_HEIST_AWARDS()
	IF CHECK_TO_SEE_IF_THIS_IS_THE_LAST_STRAND_MISSION()
		IF g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__STEALTH
			IF NOT GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_UNDETECTED)
			AND NOT IS_BIT_SET(MC_serverBD_1.sLEGACYMissionContinuityVars.iGenericTrackingBitset, ciLEGACY_ContinuityGenericTrackingBS_DroppedToDirect)
				SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_UNDETECTED, TRUE)
				PRINTLN("[MJM][GO_AWARDS] HANDLE_CASINO_HEIST_AWARDS - Awarding MP_AWARD_UNDETECTED")
			ENDIF
		ELIF g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
			IF NOT GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_INPLAINSI)
			AND NOT IS_BIT_SET(MC_serverBD_1.sLEGACYMissionContinuityVars.iGenericTrackingBitset, ciLEGACY_ContinuityGenericTrackingBS_DroppedToDirect)
				SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_INPLAINSI, TRUE)
				PRINTLN("[MJM][GO_AWARDS] HANDLE_CASINO_HEIST_AWARDS - Awarding MP_AWARD_INPLAINSI")
			ENDIF
		ELIF g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__DIRECT
			IF NOT GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_SMASHNGRAB)
				SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_SMASHNGRAB, TRUE)
				PRINTLN("[MJM][GO_AWARDS] HANDLE_CASINO_HEIST_AWARDS - Awarding MP_AWARD_SMASHNGRAB")
			ENDIF
		ENDIF
	
		DO_CREW_CUT_ACHIEVEMENT()
	ENDIF
ENDPROC

PROC HANDLE_CASINO_HEIST_PART_1_REWARDS()
	//NETWORK_EARN_CASINO_STORY_MISSION_REWARD
ENDPROC

FUNC BOOL HAVE_ANY_COP_PEDS_BEEN_SPOOKED()
	
	BOOL bCopSpooked = FALSE
	
	INT iped
	FOR iped = 0 TO (MC_serverBD.iNumPedCreated - 1)
		IF IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
		OR IS_BIT_SET(MC_serverBD.iPedAggroedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			IF IS_PED_A_SCRIPTED_COP(iped)
				bCopSpooked = TRUE
				iped = MC_serverBD.iNumPedCreated // Break out
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN bCopSpooked
	
ENDFUNC

FUNC BOOL HAVE_ANY_PEDS_BEEN_SPOOKED()
	
	BOOL bPedSpooked = FALSE
	
	INT iped
	FOR iped = 0 TO (MC_serverBD.iNumPedCreated - 1)
		IF IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
		OR IS_BIT_SET(MC_serverBD.iPedAggroedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			bPedSpooked = TRUE
			iped = MC_serverBD.iNumPedCreated // Break out
		ENDIF
	ENDFOR
	
	RETURN bPedSpooked
	
ENDFUNC

PROC HANDLE_HEIST_TELEMETRY()
	
	//Prison Break - Station: Were the cops alerted?
	IF (iCurrentHeistMissionIndex = HBCA_BS_PRISON_BREAK_STATION)
		IF HAVE_ANY_COP_PEDS_BEEN_SPOOKED()
			PRINTLN("[RCC MISSION] HANDLE_HEIST_TELEMETRY - Prison Break Station - Someone spooked/aggroed the cops, setting g_sJobHeistInfo.m_spookedCops")
			g_sJobHeistInfo.m_spookedCops = 1
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[RCC MISSION] HANDLE_HEIST_TELEMETRY - Prison Break Station - The cops weren't spooked/aggroed")
		#ENDIF
		ENDIF
	ENDIF
	
	g_sJobHeistInfo.m_teamLivesUsed = MC_serverBD.iTotalCoopDeaths[MC_playerBD[iPartToUse].iteam]
	PRINTLN("[RCC MISSION] HANDLE_HEIST_TELEMETRY - g_sJobHeistInfo.m_teamLivesUsed - ",g_sJobHeistInfo.m_teamLivesUsed )
	g_sJobHeistInfo.m_cashLost  =MC_ServerBD.iCashGrabTotalDrop
	PRINTLN("[RCC MISSION] HANDLE_HEIST_TELEMETRY - g_sJobHeistInfo.m_cashLost  - ",g_sJobHeistInfo.m_cashLost  )
	g_sJobHeistInfo.m_cashPickedUp =MC_ServerBD.iCashGrabTotalTake
	PRINTLN("[RCC MISSION] HANDLE_HEIST_TELEMETRY - g_sJobHeistInfo.m_cashPickedUp - ",g_sJobHeistInfo.m_cashPickedUp )
	g_sJobHeistInfo.m_failureRole =MC_serverBD.iFailTeam
	PRINTLN("[RCC MISSION] HANDLE_HEIST_TELEMETRY - g_sJobHeistInfo.m_failureRole - ",g_sJobHeistInfo.m_failureRole )
	
ENDPROC

PROC HANDLE_GANGOPS_TELEMETRY(BOOL bStart = FALSE)
	PRINTLN("[JS][GOTEL][TEL][TEL] - HANDLE_GANGOPS_TELEMETRY - bStart ", bStart)
	IF bStart
		g_sJobGangopsInfo.m_infos.m_ishost = (GB_GET_LOCAL_PLAYER_GANG_BOSS() = LocalPlayer)
		IF GB_GET_LOCAL_PLAYER_GANG_BOSS() != INVALID_PLAYER_INDEX()
			g_sJobGangopsInfo.m_bossId1 = GB_GET_GANG_BOSS_UUID_INT_A(GB_GET_LOCAL_PLAYER_GANG_BOSS())
			g_sJobGangopsInfo.m_bossId2 = GB_GET_GANG_BOSS_UUID_INT_B(GB_GET_LOCAL_PLAYER_GANG_BOSS())			
		ENDIF
		g_sJobGangopsInfo.m_BossType = iBossType
		g_sJobGangopsInfo.m_OwnBase =  GET_MP_INT_CHARACTER_STAT(MP_STAT_DBASE_OWNED)
		g_sJobGangopsInfo.m_OwnCannon = GET_MP_INT_CHARACTER_STAT(MP_STAT_DBASE_ORBITAL_WEAPON)
		g_sJobGangopsInfo.m_OwnSecurityRoom = GET_MP_INT_CHARACTER_STAT(MP_STAT_DBASE_SECURITY_ROOM)
		g_sJobGangopsInfo.m_OwnLounge = BOOL_TO_INT(IS_DEFUNCT_BASE_PERSONAL_QUARTERS_1_PURCHASED() OR IS_DEFUNCT_BASE_PERSONAL_QUARTERS_2_PURCHASED() OR IS_DEFUNCT_BASE_PERSONAL_QUARTERS_3_PURCHASED() OR DOES_PLAYER_OWN_A_DEFUNCT_BASE(LocalPlayer))
		g_sJobGangopsInfo.m_OwnLivingQuarters =  GET_MP_INT_CHARACTER_STAT(MP_STAT_DBASE_PERSONAL_QUARTERS)
		g_sJobGangopsInfo.m_OwnTiltRotor = BOOL_TO_INT(IS_ARMORY_AIRCRAFT_PURCHASED())
	ELSE
		g_sJobGangopsInfo.m_infos.m_teamLivesUsed = MC_serverBD.iTotalCoopDeaths[MC_playerBD[iPartToUse].iteam]
		g_sJobGangopsInfo.m_FailedStealth = 0
		g_sJobGangopsInfo.m_BossType = iBossType
		INT i
		FOR i = 0 TO FMMC_MAX_TEAMS - 1
			IF MC_serverBD.eCurrentTeamFail[i] >= mFail_PED_AGRO_T0
			AND MC_serverBD.eCurrentTeamFail[i] <= mFail_PED_AGRO_T3
				g_sJobGangopsInfo.m_FailedStealth = 1
				BREAKLOOP
			ENDIF
		ENDFOR

		g_sJobGangopsInfo.m_infos.m_failureRole = MC_serverBD.iFailTeam
		PRINTLN("[JS][GOTEL][TEL][TEL] - HANDLE_GANGOPS_TELEMETRY - g_sJobHeistInfo.m_failureRole - ",g_sJobHeistInfo.m_failureRole )
	ENDIF
ENDPROC

PROC HANDLE_CASINO_HEIST_TELEMETRY(BOOL bStart = FALSE, BOOL bPassed = FALSE)
	PRINTLN("[JS][CHTEL][TEL] - HANDLE_CASINO_HEIST_TELEMETRY - bStart ", bStart)
	PRINTLN("[JS][CHTEL][TEL] - HANDLE_CASINO_HEIST_TELEMETRY - bosstype ", g_sCasino_Heist_Finale_Telemetry_data.bosstype)
	IF bStart
		IF GB_GET_LOCAL_PLAYER_GANG_BOSS() != INVALID_PLAYER_INDEX()
			g_sCasino_Heist_Finale_Telemetry_data.bossId1 = GB_GET_GANG_BOSS_UUID_INT_A(GB_GET_LOCAL_PLAYER_GANG_BOSS())
			g_sCasino_Heist_Finale_Telemetry_data.bossId2 = GB_GET_GANG_BOSS_UUID_INT_B(GB_GET_LOCAL_PLAYER_GANG_BOSS())
			PRINTLN("[JS][CHTEL][TEL] - HANDLE_CASINO_HEIST_TELEMETRY - bossId1 ", g_sCasino_Heist_Finale_Telemetry_data.bossId1)
			PRINTLN("[JS][CHTEL][TEL] - HANDLE_CASINO_HEIST_TELEMETRY - bossId2 ", g_sCasino_Heist_Finale_Telemetry_data.bossId2)
		ENDIF
		g_sCasino_Heist_Finale_Telemetry_data.playerRole = GET_PLAYER_ROLE_FOR_TELEMETRY()
		PRINTLN("[JS][CHTEL][TEL] - HANDLE_CASINO_HEIST_TELEMETRY - playerRole ", g_sCasino_Heist_Finale_Telemetry_data.playerRole)
	ELSE	
		//g_sCasino_Heist_Finale_Telemetry_data.endingReason - Tracked Elsewhere 
		//g_sCasino_Heist_Finale_Telemetry_data.replay - Tracked Elsewhere 
		//g_sCasino_Heist_Finale_Telemetry_data.rpEarned - Tracked Elsewhere 
		//g_sCasino_Heist_Finale_Telemetry_data.percentage - Tracked Elsewhere
		
		g_sCasino_Heist_Finale_Telemetry_data.timeTakenToComplete = MC_serverBD.iTotalMissionEndTime + g_TransitionSessionNonResetVars.iTotalMissionEndTime
		PRINTLN("[JS][CHTEL][TEL] - HANDLE_CASINO_HEIST_TELEMETRY - timeTakenToComplete ", g_sCasino_Heist_Finale_Telemetry_data.timeTakenToComplete)
		IF bPassed
			g_sCasino_Heist_Finale_Telemetry_data.vaultAmt = MC_serverBD_1.iVaultTake
			PRINTLN("[JS][CHTEL][TEL] - HANDLE_CASINO_HEIST_TELEMETRY - vaultAmt ", g_sCasino_Heist_Finale_Telemetry_data.vaultAmt)
			g_sCasino_Heist_Finale_Telemetry_data.dailyCashRoomAmt = MC_serverBD_1.iDailyCashRoomTake
			PRINTLN("[JS][CHTEL][TEL] - HANDLE_CASINO_HEIST_TELEMETRY - dailyCashRoomAmt ", g_sCasino_Heist_Finale_Telemetry_data.dailyCashRoomAmt)
			g_sCasino_Heist_Finale_Telemetry_data.depositBoxAmt = MC_serverBD_1.iDepositBoxTake
			PRINTLN("[JS][CHTEL][TEL] - HANDLE_CASINO_HEIST_TELEMETRY - depositBoxAmt ", g_sCasino_Heist_Finale_Telemetry_data.depositBoxAmt)
			g_sCasino_Heist_Finale_Telemetry_data.checkpoint = 0
			PRINTLN("[JS][CHTEL][TEL] - HANDLE_CASINO_HEIST_TELEMETRY - checkpoint ", g_sCasino_Heist_Finale_Telemetry_data.checkpoint)
		ELSE
			g_sCasino_Heist_Finale_Telemetry_data.checkpoint = MC_serverBD_1.iStrandMissionsComplete
			PRINTLN("[JS][CHTEL][TEL] - HANDLE_CASINO_HEIST_TELEMETRY - checkpoint ", g_sCasino_Heist_Finale_Telemetry_data.checkpoint)
		ENDIF
		g_sCasino_Heist_Finale_Telemetry_data.approachDirect = IS_BIT_SET(MC_serverBD_1.sLEGACYMissionContinuityVars.iGenericTrackingBitset, ciLEGACY_ContinuityGenericTrackingBS_DroppedToDirect)
		PRINTLN("[JS][CHTEL][TEL] - HANDLE_CASINO_HEIST_TELEMETRY - approachDirect ", g_sCasino_Heist_Finale_Telemetry_data.approachDirect)
		g_sCasino_Heist_Finale_Telemetry_data.vehicleSwapped = BOOL_TO_INT(IS_BIT_SET(sLEGACYMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciLEGACY_ContinuityGenericTrackingBS_SwapVehicleUsed))
		PRINTLN("[JS][CHTEL][TEL] - HANDLE_CASINO_HEIST_TELEMETRY - vehicleSwapped ", g_sCasino_Heist_Finale_Telemetry_data.vehicleSwapped)
		g_sCasino_Heist_Finale_Telemetry_data.useEMP = BOOL_TO_INT(HAS_PHONE_EMP_BEEN_USED())
		PRINTLN("[JS][CHTEL][TEL] - HANDLE_CASINO_HEIST_TELEMETRY - useEMP ", g_sCasino_Heist_Finale_Telemetry_data.useEMP)
		g_sCasino_Heist_Finale_Telemetry_data.useDrone = BOOL_TO_INT(IS_BIT_SET(sLEGACYMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciLEGACY_ContinuityGenericTrackingBS_DroneUsedTelemetryTracker))
		PRINTLN("[JS][CHTEL][TEL] - HANDLE_CASINO_HEIST_TELEMETRY - useDrone ", g_sCasino_Heist_Finale_Telemetry_data.useDrone)
		g_sCasino_Heist_Finale_Telemetry_data.useThermite = BOOL_TO_INT(IS_BIT_SET(sLEGACYMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciLEGACY_ContinuityGenericTrackingBS_ThermiteUsed))
		PRINTLN("[JS][CHTEL][TEL] - HANDLE_CASINO_HEIST_TELEMETRY - useThermite ", g_sCasino_Heist_Finale_Telemetry_data.useThermite)
		g_sCasino_Heist_Finale_Telemetry_data.useKeycard = BOOL_TO_INT(IS_BIT_SET(sLEGACYMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciLEGACY_ContinuityGenericTrackingBS_KeycardUsed))
		PRINTLN("[JS][CHTEL][TEL] - HANDLE_CASINO_HEIST_TELEMETRY - useKeycard ", g_sCasino_Heist_Finale_Telemetry_data.useKeycard)
		g_sCasino_Heist_Finale_Telemetry_data.hack = BOOL_TO_INT(IS_BIT_SET(sLEGACYMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciLEGACY_ContinuityGenericTrackingBS_HackUsed))
		PRINTLN("[JS][CHTEL][TEL] - HANDLE_CASINO_HEIST_TELEMETRY - hack ", g_sCasino_Heist_Finale_Telemetry_data.hack)
		g_sCasino_Heist_Finale_Telemetry_data.accessPoints = MC_serverBD_1.iEntranceUsed
		PRINTLN("[JS][CHTEL][TEL] - HANDLE_CASINO_HEIST_TELEMETRY - accessPoints ", g_sCasino_Heist_Finale_Telemetry_data.accessPoints)
		g_sCasino_Heist_Finale_Telemetry_data.deaths = MC_Playerbd[iLocalPart].iNumPlayerDeaths
		PRINTLN("[JS][CHTEL][TEL] - HANDLE_CASINO_HEIST_TELEMETRY - deaths ", g_sCasino_Heist_Finale_Telemetry_data.deaths)
		g_sCasino_Heist_Finale_Telemetry_data.targetsKilled = MC_Playerbd[iLocalPart].iNumPedKills + MC_Playerbd[iLocalPart].iAmbientCopsKilled
		PRINTLN("[JS][CHTEL][TEL] - HANDLE_CASINO_HEIST_TELEMETRY - targetsKilled ", g_sCasino_Heist_Finale_Telemetry_data.targetsKilled)
		g_sCasino_Heist_Finale_Telemetry_data.innocentsKilled = MC_playerBD[iLocalPart].iCivilianKills
		PRINTLN("[JS][CHTEL][TEL] - HANDLE_CASINO_HEIST_TELEMETRY - innocentsKilled ", g_sCasino_Heist_Finale_Telemetry_data.innocentsKilled)
	ENDIF
ENDPROC

PROC HANDLE_CASINO_TELEMETRY(BOOL bStart = FALSE)
	PRINTLN("[ML][TEL] - HANDLE_CASINO_TELEMETRY - bStart ", bStart)
	IF bStart
		
	ELSE
		g_sCasino_Story_Telemetry_data.m_TeamLivesUsed = MC_serverBD.iTotalCoopDeaths[MC_playerBD[iPartToUse].iteam]
//		g_sCasino_Story_Telemetry_data.m_BossType = iBossType
				
		g_sCasino_Story_Telemetry_data.m_FailedStealth = 0

		IF HAVE_ANY_PEDS_BEEN_SPOOKED()
			PRINTLN("[ML][TEL] HANDLE_CASINO_TELEMETRY - Someone spooked/aggroed a ped, setting g_sCasino_Story_Telemetry_data.m_FailedStealth")
			g_sCasino_Story_Telemetry_data.m_FailedStealth = 1
		ENDIF
		
		IF HAVE_ANY_COP_PEDS_BEEN_SPOOKED()
			PRINTLN("[ML][TEL] HANDLE_CASINO_TELEMETRY - Someone spooked/aggroed the cops, setting g_sCasino_Story_Telemetry_data.m_SpookedCops")
			g_sCasino_Story_Telemetry_data.m_SpookedCops = TRUE
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[ML][TEL] HANDLE_CASINO_TELEMETRY - The cops weren't spooked/aggroed")
		#ENDIF
		ENDIF	
			
	ENDIF
ENDPROC

PROC UNLOCK_HEIST_VEHICLE_IF_NEEDED(PLAYERVEHICLE_UNLOCK vehUnlock = PLAYERVEHICLE_NONE)
	
	IF vehUnlock != PLAYERVEHICLE_NONE
	AND NOT IS_MP_VEHICLE_UNLOCKED(vehUnlock)
		SET_MP_VEHICLE_UNLOCKED(vehUnlock, TRUE, TRUE)
		PRINTLN("[RCC MISSION] UNLOCK_HEIST_VEHICLE_IF_NEEDED - Setting vehicle unlock (",ENUM_TO_INT(vehUnlock),") as unlocked")
	#IF IS_DEBUG_BUILD
	ELSE
		IF vehUnlock = PLAYERVEHICLE_NONE
			PRINTLN("[RCC MISSION] UNLOCK_HEIST_VEHICLE_IF_NEEDED - Vehicle unlock = PLAYERVEHICLE_NONE")
		ELIF IS_MP_VEHICLE_UNLOCKED(vehUnlock)
			PRINTLN("[RCC MISSION] UNLOCK_HEIST_VEHICLE_IF_NEEDED - Vehicle unlock (",ENUM_TO_INT(vehUnlock),") is already unlocked")
		ENDIF
	#ENDIF
	ENDIF
	
ENDPROC

PROC HANDLE_HEIST_VEHICLE_UNLOCKS()
	
	PLAYERVEHICLE_UNLOCK vehUnlock = PLAYERVEHICLE_NONE
	PLAYERVEHICLE_UNLOCK vehUnlock2 = PLAYERVEHICLE_NONE
	PLAYERVEHICLE_UNLOCK vehUnlock3 = PLAYERVEHICLE_NONE
	PLAYERVEHICLE_UNLOCK vehUnlock4 = PLAYERVEHICLE_NONE
	
	SWITCH iCurrentHeistMissionIndex
		
		CASE HBCA_BS_TUTORIAL_FINALE
			vehUnlock = PLAYER_VEHICLE_HEIST_KURUMA
			vehUnlock2 = PLAYER_VEHICLE_HEIST_KURUMA2
		BREAK
		
		CASE HBCA_BS_PRISON_BREAK_STATION
			vehUnlock = PLAYER_VEHICLE_HEIST_CASCO
		BREAK
		CASE HBCA_BS_PRISON_BREAK_FINALE
			vehUnlock = PLAYER_VEHICLE_HEIST_VELUM2
			vehUnlock2 = PLAYER_VEHICLE_HEIST_PBUS
		BREAK
		
		CASE HBCA_BS_HUMANE_LABS_EMP
			vehUnlock = PLAYER_VEHICLE_HEIST_HYDRA
		BREAK
		CASE HBCA_BS_HUMANE_LABS_FINALE
			vehUnlock = PLAYER_VEHICLE_HEIST_VALKRYIE
			vehUnlock2 = PLAYER_VEHICLE_HEIST_INSURGENT
			vehUnlock3 = PLAYER_VEHICLE_HEIST_INSURGENT2
			vehUnlock4 = PLAYER_VEHICLE_HEIST_DINGHY3
		BREAK
		
		CASE HBCA_BS_SERIES_A_FINALE
			vehUnlock = PLAYER_VEHICLE_HEIST_TECHNICAL
			vehUnlock2 = PLAYER_VEHICLE_HEIST_MULE3
		BREAK
		
		CASE HBCA_BS_PACIFIC_STANDARD_VANS
			vehUnlock = PLAYER_VEHICLE_HEIST_BOXVILLE4
		BREAK
		CASE HBCA_BS_PACIFIC_STANDARD_CONVOY
			vehUnlock = PLAYER_VEHICLE_HEIST_SAVAGE
		BREAK
		CASE HBCA_BS_PACIFIC_STANDARD_HACK
			vehUnlock = PLAYER_VEHICLE_HEIST_GBURRITO2
		BREAK
		CASE HBCA_BS_PACIFIC_STANDARD_FINALE2
			vehUnlock = PLAYER_VEHICLE_HEIST_LECTRO
		BREAK
	ENDSWITCH
	
	PRINTLN("[RCC MISSION] HANDLE_HEIST_VEHICLE_UNLOCKS - vehUnlock = ",ENUM_TO_INT(vehUnlock),", vehUnlock2 = ",ENUM_TO_INT(vehUnlock2),", vehUnlock3 = ",ENUM_TO_INT(vehUnlock3),", vehUnlock4 = ",ENUM_TO_INT(vehUnlock4))
	
	UNLOCK_HEIST_VEHICLE_IF_NEEDED(vehUnlock)
	UNLOCK_HEIST_VEHICLE_IF_NEEDED(vehUnlock2)
	UNLOCK_HEIST_VEHICLE_IF_NEEDED(vehUnlock3)
	UNLOCK_HEIST_VEHICLE_IF_NEEDED(vehUnlock4)
	
ENDPROC

PROC CHECK_EVENT_AWARD_FOR_COMPLETING_ADVERSARY_MODE()
	
	IF g_sMPTunables.bENABLE_AWARD_ADVMODE
		INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_ADVERSARYMISSION_CR)
		PRINTLN("CHECK_EVENT_AWARD_FOR_COMPLETING_ADVERSARY_MODE")
		IF GET_MP_INT_CHARACTER_STAT(MP_STAT_ADVERSARYMISSION_CR) >=g_sMPTunables.inumber_of_times_to_unlock_adversary_award    
			 UNLOCK_CLOTHING_AWARD(CLOTHING_AWARD_ADVMODE)
			 PRINTLN("CHECK_EVENT_AWARD_FOR_COMPLETING_ADVERSARY_MODE UNLOCK_CLOTHING_AWARD(CCLOTHING_AWARD_ADVMODE)")
		ENDIF
	ENDIF
	
ENDPROC

PROC CHECK_EVENT_AWARD_FOR_COMPLETING_COVERT_OPERATION()
	IF g_sMPTunables.bENABLE_AWARD_7COVOPMISSION
		INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_COVOMISSION_CR)
		PRINTLN("CHECK_EVENT_AWARD_FOR_COMPLETING_COVERT_OPERATION")
		IF GET_MP_INT_CHARACTER_STAT(MP_STAT_COVOMISSION_CR) >= g_sMPTunables.inumber_of_times_to_unlock_covert_award   
			 UNLOCK_CLOTHING_AWARD(CLOTHING_AWARD_7COVOPMISSION)
			 PRINTLN("CHECK_EVENT_AWARD_FOR_COMPLETING_COVERT_OPERATION UNLOCK_CLOTHING_AWARD(CLOTHING_AWARD_7COVOPMISSION)")
		ENDIF
	ENDIF
	
	IF g_sMPTunables.bENABLE_AWARD_COVOPMISSION
		 UNLOCK_CLOTHING_AWARD(CLOTHING_AWARD_COVOPMISSION)
		 PRINTLN("CHECK_EVENT_AWARD_FOR_COMPLETING_COVERT_OPERATION UNLOCK_CLOTHING_AWARD(CLOTHING_AWARD_COVOPMISSION)")
	ENDIF

ENDPROC

#IF FEATURE_CASINO_HEIST
PROC CASINO_HEIST_COMPLETION_REWARDS()
	
	IF IS_SUM22_EVENT_REWARD_ACTIVE(SUM22_EVENT_REWARD_COMPLETE_DIAMOND_CASINO_FINALE)
	AND NOT DOES_LOCAL_PLAYER_OWN_SUM22_EVENT_REWARD(SUM22_EVENT_REWARD_COMPLETE_DIAMOND_CASINO_FINALE)
		GIVE_LOCAL_PLAYER_SUM22_EVENT_REWARD(SUM22_EVENT_REWARD_COMPLETE_DIAMOND_CASINO_FINALE)
	ENDIF

	IF g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__STEALTH //Stealth
		SWITCH GET_STYLE_FROM_OUTFIT(MC_playerBD[iLocalPart].iOutfit)
			CASE OUTFIT_STYLE_CASINO_HEIST_STEALTH_I
				SET_PACKED_STAT_BOOL(PACKED_STAT_STEALTH_LIGHT_1_OUTFITS, TRUE)
				PRINTLN("CASINO_HEIST_COMPLETION_REWARDS - Setting packed stat PACKED_STAT_STEALTH_LIGHT_1_OUTFITS")
			BREAK
			CASE OUTFIT_STYLE_CASINO_HEIST_STEALTH_II
				SET_PACKED_STAT_BOOL(PACKED_STAT_STEALTH_LIGHT_2_OUTFITS, TRUE)
				PRINTLN("CASINO_HEIST_COMPLETION_REWARDS - Setting packed stat PACKED_STAT_STEALTH_LIGHT_2_OUTFITS")
			BREAK
			CASE OUTFIT_STYLE_CASINO_HEIST_STEALTH_III
				SET_PACKED_STAT_BOOL(PACKED_STAT_STEALTH_LIGHT_3_OUTFITS, TRUE)
				PRINTLN("CASINO_HEIST_COMPLETION_REWARDS - Setting packed stat PACKED_STAT_STEALTH_LIGHT_3_OUTFITS")
			BREAK
			CASE OUTFIT_STYLE_CASINO_HEIST_COVERT_STEALTH_I
				SET_PACKED_STAT_BOOL(PACKED_STAT_STEALTH_TECH_1_OUTFITS, TRUE)
				PRINTLN("CASINO_HEIST_COMPLETION_REWARDS - Setting packed stat PACKED_STAT_STEALTH_TECH_1_OUTFITS")
			BREAK
			CASE OUTFIT_STYLE_CASINO_HEIST_COVERT_STEALTH_II
				SET_PACKED_STAT_BOOL(PACKED_STAT_STEALTH_TECH_2_OUTFITS, TRUE)
				PRINTLN("CASINO_HEIST_COMPLETION_REWARDS - Setting packed stat PACKED_STAT_STEALTH_TECH_2_OUTFITS")
			BREAK
			CASE OUTFIT_STYLE_CASINO_HEIST_COVERT_STEALTH_III
				SET_PACKED_STAT_BOOL(PACKED_STAT_STEALTH_TECH_3_OUTFITS, TRUE)
				PRINTLN("CASINO_HEIST_COMPLETION_REWARDS - Setting packed stat PACKED_STAT_STEALTH_TECH_3_OUTFITS")
			BREAK
		ENDSWITCH
	ELIF g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
		SWITCH g_sCasinoHeistMissionConfigData.eSubterfugeOutfitsIn
			CASE CASINO_HEIST_OUTFIT_IN__BUGSTAR
				SET_PACKED_STAT_BOOL(PACKED_STAT_BUGSTAR_OUTFITS, TRUE)
				PRINTLN("CASINO_HEIST_COMPLETION_REWARDS - Setting packed stat PACKED_STAT_BUGSTAR_OUTFITS")
			BREAK
			CASE CASINO_HEIST_OUTFIT_IN__MECHANIC
				SET_PACKED_STAT_BOOL(PACKED_STAT_MAINTENANCE_OUTFITS, TRUE)
				PRINTLN("CASINO_HEIST_COMPLETION_REWARDS - Setting packed stat PACKED_STAT_MAINTENANCE_OUTFITS")
			BREAK
			CASE CASINO_HEIST_OUTFIT_IN__GRUPPE_SECHS
				SET_PACKED_STAT_BOOL(PACKED_STAT_GRUPPE_SECHS_OUTFITS, TRUE)
				PRINTLN("CASINO_HEIST_COMPLETION_REWARDS - Setting packed stat PACKED_STAT_GRUPPE_SECHS_OUTFITS")
			BREAK
			CASE CASINO_HEIST_OUTFIT_IN__CELEBRITY
				SET_PACKED_STAT_BOOL(PACKED_STAT_YUNG_ANCESTOR_OUTFITS, TRUE)
				PRINTLN("CASINO_HEIST_COMPLETION_REWARDS - Setting packed stat PACKED_STAT_YUNG_ANCESTOR_OUTFITS")
			BREAK
		ENDSWITCH
		
		IF g_sCasinoHeistMissionConfigData.eSubterfugeOutfitsOut = CASINO_HEIST_OUTFIT_OUT__FIREMAN
			SET_PACKED_STAT_BOOL(PACKED_STAT_FIRE_FIGHTERS_OUTFITS, TRUE)
			PRINTLN("CASINO_HEIST_COMPLETION_REWARDS - Setting packed stat PACKED_STAT_FIRE_FIGHTERS_OUTFITS")
		ENDIF
	ELSE //Direct
		SWITCH GET_STYLE_FROM_OUTFIT(MC_playerBD[iLocalPart].iOutfit)
			CASE OUTFIT_STYLE_CASINO_HEIST_DIRECT_LIGHT_I
				SET_PACKED_STAT_BOOL(PACKED_STAT_DIRECT_LIGHT_1_OUTFITS, TRUE)
				PRINTLN("CASINO_HEIST_COMPLETION_REWARDS - Setting packed stat PACKED_STAT_DIRECT_LIGHT_1_OUTFITS")
			BREAK
			CASE OUTFIT_STYLE_CASINO_HEIST_DIRECT_LIGHT_II
				SET_PACKED_STAT_BOOL(PACKED_STAT_DIRECT_LIGHT_2_OUTFITS, TRUE)
				PRINTLN("CASINO_HEIST_COMPLETION_REWARDS - Setting packed stat PACKED_STAT_DIRECT_LIGHT_2_OUTFITS")
			BREAK
			CASE OUTFIT_STYLE_CASINO_HEIST_DIRECT_LIGHT_III
				SET_PACKED_STAT_BOOL(PACKED_STAT_DIRECT_LIGHT_3_OUTFITS, TRUE)
				PRINTLN("CASINO_HEIST_COMPLETION_REWARDS - Setting packed stat PACKED_STAT_DIRECT_LIGHT_3_OUTFITS")
			BREAK
			CASE OUTFIT_STYLE_CASINO_HEIST_DIRECT_HEAVY_I
				SET_PACKED_STAT_BOOL(PACKED_STAT_DIRECT_HEAVY_1_OUTFITS, TRUE)
				PRINTLN("CASINO_HEIST_COMPLETION_REWARDS - Setting packed stat PACKED_STAT_DIRECT_HEAVY_1_OUTFITS")
			BREAK
			CASE OUTFIT_STYLE_CASINO_HEIST_DIRECT_HEAVY_II
				SET_PACKED_STAT_BOOL(PACKED_STAT_DIRECT_HEAVY_2_OUTFITS, TRUE)
				PRINTLN("CASINO_HEIST_COMPLETION_REWARDS - Setting packed stat PACKED_STAT_DIRECT_HEAVY_2_OUTFITS")
			BREAK
			CASE OUTFIT_STYLE_CASINO_HEIST_DIRECT_HEAVY_III
				SET_PACKED_STAT_BOOL(PACKED_STAT_DIRECT_HEAVY_3_OUTFITS, TRUE)
				PRINTLN("CASINO_HEIST_COMPLETION_REWARDS - Setting packed stat PACKED_STAT_DIRECT_HEAVY_3_OUTFITS")
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC
#ENDIF

/// PURPOSE: Returns the total number of peds killed on mission via the iNumPedKills and iAmbientCopsKilled player broadcast data stats
FUNC INT GET_TOTAL_NUMBER_OF_PEDS_KILLED_DURING_MISSION()
	
	INT iPart
	
	INT iKills = 0
	
	FOR iPart = 0 TO (MAX_NUM_MC_PLAYERS - 1)
		iKills += (MC_playerBD[iPart].iNumPedKills + MC_playerBD[iPart].iAmbientCopsKilled)
	ENDFOR
	
	PRINTLN("[RCC MISSION] GET_TOTAL_NUMBER_OF_PEDS_KILLED_DURING_MISSION - Total number of peds killed = ", iKills)
	
	RETURN iKills
	
ENDFUNC

/// PURPOSE:
///    obtains veh_damage_percentage, veh_damage_time. No longer calculates the the averages of the 2 variables
///    number of vehicle tracked.
proc calculate_tracked_vehicle_damage_percentage_and_time(int ipart, float &average_veh_damage_percentage, float &average_veh_damage_time)

	int i = 0
	int total_tracked_vehicles = 0
	
	for i = 0 to (FMMC_MAX_VEHICLES - 1)
	
		IF IS_BIT_SET(g_FMMC_STRUCT.iHeistCSVehDmgTrackBitSet, i)
			
			total_tracked_vehicles++

		endif 
	
	endfor 
	
	//removed average calculations from veh_damage_percentage and average_veh_damage_time
	//decided to do this because on missions where there is 2 tracked vehicles at one stage of the mission 
	//then if one driver drives their car safely and the other drivers delivers their car badly then the player who
	//drives it badly will benefit under the average calculation. 
	// e.g. player 0 = 80% damage = 0.8 * 10000 = 8000  average cal = 80 / 2 = 40  -> 0.4 * 10000 = 4000
//			player 1 = 10% damage = 0.1 * 10000 = 1000  average cal = 10 / 2 = 5  -> 0.05 * 10000 =  500
//										   diff = 7000									     diff = 3500

	//if the bad driver scores better on another stat they could make back the 3500 on that stat and end up 
	//winning the total stat score. 
	
	average_veh_damage_percentage = (MC_playerBD[ipart].veh_damage_percentage / total_tracked_vehicles)
	
	average_veh_damage_time = (MC_playerBD[ipart].veh_damage_time / total_tracked_vehicles)
	
	PRINTLN("[LK MISSION] total_tracked_vehicles : ", total_tracked_vehicles)

	PRINTLN("[LK MISSION] MC_playerBD[iPartToUse].veh_damage_percentage : ", MC_playerBD[ipart].veh_damage_percentage)
	PRINTLN("[LK MISSION] average_veh_damage_time : ", average_veh_damage_time)
	
	PRINTLN("[LK MISSION] MC_playerBD[ipart].veh_damage_time : ", MC_playerBD[ipart].veh_damage_time)
	PRINTLN("[LK MISSION] average_veh_damage_time : ", average_veh_damage_time)

endproc 

// PURPOSE:
///    returns the total number of tracked vehicles in the mission. 
func int get_total_number_of_tracked_vehicles()

	int i = 0
	int total_tracked_vehicles = 0
	
	for i = 0 to (FMMC_MAX_VEHICLES - 1)
	
		IF IS_BIT_SET(g_FMMC_STRUCT.iHeistCSVehDmgTrackBitSet, i)
			
			total_tracked_vehicles++

		endif 
	
	endfor 
	
	PRINTLN("[LK MISSION] total_tracked_vehicles : ", total_tracked_vehicles)
	
	return total_tracked_vehicles

endfunc 

/// PURPOSE: calcualtes the heist stats scores. This is used to decide the medals each player will receive.
///    		Each machine will calculate the stats for all the sesion members themselves. 
///    		Therefore each machine will cycle through all the participants of the session and perform the stat 
///    		calculation for those ped 
///    
///    		stats calculates are:
///    		
FUNC INT GET_HEIST_COMPLETION_HIGH_SCORE(INT iPart)

	//grab tunable data from:
//		x:\gta5\script\dev_network\multiplayer\globals\MP_globals_Tunables.sch
//		x:\gta5\script\dev_network\multiplayer\include\public\net_script_tunables.sch
	
		//easier to read calcualtions using time_start_value instead of g_sMPTunables.time_start_value

	int time_start_value = g_sMPTunables.time_start_value
	float time_multiplier = g_sMPTunables.time_multiplier

	int kills_multiplier = g_sMPTunables.kills_multiplier
	int max_kills = g_sMPTunables.max_kills
	float ambient_cop_kills_multiplier = g_sMPTunables.ambient_cop_kills_multiplier
	int max_ambient_cop_kills = g_sMPTunables.max_ambient_cop_kills
	
	int head_shot_multiplier = g_sMPTunables.head_shot_multiplier
	int max_headshots = g_sMPTunables.max_headshots
	
	int safe_drive_time_multiplier = g_sMPTunables.safe_drive_time_multiplier
	float safe_drive_multiplier = to_float(g_fmmc_struct.iSafeDriveMultiplier)
	
	int accuracy_multiplier = g_sMPTunables.accuracy_multiplier
	
	int civillian_kills_start_value = g_sMPTunables.civillian_kills_start_value
	int civillian_kills_multiplier = g_sMPTunables.civillian_kills_multiplier
	int max_civillian_kills	= g_sMPTunables.max_civillian_kills
	
	int health_score_start_value = g_sMPTunables.health_score_start_value
	int health_score_multiplier_0 = g_sMPTunables.health_score_multiplier_0
	float health_score_multiplier_1 = g_sMPTunables.health_score_multiplier_1
	
	int hack_score_start_value = g_sMPTunables.hack_score_start_value
	int hack_score_multiplier = g_sMPTunables.hack_score_multiplier
	int max_hacks = g_sMPTunables.max_hacks
	
	int max_wanted_level = g_sMPTunables.max_wanted_level
	int wanted_level_multiplier = g_sMPTunables.wanted_level_multiplier
	float wanted_level_time_multiplier = g_sMPTunables.wanted_level_time_multiplier
	
	int objective_max_team_players = g_sMPTunables.objective_max_team_players
	int objective_multiplier = g_sMPTunables.objective_multiplier

	INT iHighScore
	
	
	// For sc leaderboard write.
	iTotalShotsFired = (MC_playerBD[iPart].iLatestShots - MC_playerBD[iPart].iStartShots)
	iTotalShotsHit = (MC_playerBD[iPart].iLatestHits - MC_playerBD[iPart].iStartHits)
	
	
	PRINTLN("[RCC MISSION] HIGH SCORE - getting heist completion highscore for participant: ",iPart )
	
	//time score
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS,ciFMMC_IGNORE_SCORE_TIME)
	
		int time_score
		
		float time_in_seconds = TO_FLOAT(MC_serverBD.iTotalMissionEndTime/1000)
		
		time_score = (time_start_value - round(time_in_seconds * time_multiplier))

		iHighScore += time_score
		
		PRINTLN("[RCC MISSION] time_score = ", time_score)
		PRINTLN("[RCC MISSION] time iHighScore = ", iHighScore)
		
	ENDIF
	
	//enemy score
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS,ciFMMC_IGNORE_SCORE_ENEMIES)
		
		int ped_kills
		int ambient_cop_kills
		int ped_kills_score 
		int ambient_cop_kills_score
		
		ped_kills = clamp_int(MC_Playerbd[iPart].iNumPedKills, 0, max_kills)
		ambient_cop_kills = clamp_int(MC_playerBD[iPart].iAmbientCopsKilled, 0, max_ambient_cop_kills)
		
		ped_kills_score = (ped_kills * kills_multiplier)
		ambient_cop_kills_score = round((ambient_cop_kills * (kills_multiplier * ambient_cop_kills_multiplier)))
		
		iHighScore += (ped_kills_score + ambient_cop_kills_score)
		
		PRINTLN("[RCC MISSION] ped_kills = ", ped_kills)
		PRINTLN("[RCC MISSION] cop_kills = ", ambient_cop_kills)
		PRINTLN("[RCC MISSION] ped_kills score = ", ped_kills_score)
		PRINTLN("[RCC MISSION] cop_kills score = ", ambient_cop_kills_score)
		PRINTLN("[RCC MISSION] enemy iHighScore = ", iHighScore)

	ENDIF

	//headshots score 
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS,ciFMMC_IGNORE_SCORE_HEADSHOTS)
	
		int headshots 
		int headshot_score 
		
		headshots = clamp_int(MC_Playerbd[iPart].iNumHeadshots, 0, max_headshots)
		
		headshot_score = (headshots * head_shot_multiplier)

		iHighScore += headshot_score 
		
		PRINTLN("[RCC MISSION] headshots = ", headshots)
		PRINTLN("[RCC MISSION] headshot_score = ", headshot_score)
		PRINTLN("[RCC MISSION] iHighScore enemy headshots = ", iHighScore)

	ENDIF
	
//	safe drive score

//	specific vehicles healths percentage and damage time is tracked within process_veh_brain() and is only updated 
//	if the player is the driver. cache the time the player has been in the vehicle so when we do the calculation 
//	we favour the player who has been in the car the longest if both players have the same damage percentage. 
//	total MC_playerBD[iLocalPart].veh_damage_percentage and MC_playerBD[iLocalPart].veh_damage_time calculated
//	within PROCESS_VEH_BRAIN(INT iveh) 
	
	float drive_time_score 
	float vehicle_damage_score
	float vehicles_health_percentage_score
	
	//calculate_tracked_vehicle_damage_percentage_and_time(iPart, average_veh_damage_percentage, average_veh_damage_time)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS, ciFMMC_IGNORE_SCORE_SAFE_DRIVE)
		drive_time_score = (MC_playerBD[ipart].veh_damage_time * safe_drive_time_multiplier)
		
		println("[RCC MISSION] drive_time_score before clamping = ", drive_time_score)
		
		drive_time_score = clamp(drive_time_score, 0, safe_drive_multiplier) 
		
		if drive_time_score > 0
		
		
			vehicle_damage_score = ((MC_playerBD[ipart].veh_damage_percentage / 100) * safe_drive_multiplier)

			vehicles_health_percentage_score = (drive_time_score - vehicle_damage_score)
		
		endif 
	ENDIF
	
	iHighScore += round(vehicles_health_percentage_score)

	
	get_total_number_of_tracked_vehicles() //find out how many tracked vehicles there are on mission. 

	PRINTLN("[RCC MISSION] total veh_damage_time = ", MC_playerBD[iPart].veh_damage_time) 
	PRINTLN("[RCC MISSION] drive_time_score = ", drive_time_score)
	PRINTLN("[RCC MISSION] g_fmmc_struct.iSafeDriveMultiplier =", g_fmmc_struct.iSafeDriveMultiplier)
	PRINTLN("[RCC MISSION] MC_playerBD[ipart].veh_damage_percentage = ", MC_playerBD[ipart].veh_damage_percentage)
	PRINTLN("[RCC MISSION] (MC_playerBD[ipart].veh_damage_percentage / 100) = ", (MC_playerBD[ipart].veh_damage_percentage / 100))
	PRINTLN("[RCC MISSION] damage score = ", vehicle_damage_score)
	PRINTLN("[RCC MISSION] vehicles_health_percentage_score", vehicles_health_percentage_score)
	PRINTLN("[RCC MISSION] iHighScore vehicles_health_percentage_score = ", iHighScore)
	

	//accuracy score 
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS, ciFMMC_IGNORE_SCORE_ACCURACY)
	
		float player_accuracy
		float player_kill_percentage
		float player_accuracy_score 
		float total_number_of_mission_peds_killed_on_mission

		INT iEndHits = MC_playerBD[iPart].iLatestHits
		INT iEndShots = MC_playerBD[iPart].iLatestShots
		
		player_accuracy = (to_float(iEndHits - MC_playerBD[iPart].iStartHits) / TO_FLOAT(iEndShots - MC_playerBD[iPart].iStartShots))
		
		//incase get_total_number_of_mission_peds_killed_on_mission() returns 0 clamp it to a min of 1
		//mission where you don't need to kill anyone
		total_number_of_mission_peds_killed_on_mission = to_float(clamp_int(GET_TOTAL_NUMBER_OF_PEDS_KILLED_DURING_MISSION(), 1, max_kills))
	
		player_kill_percentage = (to_float(MC_Playerbd[iPart].iNumPedKills + MC_playerBD[iPart].iAmbientCopsKilled) / total_number_of_mission_peds_killed_on_mission)
		
		//incase get_total_number_of_mission_peds_killed_on_mission() = 0.0 this will cause player_kill_percentage
		//to be 0 and therefore player_accuracy_score calculation will be 0.0
		//clamp(player_kill_percentage, 0.05, 1.0)
		clamp(player_kill_percentage, 0.05, 1.0)
		
		player_accuracy_score = player_accuracy * accuracy_multiplier * player_kill_percentage
		
		iHighScore += round(player_accuracy_score)
		

		PRINTLN("[RCC MISSION] iStartShots: ", MC_playerBD[iPart].iStartShots)
		PRINTLN("[RCC MISSION] iEndShots: ", iEndShots)
		
		PRINTLN("[RCC MISSION] iStartHits: ", MC_playerBD[iPart].iStartHits)
		PRINTLN("[RCC MISSION] iEndHits: ", iEndHits)

		PRINTLN("[RCC MISSION] player_accuracy: ", player_accuracy)
		PRINTLN("[RCC MISSION] MC_Playerbd[iPart].iNumPedKills", MC_Playerbd[iPart].iNumPedKills) 
		PRINTLN("[RCC MISSION] player_kill_percentage: ", player_kill_percentage)
		PRINTLN("[RCC MISSION] player_accuracy_score: ", player_accuracy_score)
		PRINTLN("[RCC MISSION] iHighScore accuracy  = ", iHighScore)
		
	ENDIF
	
	
	//civillian kills score
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS, ciFMMC_IGNORE_SCORE_CIVILIANS)
	
		int civillian_score
	
		civillian_score = (civillian_kills_start_value - (clamp_int(MC_playerBD[iPart].iCivilianKills, 0, max_civillian_kills) * civillian_kills_multiplier))
	
		iHighScore += civillian_score
		
		PRINTLN("[RCC MISSION] civillian_score: ", civillian_score)
		PRINTLN("[RCC MISSION] iHighScore civillians  = ", iHighScore)
	
	ENDIF
	
	//health score 
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS,ciFMMC_IGNORE_SCORE_HEALTH)
	
		participant_index particapant_ped
		player_index mp_player_index 
		ped_index mp_player_ped_index
		int health_score
		int health_lost 

		//gets the participant ped_index from ipart. This could be the local player of another session player
		
		particapant_ped = INT_TO_PARTICIPANTINDEX(ipart)
		
		mp_player_index = NETWORK_GET_PLAYER_INDEX(particapant_ped)
		
		mp_player_ped_index = get_player_ped(mp_player_index)
		
		if not is_ped_injured(mp_player_ped_index)
			health_lost = GET_PED_MAX_HEALTH(mp_player_ped_index) - GET_ENTITY_HEALTH(mp_player_ped_index)
		else 
			PRINTLN("[RCC MISSION] mp_player_ped_index injured or does not exist = ")
		endif 
			
		health_score = ((health_score_start_value - (MC_PlayerBD[ipart].iNumPlayerDeaths * health_score_multiplier_0)) - (round(health_score_multiplier_1 * health_lost)))
	
		iHighScore += health_score
		
		PRINTLN("[RCC MISSION] health_score_start_value", health_score_start_value)
		PRINTLN("MC_PlayerBD[ipart].iNumPlayerDeaths", MC_PlayerBD[ipart].iNumPlayerDeaths)
		PRINTLN("health_lost", health_lost)
		PRINTLN("[RCC MISSION] health_score = ", health_score)
		PRINTLN("[RCC MISSION] iHighScore health score = ", iHighScore)

	ENDIF
	
	
	//hack speed score
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS,ciFMMC_IGNORE_SCORE_HACK_SPEED)
		
		IF MC_playerBD[iPart].iNumHacks > 0

			int hack_speed_score
			int average_hack_time

			//convert into seconds 
			average_hack_time = ((MC_PlayerBD[iPart].iHackTime / MC_PlayerBD[iPart].inumHacks) / 1000)
 
			hack_speed_score = (hack_score_start_value - average_hack_time) + (clamp_int(MC_playerBD[iPart].iNumHacks, 0, max_hacks) * hack_score_multiplier) 
		
			//check this 1000 - 20
			
			iHighScore += hack_speed_score
			
			PRINTLN("[RCC MISSION] average_hack_time = ", average_hack_time)
			PRINTLN("[RCC MISSION] MC_PlayerBD[iPart].inumHacks = ", MC_PlayerBD[iPart].inumHacks)
			PRINTLN("[RCC MISSION] hack_speed_score = ", hack_speed_score)
			PRINTLN("[RCC MISSION] iHighScore hack_speed_score = ", iHighScore)
			
		else 
		
			PRINTLN("[RCC MISSION] MC_playerBD[iPart].iNumHacks <= 0 - MC_playerBD[iPart].iNumHacks = ", MC_playerBD[iPart].iNumHacks)
		
		endif 
		
	else 
	
		PRINTLN("[RCC MISSION] ciFMMC_IGNORE_SCORE_HACK_SPEED")
	
	endif 

			
	//wanted level evade score 
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS,ciFMMC_IGNORE_SCORE_WANTED_EVADE_SPEED)
		IF MC_playerBD[iPart].iNumWantedLose > 0
		
			int wanted_level_evade_score 
			float average_wanted_lose_time
			
			average_wanted_lose_time = TO_FLOAT(MC_playerBD[iPart].iWantedTime) / TO_FLOAT(MC_playerBD[iPart].iNumWantedLose)
			average_wanted_lose_time = (average_wanted_lose_time / 1000)

			wanted_level_evade_score = ((clamp_int(MC_playerBD[iPart].iNumWantedLose, 0, max_wanted_level) * wanted_level_multiplier) - (round(wanted_level_time_multiplier * average_wanted_lose_time)))
			
			iHighScore += wanted_level_evade_score
			
			PRINTLN("[RCC MISSION]  average_wanted_lose_time = ", average_wanted_lose_time)
			PRINTLN("[RCC MISSION] wanted_level_evade_score = ", wanted_level_evade_score) 
			PRINTLN("[RCC MISSION] iHighScore wanted_level_evade_score = ", iHighScore)

		ENDIF
		
	ELSE
		PRINTLN("[RCC MISSION] g_FMMC_STRUCT.iPointAwardsBS,ciFMMC_IGNORE_SCORE_WANTED_EVADE_SPEED is set - not calculating wanted lose time stat")
	ENDIF
	

	
	//objective_complete_score
	float objective_completed_score 
	
	int team = MC_playerBD[iPart].iteam
	
	//objective_completed_score = ((to_float(MC_playerBD[iPart].medal_objective_completed_count) / to_float(clamp_int(MC_serverBD.iNumberOfPlayingPlayers[team], 0, objective_max_team_players))) * objective_multiplier)
	objective_completed_score = ((to_float(MC_playerBD[iPart].medal_objective_completed_count) / to_float(clamp_int(MC_serverBD.iNumberOfLBPlayers[team], 0, objective_max_team_players))) * objective_multiplier)
	
	iHighScore += round(objective_completed_score)
		
	PRINTLN("[RCC MISSION] objective team = ", team)
	PRINTLN("[RCC MISSION] objective_completed_count = ", MC_playerBD[iPart].medal_objective_completed_count)
	PRINTLN("[RCC MISSION] objective MC_serverBD.iNumberOfLBPlayers[team] = ", MC_serverBD.iNumberOfLBPlayers[team])
	PRINTLN("[RCC MISSION] objective MC_serverBD.iNumberOfPlayingPlayers[team] NOT USED ANY MORE = ", MC_serverBD.iNumberOfPlayingPlayers[team])
	PRINTLN("[RCC MISSION] objective value clamp MC_serverBD.iNumberOfLBPlayers[team] = ", clamp_int(MC_serverBD.iNumberOfLBPlayers[team], 0, objective_max_team_players))
	PRINTLN("[RCC MISSION] g_sMPTunables.objective_multiplier = ", g_sMPTunables.objective_multiplier)
	PRINTLN("[RCC MISSION] first part of calculation = ", (MC_playerBD[iPart].medal_objective_completed_count / clamp_int(MC_serverBD.iNumberOfLBPlayers[team], 0, objective_max_team_players)))
	PRINTLN("[RCC MISSION] objective_completed_score float = ", objective_completed_score)
	PRINTLN("[RCC MISSION] objective_completed_score rounded = ", round(objective_completed_score))
	PRINTLN("[RCC MISSION] objective iHighScore = ", iHighScore) 
	
	IF g_sMPtunables.iGLOBAL_LEADERBOARD_CAP_HEISTS != 0
		PRINTLN("[RCC MISSION] CLAMPING")
		CLAMP_INT(iHighScore, 0, g_sMPtunables.iGLOBAL_LEADERBOARD_CAP_HEISTS)
	ENDIF
	
	PRINTLN("[RCC MISSION] STATS FINISHED iHighScore = ", iHighScore)
	
	RETURN iHighScore

ENDFUNC

struct heist_completion_rankings_struct
		
	bool participant_on_mission
	int heist_participant_index
	int heist_participant_score
	
endstruct 

proc initialise_heist_completion_rankings(heist_completion_rankings_struct &heist_completion_data[])

	int i = 0
	
	for i = 0 to count_of(heist_completion_data) - 1
	
		heist_completion_data[i].participant_on_mission = false
		heist_completion_data[i].heist_participant_index = -1
		heist_completion_data[i].heist_participant_score = 0
		
	endfor 
	
endproc 

/// PURPOSE: sorts the heist completion score rankings into decending order from first place to last. 
///    		 The sort will exclude any participant who never finished the mission.  
///    		 Final data will return
///    
///    			- participant_on_mission
//				- heist_participant_index
//				- heist_participant_score
proc heist_completion_score_rankings_bubble_sort(heist_completion_rankings_struct &heist_completion_data[])

	int i 
	int k
	int temp_participant 
	int temp_score
	

	for i = 0 to count_of(heist_completion_data) - 1
	
		for k = 0 to count_of(heist_completion_data) - 2  
		
			//we must ensure that both indexes are participants ON MISSION
			//we dont want to swap a participant on mission with a participant NOT on mission
			//e.g. swapping score -50 with 0 if 0 is the default score of a participant NOT on mission. Initialised via initialise_heist_completion_rankings 
			
			if heist_completion_data[k].participant_on_mission
			and heist_completion_data[k+1].participant_on_mission 

				if heist_completion_data[k].heist_participant_score < heist_completion_data[k+1].heist_participant_score

					temp_participant = heist_completion_data[k].heist_participant_index
					temp_score = heist_completion_data[k].heist_participant_score
					
					heist_completion_data[k].heist_participant_index = heist_completion_data[k+1].heist_participant_index
					heist_completion_data[k].heist_participant_score = heist_completion_data[k+1].heist_participant_score
					
					heist_completion_data[k+1].heist_participant_index = temp_participant
					heist_completion_data[k+1].heist_participant_score = temp_score
					
				endif
				
			endif 
			
		endfor 
		
	endfor 
	
	for i = 0 to count_of(heist_completion_data) - 1
	
		printstring("[RCC MISSION] heist_completion_score_rankings_bubble_sort participant index = ")
		printint(heist_completion_data[i].heist_participant_index)
		printnl()
		
		printstring("[RCC MISSION] heist_completion_score_rankings_bubble_sort = ")
		printint(heist_completion_data[i].heist_participant_score)
		printnl()
	
	endfor 
	
endproc 

///    Checks through all participant scores and stores the participants who came first/second/third in iHCS_FirstPart/iHCS_SecondPart/iHCS_ThirdPart
PROC GET_HEIST_COMPLETION_RANKINGS()
	
	if first_place_participant = -1

		int iLoopParticipant
		int on_mission_count = 0
		
		heist_completion_rankings_struct heist_completion_score_data[MAX_NUM_MC_PLAYERS]
		
		initialise_heist_completion_rankings(heist_completion_score_data)
		
		
		for iloopparticipant = 0 to (max_num_mc_players - 1)
			
			println("[rcc mission] get_heist_completion_rankings - checking participant ",iloopparticipant)
			
			if not is_string_null_or_empty(mc_serverbd_2.tparticipantnames[iloopparticipant])
			and network_is_participant_active(int_to_participantindex(iloopparticipant))
				
				heist_completion_score_data[on_mission_count].participant_on_mission = true
				heist_completion_score_data[on_mission_count].heist_participant_index = iloopparticipant
				IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
					heist_completion_score_data[on_mission_count].heist_participant_score = MC_playerBD[iloopparticipant].iRowanCSuperHighScore
				ELSE
					heist_completion_score_data[on_mission_count].heist_participant_score = get_heist_completion_high_score(iloopparticipant)
				ENDIF
				on_mission_count++
		
			endif 
			
		endfor
		
		heist_completion_score_rankings_bubble_sort(heist_completion_score_data)
		
		first_place_participant = heist_completion_score_data[0].heist_participant_index
		second_place_participant = heist_completion_score_data[1].heist_participant_index
		third_place_participant = heist_completion_score_data[2].heist_participant_index
		forth_place_participant = heist_completion_score_data[3].heist_participant_index

		PRINTLN("[RCC MISSION] GET_HEIST_COMPLETION_RANKINGS - first_place_participant platinum  = ", first_place_participant)
		PRINTLN("[RCC MISSION] GET_HEIST_COMPLETION_RANKINGS - second_place_participant gold  = ", second_place_participant)
		PRINTLN("[RCC MISSION] GET_HEIST_COMPLETION_RANKINGS - third_place_participant silver  = ", third_place_participant)
		PRINTLN("[RCC MISSION] GET_HEIST_COMPLETION_RANKINGS - forth_place_participant bronze  = ", forth_place_participant)
	
	ENDIF
	
ENDPROC

FUNC INT GET_HEIST_COMPLETION_RATING(INT iPart)
	
	IF (first_place_participant = -1)
	
		GET_HEIST_COMPLETION_RANKINGS()
	
	ENDIF
	
	PRINTLN("[RCC MISSION] GET_HEIST_COMPLETION_RATING - participant = ", iPart)
	
	if iPart = first_place_participant
		
		PRINTLN("[RCC MISSION] GET_HEIST_COMPLETION_RATING - first_place_participant platinum")

		return ci_HEISTPLYRRATING_PLATINUM
			
	elif iPart = second_place_participant
		
		PRINTLN("[RCC MISSION] GET_HEIST_COMPLETION_RATING - second_place_participant gold")
		
		return ci_HEISTPLYRRATING_GOLD
		
	elif iPart = third_place_participant
		
		PRINTLN("[RCC MISSION] GET_HEIST_COMPLETION_RATING - third_place_participant silver")
		
		return ci_HEISTPLYRRATING_SILVER 
		
	elif iPart = forth_place_participant
		
		PRINTLN("[RCC MISSION] GET_HEIST_COMPLETION_RATING - forth_place_participant bronze")
		
		return ci_HEISTPLYRRATING_BRONZE
		
	else
		
		PRINTLN("[RCC MISSION] GET_HEIST_COMPLETION_RATING - 5th place and above BAD")
		
		return ci_HEISTPLYRRATING_BAD
			
	endif 

ENDFUNC

FUNC STRING GET_HEIST_COMPLETION_RATING_STRING(INT iPart, HUD_COLOURS eHUDColour = HUD_COLOUR_PURE_WHITE, BOOL bUseHudColour = FALSE)

	STRING sRating
	
	IF (first_place_participant = -1)
	
		GET_HEIST_COMPLETION_RANKINGS()
	
	ENDIF
	
	if iPart = first_place_participant

		sRating = "CELEB_PLATINUM"
		
	elif iPart = second_place_participant

		sRating = "CELEB_GOLD"
		
	elif iPart = third_place_participant
		
		sRating = "CELEB_SILVER"
		
	elif iPart = forth_place_participant 
		
		sRating = "CELEB_BRONZE"
	
	else
		
		sRating = "CELEB_NONE"

	endif 
	
	IF bUseHudColour
		SWITCH eHUDColour
		
			CASE HUD_COLOUR_PLATINUM
				sRating = "CELEB_PLATINUM"
			BREAK 
			
			CASE HUD_COLOUR_GOLD 
				sRating = "CELEB_GOLD" 
			BREAK
			
			CASE HUD_COLOUR_SILVER 
				sRating = "CELEB_SILVER"
			BREAK
			
			CASE HUD_COLOUR_BRONZE 
				sRating = "CELEB_BRONZE" 
			BREAK
			
			DEFAULT 
				sRating = "CELEB_NONE" 
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN sRating

ENDFUNC

FUNC STRING GET_CURRENT_STRAND_STRING()
	SWITCH(iCurrentHeistMissionIndex)
	
		CASE HBCA_BS_PACIFIC_STANDARD_FINALE2	RETURN "PACIFIC"
		CASE HBCA_BS_HUMANE_LABS_FINALE			RETURN "HUMANE"
		CASE HBCA_BS_PRISON_BREAK_FINALE		RETURN "PRISON"
		CASE HBCA_BS_SERIES_A_FINALE			RETURN "FUNDING"
		CASE HBCA_BS_TUTORIAL_FINALE			RETURN "FLEECA"
	
	ENDSWITCH

	RETURN ""
ENDFUNC

PROC HANDLE_GIVING_ELITE_REWARD(INT iEliteCashGained)
	IF iEliteCashGained > 0
		TEXT_LABEL_23 sChallenge = "ELITE_"
		sChallenge += GET_CURRENT_STRAND_STRING()
		INT iScriptTransactionIndex
		IF USE_SERVER_TRANSACTIONS()
			TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_JOB_BONUS,iEliteCashGained, iScriptTransactionIndex,FALSE,TRUE)
			g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl31ContenID = g_FMMC_STRUCT.tl31LoadedContentID
			g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl23Challenge = sChallenge
		ELSE
			NETWORK_EARN_FROM_JOB_BONUS(iEliteCashGained,g_FMMC_STRUCT.tl31LoadedContentID,sChallenge)
		ENDIF
	ENDIF
ENDPROC

FUNC GANGOPS_ELITE_TYPE GET_GANGOPS_ELITE_CHALLENGE_TYPE()
	IF GANGOPS_FLOW_GET_CURRENT_GANGOPS_MISSION() = ciGANGOPS_FLOW_MISSION_IAABASE_FINALE
		RETURN GANGOPS_ELITE_XM_BASE
	ELIF GANGOPS_FLOW_GET_CURRENT_GANGOPS_MISSION() = ciGANGOPS_FLOW_MISSION_SUBMARINE_FINALE
		RETURN GANGOPS_ELITE_XM_SUBMARINE
	ELIF GANGOPS_FLOW_GET_CURRENT_GANGOPS_MISSION() = ciGANGOPS_FLOW_MISSION_MISSILE_SILO_FINALE
	OR GANGOPS_FLOW_GET_CURRENT_GANGOPS_MISSION() = ciGANGOPS_FLOW_MISSION_MISSILE_SILO_FINALE_P2
		RETURN GANGOPS_ELITE_XM_SILO
	ENDIF
	ASSERTLN("[JS] GET_GANGOPS_ELITE_CHALLENGE_TYPE - Invalid Mission")
	RETURN GANGOPS_ELITE_XM_BASE
ENDFUNC

PROC HANDLE_GIVING_GANGOPS_ELITE_REWARD(INT iEliteCashGained, GANGOPS_ELITE_TYPE eMission)
	IF iEliteCashGained > 0
		INT iScriptTransactionIndex
		IF USE_SERVER_TRANSACTIONS()
			TRANSACTION_SERVICES eService
			SWITCH eMission
				CASE GANGOPS_ELITE_XM_BASE
					eService = SERVICE_EARN_GANGOPS_ELITE_XM_BASE
				BREAK
				CASE GANGOPS_ELITE_XM_SUBMARINE
					eService = SERVICE_EARN_GANGOPS_ELITE_XM_SUBMARINE
				BREAK
				CASE GANGOPS_ELITE_XM_SILO
					eService = SERVICE_EARN_GANGOPS_ELITE_XM_SILO
				BREAK
			ENDSWITCH
			TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(eService, iEliteCashGained, iScriptTransactionIndex, FALSE, TRUE)
			g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl31ContenID = g_FMMC_STRUCT.tl31LoadedContentID
		ELSE
			NETWORK_EARN_GANGOPS_ELITE(iEliteCashGained, g_FMMC_STRUCT.tl31LoadedContentID, eMission)
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_GIVING_CASINO_HEIST_ELITE_REWARD(INT iEliteCashGained)
	IF iEliteCashGained > 0
		INT iScriptTransactionIndex
		IF USE_SERVER_TRANSACTIONS()
			TRANSACTION_SERVICES eService
			IF g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__STEALTH
				eService = SERVICE_EARN_CASINO_HEIST_ELITE_STEALTH
			ELIF g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
				eService = SERVICE_EARN_CASINO_HEIST_ELITE_SUBTERFUGE
			ELIF g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__DIRECT
				eService = SERVICE_EARN_CASINO_HEIST_ELITE_DIRECT
			ENDIF
			TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(eService, iEliteCashGained, iScriptTransactionIndex, FALSE, TRUE)
			g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl31ContenID = g_FMMC_STRUCT.tl31LoadedContentID
		ELSE
			NETWORK_EARN_CASINO_HEIST_AWARDS(iEliteCashGained, 0, 0, 1, iEliteCashGained)
		ENDIF
	ENDIF
ENDPROC

FUNC HUD_COLOURS GET_HEIST_RATING_HUD_COLOUR(INT iParticipant)
	
	INT iRating = GET_HEIST_COMPLETION_RATING(iParticipant)
	
	SWITCH iRating
	
		CASE ci_HEISTPLYRRATING_PLATINUM
			RETURN HUD_COLOUR_PLATINUM
		BREAK 
	
		CASE ci_HEISTPLYRRATING_GOLD
			RETURN HUD_COLOUR_GOLD
		BREAK
		CASE ci_HEISTPLYRRATING_SILVER
			RETURN HUD_COLOUR_SILVER
		BREAK
		CASE ci_HEISTPLYRRATING_BRONZE
			RETURN HUD_COLOUR_BRONZE
		BREAK
		CASE ci_HEISTPLYRRATING_BAD
			RETURN HUD_COLOUR_WHITE
		BREAK
	ENDSWITCH
	
	RETURN HUD_COLOUR_BLACK
	
ENDFUNC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: CASH, XP AND RP !
//
//************************************************************************************************************************************************************



//The position in the leaderboard
INT iRoundsLBPosition = -1
INT iRoundsSuicides

FUNC BOOL ALL_TEAM_ONLY_DID_HEADSHOTS(INT iteam)

INT iparticipant
PARTICIPANT_INDEX tempPart
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		tempPart = INT_TO_PARTICIPANTINDEX(iParticipant)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			IF MC_PlayerBD[iParticipant].iteam = iteam
				IF NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet,PBBOOL_ALL_HEADSHOTS)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
	
ENDFUNC

FUNC INT GIVE_ALL_ENEMY_KILL_XP()

INT iParticipant
INT iTotalKills
INT iped
INT iTotalxpreward,ixpKillreward,ixpHeadshotreward,ixpTHeadshotreward
BOOL ballkill = TRUE
FLOAT fXPBeforeRound


	FOR iped = 0 TO (FMMC_MAX_PEDS-1)
		IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iTeam[MC_playerBD[iPartToUse].iteam] = ciPED_RELATION_SHIP_HATE
				PRINTLN("Hostile ped still alive : ",iped," for team : ",MC_PlayerBD[iPartToUse].iteam) 
				ballkill = FALSE
			ENDIF
		ENDIF
	ENDFOR
	
	IF ballkill
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
			IF MC_PlayerBD[iPartToUse].iteam = MC_PlayerBD[iParticipant].iteam
				
				iTotalKills = iTotalKills + MC_PlayerBD[iParticipant].iNumPedKills
				
				IF iTotalKills >= g_sMPTunables.iMISSION_AI_KILL_AMOUNT_CAP
					iTotalKills = g_sMPTunables.iMISSION_AI_KILL_AMOUNT_CAP
				ENDIF
				
				IF Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer)
					IF iTotalKills > 20
						iTotalKills = 20
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT

		PRINTLN("total kills : ",iTotalKills," for team : ",MC_PlayerBD[iPartToUse].iteam)
		
		IF iTotalKills > 0
			ixpKillreward = iTotalKills*10
			PRINTLN("ixpKillreward before tune: ",ixpKillreward) 
			PRINTLN("all kills tunable value: ",g_sMPTunables.fxp_tunable_Kill_all_enemies_on_a_mission) 
			fXPBeforeRound = (ixpKillreward*g_sMPTunables.fxp_tunable_Kill_all_enemies_on_a_mission)  
			ixpKillreward = ROUND(fXPBeforeRound)   
			#IF IS_DEBUG_BUILD
				iAllKillXPBonus = ixpKillreward
			#ENDIF
			PRINTLN("total kills XP : ",ixpKillreward)
		ENDIF
	ENDIF
	
	IF g_MissionControllerserverBD_LB.sleaderboard[iPartToUse].iKills >= 5
		IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_ALL_HEADSHOTS)
			INT iLBkills = g_MissionControllerserverBD_LB.sleaderboard[iPartToUse].iKills
			IF iLBkills >= g_sMPTunables.iMISSION_AI_KILL_AMOUNT_CAP
				iLBkills = g_sMPTunables.iMISSION_AI_KILL_AMOUNT_CAP
			ENDIF
			IF Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer)
				IF iLBkills > 20
					iLBkills = 20
				ENDIF
			ENDIF
			ixpHeadshotreward = ixpHeadshotreward + (iLBkills*20)
			PRINTLN("giving all headshot player bonus XP: ",ixpHeadshotreward) 
			PRINTLN("all headshot individual tunable: ",g_sMPTunables.fxp_tunable_Only_kill_enemies_with_headshots_Individual) 
			fXPBeforeRound = (ixpHeadshotreward*g_sMPTunables.fxp_tunable_Only_kill_enemies_with_headshots_Individual)
			ixpHeadshotreward = ROUND(fXPBeforeRound)
			#IF IS_DEBUG_BUILD
				iAllHeadshotXPPlayer = (ixpHeadshotreward)
			#ENDIF
		ENDIF
		IF ALL_TEAM_ONLY_DID_HEADSHOTS(MC_playerBD[iPartToUse].iteam)
			INT iLBTeamkills =MC_serverBD.iTeamKills[MC_playerBD[iPartToUse].iteam]
			IF iLBTeamkills >= g_sMPTunables.iMISSION_AI_KILL_AMOUNT_CAP
				iLBTeamkills = g_sMPTunables.iMISSION_AI_KILL_AMOUNT_CAP
			ENDIF
			IF Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer)
				IF iLBTeamkills > 20
					iLBTeamkills = 20
				ENDIF
			ENDIF
			ixpTHeadshotreward = ixpTHeadshotreward + (iLBTeamkills*10)
			PRINTLN("giving all headshot team bonus XP: ",ixpTHeadshotreward) 
			PRINTLN("all headshot individual tunable: ",g_sMPTunables.fxp_tunable_Only_kill_enemies_with_headshots_Team) 
			fXPBeforeRound =(ixpTHeadshotreward*g_sMPTunables.fxp_tunable_Only_kill_enemies_with_headshots_Team) 
			ixpTHeadshotreward = ROUND(fXPBeforeRound)
			#IF IS_DEBUG_BUILD
				iAllHeadshotXPTeam = ixpTHeadshotreward
			#ENDIF
		ENDIF
	ENDIF
	iTotalxpreward = (ixpKillreward+ixpHeadshotreward+ixpTHeadshotreward)
	
	PRINTLN("iTotalxpreward: ",iTotalxpreward) 
	
	IF iTotalxpreward > 0
		IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
			GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD,"REWXP_MKILLS",XPTYPE_ACTION, XPCATEGORY_ACTION_KILLS, iTotalxpreward)
		ENDIF
	ENDIF
	
	RETURN iTotalxpreward

ENDFUNC

FUNC INT GET_LIVES_XP_BONUS()

	INT ixpAdded
	FLOAT fLivesXP
	
	IF  MC_serverBD.iTeamDeaths[MC_playerBD[iPartToUse].iteam] = 0
		PRINTLN("g_sMPTunables.fxp_tunable_Not_losing_any_lives_bonus",g_sMPTunables.fxp_tunable_Not_losing_any_lives_bonus)
		fLivesXP = 200*g_sMPTunables.fxp_tunable_Not_losing_any_lives_bonus   
		ixpAdded = ROUND(fLivesXP)
		PRINTLN("ixpAdded for lives: ",ixpAdded)
	ENDIF


	RETURN ixpAdded
	
ENDFUNC



PROC GIVE_MISSION_CASH_REWARDS(INT icash, BOOL bPass)
GAMER_HANDLE player_handle 
	INT iCachedReward = 0
	
	IF icash > 0
		GIVE_LOCAL_PLAYER_FM_CASH_END_OF_MISSION(icash,1,TRUE,0)
			
		iCachedReward = (MC_playerBD[iLocalPart].iCashReward + icash)
		
		FLOAT fCashMulti = TO_FLOAT(g_iCachedMatchBonus) * g_sMPTunables.cashMultiplier
		INT iCachedValRounded = ROUND(fCashMulti)
		PRINTLN("[RCC_MISSION] [CASH] - GIVE_MISSION_CASH_REWARDS iCachedValRounded ", iCachedValRounded)		
		
		MC_playerBD[iLocalPart].iCashReward = iCachedReward - iCachedValRounded
		
		ADD_TO_JOB_CASH_EARNINGS(icash)
		PRINTLN("[RCC_MISSION] [CASH] icash = ", icash)
		PRINTLN("[RCC_MISSION] [CASH] iCashReward = ", MC_playerBD[iLocalPart].iCashReward )
		PRINTLN("[RCC_MISSION] [CASH] g_iCachedMatchBonus = ", g_iCachedMatchBonus)
		PRINTLN("[RCC_MISSION] [CASH] iCachedReward = ", iCachedReward)
	ENDIF
	IF MC_playerBD[iPartToUse].iCashReward > 0
		PRINTLN("[RCC_MISSION] [CASH] AFTER iCashReward = ", MC_playerBD[iLocalPart].iCashReward, "  g_FMMC_STRUCT.tl31LoadedContentID = ", g_FMMC_STRUCT.tl31LoadedContentID)
		INT iScriptTransactionIndex
		IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
		OR Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
			IF USE_SERVER_TRANSACTIONS()
				TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_BEND_JOB, iCachedReward, iScriptTransactionIndex, DEFAULT, DEFAULT)
				g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl31ContenID = g_FMMC_STRUCT.tl31LoadedContentID
			ELSE
				NETWORK_EARN_FROM_BEND_JOB(iCachedReward, g_FMMC_STRUCT.tl31LoadedContentID)
			ENDIF
		ELIF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
			IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FINALE()
				IF USE_SERVER_TRANSACTIONS()
					TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_GANGOPS_FINALE, iCachedReward, iScriptTransactionIndex, DEFAULT, DEFAULT)
					g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl31ContenID = g_FMMC_STRUCT.tl31LoadedContentID
				ELSE
					NETWORK_EARN_GANGOPS_FINALE(iCachedReward, g_FMMC_STRUCT.tl31LoadedContentID)
				ENDIF
			ELSE
				IF USE_SERVER_TRANSACTIONS()
					IF bPass
						TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_GANGOPS_SETUP, iCachedReward, iScriptTransactionIndex, DEFAULT, DEFAULT)
					ELSE
						TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_GANGOPS_SETUP_FAIL, iCachedReward, iScriptTransactionIndex, DEFAULT, DEFAULT)
					ENDIF
					g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl31ContenID = g_FMMC_STRUCT.tl31LoadedContentID
				ELSE
					NETWORK_EARN_GANGOPS_SETUP(iCachedReward, g_FMMC_STRUCT.tl31LoadedContentID)
				ENDIF
			ENDIF
		ELIF VCM_FLOW_IS_THIS_MISSION_VCM_FLOW(g_FMMC_STRUCT.iRootContentIDHash)
			PRINTLN("[RCC_MISSION] [CASH] - Vinewood Casino (Part 1)")
			IF USE_SERVER_TRANSACTIONS()
				TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_CASINO_STORY_MISSION_REWARD, iCachedReward, iScriptTransactionIndex, DEFAULT, DEFAULT)
				g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl31ContenID = g_FMMC_STRUCT.tl31LoadedContentID
			ELSE
				NETWORK_EARN_CASINO_STORY_MISSION_REWARD(iCachedReward)
			ENDIF
		ELIF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE()
			PRINTLN("[RCC_MISSION] [CASH] - CASINO HEIST")
			IF USE_SERVER_TRANSACTIONS()
				TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_CASINO_HEIST_FINALE, iCachedReward, iScriptTransactionIndex, DEFAULT, DEFAULT)
				g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl31ContenID = g_FMMC_STRUCT.tl31LoadedContentID
			ELSE
				NETWORK_EARN_CASINO_HEIST(iCachedReward, 0, 0, 0, 0, 1, iCachedReward)
			ENDIF
		
		ELIF IS_ARENA_WARS_JOB()
			PRINTLN("[RCC_MISSION] [CASH] - Arena Wars")
			IF USE_SERVER_TRANSACTIONS()
				TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_ARENA_WAR, iCachedReward, iScriptTransactionIndex, DEFAULT, DEFAULT)
				g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl31ContenID = g_FMMC_STRUCT.tl31LoadedContentID
			ELSE
				NETWORK_EARN_ARENA_WAR(iCachedReward, 0, 0, 0)
			ENDIF
		ELSE
			IF USE_SERVER_TRANSACTIONS()
				TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_JOBS, iCachedReward, iScriptTransactionIndex, DEFAULT, DEFAULT)
				g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl31ContenID = g_FMMC_STRUCT.tl31LoadedContentID
			ELSE
				NETWORK_EARN_FROM_JOB(iCachedReward, g_FMMC_STRUCT.tl31LoadedContentID)
			ENDIF
		ENDIF
		
		IF Is_FM_Cloud_Loaded_Activity_A_Heist(g_sTriggerMP.mtMissionData.mdID)
	 		player_handle = GET_GAMER_HANDLE_PLAYER(LocalPlayer)
			IF NETWORK_CLAN_SERVICE_IS_VALID() 
				IF NETWORK_CLAN_PLAYER_IS_ACTIVE(player_handle)   
					INCREMENT_BY_MP_INT_PLAYER_STAT(GET_PLAYER_CREW_HEIST_CASH_PLAYER_ID(LocalPlayer), 	iCachedReward)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_PLAYERS_LEADERBOARD_POSITION(PLAYER_INDEX tempPlayer)

INT iplayer
INT iReturnPlayer = -1

	FOR iplayer = 0 TO (NUM_NETWORK_PLAYERS-1)
		IF g_MissionControllerserverBD_LB.sleaderboard[iplayer].playerID = tempPlayer
			iReturnPlayer = iplayer
			iplayer = NUM_NETWORK_PLAYERS // Break out!
		ENDIF
	ENDFOR
	
	RETURN iReturnPlayer

ENDFUNC

FUNC INT GET_PLAYERS_TEAM_LEADERBOARD_POSITION(PLAYER_INDEX tempPlayer, INT iTeam)

	INT iplayer
	INT iReturnPos = -1
	INT iOffset

	FOR iplayer = 0 TO (NUM_NETWORK_PLAYERS-1)
		IF g_MissionControllerserverBD_LB.sleaderboard[iplayer].iTeam = iTeam
			IF g_MissionControllerserverBD_LB.sleaderboard[iplayer].playerID = tempPlayer
				iReturnPos = iplayer
				iplayer = NUM_NETWORK_PLAYERS // Break out!
			ENDIF
		ELSE
			iOffset++
		ENDIF
	ENDFOR
	
	iReturnPos = iReturnPos - iOffset
	PRINTLN("[JS] GET_PLAYERS_TEAM_LEADERBOARD_POSITION - ", iReturnPos)
	RETURN iReturnPos

ENDFUNC

PROC SAVE_EOM_VEHCILE()
	
	IF NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_BLOCK_SAVE_EOM_CAR_WAS_DELIVERED)
		IF NOT IS_BIT_SET(iLocalBoolCheck, LBOOL_SAVED_EOM_VEHICLE)
			IF IS_PLAYER_ON_CONTACT_STANDARD_OR_HEIST_MISSION(LocalPlayer)
				IF NOT IS_PLAYER_ON_LTS_OR_CTF_MISSION(LocalPlayer)
					IF NOT IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
						SAVE_MY_EOM_VEHICLE_DATA(NULL, FALSE, TRUE, 5)
						SET_BIT(iLocalBoolCheck, LBOOL_SAVED_EOM_VEHICLE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		PRINTLN("[RCC MISSION] - SAVE_EOM_VEHCILE - not saving vehicle, I am in it; received event and LBOOL6_BLOCK_SAVE_EOM_CAR_WAS_DELIVERED is set.")
	ENDIF
	
ENDPROC

FUNC INT HANDLE_GIVING_END_REWARDS(INT iCashGained,INT iXPRewardGained,BOOL bPass,INT iLBPosition)

	INT isuicides = (GlobalplayerBD_FM[NATIVE_TO_INT(PlayerToUse)].iSuicides - iStartSuicides)
	
	INT iXPReturn
	IF bPass
		IF Is_Player_Currently_On_MP_Contact_Mission(LocalPlayer) 
		AND NOT GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		#IF FEATURE_CASINO_HEIST
		AND NOT CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
		#ENDIF
			IF NOT IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
				SET_PLAYER_RECENTLY_PASSED_MISSION_FOR_CONTACT(Get_Current_Mission_Contact())
			ENDIF
		ENDIF
		IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
			iMissionResult = ciFMMC_END_OF_MISSION_STATUS_PASSED
			
			IF iCashGained > 0
				//If a stand mission is being initilised then store out the XP and give it all at then end
				IF IS_A_STRAND_MISSION_BEING_INITIALISED()
					iXPReturn+=g_iFirstMissionCash
					PRINTLN("[TEL] g_iFirstMissionCash       = ", g_iFirstMissionCash)
				ELSE
					GIVE_MISSION_CASH_REWARDS(iCashGained+g_iFirstMissionCash, bPass)
					PRINTLN("[RCC MISSION]  end reward pass iCashGained: ",iCashGained+g_iFirstMissionCash)
					g_iFirstMissionCash = 0
					PRINTLN("[TEL] g_iFirstMissionCash       = 0")
				ENDIF
			ENDIF
			
			PRINTLN("[RCC MISSION]  giving the final iXPRewardGained: ",iXPRewardGained)
			IF iXPRewardGained > 0
				PRINTLN("[KW_RP] HANDLE_GIVING_END_REWARDS is trying to give this much XP Before ROUNDING    = ", iXPRewardGained)
				ROUND_MAX_XP_AMOUNT_PER_JOB(iXPRewardGained)
				PRINTLN("[KW_RP] HANDLE_GIVING_END_REWARDS is trying to give this much XP AFTER ROUNDING    = ", iXPRewardGained)
				//If a stand mission is being initilised then store out the XP and give it all at then end
				IF IS_A_STRAND_MISSION_BEING_INITIALISED()
					iXPReturn+=g_iFirstMissionXP
					PRINTLN("[TEL] g_iFirstMissionXP       = ", g_iFirstMissionXP)
				ELSE
					iXPReturn =GIVE_LOCAL_PLAYER_XP(eXPTYPE_CO_OP_JOBFM,"REWXP_MISS",XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_MISSION_PASS, iXPRewardGained+g_iFirstMissionXP, 1, -1, TRUE)
					STORE_OLD_XP_VALUE() //BC: Added so the rank bar doesnt' appear. bug 2482026
					g_iFirstMissionXP = 0
					PRINTLN("[TEL] g_iFirstMissionXP       = 0")
				ENDIF
			ENDIF
			IF iLBPosition > -1
				IF IS_THIS_A_ROUNDS_MISSION()
					iRoundsLBPosition = iLBPosition
					iRoundsSuicides = isuicides
					PRINTLN("[TEL] * iRoundsLBPosition = ", iLBPosition)
				ELSE
					DEAL_WITH_FM_MATCH_END(FMMC_TYPE_MISSION,g_MissionControllerserverBD_LB.iMatchTimeID,0,iMissionResult,g_MissionControllerserverBD_LB.sleaderboard[iLBPosition].iRank,g_MissionControllerserverBD_LB.sleaderboard[iLBPosition].iKills,g_MissionControllerserverBD_LB.sleaderboard[iLBPosition].iDeaths,isuicides ,iHighKillStreak,g_MissionControllerserverBD_LB.sleaderboard[iLBPosition].iTeam,g_MissionControllerserverBD_LB.sleaderboard[iLBPosition].iHeadshots, g_mnMyRaceModel, DEFAULT, DEFAULT, MC_serverBD.iDifficulty, 	GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( MC_serverBD.tdMissionLengthTimer ))
				ENDIF
			ENDIF
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_MCMWINS, 1)
			INCREMENT_MP_INT_PLAYER_STAT(MPPLY_MCMWIN)
			SAVE_EOM_VEHCILE()
		ENDIF
		IF DOES_ENTITY_EXIST(LocalPlayerPed)
			IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
				g_bPlayEndOfJobRadioFrontEnd = TRUE
				PRINTLN("[RCC MISSION] Player in a vehicle - called SET_AUDIO_FLAG(TRUE) and SET_MOBILE_PHONE_RADIO_STATE(TRUE).")
			ENDIF
		ENDIF
		PRINTLN("[RCC MISSION]  end reward pass iXPRewardGained: ",iXPRewardGained)
	ELSE
		IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
			iMissionResult = ciFMMC_END_OF_MISSION_STATUS_FAILED
			INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_MC_FAILED)
			IF iCashGained+g_iFirstMissionCash > 0
				GIVE_MISSION_CASH_REWARDS(iCashGained+g_iFirstMissionCash, bPass)
				PRINTLN("[RCC MISSION]  end reward fail iCashGained: ",iCashGained+g_iFirstMissionCash)
				g_iFirstMissionCash = 0
			ENDIF
			IF iXPRewardGained+g_iFirstMissionXP > 0
				iXPReturn =GIVE_LOCAL_PLAYER_XP(eXPTYPE_CO_OP_JOBFM,"REWXP_MISS",XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_MISSION_FAIL, iXPRewardGained+g_iFirstMissionXP, 1, -1, TRUE)
				PRINTLN("[RCC MISSION]  end reward fail iXPRewardGained: ",iXPRewardGained+g_iFirstMissionXP)
				STORE_OLD_XP_VALUE() //BC: Added so the rank bar doesnt' appear. bug 2482026
				g_iFirstMissionXP = 0
			ENDIF
			
			IF iLBPosition > -1
				IF IS_THIS_A_ROUNDS_MISSION()
					iRoundsLBPosition = iLBPosition
					iRoundsSuicides = isuicides
					PRINTLN("[TEL] * iRoundsLBPosition = ", iLBPosition)
				ELSE
					DEAL_WITH_FM_MATCH_END(FMMC_TYPE_MISSION,g_MissionControllerserverBD_LB.iMatchTimeID,0,iMissionResult,g_MissionControllerserverBD_LB.sleaderboard[iLBPosition].iRank,g_MissionControllerserverBD_LB.sleaderboard[iLBPosition].iKills,g_MissionControllerserverBD_LB.sleaderboard[iLBPosition].iDeaths,isuicides ,iHighKillStreak,g_MissionControllerserverBD_LB.sleaderboard[iLBPosition].iTeam,g_MissionControllerserverBD_LB.sleaderboard[iLBPosition].iHeadshots, g_mnMyRaceModel, DEFAULT, DEFAULT, MC_serverBD.iDifficulty, 	GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( MC_serverBD.tdMissionLengthTimer ))
				ENDIF
			ENDIF
		ENDIF
	ENDIF	

RETURN iXPReturn

ENDFUNC

FUNC BOOL SHOULD_USE_CLOUD_OVERRIDE()

	IF g_sMPTunables.boverwritecloudvsmissionawards
		PRINTLN("[RCC MISSION]  using cloud override ")
		RETURN TRUE
	ENDIF
	
	PRINTLN("[RCC MISSION]  NOT using cloud override ")
	
	RETURN FALSE

ENDFUNC

FUNC FLOAT GET_CONTACT_MISSION_RP(BOOL bPass, INT iTotalMissionEndTime)

	FLOAT fDifficultyMod = 1.0
	FLOAT fBaseMulti = g_sMPTunables.fContact_Mission_RP_Base_Multiplier 
	PRINTLN("[RCC MISSION] RP fBaseMulti: ",fBaseMulti)
	FLOAT fTimeMulti = 0.2
	FLOAT fxpgained
	FLOAT fMissionRank = TO_FLOAT(g_FMMC_STRUCT.iRank)
	
	IF IS_LOADED_MISSION_LOW_RIDER_FLOW_MISSION()
		fMissionRank = TO_FLOAT(g_sMPTunables.iLOWRIDER_FLOW_MISSION_REWARD_RANK_OVERRIDE)
		PRINTLN("[RCC MISSION] RP IS_LOADED_MISSION_LOW_RIDER_FLOW_MISSION mission rank: ",fMissionRank)
	ELIF IS_THIS_ROOT_CONTENT_ID_AN_ASSASSINATION_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
		fMissionRank = TO_FLOAT(g_sMPTunables.iASSASSINATION_FLOW_MISSION_REWARD_RANK_OVERRIDE)
		PRINTLN("[RCC MISSION] RP IS_THIS_ROOT_CONTENT_ID_AN_ASSASSINATION_MISSION mission rank: ",fMissionRank)	
	ENDIF
	
	PRINTLN("[RCC MISSION] RP mission rank: ",fMissionRank)
	fMissionRank= fMissionRank/2
	PRINTLN("[RCC MISSION] RP mission modded rank: ",fMissionRank)
	
	IF fMissionRank > g_sMPTunables.iContact_Mission_RP_Rank_Cap 
		fMissionRank = TO_FLOAT(g_sMPTunables.iContact_Mission_RP_Rank_Cap )
		PRINTLN("[RCC MISSION] RP mission rank cap: ",fMissionRank)
	ENDIF
	fMissionRank = fMissionRank + g_sMPTunables.iContact_Mission_RP_Basic_Value
	IF MC_serverBD.iDifficulty = DIFF_EASY
		fDifficultyMod = g_sMPTunables.fContact_Mission_RP_Difficulty_Multiplier_Easy
		PRINTLN("[RCC MISSION]  easy xp mod: ",fDifficultyMod) 
	ELIF MC_serverBD.iDifficulty = DIFF_HARD
		fDifficultyMod = g_sMPTunables.fContact_Mission_RP_Difficulty_Multiplier_Hard
		PRINTLN("[RCC MISSION]  hard xp mod: ",fDifficultyMod)  
	ELSE
		fDifficultyMod = g_sMPTunables.fContact_Mission_RP_Difficulty_Multiplier_Normal
		PRINTLN("[RCC MISSION]  normal xp mod: ",fDifficultyMod)  
	ENDIF
	
	IF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_1*60000)	
		fTimeMulti =g_sMPTunables.fContact_Mission_RP_Time_Period_1_Percentage
		PRINTLN("[RCC MISSION] contact time multi under 1 mins: ",fTimeMulti)
	ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_2*60000)	
		fTimeMulti =g_sMPTunables.fContact_Mission_RP_Time_Period_2_Percentage
		PRINTLN("[RCC MISSION] contact time multi under 2 mins: ",fTimeMulti)
	ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_3*60000)	
		fTimeMulti =g_sMPTunables.fContact_Mission_RP_Time_Period_3_Percentage
		PRINTLN("[RCC MISSION] contact time multi under 3 mins: ",fTimeMulti)
	ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_4*60000)	
		fTimeMulti =g_sMPTunables.fContact_Mission_RP_Time_Period_4_Percentage
		PRINTLN("[RCC MISSION] contact time multi under 4 mins: ",fTimeMulti)
	ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_5*60000)	
		fTimeMulti =g_sMPTunables.fContact_Mission_RP_Time_Period_5_Percentage
		PRINTLN("[RCC MISSION] contact time multi under 6 mins: ",fTimeMulti)
	ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_6*60000)	
		fTimeMulti =g_sMPTunables.fContact_Mission_RP_Time_Period_6_Percentage
		PRINTLN("[RCC MISSION] contact time multi under 8 mins: ",fTimeMulti)
	ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_7*60000)		
		fTimeMulti =g_sMPTunables.fContact_Mission_RP_Time_Period_7_Percentage
		PRINTLN("[RCC MISSION] contact time multi under 10 mins: ",fTimeMulti)	
	ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_8*60000)	
		fTimeMulti =g_sMPTunables.fContact_Mission_RP_Time_Period_8_Percentage
		PRINTLN("[RCC MISSION] contact time multi under 12 mins: ",fTimeMulti)							
	ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_9*60000)		
		fTimeMulti =g_sMPTunables.fContact_Mission_RP_Time_Period_9_Percentage
		PRINTLN("[RCC MISSION] contact time multi under 15 mins: ",fTimeMulti)								
	ELSE
		fTimeMulti =g_sMPTunables.fContact_Mission_RP_Time_Period_10_Percentage
		PRINTLN("[RCC MISSION] contact time multi over 15 mins: ",fTimeMulti)	
	ENDIF
	
	fxpgained = fBaseMulti*fMissionRank*fDifficultyMod*fTimeMulti
	
	PRINTLN("[RCC MISSION] contact fxpgained: ",fxpgained)	
	
	IF NOT bPass
	
		IF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_1*60000)	
			fxpgained = 100.0
			
			IF fxpgained > g_sMPTunables.iContact_Mission_Fail_RP_Minimum
				fxpgained = TO_FLOAT(g_sMPTunables.iContact_Mission_Fail_RP_Minimum)
				PRINTLN("[RCC MISSION]  Fail contact mission RP under 1 mins rounding: ",fxpgained) 
			ENDIF
			
			PRINTLN("[RCC MISSION]  Fail contact mission RP under 1 mins: ",fxpgained) 
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_2*60000)	
			fxpgained = fxpgained/g_sMPTunables.fContact_Mission_Fail_RP_Time_Period_2_Divider    
			PRINTLN("[RCC MISSION]  Fail contact mission RP under 2 mins: ",fxpgained) 
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_3*60000)	
			fxpgained = fxpgained/g_sMPTunables.fContact_Mission_Fail_RP_Time_Period_3_Divider    
			PRINTLN("[RCC MISSION]  Fail contact mission RP under 3 mins: ",fxpgained) 
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_4*60000)	
			fxpgained = fxpgained/g_sMPTunables.fContact_Mission_Fail_RP_Time_Period_4_Divider    
			PRINTLN("[RCC MISSION]  Fail contact mission RP under 4 mins: ",fxpgained) 
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_5*60000)	
			fxpgained = fxpgained/g_sMPTunables.fContact_Mission_Fail_RP_Time_Period_5_Divider    
			PRINTLN("[RCC MISSION]  Fail contact mission RP under 6 mins: ",fxpgained) 
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_6*60000)	
			fxpgained = fxpgained/g_sMPTunables.fContact_Mission_Fail_RP_Time_Period_6_Divider    
			PRINTLN("[RCC MISSION]  Fail contact mission RP under 8 mins: ",fxpgained) 
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_7*60000)		
			fxpgained = fxpgained/g_sMPTunables.fContact_Mission_Fail_RP_Time_Period_7_Divider    
			PRINTLN("[RCC MISSION]  Fail contact mission RP under 10 mins: ",fxpgained) 	
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_8*60000)		
			fxpgained = fxpgained/g_sMPTunables.fContact_Mission_Fail_RP_Time_Period_8_Divider    
			PRINTLN("[RCC MISSION]  Fail contact mission RP under 12 mins: ",fxpgained) 							
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_9*60000)		
			fxpgained = fxpgained/g_sMPTunables.fContact_Mission_Fail_RP_Time_Period_9_Divider    
			PRINTLN("[RCC MISSION]  Fail contact mission RP under 15 mins: ",fxpgained) 						
		ELSE
			fxpgained = fxpgained/g_sMPTunables.fContact_Mission_Fail_RP_Time_Period_10_Divider    
			PRINTLN("[RCC MISSION]  Fail contact mission RP over 15 mins: ",fxpgained) 
		ENDIF

		IF fxpgained < g_sMPTunables.iContact_Mission_Fail_RP_Minimum
			fxpgained = TO_FLOAT(g_sMPTunables.iContact_Mission_Fail_RP_Minimum)
			PRINTLN("[RCC MISSION]  Fail contact mission RP under min: ",fxpgained) 
		ENDIF
	
	
	ENDIF
	
	RETURN fxpgained
	
ENDFUNC

FUNC BOOL ALL_TEAMS_FAILED()

	INT iTeam
	
	FOR iTeam = 0 TO MC_ServerBD.iNumberOfTeams-1
		IF NOT HAS_TEAM_FAILED(iTeam)
			RETURN FALSE
		ENDIF
	ENDFOR


	RETURN TRUE

ENDFUNC

FUNC BOOL HAS_LOCAL_PLAYER_FAILED()

	IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_PLAYER_FAIL)
		RETURN TRUE
	ENDIF

	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_VS_TIME_BELOW_MIN_REWARD_THRESHOLD(INT iTotalMissionEndTime)

	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_HuntingPack(g_FMMC_STRUCT.iRootContentIDHash)
		IF iTotalMissionEndTime <= (g_sMPTunables.fHUNTING_PACK_versus_mission_time_1*60000)
			RETURN TRUE
		ENDIF
	ELSE
		IF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_1*60000)
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC FLOAT GET_VS_MISSION_RP(BOOL bPass,INT iTeamPos,INT iTotalMissionEndTime)

	FLOAT fxpgained =  TO_FLOAT(g_FMMC_STRUCT.iXPReward)
	PRINTLN("[RCC MISSION]  base VS ixpreward: ",g_FMMC_STRUCT.iXPReward) 
	
	IF SHOULD_USE_CLOUD_OVERRIDE()
		fxpgained = 3000
		PRINTLN("[RCC MISSION] USING CLOUD OVERRIDE lose base fxpgained: ",fxpgained) 
	ENDIF
	IF Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer)	
		fxpgained = 3000*g_sMPTunables.fxp_tuneable_ctf_xp_reward_multiplier
		PRINTLN("[RCC MISSION] USING CTF base fxpgained: ",fxpgained) 
	ENDIF
	IF bPass
		IF IS_VS_TIME_BELOW_MIN_REWARD_THRESHOLD(iTotalMissionEndTime)
			fxpgained = 100
			PRINTLN("[RCC MISSION] VS mission ended too quickly setting XP to 100")
		ELIF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_2*60000)	
			fxpgained = fxpgained/g_sMPTunables.fVersus_Winner_RP_divider_time_1
			IF fxpgained < 100
				fxpgained = 100
			ENDIF
			PRINTLN("[RCC MISSION] PASS VS mission under 3 mins setting XP to: ",fxpgained)	
		ELIF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_3*60000)
			fxpgained = fxpgained/g_sMPTunables.fVersus_Winner_RP_divider_time_2	
			IF fxpgained < 100
				fxpgained = 100
			ENDIF			
			PRINTLN("[RCC MISSION] PASS VS mission under 5 mins setting XP to: ",fxpgained)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_4*60000)
			fxpgained = fxpgained/g_sMPTunables.fVersus_Winner_RP_divider_time_3
			IF fxpgained < 100
				fxpgained = 100
			ENDIF
			PRINTLN("[RCC MISSION] PASS VS mission under 7.5 mins setting XP to: ",fxpgained)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_5*60000)
			fxpgained = fxpgained/g_sMPTunables.fVersus_Winner_RP_divider_time_4
			IF fxpgained < 100
				fxpgained = 100
			ENDIF			
			PRINTLN("[RCC MISSION] PASS VS mission under 10 mins setting XP to: ",fxpgained)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_6*60000)
			fxpgained = fxpgained/g_sMPTunables.fVersus_Winner_RP_divider_time_5
			IF fxpgained < 200
				fxpgained = 200
			ENDIF	
			PRINTLN("[RCC MISSION] PASS VS mission under 15 mins setting XP to: ",fxpgained)	
		
		ELSE
			fxpgained = fxpgained/g_sMPTunables.fVersus_Winner_RP_divider_time_6
			IF fxpgained < 300
				fxpgained = 300
			ENDIF	
			PRINTLN("[RCC MISSION] PASS VS mission over 15 mins setting XP to: ",fxpgained)	
		ENDIF

	ELSE
		IF IS_VS_TIME_BELOW_MIN_REWARD_THRESHOLD(iTotalMissionEndTime)
			fxpgained = 100
			PRINTLN("[RCC MISSION]  Fail VS mission RP under 10 secs: ",fxpgained) 
		ELIF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_2*60000)
			IF iteampos = 2 OR ALL_TEAMS_FAILED()
				fxpgained = fxpgained/g_sMPTunables.fVersus_Loser_1_RP_divider_time_1
			ELIF iteampos = 3
				fxpgained = fxpgained/g_sMPTunables.fVersus_Loser_2_RP_divider_time_1	
			ELSE
				fxpgained = fxpgained/g_sMPTunables.fVersus_Loser_3_RP_divider_time_1
			ENDIF	
			IF fxpgained < 100
				fxpgained = 100
			ENDIF

			PRINTLN("[RCC MISSION]  Fail VS mission RP under 3 mins: ",fxpgained) 
			
		ELIF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_3*60000)
			IF iteampos = 2 OR ALL_TEAMS_FAILED()
				fxpgained = fxpgained/g_sMPTunables.fVersus_Loser_1_RP_divider_time_2
			ELIF iteampos = 3
				fxpgained = fxpgained/g_sMPTunables.fVersus_Loser_2_RP_divider_time_2
			ELSE
				fxpgained = fxpgained/g_sMPTunables.fVersus_Loser_3_RP_divider_time_2
			ENDIF	
			IF fxpgained < 100
				fxpgained = 100
			ENDIF
			PRINTLN("[RCC MISSION]  Fail VS mission RP under 5 mins: ",fxpgained) 
		ELIF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_4*60000)
			IF iteampos = 2 OR ALL_TEAMS_FAILED()
				fxpgained = fxpgained/g_sMPTunables.fVersus_Loser_1_RP_divider_time_3	
			ELIF iteampos = 3
				fxpgained = fxpgained/g_sMPTunables.fVersus_Loser_2_RP_divider_time_3
			ELSE
				fxpgained = fxpgained/g_sMPTunables.fVersus_Loser_3_RP_divider_time_3
			ENDIF	
			IF fxpgained < 100
				fxpgained = 100
			ENDIF
			PRINTLN("[RCC MISSION]  Fail VS mission RP under 7.5 mins: ",fxpgained) 
		
		ELIF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_5*60000)
			IF iteampos = 2 OR ALL_TEAMS_FAILED()
				fxpgained = fxpgained/g_sMPTunables.fVersus_Loser_1_RP_divider_time_4
			ELIF iteampos = 3
				fxpgained = fxpgained/g_sMPTunables.fVersus_Loser_2_RP_divider_time_4
			ELSE
				fxpgained = fxpgained/g_sMPTunables.fVersus_Loser_3_RP_divider_time_4
			ENDIF	
			IF fxpgained < 100
				fxpgained = 100
			ENDIF
			PRINTLN("[RCC MISSION]  Fail VS mission RP under 10 mins: ",fxpgained) 
		ELIF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_6*60000)
			IF iteampos = 2 OR ALL_TEAMS_FAILED()
				fxpgained = fxpgained/g_sMPTunables.fVersus_Loser_1_RP_divider_time_5	
			ELIF iteampos = 3
				fxpgained = fxpgained/g_sMPTunables.fVersus_Loser_2_RP_divider_time_5
			ELSE
				fxpgained = fxpgained/g_sMPTunables.fVersus_Loser_3_RP_divider_time_5
			ENDIF	
			IF fxpgained < 200
				fxpgained = 200
			ENDIF
			PRINTLN("[RCC MISSION]  Fail VS mission RP under 15 mins: ",fxpgained) 
		ELSE
			IF iteampos = 2 OR ALL_TEAMS_FAILED()
				fxpgained = fxpgained/g_sMPTunables.fVersus_Loser_1_RP_divider_time_6
			ELIF iteampos = 3
				fxpgained = fxpgained/g_sMPTunables.fVersus_Loser_2_RP_divider_time_6
			ELSE
				fxpgained = fxpgained/g_sMPTunables.fVersus_Loser_3_RP_divider_time_6
			ENDIF	
			IF fxpgained < 300
				fxpgained = 300
			ENDIF
			PRINTLN("[RCC MISSION]  Fail VS mission RP Over 15 mins: ",fxpgained) 
		ENDIF
	ENDIF
	
	IF Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer)
		fxpgained = fxpgained*g_sMPTunables.fxp_tuneable_ctf_xp_reward_multiplier
	ENDIF	
	
	RETURN fxpgained
	
ENDFUNC

PROC AWARD_EXTRA_MISSION_RP(BOOL bPass)

	IF iDelayedDeliveryXPToAward > 0
		FLOAT fdeliveryXP =(iDelayedDeliveryXPToAward*100*g_sMPTunables.fxp_tunable_Deliver_a_package_bonus)
		INT ideliveryXP = ROUND(fdeliveryXP)  
		iXPExtraRewardGained = iXPExtraRewardGained+ideliveryXP
		IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
			GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD,"XPT_MEDIUMT", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_MISSION_DELIVERED_VEHICLE,  ideliveryXP,1)
		ENDIF
		#IF IS_DEBUG_BUILD
			iDebugXPDelVeh = iDebugXPDelVeh + ideliveryXP
		#ENDIF
		PRINTLN("[RCC MISSION] iDelayedDeliveryXPToAward ",ideliveryXP)
	ENDIF
	
	IF AM_I_ON_A_HEIST()
		EXIT
	ENDIF
	
	IF bPass
		IF NOT Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)
		AND NOT Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer)
		AND NOT Is_Player_Currently_On_MP_Versus_Mission(LocalPlayer)
			INT iLivesBonus = GET_LIVES_XP_BONUS()
			iXPExtraRewardGained = iXPExtraRewardGained +iLivesBonus
			PRINTLN("[RCC MISSION] adding lives bonus of: iLivesBonus ",iLivesBonus)
			#IF IS_DEBUG_BUILD
				iDebugXPLivesBonus = ilivesbonus
			#ENDIF
			IF iLivesBonus > 0
				IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
					GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD,"XPT_MEDIUMT", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_MISSION_PASS,  ilivesbonus,1)
				ENDIF
			ENDIF
			
			INT iKillBonus = GIVE_ALL_ENEMY_KILL_XP()
			iXPExtraRewardGained = iXPExtraRewardGained + iKillBonus
			PRINTLN("[RCC MISSION] adding kill bonus of: iKillBonus ",iKillBonus)
			IF g_bVSMission
				IF MC_serverBD.iTotalMissionEndTime <= ciMIN_REWARD_TIME
					iXPExtraRewardGained = 0
					iXPExtraRewardGained = 0
					PRINTLN("[RCC MISSION] VS mission ended too quickly setting lives and kill XP to 0")
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
		
ENDPROC

FUNC FLOAT CALCULATE_HEIST_RP(BOOL bPass, BOOL bHeistFinale,BOOL bLeader,INT iTotalMissionEndTime)

	FLOAT fMedalMulti,fxpgained,fStage,fRole,fTimeScale,fFailDivider

	FLOAT fBaseMulti = g_smpTunables.fHeist_RP_Base_Multiplier              
	CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Heist fBaseMulti: ",fBaseMulti) 
	
	FLOAT fMissionRank = TO_FLOAT(g_FMMC_STRUCT.iRank)
	
	CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] RP Heist rank: ",fMissionRank)
	fMissionRank= fMissionRank/2
	CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] RP Heist modded rank: ",fMissionRank)
	
	IF fMissionRank > g_sMPTunables.iHeist_RP_Rank_Cap                      
		fMissionRank = TO_FLOAT(g_sMPTunables.iHeist_RP_Rank_Cap )
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] RP Heist rank cap: ",fMissionRank)
	ENDIF
	
	CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] RP Heist g_smpTunables.iHeist_RP_Basic_Value : ",g_smpTunables.iHeist_RP_Basic_Value )
	
	fMissionRank = fMissionRank + g_smpTunables.iHeist_RP_Basic_Value 
	
	CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] RP Heist fMissionRank : ",fMissionRank )
	
	IF bPass
		IF GET_HEIST_RATING_HUD_COLOUR(iLocalPart) = HUD_COLOUR_PLATINUM
			fMedalMulti = g_sMPTunables.fRP_Heists_Platinum_Medal_RP_Multiplier	
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] platinum heist xp mod: ",fMedalMulti) 
		ELIF GET_HEIST_RATING_HUD_COLOUR(iLocalPart) = HUD_COLOUR_GOLD
			fMedalMulti = g_sMPTunables.fRP_Heists_Gold_Medal_RP_Multiplier	
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] gold heist xp mod: ",fMedalMulti) 
		ELIF GET_HEIST_RATING_HUD_COLOUR(iLocalPart) = HUD_COLOUR_SILVER
			fMedalMulti = g_sMPTunables.fRP_Heists_Silver_Medal_RP_Multiplier
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] silver heist xp mod: ",fMedalMulti) 
		ELIF GET_HEIST_RATING_HUD_COLOUR(iLocalPart) = HUD_COLOUR_BRONZE
			fMedalMulti = g_sMPTunables.fRP_Heists_Bronze_Medal_RP_Multiplier	
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] bronze heist xp mod: ",fMedalMulti) 
		ELSE
			fMedalMulti = 1.0
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] normal medal heist xp mod: ",fMedalMulti) 
		ENDIF
	ELSE
		fMedalMulti = 1.0
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] normal fail medal heist xp mod: ",fMedalMulti) 	
	ENDIF
	
	IF bLeader	
		fRole = g_smpTunables.fHeist_RP_Role_Finale_Bonus
	ELSE
		fRole = g_smpTunables.fHeist_RP_Role_Prep_Bonus  
	ENDIF
	CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] normal heist fRole: ",fRole) 
	
	IF bHeistFinale
		fStage = g_smpTunables.fHeist_RP_Stage_Finale_Bonus           
	ELSE
		fStage = g_smpTunables.fHeist_RP_Stage_Prep_Bonus 
	ENDIF
	CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] normal heist fStage: ",fStage) 
	
	INT iMissionEndTime = iTotalMissionEndTime

	IF iMissionEndTime <= (g_sMPTunables.iRP_Heists_Heist_fail_RP_time_period_1*60000)
		fTimeScale = g_smpTunables.fHeist_RP_Time_Period_1_Percentage     
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION]  finished heist RP under 1 mins: ",fxpgained," tunable: ",g_smpTunables.fHeist_RP_Time_Period_1_Percentage ) 
	ELIF iMissionEndTime <= (g_sMPTunables.iRP_Heists_Heist_fail_RP_time_period_2*60000)
		fTimeScale = g_smpTunables.fHeist_RP_Time_Period_2_Percentage     
		fFailDivider =g_smpTunables.fHeist_Fail_RP_Time_Period_2_Divider
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION]  finished heist RP under 2 mins: ",fxpgained," tunable: ",g_smpTunables.fHeist_RP_Time_Period_2_Percentage) 
	ELIF iMissionEndTime <= (g_sMPTunables.iRP_Heists_Heist_fail_RP_time_period_3*60000)
		fTimeScale = g_smpTunables.fHeist_RP_Time_Period_3_Percentage     
		fFailDivider =g_smpTunables.fHeist_Fail_RP_Time_Period_3_Divider
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION]  finished heist RP under 3 mins: ",fxpgained," tunable: ",g_smpTunables.fHeist_RP_Time_Period_3_Percentage) 
	ELIF iMissionEndTime <=(g_sMPTunables.iRP_Heists_Heist_fail_RP_time_period_4*60000)
		fTimeScale = g_smpTunables.fHeist_RP_Time_Period_4_Percentage     
		fFailDivider =g_smpTunables.fHeist_Fail_RP_Time_Period_4_Divider
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION]  finished heist RP under 4 mins: ",fxpgained," tunable: ",g_smpTunables.fHeist_RP_Time_Period_4_Percentage) 
	ELIF iMissionEndTime <= (g_sMPTunables.iRP_Heists_Heist_fail_RP_time_period_5*60000)
		fTimeScale = g_smpTunables.fHeist_RP_Time_Period_5_Percentage     
		fFailDivider =g_smpTunables.fHeist_Fail_RP_Time_Period_5_Divider
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION]  finished heist RP under 6 mins: ",fxpgained," tunable: ",g_smpTunables.fHeist_RP_Time_Period_5_Percentage) 	
	ELIF iMissionEndTime <= (g_sMPTunables.iRP_Heists_Heist_fail_RP_time_period_6*60000)
		fTimeScale = g_smpTunables.fHeist_RP_Time_Period_6_Percentage     
		fFailDivider =g_smpTunables.fHeist_Fail_RP_Time_Period_6_Divider
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION]  finished heist RP under 8 mins: ",fxpgained," tunable: ",g_smpTunables.fHeist_RP_Time_Period_6_Percentage) 	
	ELIF iMissionEndTime <= (g_sMPTunables.iHeist_fail_RP_time_period_7*60000)
		fTimeScale = g_smpTunables.fHeist_RP_Time_Period_7_Percentage     
		fFailDivider =g_smpTunables.fHeist_Fail_RP_Time_Period_7_Divider	
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION]  finished heist RP under 10 mins: ",fxpgained," tunable: ",g_smpTunables.fHeist_RP_Time_Period_7_Percentage) 
	ELIF iMissionEndTime <= (g_sMPTunables.iHeist_fail_RP_time_period_8*60000)
		fTimeScale = g_smpTunables.fHeist_RP_Time_Period_8_Percentage     
		fFailDivider =g_smpTunables.fHeist_Fail_RP_Time_Period_8_Divider
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION]  finished heist RP under 12 mins: ",fxpgained," tunable: ",g_smpTunables.fHeist_RP_Time_Period_8_Percentage) 
	ELIF iMissionEndTime <= (g_sMPTunables.iHeist_fail_RP_time_period_9*60000)
		fTimeScale = g_smpTunables.fHeist_RP_Time_Period_9_Percentage     
		fFailDivider =g_smpTunables.fHeist_Fail_RP_Time_Period_9_Divider	
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION]  finished heist RP under 15 mins: ",fxpgained," tunable: ",g_smpTunables.fHeist_RP_Time_Period_9_Percentage) 
	ELSE
		fTimeScale = g_smpTunables.fHeist_RP_Time_Period_10_Percentage     
		fFailDivider =g_smpTunables.fHeist_Fail_RP_Time_Period_10_Divider
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION]  finished heist RP over 15 mins: ",fxpgained," tunable: ",g_smpTunables.fHeist_RP_Time_Period_10_Percentage) 
	ENDIF

	fxpgained = fBaseMulti*fMissionRank*fRole*fTimeScale*(fStage+fMedalMulti)

	CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] heist RP formula fxpgained: ",fxpgained)
	
	SWITCH(g_FMMC_STRUCT.iDifficulity)
		CASE DIFF_EASY
			fxpgained = fxpgained * g_sMPTunables.fheist_difficulty_easy
			PRINTLN("[RCC MISSION] [CASH_RWD] setting heist RP reward to " ,fxpgained," since difficulty = EASY and difficulty modifier = ",g_sMPTunables.fheist_difficulty_easy)
		BREAK 
		CASE DIFF_NORMAL
			fxpgained = fxpgained * g_sMPTunables.fheist_difficulty_normal
			PRINTLN("[RCC MISSION] [CASH_RWD] setting heist RP reward to " ,fxpgained," since difficulty = NORMAL and difficulty modifier = ",g_sMPTunables.fheist_difficulty_normal)
		BREAK
		CASE DIFF_HARD
			fxpgained = fxpgained * g_sMPTunables.fheist_difficulty_hard
			PRINTLN("[RCC MISSION] [CASH_RWD] setting heist RP reward to " ,fxpgained," since difficulty = HARD and difficulty modifier = ",g_sMPTunables.fheist_difficulty_hard)
		BREAK
	ENDSWITCH

	IF NOT bPass
			
		IF fFailDivider = 0
			fxpgained = 0
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION]  fFailDivider = 0 fxpgained: ",fxpgained) 
		ELSE
			fxpgained = fxpgained/fFailDivider
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION]  Final heist fail after divider fxpgained: ",fxpgained) 
		ENDIF
		
		IF fxpgained < TO_FLOAT(g_smpTunables.iHeist_Fail_RP_Minimum)
			fxpgained = TO_FLOAT(g_smpTunables.iHeist_Fail_RP_Minimum)
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION]  Final heist fail min cap fxpgained: ",fxpgained) 
		ENDIF
		
		IF IS_THIS_A_QUICK_RESTART_JOB() 
			fxpgained =  0
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION]  Final heist fail IS_THIS_A_QUICK_RESTART_JOB TRUE fxpgained: ",fxpgained)
		ENDIF
		
	ENDIF
	
	CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION]  Final heist RP fxpgained: ",fxpgained) 
	
	RETURN fxpgained

ENDFUNC


FUNC FLOAT CALCULATE_GANGOPS_RP(BOOL bPass, INT iMissionEndTime, BOOL bFinale)
	PRINTLN("[JS] CALCULATE_GANGOPS_RP - ##################################################")
	PRINTLN("[JS] CALCULATE_GANGOPS_RP - Time: ", FLOOR(TO_FLOAT(iMissionEndTime - 500 - (iMissionEndTime - 500) % 1000) / 60000.0))
	PRINTLN("[JS] CALCULATE_GANGOPS_RP - Role: ", GB_GET_LOCAL_PLAYER_GANG_BOSS() = LocalPlayer)
	PRINTLN("[JS] CALCULATE_GANGOPS_RP - Finale: ", bFinale)
	PRINTLN("[JS] CALCULATE_GANGOPS_RP - Medal: ", ENUM_TO_INT(GET_HEIST_RATING_HUD_COLOUR(iLocalPart)))
	PRINTLN("[JS] CALCULATE_GANGOPS_RP - Difficulty: ", g_FMMC_STRUCT.iDifficulity)
	PRINTLN("[JS] CALCULATE_GANGOPS_RP - Pass: ", bPass)
	PRINTLN("[JS] CALCULATE_GANGOPS_RP - ##################################################")
	
	IF NOT bPass
	AND IS_THIS_A_QUICK_RESTART_JOB() 
		PRINTLN("[JS] CALCULATE_GANGOPS_RP - QUICK RESTART - NO RP") 
		RETURN 0.0
	ENDIF
	
	FLOAT fMedalMulti, fRPGained, fStage, fRole, fTimeScale, fFailDivider

	FLOAT fBaseMulti = g_smpTunables.fGangops_RP_Base_Multiplier              
	PRINTLN("[JS] CALCULATE_GANGOPS_RP - fBaseMulti : ",fBaseMulti )
	
	FLOAT fMissionRank = TO_FLOAT(g_FMMC_STRUCT.iRank) / 2
	IF fMissionRank > g_sMPTunables.iGangops_RP_Rank_Cap                      
		fMissionRank = TO_FLOAT(g_sMPTunables.iGangops_RP_Rank_Cap )
	ENDIF
	fMissionRank += g_smpTunables.iGangops_RP_Basic_Value 
	PRINTLN("[JS] CALCULATE_GANGOPS_RP - fMissionRank : ",fMissionRank )
	
	IF bPass
		IF GET_HEIST_RATING_HUD_COLOUR(iLocalPart) = HUD_COLOUR_PLATINUM
			fMedalMulti = g_sMPTunables.fRP_Gangops_Platinum_Medal_RP_Multiplier	
		ELIF GET_HEIST_RATING_HUD_COLOUR(iLocalPart) = HUD_COLOUR_GOLD
			fMedalMulti = g_sMPTunables.fRP_Gangops_Gold_Medal_RP_Multiplier	
		ELIF GET_HEIST_RATING_HUD_COLOUR(iLocalPart) = HUD_COLOUR_SILVER
			fMedalMulti = g_sMPTunables.fRP_Gangops_Silver_Medal_RP_Multiplier
		ELIF GET_HEIST_RATING_HUD_COLOUR(iLocalPart) = HUD_COLOUR_BRONZE
			fMedalMulti = g_sMPTunables.fRP_Gangops_Bronze_Medal_RP_Multiplier	
		ELSE
			fMedalMulti = 1.0
		ENDIF
	ELSE
		fMedalMulti = 1.0
	ENDIF
	PRINTLN("[JS] CALCULATE_GANGOPS_RP - medal bonus: ", fMedalMulti) 
	
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS() = LocalPlayer
		fRole = g_smpTunables.fGangops_RP_Role_Boss_Bonus
	ELSE
		fRole = g_smpTunables.fGangops_RP_Role_Goon_Bonus  
	ENDIF
	PRINTLN("[JS] CALCULATE_GANGOPS_RP - Role Bonus: ", fRole) 
	
	IF bFinale
		fStage = g_smpTunables.fGangops_RP_Stage_Finale_Bonus           
	ELSE
		fStage = g_smpTunables.fGangops_RP_Stage_Prep_Bonus 
	ENDIF
	PRINTLN("[JS] CALCULATE_GANGOPS_RP - Stage Bonus: ", fStage) 
	
	IF iMissionEndTime <= (g_sMPTunables.iGangops_fail_RP_time_period_1 * 60000)
		fTimeScale = g_smpTunables.fGangops_RP_Time_Period_1_Percentage     
	ELIF iMissionEndTime <= (g_sMPTunables.iGangops_fail_RP_time_period_2 * 60000)
		fTimeScale = g_smpTunables.fGangops_RP_Time_Period_2_Percentage     
		fFailDivider =g_smpTunables.fGangops_Fail_RP_Time_Period_2_Divider
	ELIF iMissionEndTime <= (g_sMPTunables.iGangops_fail_RP_time_period_3 * 60000)
		fTimeScale = g_smpTunables.fGangops_RP_Time_Period_3_Percentage     
		fFailDivider =g_smpTunables.fGangops_Fail_RP_Time_Period_3_Divider
	ELIF iMissionEndTime <= (g_sMPTunables.iGangops_fail_RP_time_period_4 * 60000)
		fTimeScale = g_smpTunables.fGangops_RP_Time_Period_4_Percentage     
		fFailDivider =g_smpTunables.fGangops_Fail_RP_Time_Period_4_Divider
	ELIF iMissionEndTime <= (g_sMPTunables.iGangops_fail_RP_time_period_5 * 60000)
		fTimeScale = g_smpTunables.fGangops_RP_Time_Period_5_Percentage     
		fFailDivider =g_smpTunables.fGangops_Fail_RP_Time_Period_5_Divider
	ELIF iMissionEndTime <= (g_sMPTunables.iGangops_fail_RP_time_period_6 * 60000)
		fTimeScale = g_smpTunables.fGangops_RP_Time_Period_6_Percentage     
		fFailDivider =g_smpTunables.fGangops_Fail_RP_Time_Period_6_Divider
	ELIF iMissionEndTime <= (g_sMPTunables.iGangops_fail_RP_time_period_7 * 60000)
		fTimeScale = g_smpTunables.fGangops_RP_Time_Period_7_Percentage     
		fFailDivider =g_smpTunables.fGangops_Fail_RP_Time_Period_7_Divider	
	ELIF iMissionEndTime <= (g_sMPTunables.iGangops_fail_RP_time_period_8 * 60000)
		fTimeScale = g_smpTunables.fGangops_RP_Time_Period_8_Percentage     
		fFailDivider =g_smpTunables.fGangops_Fail_RP_Time_Period_8_Divider
	ELIF iMissionEndTime <= (g_sMPTunables.iGangops_fail_RP_time_period_9 * 60000)
		fTimeScale = g_smpTunables.fGangops_RP_Time_Period_9_Percentage     
		fFailDivider =g_smpTunables.fGangops_Fail_RP_Time_Period_9_Divider	
	ELSE
		fTimeScale = g_smpTunables.fGangops_RP_Time_Period_10_Percentage     
		fFailDivider = g_smpTunables.fGangops_Fail_RP_Time_Period_10_Divider
	ENDIF
	PRINTLN("[JS] CALCULATE_GANGOPS_RP - Time Bonus: time = ", iMissionEndTime, " scale = ", fTimeScale, " divider = ", fFailDivider) 
	
	fRPGained = fBaseMulti * fMissionRank * fRole * fTimeScale * (fStage + fMedalMulti)
	PRINTLN("[JS] CALCULATE_GANGOPS_RP - RP Calculation: ", fRPGained) 
	
	SWITCH(g_FMMC_STRUCT.iDifficulity)
		CASE DIFF_NORMAL
			fRPGained *= g_sMPTunables.fGangops_difficulty_normal
			PRINTLN("[JS] CALCULATE_GANGOPS_RP - Normal Difficulty Scaler: ", g_sMPTunables.fGangops_difficulty_normal) 
		BREAK
		CASE DIFF_HARD
			fRPGained *= g_sMPTunables.fGangops_difficulty_hard
			PRINTLN("[JS] CALCULATE_GANGOPS_RP - Hard Difficulty Scaler: ", g_sMPTunables.fGangops_difficulty_hard) 
		BREAK
	ENDSWITCH
	PRINTLN("[JS] CALCULATE_GANGOPS_RP - After difficulty scaling: ", fRPGained) 

	IF NOT bPass
		IF fFailDivider = 0
			PRINTLN("[JS] CALCULATE_GANGOPS_RP - fFailDivider = 0") 
			fRPGained = 0
		ELSE
			fRPGained /= fFailDivider
			PRINTLN("[JS] CALCULATE_GANGOPS_RP - After Fail Divider: ", fRPGained) 
		ENDIF
		
		IF fRPGained < TO_FLOAT(g_smpTunables.iGangops_Fail_RP_Minimum)
			fRPGained = TO_FLOAT(g_smpTunables.iGangops_Fail_RP_Minimum)
			PRINTLN("[JS] CALCULATE_GANGOPS_RP - RP set to min: ", fRPGained) 
		ENDIF
	ENDIF
	
	IF fRPGained > TO_FLOAT(g_sMPTunables.iH2_JOB_RP_CAP)
		fRPGained = TO_FLOAT(g_sMPTunables.iH2_JOB_RP_CAP)
		PRINTLN("[JS] CALCULATE_GANGOPS_RP - Capping to max: ", fRPGained) 
	ENDIF
	
	PRINTLN("[JS] CALCULATE_GANGOPS_RP - Final RP : ",fRPGained) 
	
	RETURN fRPGained
ENDFUNC

FUNC FLOAT CALCULATE_CASINO_HEIST_RP(BOOL bPass, INT iMissionEndTime)
	PRINTLN("[JS] CALCULATE_CASINO_HEIST_RP - ##################################################")
	PRINTLN("[JS] CALCULATE_CASINO_HEIST_RP - Time: ", FLOOR(TO_FLOAT(iMissionEndTime - 500 - (iMissionEndTime - 500) % 1000) / 60000.0))
	PRINTLN("[JS] CALCULATE_CASINO_HEIST_RP - Role: ", GB_GET_LOCAL_PLAYER_GANG_BOSS() = LocalPlayer)
	PRINTLN("[JS] CALCULATE_CASINO_HEIST_RP - Medal: ", ENUM_TO_INT(GET_HEIST_RATING_HUD_COLOUR(iLocalPart)))
	PRINTLN("[JS] CALCULATE_CASINO_HEIST_RP - Difficulty: ", g_FMMC_STRUCT.iDifficulity)
	PRINTLN("[JS] CALCULATE_CASINO_HEIST_RP - Pass: ", bPass)
	PRINTLN("[JS] CALCULATE_CASINO_HEIST_RP - ##################################################")
	
	IF NOT bPass
	AND IS_THIS_A_QUICK_RESTART_JOB() 
		PRINTLN("[JS] CALCULATE_CASINO_HEIST_RP - QUICK RESTART - NO RP") 
		RETURN 0.0
	ENDIF
	
	FLOAT fMedalMulti, fRPGained, fStage, fRole, fTimeScale, fFailDivider

	FLOAT fBaseMulti = g_smpTunables.fCH_RP_Base_Multiplier              
	PRINTLN("[JS] CALCULATE_CASINO_HEIST_RP - fBaseMulti : ",fBaseMulti)
	
	FLOAT fMissionRank = TO_FLOAT(g_FMMC_STRUCT.iRank) / 2
	IF fMissionRank > g_sMPTunables.iCH_RP_Rank_Cap                      
		fMissionRank = TO_FLOAT(g_sMPTunables.iCH_RP_Rank_Cap)
	ENDIF
	fMissionRank += g_smpTunables.iCH_RP_Basic_Value 
	PRINTLN("[JS] CALCULATE_CASINO_HEIST_RP - fMissionRank : ",fMissionRank)
	
	IF bPass
		IF GET_HEIST_RATING_HUD_COLOUR(iLocalPart) = HUD_COLOUR_PLATINUM
			fMedalMulti = g_sMPTunables.fRP_CH_Platinum_Medal_RP_Multiplier	
		ELIF GET_HEIST_RATING_HUD_COLOUR(iLocalPart) = HUD_COLOUR_GOLD
			fMedalMulti = g_sMPTunables.fRP_CH_Gold_Medal_RP_Multiplier	
		ELIF GET_HEIST_RATING_HUD_COLOUR(iLocalPart) = HUD_COLOUR_SILVER
			fMedalMulti = g_sMPTunables.fRP_CH_Silver_Medal_RP_Multiplier
		ELIF GET_HEIST_RATING_HUD_COLOUR(iLocalPart) = HUD_COLOUR_BRONZE
			fMedalMulti = g_sMPTunables.fRP_CH_Bronze_Medal_RP_Multiplier	
		ELSE
			fMedalMulti = 1.0
		ENDIF
	ELSE
		fMedalMulti = 1.0
	ENDIF
	PRINTLN("[JS] CALCULATE_CASINO_HEIST_RP - medal bonus: ", fMedalMulti) 
	
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS() = LocalPlayer
		fRole = g_smpTunables.fCH_RP_Role_Boss_Bonus
	ELSE
		fRole = g_smpTunables.fCH_RP_Role_Goon_Bonus  
	ENDIF
	PRINTLN("[JS] CALCULATE_CASINO_HEIST_RP - Role Bonus: ", fRole) 
	
	fStage = g_smpTunables.fCH_RP_Stage_Finale_Bonus           
	PRINTLN("[JS] CALCULATE_CASINO_HEIST_RP - Stage Bonus: ", fStage) 
	
	IF iMissionEndTime <= (g_sMPTunables.iCH_fail_RP_time_period_1 * 60000)
		fTimeScale = g_smpTunables.fCH_RP_Time_Period_1_Percentage     
	ELIF iMissionEndTime <= (g_sMPTunables.iCH_fail_RP_time_period_2 * 60000)
		fTimeScale = g_smpTunables.fCH_RP_Time_Period_2_Percentage     
		fFailDivider =g_smpTunables.fCH_Fail_RP_Time_Period_2_Divider
	ELIF iMissionEndTime <= (g_sMPTunables.iCH_fail_RP_time_period_3 * 60000)
		fTimeScale = g_smpTunables.fCH_RP_Time_Period_3_Percentage     
		fFailDivider =g_smpTunables.fCH_Fail_RP_Time_Period_3_Divider
	ELIF iMissionEndTime <= (g_sMPTunables.iCH_fail_RP_time_period_4 * 60000)
		fTimeScale = g_smpTunables.fCH_RP_Time_Period_4_Percentage     
		fFailDivider =g_smpTunables.fCH_Fail_RP_Time_Period_4_Divider
	ELIF iMissionEndTime <= (g_sMPTunables.iCH_fail_RP_time_period_5 * 60000)
		fTimeScale = g_smpTunables.fCH_RP_Time_Period_5_Percentage     
		fFailDivider =g_smpTunables.fCH_Fail_RP_Time_Period_5_Divider
	ELIF iMissionEndTime <= (g_sMPTunables.iCH_fail_RP_time_period_6 * 60000)
		fTimeScale = g_smpTunables.fCH_RP_Time_Period_6_Percentage     
		fFailDivider =g_smpTunables.fCH_Fail_RP_Time_Period_6_Divider
	ELIF iMissionEndTime <= (g_sMPTunables.iCH_fail_RP_time_period_7 * 60000)
		fTimeScale = g_smpTunables.fCH_RP_Time_Period_7_Percentage     
		fFailDivider =g_smpTunables.fCH_Fail_RP_Time_Period_7_Divider	
	ELIF iMissionEndTime <= (g_sMPTunables.iCH_fail_RP_time_period_8 * 60000)
		fTimeScale = g_smpTunables.fCH_RP_Time_Period_8_Percentage     
		fFailDivider =g_smpTunables.fCH_Fail_RP_Time_Period_8_Divider
	ELIF iMissionEndTime <= (g_sMPTunables.iCH_fail_RP_time_period_9 * 60000)
		fTimeScale = g_smpTunables.fCH_RP_Time_Period_9_Percentage     
		fFailDivider =g_smpTunables.fCH_Fail_RP_Time_Period_9_Divider	
	ELSE
		fTimeScale = g_smpTunables.fCH_RP_Time_Period_10_Percentage     
		fFailDivider = g_smpTunables.fCH_Fail_RP_Time_Period_10_Divider
	ENDIF
	PRINTLN("[JS] CALCULATE_CASINO_HEIST_RP - Time Bonus: time = ", iMissionEndTime, " scale = ", fTimeScale, " divider = ", fFailDivider) 
	
	fRPGained = fBaseMulti * fMissionRank * fRole * fTimeScale * (fStage + fMedalMulti)
	PRINTLN("[JS] CALCULATE_CASINO_HEIST_RP - RP Calculation: ", fRPGained) 
	
	SWITCH(g_FMMC_STRUCT.iDifficulity)
		CASE DIFF_NORMAL
			fRPGained *= g_sMPTunables.fCH_difficulty_normal
			PRINTLN("[JS] CALCULATE_CASINO_HEIST_RP - Normal Difficulty Scaler: ", g_sMPTunables.fCH_difficulty_normal) 
		BREAK
		CASE DIFF_HARD
			fRPGained *= g_sMPTunables.fCH_difficulty_hard
			PRINTLN("[JS] CALCULATE_CASINO_HEIST_RP - Hard Difficulty Scaler: ", g_sMPTunables.fCH_difficulty_hard) 
		BREAK
	ENDSWITCH
	PRINTLN("[JS] CALCULATE_CASINO_HEIST_RP - After difficulty scaling: ", fRPGained) 

	IF NOT bPass
		IF fFailDivider = 0
			PRINTLN("[JS] CALCULATE_CASINO_HEIST_RP - fFailDivider = 0") 
			fRPGained = 0
		ELSE
			fRPGained /= fFailDivider
			PRINTLN("[JS] CALCULATE_CASINO_HEIST_RP - After Fail Divider: ", fRPGained) 
		ENDIF
		
		IF fRPGained < TO_FLOAT(g_smpTunables.iCH_Fail_RP_Minimum)
			fRPGained = TO_FLOAT(g_smpTunables.iCH_Fail_RP_Minimum)
			PRINTLN("[JS] CALCULATE_CASINO_HEIST_RP - RP set to min: ", fRPGained) 
		ENDIF
	ENDIF
	
	IF fRPGained > TO_FLOAT(g_sMPTunables.iCH_JOB_RP_CAP)
		fRPGained = TO_FLOAT(g_sMPTunables.iCH_JOB_RP_CAP)
		PRINTLN("[JS] CALCULATE_CASINO_HEIST_RP - Capping to max: ", fRPGained) 
	ENDIF
	
	PRINTLN("[JS] CALCULATE_CASINO_HEIST_RP - Final RP : ",fRPGained) 
	
	RETURN fRPGained
ENDFUNC

FUNC INT HANDLE_FINAL_XP_REWARD(BOOL bPass,INT iTeamPos,INT iTotalMissionEndTime)
	
	INT iXPRewardGained
	FLOAT fxpgained
	
	IF NOT IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()	
		
		AWARD_EXTRA_MISSION_RP(bPass)

	ENDIF

	IF bPass
		IF g_bVSMission

			fxpgained = GET_VS_MISSION_RP(bPass,iTeamPos,iTotalMissionEndTime)

		ELSE
		
			IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
			OR Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
			
				fxpgained = CALCULATE_HEIST_RP(bPass,Is_Player_Currently_On_MP_Heist(LocalPlayer),IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_HEIST_HOST),iTotalMissionEndTime)
			
			ELIF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
				
				fxpgained = CALCULATE_GANGOPS_RP(bPass,iTotalMissionEndTime, GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FINALE())
			ELIF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
				
				fxpgained = CALCULATE_CASINO_HEIST_RP(bPass, iTotalMissionEndTime)
			ELSE
			
				IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()	
					fxpgained =  TO_FLOAT(g_FMMC_STRUCT.iXPReward)
					PRINTLN("[RCC MISSION]  base Tutorial pass ixpreward: ",fxpgained) 
				ELSE
					fxpgained = GET_CONTACT_MISSION_RP(bPass,iTotalMissionEndTime)
				ENDIF
			
			ENDIF
		ENDIF
		
		iXPRewardGained  = ROUND(fxpgained)
		PRINTLN("[RCC MISSION] FINAL modified ixpreward after round: ",iXPRewardGained)
		
	ELSE
		IF g_bVSMission
			fxpgained = GET_VS_MISSION_RP(bPass,iTeamPos,iTotalMissionEndTime)
			PRINTLN("[RCC MISSION]  Fail VS mission fxpgained: ",fxpgained) 
		ELSE
			IF Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
				fxpgained = CALCULATE_HEIST_RP(bPass,Is_Player_Currently_On_MP_Heist(LocalPlayer),IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_HEIST_HOST),iTotalMissionEndTime)
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION]  Fail heist planning mission fxpgained: ",fxpgained) 
			ELIF Is_Player_Currently_On_MP_Heist(LocalPlayer)
				fxpgained = CALCULATE_HEIST_RP(bPass,Is_Player_Currently_On_MP_Heist(LocalPlayer),IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_HEIST_HOST),iTotalMissionEndTime)
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION]  Fail heist finale mission fxpgained: ",fxpgained) 
			ELIF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
				fxpgained = CALCULATE_GANGOPS_RP(bPass,iTotalMissionEndTime, GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FINALE())
			ELIF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
				fxpgained = CALCULATE_CASINO_HEIST_RP(bPass, iTotalMissionEndTime)
			ELSE	
				IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()	
					fxpgained =  0.0
					PRINTLN("[RCC MISSION]  base Tutorial fail ixpreward: ",fxpgained) 
				ELSE
					fxpgained =GET_CONTACT_MISSION_RP(bPass,iTotalMissionEndTime)
					PRINTLN("[RCC MISSION]  Fail contact mission fxpgained: ",fxpgained) 
				ENDIF
			ENDIF
		ENDIF
		
		iXPRewardGained = ROUND(fxpgained)
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION]  Fail mission iXPRewardGained: ",iXPRewardGained) 
		
	ENDIF
	
	
	IF iXPRewardGained < 0
		iXPRewardGained=0
	ENDIF
	
	
	RETURN iXPRewardGained

ENDFUNC

FUNC INT GET_CASH_FOR_LTS(BOOL bPass)

//(100+((100*((Players - Players on team(max8))/2.5)*(Kills(max8)/2))*(1+(Average Rank(max50)/16)))*2
	INT icash
	FLOAT fCash
	
	IF HAS_LOCAL_PLAYER_FAILED()
	AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciMISSION_BLOCK_REWARDS_ON_FAIL)
		icash = 0
		PRINTLN("[JS] - GET_CASH_FOR_LTS - Block Rewards On Player Fail is set, returning 0 as cash reward")
		RETURN icash
	ENDIF
	
	INT iplayersOnTeam = MC_serverBD.iNumberOfLBPlayers[MC_playerBD[iLocalPart].iteam]
	IF iplayersOnTeam > 8
		iplayersOnTeam = 8
	ENDIF
	INT iTotalPlayers = MC_serverBD.iNumberOfLBPlayers[0] + MC_serverBD.iNumberOfLBPlayers[1]+ MC_serverBD.iNumberOfLBPlayers[2]+ MC_serverBD.iNumberOfLBPlayers[3]
	IF iTotalPlayers > 8
		iTotalPlayers = 8
	ENDIF
	INT iaverageRank = GET_AVERAGE_RANK_OF_PLAYERS_IN_JOB(iTotalPlayers)
	IF iaverageRank > 40
		iaverageRank = 40
	ENDIF
	
	INT ikills = (MC_playerBD[iLocalPart].iNumPlayerKills + MC_playerBD[iLocalPart].iNumPedKills)
	
	FLOAT fkills = TO_FLOAT(ikills)
	fkills = fkills/2
	PRINTLN("[RCC MISSION] fkills: ",fkills)
	INT iplayerdiff = iTotalPlayers - iplayersOnTeam
	FLOAT fPlayers = TO_FLOAT(iplayerdiff)
	fPlayers = fPlayers/2.5
	PRINTLN("[RCC MISSION] fPlayers: ",fPlayers)
	FLOAT faveragerank = TO_FLOAT(iaverageRank)
	faveragerank = faveragerank/16
	PRINTLN("[RCC MISSION] faveragerank: ",faveragerank)

	fCash = (300+((100*(fPlayers)*fkills)*(1+faveragerank)))*2
	PRINTLN("[RCC MISSION] fCash: ",fCash)
	
	IF MC_playerBD[iLocalPart].iNumPlayerDeaths = 0
		fCash = fCash+1000
	ENDIF
	
	PRINTLN("[RCC MISSION] fCash after lives: ",fCash)
	
	IF NOT bPass
		PRINTLN("[RCC MISSION] LTS CASH being halved cause failed, old value: ",fCash)
		fCash = fCash/2
		PRINTLN("[RCC MISSION] LTS CASH being halved cause failed, 1/2 value: ",fCash)
	ENDIF
	
	fCash =	fCash/10
	icash =	ROUND(fCash)
	icash = icash*10

	IF icash < 100
		icash = 100
	ENDIF
	PRINTLN("[RCC MISSION] LTS CASH RETURNED IS: ",icash)
	
	return icash
	
ENDFUNC

FUNC INT CALCULATE_TRACKED_VEHICLE_DAMAGE()

	INT i = 0
	INT TOTAL_TRACKED_VEHICLES = 0
	FLOAT fhealth
	INT iReturn
	VEHICLE_INDEX vehPercentage
	FLOAT fPercentage
	
	FOR i = 0 TO (FMMC_MAX_VEHICLES - 1)
		IF IS_BIT_SET(G_FMMC_STRUCT.iHeistCSVehDmgTrackBitSet, i)
			IF IS_NET_VEHICLE_DRIVEABLE(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
				vehPercentage = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
				
				fPercentage = GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(vehPercentage, i, MC_serverBD.iTotalNumStartingPlayers)
				
				fhealth += fPercentage
				
				PRINTLN("[RCC_MISSION] [CASH] CALCULATE_TRACKED_VEHICLE_DAMAGE iveh = ", i ,"   fhealth   " , fhealth)
				TOTAL_TRACKED_VEHICLES++
			ENDIF
		ENDIF 
	ENDFOR 
	
	iReturn = ROUND(fhealth / TOTAL_TRACKED_VEHICLES)
	
	PRINTLN("[RCC_MISSION] [CASH] CALCULATE_TRACKED_VEHICLE_DAMAGE = ", iReturn )
	
	RETURN iReturn
	
ENDFUNC

FUNC INT CALCULATE_CONTACT_MISSION_CASH_REWARD(BOOL bPass, INT iTotalMissionEndTime)

	INT iCashReturn
	FLOAT fCashReward
	FLOAT fPlayerMulti = 1.0
	
	IF HAS_LOCAL_PLAYER_FAILED()
	AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciMISSION_BLOCK_REWARDS_ON_FAIL)
		iCashReturn = 0
		PRINTLN("[JS] - CALCULATE_CONTACT_MISSION_CASH_REWARD - Block Rewards On Player Fail is set, returning 0 as cash reward")
		RETURN iCashReturn
	ENDIF

	INT iTotalPlayers = MC_serverBD.iNumberOfLBPlayers[0] + MC_serverBD.iNumberOfLBPlayers[1]+ MC_serverBD.iNumberOfLBPlayers[2]+ MC_serverBD.iNumberOfLBPlayers[3]
	IF iTotalPlayers = 1
		fPlayerMulti = g_sMPTunables.fContact_Mission_Cash_Player_Multiplier_1         
	ELIF iTotalPlayers = 2
		fPlayerMulti = g_sMPTunables.fContact_Mission_Cash_Player_Multiplier_2 
	ELIF iTotalPlayers = 3
		fPlayerMulti = g_sMPTunables.fContact_Mission_Cash_Player_Multiplier_3 
	ELSE
		fPlayerMulti = g_sMPTunables.fContact_Mission_Cash_Player_Multiplier_4 
	ENDIF
	PRINTLN("[RCC MISSION][CASH_RWD] fPlayerMulti: ",fPlayerMulti)
	FLOAT fBaseMulti = g_sMPTunables.fContact_Mission_Cash_Base_Multiplier     
	PRINTLN("[RCC MISSION][CASH_RWD] fBaseMulti: ",fBaseMulti)

	INT iMissionRank = g_FMMC_STRUCT.iRank
	PRINTLN("[RCC MISSION][CASH_RWD] mission rank: ",iMissionRank)
	
	IF IS_LOADED_MISSION_LOW_RIDER_FLOW_MISSION()
		iMissionRank = g_sMPTunables.iLOWRIDER_FLOW_MISSION_REWARD_RANK_OVERRIDE
		PRINTLN("[RCC MISSION][CASH_RWD] IS_LOADED_MISSION_LOW_RIDER_FLOW_MISSION mission rank: ",iMissionRank)
	ELIF IS_THIS_ROOT_CONTENT_ID_AN_ASSASSINATION_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
		iMissionRank = g_sMPTunables.iASSASSINATION_FLOW_MISSION_REWARD_RANK_OVERRIDE
		PRINTLN("[RCC MISSION][CASH_RWD] IS_THIS_ROOT_CONTENT_ID_AN_ASSASSINATION_MISSION mission rank: ",iMissionRank)
	ENDIF
	
	IF iMissionRank > g_sMPTunables.iContact_Mission_Cash_Rank_Cap 
		iMissionRank = g_sMPTunables.iContact_Mission_Cash_Rank_Cap 
		PRINTLN("[RCC MISSION][CASH_RWD] mission rank cap: ",iMissionRank)
	ENDIF

	iMissionRank = iMissionRank+g_sMPTunables.iContact_Mission_Cash_Basic_Value
	
	PRINTLN("[RCC MISSION][CASH_RWD] modded mission rank: ",iMissionRank)
	
	FLOAT fDiffMulti = 1.0
	IF g_FMMC_STRUCT.iDifficulity = DIFF_EASY
		fDiffMulti = g_sMPTunables.fContact_Mission_Cash_Difficulty_Multiplier_Easy  
	ELIF g_FMMC_STRUCT.iDifficulity = DIFF_NORMAL
		fDiffMulti = g_sMPTunables.fContact_Mission_Cash_Difficulty_Multiplier_Normal 
	ELSE
		fDiffMulti = g_sMPTunables.fContact_Mission_Cash_Difficulty_Multiplier_Hard 
	ENDIF
	PRINTLN("[RCC MISSION][CASH_RWD] iDifficulity multi: ",fDiffMulti)
	
	PRINTLN("[RCC MISSION][CASH_RWD] iTotalMissionEndTime used for fTimeMulti: ",iTotalMissionEndTime)
	
	FLOAT fTimeMulti = 1.0
	IF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_1*60000)		
		fTimeMulti = g_sMPTunables.fContact_Mission_Cash_Time_Period_1_Percentage    
	ELIF iTotalMissionEndTime<= (g_sMPTunables.iContact_Mission_Time_Period_2*60000)	
		fTimeMulti = g_sMPTunables.fContact_Mission_Cash_Time_Period_2_Percentage  
	ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_3*60000)	
		fTimeMulti = g_sMPTunables.fContact_Mission_Cash_Time_Period_3_Percentage  
	ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_4*60000)	
		fTimeMulti = g_sMPTunables.fContact_Mission_Cash_Time_Period_4_Percentage  
	ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_5*60000)	
		fTimeMulti = g_sMPTunables.fContact_Mission_Cash_Time_Period_5_Percentage  
	ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_6*60000)	
		fTimeMulti = g_sMPTunables.fContact_Mission_Cash_Time_Period_6_Percentage  
	ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_7*60000)	
		fTimeMulti = g_sMPTunables.fContact_Mission_Cash_Time_Period_7_Percentage  
	ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_8*60000)	
		fTimeMulti = g_sMPTunables.fContact_Mission_Cash_Time_Period_8_Percentage  
	ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_9*60000)	
		fTimeMulti = g_sMPTunables.fContact_Mission_Cash_Time_Period_9_Percentage  
	ELSE
		fTimeMulti = g_sMPTunables.fContact_Mission_Cash_Time_Period_10_Percentage  
	ENDIF
	
	PRINTLN("[RCC MISSION][CASH_RWD] fTimeMulti : ",fTimeMulti)
	
	fCashReward = fPlayerMulti*fBaseMulti*iMissionRank*fDiffMulti*fTimeMulti
	
	
	IF NOT bPass
		
		IF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_1*60000)	
		
			// added to fix 1930038 KW 18/7/2014
			iCashReturn = 200
			fCashReward = 200
			IF iCashReturn > g_sMPTunables.iContact_Mission_Fail_Cash_Minimum 
				iCashReturn =g_sMPTunables.iContact_Mission_Fail_Cash_Minimum 
				fCashReward = TO_FLOAT(g_sMPTunables.iContact_Mission_Fail_Cash_Minimum )
			ENDIF
			
			
			PRINTLN("[RCC MISSION][CASH_RWD] LOSE contact setting cash reward to 200 since under 1 mins: ",fCashReward)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_2*60000)	
			fCashReward = fCashReward/g_sMPTunables.fContact_Mission_Fail_Cash_Time_Period_2_Divider  
			PRINTLN("[RCC MISSION][CASH_RWD] LOSE contact setting cash reward since under 2 mins to: ",fCashReward)
		ELIF iTotalMissionEndTime<= (g_sMPTunables.iContact_Mission_Time_Period_3*60000)	
			fCashReward = fCashReward/g_sMPTunables.fContact_Mission_Fail_Cash_Time_Period_3_Divider  
			PRINTLN("[RCC MISSION][CASH_RWD] LOSE contact setting cash reward since under 3 mins to: ",fCashReward)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_4*60000)	
			fCashReward = fCashReward/g_sMPTunables.fContact_Mission_Fail_Cash_Time_Period_4_Divider  
			PRINTLN("[RCC MISSION][CASH_RWD] LOSE contact setting cash reward since under 4 mins to: ",fCashReward)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_5*60000)	
			fCashReward = fCashReward/g_sMPTunables.fContact_Mission_Fail_Cash_Time_Period_5_Divider  
			PRINTLN("[RCC MISSION][CASH_RWD] LOSE contact setting cash reward since under 6 mins to: ",fCashReward)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_6*60000)	
			fCashReward = fCashReward/g_sMPTunables.fContact_Mission_Fail_Cash_Time_Period_6_Divider  
			PRINTLN("[RCC MISSION][CASH_RWD] LOSE contact setting cash reward since under 8 mins to: ",fCashReward)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_7*60000)		
			fCashReward = fCashReward/g_sMPTunables.fContact_Mission_Fail_Cash_Time_Period_7_Divider  
			PRINTLN("[RCC MISSION][CASH_RWD] LOSE contact setting cash reward since under 10 mins to: ",fCashReward)	
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_8*60000)	
			fCashReward = fCashReward/g_sMPTunables.fContact_Mission_Fail_Cash_Time_Period_8_Divider  
			PRINTLN("[RCC MISSION][CASH_RWD] LOSE contact setting cash reward since under 12 mins to: ",fCashReward)							
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iContact_Mission_Time_Period_9*60000)		
			fCashReward = fCashReward/g_sMPTunables.fContact_Mission_Fail_Cash_Time_Period_9_Divider  
			PRINTLN("[RCC MISSION][CASH_RWD] LOSE contact setting cash reward since under 15 mins to: ",fCashReward)						
		ELSE
			fCashReward = fCashReward/g_sMPTunables.fContact_Mission_Fail_Cash_Time_Period_10_Divider  
			PRINTLN("[RCC MISSION][CASH_RWD] LOSE contact setting cash reward since over 15 mins to: ",fCashReward)
		ENDIF
	ENDIF
		
	iCashReturn = ROUND(fCashReward)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciMISSION_PENALISE_FOR_DMG)
	
		//Reduced Reward= 'Normal Cash Reward' * (1-((100-('Vehicle Health Percentage'))/2)/100)
		//g_sMPTunables.ilow_flow_damage_reward_reduction_divider			= 2
		//g_sMPTunables.ilow_flow_damage_reward_reduction_max_threshold 	= 90
		//g_sMPTunables.ilow_flow_damage_reward_reduction_min_threshold 	= 50
	
		PRINTLN("[RCC_MISSION] [CASH] ciMISSION_PENALISE_FOR_DM is turned on")
	
		INT iAvgPercentageVehHealth = CALCULATE_TRACKED_VEHICLE_DAMAGE()
		
		PRINTLN("[RCC_MISSION] [CASH] ciMISSION_PENALISE_FOR_DM iDamageCashReduction: ",iAvgPercentageVehHealth)
		
		IF iAvgPercentageVehHealth < g_sMPTunables.ilow_flow_damage_reward_reduction_max_threshold
		
			//Send a text message if you damaged the vehicles a bit - readded
			IF HAS_TEAM_PASSED_MISSION(MC_playerBD[iPartToUse].iTeam)
				FM_LOW_FLOW_SENT_SEND_DAMAGED_CARS_MESS_IF_ON_CORRECT_MISSION()
				PRINTLN("[RCC_MISSION] [CASH] ciMISSION_PENALISE_FOR_DM is turned on - Vehicles too badly damaged - TEXT SENT")
			ENDIF

			//Cap this value to 100 if we have somehow gone over it.
			IF iAvgPercentageVehHealth > 100
				iAvgPercentageVehHealth = 100
				PRINTLN("[RCC_MISSION] [CASH] ciMISSION_PENALISE_FOR_DM is turned on -  Average health above 100? Capping to 100.")
			ENDIF
			
			//If vehicle health very low cap it to the min threshold.
			IF iAvgPercentageVehHealth < g_sMPTunables.ilow_flow_damage_reward_reduction_min_threshold 
				iAvgPercentageVehHealth = g_sMPTunables.ilow_flow_damage_reward_reduction_min_threshold 
				PRINTLN("[RCC_MISSION] [CASH] ciMISSION_PENALISE_FOR_DM is turned on -  Capping to min threshold.")
			ENDIF	
			
			PRINTLN("[RCC_MISSION] [CASH] Unmodified iCashReturn",iCashReturn)
			
			//(1-(((100-iAvgPercentageVehHealth)/g_sMPTunables.ilow_flow_damage_reward_reduction_divider)/100))
			
			FLOAT fVeh1 = TO_FLOAT(100-iAvgPercentageVehHealth)
			PRINTLN("[RCC_MISSION] [CASH] fVeh1",fVeh1)
			
			FLOAT fTune = TO_FLOAT(g_sMPTunables.ilow_flow_damage_reward_reduction_divider)
			PRINTLN("[RCC_MISSION] [CASH] fTune",fTune)
			
			FLOAT fVeh2 = fVeh1/fTune
			PRINTLN("[RCC_MISSION] [CASH] fVeh2",fVeh2)
			
			FLOAT fVeh3 = fVeh2 / 100.0
			PRINTLN("[RCC_MISSION] [CASH] fVeh3",fVeh3)
			
			FLOAT fMulti = 1- fVeh3
			PRINTLN("[RCC_MISSION] [CASH] fMulti",fMulti)
			
			FLOAT fCalculatedCash = iCashReturn * fMulti
			PRINTLN("[RCC_MISSION] [CASH] fCalculatedCash",fCalculatedCash)
			
			iCashReturn= ROUND(fCalculatedCash)
			
		ENDIF

	ENDIF
	
	IF iCashReturn < g_sMPTunables.iContact_Mission_Fail_Cash_Minimum 
		iCashReturn = g_sMPTunables.iContact_Mission_Fail_Cash_Minimum 
		PRINTLN("[RCC MISSION][CASH_RWD] iCashReturn setting min value : ",iCashReturn)
	ENDIF
	
	PRINTLN("[RCC MISSION][CASH_RWD] CALCULATE_CONTACT_MISSION_CASH_REWARD iCashReturn : ",iCashReturn)
		
	
	RETURN iCashReturn
ENDFUNC

FUNC BOOL ARE_WE_ON_WINNING_ROUNDS_TEAM()
	INT iLoop
	FOR iLoop = 0 TO (MAX_NUM_MC_PLAYERS - 1)
		IF GlobalplayerBD_FM_2[iLoop].sJobRoundData.iRoundScore > g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds
			IF MC_playerBD[iPartToUse].iTeam = MC_playerBD[iLoop].iTeam
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC INT CALCULATE_VS_CASH_REWARD(BOOL bPass,INT iTeamPos, INT iTotalMissionEndTime)

INT iCashGained
FLOAT fCashGained

	IF HAS_LOCAL_PLAYER_FAILED()
	AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciMISSION_BLOCK_REWARDS_ON_FAIL)
		iCashGained = 0
		PRINTLN("[JS] - CALCULATE_VS_CASH_REWARD - Block Rewards On Player Fail is set, returning 0 as cash reward")
		RETURN iCashGained
	ENDIF
	
	iCashGained = GET_CASH_VALUE_FROM_CREATOR(g_FMMC_STRUCT.iCashReward)
	fCashGained = TO_FLOAT(iCashGained)
	PRINTLN("[RCC MISSION][CASH_RWD] VS CASH Base from cloud: ",fCashGained)

	IF Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer)
		iCashGained = g_sMPTunables.ixp_tuneable_ctf_cash_reward
		fCashGained = TO_FLOAT(iCashGained)
		PRINTLN("[RCC MISSION][CASH_RWD] g_sMPTunables.ixp_tuneable_ctf_cash_reward: ",iCashGained)
	ENDIF
	
	IF SHOULD_USE_CLOUD_OVERRIDE()
		iCashGained = 20000
		fCashGained = TO_FLOAT(iCashGained)
		PRINTLN("[RCC MISSION][CASH_RWD] VS CASH SHOULD_USE_CLOUD_OVERRIDE: 20000 ")
	ENDIF
	
	IF bPass
		
		IF IS_VS_TIME_BELOW_MIN_REWARD_THRESHOLD(iTotalMissionEndTime)
			PRINTLN("[RCC MISSION][CASH_RWD] VS CASH set to 200 since under min time: ")
			iCashGained = g_sMPTunables.iVERSUS_WINNER_MINIMUM_CASH 	
			IF Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer)
				IF g_sMPTunables.ixp_tuneable_ctf_cash_reward < 200
					iCashGained =g_sMPTunables.ixp_tuneable_ctf_cash_reward 
				ENDIF
			ELIF (IS_THIS_ROCKSTAR_MISSION_NEW_VS_SUMO(g_FMMC_STRUCT.iRootContentIdHash) OR  IS_THIS_ROCKSTAR_MISSION_NEW_VS_RunningBack(g_FMMC_STRUCT.iRootContentIdHash))
				iCashGained = g_sMPTunables.iADVERSARY_WINNER_QUICK_MATCH_MINIMUM_CASH	
				PRINTLN("[RCC MISSION][CASH_RWD] VS CASH new quick match under min time: ",iCashGained)
			ENDIF
			fCashGained = TO_FLOAT(iCashGained)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_2*60000)
			fCashGained = fCashGained/g_sMPTunables.fVersus_Winner_Cash_divider_time_1
			PRINTLN("[RCC MISSION][CASH_RWD] VS CASH reduced since under 3 mins: ",fCashGained)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_3*60000)
			fCashGained = fCashGained/g_sMPTunables.fVersus_Winner_Cash_divider_time_2
			PRINTLN("[RCC MISSION][CASH_RWD] VS CASH reduced since under 5 mins: ",fCashGained)
		ELIF iTotalMissionEndTime <=  (g_sMPTunables.fVersus_Mission_time_4*60000)
			fCashGained = fCashGained/g_sMPTunables.fVersus_Winner_Cash_divider_time_3
			PRINTLN("[RCC MISSION][CASH_RWD] VS CASH reduced since under 7.5 mins: ",fCashGained)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_5*60000)
			fCashGained = fCashGained/g_sMPTunables.fVersus_Winner_Cash_divider_time_4
			PRINTLN("[RCC MISSION][CASH_RWD] VS CASH reduced since under 10 mins: ",fCashGained)	
		ELIF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_6*60000)	
			fCashGained = fCashGained/g_sMPTunables.fVersus_Winner_Cash_divider_time_5
			PRINTLN("[RCC MISSION][CASH_RWD] VS CASH reduced since under 15 mins: ",fCashGained)	
		ELSE
			fCashGained = fCashGained/g_sMPTunables.fVersus_Winner_Cash_divider_time_6
			PRINTLN("[RCC MISSION][CASH_RWD] VS CASH reduced since over 15 mins: ",fCashGained)
		ENDIF
		iCashGained = ROUND(fCashGained)
		IF ARE_WE_ON_WINNING_ROUNDS_TEAM()
			IF NOT g_sMPTunables.bADVERSARY_MATCH_BONUS_DISABLE
				FLOAT fTimeScratch = TO_FLOAT(g_iRoundsCombinedTime/1000)
				fTimeScratch = fTimeScratch/60
				INT iTimeToCalc = FLOOR(fTimeScratch)
				IF iTimeToCalc > g_sMPTunables.iADVERSARY_MATCH_BONUS_TIME_CAP
					iTimeToCalc = g_sMPTunables.iADVERSARY_MATCH_BONUS_TIME_CAP
				ENDIF
				g_iCachedMatchBonus = iTimeToCalc*g_sMPTunables.iADVERSARY_MATCH_BONUS_TIME_MULTIPLIER
				iCashGained += g_iCachedMatchBonus
				PRINTLN("[RCC MISSION][CASH_RWD] VS CASH - g_iRoundsCombinedTime - match bonus ", g_iRoundsCombinedTime)
				PRINTLN("[RCC MISSION][CASH_RWD] VS CASH - g_iCachedMatchBonus - match bonus ", g_iCachedMatchBonus)
			ELSE
				PRINTLN("[RCC MISSION][CASH_RWD] VS CASH - bonus disabled")
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION][CASH_RWD] VS CASH - not the end of the round")
		ENDIF
	ELSE
		IF IS_VS_TIME_BELOW_MIN_REWARD_THRESHOLD(iTotalMissionEndTime)
			iCashGained = 0 	
			fCashGained = TO_FLOAT(iCashGained)
			IF Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer)
				IF g_sMPTunables.ixp_tuneable_ctf_cash_reward < 200
					iCashGained =g_sMPTunables.ixp_tuneable_ctf_cash_reward 
					fCashGained = TO_FLOAT(iCashGained)
				ENDIF
			ELIF (IS_THIS_ROCKSTAR_MISSION_NEW_VS_SUMO(g_FMMC_STRUCT.iRootContentIdHash) OR  IS_THIS_ROCKSTAR_MISSION_NEW_VS_RunningBack(g_FMMC_STRUCT.iRootContentIdHash))
				iCashGained = g_sMPTunables.iADVERSARY_WINNER_QUICK_MATCH_MINIMUM_CASH	
				PRINTLN("[RCC MISSION][CASH_RWD] VS CASH new quick match LOSE under min time: ",iCashGained)
				fCashGained = TO_FLOAT(iCashGained)
				IF iTeamPos = 2 OR ALL_TEAMS_FAILED()
					fCashGained = fCashGained*g_sMPTunables.fADVERSARY_LOSER1_QUICK_MATCH_MINIMUM_CASH_MULTIPLIER 
				ELIF iTeamPos = 3
					fCashGained = fCashGained*g_sMPTunables.fADVERSARY_LOSER2_QUICK_MATCH_MINIMUM_CASH_MULTIPLIER 
				ELSE
					fCashGained = fCashGained*g_sMPTunables.fADVERSARY_LOSER3_QUICK_MATCH_MINIMUM_CASH_MULTIPLIER 
				ENDIF
			ENDIF
			PRINTLN("[RCC MISSION][CASH_RWD] LOSE VS setting cash reward since under 30 secs: ",fCashGained)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_2*60000)
			IF iTeamPos = 2 OR ALL_TEAMS_FAILED() 
				fCashGained = fCashGained/g_sMPTunables.fVersus_Loser_1_cash_divider_time_1	
			ELIF iTeamPos = 3
				fCashGained = fCashGained/g_sMPTunables.fVersus_Loser_2_cash_divider_time_1
			ELSE
				fCashGained = fCashGained/g_sMPTunables.fVersus_Loser_3_cash_divider_time_1
			ENDIF
			PRINTLN("[RCC MISSION][CASH_RWD] LOSE VS setting cash reward to: ",fCashGained, " since under 3 mins ")	
		ELIF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_3*60000)
			IF iTeamPos = 2 OR ALL_TEAMS_FAILED()
				fCashGained = fCashGained/g_sMPTunables.fVersus_Loser_1_cash_divider_time_2
			ELIF iTeamPos = 3
				fCashGained = fCashGained/g_sMPTunables.fVersus_Loser_2_cash_divider_time_2
			ELSE
				fCashGained = fCashGained/g_sMPTunables.fVersus_Loser_3_cash_divider_time_2
			ENDIF
			PRINTLN("[RCC MISSION][CASH_RWD] LOSE VS setting cash reward to: ",fCashGained, " since under 5 mins ")	
		ELIF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_4*60000)
			IF iTeamPos = 2 OR ALL_TEAMS_FAILED()
				fCashGained = fCashGained/g_sMPTunables.fVersus_Loser_1_cash_divider_time_3
			ELIF iTeamPos = 3
				fCashGained = fCashGained/g_sMPTunables.fVersus_Loser_2_cash_divider_time_3
			ELSE
				fCashGained = fCashGained/g_sMPTunables.fVersus_Loser_3_cash_divider_time_3
			ENDIF
			PRINTLN("[RCC MISSION][CASH_RWD] LOSE VS setting cash reward to: ",fCashGained, " since under 7.5 mins ")	
		ELIF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_5*60000)
			IF iTeamPos = 2 OR ALL_TEAMS_FAILED()
				fCashGained = fCashGained/g_sMPTunables.fVersus_Loser_1_cash_divider_time_4
			ELIF iTeamPos = 3
				fCashGained = fCashGained/g_sMPTunables.fVersus_Loser_2_cash_divider_time_4
			ELSE
				fCashGained = fCashGained/g_sMPTunables.fVersus_Loser_3_cash_divider_time_4
			ENDIF
			PRINTLN("[RCC MISSION][CASH_RWD] LOSE VS setting cash reward to: ",fCashGained, " since under 10 mins ")
		ELIF iTotalMissionEndTime <= (g_sMPTunables.fVersus_Mission_time_6*60000)
			IF iTeamPos = 2 OR ALL_TEAMS_FAILED()
				fCashGained = fCashGained/g_sMPTunables.fVersus_Loser_1_cash_divider_time_5
			ELIF iTeamPos = 3
				fCashGained = fCashGained/g_sMPTunables.fVersus_Loser_2_cash_divider_time_5
			ELSE
				fCashGained = fCashGained/g_sMPTunables.fVersus_Loser_3_cash_divider_time_5
			ENDIF
			PRINTLN("[RCC MISSION][CASH_RWD] LOSE VS setting cash reward to: ",fCashGained, " since under 15 mins ")
		ELSE
			IF iTeamPos = 2 OR ALL_TEAMS_FAILED()
				fCashGained = fCashGained/g_sMPTunables.fVersus_Loser_1_cash_divider_time_6
			ELIF iTeamPos = 3
				fCashGained = fCashGained/g_sMPTunables.fVersus_Loser_2_cash_divider_time_6
			ELSE
				fCashGained = fCashGained/g_sMPTunables.fVersus_Loser_3_cash_divider_time_6
			ENDIF
			PRINTLN("[RCC MISSION][CASH_RWD] LOSE VS setting cash reward to: ",fCashGained, " since over 15 mins ")
		ENDIF
		iCashGained = ROUND(fCashGained)
		IF ARE_WE_ON_WINNING_ROUNDS_TEAM()
			IF NOT g_sMPTunables.bADVERSARY_MATCH_BONUS_DISABLE
				PRINTLN("[RCC MISSION][CASH_RWD] VS CASH - g_iRoundsCombinedTime - start ", g_iRoundsCombinedTime)
				FLOAT fTimeScratch = TO_FLOAT(g_iRoundsCombinedTime/1000)
				fTimeScratch = fTimeScratch/60
				INT iTimeToCalc = FLOOR(fTimeScratch)
				IF iTimeToCalc > g_sMPTunables.iADVERSARY_MATCH_BONUS_TIME_CAP
					iTimeToCalc = g_sMPTunables.iADVERSARY_MATCH_BONUS_TIME_CAP
				ENDIF
				g_iCachedMatchBonus = iTimeToCalc*g_sMPTunables.iADVERSARY_MATCH_BONUS_TIME_MULTIPLIER
				iCashGained += g_iCachedMatchBonus
				PRINTLN("[RCC MISSION][CASH_RWD] VS CASH - g_iCachedMatchBonus - match bonus ", g_iCachedMatchBonus)
			ELSE
				PRINTLN("[RCC MISSION][CASH_RWD] VS CASH - bonus disabled")
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION][CASH_RWD] VS CASH - not the end of the round")
		ENDIF
		PRINTLN("[RCC MISSION][CASH_RWD] LOSE VS rounded cash reward returned: ",iCashGained)
	ENDIF
	
	RETURN iCashGained
	
ENDFUNC

FUNC INT HANDLE_FINAL_CASH_REWARD(BOOL bPass,INT iteampos,INT iTotalMissionEndTime)

	INT iCashGained 

//	IF Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)
//		iCashGained = GET_CASH_FOR_LTS(bPass)
//		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdMissionLengthTimer) <= ciMIN_REWARD_TIME
//			PRINTLN("[RCC MISSION][CASH_RWD] LTS CASH set to 0 since under min time: ")
//			iCashGained = 0
//		ENDIF
//	ELSE
	iCashGained = GET_CASH_VALUE_FROM_CREATOR(g_FMMC_STRUCT.iCashReward)
	PRINTLN("[RCC MISSION][CASH_RWD] Raw cash value from creator: ",iCashGained)
	
#IF FEATURE_GTAO_MEMBERSHIP
	PLAYER_INDEX piHeistLeader = INVALID_PLAYER_INDEX()
	IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
	OR Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
		INT iPart
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iPart
		
			IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_HEIST_HOST)
				RELOOP
			ENDIF
			
			piHeistLeader = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart))
			BREAKLOOP
		ENDREPEAT
	ENDIF
#ENDIF

	IF bPass
		IF g_bVSMission
		
			iCashGained = CALCULATE_VS_CASH_REWARD(bPass,iteampos,iTotalMissionEndTime)
			
			PRINTLN("[RCC MISSION][CASH_RWD] VS cash reward for pass: ",iCashGained)
			
		ELSE

			IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
				
				iCashGained = CALCULATE_HEIST_FINALE_CASH_REWARD( bPass, TRUE, iTotalMissionEndTime, #IF FEATURE_GTAO_MEMBERSHIP piHeistLeader, TRUE, #ENDIF MC_playerBD[NATIVE_TO_INT(LocalPlayer)].TSPlayerData.iCost,MC_serverBD.iCashGrabTotalDrop)
				
			ELIF Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
				
				iCashGained = CALCULATE_HEIST_PREP_CASH_REWARD( bPass, iTotalMissionEndTime, MC_serverBD_2.iHeistVehRepairCost, #IF FEATURE_GTAO_MEMBERSHIP piHeistLeader, TRUE, #ENDIF MC_playerBD[NATIVE_TO_INT(LocalPlayer)].TSPlayerData.iCost)
			
			ELIF GANGOPS_FLOW_IS_CURRENT_MISSION_A_GANGOPS_FINALE()
			
				iCashGained = CALCULATE_GANGOPS_FINALE_CASH_REWARD(bPass, TRUE, iTotalMissionEndTime, MC_serverBD.iTotalNumStartingPlayers, MC_playerBD[NATIVE_TO_INT(LocalPlayer)].TSPlayerData.iCost)
			
			ELIF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
				
				iCashGained = CALCULATE_GANGOPS_PREP_CASH_REWARD(bPass, iTotalMissionEndTime, MC_playerBD[NATIVE_TO_INT(LocalPlayer)].TSPlayerData.iCost)
			
			ELIF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
				
				iCashGained = CALCULATE_CASINO_HEIST_CASH_REWARD(bPass, MC_ServerBD.iCashGrabTotalTake, MC_serverBD.iTotalNumStartingPlayers, MC_playerBD[NATIVE_TO_INT(LocalPlayer)].TSPlayerData.iCost)
			
			ELSE
			
				IF NOT IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()	
					iCashGained = CALCULATE_CONTACT_MISSION_CASH_REWARD(TRUE,iTotalMissionEndTime)
				ENDIF

				IF Is_Player_Currently_On_MP_Contact_Mission(LocalPlayer)
					IF Is_This_Contact_Mission_First_Play()
						// KGM 13/4/14: Added so that I can still allow Full Cash to be given on a replay if the Contact Mission failed and no reward was given.
						Set_Contact_Mission_First_Play_Full_Cash_Given()
		            ENDIF
			    ENDIF
				
			ENDIF
		ENDIF
	ELSE
		IF g_bVSMission
		
			iCashGained = CALCULATE_VS_CASH_REWARD(bPass,iteampos,iTotalMissionEndTime)
			
			PRINTLN("[RCC MISSION][CASH_RWD] VS cash reward for Fail: ",iCashGained)
			

		ELSE

			IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
				
				iCashGained = CALCULATE_HEIST_FINALE_CASH_REWARD( bPass, TRUE, iTotalMissionEndTime, #IF FEATURE_GTAO_MEMBERSHIP piHeistLeader, TRUE, #ENDIF MC_playerBD[NATIVE_TO_INT(LocalPlayer)].TSPlayerData.iCost,MC_serverBD.iCashGrabTotalDrop)
				
			ELIF Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
				
				iCashGained = CALCULATE_HEIST_PREP_CASH_REWARD(bPass, iTotalMissionEndTime, MC_serverBD_2.iHeistVehRepairCost, #IF FEATURE_GTAO_MEMBERSHIP piHeistLeader, TRUE, #ENDIF MC_playerBD[NATIVE_TO_INT(LocalPlayer)].TSPlayerData.iCost)
			
			ELIF GANGOPS_FLOW_IS_CURRENT_MISSION_A_GANGOPS_FINALE()
			
				iCashGained = CALCULATE_GANGOPS_FINALE_CASH_REWARD(bPass, TRUE, iTotalMissionEndTime, MC_serverBD.iTotalNumStartingPlayers, MC_playerBD[NATIVE_TO_INT(LocalPlayer)].TSPlayerData.iCost)
			
			ELIF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
				
				iCashGained = CALCULATE_GANGOPS_PREP_CASH_REWARD(bPass, iTotalMissionEndTime, MC_playerBD[NATIVE_TO_INT(LocalPlayer)].TSPlayerData.iCost, MC_serverBD_2.iHeistVehRepairCost)
				
			ELIF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
				
				iCashGained = CALCULATE_CASINO_HEIST_CASH_REWARD(bPass, MC_ServerBD.iCashGrabTotalTake, MC_serverBD.iTotalNumStartingPlayers, MC_playerBD[NATIVE_TO_INT(LocalPlayer)].TSPlayerData.iCost)
				
			ELSE
			
				IF NOT IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()	
					iCashGained = CALCULATE_CONTACT_MISSION_CASH_REWARD(FALSE,iTotalMissionEndTime)
				ELSE
					iCashGained = 0
				ENDIF

			ENDIF

		ENDIF
	ENDIF
	//ENDIF

	PRINTLN("[RCC MISSION][CASH_RWD] g_FMMC_STRUCT.iCashReward: ",g_FMMC_STRUCT.iCashReward)
	PRINTLN("[RCC MISSION][CASH_RWD] iCashGained: ",iCashGained)

	iCashGained = MULTIPLY_CASH_BY_TUNABLE(iCashGained) 
	
	IF g_sMPTunables.fearnings_High_Rockstar_Missions_modifier != 0.0
		IF iCashGained > g_sMPTunables.fearnings_High_Rockstar_Missions_modifier
			iCashGained = ROUND(g_sMPTunables.fearnings_High_Rockstar_Missions_modifier)
			PRINTLN("[RCC MISSION][CASH_RWD] g_sMPTunables.fearnings_High_Rockstar_Missions_modifier is set capping cash at: ",iCashGained)
		ENDIF
	ENDIF
	IF g_sMPTunables.fearnings_Low_Rockstar_Missions_modifier != 0.0
		IF iCashGained < g_sMPTunables.fearnings_Low_Rockstar_Missions_modifier
			iCashGained =ROUND(g_sMPTunables.fearnings_Low_Rockstar_Missions_modifier)
			PRINTLN("[RCC MISSION][CASH_RWD] g_sMPTunables.fearnings_Low_Rockstar_Missions_modifier is set capping cash at: ",iCashGained)
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		iDebugRewardCash = iCashGained
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		iDebugPPRewardCashTune = iCashGained
	#ENDIF
	
	PRINTLN("[RCC MISSION][CASH_RWD] icash per player after tunable: ",iCashGained)
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		iCashGained = 0
	ENDIF
	
	RETURN iCashGained

ENDFUNC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: FAIL REASONS !
//
//************************************************************************************************************************************************************





FUNC STRING GET_REASON_FOR_PLAYER_FAIL(INT ireason)

	SWITCH ireason
	
		CASE PLAYER_FAIL_OUT_OF_LIVES
			RETURN "LLIVEEXP"
		BREAK
		CASE PLAYER_FAIL_REASON_BOUNDS
			RETURN "POUTBND"
		BREAK
		
	ENDSWITCH

	RETURN "PLYFAIL"
	
ENDFUNC

FUNC BOOL IS_CUSTOM_ENTITY_NAME_FAIL_TEXT_VALID(INT iTeam)
	
	IF iTeam < FMMC_MAX_TEAMS
	AND iTeam >= 0
		
		IF (MC_serverBD.iEntityCausingFail[iteam] > -1)
			
			if MC_serverBD.eCurrentTeamFail[iTeam] = mFail_PED_AGRO_T0
				IF (MC_serverBD.iEntityCausingFail[0] < FMMC_MAX_PEDS)
				AND (MC_serverBD.iEntityCausingFail[0] > -1)
				AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[0]].iCustomPedName != -1)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[0]].iCustomPedName])
						RETURN TRUE
					ENDIF
				ELSE	
					RETURN FALSE
				ENDIF
			endif
			if MC_serverBD.eCurrentTeamFail[iTeam] = mFail_PED_AGRO_T1
				IF (MC_serverBD.iEntityCausingFail[1] < FMMC_MAX_PEDS)
				AND (MC_serverBD.iEntityCausingFail[1] > -1)
				AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[1]].iCustomPedName != -1)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[1]].iCustomPedName])
						RETURN TRUE
					ENDIF
				ELSE	
					RETURN FALSE
				ENDIF
			endif
			if MC_serverBD.eCurrentTeamFail[iTeam] = mFail_PED_AGRO_T2
				IF (MC_serverBD.iEntityCausingFail[2] < FMMC_MAX_PEDS)
				AND (MC_serverBD.iEntityCausingFail[2] > -1)
				AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[2]].iCustomPedName != -1)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[2]].iCustomPedName])
						RETURN TRUE
					ENDIF
				ELSE	
					RETURN FALSE
				ENDIF
			endif
			if MC_serverBD.eCurrentTeamFail[iTeam] = mFail_PED_AGRO_T3
				IF (MC_serverBD.iEntityCausingFail[3] < FMMC_MAX_PEDS)
				AND (MC_serverBD.iEntityCausingFail[3] > -1)
				AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[3]].iCustomPedName != -1)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[3]].iCustomPedName])
						RETURN TRUE
					ENDIF
				ELSE	
					RETURN FALSE
				ENDIF
			endif
			
			IF MC_serverBD.eCurrentTeamFail[iTeam] = mfail_CC_PED_DEAD
			OR MC_serverBD.eCurrentTeamFail[iTeam] = mFail_CC_PED_HERO_GUN
			OR MC_serverBD.eCurrentTeamFail[iTeam] = mFail_CC_PED_HERO_PHONE
			OR MC_serverBD.eCurrentTeamFail[iTeam] = mFail_CC_PED_HERO_ALARM			
			OR MC_serverBD.eCurrentTeamFail[iTeam] = mfail_PED_ARRIVED
			OR MC_serverBD.eCurrentTeamFail[iTeam] = mfail_PED_CAPTURED
			OR MC_serverBD.eCurrentTeamFail[iTeam] = mfail_PED_DEAD
			OR MC_serverBD.eCurrentTeamFail[iTeam] = mfail_PED_DELIVERED
			OR MC_serverBD.eCurrentTeamFail[iTeam] = mfail_PED_FOUND_BODY
			OR MC_serverBD.eCurrentTeamFail[iTeam] = mfail_PHOTO_PED
			OR MC_serverBD.eCurrentTeamFail[iTeam] = mfail_PROTECTED_PED
				CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_ENTITY_NAME_FAIL_TEXT_VALID MC_serverBD.iEntityCausingFail[iteam]: ", MC_serverBD.iEntityCausingFail[iteam])
				IF MC_serverBD.iEntityCausingFail[iteam] > -1
					CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_ENTITY_NAME_FAIL_TEXT_VALID iCustomPedName: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[iteam]].iCustomPedName)
				ENDIF
				
				IF (MC_serverBD.iEntityCausingFail[iteam] < FMMC_MAX_PEDS)
				AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[iteam]].iCustomPedName != -1)					
					CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_ENTITY_NAME_FAIL_TEXT_VALID tlCustomPedName: ", g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[iteam]].iCustomPedName])
					
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[iteam]].iCustomPedName])
						RETURN TRUE
					ENDIF
				ELSE	
					RETURN FALSE
				ENDIF
			ENDIF
			
			IF MC_serverBD.eCurrentTeamFail[iTeam] = mfail_VEH_CAPTURED
			OR MC_serverBD.eCurrentTeamFail[iTeam] = mfail_VEH_DELIVERED
			OR MC_serverBD.eCurrentTeamFail[iTeam] = mfail_VEH_DEAD
			OR MC_serverBD.eCurrentTeamFail[iTeam] = mfail_PROTECTED_VEH
			OR MC_serverBD.eCurrentTeamFail[iTeam] = mfail_PHOTO_VEH
				CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_ENTITY_NAME_FAIL_TEXT_VALID MC_serverBD.iEntityCausingFail[iteam]: ", MC_serverBD.iEntityCausingFail[iteam])
				IF MC_serverBD.iEntityCausingFail[iteam] > -1
					CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_ENTITY_NAME_FAIL_TEXT_VALID iCustomVehName: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[MC_serverBD.iEntityCausingFail[iteam]].iCustomVehName)
				ENDIF
				
				IF (MC_serverBD.iEntityCausingFail[iteam] < FMMC_MAX_VEHICLES)
				AND (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[MC_serverBD.iEntityCausingFail[iteam]].iCustomVehName != -1)
					CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_ENTITY_NAME_FAIL_TEXT_VALID tlCustomVehName: ", g_FMMC_STRUCT_ENTITIES.tlCustomVehName[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[MC_serverBD.iEntityCausingFail[iteam]].iCustomVehName])
					
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomVehName[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[MC_serverBD.iEntityCausingFail[iteam]].iCustomVehName])
						RETURN TRUE
					ENDIF
				ELSE	
					RETURN FALSE
				ENDIF
			ENDIF
			
			IF MC_serverBD.eCurrentTeamFail[iTeam] = mfail_OBJ_CAPTURED
			OR MC_serverBD.eCurrentTeamFail[iTeam] = mfail_OBJ_DELIVERED
			OR MC_serverBD.eCurrentTeamFail[iTeam] = mfail_OBJ_DEAD
			OR MC_serverBD.eCurrentTeamFail[iTeam] = mfail_PROTECTED_OBJ
			OR MC_serverBD.eCurrentTeamFail[iTeam] = mfail_PHOTO_OBJ
			OR MC_serverBD.eCurrentTeamFail[iTeam] = mfail_HACK_OBJ
				CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_ENTITY_NAME_FAIL_TEXT_VALID MC_serverBD.iEntityCausingFail[iteam]: ", MC_serverBD.iEntityCausingFail[iteam])
				IF MC_serverBD.iEntityCausingFail[iteam] > -1
					CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_ENTITY_NAME_FAIL_TEXT_VALID iCustomObjName: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[MC_serverBD.iEntityCausingFail[iteam]].iCustomObjName)
				ENDIF
				
				IF (MC_serverBD.iEntityCausingFail[iteam] < FMMC_MAX_NUM_OBJECTS)
				AND (g_FMMC_STRUCT_ENTITIES.sPlacedObject[MC_serverBD.iEntityCausingFail[iteam]].iCustomObjName != -1)
					CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_ENTITY_NAME_FAIL_TEXT_VALID tlCustomObjName: ", g_FMMC_STRUCT_ENTITIES.tlCustomObjName[g_FMMC_STRUCT_ENTITIES.sPlacedObject[MC_serverBD.iEntityCausingFail[iteam]].iCustomObjName])
					
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomObjName[g_FMMC_STRUCT_ENTITIES.sPlacedObject[MC_serverBD.iEntityCausingFail[iteam]].iCustomObjName])
						RETURN TRUE
					ENDIF
				ELSE	
					RETURN FALSE
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
	
	CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_ENTITY_NAME_FAIL_TEXT_VALID RETURN FALSE")
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_CUSTOM_ENTITY_NAME_END_TEXT_VALID(INT iTeam)
	
	IF iTeam < FMMC_MAX_TEAMS
	AND iTeam >= 0
		
		IF (MC_serverBD.iEntityCausingFail[iteam] > -1)
			
			IF ireasonObjEnd[iteam] 	= OBJ_END_REASON_CC_PED_DEAD
			OR ireasonObjEnd[iteam] 	= OBJ_END_REASON_PED_AGRO
			OR ireasonObjEnd[iteam] 	= OBJ_END_REASON_PED_ARRIVED
			OR ireasonObjEnd[iteam] 	= OBJ_END_REASON_PED_CAPTURED
			OR ireasonObjEnd[iteam] 	= OBJ_END_REASON_PED_DEAD
			OR ireasonObjEnd[iteam] 	= OBJ_END_REASON_PED_DELIVERED
			OR ireasonObjEnd[iteam] 	= OBJ_END_REASON_PED_FOUND_BODY
			OR ireasonObjEnd[iteam] 	= OBJ_END_REASON_PHOTO_PED
			OR ireasonObjEnd[iteam] 	= OBJ_END_REASON_PROTECTED_PED
				IF (MC_serverBD.iEntityCausingFail[iteam] < FMMC_MAX_PEDS)
				AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[iteam]].iCustomPedName != -1)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[iteam]].iCustomPedName])
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF ireasonObjEnd[iteam] 	= OBJ_END_REASON_VEH_CAPTURED
			OR ireasonObjEnd[iteam] 	= OBJ_END_REASON_VEH_DELIVERED
			OR ireasonObjEnd[iteam] 	= OBJ_END_REASON_VEH_DEAD
			OR ireasonObjEnd[iteam] 	= OBJ_END_REASON_PROTECTED_VEH
			OR ireasonObjEnd[iteam] 	= OBJ_END_REASON_PHOTO_VEH
				IF (MC_serverBD.iEntityCausingFail[iteam] < FMMC_MAX_VEHICLES)
				AND (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[MC_serverBD.iEntityCausingFail[iteam]].iCustomVehName != -1)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomVehName[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[MC_serverBD.iEntityCausingFail[iteam]].iCustomVehName])
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF ireasonObjEnd[iteam] 	= OBJ_END_REASON_OBJ_CAPTURED
			OR ireasonObjEnd[iteam] 	= OBJ_END_REASON_OBJ_DELIVERED
			OR ireasonObjEnd[iteam] 	= OBJ_END_REASON_OBJ_DEAD
			OR ireasonObjEnd[iteam] 	= OBJ_END_REASON_PROTECTED_OBJ
			OR ireasonObjEnd[iteam] 	= OBJ_END_REASON_PHOTO_OBJ
			OR ireasonObjEnd[iteam] 	= OBJ_END_REASON_HACK_OBJ
				IF (MC_serverBD.iEntityCausingFail[iteam] < FMMC_MAX_NUM_OBJECTS)
				AND (g_FMMC_STRUCT_ENTITIES.sPlacedObject[MC_serverBD.iEntityCausingFail[iteam]].iCustomObjName != -1)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomObjName[g_FMMC_STRUCT_ENTITIES.sPlacedObject[MC_serverBD.iEntityCausingFail[iteam]].iCustomObjName])
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
	
	CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_ENTITY_NAME_END_TEXT_VALID RETURN FALSE")
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_CUSTOM_END_TEXT_VALID(INT iTeam,bool bpass = true)
	CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_END_TEXT_VALID iTeam: ", iTeam, " bPass: ", bPass)
	
	// if we have an entity causing the fail then check it has a custom name
	IF (MC_serverBD.iEntityCausingFail[iteam] > -1)
		CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_END_TEXT_VALID MC_serverBD.iEntityCausingFail[iteam]: ", MC_serverBD.iEntityCausingFail[iteam])
		
		if bpass
			IF IS_CUSTOM_ENTITY_NAME_END_TEXT_VALID(iTeam)
				CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_END_TEXT_VALID RETURN TRUE (1)")
				RETURN TRUE
			ENDIF
		else		
			IF IS_CUSTOM_ENTITY_NAME_FAIL_TEXT_VALID(iTeam)
				CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_END_TEXT_VALID RETURN TRUE (2)")
				RETURN TRUE
			ENDIF			
		endif
	ELSE
		CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_END_TEXT_VALID MC_serverBD.iEntityCausingFail[iteam] = -1")
		
		//check if a custom fail for timer expired is being used.
		IF MC_serverBD.eCurrentTeamFail[iTeam] = mFail_MULTI_TIME_EXPIRED			
			IF NOT IS_STRING_NULL_OR_EMPTY(g_fmmc_struct.tl63MultiRuleTimerFailString[iTeam])
				CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_END_TEXT_VALID RETURN TRUE (3)")
				RETURN TRUE
			ENDIF	
		ELIF MC_serverBD.eCurrentTeamFail[iTeam] = mFail_TIME_EXPIRED			
			IF iObjEndPriority[iTeam] < FMMC_MAX_RULES
			AND iObjEndPriority[iTeam] >= 0					
				If IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iRuleBitsetThree[iObjEndPriority[iTeam]],ciBS_RULE3_OBJECTIVE_TIMER_FAIL1)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_fmmc_struct.tl63ObjectiveTimerFailString[iTeam][0])
						CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_END_TEXT_VALID RETURN TRUE (4)")
						RETURN TRUE
					ENDIF
				endif	
				if IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iRuleBitsetThree[iObjEndPriority[iTeam]],ciBS_RULE3_OBJECTIVE_TIMER_FAIL2)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_fmmc_struct.tl63ObjectiveTimerFailString[iTeam][1])
						CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_END_TEXT_VALID RETURN TRUE (5)")
						RETURN TRUE
					ENDIF			
				endif
			ENDIF
		ELSE	
			if not (MC_serverBD.eCurrentTeamFail[iTeam] >= mFail_PED_AGRO_T0
			and MC_serverBD.eCurrentTeamFail[iTeam] <= mFail_PED_AGRO_T3)
			// if not entity do we have a custom objective we failed with
				IF iTeam < FMMC_MAX_TEAMS
				AND iTeam >= 0
					IF iObjEndPriority[iTeam] < FMMC_MAX_RULES
					AND iObjEndPriority[iTeam] >= 0
						IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl23ObjSingular[iObjEndPriority[iTeam]])
							CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_END_TEXT_VALID RETURN TRUE (6)")
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			endif
		endif		
		
	ENDIF
	
	CPRINTLN(DEBUG_MISSION,"[CV_FAIL]IS_CUSTOM_END_TEXT_VALID RETURN FALSE")
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_CUSTOM_FAIL_REASON_VALID(INT iTeam)
	// make sure custom strings are used for these kinds of fails.
	if not (MC_serverBD.eCurrentTeamFail[iTeam]  >=  mFail_T0MEMBER_DIED		
	and MC_serverBD.eCurrentTeamFail[iTeam] <=  mFail_T3MEMBER_DIED)
	and not (MC_serverBD.eCurrentTeamFail[iTeam]  >=  mFail_HEIST_TEAM_GONE_T0		
	and MC_serverBD.eCurrentTeamFail[iTeam] <=  mFail_HEIST_TEAM_GONE_T3)
	and not (MC_serverBD.eCurrentTeamFail[iTeam]  >=  mFail_HEIST_TEAM_T0_FAILED	
	and MC_serverBD.eCurrentTeamFail[iTeam] <=  mFail_HEIST_TEAM_T3_FAILED)	
	and not (MC_serverBD.eCurrentTeamFail[iTeam] = mFail_TEAM_GONE)
	AND NOT (MC_serverBD.eCurrentTeamFail[iTeam] = mFail_CEO_GONE)
	AND NOT (MC_serverBD.eCurrentTeamFail[iTeam] = mFail_MC_PRESIDENT_GONE)
	AND NOT (MC_serverBD.eCurrentTeamFail[iTeam] = mFail_SCORE_CANT_BE_MATCHED)
		IF iTeam < FMMC_MAX_TEAMS
		AND iTeam >= 0
		AND iObjEndPriority[iTeam] < FMMC_MAX_RULES
		AND iObjEndPriority[iTeam] >= 0
			IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63FailReason[iObjEndPriority[iTeam]])
				CPRINTLN(DEBUG_MISSION,"[CV_FAIL] IS_CUSTOM_FAIL_REASON_VALID (TRUE) : custom fail available for team on this rule: ",iObjEndPriority[iTeam])
				RETURN TRUE
			ELSE	
				CPRINTLN(DEBUG_MISSION,"[CV_FAIL] IS_CUSTOM_FAIL_REASON_VALID (FALSE) : custom fail not available for team on this rule: ",iObjEndPriority[iTeam])
			ENDIF
		ENDIF	
	ELSE
		CPRINTLN(DEBUG_MISSION,"[CV_FAIL] IS_CUSTOM_FAIL_REASON_VALID (FALSE) : custom fail block by fail type: ",enum_to_int(MC_serverBD.eCurrentTeamFail[iTeam]))
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC TEXT_LABEL_63 GET_OBJECTIVE_END_FAIL_ENTITY_NAME(INT iTeam, BOOL bCapitalise, BOOL bCustomEntityNameOnly = FALSE)
	
	TEXT_LABEL_63 strLiteral
	BOOL bCustomEntityName = FALSE
	
	IF (MC_serverBD.iEntityCausingFail[iteam] > -1)
		switch MC_serverBD.eCurrentTeamFail[iTeam]
			case mFail_PED_AGRO_T0
				IF (MC_serverBD.iEntityCausingFail[0] > -1)
				AND (MC_serverBD.iEntityCausingFail[0] < FMMC_MAX_PEDS)
				AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[0]].iCustomPedName != -1)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[0]].iCustomPedName])
						bCustomEntityName = TRUE
						strLiteral = g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[0]].iCustomPedName]
					ENDIF
				ENDIF
			break 
			case mFail_PED_AGRO_T1
				IF (MC_serverBD.iEntityCausingFail[1] > -1)
				AND (MC_serverBD.iEntityCausingFail[1] < FMMC_MAX_PEDS)
				AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[1]].iCustomPedName != -1)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[1]].iCustomPedName])
						bCustomEntityName = TRUE
						strLiteral = g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[1]].iCustomPedName]
					ENDIF
				ENDIF
			break
			case mFail_PED_AGRO_T2
				IF (MC_serverBD.iEntityCausingFail[2] > -1)
				AND (MC_serverBD.iEntityCausingFail[2] < FMMC_MAX_PEDS)
				AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[2]].iCustomPedName != -1)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[2]].iCustomPedName])
						bCustomEntityName = TRUE
						strLiteral = g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[2]].iCustomPedName]
					ENDIF
				ENDIF
			break
			case mFail_PED_AGRO_T3
				IF (MC_serverBD.iEntityCausingFail[3] > -1)
				AND (MC_serverBD.iEntityCausingFail[3] < FMMC_MAX_PEDS)
				AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[3]].iCustomPedName != -1)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[3]].iCustomPedName])
						bCustomEntityName = TRUE
						strLiteral = g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[3]].iCustomPedName]
					ENDIF
				ENDIF
			break
			case mfail_CC_PED_DEAD
			case mFail_CC_PED_HERO_GUN
			case mFail_CC_PED_HERO_PHONE
			case mFail_CC_PED_HERO_ALARM
			case mfail_PED_ARRIVED
			case mfail_PED_CAPTURED
			case mfail_PED_DEAD
			case mfail_PED_DELIVERED
			case mfail_PED_FOUND_BODY
			case mfail_PHOTO_PED
			case mfail_PROTECTED_PED
				IF (MC_serverBD.iEntityCausingFail[iteam] < FMMC_MAX_PEDS)
				AND  (MC_serverBD.iEntityCausingFail[iteam] > -1)
				AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[iteam]].iCustomPedName != -1)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[iteam]].iCustomPedName])
						bCustomEntityName = TRUE
						strLiteral = g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[MC_serverBD.iEntityCausingFail[iTeam]].iCustomPedName]
					ENDIF
				ENDIF
			break
			
			case mfail_VEH_CAPTURED
			case mfail_VEH_DELIVERED
			case mfail_VEH_DEAD
			case mfail_PROTECTED_VEH
			case mfail_PHOTO_VEH
				IF (MC_serverBD.iEntityCausingFail[iteam] < FMMC_MAX_VEHICLES)
				AND  (MC_serverBD.iEntityCausingFail[iteam] > -1)
				AND (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[MC_serverBD.iEntityCausingFail[iteam]].iCustomVehName != -1)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomVehName[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[MC_serverBD.iEntityCausingFail[iteam]].iCustomVehName])
						bCustomEntityName = TRUE
						strLiteral = g_FMMC_STRUCT_ENTITIES.tlCustomVehName[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[MC_serverBD.iEntityCausingFail[iteam]].iCustomVehName]
					ENDIF
				ENDIF
			BREAK
			
			case mfail_OBJ_CAPTURED
			case mfail_OBJ_DELIVERED
			case mfail_OBJ_DEAD
			case mfail_PROTECTED_OBJ
			case mfail_PHOTO_OBJ
			case mfail_HACK_OBJ
				IF (MC_serverBD.iEntityCausingFail[iteam] < FMMC_MAX_NUM_OBJECTS)
				AND  (MC_serverBD.iEntityCausingFail[iteam] > -1)
				AND (g_FMMC_STRUCT_ENTITIES.sPlacedObject[MC_serverBD.iEntityCausingFail[iteam]].iCustomObjName != -1)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomObjName[g_FMMC_STRUCT_ENTITIES.sPlacedObject[MC_serverBD.iEntityCausingFail[iteam]].iCustomObjName])
						bCustomEntityName = TRUE
						strLiteral = g_FMMC_STRUCT_ENTITIES.tlCustomObjName[g_FMMC_STRUCT_ENTITIES.sPlacedObject[MC_serverBD.iEntityCausingFail[iteam]].iCustomObjName]
					ENDIF
				ENDIF
			BREAK			
		endswitch
	Else
					
		IF MC_serverBD.eCurrentTeamFail[iTeam] = mFail_MULTI_TIME_EXPIRED
			IF NOT IS_STRING_NULL_OR_EMPTY(g_fmmc_struct.tl63MultiRuleTimerFailString[iTeam])
				strLiteral = g_fmmc_struct.tl63MultiRuleTimerFailString[iTeam]
				CPRINTLN(DEBUG_MISSION,"[CV_FAIL]GET_OBJECTIVE_END_FAIL_ENTITY_NAME tl63MultiRuleTimerFailString custom text: ",strLiteral)
			ENDIF	
		ELIF MC_serverBD.eCurrentTeamFail[iTeam] = mFail_TIME_EXPIRED			
			IF iObjEndPriority[iTeam] < FMMC_MAX_RULES
			AND iObjEndPriority[iTeam] >= 0							
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iRuleBitsetThree[iObjEndPriority[iTeam]],ciBS_RULE3_OBJECTIVE_TIMER_FAIL1)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_fmmc_struct.tl63ObjectiveTimerFailString[iTeam][0])
						strLiteral = g_fmmc_struct.tl63ObjectiveTimerFailString[iTeam][0]
						CPRINTLN(DEBUG_MISSION,"[CV_FAIL]GET_OBJECTIVE_END_FAIL_ENTITY_NAME ciBS_RULE3_OBJECTIVE_TIMER_FAIL1 custom text: ",strLiteral)
					ENDIF
				
				ELif IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iRuleBitsetThree[iObjEndPriority[iTeam]],ciBS_RULE3_OBJECTIVE_TIMER_FAIL2)
					IF NOT IS_STRING_NULL_OR_EMPTY(g_fmmc_struct.tl63ObjectiveTimerFailString[iTeam][1])
						strLiteral = g_fmmc_struct.tl63ObjectiveTimerFailString[iTeam][1]
						CPRINTLN(DEBUG_MISSION,"[CV_FAIL]GET_OBJECTIVE_END_FAIL_ENTITY_NAME ciBS_RULE3_OBJECTIVE_TIMER_FAIL2 custom text: ",strLiteral)
					ENDIF	
				ENDIF
			ENDIF			
		else		
			IF NOT bCustomEntityNameOnly		
				IF bCapitalise
					strLiteral = GET_STRING_WITH_UPPERCASE_AT_START(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl23ObjSingular[iObjEndPriority[iTeam]])
				ELSE
					strLiteral = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl23ObjSingular[iObjEndPriority[iTeam]]
				ENDIF
				CPRINTLN(DEBUG_MISSION,"[CV_FAIL]GET_OBJECTIVE_END_FAIL_ENTITY_NAME no fail entity so grab objective custom text: ",strLiteral)
			ENDIF
		endif
	endif
	
	IF bCustomEntityName
		//If strLiteral is already populated with a custom entity name, and it needs to be capitalised:
		IF bCapitalise
			strLiteral = GET_STRING_WITH_UPPERCASE_AT_START(strLiteral)
		ENDIF
	ENDIF
	
	RETURN strLiteral
	
ENDFUNC

FUNC STRING GET_TEAM_FAIL_REASON(INT iTeam,BOOL& bCapitalise)
		
	IF IS_CUSTOM_FAIL_REASON_VALID(iTeam)			
		RETURN "FAILCUST"		
	ENDIF
	
	// if you are the heist leader and you leave change to your team left
	if MC_serverBD.eCurrentTeamFail[iTeam] = mFail_HEIST_LEADER_LEFT				
	and IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_HEIST_HOST)
		return  "LYOUTEAMLFT"//mFail_YOU_LEFT
	endif
	
	BOOL bUseCustomEndText = IS_CUSTOM_END_TEXT_VALID(iTeam,false)
	
	switch MC_serverBD.eCurrentTeamFail[iTeam]
		case  	mFail_LOC_CAPTURED	
			IF bUseCustomEndText
				RETURN "LCAPCUST"
			ELSE
				RETURN "LCAPLOC"
			ENDIF
		break		
		case	mFail_PED_CAPTURED
			if bUseCustomEndText 
				return "LCAPCUST"	
			else 	
				return 	"LCAPPED"		
			endif		
		break
		case	mFail_VEH_CAPTURED
			if bUseCustomEndText 
				return "LCAPCUST"	
			else 	
				return 	"LCAPVEH"		
			endif	
		break
		case	mFail_OBJ_CAPTURED
			if bUseCustomEndText 
				return "LCAPCUST"	
			else 	
				return 	"LCAPOBJ"		
			endif	
		break
		case	mFail_PED_DELIVERED
			IF bUseCustomEndText
				RETURN "LDELCUST"
			ELSE
				RETURN "LDELPED"
			ENDIF		
		break
		case	mFail_VEH_DELIVERED
			IF bUseCustomEndText
				RETURN "LDELCUST"
			ELSE
				RETURN "LDELVEH"
			ENDIF		
		break
		case	mFail_OBJ_DELIVERED
			if bUseCustomEndText 
				return "LDELCUST"	
			else 	
				return 	"LDELOBJ"		
			endif		
		break
		case	mFail_PED_DEAD	
			IF bUseCustomEndText
				bCapitalise = TRUE
				RETURN "LDEADPCUST"
			ELSE
				RETURN "LDEADPED"
			ENDIF
		break
		case	mFail_VEH_DEAD
			IF bUseCustomEndText
				bCapitalise = TRUE
				RETURN "LDESCUST"
			ELSE
				RETURN "LDEADVEH"
			ENDIF	
		break
		case	mFail_OBJ_DEAD
			IF bUseCustomEndText
				bCapitalise = TRUE
				RETURN "LDESCUST"
			ELSE
				RETURN "LDEADOBJ"
			ENDIF	
		break
		case	mFail_PLAYERS_KILLED
			RETURN "LPLYKILL"
		break
		case	mFail_PROTECTED_PED
			IF bUseCustomEndText
				RETURN "LPROCUST"
			ELSE
				RETURN "LPROPED"
			ENDIF	
		break
		case	mFail_PROTECTED_VEH
			IF bUseCustomEndText
				RETURN "LPROCUST"
			ELSE
				RETURN "LPROVEH"
			ENDIF	
		break
		case	mFail_PROTECTED_OBJ
			if bUseCustomEndText 
				return "LPROCUST"	
			else 	
				return 	"LPROOBJ"		
			endif	
		break
		case	mFail_ARV_LOC
			IF bUseCustomEndText
				RETURN "LARVCUST"
			ELSE
				RETURN "LARVLOC"
			ENDIF	
		break
		case	mFail_ARV_PED
			if bUseCustomEndText 
				return "LARVCUST"	
			else 	
				return 	"LARVPED"		
			endif	
		break
		case	mFail_ARV_VEH
			if bUseCustomEndText 
				return "LARVCUST"	
			else 	
				return 	"LARVVEH"		
			endif	
		break
		case	mFail_ARV_OBJ
			if bUseCustomEndText 
				return "LARVCUST"	
			else 	
				return 	"LARVOBJ"		
			endif	
		break
		case	mFail_TIME_EXPIRED
		case	mFail_MULTI_TIME_EXPIRED
			if bUseCustomEndText
				Return "FAILCUST"
			else
				RETURN "LTIMEEXP"
			endif
		break
		
		case	mFail_TARGET_SCORE				
			return 	"LTRGTSCORE"	
		break
		case	mFail_PHOTO_LOC	
		case	mFail_PHOTO_PED	
		case	mFail_PHOTO_VEH	
		case	mFail_PHOTO_OBJ			
			if bUseCustomEndText 
				return "LPHOTOCUST"	
			else 	
				return 	"LPHOTO"		
			endif	
		break
		case	mFail_HACK_OBJ		
			if bUseCustomEndText 
				return "LHACKCUST"	
			else 	
				return 	"LHACK"		
			endif	
		break
		case	mFail_OUT_OF_LIVES		
			RETURN "LLIVESOBJ"	
		break
		case	mFail_PED_ARRIVED		
			IF bUseCustomEndText
				bCapitalise = TRUE
				RETURN "LPEDARVCUST"
			ELSE
				RETURN "LPEDARV"
			ENDIF	
		break
		case	mFail_TEAM_GONE			
			IF NOT DOES_TEAM_LIKE_TEAM(MC_playerBD[iPartToUse].iteam,iTeam)				
				RETURN "LOTHTEAMQ"			
			ELSE				
				RETURN "LMYTEAMQ"				
			ENDIF
		break
		case	mFail_LOST_COPS	
			return 	"LLSECOPS"	
		break
		case	mFail_PLAYERS_GOT_MASKS	
			RETURN "LPLYMASK"
		break
		case	mFail_CHARM_PED			
			if bUseCustomEndText 
				return "LCHARMCUST"	
			else 	
				return 	"LCHARM"		
			endif	
		break
		case	mFail_PED_AGRO_T0	
		case	mFail_PED_AGRO_T1	
		case	mFail_PED_AGRO_T2	
		case	mFail_PED_AGRO_T3	
			IF (MC_serverBD.iPartCausingFail[iTeam] = iPartToUse)			
				IF bUseCustomEndText					
					RETURN "LAGYUCUST"//you alerted ~a~	
				ELSE
					RETURN "LAGGROYOU"//You alerted the target.
				ENDIF				
			ELSE //Somebody else alerted the target~a~:				
				IF bUseCustomEndText					
					RETURN "LAGRPRTC" //~a~ alerted ~a~.
				ELSE
					RETURN "LAGGROPART" // ~a~ alerted the target.
				ENDIF				
			ENDIF
		break
		case	mFail_PED_FOUND_BODY	
			IF bUseCustomEndText	
				bCapitalise = TRUE
				RETURN "LBODYCUST"
			ELSE
				RETURN "LBODYAGGR"
			ENDIF	
		break
		case	mFail_PLAYERS_ARRESTED				 	
			return 	"LARRESTED"					
		break
		case	mFail_HEIST_LEADER_LEFT		
			RETURN "LHEISTLL"	
		break
		case	mFail_HEIST_TEAM_GONE			
			IF AM_I_ON_A_HEIST()
			OR ( (MC_serverBD.iPartCausingFail[iTeam] != -1) AND (MC_serverBD.iPartCausingFail[iTeam] != iPartToUse) )
				IF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAPPEND_TEAM_TO_TEAM_FAIL_FOR_T0) and iTeam = 0)
				OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAPPEND_TEAM_TO_TEAM_FAIL_FOR_T1) and iTeam = 1)
				OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAPPEND_TEAM_TO_TEAM_FAIL_FOR_T2) and iTeam = 2)
				OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAPPEND_TEAM_TO_TEAM_FAIL_FOR_T3) and iTeam = 3)
					RETURN "LHEISTTLFT"
				ELSE
					RETURN "LHSTLFT"
				ENDIF
			ELSE
				RETURN "LMYTEAMQ"
			ENDIF
		break
		case	mFail_HEIST_TEAM_GONE_T0	
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAPPEND_TEAM_TO_TEAM_FAIL_FOR_T0)
				RETURN "LHEISTTLFT"
			ELSE
				RETURN "LHSTLFT"
			ENDIF
		break
		case	mFail_HEIST_TEAM_GONE_T1	
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAPPEND_TEAM_TO_TEAM_FAIL_FOR_T1)
				RETURN "LHEISTTLFT"
			ELSE
				RETURN "LHSTLFT"
			ENDIF	
		break
		case	mFail_HEIST_TEAM_GONE_T2	
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAPPEND_TEAM_TO_TEAM_FAIL_FOR_T2)
				RETURN "LHEISTTLFT"
			ELSE
				RETURN "LHSTLFT"
			ENDIF	
		break
		case	mFail_HEIST_TEAM_GONE_T3	
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAPPEND_TEAM_TO_TEAM_FAIL_FOR_T3)
				RETURN "LHEISTTLFT"
			ELSE
				RETURN "LHSTLFT"
			ENDIF	
		break
		case 	mFail_YOU_LEFT
			return "LYOUTEAMLFT"			
		break
		case	mFail_CC_PED_DEAD			
			IF bUseCustomEndText
				bCapitalise = TRUE
				RETURN "LDEADCCPCUST"
			ELSE
				RETURN "LDEADCCPED"
			ENDIF	
		break
		case	mFail_CC_PEDS_DEAD			
			IF bUseCustomEndText
				bCapitalise = TRUE
				RETURN "LDEADCCPSCUST"
			ELSE
				RETURN "LDEADCCPEDS"
			ENDIF	
		break
		case	mFail_CC_PED_HERO_GUN		
			IF bUseCustomEndText
				bCapitalise = TRUE
				RETURN "LGUNCCPCUST"
			ELSE
				RETURN "LGUNCCPED"
			ENDIF	
		break
		case	mFail_CC_PED_HERO_PHONE		
			IF bUseCustomEndText
				bCapitalise = TRUE
				RETURN "LPHNCCPCUST"
			ELSE
				RETURN "LPHNCCPED"
			ENDIF	
		break
		case	mFail_CC_PED_HERO_ALARM
			IF bUseCustomEndText
				bCapitalise = TRUE
				RETURN "LCCALARMCUST"
			ELSE
				RETURN "LCCALARMPED"
			ENDIF	
		break
		case	mFail_LVE_LOC	
			return 	"LLVELOC"	
		break
		case	mFail_OUT_OF_BOUNDS			
			return "LOOB"				
		break
		
		case	mFail_GAINED_WANTED			
			IF MC_serverBD.iPartCausingFail[iTeam] != -1
				IF MC_serverBD.iPartCausingFail[iTeam] = iPartToUse
					RETURN "GNWTDLYOU" // You were spotted by the Cops
				ELSE
					RETURN "GNWTDLPART" // ~a~ was spotted by the Cops
				ENDIF
			ELSE
				
				RETURN "GAINWANTEDL" // The Cops were alerted
				
			ENDIF
		break
		
		case	mFail_NO_AMMO				
			RETURN "FNOAMMO"
		break
		case	mFail_MOD_SHOP_CLOSED				
			RETURN "FMDSHP"
		break
		case	mFail_LOCATE_WRONG_ANGLE	
			RETURN "LWRONGANGLE"
		break
		case	mFail_T0MEMBER_DIED
		case	mFail_T1MEMBER_DIED
		case	mFail_T2MEMBER_DIED
		case	mFail_T3MEMBER_DIED
			
			if bUseCustomEndText
				bCapitalise = TRUE
				IF MC_serverBD.iPartCausingFail[iTeam] = -1
				and not IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAPPEND_TEAM_TO_TEAM_FAIL_FOR_T0 + ( ENUM_TO_INT(MC_serverBD.eCurrentTeamFail[iTeam]) - ENUM_TO_INT(mFail_T0MEMBER_DIED)))
					return "LHMEMDIECUS"
				ELIF MC_serverBD.iPartCausingFail[iTeam] = iLocalPart
					return "LYOUDIED"
				ELSE
					return "LHEISTDIECUS"
				ENDIF
			else
				IF MC_serverBD.iPartCausingFail[iTeam] = -1
					IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
					OR Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
						return "LHEISTDIE"
					ELSE
						return "LTEAMDIE"
					ENDIF
				ELIF MC_serverBD.iPartCausingFail[iTeam] = iLocalPart
					return "LYOUDIED"
				ELSE
					return "LHEISTDIECUS"
				ENDIF
			endif	
		break
		case	mFail_HEIST_TEAM_T0_FAILED	
		case	mFail_HEIST_TEAM_T1_FAILED
		case	mFail_HEIST_TEAM_T2_FAILED
		case	mFail_HEIST_TEAM_T3_FAILED
		
			bool bTeam
			if MC_serverBD.eCurrentTeamFail[iTeam] = mFail_HEIST_TEAM_T0_FAILED
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAPPEND_TEAM_TO_TEAM_FAIL_FOR_T0)
					bTeam = true
				endif
			elif MC_serverBD.eCurrentTeamFail[iTeam] = mFail_HEIST_TEAM_T1_FAILED
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAPPEND_TEAM_TO_TEAM_FAIL_FOR_T1)
					bTeam = true
				endif
			elif MC_serverBD.eCurrentTeamFail[iTeam] = mFail_HEIST_TEAM_T2_FAILED
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAPPEND_TEAM_TO_TEAM_FAIL_FOR_T2)
					bTeam = true
				endif
			elif MC_serverBD.eCurrentTeamFail[iTeam] = mFail_HEIST_TEAM_T3_FAILED
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAPPEND_TEAM_TO_TEAM_FAIL_FOR_T3)
					bTeam = true
				endif
			endif	
			
			if !bTeam
				RETURN "LHEISTTG"
			else
				RETURN "LHEISTTG2"
			endif
						
		break
		
		case	mFail_CEO_GONE
			RETURN "LMYCEOQ"
			
		case	mFail_MC_PRESIDENT_GONE
			RETURN "LMYMCPQ"
			
		case	mFail_SCORE_CANT_BE_MATCHED						
			RETURN "LCMOTS"
	
		case	mFail_ALL_TEAMS_FAIL
			RETURN "ATSAF"
		
		case	mFail_OTHER_TEAM_HAS_ALL_OBJECTS
			RETURN "AOWCC"
	endswitch
	
	RETURN "LMISSOVER"
	
ENDFUNC

FUNC STRING GET_REASON_FOR_OBJECTIVE_END(Bool bpass,INT iTeam,BOOL& bCapitalise)
	if not bpass
		return GET_TEAM_FAIL_REASON(iTeam,bCapitalise)
	endif
	
	BOOL bUseCustomEndText = IS_CUSTOM_END_TEXT_VALID(iTeam)
	
	SWITCH ireasonObjEnd[iTeam]
	
		CASE OBJ_END_REASON_LOC_CAPTURED
			IF bUseCustomEndText
				RETURN "WCAPCUST"
			ELSE
				RETURN "WCAPLOC"
			ENDIF
		BREAK
		
		CASE OBJ_END_REASON_PED_CAPTURED			
			IF bUseCustomEndText
				RETURN "WCAPCUST"
			ELSE
				RETURN "WCAPPED"
			ENDIF			
		BREAK
		
		CASE OBJ_END_REASON_VEH_CAPTURED			
			IF bUseCustomEndText
				RETURN "WCAPCUST"
			ELSE
				RETURN "WCAPVEH"
			ENDIF			
		BREAK
		
		CASE OBJ_END_REASON_OBJ_CAPTURED			
			IF bUseCustomEndText
				RETURN "WCAPCUST"
			ELSE
				RETURN "WCAPOBJ"
			ENDIF			
		BREAK		
		
		CASE OBJ_END_REASON_PED_DELIVERED			
			IF bUseCustomEndText
				RETURN "WDELCUST"
			ELSE
				RETURN "WDELPED"
			ENDIF			
		BREAK
		
		CASE OBJ_END_REASON_VEH_DELIVERED
			IF bUseCustomEndText
				RETURN "WDELCUST"
			ELSE
				RETURN "WDELVEH"
			ENDIF		
		BREAK
		
		CASE OBJ_END_REASON_OBJ_DELIVERED
			IF bUseCustomEndText
				RETURN "WDELCUST"
			ELSE
				RETURN "WDELOBJ"
			ENDIF		
		BREAK
		
		CASE OBJ_END_REASON_PED_DEAD			
			IF bUseCustomEndText
				bCapitalise = TRUE
				RETURN "WDEADPCUST"
			ELSE
				RETURN "WDEADPED"
			ENDIF			
		BREAK	
		
		CASE OBJ_END_REASON_VEH_DEAD			
			IF bUseCustomEndText
				bCapitalise = TRUE
				RETURN "WDESCUST"
			ELSE
				RETURN "WDEADVEH"
			ENDIF			
		BREAK
		
		CASE OBJ_END_REASON_OBJ_DEAD
			IF bUseCustomEndText
				bCapitalise = TRUE
				RETURN "WDESCUST"
			ELSE
				RETURN "WDEADOBJ"
			ENDIF
		BREAK
		
		CASE OBJ_END_REASON_TIME_EXPIRED
			RETURN "WTIMEEXP"			
		BREAK	
		
		CASE OBJ_END_REASON_PLAYERS_KILLED
			RETURN "WPLYKILL"			
		BREAK
		
		CASE OBJ_END_REASON_PLAYERS_GOT_MASKS
			RETURN "WPLYMASK"			
		BREAK
		
		CASE OBJ_END_REASON_PROTECTED_PED
			IF bUseCustomEndText
				RETURN "WPROCUST"
			ELSE
				RETURN "WPROPED"
			ENDIF			
		BREAK
		
		CASE OBJ_END_REASON_PROTECTED_VEH			
			IF bUseCustomEndText
				RETURN "WPROCUST"
			ELSE
				RETURN "WPROVEH"
			ENDIF			
		BREAK
		
		CASE OBJ_END_REASON_PROTECTED_OBJ			
			IF bUseCustomEndText
				RETURN "WPROCUST"
			ELSE
				RETURN "WPROOBJ"
			ENDIF			
		BREAK
		
		CASE OBJ_END_REASON_ARV_LOC			
			IF bUseCustomEndText
				RETURN "WARVCUST"
			ELSE
				RETURN "WARVLOC"
			ENDIF			
		BREAK
		
		CASE OBJ_END_REASON_ARV_PED			
			IF bUseCustomEndText
				RETURN "WARVCUST"
			ELSE
				RETURN "WARVPED"
			ENDIF			
		BREAK
		
		CASE OBJ_END_REASON_ARV_VEH			
			IF bUseCustomEndText
				RETURN "WARVCUST"
			ELSE
				RETURN "WARVVEH"
			ENDIF			
		BREAK	
		
		CASE OBJ_END_REASON_ARV_OBJ			
			IF bUseCustomEndText
				RETURN "WARVCUST"
			ELSE
				RETURN "WARVOBJ"
			ENDIF			
		BREAK
		
		CASE OBJ_END_REASON_OUT_OF_LIVES			
			RETURN "WLIVESOBJ"			
		BREAK
		
		CASE OBJ_END_REASON_TEAM_GONE
			IF NOT DOES_TEAM_LIKE_TEAM(MC_playerBD[iPartToUse].iteam,iTeam)				
				RETURN "WOTHTEAMQ"				
			ELSE				
				RETURN "WMYTEAMQ"				
			ENDIF
		BREAK
		
		CASE OBJ_END_REASON_HEIST_TEAM_GONE
			RETURN "LHEISTTG"
		BREAK
		
		CASE OBJ_END_REASON_HEIST_TEAM_GONE_T0
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAPPEND_TEAM_TO_TEAM_FAIL_FOR_T0)
				RETURN "LHEISTTG2"
			ELSE
				RETURN "LHEISTTG"
			ENDIF
		BREAK
		
		CASE OBJ_END_REASON_HEIST_TEAM_GONE_T1
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAPPEND_TEAM_TO_TEAM_FAIL_FOR_T1)
				RETURN "LHEISTTG2"
			ELSE
				RETURN "LHEISTTG"
			ENDIF
		BREAK
		
		CASE OBJ_END_REASON_HEIST_TEAM_GONE_T2
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAPPEND_TEAM_TO_TEAM_FAIL_FOR_T2)
				RETURN "LHEISTTG2"
			ELSE
				RETURN "LHEISTTG"
			ENDIF
		BREAK
		
		CASE OBJ_END_REASON_HEIST_TEAM_GONE_T3
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAPPEND_TEAM_TO_TEAM_FAIL_FOR_T3)
				RETURN "LHEISTTG2"
			ELSE
				RETURN "LHEISTTG"
			ENDIF
		BREAK
		
		CASE OBJ_END_REASON_HEIST_LEADER_LEFT
			RETURN "LHEISTLL"
		BREAK
		
		CASE OBJ_END_REASON_GAINED_WANTED
			IF MC_serverBD.iPartCausingFail[iTeam] != -1
				IF (MC_serverBD.iPartCausingFail[iTeam] = iPartToUse)
					RETURN "GNWTDLYOU" // You were spotted by the Cops
				ELSE
					RETURN "GNWTDLPART" // ~a~ was spotted by the Cops
				ENDIF
			ELSE
				
				RETURN "GAINWANTEDL"
				
			ENDIF
		BREAK
		
		CASE OBJ_END_REASON_NO_AMMO
			RETURN "FNOAMMO"
		BREAK			

		CASE OBJ_END_REASON_PED_ARRIVED			
			IF bUseCustomEndText
				bCapitalise = TRUE
				RETURN "WPEDARVCUST"
			ELSE
				RETURN "WPEDARV"
			ENDIF			
		BREAK
		
		CASE OBJ_END_REASON_PED_AGRO			
			IF MC_serverBD.iPartCausingFail[iTeam] = -1				
				IF bUseCustomEndText
					bCapitalise = TRUE
					RETURN "WAGGROCUST"
				ELSE
					RETURN "WAGGROPED"
				ENDIF				
			ELIF (MC_serverBD.iPartCausingFail[iTeam] = iPartToUse)				
				IF bUseCustomEndText
					bCapitalise = TRUE
					RETURN "WAGYUCUST"
				ELSE
					RETURN "WAGGROYOU"
				ENDIF				
			ELSE //Somebody else spooked the target:				
				IF bUseCustomEndText
					bCapitalise = TRUE
					RETURN "WAGRPRTC"
				ELSE
					RETURN "WAGGROPART"
				ENDIF				
			ENDIF
		BREAK
		
		CASE OBJ_END_REASON_MOD_SHOP_CLOSED
			RETURN "FMDSHP"
		BREAK
		
		CASE OBJ_END_REASON_PED_FOUND_BODY	
			IF bUseCustomEndText
				bCapitalise = TRUE
				RETURN "WBODYCUST"
			ELSE
				RETURN "WBODYAGGR"
			ENDIF
		BREAK
		
		CASE OBJ_END_REASON_CC_PED_DEAD
			IF bUseCustomEndText
				bCapitalise = TRUE
				RETURN "WDEADCCPCUST"
			ELSE
				RETURN "WDEADCCPED"
			ENDIF
		BREAK
		
		CASE OBJ_END_REASON_CC_PED_ESCAPED			
			IF bUseCustomEndText
				bCapitalise = TRUE
				RETURN "WESCCCPCUST"
			ELSE
				RETURN "WESCCCPED"
			ENDIF			
		BREAK
		
		CASE OBJ_END_REASON_LOCATE_WRONG_ANGLE
			RETURN "LWRONGANGLE"
		BREAK
		
	ENDSWITCH
	
	RETURN "LMISSOVER"
	
ENDFUNC

PROC POPULATE_FAIL_RESULTS()
	
	IF IS_BIT_SET(iLocalBoolCheck15, LBOOL15_CALLED_POPULATE_FAIL_RESULTS)
		
		PRINTLN("[RCC MISSION] POPULATE_FAIL_RESULTS - This has already been called before in this mission, exit!")
		EXIT
		
	ENDIF
	
	sCelebrationStats.strFailReason = ""
	sCelebrationStats.strFailLiteral = ""
	sCelebrationStats.strFailLiteral2 = ""
	
	INT iFailTeam
	
	If IS_PLAYER_SCTV(LocalPlayer) //if spectating use the team that your spectating 
		iFailTeam = MC_playerBD[iPartToUse].iteam			
	ELSE
		iFailTeam = MC_playerBD[iLocalPart].iteam	
	ENDIF
	
	PRINTLN("[RCC MISSION] POPULATE_FAIL_RESULTS - Getting end reasons S0: init fail team is my own, team ",iFailTeam)
	CPRINTLN(DEBUG_MISSION,"[CV_FAIL]before eCurrentTeamFail[",iFailTeam,"] = ",enum_to_int(MC_serverBD.eCurrentTeamFail[iFailTeam]))	
	
	//Check to blame or use same fail for all			
	if MC_serverBD.eCurrentTeamFail[iFailTeam] = mFail_HEIST_TEAM_T0_FAILED
		if (iFailTeam != 1 and  MC_serverBD.eCurrentTeamFail[0] = MC_serverBD.eCurrentTeamFail[1])
		or (iFailTeam != 2 and  MC_serverBD.eCurrentTeamFail[0] = MC_serverBD.eCurrentTeamFail[2])
		or (iFailTeam != 3 and  MC_serverBD.eCurrentTeamFail[0] = MC_serverBD.eCurrentTeamFail[3])
			CPRINTLN(DEBUG_MISSION,"[CV_FAIL]More than one team has failed with ",enum_to_int(MC_serverBD.eCurrentTeamFail[0]) ," change MC_serverBD.eCurrentTeamFail[",iFailTeam,"] to MC_serverBD.eCurrentTeamFail[0]:",enum_to_int(MC_serverBD.eCurrentTeamFail[0]))
			MC_serverBD.eCurrentTeamFail[iFailTeam] = MC_serverBD.eCurrentTeamFail[0]
			//update the fail entity to be the same as the team with the orginal fail.
			IF (MC_serverBD.iEntityCausingFail[0] != -1)
				CPRINTLN(DEBUG_MISSION,"[CV_FAIL]update fail entity for team[",iFailTeam ,"] to match team 0's fail entity")
				MC_serverBD.iEntityCausingFail[iFailTeam] = MC_serverBD.iEntityCausingFail[0]
			endif
		endif
	elif MC_serverBD.eCurrentTeamFail[iFailTeam] = mFail_HEIST_TEAM_T1_FAILED
		if (iFailTeam != 0 and  MC_serverBD.eCurrentTeamFail[1] = MC_serverBD.eCurrentTeamFail[0])
		or (iFailTeam != 2 and  MC_serverBD.eCurrentTeamFail[1] = MC_serverBD.eCurrentTeamFail[2])
		or (iFailTeam != 3 and  MC_serverBD.eCurrentTeamFail[1] = MC_serverBD.eCurrentTeamFail[3])
			CPRINTLN(DEBUG_MISSION,"[CV_FAIL]More than one team has failed with ",enum_to_int(MC_serverBD.eCurrentTeamFail[1]) ," change MC_serverBD.eCurrentTeamFail[",iFailTeam,"] to MC_serverBD.eCurrentTeamFail[1]:",enum_to_int(MC_serverBD.eCurrentTeamFail[1]))
			MC_serverBD.eCurrentTeamFail[iFailTeam] = MC_serverBD.eCurrentTeamFail[1]
			//update the fail entity to be the same as the team with the orginal fail.
			IF (MC_serverBD.iEntityCausingFail[1] != -1)
				CPRINTLN(DEBUG_MISSION,"[CV_FAIL]update fail entity for team[",iFailTeam ,"] to match team 1's fail entity")
				MC_serverBD.iEntityCausingFail[iFailTeam] = MC_serverBD.iEntityCausingFail[1]
			endif
		endif
	elif MC_serverBD.eCurrentTeamFail[iFailTeam] = mFail_HEIST_TEAM_T2_FAILED
		if (iFailTeam != 1 and  MC_serverBD.eCurrentTeamFail[2] = MC_serverBD.eCurrentTeamFail[1])
		or (iFailTeam != 0 and  MC_serverBD.eCurrentTeamFail[2] = MC_serverBD.eCurrentTeamFail[0])
		or (iFailTeam != 3 and  MC_serverBD.eCurrentTeamFail[2] = MC_serverBD.eCurrentTeamFail[3])
			CPRINTLN(DEBUG_MISSION,"[CV_FAIL]More than one team has failed with ",enum_to_int(MC_serverBD.eCurrentTeamFail[2]) ," change MC_serverBD.eCurrentTeamFail[",iFailTeam,"] to MC_serverBD.eCurrentTeamFail[2]:",enum_to_int(MC_serverBD.eCurrentTeamFail[2]))
			MC_serverBD.eCurrentTeamFail[iFailTeam] = MC_serverBD.eCurrentTeamFail[2]
			//update the fail entity to be the same as the team with the orginal fail.
			IF (MC_serverBD.iEntityCausingFail[2] != -1)
				CPRINTLN(DEBUG_MISSION,"[CV_FAIL]update fail entity for team[",iFailTeam ,"] to match team 2's fail entity")
				MC_serverBD.iEntityCausingFail[iFailTeam] = MC_serverBD.iEntityCausingFail[2]
			endif
		endif
	elif MC_serverBD.eCurrentTeamFail[iFailTeam] = mFail_HEIST_TEAM_T3_FAILED
		if (iFailTeam != 1 and  MC_serverBD.eCurrentTeamFail[3] = MC_serverBD.eCurrentTeamFail[1])
		or (iFailTeam != 2 and  MC_serverBD.eCurrentTeamFail[3] = MC_serverBD.eCurrentTeamFail[2])
		or (iFailTeam != 0 and  MC_serverBD.eCurrentTeamFail[3] = MC_serverBD.eCurrentTeamFail[0])
			CPRINTLN(DEBUG_MISSION,"[CV_FAIL]More than one team has failed with ",enum_to_int(MC_serverBD.eCurrentTeamFail[3]) ," change MC_serverBD.eCurrentTeamFail[",iFailTeam,"] to MC_serverBD.eCurrentTeamFail[3]:",enum_to_int(MC_serverBD.eCurrentTeamFail[3]))
			MC_serverBD.eCurrentTeamFail[iFailTeam] = MC_serverBD.eCurrentTeamFail[3]
			//update the fail entity to be the same as the team with the orginal fail.
			IF (MC_serverBD.iEntityCausingFail[3] != -1)
				CPRINTLN(DEBUG_MISSION,"[CV_FAIL]update fail entity for team[",iFailTeam ,"] to match team 3's fail entity")
				MC_serverBD.iEntityCausingFail[iFailTeam] = MC_serverBD.iEntityCausingFail[3]
			endif
		endif
	endif
	
	CPRINTLN(DEBUG_MISSION,"[CV_FAIL]after eCurrentTeamFail[",iFailTeam,"] = ",enum_to_int(MC_serverBD.eCurrentTeamFail[iFailTeam]))	
	
	// get the end objective for custom text
	IF iObjEndPriority[iFailTeam] = -1
	or iObjEndPriority[iFailTeam] > FMMC_MAX_RULES
		iObjEndPriority[iFailTeam] = MC_serverBD_4.iCurrentHighestPriority[iFailTeam]
		CPRINTLN(DEBUG_MISSION,"[CV_FAIL] MC_serverBD_4.iCurrentHighestPriority[",iFailTeam,"] being used for iObjEndPriority[",iFailTeam,"] = ",iObjEndPriority[iFailTeam])
	ENDIF
	CPRINTLN(DEBUG_MISSION,"[CV_FAIL] iObjEndPriority[",iFailTeam,"] = ",iObjEndPriority[iFailTeam])

	bool bCapitalise
	//get the end text template depending on current fail end
	sObjEndText = GET_REASON_FOR_OBJECTIVE_END(false,iFailTeam,bCapitalise)
	CPRINTLN(DEBUG_MISSION,"[CV_FAIL]1 GET_REASON_FOR_OBJECTIVE_END: eCurrentTeamFail[",iFailTeam,"] = ",sObjEndText)
	
	BOOL bCustomFail
	
	IF IS_CUSTOM_END_TEXT_VALID(iFailTeam,false)
	OR IS_CUSTOM_FAIL_REASON_VALID(iFailTeam)
		
		IF iObjEndPriority[iFailTeam] < FMMC_MAX_RULES
			CPRINTLN(DEBUG_MISSION,"[CV_FAIL]2 iObjEndPriority[iFailTeam]= ",iObjEndPriority[iFailTeam]," < FMMC_MAX_RULES")
			//check for custom fail reason 
			// death and quit higher priority here
			IF IS_CUSTOM_FAIL_REASON_VALID(iFailTeam)			
				sCelebrationStats.strFailReason = sObjEndText//"STRING" fail reason here should be custom already FAILCUST: ~a~
				sCelebrationStats.strFailLiteral = g_FMMC_STRUCT.sFMMCEndConditions[iFailTeam].tl63FailReason[iObjEndPriority[iFailTeam]]
				bCustomFail = TRUE
				CPRINTLN(DEBUG_MISSION,"[CV_FAIL]3 USING CUSTOM FAIL: ",sCelebrationStats.strFailLiteral )
			ELSE
				// fill the ~a~ in the fail texts e.g.Failed to hack the ~a~.
				sCelebrationStats.strFailReason = sObjEndText
				
				SWITCH MC_serverBD.eCurrentTeamFail[iFailTeam]
					
					CASE mFail_T0MEMBER_DIED
					CASE mFail_T1MEMBER_DIED
					CASE mFail_T2MEMBER_DIED
					CASE mFail_T3MEMBER_DIED
						// if member dead then only update fail text to use actual name
						IF MC_serverBD.iPartCausingFail[iFailTeam] != -1
						AND MC_serverBD.iPartCausingFail[iFailTeam] != iLocalPart
							sCelebrationStats.strFailLiteral = MC_serverBD_2.tParticipantNames[MC_serverBD.iPartCausingFail[iFailTeam]]
							CPRINTLN(DEBUG_MISSION,"[CV_FAIL]4 adding player who died: ",sCelebrationStats.strFailLiteral )								
						ENDIF
					BREAK
					
					CASE mFail_PED_AGRO_T0
					CASE mFail_PED_AGRO_T1
					CASE mFail_PED_AGRO_T2
					CASE mFail_PED_AGRO_T3
						//Ped Agro end message check
						//first check if the player caused fail
						if (MC_serverBD.iPartCausingFail[iFailTeam] = iPartToUse)  
							// add 1 literal strings e.g. You spooked ~a~ 
							sCelebrationStats.strFailLiteral = GET_OBJECTIVE_END_FAIL_ENTITY_NAME(iFailTeam,bCapitalise)	// the target name	e.g. the biker
							CPRINTLN(DEBUG_MISSION,"[CV_FAIL]5a Add objective entity title, target: ",sCelebrationStats.strFailLiteral )
						ELSE								
							// add 2 literal strings e.g. ~a~ spooked ~a~ 
							IF (MC_serverBD.iPartCausingFail[iFailTeam] != -1)	//check for player name
								sCelebrationStats.strFailLiteral = MC_serverBD_2.tParticipantNames[MC_serverBD.iPartCausingFail[iFailTeam]] //person who spooked target
								CPRINTLN(DEBUG_MISSION,"[CV_FAIL]5b1 caused by player: ",sCelebrationStats.strFailLiteral)
							endif
							
							if (MC_serverBD.iPartCausingFail[iFailTeam] = -1) //use team name instead
							or IS_STRING_NULL_OR_EMPTY(sCelebrationStats.strFailLiteral)
								if MC_serverBD.eCurrentTeamFail[iFailTeam] =  mFail_PED_AGRO_T0 
									sCelebrationStats.strFailLiteral = GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN(0,false,false)
								elif MC_serverBD.eCurrentTeamFail[iFailTeam] =  mFail_PED_AGRO_T1
									sCelebrationStats.strFailLiteral = GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN(1,false,false)
								elif MC_serverBD.eCurrentTeamFail[iFailTeam] =  mFail_PED_AGRO_T2 
									sCelebrationStats.strFailLiteral = GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN(2,false,false)
								elif MC_serverBD.eCurrentTeamFail[iFailTeam] =  mFail_PED_AGRO_T3 
									sCelebrationStats.strFailLiteral = GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN(3,false,false)
								endif		
								CPRINTLN(DEBUG_MISSION,"[CV_FAIL]5b2 caused by player team: ",sCelebrationStats.strFailLiteral)
							ENDIF
							//target 
							sCelebrationStats.strFailLiteral2 = GET_OBJECTIVE_END_FAIL_ENTITY_NAME(iFailTeam,bCapitalise)		
							CPRINTLN(DEBUG_MISSION,"[CV_FAIL]5c caused by / target: ",sCelebrationStats.strFailLiteral," / ",sCelebrationStats.strFailLiteral2 )
						ENDIF
					BREAK
					
					DEFAULT
						sCelebrationStats.strFailLiteral = GET_OBJECTIVE_END_FAIL_ENTITY_NAME(iFailTeam,bCapitalise) // grab only one ~a~
						CPRINTLN(DEBUG_MISSION,"[CV_FAIL]6 Add custom title from objective: ",iObjEndPriority[iFailTeam]," sCelebrationStats.strFailLiteral: ",sCelebrationStats.strFailLiteral )
					BREAK
					
				ENDSWITCH
				
			ENDIF
			
		ELSE
			//no objective for team currently but custom entity is used
			sCelebrationStats.strFailReason = sObjEndText					
			IF IS_CUSTOM_ENTITY_NAME_FAIL_TEXT_VALID(iFailTeam)
				sCelebrationStats.strFailLiteral = GET_OBJECTIVE_END_FAIL_ENTITY_NAME(iFailTeam,bCapitalise,TRUE)
			ENDIF
			CPRINTLN(DEBUG_MISSION,"[CV_FAIL]7 no objective but custom used: ",sCelebrationStats.strFailLiteral )

		ENDIF
	ELSE
		// not using custom fail text 
		sCelebrationStats.strFailReason = sObjEndText
		CPRINTLN(DEBUG_MISSION,"[CV_FAIL]8a not custom sObjEndText: ",sObjEndText )
		
		// but agro fail message can take a string for who spooked the target or died
		SWITCH MC_serverBD.eCurrentTeamFail[iFailTeam]
			CASE mFail_T0MEMBER_DIED
			CASE mFail_T1MEMBER_DIED
			CASE mFail_T2MEMBER_DIED
			CASE mFail_T3MEMBER_DIED
			CASE mFail_PED_AGRO_T0
			CASE mFail_PED_AGRO_T1
			CASE mFail_PED_AGRO_T2
			CASE mFail_PED_AGRO_T3
			CASE mFail_GAINED_WANTED
				IF (MC_serverBD.iPartCausingFail[iFailTeam] != -1) 
				AND (MC_serverBD.iPartCausingFail[iFailTeam] != iPartToUse)
					sCelebrationStats.strFailLiteral = MC_serverBD_2.tParticipantNames[MC_serverBD.iPartCausingFail[iFailTeam]]
					
					CPRINTLN(DEBUG_MISSION,"[CV_FAIL]8b not custom but agro / died member added: ",sCelebrationStats.strFailLiteral )
				ENDIF
			BREAK
		ENDSWITCH
		
	ENDIF
	
	//not using custom fail
	IF NOT bCustomFail
		CPRINTLN(DEBUG_MISSION,"[CV_FAIL]9 not custom entity checks" )
		
		SWITCH MC_serverBD.eCurrentTeamFail[iFailTeam]
			
			CASE mFail_T0MEMBER_DIED
			CASE mFail_T1MEMBER_DIED
			CASE mFail_T2MEMBER_DIED
			CASE mFail_T3MEMBER_DIED
			CASE mFail_PED_AGRO_T0
			CASE mFail_PED_AGRO_T1
			CASE mFail_PED_AGRO_T2
			CASE mFail_PED_AGRO_T3
			CASE mFail_HEIST_TEAM_GONE_T0
			CASE mFail_HEIST_TEAM_GONE_T1
			CASE mFail_HEIST_TEAM_GONE_T2
			CASE mFail_HEIST_TEAM_GONE_T3
				
				IF IS_STRING_NULL_OR_EMPTY(sCelebrationStats.strFailLiteral)
				OR ((MC_serverBD.eCurrentTeamFail[iFailTeam] >= mFail_HEIST_TEAM_GONE_T0)
					AND (MC_serverBD.eCurrentTeamFail[iFailTeam] <= mFail_HEIST_TEAM_GONE_T3)) // For some reason these fail reasons didn't have a check for the string before
					
					IF (MC_serverBD.iPartCausingFail[iFailTeam] != -1)
					AND NOT IS_STRING_NULL_OR_EMPTY(MC_serverBD_2.tParticipantNames[MC_serverBD.iPartCausingFail[iFailTeam]])
						IF (NOT (MC_serverBD.eCurrentTeamFail[iFailTeam] >= mFail_T0MEMBER_DIED AND MC_serverBD.eCurrentTeamFail[iFailTeam] <= mFail_T3MEMBER_DIED))
						OR MC_serverBD.iPartCausingFail[iFailTeam] != iLocalPart // If we're failing because the local player died we don't need a custom string, we'll just be using "You died"
							sCelebrationStats.strFailLiteral = MC_serverBD_2.tParticipantNames[MC_serverBD.iPartCausingFail[iFailTeam]] //person who spooked target
							CPRINTLN(DEBUG_MISSION,"[CV_FAIL]10a adding player name: ",sCelebrationStats.strFailLiteral)
						ENDIF
					ELSE //use team name instead
						
						SWITCH MC_serverBD.eCurrentTeamFail[iFailTeam]
							CASE mFail_T0MEMBER_DIED
							CASE mFail_PED_AGRO_T0
							CASE mFail_HEIST_TEAM_GONE_T0
								sCelebrationStats.strFailLiteral = GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN(0,false,false)
							BREAK
							CASE mFail_T1MEMBER_DIED
							CASE mFail_PED_AGRO_T1
							CASE mFail_HEIST_TEAM_GONE_T1
								sCelebrationStats.strFailLiteral = GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN(1,false,false)
							BREAK
							CASE mFail_T2MEMBER_DIED
							CASE mFail_PED_AGRO_T2
							CASE mFail_HEIST_TEAM_GONE_T2
								sCelebrationStats.strFailLiteral = GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN(2,false,false)
							BREAK
							CASE mFail_T3MEMBER_DIED
							CASE mFail_PED_AGRO_T3
							CASE mFail_HEIST_TEAM_GONE_T3
								sCelebrationStats.strFailLiteral = GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN(3,false,false)
							BREAK
						ENDSWITCH
						
						CPRINTLN(DEBUG_MISSION,"[CV_FAIL]10b set team name: ",sCelebrationStats.strFailLiteral)
					ENDIF
				ENDIF
				
			BREAK
			
			CASE mFail_GAINED_WANTED
				IF IS_STRING_NULL_OR_EMPTY(sCelebrationStats.strFailLiteral)
				AND (MC_serverBD.iPartCausingFail[iFailTeam] != -1)
					
					IF NOT IS_STRING_NULL_OR_EMPTY(MC_serverBD_2.tParticipantNames[MC_serverBD.iPartCausingFail[iFailTeam]])
						sCelebrationStats.strFailLiteral = MC_serverBD_2.tParticipantNames[MC_serverBD.iPartCausingFail[iFailTeam]] //person who spooked target
						CPRINTLN(DEBUG_MISSION,"[CV_FAIL]11a adding player name for wanted gain: ",sCelebrationStats.strFailLiteral)
					ELSE //use team name instead
						
						sCelebrationStats.strFailLiteral = GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN(MC_playerBD[MC_serverBD.iPartCausingFail[iFailTeam]].iteam,false,false)
						
						CPRINTLN(DEBUG_MISSION,"[CV_FAIL]11b set team name for wanted gain: ",sCelebrationStats.strFailLiteral)
					ENDIF
					
				ENDIF
			BREAK
			
			CASE mFail_HEIST_TEAM_GONE //if your team left fill this to current team
				// set name of who left or use team:
				IF ( (MC_serverBD.iPartCausingFail[iFailTeam] != -1)
				AND (MC_serverBD.iPartCausingFail[iFailTeam] != iPartToUse) )
					sCelebrationStats.strFailLiteral = MC_serverBD_2.tParticipantNames[MC_serverBD.iPartCausingFail[iFailTeam]]
					CPRINTLN(DEBUG_MISSION,"[CV_FAIL]12a adding player who left: ",sCelebrationStats.strFailLiteral )
				else
					IF AM_I_ON_A_HEIST()
						sCelebrationStats.strFailLiteral = GET_FILENAME_FOR_AUDIO_CONVERSATION("TEAM_HEISTCREW")
						CPRINTLN(DEBUG_MISSION,"[CV_FAIL]12b adding team name for who left: ", sCelebrationStats.strFailLiteral)
					//ELSE - this won't need a literal string, we just use "Your team quit" instead. We shouldn't even get given this fail reason without being on a heist though anyway.
					ENDIF
				ENDIF
			BREAK
			
			// set team name who failed
			CASE mFail_HEIST_TEAM_T0_FAILED
				sCelebrationStats.strFailLiteral = GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN(0,false,false)
			BREAK
			CASE mFail_HEIST_TEAM_T1_FAILED
				sCelebrationStats.strFailLiteral = GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN(1,false,false)
			BREAK
			CASE mFail_HEIST_TEAM_T2_FAILED
				sCelebrationStats.strFailLiteral = GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN(2,false,false)
			BREAK
			CASE mFail_HEIST_TEAM_T3_FAILED
				sCelebrationStats.strFailLiteral = GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN(3,false,false)
			BREAK
			
		ENDSWITCH
		
		if MC_serverBD.eCurrentTeamFail[iFailTeam] 		= mFail_HEIST_TEAM_T0_FAILED
		or MC_serverBD.eCurrentTeamFail[iFailTeam] 		= mFail_HEIST_TEAM_GONE_T0
		or MC_serverBD.eCurrentTeamFail[iFailTeam] 		= mFail_T0MEMBER_DIED
		or MC_serverBD.eCurrentTeamFail[iFailTeam] 		= mFail_PED_AGRO_T0
			g_sJobHeistInfo.m_failureRole  = 0
		elif  MC_serverBD.eCurrentTeamFail[iFailTeam] 	= mFail_HEIST_TEAM_T1_FAILED
		or MC_serverBD.eCurrentTeamFail[iFailTeam] 		= mFail_HEIST_TEAM_GONE_T1
		or MC_serverBD.eCurrentTeamFail[iFailTeam] 		= mFail_T1MEMBER_DIED
		or MC_serverBD.eCurrentTeamFail[iFailTeam] 		= mFail_PED_AGRO_T1
			g_sJobHeistInfo.m_failureRole  = 1
		elif  MC_serverBD.eCurrentTeamFail[iFailTeam] 	= mFail_HEIST_TEAM_T2_FAILED
		or MC_serverBD.eCurrentTeamFail[iFailTeam] 		= mFail_HEIST_TEAM_GONE_T2
		or MC_serverBD.eCurrentTeamFail[iFailTeam] 		= mFail_T2MEMBER_DIED
		or MC_serverBD.eCurrentTeamFail[iFailTeam] 		= mFail_PED_AGRO_T2
			g_sJobHeistInfo.m_failureRole  = 2
		elif  MC_serverBD.eCurrentTeamFail[iFailTeam] 	= mFail_HEIST_TEAM_T3_FAILED
		or MC_serverBD.eCurrentTeamFail[iFailTeam] 		= mFail_HEIST_TEAM_GONE_T3
		or MC_serverBD.eCurrentTeamFail[iFailTeam] 		= mFail_T3MEMBER_DIED
		or MC_serverBD.eCurrentTeamFail[iFailTeam] 		= mFail_PED_AGRO_T3
			g_sJobHeistInfo.m_failureRole  = 3
		ELSE
			// check if all other teams are blaiming failteam, if so then I am the fail role
			bool bFailknown = false
			if iFailTeam = 0
				if (MC_serverBD.eCurrentTeamFail[1] = mFail_HEIST_TEAM_T0_FAILED and MC_serverBD.eCurrentTeamFail[2] = mFail_HEIST_TEAM_T0_FAILED and MC_serverBD.eCurrentTeamFail[3] = mFail_HEIST_TEAM_T0_FAILED)
				or (MC_serverBD.eCurrentTeamFail[1] = mFail_HEIST_TEAM_GONE_T0 and MC_serverBD.eCurrentTeamFail[2] = mFail_HEIST_TEAM_GONE_T0 and MC_serverBD.eCurrentTeamFail[3] = mFail_HEIST_TEAM_GONE_T0)
				or (MC_serverBD.eCurrentTeamFail[1] = mFail_T0MEMBER_DIED and MC_serverBD.eCurrentTeamFail[2] = mFail_T0MEMBER_DIED and MC_serverBD.eCurrentTeamFail[3] = mFail_T0MEMBER_DIED)
				or (MC_serverBD.eCurrentTeamFail[1] = mFail_PED_AGRO_T0 and MC_serverBD.eCurrentTeamFail[2] = mFail_PED_AGRO_T0 and MC_serverBD.eCurrentTeamFail[3] = mFail_PED_AGRO_T0)
					bFailknown = true	
					g_sJobHeistInfo.m_failureRole  = 0
					CPRINTLN(DEBUG_MISSION,"[CV_FAIL] POPULATE_FAIL_RESULTS - all blaming  fail team: ",g_sJobHeistInfo.m_failureRole)
				endif					
			elif iFailTeam = 1
				if (MC_serverBD.eCurrentTeamFail[0] = mFail_HEIST_TEAM_T1_FAILED and MC_serverBD.eCurrentTeamFail[2] = mFail_HEIST_TEAM_T1_FAILED and MC_serverBD.eCurrentTeamFail[3] = mFail_HEIST_TEAM_T1_FAILED)
				or (MC_serverBD.eCurrentTeamFail[0] = mFail_HEIST_TEAM_GONE_T1 and MC_serverBD.eCurrentTeamFail[2] = mFail_HEIST_TEAM_GONE_T1 and MC_serverBD.eCurrentTeamFail[3] = mFail_HEIST_TEAM_GONE_T1)
				or (MC_serverBD.eCurrentTeamFail[0] = mFail_T1MEMBER_DIED and MC_serverBD.eCurrentTeamFail[2] = mFail_T1MEMBER_DIED and MC_serverBD.eCurrentTeamFail[3] = mFail_T1MEMBER_DIED)
				or (MC_serverBD.eCurrentTeamFail[0] = mFail_PED_AGRO_T1 and MC_serverBD.eCurrentTeamFail[2] = mFail_PED_AGRO_T1 and MC_serverBD.eCurrentTeamFail[3] = mFail_PED_AGRO_T1)
					bFailknown = true
					g_sJobHeistInfo.m_failureRole  = 1
					CPRINTLN(DEBUG_MISSION,"[CV_FAIL] POPULATE_FAIL_RESULTS - all blaming  fail team: ",g_sJobHeistInfo.m_failureRole)
				endif				
			elif iFailTeam = 2
				if (MC_serverBD.eCurrentTeamFail[1] = mFail_HEIST_TEAM_T2_FAILED and MC_serverBD.eCurrentTeamFail[0] = mFail_HEIST_TEAM_T2_FAILED and MC_serverBD.eCurrentTeamFail[3] = mFail_HEIST_TEAM_T2_FAILED)
				or (MC_serverBD.eCurrentTeamFail[1] = mFail_HEIST_TEAM_GONE_T2 and MC_serverBD.eCurrentTeamFail[0] = mFail_HEIST_TEAM_GONE_T2 and MC_serverBD.eCurrentTeamFail[3] = mFail_HEIST_TEAM_GONE_T2)
				or (MC_serverBD.eCurrentTeamFail[1] = mFail_T2MEMBER_DIED and MC_serverBD.eCurrentTeamFail[0] = mFail_T2MEMBER_DIED and MC_serverBD.eCurrentTeamFail[3] = mFail_T2MEMBER_DIED)
				or (MC_serverBD.eCurrentTeamFail[1] = mFail_PED_AGRO_T2 and MC_serverBD.eCurrentTeamFail[0] = mFail_PED_AGRO_T2 and MC_serverBD.eCurrentTeamFail[3] = mFail_PED_AGRO_T2)
					bFailknown = true
					g_sJobHeistInfo.m_failureRole  = 2
					CPRINTLN(DEBUG_MISSION,"[CV_FAIL] POPULATE_FAIL_RESULTS - all blaming  fail team: ",g_sJobHeistInfo.m_failureRole)
				endif				
			elif iFailTeam = 3
				if (MC_serverBD.eCurrentTeamFail[1] = mFail_HEIST_TEAM_T3_FAILED and MC_serverBD.eCurrentTeamFail[2] = mFail_HEIST_TEAM_T3_FAILED and MC_serverBD.eCurrentTeamFail[0] = mFail_HEIST_TEAM_T3_FAILED)
				or (MC_serverBD.eCurrentTeamFail[1] = mFail_HEIST_TEAM_GONE_T3 and MC_serverBD.eCurrentTeamFail[2] = mFail_HEIST_TEAM_GONE_T3 and MC_serverBD.eCurrentTeamFail[0] = mFail_HEIST_TEAM_GONE_T3)
				or (MC_serverBD.eCurrentTeamFail[1] = mFail_T3MEMBER_DIED and MC_serverBD.eCurrentTeamFail[2] = mFail_T3MEMBER_DIED and MC_serverBD.eCurrentTeamFail[0] = mFail_T3MEMBER_DIED)
				or (MC_serverBD.eCurrentTeamFail[1] = mFail_PED_AGRO_T3 and MC_serverBD.eCurrentTeamFail[2] = mFail_PED_AGRO_T3 and MC_serverBD.eCurrentTeamFail[0] = mFail_PED_AGRO_T3)
					bFailknown = true
					g_sJobHeistInfo.m_failureRole  = 3
					CPRINTLN(DEBUG_MISSION,"[CV_FAIL] POPULATE_FAIL_RESULTS - all blaming  fail team: ",g_sJobHeistInfo.m_failureRole)
				endif				
			endif				
			if !bFailKnown
				g_sJobHeistInfo.m_failureRole  = 999 //Using 999 instead of iFailTeam as requested from Chiu and Miguel
			endif	
		endif
		IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
			g_sJobGangopsInfo.m_infos.m_failureRole = g_sJobHeistInfo.m_failureRole
			g_sJobHeistInfo.m_failureRole = 999
			PRINTLN("[JS][GOTEL][TEL] - POPULATE_FAIL_RESULTS - g_sJobGangopsInfo.m_infos.m_failureRole - ", g_sJobGangopsInfo.m_infos.m_failureRole)
		ELSE
			PRINTLN("[RCC MISSION] POPULATE_FAIL_RESULTS - g_sJobHeistInfo.m_failureRole - ",g_sJobHeistInfo.m_failureRole )
		ENDIF
		
	ENDIF
	
	SET_BIT(iLocalBoolCheck15, LBOOL15_CALLED_POPULATE_FAIL_RESULTS)
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		g_sJobGangopsInfo.m_infos.m_failureReason = enum_to_int(MC_serverBD.eCurrentTeamFail[iFailTeam])
		PRINTLN("[JS][GOTEL][TEL] - POPULATE_FAIL_RESULTS - g_sJobGangopsInfo.m_infos.m_failureReason - ", g_sJobGangopsInfo.m_infos.m_failureReason)
	ELIF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE()
		g_sCasino_Heist_Finale_Telemetry_data.endingReason = enum_to_int(MC_serverBD.eCurrentTeamFail[iFailTeam])
		PRINTLN("[JS][CHTEL][TEL] - POPULATE_FAIL_RESULTS - g_sCasino_Heist_Finale_Telemetry_data.endingReason - ", g_sCasino_Heist_Finale_Telemetry_data.endingReason)
	ELIF VCM_FLOW_IS_THIS_MISSION_VCM_FLOW(g_FMMC_STRUCT.iRootContentIDHash)
		g_sCasino_Story_Telemetry_data.m_FailureReason = enum_to_int(MC_serverBD.eCurrentTeamFail[iFailTeam])
		PRINTLN("[ML][TEL] - POPULATE_FAIL_RESULTS - g_sCasino_Story_Telemetry_data.m_FailureReason - ", g_sCasino_Story_Telemetry_data.m_FailureReason)
	ELSE
		g_sJobHeistInfo.m_failureReason  = enum_to_int(MC_serverBD.eCurrentTeamFail[iFailTeam])	
		PRINTLN("[RCC MISSION] POPULATE_FAIL_RESULTS - g_sJobHeistInfo.m_failureReason - ",g_sJobHeistInfo.m_failureReason )
	ENDIF
	
	CPRINTLN(DEBUG_MISSION,"[CV_FAIL] POPULATE_FAIL_RESULTS - Getting end reasons S5: end reason text: ",sObjEndText)
	CPRINTLN(DEBUG_MISSION,"[CV_FAIL] POPULATE_FAIL_RESULTS - Getting end reasons S5: end reason literal text: ",  sCelebrationStats.strFailLiteral )
	CPRINTLN(DEBUG_MISSION,"[CV_FAIL] POPULATE_FAIL_RESULTS - Getting end reasons S5: end reason literal text 2: ",sCelebrationStats.strFailLiteral2 )
	
ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: GENERIC RESULTS FUNCTIONALITY !
//
//************************************************************************************************************************************************************



PROC HANDLE_PLAYLIST_POSITION_DATA_WRITE()

	INT i
	IF IS_PLAYER_ON_A_PLAYLIST(LocalPlayer)
		SET_PLAYLIST_LEADERBOARD_ACTIVE(TRUE)
	ENDIF
	IF g_FMMC_STRUCT.iMissionEndType = ciMISSION_SCORING_TYPE_TIME	
		FOR i = 0 TO (NUM_NETWORK_PLAYERS-1)
			IF g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant > -1
				WRITE_MISSION_LB_DATA_TO_GLOBALS(g_MissionControllerserverBD_LB.sleaderboard[i].playerID,g_MissionControllerserverBD_LB.sleaderboard[i].iTeam,GET_TEAM_FINISH_POSITION(g_MissionControllerserverBD_LB.sleaderboard[i].iTeam),-1,g_MissionControllerserverBD_LB.sleaderboard[i].iMissionTime,g_MissionControllerserverBD_LB.sleaderboard[i].iKills,g_MissionControllerserverBD_LB.sleaderboard[i].iDeaths,g_MissionControllerserverBD_LB.sleaderboard[i].iHeadshots,DUMMY_MODEL_FOR_SCRIPT,MCT_METALLIC,0,MC_playerBD[g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant].iRowanCSuperHighScore)
			ENDIF
		ENDFOR
	ELSE
		FOR i = 0 TO (NUM_NETWORK_PLAYERS-1)
			IF g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant > -1
				WRITE_MISSION_LB_DATA_TO_GLOBALS(g_MissionControllerserverBD_LB.sleaderboard[i].playerID,g_MissionControllerserverBD_LB.sleaderboard[i].iTeam,GET_TEAM_FINISH_POSITION(g_MissionControllerserverBD_LB.sleaderboard[i].iTeam),g_MissionControllerserverBD_LB.sleaderboard[i].iplayerScore,-1,g_MissionControllerserverBD_LB.sleaderboard[i].iKills,g_MissionControllerserverBD_LB.sleaderboard[i].iDeaths,g_MissionControllerserverBD_LB.sleaderboard[i].iHeadshots,DUMMY_MODEL_FOR_SCRIPT,MCT_METALLIC,0,MC_playerBD[g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant].iRowanCSuperHighScore)
			ENDIF
		ENDFOR
	ENDIF
		
ENDPROC

FUNC INT GET_LEADERBOARD_INDEX_OF_TOP_WINNER()
	INT i = 0
	REPEAT COUNT_OF(g_MissionControllerserverBD_LB.sleaderboard) i
		INT iParticipant = g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant
		IF iParticipant > -1
			IF MC_Playerbd[iParticipant].iteam = MC_serverBD.iWinningTeam
			AND HAS_TEAM_PASSED_MISSION(MC_Playerbd[iParticipant].iteam)
				RETURN i
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN 0
ENDFUNC

FUNC INT GET_LEADERBOARD_INDEX_OF_PLAYER(PLAYER_INDEX playerId)
	INT i = 0
	REPEAT COUNT_OF(g_MissionControllerserverBD_LB.sleaderboard) i
		IF g_MissionControllerserverBD_LB.sleaderboard[i].playerID = playerId
			RETURN i
		ENDIF
	ENDREPEAT
	RETURN 0
ENDFUNC

FUNC INT GET_LEADERBOARD_INDEX_OF_TOP_PLAYER_ON_TEAM(INT iteam)
	INT i = 0
	REPEAT COUNT_OF(g_MissionControllerserverBD_LB.sleaderboard) i
		INT iParticipant = g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant
	
		IF iParticipant > -1
			IF MC_Playerbd[iParticipant].iteam = iteam
				RETURN i
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN 0
ENDFUNC

FUNC BOOL SHOULD_I_JOIN_SMALLEST_TEAM(INT iLBPosition, BOOL bAlwaysSwap = FALSE)
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VENETIAN_JOB(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN TRUE
	ENDIF
	
	BOOL bJoin = FALSE
	
	INT iSmallestTeam = FIND_SMALLEST_TEAM_FOR_SWAP_TO_SMALLEST_TEAM_MISSION()
	INT iSmallestTeamPlayers = 0
	
	PRINTLN("[RCC MISSION] SHOULD_I_JOIN_SMALLEST_TEAM - iSmallestTeam = ",iSmallestTeam)
	
	IF iSmallestTeam != -1
		
		iSmallestTeamPlayers = g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iSmallestTeam]
		
		PRINTLN("[RCC MISSION] SHOULD_I_JOIN_SMALLEST_TEAM - iSmallestTeamPlayers = ",iSmallestTeamPlayers)
		
		INT iMyPosition = -1
		INT iLBPos = 0
		
		INT i = 0
		REPEAT COUNT_OF(g_MissionControllerserverBD_LB.sleaderboard) i
			PLAYER_INDEX tempPlayer = g_MissionControllerserverBD_LB.sleaderboard[i].playerID
			
			IF tempPlayer != INVALID_PLAYER_INDEX()
			AND g_MissionControllerserverBD_LB.sleaderboard[i].iTeam != -1
				PRINTLN("[RCC MISSION] SHOULD_I_JOIN_SMALLEST_TEAM - Leaderboard pos ",i," contains participant ",g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant," from team ",g_MissionControllerserverBD_LB.sleaderboard[i].iTeam)
				IF g_MissionControllerserverBD_LB.sleaderboard[i].iTeam = MC_serverBD.iWinningTeam
				AND HAS_TEAM_PASSED_MISSION(g_MissionControllerserverBD_LB.sleaderboard[i].iTeam)
					PRINTLN("[RCC MISSION] SHOULD_I_JOIN_SMALLEST_TEAM - w winning team ",MC_serverBD.iWinningTeam)
					IF tempPlayer = LocalPlayer
						iMyPosition = iLBPos
						PRINTLN("[RCC MISSION] SHOULD_I_JOIN_SMALLEST_TEAM - it's me, position in winning team is ",iMyPosition)
						i = COUNT_OF(g_MissionControllerserverBD_LB.sleaderboard) //Break out!
					ELSE
						PRINTLN("[RCC MISSION] SHOULD_I_JOIN_SMALLEST_TEAM - it's not me, position in winning team is ",iLBPos)
						iLBPos++
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF iMyPosition != -1
			IF iMyPosition < iSmallestTeamPlayers
				PRINTLN("[RCC MISSION] SHOULD_I_JOIN_SMALLEST_TEAM - TRUE - My position on the winning team is high enough to join! iMyPosition (",iMyPosition,") < iSmallestTeamPlayers (",iSmallestTeamPlayers,")")
				bJoin = TRUE
			ELSE
				PRINTLN("[RCC MISSION] SHOULD_I_JOIN_SMALLEST_TEAM - My position on the winning team is too low: iMyPosition (",iMyPosition,") >= iSmallestTeamPlayers (",iSmallestTeamPlayers,")")
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION] SHOULD_I_JOIN_SMALLEST_TEAM - I'm not on the winning team")
			IF iLBPos = 0 // No winning team, pick from who came top on the leaderboard
				PRINTLN("[RCC MISSION] SHOULD_I_JOIN_SMALLEST_TEAM - Nobody was on the winning team! (MC_serverBD.iWinningTeam = ",MC_serverBD.iWinningTeam,")")
				IF iLBPosition < iSmallestTeamPlayers
					PRINTLN("[RCC MISSION] SHOULD_I_JOIN_SMALLEST_TEAM - TRUE - but my position on the leaderboard is high enough anyway! iLBPosition (",iLBPosition,") < iSmallestTeamPlayers (",iSmallestTeamPlayers,")")
					bJoin = TRUE
				ELSE
					PRINTLN("[RCC MISSION] SHOULD_I_JOIN_SMALLEST_TEAM - My position on the winning team is too low: iLBPosition (",iLBPosition,") >= iSmallestTeamPlayers (",iSmallestTeamPlayers,")")
				ENDIF
			ELIF bAlwaysSwap AND (iSmallestTeam != GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen)
				IF iLBPosition < iSmallestTeamPlayers
					PRINTLN("[RCC MISSION] SHOULD_I_JOIN_SMALLEST_TEAM - TRUE - bAlwaysSwap - but my position on the leaderboard is high enough anyway! iLBPosition (",iLBPosition,") < iSmallestTeamPlayers (",iSmallestTeamPlayers,")")
					bJoin = TRUE
				ELSE
					PRINTLN("[RCC MISSION] SHOULD_I_JOIN_SMALLEST_TEAM - bAlwaysSwap - My position on the winning team is too low: iLBPosition (",iLBPosition,") >= iSmallestTeamPlayers (",iSmallestTeamPlayers,")")
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	IF bAlwaysSwap AND (iSmallestTeam = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen)
		bJoin = FALSE
	ENDIF
	
	
	PRINTLN("[RCC MISSION] SHOULD_I_JOIN_SMALLEST_TEAM - ***** Returning ",bJoin," *****")
	
	RETURN bJoin
	
ENDFUNC

CONST_INT ciMIN_NUM_ON_TEAM_FOR_TEAM_KILL_AWARD 2

PROC HANDLE_END_AWARDS(INT iFinishPos, INT iTeam)
	IF IS_A_STRAND_MISSION_BEING_INITIALISED()
		EXIT
	ENDIF
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		EXIT
	ENDIF
	
	IF iFinishPos = 1
		IF Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)	
			INCREMENT_MP_INT_CHARACTER_AWARD(MP_AWARD_WIN_LAST_TEAM_STANDINGS)
			PRINTLN("[RCC MISSION] AWARD, HANDLE_END_AWARDS, MP_AWARD_WIN_LAST_TEAM_STANDINGS ")
			IF iTeam <> -1

				// Give award when you kill an entire team
				INT iTeamsLoop

				FOR iTeamsLoop = 0 TO (FMMC_MAX_TEAMS -1)
					IF (iKilledThisManyFrmTeam[iTeamsLoop] > 0)
					
						PRINTLN("[RCC MISSION] AWARD, HANDLE_END_AWARDS,  iKilledThisManyFrmTeam = ", iKilledThisManyFrmTeam[iTeamsLoop], " iTeamsLoop = ", iTeamsLoop)
					ENDIF
					IF (MC_serverBD.iNumberOfLBPlayers[iTeamsLoop] >= ciMIN_NUM_ON_TEAM_FOR_TEAM_KILL_AWARD)
						IF NOT IS_BIT_SET(iLocalBoolCheck4, LBOOL4_AWARD_LAST_ALIVE)
							PRINTLN("[RCC MISSION] AWARD, HANDLE_END_AWARDS, SET_BIT, LBOOL4_AWARD_LAST_ALIVE ")
							SET_BIT(iLocalBoolCheck4, LBOOL4_AWARD_LAST_ALIVE)
						ENDIF
						IF (iKilledThisManyFrmTeam[iTeamsLoop] >= MC_serverBD.iNumberOfLBPlayers[iTeamsLoop])
							INCREMENT_MP_INT_CHARACTER_AWARD(MP_AWARD_KILL_TEAM_YOURSELF_LTS)
							PRINTLN("[RCC MISSION] AWARD, HANDLE_END_AWARDS, MP_AWARD_KILL_TEAM_YOURSELF_LTS, iCounter = ", iKilledThisManyFrmTeam[iTeamsLoop], " iTeamsLoop = ", iTeamsLoop)
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
			IF IS_BIT_SET(iLocalBoolCheck4, LBOOL4_AWARD_LAST_ALIVE)
				// Least person left alive award
				IF (MC_serverBD.iTotalPlayingCoopPlayers[iTeam] = 1)
					IF bLocalPlayerOK
						INCREMENT_MP_INT_CHARACTER_AWARD(MP_AWARD_ONLY_PLAYER_ALIVE_LTS)
						PRINTLN("[RCC MISSION] AWARD, HANDLE_END_AWARDS, MP_AWARD_ONLY_PLAYER_ALIVE_LTS ")
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION] AWARD, HANDLE_END_AWARDS, MP_AWARD_ONLY_PLAYER_ALIVE_LTS, iTotalPlayingCoopPlayers = ", MC_serverBD.iTotalPlayingCoopPlayers[iTeam])
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION] AWARD, HANDLE_END_AWARDS, MP_AWARD_ONLY_PLAYER_ALIVE_LTS, NOT_SET LBOOL4_AWARD_LAST_ALIVE ")
			ENDIF
		ELIF Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer)
			// Win CTF award
			INCREMENT_MP_INT_CHARACTER_AWARD(MP_AWARD_WIN_CAPTURES)
			PRINTLN("[RCC MISSION] AWARD, HANDLE_END_AWARDS, MP_AWARD_WIN_CAPTURES ")
			// Win CTF with 0 deaths award
			IF MC_PlayerBD[iLocalPart].iNumPlayerDeaths = 0
				INCREMENT_MP_INT_CHARACTER_AWARD(MP_AWARD_WIN_CAPTURE_DONT_DYING)
				PRINTLN("[RCC MISSION] AWARD, HANDLE_END_AWARDS, MP_AWARD_WIN_CAPTURE_DONT_DYING ")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MANAGE_DAILY_OBJECTIVES(BOOL bPass)
	
	PRINTLN("[MJL][MANAGE_DAILY_OBJECTIVES] Entered")
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		EXIT
	ENDIF

	// SERIES
	IF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_FEATURED_ADVERSARY_MODE_JOB(g_FMMC_STRUCT.iRootContentIDHash)
		SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_PARTICIPATE_IN_FEATURED_SERIES)
		PRINTLN("[MJL][DAILY OBJECTIVE] This was in a Featured series", g_FMMC_STRUCT.iRootContentIDHash)
	ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_ADVERSARY_SERIES_JOB(g_FMMC_STRUCT.iRootContentIDHash)
		SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_PARTICIPATE_IN_ADVERSARY_SERIES)
		PRINTLN("[MJL][DAILY OBJECTIVE] This was in a Adversary series", g_FMMC_STRUCT.iRootContentIDHash)
	ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_ARENA_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
		SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_PARTICIPATE_IN_ARENA_WAR_SERIES)
		PRINTLN("[MJL][DAILY OBJECTIVE] This was in a Arena series", g_FMMC_STRUCT.iRootContentIDHash)
	ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_BUNKER_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
		SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_PARTICIPATE_IN_BUNKER_SERIES)
		PRINTLN("[MJL][DAILY OBJECTIVE] This was in a Bunker series", g_FMMC_STRUCT.iRootContentIDHash)
	ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_ARENA_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
		SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_PARTICIPATE_IN_ARENA_WAR_SERIES)
		PRINTLN("[MJL][DAILY OBJECTIVE] This was in a Arena series", g_FMMC_STRUCT.iRootContentIDHash)
	ENDIF
				
	// INDIVIDUAL
	
	SWITCH g_FMMC_STRUCT.iAdversaryModeType
		CASE 0
			PRINTLN("[ML][DAILY OBJECTIVE] Case 0 as g_FMMC_STRUCT.iAdversaryModeType = 0")
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_ComeOuttoPlay(g_FMMC_STRUCT.iRootContentIDHash)
				SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_COME_OUT_TO_PLAY)
				PRINTLN("[ML][DAILY OBJECTIVE] This was Come Out To Play")
			ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_SiegeMentality(g_FMMC_STRUCT.iRootContentIDHash)
				SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_SIEGE_MENTALITY)
				PRINTLN("[ML][DAILY OBJECTIVE] This was Siege Mentality")
			ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_HastaLaVista(g_FMMC_STRUCT.iRootContentIDHash)
				SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_HASTA_LA_VISTA)
				PRINTLN("[ML][DAILY OBJECTIVE] This was Hasta La Vista")
			ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_CrossTheLine(g_FMMC_STRUCT.iRootContentIDHash)
				SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_CROSS_THE_LINE)
				PRINTLN("[ML][DAILY OBJECTIVE] This was Cross the Line")
			ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_HuntingPack(g_FMMC_STRUCT.iRootContentIDHash, FALSE)
				SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_HUNTING_PACK)	
				PRINTLN("[ML][DAILY OBJECTIVE] This was Hunting Pack")
			ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_Relay(g_FMMC_STRUCT.iRootContentIDHash)
				SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_RELAY)
				PRINTLN("[ML][DAILY OBJECTIVE] This was Relay")
			ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_EXTRACTION(g_FMMC_STRUCT.iRootContentIDHash)
				SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_EXTRACTION)
				PRINTLN("[ML][DAILY OBJECTIVE] This was Extraction")
			ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_BVS1(g_FMMC_STRUCT.iRootContentIDHash)
				SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_BEAST_VS_SLASHER)
				PRINTLN("[ML][DAILY OBJECTIVE] This was Beast Vs Slashers")
			ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_ZONE(g_FMMC_STRUCT.iRootContentIDHash)
				SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_DROP_ZONE)
				PRINTLN("[ML][DAILY OBJECTIVE] This was Drop Zone")
			ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_BE_MY_VALENTINE(g_FMMC_STRUCT.iRootContentIDHash)
				SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_TILL_DEATH_DO_US_PART)
				PRINTLN("[ML][DAILY OBJECTIVE] This was Til Death Do Us Part")
			ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_SUMO(g_FMMC_STRUCT.iRootContentIDHash)
				SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_SUMO)
				PRINTLN("[ML][DAILY OBJECTIVE] This was Sumo")
			ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUGBY(g_FMMC_STRUCT.iRootContentIDHash)
				SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_INCH_BY_INCH)
				PRINTLN("[ML][DAILY OBJECTIVE] This was Inch By Inch")	
			ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_RunningBack(g_FMMC_STRUCT.iRootContentIDHash)
				SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_RUNNING_BACK)
				PRINTLN("[ML][DAILY OBJECTIVE] This was Running Back")
			ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_Ebc(g_FMMC_STRUCT.iRootContentIDHash)
				SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_EVERY_BULLET_COUNTS)
				PRINTLN("[ML][DAILY OBJECTIVE] This was Every Bullet Counts")
			ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAVID_AND_GOLIATH(g_FMMC_STRUCT.iRootContentIDHash)
				SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_RHINO_HUNT)
				PRINTLN("[ML][DAILY OBJECTIVE] This was Rhino Hunt")
			ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_IN_AND_OUT(g_FMMC_STRUCT.iRootContentIDHash)
				SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_IN_AND_OUT)
				PRINTLN("[ML][DAILY OBJECTIVE] This was In and Out")
			ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_Shepherd(g_FMMC_STRUCT.iRootContentIDHash)
				SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_OFFENSE_DEFENSE)
				PRINTLN("[ML][DAILY OBJECTIVE] This was Shepherd / Offence Defence")
			ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_SpeedRace(g_FMMC_STRUCT.iRootContentIDHash)
				SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_KEEP_THE_PACE)
				PRINTLN("[ML][DAILY OBJECTIVE] This was SpeedRace / Keep the Pace")
			ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_HuntDark(g_FMMC_STRUCT.iRootContentIDHash)
				SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_SLASHER)
				PRINTLN("[ML][DAILY OBJECTIVE] This was Slasher / HuntDark")			
			ELSE	
				PRINTLN("[ML][DAILY OBJECTIVE] Mission was none of these. Thats not great")
				PRINTLN("[ML][DAILY OBJECTIVE] g_FMMC_STRUCT.iAdversaryModeType = ", g_FMMC_STRUCT.iAdversaryModeType)
				PRINTLN("[ML][DAILY OBJECTIVE] g_FMMC_STRUCT.iRootContentIDHash = ", g_FMMC_STRUCT.iRootContentIDHash)
			ENDIF
				
		BREAK
		
		CASE ciNEWVS_TOUR_DE_FORCE //IS_THIS_ROCKSTAR_MISSION_NEW_VS_TOUR_DE_FORCE(g_FMMC_STRUCT.iAdversaryModeType)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_SLIPSTREAM)
			PRINTLN("[ML][DAILY OBJECTIVE] This was Slipstream")
		BREAK
		
		CASE ciNEWVS_KEEP_THE_PACE //IS_THIS_ROCKSTAR_MISSION_NEW_VS_KEEP_THE_PACE(g_FMMC_STRUCT.iAdversaryModeType)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_KEEP_THE_PACE)
			PRINTLN("[ML][DAILY OBJECTIVE] This was Keep the Pace")
		BREAK
		
		CASE ciNEWVS_STUNT_OFFENSE_DEFENSE //IS_THIS_ROCKSTAR_MISSION_NEW_VS_STUNT_OFFENSE_DEFENSE(g_FMMC_STRUCT.iAdversaryModeType)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_OFFENSE_DEFENSE)
			PRINTLN("[ML][DAILY OBJECTIVE] This was Offense Defence")
		BREAK
		
		CASE ciNEWVS_TRADING_PLACES //IS_THIS_ROCKSTAR_MISSION_NEW_VS_GRASS_GREENER(g_FMMC_STRUCT.iAdversaryModeType)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_TRADING_PLACES)
			PRINTLN("[ML][DAILY OBJECTIVE] This was Trading Places")
		BREAK
		
		CASE ciNEWVS_POWER_PLAY //IS_THIS_ROCKSTAR_MISSION_NEW_VS_POWER_PLAY(g_FMMC_STRUCT.iAdversaryModeType)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_POWER_PLAY)
			PRINTLN("[ML][DAILY OBJECTIVE] This was Power Play")
		BREAK
		
		CASE ciNEWVS_ENTOURAGE //IS_THIS_ROCKSTAR_MISSION_NEW_VS_ENTOURAGE(g_FMMC_STRUCT.iAdversaryModeType)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_ENTOURAGE)
			PRINTLN("[ML][DAILY OBJECTIVE] This was Entourage")
		BREAK
		
		CASE ciNEWVS_LOST_DAMNED //IS_THIS_ROCKSTAR_MISSION_NEW_VS_LOST_DAMNED(g_FMMC_STRUCT.iAdversaryModeType)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_LOST_VS_DAMNED)
			PRINTLN("[ML][DAILY OBJECTIVE] This was Lost Vs Damned")
		BREAK
		
		CASE ciNEWVS_DONT_CROSS_THE_LINE //IS_THIS_ROCKSTAR_MISSION_NEW_VS_DCTL(g_FMMC_STRUCT.iAdversaryModeType)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_DEADLINE)
			PRINTLN("[ML][DAILY OBJECTIVE] This was Deadline")	
		BREAK
	
		CASE ciNEWVS_GUNSMITH //IS_THIS_ROCKSTAR_MISSION_NEW_VS_GUNSMITH(g_FMMC_STRUCT.iAdversaryModeType)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_KILL_QUOTA)
			PRINTLN("[ML][DAILY OBJECTIVE] This was Kill Quota")
		BREAK
		
		CASE ciNEWVS_TURF_WARS //IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARS(g_FMMC_STRUCT.iAdversaryModeType)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_TURF_WARS)
			PRINTLN("[ML][DAILY OBJECTIVE] This was Turf Wars")
		BREAK
		
		CASE ciNEWVS_VEHICULAR_VENDETTA //IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA(g_FMMC_STRUCT.iAdversaryModeType)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_VEHICLE_VENDETTA)
			PRINTLN("[ML][DAILY OBJECTIVE] This was Vehicle Vendetta")
		BREAK
		
		CASE ciNEWVS_POINTLESS //IS_THIS_ROCKSTAR_MISSION_NEW_VS_POINTLESS(g_FMMC_STRUCT.iAdversaryModeType)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_COLLECTION_TIME)
			PRINTLN("[ML][DAILY OBJECTIVE] This was Collection time")
		BREAK
	
		CASE ciNEWVS_JUGGERNAUT //IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_JUGGERNAUT)
			PRINTLN("[ML][DAILY OBJECTIVE] This was Juggernaut")
		BREAK
		
		CASE ciNEWVS_SLASHER //IS_THIS_ROCKSTAR_MISSION_NEW_VS_HuntDark(g_FMMC_STRUCT.iAdversaryModeType)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_SLASHER)
			PRINTLN("[ML][DAILY OBJECTIVE] This was Slasher")
		BREAK
		
		CASE ciNEWVS_VALENTINES //IS_THIS_ROCKSTAR_MISSION_NEW_VS_BE_MY_VALENTINE(g_FMMC_STRUCT.iAdversaryModeType)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_TILL_DEATH_DO_US_PART)
			PRINTLN("[ML][DAILY OBJECTIVE] This was Til Death do Us Part")
		BREAK
		
		CASE ciNEWVS_EVERY_BULLET_COUNTS //IS_THIS_ROCKSTAR_MISSION_NEW_VS_Ebc(g_FMMC_STRUCT.iAdversaryModeType)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_EVERY_BULLET_COUNTS)
			PRINTLN("[ML][DAILY OBJECTIVE] This was Every Bullet counts")
		BREAK
		
		CASE ciNEWVS_RESURRECTION //IS_THIS_ROCKSTAR_MISSION_NEW_VS_RESURRECTION(g_FMMC_STRUCT.iAdversaryModeType)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_RESURRECTION)
			PRINTLN("[ML][DAILY OBJECTIVE] This was Resurrection")
		BREAK
		
		CASE ciNEWVS_TINYRACERS //IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_TINY_RACERS)
			PRINTLN("[ML][DAILY OBJECTIVE] This was Tiny Racers")
		BREAK
		
		CASE ciNEWVS_DAWN_RAID //IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(g_FMMC_STRUCT.iAdversaryModeType)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_DAWN_RAID)
			PRINTLN("[ML][DAILY OBJECTIVE] This was Dawn Raid")
		BREAK

		CASE ciNEWVS_OVERTIME_RUMBLE //IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_RUMBLE(g_FMMC_STRUCT.iAdversaryModeType)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_OVERTIME_RUMBLE)
			PRINTLN("[ML][DAILY OBJECTIVE] This was Overtime rumble")
		BREAK
		
		CASE ciNEWVS_POWER_MAD //IS_THIS_ROCKSTAR_MISSION_NEW_VS_POWER_MAD(g_FMMC_STRUCT.iAdversaryModeType)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_POWER_MAD)
			PRINTLN("[ML][DAILY OBJECTIVE] This was Power Mad")
		BREAK
		
		CASE ciNEWVS_VEHICULAR_WARFARE //IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_WARFARE(g_FMMC_STRUCT.iAdversaryModeType)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_MOTOR_WARS)
			PRINTLN("[ML][DAILY OBJECTIVE] This was Motor Wars")
		BREAK
		
		CASE ciNEWVS_BOMBUSHKA //IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMBUSHKA(g_FMMC_STRUCT.iAdversaryModeType)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_BOMBUSHKA_RUN)
			PRINTLN("[ML][DAILY OBJECTIVE] This was Bombushka Run")
		BREAK
		
		CASE ciNEWVS_STOCKPILE //IS_THIS_ROCKSTAR_MISSION_NEW_VS_STOCKPILE(g_FMMC_STRUCT.iAdversaryModeType)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_STOCKPILE)
			PRINTLN("[ML][DAILY OBJECTIVE] This was Stockpile")
		BREAK
		
		CASE ciNEWVS_VENETIAN_JOB //IS_THIS_ROCKSTAR_MISSION_NEW_VS_VENETIAN_JOB(g_FMMC_STRUCT.iAdversaryModeType)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_VESPUCCI_JOB)
			PRINTLN("[ML][DAILY OBJECTIVE] This was Vespucci Job")	
		BREAK
		
		CASE ciNEWVS_STUNTING_PACK //IS_THIS_ROCKSTAR_MISSION_NEW_VS_STUNTING_PACK(g_FMMC_STRUCT.iAdversaryModeType)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_HUNTING_PACK_RE)
			PRINTLN("[ML][DAILY OBJECTIVE] This was Hunting Pack Remix")
		BREAK
		
		CASE ciNEWVS_TRADING_PLACES_REMIX //IS_THIS_ROCKSTAR_MISSION_NEW_VS_TRADING_PLACES_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_TRADING_PLACES_RE)
			PRINTLN("[ML][DAILY OBJECTIVE] This was Trading Places Remix")
		BREAK
		
		CASE ciNEWVS_RUNNING_BACK_REMIX //IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUNNING_BACK_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_RUNNING_BACK_RE)
			PRINTLN("[ML][DAILY OBJECTIVE] This was Running Back Remix")
		BREAK
		
		CASE ciNEWVS_SUMO_RUN //IS_THIS_ROCKSTAR_MISSION_NEW_VS_SUMO_RUN(g_FMMC_STRUCT.iAdversaryModeType)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_SUMO_RE)
			PRINTLN("[ML][DAILY OBJECTIVE] This was Sumo Remix")
		BREAK
		
		CASE ciNEWVS_BOMB_FOOTBALL //IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMB_FOOTBALL(g_FMMC_STRUCT.iAdversaryModeType)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_BOMB_BALL)
			PRINTLN("[ML][DAILY OBJECTIVE] This was Arena Bomb Football")
		BREAK
		
		CASE ciNEWVS_TAG_TEAM //IS_THIS_ROCKSTAR_MISSION_NEW_VS_TAG_TEAM(g_FMMC_STRUCT.iAdversaryModeType)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_TAG_TEAM)
			PRINTLN("[ML][DAILY OBJECTIVE] This was Arena Tag Team")
		BREAK
		
		CASE ciNEWVS_GAMES_MASTERS //IS_THIS_ROCKSTAR_MISSION_NEW_VS_GAMES_MASTERS(g_FMMC_STRUCT.iAdversaryModeType)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_GAMES_MASTERS)
			PRINTLN("[ML][DAILY OBJECTIVE] This was Arena Games Masters")
		BREAK
		
		CASE ciNEWVS_MONSTER_JAM //IS_THIS_ROCKSTAR_MISSION_NEW_VS_MONSTER_JAM(g_FMMC_STRUCT.iAdversaryModeType)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_HERE_COME_THE_MONSTERS)
			PRINTLN("[ML][DAILY OBJECTIVE] This was Arena Monster Jam")
		BREAK
		
		CASE ciNEWVS_ARENA_CTF //IS_THIS_ROCKSTAR_MISSION_NEW_VS_ARENA_CTF(g_FMMC_STRUCT.iAdversaryModeType)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_FLAG_WAR)
			PRINTLN("[ML][DAILY OBJECTIVE] This was Arena Flag War")
		BREAK
		
		DEFAULT
			PRINTLN("[ML][DAILY OBJECTIVE] g_FMMC_STRUCT.iAdversaryModeType = ", g_FMMC_STRUCT.iAdversaryModeType)
			PRINTLN("[ML][DAILY OBJECTIVE] g_FMMC_STRUCT.iRootContentIDHash = ", g_FMMC_STRUCT.iRootContentIDHash)
		BREAK
	ENDSWITCH
	
	IF IS_THIS_CURRENTLY_OVERTIME_TURNS()
		SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_OVERTIME_SHOOTOUT)
		PRINTLN("[ML][DAILY OBJECTIVE] This was Overtime Shootout")	
	ENDIF

	IF Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)
		SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_PARTICIPATE_IN_LTS)
		PRINTLN("[RCC MISSION] MP_DAILY_JOB_PARTICIPATE_IN_LTS COMPLETE")
	ENDIF
	IF Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer)
	
		IF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_MISSION_IS_CTF_CONTEND)	
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_PARTICIPATE_IN_CAPTURE_CONTEND) 
			PRINTLN("[RCC MISSION] MP_DAILY_JOB_PARTICIPATE_IN_CAPTURE_CONTEND COMPLETE")
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_MISSION_IS_CTF_GTA)	
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_PARTICIPATE_IN_CAPTURE_GTA) 
			PRINTLN("[RCC MISSION] MP_DAILY_JOB_PARTICIPATE_IN_CAPTURE_GTA COMPLETE")
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_MISSION_IS_CTF_HOLD)	
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_PARTICIPATE_IN_CAPTURE_HOLD) 
			PRINTLN("[RCC MISSION] MP_DAILY_JOB_PARTICIPATE_IN_CAPTURE_HOLD COMPLETE")
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_MISSION_IS_CTF_RAID)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_PARTICIPATE_IN_CAPTURE_RAID) 
			PRINTLN("[RCC MISSION] MP_DAILY_JOB_PARTICIPATE_IN_CAPTURE_RAID COMPLETE")
		ENDIF
		
		SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_PARTICIPATE_IN_CAPTURE) 
		PRINTLN("[RCC MISSION] MP_DAILY_JOB_PARTICIPATE_IN_CAPTURE COMPLETE")
		
	ENDIF
	
	IF Is_Player_Currently_On_MP_Versus_Mission(LocalPlayer)
	AND NOT IS_THIS_AN_ADVERSARY_MODE_MISSION(g_FMMC_STRUCT.iRootContentIDHash, g_FMMC_STRUCT.iAdversaryModeType) 

		SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_PARTICIPATE_IN_VERSUS_MISSION)
		PRINTLN("[RCC MISSION] MP_DAILY_JOB_PARTICIPATE_IN_VERSUS_MISSION COMPLETE")
	ENDIF
	
	IF bPass
		IF Is_Player_Currently_On_MP_Contact_Mission(LocalPlayer)
		OR Is_Player_Currently_On_MP_Random_Event(LocalPlayer)
		OR Is_Player_Currently_On_MP_Coop_Mission(LocalPlayer)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_PARTICIPATE_IN_COOP_MISSION)
			PRINTLN("[RCC MISSION] MP_DAILY_JOB_PARTICIPATE_IN_COOP_MISSION COMPLETE")
		ENDIF
		
		IF IS_LOADED_MISSION_LOW_RIDER_FLOW_MISSION()
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_COMPLETE_LOWRIDER_MISSION)
			PRINTLN("[MJL][DAILY OBJECTIVE] This was a Lowrider", g_FMMC_STRUCT.iRootContentIDHash)
		ELIF IS_THIS_ROOT_CONTENT_ID_AN_ASSASSINATION_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_COMPLETE_DISPATCH_MISSION)
			PRINTLN("[MJL][DAILY OBJECTIVE] This was a Dispatch", g_FMMC_STRUCT.iRootContentIDHash)
		ENDIF
	
		IF IS_THIS_ROOT_CONTENT_ID_A_REPO_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
			PRINTLN("[MJL][DAILY OBJECTIVE] This was a Simeon Repo Mission", g_FMMC_STRUCT.iRootContentIDHash)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_COMPLETE_SIMEON_REPO_MISSION)
		ENDIF

		IF GANGOPS_FLOW_IS_A_GANGOPS_FINALE_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_COMPLETE_DOOMSDAY_FINALE)
			PRINTLN("[MJL][DAILY OBJECTIVE] Completed a Doomsday Heist Finale", g_FMMC_STRUCT.iRootContentIDHash)
		ELIF GANGOPS_FLOW_IS_A_GANGOPS_MISSION_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash) 
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_COMPLETE_DOOMSDAY_SETUP)
			PRINTLN("[MJL][DAILY OBJECTIVE] Completed a Doomsday Heist Setup", g_FMMC_STRUCT.iRootContentIDHash)
		ENDIF
		
		IF IS_THIS_CASINO_HEIST_MISSION_THE_FINAL_STAGE(GET_CASINO_HEIST_MISSION_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash))
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_COMPLETE_CASINO_HEIST_FINALE)
			PRINTLN("[MJL][DAILY OBJECTIVE] Completed a Casino Heist Finale", g_FMMC_STRUCT.iRootContentIDHash)
		ENDIF
	ENDIF

ENDPROC

PROC CLEANUP_ALL_BLIPS()
	CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION] Cleanup all blips called!")

	INT i
	
	CLEANUP_ALL_AI_PED_BLIPS(biHostilePedBlip)
	
	FOR i = 0 TO (FMMC_MAX_PEDS-1)
		IF IS_BIT_SET(iPedTagCreated[GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i))
	        REMOVE_PED_OVERHEAD_ICONS(iPedTag[i], i, iPedTagCreated, iPedTagInitalised)
	    ENDIF
		IF DOES_BLIP_EXIST(biPedBlip[i])
			CDEBUG2LN(DEBUG_MC_BLIP, "[RCC MISSION] Cleanup all: Removing blip for ped ", i, ".")
			REMOVE_BLIP(biPedBlip[i])
		ENDIF
	ENDFOR

	FOR i = 0 TO (FMMC_MAX_VEHICLES-1)
		IF DOES_BLIP_EXIST(biVehBlip[i])
			CDEBUG2LN(DEBUG_MC_BLIP, "[RCC MISSION] Cleanup all: Removing blip for veh ", i, ".")
			REMOVE_VEHICLE_BLIP(i)
		ENDIF
	ENDFOR
	FOR i = 0 TO (FMMC_MAX_NUM_OBJECTS-1)
		CDEBUG2LN(DEBUG_MC_BLIP, "[RCC MISSION] Cleanup all: Removing blip for package ", i, ".")
		REMOVE_OBJECT_BLIP(i)
	ENDFOR
	FOR i = 0 TO (FMMC_MAX_GO_TO_LOCATIONS -1)
		IF DOES_BLIP_EXIST(LocBlip[i])
			CDEBUG2LN(DEBUG_MC_BLIP, "[RCC MISSION] Cleanup all: Removing blip for location(1) ", i, ".")
			REMOVE_BLIP(LocBlip[i])
		ENDIF
		IF DOES_BLIP_EXIST(LocBlip1[i])
			CDEBUG2LN(DEBUG_MC_BLIP, "[RCC MISSION] Cleanup all: Removing blip for location(2) ", i, ".")
			REMOVE_BLIP(LocBlip1[i])
		ENDIF
	ENDFOR
	
	// Remove pickup blips
	FOR i = 0 TO (FMMC_MAX_WEAPONS-1)
		IF DOES_BLIP_EXIST(biPickup[i])
			CDEBUG2LN(DEBUG_MC_BLIP, "[RCC MISSION] Cleanup all: Removing blip for pickup ", i, ".")
			REMOVE_BLIP(biPickup[i])
		ENDIF
	ENDFOR
	
	IF DOES_BLIP_EXIST(DeliveryBlip)
		CDEBUG2LN(DEBUG_MC_BLIP, "[RCC MISSION] Cleanup all: Removing delivery blip.")
		REMOVE_BLIP(DeliveryBlip)
	ENDIF
	
	IF DOES_BLIP_EXIST(biMissionBounds) 
		CDEBUG2LN(DEBUG_MC_BLIP, "[RCC MISSION] Cleanup all: Removing bounds blip.")
		REMOVE_BLIP(biMissionBounds)
	ENDIF
	
ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: POPULATE RESULTS !
//
//************************************************************************************************************************************************************



PROC POPULATE_RESULTS()

BOOL bPass
INT iLocalPlayerJP = -1
FLOAT fRoundCashRewardGained
INT iWinningPlayer = -1


	IF NOT IS_BIT_SET(iLocalBoolCheck, ciPOPULATE_RESULTS)
		
		#IF IS_DEBUG_BUILD
		PRINTLN("[RCC MISSION] - populating results:")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
		
		START_NET_TIMER(tdresultstimer)
		
		SET_BIT(iLocalBoolCheck,ciPOPULATE_RESULTS)
		
		SET_MP_DECORATOR_BIT(LocalPlayer, MP_DECORATOR_BS_INVALID_TO_SPECTATE)
		CLEAR_ALL_BIG_MESSAGES()
			
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_AlwaysAllPlayersSkipNJVS)
			PRINTLN("[RCC MISSION] POPULATE_RESULTS - ciOptionsBS27_AlwaysAllPlayersSkipNJVS is set. Calling: SET_FM_FLOW_MISSION_SKIP_NJVS")
			SET_FM_FLOW_MISSION_SKIP_NJVS()
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_MissionPassAllPlayersSkipNJVS)
		AND HAS_TEAM_PASSED_MISSION(MC_playerBD[iPartToUse].iteam)
			PRINTLN("[RCC MISSION] POPULATE_RESULTS - Team Passed and ciOptionsBS27_MissionPassAllPlayersSkipNJVS is set. Calling: SET_FM_FLOW_MISSION_SKIP_NJVS")
			SET_FM_FLOW_MISSION_SKIP_NJVS()
		ENDIF
		
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_START_SPECTATOR)
			IF not HAS_TEAM_PASSED_MISSION(MC_playerBD[iPartToUse].iteam)
				POPULATE_FAIL_RESULTS()
				
				IF IS_LOADED_MISSION_TYPE_FLOW_MISSION()
				OR VCM_FLOW_IS_THIS_MISSION_VCM_FLOW(g_FMMC_STRUCT.iRootContentIDHash)
					SET_FM_FLOW_MISSION_SKIP_NJVS()
				ENDIF
			ENDIF
			PRINTLN("[RCC MISSION] ___RESULTS___ :QUIT WAS ONLY SPECTATOR")
			IF g_bVSMission
				IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
					SET_SHOW_QUICK_RESTART_OPTION()
					g_bAllowJobReplay = TRUE
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_STOPPED_ARENA_MUSIC_FOR_JIP)
				IF CONTENT_IS_USING_ARENA()
				AND DID_I_JOIN_MISSION_AS_SPECTATOR()
					SCRIPT_TRIGGER_MUSIC_EVENT_STOP()
					PRINTLN("[RCC MISSION] MUSIC - TRIGGER_MUSIC_EVENT(MP_MC_STOP) - F - DID_I_JOIN_MISSION_AS_SPECTATOR")
					SET_BIT(iLocalBoolCheck30, LBOOL30_STOPPED_ARENA_MUSIC_FOR_JIP)
				ENDIF
			ENDIF
				
			EXIT
		ENDIF
		#IF IS_DEBUG_BUILD
			INT iteam
			PRINTLN("[RCC MISSION] *************************************")
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] ________________RESULTS______________")
			PRINTLN("[RCC MISSION] MISSION NAME = ",g_FMMC_STRUCT.tl63DebugMissionName)
			PRINTLN("[RCC MISSION] MY TEAM = ", MC_playerBD[iPartToUse].iteam)
			PRINTLN("[RCC MISSION] GET_TEAM_FINISH_POSITION(MC_playerBD[iPartToUse].iteam) = ", GET_TEAM_FINISH_POSITION(MC_playerBD[iPartToUse].iteam))
			PRINTLN("[RCC MISSION] MC_serverBD.iTeamScore[MC_playerBD[iPartToUse].iteam] = ", MC_serverBD.iTeamScore[MC_playerBD[iPartToUse].iteam])
			PRINTLN("[RCC MISSION] MC_serverBD.iWinningTeam = ", MC_serverBD.iWinningTeam)
			PRINTLN("[RCC MISSION] MC_serverBD.iSecondTeam = ", MC_serverBD.iSecondTeam)
			PRINTLN("[RCC MISSION] MC_serverBD.iThirdTeam = ", MC_serverBD.iThirdTeam)
			PRINTLN("[RCC MISSION] MC_serverBD.iLosingTeam = ", MC_serverBD.iLosingTeam)
			IF MC_serverBD.iWinningTeam > -1
			AND MC_serverBD.iWinningTeam < FMMC_MAX_TEAMS
				PRINTLN("[RCC MISSION] MC_serverBD.iWinningTeam score =  ", MC_serverBD.iTeamScore[MC_serverBD.iWinningTeam])
			ENDIF
			PRINTLN("[RCC MISSION] DOES_TEAM_LIKE_TEAM winners = ", DOES_TEAM_LIKE_TEAM(MC_playerBD[iPartToUse].iteam,MC_serverBD.iWinningTeam))
			PRINTLN("[RCC MISSION] ALL_OTHER_HOSTILE_TEAMS_FAILED = ", ALL_OTHER_HOSTILE_TEAMS_FAILED(MC_playerBD[iPartToUse].iteam))
			
			FOR iteam = 0 TO (FMMC_MAX_TEAMS-1)
				PRINTLN("[RCC MISSION]  ***  TEAM *** ", iteam)
				PRINTLN("[RCC MISSION] GET_TEAM_FINISH_POSITION(iteam) = ", GET_TEAM_FINISH_POSITION(iteam))
				PRINTLN("[RCC MISSION] MC_serverBD.iTeamScore[iteam] = ", MC_serverBD.iTeamScore[iteam])
				
				IF g_FMMC_STRUCT.sFMMCEndConditions[iteam].iMissionTargetScore = -1
					IF MC_serverBD.iNumberOfPlayingPlayers[iteam] > 0
						PRINTLN("[RCC MISSION] pass score is number of players = ", MC_serverBD.iNumberOfPlayingPlayers[iteam])
					ELSE
						PRINTLN("[RCC MISSION] pass score is number of players but that is 0 so setting to: ", MC_serverBD.iNumberOfPlayingPlayers[iteam])
					ENDIF
				ELIF g_FMMC_STRUCT.sFMMCEndConditions[iteam].iMissionTargetScore != 0
					PRINTLN("[RCC MISSION] pass score is mission target score = ", g_FMMC_STRUCT.sFMMCEndConditions[iteam].iMissionTargetScore)
				ELSE
					PRINTLN("[RCC MISSION] pass score is mission default score = ", 1)
				ENDIF
			ENDFOR
			
			PRINTLN("[JS] MEDAL - PLAYER SCORE: ", MC_Playerbd[iPartToUse].iPlayerScore)
			PRINTLN("[JS] MEDAL - PLAYER FAKE SCORE: ", MC_playerBD_1[iPartToUse].iOffRuleMinigameScoreForMedal)

		#ENDIF
		
		IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
			IF AM_I_ON_A_HEIST()
				MC_playerBD[iLocalPart].iRowanCSuperHighScore = GET_HEIST_COMPLETION_HIGH_SCORE(iPartToUse)
				HANDLE_HEIST_TELEMETRY()
			ELIF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
				MC_playerBD[iLocalPart].iRowanCSuperHighScore = GET_MISSION_COMPLETION_HIGH_SCORE()
				HANDLE_GANGOPS_TELEMETRY()
			ELIF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE()
				MC_playerBD[iLocalPart].iRowanCSuperHighScore = GET_MISSION_COMPLETION_HIGH_SCORE()
				HANDLE_CASINO_HEIST_TELEMETRY(DEFAULT, HAS_TEAM_PASSED_MISSION(MC_playerBD[iLocalPart].iteam))
				IF CHECK_TO_SEE_IF_THIS_IS_THE_LAST_STRAND_MISSION()
					IF HAS_TEAM_PASSED_MISSION(MC_playerBD[iLocalPart].iteam)
					AND GB_GET_LOCAL_PLAYER_GANG_BOSS() != LocalPlayer
					AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
						SET_H3_QUICKMATCH_COOLDOWN_TIMER()
						PRINTLN("[JS] - POPULATE_RESULTS-  Setting SET_H3_QUICKMATCH_COOLDOWN_TIMER")
					ENDIF
				ENDIF
			ELIF IS_ARENA_WARS_JOB()
				g_sArena_Telemetry_data.m_playingtime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdMissionLengthTimer)
				g_sArena_Telemetry_data.m_totalPoints = MC_playerBD[iLocalPart].iPlayerScore
				IF DOES_TEAM_LIKE_TEAM(MC_playerBD[iPartToUse].iteam, MC_serverBD.iWinningTeam)
					g_sArena_Telemetry_data.m_endreason = 1
					g_sArena_Telemetry_data.m_winConditions = 1
				ELSE
					g_sArena_Telemetry_data.m_endreason = 0
				ENDIF
				g_sArena_Telemetry_data.m_publiccontentid = g_FMMC_STRUCT.iRootContentIDHash
				PRINTLN("[ARENA][TEL] - RACE g_sArena_Telemetry_data.m_publiccontentid - ", g_sArena_Telemetry_data.m_publiccontentid)
				PRINTLN("[ARENA][TEL] - Playing Time ", g_sArena_Telemetry_data.m_playingtime)
				PRINTLN("[ARENA][TEL] - Total Points ", g_sArena_Telemetry_data.m_totalPoints)
				PRINTLN("[ARENA][TEL] - End Reason ", g_sArena_Telemetry_data.m_endreason)
				PRINTLN("[ARENA][TEL] - Win Condition ", g_sArena_Telemetry_data.m_winConditions)
			ELIF VCM_FLOW_IS_THIS_MISSION_VCM_FLOW(g_FMMC_STRUCT.iRootContentIDHash)
				g_sCasino_Story_Telemetry_data.m_MissionID = g_FMMC_STRUCT.iRootContentIDHash
				g_sCasino_Story_Telemetry_data.m_PlayingTime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdMissionLengthTimer)
				PRINTLN("[ARENA][TEL] - MissionID ", g_sCasino_Story_Telemetry_data.m_MissionID)
				PRINTLN("[ARENA][TEL] - Playing Time ", g_sCasino_Story_Telemetry_data.m_PlayingTime)
				HANDLE_CASINO_TELEMETRY()		
			ELSE
				MC_playerBD[iLocalPart].iRowanCSuperHighScore = GET_MISSION_COMPLETION_HIGH_SCORE()
			ENDIF
		ENDIF

		// clear blips etc
		CLEANUP_ALL_BLIPS()
		CLEAR_PRINTS()
		Clear_Any_Objective_Text()
		CLEAR_HELP()
					
							
		// B*2124109 - Crew Cut Achievement should be given out when completing a job - on heists it should only be given if you pass
		IF NOT AM_I_ON_A_HEIST()
		AND NOT GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FINALE()
		AND NOT CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE()
			PRINTLN("[LH][ACHIEVEMENTS] Mission Controller calling DO_CREW_CUT_ACHIEVEMENT")
			DO_CREW_CUT_ACHIEVEMENT()
		ENDIF
		
		IF IS_BIT_SET(ilocalboolcheck,LBOOL_TRIGGER_CTF_END_MUSIC)
		OR IS_BIT_SET( g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciUSE_COUNTDOWN )
			IF MC_serverBD.iReasonForObjEnd[MC_playerBD[iLocalPart].iteam] = OBJ_END_REASON_TARGET_SCORE
			OR IS_BIT_SET( g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciUSE_COUNTDOWN )
				TRIGGER_MUSIC_EVENT("MP_DM_COUNTDOWN_KILL")
				PRINTLN("[RCC MISSION] TRIGGER_MUSIC_EVENT(\"MP_DM_COUNTDOWN_KILL\")")
			ENDIF
		ENDIF
		
		IF HAS_TEAM_PASSED_MISSION(MC_playerBD[iLocalPart].iteam)
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] TEAM HAS PASSED = ", MC_playerBD[iLocalPart].iteam)
			bPass = TRUE
			SET_BIT(iLocalBoolCheck2,LBOOL2_PASSED_MISSION)
			
			IF MC_serverBD.iTeamScore[MC_playerBD[iLocalPart].iteam] > 0
				SET_MISSION_TEST_COMPLETE()
			ENDIF
			
			IF NOT IS_BIT_SET(ilocalboolcheck,LBOOL_TRIGGER_CTF_END_MUSIC)
			//2081710 - Humane Labs | Keycodes: Music during Karen arrival cutscene needs updating
			AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
				IF NOT ARE_STRINGS_EQUAL( sMocapCutscene, "MPH_TUT_EXT" ) //Stop music cutting out too early in Fleeca cutscene
				AND NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC)
					SCRIPT_TRIGGER_MUSIC_EVENT_STOP()
					PRINTLN("[RCC MISSION] MUSIC - TRIGGER_MUSIC_EVENT(MP_MC_STOP) - F")
				ENDIF
			ENDIF
			IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
				g_bPassedMyTutorialMission = TRUE
				g_bFailedTutorialMission = FALSE
			ENDIF
			
			//Call the flow strand mission data
			IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
				SET_FLOW_STRAND_PROGRESS_ON_MISSION_COMPLEATION(IS_LOCAL_PLAYER_ANY_SPECTATOR(), CHECK_TO_SEE_IF_THIS_IS_THE_LAST_STRAND_MISSION())
			ENDIF
		
			IF AM_I_ON_A_HEIST()
				CPRINTLN(DEBUG_ACHIEVEMENT, "[ACHIEVEMENT] - HEIST MISSION DONE")
				
				SET_UGC_PLAYER_DATA_FOR_HEIST()
				
				//Vehicle unlocks:
				HANDLE_HEIST_VEHICLE_UNLOCKS()
				//Completion awards:
				HANDLE_HEIST_AWARDS()
				//Stats:
				HANDLE_HEIST_STATS()
				
				IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
				//2096554- Key Codes - Missions that are split in two are unlocking achievements at the half way point; crew cut for example
				AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
					//Finale completion achievements:
					HANDLE_HEIST_ACHIEVEMENTS()
				ENDIF
			ENDIF
			
			IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
				HANDLE_GANGOPS_AWARDS()
			ENDIF
			
			IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE()
			AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
				HANDLE_CASINO_HEIST_AWARDS()
			ENDIF
			
			IF VCM_FLOW_IS_THIS_MISSION_VCM_FLOW(g_FMMC_STRUCT.iRootContentIDHash)
				HANDLE_CASINO_HEIST_PART_1_REWARDS()
			ENDIF
			
		ELSE
			IF NOT IS_BIT_SET(ilocalboolcheck,LBOOL_TRIGGER_CTF_END_MUSIC)
			AND NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC)
				SCRIPT_TRIGGER_MUSIC_EVENT_FAIL()
				PRINTLN("TRIGGER_MUSIC_EVENT(\"MP_MC_FAIL\") Pr1")
			ENDIF
			IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
				g_bFailedTutorialMission = TRUE
				g_bPassedMyTutorialMission = FALSE
				g_vFMMC_MissionStartPos = <<360.3316, 290.4116, 102.4993>>  //<<364.9413,278.7449,102.2490>>
				SET_LOCAL_PLAYER_SHOULD_SPANW_AT_LAMARS_HOUSE(FALSE)
				PRINTLN("[dsw] Set g_vFMMC_MissionStartPos = ", g_vFMMC_MissionStartPos)
				PRINTLN("[dsw] [DO_TUTORIAL_MISSION_INTRO_MOCAP] g_bFailedTutorialMission = TRUE")
			ENDIF
		ENDIF	
		
		
		INT iRootContentID = GET_STRAND_ROOT_CONTENT_ID_HASH()
	
		IF iRootContentID = 0
			PRINTLN("[AMEC][HEIST_MISC] - POPULATE_RESULTS - RootContentID is 0, using g_FMMC_STRUCT data: ", g_FMMC_STRUCT.iRootContentIdHash)
			iRootContentID = g_FMMC_STRUCT.iRootContentIdHash
		ELSE
			PRINTLN("[AMEC][HEIST_MISC] - POPULATE_RESULTS - GET_STRAND_ROOT_CONTENT_ID_HASH() = ", iRootContentID)
		ENDIF
		
		// KGM 24/2/15 [BUG 2248479]: Adding check as instructed by Alastair to prevent this stuff running at end of Pacific Standard Heist Part One (was also running after Part Two)
		IF (SHOULD_END_OF_MISSION_HEIST_LOGIC_BE_PROCESSED_BECAUSE_OF_STRAND_MISSION()) // AKA "No mission is chained AFTER this one"
			
			
			INT index
			TEXT_LABEL_23 tlRContID
			BOOL bAmLeader

			IF GET_HEIST_AM_I_HEIST_LEADER_GLOBAL()
			OR IS_BIT_SET(MC_playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, PBBOOL_HEIST_HOST)
			
				PRINTLN("[AMEC][HEIST_MISC] - POPULATE_RESULTS - LEADER - IS_BIT_SET(PBBOOL_HEIST_HOST) = ", IS_BIT_SET(MC_playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, PBBOOL_HEIST_HOST))
				tlRContID = GET_HEIST_FINALE_STAT_DATA()
				IF NOT IS_STRING_NULL_OR_EMPTY(tlRContID)
				AND NOT ARE_STRINGS_EQUAL(tlRContID, ".")
					g_HeistTelemetryData.iRootContentIdHash = GET_HASH_KEY(tlRContID)
					PRINTLN("[AMEC][HEIST_MISC] - POPULATE_RESULTS - LEADER - g_HeistTelemetryData.iRootContentIdHash = ", g_HeistTelemetryData.iRootContentIdHash)
				ELSE
					PRINTLN("[AMEC][HEIST_MISC] - POPULATE_RESULTS - LEADER - Not saving tlRContID value: ", tlRContID)
				ENDIF
			ELSE
				REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() index
					IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(index))
						IF IS_BIT_SET(MC_playerBD[index].iClientBitSet, PBBOOL_HEIST_HOST)
							PLAYER_INDEX playerIndex = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(index))
							IF IS_NET_PLAYER_OK(playerIndex, FALSE, FALSE)
							
								tlRContID = GlobalplayerBD_FM_HeistPlanning[NATIVE_TO_INT(playerIndex)].tlHeistFinaleRootContID
							
								IF NOT IS_STRING_NULL_OR_EMPTY(tlRContID)
								AND NOT ARE_STRINGS_EQUAL(tlRContID, ".")
									g_HeistTelemetryData.iRootContentIdHash = GET_HASH_KEY(tlRContID)
									PRINTLN("[AMEC][HEIST_MISC] - POPULATE_RESULTS - MEMBER - g_HeistTelemetryData.iRootContentIdHash = ", g_HeistTelemetryData.iRootContentIdHash)
								ELSE
									PRINTLN("[AMEC][HEIST_MISC] - POPULATE_RESULTS - MEMBER - Not saving tlRContID value: ", tlRContID)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF

			IF bPass
			
				IF Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
	
					IF NOT IS_PLAYER_SCTV(LocalPlayer) 
						g_sJobHeistInfo.m_medal = GET_HEIST_COMPLETION_RATING(iLocalPart)
						
						#IF IS_DEBUG_BUILD
							PRINTLN("[AMEC][HEIST_MISC] - POPULATE_RESULTS - Saving medal details - value: ", g_sJobHeistInfo.m_medal)
						#ENDIF
					ENDIF
					
				ELIF Is_Player_Currently_On_MP_Heist(LocalPlayer)
				
					#IF IS_DEBUG_BUILD
					IF NOT g_bDebugLaunchedMission
					#ENDIF
					
						IF NOT IS_PLAYER_SCTV(LocalPlayer) 
							g_sJobHeistInfo.m_medal = GET_HEIST_COMPLETION_RATING(iLocalPart)
							
							#IF IS_DEBUG_BUILD
								PRINTLN("[AMEC][HEIST_MISC] - POPULATE_RESULTS - FIN - Saving medal details - value: ", g_sJobHeistInfo.m_medal)
							#ENDIF
							
							IF GET_HEIST_AM_I_HEIST_LEADER_GLOBAL()
							OR IS_BIT_SET(MC_playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, PBBOOL_HEIST_HOST)
								bAmLeader = TRUE
							ELSE
								bAmLeader = FALSE
							ENDIF
							
							SET_HEIST_TOTAL_COMPLETED_FOR_MISSION_INDEX(iRootContentID, bAmLeader)
							HANDLE_UNLOCKING_REPLAY_BOARD_AS_MEMBER(bAmLeader)
						ENDIF
						
					ELIF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
						IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FINALE()
							IF NOT IS_PLAYER_SCTV(LocalPlayer) 
								g_sJobGangopsInfo.m_infos.m_medal = GET_HEIST_COMPLETION_RATING(iLocalPart)
								PRINTLN("[JS][GOTEL][TEL] - POPULATE_RESULTS - Setting medal details (finale): ", g_sJobGangopsInfo.m_infos.m_medal)
								IF GB_GET_LOCAL_PLAYER_GANG_BOSS() = LocalPlayer
									bAmLeader = TRUE
								ELSE
									bAmLeader = FALSE
								ENDIF
								//Set num times completed if required
								//Unlock "replay board" if required
							ENDIF
						ELSE
							IF NOT IS_PLAYER_SCTV(LocalPlayer) 
								g_sJobGangopsInfo.m_infos.m_medal = GET_HEIST_COMPLETION_RATING(iLocalPart)
								PRINTLN("[JS][GOTEL][TEL] - POPULATE_RESULTS - Setting medal details: ", g_sJobGangopsInfo.m_infos.m_medal)
							ENDIF
						ENDIF
					ELIF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
						GET_HEIST_COMPLETION_RATING(iLocalPart) //Called to populate the array, don't care about the return value.
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("[AMEC][HEIST_MISC] - POPULATE_RESULTS - Completed mission is FINALE but g_bDebugLaunchedMission = TRUE; not saving stats.")
					ENDIF
					#ENDIF
				ELSE
					PRINTLN("[AMEC][HEIST_MISC] - POPULATE_RESULTS - Completed mission is not FINALE or SETUP, do nothing.")
				ENDIF
				
			ENDIF
		ELSE
			PRINTLN("[AMEC][HEIST_MISC] - POPULATE_RESULTS - Completed mission is Part One of a Strand Mission, do nothing.")
		ENDIF
		
		
		MANAGE_DAILY_OBJECTIVES(bPass)
		
		IF bPass
		OR g_bVSMission
			
			INT iLBDIndexWinner = GET_LEADERBOARD_INDEX_OF_TOP_PLAYER_ON_TEAM(MC_playerBD[iLocalPart].iteam)
			INT iteamwinner 	= NATIVE_TO_INT(g_MissionControllerserverBD_LB.sleaderboard[iLBDIndexWinner].playerID)
			PRINTLN("[RCC MISSION] [JOB POINT] -iteamwinner lb index=  ",iteamwinner, " iLBDIndexWinner = ", iLBDIndexWinner)
			PRINTLN("[RCC MISSION] [JOB POINT] local player: ",NATIVE_TO_INT(LocalPlayer))
		
			// Add MVP Job Point
			IF MC_serverBD.iNumActiveTeams > 1	
			AND NOT g_bOnCoopMission
				iLocalPlayerJP = NETWORK_PLAYER_ID_TO_INT()
				IF iLocalPlayerJP <> -1
					GlobalplayerBD_FM[iLocalPlayerJP].sPlaylistVars.iMVPThisJob = iteamwinner
					PRINTLN("[RCC MISSION] [JOB POINT] iLocalPlayerJP = ", iLocalPlayerJP, " iteamwinner = ", iteamwinner)
				ENDIF
			ENDIF

			IF DOES_TEAM_LIKE_TEAM(MC_playerBD[iPartToUse].iteam,MC_serverBD.iWinningTeam)
				sCelebrationStats.iLocalPlayerJobPoints = GET_POINTS_FOR_POSITION(GET_TEAM_FINISH_POSITION(MC_serverBD.iWinningTeam), iLocalPlayerJP)
			ELSE
				sCelebrationStats.iLocalPlayerJobPoints = GET_POINTS_FOR_POSITION(GET_TEAM_FINISH_POSITION(MC_playerBD[iLocalPart].iteam), iLocalPlayerJP)
			ENDIF
			
			IF NOT IS_THIS_A_ROUNDS_MISSION() // SEE (ADD_MISSION_JOB_POINTS_ROUNDS)
			OR SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
				ADD_TO_JOB_POINTS_TOTAL(sCelebrationStats.iLocalPlayerJobPoints)
			ELSE
				PRINTLN("[RCC MISSION] [JOB POINT] LBOOL6_HIDE_JOB_POINTS_CELEB_SCREEN: ")
				SET_BIT(iLocalBoolCheck6, LBOOL6_HIDE_JOB_POINTS_CELEB_SCREEN)
			ENDIF
			
		ENDIF
		
		//FAIL REASONS 
		IF !bPass
		// FAIL -CVJump1		
			POPULATE_FAIL_RESULTS()
			
			IF IS_LOADED_MISSION_TYPE_FLOW_MISSION()
			OR VCM_FLOW_IS_THIS_MISSION_VCM_FLOW(g_FMMC_STRUCT.iRootContentIDHash)
				SET_FM_FLOW_MISSION_SKIP_NJVS()
			ENDIF
		endif
				
		IF g_bVSMission OR NOT bPass
			IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
				SET_SHOW_QUICK_RESTART_OPTION()
				g_bAllowJobReplay = TRUE
				IF DOES_CURRENT_MISSION_USE_GANG_BOSS()
				AND IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_MISSION_ENDING_BECAUSE_PLAYERS_LEFT)
					g_bFMMCCriticalFail = TRUE
				ENDIF
			ENDIF
		ENDIF
 
		INT iCashGained
		INT iEliteCashGained
		INT iXPRewardToGive 
		INT iLBPosition		= GET_PLAYERS_LEADERBOARD_POSITION(LocalPlayer)
		INT iTeamLBPosition = GET_PLAYERS_TEAM_LEADERBOARD_POSITION(LocalPlayer, MC_playerBD[iLocalPart].iteam)
		INT iNumPlayers 	= (MC_serverBD.iNumberOfLBPlayers[0] + MC_serverBD.iNumberOfLBPlayers[1] + MC_serverBD.iNumberOfLBPlayers[2] + MC_serverBD.iNumberOfLBPlayers[3])
		INT iAverageRank 	= GET_AVERAGE_RANK_OF_PLAYERS_IN_JOB(iNumPlayers)
		INT iTeamPosition 	= GET_TEAM_FINISH_POSITION(MC_playerBD[iPartToUse].iteam) //(iLBPosition+1)
		
		iLBPositionStore = iLBPosition
		iTeamLBPositionStore = iTeamLBPosition
		
		
		// Give out awards
		HANDLE_END_AWARDS(iTeamPosition, MC_playerBD[iPartToUse].iteam)
		
		INT iTeamRankForRewards = iTeamPosition
		
		INT iTeamLoop
		FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS - 1
			IF DOES_TEAM_LIKE_TEAM( MC_playerBD[iPartToUse].iteam, iTeamLoop ) // Check to see if this is a friendly team
			AND GET_TEAM_FINISH_POSITION( iTeamLoop ) < iTeamRankForRewards // and if they have done better than we have, we use their scores
				iTeamRankForRewards = GET_TEAM_FINISH_POSITION( iTeamLoop )
			ENDIF
		ENDFOR
				
		// Speirs added 1698072 whilst Rowan was smoking Cuban cigars and gettin beat at chess
		IF Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)
			// Team finish position
			
			INT iWinnersBonus = 0
			// Timer for reward deductions based on time played
			SCRIPT_TIMER stTimer
			START_NET_TIMER(stTimer)
			stTimer.Timer = GET_NETWORK_TIME()
			// 1708103 Team positions to go into the cash formula only
			INT iTeamCashPosition
			IF iTeamPosition = 1
				iTeamCashPosition = 2 
				iWinnersBonus = 100
			ELIF iTeamPosition = 2
				iTeamCashPosition = 4
			ELIF iTeamPosition = 3
				iTeamCashPosition = 5
			ELSE
				iTeamCashPosition = 6
			ENDIF
			// RP positions to go into formula
			INT iRPWinnersBonus = 0
			INT iTeamRPPosition
			IF iTeamPosition = 1
				iTeamRPPosition = 2
				iRPWinnersBonus = 100
			ELSE
				iTeamRPPosition = 4
			ENDIF
			
			// Cash			
			iCashGained 	= GET_END_OF_JOB_CASH(iNumPlayers, 20, iAverageRank, iTeamCashPosition, MC_serverBD.tdMissionLengthTimer, stTimer, FALSE, REWARD_JOB_LTS)
			iCashGained		= (iCashGained + iWinnersBonus)
			iCashGained 	= MULTIPLY_CASH_BY_TUNABLE(iCashGained) 
			PRINTLN("[RCC MISSION] [CASHRP] LTSiCashGained = ", iCashGained, " iTeamCashPosition = ", iTeamCashPosition, " iWinnersBonus = ", iWinnersBonus)			
			
			// RP
			iXPRewardToGive = GET_LTS_END_RP_AWARD(iNumPlayers, 20, iAverageRank, iTeamRPPosition, MC_serverBD.tdMissionLengthTimer, (iTeamPosition = 1))
			iXPRewardToGive = (iXPRewardToGive + iRPWinnersBonus)
			PRINTLN("[RCC MISSION][CASHRP] iXPRewardToGive LTS: ", iXPRewardToGive, " iNumPlayers = ", iNumPlayers, " iNumPlayers = ", iNumPlayers, " iAverageRank = ", iAverageRank)
			iXPRewardToGive = HANDLE_GIVING_END_REWARDS(iCashGained,iXPRewardToGive,bPass,iLBPosition)
			PRINTLN("[RCC MISSION] [CASHRP] iXPRewardToGive LTS after HANDLE_GIVING_END_REWARDS = : ", iXPRewardToGive, " iCashGained = ", iCashGained, " iTeamRPPosition = ", iTeamRPPosition)

		ELSE		
			INT iTotalMissionEndTime = MC_serverBD.iTotalMissionEndTime
			PRINTLN("[RCC MISSION] Before HANDLE_FINAL_XP_REWARD MC_serverBD.iTotalMissionEndTime = ",MC_serverBD.iTotalMissionEndTime)

			//If not grabbed server time, use local time
			IF iTotalMissionEndTime <= 0
				iTotalMissionEndTime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( MC_serverBD.tdMissionLengthTimer )
				PRINTLN("[RCC MISSION] iTotalMissionEndTime <= 0 taking local time as ",iTotalMissionEndTime)
			ENDIF
			
			iTotalMissionEndTime += 10000
			
			PRINTLN("[RCC MISSION] modded with 10 sec bonus iTotalMissionEndTime = ",iTotalMissionEndTime)
		
			IF HAS_FIRST_STRAND_MISSION_BEEN_PASSED() OR HAS_FIRST_STRAND_MISSION_BEEN_PASSED_NO_RESET()
				IF IS_LOADED_MISSION_LOW_RIDER_FLOW_MISSION()
					IF bPass
						iTotalMissionEndTime += g_TransitionSessionNonResetVars.iTotalMissionEndTime
						PRINTLN("[RCC MISSION] HAS_FIRST_STRAND_MISSION_BEEN_PASSED total mission time ",iTotalMissionEndTime)
					ENDIF
				ELSE
					iTotalMissionEndTime += g_TransitionSessionNonResetVars.iTotalMissionEndTime
					PRINTLN("[RCC MISSION] HAS_FIRST_STRAND_MISSION_BEEN_PASSED total mission time ",iTotalMissionEndTime)
				ENDIF
			ENDIF
			
			g_iRoundsCombinedTime += iTotalMissionEndTime
			PRINTLN("[MMacK][RoundsTimeBonus] g_iRoundsCombinedTime = ", g_iRoundsCombinedTime)
					
			INT iTeamSorting, iTeamToCheckAgainst
			INT iAdjustedPosition[FMMC_MAX_TEAMS]
			BOOL bHasFriendlyTeams
			
			FOR iTeamSorting = 0 TO FMMC_MAX_TEAMS - 1
				iAdjustedPosition[iTeamSorting] = GET_TEAM_FINISH_POSITION(iTeamSorting)
				
				FOR iTeamToCheckAgainst = 0 TO FMMC_MAX_TEAMS - 1
					IF DOES_TEAM_LIKE_TEAM(iTeamSorting, iTeamToCheckAgainst)
						IF iTeamSorting != iTeamToCheckAgainst
							bHasFriendlyTeams = TRUE
						ENDIF
						IF GET_TEAM_FINISH_POSITION(iTeamSorting) > GET_TEAM_FINISH_POSITION(iTeamToCheckAgainst)	
							iAdjustedPosition[iTeamSorting] = GET_TEAM_FINISH_POSITION(iTeamToCheckAgainst)
						ENDIF
					ENDIF
				ENDFOR
			ENDFOR
			
			IF bHasFriendlyTeams
				#IF IS_DEBUG_BUILD
					FOR iTeamSorting = 0 TO FMMC_MAX_TEAMS - 1
						PRINTLN("[MMacK][RoundsSorting] iAdjustedPosition[",iTeamSorting,"] = ", iAdjustedPosition[iTeamSorting])
					ENDFOR
				#ENDIF
			
				FOR iTeamSorting = 0 TO FMMC_MAX_TEAMS - 1
					IF iAdjustedPosition[iTeamSorting] < GET_TEAM_FINISH_POSITION(MC_playerBD[iPartToUse].iteam)
					AND MC_playerBD[iPartToUse].iteam != iTeamSorting
					AND NOT DOES_TEAM_LIKE_TEAM(iTeamSorting, MC_playerBD[iPartToUse].iteam)
						//IF iTeamRankForRewards > iAdjustedPosition[iTeamSorting] + 1
							iTeamRankForRewards = iAdjustedPosition[iTeamSorting] + 1
							PRINTLN("[MMacK][RoundsSorting] iTeamRankForRewards = ", iTeamRankForRewards)
						//ENDIF
					ENDIF
				ENDFOR
			ENDIF
			
			iCashGained 	= HANDLE_FINAL_CASH_REWARD( bPass, iTeamRankForRewards,iTotalMissionEndTime ) //Old
			
			
			//Stuff for heist celebration screen
			
			
			INT iHeistRewardMMTimer = MC_serverBD.iHeistRewardMMTimer
			FLOAT fHeistVehDamage = MC_serverBD_2.fHeistVehDamage
			FLOAT fVIPDamage = MC_serverBD_2.fVIPDamage
			BOOL bSwatInvolved = IS_BIT_SET(MC_serverBD.iServerBitSet2,SBBOOL2_HEIST_REWARD_SWAT_INVOLVED)
			PRINTLN("[MJM] - 1 bSwatInvolved = ",bSwatInvolved)
			INT iPlayerMostHealthLost = MC_serverBD.iPlayerMostHealthLost
			
			INT iTeamKills[FMMC_MAX_TEAMS]
			INT iTeamDeaths[FMMC_MAX_TEAMS] 
			INT iHeadshots
			INT iHackFails = MC_ServerBD_4.iHackingFails
			
			INT iTeamCounter
			REPEAT FMMC_MAX_TEAMS iTeamCounter
				iTeamKills[iTeamCounter] = MC_serverBD.iTeamKills[iTeamCounter] 
				iTeamDeaths[iTeamCounter] = MC_serverBD.iTeamDeaths[iTeamCounter] 	
				iHeadshots += MC_serverBD.iteamHeadshots[iTeamCounter] 	
			ENDREPEAT
			
			//Add on stuff from previous mission if second mission in strand
			IF HAS_FIRST_STRAND_MISSION_BEEN_PASSED() OR HAS_FIRST_STRAND_MISSION_BEEN_PASSED_NO_RESET()
				
				PRINTLN("[JS] POPULATE_RESULTS - HAS_FIRST_STRAND_MISSION_BEEN_PASSED")
			
				iHeistRewardMMTimer += g_TransitionSessionNonResetVars.iHeistRewardMMTimer
				
				iPlayerMostHealthLost += g_TransitionSessionNonResetVars.iPlayerMostHealthLost
				
				IF g_TransitionSessionNonResetVars.fHeistVehDamage > fHeistVehDamage
					fHeistVehDamage = g_TransitionSessionNonResetVars.fHeistVehDamage
				ENDIF
				
				IF g_TransitionSessionNonResetVars.fVIPDamage > fVIPDamage
					fVIPDamage = g_TransitionSessionNonResetVars.fVIPDamage
				ENDIF
				
				IF g_TransitionSessionNonResetVars.bSwatInvolved 
					bSwatInvolved = TRUE
					PRINTLN("[MJM] - 2 bSwatInvolved = TRUE")
				ENDIF
				
				IF g_TransitionSessionNonResetVars.bHasBeatCashGrab
					bHasBeatCashGrab = TRUE
				ENDIF
				
				REPEAT FMMC_MAX_TEAMS iTeamCounter
					iTeamKills[iTeamCounter] += g_TransitionSessionNonResetVars.iTeamKills[iTeamCounter]
					iTeamDeaths[iTeamCounter] += g_TransitionSessionNonResetVars.iTotalCoopDeaths[iTeamCounter]	
				ENDREPEAT
						
				g_TransitionSessionNonResetVars.iHackFails = iHackFails
				g_TransitionSessionNonResetVars.iHeadshots = iHeadshots	
			ENDIF
					
			IF bPass
				IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
					IF NOT g_TransitionSessionNonResetVars.bHasQuickRestartedDuringStrandMission
						iEliteCashGained = ADD_HEIST_BONUS_CASH_REWARDS(iCurrentHeistMissionIndex,iTotalMissionEndTime,
									fHeistVehDamage, fVIPDamage,  iTeamKills, bSwatInvolved,
									bHasBeatCashGrab,iPlayerMostHealthLost,iHeistRewardMMTimer,
									iTeamDeaths,MC_playerBD[NATIVE_TO_INT(LocalPlayer)].TSPlayerData.iStage > TSS_WAIT,
									MC_playerBD[NATIVE_TO_INT(LocalPlayer)].TSPlayerData.iCost > 0, iHackFails, iHeadshots,
									IS_BIT_SET(MC_serverBD_1.sLEGACYMissionContinuityVars.iGenericTrackingBitset, ciLEGACY_ContinuityGenericTrackingBS_DroppedToDirect))
					ELSE
						g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentDidQuickRestart = TRUE
						PRINTLN("[RCC MISSION] [MJM] No elite challenges as quick restart")
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION] [MJM] No elite challenges as first part of strand mission")
				ENDIF
			ENDIF

			fRoundCashRewardGained = TO_FLOAT(iCashGained)
			fRoundCashRewardGained = (fRoundCashRewardGained/ciROUND_CASH_TO)			
			iCashGained = ROUND(fRoundCashRewardGained)
			iCashGained = (iCashGained * ciROUND_CASH_TO)
			
			// For end heist screen.
			iActualTake = iCashGained
			
			IF IS_ARENA_WARS_JOB(TRUE)
				g_sArena_Telemetry_data.m_cashearned = iActualTake
				PRINTLN("[ARENA][TEL] - MC Cash - ", g_sArena_Telemetry_data.m_cashearned)
			ENDIF
			
			IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
				IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
					IF iEliteCashGained > 0
						HANDLE_GIVING_ELITE_REWARD(iEliteCashGained)
					ENDIF
				ENDIF
			ENDIF
			
			IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FINALE()
				IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
					IF iEliteCashGained > 0
						HANDLE_GIVING_GANGOPS_ELITE_REWARD(iEliteCashGained, GET_GANGOPS_ELITE_CHALLENGE_TYPE())
					ENDIF
				ENDIF
			ENDIF
			
			IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE()
				IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
					IF iEliteCashGained > 0
						HANDLE_GIVING_CASINO_HEIST_ELITE_REWARD(iEliteCashGained)
					ENDIF
				ENDIF
			ENDIF
			
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] iCashGained Normal: ", iCashGained, " rounded to ", ciROUND_CASH_TO)
			
			PRINTLN("[RCC MISSION] Before HANDLE_FINAL_XP_REWARD iTotalMissionEndTime = ",iTotalMissionEndTime)
			iXPRewardToGive = HANDLE_FINAL_XP_REWARD( bPass, iTeamRankForRewards,iTotalMissionEndTime )
			
			iXPRewardToGive = HANDLE_GIVING_END_REWARDS(iCashGained,iXPRewardToGive,bPass,iLBPosition)
			
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DCTL(g_FMMC_STRUCT.iAdversaryModeType)
				INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_CRDEADLINE)
				PRINTLN("[JS] [DEADLINERWD] - Incrementing MP_STAT_CRDEADLINE")
				INT iDeadlinePlayed = GET_MP_INT_CHARACTER_STAT(MP_STAT_CRDEADLINE)
				IF iDeadlinePlayed >= g_sMPTunables.iDEADLINE_VEHICLE_UNLOCK_PLAYS
				AND g_sMPTunables.iDEADLINE_VEHICLE_UNLOCK_PLAYS > -1
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_DEADLINE_RWD_VEHICLE, TRUE)
					SET_BIT(MPGlobalsAmbience.iFmGbHelpBitSet3, BI_FM_DO_DEADLINER_HELP_ON_RETURN_TO_MP)
				ENDIF
				IF iDeadlinePlayed >= g_sMPTunables.iDEADLINE_CLOTHING_UNLOCK_PLAYS
				AND g_sMPTunables.iDEADLINE_CLOTHING_UNLOCK_PLAYS > -1
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_DEADLINE_RWD_CLOTHES, TRUE)
					SET_BIT(MPGlobalsAmbience.iFmGbHelpBitSet3, BI_FM_DO_DEADLINER_HELP_ON_RETURN_TO_MP)
				ENDIF
			ENDIF

			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_POWER_MAD(g_FMMC_STRUCT.iAdversaryModeType)
			OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
			OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(g_FMMC_STRUCT.iAdversaryModeType)
				CHECK_EVENT_AWARD_FOR_COMPLETING_ADVERSARY_MODE()
			ENDIF
			
			IF WVM_FLOW_IS_CURRENT_MISSION_WVM_FLOW()
				CHECK_EVENT_AWARD_FOR_COMPLETING_COVERT_OPERATION()
			ENDIF
			
			//Increment the current mission flow progress if needed
			SVM_FLOW_INCREMENT_MISSION_PASSED_IF_NEEDED(bPass, MC_serverBD.iTotalNumStartingPlayers)
			IF GET_VCM_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash) != eVCM_INVALID
				IF bPass
					SET_PLAYER_HAS_COMPLETED_THIS_VCM(GET_VCM_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash), g_bIamVCMLeader)
					g_bIamVCMLeader = FALSE
					
					SET_FM_FLOW_MISSION_SKIP_NJVS()
					
					IF g_bSendVCMQuickInvite
						g_bSendVCMQuickInvite = FALSE
						PRINTLN("[VCM_FLOW] g_bSendVCMQuickInvite = FALSE")
					ENDIF
					
					IF IS_THIS_A_QUICK_RESTART_JOB()
						PRINTLN("[VCM_FLOW][ML] Used a quick restart checkpoint, so not allowing the award of MP_AWARD_SURVIVALIST")
					ELIF MC_playerBD[iLocalPart].iNumPlayerDeaths = 0
						PRINTLN("[VCM_FLOW][ML] Setting award MP_AWARD_SURVIVALIST because MC_playerBD[iLocalPart].iNumPlayerDeaths = 0")
						SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_SURVIVALIST, TRUE)
					ENDIF
				ELSE
					IF g_bIamVCMLeader
					AND NOT HAS_LOCAL_PLAYER_UNLOCKED_VCM_REPLAY()
					AND HAS_LOCAL_PLAYER_VIEWED_CASINO_PENTHOUSE_MOCAP()
					AND GET_LOCAL_PLAYERS_CURRENT_VCM() = GET_VCM_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash)
						IF !g_bSendVCMQuickInvite
							g_bSendVCMQuickInvite = TRUE
							PRINTLN("[VCM_FLOW] g_bSendVCMQuickInvite = TRUE")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			#IF FEATURE_CASINO_HEIST
			IF IS_THIS_CASINO_HEIST_MISSION_THE_FINAL_STAGE(GET_CASINO_HEIST_MISSION_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash))
				IF bPass
					PROCESS_PLAYER_HAS_COMPLETED_CASINO_HEIST(g_sCasinoHeistMissionConfigData.eChosenApproachType, GB_GET_LOCAL_PLAYER_GANG_BOSS())
					
					SET_FM_FLOW_MISSION_SKIP_NJVS()
				
					IF g_FMMC_STRUCT.iDifficulity = DIFF_HARD
						IF IS_THIS_A_QUICK_RESTART_JOB()
							PRINTLN("[CASINO_HEIST_FLOW][ML] Used a quick restart checkpoint, so not allowing the award of MP_AWARD_PRO")
						ELIF MC_playerBD[iLocalPart].iNumPlayerDeaths = 0
							PRINTLN("[CASINO_HEIST_FLOW][ML] Setting award MP_AWARD_PRO because MC_playerBD[iLocalPart].iNumPlayerDeaths = 0")
							SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_PRO, TRUE)
						ENDIF
					ENDIF
					
					CASINO_HEIST_COMPLETION_REWARDS()
					
					#IF FEATURE_GEN9_EXCLUSIVE
					IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
						END_CASINO_HEIST_UDS_ACTIVITY_TASK(CASINO_HEIST_UDS_ACTIVITY_TASK_HEIST, UDS_ACTIVITY_END_REASON_COMPLETED)
						END_CASINO_HEIST_UDS_ACTIVITY(UDS_ACTIVITY_END_REASON_COMPLETED)
					ENDIF
					#ENDIF
				ELSE
				
				ENDIF
			ENDIF
			#ENDIF
					
//			fRoundXPRewardGained = TO_FLOAT(iXPRewardToGive)
//			fRoundXPRewardGained = (fRoundXPRewardGained/ciROUND_RP_TO)
//			iRoundXPRewardGained = ROUND(fRoundXPRewardGained)
//			iRoundXPRewardGained = (iRoundXPRewardGained * ciROUND_RP_TO)
//			PRINTLN("[RCC MISSION] iRoundXPRewardGained Normal: ", iRoundXPRewardGained, " rounded to ", ciROUND_RP_TO)
			
			IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
			AND NOT IS_PLAYER_SPECTATING(LocalPlayer)
				IF bPass
					REQUEST_SYSTEM_ACTIVITY_TYPE_DONE_HEIST()
				ENDIF
			ENDIF
			

		ENDIF
		
//		IF CONTENT_IS_USING_ARENA()
//			IF MC_serverBD.iNumberOfPlayingTeams > 1
//				HANDLE_END_OF_ARENA_EVENT_PLAYER_CAREER_REWARDS(iTeamLBPosition+1, TRUE, FALSE)
//			ELSE			
//				HANDLE_END_OF_ARENA_EVENT_PLAYER_CAREER_REWARDS(iLBPosition+1, FALSE, FALSE)
//			ENDIF
//		ENDIF
		
		MC_playerBD[iLocalPart].iRewardXP = iXPRewardToGive + iBonusXP
		
		iWinningPlayer = GET_LEADERBOARD_INDEX_OF_TOP_WINNER()
		
		IF NOT IS_THIS_A_ROUNDS_MISSION()
		OR (IS_PLAYER_ON_A_PLAYLIST(LocalPlayer) AND SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()) // 2040227
			IF iWinningPlayer > -1
			AND (g_MissionControllerserverBD_LB.sleaderboard[iWinningPlayer].playerID != INVALID_PLAYER_INDEX())

				MC_playerBD[iLocalPart].iBet = BROADCAST_BETTING_MISSION_FINISHED(g_MissionControllerserverBD_LB.sleaderboard[iWinningPlayer].playerID)
			ENDIF
		ENDIF

		sCelebrationStats.winningPlayer = g_MissionControllerserverBD_LB.sleaderboard[iWinningPlayer].playerID
		sCelebrationStats.iLocalPlayerCash = iCashGained
		sCelebrationStats.iLocalPlayerXP = iXPRewardToGive
		IF NOT GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
			sCelebrationStats.iLocalPlayerXP += iXPExtraRewardGained
		ENDIF
		IF iLBPosition > -1
			IF SHOULD_TEAMS_COMPARE_SCORE()
				sCelebrationStats.iLocalPlayerScore = g_MissionControllerserverBD_LB.sleaderboard[iLBPosition].iPlayerScore
			ELSE
				sCelebrationStats.iLocalPlayerScore = g_MissionControllerserverBD_LB.sleaderboard[iLBPosition].iKills
			ENDIF
			sCelebrationStats.iLocalPlayerPosition = g_MissionControllerserverBD_LB.sleaderboard[iLBPosition].iRank
		ENDIF
		
		sCelebrationStats.iLocalPlayerBetWinnings = MC_playerBD[iLocalPart].iBet
		sCelebrationStats.bPassedMission = bPass
		sCelebrationStats.bIsDraw = FALSE // you cannot draw
		
		IF IS_PLAYER_ON_A_PLAYLIST(LocalPlayer)
		AND NOT IS_BIT_SET(iLocalBoolCheck8, LBOOL8_HANDLE_PLAYLIST_POSITION_DATA_WRITE_CALLED)
			HANDLE_PLAYLIST_POSITION_DATA_WRITE()
			SET_BIT(iLocalBoolCheck6, LBOOL6_SAVED_ROUNDS_LBD_DATAa)
		ENDIF
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[JS] POPULATE_RESULTS already done")	
	#ENDIF		
	ENDIF
	IF NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_SAVED_ROUNDS_LBD_DATAb)
	AND ( NOT CONTENT_IS_USING_ARENA() OR IS_BIT_SET(iLocalBoolCheck30, LBOOL30_FILLED_ARENA_POINTS) )
		// Write rewards, kills etc. are written in (PROCESS_MISSION_END_STAGE)
		PRINTLN("[TS] [MSROUND] - WRITE_TO_ROUNDS_MISSION_END_LB_DATA 1b) ")
		FLOAT fCashMulti = TO_FLOAT(g_iCachedMatchBonus) * g_sMPTunables.cashMultiplier
		INT iCachedValRounded = ROUND(fCashMulti)
		PRINTLN("[RCC_MISSION] [CASH] - POPULATE_RESULTS iCachedValRounded ", iCachedValRounded)
		WRITE_TO_ROUNDS_MISSION_END_LB_DATA(0,
											0,
											0,	
											MC_playerBD[iLocalPart].iRewardXP,
											MC_playerBD[iLocalPart].iCashReward+iCachedValRounded+MC_playerBD[iLocalPart].iBet, 
											DEFAULT,
											DEFAULT,
											MC_playerBD_1[iLocalPart].iApEarned)
		SET_BIT(iLocalBoolCheck6, LBOOL6_SAVED_ROUNDS_LBD_DATAb)
	ENDIF
	
//	#IF FEATURE_ARENA_WARS
//	IF CONTENT_IS_USING_ARENA()
//		INT iFinishPos 
//		IF MC_serverBD.iWinningTeam = MC_playerBD[iLocalPart].iteam
//			iFinishPos = 1 // Winner
//		ELSE
//			iFinishPos = 2 // Losers
//		ENDIF
//		INT iNumPlayers = (MC_serverBD.iNumberOfLBPlayers[0] + MC_serverBD.iNumberOfLBPlayers[1] + MC_serverBD.iNumberOfLBPlayers[2] + MC_serverBD.iNumberOfLBPlayers[3])
//		MC_playerBD_1[iLocalPart].iApEarned = GET_ARENA_EVENT_ARENA_CAREER_POINTS_REWARD((MC_serverBD.iNumberOfTeams > 1), iFinishPos, DEFAULT, iNumPlayers, FALSE)//(NUM_FRIENDS_PLAYING() > 0))
//		PRINTLN("[RCC_MISSION] [CASH] POPULATE_RESULTS, iApEarned = ", MC_playerBD_1[iLocalPart].iApEarned)
//	ENDIF
//	#ENDIF

ENDPROC



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: CELEBRATION THINGS !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************



STRUCT sEarlyCelebrationData
	BOOL bShouldShowPrematurely = FALSE
	INT iTeam 					= -1
	INT iTeamWinningPoints 		= 0
ENDSTRUCT

///PURPOSE: This function checks the rules ahead for other teams to see if, once we've watched this cutscene,
///    we are going to finish the mission anyway. It's used for split missions like Prison: Station and
///    Prison: Wet Work where two teams have different objectives that can be completed in any order
///    
///    RETURNS: an sEarlyCelebrationData struct which holds the team 
///    and the points they would be awarded if they passed all their rules
FUNC sEarlyCelebrationData SHOULD_CELEBRATION_SCREEN_BE_SHOWN_PREMATURELY()
	sEarlyCelebrationData sReturn
	
	sReturn.bShouldShowPrematurely = FALSE
	
	
	INT iMyTeam = MC_playerBD[ iPartToUse ].iteam
	INT iMyCurrentRule = MC_ServerBD_4.iCurrentHighestPriority[ iMyTeam ] + 1 // Check the rule after the cutscene
	
	// Don't care about any of this if we're already failed.
	IF NOT HAS_TEAM_PASSED_MISSION( iMyTeam )
		CWARNINGLN( DEBUG_OWAIN, "[RCC MISSION] We've already failed by the time SHOULD_CELEBRATION_SCREEN_BE_SHOWN_PREMATURELY is called - returning false" )
		RETURN sReturn
	ENDIF

	IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
		INT iSpectatorTargetPart = GET_SPECTATOR_TARGET_PARTICIPANT_OF_PARTICIPANT( iLocalPart )
		IF iSpectatorTargetPart > -1
			iMyTeam = MC_playerBD[ iSpectatorTargetPart ].iTeam
		ENDIF
	ENDIF
	
	sReturn.iTeam = iMyTeam

	IF iMyCurrentRule > MC_serverBD.iMaxObjectives[ iMyTeam ]
		iMyCurrentRule = MC_serverBD.iMaxObjectives[ iMyTeam ]
	ENDIF
	
	INT iPointsForRemainingRules = 0
	
	INT iLoopRules = 0
	INT iLoopTeams = 0
	
	INT iOtherTeamRule = 0
	
	// Loop through all our rules from now until the mission end
	FOR iLoopRules = iMyCurrentRule TO MC_serverBD.iMaxObjectives[ iMyTeam ]
		CWARNINGLN( DEBUG_OWAIN, "[RCC MISSION] Checking rule ", iLoopRules, " for this team for other team progression rules" )

		FOR iLoopTeams = 0 TO FMMC_MAX_TEAMS - 1 // Check all other teams
			iPointsForRemainingRules = 0
			IF IS_TEAM_ACTIVE( iLoopTeams ) AND iLoopTeams != iMyTeam // That are in the mission and aren't us
				IF g_FMMC_STRUCT.sFMMCEndConditions[ iMyTeam ].iObjectiveProgressionLogic[ iLoopRules ][ iLoopTeams ] != -1 // Our team can be progressed by another team
					// Check to see if they've got a progression rule for our current rule - ie could they progress us forwards now
					CWARNINGLN( DEBUG_OWAIN, "[RCC MISSION] Team " ,iLoopTeams,"  has a progression rule " )
				
					FOR iOtherTeamRule = MC_serverBD_4.iCurrentHighestPriority[ iLoopTeams ] TO MC_serverBD.iMaxObjectives[ iLoopTeams ] // And loop all their remaining objectives
						CWARNINGLN( DEBUG_OWAIN, "[RCC MISSION] Checking progression rules for team ", iLoopTeams, " for their rule ",  iOtherTeamRule )
					
						IF iOtherTeamRule >= g_FMMC_STRUCT.sFMMCEndConditions[ iMyTeam ].iObjectiveProgressionLogic[ iLoopRules ][ iLoopTeams ]
							IF iOtherTeamRule < FMMC_MAX_RULES
								IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iMyTeam ].iObjectiveProgMidPointBitset[ iLoopRules ], ciObjProgBS_MoveOnTeamMidpoint_T0 + iLoopTeams )
									
									IF IS_BIT_SET( MC_serverBD.iObjectiveMidPointBitset[ iLoopTeams ], iOtherTeamRule )
									//Or the team has moved past it, so we don't care about the midpoint:
									OR iOtherTeamRule > g_FMMC_STRUCT.sFMMCEndConditions[ iMyTeam ].iObjectiveProgressionLogic[ iLoopRules ][ iLoopTeams ]
										CWARNINGLN( DEBUG_OWAIN, "[RCC MISSION] Midpoint is set" )
										
										iPointsForRemainingRules += GET_FMMC_POINTS_FOR_TEAM( iMyTeam, iLoopRules )
										
										IF iLoopRules = MC_serverBD.iMaxObjectives[ iMyTeam ]
											sReturn.bShouldShowPrematurely = TRUE
											CWARNINGLN( DEBUG_OWAIN, "[RCC MISSION] returning true 1" )
											sReturn.iTeamWinningPoints = iPointsForRemainingRules
											RETURN sReturn
										ENDIF
									ELSE // Or we haven't met that midpoint yet
										CWARNINGLN( DEBUG_OWAIN, "[RCC MISSION] False 1" )
										iOtherTeamRule = MC_serverBD.iMaxObjectives[ iLoopTeams ] // Skip to checking the next team
										sReturn.bShouldShowPrematurely = FALSE
									ENDIF
									
								ELSE // Else it's not a midpoint progression
									CWARNINGLN( DEBUG_OWAIN, "[RCC MISSION] Objective progression logic is true" )
									
									iPointsForRemainingRules += GET_FMMC_POINTS_FOR_TEAM( iMyTeam, iLoopRules )
									IF iLoopRules = MC_serverBD.iMaxObjectives[ iMyTeam ]
										sReturn.bShouldShowPrematurely = TRUE
										CWARNINGLN( DEBUG_OWAIN, "[RCC MISSION] returning true 2" )
										sReturn.iTeamWinningPoints = iPointsForRemainingRules
										RETURN sReturn
									ENDIF
								ENDIF
							ELSE // Else the other team is off the end of their rules
								CWARNINGLN( DEBUG_OWAIN, "[RCC MISSION] Objective progress bitset is set")
								
								iPointsForRemainingRules += GET_FMMC_POINTS_FOR_TEAM( iMyTeam, iLoopRules )
								IF iLoopRules = MC_serverBD.iMaxObjectives[ iMyTeam ]
									sReturn.bShouldShowPrematurely = TRUE
									CWARNINGLN( DEBUG_OWAIN, "[RCC MISSION] returning true 3" )
									sReturn.iTeamWinningPoints = iPointsForRemainingRules
									RETURN sReturn
								ENDIF
							ENDIF
						ELSE // Otherwise their other rule is beyond the current progression rule
							CWARNINGLN( DEBUG_OWAIN, "[RCC MISSION] False 2" )
							sReturn.bShouldShowPrematurely = FALSE
							iOtherTeamRule = MC_serverBD.iMaxObjectives[ iLoopTeams ] // Skip to checking the next team
						ENDIF

					ENDFOR // The other team rule loop
				ELSE // This team does not progress our team this rule
					CWARNINGLN( DEBUG_OWAIN, "[RCC MISSION] Team ", iLoopTeams, " doesn't progress our current rule." )
					
					sReturn.bShouldShowPrematurely = FALSE
					
					// If we've checked all teams for our current rule, and each one doesn't have a progression rule
					// then there's no point in checking our later rules - we already know we're not gonna get progressed
					// so we leave the outer loop here and return false
					// If there WAS a progression rule before now, we would have already returned TRUE
					IF iLoopTeams >= FMMC_MAX_TEAMS - 1
						iLoopRules = MC_serverBD.iMaxObjectives[ iMyTeam ]
						CWARNINGLN( DEBUG_OWAIN, "[RCC MISSION] checked the last team - exiting." )
					ENDIF
					
					// If this team doesn't have a progression rule now, don't bother checking their later rules, check the next team
					iLoopTeams++
				ENDIF
			ENDIF
		ENDFOR // Team loop
	ENDFOR // Our rule loop
	
	// If we're on our last rule, always return true
	IF MC_ServerBD_4.iCurrentHighestPriority[ iMyTeam ] = MC_serverBD.iMaxObjectives[ iMyTeam ]
		sReturn.bShouldShowPrematurely = TRUE
		CWARNINGLN( DEBUG_OWAIN, "[RCC MISSION] we're on our last rule and haven't failed - an early celebration for us!" )
	ENDIF

	CWARNINGLN( DEBUG_OWAIN, "[RCC MISSION] Should the celebration screen be shown prematurely? ", sReturn.bShouldShowPrematurely )
	
	RETURN sReturn
ENDFUNC

PROC SET_DRAW_BLACK_RECTANGLE_FOR_FADED_MOCAP(BOOL bDrawRect)
	
	IF bDrawRect
		IF NOT IS_BIT_SET(iLocalBoolCheck7, LBOOL7_DRAW_BLACK_RECTANGLE)
			SET_BIT(iLocalBoolCheck7, LBOOL7_DRAW_BLACK_RECTANGLE)
			PRINTLN("[RCC MISSION] - [NETCELEBRATION] - [SAC] - set bit iLocalBoolCheck7, LBOOL7_DRAW_BLACK_RECTANGLE.")
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck7, LBOOL7_DRAW_BLACK_RECTANGLE)
			CLEAR_BIT(iLocalBoolCheck7, LBOOL7_DRAW_BLACK_RECTANGLE)
			PRINTLN("[RCC MISSION] - [NETCELEBRATION] - [SAC] - cleared bit iLocalBoolCheck7, LBOOL7_DRAW_BLACK_RECTANGLE.")
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_DRAW_BLACK_RECTANGLE_FOR_FADED_MOCAP()
	
	BOOL bDraw
	
	IF NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDrawBlackRectangle[1] 
		IF IS_BIT_SET(iLocalBoolCheck7, LBOOL7_DRAW_BLACK_RECTANGLE)
//			IF NOT IS_STRING_NULL_OR_EMPTY(sMocapCutscene)
//				IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_NAR_FIN_EXT")
//					bDraw = TRUE
//				ENDIF
//			ENDIF
			IF ARE_STRINGS_EQUAL(tl63_CrowdControlCutscene, tl63_CrowdControlCutscene)
				tl63_CrowdControlCutscene = tl63_CrowdControlCutscene
			ENDIF
//			IF NOT IS_STRING_NULL_OR_EMPTY(tl63_CrowdControlCutscene)
//				IF ARE_STRINGS_EQUAL(tl63_CrowdControlCutscene, "MPH_TUT_MCS1")
//					bDraw = TRUE
//				ENDIF
//			ENDIF
		ENDIF
	ENDIF
	
	IF bDraw
		DRAW_RECT(0.5, 0.5, 3.0, 3.0, 0, 0, 0, 255)
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDrawBlackRectangle[0] = TRUE
	ENDIF
	
ENDPROC

///Determines whether the player the winning or losing postfx:
///Win:
///    - Player passes a contact mission.
///    - Player's team comes first in LTS/Versus
///Lose:
///    - Player fails a contact mission.
///    - Player died early (use bCurrentlyOnEarlyDeathScreen to distinguish between early death and the mission passing/winning while dead).
///    - Player's team didn't win the LTS/Versus mission.
FUNC BOOL SHOULD_POSTFX_BE_WINNER_VERSION(BOOL bCurrentlyOnEarlyDeathScreen = FALSE)
	IF NOT bCurrentlyOnEarlyDeathScreen //Always do a fail transition if the player died early and we're transitioning.
		IF Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)
		OR Is_Player_Currently_On_MP_Versus_Mission(LocalPlayer)
		OR Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer)
			//Can we just use the same function for versus missions?
			IF HAS_TEAM_PASSED_MISSION(MC_playerBD[iLocalPart].iteam)
				PRINTLN("[NETCELEBRATION] SHOULD_POSTFX_BE_WINNER_VERSION is TRUE, player's team won the LTS/Versus mission.")
			
				RETURN TRUE
			ENDIF
		ELSE
			IF HAS_TEAM_PASSED_MISSION(MC_playerBD[iLocalPart].iteam)
				PRINTLN("[NETCELEBRATION] SHOULD_POSTFX_BE_WINNER_VERSION is TRUE, player passed the current mission.")
			
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DO_RENDER_PHASE_PAUSE_FOR_CELEBRATION_SCREEN(BOOL bPlaySoundFx = TRUE, BOOL bPlayResultPostFx = TRUE, BOOL bDoRespawningDuringFailCheck = TRUE)

	#IF IS_DEBUG_BUILD
		PRINTLN("[RCC MISSION] - [NETCELEBRATION] - Calling DO_RENDER_PHASE_PAUSE_FOR_CELEBRATION_SCREEN.")
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	IF IS_SCREEN_FADING_OUT()
		PRINTLN("[RCC MISSION] - [NETCELEBRATION] - DO_RENDER_PHASE_PAUSE_FOR_CELEBRATION_SCREEN - IS_SCREEN_FADING_OUT = TRUE, exiting. ")
		EXIT
	ENDIF
	
	IF bDoRespawningDuringFailCheck
		IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_RESPAWNING_DURING_FAIL)
			PRINTLN("[RCC MISSION] - [NETCELEBRATION] - DO_RENDER_PHASE_PAUSE_FOR_CELEBRATION_SCREEN - LBOOL10_RESPAWNING_DURING_FAIL bit is set, exiting. ")
			EXIT
		ENDIF
	ENDIF
	
	IF MC_serverBD.iNextMission < FMMC_MAX_STRAND_MISSIONS
	AND MC_serverBD.iNextMission >= 0
	AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl23NextContentID[MC_serverBD.iNextMission])
		//Strand mission is going to be initialised soon
		IF g_FMMC_STRUCT.eBranchingTransitionType[MC_serverBD.iNextMission] != FMMC_BRANCHING_MISSION_TRANSITION_TYPE_MOCAP
			PRINTLN("[RCC MISSION] - [NETCELEBRATION][MSRAND] - DO_RENDER_PHASE_PAUSE_FOR_CELEBRATION_SCREEN - Exiting (Airlock or Fade Out strand transition")
			EXIT
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggeredRenderPhasePause)
	
		SET_CELEB_BLOCK_RESURRECT_RENDERPHASE_UNPAUSE(TRUE)
		
		IF bPlayResultPostFx
			
			IF SHOULD_POSTFX_BE_WINNER_VERSION()
				PLAY_CELEB_WIN_POST_FX()
			ELSE
				IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
				OR Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
					PLAY_HEIST_FAIL_FX("HeistCelebFail")
					IF bPlaySoundFx
						PLAY_SOUND_FRONTEND(-1,"Pre_Screen_Stinger","DLC_HEISTS_FAILED_SCREEN_SOUNDS", FALSE)
						PRINTLN("[SAC] - [RCC MISSION] - [NETCELEBRATION] - [CELEB_AUDIO] - DO_RENDER_PHASE_PAUSE_FOR_CELEBRATION_SCREEN - played Pre_Screen_Stinger, DLC_HEISTS_FAILED_SCREEN_SOUNDS")
					ENDIF
				ELSE
					PLAY_CELEB_LOSE_POST_FX()
				ENDIF
			ENDIF
			
		ENDIF
		
		IF NOT Is_Player_Currently_On_MP_Heist(LocalPlayer)
		OR NOT Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
			IF CONTENT_IS_USING_ARENA()
				IF NOT IS_AUDIO_SCENE_ACTIVE("MP_CELEB_SCREEN_ARENA_SCENE")
					START_AUDIO_SCENE("MP_CELEB_SCREEN_ARENA_SCENE")
				ENDIF
			ELIF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciPLAY_POWERPLAY_INTRO_ANNOUNCER)
				START_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
			ELSE
				START_AUDIO_SCENE("MP_CELEB_SCREEN_PP_SCENE")
			ENDIF
		ENDIF
		
		SET_DRAW_BLACK_RECTANGLE_FOR_FADED_MOCAP(TRUE)

		IF NOT Is_Player_Currently_On_MP_Heist(LocalPlayer)
		OR NOT Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
			IF g_bFMMCLightsTurnedOff
				SET_ARTIFICIAL_LIGHTS_STATE(FALSE)
				g_bFMMCLightsTurnedOff = FALSE
			ENDIF
		ENDIF
//		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciPRE_GAME_EMP)
//			SET_ARTIFICIAL_LIGHTS_STATE(FALSE)
//			g_bFMMCLightsTurnedOff = FALSE
//		ENDIF
		
		SET_NIGHTVISION(FALSE)
		
		THEFEED_PAUSE()
		PRINTLN("[RCC MISSION] - [NETCELEBRATION] - [SAC] - DO_RENDER_PHASE_PAUSE_FOR_CELEBRATION_SCREEN - called THEFEED_FLUSH_QUEUE and THEFEED_PAUSE.")
		
		TOGGLE_RENDERPHASES(FALSE, TRUE)
//		
//		// url:bugstar:2225920 - ST.
//		IF bPlaySoundFx
//			IF Is_Player_Currently_On_MP_Heist(PLAYER_ID())
//				PLAY_SOUND_FRONTEND(-1,  "Mission_Pass_Notify",  "DLC_HEISTS_GENERAL_FRONTEND_SOUNDS", FALSE)
//				PRINTLN("[RCC MISSION] - [NETCELEBRATION] - [SAC] - PLAY_SOUND_FRONTEND(-1,  Mission_Pass_Notify,  DLC_HEISTS_GENERAL_FRONTEND_SOUNDS, FALSE) - Mission Fail")
//			ENDIF
//		ENDIF
//		
		SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggeredRenderPhasePause)
		
	ENDIF
							
ENDPROC

//Stops players leaving a vehicle while a cutscene is about to start
PROC STOP_PLAYER_LEAVING_VEHICLE_THIS_FRAME()

	IF NOT IS_STRING_NULL_OR_EMPTY( sMocapCutscene )
		IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PRI_FIN_MCS2")
		OR ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_TUT_CAR_EXT")
		OR ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PAC_FIN_EXT")
		OR ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PRI_FIN_EXT")
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
				PRINTLN("STOP_PLAYER_LEAVING_VEHICLE_THIS_FRAME - DISABLING VEHICLE EXIT!")
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

//Time to wait on the pre mocap cinematic for end cutscenes
FUNC INT GET_HEIST_PRE_MOCAP_CINE_TIME()
	
	IF MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_PRI_FIN_EXT
	OR MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_PAC_FIN_EXT
		RETURN 100
	ENDIF

	RETURN 750
ENDFUNC

PROC TRIGGER_END_HEIST_WINNER_CELEBRATION(BOOL bDoFreezeAndPostFx)
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[RCC MISSION] [NETCELEBRATION] - [SAC] - [PMC] - TRIGGER_END_HEIST_WINNER_CELEBRATION - being called.")
	DEBUG_PRINTCALLSTACK()
	PRINTLN("[RCC MISSION] [NETCELEBRATION] - [SAC] - [PMC] - bDoFreezeAndPostFx = ", bDoFreezeAndPostFx)
	#ENDIF
	
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
	OR CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
		bDoFreezeAndPostFx = TRUE
		PRINTLN("[RCC MISSION] [NETCELEBRATION] - [SAC] - [PMC] - GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW - bDoFreezeAndPostFx = ", bDoFreezeAndPostFx)
		#IF IS_DEBUG_BUILD
		IF NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bGangOpsTransition
			PRINTLN("[RCC MISSION] [NETCELEBRATION] - [SAC] - [PMC] - g_TransitionSessionNonResetVars.sGlobalCelebrationData.bGangOpsTransition = TRUE")
		ENDIF
		#ENDIF
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.bGangOpsTransition = TRUE
	ENDIF
	
	IF bDoFreezeAndPostFx
		
		PLAY_HEIST_PASS_FX("HeistCelebPass")
		PRINTLN("[RCC MISSION] [NETCELEBRATION] - [SAC] - [PMC] - TRIGGER_END_HEIST_WINNER_CELEBRATION - ANIMPOSTFX_PLAY - HeistCelebPass")
		TOGGLE_RENDERPHASES(FALSE, TRUE)
		
		IF Is_Player_Currently_On_MP_Heist(PLAYER_ID())
			PLAY_SOUND_FRONTEND(-1,"Pre_Screen_Stinger","DLC_HEISTS_FINALE_SCREEN_SOUNDS", FALSE)
			PRINTLN("[SAC] - [RCC MISSION] - [NETCELEBRATION] - [CELEB_AUDIO] - TRIGGER_END_HEIST_WINNER_CELEBRATION - played Pre_Screen_Stinger, DLC_HEISTS_FINALE_SCREEN_SOUNDS")
		ELIF Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
			PLAY_SOUND_FRONTEND(-1,"Pre_Screen_Stinger","DLC_HEISTS_PREP_SCREEN_SOUNDS", FALSE)
			PRINTLN("[SAC] - [RCC MISSION] - [NETCELEBRATION] - [CELEB_AUDIO] - TRIGGER_END_HEIST_WINNER_CELEBRATION - played Pre_Screen_Stinger, DLC_HEISTS_PREP_SCREEN_SOUNDS")
		ENDIF
		
		TOGGLE_PLAYER_DAMAGE_OVERLAY(FALSE)
		REMOVE_MP_HEIST_GEAR(PLAYER_PED_ID(), HEIST_GEAR_NIGHTVISION)
		
		IF GET_REQUESTINGNIGHTVISION()
		OR GET_USINGNIGHTVISION()
			ENABLE_NIGHTVISION(VISUALAID_OFF)
			DISABLE_NIGHT_VISION_CONTROLLER(TRUE) // Added under instruction of Alwyn for B*2196822
		ENDIF
		
	ENDIF
	
	IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
	OR CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
		SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggeredCelebrationStreamingRequests)
	ENDIF
	
	SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerHeistwinnerCutscene)
	
	g_bCelebrationScreenIsActive = TRUE	
	
ENDPROC

FUNC BOOL SHOULD_PLAYER_BE_ADDED_TO_POST_SCENE_HASH_LIST(PLAYER_INDEX playerId)
	
	INT iMyTeam, iOtherPlayerTeam
	INT iMyPostMissionScene = -1
	INT iOtherPlayerPostMissionScene = -1
	PARTICIPANT_INDEX otherPlayerPartId
	INT iOtherPlayerPart = -1
	
	IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE)
		IF IS_NET_PLAYER_OK(playerId, FALSE)
			IF NOT IS_PLAYER_SCTV(playerId)
				IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerId)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(playerId)].bJoinedMissionAsSpectator
					
					PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - SHOULD_PLAYER_BE_ADDED_TO_POST_SCENE_HASH_LIST - players ok.")
					
					otherPlayerPartId = NETWORK_GET_PARTICIPANT_INDEX(playerId)
					iOtherPlayerPart = NATIVE_TO_INT(otherPlayerPartId)
					
					PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - SHOULD_PLAYER_BE_ADDED_TO_POST_SCENE_HASH_LIST - iOtherPlayerPart = ", iOtherPlayerPart)
					
					IF NETWORK_IS_PARTICIPANT_ACTIVE(otherPlayerPartId)
						
						iMyTeam = MC_playerBD[PARTICIPANT_ID_TO_INT()].iteam
						iOtherPlayerTeam = MC_playerBD[iOtherPlayerPart].iteam
						
						iMyPostMissionScene = g_FMMC_STRUCT.iPostMissionSceneId[iMyTeam]
						iOtherPlayerPostMissionScene = g_FMMC_STRUCT.iPostMissionSceneId[iOtherPlayerTeam]
							
						PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - SHOULD_PLAYER_BE_ADDED_TO_POST_SCENE_HASH_LIST - iMyTeam = ", iMyTeam)
						PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - SHOULD_PLAYER_BE_ADDED_TO_POST_SCENE_HASH_LIST - iOtherPlayerTeam = ", iOtherPlayerTeam)
						PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - SHOULD_PLAYER_BE_ADDED_TO_POST_SCENE_HASH_LIST - iMyPostMissionScene = ", iMyPostMissionScene)
						PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - SHOULD_PLAYER_BE_ADDED_TO_POST_SCENE_HASH_LIST - iOtherPlayerPostMissionScene = ", iOtherPlayerPostMissionScene)
						
						IF iMyPostMissionScene >= 0
						AND iOtherPlayerPostMissionScene >= 0
							
							IF iMyPostMissionScene = iOtherPlayerPostMissionScene
								PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - SHOULD_PLAYER_BE_ADDED_TO_POST_SCENE_HASH_LIST - iMyPostMissionScene = iOtherPlayerPostMissionScene, returning TRUE.")
								RETURN TRUE
							ENDIF
							
						ENDIF
						
					ENDIF
				
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC COPY_CELEBRATION_DATA_TO_GLOBALS()
	
	INT iPostMissionTeam
	TEXT_LABEL_63 tl63temp
		
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		#IF IS_DEBUG_BUILD
			PRINTLN("[NETCELEBRATION] - [PMC] - [RCC MISSION] - calling COPY_CELEBRATION_DATA_TO_GLOBALS:")
			DEBUG_PRINTCALLSTACK()
		#ENDIF
		PRINTLN("[NETCELEBRATION] - [PMC] - [RCC MISSION] - exiting because NETWORK_IS_GAME_IN_PROGRESS() = FALSE.")
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(MC_playerBD[PARTICIPANT_ID_TO_INT()].iClientBitset2, PBBOOL2_COMPLETED_POPULATION_OF_CELEB_GLOBALS)
		
		#IF IS_DEBUG_BUILD
		IF IS_THIS_A_QUICK_RESTART_JOB() 
			PRINTLN("[NETCELEBRATION] - [PMC] - [RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - IS_THIS_A_QUICK_RESTART_JOB() = TRUE")
		ELSE
			PRINTLN("[NETCELEBRATION] - [PMC] - [RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - IS_THIS_A_QUICK_RESTART_JOB() = FALSE")
		ENDIF
		IF IS_A_STRAND_MISSION_BEING_INITIALISED()
			PRINTLN("[NETCELEBRATION] - [PMC] - [RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - IS_A_STRAND_MISSION_BEING_INITIALISED() = TRUE")
		ELSE
			PRINTLN("[NETCELEBRATION] - [PMC] - [RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - IS_A_STRAND_MISSION_BEING_INITIALISED() = FALSE")
		ENDIF
		#ENDIF
		
		IF DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(PLAYER_ID())
			//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator = TRUE
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bJoinedMissionAsSpectator = TRUE
			PRINTLN("[NETCELEBRATION] - [PMC] - [RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - DID_PLAYER_JOIN_MISSION_AS_SPECTATOR() = TRUE, setting bJoinedMissionAsSpectator = TRUE.")
		ENDIF
		
		INT i, iPartCount
		INT iMedalList[4]
		PARTICIPANT_INDEX partId, partDataForCelebScreen
		PLAYER_INDEX playerId, playerForCelebScreen
		PED_INDEX pedTemp
		
		IF IS_PLAYER_SCTV(PLAYER_ID())
		OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator
			pedTemp = GET_SPECTATOR_CURRENT_FOCUS_PED()
			IF DOES_ENTITY_EXIST(pedTemp)
				playerForCelebScreen = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedTemp)
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerForCelebScreen)
					partDataForCelebScreen = NETWORK_GET_PARTICIPANT_INDEX(playerForCelebScreen)
				ENDIF
			ENDIF
		ELSE
			pedTemp = PLAYER_PED_ID()
			partDataForCelebScreen = PARTICIPANT_ID()
			playerForCelebScreen = PLAYER_ID()
		ENDIF
		
		// Get RP, and job point values used for calculating celebration data.
		INT iCurrentRP = 0
		IF NETWORK_IS_PARTICIPANT_ACTIVE(partDataForCelebScreen)
			iCurrentRP = GET_PLAYER_FM_XP(NETWORK_GET_PLAYER_INDEX(partDataForCelebScreen)) - sCelebrationStats.iLocalPlayerXP
		ENDIF
		INT iCurrentLvl = GET_FM_RANK_FROM_XP_VALUE(iCurrentRP) //GET_PLAYER_GLOBAL_RANK(NETWORK_GET_PLAYER_INDEX(PARTICIPANT_ID())) //We don't want the current level, but their level before the XP was given.
		INT iNextLvl = iCurrentLvl + 1
		INT iRPToReachCurrentLvl = GET_XP_NEEDED_FOR_FM_RANK(iCurrentLvl)
		INT iRPToReachNextLvl = GET_XP_NEEDED_FOR_FM_RANK(iNextLvl)
		INT iHashCount
		
		// Sort list of players from leaderboard into order based on medal colour awarded. Only do this if not a strand mission and the celebration screen is going to actually play.
		IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
			
			IF NETWORK_IS_PARTICIPANT_ACTIVE(partDataForCelebScreen)
			
				iPostMissionTeam = MC_playerBD[NATIVE_TO_INT(partDataForCelebScreen)].iteam
				
				IF g_FMMC_STRUCT.iPostMissionSceneId[iPostMissionTeam] = SIS_POST_NONE
					#IF IS_DEBUG_BUILD
					PRINTLN("[RCC MISSION] - [SAC] - COPY_CELEBRATION_DATA_TO_GLOBALS - iPostMissionSceneId = SIS_POST_NONE.")
					#ENDIF
				ENDIF
				
				IF g_FMMC_STRUCT.iPostMissionSceneId[iPostMissionTeam] < SIS_POST_NONE
					#IF IS_DEBUG_BUILD
					SCRIPT_ASSERT("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - iPostMissionSceneId < SIS_POST_NONE. Has the option been set in the creator or a cloud error?")
					PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - iPostMissionSceneId < SIS_POST_NONE. Defaulting to SIS_POST_SERIES_A_COKE.")
					#ENDIF
					g_FMMC_STRUCT.iPostMissionSceneId[iPostMissionTeam] = SIS_POST_SERIES_A_COKE 
				ENDIF
				
		//		IF g_FMMC_STRUCT.iPostMissionSceneId[iPostMissionTeam]  = SIS_POST_APARTMENT
		//			#IF IS_DEBUG_BUILD
		//			SCRIPT_ASSERT("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - iPostMissionSceneId = SIS_POST_APARTMENT.")
		//			PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - iPostMissionSceneId = SIS_POST_APARTMENT. Defaulting to SIS_POST_SERIES_A_COKE.")
		//			#ENDIF
		//			g_FMMC_STRUCT.iPostMissionSceneId[iPostMissionTeam]  = SIS_POST_SERIES_A_COKE 
		//		ENDIF
				
		//		IF g_FMMC_STRUCT.iPostMissionSceneId = SIS_POST_STRIP_CLUB
		//			#IF IS_DEBUG_BUILD
		//			SCRIPT_ASSERT("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - iPostMissionSceneId = SIS_POST_STRIP_CLUB.")
		//			PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - iPostMissionSceneId = SIS_POST_STRIP_CLUB. Defaulting to SIS_POST_SERIES_A_COKE.")
		//			#ENDIF
		//			g_FMMC_STRUCT.iPostMissionSceneId = SIS_POST_SERIES_A_COKE 
		//		ENDIF
				
				IF g_FMMC_STRUCT.iPostMissionSceneId[iPostMissionTeam] >= TOTAL_SYNCED_INTERACTION_SCENES
					#IF IS_DEBUG_BUILD
					SCRIPT_ASSERT("Celebrations - iPostMissionSceneId >= TOTAL_SYNCED_INTERACTION_SCENES. Invaliud value. Has the option been set in the creator or a cloud error?")
					PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - iPostMissionSceneId >= TOTAL_SYNCED_INTERACTION_SCENES. Defaulting to SIS_POST_SERIES_A_COKE.")
					#ENDIF
					g_FMMC_STRUCT.iPostMissionSceneId[iPostMissionTeam] = SIS_POST_SERIES_A_COKE 
				ENDIF
				
			ELSE
				
				#IF IS_DEBUG_BUILD
				SCRIPT_ASSERT("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - partDataForCelebScreen is not a valid participant. Did they stack overflow or array overrun and abruptly leave the script?")
				PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - Defaulting to SIS_POST_SERIES_A_COKE.")
				#ENDIF
				g_FMMC_STRUCT.iPostMissionSceneId[iPostMissionTeam] = SIS_POST_SERIES_A_COKE 
				
			ENDIF
			
	//		g_FMMC_STRUCT.iPostMissionSceneId[iPostMissionTeam]  = SIS_POST_STRIP_CLUB
			
	//		g_FMMC_STRUCT.iPostMissionSceneId = SIS_POST_STRIP_CLUB
				
			REPEAT 4 i
				iMedalList[i] = -1
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.strLeaderBoardPlayerName[i] = tl63temp
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.eRatingColour[i] = HUD_COLOUR_WHITE
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.strRatingColour[i] = tl63temp
				PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - iMedalList[", i, "] = -1.")
				PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - strLeaderBoardPlayerName[", i, "] = ", tl63temp)
				PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - eRatingColour[", i, "] = HUD_COLOUR_WHITE")
				PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - strRatingColour[", i, "] = ", tl63temp)
			ENDREPEAT
			
			#IF IS_DEBUG_BUILD
			PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - printing participant data.")
			REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iPartCount
				partId = INT_TO_NATIVE(PARTICIPANT_INDEX, iPartCount)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(partId)
					PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - part ", iPartCount, " is active.")
					playerId = NETWORK_GET_PLAYER_INDEX(partId)
					IF IS_NET_PLAYER_OK(playerId, FALSE)
						PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - part ", iPartCount, " is net player ok.")
						IF NOT IS_PLAYER_SCTV(playerId)
							PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - part ", iPartCount, " is not SCTV.")
						ELSE
							PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - part ", iPartCount, " is SCTV.")
						ENDIF
						IF NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(playerId)
							PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - part ", iPartCount, " is not DID_PLAYER_JOIN_MISSION_AS_SPECTATOR.")
						ELSE
							PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - part ", iPartCount, " is DID_PLAYER_JOIN_MISSION_AS_SPECTATOR.")
						ENDIF
					ELSE
						PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - part ", iPartCount, " is not net player ok.")
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - part ", iPartCount, " is not active.")
				ENDIF
			ENDREPEAT
			#ENDIF
			
			REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iPartCount
				SWITCH GET_HEIST_RATING_HUD_COLOUR(iPartCount)
					CASE HUD_COLOUR_PLATINUM
						PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - medals - part ", iPartCount, " medal rating is platinum.")
						IF iMedalList[0] = (-1)
							iMedalList[0] = iPartCount
							PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - medals - iMedalList[0] = GOLD. Setting to iPartCount = ", iMedalList[0])
						ENDIF
					BREAK
					CASE HUD_COLOUR_GOLD
						PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - medals - part ", iPartCount, " medal rating is gold.")
						IF iMedalList[1] = (-1)
							iMedalList[1] = iPartCount
							PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - medals - iMedalList[1] = SILVER. Setting to iPartCount = ", iMedalList[1])
						ENDIF
					BREAK
					CASE HUD_COLOUR_SILVER
						PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - medals - part ", iPartCount, " medal rating is silver.")
						IF iMedalList[2] = (-1)
							iMedalList[2] = iPartCount
							PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - medals - iMedalList[2] = BRONZE. Setting to iPartCount = ", iMedalList[2])
						ENDIF
					BREAK
					CASE HUD_COLOUR_BRONZE
						PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - medals - part ", iPartCount, " medal rating is bronze.")
						IF iMedalList[3] = (-1)
							iMedalList[3] = iPartCount
							PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - medals - iMedalList[3] = NONE. Setting to iPartCount = ", iMedalList[3])
						ENDIF
					BREAK
					CASE HUD_COLOUR_WHITE
						PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - medals - part ", iPartCount, " medal rating is none (white). Not adding to medal list.")
					BREAK
					CASE HUD_COLOUR_BLACK
						PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - medals - part ", iPartCount, " medal rating is invalid. Not adding to medal list.")
					BREAK
				ENDSWITCH
			ENDREPEAT
			
			REPEAT 4 i
				IF (iMedalList[i] != -1)
					REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iPartCount
						IF (iMedalList[i] = iPartCount)
							partId = INT_TO_NATIVE(PARTICIPANT_INDEX, iPartCount)
							IF NETWORK_IS_PARTICIPANT_ACTIVE(partId)
								playerId = NETWORK_GET_PLAYER_INDEX(partId)
								IF IS_NET_PLAYER_OK(playerId, FALSE)
									IF NOT IS_PLAYER_SCTV(playerId)
										IF NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(playerId)
											g_TransitionSessionNonResetVars.sGlobalCelebrationData.strLeaderBoardPlayerName[i] = MC_serverBD_2.tParticipantNames[iPartCount]
											g_TransitionSessionNonResetVars.sGlobalCelebrationData.eRatingColour[i] = GET_HEIST_RATING_HUD_COLOUR(iPartCount)
											g_TransitionSessionNonResetVars.sGlobalCelebrationData.strRatingColour[i] = GET_HEIST_COMPLETION_RATING_STRING(iPartCount, g_TransitionSessionNonResetVars.sGlobalCelebrationData.eRatingColour[i], TRUE)
											PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - iMedalList[", i, "] = strLeaderBoardPlayerName = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.strLeaderBoardPlayerName[i], ", iHashCount = ", iHashCount)
											PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - iMedalList[", i, "] = strRatingColour = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.strRatingColour[i])
							
											IF SHOULD_PLAYER_BE_ADDED_TO_POST_SCENE_HASH_LIST(playerId)
												g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPlayerNameHash[iHashCount] = NETWORK_HASH_FROM_PLAYER_HANDLE(playerId) //GET_HASH_KEY(strNameForHash)
												iHashCount++
												PRINTLN("[RCC MISSION] - [SAC] - COPY_CELEBRATION_DATA_TO_GLOBALS - iMedalList[", i, "] = strLeaderBoardPlayerName = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.strLeaderBoardPlayerName[i])
												PRINTLN("[RCC MISSION] - [SAC] - COPY_CELEBRATION_DATA_TO_GLOBALS - iPlayerNameHash[", i, "] = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPlayerNameHash[i])
											ELSE
												PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - iMedalList[", i, "] = SHOULD_PLAYER_BE_ADDED_TO_POST_SCENE_HASH_LIST = FALSE.")
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
				ELSE
					PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - iMedalList[", i, "] = ", iMedalList[i])
				ENDIF
			ENDREPEAT
			
		ENDIF
		
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId = g_FMMC_STRUCT.iPostMissionSceneId[iPostMissionTeam]
		
//		g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId = SIS_POST_APARTMENT
		
		PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - iPostMissionSceneId saved as ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId)
		
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.iWinnerSceneType = g_FMMC_STRUCT.iCelebrationType
		
		IF NOT HAS_TEAM_PASSED_MISSION(iPostMissionTeam)
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_POST_MISSION_SPAWN_ON_FAIL)
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPos = << 0.0, 0.0, 0.0 >> 
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.fPostMissionPosSearchRadius = 0.0
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPointOfInterest = << 0.0, 0.0, 0.0 >> 
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPostMissionPosIgnoreExclusionZones = FALSE
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.bForcePostMissionWarpCreatorPosition = FALSE
				PRINTLN("[RCC MISSION] - [SAC] - [NETCELEBRATION] - COPY_CELEBRATION_DATA_TO_GLOBALS - resetting vPostMissionPos. I failed the mission.")
			ENDIF
		ENDIF
		
		IF NOT IS_NET_PLAYER_OK(playerForCelebScreen, FALSE)
			pedTemp = PLAYER_PED_ID()
			partDataForCelebScreen = PARTICIPANT_ID()
			playerForCelebScreen = PLAYER_ID()
			PRINTLN("[RCC MISSION] - [SAC] - [NETCELEBRATION] - COPY_CELEBRATION_DATA_TO_GLOBALS - playerForCelebScreen not net ok, setting to PLAYER_ID().")
		ENDIF
		
		IF IS_NET_PLAYER_OK(playerForCelebScreen, FALSE)
			IF Is_Player_Currently_On_MP_Heist_Planning(playerForCelebScreen)
			OR (GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
			AND NOT GANGOPS_FLOW_IS_CURRENT_MISSION_A_GANGOPS_FINALE())
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.iHeistType = ciCELEBRATION_HEIST_TYPE__PREP
			ELIF Is_Player_Currently_On_MP_Heist(playerForCelebScreen)
			OR GANGOPS_FLOW_IS_CURRENT_MISSION_A_GANGOPS_FINALE()
			OR CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.iHeistType = ciCELEBRATION_HEIST_TYPE__FINALE
			ENDIF
			PRINTLN("[RCC MISSION] - [SAC] - [NETCELEBRATION] - COPY_CELEBRATION_DATA_TO_GLOBALS - iHeistType = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iHeistType)
		ELSE
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iHeistType = ciCELEBRATION_HEIST_TYPE__PREP
			PRINTLN("[RCC MISSION] - [SAC] - [NETCELEBRATION] - COPY_CELEBRATION_DATA_TO_GLOBALS - playerForCelebScreen not net ok, iHeistType = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iHeistType)
		ENDIF
		
		IF NOT IS_PLAYER_SCTV(PLAYER_ID())
		AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iRpGained = sCelebrationStats.iLocalPlayerXP
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iRpStarted = iCurrentRP
		 	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iCurrentLevelStartPoints = iRPToReachCurrentLvl
		 	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iCurrentLevelEndPoints = iRPToReachNextLvl
		 	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iCurrentLevel = iCurrentLvl
		 	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNextLevel = iNextLvl
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMyTake = iActualTake
			PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMyTake = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMyTake)
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumJobPoints = sCelebrationStats.iLocalPlayerJobPoints
		ENDIF
		
		// Save local participant mission stats.
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.bPassedMission = HAS_TEAM_PASSED_MISSION(iPostMissionTeam)
		IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bIAmHeistLeader = (GB_GET_LOCAL_PLAYER_GANG_BOSS() = LocalPlayer)
		ELSE
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bIAmHeistLeader = IS_BIT_SET(MC_playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PBBOOL_HEIST_HOST)
		ENDIF
		
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.strFailReason = sCelebrationStats.strFailReason
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.strFailReasonLiteral = sCelebrationStats.strFailLiteral
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.strFailReasonLiteral2 = sCelebrationStats.strFailLiteral2
		
		IF IS_THIS_IS_A_STRAND_MISSION()
		AND NOT IS_STRING_NULL_OR_EMPTY(g_sTransitionSessionData.sStrandMissionData.tl63FirstMissionName)
			PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - g_TransitionSessionNonResetVars.sGlobalCelebrationData.tl63_missionName = g_sTransitionSessionData.sStrandMissionData.tl63FirstMissionName")
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.tl63_missionName = g_sTransitionSessionData.sStrandMissionData.tl63FirstMissionName
		ELSE
			PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - g_TransitionSessionNonResetVars.sGlobalCelebrationData.tl63_missionName = g_FMMC_STRUCT.tl63MissionName")
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.tl63_missionName = g_FMMC_STRUCT.tl63MissionName
		ENDIF
		
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.bFirstUpdate = FALSE
		
		/* If we have performed an update, clear the flag saying we're doing the first update. 
		This will get cleared in the post mission cleanup. The post mission clenaup does not run between strand missions, preventing it being cleared early. */
		SET_CELEBRATION_GLOBALS_FIRST_UPDATE(FALSE)
		
		PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - iWinnerSceneType 			= ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iWinnerSceneType)
		
		PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - bPassedMission 				= ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bPassedMission)
		PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - bJoinedMissionAsSpectator 	= ", IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator))
		PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - strFailReason 				= ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.strFailReason)
		PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - strFailReasonLiteral 		= ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.strFailReasonLiteral)
		PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - strFailReasonLiteral2 		= ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.strFailReasonLiteral2)
		PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - iNumJobPoints 				= ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumJobPoints)
	 	PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - iRpGained 					= ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iRpGained)
	 	PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - iRpStarted 					= ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iRpStarted)
	 	PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - iCurrentLevelStartPoints 	= ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iCurrentLevelStartPoints)
	 	PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - iCurrentLevelEndPoints 		= ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iCurrentLevelEndPoints)
	 	PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - iCurrentLevel 				= ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iCurrentLevel)
	 	PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - iNextLeve 					= ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNextLevel)
		PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - bIAmHeistLeader 			= ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bIAmHeistLeader)
		PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - iMaximumTake 				= ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMaximumTake)
		PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - iMyTake 					= ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMyTake)
		PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - tl63_missionName 			= ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.tl63_missionName)
		
		IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()	
			missionEndShardData.bRunOnReturnToFreemode 							= TRUE
			missionEndShardData.eType 											= MISSION_END_SHARD_TYPE_CASINO_HEIST
			missionEndShardData.bPass 											= g_TransitionSessionNonResetVars.sGlobalCelebrationData.bPassedMission
			missionEndShardData.iEliteChallengeCash								= g_TransitionSessionNonResetVars.sGlobalCelebrationData.iEliteChallengeBonusValue
			
			IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bEliteChallengeComplete
				missionEndShardData.iEliteChallenge 					= CASINO_HEIST_END_ELITE_COMPLETE
			ELIF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentDidQuickRestart
				missionEndShardData.iEliteChallenge 					= CASINO_HEIST_END_ELITE_RESTART
			ELSE
				missionEndShardData.iEliteChallenge 					= CASINO_HEIST_END_ELITE_NO_TRIED
			ENDIF
			
			PRINTLN("[RCC MISSION][HeistPassedShard] - COPY_CELEBRATION_DATA_TO_GLOBALS - missionEndShardData.bPass			 						= ", missionEndShardData.bPass 			)
			PRINTLN("[RCC MISSION][HeistPassedShard] - COPY_CELEBRATION_DATA_TO_GLOBALS - missionEndShardData.eType			 						= ", missionEndShardData.eType) 
			PRINTLN("[RCC MISSION][HeistPassedShard] - COPY_CELEBRATION_DATA_TO_GLOBALS - missionEndShardData.iApproachID	 		 				= ", missionEndShardData.iApproachID		)
			PRINTLN("[RCC MISSION][HeistPassedShard] - COPY_CELEBRATION_DATA_TO_GLOBALS - missionEndShardData.iTargetID 			 				= ", missionEndShardData.iTargetID 		)
			PRINTLN("[RCC MISSION][HeistPassedShard] - COPY_CELEBRATION_DATA_TO_GLOBALS - missionEndShardData.iTake[MISSION_END_SHARD_CH_POT_TAKE]   		= ", missionEndShardData.iTake[MISSION_END_SHARD_CH_POT_TAKE] 	)
			PRINTLN("[RCC MISSION][HeistPassedShard] - COPY_CELEBRATION_DATA_TO_GLOBALS - missionEndShardData.iTake[MISSION_END_SHARD_CH_TAKE] 			 	= ", missionEndShardData.iTake[MISSION_END_SHARD_CH_TAKE]			)
			PRINTLN("[RCC MISSION][HeistPassedShard] - COPY_CELEBRATION_DATA_TO_GLOBALS - missionEndShardData.npcTake[MISSION_END_SHARD_NPC_WEAPON].iNPCID 	= ", missionEndShardData.npcTake[MISSION_END_SHARD_NPC_WEAPON].iNPCID 			)
			PRINTLN("[RCC MISSION][HeistPassedShard] - COPY_CELEBRATION_DATA_TO_GLOBALS - missionEndShardData.npcTake[MISSION_END_SHARD_NPC_HACKER].iNPCID 	= ", missionEndShardData.npcTake[MISSION_END_SHARD_NPC_HACKER].iNPCID			)
			PRINTLN("[RCC MISSION][HeistPassedShard] - COPY_CELEBRATION_DATA_TO_GLOBALS - missionEndShardData.npcTake[MISSION_END_SHARD_NPC_DRIVER].iNPCID 	= ", missionEndShardData.npcTake[MISSION_END_SHARD_NPC_DRIVER].iNPCID 			)
			PRINTLN("[RCC MISSION][HeistPassedShard] - COPY_CELEBRATION_DATA_TO_GLOBALS - missionEndShardData.iEliteChallenge  				= ", missionEndShardData.iEliteChallenge )
			PRINTLN("[RCC MISSION][HeistPassedShard] - COPY_CELEBRATION_DATA_TO_GLOBALS - missionEndShardData.iEliteChallengeCash			= ", missionEndShardData.iEliteChallengeCash )
		ENDIF
		
		//Send events to populate for spectator
		PRINTLN("[RCC MISSION] - [SAC] - [PMC] - ***** Broadcasting celebration data to people sepctating me START *****")
		REPEAT NUM_NETWORK_PLAYERS i
			PRINTLN("[RCC MISSION] - [SAC] - [PMC] - checking player ", i)
			IF IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, i), FALSE)
				#IF IS_DEBUG_BUILD
				INT iTemp = NATIVE_TO_INT(GlobalplayerBD_FM[i].playerSCTVTarget)
				INT iTemp2 = NATIVE_TO_INT(PLAYER_ID())
				PRINTLN("[RCC MISSION] - [SAC] - [PMC] - player ", i, " is net ok.")
				PRINTLN("[RCC MISSION] - [SAC] - [PMC] - player ", i, " spectator target = ", iTemp, ". I am player ", iTemp2)
				#ENDIF
				IF GlobalplayerBD_FM[i].playerSCTVTarget = PLAYER_ID()
					BROADCAST_SYNC_SPECTATOR_CELEBRATION_GLOBALS_1(SPECIFIC_PLAYER(INT_TO_NATIVE(PLAYER_INDEX, i)))
					BROADCAST_SYNC_SPECTATOR_CELEBRATION_GLOBALS_2(SPECIFIC_PLAYER(INT_TO_NATIVE(PLAYER_INDEX, i)))
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION] - [SAC] - [PMC] - player ", i, " is not net ok.")
			ENDIF
		ENDREPEAT
		PRINTLN("[RCC MISSION] - [SAC] - [PMC] - ***** Broadcasting celebration data to people sepctating me END *****")
		
		SET_BIT(MC_playerBD[PARTICIPANT_ID_TO_INT()].iClientBitset2, PBBOOL2_COMPLETED_POPULATION_OF_CELEB_GLOBALS)
		
	ENDIF
	
ENDPROC

/*ENUM ePRE_SWOOP_STAGE
	ePRESWOOPSTAGE_FREEZE_SCREEN = 0,
	ePRESWOOPSTAGE_DO_SWOOP,
	ePRESWOOPSTAGE_FINISHED
ENDENUM
ePRE_SWOOP_STAGE ePreSwoopStage

FUNC BOOL HAS_COMPLETED_PRE_CELEBRATION_SWOOP()
	
	IF IS_A_STRAND_MISSION_BEING_INITIALISED()
		RETURN TRUE
	ENDIF
	
	IF NOT Is_Player_Currently_On_MP_Contact_Mission(LocalPlayer)
		RETURN TRUE
	ENDIF
	
	SWITCH ePreSwoopStage
	
		CASE ePRESWOOPSTAGE_FREEZE_SCREEN
			ePreSwoopStage = ePRESWOOPSTAGE_DO_SWOOP
		BREAK
		
		CASE ePRESWOOPSTAGE_DO_SWOOP
			IF SET_SKYSWOOP_UP(FALSE, TRUE, TRUE, DEFAULT, FALSE, DEFAULT, DEFAULT, DEFAULT, TRUE)
				ePreSwoopStage = ePRESWOOPSTAGE_FINISHED
			ENDIF
		BREAK
		
		CASE ePRESWOOPSTAGE_FINISHED
			RETURN TRUE
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC*/

FUNC BOOL HAS_CELEBRATION_SCREEN_SKIP_FINISHED()
	
	IF IS_A_STRAND_MISSION_BEING_INITIALISED()
	OR IS_FAKE_MULTIPLAYER_MODE_SET()
		PRINTLN("[RCC] - HAS_CELEBRATION_SCREEN_SKIP_FINISHED - IS_THIS_A_STRAND_MISSION = TRUE, returning TRUE.")
		RETURN TRUE
	ENDIF
	
	IF IS_SCREEN_FADED_IN()
		IF SET_SKYSWOOP_UP(TRUE, TRUE, FALSE, DEFAULT, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
			PRINTLN("[RCC] - HAS_CELEBRATION_SCREEN_SKIP_FINISHED - SET_SKYSWOOP_UP done, returning TRUE.")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_SCREEN_FADED_IN()
		IF IS_SKYSWOOP_IN_SKY()
			PRINTLN("[RCC] - HAS_CELEBRATION_SCREEN_SKIP_FINISHED - IS_SKYSWOOP_IN_SKY says yes, returning TRUE.")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_ANYONE_STILL_AT_KEYCODES_CARPARK()
	BOOL bAnyoneAtCarPark = FALSE
	
	VECTOR vCarPark = << 145.569, -1072.38, 28.192 >>
	FLOAT fRadius2 = 372109.755 // ( 305 * 2 )^2
	
	PARTICIPANT_INDEX tempPart
	PLAYER_INDEX tempPlayer
	PED_INDEX player
	
	INT iParticipant
	INT iPlayersChecked
	INT iNumPlayingPlayers = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]
	
	FLOAT fDistance
	
	FOR iParticipant = 0 TO MAX_NUM_MC_PLAYERS -1 
		tempPart = INT_TO_PARTICIPANTINDEX( iParticipant )
		IF NETWORK_IS_PARTICIPANT_ACTIVE( tempPart )
		AND NOT IS_PARTICIPANT_A_NON_HEIST_SPECTATOR( iParticipant )
			tempPlayer = NETWORK_GET_PLAYER_INDEX( tempPart )
			IF IS_NET_PLAYER_OK( tempPlayer )
				player = GET_PLAYER_PED( tempPlayer )
				
				fDistance = VDIST2( GET_ENTITY_COORDS( player, FALSE ), vCarPark )
				
				IF fDistance <= fRadius2
					bAnyoneAtCarPark = TRUE
					iParticipant = MAX_NUM_MC_PLAYERS
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Participant ", iParticipant ," is still at the carpark ")
				ENDIF
				
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Participant ", iParticipant ," is ", fDistance," away")

			ENDIF
			++iPlayersChecked
			
			IF iPlayersChecked >= iNumPlayingPlayers
				iParticipant = MAX_NUM_MC_PLAYERS
			ENDIF
		ENDIF
	ENDFOR

	RETURN bAnyoneAtCarPark

ENDFUNC

PROC CHECK_EARLY_CELEBRATION_SCREEN( BOOL bEndOfMission, STRING sMocapName )
	INT iMyTeam = MC_playerBD[ iLocalPart ].iTeam 

	IF IS_CUTSCENE_PLAYING()
	AND NOT bEndOfMission
	AND HAS_TEAM_PASSED_MISSION( iMyTeam )
		IF GET_CUTSCENE_END_TIME() - GET_CUTSCENE_TIME() < 750 // As close to the end as I dare
			IF NOT IS_BIT_SET( iLocalBoolCheck8, LBOOL8_SHOULD_CELEBRATION_BE_SHOWN_PREMATURELY )
				IF ARE_STRINGS_EQUAL( sMocapName, "MPH_HUM_KEY_EXT" )
					IF NOT IS_ANYONE_STILL_AT_KEYCODES_CARPARK()
						CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Keycodes cutscene - no-one is at the carpark" )
						SET_BIT( iLocalBoolCheck8, LBOOL8_SHOULD_CELEBRATION_BE_SHOWN_PREMATURELY )
						
						INT iLoopRules
						INT iPointsToAward
					
						FOR iLoopRules = MC_serverBD_4.iCurrentHighestPriority[ iMyTeam ] TO MC_serverBD.iMaxObjectives[ iMyTeam ]
							iPointsToAward += GET_FMMC_POINTS_FOR_TEAM( iMyTeam, iLoopRules )
						ENDFOR
						
						CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Broadcasting an additional ",iPointsToAward, " points to everyone, hopefully this makes us all win!" )
						
						BROADCAST_TEAM_HAS_EARLY_CELEBRATION( iMyTeam, iPointsToAward )
						
						END_MISSION_TIMERS()
						iEarlyCelebrationTeamPoints[ iMyTeam ] = iPointsToAward
						
						g_bEndOfMissionCleanUpHUD = TRUE
						
						POPULATE_RESULTS()
					ELSE
						CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Keycodes cutscene - someone's still at the carpark" )	
					ENDIF
				ELSE
					sEarlyCelebrationData sData
					sData = SHOULD_CELEBRATION_SCREEN_BE_SHOWN_PREMATURELY()
					
					IF sData.bShouldShowPrematurely
						BROADCAST_TEAM_HAS_EARLY_CELEBRATION( sData.iTeam, sData.iTeamWinningPoints )
						
						END_MISSION_TIMERS()
						iEarlyCelebrationTeamPoints[ sData.iTeam ] = sData.iTeamWinningPoints
						
						SET_BIT( iLocalBooLCheck8, LBOOL8_SHOULD_CELEBRATION_BE_SHOWN_PREMATURELY )
						
						CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] We have an early celebration screen" )
						
						g_bEndOfMissionCleanUpHUD = TRUE
						
						POPULATE_RESULTS()
					ELSE
						CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Celebration screen should not be shown early." )
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC START_CELEBRATION_SCREEN( BOOL bShowCelebrationScreen )
	
	//1630878 - Start the celebration screen just before the cutscene ends
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		IF bShowCelebrationScreen
		OR ( IS_BIT_SET( iLocalBoolCheck8, LBOOL8_SHOULD_CELEBRATION_BE_SHOWN_PREMATURELY ) AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED() )
			IF IS_CUTSCENE_PLAYING()
			
				CELEBRATION_SCREEN_TYPE eCelebType = CELEBRATION_STANDARD
			
				IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
				OR Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
				OR CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
					
					eCelebType = CELEBRATION_HEIST
					PRINTLN("[RCC MISSION] - [NETCELEBRATION] - USING HEIST CELEBRATION SCREEN 4")	
					
				ELIF IS_LOADED_MISSION_TYPE_FLOW_MISSION()
				AND (IS_LOADED_MISSION_LOW_FLOW_MISSION(ciLOW_FLOW_MISSION_LOWRIDER_FINALE)
				OR IS_LOADED_MISSION_LOW_FLOW_MISSION(ciLOW_FLOW_MISSION_LOWRIDER_ASSASSINATION)
				OR IS_LOADED_MISSION_LOW_FLOW_MISSION(ciLOW_FLOW_MISSION_PHOTO_CARS)
				OR IS_LOADED_MISSION_LOW_FLOW_MISSION(ciLOW_FLOW_MISSION_BLOW_UP_LOWRIDERS)
				OR IS_LOADED_MISSION_LOW_FLOW_MISSION(ciLOW_FLOW_MISSION_FUNERAL_SHOOTOUT)
				OR IS_LOADED_MISSION_LOW_FLOW_MISSION(ciLOW_FLOW_MISSION_LOWRIDERS_RESCUE)
				OR IS_LOADED_MISSION_LOW_FLOW_MISSION(ciLOW_FLOW_MISSION_TRANSPORT_LOWRIDERS)
				OR IS_LOADED_MISSION_LOW_FLOW_MISSION(ciLOW_FLOW_MISSION_DRIVE_BY_SHOOTOUT))
					
					eCelebType = CELEBRATION_HEIST
					PRINTLN("[RCC MISSION] - [NETCELEBRATION] - USING FLOW MISSION (HEIST) CELEBRATION SCREEN 4")
					

				ELIF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()				
					eCelebType = CELEBRATION_GANGOPS
					PRINTLN("[RCC MISSION] - [NETCELEBRATION] - USING GANGOPS CELEBRATION SCREEN 4")
				ELSE			
					eCelebType = CELEBRATION_STANDARD
					PRINTLN("[RCC MISSION] - [NETCELEBRATION] - USING STANDARD CELEBRATION SCREEN 4")
				
				ENDIF
				
				REQUEST_CELEBRATION_SCREEN(sCelebrationData, eCelebType)
				
				SWITCH eCelebType
					
					DEFAULT
					CASE CELEBRATION_STANDARD
						IF GET_CUTSCENE_END_TIME() - GET_CUTSCENE_TIME() < 450
							SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bStartedCelebrationBeforeCutsceneEnded)
							IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration)
								DO_RENDER_PHASE_PAUSE_FOR_CELEBRATION_SCREEN()
								SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration)
								PRINTLN("[RCC MISSION] - [NETCELEBRATION] - [SAC] - ciCELEBRATION_BIT_SET_TriggerEndCelebration - call 0.")
							ENDIF
						ENDIF
					BREAK
					
					CASE CELEBRATION_HEIST
					CASE CELEBRATION_GANGOPS
						IF GET_CUTSCENE_END_TIME() - GET_CUTSCENE_TIME() < 100
							SET_DRAW_BLACK_RECTANGLE_FOR_FADED_MOCAP(TRUE)
						ENDIF
						IF GET_CUTSCENE_END_TIME() - GET_CUTSCENE_TIME() < 450
							IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerHeistwinnerCutscene)
								TRIGGER_END_HEIST_WINNER_CELEBRATION((NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoFacePlayerCamCut))
								PRINTLN("[RCC MISSION] - [NETCELEBRATION] - [SAC] - TRIGGER_END_HEIST_WINNER_CELEBRATION - call A.")
							ENDIF
						ENDIF
					BREAK
					
				ENDSWITCH

			ENDIF
		ENDIF
	ENDIF
ENDPROC
