
USING "FM_Mission_Controller_USING.sch"
USING "FM_Mission_Controller_Utility.sch"
USING "FM_Mission_Controller_Entities.sch"
USING "FM_Mission_Controller_ObjectiveCompletion.sch"
USING "FM_Mission_Controller_SpecialCase.sch"
USING "FM_Mission_Controller_Rules.sch"
USING "FM_Mission_Controller_Doors.sch"
USING "FM_Mission_Controller_Cutscene.sch"
USING "FM_Mission_Controller_Debug.sch"
USING "FM_Mission_Controller_Locations.sch"
USING "FM_Mission_Controller_Vehicles.sch"
USING "FM_Mission_Controller_Peds_Brain.sch"



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: PED ANIMATION TASKING !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************

FUNC STRING GET_PED_SYNC_SCENE_STATE_NAME(PED_SYNC_SCENE_STATE ePedSyncSceneState)
	SWITCH ePedSyncSceneState
		CASE	PED_SYNC_SCENE_STATE_INIT					RETURN "STATE_INIT"
		CASE	PED_SYNC_SCENE_STATE_PROCESS_START			RETURN "STATE_PROCESS_START"
		CASE	PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT		RETURN "STATE_PROCESS_BREAKOUT"
		CASE	PED_SYNC_SCENE_STATE_CLEANUP				RETURN "STATE_CLEANUP"
		CASE	PED_SYNC_SCENE_STATE_PROCESS_END			RETURN "STATE_PROCESS_END"		
	ENDSWITCH
	
	RETURN "Invalid"
ENDFUNC

PROC SET_PED_SYNC_SCENE_STATE(INT iPed, PED_SYNC_SCENE_STATE eNewPedSyncSceneState)
	IF sMissionPedsLocalVars[iPed].ePedSyncSceneState != eNewPedSyncSceneState
		PRINTLN("[LM][SET_PED_SYNC_SCENE_STATE][Sync_anims: ", iPed, "] - Moving from old state : ", GET_PED_SYNC_SCENE_STATE_NAME(sMissionPedsLocalVars[iPed].ePedSyncSceneState), " >>>> New State: ", GET_PED_SYNC_SCENE_STATE_NAME(eNewPedSyncSceneState))
		sMissionPedsLocalVars[iPed].ePedSyncSceneState = eNewPedSyncSceneState
	ENDIF
ENDPROC

FUNC PED_INDEX GET_CLOSEST_VISIBLE_PLAYER(INT iPed, PED_INDEX tempPed, BOOL bIgnorePerceptionCheck = FALSE, FLOAT maxDistance = 30.0, BOOL bIgnoreLOSCheck = FALSE)

	INT iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY(tempPed)
	PLAYER_INDEX ClosestPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
	 
	IF IS_NET_PLAYER_OK(ClosestPlayer)
		
		PED_INDEX ClosestPlayerPed = GET_PLAYER_PED(ClosestPlayer)
		
		IF GET_DISTANCE_BETWEEN_ENTITIES(tempPed,ClosestPlayerPed) < maxDistance
		AND (bIgnorePerceptionCheck OR
			IS_TARGET_PED_IN_PERCEPTION_AREA(tempPed,ClosestPlayerPed)) //added a perception check so peds don't head-track the player while sneaking up for a stealth kill
		AND (bIgnoreLOSCheck OR
			HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(iPed, tempPed,ClosestPlayerPed,SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE))
			RETURN ClosestPlayerPed
		ENDIF
	ENDIF
	RETURN NULL
ENDFUNC

PROC TASK_PERFORM_LOOK_AT_CLOSEST_PLAYER(PED_INDEX tempPed, INT iped, BOOL bIgnorePerceptionCheck = FALSE)
	
	IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_LOOK_AT_ENTITY)
		PED_INDEX ClosestPlayerPed = GET_CLOSEST_VISIBLE_PLAYER(iPed, tempPed, bIgnorePerceptionCheck)
		IF ClosestPlayerPed != NULL
			TASK_LOOK_AT_ENTITY(tempPed, ClosestPlayerPed,10000,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_PITCH_LIMIT | SLF_WIDEST_YAW_LIMIT,SLF_LOOKAT_HIGH)
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_TREVOR_BE_GIVEN_VAN_LEAN_OUT_IDLE(PED_INDEX tempPed, INT iped)
	
	BOOL bReadyForIdle = TRUE
	
	IF NOT SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
		
		STRING strAnimDict = "anim@heists@narcotics@finale@ig_5_trevor_in_truck"
		STRING strAnimName
		
		INT iLoop
		FLOAT fPhase
		
		FOR iLoop = 0 TO 4
			
			SWITCH iLoop
				CASE 0
					strAnimName = "whos_driving_me"
					fPhase = 0.9
				BREAK
				CASE 1
					strAnimName = "we_gotta_go"
					fPhase = 0.8
				BREAK
				CASE 2
					strAnimName = "ceo_dont_drive_himself"
					fPhase = 0.82//0.97
				BREAK
				CASE 3
					strAnimName = "dont_keep_me_waiting"
					fPhase = 0.75
				BREAK
				CASE 4
					strAnimName = "are_we_going"
					fPhase = 0.75
				BREAK
			ENDSWITCH
			
			IF IS_ENTITY_PLAYING_ANIM(tempPed, strAnimDict, strAnimName, ANIM_SCRIPT)
				IF GET_ENTITY_ANIM_CURRENT_TIME(tempPed, strAnimDict, strAnimName) < fPhase
					bReadyForIdle = FALSE
				ELSE
				ENDIF
			ENDIF
			
		ENDFOR
		
	ENDIF
		
	RETURN bReadyForIdle
	
ENDFUNC

//PURPOSE: Checks the ped's settings to see if ths character should be acting sneaky
PROC PROCESS_SHOULD_PED_BE_SNEAKING(INT iPed)

	//Exit if this ped doesn't exist
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(mc_serverBD_1.sFMMC_SBD.niPed[iPed])
		EXIT
	ENDIF
	
	//Exit if we do not own the ped
	IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(mc_serverBD_1.sFMMC_SBD.niPed[iPed])
		EXIT
	ENDIF
	
	PED_INDEX piThisPed = NET_TO_PED(mc_serverBD_1.sFMMC_SBD.niPed[iPed])
	
	//Exit if this ped is injured
	IF IS_PED_INJURED(piThisPed)
		EXIT
	ENDIF
	
	IF IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed], PED_ASSOCIATED_GOTO_TASK_TYPE_SNEAKY) 
	AND NOT IS_PED_IN_COMBAT(piThisPed) // Am I in combat?
		IF NOT GET_PED_STEALTH_MOVEMENT(piThisPed)
			SET_PED_STEALTH_MOVEMENT((piThisPed), TRUE)
			PRINTLN("[TMS] PROCESS_SHOULD_PED_BE_SNEAKING - Ped ", iPed, " is being told to be sneaky")
		ELSE
			//PRINTLN("[TMS] Ped ", iPed, " is already being sneaky")
		ENDIF
	ELSE
		IF GET_PED_STEALTH_MOVEMENT(piThisPed)
			SET_PED_STEALTH_MOVEMENT((piThisPed), FALSE)
			PRINTLN("[TMS] PROCESS_SHOULD_PED_BE_SNEAKING - Ped ", iPed, " is being told to stop being sneaky due to his Go-To settings OR because he's in combat")
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_PED_FLICKER(INT iPed)

	PED_INDEX piCurPed = NET_TO_PED(mc_serverBD_1.sFMMC_SBD.niPed[iPed])
	INT iPedHealth = GET_ENTITY_HEALTH(piCurPed)
	INT iPedMaxHealth = GET_ENTITY_MAX_HEALTH(piCurPed)
	
	FLOAT fPedHealthPercentage = (TO_FLOAT(iPedHealth) / TO_FLOAT(iPedMaxHealth)) * 100
	
	SET_RANDOM_SEED(NATIVE_TO_INT(GET_NETWORK_TIME()))
	FLOAT fRand = GET_RANDOM_FLOAT_IN_RANGE(0.0, 100.0)
	
	PRINTLN("[TMS][FlickeringPeds] HP = ", iPedHealth, " | MAX HP: ", iPedMaxHealth, " | Ped ", iPed)
	PRINTLN("[TMS][FlickeringPeds] fRand = ", fRand, " | Health Percentage: ", fPedHealthPercentage, " | Ped ", iPed)
	
	IF fPedHealthPercentage > 75.0
		PRINTLN("[TMS][FlickeringPeds] Returning FALSE because health is larger than 75% | Ped ", iPed)
		
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxFlickeringInvisiblePedEffects[iPed])
			REMOVE_PARTICLE_FX(ptfxFlickeringInvisiblePedEffects[iPed])
			PRINTLN("[FlickeringPeds] - removing particles for ped ", iPed)
		ENDIF
		
		RETURN FALSE
	ELSE
		IF HAS_NAMED_PTFX_ASSET_LOADED("scr_xm_heat")
		AND NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxFlickeringInvisiblePedEffects[iPed])
			USE_PARTICLE_FX_ASSET("scr_xm_heat")
			
			ptfxFlickeringInvisiblePedEffects[iPed] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_xm_heat_camo", piCurPed, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>)
			PRINTLN("[FlickeringPeds] - starting particles for ped ", iPed)
		ENDIF
		
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxFlickeringInvisiblePedEffects[iPed])
			PRINTLN("[FlickeringPeds] - flicker particles already exist for ped ", iPed)
		ENDIF
	ENDIF
	
	IF fPedHealthPercentage <= 25.0
		PRINTLN("[TMS][FlickeringPeds] Returning TRUE because health is less than 25% | Ped ", iPed)
		RETURN TRUE
	ENDIF
	
	RETURN fRand > fPedHealthPercentage
ENDFUNC

FUNC BOOL IS_PED_INVISIBLE(INT iPed)

	INT iTeamToCheck = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCloakTeamIndex
		
	IF iTeamToCheck = -1
		RETURN FALSE
	ENDIF	
	
	INT iRuleToCheck = gBG_MC_serverBD_VARS.iCurrentHighestPriority[iTeamToCheck]
	
	//Exit if the feature isn't used or if the current rule index is invalid
	IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iBecomeCloakedOnRule < 0)
	OR (iRuleToCheck >= FMMC_MAX_RULES)
	OR NOT DOES_ENTITY_EXIST(NET_TO_PED(mc_serverBD_1.sFMMC_SBD.niPed[iPed]))
		RETURN FALSE
	ENDIF
	
	//If the current rule number is higher than the "become cloaked on this rule", you might want to be cloaked right now
	BOOL bCurrentlyInvisible = (iRuleToCheck >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iBecomeCloakedOnRule)
	
	//Unless the rule number has gone past the "remove cloak" rule number (if it's larger than -1)
	IF (iRuleToCheck >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iLoseCloakOnRule)
	AND(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iLoseCloakOnRule > -1)
		bCurrentlyInvisible = FALSE
	ENDIF
	
	RETURN bCurrentlyInvisible
ENDFUNC
FUNC BOOL PLAY_TASK_ANIMATION_DIC(PED_INDEX tempPed,INT iped, STRING strAnimDict,STRING strAnimName, FLOAT fBlendIn = NORMAL_BLEND_IN, FLOAT fBlendOut = NORMAL_BLEND_OUT, ANIMATION_FLAGS eFlags = AF_LOOPING, BOOL bForceTask = FALSE)
	IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
	OR bForceTask
		REQUEST_ANIM_DICT(strAnimDict)
		IF HAS_ANIM_DICT_LOADED(strAnimDict)
			PRINTLN("[PlayAnimation] - Ped ", iPed, " is playing Animation from PLAY_TASK_ANIMATION_DIC - Dict: ", strAnimDict, " Name: ", strAnimName)
			TASK_PLAY_ANIM(tempPed, strAnimDict, strAnimName, fBlendIn, fBlendOut, -1, eFlags)
			SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
			RETURN TRUE
		ENDIF
	ELSE
		REMOVE_ANIM_DICT(strAnimDict)
	ENDIF
	
	RETURN FALSE
ENDFUNC
PROC PLAY_TASK_ANIMATION_SET(PED_INDEX tempPed,INT iped, STRING strAnimSet,STRING strAnimDict,STRING strAnimName)
	IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
		REQUEST_CLIP_SET(strAnimSet)
		REQUEST_ANIM_DICT(strAnimDict)
		
		IF HAS_CLIP_SET_LOADED(strAnimSet)
		AND HAS_ANIM_DICT_LOADED(strAnimDict)
			PRINTLN("[PlayAnimation] - Ped ", iPed, " is playing Animation from PLAY_TASK_ANIMATION_SET - strAnimSet: ", strAnimSet, " Dict: ", strAnimDict, " Name: ", strAnimName)
			TASK_PLAY_ANIM(tempPed,strAnimDict,strAnimName,NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
			SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
		ENDIF
	ELSE
		REMOVE_CLIP_SET(strAnimSet)
	ENDIF
ENDPROC
//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: TASK PERFORM AMBIENT ANIMATION (animation tasking function) !
//
//************************************************************************************************************************************************************

FUNC STRING GET_DBG_NAME_PED_SYNC_SCENE_ANIM(INT iAnimation)
	SWITCH iAnimation
		CASE ciPED_IDLE_ANIM__CASINO_VINCENT_INTRO							RETURN "VINCENT_INTRO"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_BREAKOUT_1						RETURN "FIGHT_BREAKOUT"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_BREAKOUT_1_CROUPIER				RETURN "FIGHT_BREAKOUT"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_BREAKOUT_1_PEDESTRIAN			RETURN "FIGHT_BREAKOUT"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_SECURITY_GUARD					RETURN "FIGHT_SECURITY_GUARD"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_SECURITY_GUARD_REDNECK_1			RETURN "FIGHT_SECURITY_GUARD"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_SECURITY_GUARD_REDNECK_2			RETURN "FIGHT_SECURITY_GUARD"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CONVERSATION						RETURN "FIGHT_CONVERSATION"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CONVERSATION_SECOND				RETURN "FIGHT_CONVERSATION"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_HEADLOCK							RETURN "FIGHT_HEADLOCK"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_HEADLOCK_VICTIM					RETURN "FIGHT_HEADLOCK"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_ARGUE							RETURN "FIGHT_ARGUE"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_ARGUE_VICTIM						RETURN "FIGHT_ARGUE"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_BAR_THROW						RETURN "FIGHT_BAR_THROW"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_BAR_THROW_PEDESTRIAN				RETURN "FIGHT_BAR_THROW"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_SLOT_SLAM						RETURN "FIGHT_SLOT_SLAM"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_SLOT_SLAM_PEDESTRIAN				RETURN "FIGHT_SLOT_SLAM"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CHIP_STEAL_ATTACKER_1			RETURN "FIGHT_CHIP_STEAL"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CHIP_STEAL_ATTACKER_2			RETURN "FIGHT_CHIP_STEAL"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CHIP_STEAL_CROUPIER				RETURN "FIGHT_CHIP_STEAL"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CHIP_STEAL_PEDESTRIAN_1			RETURN "FIGHT_CHIP_STEAL"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CHIP_STEAL_PEDESTRIAN_2			RETURN "FIGHT_CHIP_STEAL"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CASHIER							RETURN "FIGHT_CASHIER"
		CASE ciPED_IDLE_ANIM__CASINO_SLOTMACHINE_ATTACK						RETURN "SLOTMACHINE_ATTACK"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_SHOP_THROW						RETURN "FIGHT_SHOP_THROW"
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_SHOP_THROW_VICTIM				RETURN "FIGHT_SHOP_THROW"
		CASE ciPED_IDLE_ANIM__CASINO_ROULETTE_REACT_1						RETURN "ROULETTE_REACT_1"
		CASE ciPED_IDLE_ANIM__CASINO_ROULETTE_REACT_2						RETURN "ROULETTE_REACT_2"
		CASE ciPED_IDLE_ANIM__CASINO_GET_IN_CAR_TAO							RETURN "GET_IN_CAR"
		CASE ciPED_IDLE_ANIM__CASINO_GET_IN_CAR_TRANS						RETURN "GET_IN_CAR"
		CASE ciPED_IDLE_ANIM__CASINO_GET_IN_CAR_AGATHA						RETURN "GET_IN_CAR"
		CASE ciPED_IDLE_ANIM__WINDOW_SHOP_IDLE								RETURN "BROWSE_A"
		CASE ciPED_IDLE_ANIM__CAR_PARK_ATTENDANT_IDLE						RETURN "BASE"
	ENDSWITCH
	
	RETURN "INVALID_POPULATE GET_DBG_NAME_PED_SYNC_SCENE_ANIM"
ENDFUNC


FUNC BOOL IS_THIS_A_SPECIAL_SYNC_ANIMATION(INT iAnimation)
	SWITCH iAnimation
		CASE ciPED_IDLE_ANIM__CASINO_VINCENT_INTRO					
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_BREAKOUT_1				
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_BREAKOUT_1_CROUPIER		
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_BREAKOUT_1_PEDESTRIAN	
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_SECURITY_GUARD			
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_SECURITY_GUARD_REDNECK_1	
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_SECURITY_GUARD_REDNECK_2	
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CONVERSATION				
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CONVERSATION_SECOND		
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_HEADLOCK					
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_HEADLOCK_VICTIM			
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_ARGUE					
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_ARGUE_VICTIM				
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_BAR_THROW				
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_BAR_THROW_PEDESTRIAN		
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_SLOT_SLAM				
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_SLOT_SLAM_PEDESTRIAN		
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CHIP_STEAL_ATTACKER_1	
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CHIP_STEAL_ATTACKER_2	
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CHIP_STEAL_CROUPIER		
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CHIP_STEAL_PEDESTRIAN_1	
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CHIP_STEAL_PEDESTRIAN_2	
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CASHIER					
		CASE ciPED_IDLE_ANIM__CASINO_SLOTMACHINE_ATTACK				
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_SHOP_THROW				
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_SHOP_THROW_VICTIM		
		CASE ciPED_IDLE_ANIM__CASINO_ROULETTE_REACT_1				
		CASE ciPED_IDLE_ANIM__CASINO_ROULETTE_REACT_2				
		CASE ciPED_IDLE_ANIM__CASINO_GET_IN_CAR_TAO					
		CASE ciPED_IDLE_ANIM__CASINO_GET_IN_CAR_TRANS				
		CASE ciPED_IDLE_ANIM__CASINO_GET_IN_CAR_AGATHA	
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC CLEAR_SYNC_SCENE_PED_OWNER(INT iPed)
	sMissionPedsLocalVars[iPed].iSyncPedPartOwner = -1
ENDPROC

PROC SET_PARTICIPANT_AS_SYNC_SCENE_PED_OWNER(INT iPart, INT iPed)
	PRINTLN("[LM][RCC MISSION][SET_PARTICIPANT_AS_SYNC_SCENE_PED_OWNER] - PROCESS_PED_ANIMATION - iPart: ", iPart, " is the new owner for the anims for iPed: ", iPed)
	sMissionPedsLocalVars[iPed].iSyncPedPartOwner = iPart
	BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iPart, g_ciInstancedcontentEventType_SyncScenePedOwnerBroadcast, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT)
ENDPROC

FUNC INT GET_PARTICIPANT_SYNC_SCENE_PED_OWNER(INT iPed)
	RETURN sMissionPedsLocalVars[iPed].iSyncPedPartOwner
ENDFUNC

FUNC BOOL IS_PARTICIPANT_SYNC_SCENE_PED_OWNER_SET(INT iPed)
	RETURN (GET_PARTICIPANT_SYNC_SCENE_PED_OWNER(iPed) > -1)
ENDFUNC

FUNC BOOL IS_PARTICIPANT_SYNC_SCENE_PED_OWNER_ACTIVE(INT iPed)
	IF IS_PARTICIPANT_SYNC_SCENE_PED_OWNER_SET(iPed)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(GET_PARTICIPANT_SYNC_SCENE_PED_OWNER(iPed)))
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PARTICIPANT_SYNC_SCENE_PED_OWNER(INT iPart, INT iPed)
	RETURN (GET_PARTICIPANT_SYNC_SCENE_PED_OWNER(iPed) = iPart)
ENDFUNC

// This function will check all the related peds exist in the SYNC SCENE we are about to process. We cache related peds with the main peds index "iPed" which is being passed in.
FUNC BOOL ARE_ALL_PEDS_READY_FOR_SYNC_SCENE(INT iPed, BOOL bGrabOwnership = TRUE)
	
	NETWORK_INDEX netPed
	PED_INDEX pedToCheck
	
	netPed = MC_serverBD_1.sFMMC_SBD.niPed[iPed]
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(netPed)
		PRINTLN("[LM][RCC MISSION][ARE_ALL_PEDS_READY_FOR_SYNC_SCENE][Sync_anims: ", iPed, "] PROCESS_PED_ANIMATION - (Main) iPed: ", iPed, " (NET) Does not exist")
		RETURN FALSE
	ENDIF
	pedToCheck = NET_TO_PED(netPed)
	IF NOT DOES_ENTITY_EXIST(pedToCheck)
		PRINTLN("[LM][RCC MISSION][ARE_ALL_PEDS_READY_FOR_SYNC_SCENE][Sync_anims: ", iPed, "] PROCESS_PED_ANIMATION - (Main) iPed: ", iPed, " (PED) Does not exist")
		RETURN FALSE
	ENDIF
	IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(netPed)
	AND bGrabOwnership
		PRINTLN("[LM][RCC MISSION][ARE_ALL_PEDS_READY_FOR_SYNC_SCENE][Sync_anims: ", iPed, "] PROCESS_PED_ANIMATION - (Main) iPed: ", iPed, " We do not have control - Requesting Control.")
		NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(netPed)
		RETURN FALSE
	ENDIF
	
	INT iActualPed = -1	
	INT iSyncedPeds = 0
	FOR iSyncedPeds = 0 TO FMMC_MAX_PED_SCENE_SYNCED_PEDS-1
		iActualPed = sMissionPedsLocalVars[iPed].iSyncedPeds[iSyncedPeds]
		
		IF iActualPed = 999 // -1 means unassigned. 999 means missing...
			PRINTLN("[LM][RCC MISSION][ARE_ALL_PEDS_READY_FOR_SYNC_SCENE][Sync_anims: ", iPed, "] PROCESS_PED_ANIMATION - (SyncedPed) Slot: ", iSyncedPeds, " | Actual Index: ", iActualPed)
			ASSERTLN("PED SYNC ANIMATION IS NOT SET UP PROPERLY. YOU HAVE ANIMS WITH MISSING PEDS.")
			RETURN FALSE
		ENDIF
		
		IF iActualPed > -1
			netPed = MC_serverBD_1.sFMMC_SBD.niPed[iActualPed]
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(netPed)
				PRINTLN("[LM][RCC MISSION][ARE_ALL_PEDS_READY_FOR_SYNC_SCENE][Sync_anims: ", iPed, "] PROCESS_PED_ANIMATION - (SyncedPed) Slot: ", iSyncedPeds, " | Actual Index: ", iActualPed, " (NET) Does not exist")
				RETURN FALSE
			ENDIF
			pedToCheck = NET_TO_PED(netPed)
			IF NOT DOES_ENTITY_EXIST(pedToCheck)
				PRINTLN("[LM][RCC MISSION][ARE_ALL_PEDS_READY_FOR_SYNC_SCENE][Sync_anims: ", iPed, "] PROCESS_PED_ANIMATION - (SyncedPed) Slot: ", iSyncedPeds, " | Actual Index: ", iActualPed, " (PED) Does not exist")
				RETURN FALSE
			ENDIF
			IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(netPed)
			AND bGrabOwnership
				PRINTLN("[LM][RCC MISSION][ARE_ALL_PEDS_READY_FOR_SYNC_SCENE][Sync_anims: ", iPed, "] PROCESS_PED_ANIMATION - (SyncedPed) Slot: ", iSyncedPeds, " | Actual Index iPed: ", iActualPed, " We do not have control - Requesting Control.")
				NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(netPed)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SYNC_SCENE_KEEP_ENTITY_OWNERSHIP(INt iPed)
	
	NETWORK_INDEX netPed
	netPed = MC_serverBD_1.sFMMC_SBD.niPed[iPed]
	IF NETWORK_DOES_NETWORK_ID_EXIST(netPed)
		IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(netPed)
			PRINTLN("[LM][RCC MISSION][SYNC_SCENE_KEEP_ENTITY_OWNERSHIP][Sync_anims: ", iPed, "] PROCESS_PED_ANIMATION - (Main) iPed: ", iPed, " We do not have control - Requesting Control.")
			NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(netPed)
			RETURN FALSE
		ELSE
			SET_NETWORK_ID_CAN_MIGRATE(netPed, FALSE)
		ENDIF
	ENDIF
	
	INT iActualPed = -1	
	INT iSyncedPeds = 0
	FOR iSyncedPeds = 0 TO FMMC_MAX_PED_SCENE_SYNCED_PEDS-1
		iActualPed = sMissionPedsLocalVars[iPed].iSyncedPeds[iSyncedPeds]		
		IF iActualPed < 999 // -1 means unassigned. 999 means missing...
		AND iActualPed > -1
			netPed = MC_serverBD_1.sFMMC_SBD.niPed[iActualPed]
			IF NETWORK_DOES_NETWORK_ID_EXIST(netPed)
				IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(netPed)
					PRINTLN("[LM][RCC MISSION][SYNC_SCENE_KEEP_ENTITY_OWNERSHIP][Sync_anims: ", iPed, "] PROCESS_PED_ANIMATION - (SyncedPed) Slot: ", iSyncedPeds, " | Actual Index iPed: ", iActualPed, " We do not have control - Requesting Control.")
					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(netPed)
					RETURN FALSE
				ELSE
					SET_NETWORK_ID_CAN_MIGRATE(netPed, FALSE)
				ENDIF
			ENDIF
		ENDIF		
	ENDFOR
	
	RETURN TRUE
ENDFUNC

// We are not script defined owner, allow it to migrate to whoever is requesting control...
FUNC BOOL RELINQUISH_SYNC_SCENE_ENTITY_OWNERSHIP(INt iPed)
	
	NETWORK_INDEX netPed
	netPed = MC_serverBD_1.sFMMC_SBD.niPed[iPed]
	IF NETWORK_DOES_NETWORK_ID_EXIST(netPed)
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(netPed)
			PRINTLN("[LM][RCC MISSION][RELINQUISH_SYNC_SCENE_ENTITY_OWNERSHIP][Sync_anims: ", iPed, "] PROCESS_PED_ANIMATION - (Main) iPed: ", iPed, " We have control but are not the script defined owner - releasing Control.")
			SET_NETWORK_ID_CAN_MIGRATE(netPed, TRUE)
		ENDIF
	ENDIF
	
	INT iActualPed = -1	
	INT iSyncedPeds = 0
	FOR iSyncedPeds = 0 TO FMMC_MAX_PED_SCENE_SYNCED_PEDS-1
		iActualPed = sMissionPedsLocalVars[iPed].iSyncedPeds[iSyncedPeds]		
		IF iActualPed < 999 // -1 means unassigned. 999 means missing...
		AND iActualPed > -1
			netPed = MC_serverBD_1.sFMMC_SBD.niPed[iActualPed]
			IF NETWORK_DOES_NETWORK_ID_EXIST(netPed)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(netPed)
					PRINTLN("[LM][RCC MISSION][RELINQUISH_SYNC_SCENE_ENTITY_OWNERSHIP][Sync_anims: ", iPed, "] PROCESS_PED_ANIMATION - (SyncedPed) Slot: ", iSyncedPeds, " | Actual Index iPed: ", iActualPed, " We have control but are not the script defined owner - releasing Control.")
					SET_NETWORK_ID_CAN_MIGRATE(netPed, TRUE)
				ENDIF
			ENDIF
		ENDIF		
	ENDFOR
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_ANY_PLAYER_INSIDE_BREAKOUT_RANGE(BOOL bSpecialAnim, INT iPed, PED_INDEX tempPed)
	FLOAT fBreakoutRange = -1	
	IF NOT bSpecialAnim
		fBreakoutRange = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].fAnimBreakoutRange
	ELSE
		fBreakoutRange = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].fAnimSpecialBreakoutRange
	ENDIF
	
	INT iPart = 0
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		PARTICIPANT_INDEX piPart = INT_TO_PARTICIPANTINDEX(iPart)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(piPart)
			IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)
				PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)
				PED_INDEX pedPlayer = GET_PLAYER_PED(piPlayer)
				
				IF NOT IS_PED_INJURED(pedPlayer)
					IF IS_ENTITY_ALIVE(pedPlayer) AND IS_ENTITY_ALIVE(tempPed)
						VECTOR vPos1 = GET_ENTITY_COORDS(pedPlayer)
						VECTOR vPos2 = GET_ENTITY_COORDS(tempPed)
						IF VDIST2(vPos1, vPos2) <= POW(fBreakoutRange, 2.0)
							PRINTLN("[LM][RCC MISSION][IS_ANY_PLAYER_INSIDE_BREAKOUT_RANGE][Sync_anims: ", iPed, "] PROCESS_PED_ANIMATION - iPart: ", iPart, " Pos: ", vPos1, " is within fBreakoutRange: ", fBreakoutRange, " Enemy Pos: ", vPos2) 
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

PROC ASSIGN_LINKED_PEDS_FOR_PED_INDEX(INT iPed, BOOL bSpecialAnim, INT iAnimConstStart, INT iAnimConstEnd #IF IS_DEBUG_BUILD, STRING sDbgName #ENDIF, BOOL bOwner = FALSE)
	INT iAnimation
	INT iPeds
	INT iSlot
	FLOAT fTempDist2
	INT iPedsAdded
	
	FOR iPeds = 0 TO FMMC_MAX_PEDS-1
		IF NOT bSpecialAnim
			iAnimation = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPeds].iIdleAnim
		ELSE
			iAnimation = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPeds].iSpecialAnim
		ENDIF
		
		IF iAnimation >= iAnimConstStart AND iAnimation <= iAnimConstEnd
			iSlot = (iAnimation - iAnimConstStart)
			
			PRINTLN("[LM][INITIALIZE_LINKED_SYNC_PEDS][Sync_anims: ", iPed, "][", sDbgName, "] - Looking at adding iPed: ", iPeds, " based on selected Sync Animations - iAnimation: ", iAnimation, " iSlot: ", iSlot)
			
			NETWORK_INDEX netPed = MC_serverBD_1.sFMMC_SBD.niPed[iPeds]
			IF NETWORK_DOES_NETWORK_ID_EXIST(netPed)
				IF iSlot > -1 AND iSlot <= iAnimConstEnd
					IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPeds].vPos)
						fTempDist2 = VDIST2(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPeds].vPos, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPeds].vPos)
						IF fTempDist2 <= 80
							PRINTLN("[LM][INITIALIZE_LINKED_SYNC_PEDS][Sync_anims: ", iPed, "][", sDbgName, "] - iPedFound: ", iPeds, " iAnimation: ", iAnimation, " ADDING in slot: ", iSlot)
							sMissionPedsLocalVars[iPed].iSyncedPeds[iSlot] = iPeds
							IF bOwner
								SET_PARTICIPANT_AS_SYNC_SCENE_PED_OWNER(iLocalPart, iPeds)
							ENDIF
							iPedsAdded++
						ELSE
							PRINTLN("[LM][INITIALIZE_LINKED_SYNC_PEDS][Sync_anims: ", iPed, "][", sDbgName, "] - iPedFound: ", iPeds, " iAnimation: ", iAnimation, " BUT NOT ADDING BECAUSE OUT OF RANGE!")
						ENDIF
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[LM][INITIALIZE_LINKED_SYNC_PEDS][Sync_anims: ", iPed, "][", sDbgName, "] - Looking at adding iPed: ", iPeds, " SKIPPING - PED DOES NOT EXIST.")
			ENDIF
		ENDIF
		
		IF iPedsAdded >= FMMC_MAX_PED_SCENE_SYNCED_PEDS
			PRINTLN("[LM][INITIALIZE_LINKED_SYNC_PEDS][Sync_anims: ", iPed, "][", sDbgName, "] - iPedsAdded >= FMMC_MAX_PED_SCENE_SYNCED_PEDS - Breakloop..........")
			BREAKLOOP
		ENDIF
	ENDFOR
	
	// Overrun prevention for missing anim peds.... url:bugstar:5810407
	// 999 means missing. -1 Just means unassigned.
	IF iPedsAdded <= (iAnimConstEnd-iAnimConstStart)
		FOR iPeds = 0 TO FMMC_MAX_PED_SCENE_SYNCED_PEDS-1
			IF sMissionPedsLocalVars[iPed].iSyncedPeds[iPeds] != -1
				sMissionPedsLocalVars[iPed].iSyncedPeds[iPeds] = 999
			ENDIF
		ENDFOR
		SET_BIT(sMissionPedsLocalVars[iPed].iPedBS,  ci_MissionPedBS_iSyncSceneAnimationNeedResync)
	ENDIF
ENDPROC

// This function will Assign all the related peds based on their selected animations in the Creator. Animation CONSTs should be sequential if they are related, else this won't work....
PROC INITIALIZE_LINKED_SYNC_PEDS_DATA(INT iPed, BOOL bSpecialAnim, INT iAnimConstStart, INT iAnimConstEnd #IF IS_DEBUG_BUILD, STRING sDbgName #ENDIF , BOOL bOwner = FALSE)
	INT iSyncedPeds
	INT iLinkedPed
	
	PRINTLN("[LM][INITIALIZE_LINKED_SYNC_PEDS][Sync_anims: ", iPed, "][", sDbgName, "] - Linking Peds for Sync Scene for PED: ", iPed, " based on selected Sync Animations.")	
	PRINTLN("[LM][INITIALIZE_LINKED_SYNC_PEDS][Sync_anims: ", iPed, "][", sDbgName, "] - bSpecialAnim: ", bSpecialAnim, " iAnimConstStart: ", iAnimConstStart, " iAnimConstEnd: ", iAnimConstEnd)
	
	ASSIGN_LINKED_PEDS_FOR_PED_INDEX(iPed, bSpecialAnim, iAnimConstStart, iAnimConstEnd #IF IS_DEBUG_BUILD , sDbgName #ENDIF , bOwner)
	
	// Might not need this two-way linkage, but it might come in handy 
	PRINTLN("[LM][INITIALIZE_LINKED_SYNC_PEDS][Sync_anims: ", iPed, "][", sDbgName, "] - ------- (Two-Way Linking Starting) -------")
	FOR iSyncedPeds = 0 TO FMMC_MAX_PED_SCENE_SYNCED_PEDS-1
		IF sMissionPedsLocalVars[iPed].iSyncedPeds[iSyncedPeds] != -1
		AND sMissionPedsLocalVars[iPed].iSyncedPeds[iSyncedPeds] < 999
			iLinkedPed = sMissionPedsLocalVars[iPed].iSyncedPeds[iSyncedPeds]
			IF iLinkedPed != iPed			
				PRINTLN("[LM][INITIALIZE_LINKED_SYNC_PEDS][Sync_anims: ", iPed, "][", sDbgName, "] - (Two-way) - Linking Peds for Sync Scene for PED: ", iLinkedPed, " based on selected Sync Animations.")
				ASSIGN_LINKED_PEDS_FOR_PED_INDEX(iLinkedPed, bSpecialAnim, iAnimConstStart, iAnimConstEnd #IF IS_DEBUG_BUILD , sDbgName #ENDIF , bOwner)
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

FUNC BOOL IS_NETWORKED_SYNC_SCENE_PED_READY(INT iPed, STRING strAnimDict, INT iAnimConstStart, INT iAnimConstEnd, BOOL bGrabOwnership #IF IS_DEBUG_BUILD, STRING sDbgName #ENDIF)
	
	// Already Finished
	IF IS_BIT_SET(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_iSyncSceneAnimationFinished)
		PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", sDbgName, "] - PROCESS_PED_ANIMATION - Sync Scene Animation Finished.")
		RETURN FALSE
	ENDIF
	
	// If we haven't started yet.
	IF NOT IS_BIT_SET(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_iSyncSceneAnimationReady)	
		// Asset Load.
		REQUEST_ANIM_DICT(strAnimDict)
		IF NOT HAS_ANIM_DICT_LOADED(strAnimDict)
			PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", sDbgName, "] - PROCESS_PED_ANIMATION - NOT HAS_ANIM_DICT_LOADED - Exit")
			RETURN FALSE
		ENDIF
		
		// Initialize Ped Data.
		IF sMissionPedsLocalVars[iPed].iSyncedPeds[0] = -1
		OR IS_BIT_SET(sMissionPedsLocalVars[iPed].iPedBS,  ci_MissionPedBS_iSyncSceneAnimationNeedResync)
			INITIALIZE_LINKED_SYNC_PEDS_DATA(iPed, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)), iAnimConstStart, iAnimConstEnd #IF IS_DEBUG_BUILD , sDbgName #ENDIF , bGrabOwnership)
			CLEAR_BIT(sMissionPedsLocalVars[iPed].iPedBS,  ci_MissionPedBS_iSyncSceneAnimationNeedResync)
		ENDIF
		
		// Check we have valid data. Overruns and such should be prevented here.
		IF IS_BIT_SET(sMissionPedsLocalVars[iPed].iPedBS,  ci_MissionPedBS_iSyncSceneAnimationNeedResync)
		OR NOT ARE_ALL_PEDS_READY_FOR_SYNC_SCENE(iPed, bGrabOwnership)
			PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", sDbgName, "] - PROCESS_PED_ANIMATION - Peds are not ready - Exit")
			RETURN FALSE
		ENDIF
			
		// Ready Lock Peds In.
		INITIALIZE_LINKED_SYNC_PEDS_DATA(iPed, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)), iAnimConstStart, iAnimConstEnd #IF IS_DEBUG_BUILD , sDbgName #ENDIF , bGrabOwnership)
		
	ELSE
		// This will ensure if the player owning the entities leaves we can re-grab ownership and stop them migrating.
		IF IS_PARTICIPANT_SYNC_SCENE_PED_OWNER(iLocalPart, iPed)
			IF sMissionPedsLocalVars[iPed].ePedSyncSceneState > PED_SYNC_SCENE_STATE_INIT
			AND sMissionPedsLocalVars[iPed].ePedSyncSceneState < PED_SYNC_SCENE_STATE_CLEANUP
				IF NOT SYNC_SCENE_KEEP_ENTITY_OWNERSHIP(iPed)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// If it "WAS" ready, we don't care about whether peds are alive or not afterwards because they can die as part of the animations.
	SET_BIT(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_iSyncSceneAnimationReady)
	
	RETURN TRUE
ENDFUNC

FUNC BOOL ARE_LINKED_SYNC_PEDS_ALIVE(INT iPed)

	IF IS_BIT_SET(MC_serverBD.iPedCleanup_NeedOwnershipBS[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
		PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "] - PROCESS_PED_ANIMATION - Peds are set to cleanup - Sending straight to cleanup state!")
		RETURN FALSE
	ENDIF
	
	NETWORK_INDEX netPed
	PED_INDEX pedToCheck
	INT iSyncedPeds, iActualPed
	FOR iSyncedPeds = 0 TO FMMC_MAX_PED_SCENE_SYNCED_PEDS-1
		iActualPed = sMissionPedsLocalVars[iPed].iSyncedPeds[iSyncedPeds]
		IF iActualPed > -1
		AND iActualPed < 999
			IF IS_BIT_SET(MC_serverBD.iPedCleanup_NeedOwnershipBS[GET_LONG_BITSET_INDEX(iActualPed)], GET_LONG_BITSET_BIT(iActualPed))
				PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "] - PROCESS_PED_ANIMATION - iActualPed: ", iActualPed, " is set to cleanup - Sending straight to cleanup state!")
				RETURN FALSE
			ENDIF			
			netPed = MC_serverBD_1.sFMMC_SBD.niPed[iActualPed]
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(netPed)
				RETURN FALSE
			ENDIF
			pedToCheck = NET_TO_PED(netPed)
			IF NOT DOES_ENTITY_EXIST(pedToCheck)
				RETURN FALSE
			ENDIF
			IF IS_PED_INJURED(pedToCheck)
				RETURN FALSE
			ENDIF	
		ENDIF
	ENDFOR
	RETURN TRUE
ENDFUNC

PROC INITIALIZE_LINKED_SYNC_PEDS_STATE(INT iPed, BOOL bClearTasks = TRUE)
	NETWORK_INDEX netPed
	PED_INDEX pedToCheck
	INT iSyncedPeds, iActualPed
	FOR iSyncedPeds = 0 TO FMMC_MAX_PED_SCENE_SYNCED_PEDS-1
		iActualPed = sMissionPedsLocalVars[iPed].iSyncedPeds[iSyncedPeds]
		IF iActualPed > -1
		AND iActualPed < 999
			netPed = MC_serverBD_1.sFMMC_SBD.niPed[iActualPed]
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(netPed)
				RELOOP
			ENDIF
			pedToCheck = NET_TO_PED(netPed)
			IF NOT DOES_ENTITY_EXIST(pedToCheck)
				RELOOP
			ENDIF
			IF NOT IS_PED_INJURED(pedToCheck)
				SET_PED_CONFIG_FLAG(pedToCheck, PCF_PedIgnoresAnimInterruptEvents, TRUE)
			ENDIF
			SET_NETWORK_ID_CAN_MIGRATE(netPed, FALSE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedToCheck, TRUE)
			IF bClearTasks
				PRINTLN("[LM][RCC MISSION][INITIALIZE_LINKED_SYNC_PEDS_STATE] - PROCESS_PED_ANIMATION - iPed: ", iActualPed, "Clearing Ped Tasks")
				CLEAR_PED_TASKS_IMMEDIATELY(pedToCheck)
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC CLEANUP_SYNC_SCENE_OWNER_DATA(INT iPed)
	INT iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sMissionPedsLocalVars[iPed].iSyncScene)
	IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
		NETWORK_STOP_SYNCHRONISED_SCENE(sMissionPedsLocalVars[iPed].iSyncScene)
	ENDIF
	
	NETWORK_INDEX netPed
	PED_INDEX pedToCheck
	INT iSyncedPeds, iActualPed
	FOR iSyncedPeds = 0 TO FMMC_MAX_PED_SCENE_SYNCED_PEDS-1
		iActualPed = sMissionPedsLocalVars[iPed].iSyncedPeds[iSyncedPeds]
		IF iActualPed > -1
			netPed = MC_serverBD_1.sFMMC_SBD.niPed[iActualPed]
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(netPed)
				RELOOP
			ENDIF
			pedToCheck = NET_TO_PED(netPed)
			IF NOT DOES_ENTITY_EXIST(pedToCheck)
			OR IS_PED_INJURED(pedToCheck)
				RELOOP
			ENDIF
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedToCheck, TRUE)
			SET_NETWORK_ID_CAN_MIGRATE(netPed, TRUE)
			SET_PED_USING_ACTION_MODE(pedToCheck, TRUE, -1, "DEFAULT_ACTION")
			FORCE_PED_AI_AND_ANIMATION_UPDATE(pedToCheck)
		ENDIF
	ENDFOR
ENDPROC

PROC CLEANUP_SYNC_SCENE_LOCAL_DATA(INT iPed)
	INT iSyncedPeds
	FOR iSyncedPeds = 0 TO FMMC_MAX_PED_SCENE_SYNCED_PEDS-1
		IF sMissionPedsLocalVars[iPed].iSyncedPeds[iSyncedPeds] > -1
			SET_BIT(sMissionPedsLocalVars[sMissionPedsLocalVars[iPed].iSyncedPeds[iSyncedPeds]].iPedBS, ci_MissionPedBS_iSyncSceneAnimationFinished)
		ENDIF
		sMissionPedsLocalVars[iPed].iSyncedPeds[iSyncedPeds] = -1
		CLEAR_SYNC_SCENE_PED_OWNER(iSyncedPeds)
	ENDFOR
	sMissionPedsLocalVars[iPed].iSyncScene = -1	
	CLEAR_SYNC_SCENE_PED_OWNER(iPed)
	iCasinoBrawlBottleOutcome = -1
	
	SET_BIT(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_iSyncSceneAnimationFinished)
	
	IF NOT IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetTwelve, ciPed_BSTwelve_SpookAfterFinishingAnimation)
	AND NOT IS_BIT_SET( iPedHitByStunGun[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
		PRINTLN("[LM][RCC MISSION][Sync_anims: ", iPed, "] - PROCESS_PED_ANIMATION - Spooking Ped: ", iPed, " as we have finished the animation.")
		BROADCAST_FMMC_PED_SPOOKED(iped, ciSPOOK_NONE, iLocalPart)
	ENDIF
	
	PRINTLN("[LM][RCC MISSION][Sync_anims: ", iPed, "] - PROCESS_PED_ANIMATION - CLEANUP_SYNC_SCENE_LOCAL_DATA Cleaning up Anim data for Ped: ", iPed)
ENDPROC

#IF IS_DEBUG_BUILD
PROC DEBUG_SYNC_SCENE_ANIM_FOR_PED(INT iPed)
	INT iPedSLot
	FOR iPedSlot = 0 TO FMMC_MAX_PED_SCENE_SYNCED_PEDS-1
		TEXT_LABEL_63 tl31
		tl31 = "Slot: "
		tl31+= iPedSlot
		tl31+= " PedIdex: "
		tl31+= sMissionPedsLocalVars[iPed].iSyncedPeds[iPedSlot]	
		PRINT_LUKE_ON_SCREEN_DEBUG_TEXT(tl31, "", vBlankDbg, DEFAULT, DEFAULT, TRUE)
	ENDFOR
ENDPROC
#ENDIF

FUNC BOOL PLAY_SCENARIO_ON_PED_WITH_SPECIFIC_ANIM(PED_INDEX piPed, INT iPed, STRING sScenario, STRING sDict, STRING sClipSet, STRING sClip)
	
	IF SHOULD_PED_BE_GIVEN_TASK(piPed, iped, SCRIPT_TASK_START_SCENARIO_IN_PLACE)
		REQUEST_ANIM_DICT(sDict)
		IF HAS_ANIM_DICT_LOADED(sDict)
			//OVERRIDE_TASKED_SCENARIO_BASE_ANIM(piPed, sClipSet, sClip)
			TASK_START_SCENARIO_IN_PLACE(piPed, sScenario, 0, FALSE)
			CLEAR_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			RETURN TRUE
		ENDIF
	ELSE
		IF IS_PED_PLAYING_BASE_CLIP_IN_SCENARIO(piPed)
		AND NOT IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			PLAY_ANIM_ON_RUNNING_SCENARIO(piPed, sClipSet, sClip)
			SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: This function tasks peds with the animation being passed into it, as well as handling other logic that needs to happen
///	    	along with the tasking (eg: requesting assets, making some animation objects and setting up sync scenes)
///    Returns TRUE if the ped is performing the animation (though only in some animation cases where we only want the animation to
///    		play once)
FUNC BOOL TASK_PERFORM_AMBIENT_ANIMATION(PED_INDEX tempPed, INT iped, INT iAnimation, BOOL bForcingBreakout = FALSE, BOOL bisSpecialAnim = FALSE)

	PRINTLN("[RCC Mission] TASK_PERFORM_AMBIENT_ANIMATION entered - iped: ", iped, ", iAnimation: ", iAnimation)
	
	BOOL bAnimating = TRUE
	INT iMaxObjects = GET_MAX_NUM_NETWORK_OBJECTS()
	VECTOR vCoords, vRot
	FLOAT fHeading
	
	#IF NOT IS_DEBUG_BUILD
	iped = iped
	bForcingBreakout = bForcingBreakout
	#ENDIF
	
	VECTOR vPedPos
	
	CONST_FLOAT cfSIT_RADIUS 10.00
	
	STRING strAnimDict
	STRING strAnimName
	STRING strAnimNameIdle
	
	TEXT_LABEL_63 strCustomAnimDict = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].tlCustomIdleAnimDict
	TEXT_LABEL_63 strCustomAnimName = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].tlCustomIdleAnimName
	
	IF bIsSpecialAnim
		//This is where to plug in g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].tlCustomSpecialAnimDict etc if requested
//		strCustomAnimDict = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].tlCustomIdleAnimDict
//		strCustomAnimName = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].tlCustomIdleAnimName
	ENDIF
	
	IF IS_STRING_NULL_OR_EMPTY(strCustomAnimDict)
		SWITCH iAnimation
			CASE ciPED_IDLE_ANIM__PHONE
				IF NETWORK_HAS_CONTROL_OF_ENTITY(tempPed)	
					IF NOT IS_PED_RUNNING_MOBILE_PHONE_TASK(tempPed)
						TASK_USE_MOBILE_PHONE(tempPed,TRUE)
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					ENDIF
				ENDIF
			BREAK
			CASE ciPED_IDLE_ANIM__SMOKE
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_START_SCENARIO_IN_PLACE)
					TASK_START_SCENARIO_IN_PLACE(tempPed,"WORLD_HUMAN_SMOKING", DEFAULT, TRUE)
					SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				ENDIF
			BREAK
			CASE ciPED_IDLE_ANIM__HANG
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_START_SCENARIO_IN_PLACE)
					TASK_START_SCENARIO_IN_PLACE(tempPed,"WORLD_HUMAN_HANG_OUT_STREET", DEFAULT, TRUE)
					SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				ENDIF
			BREAK
			CASE ciPED_IDLE_ANIM__SMOKEP
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_START_SCENARIO_IN_PLACE)
					TASK_START_SCENARIO_IN_PLACE(tempPed,"WORLD_HUMAN_SMOKING_POT", DEFAULT, TRUE)
					SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				ENDIF
			
			BREAK
			CASE ciPED_IDLE_ANIM__DRINK
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_START_SCENARIO_IN_PLACE)
					TASK_START_SCENARIO_IN_PLACE(tempPed,"WORLD_HUMAN_DRINKING", DEFAULT, TRUE)
					SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				ENDIF
			BREAK	
			
			CASE ciPED_IDLE_ANIM__GUARD_IDLE_A
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_START_SCENARIO_IN_PLACE)
					TASK_START_SCENARIO_IN_PLACE(tempPed,"WORLD_HUMAN_GUARD_STAND", DEFAULT, TRUE)
					SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				ENDIF
			BREAK	
			
			CASE ciPED_IDLE_ANIM__PAPARAZZI
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_START_SCENARIO_IN_PLACE)
					TASK_START_SCENARIO_IN_PLACE(tempPed,"WORLD_HUMAN_PAPARAZZI", DEFAULT, TRUE)
					SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				ENDIF			
			BREAK
			
			CASE ciPED_IDLE_ANIM__SHOW_OFF_VEHICLE_HYDRAULICS
				SET_BIT( iPerformHydraulicsAsPriorityBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed) )
			BREAK
			
			
			CASE ciPED_IDLE_ANIM__WELDING
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_START_SCENARIO_IN_PLACE)
					TASK_START_SCENARIO_IN_PLACE(tempPed,"WORLD_HUMAN_WELDING", DEFAULT, DEFAULT) //this scenario has no entry anim, see B*2120669
					SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				ENDIF
			BREAK	
			
			CASE ciPED_IDLE_ANIM__CLIPBOARD
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PERFORM_SEQUENCE,TRUE)
					REQUEST_ANIM_DICT("missheistdockssetup1ig_9@main_action")
					REQUEST_ANIM_DICT("missheistdockssetup1ig_9@start_idle")
					IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_9@main_action")
					and HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_9@start_idle")
						REQUEST_MODEL(P_CS_Clipboard)
						REQUEST_MODEL(prop_pencil_01)
						IF HAS_MODEL_LOADED(P_CS_Clipboard)
						and HAS_MODEL_LOADED(prop_pencil_01)
							
							OBJECT_INDEX ObjClipboard		
							
							ObjClipboard =  GET_CLOSEST_OBJECT_OF_TYPE(get_entity_coords(tempPed),20,P_CS_Clipboard, false, false, false)					
							
							IF NOT IS_BIT_SET(iPedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							OR not DOES_ENTITY_EXIST(ObjClipboard)
								IF CAN_REGISTER_MISSION_OBJECTS(1)
								AND GET_NUM_RESERVED_MISSION_OBJECTS(TRUE) + 1 < iMaxObjects
									MC_RESERVE_NETWORK_MISSION_OBJECTS(1)
									NETWORK_INDEX netTempObj1
									CREATE_NET_OBJ(netTempObj1,P_CS_Clipboard,get_entity_coords(tempPed),bIsLocalPlayerHost,DEFAULT,TRUE,FALSE)
									ATTACH_ENTITY_TO_ENTITY(NET_TO_ENT(netTempObj1),tempPed, GET_PED_BONE_INDEX(tempPed, BONETAG_PH_L_HAND),<<0,0,0>>,<<0,0,0>>,true,true,true)
								ENDIF
							else
								IF NETWORK_HAS_CONTROL_OF_ENTITY(ObjClipboard)
								AND NOT IS_ENTITY_ATTACHED_TO_ENTITY(ObjClipboard, tempPed)
									ATTACH_ENTITY_TO_ENTITY(ObjClipboard,tempPed, GET_PED_BONE_INDEX(tempPed, BONETAG_PH_L_HAND),<<0,0,0>>,<<0,0,0>>,true,true,true)
								ENDIF
							endif		
							
							OBJECT_INDEX Objpencil
							
							Objpencil =  GET_CLOSEST_OBJECT_OF_TYPE(get_entity_coords(tempPed),20,prop_pencil_01, false, false, false)
							
							IF NOT IS_BIT_SET(iPedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							OR not DOES_ENTITY_EXIST(Objpencil)		
								IF CAN_REGISTER_MISSION_OBJECTS(1)
								AND GET_NUM_RESERVED_MISSION_OBJECTS(TRUE) + 1 < iMaxObjects
									MC_RESERVE_NETWORK_MISSION_OBJECTS(1)
									NETWORK_INDEX netTempObj2
									CREATE_NET_OBJ(netTempObj2,prop_pencil_01,get_entity_coords(tempPed),bIsLocalPlayerHost,DEFAULT,TRUE,FALSE)
									ATTACH_ENTITY_TO_ENTITY(NET_TO_ENT(netTempObj2),tempPed, GET_PED_BONE_INDEX(tempPed, BONETAG_PH_R_HAND),<<0,0,0>>,<<0,0,0>>,true,true,true)
								ENDIF
							else
								IF NETWORK_HAS_CONTROL_OF_ENTITY(Objpencil)
								AND NOT IS_ENTITY_ATTACHED_TO_ENTITY(Objpencil, tempPed)
									ATTACH_ENTITY_TO_ENTITY(Objpencil,tempPed, GET_PED_BONE_INDEX(tempPed, BONETAG_PH_R_HAND),<<0,0,0>>,<<0,0,0>>,true,true,true)
								ENDIF
							endif	
							
							IF (DOES_ENTITY_EXIST(ObjClipboard) AND IS_ENTITY_ATTACHED_TO_ENTITY(ObjClipboard, tempPed))
							AND (DOES_ENTITY_EXIST(Objpencil) AND IS_ENTITY_ATTACHED_TO_ENTITY(Objpencil, tempPed))
								CLEAR_SEQUENCE_TASK(temp_sequence)
								OPEN_SEQUENCE_TASK(temp_sequence)
									TASK_PLAY_ANIM(null,"missheistdockssetup1ig_9@main_action","forklift_supervise_mainaction_supervisor",NORMAL_BLEND_IN,WALK_BLEND_OUT)
									TASK_PLAY_ANIM(null,"missheistdockssetup1ig_9@start_idle","forklift_supervise_idlea_supervisor",NORMAL_BLEND_IN,WALK_BLEND_OUT)
									TASK_PLAY_ANIM(null,"missheistdockssetup1ig_9@start_idle","forklift_supervise_idlebase_supervisor",NORMAL_BLEND_IN,WALK_BLEND_OUT)
									TASK_PLAY_ANIM(null,"missheistdockssetup1ig_9@main_action","forklift_supervise_mainaction_supervisor",NORMAL_BLEND_IN,WALK_BLEND_OUT)
									TASK_PLAY_ANIM(null,"missheistdockssetup1ig_9@start_idle","forklift_supervise_idlea_supervisor",NORMAL_BLEND_IN,WALK_BLEND_OUT,-1,AF_LOOPING)
								CLOSE_SEQUENCE_TASK(temp_sequence)
								TASK_PERFORM_SEQUENCE(tempPed,temp_sequence)			
								SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
								BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
								PRINTLN("[RCC MISSION] scientist with clip board created iped = ", iped)
							ENDIF
						ENDIF					
					ENDIF			
				ENDIF
			BREAK	
			
			
			CASE ciPED_IDLE_ANIM__BRIEFCASE
				
				IF NOT IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM,TRUE)
						REQUEST_ANIM_DICT("anim@heists@biolab@")
						IF HAS_ANIM_DICT_LOADED("anim@heists@biolab@")
							REQUEST_MODEL(PROP_SECURITY_CASE_01)
							IF HAS_MODEL_LOADED(PROP_SECURITY_CASE_01)
								IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niBriefcaseForPedTask)
									IF CREATE_NET_OBJ(niBriefcaseForPedTask,PROP_SECURITY_CASE_01,GET_ENTITY_COORDS(tempPed),bIsLocalPlayerHost)
										ATTACH_ENTITY_TO_ENTITY(NET_TO_ENT(niBriefcaseForPedTask),tempPed, GET_PED_BONE_INDEX(tempPed, BONETAG_PH_R_HAND),<<0,0,0>>,<<0,0,0>>)
										TASK_PLAY_ANIM(tempPed,"anim@heists@biolab@","Contact_Gives_Codes")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						//BROADCAST_PED_ANIM_PLAYING(iped,g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnim)
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
					ENDIF
				ELSE
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niBriefcaseForPedTask) //What if ped control migrates? - send an event to delete the briefcase if it exists
						IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM,TRUE)
							REMOVE_ANIM_DICT("anim@heists@biolab@")
							DELETE_NET_ID(niBriefcaseForPedTask)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,FALSE)
							SET_MODEL_AS_NO_LONGER_NEEDED(PROP_SECURITY_CASE_01)
						ENDIF
					ENDIF
				ENDIF
				
				
			BREAK // ciPED_IDLE_ANIM__BRIEFCASE
			
			CASE ciPED_IDLE_ANIM__SIT_ON_FLOOR	// idle loop
				IF NOT IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				AND NOT IS_BIT_SET(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM,TRUE)
						REQUEST_ANIM_DICT("ANIM@HEISTS@FLEECA_BANK@IG_7_JETSKI_OWNER")
						IF HAS_ANIM_DICT_LOADED("ANIM@HEISTS@FLEECA_BANK@IG_7_JETSKI_OWNER")										
							TASK_PLAY_ANIM(tempPed,"ANIM@HEISTS@FLEECA_BANK@IG_7_JETSKI_OWNER","owner_idle",NORMAL_BLEND_IN,INSTANT_BLEND_OUT,-1,AF_LOOPING|AF_NOT_INTERRUPTABLE)
							CPRINTLN(DEBUG_MISSION,"ciPED_IDLE_ANIM__SIT_ON_FLOOR here 1  ")
							SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
						ENDIF
					ELSE
						REMOVE_ANIM_DICT("ANIM@HEISTS@FLEECA_BANK@IG_7_JETSKI_OWNER")
					ENDIF
				endif	
			BREAK	
			
			CASE ciPED_IDLE_ANIM__FLOOR_STAND_REACT // starts sitting / stands up / do phone task
				IF NOT IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				AND NOT IS_BIT_SET(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					//Don't check if the task status is clear because we'll be transitioning in straight from another anim task!  				
					REQUEST_ANIM_DICT("ANIM@HEISTS@FLEECA_BANK@IG_7_JETSKI_OWNER")
					IF HAS_ANIM_DICT_LOADED("ANIM@HEISTS@FLEECA_BANK@IG_7_JETSKI_OWNER")	
						SET_PED_CONFIG_FLAG(tempPed,PCF_PhoneDisableTextingAnimations,FALSE) 
						SET_PED_CONFIG_FLAG(tempPed,PCF_PhoneDisableTalkingAnimations,FALSE)
						TASK_PLAY_ANIM(tempPed,"ANIM@HEISTS@FLEECA_BANK@IG_7_JETSKI_OWNER","owner_reaction",INSTANT_BLEND_IN,SLOW_BLEND_OUT) 
						CPRINTLN(DEBUG_MISSION,"ciPED_IDLE_ANIM__FLOOR_STAND_REACT here 1  ")
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						SET_BIT(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)), TRUE)
					ENDIF
				ELSE
					REMOVE_ANIM_DICT("ANIM@HEISTS@FLEECA_BANK@IG_7_JETSKI_OWNER")
					
					if bForcingBreakout					
						if IS_ENTITY_PLAYING_ANIM(tempPed,"ANIM@HEISTS@FLEECA_BANK@IG_7_JETSKI_OWNER","owner_reaction")
							if GET_ENTITY_ANIM_CURRENT_TIME(tempPed,"ANIM@HEISTS@FLEECA_BANK@IG_7_JETSKI_OWNER","owner_reaction") > 0.080
								CPRINTLN(DEBUG_MISSION,"ciPED_IDLE_ANIM__FLOOR_STAND_REACT here 1  ")
								STOP_ANIM_TASK(tempPed,"ANIM@HEISTS@FLEECA_BANK@IG_7_JETSKI_OWNER","owner_reaction",SLOW_BLEND_OUT)
								bAnimating = FALSE
							ENDIF
						ENDIF	
					ELSE
						IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM,TRUE)
							bAnimating = FALSE
							REMOVE_ANIM_DICT("ANIM@HEISTS@FLEECA_BANK@IG_7_JETSKI_OWNER")
							SET_MODEL_AS_NO_LONGER_NEEDED(PROP_AMB_PHONE)
							IF NETWORK_HAS_CONTROL_OF_ENTITY(tempPed)	
								IF NOT IS_PED_RUNNING_MOBILE_PHONE_TASK(tempPed)
									TASK_USE_MOBILE_PHONE(tempPed,TRUE)						
									IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niPhoneforPedtask)
										DELETE_NET_ID(niPhoneforPedtask)
									endif
								ENDIF
							ENDIF
						ELSE
							if IS_ENTITY_PLAYING_ANIM(tempPed,"ANIM@HEISTS@FLEECA_BANK@IG_7_JETSKI_OWNER","owner_reaction")
								if GET_ENTITY_ANIM_CURRENT_TIME(tempPed,"ANIM@HEISTS@FLEECA_BANK@IG_7_JETSKI_OWNER","owner_reaction") > 0.33
									REQUEST_MODEL(PROP_AMB_PHONE)
									IF HAS_MODEL_LOADED(PROP_AMB_PHONE)
										IF not NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niPhoneforPedtask) //What if ped control migrates? - send an event to delete the phone if it exists
											IF CREATE_NET_OBJ(niPhoneforPedtask,PROP_AMB_PHONE,GET_ENTITY_COORDS(tempPed),bIsLocalPlayerHost)											
												ATTACH_ENTITY_TO_ENTITY(NET_TO_ENT(niPhoneforPedtask),tempPed, GET_PED_BONE_INDEX(tempPed, BONETAG_PH_R_HAND),<<0,0,0>>,<<0,0,0>>)
												SET_MODEL_AS_NO_LONGER_NEEDED(PROP_AMB_PHONE)
											endif			
										endif
									endif
								endif
							endif
						ENDIF	
					ENDIF
				ENDIF
			BREAK		
			
			CASE ciPED_IDLE_ANIM__PRISONER_BREAKOUT_COWER
				//Only run if the special animation hasn't finished yet
				IF NOT IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				AND NOT IS_BIT_SET(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM,TRUE)
						REQUEST_ANIM_DICT("anim@heists@prison_heist")
						IF HAS_ANIM_DICT_LOADED("anim@heists@prison_heist")
						
							IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
								TASK_PLAY_ANIM(tempPed,"anim@heists@prison_heist","idle_loop",SLOW_BLEND_IN,REALLY_SLOW_BLEND_OUT,-1,AF_LOOPING|AF_NOT_INTERRUPTABLE|AF_ABORT_ON_WEAPON_DAMAGE)
							ELSE
								TASK_PLAY_ANIM(tempPed,"anim@heists@prison_heist","idle_loop",SLOW_BLEND_IN,REALLY_SLOW_BLEND_OUT,-1,AF_LOOPING|AF_NOT_INTERRUPTABLE)
							ENDIF
							SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
						ENDIF
					ELSE
						REMOVE_ANIM_DICT("anim@heists@prison_heist")
					ENDIF
				ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__PRISONER_BREAKOUT_FREED
				IF NOT IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				AND NOT IS_BIT_SET(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					//Don't check if the task status is clear because we'll be transitioning in straight from another anim task!
					REQUEST_ANIM_DICT("anim@heists@prison_heist")
					IF HAS_ANIM_DICT_LOADED("anim@heists@prison_heist")
						TASK_PLAY_ANIM(tempPed,"anim@heists@prison_heist","outro",REALLY_SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_DEFAULT|AF_NOT_INTERRUPTABLE)
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						SET_BIT(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)), TRUE)
					ENDIF
				ELSE
					IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM,TRUE)
						bAnimating = FALSE
						REMOVE_ANIM_DICT("anim@heists@prison_heist")
						IF NOT IS_PED_IN_GROUP(tempPed)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,FALSE)
						ENDIF
					ELSE
						IF GET_SCRIPT_TASK_STATUS(tempPed, SCRIPT_TASK_PLAY_ANIM) = PERFORMING_TASK
							//The animation is really long, break out of it at a decent location to speed up mission flow
							IF IS_ENTITY_PLAYING_ANIM(tempPed,"anim@heists@prison_heist","outro")
								IF (GET_ENTITY_ANIM_CURRENT_TIME(tempPed,"anim@heists@prison_heist","outro") >= 0.577)
									STOP_ANIM_TASK(tempPed,"anim@heists@prison_heist","outro",SLOW_BLEND_OUT)
								ENDIF
							ELIF IS_ENTITY_PLAYING_ANIM(tempPed,"anim@heists@prison_heist","idle_loop")
								PRINTLN("[RCC MISSION] TASK_PERFORM_AMBIENT_ANIMATION - Ped is currently meant to be tasked with Prisoner Breakout Freed, but is still playing the Cower animation...")
								STOP_ANIM_TASK(tempPed,"anim@heists@prison_heist","idle_loop",SLOW_BLEND_OUT)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__GUARD_WAVING_IDLE //Don't break out of the sequence if already started the wave sequence			
	//			IF NOT IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_SYNCHRONIZED_SCENE)
						REQUEST_ANIM_DICT("ANIM@HEISTS@PRISON_HEISTIG1_P1_GUARD_CHECKS_BUS")				
						IF HAS_ANIM_DICT_LOADED("ANIM@HEISTS@PRISON_HEISTIG1_P1_GUARD_CHECKS_BUS")					
							int syncscene
							IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
								syncscene = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_ENTITY_COORDS(tempPed), GET_ENTITY_ROTATION(tempPed), EULER_YXZ, FALSE, TRUE)
							ELSE
								syncscene = NETWORK_CREATE_SYNCHRONISED_SCENE(<<1831.2980,2604.0881,45.8890>>, <<0,0,7.72>>, EULER_YXZ, FALSE, TRUE)
							ENDIF
							
							NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(tempPed,syncscene,"ANIM@HEISTS@PRISON_HEISTIG1_P1_GUARD_CHECKS_BUS","loop",
																	NORMAL_BLEND_IN,NORMAL_BLEND_OUT,SYNCED_SCENE_LOOP_WITHIN_SCENE,RBF_NONE,SLOW_BLEND_IN)
							NETWORK_START_SYNCHRONISED_SCENE(syncscene)
							SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
						ENDIF			
					ENDIF
	//			ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__GUARD_WAVING_WAVE
				IF NOT IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					//Don't check if the task status is clear because we'll be transitioning in straight from another anim task!  
					REQUEST_ANIM_DICT("ANIM@HEISTS@PRISON_HEISTIG1_P1_GUARD_CHECKS_BUS")
					IF HAS_ANIM_DICT_LOADED("ANIM@HEISTS@PRISON_HEISTIG1_P1_GUARD_CHECKS_BUS")
						INT syncscene
						IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
							syncscene = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_ENTITY_COORDS(tempPed), GET_ENTITY_ROTATION(tempPed), EULER_YXZ, TRUE)
						ELSE
							syncscene = NETWORK_CREATE_SYNCHRONISED_SCENE(<<1831.2980,2604.0881,45.8890>>, <<0,0,7.72>>, EULER_YXZ, TRUE)
						ENDIF
						
						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(tempPed,syncscene,"ANIM@HEISTS@PRISON_HEISTIG1_P1_GUARD_CHECKS_BUS","Guard_Checks_Bus",INSTANT_BLEND_IN,WALK_BLEND_OUT)
						NETWORK_START_SYNCHRONISED_SCENE(syncscene)
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
					ENDIF
				ELSE
					INT syncscene			
					
					IF NOT IS_ENTITY_PLAYING_ANIM( tempPed,"ANIM@HEISTS@PRISON_HEISTIG1_P1_GUARD_CHECKS_BUS","Guard_Checks_Bus")
						IF NOT IS_ENTITY_PLAYING_ANIM( tempPed,"ANIM@HEISTS@PRISON_HEISTIG1_P1_GUARD_CHECKS_BUS","loop")
						
							IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
								syncscene = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_ENTITY_COORDS(tempPed), GET_ENTITY_ROTATION(tempPed), EULER_YXZ, FALSE, TRUE)
							ELSE
								syncscene = NETWORK_CREATE_SYNCHRONISED_SCENE(<<1831.2980,2604.0881,45.8890>>, <<0,0,7.72>>, EULER_YXZ, FALSE, TRUE)
							ENDIF
							
							NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(tempPed,syncscene,"ANIM@HEISTS@PRISON_HEISTIG1_P1_GUARD_CHECKS_BUS","loop",
																	INSTANT_BLEND_IN,NORMAL_BLEND_OUT,SYNCED_SCENE_LOOP_WITHIN_SCENE,RBF_NONE,WALK_BLEND_IN)
																	
							NETWORK_START_SYNCHRONISED_SCENE(syncscene)
						ENDIF	
					ELSE
						IF HAS_ENTITY_ANIM_FINISHED(tempPed,"ANIM@HEISTS@PRISON_HEISTIG1_P1_GUARD_CHECKS_BUS","Guard_Checks_Bus")
	//						SET_ENTITY_ANIM_BLEND_OUT_SPEED(tempPed,"Guard_Checks_Bus","ANIM@HEISTS@PRISON_HEISTIG1_P1_GUARD_CHECKS_BUS",INSTANT_BLEND_OUT)

							IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
								syncscene = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_ENTITY_COORDS(tempPed), GET_ENTITY_ROTATION(tempPed), EULER_YXZ,FALSE,TRUE)
							ELSE
								syncscene = NETWORK_CREATE_SYNCHRONISED_SCENE(<<1831.2980,2604.0881,45.8890>>,<<0,0,7.72>>,euler_yxz,false,true)
							ENDIF
							
							NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(tempPed,syncscene,"ANIM@HEISTS@PRISON_HEISTIG1_P1_GUARD_CHECKS_BUS","loop",
																	WALK_BLEND_IN,NORMAL_BLEND_OUT,SYNCED_SCENE_LOOP_WITHIN_SCENE,RBF_NONE,WALK_BLEND_IN)															
							NETWORK_START_SYNCHRONISED_SCENE(syncscene)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(tempped)
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__GUARD_WAVING_REACTINBOOTH
				IF NOT IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					//Don't check if the task status is clear because we'll be transitioning in straight from another anim task!  
					REQUEST_ANIM_DICT("anim@heists@chicken_heist@ig_5_guard_wave_in")
					IF HAS_ANIM_DICT_LOADED("anim@heists@chicken_heist@ig_5_guard_wave_in")
						CLEAR_SEQUENCE_TASK(temp_sequence)
						OPEN_SEQUENCE_TASK(temp_sequence)
							IF ( ABSF(GET_ENTITY_HEADING(tempPed) - (-33.120)) >= 5 )
								TASK_ACHIEVE_HEADING(NULL,-33.120)
							ENDIF
							TASK_PLAY_ANIM(NULL,"anim@heists@chicken_heist@ig_5_guard_wave_in","guard_reaction_in_booth") //should have a location  x: -1134.320, y: -2706.855, z: 13.257 / heading -33.120 ?
							TASK_PLAY_ANIM(NULL,"anim@heists@chicken_heist@ig_5_guard_wave_in","guard_base",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
						CLOSE_SEQUENCE_TASK(temp_sequence)
						TASK_PERFORM_SEQUENCE(tempPed,temp_sequence)
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
					ENDIF
				ELSE
					REMOVE_ANIM_DICT("anim@heists@chicken_heist@ig_5_guard_wave_in")
				ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__SITONBENCH
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_START_SCENARIO_AT_POSITION)
					vPedPos = GET_ENTITY_COORDS(tempPed)				
					IF DOES_SCENARIO_OF_TYPE_EXIST_IN_AREA(vPedPos, "PROP_HUMAN_SEAT_BENCH", cfSIT_RADIUS, FALSE)
						IF NOT IS_SCENARIO_OCCUPIED(vPedPos, cfSIT_RADIUS, FALSE)
							TASK_USE_NEAREST_SCENARIO_TO_COORD(tempPed, vPedPos, cfSIT_RADIUS)
			//				TASK_START_SCENARIO_IN_PLACE(tempPed,"PROP_HUMAN_SEAT_BENCH", DEFAULT, TRUE)
			//				TASK_START_SCENARIO_AT_POSITION(tempPed, "PROP_HUMAN_SEAT_BENCH", vPedPos, fPedHead)
							SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
							PRINTLN("[RCC MISSION] TASK_PERFORM_AMBIENT_ANIMATION, ciPED_IDLE_ANIM__SITONBENCH ")
	//					ELSE
	//						PRINTLN("[RCC MISSION] TASK_PERFORM_AMBIENT_ANIMATION, IS_SCENARIO_OCCUPIED ")
						ENDIF
	//				ELSE
	//					PRINTLN("[RCC MISSION] TASK_PERFORM_AMBIENT_ANIMATION, DOES_SCENARIO_OF_TYPE_EXIST_IN_AREA ")
					ENDIF
				ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__KARENCODES_KARENIDLE
				
				IF NOT IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM,TRUE)
						REQUEST_ANIM_DICT("anim@heists@humane_labs@finale@ext_idles")
						IF HAS_ANIM_DICT_LOADED("anim@heists@humane_labs@finale@ext_idles")
							
							VEHICLE_INDEX tempVeh
							INT iSearchFlags
							iSearchFlags = VEHICLE_SEARCH_FLAG_RETURN_LAW_ENFORCER_VEHICLES + VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES + VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES + VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_GROUP_MEMBERS + VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_PLAYER + VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_DEAD_OR_DYING_PED + VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_SCRIPTED_TASK + VEHICLE_SEARCH_FLAG_ALLOW_LAW_ENFORCER_VEHICLES_WITH_WANTED_LEVEL
							tempVeh = GET_CLOSEST_VEHICLE(GET_ENTITY_COORDS(tempPed),40,DUMMY_MODEL_FOR_SCRIPT,iSearchFlags)
							
							IF DOES_ENTITY_EXIST(tempVeh)
								vCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempVeh,<<1.22,-1.26,0.368>>)
								vRot = <<0,0, (GET_ENTITY_HEADING(tempVeh) - 78.46)>>
								
								PRINTLN("[RCC MISSION] TASK_PERFORM_AMBIENT_ANIMATION - ciPED_IDLE_ANIM__KARENCODES_KARENIDLE called on ped ",iped)
								TASK_PLAY_ANIM_ADVANCED(tempPed,"anim@heists@humane_labs@finale@ext_idles","loop_karen",vCoords,vRot,SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_LOOPING|AF_USE_KINEMATIC_PHYSICS|AF_USE_MOVER_EXTRACTION)
							#IF IS_DEBUG_BUILD
							ELSE
								SCRIPT_ASSERT("Unable to find a car within 40m to perform Keycodes Karen Idle anim with!")
							#ENDIF
							ENDIF
							
							SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
							REMOVE_ANIM_DICT("anim@heists@humane_labs@finale@ext_idles")
						ENDIF
					ENDIF
				ENDIF
				
			BREAK // ciPED_IDLE_ANIM__KARENCODES_KARENIDLE
			
			CASE ciPED_IDLE_ANIM__KARENCODES_DRIVERIDLE
				
				IF NOT IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM,TRUE)
						REQUEST_ANIM_DICT("anim@heists@humane_labs@finale@ext_idles")
						IF HAS_ANIM_DICT_LOADED("anim@heists@humane_labs@finale@ext_idles")
							
							VEHICLE_INDEX tempVeh
							INT iSearchFlags
							iSearchFlags = VEHICLE_SEARCH_FLAG_RETURN_LAW_ENFORCER_VEHICLES + VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES + VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES + VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_GROUP_MEMBERS + VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_PLAYER + VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_DEAD_OR_DYING_PED + VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_SCRIPTED_TASK + VEHICLE_SEARCH_FLAG_ALLOW_LAW_ENFORCER_VEHICLES_WITH_WANTED_LEVEL
							tempVeh = GET_CLOSEST_VEHICLE(GET_ENTITY_COORDS(tempPed),40,DUMMY_MODEL_FOR_SCRIPT,iSearchFlags)
							
							IF DOES_ENTITY_EXIST(tempVeh)
								vCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempVeh,<<-1.22,-0.23,0.498>>)
								vRot = <<0,0, (GET_ENTITY_HEADING(tempVeh) - 93.84)>>
								
								PRINTLN("[RCC MISSION] TASK_PERFORM_AMBIENT_ANIMATION - ciPED_IDLE_ANIM__KARENCODES_DRIVERIDLE called on ped ",iped)
								TASK_PLAY_ANIM_ADVANCED(tempPed,"anim@heists@humane_labs@finale@ext_idles","loop_ped",vCoords,vRot,SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_LOOPING|AF_USE_KINEMATIC_PHYSICS|AF_USE_MOVER_EXTRACTION)
							#IF IS_DEBUG_BUILD
							ELSE
								SCRIPT_ASSERT("Unable to find a car within 40m to perform Keycodes Driver Idle anim with!")
							#ENDIF
							ENDIF
							
							SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
							REMOVE_ANIM_DICT("anim@heists@humane_labs@finale@ext_idles")
						ENDIF
					ENDIF
				ENDIF
				
			BREAK // ciPED_IDLE_ANIM__KARENCODES_DRIVERIDLE
			
			CASE ciPED_IDLE_ANIM__PRISONBREAK_RASHKOVSKY_IDLE
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM)
					REQUEST_ANIM_DICT("anim@heists@prison_heistig_5_p1_rashkovsky_idle")
					IF HAS_ANIM_DICT_LOADED("anim@heists@prison_heistig_5_p1_rashkovsky_idle")					
						FREEZE_ENTITY_POSITION(tempPed,true)
						SET_RAGDOLL_BLOCKING_FLAGS(tempPed,RBF_PED_RAGDOLL_BUMP|RBF_PLAYER_BUMP|RBF_PLAYER_IMPACT|RBF_PLAYER_RAGDOLL_BUMP)
						TASK_PLAY_ANIM(tempPed,"anim@heists@prison_heistig_5_p1_rashkovsky_idle","idle_180",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_LOOPING|AF_NOT_INTERRUPTABLE)
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
					ENDIF
				ELSE
					REMOVE_ANIM_DICT("anim@heists@prison_heistig_5_p1_rashkovsky_idle")
				ENDIF			
			BREAK
			
			CASE ciPED_IDLE_ANIM__PRISONPLANE_AGENT_A_IDLE
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
					REQUEST_ANIM_DICT("anim@heists@prison_heist")
					IF HAS_ANIM_DICT_LOADED("anim@heists@prison_heist")
						TASK_PLAY_ANIM(tempPed,"anim@heists@prison_heist","ped_a_loop_a",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
					ENDIF
				ELSE
					REMOVE_ANIM_DICT("anim@heists@prison_heist")
				ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__PRISONPLANE_AGENT_A_REACT
				IF NOT IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					//Don't check if the task status is clear because we'll be transitioning in straight from another anim task!  
					REQUEST_ANIM_DICT("anim@heists@prison_heist")
					IF HAS_ANIM_DICT_LOADED("anim@heists@prison_heist")
						CLEAR_SEQUENCE_TASK(temp_sequence)
						OPEN_SEQUENCE_TASK(temp_sequence)
							TASK_PLAY_ANIM(NULL,"anim@heists@prison_heist","ped_a_react")
							TASK_PLAY_ANIM(NULL,"anim@heists@prison_heist","ped_a_loop_b",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING|AF_EXIT_AFTER_INTERRUPTED)
						CLOSE_SEQUENCE_TASK(temp_sequence)
						TASK_PERFORM_SEQUENCE(tempPed,temp_sequence)
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
					ENDIF
				ELSE
					REMOVE_ANIM_DICT("anim@heists@prison_heist")
				ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__PRISONPLANE_AGENT_B_IDLE
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM,TRUE)
				AND NOT IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					REQUEST_ANIM_DICT("anim@heists@prison_heist")
					IF HAS_ANIM_DICT_LOADED("anim@heists@prison_heist")
						VEHICLE_INDEX tempveh
						tempveh = GET_CLOSEST_VEHICLE(<<-966.93097, -2976.15479, 13.94617>>,10,FBI,VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES)
	//					IF DOES_ENTITY_EXIST(tempveh)
	//						SET_ENTITY_NO_COLLISION_ENTITY(tempped,tempveh,TRUE)						
	//					ENDIF
						VECTOR v_Offset
						v_Offset = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempveh,<<0,2.475,0>>)
						SET_ENTITY_COORDS_NO_OFFSET(tempPed,v_Offset)
						
						VECTOR newOffset, newPos
						newOffset = <<0.0,0.0,0.37>>
						newPos = v_Offset + newOffset
											
						//TASK_PLAY_ANIM(tempPed,"anim@heists@prison_heist","ped_b_loop_a",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING|AF_USE_KINEMATIC_PHYSICS|AF_USE_MOVER_EXTRACTION)
						TASK_PLAY_ANIM_ADVANCED(tempPed,"anim@heists@prison_heist","ped_b_loop_a", newPos, GET_ENTITY_ROTATION(tempPed), NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING|AF_USE_KINEMATIC_PHYSICS|AF_USE_MOVER_EXTRACTION|AF_EXIT_AFTER_INTERRUPTED, 0, EULER_YXZ, AIK_DISABLE_LEG_IK)
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
					ENDIF
				ELSE
					REMOVE_ANIM_DICT("anim@heists@prison_heist")
				ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__PRISONPLANE_AGENT_B_REACT
				IF NOT IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					//Don't check if the task status is clear because we'll be transitioning in straight from another anim task!
					REQUEST_ANIM_DICT("anim@heists@prison_heist")
					IF HAS_ANIM_DICT_LOADED("anim@heists@prison_heist")
						CLEAR_SEQUENCE_TASK(temp_sequence)
						OPEN_SEQUENCE_TASK(temp_sequence)
							TASK_PLAY_ANIM(NULL,"anim@heists@prison_heist","ped_b_react",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_USE_KINEMATIC_PHYSICS|AF_USE_MOVER_EXTRACTION|AF_EXIT_AFTER_INTERRUPTED)
							TASK_PLAY_ANIM(NULL,"anim@heists@prison_heist","ped_b_loop_b",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING|AF_USE_KINEMATIC_PHYSICS|AF_USE_MOVER_EXTRACTION|AF_EXIT_AFTER_INTERRUPTED)
						CLOSE_SEQUENCE_TASK(temp_sequence)
						TASK_PERFORM_SEQUENCE(tempPed,temp_sequence)
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
					ENDIF
				ELSE
					REMOVE_ANIM_DICT("anim@heists@prison_heist")
				ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__PRISONBREAK_BUS_GUARD_A_IDLE_CHAT
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
				AND NOT IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					REQUEST_ANIM_DICT("anim@heists@prison_heistig_2_p1_exit_bus")
					IF HAS_ANIM_DICT_LOADED("anim@heists@prison_heistig_2_p1_exit_bus")
						TASK_PLAY_ANIM(tempPed,"anim@heists@prison_heistig_2_p1_exit_bus","loop_a_guard_a",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
					ENDIF
				ELSE
					REMOVE_ANIM_DICT("anim@heists@prison_heistig_2_p1_exit_bus")
				ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__PRISONBREAK_BUS_GUARD_A_REACT_TO_BUS
				IF NOT IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					//Don't check if the task status is clear because we'll be transitioning in straight from another anim task!  
					REQUEST_ANIM_DICT("anim@heists@prison_heistig_2_p1_exit_bus")
					IF HAS_ANIM_DICT_LOADED("anim@heists@prison_heistig_2_p1_exit_bus")
						CLEAR_SEQUENCE_TASK(temp_sequence)
						OPEN_SEQUENCE_TASK(temp_sequence)
							TASK_PLAY_ANIM(NULL,"anim@heists@prison_heistig_2_p1_exit_bus","exit_bus_guard_a")
							TASK_PLAY_ANIM(NULL,"anim@heists@prison_heistig_2_p1_exit_bus","loop_b_guard_a",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
						CLOSE_SEQUENCE_TASK(temp_sequence)
						TASK_PERFORM_SEQUENCE(tempPed,temp_sequence)
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
					ENDIF
				ELSE
					REMOVE_ANIM_DICT("anim@heists@prison_heistig_2_p1_exit_bus")
				ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__PRISONBREAK_BUS_GUARD_B_IDLE_CHAT
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
					REQUEST_ANIM_DICT("anim@heists@prison_heistig_2_p1_exit_bus")
					IF HAS_ANIM_DICT_LOADED("anim@heists@prison_heistig_2_p1_exit_bus")
						TASK_PLAY_ANIM(tempPed,"anim@heists@prison_heistig_2_p1_exit_bus","loop_a_guard_b",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
					ENDIF
				ELSE
					REMOVE_ANIM_DICT("anim@heists@prison_heistig_2_p1_exit_bus")
				ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__WITSEC_CHAIR_SNOOZE
				IF NOT IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				AND NOT IS_BIT_SET(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_SYNCHRONIZED_SCENE, TRUE)
						IF (CAN_REGISTER_MISSION_OBJECTS(1)
						AND GET_NUM_RESERVED_MISSION_OBJECTS(TRUE) + 1 < iMaxObjects)
						OR IS_BIT_SET(iPedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						
							REQUEST_ANIM_DICT("anim@heists@ornate_bank@witsec@jetski_owner")
							REQUEST_MODEL(HEI_PROP_HEI_SKID_CHAIR)
							
							IF 	HAS_ANIM_DICT_LOADED("anim@heists@ornate_bank@witsec@jetski_owner")
							AND HAS_MODEL_LOADED(HEI_PROP_HEI_SKID_CHAIR)
								
								vCoords = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vPos							
								fHeading = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fHead + 90
								
								INT iSynchScene
								
								iSynchScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCoords,<<0,0,fHeading>>,EULER_XYZ,FALSE,TRUE)							
								//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, TRUE)
								//unlock events but use SYNCED_SCENE_DONT_INTERRUPT, so that events are unblocked when ramming into the chair, see B*2206754
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, FALSE)
								NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(tempPed,iSynchScene,"anim@heists@ornate_bank@witsec@jetski_owner","idle",INSTANT_BLEND_IN,INSTANT_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON|SYNCED_SCENE_ON_ABORT_STOP_SCENE|SYNCED_SCENE_DONT_INTERRUPT)
								
								OBJECT_INDEX ObjChair		
								
								IF IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
									ObjChair = GET_CLOSEST_OBJECT_OF_TYPE(vCoords,20,HEI_PROP_HEI_SKID_CHAIR, false, false, false)
								ENDIF
								
								IF NOT IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
								OR NOT DOES_ENTITY_EXIST(objChair)
									IF CAN_REGISTER_MISSION_OBJECTS(1)
									AND GET_NUM_RESERVED_MISSION_OBJECTS(TRUE) + 1 < iMaxObjects
										MC_RESERVE_NETWORK_MISSION_OBJECTS(1)
										NETWORK_INDEX netChair
										CREATE_NET_OBJ(netChair,HEI_PROP_HEI_SKID_CHAIR,vCoords,bIsLocalPlayerHost,DEFAULT,TRUE,FALSE)
										NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_ENT(netChair),iSynchScene,"anim@heists@ornate_bank@witsec@jetski_owner","idle_chair",INSTANT_BLEND_IN,INSTANT_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_ON_ABORT_STOP_SCENE|SYNCED_SCENE_VEHICLE_ABORT_ON_LARGE_IMPACT)
									ENDIF
								else
									NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(ObjChair,iSynchScene,"anim@heists@ornate_bank@witsec@jetski_owner","idle_chair",NORMAL_BLEND_IN,INSTANT_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_ON_ABORT_STOP_SCENE|SYNCED_SCENE_VEHICLE_ABORT_ON_LARGE_IMPACT)
								endif
								
								NETWORK_START_SYNCHRONISED_SCENE(iSynchScene)							
								SET_MODEL_AS_NO_LONGER_NEEDED(HEI_PROP_HEI_SKID_CHAIR)
							ENDIF
						ENDIF
					ELSE
						// Might just need to set iPedPerformingSpecialAnimBS instead. url:bugstar:4632551
						IF NOT IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
							REMOVE_ANIM_DICT("anim@heists@ornate_bank@witsec@jetski_owner")
						ENDIF
					ENDIF
				ELSE
					REMOVE_ANIM_DICT("anim@heists@ornate_bank@witsec@jetski_owner")
				ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__WITSEC_CHAIR_STAND_UP
				IF NOT IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				AND NOT IS_BIT_SET(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					REQUEST_ANIM_DICT("anim@heists@ornate_bank@witsec@jetski_owner")
					IF HAS_ANIM_DICT_LOADED("anim@heists@ornate_bank@witsec@jetski_owner")
						
						vCoords = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vPos
						fHeading = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fHead + 90
						
						INT iSynchScene
						
						iSynchScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCoords,<<0,0,fHeading>>)
						
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, TRUE)
						
						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(tempPed,iSynchScene,"anim@heists@ornate_bank@witsec@jetski_owner","exit",INSTANT_BLEND_IN,SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON|SYNCED_SCENE_ON_ABORT_STOP_SCENE|SYNCED_SCENE_DONT_INTERRUPT)
						
						OBJECT_INDEX oiChair
						oiChair = GET_CLOSEST_OBJECT_OF_TYPE(vCoords,10,HEI_PROP_HEI_SKID_CHAIR, false, false, false)
						
						IF DOES_ENTITY_EXIST(oiChair)
							NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(oiChair,iSynchScene,"anim@heists@ornate_bank@witsec@jetski_owner","exit_chair",INSTANT_BLEND_IN,SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_ON_ABORT_STOP_SCENE|SYNCED_SCENE_VEHICLE_ABORT_ON_LARGE_IMPACT)
						ENDIF
						
						NETWORK_START_SYNCHRONISED_SCENE(iSynchScene)
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						SET_BIT(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)), TRUE)
						
						IF DOES_ENTITY_EXIST(oiChair)
							SET_OBJECT_AS_NO_LONGER_NEEDED(oiChair)
						ENDIF
						
					ENDIF
				ELSE
					REMOVE_ANIM_DICT("anim@heists@ornate_bank@witsec@jetski_owner")
					IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_SYNCHRONIZED_SCENE, TRUE)
						bAnimating = FALSE
					ENDIF
				ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__WITSEC_CHAIR_FALL
				IF NOT IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				AND NOT IS_BIT_SET(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					REQUEST_ANIM_DICT("anim@heists@ornate_bank@witsec@jetski_owner")
					IF HAS_ANIM_DICT_LOADED("anim@heists@ornate_bank@witsec@jetski_owner")
					
						vCoords = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vPos
						fHeading = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fHead + 90
						
						INT iSynchScene
						
						iSynchScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCoords,<<0,0,fHeading>>)
						
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, TRUE)
						
						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(tempPed,iSynchScene,"anim@heists@ornate_bank@witsec@jetski_owner","react_fall",INSTANT_BLEND_IN,SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON)
						
						OBJECT_INDEX oiChair
						oiChair = GET_CLOSEST_OBJECT_OF_TYPE(vCoords,10,HEI_PROP_HEI_SKID_CHAIR, false, false, false)
						
						IF DOES_ENTITY_EXIST(oiChair)
							NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(oiChair,iSynchScene,"anim@heists@ornate_bank@witsec@jetski_owner","react_fall_chair",INSTANT_BLEND_IN,SLOW_BLEND_OUT)
						ENDIF
						
						NETWORK_START_SYNCHRONISED_SCENE(iSynchScene)
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						SET_BIT(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)), TRUE)
						
						IF DOES_ENTITY_EXIST(oiChair)
							SET_OBJECT_AS_NO_LONGER_NEEDED(oiChair)
						ENDIF
						
					ENDIF
				ELSE
					REMOVE_ANIM_DICT("anim@heists@ornate_bank@witsec@jetski_owner")
					IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_SYNCHRONIZED_SCENE, TRUE)
						bAnimating = FALSE
					ENDIF
				ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__UNFSHDBIZ_JUDGE_IDLE
				IF NOT IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				AND NOT IS_BIT_SET(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				AND SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
					IF CAN_REGISTER_MISSION_OBJECTS(1)
					OR IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						REQUEST_ANIM_DICT("anim@heists@prison_heistunfinished_biztarget_idle")
						REQUEST_MODEL(PROP_CS_TABLET)
						IF HAS_ANIM_DICT_LOADED("anim@heists@prison_heistunfinished_biztarget_idle")
						AND HAS_MODEL_LOADED(PROP_CS_TABLET)
						
							vCoords = <<-1806.5146, 427.6125, 131.7062>>
							vRot = <<0,0,3.63>>
							
							//vCoords.x -= 0.14
							vRot.z -= 5.1
							
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, TRUE)
							
							SET_ENTITY_COORDS(tempPed,vCoords)
							
							TASK_PLAY_ANIM_ADVANCED(tempPed,"anim@heists@prison_heistunfinished_biztarget_idle","target_idle",vCoords,vRot,NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING|AF_EXTRACT_INITIAL_OFFSET,0,EULER_YXZ)
							
							OBJECT_INDEX oiTablet
							
							IF IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
								oiTablet = GET_CLOSEST_OBJECT_OF_TYPE(vCoords, 10, PROP_CS_TABLET, false, false, false)
							ENDIF
							
							IF NOT IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							OR NOT DOES_ENTITY_EXIST(oiTablet)
								IF CAN_REGISTER_MISSION_OBJECTS(1)
								AND GET_NUM_RESERVED_MISSION_OBJECTS(TRUE) + 1 < iMaxObjects
								
									MC_RESERVE_NETWORK_MISSION_OBJECTS(1)
									
									NETWORK_INDEX netTablet
									
									CREATE_NET_OBJ(netTablet,PROP_CS_TABLET,vCoords,bIsLocalPlayerHost,DEFAULT,TRUE,FALSE)
									
									ATTACH_ENTITY_TO_ENTITY(NET_TO_ENT(netTablet),tempPed,GET_PED_BONE_INDEX(tempPed,BONETAG_PH_L_HAND),<<0,0,0>>,<<0,0,0>>,TRUE,TRUE)
								ENDIF
							ELSE
								IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(oiTablet, tempPed)
									ATTACH_ENTITY_TO_ENTITY(oiTablet,tempPed,GET_PED_BONE_INDEX(tempPed,BONETAG_PH_L_HAND),<<0,0,0>>,<<0,0,0>>,TRUE,TRUE)
								ENDIF
							ENDIF
							
							SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
							
							SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CS_TABLET)
						ENDIF
					ENDIF
				ELSE
					REMOVE_ANIM_DICT("anim@heists@prison_heistunfinished_biztarget_idle")
				ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__UNFSHDBIZ_JUDGE_BREAKOUT
				IF NOT IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				AND NOT IS_BIT_SET(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					
					REQUEST_ANIM_DICT("anim@heists@prison_heistunfinished_biztarget_idle")
					REQUEST_ANIM_DICT("anim@heists@prison_heistunfinished_biz@popov_react")
					
					IF HAS_ANIM_DICT_LOADED("anim@heists@prison_heistunfinished_biztarget_idle")
					AND HAS_ANIM_DICT_LOADED("anim@heists@prison_heistunfinished_biz@popov_react")
					
						vCoords = <<-1806.5146, 427.6125, 131.7062>>
						
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, TRUE)
						
						FLOAT fDistance2ToPlayer
						fDistance2ToPlayer = 999999999
						BOOL bInAngledArea, bInAngledArea_RoofLeftOfPopov
						
						PLAYER_INDEX tempPlayer
						tempPlayer = INT_TO_PLAYERINDEX(GET_NEAREST_PLAYER_TO_ENTITY(tempPed))
						
						IF (tempPlayer != INVALID_PLAYER_INDEX())
						AND IS_NET_PLAYER_OK(tempPlayer)
						AND NETWORK_IS_PLAYER_A_PARTICIPANT(tempPlayer)
							fDistance2ToPlayer = VDIST2(GET_PLAYER_COORDS(tempPlayer), vCoords)
							
							IF fDistance2ToPlayer < 50
								PED_INDEX playerPed
								playerPed = GET_PLAYER_PED(tempPlayer)
								
								bInAngledArea = IS_ENTITY_IN_ANGLED_AREA(playerPed, <<-1823.6, 420.9, 126.7>>, <<-1790.4, 418.6, 141.6>>, 15.75)
								
								IF bInAngledArea
									bInAngledArea_RoofLeftOfPopov = IS_ENTITY_IN_ANGLED_AREA(playerPed, <<-1820.3, 425.7, 130.8>>, <<-1806.5, 424.8, 138.4>>, 7)
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT bInAngledArea
						OR bInAngledArea_RoofLeftOfPopov // Player isn't actually blocking Popov's exit route, just run off
							strAnimDict = "anim@heists@prison_heistunfinished_biztarget_idle"
							strAnimName = "target_breakout"
						ELSE
							strAnimDict = "anim@heists@prison_heistunfinished_biz@popov_react"
							strAnimName = "popov_react"
						ENDIF
						
						TASK_PLAY_ANIM(tempPed, strAnimDict, strAnimName, NORMAL_BLEND_IN, WALK_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE, 0, FALSE)
						
						IF bInAngledArea
						OR fDistance2ToPlayer < 100 // Less than 10 metres away
							TASK_LOOK_AT_ENTITY(tempPed, LocalPlayerPed, 2000)
						ENDIF
						
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						SET_BIT(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)), TRUE)
						DEBUG_PRINTCALLSTACK()
					ENDIF
				ELSE
					REMOVE_ANIM_DICT("anim@heists@prison_heistunfinished_biztarget_idle")
					REMOVE_ANIM_DICT("anim@heists@prison_heistunfinished_biz@popov_react")
					IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
						bAnimating = FALSE
					ENDIF
				ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__BROKEN_CAR
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
					REQUEST_ANIM_DICT("anim@heists@narcotics@funding@biker_loop")
					IF HAS_ANIM_DICT_LOADED("anim@heists@narcotics@funding@biker_loop")		
						TASK_PLAY_ANIM(tempPed,"anim@heists@narcotics@funding@biker_loop","biker_combined_loop",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1)
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
					ENDIF
				ELSE
					REMOVE_ANIM_DICT("anim@heists@narcotics@funding@biker_loop")
				ENDIF
			BREAK			
			
			CASE ciPED_IDLE_ANIM__DOOR_KNOCK
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
					REQUEST_ANIM_DICT("anim@heists@narcotics@funding@horny_biker")
					IF HAS_ANIM_DICT_LOADED("anim@heists@narcotics@funding@horny_biker")										
						TASK_PLAY_ANIM(tempPed,"anim@heists@narcotics@funding@horny_biker","horny_biker_loop",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)	
						IF NOT IS_BIT_SET(iLocalBoolCheck9,LBOOL9_KNOCKING_QUIETED)
							BROADCAST_HEIST_QUIET_KNOCKING(ALL_PLAYERS(),TRUE)
						ENDIF
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
					ENDIF
				ELSE
					REMOVE_ANIM_DICT("anim@heists@narcotics@funding@horny_biker")
				ENDIF	
			BREAK
			
			CASE ciPED_IDLE_ANIM__SIT_DOWN_DRINKING
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
									
					REQUEST_ANIM_DICT("anim@heists@narcotics@funding@gang_chat")
					request_model(prop_cs_beer_bot_01)				
					IF CAN_REGISTER_MISSION_OBJECTS(1)
					AND GET_NUM_RESERVED_MISSION_OBJECTS(TRUE) + 1 < iMaxObjects
						IF HAS_ANIM_DICT_LOADED("anim@heists@narcotics@funding@gang_chat")
						and HAS_MODEL_LOADED(prop_cs_beer_bot_01)
						
							SET_CURRENT_PED_WEAPON(tempPed,WEAPONTYPE_UNARMED,true)			
							
							OBJECT_INDEX Objbottle		
							
							IF IS_BIT_SET(iPedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
								Objbottle = GET_CLOSEST_OBJECT_OF_TYPE(get_entity_coords(tempPed),20,prop_cs_beer_bot_01, false, false, false)
							ENDIF
							
							IF NOT IS_BIT_SET(iPedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							OR not DOES_ENTITY_EXIST(Objbottle)
								MC_RESERVE_NETWORK_MISSION_OBJECTS(1)
								NETWORK_INDEX netTempObj
								IF CREATE_NET_OBJ(netTempObj,prop_cs_beer_bot_01,get_entity_coords(tempPed),bIsLocalPlayerHost,DEFAULT,TRUE,FALSE)
									ATTACH_ENTITY_TO_ENTITY(net_to_ent(netTempObj),tempPed,GET_PED_BONE_INDEX(tempPed,BONETAG_PH_R_HAND),<<0,0,0>>,<<0,0,0>>,TRUE,TRUE)
								ENDIF
							else
								ATTACH_ENTITY_TO_ENTITY(Objbottle,tempPed,GET_PED_BONE_INDEX(tempPed,BONETAG_PH_R_HAND),<<0,0,0>>,<<0,0,0>>,TRUE,TRUE)
							endif
							
							VECTOR vCreatorPosition, vAnimPosition, vAnimRotation
							
							vCreatorPosition  	= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vPos
							vAnimRotation		= << 0.0, 0.0, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fHead >>
							vAnimPosition 		= vCreatorPosition - << 0.664001, 0.302979, 0.00200272 >> //apply constant offset, because animation asset has offset
							
							PRINTLN("Tasking ped ", iped, " with placed ped position ", vCreatorPosition, " to play animation ciPED_IDLE_ANIM__SIT_DOWN_DRINKING at position ", vAnimPosition, " and rotation ", vAnimRotation)
									
							TASK_PLAY_ANIM_ADVANCED(tempPed,"anim@heists@narcotics@funding@gang_chat","gang_chatting_combined",vAnimPosition, vAnimRotation,INSTANT_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_EXTRACT_INITIAL_OFFSET|AF_OVERRIDE_PHYSICS|AF_LOOPING, DEFAULT, DEFAULT, AIK_DISABLE_LEG_IK)
							
							SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
							
							SET_MODEL_AS_NO_LONGER_NEEDED(prop_cs_beer_bot_01)
						ENDIF
					ENDIF
				ELSE
					REMOVE_ANIM_DICT("anim@heists@narcotics@funding@gang_chat")
				ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__BIKE_INSPECTION
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
					REQUEST_ANIM_DICT("anim@heists@narcotics@funding@gang_idle")
					IF HAS_ANIM_DICT_LOADED("anim@heists@narcotics@funding@gang_idle")										
						TASK_PLAY_ANIM(tempPed,"anim@heists@narcotics@funding@gang_idle","gang_chatting_idle01",NORMAL_BLEND_IN,SLOW_BLEND_OUT,-1,AF_LOOPING)						
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
					ENDIF
				ELSE
					REMOVE_ANIM_DICT("anim@heists@narcotics@funding@gang_idle")
				ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__HOSTAGE_1
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
					REQUEST_ANIM_DICT( GET_CROWD_CONTROL_PED_ANIM_DICTIONARY( CCPANIM_COWER_LOOP, CCPED_CUSTOMER_A, CCHEIST_CUSTOM_ORNATE ) )
					IF HAS_ANIM_DICT_LOADED( GET_CROWD_CONTROL_PED_ANIM_DICTIONARY( CCPANIM_COWER_LOOP, CCPED_CUSTOMER_A, CCHEIST_CUSTOM_ORNATE ) )
						
						IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
							TASK_PLAY_ANIM(tempPed,
								GET_CROWD_CONTROL_PED_ANIM_DICTIONARY( CCPANIM_COWER_LOOP, CCPED_CUSTOMER_A, CCHEIST_CUSTOM_ORNATE ),
								GET_CROWD_CONTROL_PED_ANIM_NAME( CCPANIM_COWER_LOOP, CCPED_CUSTOMER_A, CCHEIST_CUSTOM_ORNATE ),
								REALLY_SLOW_BLEND_IN,REALLY_SLOW_BLEND_OUT,-1,AF_LOOPING|AF_NOT_INTERRUPTABLE|AF_USE_KINEMATIC_PHYSICS|AF_USE_MOVER_EXTRACTION|AF_ABORT_ON_WEAPON_DAMAGE)
						ELSE
							TASK_PLAY_ANIM(tempPed,
								GET_CROWD_CONTROL_PED_ANIM_DICTIONARY( CCPANIM_COWER_LOOP, CCPED_CUSTOMER_A, CCHEIST_CUSTOM_ORNATE ),
								GET_CROWD_CONTROL_PED_ANIM_NAME( CCPANIM_COWER_LOOP, CCPED_CUSTOMER_A, CCHEIST_CUSTOM_ORNATE ),
								REALLY_SLOW_BLEND_IN,REALLY_SLOW_BLEND_OUT,-1,AF_LOOPING|AF_NOT_INTERRUPTABLE|AF_USE_KINEMATIC_PHYSICS|AF_USE_MOVER_EXTRACTION)
						ENDIF
						
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
					ENDIF
				ELSE
					REMOVE_ANIM_DICT( GET_CROWD_CONTROL_PED_ANIM_DICTIONARY( CCPANIM_COWER_LOOP, CCPED_CUSTOMER_A, CCHEIST_CUSTOM_ORNATE ) )
				ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__HOSTAGE_2
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
					REQUEST_ANIM_DICT( GET_CROWD_CONTROL_PED_ANIM_DICTIONARY( CCPANIM_COWER_LOOP, CCPED_CUSTOMER_C, CCHEIST_CUSTOM_ORNATE ) )
					IF HAS_ANIM_DICT_LOADED( GET_CROWD_CONTROL_PED_ANIM_DICTIONARY( CCPANIM_COWER_LOOP, CCPED_CUSTOMER_C, CCHEIST_CUSTOM_ORNATE ) )
						
						IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
							TASK_PLAY_ANIM(tempPed,
								GET_CROWD_CONTROL_PED_ANIM_DICTIONARY( CCPANIM_COWER_LOOP, CCPED_CUSTOMER_C, CCHEIST_CUSTOM_ORNATE ),
								GET_CROWD_CONTROL_PED_ANIM_NAME( CCPANIM_COWER_LOOP, CCPED_CUSTOMER_C, CCHEIST_CUSTOM_ORNATE ),
								REALLY_SLOW_BLEND_IN,REALLY_SLOW_BLEND_OUT,-1,AF_LOOPING|AF_NOT_INTERRUPTABLE|AF_USE_KINEMATIC_PHYSICS|AF_USE_MOVER_EXTRACTION|AF_ABORT_ON_WEAPON_DAMAGE)
						ELSE
							TASK_PLAY_ANIM(tempPed,
								GET_CROWD_CONTROL_PED_ANIM_DICTIONARY( CCPANIM_COWER_LOOP, CCPED_CUSTOMER_C, CCHEIST_CUSTOM_ORNATE ),
								GET_CROWD_CONTROL_PED_ANIM_NAME( CCPANIM_COWER_LOOP, CCPED_CUSTOMER_C, CCHEIST_CUSTOM_ORNATE ),
								REALLY_SLOW_BLEND_IN,REALLY_SLOW_BLEND_OUT,-1,AF_LOOPING|AF_NOT_INTERRUPTABLE|AF_USE_KINEMATIC_PHYSICS|AF_USE_MOVER_EXTRACTION)
						ENDIF
						
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
					ENDIF
				ELSE
					REMOVE_ANIM_DICT( GET_CROWD_CONTROL_PED_ANIM_DICTIONARY( CCPANIM_COWER_LOOP, CCPED_CUSTOMER_C, CCHEIST_CUSTOM_ORNATE ) )
				ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__HOSTAGE_3
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
					REQUEST_ANIM_DICT( GET_CROWD_CONTROL_PED_ANIM_DICTIONARY( CCPANIM_COWER_LOOP, CCPED_CASHIER_A, CCHEIST_CUSTOM_ORNATE ) )
					IF HAS_ANIM_DICT_LOADED( GET_CROWD_CONTROL_PED_ANIM_DICTIONARY( CCPANIM_COWER_LOOP, CCPED_CASHIER_A, CCHEIST_CUSTOM_ORNATE ) )
						
						IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
							TASK_PLAY_ANIM(tempPed,
								GET_CROWD_CONTROL_PED_ANIM_DICTIONARY( CCPANIM_COWER_LOOP, CCPED_CASHIER_A, CCHEIST_CUSTOM_ORNATE ),
								GET_CROWD_CONTROL_PED_ANIM_NAME( CCPANIM_COWER_LOOP, CCPED_CASHIER_A, CCHEIST_CUSTOM_ORNATE ),
								REALLY_SLOW_BLEND_IN,REALLY_SLOW_BLEND_OUT,-1,AF_LOOPING|AF_NOT_INTERRUPTABLE|AF_USE_KINEMATIC_PHYSICS|AF_USE_MOVER_EXTRACTION|AF_ABORT_ON_WEAPON_DAMAGE)
						ELSE
							TASK_PLAY_ANIM(tempPed,
								GET_CROWD_CONTROL_PED_ANIM_DICTIONARY( CCPANIM_COWER_LOOP, CCPED_CASHIER_A, CCHEIST_CUSTOM_ORNATE ),
								GET_CROWD_CONTROL_PED_ANIM_NAME( CCPANIM_COWER_LOOP, CCPED_CASHIER_A, CCHEIST_CUSTOM_ORNATE ),
								REALLY_SLOW_BLEND_IN,REALLY_SLOW_BLEND_OUT,-1,AF_LOOPING|AF_NOT_INTERRUPTABLE|AF_USE_KINEMATIC_PHYSICS|AF_USE_MOVER_EXTRACTION)
						ENDIF
						
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
					ENDIF
				ELSE
					REMOVE_ANIM_DICT( GET_CROWD_CONTROL_PED_ANIM_DICTIONARY( CCPANIM_COWER_LOOP, CCPED_CASHIER_A, CCHEIST_CUSTOM_ORNATE ) )
				ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__HOSTAGE_4
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
					REQUEST_ANIM_DICT( GET_CROWD_CONTROL_PED_ANIM_DICTIONARY( CCPANIM_COWER_LOOP, CCPED_CUSTOMER_E, CCHEIST_CUSTOM_ORNATE ) )
					IF HAS_ANIM_DICT_LOADED( GET_CROWD_CONTROL_PED_ANIM_DICTIONARY( CCPANIM_COWER_LOOP, CCPED_CUSTOMER_E, CCHEIST_CUSTOM_ORNATE ) )
						
						IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
							TASK_PLAY_ANIM(tempPed,
								GET_CROWD_CONTROL_PED_ANIM_DICTIONARY( CCPANIM_COWER_LOOP, CCPED_CUSTOMER_E, CCHEIST_CUSTOM_ORNATE ),
								GET_CROWD_CONTROL_PED_ANIM_NAME( CCPANIM_COWER_LOOP, CCPED_CUSTOMER_E, CCHEIST_CUSTOM_ORNATE ),
								REALLY_SLOW_BLEND_IN,REALLY_SLOW_BLEND_OUT,-1,AF_LOOPING|AF_NOT_INTERRUPTABLE|AF_USE_KINEMATIC_PHYSICS|AF_USE_MOVER_EXTRACTION|AF_ABORT_ON_WEAPON_DAMAGE)
						ELSE
							TASK_PLAY_ANIM(tempPed,
								GET_CROWD_CONTROL_PED_ANIM_DICTIONARY( CCPANIM_COWER_LOOP, CCPED_CUSTOMER_E, CCHEIST_CUSTOM_ORNATE ),
								GET_CROWD_CONTROL_PED_ANIM_NAME( CCPANIM_COWER_LOOP, CCPED_CUSTOMER_E, CCHEIST_CUSTOM_ORNATE ),
								REALLY_SLOW_BLEND_IN,REALLY_SLOW_BLEND_OUT,-1,AF_LOOPING|AF_NOT_INTERRUPTABLE|AF_USE_KINEMATIC_PHYSICS|AF_USE_MOVER_EXTRACTION)
						ENDIF

						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
					ENDIF
				ELSE
					REMOVE_ANIM_DICT( GET_CROWD_CONTROL_PED_ANIM_DICTIONARY( CCPANIM_COWER_LOOP, CCPED_CUSTOMER_E, CCHEIST_CUSTOM_ORNATE ) )
				ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__HEIST_TUT_LESTER_PHONE_INTRO
				IF NOT IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				AND NOT IS_BIT_SET(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PERFORM_SEQUENCE, TRUE)
						REQUEST_ANIM_DICT("ANIM@HEISTS@FLEECA_BANK@SCOPE_OUT@LESTER_IN_CAR")
						REQUEST_MODEL(PROP_AMB_PHONE)
						
						IF HAS_ANIM_DICT_LOADED("ANIM@HEISTS@FLEECA_BANK@SCOPE_OUT@LESTER_IN_CAR")
						AND HAS_MODEL_LOADED(PROP_AMB_PHONE)
							VEHICLE_INDEX vehLesterCar
							
							IF IS_PED_IN_ANY_VEHICLE(tempPed)
								vehLesterCar = GET_VEHICLE_PED_IS_IN(tempPed, TRUE)
								
								// Only do anims if lester is in the right seat
								IF GET_PED_IN_VEHICLE_SEAT(vehLesterCar, VS_BACK_RIGHT) = tempPed
									// CLEAR AND BLOCK LOOK AT PLAYER TASK
									IF NOT IS_BIT_SET(iLocalBoolCheck11, LBOOL11_BLOCK_LOOK_AT_PLAYER_TASK)
										PRINTLN("[RCC MISSION] TASK_PERFORM_AMBIENT_ANIMATION - Setting LBOOL11_BLOCK_LOOK_AT_PLAYER_TASK bit")
										SET_BIT(iLocalBoolCheck11, LBOOL11_BLOCK_LOOK_AT_PLAYER_TASK)
										
										TASK_CLEAR_LOOK_AT(tempPed)
									ENDIF
									
									//Beware this command, need to set it to stop weirdness with sequence migrating
									PRINTLN("[RCC MISSION] TASK_PERFORM_AMBIENT_ANIMATION - Calling ped ", iped, " with ciPED_IDLE_ANIM__HEIST_TUT_LESTER_PHONE_INTRO, setting net ID to not migrate!")
									SET_NETWORK_ID_CAN_MIGRATE(MC_serverBD_1.sFMMC_SBD.niped[iped], FALSE)
									
									TASK_PLAY_ANIM(tempPed, "ANIM@HEISTS@FLEECA_BANK@SCOPE_OUT@LESTER_IN_CAR", "Lester_INTRO", NORMAL_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_HOLD_LAST_FRAME)
									PRINTLN("[RCC MISSION] TASK_PERFORM_AMBIENT_ANIMATION - Tasking Lester to play Intro Anim")
									
									IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niPhoneforPedtask) //What if ped control migrates? - send an event to delete the phone if it exists
										IF CREATE_NET_OBJ(niPhoneforPedtask, PROP_AMB_PHONE, GET_ENTITY_COORDS(tempPed), bIsLocalPlayerHost)
											ATTACH_ENTITY_TO_ENTITY(NET_TO_ENT(niPhoneforPedtask), tempPed, GET_PED_BONE_INDEX(tempPed, BONETAG_PH_L_HAND), <<0,0,0>>, <<0,0,0>>)
											PRINTLN("[RCC MISSION] TASK_PERFORM_AMBIENT_ANIMATION - Created & Attached Lester Mid Mission Cutscene Prop Phone")
										ENDIF
									ENDIF
									
									// BROADCAST TO ALL PLAYERS THAT THEY SHOULD PLAY THE CUTSCENE INTRO ANIMS
									SET_BIT(MC_playerBD[iPartToUse].iClientBitSet2, PBBOOL2_TASK_PLAYER_CUTANIM_INTRO)
								ENDIF
							ENDIF
							
							SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__HEIST_TUT_LESTER_PHONE_OUTRO
				IF NOT IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				AND NOT IS_BIT_SET(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					REQUEST_ANIM_DICT("ANIM@HEISTS@FLEECA_BANK@SCOPE_OUT@LESTER_IN_CAR")
					
					IF HAS_ANIM_DICT_LOADED("ANIM@HEISTS@FLEECA_BANK@SCOPE_OUT@LESTER_IN_CAR")
						VEHICLE_INDEX vehLesterCar
						
						IF IS_PED_IN_ANY_VEHICLE(tempPed)
							vehLesterCar = GET_VEHICLE_PED_IS_IN(tempPed, TRUE)
							
							IF GET_PED_IN_VEHICLE_SEAT(vehLesterCar, VS_BACK_RIGHT) = tempPed
								TASK_PLAY_ANIM(tempPed, "ANIM@HEISTS@FLEECA_BANK@SCOPE_OUT@LESTER_IN_CAR", "Lester_OUTRO", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
								PRINTLN("[RCC MISSION] TASK_PERFORM_AMBIENT_ANIMATION - Tasking Lester to play Outro Anim")	
							ENDIF
							
							// BROADCAST TO ALL PLAYERS THAT THEY SHOULD PLAY THE CUTSCENE OUTRO ANIMS
							SET_BIT(MC_playerBD[iPartToUse].iClientBitSet2, PBBOOL2_TASK_PLAYER_CUTANIM_OUTRO)
						ENDIF
						
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						SET_BIT(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)), TRUE)
					ENDIF
					
				ELSE
					// HANDLE REMOVING LESTER'S PHONE PROPERLY
					IF IS_PED_IN_ANY_VEHICLE(tempPed)
					AND IS_ENTITY_PLAYING_ANIM(tempPed, "ANIM@HEISTS@FLEECA_BANK@SCOPE_OUT@LESTER_IN_CAR", "Lester_OUTRO")
					AND GET_ENTITY_ANIM_CURRENT_TIME(tempPed, "ANIM@HEISTS@FLEECA_BANK@SCOPE_OUT@LESTER_IN_CAR", "Lester_OUTRO") >= 0.8
					AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niPhoneforPedtask)
						DELETE_NET_ID(niPhoneforPedtask)
						SET_MODEL_AS_NO_LONGER_NEEDED(PROP_AMB_PHONE)
						PRINTLN("[RCC MISSION] TASK_PERFORM_AMBIENT_ANIMATION - Deleting Lester Mid Mission Cutscene Prop Phone")
						
						//Set the ped back to migrating:
						PRINTLN("[RCC MISSION] TASK_PERFORM_AMBIENT_ANIMATION - Calling ped ", iped, " with ciPED_IDLE_ANIM__HEIST_TUT_LESTER_PHONE_OUTRO, setting net ID to migrate again")
						SET_NETWORK_ID_CAN_MIGRATE(MC_serverBD_1.sFMMC_SBD.niped[iped], TRUE)
					ENDIF
					
					// CLEANUP LESTER ANIM DICT & TASK
					IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
					AND NOT IS_ENTITY_PLAYING_ANIM(tempPed, "ANIM@HEISTS@FLEECA_BANK@SCOPE_OUT@LESTER_IN_CAR", "Lester_OUTRO")
						bAnimating = FALSE
						REMOVE_ANIM_DICT("ANIM@HEISTS@FLEECA_BANK@SCOPE_OUT@LESTER_IN_CAR")
						PRINTLN("[RCC MISSION] TASK_PERFORM_AMBIENT_ANIMATION - Cleanup Lester Mid Mission Cutscene Anim Dict & Task")
						
						// CLEAR TASK LOOK AT PLAYER BLOCK
						IF IS_BIT_SET(iLocalBoolCheck11, LBOOL11_BLOCK_LOOK_AT_PLAYER_TASK)
							PRINTLN("[RCC MISSION] TASK_PERFORM_AMBIENT_ANIMATION - Clearing LBOOL11_BLOCK_LOOK_AT_PLAYER_TASK bit")
							CLEAR_BIT(iLocalBoolCheck11, LBOOL11_BLOCK_LOOK_AT_PLAYER_TASK)
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__WELDING_KNEELING
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)				
					IF CAN_REGISTER_MISSION_OBJECTS(1)
					AND GET_NUM_RESERVED_MISSION_OBJECTS(TRUE) + 1 < iMaxObjects
						REQUEST_ANIM_DICT("missheistdockssetup1ig_3@talk")
						request_model(PROP_WELD_TORCH)				
						IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_3@talk")
						and HAS_MODEL_LOADED(PROP_WELD_TORCH)																																		
							IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niWeldTorch)
								SET_CURRENT_PED_WEAPON(tempPed,WEAPONTYPE_UNARMED,true)
								MC_RESERVE_NETWORK_MISSION_OBJECTS(1)
								IF CREATE_NET_OBJ(niWeldTorch,PROP_WELD_TORCH,get_entity_coords(tempPed),bIsLocalPlayerHost,DEFAULT,TRUE,FALSE)		
									ATTACH_ENTITY_TO_ENTITY(NET_TO_ENT(niWeldTorch),tempPed,GET_PED_BONE_INDEX(tempPed,BONETAG_PH_R_HAND),<<0,0,0>>,<<0,0,0>>,TRUE,TRUE)
									SET_MODEL_AS_NO_LONGER_NEEDED(PROP_WELD_TORCH)
								ENDIF
							ENDIF		
							TASK_PLAY_ANIM(tempPed,"missheistdockssetup1ig_3@talk","oh_hey_vin_dockworker",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
						
							SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
						ENDIF
					ENDIF
				ELSE
					REMOVE_ANIM_DICT("missheistdockssetup1ig_3@talk")
				ENDIF			
			BREAK	
			
			CASE ciPED_IDLE_ANIM__CASHIER_LOOP
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
					REQUEST_ANIM_DICT("ANIM@HEISTS@FLEECA_BANK@SCOPE_OUT@CASHIER_LOOP")
					IF HAS_ANIM_DICT_LOADED("ANIM@HEISTS@FLEECA_BANK@SCOPE_OUT@CASHIER_LOOP")	
						TASK_PLAY_ANIM(tempPed,"ANIM@HEISTS@FLEECA_BANK@SCOPE_OUT@CASHIER_LOOP","CASHIER_LOOP",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING|AF_NOT_INTERRUPTABLE|AF_USE_KINEMATIC_PHYSICS|AF_USE_MOVER_EXTRACTION)
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
					ENDIF
				ELSE
					REMOVE_ANIM_DICT("ANIM@HEISTS@FLEECA_BANK@SCOPE_OUT@CASHIER_LOOP")
				ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_BODHI_LOOP
				IF NOT IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				AND NOT IS_BIT_SET(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_SYNCHRONIZED_SCENE, TRUE)
						IF CAN_REGISTER_MISSION_OBJECTS(1)
						OR IS_BIT_SET(iPedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							REQUEST_ANIM_DICT("ANIM@HEISTS@NARCOTICS@FINALE@IG_4_CAR_INTRO")//"anim@narcotics@finale@trevor_car_idle"
							REQUEST_MODEL(PROP_PHONE_ING)
							
							IF HAS_ANIM_DICT_LOADED("ANIM@HEISTS@NARCOTICS@FINALE@IG_4_CAR_INTRO")
							AND HAS_MODEL_LOADED(PROP_PHONE_ING)
								
								VEHICLE_INDEX tempVeh
								INT iSearchFlags
								iSearchFlags = VEHICLE_SEARCH_FLAG_RETURN_LAW_ENFORCER_VEHICLES + VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES + VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES + VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_GROUP_MEMBERS + VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_PLAYER + VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_DEAD_OR_DYING_PED + VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_SCRIPTED_TASK + VEHICLE_SEARCH_FLAG_ALLOW_LAW_ENFORCER_VEHICLES_WITH_WANTED_LEVEL
								tempVeh = GET_CLOSEST_VEHICLE(GET_ENTITY_COORDS(tempPed), 20, BODHI2, iSearchFlags)
								
								IF IS_VEHICLE_DRIVEABLE(tempVeh)
								
									vCoords = GET_ENTITY_COORDS(tempVeh)
									vRot = GET_ENTITY_ROTATION(tempVeh)
									
									INT iSynchScene
									iSynchScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCoords,vRot,EULER_XYZ,FALSE,TRUE)
									
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, TRUE)
									
									NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(tempPed,iSynchScene,"ANIM@HEISTS@NARCOTICS@FINALE@IG_4_CAR_INTRO","TREVOR_LOOP",NORMAL_BLEND_IN,INSTANT_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON|SYNCED_SCENE_ON_ABORT_STOP_SCENE)
									
									NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(tempVeh,iSynchScene,"ANIM@HEISTS@NARCOTICS@FINALE@IG_4_CAR_INTRO","BODHI_LOOP",NORMAL_BLEND_IN,INSTANT_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON|SYNCED_SCENE_ON_ABORT_STOP_SCENE)
									
									OBJECT_INDEX oiPhone
									
									IF IS_BIT_SET(iPedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
										oiPhone = GET_CLOSEST_OBJECT_OF_TYPE(vCoords, 20, PROP_PHONE_ING, false, false, false)
									ENDIF
									
									IF NOT IS_BIT_SET(iPedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
									OR NOT DOES_ENTITY_EXIST(oiPhone)
										IF CAN_REGISTER_MISSION_OBJECTS(1)
										AND GET_NUM_RESERVED_MISSION_OBJECTS(TRUE) + 1 < iMaxObjects
										
											MC_RESERVE_NETWORK_MISSION_OBJECTS(1)
											
											NETWORK_INDEX netPhone
											
											IF CREATE_NET_OBJ(netPhone,PROP_PHONE_ING,vCoords,bIsLocalPlayerHost,DEFAULT,TRUE,FALSE)
												NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_ENT(netPhone),iSynchScene,"ANIM@HEISTS@NARCOTICS@FINALE@IG_4_CAR_INTRO","PHONE_LOOP",NORMAL_BLEND_IN,INSTANT_BLEND_OUT)
											ENDIF
										ENDIF
									ELSE
										NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(oiPhone,iSynchScene,"ANIM@HEISTS@NARCOTICS@FINALE@IG_4_CAR_INTRO","PHONE_LOOP",NORMAL_BLEND_IN,INSTANT_BLEND_OUT)
									ENDIF
									
									NETWORK_START_SYNCHRONISED_SCENE(iSynchScene)
									
								ELSE
									//no car found
								ENDIF
								
								SET_MODEL_AS_NO_LONGER_NEEDED(PROP_PHONE_ING)
								
								SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
								BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
							ENDIF
						ENDIF
					ELSE
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						REMOVE_ANIM_DICT("ANIM@HEISTS@NARCOTICS@FINALE@IG_4_CAR_INTRO")
					ENDIF
				ELSE
					REMOVE_ANIM_DICT("ANIM@HEISTS@NARCOTICS@FINALE@IG_4_CAR_INTRO")
				ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_BODHI_OUTRO
				IF NOT IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				AND NOT IS_BIT_SET(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				//check if this is not a quick retry, B*2226207
					REQUEST_ANIM_DICT("ANIM@HEISTS@NARCOTICS@FINALE@IG_4_CAR_INTRO")
					IF HAS_ANIM_DICT_LOADED("ANIM@HEISTS@NARCOTICS@FINALE@IG_4_CAR_INTRO")
						
						VEHICLE_INDEX tempVeh
						INT iSearchFlags
						iSearchFlags = VEHICLE_SEARCH_FLAG_RETURN_LAW_ENFORCER_VEHICLES + VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES + VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES + VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_GROUP_MEMBERS + VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_PLAYER + VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_DEAD_OR_DYING_PED + VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_SCRIPTED_TASK + VEHICLE_SEARCH_FLAG_ALLOW_LAW_ENFORCER_VEHICLES_WITH_WANTED_LEVEL
						tempVeh = GET_CLOSEST_VEHICLE(GET_ENTITY_COORDS(tempPed), 20, BODHI2, iSearchFlags)
						
						IF IS_VEHICLE_DRIVEABLE(tempVeh)
						
							vCoords = GET_ENTITY_COORDS(tempVeh)
							
							vRot = GET_ENTITY_ROTATION(tempVeh)
							
							//B*2226207, check if Trevor is next to the bodhi
							IF IS_ENTITY_AT_ENTITY(tempPed, tempVeh, << 5.0, 5.0, 5.0 >>)
							
								INT iSynchScene
								iSynchScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCoords,vRot)
								
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, TRUE)
								NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(tempPed,iSynchScene,"ANIM@HEISTS@NARCOTICS@FINALE@IG_4_CAR_INTRO","TREVOR_OUTRO",SLOW_BLEND_IN,WALK_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_ON_ABORT_STOP_SCENE|SYNCED_SCENE_TAG_SYNC_OUT)
								
								NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(tempVeh,iSynchScene,"ANIM@HEISTS@NARCOTICS@FINALE@IG_4_CAR_INTRO","BODHI_OUTRO",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_ON_ABORT_STOP_SCENE)
								
								OBJECT_INDEX oiPhone
								
								oiPhone = GET_CLOSEST_OBJECT_OF_TYPE(vCoords, 10, PROP_PHONE_ING, false, false, false)
								
								IF DOES_ENTITY_EXIST(oiPhone)
									NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(oiPhone,iSynchScene,"ANIM@HEISTS@NARCOTICS@FINALE@IG_4_CAR_INTRO","PHONE_OUTRO",SLOW_BLEND_IN,NORMAL_BLEND_OUT)
								ENDIF
								
								NETWORK_START_SYNCHRONISED_SCENE(iSynchScene)
								
								iLocalTrevorBodhiBreakoutSynchSceneID = iSynchScene
								
								IF bIsLocalPlayerHost
									MC_serverBD_1.iTrevorBodhiBreakoutSynchSceneID = iSynchScene
								ENDIF
								
								BROADCAST_FMMC_TREVOR_BODHI_BREAKOUT_SYNCH_SCENE_ID(iSynchScene)
							
							ELSE
								PRINTLN("[RCC MISSION] TASK_PERFORM_AMBIENT_ANIMATION Bailing out of anim ciPED_IDLE_ANIM__TREVOR_NARCFIN_BODHI_OUTRO for ped ", iped)
							ENDIF
						ENDIF
						
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						SET_BIT(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)), TRUE)
					ENDIF
				ELSE
					REMOVE_ANIM_DICT("ANIM@HEISTS@NARCOTICS@FINALE@IG_4_CAR_INTRO")
				ENDIF
			BREAK
			
			//Lukasz: comment this out for B*2214154, no longer need these anims to play, but leave it commented in case we need to add it again
			//CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_SHOUT_A
			//CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_SHOUT_B
			//CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_SHOUT_C
			CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_A
			CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_B
			CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_C
			CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_D
			CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_VIGNETTE_01
			CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_VIGNETTE_02_L
			CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_VIGNETTE_02_R
			
				IF iAnimation = ciPED_IDLE_ANIM__TREVOR_NARCFIN_VIGNETTE_01
				OR iAnimation = ciPED_IDLE_ANIM__TREVOR_NARCFIN_VIGNETTE_02_L
				OR iAnimation = ciPED_IDLE_ANIM__TREVOR_NARCFIN_VIGNETTE_02_R
					IF IS_ENTITY_PLAYING_ANIM(tempPed,"anim@heists@narcotics@finale@trevor_cover_idles",GET_TREVOR_SECONDARY_ANIM(ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_A),ANIM_SCRIPT)
					OR IS_ENTITY_PLAYING_ANIM(tempPed,"anim@heists@narcotics@finale@trevor_cover_idles",GET_TREVOR_SECONDARY_ANIM(ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_B),ANIM_SCRIPT)
					OR IS_ENTITY_PLAYING_ANIM(tempPed,"anim@heists@narcotics@finale@trevor_cover_idles",GET_TREVOR_SECONDARY_ANIM(ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_C),ANIM_SCRIPT)
					OR IS_ENTITY_PLAYING_ANIM(tempPed,"anim@heists@narcotics@finale@trevor_cover_idles",GET_TREVOR_SECONDARY_ANIM(ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_D),ANIM_SCRIPT)
						IF IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							//STOP_ANIM_PLAYBACK(tempPed, DEFAULT, TRUE)	//stop any secondary anims in preparation for vignette anims
							CLEAR_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							PRINTLN("[RCC MISSION] TASK_PERFORM_AMBIENT_ANIMATION Clearing ipedAnimBitset for vignette anims as they are the priority to play on ped ", iped)
						ENDIF
					ENDIF
				ELSE
					//fix for B*2239794, if players are told to kill the enemies we no longer want Trevor's cover idles to play
					IF MC_serverBD_4.iPedMissionLogic[MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL
						iTrevorAnimPerforming = ciPED_IDLE_ANIM__NONE	//stop other Trevor idles from triggering
						RETURN FALSE
					ENDIF
				ENDIF
				
				IF NOT IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					IF NOT IS_ENTITY_PLAYING_ANIM(tempPed,"anim@heists@narcotics@finale@trevor_cover_idles",GET_TREVOR_SECONDARY_ANIM(iAnimation),ANIM_SCRIPT)

						IF IS_PED_IN_COVER(tempPed) AND NOT IS_PED_SHOOTING(tempPed) AND NOT IS_PED_AIMING_FROM_COVER(tempPed)
						//these animations should only occur when the ped is in cover, and not shooting and not aiming from cover
						//or when Trevor is already playing one of the cover idles 
						OR IS_ENTITY_PLAYING_ANIM(tempPed,"anim@heists@narcotics@finale@trevor_cover_idles",GET_TREVOR_SECONDARY_ANIM(ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_A),ANIM_SCRIPT)
						OR IS_ENTITY_PLAYING_ANIM(tempPed,"anim@heists@narcotics@finale@trevor_cover_idles",GET_TREVOR_SECONDARY_ANIM(ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_B),ANIM_SCRIPT)
						OR IS_ENTITY_PLAYING_ANIM(tempPed,"anim@heists@narcotics@finale@trevor_cover_idles",GET_TREVOR_SECONDARY_ANIM(ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_C),ANIM_SCRIPT)
						OR IS_ENTITY_PLAYING_ANIM(tempPed,"anim@heists@narcotics@finale@trevor_cover_idles",GET_TREVOR_SECONDARY_ANIM(ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_D),ANIM_SCRIPT)
						
							REQUEST_ANIM_DICT("anim@heists@narcotics@finale@trevor_cover_idles")
							
							IF NOT IS_BIT_SET(iLocalBoolCheck12, LBOOL12_TREVOR_COVER_ANIMS_LOADED)
								PRINTLN("[RCC MISSION] TASK_PERFORM_AMBIENT_ANIMATION - Loading in trevor cover idle animations anim@heists@narcotics@finale@trevor_cover_idles for ped ",iped)
								SET_BIT(iLocalBoolCheck12, LBOOL12_TREVOR_COVER_ANIMS_LOADED)
							ENDIF
							
							IF HAS_ANIM_DICT_LOADED("anim@heists@narcotics@finale@trevor_cover_idles")
								
								ANIMATION_FLAGS animFlags
								
								IF iAnimation >= ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_A
									animFlags = AF_NOT_INTERRUPTABLE|AF_USE_KINEMATIC_PHYSICS|AF_USE_MOVER_EXTRACTION|AF_ABORT_ON_WEAPON_DAMAGE|AF_HOLD_LAST_FRAME//|AF_SECONDARY
								ELSE
									animFlags = AF_NOT_INTERRUPTABLE|AF_USE_KINEMATIC_PHYSICS|AF_USE_MOVER_EXTRACTION|AF_ABORT_ON_WEAPON_DAMAGE//|AF_SECONDARY
								ENDIF
								
								IF iAnimation = ciPED_IDLE_ANIM__TREVOR_NARCFIN_VIGNETTE_02_L
								OR iAnimation = ciPED_IDLE_ANIM__TREVOR_NARCFIN_VIGNETTE_02_R
									IF IS_PED_IN_COVER_FACING_LEFT(tempPed)
										iAnimation = ciPED_IDLE_ANIM__TREVOR_NARCFIN_VIGNETTE_02_L
									ELSE
										iAnimation = ciPED_IDLE_ANIM__TREVOR_NARCFIN_VIGNETTE_02_R
									ENDIF
								ENDIF
								
								IF iAnimation = ciPED_IDLE_ANIM__TREVOR_NARCFIN_VIGNETTE_01
								OR iAnimation = ciPED_IDLE_ANIM__TREVOR_NARCFIN_VIGNETTE_02_L
								OR iAnimation = ciPED_IDLE_ANIM__TREVOR_NARCFIN_VIGNETTE_02_R
								
									iTrevorAnimPerforming = ciPED_IDLE_ANIM__NONE	//stop other Trevor idles from triggering
																	
									//SET_RAGDOLL_BLOCKING_FLAGS(tempPed, RBF_MELEE|RBF_PED_RAGDOLL_BUMP|RBF_PLAYER_BUMP|RBF_PLAYER_IMPACT)
									animFlags = AF_NOT_INTERRUPTABLE|AF_USE_KINEMATIC_PHYSICS|AF_USE_MOVER_EXTRACTION|AF_ABORT_ON_WEAPON_DAMAGE|AF_HOLD_LAST_FRAME
									
								ELSE
									
									//play scripted conversation for any cover anim other than vignette
									IF iTrevorAnimsRandom = 0
										SWITCH iAnimation
											CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_B
												BROADCAST_SCRIPT_EVENT_TREVOR_COVER_CONVERSATION_TRIGGERED(1, iTrevorAnimsRandom, iped)
											BREAK
											CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_A
												BROADCAST_SCRIPT_EVENT_TREVOR_COVER_CONVERSATION_TRIGGERED(2, iTrevorAnimsRandom, iped)
											BREAK
											CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_D
												BROADCAST_SCRIPT_EVENT_TREVOR_COVER_CONVERSATION_TRIGGERED(3, iTrevorAnimsRandom, iped)
											BREAK
											CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_C
												BROADCAST_SCRIPT_EVENT_TREVOR_COVER_CONVERSATION_TRIGGERED(4, iTrevorAnimsRandom, iped)
											BREAK
										ENDSWITCH
									ELSE
										BROADCAST_SCRIPT_EVENT_TREVOR_COVER_CONVERSATION_TRIGGERED(6, iTrevorAnimsRandom, iped)
									ENDIF
									
								ENDIF

								SET_RAGDOLL_BLOCKING_FLAGS(tempPed, RBF_MELEE|RBF_PED_RAGDOLL_BUMP|RBF_PLAYER_BUMP|RBF_PLAYER_IMPACT)
								TASK_PLAY_ANIM(tempPed,"anim@heists@narcotics@finale@trevor_cover_idles",GET_TREVOR_SECONDARY_ANIM(iAnimation),NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,animFlags,0,FALSE,AIK_DISABLE_ARM_IK)
								DEBUG_PRINTCALLSTACK()
								
								SET_PED_CAN_PEEK_IN_COVER(tempPed, FALSE)
								SET_PED_CONFIG_FLAG(tempPed, PCF_ForcedToStayInCover, TRUE)
								SET_PED_CONFIG_FLAG(tempPed, PCF_BlockPedFromTurningInCover, TRUE)
								
								SET_PED_CONFIG_FLAG(tempPed, PCF_ForcePedToFaceLeftInCover, FALSE)
								SET_PED_CONFIG_FLAG(tempPed, PCF_ForcePedToFaceRightInCover, FALSE)
								
								PRINTLN("[RCC MISSION] TASK_PERFORM_AMBIENT_ANIMATION Setting ped config flags PCF_ForcedToStayInCover and PCF_BlockPedFromTurningInCover on ped ", iped)

								SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
								SET_BIT(iPedSecondaryAnimCleanupBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))

								BROADCAST_FMMC_SECONDARY_ANIM_CLEANUP_EVENT(iped, TRUE)
								BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
								
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			BREAK
			
			CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_VAN_INTRO_AND_IDLE_LOOP
				
				strAnimDict = "anim@heists@narcotics@finale@ig_5_trevor_in_truck"
				
				IF NOT IS_BIT_SET(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					REQUEST_ANIM_DICT(strAnimDict)
					
					IF HAS_ANIM_DICT_LOADED(strAnimDict)
						
						CLEAR_SEQUENCE_TASK(temp_sequence)
						
						OPEN_SEQUENCE_TASK(temp_sequence)
							TASK_PLAY_ANIM(NULL, strAnimDict, "intro", SLOW_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE, 0, FALSE, AIK_DISABLE_ARM_IK)
							TASK_PLAY_ANIM(NULL, strAnimDict, "base", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE|AF_LOOPING)
						CLOSE_SEQUENCE_TASK(temp_sequence)
						
						TASK_PERFORM_SEQUENCE(tempPed, temp_sequence)
						
						CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Tasking ped ", iped, " to perform trevor van intro-idle sequence")
						
						CLEAR_SEQUENCE_TASK(temp_sequence)
						
						SET_BIT(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)), TRUE)
					ENDIF
				ELSE
					IF NOT IS_ENTITY_PLAYING_ANIM(tempPed, strAnimDict, "intro", ANIM_SCRIPT)
						IF NOT IS_ENTITY_PLAYING_ANIM(tempPed, strAnimDict, "base", ANIM_SCRIPT)
							REQUEST_ANIM_DICT(strAnimDict)
							IF HAS_ANIM_DICT_LOADED(strAnimDict)
								TASK_PLAY_ANIM(tempPed, strAnimDict, "base", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE|AF_LOOPING)
								
								CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Tasking ped ", iped, " to play trevor van base anim via iAnimation for intro/idle")
								
								bAnimating = FALSE
							ENDIF
						ELSE
							REMOVE_ANIM_DICT(strAnimDict)
							bAnimating = FALSE
						ENDIF
					ENDIF
				ENDIF
				
			BREAK
			
			CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_VAN_WHOSDRIVINGME
			CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_VAN_WEGOTTAGO
			CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_VAN_CEODONTDRIVEHIMSELF
			CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_VAN_DONTKEEPMEWAITING
			CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_VAN_AREWEGOING
				
				strAnimDict = "anim@heists@narcotics@finale@ig_5_trevor_in_truck"
				
				SWITCH iAnimation
					CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_VAN_WHOSDRIVINGME
						strAnimName = "whos_driving_me"
					BREAK
					CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_VAN_WEGOTTAGO
						strAnimName = "we_gotta_go"
					BREAK
					CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_VAN_CEODONTDRIVEHIMSELF
						strAnimName = "ceo_dont_drive_himself"
					BREAK
					CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_VAN_DONTKEEPMEWAITING
						strAnimName = "dont_keep_me_waiting"
					BREAK
					CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_VAN_AREWEGOING
						strAnimName = "are_we_going"
					BREAK
				ENDSWITCH
				
				IF NOT IS_ENTITY_PLAYING_ANIM(tempPed, strAnimDict, strAnimName)
					REQUEST_ANIM_DICT(strAnimDict)
					IF HAS_ANIM_DICT_LOADED(strAnimDict)
						TASK_PLAY_ANIM(tempPed, strAnimDict, strAnimName, SLOW_BLEND_IN, SLOW_BLEND_OUT)
					ENDIF
				ENDIF
				
			BREAK
			
			CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_VAN_OUTRO
				
				strAnimDict = "anim@heists@narcotics@finale@ig_5_trevor_in_truck"
				
				IF NOT IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					REQUEST_ANIM_DICT(strAnimDict)
					IF HAS_ANIM_DICT_LOADED(strAnimDict)
						TASK_PLAY_ANIM(tempPed, strAnimDict, "outro", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
						
						REMOVE_ANIM_DICT(strAnimDict)
						
						SET_BIT(iPedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						SET_BIT(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Tasking ped ", iped, " to play trevor van outro sequence")
						
						BROADCAST_FMMC_ANIM_STARTED(iped, TRUE)
					ELSE
						bAnimating = FALSE
					ENDIF
				ENDIF
				
			BREAK
			
			CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_VAN_DRIVING_IDLE
				
				strAnimDict = "anim@heists@narcotics@finale@ig_5_trevor_in_truck"
				strAnimName = "driving_base"
				
				IF NOT IS_ENTITY_PLAYING_ANIM(tempPed, strAnimDict, strAnimName, ANIM_SCRIPT)
				AND NOT IS_ENTITY_PLAYING_ANIM(tempPed, strAnimDict, "outro", ANIM_SCRIPT)
					REQUEST_ANIM_DICT(strAnimDict)
					
					IF HAS_ANIM_DICT_LOADED(strAnimDict)
						TASK_PLAY_ANIM(tempPed, strAnimDict, strAnimName, SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
						
						CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Tasking ped ", iped, " to play trevor anim ", strAnimName)
						
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
					ENDIF
				ELSE
					REMOVE_ANIM_DICT(strAnimDict)
				ENDIF
				
			BREAK
			
			CASE ciPED_IDLE_ANIM__STATION_COP_BEHIND_DESK_IDLE
				
				strAnimDict = "anim@heists@prison_heiststation@cop_reactions"
				strAnimName = "cop_b_idle"

				IF NOT IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					IF NOT IS_ENTITY_PLAYING_ANIM( tempPed, strAnimDict, strAnimName, ANIM_SCRIPT )
						REQUEST_ANIM_DICT( strAnimDict )
						IF HAS_ANIM_DICT_LOADED( strAnimDict )
							CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Tasking ped ", iped, " to play anim ", strAnimName)						
							TASK_PLAY_ANIM( tempPed, strAnimDict, strAnimName, DEFAULT, DEFAULT, DEFAULT, AF_LOOPING )
						ENDIF
					ELSE
						REMOVE_ANIM_DICT(strAnimDict)
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED( iped, IS_BIT_SET( iPedPerformingSpecialAnimBS[ GET_LONG_BITSET_INDEX( iped ) ], GET_LONG_BITSET_BIT( iped ) ) )
					ENDIF
				ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__STATION_COP_BEHIND_DESK_REACT

				strAnimDict = "anim@heists@prison_heiststation@cop_reactions"
				strAnimName = "cop_b_reaction"
				strAnimNameIdle = "cop_b_idle"
				
				IF NOT IS_BIT_SET( ipedAnimBitset[ GET_LONG_BITSET_INDEX( iped ) ], GET_LONG_BITSET_BIT( iped ) )
				AND NOT IS_BIT_SET(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					IF NOT IS_ENTITY_PLAYING_ANIM( tempPed, strAnimDict, strAnimName, ANIM_SCRIPT )
						REQUEST_ANIM_DICT( strAnimDict )
						IF HAS_ANIM_DICT_LOADED( strAnimDict )
						
							CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Tasking ped ", iped, " to play anim ", strAnimName)
							
							PLAY_PED_AMBIENT_SPEECH_NATIVE( tempPed, "GENERIC_HI", "SPEECH_PARAMS_FORCE" )
							
							SEQUENCE_INDEX SequenceIndex
							
							CLEAR_SEQUENCE_TASK(SequenceIndex)
							OPEN_SEQUENCE_TASK(SequenceIndex)
								TASK_PLAY_ANIM( NULL, strAnimDict, strAnimName)
								TASK_PLAY_ANIM( NULL, strAnimDict, strAnimNameIdle, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
							CLOSE_SEQUENCE_TASK(SequenceIndex)
							TASK_PERFORM_SEQUENCE(tempPed, SequenceIndex)
							CLEAR_SEQUENCE_TASK(SequenceIndex)
							
						ENDIF
					ELSE
						REMOVE_ANIM_DICT(strAnimDict)
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						SET_BIT(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED( iped, IS_BIT_SET( iPedPerformingSpecialAnimBS[ GET_LONG_BITSET_INDEX( iped ) ], GET_LONG_BITSET_BIT( iped ) ), TRUE )
					ENDIF
				ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__STATION_COP_BESIDE_LADY_IDLE

				strAnimDict = "anim@heists@prison_heiststation@cop_reactions"
				strAnimName = "cop_a_idle"
				
				PRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Trying to play ", strAnimName, " on ped ", iPed )
				
				IF NOT IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					IF NOT IS_ENTITY_PLAYING_ANIM( tempPed, strAnimDict, strAnimName, ANIM_SCRIPT )
						REQUEST_ANIM_DICT( strAnimDict )
						IF HAS_ANIM_DICT_LOADED( strAnimDict )
							CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Tasking ped ", iped, " to play anim ", strAnimName)	
							TASK_PLAY_ANIM( tempPed, strAnimDict, strAnimName, DEFAULT, DEFAULT, DEFAULT, AF_LOOPING )
						ENDIF
					ELSE
						REMOVE_ANIM_DICT( strAnimDict )
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED( iped, IS_BIT_SET( iPedPerformingSpecialAnimBS[ GET_LONG_BITSET_INDEX( iped ) ], GET_LONG_BITSET_BIT( iped ) ) )
					ENDIF
				ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__STATION_COP_BESIDE_LADY_REACT

				strAnimDict = "anim@heists@prison_heiststation@cop_reactions"
				strAnimName = "cop_a_reaction"
				strAnimNameIdle = "cop_a_idle"
				
				IF NOT IS_BIT_SET( ipedAnimBitset[ GET_LONG_BITSET_INDEX( iped ) ], GET_LONG_BITSET_BIT( iped ) )
				AND NOT IS_BIT_SET(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					IF NOT IS_ENTITY_PLAYING_ANIM( tempPed, strAnimDict, strAnimName, ANIM_SCRIPT )
						REQUEST_ANIM_DICT( strAnimDict )
						IF HAS_ANIM_DICT_LOADED( strAnimDict )
							
							CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Tasking ped ", iped, " to play anim ", strAnimName)	
							
							PLAY_PED_AMBIENT_SPEECH_NATIVE( tempPed, "GENERIC_HOWS_IT_GOING", "SPEECH_PARAMS_FORCE" )
							
							SEQUENCE_INDEX SequenceIndex
							
							CLEAR_SEQUENCE_TASK(SequenceIndex)
							OPEN_SEQUENCE_TASK(SequenceIndex)
								TASK_PLAY_ANIM( NULL, strAnimDict, strAnimName, NORMAL_BLEND_IN, SLOW_BLEND_OUT)
								TASK_PLAY_ANIM( NULL, strAnimDict, strAnimNameIdle, SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
							CLOSE_SEQUENCE_TASK(SequenceIndex)
							TASK_PERFORM_SEQUENCE(tempPed, SequenceIndex)
							CLEAR_SEQUENCE_TASK(SequenceIndex)
						ENDIF
					ELSE
						REMOVE_ANIM_DICT(strAnimDict)
						SET_BIT( ipedAnimBitset[ GET_LONG_BITSET_INDEX( iped ) ], GET_LONG_BITSET_BIT( iped ) )
						SET_BIT(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED( iped, IS_BIT_SET( iPedPerformingSpecialAnimBS[ GET_LONG_BITSET_INDEX( iped ) ], GET_LONG_BITSET_BIT( iped ) ), TRUE )
					ENDIF
				ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__STATION_DRUNK_LADY_IDLE

				strAnimDict = "anim@heists@prison_heiststation@cop_reactions"
				strAnimName = "drunk_idle"
				
				IF NOT IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				AND NOT IS_BIT_SET(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					IF NOT IS_ENTITY_PLAYING_ANIM( tempPed, strAnimDict, strAnimName, ANIM_SCRIPT )
						REQUEST_ANIM_DICT( strAnimDict )
						FREEZE_ENTITY_POSITION(tempPed,true)
						SET_RAGDOLL_BLOCKING_FLAGS(tempPed,RBF_PED_RAGDOLL_BUMP|RBF_PLAYER_BUMP|RBF_PLAYER_IMPACT|RBF_PLAYER_RAGDOLL_BUMP)
						IF HAS_ANIM_DICT_LOADED( strAnimDict )
							CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Tasking ped ", iped, " to play anim ", strAnimName)	
							DEBUG_PRINTCALLSTACK()
							TASK_PLAY_ANIM( tempPed, strAnimDict, strAnimName, DEFAULT, -0.1, DEFAULT, AF_LOOPING | AF_EXIT_AFTER_INTERRUPTED )
						ENDIF
					ELSE
						REMOVE_ANIM_DICT(strAnimDict)
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT( iped ) )
						SET_BIT(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED( iped, IS_BIT_SET( iPedPerformingSpecialAnimBS[ GET_LONG_BITSET_INDEX( iped ) ], GET_LONG_BITSET_BIT( iped ) ), TRUE )
					ENDIF
				ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__STATION_DRUNK_LADY_REACT

				strAnimDict = "anim@heists@prison_heiststation@cop_reactions"
				strAnimName = "drunk_talk"
				strAnimNameIdle = "drunk_idle"
				
				IF NOT IS_BIT_SET( ipedAnimBitset[ GET_LONG_BITSET_INDEX( iped ) ], GET_LONG_BITSET_BIT( iped ) )
					IF NOT IS_ENTITY_PLAYING_ANIM( tempPed, strAnimDict, strAnimName, ANIM_SCRIPT )
						REQUEST_ANIM_DICT( strAnimDict )
						IF HAS_ANIM_DICT_LOADED( strAnimDict )
							
							CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Tasking ped ", iped, " to play anim ", strAnimName)	
							
							SEQUENCE_INDEX SequenceIndex
							
							// this could protentially be a problem, sequences aren't net synced, if ped migrates while playing the first anim once it ends
							// the will lose everything that comes after it in the sequence!
							CLEAR_SEQUENCE_TASK(SequenceIndex)
							OPEN_SEQUENCE_TASK(SequenceIndex)
								TASK_PLAY_ANIM( NULL, strAnimDict, strAnimName, NORMAL_BLEND_IN, -0.1) // slow blend out to cover transition to next anim
								TASK_PLAY_ANIM( NULL, strAnimDict, strAnimNameIdle, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_EXIT_AFTER_INTERRUPTED)
							CLOSE_SEQUENCE_TASK(SequenceIndex)
							TASK_PERFORM_SEQUENCE(tempPed, SequenceIndex)
							CLEAR_SEQUENCE_TASK(SequenceIndex)
							FREEZE_ENTITY_POSITION(tempPed,true)
							SET_RAGDOLL_BLOCKING_FLAGS(tempPed,RBF_PED_RAGDOLL_BUMP|RBF_PLAYER_BUMP|RBF_PLAYER_IMPACT|RBF_PLAYER_RAGDOLL_BUMP)
							
							// 2196520 - set audio stream to play, broadcast to other players
							i_Pri_Sta_IG2_AudioStreamPedID = iped
							BROADCAST_FMMC_PRI_STA_IG2_AUDIOSTREAM( iped )
							CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Setting i_Pri_Sta_IG2_AudioStreamPedID = ", iped, ", to trigger audio scene")	
							
						ENDIF
					ELSE
						REMOVE_ANIM_DICT(strAnimDict)
						SET_BIT( ipedAnimBitset[ GET_LONG_BITSET_INDEX( iped ) ], GET_LONG_BITSET_BIT( iped ) )
						
						BROADCAST_FMMC_ANIM_STARTED( iped, IS_BIT_SET( iPedPerformingSpecialAnimBS[ GET_LONG_BITSET_INDEX( iped ) ], GET_LONG_BITSET_BIT( iped ) ) )
					ENDIF
				ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__VALKYRIE_AGENT_IDLE
			
				strAnimDict = "amb@world_human_stand_guard@male@base"
				strAnimName = "base"
				
				IF NOT IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					IF NOT IS_ENTITY_PLAYING_ANIM( tempPed, strAnimDict, strAnimName, ANIM_SCRIPT )
						REQUEST_ANIM_DICT( strAnimDict )
						IF HAS_ANIM_DICT_LOADED( strAnimDict )
							CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Tasking ped ", iped, " to play anim ", strAnimName)	
							TASK_PLAY_ANIM( tempPed, strAnimDict, strAnimName, DEFAULT, DEFAULT, DEFAULT, AF_LOOPING )
						ENDIF
					ELSE
						REMOVE_ANIM_DICT( strAnimDict )
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT( iped ) )
						BROADCAST_FMMC_ANIM_STARTED( iped, IS_BIT_SET( iPedPerformingSpecialAnimBS[ GET_LONG_BITSET_INDEX( iped ) ], GET_LONG_BITSET_BIT( iped ) ) )
					ENDIF
				ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__VALKYRIE_AGENT_HANDOVER
			
				strAnimDict 	= "anim@heists@humane_labs@finale@heli_react"
				strAnimName 	= "heli_react"
				
				IF NOT IS_BIT_SET( ipedAnimBitset[ GET_LONG_BITSET_INDEX( iped ) ], GET_LONG_BITSET_BIT( iped ) )
					IF NOT IS_ENTITY_PLAYING_ANIM( tempPed, strAnimDict, strAnimName, ANIM_SCRIPT )
						REQUEST_ANIM_DICT( strAnimDict )
						IF HAS_ANIM_DICT_LOADED( strAnimDict )
							
							CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Tasking ped ", iped, " to play anim ", strAnimName )	
							
							SET_RAGDOLL_BLOCKING_FLAGS(tempPed, RBF_PLAYER_BUMP | RBF_PLAYER_IMPACT | RBF_PLAYER_RAGDOLL_BUMP | RBF_MELEE )
							SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT( tempPed )
							
							SEQUENCE_INDEX seq
							OPEN_SEQUENCE_TASK( seq )
								TASK_PLAY_ANIM( NULL, strAnimDict, strAnimName, DEFAULT, SLOW_BLEND_OUT, DEFAULT, AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS )
								TASK_START_SCENARIO_IN_PLACE( NULL, "WORLD_HUMAN_GUARD_STAND", DEFAULT, FALSE )
							CLOSE_SEQUENCE_TASK( seq )
							TASK_PERFORM_SEQUENCE( tempPed, seq )
							CLEAR_SEQUENCE_TASK( seq )
						ENDIF
					ELSE
						REMOVE_ANIM_DICT( strAnimDict )
						SET_BIT( ipedAnimBitset[ GET_LONG_BITSET_INDEX( iped ) ], GET_LONG_BITSET_BIT( iped ) )
						BROADCAST_FMMC_ANIM_STARTED( iped, IS_BIT_SET( iPedPerformingSpecialAnimBS[ GET_LONG_BITSET_INDEX( iped ) ], GET_LONG_BITSET_BIT( iped ) ) )
					ENDIF
				ENDIF
			
			BREAK
			
			
			
			CASE ciPED_IDLE_ANIM__LAMAR_HIDING
			
				strAnimDict	= "ANIM@MISS@LOW@FIN@LAMAR@"
				strAnimName	= "IDLE"
			
				IF SHOULD_PED_BE_GIVEN_TASK( tempPed, iped, SCRIPT_TASK_PLAY_ANIM )
					IF NOT IS_ENTITY_PLAYING_ANIM( tempPed, strAnimDict, strAnimName, ANIM_SCRIPT )
						REQUEST_ANIM_DICT( strAnimDict )
						IF HAS_ANIM_DICT_LOADED( strAnimDict )				
							FREEZE_ENTITY_POSITION( tempPed, TRUE )
							SET_RAGDOLL_BLOCKING_FLAGS( tempPed, RBF_PED_RAGDOLL_BUMP | RBF_PLAYER_BUMP | RBF_PLAYER_IMPACT | RBF_PLAYER_RAGDOLL_BUMP )
							TASK_PLAY_ANIM( tempPed, strAnimDict, strAnimName, SLOW_BLEND_IN,SLOW_BLEND_OUT, -1, AF_LOOPING)
							SET_BIT( ipedAnimBitset[ GET_LONG_BITSET_INDEX( iped ) ], GET_LONG_BITSET_BIT( iped ) )
							BROADCAST_FMMC_ANIM_STARTED( iped, IS_BIT_SET( iPedPerformingSpecialAnimBS[ GET_LONG_BITSET_INDEX( iped ) ], GET_LONG_BITSET_BIT( iped ) ) )
						ENDIF
					ENDIF
				ELSE
					REMOVE_ANIM_DICT( strAnimDict )
				ENDIF			
			BREAK
			
			CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_1
			CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_2
			CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_3
			CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_4
			CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_5
			CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_6
			CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_7
			CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_8
			
				SWITCH iAnimation
					CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_1
						strAnimName	= "IDLE_PED01"
					BREAK
					CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_2
						strAnimName	= "IDLE_PED02"
					BREAK
					CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_3
						strAnimName	= "IDLE_PED03"
					BREAK
					CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_4
						strAnimName	= "IDLE_PED04"
					BREAK
					CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_5
						strAnimName	= "IDLE_PED05"
					BREAK
					CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_6
						strAnimName	= "IDLE_PED06"
					BREAK
					CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_7
						strAnimName	= "IDLE_PED07"
					BREAK
					CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_8
						strAnimName	= "IDLE_PED08"
					BREAK
				ENDSWITCH
			
				strAnimDict	= "ANIM@MISS@LOW@FIN@VAGOS@"
				
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_PED_ANIMATION - idle iAnimation = ",iAnimation," strAnimDict = ",strAnimDict," strAnimName = ",strAnimName)
				
				IF NOT IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				AND NOT IS_BIT_SET(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				AND SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
				
					REQUEST_ANIM_DICT(strAnimDict)
					
					IF HAS_ANIM_DICT_LOADED(strAnimDict)						
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, TRUE)				
						CPRINTLN(DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_PED_ANIMATION - TASK_PLAY_ANIM")
						TASK_PLAY_ANIM(tempPed,strAnimDict,strAnimName,NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)						
						//TASK_PLAY_ANIM_ADVANCED((tempPed,strAnimDict,strAnimName,vCoords,vRot,NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING|AF_EXTRACT_INITIAL_OFFSET,0,EULER_YXZ)
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
					ENDIF
				ELSE
					REMOVE_ANIM_DICT(strAnimDict)
				ENDIF
				
			BREAK
			
			CASE ciPED_IDLE_ANIM__FUNERAL_REACTION_1 
			CASE ciPED_IDLE_ANIM__FUNERAL_REACTION_2 
			CASE ciPED_IDLE_ANIM__FUNERAL_REACTION_3 
			CASE ciPED_IDLE_ANIM__FUNERAL_REACTION_4 
			CASE ciPED_IDLE_ANIM__FUNERAL_REACTION_5 
			CASE ciPED_IDLE_ANIM__FUNERAL_REACTION_6 
			CASE ciPED_IDLE_ANIM__FUNERAL_REACTION_7 
			CASE ciPED_IDLE_ANIM__FUNERAL_REACTION_8 		
			
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_PED_ANIMATION - reaction iAnimation = ",iAnimation)
				strAnimDict	= "ANIM@MISS@LOW@FIN@VAGOS@"
				
				IF NOT IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				AND NOT IS_BIT_SET(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					
					REQUEST_ANIM_DICT(strAnimDict)
					
					IF HAS_ANIM_DICT_LOADED(strAnimDict)
						
						SET_CURRENT_PED_WEAPON(tempPed, GET_BEST_PED_WEAPON(tempPed), TRUE)
						
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, TRUE)
						
						SET_PED_USING_ACTION_MODE(tempPed, TRUE, -1, "DEFAULT_ACTION")
						
						BOOL bInAngledArea		
						bInAngledArea = TRUE
						
						VECTOR vCutsceneEndPosition
						vCutsceneEndPosition = <<-295.7,2814.1,58.0>>
						FLOAT fTolerance2
						fTolerance2 = 100
						
						INT iPart
						PARTICIPANT_INDEX tempPart
						PLAYER_INDEX tempPlayer
						INT iPlayersChecked
						INT iPlayersToCheck 
						iPlayersToCheck = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]
						
						FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
							tempPart = INT_TO_PARTICIPANTINDEX(iPart)
							
							IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
							AND ( (NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)) OR IS_BIT_SET(MC_playerBD[iPart].iClientBitSet2, PBBOOL_HEIST_SPECTATOR) )
								
								tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
								iPlayersChecked++
								
								IF IS_NET_PLAYER_OK(tempPlayer)
									vCoords = GET_PLAYER_COORDS(tempPlayer)
									
									IF VDIST2(vCoords, vCutsceneEndPosition) > fTolerance2
										bInAngledArea = FALSE
									ENDIF
									
								ENDIF
								
								IF (NOT bInAngledArea)
								OR (iPlayersChecked >= iPlayersToCheck)
									iPart = NUM_NETWORK_PLAYERS
								ENDIF
							ENDIF
						ENDFOR
											
						SWITCH iAnimation
							CASE ciPED_IDLE_ANIM__FUNERAL_REACTION_1 
								IF bInAngledArea
									strAnimName = "REACT_01_PED01"
								ELSE
									strAnimName = "REACT_02_PED01"
								ENDIF
							BREAK
							CASE ciPED_IDLE_ANIM__FUNERAL_REACTION_2 
								IF bInAngledArea
									strAnimName = "REACT_01_PED02"
								ELSE
									strAnimName = "REACT_02_PED02"
								ENDIF
							BREAK
							CASE ciPED_IDLE_ANIM__FUNERAL_REACTION_3 
								IF bInAngledArea
									strAnimName = "REACT_01_PED03"
								ELSE
									strAnimName = "REACT_02_PED03"
								ENDIF
							BREAK
							CASE ciPED_IDLE_ANIM__FUNERAL_REACTION_4 
								IF bInAngledArea
									strAnimName = "REACT_01_PED04"
								ELSE
									strAnimName = "REACT_02_PED04"
								ENDIF
							BREAK
							CASE ciPED_IDLE_ANIM__FUNERAL_REACTION_5 
								IF bInAngledArea
									strAnimName = "REACT_01_PED05"
								ELSE
									strAnimName = "REACT_02_PED05"
								ENDIF
							BREAK
							CASE ciPED_IDLE_ANIM__FUNERAL_REACTION_6 
								IF bInAngledArea
									strAnimName = "REACT_01_PED06"
								ELSE
									strAnimName = "REACT_02_PED06"
								ENDIF
							BREAK
							CASE ciPED_IDLE_ANIM__FUNERAL_REACTION_7 
								IF bInAngledArea
									strAnimName = "REACT_01_PED07"
								ELSE
									strAnimName = "REACT_02_PED07"
								ENDIF
							BREAK
							CASE ciPED_IDLE_ANIM__FUNERAL_REACTION_8 	
								IF bInAngledArea
									strAnimName = "REACT_01_PED08"
								ELSE
									strAnimName = "REACT_02_PED08"
								ENDIF
							BREAK
						ENDSWITCH			
						
						CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_PED_ANIMATION - iAnimation = ",iAnimation," strAnimDict = ",strAnimDict," strAnimName = ",strAnimName)
						
						TASK_PLAY_ANIM(tempPed,strAnimDict,strAnimName)
						//TASK_PLAY_ANIM(tempPed, strAnimDict, strAnimName, NORMAL_BLEND_IN, WALK_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE, 0, FALSE)
						
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						SET_BIT(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)), TRUE)
					ENDIF
				ELSE
					IF DOES_ANIM_DICT_EXIST(strAnimDict)
						REMOVE_ANIM_DICT(strAnimDict)
					ENDIF
					
					IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
						bAnimating = FALSE
					ENDIF
				ENDIF
				
			BREAK
			
			CASE ciPED_IDLE_ANIM__HANG_OUT_STREET 
			
				strAnimDict	= "amb@world_human_hang_out_street@male_b@idle_a"
				strAnimName	= "idle_a"
				
				IF NOT IS_BIT_SET( ipedAnimBitset[ GET_LONG_BITSET_INDEX( iped ) ], GET_LONG_BITSET_BIT( iped ) )
					IF SHOULD_PED_BE_GIVEN_TASK( tempPed, iped, SCRIPT_TASK_PLAY_ANIM )
						IF NOT IS_ENTITY_PLAYING_ANIM( tempPed, strAnimDict, strAnimName, ANIM_SCRIPT )
							REQUEST_ANIM_DICT( strAnimDict )
							IF HAS_ANIM_DICT_LOADED( strAnimDict )
								
								CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Tasking ped ", iped, " to play anim ", strAnimName )	
								
								SET_RAGDOLL_BLOCKING_FLAGS(tempPed, RBF_PLAYER_BUMP | RBF_PLAYER_IMPACT | RBF_PLAYER_RAGDOLL_BUMP | RBF_MELEE )
								SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT( tempPed )
								
								SEQUENCE_INDEX seq
								OPEN_SEQUENCE_TASK( seq )
									TASK_PLAY_ANIM( NULL, strAnimDict, strAnimName, DEFAULT, SLOW_BLEND_OUT, DEFAULT, AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS )
									TASK_PLAY_ANIM( NULL, strAnimDict, "IDLE_B", DEFAULT, SLOW_BLEND_OUT, DEFAULT, AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS )
									TASK_PLAY_ANIM( NULL, strAnimDict, "IDLE_C", DEFAULT, SLOW_BLEND_OUT, DEFAULT, AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS )
									TASK_PLAY_ANIM( NULL, strAnimDict, "IDLE_D", DEFAULT, SLOW_BLEND_OUT, DEFAULT, AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS )
									TASK_PLAY_ANIM( NULL, strAnimDict, strAnimName, DEFAULT, SLOW_BLEND_OUT, DEFAULT, AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING )
									
								CLOSE_SEQUENCE_TASK( seq )
								TASK_PERFORM_SEQUENCE( tempPed, seq )
								CLEAR_SEQUENCE_TASK( seq )
							ENDIF
						ELSE
							REMOVE_ANIM_DICT( strAnimDict )
							SET_BIT( ipedAnimBitset[ GET_LONG_BITSET_INDEX( iped ) ], GET_LONG_BITSET_BIT( iped ) )
							BROADCAST_FMMC_ANIM_STARTED( iped, IS_BIT_SET( iPedPerformingSpecialAnimBS[ GET_LONG_BITSET_INDEX( iped ) ], GET_LONG_BITSET_BIT( iped ) ) )
						ENDIF
					ELSE
						REMOVE_ANIM_DICT( strAnimDict )
					ENDIF
				ENDIF
			BREAK
			
			
			CASE ciPED_IDLE_ANIM__COP_MALE_IDLE_A
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
					REQUEST_ANIM_DICT("amb@world_human_cop_idles@male@idle_a")
					IF HAS_ANIM_DICT_LOADED("amb@world_human_cop_idles@male@idle_a")
						TASK_PLAY_ANIM(tempPed,"amb@world_human_cop_idles@male@idle_a","idle_a",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
					ENDIF
				ELSE
					REMOVE_ANIM_DICT("amb@world_human_cop_idles@male@idle_a")
				ENDIF
			BREAK
			CASE ciPED_IDLE_ANIM__COP_MALE_IDLE_B
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
					REQUEST_ANIM_DICT("amb@world_human_cop_idles@male@idle_a")
					IF HAS_ANIM_DICT_LOADED("amb@world_human_cop_idles@male@idle_a")
						TASK_PLAY_ANIM(tempPed,"amb@world_human_cop_idles@male@idle_a","idle_b",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
					ENDIF
				ELSE
					REMOVE_ANIM_DICT("amb@world_human_cop_idles@male@idle_a")
				ENDIF
			BREAK
			CASE ciPED_IDLE_ANIM__COP_MALE_IDLE_C 
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
					REQUEST_ANIM_DICT("amb@world_human_cop_idles@male@idle_a")
					IF HAS_ANIM_DICT_LOADED("amb@world_human_cop_idles@male@idle_a")
						TASK_PLAY_ANIM(tempPed,"amb@world_human_cop_idles@male@idle_a","idle_c",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
					ENDIF
				ELSE
					REMOVE_ANIM_DICT("amb@world_human_cop_idles@male@idle_a")
				ENDIF
			BREAK
			CASE ciPED_IDLE_ANIM__COP_MALE_IDLE_D
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
					REQUEST_ANIM_DICT("amb@world_human_cop_idles@male@idle_b")
					IF HAS_ANIM_DICT_LOADED("amb@world_human_cop_idles@male@idle_b")
						TASK_PLAY_ANIM(tempPed,"amb@world_human_cop_idles@male@idle_b","idle_d",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
					ENDIF
				ELSE
					REMOVE_ANIM_DICT("amb@world_human_cop_idles@male@idle_b")
				ENDIF
			BREAK
			CASE ciPED_IDLE_ANIM__COP_MALE_IDLE_E 
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
					REQUEST_ANIM_DICT("amb@world_human_cop_idles@male@idle_b")
					IF HAS_ANIM_DICT_LOADED("amb@world_human_cop_idles@male@idle_b")
						TASK_PLAY_ANIM(tempPed,"amb@world_human_cop_idles@male@idle_b","idle_e",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
					ENDIF
				ELSE
					REMOVE_ANIM_DICT("amb@world_human_cop_idles@male@idle_b")
				ENDIF
			BREAK
			CASE ciPED_IDLE_ANIM__COP_MALE_IDLE_BASE
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
					REQUEST_ANIM_DICT("amb@world_human_cop_idles@male@base")
					IF HAS_ANIM_DICT_LOADED("amb@world_human_cop_idles@male@base")
						TASK_PLAY_ANIM(tempPed,"amb@world_human_cop_idles@male@base","base",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
					ENDIF
				ELSE
					REMOVE_ANIM_DICT("amb@world_human_cop_idles@male@base")
				ENDIF
			BREAK
			CASE ciPED_IDLE_ANIM__IMPATIENT_MALE_IDLE_A 
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
					REQUEST_ANIM_DICT("amb@world_human_stand_impatient@male@no_sign@idle_a")
					IF HAS_ANIM_DICT_LOADED("amb@world_human_stand_impatient@male@no_sign@idle_a")
						TASK_PLAY_ANIM(tempPed,"amb@world_human_stand_impatient@male@no_sign@idle_a","idle_a",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
					ENDIF
				ELSE
					REMOVE_ANIM_DICT("amb@world_human_stand_impatient@male@no_sign@idle_a")
				ENDIF
			BREAK
			CASE ciPED_IDLE_ANIM__IMPATIENT_MALE_IDLE_B
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
					REQUEST_ANIM_DICT("amb@world_human_stand_impatient@male@no_sign@idle_a")
					IF HAS_ANIM_DICT_LOADED("amb@world_human_stand_impatient@male@no_sign@idle_a")
						TASK_PLAY_ANIM(tempPed,"amb@world_human_stand_impatient@male@no_sign@idle_a","idle_b",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
					ENDIF
				ELSE
					REMOVE_ANIM_DICT("amb@world_human_stand_impatient@male@no_sign@idle_a")
				ENDIF
			BREAK
			CASE ciPED_IDLE_ANIM__IMPATIENT_MALE_IDLE_C 
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
					REQUEST_ANIM_DICT("amb@world_human_stand_impatient@male@no_sign@idle_a")
					IF HAS_ANIM_DICT_LOADED("amb@world_human_stand_impatient@male@no_sign@idle_a")
						TASK_PLAY_ANIM(tempPed,"amb@world_human_stand_impatient@male@no_sign@idle_a","idle_c",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
					ENDIF
				ELSE
					REMOVE_ANIM_DICT("amb@world_human_stand_impatient@male@no_sign@idle_a")
				ENDIF
			BREAK
			CASE ciPED_IDLE_ANIM__IMPATIENT_MALE_IDLE_BASE 
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
					REQUEST_ANIM_DICT("amb@world_human_stand_impatient@male@no_sign@base")
					IF HAS_ANIM_DICT_LOADED("amb@world_human_stand_impatient@male@no_sign@base")
						TASK_PLAY_ANIM(tempPed,"amb@world_human_stand_impatient@male@no_sign@base","base",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
						SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
					ENDIF
				ELSE
					REMOVE_ANIM_DICT("amb@world_human_stand_impatient@male@no_sign@base")
				ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__GO_FOUNDRY_HOSTAGE_PERP_IDLE
				
				strAnimDict = "anim@GangOps@Hostage@"
				
				IF NOT IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				AND NOT IS_BIT_SET(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					INT iVictimPed
					iVictimPed = GET_GO_FOUNDRY_VICTIM_PED(iped)
					
					IF iVictimPed != -1
						IF NOT IS_BIT_SET(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iVictimPed)], GET_LONG_BITSET_BIT(iVictimPed))
							IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iVictimPed])
								
								PED_INDEX victimPed
								victimPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iVictimPed])
								
								IF NOT IS_PED_INJURED(victimPed)
									
									IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_SYNCHRONIZED_SCENE, TRUE)
										
										IF NOT IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
											
											IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iVictimPed])
												
												REQUEST_ANIM_DICT(strAnimDict)
												
												IF HAS_ANIM_DICT_LOADED(strAnimDict)
													
													vCoords = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vPos + <<0, 0, 1.0>>
													vRot = <<0, 0, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fHead>>
													
													INT iSynchScene
													iSynchScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCoords, vRot, EULER_XYZ, FALSE, TRUE)
													
													SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, TRUE)
													SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(victimPed, TRUE)
													
													NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(tempPed, iSynchScene, strAnimDict, "perp_idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, RBF_PLAYER_BUMP|RBF_PLAYER_IMPACT|RBF_PLAYER_RAGDOLL_BUMP|RBF_BULLET_IMPACT|RBF_RUBBER_BULLET|RBF_ALLOW_BLOCK_DEAD_PED)
													NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(victimPed, iSynchScene, strAnimDict, "victim_idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, RBF_PLAYER_BUMP|RBF_PLAYER_IMPACT|RBF_PLAYER_RAGDOLL_BUMP)
													
													NETWORK_START_SYNCHRONISED_SCENE(iSynchScene)
													
													PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - GO Foundry Hostage Perp Idle ped ",iped,", victim ",iVictimPed,", synchronised scene started")
													
													SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
													BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
													
													SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iVictimPed)], GET_LONG_BITSET_BIT(iVictimPed))
													BROADCAST_FMMC_ANIM_STARTED(iVictimPed, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iVictimPed)], GET_LONG_BITSET_BIT(iVictimPed)))
													
												ENDIF
											ELSE
												PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - GO Foundry Hostage Perp Idle ped ",iped,", requesting control of victim ",iVictimPed)
												NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iVictimPed])
											ENDIF
											
										#IF IS_DEBUG_BUILD
										ELSE
											CASSERTLN(DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_PED_ANIMATION - GO Foundry Hostage Perp Idle ped ",iped," not performing sync scene, but ipedAnimBitset is already set!")
											PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - GO Foundry Hostage Perp Idle ped ",iped," not performing sync scene, but ipedAnimBitset is already set!")
										#ENDIF
										ENDIF
									ELSE
										SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
										REMOVE_ANIM_DICT(strAnimDict)
										
										IF IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iVictimPed)], GET_LONG_BITSET_BIT(iVictimPed))
										AND SHOULD_PED_BE_GIVEN_TASK(victimPed, iVictimPed, SCRIPT_TASK_SYNCHRONIZED_SCENE, TRUE)
											// Victim ped has got out of our hands somehow, give up on the anims
											PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - GO Foundry Hostage Perp Idle ped ",iped,", victim ",iVictimPed," is not animating")
											
											// Pretend to have done the breakout:
											SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
											SET_BIT(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
											SET_BIT(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
											BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)), TRUE)
											
											bAnimating = FALSE
										ENDIF
									ENDIF
								ELSE
									REMOVE_ANIM_DICT(strAnimDict)
									PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - GO Foundry Hostage Perp Idle ped ",iped,", victim ",iVictimPed," is injured")
									
									IF IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iVictimPed)], GET_LONG_BITSET_BIT(iVictimPed))
										// Victim ped is dead, need to fool the system into thinking that we've finished our breakout
										
										SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
										SET_BIT(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
										SET_BIT(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
										BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)), TRUE)
										
										bAnimating = FALSE
									ENDIF
								ENDIF
							ELSE
								REMOVE_ANIM_DICT(strAnimDict)
								PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - GO Foundry Hostage Perp Idle ped ",iped,", victim ",iVictimPed," does not exist")
								
								IF IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iVictimPed)], GET_LONG_BITSET_BIT(iVictimPed))
									// Victim ped stopped existing, need to fool the system into thinking that we've finished our breakout
									
									SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
									SET_BIT(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
									SET_BIT(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
									BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)), TRUE)
									
									bAnimating = FALSE
								ENDIF
							ENDIF
						ELSE
							REMOVE_ANIM_DICT(strAnimDict)
							PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - GO Foundry Hostage Perp Idle ped ",iped,", victim ",iVictimPed," already started their breakout anim")
							
							// Victim ped started their breakout, need to fool the system into thinking that we've finished our breakout
							
							SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							SET_BIT(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							SET_BIT(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)), TRUE)
							
							bAnimating = FALSE
						ENDIF
					ELSE
						// Don't need to unload the dictionary here, it won't ever have been requested
						CASSERTLN(DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_PED_ANIMATION - GO Foundry Hostage Perp Idle ped ",iped,", victim not found!")
						PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - GO Foundry Hostage Perp Idle ped ",iped,", victim not found!")
					ENDIF
				ELSE
					REMOVE_ANIM_DICT(strAnimDict)
				ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__GO_FOUNDRY_HOSTAGE_VICTIM_IDLE
				// All handled in the perp idle stage above, this needs to be a synched scene
			BREAK
			
			CASE ciPED_IDLE_ANIM__GO_FOUNDRY_HOSTAGE_PERP_EXECUTE
				
				strAnimDict = "anim@GangOps@Hostage@"
				
				IF NOT IS_BIT_SET(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					
					INT iVictimPed
					iVictimPed = GET_GO_FOUNDRY_VICTIM_PED(iped)
					
					IF iVictimPed != -1
						IF NOT IS_BIT_SET(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iVictimPed)], GET_LONG_BITSET_BIT(iVictimPed))
							IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iVictimPed])
								
								PED_INDEX victimPed
								victimPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iVictimPed])
								
								IF NOT IS_PED_INJURED(victimPed)
									
									IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iVictimPed])
										
										REQUEST_ANIM_DICT(strAnimDict)
										
										IF HAS_ANIM_DICT_LOADED(strAnimDict)
											
											vCoords = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vPos + <<0, 0, 1.0>>
											vRot = <<0, 0, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fHead>>
											
											INT iSynchScene
											iSynchScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCoords, vRot, EULER_XYZ, TRUE)
											
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, TRUE)
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(victimPed, TRUE)
											
											NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(tempPed, iSynchScene, strAnimDict, "perp_fail", NORMAL_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT)
											NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(victimPed, iSynchScene, strAnimDict, "victim_fail", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, RBF_BULLET_IMPACT|RBF_RUBBER_BULLET|RBF_ALLOW_BLOCK_DEAD_PED)
											
											NETWORK_START_SYNCHRONISED_SCENE(iSynchScene)
											
											PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - GO Foundry Hostage Perp Execute ped ",iped,", victim ",iVictimPed,", synchronised scene started")
											
											iLocalGOFoundryHostageSynchSceneID = iSynchScene
											PRINTLN("[RCC MISSION][HostageSync] PROCESS_PED_ANIMATION - GO Foundry Hostage Perp Execute anim, set iLocalGOFoundryHostageSynchSceneID as ",iSynchScene)
											
											IF bIsLocalPlayerHost
												MC_serverBD_1.iGOFoundryHostageSynchSceneID = iSynchScene
												PRINTLN("[RCC MISSION][HostageSync] PROCESS_PED_ANIMATION - GO Foundry Hostage Perp Execute anim, set host iGOFoundryHostageSynchSceneID as ",iSynchScene)
											ENDIF
											
											BROADCAST_FMMC_GO_FOUNDRY_HOSTAGE_SYNCH_SCENE_ID(iSynchScene)
											
											SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
											SET_BIT(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
											BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)), TRUE)
											
											// We don't want to set the breakout flag - if the victim is rescued in the small window before they are shot, they should probably still play the breakout animation
											SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iVictimPed)], GET_LONG_BITSET_BIT(iVictimPed))
											BROADCAST_FMMC_ANIM_STARTED(iVictimPed, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iVictimPed)], GET_LONG_BITSET_BIT(iVictimPed)), FALSE)
											
										ENDIF
										
									ELSE
										PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - GO Foundry Hostage Perp Execute ped ",iped,", requesting control of victim ",iVictimPed," for anim")
										NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iVictimPed])
									ENDIF
								ELSE
									REMOVE_ANIM_DICT(strAnimDict)
									PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - GO Foundry Hostage Perp Execute ped ",iped,", victim ",iVictimPed," is injured")
								ENDIF
							ELSE
								REMOVE_ANIM_DICT(strAnimDict)
								PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - GO Foundry Hostage Perp Execute ped ",iped,", victim ",iVictimPed," does not exist")
							ENDIF
						ELSE
							REMOVE_ANIM_DICT(strAnimDict)
							PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - GO Foundry Hostage Perp Execute ped ",iped,", victim ",iVictimPed," already broken out")
						ENDIF
					ELSE
						// Don't need to unload the dictionary here, it won't ever have been requested
						CASSERTLN(DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_PED_ANIMATION - GO Foundry Hostage Perp Execute ped ",iped,", victim not found!")
						PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - GO Foundry Hostage Perp Execute ped ",iped,", victim not found!")
					ENDIF
				ELSE
					REMOVE_ANIM_DICT(strAnimDict)
					
					IF (NOT SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_SYNCHRONIZED_SCENE, TRUE))
					AND IS_ENTITY_PLAYING_ANIM(tempPed, "anim@GangOps@Hostage@", "perp_fail")
						
						INT iVictimPed
						iVictimPed = GET_GO_FOUNDRY_VICTIM_PED(iped)
						
						IF iVictimPed != -1
							IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iVictimPed])
							AND NOT IS_PED_INJURED(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iVictimPed]))
								
								PED_INDEX VictimPed
								VictimPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iVictimPed])
								
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iVictimPed])
									
									INT iSynchScene
									iSynchScene = iLocalGOFoundryHostageSynchSceneID
									
									IF iSynchScene = -1
										iSynchScene = MC_serverBD_1.iGOFoundryHostageSynchSceneID
									ENDIF
									
									BOOL bFireGun
									
									IF iSynchScene != -1
										INT iLocalScene
										iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iSynchScene)
										IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
											IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene) >= 0.09
												PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - GO Foundry Hostage Perp Execute ped ",iped," sync scene ",iSynchScene," passed gun fire phase")
												bFireGun = TRUE
											ELSE
												PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - GO Foundry Hostage Perp Execute ped ",iped," sync scene ",iSynchScene,", current phase ",GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene)," not passed gun fire phase")
											ENDIF
										ELSE
											PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - GO Foundry Hostage Perp Execute ped ",iped," sync scene ",iSynchScene," not running yet")
										ENDIF
									ELSE
										PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - GO Foundry Hostage Perp Execute ped ",iped," found no sync scene, so fire away")
										bFireGun = TRUE
									ENDIF
									
									IF bFireGun
										VECTOR vVictimHead
										
										//vGun = GET_PED_BONE_COORDS(tempPed, BONETAG_PH_R_HAND, <<0,0,0>>)
										vVictimHead = GET_PED_BONE_COORDS(VictimPed, BONETAG_HEAD, <<0,0,0>>)
										
										//WEAPON_TYPE wtGun
										//wtGun = WEAPONTYPE_INVALID
										
										//GET_CURRENT_PED_WEAPON(tempPed, wtGun)
										
										//IF wtGun = WEAPONTYPE_INVALID
										//OR wtGun = WEAPONTYPE_UNARMED
										//	wtGun = WEAPONTYPE_PISTOL
										//ENDIF
										
										PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - GO Foundry Hostage Perp Execute ped ",iped,", firing gun at victim ",iVictimPed," head at ",vVictimHead)
										SET_PED_SHOOTS_AT_COORD(tempPed, vVictimHead, TRUE)
										//SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY(vGun, vVictimHead, 100, TRUE, wtGun, tempPed, DEFAULT, DEFAULT, DEFAULT, tempPed)
										
										IF iSynchScene != -1
											PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - GO Foundry Hostage Perp Execute ped ",iped,", setting victim ",iVictimPed," as frozen to carry out anim")
											FREEZE_ENTITY_POSITION(VictimPed, TRUE) // We freeze the ped as otherwise they stop doing the animation once they die. We unfreeze them in 
										ENDIF
										
										PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - GO Foundry Hostage Perp Execute ped ",iped,", setting victim ",iVictimPed," as 0 health")
										SET_ENTITY_HEALTH(VictimPed, 0)
									ENDIF
								ELSE
									PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - GO Foundry Hostage Perp Execute ped ",iped,", requesting control of victim ",iVictimPed," for killing")
									NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iVictimPed])
								ENDIF
								
							ELSE
								// Victim is injured / cleaned up:
								
								INT iSynchScene
								iSynchScene = iLocalGOFoundryHostageSynchSceneID
								
								IF iSynchScene = -1
									iSynchScene = MC_serverBD_1.iGOFoundryHostageSynchSceneID
								ENDIF
								
								IF iSynchScene != -1
									INT iLocalScene
									iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iSynchScene)
									IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
										IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene) >= 0.9
											PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - GO Foundry Hostage Perp Execute ped ",iped," sync scene ",iSynchScene," passed point to move into combat")
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, FALSE)
											SET_PED_RESET_FLAG(tempPed, PRF_InstantBlendToAim, TRUE)
											iGOFoundryHostagePerpResetFlagPed = iped
											TASK_COMBAT_HATED_TARGETS_AROUND_PED(tempPed, 299)
										ENDIF
									ENDIF
								ENDIF
								
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__GO_FOUNDRY_HOSTAGE_VICTIM_FREED
				
				strAnimDict = "anim@GangOps@Hostage@"
				
				IF NOT IS_BIT_SET(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					
					INT iPerpPed
					iPerpPed = GET_GO_FOUNDRY_PERP_PED(iped)
					
					IF iPerpPed != -1
						IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iPerpPed])
							
							PED_INDEX perpPed
							perpPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPerpPed])
							
							IF DOES_ENTITY_EXIST(perpPed)
								
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iPerpPed])
									
									REQUEST_ANIM_DICT(strAnimDict)
									
									IF HAS_ANIM_DICT_LOADED(strAnimDict)
										
										vCoords = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPerpPed].vPos + <<0, 0, 1.0>>
										vRot = <<0, 0, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPerpPed].fHead>>
										
										INT iSynchScene
										iSynchScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCoords, vRot, EULER_XYZ)
										
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, TRUE)
										//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(perpPed, TRUE)
										
										NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(tempPed, iSynchScene, strAnimDict, "victim_success", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT)
										NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(perpPed, iSynchScene, strAnimDict, "perp_success", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, RBF_BULLET_IMPACT|RBF_RUBBER_BULLET|RBF_ALLOW_BLOCK_DEAD_PED)
										
										NETWORK_START_SYNCHRONISED_SCENE(iSynchScene)
										
										PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - GO Foundry Hostage Victim Freed ped ",iped,", perp ",iPerpPed,", synchronised scene started")
										
										iLocalGOFoundryHostageSynchSceneID = iSynchScene
										PRINTLN("[RCC MISSION][HostageSync] PROCESS_PED_ANIMATION - GO Foundry Hostage Victim Freed anim, set iLocalGOFoundryHostageSynchSceneID as ",iSynchScene)
										
										IF bIsLocalPlayerHost
											MC_serverBD_1.iGOFoundryHostageSynchSceneID = iSynchScene
											PRINTLN("[RCC MISSION][HostageSync] PROCESS_PED_ANIMATION - GO Foundry Hostage Victim Freed anim, set host iGOFoundryHostageSynchSceneID as ",iSynchScene)
										ENDIF
										
										BROADCAST_FMMC_GO_FOUNDRY_HOSTAGE_SYNCH_SCENE_ID(iSynchScene)
										
										SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
										SET_BIT(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
										BROADCAST_FMMC_ANIM_STARTED(iped, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)), TRUE)
										
										// The perp is dead, we don't need to worry about these flags:
										//SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iPerpPed)], GET_LONG_BITSET_BIT(iPerpPed))
										//BROADCAST_FMMC_ANIM_STARTED(iPerpPed, IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iPerpPed)], GET_LONG_BITSET_BIT(iPerpPed)), FALSE)
										
									ENDIF
									
								ELSE
									PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - GO Foundry Hostage Victim Freed ped ",iped,", requesting control of perp ",iPerpPed)
									NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iPerpPed])
								ENDIF
							ELSE
								REMOVE_ANIM_DICT(strAnimDict)
								PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - GO Foundry Hostage Victim Freed ped ",iped,", perp ",iPerpPed," does not exist2")
							ENDIF
						ELSE
							REMOVE_ANIM_DICT(strAnimDict)
							PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - GO Foundry Hostage Victim Freed ped ",iped,", perp ",iPerpPed," does not exist")
						ENDIF
					ELSE
						// Don't need to unload the dictionary here, it won't ever have been requested
						CASSERTLN(DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_PED_ANIMATION - GO Foundry Hostage Victim Freed ped ",iped,", perp not found!")
						PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - GO Foundry Hostage Victim Freed ped ",iped,", perp not found!")
					ENDIF
				ELSE
					REMOVE_ANIM_DICT(strAnimDict)
				ENDIF
			BREAK
			CASE ciPED_IDLE_ANIM__MALE_DANCE_SOLO_HIGH
				PLAY_TASK_ANIMATION_SET(tempPed,iped,"NIGHTCLUB@DANCE_MINIGAME@DANCE_SOLO@MALE@VAR_A","ANIM@AMB@NIGHTCLUB@MINI@DANCE@DANCE_SOLO@MALE@VAR_A@","high_center")
			BREAK
			CASE ciPED_IDLE_ANIM__MALE_DANCE_SOLO_HIGH_UP
				PLAY_TASK_ANIMATION_SET(tempPed,iped,"NIGHTCLUB@DANCE_MINIGAME@DANCE_SOLO@MALE@VAR_A","ANIM@AMB@NIGHTCLUB@MINI@DANCE@DANCE_SOLO@MALE@VAR_A@","high_center_up")
			BREAK
			CASE ciPED_IDLE_ANIM__MALE_DANCE_SOLO_MED
				PLAY_TASK_ANIMATION_SET(tempPed,iped,"NIGHTCLUB@DANCE_MINIGAME@DANCE_SOLO@MALE@VAR_A","ANIM@AMB@NIGHTCLUB@MINI@DANCE@DANCE_SOLO@MALE@VAR_A@","med_center")
			BREAK
			CASE ciPED_IDLE_ANIM__FEMALE_DANCE_SOLO_HIGH
				PLAY_TASK_ANIMATION_SET(tempPed,iped,"NIGHTCLUB@DANCE_MINIGAME@DANCE_SOLO@FEMALE@VAR_A","ANIM@AMB@NIGHTCLUB@MINI@DANCE@DANCE_SOLO@FEMALE@VAR_A@","high_center")
			BREAK
			CASE ciPED_IDLE_ANIM__FEMALE_DANCE_SOLO_HIGH_UP
				PLAY_TASK_ANIMATION_SET(tempPed,iped,"NIGHTCLUB@DANCE_MINIGAME@DANCE_SOLO@FEMALE@VAR_A","ANIM@AMB@NIGHTCLUB@MINI@DANCE@DANCE_SOLO@FEMALE@VAR_A@","high_center_up")
			BREAK
			CASE ciPED_IDLE_ANIM__FEMALE_DANCE_SOLO_MED
				PLAY_TASK_ANIMATION_SET(tempPed,iped,"NIGHTCLUB@DANCE_MINIGAME@DANCE_SOLO@FEMALE@VAR_A","ANIM@AMB@NIGHTCLUB@MINI@DANCE@DANCE_SOLO@FEMALE@VAR_A@","med_center")
			BREAK
			CASE ciPED_IDLE_ANIM__PRIVATE_DANCE_1
				PLAY_TASK_ANIMATION_DIC(tempPed,iped,"mini@strip_club@private_dance@part1","priv_dance_p1")
			BREAK
			CASE ciPED_IDLE_ANIM__DRUNK_IDLE_1
				PLAY_TASK_ANIMATION_DIC(tempPed,iped,"move_m@drunk@VERYDRUNK_IDLES@","fidget_01")
			BREAK
			CASE ciPED_IDLE_ANIM__DRUNK_IDLE_2
				PLAY_TASK_ANIMATION_DIC(tempPed,iped,"move_m@drunk@VERYDRUNK_IDLES@","fidget_02")
			BREAK
			CASE ciPED_IDLE_ANIM__DRUNK_IDLE_3
				PLAY_TASK_ANIMATION_DIC(tempPed,iped,"move_m@drunk@VERYDRUNK_IDLES@","fidget_03")
			BREAK
			CASE ciPED_IDLE_ANIM__DRUNK_IDLE_4
				PLAY_TASK_ANIMATION_DIC(tempPed,iped,"move_m@drunk@VERYDRUNK_IDLES@","fidget_04")
			BREAK
			CASE ciPED_IDLE_ANIM__DRUNK_IDLE_5
				PLAY_TASK_ANIMATION_DIC(tempPed,iped,"move_m@drunk@VERYDRUNK_IDLES@","fidget_05")
			BREAK
			CASE ciPED_IDLE_ANIM__DRUNK_IDLE_6
				PLAY_TASK_ANIMATION_DIC(tempPed,iped,"move_m@drunk@VERYDRUNK_IDLES@","fidget_06")
			BREAK
			CASE ciPED_IDLE_ANIM__DRUNK_IDLE_7
				PLAY_TASK_ANIMATION_DIC(tempPed,iped,"move_m@drunk@VERYDRUNK_IDLES@","fidget_07")
			BREAK
			CASE ciPED_IDLE_ANIM__DRUNK_IDLE_8
				PLAY_TASK_ANIMATION_DIC(tempPed,iped,"move_m@drunk@VERYDRUNK_IDLES@","fidget_08")
			BREAK
			CASE ciPED_IDLE_ANIM__DRUNK_IDLE_9
				PLAY_TASK_ANIMATION_DIC(tempPed,iped,"move_m@drunk@VERYDRUNK_IDLES@","fidget_09")
			BREAK
			CASE ciPED_IDLE_ANIM__DRUNK_IDLE_10
				PLAY_TASK_ANIMATION_DIC(tempPed,iped,"move_m@drunk@VERYDRUNK_IDLES@","fidget_10")
			BREAK
		
			// Only add the main animations here, not the child ones.
			CASE ciPED_IDLE_ANIM__CASINO_VINCENT_INTRO
			CASE ciPED_IDLE_ANIM__CASINO_FIGHT_BREAKOUT_1
			CASE ciPED_IDLE_ANIM__CASINO_FIGHT_SECURITY_GUARD
			CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CONVERSATION
			CASE ciPED_IDLE_ANIM__CASINO_FIGHT_HEADLOCK
			CASE ciPED_IDLE_ANIM__CASINO_FIGHT_ARGUE
			CASE ciPED_IDLE_ANIM__CASINO_FIGHT_BAR_THROW
			CASE ciPED_IDLE_ANIM__CASINO_FIGHT_SLOT_SLAM
			CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CHIP_STEAL_ATTACKER_1
			CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CASHIER	
			CASE ciPED_IDLE_ANIM__CASINO_SLOTMACHINE_ATTACK
			CASE ciPED_IDLE_ANIM__CASINO_FIGHT_SHOP_THROW
			CASE ciPED_IDLE_ANIM__CASINO_ROULETTE_REACT_1
			CASE ciPED_IDLE_ANIM__CASINO_ROULETTE_REACT_2
			CASE ciPED_IDLE_ANIM__CASINO_GET_IN_CAR_TAO
			CASE ciPED_IDLE_ANIM__CASINO_GET_IN_CAR_TRANS
			CASE ciPED_IDLE_ANIM__CASINO_GET_IN_CAR_AGATHA
				IF NOT IS_PARTICIPANT_SYNC_SCENE_PED_OWNER_SET(iPed)
				AND NOT IS_BIT_SET(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_iSyncSceneAnimationFinished)
					PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation), "] - PROCESS_PED_ANIMATION - Setting as Owner for Animation: ", iAnimation, " bisSpecialAnim: ", bisSpecialAnim)
					SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					IF bisSpecialAnim
						SET_BIT(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					ENDIF
					BROADCAST_FMMC_ANIM_STARTED(iped, bisSpecialAnim, FALSE)
					SET_PARTICIPANT_AS_SYNC_SCENE_PED_OWNER(iLocalPart, iPed)
				ENDIF
				RETURN FALSE
				
			CASE ciPED_IDLE_ANIM__WINDOW_SHOP_IDLE
				PLAY_TASK_ANIMATION_DIC(tempPed,iped,"amb@world_human_window_shop@male@idle_a","browse_a")
			BREAK
			CASE ciPED_IDLE_ANIM__CAR_PARK_ATTENDANT_IDLE
				PLAY_TASK_ANIMATION_DIC(tempPed,iped,"amb@world_human_car_park_attendant@male@base","base")
			BREAK
			CASE ciPED_IDLE_ANIM__STUPOR_A
				PLAY_TASK_ANIMATION_DIC(tempPed,iped,"amb@world_human_stupor@male@idle_a", "idle_a")
			BREAK
			CASE ciPED_IDLE_ANIM__STUPOR_B
				PLAY_TASK_ANIMATION_DIC(tempPed,iped,"amb@world_human_stupor@male@idle_a", "idle_b")
			BREAK
			CASE ciPED_IDLE_ANIM__STUPOR_C
				PLAY_TASK_ANIMATION_DIC(tempPed,iped,"amb@world_human_stupor@male@idle_a", "idle_c")
			BREAK
			CASE ciPED_IDLE_ANIM__FILM_SHOCKING
				//PLAY_TASK_ANIMATION_DIC(tempPed,iped,"amb@world_human_mobile_film_shocking@male@base", "base")
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_START_SCENARIO_IN_PLACE)
					TASK_START_SCENARIO_IN_PLACE(tempPed, "WORLD_HUMAN_MOBILE_FILM_SHOCKING", DEFAULT, TRUE)
				ENDIF
			BREAK
			CASE ciPED_IDLE_ANIM__CROUCH_AIM
				IF NOT IS_PED_IN_COMBAT(tempPed)
					IF PLAY_TASK_ANIMATION_DIC(tempPed,iped,"move_aim_strafe_crouch_2h", "idle")
						WEAPON_TYPE wtCurrentWeapon
						IF NOT GET_CURRENT_PED_WEAPON(tempPed, wtCurrentWeapon)
						OR wtCurrentWeapon = WEAPONTYPE_UNARMED
							SET_CURRENT_PED_WEAPON(tempPed, GET_BEST_PED_WEAPON(tempPed), TRUE)
						ENDIF
					ENDIF
					
					SET_PED_DUCKING(tempPed, TRUE)
				ENDIF
			BREAK
			CASE ciPED_IDLE_ANIM__STAND_AIM
				
				IF NOT IS_PED_IN_COMBAT(tempPed)
					IF bisSpecialAnim
					AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnim = ciPED_IDLE_ANIM__CROUCH_AIM
						IF NOT IS_BIT_SET(iPedSpecAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							
							REQUEST_ANIM_DICT("move_aim_strafe_crouch_2h")
							IF HAS_ANIM_DICT_LOADED("move_aim_strafe_crouch_2h")
								SET_BIT(iPedSpecAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							ENDIF
							
							PLAY_TASK_ANIMATION_DIC(tempPed,iped, "move_aim_strafe_crouch_2h", "idle_transition", SLOW_BLEND_IN, DEFAULT, AF_DEFAULT, TRUE)
						ENDIF
						
						IF IS_ENTITY_PLAYING_ANIM(tempPed, "move_aim_strafe_crouch_2h", "idle_transition")
						AND GET_ENTITY_ANIM_CURRENT_TIME(tempPed, "move_aim_strafe_crouch_2h", "idle_transition") >= 0.4
							PLAY_TASK_ANIMATION_DIC(tempPed,iped, "combat@aim_variations@cop", "fwd_refocus", SLOW_BLEND_IN, DEFAULT, AF_LOOPING, TRUE)
						ENDIF
						
						SET_PED_RESET_FLAG(tempPed, PRF_InstantBlendToAim, TRUE)
					ELSE
						VECTOR vAimCoords
						vAimCoords = GET_ENTITY_COORDS(tempPed) + (GET_ENTITY_FORWARD_VECTOR(tempPed) * 10.0)
						IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_AIM_GUN_AT_COORD)
							TASK_AIM_GUN_AT_COORD(tempPed, vAimCoords, 9999999, FALSE, FALSE)
							
							WEAPON_TYPE wtCurrentWeapon
							IF NOT GET_CURRENT_PED_WEAPON(tempPed, wtCurrentWeapon)
							OR wtCurrentWeapon = WEAPONTYPE_UNARMED
								SET_CURRENT_PED_WEAPON(tempPed, GET_BEST_PED_WEAPON(tempPed), TRUE)
							ENDIF
						ELSE
							SET_PED_RESET_FLAG(tempPed, PRF_InstantBlendToAim, TRUE)
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE ciPED_IDLE_ANIM__GANG_SMG_BECKON
				BOOL bCanBeckon
				IF bisSpecialAnim
					bCanBeckon = NOT IS_BIT_SET(iPedSpecAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				ELSE
					bCanBeckon = NOT IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				ENDIF
				
				IF bCanBeckon
					IF PLAY_TASK_ANIMATION_DIC(tempPed,iped,"combat@gestures@gang@smg@beckon", "0", DEFAULT, DEFAULT, AF_DEFAULT)
						IF bisSpecialAnim
							SET_BIT(iPedSpecAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						ELSE
							SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						ENDIF
					ENDIF
				ELSE
					PRINTLN("[RCC Mission] TASK_PERFORM_AMBIENT_ANIMATION - Ped ", iPed, " has already beckoned!")
				ENDIF
			BREAK
			
			CASE ciPED_IDLE_ANIM__CLEANING_A
				PLAY_SCENARIO_ON_PED_WITH_SPECIFIC_ANIM(tempPed, iPed, "WORLD_HUMAN_MAID_CLEAN", "amb@world_human_maid_clean@idle_a", "AMB@WORLD_HUMAN_MAID_CLEAN@IDLES_A", "idle_a")
			BREAK
			CASE ciPED_IDLE_ANIM__CLEANING_B
				PLAY_SCENARIO_ON_PED_WITH_SPECIFIC_ANIM(tempPed, iPed, "WORLD_HUMAN_MAID_CLEAN", "amb@world_human_maid_clean@idle_a", "AMB@WORLD_HUMAN_MAID_CLEAN@IDLES_A", "idle_b")
			BREAK
			CASE ciPED_IDLE_ANIM__CLEANING_C
				PLAY_SCENARIO_ON_PED_WITH_SPECIFIC_ANIM(tempPed, iPed, "WORLD_HUMAN_MAID_CLEAN", "amb@world_human_maid_clean@idle_a", "AMB@WORLD_HUMAN_MAID_CLEAN@IDLES_A", "idle_c")
			BREAK
			CASE ciPED_IDLE_ANIM__CLEANING_D
				PLAY_SCENARIO_ON_PED_WITH_SPECIFIC_ANIM(tempPed, iPed, "WORLD_HUMAN_MAID_CLEAN", "amb@world_human_maid_clean@idle_b", "AMB@WORLD_HUMAN_MAID_CLEAN@IDLES_b", "idle_d")
			BREAK
			CASE ciPED_IDLE_ANIM__CLEANING_E
				PLAY_SCENARIO_ON_PED_WITH_SPECIFIC_ANIM(tempPed, iPed, "WORLD_HUMAN_MAID_CLEAN", "amb@world_human_maid_clean@idle_b", "AMB@WORLD_HUMAN_MAID_CLEAN@IDLES_b", "idle_e")
			BREAK
			
			CASE ciPED_IDLE_ANIM__PHONE_TEXTING
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_START_SCENARIO_IN_PLACE)
					PRINTLN("[RCC Mission] TASK_PERFORM_AMBIENT_ANIMATION - Ped ", iPed, " starting WORLD_HUMAN_STAND_MOBILE scenario!")
					TASK_START_SCENARIO_IN_PLACE(tempPed, "WORLD_HUMAN_STAND_MOBILE", 0, TRUE)
				ENDIF
			BREAK
			CASE ciPED_IDLE_ANIM__VALET
				PLAY_TASK_ANIMATION_DIC(tempPed,iped,"anim@amb@world_human_valet@formal_left@base@", "base_a_m_y_vinewood_01")
			BREAK
			CASE ciPED_IDLE_ANIM__VALET_LOOK_LEFT
				PLAY_TASK_ANIMATION_DIC(tempPed,iped,"anim@amb@world_human_valet@formal_left@idle_b@", "idle_e_a_m_y_vinewood_01")
			BREAK
			CASE ciPED_IDLE_ANIM__VALET_LOOK_RIGHT
				PLAY_TASK_ANIMATION_DIC(tempPed,iped,"anim@amb@world_human_valet@formal_left@idle_b@", "idle_f_a_m_y_vinewood_01")
			BREAK
		ENDSWITCH
	ELSE
		PRINTLN("[RCC Mission] TASK_PERFORM_AMBIENT_ANIMATION - Ped ", iPed, " Using custom anim: ", strCustomAnimDict, " | ", strCustomAnimName)
		PLAY_TASK_ANIMATION_DIC(tempPed, iped, strCustomAnimDict, strCustomAnimName)
	ENDIF
	
	BOOL bIsPerformingAnim = IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
	BOOL bIsPerformingSpecialAnim = IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
	PRINTLN("[RCC Mission] TASK_PERFORM_AMBIENT_ANIMATION - Finished - iped: ", iped, ", iAnimation: ", iAnimation, ", bAnimating: ", bAnimating, ", bIsPerformingAnim", bIsPerformingAnim, " - bIsPerformingSpecialAnim: ", bIsPerformingSpecialAnim)
	RETURN bAnimating
	
ENDFUNC

PROC PROCESS_JUST_OWNER_CLEANUP(INT iPed)
	SWITCH sMissionPedsLocalVars[iPed].ePedSyncSceneState
		CASE	PED_SYNC_SCENE_STATE_CLEANUP
			CLEANUP_SYNC_SCENE_OWNER_DATA(iPed)
			CLEANUP_SYNC_SCENE_LOCAL_DATA(iPed)
			SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_PROCESS_END)
		BREAK
	ENDSWITCH
ENDPROC

PROC PROCESS_JUST_LOCAL_CLEANUP(INT iPed)
	SWITCH sMissionPedsLocalVars[iPed].ePedSyncSceneState
		CASE	PED_SYNC_SCENE_STATE_CLEANUP
			IF NOT IS_PARTICIPANT_SYNC_SCENE_PED_OWNER(iLocalPart, iPed)
				CLEANUP_SYNC_SCENE_LOCAL_DATA(iPed)
				SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_PROCESS_END)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC PROCESS_EVERY_FRAME_PROPERTIES_FOR_SYNC_SCENE_ANIMATED_PEDS(INT iPed)
	NETWORK_INDEX netPed
	PED_INDEX pedToCheck	
	netPed = MC_serverBD_1.sFMMC_SBD.niPed[iPed]
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(netPed)
		EXIT
	ENDIF
	pedToCheck = NET_TO_PED(netPed)
	IF NOT DOES_ENTITY_EXIST(pedToCheck)
		EXIT
	ENDIF
	IF IS_PED_INJURED(pedtoCheck)
		EXIT
	ENDIF
	IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(netPed)
		EXIT
	ENDIF
	
	SET_PED_RESET_FLAG(pedtoCheck, PRF_CannotBeTargetedByAI, TRUE)
ENDPROC

// This was going to be a generic system but too many naunces and slight special cases had to go in various states which would have just made it harder to read by functionalising the state processing..... :(
PROC PROCESS_EVERY_FRAME_OWNER_PLAYER_SYNC_SCENE_PEDS(INT iPed)	
		
	STRING strAnimDict
	VECTOR vCoords, vRot
	BOOL bIsPerformingSpecialAnim = IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
	
	INT iAnimation	
	IF bIsPerformingSpecialAnim
		iAnimation = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnim
	ELSE
		iAnimation = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnim
	ENDIF
	
	IF NOT IS_THIS_A_SPECIAL_SYNC_ANIMATION(iAnimation)
		EXIT
	ENDIF
	
	// IMPORTANT: We define in script who is the owner and who the new ownership should be if someone leave sso that we can manage giving ownership of all the peds to one player who is essentially the owner and controller for that particlar sync animation.
	IF NOT IS_PARTICIPANT_SYNC_SCENE_PED_OWNER(iLocalPart, iPed)
		
		// Not the script defined owner, but might be the network entity owner, relinquish ownership.
		RELINQUISH_SYNC_SCENE_ENTITY_OWNERSHIP(iPed)
		
		IF IS_PARTICIPANT_SYNC_SCENE_PED_OWNER_SET(iPed)
			IF NOT IS_PARTICIPANT_SYNC_SCENE_PED_OWNER_ACTIVE(iPed)
				INT iNewOwner = GET_CLOSEST_PARTICIPANT_NUMBER_ASCENDING(GET_PARTICIPANT_SYNC_SCENE_PED_OWNER(iPed))
				
				PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation), "][OWNER] - PROCESS_PED_ANIMATION - Current Anim Owner: ", GET_PARTICIPANT_SYNC_SCENE_PED_OWNER(iPed), " is not active and Best new owner is: ", iNewOwner)
				
				IF iNewOwner = iLocalPart
					SET_PARTICIPANT_AS_SYNC_SCENE_PED_OWNER(iLocalPart, iPed)
					CLEAR_BIT(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_iSyncSceneAnimationReady)
					IF sMissionPedsLocalVars[iPed].ePedSyncSceneState < PED_SYNC_SCENE_STATE_CLEANUP
						PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation), "][OWNER] - PROCESS_PED_ANIMATION - Pushing back to start as Migrating made it dirty!")
						SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_INIT)
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetPedSyncSceneState, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ENUM_TO_INT(PED_SYNC_SCENE_STATE_INIT))
						sMissionPedsLocalVars[iPed].iSyncScene = -1
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SyncSceneBroadcast, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, -1)
						SET_BIT(sMissionPedsLocalVars[iPed].iPedBS,  ci_MissionPedBS_iSyncSceneAnimationNeedResync)
					ENDIF
				ENDIF
			ENDIF
		ENDIF		
		
		PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation), "][OWNER] - PROCESS_PED_ANIMATION - Exitting due to NOT being the owner. Owner is Participant: ", GET_PARTICIPANT_SYNC_SCENE_PED_OWNER(iPed))
		EXIT
	ENDIF
	
	INT iLocalScene
	
	vCoords = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].vPos
	vRot = <<0, 0, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].fHead>>
	IK_CONTROL_FLAGS ikFlags
	ikFlags = AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK | AIK_DISABLE_HEAD_IK | AIK_DISABLE_TORSO_IK	| AIK_DISABLE_TORSO_REACT_IK
	
	PROCESS_EVERY_FRAME_PROPERTIES_FOR_SYNC_SCENE_ANIMATED_PEDS(iPed)
	
	SWITCH iAnimation
		// === Casino Vincent Intro ===
		CASE ciPED_IDLE_ANIM__CASINO_VINCENT_INTRO
			strAnimDict = "ANIM@AMB@CASINO@BRAWL@VINCENT@"
			IF NOT IS_NETWORKED_SYNC_SCENE_PED_READY(iPed, strAnimDict, ciPED_IDLE_ANIM__CASINO_VINCENT_INTRO, ciPED_IDLE_ANIM__CASINO_VINCENT_INTRO, TRUE #IF IS_DEBUG_BUILD , GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) #ENDIF )
				EXIT
			ENDIF
			
			// Assign our Indexes.				
			PED_INDEX pedVincent			
			pedVincent = GET_PED_INDEX_FROM_NET_ID(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
			
			IF sMissionPedsLocalVars[iPed].ePedSyncSceneState >= PED_SYNC_SCENE_STATE_INIT
			AND sMissionPedsLocalVars[iPed].ePedSyncSceneState < PED_SYNC_SCENE_STATE_CLEANUP
				IF NOT ARE_LINKED_SYNC_PEDS_ALIVE(iPed)
					SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_CLEANUP)
					EXIT
				ENDIF
			ENDIF
			
			IF IS_PED_WALKING(pedVincent)
				PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Waiting until we stopped walking...")
				EXIT
			ENDIF
			
			IF CONTENT_IS_USING_CASINO()
				//vCoords = <<1122.351, 263.5161, -50.8909>> old anim
				//vRot = <<0, 0, 90.0>>
				vCoords = <<1114.8484, 234.5277, -50.8409>>
				vRot = <<0, 0, 108.0>>
			ENDIF
			
			SWITCH sMissionPedsLocalVars[iPed].ePedSyncSceneState
				CASE	PED_SYNC_SCENE_STATE_INIT
					
					// Create the Sync Scene
					IF sMissionPedsLocalVars[iPed].iSyncScene = -1
						PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Creating the Sync Scene with vCoords: ", vCoords, " vRot: ", vRot)
						
						INITIALIZE_LINKED_SYNC_PEDS_STATE(iPed, FALSE)
						
						sMissionPedsLocalVars[iPed].iSyncScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCoords, vRot, EULER_XYZ, FALSE, TRUE)
						
						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedVincent, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "WALK_AND_TALK_VINCENT_S_M_Y_Doorman_01", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedVincent, TRUE)						
						NETWORK_START_SYNCHRONISED_SCENE(sMissionPedsLocalVars[iPed].iSyncScene)
						
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SyncSceneBroadcast, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, sMissionPedsLocalVars[iPed].iSyncScene)
						SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT)
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetPedSyncSceneState, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ENUM_TO_INT(PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT))
					ENDIF					
				BREAK	
				
				CASE	PED_SYNC_SCENE_STATE_PROCESS_START
					//WALK_AND_TALK_VINCENT_vw_Prop_VW_Casino_Door_01a_LEFT
				BREAK
				
				CASE	PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT
					
					// The Scene is Running	
					iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sMissionPedsLocalVars[iPed].iSyncScene)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
						PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Breakout - Scene Playing. Phase: ", GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene))
						
						IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene) >= 0.975
							PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Breakout - Scene Finished")
							SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_CLEANUP)
							BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetPedSyncSceneState, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ENUM_TO_INT(PED_SYNC_SCENE_STATE_CLEANUP))
						ENDIF
					ELSE
						PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - NOT IS_SYNCHRONIZED_SCENE_RUNNING")
						SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_CLEANUP)
					ENDIF
				BREAK
			
				CASE	PED_SYNC_SCENE_STATE_CLEANUP
					CLEANUP_SYNC_SCENE_OWNER_DATA(iPed)
					CLEANUP_SYNC_SCENE_LOCAL_DATA(iPed)
					SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_PROCESS_END)
				BREAK				
					
			ENDSWITCH
		BREAK
		// ===============================================	
		
		// === Casino Fight breakout ====================
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_BREAKOUT_1
			strAnimDict = "ANIM@AMB@CASINO@BRAWL@FIGHTS@BREAKOUT@"			
			IF NOT IS_NETWORKED_SYNC_SCENE_PED_READY(iPed, strAnimDict, ciPED_IDLE_ANIM__CASINO_FIGHT_BREAKOUT_1, ciPED_IDLE_ANIM__CASINO_FIGHT_BREAKOUT_1_PEDESTRIAN, TRUE #IF IS_DEBUG_BUILD , GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) #ENDIF )
				EXIT
			ENDIF
			
			// Assign our Indexes.
			PED_INDEX pedBreakoutBrawler, pedBreakoutCroupier, pedBreakoutPedestrian
			INT iPedBreakoutCroupierSlot, iPedBreakoutPedestrianSlot
			iPedBreakoutCroupierSlot = (ciPED_IDLE_ANIM__CASINO_FIGHT_BREAKOUT_1_CROUPIER - ciPED_IDLE_ANIM__CASINO_FIGHT_BREAKOUT_1)
			iPedBreakoutPedestrianSlot = (ciPED_IDLE_ANIM__CASINO_FIGHT_BREAKOUT_1_PEDESTRIAN - ciPED_IDLE_ANIM__CASINO_FIGHT_BREAKOUT_1)
			
			pedBreakoutBrawler = GET_PED_INDEX_FROM_NET_ID(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
			pedBreakoutCroupier = GET_PED_INDEX_FROM_NET_ID(MC_serverBD_1.sFMMC_SBD.niPed[sMissionPedsLocalVars[iPed].iSyncedPeds[iPedBreakoutCroupierSlot]])
			pedBreakoutPedestrian = GET_PED_INDEX_FROM_NET_ID(MC_serverBD_1.sFMMC_SBD.niPed[sMissionPedsLocalVars[iPed].iSyncedPeds[iPedBreakoutPedestrianSlot]])
			
			IF sMissionPedsLocalVars[iPed].ePedSyncSceneState >= PED_SYNC_SCENE_STATE_INIT
			AND sMissionPedsLocalVars[iPed].ePedSyncSceneState < PED_SYNC_SCENE_STATE_CLEANUP
				IF NOT ARE_LINKED_SYNC_PEDS_ALIVE(iPed)
					PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Some peds aren't alive...")
					SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_CLEANUP)
					EXIT
				ENDIF
			ENDIF
			
			// Poker Table.
			IF CONTENT_IS_USING_CASINO()
				vCoords = <<1143.3379, 264.2453, -52.8409>>
				vRot = <<0, 0, -135.0>>
			ENDIF
			
			SWITCH sMissionPedsLocalVars[iPed].ePedSyncSceneState
				CASE	PED_SYNC_SCENE_STATE_INIT
					
					// Create the Sync Scene
					IF sMissionPedsLocalVars[iPed].iSyncScene = -1
						PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Creating the Sync Scene with vCoords: ", vCoords, " vRot: ", vRot)
						
						INITIALIZE_LINKED_SYNC_PEDS_STATE(iPed)
						
						sMissionPedsLocalVars[iPed].iSyncScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCoords, vRot, EULER_XYZ, FALSE, TRUE)
						
						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedBreakoutBrawler, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "ARGUMENT_LOOP_MP_M_BRAWLER", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)
						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedBreakoutCroupier, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "ARGUMENT_LOOP_MP_M_CROUPIER", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)						
						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedBreakoutPedestrian, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "ARGUMENT_LOOP_MP_F_PEDESTRIAN", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedBreakoutBrawler, TRUE)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedBreakoutCroupier, TRUE)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedBreakoutPedestrian, TRUE)
						NETWORK_START_SYNCHRONISED_SCENE(sMissionPedsLocalVars[iPed].iSyncScene)
						
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SyncSceneBroadcast, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, sMissionPedsLocalVars[iPed].iSyncScene)
						SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_PROCESS_START)
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetPedSyncSceneState, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ENUM_TO_INT(PED_SYNC_SCENE_STATE_PROCESS_START))
					ENDIF					
				BREAK
				
				CASE	PED_SYNC_SCENE_STATE_PROCESS_START
					
					// The Scene is Running
					iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sMissionPedsLocalVars[iPed].iSyncScene)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)											
						IF IS_ANY_PLAYER_INSIDE_BREAKOUT_RANGE(IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)), iPed, pedBreakoutBrawler)
							PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Timer Expired, Playing the Slam Anim.")
							
							sMissionPedsLocalVars[iPed].iSyncScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCoords, vRot, EULER_XYZ, FALSE, FALSE)
							NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedBreakoutBrawler, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "BREAKOUT_MP_M_BRAWLER", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)
							NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedBreakoutCroupier, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "BREAKOUT_MP_M_CROUPIER", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)						
							NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedBreakoutPedestrian, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "BREAKOUT_MP_F_PEDESTRIAN", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)						
							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedBreakoutBrawler, TRUE)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedBreakoutCroupier, TRUE)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedBreakoutPedestrian, TRUE)
							NETWORK_START_SYNCHRONISED_SCENE(sMissionPedsLocalVars[iPed].iSyncScene)
							
							BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SyncSceneBroadcast, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, sMissionPedsLocalVars[iPed].iSyncScene)
							SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT)
							BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetPedSyncSceneState, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ENUM_TO_INT(PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT))
						ENDIF
					ENDIF
				BREAK
				
				CASE	PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT
					
					// The Scene is Running	
					iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sMissionPedsLocalVars[iPed].iSyncScene)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
						PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Breakout - Scene Playing. Phase: ", GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene))
							
						IF CONTENT_IS_USING_CASINO()
							IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene) >= 0.4
							AND NOT IS_BIT_SET(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_iSyncSceneRequestRuleProgress)
								SET_BIT(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_iSyncSceneRequestRuleProgress)
								BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_RequestProgressToRule, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]+1)
							ENDIF
						ENDIF
						
						IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene) >= 0.925
							PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Breakout - Scene Finished")
							SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_CLEANUP)
							BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetPedSyncSceneState, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ENUM_TO_INT(PED_SYNC_SCENE_STATE_CLEANUP))
						ENDIF
					ELSE
						PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - NOT IS_SYNCHRONIZED_SCENE_RUNNING")
						SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_CLEANUP)
					ENDIF
				BREAK
			
				CASE	PED_SYNC_SCENE_STATE_CLEANUP
					CLEANUP_SYNC_SCENE_OWNER_DATA(iPed)
					
					PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Killing Knocked Out ped.")
					IF NOT IS_PED_INJURED(pedBreakoutCroupier)
						SET_PED_CONFIG_FLAG(pedBreakoutCroupier, PCF_ForceRagdollUponDeath, TRUE)
						SET_ENTITY_HEALTH(pedBreakoutCroupier, 0)
					ENDIF
										
					CLEANUP_SYNC_SCENE_LOCAL_DATA(iPed)					
					SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_PROCESS_END)
				BREAK				
					
			ENDSWITCH
		BREAK
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_BREAKOUT_1_CROUPIER
			PROCESS_JUST_OWNER_CLEANUP(iPed)
		BREAK
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_BREAKOUT_1_PEDESTRIAN
			PROCESS_JUST_OWNER_CLEANUP(iPed)
		BREAK
		// ===============================================	
		
		// === Casino Security Guard / Redneck Fight =====
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_SECURITY_GUARD
			strAnimDict = "ANIM@AMB@CASINO@BRAWL@FIGHTS@GUARD@"			
			IF NOT IS_NETWORKED_SYNC_SCENE_PED_READY(iPed, strAnimDict, ciPED_IDLE_ANIM__CASINO_FIGHT_SECURITY_GUARD, ciPED_IDLE_ANIM__CASINO_FIGHT_SECURITY_GUARD_REDNECK_2, TRUE #IF IS_DEBUG_BUILD , GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) #ENDIF )
				EXIT
			ENDIF
			
			// Assign our Indexes.
			PED_INDEX pedGuardVictim, pedGuardBrawler1, pedGuardBrawler2
			INT iPedGuardBrawlerSlot1, iPedGuardBrawlerSlot2
			iPedGuardBrawlerSlot1 = (ciPED_IDLE_ANIM__CASINO_FIGHT_SECURITY_GUARD_REDNECK_1 - ciPED_IDLE_ANIM__CASINO_FIGHT_SECURITY_GUARD)
			iPedGuardBrawlerSlot2 = (ciPED_IDLE_ANIM__CASINO_FIGHT_SECURITY_GUARD_REDNECK_2 - ciPED_IDLE_ANIM__CASINO_FIGHT_SECURITY_GUARD)
			
			pedGuardVictim = GET_PED_INDEX_FROM_NET_ID(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
			pedGuardBrawler1 = GET_PED_INDEX_FROM_NET_ID(MC_serverBD_1.sFMMC_SBD.niPed[sMissionPedsLocalVars[iPed].iSyncedPeds[iPedGuardBrawlerSlot1]])
			pedGuardBrawler2 = GET_PED_INDEX_FROM_NET_ID(MC_serverBD_1.sFMMC_SBD.niPed[sMissionPedsLocalVars[iPed].iSyncedPeds[iPedGuardBrawlerSlot2]])
			
			IF sMissionPedsLocalVars[iPed].ePedSyncSceneState >= PED_SYNC_SCENE_STATE_INIT
			AND sMissionPedsLocalVars[iPed].ePedSyncSceneState < PED_SYNC_SCENE_STATE_CLEANUP
				IF NOT ARE_LINKED_SYNC_PEDS_ALIVE(iPed)
					SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_CLEANUP)
					EXIT
				ENDIF
			ENDIF			
			
			vCoords = g_FMMC_STRUCT_ENTITIES.sPlacedPed[sMissionPedsLocalVars[iPed].iSyncedPeds[iPedGuardBrawlerSlot1]].vPos
			vRot = <<0, 0, g_FMMC_STRUCT_ENTITIES.sPlacedPed[sMissionPedsLocalVars[iPed].iSyncedPeds[iPedGuardBrawlerSlot1]].fHead>>
			
			SWITCH sMissionPedsLocalVars[iPed].ePedSyncSceneState
				CASE	PED_SYNC_SCENE_STATE_INIT
					
					// Create the Sync Scene
					IF sMissionPedsLocalVars[iPed].iSyncScene = -1
						PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Creating the Sync Scene with vCoords: ", vCoords, " vRot: ", vRot)
						
						INITIALIZE_LINKED_SYNC_PEDS_STATE(iPed)
												
						sMissionPedsLocalVars[iPed].iSyncScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCoords, vRot, EULER_XYZ, FALSE, TRUE)
						
						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedGuardVictim, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "ARGUMENT_LOOP_MP_M_GUARD", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)
						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedGuardBrawler1, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "ARGUMENT_LOOP_MP_M_BRAWLER_01", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)						
						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedGuardBrawler2, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "ARGUMENT_LOOP_MP_M_BRAWLER_02", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedGuardVictim, TRUE)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedGuardBrawler1, TRUE)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedGuardBrawler2, TRUE)
						NETWORK_START_SYNCHRONISED_SCENE(sMissionPedsLocalVars[iPed].iSyncScene)
						
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SyncSceneBroadcast, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, sMissionPedsLocalVars[iPed].iSyncScene)
						SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_PROCESS_START)
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetPedSyncSceneState, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ENUM_TO_INT(PED_SYNC_SCENE_STATE_PROCESS_START))
					ENDIF					
				BREAK
				
				CASE	PED_SYNC_SCENE_STATE_PROCESS_START
															
					// The Scene is Running
					iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sMissionPedsLocalVars[iPed].iSyncScene)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)											
						IF IS_ANY_PLAYER_INSIDE_BREAKOUT_RANGE(IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)), iPed, pedGuardVictim)
							PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Timer Expired, Playing the Guard anim")
							
							sMissionPedsLocalVars[iPed].iSyncScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCoords, vRot, EULER_XYZ, FALSE, FALSE)
							NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedGuardVictim, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "BREAKOUT_MP_M_GUARD", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)
							NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedGuardBrawler1, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "BREAKOUT_MP_M_BRAWLER_01", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)						
							NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedGuardBrawler2, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "BREAKOUT_MP_M_BRAWLER_02", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)						
							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedGuardVictim, TRUE)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedGuardBrawler1, TRUE)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedGuardBrawler2, TRUE)
							NETWORK_START_SYNCHRONISED_SCENE(sMissionPedsLocalVars[iPed].iSyncScene)
							
							BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SyncSceneBroadcast, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, sMissionPedsLocalVars[iPed].iSyncScene)
							SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT)
							BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetPedSyncSceneState, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ENUM_TO_INT(PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT))
						ENDIF
					ENDIF
				BREAK
				
				CASE	PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT
					
					// The Scene is Running	
					iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sMissionPedsLocalVars[iPed].iSyncScene)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
						PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Breakout - Scene Playing. Phase: ", GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene))
						
						IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene) >= 0.925
							PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Breakout - Scene Finished")
							SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_CLEANUP)
							BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetPedSyncSceneState, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ENUM_TO_INT(PED_SYNC_SCENE_STATE_CLEANUP))
						ENDIF
					ELSE
						PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - NOT IS_SYNCHRONIZED_SCENE_RUNNING")
						SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_CLEANUP)
					ENDIF
				BREAK
			
				CASE	PED_SYNC_SCENE_STATE_CLEANUP
					CLEANUP_SYNC_SCENE_OWNER_DATA(iPed)
					
					PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Killing Knocked Out ped.")
					IF NOT IS_PED_INJURED(pedGuardVictim)
						SET_PED_CONFIG_FLAG(pedGuardVictim, PCF_ForceRagdollUponDeath, TRUE)
						SET_ENTITY_HEALTH(pedGuardVictim, 0)
					ENDIF
										
					CLEANUP_SYNC_SCENE_LOCAL_DATA(iPed)					
					SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_PROCESS_END)
				BREAK				
					
			ENDSWITCH
		BREAK
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_SECURITY_GUARD_REDNECK_1
			PROCESS_JUST_OWNER_CLEANUP(iPed)
		BREAK
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_SECURITY_GUARD_REDNECK_2
			PROCESS_JUST_OWNER_CLEANUP(iPed)
		BREAK
		// ===============================================	
		
		// === Casino Fight/Conversation =================
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CONVERSATION
			strAnimDict = "ANIM@AMB@CASINO@BRAWL@FIGHTS@CONVERSATION@"
		
			IF NOT IS_NETWORKED_SYNC_SCENE_PED_READY(iPed, strAnimDict, ciPED_IDLE_ANIM__CASINO_FIGHT_CONVERSATION, ciPED_IDLE_ANIM__CASINO_FIGHT_CONVERSATION_SECOND, TRUE #IF IS_DEBUG_BUILD , GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) #ENDIF )
				EXIT
			ENDIF
			
			// Assign our Indexes.
			INT iSecondPedSlot
			iSecondPedSlot = (ciPED_IDLE_ANIM__CASINO_FIGHT_CONVERSATION_SECOND - ciPED_IDLE_ANIM__CASINO_FIGHT_CONVERSATION)
			
			PED_INDEX pedSecond, pedBottle
			pedBottle = GET_PED_INDEX_FROM_NET_ID(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
			pedSecond = GET_PED_INDEX_FROM_NET_ID(MC_serverBD_1.sFMMC_SBD.niPed[sMissionPedsLocalVars[iPed].iSyncedPeds[iSecondPedSlot]])			
			
			IF sMissionPedsLocalVars[iPed].ePedSyncSceneState >= PED_SYNC_SCENE_STATE_INIT
			AND sMissionPedsLocalVars[iPed].ePedSyncSceneState < PED_SYNC_SCENE_STATE_CLEANUP
				IF NOT ARE_LINKED_SYNC_PEDS_ALIVE(iPed)
					SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_CLEANUP)
					EXIT
				ENDIF
			ENDIF
			
			SWITCH sMissionPedsLocalVars[iPed].ePedSyncSceneState
				CASE	PED_SYNC_SCENE_STATE_INIT
				
					// Create the Sync Scene
					IF sMissionPedsLocalVars[iPed].iSyncScene = -1
						PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Creating the Sync Scene with vCoords: ", vCoords, " vRot: ", vRot)
						
						INITIALIZE_LINKED_SYNC_PEDS_STATE(iPed)
						
						sMissionPedsLocalVars[iPed].iSyncScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCoords, vRot, EULER_XYZ, FALSE, TRUE)						
						
						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedSecond, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "WATCHING_LOOP_MP_M_BRAWLER", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)
						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedBottle, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "WATCHING_LOOP_MP_M_PEDESTRIAN", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedSecond, TRUE)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedBottle, TRUE)
						NETWORK_START_SYNCHRONISED_SCENE(sMissionPedsLocalVars[iPed].iSyncScene)
						
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SyncSceneBroadcast, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, sMissionPedsLocalVars[iPed].iSyncScene)						
						SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_PROCESS_START)
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetPedSyncSceneState, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ENUM_TO_INT(PED_SYNC_SCENE_STATE_PROCESS_START))
					ENDIF					
				BREAK
				
				CASE	PED_SYNC_SCENE_STATE_PROCESS_START
				
					// The Scene is Running
					iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sMissionPedsLocalVars[iPed].iSyncScene)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
					
						// Local Clients Load Prop.
						IF NOT DOES_ENTITY_EXIST(oiCasinoBrawlBottleObject)
							PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Creating oiGenericIndexForSyncScene1 - Prop_CS_Beer_Bot_01")
							oiCasinoBrawlBottleObject = CREATE_OBJECT(Prop_CS_Beer_Bot_01, vCoords, FALSE, FALSE)
							EXIT
						ENDIF
						
						// Attach
						IF NOT IS_ENTITY_ATTACHED_TO_ANY_PED(oiCasinoBrawlBottleObject)						
							IF DOES_ENTITY_EXIST(oiCasinoBrawlBottleObject)
								INT iBone
								iBone = GET_PED_BONE_INDEX(pedBottle, BONETAG_PH_R_HAND)								
								IF iBone > -1
									PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Attaching the bottle locally.")
									ATTACH_ENTITY_TO_ENTITY(oiCasinoBrawlBottleObject, pedBottle, iBone, (<<0.0, 0.0, 0.0>>), (<<0.0, 0.0, 0.0>>), TRUE, TRUE, FALSE)
								ELSE
									PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - iBone is -1 - ERROR.")
								ENDIF
							ENDIF
							EXIT
						ENDIF
						
						IF IS_ANY_PLAYER_INSIDE_BREAKOUT_RANGE(IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)), iPed, pedBottle)
							PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][FIGHT_CONVERSATION] - PROCESS_PED_ANIMATION - Timer Expired, playing one of the two breakouts")
							
							IF iCasinoBrawlBottleOutcome = -1
								iCasinoBrawlBottleOutcome = GET_RANDOM_INT_IN_RANGE(0, 100)
							ENDIF
							
							sMissionPedsLocalVars[iPed].iSyncScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCoords, vRot, EULER_XYZ, FALSE, FALSE)
							
							// Breakout - Both Fight 
							IF iCasinoBrawlBottleOutcome >= 50
								PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Breakout - Both Fight")
								NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedSecond, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "BREAKOUT_BothFight_MP_M_BRAWLER", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)
								NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedBottle, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "BREAKOUT_BothFight_MP_M_PEDESTRIAN", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)
								
							// Breakout - One Knocks Other
							ELSE
								PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Breakout - One Knocks Other")
								NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedSecond, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "BREAKOUT_OneKnocksOther_MP_M_BRAWLER", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)
								NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedBottle, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "BREAKOUT_OneKnocksOther_MP_M_PEDESTRIAN", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)
							ENDIF
							
							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedSecond, TRUE)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedBottle, TRUE)						
							NETWORK_START_SYNCHRONISED_SCENE(sMissionPedsLocalVars[iPed].iSyncScene)
							
							BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SyncSceneBroadcast, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, sMissionPedsLocalVars[iPed].iSyncScene)							
							SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT)
							BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetPedSyncSceneState, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ENUM_TO_INT(PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT))
						ENDIF
					ENDIF
				BREAK
				
				CASE	PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT
					
					// The Scene is Running	
					iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sMissionPedsLocalVars[iPed].iSyncScene)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
						PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Breakout - Scene Playing. Phase: ", GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene))
						
						IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene) >= 0.925
							PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Breakout - Scene Finished")
							SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_CLEANUP)
							BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetPedSyncSceneState, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ENUM_TO_INT(PED_SYNC_SCENE_STATE_CLEANUP))
						ENDIF
					ELSE
						PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - NOT IS_SYNCHRONIZED_SCENE_RUNNING")
						SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_CLEANUP)
					ENDIF
				BREAK
			
				CASE	PED_SYNC_SCENE_STATE_CLEANUP
					CLEANUP_SYNC_SCENE_OWNER_DATA(iPed)
					
					IF iCasinoBrawlBottleOutcome < 50
					AND iCasinoBrawlBottleOutcome > -1
						PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Killing Knocked Out ped.")
						IF NOT IS_PED_INJURED(pedBottle)
							SET_PED_CONFIG_FLAG(pedBottle, PCF_ForceRagdollUponDeath, TRUE)
							SET_ENTITY_HEALTH(pedBottle, 0)
						ENDIF
					ELSE
						PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Nothing: ", iCasinoBrawlBottleOutcome)
					ENDIF
										
					IF DOES_ENTITY_EXIST(oiCasinoBrawlBottleObject)
						PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Cleaning up the Bottle Prop - oiCasinoBrawlBottleObject.")
						DELETE_OBJECT(oiCasinoBrawlBottleObject)
					ENDIF
					
					CLEANUP_SYNC_SCENE_LOCAL_DATA(iPed)
					SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_PROCESS_END)
				BREAK
				
			ENDSWITCH
		BREAK
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CONVERSATION_SECOND
			PROCESS_JUST_OWNER_CLEANUP(iPed)
		BREAK		
		// ===============================================
		
		// === Casino Fight Headlock =================
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_HEADLOCK
			strAnimDict = "ANIM@AMB@CASINO@BRAWL@FIGHTS@HEADLOCK@"			
			IF NOT IS_NETWORKED_SYNC_SCENE_PED_READY(iPed, strAnimDict, ciPED_IDLE_ANIM__CASINO_FIGHT_HEADLOCK, ciPED_IDLE_ANIM__CASINO_FIGHT_HEADLOCK_VICTIM, TRUE #IF IS_DEBUG_BUILD , GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) #ENDIF )
				EXIT
			ENDIF
			
			// Assign our Indexes.	
			INT iHeadlockVictimPedSlot
			PED_INDEX pedHeadlockBrawler, pedHeadlockVictim
			iHeadlockVictimPedSlot = (ciPED_IDLE_ANIM__CASINO_FIGHT_HEADLOCK_VICTIM - ciPED_IDLE_ANIM__CASINO_FIGHT_HEADLOCK)
			
			pedHeadlockBrawler = GET_PED_INDEX_FROM_NET_ID(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
			pedHeadlockVictim = GET_PED_INDEX_FROM_NET_ID(MC_serverBD_1.sFMMC_SBD.niPed[sMissionPedsLocalVars[iPed].iSyncedPeds[iHeadlockVictimPedSlot]])
			
			IF sMissionPedsLocalVars[iPed].ePedSyncSceneState >= PED_SYNC_SCENE_STATE_INIT
			AND sMissionPedsLocalVars[iPed].ePedSyncSceneState < PED_SYNC_SCENE_STATE_CLEANUP
				IF NOT ARE_LINKED_SYNC_PEDS_ALIVE(iPed)
					SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_CLEANUP)
					EXIT
				ENDIF
			ENDIF
			
			SWITCH sMissionPedsLocalVars[iPed].ePedSyncSceneState
				CASE	PED_SYNC_SCENE_STATE_INIT
					
					// Create the Sync Scene
					IF sMissionPedsLocalVars[iPed].iSyncScene = -1
						PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Creating the Sync Scene with vCoords: ", vCoords, " vRot: ", vRot)
						
						INITIALIZE_LINKED_SYNC_PEDS_STATE(iPed)
						
						sMissionPedsLocalVars[iPed].iSyncScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCoords, vRot, EULER_XYZ, FALSE, TRUE)
						
						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedHeadlockBrawler, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "HEADLOCK_LOOP_MP_M_BRAWLER", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)
						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedHeadlockVictim, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "HEADLOCK_LOOP_MP_M_PEDESTRIAN", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedHeadlockBrawler, TRUE)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedHeadlockVictim, TRUE)						
						NETWORK_START_SYNCHRONISED_SCENE(sMissionPedsLocalVars[iPed].iSyncScene)
						
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SyncSceneBroadcast, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, sMissionPedsLocalVars[iPed].iSyncScene)
						SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_PROCESS_START)
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetPedSyncSceneState, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ENUM_TO_INT(PED_SYNC_SCENE_STATE_PROCESS_START))
					ENDIF					
				BREAK
				
				CASE	PED_SYNC_SCENE_STATE_PROCESS_START
					
					// The Scene is Running
					iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sMissionPedsLocalVars[iPed].iSyncScene)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)											
						IF IS_ANY_PLAYER_INSIDE_BREAKOUT_RANGE(IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)), iPed, pedHeadlockBrawler)
							PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Timer Expired, Playing the Slam Anim.")
							
							sMissionPedsLocalVars[iPed].iSyncScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCoords, vRot, EULER_XYZ, FALSE, FALSE)
							NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedHeadlockBrawler, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "BREAKOUT_MP_M_BRAWLER", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)
							NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedHeadlockVictim, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "BREAKOUT_MP_M_PEDESTRIAN", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedHeadlockBrawler, TRUE)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedHeadlockVictim, TRUE)
							NETWORK_START_SYNCHRONISED_SCENE(sMissionPedsLocalVars[iPed].iSyncScene)
							
							BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SyncSceneBroadcast, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, sMissionPedsLocalVars[iPed].iSyncScene)
							SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT)
							BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetPedSyncSceneState, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ENUM_TO_INT(PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT))
						ENDIF
					ENDIF
				BREAK
				
				CASE	PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT
					
					// The Scene is Running	
					iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sMissionPedsLocalVars[iPed].iSyncScene)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
						PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Breakout - Scene Playing. Phase: ", GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene))
						
						IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene) >= 0.925
							PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Breakout - Scene Finished")
							SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_CLEANUP)
							BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetPedSyncSceneState, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ENUM_TO_INT(PED_SYNC_SCENE_STATE_CLEANUP))
						ENDIF
					ELSE
						PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - NOT IS_SYNCHRONIZED_SCENE_RUNNING")
						SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_CLEANUP)
					ENDIF
				BREAK
			
				CASE	PED_SYNC_SCENE_STATE_CLEANUP
					CLEANUP_SYNC_SCENE_OWNER_DATA(iPed)
					
					PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Killing Knocked Out ped.")
					
					IF NOT IS_PED_INJURED(pedHeadlockVictim)
						SET_PED_CONFIG_FLAG(pedHeadlockVictim, PCF_ForceRagdollUponDeath, TRUE)
						SET_ENTITY_HEALTH(pedHeadlockVictim, 0)
					ENDIF
					
					CLEANUP_SYNC_SCENE_LOCAL_DATA(iPed)
					SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_PROCESS_END)
				BREAK
					
			ENDSWITCH
		BREAK
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_HEADLOCK_VICTIM
			PROCESS_JUST_OWNER_CLEANUP(iPed)
		BREAK
		
		// === Casino Fight/Argue =========================
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_ARGUE
			strAnimDict = "ANIM@AMB@CASINO@BRAWL@FIGHTS@ARGUE@"
			IF NOT IS_NETWORKED_SYNC_SCENE_PED_READY(iPed, strAnimDict, ciPED_IDLE_ANIM__CASINO_FIGHT_ARGUE, ciPED_IDLE_ANIM__CASINO_FIGHT_ARGUE_VICTIM, TRUE #IF IS_DEBUG_BUILD , GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) #ENDIF )
				EXIT
			ENDIF
			
			// Assign our Indexes.
			PED_INDEX pedArgueBrawler, pedArgueVictim
			INT iArgueVictimPedSlot
			iArgueVictimPedSlot = (ciPED_IDLE_ANIM__CASINO_FIGHT_ARGUE_VICTIM - ciPED_IDLE_ANIM__CASINO_FIGHT_ARGUE)
		
			pedArgueBrawler = GET_PED_INDEX_FROM_NET_ID(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
			pedArgueVictim = GET_PED_INDEX_FROM_NET_ID(MC_serverBD_1.sFMMC_SBD.niPed[sMissionPedsLocalVars[iPed].iSyncedPeds[iArgueVictimPedSlot]])
			
			IF sMissionPedsLocalVars[iPed].ePedSyncSceneState >= PED_SYNC_SCENE_STATE_INIT
			AND sMissionPedsLocalVars[iPed].ePedSyncSceneState < PED_SYNC_SCENE_STATE_CLEANUP
				IF NOT ARE_LINKED_SYNC_PEDS_ALIVE(iPed)
					SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_CLEANUP)
					EXIT
				ENDIF
			ENDIF
			
			SWITCH sMissionPedsLocalVars[iPed].ePedSyncSceneState
				CASE	PED_SYNC_SCENE_STATE_INIT
					
					// Create the Sync Scene
					IF sMissionPedsLocalVars[iPed].iSyncScene = -1
						PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Creating the Sync Scene with vCoords: ", vCoords, " vRot: ", vRot)
						
						INITIALIZE_LINKED_SYNC_PEDS_STATE(iPed)
						
						sMissionPedsLocalVars[iPed].iSyncScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCoords, vRot, EULER_XYZ, FALSE, TRUE)
						
						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedArgueBrawler, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "ARGUEMENT_LOOP_MP_M_BRAWLER_01", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)
						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedArgueVictim, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "ARGUEMENT_LOOP_MP_M_BRAWLER_02", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedArgueBrawler, TRUE)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedArgueVictim, TRUE)						
						NETWORK_START_SYNCHRONISED_SCENE(sMissionPedsLocalVars[iPed].iSyncScene)
						
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SyncSceneBroadcast, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, sMissionPedsLocalVars[iPed].iSyncScene)
						SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_PROCESS_START)
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetPedSyncSceneState, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ENUM_TO_INT(PED_SYNC_SCENE_STATE_PROCESS_START))
					ENDIF					
				BREAK
				
				CASE	PED_SYNC_SCENE_STATE_PROCESS_START
					
					// The Scene is Running
					iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sMissionPedsLocalVars[iPed].iSyncScene)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)											
						IF IS_ANY_PLAYER_INSIDE_BREAKOUT_RANGE(IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)), iPed, pedArgueBrawler)
							PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Timer Expired, Playing the Slam Anim.")
							
							sMissionPedsLocalVars[iPed].iSyncScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCoords, vRot, EULER_XYZ, FALSE, FALSE)
							NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedArgueBrawler, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "BREAKOUT_MP_M_BRAWLER_01", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)
							NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedArgueVictim, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "BREAKOUT_MP_M_BRAWLER_02", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedArgueBrawler, TRUE)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedArgueVictim, TRUE)
							NETWORK_START_SYNCHRONISED_SCENE(sMissionPedsLocalVars[iPed].iSyncScene)
							
							BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SyncSceneBroadcast, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, sMissionPedsLocalVars[iPed].iSyncScene)
							SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT)
							BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetPedSyncSceneState, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ENUM_TO_INT(PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT))
						ENDIF
					ENDIF
				BREAK
				
				CASE	PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT
					
					// The Scene is Running	
					iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sMissionPedsLocalVars[iPed].iSyncScene)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
						PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Breakout - Scene Playing. Phase: ", GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene))
						
						IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene) >= 0.925
							PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Breakout - Scene Finished")
							SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_CLEANUP)
							BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetPedSyncSceneState, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ENUM_TO_INT(PED_SYNC_SCENE_STATE_CLEANUP))
						ENDIF
					ELSE
						PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - NOT IS_SYNCHRONIZED_SCENE_RUNNING")
						SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_CLEANUP)
					ENDIF
				BREAK
			
				CASE	PED_SYNC_SCENE_STATE_CLEANUP
					CLEANUP_SYNC_SCENE_OWNER_DATA(iPed)
					
					PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Killing Knocked Out ped.")
					IF NOT IS_PED_INJURED(pedArgueVictim)
						SET_PED_CONFIG_FLAG(pedArgueVictim, PCF_ForceRagdollUponDeath, TRUE)
						SET_ENTITY_HEALTH(pedArgueVictim, 0)
					ENDIF
										
					CLEANUP_SYNC_SCENE_LOCAL_DATA(iPed)					
					SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_PROCESS_END)
				BREAK				
					
			ENDSWITCH
		BREAK
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_ARGUE_VICTIM
			PROCESS_JUST_OWNER_CLEANUP(iPed)
		BREAK
		// ===============================================
		
		// === Casino Bar Throw ==========================
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_BAR_THROW
			strAnimDict = "ANIM@AMB@CASINO@BRAWL@FIGHTS@BAR_THROW@"
			IF NOT IS_NETWORKED_SYNC_SCENE_PED_READY(iPed, strAnimDict, ciPED_IDLE_ANIM__CASINO_FIGHT_BAR_THROW, ciPED_IDLE_ANIM__CASINO_FIGHT_BAR_THROW_PEDESTRIAN, TRUE #IF IS_DEBUG_BUILD , GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) #ENDIF )
				EXIT
			ENDIF
			
			// Assign our Indexes.
			PED_INDEX pedBarThrowBrawler, pedBarThrowVictim
			INT iBarThrowVictimPedSlot
			iBarThrowVictimPedSlot = (ciPED_IDLE_ANIM__CASINO_FIGHT_BAR_THROW_PEDESTRIAN - ciPED_IDLE_ANIM__CASINO_FIGHT_BAR_THROW)
		
			pedBarThrowBrawler = GET_PED_INDEX_FROM_NET_ID(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
			pedBarThrowVictim = GET_PED_INDEX_FROM_NET_ID(MC_serverBD_1.sFMMC_SBD.niPed[sMissionPedsLocalVars[iPed].iSyncedPeds[iBarThrowVictimPedSlot]])
			
			IF sMissionPedsLocalVars[iPed].ePedSyncSceneState >= PED_SYNC_SCENE_STATE_INIT
			AND sMissionPedsLocalVars[iPed].ePedSyncSceneState < PED_SYNC_SCENE_STATE_CLEANUP
				IF NOT ARE_LINKED_SYNC_PEDS_ALIVE(iPed)
					SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_CLEANUP)
					EXIT
				ENDIF
			ENDIF
			
			IF CONTENT_IS_USING_CASINO()
				vCoords = <<1111.9396, 208.0599, -49.9407>>
				vRot = <<0, 0, 0>>
			ENDIF
			
			SWITCH sMissionPedsLocalVars[iPed].ePedSyncSceneState
				CASE	PED_SYNC_SCENE_STATE_INIT
					
					// Create the Sync Scene
					IF sMissionPedsLocalVars[iPed].iSyncScene = -1
						PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Creating the Sync Scene with vCoords: ", vCoords, " vRot: ", vRot)
						INITIALIZE_LINKED_SYNC_PEDS_STATE(iPed)
						
						sMissionPedsLocalVars[iPed].iSyncScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCoords, vRot, EULER_XYZ, FALSE, TRUE)
						
						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedBarThrowBrawler, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "MISCREANT_BAR_FIGHT_LOOP", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)
						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedBarThrowVictim, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "VICTIM_BAR_FIGHT_LOOP", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedBarThrowBrawler, TRUE)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedBarThrowVictim, TRUE)						
						NETWORK_START_SYNCHRONISED_SCENE(sMissionPedsLocalVars[iPed].iSyncScene)
						
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SyncSceneBroadcast, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, sMissionPedsLocalVars[iPed].iSyncScene)						
						SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_PROCESS_START)
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetPedSyncSceneState, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ENUM_TO_INT(PED_SYNC_SCENE_STATE_PROCESS_START))
					ENDIF					
				BREAK
				
				CASE	PED_SYNC_SCENE_STATE_PROCESS_START
					
					// The Scene is Running
					iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sMissionPedsLocalVars[iPed].iSyncScene)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)											
						IF IS_ANY_PLAYER_INSIDE_BREAKOUT_RANGE(IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)), iPed, pedBarThrowBrawler)
							PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Timer Expired, Playing the Slam Anim.")
													
							sMissionPedsLocalVars[iPed].iSyncScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCoords, vRot, EULER_XYZ, FALSE, FALSE)
							NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedBarThrowBrawler, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "MISCREANT_BAR_FIGHT_THROW", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)
							NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedBarThrowVictim, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "VICTIM_BAR_FIGHT_THROW", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)							
							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedBarThrowBrawler, TRUE)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedBarThrowVictim, TRUE)						
							NETWORK_START_SYNCHRONISED_SCENE(sMissionPedsLocalVars[iPed].iSyncScene)
							
							BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SyncSceneBroadcast, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, sMissionPedsLocalVars[iPed].iSyncScene)							
							SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT)
							BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetPedSyncSceneState, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ENUM_TO_INT(PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT))
						ENDIF
					ENDIF
				BREAK
				
				CASE	PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT
					
					// The Scene is Running	
					iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sMissionPedsLocalVars[iPed].iSyncScene)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
						PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Breakout - Scene Playing. Phase: ", GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene))
						
						IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene) >= 0.85
							PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Breakout - Scene Finished")
							SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_CLEANUP)
							BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetPedSyncSceneState, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ENUM_TO_INT(PED_SYNC_SCENE_STATE_CLEANUP))
						ENDIF
					ELSE
						PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - NOT IS_SYNCHRONIZED_SCENE_RUNNING")
						SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_CLEANUP)
					ENDIF
				BREAK
			
				CASE	PED_SYNC_SCENE_STATE_CLEANUP				
					CLEANUP_SYNC_SCENE_OWNER_DATA(iPed)
					
					PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Killing Knocked Out ped.")
					IF NOT IS_PED_INJURED(pedBarThrowVictim)
						SET_PED_CONFIG_FLAG(pedBarThrowVictim, PCF_ForceRagdollUponDeath, TRUE)
						SET_ENTITY_HEALTH(pedBarThrowVictim, 0)
					ENDIF
										
					CLEANUP_SYNC_SCENE_LOCAL_DATA(iPed)					
					SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_PROCESS_END)
				BREAK				
									
			ENDSWITCH
		BREAK
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_BAR_THROW_PEDESTRIAN
			PROCESS_JUST_OWNER_CLEANUP(iPed)
		BREAK
		// ===============================================
		
		// === Casino Attack Slot Machine ===
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_SLOT_SLAM
			strAnimDict = "ANIM@AMB@CASINO@BRAWL@FIGHTS@SLOT_SLAM@"
			IF NOT IS_NETWORKED_SYNC_SCENE_PED_READY(iPed, strAnimDict, ciPED_IDLE_ANIM__CASINO_FIGHT_SLOT_SLAM, ciPED_IDLE_ANIM__CASINO_FIGHT_SLOT_SLAM_PEDESTRIAN, TRUE #IF IS_DEBUG_BUILD , GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) #ENDIF )
				EXIT
			ENDIF
			
			// Assign our Indexes.
			PED_INDEX pedSlotSlamBrawler, pedSlotSlamVictim
			INT iSlotSlamVictimPedSlot
			iSlotSlamVictimPedSlot = (ciPED_IDLE_ANIM__CASINO_FIGHT_SLOT_SLAM_PEDESTRIAN - ciPED_IDLE_ANIM__CASINO_FIGHT_SLOT_SLAM)
			
			
			pedSlotSlamBrawler = GET_PED_INDEX_FROM_NET_ID(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
			pedSlotSlamVictim = GET_PED_INDEX_FROM_NET_ID(MC_serverBD_1.sFMMC_SBD.niPed[sMissionPedsLocalVars[iPed].iSyncedPeds[iSlotSlamVictimPedSlot]])
			
			IF sMissionPedsLocalVars[iPed].ePedSyncSceneState >= PED_SYNC_SCENE_STATE_INIT
			AND sMissionPedsLocalVars[iPed].ePedSyncSceneState < PED_SYNC_SCENE_STATE_CLEANUP
				IF NOT ARE_LINKED_SYNC_PEDS_ALIVE(iPed)
					SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_CLEANUP)
					EXIT
				ENDIF
			ENDIF
			
			//IF CONTENT_IS_USING_CASINO()
			//	vCoords = <<1114.8, 234.5, -50.8>>
			//	vRot = <<0, 0, 108>>
				//vCoords = <<1113.6398, 233.6755, -50.8409>> other machine
				//vRot = <<0, 0, -36>>
			//ELSE
				vCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords, GET_ENTITY_HEADING(pedSlotSlamBrawler), <<0.0, 1.8, 0.0>>)
			//ENDIF
			
			SWITCH sMissionPedsLocalVars[iPed].ePedSyncSceneState
				CASE	PED_SYNC_SCENE_STATE_INIT
					
					// Create the Sync Scene
					IF sMissionPedsLocalVars[iPed].iSyncScene = -1
						PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Creating the Sync Scene with vCoords: ", vCoords, " vRot: ", vRot)
						INITIALIZE_LINKED_SYNC_PEDS_STATE(iPed)
						
						sMissionPedsLocalVars[iPed].iSyncScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCoords, vRot, EULER_XYZ, FALSE, TRUE)
						
						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedSlotSlamBrawler, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "ARGUMENT_LOOP_MP_M_BRAWLER", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)
						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedSlotSlamVictim, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "ARGUMENT_LOOP_MP_M_PEDESTRIAN", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedSlotSlamBrawler, TRUE)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedSlotSlamVictim, TRUE)						
						NETWORK_START_SYNCHRONISED_SCENE(sMissionPedsLocalVars[iPed].iSyncScene)
						
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SyncSceneBroadcast, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, sMissionPedsLocalVars[iPed].iSyncScene)						
						SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_PROCESS_START)
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetPedSyncSceneState, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ENUM_TO_INT(PED_SYNC_SCENE_STATE_PROCESS_START))
					ENDIF					
				BREAK
				
				CASE	PED_SYNC_SCENE_STATE_PROCESS_START
					
					// The Scene is Running
					iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sMissionPedsLocalVars[iPed].iSyncScene)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)											
						IF IS_ANY_PLAYER_INSIDE_BREAKOUT_RANGE(IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)), iPed, pedSlotSlamBrawler)
							PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Timer Expired, Playing the Slam Anim.")
							
							sMissionPedsLocalVars[iPed].iSyncScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCoords, vRot, EULER_XYZ, FALSE, FALSE)
							NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedSlotSlamBrawler, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "BREAKOUT_MP_M_BRAWLER", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)
							NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedSlotSlamVictim, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "BREAKOUT_MP_M_PEDESTRIAN", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)							
							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedSlotSlamBrawler, TRUE)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedSlotSlamVictim, TRUE)
							NETWORK_START_SYNCHRONISED_SCENE(sMissionPedsLocalVars[iPed].iSyncScene)
							
							BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SyncSceneBroadcast, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, sMissionPedsLocalVars[iPed].iSyncScene)							
							SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT)
							BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetPedSyncSceneState, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ENUM_TO_INT(PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT))
						ENDIF
					ENDIF
				BREAK
				
				CASE	PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT
					
					// The Scene is Running	
					iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sMissionPedsLocalVars[iPed].iSyncScene)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
						PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Breakout - Scene Playing. Phase: ", GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene))
											
						IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene) >= 0.925
							PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Breakout - Scene Finished")
							SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_CLEANUP)
							BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetPedSyncSceneState, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ENUM_TO_INT(PED_SYNC_SCENE_STATE_CLEANUP))
						ENDIF
					ELSE
						PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - NOT IS_SYNCHRONIZED_SCENE_RUNNING")
						SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_CLEANUP)
					ENDIF
				BREAK
			
				CASE	PED_SYNC_SCENE_STATE_CLEANUP				
					CLEANUP_SYNC_SCENE_OWNER_DATA(iPed)
					
					PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Killing Knocked Out ped.")
					IF NOT IS_PED_INJURED(pedSlotSlamVictim)
						SET_PED_CONFIG_FLAG(pedSlotSlamVictim, PCF_ForceRagdollUponDeath, TRUE)
						SET_ENTITY_HEALTH(pedSlotSlamVictim, 0)
					ENDIF
										
					CLEANUP_SYNC_SCENE_LOCAL_DATA(iPed)
					SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_PROCESS_END)
				BREAK				
					
			ENDSWITCH
		BREAK
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_SLOT_SLAM_PEDESTRIAN
			PROCESS_JUST_OWNER_CLEANUP(iPed)
		BREAK		
		// ===============================================		
		
		// === Casino Chip Steal Scene ===================
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CHIP_STEAL_ATTACKER_1
			PROCESS_JUST_OWNER_CLEANUP(iPed)
		BREAK
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CHIP_STEAL_ATTACKER_2
			PROCESS_JUST_OWNER_CLEANUP(iPed)
		BREAK
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CHIP_STEAL_CROUPIER
			PROCESS_JUST_OWNER_CLEANUP(iPed)
		BREAK
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CHIP_STEAL_PEDESTRIAN_1
			PROCESS_JUST_OWNER_CLEANUP(iPed)
		BREAK
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CHIP_STEAL_PEDESTRIAN_2
			PROCESS_JUST_OWNER_CLEANUP(iPed)
		BREAK
		// ===============================================
		
		// === Casino Cashier Fight ===
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CASHIER
			
			strAnimDict = "ANIM@AMB@CASINO@BRAWL@FIGHTS@CASHIER@"
			IF NOT IS_NETWORKED_SYNC_SCENE_PED_READY(iPed, strAnimDict, ciPED_IDLE_ANIM__CASINO_FIGHT_CASHIER, ciPED_IDLE_ANIM__CASINO_FIGHT_CASHIER, TRUE #IF IS_DEBUG_BUILD , GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) #ENDIF )
				EXIT
			ENDIF
			
			// Assign our Indexes.				
			PED_INDEX pedCashier
			pedCashier = GET_PED_INDEX_FROM_NET_ID(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
			
			IF sMissionPedsLocalVars[iPed].ePedSyncSceneState >= PED_SYNC_SCENE_STATE_INIT
			AND sMissionPedsLocalVars[iPed].ePedSyncSceneState < PED_SYNC_SCENE_STATE_CLEANUP
				IF NOT ARE_LINKED_SYNC_PEDS_ALIVE(iPed)
					SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_CLEANUP)
					EXIT
				ENDIF
			ENDIF
			
			IF CONTENT_IS_USING_CASINO()
				vCoords = <<1120.92, 219.99, -50.43>>
				vRot = <<0, 0, 0.0>>
			ENDIF
			
			SWITCH sMissionPedsLocalVars[iPed].ePedSyncSceneState
				CASE	PED_SYNC_SCENE_STATE_INIT
					
					// Create the Sync Scene
					IF sMissionPedsLocalVars[iPed].iSyncScene = -1
						PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Creating the Sync Scene with vCoords: ", vCoords, " vRot: ", vRot)
						
						INITIALIZE_LINKED_SYNC_PEDS_STATE(iPed)
						
						sMissionPedsLocalVars[iPed].iSyncScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCoords, vRot, EULER_XYZ, FALSE, TRUE)
						
						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedCashier, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "ROB_LOOP_MP_M_BRAWLER", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedCashier, TRUE)						
						NETWORK_START_SYNCHRONISED_SCENE(sMissionPedsLocalVars[iPed].iSyncScene)
						
						IF iCasinoBrawlCashierOutcome = -1
							iCasinoBrawlCashierOutcome = GET_RANDOM_INT_IN_RANGE(0, 100)
						ENDIF
						
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SyncSceneBroadcast, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, sMissionPedsLocalVars[iPed].iSyncScene)
						SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_PROCESS_START)
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetPedSyncSceneState, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ENUM_TO_INT(PED_SYNC_SCENE_STATE_PROCESS_START))
					ENDIF					
				BREAK
				
				CASE	PED_SYNC_SCENE_STATE_PROCESS_START
					// The Scene is Running
					iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sMissionPedsLocalVars[iPed].iSyncScene)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)											
						IF IS_ANY_PLAYER_INSIDE_BREAKOUT_RANGE(IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)), iPed, pedCashier)
							PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Timer Expired, Playing the Slam Anim.")
							
							sMissionPedsLocalVars[iPed].iSyncScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCoords, vRot, EULER_XYZ, FALSE, FALSE)
							
							IF iCasinoBrawlCashierOutcome >= 50
								NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedCashier, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "BREAKOUT_RIGHT_MP_M_BRAWLER", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)
							ELSE
								NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedCashier, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "BREAKOUT_LEFT_MP_M_BRAWLER", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)
							ENDIF
							
							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedCashier, TRUE)
							NETWORK_START_SYNCHRONISED_SCENE(sMissionPedsLocalVars[iPed].iSyncScene)
							
							BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SyncSceneBroadcast, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, sMissionPedsLocalVars[iPed].iSyncScene)							
							SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT)
							BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetPedSyncSceneState, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ENUM_TO_INT(PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT))
						ENDIF
					ENDIF
				BREAK
				
				CASE	PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT
					
					// The Scene is Running	
					iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sMissionPedsLocalVars[iPed].iSyncScene)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
						PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Breakout - Scene Playing. Phase: ", GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene))
						
						IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene) >= 0.975
							PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Breakout - Scene Finished")
							SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_CLEANUP)
							BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetPedSyncSceneState, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ENUM_TO_INT(PED_SYNC_SCENE_STATE_CLEANUP))
						ENDIF
					ELSE
						PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - NOT IS_SYNCHRONIZED_SCENE_RUNNING")
						SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_CLEANUP)
					ENDIF
				BREAK
			
				CASE	PED_SYNC_SCENE_STATE_CLEANUP
					iCasinoBrawlCashierOutcome = -1
					CLEANUP_SYNC_SCENE_OWNER_DATA(iPed)
					CLEANUP_SYNC_SCENE_LOCAL_DATA(iPed)
					SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_PROCESS_END)
				BREAK
					
			ENDSWITCH
		BREAK
		
		// === Casino Attack Slot Machine =================
		CASE ciPED_IDLE_ANIM__CASINO_SLOTMACHINE_ATTACK			
			strAnimDict = "ANIM@AMB@CASINO@BRAWL@ATTACKS@SLOT_MACHINE@"
			IF NOT IS_NETWORKED_SYNC_SCENE_PED_READY(iPed, strAnimDict, ciPED_IDLE_ANIM__CASINO_SLOTMACHINE_ATTACK, ciPED_IDLE_ANIM__CASINO_SLOTMACHINE_ATTACK, TRUE #IF IS_DEBUG_BUILD , GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) #ENDIF )
				EXIT
			ENDIF
			
			// Assign our Indexes.				
			PED_INDEX pedMiscreant
			pedMiscreant = GET_PED_INDEX_FROM_NET_ID(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
			
			IF sMissionPedsLocalVars[iPed].ePedSyncSceneState >= PED_SYNC_SCENE_STATE_INIT
			AND sMissionPedsLocalVars[iPed].ePedSyncSceneState < PED_SYNC_SCENE_STATE_CLEANUP
				IF NOT ARE_LINKED_SYNC_PEDS_ALIVE(iPed)
					SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_CLEANUP)
					EXIT
				ENDIF
			ENDIF
			
			IF CONTENT_IS_USING_CASINO()
				vCoords = <<1120.92, 219.99, -50.43>>
				vRot = <<0, 0, 0.0>>
			ENDIF
			
			SWITCH sMissionPedsLocalVars[iPed].ePedSyncSceneState
				CASE	PED_SYNC_SCENE_STATE_INIT
					
					// Create the Sync Scene
					IF sMissionPedsLocalVars[iPed].iSyncScene = -1
						PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Creating the Sync Scene with vCoords: ", vCoords, " vRot: ", vRot)
						
						INITIALIZE_LINKED_SYNC_PEDS_STATE(iPed)
						
						sMissionPedsLocalVars[iPed].iSyncScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCoords, vRot, EULER_XYZ, FALSE, TRUE)
						
						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedMiscreant, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "ROCKING_SLOT_MACHINE_LOOP_MP_M_BRAWLER", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedMiscreant, TRUE)						
						NETWORK_START_SYNCHRONISED_SCENE(sMissionPedsLocalVars[iPed].iSyncScene)
						
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SyncSceneBroadcast, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, sMissionPedsLocalVars[iPed].iSyncScene)
						SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_PROCESS_START)
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetPedSyncSceneState, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ENUM_TO_INT(PED_SYNC_SCENE_STATE_PROCESS_START))
					ENDIF					
				BREAK
				
				CASE	PED_SYNC_SCENE_STATE_PROCESS_START
					// The Scene is Running
					iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sMissionPedsLocalVars[iPed].iSyncScene)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)											
						IF IS_ANY_PLAYER_INSIDE_BREAKOUT_RANGE(IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)), iPed, pedMiscreant)
							PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Timer Expired, Playing the Slam Anim.")
							
							sMissionPedsLocalVars[iPed].iSyncScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCoords, vRot, EULER_XYZ, FALSE, FALSE)
							NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedMiscreant, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "BREAKOUT_MP_M_BRAWLER", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)
							
							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedMiscreant, TRUE)
							NETWORK_START_SYNCHRONISED_SCENE(sMissionPedsLocalVars[iPed].iSyncScene)
							
							BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SyncSceneBroadcast, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, sMissionPedsLocalVars[iPed].iSyncScene)							
							SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT)
							BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetPedSyncSceneState, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ENUM_TO_INT(PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT))
						ENDIF
					ENDIF
				BREAK
				
				CASE	PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT
					
					// The Scene is Running	
					iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sMissionPedsLocalVars[iPed].iSyncScene)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
						PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Breakout - Scene Playing. Phase: ", GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene))
						
						IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene) >= 0.975
							PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Breakout - Scene Finished")
							SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_CLEANUP)
							BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetPedSyncSceneState, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ENUM_TO_INT(PED_SYNC_SCENE_STATE_CLEANUP))
						ENDIF
					ELSE
						PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - NOT IS_SYNCHRONIZED_SCENE_RUNNING")
						SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_CLEANUP)
					ENDIF
				BREAK
			
				CASE	PED_SYNC_SCENE_STATE_CLEANUP
					iCasinoBrawlCashierOutcome = -1
					CLEANUP_SYNC_SCENE_OWNER_DATA(iPed)
					CLEANUP_SYNC_SCENE_LOCAL_DATA(iPed)
					SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_PROCESS_END)
				BREAK
					
			ENDSWITCH
		BREAK
		// ===============================================
		
		// === Casino Shop Throw =========================
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_SHOP_THROW
			strAnimDict = "ANIM@AMB@CASINO@BRAWL@FIGHTS@SHOP_THROW@"
			IF NOT IS_NETWORKED_SYNC_SCENE_PED_READY(iPed, strAnimDict, ciPED_IDLE_ANIM__CASINO_FIGHT_SHOP_THROW, ciPED_IDLE_ANIM__CASINO_FIGHT_SHOP_THROW_VICTIM, TRUE #IF IS_DEBUG_BUILD , GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) #ENDIF )
				EXIT
			ENDIF
			
			// Assign our Indexes.
			PED_INDEX pedShopThrowBrawler, pedShopThrowVictim
			INT iShopThrowVictimPedSlot
			iShopThrowVictimPedSlot = (ciPED_IDLE_ANIM__CASINO_FIGHT_SHOP_THROW_VICTIM - ciPED_IDLE_ANIM__CASINO_FIGHT_SHOP_THROW)
			
			
			pedShopThrowBrawler = GET_PED_INDEX_FROM_NET_ID(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
			pedShopThrowVictim = GET_PED_INDEX_FROM_NET_ID(MC_serverBD_1.sFMMC_SBD.niPed[sMissionPedsLocalVars[iPed].iSyncedPeds[iShopThrowVictimPedSlot]])
			
			IF sMissionPedsLocalVars[iPed].ePedSyncSceneState >= PED_SYNC_SCENE_STATE_INIT
			AND sMissionPedsLocalVars[iPed].ePedSyncSceneState < PED_SYNC_SCENE_STATE_CLEANUP
				IF NOT ARE_LINKED_SYNC_PEDS_ALIVE(iPed)
					SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_CLEANUP)
					EXIT
				ENDIF
			ENDIF
			
			//IF CONTENT_IS_USING_CASINO()
			//	vCoords = <<1114.8, 234.5, -50.8>>
			//	vRot = <<0, 0, 108>>
				//vCoords = <<1113.6398, 233.6755, -50.8409>> other machine
				//vRot = <<0, 0, -36>>
			//ELSE
			//	vCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords, GET_ENTITY_HEADING(pedShopThrowBrawler), <<0.0, 1.8, 0.0>>)
			//ENDIF
			
			SWITCH sMissionPedsLocalVars[iPed].ePedSyncSceneState
				CASE	PED_SYNC_SCENE_STATE_INIT
					
					// Create the Sync Scene
					IF sMissionPedsLocalVars[iPed].iSyncScene = -1
						PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Creating the Sync Scene with vCoords: ", vCoords, " vRot: ", vRot)
						INITIALIZE_LINKED_SYNC_PEDS_STATE(iPed)
						
						sMissionPedsLocalVars[iPed].iSyncScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCoords, vRot, EULER_XYZ, FALSE, TRUE)
						
						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedShopThrowBrawler, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "FIGHT_LOOP_MP_M_BRAWLER", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)
						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedShopThrowVictim, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "FIGHT_LOOP_MP_M_PEDESTRIAN", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedShopThrowBrawler, TRUE)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedShopThrowVictim, TRUE)						
						NETWORK_START_SYNCHRONISED_SCENE(sMissionPedsLocalVars[iPed].iSyncScene)
						
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SyncSceneBroadcast, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, sMissionPedsLocalVars[iPed].iSyncScene)						
						SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_PROCESS_START)
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetPedSyncSceneState, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ENUM_TO_INT(PED_SYNC_SCENE_STATE_PROCESS_START))
					ENDIF					
				BREAK
				
				CASE	PED_SYNC_SCENE_STATE_PROCESS_START
					
					// The Scene is Running
					iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sMissionPedsLocalVars[iPed].iSyncScene)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)											
						IF IS_ANY_PLAYER_INSIDE_BREAKOUT_RANGE(IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)), iPed, pedShopThrowBrawler)
							PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Timer Expired, Playing the Slam Anim.")
							
							sMissionPedsLocalVars[iPed].iSyncScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCoords, vRot, EULER_XYZ, FALSE, FALSE)
							NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedShopThrowBrawler, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "BREAKOUT_MP_M_BRAWLER", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)
							NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedShopThrowVictim, sMissionPedsLocalVars[iPed].iSyncScene, strAnimDict, "BREAKOUT_MP_M_PEDESTRIAN", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, ikFlags)							
							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedShopThrowBrawler, TRUE)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedShopThrowVictim, TRUE)
							NETWORK_START_SYNCHRONISED_SCENE(sMissionPedsLocalVars[iPed].iSyncScene)
							
							BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SyncSceneBroadcast, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, sMissionPedsLocalVars[iPed].iSyncScene)							
							SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT)
							BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetPedSyncSceneState, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ENUM_TO_INT(PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT))
						ENDIF
					ENDIF
				BREAK
				
				CASE	PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT
					
					// The Scene is Running	
					iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sMissionPedsLocalVars[iPed].iSyncScene)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
						PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Breakout - Scene Playing. Phase: ", GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene))
											
						IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene) >= 0.925
							PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Breakout - Scene Finished")
							SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_CLEANUP)
							BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetPedSyncSceneState, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ENUM_TO_INT(PED_SYNC_SCENE_STATE_CLEANUP))
						ENDIF
					ELSE
						PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - NOT IS_SYNCHRONIZED_SCENE_RUNNING")
						SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_CLEANUP)
					ENDIF
				BREAK
			
				CASE	PED_SYNC_SCENE_STATE_CLEANUP				
					CLEANUP_SYNC_SCENE_OWNER_DATA(iPed)
					
					PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Killing Knocked Out ped.")
					IF NOT IS_PED_INJURED(pedShopThrowVictim)
						SET_PED_CONFIG_FLAG(pedShopThrowVictim, PCF_ForceRagdollUponDeath, TRUE)
						SET_ENTITY_HEALTH(pedShopThrowVictim, 0)
					ENDIF
										
					CLEANUP_SYNC_SCENE_LOCAL_DATA(iPed)
					SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_PROCESS_END)
				BREAK				
					
			ENDSWITCH
		BREAK
		CASE ciPED_IDLE_ANIM__CASINO_FIGHT_SHOP_THROW_VICTIM
			PROCESS_JUST_OWNER_CLEANUP(iPed)
		BREAK
		// ================================================
		
		// === Casino Roulette Reaction 1 =================
		CASE ciPED_IDLE_ANIM__CASINO_ROULETTE_REACT_1
			PROCESS_JUST_OWNER_CLEANUP(iPed)
		BREAK
		// ===============================================
		
		// === Casino Roulette Reaction 2 ================
		CASE ciPED_IDLE_ANIM__CASINO_ROULETTE_REACT_2
			PROCESS_JUST_OWNER_CLEANUP(iPed)
		BREAK
		// ===============================================
		
		// === Casino Get in Car =========================
		CASE ciPED_IDLE_ANIM__CASINO_GET_IN_CAR_TAO
			PROCESS_JUST_OWNER_CLEANUP(iPed)
		BREAK
		CASE ciPED_IDLE_ANIM__CASINO_GET_IN_CAR_TRANS
			PROCESS_JUST_OWNER_CLEANUP(iPed)
		BREAK		
		CASE ciPED_IDLE_ANIM__CASINO_GET_IN_CAR_AGATHA
			PROCESS_JUST_OWNER_CLEANUP(iPed)
		BREAK
		
		DEFAULT
			IF sMissionPedsLocalVars[iPed].ePedSyncSceneState != PED_SYNC_SCENE_STATE_PROCESS_END
			AND sMissionPedsLocalVars[iPed].ePedSyncSceneState != PED_SYNC_SCENE_STATE_INIT
				PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PED IS IN A DIRTY STATE. DO A FORCE CLEANUP")
				CLEANUP_SYNC_SCENE_OWNER_DATA(iPed)
				CLEANUP_SYNC_SCENE_LOCAL_DATA(iPed)
				SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_PROCESS_END)
			ENDIF
		BREAK
		// ===============================================s
	ENDSWITCH
ENDPROC

PROC PROCESS_EVERY_FRAME_LOCAL_PLAYER_SYNC_SCENE_PEDS(INT iPed)
	
	IF iPed = -1
		EXIT
	ENDIF
		
	STRING strAnimDict
	VECTOR vCoords //, vRot
	BOOL bIsPerformingSpecialAnim = IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
	
	INT iLocalScene
	INT iAnimation
	IF bIsPerformingSpecialAnim
		iAnimation = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnim
	ELSE
		iAnimation = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnim
	ENDIF
	
	IF sMissionPedsLocalVars[iPed].iSyncScene != -1
		SWITCH iAnimation
			// === Casino Vincent Intro ===
			CASE ciPED_IDLE_ANIM__CASINO_VINCENT_INTRO
				SWITCH sMissionPedsLocalVars[iPed].ePedSyncSceneState											
					/*CASE	PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT
						PED_INDEX pedBreakoutVincent
						pedBreakoutVincent = GET_PED_INDEX_FROM_NET_ID(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
						
						// The Scene is Running	
						iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sMissionPedsLocalVars[iPed].iSyncScene)
						IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
							
							IF IS_CONVERSATION_STATUS_FREE()
							OR NOT IS_SCRIPTED_CONVERSATION_ONGOING()
								// Intro Dialogue
								IF NOT IS_BIT_SET(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_iSyncSceneLoadedIntroDialogue)
									
									ADD_PED_FOR_DIALOGUE(sMissionPedSpecialConversation, 3, pedBreakoutVincent, "CAS_VINCENT")
									
									PRINTLN("[LM][RCC MISSION][LOCAL][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Setting that Dialogue has loaded.  (Intro)")
									SET_BIT(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_iSyncSceneLoadedIntroDialogue)
									
								ELIF NOT IS_BIT_SET(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_iSyncScenePlayedIntroDialogue)
									PRINTLN("[LM][RCC MISSION][LOCAL][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Doing Conversation....  (Intro)")
									IF CREATE_CONVERSATION(sMissionPedSpecialConversation, "CAS2AUD", "CAS2_FOLLOW", CONV_PRIORITY_VERY_HIGH)
										PRINTLN("[LM][RCC MISSION][LOCAL][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Setting that Dialogue has played.  (Intro)")
										SET_BIT(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_iSyncScenePlayedIntroDialogue)
									ENDIF									
								ENDIF
							
								// Breakout Dialogue
								IF IS_BIT_SET(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_iSyncScenePlayedIntroDialogue)
									IF NOT IS_BIT_SET(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_iSyncSceneLoadedBreakoutDialogue)
										
										ADD_PED_FOR_DIALOGUE(sMissionPedSpecialConversation, 3, pedBreakoutVincent, "CAS_VINCENT")
										
										PRINTLN("[LM][RCC MISSION][LOCAL][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Setting that Dialogue has loaded.  (Breakout)")
										SET_BIT(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_iSyncSceneLoadedBreakoutDialogue)
										
									ELIF NOT IS_BIT_SET(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_iSyncScenePlayedBreakoutDialogue)
										PRINTLN("[LM][RCC MISSION][LOCAL][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Doing Conversation....  (Breakout)")
										IF CREATE_CONVERSATION(sMissionPedSpecialConversation, "CAS2AUD", "CAS2_BREAK", CONV_PRIORITY_VERY_HIGH)
											PRINTLN("[LM][RCC MISSION][LOCAL][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Setting that Dialogue has played.  (Breakout)")
											SET_BIT(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_iSyncScenePlayedBreakoutDialogue)
										ENDIF
									ENDIF
								ENDIF
								
								// Ending Dialogue
								IF IS_BIT_SET(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_iSyncScenePlayedBreakoutDialogue)
									IF NOT IS_BIT_SET(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_iSyncSceneLoadedEndDialogue)
										
										ADD_PED_FOR_DIALOGUE(sMissionPedSpecialConversation, 3, pedBreakoutVincent, "CAS_VINCENT")
										
										PRINTLN("[LM][RCC MISSION][LOCAL][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Setting that Dialogue has loaded.  (End)")
										SET_BIT(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_iSyncSceneLoadedEndDialogue)
										
									ELIF NOT IS_BIT_SET(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_iSyncScenePlayedEndDialogue)
										PRINTLN("[LM][RCC MISSION][LOCAL][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Doing Conversation....  (End)")
										IF CREATE_CONVERSATION(sMissionPedSpecialConversation, "CAS2AUD", "CAS2_DEAL", CONV_PRIORITY_VERY_HIGH)
											PRINTLN("[LM][RCC MISSION][LOCAL][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Setting that Dialogue has played.  (End)")
											SET_BIT(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_iSyncScenePlayedEndDialogue)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							PRINTLN("[LM][RCC MISSION][LOCAL][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - NOT IS_SYNCHRONIZED_SCENE_RUNNING")
						ENDIF
					BREAK*/
				
					CASE	PED_SYNC_SCENE_STATE_CLEANUP
						IF NOT IS_PARTICIPANT_SYNC_SCENE_PED_OWNER(iLocalPart, iPed)
							CLEANUP_SYNC_SCENE_LOCAL_DATA(iPed)
							SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_PROCESS_END)
						ENDIF
					BREAK				
						
				ENDSWITCH
			BREAK
			// ===============================================	
			
			// === Casino Fight breakout ====================
			CASE ciPED_IDLE_ANIM__CASINO_FIGHT_BREAKOUT_1
				strAnimDict = "ANIM@AMB@CASINO@BRAWL@FIGHTS@BREAKOUT@"
				IF NOT IS_NETWORKED_SYNC_SCENE_PED_READY(iPed, strAnimDict, ciPED_IDLE_ANIM__CASINO_FIGHT_BREAKOUT_1, ciPED_IDLE_ANIM__CASINO_FIGHT_BREAKOUT_1_PEDESTRIAN, FALSE #IF IS_DEBUG_BUILD , GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) #ENDIF )
					EXIT
				ENDIF
				
				SWITCH sMissionPedsLocalVars[iPed].ePedSyncSceneState
					CASE	PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT
						PED_INDEX pedBreakoutBrawler, pedBreakoutCroupier
						INT iPedBreakoutCroupierSlot
						iPedBreakoutCroupierSlot = (ciPED_IDLE_ANIM__CASINO_FIGHT_BREAKOUT_1_CROUPIER - ciPED_IDLE_ANIM__CASINO_FIGHT_BREAKOUT_1)						
						pedBreakoutBrawler = GET_PED_INDEX_FROM_NET_ID(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
						pedBreakoutCroupier = GET_PED_INDEX_FROM_NET_ID(MC_serverBD_1.sFMMC_SBD.niPed[sMissionPedsLocalVars[iPed].iSyncedPeds[iPedBreakoutCroupierSlot]])
						
						// The Scene is Running	
						iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sMissionPedsLocalVars[iPed].iSyncScene)
						IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
							PRINTLN("[LM][RCC MISSION][LOCAL][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Breakout - Scene Playing Phase: ", GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene))
							
							/*IF NOT IS_BIT_SET(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_iSyncSceneLoadedBreakoutDialogue)																
								ADD_PED_FOR_DIALOGUE(sMissionPedSpecialConversation, 2, pedBreakoutBrawler, "CAS_BRAWLER")
								ADD_PED_FOR_DIALOGUE(sMissionPedSpecialConversation, 4, pedBreakoutCroupier, "CAS_CROUPIER")								
								PRINTLN("[LM][RCC MISSION][LOCAL][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Setting that Dialogue has loaded.")
								SET_BIT(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_iSyncSceneLoadedBreakoutDialogue)								
							ELIF NOT IS_BIT_SET(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_iSyncScenePlayedBreakoutDialogue)							
								IF IS_CONVERSATION_STATUS_FREE()
								OR NOT IS_SCRIPTED_CONVERSATION_ONGOING()
									PRINTLN("[LM][RCC MISSION][LOCAL][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Doing Conversation....")
									IF CREATE_CONVERSATION(sMissionPedSpecialConversation, "CAS2AUD", "CAS2_IG2_BRK", CONV_PRIORITY_VERY_HIGH)
										PRINTLN("[LM][RCC MISSION][LOCAL][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Setting that Dialogue has played.")
										SET_BIT(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_iSyncScenePlayedBreakoutDialogue)
									ENDIF
								ELSE
									PRINTLN("[LM][RCC MISSION][LOCAL][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Conversation Busy....")
								ENDIF
							ENDIF*/
							
							IF NOT IS_PED_INJURED(pedBreakoutCroupier)
								IF NOT IS_BIT_SET(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_iSyncScenePlayedIntroDialogue)
									IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene) >= 0.05
										PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(pedBreakoutCroupier, "CAS2_ARAA", "CAS_CROUPIER", "SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
										SET_BIT(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_iSyncScenePlayedIntroDialogue)
									ENDIF
								ENDIF
							ENDIF
							IF NOT IS_PED_INJURED(pedBreakoutBrawler)
								IF NOT IS_BIT_SET(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_iSyncScenePlayedBreakoutDialogue)
									IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene) >= 0.35						
										PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(pedBreakoutBrawler, "CAS2_ARAB", "CAS_BRAWLER", "SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
										SET_BIT(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_iSyncScenePlayedBreakoutDialogue)
									ENDIF
								ENDIF
							ENDIF
						ELSE
							PRINTLN("[LM][RCC MISSION][LOCAL][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - NOT IS_SYNCHRONIZED_SCENE_RUNNING")
						ENDIF
					BREAK
					CASE	PED_SYNC_SCENE_STATE_CLEANUP
						IF NOT IS_PARTICIPANT_SYNC_SCENE_PED_OWNER(iLocalPart, iPed)
							CLEANUP_SYNC_SCENE_LOCAL_DATA(iPed)
							SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_PROCESS_END)
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			CASE ciPED_IDLE_ANIM__CASINO_FIGHT_BREAKOUT_1_CROUPIER
				PROCESS_JUST_LOCAL_CLEANUP(iPed)
			BREAK
			CASE ciPED_IDLE_ANIM__CASINO_FIGHT_BREAKOUT_1_PEDESTRIAN
				PROCESS_JUST_LOCAL_CLEANUP(iPed)
			BREAK
			// ===============================================		
			
			// === Casino Security Guard / Redneck Fight =====
			CASE ciPED_IDLE_ANIM__CASINO_FIGHT_SECURITY_GUARD
				PROCESS_JUST_LOCAL_CLEANUP(iPed)
			BREAK
			CASE ciPED_IDLE_ANIM__CASINO_FIGHT_SECURITY_GUARD_REDNECK_1
				PROCESS_JUST_LOCAL_CLEANUP(iPed)
			BREAK
			CASE ciPED_IDLE_ANIM__CASINO_FIGHT_SECURITY_GUARD_REDNECK_2
				PROCESS_JUST_LOCAL_CLEANUP(iPed)
			BREAK
			// ===============================================
			
			// === Casino Fight/Conversation =================
			CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CONVERSATION
				strAnimDict = "ANIM@AMB@CASINO@BRAWL@FIGHTS@CONVERSATION@"
				IF NOT IS_NETWORKED_SYNC_SCENE_PED_READY(iPed, strAnimDict, ciPED_IDLE_ANIM__CASINO_FIGHT_CONVERSATION, ciPED_IDLE_ANIM__CASINO_FIGHT_CONVERSATION_SECOND, FALSE #IF IS_DEBUG_BUILD , GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) #ENDIF )
					EXIT
				ENDIF
				
				// Assign our Indexes.
				INT iSecondPed, iSecondPedSlot
				iSecondPedSlot = (ciPED_IDLE_ANIM__CASINO_FIGHT_CONVERSATION_SECOND - ciPED_IDLE_ANIM__CASINO_FIGHT_CONVERSATION)
				iSecondPed = sMissionPedsLocalVars[iPed].iSyncedPeds[iSecondPedSlot]
				
				PED_INDEX pedSecond, pedBottle
				pedSecond = GET_PED_INDEX_FROM_NET_ID(MC_serverBD_1.sFMMC_SBD.niPed[iSecondPed])
				pedBottle = GET_PED_INDEX_FROM_NET_ID(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
				
				vCoords = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].vPos
				
				SWITCH sMissionPedsLocalVars[iPed].ePedSyncSceneState
					CASE	PED_SYNC_SCENE_STATE_INIT
					
					BREAK
					
					CASE	PED_SYNC_SCENE_STATE_PROCESS_START					
						IF IS_PED_INJURED(pedSecond)
						OR IS_PED_INJURED(pedBottle)
							EXIT
						ENDIF
						
						// The Scene is Running
						iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sMissionPedsLocalVars[iPed].iSyncScene)
						IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
						
							// Local Clients Load Prop.
							IF NOT DOES_ENTITY_EXIST(oiCasinoBrawlBottleObject)
								PRINTLN("[LM][RCC MISSION][LOCAL][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Creating oiGenericIndexForSyncScene1 - Prop_CS_Beer_Bot_01")
								oiCasinoBrawlBottleObject = CREATE_OBJECT(Prop_CS_Beer_Bot_01, vCoords, FALSE, FALSE)
								EXIT
							ENDIF
							
							// Attach
							IF NOT IS_ENTITY_ATTACHED_TO_ANY_PED(oiCasinoBrawlBottleObject)						
								IF DOES_ENTITY_EXIST(oiCasinoBrawlBottleObject)
									INT iBone
									iBone = GET_PED_BONE_INDEX(pedBottle, BONETAG_PH_R_HAND)								
									IF iBone > -1
										PRINTLN("[LM][RCC MISSION][LOCAL][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Attaching the bottle locally.")
										ATTACH_ENTITY_TO_ENTITY(oiCasinoBrawlBottleObject, pedBottle, iBone, (<<0.0, 0.0, 0.0>>), (<<0.0, 0.0, 0.0>>), TRUE, TRUE, FALSE)
									ELSE
										PRINTLN("[LM][RCC MISSION][LOCAL][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - iBone is -1 - ERROR.")
									ENDIF
								ENDIF
								EXIT
							ENDIF
						ENDIF
					BREAK
					
					CASE	PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT
					
					BREAK
				
					CASE	PED_SYNC_SCENE_STATE_CLEANUP
						IF NOT IS_PARTICIPANT_SYNC_SCENE_PED_OWNER(iLocalPart, iPed)
							IF DOES_ENTITY_EXIST(oiCasinoBrawlBottleObject)
								PRINTLN("[LM][RCC MISSION][LOCAL][Sync_anims: ", iPed, "][", GET_DBG_NAME_PED_SYNC_SCENE_ANIM(iAnimation) ,"] - PROCESS_PED_ANIMATION - Cleaning up the Bottle Prop - oiCasinoBrawlBottleObject.")
								DELETE_OBJECT(oiCasinoBrawlBottleObject)
							ENDIF
							
							CLEANUP_SYNC_SCENE_LOCAL_DATA(iPed)
							SET_PED_SYNC_SCENE_STATE(iPed, PED_SYNC_SCENE_STATE_PROCESS_END)
						ENDIF
					BREAK	
				ENDSWITCH
				
			BREAK
			CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CONVERSATION_SECOND
				PROCESS_JUST_LOCAL_CLEANUP(iPed)
			BREAK		
			// ===============================================
			
			CASE ciPED_IDLE_ANIM__CASINO_FIGHT_HEADLOCK
				PROCESS_JUST_LOCAL_CLEANUP(iPed)
			BREAK
			CASE ciPED_IDLE_ANIM__CASINO_FIGHT_HEADLOCK_VICTIM
				PROCESS_JUST_LOCAL_CLEANUP(iPed)
			BREAK
			
			// === Casino Fight/Argue =========================
			CASE ciPED_IDLE_ANIM__CASINO_FIGHT_ARGUE
				PROCESS_JUST_LOCAL_CLEANUP(iPed)
			BREAK
			CASE ciPED_IDLE_ANIM__CASINO_FIGHT_ARGUE_VICTIM
				PROCESS_JUST_LOCAL_CLEANUP(iPed)
			BREAK
			// ===============================================
			
			// === Casino Bar Throw ==========================
			CASE ciPED_IDLE_ANIM__CASINO_FIGHT_BAR_THROW
				PROCESS_JUST_LOCAL_CLEANUP(iPed)
			BREAK
			CASE ciPED_IDLE_ANIM__CASINO_FIGHT_BAR_THROW_PEDESTRIAN
				PROCESS_JUST_LOCAL_CLEANUP(iPed)
			BREAK
			// ===============================================
			
			// === Casino Attack Slot Machine ===
			CASE ciPED_IDLE_ANIM__CASINO_FIGHT_SLOT_SLAM
				PROCESS_JUST_LOCAL_CLEANUP(iPed)
			BREAK
			CASE ciPED_IDLE_ANIM__CASINO_FIGHT_SLOT_SLAM_PEDESTRIAN
				PROCESS_JUST_LOCAL_CLEANUP(iPed)
			BREAK		
			// ===============================================		
			
			// === Casino Chip Steal Scene ===================
			CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CHIP_STEAL_ATTACKER_1
				PROCESS_JUST_LOCAL_CLEANUP(iPed)
			BREAK
			CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CHIP_STEAL_ATTACKER_2
				PROCESS_JUST_LOCAL_CLEANUP(iPed)
			BREAK
			CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CHIP_STEAL_CROUPIER
				PROCESS_JUST_LOCAL_CLEANUP(iPed)
			BREAK
			CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CHIP_STEAL_PEDESTRIAN_1
				PROCESS_JUST_LOCAL_CLEANUP(iPed)
			BREAK
			CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CHIP_STEAL_PEDESTRIAN_2
				PROCESS_JUST_LOCAL_CLEANUP(iPed)
			BREAK
			// ===============================================
			
			CASE ciPED_IDLE_ANIM__CASINO_FIGHT_CASHIER
				PROCESS_JUST_LOCAL_CLEANUP(iPed)
			BREAK
			
			// === Casino Attack Slot Machine =================
			CASE ciPED_IDLE_ANIM__CASINO_SLOTMACHINE_ATTACK
				PROCESS_JUST_LOCAL_CLEANUP(iPed)
			BREAK
			// ===============================================
			
			// === Casino Shop Throw =========================
			CASE ciPED_IDLE_ANIM__CASINO_FIGHT_SHOP_THROW
				PROCESS_JUST_LOCAL_CLEANUP(iPed)
			BREAK
			CASE ciPED_IDLE_ANIM__CASINO_FIGHT_SHOP_THROW_VICTIM
				PROCESS_JUST_LOCAL_CLEANUP(iPed)
			BREAK
			// ================================================
			
			// === Casino Roulette Reaction 1 =================
			CASE ciPED_IDLE_ANIM__CASINO_ROULETTE_REACT_1
				PROCESS_JUST_LOCAL_CLEANUP(iPed)
			BREAK
			// ===============================================
			
			// === Casino Roulette Reaction 2 ================
			CASE ciPED_IDLE_ANIM__CASINO_ROULETTE_REACT_2
				PROCESS_JUST_LOCAL_CLEANUP(iPed)
			BREAK
			// ===============================================
			
			// === Casino Get in Car =========================
			CASE ciPED_IDLE_ANIM__CASINO_GET_IN_CAR_TAO
				PROCESS_JUST_LOCAL_CLEANUP(iPed)
			BREAK
			CASE ciPED_IDLE_ANIM__CASINO_GET_IN_CAR_TRANS
				PROCESS_JUST_LOCAL_CLEANUP(iPed)
			BREAK		
			CASE ciPED_IDLE_ANIM__CASINO_GET_IN_CAR_AGATHA
				PROCESS_JUST_LOCAL_CLEANUP(iPed)
			BREAK
			// ===============================================
		ENDSWITCH
	ENDIF
ENDPROC

PROC APPLY_DONT_DISRUPT(PED_INDEX tempPed)
	SET_RAGDOLL_BLOCKING_FLAGS(tempPed, RBF_PLAYER_IMPACT | RBF_IMPACT_OBJECT | RBF_PED_RAGDOLL_BUMP | RBF_PLAYER_RAGDOLL_BUMP)
	PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - Ped is set to not have his animation disrupted so setting flags now")
ENDPROC

FUNC BOOL SHOULD_PROCESS_PED_ANIMATION(PED_INDEX tempPed, INT iPed)
	UNUSED_PARAMETER(tempPed)
	UNUSED_PARAMETER(iPed)
	
	IF IS_PHONE_EMP_CURRENTLY_ACTIVE()
		PRINTLN("[RCC MISSION] SHOULD_PROCESS_PED_ANIMATION | Returning FALSE for ped ", iPed, " due to IS_PHONE_EMP_CURRENTLY_ACTIVE()")
		RETURN FALSE
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(stPedMetalDetectorInvestigationTimer[iPed])
	AND NOT HAS_NET_TIMER_EXPIRED(stPedMetalDetectorInvestigationTimer[iPed], ciPedMetalDetectorInvestigationTime)
		PRINTLN("[RCC MISSION] SHOULD_PROCESS_PED_ANIMATION | Returning FALSE for ped ", iPed, " due to stPedMetalDetectorInvestigationTimer")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC


//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: PROCESS PED ANIMATION (runs the tasking function with the relevant animations for the ped at the relevant times) !
//
//************************************************************************************************************************************************************



/// PURPOSE: Processes whether peds should be running an animation right now, and calls the tasking function (TASK_PERFORM_AMBIENT_ANIMATION)
///    		with the relevant animation if they should
///    Returns TRUE if the ped is performing an animation
FUNC BOOL PROCESS_PED_ANIMATION(PED_INDEX tempPed, INT iped, BOOL bNotGrouped = TRUE,BOOL bInVehicle = FALSE, BOOL bForceSpecial = FALSE, BOOL bBusyReaction = FALSE, BOOL bBusyReaction_Idle = TRUE)
	
	BOOL bAnimating
	
	IF NOT SHOULD_PROCESS_PED_ANIMATION(tempPed, iPed)
		RETURN FALSE
	ENDIF
	
	//SPECIAL ANIM:
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnim != ciPED_IDLE_ANIM__NONE
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimTeam != -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimRule != -1
	AND NOT bBusyReaction
	OR bForceSpecial
		
		IF bForceSpecial
		OR IS_PED_AT_RIGHT_POINT_FOR_SPECIAL_ANIM(iped,tempPed,bInVehicle)
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].fSpecialAnimTriggerRange < cfMAX_SPECIALANIM_TRIGGER_DIST
			AND NOT bForceSpecial
			// For some reason Lester in Scope Out has data set to check if players are within 50m, but the team bitset is empty - so no players are returned
			AND NOT ((g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].mn = IG_LESTERCREST) AND (iCurrentHeistMissionIndex = HBCA_BS_TUTORIAL_SCOPE_OUT))
				
				FLOAT fClosestDistance2
				INT iClosestPart
				
				iClosestPart = GET_CLOSEST_PART_TO_PED_FROM_TEAMS(tempPed,g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimTriggerTeamBitset, fClosestDistance2)
				
				IF iClosestPart != -1
					
					IF (fClosestDistance2 <= ((g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].fSpecialAnimTriggerRange)*(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].fSpecialAnimTriggerRange)) )
						
						IF NOT IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
							IF IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
								IF NOT IS_PED_DOING_ANIMATION_TRANSITION(iped,tempPed)
									CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_PED_ANIMATION - Clearing ped ",iped," tasks as swapping to special anim, but not doing an anim transition - 1")
									CLEAR_PED_TASKS(tempPed)
								ENDIF
								CLEAR_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
								BROADCAST_FMMC_ANIM_STOPPED(iped)
							ENDIF
							PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - Ped ",iped," starting special animation, iSpecialAnim = ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnim)
							SET_BIT(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
						ENDIF
						
						IF TASK_PERFORM_AMBIENT_ANIMATION(tempPed,iped,g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnim,bForceSpecial, TRUE)
						OR DOES_SPECIAL_ANIM_LOOP(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnim)
							bAnimating = TRUE
							
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetTwelve, ciPed_BSTwelve_DontDisruptAnim)
								APPLY_DONT_DISRUPT(tempPed)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			ELSE
				IF NOT IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					IF IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						IF NOT (IS_PED_DOING_ANIMATION_TRANSITION(iped,tempPed)
						OR (bForceSpecial AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnim != ciPED_IDLE_ANIM__SIT_ON_FLOOR)) //dont clear ped tasks for this jetski anim
							PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - Clearing ped ",iped," tasks as swapping to special anim, but not doing an anim transition - 2")
							CLEAR_PED_TASKS(tempPed)
						ENDIF
						CLEAR_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_ANIM_STOPPED(iped)
					ENDIF
					PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - Ped ",iped," starting special animation, iSpecialAnim = ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnim,", bForceSpecial = ",bForceSpecial)
					SET_BIT(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				ENDIF
				
				IF TASK_PERFORM_AMBIENT_ANIMATION(tempPed,iped,g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnim,bForceSpecial, TRUE)
				OR DOES_SPECIAL_ANIM_LOOP(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnim)
					bAnimating = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//IDLE ANIM:
	IF !bAnimating
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnim != ciPED_IDLE_ANIM__NONE
	AND (bNotGrouped OR CAN_ANIMATION_PLAY_WHILE_GROUPED(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnim))
	AND ((NOT bInVehicle) OR CAN_ANIMATION_PLAY_IN_CAR(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnim))
	AND NOT bBusyReaction
		
		IF IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		AND IS_ANIMATION_INTERRUPTIBLE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnim)
			IF IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_PED_ANIMATION - Clear ped ",iped," tasks, moving back from special animation to idle")
				CLEAR_PED_TASKS(tempPed)
				CLEAR_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			ENDIF
			PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - Ped ",iped," moving back from special animation to idle")
			CLEAR_BIT(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			BROADCAST_FMMC_ANIM_STOPPED(iped, TRUE)
		ENDIF
		
		IF NOT (   IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_IdleAnimOnJustThisRule)
				OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_IdleAnimBeforeAndInclThisRule) 
				OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_IdleAnimAfterAndInclThisRule) )
			bAnimating = TRUE
			PRINTLN("[MJM] bAnimating = TRUE 3")
		ELSE
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnimTeam != -1
			AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnimRule != -1
				BOOL bAtMidPoint,bAtRightRule
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_IdleAnimOnJustThisRule)
					IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnimTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnimRule
						bAtRightRule = TRUE
					ENDIF
				ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_IdleAnimAfterAndInclThisRule)
					IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnimTeam] >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnimRule
						bAtRightRule = TRUE
					ENDIF
				ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_IdleAnimBeforeAndInclThisRule)
					IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnimTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnimRule
						bAtRightRule = TRUE
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetTwo,ciPED_BSTwo_IdleAnim_On_Midpoint)
					IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnimTeam],g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnimRule)
					OR ( (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnim = ciPED_IDLE_ANIM__PRISONER_BREAKOUT_COWER) AND IS_PED_IN_GROUP(tempPed) )
						IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_IdleAnimBeforeAndInclThisRule)
							bAtMidPoint = TRUE
						ENDIF
					ELSE
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_IdleAnimBeforeAndInclThisRule)
							bAtMidPoint = TRUE //Run up to the midpoint and not after!
						ENDIF
					ENDIF
				ELSE
					bAtMidPoint = TRUE
				ENDIF
			
				IF bAtRightRule AND bAtMidPoint
					bAnimating = TRUE
					PRINTLN("[MJM] bAnimating = TRUE 4")
				ENDIF
			ENDIF
		ENDIF
		
		IF bAnimating
			
			BOOL bLookAtPerceptionCheck = FALSE
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iidleAnim = ciPED_IDLE_ANIM__PRISONBREAK_RASHKOVSKY_IDLE
				bLookAtPerceptionCheck = TRUE
			ENDIF
			
			IF NOT IS_BIT_SET(iLocalBoolCheck11, LBOOL11_BLOCK_LOOK_AT_PLAYER_TASK)
				//PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - Ped ", iped," called TASK_PERFORM_LOOK_AT_CLOSEST_PLAYER 1")
				TASK_PERFORM_LOOK_AT_CLOSEST_PLAYER(tempPed, iped, bLookAtPerceptionCheck)
			ELSE
				PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - TASK PERFORM LOOK AT CLOSEST PLAYER BLOCKED 1")
			ENDIF
			
			IF NOT TASK_PERFORM_AMBIENT_ANIMATION(tempPed,iped,g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnim, DEFAULT, FALSE)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnim = ciPED_IDLE_ANIM__GO_FOUNDRY_HOSTAGE_PERP_IDLE // Special case: we don't want the hostage taker to go through the execution breakout anim if the victim has gone
					bAnimating = FALSE
				ENDIF
			ELSE
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetTwelve, ciPed_BSTwelve_DontDisruptAnim)
					APPLY_DONT_DISRUPT(tempPed)
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	IF NOT bAnimating
		//Animation for using a phone while running dialogue:
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_DIALOGUE_ANIM_NEEDED)
		PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION LBOOL6_DIALOGUE_ANIM_NEEDED")
			
			IF NOT IS_CONVERSATION_STATUS_FREE()
				
				BOOL bAnimsNeeded = FALSE // Across all peds
				
				TEXT_LABEL_23 tlConvLbl = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
				
				IF NOT ARE_STRINGS_EQUAL(tlConvLbl,"NULL")
					INT iDLoop
					INT iDialoguePlaying = -1
					
					FOR iDLoop = 0 TO g_FMMC_STRUCT.iNumberOfDialogueTriggers - 1
						TEXT_LABEL_23 tlDLoop
						IF IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sDialogueTriggers[iDLoop].tlRoot)
							tlDLoop = GET_DIALOGUE_LABEL_FROM_ID(g_FMMC_STRUCT.sDialogueTriggers[iDLoop].iFileID, g_FMMC_STRUCT.sDialogueTriggers[iDLoop].iDialogueID)
						ELSE
							tlDLoop = g_FMMC_STRUCT.sDialogueTriggers[iDLoop].tlRoot
						ENDIF
						IF ARE_STRINGS_EQUAL(tlConvLbl,tlDLoop)
							iDialoguePlaying = iDLoop
							iDLoop = g_FMMC_STRUCT.iNumberOfDialogueTriggers
						ENDIF
					ENDFOR
					
					IF iDialoguePlaying != -1 //If the dialogue playing is due to the mission creator
					AND (g_FMMC_STRUCT.sDialogueTriggers[iDialoguePlaying].iPedVoice != -1)
						IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialoguePlaying].iDialogueBitset,iBS_DialogueUsePhone)
						OR g_FMMC_STRUCT.sDialogueTriggers[iDialoguePlaying].iPedAnimation != ciPED_IDLE_ANIM__NONE	
							
							bAnimsNeeded = TRUE // Some ped somewhere needs to be doing an animation along to a dialogue
							
							IF (g_FMMC_STRUCT.sDialogueTriggers[iDialoguePlaying].iPedVoice = iped)
								IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialoguePlaying].iDialogueBitset,iBS_DialogueUsePhone)
									
									SET_PED_CONFIG_FLAG(tempPed,PCF_PhoneDisableTextingAnimations,FALSE)
									SET_PED_CONFIG_FLAG(tempPed,PCF_PhoneDisableTalkingAnimations,FALSE)
									
									TASK_PERFORM_AMBIENT_ANIMATION(tempPed,iped,ciPED_IDLE_ANIM__PHONE, DEFAULT, FALSE)
									bAnimating = TRUE
									PRINTLN("[MJM] bAnimating = TRUE 5")
									PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - IDLE_TASK_PHONE ",iped)
								ELIF g_FMMC_STRUCT.sDialogueTriggers[iDialoguePlaying].iPedAnimation != ciPED_IDLE_ANIM__NONE
									TASK_PERFORM_AMBIENT_ANIMATION(tempPed, iped, g_FMMC_STRUCT.sDialogueTriggers[iDialoguePlaying].iPedAnimation, DEFAULT, FALSE)
									bAnimating = TRUE
									PRINTLN("[MJM] bAnimating = TRUE 6")
									PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - != ciPED_IDLE_ANIM__NONE ",iped)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT bAnimsNeeded // On all peds
					CLEAR_BIT(iLocalBoolCheck6, LBOOL6_DIALOGUE_ANIM_NEEDED)
					PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION CLEAR BIT LBOOL6_DIALOGUE_ANIM_NEEDED 1")
				ENDIF
				
			ELSE
				CLEAR_BIT(iLocalBoolCheck6, LBOOL6_DIALOGUE_ANIM_NEEDED)
				PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION CLEAR BIT LBOOL6_DIALOGUE_ANIM_NEEDED 2")
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bAnimating
	AND bBusyReaction
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn = PLAYER_TWO // Ped is Trevor (for Narcotics Heist Finale)
		
		SWITCH MC_serverBD_2.iPedState[iped]
			
			CASE ciTASK_TAKE_COVER
				
				IF bBusyReaction_Idle
				AND IS_PED_IN_COVER(tempPed)
					
					//Lukasz: comment this out for B*2214154, no longer need these anims to play, but leave it commented in case we need to add it again
					//actually need this back for B*2225816
					IF iTrevorAnimPerforming != ciPED_IDLE_ANIM__NONE
						IF NOT IS_ENTITY_PLAYING_ANIM(tempPed,"anim@heists@narcotics@finale@trevor_cover_idles",GET_TREVOR_SECONDARY_ANIM(iTrevorAnimPerforming),ANIM_SCRIPT)//SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
							
							IF iTrevorIdleAnimTimer >= 0
								iTrevorIdleAnimTimer += ROUND(fLastFrameTime * 1000)
							ENDIF
							
							IF iTrevorIdleAnimTimer >= iTrevorAnimRandomNo
								
								IF iTrevorAnimRandomNo >= 0 // Animation hasn't been chosen yet

									INT iMinRange, iA, iB, iC, iD
									
									iMinRange = -100
									
									//prioritise A & D = 35%
									//B = 10%
									//C = 20%
									
									iA = -35
									iD = -35
									iC = -20
									iB = -10
									
									SWITCH iTrevorAnimPerforming // Last anim that was performed
										CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_A //scratch balls
											iMinRange -= iA
											iA = 0
										BREAK
										CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_D //scratch ass
											iMinRange -= iD
											iD = 0
										BREAK
										CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_C //move rifle
											iMinRange -= iC
											iC = 0
										BREAK
										CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_B //wave
											iMinRange -= iB
											iB = 0
										BREAK
									ENDSWITCH
									
									iTrevorAnimRandomNo = GET_RANDOM_INT_IN_RANGE(iMinRange,0)

									IF iTrevorAnimsRandom = 1
									
										IF iTrevorAnimRandomNo >= iA
											iTrevorAnimPerforming = ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_A
										ELIF iTrevorAnimRandomNo >= iA + iD
											iTrevorAnimPerforming = ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_D
										ELIF iTrevorAnimRandomNo >= iA + iD + iC
											iTrevorAnimPerforming = ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_C
										ELSE
											iTrevorAnimPerforming = ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_B
										ENDIF
										
									ELSE
									
										SWITCH iTrevorAnimsCount
											CASE 0
												iTrevorAnimPerforming = ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_B
											BREAK
											CASE 1
												iTrevorAnimPerforming = ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_A
											BREAK
											CASE 2
												iTrevorAnimPerforming = ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_D
											BREAK
											CASE 3
												iTrevorAnimPerforming = ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_C
											BREAK
										ENDSWITCH
										
									ENDIF
									
									PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - performing cover idle animation ",iTrevorAnimPerforming," on ped ",iped)
								ENDIF
								
								TASK_PERFORM_AMBIENT_ANIMATION(tempPed, iped, iTrevorAnimPerforming, DEFAULT, FALSE)

								bAnimating = TRUE
								PRINTLN("[MJM] bAnimating = TRUE 7")
							ENDIF
						ELSE
							IF iTrevorAnimRandomNo < 0
								iTrevorIdleAnimTimer = 0 // Reset the timer once Trevor is actually playing an animation
								iTrevorAnimRandomNo = GET_RANDOM_INT_IN_RANGE(3000, 7000)
								
								IF iTrevorAnimsRandom = 0
									SWITCH iTrevorAnimPerforming
										CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_B
											iTrevorAnimPerforming = ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_A
										BREAK
										CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_A
											iTrevorAnimPerforming = ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_D
										BREAK
										CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_D
											iTrevorAnimPerforming = ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_C
										BREAK
										CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_C
											iTrevorAnimsRandom = 1
											iTrevorAnimPerforming = ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_A
										BREAK
									ENDSWITCH
								ENDIF
							ENDIF
							
							//TASK_PERFORM_AMBIENT_ANIMATION(tempPed, iped, iTrevorAnimPerforming, DEFAULT, FALSE)
							bAnimating = TRUE
							PRINTLN("[MJM] bAnimating = TRUE 8")
						ENDIF
					ENDIF
					
				ELSE
					//Lukasz: comment this out for B*2214154, no longer need these anims to play, but leave it commented in case we need to add it again
					//actually need this back for B*2225816
					IF iTrevorAnimPerforming != ciPED_IDLE_ANIM__NONE
						iTrevorIdleAnimTimer = 0 // Reset the timer if Trevor is no longer idle
						
						IF iTrevorAnimRandomNo < 0
							iTrevorAnimRandomNo = GET_RANDOM_INT_IN_RANGE(3000, 7000)
							
							IF iTrevorAnimsRandom = 0
								SWITCH iTrevorAnimPerforming
									CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_B
										iTrevorAnimPerforming = ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_A
									BREAK
									CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_A
										iTrevorAnimPerforming = ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_D
									BREAK
									CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_D
										iTrevorAnimPerforming = ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_C
									BREAK
									CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_C
										iTrevorAnimsRandom = 1
										iTrevorAnimPerforming = ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_A
									BREAK
								ENDSWITCH
							ENDIF
							
						ENDIF
					ENDIF
					
					IF IS_ENTITY_PLAYING_ANIM(tempPed,"anim@heists@narcotics@finale@trevor_cover_idles",GET_TREVOR_SECONDARY_ANIM(ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_A),ANIM_SCRIPT)
						IF GET_ENTITY_ANIM_CURRENT_TIME(tempPed, "anim@heists@narcotics@finale@trevor_cover_idles",GET_TREVOR_SECONDARY_ANIM(ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_A)) >= 0.99
							IF IS_ENTITY_AT_COORD(tempPed, <<601.5997, -404.5632, 23.7372>>, <<0.5, 0.5, 1.0>>) AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[0].vPos)
								TASK_PUT_PED_DIRECTLY_INTO_COVER(tempPed, g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[0].vPos,-1,FALSE)
								PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - Putting Trevor directly into cover at cover point in position ", g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[0].vPos)
							ELSE
								TASK_PUT_PED_DIRECTLY_INTO_COVER(tempPed, GET_ENTITY_COORDS(tempPed),-1,FALSE)
								PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - Putting Trevor directly into cover at current ped position.")
							ENDIF
							CLEAR_RAGDOLL_BLOCKING_FLAGS(tempPed, RBF_MELEE|RBF_PED_RAGDOLL_BUMP|RBF_PLAYER_BUMP|RBF_PLAYER_IMPACT)
						ENDIF
						bAnimating = TRUE
					ENDIF
					
					IF IS_ENTITY_PLAYING_ANIM(tempPed,"anim@heists@narcotics@finale@trevor_cover_idles",GET_TREVOR_SECONDARY_ANIM(ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_B),ANIM_SCRIPT)
						IF GET_ENTITY_ANIM_CURRENT_TIME(tempPed, "anim@heists@narcotics@finale@trevor_cover_idles",GET_TREVOR_SECONDARY_ANIM(ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_B)) >= 0.99
							IF IS_ENTITY_AT_COORD(tempPed, <<601.5997, -404.5632, 23.7372>>, <<0.5, 0.5, 1.0>>) AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[0].vPos)
								TASK_PUT_PED_DIRECTLY_INTO_COVER(tempPed, g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[0].vPos,-1,FALSE)
								PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - Putting Trevor directly into cover at cover point in position ", g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[0].vPos)
							ELSE
								TASK_PUT_PED_DIRECTLY_INTO_COVER(tempPed, GET_ENTITY_COORDS(tempPed),-1,FALSE)
								PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - Putting Trevor directly into cover at current ped position.")
							ENDIF
							CLEAR_RAGDOLL_BLOCKING_FLAGS(tempPed, RBF_MELEE|RBF_PED_RAGDOLL_BUMP|RBF_PLAYER_BUMP|RBF_PLAYER_IMPACT)
						ENDIF
						bAnimating = TRUE
					ENDIF
					
					IF IS_ENTITY_PLAYING_ANIM(tempPed,"anim@heists@narcotics@finale@trevor_cover_idles",GET_TREVOR_SECONDARY_ANIM(ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_C),ANIM_SCRIPT)
						IF GET_ENTITY_ANIM_CURRENT_TIME(tempPed, "anim@heists@narcotics@finale@trevor_cover_idles",GET_TREVOR_SECONDARY_ANIM(ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_C)) >= 0.99
							IF IS_ENTITY_AT_COORD(tempPed, <<601.5997, -404.5632, 23.7372>>, <<0.5, 0.5, 1.0>>) AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[0].vPos)
								TASK_PUT_PED_DIRECTLY_INTO_COVER(tempPed, g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[0].vPos,-1,FALSE)
								PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - Putting Trevor directly into cover at cover point in position ", g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[0].vPos)
							ELSE
								TASK_PUT_PED_DIRECTLY_INTO_COVER(tempPed, GET_ENTITY_COORDS(tempPed),-1,FALSE)
								PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - Putting Trevor directly into cover at current ped position.")
							ENDIF
							CLEAR_RAGDOLL_BLOCKING_FLAGS(tempPed, RBF_MELEE|RBF_PED_RAGDOLL_BUMP|RBF_PLAYER_BUMP|RBF_PLAYER_IMPACT)
						ENDIF
						bAnimating = TRUE
					ENDIF
					
					IF IS_ENTITY_PLAYING_ANIM(tempPed,"anim@heists@narcotics@finale@trevor_cover_idles",GET_TREVOR_SECONDARY_ANIM(ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_D),ANIM_SCRIPT)
						IF GET_ENTITY_ANIM_CURRENT_TIME(tempPed, "anim@heists@narcotics@finale@trevor_cover_idles",GET_TREVOR_SECONDARY_ANIM(ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_D)) >= 0.99
							IF IS_ENTITY_AT_COORD(tempPed, <<601.5997, -404.5632, 23.7372>>, <<0.5, 0.5, 1.0>>) AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[0].vPos)
								TASK_PUT_PED_DIRECTLY_INTO_COVER(tempPed, g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[0].vPos,-1,FALSE)
								PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - Putting Trevor directly into cover at cover point in position ", g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[0].vPos)
							ELSE
								TASK_PUT_PED_DIRECTLY_INTO_COVER(tempPed, GET_ENTITY_COORDS(tempPed),-1,FALSE)
								PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - Putting Trevor directly into cover at current ped position.")
							ENDIF
							CLEAR_RAGDOLL_BLOCKING_FLAGS(tempPed, RBF_MELEE|RBF_PED_RAGDOLL_BUMP|RBF_PLAYER_BUMP|RBF_PLAYER_IMPACT)
						ENDIF
						bAnimating = TRUE
					ENDIF
					
					IF IS_ENTITY_PLAYING_ANIM(tempPed,"anim@heists@narcotics@finale@trevor_cover_idles",GET_TREVOR_SECONDARY_ANIM(ciPED_IDLE_ANIM__TREVOR_NARCFIN_VIGNETTE_01),ANIM_SCRIPT)
						IF GET_ENTITY_ANIM_CURRENT_TIME(tempPed, "anim@heists@narcotics@finale@trevor_cover_idles",GET_TREVOR_SECONDARY_ANIM(ciPED_IDLE_ANIM__TREVOR_NARCFIN_VIGNETTE_01)) >= 0.99
							IF IS_ENTITY_AT_COORD(tempPed, <<601.5997, -404.5632, 23.7372>>, <<0.5, 0.5, 1.0>>) AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[0].vPos)
								TASK_PUT_PED_DIRECTLY_INTO_COVER(tempPed, g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[0].vPos,-1,FALSE, 0.5)
								PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - Putting Trevor directly into cover at cover point in position ", g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[0].vPos)
							ELSE
								TASK_PUT_PED_DIRECTLY_INTO_COVER(tempPed, GET_ENTITY_COORDS(tempPed),-1,FALSE)
								PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - Putting Trevor directly into cover at current ped position.")
							ENDIF
							CLEAR_RAGDOLL_BLOCKING_FLAGS(tempPed, RBF_MELEE|RBF_PED_RAGDOLL_BUMP|RBF_PLAYER_BUMP|RBF_PLAYER_IMPACT)
						ENDIF
						bAnimating = TRUE
					ENDIF
					
					IF IS_ENTITY_PLAYING_ANIM(tempPed,"anim@heists@narcotics@finale@trevor_cover_idles",GET_TREVOR_SECONDARY_ANIM(ciPED_IDLE_ANIM__TREVOR_NARCFIN_VIGNETTE_02_L),ANIM_SCRIPT)
						IF GET_ENTITY_ANIM_CURRENT_TIME(tempPed, "anim@heists@narcotics@finale@trevor_cover_idles",GET_TREVOR_SECONDARY_ANIM(ciPED_IDLE_ANIM__TREVOR_NARCFIN_VIGNETTE_02_L)) >= 0.99
							IF IS_ENTITY_AT_COORD(tempPed, <<601.5997, -404.5632, 23.7372>>, <<0.5, 0.5, 1.0>>) AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[0].vPos)
								TASK_PUT_PED_DIRECTLY_INTO_COVER(tempPed, g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[0].vPos,-1,FALSE,0,TRUE,TRUE)
								PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - Putting Trevor directly into cover facing left at cover point in position ", g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[0].vPos)
							ELSE
								TASK_PUT_PED_DIRECTLY_INTO_COVER(tempPed, GET_ENTITY_COORDS(tempPed),-1,FALSE,0,TRUE,TRUE)
								PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - Putting Trevor directly into cover facing left at current ped position.")
							ENDIF
							CLEAR_RAGDOLL_BLOCKING_FLAGS(tempPed, RBF_MELEE|RBF_PED_RAGDOLL_BUMP|RBF_PLAYER_BUMP|RBF_PLAYER_IMPACT)
						ENDIF
						bAnimating = TRUE
					ENDIF
					
					IF IS_ENTITY_PLAYING_ANIM(tempPed,"anim@heists@narcotics@finale@trevor_cover_idles",GET_TREVOR_SECONDARY_ANIM(ciPED_IDLE_ANIM__TREVOR_NARCFIN_VIGNETTE_02_R),ANIM_SCRIPT)
						IF GET_ENTITY_ANIM_CURRENT_TIME(tempPed, "anim@heists@narcotics@finale@trevor_cover_idles",GET_TREVOR_SECONDARY_ANIM(ciPED_IDLE_ANIM__TREVOR_NARCFIN_VIGNETTE_02_R)) >= 0.99
							IF IS_ENTITY_AT_COORD(tempPed, <<601.5997, -404.5632, 23.7372>>, <<0.5, 0.5, 1.0>>) AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[0].vPos)
								TASK_PUT_PED_DIRECTLY_INTO_COVER(tempPed, g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[0].vPos,-1,FALSE,0,TRUE,FALSE)
								PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - Putting Trevor directly into cover facing right at cover point in position ", g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[0].vPos)
							ELSE
								TASK_PUT_PED_DIRECTLY_INTO_COVER(tempPed, GET_ENTITY_COORDS(tempPed),-1,FALSE,0,TRUE,FALSE)
								PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - Putting Trevor directly into cover facing right at current ped position.")
							ENDIF
							CLEAR_RAGDOLL_BLOCKING_FLAGS(tempPed, RBF_MELEE|RBF_PED_RAGDOLL_BUMP|RBF_PLAYER_BUMP|RBF_PLAYER_IMPACT)
						ENDIF
						bAnimating = TRUE
					ENDIF
					
				ENDIF
			BREAK
			
			CASE ciTASK_GOTO_ENTITY
				
				IF bBusyReaction_Idle
				AND IS_PED_IN_ANY_VEHICLE(tempPed)
				AND (GET_VEHICLE_DOOR_ANGLE_RATIO(GET_VEHICLE_PED_IS_IN(tempPed),SC_DOOR_FRONT_RIGHT) < 0.1)
					IF SHOULD_TREVOR_BE_GIVEN_VAN_LEAN_OUT_IDLE(tempPed, iped)
						TASK_PERFORM_AMBIENT_ANIMATION(tempPed, iped, ciPED_IDLE_ANIM__TREVOR_NARCFIN_VAN_INTRO_AND_IDLE_LOOP, DEFAULT, FALSE)
						bAnimating = TRUE
					ENDIF
				ENDIF
				
			BREAK
			
		ENDSWITCH
		
	ENDIF
	
	//If we're not animating, but we used to be - clear tasks to stop
	IF NOT bAnimating
		PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - Not animating ",iped)
		IF IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))		
			PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - ipedAnimBitset SET ",iped)
		//AND NOT IS_BIT_SET(iLocalBoolCheck9, LBOOL9_STARTING_PHONE_ANIM)
			IF NOT IS_PED_DOING_ANIMATION_TRANSITION(iped, tempPed, TRUE, bBusyReaction)
			AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnim != ciPED_IDLE_ANIM__PRISONBREAK_RASHKOVSKY_IDLE //Tasks will get cleared by the upcoming mocap
				PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - Clearing ped ",iped," tasks as no longer performing an animation")
				CLEAR_PED_TASKS(tempPed)
				IF NOT IS_PED_IN_GROUP(tempPed)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,FALSE)
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[RCC MISSION] PROCESS_PED_ANIMATION - Not clearing ped ",iped," tasks; no longer performing an animation but ped is doing animation transition (or is Rashkovsky anim and about to go into mocap)")
			#ENDIF
			ENDIF
			
			CLEAR_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			BROADCAST_FMMC_ANIM_STOPPED(iped)
		ENDIF
	ENDIF
	
	RETURN bAnimating
	
ENDFUNC



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: PED COLLISION LOADING !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************



FUNC BOOL IS_IT_SAFE_TO_LOAD_NAVMESH()
	
	// Only one script can use ADD_NAVMESH_REQUIRED_REGION at a time, so we must let other scripts use it when they need it
	
	IF NOT bLocalPlayerPedOk
	OR IS_PLAYER_RESPAWNING(LocalPlayer)
		// The respawning system needs navmesh
		PRINTLN("IS_IT_SAFE_TO_LOAD_NAVMESH - Returning FALSE, bLocalPlayerPedOk = ",bLocalPlayerPedOk,", IS_PLAYER_RESPAWNING = ",IS_PLAYER_RESPAWNING(LocalPlayer))
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(LocalPlayer)
		// The simple interior loading/unloading requires a player warp, which uses navmesh
		PRINTLN("IS_IT_SAFE_TO_LOAD_NAVMESH - Returning FALSE, IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR is TRUE")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

PROC PROCESS_NAVMESH_UNLOADING_WHEN_UNSAFE()
	
	IF IS_BIT_SET(iLocalBoolCheck8, LBOOL8_NAVMESH_REGION_REQUESTED)
		IF NOT IS_IT_SAFE_TO_LOAD_NAVMESH()
			
			PRINTLN("[LOAD_COLLISION_FLAG] PROCESS_NAVMESH_UNLOADING_WHEN_UNSAFE - IS_IT_SAFE_TO_LOAD_NAVMESH returning FALSE, remove navmesh")
			REMOVE_NAVMESH_REQUIRED_REGIONS()
			
			#IF IS_DEBUG_BUILD
			INT iPedsWithLoadedNavmesh
			#ENDIF
			
			IF iPedNavmeshLoadedBS[0] != 0
			OR iPedNavmeshLoadedBS[1] != 0
			OR iPedNavmeshLoadedBS[2] != 0
				INT iped
				
				FOR iped = 0 TO (MC_serverBD.iNumPedCreated - 1)
					IF IS_BIT_SET(iPedNavmeshLoadedBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
						PRINTLN("[LOAD_COLLISION_FLAG] PROCESS_NAVMESH_UNLOADING_WHEN_UNSAFE - Clearing iPedNavmeshLoadedBS & iPedNavmeshCheckedBS for ped ",iped)
						CLEAR_BIT(iPedNavmeshCheckedBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
						CLEAR_BIT(iPedNavmeshLoadedBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
						
						#IF IS_DEBUG_BUILD
						iPedsWithLoadedNavmesh++
						#ENDIF
					ENDIF
				ENDFOR
			ENDIF
			
			IF iBackupPedNavmeshLoadedBS != 0
				INT iBackupUnit
				
				FOR iBackupUnit = 0 TO (MAX_BACKUP_VEHICLES - 1)
					IF IS_BIT_SET(iBackupPedNavmeshLoadedBS, iBackupUnit)
						PRINTLN("[LOAD_COLLISION_FLAG][gang_backup] PROCESS_NAVMESH_UNLOADING_WHEN_UNSAFE - Clearing iBackupPedNavmeshLoadedBS & iBackupPedNavmeshCheckedBS for backup unit ",iBackupUnit)
						CLEAR_BIT(iBackupPedNavmeshCheckedBS, iBackupUnit)
						CLEAR_BIT(iBackupPedNavmeshLoadedBS, iBackupUnit)
						
						#IF IS_DEBUG_BUILD
						iPedsWithLoadedNavmesh++
						#ENDIF
					ENDIF
				ENDFOR
			ENDIF
			
			#IF IS_DEBUG_BUILD
			IF iPedsWithLoadedNavmesh != 1
				PRINTLN("[LOAD_COLLISION_FLAG] PROCESS_NAVMESH_UNLOADING_WHEN_UNSAFE - Found ",iPedsWithLoadedNavmesh," peds with loaded navmesh, should only be one!")
				CASSERTLN(DEBUG_CONTROLLER, "[LOAD_COLLISION_FLAG] PROCESS_NAVMESH_UNLOADING_WHEN_UNSAFE - Found ",iPedsWithLoadedNavmesh," peds with loaded navmesh, should only be one!")
			ELSE
				PRINTLN("[LOAD_COLLISION_FLAG] PROCESS_NAVMESH_UNLOADING_WHEN_UNSAFE - Only found 1 ped with loaded navmesh, as expected :)")
			ENDIF
			#ENDIF
			
			CLEAR_BIT(iLocalBoolCheck8, LBOOL8_NAVMESH_REGION_REQUESTED)
		ENDIF
	ENDIF
	
ENDPROC

PROC CLEANUP_COLLISION_ON_PED(PED_INDEX tempPed,INT iped)

	IF tempPed != NULL
	AND DOES_ENTITY_EXIST(tempPed)
		IF NOT IS_ENTITY_DEAD(tempPed)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(tempPed)
				SET_ENTITY_LOAD_COLLISION_FLAG(tempPed, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iPedCollisionBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		IF iCollisionStreamingLimit > 0
			iCollisionStreamingLimit--
		ELSE
			PRINTLN("[LOAD_COLLISION_FLAG] CLEANUP_COLLISION_ON_PED - iCollisionStreamingLimit somehow dropping to below 0, somebody messed up!")
			SCRIPT_ASSERT("CLEANUP_COLLISION_ON_PED - iCollisionStreamingLimit somehow dropping to below 0, somebody messed up!")
		ENDIF
		
		CLEAR_BIT(iPedCollisionBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		PRINTLN("[LOAD_COLLISION_FLAG] Clearing iPedCollisionBitset for ped: ",iped, " current requests: ",iCollisionStreamingLimit)
	ENDIF
	
	IF IS_BIT_SET(iPedNavmeshCheckedBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		IF IS_BIT_SET(iPedNavmeshLoadedBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			IF IS_BIT_SET(iLocalBoolCheck8, LBOOL8_NAVMESH_REGION_REQUESTED)
				PRINTLN("[LOAD_COLLISION_FLAG] CLEANUP_COLLISION_ON_PED - Calling REMOVE_NAVMESH_REQUIRED_REGIONS for ped: ",iped,".")
				REMOVE_NAVMESH_REQUIRED_REGIONS()
				CLEAR_BIT(iLocalBoolCheck8, LBOOL8_NAVMESH_REGION_REQUESTED)
			ELSE
				PRINTLN("[LOAD_COLLISION_FLAG] CLEANUP_COLLISION_ON_PED - iPedNavmeshLoadedBS set, but no navmesh region requested")
				SCRIPT_ASSERT("CLEANUP_COLLISION_ON_PED - iPedNavmeshLoadedBS set, but no navmesh region requested!")
			ENDIF
			
			CLEAR_BIT(iPedNavmeshLoadedBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			PRINTLN("[LOAD_COLLISION_FLAG] CLEANUP_COLLISION_ON_PED - Clearing iPedNavmeshLoadedBS for ped: ",iped,".")
		ENDIF
		
		CLEAR_BIT(iPedNavmeshCheckedBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		PRINTLN("CLEANUP_COLLISION_ON_PED - Clearing iPedNavmeshCheckedBS for ped: ",iped,".")
	ENDIF
	
ENDPROC

PROC LOAD_NAVMESH_ON_PED_IF_POSSIBLE(PED_INDEX tempPed, INT iped)
	
	IF NOT IS_BIT_SET(iPedNavmeshCheckedBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		IF IS_IT_SAFE_TO_LOAD_NAVMESH()
			IF NOT IS_BIT_SET(iLocalBoolCheck8, LBOOL8_NAVMESH_REGION_REQUESTED)
				
				INT			i, j, k
				VECTOR 		vPedCoords
				PED_INDEX 	nearbyPeds[8]
				
				i 			= GET_PED_NEARBY_PEDS(tempPed, nearbyPeds)
				vPedCoords 	= GET_ENTITY_COORDS(tempPed)
				
				PRINTLN("LOAD_NAVMESH_ON_PED_IF_POSSIBLE - Checking navmesh required region for ped: ",iped,".")
				
				IF ( i > 0 )
					PRINTLN("LOAD_NAVMESH_ON_PED_IF_POSSIBLE - Checking navmesh required region for ped as there are peds around this ped: ",iped,".")
					
					REPEAT COUNT_OF(nearbyPeds) j
						IF DOES_ENTITY_EXIST(nearbyPeds[j]) AND NOT IS_ENTITY_DEAD(nearbyPeds[j])
							IF GET_PED_RELATIONSHIP_GROUP_HASH(tempPed) = GET_PED_RELATIONSHIP_GROUP_HASH(nearbyPeds[j])
								IF VDIST2(vPedCoords, GET_ENTITY_COORDS(nearbyPeds[j])) < 50*50
									k++
									PRINTLN("LOAD_NAVMESH_ON_PED_IF_POSSIBLE - Adding nearby friendly peds to ped: ",iped," currently friendly neighbours at ", k,".")
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
					
					IF ( k > 5 )
						ADD_NAVMESH_REQUIRED_REGION(vPedCoords.x, vPedCoords.y, 125.0)
						SET_BIT(iPedNavmeshLoadedBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
						SET_BIT(iLocalBoolCheck8, LBOOL8_NAVMESH_REGION_REQUESTED)
						PRINTLN("[LOAD_COLLISION_FLAG] LOAD_NAVMESH_ON_PED_IF_POSSIBLE - Adding navmesh required region for ped: ",iped,".")
					ENDIF
				ELSE
					PRINTLN("LOAD_NAVMESH_ON_PED_IF_POSSIBLE - Navmesh required region not needed as no other peds around ped: ",iped,".")
				ENDIF
				
				SET_BIT(iPedNavmeshCheckedBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			ENDIF
		ELSE
			PRINTLN("LOAD_NAVMESH_ON_PED_IF_POSSIBLE - IS_IT_SAFE_TO_LOAD_NAVMESH returning FALSE, waiting to add navmesh required for ped: ",iped,".")
		ENDIF
	ENDIF
	
ENDPROC

PROC LOAD_COLLISION_ON_PED_IF_POSSIBLE(PED_INDEX tempPed, INT iped)
	
	// We know the ped is not injured and that we have control if we're calling this
	
	IF NOT IS_BIT_SET(iPedCollisionBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		IF IS_ENTITY_WAITING_FOR_WORLD_COLLISION(tempPed)
		OR (IS_PED_IN_ANY_VEHICLE(tempPed) AND IS_ENTITY_WAITING_FOR_WORLD_COLLISION(GET_VEHICLE_PED_IS_IN(tempPed)))
			IF iCollisionStreamingLimit <= ciCOLLISION_STREAMING_MAX
				
				SET_ENTITY_LOAD_COLLISION_FLAG(tempPed, TRUE)
				
				iCollisionStreamingLimit++
				SET_BIT(iPedCollisionBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				PRINTLN("[LOAD_COLLISION_FLAG] LOAD_COLLISION_ON_PED_IF_POSSIBLE - Loading collision for ped: ",iped, " current requests: ",iCollisionStreamingLimit)
				
				LOAD_NAVMESH_ON_PED_IF_POSSIBLE(tempPed, iped)
				
			ELSE
				PRINTLN("[LOAD_COLLISION_FLAG] LOAD_COLLISION_ON_PED_IF_POSSIBLE - iCollisionStreamingLimit reached! Shelved CL 13920263 has a possible solution. ped: ",iped)
				SCRIPT_ASSERT("[LOAD_COLLISION_FLAG] LOAD_COLLISION_ON_PED_IF_POSSIBLE - iCollisionStreamingLimit reached!")
			ENDIF
		ENDIF
	ELSE
		LOAD_NAVMESH_ON_PED_IF_POSSIBLE(tempPed, iped)
	ENDIF

ENDPROC



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: PED DIALOGUE / SPEECH !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************



FUNC BOOL TURN_TO_FACE_PLAYER(PED_INDEX tempPed,PED_INDEX ClosestPlayerPed,INT iped)

INT itaskTime
	
	IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAggroTime != -1
			itaskTime =-1// (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAggroTime*1000)
		ELSE
			itaskTime = 10000
		ENDIF
		TASK_TURN_PED_TO_FACE_ENTITY(tempPed, ClosestPlayerPed, itaskTime)
		TASK_LOOK_AT_ENTITY(tempPed, ClosestPlayerPed,itaskTime,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_PITCH_LIMIT | SLF_WIDEST_YAW_LIMIT,SLF_LOOKAT_VERY_HIGH)
		PRINTLN("[RCC MISSION] TURN_TO_FACE_PLAYER - turning to player")
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL AIM_AT_PLAYER(PED_INDEX tempPed,PED_INDEX ClosestPlayerPed,INT iped)

INT itaskTime
	
	IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_AIM_GUN_AT_ENTITY)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAggroTime != -1
			itaskTime =-1// (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAggroTime*1000)
		ELSE
			itaskTime = 10000
		ENDIF
		SET_CURRENT_PED_WEAPON(tempPed,GET_BEST_PED_WEAPON(tempPed),TRUE)
		TASK_AIM_GUN_AT_ENTITY(tempPed, ClosestPlayerPed, itaskTime)
		TASK_LOOK_AT_ENTITY(tempPed, ClosestPlayerPed,itaskTime,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_PITCH_LIMIT | SLF_WIDEST_YAW_LIMIT,SLF_LOOKAT_VERY_HIGH)
		PRINTLN("Aiming at player")
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC PLAY_AMBIENT_STOP_DIALOGUE(PED_INDEX tempPed)
	TEXT_LABEL_23 sRoot = "PBF_STOP"
	
	INT iCurrentPedSpeaker = GET_SPEAKER_INT_FROM_ROOT(sRoot, FMMC_MAX_SPEAKERS)
	IF iCurrentPedSpeaker >= constMaxNum_Conversers
		PRINTLN("PLAY_AMBIENT_STOP_DIALOGUE - iCurrentSpeaker >= constMaxNum_Conversers setting to 0 from ", iCurrentPedSpeaker)
		iCurrentPedSpeaker = 0
	ENDIF
	
	ADD_PED_FOR_DIALOGUE(speechPedStruct, iCurrentPedSpeaker, tempPed, g_FMMC_STRUCT.sSpeakers[GET_SPEAKER_INT_FROM_ROOT(sRoot, FMMC_MAX_SPEAKERS)])
	IF CREATE_CONVERSATION(speechPedStruct, "PBFAU", sRoot, CONV_PRIORITY_MEDIUM)
		PRINTLN("CREATE_CONVERSATION - created new dialogue PBF_STOP")
	ENDIF
ENDPROC

FUNC STRING GET_FORCED_COP_VOICE_TYPE_FROM_INT(Int iForcedVoiceType, Int iPed)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDialogueLine = FMMC_DIALOGUE_LINE_PROVOKE_TRESPASS_SECURITY
		RETURN "S_M_M_GENERICSECURITY_01_WHITE_MINI_01"
	ENDIF
	
	IF iForcedVoiceType > -1
		SWITCH iForcedVoiceType
			CASE 0
				RETURN "S_M_Y_Cop_01_BLACK_FULL_01"
			CASE 1
				RETURN "S_M_Y_Cop_01_WHITE_FULL_01"
		ENDSWITCH
	ENDIF
	RETURN ""
ENDFUNC

PROC MANAGE_DIALOGUE_TURN_AND_TRIGGER(PED_INDEX tempPed,PED_INDEX ClosestPlayerPed,INT iped,STRING sDialogue,BOOL binCombat,INT iPlayerTeam, STRING sParams, BOOL bPlayFromPos = FALSE)
	
	PRINTLN("MANAGE_DIALOGUE_TURN_AND_TRIGGER - iped: ", iped)
	
	IF NOT IS_BIT_SET(iPedInitSpeechBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		SET_BIT(iPedInitSpeechBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		IF NOT binCombat
			SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(tempPed)
			CLEAR_PED_TASKS(tempPed)
		ENDIF
		IF IS_BIT_SET(iPedAgroStageOne[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iTeam[iPlayerTeam] = ciPED_RELATION_SHIP_DISLIKE
			OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iTeam[iPlayerTeam] = ciPED_RELATION_SHIP_HATE)
				PRINTLN("iPedAgroStageOne has been triggered for ped ",iped)
				SET_BIT(iPedSpeechAimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			ENDIF
		ENDIF
	ENDIF
	IF NOT binCombat
		PRINTLN("MANAGE_DIALOGUE_TURN_AND_TRIGGER - NOT In Combat")
		IF IS_BIT_SET(iPedSpeechAimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			IF AIM_AT_PLAYER(tempPed, ClosestPlayerPed,iped)
				PRINTLN("MANAGE_DIALOGUE_TURN_AND_TRIGGER Aim at Player")
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPed_BSEight_OverrideCurrentPlayingDialogue)
					PRINTLN("MANAGE_DIALOGUE_TURN_AND_TRIGGER - Override Playing Dialogue Set")
					IF IS_ANY_SPEECH_PLAYING(tempPed)
						PRINTLN("MANAGE_DIALOGUE_TURN_AND_TRIGGER - Stopping Current Dialogue")
						STOP_CURRENT_PLAYING_SPEECH(tempPed)
					ENDIF
				ENDIF
				
				IF ARE_STRINGS_EQUAL(sDialogue,"PBF_STOP")
					PLAY_AMBIENT_STOP_DIALOGUE(tempPed)
				ELSE
					RESET_NET_TIMER(PedDelayDialogueTimer[iped])
					IF bPlayFromPos
						PLAY_AMBIENT_SPEECH_FROM_POSITION_NATIVE(sDialogue, GET_FORCED_COP_VOICE_TYPE_FROM_INT(1, iped), GET_ENTITY_COORDS(tempPed), sParams)
						PRINTLN("PLAY_AMBIENT_SPEECH_FROM_POSITION_NATIVE triggered with context: ",sDialogue)
					ELSE
						PLAY_PED_AMBIENT_SPEECH_AND_CLONE_NATIVE(tempPed, sDialogue, sParams)
						PRINTLN("PLAY_PED_AMBIENT_SPEECH_NATIVE triggered with context: ", sDialogue)
					ENDIF
				ENDIF
				SET_BIT(iPedSpeechTriggeredBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				BROADCAST_DIALOGUE_TRIGGER_LOOK_EVENT(iped)
				PRINTLN("iDialogueLine: ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDialogueLine)
			ENDIF
		ELSE
			IF IS_PED_IN_ANY_VEHICLE(tempPed,TRUE)
				PRINTLN("MANAGE_DIALOGUE_TURN_AND_TRIGGER In Vehicle")
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPed_BSEight_OverrideCurrentPlayingDialogue)
					PRINTLN("MANAGE_DIALOGUE_TURN_AND_TRIGGER - Override Playing Dialogue Set")
					IF IS_ANY_SPEECH_PLAYING(tempPed)
						PRINTLN("PLAY_PED_AMBIENT_SPEECH_NATIVE - Stopping Current Dialogue")
						STOP_CURRENT_PLAYING_SPEECH(tempPed)
					ENDIF
				ENDIF
				
				TASK_LOOK_AT_ENTITY(tempPed, ClosestPlayerPed,10000,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_PITCH_LIMIT | SLF_WIDEST_YAW_LIMIT,SLF_LOOKAT_VERY_HIGH)
				IF ARE_STRINGS_EQUAL(sDialogue,"PBF_STOP")
					PLAY_AMBIENT_STOP_DIALOGUE(tempPed)
				ELSE
					RESET_NET_TIMER(PedDelayDialogueTimer[iped])
					
					IF bPlayFromPos
						PLAY_AMBIENT_SPEECH_FROM_POSITION_NATIVE(sDialogue, GET_FORCED_COP_VOICE_TYPE_FROM_INT(1, iped), GET_ENTITY_COORDS(tempPed), sParams)
						PRINTLN("PLAY_AMBIENT_SPEECH_FROM_POSITION_NATIVE triggered with context: ",sDialogue)
					ELSE
						PLAY_PED_AMBIENT_SPEECH_AND_CLONE_NATIVE(tempPed,sDialogue,sParams)
						PRINTLN("PLAY_PED_AMBIENT_SPEECH_NATIVE triggered with context: ",sDialogue)
					ENDIF
				ENDIF
				SET_BIT(iPedSpeechTriggeredBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				BROADCAST_DIALOGUE_TRIGGER_LOOK_EVENT(iped)
				PRINTLN("iDialogueLine: ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDialogueLine)
			ELIF TURN_TO_FACE_PLAYER(tempPed, ClosestPlayerPed,iped)
			OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThirteen, ciPED_BSThirteen_BypassDialogueTurnFaceTasking)
				PRINTLN("MANAGE_DIALOGUE_TURN_AND_TRIGGER Turn to Face Player")
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPed_BSEight_OverrideCurrentPlayingDialogue)
					PRINTLN("MANAGE_DIALOGUE_TURN_AND_TRIGGER - Override Playing Dialogue Set")
					IF IS_ANY_SPEECH_PLAYING(tempPed)
						PRINTLN("MANAGE_DIALOGUE_TURN_AND_TRIGGER - Stopping Current Dialogue")
						STOP_CURRENT_PLAYING_SPEECH(tempPed)
					ENDIF
				ENDIF
				
				IF ARE_STRINGS_EQUAL(sDialogue,"PBF_STOP")
					PLAY_AMBIENT_STOP_DIALOGUE(tempPed)
				ELSE
					RESET_NET_TIMER(PedDelayDialogueTimer[iped])
					IF bPlayFromPos
						PLAY_AMBIENT_SPEECH_FROM_POSITION_NATIVE(sDialogue, GET_FORCED_COP_VOICE_TYPE_FROM_INT(1, iped), GET_ENTITY_COORDS(tempPed), sParams)
						PRINTLN("PLAY_AMBIENT_SPEECH_FROM_POSITION_NATIVE triggered with context: ",sDialogue)
					ELSE
						PLAY_PED_AMBIENT_SPEECH_AND_CLONE_NATIVE(tempPed,sDialogue,sParams)
						PRINTLN("PLAY_PED_AMBIENT_SPEECH_NATIVE triggered with context: ",sDialogue)
					ENDIF
				ENDIF
				SET_BIT(iPedSpeechTriggeredBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				BROADCAST_DIALOGUE_TRIGGER_LOOK_EVENT(iped)
				PRINTLN("iDialogueLine: ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDialogueLine)
			ENDIF
		ENDIF
	ELSE
		PRINTLN("MANAGE_DIALOGUE_TURN_AND_TRIGGER - In Combat")
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPed_BSEight_OverrideCurrentPlayingDialogue)
			PRINTLN("MANAGE_DIALOGUE_TURN_AND_TRIGGER - Override Playing Dialogue Set")
			IF IS_ANY_SPEECH_PLAYING(tempPed)
				PRINTLN("MANAGE_DIALOGUE_TURN_AND_TRIGGER - Stopping Current Dialogue")
				STOP_CURRENT_PLAYING_SPEECH(tempPed)
			ENDIF
		ENDIF
		
		IF ARE_STRINGS_EQUAL(sDialogue,"PBF_STOP")
			PLAY_AMBIENT_STOP_DIALOGUE(tempPed)
		ELSE
			RESET_NET_TIMER(PedDelayDialogueTimer[iped])
			
			IF bPlayFromPos
				PLAY_AMBIENT_SPEECH_FROM_POSITION_NATIVE(sDialogue, GET_FORCED_COP_VOICE_TYPE_FROM_INT(1, iped), GET_ENTITY_COORDS(tempPed), sParams)
				PRINTLN("PLAY_AMBIENT_SPEECH_FROM_POSITION_NATIVE triggered with context: ",sDialogue)
			ELSE
				PLAY_PED_AMBIENT_SPEECH_AND_CLONE_NATIVE(tempPed,sDialogue,sParams)
				PRINTLN("PLAY_PED_AMBIENT_SPEECH_NATIVE triggered with context: ",sDialogue)
			ENDIF
		ENDIF
		SET_BIT(iPedSpeechTriggeredBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		PRINTLN("iDialogueLine: ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDialogueLine)
	ENDIF
		
ENDPROC

/// PURPOSE: This function will deal with peds that have the Cower Speech
///    option set on them. They will play pain audio at random intervals
///    given the NET_TIME()
PROC HANDLE_PED_COWER_SPEECH( PED_INDEX pedThis, INT iPedIndex )
	INT iNetworkTime = NATIVE_TO_INT( GET_NETWORK_TIME() )
	
	IF NOT IS_PED_INJURED( pedThis )
		IF iNetworkTime % ( iPedIndex * 10 ) = 0
			PLAY_PAIN( pedThis, AUD_DAMAGE_REASON_COWER, 0 )
		ENDIF
	ENDIF	
ENDPROC




PROC HANDLE_PED_SPEECH(PED_INDEX TempPed,INT iped)

FLOAT fdist
BOOL bPlayDialog,binCombat,bInRange,bDialogueConditionsMet, bDelayOver, bIgnoreRangeCheck
ENTITY_INDEX dummyEnt
PED_INDEX ClosestPlayerPed
	
	// Handle their cower speech if they 
	IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iped ].iPedBitsetTwo, ciPED_BSTwo_Cowering_Speech )
	AND IS_PED_FLEEING( TempPed )
		HANDLE_PED_COWER_SPEECH( TempPed, iPed )
	ENDIF

	IF NOT IS_BIT_SET(iPedSpeechAgroBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
	OR (HAS_NET_TIMER_STARTED(PedDelayDialogueTimer[iped]) OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDialogueDelay = -1)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDialogueTeam !=-1
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDialogueTeam = ciDIALOGUE_TRIGGER_TEAM_ANY 
				INT iDialogueTeam
				REPEAT FMMC_MAX_TEAMS iDialogueTeam
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDialogueObjective = MC_serverBD_4.iCurrentHighestPriority[iDialogueTeam]
						bDialogueConditionsMet = TRUE
						iDialogueTeam = FMMC_MAX_TEAMS
					ENDIF
				ENDREPEAT
			ELSE
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDialogueObjective = MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDialogueTeam]
					bDialogueConditionsMet = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset,  ciPED_BS_PedDialogueAnyTime)
		OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDialogueObjective = -2
			bDialogueConditionsMet = TRUE
		ENDIF
		
		IF DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_PED(iPed)
			bDialogueConditionsMet = FALSE
		ENDIF
		
		IF bDialogueConditionsMet
			INT iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY(tempPed)
			PLAYER_INDEX ClosestPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
			INT iplayerTeam = GET_PLAYER_TEAM(LocalPlayer)
			IF IS_NET_PLAYER_OK(ClosestPlayer)
				ClosestPlayerPed = GET_PLAYER_PED(ClosestPlayer)
			ENDIF
			IF NOT IS_PED_INJURED(DialoguePedToLookAt[iped])
				IF GET_DISTANCE_BETWEEN_ENTITIES(tempPed,DialoguePedToLookAt[iped]) >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDialogueRange
					DialoguePedToLookAt[iped] = NULL
				ELSE
					ClosestPlayerPed = DialoguePedToLookAt[iped]
				ENDIF
			ELSE
				DialoguePedToLookAt[iped] = NULL
			ENDIF
			
			IF NOT IS_PED_INJURED(ClosestPlayerPed)
				//If a) there is no LOS check set on this PED
				//or b) there is and the player is within the perception area:
				IF NOT (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset,ciPed_BS_LOSCheckForDialogue))
				OR ( IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset,ciPed_BS_LOSCheckForDialogue) AND (IS_TARGET_PED_IN_PERCEPTION_AREA(TempPed,ClosestPlayerPed) AND HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(iPed, TempPed,ClosestPlayerPed,SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)) )
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDialogueTiming = FMMC_DIALOGUE_AT_START 
						bPlayDialog = TRUE
						bInRange = TRUE
						fdist = GET_DISTANCE_BETWEEN_PEDS(ClosestPlayerPed,tempPed)
					ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDialogueTiming = FMMC_DIALOGUE_IN_RANGE
						bPlayDialog = TRUE
						fdist = GET_DISTANCE_BETWEEN_PEDS(ClosestPlayerPed,tempPed)
						IF fdist < g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDialogueRange
							bInRange = TRUE
						ENDIF
					ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDialogueTiming = FMMC_DIALOGUE_AT_MID
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset,  ciPED_BS_PedDialogueAnyTime)
							bPlayDialog = TRUE
							bInRange = TRUE
							fdist = GET_DISTANCE_BETWEEN_PEDS(ClosestPlayerPed,tempPed)
						ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDialogueTeam = ciDIALOGUE_TRIGGER_TEAM_ANY 
							INT iDialogueTeam
							REPEAT FMMC_MAX_TEAMS iDialogueTeam
								IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iDialogueTeam], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDialogueObjective)
									bPlayDialog = TRUE
									bInRange = TRUE
									fdist = GET_DISTANCE_BETWEEN_PEDS(ClosestPlayerPed,tempPed)
									iDialogueTeam = FMMC_MAX_TEAMS
								ENDIF
							ENDREPEAT
						ELSE
							IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDialogueTeam], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDialogueObjective)
								bPlayDialog = TRUE
								bInRange = TRUE
								fdist = GET_DISTANCE_BETWEEN_PEDS(ClosestPlayerPed,tempPed)
							ENDIF
						ENDIF
						
					ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDialogueTiming = FMMC_DIALOGUE_IN_ANGLED_AREA
						bPlayDialog = TRUE
						VECTOR vPos1 = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vDialogueAAPos1
						VECTOR vPos2 = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vDialogueAAPos2
						FLOAT fWidth = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fDialogueAAWidth
						IF IS_ENTITY_IN_ANGLED_AREA(ClosestPlayerPed,vPos1,vPos2,fWidth)
							bInRange = TRUE
							fdist = GET_DISTANCE_BETWEEN_PEDS(ClosestPlayerPed,tempPed)
						ENDIF
					ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDialogueTiming = FMMC_DIALOGUE_SPOOKED
						IF IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
						
							bInRange = TRUE	
							bPlayDialog = TRUE
							fdist = GET_DISTANCE_BETWEEN_PEDS(ClosestPlayerPed,tempPed)
							PRINTLN("[LM][Actor Dialogue] - FMMC_DIALOGUE_SPOOKED")
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_BIT_SET(iPedSpeechTriggeredBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDialogueDelay > 0
						IF HAS_NET_TIMER_STARTED(PedDelayDialogueTimer[iped])
							#IF IS_DEBUG_BUILD
								PRINTLN("[LM][Actor Dialogue] - Timer: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(PedDelayDialogueTimer[iped]), " out of: ", (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDialogueDelay * 1000))
							#ENDIF
							IF HAS_NET_TIMER_EXPIRED_READ_ONLY(PedDelayDialogueTimer[iped], (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDialogueDelay * 1000)) // Convert creator setting to miliseconds.
								PRINTLN("[LM][Actor Dialogue] - Delay is over.")
								bDelayOver = TRUE
								bPlayDialog = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF bPlayDialog
					IF NOT IS_BIT_SET(iPedSpeechTriggeredBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
						IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDialogueDelay > 0
							IF NOT HAS_NET_TIMER_STARTED(PedDelayDialogueTimer[iped])
								PRINTLN("[LM][Actor Dialogue] - Delay is starting now. We must wait: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDialogueDelay, " Seconds.")
								START_NET_TIMER(PedDelayDialogueTimer[iped])
							ENDIF
						ELSE
							bDelayOver = TRUE
						ENDIF
					ENDIF	
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPed_BSEight_DontRecheckRadius)
					AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDialogueDelay > 0
						bIgnoreRangeCheck = TRUE
					ELSE
						bIgnoreRangeCheck = FALSE
					ENDIF
					
					IF bInRange
						IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAggroTime != -1
							IF NOT HAS_NET_TIMER_STARTED(PedAgroTimer[iped])
								REINIT_NET_TIMER(PedAgroTimer[iped])
							ELSE
								IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAggroTime != -1
									IF HAS_NET_TIMER_STARTED(PedAgroTimer[iped])
										IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(PedAgroTimer[iped]) > (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAggroTime*1000)
										OR IS_BIT_SET(iPedAgroStageTwo[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
											PRINTLN("iPedAgroStageTwo has been triggered for ped ",iped)
											SET_BIT(iPedSpeechAgroBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
											BROADCAST_FMMC_PED_SPEECH_AGRO_BITSET(iped)
											
											IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iped ].iPedBitsetEight, ciPED_BSEight_CancelGoToWhenAggro )
												IF (MC_serverBD_2.iPedState[iped] = ciTASK_GOTO_COORDS)
												AND ( (MC_serverBD_2.iTargetID[iped] = ciINITIAL_GOTO) OR (MC_serverBD_2.iTargetID[iped] = ciSECONDARY_GOTO) )
													PRINTLN("Ped is doing a GOTO task, but they wanna get a bit aggro - ",iped)
													BROADCAST_FMMC_PED_SPEECH_AGGROED(iped)
												ELSE
													PRINTLN("Ped isnt doing a GOTO task, but they are aggro as fuck - ",iped)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						IF NOT IS_BIT_SET(iPedDialogueRangeActive[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
							
							IF NOT IS_BIT_SET(iPedAgroStageZero[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
								SET_BIT(iPedAgroStageZero[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
							ELIF (NOT IS_BIT_SET(iPedAgroStageOne[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed)) )
								SET_BIT(iPedAgroStageOne[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
							ELIF (NOT IS_BIT_SET(iPedAgroStageTwo[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed)) )
								SET_BIT(iPedAgroStageTwo[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
							ENDIF
							
							SET_BIT(iPedDialogueRangeActive[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
						ENDIF
					ELSE
						CLEAR_BIT(iPedDialogueRangeActive[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
						CLEAR_BIT(iPedDialogueRangeActive[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
						CLEAR_BIT(iPedSpeechAimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
						CLEAR_BIT(iPedInitSpeechBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
						CLEAR_BIT(iPedSpeechTriggeredBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
						IF HAS_NET_TIMER_STARTED(PedAgroTimer[iped])
							RESET_NET_TIMER(PedAgroTimer[iped])
						ENDIF
					ENDIF
					IF bInRange
					AND bDelayOver
						IF NOT IS_BIT_SET(iPedSpeechTriggeredBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
							IF SHOULD_PED_START_COMBAT(tempPed, iped)
							OR IS_PED_EVASIVE_DIVING(tempPed,dummyEnt)
							OR IS_PED_RAGDOLL(tempPed)
							OR IS_PED_GETTING_UP(tempPed)
								binCombat = TRUE
							ENDIF
							SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDialogueLine
								CASE FMMC_DIALOGUE_LINE_WARNING
									IF binCombat OR bInRange
										IF fdist < 15.0										
										OR bInRange
										AND NOT binCombat
											MANAGE_DIALOGUE_TURN_AND_TRIGGER(tempPed,ClosestPlayerPed,iped,"PROVOKE_TRESPASS",binCombat,iplayerTeam, "SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
										ELIF (fdist < 40.0 OR binCombat)
										OR bInRange
											MANAGE_DIALOGUE_TURN_AND_TRIGGER(tempPed,ClosestPlayerPed,iped,"SHOUT_THREATEN_PED",binCombat,iplayerTeam, "SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
										ENDIF
									ENDIF
								BREAK
								
								CASE FMMC_DIALOGUE_LINE_SPOTTED_UP	
									IF (fdist < 40.0 OR binCombat)
									OR bInRange
									OR bIgnoreRangeCheck
										MANAGE_DIALOGUE_TURN_AND_TRIGGER(tempPed,ClosestPlayerPed,iped,"MP_SUSPECT_SPOTTED_UP",binCombat,iplayerTeam, "SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
									ENDIF
								BREAK
								
								CASE FMMC_DIALOGUE_LINE_THANKS	
									IF (fdist < 40.0 OR binCombat)
									OR bInRange
									OR bIgnoreRangeCheck
										MANAGE_DIALOGUE_TURN_AND_TRIGGER(tempPed,ClosestPlayerPed,iped,"GENERIC_THANKS",binCombat,iplayerTeam, "SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
									ENDIF
								BREAK
								
								CASE FMMC_DIALOGUE_LINE_HI	
									IF (fdist < 40.0 OR binCombat)
									OR bInRange	
									OR bIgnoreRangeCheck
										IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn != IG_LESTERCREST //It's you! 2124220
											//Only do this once per mission
											IF NOT IS_BIT_SET(iDialogueHiBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
												MANAGE_DIALOGUE_TURN_AND_TRIGGER(tempPed,ClosestPlayerPed,iped,"GENERIC_HI",binCombat,iplayerTeam, "SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
												SET_BIT(iDialogueHiBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
											ENDIF
										ENDIF
									ENDIF
								BREAK
								
								CASE FMMC_DIALOGUE_LINE_FRIGHT_HIGH	
									IF (fdist < 40.0 OR binCombat)
									OR bInRange
									OR bIgnoreRangeCheck
										MANAGE_DIALOGUE_TURN_AND_TRIGGER(tempPed,ClosestPlayerPed,iped,"GENERIC_FRIGHTENED_HIGH",binCombat,iplayerTeam, "SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
									ENDIF
								BREAK
								
								CASE FMMC_DIALOGUE_LINE_SHOCK_HIGH	
									IF (fdist < 40.0 OR binCombat)
									OR bInRange
									OR bIgnoreRangeCheck
										MANAGE_DIALOGUE_TURN_AND_TRIGGER(tempPed,ClosestPlayerPed,iped,"GENERIC_SHOCKED_HIGH",binCombat,iplayerTeam, "SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
									ENDIF
								BREAK
								
								CASE FMMC_DIALOGUE_LINE_SURROUNDED	
									IF (fdist < 40.0 OR binCombat)
									OR bInRange
									OR bIgnoreRangeCheck
										MANAGE_DIALOGUE_TURN_AND_TRIGGER(tempPed,ClosestPlayerPed,iped,"SURROUNDED",binCombat,iplayerTeam, "SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
									ENDIF
								BREAK
								
								CASE FMMC_DIALOGUE_LINE_FALLBACK	
									IF (fdist < 40.0 OR binCombat)
									OR bInRange
									OR bIgnoreRangeCheck
										MANAGE_DIALOGUE_TURN_AND_TRIGGER(tempPed,ClosestPlayerPed,iped,"FALL_BACK",binCombat,iplayerTeam, "SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
									ENDIF
								BREAK
							
								CASE FMMC_DIALOGUE_LINE_STOP
									IF (fdist < 40.0 OR binCombat)
									OR bInRange
									OR bIgnoreRangeCheck
										MANAGE_DIALOGUE_TURN_AND_TRIGGER(tempPed,ClosestPlayerPed,iped,"PBF_STOP",binCombat,iplayerTeam, "SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
									ENDIF
								BREAK
								
								CASE FMMC_DIALOGUE_LINE_STOP_VEHICLE_BOAT_MEGAPHONE
									IF (fdist < 40.0 OR binCombat)
									OR bInRange
									OR bIgnoreRangeCheck
										MANAGE_DIALOGUE_TURN_AND_TRIGGER(tempPed,ClosestPlayerPed,iped,"STOP_VEHICLE_BOAT_MEGAPHONE",binCombat,iplayerTeam, "SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
									ENDIF
								BREAK
								
								CASE FMMC_DIALOGUE_LINE_STOP_VEHICLE_GENERIC_MEGAPHONE
									IF (fdist < 40.0 OR binCombat)
									OR bInRange
									OR bIgnoreRangeCheck
										MANAGE_DIALOGUE_TURN_AND_TRIGGER(tempPed,ClosestPlayerPed,iped,"STOP_VEHICLE_GENERIC_MEGAPHONE",binCombat,iplayerTeam, "SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
									ENDIF
								BREAK
								
								CASE FMMC_DIALOGUE_LINE_STOP_VEHICLE_GENERIC_WARNING_MEGAPHONE
									IF (fdist < 40.0 OR binCombat)
									OR bInRange
									OR bIgnoreRangeCheck
										MANAGE_DIALOGUE_TURN_AND_TRIGGER(tempPed,ClosestPlayerPed,iped,"STOP_VEHICLE_GENERIC_WARNING_MEGAPHONE",binCombat,iplayerTeam, "SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
									ENDIF
								BREAK
								
								CASE FMMC_DIALOGUE_LINE_SHOT_AT_HELI_MEGAPHONE
									IF (fdist < 40.0 OR binCombat)
									OR bInRange
									OR bIgnoreRangeCheck
										MANAGE_DIALOGUE_TURN_AND_TRIGGER(tempPed,ClosestPlayerPed,iped,"SHOT_AT_HELI_MEGAPHONE",binCombat,iplayerTeam, "SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
									ENDIF
								BREAK
								
								CASE FMMC_DIALOGUE_LINE_COP_ARRIVAL_ANNOUNCE_MEGAPHONE
									IF (fdist < 40.0 OR binCombat)
									OR bInRange
									OR bIgnoreRangeCheck
										MANAGE_DIALOGUE_TURN_AND_TRIGGER(tempPed,ClosestPlayerPed,iped,"COP_ARRIVAL_ANNOUNCE_MEGAPHONE",binCombat,iplayerTeam, "SPEECH_PARAMS_FORCE_MEGAPHONE", TRUE)
									ENDIF
								BREAK
								
								CASE FMMC_DIALOGUE_LINE_CLEAR_AREA_PANIC_MEGAPHONE
									IF (fdist < 40.0 OR binCombat)
									OR bInRange
									OR bIgnoreRangeCheck
										MANAGE_DIALOGUE_TURN_AND_TRIGGER(tempPed,ClosestPlayerPed,iped,"CLEAR_AREA_PANIC_MEGAPHONE",binCombat,iplayerTeam, "SPEECH_PARAMS_FORCE_MEGAPHONE", TRUE)
									ENDIF
								BREAK
								
								CASE FMMC_DIALOGUE_LINE_CLEAR_AREA_MEGAPHONE
									IF (fdist < 40.0 OR binCombat)
									OR bInRange
									OR bIgnoreRangeCheck
										MANAGE_DIALOGUE_TURN_AND_TRIGGER(tempPed,ClosestPlayerPed,iped,"CLEAR_AREA_MEGAPHONE",binCombat,iplayerTeam, "SPEECH_PARAMS_FORCE_MEGAPHONE", TRUE)
									ENDIF
								BREAK
								
								CASE FMMC_DIALOGUE_LINE_PROVOKE_TRESPASS_SECURITY
									IF (fdist < 40.0 OR binCombat)
									OR bInRange
									OR bIgnoreRangeCheck
										SET_AMBIENT_VOICE_NAME(tempPed, "S_M_M_GENERICSECURITY_01_WHITE_MINI_01")
										MANAGE_DIALOGUE_TURN_AND_TRIGGER(tempPed,ClosestPlayerPed,iped,"PROVOKE_TRESPASS",binCombat,iplayerTeam, "SPEECH_PARAMS_FORCE_MEGAPHONE", TRUE)
									ENDIF
								BREAK

							ENDSWITCH // End of SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDialogueLine
						ENDIF
					ENDIF // End of bInRange
				ENDIF // End of bPlayDialogue
			ENDIF
		ENDIF
	ENDIF // end of IF NOT IS_BIT_SET(iPedSpeechAgroBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))

ENDPROC



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: PED TASKING !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************



FUNC BOOL CHECK_FOR_DROP_OFF_FLEE(PED_INDEX tempPed, INT iped)

INT iteam

	IF MC_serverBD_2.iPartPedFollows[iped] = -1	
	AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_DontFleeDropOffs)
		FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
			IF IS_BIT_SET(MC_serverBD.iPedNearDropOff[iteam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				IF MC_serverBD_4.iCurrentHighestPriority[iteam] < FMMC_MAX_RULES
					IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_SMART_FLEE_POINT)
						IF NOT IS_VECTOR_ZERO(GET_DROP_OFF_CENTER_FOR_TEAM(FALSE, iteam, iPartToUse))
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,TRUE)
							SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_ALWAYS_FIGHT, FALSE)
							SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_ALWAYS_FLEE, TRUE)
							SET_PED_FLEE_ATTRIBUTES(tempPed, FA_DISABLE_COWER,TRUE)
							SET_PED_FLEE_ATTRIBUTES(tempPed, FA_COWER_INSTEAD_OF_FLEE,FALSE)
							SET_PED_FLEE_ATTRIBUTES(tempPed, FA_DISABLE_FLEE_FROM_INDIRECT_THREATS,FALSE)
							
							TASK_SMART_FLEE_COORD(tempPed,GET_DROP_OFF_CENTER_FOR_TEAM(FALSE, iteam, iPartToUse),5000,-1)
							
							PRINTLN("[RCC MISSION] TASK_FLEE (w CA_ALWAYS_FLEE) DROPOFF FOR PED: ",iped)
							RETURN TRUE
						ENDIF
					ELSE
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	RETURN FALSE

ENDFUNC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: VEHICLE TASKS (enter/leave, rappel, stop) !
//
//************************************************************************************************************************************************************



PROC TASK_PED_LEAVE_VEHICLE(PED_INDEX tempPed,  INT iped)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPed_BSEight_NeverLeaveVehicle)
		INT idelay

		IF IS_PED_IN_COMBAT( tempPed )
			IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED)
				SET_PED_COMBAT_ATTRIBUTES( tempPed, CA_USE_VEHICLE, false )
				TASK_COMBAT_HATED_TARGETS_AROUND_PED( tempPed, 299 )
				IF iped > -1
					PRINTLN("[RCC MISSION] TASK_PED_LEAVE_VEHICLE - calling TASK_COMBAT_HATED_TARGETS_AROUND_PED on ped ", iped)
					CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_HOVER_VEHICLE - callstack: ")
					DEBUG_PRINTCALLSTACK()
				ENDIF
			ENDIF
		ELSE
			IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_LEAVE_ANY_VEHICLE)
				idelay = GET_RANDOM_INT_IN_RANGE(0,1000)
				SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE, false)
				TASK_LEAVE_ANY_VEHICLE(tempPed,idelay)
				IF iped > -1
					PRINTLN("[RCC MISSION] TASK_PED_LEAVE_VEHICLE - calling TASK_LEAVE_ANY_VEHICLE with delay on ped ", iped)
					CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_LEAVE_VEHICLE - idelay = ", idelay)
					CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_LEAVE_VEHICLE - callstack: ")
					DEBUG_PRINTCALLSTACK()
				ENDIF
			ENDIF
		ENDIF
	ELSE
		PRINTLN("[RCC MISSION] TASK_PED_LEAVE_VEHICLE - ped ", iped ," is set to never ever leave veh")
	ENDIF

ENDPROC

PROC TASK_PED_HOVER_VEHICLE(PED_INDEX tempPed, VEHICLE_INDEX tempVeh, INT iped, VECTOR vCoords)
	
	FLOAT iCruiseSpeed = 1.0
	FLOAT fArriveDis = -1.0
	FLOAT fHeliOrientation = -1.0
	INT iFlightHeight = 10
	FLOAT fSlowDownDis = 5.0
	HELIMODE heliFlags = HF_NONE
	INT iMinFlightHeight = iFlightHeight
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour,ciPED_BSFour_IgnoreMinHeight)
		IF (MC_serverBD_2.iPedState[iped] = ciTASK_GOTO_COORDS)
		AND (MC_serverBD_2.iPedGotoProgress[iped] < MAX_ASSOCIATED_GOTO_TASKS)
			FLOAT fCustomArrivalHeight = GET_ASSOCIATED_GOTO_TASK_DATA__ARRIVAL_HEIGHT(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed])
			iMinFlightHeight = ROUND(fCustomArrivalHeight)
			iFlightHeight = 0
			heliFlags = HF_MaintainHeightAboveTerrain
			PRINTLN("[RCC MISSION] TASK_PED_HOVER_VEHICLE - Heli flight height for ped ",iped," from globals: ",iMinFlightHeight)
		ELSE
			PRINTLN("[RCC MISSION] TASK_PED_HOVER_VEHICLE - Heli flight height for ped ",iped," not found in globals")
			
			FLOAT fGroundZ
			IF GET_GROUND_Z_FOR_3D_COORD(Vcoords,fGroundZ)
				iMinFlightHeight = ROUND(Vcoords.z - fGroundZ)
				iFlightHeight = 0
				heliFlags = HF_MaintainHeightAboveTerrain
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(iClearedTasksForHover[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
		CLEAR_PED_TASKS(tempPed)
		
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,TRUE)		
		
		SET_BIT(iClearedTasksForHover[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed)) 
		TASK_HELI_MISSION(tempPed, tempVeh, NULL, NULL, vCoords, MISSION_GOTO, iCruiseSpeed, fArriveDis, fHeliOrientation, iFlightHeight, iMinFlightHeight, fSlowDownDis, heliFlags)

		PRINTLN("[RCC MISSION] TASK_PED_HOVER_VEHICLE - setting cleared task flag and calling TASK_HELI_MISSION on ped ", iped)
		CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_HOVER_VEHICLE - iCruiseSpeed = ", iCruiseSpeed)
		CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_HOVER_VEHICLE - fArriveDis = ", fArriveDis)
		CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_HOVER_VEHICLE - fHeliOrientation = ", fHeliOrientation)
		CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_HOVER_VEHICLE - iFlightHeight = ", iFlightHeight)
		CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_HOVER_VEHICLE - iMinFlightHeight = ", iMinFlightHeight)
		CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_HOVER_VEHICLE - fSlowDownDis = ", fSlowDownDis)
		CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_HOVER_VEHICLE - callstack: ")
		DEBUG_PRINTCALLSTACK()
	ELSE
		IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_VEHICLE_MISSION, FALSE, MISSION_GOTO, tempVeh)
			TASK_HELI_MISSION(tempPed, tempVeh, NULL, NULL, vCoords, MISSION_GOTO, iCruiseSpeed, fArriveDis, fHeliOrientation, iFlightHeight, iMinFlightHeight, fSlowDownDis, heliFlags)

			PRINTLN("[RCC MISSION] TASK_PED_HOVER_VEHICLE - Calling TASK_HELI_MISSION on ped ", iped)
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_HOVER_VEHICLE - iCruiseSpeed = ", iCruiseSpeed)
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_HOVER_VEHICLE - fArriveDis = ", fArriveDis)
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_HOVER_VEHICLE - fHeliOrientation = ", fHeliOrientation)
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_HOVER_VEHICLE - iFlightHeight = ", iFlightHeight)
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_HOVER_VEHICLE - iMinFlightHeight = ", iMinFlightHeight)
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_HOVER_VEHICLE - fSlowDownDis = ", fSlowDownDis)
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_HOVER_VEHICLE - callstack: ")
			DEBUG_PRINTCALLSTACK()
		ENDIF
	ENDIF
	
ENDPROC

PROC TASK_PED_LAND_HELI(PED_INDEX tempPed, VEHICLE_INDEX tempVeh, INT iped, VECTOR vCoords)
	
	LOAD_COLLISION_ON_PED_IF_POSSIBLE(tempPed,iped)
	
	IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_VEHICLE_MISSION, FALSE, MISSION_LAND, tempVeh)
		
		FLOAT fCruiseSpeed = 5.0
		FLOAT fTargetReachedDist = 5.0
		FLOAT fHeliOrientation = -1.0
		INT iFlightHeight = 0
		INT iMinFlightHeight = 0
		FLOAT fSlowDownDistance = -1.0
		HELIMODE heliFlags = HF_LandOnArrival | HF_IgnoreHiddenEntitiesDuringLand
		
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, TRUE)
		
		TASK_HELI_MISSION(tempPed, tempVeh, NULL, NULL, vCoords, MISSION_LAND, fCruiseSpeed, fTargetReachedDist, fHeliOrientation, iFlightHeight, iMinFlightHeight, fSlowDownDistance, heliFlags)
		
		PRINTLN("[RCC MISSION] TASK_PED_LAND_HELI - Calling TASK_HELI_MISSION on ped ", iped)
		CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_LAND_HELI - vCoords = ", vCoords)
		CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_LAND_HELI - fCruiseSpeed = ", fCruiseSpeed)
		CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_LAND_HELI - fTargetReachedDist = ", fTargetReachedDist)
		CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_LAND_HELI - fHeliOrientation = ", fHeliOrientation)
		CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_LAND_HELI - iFlightHeight = ", iFlightHeight)
		CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_LAND_HELI - iMinFlightHeight = ", iMinFlightHeight)
		CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_LAND_HELI - fSlowDownDistance = ", fSlowDownDistance)
		CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_LAND_HELI - callstack: ")
		DEBUG_PRINTCALLSTACK()
		
	ELSE
		
		SET_SHORT_SLOWDOWN_FOR_LANDING(tempVeh)
		CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_LAND_HELI - SET_SHORT_SLOWDOWN_FOR_LANDING Ped: ", iped)	
		
	ENDIF
	
ENDPROC

PROC TASK_PED_RAPPEL_FROM_VEHICLE(PED_INDEX tempPed, INT iped)
	
	IF (iped = iped)
		iped = iped
	ENDIF
	
	IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_RAPPEL_FROM_HELI)
		TASK_RAPPEL_FROM_HELI(tempPed)
		
		PRINTLN("[RCC MISSION] TASK_PED_RAPPEL_FROM_VEHICLE - Calling TASK_RAPPEL_FROM_HELI on ped ", iped)
		CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_RAPPEL_FROM_VEHICLE - callstack: ")
		DEBUG_PRINTCALLSTACK()
	ENDIF
	
ENDPROC
						
FUNC BOOL IS_ANY_PED_ENTERING_VEHICLE(VEHICLE_INDEX tempveh)

INT iped
PED_INDEX tempPed

	IF GET_ENTITY_SPEED(tempveh) < 5.0
		FOR iped = 0 TO (MC_serverBD.iNumPedCreated - 1)
			IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
				tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
				IF GET_VEHICLE_PED_IS_ENTERING(tempPed) = tempveh
					PRINTLN("[RCC MISSION] - [IS_ANY_PED_ENTERING_VEHICLE] - ped ", iPed, " is trying to enter vehicle.")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDFOR
	ENDIF

	RETURN FALSE
	
ENDFUNC

PROC TASK_PED_ENTER_VEH(PED_INDEX tempPed, INT iped,VEHICLE_INDEX tempveh,INT iDefaultWarpTime =10000)

	FLOAT fOnFootSpeed
	VEHICLE_SEAT vsSeatToUse
	INT iWarpTime
	
	vsSeatToUse = GET_VEHICLE_SEAT_PED_SHOULD_ENTER(tempveh, iped #IF IS_DEBUG_BUILD , TRUE #ENDIF )
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iWarpTimer != -1
		iWarpTime = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iWarpTimer
	ELSE
		iWarpTime = iDefaultWarpTime
	ENDIF
	CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_ENTER_VEH - Ped ", iped, " warp timer g_FMMC_STRUCT_ENTITIES.sPlacedPed[",iped,"].iWarpTimer is ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iWarpTimer, ". Task enter vehicle warp time is now: ",iWarptime)
	
	IF vsSeatToUse != VS_ANY_PASSENGER // The vehicle has some free seat somewhere:
		IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_ENTER_VEHICLE)
		OR targetseat[iped] != vsSeatToUse
			
			IF targetseat[iped] != vsSeatToUse
				IF CAN_PED_TASKS_BE_CLEARED(tempPed)
					//CLEAR_PED_TASKS(tempPed)
					SET_PED_RETASK_DIRTY_FLAG(iPed)
				ENDIF
				targetseat[iped] = vsSeatToUse
			ENDIF
			
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,TRUE)
			
			SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE, TRUE)
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_AssActionWalk)
				fOnFootSpeed = PEDMOVEBLENDRATIO_WALK
			ELSE
				fOnFootSpeed = PEDMOVEBLENDRATIO_RUN
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetThree,ciPED_BSThree_SprintTaskOnAgro)
			AND IS_BIT_SET(MC_serverBD.iPedAggroedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				fOnFootSpeed = PEDMOVEBLENDRATIO_SPRINT
			ENDIF
			
			IF IS_VEHICLE_SEAT_FREE(tempVeh, targetseat[iped])
				TASK_ENTER_VEHICLE(tempPed,tempVeh,iwarpTime,targetseat[iped],fOnFootSpeed)		//enter vehicle seat normally
				CLEAR_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				
				PRINTLN("[RCC MISSION] TASK_PED_ENTER_VEH - Calling TASK_ENTER_VEHICLE on ped ", iped)
				CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_ENTER_VEH - seat = ", ENUM_TO_INT(targetseat[iped]))
				CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_ENTER_VEH - fOnFootSpeed = ", fOnFootSpeed)
				CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_ENTER_VEH - iwarpTime = ", iwarpTime)
				CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_ENTER_VEH - callstack: ")
				DEBUG_PRINTCALLSTACK()
			ELSE
				PED_INDEX tempPedInSeat = GET_PED_IN_VEHICLE_SEAT(tempVeh, targetseat[iped])	//drag out dead ped from seat
				IF IS_PED_INJURED(tempPedInSeat)
					TASK_ENTER_VEHICLE(tempPed,tempVeh,iwarpTime,targetseat[iped],fOnFootSpeed, ECF_JACK_ANYONE)	
					CLEAR_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					
					PRINTLN("[RCC MISSION] TASK_PED_ENTER_VEH - Calling TASK_ENTER_VEHICLE with ECF_JACK_ANYONE on ped ", iped)
					CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_ENTER_VEH - seat = ", ENUM_TO_INT(targetseat[iped]))
					CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_ENTER_VEH - fOnFootSpeed = ", fOnFootSpeed)
					CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_ENTER_VEH - iwarpTime = ", iwarpTime)
					CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_ENTER_VEH - callstack: ")
					DEBUG_PRINTCALLSTACK()
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
		
ENDPROC

PROC TASK_PED_STOP_VEHICLE(PED_INDEX tempPed, INT iPed, VEHICLE_INDEX tempVeh,VECTOR vParkCoords)
	
	IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iPed, SCRIPT_TASK_VEHICLE_MISSION, FALSE, MISSION_STOP, tempVeh)
		TASK_VEHICLE_MISSION_COORS_TARGET(tempPed,tempVeh,vParkCoords,MISSION_STOP,5.0,DRIVINGMODE_AVOIDCARS,-1,1)
		CLEAR_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
		
		PRINTLN("[RCC MISSION] TASK_PED_STOP_VEHICLE - calling TASK_VEHICLE_MISSION_COORS_TARGET")
		CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_STOP_VEHICLE - vParkCoords = ", vParkCoords)
		CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_STOP_VEHICLE - callstack: ")
		DEBUG_PRINTCALLSTACK()
	ENDIF

ENDPROC

PROC TASK_PED_LAND_AIRCRAFT(PED_INDEX tempPed, INT iped, VEHICLE_INDEX tempVeh, VECTOR vLandCoords, BOOL bPlane)

	IF NOT bPlane
		IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_VEHICLE_MISSION, FALSE, MISSION_LAND, tempVeh)
			TASK_VEHICLE_MISSION_COORS_TARGET(tempPed,tempVeh,vLandCoords,MISSION_LAND,20.0,DRIVINGMODE_AVOIDCARS_RECKLESS,-1,-1)
			SET_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))

			PRINTLN("[RCC MISSION] TASK_PED_LAND_AIRCRAFT - calling TASK_VEHICLE_MISSION_COORS_TARGET")
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_LAND_AIRCRAFT - vParkCoords = ", vLandCoords)
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_LAND_AIRCRAFT - callstack: ")
			DEBUG_PRINTCALLSTACK()
		ENDIF
	ELSE
		IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLANE_LAND)
			TASK_PLANE_LAND(tempPed, tempVeh, vLandCoords, vLandCoords + <<100, 0, 0>>)
			SET_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			
			PRINTLN("[RCC MISSION] TASK_PED_LAND_AIRCRAFT - calling TASK_PLANE_LAND")
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PLANE_LAND - vParkCoords = ", vLandCoords)
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PLANE_LAND - callstack: ")
			DEBUG_PRINTCALLSTACK()
		ENDIF
	ENDIF
	
ENDPROC

PROC TASK_WAIT_FOR_PASSENGERS(PED_INDEX tempPed, VEHICLE_INDEX tempveh, INT iped)
	
	IF NOT SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_VEHICLE_TEMP_ACTION)
		EXIT
	ENDIF
	
	PRINTLN("[RCC MISSION] TASK_WAIT_FOR_PASSENGERS - Called for ped ",iped,", callstack:")
	DEBUG_PRINTCALLSTACK()
	
	IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(tempPed)
		CASSERTLN(DEBUG_CONTROLLER, "[RCC MISSION] TASK_WAIT_FOR_PASSENGERS - Don't have control of ped ",iped)
		PRINTLN("[RCC MISSION] TASK_WAIT_FOR_PASSENGERS - Don't have control of ped ",iped)
		
		EXIT
	ENDIF
	
	IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
		PRINTLN("[RCC MISSION] TASK_WAIT_FOR_PASSENGERS - Requesting control of vehicle for tasking of ped ",iped)
		NETWORK_REQUEST_CONTROL_OF_ENTITY(tempVeh)
		
		EXIT
	ENDIF
	
	PRINTLN("[RCC MISSION] TASK_WAIT_FOR_PASSENGERS - Calling TASK_VEHICLE_TEMP_ACTION w TEMPACT_BRAKE on ped ",iped)
	SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE, TRUE)
	TASK_VEHICLE_TEMP_ACTION(tempPed, tempveh, TEMPACT_BRAKE, 2500)
	CLEAR_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
	
	REINIT_NET_TIMER(tdparkTimer[iped])
	
ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: COMBAT TASKS !
//
//************************************************************************************************************************************************************



PROC TASK_PED_VEHICLE_COMBAT_COORDS(PED_INDEX tempPed, INT iped, VEHICLE_INDEX tempVeh,VECTOR Vcoords,BOOL bPlane = FALSE,BOOL Heli = FALSE,BOOL bBoat = FALSE)
	
	IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_VEHICLE_MISSION, FALSE, MISSION_ATTACK, tempVeh)
		
		SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE, TRUE)
		IF IS_COMBAT_VEHICLE(tempveh)
		AND NOT	SHOULD_PED_BE_BLOCKED_FROM_USING_VEH_WEAPON(tempPed, iPed)
			SET_PED_ACCURACY(tempPed,3)
			SET_PED_SHOOT_RATE(tempPed,100)
			SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE_ATTACK,TRUE)
			SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE_ATTACK_IF_VEHICLE_HAS_MOUNTED_GUNS,TRUE)
		ELSE
			SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE_ATTACK,FALSE)
			SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE_ATTACK_IF_VEHICLE_HAS_MOUNTED_GUNS,FALSE)
		ENDIF
		IF bboat
			INIT_BOAT(tempveh,-1,TRUE)
		ELIF bPlane OR Heli
			REMOVE_PED_DEFENSIVE_AREA(tempPed)
			INIT_PLANE(tempveh,-1,TRUE)
		ENDIF
		
		SET_PED_CONFIG_FLAG(tempPed, PCF_DontHitVehicleWithProjectiles, TRUE)
		
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,TRUE)
		
		TASK_VEHICLE_MISSION_COORS_TARGET(tempPed,tempVeh,Vcoords,MISSION_ATTACK,30,DRIVINGMODE_PLOUGHTHROUGH,-1,-1)
		SET_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
		
		IF iped > -1
			PRINTLN("[RCC MISSION] TASK_PED_VEHICLE_COMBAT_COORDS - Calling TASK_VEHICLE_MISSION_COORS_TARGET on ped ", iped)
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_VEHICLE_COMBAT_COORDS - bPlane = ", bPlane)
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_VEHICLE_COMBAT_COORDS - Heli = ", Heli)
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_VEHICLE_COMBAT_COORDS - bBoat = ", bBoat)
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_VEHICLE_COMBAT_COORDS - callstack: ")
			DEBUG_PRINTCALLSTACK()
		ENDIF
	ENDIF

ENDPROC

PROC APPLY_FIXATION_SETTINGS_PED(PED_INDEX tempPed, BOOL bApply, INT iPed)
	PRINTLN("[RCC MISSION] APPLY_FIXATION_SETTINGS_PED - iPed: ", iPed, " bApply: ", bApply)
	
	SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_DISABLE_SECONDARY_TARGET, bApply)
	SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_ALWAYS_FIGHT, bApply)
	SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_AGGRESSIVE, bApply)
	SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_CAN_FIGHT_ARMED_PEDS_WHEN_NOT_ARMED, bApply)	
	SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_CAN_CHASE_TARGET_ON_FOOT, bApply)		
	SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_CAN_USE_FRUSTRATED_ADVANCE, bApply) 
	
	SET_PED_FLEE_ATTRIBUTES(tempPed, FA_DISABLE_COWER, bApply)
	SET_PED_FLEE_ATTRIBUTES(tempPed, FA_DISABLE_FLEE_FROM_INDIRECT_THREATS, bApply)	
	SET_PED_FLEE_ATTRIBUTES(tempPed, FA_NEVER_FLEE, bApply)	
	
	SET_PED_COMBAT_RANGE(tempPed, CR_NEAR)
	SET_PED_COMBAT_MOVEMENT(tempPed, CM_WILLADVANCE) 	
	
	SET_PED_CONFIG_FLAG(tempPed, PCF_TreatNonFriendlyAsHateWhenInCombat, TRUE)
	SET_PED_CONFIG_FLAG(tempPed, PCF_TreatDislikeAsHateWhenInCombat, TRUE)
	SET_PED_CONFIG_FLAG(tempPed, PCF_ForceIgnoreMaxMeleeActiveSupportCombatants, TRUE)
	SET_PED_CONFIG_FLAG(tempPed, PCF_ForceIgnoreMeleeActiveCombatant, FALSE)
	
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, TRUE)
	
	IF NOT bApply
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwelve, ciPed_BSTwelve_HatePlayersOnFixationIntervention)
			INT iCop, i
			INT iteamrel[FMMC_MAX_TEAMS]
			IF IS_PED_A_SCRIPTED_COP(iPed)
			OR IS_PED_A_COP_MC(iPed)
				icop = FM_RelationshipLikeCops
			ELSE
				icop = FM_RelationshipNothingCops
			ENDIF
			FOR i = 0 TO (FMMC_MAX_TEAMS-1)
				iteamrel[i] = FM_RelationshipHate
				SET_PED_CAN_BE_TARGETTED_BY_TEAM(tempPed, i, TRUE)
				SET_ENTITY_IS_TARGET_PRIORITY(tempPed, TRUE)
			ENDFOR
			SET_PED_RELATIONSHIP_GROUP_HASH(tempPed, rgFM_AiPed[iteamrel[0]][iteamrel[1]][iteamrel[2]][iteamrel[3]][icop])	//Use FM_RelationshipNothingCops or FM_RelationshipLikeCops
			SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_ALWAYS_FIGHT, TRUE)
			SET_PED_FLEE_ATTRIBUTES(tempPed, FA_DISABLE_COWER, TRUE)
			SET_PED_FLEE_ATTRIBUTES(tempPed, FA_NEVER_FLEE, TRUE)
			
			PRINTLN("[RCC MISSION] APPLY_FIXATION_SETTINGS_PED - iPed: ", iPed, " Changing REL GROUP settings to HATE PLAYER.")
		ENDIF
		SET_BIT(iBSTaskUsingIgnoreCombatChecks[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
	ENDIF
ENDPROC

PROC TASK_PED_COMBAT_PED(PED_INDEX tempPed, INT iped,PED_INDEX temptargetped,BOOL Heli = FALSE,BOOL bPlane = FALSE,BOOL bBoat = FALSE,BOOL bDriver = FALSE, VEHICLE_INDEX tempveh = NULL)
	
	IF NOT IS_PED_IN_COMBAT(tempPed,temptargetped)
		IF bDriver
			SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE, TRUE)
			IF IS_COMBAT_VEHICLE(tempveh)
			AND NOT SHOULD_PED_BE_BLOCKED_FROM_USING_VEH_WEAPON(tempPed, iPed)
				SET_PED_ACCURACY(tempPed,3)
				SET_PED_SHOOT_RATE(tempPed,50)
				SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE_ATTACK,TRUE)
				SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE_ATTACK_IF_VEHICLE_HAS_MOUNTED_GUNS,TRUE)
			ELSE
				SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE_ATTACK,FALSE)
				SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE_ATTACK_IF_VEHICLE_HAS_MOUNTED_GUNS,FALSE)
			ENDIF
			IF bboat
				INIT_BOAT(tempveh,-1,TRUE)
			ELIF bPlane OR Heli
				IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEleven, ciPED_BSEleven_CanHaveDefensiveAreaInVehicle)
					REMOVE_PED_DEFENSIVE_AREA(tempPed)
				ENDIF
				INIT_PLANE(tempveh,-1,TRUE)
			ENDIF
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fCombatMinimumDist > 0.0
				PRINTLN("[RCC MISSION] TASK_PED_COMBAT_PED - Setting min veh distance for ",iped," to ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fCombatMinimumDist)
				SET_ARRIVE_DISTANCE_OVERRIDE_FOR_VEHICLE_PERSUIT_ATTACK(tempveh, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fCombatMinimumDist)
			ENDIF
		ENDIF
		
		SET_PED_CONFIG_FLAG(tempPed, PCF_DontHitVehicleWithProjectiles, TRUE)
		
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,TRUE)

		TASK_COMBAT_PED_FLAGS tcpflags = COMBAT_PED_NONE
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedFixationTarget > -1
		AND NOT IS_BIT_SET(MC_ServerBD_1.iPedFixationTaskEnded[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			IF IS_RULE_VALID_FOR_PED_FIXATION_STATE(iPed)			
				CLEAR_PED_TASKS(tempPed)
				APPLY_FIXATION_SETTINGS_PED(tempPed, TRUE, iPed)
				tcpflags = COMBAT_PED_PREVENT_CHANGING_TARGET
				PRINTLN("[RCC MISSION] TASK_PED_COMBAT_PED - Calling TASK_COMBAT_PED on ped ", iped,", tcpflags = COMBAT_PED_PREVENT_CHANGING_TARGET")
			ENDIF
		ENDIF
		
		IF GET_RELATIONSHIP_BETWEEN_PEDS(tempPed, temptargetped) != ACQUAINTANCE_TYPE_PED_LIKE
		AND GET_RELATIONSHIP_BETWEEN_PEDS(tempPed, temptargetped) != ACQUAINTANCE_TYPE_PED_RESPECT
			TASK_COMBAT_PED(tempPed, temptargetped, tcpflags)
			CLEAR_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
		ENDIF
		
		IF iped > -1
			#IF IS_DEBUG_BUILD
			VECTOR vTarget = GET_ENTITY_COORDS(temptargetped)
			PRINTLN("[RCC MISSION] TASK_PED_COMBAT_PED - Calling TASK_COMBAT_PED on ped ", iped,", target coords ",vTarget)
			
			IF IS_PED_A_PLAYER(temptargetped)
				PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(temptargetped)
				IF IS_NET_PLAYER_OK(tempPlayer)
				AND NETWORK_IS_PLAYER_A_PARTICIPANT(tempPlayer)
					PRINTLN("[RCC MISSION] TASK_PED_COMBAT_PED - target ped for ped ",iped," is player ",NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(tempPlayer)))
				ENDIF
			ENDIF
			
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_COMBAT_PED - bPlane = ", bPlane)
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_COMBAT_PED - Heli = ", Heli)
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_COMBAT_PED - bBoat = ", bBoat)
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_COMBAT_PED - bDriver = ", bDriver)
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_COMBAT_PED - callstack: ")
			DEBUG_PRINTCALLSTACK()
			#ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

PROC TASK_PED_VEHICLE_COMBAT_VEH(PED_INDEX tempPed, INT iped, VEHICLE_INDEX tempVeh,VEHICLE_INDEX targetVeh,BOOL Heli = FALSE,BOOL bPlane = FALSE,BOOL bBoat = FALSE)
	
	IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_VEHICLE_MISSION, FALSE, MISSION_ATTACK, tempVeh)
		
		SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE, TRUE)
		IF IS_COMBAT_VEHICLE(tempveh)
		AND NOT SHOULD_PED_BE_BLOCKED_FROM_USING_VEH_WEAPON(tempPed, iPed)
			SET_PED_ACCURACY(tempPed,3)
			SET_PED_SHOOT_RATE(tempPed,50)
			SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE_ATTACK,TRUE)
			SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE_ATTACK_IF_VEHICLE_HAS_MOUNTED_GUNS,TRUE)
		ELSE
			SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE_ATTACK,FALSE)
			SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE_ATTACK_IF_VEHICLE_HAS_MOUNTED_GUNS,FALSE)
		ENDIF
		IF bboat
			INIT_BOAT(tempveh,-1,TRUE)
		ELIF bPlane OR Heli
			REMOVE_PED_DEFENSIVE_AREA(tempPed)
			INIT_PLANE(tempveh,-1,TRUE)
		ENDIF
		
		SET_PED_CONFIG_FLAG(tempPed, PCF_DontHitVehicleWithProjectiles, TRUE)
		
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,TRUE)
		
		TASK_VEHICLE_MISSION(tempPed,tempVeh,targetVeh,MISSION_ATTACK,30,DRIVINGMODE_PLOUGHTHROUGH,-1,-1)
		CLEAR_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
		IF iped > -1
			PRINTLN("[RCC MISSION] TASK_PED_VEHICLE_COMBAT_VEH - Calling TASK_VEHICLE_MISSION on ped ", iped)
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_VEHICLE_COMBAT_VEH - bPlane = ", bPlane)
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_VEHICLE_COMBAT_VEH - Heli = ", Heli)
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_VEHICLE_COMBAT_VEH - bBoat = ", bBoat)
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_VEHICLE_COMBAT_VEH - callstack: ")
			DEBUG_PRINTCALLSTACK()
		ENDIF
	ENDIF

ENDPROC

PROC TASK_PED_COMBAT_ENTITY(PED_INDEX tempPed, INT iped,ENTITY_INDEX tempEnt,BOOL Heli = FALSE,BOOL bPlane = FALSE,BOOL bBoat = FALSE,BOOL bDriver = FALSE,VEHICLE_INDEX tempVeh = NULL)

	IF IS_ENTITY_A_PED(tempEnt)
		PED_INDEX temptargetPed = GET_PED_INDEX_FROM_ENTITY_INDEX(tempEnt)
		TASK_PED_COMBAT_PED(tempPed,iped,temptargetPed,Heli,bPlane,bBoat,bDriver,tempVeh)
	ELSE
		
		IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_SHOOT_AT_ENTITY)
			SET_CURRENT_PED_WEAPON(tempPed,GET_BEST_PED_WEAPON(tempPed),TRUE)
			
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,TRUE)
			
			SET_PED_CONFIG_FLAG(tempPed, PCF_DontHitVehicleWithProjectiles, TRUE)
			
			SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_REQUIRES_LOS_TO_SHOOT,TRUE)
			SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_CAN_SHOOT_WITHOUT_LOS,FALSE)
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].gun = WEAPONTYPE_MINIGUN
				TASK_SHOOT_AT_ENTITY(tempPed,tempEnt,-1,FIRING_TYPE_CONTINUOUS)
			ELSE
				TASK_SHOOT_AT_ENTITY(tempPed,tempEnt,-1,FIRING_TYPE_RANDOM_BURSTS)
			ENDIF
			CLEAR_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			
			PRINTLN("[RCC MISSION] TASK_PED_COMBAT_ENTITY - Calling TASK_SHOOT_AT_ENTITY on ped ", iped)
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_COMBAT_ENTITY - bPlane = ", bPlane)
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_COMBAT_ENTITY - Heli = ", Heli)
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_COMBAT_ENTITY - bBoat = ", bBoat)
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_COMBAT_ENTITY - bDriver = ", bDriver)
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_COMBAT_ENTITY - callstack: ")
			DEBUG_PRINTCALLSTACK()
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL IS_PED_UNFRIENDLY_TO_ANY_TEAM(INT iped)
	
	BOOL bUnfriendly
	
	INT iTeam
	
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iTeam[iTeam] != ciPED_RELATION_SHIP_LIKE
			bUnfriendly = TRUE
			iTeam = MC_serverBD.iNumberOfTeams // Break out!
		ENDIF
	ENDFOR
	
	RETURN bUnfriendly
	
ENDFUNC

PROC TASK_PED_GENERAL_COMBAT(PED_INDEX tempPed,INT iped,BOOL bHeli = FALSE,BOOL bPlane = FALSE,BOOL bBoat = FALSE,BOOL bDriver = FALSE, VEHICLE_INDEX tempveh = NULL)
		
	#IF IS_DEBUG_BUILD
		IF bLMhelpfulScriptAIPrints
			PRINTLN("[LM][RCC MISSION] - TASK_PED_GENERAL_COMBAT - iPed: ", iPed, " bDriver: ", bDriver, " SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(tempPed))
		ENDIF
	#ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].ipedBitsetEight, ciPed_BSEight_ForceTaskMissionBoat)
		IF IS_BIT_SET( iPedHitByStunGun[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed) )
			EXIT
		ENDIF
		
		IF NETWORK_HAS_CONTROL_OF_ENTITY(tempPed)	
			IF IS_PED_RUNNING_MOBILE_PHONE_TASK(tempPed)
				CLEAR_PED_SECONDARY_TASK(tempPed)
			ENDIF
		ENDIF
		
		IF GET_PED_HAS_FREE_MOVEMENT_IN_COMBAT(iped)
			REMOVE_PED_DEFENSIVE_AREA(tempPed)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_ExtendedLOS)
			SET_PED_SEEING_RANGE(tempPed, 150)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_ExtendLOSOnCombat)
			SET_PED_SEEING_RANGE(tempPed, 150)
			SET_PED_COMBAT_RANGE(tempPed, CR_FAR)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_UseStrictLOSAimChecks)
			SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_DEFAULT_BLOCKED_LOS_POSITION_AND_DIRECTION, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_REQUIRES_LOS_TO_AIM, TRUE)
			PRINTLN("[RCC MISSION] - TASK_PED_GENERAL_COMBAT - Setting CA_USE_DEFAULT_BLOCKED_LOS_POSITION_AND_DIRECTION and CA_REQUIRES_LOS_TO_AIM for ped ", iped)
		ELSE
			SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_REQUIRES_LOS_TO_AIM, FALSE)
		ENDIF
		
		SET_PED_CONFIG_FLAG(tempPed, PCF_DontHitVehicleWithProjectiles, TRUE)
		
		IF bDriver
			SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE, TRUE)
			
			IF IS_COMBAT_VEHICLE(tempveh)
			AND NOT SHOULD_PED_BE_BLOCKED_FROM_USING_VEH_WEAPON(tempPed, iPed)
			
				IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEleven, ciPed_BSEleven_CombatVehicleCombatTaskDontChangeShootingSkill)
					SET_PED_ACCURACY(tempPed,3)
					SET_PED_SHOOT_RATE(tempPed,50)
				ENDIF
				SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE_ATTACK,TRUE)
				
				//SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE_ATTACK_IF_VEHICLE_HAS_MOUNTED_GUNS, TRUE)
				//SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_BLOCK_FIRE_FOR_VEHICLE_PASSENGER_MOUNTED_GUNS, FALSE)
			ELSE
				SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE_ATTACK,FALSE)
				//SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE_ATTACK_IF_VEHICLE_HAS_MOUNTED_GUNS, FALSE)
				//SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_BLOCK_FIRE_FOR_VEHICLE_PASSENGER_MOUNTED_GUNS, TRUE)
			ENDIF
			
			IF bboat
				INIT_BOAT(tempveh,-1,TRUE)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(tempveh)
					SET_BOAT_IGNORE_LAND_PROBES(tempveh,TRUE)
				ENDIF
			ELIF bPlane OR bHeli
				IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEleven, ciPED_BSEleven_CanHaveDefensiveAreaInVehicle)
					REMOVE_PED_DEFENSIVE_AREA(tempPed)
				ENDIF
				INIT_PLANE(tempveh,-1,TRUE)
			ENDIF
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fCombatMinimumDist > 0.0
				PRINTLN("[RCC MISSION] TASK_PED_GENERAL_COMBAT - Setting min veh distance for ",iped," to ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fCombatMinimumDist)
				SET_ARRIVE_DISTANCE_OVERRIDE_FOR_VEHICLE_PERSUIT_ATTACK(tempveh, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fCombatMinimumDist)
			ENDIF
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(tempVeh)
			IF IS_PED_IN_ANY_VEHICLE(tempPed)
				tempVeh = GET_VEHICLE_PED_IS_USING(tempPed)
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(tempVeh)
			IF GET_ENTITY_MODEL(tempVeh) = TRAILERLARGE
				SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_FORCE_CHECK_ATTACK_ANGLE_FOR_MOUNTED_GUNS, TRUE)
				PRINTLN("[JS][RCC MISSION] - TASK_PED_GENERAL_COMBAT - Setting CA_FORCE_CHECK_ATTACK_ANGLE_FOR_MOUNTED_GUNS for ped ", iped)
			ENDIF
		ENDIF
		
		//Lukasz: fix for B*2175558 make peds set with ciPED_BSFive_DontFleeInvehicle to not use vehicles when fleeing/in combat
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_DontFleeInvehicle)
			SET_PED_FLEE_ATTRIBUTES(tempPed, FA_USE_VEHICLE, FALSE)
			SET_PED_FLEE_ATTRIBUTES(tempPed, FA_FORCE_EXIT_VEHICLE, TRUE)
			
			SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_LEAVE_VEHICLES, TRUE)
			PRINTLN("[RCC MISSION] - TASK_PED_GENERAL_COMBAT - Setting FA_USE_VEHICLE and CA_USE_VEHICLE to FALSE for ped ", iped)
		ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetSeven, ciPED_BSSeven_ExitVehOnCombat)
			SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_LEAVE_VEHICLES, TRUE)
			PRINTLN("[RCC MISSION] - TASK_PED_GENERAL_COMBAT - Setting CA_USE_VEHICLE to FALSE for ped ", iped)
		ENDIF
		
		IF GET_PED_COMBAT_STYLE(iped) = ciPED_BERSERK
			IF NOT IS_BIT_SET( iPedHasCharged[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed) )
				SET_PED_CONFIG_FLAG(tempPed,PCF_ShouldChargeNow,TRUE)
				SET_BIT( iPedHasCharged[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed) )
			ENDIF
		ENDIF
		
		IF (NOT IS_PED_A_HIGH_PRIORITY_PED(iped))
		OR IS_PED_UNFRIENDLY_TO_ANY_TEAM(iped) // They don't like players, so they can combat them here - otherwise this'll be called forever, and never do anything
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetSix, ciPED_BSSix_HateAllOtherPedGroups) // The ped doesn't have a relationship setup with any other peds, so without this flag no combat will happen
		
			// Thrown Weapon Combat Behaviours
			IF IS_WEAPON_A_PROJECTILE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].gun)
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_THROW_PROJECTILE)
				AND (NOT IS_PED_IN_COMBAT(tempPed) OR IS_BIT_SET(iBSTaskUsingIgnoreCombatChecks[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))				
					PED_INDEX target = GET_CLOSEST_VISIBLE_PLAYER(iPed, tempPed, TRUE, PED_THROWING_MAX_RANGE)
					IF target != NULL
						VECTOR targetCoord = GET_ENTITY_COORDS(target)
						targetCoord += (GET_ENTITY_SPEED_VECTOR(target) * PED_THROWING_LEAD_SCALAR)
						IF PED_THROWING_ACCURACY_RADIUS > 0.0
							targetCoord = GET_RANDOM_POINT_IN_SPHERE(targetCoord, PED_THROWING_ACCURACY_RADIUS)
						ENDIF
						SET_CURRENT_PED_WEAPON(tempPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].gun, TRUE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( tempPed, TRUE )
						TASK_THROW_PROJECTILE(tempPed, GET_ENTITY_COORDS(target))
						SET_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						CLEAR_BIT(iBSTaskUsingIgnoreCombatChecks[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))						
					ENDIF
				ENDIF
			
			// Normal Combat Behaviours.
			ELIF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED, DEFAULT, DEFAULT, DEFAULT #IF IS_DEBUG_BUILD , TRUE #ENDIF)
			AND (NOT IS_PED_IN_COMBAT(tempPed) OR IS_BIT_SET(iBSTaskUsingIgnoreCombatChecks[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
				
				IF IS_BIT_SET(iBSTaskUsingIgnoreCombatChecks[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					PRINTLN("[RCC MISSION] - TASK_PED_GENERAL_COMBAT - Using iBSTaskUsingIgnoreCombatChecks for iPed", iped)
				ENDIF
				
				PRINTLN("[RCC MISSION] - TASK_PED_GENERAL_COMBAT - Calling TASK_COMBAT_HATED_TARGETS_AROUND_PED for ped ", iped)
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFourteen, ciPED_BSFourteen_RemoveDefensiveAreaOnCombat)
					PRINTLN("[RCC MISSION] - TASK_PED_GENERAL_COMBAT - ped ", iped, " has ciPED_BSFourteen_RemoveDefensiveAreaOnCombat set. Removing Defensive Area.")
					REMOVE_PED_DEFENSIVE_AREA(tempPed, TRUE)
				ENDIF
				
				TASK_COMBAT_HATED_TARGETS_AROUND_PED(tempPed,299)
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwelve, ciPed_BSTwelve_AggresiveIfJackedWhileFleeing)
					PRINTLN("[RCC MISSION] - TASK_PED_GENERAL_COMBAT - Calling CA_USE_VEHICLE = false, CA_CAN_COMMANDEER_VEHICLES = true. ped ", iped)
					SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE, FALSE)
					SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_ALWAYS_FLEE, FALSE)
					SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_ALWAYS_FIGHT, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_AGGRESSIVE, TRUE)						
					
					IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEleven, ciPed_BSEleven_DontCommandeerVehicles)
						SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_CAN_COMMANDEER_VEHICLES, TRUE)
					ENDIF
				ENDIF
				
				CLEAR_BIT(iBSTaskUsingIgnoreCombatChecks[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				CLEAR_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				
				IF bboat
					IF NETWORK_HAS_CONTROL_OF_ENTITY(tempveh)
						SET_BOAT_IGNORE_LAND_PROBES(tempveh,TRUE)
					ENDIF
				ENDIF
			
				#IF IS_DEBUG_BUILD
				PRINTLN("[RCC MISSION] - TASK_PED_GENERAL_COMBAT - has been called for ped ", iped)
				CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_COMBAT - bPlane = ", bPlane)
				CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_COMBAT - bHeli = ", bHeli)
				CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_COMBAT - bBoat = ", bBoat)
				CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_COMBAT - bDriver = ", bDriver)
				TEXT_LABEL_15 tlState = GET_PED_STATE_NAME(MC_serverBD_2.iPedState[iped])
				CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_COMBAT - ped state = ", tlState)
				CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_COMBAT - ped in combat = ", IS_PED_IN_COMBAT(tempPed))
				CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_COMBAT - callstack: ")
				DEBUG_PRINTCALLSTACK()
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC TASK_PED_AIM_AT_ENTITY(PED_INDEX tempPed, INT iped, ENTITY_INDEX tempTargetEnt, BOOL Heli = FALSE, BOOL bPlane = FALSE, BOOL bBoat = FALSE ,BOOL bDriver = FALSE, VEHICLE_INDEX tempveh = NULL)
	
	IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_LEAVE_VEHICLE)
		
		IF bDriver
			SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE, TRUE)
			IF bboat
				INIT_BOAT(tempveh,-1,TRUE)
			ELIF bPlane OR Heli
				REMOVE_PED_DEFENSIVE_AREA(tempPed)
				INIT_PLANE(tempveh,-1,TRUE)
			ENDIF
		ENDIF
		
		BOOL bArmed = IS_PED_ARMED(tempPed,WF_INCLUDE_GUN)
		
		IF (bArmed AND SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_AIM_GUN_AT_ENTITY))
		OR ((NOT bArmed) AND SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_LOOK_AT_ENTITY))
			
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,TRUE)
			
			IF bArmed
				TASK_AIM_GUN_AT_ENTITY(tempPed,tempTargetEnt,-1)
			ELSE
				TASK_LOOK_AT_ENTITY(tempPed,tempTargetEnt,-1)
			ENDIF
			CLEAR_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			
			IF iped > -1
				PRINTLN("[RCC MISSION] TASK_PED_AIM_AT_ENTITY - running on ped ",iped)
				CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_AIM_AT_ENTITY - bPlane = ", bPlane)
				CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_AIM_AT_ENTITY - bArmed = ", bArmed)
				CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_AIM_AT_ENTITY - bBoat = ", bBoat)
				CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_AIM_AT_ENTITY - bDriver = ", bDriver)
				CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_AIM_AT_ENTITY - callstack: ")
				DEBUG_PRINTCALLSTACK()
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

PROC PROCESS_GENERAL_COMBAT_TASK_BODY(PED_INDEX ped, INT iPed, BOOL bheli, BOOL bplane, BOOL bBoat, BOOL bDriver, VEHICLE_INDEX tempveh)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,ciPED_BSThree_CancelTasksToCombatPlayers)
		//Set their fire rate back up if they're coming out of their tasks and had a lowered shoot rate for them
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,ciPED_BSThree_LoweredShootRate)
			IF MC_serverBD.iDifficulty = DIFF_EASY //default shoot rate is 60
				SET_PED_SHOOT_RATE(ped,60)
			ELSE
				SET_PED_SHOOT_RATE(ped,100)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour,ciPED_BSFour_ActivateSiren_On_Aggro)
			IF bDriver
				IF NOT IS_VEHICLE_SIREN_ON(tempVeh)
				OR NOT IS_VEHICLE_SIREN_AUDIO_ON(tempVeh)
					SET_VEHICLE_SIREN(tempVeh, TRUE)
					SET_VEHICLE_HAS_MUTED_SIRENS(tempVeh, FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF CHECK_FOR_DROP_OFF_FLEE(ped,iped)
		EXIT
	ENDIF
	
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped,FALSE)
	
	LOAD_COLLISION_ON_PED_IF_POSSIBLE(ped,iped)
	
	TASK_PED_GENERAL_COMBAT(ped,iped,bheli,bplane,bBoat,bDriver,tempveh)
			
ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: GO TO TASKS !
//
//************************************************************************************************************************************************************



FUNC FLOAT GET_DRIVE_SPEED(INT iped, FLOAT fDistanceToCompletion, FLOAT fDefaultSpeed)
	
	FLOAT fCruiseSpeed = fDefaultSpeed
	FLOAT fSpeed15 = 5
	FLOAT fSpeed30 = 8
	FLOAT fSpeed50 = 15//25
	FLOAT fSpeed80 = 20//30
	
	//If we're getting close, slow down! - perhaps add an option to change these (a reckless driver or a super-safe driver?)
	
	IF fCruiseSpeed > cfMAX_SPEED_PED_VEH
		fCruiseSpeed = cfMAX_SPEED_PED_VEH
	ENDIF
	IF fDefaultSpeed > cfMAX_SPEED_PED_VEH
		fDefaultSpeed = cfMAX_SPEED_PED_VEH
	ENDIF
	
	IF MC_serverBD_2.iTargetID[iped] = ciINITIAL_GOTO
	AND MC_serverBD_2.iPedGotoProgress[iped] < MAX_ASSOCIATED_GOTO_TASKS
		IF IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iped], PED_ASSOCIATED_GOTO_TASK_TYPE_NOSLOWDOWN) 
			RETURN fDefaultSpeed
		ENDIF
	ENDIF
	
	IF fDistanceToCompletion <= 15
		IF fCruiseSpeed > fSpeed15
			fCruiseSpeed = fSpeed15
		ENDIF
	ELIF fDistanceToCompletion <= 30
		IF fCruiseSpeed > fSpeed30
			fCruiseSpeed = fSpeed30
		ENDIF
	ELIF fDistanceToCompletion <= 50
		IF fCruiseSpeed > fSpeed50
			fCruiseSpeed = fSpeed50
		ENDIF
	ELIF fDistanceToCompletion <= 80
		IF fCruiseSpeed > fSpeed80
			fCruiseSpeed = fSpeed80
		ENDIF
	ENDIF
	
	RETURN fCruiseSpeed
	
ENDFUNC

FUNC FLOAT GET_HELI_DRIVE_SPEED(INT iped, FLOAT fdist, FLOAT fDefaultSpeed)
	IF IS_BIT_SET(MC_serverBD_4.iPedSpeedOverride[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedSpeedOverride > -1
		RETURN TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedSpeedOverride)
	ENDIF	
	RETURN GET_DRIVE_SPEED(iped, fdist, fDefaultSpeed)
ENDFUNC

FUNC BOOL IS_PED_GOTO_TASK_PROGRESS_OUT_OF_DATE(INT iped)
	
	IF MC_serverBD_2.iPedState[iped] = ciTASK_GOTO_COORDS
	AND MC_serverBD_2.iTargetID[iped] = ciINITIAL_GOTO
	AND iPedTaskedGotoProgress[iped] != MC_serverBD_2.iPedGotoProgress[iped]
		PRINTLN("[RCC MISSION] IS_PED_GOTO_TASK_PROGRESS_OUT_OF_DATE - Return TRUE for ped ",iped," on initial goto, MC_serverBD_2.iPedGotoProgress ",MC_serverBD_2.iPedGotoProgress[iped]," != iPedTaskedGotoProgress ",iPedTaskedGotoProgress[iped])
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_PED_BE_GIVEN_VEH_GOTO_TASK(PED_INDEX tempPed, VEHICLE_INDEX tempVeh, INT iPed, BOOL bHeli, BOOL bPlane, FLOAT fThisGotoSpeed)
	
	VEHICLE_INDEX viVeh
	viVeh = NULL
	IF IS_ASSOCIATED_GOTO_ENTER_VEH_VALID(iped, MC_serverBD_2.iPedGotoProgress[iped], viVeh)
	AND NOT IS_PED_IN_VEHICLE(tempPed, viVeh, FALSE)
		IF NOT IS_PED_IN_VEHICLE(tempPed, viVeh, TRUE)
			PRINTLN("[LM][RCC MISSION] SHOULD_PED_BE_GIVEN_VEH_GOTO_TASK - iPed: ", iPed, " Returning True.")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF (bHeli AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour,ciPED_BSFour_IgnoreMinHeight))
	OR (bPlane AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPed_BSEight_CustomHeliGotos))
	OR (bPlane AND IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed], PED_ASSOCIATED_GOTO_TASK_TYPE_PLANETAXI))
		IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_VEHICLE_MISSION, FALSE, MISSION_GOTO, tempVeh)
			RETURN TRUE
		ENDIF
	ELSE
		IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS_WITH_CRUISE_SPEED)
			RETURN TRUE
		ENDIF
		
		// If both of these values are -1, we want to change drive task flags when the ped gets spooked/unspooked - to do that, we need to retask them
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDriveSpeed = -1
		AND fThisGotoSpeed = -1
			IF IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				IF NOT IS_BIT_SET(iPedGivenSpookedGotoTaskBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					PRINTLN("[RCC MISSION] SHOULD_PED_BE_GIVEN_GOTO_TASK - Return TRUE for ped ",iped,", ped has not been tasked with spooky flags but is spooked")
					RETURN TRUE
				ENDIF
			ELSE
				IF IS_BIT_SET(iPedGivenSpookedGotoTaskBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					PRINTLN("[RCC MISSION] SHOULD_PED_BE_GIVEN_GOTO_TASK - Return TRUE for ped ",iped,", ped has been tasked with spooky flags but is no longer spooked")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_PED_GOTO_TASK_PROGRESS_OUT_OF_DATE(iped)
		PRINTLN("[RCC MISSION] SHOULD_PED_BE_GIVEN_GOTO_TASK - Return TRUE for ped ",iped,", IS_PED_GOTO_TASK_PROGRESS_OUT_OF_DATE returned TRUE")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC UPDATE_PED_TASKED_GOTO_PROGRESS(INT iped)
	
	IF MC_serverBD_2.iPedState[iped] = ciTASK_GOTO_COORDS
	AND MC_serverBD_2.iTargetID[iped] = ciINITIAL_GOTO
	AND iPedTaskedGotoProgress[iped] != MC_serverBD_2.iPedGotoProgress[iped]
		PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - Update ped ",iped," iPedTaskedGotoProgress to ",MC_serverBD_2.iPedGotoProgress[iped]," & broadcast")
		iPedTaskedGotoProgress[iped] = MC_serverBD_2.iPedGotoProgress[iped]
		BROADCAST_FMMC_PED_UPDATE_TASKED_GOTO_PROGRESS(iped, MC_serverBD_2.iPedGotoProgress[iped])
	ENDIF
	
ENDPROC

FUNC FLOAT GET_ADJUSTED_CRUISE_SPEED(PED_INDEX tempPed, INT iPed, FLOAT speed)
	IF IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iped], PED_ASSOCIATED_GOTO_TASK_TYPE_ACCELCAP) 
	AND GET_ASSOCIATED_GOTO_TASK_DATA__ENTIRE_PED_ACCELERATION_CAP(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData) > 0.0
		FLOAT currentSpeed = GET_ENTITY_SPEED(tempPed)
		FLOAT maxAcceleration = GET_ASSOCIATED_GOTO_TASK_DATA__ENTIRE_PED_ACCELERATION_CAP(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData)
		PRINTLN("[RCC MISSION] GET_ADJUSTED_CRUISE_SPEED - ped ",iped, " desired speed: ", speed, " acc cap: ", maxAcceleration, " current: ", currentSpeed)
		
		IF HAS_NET_TIMER_STARTED(maxAccelerationUpdateTimers[iPed])
			IF HAS_NET_TIMER_EXPIRED(maxAccelerationUpdateTimers[iPed], 1000)
				maxAccelerationUpdateTimers[iPed].Timer = GET_TIME_OFFSET(maxAccelerationUpdateTimers[iPed].Timer, 1000)//INT_TO_NATIVE(TIME_DATATYPE, NATIVE_TO_INT(maxAccelerationUpdateTimers[iPed].Timer) + 1000)
			ELSE
				PRINTLN("[RCC MISSION] GET_ADJUSTED_CRUISE_SPEED - ped ",iped, " wait timer, return prev value: ", maxAccelerationCurrentSpeed[iPed])
				RETURN maxAccelerationCurrentSpeed[iPed]
			ENDIF
		ELSE
			START_NET_TIMER(maxAccelerationUpdateTimers[iPed], DEFAULT, TRUE)
		ENDIF
		
		IF speed < currentSpeed-maxAcceleration
			speed = currentSpeed-maxAcceleration
		ENDIF
		IF speed > currentSpeed+maxAcceleration
			speed = currentSpeed+maxAcceleration
		ENDIF
		maxAccelerationCurrentSpeed[iPed] = speed
		PRINTLN("[RCC MISSION] GET_ADJUSTED_CRUISE_SPEED - ped ",iped, " return: ",speed)
	ENDIF
	RETURN speed
ENDFUNC

PROC TASK_PED_GOTO_ANY_MEANS(PED_INDEX tempPed, VECTOR Vcoords, FLOAT fdist, INT iped,VEHICLE_INDEX tempveh,BOOL binveh,BOOL bdriver = FALSE,BOOL bLongRange = FALSE, BOOL bheli = FALSE,BOOL bplane = FALSE,BOOL bboat = FALSE, BOOL bGetOut = TRUE)

FLOAT fdrivespeed
FLOAT fshootrange
FLOAT fOnFootSpeed
FLOAT fVehArriveRange = 4.0
	
	LOAD_COLLISION_ON_PED_IF_POSSIBLE(tempPed,iped)
	IF bdriver
	AND binveh
		IF NOT IS_ANY_PED_ENTERING_VEHICLE(tempveh)
			IF IS_BIT_SET(MC_serverBD_4.iPedClearFailOnGoTo[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				IF (GET_SCRIPT_TASK_STATUS(tempPed, SCRIPT_TASK_SMART_FLEE_POINT) != PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(tempPed, SCRIPT_TASK_SMART_FLEE_POINT) != WAITING_TO_START_TASK)
					PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - ped ",iped, "was hacked. Fleeing")
					SET_PED_FLEE_ATTRIBUTES(tempPed, FA_USE_VEHICLE, TRUE)
					SET_PED_FLEE_ATTRIBUTES(tempPed, FA_DISABLE_COWER, TRUE)
					SET_PED_FLEE_ATTRIBUTES(tempPed, FA_DISABLE_EXIT_VEHICLE, TRUE)
					SET_PED_FLEE_ATTRIBUTES(tempPed, FA_COWER_INSTEAD_OF_FLEE, FALSE)
					SET_PED_FLEE_ATTRIBUTES(tempPed, FA_DISABLE_HESITATE_IN_VEHICLE, TRUE)
					SET_PED_FLEE_ATTRIBUTES(tempPed, FA_DISABLE_COWER,TRUE)
					TASK_SMART_FLEE_PED(tempPed, LocalPlayerPed, 500, INFINITE_TASK_TIME)
					CLEAR_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				ENDIF
				EXIT
			ENDIF
			
			FLOAT fThisGotoSpeed = -1
			INT iCustomVehicleGotoSpeed = GET_ASSOCIATED_GOTO_TASK_DATA__VEHICLE_GOTO_SPEED(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed])
			FLOAT fCustomVehicleArrivalRadius = GET_ASSOCIATED_GOTO_TASK_DATA__CUSTOM_VEHICLE_ARRIVE_RADIUS(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed])
			
			IF iped >= 0
				IF MC_serverBD_2.iPedGotoProgress[iped] >= 0
					IF iCustomVehicleGotoSpeed != -1
						fThisGotoSpeed = TO_FLOAT(iCustomVehicleGotoSpeed)
						PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - iVehicleGotoSpeed > -1 for ped ",iped, ", iVehicleGotoSpeed = ", iCustomVehicleGotoSpeed, ", iPedGotoProgress[iped]] = ", MC_serverBD_2.iPedGotoProgress[iped])
						IF fThisGotoSpeed > cfMAX_SPEED_PED_VEH
							fThisGotoSpeed = cfMAX_SPEED_PED_VEH
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF SHOULD_PED_BE_GIVEN_VEH_GOTO_TASK(tempPed, tempVeh, iped, bHeli, bPlane, fThisGotoSpeed)
				IF NOT HAS_NET_TIMER_STARTED(tdparkTimer[iped])
				OR GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdparkTimer[iped]) > 2000
					IF bplane OR bheli
						INIT_PLANE(tempVeh,-1,TRUE)
					ELIF bboat
						INIT_BOAT(tempVeh,-1,TRUE)
					ENDIF
					
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,TRUE)
					
					SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE_ATTACK,FALSE)
					SET_PED_CONFIG_FLAG(tempPed, PCF_GetOutBurningVehicle, FALSE)
					SET_PED_CONFIG_FLAG(tempPed, PCF_GetOutUndriveableVehicle, FALSE)
					
					IF (NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetSix, ciPED_BSSix_NeverShootOnGoto))
					AND (IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
						OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset,ciPED_BS_PedAttackWhileGoto))
						fshootrange = 100
					ELSE
						fshootrange = -1
					ENDIF
					
					FLOAT fCruiseSpeed = -1
					
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDriveSpeed !=-1
					OR IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					OR fThisGotoSpeed > -1
						IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDriveSpeed !=-1
						OR fThisGotoSpeed > -1
							
							IF fThisGotoSpeed > -1
								fCruiseSpeed = fThisGotoSpeed
								PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - Setting fCruiseSpeed = fThisGotoSpeed for ped ",iped, " fCruiseSpeed = ", fCruiseSpeed)
							ELSE
								fCruiseSpeed = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDriveSpeed)
								PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - Setting fCruiseSpeed = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDriveSpeed) for ped ",iped, " fCruiseSpeed = ", fCruiseSpeed)
							ENDIF
							
							IF fCruiseSpeed > cfMAX_SPEED_PED_VEH
								fCruiseSpeed = cfMAX_SPEED_PED_VEH
							ENDIF
							
							//Handle slowing down on approach to certain Ped Goto locations:
							IF NOT ((bPlane) OR (bHeli))
							AND (MC_serverBD_2.iPedState[iped] = ciTASK_GOTO_COORDS)
							AND ( (MC_serverBD_2.iTargetID[iped] = ciINITIAL_GOTO)
							   OR (MC_serverBD_2.iTargetID[iped] = ciSECONDARY_GOTO) )
								fCruiseSpeed = GET_DRIVE_SPEED(iped, fdist, fCruiseSpeed)
							ENDIF
							
						ELSE //Ped must be spooked
							
							IF bPlane
								fCruiseSpeed = 70.0
							ELIF bHeli
								fCruiseSpeed = 40.0
							ELSE
								fCruiseSpeed = 30
								
								IF (MC_serverBD_2.iPedState[iped] = ciTASK_GOTO_COORDS)
								AND ( (MC_serverBD_2.iTargetID[iped] = ciINITIAL_GOTO)
								   OR (MC_serverBD_2.iTargetID[iped] = ciSECONDARY_GOTO) )
									fCruiseSpeed = GET_DRIVE_SPEED(iped, fdist, fCruiseSpeed)
								ENDIF
							ENDIF
							
						ENDIF
						
						IF bPlane
						AND IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed], PED_ASSOCIATED_GOTO_TASK_TYPE_PLANETAXI)
							
							IF MC_serverBD_2.iPedState[iped] = ciTASK_GOTO_COORDS
							AND MC_serverBD_2.iTargetID[iped] = ciINITIAL_GOTO
							AND MC_serverBD_2.iPedGotoProgress[iped] < MAX_ASSOCIATED_GOTO_TASKS
								IF fCustomVehicleArrivalRadius != 0.0
									fVehArriveRange = fCustomVehicleArrivalRadius
								ENDIF
							ENDIF
							
							PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - Calling TASK_PLANE_TAXI on ped ",iped," to coords ",Vcoords,", cruise speed ",fcruisespeed, ", fVehArriveRange = ",fVehArriveRange)
							TASK_PLANE_TAXI(tempPed, tempVeh, Vcoords, fCruiseSpeed, fVehArriveRange)
							
							UPDATE_PED_TASKED_GOTO_PROGRESS(iped)
							
						ELIF (bHeli
						AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour,ciPED_BSFour_IgnoreMinHeight))
						OR (bPlane
						AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight,  ciPed_BSEight_CustomHeliGotos))
							
							INT iMinHeightAboveTerrain = 10
							IF bPlane
								iMinHeightAboveTerrain = 0
							ENDIF
							INT iFlyHeight = 0
							// Careful changing heli mode! Down below it is set to HF_NONE, to turn off HF_MaintainHeightAboveTerrain bit
							HELIMODE eHeliMode = HF_MaintainHeightAboveTerrain
							
							IF (MC_serverBD_2.iPedState[iped] = ciTASK_GOTO_COORDS)
							AND ( (MC_serverBD_2.iTargetID[iped] = ciINITIAL_GOTO)
							   OR (MC_serverBD_2.iTargetID[iped] = ciSECONDARY_GOTO) )
								
								IF MC_serverBD_2.iTargetID[iped] = ciINITIAL_GOTO
									FLOAT fCustomArrivalHeight = GET_ASSOCIATED_GOTO_TASK_DATA__ARRIVAL_HEIGHT(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed])
									iMinHeightAboveTerrain = ROUND(fCustomArrivalHeight)
									PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - Heli flight height for ped ",iped," from globals: ",iMinHeightAboveTerrain)
								ELSE
									iMinHeightAboveTerrain = ROUND(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fSecondaryVectorGroundHeight)
									PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - Heli flight height for ped ",iped," from secondary task globals: ", iMinHeightAboveTerrain)
								ENDIF
								
							ELSE
								PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - Heli flight height for ped ",iped," not found in globals")
								
								FLOAT fGroundZ
								IF GET_GROUND_Z_FOR_3D_COORD(Vcoords,fGroundZ)
									iMinHeightAboveTerrain = ROUND(Vcoords.z - fGroundZ)
								ENDIF
							ENDIF
							
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight,  ciPed_BSEight_CustomHeliGotos)
								IF (MC_serverBD_2.iPedState[iped] = ciTASK_GOTO_COORDS)
								AND ( (MC_serverBD_2.iTargetID[iped] = ciINITIAL_GOTO)
								   OR (MC_serverBD_2.iTargetID[iped] = ciSECONDARY_GOTO) )
									fCruiseSpeed = GET_HELI_DRIVE_SPEED(iped, fdist, fCruiseSpeed)
									PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - Heli setting custom cruise speed")
								ENDIF
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPed_BSEight_DisableMaintainHeightAboveTerrain)
									eHeliMode = HF_NONE // Careful not to modify eHeliMode before here
									PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - Heli disable maintain height above terrain")
								ENDIF
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPed_BSEight_ForceHeightMapAvoidance)
									eHeliMode = eHeliMode | HF_ForceHeightMapAvoidance
									PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - Heli force height map avoidance")
								ENDIF
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPed_BSEight_HeliDontDoAvoidance)
									eHeliMode = eHeliMode | HF_DontDoAvoidance
									PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - Heli dont do avoidance")
								ENDIF
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPed_BSEight_DisableHeightMapAvoidance)
									eHeliMode = eHeliMode | HF_DisableAllHeightMapAvoidance
									PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - Heli dont avoid map")
								ENDIF
								
								iFlyHeight = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFlightHeight
								PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - Heli iFlyHeight for ped ",iped," from ped setting ",iFlyHeight)
								
								IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iMinFlyHeightAboveTerrain != -1
									iMinHeightAboveTerrain = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iMinFlyHeightAboveTerrain
									PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - Heli min flight height for ped ",iped," from ped setting ",iMinHeightAboveTerrain)
								ENDIF
							ENDIF
							fCruiseSpeed = GET_ADJUSTED_CRUISE_SPEED(tempPed, iped, fCruiseSpeed)
							
							IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedGotoHeightOffset != -101
							AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedGoToHeightOffsetAtHealth != -1
								FLOAT fPercentageThreshhold = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedGoToHeightOffsetAtHealth)
								
								IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle > -1
									IF GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(tempVeh, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle, MC_serverBD.iTotalNumStartingPlayers) < fPercentageThreshhold
										iFlyHeight += g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedGotoHeightOffset
										Vcoords.z += g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedGotoHeightOffset
										iMinHeightAboveTerrain += g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedGotoHeightOffset
										PRINTLN("[LM][TASK_GO_TO_COORD_ANY_MEANS_etc][iPedGotoHeightOffset] - iPedGotoHeightOffset: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedGotoHeightOffset)
										PRINTLN("[LM][TASK_GO_TO_COORD_ANY_MEANS_etc][iPedGotoHeightOffset] - Applying Height Override (0) - GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE: ", GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(tempVeh, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle, MC_serverBD.iTotalNumStartingPlayers), " fPercentageThreshhold: ", fPercentageThreshhold, " iVehicle: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle, " iFlyHeight is now: ", iFlyHeight)
									ENDIF
								ENDIF
							ENDIF
							
							IF bHeli
								PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - Calling TASK_HELI_MISSION on ped ",iped," to coords ",vCoords,", fCruiseSpeed ", fCruiseSpeed,", iMinHeightAboveTerrain ",iMinHeightAboveTerrain)
								TASK_HELI_MISSION(tempPed, tempVeh, NULL, NULL, Vcoords, MISSION_GOTO, fCruiseSpeed, 5, -1.0, iFlyHeight, iMinHeightAboveTerrain, -1, eHeliMode)
							ELSE
								PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - Calling TASK_PLANE_MISSION on ped ",iped," to coords ",vCoords,", fCruiseSpeed ", fCruiseSpeed,", iMinHeightAboveTerrain ",iMinHeightAboveTerrain)
								TASK_PLANE_MISSION(tempPed, tempVeh, NULL, NULL, Vcoords, MISSION_GOTO, fCruiseSpeed, 5, -1.0, iFlyHeight, iMinHeightAboveTerrain)
							ENDIF
							
							SET_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							
							UPDATE_PED_TASKED_GOTO_PROGRESS(iped)
							
						ELSE
							DRIVINGMODE dmFlags = DF_SwerveAroundAllCars | DF_SteerAroundStationaryCars | DF_SteerAroundObjects | DF_SteerAroundPeds | DF_UseShortCutLinks | DF_ChangeLanesAroundObstructions | DF_UseSwitchedOffNodes | DF_AdjustCruiseSpeedBasedOnRoadSpeed | DF_ForceJoinInRoadDirection
							TASK_GO_TO_COORD_ANY_MEANS_FLAGS tgcamFlags = TGCAM_IGNORE_VEHICLE_HEALTH | TGCAM_CONSIDER_ALL_NEARBY_VEHICLES | TGCAM_PROPER_IS_DRIVEABLE_CHECK
							
							IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwelve, ciPed_BSTwelve_DisableDriveIntoOncomingTraffic)
								dmFlags = dmFlags | DF_DriveIntoOncomingTraffic
								PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS added DriveIntoOncomingTraffic in driving mode.")
							ENDIF
							
							IF NOT bGetOut
								tgcamFlags = tgcamFlags | TGCAM_REMAIN_IN_VEHICLE_AT_DESTINATION
							ENDIF
							
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetSix, ciPED_BSSix_DontAbandonVehicleOnGoto_IfMoving)
								tgcamFlags = tgcamFlags | TGCAM_NEVER_ABANDON_VEHICLE_IF_MOVING
							ENDIF
							
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPed_BSEight_NeverLeaveVehicle)
								tgcamFlags = tgcamFlags | TGCAM_NEVER_ABANDON_VEHICLE
							ENDIF
							
							IF fshootrange > -1
								SET_PED_CONFIG_FLAG(tempPed, PCF_BroadcastRepondedToThreatWhenGoingToPointShooting,TRUE)
								tgcamFlags = tgcamFlags | TGCAM_USE_AI_TARGETING_FOR_THREATS
								PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS on foot only - calling TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS Set PCF_BroadcastRepondedToThreatWhenGoingToPointShooting (1) for ped: ",iped, " fshootrange = ", fshootrange)
							ENDIF 
							
							IF IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed], PED_ASSOCIATED_GOTO_TASK_TYPE_FORCENAVMESH) 
								IF IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed], PED_ASSOCIATED_GOTO_TASK_TYPE_IGNORE_NAVMESH) 
									dmFlags = dmFlags | DF_ForceStraightLine
									PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS on foot only - calling TASK_GO_TO_COORD_ANY_MEANS_etc Set DF_ForceStraightLine for ped: ", iped)
									
								ELSE
									dmFlags = dmFlags | DF_PreferNavmeshRoute
									PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS on foot only - calling TASK_GO_TO_COORD_ANY_MEANS_etc Set DF_PreferNavmeshRoute for ped: ", iped)
								ENDIF
							ENDIF
							
							IF MC_serverBD_2.iPedState[iped] = ciTASK_GOTO_COORDS
							AND MC_serverBD_2.iTargetID[iped] = ciINITIAL_GOTO
							AND MC_serverBD_2.iPedGotoProgress[iped] < MAX_ASSOCIATED_GOTO_TASKS
								IF fCustomVehicleArrivalRadius != 0.0
									fVehArriveRange = fCustomVehicleArrivalRadius
								ENDIF
							ENDIF
							fCruiseSpeed = GET_ADJUSTED_CRUISE_SPEED(tempPed, iped, fCruiseSpeed)
							PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - Calling TASK_GO_TO_COORD_ANY_MEANS_etc on ped ",iped," to coords ",Vcoords,", bGetOut ",bGetout,", cruise speed ",fcruisespeed,", bLongRange ",bLongRange, ", fVehArriveRange = ",fVehArriveRange)
							
							IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedGotoHeightOffset != -101
							AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedGoToHeightOffsetAtHealth != -1
								FLOAT fPercentageThreshhold = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedGoToHeightOffsetAtHealth)
								
								IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle > -1
									IF GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(tempVeh, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle, MC_serverBD.iTotalNumStartingPlayers) < fPercentageThreshhold
										Vcoords.z += g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedGotoHeightOffset
										PRINTLN("[LM][TASK_GO_TO_COORD_ANY_MEANS_etc][iPedGotoHeightOffset] - Applying Height Override (1) - GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE: ", GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(tempVeh, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle, MC_serverBD.iTotalNumStartingPlayers), " fPercentageThreshhold: ", fPercentageThreshhold, " iVehicle: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle)
									ENDIF
								ENDIF
							ENDIF
							
							TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS_WITH_CRUISE_SPEED(tempPed, Vcoords, PEDMOVEBLENDRATIO_RUN, tempVeh, bLongRange, dmFlags, fshootrange, 0, 10, tgcamFlags, fCruiseSpeed, fVehArriveRange)
							SET_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							
							UPDATE_PED_TASKED_GOTO_PROGRESS(iped)
							
							IF NOT IS_BIT_SET(iPedGivenSpookedGotoTaskBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
								PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - Set iPedGivenSpookedGotoTaskBS & broadcast for ped ",iped)
								SET_BIT(iPedGivenSpookedGotoTaskBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
								BROADCAST_FMMC_PED_GIVEN_SPOOKED_GOTO_TASK(iped)
							ENDIF
							
						ENDIF
						
					ELSE //Not spooked and does not have driving speed set:
						
						IF bPlane
							fCruiseSpeed = 35.0
						ELIF bHeli
							fCruiseSpeed = 20.0
						ELSE
							fCruiseSpeed = 15.0
							
							IF (MC_serverBD_2.iPedState[iped] = ciTASK_GOTO_COORDS)
							AND ( (MC_serverBD_2.iTargetID[iped] = ciINITIAL_GOTO)
							   OR (MC_serverBD_2.iTargetID[iped] = ciSECONDARY_GOTO) )
								fCruiseSpeed = GET_DRIVE_SPEED(iped, fdist, fCruiseSpeed)
							ENDIF
						ENDIF
						
						IF bPlane
						AND IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed], PED_ASSOCIATED_GOTO_TASK_TYPE_PLANETAXI)
							
							IF MC_serverBD_2.iPedState[iped] = ciTASK_GOTO_COORDS
							AND MC_serverBD_2.iTargetID[iped] = ciINITIAL_GOTO
							AND MC_serverBD_2.iPedGotoProgress[iped] < MAX_ASSOCIATED_GOTO_TASKS
								IF fCustomVehicleArrivalRadius != 0.0
									fVehArriveRange = fCustomVehicleArrivalRadius
								ENDIF
							ENDIF
							
							PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - Calling TASK_PLANE_TAXI on ped ",iped," to coords ",Vcoords,", cruise speed ",fcruisespeed, ", fVehArriveRange = ",fVehArriveRange)
							TASK_PLANE_TAXI(tempPed, tempVeh, Vcoords, fCruiseSpeed, fVehArriveRange)
							SET_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							
							UPDATE_PED_TASKED_GOTO_PROGRESS(iped)
							
						ELIF (bHeli
						AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour,ciPED_BSFour_IgnoreMinHeight))
						OR (bPlane
						AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight,  ciPed_BSEight_CustomHeliGotos))
						
							INT iMinHeightAboveTerrain = 10
							IF bPlane
								iMinHeightAboveTerrain = 0
							ENDIF
							INT iFlyHeight = 0
							// Careful changing heli mode! Down below it is set to HF_NONE, to turn off HF_MaintainHeightAboveTerrain bit
							HELIMODE eHeliMode = HF_MaintainHeightAboveTerrain
							FLOAT targetReachedDistance = 5
							
							IF (MC_serverBD_2.iPedState[iped] = ciTASK_GOTO_COORDS)
							AND ( (MC_serverBD_2.iTargetID[iped] = ciINITIAL_GOTO)
							   OR (MC_serverBD_2.iTargetID[iped] = ciSECONDARY_GOTO) )
							   
								IF MC_serverBD_2.iTargetID[iped] = ciINITIAL_GOTO
									FLOAT fCustomArrivalHeight = GET_ASSOCIATED_GOTO_TASK_DATA__ARRIVAL_HEIGHT(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed])
									iMinHeightAboveTerrain = ROUND(fCustomArrivalHeight)
									PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - Heli flight height for ped ",iped," from globals: ",iMinHeightAboveTerrain)
								ELSE
									iMinHeightAboveTerrain = ROUND(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fSecondaryVectorGroundHeight)
									PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - Heli flight height for ped ",iped," from secondary task globals: ",iMinHeightAboveTerrain)
								ENDIF
								
							ELSE
								PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - Heli flight height for ped ",iped," not found in globals")
								
								FLOAT fGroundZ
								IF GET_GROUND_Z_FOR_3D_COORD(Vcoords,fGroundZ)
									iMinHeightAboveTerrain = ROUND(Vcoords.z - fGroundZ)
								ENDIF
							ENDIF
							
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight,  ciPed_BSEight_CustomHeliGotos)
								IF (MC_serverBD_2.iPedState[iped] = ciTASK_GOTO_COORDS)
								AND ( (MC_serverBD_2.iTargetID[iped] = ciINITIAL_GOTO)
								   OR (MC_serverBD_2.iTargetID[iped] = ciSECONDARY_GOTO) )
									fCruiseSpeed = GET_HELI_DRIVE_SPEED(iped, fdist, fCruiseSpeed)
									PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - Heli setting custom cruise speed")
								ENDIF
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPed_BSEight_DisableMaintainHeightAboveTerrain)
									eHeliMode = HF_NONE // Careful not to modify eHeliMode before here
									PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - Heli disable maintain height above terrain")
								ENDIF
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPed_BSEight_ForceHeightMapAvoidance)
									eHeliMode = eHeliMode | HF_ForceHeightMapAvoidance
									PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - Heli force height map avoidance")
								ENDIF
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPed_BSEight_HeliDontDoAvoidance)
									eHeliMode = eHeliMode | HF_DontDoAvoidance
									PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - Heli dont do avoidance")
								ENDIF
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPed_BSEight_DisableHeightMapAvoidance)
									eHeliMode = eHeliMode | HF_DisableAllHeightMapAvoidance
									PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - Heli dont avoid map")
								ENDIF
								iFlyHeight = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFlightHeight
								IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iMinFlyHeightAboveTerrain != -1
									iMinHeightAboveTerrain = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iMinFlyHeightAboveTerrain
									PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - Heli flight height for ped ",iped," from secondary task globals: ",iMinHeightAboveTerrain)
								ENDIF
							ENDIF
							fCruiseSpeed = GET_ADJUSTED_CRUISE_SPEED(tempPed, iped, fCruiseSpeed)
							IF bHeli
								PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - Calling TASK_HELI_MISSION + HF_MaintainHeightAboveTerrain on ped ",iped," to coords ",vCoords,", fCruiseSpeed ", fCruiseSpeed,", iMinHeightAboveTerrain ",iMinHeightAboveTerrain)
								TASK_HELI_MISSION(tempPed, tempVeh, NULL, NULL, Vcoords, MISSION_GOTO, fCruiseSpeed, targetReachedDistance, -1.0, iFlyHeight, iMinHeightAboveTerrain, -1, eHeliMode)
							ELSE
								PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - Calling TASK_PLANE_MISSION on ped ",iped," to coords ",vCoords,", fCruiseSpeed ", fCruiseSpeed,", iMinHeightAboveTerrain ",iMinHeightAboveTerrain)
								TASK_PLANE_MISSION(tempPed, tempVeh, NULL, NULL, Vcoords, MISSION_GOTO, fCruiseSpeed, targetReachedDistance, -1.0, iFlyHeight, iMinHeightAboveTerrain)
							ENDIF
							
							SET_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							
							UPDATE_PED_TASKED_GOTO_PROGRESS(iped)
							
						ELSE
							DRIVINGMODE dmFlags = DRIVINGMODE_STOPFORCARS | DF_UseSwitchedOffNodes | DF_AdjustCruiseSpeedBasedOnRoadSpeed
							TASK_GO_TO_COORD_ANY_MEANS_FLAGS tgcamFlags = TGCAM_IGNORE_VEHICLE_HEALTH | TGCAM_CONSIDER_ALL_NEARBY_VEHICLES | TGCAM_PROPER_IS_DRIVEABLE_CHECK
							
							IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwelve, ciPed_BSTwelve_DisableDriveIntoOncomingTraffic)
								dmFlags = dmFlags | DF_DriveIntoOncomingTraffic
								PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS added DriveIntoOncomingTraffic in driving mode.")
							ENDIF
							
							IF NOT bGetOut
								tgcamFlags = tgcamFlags | TGCAM_REMAIN_IN_VEHICLE_AT_DESTINATION
							ENDIF
							
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetSix, ciPED_BSSix_DontAbandonVehicleOnGoto_IfMoving)
								tgcamFlags = tgcamFlags | TGCAM_NEVER_ABANDON_VEHICLE_IF_MOVING
							ENDIF
							
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPed_BSEight_NeverLeaveVehicle)
								tgcamFlags = tgcamFlags | TGCAM_NEVER_ABANDON_VEHICLE
							ENDIF
							
							IF fshootrange > -1
								SET_PED_CONFIG_FLAG(tempPed, PCF_BroadcastRepondedToThreatWhenGoingToPointShooting,TRUE)
								tgcamFlags = tgcamFlags | TGCAM_USE_AI_TARGETING_FOR_THREATS
								PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS on foot only - calling TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS Set PCF_BroadcastRepondedToThreatWhenGoingToPointShooting (2) for ped: ",iped, " fshootrange = ", fshootrange)
							ENDIF 
							
							IF IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed], PED_ASSOCIATED_GOTO_TASK_TYPE_FORCENAVMESH) 
								dmFlags = dmFlags | DF_PreferNavmeshRoute
								PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS on foot only - calling TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS Set DF_PreferNavmeshRoute for ped: ",iped)
							ENDIF
							
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].ipedBitsetTwelve, ciPed_BSTwelve_ForceAggroAvoidanceFlag)
								dmFlags = dmFlags | DF_SwerveAroundAllCars | DF_SteerAroundStationaryCars | DF_SteerAroundObjects | DF_SteerAroundPeds | DF_UseShortCutLinks | DF_ChangeLanesAroundObstructions | DF_ForceJoinInRoadDirection
								PRINTLN("[RCC MISSION][LV] TASK_PED_GOTO_ANY_MEANS added EXTRA FLAGS for avoidance the same as aggro")
							ENDIF
							
							IF MC_serverBD_2.iPedState[iped] = ciTASK_GOTO_COORDS
							AND MC_serverBD_2.iTargetID[iped] = ciINITIAL_GOTO
							AND MC_serverBD_2.iPedGotoProgress[iped] < MAX_ASSOCIATED_GOTO_TASKS
								IF fCustomVehicleArrivalRadius != 0.0
									fVehArriveRange = fCustomVehicleArrivalRadius
								ENDIF
							ENDIF
							fCruiseSpeed = GET_ADJUSTED_CRUISE_SPEED(tempPed, iped, fCruiseSpeed)
							PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - Calling TASK_GO_TO_COORD_ANY_MEANS_etc on ped ",iped," to coords ",Vcoords,", bGetOut ",bGetout,", cruise speed ",fcruisespeed,", bLongRange ",bLongRange, ", fVehArriveRange = ",fVehArriveRange)
														
							IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedGotoHeightOffset != -101
							AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedGoToHeightOffsetAtHealth != -1
								FLOAT fPercentageThreshhold = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedGoToHeightOffsetAtHealth)
								
								IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle > -1
									IF GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(tempVeh, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle, MC_serverBD.iTotalNumStartingPlayers) < fPercentageThreshhold
										Vcoords.z += g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedGotoHeightOffset
										PRINTLN("[LM][TASK_GO_TO_COORD_ANY_MEANS_etc][iPedGotoHeightOffset] - Applying Height Override (2) - GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE: ", GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(tempVeh, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle, MC_serverBD.iTotalNumStartingPlayers), " fPercentageThreshhold: ", fPercentageThreshhold, " iVehicle: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle)
									ENDIF
								ENDIF
							ENDIF
							
							TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS_WITH_CRUISE_SPEED(tempPed,Vcoords,PEDMOVEBLENDRATIO_RUN,tempVeh,bLongRange,dmFlags,fshootrange,0,10,tgcamFlags,fCruiseSpeed,fVehArriveRange)
							SET_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							
							UPDATE_PED_TASKED_GOTO_PROGRESS(iped)
							
							IF IS_BIT_SET(iPedGivenSpookedGotoTaskBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
								PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - Clear iPedGivenSpookedGotoTaskBS & broadcast for ped ",iped)
								CLEAR_BIT(iPedGivenSpookedGotoTaskBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
								BROADCAST_FMMC_PED_GIVEN_UNSPOOKED_GOTO_TASK(iped)
							ENDIF
							
						ENDIF
						
					ENDIF
				ENDIF
			ELSE
				
				IF DOES_ENTITY_EXIST(tempVeh)
				AND IS_VEHICLE_DRIVEABLE(tempVeh)
					IF IS_VEHICLE_ATTACHED_TO_TRAILER(tempVeh)
						IF NOT IS_BIT_SET(iPedHasTrailerBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
							PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - ped ", iped, " Has a trailer attached.")
							BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_HasTrailerattachedForPed, DEFAULT, iPed)
							SET_BIT(iPedHasTrailerBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
						ENDIF
					ELIF IS_BIT_SET(iPedHasTrailerBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					AND NOT IS_BIT_SET(iPedTrailerDetachedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					AND IS_BIT_SET(iPedHasTrailerBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
						PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - ped ", iped, " No longer has a trailer attached.")
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_TrailerDetachedForPed, DEFAULT, iPed)
						SET_BIT(iPedTrailerDetachedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					ENDIF
				ENDIF
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDriveSpeed !=-1
				OR (MC_serverBD_2.iPedGotoProgress[iped] >= 0 AND iCustomVehicleGotoSpeed != -1)
					PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - TASK_GO_TO_COORD_ANY_MEANS_etc for ped " ,iped, " iDriveSpeed: ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDriveSpeed)
					
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDriveSpeed !=-1
						fdrivespeed = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDriveSpeed)
						IF fdrivespeed > cfMAX_SPEED_PED_VEH
							fdrivespeed = cfMAX_SPEED_PED_VEH
						ENDIF
					ENDIF
					
					IF iCustomVehicleGotoSpeed != -1
						fdrivespeed = TO_FLOAT(iCustomVehicleGotoSpeed)
						IF fdrivespeed > cfMAX_SPEED_PED_VEH
							fdrivespeed = cfMAX_SPEED_PED_VEH
						ENDIF
						PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - SCRIPT_TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS_WITH_CRUISE_SPEED perfroming task, custom speed set for this goto = ", fdrivespeed, " ped = ", iped, " iPedGotoProgress[iped]] = ", MC_serverBD_2.iPedGotoProgress[iped])
					ENDIF
					
					//Handle slowing down on approach to certain Ped Goto locations:
					IF NOT ((bPlane) OR (bHeli))
					AND (MC_serverBD_2.iPedState[iped] = ciTASK_GOTO_COORDS)
					AND ( (MC_serverBD_2.iTargetID[iped] = ciINITIAL_GOTO)
						   OR (MC_serverBD_2.iTargetID[iped] = ciSECONDARY_GOTO) )
						fdrivespeed = GET_DRIVE_SPEED(iped, fdist, fdrivespeed)
					ENDIF
					
					IF IS_BIT_SET(MC_serverBD_4.iPedSpeedOverride[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedSpeedOverride > -1
						PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - TASK_GO_TO_COORD_ANY_MEANS_etc for ped ",iped," speed overriden to ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedSpeedOverride)
						fdrivespeed = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedSpeedOverride)
					ENDIF
					
					IF IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					AND IS_ASSOCIATED_GOTO_TASK_DATA__ENTIRE_BIT_SET(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData, ciENTIRE_TASK_BITSET_DONT_OVERRIDE_SPEED_WHEN_SPOOKED)
						PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - TASK_GO_TO_COORD_ANY_MEANS_etc for ped ",iped," SPOOKED")
						fdrivespeed = 30
						
						IF bPlane
							fdrivespeed = 70.0
						ELIF bHeli
							fdrivespeed = 40.0
						ELSE
							//Handle slowing down on approach to certain Ped Goto locations:
							IF (MC_serverBD_2.iPedState[iped] = ciTASK_GOTO_COORDS)
							AND ( (MC_serverBD_2.iTargetID[iped] = ciINITIAL_GOTO)
							   OR (MC_serverBD_2.iTargetID[iped] = ciSECONDARY_GOTO) )
								fdrivespeed = GET_DRIVE_SPEED(iped, fdist, fdrivespeed)
							ENDIF
						ENDIF
					ENDIF
					
					fdrivespeed = GET_ADJUSTED_CRUISE_SPEED(tempPed, iped, fdrivespeed)
					SET_DRIVE_TASK_CRUISE_SPEED(tempPed,fdrivespeed)
										
					PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - TASK_GO_TO_COORD_ANY_MEANS_etc for ped ",iped,", overriding drive speed 1: ",fdrivespeed)
				ELSE
					IF IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
						
						fdrivespeed = 30
						
						IF bPlane
							fdrivespeed = 70.0
						ELIF bHeli
							fdrivespeed = 40.0
						ELSE
							//Handle slowing down on approach to certain Ped Goto locations:
							IF (MC_serverBD_2.iPedState[iped] = ciTASK_GOTO_COORDS)
							AND ( (MC_serverBD_2.iTargetID[iped] = ciINITIAL_GOTO)
							   OR (MC_serverBD_2.iTargetID[iped] = ciSECONDARY_GOTO) )
								fdrivespeed = GET_DRIVE_SPEED(iped, fdist, fdrivespeed)
							ENDIF
						ENDIF
						
						IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSpookedDriveSpeed > 0
							fdrivespeed = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSpookedDriveSpeed)
							PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - TASK_GO_TO_COORD_ANY_MEANS_etc - Using creator set override g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSpookedDriveSpeed")
						ENDIF
						
						SET_DRIVE_TASK_CRUISE_SPEED(tempPed,fdrivespeed)
						
						PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - TASK_GO_TO_COORD_ANY_MEANS_etc for ped ",iped,", overriding drive speed 2: ",fdrivespeed)
					ELSE
						fdrivespeed = 15.0
						
						IF bPlane
							fdrivespeed = 35.0
						ELIF bHeli
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight,  ciPed_BSEight_CustomHeliGotos)
							AND (MC_serverBD_2.iPedState[iped] = ciTASK_GOTO_COORDS)
							AND ( (MC_serverBD_2.iTargetID[iped] = ciINITIAL_GOTO)
							   OR (MC_serverBD_2.iTargetID[iped] = ciSECONDARY_GOTO) )
								fdrivespeed = GET_HELI_DRIVE_SPEED(iped, fdist, 20.0)
							ELSE
								fdrivespeed = 20.0
							ENDIF
						ELSE
							//Handle slowing down on approach to certain Ped Goto locations:
							IF (MC_serverBD_2.iPedState[iped] = ciTASK_GOTO_COORDS)
							AND ( (MC_serverBD_2.iTargetID[iped] = ciINITIAL_GOTO)
							   OR (MC_serverBD_2.iTargetID[iped] = ciSECONDARY_GOTO) )
								fdrivespeed = GET_DRIVE_SPEED(iped, fdist, fdrivespeed)
							ENDIF
						ENDIF
						fdrivespeed = GET_ADJUSTED_CRUISE_SPEED(tempPed, iped, fdrivespeed)
						SET_DRIVE_TASK_CRUISE_SPEED(tempPed,fdrivespeed)
						
						PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - TASK_GO_TO_COORD_ANY_MEANS_etc for ped ",iped,", overriding drive speed 3: ",fdrivespeed)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			TASK_WAIT_FOR_PASSENGERS(tempPed,tempveh,iped)
		ENDIF 
	ELSE
		// Might need to add in some combat functionality here.
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPed_BSEight_NeverLeaveVehicle)
			PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - TASK_GO_TO_COORD_ANY_MEANS iPed: ", iPed, " Vehicle is broken. We are Exitting from on-foot section due to ciPed_BSEight_NeverLeaveVehicle")
			EXIT
		ENDIF
		
		IF IS_ASSOCIATED_GOTO_TASK_DATA__ENTIRE_BIT_SET(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, ciENTIRE_TASK_BITSET_PAUSE_GOTO_DURING_AGGRO_TIMER)
			IF HAS_NET_TIMER_STARTED(PedAgroTimer[iped])
			AND NOT IS_BIT_SET(iPedAgroStageTwo[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				IF NOT IS_PED_PERFORMING_TASK(tempPed, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
				AND NOT IS_PED_PERFORMING_TASK(tempPed, SCRIPT_TASK_AIM_GUN_AT_ENTITY)
					IF CAN_PED_TASKS_BE_CLEARED(tempPed)
					AND NOT IS_PED_RAGDOLL(tempPed)
					AND NOT IS_PED_GETTING_UP(tempPed)
					AND NOT IS_PED_FLEEING(tempPed)
						CLEAR_PED_TASKS(tempPed)
					ENDIF
				ENDIF
				EXIT
			ENDIF
		ENDIF
		
		//Functionality for non-navmesh movement, not needed in the end but could be useful if needed in future:
		IF MC_serverBD_2.iTargetID[iped] = ciINITIAL_GOTO
		AND (MC_serverBD_2.iPedGotoProgress[iped] < MAX_ASSOCIATED_GOTO_TASKS)
		AND IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed] , PED_ASSOCIATED_GOTO_TASK_TYPE_IGNORE_NAVMESH)
		AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPed_BSEight_NeverLeaveVehicle)
			IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_GO_STRAIGHT_TO_COORD)
			OR IS_PED_GOTO_TASK_PROGRESS_OUT_OF_DATE(iped)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,TRUE)
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_AssActionWalk)
					fOnFootSpeed = PEDMOVEBLENDRATIO_WALK
				ELSE
					fOnFootSpeed = PEDMOVEBLENDRATIO_RUN
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetThree,ciPED_BSThree_SprintTaskOnAgro)
				AND IS_BIT_SET(MC_serverBD.iPedAggroedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					fOnFootSpeed = PEDMOVEBLENDRATIO_SPRINT
				ENDIF
				
				TASK_GO_STRAIGHT_TO_COORD(tempPed,Vcoords,fOnFootSpeed,DEFAULT,GET_HEADING_FROM_COORDS_LA(GET_ENTITY_COORDS(tempPed), vCoords),0.3)
				SET_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				
				UPDATE_PED_TASKED_GOTO_PROGRESS(iped)
			ENDIF
			
		ELSE
		
			IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS)
			OR IS_PED_GOTO_TASK_PROGRESS_OUT_OF_DATE(iped)
				IF GET_NAVMESH_ROUTE_RESULT(tempPed) = NAVMESHROUTE_ROUTE_NOT_FOUND
					//NET_PRINT(" calling TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS starting combat timer: ") NET_PRINT_INT(iped) NET_NL()
					REINIT_NET_TIMER(tdPathFailCombat[iped])
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_BecomeAggressiveOnGotoPathFail)
					AND (GET_PED_COMBAT_STYLE(iped) = ciPED_DEFENSIVE)
					AND (GET_PED_COMBAT_MOVEMENT(tempPed) = CM_DEFENSIVE)
						PRINTLN("[RCC MISSION] - Ped ",iped," goto path failed, setting as aggressive due to creator setting")
						SET_PED_COMBAT_MOVEMENT(tempPed, CM_WILLADVANCE)
						SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_CAN_USE_FRUSTRATED_ADVANCE,TRUE)
						SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_CAN_CHARGE,TRUE)
						SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_PERMIT_CHARGE_BEYOND_DEFENSIVE_AREA, TRUE)
						REMOVE_PED_DEFENSIVE_AREA(tempPed)
					ENDIF
				ENDIF
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdPathFailCombat[iped]) > 30000
					
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,TRUE)
					
					SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE_ATTACK,FALSE)
					SET_PED_CONFIG_FLAG(tempPed, PCF_GetOutBurningVehicle, FALSE)
					SET_PED_CONFIG_FLAG(tempPed, PCF_GetOutUndriveableVehicle, FALSE)
					
					IF (NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetSix, ciPED_BSSix_NeverShootOnGoto))
					AND (IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
						OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset,ciPED_BS_PedAttackWhileGoto))
						fshootrange = 100
					ELSE
						fshootrange = -1
					ENDIF
					
					//TASK_GO_TO_COORD_ANY_MEANS(tempPed,Vcoords,PEDMOVEBLENDRATIO_RUN,tempVeh,FALSE,DRIVINGMODE_AVOIDCARS |DF_DriveIntoOncomingTraffic |DF_UseSwitchedOffNodes,fshootrange)
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_AssActionWalk)
						fOnFootSpeed = PEDMOVEBLENDRATIO_WALK
					ELSE
						fOnFootSpeed = PEDMOVEBLENDRATIO_RUN
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetThree,ciPED_BSThree_SprintTaskOnAgro)
					AND IS_BIT_SET(MC_serverBD.iPedAggroedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
						fOnFootSpeed = PEDMOVEBLENDRATIO_SPRINT
					ENDIF
					
					//IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_GoToOnFoot)
					OR IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iped]-1, PED_ASSOCIATED_GOTO_TASK_TYPE_EXIT_VEHICLE)
						PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS on foot only - calling TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS ciPED_BS_GoToOnFoot for ped: ",iped," to coords: ",Vcoords, " fshootrange = ", fshootrange)
						SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE, FALSE)
						INT iWarpTime
						
						IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iWarpTimer != -1
							iWarpTime = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iWarpTimer
						ELSE
							iWarpTime = -1 
						ENDIF
						
						DRIVINGMODE dmFlags = DRIVINGMODE_AVOIDCARS | DF_UseSwitchedOffNodes
						
						IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwelve, ciPed_BSTwelve_DisableDriveIntoOncomingTraffic)
							dmFlags = dmFlags | DF_DriveIntoOncomingTraffic
							PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS added DriveIntoOncomingTraffic in driving mode.")
						ENDIF
						
						TASK_GO_TO_COORD_ANY_MEANS_FLAGS tgcamFlags = TGCAM_DEFAULT
						
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetSix, ciPED_BSSix_DontAbandonVehicleOnGoto_IfMoving)
							tgcamFlags = tgcamFlags | TGCAM_NEVER_ABANDON_VEHICLE_IF_MOVING
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPed_BSEight_NeverLeaveVehicle)
							tgcamFlags = tgcamFlags | TGCAM_NEVER_ABANDON_VEHICLE
						ENDIF
						
						PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - Ped ", iped, " warp timer g_FMMC_STRUCT_ENTITIES.sPlacedPed[",iped,"].iWarpTimer is ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iWarpTimer)
						
						IF fshootrange > -1
							SET_PED_CONFIG_FLAG(tempPed, PCF_BroadcastRepondedToThreatWhenGoingToPointShooting,TRUE)
							tgcamFlags = tgcamFlags | TGCAM_USE_AI_TARGETING_FOR_THREATS
							PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS on foot only - calling TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS Set PCF_BroadcastRepondedToThreatWhenGoingToPointShooting (3) for ped: ",iped, " fshootrange = ", fshootrange)
						ENDIF 
						
						IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedGotoHeightOffset != -101
						AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedGoToHeightOffsetAtHealth != -1
							FLOAT fPercentageThreshhold = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedGoToHeightOffsetAtHealth)
							IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle > -1
								IF GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(tempVeh, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle, MC_serverBD.iTotalNumStartingPlayers) < fPercentageThreshhold
									Vcoords.z += g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedGotoHeightOffset
									PRINTLN("[LM][TASK_GO_TO_COORD_ANY_MEANS_etc][iPedGotoHeightOffset] - Applying Height Override (3) - GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE: ", GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(tempVeh, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle, MC_serverBD.iTotalNumStartingPlayers), " fPercentageThreshhold: ", fPercentageThreshhold, " iVehicle: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle)
								ENDIF
							ENDIF
						ENDIF
							
						TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS(tempPed,Vcoords,fOnFootSpeed,NULL,FALSE,dmFlags,fshootrange,100000,10,tgcamFlags,TO_FLOAT(iWarpTime))
						SET_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						
						UPDATE_PED_TASKED_GOTO_PROGRESS(iped)
						
						IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn = PLAYER_TWO
							IF IS_ENTITY_PLAYING_ANIM(tempPed,"ANIM@HEISTS@NARCOTICS@FINALE@IG_4_CAR_INTRO","TREVOR_OUTRO")
								FORCE_PED_MOTION_STATE(tempPed, MS_ON_FOOT_RUN)
								PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - Calling FORCE_PED_MOTION_STATE for ped: ", iped)
							ENDIF
						ENDIF
						
						DEBUG_PRINTCALLSTACK()
					ELSE
						IF binveh
							//if ped is in vehicle but not the driver, wait until vehicle slows down before tasking,
							//otherwise ped exits the car with dead driver
							IF GET_ENTITY_SPEED(tempPed) < 2.0
								SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE, TRUE)
								
								PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS not driving, but in veh - calling TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS for ped: ",iped," in vehicle to coords: ",Vcoords," blongrange ",bLongRange)
								DRIVINGMODE dmFlags = DRIVINGMODE_AVOIDCARS | DF_UseSwitchedOffNodes
								
								IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwelve, ciPed_BSTwelve_DisableDriveIntoOncomingTraffic)
									dmFlags = dmFlags | DF_DriveIntoOncomingTraffic
									PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS added DriveIntoOncomingTraffic in driving mode.")
								ENDIF
							
								TASK_GO_TO_COORD_ANY_MEANS_FLAGS tgcamFlags = TGCAM_IGNORE_VEHICLE_HEALTH | TGCAM_CONSIDER_ALL_NEARBY_VEHICLES | TGCAM_PROPER_IS_DRIVEABLE_CHECK
								
								IF NOT bGetOut
									tgcamFlags = tgcamFlags | TGCAM_REMAIN_IN_VEHICLE_AT_DESTINATION
								ENDIF
								
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetSix, ciPED_BSSix_DontAbandonVehicleOnGoto_IfMoving)
									tgcamFlags = tgcamFlags | TGCAM_NEVER_ABANDON_VEHICLE_IF_MOVING
								ENDIF
								
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPed_BSEight_NeverLeaveVehicle)
									tgcamFlags = tgcamFlags | TGCAM_NEVER_ABANDON_VEHICLE
								ENDIF
								
								IF fshootrange > -1
									SET_PED_CONFIG_FLAG(tempPed, PCF_BroadcastRepondedToThreatWhenGoingToPointShooting,TRUE)
									tgcamFlags = tgcamFlags | TGCAM_USE_AI_TARGETING_FOR_THREATS
									PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS on foot only - calling TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS Set PCF_BroadcastRepondedToThreatWhenGoingToPointShooting (4) for ped: ",iped, " fshootrange = ", fshootrange)
								ENDIF 
								
								IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedGotoHeightOffset != -101
								AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedGoToHeightOffsetAtHealth != -1
									FLOAT fPercentageThreshhold = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedGoToHeightOffsetAtHealth)
									
									IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle > -1
										IF GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(tempVeh, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle, MC_serverBD.iTotalNumStartingPlayers) < fPercentageThreshhold
											Vcoords.z += g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedGotoHeightOffset
											PRINTLN("[LM][TASK_GO_TO_COORD_ANY_MEANS_etc][iPedGotoHeightOffset] - Applying Height Override (4) - GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE: ", GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(tempVeh, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle, MC_serverBD.iTotalNumStartingPlayers), " fPercentageThreshhold: ", fPercentageThreshhold, " iVehicle: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle)
										ENDIF
									ENDIF
								ENDIF
									
								TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS(tempPed,Vcoords,fOnFootSpeed,tempVeh,bLongRange,dmFlags,fshootrange,0,10,tgcamFlags)
								SET_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
								
								UPDATE_PED_TASKED_GOTO_PROGRESS(iped)
							ENDIF
						ELSE
							IF NOT IS_VECTOR_ZERO(Vcoords)
								//task ped normally if on foot
								SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE, TRUE)
								
								PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS on foot normal - calling TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS for ped: ",iped," to coords: ",Vcoords," blongrange ",bLongRange)
								
								DRIVINGMODE dmFlags = DRIVINGMODE_AVOIDCARS | DF_UseSwitchedOffNodes
								
								IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwelve, ciPed_BSTwelve_DisableDriveIntoOncomingTraffic)
									dmFlags = dmFlags | DF_DriveIntoOncomingTraffic
									PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS added DriveIntoOncomingTraffic in driving mode.")
								ENDIF
							
								TASK_GO_TO_COORD_ANY_MEANS_FLAGS tgcamFlags = TGCAM_IGNORE_VEHICLE_HEALTH | TGCAM_CONSIDER_ALL_NEARBY_VEHICLES | TGCAM_PROPER_IS_DRIVEABLE_CHECK
								
								IF NOT bGetOut
									tgcamFlags = tgcamFlags | TGCAM_REMAIN_IN_VEHICLE_AT_DESTINATION
								ENDIF
								
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetSix, ciPED_BSSix_DontAbandonVehicleOnGoto_IfMoving)
									tgcamFlags = tgcamFlags | TGCAM_NEVER_ABANDON_VEHICLE_IF_MOVING
								ENDIF
								
								IF fshootrange > -1
									SET_PED_CONFIG_FLAG(tempPed, PCF_BroadcastRepondedToThreatWhenGoingToPointShooting,TRUE)
									tgcamFlags = tgcamFlags | TGCAM_USE_AI_TARGETING_FOR_THREATS
									PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS on foot only - calling TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS Set PCF_BroadcastRepondedToThreatWhenGoingToPointShooting (5) for ped: ",iped, " fshootrange = ", fshootrange)
								ENDIF
								
								IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedGotoHeightOffset != -101
								AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedGoToHeightOffsetAtHealth != -1
									FLOAT fPercentageThreshhold = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedGoToHeightOffsetAtHealth)
								
									IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle > -1
										IF GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(tempVeh, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle, MC_serverBD.iTotalNumStartingPlayers) < fPercentageThreshhold
											Vcoords.z += g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedGotoHeightOffset
											PRINTLN("[LM][TASK_GO_TO_COORD_ANY_MEANS_etc][iPedGotoHeightOffset] - Applying Height Override (5) - GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE: ", GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(tempVeh, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle, MC_serverBD.iTotalNumStartingPlayers), " fPercentageThreshhold: ", fPercentageThreshhold, " iVehicle: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle)
										ENDIF
									ENDIF
								ENDIF
								
								TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS(tempPed,Vcoords,fOnFootSpeed,tempVeh,bLongRange,dmFlags,fshootrange,0,10,tgcamFlags)
								SET_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
								
								UPDATE_PED_TASKED_GOTO_PROGRESS(iped)
							ELSE
								PRINTLN("[RCC MISSION] TASK_PED_GOTO_ANY_MEANS - NOT calling TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS for ped: ",iped," because coords are zero!")
								CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION] TASK_PED_GOTO_ANY_MEANS on foot normal - ped ",iped," tasked to go to the origin!")
							ENDIF
						ENDIF 
					ENDIF
				ELSE
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,FALSE)
					IF IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
						TASK_PED_GENERAL_COMBAT(tempPed,iped)
					ENDIF
				ENDIF
			ELSE
				IF GET_NAVMESH_ROUTE_RESULT(tempPed) = NAVMESHROUTE_ROUTE_NOT_FOUND
					REINIT_NET_TIMER(tdPathFailCombat[iped])
					//NET_PRINT(" calling TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS starting combat timer: ") NET_PRINT_INT(iped) NET_NL()
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_BecomeAggressiveOnGotoPathFail)
					AND (GET_PED_COMBAT_STYLE(iped) = ciPED_DEFENSIVE)
					AND (GET_PED_COMBAT_MOVEMENT(tempPed) = CM_DEFENSIVE)
						PRINTLN("[RCC MISSION] - Ped ",iped," goto path failed, setting as aggressive due to creator setting")
						SET_PED_COMBAT_MOVEMENT(tempPed, CM_WILLADVANCE)
						SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_CAN_USE_FRUSTRATED_ADVANCE,TRUE)
						SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_CAN_CHARGE,TRUE)
						SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_PERMIT_CHARGE_BEYOND_DEFENSIVE_AREA, TRUE)
						REMOVE_PED_DEFENSIVE_AREA(tempPed)
					ENDIF
				ENDIF
				
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdPathFailCombat[iped]) <= 30000
				AND NOT IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_FOUNDRY)
				
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,FALSE)
					
					IF IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
						IF NOT SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS, TRUE)
							CLEAR_PED_TASKS(tempPed)
						ENDIF
						TASK_PED_GENERAL_COMBAT(tempPed,iped)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
							
ENDPROC

FUNC VEHICLE_INDEX GET_ORIGINAL_VALID_VEHICLE(INT iped, PED_INDEX tempPed)
	
	VEHICLE_INDEX tempVeh = NULL
	
	INT iVeh = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle
	
	IF iVeh != -1
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
			VEHICLE_INDEX tempVeh2 = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
			
			IF IS_VEHICLE_DRIVEABLE(tempVeh2, TRUE) AND (NOT IS_ENTITY_ON_FIRE(tempVeh2))
				IF NOT IS_PED_IN_VEHICLE(tempPed, tempVeh2, TRUE)
					IF GET_DISTANCE_BETWEEN_ENTITIES(tempVeh2, tempPed) < 40.0
						IF NOT IS_ANY_PLAYER_IN_VEHICLE(tempVeh2)
							IF GET_ENTITY_SPEED(tempVeh2) < 5.0
								IF IS_VEHICLE_ON_ALL_WHEELS(tempVeh2) OR IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(tempVeh2))
									IF DOES_VEHICLE_HAVE_FREE_SEAT(tempVeh2)
										IF IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(tempVeh2))
											IF IS_ENTITY_IN_WATER(tempVeh2)
												tempVeh = tempVeh2
											ENDIF
										ELSE
											tempVeh = tempVeh2
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN tempVeh
	
ENDFUNC

PROC PROCESS_PLACED_PED_ROLLING_START(INT iPed, PED_INDEX tempPed)
	
	IF NOT IS_BIT_SET(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_RollingStart)
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iRollingStart > -1	
		IF NOT IS_PED_INJURED(tempPed)
		AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
		AND NETWORK_HAS_CONTROL_OF_ENTITY(tempPed)
		AND HAS_PED_HEAD_BLEND_FINISHED(tempPed)
			VECTOR vCoord = GET_ENTITY_COORDS(tempPed)
			vCoord = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoord, GET_ENTITY_HEADING(tempPed), <<0.0, 7.0, 0.0>>) // 7.0 needs to be default. Create an option for this.
			
			PED_MOTION_STATE ePedMotionState
			FLOAT fMoveBlendRatio
			SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iRollingStart
				CASE ciMS_ON_FOOT_WALK		fMoveBlendRatio = PEDMOVEBLENDRATIO_WALK	ePedMotionState = MS_ON_FOOT_WALK	BREAK
				CASE ciMS_ON_FOOT_RUN		fMoveBlendRatio = PEDMOVEBLENDRATIO_RUN		ePedMotionState = MS_ON_FOOT_RUN	BREAK
				CASE ciMS_ON_FOOT_SPRINT	fMoveBlendRatio = PEDMOVEBLENDRATIO_SPRINT	ePedMotionState = MS_ON_FOOT_SPRINT	BREAK
			ENDSWITCH
			
			#IF IS_DEBUG_BUILD
				VECTOR vOriginalPos = GET_ENTITY_COORDS(tempPed)
				PRINTLN("[LM][RCC MISSION][PROCESS_PLACED_PED_ROLLING_START] - iPed: ", iPed, " vOriginalPos: ", vOriginalPos, " vCoord: ", vCoord)
			#ENDIF
			
			GET_GROUND_Z_FOR_3D_COORD(vCoord, vCoord.z, FALSE, TRUE)
			
			PRINTLN("[LM][RCC MISSION][PROCESS_PLACED_PED_ROLLING_START] - iPed: ", iPed, " New vCoord ", vCoord)
			
			FREEZE_ENTITY_POSITION(tempPed, FALSE)
			
			SET_NETWORK_ID_CAN_MIGRATE(MC_serverBD_1.sFMMC_SBD.niPed[iPed], TRUE)
			
			CLEAR_PED_TASKS_IMMEDIATELY(tempPed)
			
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, TRUE)
			
			TASK_GO_STRAIGHT_TO_COORD(tempPed, vCoord, fMoveBlendRatio, -1)
			
			//TASK_GO_TO_COORD_ANY_MEANS(tempPed, vCoord, fMoveBlendRatio, NULL)
			FORCE_PED_MOTION_STATE(tempPed, ePedMotionState)
			
			FORCE_PED_AI_AND_ANIMATION_UPDATE(tempPed)
			
			SET_BIT(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_RollingStart)
			
			PRINTLN("[LM][RCC MISSION][PROCESS_PLACED_PED_ROLLING_START] - Performing Rolling Start with TASK_GO_STRAIGHT_TO_COORD for Ped: ", iPed)
		ENDIF
	ENDIF
ENDPROC

PROC TASK_PED_SPECIAL_SVM_GOTO(BOOL bDriver, PED_INDEX tempPed, VEHICLE_INDEX tempVeh, INT iPed, FLOAT fdrivespeed, VECTOR vCoords)
	IF bdriver
		
		PRINTLN("[TASK_PED_SPECIAL_SVM_GOTO] - iPed: ", iPed)
		PRINTLN("[TASK_PED_SPECIAL_SVM_GOTO] - Vcoords: ", Vcoords)

		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDriveSpeed !=-1
			fdrivespeed = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDriveSpeed)
		ELSE
			fdrivespeed = 30.0
		ENDIF
		
		FLOAT fdriveSpeedOverride = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedSpeedOverride)
			
		IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedSpeedRange != -1 OR SHOULD_PED_SPEED_UP_FROM_BEING_ALERTED(iPed, tempPed))
		AND fdriveSpeedOverride > -1
			PRINTLN("[TASK_PED_SPECIAL_SVM_GOTO] speed is to be overridden if a player is within ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedSpeedRange)
			IF CHECK_PLAYERS_IN_RANGE_OF_PED(iped, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedSpeedRange)
			OR SHOULD_PED_SPEED_UP_FROM_BEING_ALERTED(iPed, tempPed)
				IF NOT IS_BIT_SET(iPedLocalBoatSpeedSetEnteredOverrideRangeBoat[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					SET_BIT(iPedLocalBoatSpeedSetEnteredOverrideRangeBoat[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					SET_BIT(iPedLocalBoatSpeedSetOverrideSpeedBoat[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					PRINTLN("[TASK_PED_SPECIAL_SVM_GOTO] - Setting to initialize the Override speed.")
				ENDIF
			ELSE				
				IF IS_BIT_SET(iPedLocalBoatSpeedSetEnteredOverrideRangeBoat[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					CLEAR_BIT(iPedLocalBoatSpeedSetEnteredOverrideRangeBoat[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					SET_BIT(iPedLocalBoatSpeedSetRegularSpeedBoat[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					PRINTLN("[TASK_PED_SPECIAL_SVM_GOTO] - Setting to reinitialize the regular speed.")
				ENDIF
			ENDIF
		ENDIF
					
		IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE)		
		OR IS_BIT_SET(iPedLocalBoatSpeedSetOverrideSpeedBoat[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		OR IS_BIT_SET(iPedLocalBoatSpeedSetRegularSpeedBoat[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			PRINTLN("[TASK_PED_SPECIAL_SVM_GOTO] - Tasking iPed: ", iPed, " fDriveSpeed: ", fDriveSpeed," fdriveSpeedOverride: ", fdriveSpeedOverride)
			PRINTLN("[TASK_PED_SPECIAL_SVM_GOTO] - Regular: ", IS_BIT_SET(iPedLocalBoatSpeedSetRegularSpeedBoat[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed)), " Override set: ", IS_BIT_SET(iPedLocalBoatSpeedSetOverrideSpeedBoat[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed)))
			
			INIT_BOAT(tempVeh,-1,TRUE)
			
			SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE_ATTACK,FALSE)
			SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_DISABLE_BULLET_REACTIONS,FALSE)
			SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_DISABLE_REACT_TO_BUDDY_SHOT,FALSE)
			SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_DISABLE_FLEE_FROM_COMBAT,FALSE)
			
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,TRUE)
			
			IF IS_BIT_SET(iPedLocalBoatSpeedSetOverrideSpeedBoat[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			OR IS_BIT_SET(iPedLocalBoatSpeedSetRegularSpeedBoat[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				// Overrides and Resets.
				IF IS_BIT_SET(iPedLocalBoatSpeedSetOverrideSpeedBoat[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					
					TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE(tempPed, tempVeh, Vcoords, fdriveSpeedOverride, DRIVINGMODE_AVOIDCARS, 10)
					CLEAR_BIT(iPedLocalBoatSpeedSetOverrideSpeedBoat[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					SET_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					
				ELIF IS_BIT_SET(iPedLocalBoatSpeedSetRegularSpeedBoat[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					
					TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE(tempPed, tempVeh, Vcoords, fDriveSpeed, DRIVINGMODE_AVOIDCARS, 10)
					CLEAR_BIT(iPedLocalBoatSpeedSetRegularSpeedBoat[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					SET_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					
				ENDIF
			ELSE
				// Default
				TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE(tempPed, tempVeh, Vcoords, fDriveSpeed, DRIVINGMODE_AVOIDCARS, 10)
				SET_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			ENDIF
		ENDIF
			
		IF IS_PED_PERFORMING_TASK(tempPed, SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE)
			PRINTLN("[TASK_PED_SPECIAL_SVM_GOTO] - Performing SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE")
		ELSE
			PRINTLN("[TASK_PED_SPECIAL_SVM_GOTO] - Not Performing SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DOES_PED_IN_GOTO_TASK_HAVE_TO_WAIT(PED_INDEX tempPed, VEHICLE_INDEX tempVeh, INT iped, BOOL binveh, BOOL bHasControl)
	
	IF binveh

		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThirteen, ciPED_BSThirteen_BlockGoToTaskWhenVehicleNotFull)
			MODEL_NAMES mn = GET_ENTITY_MODEL(tempVeh)
			
			IF NOT ARE_ANY_VEHICLE_SEATS_FREE(tempVeh)
				RETURN FALSE
			ENDIF
						
			INT iPlayersMax = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]
			INT iPlayersChecked
			INT iPlayersAvailable = iPlayersMax
			INT iSeatsMax = GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(tempVeh)
			INT iSeatsTaken	= GET_VEHICLE_NUMBER_OF_PASSENGERS(tempVeh)
			INT iSeatsAvailable = iSeatsMax - iSeatsTaken
			INT iPlayersInThisVehicle
			
			IF iPlayersMax > 0
				PRINTLN("[LM][PedAiTaxi][DOES_PED_IN_GOTO_TASK_HAVE_TO_WAIT] - iPlayersMax: ", iPlayersMax, " 		iSeatsMax: ", iSeatsMax, "		iSeatsTaken: ", iSeatsTaken, "		iSeatsAvailable: ", iSeatsAvailable)
				
				INT i = 0 
				FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
					
					PARTICIPANT_INDEX piPart = INT_TO_PARTICIPANTINDEX(i)
					PLAYER_INDEX piPlayer
					PED_INDEX pedPlayer
					IF NETWORK_IS_PARTICIPANT_ACTIVE(piPart)
					AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_ANY_SPECTATOR) 
					AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet2, PBBOOL_HEIST_SPECTATOR)
						iPlayersChecked++
						
						piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)
						IF NETWORK_IS_PLAYER_ACTIVE(piPlayer)
							pedPlayer = GET_PLAYER_PED(piPlayer)
							
							IF IS_PED_IN_ANY_VEHICLE(pedPlayer, FALSE)
								VEHICLE_INDEX vehPlayerIsIn = GET_VEHICLE_PED_IS_IN(pedPlayer)
								PED_INDEX pedDriver = GET_PED_IN_VEHICLE_SEAT(vehPlayerIsIn, VS_DRIVER, true)
								IF NOT IS_PED_INJURED(pedDriver)
									INT iTempPed = IS_ENTITY_A_MISSION_CREATOR_ENTITY(pedDriver)
									IF iTempPed > -1
									AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iTempPed].iPedBitsetThirteen, ciPED_BSThirteen_BlockGoToTaskWhenVehicleNotFull)	
										iPlayersAvailable--
									ENDIF
								ENDIF
							ENDIF
							
							IF IS_PED_IN_THIS_VEHICLE(pedPlayer, tempVeh, FALSE)
								iPlayersInThisVehicle++
							ENDIF
						ENDIF
					ENDIF
					IF iPlayersChecked > iPlayersMax
						BREAKLOOP
					ENDIF
				ENDFOR
				
				//PRINTLN("[LM][PedAiTaxi][DOES_PED_IN_GOTO_TASK_HAVE_TO_WAIT] - iPlayersAvailable: ", iPlayersAvailable, " 		iPlayersChecked: ", iPlayersChecked, " iPlayersInThisVehicle: ", iPlayersInThisVehicle)
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThirteen, ciPED_BSThirteen_DontBlockGoToTaskIfVehicleCantBeFull)
					IF iPlayersAvailable = 0
					AND iPlayersInThisVehicle > 0
						PRINTLN("[LM][PedAiTaxi][DOES_PED_IN_GOTO_TASK_HAVE_TO_WAIT][RETURN_FALSE] - Returning False. No players available.")
						RETURN FALSE
					ENDIF
				ENDIF
				IF iSeatsAvailable > 0
					IF bHasControl
						IF IS_PED_PERFORMING_TASK(tempPed, SCRIPT_TASK_GO_TO_ENTITY)
						OR IS_PED_PERFORMING_TASK(tempPed, SCRIPT_TASK_GO_TO_COORD_ANY_MEANS)
						OR IS_PED_PERFORMING_TASK(tempPed, SCRIPT_TASK_GO_TO_COORD_WHILE_SHOOTING)
						OR IS_PED_PERFORMING_TASK(tempPed, SCRIPT_TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS_WITH_CRUISE_SPEED)
						OR IS_PED_PERFORMING_TASK(tempPed, SCRIPT_TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS)
						OR IS_PED_PERFORMING_TASK(tempPed, SCRIPT_TASK_GO_TO_ENTITY)
						
							PRINTLN("[LM][PedAiTaxi][DOES_PED_IN_GOTO_TASK_HAVE_TO_WAIT] - Clearing GO TO task NOW.")
							CLEAR_PED_TASKS(tempPed)
							
							IF IS_THIS_MODEL_A_HELI(mn)
								VECTOR vPos = GET_ENTITY_COORDS(tempVeh)
								FLOAT fGround
								GET_GROUND_Z_FOR_3D_COORD(vPos, fGround, TRUE)
								vPos.z = fGround
								
								TASK_HELI_MISSION(tempPed, tempVeh, NULL, NULL, vPos, MISSION_LAND, 25.0, 0.5, GET_ENTITY_HEADING(tempVeh), 0, 0, -1)
								PRINTLN("[LM][PedAiTaxi][DOES_PED_IN_GOTO_TASK_HAVE_TO_WAIT] - Tasking Heli Mission NOW.")
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFourteen, ciPED_BSFourteen_MissionTaxiUseCustomWaitText)
					AND IS_PED_IN_THIS_VEHICLE(LocalPlayerPed, tempVeh)
						SET_BIT(iLocalBoolCheck32, LBOOL32_SHOULD_USE_CUSTOM_HELP_WAIT_TEXT_TAXI)
					ENDIF
					
					RETURN TRUE
				ENDIF
			ELSE
				PRINTLN("[LM][PedAiTaxi][DOES_PED_IN_GOTO_TASK_HAVE_TO_WAIT] - Returning TRUE. Too early in processing to calculate player counts.")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	//PRINTLN("[LM][PedAiTaxi][DOES_PED_IN_GOTO_TASK_HAVE_TO_WAIT][RETURN_FALSE] - Returning False. Default.")
	
	RETURN FALSE
ENDFUNC

PROC TASK_PED_GENERAL_GOTO(PED_INDEX tempPed, INT iped,VECTOR Vcoords,BOOL binveh, FLOAT fDist, FLOAT ftargetspeed, BOOL bHeli,BOOL bPlane, BOOL bBoat, PED_INDEX targetPed,BOOL bDriver, VEHICLE_INDEX tempVeh = NULL, BOOL bGetOut = TRUE, FLOAT fGotorange = 8.0)
	
	UNUSED_PARAMETER(fGotoRange)
	VEHICLE_INDEX temp_veh
	FLOAT fdrivespeed = 30.0
	
	LOAD_COLLISION_ON_PED_IF_POSSIBLE(tempPed,iped)
	
	#IF IS_DEBUG_BUILD
		IF bLMhelpfulScriptAIPrints
			PRINTLN("[LM][RCC MISSION] - TASK_PED_GENERAL_GOTO - iPed: ", iPed, " bGetOut: ", bGetOut, " binVeh: ", binVeh, " bDriver: ", bDriver, " SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(tempPed))
		ENDIF
	#ENDIF
	
	IF binveh
		
		IF NOT IS_BIT_SET(MC_serverBD.iPedSecondaryTaskActive[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			IF DOES_PED_IN_GOTO_TASK_HAVE_TO_WAIT(tempPed, tempVeh, iped, binveh, TRUE)
				PRINTLN("[LM][RCC MISSION] - TASK_PED_GENERAL_GOTO - DOES_PED_IN_GOTO_TASK_HAVE_TO_WAIT is true, exitting from task function.")
				EXIT
			ENDIF
		ENDIF
		
		IF bboat
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].ipedBitsetEight, ciPed_BSEight_ForceTaskMissionBoat)
				IF bdriver
					CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - Calling TASK_PED_SPECIAL_SVM_GOTO... iped: ", iped)
					TASK_PED_SPECIAL_SVM_GOTO(bDriver, tempPed, tempVeh, iPed, fDriveSpeed, vCoords)
				ENDIF
			ELIF NOT IS_PED_INJURED(targetPed)
				IF bdriver
					IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_VEHICLE_MISSION, FALSE, MISSION_GOTO, tempVeh)
						INIT_BOAT(tempVeh,-1,TRUE)
						IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDriveSpeed !=-1
							fdrivespeed = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDriveSpeed)
						ELSE
							fdrivespeed = 30.0
						ENDIF
						SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE_ATTACK,FALSE)
						
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,TRUE)
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].ipedBitsetEight, ciPED_BSEight_GoStraightToTargetBoat)
							TASK_BOAT_MISSION(tempPed,tempVeh,NULL,targetPed,Vcoords,MISSION_GOTO,fdrivespeed,DRIVINGMODE_AVOIDCARS,10,BCF_NeverRoute | BCF_NeverNavMesh)
						ELSE
							TASK_BOAT_MISSION(tempPed,tempVeh,NULL,targetPed,Vcoords,MISSION_GOTO,fdrivespeed,DRIVINGMODE_AVOIDCARS,10, BCF_OPENOCEANSETTINGS)
						ENDIF
						CLEAR_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						
						PRINTLN("[RCC MISSION] TASK_PED_GENERAL_GOTO - calling TASK_BOAT_MISSION on ped ", iped)
						CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - Vcoords = ", Vcoords)
						CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - binveh = ", binveh)
						CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - fDist = ", fDist)
						CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - ftargetspeed = ", ftargetspeed)
						CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - bHeli = ", bHeli)
						CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - bPlane = ", bPlane)
						CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - bBoat = ", bBoat)
						CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - bDriver = ", bDriver)
						CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - fdrivespeed = ", fdrivespeed)
						CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - bGetOut = ", bGetOut)
						CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - Go straight to target = ", IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].ipedBitsetEight, ciPED_BSEight_GoStraightToTargetBoat))
						CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - Force Task Mission Boat = ", IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].ipedBitsetEight, ciPed_BSEight_ForceTaskMissionBoat))
						CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - callstack: ")
						
						//DRAW_ANGLED_AREA(<<-3747, 8021, -10>>, <<1159, 3012, 50>>, 500, 255,255,255,255)
						DEBUG_PRINTCALLSTACK()
					ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					IF bLMhelpfulScriptAIPrints
						PRINTLN("[LM][RCC MISSION] - TASK_PED_GENERAL_GOTO - bboat iPed: ", iPed, " Calling TASK_PED_GOTO_ANY_MEANS")
					ENDIF
				#ENDIF
				TASK_PED_GOTO_ANY_MEANS(tempPed,Vcoords,fDist,iped,tempVeh,binveh,bdriver,FALSE,bHeli,bPlane,bboat,bGetOut)
			ENDIF
		ELIF bHeli
			IF NOT IS_BIT_SET(MC_serverBD.iPedShouldFleeDropOff[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				IF IS_VEHICLE_DRIVEABLE(tempVeh, TRUE)
					IF NOT IS_PED_INJURED(targetPed)
						IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_VEHICLE_MISSION, FALSE, MISSION_PROTECT, tempVeh)
							INIT_PLANE(tempVeh,-1,TRUE)
							IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDriveSpeed !=-1
								fdrivespeed = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDriveSpeed)
							ELSE
								fdrivespeed = 60.0
							ENDIF
							SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE_ATTACK,FALSE)
							
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,TRUE)
							
							INT iFlightHeight = 35
							INT iMinFlightHeight = 35
							
							TASK_HELI_MISSION(tempPed, tempVeh, NULL, targetPed, Vcoords, MISSION_PROTECT, fdrivespeed, 25, -1.0, iFlightHeight, iMinFlightHeight)
							CLEAR_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))

							PRINTLN("[RCC MISSION] TASK_PED_GENERAL_GOTO - calling TASK_HELI_MISSION (MISSION_PROTECT) on ped ", iped)
							CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - Vcoords = ", Vcoords)
							CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - binveh = ", binveh)
							CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - fDist = ", fDist)
							CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - ftargetspeed = ", ftargetspeed)
							CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - bHeli = ", bHeli)
							CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - bPlane = ", bPlane)
							CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - bBoat = ", bBoat)
							CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - bDriver = ", bDriver)
							CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - fdrivespeed = ", fdrivespeed)
							CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - bGetOut = ", bGetOut)
							CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - callstack: ")
							DEBUG_PRINTCALLSTACK()
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							IF bLMhelpfulScriptAIPrints
								PRINTLN("[LM][RCC MISSION] - TASK_PED_GENERAL_GOTO - bHeli iPed: ", iPed, " Calling TASK_PED_GOTO_ANY_MEANS")
							ENDIF
						#ENDIF
						TASK_PED_GOTO_ANY_MEANS(tempPed,Vcoords,fDist,iped,tempVeh,binveh,bdriver,FALSE,bHeli,bPlane,bboat,bGetOut)
					ENDIF
				else
					//If heli destroyed but ped is alive remove blocking events bug:2117563
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,false)
				endif
			ENDIF
		ELIF bPlane
		
			IF NOT IS_PED_INJURED(targetPed)
				IF bdriver
					IF IS_ENTITY_IN_AIR(tempVeh)
						IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLANE_CHASE)
							INIT_PLANE(tempVeh,-1,TRUE)
							SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE_ATTACK,FALSE)
							
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,TRUE)
							
							//TASK_VEHICLE_MISSION_PED_TARGET(tempPed,tempVeh,targetPed,MISSION_FOLLOW,60,DRIVINGMODE_AVOIDCARS |DF_DriveIntoOncomingTraffic |DF_UseSwitchedOffNodes,10,10)
							//TASK_VEHICLE_CHASE(tempPed,targetPed)
							TASK_PLANE_CHASE(tempPed, targetPed, <<0,-20, 20>>)
							CLEAR_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							
							IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDriveSpeed !=-1
								fdrivespeed = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDriveSpeed)
							ELSE
								fdrivespeed = 30.0
							ENDIF
							SET_DRIVE_TASK_CRUISE_SPEED(tempPed,fdrivespeed)

							PRINTLN("[RCC MISSION] TASK_PED_GENERAL_GOTO - calling TASK_PLANE_CHASE on ped ", iped)
							CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - Vcoords = ", Vcoords)
							CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - binveh = ", binveh)
							CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - fDist = ", fDist)
							CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - ftargetspeed = ", ftargetspeed)
							CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - bHeli = ", bHeli)
							CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - bPlane = ", bPlane)
							CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - bBoat = ", bBoat)
							CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - bDriver = ", bDriver)
							CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - fdrivespeed = ", fdrivespeed)
							CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - bGetOut = ", bGetOut)
							CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - callstack: ")
							DEBUG_PRINTCALLSTACK()
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							IF bLMhelpfulScriptAIPrints
								PRINTLN("[LM][RCC MISSION] - TASK_PED_GENERAL_GOTO - bPlane1 iPed: ", iPed, " Calling TASK_PED_GOTO_ANY_MEANS")
							ENDIF
						#ENDIF
						TASK_PED_GOTO_ANY_MEANS(tempPed,Vcoords,fDist,iped,tempVeh,binveh,bdriver,FALSE,bHeli,bPlane,bboat,bGetOut)
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						IF bLMhelpfulScriptAIPrints
							PRINTLN("[LM][RCC MISSION] - TASK_PED_GENERAL_GOTO - bPlane2 iPed: ", iPed, " Calling TASK_PED_GOTO_ANY_MEANS")
						ENDIF
					#ENDIF
					TASK_PED_GOTO_ANY_MEANS(tempPed,Vcoords,fDist,iped,tempVeh,binveh,bdriver,FALSE,bHeli,bPlane,bboat,bGetOut)
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					IF bLMhelpfulScriptAIPrints
						PRINTLN("[LM][RCC MISSION] - TASK_PED_GENERAL_GOTO - bPlane3 iPed: ", iPed, " Calling TASK_PED_GOTO_ANY_MEANS")
					ENDIF
				#ENDIF
				TASK_PED_GOTO_ANY_MEANS(tempPed,Vcoords,fDist,iped,tempVeh,binveh,bdriver,FALSE,bHeli,bPlane,bboat,bGetOut)
			ENDIF
		ELSE
			IF bDriver
			AND binveh
			AND NOT IS_PED_INJURED(targetPed)
			AND IS_PED_IN_ANY_VEHICLE(tempPed, FALSE)
				SET_DRIVE_TASK_MAX_CRUISE_SPEED(tempPed, cfMAX_SPEED_PED_VEH)
			ENDIF
			IF fDist >=300.0
				#IF IS_DEBUG_BUILD
					IF bLMhelpfulScriptAIPrints
						PRINTLN("[LM][RCC MISSION] - TASK_PED_GENERAL_GOTO - bOther1 iPed: ", iPed, " Calling TASK_PED_GOTO_ANY_MEANS")
					ENDIF
				#ENDIF
				TASK_PED_GOTO_ANY_MEANS(tempPed,Vcoords,fDist,iped,tempVeh,binveh,bdriver,TRUE,DEFAULT,DEFAULT,DEFAULT,bGetOut)
				CLEAR_BIT(iGotoObjectBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			ELSE
				IF NOT IS_PED_INJURED(targetPed)
					IF NOT IS_ANY_PED_ENTERING_VEHICLE(tempveh)
						FLOAT spookedDistance = 40.0
						IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedEscortRange = 0
							IF fDist < 10
								fdrivespeed = GET_ENTITY_SPEED(targetPed)
							ELIF fDist < 20
								fdrivespeed = GET_ENTITY_SPEED(targetPed) + 5
							ELIF fDist < 30
								fdrivespeed = GET_ENTITY_SPEED(targetPed) + 10
							ELSE
								fdrivespeed = MAX_VALUE(GET_ENTITY_SPEED(targetPed) + 20,  TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDriveSpeed))
							ENDIF
							spookedDistance = 40.0
						ELSE
							IF fDist < g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedEscortRange
								fdrivespeed = GET_ENTITY_SPEED(targetPed)
							ELIF fDist < (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedEscortRange+10)
								fdrivespeed = GET_ENTITY_SPEED(targetPed) + 5
							ELIF fDist < (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedEscortRange+20)
								fdrivespeed = GET_ENTITY_SPEED(targetPed) + 10
							ELSE
								fdrivespeed = MAX_VALUE(GET_ENTITY_SPEED(targetPed) + 20,  TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDriveSpeed))
							ENDIF
							spookedDistance = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedEscortRange + 30.0
						ENDIF
						IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_VEHICLE_MISSION, FALSE, MISSION_GOTO, tempVeh)
							IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdparkTimer[iped]) > 2000
								
								IF NOT IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
								AND fDist < spookedDistance
									TASK_VEHICLE_MISSION_PED_TARGET(tempPed,tempVeh,targetPed,MISSION_GOTO,fdrivespeed,DRIVINGMODE_STOPFORCARS |DF_DriveIntoOncomingTraffic |DF_UseSwitchedOffNodes,8,10)
									PRINTLN("[RCC MISSION] TASK FOLLOW not spooked for ped: ",iped)
								ELSE
									//TASK_VEHICLE_MISSION_PED_TARGET(tempPed,tempVeh,targetPed,MISSION_GOTO,30.0,DRIVINGMODE_AVOIDCARS |DF_DriveIntoOncomingTraffic |DF_UseSwitchedOffNodes,10,10)
									TASK_VEHICLE_MISSION_PED_TARGET(tempPed,tempVeh,targetPed,MISSION_GOTO,fdrivespeed,DF_SwerveAroundAllCars|DF_SteerAroundStationaryCars|DF_SteerAroundObjects|DF_SteerAroundPeds|DF_UseShortCutLinks|DF_ChangeLanesAroundObstructions|DF_DriveIntoOncomingTraffic|DF_UseSwitchedOffNodes|DF_AdjustCruiseSpeedBasedOnRoadSpeed,8,10)
									PRINTLN("[RCC MISSION] TASK FOLLOW spooked for ped: ",iped)
								ENDIF
								CLEAR_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
								
								PRINTLN("[RCC MISSION] TASK_PED_GENERAL_GOTO - calling TASK_VEHICLE_MISSION_PED_TARGET (MISSION_GOTO) on ped ", iped)
								
								CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - Vcoords = ", Vcoords)
								CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - binveh = ", binveh)
								CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - fDist = ", fDist)
								CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - ftargetspeed = ", ftargetspeed)
								CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - bHeli = ", bHeli)
								CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - bPlane = ", bPlane)
								CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - bBoat = ", bBoat)
								CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - bDriver = ", bDriver)
								CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - fdrivespeed = ", fdrivespeed)
								CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - bGetOut = ", bGetOut)
								CDEBUG3LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_GENERAL_GOTO - callstack: ")
								DEBUG_PRINTCALLSTACK()
								
							ENDIF
						ELSE
							SET_DRIVE_TASK_CRUISE_SPEED(tempPed,fdrivespeed)
						ENDIF
						SET_BIT(iGotoObjectBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					ELSE
						IF bDriver
							#IF IS_DEBUG_BUILD
								IF bLMhelpfulScriptAIPrints
									PRINTLN("[LM][RCC MISSION] - TASK_PED_GENERAL_GOTO - bOther2 iPed: ", iPed, " Calling TASK_WAIT_FOR_PASSENGERS")
								ENDIF
							#ENDIF
							TASK_WAIT_FOR_PASSENGERS(tempPed,tempveh,iped)
						ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						IF bLMhelpfulScriptAIPrints
							PRINTLN("[LM][RCC MISSION] - TASK_PED_GENERAL_GOTO - bOther2 iPed: ", iPed, " Calling TASK_PED_GOTO_ANY_MEANS")
						ENDIF
					#ENDIF
					TASK_PED_GOTO_ANY_MEANS(tempPed,Vcoords,fDist,iped,tempVeh,binveh,bdriver,fDist >=300.0,DEFAULT,DEFAULT,DEFAULT,bGetOut)
					CLEAR_BIT(iGotoObjectBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF bDriver
		AND NOT IS_PED_INJURED(targetPed)
			SET_DRIVE_TASK_MAX_CRUISE_SPEED(tempPed, cfMAX_SPEED_PED_VEH)
		ENDIF
		
		VEHICLE_INDEX viVeh = NULL
		IF IS_ASSOCIATED_GOTO_ENTER_VEH_VALID(iped, MC_serverBD_2.iPedGotoProgress[iped], viVeh)
		AND NOT IS_PED_IN_VEHICLE(tempPed, viVeh, FALSE)
			IF NOT IS_PED_IN_VEHICLE(tempPed, viVeh, TRUE)
				PRINTLN("[LM][RCC MISSION] TASK_PED_GENERAL_GOTO - iPed: ", iPed, " TASK_PED_ENTER_VEH.")
				TASK_PED_ENTER_VEH(tempPed, iped, viVeh, 9999999)
			ENDIF
		ELSE		
			IF fDist >=100.0
			OR (fTargetSpeed > 8.0 AND fDist >=40.0)
				temp_veh = GET_ORIGINAL_VALID_VEHICLE(iped, tempPed)
				IF IS_VEHICLE_DRIVEABLE(temp_veh, TRUE)
				AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_GoToOnFoot)
				AND NOT IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iped]-1, PED_ASSOCIATED_GOTO_TASK_TYPE_EXIT_VEHICLE)
					IF NOT IS_PED_IN_VEHICLE(tempPed,temp_veh,TRUE)
						#IF IS_DEBUG_BUILD
							IF bLMhelpfulScriptAIPrints
								PRINTLN("[LM][RCC MISSION] - TASK_PED_GENERAL_GOTO - bOther iPed: ", iPed, " Calling TASK_PED_ENTER_VEH")
							ENDIF
						#ENDIF
						TASK_PED_ENTER_VEH(tempPed,iped,temp_veh)
					ENDIF
				ELSE
					IF NOT IS_PED_IN_ANY_VEHICLE(tempPed,TRUE)
						#IF IS_DEBUG_BUILD
							IF bLMhelpfulScriptAIPrints
								PRINTLN("[LM][RCC MISSION] - TASK_PED_GENERAL_GOTO - bOther3 iPed: ", iPed, " Calling TASK_PED_GOTO_ANY_MEANS")
							ENDIF
						#ENDIF
						TASK_PED_GOTO_ANY_MEANS(tempPed,Vcoords,fDist,iped,tempVeh,binveh,bdriver,DEFAULT,DEFAULT,DEFAULT,DEFAULT,bGetOut)
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_PED_IN_ANY_VEHICLE(tempPed,TRUE)
					#IF IS_DEBUG_BUILD
						IF bLMhelpfulScriptAIPrints
							PRINTLN("[LM][RCC MISSION] - TASK_PED_GENERAL_GOTO - bOther4 iPed: ", iPed, " Calling TASK_PED_GOTO_ANY_MEANS")
						ENDIF
					#ENDIF
					TASK_PED_GOTO_ANY_MEANS(tempPed,Vcoords,fDist,iped,tempVeh,binveh,bdriver,DEFAULT,DEFAULT,DEFAULT,DEFAULT,bGetOut)
				ENDIF
			ENDIF
		ENDIF
		CLEAR_BIT(iGotoObjectBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
	ENDIF

ENDPROC

PROC TASK_PED_DEFENSIVE_AREA_GOTO(PED_INDEX tempPed, INT iped, VECTOR vCoords, FLOAT fGotoRange)
	
	IF (NOT IS_PED_DEFENSIVE_AREA_ACTIVE(tempPed, FALSE))
	OR (NOT ARE_VECTORS_ALMOST_EQUAL(GET_PED_DEFENSIVE_AREA_POSITION(tempped), vCoords))
	OR (GET_PED_COMBAT_MOVEMENT(tempPed) != CM_DEFENSIVE)
		
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, FALSE)
		
		IF fGotoRange > 0.75
			SET_PED_SPHERE_DEFENSIVE_AREA(tempPed, vCoords, fGotoRange, TRUE)
		ELSE
			SET_PED_SPHERE_DEFENSIVE_AREA(tempPed, vCoords, 0.75, TRUE)
			PRINTLN("[RCC MISSION] SET_PED_SPHERE_DEFENSIVE_AREA set to low capping to 0.75 ")
		ENDIF
		
		SET_PED_COMBAT_MOVEMENT(tempPed, CM_DEFENSIVE)
		
		/*FLOAT fOnFootSpeed = PEDMOVEBLENDRATIO_RUN
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_AssActionWalk)
			fOnFootSpeed = PEDMOVEBLENDRATIO_WALK
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetThree,ciPED_BSThree_SprintTaskOnAgro)
		AND IS_BIT_SET(MC_serverBD.iPedAggroedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			fOnFootSpeed = PEDMOVEBLENDRATIO_SPRINT
		ENDIF
		
		SET_PED_MAX_MOVE_BLEND_RATIO(tempPed, fOnFootSpeed)*/ //-this needs to be called every frame for it to work
		
		SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_CAN_USE_DYNAMIC_STRAFE_DECISIONS, TRUE)
		
		PRINTLN("[RCC MISSION] TASK_PED_DEFENSIVE_AREA_GOTO - Set ped ",iped," with defensive area at ",vCoords,", size ",fGotoRange)
		
	ENDIF
	
	IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED)
	AND NOT IS_PED_IN_COMBAT(tempPed)
		IF (NOT IS_PED_A_HIGH_PRIORITY_PED(iped))
		OR IS_PED_UNFRIENDLY_TO_ANY_TEAM(iped) // They don't like players, so they can combat them here - otherwise this'll be called forever, and never do anything
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetSix, ciPED_BSSix_HateAllOtherPedGroups) // The ped doesn't have a relationship setup with any other peds, so without this flag no combat will happen
			TASK_COMBAT_HATED_TARGETS_AROUND_PED(tempPed,299)
			CLEAR_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			PRINTLN("[RCC MISSION] TASK_PED_DEFENSIVE_AREA_GOTO - Calling TASK_COMBAT_HATED_TARGETS_AROUND_PED for ped ", iped)
		ENDIF
	ENDIF
	
ENDPROC

PROC TASK_PED_SHORT_RANGE_GOTO(PED_INDEX tempPed, INT iped, VECTOR Vcoords)

FLOAT fOnFootSpeed
	
	IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD)
		
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,TRUE)
		
		SET_CURRENT_PED_WEAPON(tempPed,GET_BEST_PED_WEAPON(tempPed),TRUE)
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_AssActionWalk)
			fOnFootSpeed = PEDMOVEBLENDRATIO_WALK
		ELSE
			fOnFootSpeed = PEDMOVEBLENDRATIO_RUN
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetThree,ciPED_BSThree_SprintTaskOnAgro)
		AND IS_BIT_SET(MC_serverBD.iPedAggroedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			fOnFootSpeed = PEDMOVEBLENDRATIO_SPRINT
		ENDIF
		
		IF iped > -1
			PRINTLN("[RCC MISSION] TASK_PED_SHORT_RANGE_GOTO - Calling TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD on ped ", iped)
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_SHORT_RANGE_GOTO - Vcoords = ", Vcoords)
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_SHORT_RANGE_GOTO - fOnFootSpeed = ", fOnFootSpeed)
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] - TASK_PED_SHORT_RANGE_GOTO - callstack: ")
			DEBUG_PRINTCALLSTACK()
		ENDIF
		
		TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(tempPed,Vcoords,Vcoords,fOnFootSpeed,TRUE)
		SET_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
		
	ENDIF

ENDPROC



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: CLIENT PED SPOOK / AGGRO LOGIC !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************



PROC RUN_AGGRO_PED_HUNTING_BODY(PED_INDEX tempPed, INT iPed)
	
	IF NOT (IS_PED_IN_COMBAT(tempPed) OR IS_PED_FLEEING(tempPed))
		
		//Maybe replace with part causing fail?
		PLAYER_INDEX ClosestPlayerID = INT_TO_PLAYERINDEX(GET_NEAREST_PLAYER_TO_ENTITY(tempPed))
		PED_INDEX ClosestPlayer = GET_PLAYER_PED(ClosestPlayerID)
		
		IF NOT IS_BIT_SET(iPedHeardBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			SET_BIT(iPedHeardBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			BROADCAST_FMMC_PED_HEARING(iped)
			SET_PED_HIGHLY_PERCEPTIVE(tempPed,TRUE)
			PRINTLN("[RCC MISSION] RUN_AGGRO_PED_HUNTING_BODY - Aggro ped has been set to highly perceptive - ",iped) 
			
			IF NOT IS_PED_INJURED(ClosestPlayer)
			AND NOT IS_PED_INJURED(tempPed)
				IF NOT (IS_TARGET_PED_IN_PERCEPTION_AREA(tempPed, ClosestPlayer) AND HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(iPed, tempPed, ClosestPlayer, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE))
					
					VECTOR vPlayerLoc = GET_ENTITY_COORDS(ClosestPlayer)
					
					PRINTLN("[RCC MISSION] RUN_AGGRO_PED_HUNTING_BODY - Aggro ped ",iped," can't see player, investigating disturbance at coords ",vPlayerLoc)
									
					CLEAR_SEQUENCE_TASK(temp_sequence)
					OPEN_SEQUENCE_TASK(temp_sequence)
						TASK_STAND_STILL(NULL,1000)
						TASK_TURN_PED_TO_FACE_COORD(NULL,vPlayerLoc,1000)
						TASK_STAND_STILL(NULL,1000)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,vPlayerLoc,PEDMOVEBLENDRATIO_WALK,-1)
						TASK_STAND_STILL(NULL,2000)
					CLOSE_SEQUENCE_TASK(temp_sequence)
					TASK_PERFORM_SEQUENCE(tempPed,temp_sequence)
					CLEAR_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				ELSE
					INT iPart = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(ClosestPlayerID))
					PRINTLN("[RCC MISSION] RUN_AGGRO_PED_HUNTING_BODY - Aggro ped ",iped," can see player ",iPart)
					TRIGGER_AGGRO_ON_PED(tempPed,iped,iPart)
				ENDIF
			ENDIF
		ELSE
			IF NOT (IS_TARGET_PED_IN_PERCEPTION_AREA(tempPed,ClosestPlayer) AND HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(iPed, tempPed,ClosestPlayer,SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE))
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PERFORM_SEQUENCE, TRUE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,FALSE)
					CLEAR_BIT(iPedHeardBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					SET_PED_HIGHLY_PERCEPTIVE(tempPed,FALSE)
					
					PRINTLN("[RCC MISSION] RUN_AGGRO_PED_HUNTING_BODY - Aggro ped ",iped," is giving up")
					CLEAR_BIT(iPedZoneAggroed[GET_LONG_BITSET_INDEX(iped)],GET_LONG_BITSET_BIT(iped))
					CLEAR_BIT(iPedLocalSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					BROADCAST_FMMC_PED_UNSPOOKED(iped)
				ENDIF
			ELSE
				INT iPart = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(ClosestPlayerID))
				PRINTLN("[RCC MISSION] RUN_AGGRO_PED_HUNTING_BODY - Aggro ped ",iped," can see player ",iPart)
				TRIGGER_AGGRO_ON_PED(tempPed,iped,iPart)
			ENDIF
		ENDIF
	ELSE
		PRINTLN("[RCC MISSION] RUN_AGGRO_PED_HUNTING_BODY - Aggro ped is in combat - ",iped)
		INT iCombatPart = -1
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroDoesntFail)
			iCombatPart = GET_PART_PED_IS_IN_COMBAT_WITH(tempPed)
			PRINTLN("[RCC MISSION] GET_PART_PED_IS_IN_COMBAT_WITH - Ped ",iped," is in combat with part ",iCombatPart)
		ENDIF
		TRIGGER_AGGRO_ON_PED(tempPed,iped,iCombatPart)
	ENDIF
	
ENDPROC

PROC PROCESS_PED_BODY_AGGRO_INIT(PED_INDEX ThisPed, INT iped, INT iTeam, BOOL bInVeh, BOOl bForceAggro = FALSE)
	BOOL bAggro
	INT iSpookedReason = ciSPOOK_NONE
	INT iPartCausingFail = -1
	
	PRINTLN("[RCC MISSION][SpookAggro] PROCESS_PED_BODY_AGGRO_INIT - Ped ",iped," Checking if we should aggro on spawn.")
	
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIncludesNPCs)
		IF bForceAggro
		OR ( ( SHOULD_PED_START_COMBAT(ThisPed, iped) OR IS_PED_FLEEING(ThisPed) )
			  AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iTeam[iteam] != ciPED_RELATION_SHIP_LIKE )
		OR HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER(iped, iPartCausingFail, iSpookedReason, bInVeh, iTeam)
			bAggro = TRUE
		ENDIF
	ELSE
		bAggro = TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwelve, ciPed_BSTwelve_AggroOnSpawnIfAggroAlertEventWasSentOut)
		IF IS_BIT_SET(iPedLocalCheckedSpawnedAggroed[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
		OR ARE_ANY_PEDS_AGGROED()
			PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - iPed: ", iPed, " Spawning into aggro'd is spooked from Force Aggro")		
			SET_BIT(iPedLocalCheckedSpawnedAggroed[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			bAggro = TRUE
		ENDIF
	ENDIF
	
	IF bAggro
		TRIGGER_AGGRO_TASKS_ON_PED(ThisPed, iped, iSpookedReason)
		
		SET_BIT(iPedLocalSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		
		#IF IS_DEBUG_BUILD
		IF iSpookedReason = ciSPOOK_NONE
			IF bForceAggro
				PRINTLN("[RCC MISSION][SpookAggro] PROCESS_PED_BODY_AGGRO_INIT - Ped ",iped," aggroed due to bForceAggro (probably due to being hurt/killed)")
			ELIF IS_PED_IN_COMBAT(ThisPed)
				PRINTLN("[RCC MISSION][SpookAggro] PROCESS_PED_BODY_AGGRO_INIT - Ped ",iped," aggroed because in combat!")
			ELIF IS_PED_FLEEING(ThisPed)
				PRINTLN("[RCC MISSION][SpookAggro] PROCESS_PED_BODY_AGGRO_INIT - Ped ",iped," aggroed because fleeing!")
			ELIF (GET_PED_ALERTNESS(ThisPed) != AS_NOT_ALERT AND GET_PED_ALERTNESS(ThisPed) != INT_TO_ENUM(ALERTNESS_STATE, -1))
				PRINTLN("[RCC MISSION][SpookAggro] PROCESS_PED_BODY_AGGRO_INIT - Ped ",iped," aggroed because alerted!")
			ELSE
				PRINTLN("[RCC MISSION][SpookAggro] PROCESS_PED_BODY_AGGRO_INIT - Ped ",iped," aggroed because MYSTERY")
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION][SpookAggro] PROCESS_PED_BODY_AGGRO_INIT - Ped ",iped," aggroed w spook reason ",iSpookedReason)
		ENDIF
		#ENDIF
		
		IF (NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroDoesntFail) AND iSpookedReason != ciSPOOK_SEEN_BODY)
		OR (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_AggroAlertsAllOtherAggroPeds) AND WILL_ANY_AGGRO_PED_FAIL_THE_MISSION())
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwelve, ciPed_BSTwelve_AlwaysSetSpookReason)
			PRINTLN("[RCC MISSION][SpookAggro] GET_PART_PED_IS_IN_COMBAT_WITH - Ped ",iped, " is in combat with part ", iPartCausingFail, " || BEFORE CHECK")
			IF iPartCausingFail = -1
				iPartCausingFail = GET_PART_PED_IS_IN_COMBAT_WITH(ThisPed)
				PRINTLN("[RCC MISSION][SpookAggro] GET_PART_PED_IS_IN_COMBAT_WITH - Ped ",iped," is in combat with part ",iPartCausingFail)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwelve, ciPed_BSTwelve_AlwaysSetSpookReason)
		OR IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO)
			IF iPartCausingFail = -1
				iPartCausingFail = GET_PART_CLOSEST_TO_ALERTED_PED(ThisPed)
				PRINTLN("[RCC MISSION][SpookAggro] GET_PART_CLOSEST_TO_ALERTED_PED - The closest player to ped ",iped," is participant ", iPartCausingFail)
			ENDIF
		ENDIF
		
		IF bIsLocalPlayerHost
			 
			INT iSpooked
			
			IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO)
				iSpooked = iSpookedReason
			ENDIF
			
			IF NOT IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				BROADCAST_FMMC_PED_SPOOKED(iped, iSpooked, iPartCausingFail, NATIVE_TO_INT(GET_NETWORK_TIME()))
			ENDIF
			
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour,ciPED_BSFour_StayInTasks_On_Aggro)
				IF iSpookedReason = ciSPOOK_PLAYER_TOUCHING_ME
				OR iSpookedReason = ciSPOOK_PLAYER_HEARD
					TRIGGER_SERVER_AGGRO_HUNTING_ON_PED(ThisPed, iped, iPartCausingFail)
				ELIF iSpookedReason = ciSPOOK_SEEN_BODY
					TRIGGER_SERVER_AGGRO_DEAD_BODY_RESPONSE_ON_PED(ThisPed, iped, iPartCausingFail)
				ELSE
					TRIGGER_AGGRO_ON_PED(ThisPed,iped,iPartCausingFail)
				ENDIF
			ELSE
				TRIGGER_AGGRO_ON_PED(ThisPed,iped,iPartCausingFail)
			ENDIF
			
		ELSE
			IF NOT IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				BROADCAST_FMMC_PED_SPOOKED(iped, iSpookedReason, iPartCausingFail, NATIVE_TO_INT(GET_NETWORK_TIME()))
			ENDIF
			
			IF iSpookedReason = ciSPOOK_NONE
				// As this won't get caught as an aggro in the PROCESS_FMMC_PED_SPOOKED check, send that out manually too:
				TRIGGER_AGGRO_ON_PED(ThisPed, iped, iPartCausingFail)
			ENDIF
			
		ENDIF
		
	ENDIF
	
ENDPROC





FUNC BOOL IS_ENTITY_IN_CENTER_ANGLE(PED_INDEX pedId, ENTITY_INDEX entId)
	IF IS_ENTITY_ALIVE(pedId)
		VECTOR vToTrail = NORMALISE_VECTOR((GET_ENTITY_COORDS(entId) - GET_ENTITY_COORDS(pedId)))
		FLOAT fDot = DOT_PRODUCT(GET_ENTITY_FORWARD_VECTOR(pedId), vToTrail)
		FLOAT fCenterAngle
		fCenterAngle = DEG_TO_RAD(GET_PED_VISUAL_FIELD_CENTER_ANGLE(pedId))
		RETURN fDot >= 1-fCenterAngle
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC BOOL HAS_PED_PERCEIVED_DRONE(PED_INDEX pedId)
	ENTITY_INDEX eiDrone = GET_LOCAL_DRONE_PROP_OBJ_INDEX()
	
	IF NOT DOES_ENTITY_EXIST(eiDrone)
		PRINTLN("[PED BODY DRONE] HAS_PED_PERCEIVED_DRONE - Drone doesn't exist")
		RETURN FALSE
	ENDIF
	
	FLOAT fDist2 = VDIST2(GET_ENTITY_COORDS(pedId, FALSE), GET_ENTITY_COORDS(eiDrone, FALSE))

	IF fDist2 > 9.0 //3.0^2
		
		RETURN FALSE
	ENDIF
	
	
	IF (fDist2 < 4.0 //2.0
	AND	IS_ENTITY_IN_CENTER_ANGLE(pedId,eiDrone)
	AND HAS_ENTITY_CLEAR_LOS_TO_ENTITY(pedId, eiDrone, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE))
	OR fDist2 < 1.0 //1.0
		PRINTLN("[PED BODY DRONE] HAS_PED_PERCEIVED_DRONE - Drone seen")
		RETURN TRUE
	ENDIF
	
	PRINTLN("[PED BODY DRONE] HAS_PED_PERCEIVED_DRONE - Drone not seen")
	RETURN FALSE
ENDFUNC 

FUNC BOOL HAS_PED_BEEN_AGGROD_BY_NEARBY_DRONE(PED_INDEX tempPed, INT iPed)

	IF NOT IS_LOCAL_PLAYER_USING_DRONE()
		RETURN FALSE
	ENDIF
	
	IF HAS_PED_PERCEIVED_DRONE(tempPed)
		IF (GET_RELATIONSHIP_BETWEEN_PEDS(tempPed, LocalPlayerPed) = ACQUAINTANCE_TYPE_PED_LIKE)
		OR (GET_RELATIONSHIP_BETWEEN_PEDS(tempPed, LocalPlayerPed) = ACQUAINTANCE_TYPE_PED_RESPECT)
			//Don't aggro friends!
			PRINTLN("[PED BODY DRONE] MAINTAIN_HAS_PED_PERCEIVED_DRONE - Friends with spotter:  ",iPed)
			
			RETURN FALSE
		ENDIF
		
		IF iPedDroneSpotter = -1
			iPedDroneSpotter = iPed
			PRINTLN("[NM] HAS_PED_BEEN_AGGROD_BY_NEARBY_DRONE - Spotted by ped ",iPedDroneSpotter,", waiting for timer to expire...")
		ENDIF
		IF HAS_NET_TIMER_EXPIRED(pedDronePerceptionBufferTimer,2000)
			PRINTLN("[NM] HAS_PED_BEEN_AGGROD_BY_NEARBY_DRONE - Timer expired.")
			PRINTLN("[PED BODY DRONE] HAS_PED_BEEN_AGGROD_BY_NEARBY_DRONE - Spotted by: ",iPed)
			TRIGGER_AGGRO_ON_PED(tempPed,iped)
			
			FLOAT fInformRange = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iInformRange)
				
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroDoesntFail)
				fInformRange = 30.0
			ENDIF
			
			SET_PED_TO_INFORM_RESPECTED_FRIENDS(tempPed, fInformRange, 50)
			RETURN TRUE
		ENDIF
	ELSE
		IF iPed = iPedDroneSpotter
			RESET_NET_TIMER(pedDronePerceptionBufferTimer)
			iPedDroneSpotter = -1
			PRINTLN("[NM] HAS_PED_BEEN_AGGROD_BY_NEARBY_DRONE - Timer reset.")
		ENDIF	

	ENDIF
	
	RETURN FALSE
ENDFUNC




PROC MAINTAIN_DRONE_RELATIONSHIP_GROUP_WITH_AGGRO(INT iped)
	IF IS_LOCAL_PLAYER_USING_DRONE()
		INT iTeam
		FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
	
			IF SHOULD_AGRO_FAIL_FOR_TEAM(iped,iTeam)
				IF GET_DRONE_RELATIONSHIP_GROUP() != relMyFmGroup
					SET_DRONE_RELATIONSHIP_GROUP(relMyFmGroup  )
					IF DOES_ENTITY_EXIST(g_sDroneGlobals.DroneFakePed)
					AND NOT IS_ENTITY_DEAD(g_sDroneGlobals.DroneFakePed)
						PRINTLN("[PED_BODY] MAINTAIN_DRONE() = relMyFmGroup")
						SET_PED_RELATIONSHIP_GROUP_HASH(g_sDroneGlobals.DroneFakePed , GET_DRONE_RELATIONSHIP_GROUP())
					ENDIF
				ENDIF
			ELSE
				IF GET_DRONE_RELATIONSHIP_GROUP() !=rgFM_HateAiHate
					SET_DRONE_RELATIONSHIP_GROUP(rgFM_HateAiHate  )
					IF DOES_ENTITY_EXIST(g_sDroneGlobals.DroneFakePed)
					AND NOT IS_ENTITY_DEAD(g_sDroneGlobals.DroneFakePed)
						SET_PED_RELATIONSHIP_GROUP_HASH(g_sDroneGlobals.DroneFakePed , GET_DRONE_RELATIONSHIP_GROUP())
						PRINTLN("[PED_BODY] MAINTAIN_DRONE() = rgFM_HateAiHate")
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

FUNC BOOL DOES_PED_CARE_IF_THIS_PLAYER_IS_HACKING_SOMETHING(PLAYER_INDEX piPlayer, INT iPed)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPed_BSEight_SpookOnHack)
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_PLAYER_ACTIVE(piPlayer)	
		PARTICIPANT_INDEX piPart = NETWORK_GET_PARTICIPANT_INDEX(piPlayer)
		INT iPart = NATIVE_TO_INT(piPart)
		
		IF iPart != -1
			IF MC_playerBD[iPart].iObjHacking > -1
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


PROC RUN_PED_BODY_SPOOKED_AND_CANCEL_TASKS_CHECKS(PED_INDEX tempPed, INT iped, VEHICLE_INDEX pedVehicle)
	
	#IF IS_DEBUG_BUILD
		IF bHandcuff
			PRINTLN("[RCC MISSION][SpookAggro] [sc_handcuff] RUN_PED_BODY_SPOOKED_AND_CANCEL_TASKS_CHECKS called for ped ", iped)
		ENDIF
	#ENDIF
	BOOL bInVeh, bCheckCancelTasks1, bCheckCancelTasks2, bCancelTasks, bCloseEnough
	FLOAT fDistance
	
	IF sMissionPedsLocalVars[iPed].ePedSyncSceneState > PED_SYNC_SCENE_STATE_INIT
	AND sMissionPedsLocalVars[iPed].ePedSyncSceneState < PED_SYNC_SCENE_STATE_PROCESS_END
	AND sMissionPedsLocalVars[iPed].iSyncScene > -1
	AND NOT IS_BIT_SET(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_iSyncSceneAnimationFinished)
		PRINTLN("[LM][RCC MISSION][SpookAggro] RUN_PED_BODY_SPOOKED_AND_CANCEL_TASKS_CHECKS called for ped ", iped, " Returning false as we are processing sync scene animations.")
		EXIT
	ENDIF
		
	IF DOES_ENTITY_EXIST(pedVehicle)
		bInVeh = TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,ciPED_BSThree_CancelTasksToCombatPlayers)
	AND MC_serverBD_2.iPedState[iped] != ciTASK_GENERAL_COMBAT
	AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].ipedBitsetEight, ciPed_BSEight_ForceTaskMissionBoat)
		bCheckCancelTasks1 = TRUE
		
		fDistance = 60
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,ciPED_BSThree_CancelTasksWSpookedChecks)
			fDistance = 100
		ENDIF
		
		IF bInVeh
		AND (IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(pedVehicle)) OR IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(pedVehicle)))
			fDistance = 300
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThirteen, ciPed_BSThirteen_UseSmallPlaneAndHeliSpookRange)
				fDistance = 100
			ENDIF
		ENDIF
	ENDIF
	
	PRINTLN("[LM][RCC MISSION][SpookAggro] RUN_PED_BODY_SPOOKED_AND_CANCEL_TASKS_CHECKS called for ped ", iped, " fDistance: ", fDistance, " bCheckCancelTasks1: ", bCheckCancelTasks1)
	
	INT iAggroTeam = -1
	INT iTeam
	
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		
		IF iAggroTeam = -1
		AND SHOULD_AGRO_FAIL_FOR_TEAM(iped, iTeam)
			iAggroTeam = iTeam
		ENDIF
		
		IF bCheckCancelTasks1
		AND NOT bCancelTasks
			
			PLAYER_INDEX ClosestPlayer = INT_TO_PLAYERINDEX(GET_NEAREST_PLAYER_TO_ENTITY_ON_TEAM(tempPed,iTeam))
			PED_INDEX ClosestPlayerPed = GET_PLAYER_PED(ClosestPlayer)
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iTeam[iTeam] = ciPED_RELATION_SHIP_DISLIKE
			OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iTeam[iTeam] = ciPED_RELATION_SHIP_HATE
				
				BOOL bPlayerCloseEnough = FALSE
				BOOL bLineOfSight = HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(iPed, tempPed,ClosestPlayerPed,SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)
				
				IF (NOT IS_PED_INJURED(ClosestPlayerPed))
				AND (bLineOfSight OR NOT IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)) // Using Mode check here as this is a "stealth fix" for just casino heist.
				AND (GET_DISTANCE_BETWEEN_ENTITIES(ClosestPlayerPed ,tempPed) < fDistance)
					IF NOT DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_PED(iped)
						bPlayerCloseEnough = TRUE
						PRINTLN("[RCC MISSION][SpookAggro] SPOOKED PED - bCancelTasks = TRUE set. iPed ", iped, " Closest Player: ", GET_PLAYER_NAME(ClosestPlayer), " bPlayerCloseEnough = TRUE.")
					ENDIF
				ENDIF
				
				IF SHOULD_PED_START_COMBAT(tempPed, iped) 
				OR bPlayerCloseEnough
					
					bCheckCancelTasks2 = TRUE
					bCloseEnough = bPlayerCloseEnough
					
					IF bPlayerCloseEnough
					AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,ciPED_BSThree_CancelTasksWSpookedChecks)
						IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iTeam[iTeam] = ciPED_RELATION_SHIP_DISLIKE OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iTeam[iTeam] = ciPED_RELATION_SHIP_HATE)
						AND IS_TARGET_PED_IN_PERCEPTION_AREA(tempPed,ClosestPlayerPed)
						AND bLineOfSight
							bCancelTasks = TRUE
							PRINTLN("[RCC MISSION][SpookAggro] SPOOKED PED - bCancelTasks = TRUE set. iPed ", iped, " Closest Player: ", GET_PLAYER_NAME(ClosestPlayer), " Was Visible.")
						ENDIF
						IF IS_ENTITY_TOUCHING_ENTITY(tempPed, ClosestPlayerPed)
							bCancelTasks = TRUE
							PRINTLN("[RCC MISSION][SpookAggro] SPOOKED PED - bCancelTasks = TRUE set. iPed ", iped, " Closest Player: ", GET_PLAYER_NAME(ClosestPlayer), " Was Touching.")
						ENDIF
						IF IS_PED_SHOOTING(ClosestPlayerPed)
							IF NOT IS_PED_CURRENT_WEAPON_SILENCED(ClosestPlayerPed)
								bCancelTasks = TRUE
								PRINTLN("[RCC MISSION][SpookAggro] SPOOKED PED - bCancelTasks = TRUE set. iPed ", iped, " Closest Player: ", GET_PLAYER_NAME(ClosestPlayer), " Was Shooting.")
							ELSE
								PRINTLN("[RCC MISSION][SpookAggro] SPOOKED PED - bCancelTasks = TRUE set. iPed ", iped, " Closest Player: ", GET_PLAYER_NAME(ClosestPlayer), " Was Shooting. But the weapon is silenced. Ignoring...")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDFOR
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPed_BSEight_SpookOnHack)
		PRINTLN("[RCC MISSION][SpookAggro] SPOOKED PED - Checking SpookOnHack.")
		
		FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
			FLOAT fHackPerception = 25
			
			IF iAggroTeam = -1
			AND SHOULD_AGRO_FAIL_FOR_TEAM(iped, iTeam)
				iAggroTeam = iTeam
			ENDIF
			
			PLAYER_INDEX ClosestPlayer = INT_TO_PLAYERINDEX(GET_NEAREST_PLAYER_TO_ENTITY_ON_TEAM(tempPed,iTeam))
			PED_INDEX ClosestPlayerPed = GET_PLAYER_PED(ClosestPlayer)
			BOOL bPlayerCloseEnough = FALSE
			BOOL bIsHostileFromHacking = DOES_PED_CARE_IF_THIS_PLAYER_IS_HACKING_SOMETHING(ClosestPlayer, iPed)	
			
			IF (NOT IS_PED_INJURED(ClosestPlayerPed))
			AND (GET_DISTANCE_BETWEEN_ENTITIES(ClosestPlayerPed ,tempPed) < fHackPerception)
				bPlayerCloseEnough = TRUE
				PRINTLN("[RCC MISSION][SpookAggro] SPOOKED PED - iPed ", iped, " Closest Player: ", GET_PLAYER_NAME(ClosestPlayer), " bPlayerCloseEnough = TRUE.")
			ENDIF
			
			IF bIsHostileFromHacking AND bPlayerCloseEnough
				PRINTLN("[RCC MISSION][SpookAggro] SPOOKED PED - iPed ", iped, " Should be hostile from hacking and player is close enough.")				
				IF IS_TARGET_PED_IN_PERCEPTION_AREA(tempPed,ClosestPlayerPed)
				AND HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(iPed, tempPed, ClosestPlayerPed, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)
					bCancelTasks = TRUE
					PRINTLN("[RCC MISSION][SpookAggro] SPOOKED PED - bCancelTasks = TRUE set. iPed ", iped, " Closest Player: ", GET_PLAYER_NAME(ClosestPlayer), " Was Visible.")
				ENDIF
			ENDIF			
		ENDFOR		
	ENDIF
	
	BOOL bCheckSpooked, bSpooked, bForceAggro
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSpookPedOnVehicleDamageBS != 0
		INT iVeh = 0
		FOR iVeh = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles-1
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSpookPedOnVehicleDamageBS, iVeh)
			AND IS_BIT_SET(iVehHasBeenDamagedOrHarmedBS, iVeh)
				PRINTLN("[RCC MISSION][SpookAggro] SPOOKED PED - iVehHasBeenDamagedOrHarmedBS and iSpookPedOnVehicleDamageBS set. iPed ", iped, " iVeh: ", iVeh)
				bCheckSpooked = TRUE
				bSpooked = TRUE
				bCancelTasks = TRUE
			ENDIF
		ENDFOR
	ENDIF
	
	IF bCheckCancelTasks2
	AND NOT bCancelTasks
	AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].ipedBitsetEight, ciPed_BSEight_ForceTaskMissionBoat)
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,ciPED_BSThree_CancelTasksWSpookedChecks)
			IF SHOULD_PED_START_COMBAT(tempPed,iped)
			AND (bCloseEnough OR fDistance = 0)
				bCancelTasks = TRUE
				PRINTLN("[RCC MISSION][SpookAggro] SPOOKED PED - iPed ", iped, " I've been spooked, cancel tasks.  (1) ")
			ENDIF
		ELSE
			IF SHOULD_PED_START_COMBAT(tempPed,iped)
			AND (bCloseEnough OR fDistance = 0)
				bCancelTasks = TRUE
				PRINTLN("[RCC MISSION][SpookAggro] SPOOKED PED - iPed ", iped, " I've been spooked, cancel tasks.  (2A) ")
			ENDIF
			IF IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				bCancelTasks = TRUE
				PRINTLN("[RCC MISSION][SpookAggro] SPOOKED PED - iPed ", iped, " I've been spooked, cancel tasks.  (2B) ")
			ENDIF			
			IF HAS_PED_BEEN_SPOOKED(iped, TRUE, TRUE, bInVeh)
				bCancelTasks = TRUE
				PRINTLN("[RCC MISSION][SpookAggro] SPOOKED PED - iPed ", iped, " I've been spooked, cancel tasks.  (2C) ")
			ENDIF
		ENDIF
	ENDIF
	
	IF bCancelTasks
	AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].ipedBitsetEight, ciPed_BSEight_ForceTaskMissionBoat)
		IF NOT IS_BIT_SET(iPedLocalCancelTasksBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			SET_BIT(iPedLocalCancelTasksBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			PRINTLN("[JS][SpookAggro] RUN_PED_BODY_SPOOKED_AND_CANCEL_TASKS_CHECKS - Calling BROADCAST_FMMC_PED_CANCEL_TASKS")
			BROADCAST_FMMC_PED_CANCEL_TASKS(iped) //Previosly spammed, if peds stop getting their tasks cleared this may be the culprate
		ENDIF
		
		IF NETWORK_HAS_CONTROL_OF_ENTITY(tempPed)
			PRINTLN("[RCC MISSION][SpookAggro] RUN_PED_BODY_SPOOKED_AND_CANCEL_TASKS_CHECKS - Task ped ",iped," with combat, they should cancel their tasks!")
			TASK_PED_GENERAL_COMBAT(tempPed, iped)
		ENDIF
		
		IF bIsLocalPlayerHost
			PROCESS_SERVER_PED_CANCEL_TASKS(iped)
		ENDIF
		
	ENDIF
		
	//SPOOKED CHECKS:
	IF IS_BIT_SET(MC_serverBD.iPedTaskActive[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		IF NOT IS_PED_IN_A_CUTSCENE(iped)
			IF (NOT IS_BIT_SET(iPedLocalSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed)) )
				PRINTLN("[RCC MISSION][SpookAggro] RUN_PED_BODY_SPOOKED_AND_CANCEL_TASKS_CHECKS bCheckSpooked : 1 iPedLocalSpookedBitset ", iped)				
				bCheckSpooked = TRUE
			ENDIF

			IF ( (iAggroTeam != -1) AND (NOT IS_BIT_SET(iPedLocalAggroedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))) )
				PRINTLN("[RCC MISSION][SpookAggro] RUN_PED_BODY_SPOOKED_AND_CANCEL_TASKS_CHECKS bCheckSpooked : 1 iAggroTeam etc ", iped)
				bCheckSpooked = TRUE
			ENDIF
		ENDIF
	ELSE
		IF iAggroTeam != -1
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_SpookFromBeginning)
			IF (NOT IS_BIT_SET(iPedLocalSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed)) )
			OR ((iAggroTeam != -1) AND (NOT IS_BIT_SET(iPedLocalAggroedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed)) ) )			
				
				PRINTLN("[RCC MISSION][SpookAggro] RUN_PED_BODY_SPOOKED_AND_CANCEL_TASKS_CHECKS bCheckSpooked 2 ped ", iped)
				
				bCheckSpooked = TRUE
			ENDIF
		ENDIF
	ENDIF
		
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwelve, ciPed_BSTwelve_BlockThreatenEvents)
	AND IS_PED_SPOOKED_AT_COORD_IN_RADIUS(iPed)
		PRINTLN("[RCC MISSION][SpookAggro] RUN_PED_BODY_SPOOKED_AND_CANCEL_TASKS_CHECKS - iped: ", iped, " is spooked by special script event at Coord and Radius.")
		bCheckSpooked = TRUE
	ENDIF	
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwelve, ciPed_BSTwelve_AggroOnSpawnIfAggroAlertEventWasSentOut)
		IF IS_BIT_SET(iPedLocalCheckedSpawnedAggroed[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
		OR ARE_ANY_PEDS_AGGROED()
			PRINTLN("[RCC MISSION][SpookAggro] SPOOKED PED - RUN_PED_BODY_SPOOKED_AND_CANCEL_TASKS_CHECKS iPed ", iped, " is spooked from Force Aggro")
			bCheckSpooked = TRUE
		ENDIF
	ENDIF
	
	IF bCheckSpooked
	AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].ipedBitsetEight, ciPed_BSEight_ForceTaskMissionBoat)		
		PRINTLN("[RCC MISSION][SpookAggro] RUN_PED_BODY_SPOOKED_AND_CANCEL_TASKS_CHECKS iPed: ", iped, " Having spook checked (bCheckSpooked)")
		
		IF NOT IS_BIT_SET( iPedHitByStunGun[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
			IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(tempPed, WEAPONTYPE_STUNGUN)
			OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(tempPed, WEAPONTYPE_DLC_TRANQUILIZER)
			
				PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - don't aggro yet, was hit by stun gun: ",iped)
				NETWORK_REQUEST_CONTROL_OF_ENTITY(tempPed)
				
				PRINTLN("[RCC MISSION][SpookAggro] SPOOKED PED - RUN_PED_BODY_SPOOKED_AND_CANCEL_TASKS_CHECKS iPed ", iped, " setting HIT BY STUNGUN")
				SET_BIT( iPedHitByStunGun[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed) )
				SET_PED_CONFIG_FLAG(tempPed, PCF_DisableInjuredCryForHelpEvents, TRUE)
				
				EXIT
			ENDIF
		ELSE
			IF 	IS_PED_FALLING(tempPed)
			OR 	IS_PED_RAGDOLL(tempPed)
			OR 	IS_PED_PERFORMING_TASK(tempPed, SCRIPT_TASK_NM_ELECTROCUTE)
			OR 	IS_PED_BEING_STUNNED(tempPed, WEAPONTYPE_STUNGUN)
				SET_PED_CONFIG_FLAG(tempPed, PCF_DisableInjuredCryForHelpEvents, TRUE)
				PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - NO - ELECTROCUTION ONGOING: ",iped)
				EXIT
			ELSE
				NETWORK_REQUEST_CONTROL_OF_ENTITY(tempPed)
				SET_PED_CONFIG_FLAG(tempPed, PCF_DisableInjuredCryForHelpEvents, FALSE)

				PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - GETTING UP FROM STUNGUN - TRIGGER AGGRO ",iped)
				bSpooked = TRUE
				CLEAR_BIT( iPedHitByStunGun[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
			ENDIF
		ENDIF		
		
		//Handle drone awareness
		IF  HAS_PED_BEEN_AGGROD_BY_NEARBY_DRONE(tempPed, iPed)
			bSpooked = TRUE
		ENDIF
		
		IF SHOULD_PED_START_COMBAT(tempPed, iped)
			bSpooked = TRUE
			
			#IF IS_DEBUG_BUILD
			IF IS_PED_IN_COMBAT(tempPed)
				PRINTLN("[RCC MISSION][SpookAggro] SPOOKED PED ",iped," - Should start combat, ped is in combat!")
			ELIF IS_PED_FLEEING(tempPed)
				PRINTLN("[RCC MISSION][SpookAggro] SPOOKED PED ",iped," - Should start combat, ped is fleeing!")
			ELIF (GET_PED_ALERTNESS(tempPed) != AS_NOT_ALERT AND GET_PED_ALERTNESS(tempPed) != INT_TO_ENUM(ALERTNESS_STATE, -1))
				PRINTLN("[RCC MISSION][SpookAggro] SPOOKED PED ",iped," - Should start combat, ped is in alerted!")
			ENDIF
			#ENDIF
		ENDIF
		
		IF IS_PED_FLEEING(tempPed)
			bSpooked = TRUE
			PRINTLN("[RCC MISSION][SpookAggro] SPOOKED PED ",iped," - Ped is fleeing!")
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_AggroWhenHurtOrKilled)
			IF IS_BIT_SET(iPedLocalReceivedEvent_HurtOrKilled[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				bSpooked = TRUE
				bForceAggro = TRUE
				PRINTLN("[RCC MISSION][SpookAggro] SPOOKED PED ",iped," - iPedLocalReceivedEvent_HurtOrKilled bit is set")
			ELSE
				IF IS_PED_RAGDOLL(tempPed)
				OR IS_PED_GETTING_UP(tempPed)
				OR IS_PED_IN_WRITHE(tempPed)
					
					SET_BIT(iPedLocalReceivedEvent_HurtOrKilled[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					bSpooked = TRUE
					bForceAggro = TRUE
					
					#IF IS_DEBUG_BUILD
					IF IS_PED_RAGDOLL(tempPed)
						PRINTLN("[RCC MISSION][SpookAggro] SPOOKED PED ",iped," - Ped is ragdolling")
					ENDIF
					IF IS_PED_GETTING_UP(tempPed)
						PRINTLN("[RCC MISSION][SpookAggro] SPOOKED PED ",iped," - Ped is getting up")
					ENDIF
					IF IS_PED_IN_WRITHE(tempPed)
						PRINTLN("[RCC MISSION][SpookAggro] SPOOKED PED ",iped," - Ped is writhing")
					ENDIF
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF HAS_PED_BEEN_SPOOKED(iped, DEFAULT, DEFAULT, bInVeh)
			bSpooked = TRUE
			PRINTLN("[RCC MISSION][SpookAggro] SPOOKED PED ",iped," - Has_ped_been_spooked returning true!")
		ENDIF
		
		IF ( (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iActivationRange != 0) //This is a thing for some reason: defensive peds with gotos and an
			 AND GET_PED_COMBAT_STYLE(iped) = ciPED_DEFENSIVE //activation range get spooked by default when triggered.
			 AND (NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,ciPED_BSThree_DontCombatOnActivationRange))//-This line is a creator override of that default functionality!
			 AND DOES_PED_HAVE_VALID_GOTO(iped) )
			bSpooked = TRUE
			PRINTLN("[RCC MISSION][SpookAggro] SPOOKED PED ",iped," - Defensive ped w goto on activation range has been triggered")
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetNine, ciPed_BSNine_Check_For_Aggro_On_Spawn)
			IF NOT IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				INT iTeamToCheck
				FOR iTeamToCheck = 0 TO MC_serverBD.iNumActiveTeams
					IF IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_0 + iTeamToCheck)
						bSpooked = TRUE
						PRINTLN("[RCC MISSION][SpookAggro] SPOOKED PED ",iped," -  Spawning in checking for aggro, spooking")
						BREAKLOOP
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
		
		IF bSpooked
			IF NOT IS_BIT_SET( iPedHitByStunGun[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
		
				IF iAggroTeam != -1
					PROCESS_PED_BODY_AGGRO_INIT(tempPed, iped, iAggroTeam, bInVeh, bForceAggro)
				ELSE
					IF NOT IS_BIT_SET(iPedLocalSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
						BROADCAST_FMMC_PED_SPOOKED(iped)
						SET_BIT(iPedLocalSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					ELSE
						PRINTLN("[RCC MISSION][SpookAggro] SPOOKED PED ",iped," - Ped already Spooked.")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: PED BLIPPING !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************



FUNC BOOL SHOULD_DISPLAY_CONE_ON_THIS_PED( INT iPedIndex )
	
	BOOL bCone
	BOOL bAggroPed
	
	bCone = IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPedIndex ].iPedBitsetThree, ciPED_BSThree_ShowVisionCone )
	
	IF bCone
		
		bAggroPed = SHOULD_AGRO_FAIL_FOR_TEAM(iPedIndex, MC_playerBD[iPartToUse].iteam)
		
		IF (bAggroPed AND IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_0 + MC_playerBD[iPartToUse].iteam) AND IS_BIT_SET(MC_serverBD.iPedAggroedBitset[GET_LONG_BITSET_INDEX(iPedIndex)], GET_LONG_BITSET_BIT(iPedIndex)))
		OR ((NOT bAggroPed) AND IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iPedIndex)], GET_LONG_BITSET_BIT(iPedIndex)))
			bCone = FALSE
		ENDIF
	ENDIF
	
	RETURN bCone
	
ENDFUNC

///PURPOSE: This function will change whether the blip's vision cone is displayed. 
PROC ENABLE_BLIP_VISION_CONE( BLIP_INDEX& thisBlip, BOOL bEnable )
	IF DOES_BLIP_EXIST( thisBlip )
		SET_BLIP_SHOW_CONE( thisBlip, bEnable )
	ENDIF
ENDPROC

/// PURPOSE:
///    Check to see if we should force this ped blip to be visible because they are the current objective
FUNC BOOL SHOULD_PED_BLIP_DISPLAY_FOR_OBJECTIVE(INT iPedIndex)

	// Debug we can uncomment if needed (potential frame spam when ped in vehicle
	//CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION] This ped ", iPedIndex, "'s Priority: ",  MC_ServerBD_4.iPedPriority[iPedIndex][MC_playerBD[iPartToUse].iteam],
	//			", and current priority: ", MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam],
	//			", and current rule: ", MC_serverBD_4.iPedRule[iPedIndex][MC_playerBD[iPartToUse].iteam])
	
	//3887656 - Hide invisible ped's blips
	IF IS_PED_INVISIBLE(iPedIndex)
		IF NOT GET_USINGSEETHROUGH()
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Check that the ped is an active objective
	IF MC_ServerBD_4.iPedPriority[iPedIndex][MC_playerBD[iPartToUse].iteam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
	
		// Currently only worry about those that we have to 'COLLECT' & 'PROTECT'
		IF MC_serverBD_4.iPedRule[iPedIndex][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
		OR MC_serverBD_4.iPedRule[iPedIndex][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_PROTECT
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BLIP_SPRITE GetCorrectBlipSpriteForMissionPed(PED_INDEX TempPed, INT iPedIndex, INT &iBlipColour)
	BLIP_SPRITE BlipSprite
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].iPedBitsetFour, ciPED_BSFour_SolidBlip)
		BlipSprite = GET_STANDARD_BLIP_ENUM_ID() // we wont set any blip sprite
	ELSE
		IF IS_PED_IN_ANY_VEHICLE(TempPed)
			IF SHOULD_PED_BLIP_DISPLAY_FOR_OBJECTIVE(iPedIndex)
				BlipSprite = RADAR_TRACE_AI // we use the donut	if the ped is the current objective (see 2193798)
			ELSE
				BlipSprite = GET_CORRECT_BLIP_SPRITE_FOR_PED_IN_VEHICLE(TempPed)
			ENDIF
		ELSE	
			BlipSprite = RADAR_TRACE_AI // we use the donut	
		ENDIF
		
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].sPedBlipStruct.iBlipSpriteOverride != 0
		BlipSprite = GET_BLIP_SPRITE_FROM_BLIP_SPRITE_OVERRIDE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].sPedBlipStruct.iBlipSpriteOverride)
	ENDIF
	
	// if we are using donut, use the same type of red as code
//	IF (BlipSprite = RADAR_TRACE_AI)
//	AND (iBlipColour = BLIP_COLOUR_RED)
//		iBlipColour = BLIP_COLOUR_HUDCOLOUR_RED	
//	ENDIF
	IF (BlipSprite = RADAR_TRACE_AI)
	AND (iBlipColour = BLIP_COLOUR_HUDCOLOUR_RED)
		iBlipColour = BLIP_COLOUR_RED	
	ENDIF
	
	// if we are using heli spin, but colour is not red (or hud red) then use the player heli sprite.
	IF (BlipSprite = RADAR_TRACE_ENEMY_HELI_SPIN)
	AND NOT ((iBlipColour = BLIP_COLOUR_HUDCOLOUR_RED) OR (iBlipColour = BLIP_COLOUR_RED))
		BlipSprite = RADAR_TRACE_PLAYER_HELI  	
	ENDIF	
	
	RETURN BlipSprite
ENDFUNC

PROC CREATE_PED_BLIP_WITH_COLOUR(PED_INDEX TempPed, INT iPedIndex, INT iBlipColour)
	CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION] Adding script blip for ped ", iPedIndex, " with colour: ", iBlipColour)
	
	// if this ped is pedtype_cop and the player has a wanted level then let the code handle it
	IF ((GET_PED_TYPE(TempPed) = PEDTYPE_COP)
	OR IS_PED_A_COP_MC(iPedIndex))
	AND (GET_PLAYER_WANTED_LEVEL(LocalPlayer) > 0)
		CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION] This ped is a cop and player has wanted level, leaving it to code.")
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF biHostilePedBlip[iPedIndex].PedID != NULL
			CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][AI] Ped ", iPedIndex, " AI blip cleaning up due to main ped blip being created.")
		ENDIF
	#ENDIF
	CLEANUP_AI_PED_BLIP( biHostilePedBlip[ iPedIndex ] )
	
	CDEBUG2LN(DEBUG_MC_BLIP, "[RCC MISSION]Removing any existing blips on ped ", iPedIndex, " ahead of ped blip creation.")
	REMOVE_ANY_EXISTING_BLIP_FROM_ENTITY(TempPed)
	
	BLIP_SPRITE BlipSprite
	
	biPedBlip[ iPedIndex ] = ADD_BLIP_FOR_ENTITY( TempPed )
	
	// What blip sprite should we be dealing with?
	BlipSprite = GetCorrectBlipSpriteForMissionPed(TempPed, iPedIndex, iBlipColour)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].sPedBlipStruct.iBlipSpriteOverride != 0
		BlipSprite = GET_BLIP_SPRITE_FROM_BLIP_SPRITE_OVERRIDE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].sPedBlipStruct.iBlipSpriteOverride)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].iPedBitsetEight, ciPed_BSEight_ForceBoatBlipSprite)
		BlipSprite = RADAR_TRACE_PLAYER_BOAT
		CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION] Overriding blip to Boat, ped ", iPedIndex)
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPedIndex ].sPedBlipStruct.iBlipColour != BLIP_COLOUR_DEFAULT
		iBlipColour = GET_BLIP_COLOUR_FROM_CREATOR(g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPedIndex ].sPedBlipStruct.iBlipColour)
		CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION] Custom colour override: ",iBlipColour )
	ENDIF
	
	
	// Set the correct blip sprite.
	IF NOT (BlipSprite = GET_STANDARD_BLIP_ENUM_ID())
		CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION] Custom ped sprite override: ", GET_BLIP_SPRITE_DEBUG_STRING(BlipSprite))
		SET_BLIP_SPRITE(biPedBlip[ iPedIndex ], BlipSprite)
	ENDIF
	
	// Should this blip be rotated?
	IF SHOULD_BLIP_BE_MANUALLY_ROTATED(biPedBlip[ iPedIndex ])
		SET_BLIP_ROTATION(biPedBlip[ iPedIndex ], ROUND(GET_ENTITY_HEADING_FROM_EULERS(TempPed)))
	ENDIF
	
	SET_BLIP_SCALE(biPedBlip[ iPedIndex ], g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPedIndex ].sPedBlipStruct.fBlipScale)
	
	// If local player is inside this vehicle then hide it
	IF IS_PED_IN_ANY_VEHICLE(TempPed)
	AND IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(MPGlobals.LocalPlayerID))
	AND (GET_VEHICLE_PED_IS_IN(TempPed) = GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(MPGlobals.LocalPlayerID)))	
		CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION] Setting ped ", iPedIndex, " blip alpha to 0 as they are in the same vehicle as the local player.")
		SET_BLIP_ALPHA(biPedBlip[ iPedIndex ], 0)
	ELSE
	
		// Should this blip be faded out if not 'leader' of vehicle (for heli's)
		IF IS_PED_IN_VEHICLE_THAT_HAS_A_BLIP(TempPed)
			CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION] Setting ped ", iPedIndex, " blip alpha to 0 as they are in a vehicle that is blipped.")
			SET_BLIP_ALPHA(biPedBlip[ iPedIndex ], 0)
		ELSE
			IF SHOULD_BLIP_ONLY_SHOW_LEADERS_WHEN_IN_VEHICLE(biPedBlip[ iPedIndex ])
			AND IS_PED_IN_ANY_VEHICLE(TempPed)
			AND NOT IS_PED_LEADER_OF_VEHICLE(TempPed, GET_VEHICLE_PED_IS_IN(TempPed), DEFAULT, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].iPedBitsetThirteen, ciPed_BSThirteen_ForceBlipWhenInvalidVehicleLeader))
				IF SHOULD_PED_BLIP_DISPLAY_FOR_OBJECTIVE(iPedIndex)
					CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION] Setting ped ", iPedIndex, " blip to flash since they are an objective ped.")
					SET_BLIP_FLASHES(biPedBlip[ iPedIndex ], TRUE)
				ELSE
					CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION] Setting ped ", iPedIndex, " blip alpha to 0 as they are in a vehicle that they are not the leader of.")
					SET_BLIP_ALPHA(biPedBlip[ iPedIndex ], 0)
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	
	
	IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].iPedBitsetThirteen, ciPED_BSThirteen_OverrideBlipColourForDisguise)
			IF  DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_PED(iPedIndex)
				iBlipColour = BLIP_COLOUR_WHITE
			ELSE
				iBlipColour =  BLIP_COLOUR_RED
			ENDIF
		ENDIF
	ENDIF 
	
	// Should we show the height indicator?
	SHOW_HEIGHT_ON_BLIP(biPedBlip[ iPedIndex ], IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPedIndex ].iPedBitsetFive, ciPED_BSFive_ShowBlipHeightIndicator)) 
	
	SET_BLIP_SHORT_HEIGHT_THRESHOLD(biPedBlip[iPedIndex], IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].iPedBitsetEleven, ciPED_BSEleven_UseShortBlipHeightThreshold))
	
	SET_BLIP_COLOUR( biPedBlip[ iPedIndex ], iBlipColour )
	
	CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][CACHE] Looking up custom blip name for ped ", iPedIndex, ".")
	IF NOT SET_BLIP_CUSTOM_NAME( 	biPedBlip[ iPedIndex ],
									MC_playerBD[ iPartToUse ].iteam,
									MC_serverBD_4.iPedPriority[ iPedIndex ][MC_playerBD[ iPartToUse ].iteam ],
									iPedBlipCachedNamePriority[ iPedIndex ] )
									
		SET_BLIP_NAME_FROM_TEXT_FILE( biPedBlip[iPedIndex], "HPEDBLIP" )
	ENDIF
	
	SET_BLIP_OVERRIDE_SPRITE_USE_CUSTOM_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].sPedBlipStruct.iBlipSpriteOverride, biPedBlip[iPedIndex])
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].iPedBitsetTwelve, ciPed_BSTwelve_UseFailNameForBlip)
		CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][CACHE] iPedIndex: ", iPedIndex, " Using Fail Name for Blip.")
		
		INT iName = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].iCustomPedName
		IF iName > -1
			CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][CACHE] iPedIndex: ", iPedIndex, " iName: ", iName, " ActualString: ", g_FMMC_STRUCT_ENTITIES.tlCustomPedName[iName], ".")
			IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomPedName[iName])
				//SET_BLIP_NAME_FROM_TEXT_FILE(biPedBlip[iPedIndex], g_FMMC_STRUCT_ENTITIES.tlCustomPedName[iName])
				BEGIN_TEXT_COMMAND_SET_BLIP_NAME("MCUSTBLIP")
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_FMMC_STRUCT_ENTITIES.tlCustomPedName[iName])
				END_TEXT_COMMAND_SET_BLIP_NAME(biPedBlip[iPedIndex])
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][CACHE] iPedIndex: ", iPedIndex, " iName = -1 for some reason. Inproper setup?")
		ENDIF
	ENDIF

	IF iBlipColour = BLIP_COLOUR_BLUE
		SET_BLIP_SCALE( biPedBlip[ iPedIndex ],g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPedIndex ].sPedBlipStruct.fBlipScale )
	ELSE
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPedIndex ].sPedBlipStruct.fBlipScale = 1.0
			SET_BLIP_SCALE( biPedBlip[ iPedIndex ], GET_BASE_SCALE_FOR_BLIP_SPRITE(BlipSprite, BLIP_SIZE_NETWORK_PED) )
		ELSE
			SET_BLIP_SCALE( biPedBlip[ iPedIndex ],g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPedIndex ].sPedBlipStruct.fBlipScale )
		ENDIF
	ENDIF
	SET_BLIP_PRIORITY( biPedBlip[ iPedIndex ],BLIPPRIORITY_HIGHEST )
	
	ENABLE_BLIP_VISION_CONE( biPedBlip[ iPedIndex ], SHOULD_DISPLAY_CONE_ON_THIS_PED( iPedIndex ) )
	
ENDPROC

PROC CREATE_PED_BOUNDS_BLIP_WITH_COLOUR(PED_INDEX TempPed, INT iPedIndex, INT iBlipColour, BOOL bSecondary = FALSE)
	
	// if this ped is pedtype_cop and the player has a wanted level then let the code handle it
	IF (GET_PED_TYPE(TempPed) = PEDTYPE_COP)
	AND (GET_PLAYER_WANTED_LEVEL(LocalPlayer) > 0)
		CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION] CREATE_PED_BOUNDS_BLIP_WITH_COLOUR - cop ped and player has wanted level, leaving it to code. ped: ",iPedIndex," with colour: ", iBlipColour)
		EXIT
	ENDIF
	
	IF DOES_BLIP_EXIST(biPedBlip[iPedIndex]) 
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF biHostilePedBlip[iPedIndex].PedID != NULL
			CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][AI] Ped ", iPedIndex, " AI blip cleaning up due to ped bounds blip being created.")
		ENDIF
	#ENDIF
	CLEANUP_AI_PED_BLIP( biHostilePedBlip[ iPedIndex ] )
	
	CDEBUG2LN(DEBUG_MC_BLIP, "[RCC MISSION]Removing any existing blips on ped ", iPedIndex, " ahead of ped bounds blip creation.")
	REMOVE_ANY_EXISTING_BLIP_FROM_ENTITY(TempPed)
	
	BLIP_SPRITE BlipSprite
	
	BLIP_INDEX tempBlip
	
	tempBlip = ADD_BLIP_FOR_ENTITY( TempPed )
	
	CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION] Adding bounds blip for ped: ",iPedIndex," with colour: ", iBlipColour)
	
	// what blip sprite should we be dealing with?
	BlipSprite = GetCorrectBlipSpriteForMissionPed(TempPed, iPedIndex, iBlipColour)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].sPedBlipStruct.iBlipSpriteOverride != 0
		BlipSprite = GET_BLIP_SPRITE_FROM_BLIP_SPRITE_OVERRIDE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].sPedBlipStruct.iBlipSpriteOverride)
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPedIndex ].sPedBlipStruct.iBlipColour != BLIP_COLOUR_DEFAULT
		iBlipColour = GET_BLIP_COLOUR_FROM_CREATOR(g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPedIndex ].sPedBlipStruct.iBlipColour)
		CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION] Custom colour override: ",iBlipColour )
	ENDIF
	
	IF NOT (BlipSprite = GET_STANDARD_BLIP_ENUM_ID())
		CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION] Custom ped bounds sprite override: ", GET_BLIP_SPRITE_DEBUG_STRING(BlipSprite))
		SET_BLIP_SPRITE(tempBlip, BlipSprite)
	ENDIF
	
	IF SHOULD_BLIP_BE_MANUALLY_ROTATED(bounds_blip)
		SET_BLIP_ROTATION(tempBlip, ROUND(GET_ENTITY_HEADING_FROM_EULERS(TempPed)))
	ENDIF
	
	SHOW_HEIGHT_ON_BLIP(tempBlip, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPedIndex ].iPedBitsetFive, ciPED_BSFive_ShowBlipHeightIndicator))
	
	SET_BLIP_COLOUR(tempBlip, iBlipColour )
	
	CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][CACHE] Looking up custom blip name for ped ", iPedIndex, ".")
	IF NOT SET_BLIP_CUSTOM_NAME( 	tempBlip,
									MC_playerBD[ iPartToUse ].iteam,
									MC_serverBD_4.iPedPriority[ iPedIndex ][MC_playerBD[ iPartToUse ].iteam ],
									iPedBlipCachedNamePriority[ iPedIndex ] )
									
		SET_BLIP_NAME_FROM_TEXT_FILE(tempBlip, "HPEDBLIP" )
	ENDIF
				
	SET_BLIP_OVERRIDE_SPRITE_USE_CUSTOM_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedIndex].sPedBlipStruct.iBlipSpriteOverride, biPedBlip[iPedIndex])
		
	IF iBlipColour = BLIP_COLOUR_BLUE
		SET_BLIP_SCALE(tempBlip, g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPedIndex ].sPedBlipStruct.fBlipScale )
	ELSE
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPedIndex ].sPedBlipStruct.fBlipScale = 1.0
			SET_BLIP_SCALE(tempBlip, GET_BASE_SCALE_FOR_BLIP_SPRITE(BlipSprite, BLIP_SIZE_NETWORK_PED) )
		ELSE
			SET_BLIP_SCALE(tempBlip, g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPedIndex ].sPedBlipStruct.fBlipScale)
		ENDIF
	ENDIF
	
	SET_BLIP_PRIORITY(tempBlip, BLIPPRIORITY_HIGHEST )
	
	IF NOT bSecondary
		bounds_blip = tempBlip
	ELSE
		sec_bounds_blip = tempBlip
	ENDIF
	
ENDPROC

FUNC BOOL DOES_PED_HAVE_DUMMY_BLIP(INT iPed)

	IF NOT IS_ENTITY_ALIVE(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPed]))
		RETURN FALSE
	ENDIF

	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[ iTeam ]
	INT iBlipLoop 
	
	FOR iBlipLoop = 0 TO FMMC_MAX_DUMMY_BLIPS - 1
		IF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iBlipLoop].iType = ciFMMC_DROP_OFF_TYPE_PED
		AND g_FMMC_STRUCT_ENTITIES.sDummyBlips[iBlipLoop].iTeam = iTeam	
			IF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iBlipLoop].iRule = iRule
			OR g_FMMC_STRUCT_ENTITIES.sDummyBlips[iBlipLoop].iRule = -2
				IF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iBlipLoop].iEntityToUse = iPed
					PRINTLN("[RCC MISSION][MMacK] ped ", iPed, " Has A Dummy Blip on rule ", iRule, " for team ", iTeam)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_OBJ_BLOCKER_AFFECT_BLIP(INT iPed)
	IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iped ].iPedBitsetTwo, ciPED_BSTwo_BlipOnFollow )
		RETURN FALSE
	ENDIF

	RETURN IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_OBJECTIVE_BLOCKER)
ENDFUNC

FUNC BOOL SHOULD_PED_BE_BLIPPED_FOR_CURRENT_RULE(INT iPed, INT &iBlipColour, BOOL &bCustomBlip)

	bCustomBlip = FALSE
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[ iTeam ]
	PED_INDEX tempPed
	
	IF IS_BIT_SET(MC_serverBD_4.iPedClearFailOnGoTo[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
		RETURN FALSE
	ENDIF
	
	//3887656 - Hide invisible ped's blips
	IF IS_PED_INVISIBLE(iPed)
		IF NOT GET_USINGSEETHROUGH()
			#IF IS_DEBUG_BUILD
				IF (GET_FRAME_COUNT()%30) = 0
					CDEBUG3LN(DEBUG_MC_BLIP, "[RCC MISSION] SHOULD_PED_BE_BLIPPED_FOR_CURRENT_RULE Blip ", iPed, "? No. Ped is invisible.")
				ENDIF
			#ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iped])
		tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
		
		// [ML] 5710951 - Override ped blip ranges
		IF iRule < FMMC_MAX_RULES
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_OVERRIDE_PED_BLIP_RANGE)				
			CDEBUG3LN(DEBUG_MC_BLIP, "[RCC MISSION] SHOULD_PED_BE_BLIPPED_FOR_CURRENT_RULE Current rule: ", iRule, " Override blip from rule ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sPedBlipStruct.sTeamBlip[iTeam].iBlipOverrideStartRule, " to rule ",  g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sPedBlipStruct.sTeamBlip[iTeam].iBlipOverrideEndRule)	
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sPedBlipStruct.sTeamBlip[iTeam].iBlipOverrideStartRule != -1
			AND iRule >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sPedBlipStruct.sTeamBlip[iTeam].iBlipOverrideStartRule
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sPedBlipStruct.sTeamBlip[iTeam].iBlipOverrideEndRule = -1
				OR iRule <= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sPedBlipStruct.sTeamBlip[iTeam].iBlipOverrideEndRule
					CDEBUG3LN(DEBUG_MC_BLIP, "[RCC MISSION] SHOULD_PED_BE_BLIPPED_FOR_CURRENT_RULE Blip ", iPed, "? ciBS_RULE13_OVERRIDE_PED_BLIP_RANGE is SET, and current rule has blip overrides SET, and within these rules. Returning TRUE")
					RETURN TRUE
				ELSE
					CDEBUG3LN(DEBUG_MC_BLIP, "[RCC MISSION] SHOULD_PED_BE_BLIPPED_FOR_CURRENT_RULE [ML] - g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sPedBlipStruct.sTeamBlip[iTeam].iBlipOverrideEndRule is OFF or current rule is beyond it")
				ENDIF
			ELSE
				CDEBUG3LN(DEBUG_MC_BLIP, "[RCC MISSION] SHOULD_PED_BE_BLIPPED_FOR_CURRENT_RULE Blip ", iPed, "? ciBS_RULE13_OVERRIDE_PED_BLIP_RANGE is SET, but current rule does not have blip overrides SET")
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_ForcedBlip)
			CDEBUG3LN(DEBUG_MC_BLIP, "[RCC MISSION] SHOULD_PED_BE_BLIPPED_FOR_CURRENT_RULE Blip ", iPed, " ci_MissionPedBS_ForcedBlip is SET. Returning TRUE")
			RETURN TRUE
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetSix, ciPED_BSSix_BlipOnlyInNoticeRange)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iBlipNoticeRange != -1
				// Safeguard: Only allow custom blip overrides if the ped exists and isn't injured.
				IF DOES_ENTITY_EXIST(tempPed)					
					IF NOT IS_PED_INJURED(tempPed) AND NOT IS_PED_INJURED(PLAYER_PED_ID())
						FLOAT fNoticableRadius = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iBlipNoticeRange)
						
						VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
						IF IS_PLAYER_IN_CREATOR_AIRCRAFT(PLAYER_ID())
						AND IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bInHeliGunCam	)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bInHeliGunCam
							vPlayerPos = GET_BLIP_COORD_OF_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID())
						ENDIF
						
						IF VDIST2(GET_ENTITY_COORDS(tempPed, FALSE), vPlayerPos) > fNoticableRadius*fNoticableRadius
							#IF IS_DEBUG_BUILD
								IF (GET_FRAME_COUNT()%90) = 0
									CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] SHOULD_PED_BE_BLIPPED_FOR_CURRENT_RULE Ped ", iPed, " blip being blocked due to OnlyInNoticeRange setting.")
									CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] SHOULD_PED_BE_BLIPPED_FOR_CURRENT_RULE Player location: ",vPlayerPos," Distance: ", VDIST2(GET_ENTITY_COORDS(tempPed, FALSE), vPlayerPos), " Notice range: ", fNoticableRadius ,"(",fNoticableRadius*fNoticableRadius,")")
								ENDIF
							#ENDIF
							RETURN FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sPedBlipStruct.fBlipHeightDifference != 0.0
			IF DOES_ENTITY_EXIST(tempPed)
				IF NOT IS_PED_INJURED(tempPed)
				AND NOT IS_PED_INJURED(LocalPlayerPed)
					VECTOR vPlayerPos = GET_ENTITY_COORDS(LocalPlayerPed)
					VECTOR vPedPos = GET_ENTITY_COORDS(tempPed)
					
					IF NOT IS_VECTOR_ZERO(vPlayerPos)
					AND NOT IS_VECTOR_ZERO(vPedPos)
						FLOAT fZDifference = ABSF(vPlayerPos.z - vPedPos.z)
						IF fZDifference > g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sPedBlipStruct.fBlipHeightDifference
							#IF IS_DEBUG_BUILD
							IF (GET_FRAME_COUNT()%90) = 0
								CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] SHOULD_PED_BE_BLIPPED_FOR_CURRENT_RULE Ped ", iPed, " blip being blocked due to fBlipHeightDifference setting, creator fBlipHeightDifference: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sPedBlipStruct.fBlipHeightDifference, " fZDifference: ", fZDifference)
							ENDIF
							#ENDIF
							RETURN FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
				
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_HIDE_INTERIOR_PED_BLIPS_IN_CANNON)
			IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bInHeliGunCam	)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bInHeliGunCam
			AND GET_INTERIOR_FROM_ENTITY(tempPed) != NULL
				#IF IS_DEBUG_BUILD
					IF (GET_FRAME_COUNT()%30) = 0
						CDEBUG3LN(DEBUG_MC_BLIP, "[RCC MISSION] SHOULD_PED_BE_BLIPPED_FOR_CURRENT_RULE SHOULD_PED_BE_BLIPPED_FOR_CURRENT_RULE Blip ", iPed, "? No. Ped is in an interior whilst using a scripter turret.")
					ENDIF
				#ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEleven, ciPED_BSEleven_OnlyBlipInSameInterior)
			IF GET_INTERIOR_FROM_ENTITY(PlayerPedToUse) != GET_INTERIOR_FROM_ENTITY(tempPed)
				#IF IS_DEBUG_BUILD
					IF (GET_FRAME_COUNT()%30) = 0
						CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] SHOULD_PED_BE_BLIPPED_FOR_CURRENT_RULE Ped ", iPed, " blip being blocked due to ciPED_BSEleven_OnlyBlipInSameInterior setting.")
					ENDIF
				#ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
		
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEleven, ciPed_BSEleven_BlipOnlyInScriptedGunTurret)
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bInHeliGunCam	)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bInHeliGunCam 
			#IF IS_DEBUG_BUILD
				IF (GET_FRAME_COUNT()%30) = 0
					CDEBUG3LN(DEBUG_MC_BLIP, "[RCC MISSION] SHOULD_PED_BE_BLIPPED_FOR_CURRENT_RULE Blip ", iPed, "? No. Ped is set up to only be blipped if in a scripted camera.")
				ENDIF
			#ENDIF
			RETURN FALSE
		ENDIF
	ENDIF

	IF MC_serverBD_4.iPedPriority[iped][ iTeam ] <= iRule
	AND MC_serverBD_4.iPedPriority[iped][ iTeam ] < FMMC_MAX_RULES
	
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_BlipOff)
		OR MC_serverBD_4.iPedRule[iped][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_CROWD_CONTROL

			IF NOT SHOULD_OBJ_BLOCKER_AFFECT_BLIP(iPed)
			
				IF MC_serverBD_4.iPedRule[iped][ iTeam ] = FMMC_OBJECTIVE_LOGIC_KILL
				OR IS_BIT_SET(MC_serverBD.iDeadPedPhotoBitset[ iTeam ][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					iBlipColour = BLIP_COLOUR_RED
					RETURN TRUE
				ELSE
					IF MC_serverBD_4.iPedRule[iped][ iTeam ] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD
						IF NOT IS_BIT_SET(MC_serverBD.iPedAtYourHolding[ iTeam ][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							iBlipColour = BLIP_COLOUR_BLUE
							RETURN TRUE
						ENDIF
					ELSE
						iBlipColour = BLIP_COLOUR_BLUE
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	// Are custom blip rules set?
	IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sPedBlipStruct.sTeamBlip[iTeam].iBlipOverrideStartRule != -1)
	OR (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sPedBlipStruct.sTeamBlip[iTeam].iBlipOverrideEndRule != -1)

		// Safeguard: Only allow custom blip overrides if the ped exists and isn't injured.
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iped])
			PED_INDEX pedID = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
			IF NOT IS_PED_INJURED(pedID)

				// Ped uninjured, check if we're on the right rule and what colours to use.
				INT iBlipFrom = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sPedBlipStruct.sTeamBlip[iTeam].iBlipOverrideStartRule
				INT iBlipTo = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sPedBlipStruct.sTeamBlip[iTeam].iBlipOverrideEndRule
		
				BOOL bAtRightPoint1
				BOOL bAtRightPoint2
		
				IF (iBlipFrom = -1)
				OR (iRule > iBlipFrom)
					bAtRightPoint1 = TRUE
				ELIF (iRule = iBlipFrom)
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,ciPED_BSThree_CustomBlipStartOnMidpoint_T0 +  iTeam )
						IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[ iTeam ],iBlipFrom)
							bAtRightPoint1 = TRUE
						ENDIF
					ELSE
						bAtRightPoint1 = TRUE
					ENDIF
				ENDIF
		
				IF (iBlipTo = -1)
				OR (iRule < iBlipTo)
					bAtRightPoint2 = TRUE
				ELIF (iRule = iBlipTo)
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,ciPED_BSThree_CustomBlipEndOnMidpoint_T0 +  iTeam )
						IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[ iTeam ],iBlipTo)
							bAtRightPoint2 = TRUE
						ENDIF
					ELSE
						bAtRightPoint2 = TRUE
					ENDIF
				ENDIF
		
				IF bAtRightPoint1 AND bAtRightPoint2
					bCustomBlip = TRUE
			
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iTeam[ iTeam ] = ciPED_RELATION_SHIP_LIKE
						iBlipColour = BLIP_COLOUR_BLUE
					ELSE
						iBlipColour = BLIP_COLOUR_RED
					ENDIF
			
					RETURN TRUE
			
				ENDIF

			ENDIF

		ENDIF

	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL CHECK_SHOULD_PED_HAVE_BLIP(INT iPed, INT &iBlipColour, BOOL &bCustomBlip)
	
	bCustomBlip = FALSE
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		
	IF iRule >= FMMC_MAX_RULES
		CDEBUG3LN(DEBUG_MC_BLIP, "[RCC MISSION] SHOULD_PED_HAVE_BLIP Blip ", iPed, "? No. Current rule greater than max.")
		RETURN FALSE
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_AiBlipsOnly)
		CDEBUG3LN(DEBUG_MC_BLIP, "[RCC MISSION] SHOULD_PED_HAVE_BLIP Blip ", iPed, "? No. Ped flagged to use AI blips only.")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD_4.iPedClearFailOnGoTo[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
		CDEBUG3LN(DEBUG_MC_BLIP, "[RCC MISSION] SHOULD_PED_HAVE_BLIP Blip ", iPed, "? No. Clear fail on Go To.")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFourteen, ciPED_BSFourteen_RemovePedBlipWhenAggroed)
	AND IS_BIT_SET(MC_serverBD.iPedAggroedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		CDEBUG3LN(DEBUG_MC_BLIP, "[RCC MISSION] SHOULD_PED_HAVE_BLIP Blip ", iPed, "? No. ciPED_BSFourteen_RemovePedBlipWhenAggroed is set and we're aggro'd")
		RETURN FALSE
	ENDIF
	
	//3887656 - Hide invisible ped's blips
	IF IS_PED_INVISIBLE(iPed)
		IF NOT GET_USINGSEETHROUGH()
			CDEBUG3LN(DEBUG_MC_BLIP, "[RCC MISSION] SHOULD_PED_HAVE_BLIP Blip ", iPed, "? No. Ped is invisible.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF SHOULD_PED_BE_BLIPPED_FOR_CURRENT_RULE(iPed, iBlipColour, bCustomBlip)
		//CDEBUG3LN(DEBUG_MC_BLIP, "[RCC MISSION] Blip ", iPed, "? Yes. Blip should be enabled for current rule.")
		RETURN TRUE
	ENDIF

	IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iped ].iPedBitsetTwo, ciPED_BSTwo_BlipOnFollow )
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iped ].iPriority[ iTeam ] <= iRule
	AND MC_serverBD_4.iPedPriority[iped][ iTeam ] < FMMC_MAX_RULES
		iBlipColour = BLIP_COLOUR_BLUE
		//CDEBUG3LN(DEBUG_MC_BLIP, "[RCC MISSION] Blip ", iPed, "? Yes. BlipOnFollow is set and current rule matches.")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_ForcedBlip)
		CDEBUG3LN(DEBUG_MC_BLIP, "[RCC MISSION] SHOULD_PED_HAVE_BLIP Blip ", iPed, "? Yes.  ci_MissionPedBS_ForcedBlip Special Forced Option..")
		RETURN TRUE
	ENDIF	
	
	#IF IS_DEBUG_BUILD
	IF bDebugBlipPrints
		CDEBUG3LN(DEBUG_MC_BLIP, "[RCC MISSION] SHOULD_PED_HAVE_BLIP Blip ", iPed, "? No. Current rule checks failed.")
	ENDIF #ENDIF
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_PED_HAVE_BLIP(INT iPed, INT &iBlipColour, BOOL &bCustomBlip)
	
	IF IS_BIT_SET(iSPHBBS_PedHasHadBlipCheckedBS[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
		RETURN IS_BIT_SET(iSPHBBS_PedShouldHaveBlipBS[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
	ENDIF
	
	SET_BIT(iSPHBBS_PedHasHadBlipCheckedBS[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
	
	IF CHECK_SHOULD_PED_HAVE_BLIP(iPed, iBlipColour, bCustomBlip)
		SET_BIT(iSPHBBS_PedShouldHaveBlipBS[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SUPPRESS_SCRIPT_PED_BLIP_TILL_COMBAT(INT iped, PED_INDEX TempPed = NULL, BOOL bAddBlip = FALSE)

	INT iBlipColour
	BOOL bCustomBlip
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_Blip_After_Combat)
		RETURN FALSE
	ENDIF
	
	IF DOES_BLIP_EXIST(biHostilePedBlip[iped].BlipID)
		IF bAddBlip
			IF NOT DOES_BLIP_EXIST( biPedBlip[ iped ] )
				IF SHOULD_PED_HAVE_BLIP(iped,iBlipColour,bCustomBlip)
					IF NOT IS_PED_INJURED(TempPed)
						CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION] Calling CREATE_PED_BLIP_WITH_COLOUR because ped ",iped," should have blip, ")
						CREATE_PED_BLIP_WITH_COLOUR( TempPed, iPed, iBlipColour )
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		RETURN FALSE
	ENDIF
	
	IF DOES_BLIP_EXIST(biPedBlip[iped])
		RETURN FALSE
	ENDIF

	RETURN TRUE

ENDFUNC



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: GENERAL PED CLIENT PROCESSING !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************



///PURPOSE: Boots the shooting sounds from the peds firing at the Insurgents during The Humane Labs - Insurgents heist prep mission
PROC DO_BIOLAB_GUN_BOOST(INT iped)
	IF iCurrentHeistMissionIndex = HBCA_BS_HUMANE_LABS_INSURGENTS
		IF iped >= 0 AND iped < FMMC_MAX_PEDS
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iped])
				IF MC_serverBD_2.iPedState[iped] = ciTASK_ATTACK_ENTITY 
					IF NOT IS_BIT_SET(iBiolabBoostingGun[GET_LONG_BITSET_INDEX(iped)],GET_LONG_BITSET_BIT(iped))
						
						ADD_ENTITY_TO_AUDIO_MIX_GROUP(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped]), "DLC_HEIST_BIOLAB_SHOOTER_GUN_BOOST_Group", 0.0)
						
						IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_HEIST_BIOLAB_ENEMY_GUN_BOOST_SCENE")
							START_AUDIO_SCENE("DLC_HEIST_BIOLAB_ENEMY_GUN_BOOST_SCENE")
						ENDIF
						SET_BIT(iBiolabBoostingGun[GET_LONG_BITSET_INDEX(iped)],GET_LONG_BITSET_BIT(iped))
						PRINTLN("[MJM] DO_BIOLAB_GUN_BOOST on ped ",iped)
					ENDIF
				ELSE
					IF IS_BIT_SET(iBiolabBoostingGun[GET_LONG_BITSET_INDEX(iped)],GET_LONG_BITSET_BIT(iped))
						CLEAR_BIT(iBiolabBoostingGun[GET_LONG_BITSET_INDEX(iped)],GET_LONG_BITSET_BIT(iped))
						IF iBiolabBoostingGun[0] = 0
						AND iBiolabBoostingGun[1] = 0
							STOP_AUDIO_SCENE("DLC_HEIST_BIOLAB_ENEMY_GUN_BOOST_SCENE")
						ENDIF
						PRINTLN("[MJM] DO_BIOLAB_GUN_BOOST stopped on ped ",iped)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL WAS_PED_NEARLY_RUN_OVER(PED_INDEX tempPed,INT iped)

ENTITY_INDEX tempEnt
VEHICLE_INDEX targetVeh
PED_INDEX targetPed
PLAYER_INDEX tempPlayer

	IF IS_PED_EVASIVE_DIVING(tempPed,tempEnt)
		IF DOES_ENTITY_EXIST(tempEnt)
			IF IS_ENTITY_A_VEHICLE(tempEnt)
				targetVeh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(tempEnt)
				IF IS_VEHICLE_DRIVEABLE(targetVeh)
					targetPed = GET_PED_IN_VEHICLE_SEAT(targetVeh)
				ENDIF
			ELSE
				//Bumping into a ped gets them into combat!
				targetPed = GET_PED_INDEX_FROM_ENTITY_INDEX(tempEnt)
			ENDIF
			IF NOT IS_PED_INJURED(targetPed)
				IF IS_PED_A_PLAYER(targetPed)
				AND (GET_PED_GROUP_INDEX(tempPed) != GET_PED_GROUP_INDEX(targetPed))
					tempPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(targetPed)
					INT iteam = GET_PLAYER_TEAM(tempPlayer)
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iTeam[iteam] = ciPED_RELATION_SHIP_DISLIKE
					OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iTeam[iteam] = ciPED_RELATION_SHIP_HATE
						CLEAR_PED_TASKS(tempPed)
						TASK_PED_COMBAT_PED(tempPed,iped,targetPed)
					ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC PROCESS_COP_DECOY_WANTED_LEVELS(INT iPed, PED_INDEX piPed)
	
	IF NOT bLocalPlayerPedOk
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iCopDecoyBS, ci_COP_DECOY_DEDUCTED_STAR_THIS_FRAME)
		SET_BLOCK_WANTED_FLASH(FALSE)
		CLEAR_BIT(iCopDecoyBS, ci_COP_DECOY_DEDUCTED_STAR_THIS_FRAME)
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_COP_DECOY_STARTED)
	AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_COP_DECOY_FLEEING)
	AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_COP_DECOY_EXPIRED)
	
		IF NOT IS_BIT_SET(iCopDecoyBS, ci_COP_DECOY_ENTERED_AREA)
			IF IS_ENTITY_IN_RANGE_COORDS(LocalPlayerPed, GET_ENTITY_COORDS(piPed, FALSE), 100.0)
				PRINTLN("[CopDecoy] PROCESS_COP_DECOY_WANTED_LEVELS() Player entered decoy area! Setting ci_COP_DECOY_ENTERED_AREA")
				SET_BIT(iCopDecoyBS, ci_COP_DECOY_ENTERED_AREA)
				
				//Further wanted processing for the player done in PROCESS_PLAYER_COP_DECOY in _players.sch incase decoy ped no longer exists during the forced 30 seconds.
			ENDIF
		ELSE
			//Make the gunner flee if a player gets too close.
			IF IS_ENTITY_IN_RANGE_COORDS(LocalPlayerPed, GET_ENTITY_COORDS(piPed, FALSE), 50.0)
				BROADCAST_FMMC_COP_DECOY_EVENT(iPed, FALSE, FALSE, TRUE)
			ENDIF
			
			//No more wanted level processing for this player if they entered the decoy area.
			EXIT
		ENDIF
	
		IF NOT IS_BIT_SET(iCopDecoyBS, ci_COP_DECOY_DEDUCT_1_STAR)
		AND NOT IS_BIT_SET(iCopDecoyBS, ci_COP_DECOY_DEDUCT_2_STAR)
			
			IF IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_COP_DECOY_DEDUCT_2_STAR)
				PRINTLN("[CopDecoy] PROCESS_COP_DECOY_WANTED_LEVELS() Decoy will deduct 2 stars!")
				
				FLOAT fTotalDecoyTime = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedCopDecoyActiveTime * 1000)
				iCopDecoyDeductFirstStarTime  = ROUND(fTotalDecoyTime * 0.25) //Remove first star 25% through decoy time. 
				iCopDecoyDeductSecondStarTime = ROUND(fTotalDecoyTime * 0.75) //Remove second star 75% through decoy time. 
				PRINTLN("[CopDecoy] PROCESS_COP_DECOY_WANTED_LEVELS() Total decoy time: ", ROUND(fTotalDecoyTime), ", Will deduct first star at: ", iCopDecoyDeductFirstStarTime, ", Will deduct second star at: ", iCopDecoyDeductSecondStarTime)
				
				SET_BIT(iCopDecoyBS, ci_COP_DECOY_DEDUCT_2_STAR)
			ELSE
				PRINTLN("[CopDecoy] PROCESS_COP_DECOY_WANTED_LEVELS() Decoy will deduct 1 star!")
					
				FLOAT fTotalDecoyTime = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedCopDecoyActiveTime * 1000)
				iCopDecoyDeductFirstStarTime = ROUND(fTotalDecoyTime * 0.50) //Remove star 50% through decoy time. 
				PRINTLN("[CopDecoy] PROCESS_COP_DECOY_WANTED_LEVELS() Total decoy time: ", ROUND(fTotalDecoyTime), ", Will deduct star at: ", iCopDecoyDeductFirstStarTime)
					
				SET_BIT(iCopDecoyBS, ci_COP_DECOY_DEDUCT_1_STAR)
			ENDIF
		ELSE
			IF HAS_NET_TIMER_STARTED(MC_serverBD_1.stCopDecoyActiveTimer)
				
				IF NOT IS_BIT_SET(iCopDecoyBS, ci_COP_DECOY_NEAR_END_DIA_TRIGGER)
					IF HAS_NET_TIMER_EXPIRED(MC_serverBD_1.stCopDecoyActiveTimer, (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedCopDecoyActiveTime * 1000) - ci_COP_DECOY_TIME_LEFT_FOR_DIA_TRIGGER) //15 seconds remaining.
						PRINTLN("[CopDecoy] PROCESS_COP_DECOY_WANTED_LEVELS() Set ci_COP_DECOY_NEAR_END_DIA_TRIGGER for 15 seconds remaining of decoy.")
						SET_BIT(iCopDecoyBS, ci_COP_DECOY_NEAR_END_DIA_TRIGGER)
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(iCopDecoyBS, ci_COP_DECOY_DEDUCT_2_STAR)
					IF NOT IS_BIT_SET(iCopDecoyBS, ci_COP_DECOY_DEDUCTED_STAR_1)
						IF HAS_NET_TIMER_EXPIRED(MC_serverBD_1.stCopDecoyActiveTimer, iCopDecoyDeductFirstStarTime)
							INT iNewWantedLevel = (GET_PLAYER_WANTED_LEVEL(LocalPlayer) - 1)
							IF iNewWantedLevel >= 0
								SET_PLAYER_WANTED_LEVEL(LocalPlayer, iNewWantedLevel)
								SET_PLAYER_WANTED_LEVEL_NOW(LocalPlayer)
								SET_MAX_WANTED_LEVEL(iNewWantedLevel) //Don't allow the player to gain higher than this from now on.
								
								//Force hidden evasion on this change if the player is in a vehicle with block wanted cone response active.
								IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_SET_BLIP_IN_BLOCK_WANTED_CONE_RESPONSE_VEHICLE)
								OR NOT IS_WANTED_AND_HAS_BEEN_SEEN_BY_COPS(LocalPlayer)
									SET_BLOCK_WANTED_FLASH(TRUE)
									FORCE_START_HIDDEN_EVASION(LocalPlayer)
									SET_BIT(iCopDecoyBS, ci_COP_DECOY_DEDUCTED_STAR_THIS_FRAME)
								ENDIF
								
								PRINTLN("[CopDecoy] PROCESS_COP_DECOY_WANTED_LEVELS() Deducted first wanted star.")
							ELSE
								PRINTLN("[CopDecoy] PROCESS_COP_DECOY_WANTED_LEVELS() Tried to deduct first wanted star but player's wanted was already 0.")
							ENDIF
							SET_BIT(iCopDecoyBS, ci_COP_DECOY_DEDUCTED_STAR_1)
						ENDIF
					ENDIF
					
					IF NOT IS_BIT_SET(iCopDecoyBS, ci_COP_DECOY_DEDUCTED_STAR_2)
						IF HAS_NET_TIMER_EXPIRED(MC_serverBD_1.stCopDecoyActiveTimer, iCopDecoyDeductSecondStarTime)
							INT iNewWantedLevel = (GET_PLAYER_WANTED_LEVEL(LocalPlayer) - 1)
							IF iNewWantedLevel >= 0
								SET_PLAYER_WANTED_LEVEL(LocalPlayer, iNewWantedLevel)
								SET_PLAYER_WANTED_LEVEL_NOW(LocalPlayer)
								SET_MAX_WANTED_LEVEL(iNewWantedLevel) //Don't allow the player to gain higher than this from now on.
								
								//Force hidden evasion on this change if the player is in a vehicle with block wanted cone response active.
								IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_SET_BLIP_IN_BLOCK_WANTED_CONE_RESPONSE_VEHICLE)
								OR NOT IS_WANTED_AND_HAS_BEEN_SEEN_BY_COPS(LocalPlayer)
									SET_BLOCK_WANTED_FLASH(TRUE)
									FORCE_START_HIDDEN_EVASION(LocalPlayer)
									SET_BIT(iCopDecoyBS, ci_COP_DECOY_DEDUCTED_STAR_THIS_FRAME)
								ENDIF
								
								PRINTLN("[CopDecoy] PROCESS_COP_DECOY_WANTED_LEVELS() Deducted second wanted star.")
							ELSE
								PRINTLN("[CopDecoy] PROCESS_COP_DECOY_WANTED_LEVELS() Tried to deduct second wanted star but player's wanted was already 0.")
							ENDIF
							SET_BIT(iCopDecoyBS, ci_COP_DECOY_DEDUCTED_STAR_2)
						ENDIF
					ENDIF
					
				ELIF IS_BIT_SET(iCopDecoyBS, ci_COP_DECOY_DEDUCT_1_STAR)

					IF NOT IS_BIT_SET(iCopDecoyBS, ci_COP_DECOY_DEDUCTED_STAR_1)
						IF HAS_NET_TIMER_EXPIRED(MC_serverBD_1.stCopDecoyActiveTimer, iCopDecoyDeductFirstStarTime)
							INT iNewWantedLevel = (GET_PLAYER_WANTED_LEVEL(LocalPlayer) - 1)
							IF iNewWantedLevel >= 0
								SET_PLAYER_WANTED_LEVEL(LocalPlayer, iNewWantedLevel)
								SET_PLAYER_WANTED_LEVEL_NOW(LocalPlayer)
								SET_MAX_WANTED_LEVEL(iNewWantedLevel) //Don't allow the player to gain higher than this from now on.
								
								//Force hidden evasion on this change if the player is in a vehicle with block wanted cone response active.
								IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_SET_BLIP_IN_BLOCK_WANTED_CONE_RESPONSE_VEHICLE)
								OR NOT IS_WANTED_AND_HAS_BEEN_SEEN_BY_COPS(LocalPlayer)
									SET_BLOCK_WANTED_FLASH(TRUE)
									FORCE_START_HIDDEN_EVASION(LocalPlayer)
									SET_BIT(iCopDecoyBS, ci_COP_DECOY_DEDUCTED_STAR_THIS_FRAME)
								ENDIF
								
								PRINTLN("[CopDecoy] PROCESS_COP_DECOY_WANTED_LEVELS() Deducted wanted star.")
							ELSE
								PRINTLN("[CopDecoy] PROCESS_COP_DECOY_WANTED_LEVELS() Tried to deduct wanted star but player's wanted was already 0.")
							ENDIF
							SET_BIT(iCopDecoyBS, ci_COP_DECOY_DEDUCTED_STAR_1)
						ENDIF
					ENDIF

				ENDIF
			ENDIF
		ENDIF	
	ENDIF
	
ENDPROC

PROC PROCESS_COP_DECOY_PED(INT iPed, PED_INDEX piPed, BOOL &ReleasedCopDecoyPed)
	
	IF IS_PED_INJURED(piPed)
		EXIT
	ENDIF

	//Additional processing (blips, hud, etc.) done in PROCESS_COP_DECOY_PED_BLIP_AND_HUD in fm_mission_controller_peds.sch
	PROCESS_COP_DECOY_WANTED_LEVELS(iPed, piPed)
	
	IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(piPed)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_COP_DECOY_STARTED)
		ADD_RELATIONSHIP_GROUP("relDecoyPed", relDecoyPed)
		SET_PED_RELATIONSHIP_GROUP_HASH(piPed, relDecoyPed)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, relDecoyPed)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relDecoyPed, RELGROUPHASH_PLAYER)
		SET_AMBIENT_COP_REL_TO_THIS_RELGROUP(ACQUAINTANCE_TYPE_PED_HATE, relDecoyPed)
	
		SET_PED_CONFIG_FLAG(piPed, PCF_ThisPedIsATargetPriorityForAI, TRUE)
		SET_PED_CONFIG_FLAG(piPed, PCF_TreatAsPlayerDuringTargeting, TRUE)
		SET_PED_CONFIG_FLAG(piPed, PCF_DecoyPed, TRUE)
		TELL_GROUP_PEDS_IN_AREA_TO_ATTACK(piPed, GET_ENTITY_COORDS(piPed), 500.0, RELGROUPHASH_COP)
		SET_ENTITY_INVINCIBLE(piPed, TRUE)
		
		BROADCAST_FMMC_COP_DECOY_EVENT(iPed, TRUE, FALSE, FALSE)
		PRINTLN("[CopDecoy] Set up Decoy ped relationship settings and starting decoy.")
		
	ELIF NOT IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_COP_DECOY_FLEEING)
	AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_COP_DECOY_EXPIRED)
		IF HAS_NET_TIMER_STARTED(MC_serverBD_1.stCopDecoyActiveTimer)
			INT iActiveTime = (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedCopDecoyActiveTime * 1000)
			
			IF NOT HAS_NET_TIMER_EXPIRED(MC_serverBD_1.stCopDecoyActiveTimer, iActiveTime)
				//Periodically task group peds to attack the decoy.
				IF NOT HAS_NET_TIMER_STARTED(tdRegisterDecoyAsTargetTimer)
					START_NET_TIMER(tdRegisterDecoyAsTargetTimer)
				ELIF HAS_NET_TIMER_EXPIRED(tdRegisterDecoyAsTargetTimer, 3000)
					TELL_GROUP_PEDS_IN_AREA_TO_ATTACK(piPed, GET_ENTITY_COORDS(piPed), 500.0, RELGROUPHASH_COP)
					REINIT_NET_TIMER(tdRegisterDecoyAsTargetTimer)
					PRINTLN("[CopDecoy] Calling TELL_GROUP_PEDS_IN_AREA_TO_ATTACK periodically.")
				ENDIF
			ELSE
				BROADCAST_FMMC_COP_DECOY_EVENT(iPed, FALSE, TRUE, FALSE)
			ENDIF
		ENDIF
	ELSE
		SET_ENTITY_INVINCIBLE(piPed, FALSE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piPed, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(piPed, CA_ALWAYS_FIGHT, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(piPed, CA_DISABLE_FLEE_FROM_COMBAT, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(piPed, CA_ALWAYS_FLEE, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(piPed, CA_FLEE_WHILST_IN_VEHICLE, TRUE)
		SET_PED_FLEE_ATTRIBUTES(piPed, FA_NEVER_FLEE, FALSE)
		SET_PED_FLEE_ATTRIBUTES(piPed, FA_COWER_INSTEAD_OF_FLEE, FALSE)
		SET_PED_FLEE_ATTRIBUTES(piPed, FA_DISABLE_COWER, TRUE)
		TASK_SMART_FLEE_COORD(piPed, GET_ENTITY_COORDS(piPed), 10000.0, -1)
		
		PRINTLN("[CopDecoy] Decoy ped set to flee, releasing ped.")
		CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
		ReleasedCopDecoyPed = TRUE
	ENDIF

ENDPROC

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: CLIENT PED CAPTURE RANGE PROCESSING !
//
//************************************************************************************************************************************************************

FUNC BOOL IS_ANY_PED_BEING_CAPTURED()
	INT iped
	FOR iped = 0 TO FMMC_MAX_PEDS - 1
		IF IS_BIT_SET(MC_serverBD_4.iCapturePedRequestPartBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_PED_CAPTURE_RANGE(INT iped)
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iped])
		IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
			IF IS_BIT_SET(MC_serverBD_4.iPedClearFailOnGoTo[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				bShouldStartControl = FALSE
				IF IS_BIT_SET(iHackingPedInRangeBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					CLEAR_BIT(iHackingPedInRangeBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				ENDIF
				IF DOES_BLIP_EXIST(biCaptureRangeBlip[iped])
					REMOVE_BLIP(biCaptureRangeBlip[iped])
				ENDIF
			ENDIF
			IF NOT IS_BIT_SET(MC_serverBD_4.iPedExplodeKillOnCaptureBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			AND NOT IS_BIT_SET(MC_serverBD_4.iPedClearFailOnGoTo[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				IF VDIST2(GET_ENTITY_COORDS(LocalPlayerPed), GET_ENTITY_COORDS(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped]))) < POW(TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCaptureRange), 2)
					IF NOT IS_BIT_SET(iHackingPedInRangeBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						SET_BIT(iHackingPedInRangeBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					ENDIF
					IF NOT IS_BIT_SET(MC_serverBD_4.iCapturePedRequestPartBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						IF NOT bShouldStartControl
							IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iAlreadyBeenCapturingPedBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							AND NOT IS_BIT_SET(iHackingPedHelpBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
								IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH("appSecuroHack")) = 0
									IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING)
									AND iHackStage != ciSECUROSERV_HACK_STAGE_HACK_COMPLETE
										PRINT_HELP_FOREVER("HACK_HELP2")
									ELSE
										PRINT_HELP_FOREVER("HACK_HELP")
									ENDIF
								ENDIF
								SET_BIT(iHackingPedHelpBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							ENDIF
						ENDIF
						IF bShouldStartControl
						OR IS_BIT_SET(MC_playerBD[iPartToUse].iAlreadyBeenCapturingPedBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
							PRINTLN("[KH] PROCESS_PED_CAPTURE_RANGE - Setting request capture bit, asking server to let this player capture the ped")
							iHackPercentage = 0
							SET_BIT(MC_playerBD[iPartToUse].iPedFollowingBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
							SET_BIT(MC_playerBD[iPartToUse].iRequestCapturePedBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							SET_BIT(MC_playerBD[iPartToUse].iAlreadyBeenCapturingPedBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							bShouldStartControl = FALSE
							IF IS_BIT_SET(iHackingPedHelpBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
								CLEAR_HELP()
								CLEAR_BIT(iHackingPedHelpBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							ENDIF
						ENDIF
					ELSE
						IF IS_BIT_SET(MC_playerBD[iPartToUse].iRequestCapturePedBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciSECUROSERV_DONT_FORCE_ON_SCREEN)	
							IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH("appSecuroHack")) = 0
							AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
							AND NOT IS_PLAYER_RESPAWNING(LocalPlayer)
							AND NOT IS_PED_DEAD_OR_DYING(LocalPlayerPed)
								PRINTLN("[JS][SECUROHACK] relaunching app")
								LAUNCH_CELLPHONE_APPLICATION( AppDummyApp0, TRUE, TRUE, FALSE )
							ENDIF
						ENDIF
						IF IS_BIT_SET(iHackingPedHelpBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							CLEAR_HELP()
							CLEAR_BIT(iHackingPedHelpBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						ENDIF
						bShouldStartControl = FALSE
					ENDIF
					
					IF DOES_BLIP_EXIST(biCaptureRangeBlip[iped])
						SET_BLIP_ALPHA(biCaptureRangeBlip[iped], 50)
					ENDIF
				ELSE
					IF IS_BIT_SET(iHackingPedInRangeBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						CLEAR_BIT(iHackingPedInRangeBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING)
							PRINTLN("[JS][SECUROHACK] Reseting hack stage")
							PLAY_SOUND_FRONTEND(-1, "Hack_Stop", "DLC_IE_SVM_Voltic2_Hacking_Sounds")
							iHackStage = ciSECUROSERV_HACK_STAGE_NO_SIGNAL
						ENDIF
					ENDIF
					IF IS_BIT_SET(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
						STOP_SOUND(iHackingLoopSoundID)
						RELEASE_SOUND_ID(iHackingLoopSoundID)
						iHackingLoopSoundID = -1
						CLEAR_BIT(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
					ENDIF
					IF IS_BIT_SET(iHackingPedHelpBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						CLEAR_HELP()
						CLEAR_BIT(iHackingPedHelpBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					ENDIF
					IF IS_BIT_SET(MC_playerBD[iPartToUse].iRequestCapturePedBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						PRINTLN("[KH] PROCESS_PED_CAPTURE_RANGE - Clearing the capture request bit for local player as they have gone out of range - range is: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCaptureRange)
						CLEAR_BIT(MC_playerBD[iPartToUse].iRequestCapturePedBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						CLEAR_BIT(MC_playerBD[iPartToUse].iPedFollowingBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
						bShouldStartControl = FALSE
					ENDIF
					
					IF DOES_BLIP_EXIST(biCaptureRangeBlip[iped])
						SET_BLIP_ALPHA(biCaptureRangeBlip[iped], 255)
					ENDIF
				ENDIF
			
				IF NOT DOES_BLIP_EXIST(biCaptureRangeBlip[iped])
					IF IS_PED_IN_ANY_VEHICLE(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped]))
						biCaptureRangeBlip[iped] = ADD_BLIP_FOR_RADIUS(GET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped])), FALSE), TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCaptureRange))
					ELSE
						biCaptureRangeBlip[iped] = ADD_BLIP_FOR_RADIUS(GET_ENTITY_COORDS(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped]), FALSE), TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCaptureRange))
					ENDIF
					SET_BLIP_COLOUR(biCaptureRangeBlip[iped], BLIP_COLOUR_YELLOW)
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(iHackingPedInRangeBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				CLEAR_BIT(iHackingPedInRangeBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			ENDIF
			IF DOES_BLIP_EXIST(biCaptureRangeBlip[iped])
				REMOVE_BLIP(biCaptureRangeBlip[iped])
			ENDIF
			IF IS_BIT_SET(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
				STOP_SOUND(iHackingLoopSoundID)
				RELEASE_SOUND_ID(iHackingLoopSoundID)
				iHackingLoopSoundID = -1
				CLEAR_BIT(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
			ENDIF
		ENDIF
		
//		IF MC_serverBD_4.iPedRule[iped][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_GO_TO
//			SET_BIT(iKeepFollowPedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
//		
//			SET_BIT(MC_playerBD[iPartToUse].iPedFollowingBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
//		ENDIF
	ELSE
		IF IS_BIT_SET(iHackingPedInRangeBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			CLEAR_BIT(iHackingPedInRangeBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
		ENDIF
		IF DOES_BLIP_EXIST(biCaptureRangeBlip[iped])
			REMOVE_BLIP(biCaptureRangeBlip[iped])
		ENDIF
		IF IS_BIT_SET(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
			STOP_SOUND(iHackingLoopSoundID)
			RELEASE_SOUND_ID(iHackingLoopSoundID)
			iHackingLoopSoundID = -1
			CLEAR_BIT(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPED_BSEight_KillExplodeOnCapture)
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iped])
			PED_INDEX pedToKill = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
			
			IF IS_BIT_SET(MC_serverBD_4.iPedExplodeKillOnCaptureBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				IF NOT IS_PED_INJURED(pedToKill)
					VEHICLE_INDEX vehPedIsIn
					INT iVehPedIsIn = -1
					IF IS_PED_IN_ANY_VEHICLE(pedToKill)
						vehPedIsIn = GET_VEHICLE_PED_IS_IN(pedToKill)
						iVehPedIsIn = IS_ENTITY_A_MISSION_CREATOR_ENTITY(vehPedIsIn)
					ENDIF
					IF MC_serverBD.iPedteamFailBitset[iPed] > 0
					OR (iVehPedIsIn != -1 AND MC_serverBD.iVehteamFailBitset[iVehPedIsIn] > 0)
						IF bIsLocalPlayerHost
							INT iTeam
							
							FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
								CLEAR_BIT(MC_serverBD.iMissionCriticalPed[iTeam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
								CLEAR_BIT(MC_serverBD.iPedteamFailBitset[iPed],iTeam)
								IF iVehPedIsIn != -1
									CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iTeam], iVehPedIsIn)
									CLEAR_BIT(MC_serverBD.iVehteamFailBitset[iVehPedIsIn], iTeam)
								ENDIF
							ENDFOR
						ENDIF
					ELSE
						IF bIsLocalPlayerHost
							IF IS_PED_IN_ANY_VEHICLE(pedToKill)
								IF NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(pedToKill))
									NETWORK_EXPLODE_VEHICLE(GET_VEHICLE_PED_IS_IN(pedToKill))
								ELSE
									NETWORK_REQUEST_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(pedToKill))
								ENDIF
							ELSE
								IF NETWORK_HAS_CONTROL_OF_ENTITY(pedToKill)
									SET_ENTITY_HEALTH(pedToKill, 0)
								ELSE
									NETWORK_REQUEST_CONTROL_OF_ENTITY(pedToKill)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF IS_BIT_SET(iHackingPedInRangeBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						CLEAR_BIT(iHackingPedInRangeBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					ENDIF
					
					IF DOES_BLIP_EXIST(biCaptureRangeBlip[iped])
						REMOVE_BLIP(biCaptureRangeBlip[iped])
					ENDIF
					
					IF bIsLocalPlayerHost
						CLEAR_BIT(MC_serverBD_4.iPedExplodeKillOnCaptureBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					ENDIF
				ENDIF
			ELSE
				IF IS_PED_INJURED(pedToKill)
					IF IS_BIT_SET(iHackingPedInRangeBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						CLEAR_BIT(iHackingPedInRangeBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					ENDIF
					IF DOES_BLIP_EXIST(biCaptureRangeBlip[iped])
						REMOVE_BLIP(biCaptureRangeBlip[iped])
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(biCaptureRangeBlip[iped])
				REMOVE_BLIP(biCaptureRangeBlip[iped])
			ENDIF
			IF IS_BIT_SET(iHackingPedInRangeBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				CLEAR_BIT(iHackingPedInRangeBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			ENDIF
			IF bIsLocalPlayerHost
				CLEAR_BIT(MC_serverBD_4.iPedExplodeKillOnCaptureBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: CLIENT PED GROUP PROCESSING !
//
//************************************************************************************************************************************************************

FUNC BOOL IS_PED_ON_A_RULE_ON_WHICH_THEY_SHOULD_JOIN_MY_PLAYER_GROUP(INT iped)
	
	BOOL bJoinGroup = FALSE
	INT iTeam = MC_playerBD[iPartToUse].iteam
	
	IF (MC_serverBD_4.iPedPriority[iped][iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam])
	AND (MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES)
		
		IF NOT ( IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_SUB_TEAM_ROLES) AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRolesBitset[MC_serverBD_4.iPedPriority[iped][iTeam]][MC_playerBD[iLocalPart].iCoronaRole], ciBS_ROLE_Cant_Complete_This_Rule) )
			
			SWITCH MC_serverBD_4.iPedRule[iped][iTeam]
				CASE FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
				CASE FMMC_OBJECTIVE_LOGIC_CAPTURE
					bJoinGroup = TRUE
				BREAK
				
				CASE FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD
					IF NOT IS_BIT_SET(MC_serverBD.iPedAtYourHolding[iTeam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						bJoinGroup = TRUE
					ENDIF
				BREAK
				
				CASE FMMC_OBJECTIVE_LOGIC_GO_TO
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFollow = ciPED_FOLLOW_ON
						bJoinGroup = TRUE
					ENDIF
				BREAK
			ENDSWITCH
			
		ENDIF
	ENDIF
	
	RETURN bJoinGroup
	
ENDFUNC

FUNC BOOL IS_PED_FOLLOWING_ANYONE(INT iped)
INT ipart	

	REPEAT GET_MAXIMUM_PLAYERS_ON_MISSION() ipart
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(ipart))
			IF IS_BIT_SET(MC_playerBD[ipart].iPedFollowingBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				
				#IF IS_DEBUG_BUILD
				IF bAddToGroupPrints
					PRINTLN("[RCC MISSION] IS_PED_FOLLOWING_ANYONE / bAddToGroupPrints - Ped ",iped," found to be following part ",ipart)
				ENDIF
				#ENDIF
				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
	
ENDFUNC

FUNC INT GET_NUMBER_PEDS_FOLLOWING_YOU()

INT iped	
INT ipedcount	

	FOR iped = 0 TO (MC_serverBD.iNumPedCreated - 1)
		IF IS_BIT_SET(MC_playerBD[iPartToUse].iPedFollowingBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			ipedcount++
		ENDIF
	ENDFOR
	
	RETURN ipedcount
	
ENDFUNC

FUNC BOOL CAN_ADD_PED_TO_GROUP(PED_INDEX tempPed,INT iped)	

	VEHICLE_INDEX TempVeh,TempVeh2
	PED_INDEX DriverPed
	
	PRINTLN("[RCC MISSION][GROUP CHECK] Standard Group Check = ", IS_BIT_SET(iLocalBoolCheck13, LBOOL13_CANADDPEDTOGROUP_JOINGROUPFROMVAN_STANDARDCHECK))
	PRINTLN("[RCC MISSION][GROUP CHECK] Doors Blown = ", IS_BIT_SET(iLocalBoolCheck13, LBOOL13_CANADDPEDTOGROUP_JOINGROUPFROMVAN_DOORSBLOWN))
	
	IF MC_serverBD_2.iPedState[iped] = ciTASK_GOTO_COORDS
		
		#IF IS_DEBUG_BUILD
		IF bAddToGroupPrints
			PRINTLN("[RCC MISSION][GROUP CHECK] CAN_ADD_PED_TO_GROUP / bAddToGroupPrints - Ped ",iped," is currently in GOTO_COORDS state, return FALSE")
		ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetSeven, ciPED_BSSeven_JoinGroupFromVan)
	AND NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_CANADDPEDTOGROUP_JOINGROUPFROMVAN_STANDARDCHECK)
		
		PRINTLN("[RCC MISSION][GROUP CHECK] Bit is set for van check")
		
		IF IS_PED_IN_ANY_VEHICLE(tempPed) 
			TempVeh = GET_VEHICLE_PED_IS_IN(tempPed)
		ENDIF
		
		IF DOES_ENTITY_EXIST(TempVeh)
		AND IS_VEHICLE_DRIVEABLE(TempVeh)
			IF GET_ENTITY_MODEL(TempVeh) = RIOT
				IF GET_VEHICLE_DOOR_ANGLE_RATIO(TempVeh, SC_DOOR_REAR_LEFT) > 0.25
				OR GET_VEHICLE_DOOR_ANGLE_RATIO(TempVeh, SC_DOOR_REAR_RIGHT) > 0.25
					SET_BIT(iLocalBoolCheck13, LBOOL13_CANADDPEDTOGROUP_JOINGROUPFROMVAN_DOORSBLOWN)
				ENDIF
			ELSE
				SET_BIT(iLocalBoolCheck13, LBOOL13_CANADDPEDTOGROUP_JOINGROUPFROMVAN_DOORSBLOWN) // They've probably already blown the doors open by this point
			ENDIF
			
			IF GET_ENTITY_SPEED(TempVeh) < 5.0
				PRINTLN("[RCC MISSION][GROUP CHECK] vehicle is going slow enough")
				IF IS_BIT_SET(iLocalBoolCheck13, LBOOL13_CANADDPEDTOGROUP_JOINGROUPFROMVAN_DOORSBLOWN)
					PRINTLN("[RCC MISSION][GROUP CHECK] At least one of the back doors is open or damaged")
					IF IS_ENTITY_AT_ENTITY(LocalPlayerPed,tempPed,<<20.0,20.0,20.0>>)
						PRINTLN("[RCC MISSION][GROUP CHECK] Player is close enough - returning true")
						SET_BIT(iLocalBoolCheck13, LBOOL13_CANADDPEDTOGROUP_JOINGROUPFROMVAN_STANDARDCHECK)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			SET_BIT(iLocalBoolCheck13, LBOOL13_CANADDPEDTOGROUP_JOINGROUPFROMVAN_STANDARDCHECK)
		ENDIF
	ENDIF
	
	PRINTLN("[RCC MISSION][GROUP CHECK] Standard Group Check, 2 = ", IS_BIT_SET(iLocalBoolCheck13, LBOOL13_CANADDPEDTOGROUP_JOINGROUPFROMVAN_STANDARDCHECK))
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetSeven,  ciPED_BSSeven_JoinGroupFromVan)
	OR IS_BIT_SET(iLocalBoolCheck13, LBOOL13_CANADDPEDTOGROUP_JOINGROUPFROMVAN_STANDARDCHECK)
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			PRINTLN("[RCC MISSION][GROUP CHECK] Setting player as in a vehicle")
			TempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)	
		ENDIF
		
		IF IS_PED_IN_ANY_VEHICLE(tempPed)
			IF GET_VEHICLE_PED_IS_IN(tempPed) = TempVeh
				SET_BIT(ilocalboolcheck,LBOOL_IN_GROUP_PED_VEH_TEMP)
				PRINTLN("[RCC MISSION][GROUP CHECK] Ped and player are in same vehicle - ped can join group")
				RETURN TRUE
			ELSE
				TempVeh2 = GET_VEHICLE_PED_IS_IN(tempPed)
				IF GET_ENTITY_SPEED(TempVeh2) < 2.0
					DriverPed = GET_PED_IN_VEHICLE_SEAT(TempVeh2)
					IF DriverPed = tempPed
						#IF IS_DEBUG_BUILD
						PRINTLN("[RCC MISSION][GROUP CHECK] Ped is in slow moving vehicle - ped can join group")
						#ENDIF
						RETURN TRUE
					ELSE
						IF IS_PED_INJURED(DriverPed)
							#IF IS_DEBUG_BUILD
							PRINTLN("[RCC MISSION][GROUP CHECK] Driver ped is injured - ped can join group")
							#ENDIF
							RETURN TRUE
						#IF IS_DEBUG_BUILD
						ELIF bAddToGroupPrints
							PRINTLN("[RCC MISSION][GROUP CHECK] CAN_ADD_PED_TO_GROUP / bAddToGroupPrints - Ped ",iped," is in a different car to me but they're not driving, return FALSE")
						#ENDIF
						ENDIF
					ENDIF
				#IF IS_DEBUG_BUILD
				ELIF bAddToGroupPrints
					PRINTLN("[RCC MISSION][GROUP CHECK] CAN_ADD_PED_TO_GROUP / bAddToGroupPrints - Ped ",iped," is in a different car to me and they're going too fast, return FALSE")
				#ENDIF
				ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			PRINTLN("[RCC MISSION][GROUP CHECK] Default returning true")
			#ENDIF
			RETURN TRUE
		ENDIF
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[RCC MISSION][GROUP CHECK] Neither the bit nor the bool are set")
	#ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC REMOVE_FMMC_PED_FROM_GROUP(PED_INDEX tempPed, INT iped, GROUP_INDEX group = NULL)
	
	IF ((group = NULL) AND (IS_PED_IN_GROUP(tempPed)))
	OR ((group != NULL) AND (IS_PED_GROUP_MEMBER(tempPed, group)))
		REMOVE_PED_FROM_GROUP(tempPed)
		PRINTLN("[RCC MISSION] REMOVE_FMMC_PED_FROM_GROUP - Remove ped ",iped," from group")
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[RCC MISSION] REMOVE_FMMC_PED_FROM_GROUP - Ped ",iped," isn't in group, but set group removal ped flag things anyway")
	#ENDIF
	ENDIF
	
	//SET_PED_CONFIG_FLAG(tempPed,PCF_PreventDraggedOutOfCarThreatResponse,FALSE)
	SET_PED_USING_ACTION_MODE(tempPed, FALSE, -1, "DEFAULT_ACTION")
	
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,FALSE)
	SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_PLAY_REACTION_ANIMS, TRUE)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour, ciPED_BSFour_DisableRagdolling)
		CLEAR_RAGDOLL_BLOCKING_FLAGS(tempPed, RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT)
	ENDIF
	
ENDPROC

PROC PROCESS_REMOTE_REMOVE_PED_FROM_GROUP(INT iped, PED_INDEX tempPed, BOOL bClearKeepFollowing = FALSE)
	
	IF ((MC_serverBD.iMaxLoopSize * fLastFrameTime) >= 250 // If it'll take more than 0.25s to get back to this ped in the staggered loop
		AND NOT IS_PED_A_HIGH_PRIORITY_PED(iped))
	OR GET_DISTANCE_BETWEEN_PEDS(LocalPlayerPed, tempPed) >= 15 // We're too far away, it's not worth the chance of getting a low-LOD ped
		PRINTLN("[RCC MISSION] PROCESS_REMOTE_REMOVE_PED_FROM_GROUP - BROADCAST remove ped from group: ",iped)
		BROADCAST_FMMC_REMOVE_PED_FROM_GROUP(iped)
		
		IF bClearKeepFollowing
			CLEAR_BIT(iKeepFollowPedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		ENDIF
	ELSE
		PRINTLN("[RCC MISSION] PROCESS_REMOTE_REMOVE_PED_FROM_GROUP - Request control to remove ped from group: ",iped)
		NETWORK_REQUEST_CONTROL_OF_ENTITY(tempPed)
	ENDIF
	
ENDPROC

// handles ped/player grouping
PROC PROCESS_PED_GROUPING(INT iped)

INT ipedcarrylimit
INT iJoinRange
PED_INDEX tempPed
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iped])
		IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
			
			tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
			
			IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_FINISHED)
			OR IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_START_SPECTATOR)
				IF IS_PED_GROUP_MEMBER(tempPed, giPlayerGroup)
					IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iped])
						PRINTLN("[RCC MISSION] PROCESS_PED_GROUPING - Remote remove ped from group since player finished: ",iped) 
						PROCESS_REMOTE_REMOVE_PED_FROM_GROUP(iped, tempPed)
						CLEAR_BIT(MC_playerBD[iPartToUse].iPedFollowingBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					ELSE
						PRINTLN("[RCC MISSION] PROCESS_PED_GROUPING - local remove ped from group since player finished: ",iped) 
						REMOVE_FMMC_PED_FROM_GROUP(tempPed, iped)
						CLEAR_BIT(MC_playerBD[iPartToUse].iPedFollowingBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					ENDIF
				ENDIF
			ENDIF
			
			IF iSpectatorTarget = -1
			AND MC_playerBD[iPartToUse].iObjectiveTypeCompleted = -1
				
				// This is the main entry point into giving a ped a group
				IF (MC_serverBD_4.iPedPriority[iped][MC_playerBD[iPartToUse].iteam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
				AND MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES)
				OR SHOULD_PED_ALWAYS_FOLLOW_TEAM(iped,MC_playerBD[iPartToUse].iteam)
				OR (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFollowTeamOnRuleBS[MC_playerBD[iPartToUse].iteam] > 0)
					
					IF IS_PED_ON_A_RULE_ON_WHICH_THEY_SHOULD_JOIN_MY_PLAYER_GROUP(iped)
					OR SHOULD_PED_ALWAYS_FOLLOW_TEAM(iped, MC_playerBD[iPartToUse].iteam)
					OR SHOULD_PED_FOLLOW_TEAM_ON_CURRENT_RULE(iped, MC_playerBD[iPartToUse].iteam)
						
						IF bPlayerToUseOK
//							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetTwelve, ciPed_BSTwelve_PedFollowStayInHeli)
//								// url:bugstar:4136784
//								PRINTLN("[RCC MISSION] PROCESS_PED_GROUPING_STAY_IN_HELI - Setting enabled")
//								IF IS_PED_IN_ANY_HELI(LocalPlayerPed)
//									PRINTLN("[RCC MISSION] PROCESS_PED_GROUPING_STAY_IN_HELI - Am in heli")
//									VEHICLE_INDEX myVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
//									PED_INDEX myDriver = NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(myVeh, VS_DRIVER))
//									IF LocalPlayerPed != myDriver
//									AND GET_PLAYER_TEAM(myDriver) = MC_playerBD[iPartToUse].iteam
//									AND GET_VEHICLE_PED_IS_IN(tempPed) = myVeh
//										PRINTLN("[RCC MISSION] PROCESS_PED_GROUPING_STAY_IN_HELI - I am the passenger, exiting early")
//										IF IS_PED_GROUP_MEMBER(tempPed, giPlayerGroup)
//											PRINTLN("[RCC MISSION] PROCESS_PED_GROUPING_STAY_IN_HELI - Ped is a member of my group, trying to kick him!")
//											IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iped])
//												PRINTLN("[RCC MISSION] PROCESS_PED_GROUPING_STAY_IN_HELI - Remote remove ped from group",iped) 
//												PROCESS_REMOTE_REMOVE_PED_FROM_GROUP(iped, tempPed)
//												CLEAR_BIT(MC_playerBD[iPartToUse].iPedFollowingBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
//											ELSE
//												PRINTLN("[RCC MISSION] PROCESS_PED_GROUPING_STAY_IN_HELI - local remove ped from group",iped) 
//												REMOVE_FMMC_PED_FROM_GROUP(tempPed, iped)
//												CLEAR_BIT(MC_playerBD[iPartToUse].iPedFollowingBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
//											ENDIF
//										ENDIF
//										/////////////////////////
//										EXIT ///////////////////
//										/////////////////////////
//									ENDIF
//								ENDIF
//							ENDIF
							IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
								IF IS_PED_IN_ANY_PLANE(LocalPlayerPed)
									iJoinRange = 20
								ELSE
									iJoinRange = 12
								ENDIF
							ELSE
								IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedFollowRange > 0
									iJoinRange = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedFollowRange 
								ELSE
									iJoinRange = 4
								ENDIF
							ENDIF
							
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetSeven, ciPED_BSSeven_JoinGroupFromVan)
								iJoinRange = 25
							ENDIF
							
							PRINTLN("[RCC MISSION] iJoinRange: ",iJoinRange) 
							FLOAT fDistance = GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed,tempPed)
							
							IF fDistance < iJoinRange
								IF CAN_ADD_PED_TO_GROUP(tempPed,iped)
									IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
									AND g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMaxDeliveryEntitiesCanCarry[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] > 0
										ipedcarrylimit = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMaxDeliveryEntitiesCanCarry[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] 
									ELSE
										ipedcarrylimit = FMMC_MAX_PEDS
									ENDIF
									
									// Adding a ped into the team's group
									IF MC_playerBD[iPartToUse].iPedCarryCount < ipedcarrylimit
										IF NOT IS_PED_IN_GROUP(tempPed)
											IF NOT IS_PED_FOLLOWING_ANYONE(iped)
												IF GET_NUMBER_PEDS_FOLLOWING_YOU() < ipedcarrylimit
													IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iped])
														PRINTLN("[RCC MISSION] PROCESS_PED_GROUPING - Requesting control of ped ",iped," to add into my group")
														NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iped])
													ELSE
														giPlayerGroup = GET_PLAYER_GROUP(LocalPlayer)
														REMOVE_PED_DEFENSIVE_AREA(tempPed,TRUE)
														
														IF GET_BEST_PED_WEAPON(tempPed) = WEAPONTYPE_UNARMED
															IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn != IG_MONEY
																SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,TRUE)
															ENDIF
														ENDIF
														
														IF GET_BEST_PED_WEAPON(tempPed) != WEAPONTYPE_UNARMED
														OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour, ciPED_BSFour_AlwaysUseActionModeWhenInGroup)
															SET_PED_USING_ACTION_MODE(tempPed, TRUE, -1, "DEFAULT_ACTION")
														ENDIF
														
														IF CAN_PED_TASKS_BE_CLEARED(tempPed)
														AND NOT IS_PED_DOING_ANIMATION_TRANSITION(iped,tempPed,TRUE)
														AND NOT IS_PED_IN_ANY_VEHICLE(tempPed)
															CLEAR_PED_TASKS(tempPed)
															CPRINTLN( DEBUG_SIMON, "PROCESS_PED_GROUPING - Setting PRF_ForceScanForEventsThisFrame" )
															SET_PED_RESET_FLAG(tempPed, PRF_ForceScanForEventsThisFrame, TRUE)
														ENDIF
														
														SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_PLAY_REACTION_ANIMS, FALSE)
														SET_PED_COMBAT_ATTRIBUTES(tempPed,CA_LEAVE_VEHICLES,FALSE)
														
														SET_PED_CONFIG_FLAG(tempPed, PCF_TeleportToLeaderVehicle, TRUE)
														
														IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetTwelve, ciPed_BSTwelve_PedFollowStayInHeli)
															SET_PED_CONFIG_FLAG(tempPed, PCF_DisableJumpingFromVehiclesAfterLeader, TRUE)
														ENDIF
														
														IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour, ciPED_BSFour_DisableRagdolling)
															SET_RAGDOLL_BLOCKING_FLAGS(tempPed, RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT)
														ENDIF
														
														//SET_PED_CONFIG_FLAG(tempPed,PCF_PreventDraggedOutOfCarThreatResponse,TRUE)
														
														SET_PED_AS_GROUP_MEMBER(tempPed, giPlayerGroup)
														
														FLOAT fFollowDist = 6.0
														
														IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset,ciPED_BS_GroupAlongSide)
															SET_GROUP_FORMATION(giPlayerGroup, FORMATION_LINE_ABREAST)
															fFollowDist = 2.5
															SET_PED_GROUP_MEMBER_PASSENGER_INDEX(tempPed, VS_FRONT_RIGHT)
														ENDIF
														
														IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iGroupFollowRange != 0
															fFollowDist = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iGroupFollowRange)
														ENDIF
														
														SET_GROUP_FORMATION_SPACING(giPlayerGroup, fFollowDist)
														SET_GROUP_SEPARATION_RANGE(giPlayerGroup,36.0)
														
														IF ( g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicleSeatPreference > ciFMMC_SeatPreference_Driver )
															SET_PED_GROUP_MEMBER_PASSENGER_INDEX( tempPed, GET_PED_PREFERRED_SEAT_FROM_CREATOR( iPed ) )
														ENDIF
														
														// Giving a partcular ped a particular shout
														IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn =	s_m_y_robber_01
															PLAY_PED_AMBIENT_SPEECH_AND_CLONE_NATIVE(tempPed,"FALL_BACK","SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
														ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn != IG_LESTERCREST //It's you! 2124220
															IF NOT IS_BIT_SET(iDialogueHiBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
																PLAY_PED_AMBIENT_SPEECH_AND_CLONE_NATIVE(tempPed,"GENERIC_HI","SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
																SET_BIT(iDialogueHiBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
															ENDIF
														ENDIF
														
														// State checking on this ped
														IF MC_serverBD_4.iPedRule[iped][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_GO_TO
															SET_BIT(iKeepFollowPedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
															
															SET_BIT(MC_playerBD[iPartToUse].iPedFollowingBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
															
														ELIF MC_serverBD_4.iPedRule[iped][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
															BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_COLLECT_PED,0,MC_playerBD[iPartToUse].iteam,-1,NULL,ci_TARGET_PED,iped)
															IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFollow = ciPED_FOLLOW_ON
															OR SHOULD_PED_ALWAYS_FOLLOW_TEAM(iped,MC_playerBD[iPartToUse].iteam)
																SET_BIT(iKeepFollowPedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
															ENDIF
															
															SET_BIT(MC_playerBD[iPartToUse].iPedFollowingBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
															
														ELIF MC_serverBD_4.iPedRule[iped][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
															IF (NOT SHOULD_PED_CLEAN_UP_ON_DELIVERY(iped,MC_playerBD[iPartToUse].iteam))
															AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFollow = ciPED_FOLLOW_ON
															OR SHOULD_PED_ALWAYS_FOLLOW_TEAM(iped,MC_playerBD[iPartToUse].iteam))	
																SET_BIT(iKeepFollowPedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
															ENDIF
															
															SET_BIT(MC_playerBD[iPartToUse].iPedFollowingBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
														ELSE
															
															IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFollow = ciPED_FOLLOW_ON
																SET_BIT(iKeepFollowPedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
															ENDIF
															
															IF SHOULD_PED_ALWAYS_FOLLOW_TEAM(iped,MC_playerBD[iPartToUse].iteam)
																SET_BIT(MC_playerBD[iPartToUse].iPedFollowingBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
																SET_BIT(iKeepFollowPedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
															ELIF SHOULD_PED_FOLLOW_TEAM_ON_CURRENT_RULE(iped,MC_playerBD[iPartToUse].iteam)
																SET_BIT(MC_playerBD[iPartToUse].iPedFollowingBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
															ENDIF
															
															BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS( TICKER_EVENT_COLLECT_PED,0,MC_playerBD[iPartToUse].iteam,-1,NULL,ci_TARGET_PED,iped )
														ENDIF
														
														PRINTLN("[RCC MISSION] PROCESS_PED_GROUPING - local add ped ",iped," my to group, playerBD iPedFollowingBitset = ",IS_BIT_SET(MC_playerBD[iPartToUse].iPedFollowingBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed)),", local iKeepFollowPedBitset = ",IS_BIT_SET(iKeepFollowPedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed)))
													ENDIF
												#IF IS_DEBUG_BUILD
												ELIF bAddToGroupPrints
													PRINTLN("[RCC MISSION] PROCESS_PED_GROUPING / bAddToGroupPrints - Can't add ped ",iped," to my group as we're at follow limit (following ",GET_NUMBER_PEDS_FOLLOWING_YOU()," >= ipedcarrylimit ",ipedcarrylimit,")")
												#ENDIF
												ENDIF
											#IF IS_DEBUG_BUILD
											ELIF bAddToGroupPrints
												PRINTLN("[RCC MISSION] PROCESS_PED_GROUPING / bAddToGroupPrints - Can't add ped ",iped," to my group as they're already following someone")
											#ENDIF
											ENDIF
										#IF IS_DEBUG_BUILD
										ELIF bAddToGroupPrints
											PRINTLN("[RCC MISSION] PROCESS_PED_GROUPING / bAddToGroupPrints - Can't add ped ",iped," to my group as they're already in a group")
											
											PED_INDEX pdLeader = GET_PED_AS_GROUP_LEADER(GET_PED_GROUP_INDEX(tempPed))
											
											IF IS_PED_A_PLAYER(pdLeader)
												PLAYER_INDEX plLeader = NETWORK_GET_PLAYER_INDEX_FROM_PED(pdLeader)
												
												IF IS_NET_PLAYER_OK(plLeader)
													IF NETWORK_IS_PLAYER_A_PARTICIPANT(plLeader)
														PRINTLN("[RCC MISSION] PROCESS_PED_GROUPING - Can't add ped ",iped," to my group as in part ",NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(plLeader)),"'s group")
													ELSE
														PRINTLN("[RCC MISSION] PROCESS_PED_GROUPING - Can't add ped ",iped," to my group as in some non-participant player's group")
													ENDIF
												ELSE
													PRINTLN("[RCC MISSION] PROCESS_PED_GROUPING - Can't add ped ",iped," to my group as in some non-okay player's group")
												ENDIF
											ELSE
												PRINTLN("[RCC MISSION] PROCESS_PED_GROUPING - Can't add ped ",iped," to my group as in some ped's group (not a player...?)")
											ENDIF
										#ENDIF
										ENDIF
									#IF IS_DEBUG_BUILD
									ELIF bAddToGroupPrints
										PRINTLN("[RCC MISSION] PROCESS_PED_GROUPING / bAddToGroupPrints - Can't add ped ",iped," to my group as we're at carry limit (following ",MC_playerBD[iPartToUse].iPedCarryCount," >= ipedcarrylimit ",ipedcarrylimit,")")
									#ENDIF
									ENDIF
								#IF IS_DEBUG_BUILD
								ELIF bAddToGroupPrints
									PRINTLN("[RCC MISSION] PROCESS_PED_GROUPING / bAddToGroupPrints - Can't add ped ",iped," to my group as CAN_ADD_PED_TO_GROUP returning FALSE")
								#ENDIF
								ENDIF
							ELSE // Else this ped is outside the iJoinRange range
								IF IS_BIT_SET(MC_playerBD[iPartToUse].iPedFollowingBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
									IF fDistance > 35 OR NOT IS_PED_GROUP_MEMBER(tempPed, giPlayerGroup)
										IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iped])
											IF IS_PED_GROUP_MEMBER(tempPed, giPlayerGroup)
											OR NOT IS_PED_IN_GROUP(tempPed)
												PRINTLN("[RCC MISSION] PROCESS_PED_GROUPING - Remote remove ped from group since over 35 meters: ",iped)
												PROCESS_REMOTE_REMOVE_PED_FROM_GROUP(iped, tempPed, TRUE)
											ELSE
												PRINTLN("[RCC MISSION] PROCESS_PED_GROUPING - clear ped group data since over 35 meters: ",iped)
												CLEAR_BIT(MC_playerBD[iPartToUse].iPedFollowingBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
												CLEAR_BIT(iKeepFollowPedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
											ENDIF
										ELSE
											PRINTLN("[RCC MISSION] PROCESS_PED_GROUPING - local remove ped from group since over 35 meters: ",iped)
											REMOVE_FMMC_PED_FROM_GROUP(tempPed, iped, giPlayerGroup)
											CLEAR_BIT(MC_playerBD[iPartToUse].iPedFollowingBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
											CLEAR_BIT(iKeepFollowPedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
										ENDIF
									ENDIF
								#IF IS_DEBUG_BUILD
								ELIF bAddToGroupPrints
									PRINTLN("[RCC MISSION] PROCESS_PED_GROUPING / bAddToGroupPrints - Trying to add ped ",iped," into group but too far, distance to ped = ",fDistance,", iJoinRange = ",iJoinRange)
								#ENDIF
								ENDIF
							ENDIF
						ELSE //Player is injured
							IF IS_BIT_SET(MC_playerBD[iPartToUse].iPedFollowingBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
								IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iped])
									IF IS_PED_GROUP_MEMBER(tempPed, giPlayerGroup)
									OR NOT IS_PED_IN_GROUP(tempPed)
										PRINTLN("[RCC MISSION] PROCESS_PED_GROUPING - Remote remove ped from group since player not ok: ",iped)
										PROCESS_REMOTE_REMOVE_PED_FROM_GROUP(iped, tempPed)
									ELSE
										PRINTLN("[RCC MISSION] PROCESS_PED_GROUPING - clear ped group data since player not ok: ",iped)
										CLEAR_BIT(MC_playerBD[iPartToUse].iPedFollowingBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
									ENDIF
								ELSE
									PRINTLN("[RCC MISSION] PROCESS_PED_GROUPING - local remove ped from group since player not ok: ",iped)
									REMOVE_FMMC_PED_FROM_GROUP(tempPed, iped, giPlayerGroup)
									CLEAR_BIT(MC_playerBD[iPartToUse].iPedFollowingBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
								ENDIF
							#IF IS_DEBUG_BUILD
							ELIF bAddToGroupPrints
								PRINTLN("[RCC MISSION] PROCESS_PED_GROUPING / bAddToGroupPrints - Trying to add ped ",iped," into group but I'm injured")
							#ENDIF
							ENDIF
						ENDIF
					ELSE //Ped no longer on a rule which requires them to be following
						IF IS_BIT_SET(MC_playerBD[iPartToUse].iPedFollowingBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
							IF NOT IS_BIT_SET(iKeepFollowPedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
								IF IS_PED_GROUP_MEMBER(tempPed, giPlayerGroup)
									IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iped])
										PRINTLN("[RCC MISSION] PROCESS_PED_GROUPING - Remote remove ped from group since team logic not collect or control: ",iped)
										PROCESS_REMOTE_REMOVE_PED_FROM_GROUP(iped, tempPed)
									ELSE
										PRINTLN("[RCC MISSION] PROCESS_PED_GROUPING - local remove ped from group since team logic not collect or control: ",iped)
										REMOVE_FMMC_PED_FROM_GROUP(tempPed, iped, giPlayerGroup)
										CLEAR_BIT(MC_playerBD[iPartToUse].iPedFollowingBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
									ENDIF
								ENDIF
							ELSE
								CLEAR_BIT(MC_playerBD[iPartToUse].iPedFollowingBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
							ENDIF
						ENDIF
					ENDIF
				ELSE //Ped is no longer a priority
					IF IS_BIT_SET(MC_playerBD[iPartToUse].iPedFollowingBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
						//If they are no longer needed (includes extra objectives!):
						IF MC_serverBD_4.iPedPriority[iped][MC_playerBD[iPartToUse].iTeam] = FMMC_PRIORITY_IGNORE
							CLEAR_BIT(MC_playerBD[iPartToUse].iPedFollowingBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE // If the ped doing the following is injured:
			IF iSpectatorTarget = -1
				IF IS_BIT_SET(MC_playerBD[iPartToUse].iPedFollowingBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					CLEAR_BIT(MC_playerBD[iPartToUse].iPedFollowingBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				ENDIF
			ENDIF
		ENDIF
	ELSE // If the ped's network ID doesn't exist:
		IF iSpectatorTarget = -1
			IF IS_BIT_SET(MC_playerBD[iPartToUse].iPedFollowingBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				CLEAR_BIT(MC_playerBD[iPartToUse].iPedFollowingBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			ENDIF
		ENDIF
	ENDIF
ENDPROC
INT iPedPreventMigrationBitset[FMMC_MAX_PEDS_BITSET]
INT iPedPreventMigrationBitsetNext[FMMC_MAX_PEDS_BITSET]
PROC PREVENT_PED_MIGRATION_THIS_UPDATE(PED_INDEX tempPed, INT iPed) // Must be called every body update or gets reset
	IF NOT IS_BIT_SET(iPedPreventMigrationBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
		PRINTLN("[LM][PROCESS_PED_BODY] - PED_CAN_MIGRATE set to FALSE on ped ", iped, " id: ", NATIVE_TO_INT(tempPed))
		SET_NETWORK_ID_CAN_MIGRATE(PED_TO_NET(tempPed), FALSE)
		SET_BIT(iPedPreventMigrationBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
	ENDIF
	SET_BIT(iPedPreventMigrationBitsetNext[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
ENDPROC
#IF IS_DEBUG_BUILD
VECTOR debugPedThrowTarget[FMMC_MAX_PEDS]
#endif
ENTITY_INDEX projectileEntity[FMMC_MAX_PEDS]
SCRIPT_TIMER projectileTimer[FMMC_MAX_PEDS]
//INT iProjectileThrownBit
PROC TASK_THROW_PROJECTILES(PED_INDEX tempPed, INT iped)
	IF HAS_NET_TIMER_STARTED(projectileTimer[iPed])
		PREVENT_PED_MIGRATION_THIS_UPDATE(tempPed, iPed)
		#IF IS_DEBUG_BUILD
		IF PED_THROWING_DRAW_DEBUG
			DRAW_DEBUG_SPHERE(debugPedThrowTarget[iPed], 3.0, 255, 0, 0, 128)
			DRAW_DEBUG_LINE(GET_ENTITY_COORDS(tempPed), debugPedThrowTarget[iPed], 255,0,0,255)
		ENDIF
		#ENDIF
		IF IS_ENTITY_ALIVE(projectileEntity[iPed])
			#IF IS_DEBUG_BUILD
			IF PED_THROWING_DRAW_DEBUG
				INT iTimeLeft = g_FMMC_STRUCT.iStickyBombExplodeTime*1000 - ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), projectileTimer[iPed].Timer))
				FLOAT fTimeLeft = TO_FLOAT(iTimeLeft) / TO_FLOAT(g_FMMC_STRUCT.iStickyBombExplodeTime*1000)
				INT iRed = 0
				IF (fTimeLeft > 0.49 AND fTimeLeft < 0.5)
				OR (fTimeLeft > 0.39 AND fTimeLeft < 0.4)
				OR (fTimeLeft > 0.29 AND fTimeLeft < 0.3)
				OR (fTimeLeft > 0.14 AND fTimeLeft < 0.15)
				OR (fTimeLeft > 0.09 AND fTimeLeft < 0.1)
				OR (fTimeLeft > 0.04 AND fTimeLeft < 0.05)
					iRed = 255
				ENDIF
				DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(projectileEntity[iPed]), TO_FLOAT(g_FMMC_STRUCT.iStickyBombExplodeRange), iRed, 0, 255, 128)
				//DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(projectileEntity[iPed]), TO_FLOAT(g_FMMC_STRUCT.iStickyBombExplodeRange*2)*fTimeLeft, 0, 255, 0, 128)
			ENDIF
			#ENDIF
			IF HAS_NET_TIMER_EXPIRED(projectileTimer[iPed], g_FMMC_STRUCT.iStickyBombExplodeTime*1000)
				#IF IS_DEBUG_BUILD
				VECTOR v = GET_ENTITY_COORDS(projectileEntity[iPed])
				#ENDIF
				IF GET_DISTANCE_BETWEEN_ENTITIES(tempPed, projectileEntity[iPed]) >= 10.0
					CLEAR_AREA_OF_PROJECTILES(GET_ENTITY_COORDS(tempPed), 5)
					SET_CURRENT_PED_WEAPON(tempPed, WEAPONTYPE_UNARMED, TRUE)
					PRINTLN("[LM][TASK_THROW_PROJECTILES] - ", iped, " Exploding (timer) at ", v)
					EXPLODE_PROJECTILES(tempPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].gun, TRUE)
				ELSE
					PRINTLN("[LM][TASK_THROW_PROJECTILES] - ", iped, " Deleting without exploding at ", v, " id: ", NATIVE_TO_INT(projectileEntity[iPed]))
					REMOVE_ALL_PROJECTILES_OF_TYPE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].gun)
				ENDIF
				RESET_NET_TIMER(projectileTimer[iPed])
			ELSE
				PED_INDEX piClosestPlayerPed = GET_CLOSEST_VISIBLE_PLAYER(iPed, tempPed, TRUE, 150.0)
				IF piClosestPlayerPed != NULL
					FLOAT distance = GET_DISTANCE_BETWEEN_ENTITIES(piClosestPlayerPed, projectileEntity[iPed])
					IF (distance <= g_FMMC_STRUCT.iStickyBombExplodeRange)
					AND GET_DISTANCE_BETWEEN_ENTITIES(tempPed, projectileEntity[iPed]) >= 10.0
						CLEAR_AREA_OF_PROJECTILES(GET_ENTITY_COORDS(tempPed), 5)
						SET_CURRENT_PED_WEAPON(tempPed, WEAPONTYPE_UNARMED, TRUE)
						VECTOR v = GET_ENTITY_COORDS(projectileEntity[iPed])
						PRINTLN("[LM][TASK_THROW_PROJECTILES] - ", iped, " Exploding (proximity) at ", v)
						EXPLODE_PROJECTILES(tempPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].gun, TRUE)
						RESET_NET_TIMER(projectileTimer[iPed])
					ENDIF
				ENDIF
			ENDIF
		ELSE
			VECTOR tempPos
			IF GET_PROJECTILE_OF_PROJECTILE_TYPE_WITHIN_DISTANCE(tempPed,g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].gun,100.0,tempPos,projectileEntity[iPed])
				SET_ENTITY_AS_MISSION_ENTITY(projectileEntity[iPed], FALSE, TRUE)
				SET_ENTITY_INVINCIBLE(projectileEntity[iPed], TRUE)
				PRINTLN("[LM][TASK_THROW_PROJECTILES] - ", iped, " Found sticky")
			ELSE
				IF HAS_NET_TIMER_EXPIRED(projectileTimer[iPed], g_FMMC_STRUCT.iStickyBombExplodeTime*2*1000)
				// This is a safety feature to stop the ped getting stuck somehow, because the projectile was missed or something
					RESET_NET_TIMER(projectileTimer[iPed])
					PRINTLN("[LM][TASK_THROW_PROJECTILES] - ", iped, " Timer expired, failed to find sticky!")
				ELIF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_THROW_PROJECTILE) // This sometimes has a false positive
					//RESET_NET_TIMER(projectileTimer[iPed]) 
					PRINTLN("[LM][TASK_THROW_PROJECTILES] - ", iped, " Throw task ended without spawning a projectile!")
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_THROW_PROJECTILE)
			IF IS_WEAPON_A_PROJECTILE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].gun)
				PED_INDEX target
				target = GET_CLOSEST_VISIBLE_PLAYER(iPed, tempPed, TRUE, PED_THROWING_MAX_RANGE, FALSE)
				IF target != NULL
				AND NOT IS_ENTITY_ALIVE(projectileEntity[iPed]) //Should stop multiple from being up at once
					VECTOR targetCoord
					targetCoord = GET_ENTITY_COORDS(target)
					PRINTLN("[LM][TASK_THROW_PROJECTILES] - ", iped, " Target location ", targetCoord)
					targetCoord += (GET_ENTITY_VELOCITY(target) * PED_THROWING_LEAD_SCALAR)
					PRINTLN("[LM][TASK_THROW_PROJECTILES] - ", iped, " Added lead vector, throwing at:  ", targetCoord)
					IF PED_THROWING_ACCURACY_RADIUS > 0.0
						targetCoord = GET_RANDOM_POINT_IN_SPHERE(targetCoord, PED_THROWING_ACCURACY_RADIUS)
						PRINTLN("[LM][TASK_THROW_PROJECTILES] - ", iped, " Added inaccuracy: ", targetCoord)
					ENDIF
					#IF IS_DEBUG_BUILD
					debugPedThrowTarget[iPed] = targetCoord
					#ENDIF
					
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].gun = WEAPONTYPE_STICKYBOMB
						REMOVE_ALL_PROJECTILES_OF_TYPE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].gun)
						PRINTLN("[TASK_THROW_PROJECTILES] Calling REMOVE_ALL_PROJECTILES_OF_TYPE to remove other sticky bombs before throwing")
					ENDIF
					
					GIVE_WEAPON_TO_PED(tempPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].gun, 1, TRUE)
					SET_CURRENT_PED_WEAPON(tempPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].gun, TRUE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( tempPed, TRUE )
					ENTITY_INDEX attachedVeh = NULL
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle != -1
						IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[ g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle ])
						AND IS_ENTITY_ALIVE(NET_TO_VEH( MC_serverBD_1.sFMMC_SBD.niVehicle[ g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle ] ))
							attachedVeh = NET_TO_VEH( MC_serverBD_1.sFMMC_SBD.niVehicle[ g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle ] )
						ENDIF
					ENDIF
					TASK_THROW_PROJECTILE(tempPed, targetCoord, attachedVeh, TRUE)
					
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].gun = WEAPONTYPE_STICKYBOMB
						START_NET_TIMER(projectileTimer[iPed])
						projectileEntity[iPed] = INT_TO_NATIVE(ENTITY_INDEX, -1)
						PREVENT_PED_MIGRATION_THIS_UPDATE(tempPed, iPed) // url:bugstar:3596258
					ENDIF
				ELSE
					//DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(tempPed), PED_THROWING_MAX_RANGE, 255, 0, 0, 128)
					//PROCESS_GENERAL_COMBAT_TASK_BODY(tempPed,iped,bheli,bplane,bBoat,bDriver,tempveh)
				ENDIF
			ELSE
				//PROCESS_GENERAL_COMBAT_TASK_BODY(tempPed,iped,bheli,bplane,bBoat,bDriver,tempveh)
			ENDIF
		ENDIF
	ENDIF
ENDPROC
// Angle: 90 means behind it rather than in front, 15 means almost directly behind it, >180 will always return true
// Ignores z
FUNC BOOL IS_TARGET_BEHIND_ENTITY(ENTITY_INDEX ent, ENTITY_INDEX target, FLOAT fAngleThatCountsAsBehind = 90.0)
	VECTOR entDir = GET_ENTITY_FORWARD_VECTOR(ent)
	entDir.z = 0
	entDir = NORMALISE_VECTOR(entDir)
	VECTOR targetDir = GET_ENTITY_COORDS(target) - GET_ENTITY_COORDS(ent)
	targetDir.z = 0
	targetDir = NORMALISE_VECTOR(targetDir)
	FLOAT direction = ATAN2(entDir.y, entDir.x) - ATAN2(targetDir.y, targetDir.x)
	IF direction < 0.0
		direction += 360.0
	ENDIF
	IF direction < 180.0
		direction = 180.0 - direction
	ELSE
		direction = direction - 180.0
	ENDIF
	RETURN direction < fAngleThatCountsAsBehind
ENDFUNC
PROC SUB_TASK_FIRE_TAMPA_MORTAR(PED_INDEX tempPed, VEHICLE_INDEX tempVeh, INT iped)
	IF GET_ENTITY_MODEL(tempVeh) = TAMPA3
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEight, ciPed_BSEight_DisableBuzzardRocketUse)
		PED_INDEX target = GET_CLOSEST_VISIBLE_PLAYER(iPed, tempPed, TRUE, PED_MAX_TAMPA_MORTAR_RANGE, TRUE)
		
		// Default settings
		WEAPON_TYPE desiredWeapon = WEAPONTYPE_DLC_VEHICLE_TAMPA_DUALMINIGUN //WEAPONTYPE_DLC_VEHICLE_TAMPA_MORTAR //WEAPONTYPE_DLC_VEHICLE_TAMPA_DUALMINIGUN
		FIRING_PATTERN_HASH firingPattern = FIRING_PATTERN_FULL_AUTO

		// Check target and decide state / weapon / firing pattern
		IF DOES_ENTITY_EXIST(target)
			IF IS_TARGET_BEHIND_ENTITY(tempPed, target, 22.5)
				PRINTLN("TAMPA_COMBAT - Target found behind us, fire using MORTAR weapon")
				desiredWeapon = WEAPONTYPE_DLC_VEHICLE_TAMPA_MORTAR
				firingPattern = FIRING_PATTERN_TAMPA_MORTAR
			ELSE
				PRINTLN("TAMPA_COMBAT - Target found but not behind us, fire using MINIGUN weapon default settings")
			ENDIF
		ENDIF
		
		// Make sure we have control of the weapon
		IF NOT IS_MOUNTED_WEAPON_TASK_UNDERNEATH_DRIVING_TASK(tempPed)
			PRINTLN("TAMPA_COMBAT - We don't have control of mounted weapon, so taking control")
			CONTROL_MOUNTED_WEAPON(tempPed)
		ENDIF
		
		// There seems to be a frame delay actually creating the task, so check here too
		IF IS_MOUNTED_WEAPON_TASK_UNDERNEATH_DRIVING_TASK(tempPed)		
		
			// See if we need to switch weapon
			WEAPON_TYPE weaponType = WEAPONTYPE_INVALID
			GET_CURRENT_PED_VEHICLE_WEAPON(tempPed, weaponType)
			IF weaponType != desiredWeapon
				PRINTLN("TAMPA_COMBAT - We don't have the correct weapon, attempting to switch")
				SET_PED_FIRING_PATTERN(tempPed, firingPattern)
				IF NOT SET_CURRENT_PED_VEHICLE_WEAPON(tempPed, desiredWeapon)
					PRINTLN("TAMPA_COMBAT - Failed to change weapon, ped ", iPed)
					SCRIPT_ASSERT("TAMPA_COMBAT - Failed to change weapon")
				ELSE
					PRINTLN("TAMPA_COMBAT - Successfully changed weapon")
				ENDIF
				PRINTLN("TAMPA_COMBAT - Restarting control mounted weapon task after a weapon switch")
				CONTROL_MOUNTED_WEAPON(tempPed)
			ENDIF

			// Fire if we have target, otherwise set weapon as idle
			IF target = NULL
				// Can't pass in null as target entity, so passing in itself...
				SET_MOUNTED_WEAPON_TARGET(tempPed, tempPed, NULL, <<0,0,0>>, TASK_IDLE) 
			ELSE
				SET_MOUNTED_WEAPON_TARGET(tempPed, target, NULL, <<0,0,0>>, TASK_FIRE)
			ENDIF
				
		ENDIF
	ENDIF
ENDPROC

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: PROCESS PED BODY !
//
//************************************************************************************************************************************************************



FUNC BOOL SHOULD_PED_HAVE_OVERHEAD_FOR_THIS_OBJECTIVE(INT iped, INT iTeam)
	
	BOOL bOverhead = TRUE
	
	IF MC_serverBD_4.iPedRule[iped][iteam] = FMMC_OBJECTIVE_LOGIC_KILL
		bOverhead = FALSE
	ELSE
		IF (MC_serverBD_4.iPedRule[iped][iteam] = FMMC_OBJECTIVE_LOGIC_PHOTO)
		AND IS_BIT_SET(MC_serverBD.iDeadPedPhotoBitset[iteam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
				bOverhead = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bOverhead
	
ENDFUNC

FUNC BOOL IS_PED_ON_GOTO_ENTITY_TASK_OUT_OF_RANGE(INT iped, PED_INDEX tempPed, ENTITY_INDEX tempTarget, FLOAT fTargetDist, FLOAT fGotoRange, FLOAT fSpeed, FLOAT fTargetSpeed, BOOL bHeli, BOOL bPlane, BOOL bDriver)
	
	IF (fTargetDist > FMAX( fGotoRange, 15.0 ) // If we're too far away
	OR ((fSpeed > 5.0 OR fTargetSpeed > 5.0) AND NOT bHeli AND NOT bPlane)) // If the target is moving quickly, we can't keep up
		
		// url:bugstar:3610584 - All peds doing Go To Entity tasks on this mission are passengers, and their drivers are not on the Go To Entity task (as we normally assume in the Go To Entity task for passengers).
		// We need to make sure they get out and start entering their target vehicle, as they should be close enough at this point anyway.
		// We could add a new ped bit here if we need this to be handled in future missions to make sure this is handled on a per-ped basis, rather than like this.
	
		IF NOT (IS_THIS_ROCKSTAR_MISSION_SVM_DUNE4(g_FMMC_STRUCT.iRootContentIDHash) AND (iPed = 3 OR iPed = 4 OR iPed = 5))
			
			IF (NOT IS_THIS_ROCKSTAR_MISSION_WVM_TRAILERSMALL(g_FMMC_STRUCT.iRootContentIDHash))
			
			// If we're trying to get into a vehicle, we're not driving our own vehicle and we're not in the target yet: then we are 'in range' during this mission - we need to get out of this vehicle and into the target vehicle.
			OR (NOT IS_ENTITY_A_VEHICLE(tempTarget)
				AND NOT bDriver
				AND NOT IS_PED_IN_VEHICLE(tempPed, NET_TO_VEH(MC_serverBD.niTargetID[iped])))
				
				RETURN TRUE
				
			ENDIF
			
		ENDIF
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_PED_COP_OVERRIDE_CHANGE_ON_RULE(PED_INDEX piPassed, INT iPedNumber)
		
	IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(piPassed)
		EXIT
	ENDIF
	
	BROADCAST_FMMC_COP_BEHAVIOUR_OVERRIDE_EVENT(iPedNumber, TRUE)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iFlee != ciPED_FLEE_ON
	
		SET_PED_CONFIG_FLAG(piPassed, PCF_DontBlip, FALSE)
		SET_PED_CONFIG_FLAG(piPassed, PCF_DontBlipCop, FALSE)
		SET_PED_AS_COP(piPassed,FALSE)
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetTwelve, ciPed_BSTwelve_NeverLoseTarget)
			SET_PED_TARGET_LOSS_RESPONSE(piPassed,TLR_SEARCH_FOR_TARGET)
		ENDIF
		//SET_PED_CONFIG_FLAG(piPassed,PCF_DontBehaveLikeLaw,TRUE)
		SET_PED_CONFIG_FLAG(piPassed,PCF_CanAttackNonWantedPlayerAsLaw,TRUE)
		SET_PED_CONFIG_FLAG(piPassed,PCF_OnlyUpdateTargetWantedIfSeen,TRUE)
		SET_PED_CONFIG_FLAG(piPassed,PCF_AllowContinuousThreatResponseWantedLevelUpdates,TRUE)
		PRINTLN("[RCC MISSION] PROCESS_PED_COP_OVERRIDE_CHANGE_ON_RULE - setting PCF_CanAttackNonWantedPlayerAsLaw on, ped: ",iPedNumber)
	ENDIF
	
	PRINTLN("[RCC MISSION] PROCESS_PED_COP_OVERRIDE_CHANGE_ON_RULE - setting up as cop, ped: ",iPedNumber)
	
ENDPROC

PROC PROCESS_PED_RELATIONSHIP_CHANGE_ON_RULE(PED_INDEX piPassed, INT iPed)
	INT iTeam = MC_playerBD[iPartToUse].iTeam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule < FMMC_MAX_RULES
		IF iRule = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iNewRelationshipGroupOnRule 
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iNewRelationshipGroupType	> -1
				INT iCop
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetFourteen, ciPED_BSFourteen_ForceCopBehaviourOnNewRelationshipGroup)
					PROCESS_PED_COP_OVERRIDE_CHANGE_ON_RULE(piPassed, iPed)
				ENDIF
				
				IF IS_PED_A_SCRIPTED_COP(iPed)
				OR IS_PED_A_COP_MC(iPed)
					icop = FM_RelationshipLikeCops
				ELSE
					icop = FM_RelationshipNothingCops
				ENDIF
				
				INT iteamrel[FMMC_MAX_TEAMS]
				INT irepeat
				FOR irepeat = 0 TO (FMMC_MAX_TEAMS-1)
					
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iNewRelationshipGroupType = ciPED_RELATION_SHIP_LIKE
						iteamrel[irepeat] = FM_RelationshipLike 
						SET_PED_CAN_BE_TARGETTED_BY_TEAM(piPassed, irepeat, FALSE)
						SET_ENTITY_IS_TARGET_PRIORITY(piPassed, FALSE)
						PRINTLN("[RCC MISSION] PROCESS_PED_RELATIONSHIP_CHANGE_ON_RULE - ped likes team: ",irepeat," ped: ",iPed) 
					ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iNewRelationshipGroupType = ciPED_RELATION_SHIP_DISLIKE
						iteamrel[irepeat] = FM_RelationshipDislike
						
						//Only auto-target if they're going to respond with hostility
						IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iFlee != ciPED_FLEE_ON
							IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCombatStyle = ciPED_AGRESSIVE
							OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCombatStyle = ciPED_BERSERK
							OR ( g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCombatStyle = ciPED_DEFENSIVE AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].gun != WEAPONTYPE_UNARMED AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].gun != WEAPONTYPE_INVALID )
								SET_PED_CAN_BE_TARGETTED_BY_TEAM(piPassed, irepeat, TRUE)
							ELSE
								SET_PED_CAN_BE_TARGETTED_BY_TEAM(piPassed, irepeat, FALSE)
							ENDIF
						ELSE
							IF ( g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].gun != WEAPONTYPE_UNARMED AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].gun != WEAPONTYPE_INVALID )
								SET_PED_CAN_BE_TARGETTED_BY_TEAM(piPassed, irepeat, TRUE)
							ELSE
								SET_PED_CAN_BE_TARGETTED_BY_TEAM(piPassed, irepeat, FALSE)
							ENDIF
						ENDIF
						
						SET_ENTITY_IS_TARGET_PRIORITY(piPassed, FALSE)
						PRINTLN("[RCC MISSION] PROCESS_PED_RELATIONSHIP_CHANGE_ON_RULE - ped dislikes team: ",irepeat," ped: ",iPed) 
					ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iNewRelationshipGroupType = ciPED_RELATION_SHIP_HATE
						iteamrel[irepeat] = FM_RelationshipHate
						SET_PED_CAN_BE_TARGETTED_BY_TEAM(piPassed, irepeat, TRUE)
						SET_ENTITY_IS_TARGET_PRIORITY(piPassed, TRUE)
						PRINTLN("[RCC MISSION] PROCESS_PED_RELATIONSHIP_CHANGE_ON_RULE - ped hates team: ",irepeat," ped: ",iPed) 
					ENDIF
					
				ENDFOR
				
				IF NETWORK_HAS_CONTROL_OF_ENTITY(piPassed)
					APPLY_PED_COMBAT_STYLE(piPassed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCombatStyle, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].gun, iPed)
				ENDIF
				
				SET_PED_RELATIONSHIP_GROUP_HASH(piPassed,rgFM_AiPed[iteamrel[0]][iteamrel[1]][iteamrel[2]][iteamrel[3]][icop])	//Use FM_RelationshipNothingCops or FM_RelationshipLikeCops				
			ENDIF
		ENDIF
	ENDIF
ENDPROC



PROC PROCESS_PED_MOVEMENT_TYPE_OVERRIDE(PED_INDEX tempPed, INT iPed)
	
	INT iTeam = MC_PlayerBD[iLocalPart].iteam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule < FMMC_MAX_RULES
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iMovementOverrideAnim1Rule > -1
		AND iRule >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iMovementOverrideAnim1Rule
		AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iMovementOverrideAnim1Rule < g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iMovementOverrideAnim2Rule OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iMovementOverrideAnim2Rule = -1)
			STRING sClipSet = GET_MOVEMENT_OVERRIDE_CLIP_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iMovementOverrideAnim1Type)
			IF NOT IS_STRING_NULL_OR_EMPTY(sClipSet)
			AND NOT IS_BIT_SET(iPedMovementClipsetOverrideBitset1[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				REQUEST_CLIP_SET(sClipSet)
				IF HAS_CLIP_SET_LOADED(sClipSet)
					PRINTLN("[LM][PROCESS_PED_MOVEMENT_TYPE_OVERRIDE] - Ped: ", iPed, ", Overriding movement clipset to: ", sClipSet, " Condition 1, Current Rule: ", iRule, " Rule Set in Creator: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iMovementOverrideAnim1Rule)
					SET_PED_USING_ACTION_MODE(tempPed, FALSE, -1, "DEFAULT_ACTION")	
					SET_PED_MOVEMENT_CLIPSET(tempPed, sClipset)
					SET_BIT(iPedMovementClipsetOverrideBitset1[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(iPedMovementClipsetOverrideBitset1[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				PRINTLN("[LM][PROCESS_PED_MOVEMENT_TYPE_OVERRIDE] - Ped: ", iPed, ", Resetting the movement Clipset")
				RESET_PED_MOVEMENT_CLIPSET(tempPed)
				CLEAR_BIT(iPedMovementClipsetOverrideBitset1[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			ENDIF
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iMovementOverrideAnim2Rule > -1
		AND iRule >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iMovementOverrideAnim2Rule
			STRING sClipSet = GET_MOVEMENT_OVERRIDE_CLIP_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iMovementOverrideAnim2Type)
			IF NOT IS_STRING_NULL_OR_EMPTY(sClipSet)
			AND NOT IS_BIT_SET(iPedMovementClipsetOverrideBitset2[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				REQUEST_CLIP_SET(sClipSet)
				IF HAS_CLIP_SET_LOADED(sClipSet)
					PRINTLN("[LM][PROCESS_PED_MOVEMENT_TYPE_OVERRIDE] - Ped: ", iPed, ", Overriding movement clipset to: ", sClipSet, " Condition 2, Current Rule: ", iRule, " Rule Set in Creator: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iMovementOverrideAnim2Rule)
					SET_PED_USING_ACTION_MODE(tempPed, FALSE, -1, "DEFAULT_ACTION")	
					SET_PED_MOVEMENT_CLIPSET(tempPed, sClipset)
					SET_BIT(iPedMovementClipsetOverrideBitset2[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(iPedMovementClipsetOverrideBitset2[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				PRINTLN("[LM][PROCESS_PED_MOVEMENT_TYPE_OVERRIDE] - Ped: ", iPed, ", Resetting the movement Clipset")
				RESET_PED_MOVEMENT_CLIPSET(tempPed)
				CLEAR_BIT(iPedMovementClipsetOverrideBitset2[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

PROC PROCESS_CACHING_NEARBY_PROJECTILE(PED_INDEX piPed, WEAPON_TYPE wtProjType = WEAPONTYPE_RPG)
	
	ENTITY_INDEX eiNearbyRocket
	VECTOR tempPos
	
	FLOAT fDistanceToCheck = 250.0	
	IF GET_PROJECTILE_OF_PROJECTILE_TYPE_WITHIN_DISTANCE(piPed, wtProjType, fDistanceToCheck, tempPos, eiNearbyRocket)
		
//		IF eiPreviousNearbyRocketAddedToList = eiNearbyRocket
//			PRINTLN("[EXPPROJ] PROCESS_CACHING_NEARBY_PROJECTILE - Already added this missile!")
//			DRAW_DEBUG_LINE(GET_ENTITY_COORDS(piPed), GET_ENTITY_COORDS(eiPreviousNearbyRocketAddedToList), 255, 0, 0)
//			EXIT
//		ENDIF
	
		eiNearbyRocketsList[iExplodeNearbyRocketsListIndex] = eiNearbyRocket
		//eiPreviousNearbyRocketAddedToList = eiNearbyRocket
		PRINTLN("[EXPPROJ] PROCESS_CACHING_NEARBY_PROJECTILE - Found missile! Adding to slot ", iExplodeNearbyRocketsListIndex)
		iExplodeNearbyRocketsListIndex++
		
		#IF IS_DEBUG_BUILD
		IF bExpProjDebug
			DRAW_DEBUG_LINE(GET_ENTITY_COORDS(piPed), GET_ENTITY_COORDS(eiNearbyRocket), 0, 255, 0)
		ENDIF
		#ENDIF
		
		IF iExplodeNearbyRocketsListIndex >= ciNearbyRocketsListSize
			iExplodeNearbyRocketsListIndex = 0
		ENDIF
	ENDIF
		
ENDPROC

PROC PROCESS_EXPLODING_CACHED_PROJECTILES(FLOAT fRange)
	
	ENTITY_INDEX eiRocket = eiNearbyRocketsList[iExplodeNearbyRocketsIterator]
	
	IF DOES_ENTITY_EXIST(eiRocket)
	
		IF VDIST2(GET_ENTITY_COORDS(eiRocket), GET_ENTITY_COORDS(LocalPlayerPed)) < (fRange * fRange)
			
			PRINTLN("[EXPPROJ] PROCESS_EXPLODING_CACHED_PROJECTILES - There is a projectile within ", fRange)
			
			#IF IS_DEBUG_BUILD
			IF bExpProjDebug
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
				DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(eiRocket), 1.5, 0, 255, 0, 100)
			ENDIF
			#ENDIF
			
			SET_ENTITY_AS_MISSION_ENTITY(eiRocket, FALSE, TRUE)
			SET_ENTITY_INVINCIBLE(eiRocket, FALSE)
			
			IF NETWORK_HAS_CONTROL_OF_ENTITY(eiRocket)
				ADD_EXPLOSION(GET_ENTITY_COORDS(eiRocket), EXP_TAG_ROCKET, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
				DELETE_ENTITY(eiRocket)
				PRINTLN("[EXPPROJ] PROCESS_EXPLODING_CACHED_PROJECTILES - exploding rocket now")
			ELSE
				NETWORK_REQUEST_CONTROL_OF_ENTITY(eiRocket)
				PRINTLN("[EXPPROJ] PROCESS_EXPLODING_CACHED_PROJECTILES - requesting control")
			ENDIF
		ELSE
			PRINTLN("[EXPPROJ] PROCESS_EXPLODING_CACHED_PROJECTILES - There is a projectile real far away")
			
			#IF IS_DEBUG_BUILD
			IF bExpProjDebug
				DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(eiRocket), 1.5, 255, 0, 0, 100)
				DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(LocalPlayerPed), fRange, 50, 0, 0, 40)
			ENDIF
			#ENDIF
		ENDIF
	ENDIF
	
	iExplodeNearbyRocketsIterator++
	IF iExplodeNearbyRocketsIterator >= ciNearbyRocketsListSize
		iExplodeNearbyRocketsIterator = 0
	ENDIF
ENDPROC

PROC PROCESS_EXPLODING_NEARBY_ROCKETS(PED_INDEX piFiringPed)
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule >= FMMC_MAX_RULES
	OR iRule < 0
		EXIT
	ENDIF
	
	FLOAT fRange = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fExplodeNearbyRocketsRange[iRule]
	
	IF NETWORK_HAS_CONTROL_OF_ENTITY(piFiringPed)
		IF fRange < 0.25
			IF GET_PED_CONFIG_FLAG(piFiringPed, PCF_FiresDummyRockets)
				SET_PED_CONFIG_FLAG(piFiringPed, PCF_FiresDummyRockets, FALSE)
				PRINTLN("PROCESS_EXPLODING_NEARBY_ROCKETS - Clearing PCF_FiresDummyRockets for ped")
			ENDIF
		ELSE
			IF NOT GET_PED_CONFIG_FLAG(piFiringPed, PCF_FiresDummyRockets)
				SET_PED_CONFIG_FLAG(piFiringPed, PCF_FiresDummyRockets, TRUE)
				PRINTLN("PROCESS_EXPLODING_NEARBY_ROCKETS - Setting PCF_FiresDummyRockets for ped")
			ENDIF
		ENDIF
	ENDIF
	
	PROCESS_EXPLODING_CACHED_PROJECTILES(fRange)
ENDPROC

PROC PROCESS_CANCELLING_RAPPEL_TASK(INT iPed, PED_INDEX tempPed)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_RappelAtGoToDest)
	OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle = -1
	OR IS_PED_INJURED(tempPed)
	OR NOT NETWORK_HAS_CONTROL_OF_ENTITY(tempPed)
		EXIT
	ENDIF
	
	IF (GET_SCRIPT_TASK_STATUS(tempPed, SCRIPT_TASK_RAPPEL_FROM_HELI) = PERFORMING_TASK
	OR GET_SCRIPT_TASK_STATUS(tempPed, SCRIPT_TASK_RAPPEL_FROM_HELI) = WAITING_TO_START_TASK)
		SET_BIT(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_PerformingGrappelFromHeli)
	ENDIF
	
	IF IS_BIT_SET(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_PerformingGrappelFromHeli)
	AND IS_ENTITY_IN_AIR(tempPed)
		PRINTLN("[RCC MISSION][PROCESS_CANCELLING_RAPPEL_TASK] - iPed: ", iPed, " We are performing a Rappel.")
		
		NETWORK_INDEX niVeh = MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle]
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niVeh)
			VEHICLE_INDEX viVeh = NET_TO_VEH(niVeh)
		
			IF NOT DOES_ENTITY_EXIST(viVeh)
			OR NOT IS_VEHICLE_DRIVEABLE(viVeh)
				PRINTLN("[RCC MISSION][PROCESS_CANCELLING_RAPPEL_TASK] - iPed: ", iPed, " We were performing a rappel when the vehicle we were doing it from was destroyed. Clearing task.")
				CLEAR_PED_TASKS(tempPed)				
				SET_PED_TO_RAGDOLL(tempPed, 10000, 20000, TASK_RELAX, FALSE, TRUE)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(tempPed, TRUE)
				SET_DISABLE_HIGH_FALL_DEATH(tempPed, FALSE)
				
				//SET_HIGH_FALL_TASK(tempPed, 5000, 10000, HIGHFALL_IN_AIR)
				//SET_ENTITY_HEALTH(tempPed, 0)
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION][PROCESS_CANCELLING_RAPPEL_TASK] - iPed: ", iPed, " We were performing a rappel when the vehicle we were doing it from was cleaned up. Clearing task.")
			CLEAR_PED_TASKS(tempPed)
			SET_PED_TO_RAGDOLL(tempPed, 10000, 20000, TASK_RELAX, FALSE, TRUE)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(tempPed, TRUE)
			SET_DISABLE_HIGH_FALL_DEATH(tempPed, FALSE)
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_KILL_PED_IN_UNDRIVEABLE_VEHICLE(INT iPed, PED_INDEX tempPed)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThirteen, ciPed_BSThirteen_KillPedIfInDestroyedVehicle)
		EXIT
	ENDIF
	
	PRINTLN("[LM][RCC MISSION][PROCESS_KILL_PED_IN_UNDRIVEABLE_VEHICLE] - iPed: ", iPed, " We are giving this ped special treatment.")

	IF NOT IS_PED_INJURED(tempPed)
		PRINTLN("[LM][RCC MISSION][PROCESS_KILL_PED_IN_UNDRIVEABLE_VEHICLE] - iPed: ", iPed, " Not Injured.")
		IF NETWORK_HAS_CONTROL_OF_ENTITY(tempPed)
			PRINTLN("[LM][RCC MISSION][PROCESS_KILL_PED_IN_UNDRIVEABLE_VEHICLE] - iPed: ", iPed, " Has Control.")
			IF IS_PED_IN_ANY_VEHICLE(tempPed)
				PRINTLN("[LM][RCC MISSION][PROCESS_KILL_PED_IN_UNDRIVEABLE_VEHICLE] - iPed: ", iPed, " In a Vehicle.")
				IF NOT IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(tempPed))
					PRINTLN("[LM][RCC MISSION][PROCESS_KILL_PED_IN_UNDRIVEABLE_VEHICLE] - iPed: ", iPed, " Not Driveable - Killing Ped.")
					SET_ENTITY_HEALTH(tempPed, 0)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC STRING GET_DICT_FOR_PED_PHONE_EMP_REACTION(INT iPed, PED_INDEX piPed, INT iRandomInt, BOOL bUseFlashlight)

	UNUSED_PARAMETER(iPed)
	UNUSED_PARAMETER(piPed)
	
	IF bUseFlashlight
		SWITCH iRandomInt
			CASE 1				RETURN "anim@weapons@flashlight@stealth"
		ENDSWITCH
	ELSE
		SWITCH iRandomInt
			CASE 1				RETURN "AMB@CODE_HUMAN_POLICE_INVESTIGATE@idle_a"
			CASE 2				RETURN "AMB@CODE_HUMAN_POLICE_INVESTIGATE@idle_b"
		ENDSWITCH
	ENDIF
	
	RETURN "INVALID"
ENDFUNC

FUNC STRING GET_ANIM_FOR_PED_PHONE_EMP_REACTION(INT iPed, PED_INDEX piPed, INT iRandomInt, BOOL bUseFlashlight)

	UNUSED_PARAMETER(iPed)
	UNUSED_PARAMETER(piPed)
	
	IF bUseFlashlight
		SWITCH iRandomInt
			CASE 1				RETURN "aim_med_loop"
		ENDSWITCH
	ELSE
		SWITCH iRandomInt
			CASE 1				RETURN "idle_a"
			CASE 2				RETURN "idle_d"
		ENDSWITCH
	ENDIF
	
	RETURN "INVALID"
ENDFUNC

PROC BAIL_PED_PHONE_EMP_REACTION(INT iPed, PED_INDEX piPed, STRING sDict, STRING sAnim)
	UNUSED_PARAMETER(iPed)
	
	IF IS_PED_ACTIVE_IN_SCENARIO(piPed)
		PRINTLN("[PhoneEMP] BAIL_PED_PHONE_EMP_REACTION - iPed: ", iPed, " | Clearing ped tasks")
		CLEAR_PED_TASKS(piPed)
	ELSE
		IF IS_PED_PERFORMING_TASK(piPed, SCRIPT_TASK_PLAY_ANIM)
			IF IS_ENTITY_PLAYING_ANIM(piPed, sDict, sAnim)
				PRINTLN("[PhoneEMP] BAIL_PED_PHONE_EMP_REACTION - iPed: ", iPed, " | Calling STOP_ANIM_TASK")
				STOP_ANIM_TASK(piPed, sDict, sAnim)
			ELSE
				PRINTLN("[PhoneEMP] BAIL_PED_PHONE_EMP_REACTION - iPed: ", iPed, " | Ped isn't playing ", sDict, " / ", sAnim)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PLAY_PED_PHONE_EMP_UPPERBODY_REACTION(INT iPed, PED_INDEX piPed, STRING sDict, STRING sAnim)

	UNUSED_PARAMETER(iPed)

	ANIM_DATA sAimAnim, sEmptyAnim
	sAimAnim.dictionary0 = sDict
	sAimAnim.anim0 = sAnim
	sAimAnim.type = APT_SINGLE_ANIM
	sAimAnim.flags = (AF_LOOPING | AF_SECONDARY | AF_UPPERBODY)
	sAimAnim.filter = GET_HASH_KEY("BONEMASK_UPPERONLY")
	TASK_SCRIPTED_ANIMATION(piPed, sAimAnim, sEmptyAnim, sEmptyAnim)
ENDPROC

PROC PROCESS_PED_PHONE_EMP_VOICE_REACTION(INT iPed, PED_INDEX piPed, BOOL bEMPOn)

	FLOAT fChance = 60.0
	
	IF GET_RANDOM_FLOAT_IN_RANGE(0.0, 100.0) > fChance
		PRINTLN("[PhoneEMP] PROCESS_PED_PHONE_EMP_VOICE_REACTION - Ped ", iPed, " | Not going to react vocally")
		EXIT
	ENDIF

	STRING sVoice, sLine
	
	IF bEMPOn
		IF IS_PED_IN_COMBAT(piPed)
			IF iPed % 3 = 0
				sLine = "HS3F_NRAA"
				sVoice = "HS3_DUGGAN1"
			ELIF iPed % 2 = 0
				sLine = "HS3F_NVAA"
				sVoice = "HS3_DUGGAN2"
			ELSE
				sLine = "HS3F_NWAA"
				sVoice = "HS3_DUGGAN3"
			ENDIF
			
			EXIT
		ELSE
			IF iPed % 3 = 0
				sLine = "HS3F_NOAA"
				sVoice = "HS3_DUGGAN1"
			ELIF iPed % 2 = 0
				sLine = "HS3F_NMAA"
				sVoice = "HS3_DUGGAN2"
			ELSE
				sLine = "HS3F_NNAA"
				sVoice = "HS3_DUGGAN3"
			ENDIF
		ENDIF
	ELSE
		IF IS_PED_IN_COMBAT(piPed)
			IF iPed % 3 = 0
				sLine = "HS3F_NUAA"
				sVoice = "HS3_DUGGAN1"
			ELIF iPed % 2 = 0
				sLine = "HS3F_OBAA"
				sVoice = "HS3_DUGGAN2"
			ELSE
				sLine = "HS3F_OCAA"
				sVoice = "HS3_DUGGAN3"
			ENDIF
			
			EXIT
		ELSE
			IF iPed % 3 = 0
				sLine = "HS3F_NTAA"
				sVoice = "HS3_DUGGAN1"
			ELIF iPed % 2 = 0
				sLine = "HS3F_NXAA"
				sVoice = "HS3_DUGGAN2"
			ELSE
				sLine = "HS3F_NYAA"
				sVoice = "HS3_DUGGAN3"
			ENDIF
		ENDIF
	ENDIF
	
	PRINTLN("[PhoneEMP] PROCESS_PED_PHONE_EMP_VOICE_REACTION - Ped ", iPed, " | sLine: ", sLine, " / sVoice: ", sVoice)
	PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(piPed, sLine, sVoice, AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE), TRUE)
ENDPROC

PROC PROCESS_PED_PHONE_EMP_REACTIONS(INT iPed, PED_INDEX piPed)
	
	IF IS_PED_INJURED(piPed)
		EXIT
	ENDIF
	
	BOOL bUseFlashlight = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetThirteen, ciPED_BSThirteen_ReactToEMP_Flashlight)
	
	IF iPedEMPRandomAnimationIndex[iPed] = ciPhoneEMP_PedAnims_Unassigned
		IF bUseFlashlight
			iPedEMPRandomAnimationIndex[iPed] = GET_RANDOM_INT_IN_RANGE(1, ciPhoneEMP_PedAnims_Flashlight + 1)
		ELSE
			iPedEMPRandomAnimationIndex[iPed] = GET_RANDOM_INT_IN_RANGE(1, ciPhoneEMP_PedAnims_NoFlashlight + 1)
		ENDIF
	ENDIF
	
	STRING sDict, sAnim
	sDict = GET_DICT_FOR_PED_PHONE_EMP_REACTION(iPed, piPed, iPedEMPRandomAnimationIndex[iPed], bUseFlashlight)
	sAnim = GET_ANIM_FOR_PED_PHONE_EMP_REACTION(iPed, piPed, iPedEMPRandomAnimationIndex[iPed], bUseFlashlight)
	
	IF IS_PHONE_EMP_TIMER_RUNNING()
	
		REQUEST_ANIM_DICT(sDict)
		IF NOT HAS_ANIM_DICT_LOADED(sDict)
			PRINTLN("[PhoneEMP] PROCESS_PED_PHONE_EMP_REACTIONS - Ped is still loading ", sDict)
			EXIT
		ENDIF
	
		IF NOT IS_LONG_BIT_SET(iPhoneEMPPedReactedBS, iPed)
			
			IF IS_PED_IN_COMBAT(piPed)
				EXIT
			ENDIF
		
			IF HAS_NET_TIMER_EXPIRED(stPhoneEMP_DeactivationTimer, GET_RANDOM_INT_IN_RANGE(400, 800))
				
				SET_LONG_BIT(iPhoneEMPPedReactedBS, iPed)
				PRINTLN("[PhoneEMP] PROCESS_PED_PHONE_EMP_REACTIONS - Ped ", iPed, " is reacting to phone emp =========================================")
			
				IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(piPed)
					EXIT
				ENDIF
				
				PROCESS_PED_PHONE_EMP_VOICE_REACTION(iPed, piPed, TRUE)
			
				IF NOT IS_PED_WALKING(piPed)
					IF bUseFlashlight
						TASK_START_SCENARIO_IN_PLACE(piPed, "WORLD_HUMAN_SECURITY_SHINE_TORCH", DEFAULT, TRUE)
					ELSE
						TASK_PLAY_ANIM(piPed, sDict, sAnim, DEFAULT, DEFAULT, DEFAULT, AF_LOOPING | AF_HIDE_WEAPON)
					ENDIF
				ELSE
					IF bUseFlashlight
						GIVE_PED_A_FLASHLIGHT_ATTACHMENT(piPed, iPed)
						GIVE_WEAPON_TO_PED(piPed, WEAPONTYPE_DLC_FLASHLIGHT, 100, FALSE, TRUE)
						SET_CURRENT_PED_WEAPON(piPed, WEAPONTYPE_DLC_FLASHLIGHT, TRUE)
					ENDIF
					
					PLAY_PED_PHONE_EMP_UPPERBODY_REACTION(iPed, piPed, sDict, sAnim)
				ENDIF
			ELSE
				PRINTLN("[PhoneEMP] PROCESS_PED_PHONE_EMP_REACTIONS - Ped ", iPed, " is waiting for random reaction timer")
			ENDIF
		ELSE
			
			IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(piPed)
				EXIT
			ENDIF
			
			SET_PED_RESET_FLAG(piPed, PRF_DisableFlashLight, FALSE)
			SET_PED_RESET_FLAG(piPed, PRF_ForceEnableFlashLightForAI, TRUE)
			
			IF IS_PED_IN_COMBAT(piPed)
				BAIL_PED_PHONE_EMP_REACTION(iPed, piPed, sDict, sAnim)
				CLEAR_LONG_BIT(iPhoneEMPPedReactedBS, iPed)
			ENDIF
		ENDIF
	ELSE
		IF IS_LONG_BIT_SET(iPhoneEMPPedReactedBS, iPed)
			
			CLEAR_LONG_BIT(iPhoneEMPPedReactedBS, iPed)
			
			IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(piPed)
				EXIT
			ENDIF
			
			BAIL_PED_PHONE_EMP_REACTION(iPed, piPed, sDict, sAnim)
			PROCESS_PED_PHONE_EMP_VOICE_REACTION(iPed, piPed, FALSE)
		ENDIF
	ENDIF
ENDPROC



/// PURPOSE: handles mission ped navigation/tasks
///
/// PARAMS: Takes in the index of the ped going to be updated 
PROC PROCESS_PED_BODY(INT iped, BOOL bEveryFrame = FALSE)

	// TASKEE
	PED_INDEX TempPed
	VEHICLE_INDEX tempVeh
	BOOL bVehOK, bInVeh, bHeli, bPlane, bBoat, bCombatVehicle
	VECTOR vPedCoords
	FLOAT fSpeed, fdrivespeed, fGotoRange
	
	// DRIVER
	PED_INDEX driverPed
	BOOL bDriver, bDriverOk

	// TARGET
	ENTITY_INDEX tempTarget
	PED_INDEX tempPedTarget
	VEHICLE_INDEX targetVeh
	ENTITY_INDEX temp_carrier
	BOOL bTargetOK,bPedTargetOK
	VECTOR vTempTargetCoords
	FLOAT fTargetSpeed, fTargetDist
	
	INT iobj

	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPED_BSEight_UseDistanceToCapture)
		PROCESS_PED_CAPTURE_RANGE(iped)
	ELSE
		PROCESS_PED_GROUPING(iped)
	ENDIF
	
	//3834707
	PROCESS_SHOULD_PED_BE_SNEAKING(iped)
	
	MAINTAIN_RETASK_DIRTY_FLAG_DATA_COMPARISONS(iped)
	
	CLEAR_BIT( iPerformHydraulicsAsPriorityBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed) )
	
	IF NETWORK_DOES_NETWORK_ID_EXIST( MC_serverBD_1.sFMMC_SBD.niPed[ iped ] )
		
		tempPed = NET_TO_PED( MC_serverBD_1.sFMMC_SBD.niPed[ iped ] )
		
		IF IS_NET_PED_INJURED( MC_serverBD_1.sFMMC_SBD.niPed[ iped ] )
			
			IF IS_BIT_SET(iPedAliveChecked[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				iCurrentPlacedPedsAlive--
				CLEAR_BIT(iPedAliveChecked[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				PRINTLN("[LM][PROCESS_PED_BODY][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - Decrementing iCurrentPlacedPedsAlive to: ", iCurrentPlacedPedsAlive)
			ENDIF
			
			RESET_NET_TIMER(maxAccelerationUpdateTimers[iPed])
			CLEANUP_COLLISION_ON_PED( tempPed, iped )
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedAction = ciACTION_TAKE_COVER
			AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[ iped ])
				SET_PED_TO_LOAD_COVER(tempPed,FALSE)
			ENDIF
			
			IF IS_BIT_SET( iPedSpecBitset[ GET_LONG_BITSET_INDEX( iped ) ], GET_LONG_BITSET_BIT( iPed ) )
				//UNREGISTER_PED_WITH_SPECTATOR_TARGET_LISTS(g_BossSpecData.specHUDData,tempPed) //removed 1623032
				CLEAR_BIT( iPedSpecBitset[ GET_LONG_BITSET_INDEX( iped ) ], GET_LONG_BITSET_BIT( iPed ) )
			ENDIF
			CLEAR_BIT(iEscortingBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			CLEAR_BIT(iGotoObjectBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			// If we don't need to take a picture of this ped once dead, clear up the blip
			IF NOT IS_BIT_SET( MC_serverBD.iDeadPedPhotoBitset[ MC_playerBD[ iPartToUse ].iteam ][ GET_LONG_BITSET_INDEX( iped ) ], GET_LONG_BITSET_BIT( iPed ) )
				IF DOES_BLIP_EXIST(biPedBlip[iped])
					CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][PROCESS_PED_BODY] Removing ped blip for ped ", iped, " as they are dead and don't need to be photographed.")
					REMOVE_BLIP(biPedBlip[iped])
				ENDIF
				IF IS_BIT_SET(iPedTagCreated[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				    REMOVE_PED_OVERHEAD_ICONS(iPedTag[iped], iPed, iPedTagCreated,iPedTagInitalised)
				ENDIF
			ELSE // Otherwise clear up the ped blip and switch the blip to an appropriate one
				IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_BlipOff)
					CREATE_PED_OVERHEAD_ICONS(iPedTag[iped], iPed, iPedTagCreated,iPedTagInitalised, MC_serverBD_1.sFMMC_SBD.niPed[iped], (-1), "", MP_TAG_IF_PED_FOLLOWING, HUD_COLOUR_BLUELIGHT, FALSE)
				ENDIF
				
				IF NOT DOES_BLIP_EXIST(biPedBlip[iped])	
					IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_OBJECTIVE_BLOCKER)
					AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_BlipOff)
						IF MC_serverBD_4.iPedPriority[iped][MC_playerBD[iPartToUse].iteam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
						AND MC_serverBD_4.iPedPriority[iped][MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
							IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_AiBlipsOnly)
								IF NOT SUPPRESS_SCRIPT_PED_BLIP_TILL_COMBAT(iPed)
									CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][PROCESS_PED_BODY] Creating ped blip for ped ", iped," as they're dead and need photos taken.")
									CREATE_PED_BLIP_WITH_COLOUR( Tempped, iPed, BLIP_COLOUR_RED ) 
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			// Leave the function if the ped has been injured
			// @@@@@@
			EXIT // @
			// @@@@@@
		ELSE	// Else this ped is not injured
			IF NOT IS_BIT_SET(iPedAliveChecked[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				iCurrentPlacedPedsAlive++
				SET_BIT(iPedAliveChecked[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				PRINTLN("[LM][PROCESS_PED_BODY][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - Incrementing iCurrentPlacedPedsAlive to: ", iCurrentPlacedPedsAlive)
			ENDIF			
			
			IF NOT IS_BIT_SET(iPedSpecBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				//REGISTER_PED_AS_SPECTATOR_TARGET_GENERIC_ENEMY(g_BossSpecData.specHUDData,tempPed)//removed for 1623032
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iTeam[MC_playerBD[iPartToUse].iteam] = ciPED_RELATION_SHIP_DISLIKE
				OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iTeam[MC_playerBD[iPartToUse].iteam] = ciPED_RELATION_SHIP_HATE
					SET_PED_AS_ENEMY(tempPed,TRUE)
				ENDIF
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEleven, ciPed_BSEleven_DontSpeak)
					PRINTLN("[RCC MISSION] PROCESS_PED_BODY - setting STOP_PED_SPEAKING TRUE")
					
					IF NETWORK_HAS_CONTROL_OF_ENTITY(tempPed)
						STOP_PED_SPEAKING(tempPed, TRUE)
					ENDIF
				ENDIF
				SET_BIT(iPedSpecBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			ENDIF
			
			IF GET_VCM_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash) = eVCM_BRAWL
				IF NOT IS_LONG_BIT_SET(iCasinoBrawl_PedInMixGroupBS, iPed)
					IF IS_VALID_INTERIOR(GET_INTERIOR_FROM_ENTITY(tempPed))
						ADD_ENTITY_TO_AUDIO_MIX_GROUP(tempPed, "dlc_vw_casino_brawl_enemy_group")
						SET_LONG_BIT(iCasinoBrawl_PedInMixGroupBS, iPed)
						PRINTLN("[BRAWLAUDIO] Adding ped ", iPed, " to mixgroup dlc_vw_casino_brawl_enemy_group")
					ENDIF
				ENDIF
			ENDIF
			
			//Enable damage tracking on peds that need it
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_TrackDamage)
				IF NOT IS_BIT_SET(iPedTrackingDamageBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					ACTIVATE_DAMAGE_TRACKER_ON_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[ iped ],TRUE)
					SET_BIT(iPedTrackingDamageBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				ENDIF
			ENDIF
			
			//Make ped run away when unarmed and in combat with an armed ped
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwelve, ciPed_BSTwelve_FleeWhenUnarmed)
				//IF IS_PLAYER_AIMING_AT_PED(tempPed)
					//SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_ALWAYS_FLEE, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_ALWAYS_FIGHT, FALSE)
					SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_CAN_FIGHT_ARMED_PEDS_WHEN_NOT_ARMED, FALSE)
					
					SET_PED_FLEE_ATTRIBUTES(tempPed, FA_DISABLE_COWER, TRUE)
					SET_PED_FLEE_ATTRIBUTES(tempPed, FA_NEVER_FLEE, FALSE)
					SET_PED_FLEE_ATTRIBUTES(tempPed, FA_DISABLE_HANDS_UP, TRUE)
					
				//ENDIF
			ENDIF
			
			// Metal Detector Reactions
			IF HAS_NET_TIMER_STARTED(stPedMetalDetectorInvestigationTimer[iPed])
				IF HAS_NET_TIMER_EXPIRED(stPedMetalDetectorInvestigationTimer[iPed], ciPedMetalDetectorInvestigationTime)
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
						TASK_ACHIEVE_HEADING(tempPed, fPedHeadingBeforeMetalDetector[iPed])
						PRINTLN("[MetalDetector] Returning ped ", iPed, " to original heading")
					ENDIF
					
					RESET_NET_TIMER(stPedMetalDetectorInvestigationTimer[iPed])
					PRINTLN("[MetalDetector] Returning ped ", iPed, " to original heading")
				ENDIF
			ENDIF
	
			//Force ped out of vehicle when near end of goto
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iped])
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwelve, ciPed_BSTwelve_ForceExitVehicleAtEndOfGoto)
				AND IS_PED_IN_ANY_VEHICLE(tempPed)
				AND GET_ASSOCIATED_GOTO_TASK_DATA__NUMBER_OF_GOTOS(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, FALSE) > 0
					VECTOR vGotoEndPoint = GET_ASSOCIATED_GOTO_TASK_DATA__POSITION(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, GET_ASSOCIATED_GOTO_TASK_DATA__NUMBER_OF_GOTOS(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, FALSE) - 1)
					FLOAT fDistanceFromGotoEnd = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(tempPed), vGotoEndPoint)
					
					
					
					IF fDistanceFromGotoEnd <= GET_ASSOCIATED_GOTO_TASK_DATA__CUSTOM_ARRIVAL_RADIUS(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, GET_ASSOCIATED_GOTO_TASK_DATA__NUMBER_OF_GOTOS(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, FALSE) - 1)
						IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_LEAVE_ANY_VEHICLE)
							PRINTLN("PROCESS_PED_BODY - Making ped exit vehicle now due to ciPed_BSTwelve_ForceExitVehicleAtEndOfGoto")
							TASK_LEAVE_ANY_VEHICLE(tempPed, -1, ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)
							EXIT
						ENDIF
					ENDIF
				ENDIF
				
				PROCESS_PED_MOVEMENT_TYPE_OVERRIDE(tempPed, iPed)
			ENDIF

			PROCESS_EXPLODING_NEARBY_ROCKETS(tempPed)
			
			PROCESS_CANCELLING_RAPPEL_TASK(iPed, tempPed)
			
			IF NOT SHOULD_PED_BE_BLOCKED_FROM_USING_VEH_WEAPON(tempPed, iPed)
				IF IS_BIT_SET(iPedVehWeaponsBlockedBS[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
					SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE_ATTACK_IF_VEHICLE_HAS_MOUNTED_GUNS, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_BLOCK_FIRE_FOR_VEHICLE_PASSENGER_MOUNTED_GUNS, FALSE)
					
					PRINTLN("PROCESS_PED_BODY - PED ", iPed, " || Disabling CA_BLOCK_FIRE_FOR_VEHICLE_PASSENGER_MOUNTED_GUNS")
					CLEAR_BIT(iPedVehWeaponsBlockedBS[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(iPedVehWeaponsBlockedBS[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
					SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE_ATTACK_IF_VEHICLE_HAS_MOUNTED_GUNS, FALSE)
					SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_BLOCK_FIRE_FOR_VEHICLE_PASSENGER_MOUNTED_GUNS, TRUE)
					
					PRINTLN("PROCESS_PED_BODY - PED ", iPed, " || Turning on CA_BLOCK_FIRE_FOR_VEHICLE_PASSENGER_MOUNTED_GUNS")
					SET_BIT(iPedVehWeaponsBlockedBS[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
				ENDIF
			ENDIF
			
			// Tutorial blip logic
			IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
				IF NOT IS_BIT_SET(iTutorialMissionCutBitset, biTut_FoundEnemyBlip)
					IF DOES_BLIP_EXIST( biHostilePedBlip[iped].BlipID)
						SET_BIT(iTutorialMissionCutBitset, biTut_FoundEnemyBlip)
						PRINTLN("[dsw] [DO_TUTORIAL_MISSION_HELP_TEXT] [RCC MISSION] PROCESS_PED_BODY - Found enemy blip")
					ENDIF
				ENDIF
			ENDIF
			
			IF MC_serverBD_4.iPedPriority[iped][MC_playerBD[iPartToUse].iteam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
				IF MC_serverBD_4.iPedRule[iped][MC_playerBD[iPartToUse].iteam] != FMMC_OBJECTIVE_LOGIC_NONE
					
					IF NOT bEveryFrame // Only run this stuff in sync with the staggered loop:
						IF MC_serverBD_2.iPartPedFollows[iped] != iPartToUse
						AND NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER)
						AND NOT IS_BIT_SET(MC_serverBD.iPedAtYourHolding[MC_playerBD[iPartToUse].iteam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						
							IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_BlipOff)	//ADDED BACK IN FOR BUG 2139337
								FLOAT fPedDist = GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed,TempPed)
								IF fPedDist < fNearestTargetDistTemp
									iNearestTargetTemp = iPed
									iNearestTargetTypeTemp = ci_TARGET_PED
									fNearestTargetDistTemp = fPedDist
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF MC_serverBD_4.iPedPriority[iped][MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
					AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitsetTen[MC_serverBD_4.iPedPriority[iped][MC_playerBD[iPartToUse].iteam]], ciBS_RULE10_PROGRESS_PROTECT_WHEN_AIMING_AT_PED)
					AND MC_serverBD_4.iPedRule[iped][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_PROTECT
					AND iSpectatorTarget = -1
						IF NOT IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_PLAYER_IS_AIMING_AT_PROTECT_PED)
							IF IS_PLAYER_FREE_AIMING_AT_ENTITY(LocalPlayer, tempPed)
								PRINTLN("[RCC MISSION][PROCESS_PED_BODY] Aiming at protect ped: ",iped,". Setting PBBOOL4_PLAYER_IS_AIMING_AT_PROTECT_PED")
								SET_BIT(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_PLAYER_IS_AIMING_AT_PROTECT_PED)
							ENDIF
						ENDIF
					ENDIF
					
					IF SHOULD_PED_HAVE_OVERHEAD_FOR_THIS_OBJECTIVE(iped, MC_playerBD[iPartToUse].iteam)
						
						#IF IS_DEBUG_BUILD
							IF g_TurnOnOverheadDebugFullOn
								PRINTLN("[overheads][PROCESS_PED_BODY] Ped ", iped, " is not on a kill rule.")
							ENDIF
						#ENDIF
						
						IF iLocalLeaderPart[iped] != MC_serverBD_2.iPartPedFollows[iped]
							iLocalLeaderPart[iped]  = MC_serverBD_2.iPartPedFollows[iped]
							PRINTLN("[JS][UOBJ - PED][PROCESS_PED_BODY] - Requested Objective Update - PROCESS_PED_BODY - iLocalLeaderPart[", iped, "] = ", iLocalLeaderPart[iped])						
							SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_PED_OBJECTIVE)
						ENDIF
						
						// If a ped is part of any group block displaying overheads.
						// We can't collect peds in other team's groups anyway, and peds in our 
						// own group are already close and working with the player.
						IF NOT IS_PED_IN_GROUP(TempPed)
							
							#IF IS_DEBUG_BUILD
								IF g_TurnOnOverheadDebugFullOn
									PRINTLN("[overheads][PROCESS_PED_BODY] Ped ", iped, " is not in any group.")
								ENDIF
							#ENDIF
							
							//Collect and Hold specific checks.
							IF MC_serverBD_4.iPedRule[iped][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD
							
								#IF IS_DEBUG_BUILD
									IF g_TurnOnOverheadDebugFullOn
										PRINTLN("[overheads][PROCESS_PED_BODY] Ped ", iped, " is on a collect and hold rule.")
									ENDIF
								#ENDIF
								
								IF NOT IS_BIT_SET(MC_serverBD.iPedAtYourHolding[MC_playerBD[iPartToUse].iteam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
								AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_BlipOff)
								
									#IF IS_DEBUG_BUILD
										IF g_TurnOnOverheadDebugFullOn
											PRINTLN("[overheads][PROCESS_PED_BODY] Ped ", iped, " is at the player's holding and their blip isn't flagged off.")
										ENDIF
									#ENDIF
									
									CREATE_PED_OVERHEAD_ICONS(iPedTag[iped], iPed, iPedTagCreated,iPedTagInitalised,MC_serverBD_1.sFMMC_SBD.niPed[iped])
								ENDIF
								
							//General case checks.
							ELSE
								#IF IS_DEBUG_BUILD
									IF g_TurnOnOverheadDebugFullOn
										PRINTLN("[overheads][PROCESS_PED_BODY] Ped ", iped, " is not on a collect and hold rule.")
									ENDIF
								#ENDIF
							
								IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_BlipOff)
								
									#IF IS_DEBUG_BUILD
										IF g_TurnOnOverheadDebugFullOn
											PRINTLN("[overheads][PROCESS_PED_BODY] Ped ", iped, " is not flagged to have their blip off.")
										ENDIF
									#ENDIF
									IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPED_BSEight_DisableOverheadIcon)
										CREATE_PED_OVERHEAD_ICONS(iPedTag[iped], iPed, iPedTagCreated,iPedTagInitalised, MC_serverBD_1.sFMMC_SBD.niPed[iped])										
									ENDIF
								ENDIF
							ENDIF
							
						//Ped is part of some group.
						ELSE
							IF IS_BIT_SET(iPedTagCreated[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
								#IF IS_DEBUG_BUILD
									IF g_TurnOnOverheadDebugFullOn
										PRINTLN("[overheads][PROCESS_PED_BODY] Removing overhead for ped ", iped, " as they are now part of a group.")
									ENDIF
								#ENDIF
					       	 	REMOVE_PED_OVERHEAD_ICONS(iPedTag[iped], iPed, iPedTagCreated,iPedTagInitalised)
					   		ENDIF
						ENDIF
						
					//Ped is on a kill objective.
					ELSE 
						IF IS_BIT_SET(iPedTagCreated[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
							#IF IS_DEBUG_BUILD
								IF g_TurnOnOverheadDebugFullOn
									PRINTLN("[overheads][PROCESS_PED_BODY] Removing overhead for ped ", iped, " as they are now on a kill objective.")
								ENDIF
							#ENDIF
				       	 	REMOVE_PED_OVERHEAD_ICONS(iPedTag[iped], iPed, iPedTagCreated,iPedTagInitalised)
				   		ENDIF
					ENDIF
					
				//Ped is not associated with a rule.
				ELSE
					IF IS_BIT_SET(iPedTagCreated[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
						#IF IS_DEBUG_BUILD
							IF g_TurnOnOverheadDebugFullOn
								PRINTLN("[overheads][PROCESS_PED_BODY] Removing overhead for ped ", iped, " as they have no assigned rule.")
							ENDIF
						#ENDIF
			       	 	REMOVE_PED_OVERHEAD_ICONS(iPedTag[iped], iPed, iPedTagCreated,iPedTagInitalised)
			   		ENDIF
				ENDIF
				
			//Ped is not part of the player's current rule.
			ELSE
				IF IS_BIT_SET(iPedTagCreated[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					#IF IS_DEBUG_BUILD
						IF g_TurnOnOverheadDebugFullOn
							PRINTLN("[overheads][PROCESS_PED_BODY] Removing overhead for ped ", iped, " as they are not part of the rule the player is on.")
						ENDIF
					#ENDIF
		       	 	REMOVE_PED_OVERHEAD_ICONS(iPedTag[iped], iPed, iPedTagCreated,iPedTagInitalised)
		   		ENDIF
			ENDIF
			
			PROCESS_PED_RELATIONSHIP_CHANGE_ON_RULE(tempPed, iPed)
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSpookOnRange > -1
			AND NOT IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			AND NOT IS_BIT_SET( iPedHitByStunGun[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
		
				IF bLocalPlayerPedOK
					IF VDIST2(GET_PLAYER_COORDS(LocalPlayer), GET_ENTITY_COORDS(tempPed)) < POW(TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSpookOnRange), 2)
						IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSpookOnRangeTime > -1
							IF NOT HAS_NET_TIMER_STARTED(tdSpookRangeTimer[iPed])
								REINIT_NET_TIMER(tdSpookRangeTimer[iPed])
								PRINTLN("[JS][SPOOKYRANGE][PROCESS_PED_BODY] - Starting timer for Ped ", iPed)
							ELIF HAS_NET_TIMER_EXPIRED(tdSpookRangeTimer[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSpookOnRangeTime * 1000)
								PRINTLN("[JS][SPOOKYRANGE][PROCESS_PED_BODY] - Timer Expired Spooking Ped ", iPed)
								SET_BIT(iPedLocalSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
								BROADCAST_FMMC_PED_SPOOKED(iped, ciSPOOK_PLAYER_CAR_CRASH, iLocalPart)
								RESET_NET_TIMER(tdSpookRangeTimer[iPed])
							ENDIF
						ELSE
							PRINTLN("[JS][SPOOKYRANGE][PROCESS_PED_BODY] - Instaspook Spooking Ped ", iPed)
							SET_BIT(iPedLocalSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
							BROADCAST_FMMC_PED_SPOOKED(iped, ciSPOOK_PLAYER_CAR_CRASH, iLocalPart)
						ENDIF
					ELSE
						IF HAS_NET_TIMER_STARTED(tdSpookRangeTimer[iPed])
							PRINTLN("[JS][SPOOKYRANGE][PROCESS_PED_BODY] - No longer in range of Ped ", iPed)
							RESET_NET_TIMER(tdSpookRangeTimer[iPed])
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_THIS_ROCKSTAR_MISSION_WVM_TRAILERLARGE(g_FMMC_STRUCT.iRootContentIDHash)
				IF NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_SET_PED_MOC_FIRING_PATTERN)
					IF IS_PED_IN_ANY_VEHICLE(tempPed)
						IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(tempPed)) = TRAILERLARGE
							PRINTLN("[KH] PROCESS_PED_BODY - Setting ped firing rate while in MOC: ", iPed)
							SET_BIT(iLocalBoolCheck24, LBOOL24_SET_PED_MOC_FIRING_PATTERN)
							SET_PED_FIRING_PATTERN(tempPed, FIRING_PATTERN_SINGLE_SHOT)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwelve, ciPed_BSTwelve_DisableInVehicleActions)
				IF IS_PED_IN_ANY_VEHICLE(tempPed)
					PRINTLN("[LM] PROCESS_PED_BODY - (PRF_DisableInVehicleActions) Blocking in vehicle actions for iPed: ", iPed)
					SET_PED_RESET_FLAG(tempPed, PRF_DisableInVehicleActions, TRUE)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_TAGGING_TARGETS)
			AND NOT IS_ENTITY_ALIVE(tempPed)
				REMOVE_TAGGED_BLIP(1, iped)
			ENDIF
			
		ENDIF	// End of the IS_PED_INJURED check
	ELSE // ELSE NETWORK_DOES_NETWORK_ID_EXIST == false
		
		IF IS_BIT_SET(iPedAliveChecked[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			iCurrentPlacedPedsAlive--
			CLEAR_BIT(iPedAliveChecked[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			PRINTLN("[LM][PROCESS_PED_BODY][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - Decrementing iCurrentPlacedPedsAlive to: ", iCurrentPlacedPedsAlive)
		ENDIF
			
		RESET_NET_TIMER(maxAccelerationUpdateTimers[iPed])
		CLEANUP_COLLISION_ON_PED(NULL,iped)
		
		CLEAR_BIT(iEscortingBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		CLEAR_BIT(iGotoObjectBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		IF IS_BIT_SET(iPedSpecBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			//UNREGISTER_PED_WITH_SPECTATOR_TARGET_LISTS(g_BossSpecData.specHUDData,tempPed) //removed for 1623032
			CLEAR_BIT(iPedSpecBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		ENDIF
		IF DOES_BLIP_EXIST(biPedBlip[iped])
			CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][PROCESS_PED_BODY] Removing blip for ped ", iped, " as their networkID doesn't exist.")
			REMOVE_BLIP(biPedBlip[iped])
		ENDIF
		IF IS_BIT_SET(iPedTagCreated[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
       	 	REMOVE_PED_OVERHEAD_ICONS(iPedTag[iped], iPed, iPedTagCreated,iPedTagInitalised)
   		ENDIF
		
		#IF IS_DEBUG_BUILD
		CLEAR_BIT(iPedGivenDebugName[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
		#ENDIF
		
		// Exit the function
		// @@@@@@
		EXIT // @
		// @@@@@@
	ENDIF
	
	// Kill peds in undriveable vehicles
	PROCESS_KILL_PED_IN_UNDRIVEABLE_VEHICLE(iPed, tempPed)	
	
	INT iBlipColour = BLIP_COLOUR_RED
	BOOL bCustomBlip
	
	IF NOT DOES_BLIP_EXIST( biPedBlip[ iped ] )
	AND NOT DOES_BLIP_EXIST(GET_BLIP_FROM_ENTITY(TempPed))
		IF SHOULD_PED_HAVE_BLIP(iped,iBlipColour,bCustomBlip)
			IF NOT SUPPRESS_SCRIPT_PED_BLIP_TILL_COMBAT(iPed)
				CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION] Creating blip for ped ", iped," as SHOULD_PED_HAVE_BLIP was positive.")
				CREATE_PED_BLIP_WITH_COLOUR( TempPed, iPed, iBlipColour )
			ENDIF
		ENDIF
	ELSE // Else the blip for this ped exists
		// Make sure this ped's blip only gets removed if he hasn't got the keep-blip flag
		IF NOT SHOULD_PED_HAVE_BLIP(iped,iBlipColour,bCustomBlip)
			CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION][PROCESS_PED_BODY] Removing blip for ped ", iped," as SHOULD_PED_HAVE_BLIP was negative.")
			REMOVE_BLIP(biPedBlip[iped])
		ELSE
			// does ped have the correct sprite?
			BLIP_SPRITE BlipSprite
			BlipSprite = GetCorrectBlipSpriteForMissionPed(TempPed, iped, iBlipColour)
			
			IF (NOT DOES_BLIP_EXIST( biPedBlip[ iped ] ) AND NOT DOES_BLIP_EXIST(GET_BLIP_FROM_ENTITY(TempPed)))
			OR (DOES_BLIP_EXIST( biPedBlip[ iped ]) AND (BlipSprite != GET_BLIP_SPRITE(biPedBlip[ iped ])))
				IF DOES_BLIP_EXIST( biPedBlip[ iped ] )
					CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION][PROCESS_PED_BODY] Removing ped blip for ped ", iped, " as the sprite is wrong.")
					REMOVE_BLIP(biPedBlip[iped])
				ENDIF
				IF DOES_BLIP_EXIST(GET_BLIP_FROM_ENTITY(TempPed))
					CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION][PROCESS_PED_BODY] Removing another ped blip for ped ", iped, " as the sprite is wrong.")
					BLIP_INDEX biTempBlip = GET_BLIP_FROM_ENTITY(TempPed)
					REMOVE_BLIP(biTempBlip)
				ENDIF
				CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION][PROCESS_PED_BODY] Recreating blip for ped ", iped, " with a new spirte.")
				CREATE_PED_BLIP_WITH_COLOUR( TempPed, iPed, iBlipColour )
			ENDIF	
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree, ciPED_BSThree_ShowVisionCone)
				IF NOT SHOULD_DISPLAY_CONE_ON_THIS_PED(iped)
					CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][PROCESS_PED_BODY] disabling vision cone for ped ", iped, ".")
					ENABLE_BLIP_VISION_CONE(biPedBlip[iped], FALSE)
				ENDIF
			ENDIF
			
			// check that ped was actually given blip
			IF DOES_BLIP_EXIST(biPedBlip[ iPed ])
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThirteen, ciPED_BSThirteen_OverrideBlipColourForDisguise)
					IF  DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_PED(iPed)
					AND NOT SHOULD_PED_START_COMBAT(TempPed, iPed)
						SET_BLIP_COLOUR(biPedBlip[ iPed ], BLIP_COLOUR_WHITE)
					ELSE
						SET_BLIP_COLOUR(biPedBlip[ iPed ], BLIP_COLOUR_RED)
					ENDIF
				ENDIF
							
				// should this blip be manually rotate? 
				IF SHOULD_BLIP_BE_MANUALLY_ROTATED(biPedBlip[ iPed ])
					SET_BLIP_ROTATION(biPedBlip[ iPed ], ROUND(GET_ENTITY_HEADING_FROM_EULERS(TempPed)))
				ENDIF	
				
				// If local player is inside this vehicle then hide it.
				IF IS_PED_IN_ANY_VEHICLE(TempPed)
				AND IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(MPGlobals.LocalPlayerID))
				AND (GET_VEHICLE_PED_IS_IN(TempPed) = GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(MPGlobals.LocalPlayerID)))
					CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][PROCESS_PED_BODY] Setting blip alpha to 0 for ped ", iPed, " as the local player is in the same vehicle.")
					SET_BLIP_ALPHA(biPedBlip[iPed], 0)
				ELSE				
					//Is ped in a vehicle that already has a blip?
					IF NOT IS_PED_IN_VEHICLE_THAT_HAS_A_BLIP(TempPed)
						IF GET_BLIP_ALPHA(biPedBlip[iPed]) = 0
							CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][PROCESS_PED_BODY] Setting blip alpha to 255 for ped ", iPed, " as they aren't in a vehicle with a blip.")
							SET_BLIP_ALPHA(biPedBlip[iPed], 255)
						ENDIF
					ELSE
						IF GET_BLIP_ALPHA(biPedBlip[iPed]) = 255
							CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][PROCESS_PED_BODY] Setting blip alpha to 0 for ped ", iPed, " as they're in a vehicle with a blip.")
							SET_BLIP_ALPHA(biPedBlip[iPed], 0)
						ENDIF	
					ENDIF
					
					//Should this blip be faded out if not 'leader' of vehicle (for heli's)
					IF SHOULD_BLIP_ONLY_SHOW_LEADERS_WHEN_IN_VEHICLE(biPedBlip[ iPed ])
					AND NOT IS_PED_IN_VEHICLE_THAT_HAS_A_BLIP(TempPed)
					AND NOT SHOULD_PED_BLIP_DISPLAY_FOR_OBJECTIVE(iPed)
												
						IF GET_BLIP_ALPHA(biPedBlip[iPed]) = 0	
							IF NOT IS_PED_IN_ANY_VEHICLE(TempPed)
							OR IS_PED_LEADER_OF_VEHICLE(TempPed, GET_VEHICLE_PED_IS_IN(TempPed), DEFAULT, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetThirteen, ciPed_BSThirteen_ForceBlipWhenInvalidVehicleLeader))
								CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][PROCESS_PED_BODY] Setting blip alpha to 255 for ped ", iPed, " as they're the leader of a vehicle.")
								SET_BLIP_ALPHA(biPedBlip[iPed], 255)
							ENDIF
						ELSE
							IF IS_PED_IN_ANY_VEHICLE(TempPed)
							AND NOT IS_PED_LEADER_OF_VEHICLE(TempPed, GET_VEHICLE_PED_IS_IN(TempPed), DEFAULT, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetThirteen, ciPed_BSThirteen_ForceBlipWhenInvalidVehicleLeader))
								CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][PROCESS_PED_BODY] Setting blip alpha to 0 for ped ", iPed, " as they're not the leader of a vehicle.")
								SET_BLIP_ALPHA(biPedBlip[iPed], 0)
							ENDIF		
						ENDIF
					ENDIF
					
					// Monitor if an AI ped is in a vehicle where we still want a unique blip for the ped. 
					// We will flash this over the players blip while they are the active objective
					IF NOT IS_PED_IN_VEHICLE_THAT_HAS_A_BLIP(TempPed)
						IF SHOULD_PED_BLIP_DISPLAY_FOR_OBJECTIVE(iPed)
							IF IS_PED_IN_ANY_VEHICLE(TempPed)
														
								// Set our blip to flash if not already
								IF NOT IS_BLIP_FLASHING(biPedBlip[iPed])
									CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][PROCESS_PED_BODY] Setting blip to flash for ped ", iPed, " as they're an objective ped in vehicle.")
									SET_BLIP_FLASHES(biPedBlip[iPed], TRUE)
								ENDIF
								
								// If the alpha is 0, reset to 255
								IF GET_BLIP_ALPHA(biPedBlip[iPed]) = 0	
									SET_BLIP_ALPHA(biPedBlip[iPed], 255)
									CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][PROCESS_PED_BODY] Setting blip alpha to 255 for ped ", iPed, " as they're an objective ped in vehicle.")
								ENDIF
							ELSE
								IF IS_BLIP_FLASHING(biPedBlip[iPed])
									CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][PROCESS_PED_BODY] Setting blip to STOP flashing for ped ", iPed, " as they're an objective ped but not in a vehicle.")
									SET_BLIP_FLASHES(biPedBlip[iPed], FALSE)	
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				
				ENDIF
			
			ENDIF
					
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn = PLAYER_TWO
		IF NOT IS_BIT_SET(MC_serverBD.iPedSecondaryTaskActive[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			IF NOT IS_BIT_SET(iLocalBoolCheck8, LBOOL8_TREVOR_COVER_CLIPSET_LOADED)
				REQUEST_CLIP_SET("trevor_heist_cover_2h")
				SET_BIT(iLocalBoolCheck8, LBOOL8_TREVOR_COVER_CLIPSET_LOADING)
				
				IF HAS_CLIP_SET_LOADED("trevor_heist_cover_2h")
					SET_BIT(iLocalBoolCheck8, LBOOL8_TREVOR_COVER_CLIPSET_LOADED)
				ENDIF
			ELSE
				IF IS_PED_IN_COVER(tempPed)
				AND NOT IS_PED_IN_COVER_FACING_LEFT(tempPed) //the anims are only right facing
					IF NOT IS_BIT_SET(iLocalBoolCheck8, LBOOL8_TREVOR_COVER_CLIPSET_ACTIVE)
						SET_PED_MOTION_IN_COVER_CLIPSET_OVERRIDE(tempPed, "trevor_heist_cover_2h")
						SET_BIT(iLocalBoolCheck8, LBOOL8_TREVOR_COVER_CLIPSET_ACTIVE)
					ENDIF
				ELSE
					IF IS_BIT_SET(iLocalBoolCheck8, LBOOL8_TREVOR_COVER_CLIPSET_ACTIVE)
						CLEAR_PED_MOTION_IN_COVER_CLIPSET_OVERRIDE(tempPed)
						CLEAR_BIT(iLocalBoolCheck8, LBOOL8_TREVOR_COVER_CLIPSET_ACTIVE)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(iLocalBoolCheck8, LBOOL8_TREVOR_COVER_CLIPSET_LOADED)
				REMOVE_CLIP_SET("trevor_heist_cover_2h")
				CLEAR_BIT(iLocalBoolCheck8, LBOOL8_TREVOR_COVER_CLIPSET_LOADED)
				
				IF IS_BIT_SET(iLocalBoolCheck8, LBOOL8_TREVOR_COVER_CLIPSET_ACTIVE)
					CLEAR_PED_MOTION_IN_COVER_CLIPSET_OVERRIDE(tempPed)
					CLEAR_BIT(iLocalBoolCheck8, LBOOL8_TREVOR_COVER_CLIPSET_ACTIVE)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(iLocalBoolCheck12, LBOOL12_TREVOR_COVER_ANIMS_LOADED)
				PRINTLN("[RCC MISSION] PROCESS_PED_BODY - Removing trevor cover idles anim@heists@narcotics@finale@trevor_cover_idles for ped ",iped)
				REMOVE_ANIM_DICT("anim@heists@narcotics@finale@trevor_cover_idles")
				CLEAR_BIT(iLocalBoolCheck12, LBOOL12_TREVOR_COVER_ANIMS_LOADED)
			ENDIF
			
			IF NOT IS_BIT_SET(iLocalBoolCheck11, LBOOL11_TREVOR_VAN_IDLE_CLIPSET_LOADED)
				IF IS_PED_IN_ANY_VEHICLE(tempPed)
					
					PRINTLN("[RCC MISSION] PROCESS_PED_BODY - Ped ",iped," requesting Trevor van idle clipset")
					
					REQUEST_CLIP_SET("anim@heists@narcotics@finale@ig_5_trevor_in_truck")
					
					IF HAS_CLIP_SET_LOADED("anim@heists@narcotics@finale@ig_5_trevor_in_truck")
						PRINTLN("[RCC MISSION] PROCESS_PED_BODY - Ped ",iped," done loading Trevor van idle clipset!")
						SET_BIT(iLocalBoolCheck11, LBOOL11_TREVOR_VAN_IDLE_CLIPSET_LOADED)
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
	
	//2081689
	DO_BIOLAB_GUN_BOOST(iped)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_PhoneEMPAvailable_CasinoPrep)
		PROCESS_PED_PHONE_EMP_REACTIONS(iPed, tempPed)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetThirteen, ciPED_BSThirteen_SetAsCopDecoy)
		BOOL bReleasedCopDecoyPed = FALSE
		PROCESS_COP_DECOY_PED(iPed, tempPed, bReleasedCopDecoyPed)
		IF bReleasedCopDecoyPed //Exit early if we've released the ped.
			EXIT
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF NOT IS_BIT_SET(iPedGivenDebugName[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
		GIVE_FMMC_PED_DEBUG_NAME(tempPed, iped)
		SET_BIT(iPedGivenDebugName[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
	ENDIF
	#ENDIF
	
	// Certain entities will only ever get tasked by the player that has control over this entity
	IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iped])
		CLEAR_RETASK_DIRTY_FLAG(tempPed, iPed)
		RESET_NET_TIMER(maxAccelerationUpdateTimers[iPed])
		EXIT // Exit this function
	ELSE
		// special case to make sure Lester never runs B* 2135484
		IF GET_ENTITY_MODEL(tempPed) = IG_LESTERCREST
			SET_PED_MAX_MOVE_BLEND_RATIO(tempPed, PEDMOVEBLENDRATIO_WALK)
		ENDIF
	ENDIF
	
	//Lukasz: Fix for B* 2172466, added special check for secondary anims played on Trevor while he is in combat in cover
	//we need to undo the settings set when starting to play one of the special anims
	IF IS_BIT_SET(iPedSecondaryAnimCleanupBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
		//check if anim is not playing
		IF 	NOT IS_ENTITY_PLAYING_ANIM(tempPed, "anim@heists@narcotics@finale@trevor_cover_idles", GET_TREVOR_SECONDARY_ANIM(ciPED_IDLE_ANIM__TREVOR_NARCFIN_SHOUT_A), ANIM_SCRIPT)
		AND NOT IS_ENTITY_PLAYING_ANIM(tempPed, "anim@heists@narcotics@finale@trevor_cover_idles", GET_TREVOR_SECONDARY_ANIM(ciPED_IDLE_ANIM__TREVOR_NARCFIN_SHOUT_B), ANIM_SCRIPT)
		AND NOT IS_ENTITY_PLAYING_ANIM(tempPed, "anim@heists@narcotics@finale@trevor_cover_idles", GET_TREVOR_SECONDARY_ANIM(ciPED_IDLE_ANIM__TREVOR_NARCFIN_SHOUT_C), ANIM_SCRIPT)
		AND NOT IS_ENTITY_PLAYING_ANIM(tempPed, "anim@heists@narcotics@finale@trevor_cover_idles", GET_TREVOR_SECONDARY_ANIM(ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_A), ANIM_SCRIPT)
		AND NOT IS_ENTITY_PLAYING_ANIM(tempPed, "anim@heists@narcotics@finale@trevor_cover_idles", GET_TREVOR_SECONDARY_ANIM(ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_B), ANIM_SCRIPT)
		AND NOT IS_ENTITY_PLAYING_ANIM(tempPed, "anim@heists@narcotics@finale@trevor_cover_idles", GET_TREVOR_SECONDARY_ANIM(ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_C), ANIM_SCRIPT)
		AND NOT IS_ENTITY_PLAYING_ANIM(tempPed, "anim@heists@narcotics@finale@trevor_cover_idles", GET_TREVOR_SECONDARY_ANIM(ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_D), ANIM_SCRIPT)
		
			SET_PED_CAN_PEEK_IN_COVER(tempPed, TRUE)
			SET_PED_CONFIG_FLAG(tempPed, PCF_ForcedToStayInCover, FALSE)
			SET_PED_CONFIG_FLAG(tempPed, PCF_BlockPedFromTurningInCover, FALSE)
			
			PRINTLN("[RCC MISSION] PROCESS_PED_BODY Clearing ped config flags PCF_ForcedToStayInCover and PCF_BlockPedFromTurningInCover on ped ", iped)
			
			CLEAR_BIT(iPedSecondaryAnimCleanupBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			BROADCAST_FMMC_SECONDARY_ANIM_CLEANUP_EVENT(iped, FALSE)
			
		ENDIF
	ENDIF
	
	//
	IF NOT IS_ENTITY_VISIBLE(tempPed)
		SET_ENTITY_VISIBLE(tempPed,TRUE)
	ENDIF

	// For speech-AI based things
	IF (NOT IS_PED_IN_GROUP(tempPed)
	AND NOT IS_BIT_SET(iPedSpeechAgroBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed)))
	OR HAS_NET_TIMER_STARTED(PedDelayDialogueTimer[iped])
		HANDLE_PED_SPEECH(tempPed,iped)
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iPedInvolvedInMiniGameBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,TRUE)
		EXIT
	ENDIF
	
	IF IS_PED_IN_ANY_BOAT(tempPed)
		IF NOT IS_PED_INJURED( tempPed )
			SET_PED_DIES_IN_WATER(tempPed, FALSE)
			SET_PED_DIES_INSTANTLY_IN_WATER(tempPed, FALSE)
		ENDIF
	//ELSE //Commented out because there is now the option of setting water death separately on any ped - SET_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitset,ciPed_BS_DoesntDieInWater) - and this conflicted with that
		//SET_PED_DIES_IN_WATER(tempPed, TRUE)
		//SET_PED_DIES_INSTANTLY_IN_WATER(tempPed, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,ciPED_BSThree_OnlyDamagedByPlayers)
		IF IS_PED_IN_ANY_VEHICLE(tempPed)
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(tempPed,FALSE)
		ELSE
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(tempPed,TRUE)
		ENDIF
	ENDIF
	
	// AI navigation subclause to do with this ped's target
	IF MC_serverBD.niTargetID[iped] != NULL
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD.niTargetID[iped])
			tempTarget = NET_TO_ENT(MC_serverBD.niTargetID[iped])
			PRINTLN("[RCC MISSION] PROCESS_PED_BODY - Ped ",iped," Entity target:", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(tempTarget)))
			vPedCoords = GET_ENTITY_COORDS(tempPed)
			IF NOT IS_ENTITY_DEAD(tempTarget)
				bTargetOK = TRUE
				fTargetSpeed = GET_ENTITY_SPEED(tempTarget)
				vTempTargetCoords = GET_ENTITY_COORDS(tempTarget)
				IF IS_ENTITY_A_VEHICLE(tempTarget)
					targetVeh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(tempTarget)
					
					IF IS_VEHICLE_A_TRAILER(targetVeh)
					AND NOT IS_ENTITY_DEAD(GET_ENTITY_ATTACHED_TO(targetVeh))
						targetVeh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_ATTACHED_TO(targetVeh))
					ENDIF

					IF IS_VEHICLE_A_CREATOR_AIRCRAFT(targetVeh)
					AND g_iHostOfam_mp_armory_aircraft != -1
					AND NOT IS_PED_INJURED(MPGlobals.RemoteFakeAvengerPed[g_iHostOfam_mp_armory_aircraft])
						tempPedTarget = MPGlobals.RemoteFakeAvengerPed[g_iHostOfam_mp_armory_aircraft]
					ELSE
						tempPedTarget = GET_PED_IN_VEHICLE_SEAT(targetVeh)
					ENDIF

				ELIF IS_ENTITY_AN_OBJECT(tempTarget)
					IF IS_ENTITY_ATTACHED(tempTarget)
						temp_carrier = GET_ENTITY_ATTACHED_TO(tempTarget)
						IF NOT IS_ENTITY_DEAD(temp_carrier)
							IF IS_ENTITY_A_PED(temp_carrier)
								tempPedTarget = GET_PED_INDEX_FROM_ENTITY_INDEX(temp_carrier)
							ELIF IS_ENTITY_A_VEHICLE(temp_carrier)
								tempPedTarget =GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(temp_carrier))
							ELSE
								temp_carrier = NULL
							ENDIF
							fTargetSpeed = GET_ENTITY_SPEED(temp_carrier)
						ELSE
							temp_carrier = NULL
						ENDIF
					ENDIF
				ELIF IS_ENTITY_A_PED(tempTarget)
					tempPedTarget = GET_PED_INDEX_FROM_ENTITY_INDEX(tempTarget)
					IF IS_PED_IN_ANY_VEHICLE(tempPedTarget)
						targetVeh = GET_VEHICLE_PED_IS_IN(tempPedTarget)
					ENDIF
				ENDIF
				
				
				// DIRTY - target coords have changed
				IF NOT IS_BIT_SET(iGotoObjectBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				AND (IS_VECTOR_ZERO(vPedTargetCoords[iped]) OR GET_DISTANCE_BETWEEN_COORDS(vTempTargetCoords,vPedTargetCoords[iped]) > 10)
					
					IF NOT IS_PED_IN_COMBAT(tempPed)
					AND IS_BIT_SET(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					AND CAN_PED_TASKS_BE_CLEARED(tempPed)
						//CLEAR_PED_TASKS(tempPed)
						SET_PED_RETASK_DIRTY_FLAG(iPed)
						PRINTLN("[RCC MISSION] PROCESS_PED_BODY - Ped ",iped," target moved, clearing tasks") 
					ENDIF
					
					vPedTargetCoords[iped] = vTempTargetCoords
					PRINTLN("[RCC MISSION] PROCESS_PED_BODY - Ped ",iped," target moved, Targets new coords = ",vPedTargetCoords[iped])
				ELSE
					IF IS_ENTITY_A_VEHICLE(tempTarget)
						vPedTargetCoords[iped] = vTempTargetCoords
					ENDIF
				ENDIF
				
				// DIRTY - Target entity has changed
				IF piOldTargetPed[iped] != tempPedTarget
					SET_PED_RETASK_DIRTY_FLAG(iPed)
					PRINTLN("[RCC MISSION] PROCESS_PED_BODY - Ped ",iped," target updated, clearing tasks") 
					piOldTargetPed[iped] = tempPedTarget
				ENDIF

			ENDIF
		ENDIF
	ELIF MC_serverBD.TargetPlayerID[iped] != INVALID_PLAYER_INDEX()
		IF IS_NET_PLAYER_OK(MC_serverBD.TargetPlayerID[iped])
			bTargetOK = TRUE
			tempPedTarget = GET_PLAYER_PED(MC_serverBD.TargetPlayerID[iped])
			PRINTLN("[RCC MISSION] PROCESS_PED_BODY - Ped ",iped," Player target:", GET_PLAYER_NAME(MC_serverBD.TargetPlayerID[iped])) 
			fTargetSpeed = GET_ENTITY_SPEED(tempPedTarget)
			vPedCoords = GET_ENTITY_COORDS(tempPed)
			vTempTargetCoords = GET_ENTITY_COORDS(tempPedTarget)
			
			IF NOT IS_BIT_SET(iGotoObjectBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			AND (IS_VECTOR_ZERO(vPedTargetCoords[iped]) OR GET_DISTANCE_BETWEEN_COORDS(vTempTargetCoords,vPedTargetCoords[iped]) > 50)
				IF IS_PED_INJURED(tempPedTarget)
					IF NOT IS_PED_IN_COMBAT(tempPed)
					AND IS_BIT_SET(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					AND CAN_PED_TASKS_BE_CLEARED(tempPed)
//						CLEAR_PED_TASKS(tempPed)
						SET_PED_RETASK_DIRTY_FLAG(iPed)
						PRINTLN("[RCC MISSION] PROCESS_PED_BODY - Ped ",iped," target moved, clearing tasks") 
					ENDIF
				ENDIF
				vPedTargetCoords[iped] = vTempTargetCoords
				PRINTLN("[RCC MISSION] PROCESS_PED_BODY - Ped ",iped," target moved, Targets new coords = ",vPedTargetCoords[iped])
			ENDIF
			
		ENDIF
	ENDIF
	IF NOT IS_ENTITY_DEAD(tempPedTarget)
		bPedTargetOK = TRUE
	ENDIF

	// Grabbing info about the vehicle the current ped is in
	IF IS_PED_IN_ANY_VEHICLE(tempPed)
		
		bInVeh = TRUE
		tempVeh = GET_VEHICLE_PED_IS_IN(tempPed)
		IF IS_VEHICLE_DRIVEABLE(tempVeh)
			bVehOK = TRUE
			IF IS_PED_IN_ANY_HELI(tempPed)
				bHeli = TRUE
			ELIF IS_PED_IN_ANY_PLANE(tempPed)
				bPlane = TRUE
			ENDIF
			IF IS_PED_IN_ANY_BOAT(tempPed)
				bBoat = TRUE
			ENDIF
			IF IS_COMBAT_VEHICLE(tempVeh)
				bCombatVehicle = TRUE
				SUB_TASK_FIRE_TAMPA_MORTAR(tempPed, tempVeh, iPed)
			ENDIF
			driverPed = GET_PED_IN_VEHICLE_SEAT(tempVeh)
			IF driverPed = tempPed
			
				bDriver = TRUE
				bDriverOk = TRUE
			ELSE
				IF NOT IS_PED_INJURED(driverPed)
					bDriverOk = TRUE
				ENDIF
			ENDIF
			fSpeed = GET_ENTITY_SPEED(tempVeh)
			//NET_PRINT(" enemy fspeed - ") NET_PRINT_FLOAT(fSpeed) NET_PRINT(" ped - ") NET_PRINT_INT(iped) NET_NL()
		ENDIF
	ENDIF
	
	//Set Vehicle Tank Stationary
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEight, ciPed_BSEight_SetVehicleTankStationary)
		IF bInVeh AND bVehOK
			IF GET_ENTITY_MODEL(tempVeh) = RHINO
				SET_VEHICLE_TANK_STATIONARY(tempVeh, TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	//Giving weapons on certain rules - backup for giving ped a gun once someone's been aggroed (normally only gets checked once on a new rule/midpoint)
	IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iGunOnRuleTeam != -1)
	AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iGunOnRuleRule != -1)
	AND ((g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].wtGunOnRule != WEAPONTYPE_UNARMED) AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].wtGunOnRule != WEAPONTYPE_INVALID))
		PROCESS_GIVING_PED_WEAPON_ON_RULE(tempPed,iped)
	ENDIF
	
	// Changing combat style on a new rule
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_Team != -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_Rule != -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_NewStyle != -1
		PROCESS_CHANGING_PED_COMBAT_STYLE_ON_RULE(tempPed, iped)
	ENDIF
	
	
	// This is to do with the ending of goto tasks in various situations
	IF IS_BIT_SET( iPedArrivedBitset[ GET_LONG_BITSET_INDEX( iped ) ], GET_LONG_BITSET_BIT( iPed ) )
		IF bPlane
			fGotoRange = ciPED_PLANE_LEAVE_GOTO_RANGE
		ELIF (bBoat OR bHeli)
			fGotoRange = ciPED_BOAT_HELI_LEAVE_GOTO_RANGE
		ELSE
			fGotoRange  = ciPED_LEAVE_GOTO_RANGE
		ENDIF
		IF NOT IS_BIT_SET( iPedChangeTaskBitset[ GET_LONG_BITSET_INDEX( iped ) ], GET_LONG_BITSET_BIT( iPed ) )
			IF CAN_PED_TASKS_BE_CLEARED(tempPed)
//				CLEAR_PED_TASKS(tempPed)
				SET_PED_RETASK_DIRTY_FLAG(iPed)
			ENDIF
			PRINTLN("[RCC MISSION] PROCESS_PED_BODY - ped arrived clearing tasks: ",iped)
			SET_BIT( iPedChangeTaskBitset[ GET_LONG_BITSET_INDEX( iped ) ], GET_LONG_BITSET_BIT( iPed ) )
		ENDIF
	ELSE
		IF IS_BIT_SET( iPedChangeTaskBitset[ GET_LONG_BITSET_INDEX( iped ) ], GET_LONG_BITSET_BIT( iPed ) )
			IF CAN_PED_TASKS_BE_CLEARED( tempPed )
//				CLEAR_PED_TASKS(tempPed)	
				SET_PED_RETASK_DIRTY_FLAG(iPed)
			ENDIF
			PRINTLN("[RCC MISSION] PROCESS_PED_BODY - ped left clearing tasks: ",iped)
			CLEAR_BIT(iPedChangeTaskBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		ENDIF
		IF bPlane
			fGotoRange = ciPED_PLANE_ARRIVE_GOTO_RANGE
		ELIF (bBoat OR bHeli)
			fGotoRange = ciPED_BOAT_HELI_ARRIVE_GOTO_RANGE
		ELSE
			fGotoRange  = ciPED_ARRIVE_GOTO_RANGE
		ENDIF
	ENDIF
	
	
	IF NOT IS_BIT_SET(iPedPerceptionBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		IF IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			IF NOT IS_PED_AN_AGGRO_PED(iped)
				IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPed_BSEight_DisableSpookedAudio)
					PLAY_PED_SPOOKED_AUDIO(iped)
				ENDIF
				SET_PED_HIGHLY_PERCEPTIVE(tempPed,TRUE)
				PRINTLN("[RCC MISSION] PROCESS_PED_BODY - ped has been set to highly perceptive - ",iped) 
				SET_BIT(iPedPerceptionBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				
				IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedPerceptionSettings != ciPED_PERCEPTION_DEFAULT)
					FLOAT fRange = GET_FMMC_PED_PERCEPTION_RANGE(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedPerceptionSettings, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fPedPerceptionCentreRange)
							
					IF fRange < 60 //Default combat range
						fRange = CLAMP(fRange*1.5,0,60)
					ENDIF
					
					SET_PED_VISUAL_FIELD_PROPERTIES(tempPed,fRange)
				ENDIF
				
			//ELSE
				//Ped is hunting for player - handled in CASE ciTASK_HUNT_FOR_PLAYER below
			ENDIF
		ENDIF
	ELSE
		//Ped has been aggroed, turn up peds' inform respected peds ranges after the 3 second timer hits - peds w aggro delay have this reduced
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,ciPED_BSThree_AggroFailHasDelay)
		AND ( IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_0)
			OR IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_1)
			OR IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_2)
			OR IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_3) )
			
			FLOAT fInformRange = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iInformRange)
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroDoesntFail)
				fInformRange = 30.0
			ENDIF
			
			SET_PED_TO_INFORM_RESPECTED_FRIENDS(tempPed, fInformRange, 50)
			
		ENDIF
	ENDIF

	RUN_PED_BODY_SPOOKED_AND_CANCEL_TASKS_CHECKS(tempPed, iped, tempVeh)
	
	//Disabling One-Hit kill for certain peds
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_OneHitKill) AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetNine, ciPED_BSNine_Remove_OHK_When_Aggro)
		IF IS_BIT_SET(MC_serverBD.iPedAggroedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			IF GET_PED_MAX_HEALTH(tempPed) = 101
				
				BOOL bAggroTriggered = FALSE
				
				INT iTeamLoop
				FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
					// Make sure the 3-second safety window timer has expired if there was one, as this bit only gets set once the safety window timer expires
					// If the safety window is disabled, this bit will get set once the ped aggros anyway
					IF IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_0 + iTeamLoop)
						bAggroTriggered = TRUE
						BREAKLOOP
					ENDIF
				ENDFOR
				
				IF bAggroTriggered
					INT iDesiredHealth = GET_PED_HEALTH_FROM_MENU(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iHealth)
					PRINTLN("[RCC MISSION][TMS] PROCESS_PED_BODY - ciPED_BSNine_Remove_OHK_When_Aggro set for ped ",iped," & aggro has triggered, set new health ",iDesiredHealth)
					SET_ENTITY_MAX_HEALTH(tempPed, iDesiredHealth)
					SET_ENTITY_HEALTH(tempPed, iDesiredHealth)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iPedLocalCancelTasksBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
	AND (NOT IS_BIT_SET(MC_serverBD.iPedCancelTasksBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
		//We don't want to get retasked with something that we shouldn't be doing any more, exit out!
		PRINTLN("[RCC MISSION] PROCESS_PED_BODY - Ped ",iped," should cancel tasks, exit the function to prevent being retasked before the server has updated")
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(iPedSeatPriorityBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		IF MC_serverBD_2.iPartPedFollows[iped] !=-1	
			SET_PED_CONFIG_FLAG(tempPed,PCF_LowerPriorityOfWarpSeats,TRUE)
			SET_BIT(iPedSeatPriorityBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedAction = ciACTION_TAKE_COVER
		IF IS_BIT_SET(sRetaskDirtyFlagData.iDirtyFlagPedNeedsRetask[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
		AND MC_serverBD_2.iPedState[iped] != ciTASK_TAKE_COVER
			//If we're changing state, clear the cover loading
			IF NOT (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn = IG_RASHCOSVKI)
				SET_PED_TO_LOAD_COVER(tempPed,FALSE)
			ELIF (GET_COMBAT_FLOAT(tempPed, CCF_BLIND_FIRE_CHANCE) != 0.1)
				SET_COMBAT_FLOAT(tempPed, CCF_BLIND_FIRE_CHANCE, 0.1) // Default = 0.1
				SET_COMBAT_FLOAT(tempPed, CCF_TIME_BETWEEN_BURSTS_IN_COVER, 1.25) // Default = 1.25
				
				SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_BLIND_FIRE_IN_COVER, FALSE)
				PRINTLN("[RCC MISSION] PROCESS_PED_BODY - Rashkovsky ped ",iped," moved out of cover task, reset blindfire flag CA_BLIND_FIRE_IN_COVER and combat floats CCF_BLIND_FIRE_CHANCE & CCF_TIME_BETWEEN_BURSTS_IN_COVER")
			ENDIF
			
			IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn = PLAYER_TWO OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn = IG_RASHCOSVKI)
			AND GET_COMBAT_FLOAT(tempPed, CCF_STRAFE_WHEN_MOVING_CHANCE) = 0.0
				SET_COMBAT_FLOAT(tempPed, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
				PRINTLN("[RCC MISSION] PROCESS_PED_BODY - Ped ",iped," set CCF_STRAFE_WHEN_MOVING_CHANCE to 1.0 as ped state has changed")
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(MC_serverBD.iPedTaskStopped[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
		AND (GET_PED_COMBAT_MOVEMENT(tempPed) = CM_DEFENSIVE)
			PRINTLN("[RCC MISSION] PROCESS_PED_BODY - Ped ",iped," stopped doing cover task by Stop Tasks, removing defensive area")
			SET_PED_COMBAT_MOVEMENT(tempPed, CM_WILLADVANCE)
			SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_CAN_CHARGE, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_PERMIT_CHARGE_BEYOND_DEFENSIVE_AREA, TRUE)
			REMOVE_PED_DEFENSIVE_AREA(tempPed)
			
			SET_PED_TO_LOAD_COVER(tempPed,FALSE)
			
			IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn = PLAYER_TWO OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn = IG_RASHCOSVKI)
			AND GET_COMBAT_FLOAT(tempPed, CCF_STRAFE_WHEN_MOVING_CHANCE) = 0.0
				SET_COMBAT_FLOAT(tempPed, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
				PRINTLN("[RCC MISSION] PROCESS_PED_BODY - Ped ",iped," set CCF_STRAFE_WHEN_MOVING_CHANCE to 1.0 as ped should stop doing tasks")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iPedDoneDefensiveAreaGoto[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		IF NOT SHOULD_PED_DO_DEFENSIVE_AREA_GOTO(tempPed, iped)
		AND GET_PED_COMBAT_STYLE(iped) != ciPED_DEFENSIVE
			REMOVE_PED_DEFENSIVE_AREA(tempPed)
			SET_PED_COMBAT_MOVEMENT(tempPed, CM_WILLADVANCE)
			SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_CAN_CHARGE, TRUE)
			PRINTLN("[RCC MISSION] PROCESS_PED_BODY - Ped ",iped," defensive area cleared as done with defensive area goto")
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[RCC MISSION] PROCESS_PED_BODY - Ped ",iped," defensive area not cleared as SHOULD_PED_DO_DEFENSIVE_AREA_GOTO = ",SHOULD_PED_DO_DEFENSIVE_AREA_GOTO(tempPed, iped)," / GET_PED_COMBAT_STYLE = ",GET_PED_COMBAT_STYLE(iped))
		#ENDIF
		ENDIF
		
		IF bIsLocalPlayerHost
			CLEAR_BIT(MC_serverBD.iPedDoneDefensiveAreaGoto[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		ELSE
			BROADCAST_FMMC_PED_DEFENSIVE_AREA_GOTO_CLEANED_UP(iped)
		ENDIF
	ENDIF
	
	IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()	
		MAINTAIN_DRONE_RELATIONSHIP_GROUP_WITH_AGGRO(iped)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour, ciPED_BSFour_HeadtrackClosestPlayer)
	AND NOT IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		IF NOT IS_BIT_SET(iLocalBoolCheck11, LBOOL11_BLOCK_LOOK_AT_PLAYER_TASK)
			//PRINTLN("[RCC MISSION] PROCESS_PED_BODY - Ped ", iped," called TASK_PERFORM_LOOK_AT_CLOSEST_PLAYER 2")
			TASK_PERFORM_LOOK_AT_CLOSEST_PLAYER(tempPed, iped)
		ELSE
			PRINTLN("[RCC MISSION] PROCESS_PED_BODY - TASK PERFORM LOOK AT CLOSEST PLAYER BLOCKED 2")
		ENDIF
	ENDIF
	
	//-- headtracking, but ignore the in-perception-area check
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_HeadtrackClosestPlayerNoPerception)
	AND NOT IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		IF NOT IS_BIT_SET(iLocalBoolCheck11, LBOOL11_BLOCK_LOOK_AT_PLAYER_TASK)
			//PRINTLN("[RCC MISSION] PROCESS_PED_BODY - Ped ", iped," called TASK_PERFORM_LOOK_AT_CLOSEST_PLAYER 3")
			TASK_PERFORM_LOOK_AT_CLOSEST_PLAYER(tempPed, iped, TRUE)
		ENDIF
	ENDIF
	
	IF MC_serverBD_2.iPedState[iped] != ciTASK_CHOOSE_NEW_TASK
		IF NETWORK_HAS_CONTROL_OF_ENTITY(tempPed)	
			IF IS_PED_RUNNING_MOBILE_PHONE_TASK(tempPed)
				CLEAR_PED_SECONDARY_TASK(tempPed)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetSix, ciPED_BSSix_DefensiveAreaMovesWithGotos)
		IF IS_BIT_SET(MC_serverBD.iPedTaskActive[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		AND NOT IS_BIT_SET(MC_serverBD.iPedTaskStopped[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		AND NOT IS_PED_IN_GROUP(tempPed)
			IF MC_serverBD_2.iPedState[iped] != ciTASK_GOTO_COORDS
			AND MC_serverBD_2.iPedGotoProgress[iped] > 0
			AND MC_serverBD_2.iPedGotoProgress[iped] < (MAX_ASSOCIATED_GOTO_TASKS + 1)
				
				VECTOR vLastGoto = GET_ASSOCIATED_GOTO_TASK_DATA__POSITION(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed] - 1)
				
				IF NOT IS_PED_DEFENSIVE_AREA_ACTIVE(tempPed, FALSE)
				OR NOT ARE_VECTORS_ALMOST_EQUAL(GET_PED_DEFENSIVE_AREA_POSITION(tempPed), vLastGoto)
					
					FLOAT fRange = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fRange
					
					PRINTLN("[RCC MISSION] PROCESS_PED_BODY - Setting ped ",iped," with new defensive area at last goto (no ",MC_serverBD_2.iPedGotoProgress[iped] - 1,") w pos ",vLastGoto," + range ",fRange)
						
					IF fRange > 0.75
						SET_PED_SPHERE_DEFENSIVE_AREA(tempPed, vLastGoto, fRange)
					ELSE
						SET_PED_SPHERE_DEFENSIVE_AREA(tempPed, vLastGoto, 0.75, TRUE)
						PRINTLN("[RCC MISSION] SET_PED_SPHERE_DEFENSIVE_AREA set to low capping to 0.75 ")
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bDriver AND bPlane
		IF IS_ENTITY_IN_AIR(tempVeh)
		AND NOT ARE_PLANE_CONTROL_PANELS_INTACT(tempVeh)
			PRINTLN("[RCC MISSION] PROCESS_PED_BODY - Ped ",iped," is flying a plane with broken controls, attempting to give ped TASK_PLANE_MISSION w MISSION_CRASH...")
			
			IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_VEHICLE_MISSION, TRUE, MISSION_CRASH, tempVeh)
				
				PRINTLN("[RCC MISSION] PROCESS_PED_BODY - Ped ",iped," isn't performing SCRIPT_TASK_VEHICLE_MISSION w MISSION_CRASH")
				
				IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
					SET_VEHICLE_BROKEN_PARTS_DONT_AFFECT_AI_HANDLING(tempVeh, TRUE) // Give the ped control again, so they can use MISSION_CRASH
					TASK_PLANE_MISSION(tempPed, tempVeh, NULL, NULL, GET_ENTITY_COORDS(tempPed), MISSION_CRASH, fSpeed, 0, -1, 0, 0)
					PRINTLN("[RCC MISSION] PROCESS_PED_BODY - Giving ped ",iped," TASK_PLANE_MISSION w MISSION_CRASH!")
				ELSE
					PRINTLN("[RCC MISSION] PROCESS_PED_BODY - Ped ",iped," should crash plane, requesting control of vehicle ",IS_ENTITY_A_MISSION_CREATOR_ENTITY(tempVeh))
					NETWORK_REQUEST_CONTROL_OF_ENTITY(tempVeh)
				ENDIF
			ELSE
				VECTOR vPed = GET_ENTITY_COORDS(tempPed)
				
				FLOAT fTolerance = GET_RANDOM_FLOAT_IN_RANGE(70, 130) //So they don't always blow up against a magic wall
				
				IF IS_VECTOR_OUT_OF_MAP_BOUNDS(vPed, fTolerance)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
						PRINTLN("[RCC MISSION] PROCESS_PED_BODY - Ped ",iped," is already doing crash task, and is now out of bounds (coords = ",vPed,") - call NETWORK_EXPLODE_VEHICLE on vehicle ",IS_ENTITY_A_MISSION_CREATOR_ENTITY(tempVeh))
						NETWORK_EXPLODE_VEHICLE(tempVeh)
					ELSE
						PRINTLN("[RCC MISSION] PROCESS_PED_BODY - Ped ",iped," plane should explode for being out of bounds, requesting control of vehicle ",IS_ENTITY_A_MISSION_CREATOR_ENTITY(tempVeh))
						NETWORK_REQUEST_CONTROL_OF_ENTITY(tempVeh)
					ENDIF
				ENDIF
			ENDIF
			
			EXIT
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(tempPed)
		IF NETWORK_HAS_CONTROL_OF_ENTITY(tempPed)		
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetNine, ciPed_BSNine_Stay_in_Vehicle_with_Leader)
				IF MC_serverBD_2.iPartPedFollows[iped] > -1
					PARTICIPANT_INDEX piPlayerTarget = INT_TO_PARTICIPANTINDEX(MC_serverBD_2.iPartPedFollows[iped])
					
					IF NETWORK_IS_PARTICIPANT_ACTIVE(piPlayerTarget)
						PED_INDEX pedPlayerTarget = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(piPlayerTarget))
				
						IF NOT IS_PED_INJURED(pedPlayerTarget)
						AND IS_PED_IN_ANY_VEHICLE(pedPlayerTarget)
						AND IS_PED_IN_VEHICLE(tempPed, GET_VEHICLE_PED_IS_IN(pedPlayerTarget))
							SCRIPTTASKSTATUS TaskStatus = GET_SCRIPT_TASK_STATUS(tempPed, SCRIPT_TASK_LEAVE_VEHICLE)
							IF (TaskStatus = WAITING_TO_START_TASK)
							OR (TaskStatus = PERFORMING_TASK)	
								CLEAR_PED_SECONDARY_TASK(tempPed)
							ENDIF
						ENDIF
					ENDIF
				ENDIF		
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetNine, ciPed_BSNineClearAnyLeaveVehicleTasks)
				IF NOT IS_PED_INJURED(tempPed)
					SCRIPTTASKSTATUS TaskStatus = GET_SCRIPT_TASK_STATUS(tempPed, SCRIPT_TASK_LEAVE_VEHICLE)
					IF (TaskStatus = WAITING_TO_START_TASK)
					OR (TaskStatus = PERFORMING_TASK)	
						CLEAR_PED_SECONDARY_TASK(tempPed)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iPedLookAtBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
		TEXT_LABEL_23 tlConvLbl = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
					
		BOOL bBreakOutOfEyeContactEarly
		
		IF ((g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn = IG_LESTERCREST) OR (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn = IG_PAIGE))
		AND ARE_STRINGS_EQUAL(tlConvLbl, "TUSCO_CHAT")
			IF (GET_CURRENT_SCRIPTED_CONVERSATION_LINE() >= 31) // The last lines are directed at players, so Lester and Paige should face the players here (Bug 2253132)
				bBreakOutOfEyeContactEarly = TRUE
			ENDIF
		ENDIF
		
		IF ARE_STRINGS_EQUAL(tlConvLbl,"NULL")
		OR bBreakOutOfEyeContactEarly
			TASK_CLEAR_LOOK_AT(tempPed)
			CLEAR_BIT(iPedLookAtBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			PRINTLN("[MJM] CLEAR_BIT(iPedLookAtBS[GET_LONG_BITSET_INDEX(",iped,",)], GET_LONG_BITSET_BIT(",iped,"))")
		ENDIF
	ENDIF
	
	// Ginormo switch statement for this ped controlling AI tasks
#IF IS_DEBUG_BUILD
	TEXT_LABEL_15 pedStateName = GET_PED_STATE_NAME(MC_serverBD_2.iPedState[iped])
	PRINTLN("[RCC MISSION] PROCESS_PED_BODY - Ped ",iped, "(SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(tempPed), " State: ", pedStateName)
#ENDIF
	SWITCH MC_serverBD_2.iPedState[iped]
		
		CASE ciTASK_CHOOSE_NEW_TASK
			
			// Cleanup State Specific Settings - iPedFixationTaskEnded
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedFixationTarget > -1
				IF IS_BIT_SET(MC_ServerBD_1.iPedFixationTaskEnded[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				AND NOT IS_BIT_SET(iPedFixationStateCleanedUpBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					
					CLEAR_PED_TASKS(tempPed)
					
					IF NOT IS_PED_IN_COMBAT(tempPed)
						APPLY_FIXATION_SETTINGS_PED(tempPed, FALSE, iPed)
						SET_BIT(iPedFixationStateCleanedUpBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedContentEventType_PedClearedFixationSettings, DEFAULT, iPed)
					ELSE
						PRINTLN("[LM][APPLY_FIXATION_SETTINGS_PED] - Waiting for Ped ", iped, " to be out of combat before clearing fixation settings.")
						EXIT
					ENDIF
				ENDIF
			ENDIF
			
			IF CHECK_FOR_DROP_OFF_FLEE(tempPed,iped)
				EXIT // Exit the function
			ENDIF
			
			// 2111947
			IF NOT SHOULD_PED_BE_GIVEN_TASK( tempPed, iped, SCRIPT_TASK_STAY_IN_COVER, TRUE )
				CLEAR_PED_TASKS( tempPed )
			ENDIF
						
			BOOL bAnimNeedsBreakout, bForceBreakout
			
			IF NOT IS_PED_IN_GROUP(tempPed)
				IF ( (NOT SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE))
					OR (NOT SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_SYNCHRONIZED_SCENE, TRUE)) )
				AND ( (NOT IS_ANIMATION_INTERRUPTIBLE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim))
					  OR (NOT IS_ANIMATION_INTERRUPTIBLE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSpecialAnim)) )
				AND NOT IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					//Non-interruptible animation:
					
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,TRUE)
				ELSE
					IF ( (NOT SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE))
						OR (NOT SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_SYNCHRONIZED_SCENE, TRUE)) )
						IF NOT DOES_ANIMATION_NEED_SPECIAL_BREAKOUT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim)
							//Normal animation:
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,FALSE)
						ELSE
							bAnimNeedsBreakout = TRUE
							
							IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEleven, ciPED_BSEleven_DontPlayBreakoutAnimWhenSpooked)
								//Ped has been spooked, play the breakout animation:
								bForceBreakout = TRUE
							ENDIF
						ENDIF
					ELSE
						IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn != IG_MONEY
							//Normal ped (not doing any animation):
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,FALSE)
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEleven, ciPED_BSEleven_CanHaveDefensiveAreaInVehicle)
				AND IS_PED_IN_ANY_VEHICLE(tempPed)
					REMOVE_PED_DEFENSIVE_AREA(tempPed,TRUE)
				ENDIF
			ELSE
				IF NOT IS_PED_ARMED_BY_FMMC(iped)
				AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn != IG_MONEY)
					IF IS_PED_FLEEING(tempPed)
						CLEAR_PED_TASKS(tempPed)
					ENDIF
					
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,TRUE)
				ENDIF
			ENDIF
			
			IF ((NOT SHOULD_PED_START_COMBAT(tempPed, iped)) OR bAnimNeedsBreakout)
			AND NOT WAS_PED_NEARLY_RUN_OVER(tempPed,iped)
				IF NOT IS_PED_IN_GROUP(tempPed)
					IF NOT IS_BIT_SET(iPedSpeechAgroBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
						IF NOT IS_BIT_SET(iPedDialogueRangeActive[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
							IF ( CAN_PED_TASKS_BE_CLEARED(tempPed)
								AND NOT IS_AMBIENT_SPEECH_PLAYING(tempPed)
								AND NOT IS_PED_RAGDOLL(tempPed)
								AND NOT IS_PED_GETTING_UP(tempPed)
								AND NOT IS_PED_FLEEING(tempPed) )
							OR bAnimNeedsBreakout
							OR IS_ANIMATION_A_COWER(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim)
								PROCESS_PED_ANIMATION(tempPed,iped,TRUE,IS_PED_IN_ANY_VEHICLE(tempPed),bForceBreakout)
							ENDIF
						ENDIF
					ELSE
						TASK_PED_GENERAL_COMBAT(tempPed, iped)
					ENDIF
				ELSE
					IF NOT IS_PED_RAGDOLL(tempPed)
					AND NOT IS_PED_GETTING_UP(tempPed)
					AND NOT IS_PED_FLEEING(tempPed)
						//Run any animations that can run while grouped:
						PROCESS_PED_ANIMATION(tempPed,iped,FALSE,IS_PED_IN_ANY_VEHICLE(tempPed,TRUE),bForceBreakout)
					ENDIF
				ENDIF
				CLEANUP_COLLISION_ON_PED(tempPed,iped)
			ELSE
				IF NETWORK_HAS_CONTROL_OF_ENTITY(tempPed)	
					IF IS_PED_RUNNING_MOBILE_PHONE_TASK(tempPed)
						CLEAR_PED_SECONDARY_TASK(tempPed)
					ENDIF
				ENDIF
				
				IF NOT IS_ANIMATION_A_COWER(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim)
				OR NOT (PROCESS_PED_ANIMATION(tempPed, iped, FALSE, IS_PED_IN_ANY_VEHICLE(tempPed,TRUE)))
					LOAD_COLLISION_ON_PED_IF_POSSIBLE(tempPed,iped)
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetSix, ciPED_BSSix_DialogueAttackPersistsForever)
				AND IS_BIT_SET(iPedSpeechAgroBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					TASK_PED_GENERAL_COMBAT(tempPed, iped)
				ENDIF
				
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThirteen, ciPed_BSThirteen_IgnoreCodeTasksInIdle)
				PRINTLN("[LM][PROCESS_PED_BODY] - Setting iPed: ", iPed, " to ignore code tasks while in the idle state via ciPed_BSThirteen_IgnoreCodeTasksInIdle")
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, TRUE)
			ENDIF
			
		BREAK // end of ciTASK_CHOOSE_NEW_TASK
		
		CASE ciTASK_SHUFFLE_VEHICLE_SEAT_TO_TURRET
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, TRUE)	
			
			IF NOT IS_PED_INJURED(tempPed)
				IF IS_PED_IN_ANY_VEHICLE(tempPed)
					VEHICLE_INDEX VehPed 
					VehPed = GET_VEHICLE_PED_IS_IN(tempPed)
					
					SCRIPTTASKSTATUS TaskStatus 
					TaskStatus = GET_SCRIPT_TASK_STATUS(tempPed, SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT)
					
					VEHICLE_SEAT seatTurret 
					seatTurret = INT_TO_ENUM(VEHICLE_SEAT, GetFirstFreeSeatOfVehicle(VehPed, TRUE, FALSE, TRUE))
					
					IF IS_TURRET_SEAT(VehPed, seatTurret)
					AND IS_VEHICLE_SEAT_FREE(VehPed, seatTurret, TRUE)
					AND NOT IS_TURRET_SEAT(VehPed, GET_SEAT_PED_IS_IN(tempPed, TRUE))
					AND GET_PED_IN_VEHICLE_SEAT(VehPed, seatTurret) != tempPed
						IF (TaskStatus != WAITING_TO_START_TASK)
						AND (TaskStatus != PERFORMING_TASK)
							PRINTLN("[LM][ShuffleTurretSeatAlerted] - SETTING VEHICLE TO BE RE-TASKED - SHOULD_PED_SHUFFLE_TO_TURRET_SEAT_FROM_BEING_ALERTED")
							CLEAR_PED_SECONDARY_TASK(tempPed)
							
							#IF IS_DEBUG_BUILD
							TEXT_LABEL_15 pedStateName2 
							pedStateName2 = GET_PED_STATE_NAME(MC_serverBD_2.iPedState[iped])								
							PRINTLN("[LM][RCC MISSION][ShuffleTurretSeatAlerted] PROCESS_PED_BRAIN - iped: ", iPed, " GET_PED_STATE_NAME: ", pedStateName2, " Before")
							#ENDIF				
							
							TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(tempPed, VehPed, TRUE)
							CLEAR_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							
							#IF IS_DEBUG_BUILD
							pedStateName2 = GET_PED_STATE_NAME(MC_serverBD_2.iPedState[iped])								
							PRINTLN("[LM][RCC MISSION][ShuffleTurretSeatAlerted] PROCESS_PED_BRAIN - iped: ", iPed, " GET_PED_STATE_NAME: ", pedStateName2, " After")
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ciTASK_GOTO_COORDS
						
			BOOL bUseDefensiveAreaGoto
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn = IG_RASHCOSVKI
				SET_PED_TO_LOAD_COVER(tempPed,TRUE)
			ENDIF
			#IF IS_DEBUG_BUILD
			IF bLMhelpfulScriptAIPrints
				PRINTLN("[LM][RCC MISSION] PROCESS_PED_BODY - 		iPed: ", iPed, " SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(tempPed), " - Processing ciTASK_GOTO_COORDS")
				PRINTLN("[LM][RCC MISSION] PROCESS_PED_BODY - 		bInVeh: ", bInVeh, " bHeli: ", bHeli, " bPlane: ", bPlane, " MC_serverBD_2.iTargetID: ", MC_serverBD_2.iTargetID[iped])
			ENDIF
			#ENDIF
			
			IF MC_serverBD_2.iTargetID[iped] != -1
				vPedCoords = GET_ENTITY_COORDS(tempPed)
				VECTOR VGotoCoords
				IF MC_serverBD_2.iTargetID[iped] = ciINITIAL_GOTO
					
					#IF IS_DEBUG_BUILD
					IF MC_serverBD_2.iPedGotoProgress[iped] >= MAX_ASSOCIATED_GOTO_TASKS
						PRINTLN("[RCC MISSION] PROCESS_PED_BODY - Ped ",iped," iPedGotoProgress is >= MAX_ASSOCIATED_GOTO_TASKS, associated goto vector will return as zero!")
						CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION] PROCESS_PED_BODY - Ped ",iped," ciINITIAL_GOTO iPedGotoProgress is >= MAX_ASSOCIATED_GOTO_TASKS, associated goto vector will return as zero!")
					ENDIF
					#ENDIF
					
					VGotoCoords = GET_ASSOCIATED_GOTO_TASK_DATA__POSITION(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed])
					
					fTargetDist = GET_DISTANCE_BETWEEN_COORDS(vPedCoords,VGotoCoords)
					
					fGotoRange = 2
					
					IF(bInVeh)
						fGotoRange = 10
						IF bPlane
						AND NOT IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed], PED_ASSOCIATED_GOTO_TASK_TYPE_PLANETAXI)
							fGotoRange = 90
						ENDIF
						IF bHeli
							fGotoRange = 60
						ENDIF
					ENDIF
					
					IF SHOULD_PED_DO_DEFENSIVE_AREA_GOTO(tempPed, iped)
						bUseDefensiveAreaGoto = TRUE
					ENDIF
					
					FLOAT fCustomArrivalRadius
					fCustomArrivalRadius = GET_ASSOCIATED_GOTO_TASK_DATA__CUSTOM_ARRIVAL_RADIUS(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed])
					
					IF MC_serverBD_2.iPedGotoProgress[iped] < MAX_ASSOCIATED_GOTO_TASKS
					AND (fCustomArrivalRadius != 0)
						fGotoRange = fCustomArrivalRadius
					ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData.iArrivalRadius != 0
						fGotoRange = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData.iArrivalRadius)
					ENDIF
					
					// Put the ped into the combat state if the server is about to do so anyway, this is to stop peds standing around for a second or two while waiting for the server to update. 
					BOOL bGoToIsLooped
					IF ((NOT bPlane) OR IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed], PED_ASSOCIATED_GOTO_TASK_TYPE_PLANETAXI))
					AND HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION(tempPed, iped)
						IF SHOULD_PED_MOVE_ON_FROM_GOTO_COORDS_STATE(iped, bGoToIsLooped)
							IF SHOULD_PED_GO_FROM_NOTHING_STATE_TO_COMBAT_STATE(tempPed, iPed)
								PRINTLN("[RCC MISSION] PROCESS_PED_BODY - Ped ",iped," entering combat (a)")
								TASK_PED_GENERAL_COMBAT(tempPed,iped,bheli,bplane,bBoat,bDriver,tempveh)
							ENDIF
						ENDIF
					ENDIF
					
					#IF IS_DEBUG_BUILD
					IF bLMhelpfulScriptAIPrints
						PRINTLN("[LM][RCC MISSION] PROCESS_PED_BODY - iPed: ", iPed, "		Assigning Goto (3)")
					ENDIF
					#ENDIF
					
				ELIF MC_serverBD_2.iTargetID[iped] = ciSECONDARY_GOTO
					
					VGotoCoords = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vSecondaryVector
					
					fTargetDist = GET_DISTANCE_BETWEEN_COORDS(vPedCoords,VGotoCoords)
					
					fGotoRange = 2
					
					IF(bInVeh)
						fGotoRange = 10
						IF bPlane
						AND NOT IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed], PED_ASSOCIATED_GOTO_TASK_TYPE_PLANETAXI)
							fGotoRange = 130
						ENDIF
						IF bHeli
							fGotoRange = 60
						ENDIF
					ENDIF
					
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData.iArrivalRadius != 0
						fGotoRange = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData.iArrivalRadius)
					ENDIF
					
					#IF IS_DEBUG_BUILD
					IF bLMhelpfulScriptAIPrints
						PRINTLN("[LM][RCC MISSION] PROCESS_PED_BODY - iPed: ", iPed, "		Assigning Goto (2)")
					ENDIF
					#ENDIF
				ELSE
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedTeam != -1
						fTargetDist = GET_DISTANCE_BETWEEN_COORDS(vPedCoords,GET_LOCATION_VECTOR(MC_serverBD_2.iTargetID[iped]))
						VGotoCoords = GET_LOCATION_VECTOR(MC_serverBD_2.iTargetID[iped])

						fGotoRange = 2
						IF(bInVeh)
							fGotoRange = 10
							IF bPlane
							AND NOT IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed], PED_ASSOCIATED_GOTO_TASK_TYPE_PLANETAXI)
								fGotoRange = 130
							ENDIF
							IF bHeli
								fGotoRange = 30//60 fix for B*1611789 - reduced the range at which helicopters will begin to land
							ENDIF
						ENDIF
						IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData.iArrivalRadius != 0
							fGotoRange = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData.iArrivalRadius)
						ENDIF
						
						#IF IS_DEBUG_BUILD
						IF bLMhelpfulScriptAIPrints
							PRINTLN("[LM][RCC MISSION] PROCESS_PED_BODY - iPed: ", iPed, "		Assigning Goto (1)")
						ENDIF
						#ENDIF
					ENDIF
				ENDIF
				
				IF fGotoRange < 1.25
					fGotoRange = 1.25 // Copied some contingencies from HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION)
					PRINTLN("[LM][RCC MISSION] PROCESS_PED_BODY - iPed: ", iPed, "		fGotoRange: ", fGotoRange, " is really small, having to set it to 1.25 so that the PED can reach the destination within script (not phyiscally)")
				ENDIF
				
				#IF IS_DEBUG_BUILD
				IF bLMhelpfulScriptAIPrints
					PRINTLN("[LM][RCC MISSION] PROCESS_PED_BODY - iPed: ", iPed, "		fGotoRange: ", fGotoRange, " VGotoCoords; ", VGotoCoords, " fTargetDist: ", fTargetDist)
				ENDIF
				#ENDIF
				
				IF fTargetDist > fGotoRange				
					CLEAR_BIT(iPedArrivedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					
					IF IS_BIT_SET(iPedGotoWaitBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					OR IS_BIT_SET(iPedGotoAssCompletedWaitBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
						CLEAR_BIT(iPedGotoWaitBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
						CLEAR_BIT(iPedGotoAssCompletedWaitBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
						CLEAR_BIT(iBSTaskAchieveHeadingWait[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						BROADCAST_FMMC_PED_CLEAR_GOTO_WAITING_STATE(iped)
					ENDIF
					
					IF NOT bUseDefensiveAreaGoto
						REMOVE_PED_DEFENSIVE_AREA(tempPed, TRUE)
					ENDIF
					
					IF NOT IS_PED_IN_GROUP(tempPed)
						IF bUseDefensiveAreaGoto
							TASK_PED_DEFENSIVE_AREA_GOTO(tempPed, iped, vGotoCoords, fGotoRange)
						ELSE
							IF bDriver
								#IF IS_DEBUG_BUILD
								IF bLMhelpfulScriptAIPrints
									PRINTLN("[LM][RCC MISSION] PROCESS_PED_BODY - 		iPed: ", iPed, " Calling TASK_PED_GENERAL_GOTO (1a)")
								ENDIF
								#ENDIF
								TASK_PED_GENERAL_GOTO(tempPed,iped,VGotoCoords,bVehOK,fTargetDist,fTargetSpeed,bheli,bplane,bBoat,tempPedTarget,bDriver,tempveh,SHOULD_PED_GETOUT(iped))
							ELIF bDriverOK
								IF IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
									PRINTLN("[RCC MISSION] PROCESS_PED_BODY - Ped ",iped," entering combat (b)")
									TASK_PED_GENERAL_COMBAT(tempPed,iped)
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
								IF bLMhelpfulScriptAIPrints
									PRINTLN("[LM][RCC MISSION] PROCESS_PED_BODY - 		iPed: ", iPed, " Calling TASK_PED_GENERAL_GOTO (1b)")
								ENDIF
								#ENDIF
								TASK_PED_GENERAL_GOTO(tempPed,iped,VGotoCoords,bVehOK,fTargetDist,fTargetSpeed,bheli,bplane,bBoat,tempPedTarget,bDriver,tempveh,SHOULD_PED_GETOUT(iped))
							ENDIF
						ENDIF
					ENDIF
					
				ELSE
					
					IF MC_serverBD_2.iTargetID[iped] != ciSECONDARY_GOTO						
						PRINTLN("[RCC MISSION] [PROCESS_PED_BODY] - Ped ", iped, " MC_serverBD_2.iTargetID[iped] != ciSECONDARY_GOTO.")
						
						VEHICLE_INDEX viVeh
						viVeh = NULL
						IF IS_ASSOCIATED_GOTO_ENTER_VEH_VALID(iped, MC_serverBD_2.iPedGotoProgress[iped], viVeh)
						AND NOT IS_PED_IN_VEHICLE(tempPed, viVeh, FALSE)
							IF NOT IS_PED_IN_VEHICLE(tempPed, viVeh, TRUE)
								PRINTLN("[LM][RCC MISSION] PROCESS_PED_BODY - iPed: ", iPed, " TASK_PED_ENTER_VEH.")
								TASK_PED_ENTER_VEH(tempPed, iped, viVeh, 9999999)
							ENDIF
							
						ELIF IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iped], PED_ASSOCIATED_GOTO_TASK_TYPE_EXIT_VEHICLE)
						AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPed_BSEight_NeverLeaveVehicle)
							IF bDriver
								IF bVehOK
									IF bheli OR bplane
										IF NOT HAS_AIRCRAFT_LANDED(tempveh)
											#IF IS_DEBUG_BUILD
											IF bLMhelpfulScriptAIPrints
												PRINTLN("[LM][RCC MISSION] PROCESS_PED_BODY - ciTASK_GOTO_COORDS - iPed: ", iPed, " Calling TASK_PED_LAND_AIRCRAFT (0)")
											ENDIF
											#ENDIF
											TASK_PED_LAND_AIRCRAFT(tempPed, iped, tempveh, VGotoCoords, bPlane)
										ELSE
											#IF IS_DEBUG_BUILD
											IF bLMhelpfulScriptAIPrints
												PRINTLN("[LM][RCC MISSION] PROCESS_PED_BODY - ciTASK_GOTO_COORDS - iPed: ", iPed, " Calling TASK_PED_LEAVE_VEHICLE (0b)")
											ENDIF
											#ENDIF
											TASK_PED_LEAVE_VEHICLE(tempPed,iped)
										ENDIF
									ELSE
										IF NOT HAS_VEHICLE_STOPPED(tempveh)
											#IF IS_DEBUG_BUILD
											IF bLMhelpfulScriptAIPrints
												PRINTLN("[LM][RCC MISSION] PROCESS_PED_BODY - ciTASK_GOTO_COORDS - iPed: ", iPed, " Calling TASK_PED_STOP_VEHICLE (0)")
											ENDIF
											#ENDIF
											TASK_PED_STOP_VEHICLE(tempPed, iped, tempveh, VGotoCoords)
										ELSE
											#IF IS_DEBUG_BUILD
											IF bLMhelpfulScriptAIPrints
												PRINTLN("[LM][RCC MISSION] PROCESS_PED_BODY - ciTASK_GOTO_COORDS - iPed: ", iPed, " Calling TASK_PED_LEAVE_VEHICLE (0)")
											ENDIF
											#ENDIF
											TASK_PED_LEAVE_VEHICLE(tempPed,iped)
										ENDIF
									ENDIF
								ELSE
									SET_BIT(iPedArrivedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
									IF IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
										PRINTLN("[RCC MISSION] PROCESS_PED_BODY - ciTASK_GOTO_COORDS - Ped ",iped," entering combat (c)")
										TASK_PED_GENERAL_COMBAT(tempPed,iped,bheli,bplane,bBoat,bDriver,tempveh)
									ELSE
										PRINTLN("[RCC MISSION] [PROCESS_PED_BODY] - ciTASK_GOTO_COORDS - Ped doing nothing(m)! Ped ", iped)
									ENDIF
								ENDIF
							ELSE
								IF bInVeh
									IF bheli OR bplane
										IF HAS_AIRCRAFT_LANDED(tempveh)
											#IF IS_DEBUG_BUILD
											IF bLMhelpfulScriptAIPrints
												PRINTLN("[LM][RCC MISSION] PROCESS_PED_BODY - ciTASK_GOTO_COORDS - iPed: ", iPed, " Calling TASK_PED_LEAVE_VEHICLE (1)")
											ENDIF
											#ENDIF
											TASK_PED_LEAVE_VEHICLE(tempPed,iped)
										ELSE
											IF IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
												PRINTLN("[RCC MISSION] PROCESS_PED_BODY - ciTASK_GOTO_COORDS - Ped ",iped," entering combat (d)")
												TASK_PED_GENERAL_COMBAT(tempPed,iped,bheli,bplane,bBoat,bDriver,tempveh)
											ELSE
												PRINTLN("[RCC MISSION] [PROCESS_PED_BODY] - ciTASK_GOTO_COORDS - Ped doing nothing(n)! Ped ", iped)
											ENDIF
										ENDIF
									ELSE
										IF HAS_VEHICLE_STOPPED(tempveh)
											#IF IS_DEBUG_BUILD
											IF bLMhelpfulScriptAIPrints
												PRINTLN("[LM][RCC MISSION] PROCESS_PED_BODY - ciTASK_GOTO_COORDS - iPed: ", iPed, " Calling TASK_PED_LEAVE_VEHICLE (2)")
											ENDIF
											#ENDIF
											TASK_PED_LEAVE_VEHICLE(tempPed,iped)
										ELSE
											IF IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
												PRINTLN("[RCC MISSION] PROCESS_PED_BODY - ciTASK_GOTO_COORDS - Ped ",iped," entering combat (e)")
												TASK_PED_GENERAL_COMBAT(tempPed,iped,bheli,bplane,bBoat,bDriver,tempveh)
											ELSE
												PRINTLN("[RCC MISSION] [PROCESS_PED_BODY] - ciTASK_GOTO_COORDS - Ped doing nothing(o)! Ped ", iped)
											ENDIF
										ENDIF
									ENDIF
								ELSE
									SET_BIT(iPedArrivedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
									IF IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
										PRINTLN("[RCC MISSION] PROCESS_PED_BODY - ciTASK_GOTO_COORDS - Ped ",iped," entering combat (f)")
										TASK_PED_GENERAL_COMBAT(tempPed,iped,bheli,bplane,bBoat,bDriver,tempveh)
									ELSE
										PRINTLN("[RCC MISSION] [PROCESS_PED_BODY] - ciTASK_GOTO_COORDS - Ped doing nothing(p)! Ped ", iped)
									ENDIF
								ENDIF
							ENDIF
						ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_HoverAtGoToDest)
						OR MC_serverBD_2.iPedGotoProgress[iped] = GET_ASSOCIATED_GOTO_TASK_DATA__DETACH_CARGOBOB_ON_GOTO_INDEX(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData)
							IF bDriver
								IF bVehOK
									IF bheli
										TASK_PED_HOVER_VEHICLE(tempPed, tempVeh, iped, VGotoCoords)
										
										IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThirteen, ciPED_BSThirteen_AllowRappellingOnGoToArrival)
											SET_AIRCRAFT_PILOT_SKILL_NOISE_SCALAR(tempVeh, 0.0)
										ENDIF
									ELSE
										PRINTLN("[RCC MISSION] [PROCESS_PED_BODY] - ciTASK_GOTO_COORDS - Ped doing nothing(q)! Ped ", iped)
									ENDIF
								ELSE
									SET_BIT(iPedArrivedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
									IF IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
										PRINTLN("[RCC MISSION] PROCESS_PED_BODY - ciTASK_GOTO_COORDS - Ped ",iped," entering combat (g)")
										TASK_PED_GENERAL_COMBAT(tempPed,iped,bheli,bplane,bBoat,bDriver,tempveh)
									ELSE
										PRINTLN("[RCC MISSION] [PROCESS_PED_BODY] - ciTASK_GOTO_COORDS - Ped doing nothing(r)! Ped ", iped)
									ENDIF
								ENDIF
							ENDIF
						ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_RappelAtGoToDest)
							IF bVehOK
								IF bheli
									TASK_PED_RAPPEL_FROM_VEHICLE(tempPed,iped)
								ELSE
									PRINTLN("[RCC MISSION] [PROCESS_PED_BODY] - ciTASK_GOTO_COORDS - Ped doing nothing(s)! Ped ", iped)
								ENDIF
							ELSE
								SET_BIT(iPedArrivedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
								IF IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
									PRINTLN("[RCC MISSION] PROCESS_PED_BODY - ciTASK_GOTO_COORDS - Ped ",iped," entering combat (h1)")
									TASK_PED_GENERAL_COMBAT(tempPed,iped,bheli,bplane,bBoat,bDriver,tempveh)
								ELSE
									PRINTLN("[RCC MISSION] [PROCESS_PED_BODY] - ciTASK_GOTO_COORDS - Ped doing nothing(t)! Ped ", iped)
								ENDIF
							ENDIF
						ELIF IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iped], PED_ASSOCIATED_GOTO_TASK_TYPE_LANDVEH) 
							IF bVehOK
								IF bheli
								AND bDriver
									TASK_PED_LAND_HELI(tempPed, tempVeh, iped, vGoToCoords)
								ELSE
									PRINTLN("[RCC MISSION] [PROCESS_PED_BODY] - ciTASK_GOTO_COORDS - Ped doing nothing(u)! Ped ", iped)
								ENDIF
							ELSE
								SET_BIT(iPedArrivedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
								IF IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
									PRINTLN("[RCC MISSION] PROCESS_PED_BODY - ciTASK_GOTO_COORDS - Ped ",iped," entering combat (h)")
									TASK_PED_GENERAL_COMBAT(tempPed,iped,bheli,bplane,bBoat,bDriver,tempveh)
								ELSE
									PRINTLN("[RCC MISSION] [PROCESS_PED_BODY] - ciTASK_GOTO_COORDS - Ped doing nothing(v)! Ped ", iped)
								ENDIF
							ENDIF
						ELIF (MC_serverBD_2.iPedGotoProgress[iped] < MAX_ASSOCIATED_GOTO_TASKS)
						AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData.sPointData[MC_serverBD_2.iPedGotoProgress[iped]].iWaitTime > 0)
							
							IF IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed], PED_ASSOCIATED_GOTO_TASK_TYPE_ACHIEVEHEADING) 
								IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iPed, SCRIPT_TASK_ACHIEVE_HEADING, TRUE)
								AND NOT IS_BIT_SET(iBSTaskAchieveHeadingWait[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
									TASK_ACHIEVE_HEADING(tempPed, GET_ASSOCIATED_GOTO_TASK_DATA__ACHIEVE_FINAL_HEADING(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed]))
									SET_BIT(iBSTaskAchieveHeadingWait[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
									PRINTLN("[RCC MISSION] [PROCESS_PED_BODY] - ciTASK_GOTO_COORDS - Associated Goto WAIT has a heading too - Ped ", iped, " Has been tasked to achieve a heading.")
								ENDIF
							ENDIF
							
							IF NOT IS_BIT_SET(iBSTaskAchieveHeadingWait[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							OR IS_HEADING_ACCEPTABLE(GET_ENTITY_HEADING(tempPed), GET_ASSOCIATED_GOTO_TASK_DATA__ACHIEVE_FINAL_HEADING(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed]), 6)
								IF NOT IS_BIT_SET(iPedGotoWaitBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
									IF SHOULD_PED_BE_GIVEN_TASK(tempPed,iped,SCRIPT_TASK_START_SCENARIO_IN_PLACE,TRUE)
										IF IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed], PED_ASSOCIATED_GOTO_TASK_TYPE_PLAY_IDLE_ANIM_ON_WAIT) 
											PRINTLN("[RCC MISSION] [PROCESS_PED_BODY] - ciTASK_GOTO_COORDS - Associated Goto WAIT has an idle anim - Ped ", iped, " idleAnim: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim)
											TASK_PERFORM_AMBIENT_ANIMATION(tempPed, iped, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim, DEFAULT, FALSE)
											CLEAR_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
											RESET_NET_TIMER(tdAssociatedGOTOWaitIdleAnimTimer[iPed])
											START_NET_TIMER(tdAssociatedGOTOWaitIdleAnimTimer[iPed])
										ELSE
											IF NOT IS_PED_IN_ANY_VEHICLE(tempPed)
												PRINTLN("[RCC MISSION] [WAIT] PROCESS_PED_BODY - ciTASK_GOTO_COORDS - Giving TASK_START_SCENARIO_IN_PLACE to ped ", iped, " iWaitTime: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData.sPointData[MC_serverBD_2.iPedGotoProgress[iped]].iWaitTime)
												TASK_START_SCENARIO_IN_PLACE(tempPed,GET_PED_FMMC_GOTO_WAIT_SCENARIO_STRING(tempPed),g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData.sPointData[MC_serverBD_2.iPedGotoProgress[iped]].iWaitTime, TRUE)
												CLEAR_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
											ELSE
												PRINTLN("[RCC MISSION] [WAIT] PROCESS_PED_BODY - ciTASK_GOTO_COORDS - Giving TASK_USE_MOBILE_PHONE_TIMED to ped ", iped, " iWaitTime: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData.sPointData[MC_serverBD_2.iPedGotoProgress[iped]].iWaitTime)
												SET_PED_CONFIG_FLAG(tempPed, PCF_PhoneDisableTextingAnimations, FALSE) 
												SET_PED_CONFIG_FLAG(tempPed, PCF_PhoneDisableTalkingAnimations, FALSE)
												//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, TRUE)
												TASK_USE_MOBILE_PHONE_TIMED(tempPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData.sPointData[MC_serverBD_2.iPedGotoProgress[iped]].iWaitTime)
												CLEAR_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
											ENDIF
										ENDIF
										BROADCAST_FMMC_PED_WAITING_AFTER_GOTO(iped)
										SET_BIT(iPedGotoWaitBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
									ENDIF
								ELSE
									IF SHOULD_PED_BE_GIVEN_TASK(tempPed,iped,SCRIPT_TASK_START_SCENARIO_IN_PLACE,TRUE)
										RESET_NET_TIMER(tdAssociatedGOTOWaitIdleAnimTimer[iPed])
										SET_BIT(iPedGotoAssCompletedWaitBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
										BROADCAST_FMMC_PED_DONE_WAITING_AFTER_GOTO(iped)
									ENDIF
								ENDIF						
							ELSE
								PRINTLN("[RCC MISSION] [PROCESS_PED_BODY] - ciTASK_GOTO_COORDS - Associated Goto WAIT has a heading too - Ped ", iped, " waiting for heading to be achieved.")
							ENDIF
						ELIF (MC_serverBD_2.iPedGotoProgress[iPed] < MAX_ASSOCIATED_GOTO_TASKS)
						AND IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed], PED_ASSOCIATED_GOTO_TASK_TYPE_ACHIEVEHEADING) 
							IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iPed, SCRIPT_TASK_ACHIEVE_HEADING, TRUE)
								TASK_ACHIEVE_HEADING(tempPed, GET_ASSOCIATED_GOTO_TASK_DATA__ACHIEVE_FINAL_HEADING(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed]))
								CLEAR_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							ENDIF
						ELSE
							IF NOT ( SHOULD_PED_GETOUT(iped) )
								SET_BIT(iPedArrivedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
								IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFlee != ciPED_FLEE_ON
								AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fRange != cfMAX_PATROL_RANGE
									IF IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
									AND NOT bPlane
										PRINTLN("[RCC MISSION] PROCESS_PED_BODY - ciTASK_GOTO_COORDS - Ped ",iped," entering combat (i)")
										TASK_PED_GENERAL_COMBAT(tempPed,iped,bheli,bplane,bBoat,bDriver,tempveh)
									ELSE
										PRINTLN("[RCC MISSION] [PROCESS_PED_BODY] - ciTASK_GOTO_COORDS - Ped doing nothing(a)! Ped ", iped)
									ENDIF
								ELSE
									PRINTLN("[RCC MISSION] [PROCESS_PED_BODY] - ciTASK_GOTO_COORDS - Ped doing nothing(b)! Ped ", iped)
								ENDIF
							ELSE
								IF bInVeh
									IF IS_COMBAT_VEHICLE(tempVeh)
										SET_BIT(iPedArrivedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
										IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFlee != ciPED_FLEE_ON
										AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fRange != cfMAX_PATROL_RANGE
											IF IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
												PRINTLN("[RCC MISSION] PROCESS_PED_BODY - - ciTASK_GOTO_COORDS - Ped ",iped," entering combat (j)")
												TASK_PED_GENERAL_COMBAT(tempPed,iped,bheli,bplane,bBoat,bDriver,tempveh)
											ELSE
												PRINTLN("[RCC MISSION] [PROCESS_PED_BODY] - ciTASK_GOTO_COORDS - Ped doing nothing(c)! Ped ", iped)
											ENDIF
										ELSE
											PRINTLN("[RCC MISSION] [PROCESS_PED_BODY] - ciTASK_GOTO_COORDS - Ped doing nothing(d)! Ped ", iped)
										ENDIF
									ELSE
										PRINTLN("[RCC MISSION] [PROCESS_PED_BODY] - ciTASK_GOTO_COORDS - Ped doing nothing(e)! Ped ", iped)
									ENDIF
								ELSE
									PRINTLN("[RCC MISSION] [PROCESS_PED_BODY] - ciTASK_GOTO_COORDS - Ped doing nothing(f)! Ped ", iped)
								ENDIF
							ENDIF
						ENDIF
						
					ELSE
						
						PRINTLN("[RCC MISSION] [PROCESS_PED_BODY] - Ped ", iped, " MC_serverBD_2.iTargetID[iped] = ciSECONDARY_GOTO.")
						
						//Secondary goto task completion:
						IF NOT ( SHOULD_PED_GETOUT(iped) )
							SET_BIT(iPedArrivedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
							IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFlee != ciPED_FLEE_ON
							AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fRange != cfMAX_PATROL_RANGE
								IF IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
									PRINTLN("[RCC MISSION] PROCESS_PED_BODY - ciTASK_GOTO_COORDS - Ped ",iped," entering combat (k)")
									TASK_PED_GENERAL_COMBAT(tempPed,iped,bheli,bplane,bBoat,bDriver,tempveh)
								ELSE
									PRINTLN("[RCC MISSION] [PROCESS_PED_BODY] - ciTASK_GOTO_COORDS - Ped doing nothing(g)! Ped ", iped)
								ENDIF
							ELSE
								PRINTLN("[RCC MISSION] [PROCESS_PED_BODY] - ciTASK_GOTO_COORDS - Ped doing nothing(h)! Ped ", iped)
							ENDIF
						ELSE
							IF bInVeh
							AND IS_COMBAT_VEHICLE(tempVeh)
								SET_BIT(iPedArrivedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
								IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFlee != ciPED_FLEE_ON
								AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fRange != cfMAX_PATROL_RANGE
									IF IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
										PRINTLN("[RCC MISSION] PROCESS_PED_BODY - ciTASK_GOTO_COORDS - Ped ",iped," entering combat (l)")
										TASK_PED_GENERAL_COMBAT(tempPed,iped,bheli,bplane,bBoat,bDriver,tempveh)
									ELSE
										PRINTLN("[RCC MISSION] [PROCESS_PED_BODY] - ciTASK_GOTO_COORDS - Ped doing nothing(i)! Ped ", iped)
									ENDIF
								ELSE
									PRINTLN("[RCC MISSION] [PROCESS_PED_BODY] - ciTASK_GOTO_COORDS - Ped doing nothing(j)! Ped ", iped)
								ENDIF
							ELSE
								PRINTLN("[RCC MISSION] [PROCESS_PED_BODY] - ciTASK_GOTO_COORDS - Ped doing nothing(k)! Ped ", iped)
							ENDIF
						ENDIF
						
					ENDIF
				ENDIF
			ELSE
				//Ped is about to get their goto state cleared up, if he should go into combat stick them in it:
				IF SHOULD_PED_GO_FROM_NOTHING_STATE_TO_COMBAT_STATE(tempPed, iPed)
				// OR (IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed)) AND NOT SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK(iped)) Might want to check spooked?
					PRINTLN("[RCC MISSION] PROCESS_PED_BODY - ciTASK_GOTO_COORDS - Ped ",iped," entering combat (m)")
					TASK_PED_GENERAL_COMBAT(tempPed,iped,bheli,bplane,bBoat,bDriver,tempveh)
				ELSE
					PRINTLN("[RCC MISSION] [PROCESS_PED_BODY] - ciTASK_GOTO_COORDS - Ped doing nothing(l)! Ped ", iped)
					SET_BIT(iBSTaskUsingIgnoreCombatChecks[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				ENDIF
			ENDIF
		BREAK // end of ciTASK_GOTO_COORDS
		
		CASE ciTASK_GOTO_ENTITY
			
			IF ((g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedEscortRange = 0) AND IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vPedHeliEscortOffset)) // Either we're not escorting anybody
			OR bPedTargetOK // Or we have a living target
				IF bTargetOK
					IF bInVeh
						fTargetDist = GET_DISTANCE_BETWEEN_COORDS(GET_VEHICLE_FRONT_COORD(tempveh),vPedTargetCoords[iped])
					ELSE
						fTargetDist = GET_DISTANCE_BETWEEN_COORDS(vPedCoords,vPedTargetCoords[iped])
					ENDIF
					
					INT iDropOutOfEscortRange
					
					IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vPedHeliEscortOffset)
					AND bHeli
						fGotoRange = VMAG(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vPedHeliEscortOffset) + 30
						iDropOutOfEscortRange = ROUND(fGotoRange) + 30
					ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedEscortRange != 0
						fGotoRange = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedEscortRange)
						iDropOutOfEscortRange = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedEscortRange + 20
					ELIF bPlane
						fGotoRange = 130
						iDropOutOfEscortRange = 50
					ELIF bHeli
						fGotoRange = 60
						iDropOutOfEscortRange = 30
					ELSE
						fGotoRange = 10
						iDropOutOfEscortRange = 30
					ENDIF
					#IF IS_DEBUG_BUILD
					IF bLMhelpfulScriptAIPrints
						PRINTLN("[RCC MISSION] PROCESS_PED_BODY - ciTASK_GOTO_ENTITY		iPed: ", iPed, " SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(tempPed), " - Processing ciTASK_GOTO_ENTITY")
						PRINTLN("[RCC MISSION] PROCESS_PED_BODY - ciTASK_GOTO_ENTITY		fTargetDist: ", fTargetDist, " targetVeh: ", targetVeh != NULL, " fGotoRange: ", fGotoRange, " iDropOutOfEscortRange: ", iDropOutOfEscortRange)
						PRINTLN("[RCC MISSION] PROCESS_PED_BODY - ciTASK_GOTO_ENTITY		vPedHeliEscortOffset: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vPedHeliEscortOffset, " iPedEscortRange: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedEscortRange, " bHeli: ", bHeli, " bPlane: ", bPlane)
					ENDIF
					#ENDIF
					IF IS_PED_ON_GOTO_ENTITY_TASK_OUT_OF_RANGE(iped, tempPed, tempTarget, fTargetDist, fGotoRange, fSpeed, fTargetSpeed, bHeli, bPlane, bDriver)
						
						PRINTLN("[RCC MISSION] PROCESS_PED_BODY - ciTASK_GOTO_ENTITY - Ped ",iped,", IS_PED_ON_GOTO_ENTITY_TASK_OUT_OF_RANGE returned TRUE")
						CLEAR_BIT(iPedArrivedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
						
						IF bDriver
							IF fTargetDist > FMAX( fGotoRange, 15.0 )
								IF NOT IS_BIT_SET(iEscortingBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
									TASK_PED_GENERAL_GOTO(tempPed,iped,vPedTargetCoords[iped],bVehOK,fTargetDist,fTargetSpeed,bheli,bplane,bBoat,tempPedTarget,bDriver,tempveh,DEFAULT,1.0)
								ELSE
									IF fTargetDist > FMAX(fGotoRange, 15.0) + iDropOutOfEscortRange
										IF bHeli OR bPlane
											CLEAR_PED_TASKS(tempPed)
										ENDIF
										CLEAR_BIT(iEscortingBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
										PRINTLN("[RCC MISSION] - ciTASK_GOTO_ENTITY - Giving TASK_VEHICLE_ESCORT Cleared iEscortingBitSet iPed ", iPed)
										
										IF IS_BIT_SET(iEscortSpeedUpdatedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
											CLEAR_BIT(iEscortSpeedUpdatedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
										ENDIF
									ELSE
										IF( fTargetDist > fGotoRange )
											SET_DRIVE_TASK_CRUISE_SPEED(tempPed,fTargetSpeed * 1.2)
										ELSE
											SET_DRIVE_TASK_CRUISE_SPEED(tempPed,fTargetSpeed)
										ENDIF
										
										IF NOT IS_BIT_SET(iEscortSpeedUpdatedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
											SET_BIT(iEscortSpeedUpdatedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
											PRINTLN("[RCC MISSION] - ciTASK_GOTO_ENTITY - TASK_VEHICLE_ESCORT Set iEscortSpeedUpdatedBitset iPed ", iPed)
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
								AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_DisableReact)
									TASK_PED_GENERAL_COMBAT(tempPed,iped,bheli,bplane,bBoat,bDriver,tempveh)
									CLEAR_BIT(iGotoObjectBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
								ELSE
									IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_VEHICLE_MISSION, FALSE, MISSION_ESCORT_REAR, tempVeh)
										PRINTLN("[RCC MISSION] - ciTASK_GOTO_ENTITY - Giving TASK_VEHICLE_ESCORT (1) ped ", iped, " fTargetSpeed = ", fTargetSpeed, " fGotoRange = ", fGotoRange)
										TASK_VEHICLE_ESCORT(tempPed,tempVeh,tempPedTarget,VEHICLE_ESCORT_REAR,fTargetSpeed,DF_SwerveAroundAllCars|DF_SteerAroundStationaryCars|DF_SteerAroundObjects|DF_SteerAroundPeds|DF_UseShortCutLinks|DF_ChangeLanesAroundObstructions|DF_DriveIntoOncomingTraffic|DF_UseSwitchedOffNodes|DF_AdjustCruiseSpeedBasedOnRoadSpeed|DF_ForceJoinInRoadDirection,fGotoRange,DEFAULT,5)
										CLEAR_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
										SET_BIT(iEscortingBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
									ELSE
										IF IS_BIT_SET(iEscortSpeedUpdatedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
											SET_DRIVE_TASK_CRUISE_SPEED(tempPed,fTargetSpeed)
										ENDIF
									ENDIF
									SET_BIT(iGotoObjectBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
								ENDIF
							ENDIF
						ELIF bDriverOK
							
							IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn != PLAYER_TWO // Trevor gets his own special case for Series A Finale
								
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetSix, ciPED_BSSix_StayInVehicleAfterGotoEntityTask)
								AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedEscortRange = 0
								AND IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vPedHeliEscortOffset)
									IF IS_ENTITY_A_VEHICLE(tempTarget)
									AND IS_PED_IN_VEHICLE(tempPed,tempveh)
										SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_LEAVE_VEHICLES, FALSE)
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, FALSE)
									ENDIF
								ENDIF
								
								IF IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
								AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_DisableReact)
									TASK_PED_GENERAL_COMBAT(tempPed,iped)
								ENDIF
								
							ELSE
								IF IS_PED_IN_VEHICLE(tempPed,tempveh)
									SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_LEAVE_VEHICLES, FALSE)
									
									//Travelling faster than 5mph
									IF NOT IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
										//Play outro anim once
										TASK_PERFORM_AMBIENT_ANIMATION(tempPed, iped, ciPED_IDLE_ANIM__TREVOR_NARCFIN_VAN_OUTRO, DEFAULT, FALSE)
									ELSE
										//play driving_base, get in combat if needed etc
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, FALSE)
										
										IF NOT IS_BIT_SET(iLocalBoolCheck11, LBOOL11_TREVOR_VAN_CLIPSET_APPLIED)
										AND IS_BIT_SET(iLocalBoolCheck11, LBOOL11_TREVOR_VAN_IDLE_CLIPSET_LOADED)
											PRINTLN("[RCC MISSION] PROCESS_PED_BODY - ciTASK_GOTO_ENTITY - Ped ",iped," called SET_PED_IN_VEHICLE_CONTEXT w HEIST_NARCOTICS_TREVOR_MULE_FRONT_RIGHT, call 1")
											SET_PED_IN_VEHICLE_CONTEXT(tempPed, GET_HASH_KEY("HEIST_NARCOTICS_TREVOR_MULE_FRONT_RIGHT"))
											SET_BIT(iLocalBoolCheck11, LBOOL11_TREVOR_VAN_CLIPSET_APPLIED)
										ENDIF
										
										//This is now covered by the vehicle context above:
										//IF NOT IS_PED_IN_COMBAT(tempPed)
										//	TASK_PERFORM_AMBIENT_ANIMATION(tempPed, iped, ciPED_IDLE_ANIM__TREVOR_NARCFIN_VAN_DRIVING_IDLE, DEFAULT, FALSE)
										//ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							TASK_PED_GENERAL_GOTO(tempPed,iped,vPedTargetCoords[iped],bVehOK,fTargetDist,fTargetSpeed,bheli,bplane,bBoat,tempPedTarget,bDriver,tempveh)
						ENDIF
						
					// Close behaviour
					ELSE
						
						PRINTLN("[RCC MISSION] PROCESS_PED_BODY - ciTASK_GOTO_ENTITY - Ped ",iped,", IS_PED_ON_GOTO_ENTITY_TASK_OUT_OF_RANGE returned FALSE")
						
						// Escort Heli
						IF bHeli
						AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vPedHeliEscortOffset)
						AND DOES_ENTITY_EXIST(targetVeh)
						AND IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(targetVeh))
							
							IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_VEHICLE_MISSION, FALSE, MISSION_ESCORT_REAR, tempVeh)
								PRINTLN("[RCC MISSION] PROCESS_PED_BODY - ciTASK_GOTO_ENTITY - Calling TASK_HELI_ESCORT_HELI for ped ",iPed," with vPedHeliEscortOffset ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vPedHeliEscortOffset)
								TASK_HELI_ESCORT_HELI(tempPed, tempVeh, targetVeh, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vPedHeliEscortOffset)
								
								CLEAR_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
								SET_BIT(iEscortingBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
							ENDIF
							
						// Escort at Range
						ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedEscortRange > 0
						
							IF IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
							AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_DisableReact)
								TASK_PED_GENERAL_COMBAT(tempPed,iped,bheli,bplane,bBoat,bDriver,tempveh)
								CLEAR_BIT(iEscortingBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
								CLEAR_BIT(iGotoObjectBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
							ELSE
								IF NOT bInVeh // url:bugstar:3624201
									VEHICLE_INDEX origVeh
									origVeh = GET_ORIGINAL_VALID_VEHICLE(iPed, tempPed)
									IF IS_VEHICLE_DRIVEABLE(origVeh, TRUE)
										FLOAT vehDist
										vehDist = GET_DISTANCE_BETWEEN_ENTITIES(origVeh, tempPed)
										IF vehDist > 40.0
											PRINTLN("[RCC MISSION] - ciTASK_GOTO_ENTITY - Giving TASK_PED_GOTO_ANY_MEANS to original vehicle ped ", iped, " distance: ", vehDist)
											TASK_PED_GOTO_ANY_MEANS(tempPed,GET_ENTITY_COORDS(origVeh),vehDist,iped,tempVeh,binveh,bdriver)
										ELSE
											PRINTLN("[RCC MISSION] - ciTASK_GOTO_ENTITY - Giving TASK_PED_ENTER_VEH to original vehicle ped ", iped, " distance: ", vehDist)
											TASK_PED_ENTER_VEH(tempPed,iped,origVeh)
										ENDIF
									ELSE
										// Would put combat task in here instead, but logic has only reached this point if he's not spooked or disable reactions is set
										PRINTLN("[RCC MISSION] - ciTASK_GOTO_ENTITY - Giving TASK_PED_GENERAL_GOTO to escort ped ",iped," who lost their vehicle")
										TASK_PED_GENERAL_GOTO(tempPed,iped,vPedTargetCoords[iped],bVehOK,fTargetDist,fTargetSpeed,bheli,bplane,bBoat,tempPedTarget,bDriver,tempveh)
									ENDIF
								ELSE
									
									IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_VEHICLE_MISSION, FALSE, MISSION_ESCORT_REAR, tempVeh)
										IF bHeli
											INT iHeliFlightHeight
											iHeliFlightHeight = 30
											INT iHeliMinFlightHeight
											iHeliMinFlightHeight = 15
											
											PRINTLN("[RCC MISSION] - ciTASK_GOTO_ENTITY - Giving TASK_HELI_MISSION ped ", iped, " fTargetSpeed = ", fTargetSpeed, " fGotoRange = ", fGotoRange)
											TASK_HELI_MISSION(tempPed,tempVeh,NULL,tempPedTarget,vPedTargetCoords[iped],MISSION_ESCORT_REAR,fTargetSpeed,0,-1,iHeliFlightHeight,iHeliMinFlightHeight)
											SET_BIT(iEscortingBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
											CLEAR_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
										ELSE
											PRINTLN("[RCC MISSION] - ciTASK_GOTO_ENTITY - Giving TASK_VEHICLE_ESCORT (2) ped ", iped, " fTargetSpeed = ", fTargetSpeed, " fGotoRange = ", fGotoRange)
											TASK_VEHICLE_ESCORT(tempPed,tempVeh,tempPedTarget,VEHICLE_ESCORT_REAR,fTargetSpeed,DF_SwerveAroundAllCars|DF_SteerAroundStationaryCars|DF_SteerAroundObjects|DF_SteerAroundPeds|DF_UseShortCutLinks|DF_ChangeLanesAroundObstructions|DF_DriveIntoOncomingTraffic|DF_UseSwitchedOffNodes|DF_AdjustCruiseSpeedBasedOnRoadSpeed|DF_ForceJoinInRoadDirection,fGotoRange,DEFAULT,5)
											SET_BIT(iEscortingBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
											SET_BIT(iGotoObjectBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
											CLEAR_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
										ENDIF
									ELSE
										IF( fTargetDist > fGotoRange )
											SET_DRIVE_TASK_CRUISE_SPEED(tempPed,fTargetSpeed * 1.2)
										ELSE
											SET_DRIVE_TASK_CRUISE_SPEED(tempPed,fTargetSpeed)
										ENDIF
									ENDIF
									
								ENDIF
							ENDIF
						
						// Standard behaviour
						ELSE
							SET_BIT(iPedArrivedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
							CLEAR_BIT(iGotoObjectBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
							IF IS_ENTITY_A_VEHICLE(tempTarget)
								tempveh = NET_TO_VEH(MC_serverBD.niTargetID[iped])
								IF NOT IS_PED_IN_VEHICLE(tempPed,tempveh)
									IF ARE_ANY_VEHICLE_SEATS_FREE(tempveh)
										
										IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn = IG_RASHCOSVKI)
										OR (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn = PLAYER_TWO)
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, TRUE)
										ENDIF
										
										IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetSix, ciPED_BSSix_StayInVehicleAfterGotoEntityTask)
											IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicleSeatPreference > ciFMMC_SeatPreference_Driver
												PRINTLN("[RCC MISSION] PROCESS_PED_BODY - ciTASK_GOTO_ENTITY - Setting PCF_PreventAutoShuffleToDriversSeat on ped ",iped," due to go to vehicle task not to driver's seat!")
												SET_PED_CONFIG_FLAG(tempPed, PCF_PreventAutoShuffleToDriversSeat, TRUE)
											ENDIF
											IF IS_PED_IN_GROUP(tempPed)
												PRINTLN("[RCC MISSION] PROCESS_PED_BODY - ciTASK_GOTO_ENTITY - Setting PCF_DontLeaveVehicleIfLeaderNotInVehicle on ped ",iped," due to go to vehicle task while in group!")
												SET_PED_CONFIG_FLAG(tempPed, PCF_DontLeaveVehicleIfLeaderNotInVehicle, TRUE)
											ENDIF
										ENDIF
										
										TASK_PED_ENTER_VEH(tempPed,iped,tempveh,DEFAULT_TIME_NEVER_WARP)
									ELSE
										IF IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
											TASK_PED_GENERAL_COMBAT(tempPed,iped,bheli,bplane,bBoat,bDriver,tempveh)
										ENDIF
									ENDIF
								ELSE
									IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn != PLAYER_TWO
										
										IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetSix, ciPED_BSSix_StayInVehicleAfterGotoEntityTask)
											SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_LEAVE_VEHICLES, FALSE)
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, FALSE)
										ENDIF
										
										IF IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
											TASK_PED_GENERAL_COMBAT(tempPed,iped,bheli,bplane,bBoat,bDriver,tempveh)
										ENDIF
										
									ELSE
										SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_LEAVE_VEHICLES, FALSE)
										
										IF NOT IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
											
											BOOL bBusyReaction_Idle
											bBusyReaction_Idle = TRUE // - do we want trevor to keep doing his idles if there's combat going on nearby?
											
											PROCESS_PED_ANIMATION(tempPed, iped, FALSE, TRUE, FALSE, TRUE, bBusyReaction_Idle)
										ELSE
											//play driving_base, get in combat if needed etc
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, FALSE)
											
											IF NOT IS_BIT_SET(iLocalBoolCheck11, LBOOL11_TREVOR_VAN_CLIPSET_APPLIED)
											AND IS_BIT_SET(iLocalBoolCheck11, LBOOL11_TREVOR_VAN_IDLE_CLIPSET_LOADED)
												PRINTLN("[RCC MISSION] PROCESS_PED_BODY - ciTASK_GOTO_ENTITY - Ped ",iped," called SET_PED_IN_VEHICLE_CONTEXT w HEIST_NARCOTICS_TREVOR_MULE_FRONT_RIGHT, call 2")
												SET_PED_IN_VEHICLE_CONTEXT(tempPed, GET_HASH_KEY("HEIST_NARCOTICS_TREVOR_MULE_FRONT_RIGHT"))
												SET_BIT(iLocalBoolCheck11, LBOOL11_TREVOR_VAN_CLIPSET_APPLIED)
											ENDIF
											
											//This is now covered by the vehicle context above:
											//IF NOT IS_PED_IN_COMBAT(tempPed)
											//	TASK_PERFORM_AMBIENT_ANIMATION(tempPed, iped, ciPED_IDLE_ANIM__TREVOR_NARCFIN_VAN_DRIVING_IDLE, DEFAULT, FALSE)
											//ENDIF
										ENDIF
										
									ENDIF
								ENDIF
							ELIF IS_ENTITY_AN_OBJECT(tempTarget)
								IF NOT IS_ENTITY_ATTACHED_TO_ANY_PED(tempTarget)
									IF bInVeh
										TASK_PED_LEAVE_VEHICLE(tempPed,iped)
									ELSE
										TASK_PED_SHORT_RANGE_GOTO(tempPed,iped,vPedTargetCoords[iped])
										IF GET_DISTANCE_BETWEEN_ENTITIES(tempPed,NET_TO_ENT(MC_serverBD.niTargetID[iped])) < 2
											ATTACH_PORTABLE_PICKUP_TO_PED(NET_TO_OBJ(MC_serverBD.niTargetID[iped]),tempPed)
											iobj =GET_INT_FROM_OBJ_ID(MC_serverBD.niTargetID[iped])
											IF iobj > -1
												SET_BIT(iAICarryBitset,iobj)
											ENDIF
										ENDIF
									ENDIF
								ELSE
									IF IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
										TASK_PED_GENERAL_COMBAT(tempPed,iped,bheli,bplane,bBoat,bDriver,tempveh)
									ENDIF
								ENDIF
							ELSE
								IF bInVeh
								AND NOT bheli AND NOT bplane AND NOT bBoat
								AND NOT bCombatVehicle
									TASK_PED_LEAVE_VEHICLE(tempPed,iped)
								ELSE
									IF IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
										TASK_PED_GENERAL_COMBAT(tempPed,iped,bheli,bplane,bBoat,bDriver,tempveh)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				// If we're escorting and the driver we're escorting is dead, then we get to do combat:
				TASK_PED_GENERAL_COMBAT(tempPed,iped,bheli,bplane,bBoat,bDriver,tempveh)
				CLEAR_BIT(iGotoObjectBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			ENDIF

		BREAK // End of ciTASK_GOTO_ENTITY
		
		CASE ciTASK_DEFEND_ENTITY
			
			IF CHECK_FOR_DROP_OFF_FLEE(tempPed,iped)
				EXIT
			ENDIF
			
			IF bTargetOK
				fTargetDist = GET_DISTANCE_BETWEEN_COORDS(vPedCoords,vPedTargetCoords[iped])
				IF  bheli OR bplane
					fGotoRange = 298
				ENDIF
				IF fTargetDist > fGotoRange
				OR ((fSpeed > 5.0 OR fTargetSpeed > 5.0) AND NOT bheli AND NOT bplane)
					CLEAR_BIT( iPedArrivedBitset[GET_LONG_BITSET_INDEX( iped ) ], GET_LONG_BITSET_BIT( iPed ) )
					IF bDriver
						IF fTargetDist > fGotoRange
							TASK_PED_GENERAL_GOTO(tempPed,iped,vPedTargetCoords[iped],bVehOK,fTargetDist,fTargetSpeed,bheli,bplane,bBoat,tempPedTarget,bDriver,tempveh)
						ELSE
							TASK_PED_GENERAL_COMBAT(tempPed,iped,bheli,bplane,bBoat,bDriver,tempveh)
						ENDIF
					ELIF bDriverOK
						TASK_PED_GENERAL_COMBAT(tempPed,iped)
					ELSE
						TASK_PED_GENERAL_GOTO(tempPed,iped,vPedTargetCoords[iped],bVehOK,fTargetDist,fTargetSpeed,bheli,bplane,bBoat,tempPedTarget,bDriver,tempveh)
					ENDIF
				ELSE
					SET_BIT( iPedArrivedBitset[ GET_LONG_BITSET_INDEX( iped ) ], GET_LONG_BITSET_BIT( iPed ) )
					
					IF bInVeh
					AND NOT bheli AND NOT bplane AND NOT bBoat
					AND NOT bCombatVehicle
						TASK_PED_LEAVE_VEHICLE( tempPed, iped )
					ELSE
						TASK_PED_GENERAL_COMBAT( tempPed,iped,bheli,bplane,bBoat,bDriver,tempveh )
					ENDIF
				ENDIF
			ENDIF

		BREAK // End of ciTASK_DEFEND_ENTITY
		
		CASE ciTASK_ATTACK_ENTITY
			
			IF CHECK_FOR_DROP_OFF_FLEE(tempPed,iped)
				EXIT
			ENDIF
				
			IF bTargetOK
				fTargetDist = GET_DISTANCE_BETWEEN_COORDS(vPedCoords,vPedTargetCoords[iped])
				IF bheli OR bplane
					fGotoRange = 298
				ENDIF
				
				IF fTargetDist > fGotoRange
				OR ((fSpeed > 5.0 OR fTargetSpeed > 5.0) AND NOT bheli AND NOT bplane)
					CLEAR_BIT( iPedArrivedBitset[ GET_LONG_BITSET_INDEX( iped ) ], GET_LONG_BITSET_BIT( iPed ) )
					IF bDriver
						IF fTargetDist > fGotoRange
							PRINTLN("PED_BODY - ciTASK_ATTACK_ENTITY - Ped ", iPed, " Attack (1)")
							TASK_PED_GENERAL_GOTO(tempPed,iped,vPedTargetCoords[iped],bVehOK,fTargetDist,fTargetSpeed,bheli,bplane,bBoat,tempPedTarget,bDriver,tempveh)
						ELSE
							IF NOT IS_PED_INJURED(tempPedTarget)
								PRINTLN("PED_BODY - ciTASK_ATTACK_ENTITY - Ped ", iPed, " Attack (2)")
								TASK_PED_COMBAT_PED(tempPed,iped,tempPedTarget,bheli,bplane,bBoat,bDriver,tempveh)
							ELSE
//								IF NOT IS_ENTITY_DEAD(tempTarget)
//									IF bTargetVehicle
//										TASK_PED_VEHICLE_COMBAT_VEH(tempPed,iped,tempveh,GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(tempTarget),bheli,bplane,bBoat)
//									ENDIF
//								ELSE
									PRINTLN("PED_BODY - ciTASK_ATTACK_ENTITY - Ped ", iPed, " Attack (3)")
									TASK_PED_GENERAL_COMBAT(tempPed,iped,bheli,bplane,bBoat,bDriver,tempveh)
//								ENDIF
							ENDIF
						ENDIF
					ELIF bDriverOK
						IF NOT IS_PED_INJURED(tempPedTarget)
							PRINTLN("PED_BODY - ciTASK_ATTACK_ENTITY - Ped ", iPed, " Attack (4)")
							TASK_PED_COMBAT_PED(tempPed,iped,tempPedTarget,bheli,bplane,bBoat,bDriver,tempveh)
						ELSE
							PRINTLN("PED_BODY - ciTASK_ATTACK_ENTITY - Ped ", iPed, " Attack (5)")
							TASK_PED_GENERAL_COMBAT(tempPed,iped,bheli,bplane,bBoat,bDriver,tempveh)
						ENDIF
					ELSE
						PRINTLN("PED_BODY - ciTASK_ATTACK_ENTITY - Ped ", iPed, " Attack (6)")
						TASK_PED_GENERAL_GOTO(tempPed,iped,vPedTargetCoords[iped],bVehOK,fTargetDist,fTargetSpeed,bheli,bplane,bBoat,tempPedTarget,bDriver,tempveh)
					ENDIF
				ELSE
					SET_BIT( iPedArrivedBitset[ GET_LONG_BITSET_INDEX( iped ) ], GET_LONG_BITSET_BIT( iPed ) )
					IF NOT bheli AND NOT bplane  AND NOT bBoat
					AND NOT bCombatVehicle
						IF bInVeh
							PRINTLN("PED_BODY - ciTASK_ATTACK_ENTITY - Ped ", iPed, " Attack (7)")
							TASK_PED_LEAVE_VEHICLE(tempPed,iped)
						ELSE
							PRINTLN("PED_BODY - ciTASK_ATTACK_ENTITY - Ped ", iPed, " Attack (8)")
							TASK_PED_COMBAT_ENTITY(tempPed,iped,tempTarget,bheli,bplane,bBoat,bDriver,tempveh)
						ENDIF
					ELSE
						IF bDriver
							IF NOT IS_PED_INJURED(tempPedTarget)
								PRINTLN("PED_BODY - ciTASK_ATTACK_ENTITY - Ped ", iPed, " Attack (9)")
								TASK_PED_COMBAT_PED(tempPed,iped,tempPedTarget,bheli,bplane,bBoat,bDriver,tempveh)
							ELSE
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwelve, ciPed_BSTwelve_AllowVehicleCoordAttack)
								AND NOT IS_ENTITY_DEAD(tempTarget)
									PRINTLN("PED_BODY - ciTASK_ATTACK_ENTITY - Ped ", iPed, " Attack (10) coords @ ", GET_STRING_FROM_VECTOR(vPedTargetCoords[iped]))
									TASK_PED_VEHICLE_COMBAT_COORDS(tempPed,iped,tempveh,vPedTargetCoords[iped],bheli,bplane,bBoat)
								ELSE
									PRINTLN("PED_BODY - ciTASK_ATTACK_ENTITY - Ped ", iPed, " Attack (11)")
									TASK_PED_GENERAL_COMBAT(tempPed,iped,bheli,bplane,bBoat,bDriver,tempveh)
								ENDIF
							ENDIF
						ELIF bDriverOK
							IF NOT IS_PED_INJURED(tempPedTarget)
								PRINTLN("PED_BODY - ciTASK_ATTACK_ENTITY - Ped ", iPed, " Attack (12)")
								TASK_PED_COMBAT_PED(tempPed,iped,tempPedTarget,bheli,bplane,bBoat,bDriver,tempveh)
							ELSE
								PRINTLN("PED_BODY - ciTASK_ATTACK_ENTITY - Ped ", iPed, " Attack (13)")
								TASK_PED_GENERAL_COMBAT(tempPed,iped,bheli,bplane,bBoat,bDriver,tempveh)
							ENDIF
						ELSE
							IF bInVeh
								PRINTLN("PED_BODY - ciTASK_ATTACK_ENTITY - Ped ", iPed, " Attack (14)")
								TASK_PED_LEAVE_VEHICLE(tempPed,iped)
							ELSE
								PRINTLN("PED_BODY - ciTASK_ATTACK_ENTITY - Ped ", iPed, " Attack (15)")
								TASK_PED_COMBAT_ENTITY(tempPed,iped,tempTarget,bheli,bplane,bBoat,bDriver,tempveh)
							ENDIF
						ENDIF
					ENDIF

				ENDIF
			ENDIF

		BREAK //end of ciTASK_ATTACK_ENTITY
		
		CASE ciTASK_AIM_AT_ENTITY
			
			IF CHECK_FOR_DROP_OFF_FLEE(tempPed,iped)
				EXIT
			ENDIF
			
			IF bTargetOK
				ENTITY_INDEX entityTarget
				
				IF DOES_ENTITY_EXIST(tempPedTarget)					
					entityTarget = tempPedTarget
				ELSE
					entityTarget = tempTarget
				ENDIF
				
				fTargetDist = GET_DISTANCE_BETWEEN_COORDS(vPedCoords,vPedTargetCoords[iped])
				IF  bheli OR bplane
					fGotoRange = 298
				ENDIF
				
				PRINTLN("[PROCESS_PED_BODY] - ciTASK_AIM_AT_ENTITY - iPed: ", iPed, " fTargetDist: ", fTargetDist, " fgotoRange: ", fGotoRange)
					
				IF fTargetDist > fGotoRange
				OR ((fSpeed > 5.0 OR fTargetSpeed > 5.0) AND NOT bheli AND NOT bplane)
					CLEAR_BIT( iPedArrivedBitset[ GET_LONG_BITSET_INDEX( iped ) ], GET_LONG_BITSET_BIT( iPed ) )
					IF bDriver
						IF fTargetDist > fGotoRange
							TASK_PED_GENERAL_GOTO(tempPed,iped,vPedTargetCoords[iped],bVehOK,fTargetDist,fTargetSpeed,bheli,bplane,bBoat,tempPedTarget,bDriver,tempveh)
						ELSE
							IF NOT IS_ENTITY_DEAD(entityTarget)
								TASK_PED_AIM_AT_ENTITY(tempPed,iped,entityTarget,bheli,bplane,bBoat,bDriver,tempveh)
							ENDIF
						ENDIF
					ELIF bDriverOK
						IF NOT IS_ENTITY_DEAD(entityTarget)
							TASK_PED_AIM_AT_ENTITY(tempPed,iped,entityTarget,bheli,bplane,bBoat,bDriver,tempveh)
						ENDIF
					ELSE
						TASK_PED_GENERAL_GOTO(tempPed,iped,vPedTargetCoords[iped],bVehOK,fTargetDist,fTargetSpeed,bheli,bplane,bBoat,tempPedTarget,bDriver,tempveh)
					ENDIF
				ELSE
					SET_BIT( iPedArrivedBitset[ GET_LONG_BITSET_INDEX( iped ) ], GET_LONG_BITSET_BIT( iPed ) )
					IF NOT bheli AND NOT bplane  AND NOT bBoat
					AND NOT bCombatVehicle
						IF bInVeh
							TASK_PED_LEAVE_VEHICLE(tempPed,iped)
						ELSE
							IF NOT IS_ENTITY_DEAD(entityTarget)
								TASK_PED_AIM_AT_ENTITY(tempPed,iped,tempTarget,bheli,bplane,bBoat,bDriver,tempveh)
							ELSE
							
							ENDIF
						ENDIF
					ELSE
						IF bDriver
							IF NOT IS_ENTITY_DEAD(entityTarget)
								TASK_PED_AIM_AT_ENTITY(tempPed,iped,entityTarget,bheli,bplane,bBoat,bDriver,tempveh)
							ENDIF
						ELIF bDriverOK
							IF NOT IS_ENTITY_DEAD(entityTarget)
								TASK_PED_AIM_AT_ENTITY(tempPed,iped,entityTarget,bheli,bplane,bBoat,bDriver,tempveh)
							ENDIF
						ELSE
							IF bInVeh
								TASK_PED_LEAVE_VEHICLE(tempPed,iped)
							ELSE
								IF NOT IS_ENTITY_DEAD(entityTarget)
									TASK_PED_AIM_AT_ENTITY(tempPed,iped,entityTarget,bheli,bplane,bBoat,bDriver,tempveh)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[PROCESS_PED_BODY] - ciTASK_AIM_AT_ENTITY - iPed: ", iPed, " Target NOT ok...")
			ENDIF
			
		BREAK //end of ciTASK_AIM_AT_ENTITY
		
		CASE ciTASK_TAKE_COVER
			
			IF CHECK_FOR_DROP_OFF_FLEE(tempPed,iped)
			OR IS_PED_IN_GROUP(tempPed)
				EXIT
			ENDIF
			
			SET_PED_USING_ACTION_MODE(tempPed, TRUE, -1, "DEFAULT_ACTION")
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn = PLAYER_TWO
				SET_PED_RESET_FLAG(tempPed, PRF_UseKinematicPhysics, TRUE) // Flag to help Trevor push players out of the way as he tries to get in cover
			ENDIF
			
			BOOL bCover, bAnimate, bAlwaysInCover
			bAnimate = TRUE
			bCover = TRUE
			bAlwaysInCover = FALSE
			
			bAlwaysInCover = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour, ciPED_BSFour_TakeCover_AlwaysTakeCover)
			
			IF MC_serverBD_2.iPedGotoProgress[iped] > 0
			AND MC_serverBD_2.iPedGotoProgress[iped] < (MAX_ASSOCIATED_GOTO_TASKS + 1)
				//Check if the goto we've just completed requires us to be AlwaysInCover now:
				IF IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iped], PED_ASSOCIATED_GOTO_TASK_TYPE_COVER) 
					bAlwaysInCover = TRUE
				ENDIF
			ENDIF
			
			IF NOT bAlwaysInCover
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,FALSE)
			ENDIF
			
			IF NOT bAlwaysInCover
			AND SHOULD_PED_START_COMBAT(tempPed, iped)
				
				BOOL bInCombat
				bInCombat = IS_PED_IN_COMBAT(tempPed) OR IS_PED_FLEEING(tempPed)
				
				IF (NOT bInCombat)
				AND IS_BIT_SET(iPedCoverTask_ReachedCombat[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					SET_PED_ALERTNESS(tempPed,AS_NOT_ALERT) // Go back to being in cover
					bCover = TRUE
					PRINTLN("[RCC MISSION] - ciTASK_TAKE_COVER - Ped running ciTASK_TAKE_COVER is no longer in combat so reset alertness and return to cover task")
				ELSE
					IF bInCombat
						IF NOT IS_BIT_SET(iPedCoverTask_ReachedCombat[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							SET_BIT(iPedCoverTask_ReachedCombat[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							PRINTLN("[RCC MISSION] - ciTASK_TAKE_COVER - Ped running ciTASK_TAKE_COVER has reached combat!")
						ENDIF
						
						bAnimate = FALSE // The only time we want to stop animating is if we're actually in combat
					//ELSE
					//	maybe increment a counter and go back to cover seeking after a few seconds of not being in combat?
					ENDIF
					
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn != IG_RASHCOSVKI
					OR bInCombat
						
						TASK_PED_GENERAL_COMBAT(tempPed,iped,bHeli,bPlane,bBoat,bDriver,tempveh)
						bCover = FALSE // Stop seeking cover, we want to get into combat if we can
						
					ENDIF
				ENDIF
				
			ELSE
				CLEAR_BIT(iPedCoverTask_ReachedCombat[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			ENDIF
			
			IF NOT PROCESS_PED_ANIMATION(tempPed, iped, TRUE, FALSE, FALSE, TRUE, bAnimate)
			AND bCover
				IF bInVeh
					TASK_PED_LEAVE_VEHICLE(tempPed,iped)
				ELSE
					IF SHOULD_PED_BE_GIVEN_TASK(tempPed,iped,SCRIPT_TASK_STAY_IN_COVER)
						//check if a player within some distance
						PLAYER_INDEX tempPlayer
						tempPlayer = INT_TO_PLAYERINDEX(GET_NEAREST_PLAYER_TO_ENTITY(tempPed))
						
						IF tempPlayer != INVALID_PLAYER_INDEX()
						AND (GET_DISTANCE_BETWEEN_PEDS(tempPed,GET_PLAYER_PED(tempPlayer)) <= 250)
							
							SET_PED_TO_LOAD_COVER(tempPed,TRUE)
							
							IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn != PLAYER_TWO
							OR IS_BIT_SET(iLocalBoolCheck8, LBOOL8_TREVOR_COVER_CLIPSET_LOADED)
								
								VECTOR vCover
								vCover = GET_PED_COVER_LOCATION(iped, tempPed, MC_serverBD_2.iPedGotoProgress[iped])
								
								IF NOT IS_PED_DEFENSIVE_AREA_ACTIVE(tempPed, FALSE)
								OR NOT ARE_VECTORS_ALMOST_EQUAL(GET_PED_DEFENSIVE_AREA_POSITION(tempPed), vCover)
									SET_PED_SPHERE_DEFENSIVE_AREA(tempPed, vCover, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fRange)
								ENDIF
								
								IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vTakeCoverFromThisLoc)
									SET_PED_DEFENSIVE_AREA_DIRECTION(tempPed,g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vTakeCoverFromThisLoc)
								ENDIF
								
								IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn = PLAYER_TWO OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn = IG_RASHCOSVKI)
									
									IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn = IG_RASHCOSVKI)
										
										FLOAT fRashBurstTime
										fRashBurstTime = 4.25
										FLOAT fRashBlindfireChance
										fRashBlindfireChance = 1.0
										
										IF (GET_COMBAT_FLOAT(tempPed, CCF_BLIND_FIRE_CHANCE) != fRashBlindfireChance)
											SET_COMBAT_FLOAT(tempPed, CCF_BLIND_FIRE_CHANCE, fRashBlindfireChance) // Default = 0.1
											SET_COMBAT_FLOAT(tempPed, CCF_TIME_BETWEEN_BURSTS_IN_COVER, fRashBurstTime) // Default = 1.25
											
											SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_BLIND_FIRE_IN_COVER, TRUE)
											PRINTLN("[RCC MISSION] - ciTASK_TAKE_COVER - Rashkovsky ped ",iped," running TASK_STAY_IN_COVER, set CA_BLIND_FIRE_IN_COVER, CCF_BLIND_FIRE_CHANCE to ",fRashBlindfireChance," and CCF_TIME_BETWEEN_BURSTS_IN_COVER to ",fRashBurstTime)
										ENDIF
										
									ELIF ( g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn = PLAYER_TWO )
										SET_PED_CONFIG_FLAG(tempPed, PCF_ForcePedToFaceRightInCover, TRUE)
									ENDIF
									
									IF NOT IS_PED_IN_COVER(tempPed)
										SET_COMBAT_FLOAT(tempPed, CCF_STRAFE_WHEN_MOVING_CHANCE, 0.0)
										PRINTLN("[RCC MISSION] - ciTASK_TAKE_COVER - Ped ",iped," running TASK_STAY_IN_COVER, set CCF_STRAFE_WHEN_MOVING_CHANCE to 0.0 to get the ped in cover without aiming")
									ENDIF
								ENDIF
								
								SET_PED_CAN_PEEK_IN_COVER(tempPed, FALSE)
								
								//fix for double cover tasking causing pops with Trevor, B*2237788
								IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn = PLAYER_TWO AND IS_PED_IN_COVER(tempPed)
									PRINTLN("[RCC MISSION] - ciTASK_TAKE_COVER - Trevor ped ",iped," already in cover. Quitting out of TASK_STAY_IN_COVER.")
									EXIT
								ENDIF
								
								TASK_STAY_IN_COVER(tempPed)
								CLEAR_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
								
								PRINTLN("[RCC MISSION] - ciTASK_TAKE_COVER - Ped ",iped," running TASK_STAY_IN_COVER, location ",vCover,", range ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fRange,", take cover from ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vTakeCoverFromThisLoc)
							ENDIF
						ELSE
							IF NOT (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn = IG_RASHCOSVKI)
								SET_PED_TO_LOAD_COVER(tempPed,FALSE)
							ENDIF
						ENDIF
					ELSE
						IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn = PLAYER_TWO OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn = IG_RASHCOSVKI)
						AND IS_PED_IN_COVER(tempPed)
						AND GET_COMBAT_FLOAT(tempPed, CCF_STRAFE_WHEN_MOVING_CHANCE) = 0.0
							SET_COMBAT_FLOAT(tempPed, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
							PRINTLN("[RCC MISSION] - ciTASK_TAKE_COVER - Ped ",iped," running TASK_STAY_IN_COVER, set CCF_STRAFE_WHEN_MOVING_CHANCE to 1.0 as the ped is now in cover")
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF NOT bCover
				AND NOT (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn = IG_RASHCOSVKI)
					SET_PED_TO_LOAD_COVER(tempPed,FALSE)
				ENDIF
				
				IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn = PLAYER_TWO OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn = IG_RASHCOSVKI)
				AND GET_COMBAT_FLOAT(tempPed, CCF_STRAFE_WHEN_MOVING_CHANCE) = 0.0
					SET_COMBAT_FLOAT(tempPed, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
					PRINTLN("[RCC MISSION] - ciTASK_TAKE_COVER - Ped ",iped," running TASK_STAY_IN_COVER, set CCF_STRAFE_WHEN_MOVING_CHANCE to 1.0 as the ped should no longer do cover things")
				ENDIF
			ENDIF
			
		BREAK
		
		CASE ciTASK_GOTO_PLAYER
		CASE ciTASK_DEFEND_PLAYER
			
			IF bTargetOK
				fTargetDist = GET_DISTANCE_BETWEEN_COORDS(vPedCoords,vPedTargetCoords[iped])
				IF  bheli OR bplane
					fGotoRange = 298
				ENDIF
				IF fTargetDist > fGotoRange
				OR ((fSpeed > 5.0 OR fTargetSpeed > 5.0) AND NOT bheli AND NOT bplane)
					CLEAR_BIT(iPedArrivedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					IF bDriver
						IF fTargetDist > fGotoRange
							TASK_PED_GENERAL_GOTO(tempPed,iped,vPedTargetCoords[iped],bVehOK,fTargetDist,fTargetSpeed,bheli,bplane,bBoat,tempPedTarget,bDriver,tempveh)
						ELSE
							TASK_PED_GENERAL_COMBAT(tempPed,iped,bheli,bplane,bBoat,bDriver,tempveh)
						ENDIF
					ELIF bDriverOK
						TASK_PED_GENERAL_COMBAT(tempPed,iped)
					ELSE
						TASK_PED_GENERAL_GOTO(tempPed,iped,vPedTargetCoords[iped],bVehOK,fTargetDist,fTargetSpeed,bheli,bplane,bBoat,tempPedTarget,bDriver,tempveh)
					ENDIF
				ELSE
					SET_BIT(iPedArrivedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					IF IS_PED_IN_ANY_VEHICLE(tempPedTarget)
						tempVeh = GET_VEHICLE_PED_IS_IN(tempPedTarget)
						IF IS_VEHICLE_DRIVEABLE(tempVeh)
							IF NOT IS_PED_IN_VEHICLE(tempPed,tempveh,TRUE)
								IF ARE_ANY_VEHICLE_SEATS_FREE(tempVeh)
									TASK_PED_ENTER_VEH(tempPed,iped,tempVeh)
								ELSE
									TASK_PED_GENERAL_COMBAT(tempPed,iped,bheli,bplane,bBoat,bDriver,tempveh)
								ENDIF
							ELSE
								TASK_PED_GENERAL_COMBAT(tempPed,iped,bheli,bplane,bBoat,bDriver,tempveh)
							ENDIF
						ELSE
							TASK_PED_GENERAL_COMBAT(tempPed,iped,bheli,bplane,bBoat,bDriver,tempveh)
						ENDIF
					ELSE
						TASK_PED_GENERAL_COMBAT(tempPed,iped)
					ENDIF
				ENDIF
			ENDIF
			
		BREAK // End of ciTASK_GOTO_PLAYER and ciTASK_DEFEND_PLAYER
		
		CASE ciTASK_ATTACK_PLAYER
			
			IF bTargetOK		
				fTargetDist = GET_DISTANCE_BETWEEN_COORDS(vPedCoords,vPedTargetCoords[iped])
				IF  bheli OR bplane
					fGotoRange = 298
				ENDIF
				IF fTargetDist > fGotoRange
				OR ((fSpeed > 5.0 OR fTargetSpeed > 5.0)AND NOT bheli AND NOT bplane)
					CLEAR_BIT(iPedArrivedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					IF bDriver
						IF fTargetDist > fGotoRange
							TASK_PED_GENERAL_GOTO(tempPed,iped,vPedTargetCoords[iped],bVehOK,fTargetDist,fTargetSpeed,bheli,bplane,bBoat,tempPedTarget,bDriver,tempveh)
						ELSE
							TASK_PED_COMBAT_PED(tempPed,iped,tempPedTarget,bheli,bplane,bBoat,bDriver,tempveh)
						ENDIF
					ELIF bDriverOK
						TASK_PED_COMBAT_PED(tempPed,iped,tempPedTarget,bheli,bplane,bBoat,bDriver,tempveh)
					ELSE
						TASK_PED_GENERAL_GOTO(tempPed,iped,vPedTargetCoords[iped],bVehOK,fTargetDist,fTargetSpeed,bheli,bplane,bBoat,tempPedTarget,bDriver,tempveh)
					ENDIF
				ELSE
					SET_BIT(iPedArrivedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					TASK_PED_COMBAT_PED(tempPed,iped,tempPedTarget,bheli,bplane,bBoat,bDriver,tempveh)
				ENDIF
			ENDIF

		BREAK // End of ciTASK_ATTACK_PLAYER
		
		
		CASE ciTASK_DEFEND_AREA
			IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_GUARD_SPHERE_DEFENSIVE_AREA)
			AND GET_SCRIPT_TASK_STATUS(tempPed,SCRIPT_TASK_GUARD_SPHERE_DEFENSIVE_AREA) != DORMANT_TASK	
			AND NOT (IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed)) OR SHOULD_PED_START_COMBAT(tempPed, iped) OR HAS_PED_BEEN_SPOOKED(iped))
			AND NOT IS_PED_FLEEING(tempPed)
				
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,FALSE)
				INT iTemp
				#IF IS_DEBUG_BUILD					
					VECTOR vSpawnLoc
					vSpawnLoc = GET_PED_SPAWN_LOCATION(iped, MC_ServerBD.iHeistPresistantPropertyID, iTemp)
					PRINTLN("[RCC MISSION] - ciTASK_DEFEND_AREA - GUARD defensive area: g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vPos: ",vSpawnLoc)
					PRINTLN("[RCC MISSION] - ciTASK_DEFEND_AREA - GUARD defensive area: g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fRange: ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fRange)
				#ENDIF
				
				IF bDriver
					IF bBoat
						INIT_BOAT(tempveh,-1,TRUE)
					ELIF bheli OR bplane
						INIT_PLANE(tempveh,-1,TRUE)
					ENDIF
				ENDIF
				
				FLOAT fGuardRange
				fGuardRange = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fRange				
				IF fGuardRange <= 1.0
					fGuardRange = 1.0
				ENDIF
				
				TASK_GUARD_SPHERE_DEFENSIVE_AREA(tempPed,GET_PED_SPAWN_LOCATION(iped, MC_ServerBD.iHeistPresistantPropertyID, iTemp),GET_PED_SPAWN_HEADING(iped, MC_ServerBD.iHeistPresistantPropertyID),fGuardRange,-1,GET_PED_SPAWN_LOCATION(iped, MC_ServerBD.iHeistPresistantPropertyID, iTemp),fGuardRange)
				
				CLEAR_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			ENDIF
		BREAK // End of ciTASK_DEFEND_AREA
		
		CASE ciTASK_DEFEND_AREA_IN_VEHICLE
			INT iTemp
			#IF IS_DEBUG_BUILD				
				VECTOR vSpawnLoc
				vSpawnLoc = GET_PED_SPAWN_LOCATION(iped, MC_ServerBD.iHeistPresistantPropertyID, iTemp)
				PRINTLN("[RCC MISSION] - ciTASK_DEFEND_AREA_IN_VEHICLE - GUARD defensive area: g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vPos: ",vSpawnLoc)
				PRINTLN("[RCC MISSION] - ciTASK_DEFEND_AREA_IN_VEHICLE - GUARD defensive area: g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fRange: ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fRange)
			#ENDIF
						
			INT iPlayer
			iPlayer = GET_NEAREST_PLAYER_TO_ENTITY(tempPed)
			
			IF iPlayer > -1
				PLAYER_INDEX piPlayer 
				piPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPlayer))
			
				IF piPlayer != INVALID_PLAYER_INDEX()
					PED_INDEX pedPlayer 
					pedPlayer = GET_PLAYER_PED(piPlayer)
					
					IF NOT IS_PED_INJURED(pedPlayer)
						VECTOR vPedLoc
						VECTOR vPedPlayerLoc
						
						vPedLoc = GET_ENTITY_COORDS(tempPed)
						vPedPlayerLoc = GET_ENTITY_COORDS(pedPlayer)
						
						IF bDriver
							IF bBoat
								INIT_BOAT(tempveh,-1,TRUE)
							ELIF bheli OR bplane
								INIT_PLANE(tempveh,-1,TRUE)
							ENDIF
						ENDIF
						
						//PRINTLN("[RCC MISSION] - ciTASK_DEFEND_AREA_IN_VEHICLE - fRange (Creator Setting): ", POW(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fRange, 2.0))
						//PRINTLN("[RCC MISSION] - ciTASK_DEFEND_AREA_IN_VEHICLE - vDist2 (Player Ped and Temp Ped: ", VDIST2(GET_PED_SPAWN_LOCATION(iped, MC_ServerBD.iHeistPresistantPropertyID), vPedPlayerLoc))
						IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetSix, ciPED_BSSix_DefensiveAreaMovesWithGotos)
							IF VDIST2(GET_PED_SPAWN_LOCATION(iped, MC_ServerBD.iHeistPresistantPropertyID, iTemp), vPedPlayerLoc) < POW(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fRange, 2.0)
								// Fight
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, FALSE)
								PROCESS_GENERAL_COMBAT_TASK_BODY(tempPed, iPed, bheli, bPlane, bBoat, bDriver, tempVeh)
							ELSE
								FLOAT fDist
								fDist = GET_DISTANCE_BETWEEN_COORDS(vPedLoc, GET_PED_SPAWN_LOCATION(iped, MC_ServerBD.iHeistPresistantPropertyID, iTemp))
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, TRUE)
								
								//PRINTLN("[RCC MISSION] - ciTASK_DEFEND_AREA_IN_VEHICLE - Goto fRange: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fRange)							
								//PRINTLN("[RCC MISSION] - ciTASK_DEFEND_AREA_IN_VEHICLE - Goto fDist: ", fDist)							
								
								// Go back
								IF fDist > g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fRange
									TASK_PED_GENERAL_GOTO(tempPed, iped, GET_PED_SPAWN_LOCATION(iped, MC_ServerBD.iHeistPresistantPropertyID, iTemp), TRUE, fDist, 15.0, bHeli, bplane, bBoat, NULL, bDriver, tempveh, FALSE)
								ELSE
									//PRINTLN("[RCC MISSION] - ciTASK_DEFEND_AREA_IN_VEHICLE - We are idle or patrolling at this moment.")	
								ENDIF
							ENDIF
						ELSE
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, FALSE)
							PROCESS_GENERAL_COMBAT_TASK_BODY(tempPed, iPed, bheli, bPlane, bBoat, bDriver, tempVeh)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK // End of ciTASK_DEFEND_AREA
		
		CASE ciTASK_WANDER
		
			LOAD_COLLISION_ON_PED_IF_POSSIBLE(tempPed,iped)
			
			IF NOT bInVeh
				IF NOT IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_WANDER_STANDARD)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,TRUE)
						
						TASK_WANDER_STANDARD(tempPed)
						CLEAR_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						PRINTLN("[RCC MISSION] ciTASK_WANDER - FOR PED: ",iped)
					ENDIF
				ELSE
					TASK_PED_GENERAL_COMBAT(tempPed,iped)
				ENDIF
			ELSE
				IF bVehOK
					IF bDriver
						IF NOT IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
							IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_VEHICLE_DRIVE_WANDER)
								IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,TRUE)
									
									IF bBoat
										INIT_BOAT(tempveh,-1,TRUE)
									ELIF  bheli OR bplane
										INIT_PLANE(tempveh,-1,TRUE)
									ENDIF
									IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDriveSpeed !=-1
										fdrivespeed = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDriveSpeed)
										PRINTLN("[RCC MISSION] ciTASK_WANDER - wander speed override non-agro: ",fdrivespeed)
									ELSE
										fdrivespeed = 10.0
										PRINTLN("[RCC MISSION] ciTASK_WANDER - wander drive slow called")
									ENDIF
									TASK_VEHICLE_DRIVE_WANDER(tempPed,tempVeh,fdrivespeed,DRIVINGMODE_STOPFORCARS|DF_UseSwitchedOffNodes)
									CLEAR_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
								ELSE
									NETWORK_REQUEST_CONTROL_OF_ENTITY(tempVeh)
								ENDIF
							ELSE
								IF bLocalPlayerPedOk
									IF GET_VEHICLE_PED_IS_ENTERING(LocalPlayerPed) = tempveh
										IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_LOOK_AT_ENTITY)
											TASK_LOOK_AT_ENTITY(tempPed, LocalPlayerPed,30000,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_PITCH_LIMIT | SLF_WIDEST_YAW_LIMIT,SLF_LOOKAT_VERY_HIGH)
											CLEAR_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
											PRINTLN("[RCC MISSION] ciTASK_WANDER - wander look at player called")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_VEHICLE_MISSION, FALSE, MISSION_FLEE, tempVeh)
								IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,TRUE)
									
									IF bBoat
										INIT_BOAT(tempveh,-1,TRUE)
									ELIF  bheli OR bplane
										INIT_PLANE(tempveh,-1,TRUE)
									ENDIF
									IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDriveSpeed !=-1
										fdrivespeed = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDriveSpeed)
										PRINTLN("[RCC MISSION] ciTASK_WANDER - wander speed override agro: ",fdrivespeed)
									ELSE
										fdrivespeed = 30.0
										PRINTLN("[RCC MISSION] ciTASK_WANDER - wander drive fast called")
									ENDIF
									TASK_VEHICLE_MISSION_PED_TARGET(tempPed,tempVeh, LocalPlayerPed, MISSION_FLEE, fdrivespeed, DF_SwerveAroundAllCars|DF_SteerAroundStationaryCars|DF_SteerAroundObjects|DF_SteerAroundPeds|DF_UseShortCutLinks|DF_ChangeLanesAroundObstructions|DF_DriveIntoOncomingTraffic|DF_UseSwitchedOffNodes|DF_AdjustCruiseSpeedBasedOnRoadSpeed|DF_ForceJoinInRoadDirection|DF_StopForCars, 350.0, -1.0, TRUE)
									CLEAR_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
								ELSE
									NETWORK_REQUEST_CONTROL_OF_ENTITY(tempVeh)
								ENDIF
							ELSE
								IF bLocalPlayerPedOk
									IF GET_VEHICLE_PED_IS_ENTERING(LocalPlayerPed) = tempveh
										IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_LOOK_AT_ENTITY)
											TASK_LOOK_AT_ENTITY(tempPed, LocalPlayerPed,30000,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_PITCH_LIMIT | SLF_WIDEST_YAW_LIMIT,SLF_LOOKAT_VERY_HIGH)
											CLEAR_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
											PRINTLN("[RCC MISSION] ciTASK_WANDER - wander look at player called")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELIF bDriverOK
						IF IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
							PRINTLN("[RCC MISSION][SpookAggro] ciTASK_WANDER - Setting into combat from ciTASK_WANDER.")
							TASK_PED_GENERAL_COMBAT(tempPed,iped)
						ENDIF
					ELSE
						TASK_PED_LEAVE_VEHICLE(tempPed,iped)
					ENDIF
				ELSE
					TASK_PED_LEAVE_VEHICLE(tempPed,iped)
				ENDIF
			ENDIF

			
		BREAK // End of ciTASK_WANDER
		
		
		CASE ciTASK_GENERAL_COMBAT
			
			PROCESS_GENERAL_COMBAT_TASK_BODY(tempPed,iped,bheli,bplane,bBoat,bDriver,tempveh)
			
		BREAK // End of ciTASK_GENERAL_COMBAT
		
		CASE ciTASK_FOUND_BODY_RESPONSE
			
			IF NOT SHOULD_PED_START_COMBAT(tempPed,iped)
			
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed,iped,SCRIPT_TASK_PERFORM_SEQUENCE,TRUE)
				
					PLAY_PED_AMBIENT_SPEECH_AND_CLONE_NATIVE(tempPed,"GENERIC_FRIGHTENED_MED","SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
					
					VECTOR vDeadPed, vApproachPos
					PED_INDEX deadPed
					
					deadPed = INT_TO_NATIVE(PED_INDEX,iDeadPedIDSeen[iped])
					
					IF DOES_ENTITY_EXIST(deadPed)
					AND IS_PED_INJURED(deadPed)
					
						vDeadPed = GET_ENTITY_COORDS(deadPed,FALSE)
						
						vPedCoords = GET_ENTITY_COORDS(tempPed)
						
						VECTOR vAim
						vAim = (vDeadPed - vPedCoords)
						
						vAim = NORMALISE_VECTOR(vAim)
						
						vApproachPos = vDeadPed + (-vAim)
						
						PRINTLN("[RCC MISSION] ciTASK_FOUND_BODY_RESPONSE - Ped ",iped," performing ciTASK_FOUND_BODY_RESPONSE sequence - body at ",vDeadPed,", approach pos ",vApproachPos)
						
						CLEAR_SEQUENCE_TASK(temp_sequence)
						
						OPEN_SEQUENCE_TASK(temp_sequence)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,vApproachPos,PEDMOVEBLENDRATIO_RUN,-1,3)
							TASK_START_SCENARIO_IN_PLACE(NULL,"CODE_HUMAN_MEDIC_KNEEL",100,TRUE)
							TASK_SWAP_WEAPON(NULL,TRUE)
							TASK_GUARD_ASSIGNED_DEFENSIVE_AREA(NULL,vDeadPed,0,15,-1)
						CLOSE_SEQUENCE_TASK(temp_sequence)
						
						TASK_PERFORM_SEQUENCE(tempPed,temp_sequence)
						CLEAR_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
						
						CLEAR_SEQUENCE_TASK(temp_sequence)
					ENDIF
					
				ELSE
					IF GET_SEQUENCE_PROGRESS(tempPed) >= 1
					AND NOT IS_BIT_SET(iPedPerceptionBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
						TRIGGER_AGGRO_ON_PED(tempPed,iped)
					ENDIF
				ENDIF
				
			ELSE
				IF NOT IS_BIT_SET(iPedPerceptionBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					TRIGGER_AGGRO_ON_PED(tempPed,iped)
				ENDIF
				TASK_PED_GENERAL_COMBAT(tempPed,iped)
			ENDIF
			
		BREAK
		
		CASE ciTASK_HUNT_FOR_PLAYER
			
			RUN_AGGRO_PED_HUNTING_BODY(tempPed,iped)
			
		BREAK
		
		CASE ciTASK_THROW_PROJECTILES
			TASK_THROW_PROJECTILES(tempPed, iped)
		BREAK
		
		CASE ciTASK_ABORT_GOTO_AND_FLEE
			
			IF bInVeh
			AND SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_LEAVE_ANY_VEHICLE)
				SET_PED_FLEE_ATTRIBUTES(tempPed, FA_DISABLE_COWER, TRUE)
				SET_PED_FLEE_ATTRIBUTES(tempPed, FA_USE_VEHICLE, FALSE)
				SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_ALWAYS_FLEE, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_ALWAYS_FIGHT, FALSE)
				SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_DISABLE_FLEE_FROM_COMBAT, FALSE)
				SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE, FALSE)
				SET_PED_ALERTNESS(tempPed, AS_MUST_GO_TO_COMBAT)
				TASK_LEAVE_ANY_VEHICLE(tempPed,0)
				CLEAR_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				PRINTLN("[RCC MISSION] ciTASK_ABORT_GOTO_AND_FLEE - Ped ",iped," set to CA_ALWAYS_FLEE due to citask_abort_goto_and_flee")
			ENDIF
			
		BREAK
		
		CASE ciTASK_FLEE
			
			LOAD_COLLISION_ON_PED_IF_POSSIBLE(tempPed,iped)
			
			IF NOT bInVeh
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,FALSE)
				// SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_ALWAYS_FLEE, TRUE) Might need this.
				TASK_PED_GENERAL_COMBAT(tempPed,iped) // flee combat flag is set so they will flee
			ELSE
				IF bVehOK
					IF bDriver											
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_DontFleeInvehicle)
							TASK_PED_GENERAL_COMBAT(tempPed,iped)
						ELSE
							IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_VEHICLE_MISSION, FALSE, MISSION_FLEE, tempVeh)
								IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,TRUE)
									IF bBoat
										INIT_BOAT(tempveh,-1,TRUE)
									ELIF bheli OR bplane
										INIT_PLANE(tempveh,-1,TRUE)
									ENDIF
									IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDriveSpeed !=-1
										fdrivespeed = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDriveSpeed)
									ELSE
										fdrivespeed = 40.0
									ENDIF
									PRINTLN("[RCC MISSION] ciTASK_FLEE - flee drive called") 
									//TASK_VEHICLE_DRIVE_WANDER(tempPed, tempVeh,fdrivespeed,DF_SwerveAroundAllCars|DF_SteerAroundStationaryCars|DF_SteerAroundObjects|DF_SteerAroundPeds|DF_UseShortCutLinks|DF_ChangeLanesAroundObstructions|DF_DriveIntoOncomingTraffic|DF_UseSwitchedOffNodes|DF_AdjustCruiseSpeedBasedOnRoadSpeed|DF_ForceJoinInRoadDirection)
									TASK_VEHICLE_MISSION_PED_TARGET(tempPed,tempVeh, LocalPlayerPed, MISSION_FLEE, fdrivespeed, DF_SwerveAroundAllCars|DF_SteerAroundStationaryCars|DF_SteerAroundObjects|DF_SteerAroundPeds|DF_UseShortCutLinks|DF_ChangeLanesAroundObstructions|DF_DriveIntoOncomingTraffic|DF_UseSwitchedOffNodes|DF_AdjustCruiseSpeedBasedOnRoadSpeed|DF_ForceJoinInRoadDirection|DF_StopForCars, 350.0, -1.0, TRUE)
									CLEAR_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
								ELSE
									NETWORK_REQUEST_CONTROL_OF_ENTITY(tempVeh)
								ENDIF
							ELSE
								IF bLocalPlayerPedOk
									IF GET_VEHICLE_PED_IS_ENTERING(LocalPlayerPed) = tempveh
										IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_LOOK_AT_ENTITY)
											TASK_LOOK_AT_ENTITY(tempPed, LocalPlayerPed,30000,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_PITCH_LIMIT | SLF_WIDEST_YAW_LIMIT,SLF_LOOKAT_VERY_HIGH)
											CLEAR_BIT(iBSTaskUsingCoordTarget[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
											PRINTLN("[RCC MISSION] ciTASK_FLEE - Flee look at player called")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELIF bDriverOK
						IF IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
							TASK_PED_GENERAL_COMBAT(tempPed,iped)
						ENDIF
					ELSE
						PRINTLN("[LM][FleeCheck] - ciTASK_FLEE - Ped: ", iped, " is fleeing, > GENERAL COMBAT. (1)")
						//set ped to flee via threat response using flee flags
						SET_PED_FLEE_ATTRIBUTES(tempPed, FA_USE_VEHICLE, FALSE)
						SET_PED_FLEE_ATTRIBUTES(tempPed, FA_DISABLE_COWER, TRUE)
						SET_PED_FLEE_ATTRIBUTES(tempPed, FA_FORCE_EXIT_VEHICLE, TRUE)
						SET_PED_FLEE_ATTRIBUTES(tempPed, FA_DISABLE_EXIT_VEHICLE, FALSE)
						SET_PED_FLEE_ATTRIBUTES(tempPed, FA_COWER_INSTEAD_OF_FLEE, FALSE)
						SET_PED_FLEE_ATTRIBUTES(tempPed, FA_DISABLE_HESITATE_IN_VEHICLE, TRUE)
						
						//set ped to flee via threat response using combat flags
						SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_ALWAYS_FLEE, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE, FALSE)
						SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_LEAVE_VEHICLES, TRUE)
						
						TASK_PED_GENERAL_COMBAT(tempPed,iped)
						
						//Lukasz: Commenting leave vehicle task and swapping to general combat task with flee settings
						//for B*2212920, as the combination of exit vehicle and then flee was causing visible delay between tasks
						//so now we task the ped directly to flee using general combat with flee flags on.
						//Note: If the vehicle does not have "get_out_to_run" ped could end up cowering and never leave vehicle.
						//If this is seen for certain vehicles, then new anims could be made, AI fix could be added or we could 
						//script the ped to leave vehicle based on a creator flag
						//TASK_PED_LEAVE_VEHICLE(tempPed,iped)
					ENDIF
				ELSE
					PRINTLN("[LM][FleeCheck] - ciTASK_FLEE - Ped: ", iped, " is fleeing, > GENERAL COMBAT. (2)")
					//set ped to flee via threat response using flee flags
					SET_PED_FLEE_ATTRIBUTES(tempPed, FA_USE_VEHICLE, FALSE)
					SET_PED_FLEE_ATTRIBUTES(tempPed, FA_DISABLE_COWER, TRUE)
					SET_PED_FLEE_ATTRIBUTES(tempPed, FA_FORCE_EXIT_VEHICLE, TRUE)
					SET_PED_FLEE_ATTRIBUTES(tempPed, FA_DISABLE_EXIT_VEHICLE, FALSE)
					SET_PED_FLEE_ATTRIBUTES(tempPed, FA_COWER_INSTEAD_OF_FLEE, FALSE)
					SET_PED_FLEE_ATTRIBUTES(tempPed, FA_DISABLE_HESITATE_IN_VEHICLE, TRUE)
					
					//set ped to flee via threat response using combat flags
					SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_ALWAYS_FLEE, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE, FALSE)
					SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_LEAVE_VEHICLES, TRUE)
					
					TASK_PED_GENERAL_COMBAT(tempPed,iped)
					//TASK_PED_LEAVE_VEHICLE(tempPed,iped)
				ENDIF
			
			ENDIF // End of bInVeh
		
		BREAK // End of ciTASK_FLEE
		
	ENDSWITCH
	
	IF IS_BIT_SET(iPedPreventMigrationBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
	AND NOT IS_BIT_SET(iPedPreventMigrationBitsetNext[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
		PRINTLN("[LM][PROCESS_PED_BODY] - ciTASK_FLEE - PED_CAN_MIGRATE set to TRUE on ped ", iped, " id: ", NATIVE_TO_INT(tempPed))
		SET_NETWORK_ID_CAN_MIGRATE(PED_TO_NET(tempPed), TRUE)
		CLEAR_BIT(iPedPreventMigrationBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
	ENDIF
	CLEAR_BIT(iPedPreventMigrationBitsetNext[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_DisableReact)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, TRUE)
	ENDIF
	
	CLEAR_RETASK_DIRTY_FLAG(tempPed, iPed)
	
ENDPROC
