
USING "FM_Mission_Controller_USING.sch"
USING "FM_Mission_Controller_Utility.sch"
USING "FM_Mission_Controller_Entities.sch"
USING "FM_Mission_Controller_ObjectiveCompletion.sch"
USING "FM_Mission_Controller_SpecialCase.sch"
USING "FM_Mission_Controller_Rules.sch"
USING "FM_Mission_Controller_Doors.sch"
USING "FM_Mission_Controller_Cutscene.sch"

#IF IS_DEBUG_BUILD

PROC MAINTAIN_PLAYER_ID_CHANGE_CHECK()
	IF LocalPlayer != PLAYER_ID()
		PRINTLN("[RCC MISSION] ********************************************************************")
		PRINTLN("[RCC MISSION] ********************************************************************")
		PRINTLN("[RCC MISSION] MAINTAIN_PLAYER_ID_CHANGE_CHECK - LocalPlayer != PLAYER_ID()")
		PRINTLN("[RCC MISSION] ********************************************************************")
		PRINTLN("[RCC MISSION] ********************************************************************")
		PRINTLN("[RCC MISSION] LocalPlayer: ", NATIVE_TO_INT(LocalPlayer))
		PRINTLN("[RCC MISSION] PLAYER_ID(): ", NATIVE_TO_INT(PLAYER_ID()))
		SCRIPT_ASSERT("This is very BAD - [RCC MISSION] - LocalPlayer != PLAYER_ID()")
	ENDIF
	
	IF LocalPlayerPed != PLAYER_PED_ID()
		PRINTLN("[RCC MISSION] ********************************************************************")
		PRINTLN("[RCC MISSION] ********************************************************************")
		PRINTLN("[RCC MISSION] MAINTAIN_PLAYER_ID_CHANGE_CHECK - LocalPlayerPed != PLAYER_PED_ID()")
		PRINTLN("[RCC MISSION] ********************************************************************")
		PRINTLN("[RCC MISSION] ********************************************************************")
		PRINTLN("[RCC MISSION] LocalPlayerPed : ", NATIVE_TO_INT(LocalPlayerPed))
		PRINTLN("[RCC MISSION] PLAYER_PED_ID(): ", NATIVE_TO_INT(PLAYER_PED_ID()))
		SCRIPT_ASSERT("This is very BAD - [RCC MISSION] - LocalPlayerPed != PLAYER_PED_ID()")
	ENDIF
	
	IF iLocalPart != PARTICIPANT_ID_TO_INT()
		PRINTLN("[RCC MISSION] ********************************************************************")
		PRINTLN("[RCC MISSION] ********************************************************************")
		PRINTLN("[RCC MISSION] MAINTAIN_PLAYER_ID_CHANGE_CHECK - iLocalPart != PARTICIPANT_ID_TO_INT()")
		PRINTLN("[RCC MISSION] ********************************************************************")
		PRINTLN("[RCC MISSION] ********************************************************************")
		PRINTLN("[RCC MISSION] iLocalPart             : ", iLocalPart)
		PRINTLN("[RCC MISSION] PARTICIPANT_ID_TO_INT(): ", PARTICIPANT_ID_TO_INT())
		SCRIPT_ASSERT("This is very BAD - [RCC MISSION] - iLocalPart != PARTICIPANT_ID_TO_INT()")
	ENDIF
ENDPROC

/// PURPOSE: Get the text name for the state
FUNC TEXT_LABEL_15 GET_PED_STATE_NAME(INT iPedState)
	TEXT_LABEL_15 l_sDebug
	
	SWITCH iPedState

		CASE ciTASK_CHOOSE_NEW_TASK
			l_sDebug = "Idle"
		BREAK
		CASE ciTASK_GOTO_COORDS
			l_sDebug = "GoCrd"
		BREAK
		CASE ciTASK_DEFEND_AREA
			l_sDebug = "DefAr"
		BREAK
		CASE ciTASK_DEFEND_AREA_IN_VEHICLE
			l_sDebug = "DefArVeh"
		BREAK
		CASE ciTASK_WANDER
			l_sDebug = "Wndr"
		BREAK
		CASE ciTASK_GOTO_ENTITY
			l_sDebug = "GoEnt"
		BREAK

		CASE ciTASK_DEFEND_ENTITY
			l_sDebug = "DefEnt"
		BREAK
		CASE ciTASK_ATTACK_ENTITY
			l_sDebug = "AttEnt"
		BREAK
		CASE ciTASK_AIM_AT_ENTITY
			l_sDebug = "AimEnt"
		BREAK
		CASE ciTASK_TAKE_COVER
			l_sDebug = "Cvr"
		BREAK
		CASE ciTASK_GOTO_PLAYER
			l_sDebug = "GoPlr"
		BREAK

		CASE ciTASK_DEFEND_PLAYER
			l_sDebug = "DefPlr"
		BREAK
		CASE ciTASK_ATTACK_PLAYER
			l_sDebug = "AttPlr"
		BREAK
		CASE ciTASK_GENERAL_COMBAT
			l_sDebug = "Cmbt"
		BREAK
		CASE ciTASK_THROW_PROJECTILES
			l_sDebug = "Throw"
		BREAK
		CASE ciTASK_FLEE
			l_sDebug = "Flee"
		BREAK
		CASE ciTASK_FOUND_BODY_RESPONSE
			l_sDebug = "Body"
		BREAK
		CASE ciTASK_HUNT_FOR_PLAYER
			l_sDebug = "Hunt"
		BREAK
		CASE ciTASK_ABORT_GOTO_AND_FLEE
			l_sDebug = "Abrt"
		BREAK
		DEFAULT
			l_sDebug = "Undef"
		BREAK
		
	ENDSWITCH
	
	RETURN l_sDebug
ENDFUNC

PROC HACKING_J_SKIP()
	ENABLE_INTERACTION_MENU()
	CLEANUP_MENU_ASSETS()
	FORCE_QUIT_PASS_HACKING_MINIGAME(sHackingData)
	
	INT i
	IF MC_serverBD.iNumObjCreated > 0	
	AND MC_serverBD.iNumObjCreated <= FMMC_MAX_NUM_OBJECTS
		FOR i = 0 TO (MC_serverBD.iNumObjCreated -1)
			MC_playerBD[iPartToUse].iObjHacked = i
		ENDFOR
	ENDIF
	MC_playerBD[iPartToUse].iObjHacking = -1
	IF bLocalPlayerPedOk
		SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed,TRUE,FALSE)
		NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
	ENDIF
	CLEAR_PED_TASKS(LocalPlayerPed)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
	TASK_CLEAR_LOOK_AT(LocalPlayerPed)
//		// What is happening here
//		PLAY_SOUND_FROM_COORD( -1, 
//		                        GET_DOOR_SOUND_FROM_MODEL_NAME(GET_FMMC_DOOR_MODEL(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iLinkedDoor[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]])),
//		                          GET_FMMC_DOOR_COORDS(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iLinkedDoor[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]]),
//		                          "HACKING_DOOR_UNLOCK_SOUNDS",
//		                          TRUE,
//		                          30,
//		                          DEFAULT)
	RELEASE_SCRIPT_AUDIO_BANK()
	PRINTLN("RELEASE_SCRIPT_AUDIO_BANK - Called from fm_mission_controller_debug.sch 1")
	iHackLimitTimer = 0
	iHackProgress = 0
	PRINTLN("[RCC MISSION] [AW_MISSION] - [RCC MISSION] HACKING_J_SKIP")
ENDPROC

PROC GO_DOWN_A_LINE( VECTOR& vDrawPos )
	FLOAT iYIncrement = 0.015
	VECTOR vIncrement = << 0, iYIncrement, 0 >>
	
	vDrawPos += vIncrement
ENDPROC

ENUM eColour
	eColour_Red,
	eColour_Orange,
	eColour_White,
	eColour_Green,
		
	eColour_MAX
ENDENUM

FUNC VECTOR GET_TEXT_COLOUR( eColour eTextColour )
	VECTOR vColour = << 0,0,0 >>
	
	SWITCH eTextColour
		CASE eColour_Red
			vColour = << 255, 0, 0 >>
		BREAK
		
		CASE eColour_Orange
			vColour = << 255, 140, 50 >>
		BREAK
		
		CASE eColour_Green
			vColour = << 50, 255, 100 >>
		BREAK
		
		CASE eColour_White
			vColour = << 255, 255, 255 >>
		BREAK
		
	ENDSWITCH
	
	RETURN vColour
ENDFUNC

PROC DRAW_COLOURED_DEBUG_TEXT( STRING strToPrint, VECTOR vDrawPos, VECTOR vColour )
	IF NOT IS_STRING_NULL_OR_EMPTY( strToPrint )
		DRAW_DEBUG_TEXT_2D( strToPrint, vDrawPos, FLOOR( vColour.x ), FLOOR( vColour.y ), FLOOR( vColour.z ), 255 )
	ENDIF
ENDPROC

PROC PRINT_DEBUG_TRUEFALSE_LINE( BOOL bPredicate, STRING strTrue, STRING strFalse, eColour eTextColour, VECTOR& vDrawPos )

	TEXT_LABEL_63 strToPrint = ""
	
	IF bPredicate
		strToPrint = strTrue
	ELSE
		strToPrint = strFalse
	ENDIF
	
	GO_DOWN_A_LINE( vDrawPos )

	DRAW_COLOURED_DEBUG_TEXT( strToPrint, vDrawPos, GET_TEXT_COLOUR( eTextColour ) )
ENDPROC

PROC PRINT_DEBUG_VECTOR_WITH_TEXT( STRING strLine, VECTOR vVectorToPrint, eColour eTextColour, VECTOR& vDrawPos )

	TEXT_LABEL_63 strToPrint = strLine
	
	strToPrint += " <<"
	strToPrint += FLOOR( vVectorToPrint.x )
	strToPrint += ","
	strToPrint += FLOOR( vVectorToPrint.y )
	strToPrint += ","
	strToPrint += FLOOR( vVectorToPrint.z )
	strToPrint += ">>"
	
	GO_DOWN_A_LINE( vDrawPos )
	
	DRAW_COLOURED_DEBUG_TEXT( strToPrint, vDrawPos, GET_TEXT_COLOUR( eTextColour ) )
	
ENDPROC

PROC PRINT_DEBUG_LINE( STRING strToPrint, VECTOR& vDrawPos, eColour eTextColour )
	
	GO_DOWN_A_LINE( vDrawPos )
	
	DRAW_COLOURED_DEBUG_TEXT( strToPrint, vDrawPos, GET_TEXT_COLOUR( eTextColour ) )
	
ENDPROC

PROC CLEANUP_MENU_SKIP()

	INT iRule
	MissionStageMenuTextStruct sEmptyTeamSkipStruct
	FOR iRule = 0 TO (FMMC_MAX_RULES-1)
		sTeamSkipStruct[iRule]= sEmptyTeamSkipStruct
	ENDFOR
	iSkipTeam = -1
	bDrawMenu = FALSE

ENDPROC

STRUCT SCRIPT_EVENT_DATA_DEBUG_OBJECTIVE_SKIP_FOR_TEAM
	STRUCT_EVENT_COMMON_DETAILS Details 
	INT iTeam
	INT iSkipObjective
	INT iNewTeam
ENDSTRUCT

PROC BROADCAST_DEBUG_OBJECTIVE_SKIP_FOR_TEAM(INT iTeam, INT iObjective, INT iNewTeam = -1)
	
	PRINTLN("BROADCAST_DEBUG_OBJECTIVE_SKIP_FOR_TEAM - called... iteam: ",iTeam, " skip to objective: ", iObjective)
	
	SCRIPT_EVENT_DATA_DEBUG_OBJECTIVE_SKIP_FOR_TEAM Event
	
	Event.Details.Type = SCRIPT_EVENT_DEBUG_OBJECTIVE_SKIP_FOR_TEAM
	Event.Details.FromPlayerIndex = PLAYER_ID()
	Event.iTeam = iTeam
	Event.iSkipObjective = iObjective
	Event.iNewTeam = iNewTeam
	
	
	INT iPlayerFlags = ALL_PLAYERS(TRUE) // any player could be the host 
	IF NOT (iPlayerFlags = 0)
		
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iPlayerFlags)	
	ELSE
		NET_PRINT("BROADCAST_DEBUG_OBJECTIVE_SKIP_FOR_TEAM - playerflags = 0 so not broadcasting") NET_NL()		
	ENDIF	

ENDPROC

PROC STOP_MISSION_FAILING_AS_TEAM_SWAPS(INT iOldTeam, INT iNewTeam)
	
	INT iPlayers[FMMC_MAX_TEAMS] // iNumberOfPlayingPlayers is staggered and unreliable
	
	INT iPart
	
	FOR iPart = 0 TO (MAX_NUM_MC_PLAYERS - 1)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
			IF ((NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_ANY_SPECTATOR))
				OR IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_HEIST_SPECTATOR))
			AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_PLAYER_FAIL)
				IF IS_BIT_SET(MC_playerBD[iPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
					IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_FINISHED)
						iPlayers[MC_playerBD[iPart].iteam]++
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	IF iPlayers[iOldTeam] <= 1 // If the player leaving was the only person on this team
		CLEAR_BIT(iLocalBoolCheck, TEMP_TEAM_0_ACTIVE + iOldTeam)
		CLEAR_BIT(MC_serverBD.iServerBitSet1, SBBOOL1_TEAM_0_WAS_ACTIVE + iOldTeam)
		CLEAR_BIT(MC_serverBD.iServerBitSet, SBBOOL_TEAM_0_ACTIVE + iOldTeam)
	ENDIF
	
	IF iPlayers[iNewTeam] = 0
		SET_BIT(iLocalBoolCheck, TEMP_TEAM_0_ACTIVE + iNewTeam)
		SET_BIT(MC_serverBD.iServerBitSet1, SBBOOL1_TEAM_0_WAS_ACTIVE + iNewTeam)
		SET_BIT(MC_serverBD.iServerBitSet, SBBOOL_TEAM_0_ACTIVE + iNewTeam)
		CLEAR_BIT(MC_serverBD.iServerBitSet, SSBOOL_TEAM0_FINISHED + iNewTeam)
	ENDIF
	
ENDPROC

PROC MAINTAIN_SERVER_J_SKIP(INT iparticipant)
	
	INT i
	PARTICIPANT_INDEX temppart
	PLAYER_INDEX tempPlayer
	PED_INDEX tempPed 

	IF IS_BIT_SET(MC_playerBD[iparticipant].iClientBitSet, ENUM_TO_INT(PBBOOL_PRESSED_J))
		IF NOT IS_BIT_SET(MC_serverBD.ijSkipBitset,iparticipant)
			INT iclientstage = GET_MC_CLIENT_MISSION_STAGE(iparticipant)
			temppart = INT_TO_PARTICIPANTINDEX(iparticipant)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(temppart)
				tempPlayer = NETWORK_GET_PLAYER_INDEX(temppart)
			ENDIF
			
			IF IS_NET_PLAYER_OK(tempPlayer)
				tempPed = GET_PLAYER_PED(tempPlayer)
				SWITCH iclientstage
				
					CASE CLIENT_MISSION_STAGE_CONTROL_AREA
						FOR i = 0 TO (FMMC_MAX_GO_TO_LOCATIONS-1)
							IF MC_serverBD_4.iGotoLocationDataPriority[i][MC_playerBD[iparticipant].iteam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iparticipant].iteam]
								IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(tempPed, GET_LOCATION_VECTOR(i)) < 10
									IF HAS_NET_TIMER_STARTED(MC_serverBD.tdAreaTimer[i][MC_playerBD[iparticipant].iteam])
										MC_serverBD.tdAreaTimer[i][MC_playerBD[iparticipant].iteam].Timer =GET_TIME_OFFSET(MC_serverBD.tdAreaTimer[i][MC_playerBD[iparticipant].iteam].Timer,-1*(2*MC_serverBD.iGotoLocationDataTakeoverTime[MC_playerBD[iparticipant].iteam][MC_serverBD_4.iGotoLocationDataPriority[i][MC_playerBD[iparticipant].iteam]]))
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
					BREAK

					CASE CLIENT_MISSION_STAGE_CONTROL_PED
					
						FOR i = 0 TO (FMMC_MAX_PEDS-1)
							IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[i])
								IF MC_serverBD_4.iPedPriority[i][MC_playerBD[iparticipant].iteam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iparticipant].iteam]
									IF GET_DISTANCE_BETWEEN_ENTITIES(tempPed, NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[i])) < 10
										IF HAS_NET_TIMER_STARTED(MC_serverBD_1.tdControlPedTimer[i])
											MC_serverBD_1.tdControlPedTimer[i].Timer =GET_TIME_OFFSET(MC_serverBD_1.tdControlPedTimer[i].Timer,(-1*MC_serverBD.iPedTakeoverTime[MC_playerBD[iparticipant].iteam][MC_serverBD_4.iPedPriority[i][MC_playerBD[iparticipant].iteam]]))
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
						
					BREAK

					
					CASE CLIENT_MISSION_STAGE_CONTROL_VEH
					
						FOR i = 0 TO (FMMC_MAX_VEHICLES-1)
							IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]))
								IF MC_serverBD_4.iVehPriority[i][MC_playerBD[iparticipant].iteam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iparticipant].iteam]
									IF GET_DISTANCE_BETWEEN_ENTITIES(tempPed, NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i])) < 10
										IF HAS_NET_TIMER_STARTED(MC_serverBD_1.tdControlVehTimer[i])
											MC_serverBD_1.tdControlVehTimer[i].Timer =GET_TIME_OFFSET(MC_serverBD_1.tdControlVehTimer[i].Timer,(-1*MC_serverBD.iVehTakeoverTime[MC_playerBD[iparticipant].iteam][MC_serverBD_4.iVehPriority[i][MC_playerBD[iparticipant].iteam]]))
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
						
					BREAK

					CASE CLIENT_MISSION_STAGE_CONTROL_OBJ
					
						FOR i = 0 TO (FMMC_MAX_NUM_OBJECTS-1)
							IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[i])
								IF NOT IS_ENTITY_DEAD(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[i]))
									IF MC_serverBD_4.iObjPriority[i][MC_playerBD[iparticipant].iteam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iparticipant].iteam]
										IF GET_DISTANCE_BETWEEN_ENTITIES(tempPed, NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[i])) < 10
											IF HAS_NET_TIMER_STARTED(MC_serverBD_1.tdControlObjTimer[i])
												MC_serverBD_1.tdControlObjTimer[i].Timer =GET_TIME_OFFSET(MC_serverBD_1.tdControlObjTimer[i].Timer,(-1*(MC_serverBD.iObjTakeoverTime[MC_playerBD[iparticipant].iteam][MC_serverBD_4.iObjPriority[i][MC_playerBD[iparticipant].iteam]] - MC_serverBD.iObjectCaptureOffset[MC_playerBD[iparticipant].iteam])))
											ENDIF
										ENDIF
									ENDIF 
								ENDIF
							ENDIF
						ENDFOR			

					BREAK
					
					CASE CLIENT_MISSION_STAGE_PROTECT_VEH
					CASE CLIENT_MISSION_STAGE_PROTECT_PED
					CASE CLIENT_MISSION_STAGE_PROTECT_OBJ
					
						IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iparticipant].iteam] < FMMC_MAX_RULES
							IF HAS_NET_TIMER_STARTED(MC_serverBD_3.tdObjectiveLimitTimer[MC_playerBD[iparticipant].iteam])
								MC_serverBD_3.tdObjectiveLimitTimer[MC_playerBD[iparticipant].iteam].Timer =GET_TIME_OFFSET(MC_serverBD_3.tdObjectiveLimitTimer[MC_playerBD[iparticipant].iteam].Timer,(-1*GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iparticipant].iteam].iObjectiveTimeLimitRule[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iparticipant].iteam]])))
							ENDIF
						ENDIF
							
					BREAK
					

				ENDSWITCH	
			ENDIF
			SET_BIT(MC_serverBD.ijSkipBitset,iparticipant)
		ENDIF
	ELSE
		CLEAR_BIT(MC_serverBD.ijSkipBitset,iparticipant)
	ENDIF

ENDPROC

#IF IS_DEBUG_BUILD
FUNC STRING GET_PED_SYNC_SCENE_STATE_NAME_PRINT(PED_SYNC_SCENE_STATE ePedSyncSceneState)
	SWITCH ePedSyncSceneState
		CASE	PED_SYNC_SCENE_STATE_INIT					RETURN "STATE_INIT"
		CASE	PED_SYNC_SCENE_STATE_PROCESS_START			RETURN "STATE_PROCESS_START"
		CASE	PED_SYNC_SCENE_STATE_PROCESS_BREAKOUT		RETURN "STATE_PROCESS_BREAKOUT"
		CASE	PED_SYNC_SCENE_STATE_PROCESS_END			RETURN "STATE_PROCESS_END"
		CASE	PED_SYNC_SCENE_STATE_CLEANUP				RETURN "STATE_CLEANUP"
	ENDSWITCH
	
	RETURN "Invalid"
ENDFUNC
PROC DEBUG_SHOW_SYNC_SCENE_STATES()
	PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("====================== SyncScene Debug ======================", "", vBlankDbg)
	INT iSyncedPeds = 0
	INT iPeds = 0
	INT iAmountOfSyncedPeds
	FOR iPeds = 0 TO FMMC_MAX_PEDS-1		
		FOR iSyncedPeds = 0 TO FMMC_MAX_PED_SCENE_SYNCED_PEDS-1
			IF sMissionPedsLocalVars[iPeds].iSyncedPeds[iSyncedPeds] > -1
				iAmountOfSyncedPeds++
			ENDIF
		ENDFOR
		
		PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("------- iPed: ", "", vBlankDbg, iPeds)
		PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("iState: ", GET_PED_SYNC_SCENE_STATE_NAME_PRINT(sMissionPedsLocalVars[iPeds].ePedSyncSceneState), vBlankDbg)
		PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("iSyncID: ", "", vBlankDbg, sMissionPedsLocalVars[iPeds].iSyncScene)
		PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("iAmountOfSyncedPeds: ", "", vBlankDbg, iAmountOfSyncedPeds)		
		PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("iAnim: ", "", vBlankDbg, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPeds].iIdleAnim)
		PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("iSpecAnim: ", "", vBlankDbg, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPeds].iSpecialAnim)
		
	ENDFOR	
ENDPROC
#ENDIF

PROC PROCESS_DEBUG()
	
	INT i		
	
	IF bUsedDebugMenu
		SET_TEXT_SCALE(0.6800, 0.6800)
		SET_TEXT_COLOUR(255, 255, 255, 255)
		SET_TEXT_CENTRE(TRUE)
		SET_TEXT_DROPSHADOW(0, 0, 0, 0, 255)
		SET_TEXT_EDGE(0, 0, 0, 0, 255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.5, 0.0450, "STRING", "USED SKIP MENU!!")
	ENDIF

	IF iSpectatorTarget = -1
	
		IF IS_BIT_SET(MC_serverBD.ijSkipBitset,iPartToUse)
			CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet, ENUM_TO_INT(PBBOOL_PRESSED_J))
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_K) 
			SWITCH GET_MC_CLIENT_MISSION_STAGE(iPartToUse)
				CASE CLIENT_MISSION_STAGE_KILL_PED
					FOR i = 0 TO (FMMC_MAX_PEDS-1)
						IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[i])
							IF MC_serverBD_4.iPedRule[i][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL
								SET_ENTITY_HEALTH(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[i]),0)
							ENDIF
						ENDIF
					ENDFOR
				BREAK
				
				CASE CLIENT_MISSION_STAGE_KILL_VEH
					FOR i = 0 TO (FMMC_MAX_VEHICLES-1)
						IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
						AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]))
							IF MC_serverBD_4.ivehRule[i][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL
								PRINTLN("[RCC MISSION] PROCESS_DEBUG - Calling NETWORK_EXPLODE_VEHICLE on veh ",i," as K key was pressed")
								NETWORK_EXPLODE_VEHICLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]))
							ENDIF
						ENDIF
					ENDFOR
				BREAK
				
				CASE CLIENT_MISSION_STAGE_KILL_OBJ
					FOR i = 0 TO (FMMC_MAX_NUM_OBJECTS-1)
						IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[i])
						AND NOT IS_ENTITY_DEAD(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[i]))
							IF MC_serverBD_4.iObjRule[i][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL
								SET_ENTITY_HEALTH(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[i]),0)
							ENDIF
						ENDIF
					ENDFOR
				BREAK
				
			ENDSWITCH
		ENDIF
		
		IF iSkipTeam = -1
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_1) 
				PRINTLN("[RCC MISSION] PROCESS_DEBUG - Calling populate MissionStageMenuTextStruct for team 0 ")
				iSkipTeam = 0
			ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_2) 
				PRINTLN("[RCC MISSION] PROCESS_DEBUG - Calling populate MissionStageMenuTextStruct for team 1 ")
				iSkipTeam = 1
			ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_3) 
				PRINTLN("[RCC MISSION] PROCESS_DEBUG - Calling populate MissionStageMenuTextStruct for team 2 ")
				iSkipTeam = 2
			ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_4) 
				PRINTLN("[RCC MISSION] PROCESS_DEBUG - Calling populate MissionStageMenuTextStruct for team 3 ")
				iSkipTeam = 3
			ENDIF
			
			IF iSkipTeam > -1
				bDrawMenu = TRUE
				
				INT iLoop, iRule
				
				FOR iLoop = 0 TO (FMMC_MAX_RULES + 1)
					IF iLoop = 0
						sTeamSkipStruct[iLoop].sTxtLabel = "Join This Team"
						sTeamSkipStruct[iLoop].bSelectable = TRUE
					ELIF iLoop = 1
						sTeamSkipStruct[iLoop].sTxtLabel = "---Objectives---"
						sTeamSkipStruct[iLoop].bSelectable = FALSE
					ELIF iLoop >= 2
						
						iRule = iLoop - 2 // Goes from 0 to FMMC_MAX_RULES - 1
						
						TEXT_LABEL_63 tl63CustomGodText = g_FMMC_STRUCT.sFMMCEndConditions[iSkipTeam].tl63Objective[iRule]
						
						IF iRule < MC_serverBD_4.iCurrentHighestPriority[iSkipTeam]
							sTeamSkipStruct[iLoop].sTxtLabel = "("
							sTeamSkipStruct[iLoop].sTxtLabel += tl63CustomGodText
							sTeamSkipStruct[iLoop].sTxtLabel += ")"
							sTeamSkipStruct[iLoop].bSelectable = TRUE
							PRINTLN("[RCC MISSION] PROCESS_DEBUG - objective already complete: ",iRule, " for team: ",iSkipTeam)
						ELIF iRule = MC_serverBD_4.iCurrentHighestPriority[iSkipTeam]
							sTeamSkipStruct[iLoop].sTxtLabel = "**"
							sTeamSkipStruct[iLoop].sTxtLabel += tl63CustomGodText
							sTeamSkipStruct[iLoop].sTxtLabel += "**"
							sTeamSkipStruct[iLoop].bSelectable = TRUE
							PRINTLN("[RCC MISSION] PROCESS_DEBUG - current objective: ",iRule, " for team: ",iSkipTeam)
						ELIF iRule <= MC_serverBD.iMaxObjectives[iSkipTeam] 
							sTeamSkipStruct[iLoop].sTxtLabel = tl63CustomGodText
							sTeamSkipStruct[iLoop].bSelectable = TRUE
							PRINTLN("[RCC MISSION] PROCESS_DEBUG - objective still valid: ",iRule, " for team: ",iSkipTeam)
						ELSE
							sTeamSkipStruct[iLoop].bSelectable = FALSE
							PRINTLN("[RCC MISSION] PROCESS_DEBUG - objective does not exist: ",iRule, " for team: ",iSkipTeam)
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
		ELSE
			STRING sMenuTitle
			IF iSkipTeam = 0
				sMenuTitle = "TEAM 1"
			ELIF iSkipTeam = 1
				sMenuTitle = "TEAM 2"
			ELIF iSkipTeam = 2
				sMenuTitle = "TEAM 3"
			ELIF iSkipTeam = 3
				sMenuTitle = "TEAM 4"
			ENDIF
			
			INT iStageToSkipTo
			
			IF LAUNCH_MISSION_STAGE_MENU(sTeamSkipStruct, iStageToSkipTo, 0, FALSE, sMenuTitle, FALSE)
				IF iStageToSkipTo = 0 // Swap to team has been selected
					IF iSkipTeam != MC_playerBD[iPartToUse].iteam
						PRINTLN("[RCC MISSION] PROCESS_DEBUG - LAUNCH_MISSION_STAGE_MENU called for a change from my team ",MC_playerBD[iPartToUse].iteam," to new team: ", iSkipTeam)
						
						IF bIsLocalPlayerHost
							STOP_MISSION_FAILING_AS_TEAM_SWAPS(MC_playerBD[iPartToUse].iteam, iSkipTeam)
						ELSE
							BROADCAST_DEBUG_OBJECTIVE_SKIP_FOR_TEAM(MC_playerBD[iPartToUse].iteam, -1, iSkipTeam)
						ENDIF
						
						CHANGE_PLAYER_TEAM(iSkipTeam, TRUE)
						
						//Get the objective text updating:
						Clear_Any_Objective_Text()
						
						SET_BIT(iLocalDebugBS, LDEBUGBS_TEAM_CHANGED_WAIT_FOR_OBJTXT)
						
						PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_DEBUG")					
						SET_BIT(iLocalBoolCheck3, LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
						PRINTLN("[JS][UOBJ - PED] - Requested Objective Update - PROCESS_DEBUG")					
						SET_BIT(iLocalBoolCheck3, LBOOL3_UPDATE_PED_OBJECTIVE)
						PRINTLN("[JS][UOBJ - OBJECT] - Requested Objective Update - PROCESS_DEBUG")					
						SET_BIT(iLocalBoolCheck3, LBOOL3_UPDATE_OBJECT_OBJECTIVE)
						iLastRule = -1
					ENDIF
				ELIF iStageToSkipTo >= 2 // Jump to a rule has been selected
					
					iStageToSkipTo -= 2
					
					IF iStageToSkipTo != MC_serverBD_4.iCurrentHighestPriority[iSkipTeam]
						IF bIsLocalPlayerHost
							BOOL bPreviousObjective = FALSE
							
							IF iStageToSkipTo < MC_serverBD_4.iCurrentHighestPriority[iSkipTeam]
								PRINTLN("[RCC MISSION] LAUNCH_MISSION_STAGE_MENU - team = ",iSkipTeam, " objective to skip back to: ",iStageToSkipTo)
								bPreviousObjective = TRUE
							ELSE
								PRINTLN("[RCC MISSION] LAUNCH_MISSION_STAGE_MENU - team = ",iSkipTeam, " objective to skip to: ",iStageToSkipTo)
								iStageToSkipTo--
							ENDIF
							
							INVALIDATE_UP_TO_AND_INCLUDING_OBJECTIVE(iSkipTeam, iStageToSkipTo, FALSE, TRUE, bPreviousObjective)
						ELSE
							BROADCAST_DEBUG_OBJECTIVE_SKIP_FOR_TEAM(iSkipTeam, iStageToSkipTo)
						ENDIF
					ENDIF
				ENDIF
				
				bUsedDebugMenu = TRUE
				CLEANUP_MENU_SKIP()
			ELSE
				
				SWITCH iSkipTeam
					CASE 0
						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_1) 
							PRINTLN("[RCC MISSION] PROCESS_DEBUG - CLEANUP_MENU_SKIP for team 0")
							CLEANUP_MENU_SKIP()
						ENDIF
					BREAK
					CASE 1
						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_2) 
							PRINTLN("[RCC MISSION] PROCESS_DEBUG - CLEANUP_MENU_SKIP for team 1")
							CLEANUP_MENU_SKIP()
						ENDIF
					BREAK
					CASE 2
						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_3) 
							PRINTLN("[RCC MISSION] PROCESS_DEBUG - CLEANUP_MENU_SKIP for team 2")
							CLEANUP_MENU_SKIP()
						ENDIF
					BREAK
					CASE 3
						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_4) 
							PRINTLN("[RCC MISSION] PROCESS_DEBUG - CLEANUP_MENU_SKIP for team 3")
							CLEANUP_MENU_SKIP()
						ENDIF
					BREAK
				ENDSWITCH
				
			ENDIF
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J) 
			IF bPlayerToUseOK
			
				SET_BIT(MC_playerBD[iPartToUse].iClientBitSet, ENUM_TO_INT(PBBOOL_PRESSED_J))
				
				SWITCH GET_MC_CLIENT_MISSION_STAGE(iPartToUse)

					CASE CLIENT_MISSION_STAGE_GOTO_LOC
					CASE CLIENT_MISSION_STAGE_CONTROL_AREA
					CASE CLIENT_MISSION_STAGE_PHOTO_LOC
						FOR i = 0 TO (FMMC_MAX_GO_TO_LOCATIONS-1)
							IF MC_serverBD_4.iGotoLocationDataPriority[i][MC_playerBD[iPartToUse].iteam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
								IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed, GET_LOCATION_VECTOR(i)) > 10
									IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciUSE_MORE_ACCURATE_RACE_POSITION)
										MC_playerBD[iLocalPart].iCheckPointForPosition = -1
										MC_playerBD[iLocalPart].iPreviousCheckPointForPosition = -1
										CLEAR_BIT(iLocalBoolCheck22, LBOOL22_INSIDE_PREVIOUS_CHECKPOINT)
										CLEAR_BIT(iLocalBoolCheck22, LBOOL22_INSIDE_NEXT_CHECKPOINT)
										MC_playerBD[iLocalPart].iLapOffset = 0
										vCachedCheckpointEntry = <<0,0,0>>
									ENDIF
									J_SKIP_MP(GET_LOCATION_VECTOR(i), << 4.0, 4.0, 4.0 >>)
									EXIT
								ENDIF
							ENDIF
						ENDFOR
					BREAK


					CASE CLIENT_MISSION_STAGE_KILL_PED
					CASE CLIENT_MISSION_STAGE_PROTECT_PED
					CASE CLIENT_MISSION_STAGE_COLLECT_PED
					CASE CLIENT_MISSION_STAGE_GOTO_PED
					CASE CLIENT_MISSION_STAGE_CONTROL_PED
					CASE CLIENT_MISSION_STAGE_PHOTO_PED
					
						FOR i = 0 TO (FMMC_MAX_PEDS-1)
							IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[i])
								IF MC_serverBD_4.iPedPriority[i][MC_playerBD[iPartToUse].iteam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
									IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed, NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[i])) > 10
										IF NOT IS_PED_IN_GROUP(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[i]))
											J_SKIP_MP(GET_ENTITY_COORDS(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[i])), << 4.0, 4.0, 4.0 >>, FALSE )
											EXIT
										ENDIF
									ELSE
										IF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_KILL_PED
											IF MC_serverBD_4.iPedRule[i][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL
												SET_ENTITY_HEALTH(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[i]),0)
												EXIT
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
						
						FOR i = 0 TO (FMMC_MAX_PEDS-1)
							IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[i])
								IF MC_serverBD_4.iPedPriority[i][MC_playerBD[iPartToUse].iteam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
									IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed, NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[i])) > 10
										J_SKIP_MP(GET_ENTITY_COORDS(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[i])), << 4.0, 4.0, 4.0 >>,FALSE)
										EXIT
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
						
					BREAK
					
					CASE CLIENT_MISSION_STAGE_DELIVER_PED
					
						IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
							FOR i = 0 TO (FMMC_MAX_PEDS-1)
								IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[i])
									IF IS_PED_GROUP_MEMBER(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[i]), giPlayerGroup)
										SET_ENTITY_COORDS(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[i]),GET_DROP_OFF_CENTER())
									ENDIF
								ENDIF
							ENDFOR
							J_SKIP_MP(GET_DROP_OFF_CENTER(), << 6.0, 6.0, 6.0 >>,FALSE)
						ENDIF
						
					BREAK
					
					CASE CLIENT_MISSION_STAGE_KILL_VEH
					CASE CLIENT_MISSION_STAGE_PROTECT_VEH
					CASE CLIENT_MISSION_STAGE_COLLECT_VEH
					CASE CLIENT_MISSION_STAGE_GOTO_VEH
					CASE CLIENT_MISSION_STAGE_CONTROL_VEH
					CASE CLIENT_MISSION_STAGE_PHOTO_VEH
					
						FOR i = 0 TO (FMMC_MAX_VEHICLES-1)
							IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
							AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]))
								IF MC_serverBD_4.iVehPriority[i][MC_playerBD[iPartToUse].iteam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
									IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed, NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i])) > 10
										IF IS_VEHICLE_SEAT_FREE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]))
											J_SKIP_MP(GET_ENTITY_COORDS(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i])), << 6.0, 6.0, 6.0 >>)
											EXIT
										ENDIF
									ELSE
										IF MC_serverBD_4.ivehRule[i][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL
											PRINTLN("[RCC MISSION] PROCESS_DEBUG - Calling NETWORK_EXPLODE_VEHICLE on veh ",i," as J key was pressed")
											NETWORK_EXPLODE_VEHICLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]))
											EXIT
										ELIF MC_serverBD_4.ivehRule[i][MC_playerBD[iPartToUse].iteam] =  FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
											IF IS_VEHICLE_SEAT_FREE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]))
												TASK_ENTER_VEHICLE(LocalPlayerPed, NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]), 1, VS_DRIVER, PEDMOVE_RUN, ECF_WARP_PED)
												EXIT
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
						
						FOR i = 0 TO (FMMC_MAX_VEHICLES-1)
							IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
							AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]))
								IF MC_serverBD_4.iVehPriority[i][MC_playerBD[iPartToUse].iteam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
									IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed, NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i])) > 10
										J_SKIP_MP(GET_ENTITY_COORDS(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i])), << 6.0, 6.0, 6.0 >>)
										EXIT
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
						
					BREAK

					CASE CLIENT_MISSION_STAGE_DELIVER_VEH
					
						IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
							IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
								IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed, GET_DROP_OFF_CENTER()) > 5	
									J_SKIP_MP(GET_DROP_OFF_CENTER(), << 6.0, 6.0, 6.0 >>,TRUE)
								ENDIF
							ENDIF
						ENDIF
						
					BREAK
					
					CASE CLIENT_MISSION_STAGE_KILL_OBJ
					CASE CLIENT_MISSION_STAGE_PROTECT_OBJ
					CASE CLIENT_MISSION_STAGE_COLLECT_OBJ
					CASE CLIENT_MISSION_STAGE_GOTO_OBJ
					CASE CLIENT_MISSION_STAGE_CONTROL_OBJ
					CASE CLIENT_MISSION_STAGE_HACK_COMP
					CASE CLIENT_MISSION_STAGE_HACK_SAFE
					CASE CLIENT_MISSION_STAGE_PHOTO_OBJ
					CASE CLIENT_MISSION_STAGE_BLOW_DOOR
					CASE CLIENT_MISSION_STAGE_HACK_OPEN_CASE
					CASE CLIENT_MISSION_STAGE_CASH_GRAB
					CASE CLIENT_MISSION_STAGE_HACK_DRILL
					CASE CLIENT_MISSION_STAGE_OPEN_CONTAINER
					CASE CLIENT_MISSION_STAGE_ENTER_SCRIPTED_TURRET
					CASE CLIENT_MISSION_STAGE_INTERACT_WITH
					CASE CLIENT_MISSION_STAGE_HACK_SYNC_LOCK
						FOR i = 0 TO (FMMC_MAX_NUM_OBJECTS-1)
							IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[i])
								IF NOT IS_ENTITY_DEAD(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[i]))
									IF MC_serverBD_4.iObjPriority[i][MC_playerBD[iPartToUse].iteam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
										IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed, NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[i])) > 10
											IF NOT IS_ENTITY_ATTACHED(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[i]))
												J_SKIP_MP(GET_ENTITY_COORDS(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[i])), << 6.0, 6.0, 6.0 >>)
												EXIT
											ENDIF
										ELSE
											IF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_KILL_OBJ
												IF MC_serverBD_4.iObjRule[i][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL
													SET_ENTITY_HEALTH(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[i]),0)
													EXIT
												ENDIF
											ELIF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) =  CLIENT_MISSION_STAGE_HACK_COMP
												IF MC_playerBD[iPartToUse].iObjHacking != -1
													HACKING_J_SKIP()
													EXIT
												ENDIF
											ELIF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_HACK_SAFE
												IF MC_playerBD[iPartToUse].iObjHacking != -1
													MC_playerBD[iPartToUse].iObjHacked = MC_playerBD[iPartToUse].iObjHacking
													MC_playerBD[iPartToUse].iObjHacking = -1
													RESET_SAFE_CRACK(SafeCrackData, TRUE)
													CLEAR_SAFE_CRACK_SEQUENCES(SafeCrackData)
													CLEAR_PED_TASKS(LocalPlayerPed)
													EXIT
												ENDIF
											ELIF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_BLOW_DOOR
												MC_playerBD[iPartToUse].iObjHacked = i
												EXIT
											ELIF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_HACK_OPEN_CASE
												MC_playerBD[iPartToUse].iObjHacked = i
												EXIT
											ELIF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_OPEN_CONTAINER	
												MC_playerBD[iPartToUse].iObjHacked = i
												EXIT
											ELIF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_ENTER_SCRIPTED_TURRET
												MC_playerBD[iPartToUse].iObjHacked = i
												EXIT
											ELIF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_INTERACT_WITH
												MC_playerBD[iPartToUse].iObjHacked = i
												EXIT
											ELIF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_HACK_SYNC_LOCK
												MC_playerBD[iPartToUse].iObjHacked = i
												EXIT
											ENDIF
										ENDIF
									ENDIF 
								ENDIF
							ENDIF
						ENDFOR		
						
						FOR i = 0 TO (FMMC_MAX_NUM_OBJECTS-1)
							IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[i])
								IF NOT IS_ENTITY_DEAD(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[i]))
									IF MC_serverBD_4.iObjPriority[i][MC_playerBD[iPartToUse].iteam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
										IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed, NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[i])) > 10
											J_SKIP_MP(GET_ENTITY_COORDS(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[i])), << 1.0, 1.0, 1.0 >>)
											EXIT
										ENDIF
									ENDIF 
								ENDIF
							ENDIF
						ENDFOR		
						
						IF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_CONTROL_OBJ
							IF MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] < FMMC_MAX_TEAMS
								IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitsetNine[MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]], ciBS_RULE9_USE_GRANULAR_CAPTURE)
									MC_PlayerBD[iLocalPart].iGranularCurrentPoints += 999999
								ENDIF
							ENDIF
						ENDIF
						
					BREAK
					
					
					CASE CLIENT_MISSION_STAGE_DELIVER_OBJ
					
						IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
							IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed, GET_DROP_OFF_CENTER()) > 50	
								J_SKIP_MP(GET_DROP_OFF_CENTER(), << 6.0, 6.0, 6.0 >>,TRUE)
							ENDIF
						ENDIF
						
					BREAK

				ENDSWITCH		
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD	
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_F8, KEYBOARD_MODIFIER_ALT, "Sync Scene Ped Debug")
			IF NOT bSyncScenePedDebugDisplay
				bSyncScenePedDebugDisplay = TRUE
			ELSE
				bSyncScenePedDebugDisplay = FALSE
			ENDIF
		ENDIF
		IF bSyncScenePedDebugDisplay 
			DEBUG_SHOW_SYNC_SCENE_STATES()
		ENDIF
	#ENDIF
ENDPROC

PROC PROCESS_PAUSED_TIMERS()
	#IF IS_DEBUG_BUILD	
		
		BOOL bChange
		IF bWdPauseGameTimerRag
			bWdPauseGameTimerRag = FALSE
			bChange = TRUE
		ENDIF
		
		IF bChange
			IF NOT bWdPauseGameTimer
				bWdPauseGameTimer = TRUE				
			ELIF bWdPauseGameTimer
				bWdPauseGameTimer = FALSE				
			ENDIF
			BROADCAST_FMMC_PAUSE_ALL_TIMERS(bWdPauseGameTimer)
		ENDIF
		
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_9, KEYBOARD_MODIFIER_ALT, "Pause Mission Timers")
			IF NOT bWdPauseGameTimer
				bWdPauseGameTimer = TRUE
			ELIF bWdPauseGameTimer
				bWdPauseGameTimer = FALSE
			ENDIF			
			BROADCAST_FMMC_PAUSE_ALL_TIMERS(bWdPauseGameTimer)
		ENDIF
		
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_0, KEYBOARD_MODIFIER_ALT, "Increment Timers")		
			BROADCAST_FMMC_EDIT_ALL_TIMERS(TRUE)
		ENDIF
		
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_8, KEYBOARD_MODIFIER_ALT, "Decrement Timers")		
			BROADCAST_FMMC_EDIT_ALL_TIMERS(FALSE)
		ENDIF
		
		IF bWdPauseGameTimer
			FLOAT fTextSize = 0.55
			TEXT_LABEL_63 sText
			SET_TEXT_SCALE(fTextSize, fTextSize)
			SET_TEXT_COLOUR(75, 75, 170, 200)
			sText = "### Mission Timers Paused ###"
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.4, 0.15, "STRING", sText)
		ENDIF
		
		IF NOT bIsLocalPlayerHost
			bWdDecrementTimer = FALSE
			bWdIncrementTimer = FALSE
			EXIT
		ENDIF
		
		IF bWdIncrementTimer
			INT iTeamDebug
			FOR iTeamDebug = 0 TO FMMC_MAX_TEAMS-1
				MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeamDebug].Timer = GET_TIME_OFFSET(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeamDebug].Timer, 30000)
				MC_serverBD_3.tdObjectiveLimitTimer[iTeamDebug].Timer = GET_TIME_OFFSET(MC_serverBD_3.tdObjectiveLimitTimer[iTeamDebug].Timer, 30000)
			ENDFOR
			MC_serverBD.tdMissionLengthTimer.Timer = GET_TIME_OFFSET(MC_serverBD.tdMissionLengthTimer.Timer, 30000)
			bWdIncrementTimer = FALSE
			
			PRINTLN("[LM][PROCESS_PAUSED_TIMERS] - Incrementing by 30000")
		ENDIF
		
		IF bWdDecrementTimer
			INT iTeamDebug
			FOR iTeamDebug = 0 TO FMMC_MAX_TEAMS-1
				MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeamDebug].Timer = GET_TIME_OFFSET(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeamDebug].Timer, -30000)
				MC_serverBD_3.tdObjectiveLimitTimer[iTeamDebug].Timer = GET_TIME_OFFSET(MC_serverBD_3.tdObjectiveLimitTimer[iTeamDebug].Timer, -30000)
			ENDFOR
			MC_serverBD.tdMissionLengthTimer.Timer = GET_TIME_OFFSET(MC_serverBD.tdMissionLengthTimer.Timer, -30000)
			bWdDecrementTimer = FALSE
			
			PRINTLN("[LM][PROCESS_PAUSED_TIMERS] - Decrementing by 30000")
		ENDIF
		
		IF NOT bWdPauseGameTimer
			IF bWdPauseGameTimerStarted
				// Apply Cached Time.
				PRINTLN("[LM][PROCESS_PAUSED_TIMERS] - Unpausing Mission Timers.")
				bWdPauseGameTimerStarted = FALSE
				INT iTeamDebug
				FOR iTeamDebug = 0 TO FMMC_MAX_TEAMS-1
					MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeamDebug].Timer = GET_TIME_OFFSET(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeamDebug].Timer, -(MC_serverBD_3.iMultiObjectiveTimeLimitDebugCache[iTeamDebug]-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeamDebug])))
					MC_serverBD_3.tdObjectiveLimitTimer[iTeamDebug].Timer = GET_TIME_OFFSET(MC_serverBD_3.tdObjectiveLimitTimer[iTeamDebug].Timer, -(MC_serverBD_3.iRuleObjectiveTimeLimitDebugCache[iTeamDebug]-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdObjectiveLimitTimer[iTeamDebug])))
				ENDFOR
				MC_serverBD.tdMissionLengthTimer.Timer = GET_TIME_OFFSET(MC_serverBD.tdMissionLengthTimer.Timer, -(MC_serverBD_3.iMissionLengthTimerDebugCache-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdMissionLengthTimer)))
			ENDIF
		ELIF bWdPauseGameTimer				
			IF NOT bWdPauseGameTimerStarted
				PRINTLN("[LM][PROCESS_PAUSED_TIMERS] - Pausing Mission Timers.")
				// Cache Time Started
				bWdPauseGameTimerStarted = TRUE
				INT iTeamDebug
				FOR iTeamDebug = 0 TO FMMC_MAX_TEAMS-1
					MC_serverBD_3.iMultiObjectiveTimeLimitDebugCache[iTeamDebug] = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeamDebug])
					MC_serverBD_3.iRuleObjectiveTimeLimitDebugCache[iTeamDebug] = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdObjectiveLimitTimer[iTeamDebug])
				ENDFOR
				MC_serverBD_3.iMissionLengthTimerDebugCache = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdMissionLengthTimer)
			ENDIF
			
			IF (GET_FRAME_COUNT() % 20) = 0
				INT iTeamDebug
				FOR iTeamDebug = 0 TO FMMC_MAX_TEAMS-1
					MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeamDebug].Timer = GET_TIME_OFFSET(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeamDebug].Timer, -(MC_serverBD_3.iMultiObjectiveTimeLimitDebugCache[iTeamDebug]-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeamDebug])))
					MC_serverBD_3.tdObjectiveLimitTimer[iTeamDebug].Timer = GET_TIME_OFFSET(MC_serverBD_3.tdObjectiveLimitTimer[iTeamDebug].Timer, -(MC_serverBD_3.iRuleObjectiveTimeLimitDebugCache[iTeamDebug]-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdObjectiveLimitTimer[iTeamDebug])))
				ENDFOR
				MC_serverBD.tdMissionLengthTimer.Timer = GET_TIME_OFFSET(MC_serverBD.tdMissionLengthTimer.Timer, -(MC_serverBD_3.iMissionLengthTimerDebugCache-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdMissionLengthTimer)))
			ENDIF
		ENDIF
	#ENDIF
ENDPROC	

PROC CREATE_OWAIN_WIDGETS()
	
	/*
	START_WIDGET_GROUP( "Owain" )

		ADD_WIDGET_BOOL( "Enable Widgets", bEnableOwainWidgets )
		ADD_WIDGET_FLOAT_SLIDER( "Heist Armour Multiplier", fHeistArmourDefenseValueWidgetModifier, 0.1000001, 2, 0.05 )
		
	STOP_WIDGET_GROUP()	
	*/	
ENDPROC
BOOL bSRDebugToggleOffMeterLogic
PROC CREATE_MC_WIDGETS()
INT i
TEXT_LABEL_31 dummy
	
	FMMC_WIDGET_GROUP = START_WIDGET_GROUP( " Mission Controller") 
		SET_BEAM_HACK_PARENT_WIDGET_GROUP(FMMC_WIDGET_GROUP)
		SET_HOTWIRE_PARENT_WIDGET_GROUP(FMMC_WIDGET_GROUP)
		ADD_WIDGET_BOOL("bOutputDebug", boutputdebugspam)
		ADD_WIDGET_BOOL("Stop undriveable vehicle cleanup:", bStopVehDeadCleanup)
		ADD_WIDGET_BOOL("bDisableDeathsCount:", bDisableDeathsCount)
		ADD_WIDGET_BOOL("Add ped to group prints",bAddToGroupPrints)
		ADD_WIDGET_BOOL("Go to locate prints",bGotoLocPrints)
		ADD_WIDGET_BOOL("Plane dropoff prints",bPlaneDropoffPrints)
		ADD_WIDGET_BOOL("b4517649Debug",b4517649Debug)
		ADD_WIDGET_BOOL( "Toggle Off Low Speed Bomb Meter", bSRDebugToggleOffMeterLogic )
		
		ADD_WIDGET_BOOL("Hide Objects", bHideObjects)
		
		ADD_WIDGET_BOOL("Turn on Damage Tracking for all vehicle Network IDs", bEnableDamageTrackerOnNetworkID)
		
		ADD_WIDGET_BOOL( "Draw Respawn Point Debug ",bSlipstreamRespawnDebug)
		
		ADD_WIDGET_BOOL( "Pause Rule and MultiRule Timers ", bWdPauseGameTimerRag)
	
		ADD_WIDGET_BOOL( "Extra Leaderboard Prints", bEnableWidgetPrintsFor3137595bug )
		
		START_WIDGET_GROUP("Smash Car")
			ADD_WIDGET_INT_SLIDER("iCooldDownSmash",g_FMMC_STRUCT.iCooldDownSmash,0,10000,10)
			ADD_WIDGET_FLOAT_SLIDER("fMaxLateralForceSmash",g_FMMC_STRUCT.fMaxLateralForceSmash,0,30,1)
			ADD_WIDGET_FLOAT_SLIDER("fMaxVerticalForceSmash",g_FMMC_STRUCT.fMaxVerticalForceSmash,0,30,0.5)
			ADD_WIDGET_FLOAT_SLIDER("fMinLateralForceSmash",g_FMMC_STRUCT.fMinLateralForceSmash,0,30,1)
			ADD_WIDGET_FLOAT_SLIDER("fMinVerticalForceSmash",g_FMMC_STRUCT.fMinVerticalForceSmash,0,30,0.5)
			ADD_WIDGET_FLOAT_SLIDER("fMaxPushSmash",g_FMMC_STRUCT.fMaxPushSmash,0,30,1)
			ADD_WIDGET_FLOAT_SLIDER("fMinPushSmash",g_FMMC_STRUCT.fMinPushSmash,0,30,1)
			ADD_WIDGET_FLOAT_SLIDER("fMaxPushVSmash",g_FMMC_STRUCT.fMaxPushVSmash,0,30,1)
			ADD_WIDGET_FLOAT_SLIDER("fMinPushVSmash",g_FMMC_STRUCT.fMinPushVSmash,0,30,1)
			ADD_WIDGET_INT_SLIDER("iMaxDamageSmash",g_FMMC_STRUCT.iMaxDamageSmash,0,1000,1)
			ADD_WIDGET_INT_SLIDER("iMinDamageSmash",g_FMMC_STRUCT.iMinDamageSmash,0,1000,1)
			ADD_WIDGET_FLOAT_SLIDER("fMinSpeedForceForward",g_FMMC_STRUCT.fMinSpeedForceForward,0,1,0.1)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Beast Mode")
			ADD_WIDGET_INT_SLIDER("iStealthAlpha", iStealthAlpha, 0, 255, 1)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("RATC - Health Drain")
			ADD_WIDGET_BOOL( "Toggle Off Health Drain", bRAGStopDrain )
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Air Quota")
			ADD_WIDGET_FLOAT_SLIDER("fWingDebug", fWingDebug, -9, 9, 0.01)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Reusable")
			ADD_WIDGET_VECTOR_SLIDER("vRagPlacementVector", vRagPlacementVector, -90, 90, 0.01)
			ADD_WIDGET_VECTOR_SLIDER("vRagRotationVector", vRagRotationVector, -180, 180, 0.01)
			
			ADD_WIDGET_FLOAT_SLIDER("cfInteractWith_OccupiedAreaCheckSize ", cfInteractWith_OccupiedAreaCheckSize, 0.0, 5, 0.01)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Showdown")
			ADD_WIDGET_BOOL("Showdown: Team Score Debug", bShowdown_TeamDebugActive)
			ADD_WIDGET_BOOL("Showdown: Disable All Drain Damage", bShowdown_DisableDrain)
			ADD_WIDGET_BOOL("Showdown: Disable Idle Damage", bShowdown_DisableIdleDrain)
			ADD_WIDGET_VECTOR_SLIDER("Showdown: Team Score Debug - Start Pos", vShowdownDEBUG_StartPos, 0, 1, 0.01)
			ADD_WIDGET_VECTOR_SLIDER("Showdown: Team Score Debug - Offsets", vShowdownDEBUG_Offsets, 0, 1, 0.01)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Juggernaut Intro Cutscene")
			ADD_WIDGET_FLOAT_SLIDER("Juggernaut Intro Cutscene: Fade Speed", cf_CutsceneJugg_OpacityFadeSpeed, 0.1, 1000, 0.01)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Tiny Racers")
			START_WIDGET_GROUP("Camera")
				ADD_WIDGET_FLOAT_SLIDER("Minimum Cam Height", fMinHeightTR, 0, 50, 1)
				ADD_WIDGET_FLOAT_SLIDER("Base Height", fBEBaseHeightTR, 0, 100, 1)
				
				ADD_WIDGET_FLOAT_SLIDER("Height Speed Multiplier", fSpeedMultiplierTR, 0, 1, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("Distance Multiplier", fDistanceMultiplierTR, 0, 3, 0.1)
				ADD_WIDGET_BOOL("Allow Offset Change", bTRAllowOffsetChange)
				ADD_WIDGET_FLOAT_SLIDER("Base Offset", fBasePosOffsetTR, -50, 50, 1)
				ADD_WIDGET_FLOAT_SLIDER("Directional Offset Multiplier", fDirectionalOffsetMultiplierTR, 0, 50, 1)
				
				ADD_WIDGET_FLOAT_SLIDER("Camera Angle", fBirdsEyeAngleTR, -180, 0, 1)
				
				ADD_WIDGET_FLOAT_SLIDER("Motion Blur", fTRMotionBlurStrength, 0,1, 0.05)
				ADD_WIDGET_FLOAT_SLIDER("FOV", fBirdsEyeTRFOV, 10, 100, 1)
				ADD_WIDGET_FLOAT_SLIDER("Heading Dampening", fBirdsEyeRotInterpSpeed, 0, 1, 0.05)
				
//				ADD_WIDGET_FLOAT_SLIDER("X", fTRPointAtX, -180, 180, 1)
//				ADD_WIDGET_FLOAT_SLIDER("Y", fTRPointAtY, -180, 180, 1)
//				ADD_WIDGET_FLOAT_SLIDER("Z", fTRPointAtZ, -180, 180, 1)
//				ADD_WIDGET_BOOL("Relative", bTRRelativeCam)

			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Intro Cam")
				ADD_WIDGET_FLOAT_SLIDER("Blend Cam Phase", fBlendCamPhase, 0, 1, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Post FX Phase", fPostFxPhase, 0, 1, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Motion Blur", fMotionBlurStrength, 0, 1, 0.01)
				ADD_WIDGET_INT_SLIDER("Bone Index", iTRBoneIndex, -1, 20, 1) 
				ADD_WIDGET_INT_SLIDER("Cam interp", iTRInterpTime, 0, 5000, 100)
				ADD_WIDGET_BOOL("Deactivate Intro Camera", bDeactivate) 
				ADD_WIDGET_FLOAT_SLIDER("BirdsEye Pos Interp", fBirdsEyeInterpSpeed, 0, 1, 0.005)
				ADD_WIDGET_FLOAT_SLIDER("BirdsEye Rot Interp", fBirdsEyeRotInterpSpeed, 0, 1, 0.005)
			STOP_WIDGET_GROUP()
			
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Airstrike Controls")
			ADD_WIDGET_BOOL("Disable Explosion Bar Kill",bDisableExplosionBarKill)
			ADD_WIDGET_BOOL("Use RAG Range Values", bUseRAGValues)
			ADD_WIDGET_FLOAT_SLIDER("Range Min", fRAGAirstrikeMin, 0, 600, 1)
			ADD_WIDGET_FLOAT_SLIDER("Range Max", fRAGAirstrikeMax, 0, 600, 1)
			START_WIDGET_GROUP("Debug Sphere")
				ADD_WIDGET_BOOL("Show debug sphere for range",bAirstrikeSpheres)
				ADD_WIDGET_BOOL("Show Min",bAirstrikeSphereMin)
				ADD_WIDGET_BOOL("Show Max",bAirstrikeSphereMax)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Classic GTA Cam")
				
			ADD_WIDGET_BOOL("Use BirdsEye Cam", bUsingBirdsEyeCam)
			
			ADD_WIDGET_BOOL("Use North Cam", bLockNorth)
			ADD_WIDGET_FLOAT_SLIDER("FOV", BirdsEyeFOV, 10, 100, 1)
			
			ADD_WIDGET_FLOAT_SLIDER("BirdsEye Heading Dampening", g_FMMC_STRUCT.fBEVCamHeadDampening, 0, 1, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("POS Height Dampening", g_FMMC_STRUCT.fBEVCamHeightDampening, 0, 1, 0.01)
			
			ADD_WIDGET_FLOAT_SLIDER("POS Minimum Cam Height", fBEMinHeight, 0, 50, 1)
			ADD_WIDGET_FLOAT_SLIDER("POS Base Height", fBEBaseHeight, 0, 50, 1)
			
			ADD_WIDGET_FLOAT_SLIDER("POS Height Speed Multiplier", fSpeedMultiplier, 0, 1, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("POS Distance Multiplier", fDistanceMultiplier, 0, 3, 0.1)
			
			ADD_WIDGET_BOOL( "Use Fixed Camera Offset", bUseFixedCameraOffset )
			ADD_WIDGET_FLOAT_SLIDER("POS Base Offset", fBasePosOffset, -50, 50, 1)
			ADD_WIDGET_FLOAT_SLIDER("Directional Offset Multiplier", fDirectionalOffsetMultiplier, 0, 50, 1)
			
			ADD_WIDGET_FLOAT_SLIDER("POS Camera Angle", fBirdsEyeAngle, -180, 0, 1)
			//ADD_WIDGET_FLOAT_SLIDER("POS Interp Rate", fPosInterpRate, 0, 50, 1)
			//ADD_WIDGET_FLOAT_SLIDER("LookAt Debug Sphere", SphereSize, 0, 1, 0.1)
			//ADD_WIDGET_FLOAT_SLIDER("LookAt Distance Multiplier", fLookAtDistanceMultiplier, 0, 3, 0.1)
			//ADD_WIDGET_FLOAT_SLIDER("LookAt Interp Rate", fLookAtInterpRate, 0, 1, 0.001)
			//ADD_WIDGET_FLOAT_SLIDER("Camera Height", fCamHeight, 0, 200, 1)			
			
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("ZAPPING")
			ADD_WIDGET_INT_SLIDER("Zap Time", waittimezap, 0, 5000, 100)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Interact With Camera")
			ADD_WIDGET_BOOL("Disable Interact With Camera", bDisableIWCam)
			ADD_WIDGET_VECTOR_SLIDER("Camera Offset", vIWCamOffsetOverride, -100, 100, 0.05)
			ADD_WIDGET_VECTOR_SLIDER("Camera Rotation", vIWCamRotOverride, -180, 180, 0.05)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Rappelling Camera")
			ADD_WIDGET_BOOL("Use Rappelling Cam Data", bUseRappelCamWidget)
			ADD_WIDGET_VECTOR_SLIDER("Player Camera Offset", vRappelCam, -20, 20, 0.01)
			ADD_WIDGET_VECTOR_SLIDER("Player Camera Rotation", vRappelRot, -180, 180, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("FOV", fRappelFOV, 0, 100, 0.5)
			ADD_WIDGET_INT_SLIDER("Rappel Position", iRappelParticipant, -1, 3, 1)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("PRON")
			ADD_WIDGET_FLOAT_SLIDER("Lock Speed", fLOCKVEHSPEED, 0, 30, 0.5)
			ADD_WIDGET_INT_SLIDER("Alpha MAIN", iMain_A, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("Alpha A_Segment1", A_Segment1, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("Alpha A_Segment2", A_Segment2, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("Alpha A_SegmentB", A_SegmentB, 0, 255, 1)
			ADD_WIDGET_FLOAT_SLIDER("linetopsize", fLineTop, 0, 1, 0.001)

		STOP_WIDGET_GROUP()
	
		START_WIDGET_GROUP( " Leaderboard")
		    FOR i = 0 TO (MAX_NUM_DM_PLAYERS - 1)
		          dummy = "LB "
		          dummy += i
		          START_WIDGET_GROUP(dummy)
		                ADD_WIDGET_INT_READ_ONLY("iParticipant", g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant)
		                ADD_WIDGET_INT_READ_ONLY("iTeam", g_MissionControllerserverBD_LB.sleaderboard[i].iTeam)
		                ADD_WIDGET_INT_READ_ONLY("iTeamScore",g_MissionControllerserverBD_LB.sleaderboard[i].iTeamScore)
						ADD_WIDGET_INT_READ_ONLY("iplayerScore", g_MissionControllerserverBD_LB.sleaderboard[i].iPlayerScore)                                   
		                ADD_WIDGET_INT_READ_ONLY("iKills", g_MissionControllerserverBD_LB.sleaderboard[i].iKills)                                   
		                ADD_WIDGET_INT_READ_ONLY("iHeadshots", g_MissionControllerserverBD_LB.sleaderboard[i].iHeadshots)
		                ADD_WIDGET_INT_READ_ONLY("iDeaths", g_MissionControllerserverBD_LB.sleaderboard[i].iDeaths)
		                ADD_WIDGET_INT_READ_ONLY("iRank", g_MissionControllerserverBD_LB.sleaderboard[i].iRank)
		          STOP_WIDGET_GROUP()
		    ENDFOR
		STOP_WIDGET_GROUP()
		
		
		START_WIDGET_GROUP("Force On Heist Widgets")
			ADD_WIDGET_BOOL("bForceOnHeistPrep", g_bFakePrep)
			ADD_WIDGET_BOOL("bForceOnHeistFinale", g_bFakeFinale)
		STOP_WIDGET_GROUP()
		
		
		START_WIDGET_GROUP("Player In Drop Off")
			ADD_WIDGET_BOOL("Do Player In Drop Off Prints", bPlayerInDropOffPrints)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Ped Count")
			ADD_WIDGET_BOOL("Do Count Prints", bDoPedCountPrints)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Round Lbd")
			 ADD_WIDGET_INT_READ_ONLY("MC_ServerBroadcastData3.iRoundLbdProgress", MC_serverBD_3.iRoundLbdProgress) 
			 ADD_WIDGET_INT_READ_ONLY("iRoundsLbdStage", iRoundsLbdStage)
			 ADD_WIDGET_FLOAT_SLIDER("Condemned bar speed", g_FMMC_STRUCT.fCondemnedBarSpeed, 0.0, 150.0, 0.1)
			 ADD_WIDGET_BOOL("Next condemned flash ", bDebugCondemnedNextAlwaysFlash)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Artificial Lights")
			ADD_WIDGET_BOOL("Activate Debug", bActivateArtificialLightsDebug)
			ADD_WIDGET_INT_SLIDER("Lights Off Duration", iDebugArtificialLightsOffDuration, -1, 3600000, 1)
			ADD_WIDGET_BOOL("Print Cloud Artificial Light Timer Value", bPrintArticificalLightTimerValue)
		STOP_WIDGET_GROUP()
		
	  	START_WIDGET_GROUP("Cops No Blip Bug")
			ADD_WIDGET_BOOL("Output Prints", bOutputCopMissingBlipDebug)
		STOP_WIDGET_GROUP()

		START_WIDGET_GROUP(" FVJ")
			 ADD_WIDGET_FLOAT_READ_ONLY("fHrtbDistanceFromScaryPed", fHrtbDistanceFromScaryPed)
		STOP_WIDGET_GROUP()
	
		START_WIDGET_GROUP(" SCROLL")		
			ADD_WIDGET_INT_READ_ONLY("iLBPlayerSelection", iLBPlayerSelection)
			ADD_WIDGET_INT_SLIDER("g_i_ActualLeaderboardPlayerSelected", g_i_ActualLeaderboardPlayerSelected, -100, 9999, 1)
			ADD_WIDGET_INT_SLIDER("g_i_LeaderboardRowCounter", g_i_LeaderboardRowCounter, -100, 9999, 1)
			ADD_WIDGET_INT_SLIDER("g_i_LeaderboardShift", g_i_LeaderboardShift, -100, 9999, 1)
			ADD_WIDGET_INT_READ_ONLY("iNumSpectators", MC_serverBD.iNumSpectators)
			ADD_WIDGET_INT_READ_ONLY("iNumActiveTeams", MC_serverBD.iNumActiveTeams)
			ADD_WIDGET_INT_READ_ONLY("iNumberOfLBPlayers[0]", MC_serverBD.iNumberOfLBPlayers[0])
		STOP_WIDGET_GROUP()
	  
		START_WIDGET_GROUP("XP and CASH") 
			ADD_WIDGET_INT_READ_ONLY("Total Mission Cash", iDebugRewardCash)
			ADD_WIDGET_INT_READ_ONLY("PP CASH After tunable ", iDebugPPRewardCashTune)
			ADD_WIDGET_INT_READ_ONLY("CASH from pickup ", iDebugCashPickup)
			ADD_WIDGET_INT_READ_ONLY("XP deliver Vehicle", iDebugXPDelVeh)
			ADD_WIDGET_INT_READ_ONLY("XP for killing all enemies", iAllKillXPBonus)
			ADD_WIDGET_INT_READ_ONLY("XP for you only headshot", iAllHeadshotXPPlayer)
			ADD_WIDGET_INT_READ_ONLY("XP for team only headshot", iAllHeadshotXPTeam)
			ADD_WIDGET_INT_READ_ONLY("Mission reward XP", iDebugTotalRewardXP)
			ADD_WIDGET_INT_READ_ONLY("XP Lives Bonus", iDebugXPLivesBonus)
			ADD_WIDGET_INT_READ_ONLY("FINAL XP given", iDebugXPGiven)
			ADD_WIDGET_INT_READ_ONLY("accepted invite XP", iDebugXPInvite)
			ADD_WIDGET_INT_READ_ONLY("daily win XP", iDebugXPDailyWin)
			ADD_WIDGET_INT_READ_ONLY("Playerd 5 missions XP", iDebugXP5Plays)
		STOP_WIDGET_GROUP()
		
		start_widget_group("LBD_CS")
			ADD_WIDGET_BOOL("bLBDHold: ", bLBDHold)
			add_widget_float_slider("fRoundsOffset", fRoundsOffset, -2000.0, 2000.0, 0.1)
		STOP_WIDGET_GROUP()
	  
		start_widget_group("angled_area")
			
			add_widget_float_slider("left offset x", widget_offset_bl.x, -2000.0, 2000.0, 0.1)
			add_widget_float_slider("left offset y", widget_offset_bl.y, -2000.0, 2000.0, 0.1)
			add_widget_float_slider("left offset z", widget_offset_bl.z, -2000.0, 2000.0, 0.1)

			add_widget_float_slider("right offset x", widget_offset_br.x, -2000.0, 2000.0, 0.1)
			add_widget_float_slider("right offset y", widget_offset_br.y, -2000.0, 2000.0, 0.1)
			add_widget_float_slider("right offset z", widget_offset_br.z, -2000.0, 2000.0, 0.1)

			add_widget_float_slider("angled area length", widget_angled_length, -2000.0, 2000.0, 0.1)

			add_widget_float_slider("heading", widget_heading, -2000.0, 2000.0, 0.1)

			add_widget_float_read_only("widget_bl x", widget_bl.x)
			add_widget_float_read_only("widget_bl y", widget_bl.y)
			add_widget_float_read_only("widget_bl z", widget_bl.z)

			add_widget_float_read_only("widget_br x", widget_br.x)
			add_widget_float_read_only("widget_br y", widget_br.y)
			add_widget_float_read_only("widget_br z", widget_br.z)

		stop_widget_group()
		
		START_WIDGET_GROUP("CASH GRAB")
			
			START_WIDGET_GROUP("CASH GRAB CAMERA")
				ADD_WIDGET_STRING("Start Camera")
				ADD_WIDGET_VECTOR_SLIDER("vCashGrabCamAttachOffsetStart", vCashGrabCamAttachOffsetStart, -10.0, 10.0 , 0.1)
				ADD_WIDGET_VECTOR_SLIDER("vCashGrabCamPointOffsetStart", vCashGrabCamPointOffsetStart, -10.0, 10.0 , 0.1)
				ADD_WIDGET_VECTOR_SLIDER("vCashGrabCamRotationStart", vCashGrabCamRotationStart, -360.0, 360.0 , 1.0)
				ADD_WIDGET_STRING("End Camera")
				ADD_WIDGET_VECTOR_SLIDER("vCashGrabCamAttachOffsetEnd", vCashGrabCamAttachOffsetEnd, -10.0, 10.0 , 0.1)
				ADD_WIDGET_VECTOR_SLIDER("vCashGrabCamPointOffsetEnd", vCashGrabCamPointOffsetEnd, -10.0, 10.0 , 0.1)
				ADD_WIDGET_VECTOR_SLIDER("vCashGrabCamRotationEnd", vCashGrabCamRotationEnd, -360.0, 360.0 , 1.0)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("CASH GRAB DROP OFFSET")
				ADD_WIDGET_STRING("Drop Offset 1")
				ADD_WIDGET_VECTOR_SLIDER("vCashGrabDropOffset1", vCashGrabDropOffset1, -10.0, 10.0 , 0.01)
				ADD_WIDGET_STRING("Drop Offset 2")
				ADD_WIDGET_VECTOR_SLIDER("vCashGrabDropOffset2", vCashGrabDropOffset2, -10.0, 10.0 , 0.01)
				ADD_WIDGET_STRING("Drop Offset 3")
				ADD_WIDGET_VECTOR_SLIDER("vCashGrabDropOffset3", vCashGrabDropOffset3, -10.0, 10.0 , 0.01)
				ADD_WIDGET_STRING("Drop Radius")
				ADD_WIDGET_FLOAT_SLIDER("fCashGrabDropRadius",fCashGrabDropRadius,-1,1,0.01)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("CASH GRAB ANGLED AREA")
				ADD_WIDGET_FLOAT_SLIDER("fCashGrabAngledAreaWidth", fCashGrabAngledAreaWidth, 0, 10, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("vCashGrabAngledAreaOffset1", vCashGrabAngledAreaOffset1, -10.0, 10.0 , 0.01)
				ADD_WIDGET_VECTOR_SLIDER("vCashGrabAngledAreaOffset2", vCashGrabAngledAreaOffset2, -10.0, 10.0 , 0.01)
			STOP_WIDGET_GROUP()
			
			ADD_WIDGET_BOOL("Single Item Cash Grab", bSingleItemCashGrab)
		
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("HACKING")
		
			ADD_WIDGET_VECTOR_SLIDER("vHackAAOffset",vHackAAOffset,-100000,100000,0.001)
			ADD_WIDGET_FLOAT_SLIDER("fThisTweakValue1",fThisTweakValue1,-10,10,0.001)
			ADD_WIDGET_FLOAT_SLIDER("fThisTweakValue2",fThisTweakValue2,-10,10,0.001)
			ADD_WIDGET_VECTOR_SLIDER("vHackOffset",vHackOffset,-100000,100000,0.001)
			
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("SYNC LOCK")
			
			ADD_WIDGET_VECTOR_SLIDER("vThisOffset",vThisOffset,-100000,100000,0.001)
			ADD_WIDGET_VECTOR_SLIDER("vThisOffset",vScenePlayback,-100000,100000,0.001)
			
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("HACKING KEYPAD")
		
			//ADD_WIDGET_VECTOR_SLIDER("vThisLaptopHackOffset",vThisLaptopHackOffset,-100000,100000,0.001)
			//ADD_WIDGET_VECTOR_SLIDER("vThisKeypadHackOffset",vHackScenePlayback,-100000,100000,0.001)
			
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("OTHER HACKING")
		
			//START_WIDGET_GROUP("Hacking Minigame") ADAM BROKE EVERYTHING I HOPE YOU ARE HAPPY
	        ADD_WIDGET_BOOL("b_cibsLEGACY_FMMC_MINI_GAME_IGNORE_CONNECT",b_cibsLEGACY_FMMC_MINI_GAME_IGNORE_CONNECT)
	        ADD_WIDGET_BOOL("b_cibsLEGACY_FMMC_MINI_GAME_IGNORE_BRUTE",b_cibsLEGACY_FMMC_MINI_GAME_IGNORE_CONNECT)
			ADD_WIDGET_BOOL("b_cibsLEGACY_FMMC_MINI_GAME_IGNORE_CONNECT",b_cibsLEGACY_FMMC_MINI_GAME_DIFF_EASY)
	        ADD_WIDGET_BOOL("b_cibsLEGACY_FMMC_MINI_GAME_IGNORE_BRUTE",b_cibsLEGACY_FMMC_MINI_GAME_DIFF_HARD)
			ADD_WIDGET_VECTOR_SLIDER("vHackLaptopGotoOffset ",vHackLaptopGotoOffset ,-1000,1000,0.01)
			ADD_WIDGET_VECTOR_SLIDER("vHackKeypadGotoOffset",vHackKeypadGotoOffset,-100000,100000,0.01)
			ADD_WIDGET_VECTOR_SLIDER("vHackLaptopScenePlayback ",vHackLaptopScenePlayback ,-1000,1000,0.01)
			ADD_WIDGET_VECTOR_SLIDER("vHackKeypadScenePlayback",vHackKeypadScenePlayback,-100000,100000,0.01)
			ADD_WIDGET_VECTOR_SLIDER("vOffsetFromTerminal",vOffsetFromTerminal,-100000,100000,0.001)
			ADD_WIDGET_VECTOR_SLIDER("vHackAAOffset",vHackAAOffset,-100000,100000,0.001)
			ADD_WIDGET_FLOAT_SLIDER("fUseGoToDistance", fUseGoToDistance, -10,10,0.001)
			ADD_WIDGET_FLOAT_SLIDER("fObjRot", fObjRot, -1000,1000,0.001)
			
			ADD_WIDGET_BOOL("Hide Hacking Visuals", g_bHideHackingVisuals)
		
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Gang Chase")
			ADD_WIDGET_BOOL("Gang Chase Debug", bWdGangChaseDebug)
			ADD_WIDGET_BOOL("Lose gang chase debug", bWdLoseGangBackupDebug)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Seat Preference")
			ADD_WIDGET_BOOL("Seat Preference Debug", bWdSeatPreferenceDebug)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Leave area")
			ADD_WIDGET_BOOL("Leave area debug", bWdLocDebug)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Drop off Marker")
			ADD_WIDGET_BOOL("Drop Off marker debug", bWdDropOffMarker)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Multi rule timer")
			ADD_WIDGET_BOOL("Debug multi-rule timer", bWdMultiTimer)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Minimap")
			ADD_WIDGET_FLOAT_SLIDER("X Offset", fMinimapXOffset, -50.0, 50.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("X Offset", fMinimapYOffset, -50.0, 50.0, 0.1)
		STOP_WIDGET_GROUP()
		
		SETUP_CROWD_CONTROL_DEBUG( MC_serverBD_3.sCCServerData, sCCLocalData )
		
		CREATE_OWAIN_WIDGETS()
		
		START_WIDGET_GROUP("Drill Mini-game")
			ADD_WIDGET_INT_SLIDER("Outfit Override", iDebugDrillOutfitCheckOverride, -1, 82, 1)
			ADD_WIDGET_FLOAT_SLIDER("Cam Near DOF Start", fDebugDrillCamNearDofStart, -1.0, 100.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Cam Near DOF End", fDebugDrillCamNearDofEnd, -1.0, 100.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Cam Far DOF Start", fDebugDrillCamFarDofStart, -1.0, 100.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Cam Far DOF End", fDebugDrillCamFarDofEnd, -1.0, 100.0, 0.1)
		STOP_WIDGET_GROUP()
	
		START_WIDGET_GROUP("Thermite Mini-game")
			ADD_WIDGET_BOOL("Enable thermite debug", bDebugIsThermiteDebuggingEnabled)
			ADD_WIDGET_BOOL("Enable thermite anim replaying", bDebugEnableThermiteReplay)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP( "Helicopter Magnet" )
			ADD_WIDGET_BOOL( "Enable Magnet Tuning", bEnableMagnetTuning )
			ADD_WIDGET_FLOAT_SLIDER( "Range", fMagnetRange, 0.0, 200.0, 0.1 )
			ADD_WIDGET_FLOAT_SLIDER( "Far Range Force", fMagnetMin, 0.0, 2.0, 0.05 )
			ADD_WIDGET_FLOAT_SLIDER( "Close Range Force", fMagnetMax, 0.0, 2.0, 0.05 )
		STOP_WIDGET_GROUP()
		
		start_WIDGET_GROUP("activate casco ptfx")
			add_widget_bool("toggle ptfx",  widget_activate_ptfx)
			add_widget_float_slider("widget_ptfx_pos x", widget_ptfx_pos.x, -4000.0, 4000.0, 0.1)
			add_widget_float_slider("widget_ptfx_pos y", widget_ptfx_pos.y, -4000.0, 4000.0, 0.1)
			add_widget_float_slider("widget_ptfx_pos z", widget_ptfx_pos.z, -4000.0, 4000.0, 0.1)
			
			add_widget_float_slider("widget_ptfx_rot x", widget_ptfx_rot.x, -4000.0, 4000.0, 0.1)
			add_widget_float_slider("widget_ptfx_rot y", widget_ptfx_rot.y, -4000.0, 4000.0, 0.1)
			add_widget_float_slider("widget_ptfx_rot z", widget_ptfx_rot.z, -4000.0, 4000.0, 0.1)
		stop_widget_group()
		
		START_WIDGET_GROUP("Cross the Line HUD")
			ADD_WIDGET_BOOL("Cross the Line HUD Prints", bDebugCrossTheLineHUDPrints)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Inch By Inch Goal Target")
			ADD_WIDGET_VECTOR_SLIDER("Centre Offset", vSuddenDeathTargetCentre, -100.0, 100.0, 0.1)
			START_NEW_WIDGET_COMBO()
				ADD_TO_WIDGET_COMBO("Pong")
				ADD_TO_WIDGET_COMBO("Circular")
			STOP_WIDGET_COMBO("Style", iSuddenDeathTargetMoveStyle)
			START_WIDGET_GROUP("Pong")
				ADD_WIDGET_VECTOR_SLIDER("Bounds", vSuddenDeathTargetBounds, -10.0, 10.0, 0.05)
				ADD_WIDGET_VECTOR_SLIDER("Velocity", vSuddenDeathTargetVelocity, -10.0, 10.0, 0.05)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Circular")
				ADD_WIDGET_FLOAT_SLIDER("SinCosScale", fSuddenDeathTargetSinCosScale, -10.0, 10.0, 0.05)
				ADD_WIDGET_FLOAT_SLIDER("SinCosSpeed", fSuddenDeathTargetSinCosSpeed, -10.0, 10.0, 0.05)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP( "Turf War" )
			ADD_WIDGET_BOOL( "Enable Expensive Prop Claiming State Table", bEnableWidgetPropClaimTable )
			ADD_WIDGET_BOOL( "Enable Simple Player Prop Claim Info", bEnableWidgetPropClaimSimple )
			ADD_WIDGET_BOOL( "Draw Shapetest Debug Line", bEnableWidgetPropClaimLine )
			ADD_WIDGET_BOOL( "Draw Prop Owned Numbers", bEnableWidgetPropClaimNumberCount )
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP( "Ped Projectile throwing" )
			ADD_WIDGET_FLOAT_SLIDER("PED_THROW_PROJECTILE_MAX_RANGE", PED_THROWING_MAX_RANGE, 10, 500, 5)
			ADD_WIDGET_FLOAT_SLIDER("PED_THROWING_LEAD_SCALAR", PED_THROWING_LEAD_SCALAR, 0, 100, 1)
			ADD_WIDGET_FLOAT_SLIDER("PED_THROWING_ACCURACY_RADIUS", PED_THROWING_ACCURACY_RADIUS, 0, 100, 1)
			ADD_WIDGET_BOOL("PED_THROWING_DRAW_DEBUG", PED_THROWING_DRAW_DEBUG)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Juggernaut")
			ADD_WIDGET_INT_SLIDER("VFX Interval", ciJUGGERNAUT_VFX_INTERVAL, 0, 10000, 1)
			ADD_WIDGET_VECTOR_SLIDER("VFX Pos", vJuggernautVFXPos, 0.0, 100.0, 0.1)
			ADD_WIDGET_VECTOR_SLIDER("VFX Rot", vJuggernautVFXRot, 0.0, 100.0, 0.1)
			ADD_WIDGET_VECTOR_SLIDER("VFX FIRE1 Pos", vJuggernautVFXFire1Pos, -100, 100.0, 0.1)
			ADD_WIDGET_VECTOR_SLIDER("VFX FIRE1 Rot", vJuggernautVFXFire1Rot, -360, 100.0, 0.1)
			ADD_WIDGET_VECTOR_SLIDER("VFX FIRE2 Pos", vJuggernautVFXFire2Pos, -100, 100.0, 0.1)
			ADD_WIDGET_VECTOR_SLIDER("VFX FIRE2 Rot", vJuggernautVFXFire2Rot, -360, 100.0, 0.1)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP( "Script Spammy Prints" )
			ADD_WIDGET_BOOL( "Enable Spammy Prints", bEnableScriptAudioSpammyPrints )
			ADD_WIDGET_BOOL( "Enable Blip Prints", bDebugBlipPrints )
			ADD_WIDGET_BOOL( "Enable Bounds Prints", bDebugBoundsPrints )	
			ADD_WIDGET_BOOL("Enable Resurrection HUD prints", bDebugRezHUDPrints)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP( "Overtime" )
			ADD_WIDGET_BOOL( "Overtime Zone Markers", bEnableWidgetOvertimeZoneMarkers )
			ADD_WIDGET_BOOL("Overtime Turns Sudden Death", bPutMeInSuddenDeath)
			ADD_WIDGET_BOOL("Zone and Points Test Mode",  bTestOvertimeZones)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Placed Entities")
			ADD_WIDGET_BOOL("Placed Vehicle Health & Info", bPlacedMissionVehiclesDebugHealth)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Arena")
			ADD_WIDGET_BOOL("g_bMissionOver", g_bMissionOver)
			ADD_WIDGET_INT_SLIDER("iDisplaySlot", MC_playerBD_1[iLocalPart].iDisplaySlot, 0, 10000, 1)
			
			CREATE_FMMC_ARENA_WIDGETS()
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
  			  
ENDPROC

PROC MAINTAIN_MISSION_RESTART_DEBUG()
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_Z, KEYBOARD_MODIFIER_CTRL_SHIFT, "NO")
		PRINTLN("[JS] CRTL + SHIFT + Z PRESSED - RESTARTING MISSION")
		g_debugDisplayState = DEBUG_DISP_REPLAY
		g_SkipCelebAndLbd = TRUE
	ENDIF
ENDPROC

FUNC STRING GET_CLIENT_STATE_STRING( INT iClientState )

	STRING toReturn = ""

	SWITCH iClientState
	
		CASE CLIENT_MISSION_STAGE_SETUP
			toReturn = "CLIENT_MISSION_STAGE_SETUP"
		BREAK
		CASE CLIENT_MISSION_STAGE_KILL_PED
			toReturn = "CLIENT_MISSION_STAGE_KILL_PED"
		BREAK
		CASE CLIENT_MISSION_STAGE_PROTECT_PED
			toReturn = "CLIENT_MISSION_STAGE_PROTECT_PED"
		BREAK
		CASE CLIENT_MISSION_STAGE_COLLECT_PED
			toReturn = "CLIENT_MISSION_STAGE_COLLECT_PED"
		BREAK
		CASE CLIENT_MISSION_STAGE_DELIVER_PED
			toReturn = "CLIENT_MISSION_STAGE_DELIVER_PED"
		BREAK
		CASE CLIENT_MISSION_STAGE_KILL_VEH
			toReturn = "CLIENT_MISSION_STAGE_KILL_VEH"
		BREAK
		CASE CLIENT_MISSION_STAGE_PROTECT_VEH
			toReturn = "CLIENT_MISSION_STAGE_PROTECT_VEH"
		BREAK
		CASE CLIENT_MISSION_STAGE_COLLECT_VEH
			toReturn = "CLIENT_MISSION_STAGE_COLLECT_VEH"
		BREAK
		CASE CLIENT_MISSION_STAGE_DELIVER_VEH
			toReturn = "CLIENT_MISSION_STAGE_DELIVER_VEH"
		BREAK
		CASE CLIENT_MISSION_STAGE_KILL_OBJ
			toReturn = "CLIENT_MISSION_STAGE_KILL_OBJ"
		BREAK
		CASE CLIENT_MISSION_STAGE_PROTECT_OBJ
			toReturn = "CLIENT_MISSION_STAGE_PROTECT_OBJ"
		BREAK
		CASE CLIENT_MISSION_STAGE_COLLECT_OBJ
			toReturn = "CLIENT_MISSION_STAGE_COLLECT_OBJ"
		BREAK
		CASE CLIENT_MISSION_STAGE_DELIVER_OBJ
			toReturn = "CLIENT_MISSION_STAGE_DELIVER_OBJ"
		BREAK
		CASE CLIENT_MISSION_STAGE_GOTO_LOC
			toReturn = "CLIENT_MISSION_STAGE_GOTO_LOC"
		BREAK
		CASE CLIENT_MISSION_STAGE_CONTROL_AREA
			toReturn = "CLIENT_MISSION_STAGE_CONTROL_AREA"
		BREAK
		CASE CLIENT_MISSION_STAGE_CONTROL_PED
			toReturn = "CLIENT_MISSION_STAGE_CONTROL_PED"
		BREAK
		CASE CLIENT_MISSION_STAGE_CONTROL_VEH
			toReturn = "CLIENT_MISSION_STAGE_CONTROL_VEH"
		BREAK
		CASE CLIENT_MISSION_STAGE_CONTROL_OBJ
			toReturn = "CLIENT_MISSION_STAGE_CONTROL_OBJ"
		BREAK
		CASE CLIENT_MISSION_STAGE_GOTO_PED
			toReturn = "CLIENT_MISSION_STAGE_GOTO_PED"
		BREAK
		CASE CLIENT_MISSION_STAGE_GOTO_VEH
			toReturn = "CLIENT_MISSION_STAGE_GOTO_VEH"
		BREAK
		CASE CLIENT_MISSION_STAGE_GOTO_OBJ
			toReturn = "CLIENT_MISSION_STAGE_GOTO_OBJ"
		BREAK
		CASE CLIENT_MISSION_STAGE_KILL_PLAYERS
			toReturn = "CLIENT_MISSION_STAGE_KILL_PLAYERS"
		BREAK
		CASE CLIENT_MISSION_STAGE_KILL_TEAM0
			toReturn = "CLIENT_MISSION_STAGE_KILL_TEAM0"
		BREAK
		CASE CLIENT_MISSION_STAGE_KILL_TEAM1
			toReturn = "CLIENT_MISSION_STAGE_KILL_TEAM1"
		BREAK
		CASE CLIENT_MISSION_STAGE_KILL_TEAM2
			toReturn = "CLIENT_MISSION_STAGE_KILL_TEAM2"
		BREAK
		CASE CLIENT_MISSION_STAGE_KILL_TEAM3
			toReturn = "CLIENT_MISSION_STAGE_KILL_TEAM3"
		BREAK
		CASE CLIENT_MISSION_STAGE_PHOTO_LOC
			toReturn = "CLIENT_MISSION_STAGE_PHOTO_LOC"
		BREAK
		CASE CLIENT_MISSION_STAGE_PHOTO_PED
			toReturn = "CLIENT_MISSION_STAGE_PHOTO_PED"
		BREAK
		CASE CLIENT_MISSION_STAGE_PHOTO_VEH
			toReturn = "CLIENT_MISSION_STAGE_PHOTO_VEH"
		BREAK
		CASE CLIENT_MISSION_STAGE_PHOTO_OBJ
			toReturn = "CLIENT_MISSION_STAGE_PHOTO_OBJ"
		BREAK
		CASE CLIENT_MISSION_STAGE_HACK_COMP
			toReturn = "CLIENT_MISSION_STAGE_HACK_COMP"
		BREAK
		CASE CLIENT_MISSION_STAGE_HACK_SAFE
			toReturn = "CLIENT_MISSION_STAGE_HACK_SAFE"
		BREAK
		CASE CLIENT_MISSION_STAGE_HACK_SYNC_LOCK
			toReturn = "CLIENT_MISSION_STAGE_HACK_SYNC_LOCK"
		BREAK
		CASE CLIENT_MISSION_STAGE_HACK_OPEN_CASE
			toReturn = "CLIENT_MISSION_STAGE_HACK_OPEN_CASE"
		BREAK
		CASE CLIENT_MISSION_STAGE_OPEN_CONTAINER
			toReturn = "CLIENT_MISSION_STAGE_OPEN_CONTAINER"
		BREAK
		CASE CLIENT_MISSION_STAGE_CASH_GRAB
			toReturn = "CLIENT_MISSION_STAGE_CASH_GRAB"
		BREAK
		CASE CLIENT_MISSION_STAGE_BLOW_DOOR
			toReturn = "CLIENT_MISSION_STAGE_BLOW_DOOR"
		BREAK
		CASE CLIENT_MISSION_STAGE_ENTER_SCRIPTED_TURRET
			toReturn = "CLIENT_MISSION_STAGE_ENTER_SCRIPTED_TURRET"
		BREAK
		CASE CLIENT_MISSION_STAGE_GET_MASK
			toReturn = "CLIENT_MISSION_STAGE_GET_MASK"
		BREAK
		CASE CLIENT_MISSION_STAGE_SCRIPTED_CUTSCENE
			toReturn = "CLIENT_MISSION_STAGE_SCRIPTED_CUTSCENE"
		BREAK
		CASE CLIENT_MISSION_STAGE_CROWD_CONTROL
			toReturn = "CLIENT_MISSION_STAGE_CROWD_CONTROL"
		BREAK
		CASE CLIENT_MISSION_STAGE_CHARM_PED
			toReturn = "CLIENT_MISSION_STAGE_CHARM_PED"
		BREAK
		CASE CLIENT_MISSION_STAGE_ARREST_ALL
			toReturn = "CLIENT_MISSION_STAGE_ARREST_ALL"
		BREAK
		CASE CLIENT_MISSION_STAGE_ARREST_TEAM0
			toReturn = "CLIENT_MISSION_STAGE_ARREST_TEAM0"
		BREAK
		CASE CLIENT_MISSION_STAGE_ARREST_TEAM1
			toReturn = "CLIENT_MISSION_STAGE_ARREST_TEAM1"
		BREAK
		CASE CLIENT_MISSION_STAGE_ARREST_TEAM2
			toReturn = "CLIENT_MISSION_STAGE_ARREST_TEAM2"
		BREAK
		CASE CLIENT_MISSION_STAGE_ARREST_TEAM3
			toReturn = "CLIENT_MISSION_STAGE_ARREST_TEAM3"
		BREAK
		CASE CLIENT_MISSION_STAGE_LEAVE_LOC
			toReturn = "CLIENT_MISSION_STAGE_LEAVE_LOC"
		BREAK
		CASE CLIENT_MISSION_STAGE_PHONE_BULLSHARK
			toReturn = "CLIENT_MISSION_STAGE_PHONE_BULLSHARK"
		BREAK
		CASE CLIENT_MISSION_STAGE_PHONE_AIRSTRIKE
			toReturn = "CLIENT_MISSION_STAGE_PHONE_AIRSTRIKE"
		BREAK
		CASE CLIENT_MISSION_STAGE_HACK_DRILL
			toReturn = "CLIENT_MISSION_STAGE_HACK_DRILL"
		BREAK
		CASE CLIENT_MISSION_STAGE_GOTO_ANY_PLAYER
			toReturn = "CLIENT_MISSION_STAGE_GOTO_ANY_PLAYER"
		BREAK
		CASE CLIENT_MISSION_STAGE_GOTO_TEAM0
			toReturn = "CLIENT_MISSION_STAGE_GOTO_TEAM0"
		BREAK
		CASE CLIENT_MISSION_STAGE_GOTO_TEAM1
			toReturn = "CLIENT_MISSION_STAGE_GOTO_TEAM1"
		BREAK
		CASE CLIENT_MISSION_STAGE_GOTO_TEAM2
			toReturn = "CLIENT_MISSION_STAGE_GOTO_TEAM2"
		BREAK
		CASE CLIENT_MISSION_STAGE_GOTO_TEAM3
			toReturn = "CLIENT_MISSION_STAGE_GOTO_TEAM3"
		BREAK
		
		CASE CLIENT_MISSION_STAGE_RESULTS
			toReturn = "CLIENT_MISSION_STAGE_RESULTS"
		BREAK
		CASE CLIENT_MISSION_STAGE_TERMINATE_DELAY
			toReturn = "CLIENT_MISSION_STAGE_TERMINATE_DELAY"
		BREAK
		CASE CLIENT_MISSION_STAGE_END
			toReturn = "CLIENT_MISSION_STAGE_END"
		BREAK

		DEFAULT
			toReturn = "INVALID_CLIENT_MISSION_STAGE"
		BREAK
	
	ENDSWITCH
	
	RETURN toReturn

ENDFUNC

FUNC STRING GET_GAME_STATE_STRING( INT iGameState )
	STRING toReturn = ""
	
	SWITCH iGameState
		CASE GAME_STATE_INI
			toReturn = "GAME_STATE_INI"
		BREAK
		
		CASE GAME_STATE_APARTMENT_WALKOUT
			toReturn = "GAME_STATE_APARTMENT_WALKOUT"
		BREAK
		
		CASE GAME_STATE_CAMERA_BLEND
			toReturn = "GAME_STATE_CAMERA_BLEND"
		BREAK
		
		CASE GAME_STATE_END
			toReturn = "GAME_STATE_END"
		BREAK
		
		CASE GAME_STATE_FORCED_TRIP_SKIP
			toReturn = "GAME_STATE_FORCED_TRIP_SKIP"
		BREAK
		
		CASE GAME_STATE_HEIST_INTRO
			toReturn = "GAME_STATE_HEIST_INTRO"
		BREAK
		
		CASE GAME_STATE_INTRO_CUTSCENE
			toReturn = "GAME_STATE_INTRO_CUTSCENE"
		BREAK
		
		CASE GAME_STATE_LEAVE
			toReturn = "GAME_STATE_LEAVE"
		BREAK
		
		CASE GAME_STATE_MISSION_OVER
			toReturn = "GAME_STATE_MISSION_OVER"
		BREAK
		
		CASE GAME_STATE_RUNNING
			toReturn = "GAME_STATE_RUNNING"
		BREAK
		
		CASE GAME_STATE_SOLO_PLAY
			toReturn = "GAME_STATE_SOLO_PLAY"
		BREAK
		
		CASE GAME_STATE_TEAM_PLAY
			toReturn = "GAME_STATE_TEAM_PLAY"
		BREAK
		
		CASE GAME_STATE_TUTORIAL_CUTSCENE
			toReturn = "GAME_STATE_TUTORIAL_CUTSCENE"
		BREAK
		
		DEFAULT
			toReturn = "INVALID_GAME_STATE"
		BREAK
	ENDSWITCH
	
	RETURN toReturn
ENDFUNC

CONST_INT ciMAX_RECORDED_GAME_STATES 	3
CONST_INT ciMAX_RECORDED_CLIENT_STATES 	4

INT iGameStates[ ciMAX_RECORDED_GAME_STATES ]
INT iClientStates[ ciMAX_RECORDED_CLIENT_STATES ]
INT iPreviousRule = -2

ENUM eControllerDebugState
	eControllerDebugState_OFF,
	eControllerDebugState_OVERVIEW,
	eControllerDebugState_APARTMENTS,
	eControllerDebugState_CUTSCENES,
	eControllerDebugState_CAPTURELOC,
	eControllerDebugState_POINTLESS,
	eControllerDebugState_ENTITYSTUFF,
	eControllerDebugState_PLAYER,
	eControllerDebugState_VEHICLES,
	eControllerDebugState_END
ENDENUM

eControllerDebugState debugState = eControllerDebugState_OFF

// Controls the debug menu
PROC HANDLE_DEBUG_WINDOW_KEYBOARD()
	IF IS_KEYBOARD_KEY_JUST_PRESSED( KEY_RIGHT )
		SWITCH debugState
			CASE eControllerDebugState_OVERVIEW
				debugState = eControllerDebugState_APARTMENTS
			BREAK
			
			CASE eControllerDebugState_APARTMENTS
				debugState = eControllerDebugState_CUTSCENES
			BREAK
			
			CASE eControllerDebugState_CUTSCENES
				debugState = eControllerDebugState_CAPTURELOC
			BREAK
			
			CASE eControllerDebugState_CAPTURELOC
				debugState = eControllerDebugState_POINTLESS
			BREAK
			
			CASE eControllerDebugState_POINTLESS
				debugState = eControllerDebugState_ENTITYSTUFF
			BREAK
			
			CASE eControllerDebugState_ENTITYSTUFF
				debugState = eControllerDebugState_PLAYER
			BREAK
			
			CASE eControllerDebugState_PLAYER
				debugState = eControllerDebugState_VEHICLES
			BREAK
			
			CASE eControllerDebugState_VEHICLES
				debugState = eControllerDebugState_END
			BREAK
			
			CASE eControllerDebugState_END
				debugState = eControllerDebugState_OVERVIEW
			BREAK
			
		ENDSWITCH
	ELIF IS_KEYBOARD_KEY_JUST_PRESSED( KEY_LEFT )
		SWITCH debugState
			CASE eControllerDebugState_OVERVIEW
				debugState = eControllerDebugState_END
			BREAK
			
			CASE eControllerDebugState_APARTMENTS
				debugState = eControllerDebugState_OVERVIEW
			BREAK
			
			CASE eControllerDebugState_CUTSCENES
				debugState = eControllerDebugState_APARTMENTS
			BREAK
			
			CASE eControllerDebugState_CAPTURELOC
				debugState = eControllerDebugState_CUTSCENES
			BREAK
			
			CASE eControllerDebugState_POINTLESS
				debugState = eControllerDebugState_CAPTURELOC
			BREAK
			
			CASE eControllerDebugState_ENTITYSTUFF
				debugState = eControllerDebugState_POINTLESS
			BREAK
			
			CASE eControllerDebugState_PLAYER
				debugState = eControllerDebugState_ENTITYSTUFF
			BREAK
			
			CASE eControllerDebugState_VEHICLES
				debugState = eControllerDebugState_PLAYER
			BREAK
			
			CASE eControllerDebugState_END
				debugState = eControllerDebugState_VEHICLES
			BREAK
			
		ENDSWITCH
	ENDIF
ENDPROC
PROC MAINTAIN_DEBUG_TURFWARS_INFO_PROP_CACHE(INT iPlayerPart = 0)
	// Draw BG for Window
	FLOAT fTextSize = 0.3
	TEXT_LABEL_63 sText
	
	DRAW_RECT(0.925, 0.175, 0.23, 0.175, 25, 25, 25, 200)
	
	SET_TEXT_SCALE(fTextSize, fTextSize)
	SET_TEXT_COLOUR(50, 225, 50, 200)
	sText = "Prop Local Player Cache Window"
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.82, 0.15, "STRING", sText)

	SET_TEXT_SCALE(fTextSize, fTextSize)
	SET_TEXT_COLOUR(50, 225, 50, 200)
	sText = "Standing on Prop: "
	sText += GET_STRING_FROM_INT(MC_PlayerBD[iPlayerPart].iCurrentPropHit[0])
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.82, 0.18, "STRING", sText)

	SET_TEXT_SCALE(fTextSize, fTextSize)
	SET_TEXT_COLOUR(50, 225, 50, 200)
	sText = "iLocalPropLastBroadcast: "
	sText += GET_STRING_FROM_INT(iLocalPropLastBroadcast)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.82, 0.22, "STRING", sText)
ENDPROC

PROC MAINTAIN_DEBUG_TURFWARS_OWNERSHIP_NUMBERS()
	TEXT_LABEL_31 sText
	FLOAT fTextSize = 0.35
		
	DRAW_RECT(0.475, 0.15, 0.17, 0.125, 25, 25, 25, 200)
	SET_TEXT_SCALE(fTextSize, fTextSize)
	SET_TEXT_COLOUR(50, 225, 50, 200)
	sText = "Prop Team Numbers"
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.4, 0.085, "STRING", sText)
	
	SET_TEXT_SCALE(fTextSize, fTextSize)
	SET_TEXT_COLOUR(50, 225, 50, 200)
	sText = "Team 0:   "
	sText += MC_ServerBD_4.iPropsAmountOwnedByTeam[0]
	sText += " / "
	sText += iTurfWarTotalClaimableProps
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.4, 0.1, "STRING", sText)
	
	SET_TEXT_SCALE(fTextSize, fTextSize)
	SET_TEXT_COLOUR(50, 225, 50, 200)
	sText = "Team 1:    "
	sText += MC_ServerBD_4.iPropsAmountOwnedByTeam[1]
	sText += " / "
	sText += iTurfWarTotalClaimableProps
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.4, 0.115, "STRING", sText)
	
	SET_TEXT_SCALE(fTextSize, fTextSize)
	SET_TEXT_COLOUR(50, 225, 50, 200)
	sText = "Team 2:   "
	sText += MC_ServerBD_4.iPropsAmountOwnedByTeam[2]
	sText += " / "
	sText += iTurfWarTotalClaimableProps
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.4, 0.130, "STRING", sText)
	
	SET_TEXT_SCALE(fTextSize, fTextSize)
	SET_TEXT_COLOUR(50, 225, 50, 200)
	sText = "Team 3:   "
	sText += MC_ServerBD_4.iPropsAmountOwnedByTeam[3]
	sText += " / "
	sText += iTurfWarTotalClaimableProps
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.4, 0.145, "STRING", sText)
ENDPROC

PROC PRINT_PROP_CLAIMING_DATA()
	PRINTLN("#########################################################################################")
	PRINTLN("#########################################################################################")
	PRINTLN("#########################################################################################")
	PRINTLN("[LM][PRINT_PROP_CLAIMING_DATA] - SHIFT 9 Pressed. Prepare to spit out PROP CLAIMING DATA!")
	
	INT iProp
	FOR iProp = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfProps-1
		SWITCH MC_ServerBD_4.ePropClaimState[iProp]
			CASE ePropClaimingState_Unclaimed
				PRINTLN("[LM][TURF WAR][PRINT_PROP_CLAIMING_DATA] - iProp: ", iProp, " State: ePropClaimingState_Unclaimed", " For Model: ", GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn))
			BREAK		
			CASE ePropClaimingState_Claimed_Team_0
				PRINTLN("[LM][TURF WAR][PRINT_PROP_CLAIMING_DATA] - iProp: ", iProp, "State: ePropClaimingState_Claimed for Team: ", 0, " For Model: ", GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn))
			BREAK
			CASE ePropClaimingState_Claimed_Team_1
				PRINTLN("[LM][TURF WAR][PRINT_PROP_CLAIMING_DATA] - iProp: ", iProp, "State: ePropClaimingState_Claimed for Team: ", 1, " For Model: ", GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn))
			BREAK
			CASE ePropClaimingState_Claimed_Team_2
				PRINTLN("[LM][TURF WAR][PRINT_PROP_CLAIMING_DATA] - iProp: ", iProp, "State: ePropClaimingState_Claimed for Team: ", 2, " For Model: ", GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn))
			BREAK
			CASE ePropClaimingState_Claimed_Team_3
				PRINTLN("[LM][TURF WAR][PRINT_PROP_CLAIMING_DATA] - iProp: ", iProp, "State: ePropClaimingState_Claimed for Team: ", 3, " For Model: ", GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn))
			BREAK		
			CASE ePropClaimingState_Contested
				PRINTLN("[LM][TURF WAR][PRINT_PROP_CLAIMING_DATA] - iProp: ", iProp, "State: ePropClaimingState_Contested", " For Model: ", GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn))
			BREAK
			CASE ePropClaimingState_Disabled
				PRINTLN("[LM][TURF WAR][PRINT_PROP_CLAIMING_DATA] - iProp: ", iProp, "State: ePropClaimingState_Disabled", " For Model: ", GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn))
			BREAK
		ENDSWITCH
	ENDFOR
	
	PRINTLN("[LM][PRINT_PROP_CLAIMING_DATA] - That is end.")
	PRINTLN("#########################################################################################")	
	PRINTLN("#########################################################################################")
ENDPROC

// GET_CHANGE_PROP_COLOUR_TURF_WAR
PROC ASSIGN_TURF_WAR_COLOURS_FOR_DEBUG(INT iColourIndex, INT &r, INT &g, INT &b)
	SWITCH iColourIndex
		// Orange
		CASE 0			
			r = 200 g = 125 b = 50
		BREAK
		
		// Purple
		CASE 1			
			r = 150 g = 50 b = 200
		BREAK
		
		// Pink
		CASE 2			
			r = 255 g = 20 b = 147
		BREAK
		
		// Green
		CASE 3			
			r = 50 g = 255 b = 50
		BREAK
		
		// Red
		CASE 4			
			r = 255 g = 0 b = 0
		BREAK
		
		// Blue
		CASE 5			
			r = 25 g = 25 b = 200
		BREAK
		
		// Grey
		CASE 6			
			r = 100 g = 100 b = 100
		BREAK
		
		// White
		CASE 7			
			r = 255 g = 255 b = 255
		BREAK
		
		// Yellow
		CASE 8			
			r = 150 g = 150 b = 0
		BREAK
		
		// Black
		CASE 9			
			r = 25 g = 25 b = 25
		BREAK
		
		// ??
		CASE 10			
			
		BREAK
	ENDSWITCH
		
ENDPROC

PROC MAINTAIN_DEBUG_TURFWARS_INFO_PROP_STATES(INT iProp)
	// Draw BG for Window
	FLOAT fSpacing = 0.01275
	FLOAT fTextSize = 0.175
	FLOAT fInc = 0.38
	FLOAT fIncY = ((iProp+1)*fSpacing)
	FLOAT fYOffset = 0.07
	TEXT_LABEL_31 sText
	TEXT_LABEL_7 sText2
	
	IF iProp = 0
		DRAW_RECT(0.69, 0.375+fYOffset, 0.6, 0.75, 25, 25, 25, 200)
		SET_TEXT_SCALE(0.3, 0.3)
		SET_TEXT_COLOUR(50, 225, 50, 200)
		sText = "Prop Claiming State Window"
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.6, 0.0+fYOffset, "STRING", sText)
	ENDIF	
	
	IF iProp > 100
		fInc = 0.78
		fIncY -= (fSpacing*100)
	ELIF iProp > 50
		fInc = 0.58
		fIncY -= (fSpacing*50)
	ENDIF
	
	SET_TEXT_SCALE(fTextSize, fTextSize)
	SET_TEXT_COLOUR(225, 225, 225, 200)
	sText = GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn)
	//DISPLAY_TEXT_WITH_LITERAL_STRING(0.025+fInc, 0.01325+fIncY, "STRING", TEXT_LABEL_TO_STRING(sText))
	DRAW_DEBUG_TEXT_2D(sText, <<0.02+fInc, 0.0155+fIncY+fYOffset, 1.0>>, 170, 170, 170, 200)
	
	SET_TEXT_SCALE(fTextSize, fTextSize)
	SET_TEXT_COLOUR(225, 225, 225, 200)
	sText2 += ""
	sText2 += GET_STRING_FROM_INT(iProp)
	//DISPLAY_TEXT_WITH_LITERAL_STRING(0.118+fInc, 0.01325+fIncY, "STRING", TEXT_LABEL_TO_STRING(sText2))
	DRAW_DEBUG_TEXT_2D(sText2, <<0.165+fInc, 0.0155+fIncY+fYOffset, 1.0>>, 255, 255, 0, 200)
	
	INT r,g,b
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_Is_Claimable)
		SWITCH MC_ServerBD_4.ePropClaimState[iProp]
			CASE ePropClaimingState_Unclaimed
				ASSIGN_TURF_WAR_COLOURS_FOR_DEBUG(g_FMMC_STRUCT.iTurfWarColourUnclaimed, r, g, b)
				DRAW_RECT(0.19+fInc, 0.02+fIncY+fYOffset, 0.012, 0.012, r, g, b, 255)		
			BREAK

			CASE ePropClaimingState_Claimed_Team_0			
				ASSIGN_TURF_WAR_COLOURS_FOR_DEBUG(g_FMMC_STRUCT.iTurfWarColourTeam1, r, g, b)
				DRAW_RECT(0.19+fInc, 0.02+fIncY+fYOffset, 0.012, 0.012, r, g, b, 255)
				DRAW_DEBUG_TEXT_2D(sText, <<0.19+fInc, 0.02+fIncY+fYOffset, 1.0>>, 255, 255, 0, 200)				
			BREAK
			
			CASE ePropClaimingState_Claimed_Team_1			
				ASSIGN_TURF_WAR_COLOURS_FOR_DEBUG(g_FMMC_STRUCT.iTurfWarColourTeam2, r, g, b)
				DRAW_RECT(0.19+fInc, 0.02+fIncY+fYOffset, 0.012, 0.012, r, g, b, 255)
				DRAW_DEBUG_TEXT_2D(sText, <<0.19+fInc, 0.02+fIncY+fYOffset, 1.0>>, 255, 255, 0, 200)
			BREAK
			
			CASE ePropClaimingState_Claimed_Team_2			
				ASSIGN_TURF_WAR_COLOURS_FOR_DEBUG(g_FMMC_STRUCT.iTurfWarColourTeam3, r, g, b)
				DRAW_RECT(0.19+fInc, 0.02+fIncY+fYOffset, 0.012, 0.012, r, g, b, 255)
				DRAW_DEBUG_TEXT_2D(sText, <<0.19+fInc, 0.02+fIncY+fYOffset, 1.0>>, 255, 255, 0, 200)
			BREAK				
			
			CASE ePropClaimingState_Claimed_Team_3			
				ASSIGN_TURF_WAR_COLOURS_FOR_DEBUG(g_FMMC_STRUCT.iTurfWarColourTeam4, r, g, b)
				DRAW_RECT(0.19+fInc, 0.02+fIncY+fYOffset, 0.012, 0.012, r, g, b, 255)
				DRAW_DEBUG_TEXT_2D(sText, <<0.19+fInc, 0.02+fIncY+fYOffset, 1.0>>, 255, 255, 0, 200)
			BREAK
			
			CASE ePropClaimingState_Contested				
				ASSIGN_TURF_WAR_COLOURS_FOR_DEBUG(g_FMMC_STRUCT.iTurfWarColourContested, r, g, b)
				DRAW_RECT(0.19+fInc, 0.02+fIncY+fYOffset, 0.012, 0.012, r, g, b, 255)
			BREAK
			
			CASE ePropClaimingState_Disabled				
				ASSIGN_TURF_WAR_COLOURS_FOR_DEBUG(g_FMMC_STRUCT.iTurfWarColourDisabled, r, g, b)
				DRAW_RECT(0.19+fInc, 0.02+fIncY+fYOffset, 0.012, 0.012, r, g, b, 255)	
			BREAK
		ENDSWITCH
	ELSE
		DRAW_RECT(0.19+fInc, 0.02+fIncY+fYOffset, 0.012, 0.012, 0, 0, 0, 255)
	ENDIF
	
	IF iProp = 0
		sText = "Standing on Prop: "
		sText += MC_PlayerBD[iLocalPart].iCurrentPropHit[0]
		DRAW_DEBUG_TEXT_2D(sText, <<0.40, 0.78, 1.0>>, 255, 255, 0, 200)
			
		sText = "Team 0: "
		sText += MC_ServerBD_4.iPropsAmountOwnedByTeam[0]
		sText += " / "
		sText += iTurfWarTotalClaimableProps
		DRAW_DEBUG_TEXT_2D(sText, <<0.55, 0.78, 1.0>>, 255, 255, 0, 200)
		
		sText = "Team 1:  "
		sText += MC_ServerBD_4.iPropsAmountOwnedByTeam[1]
		sText += " / "
		sText += iTurfWarTotalClaimableProps
		DRAW_DEBUG_TEXT_2D(sText, <<0.65, 0.78, 1.0>>, 255, 255, 0, 200)
		
		sText = "Team 2: "
		sText += MC_ServerBD_4.iPropsAmountOwnedByTeam[2]
		sText += " / "
		sText += iTurfWarTotalClaimableProps
		DRAW_DEBUG_TEXT_2D(sText, <<0.75, 0.78, 1.0>>, 255, 255, 0, 200)
		
		sText = "Team 3: "
		sText += MC_ServerBD_4.iPropsAmountOwnedByTeam[3]
		sText += " / "
		sText += iTurfWarTotalClaimableProps
		DRAW_DEBUG_TEXT_2D(sText, <<0.85, 0.78, 1.0>>, 255, 255, 0, 200)
			
		SET_TEXT_SCALE(fTextSize, fTextSize)
		SET_TEXT_COLOUR(200, 200, 200, 200)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.52, 0.725+fYOffset, "STRING", "Unclaimed")
		ASSIGN_TURF_WAR_COLOURS_FOR_DEBUG(g_FMMC_STRUCT.iTurfWarColourUnclaimed, r, g, b)
		DRAW_RECT(0.53, 0.745+fYOffset, 0.02, 0.0075, r, g, b, 255)
		
		SET_TEXT_SCALE(fTextSize, fTextSize)
		SET_TEXT_COLOUR(200, 200, 200, 200)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.57, 0.725+fYOffset, "STRING", "Claimed Team 0")
		ASSIGN_TURF_WAR_COLOURS_FOR_DEBUG(g_FMMC_STRUCT.iTurfWarColourTeam1, r, g, b)
		DRAW_RECT(0.58, 0.745+fYOffset, 0.02, 0.0075, r, g, b, 255)
		
		SET_TEXT_SCALE(fTextSize, fTextSize)
		SET_TEXT_COLOUR(200, 200, 200, 200)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.62, 0.725+fYOffset, "STRING", "Claimed Team 1")
		ASSIGN_TURF_WAR_COLOURS_FOR_DEBUG(g_FMMC_STRUCT.iTurfWarColourTeam2, r, g, b)
		DRAW_RECT(0.63, 0.745+fYOffset, 0.02, 0.0075, r, g, b, 255)
		
		SET_TEXT_SCALE(fTextSize, fTextSize)
		SET_TEXT_COLOUR(200, 200, 200, 200)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.67, 0.725+fYOffset, "STRING", "Claimed Team 2")
		ASSIGN_TURF_WAR_COLOURS_FOR_DEBUG(g_FMMC_STRUCT.iTurfWarColourTeam3, r, g, b)
		DRAW_RECT(0.68, 0.745+fYOffset, 0.02, 0.0075, r, g, b, 255)
		
		SET_TEXT_SCALE(fTextSize, fTextSize)
		SET_TEXT_COLOUR(200, 200, 200, 200)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.72, 0.725+fYOffset, "STRING", "Claimed Team 3")
		ASSIGN_TURF_WAR_COLOURS_FOR_DEBUG(g_FMMC_STRUCT.iTurfWarColourTeam4, r, g, b)
		DRAW_RECT(0.73, 0.745+fYOffset, 0.02, 0.0075, r, g, b, 255)
		
		SET_TEXT_SCALE(fTextSize, fTextSize)
		SET_TEXT_COLOUR(200, 200, 200, 200)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.77, 0.725+fYOffset, "STRING", "Contested")
		ASSIGN_TURF_WAR_COLOURS_FOR_DEBUG(g_FMMC_STRUCT.iTurfWarColourContested, r, g, b)
		DRAW_RECT(0.78, 0.745+fYOffset, 0.02, 0.0075, r, g, b, 255)
		
		SET_TEXT_SCALE(fTextSize, fTextSize)
		SET_TEXT_COLOUR(200, 200, 200, 200)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.82, 0.725+fYOffset, "STRING", "Unclaimable")
		ASSIGN_TURF_WAR_COLOURS_FOR_DEBUG(g_FMMC_STRUCT.iTurfWarColourDisabled, r, g, b)
		DRAW_RECT(0.83, 0.745+fYOffset, 0.02, 0.0075, r, g, b, 255)
	ENDIF
ENDPROC

PROC MAINTAIN_DEBUG_TURFWARS_INFO_PROP_STATES_CONDENSED(INT iProp)
	// Draw BG for Window
	FLOAT fSpacing = 0.01275
	FLOAT fTextSize = 0.175
	FLOAT fInc = 0.38
	FLOAT fIncY = ((iProp+1)*fSpacing)
	FLOAT fYOffset = 0.07
	FLOAT fXOffsetOther = 0.14
	TEXT_LABEL_31 sText
	TEXT_LABEL_7 sText2
	
	IF iProp = 0
		DRAW_RECT(0.79, 0.375+fYOffset, 0.4, 0.75, 25, 25, 25, 140)
		SET_TEXT_SCALE(0.3, 0.3)
		SET_TEXT_COLOUR(50, 225, 50, 200)
		sText = "Prop Claiming State Window"
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.6, 0.0+fYOffset, "STRING", sText)
	ENDIF	
	
	IF iProp > 100
		fInc = 0.65
		fIncY -= (fSpacing*100)
	ELIF iProp > 50
		fInc = 0.5
		fIncY -= (fSpacing*50)
	ENDIF
	
	SET_TEXT_SCALE(fTextSize, fTextSize)
	SET_TEXT_COLOUR(225, 225, 225, 200)
	sText2 += ""
	sText2 += GET_STRING_FROM_INT(iProp)
	DRAW_DEBUG_TEXT_2D(sText2, <<0.225+fInc, 0.0155+fIncY+fYOffset, 1.0>>, 255, 255, 0, 200)
	
	INT r,g,b
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_Is_Claimable)
		SWITCH MC_ServerBD_4.ePropClaimState[iProp]
			CASE ePropClaimingState_Unclaimed
				ASSIGN_TURF_WAR_COLOURS_FOR_DEBUG(g_FMMC_STRUCT.iTurfWarColourUnclaimed, r, g, b)
				DRAW_RECT(0.26+fInc, 0.02+fIncY+fYOffset, 0.012, 0.012, r, g, b, 255)		
			BREAK

			CASE ePropClaimingState_Claimed_Team_0			
				ASSIGN_TURF_WAR_COLOURS_FOR_DEBUG(g_FMMC_STRUCT.iTurfWarColourTeam1, r, g, b)
				DRAW_RECT(0.26+fInc, 0.02+fIncY+fYOffset, 0.012, 0.012, r, g, b, 255)
				DRAW_DEBUG_TEXT_2D(sText, <<0.19+fInc, 0.02+fIncY+fYOffset, 1.0>>, 255, 255, 0, 200)
			BREAK
			
			CASE ePropClaimingState_Claimed_Team_1			
				ASSIGN_TURF_WAR_COLOURS_FOR_DEBUG(g_FMMC_STRUCT.iTurfWarColourTeam2, r, g, b)
				DRAW_RECT(0.26+fInc, 0.02+fIncY+fYOffset, 0.012, 0.012, r, g, b, 255)	
				DRAW_DEBUG_TEXT_2D(sText, <<0.19+fInc, 0.02+fIncY+fYOffset, 1.0>>, 255, 255, 0, 200)
			BREAK
			
			CASE ePropClaimingState_Claimed_Team_2			
				ASSIGN_TURF_WAR_COLOURS_FOR_DEBUG(g_FMMC_STRUCT.iTurfWarColourTeam3, r, g, b)
				DRAW_RECT(0.26+fInc, 0.02+fIncY+fYOffset, 0.012, 0.012, r, g, b, 255)
				DRAW_DEBUG_TEXT_2D(sText, <<0.19+fInc, 0.02+fIncY+fYOffset, 1.0>>, 255, 255, 0, 200)
			BREAK				
			
			CASE ePropClaimingState_Claimed_Team_3			
				ASSIGN_TURF_WAR_COLOURS_FOR_DEBUG(g_FMMC_STRUCT.iTurfWarColourTeam4, r, g, b)
				DRAW_RECT(0.26+fInc, 0.02+fIncY+fYOffset, 0.012, 0.012, r, g, b, 255)
				DRAW_DEBUG_TEXT_2D(sText, <<0.19+fInc, 0.02+fIncY+fYOffset, 1.0>>, 255, 255, 0, 200)
			BREAK
			
			CASE ePropClaimingState_Contested				
				ASSIGN_TURF_WAR_COLOURS_FOR_DEBUG(g_FMMC_STRUCT.iTurfWarColourContested, r, g, b)
				DRAW_RECT(0.26+fInc, 0.02+fIncY+fYOffset, 0.012, 0.012, r, g, b, 255)
			BREAK
			
			CASE ePropClaimingState_Disabled				
				ASSIGN_TURF_WAR_COLOURS_FOR_DEBUG(g_FMMC_STRUCT.iTurfWarColourDisabled, r, g, b)
				DRAW_RECT(0.26+fInc, 0.02+fIncY+fYOffset, 0.012, 0.012, r, g, b, 255)				
			BREAK
		ENDSWITCH
		
		INT iColourA = GET_OBJECT_TINT_INDEX(oiProps[iProp])
		IF iColourA = GET_CHANGE_PROP_COLOUR_TURF_WAR(g_FMMC_STRUCT.iTurfWarColourUnclaimed)
			ASSIGN_TURF_WAR_COLOURS_FOR_DEBUG(g_FMMC_STRUCT.iTurfWarColourUnclaimed, r, g, b)
			DRAW_RECT(0.29+fInc, 0.02+fIncY+fYOffset, 0.012, 0.012, r, g, b, 255)		
		ELIF iColourA = GET_CHANGE_PROP_COLOUR_TURF_WAR(g_FMMC_STRUCT.iTurfWarColourTeam1)	
			ASSIGN_TURF_WAR_COLOURS_FOR_DEBUG(g_FMMC_STRUCT.iTurfWarColourTeam1, r, g, b)
			DRAW_RECT(0.29+fInc, 0.02+fIncY+fYOffset, 0.012, 0.012, r, g, b, 255)	
		ELIF iColourA = GET_CHANGE_PROP_COLOUR_TURF_WAR(g_FMMC_STRUCT.iTurfWarColourTeam2)	
			ASSIGN_TURF_WAR_COLOURS_FOR_DEBUG(g_FMMC_STRUCT.iTurfWarColourTeam2, r, g, b)
			DRAW_RECT(0.29+fInc, 0.02+fIncY+fYOffset, 0.012, 0.012, r, g, b, 255)		
		ELIF iColourA = GET_CHANGE_PROP_COLOUR_TURF_WAR(g_FMMC_STRUCT.iTurfWarColourTeam3)	
			ASSIGN_TURF_WAR_COLOURS_FOR_DEBUG(g_FMMC_STRUCT.iTurfWarColourTeam3, r, g, b)
			DRAW_RECT(0.29+fInc, 0.02+fIncY+fYOffset, 0.012, 0.012, r, g, b, 255)
		ELIF iColourA = GET_CHANGE_PROP_COLOUR_TURF_WAR(g_FMMC_STRUCT.iTurfWarColourTeam4)
			ASSIGN_TURF_WAR_COLOURS_FOR_DEBUG(g_FMMC_STRUCT.iTurfWarColourTeam4, r, g, b)
			DRAW_RECT(0.29+fInc, 0.02+fIncY+fYOffset, 0.012, 0.012, r, g, b, 255)	
		ELIF iColourA = GET_CHANGE_PROP_COLOUR_TURF_WAR(g_FMMC_STRUCT.iTurfWarColourContested)
			ASSIGN_TURF_WAR_COLOURS_FOR_DEBUG(g_FMMC_STRUCT.iTurfWarColourContested, r, g, b)
			DRAW_RECT(0.29+fInc, 0.02+fIncY+fYOffset, 0.012, 0.012, r, g, b, 255)
		ELIF iColourA = GET_CHANGE_PROP_COLOUR_TURF_WAR(g_FMMC_STRUCT.iTurfWarColourDisabled)
			ASSIGN_TURF_WAR_COLOURS_FOR_DEBUG(g_FMMC_STRUCT.iTurfWarColourDisabled, r, g, b)
			DRAW_RECT(0.29+fInc, 0.02+fIncY+fYOffset, 0.012, 0.012, r, g, b, 255)	
		ELSE
			DRAW_RECT(0.29+fInc, 0.02+fIncY+fYOffset, 0.012, 0.012, 0, 0, 0, 255)
		ENDIF
	ELSE
		DRAW_RECT(0.26+fInc, 0.02+fIncY+fYOffset, 0.012, 0.012, 0, 0, 0, 255)
	ENDIF
	
	IF iProp = 0
		
		sText = "First Block: State"
		DRAW_DEBUG_TEXT_2D(sText, <<0.40, 0.70, 1.0>>, 255, 255, 0, 200)
		
		sText = "Second Block: Actual Colour"
		DRAW_DEBUG_TEXT_2D(sText, <<0.40, 0.74, 1.0>>, 255, 255, 0, 200)
	
		sText = "Standing on Prop: "
		sText += MC_PlayerBD[iLocalPart].iCurrentPropHit[0]
		DRAW_DEBUG_TEXT_2D(sText, <<0.40, 0.78, 1.0>>, 255, 255, 0, 200)
			
		sText = "Team 0: "
		sText += MC_ServerBD_4.iPropsAmountOwnedByTeam[0]
		sText += " / "
		sText += iTurfWarTotalClaimableProps
		DRAW_DEBUG_TEXT_2D(sText, <<0.60, 0.78, 1.0>>, 255, 255, 0, 200)
		
		sText = "Team 1:  "
		sText += MC_ServerBD_4.iPropsAmountOwnedByTeam[1]
		sText += " / "
		sText += iTurfWarTotalClaimableProps
		DRAW_DEBUG_TEXT_2D(sText, <<0.70, 0.78, 1.0>>, 255, 255, 0, 200)
		
		sText = "Team 2: "
		sText += MC_ServerBD_4.iPropsAmountOwnedByTeam[2]
		sText += " / "
		sText += iTurfWarTotalClaimableProps
		DRAW_DEBUG_TEXT_2D(sText, <<0.80, 0.78, 1.0>>, 255, 255, 0, 200)
		
		sText = "Team 3: "
		sText += MC_ServerBD_4.iPropsAmountOwnedByTeam[3]
		sText += " / "
		sText += iTurfWarTotalClaimableProps
		DRAW_DEBUG_TEXT_2D(sText, <<0.90, 0.78, 1.0>>, 255, 255, 0, 200)
		
		sText = "Taking Cover on Prop: "
		sText += MC_PlayerBD[iLocalPart].iCurrentPropHit[1]
		DRAW_DEBUG_TEXT_2D(sText, <<0.40, 0.825, 1.0>>, 255, 255, 0, 200)
					
		INT iPlay
		FOR iPlay = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
			sText = "Part "
			sText += iPlay
			sText += " : "
			sText += MC_ServerBD_4.iPropAmountOwnedByPart[iPlay]
			DRAW_DEBUG_TEXT_2D(sText, <<0.20, 0.20+(iPlay*0.015), 1.0>>, 255, 255, 0, 200)
		ENDFOR
			
		SET_TEXT_SCALE(fTextSize, fTextSize)
		SET_TEXT_COLOUR(200, 200, 200, 200)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.51+fXOffsetOther, 0.725+fYOffset, "STRING", "Unclaimed")
		ASSIGN_TURF_WAR_COLOURS_FOR_DEBUG(g_FMMC_STRUCT.iTurfWarColourUnclaimed, r, g, b)
		DRAW_RECT(0.52+fXOffsetOther, 0.745+fYOffset, 0.02, 0.0075, r, g, b, 255)
		
		SET_TEXT_SCALE(fTextSize, fTextSize)
		SET_TEXT_COLOUR(200, 200, 200, 200)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.56+fXOffsetOther, 0.725+fYOffset, "STRING", "Claimed Team 0")
		ASSIGN_TURF_WAR_COLOURS_FOR_DEBUG(g_FMMC_STRUCT.iTurfWarColourTeam1, r, g, b)
		DRAW_RECT(0.57+fXOffsetOther, 0.745+fYOffset, 0.02, 0.0075, r, g, b, 255)
		
		SET_TEXT_SCALE(fTextSize, fTextSize)
		SET_TEXT_COLOUR(200, 200, 200, 200)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.61+fXOffsetOther, 0.725+fYOffset, "STRING", "Claimed Team 1")
		ASSIGN_TURF_WAR_COLOURS_FOR_DEBUG(g_FMMC_STRUCT.iTurfWarColourTeam2, r, g, b)
		DRAW_RECT(0.62+fXOffsetOther, 0.745+fYOffset, 0.02, 0.0075, r, g, b, 255)
		
		SET_TEXT_SCALE(fTextSize, fTextSize)
		SET_TEXT_COLOUR(200, 200, 200, 200)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.66+fXOffsetOther, 0.725+fYOffset, "STRING", "Claimed Team 2")
		ASSIGN_TURF_WAR_COLOURS_FOR_DEBUG(g_FMMC_STRUCT.iTurfWarColourTeam3, r, g, b)
		DRAW_RECT(0.67+fXOffsetOther, 0.745+fYOffset, 0.02, 0.0075, r, g, b, 255)
		
		SET_TEXT_SCALE(fTextSize, fTextSize)
		SET_TEXT_COLOUR(200, 200, 200, 200)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.71+fXOffsetOther, 0.725+fYOffset, "STRING", "Claimed Team 3")
		ASSIGN_TURF_WAR_COLOURS_FOR_DEBUG(g_FMMC_STRUCT.iTurfWarColourTeam4, r, g, b)
		DRAW_RECT(0.72+fXOffsetOther, 0.745+fYOffset, 0.02, 0.0075, r, g, b, 255)
		
		SET_TEXT_SCALE(fTextSize, fTextSize)
		SET_TEXT_COLOUR(200, 200, 200, 200)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.76+fXOffsetOther, 0.725+fYOffset, "STRING", "Contested")
		ASSIGN_TURF_WAR_COLOURS_FOR_DEBUG(g_FMMC_STRUCT.iTurfWarColourContested, r, g, b)
		DRAW_RECT(0.77+fXOffsetOther, 0.745+fYOffset, 0.02, 0.0075, r, g, b, 255)
		
		SET_TEXT_SCALE(fTextSize, fTextSize)
		SET_TEXT_COLOUR(200, 200, 200, 200)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.81+fXOffsetOther, 0.725+fYOffset, "STRING", "Unclaimable")
		ASSIGN_TURF_WAR_COLOURS_FOR_DEBUG(g_FMMC_STRUCT.iTurfWarColourDisabled, r, g, b)
		DRAW_RECT(0.82+fXOffsetOther, 0.745+fYOffset, 0.02, 0.0075, r, g, b, 255)
	ENDIF
ENDPROC
PROC DRAW_DEBUG_HELP()
	FLOAT iXStartPos = 0.03
	FLOAT iYStartPos = 0.09
	
	VECTOR vDrawPos = << iXStartPos, iYStartPos, 0 >>
	
	PRINT_DEBUG_LINE( "~~ MISSION CONTROLLER DEBUG ~~", vDrawPos, eColour_Orange )
	
	SWITCH debugState
		CASE eControllerDebugState_OVERVIEW
			PRINT_DEBUG_LINE( "<< End | OVERVIEW | Apartments >>", vDrawPos, eColour_Green )
		BREAK
		
		CASE eControllerDebugState_APARTMENTS
			PRINT_DEBUG_LINE( "<< Overview | APARTMENTS | Cutscenes >>", vDrawPos, eColour_Green )
		BREAK
		
		CASE eControllerDebugState_CUTSCENES
			PRINT_DEBUG_LINE( "<< Apartments | CUTSCENES | CaptureLoc >>", vDrawPos, eColour_Green )
		BREAK
		
		CASE eControllerDebugState_CAPTURELOC
			PRINT_DEBUG_LINE( "<< Cutscenes | CAPTURELOC | Pointless >>", vDrawPos, eColour_Green )
		BREAK
		
		CASE eControllerDebugState_POINTLESS
			PRINT_DEBUG_LINE( "<< CaptureLoc | POINTLESS | Entitystuff >>", vDrawPos, eColour_Green )
		BREAK
		
		CASE eControllerDebugState_ENTITYSTUFF
			PRINT_DEBUG_LINE( "<< Pointless | ENTITYSTUFF | Player >>", vDrawPos, eColour_Green )
		BREAK
		
		CASE eControllerDebugState_PLAYER
			PRINT_DEBUG_LINE( "<< Entitystuff | PLAYER | Vehicles >>", vDrawPos, eColour_Green )
		BREAK
		
		CASE eControllerDebugState_VEHICLES
			PRINT_DEBUG_LINE( "<< Player | VEHICLES | End >>", vDrawPos, eColour_Green )
		BREAK
		
		CASE eControllerDebugState_END
			PRINT_DEBUG_LINE( "<< Vehicles | END | Overview >>", vDrawPos, eColour_Green )
		BREAK
		
	ENDSWITCH
ENDPROC

PROC MAINTAIN_RECORD_OF_PREVIOUS_GAMESTATES()
	INT iLoop
	
	INT iTeam = MC_playerBD[ iPartToUse ].iteam
	INT iCurrentRule = MC_serverBD_4.iCurrentHighestPriority[ iTeam ]
	
	IF iPreviousRule != iCurrentRule
		FOR iLoop = 0 TO ciMAX_RECORDED_CLIENT_STATES - 2
			iClientStates[ iLoop ] = iClientStates[ iLoop + 1 ]
		ENDFOR
		iClientStates[ ciMAX_RECORDED_CLIENT_STATES - 1 ] = GET_MC_CLIENT_MISSION_STAGE( iPartToUse )
		iPreviousRule = iCurrentRule
	ENDIF
	
	IF iGameStates[ ciMAX_RECORDED_GAME_STATES - 1 ] != MC_playerBD[iLocalPart].iGameState
		FOR iLoop = 0 TO ciMAX_RECORDED_GAME_STATES - 2
			iGameStates[ iLoop ] = iGameStates[ iLoop + 1 ]
		ENDFOR
		iGameStates[ ciMAX_RECORDED_GAME_STATES - 1 ] = MC_playerBD[iLocalPart].iGameState
	ENDIF

ENDPROC

PROC DRAW_STATE_RECORD( VECTOR& vDrawPos )

	TEXT_LABEL_63 sTemp
	INT iLoop
	
	INT iIterator
	
	//TODO(Owain): store the last n frames of game state
	sTemp = "C: >> "
	sTemp += GET_CLIENT_STATE_STRING( GET_MC_CLIENT_MISSION_STAGE( iPartToUse ) )
	PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_Orange )
	
	iIterator = ciMAX_RECORDED_CLIENT_STATES - 1
	
	FOR iLoop = 0 TO ciMAX_RECORDED_CLIENT_STATES - 1
		sTemp = "P: >> "
		sTemp += GET_CLIENT_STATE_STRING( iClientStates[ iIterator ] )
		PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_White )
		iIterator--
	ENDFOR
	
	vDrawPos += << 0, 0.015, 0 >>
	
	//TODO(Owain): store the last n frames of game state
	sTemp = "C: * "
	sTemp += GET_GAME_STATE_STRING( MC_playerBD[iLocalPart].iGameState )
	PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_White )
	
	iIterator = ciMAX_RECORDED_GAME_STATES - 1
	
	FOR iLoop = 0 TO ciMAX_RECORDED_GAME_STATES - 1
		sTemp = "P: * "
		sTemp += GET_GAME_STATE_STRING( iGameStates[ iIterator ] )
		PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_White )
		iIterator--
	ENDFOR
ENDPROC

PROC MAINTAIN_OWAIN_WIDGETS()

	IF IS_KEYBOARD_KEY_JUST_PRESSED( KEY_MULTIPLY )
	OR IS_KEYBOARD_KEY_JUST_PRESSED( KEY_LWIN )
		IF debugState = eControllerDebugState_OFF
			debugState = eControllerDebugState_OVERVIEW
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE( TRUE )
		ELSE
			debugState = eControllerDebugState_OFF
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE( FALSE )
		ENDIF
	ENDIF
	INT i
	PLAYER_INDEX tempPlayer
	MAINTAIN_RECORD_OF_PREVIOUS_GAMESTATES()

	IF debugState != eControllerDebugState_OFF
	
		HANDLE_DEBUG_WINDOW_KEYBOARD()
		DRAW_DEBUG_HELP()

		FLOAT iXStartPos = 0.03
		FLOAT iYStartPos = 0.20
		INT iTeam = MC_playerBD[ iPartToUse ].iteam
		
		VECTOR vDrawPos = << iXStartPos, iYStartPos, 0 >>
		TEXT_LABEL_63 sTemp
		INT iLoop
		
		VECTOR vMyPos
		FLOAT fPlayerHeading
		FLOAT diff

		SWITCH( debugState )
			
			CASE eControllerDebugState_OVERVIEW
				
				IF IS_KEYBOARD_KEY_JUST_PRESSED( KEY_NUMPAD1 )
					BROADCAST_PP_MANUAL_MUSIC_CHANGE(MUSIC_TYPE_EXEC1_START_BESPOKE_BROKE)
				ELIF IS_KEYBOARD_KEY_JUST_PRESSED( KEY_NUMPAD2 )
					BROADCAST_PP_MANUAL_MUSIC_CHANGE(MUSIC_TYPE_EXEC1_START_CAR_STEAL)
				ELIF IS_KEYBOARD_KEY_JUST_PRESSED( KEY_NUMPAD3 )
					BROADCAST_PP_MANUAL_MUSIC_CHANGE(MUSIC_TYPE_EXEC1_START_CLIFF12)
				ELIF IS_KEYBOARD_KEY_JUST_PRESSED( KEY_NUMPAD4 )
					BROADCAST_PP_MANUAL_MUSIC_CHANGE(MUSIC_TYPE_EXEC1_START_CLIFF18)
				ELIF IS_KEYBOARD_KEY_JUST_PRESSED( KEY_NUMPAD5 )
					BROADCAST_PP_MANUAL_MUSIC_CHANGE(MUSIC_TYPE_EXEC1_START_CLIFF19)
				ELIF IS_KEYBOARD_KEY_JUST_PRESSED( KEY_NUMPAD6 )
					BROADCAST_PP_MANUAL_MUSIC_CHANGE(MUSIC_TYPE_EXEC1_START_CLIFF33)
				ELIF IS_KEYBOARD_KEY_JUST_PRESSED( KEY_NUMPAD7 )
					BROADCAST_PP_MANUAL_MUSIC_CHANGE(MUSIC_TYPE_EXEC1_START_GET_ON_THE_MOVE)
				ELIF IS_KEYBOARD_KEY_JUST_PRESSED( KEY_NUMPAD8 )
					BROADCAST_PP_MANUAL_MUSIC_CHANGE(MUSIC_TYPE_EXEC1_START_PSYCHOPATH)
				ELIF IS_KEYBOARD_KEY_JUST_PRESSED( KEY_NUMPAD9 )
					BROADCAST_PP_MANUAL_MUSIC_CHANGE(MUSIC_TYPE_EXEC1_START_RED_SQUARE)
				ELIF IS_KEYBOARD_KEY_JUST_PRESSED( KEY_NUMPAD0 )
					BROADCAST_PP_MANUAL_MUSIC_CHANGE(MUSIC_TYPE_EXEC1_START_WHO_CALLED_POPO)
				ENDIF
					
				sTemp = "g_FMMC_STRUCT.iAudioScore: "
				sTemp += g_FMMC_STRUCT.iAudioScore
				PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_White )
				GO_DOWN_A_LINE( vDrawPos )
				
				IF g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_EXEC1_START_BESPOKE_BROKE
					PRINT_DEBUG_LINE("(1) EXEC1_START_BESPOKE_BROKE", vDrawPos, eColour_Green)
				ENDIF
			
				IF g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_EXEC1_START_CAR_STEAL
					PRINT_DEBUG_LINE("(2) EXEC1_START_CAR_STEAL", vDrawPos, eColour_Green)
				ENDIF
				
				IF g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_EXEC1_START_CLIFF12
					PRINT_DEBUG_LINE("(3) EXEC1_START_CLIFF12", vDrawPos, eColour_Green)
				ENDIF
				
				IF g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_EXEC1_START_CLIFF18
					PRINT_DEBUG_LINE("(4) EXEC1_START_CLIFF18", vDrawPos, eColour_Green)
				ENDIF
				
				IF g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_EXEC1_START_CLIFF19
					PRINT_DEBUG_LINE("(5) EXEC1_START_CLIFF19", vDrawPos, eColour_Green)
				ENDIF
				
				IF g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_EXEC1_START_CLIFF33
					PRINT_DEBUG_LINE("(6) EXEC1_START_CLIFF33", vDrawPos, eColour_Green)
				ENDIF
				
				IF g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_EXEC1_START_GET_ON_THE_MOVE
					PRINT_DEBUG_LINE("(7) EXEC1_START_GET_ON_THE_MOVE", vDrawPos, eColour_Green)
				ENDIF
				
				IF g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_EXEC1_START_PSYCHOPATH
					PRINT_DEBUG_LINE("(8) EXEC1_START_PSYCHOPATH", vDrawPos, eColour_Green)
				ENDIF
				
				IF g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_EXEC1_START_RED_SQUARE
					PRINT_DEBUG_LINE("(9) EXEC1_START_RED_SQUARE", vDrawPos, eColour_Green)
				ENDIF
				
				IF g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_EXEC1_START_WHO_CALLED_POPO
					PRINT_DEBUG_LINE("(0) EXEC1_START_WHO_CALLED_POPO", vDrawPos, eColour_Green)
				ENDIF
				
				IF g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_BIKER_GUNSMITH_START
					PRINT_DEBUG_LINE("(0) MUSIC_TYPE_BIKER_GUNSMITH_START", vDrawPos, eColour_Green)
				ENDIF
				
				IF g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_BIKER_DEADLINE_START
					PRINT_DEBUG_LINE("(0) MUSIC_TYPE_BIKER_DEADLINE_START", vDrawPos, eColour_Green)
				ENDIF
				
				IF g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_BIKER_TLAD_START
					PRINT_DEBUG_LINE("(0) MUSIC_TYPE_BIKER_TLAD_START", vDrawPos, eColour_Green)
				ENDIF
				
				IF g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_BIKER_DEADLINE_PLACEHOLDER
					PRINT_DEBUG_LINE("(0) MUSIC_TYPE_BIKER_DEADLINE_PLACEHOLDER", vDrawPos, eColour_Green)
				ENDIF
					
				IF g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_IE_TW_START
					PRINT_DEBUG_LINE("(0) MUSIC_TYPE_IE_TW_START", vDrawPos, eColour_Green)
				ENDIF
				
				IF g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_IE_SVM_START
					PRINT_DEBUG_LINE("(0) MUSIC_TYPE_IE_SVM_START", vDrawPos, eColour_Green)
				ENDIF
				
				IF g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_GR_START
					PRINT_DEBUG_LINE("(0) MUSIC_TYPE_GR_START", vDrawPos, eColour_Green)
				ENDIF
				
				IF g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_CMH_START
					PRINT_DEBUG_LINE("(0) MP_MC_CMH_START", vDrawPos, eColour_Green)
				ENDIF	
				
				IF g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_CMH_ADV_START
					PRINT_DEBUG_LINE("(0) CMH_ADV_START", vDrawPos, eColour_Green)
				ENDIF	

				IF g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_ASSAULT_ADV_START
					PRINT_DEBUG_LINE("(0) MP_MC_ASSAULT_ADV_START", vDrawPos, eColour_Green)
				ENDIF
				
				IF g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_CASINO_BRAWL
					PRINT_DEBUG_LINE("(0) MP_MC_CASINO_BRAWL_START", vDrawPos, eColour_Green)
				ENDIF
					
				IF g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_CASINO_HEIST_FINALE
					PRINT_DEBUG_LINE("(0) MP_CHF_START", vDrawPos, eColour_Green)
				ENDIF
				
				IF g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_YACHT_MISSION
					PRINT_DEBUG_LINE("(0) MP_MC_SUM20_START", vDrawPos, eColour_Green)
				ENDIF
					
				IF g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_ISLAND_HEIST_FINALE
					PRINT_DEBUG_LINE("(0) HEI4_FIN_START", vDrawPos, eColour_Green)
				ENDIF
					
				IF g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_TUNER
					PRINT_DEBUG_LINE("(0) MP_MC_TUNER_START_MUSIC", vDrawPos, eColour_Green)
				ENDIF
				
				IF g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_CMH_SUB_PREP_START
					PRINT_DEBUG_LINE("(0) MP_MC_CMH_SUB_PREP_START", vDrawPos, eColour_Green)
				ENDIF
				
				IF g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_CMH_SUB_FINALE_START
					PRINT_DEBUG_LINE("(0) MP_MC_CMH_SUB_FINALE_START", vDrawPos, eColour_Green)
				ENDIF
				
				IF g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_CMH_SILO_PREP_START
					PRINT_DEBUG_LINE("(0) MP_MC_CMH_SILO_PREP_START", vDrawPos, eColour_Green)
				ENDIF
				
				IF g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_CMH_SILO_FINALE_START
					PRINT_DEBUG_LINE("(0) MP_MC_CMH_SILO_FINALE_START", vDrawPos, eColour_Green)
				ENDIF
				
				IF g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_CMH_IAA_PREP_START
					PRINT_DEBUG_LINE("(0) MP_MC_CMH_IAA_PREP_START", vDrawPos, eColour_Green)
				ENDIF
				
				IF g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_CMH_IAA_FINALE_START
					PRINT_DEBUG_LINE("(0) MP_MC_CMH_IAA_FINALE_START", vDrawPos, eColour_Green)
				ENDIF
								
				IF g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_SMG_START
					PRINT_DEBUG_LINE("(0) MUSIC_TYPE_SMG_START", vDrawPos, eColour_Green)
				ENDIF
					
				IF g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_SMGH_START
					PRINT_DEBUG_LINE("(0) MUSIC_TYPE_SMGH_START", vDrawPos, eColour_Green)
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[MC_playerBD[iLocalPart].iteam][0].iSpawnBitSet, ci_SpawnBS_AirSpawn)
					PRINT_DEBUG_LINE( "Is air spawn!", vDrawPos, eColour_Green )
					vMyPos = GET_ENTITY_COORDS(LocalPlayerPed)
					sTemp = "My Pos: "
					sTemp += FLOAT_TO_STRING(vMyPos.z)
					PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_White )
					
					sTemp = "CB Pos: "
					sTemp += FLOAT_TO_STRING(vDZPlayerSpawnLoc.z)
					PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_White )
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciFORCE_JUMP_FROM_CARGOBOB)
						PRINT_DEBUG_LINE( "Force jump BS set!", vDrawPos, eColour_Green )
						
						IF IS_PED_JUMPING(LocalPlayerPed)
							PRINT_DEBUG_LINE( "Ped is jumping", vDrawPos, eColour_Red )
						ELSE
							PRINT_DEBUG_LINE( "Ped isn't jumping", vDrawPos, eColour_Green )
						ENDIF
						
						IF GET_PED_PARACHUTE_STATE(LocalPlayerPed) = PPS_INVALID
							PRINT_DEBUG_LINE( "Ped isn't parachuting!", vDrawPos, eColour_Green )
						ELSE
							PRINT_DEBUG_LINE( "Ped is parachuting", vDrawPos, eColour_Red )
						ENDIF
						
						IF IS_BIT_SET(iLocalBoolCheck17, LBOOL17_CAN_FORCE_PLAYER_TO_JUMP_FROM_CARGOBOB)
							PRINT_DEBUG_LINE( "Can force jump!", vDrawPos, eColour_Green )
						ELSE
							PRINT_DEBUG_LINE( "Can't force jump", vDrawPos, eColour_Red )
						ENDIF
						
						fPlayerHeading = GET_ENTITY_HEADING(LocalPlayerPed)
						diff = fPlayerHeading - fDZPlayerVehHeading
						diff = PICK_FLOAT(diff < 0, -diff, diff)
						
						sTemp = "My heading: "
						sTemp += FLOAT_TO_STRING(fPlayerHeading)
						PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_White )
						
						sTemp = "CB heading: "
						sTemp += FLOAT_TO_STRING(fDZPlayerVehHeading)
						PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_White )
						
						sTemp = "heading diff: "
						sTemp += FLOAT_TO_STRING(diff)
						PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_White )
					ELSE
						
					ENDIF
				ELSE
					PRINT_DEBUG_LINE( "Isn't air spawn", vDrawPos, eColour_Red )
				ENDIF
				
				sTemp = "iMusicState = "
				sTemp += MC_PlayerBD[iPartToUse].iMusicState
				PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_White )
				
				sTemp = "iAudioScore = "
				sTemp += g_FMMC_STRUCT.iAudioScore
				PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_White )
				
				IF IS_BIT_SET(iLocalBoolCheck,LBOOL_MUSIC_CHANGED)
					PRINT_DEBUG_LINE( "LBOOL_MUSIC_CHANGED", vDrawPos, eColour_Green )
				ELSE
					PRINT_DEBUG_LINE( "LBOOL_MUSIC_CHANGED", vDrawPos, eColour_Red )
				ENDIF
		
				IF IS_BIT_SET(iLocalBoolCheck,LBOOL3_MUSIC_PLAYING)
					PRINT_DEBUG_LINE( "LBOOL3_MUSIC_PLAYING", vDrawPos, eColour_Green )
				ELSE
					PRINT_DEBUG_LINE( "LBOOL3_MUSIC_PLAYING", vDrawPos, eColour_Red )
				ENDIF
				
				FOR i = 0 TO FMMC_MAX_VEHICLES-1
					IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
						IF IS_VEHICLE_DRIVEABLE( NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]) )
						AND IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i])))
							IF IS_VEHICLE_STUCK_TIMER_UP( NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]), VEH_STUCK_ON_ROOF, 0 )
								PRINT_DEBUG_LINE( "VEH_STUCK_ON_ROOF", vDrawPos, eColour_Green )
							ELSE
								PRINT_DEBUG_LINE( "VEH_STUCK_ON_ROOF", vDrawPos, eColour_Red )
							ENDIF
							IF IS_VEHICLE_STUCK_TIMER_UP( NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]), VEH_STUCK_ON_SIDE, 0 )
								PRINT_DEBUG_LINE( "VEH_STUCK_ON_SIDE", vDrawPos, eColour_Green )
							ELSE
								PRINT_DEBUG_LINE( "VEH_STUCK_ON_SIDE", vDrawPos, eColour_Red )
							ENDIF
							IF IS_VEHICLE_STUCK_TIMER_UP( NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]), VEH_STUCK_HUNG_UP, 0 )
								PRINT_DEBUG_LINE( "VEH_STUCK_HUNG_UP", vDrawPos, eColour_Green )
							ELSE
								PRINT_DEBUG_LINE( "VEH_STUCK_HUNG_UP", vDrawPos, eColour_Red )
							ENDIF
							IF IS_VEHICLE_STUCK_TIMER_UP( NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]), VEH_STUCK_JAMMED, 0 )
								PRINT_DEBUG_LINE( "VEH_STUCK_JAMMED", vDrawPos, eColour_Green )
							ELSE
								PRINT_DEBUG_LINE( "VEH_STUCK_JAMMED", vDrawPos, eColour_Red )
							ENDIF								
						ENDIF
					ENDIF
				ENDFOR
				
				
				sTemp = "Swan Cam state = "
				sTemp += GET_SWAN_CAM_AS_STRING()
				PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_White )
				
				sTemp = "Swan Anim state = "
				sTemp += GET_SWAN_DIVE_AS_STRING()
				PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_White )
								
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					GO_DOWN_A_LINE( vDrawPos )
					sTemp = "Mini Round State = "
					sTemp += GET_TR_ROUND_STATE_AS_STRING()
					PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_Orange )
					
					INT iPart, iPlayersToCheck, iPlayersChecked
					iPlayersToCheck = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]
					FOR iPart = 0 TO (MAX_NUM_MC_PLAYERS - 1)
						IF (NOT(IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)) OR IS_BIT_SET(MC_playerBD[iPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN))
						AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_PLAYER_FAIL)
						AND IS_BIT_SET(MC_playerBD[iPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
						AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_FINISHED)
							PARTICIPANT_INDEX tempPart
							tempPart = INT_TO_PARTICIPANTINDEX(iPart)
							IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
								iPlayersChecked++
								PLAYER_INDEX tempLoopPlayer
								tempLoopPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
								IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
								AND IS_NET_PLAYER_OK(tempLoopPlayer, TRUE)
									IF MC_playerBD[iPart].iPreviousCheckPointForPosition != -1
									AND MC_playerBD[iPart].iCheckPointForPosition != -1
										VECTOR vPlayerCoords, vPrevCP, vNextCP
										vPlayerCoords = GET_PLAYER_COORDS(tempLoopPlayer)
										vPrevCP = GET_LOCATION_VECTOR(MC_playerBD[iPart].iPreviousCheckPointForPosition)
										vNextCP = GET_LOCATION_VECTOR(MC_playerBD[iPart].iCheckPointForPosition)
										DRAW_DEBUG_LINE(vPlayerCoords, vNextCP, 0, 255, 0, 255)
										DRAW_DEBUG_LINE(vPlayerCoords, vPrevCP, 255, 0, 0, 255)
										sTemp = "Part "
										sTemp += iPart
										sTemp += " next checkpoint: "
										sTemp += g_FMMC_STRUCT_ENTITIES.sGotoLocationData[MC_playerBD[iPart].iCheckPointForPosition].iPriority[MC_playerBD[iPart].iTeam]
										sTemp += " prev checkpoint: "
										sTemp += g_FMMC_STRUCT_ENTITIES.sGotoLocationData[MC_playerBD[iPart].iPreviousCheckPointForPosition].iPriority[MC_playerBD[iPart].iTeam]
										PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_Red )
										sTemp = "position distance: "
										FLOAT fDistForPos
										fDistForPos = VDIST2(vPlayerCoords, vNextCP) - VDIST2(vPlayerCoords, vPrevCP)
										sTemp += FLOOR(fDistForPos)
										sTemp += "."
										sTemp += ROUND((fDistForPos - FLOOR(fDistForPos)) * 1000)
										sTemp += " lap for pos: "
										sTemp += (MC_ServerBD_4.iTeamLapsCompleted[MC_playerBD[iPart].iTeam] - MC_playerBD[iPart].iLapOffset)
										PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_Red )
									ENDIF	
								ENDIF
							ENDIF
						ENDIF
						IF iPlayersChecked >= iPlayersToCheck
							BREAKLOOP
						ENDIF
					ENDFOR
				ENDIF
				
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_POWER_MAD(g_FMMC_STRUCT.iAdversaryModeType)
					GO_DOWN_A_LINE(vDrawPos )
					sTemp = "Current Capture Score: "
					sTemp += MC_playerBD[iLocalPart].iGranularCurrentPoints
					PRINT_DEBUG_LINE(sTemp, vDrawPos, eColour_White)
					sTemp = "Current Team Capture Score: "
					sTemp += MC_serverBD.iGranularCurrentPoints[MC_playerBD[iLocalPart].iTeam]
					PRINT_DEBUG_LINE(sTemp, vDrawPos, eColour_White)
				ENDIF
				
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
					GO_DOWN_A_LINE( vDrawPos )
					sTemp = "iOTPartToMove: "
					sTemp += iOTPartToMove
					PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_Orange )
					sTemp = "iTeamToMove: "
					sTemp += MC_serverBD_1.iTeamToMove
					PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_Orange )
					sTemp = "iPartToMove: "
					sTemp += MC_serverBD_1.iPartToMove[MC_serverBD_1.iTeamToMove]
					PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_Orange )
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciALLOW_MISSION_AIRSTRIKES)
					GO_DOWN_A_LINE( vDrawPos )
					sTemp = "Airstrike Coords: "
					sTemp += VECTOR_TO_STRING(g_vAirstrikeCoords, 1)
					PRINT_DEBUG_LINE(sTemp,vDrawPos, eColour_Orange)
					sTemp = "Airstrike Offset: "
					sTemp += VECTOR_TO_STRING(vAirstrikeOffset, 1)
					PRINT_DEBUG_LINE(sTemp,vDrawPos, eColour_Orange)
					
				ENDIF
								
				sTemp = "------------------------------------------"
				vDrawPos += << 0, 0.015, 0 >>
				
				DRAW_STATE_RECORD( vDrawPos )
				
				PRINT_DEBUG_LINE( "", vDrawPos, eColour_White )
				
				IF iTeam < FMMC_MAX_TEAMS AND iTeam >= 0
				AND MC_serverBD_4.iCurrentHighestPriority[ iTeam ] < FMMC_MAX_RULES
				AND MC_serverBD_4.iCurrentHighestPriority[ iTeam ] >= 0
					
					sTemp = "(Indexed from 1)"
					PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_RED )
					
					sTemp = "Current Team Rule = "
					sTemp += MC_serverBD_4.iCurrentHighestPriority[ iTeam ]+1
					PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_White )
					
					sTemp = "Mid point Hit = "
					IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], MC_serverBD_4.iCurrentHighestPriority[iTeam])
						sTemp += "YES"
						PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_Green )
					ELSE
						sTemp += "NO"
						PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_White )
					ENDIF
					
					sTemp = "Single Rule Timer: "
					sTemp += GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdObjectiveLimitTimer[iteam])
					sTemp += ": "
					sTemp += GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iObjectiveTimeLimitRule[MC_serverBD_4.iCurrentHighestPriority[ iTeam ]])
					PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_White )
					
					sTemp = "Multi Rule Timer: "
					sTemp += GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[iteam])
					sTemp += ": "
					sTemp += MC_serverBD_3.iMultiObjectiveTimeLimit[iteam]
					PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_White )
					
					IF bIsLocalPlayerHost
						IF IS_KEYBOARD_KEY_JUST_PRESSED( KEY_CAPITAL )
							MC_serverBD_3.tdObjectiveLimitTimer[iteam].Timer = GET_TIME_OFFSET(MC_serverBD_3.tdObjectiveLimitTimer[iteam].Timer, -30000)
							MC_serverBD_3.tdMultiObjectiveLimitTimer[iteam].Timer = GET_TIME_OFFSET(MC_serverBD_3.tdMultiObjectiveLimitTimer[iteam].Timer, -30000)
							PRINTLN("[RCC MISSION][MultiRuleTimer] MAINTAIN_OWAIN_WIDGETS - Host pressed KEY_CAPITAL, offset multi rule timer for team ",iteam," by -30000")
						ENDIF
					ENDIF
				ENDIF
				
				vDrawPos += << 0, 0.015, 0 >>
				
				sTemp = "Player Score: "
				sTemp += MC_PlayerBD[iLocalPart].iPlayerScore
				PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_Green )
				
				PRINT_DEBUG_LINE( "", vDrawPos, eColour_White )
				
				sTemp = "Overall Team Score: "
				sTemp += MC_serverBD.iTeamScore[ iTeam ]
				PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_Green )
				
				sTemp = "Mission Team Target Score: "
				sTemp += g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iMissionTargetScore
				PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_Green )
								
				PRINT_DEBUG_LINE( "", vDrawPos, eColour_White )
				
				sTemp = "Current Rule Team Score: "
				sTemp += MC_serverBD.iScoreOnThisRule [ iTeam ]
				PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_Green )
						
				IF MC_serverBD_4.iCurrentHighestPriority[ iTeam ] > -1
				AND MC_serverBD_4.iCurrentHighestPriority[ iTeam ] < FMMC_MAX_RULES
					sTemp = "Current Rule Score Target: "
					sTemp += g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iTargetScore[MC_serverBD_4.iCurrentHighestPriority[iTeam]]
					PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_Green )
				ENDIF
				
			BREAK
			
			CASE eControllerDebugState_APARTMENTS
				
				PRINT_DEBUG_LINE( "APARTMENTS THINGS:", vDrawPos, eColour_Green )

				PRINT_DEBUG_TRUEFALSE_LINE( IS_BIT_SET( iLocalBoolCheck10, LBOOL10_CURRENTLY_ON_APARTMENT_GO_TO ), "LBOOL10_CURRENTLY_ON_APARTMENT_GO_TO = TRUE", "LBOOL10_CURRENTLY_ON_APARTMENT_GO_TO = FALSE", eColour_Red, vDrawPos )
				
				PRINT_DEBUG_TRUEFALSE_LINE( g_bGotMissionCriticalEntity, "g_bGotMissionCriticalEntity = TRUE", "g_bGotMissionCriticalEntity = FALSE", eColour_Red, vDrawPos )
				PRINT_DEBUG_TRUEFALSE_LINE( g_bShouldThisPlayerGetPulledIntoApartment, "g_bShouldThisPlayerGetPulledIntoApartment = TRUE", "g_bShouldThisPlayerGetPulledIntoApartment = FALSE", eColour_Red, vDrawPos )
				
				vDrawPos += << 0, 0.007, 0 >>
				
				PRINT_DEBUG_TRUEFALSE_LINE( g_bPropertyDropOff, "g_bPropertyDropOff = TRUE", "g_bPropertyDropOff = FALSE", eColour_Red, vDrawPos )
				PRINT_DEBUG_TRUEFALSE_LINE( g_HeistApartmentDropoffPanStarted, "g_HeistApartmentDropoffPanStarted = TRUE", "g_HeistApartmentDropoffPanStarted = FALSE", eColour_Red, vDrawPos )
				PRINT_DEBUG_TRUEFALSE_LINE( g_HeistApartmentReadyForDropOffCut, "g_HeistApartmentReadyForDropOffCut = TRUE", "g_HeistApartmentReadyForDropOffCut = FALSE", eColour_Red, vDrawPos )
				
				vDrawPos += << 0, 0.007, 0 >>
				
				PRINT_DEBUG_TRUEFALSE_LINE( g_bGarageDropOff, "g_bGarageDropOff = TRUE", "g_bGarageDropOff = FALSE", eColour_Red, vDrawPos )
				PRINT_DEBUG_TRUEFALSE_LINE( g_HeistGarageDropoffPanStarted, "g_HeistGarageDropoffPanStarted = TRUE", "g_HeistGarageDropoffPanStarted = FALSE", eColour_Red, vDrawPos )
				PRINT_DEBUG_TRUEFALSE_LINE( g_HeistGarageDropoffPanComplete, "g_HeistGarageDropoffPanComplete = TRUE", "g_HeistGarageDropoffPanComplete = FALSE", eColour_Red, vDrawPos )
				
				vDrawPos += << 0, 0.015, 0 >>
				
				PRINT_DEBUG_TRUEFALSE_LINE( IS_BIT_SET( iLocalBoolCheck7, LBOOL7_TOGGLED_RENDERPHASES_FOR_DROPOFF ), "Renderphases Toggled", "Renderphases not toggled", eColour_Red, vDrawPos )
				PRINT_DEBUG_TRUEFALSE_LINE( IS_BIT_SET( iLocalBoolCheck7, LBOOL7_SHOULD_HOLD_UP_MOCAP_FOR_CUTSCENE ), "Holding up streaming of next mocap", "not holding up mocap streaming", eColour_Red, vDrawPos )
				PRINT_DEBUG_TRUEFALSE_LINE( IS_BIT_SET( iLocalBoolCheck7, LBOOL8_NO_MOCAP_AFTER_THIS_DROPOFF ), "No mocap after this dropoff - bailing", "There's a mocap after this", eColour_Red, vDrawPos )
			
				vDrawPos += << 0, 0.015, 0 >>

				BOOL bReadyToSeeCutscene
				BOOL bReadyToEnterApartment
				
				IF ( g_bPropertyDropOff OR g_bGarageDropOff ) 
				AND IS_BIT_SET( iLocalBoolCheck7, LBOOL7_SHOULD_HOLD_UP_MOCAP_FOR_CUTSCENE ) 
				AND g_bShouldThisPlayerGetPulledIntoApartment 
					bReadyToEnterApartment = TRUE
					IF g_HeistApartmentReadyForDropOffCut
					AND IS_BIT_SET( iLocalBoolCheck7, LBOOL7_TOGGLED_RENDERPHASES_FOR_DROPOFF )
						bReadyToSeeCutscene = TRUE
					ENDIF
				ENDIF
				
				PRINT_DEBUG_TRUEFALSE_LINE( bReadyToEnterApartment, "Ready to enter apartment", "Not ready to enter apartment", eColour_Red, vDrawPos )
				PRINT_DEBUG_TRUEFALSE_LINE( bReadyToSeeCutscene, "Ready to see apartment dropoff cut", "Not ready to see apartment dropoff cut", eColour_Red, vDrawPos )

			BREAK
			
			CASE eControllerDebugState_CUTSCENES
			
				eColour renderColor
			
				//
				// MID
				//
				renderColor = eColour_Green
				PRINT_DEBUG_LINE("MID CUTSCENE", vDrawPos, renderColor)
				vDrawPos += << 0, 0.015, 0 >>
			
				FOR iLoop = 0 TO FMMC_MAX_TEAMS - 1
					sTemp = "Team "
					sTemp += iLoop
					sTemp += " - iCutsceneID: "
					sTemp += MC_serverBD.iCutsceneID[iLoop]
					sTemp += " - iCutsceneStreamingIndex: "
					sTemp += MC_serverBD.iCutsceneStreamingIndex[iLoop]
					PRINT_DEBUG_LINE(sTemp, vDrawPos, renderColor)
				ENDFOR
				
				// Cutscene team
				INT iCutsceneTeam 
				iCutsceneTeam = GET_CUTSCENE_TEAM(FALSE)
				sTemp = "iCutsceneTeam: "
				sTemp += iCutsceneTeam
				PRINT_DEBUG_LINE(sTemp, vDrawPos, renderColor)

				// Streaming cutscene team
				INT iStreamingCutsceneTeam
				iStreamingCutsceneTeam = GET_CUTSCENE_TEAM(TRUE)
				sTemp = "iStreamingCutsceneTeam: "
				sTemp += iStreamingCutsceneTeam
				PRINT_DEBUG_LINE(sTemp, vDrawPos, renderColor)
				
				//
				// END
				//
				renderColor = eColour_Red
				vDrawPos += << 0, 0.015, 0 >>
				PRINT_DEBUG_LINE("END", vDrawPos, renderColor)
				vDrawPos += << 0, 0.015, 0 >>
				
				sTemp = "iEndCutscene: "
				sTemp += MC_ServerBD.iEndCutscene
				PRINT_DEBUG_LINE(sTemp, vDrawPos, renderColor)
				
				FOR iLoop = 0 TO FMMC_MAX_TEAMS - 1
					sTemp = "Team "
					sTemp += iLoop
					sTemp += " - iEndCutsceneNum: "
					sTemp += MC_serverBD.iEndCutsceneNum[iLoop]
					PRINT_DEBUG_LINE(sTemp, vDrawPos, renderColor)
				ENDFOR
				
				//
				// MOCAP
				//
				renderColor = eColour_Orange
				vDrawPos += << 0, 0.015, 0 >>
				PRINT_DEBUG_LINE("MOCAP", vDrawPos, eColour_Orange)
				vDrawPos += << 0, 0.015, 0 >>
				
				sTemp = "sMocapCutscene: "
				sTemp += sMocapCutscene
				PRINT_DEBUG_LINE(sMocapCutscene, vDrawPos, renderColor)
				
				sTemp = GET_MOCAP_STREAMING_STAGE_DEBUG_NAME(eMocapStreamingStage)
				PRINT_DEBUG_LINE(sTemp, vDrawPos, renderColor)

				sTemp = "eMocapManageCutsceneProgress: "
				sTemp += GET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS_NAME(eMocapManageCutsceneProgress)
				PRINT_DEBUG_LINE(sTemp, vDrawPos, renderColor)
				
				sTemp = "eMocapRunningCutsceneProgress: "
				sTemp += GET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS_NAME(eMocapRunningCutsceneProgress)
				PRINT_DEBUG_LINE(sTemp, vDrawPos, renderColor)
				
				sTemp = "PBBOOL2_PLAYING_MOCAP_CUTSCENE: "
				sTemp += BOOL_TO_STRING(IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitset2, PBBOOL2_PLAYING_MOCAP_CUTSCENE))
				PRINT_DEBUG_LINE(sTemp, vDrawPos, renderColor)
				
				//
				// SCRIPTED
				//
				renderColor = eColour_Green
				vDrawPos += << 0, 0.015, 0 >>
				PRINT_DEBUG_LINE("SCRIPTED", vDrawPos, renderColor)
				vDrawPos += << 0, 0.015, 0 >>
				
				sTemp = "eScriptedCutsceneProgress: "
				sTemp += GET_SCRIPTED_CUTSCENE_STATE_PROGRESS_NAME(eScriptedCutsceneProgress)
				PRINT_DEBUG_LINE( sTemp, vDrawPos, renderColor)

				//
				// COMMON
				//
				renderColor = eColour_White
				vDrawPos += << 0, 0.015, 0 >>
				PRINT_DEBUG_LINE("COMMON", vDrawPos, renderColor)
				vDrawPos += << 0, 0.015, 0 >>
				
				
				IF iCutsceneTeam != -1
					
					BOOL bCutSelected
					FMMC_CUTSCENE_TYPE eCutType
					IF MC_serverBD.iEndCutsceneNum[iCutsceneTeam] != -1
						
						eCutType = FMMCCUT_ENDMOCAP
						bCutSelected = TRUE
						
					ELIF MC_serverBD.iCutsceneID[iCutsceneTeam] >= 0 AND MC_serverBD.iCutsceneID[iCutsceneTeam] < FMMC_GET_MAX_SCRIPTED_CUTSCENES() 
						
						eCutType = FMMCCUT_SCRIPTED
						bCutSelected = TRUE
						
					ELIF MC_serverBD.iCutsceneID[iCutsceneTeam] >= FMMC_GET_MAX_SCRIPTED_CUTSCENES() AND MC_serverBD.iCutsceneID[iCutsceneTeam] < FMMC_GET_MAX_CUTSCENES_WITH_MOCAP()
						
						eCutType = FMMCCUT_MOCAP
						bCutSelected = TRUE
						
					ENDIF
					
					IF bCutSelected
					
						sTemp = "Cutscene Type: "
						sTemp += GET_FMMC_CUTSCENE_TYPE_NAME(eCutType)
						PRINT_DEBUG_LINE(sTemp, vDrawPos, renderColor)
					
						VECTOR vCutsceneCoords 
						vCutsceneCoords = GET_CUTSCENE_COORDS(iCutsceneTeam, eCutType)
						PRINT_DEBUG_VECTOR_WITH_TEXT("Cutscene Coords: ", vCutsceneCoords, renderColor, vDrawPos)
					
					ENDIF
					
				ENDIF
				
				FOR iLoop = 0 TO FMMC_MAX_TEAMS - 1
					sTemp = "Team "
					sTemp += iLoop
					sTemp += " - CutscenePlayerIDs: "
					
					INT iPlayer
					FOR iPlayer = 0 TO FMMC_MAX_CUTSCENE_PLAYERS -1
						sTemp += "["
						sTemp += NATIVE_TO_INT(MC_serverBD.piCutscenePlayerIDs[iLoop][iPlayer])
						sTemp += "]"
					ENDFOR
					PRINT_DEBUG_LINE(sTemp, vDrawPos, renderColor)
				ENDFOR
				
				sTemp = "LBOOL2_CUTSCENE_STREAMED: "
				sTemp += BOOL_TO_STRING(IS_BIT_SET(iLocalBoolCheck2, LBOOL2_CUTSCENE_STREAMED))
				PRINT_DEBUG_LINE(sTemp, vDrawPos, renderColor)
				
				sTemp = "PBBOOL2_STARTED_CUTSCENE: "
				sTemp += BOOL_TO_STRING(IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitset2, PBBOOL2_STARTED_CUTSCENE))
				PRINT_DEBUG_LINE(sTemp, vDrawPos, renderColor)
				
				sTemp = "PBBOOL2_FINISHED_CUTSCENE: "
				sTemp += BOOL_TO_STRING(IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitset2, PBBOOL2_FINISHED_CUTSCENE))
				PRINT_DEBUG_LINE(sTemp, vDrawPos, renderColor)

				IF iCutsceneTeam != -1
					sTemp = "Num players who should watch this cutscene = "
					sTemp += GET_NUMBER_OF_PEOPLE_SHOULD_BE_WATCHING_CUTSCENE(iCutsceneTeam)
					PRINT_DEBUG_LINE(sTemp, vDrawPos, renderColor)
					
					sTemp = "Num players who finished Cutscene = "
					sTemp += GET_NUMBER_OF_PEOPLE_FINISHED_CUTSCENE(iCutsceneTeam)
					PRINT_DEBUG_LINE(sTemp, vDrawPos, renderColor)
				ENDIF
				
			BREAK	
			
			CASE eControllerDebugState_CAPTURELOC

				FOR iLoop = 0 TO FMMC_MAX_TEAMS-1
					sTemp = "Team "
					sTemp += iLoop
					
					sTemp += ": "
					sTemp += MC_ServerBD.iCaptureBarPercentage[iLoop]
					
					sTemp += ", "
					sTemp += PICK_STRING(bTeamInLocDebug[iLoop], "Y", "N")
					
					IF g_iTheWinningTeam = iLoop
						sTemp += " *"
					ENDIF
					
					PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_White )
					vDrawPos += << 0, 0.015, 0 >>
					
				ENDFOR
				vDrawPos += << 0, 0.015, 0 >>
				
				sTemp = "iNumberofCaptureTeams = "
				sTemp += iNumberofCaptureTeamsDebug
				PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_White )
				vDrawPos += << 0, 0.015, 0 >>
				
				sTemp = "iCaptureTeam = "
				sTemp += iCaptureTeamDebug
				PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_White )
			BREAK
			
			CASE eControllerDebugState_POINTLESS
			
				sTemp = "iTotalNumStartingPlayers = "
				sTemp += MC_serverBD.iTotalNumStartingPlayers
				PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_White )
				
				sTemp = "iNumObjCreated = "
				sTemp += MC_serverBD.iNumObjCreated
				PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_White )
				
				
				
				FOR i = 0 TO (MAX_NUM_MC_PLAYERS - 1)
					IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
						tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
						
						sTemp = GET_PLAYER_NAME(tempPlayer)
						sTemp += ": Team "
						sTemp += MC_playerBD[i].iteam
						sTemp += ", score "
						sTemp += MC_playerBD[i].iPlayerScore
						PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_White )
					ENDIF
				ENDFOR
				
				GO_DOWN_A_LINE(vDrawPos)
				
				FOR i = 0 TO (FMMC_MAX_NUM_OBJECTS-1)
//					IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[i])
//						IF IS_ENTITY_ATTACHED_TO_ANY_PED(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[i]))
//							//tempPlayer = GET_ENTITY_ATTACHED_TO
//							tempPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_ATTACHED_TO(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[i])))	)
//							
//							sTemp = "Object "
//							sTemp += i
//							sTemp += " attached to "
//							sTemp += GET_PLAYER_NAME(tempPlayer)
//							PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_White )
//						
//						ENDIF
//					ENDIF
					
					IF MC_serverBD.iObjCarrier[i] != -1
						sTemp = "Object "
						sTemp += i
						sTemp += ", carrier = "
						sTemp += GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(MC_serverBD.iObjCarrier[i])))
						PRINT_DEBUG_LINE( sTemp, vDrawPos, eColour_White )
					ENDIF
				ENDFOR
				
				
				
			BREAK
			
			
			CASE eControllerDebugState_ENTITYSTUFF
			
				sTemp = "Local Player Data:"
				PRINT_DEBUG_LINE(sTemp, vDrawPos, eColour_Green)
				
				sTemp = "iCurrentLoc = "
				sTemp += MC_playerBD[iLocalPart].iCurrentLoc
				PRINT_DEBUG_LINE(sTemp, vDrawPos, eColour_White)
				
				sTemp = "iInAnyLoc = "
				sTemp += MC_playerBD[iLocalPart].iInAnyLoc
				PRINT_DEBUG_LINE(sTemp, vDrawPos, eColour_White)
				
				sTemp = "iLeaveLoc = "
				sTemp += MC_playerBD[iLocalPart].iLeaveLoc
				PRINT_DEBUG_LINE(sTemp, vDrawPos, eColour_White)
				
				sTemp = "iPedNear = "
				sTemp += MC_playerBD[iLocalPart].iPedNear
				PRINT_DEBUG_LINE(sTemp, vDrawPos, eColour_White)
				
				sTemp = "iVehNear = "
				sTemp += MC_playerBD[iLocalPart].iVehNear
				PRINT_DEBUG_LINE(sTemp, vDrawPos, eColour_White)
				
				sTemp = "iObjNear = "
				sTemp += MC_playerBD[iLocalPart].iObjNear
				PRINT_DEBUG_LINE(sTemp, vDrawPos, eColour_White)
				
				sTemp = "iPartNear = "
				sTemp += MC_playerBD[iLocalPart].iPartNear
				PRINT_DEBUG_LINE(sTemp, vDrawPos, eColour_White)
				
				sTemp = "iObjHacked = "
				sTemp += MC_playerBD[iLocalPart].iObjHacked
				PRINT_DEBUG_LINE(sTemp, vDrawPos, eColour_White)
				
				sTemp = "iObjHacking = "
				sTemp += MC_playerBD[iLocalPart].iObjHacking
				PRINT_DEBUG_LINE(sTemp, vDrawPos, eColour_White)
				
				sTemp = "iPedCarryCount = "
				sTemp += MC_playerBD[iLocalPart].iPedCarryCount
				PRINT_DEBUG_LINE(sTemp, vDrawPos, eColour_White)
				
				sTemp = "iVehCarryCount = "
				sTemp += MC_playerBD[iLocalPart].iVehCarryCount
				PRINT_DEBUG_LINE(sTemp, vDrawPos, eColour_White)
				
				sTemp = "iObjCarryCount = "
				sTemp += MC_playerBD[iLocalPart].iObjCarryCount
				PRINT_DEBUG_LINE(sTemp, vDrawPos, eColour_White)
				
				sTemp = "Server Data:"
				PRINT_DEBUG_LINE(sTemp, vDrawPos, eColour_Green)
				
				FOR i = 0 TO MC_serverBD.iNumObjCreated - 1
					sTemp = "MC_serverBD.iObjHackPart = "
					sTemp += MC_serverBD.iObjHackPart[i]
					PRINT_DEBUG_LINE(sTemp, vDrawPos, eColour_White)
				ENDFOR
			BREAK
			
			CASE eControllerDebugState_PLAYER
			
				sTemp = "Player Overrides:"
				PRINT_DEBUG_LINE(sTemp, vDrawPos, eColour_Green)
				
				sTemp = "g_sFMMCVisualAid.eOverride = "
				sTemp += GET_VISUAL_AID_MODE_NAME(g_sFMMCVisualAid.eOverride)
				PRINT_DEBUG_LINE(sTemp, vDrawPos, eColour_White)
				
				sTemp = "g_sFMMCVisualAid.bMaintainOverride = "
				sTemp += BOOL_TO_STRING(g_sFMMCVisualAid.bMaintainOverride)
				PRINT_DEBUG_LINE(sTemp, vDrawPos, eColour_White)
				
				sTemp = "g_sFMMCVisualAid.tl31OverrideScriptName = \""
				sTemp += (g_sFMMCVisualAid.tl31OverrideScriptName)
				sTemp += "\""
				PRINT_DEBUG_LINE(sTemp, vDrawPos, eColour_White)
				
				sTemp = "g_sFMMCVisualAid.eUseSound = "
				sTemp += GET_VISUAL_AID_SOUND_NAME(g_sFMMCVisualAid.eUseSound)
				PRINT_DEBUG_LINE(sTemp, vDrawPos, eColour_White)
				
				INT iRule
				iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
				
				sTemp = ""
				IF iteam < FMMC_MAX_TEAMS
				AND iRule < FMMC_MAX_RULES
					sTemp += BOOL_TO_STRING(IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[iRule], ciBS_RULE11_IAA_GUN_CAMERAS_ENABLED))
				ELSE
					sTemp += "N/A"
				ENDIF
				PRINT_DEBUG_LINE(sTemp, vDrawPos, eColour_White)
				
				sTemp = "g_eIAAGunCamerasEnabled = "
				sTemp += ENUM_TO_INT(g_eIAAGunCamerasEnabled)
				PRINT_DEBUG_LINE(sTemp, vDrawPos, eColour_White)
				
			BREAK
			
			CASE eControllerDebugState_VEHICLES
			
				
			
				PRINT_DEBUG_LINE("Avenger", vDrawPos, eColour_Green)
				GO_DOWN_A_LINE(vDrawPos)
				
				PRINT_DEBUG_LINE("PBBOOL4_REQUEST_AVENGER_HOLD_TRANSITION_BLOCK:", vDrawPos, eColour_White)
				vDrawPos += <<0.015,0,0>>
				FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
					
					IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
						RELOOP
					ENDIF
					
					sTemp = "Part "
					sTemp += i
					sTemp += ": "
					sTemp += BOOL_TO_STRING(IS_BIT_SET(MC_playerBD[i].iClientBitSet4, PBBOOL4_REQUEST_AVENGER_HOLD_TRANSITION_BLOCK))
					PRINT_DEBUG_LINE(sTemp, vDrawPos, eColour_White)
				ENDFOR
				vDrawPos -= <<0.015,0,0>>
				
				PRINT_DEBUG_LINE("PBBOOL4_BLOCKING_AVENGER_HOLD_TRANSITION:", vDrawPos, eColour_White)
				vDrawPos += <<0.015,0,0>>
				FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
					
					IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
						RELOOP
					ENDIF
					
					sTemp = "Part "
					sTemp += i
					sTemp += ": "
					sTemp += BOOL_TO_STRING(IS_BIT_SET(MC_playerBD[i].iClientBitSet4, PBBOOL4_BLOCKING_AVENGER_HOLD_TRANSITION))
					PRINT_DEBUG_LINE(sTemp, vDrawPos, eColour_White)
				ENDFOR
				vDrawPos -= <<0.015,0,0>>
				
				PRINT_DEBUG_LINE("iAvengerHoldTransitionBlockedCount:", vDrawPos, eColour_White)
				vDrawPos += <<0.015,0,0>>
				FOR i = 0 TO FMMC_MAX_TEAMS-1
					sTemp = "Team "
					sTemp += i
					sTemp += ": "
					sTemp += MC_serverBD_4.iAvengerHoldTransitionBlockedCount[i]
					PRINT_DEBUG_LINE(sTemp, vDrawPos, eColour_White)
				ENDFOR
				vDrawPos -= <<0.015,0,0>>
				
				PRINT_DEBUG_LINE("IS_CREATOR_AIRCRAFT_ENTRY_BLOCKED:", vDrawPos, eColour_White)
				vDrawPos += <<0.015,0,0>>
				FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
					
					PARTICIPANT_INDEX partID
					partID = INT_TO_PARTICIPANTINDEX(i)
					IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(partID)
						RELOOP
					ENDIF

					sTemp = "Part "
					sTemp += i
					sTemp += ": "
					sTemp += BOOL_TO_STRING(IS_CREATOR_AIRCRAFT_ENTRY_BLOCKED(NETWORK_GET_PLAYER_INDEX(partID)))
					PRINT_DEBUG_LINE(sTemp, vDrawPos, eColour_White)
				ENDFOR
				vDrawPos -= <<0.015,0,0>>
				
				sTemp = "IS_SIMPLE_INTERIOR_EXIT_BLOCKED: "
				sTemp += BOOL_TO_STRING(IS_SIMPLE_INTERIOR_EXIT_BLOCKED())
				PRINT_DEBUG_LINE(sTemp, vDrawPos, eColour_White)

			BREAK
			
			CASE eControllerDebugState_END
			
				PRINT_DEBUG_LINE("iCelebrationBitSet:", vDrawPos, eColour_White)
				
				vDrawPos += <<0.015,0,0>>
				FOR i = 0 TO ciCELEBRATION_BIT_SET_PlacedPlayerForLowriderPostScene
					sTemp = "["
					sTemp += i
					sTemp += "]: "
					sTemp += BOOL_TO_STRING(IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_SCRIPTED_CUT_NEEDS_CLEANED_UP))
					sTemp += " "
					sTemp += GET_CELEBRATION_BIT_SET_NAME(i)
					PRINT_DEBUG_LINE(sTemp, vDrawPos, eColour_White)
				ENDFOR	
				vDrawPos -= <<0.015,0,0>>
				
			BREAK
		
		ENDSWITCH

	ENDIF
ENDPROC

FUNC BOOL WAS_MISSION_LAUNCHED_THROUGH_Z_MENU()
	PARTICIPANT_INDEX temppart
	PLAYER_INDEX playerId 
	INT iparticipant
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iparticipant
		temppart = INT_TO_PARTICIPANTINDEX(iparticipant)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(temppart)
			playerId = NETWORK_GET_PLAYER_INDEX(tempPart)
			IF NOT IS_PLAYER_SCTV(playerId)
				IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerId)].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_DONT_USE_END_VOTE_SCREEN)
					PRINTLN("WAS_MISSION_LAUNCHED_THROUGH_Z_MENU() -  IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerId)].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_DONT_USE_END_VOTE_SCREEN)")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

PROC CACHE_COMMANDLINES()
	PRINTLN("CACHE_COMMANDLINES - Caching all command lines")
	
	bInVehicleDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_InVehicleDebug")
	bPlacedMissionVehiclesDebugHealth = GET_COMMANDLINE_PARAM_EXISTS("sc_MissionPlacedVehicleHealth")
	bFMMCVehBodyNetVehIDprints = GET_COMMANDLINE_PARAM_EXISTS("sc_FMMCVehBodyNetVehIDprints")
	bobjectiveVehicleOnCargobobDebug = GET_COMMANDLINE_PARAM_EXISTS("objectiveVehicleOnCargobobDebug")
	bextrabombfootballdebug = GET_COMMANDLINE_PARAM_EXISTS("extrabombfootballdebug")
	bRunningBackDiveCamLine = GET_COMMANDLINE_PARAM_EXISTS("sc_RunningBackDiveCamLine")
	bParticipantLoopPrints = GET_COMMANDLINE_PARAM_EXISTS("sc_ParticipantLoopPrints")
	bAllowAllMissionsWithTwoPlayers = DEBUG_IS_ALLOW_ALL_MISSION_WITH_TWO_PLAYERS_ENABLED()
	bTeamColourDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_TeamColourDebug")
	bFMMCOverrideMultiRuleTimer = GET_COMMANDLINE_PARAM_EXISTS("sc_FMMCOverrideMultiRuleTimer")
	bTimeLimit = GET_COMMANDLINE_PARAM_EXISTS("sc_TimeLimit")
	bBailTimerMc = GET_COMMANDLINE_PARAM_EXISTS("sc_BailTimerMc")
	bShowMeTagTeam = GET_COMMANDLINE_PARAM_EXISTS("sc_ShowMeTagTeam")
	bWantedChange = GET_COMMANDLINE_PARAM_EXISTS("sc_WantedChange")
	bJS3226522 = GET_COMMANDLINE_PARAM_EXISTS("JS3226522")
	bAllowGangOpsWithoutBoss = GET_COMMANDLINE_PARAM_EXISTS("sc_AllowGangOpsWithoutBoss")
	bHandcuff = GET_COMMANDLINE_PARAM_EXISTS("sc_handcuff")
	bLMhelpfulScriptAIPrints = TRUE // need this on for a lot of ai bugs in casino_heist GET_COMMANDLINE_PARAM_EXISTS("sc_LMhelpfulScriptAIPrints")
	bExpProjDebug = GET_COMMANDLINE_PARAM_EXISTS("ExpProjDebug")
	bObjectGotoDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_ObjectGotoDebug")
	bDebugPrintContainer = GET_COMMANDLINE_PARAM_EXISTS("sc_DebugPrintContainer")
	bDawnRaidWarpToObject = GET_COMMANDLINE_PARAM_EXISTS("sc_DawnRaidWarpToObject")
	bShowVehSpawnNumberCycle = GET_COMMANDLINE_PARAM_EXISTS("sc_ShowVehSpawnNumberCycle")
	bbmbfbRadioDebug = GET_COMMANDLINE_PARAM_EXISTS("bmbfbRadioDebug")
	bbombfootballdebug = GET_COMMANDLINE_PARAM_EXISTS("bombfootballdebug")
	bMultiPersonVehiclePrints = GET_COMMANDLINE_PARAM_EXISTS("sc_MultiPersonVehiclePrints")
	bDoNotPauseRenderphases = GET_COMMANDLINE_PARAM_EXISTS("sc_DoNotPauseRenderphases")
	bSkipSVMIntro = GET_COMMANDLINE_PARAM_EXISTS("sc_SkipSVMIntro")
	bAllowTripSkipNoPlay = GET_COMMANDLINE_PARAM_EXISTS("sc_AllowTripSkipNoPlay")
	bIdleKickPrints = GET_COMMANDLINE_PARAM_EXISTS("sc_g_B_IdleKickPrints")
	bDebugPrintCustomText = GET_COMMANDLINE_PARAM_EXISTS("sc_DebugPrintCustomText")
	bHUDHidingReasons = GET_COMMANDLINE_PARAM_EXISTS("sc_HUDHidingReasons")
	bGangChaseCleanup = GET_COMMANDLINE_PARAM_EXISTS("sc_GangChaseCleanup")
	bOverrideCCTVRotDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_OverrideCCTVRotDebug")
	bsc3378817 = GET_COMMANDLINE_PARAM_EXISTS("sc3378817")
	bHeistHashedMac = GET_COMMANDLINE_PARAM_EXISTS("sc_HeistHashedMac")
	bAutoForceRestartNoCorona = GET_COMMANDLINE_PARAM_EXISTS("sc_AutoForceRestartNoCorona")
	bPhoneEMPDebug = GET_COMMANDLINE_PARAM_EXISTS("phoneEMPDebug")
	bMissionYachtDebug = GET_COMMANDLINE_PARAM_EXISTS("missionYachtDebug")
	bVaultPoisonGasDebug = GET_COMMANDLINE_PARAM_EXISTS("vaultPoisonGasDebug")
	bCasinoVaultDoorDebug = GET_COMMANDLINE_PARAM_EXISTS("casinoVaultDoorDebug")
	bObjectTaserDebug = GET_COMMANDLINE_PARAM_EXISTS("objectTaserDebug")
	bWallRappelDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_showWallRappelDebug")
	bDisableWallRappelCam = GET_COMMANDLINE_PARAM_EXISTS("sc_DisableWallRappelCam")
	bZoneTimerDebug = GET_COMMANDLINE_PARAM_EXISTS("zoneTimerDebug")
	bInteractWithDebug = GET_COMMANDLINE_PARAM_EXISTS("interactWithDebug")
	bHandheldDrillMinigameDebug = GET_COMMANDLINE_PARAM_EXISTS("HandheldDrillMinigameDebug")
	bCCTVDebug = GET_COMMANDLINE_PARAM_EXISTS("CCTVDebug")
	bCasinoHeistBlockBranchingToDirect = GET_COMMANDLINE_PARAM_EXISTS("sc_CasinoHeistBlockBranchingToDirect")
	bDirectionalDoorDebug = GET_COMMANDLINE_PARAM_EXISTS("DirectionalDoorDebug")
	bByPassMissionVarSyncBail = GET_COMMANDLINE_PARAM_EXISTS("bByPassMissionVarSyncBail")
	bDynoPropSpam = GET_COMMANDLINE_PARAM_EXISTS("sc_DynoPropSpam")
	bObjDebug = GET_COMMANDLINE_PARAM_EXISTS("ObjDebug")
	bHumaneSyncLockDebug = GET_COMMANDLINE_PARAM_EXISTS("HumaneSyncLockDebug")
	bCasinoTunnelRubbleDebug = GET_COMMANDLINE_PARAM_EXISTS("CasinoTunnelRubbleDebug")
	bDoorCoverBlockZones = GET_COMMANDLINE_PARAM_EXISTS("CasinoCoverBlockZones")
ENDPROC

proc angled_area_locate_widget()
	
	widget_bl = get_offset_from_entity_in_world_coords(LocalPlayerPed, widget_offset_bl) 
	widget_br = get_offset_from_entity_in_world_coords(LocalPlayerPed, widget_offset_br)

	IS_ENTITY_IN_ANGLED_AREA(LocalPlayerPed, widget_bl, widget_br, widget_angled_length, false, true)	
	
	if widget_offset_br.z > 0.0
		SET_ENTITY_HEADING(LocalPlayerPed, widget_heading)
	endif 

endproc

PROC PROCESS_MAIN_MISSION_CONTROLLER_DEBUG()
	
	g_iDialogueProgressLastPlayed = iDialogueProgressLastPlayed
	
	MAINTAIN_OWAIN_WIDGETS()

	angled_area_locate_widget()
	
	IF iPlayerPressedF != -1 
		PRINTLN("[RCC MISSION]  player pressed F: ",iPlayerPressedF)
	ENDIF
	IF iPlayerPressedS != -1
		PRINTLN("[RCC MISSION] player pressed S: ",iPlayerPressedS) 
	ENDIF
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
		SET_BIT(MC_playerBD[iLocalPart].iClientBitSet, ENUM_TO_INT(PBBOOL_PRESSED_F))
	ENDIF
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
		SET_BIT(MC_playerBD[iLocalPart].iClientBitSet, ENUM_TO_INT(PBBOOL_PRESSED_S))
	ENDIF
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_U)
		TOGGLE_RENDERPHASES(TRUE)
	ENDIF
	
	//Do/Don't Fail On Death debug, apostrophe key:
	IF NOT IS_BIT_SET(iLocalDebugBS, LDEBUGBS_FIRST_DONTFAILONDEATH_CHECK)
		
		IF NOT HAS_NET_TIMER_STARTED(tdDontFailOnDeathDelayTimer)
			IF MC_playerBD[iLocalPart].iGameState = GAME_STATE_RUNNING
			AND GET_MC_SERVER_GAME_STATE() = GAME_STATE_RUNNING
			AND Is_MP_Objective_Text_On_Display() // Has the HUD come ups
				
				PARTICIPANT_INDEX serverPart = NETWORK_GET_HOST_OF_THIS_SCRIPT()
				INT iServerPart = NATIVE_TO_INT(serverPart)
				
				IF NETWORK_IS_PARTICIPANT_ACTIVE(serverPart)
					IF (iServerPart >= 0) AND (iServerPart < MAX_NUM_MC_PLAYERS)
						IF MC_playerBD[iServerPart].iGameState = GAME_STATE_RUNNING
							PRINTLN("[RCC MISSION][FailOnDeath] Init - I'm in and the server is in, start the timer!")
							REINIT_NET_TIMER(tdDontFailOnDeathDelayTimer)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdDontFailOnDeathDelayTimer) >= 3000
				IF NOT IS_BIT_SET(MC_serverBD.iDebugBitSet, SBDEBUG_DONTFAILWHENOUTOFLIVES)
				AND g_bIgnoreOutOfLivesFailure
					//The server thinks we should fail when out of lives but I don't, tell the server to sort it out
					
					PRINTLN("[RCC MISSION][FailOnDeath] Init - Server doesn't have SBDEBUG_DONTFAILWHENOUTOFLIVES set, but I have g_bIgnoreOutOfLivesFailure set, broadcast it out!")
					BROADCAST_FMMC_OVERRIDE_LIVES_FAIL(TRUE)
					
				ELIF IS_BIT_SET(MC_serverBD.iDebugBitSet, SBDEBUG_DONTFAILWHENOUTOFLIVES)
					
					PLAYER_INDEX serverPlayer = INVALID_PLAYER_INDEX()
					PARTICIPANT_INDEX serverPart = NETWORK_GET_HOST_OF_THIS_SCRIPT()
					
					IF NETWORK_IS_PARTICIPANT_ACTIVE(serverPart) // should be true for us to reach this point
						serverPlayer = NETWORK_GET_PLAYER_INDEX(serverPart)
					ENDIF
					
					PRINTLN("[RCC MISSION][FailOnDeath] Init - Server has SBDEBUG_DONTFAILWHENOUTOFLIVES, print the ticker and synchronise g_bIgnoreOutOfLivesFailure by setting it TRUE")
					PRINT_TICKER_WITH_PLAYER_NAME_AND_STRING("TICK_CHEATED", serverPlayer, "TICK_CHEAT_OOL1")
					g_bIgnoreOutOfLivesFailure = TRUE
				ENDIF
				
				PRINTLN("[RCC MISSION][FailOnDeath] Init - Initialisation completed, g_bIgnoreOutOfLivesFailure = ",g_bIgnoreOutOfLivesFailure)
				
				SET_BIT(iLocalDebugBS, LDEBUGBS_FIRST_DONTFAILONDEATH_CHECK)
				RESET_NET_TIMER(tdDontFailOnDeathDelayTimer)
			ENDIF
			
		ENDIF
	ELSE
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_APOSTROPHE)
			IF g_bIgnoreOutOfLivesFailure
				PRINTLN("[RCC MISSION][FailOnDeath] Broadcast out that g_bIgnoreOutOfLivesFailure should be false")
				BROADCAST_FMMC_OVERRIDE_LIVES_FAIL(FALSE)
			ELSE
				PRINTLN("[RCC MISSION][FailOnDeath] Broadcast out that g_bIgnoreOutOfLivesFailure should be true")
				//g_bIgnoreOutOfLivesFailure = TRUE
				BROADCAST_FMMC_OVERRIDE_LIVES_FAIL(TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	//Home key requests to be the host of the mission controller script
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_HOME)
		IF NOT bIsLocalPlayerHost
			NETWORK_REQUEST_TO_BE_HOST_OF_THIS_SCRIPT()
		ENDIF
	ENDIF
	
	MAINTAIN_PLAYER_ID_CHANGE_CHECK()			
	
	//Check the status of IS_A_STRAND_MISSION_BEING_INITIALISED every so often and assert if it's wrong
	IF GET_GAME_TIMER() % 1000 < 50
		IF NOT HAS_FIRST_STRAND_MISSION_BEEN_PASSED_NO_RESET()
		AND IS_A_STRAND_MISSION_BEING_INITIALISED()
		AND IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl23NextContentID[0]) 
		AND IS_STRING_NULL_OR_EMPTY(g_sTransitionSessionData.sStrandMissionData.tl23NextContentID)
			PRINTLN("[TS] [MSRAND] - IS_A_STRAND_MISSION_BEING_INITIALISED and it shouldn't be")
			SCRIPT_ASSERT("IS_A_STRAND_MISSION_BEING_INITIALISED and it shouldn't be")
			CLEAR_THIS_A_STRAND_MISSION_BEING_INITIALISED()
		ENDIF
	ENDIF
ENDPROC

PROC DEBUG_AUDIO_DIALOGUE_TRIGGERS_READOUT(INT iDialogueTriggerProgress)
	PRINTLN("[DIALOGUE TRIGGER][CDT] -----------------------------------------------------------------------------------------------")
	PRINTLN("[DIALOGUE TRIGGER][CDT] iDialogueProgress: 		", iDialogueTriggerProgress)
	PRINTLN("[DIALOGUE TRIGGER][CDT] tlBlock: 					", g_FMMC_STRUCT.sDialogueTriggers[iDialogueTriggerProgress].tlBlock)
	PRINTLN("[DIALOGUE TRIGGER][CDT] tlRoot: 					", g_FMMC_STRUCT.sDialogueTriggers[iDialogueTriggerProgress].tlRoot)
	PRINTLN("[DIALOGUE TRIGGER][CDT] tlDialogueTriggerAdditionalHelpText: 	", g_FMMC_STRUCT.sDialogueTriggers[iDialogueTriggerProgress].tlDialogueTriggerAdditionalHelpText)
	PRINTLN("[DIALOGUE TRIGGER][CDT] iTimeDelay: 				", g_FMMC_STRUCT.sDialogueTriggers[iDialogueTriggerProgress].iTimeDelay)
	PRINTLN("[DIALOGUE TRIGGER][CDT] iRule: 					", g_FMMC_STRUCT.sDialogueTriggers[iDialogueTriggerProgress].iRule)
	PRINTLN("[DIALOGUE TRIGGER][CDT] iTeam: 					", g_FMMC_STRUCT.sDialogueTriggers[iDialogueTriggerProgress].iTeam)
	PRINTLN("[DIALOGUE TRIGGER][CDT] iVehicleTrigger: 			", g_FMMC_STRUCT.sDialogueTriggers[iDialogueTriggerProgress].iVehicleTrigger)
	PRINTLN("[DIALOGUE TRIGGER][CDT] iPrerequisite: 			", g_FMMC_STRUCT.sDialogueTriggers[iDialogueTriggerProgress].iPrerequisite)
	PRINTLN("[DIALOGUE TRIGGER][CDT] iPedTrigger: 				", g_FMMC_STRUCT.sDialogueTriggers[iDialogueTriggerProgress].iPedTrigger)
	PRINTLN("[DIALOGUE TRIGGER][CDT] Played: 					", IS_BIT_SET(iLocalDialoguePlayedBS[iDialogueTriggerProgress/32], iDialogueTriggerProgress%32))
	
	PRINTLN("[DIALOGUE TRIGGER][CDT] ")
	PRINTLN("[DIALOGUE TRIGGER][CDT] Other Unlikely Causes -----------")
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTriggerProgress].iTeam >= 0
		PRINTLN("[DIALOGUE TRIGGER][CDT] iTeam Rule Limit:			", g_FMMC_STRUCT.sDialogueTriggers[iDialogueTriggerProgress].iTeamRuleLimit[g_FMMC_STRUCT.sDialogueTriggers[iDialogueTriggerProgress].iTeam])
	ENDIF
	PRINTLN("[DIALOGUE TRIGGER][CDT] Trigger Later: 			", IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[iDialogueTriggerProgress].iDialogueBitset,iBS_DialogueTriggerLater))
	PRINTLN("[DIALOGUE TRIGGER][CDT] First Person Cam			", IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTriggerProgress].iDialogueBitset2,iBS_Dialogue2PlayOnlyInFirstPerson))
	PRINTLN("[DIALOGUE TRIGGER][CDT] Block Text if Wanted		", IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[iDialogueTriggerProgress].iDialogueBitset2,iBS_Dialogue2BlockIfWanted))
	PRINTLN("[DIALOGUE TRIGGER][CDT] Wanted Loss				", IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTriggerProgress].iDialogueBitset,iBS_DialogueTriggerOnWantedGain))
	PRINTLN("[DIALOGUE TRIGGER][CDT] Wanted Gain 				", IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTriggerProgress].iDialogueBitset,iBS_DialogueTriggerOnWantedLoss))
	
ENDPROC

#ENDIF

