
USING "FM_Mission_Controller_USING.sch"
USING "FM_Mission_Controller_Utility.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Entity Creation and Setup
// ##### Description: From fmmc_creation - no point in being in a shared header
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SET_UP_FMMC_PED_RC(PED_INDEX piPassed, INT iPedNumber, INT istartingplayers, FMMC_LEGACY_MISSION_CONTINUITY_VARS &sLEGACYMissionContinuityVars, BOOL bVisible = TRUE, VEHICLE_INDEX tempVeh = NULL)
	
	INT iteamrel[FMMC_MAX_TEAMS]
	INT irepeat
	INT iAccuracy
	INT icop
	COMBAT_ABILITY_LEVEL eCombatAbility
	FLOAT fstartingplayers 
	fstartingplayers = TO_FLOAT(istartingplayers)
	FLOAT fNumParticipants
	fNumParticipants = TO_FLOAT(g_FMMC_STRUCT.iNumParticipants)
	FLOAT fDifficultyMod, fhealth, ftunablehealth
	fDifficultyMod = (fstartingplayers/fNumParticipants)
	CONST_FLOAT fLesMultiplier 0.8
	VECTOR vPos = GET_ENTITY_COORDS(piPassed)
	
	PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - g_FMMC_STRUCT.iNumParticipants = ",g_FMMC_STRUCT.iNumParticipants) 
	PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - istartingplayers = ",istartingplayers) 
	PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - fDifficultyMod = ",fDifficultyMod) 
	PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - Corona difficulty = ",g_FMMC_STRUCT.iDifficulity)
	PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - Model Name: ", GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].mn))
	PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - iModelVariation: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iModelVariation)
	
	IF g_FMMC_STRUCT.iDifficulity = DIFF_EASY
		IF NOT IS_PED_IN_ANY_PLANE(piPassed)
			SET_PED_SHOOT_RATE(piPassed, 60)
		ENDIF
		fDifficultyMod = fDifficultyMod - 1.0
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - fDifficultyMod after easy mod = ") NET_PRINT_FLOAT(fDifficultyMod) NET_NL()
	ELIF g_FMMC_STRUCT.iDifficulity = DIFF_HARD
		fDifficultyMod = fDifficultyMod + 1.0
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - fDifficultyMod after hard mod = ") NET_PRINT_FLOAT(fDifficultyMod) NET_NL()
	ENDIF
	
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFive, ciPED_BSFive_DoNotTriggerBarrier)
		SET_PED_CONFIG_FLAG(piPassed, PCF_IgnoredByAutoOpenDoors, TRUE)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - ped = ", iPedNumber, ", ciPED_BSFive_DoNotTriggerBarrier bit is set. Calling PCF_IgnoredByAutoOpenDoors.")
	ENDIF

	FMMC_SET_PED_VARIATION(piPassed,g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].mn, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iModelVariation)
	
	//If this is the juggernaut
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].mn = INT_TO_ENUM(MODEL_NAMES, HASH("U_M_Y_Juggernaut_01"))
		SET_PED_MOVEMENT_CLIPSET(piPassed, "ANIM_GROUP_MOVE_BALLISTIC")
		SET_PED_STRAFE_CLIPSET(piPassed, "MOVE_STRAFE_BALLISTIC")
		SET_WEAPON_ANIMATION_OVERRIDE(piPassed, HASH("BALLISTIC"))
		
		PRINTLN("{TMS] SET_UP_FMMC_PED_RC Juggernaut anim and component variations applied")
	ENDIF

	IF IS_PED_A_SCRIPTED_COP(iPedNumber)

		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iFlee != ciPED_FLEE_ON
			IF g_FMMC_STRUCT.iPolice != 1 // not wanted off
				IF IS_OBJECTIVE_PED(iPedNumber)
					PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting up as objective cop, ped: ",iPedNumber) 
					SET_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetTwo, ciPED_BSTwo_IS_OBJECTIVE_PED)
				ELSE
					PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting up as non-objective cop, ped: ",iPedNumber) 
					CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetTwo, ciPED_BSTwo_IS_OBJECTIVE_PED)
				ENDIF
			ENDIF
			
			SET_PED_CONFIG_FLAG(piPassed, PCF_DontBlip, FALSE)
			SET_PED_CONFIG_FLAG(piPassed, PCF_DontBlipCop, FALSE)
			SET_PED_AS_COP(piPassed,FALSE)
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetTwelve, ciPed_BSTwelve_NeverLoseTarget)
				SET_PED_TARGET_LOSS_RESPONSE(piPassed,TLR_SEARCH_FOR_TARGET)
			ENDIF
			//SET_PED_CONFIG_FLAG(piPassed,PCF_DontBehaveLikeLaw,TRUE)
			SET_PED_CONFIG_FLAG(piPassed,PCF_CanAttackNonWantedPlayerAsLaw,TRUE)
			SET_PED_CONFIG_FLAG(piPassed,PCF_OnlyUpdateTargetWantedIfSeen,TRUE)
			SET_PED_CONFIG_FLAG(piPassed,PCF_AllowContinuousThreatResponseWantedLevelUpdates,TRUE)
			PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting PCF_CanAttackNonWantedPlayerAsLaw on, ped: ",iPedNumber)
		ENDIF
		icop = FM_RelationshipLikeCops
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting up as cop, ped: ",iPedNumber)
	ELSE
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - not a scripted cop, setting PCF_DontBlipCop ped : ",iPedNumber)
		SET_PED_CONFIG_FLAG(piPassed, PCF_DontBlipCop, TRUE)
		SET_PED_CONFIG_FLAG(piPassed, PCF_CanAttackNonWantedPlayerAsLaw, TRUE)
		icop = FM_RelationshipNothingCops
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetThirteen, ciPED_BSThirteen_IgnoreDisguise_Noose)
			SET_PED_CONFIG_FLAG(piPassed, PCF_CanAttackNonWantedPlayerAsLaw, FALSE)
			SET_PED_CONFIG_FLAG(piPassed, PCF_OnlyUpdateTargetWantedIfSeen, FALSE)
			SET_PED_CONFIG_FLAG(piPassed, PCF_AllowContinuousThreatResponseWantedLevelUpdates, FALSE)
			PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - Turning off cop behaviour as we are ignoring Noose disguise")
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].mn = mp_g_m_pros_01
		SET_PED_COMPONENT_VARIATION(piPassed,PED_COMP_SPECIAL2,0,0)
		NET_PRINT("[RCC MISSION] SET_UP_FMMC_PED_RC - removing prof mask: ") NET_PRINT_INT(iPedNumber) NET_NL()
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].mn = IG_LESTERCREST
		SET_PED_PROP_INDEX(piPassed, ANCHOR_EYES, 0,0)
		NET_PRINT("[RCC MISSION] SET_UP_FMMC_PED_RC - Ped is lester, giving glasses") NET_PRINT_INT(iPedNumber) NET_NL()
		SET_PED_MOVEMENT_CLIPSET(piPassed, "move_heist_lester")
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].mn = PLAYER_TWO
		NET_PRINT("[RCC MISSION] SET_UP_FMMC_PED_RC - Ped is Trevor, giving outfit") NET_PRINT_INT(iPedNumber) NET_NL()
		SET_PED_COMPONENT_VARIATION(piPassed,PED_COMP_TORSO,3, 3)
		SET_PED_COMPONENT_VARIATION(piPassed,PED_COMP_LEG,18, 9)
		SET_PED_COMPONENT_VARIATION(piPassed,PED_COMP_FEET,1, 0)
		SET_PED_PROP_INDEX(piPassed, ANCHOR_EYES, 8,0)
		SET_COMBAT_FLOAT(piPassed, CCF_TIME_TO_INVALIDATE_INJURED_TARGET, 1.0)	//B*2219732
		SETUP_PLAYER_PED_DEFAULTS_FOR_MP(piPassed)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iIdleAnim = ciPED_IDLE_ANIM__TREVOR_NARCFIN_BODHI_LOOP
			//set a nice dirty flag here
		ENDIF
	ENDIF
	PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - ped: ",iPedNumber) 
	PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - associated team: ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iAssociatedTeam) 
	PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - associated rule: ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iAssociatedObjective)
	PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - associated spawn: ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iAssociatedSpawn) 
	PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - Associated Action: ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iAssociatedAction) 
	
	#IF IS_DEBUG_BUILD
		INT iweapon = enum_to_int(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - ped number: ",iPedNumber, " has weapon: ",iweapon)
	#ENDIF
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun != WEAPONTYPE_INVALID
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun = WEAPONTYPE_MINIGUN
			SET_PED_FIRING_PATTERN(piPassed,FIRING_PATTERN_FULL_AUTO)
			PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting ped with minigun to fire in FIRING_PATTERN_FULL_AUTO for ped: ",iPedNumber)
		ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun = WEAPONTYPE_RPG
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitset, ciPED_BS_RocketPedsAndVehicles)
				GIVE_DELAYED_WEAPON_TO_PED(piPassed, WEAPONTYPE_PISTOL, 25000, FALSE)
				SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_USE_ROCKETS_AGAINST_VEHICLES_ONLY,TRUE)
				PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting rocket ped to only fire at vehicles for ped: ",iPedNumber)
			ENDIF
		ENDIF
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iWepHolstered = ciPED_WEAPON_HOLSTERED_ON
			PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - giving ped holstered gun, ped: ",iPedNumber)
			GIVE_DELAYED_WEAPON_TO_PED(piPassed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun, 25000, FALSE)
		ELSE
			GIVE_DELAYED_WEAPON_TO_PED(piPassed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun, 25000, TRUE)
			SET_CURRENT_PED_WEAPON(piPassed,g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun,TRUE)
		ENDIF
		
		// Adding flashlights to NG only
		#IF IS_NEXTGEN_BUILD
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetSix, ciPED_BSSix_FlashlightsNGOnly)
				PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - giving ped Flashlight attachment: ",iPedNumber)
				GIVE_PED_A_FLASHLIGHT_ATTACHMENT(piPassed, iPedNumber)
			ENDIF
		#ENDIF
	ENDIF
	
	//See if this ped should turn invisible immediately
	IF SHOULD_PED_SPAWN_INVISIBLE(iPedNumber, piPassed)
		SET_ENTITY_ALPHA(piPassed, 0, FALSE)
		SET_PED_CAN_BE_TARGETTED(piPassed, FALSE)
		PRINTLN("[TMS] SET_UP_FMMC_PED_RC spawning ped ", iPedNumber, " invisible")
	ENDIF
	
	FLOAT fBreakLockAngle
	FLOAT fBreakLockAngleClose
	FLOAT fBreakLockCloseDistance
	FLOAT fTurnRateModifier
	
	IF IS_PED_IN_ANY_VEHICLE(piPassed)
		MODEL_NAMES mnVeh = DUMMY_MODEL_FOR_SCRIPT
		
		IF NOT DOES_ENTITY_EXIST(tempVeh)
			tempVeh = GET_VEHICLE_PED_IS_IN(piPassed)
		ENDIF
		
		IF DOES_ENTITY_EXIST(tempVeh)
			mnVeh = GET_ENTITY_MODEL(tempVeh)
		ENDIF
		
//		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iForcedVehicleSeat > -1 // If we're interested in forcing which seat the ped sits in
//			PRINTLN("[TMS] putting in seat in creation new bit")
//			FORCE_PED_INTO_HIS_ASSIGNED_SEAT(iPedNumber, piPassed, tempVeh)
//		ENDIF
		
		//-- Homing missile accuracy
		IF IS_THIS_MODEL_A_HELI(mnVeh)
		OR IS_THIS_MODEL_A_PLANE(mnVeh)
		
			PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - ped homing missile - Getting tunable data - iPedNumber = ", iPedNumber, " vehicle model = ", ENUM_TO_INT(mnVeh))
			Fill_MP_Float_Tunable(TUNE_CONTEXT_MP_GLOBALS, TUNE_CONTEXT_FM_TYPE_CONTENT_MODIFIER_NONE, HASH( "CCF_HOMING_ROCKET_TURN_RATE_MODIFIER"), g_sMPTunables.fCCF_HOMING_ROCKET_TURN_RATE_MODIFIER)
			Fill_MP_Float_Tunable(TUNE_CONTEXT_MP_GLOBALS, TUNE_CONTEXT_FM_TYPE_CONTENT_MODIFIER_NONE, HASH( "HOMING_ROCKET_BREAK_LOCK_ANGLE"), g_sMPTunables.fHOMING_ROCKET_BREAK_LOCK_ANGLE )
			Fill_MP_Float_Tunable(TUNE_CONTEXT_MP_GLOBALS, TUNE_CONTEXT_FM_TYPE_CONTENT_MODIFIER_NONE, HASH( "HOMING_ROCKET_BREAK_LOCK_ANGLE_CLOSE"), g_sMPTunables.fHOMING_ROCKET_BREAK_LOCK_ANGLE_CLOSE)
			Fill_MP_Float_Tunable(TUNE_CONTEXT_MP_GLOBALS, TUNE_CONTEXT_FM_TYPE_CONTENT_MODIFIER_NONE, HASH( "HOMING_ROCKET_BREAK_LOCK_CLOSE_DISTANCE"), g_sMPTunables.fHOMING_ROCKET_BREAK_LOCK_CLOSE_DISTANCE)

			fBreakLockAngle				= g_sMPTunables.fHOMING_ROCKET_BREAK_LOCK_ANGLE
			fBreakLockAngleClose		= g_sMPTunables.fHOMING_ROCKET_BREAK_LOCK_ANGLE_CLOSE	
			fBreakLockCloseDistance		= g_sMPTunables.fHOMING_ROCKET_BREAK_LOCK_CLOSE_DISTANCE
			fTurnRateModifier			= g_sMPTunables.fCCF_HOMING_ROCKET_TURN_RATE_MODIFIER
		
			SET_COMBAT_FLOAT(piPassed, CCF_HOMING_ROCKET_BREAK_LOCK_ANGLE, fBreakLockAngle)
			SET_COMBAT_FLOAT(piPassed, CCF_HOMING_ROCKET_BREAK_LOCK_ANGLE_CLOSE, fBreakLockAngleClose)
			SET_COMBAT_FLOAT(piPassed, CCF_HOMING_ROCKET_BREAK_LOCK_CLOSE_DISTANCE, fBreakLockCloseDistance)
			SET_COMBAT_FLOAT(piPassed, CCF_HOMING_ROCKET_TURN_RATE_MODIFIER, fTurnRateModifier)
			
			PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - ped homing missile iPedNumber = ", iPedNumber, " CCF_HOMING_ROCKET_BREAK_LOCK_ANGLE = ", fBreakLockAngle, " CCF_HOMING_ROCKET_BREAK_LOCK_ANGLE_CLOSE = ", fBreakLockAngleClose,  " CCF_HOMING_ROCKET_BREAK_LOCK_CLOSE_DISTANCE = ", fBreakLockAngleClose, " CCF_HOMING_ROCKET_TURN_RATE_MODIFIER = ", fTurnRateModifier) 
		ELSE
			PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - NOT setting homing missile tunables for iPedNumber = ", iPedNumber)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetThree, ciPED_BSThree_UseRocketsInJets)
			SET_CURRENT_PED_VEHICLE_WEAPON(piPassed, WEAPONTYPE_VEHICLE_SPACE_ROCKET)
			PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - ped = ", iPedNumber, ", ciPED_BSThree_UseRocketsInJets bit is set. Calling SET_CURRENT_PED_VEHICLE_WEAPON(piPassed, WEAPONTYPE_VEHICLE_SPACE_ROCKET).")
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetEight, ciPed_BSEight_DisableBuzzardRocketUse)
			PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - ped = ", iPedNumber, ": ciPed_BSEight_DisableBuzzardRocketUse set!")
			
			IF tempVeh != NULL
				MODEL_NAMES eVehModel = GET_ENTITY_MODEL(tempVeh)
				
				IF eVehModel = BUZZARD
				OR eVehModel = BUZZARD2
					PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - ped = ", iPedNumber, ": disabling use of buzzard rockets!")
					
					DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_VEHICLE_SPACE_ROCKET, tempVeh, piPassed)
					SET_CURRENT_PED_VEHICLE_WEAPON(piPassed, WEAPONTYPE_VEHICLE_PLAYER_BUZZARD)
					SET_PED_CAN_SWITCH_WEAPON(piPassed, FALSE) 				
				ELIF eVehModel = SAVAGE
					PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - ped = ", iPedNumber, ": disabling use of savage cannons!")
				
					DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_PLAYER_SAVAGE, tempVeh, piPassed)
					SET_CURRENT_PED_VEHICLE_WEAPON(piPassed, WEAPONTYPE_VEHICLE_SPACE_ROCKET)
					SET_PED_CAN_SWITCH_WEAPON(piPassed, FALSE) 
				ELIF eVehModel = TAMPA3
					PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - ped = ", iPedNumber, ": forcing use of tampa mortar!")
					//SET_CURRENT_PED_VEHICLE_WEAPON(piPassed, WEAPONTYPE_DLC_VEHICLE_TAMPA_MORTAR)
					//SET_PED_CAN_SWITCH_WEAPON(piPassed, FALSE)
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - ped = ", iPedNumber, ": ped veh is not a buzzard or a savage!")
				#ENDIF
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - ped = ", iPedNumber, ": ped veh is null!")
			#ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitset, ciPED_BS_RocketPedsAndVehicles)	
			IF mnVeh != SAVAGE
				SET_CURRENT_PED_VEHICLE_WEAPON(piPassed, WEAPONTYPE_VEHICLE_PLAYER_BUZZARD)
				PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - ped only use mini guns in a heli: ",iPedNumber)
			ELSE
				SET_CURRENT_PED_VEHICLE_WEAPON(piPassed, WEAPONTYPE_DLC_VEHICLE_PLAYER_SAVAGE)
				PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - ped only use mini guns in a savage heli: ",iPedNumber)
			ENDIF
		ENDIF
	ENDIF
	
	//-- For vehicles created in the air
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iVehicle != -1
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iVehicle].iVehBitsetThree, ciFMMC_VEHICLE3_VehicleStartsAirborne)	
	AND IS_PED_IN_ANY_VEHICLE(piPassed)
	AND NOT (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iVehicle].mn = TITAN AND IS_THIS_ROCKSTAR_MISSION_WVM_DUNE3(g_FMMC_STRUCT.iRootContentIDHash))
		
		IF NOT DOES_ENTITY_EXIST(tempVeh)
			tempVeh = GET_VEHICLE_PED_IS_IN(piPassed)
		ENDIF
		
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - ciFMMC_VEHICLE3_VehicleStartsAirborne is set for veh ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iVehicle, " ped ", iPedNumber)
		
		SET_VEHICLE_ENGINE_ON(tempVeh, TRUE, TRUE)
		
		SET_ENTITY_DYNAMIC(tempVeh, TRUE)
		ACTIVATE_PHYSICS(tempVeh)
		
		IF IS_THIS_MODEL_A_HELI(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iVehicle].mn)
			SET_HELI_BLADES_FULL_SPEED(tempVeh)
		ELIF IS_THIS_MODEL_A_PLANE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iVehicle].mn)
			SET_HELI_BLADES_FULL_SPEED(tempVeh)
			SET_VEHICLE_FORWARD_SPEED(tempVeh, 30.0)     
		ENDIF
		
	ENDIF
	
	FOR irepeat = 0 TO (FMMC_MAX_TEAMS-1)
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iRule[irepeat] = FMMC_OBJECTIVE_LOGIC_CROWD_CONTROL
			SET_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitset, ciPED_BS_InvolvedInCrowdControl)
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iTeam[irepeat] = ciPED_RELATION_SHIP_LIKE
			iteamrel[irepeat] = FM_RelationshipLike 
			SET_PED_CAN_BE_TARGETTED_BY_TEAM(piPassed, irepeat, FALSE)
			SET_ENTITY_IS_TARGET_PRIORITY(piPassed, FALSE)
			PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - ped likes team: ",irepeat," ped: ",iPedNumber)
			
		ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iTeam[irepeat] = ciPED_RELATION_SHIP_DISLIKE
			iteamrel[irepeat] = FM_RelationshipDislike
			
			//Only auto-target if they're going to respond with hostility
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iFlee != ciPED_FLEE_ON
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iCombatStyle = ciPED_AGRESSIVE
				OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iCombatStyle = ciPED_BERSERK
				OR ( g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iCombatStyle = ciPED_DEFENSIVE AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun != WEAPONTYPE_UNARMED AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun != WEAPONTYPE_INVALID )
					SET_PED_CAN_BE_TARGETTED_BY_TEAM(piPassed, irepeat, TRUE)
				ELSE
					SET_PED_CAN_BE_TARGETTED_BY_TEAM(piPassed, irepeat, FALSE)
				ENDIF
			ELSE
				IF ( g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun != WEAPONTYPE_UNARMED AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun != WEAPONTYPE_INVALID )
					SET_PED_CAN_BE_TARGETTED_BY_TEAM(piPassed, irepeat, TRUE)
				ELSE
					SET_PED_CAN_BE_TARGETTED_BY_TEAM(piPassed, irepeat, FALSE)
				ENDIF
			ENDIF
			
			SET_ENTITY_IS_TARGET_PRIORITY(piPassed, FALSE)
			PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - ped dislikes team: ",irepeat," ped: ",iPedNumber) 
		ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iTeam[irepeat] = ciPED_RELATION_SHIP_HATE
			iteamrel[irepeat] = FM_RelationshipHate
			SET_PED_CAN_BE_TARGETTED_BY_TEAM(piPassed, irepeat, TRUE)
			SET_ENTITY_IS_TARGET_PRIORITY(piPassed, TRUE)
			PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - ped hates team: ",irepeat," ped: ",iPedNumber) 
		ENDIF
	ENDFOR
	
	SET_PED_RELATIONSHIP_GROUP_HASH(piPassed,rgFM_AiPed[iteamrel[0]][iteamrel[1]][iteamrel[2]][iteamrel[3]][icop])	//Use FM_RelationshipNothingCops or FM_RelationshipLikeCops
	
	#IF IS_DEBUG_BUILD
		GIVE_FMMC_PED_DEBUG_NAME(piPassed, iPedNumber)
	#ENDIF
	
	
	IF IS_THIS_A_LTS()
	OR IS_THIS_A_CAPTURE()
		SET_PED_MONEY(piPassed, 0)
		PRINTLN( "[RCC MISSION] SET_UP_FMMC_PED_RC - setting ped cash amount to 0 as this is a LTS or Capture") 
	ELIF( g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPedNumber ].iPedCashAmount != -1 )
		PRINTLN( "[RCC MISSION] SET_UP_FMMC_PED_RC - setting ped cash amount ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPedNumber ].iPedCashAmount, " for ped ", iPedNumber ) 
		CPRINTLN( DEBUG_SIMON, "[RCC MISSION] SET_UP_FMMC_PED_RC - setting ped cash amount ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPedNumber ].iPedCashAmount, " for ped ", iPedNumber ) 
		SET_PED_MONEY( piPassed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPedNumber ].iPedCashAmount )
	ENDIF
	
	SET_UP_FMMC_PED_DEFENSIVE_AREA(piPassed, iPedNumber, vPos, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iFlee = ciPED_FLEE_ON
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_ALWAYS_FLEE, TRUE)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting ped to flee: ",iPedNumber)
	ELSE
		//SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_DISABLE_TARGET_CHANGES_DURING_VEHICLE_PURSUIT, FALSE)
		
		APPLY_PED_COMBAT_STYLE(piPassed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iCombatStyle, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun, iPedNumber)
		
		SET_PED_FLEE_ATTRIBUTES(piPassed, FA_DISABLE_FLEE_FROM_INDIRECT_THREATS,TRUE)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].mn = IG_LESTERCREST
			SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_DISABLE_FLEE_FROM_COMBAT, FALSE)
			SET_PED_FLEE_ATTRIBUTES(piPassed, FA_DISABLE_COWER, TRUE)
		ELSE	
			SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_DISABLE_FLEE_FROM_COMBAT, TRUE)
			
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetThirteen, ciPed_BSThirteen_DisableCowering)
				SET_PED_FLEE_ATTRIBUTES(piPassed, FA_DISABLE_COWER, FALSE)
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetThirteen, ciPed_BSThirteen_DisableCowering)
			SET_PED_FLEE_ATTRIBUTES(piPassed, FA_COWER_INSTEAD_OF_FLEE, TRUE)
		ENDIF
	ENDIF
	
	IF IS_PED_IN_ANY_HELI(piPassed)
	OR IS_PED_IN_ANY_PLANE(piPassed)
	OR IS_PED_IN_ANY_BOAT(piPassed)
	OR IS_PED_IN_ANY_SUB(piPassed)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFive, ciPED_BSFive_CanUseVehiclesForTasks)
		SET_PED_COMBAT_ATTRIBUTES(piPassed,CA_LEAVE_VEHICLES,FALSE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFive, ciPED_BSFive_DontFleeInvehicle)
		SET_PED_FLEE_ATTRIBUTES(piPassed, FA_USE_VEHICLE, FALSE)
	ELSE
		SET_PED_FLEE_ATTRIBUTES(piPassed, FA_USE_VEHICLE, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetThirteen, ciPed_BSThirteen_DisableCowering)
		SET_PED_FLEE_ATTRIBUTES(piPassed, FA_DISABLE_COWER, TRUE)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - Setting FA_DISABLE_COWER for ped: ",iPedNumber)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFive, ciPED_BSFive_DontAllowToBeDraggedOutOfVehicle)
		SET_PED_CONFIG_FLAG(piPassed, PCF_DontAllowToBeDraggedOutOfVehicle, TRUE)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - Setting ped config flag PCF_DontAllowToBeDraggedOutOfVehicle for ped: ",iPedNumber) 
	ENDIF
	
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piPassed,FALSE)
	
	//Check to see if the mission creator has selected this ped to die in water or not:
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitset,ciPed_BS_DoesntDieInWater)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetThree,ciPED_BSThree_OnlyDamagedByPlayers)
	
		SET_PED_DIES_IN_WATER(piPassed,FALSE)
		SET_PED_DIES_INSTANTLY_IN_WATER(piPassed,FALSE)
	
	ELSE
	
		SET_PED_DIES_IN_WATER(piPassed,TRUE)
		SET_PED_DIES_INSTANTLY_IN_WATER(piPassed,TRUE) 
	
	ENDIF
	
	
	SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(piPassed)

	//SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_ENABLE_TACTICAL_POINTS_WHEN_DEFENSIVE,TRUE)
	
	IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO)
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_CAN_TAUNT_IN_VEHICLE, FALSE)
	ENDIF
	
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_USE_VEHICLE,TRUE)
	//SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_USE_VEHICLE_ATTACK,TRUE)
	IF( NOT IS_PED_IN_ANY_BOAT( piPassed ) )
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_USE_VEHICLE_ATTACK_IF_VEHICLE_HAS_MOUNTED_GUNS,TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetTwelve, ciPed_BSTwelve_PreventVehicleWeaponUsage)
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_USE_VEHICLE_ATTACK, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_USE_VEHICLE_ATTACK_IF_VEHICLE_HAS_MOUNTED_GUNS, FALSE) 
		//SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_BLOCK_FIRE_FOR_VEHICLE_PASSENGER_MOUNTED_GUNS, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetSix, ciPED_BSSix_AllowDogFighting)
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_ALLOW_DOG_FIGHTING, TRUE)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - Setting ped config flag CA_ALLOW_DOG_FIGHTING for ped: ",iPedNumber) 
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFive, ciPED_BSFive_DontDoDriveBys)
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_DO_DRIVEBYS, TRUE)
	ELSE
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_DO_DRIVEBYS, FALSE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetSix, ciPED_BSSix_PreferAirCombat)
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_PREFER_AIR_COMBAT_WHEN_IN_AIRCRAFT, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetSix, ciPED_BSSix_PreferNonAircraftTargets)
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_PREFER_AIR_COMBAT_WHEN_IN_AIRCRAFT, TRUE)
	ENDIF
	
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFive, ciPED_BSFive_BlockAggroDueToAmbientPeds)
		SET_PED_CONFIG_FLAG(piPassed,PCF_DontRespondToRandomPedsDamage,TRUE)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting PCF_DontRespondToRandomPedsDamage TRUE")
	ENDIF
	
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_DISABLE_ALL_RANDOMS_FLEE, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_DISABLE_SEEK_DUE_TO_LINE_OF_SIGHT,FALSE)
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_PREFER_KNOWN_TARGETS_WHEN_COMBAT_CLOSEST_TARGET, TRUE)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetThree, ciPED_BSThree_AggroOnSeeingDeadBody)
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_WILL_GENERATE_DEAD_PED_SEEN_SCRIPT_EVENTS, TRUE)
	ENDIF
	
	SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(piPassed,KNOCKOFFVEHICLE_HARD)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iIdleAnim = ciPED_IDLE_ANIM__PHONE
	OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iSpecialAnim = ciPED_IDLE_ANIM__PHONE
		SET_PED_CONFIG_FLAG(piPassed,PCF_PhoneDisableTextingAnimations,FALSE)
		SET_PED_CONFIG_FLAG(piPassed,PCF_PhoneDisableTalkingAnimations,FALSE)
	ENDIF
	
	SET_PED_CONFIG_FLAG(piPassed,PCF_GetOutUndriveableVehicle,TRUE)
	
	SET_PED_CONFIG_FLAG(piPassed,PCF_OnlyWritheFromWeaponDamage,TRUE)
	SET_PED_CONFIG_FLAG(piPassed,PCF_DisableGoToWritheWhenInjured,TRUE)
	
	SET_PED_CONFIG_FLAG(piPassed,PCF_ListensToSoundEvents,TRUE)
	SET_PED_CONFIG_FLAG(piPassed,PCF_CheckLoSForSoundEvents,TRUE)
	
	SET_PED_CONFIG_FLAG(piPassed, PCF_KeepRelationshipGroupAfterCleanUp, TRUE)
	SET_PED_CONFIG_FLAG(piPassed, PCF_CanBeAgitated, FALSE)
	SET_PED_CONFIG_FLAG(piPassed,PCF_KeepTargetLossResponseOnCleanup,TRUE)

	SET_PED_CONFIG_FLAG(piPassed,PCF_AICanDrivePlayerAsRearPassenger,TRUE)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].mn <> IG_LESTERCREST
		SET_PED_CONFIG_FLAG(piPassed,PCF_PreventUsingLowerPrioritySeats,FALSE)
	ELSE
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting PCF_PreventUsingLowerPrioritySeats as it's Lester")
		SET_PED_CONFIG_FLAG(piPassed,PCF_PreventUsingLowerPrioritySeats,TRUE)
	ENDIF
	
	SET_PED_CONFIG_FLAG(piPassed,PCF_DisableEventInteriorStatusCheck,TRUE)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetSeven, ciPED_BSSeven_DisableDislikeAsHateWhenInCombat)
		SET_PED_CONFIG_FLAG(piPassed,PCF_TreatDislikeAsHateWhenInCombat,TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetSix, ciPED_BSSix_HateAllOtherPedGroups)
		SET_PED_CONFIG_FLAG(piPassed, PCF_TreatNonFriendlyAsHateWhenInCombat, TRUE)
	ENDIF
	
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFive, ciPED_BSFive_IgnoreExplosions)
		SET_PED_CONFIG_FLAG(piPassed, PCF_DisableExplosionReactions, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFive, ciPED_BSFive_RunFromFiresAndExplosions)
		SET_PED_CONFIG_FLAG(piPassed, PCF_RunFromFiresAndExplosions, TRUE)
		SET_PED_CONFIG_FLAG(piPassed, PCF_GetOutBurningVehicle, TRUE)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting PCF_RunFromFiresAndExplosions TRUE")
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetNine, ciPed_BSNine_DisableRunFromFiresAndExplosions)
		SET_PED_CONFIG_FLAG(piPassed, PCF_RunFromFiresAndExplosions,FALSE)
		SET_PED_CONFIG_FLAG(piPassed, PCF_GetOutBurningVehicle, FALSE)
		SET_PED_CONFIG_FLAG(piPassed, PCF_DisableExplosionReactions, TRUE)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting PCF_RunFromFiresAndExplosions FALSE")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetEleven, ciPed_BSEleven_DontShuffleIntoTurret)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting PCF_PreventAutoShuffleToTurretSeat TRUE")
		SET_PED_CONFIG_FLAG(piPassed, PCF_PreventAutoShuffleToTurretSeat, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetEleven, ciPed_BSEleven_DontCommandeerVehicles)
		SET_PED_CONFIG_FLAG(piPassed, PCF_NotAllowedToJackAnyPlayers, TRUE)
		PRINTLN("[RCC MISSION] - SET_UP_FMMC_PED_RC - Setting PCF_NotAllowedToJackAnyPlayers for ped ", iPedNumber)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetEleven, ciPed_BSEleven_DontSpeak)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting STOP_PED_SPEAKING TRUE")
		STOP_PED_SPEAKING(piPassed, TRUE)
	ENDIF
	
	INT ihealth = GET_PED_HEALTH_FROM_MENU(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iHealth)
	
	#IF NOT IS_NEXTGEN_BUILD
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iLGHealth != -1
			ihealth = GET_PED_HEALTH_FROM_MENU(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iLGHealth)
			PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting ped last gen override health to: ",ihealth," for ped: ",iPedNumber)
		ENDIF
	#ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedPreciseHealth > 100
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - Using the PRECISE health value of: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedPreciseHealth, " for ped: ", iPedNumber)
		ihealth = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedPreciseHealth
	ENDIF
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - starting ihealth = ",ihealth)
		
		IF AM_I_ON_A_HEIST()
			
			IF g_FMMC_STRUCT.iDifficulity = DIFF_EASY
				fDifficultyMod = 0.8
				PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - fDifficultyMod after heist easy mod = ") NET_PRINT_FLOAT(fDifficultyMod) NET_NL()
			ELIF g_FMMC_STRUCT.iDifficulity = DIFF_HARD
				fDifficultyMod = 1.5
				PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - fDifficultyMod after heist hard mod = ") NET_PRINT_FLOAT(fDifficultyMod) NET_NL()
			ELSE
				fDifficultyMod = 1.0
				PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - fDifficultyMod after heist norm mod = ") NET_PRINT_FLOAT(fDifficultyMod) NET_NL()
			ENDIF
			
			fhealth = 100+(fDifficultyMod*(ihealth-100))
			ihealth = ROUND(fhealth)
			PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - heist starting ihealth = ",ihealth," for ped: ",iPedNumber) 
			
		ELIF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_UseHeistEnemyAccuracyAndHealthSettings)
			
			FLOAT fPlayerAddMulti = 0.0, fDifficultyAddMulti = 0.0
			
			SWITCH istartingplayers
				CASE 4
				CASE 3
					fPlayerAddMulti = 0.5
				BREAK
				CASE 2
					fPlayerAddMulti = 0.1
				BREAK
			ENDSWITCH
			
			IF g_FMMC_STRUCT.iDifficulity = DIFF_EASY // No easy mode in Gang Ops heist missions
				fDifficultyAddMulti = -0.25
			ELIF g_FMMC_STRUCT.iDifficulity = DIFF_HARD
				fDifficultyAddMulti = 0.25
			ELSE
				fDifficultyAddMulti = 0.0
			ENDIF
			
			fhealth = ihealth + ((fPlayerAddMulti + fDifficultyAddMulti) * (ihealth - 100))
			ihealth = ROUND(fhealth)
			PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - Gang ops health: istartingplayers ",istartingplayers," means fPlayerAddMulti ",fPlayerAddMulti," + fDifficultyAddMulti ",fDifficultyAddMulti," -> modded ihealth ",ihealth)
			
		ELSE
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetThirteen, ciPED_BSThirteen_IgnoreDifficultyHealthModifier)
				fhealth = ihealth + (0.5*fDifficultyMod*(ihealth-100))
				
				PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - modded fhealth = ",fhealth) 
				
				ihealth = ROUND(fhealth)
				
				#IF IS_DEBUG_BUILD 
					IF lw_fSetMissPedHealth != 1.0
				
						fhealth = ihealth * lw_fSetMissPedHealth
						ihealth = ROUND(fhealth)
						PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - debug health override starting ihealth = ",ihealth) 
						
					ENDIF
				#ENDIF
			ELSE
				PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - Ped ", iPedNumber, " has ciPED_BSThirteen_IgnoreDifficultyHealthModifier SET. iHealth = ", iHealth) 
			ENDIF
		ENDIF
			
		IF ihealth < 101
			ihealth = 101
		ENDIF
		
	ENDIF
	
	PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - natural modded ihealth = ",ihealth) 
	
	ftunablehealth = ihealth*g_sMPTunables.fAiHealthModifier
	
	ihealth = ROUND(ftunablehealth)
	
	PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - tunable modded ihealth = ",ihealth)

	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].fHealthVarMultiplier > 0
		ihealth -= 100
		FLOAT fNewHealth = TO_FLOAT(ihealth)
		FLOAT fPercent = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].fHealthVarMultiplier
		
		fPercent *= 0.01
		fNewHealth *= fPercent
		
		ihealth = ROUND(fNewHealth)
		ihealth += 100
		PRINTLN("[RCC MISSION][MissionVariation] - SET_UP_FMMC_PED_RC - iPedNumber: ", iPedNumber, " Health updated to: ", ihealth, " Using Percent: ", fPercent)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFive, ciPED_BSFive_OneHitKill)
		IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
			ihealth = ciPED_OneHitKill_Health
		ELSE
			ihealth = 101
		ENDIF
			
		SET_ENTITY_MAX_HEALTH(piPassed, ihealth)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - one hit kill set | Setting ped health to ", ihealth)
	ENDIF
	
	SET_ENTITY_HEALTH(piPassed, ihealth)
	
	IF IS_JOB_FORCED_WEAPON_ONLY()
    OR IS_JOB_FORCED_WEAPON_PLUS_PICKUPS()
	OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun = WEAPONTYPE_STUNGUN
	OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun = WEAPONTYPE_DLC_RAILGUN
		SET_PED_DROPS_WEAPONS_WHEN_DEAD(piPassed,FALSE)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting ped to not drop weapon when dead: ",iPedNumber) 
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetThree,ciPED_BSThree_OnlyDamagedByPlayers)
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(piPassed,TRUE)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting ped only damaged by player: ", iPedNumber)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitset, ciPED_BS_SufferCritsOverride)
		SET_PED_SUFFERS_CRITICAL_HITS(piPassed,FALSE)
		SET_RAGDOLL_BLOCKING_FLAGS(piPassed, RBF_BULLET_IMPACT)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting ped immune to critical hits: ", iPedNumber)
	ENDIF
	
	BOOL bBullet = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFour, ciPED_BSFour_BulletProofFlag)
	BOOL bFlame = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFour, ciPED_BSFour_FlameProofFlag)
	BOOL bExplosion = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFour, ciPED_BSFour_ExplosionProofFlag)
	BOOL bCollision = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFour, ciPED_BSFour_CollisionProofFlag)
	BOOL bMelee = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFour, ciPED_BSFour_MeleeProofFlag)
	BOOL bSteam = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFour, ciPED_BSFour_SteamProofFlag)
	BOOL bSmoke = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFour, ciPED_BSFour_SmokeProofFlag)
	SET_ENTITY_PROOFS(piPassed,bBullet,bFlame,bExplosion,bCollision,bMelee,bSteam,DEFAULT,bSmoke)
	
	PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting ped ",iPedNumber," proofs: bullet ",bBullet,", flame ",bFlame,", explosion ",bExplosion,", collision ",bCollision,", melee ",bMelee,", steam ",bSteam,", smoke ",bSmoke)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFour, ciPED_BSFour_DisableRagdolling)
		
		//SET_PED_CAN_RAGDOLL(piPassed, FALSE)
		SET_RAGDOLL_BLOCKING_FLAGS(piPassed,
			RBF_PLAYER_IMPACT | RBF_PLAYER_BUMP | RBF_PLAYER_RAGDOLL_BUMP | RBF_PED_RAGDOLL_BUMP | RBF_ALLOW_BLOCK_DEAD_PED |
			RBF_BULLET_IMPACT | RBF_RUBBER_BULLET | RBF_MELEE | RBF_RUBBER_BULLET | RBF_WATER_JET | 
			RBF_FIRE | RBF_ELECTROCUTION | RBF_EXPLOSION | 
			RBF_FALLING | RBF_DROWNING | 
			RBF_VEHICLE_IMPACT | RBF_VEHICLE_GRAB | RBF_IMPACT_OBJECT)
			
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - iPed: ", iPedNumber," - SET_RAGDOLL_BLOCKING_FLAGS = ciPED_BSFour_DisableRagdolling")
		
	ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetNine, ciPed_BSNine_ImmuneToForcesFireExposionCollision)
	
		SET_RAGDOLL_BLOCKING_FLAGS(piPassed, RBF_FIRE | RBF_PLAYER_IMPACT | RBF_PED_RAGDOLL_BUMP | RBF_PLAYER_BUMP | RBF_PLAYER_IMPACT | RBF_PLAYER_RAGDOLL_BUMP | RBF_EXPLOSION )
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - iPed: ", iPedNumber," - SET_RAGDOLL_BLOCKING_FLAGS = ciPed_BSNine_ImmuneToForcesFireExposionCollision")
		
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetNine, ciPed_BSNine_ImmuneToForcesFireExposionCollision)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - ciPed_BSNine_ImmuneToForcesFireExposionCollision set on ped ",iPedNumber,", disable ragdoll flags, call PCF_IgnoreBeingOnFire")
		SET_PED_UPPER_BODY_DAMAGE_ONLY(piPassed, TRUE)
		SET_PED_CONFIG_FLAG(piPassed, PCF_IgnoreBeingOnFire, TRUE)
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iAccuracy = FMMC_PED_ACCURACY_EXTRA_LOW
		iAccuracy = 5
		eCombatAbility = CAL_POOR
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting ped combat ability extra poor and accuracy 5 for ped: ",iPedNumber)
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iAccuracy = FMMC_PED_ACCURACY_LOW
		iAccuracy = 15
		eCombatAbility = CAL_POOR
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting ped combat ability poor and accuracy 15 for ped: ",iPedNumber)
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iAccuracy = FMMC_PED_ACCURACY_HIGH
		iAccuracy = 40
		eCombatAbility = CAL_PROFESSIONAL
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting ped combat ability pro and accuracy 40 for ped: ",iPedNumber)
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iAccuracy = FMMC_PED_ACCURACY_EXTRA_HIGH
		iAccuracy = 60
		eCombatAbility = CAL_PROFESSIONAL
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting ped combat ability pro and accuracy 60 for ped: ",iPedNumber)
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iAccuracy = FMMC_PED_ACCURACY_100_PERCENT
		iAccuracy = 100
		eCombatAbility = CAL_PROFESSIONAL
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting ped combat ability pro and accuracy 100 for ped: ",iPedNumber)
	ELSE
		iAccuracy = 25
		eCombatAbility = CAL_AVERAGE
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting ped combat ability av and accuracy 25 for ped: ",iPedNumber)
	ENDIF
	
	#IF NOT IS_NEXTGEN_BUILD
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iLGAccuracy != -1
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iLGAccuracy =  FMMC_PED_ACCURACY_EXTRA_LOW
				iAccuracy = 5
				eCombatAbility = CAL_POOR
				PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting ped combat ability LAST GEN extra poor and accuracy 5 for ped: ",iPedNumber)
			ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iLGAccuracy = FMMC_PED_ACCURACY_LOW
				iAccuracy = 15
				eCombatAbility = CAL_POOR
				PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting ped combat ability LAST GEN poor and accuracy 15 for ped: ",iPedNumber)
			ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iLGAccuracy = FMMC_PED_ACCURACY_HIGH
				iAccuracy = 40
				eCombatAbility = CAL_PROFESSIONAL
				PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting ped combat ability LAST GEN pro and accuracy 40 for ped: ",iPedNumber)				
			ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iLGAccuracy = FMMC_PED_ACCURACY_EXTRA_HIGH
				iAccuracy = 60
				eCombatAbility = CAL_PROFESSIONAL
				PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting ped combat ability pro and accuracy 60 for ped: ",iPedNumber)
			ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iLGAccuracy = FMMC_PED_ACCURACY_100_PERCENT
				iAccuracy = 100
				eCombatAbility = CAL_PROFESSIONAL
				PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting ped combat ability pro and accuracy 100 for ped: ",iPedNumber)
			ELSE
				iAccuracy = 25
				eCombatAbility = CAL_AVERAGE
				PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting ped combat ability LAST GEN av and accuracy 25 for ped: ",iPedNumber)			
			ENDIF
		ENDIF
	#ENDIF
	
	FLOAT fAccuracy
	
	IF NOT GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_UseHeistEnemyAccuracyAndHealthSettings)
		fAccuracy = iAccuracy + (0.25*iAccuracy*fDifficultyMod)
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iAccuracy != FMMC_PED_ACCURACY_100_PERCENT
			fAccuracy = fAccuracy * fLesMultiplier
			
			iAccuracy = ROUND(fAccuracy)
		ENDIF
		
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - normal modded iAccuracy = ",iAccuracy)
		
	ELSE
		FLOAT fDifficultyMulti
		
		IF g_FMMC_STRUCT.iDifficulity = DIFF_EASY // No easy mode in Gang Ops heist missions
			fDifficultyMulti = 0.8
		ELIF g_FMMC_STRUCT.iDifficulity = DIFF_HARD
			fDifficultyMulti = 1.2
		ELSE // Normal
			fDifficultyMulti = 1.0
		ENDIF
		
		fAccuracy = iAccuracy * fDifficultyMulti
		iAccuracy = ROUND(fAccuracy)
		
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - Gang Ops w fDifficultyMulti ",fDifficultyMulti," -> modded iAccuracy = ",iAccuracy)
	ENDIF
	
	#IF IS_DEBUG_BUILD 
		IF lw_fSetMissPedAccuracy != 1.0
			
			faccuracy = iAccuracy * lw_fSetMissPedAccuracy
			
			iAccuracy = ROUND(faccuracy)
			
			PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - debug accuracy override starting iAccuracy = ",iAccuracy)
			
			IF iAccuracy <=15
				eCombatAbility = CAL_POOR
			ELIF iAccuracy <=25
				eCombatAbility = CAL_AVERAGE
			ELSE
				eCombatAbility = CAL_PROFESSIONAL
			ENDIF
		ENDIF
	#ENDIF
	
	IF iAccuracy < 0
		iAccuracy = 0
	ENDIF
	IF iAccuracy > 100
		iAccuracy = 100
	ENDIF
	
	SET_PED_COMBAT_ABILITY(piPassed, eCombatAbility)
	SET_PED_ACCURACY(piPassed, iAccuracy)
	PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - Setting ped ",iPedNumber," with accuracy ",iAccuracy,", combat ability ",ENUM_TO_INT(eCombatAbility))
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetSeven, ciPED_BSSeven_LoudPropellorPlanes)
		START_AUDIO_SCENE("Speed_Race_Desert_Airport_Scene")
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_RocketAccuracy")
		fBreakLockAngle = g_fDebugBreakLockAngle
		fBreakLockAngleClose = g_fDebugBreakLockAngleClose
		fTurnRateModifier =  g_fDebugTurnModifier
		fBreakLockCloseDistance = g_fDebugLockCloseDitance
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - Overriding ped homing missile details from widget iPedNumber = ", iPedNumber, " CCF_HOMING_ROCKET_BREAK_LOCK_ANGLE = ", fBreakLockAngle, " CCF_HOMING_ROCKET_BREAK_LOCK_ANGLE_CLOSE = ", fBreakLockAngleClose,  " CCF_HOMING_ROCKET_BREAK_LOCK_CLOSE_DISTANCE = ", fBreakLockCloseDistance, " CCF_HOMING_ROCKET_TURN_RATE_MODIFIER = ", fTurnRateModifier) 
		
		SET_COMBAT_FLOAT(piPassed, CCF_HOMING_ROCKET_BREAK_LOCK_ANGLE, fBreakLockAngle)
		SET_COMBAT_FLOAT(piPassed, CCF_HOMING_ROCKET_BREAK_LOCK_ANGLE_CLOSE, fBreakLockAngleClose)
		SET_COMBAT_FLOAT(piPassed, CCF_HOMING_ROCKET_BREAK_LOCK_CLOSE_DISTANCE, fBreakLockCloseDistance)
		SET_COMBAT_FLOAT(piPassed, CCF_HOMING_ROCKET_TURN_RATE_MODIFIER, fTurnRateModifier)
	ENDIF
	#ENDIF // IS_DEBUG_BUILD
	 // FEATURE_HEIST_PLANNING
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetThree,ciPED_BSThree_LoweredShootRate)
		IF g_FMMC_STRUCT.iDifficulity = DIFF_EASY //default shoot rate is 60
			SET_PED_SHOOT_RATE(piPassed,30)
		ELSE
			SET_PED_SHOOT_RATE(piPassed,50)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFive, ciPED_BSFive_DontBlindFireFromCover)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - Setting ped ",iPedNumber," as unable to blind fire from cover")
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_BLIND_FIRE_IN_COVER, FALSE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFive, ciPED_BSFive_UseStrictLOSAimChecks)
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_USE_DEFAULT_BLOCKED_LOS_POSITION_AND_DIRECTION, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_REQUIRES_LOS_TO_AIM, TRUE)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - Setting ped ",iPedNumber," with CA_USE_DEFAULT_BLOCKED_LOS_POSITION_AND_DIRECTION and CA_REQUIRES_LOS_TO_AIM")
	ENDIF
	
	SET_PED_DIES_WHEN_INJURED(piPassed,TRUE)
	SET_PED_KEEP_TASK(piPassed,TRUE)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetThree,ciPED_BSThree_DisableEvasiveDive)
		SET_PED_CAN_EVASIVE_DIVE(piPassed,FALSE)
	ENDIF
	
	IF IS_PED_IN_ANY_HELI(piPassed)
	OR IS_PED_IN_ANY_PLANE(piPassed)
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetThirteen, ciPed_BSThirteen_UseSmallPlaneAndHeliSpookRange)
			PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - Setting heli or plane seeing range 299, ped: ",iPedNumber) 
			SET_PED_SEEING_RANGE(piPassed, 299)
		ELSE			
			PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - Setting heli or plane seeing range 100, ped: ",iPedNumber, " ciPed_BSThirteen_UseSmallPlaneAndHeliSpookRange")
			SET_PED_SEEING_RANGE(piPassed, 100)
		ENDIF
		
		IF GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(piPassed)) = piPassed			
			IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(piPassed)) = HUNTER
				SET_PED_FIRING_PATTERN(piPassed, INT_TO_ENUM(FIRING_PATTERN_HASH, HASH("FIRING_PATTERN_AKULA_BARRAGE")))
				PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - Setting heli or plane ped to FIRING_PATTERN_AKULA_BARRAGE, ped: ",iPedNumber) 
			ELSE
				SET_PED_FIRING_PATTERN(piPassed, FIRING_PATTERN_BURST_FIRE_HELI)
				PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - Setting heli or plane ped to FIRING_PATTERN_BURST_FIRE_HELI, ped: ",iPedNumber) 
			ENDIF
		ENDIF
		IF NOT IS_PED_A_SCRIPTED_COP(iPedNumber)
			SET_PED_TARGET_LOSS_RESPONSE(piPassed, TLR_NEVER_LOSE_TARGET)
			PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - Setting heli or plane ped to TLR_NEVER_LOSE_TARGET, ped: ",iPedNumber) 
		ENDIF
	ENDIF
	
	FLOAT fInformRange = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iInformRange)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetThree,ciPED_BSThree_AggroFailHasDelay)
		fInformRange = 5.0
	ENDIF
	
	PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - Setting ped ",iPedNumber," with inform_respected_friends range of ",fInformRange)
	SET_PED_TO_INFORM_RESPECTED_FRIENDS(piPassed, fInformRange, 50)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFive, ciPED_BSFive_UseInformRangeWhenReceivingEvents)
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_USE_MAX_SENSE_RANGE_WHEN_RECEIVING_EVENTS, TRUE)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - Setting ped ",iPedNumber," with CA_USE_MAX_SENSE_RANGE_WHEN_RECEIVING_EVENTS")
	ELSE
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - NOT Setting ped ",iPedNumber," with CA_USE_MAX_SENSE_RANGE_WHEN_RECEIVING_EVENTS")
	ENDIF
	
	IF NOT IS_THIS_A_HEIST_CREATION()
		SET_ENTITY_VISIBLE(piPassed,bVisible)
	ELSE
		SET_ENTITY_VISIBLE(piPassed,TRUE)
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].mn = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_bogdangoon"))
		PRINTLN("[LM] SET_UP_FMMC_PED_RC Adding PVG for model mp_m_bogdangoon and using G_M_M_X17_RSO_PVG")
		SET_PED_VOICE_GROUP(piPassed, GET_HASH_KEY("G_M_M_X17_RSO_PVG"))
	ENDIF	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].mn = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_avongoon"))
		PRINTLN("[LM] SET_UP_FMMC_PED_RC Adding PVG for model mp_m_avongoon and using G_M_M_X17_AGuard_PVG")
		SET_PED_VOICE_GROUP(piPassed, GET_HASH_KEY("G_M_M_X17_AGuard_PVG"))
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_UseBlackopsVoice)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].mn = INT_TO_ENUM(MODEL_NAMES, HASH("S_M_Y_WestSec_01"))
			PRINTLN("[LM] SET_UP_FMMC_PED_RC Adding PVG for model S_M_Y_WestSec_01 and using MP_BLACKOPS_R2PVG")
			SET_PED_VOICE_GROUP(piPassed, GET_HASH_KEY("MP_BLACKOPS_R2PVG"))
		ENDIF
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].mn = INT_TO_ENUM(MODEL_NAMES, HASH("MP_G_M_Pros_01"))
			PRINTLN("[LM] SET_UP_FMMC_PED_RC Adding PVG for model MP_G_M_Pros_01 and using MP_BLACKOPS_R2PVG")
			SET_PED_VOICE_GROUP(piPassed, GET_HASH_KEY("MP_BLACKOPS_R2PVG"))
		ENDIF
	ENDIF
	
	INT i
	TEXT_LABEL_63 tlAmbientVoice
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].mn = INT_TO_ENUM(MODEL_NAMES, HASH("MP_M_G_VagFun_01"))
		PRINTLN("[JS] SET_UP_FMMC_PED_RC Adding voice for model MP_M_G_VagFun_01")
		tlAmbientVoice = "GANG_MEXGOON_R2PVG"
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].mn = INT_TO_ENUM(MODEL_NAMES, HASH("G_M_M_Chemwork_01"))
	AND GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		PRINTLN("[LM] SET_UP_FMMC_PED_RC Adding voice for model G_M_M_Chemwork_01 because it's GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION")
		tlAmbientVoice = "S_M_M_CIASEC_01_WHITE_MINI_01"
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].mn = INT_TO_ENUM(MODEL_NAMES, HASH("ig_taocheng2"))
		PRINTLN("[LM] SET_UP_FMMC_PED_RC Adding voice for model ig_TAOCHENG2 because it sometimes sounds like lester...")
		tlAmbientVoice = "CHENG"
	ENDIF
		
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].mn = INT_TO_ENUM(MODEL_NAMES, HASH("ig_paper"))
	AND GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		tlAmbientVoice = "UNITEDPAPER"
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iAmbientVoice > -1
		tlAmbientVoice = g_FMMC_STRUCT.sSpeakers[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iAmbientVoice]
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iAmbientVoice > -1, ped: ",iPedNumber," to voice num: ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iAmbientVoice, " voice: ",tlAmbientVoice )
	ENDIF

	IF NOT IS_STRING_NULL_OR_EMPTY(tlAmbientVoice)	
		SET_AMBIENT_VOICE_NAME(piPassed,tlAmbientVoice)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting ped ambient voice for ped not in dialogue struct : ",iPedNumber," to voice: ",tlAmbientVoice)	
	ELSE
		FOR i = 0  TO (FMMC_MAX_DIALOGUES-1)
			PRINTLN("[RCC MISSION] [DLG] SET_UP_FMMC_PED_RC - i = ", i)
			IF g_FMMC_STRUCT.sDialogueTriggers[i].iPedVoice = iPedNumber
				PRINTLN("[RCC MISSION] [DLG] SET_UP_FMMC_PED_RC - i = ", i)

				tlAmbientVoice = GET_PED_AMBIENT_VOICE(i)

				IF NOT IS_STRING_NULL_OR_EMPTY(tlAmbientVoice)
					SET_AMBIENT_VOICE_NAME(piPassed,tlAmbientVoice)
					PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting ped ambient voice for ped: ",iPedNumber," to voice: ",tlAmbientVoice)
				ENDIF
			ENDIF
			IF g_FMMC_STRUCT.sDialogueTriggers[i].iSecondPedVoice = iPedNumber
				PRINTLN("[RCC MISSION] [DLG] SET_UP_FMMC_PED_RC - i = ", i)

				tlAmbientVoice = GET_PED_AMBIENT_VOICE(i)

				IF NOT IS_STRING_NULL_OR_EMPTY(tlAmbientVoice)
					SET_AMBIENT_VOICE_NAME(piPassed,tlAmbientVoice)
					PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting second ped ambient voice for ped: ",iPedNumber," to voice: ",tlAmbientVoice)
				ENDIF
			ENDIF
		ENDFOR
	
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].mn = INT_TO_ENUM(MODEL_NAMES, HASH("S_M_Y_BLACKOPS_01"))
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetEleven, ciPed_BSEleven_UseAlternateComponentsForPedOutfit)
		PRINTLN("[LM] SET_UP_FMMC_PED_RC - iPedNumber: ", iPedNumber, " Overriding the components of S_M_Y_BLACKOPS_01.")
		
		INT iRand = GET_RANDOM_INT_IN_RANGE(0,3)
		SWITCH iRand
			CASE 0
				PRINTLN("[LM] SET_UP_FMMC_PED_RC - iPedNumber: ", iPedNumber, " Overriding the components of S_M_Y_BLACKOPS_01 (0).")
				SET_PED_COMPONENT_VARIATION(piPassed, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0)				
			BREAK
			
			CASE 1
				PRINTLN("[LM] SET_UP_FMMC_PED_RC - iPedNumber: ", iPedNumber, " Overriding the components of S_M_Y_BLACKOPS_01 (1).")
				SET_PED_COMPONENT_VARIATION(piPassed, INT_TO_ENUM(PED_COMPONENT,8), 0, 1, 0) 				
			BREAK

			CASE 2
				PRINTLN("[LM] SET_UP_FMMC_PED_RC - iPedNumber: ", iPedNumber, " Overriding the components of S_M_Y_BLACKOPS_01 (2).")
				SET_PED_COMPONENT_VARIATION(piPassed, INT_TO_ENUM(PED_COMPONENT,8), 0, 2, 0)				
			BREAK
		ENDSWITCH
	ENDIF

	IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedPerceptionSettings != ciPED_PERCEPTION_DEFAULT)
		
		FLOAT fCentreRange = GET_FMMC_PED_PERCEPTION_RANGE(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedPerceptionSettings, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].fPedPerceptionCentreRange)
		FLOAT fCentreAngle = GET_FMMC_PED_PERCEPTION_FOV(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedPerceptionSettings, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].fPedPerceptionCentreAngle)
		FLOAT fPeripheralAngle = GET_FMMC_PED_PERIPHERAL_EXTREME(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedPerceptionSettings)

		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting ped ", iPedNumber, " perception profile = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedPerceptionSettings)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - fCentreRange: ", fCentreRange)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - fCentreAngle: ", fCentreAngle)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - fPeripheralAngle: ", fPeripheralAngle)
		
		SET_PED_VISUAL_FIELD_PROPERTIES(piPassed, fCentreRange, DEFAULT, fCentreAngle, -fPeripheralAngle, fPeripheralAngle)
		
		IF fCentreRange >= 90
			SET_PED_COMBAT_RANGE(piPassed, CR_FAR)
		ENDIF
		
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitset, ciPED_BS_ExtendedLOS)
		SET_PED_SEEING_RANGE(piPassed, 150) 
		SET_PED_COMBAT_RANGE(piPassed, CR_FAR)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - ped set as long range combat: ",iPedNumber)
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iMaxShootingRange != 0
		SET_COMBAT_FLOAT( piPassed, CCF_MAX_SHOOTING_DISTANCE, TO_FLOAT( g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iMaxShootingRange ) )
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - ped CCF_MAX_SHOOTING_DISTANCE for ped: ",iPedNumber, " iMaxShootingRange = ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iMaxShootingRange)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFour, ciPED_BSFour_PreventReactingToSilencedCloneBullets)
		SET_PED_CONFIG_FLAG(piPassed, PCF_PreventReactingToSilencedCloneBullets, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFour, ciPED_BSFour_DisableScreams)
		DISABLE_PED_PAIN_AUDIO(piPassed, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFive, ciPED_BSFive_DontPlayHeadOnHornAnimWhenDiesInVeh)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - ped set to not play head on horn anim when dying in a vehicle: ",iPedNumber)
		SET_PED_PLAYS_HEAD_ON_HORN_ANIM_WHEN_DIES_IN_VEHICLE(piPassed, FALSE)
		SET_PED_CONFIG_FLAG(piPassed, PCF_DisableHornAudioWhenDead, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFour, ciPED_BSFour_Stop_Weapon_Firing_When_Dropped)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - ped set to stop weapon firing when dropped: ",iPedNumber)
		STOP_PED_WEAPON_FIRING_WHEN_DROPPED(piPassed)
		//SET_PED_CONFIG_FLAG(piPassed, PCF_DisableBlindFiringInShotReactions, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetEleven, ciPED_BSEleven_UsePerceptionConeForAimedAtEvents)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - ped set to use perception for aimed at events: ",iPedNumber)
		SET_PED_CONFIG_FLAG(piPassed, PCF_UseTargetPerceptionForCreatingAimedAtEvents, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFive, ciPED_BSFive_StartDead)
		IF HAS_ANIM_DICT_LOADED(GET_DEAD_PED_ANIM_DICT_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iDeadAnim))
			
			VECTOR vAnimPos = vPos
			
			SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iDeadAnim
				CASE ci_DEAD_PED_ANIM_FBI_CPR_IDLE
					vAnimPos.z -= 1.025
					PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - Dead Ped ",iPedNumber," - Moving down 1.025 to play anim")
				BREAK
				CASE ci_DEAD_PED_ANIM_IAA_MORGUE_SEARCH
					vAnimPos.z -= 2.06
					PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - Dead Ped ",iPedNumber," - Moving down 2 to play anim")
				BREAK
			ENDSWITCH
			
			PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - Dead Ped ",iPedNumber," - Using AnimDict: ", GET_DEAD_PED_ANIM_DICT_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iDeadAnim), " AnimName: ", GET_DEAD_PED_ANIM_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iDeadAnim), " g_FMMC_STRUCT_ENTITIES.sPlacedPed[",iPedNumber,"].iDeadAnim: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iDeadAnim)
			PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - Dead Ped ",iPedNumber,", call TASK_PLAY_ANIM_ADVANCED at vAnimPos ",vAnimPos)
			TASK_PLAY_ANIM_ADVANCED(piPassed, GET_DEAD_PED_ANIM_DICT_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iDeadAnim), GET_DEAD_PED_ANIM_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iDeadAnim), vAnimPos, GET_ENTITY_ROTATION(piPassed), INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS|AF_ENDS_IN_DEAD_POSE|AF_OVERRIDE_PHYSICS|AF_EXTRACT_INITIAL_OFFSET|AF_NOT_INTERRUPTABLE, 0.99) 
			FORCE_PED_AI_AND_ANIMATION_UPDATE(piPassed)
			
		#IF IS_DEBUG_BUILD
		ELSE
			CASSERTLN(DEBUG_CONTROLLER, "[RCC MISSION] SET_UP_FMMC_PED_RC - Dead Ped ",iPedNumber," anim dictionary has not loaded!")
			PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - Dead Ped ",iPedNumber," anim dictionary has not loaded!")
			PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - Dead Ped ",iPedNumber," - Using AnimDict: ", GET_DEAD_PED_ANIM_DICT_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iDeadAnim), " AnimName: ", GET_DEAD_PED_ANIM_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iDeadAnim), " g_FMMC_STRUCT_ENTITIES.sPlacedPed[",iPedNumber,"].iDeadAnim: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iDeadAnim)
		#ENDIF
		ENDIF
		
		SET_ENTITY_CAN_BE_DAMAGED(piPassed, TRUE)
		SET_ENTITY_INVINCIBLE(piPassed, TRUE)
		SET_PED_CAN_RAGDOLL(piPassed, FALSE)
		FREEZE_ENTITY_POSITION(piPassed, TRUE)
		
		SET_ENTITY_DYNAMIC(piPassed, FALSE)
		
		SET_PED_CONFIG_FLAG(piPassed, PCF_BlockDroppingHealthSnacksOnDeath, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFifteen, ciPED_BSFifteen_StartDeadWithPhysics)
		
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - Dead Ped ", iPedNumber, " - Using Physics: ciPED_BSFifteen_StartDeadWithPhysics")
		
		TASK_PLAY_ANIM_ADVANCED(piPassed, "dead", "dead_a", vPos, GET_ENTITY_ROTATION(piPassed), INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_ENDS_IN_DEAD_POSE|AF_EXTRACT_INITIAL_OFFSET, 0.99) 
		FORCE_PED_AI_AND_ANIMATION_UPDATE(piPassed)
		
		SET_ENTITY_CAN_BE_DAMAGED(piPassed, FALSE)
		SET_ENTITY_INVINCIBLE(piPassed, FALSE)
		SET_PED_CAN_RAGDOLL(piPassed, TRUE)
		
		SET_PED_CONFIG_FLAG(piPassed, PCF_BlockDroppingHealthSnacksOnDeath, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetEleven, ciPED_BSEleven_BlockSnacksDrop)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - Setting PCF_BlockDroppingHealthSnacksOnDeath on ped ",iPedNumber)
		SET_PED_CONFIG_FLAG(piPassed, PCF_BlockDroppingHealthSnacksOnDeath, TRUE)
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetEight, ciPed_BSEight_RemoveArmourOnCreation)
		SET_PED_ARMOUR(piPassed, 0)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - Removing Ped armour on Creation via: ciPed_BSEight_RemoveArmourOnCreation, for ped: ", iPedNumber)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciPEDS_CANNOT_HAVE_ARMOUR)
		SET_PED_ARMOUR(piPassed, 0)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - Global Ped Override set for Armour. Removing it on creation., for ped: ", iPedNumber)
	ENDIF
//	
//	SET_PED_SEEING_RANGE(piPassed, 120) 
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iInventoryBS > 0
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iInventoryBS,ciPED_INV_Briefcase_prop_ld_case_01)
		AND HAS_MODEL_LOADED(PROP_LD_CASE_01)
		AND HAS_ANIM_DICT_LOADED("weapons@misc@jerrycan@mp_male")
		AND CAN_REGISTER_MISSION_OBJECTS(1)
			RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()+1)
			
			VECTOR vBriefcaseOffset = <<0.094,0.020,-0.005>>
			
			VECTOR vBriefcaseRot = << -92.240, 63.640, 150.240 >>
			
			NETWORK_INDEX niBriefcase
			
			CREATE_NET_OBJ(niBriefcase,PROP_LD_CASE_01,vPos)
			SET_ENTITY_LOD_DIST(NET_TO_ENT(niBriefcase),500)
			ATTACH_ENTITY_TO_ENTITY(NET_TO_ENT(niBriefcase),piPassed,GET_PED_BONE_INDEX(piPassed,BONETAG_PH_R_HAND),vBriefcaseOffset,vBriefcaseRot,TRUE)
			
			//TASK_PLAY_ANIM(piPassed,"anim@heists@biolab@","Contact_Gives_Codes",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_UPPERBODY|AF_LOOPING|AF_SECONDARY)
			
			ANIM_DATA LocalAnimData, EmptyAnimData
			
			LocalAnimData.type = APT_SINGLE_ANIM
			LocalAnimData.dictionary0 = "weapons@misc@jerrycan@mp_male"
			LocalAnimData.anim0 = "idle"
			
			LocalAnimData.flags = AF_LOOPING | AF_UPPERBODY | AF_SECONDARY | AF_NOT_INTERRUPTABLE | AF_HIDE_WEAPON
			
			LocalAnimData.rate0 = 0.5
			
			LocalAnimData.filter = GET_HASH_KEY("BONEMASK_ARMONLY_R")
			
			TASK_SCRIPTED_ANIMATION(piPassed, LocalAnimData, EmptyAnimData, EmptyAnimData, INSTANT_BLEND_DURATION, SLOW_BLEND_DURATION)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(PROP_LD_CASE_01)
			REMOVE_ANIM_DICT("anim@heists@biolab@")
			
		ENDIF
		//More inventory items go here:
	ENDIF

	SET_CREATOR_ID_INT_DECOR_ON_ENTITY(GET_ENTITY_FROM_PED_OR_VEHICLE(piPassed),iPedNumber)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iRespawnOnRuleChangeBS > 0
		SET_ALLOW_MIGRATE_TO_SPECTATOR(piPassed, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetEight, ciPed_BSEight_DontDropWeaponOnDeath)
	OR IS_BIT_SET(g_FMMC_STRUCT.iWepRestBS, REST_DISABLE_PED_DROP)
		SET_PED_DROPS_WEAPONS_WHEN_DEAD(piPassed, FALSE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetNine, ciPed_BSNine_CantBeDamagedByPlayers)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting cant be damaged by players for ped: ", iPedNumber)
		SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(piPassed, FALSE, rgfm_PlayerTeam[0])
		SET_PED_CAN_BE_TARGETTED_BY_TEAM(piPassed, 0, FALSE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetTwelve, ciPed_BSTwelve_OnlyDamagedByPlayers)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting Can Only be damaged by players for ped: ", iPedNumber)
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(piPassed, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetEleven, ciPED_BSEleven_BlockHomingLockon)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting cant be locked on for ped: ", iPedNumber)
		SET_PED_CONFIG_FLAG(piPassed, PCF_DisableHomingMissileLockon, TRUE)
	ENDIF
			
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetTwelve, ciPed_BSTwelve_IgnoreMaxNumberOfMeleeCombatants)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting PCF_ForceIgnoreMaxMeleeActiveSupportCombatants for iPed: ", iPedNumber)
		SET_PED_CONFIG_FLAG(piPassed, PCF_ForceIgnoreMaxMeleeActiveSupportCombatants, TRUE)
	ENDIF	
	
	// If any of these are set, then we want to trigger them all.
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iEntityRelGroupDamageRestrictions <> 0
		INT iEntDmgRestriction = 0
		FOR iEntDmgRestriction = 0 TO OPTION_ENTITY_CAN_ONLY_BE_DAMAGE_BY_RELGR_MAX
			PRINTLN("[LM][RCC MISSION] SET_UP_FMMC_PED_RC - ped: ", iPedNumber, " Relationship Group Number: ", iEntDmgRestriction, " Set: ", IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iEntityRelGroupDamageRestrictions, iEntDmgRestriction))
			SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(piPassed, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iEntityRelGroupDamageRestrictions, iEntDmgRestriction), GET_REL_GROUP_HASH_FROM_INT(iEntDmgRestriction))			
		ENDFOR
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetTwelve, ciPed_BSTwelve_DisableRagdollingFromPlayerImpacts)	
		PRINTLN("[RCC MISSION] SET_UP_FMMC_PED_RC - setting ped to ignore player impacts causing ragdoll (SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT) ped: ", iPedNumber)
		SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(piPassed, FALSE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFourteen, ciPED_BSFourteen_ForceActionModeUnholsterWeapon)
		PRINTLN("[LM][RCC MISSION] SET_UP_FMMC_PED_RC - ped: ", iPedNumber, " (ForceActionModeUnholsterWeapon) - Setting SET_PED_USING_ACTION_MODE to DEFAULT_ACTION, and equipping current weapon.")		
		SET_PED_USING_ACTION_MODE(piPassed, TRUE, -1, "DEFAULT_ACTION")
		SET_CURRENT_PED_WEAPON(piPassed,g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun, TRUE)
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_UN_Wanted_Level_bug")
		PRINTLN("[LM][RCC MISSION] SET_UP_FMMC_PED_RC - ped: ", iPedNumber, " Setting PCF_DontInfluenceWantedLevel...")
		SET_PED_CONFIG_FLAG(piPassed, PCF_DontInfluenceWantedLevel, TRUE)
	ENDIF
	#ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iPedBitsetFourteen, ciPED_BSFourteen_DisablePedFromBeingTargeted)
		PRINTLN("[LM][RCC MISSION] SET_UP_FMMC_PED_RC - ped: ", iPedNumber, " Using ciPED_BSFourteen_DisablePedFromBeingTargeted...")
		SET_PED_CAN_BE_TARGETTED(piPassed, FALSE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_PedSpecialTracking)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iContinuityId > -1
		AND IS_LONG_BIT_SET(sLEGACYMissionContinuityVars.iPedSpecialBitset, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iContinuityId)
			SWITCH g_FMMC_STRUCT.eContinuitySpecialPedType
				CASE FMMC_MISSION_CONTINUITY_PED_TRACKING_TYPE_TRANQUILIZED
					PRINTLN("[JS][CONTINUITY] - SET_UP_FMMC_PED_RC - ped: ", iPedNumber, " Ped was tranquilised on a previous strand mission!")
					SET_PED_USING_ACTION_MODE(piPassed, FALSE, -1, "DEFAULT_ACTION")	
					SET_PED_MOVEMENT_CLIPSET(piPassed, "move_m@drunk@moderatedrunk", 0.75)					
					SET_PED_COMBAT_ABILITY(piPassed, CAL_POOR)					
				BREAK
				DEFAULT
					PRINTLN("[JS][CONTINUITY] - SET_UP_FMMC_PED_RC - ped: ", iPedNumber, " Tracking special but type is invalid")
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
	
ENDPROC

PROC SET_UP_FMMC_OBJ_RC(OBJECT_INDEX oiPassed, INT iobj,NETWORK_INDEX &niPeds[], FMMC_LEGACY_MISSION_CONTINUITY_VARS &sLEGACYMissionContinuityVars, BOOL bOnGround = TRUE,BOOL bVisible = TRUE, BOOL bSecondary = FALSE, BOOL bSpawnNear = FALSE, BOOL bIsInitialSpawn = FALSE)
	
	VECTOR vPos
	
	IF bOnGround
	AND NOT bSpawnNear
		vPos = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vPos
		FLOAT fHeading = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].fHead
		
		IF bSecondary
		AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vSecondSpawnPos)
			vPos = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vSecondSpawnPos
			fHeading = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].fSecondHeading
		ENDIF
				
		SET_ENTITY_COORDS_NO_OFFSET(oiPassed, vPos)
		SET_ENTITY_HEADING(oiPassed, fHeading)
	ENDIF
	
	IF (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetFour, cibsOBJ4_UseRandomPoolPosAsRespawnSpawn) AND NOT bIsInitialSpawn)
	OR (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetFour, cibsOBJ4_UseRandomPoolPosAsInitialSpawn) AND bIsInitialSpawn)
		FLOAT fNewHeading = 0.0
		INT iSpawnPool = GET_RANDOM_SELECTION_FROM_SPAWN_POOL(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].sWarpLocationSettings.vPosition)		
		fNewHeading = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].sWarpLocationSettings.fHeading[iSpawnPool]
		SET_ENTITY_HEADING(oiPassed, fNewHeading)
		PRINTLN( "[RCC MISSION] SET_UP_FMMC_OBJ_RC - Grabbing random heading from pool for obj ", iobj, " iSpawnPool: ", iSpawnPool, " fNewHeading: ", fNewHeading)
	ENDIF

	NET_PRINT(" object created at: ") NET_PRINT_VECTOR(vPos) NET_NL()
	
	BOOL bFireProof = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetTwo, cibsOBJ2_FireProof)
	
	SET_ENTITY_INVINCIBLE(oiPassed, FALSE)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_OverrideDefaultObjectProofs)
		BOOL bBullet = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_BulletProofFlag)
		BOOL bFlame = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_FlameProofFlag)
		BOOL bExplosion = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_ExplosionProofFlag)
		BOOL bCollision = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_CollisionProofFlag)
		BOOL bMelee = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_MeleeProofFlag)
		BOOL bSteam = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_SteamProofFlag)
		BOOL bSmoke = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_SmokeProofFlag)
		SET_ENTITY_PROOFS(oiPassed,bBullet,bFlame,bExplosion,bCollision,bMelee,bSteam,DEFAULT,bSmoke)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_OBJ - setting obj ",iObj," proofs: bullet ",bBullet,", flame ",bFlame,", explosion ",bExplosion,", collision ",bCollision,", melee ",bMelee,", steam ",bSteam,", smoke ",bSmoke)
	ELSE
		PRINTLN("[RCC MISSION] SET_UP_FMMC_OBJ - cibsOBJ3_OverrideDefaultObjectProofs is NOT SET for obj ", iobj)
		SET_ENTITY_PROOFS(oiPassed, FALSE, bFireProof, FALSE, TRUE, TRUE)
	ENDIF

	IF (g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn = INT_TO_ENUM(MODEL_NAMES, HASH("pil_Prop_Pilot_Icon_01")))
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn = INT_TO_ENUM(MODEL_NAMES, HASH("prop_ic_cp_bag"))
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn = INT_TO_ENUM(MODEL_NAMES, HASH("ar_prop_ar_cp_bag"))
	OR DOES_PICKUP_NEED_BIGGER_RADIUS(iobj)
		SET_ENTITY_LOD_DIST(oiPassed,1999)
	ELSE
		SET_ENTITY_LOD_DIST(oiPassed,150)
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectHealth = 5
		SET_ENTITY_INVINCIBLE(oiPassed, TRUE)
		SET_ENTITY_PROOFS(oiPassed, TRUE,TRUE,TRUE,TRUE,TRUE)
		SET_ENTITY_HEALTH(oiPassed, 500)	
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectHealth = 4
		SET_ENTITY_HEALTH(oiPassed, 400)	
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectHealth = 3
		SET_ENTITY_HEALTH(oiPassed, 300)	
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectHealth = 2
		SET_ENTITY_HEALTH(oiPassed, 200)	
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectHealth = 1
		SET_ENTITY_HEALTH(oiPassed, 100)	
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectHealth = -1
		SET_ENTITY_HEALTH(oiPassed, 1)		
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn = HEI_PROP_HEIST_BINBAG 
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn = HEI_PROP_HEI_TIMETABLE 
		SET_OBJECT_TARGETTABLE(oiPassed, FALSE)
	ELSE
		SET_OBJECT_TARGETTABLE(oiPassed, TRUE)
	ENDIF
	
	IF( g_FMMC_STRUCT_ENTITIES.sPlacedObject[ iobj ].mn = HEI_PROP_HEI_TIMETABLE ) // override timetable position
		IF( GET_DISTANCE_BETWEEN_ENTITY_AND_COORD( oiPassed, <<438.1272,-995.6921,30.6290>> ) < 5.0 )
			CPRINTLN( DEBUG_SIMON, "SET_UP_FMMC_OBJ_RC - Overriding HEI_PROP_HEI_TIMETABLE position" )
			g_FMMC_STRUCT_ENTITIES.sPlacedObject[ iobj ].vPos = <<438.0482, -995.7059, 30.6025>> // <<438.13, -995.69, 30.595>>
			g_FMMC_STRUCT_ENTITIES.sPlacedObject[ iobj ].vRot = <<0.0, 0.0, -21.18>>
			SET_ENTITY_COORDS( oiPassed, g_FMMC_STRUCT_ENTITIES.sPlacedObject[ iobj ].vPos, FALSE, FALSE, FALSE, FALSE )
			SET_ENTITY_ROTATION( oiPassed, g_FMMC_STRUCT_ENTITIES.sPlacedObject[ iobj ].vRot, DEFAULT, FALSE )
			IF SHOULD_OBJECT_BE_FROZEN(oiPassed)
				FREEZE_ENTITY_POSITION( oiPassed, TRUE )
				SET_ENTITY_PROOFS( oiPassed, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, TRUE )
			ENDIF
		ENDIF
	ENDIF

	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSet, cibsOBJ_ShowChevron)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_MINI_GAME_OBJECT_RC - adding chevron to object")
		SET_PICKUP_OBJECT_ARROW_MARKER(oiPassed, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciALPHA_OUT_PACKAGES_NOT_COLLECTABLE)
		PRINTLN("SET_UP_FMMC_OBJ_RC - SET_PICKUP_OBJECT_TRANSPARENT_WHEN_UNCOLLECTABLE - TRUE")
		SET_PICKUP_OBJECT_TRANSPARENT_WHEN_UNCOLLECTABLE(oiPassed, TRUE)
	ENDIF
	
	SET_ENTITY_VISIBLE(oiPassed, bVisible)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iRespawnOnRuleChangeBS > 0
		SET_ALLOW_MIGRATE_TO_SPECTATOR(oiPassed, TRUE)
	ENDIF
	
	IF NOT bOnGround
	AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectAttachmentPed > -1
		ATTACH_PORTABLE_PICKUP_TO_PED(oiPassed, NET_TO_PED(niPeds[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectAttachmentPed]))
		NET_PRINT("attaching pickup: ") NET_PRINT_INT(iobj) NET_PRINT(" to ped: ")NET_PRINT_INT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectAttachmentPed) NET_NL()
	ENDIF
	
	IF NOT IS_THIS_A_SPECIAL_PICKUP(iobj)
		INT iTeamLoop
		FOR iTeamLoop = 0 TO (g_FMMC_STRUCT.iNumberOfTeams - 1)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iPriority[iTeamLoop] > 0
			OR NOT DOES_OBJECT_RULE_TYPE_REQUIRE_PICKUP(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iRule[iTeamLoop])
			OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciDAWN_RAID_ENABLE_MODE) // This will be turned back on later for this object in the Dawn Raid missions, once the anim has played
			AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn = INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_adv_hdsec")))
				PRINTLN("SET_UP_FMMC_OBJ_RC - SET_TEAM_PICKUP_OBJECT called with FALSE on team ",iTeamLoop," for object ",iobj)
				SET_TEAM_PICKUP_OBJECT(oiPassed, iTeamLoop, FALSE)
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciDAWN_RAID_ENABLE_MODE) // This will be turned back on later for this object in the Dawn Raid missions, once the anim has played
				AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn = INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_adv_hdsec"))
					SET_ENTITY_COLLISION(oiPassed, FALSE)
					FREEZE_ENTITY_POSITION(oiPassed, TRUE)
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciFLASH_PICKUPS_FOR_ALL_TEAMS)
		PRINTLN("SET_UP_FMMC_OBJ_RC - Turning on flashing pickup on same team for object ", iobj)
		SET_OBJECT_GLOW_IN_SAME_TEAM(oiPassed)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSet, cibsOBJ_ShowChevron)
		PRINTLN("SET_UP_FMMC_OBJ_RC - CREATE_FMMC_OBJECTS_MP_RC - adding chevron to object")
		SET_PICKUP_OBJECT_ARROW_MARKER(oiPassed, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_STOCKPILE_SOUNDS)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_OBJ_RC - Setting SUPPRESS_PICKUP_SOUND_FOR_PICKUP on pickup ", iobj)
		SUPPRESS_PICKUP_SOUND_FOR_PICKUP(oiPassed, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetTwo, cibsOBJ2_OnlyDamagedByPlayers)
		PRINTLN("[TMS][RCC MISSION] SET_UP_FMMC_OBJ_RC - Making object only damagable by players: ", iobj)
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(oiPassed, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetTwo, cibsOBJ2_CanBeLockedOn)
		SET_OBJECT_TARGETTABLE(oiPassed, TRUE)
		SET_ENTITY_IS_TARGET_PRIORITY(oiPassed, TRUE, 50)
		PRINTLN("[RCC MISSION] - SET_UP_FMMC_OBJ_RC - Setting object ", iObj," as priority target")
	ENDIF
			
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjHealthOverride > 0
		PRINTLN("SET_UP_FMMC_OBJ - iObj: ", iObj, " Setting Health to iPropHealthOverride: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjHealthOverride)
		SET_ENTITY_INVINCIBLE(oiPassed, FALSE)
		SET_ENTITY_CAN_BE_DAMAGED(oiPassed, TRUE)
		SET_ENTITY_PROOFS(oiPassed, FALSE, FALSE, TRUE, FALSE, FALSE)
		SET_ENTITY_HEALTH(oiPassed, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjHealthOverride)
		SET_DISABLE_FRAG_DAMAGE(oiPassed, TRUE)
	ENDIF
		
	PROCESS_OBJECT_CONTINUITY(oiPassed, iObj, sLEGACYMissionContinuityVars)
	
ENDPROC


PROC SET_UP_FMMC_MINI_GAME_OBJECT_RC(OBJECT_INDEX oiPassed, INT iPropNumber, BOOL bVisible = TRUE, BOOL bSecondary = FALSE)

	SET_ENTITY_COORDS(oiPassed, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].vPos)
	
	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].vRot)
		SET_ENTITY_ROTATION(oiPassed, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].vRot)
	ELSE
		IF bSecondary
		AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].vSecondSpawnPos)
			SET_ENTITY_HEADING(oiPassed, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].fSecondHeading)
		ELSE
			SET_ENTITY_HEADING(oiPassed, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].fHead)
		ENDIF
	ENDIF
	
	PRINTLN("[RCC MISSION] SET_UP_FMMC_MINI_GAME_OBJECT_RC - mini game object/crate ",iPropNumber," created at: ",g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].vPos)
		
	BOOL bFireProof = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].iObjectBitSetTwo, cibsOBJ2_FireProof)
	PRINTLN(" [RCC MISSION] SET_UP_FMMC_MINI_GAME_OBJECT_RC - FireProof: ",bFireProof)
	SET_ENTITY_PROOFS(oiPassed, FALSE, bFireProof, FALSE, TRUE, TRUE)
	
	IF IS_HACK_LINKED_TO_DOOR( iPropNumber )
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].mn = Prop_Container_LD
		PRINTLN("[RCC MISSION] SET_UP_FMMC_MINI_GAME_OBJECT_RC - ",iPropNumber," is a hack object")
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_BLOW_DOOR)
			SET_ENTITY_INVINCIBLE(oiPassed, TRUE)
			SET_ENTITY_PROOFS(oiPassed, TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_BLOW_VISIBLE)
				SET_ENTITY_VISIBLE(oiPassed, TRUE)
			ELSE
				SET_ENTITY_VISIBLE(oiPassed, FALSE)
			ENDIF
			
			// Ensure the container has collision
			IF  g_FMMC_STRUCT_ENTITIES.sPlacedObject[ iPropNumber ].mn = Prop_Container_LD
				SET_ENTITY_COLLISION( oiPassed, TRUE )
				SET_ENTITY_VISIBLE( oiPassed, TRUE )
			ELSE
				SET_ENTITY_COLLISION( oiPassed, FALSE )
			ENDIF
	
			PRINTLN("[RCC MISSION] SET_UP_FMMC_MINI_GAME_OBJECT_RC - ",iPropNumber," is a blow doors hack object")
		ELSE
			SET_ENTITY_INVINCIBLE( oiPassed, TRUE )
			SET_ENTITY_PROOFS( oiPassed, TRUE, TRUE, TRUE, TRUE, TRUE )
			SET_ENTITY_VISIBLE(oiPassed, bVisible)
			
		ENDIF
		
	ELSE
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_BLOW_DOOR)
			SET_ENTITY_INVINCIBLE(oiPassed, TRUE)
			SET_ENTITY_PROOFS(oiPassed, TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
			SET_ENTITY_COLLISION(oiPassed, FALSE)
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_BLOW_VISIBLE)
				SET_ENTITY_VISIBLE(oiPassed, TRUE)
			ELSE
				SET_ENTITY_VISIBLE(oiPassed, FALSE)
			ENDIF
			PRINTLN("[RCC MISSION] SET_UP_FMMC_MINI_GAME_OBJECT_RC - ",iPropNumber," is a blow doors hack object")
		ELSE
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].iObjectHealth = 5
				IF NOT IS_THIS_MODEL_A_CCTV_CAMERA(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].mn)
					SET_ENTITY_PROOFS(oiPassed, TRUE, TRUE, TRUE, TRUE, TRUE)
					SET_ENTITY_INVINCIBLE(oiPassed, TRUE)
					
					PRINTLN("[RCC MISSION] SET_UP_FMMC_MINI_GAME_OBJECT_RC - ",iPropNumber," is not a CCTV camera; set it as invincible w all the basic proofs")
				ELSE
					//SET_ENTITY_PROOFS(oiPassed, FALSE, FALSE, FALSE, FALSE, FALSE)
					SET_ENTITY_INVINCIBLE(oiPassed, FALSE)
					PRINTLN("[RCC MISSION] SET_UP_FMMC_MINI_GAME_OBJECT_RC - ",iPropNumber," is a CCTV camera; set it as not invincible, and not any proofs")
				ENDIF
				SET_ENTITY_HEALTH(oiPassed, 500)	
			ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].iObjectHealth = 4
				SET_ENTITY_HEALTH(oiPassed, 400)	
			ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].iObjectHealth = 3
				SET_ENTITY_HEALTH(oiPassed, 300)	
			ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].iObjectHealth = 2
				SET_ENTITY_HEALTH(oiPassed, 200)	
			ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].iObjectHealth = 1
				SET_ENTITY_HEALTH(oiPassed, 100)	
			ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].iObjectHealth = -1
				SET_ENTITY_HEALTH(oiPassed, 1)		
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].iObjectBitSet, cibsOBJ_Unfraggable)
				SET_ENTITY_PROOFS(oiPassed, TRUE, TRUE, TRUE, TRUE, TRUE)
				
				IF SHOULD_OBJECT_BE_INVINCIBLE_WHEN_UNFRAGGABLE(iPropNumber)
					SET_ENTITY_INVINCIBLE(oiPassed, TRUE)
					PRINTLN("[RCC MISSION] SET_UP_FMMC_MINI_GAME_OBJECT_RC - ",iPropNumber," is being set to be invincible due to cibsOBJ_Unfraggable")
				ELSE
					PRINTLN("[RCC MISSION] SET_UP_FMMC_MINI_GAME_OBJECT_RC - ",iPropNumber," is set not to frag BUT we're not going to make it invincible due to SHOULD_OBJECT_BE_INVINCIBLE_WHEN_UNFRAGGABLE")
				ENDIF
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].mn != INT_TO_ENUM(MODEL_NAMES, HASH("gr_Prop_gr_Adv_Case"))	//Bug 3168381
				AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].mn != INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_impexp_coke_trolly"))	//Bug 3191355
				AND GET_IS_ENTITY_A_FRAG(oiPassed) //3973501
					SET_DISABLE_FRAG_DAMAGE(oiPassed, TRUE)
				ENDIF
				PRINTLN("[RCC MISSION] SET_UP_FMMC_MINI_GAME_OBJECT_RC - ",iPropNumber," is set not to frag")
			ENDIF
				
			SET_ENTITY_VISIBLE(oiPassed, bVisible)
		ENDIF
	ENDIF
	
	IF IS_THIS_MODEL_A_CCTV_CAMERA(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].mn)
		IF NETWORK_IS_ACTIVITY_SESSION()
			NETWORK_ENTITY_USE_HIGH_PRECISION_ROTATION(OBJ_TO_NET(oiPassed), TRUE)
			PRINTLN("[CCTV] SET_UP_FMMC_MINI_GAME_OBJECT_RC - Setting NETWORK_ENTITY_USE_HIGH_PRECISION_ROTATION on obj ", iPropNumber)
		ENDIF
	ENDIF
	
	IF (NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].vInteriorCoords))
	OR (g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].iInterior != -1)
	
		INTERIOR_INSTANCE_INDEX tempInterior
		
		IF (NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].vInteriorCoords))
			PRINTLN("[RCC MISSION] SET_UP_FMMC_MINI_GAME_OBJECT_RC - object ",iPropNumber," has interior coords ",g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].vInteriorCoords," & typehash ",g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].iInteriorTypeHash)
			tempInterior = GET_INTERIOR_AT_COORDS_WITH_TYPEHASH(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].vInteriorCoords,g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].iInteriorTypeHash)
		ELSE
			PRINTLN("[RCC MISSION] SET_UP_FMMC_MINI_GAME_OBJECT_RC - object ",iPropNumber," has iInterior ",g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].iInterior)
			tempInterior = INT_TO_NATIVE(INTERIOR_INSTANCE_INDEX, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].iInterior)
		ENDIF
		
		IF IS_VALID_INTERIOR(tempInterior)
			PRINTLN("[RCC MISSION] SET_UP_FMMC_MINI_GAME_OBJECT_RC - object ",iPropNumber," RETAIN_ENTITY_IN_INTERIOR called on interior ", NATIVE_TO_INT(tempInterior))
			RETAIN_ENTITY_IN_INTERIOR(oiPassed, tempInterior)
		ELSE
			PRINTLN("[RCC MISSION] SET_UP_FMMC_MINI_GAME_OBJECT_RC - object ",iPropNumber," supposed to be retained in interior ", NATIVE_TO_INT(tempInterior)," but it isn't valid!")
		ENDIF
		
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].iObjectBitSetTwo, cibsOBJ2_CanBeLockedOn)
		SET_OBJECT_TARGETTABLE(oiPassed, TRUE)
		SET_ENTITY_IS_TARGET_PRIORITY(oiPassed, TRUE, 50)
		PRINTLN("SET_UP_FMMC_MINI_GAME_OBJECT_RC - Setting object ", iPropNumber," as priority target")
	ELSE
		SET_OBJECT_TARGETTABLE(oiPassed, FALSE)
		PRINTLN("SET_UP_FMMC_MINI_GAME_OBJECT_RC - Setting object ", iPropNumber," as NOT targetable")
	ENDIF
	
	IF SHOULD_PLACED_OBJECT_BE_PORTABLE(iPropNumber)
		PRINTLN("[RCC MISSION] SET_UP_FMMC_MINI_GAME_OBJECT_RC - object ",iPropNumber," being frozen in position!")
		FREEZE_ENTITY_POSITION(oiPassed, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].iObjectBitSetTwo, cibsOBJ2_OnlyDamagedByPlayers)
		PRINTLN("[TMS][RCC MISSION] SET_UP_FMMC_MINI_GAME_OBJECT_RC - Making object only damagable by players: ", iPropNumber)
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(oiPassed, TRUE)
	ENDIF	
				
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].iObjHealthOverride > 0
		PRINTLN("SET_UP_FMMC_OBJ - iObj: ", iPropNumber, " Setting Health to iPropHealthOverride: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].iObjHealthOverride)
		SET_ENTITY_INVINCIBLE(oiPassed, FALSE)
		SET_ENTITY_CAN_BE_DAMAGED(oiPassed, TRUE)
		SET_ENTITY_PROOFS(oiPassed, FALSE, FALSE, TRUE, FALSE, FALSE)
		SET_ENTITY_HEALTH(oiPassed, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].iObjHealthOverride)
		SET_DISABLE_FRAG_DAMAGE(oiPassed, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_BOMB_FOOTBALL_FUNCTIONALITY)
		APPLY_BOMB_FOOTBALL_PROPERTIES_TO_FOOTBALL(oiPassed, iPropNumber)
	ENDIF
	
	// We might now be doing this on all objects with an interior index stored by the creator when the object gets placed (I think)
	//IF IS_THIS_MODEL_A_CCTV_CAMERA(iPropNumber)
	//	INTERIOR_INSTANCE_INDEX interiorID = GET_INTERIOR_AT_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iPropNumber].vPos)
	//	IF IS_VALID_INTERIOR(interiorID)
	//		PRINTLN("[RCC MISSION] SET_UP_FMMC_MINI_GAME_OBJECT_RC - Retain CCTV object ",iPropNumber," in interior!")
	//		RETAIN_ENTITY_IN_INTERIOR(oiPassed, interiorID)
	//	ELSE
	//		PRINTLN("[RCC MISSION] SET_UP_FMMC_MINI_GAME_OBJECT_RC - CCTV object ",iPropNumber," isn't in an interior")
	//	ENDIF
	//ENDIF
	
ENDPROC

PROC SET_OBJECT_SETTINGS_AFTER_CREATION( INT index, NETWORK_INDEX& niVeh[], OBJECT_INDEX obj, sCreatedCountData& sCount, FMMC_LEGACY_MISSION_CONTINUITY_VARS &sLEGACYMissionContinuityVars)
	SET_UP_FMMC_MINI_GAME_OBJECT_RC( obj, index, FALSE )
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[ index ].mn = Prop_Contr_03b_LD
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[ index ].mn = Prop_Container_LD_PU
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[ index ].mn = prop_container_ld
		SET_ENTITY_LOD_DIST( obj, 500 )
	ENDIF
	
	//If we're attaching a car into this container:
	FIND_AND_ATTACH_CAR_TO_CONTAINER( index, niVeh, obj )
	
	INT iAttachParentIndex = g_FMMC_STRUCT_ENTITIES.sPlacedObject[ index ].iAttachParent
	
	PRINTLN("[RCC MISSION] [dsw] [SET_OBJECT_SETTINGS_AFTER_CREATION] Object ",index," has iAttachParentIndex = ", iAttachParentIndex)
	
	IF iAttachParentIndex != -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[index].iAttachParentType = CREATION_TYPE_VEHICLES
		AND NETWORK_DOES_NETWORK_ID_EXIST(niVeh[iAttachParentIndex])
			//We're attaching onto a flatbed!
			VEHICLE_INDEX vehFlatbed = NET_TO_VEH(niVeh[iAttachParentIndex])
			ATTACH_OBJECT_TO_FLATBED(obj, vehFlatbed, g_FMMC_STRUCT_ENTITIES.sPlacedObject[index].vAttachOffset, g_FMMC_STRUCT_ENTITIES.sPlacedObject[index].vAttachOffsetRotation, TRUE)
		ENDIF
	ENDIF
	
	PROCESS_OBJECT_CONTINUITY(obj, index, sLEGACYMissionContinuityVars)
	
	SET_UP_TROLLEY_STARTING_AMOUNT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[index].mn, obj, sLEGACYMissionContinuityVars.iDailyVaultCashGrabbed)
	
	sCount.iNumObjCreated[0]++
	
	#IF IS_DEBUG_BUILD
		NET_PRINT_TIME() NET_PRINT_STRINGS("     ---------->     FMMC MINI GAME OBJECT CREATED    <----------     ", "FMMC: ") NET_PRINT_INT( index ) NET_NL()
	#ENDIF
ENDPROC


PROC SET_UP_FMMC_DYNOPROP_MP_RC(OBJECT_INDEX oiPassed, INT iPropNumber, MODEL_NAMES mnPassed, BOOL bTest, BOOL bVisible = TRUE)
	SET_ENTITY_COORDS(oiPassed, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iPropNumber].vPos)
	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iPropNumber].vRot)
		SET_ENTITY_ROTATION(oiPassed, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iPropNumber].vRot)
	ELSE
		SET_ENTITY_HEADING(oiPassed, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iPropNumber].fHead)
	ENDIF
	SET_ENTITY_VISIBLE(oiPassed, bVisible)
	SET_OBJECT_TARGETTABLE(oiPassed, FALSE)
	
	IF (NOT SHOULD_THIS_MODEL_BE_FROZEN(mnPassed)) OR bTest
		SET_ENTITY_INVINCIBLE(oiPassed, FALSE)
		SET_ENTITY_PROOFS(oiPassed, FALSE, FALSE, FALSE, FALSE, FALSE)
		
		IF SHOULD_THIS_MODEL_BE_FROZEN(mnPassed)
		AND NOT IS_THIS_MODEL_A_TRAP_DYNO_PROP(mnPassed)
			FREEZE_ENTITY_POSITION(oiPassed, FALSE)
		ENDIF
	ELSE
		SET_ENTITY_INVINCIBLE(oiPassed, TRUE)
		SET_ENTITY_PROOFS(oiPassed, TRUE, TRUE, TRUE, TRUE, TRUE)
		
		IF SHOULD_THIS_MODEL_BE_FROZEN(mnPassed)
		AND NOT IS_THIS_MODEL_A_TRAP_DYNO_PROP(mnPassed)
			FREEZE_ENTITY_POSITION(oiPassed, TRUE)
		ENDIF
	ENDIF
	
	//Set the colour of the prop to the american flags if it is independence day
	IF g_sMPTunables.bEnableAmericanFlagStuntRaces
		PRINTLN("[LH][FlagColours] g_sMPTunables.bEnableAmericanFlagStuntRaces = TRUE")
		PRINTLN("[LH][FlagColours] Setting g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[",iPropNumber,"] from ",g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iPropNumber].iPropColour," to 0")
		g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iPropNumber].iPropColour = 0
	ENDIF
	
	SET_OBJECT_TINT_INDEX(oiPassed, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iPropNumber].iPropColour)
	
	IF IS_MODEL_TARGETABLE(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iPropNumber].mn)
		SET_OBJECT_TARGETTABLE(oiPassed, TRUE)
	ENDIF
		
	IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iPropNumber].iPropHealthOverride > 0
		PRINTLN("SET_UP_FMMC_DYNOPROP_MP_RC - iPropNumber: ", iPropNumber, " Setting Health to iPropHealthOverride: ", g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iPropNumber].iPropHealthOverride)
		SET_ENTITY_INVINCIBLE(oiPassed, FALSE)
		SET_ENTITY_CAN_BE_DAMAGED(oiPassed, TRUE)
		SET_ENTITY_PROOFS(oiPassed, FALSE, FALSE, TRUE, FALSE, FALSE)
		SET_ENTITY_HEALTH(oiPassed, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iPropNumber].iPropHealthOverride)
		//SET_DISABLE_FRAG_DAMAGE(oiPassed, TRUE)
	ENDIF

	IF GET_MODEL_NAME_OF_CHILD_PROP_FOR_DYNO(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iPropNumber].mn) != DUMMY_MODEL_FOR_SCRIPT
		PRINTLN("[LM] SET_UP_FMMC_DYNOPROP_MP_RC - Freezing Prop")
		FREEZE_ENTITY_POSITION(oiPassed, TRUE)
	ENDIF
	
	IF IS_THIS_MODEL_A_CCTV_CAMERA(mnPassed)
		SET_ENTITY_PROOFS(oiPassed, FALSE, FALSE, TRUE, TRUE, FALSE)
		FREEZE_ENTITY_POSITION(oiPassed, FALSE)
		PRINTLN("[CCTV] SET_UP_FMMC_DYNOPROP_MP_RC - Setting dynoprop ", iPropNumber, " to have CCTV proofs")
		
		IF NETWORK_IS_ACTIVITY_SESSION()
			NETWORK_ENTITY_USE_HIGH_PRECISION_ROTATION(OBJ_TO_NET(oiPassed), TRUE)
			PRINTLN("[CCTV] SET_UP_FMMC_DYNOPROP_MP_RC - Setting NETWORK_ENTITY_USE_HIGH_PRECISION_ROTATION on dynoprop ", iPropNumber)
		ENDIF
	ENDIF
	
	IF IS_THIS_MODEL_A_TRAP_DYNO_PROP(GET_ENTITY_MODEL(oiPassed))
		INIT_ARENA_TRAP(oiPassed, iPropNumber)
	ENDIF
	
	RETAIN_ENTITY_INSIDE_ITS_CURRENT_INTERIOR(oiPassed)
	
ENDPROC

FUNC BOOL REQUEST_LOAD_FMMC_MODELS_RC( sCreatedCountData& sCountDataPassed,
										INT& iStartingPlayers[],
										next_gen_only_entity_counts& sEntityCounts,
										sFMMC_RandomSpawnGroupStruct rsgSpawnSeed,
										FMMC_LEGACY_MISSION_CONTINUITY_VARS &sLEGACYMissionContinuityVars)//INT iStage = 0 )
	BOOL bReturn = TRUE
	
	INT i
	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfPeds 	 i
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn != DUMMY_MODEL_FOR_SCRIPT
		//AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iStage = iStage 
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetFive, ciPED_BSFive_NGSpawnOnly)
				sEntityCounts.iPeds++
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetFive, ciPED_BSFive_StartDead)
				REQUEST_ANIM_DICT("missfbi1")
				IF NOT HAS_ANIM_DICT_LOADED("missfbi1")
					bReturn = FALSE
				ENDIF
			ENDIF
			
			PRINTLN("[RCC MISSION] - requesting model for ped : ", i)
			IF SHOULD_PED_SPAWN_AT_START(i, sCountDataPassed, iStartingPlayers, FALSE, rsgSpawnSeed, sLEGACYMissionContinuityVars)
				IF NOT REQUEST_LOAD_MODEL( g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn )
					PRINTLN("[RCC MISSION] REQUEST_LOAD_FMMC_MODELS - FAILING ON - g_FMMC_STRUCT_ENTITIES.sPlacedPed[", i, "].mn = ", ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn))
					bReturn = FALSE
				ENDIF
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iInventoryBS > 0
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iInventoryBS,ciPED_INV_Briefcase_prop_ld_case_01)
						REQUEST_MODEL(PROP_LD_CASE_01)
						REQUEST_ANIM_DICT("weapons@misc@jerrycan@mp_male")
						IF (NOT HAS_MODEL_LOADED(PROP_LD_CASE_01))
							PRINTLN("REQUEST_LOAD_FMMC_MODELS - FAILING ON load Ped inventory Briefcase model PROP_LD_CASE_01")
							bReturn = FALSE
						ENDIF
						IF (NOT HAS_ANIM_DICT_LOADED("weapons@misc@jerrycan@mp_male"))
							PRINTLN("REQUEST_LOAD_FMMC_MODELS - FAILING ON load Ped inventory Briefcase anim dict weapons@misc@jerrycan@mp_male")
							bReturn = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn = IG_LESTERCREST
				REQUEST_CLIP_SET("move_heist_lester")
				IF NOT HAS_CLIP_SET_LOADED("move_heist_lester")
					PRINTLN("[RCC MISSION] REQUEST_LOAD_FMMC_MODELS - FAILING ON - lester ped clipset ", i)
					bReturn = FALSE
				ENDIF
			ENDIF
			
			// bug 2124886 - load trevors anim dictionary before we start brining the sky cam down 
			IF g_FMMC_STRUCT.iRootContentIDHash = g_sMPTunables.iroot_id_HASH_Series_A_Funding
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn = PLAYER_TWO
					REQUEST_ANIM_DICT( "anim@narcotics@finale@trevor_car_idle" )
					IF NOT HAS_ANIM_DICT_LOADED( "anim@narcotics@finale@trevor_car_idle" )
						PRINTLN("[RCC MISSION] REQUEST_LOAD_FMMC_MODELS - FAILING ON - trevor car idle anim dictionary", i)
						bReturn = FALSE
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetFive, ciPED_BSFive_StartDead)
				REQUEST_ANIM_DICT(GET_DEAD_PED_ANIM_DICT_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iDeadAnim))
				IF NOT HAS_ANIM_DICT_LOADED(GET_DEAD_PED_ANIM_DICT_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iDeadAnim))
					PRINTLN("[RCC MISSION] REQUEST_LOAD_FMMC_MODELS - FAILING ON - dead ped ",i," anim dict ",GET_DEAD_PED_ANIM_DICT_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iDeadAnim))
					bReturn = FALSE
				ENDIF
			ENDIF
			
		ELSE
			//PRINTSTRING("g_FMMC_STRUCT_ENTITIES.iNumberOfPeds[")PRINTINT(i)PRINTSTRING("].mn = ")PRINTINT(ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn))PRINTNL()
		ENDIF
	ENDREPEAT	
//	REPEAT g_FMMC_STRUCT.iNumberOfAnimals 	 i
//		IF g_FMMC_STRUCT.sPlacedAnimal[i].mn != DUMMY_MODEL_FOR_SCRIPT
//		//AND g_FMMC_STRUCT.sPlacedAnimal[i].iStage = iStage 
//			IF NOT REQUEST_LOAD_MODEL(g_FMMC_STRUCT.sPlacedAnimal[i].mn)
//				bReturn = FALSE
//			ENDIF
//		ELSE
//			//PRINTSTRING("g_FMMC_STRUCT_ENTITIES.iNumberOfPeds[")PRINTINT(i)PRINTSTRING("].mn = ")PRINTINT(ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn))PRINTNL()
//		ENDIF
//	ENDREPEAT

	IF SVM_FLOW_IS_CURRENT_MISSION_SVM_MISSION()
		IF NOT REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("BJXL")))
			PRINTLN("REQUEST_LOAD_FMMC_MODELS - FAILING ON - BJXL = ")
			bReturn = FALSE
		ENDIF
	ENDIF
	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles 	 i
	
		IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetThree, ciFMMC_VEHICLE3_NGSpawnOnly )
			sEntityCounts.iVehicles++
		ENDIF
	
		IF SHOULD_VEH_SPAWN_AT_START(i, sCountDataPassed, iStartingPlayers, FALSE, rsgSpawnSeed, sLEGACYMissionContinuityVars)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn != DUMMY_MODEL_FOR_SCRIPT
			//AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iStage = iStage 
				PRINTLN("[RCC MISSION] - setting requesting model for veh : ", i)
				IF NOT REQUEST_LOAD_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
					PRINTLN("REQUEST_LOAD_FMMC_MODELS - FAILING ON - g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[", i, "].mn = ", ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn))
					bReturn = FALSE
				ENDIF
			ELSE
				//PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[")PRINTINT(i)PRINTSTRING("].mn = ")PRINTINT(ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn))PRINTNL()
			ENDIF
		ENDIF
	ENDREPEAT	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfObjects 	 i

		IF SHOULD_OBJ_SPAWN_AT_START(i, sCountDataPassed, iStartingPlayers, FALSE, rsgSpawnSeed)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn != DUMMY_MODEL_FOR_SCRIPT
			//AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iStage = iStage 
				PRINTLN("[RCC MISSION] - setting requesting model for object : ", i, " || ", GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn))
				IF NOT REQUEST_LOAD_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn)
					PRINTLN("REQUEST_LOAD_FMMC_MODELS - FAILING ON - g_FMMC_STRUCT_ENTITIES.sPlacedObject[", i, "].mn = ", ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn))
					bReturn = FALSE
				ENDIF
			ELSE
				//PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedObject[")PRINTINT(i)PRINTSTRING("].mn = ")PRINTINT(ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn))PRINTNL()
			ENDIF
		ENDIF
	ENDREPEAT
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps 	 i
	
		IF SHOULD_DYNOPROP_SPAWN_AT_START(i, rsgSpawnSeed, sLEGACYMissionContinuityVars)
		AND g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].mn != DUMMY_MODEL_FOR_SCRIPT
		//AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iStage = iStage 
			IF NOT REQUEST_LOAD_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].mn)
				PRINTLN("REQUEST_LOAD_FMMC_MODELS - FAILING ON - g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[", i, "].mn = ", ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].mn))
				bReturn = FALSE
			ENDIF
		ELSE
			//PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[")PRINTINT(i)PRINTSTRING("].mn = ")PRINTINT(ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].mn))PRINTNL()
		ENDIF
	ENDREPEAT
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfProps 	 i
	
		//IF SHOULD_PROP_SPAWN_AT_START(i)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn != DUMMY_MODEL_FOR_SCRIPT
		//AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iStage = iStage 
			IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_Prop_Stunt_Tube_Speed"))
				IF NOT REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("stt_Prop_Stunt_Tube_Speeda")))
				AND NOT REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("stt_Prop_Stunt_Tube_Speedb")))
					PRINTLN("REQUEST_LOAD_FMMC_MODELS - FAILING ON - g_FMMC_STRUCT_ENTITIES.sPlacedProp[", i, "].mn = ", ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn))
					bReturn = FALSE
				ELSE
					PRINTLN("[LM] LOADED: REQUEST_LOAD_FMMC_MODELS_RC")
				ENDIF
			ENDIF
			IF NOT REQUEST_LOAD_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn)
				PRINTLN("REQUEST_LOAD_FMMC_MODELS - FAILING ON - g_FMMC_STRUCT_ENTITIES.sPlacedProp[", i, "].mn = ", ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn))
				bReturn = FALSE
			ENDIF
		ELSE
			//PRINTSTRING("g_FMMC_STRUCT_ENTITIES.sPlacedProp[")PRINTINT(i)PRINTSTRING("].mn = ")PRINTINT(ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn))PRINTNL()
		ENDIF
	ENDREPEAT
	RETURN bReturn 
ENDFUNC


//////////////////////////////      MP PROPS           //////////////////////////////
FUNC BOOL CREATE_FMMC_PROPS_RC(OBJECT_INDEX &oiProp[], INT &iSpawnPointsUsed[], INT iNumberOfTeams, sFMMC_RandomSpawnGroupStruct &rsgSpawnSeed)
	INT i	
	text_label_15 monkey
	
	IF REQUEST_LOAD_FMMC_PROP_MODELS()
	
		REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfProps 	 i
		
			isoundid[i] = -1
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn != DUMMY_MODEL_FOR_SCRIPT
			AND SHOULD_PROP_SPAWN_AT_START(i, iSpawnPointsUsed, LocalRandomSpawnBitset, iNumberOfTeams, rsgSpawnSeed)

				IF NOT DOES_ENTITY_EXIST(oiProp[i])
					IF DOES_PROP_HAVE_A_SUB_COMPONENT(i)
						oiProp[i] = CREATE_PROP_WITH_SUB_COMPONENT(i, oiPropsChildren)
					ELSE
						oiProp[i] = CREATE_OBJECT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn, g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].vPos, FALSE, FALSE, TRUE)
					ENDIF
					
					SET_UP_FMMC_PROP_RC(oiProp[i], i, FALSE)
					
					IF GET_PROP_LOD_OVERRIDE_DISTANCE(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn) !=-1
						SET_ENTITY_LOD_DIST(oiProp[i],GET_PROP_LOD_OVERRIDE_DISTANCE(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn))
						PRINTLN("[RCC MISSION] CREATE_FMMC_PROPS_RC - SETTING PROP LOD DISTANCE: ",GET_PROP_LOD_OVERRIDE_DISTANCE(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn))
					ENDIF
					if g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn = HEI_PROP_HEIST_APECRATE 
					AND iMonkeyPedCount < ciMAX_MONKEYPEDS
						
						if can_register_mission_peds(1)
						
							vector monkey_pos
							float monkey_heading
							float anim_start_phase
							
							reserve_network_mission_objects(get_num_reserved_mission_peds() + 1) 
							
							monkey_pos = get_offset_from_entity_in_world_coords(oiprop[i], <<0.0, 0.0, 0.7>>)
							monkey_heading = (get_entity_heading(oiprop[i]) + 180.00)
							
							monkey_ped[iMonkeyPedCount] = create_ped(pedtype_mission, a_c_rhesus, monkey_pos, monkey_heading, false, false)
							//monkey_ped[i] = create_ped(pedtype_mission, a_c_rhesus, monkey_pos, monkey_heading)
							setup_monkey_attributes(monkey_ped[iMonkeyPedCount])
							monkey = "monkey"
							monkey += iMonkeyPedCount
							set_ped_name_debug(monkey_ped[iMonkeyPedCount], monkey)
							
							
							anim_start_phase = (get_random_int_in_range(0, 3) / 10.0)
							
							switch get_random_int_in_range(0, 3)
							
								case 0
									
									task_play_anim(monkey_ped[iMonkeyPedCount], "missfbi5ig_30monkeys", "monkey_a_idle", normal_blend_in, normal_blend_out, -1, af_looping, anim_start_phase) 	
									
								break 
								
								case 1
									
									task_play_anim(monkey_ped[iMonkeyPedCount], "missfbi5ig_30monkeys", "monkey_b_idle", normal_blend_in, normal_blend_out, -1, af_looping, anim_start_phase) 	
									
								break 
								
								case 2
									
									task_play_anim(monkey_ped[iMonkeyPedCount], "missfbi5ig_30monkeys", "monkey_c_idle", normal_blend_in, normal_blend_out, -1, af_looping, anim_start_phase) 	
									
								break 
								
							endswitch 
							
							iMonkeyPedCount++
						endif 
						
					endif
 					
					IF( g_FMMC_STRUCT_ENTITIES.sPlacedProp[ i ].mn = PROP_FLARE_01 )
					OR( g_FMMC_STRUCT_ENTITIES.sPlacedProp[ i ].mn = ind_prop_firework_03 )
						REQUEST_NAMED_PTFX_ASSET( "scr_biolab_heist" )
					ENDIF
					
					//Turn on crashed plane sound if the crashed plane prop is created					
					IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[ i ].mn = PROP_SHAMAL_CRASH
					OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[ i ].mn = INT_TO_ENUM(MODEL_NAMES, HASH("apa_MP_Apa_Crashed_USAF_01a"))
					OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[ i ].mn = INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_Shamal_crash"))
						IF DOES_ENTITY_EXIST( oiProp[ i ] )
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iPropBitset, ciFMMC_PROP_EmitCrashSound)
								iCrashSoundProp = i
							ENDIF
						ENDIF
					ENDIF
					
					#IF IS_DEBUG_BUILD
						PRINTLN("[RCC MISSION] CREATE_FMMC_PROPS_RC - Prop ",i," created!")
						NET_PRINT_TIME() NET_PRINT_STRINGS("[RCC MISSION]  ---------->     FMMC PROP CREATED     <----------     ", "FMMC") NET_NL()
					#ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		//Check we have created all the things
		REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfProps 	 i
			IF NOT DOES_ENTITY_EXIST(oiProp[i])
			AND g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn != DUMMY_MODEL_FOR_SCRIPT
			AND SHOULD_PROP_SPAWN_AT_START(i, iSpawnPointsUsed, LocalRandomSpawnBitset, iNumberOfTeams, rsgSpawnSeed)
				RETURN FALSE
			ENDIF
		ENDREPEAT
	ELSE
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CREATE_FMMC_DYNOPROPS_MP_RC( NETWORK_INDEX &niProp[],
									BOOL bTest, 
									sCreatedCountData& sCountDataPassed, 
									INT& iStartingPlayers[],
									INT &iServerBS5,
									sFMMC_RandomSpawnGroupStruct rsgSpawnSeed,
									FMMC_LEGACY_MISSION_CONTINUITY_VARS &sLEGACYMissionContinuityVars)
									
	INT i	

	//Don't stagger on fade transitions
	BOOL bStrand = IS_A_STRAND_MISSION_BEING_INITIALISED() AND (GET_STRAND_MISSION_TRANSITION_TYPE() != FMMC_BRANCHING_MISSION_TRANSITION_TYPE_FADEOUT)
	BOOL bVisible	
	IF bStrand
		bVisible = TRUE
	ENDIF
	INT iRequestThisFrame
	
	next_gen_only_entity_counts	dummy // Blank struct for compilation
	sFMMC_RandomSpawnGroupStruct rsgDummy
	FMMC_LEGACY_MISSION_CONTINUITY_VARS sDummyContinuity
	
	IF REQUEST_LOAD_FMMC_MODELS_RC( sCountDataPassed, iStartingPlayers, dummy, rsgDummy, sDummyContinuity)
		REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps 	 i
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iRespawnOnRuleChangeBS > 0
			AND NOT IS_BIT_SET(iServerBS5, SBBOOL5_ENTITIES_CAN_RESPAWN_ON_RULE_CHANGE_COPY)
				SET_BIT(iServerBS5, SBBOOL5_ENTITIES_CAN_RESPAWN_ON_RULE_CHANGE_COPY)
			ENDIF
			IF SHOULD_DYNOPROP_SPAWN_AT_START(i, rsgSpawnSeed, sLEGACYMissionContinuityVars)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].mn != DUMMY_MODEL_FOR_SCRIPT
				AND g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].mn != Prop_Container_LD_PU
				AND g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iObjReference = -1
					IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niProp[i])
						IF CREATE_NET_OBJ(niProp[i],g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].mn , g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].vPos, TRUE, TRUE)
							SET_UP_FMMC_DYNOPROP_MP_RC(NET_TO_OBJ(niProp[i]), i, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].mn, bTest, bVisible)
							#IF IS_DEBUG_BUILD
								NET_PRINT_TIME() NET_PRINT_STRINGS("     ---------->     FMMC ........DYNO........... PROPS CREATED     <----------     ", "FMMC: ") NET_PRINT_INT(i) NET_NL()
							#ENDIF
							SET_NETWORK_INDEX_FROZEN_AND_UNABLE_TO_MIGRATE(niProp[i],TRUE)
							IF bStrand
								IF iRequestThisFrame >= PICK_INT(IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING(), ciMAX_PER_FRAME_CREATES_AIRLOCK, ciMAX_PER_FRAME_CREATES)
									RETURN FALSE
								ELSE
									iRequestThisFrame++
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					NET_PRINT_TIME() NET_PRINT_STRINGS("model name invalid dyno prob", "FMMC: ") NET_PRINT_INT(i) NET_NL()
					PRINTLN("CREATE_FMMC_DYNOPROPS_MP_RC, failing on model = ", ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].mn)) 
					
					IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].mn = DUMMY_MODEL_FOR_SCRIPT
						PRINTLN("CREATE_FMMC_DYNOPROPS_MP_RC, DUMMY_MODEL_FOR_SCRIPT ")
					ELIF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].mn = Prop_Container_LD_PU
						PRINTLN("CREATE_FMMC_DYNOPROPS_MP_RC, Prop_Container_LD_PU ")
					ELIF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iObjReference = -1
						PRINTLN("CREATE_FMMC_DYNOPROPS_MP_RC, iObjReference = -1 ")
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		//Check we have created all the things
		REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps 	 i
		
			IF SHOULD_DYNOPROP_SPAWN_AT_START(i, rsgSpawnSeed, sLEGACYMissionContinuityVars)
			
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niProp[i])
				AND SHOULD_DYNOPROP_SPAWN_AT_START(i, rsgSpawnSeed, sLEGACYMissionContinuityVars)
				AND g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].mn != DUMMY_MODEL_FOR_SCRIPT
				AND g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].mn != Prop_Container_LD_PU
				AND g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iObjReference = -1
					RETURN FALSE
				ENDIF
			
			ENDIF
			
		ENDREPEAT
	ELSE
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

//////////////////////////////      MP  VEHICLES           //////////////////////////////
FUNC BOOL CREATE_FMMC_VEHICLES_MP_RC( NETWORK_INDEX &niVehicle[],
									NETWORK_INDEX &niVehicleCrate[],
									sCreatedCountData &sCountDataPassed,
									INT &iStartingPlayers[],
									INT iServerPropertyID, 
									int &sb_casco_index,
									sFMMC_RandomSpawnGroupStruct rsgSpawnSeed,
									INT &iServerBS5,
									INT &iVehicleNotSpawningDueToRandomBS, 
									INT &iSpecialOverridePosSlot,
									FMMC_LEGACY_MISSION_CONTINUITY_VARS &sLEGACYMissionContinuityVars) //, INT iStage = 0 )
	INT i
	//Don't stagger on fade transitions
	BOOL bStrand = IS_A_STRAND_MISSION_BEING_INITIALISED() AND (GET_STRAND_MISSION_TRANSITION_TYPE() != FMMC_BRANCHING_MISSION_TRANSITION_TYPE_FADEOUT)
	BOOL bVisible
	BOOL bCallAgain
	IF bStrand
		bVisible = TRUE
	ENDIF
	INT iRequestThisFrame
	INT iRandomHeadingToSelect, iSpawnInterior
	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles i
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn != DUMMY_MODEL_FOR_SCRIPT
			
			//Catch to detect blocked vehicles in UGC content
			IF g_sMPTunables.bENABLE_CREATOR_BLOCK
				IF NOT IS_CURRENT_MISSION_ROCKSTAR_CREATED()
				AND SHOULD_THIS_VEHICLE_BE_BLOCKED_FROM_THE_CREATORS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
					SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
					ASSERTLN("[CREATOR_BLOCK] INVALID VEHICLE DETECTED SWITCHING MODEL TO NINEF")
					PRINTLN("[CREATOR_BLOCK] RC - INVALID VEHICLE DETECTED SWITCHING MODEL TO NINEF - Vehicle index: ", i, " Model: ", GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn))
					g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn = NINEF
				ENDIF
			ENDIF
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iRespawnOnRuleChangeBS > 0
			AND NOT IS_BIT_SET(iServerBS5, SBBOOL5_ENTITIES_CAN_RESPAWN_ON_RULE_CHANGE_COPY)
				SET_BIT(iServerBS5, SBBOOL5_ENTITIES_CAN_RESPAWN_ON_RULE_CHANGE_COPY)
			ENDIF
			IF SHOULD_VEH_SPAWN_AT_START(i, sCountDataPassed, iStartingPlayers, FALSE, rsgSpawnSeed, sLEGACYMissionContinuityVars)
			AND NOT IS_BIT_SET(iVehicleNotSpawningDueToRandomBS, i)
				IF NOT HAS_VEHICLE_CREATION_FINISHED(niVehicle[i],niVehicleCrate[i],i)
					IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niVehicle[i])
						iRandomHeadingToSelect = -1
						iSpawnInterior = -1
						VECTOR vSpawnPos = GET_VEH_SPAWN_LOCATION(i, iServerPropertyID, iRandomHeadingToSelect, iSpawnInterior, iSpecialOverridePosSlot, TRUE)
						IF IS_VECTOR_ZERO(vSpawnPos)
						AND NOT IS_BIT_SET(iVehicleNotSpawningDueToRandomBS, i)
							SET_BIT(iVehicleNotSpawningDueToRandomBS, i)
						ENDIF
						IF IS_BIT_SET(iVehicleNotSpawningDueToRandomBS, i)
							PRINTLN("[RCC MISSION][CREATE_FMMC_VEHICLES_MP_RC] not spawning vehicle: ", i ," due to random spawn at origin")
							RELOOP
						ENDIF
						
						IF NOT bStrand
							CLEAR_AREA_LEAVE_VEHICLE_HEALTH(vSpawnPos, 20.0, FALSE)
						ENDIF
						
						BOOL bFreezeWaitingOnCollision = TRUE
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetThree, ciFMMC_VEHICLE3_VehicleStartsAirborne)
						AND NOT (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn = TITAN AND IS_THIS_ROCKSTAR_MISSION_WVM_DUNE3(g_FMMC_STRUCT.iRootContentIDHash))
							// Don't call SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION if the vehicle is being created in the air
							bFreezeWaitingOnCollision = FALSE
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetFive, ciFMMC_VEHICLE5_RESTART_WITH_TRAILER)
						AND g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[i] != -1
						AND (IS_THIS_A_QUICK_RESTART_JOB() 
						OR IS_CORONA_INITIALISING_A_QUICK_RESTART())
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetSix, ciFMMC_VEHICLE6_DONT_RESTART_WITH_TRAILER_MISSION_START)
							AND NOT FMMC_CHECKPOINTS_IS_ANY_RETRY_START_POINT_SET()
								PRINTLN("[RCC MISSION][CREATE_FMMC_VEHICLES_MP_RC] Mission start - Clearing iAttachedVehicleTrailers for Vehicle ", i)
								g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[i] = -1
							ELSE
								IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niVehicle[g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[i]])
								AND SHOULD_VEH_SPAWN_AT_START(g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[i], sCountDataPassed, iStartingPlayers, FALSE, rsgSpawnSeed, sLEGACYMissionContinuityVars)
									PRINTLN("[RCC MISSION][CREATE_FMMC_VEHICLES_MP_RC] Vehicle ", i, " is waiting for Trailer: ", g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[i], " To spawn.")
									RELOOP
								ENDIF
							ENDIF
						ENDIF
						
						IF CREATE_NET_VEHICLE(niVehicle[i], g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn, vSpawnPos, GET_VEH_SPAWN_HEADING(i, iServerPropertyID, DEFAULT, iRandomHeadingToSelect, iSpecialOverridePosSlot), DEFAULT, DEFAULT, DEFAULT, DEFAULT, bFreezeWaitingOnCollision, FALSE, DEFAULT, DEFAULT, SHOULD_VEHICLE_CREATION_IGNORE_GROUND_CHECK(i, vSpawnPos, iSpawnInterior))
							INT iTotalStartingPlayers = iStartingPlayers[0] + iStartingPlayers[1] + iStartingPlayers[2] + iStartingPlayers[3]
							SET_UP_FMMC_VEH_RC(NET_TO_VEH(niVehicle[i]), i, niVehicle, vSpawnPos, (iRandomHeadingToSelect > 0), iSpawnInterior, bVisible, iTotalStartingPlayers)
							
							//store the index of the vehicle with the casco model for prison break station mission
							if g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn = casco
								sb_casco_index = i
							endif 
							
							sCountDataPassed.iNumVehCreated[0]++
							
							//This call prevent the vehicle fall trough the map when a player enters into the veh.
							//url:bugstar:4657671
							SET_ENTITY_LOAD_COLLISION_FLAG(NET_TO_VEH(niVehicle[i]),TRUE,FALSE)
														
							IF( STRAND_MISSION_MPH_HUM_KEY_MCS1_CUT_PLAYING() ) // B* 2164633
							OR GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION() //url:bugstar:4099668
								bCallAgain = TRUE
								SET_NETWORK_INDEX_FROZEN_AND_UNABLE_TO_MIGRATE( niVehicle[i], TRUE, TRUE )
								PRINTLN( "[RCC MISSION] Calling SET_NETWORK_INDEX_FROZEN_AND_UNABLE_TO_MIGRATE with bDontFreeze = FALSE" )
							ELSE
								SET_NETWORK_INDEX_FROZEN_AND_UNABLE_TO_MIGRATE( niVehicle[i], TRUE )
							ENDIF
							
							NET_PRINT("[RCC MISSION] created vehicle at this position: ") NET_PRINT_VECTOR(vSpawnPos)NET_PRINT(" vehicle number: ") NET_PRINT_INT(i)  NET_NL()
							IF bStrand
								IF iRequestThisFrame >= PICK_INT(IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING(), ciMAX_PER_FRAME_CREATES_AIRLOCK, ciMAX_PER_FRAME_CREATES)
									RETURN FALSE
								ELSE
									iRequestThisFrame++
								ENDIF
							ENDIF
							
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetSeven, ciFMMC_VEHICLE7_DYNAMIC_FACILITY_SPAWN_OVERRIDE)
							AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn = STROMBERG
								PRINTLN("[LM] (1) CREATE_FMMC_VEHICLES_MP_RC - Incrementing the iFacilityPosVehSpawned to: ", (iSpecialOverridePosSlot+1))
								iSpecialOverridePosSlot++
							ENDIF
						ENDIF
					ELSE
						IF IS_NET_VEHICLE_DRIVEABLE(niVehicle[i])
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(niVehicle[i])
								IF FMMC_SET_THIS_VEHICLE_CRATES_ROWAN_C(NET_TO_VEH(niVehicle[i]),g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitSet,g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetTwo,g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetSeven,niVehicleCrate[i])
									NET_PRINT("[RCC MISSION] created vehicle crates for veh: ") NET_PRINT_INT(i)  NET_NL()
								ELSE
									NET_PRINT_TIME() NET_PRINT_STRINGS("[RCC MISSION]      ---------->     FMMC VEHICLE WAITING FOR CRATES TO SPAWN    <----------     ", "FMMC veh: ") NET_PRINT_INT(i) NET_NL()
								ENDIF
								IF( STRAND_MISSION_MPH_HUM_KEY_MCS1_CUT_PLAYING() ) // B* 2164633
									PRINTLN( "[RCC MISSION] Calling SET_VEHICLE_ON_GROUND_PROPERLY because STRAND_MISSION_MPH_HUM_KEY_MCS1_CUT_PLAYING()" )
									SET_VEHICLE_ON_GROUND_PROPERLY( NET_TO_VEH( niVehicle[i] ) )
								ENDIF
								IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn = TRAILERLARGE
									SET_VEHICLE_ON_GROUND_PROPERLY(NET_TO_VEH(niVehicle[i]))
								ENDIF
							ELSE
								NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(niVehicle[i])
							ENDIF
						ELSE
							NET_PRINT_TIME() NET_PRINT_STRINGS("[RCC MISSION]      ---------->     FMMC VEHICLE IS NOT DRIVABLE    <----------     ", "FMMC veh: ") NET_PRINT_INT(i) NET_NL()
						ENDIF
					ENDIF
				ENDIF
			ELSE
				NET_PRINT_TIME() NET_PRINT_STRINGS("[RCC MISSION]     ---------->     FMMC VEHICLE NOT SET TO SPAWN AT START     <----------     ", "FMMC veh: ") NET_PRINT_INT(i) NET_NL()
			ENDIF
		ENDIF
	ENDREPEAT

	//Check we have created the Truck
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles i
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niVehicle[i])
		AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn != DUMMY_MODEL_FOR_SCRIPT
			IF SHOULD_VEH_SPAWN_AT_START(i, sCountDataPassed, iStartingPlayers, FALSE, rsgSpawnSeed, sLEGACYMissionContinuityVars)
			AND NOT IS_BIT_SET(iVehicleNotSpawningDueToRandomBS, i)
				RETURN FALSE
			ENDIF
		ENDIF
		IF NETWORK_DOES_NETWORK_ID_EXIST(niVehicle[i])
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(niVehicle[i])
				MODEL_NAMES crateModel = GET_VEHICLE_CRATE_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitSet, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetTwo, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetSeven, NET_TO_VEH(niVehicle[i]))
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niVehicleCrate[i])
				AND crateModel != DUMMY_MODEL_FOR_SCRIPT
					RETURN FALSE
				ENDIF
				IF NOT IS_ENTITY_DEAD(NET_TO_VEH(niVehicle[i]))
					IF DOES_VEHICLE_HAVE_ANY_WEAPON_MODS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
						PRINTLN("HAVE_VEHICLE_WEAPON_MODS_LOADED (3) - Attempting to load weapon vehicle mods for iVeh: ", i)
						IF NOT HAVE_VEHICLE_WEAPON_MODS_LOADED(NET_TO_VEH(niVehicle[i]), g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVModTurret, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVModArmour, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetFive, i, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVModAirCounter, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVModExhaust, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVModBombBay, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVModSpoiler)
							PRINTLN("CREATE_FMMC_VEHICLES_MP_RC - Weapon mods not loaded for vehicle ", i)
							RETURN FALSE
						ENDIF
					ENDIF
				ELSE
					PRINTLN("CREATE_FMMC_VEHICLES_MP_RC - Vehicle is dead! ", i)
				ENDIF
			ENDIF
		ENDIF
		
		IF SHOULD_VEH_SPAWN_AT_START(i, sCountDataPassed, iStartingPlayers, FALSE, rsgSpawnSeed, sLEGACYMissionContinuityVars)
		AND NOT IS_BIT_SET(iVehicleNotSpawningDueToRandomBS, i)
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetFive, ciFMMC_VEHICLE5_RESTART_WITH_TRAILER)
			AND g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[i] != -1
			AND (IS_THIS_A_QUICK_RESTART_JOB() 
			OR IS_CORONA_INITIALISING_A_QUICK_RESTART())
				PRINTLN("[RCC MISSION] - RESPAWN_WITH_TRAILER - Vehicle ", g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[i], " needs to be attached to me")
				
				IF IS_NET_VEHICLE_DRIVEABLE(niVehicle[g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[i]])
					VEHICLE_INDEX trailer = NET_TO_VEH(niVehicle[g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[i]])
					FLOAT fOffset = 4.0
					IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn = HAULER
					OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn = HAULER2
						fOffset = 8.55 // Length of the hauler
					ENDIF
					SET_VEHICLE_ON_GROUND_PROPERLY(NET_TO_VEH(niVehicle[i]), 8.0)
					SET_ENTITY_COORDS(trailer, GET_ENTITY_COORDS(NET_TO_VEH(niVehicle[i])) - (GET_ENTITY_FORWARD_VECTOR(NET_TO_VEH(niVehicle[i]))*fOffset))
					SET_ENTITY_HEADING(trailer, GET_ENTITY_HEADING(NET_TO_VEH(niVehicle[i])))
					SET_VEHICLE_ON_GROUND_PROPERLY(trailer, 8.0)
					ATTACH_VEHICLE_TO_TRAILER(NET_TO_VEH(niVehicle[i]), trailer)
					PRINTLN("[RCC MISSION] - RESPAWN_WITH_TRAILER - Trailer attached!")
					g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[i] = -1
				ELSE
					PRINTLN("[RCC MISSION] - RESPAWN_WITH_TRAILER - Failed to attach trailer to me, its not spawned!")
				ENDIF								
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetFive, ciFMMC_VEHICLE5_RESTART_WITH_TRAILER)
			AND g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[i] != -1
			AND (IS_THIS_A_QUICK_RESTART_JOB() 
			OR IS_CORONA_INITIALISING_A_QUICK_RESTART())
			AND (SHOULD_VEH_SPAWN_AT_START(g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[i], sCountDataPassed, iStartingPlayers, FALSE, rsgSpawnSeed, sLEGACYMissionContinuityVars)
			AND NOT IS_BIT_SET(iVehicleNotSpawningDueToRandomBS, g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[i]))
				PRINTLN("CREATE_FMMC_VEHICLES_MP_RC - iVehicle: ", i, " is still waiting for trailer: ", g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[i], " To be attached.")
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niVehicle[g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[i]])
					PRINTLN("CREATE_FMMC_VEHICLES_MP_RC - Trailer is not even Spawned yet!")
				ENDIF
				
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(niVehicle[i])
			VEHICLE_INDEX vehTemp = NET_TO_VEH(niVehicle[i])
			IF NOT IS_ENTITY_DEAD(vehTemp)
				IF IS_ENTITY_IN_AIR(vehTemp)
					PRINTLN("CREATE_FMMC_VEHICLES_MP_RC - Vehicle is in the air. iVeh", i)
					IF NOT IS_THIS_MODEL_A_PLANE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
					AND NOT IS_THIS_MODEL_A_BOAT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
					AND NOT IS_THIS_MODEL_A_HELI(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
					AND NOT IS_THIS_MODEL_A_TRAIN(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
					AND NOT IS_THIS_MODEL_AN_AMPHIBIOUS_CAR(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
					AND NOT IS_THIS_MODEL_AN_AMPHIBIOUS_QUADBIKE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
					AND NOT IS_THIS_MODEL_A_JETSKI(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
						PRINTLN("CREATE_FMMC_VEHICLES_MP_RC - Vehicle is NOT an air vehicle. Calling: SET_VEHICLE_ON_GROUND_PROPERLY on iVeh", i)
						SET_VEHICLE_ON_GROUND_PROPERLY(NET_TO_VEH(niVehicle[i]), 8.0)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	IF( bCallAgain )
		RETURN FALSE
	ENDIF
	FOR i = 0 TO (FMMC_MAX_VEHICLES-1)
		g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[i] = -1
	ENDFOR
	
	RETURN TRUE
	
ENDFUNC

//////////////////////////////      MP PEDS           //////////////////////////////
FUNC BOOL CREATE_FMMC_PEDS_MP_RC( NETWORK_INDEX &niPed[],
								NETWORK_INDEX &niVeh[],
								sCreatedCountData &sCountDataPassed, 
								INT &istartingplayers[], 
								INT iServerPropertyID,
								sFMMC_RandomSpawnGroupStruct rsgSpawnSeed,
								INT &iServerBS5,
								FMMC_LEGACY_MISSION_CONTINUITY_VARS &sLEGACYMissionContinuityVars) //, INT iStage = 0 )

	
	INT i
	VEHICLE_SEAT tempVehSeat
	
	//Don't stagger on fade transitions
	BOOL bStrand = IS_A_STRAND_MISSION_BEING_INITIALISED() AND (GET_STRAND_MISSION_TRANSITION_TYPE() != FMMC_BRANCHING_MISSION_TRANSITION_TYPE_FADEOUT)
	BOOL bVisible
	
	IF bStrand
		bVisible = TRUE
	ENDIF
	INT iRequestThisFrame
	
	NET_PRINT_TIME() NET_PRINT("     ---------->  number of peds  ") NET_PRINT_INT(g_FMMC_STRUCT_ENTITIES.iNumberOfPeds) NET_NL()
	INT itotalplayers = istartingplayers[0]+istartingplayers[1]+istartingplayers[2]+istartingplayers[3]
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfPeds i
					
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn != DUMMY_MODEL_FOR_SCRIPT
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iRespawnOnRuleChangeBS > 0
			AND NOT IS_BIT_SET(iServerBS5, SBBOOL5_ENTITIES_CAN_RESPAWN_ON_RULE_CHANGE_COPY)
				SET_BIT(iServerBS5, SBBOOL5_ENTITIES_CAN_RESPAWN_ON_RULE_CHANGE_COPY)
			ENDIF

			IF SHOULD_PED_SPAWN_AT_START(i, sCountDataPassed,istartingplayers, FALSE, rsgSpawnSeed, sLEGACYMissionContinuityVars)
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niPed[i])
					BOOL success = FALSE
					VEHICLE_INDEX tempVeh = NULL
					BOOL bDontFreeze = FALSE
					STRING printOut
					UNUSED_PARAMETER(printOut)
					BOOL bReadyToMakePed = TRUE
					
					//If this ped is the Juggernaut, hold up the function until the anims are loaded
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn = INT_TO_ENUM(MODEL_NAMES, HASH("U_M_Y_Juggernaut_01"))
						IF NOT HAVE_JUGGERNAUT_ANIMS_LOADED_FOR_CREATION()
							bReadyToMakePed = FALSE
							PRINTLN("[TMS] Holding back creation while loading Juggernaut animations")
						ENDIF
					ENDIF
					
					INT iRandomHeadingSelected = 0
					VECTOR vPos = GET_PED_SPAWN_LOCATION(i, iServerPropertyID, iRandomHeadingSelected, TRUE)
					FLOAT fHead = GET_PED_SPAWN_HEADING(i, iServerPropertyID, DEFAULT, iRandomHeadingSelected, TRUE)
					
					IF bReadyToMakePed
						IF NOT SHOULD_PED_SPAWN_IN_VEHICLE(i)							
							IF CREATE_NET_PED(niPed[i], PEDTYPE_MISSION, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn, vPos, fHead)
								success = TRUE
								printOut = "     ---------->     FMMC PED CREATED     <----------     "
							ENDIF
						ELSE
							IF IS_NET_VEHICLE_DRIVEABLE(niVeh[g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicle])
								IF NOT IS_VEHICLE_FULL(NET_TO_VEH(niVeh[g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicle]))
									IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicleSeat = -3
										tempVehSeat =GET_FIRST_FREE_VEHICLE_SEAT_RC(NET_TO_VEH(niVeh[g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicle]))
									ELSE
										VEHICLE_SEAT seat = INT_TO_ENUM(VEHICLE_SEAT,g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicleSeat)
										IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicleSeat = VS_ANCHORED_1
										OR IS_VEHICLE_SEAT_FREE(NET_TO_VEH(niVeh[g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicle]),seat)
											tempVehSeat = INT_TO_ENUM(VEHICLE_SEAT,g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicleSeat)
										ELSE
											tempVehSeat =GET_FIRST_FREE_VEHICLE_SEAT_RC(NET_TO_VEH(niVeh[g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicle]))
										ENDIF
									ENDIF
									IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicleSeat = VS_ANCHORED_1
										vPos = g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].vPos
										IF CREATE_NET_PED(niPed[i], PEDTYPE_MISSION, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn, vPos, GET_PED_SPAWN_HEADING(i, iServerPropertyID, DEFAULT, DEFAULT, TRUE))
											VECTOR offset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(NET_TO_VEH(niVeh[g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicle]), vPos)
											offset = offset + <<0.0,0.0, 1.85*0.5>>
											FLOAT heading = GET_PED_SPAWN_HEADING(i, iServerPropertyID, DEFAULT, DEFAULT, TRUE) - GET_ENTITY_HEADING(NET_TO_VEH(niVeh[g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicle]))
											//GET_MODEL_DIMENSIONS
											ATTACH_ENTITY_TO_ENTITY(NET_TO_PED(niPed[i]), NET_TO_VEH(niVeh[g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicle]), 0, offset, <<0,0,heading>>, TRUE, TRUE)
											success = TRUE
																						
											printOut = "     ---------->     FMMC PED CREATED ANCHORED    <----------     "
										ENDIF	
									ELIF CREATE_NET_PED_IN_VEHICLE(niPed[i], niVeh[g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicle], PEDTYPE_MISSION, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn,tempVehSeat)
										success = TRUE
										tempVeh = NET_TO_VEH(niVeh[g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicle])
										bDontFreeze = TRUE
										printOut = "     ---------->     FMMC PED CREATED IN VEHICLE    <----------     "
									ENDIF
								ELSE
									IF CREATE_NET_PED(niPed[i], PEDTYPE_MISSION, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn, vPos, fHead)
										success = TRUE
										printOut = "     ---------->     FMMC PED CREATED VEHICLE FULL     <----------     "
									ENDIF	
								ENDIF
							ELSE
								IF CREATE_NET_PED(niPed[i], PEDTYPE_MISSION, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn, vPos, fHead)
									success = TRUE
									printOut = "     ---------->     FMMC PED CREATED VEHICLE DEAD    <----------     "
								ENDIF	
								NET_PRINT_TIME() NET_PRINT("     ---------->     FMMC PED ") NET_PRINT_INT(i) NET_PRINT(" spawn vehicle not drivable  <----------     ") NET_NL()
							ENDIF
						ENDIF		
						
					ENDIF
					IF success
						SET_UP_FMMC_PED_RC(NET_TO_PED(niPed[i]), i, itotalplayers, sLEGACYMissionContinuityVars, bVisible, tempVeh)
						SET_NETWORK_INDEX_FROZEN_AND_UNABLE_TO_MIGRATE(niPed[i],TRUE,bDontFreeze)
						sCountDataPassed.iNumPedCreated[0]++
						PRINTLN("[LM][FMMC PED CREATED] - Added to iNumPedCreated, is now: ", sCountDataPassed.iNumPedCreated[0], " for iRule: ", 0)
						NET_PRINT_TIME() NET_PRINT_STRINGS(printOut, "") NET_NL()
						IF bStrand
							IF iRequestThisFrame >= PICK_INT(IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING(), ciMAX_PER_FRAME_CREATES_AIRLOCK, ciMAX_PER_FRAME_CREATES)
								RETURN FALSE
							ELSE
								iRequestThisFrame++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				NET_PRINT_TIME() NET_PRINT("     ---------->     FMMC PED ") NET_PRINT_INT(i) NET_PRINT(" is not set to spawn at start.     <----------     ") NET_NL()
			ENDIF
		ELSE
			NET_PRINT_TIME() NET_PRINT("     ---------->     FMMC PED ") NET_PRINT_INT(i) NET_PRINT(" is dummy model.     <----------     ") NET_NL()
		ENDIF	
	ENDREPEAT
	//Check we have created all Security Guards
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfPeds i
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niPed[i])
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn != DUMMY_MODEL_FOR_SCRIPT
				IF SHOULD_PED_SPAWN_AT_START(i, sCountDataPassed, istartingplayers, FALSE, rsgSpawnSeed, sLEGACYMissionContinuityVars)
					NET_PRINT_TIME() NET_PRINT("     ---------->     FMMC PED ") NET_PRINT_INT(i) NET_PRINT(" Stuck on this ped.     <----------     ") NET_NL()
					RETURN FALSE
				ELSE
					NET_PRINT_TIME() NET_PRINT("     ---------->     FMMC PED ") NET_PRINT_INT(i) NET_PRINT(" is NOT set to spawn at start 1.     <----------     ") NET_NL()
				ENDIF
			ELSE
				NET_PRINT_TIME() NET_PRINT("     ---------->     FMMC PED ") NET_PRINT_INT(i) NET_PRINT(" is dummy model 1.     <----------     ") NET_NL()
			ENDIF
		ELSE
			NET_PRINT_TIME() NET_PRINT("     ---------->     FMMC PED ") NET_PRINT_INT(i) NET_PRINT(" exists.     <----------     ") NET_NL()
		ENDIF
	ENDREPEAT
	RETURN TRUE
ENDFUNC


//////////////////////////////      MP OBJECTS           //////////////////////////////
FUNC BOOL CREATE_FMMC_OBJECTS_MP_RC( 	NETWORK_INDEX &niObject[],
										NETWORK_INDEX &niPed[],
										NETWORK_INDEX &niVeh[],
										NETWORK_INDEX &niCrateDoors[], // Passing through the index where any blow-up-crate minigame doors will get made
										sCreatedCountData &sCountDataPassed, 
										INT &iNumStartingPlayers[],
										sFMMC_RandomSpawnGroupStruct rsgSpawnSeed,
										INT &iServerBS5,
										FMMC_LEGACY_MISSION_CONTINUITY_VARS &sLEGACYMissionContinuityVars)	
										//, INT iStage = 0 )
	INT i
	BOOL bOnGround = TRUE
	//Don't stagger on fade transitions
	BOOL bStrand = IS_A_STRAND_MISSION_BEING_INITIALISED() AND (GET_STRAND_MISSION_TRANSITION_TYPE() != FMMC_BRANCHING_MISSION_TRANSITION_TYPE_FADEOUT)
	BOOL bVisible	
	IF bStrand
		bVisible = TRUE
	ENDIF
	FLOAT fHeading
	SPAWN_SEARCH_PARAMS sSearchParams
	INT	iObjModelTailIndex[OBJECT_LIBRARY_MAX]
	MODEL_NAMES mnObjectType[OBJECT_LIBRARY_MAX][MAX_OBJECTS_IN_LIBRARY]
	INT iRequestThisFrame
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfObjects 	 i
		
		//Catch to detect invalid objects in UGC content
		IF g_sMPTunables.bENABLE_CREATOR_BLOCK
			IF NOT IS_CURRENT_MISSION_ROCKSTAR_CREATED()
			#IF IS_DEBUG_BUILD
			AND NOT IS_ROCKSTAR_DEV()
			#ENDIF
				INITIALISE_OBJECT_MODEL_ARRAY(iObjModelTailIndex, mnObjectType)
				IF GET_OBJ_TYPE_SELECTION_FROM_MODEL_IN_LIBRARY(iObjModelTailIndex, mnObjectType, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn, OBJECT_LIBRARY_CAPTURE) = 0
					IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn != PROP_SECURITY_CASE_01
						SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn)
						ASSERTLN("[CREATOR_BLOCK] INVALID OBJECT DETECTED SWITCHING MODEL TO PROP_SECURITY_CASE_01")
						PRINTLN("[CREATOR_BLOCK] INVALID OBJECT DETECTED SWITCHING MODEL TO PROP_SECURITY_CASE_01")
						g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn = PROP_SECURITY_CASE_01
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iRespawnOnRuleChangeBS > 0
		AND NOT IS_BIT_SET(iServerBS5, SBBOOL5_ENTITIES_CAN_RESPAWN_ON_RULE_CHANGE_COPY)
			SET_BIT(iServerBS5, SBBOOL5_ENTITIES_CAN_RESPAWN_ON_RULE_CHANGE_COPY)
		ENDIF
		
		IF SHOULD_OBJ_SPAWN_AT_START(i, sCountDataPassed, iNumStartingPlayers, FALSE, rsgSpawnSeed)
			IF NOT HAS_OBJECT_CREATION_FINISHED(niObject[i], i, niCrateDoors, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn)
				VECTOR vObjPos = <<0,0,0>>	
				IF NOT IS_MINI_GAME_ACTIVE_ON_THIS_OBJECT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iLegacyMiniGameBitSet)
				AND SHOULD_PLACED_OBJECT_BE_PORTABLE(i)
					bOnGround = TRUE
					IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitSet, cibsOBJ_SnapToGround)
						IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectAttachmentPed !=-1
							IF NOT IS_NET_PED_INJURED(niPed[g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectAttachmentPed])
								bOnGround = FALSE
							ENDIF
						ENDIF
					ELSE
						PRINTLN("[JS] CREATE_FMMC_OBJECTS_MP_RC cibsOBJ_SnapToGround is TRUE")
					ENDIF
					
					IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
						IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitSet, cibsOBJ_SnapToGround)
						AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitSet, cibsOBJ_Airborne)
						AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitSet, cibsOBJ_Position_Override)
							bOnGround = FALSE
							PRINTLN("CREATE_FMMC_OBJECTS_MP_RC - Setting bOnGround to FALSE because SnapToGround is disabled")
						ENDIF
					ENDIF
					
					BOOL bSpawnNear
					
					IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnNearType != ciRULE_TYPE_NONE
					AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnNearEntityID > -1
						vObjPos = GET_SPAWN_NEAR_LOCATION(i, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnNearType, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnNearEntityID)

						sSearchParams.bConsiderInteriors = TRUE
						sSearchParams.fMinDistFromPlayer = 2			
						sSearchParams.vFacingCoords = vObjPos
						sSearchParams.vAvoidCoords[0] = vObjPos
						sSearchParams.fAvoidRadius[0] = g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].fRespawnMinRange			
						IF NOT GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY(vObjPos, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].fRespawnRange, vObjPos, fHeading, sSearchParams)
							vObjPos = g_FMMC_STRUCT_ENTITIES.sPlacedObject[ i ].vPos
						ELSE
							bSpawnNear = TRUE
						ENDIF
					ELSE
						vObjPos = g_FMMC_STRUCT_ENTITIES.sPlacedObject[ i ].vPos
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitSetFour, cibsOBJ4_UseRandomPoolPosAsInitialSpawn)				
						INT iSpawnPool = GET_RANDOM_SELECTION_FROM_SPAWN_POOL(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sWarpLocationSettings.vPosition)						
						vObjPos = g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sWarpLocationSettings.vPosition[iSpawnPool]
						PRINTLN( "[RCC MISSION] CREATE_FMMC_OBJECTS_MP_RC - Grabbing random position from pool for obj ", i, " iSpawnPool: ", iSpawnPool, " vObjPos: ", vObjPos)
					ENDIF
					
					IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnNearType != ciRULE_TYPE_NONE
					AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnNearEntityID > -1
						IF bSpawnNear
							niObject[i] = CREATE_PORTABLE_PICKUP_BASED_ON_SETTINGS( i, bOnGround, vObjPos, IS_KING_OF_THE_HILL())
						ENDIF
					ELSE
						niObject[i] = CREATE_PORTABLE_PICKUP_BASED_ON_SETTINGS( i, bOnGround, vObjPos, IS_KING_OF_THE_HILL())
					ENDIF
					
					SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(niObject[i],TRUE)
					SET_UP_FMMC_OBJ_RC(NET_TO_OBJ(niObject[i]), i, niPed, sLEGACYMissionContinuityVars, bOnGround, bVisible, FALSE, bSpawnNear, TRUE)
					
					sCountDataPassed.iNumObjCreated[0]++
					
					PROCESS_REPOSITION_OBJ_BASED_ON_SUB_SPAWN_GROUP_SETTINGS(i, NET_TO_OBJ( niObject[ i ] ), rsgSpawnSeed)
					
					SET_NETWORK_INDEX_FROZEN_AND_UNABLE_TO_MIGRATE(niObject[i],TRUE)					
					
					#IF IS_DEBUG_BUILD
						NET_PRINT_TIME() NET_PRINT_STRINGS("     ---------->     FMMC PICKUP OBJECT CREATED     <----------     ", "FMMC") NET_NL()
					#ENDIF
					IF bStrand
						IF iRequestThisFrame >= PICK_INT(IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING(), ciMAX_PER_FRAME_CREATES_AIRLOCK, ciMAX_PER_FRAME_CREATES)
							RETURN FALSE
						ELSE
							iRequestThisFrame++
						ENDIF
					ENDIF
				ELSE
					IF NOT NETWORK_DOES_NETWORK_ID_EXIST( niObject[ i ] )
						BOOL bSpawnNear
						IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnNearType != ciRULE_TYPE_NONE
						AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnNearEntityID > -1
							vObjPos = GET_SPAWN_NEAR_LOCATION(i, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnNearType, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnNearEntityID)
							sSearchParams.bConsiderInteriors = TRUE
							sSearchParams.fMinDistFromPlayer = 2			
							sSearchParams.vFacingCoords = vObjPos
							sSearchParams.vAvoidCoords[0] = vObjPos
							sSearchParams.fAvoidRadius[0] = g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].fRespawnMinRange			
							IF NOT GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY(vObjPos, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].fRespawnRange, vObjPos, fHeading, sSearchParams)
								vObjPos = g_FMMC_STRUCT_ENTITIES.sPlacedObject[ i ].vPos
							ELSE
								bSpawnNear = TRUE
							ENDIF
						ELSE
							vObjPos = g_FMMC_STRUCT_ENTITIES.sPlacedObject[ i ].vPos
						ENDIF
						
						IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnNearType != ciRULE_TYPE_NONE
						AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnNearEntityID > -1
							IF bSpawnNear
								IF CREATE_NET_OBJ( niObject[ i ], g_FMMC_STRUCT_ENTITIES.sPlacedObject[ i ].mn, vObjPos )
									SET_OBJECT_SETTINGS_AFTER_CREATION( i, niVeh, NET_TO_OBJ( niObject[ i ] ), sCountDataPassed, sLEGACYMissionContinuityVars )
									PROCESS_REPOSITION_OBJ_BASED_ON_SUB_SPAWN_GROUP_SETTINGS(i, NET_TO_OBJ( niObject[ i ] ), rsgSpawnSeed)
									SET_NETWORK_INDEX_FROZEN_AND_UNABLE_TO_MIGRATE(niObject[i],TRUE)
								ENDIF
							ENDIF
						ELSE
							IF CREATE_NET_OBJ( niObject[ i ], g_FMMC_STRUCT_ENTITIES.sPlacedObject[ i ].mn, vObjPos )
								SET_OBJECT_SETTINGS_AFTER_CREATION( i, niVeh, NET_TO_OBJ( niObject[ i ] ), sCountDataPassed, sLEGACYMissionContinuityVars )
								PROCESS_REPOSITION_OBJ_BASED_ON_SUB_SPAWN_GROUP_SETTINGS(i, NET_TO_OBJ( niObject[ i ] ), rsgSpawnSeed)
								SET_NETWORK_INDEX_FROZEN_AND_UNABLE_TO_MIGRATE(niObject[i],TRUE)
							ENDIF
						ENDIF
					ELSE
						IF IS_THIS_OBJECT_A_HACK_CONTAINER( i )
							IF HAS_CONTAINER_AND_ASSETS_LOADED()
								IF CREATE_AND_ATTACH_CRATE_DOORS( niObject[ i ], niCrateDoors, g_FMMC_STRUCT_ENTITIES.sPlacedObject[ i ].vPos )									
									sCountDataPassed.iNumObjCreated[0] += 3
									IF bStrand
										IF iRequestThisFrame >= PICK_INT(IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING(), ciMAX_PER_FRAME_CREATES_AIRLOCK, ciMAX_PER_FRAME_CREATES)
											RETURN FALSE
										ELSE
											iRequestThisFrame++
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			NET_PRINT_TIME() NET_PRINT_STRINGS("     ---------->     FMMC PICKUP OBJECT NOT SET TO SPAWN AT START     <----------     ", "FMMC obj: ") NET_PRINT_INT(i) NET_NL()
		ENDIF
	ENDREPEAT

	//Check we have created all the things
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfObjects 	 i
		
		IF SHOULD_OBJ_SPAWN_AT_START(i, sCountDataPassed, iNumStartingPlayers, FALSE, rsgSpawnSeed)
			IF NOT HAS_OBJECT_CREATION_FINISHED( niObject[ i ], i, niCrateDoors, g_FMMC_STRUCT_ENTITIES.sPlacedObject[ i ].mn )
				RETURN FALSE
			ENDIF
		ENDIF
		
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CREATE_FMMC_DROP_ZONE_SPAWN_VEHICLES( NETWORK_INDEX &niDZSpawnVehicle[],
												NETWORK_INDEX &niDZSpawnVehPed[],
												INT &istartingplayers[])
	
	BOOL bDoneLoading = TRUE
	
	INT i, y, index, numTeams
	
	FOR i = 0 TO FMMC_MAX_TEAMS-1
		IF istartingplayers[i] > 0
			numTeams++
		ENDIF
	ENDFOR
	
	//B*2616032 - Hack this to stop a heli not spawning for a team. istartingplayers isn't the most reliable thing.
	numTeams = 4
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[JJT] CREATE_FMMC_DROP_ZONE_SPAWN_VEHICLES - numTeams = ", numTeams)
	#ENDIF
	
	FOR i = 0 TO (numTeams-1)
		
		INT iTeamVehicleCount = 0
		
		//IF MORE THAN 4 PLAYERS, SPAWN A SECOND CARGOBOB FOR THE TEAM
		IF istartingplayers[i] > 4
			iTeamVehicleCount = 1 
		ENDIF
		
		FOR y = 0 TO iTeamVehicleCount
			
			//If we're using a airspawn vehicle for DROP ZONE
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][y].iSpawnBitSet, ci_SpawnBS_AirSpawn)
				index = i + (y*4)
				
				PRINTLN("[JJT] CREATE_FMMC_DROP_ZONE_SPAWN_VEHICLES - ci_SpawnBS_AirSpawn is set for: Team = ", i, ", Spawn Point = ", y, ", Index = ", index ," istartingplayers[",i,"] = ", istartingplayers[i])
				
				IF index < FMMC_MAX_TEAMS*2 //Only two vehicles per team
					IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niDZSpawnVehicle[index])
						
						IF CAN_REGISTER_MISSION_VEHICLES( 1 )
							REQUEST_MODEL( CARGOBOB4 )
							IF HAS_MODEL_LOADED( CARGOBOB4 )
								IF CREATE_NET_VEHICLE(	niDZSpawnVehicle[index], CARGOBOB4, 
														g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][y].vPos, 
														g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][y].fHead  )
									
									FREEZE_ENTITY_POSITION(	NET_TO_VEH( niDZSpawnVehicle[index] ), TRUE )
									SET_VEHICLE_ENGINE_ON( NET_TO_VEH( niDZSpawnVehicle[index] ), TRUE, TRUE )
									SET_HELI_BLADES_FULL_SPEED( NET_TO_VEH( niDZSpawnVehicle[index] ))
									SET_ENTITY_INVINCIBLE( NET_TO_VEH( niDZSpawnVehicle[index] ), TRUE )
									SET_NETWORK_ID_CAN_MIGRATE( niDZSpawnVehicle[index], FALSE )
									SET_FORCE_HD_VEHICLE( NET_TO_VEH( niDZSpawnVehicle[index] ), TRUE)
									
									#IF IS_DEBUG_BUILD
										IF IS_FAKE_MULTIPLAYER_MODE_SET()
											SET_VEHICLE_DOOR_OPEN( NET_TO_VEH( niDZSpawnVehicle[index] ), SC_DOOR_REAR_LEFT, FALSE, TRUE )
										ENDIF
									#ENDIF
									
									PRINTLN("[JJT] CREATE_FMMC_DROP_ZONE_SPAWN_VEHICLES - Created CargoBob4 at postion: ", g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][y].vPos) 
								ELSE
									PRINTLN("[JJT] CREATE_FMMC_DROP_ZONE_SPAWN_VEHICLES - CREATE_NET_VEHICLE creating.")
								ENDIF
							ELSE
								PRINTLN("[JJT] CREATE_FMMC_DROP_ZONE_SPAWN_VEHICLES - Requesting CARGOBOB4 model.")
							ENDIF
							bDoneLoading = FALSE
						ELSE
							PRINTLN("[JJT] CREATE_FMMC_DROP_ZONE_SPAWN_VEHICLES - CAN_REGISTER_MISSION_VEHICLES Failed.")
						ENDIF
					ELSE
						PRINTLN("[JJT] CREATE_FMMC_DROP_ZONE_SPAWN_VEHICLES - niDZSpawnVehicle[", index,"] does exist.")
						
						IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niDZSpawnVehPed[index])
							IF CAN_REGISTER_MISSION_PEDS( 1 )
								REQUEST_MODEL( S_M_M_PILOT_02 )
								IF HAS_MODEL_LOADED( S_M_M_PILOT_02 )
									IF CREATE_NET_PED(	niDZSpawnVehPed[index], PEDTYPE_MISSION, S_M_M_PILOT_02, 
														g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][y].vPos, 
														g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][y].fHead  )
										
										IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(niDZSpawnVehicle[index]))
										AND NOT IS_PED_INJURED(NET_TO_PED(niDZSpawnVehPed[index]))
											
											TASK_ENTER_VEHICLE( NET_TO_PED(niDZSpawnVehPed[index]),
																NET_TO_VEH(niDZSpawnVehicle[index]),
																1, VS_DRIVER, PEDMOVEBLENDRATIO_RUN, ECF_WARP_PED )
										ELSE
											#IF IS_DEBUG_BUILD
											PRINTLN("[JJT] CREATE_FMMC_DROP_ZONE_SPAWN_VEHICLES - Couldn't place ped into vehicle")
											IF NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(niDZSpawnVehicle[index]))
												PRINTLN("[JJT] CREATE_FMMC_DROP_ZONE_SPAWN_VEHICLES - IS_VEHICLE_DRIVEABLE(NET_TO_VEH(niDZSpawnVehicle[index]))")
											ENDIF
											
											IF IS_PED_INJURED(NET_TO_PED(niDZSpawnVehPed[index]))
												PRINTLN("[JJT] CREATE_FMMC_DROP_ZONE_SPAWN_VEHICLES - IS_PED_INJURED(NET_TO_PED(niDZSpawnVehPed[index]))")
											ENDIF
											
											#ENDIF
										ENDIF
										
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(niDZSpawnVehPed[index]), TRUE)
										
										PRINTLN("[JJT] CREATE_FMMC_DROP_ZONE_SPAWN_VEHICLES - Created ped for CargoBob. Index = ", index) 
									ELSE
										PRINTLN("[JJT] CREATE_FMMC_DROP_ZONE_SPAWN_VEHICLES - CREATE_NET_PED creating.")
									ENDIF
								ELSE
									PRINTLN("[JJT] CREATE_FMMC_DROP_ZONE_SPAWN_VEHICLES - Requesting S_M_M_PILOT_02 model.")
								ENDIF
								bDoneLoading = FALSE
							ELSE
								PRINTLN("[JJT] CREATE_FMMC_DROP_ZONE_SPAWN_VEHICLES - CAN_REGISTER_MISSION_VEHICLES Failed.")
							ENDIF
						ELSE
							PRINTLN("[JJT] CREATE_FMMC_DROP_ZONE_SPAWN_VEHICLES - niDZSpawnVehPed[", index,"] does exist.")
						ENDIF
					ENDIF
				ELSE
					PRINTLN("[JJT] CREATE_FMMC_DROP_ZONE_SPAWN_VEHICLES - Index is too high! Index = ", index)
				ENDIF
			ELSE
				PRINTLN("[JJT] CREATE_FMMC_DROP_ZONE_SPAWN_VEHICLES - g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[", i,"][", y,"].iSpawnBitSet, ci_SpawnBS_AirSpawn is NOT set.")
			ENDIF
		ENDFOR
	ENDFOR
	
	RETURN bDoneLoading
ENDFUNC

FUNC BOOL CREATE_FMMC_SVM_BJXL(NETWORK_INDEX &niBJXL, INT iServerPropertyID)

	IF SVM_FLOW_IS_CURRENT_MISSION_SVM_MISSION()
	AND SVM_GET_LOBBY_EXIT_TYPE() = 1	//On foot
		IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(1)
		OR FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(0)
				PRINTLN("[JS] - CREATE_FMMC_SVM_BJXL - Quick restart checkpoint 2, don't bother making the BJXL")
			RETURN TRUE
		ENDIF
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niBJXL)
			IF CAN_REGISTER_MISSION_VEHICLES( 1 )
				IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("BJXL")))
					VECTOR vCoords
					FLOAT fHeading =  GET_BJXL_SPAWN_COORDS_AND_HEADING(vCoords, iServerPropertyID)
					IF NOT IS_VECTOR_ZERO(vCoords)
						IF CREATE_NET_VEHICLE(niBJXL, INT_TO_ENUM(MODEL_NAMES, HASH("BJXL")), vCoords, fHeading)
							SET_VEHICLE_ON_GROUND_PROPERLY(NET_TO_VEH(niBJXL))
							PRINTLN("[JS] - CREATE_FMMC_SVM_BJXL - Vehicle created.")
							RETURN TRUE
						ELSE
							PRINTLN("[JS] - CREATE_FMMC_SVM_BJXL - Failing on CREATE_NET_VEHICLE")
							RETURN FALSE
						ENDIF
					ELSE
						PRINTLN("[JS] - CREATE_FMMC_SVM_BJXL - We aren't in an office, don't spawn the BJXL")
						RETURN TRUE
					ENDIF
				ELSE
					PRINTLN("[JS] - CREATE_FMMC_SVM_BJXL - Failing on REQUEST_LOAD_MODEL")
					RETURN FALSE
				ENDIF
			ELSE
				PRINTLN("[JS] - CREATE_FMMC_SVM_BJXL - Failing on CAN_REGISTER_MISSION_VEHICLES")
				RETURN FALSE
			ENDIF
		ELSE
			//Do any vehicle set up here
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL LOAD_AND_CREATE_ALL_FMMC_ENTITIES_RC( FMMC_SERVER_DATA_STRUCT &sSDSpassed,
												sCreatedCountData &sCountDataPassed,
												INT &istartingplayers[],
												NETWORK_INDEX& niMinigameCrateDoors[],
												INT iContainerBitset,
												INT iServerPropertyID,
												sFMMC_RandomSpawnGroupStruct rsgSpawnSeed,
												NETWORK_INDEX &niDZSpawnVehicle[],
												NETWORK_INDEX &niDZSpawnVehPed[],
												NETWORK_INDEX &niBJXL,
												INT &iServerBS5,
												INT &iVehicleNotSpawningDueToRandomBS,
												INT &iSpecialOverridePosSlot,
												FMMC_LEGACY_MISSION_CONTINUITY_VARS &sLEGACYMissionContinuityVars)
	
	next_gen_only_entity_counts sEntityCounts
	
	IF IS_SKYSWOOP_IN_SKY()
		IF NOT BUSYSPINNER_IS_ON()
			BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("MP_SPINLOADING")
			END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_LOADING))
		ENDIF
	ENDIF
			
	
	IF REQUEST_LOAD_FMMC_MODELS_RC( sCountDataPassed, istartingplayers, sEntityCounts, rsgSpawnSeed, sLEGACYMissionContinuityVars)
		//Set the number of entities to load
		
		INT iNumberOfPeds		= CLAMP_INT(g_FMMC_STRUCT_ENTITIES.iNumberOfPeds, 0, g_FMMC_STRUCT_ENTITIES.iPedSpawnCap)
		INT iNumberOfVehicles	= g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles
		IF SVM_FLOW_IS_CURRENT_MISSION_SVM_MISSION()
			iNumberOfVehicles++
			PRINTLN("[JS] LOAD_AND_CREATE_ALL_FMMC_ENTITIES_RC - Adding 1 to iNumberOfVehicles for the BJXL")
		ENDIF
		INT iNumberOfObjects	= GET_NETWORKED_OBJECT_COUNT() + g_FMMC_STRUCT_ENTITIES.iNumberOfVehicleCrates
		NET_PRINT("LOAD_AND_CREATE_ALL_FMMC_ENTITIES_RC - g_FMMC_STRUCT_ENTITIES.iNumberOfObjects: ") NET_PRINT_INT(g_FMMC_STRUCT_ENTITIES.iNumberOfObjects) NET_NL()
		NET_PRINT("LOAD_AND_CREATE_ALL_FMMC_ENTITIES_RC - sCountDataPassed.iNumObjCreated[0]: ") NET_PRINT_INT(sCountDataPassed.iNumObjCreated[0]) NET_NL()		
		NET_PRINT("LOAD_AND_CREATE_ALL_FMMC_ENTITIES_RC - g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps: ") NET_PRINT_INT(g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps) NET_NL()
		NET_PRINT("LOAD_AND_CREATE_ALL_FMMC_ENTITIES_RC - g_FMMC_STRUCT_ENTITIES.iNumberOfVehicleCrates: ") NET_PRINT_INT(g_FMMC_STRUCT_ENTITIES.iNumberOfVehicleCrates) NET_NL()
		NET_PRINT("LOAD_AND_CREATE_ALL_FMMC_ENTITIES_RC - g_FMMC_STRUCT_ENTITIES.iNumberOfPeds: ") NET_PRINT_INT(g_FMMC_STRUCT_ENTITIES.iNumberOfPeds) NET_NL()
		NET_PRINT("LOAD_AND_CREATE_ALL_FMMC_ENTITIES_RC - sCountDataPassed.iNumPedCreated[0] ") NET_PRINT_INT(sCountDataPassed.iNumPedCreated[0]) NET_NL()
		NET_PRINT("LOAD_AND_CREATE_ALL_FMMC_ENTITIES_RC - g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles: ") NET_PRINT_INT(g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles) NET_NL()
		NET_PRINT("LOAD_AND_CREATE_ALL_FMMC_ENTITIES_RC - sCountDataPassed.iNumVehCreated[0]: ") NET_PRINT_INT(sCountDataPassed.iNumVehCreated[0]) NET_NL()
		NET_PRINT("LOAD_AND_CREATE_ALL_FMMC_ENTITIES_RC - g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons: ") NET_PRINT_INT(g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons) NET_NL()
		
		#IF NOT IS_NEXTGEN_BUILD
			NET_PRINT("LOAD_AND_CREATE_ALL_FMMC_ENTITIES_RC -removing NG only iPeds = ") NET_PRINT_INT(sEntityCounts.iPeds) NET_NL()
			iNumberOfPeds -= sEntityCounts.iPeds
			
			NET_PRINT("LOAD_AND_CREATE_ALL_FMMC_ENTITIES_RC -removing NG only  iVehicles = ") NET_PRINT_INT(sEntityCounts.iVehicles) NET_NL()
			iNumberOfVehicles -= sEntityCounts.iVehicles
		#ENDIF
		
		iNumberOfPeds -= sCountDataPassed.iNumPedCreated[0]
		iNumberOfVehicles -= sCountDataPassed.iNumVehCreated[0]
		iNumberOfObjects -= sCountDataPassed.iNumObjCreated[0]
	
		//If we need to make container doors:
		iNumberOfObjects += ( COUNT_SET_BITS( iContainerBitset ) * 3 ) 
		
		NET_PRINT("LOAD_AND_CREATE_ALL_FMMC_ENTITIES_RC - iNumberOfPeds to register: ") NET_PRINT_INT(iNumberOfPeds) NET_NL()
		NET_PRINT("LOAD_AND_CREATE_ALL_FMMC_ENTITIES_RC - iNumberOfVehicles to register: ") NET_PRINT_INT(iNumberOfVehicles) NET_NL()
		NET_PRINT("LOAD_AND_CREATE_ALL_FMMC_ENTITIES_RC - iNumberOfObjects to register: ") NET_PRINT_INT(iNumberOfObjects) NET_NL()
		
		IF CAN_REGISTER_MISSION_ENTITIES(iNumberOfPeds, iNumberOfVehicles, iNumberOfObjects,  0)
			IF CREATE_FMMC_VEHICLES_MP_RC(sSDSpassed.niVehicle, sSDSpassed.niVehicleCrate, sCountDataPassed, istartingplayers, iServerPropertyID, sSDSpassed.casco_index, rsgSpawnSeed, iServerBS5, iVehicleNotSpawningDueToRandomBS, iSpecialOverridePosSlot, sLEGACYMissionContinuityVars)
				IF CREATE_FMMC_PEDS_MP_RC(sSDSpassed.niPed, sSDSpassed.niVehicle, sCountDataPassed, istartingplayers, iServerPropertyID, rsgSpawnSeed, iServerBS5, sLEGACYMissionContinuityVars)
					IF CREATE_FMMC_OBJECTS_MP_RC(sSDSpassed.niObject, sSDSpassed.niPed, sSDSpassed.niVehicle, niMinigameCrateDoors, sCountDataPassed, istartingplayers, rsgSpawnSeed, iServerBS5, sLEGACYMissionContinuityVars)
						IF CREATE_FMMC_DYNOPROPS_MP_RC(sSDSpassed.niDynoProps, TRUE, sCountDataPassed, iStartingPlayers, iServerBS5, rsgSpawnSeed, sLEGACYMissionContinuityVars)
							IF CREATE_FMMC_DROP_ZONE_SPAWN_VEHICLES(niDZSpawnVehicle,niDZSpawnVehPed,istartingplayers)
								IF CREATE_FMMC_SVM_BJXL(niBJXL, iServerPropertyID)
									NET_PRINT_TIME() NET_PRINT_STRINGS("     ---------->     LOAD_AND_CREATE_ALL_FMMC_ENTITIES_RC RETURNING TRUE    <----------     ", "LOAD_AND_CREATE_ALL_FMMC_ENTITIES") NET_NL()
									IF BUSYSPINNER_IS_ON()
										BUSYSPINNER_OFF()
									ENDIF
									
									RETURN TRUE
								ELSE
									NET_PRINT_TIME() NET_PRINT_STRINGS("-----> Waiting to create BJXL <-----", "LOAD_AND_CREATE_ALL_FMMC_ENTITIES_RC") NET_NL()
									RETURN FALSE
								ENDIF
							ELSE
								NET_PRINT_TIME() NET_PRINT_STRINGS("-----> Waiting to create Drop Zone spawn vehicles <-----", "LOAD_AND_CREATE_ALL_FMMC_ENTITIES_RC") NET_NL()
								RETURN FALSE
							ENDIF
						ELSE
							NET_PRINT_TIME() NET_PRINT_STRINGS("-----> Waiting to create Dyno props <-----", "LOAD_AND_CREATE_ALL_FMMC_ENTITIES_RC") NET_NL()
							RETURN FALSE
						ENDIF
					ELSE
						NET_PRINT_TIME() NET_PRINT_STRINGS("-----> Waiting to create OBJECTS <-----", "LOAD_AND_CREATE_ALL_FMMC_ENTITIES_RC") NET_NL()
						RETURN FALSE
					ENDIF
				ELSE
					NET_PRINT_TIME() NET_PRINT_STRINGS("-----> Waiting to create PEDS <-----", "LOAD_AND_CREATE_ALL_FMMC_ENTITIES_RC") NET_NL()
					RETURN FALSE
				ENDIF
			ELSE
				NET_PRINT_TIME() NET_PRINT_STRINGS("-----> Waiting to create VEHICLES <-----", "LOAD_AND_CREATE_ALL_FMMC_ENTITIES_RC") NET_NL()
				RETURN FALSE
			ENDIF
		ELSE
			NET_PRINT_TIME() NET_PRINT_STRINGS("-----> Waiting for CAN_REGISTER_MISSION_VEHICLES <-----", "LOAD_AND_CREATE_ALL_FMMC_ENTITIES_RC") NET_NL()
			RETURN FALSE
		ENDIF
	ELSE
		NET_PRINT_TIME() NET_PRINT_STRINGS("-----> Waiting for Model Requests <-----", "LOAD_AND_CREATE_ALL_FMMC_ENTITIES_RC") NET_NL()
		RETURN FALSE
	ENDIF	
	
	NET_PRINT_TIME() NET_PRINT_STRINGS("----->EVERYTHING LOADED! <-----", "LOAD_AND_CREATE_ALL_FMMC_ENTITIES_RC") NET_NL()
	
	RETURN TRUE
	
ENDFUNC

//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: GENERAL PED/VEHICLE/OBJECT STUFF !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************

FUNC BOOL SHOULD_AGRO_FAIL_FOR_TEAM(INT iped,INT iTeam)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAgroFailPriority[iTeam] > -1
		IF NOT ( IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroAfterAndInclRuleT0 + iTeam * 2) 
				 OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroBeforeAndInclRuleT0 + iTeam * 2) )
				
			//Only aggro on this rule, not on any others:
			IF MC_serverBD_4.iCurrentHighestPriority[iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAgroFailPriority[iTeam]
				IF (NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPED_BSEight_Aggro_SwitchesOnMidpoint_T0 + iTeam))
				OR (NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAgroFailPriority[iTeam])) // If we haven't reached the midpoint yet
					RETURN TRUE
				ENDIF
			ENDIF
			
		ELSE
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroAfterAndInclRuleT0 + iTeam * 2)
				
				IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPED_BSEight_Aggro_SwitchesOnMidpoint_T0 + iTeam)
					IF MC_serverBD_4.iCurrentHighestPriority[iTeam] >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAgroFailPriority[iTeam]
						RETURN TRUE
					ENDIF
				ELSE
					IF MC_serverBD_4.iCurrentHighestPriority[iTeam] > g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAgroFailPriority[iTeam]
						RETURN TRUE
					ELIF MC_serverBD_4.iCurrentHighestPriority[iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAgroFailPriority[iTeam]
						IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAgroFailPriority[iTeam])
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
				
			ELSE
				
				IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPED_BSEight_Aggro_SwitchesOnMidpoint_T0 + iTeam)
					IF MC_serverBD_4.iCurrentHighestPriority[iTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAgroFailPriority[iTeam]
						RETURN TRUE
					ENDIF
				ELSE
					IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAgroFailPriority[iTeam]
						RETURN TRUE
					ELIF MC_serverBD_4.iCurrentHighestPriority[iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAgroFailPriority[iTeam]
						IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAgroFailPriority[iTeam])
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_AIRCRAFT_LANDED(VEHICLE_INDEX tempVeh)
	
	IF (GET_ENTITY_SPEED(tempVeh) < 5.0 AND NOT IS_ENTITY_IN_AIR(tempVeh))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC VECTOR GET_VEHICLE_FRONT_COORD( VEHICLE_INDEX veh )
	VECTOR vPos, vDimMin, vDimMax, vForward
	IF( IS_ENTITY_ALIVE( veh ) )
		GET_MODEL_DIMENSIONS( GET_ENTITY_MODEL( veh ), vDimMin, vDimMax )
		vForward 	= GET_ENTITY_FORWARD_VECTOR( veh )
		vPos 		= GET_ENTITY_COORDS( veh, FALSE )
		vPos 		= vPos + ( vForward * -vDimMin.y )
	ENDIF
	RETURN vPos
ENDFUNC

FUNC VECTOR GET_LOCATION_VECTOR( INT iLoc )
	
	VECTOR vLoc

	VECTOR vPlacedGotoPosition = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iLoc ].vloc[ 0 ]
	
	IF IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].vLoc1)
	OR IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].vLoc2)
		
		vLoc = vPlacedGotoPosition
		

			INT iTeam = MC_playerBD[iPartToUse].iteam
			INT iRule = MC_serverBD_4.iCurrentHighestPriority[ iTeam ]

			IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS, ciLoc_BS_ReturnToHeistApartment)
			AND ( AM_I_ON_A_HEIST() OR IS_FAKE_MULTIPLAYER_MODE_SET() )
				
				INT iHeistPropertyID = MC_ServerBD.iHeistPresistantPropertyID
				
				// iHeistPresistantPropertyID won't be set in test mode, setting to a default 
				IF IS_FAKE_MULTIPLAYER_MODE_SET()
					iHeistPropertyID = 1
				ENDIF
				
				IF NOT IS_VECTOR_ZERO( vPlacedGotoPosition )
					IF iHeistPropertyID >= 0
					AND iHeistPropertyID < ( MAX_MP_PROPERTIES + 1 )
						GET_PLAYER_PROPERTY_HEIST_LOCATION( vLoc, 
															MP_PROP_ELEMENT_HEIST_PLAN_LOC,
															iHeistPropertyID )
					ELSE
						IF MC_serverBD_4.iGotoLocationDataPriority[ iLoc ][ iTeam ] <= iRule
							PRINTLN( "[RCC MISSION] iHeistPropertyID = ", g_iHeistPropertyID )
						ENDIF
					ENDIF
				ENDIF

			ENDIF

	ELSE
	    vLoc = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iLoc ].vLoc1 
				+ g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iLoc ].vLoc2
        vLoc *= << 0.5, 0.5, 0.5 >>
	ENDIF
	
	RETURN vLoc
	
ENDFUNC

FUNC VECTOR GET_FMMC_ENTITY_LOCATION(VECTOR vPlayerNearestThisLoc, INT iSpawnNearType = ciRULE_TYPE_NONE, INT iSpawnNearEntityID = -1, ENTITY_INDEX eiPlayerNearestThisEntity = NULL, INT iVeh = -1)
	
	VECTOR vEntityLoc
	BOOL bTargetCheck
	
	NETWORK_INDEX niTarget = NULL
	
	SWITCH iSpawnNearType
		CASE ciRULE_TYPE_PED
			IF iSpawnNearEntityID < FMMC_MAX_PEDS
				niTarget = MC_serverBD_1.sFMMC_SBD.niPed[iSpawnNearEntityID]
				bTargetCheck = TRUE
			ENDIF
		BREAK
		
		CASE ciRULE_TYPE_VEHICLE
			IF iSpawnNearEntityID < FMMC_MAX_VEHICLES
				PRINTLN("[JS] [SPAWNNEAR] - GET_FMMC_ENTITY_LOCATION - 1 VEHICLE")
				niTarget = MC_serverBD_1.sFMMC_SBD.niVehicle[iSpawnNearEntityID]
				bTargetCheck = TRUE
			ENDIF
		BREAK
		
		CASE ciRULE_TYPE_OBJECT
			IF iSpawnNearEntityID < FMMC_MAX_NUM_OBJECTS
				niTarget = MC_serverBD_1.sFMMC_SBD.niObject[iSpawnNearEntityID]
				bTargetCheck = TRUE
			ENDIF
		BREAK
		
		CASE ciRULE_TYPE_GOTO
			IF iSpawnNearEntityID < FMMC_MAX_RULES
				vEntityLoc = GET_LOCATION_VECTOR(iSpawnNearEntityID)
			ENDIF
		BREAK
		
		CASE ciRULE_TYPE_PLAYER
			//Get the player closest to the original spawn position:
			IF iSpawnNearEntityID < MC_serverBD.iNumberOfTeams
			AND (MC_serverBD.iNumberOfPlayingPlayers[iSpawnNearEntityID] > 0)
				INT iClosestPlayer
				PLAYER_INDEX tempPlayer
				
				iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY_ON_TEAM(eiPlayerNearestThisEntity, iSpawnNearEntityID)
				tempPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
				PRINTLN("GET_FMMC_ENTITY_LOCATION - Checking player: ", iClosestPlayer)
				IF (iClosestPlayer != -1 AND iClosestPlayer != iLocalPart)
				AND IS_NET_PLAYER_OK(tempPlayer)
					vEntityLoc = GET_PLAYER_COORDS(tempPlayer)
				#IF IS_DEBUG_BUILD
				ELSE
					IF NOT IS_NET_PLAYER_OK(tempPlayer)
						PRINTLN("GET_FMMC_ENTITY_LOCATION - Player is not OK, going to manual method of finding closest player")
					ENDIF
				#ENDIF
				ENDIF
				
				IF eiPlayerNearestThisEntity = NULL
				OR IS_VECTOR_ZERO(vEntityLoc)
					
					INT iPartLoop
					INT iClosestPart
					iClosestPart = -1
					INT iTeamChecked
					FLOAT fClosestDist2
					fClosestDist2 = 999999999 //Furthest distance apart on map is O(10,000)
					FLOAT fDistance2
					
					FOR iPartLoop = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartLoop))
						AND (MC_playerBD[iPartLoop].iteam = iSpawnNearEntityID)
							iTeamChecked++
							PRINTLN("NEW DEBUG - Checking part ", iPartLoop)
							tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartLoop))
							
							PED_INDEX piEnt
							piEnt = GET_PLAYER_PED(tempPlayer)
							
							IF IS_NET_PLAYER_OK(tempPlayer)
							AND NOT IS_ENTITY_IN_WATER(piEnt)
							AND NOT IS_ENTITY_IN_AIR(piEnt)
							AND tempPlayer != LocalPlayer
								fDistance2 = VDIST2(GET_PLAYER_COORDS(tempPlayer),vPlayerNearestThisLoc)
								PRINTLN("NEW DEBUG - this participant is fine, fDistance2: ", fDistance2)
								IF fDistance2 < fClosestDist2
									fClosestDist2 = fDistance2
									iClosestPart = iPartLoop
									PRINTLN("NEW DEBUG - Updating: ", fClosestDist2, " iClosestPart: ", iClosestPart)
								ENDIF
							ENDIF
						ENDIF
						
						//This is here to stop it from ending the loop prematurely to TPR. Worried about changing this more directly, as function has been around and used for a long time.
						IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TRADING_PLACES_REMIX(g_FMMC_STRUCT.iAdversaryModeType) 
							IF iTeamChecked >= MC_serverBD.iNumberOfPlayingPlayers[iSpawnNearEntityID]
								iPartLoop = MAX_NUM_MC_PLAYERS // Break out!
							ENDIF
						ENDIF
					ENDFOR
					
					IF iClosestPart != -1
						vEntityLoc = GET_PLAYER_COORDS(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iClosestPart)))
					ENDIF
					
				ENDIF
			ENDIF
		BREAK
		CASE ciSPAWN_NEAR_ENTITY_TYPE_LAST_PLAYER
			IF iVeh > -1
				INT i, iPlayersToCheck, iPlayersChecked
				iPlayersToCheck = GET_NUMBER_OF_PLAYING_PLAYERS()
				IF iPlayersToCheck > 1
					FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
						IF (NOT(IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_ANY_SPECTATOR)) OR IS_BIT_SET(MC_playerBD[i].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN))
						AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet,PBBOOL_PLAYER_FAIL)
						AND IS_BIT_SET(MC_playerBD[i].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
						AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_FINISHED)
							PARTICIPANT_INDEX tempPart
							tempPart = INT_TO_PARTICIPANTINDEX(i)
							IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
								iPlayersChecked++
								IF MC_PlayerBD[i].iLastVeh = iVeh
									vEntityLoc = GET_PLAYER_COORDS(NETWORK_GET_PLAYER_INDEX(tempPart))
									PRINTLN("[JS] GET_FMMC_ENTITY_LOCATION - Spawning near part ", i, " vector: ", vEntityLoc)
									BREAKLOOP
								ENDIF
								IF iPlayersChecked >= iPlayersToCheck
									BREAKLOOP
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	IF bTargetCheck
		PRINTLN("[JS] [SPAWNNEAR] - GET_FMMC_ENTITY_LOCATION - 2")
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niTarget)
			vEntityLoc = GET_ENTITY_COORDS(NET_TO_ENT(niTarget), FALSE)
			PRINTLN("[JS] [SPAWNNEAR] - GET_FMMC_ENTITY_LOCATION - 3 vEntityLoc = ", vEntityLoc)
		ENDIF
	ENDIF
	
	PRINTLN("[JS] [SPAWNNEAR] - GET_FMMC_ENTITY_LOCATION - Returning vEntityLoc = ", vEntityLoc)
	
	RETURN vEntityLoc
	
ENDFUNC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: ENTITY RULES !
//
//************************************************************************************************************************************************************



//Gets what rule the entity is on for a certain team - first rule = 0, from any extra objectives = 1 - 5
FUNC INT GET_ENTITY_MULTIRULE_NUMBER(INT iEntity, INT iTeam, INT iTargetType = ci_TARGET_NONE)
	
	INT iMRule = -1
	
	IF iTargetType != ci_TARGET_NONE
	
		INT iCurrentPriority = FMMC_PRIORITY_IGNORE
		INT iPrimaryPriority = FMMC_PRIORITY_IGNORE
		INT iPrimaryRule = FMMC_OBJECTIVE_LOGIC_NONE
		INT iEntityType = ciRULE_TYPE_NONE
		
		SWITCH iTargetType
			CASE ci_TARGET_PED
				iCurrentPriority = MC_serverBD_4.iPedPriority[iEntity][iTeam]
				iEntityType = ciRULE_TYPE_PED
				iPrimaryPriority = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEntity].iPriority[iTeam]
				iPrimaryRule = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEntity].iRule[iTeam]
			BREAK
			
			CASE ci_TARGET_OBJECT
				iCurrentPriority = MC_serverBD_4.iObjPriority[iEntity][iTeam]
				iEntityType = ciRULE_TYPE_OBJECT
				iPrimaryPriority = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntity].iPriority[iTeam]
				iPrimaryRule = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntity].iRule[iTeam]
			BREAK
			
			CASE ci_TARGET_VEHICLE
				iCurrentPriority = MC_serverBD_4.iVehPriority[iEntity][iTeam]
				iEntityType = ciRULE_TYPE_VEHICLE
				iPrimaryPriority = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntity].iPriority[iTeam]
				iPrimaryRule = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntity].iRule[iTeam]
			BREAK
		ENDSWITCH
		
		IF iCurrentPriority < FMMC_MAX_RULES
			IF iPrimaryPriority < FMMC_MAX_RULES
			AND iPrimaryRule != FMMC_OBJECTIVE_LOGIC_NONE
				IF (iPrimaryPriority >= iCurrentPriority)
					iMRule = 0
				ELSE
					
					INT iextraObjectiveNum
					// Loop through all potential entites that have multiple rules
					FOR iextraObjectiveNum = 0 TO ( MAX_NUM_EXTRA_OBJECTIVE_ENTITIES-1 )
						// If the type matches the one passed
						IF g_FMMC_STRUCT.iExtraObjectiveEntityType[iextraObjectiveNum] = iEntityType
						AND g_FMMC_STRUCT.iExtraObjectiveEntityId[iextraObjectiveNum] = iEntity
							INT iextraObjectiveLoop
							
							FOR iextraObjectiveLoop = 0 TO (MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY - 1)
								IF g_FMMC_STRUCT.iExtraObjectivePriority[iextraObjectiveNum][iextraObjectiveLoop][iTeam] < FMMC_MAX_RULES
								AND (GET_FMMC_RULE_FROM_CREATOR_RULE(g_FMMC_STRUCT.iExtraObjectiveRule[iextraObjectiveNum][iextraObjectiveLoop][iTeam]) != FMMC_OBJECTIVE_LOGIC_NONE)
									
									IF (g_FMMC_STRUCT.iExtraObjectivePriority[iextraObjectiveNum][iextraObjectiveLoop][iTeam] >= iCurrentPriority)
										iMRule = iextraObjectiveLoop + 1
										iextraObjectiveLoop = MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY //Break out
									ELSE
										//Keep looking through the loop
									ENDIF
									
								ENDIF
							ENDFOR
							//We found the multi-rule number for this entity, there won't be any more so break out of the loop:
							iextraObjectiveNum = MAX_NUM_EXTRA_OBJECTIVE_ENTITIES
						ENDIF
					ENDFOR
					
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN iMRule
	
ENDFUNC

FUNC INT GET_NEXT_PRIORITY_FOR_ENTITY(INT iObjectiveType, INT iObjectiveID, INT iteam, INT iMinRuleToCheck )
	
	INT iextraObjectiveNum

	// Loop through all potential entites that have multiple rules
	FOR iextraObjectiveNum = 0 TO ( MAX_NUM_EXTRA_OBJECTIVE_ENTITIES-1 )
		// If the type matches the one passed
		IF g_FMMC_STRUCT.iExtraObjectiveEntityType[iextraObjectiveNum]= iObjectiveType
		AND g_FMMC_STRUCT.iExtraObjectiveEntityId[iextraObjectiveNum] = iObjectiveID
	
			// If the current objective passed is less than that of the max per entity
			IF MC_serverBD.iCurrentExtraObjective[iextraObjectiveNum][iteam] < MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY
				
				IF g_FMMC_STRUCT.iExtraObjectivePriority[iextraObjectiveNum][MC_serverBD.iCurrentExtraObjective[iextraObjectiveNum][iteam]][iteam] < FMMC_MAX_RULES
				AND g_FMMC_STRUCT.iExtraObjectivePriority[iextraObjectiveNum][MC_serverBD.iCurrentExtraObjective[iextraObjectiveNum][iteam]][iteam] >= 0 // and it's not -1
					
					INT iNextObjectiveValue = g_FMMC_STRUCT.iExtraObjectivePriority[ iextraObjectiveNum ][ MC_serverBD.iCurrentExtraObjective[ iextraObjectiveNum ][ iteam ] ][ iteam ]
					
					// So if iNextObjectiveValue is less than the total there can be, we've found a valid thang
					IF iNextObjectiveValue > iMinRuleToCheck 
						RETURN iNextObjectiveValue
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
	ENDFOR

	RETURN FMMC_PRIORITY_IGNORE

ENDFUNC

FUNC INT GET_NEXT_RULE_FOR_ENTITY(INT iObjectiveType, INT iObjectiveID,INT iteam,INT iobjectiveToJumpTo)

	INT iextraObjectiveNum
	INT iRule =FMMC_OBJECTIVE_LOGIC_NONE
	
	FOR iextraObjectiveNum = 0 TO (MAX_NUM_EXTRA_OBJECTIVE_ENTITIES-1)
		IF g_FMMC_STRUCT.iExtraObjectiveEntityType[iextraObjectiveNum] = iObjectiveType
		AND g_FMMC_STRUCT.iExtraObjectiveEntityId[iextraObjectiveNum] = iObjectiveID
			IF MC_serverBD.iCurrentExtraObjective[iextraObjectiveNum][iteam] < MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY
				IF g_FMMC_STRUCT.iExtraObjectivePriority[iextraObjectiveNum][MC_serverBD.iCurrentExtraObjective[iextraObjectiveNum][iteam]][iteam] < FMMC_MAX_RULES
				AND g_FMMC_STRUCT.iExtraObjectivePriority[iextraObjectiveNum][MC_serverBD.iCurrentExtraObjective[iextraObjectiveNum][iteam]][iteam] >= 0
					IF g_FMMC_STRUCT.iExtraObjectivePriority[iextraObjectiveNum][MC_serverBD.iCurrentExtraObjective[iextraObjectiveNum][iteam]][iteam] >= iobjectiveToJumpTo
						iRule = g_FMMC_STRUCT.iExtraObjectiveRule[iextraObjectiveNum][MC_serverBD.iCurrentExtraObjective[iextraObjectiveNum][iteam]][iteam]
						iRule = GET_FMMC_RULE_FROM_CREATOR_RULE(iRule)
						RETURN iRule
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR

	RETURN iRule

ENDFUNC

FUNC BOOL IS_ENTITY_INVOLVED_IN_THIS_TEAMS_RULE(INT iRuleType = ciRULE_TYPE_NONE, INT iNumber = -1, INT iTeam = -1, INT iRule = -1)
	
	BOOL bOKData = FALSE
	BOOL bInvolved = FALSE
		
	PRINTLN("[IS_ENTITY_INVOLVED_IN_THIS_TEAMS_RULE] - iRuleType: ", iRuleType, "  iNumber: ", iNumber, " iTeam: ", iTeam, " iRule: ", iRule)
	
	SWITCH iRuleType
		CASE ciRULE_TYPE_PED
			IF iNumber >= 0
			AND iNumber < FMMC_MAX_PEDS
			AND iTeam >= 0
			AND iTeam < FMMC_MAX_TEAMS
			AND iRule >= 0
			AND iRule < FMMC_MAX_RULES				
				PRINTLN("[IS_ENTITY_INVOLVED_IN_THIS_TEAMS_RULE] - pedPriority: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iNumber].iPriority[iTeam])
				bOKData = TRUE
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iNumber].iPriority[iTeam] = iRule
					bInvolved = TRUE
				ENDIF
			ELSE
				//Data is not valid
			ENDIF
		BREAK
		CASE ciRULE_TYPE_VEHICLE
			IF iNumber >= 0
			AND iNumber < FMMC_MAX_VEHICLES
			AND iTeam >= 0
			AND iTeam < FMMC_MAX_TEAMS
			AND iRule >= 0
			AND iRule < FMMC_MAX_RULES
				PRINTLN("[IS_ENTITY_INVOLVED_IN_THIS_TEAMS_RULE] - vehPriority: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iNumber].iPriority[iTeam])
				bOKData = TRUE
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iNumber].iPriority[iTeam] = iRule
					bInvolved = TRUE
				ENDIF
			ELSE
				//Data is not valid
			ENDIF
		BREAK
		CASE ciRULE_TYPE_OBJECT
			IF iNumber >= 0
			AND iNumber < FMMC_MAX_NUM_OBJECTS
			AND iTeam >= 0
			AND iTeam < FMMC_MAX_TEAMS
			AND iRule >= 0
			AND iRule < FMMC_MAX_RULES
				PRINTLN("[IS_ENTITY_INVOLVED_IN_THIS_TEAMS_RULE] - ObjPriority: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iNumber].iPriority[iTeam])
				bOKData = TRUE
				IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iNumber].iPriority[iTeam] = iRule
					bInvolved = TRUE
				ENDIF
			ELSE
				//Data is not valid
			ENDIF
		BREAK
		CASE ciRULE_TYPE_GOTO			
			IF iNumber >= 0
			AND iNumber < FMMC_MAX_GO_TO_LOCATIONS
			AND iTeam >= 0
			AND iTeam < FMMC_MAX_TEAMS
			AND iRule >= 0
			AND iRule < FMMC_MAX_RULES
				PRINTLN("[IS_ENTITY_INVOLVED_IN_THIS_TEAMS_RULE] (Goto) ObjPriority: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iNumber].iPriority[iTeam])
				bOKData = TRUE
				IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iNumber].iPriority[iTeam] = iRule
					bInvolved = TRUE
				ENDIF
			ELSE
				//Data is not valid
			ENDIF
		BREAK
	ENDSWITCH
	
	IF bOKData
	AND NOT bInvolved
		
		INT iextraObjectiveNum
		
		// Loop through all potential entites that have multiple rules
		FOR iextraObjectiveNum = 0 TO ( MAX_NUM_EXTRA_OBJECTIVE_ENTITIES-1 )
			
			// If the type matches the one passed
			IF g_FMMC_STRUCT.iExtraObjectiveEntityType[iextraObjectiveNum] = iRuleType
			AND g_FMMC_STRUCT.iExtraObjectiveEntityId[iextraObjectiveNum] = iNumber
				
				INT iextraObjectiveLoop
				
				FOR iextraObjectiveLoop = 0 TO (MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY - 1)
					
					IF g_FMMC_STRUCT.iExtraObjectivePriority[iextraObjectiveNum][iextraObjectiveLoop][iTeam] = iRule
						bInvolved = TRUE
						//Break out of the loop:
						iextraObjectiveLoop = MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY
					ENDIF
					
				ENDFOR
				
				//We found the multi-rule number for this entity, there won't be any more so break out of the loop:
				iextraObjectiveNum = MAX_NUM_EXTRA_OBJECTIVE_ENTITIES
				
			ENDIF
		ENDFOR
		
	ENDIF
	
	PRINTLN("[IS_ENTITY_INVOLVED_IN_THIS_TEAMS_RULE] - bInvolved: ", bInvolved)
	
	RETURN bInvolved
	
ENDFUNC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: POINTS FOR KILLING ENTITIES !
//
//************************************************************************************************************************************************************



///PURPOSE: this function checks to see if a mission critical ped is moving on
///    to a kill rule, and if so, it sets a bit that stops this team for failing if it's destroyed
PROC CHECK_IF_PED_CAN_BE_KILLED_ON_THIS_RULE( INT iPedIndex, INT iTeam )
	
	IF iPedIndex > -1
		

		PRINTLN("[RCC MISSION] CHECK_IF_PED_CAN_BE_KILLED_ON_THIS_RULE 1 (iPed: ", iPedIndex, " )")
		IF MC_serverBD_4.iPedRule[ iPedIndex ][ iteam ] = FMMC_OBJECTIVE_LOGIC_KILL
			IF iteam = 0
				PRINTLN("[RCC MISSION] CHECK_IF_PED_CAN_BE_KILLED_ON_THIS_RULE 2")
				SET_BIT( MC_serverBD.iPedteamFailBitset[ iPedIndex ], SBBOOL1_TEAM0_NEEDS_KILL )
			ELIF iteam = 1
				SET_BIT( MC_serverBD.iPedteamFailBitset[ iPedIndex ], SBBOOL1_TEAM1_NEEDS_KILL )
			ELIF iteam = 2
				SET_BIT( MC_serverBD.iPedteamFailBitset[ iPedIndex ], SBBOOL1_TEAM2_NEEDS_KILL )
			ELIF iteam = 3
				SET_BIT( MC_serverBD.iPedteamFailBitset[ iPedIndex ], SBBOOL1_TEAM3_NEEDS_KILL )
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

FUNC INT GET_PED_KILLERS_TEAM(PED_INDEX killerPed,INT iPedID)

PLAYER_INDEX tempPlayer
PARTICIPANT_INDEX tempPart
INT ipart

	IF DOES_ENTITY_EXIST(killerPed)
		IF IS_PED_A_PLAYER(killerPed)
			tempPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(killerPed)
			IF NETWORK_IS_PLAYER_A_PARTICIPANT(tempPlayer)
				tempPart = NETWORK_GET_PARTICIPANT_INDEX(tempPlayer)
				ipart = NATIVE_TO_INT(tempPart)
				IF MC_serverBD_4.iPedPriority[iPedID][MC_playerBD[ipart].iteam] = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[ipart].iteam]
					IF MC_serverBD_4.iPedPriority[iPedID][MC_playerBD[ipart].iteam] < FMMC_MAX_RULES
						IF MC_serverBD_4.iPedRule[iPedID][MC_playerBD[ipart].iteam] = FMMC_OBJECTIVE_LOGIC_KILL
							RETURN MC_playerBD[ipart].iteam
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN -1

ENDFUNC

PROC GIVE_TEAM_POINTS_FOR_PED_KILL(INT& iPedID, PED_INDEX Victimped,PED_INDEX killerPed = NULL)

INT iteam
INT iRule
INT ikillerTeam = -1
	
	IF bIsLocalPlayerHost
		
		IF iPedID = -2
			ipedid = IS_ENTITY_A_MISSION_CREATOR_ENTITY(Victimped)
		ENDIF
		
		IF ipedid >= 0
			ikillerTeam = GET_PED_KILLERS_TEAM(killerPed,ipedid)
			FOR iteam = 0 TO (MC_serverBD.iNumActiveTeams-1)
				IF iKillerTeam != iteam
					iRule = MC_serverBD_4.iCurrentHighestPriority[iteam]
					IF iRule < FMMC_MAX_RULES
						IF MC_serverBD_4.iPedPriority[ipedid][iteam] < FMMC_MAX_RULES
							IF MC_serverBD_4.iPedPriority[ipedid][iteam] = iRule
								IF MC_serverBD_4.iPedRule[ipedid][iteam] = FMMC_OBJECTIVE_LOGIC_KILL
									INCREMENT_SERVER_TEAM_SCORE(iteam,MC_serverBD_4.iPedPriority[ipedid][iteam],GET_FMMC_POINTS_FOR_TEAM(iteam,MC_serverBD_4.iPedPriority[ipedid][iteam]))
									PRINTLN("ADDING TO TEAM SCORE for team: ", iteam," PED was killed : ",ipedid)
									BROADCAST_OBJECTIVE_MID_POINT(MC_serverBD_4.iPedPriority[ipedid][iteam],iteam)
									//BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM_SCORE, GET_FMMC_POINTS_FOR_TEAM(iteam,MC_serverBD_4.iPedPriority[ipedid][iteam]),iteam)
								ELSE
									PRINTLN("rule not kill ped for team: ", iteam," PED was killed : ",ipedid)
								ENDIF
							ELSE
								PRINTLN("priority not current ped for team: ", iteam," PED was killed : ",ipedid)
							ENDIF
						ELSE
							PRINTLN("priority ignore ped for team: ", iteam," PED was killed : ",ipedid)
						ENDIF
						
						INT iCustomPoints = g_FMMC_STRUCT_ENTITIES.sPlacedPed[ipedid].iPedPointsToGive
						INT iRuleStart = g_FMMC_STRUCT_ENTITIES.sPlacedPed[ipedid].iPedGivePointsOnRuleStart
						INT iRuleEnd = g_FMMC_STRUCT_ENTITIES.sPlacedPed[ipedid].iPedGivePointsOnRuleEnd
						
						IF iCustomPoints > 0
							PRINTLN("GIVE_TEAM_POINTS_FOR_PED_KILL - iPed: ", iPedID, " iTeam: ", iTeam, " iRule: ", iRule, " iCustomPoints: ", iCustomPoints, " iRuleStart: ", iRuleStart, " iRuleEnd: ", iRuleEnd)
							
							IF iRule >= iRuleStart
							AND (iRule < iRuleEnd OR iRuleEnd = -1)
								INCREMENT_SERVER_TEAM_SCORE(iteam, iRule, iCustomPoints)
								PRINTLN("ADDING TO TEAM SCORE for team: ", iteam," PED was killed : ",ipedid)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ELSE
			PRINTLN("not mission ped for team: ", iteam," PED was killed : ",ipedid)
		ENDIF
	ENDIF


ENDPROC

///PURPOSE: this function checks to see if a mission critical vehicle is moving on
///    to a kill rule, and if so, it sets a bit that stops this team for failing if it's destroyed
PROC CHECK_IF_VEHICLE_CAN_BE_KILLED_ON_THIS_RULE( INT iVehicleIndex, INT iTeam )

	IF iVehicleIndex > -1
		IF MC_serverBD_4.ivehRule[ iVehicleIndex ][ iteam ] = FMMC_OBJECTIVE_LOGIC_KILL
			IF iteam = 0
				SET_BIT( MC_serverBD.iVehteamFailBitset[ iVehicleIndex ], SBBOOL1_TEAM0_NEEDS_KILL )
			ELIF iteam = 1
				SET_BIT( MC_serverBD.iVehteamFailBitset[ iVehicleIndex ], SBBOOL1_TEAM1_NEEDS_KILL )
			ELIF iteam = 2
				SET_BIT( MC_serverBD.iVehteamFailBitset[ iVehicleIndex ], SBBOOL1_TEAM2_NEEDS_KILL )
			ELIF iteam = 3
				SET_BIT( MC_serverBD.iVehteamFailBitset[ iVehicleIndex ], SBBOOL1_TEAM3_NEEDS_KILL )
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC INT GET_VEH_KILLERS_TEAM(PED_INDEX killerPed,INT ivehID)

PLAYER_INDEX tempPlayer
PARTICIPANT_INDEX tempPart
INT ipart

	IF DOES_ENTITY_EXIST(killerPed)
		tempPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(killerPed)
		IF NETWORK_IS_PLAYER_A_PARTICIPANT(tempPlayer)
			tempPart = NETWORK_GET_PARTICIPANT_INDEX(tempPlayer)
			ipart = NATIVE_TO_INT(tempPart)
			IF MC_serverBD_4.iVehPriority[ivehID][MC_playerBD[ipart].iteam] = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[ipart].iteam]
				IF MC_serverBD_4.iVehPriority[ivehID][MC_playerBD[ipart].iteam] < FMMC_MAX_RULES
					IF MC_serverBD_4.ivehRule[ivehID][MC_playerBD[ipart].iteam] = FMMC_OBJECTIVE_LOGIC_KILL
						RETURN MC_playerBD[ipart].iteam
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN -1

ENDFUNC

PROC GIVE_TEAM_POINTS_FOR_VEH_KILL(INT& iVehID, VEHICLE_INDEX Victimveh,PED_INDEX killerPed = NULL)

INT iteam
INT iKillerTeam = -1

	IF bIsLocalPlayerHost
		IF iVehID = -2
			PRINTLN("[RCC MISSION]  - GIVE_TEAM_POINTS_FOR_VEH_KILL - iVehID = -2")
			ivehid = IS_VEH_A_MISSION_CREATOR_VEH(Victimveh)
		ENDIF
		IF ivehid >= 0
			IF killerPed != NULL
				iKillerTeam = GET_VEH_KILLERS_TEAM(killerPed,ivehid)
			ENDIF
			IF NOT IS_BIT_SET(MC_serverBD_2.iVehIsDestroyed, iVehID)
				FOR iteam = 0 TO (MC_serverBD.iNumActiveTeams-1)
					IF iKillerTeam != iteam
						IF MC_serverBD_4.iVehPriority[ivehid][iteam] < FMMC_MAX_RULES
							IF MC_serverBD_4.iVehPriority[ivehid][iteam] <= MC_serverBD_4.iCurrentHighestPriority[iteam]
								IF MC_serverBD_4.ivehRule[ivehid][iteam] = FMMC_OBJECTIVE_LOGIC_KILL
									INCREMENT_SERVER_TEAM_SCORE(iteam,MC_serverBD_4.iVehPriority[ivehid][iteam],GET_FMMC_POINTS_FOR_TEAM(iteam,MC_serverBD_4.iVehPriority[ivehid][iteam]))
									PRINTLN("ADDING TO TEAM SCORE for team: ", iteam," VEH was killed : ",ivehid)
									BROADCAST_OBJECTIVE_MID_POINT(MC_serverBD_4.iVehPriority[ivehid][iteam],iteam)
									IF NOT IS_BIT_SET(MC_serverBD_2.iVehIsDestroyed, iVehID)
										SET_BIT(MC_serverBD_2.iVehIsDestroyed, iVehID)
									ENDIF
									//BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM_SCORE, GET_FMMC_POINTS_FOR_TEAM(iteam,MC_serverBD_4.iVehPriority[ivehid][iteam]),iteam)
								ELSE
									PRINTLN("rule not kill veh for team: ", iteam," veh was killed : ",ivehid)
								ENDIF
							ELSE
								PRINTLN("priority not current veh for team: ", iteam," veh was killed : ",ivehid)
								PRINTLN("priority: ", MC_serverBD_4.iCurrentHighestPriority[iteam]," veh priority : ",MC_serverBD_4.iVehPriority[ivehid][iteam])
								
							ENDIF
						ELSE
							PRINTLN("priority ignore veh for team: ", iteam," veh was killed : ",ivehid)
						ENDIF
					ELSE
						PRINTLN("iKillerTeam = iteam: ", iteam," veh was killed : ",ivehid)
					ENDIF
				ENDFOR
			ENDIF
		ELSE
			PRINTLN("Invalid vehicle id - ivehid =-1 for veh for team: ", iteam," veh was killed : ",ivehid)
		ENDIF
	ENDIF


ENDPROC

///PURPOSE: this function checks to see if a mission critical ped is moving on
///    to a kill rule, and if so, it sets a bit that stops this team for failing if it's destroyed
PROC CHECK_IF_OBJECT_CAN_BE_KILLED_ON_THIS_RULE( INT iObjectIndex, INT iTeam )
	
	IF iObjectIndex > -1
		IF MC_serverBD_4.iObjRule[ iObjectIndex ][ iTeam ] = FMMC_OBJECTIVE_LOGIC_KILL
			IF iteam = 0
				SET_BIT(MC_serverBD.iObjteamFailBitset[ iObjectIndex ],SBBOOL1_TEAM0_NEEDS_KILL)
			ELIF iteam = 1
				SET_BIT(MC_serverBD.iObjteamFailBitset[ iObjectIndex ],SBBOOL1_TEAM1_NEEDS_KILL)
			ELIF iteam = 2
				SET_BIT(MC_serverBD.iObjteamFailBitset[ iObjectIndex ],SBBOOL1_TEAM2_NEEDS_KILL)
			ELIF iteam = 3
				SET_BIT(MC_serverBD.iObjteamFailBitset[ iObjectIndex ],SBBOOL1_TEAM3_NEEDS_KILL)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_OBJ_KILLERS_TEAM(PED_INDEX killerPed,INT iObjID)
	
	PLAYER_INDEX tempPlayer
	PARTICIPANT_INDEX tempPart
	INT ipart
	
	PRINTLN("[RCC MISSION] GET_OBJ_KILLERS_TEAM - Called with iObjID ",iObjID)
	
	IF DOES_ENTITY_EXIST(killerPed)
		tempPlayer =NETWORK_GET_PLAYER_INDEX_FROM_PED(killerPed)
		IF NETWORK_IS_PLAYER_A_PARTICIPANT(tempPlayer)
			tempPart = NETWORK_GET_PARTICIPANT_INDEX(tempPlayer)
			ipart =NATIVE_TO_INT(tempPart)
			IF MC_serverBD_4.iObjPriority[iObjID][MC_playerBD[ipart].iteam] = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[ipart].iteam]
				IF MC_serverBD_4.iObjPriority[iObjID][MC_playerBD[ipart].iteam] < FMMC_MAX_RULES
					IF MC_serverBD_4.iObjRule[iObjID][MC_playerBD[ipart].iteam] = FMMC_OBJECTIVE_LOGIC_KILL
						
						PRINTLN("[RCC MISSION] GET_OBJ_KILLERS_TEAM - killer part ",ipart," on team ",MC_playerBD[ipart].iteam," has a kill rule, RETURN team ",MC_playerBD[ipart].iteam)
						RETURN MC_playerBD[ipart].iteam
						
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("[RCC MISSION] GET_OBJ_KILLERS_TEAM - killer part ",ipart," on team ",MC_playerBD[ipart].iteam," obj rule ",MC_serverBD_4.iObjRule[iObjID][MC_playerBD[ipart].iteam]," is not a kill rule")
					#ENDIF
					ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[RCC MISSION] GET_OBJ_KILLERS_TEAM - killer part ",ipart," on team ",MC_playerBD[ipart].iteam," obj priority > FMMC_MAX_RULES")
				#ENDIF
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[RCC MISSION] GET_OBJ_KILLERS_TEAM - killer part ",ipart," on team ",MC_playerBD[ipart].iteam," obj priority ",MC_serverBD_4.iObjPriority[iObjID][MC_playerBD[ipart].iteam]," is not current")
			#ENDIF
			ENDIF
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[RCC MISSION] GET_OBJ_KILLERS_TEAM - killerPed's player index is not a participant, player ",NATIVE_TO_INT(tempPlayer))
		#ENDIF
		ENDIF
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[RCC MISSION] GET_OBJ_KILLERS_TEAM - No killerPed")
	#ENDIF
	ENDIF
	
	RETURN -1

ENDFUNC

PROC GIVE_TEAM_POINTS_FOR_OBJ_KILL(INT& iObjID, OBJECT_INDEX VictimObj,PED_INDEX killerPed = NULL)

INT iteam
INT iKillerTeam = -1
	IF bIsLocalPlayerHost
		IF iobjID = -2
			iobjid = IS_OBJ_A_MISSION_CREATOR_OBJ(VictimObj)
		ENDIF
		
		IF iobjid >= 0
			iKillerTeam = GET_OBJ_KILLERS_TEAM(killerPed,iobjid)
			PRINTLN("[RCC MISSION] GIVE_TEAM_POINTS_FOR_OBJ_KILL - Object ",iObjID," has iKillerTeam ",iKillerTeam)
			FOR iteam = 0 TO (MC_serverBD.iNumActiveTeams-1)
				IF iKillerTeam != iteam
					PRINTLN("[RCC MISSION] GIVE_TEAM_POINTS_FOR_OBJ_KILL - Object ",iObjID," has priority ",MC_serverBD_4.iObjPriority[iobjid][iteam]," & rule ",MC_serverBD_4.iObjRule[iobjid][iteam]," for team ",iteam)
					IF MC_serverBD_4.iObjPriority[iobjid][iteam] < FMMC_MAX_RULES
						IF MC_serverBD_4.iObjPriority[iobjid][iteam] = MC_serverBD_4.iCurrentHighestPriority[iteam]
							IF MC_serverBD_4.iObjRule[iobjid][iteam] = FMMC_OBJECTIVE_LOGIC_KILL
								INCREMENT_SERVER_TEAM_SCORE(iteam,MC_serverBD_4.iObjPriority[iobjid][iteam],GET_FMMC_POINTS_FOR_TEAM(iteam,MC_serverBD_4.iObjPriority[iobjid][iteam]))
								PRINTLN("[RCC MISSION] GIVE_TEAM_POINTS_FOR_OBJ_KILL - ADDING TO TEAM SCORE for team: ", iteam," OBJ was killed : ",iobjid)
								BROADCAST_OBJECTIVE_MID_POINT(MC_serverBD_4.iObjPriority[iobjid][iteam],iteam)
								//BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM_SCORE, GET_FMMC_POINTS_FOR_TEAM(iteam,MC_serverBD_4.iObjPriority[iobjid][iteam]),iteam)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[RCC MISSION] GIVE_TEAM_POINTS_FOR_OBJ_KILL - Object is not a mission creator object")
		#ENDIF
		ENDIF
	ENDIF

ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: ENTITIES IN DROP OFF !
//
//************************************************************************************************************************************************************



//TODO(Owain): Tidy this the fuck up
FUNC VECTOR GET_PROPERTY_DROP_OFF_COORDS( ePropertyDropoffType eDropoffType, BOOL bReturnDataOnly = FALSE )
	
	VECTOR vLoc
	
	INT iTeam = MC_PlayerBD[ iPartToUse ].iteam
	UNUSED_PARAMETER( bReturnDataOnly ) //for release

	
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[ iTeam ]

		IF IS_VECTOR_ZERO( vPropertyDropoff )
		OR bReturnDataOnly	
			
			INT iPropertyIndex = -1
				
			IF IS_FAKE_MULTIPLAYER_MODE_SET()
				iPropertyIndex = 1
			ELSE
				iPropertyIndex = MC_ServerBD.iHeistPresistantPropertyID
			ENDIF
			
			IF iPropertyIndex >= 0
			AND iPropertyIndex < ( MAX_MP_PROPERTIES + 1 )
				
				SWITCH( eDropoffType )
					CASE eApartmentBuzzerEntrance
						vLoc = GET_HEIST_HIGH_END_PROPERTY_ENTRANCE_LOCATE_POSITION(iPropertyIndex)
						
						IF NOT IS_VECTOR_ZERO( vLoc )
							IF NOT bReturnDataOnly
								vPropertyDropoff = vLoc
								PRINTLN("[RCC MISSION] GET_PROPERTY_DROP_OFF_COORDS - Planning board drop off location successfully found!: ",vPropertyDropoff)
							ELSE
								PRINTLN("[RCC MISSION] GET_PROPERTY_DROP_OFF_COORDS - not setting drop off coords 1")
								DEBUG_PRINTCALLSTACK()
							ENDIF
						ENDIF
						
					BREAK
					
					CASE eGarageExterior
						vLoc = GET_HEIST_PROPERTY_GARAGE_ENTRANCE_LOCATE_POSITION( iPropertyIndex )
						
						IF NOT IS_VECTOR_ZERO( vLoc )
							IF NOT bReturnDataOnly
								vPropertyDropoff = vLoc
								PRINTLN("[RCC MISSION] GET_PROPERTY_DROP_OFF_COORDS - Garage drop off location successfully found!: ",vPropertyDropoff)
							ELSE
								PRINTLN("[RCC MISSION] GET_PROPERTY_DROP_OFF_COORDS - not setting drop off coords 2")
							ENDIF
						ENDIF
						
					BREAK
				ENDSWITCH
				
			ELSE
				PRINTLN("[RCC MISSION] GET_PROPERTY_DROP_OFF_COORDS - Invalid heist property index returned")
			ENDIF
			
			IF IS_VECTOR_ZERO( vLoc )
				IF MC_serverBD_4.iCurrentHighestPriority[ iTeam ] < FMMC_MAX_RULES
					vLoc = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].vDropOff[ iRule ]
					IF NOT bReturnDataOnly
						vPropertyDropoff = vLoc
						PRINTLN("[RCC MISSION] GET_PROPERTY_DROP_OFF_COORDS - Vector is zero - fallback: ",vPropertyDropoff)
					ELSE
						PRINTLN("[RCC MISSION] GET_PROPERTY_DROP_OFF_COORDS - not setting drop off coords 3")
					ENDIF
				ENDIF
			ENDIF
			
		ELSE
			vLoc = vPropertyDropoff
		ENDIF
		
	
	
	UNUSED_PARAMETER( eDropoffType )
	
	RETURN vLoc
	
ENDFUNC

FUNC BOOL IS_ENTITY_IN_DROP_OFF_AREA(ENTITY_INDEX tempEnt, INT iteam, INT ipriority, INT iEntityType = ciRULE_TYPE_NONE, INT iEntityID = -1)

	BOOL bEntityIsInArea = FALSE
	
	FLOAT fradius = g_FMMC_STRUCT.sFMMCEndConditions[iteam].fDropOffRadius[ipriority]
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[iteam].iDropOffType[ipriority] = ciFMMC_DROP_OFF_TYPE_CYLINDER
		VECTOR vDropOff = g_FMMC_STRUCT.sFMMCEndConditions[iteam].vDropOff[ipriority]
		
		SWITCH iEntityType
			CASE ciRULE_TYPE_VEHICLE
				IF iEntityID >= 0
				AND iEntityID < FMMC_MAX_VEHICLES
					IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntityID].vDropOffOverride)
						vDropOff = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntityID].vDropOffOverride
						IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntityID].fVehDropOffRadius > 0
							fradius = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntityID].fVehDropOffRadius
						ENDIF
					ELSE
						IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[iteam].vDropOff2[ipriority])
							IF iEntityID != iFirstHighPriorityVehicleThisRule[iteam]
								vDropOff = g_FMMC_STRUCT.sFMMCEndConditions[iteam].vDropOff2[ipriority]
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iRuleBitsetThirteen[ipriority], ciBS_RULE13_CHECK_CYLINDER)
			IF IS_ENTITY_AT_COORD(tempEnt, vDropOff, <<fradius,fradius,fradius>>, FALSE)	
				bEntityIsInArea = TRUE
			ENDIF
		ELSE
			VECTOR vEntityCoord = GET_ENTITY_COORDS(tempEnt)
			IF IS_POINT_IN_CYLINDER(vEntityCoord, vDropOff, fradius, g_FMMC_STRUCT.sFMMCEndConditions[iteam].fDropOffHeight[ipriority], FALSE)
				bEntityIsInArea = TRUE
			ENDIF 
		ENDIF
		
	ELIF g_FMMC_STRUCT.sFMMCEndConditions[iteam].iDropOffType[ipriority] = ciFMMC_DROP_OFF_TYPE_PROPERTY
		IF DOES_ENTITY_EXIST(tempEnt)
			IF IS_ENTITY_AT_COORD(tempEnt,GET_PROPERTY_DROP_OFF_COORDS( eApartmentBuzzerEntrance,TRUE),<<fradius,fradius,fradius>>,FALSE)	
				bEntityIsInArea = TRUE
			ENDIF
		ELSE
			PRINTLN("IS_ENTITY_IN_DROP_OFF_AREA - tempEnt doesn't exist")
		ENDIF
	ELIF g_FMMC_STRUCT.sFMMCEndConditions[iteam].iDropOffType[ipriority] = ciFMMC_DROP_OFF_TYPE_AREA
		IF DOES_ENTITY_EXIST(tempEnt)
			IF IS_ENTITY_IN_ANGLED_AREA(tempEnt,g_FMMC_STRUCT.sFMMCEndConditions[iteam].vDropOff[ipriority],g_FMMC_STRUCT.sFMMCEndConditions[iteam].vDropOff2[ipriority],fradius,FALSE)
				bEntityIsInArea = TRUE
			ENDIF
		ELSE
			PRINTLN("IS_ENTITY_IN_DROP_OFF_AREA - tempEnt doesn't exist")
		ENDIF
	ELIF g_FMMC_STRUCT.sFMMCEndConditions[iteam].iDropOffType[ipriority] = ciFMMC_DROP_OFF_TYPE_SPHERE
		IF VDIST2(GET_ENTITY_COORDS(tempEnt), g_FMMC_STRUCT.sFMMCEndConditions[iteam].vDropOff[ipriority]) < POW(fRadius,2)
			bEntityIsInArea = TRUE
		ENDIF
	ELSE //Delivering to a vehicle
		IF ipriority < FMMC_MAX_RULES
			IF (g_FMMC_STRUCT.sFMMCEndConditions[iteam].iDropOff_Vehicle[ipriority] > -1)
				
				NETWORK_INDEX niVeh = MC_serverBD_1.sFMMC_SBD.niVehicle[ g_FMMC_STRUCT.sFMMCEndConditions[iteam].iDropOff_Vehicle[ipriority] ]
				
				IF NETWORK_DOES_NETWORK_ID_EXIST(niVeh)
					
					VEHICLE_INDEX tempVeh = NET_TO_VEH( niVeh )
					
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iVehDropOff_GetInBS,ipriority)
						IF GET_DISTANCE_BETWEEN_ENTITIES(tempEnt, tempVeh) <= fradius
							IF IS_ENTITY_AT_COORD(tempEnt,GET_ENTITY_COORDS(tempVeh),<<fradius,fradius,fradius>>,FALSE)	
								bEntityIsInArea = TRUE
							ENDIF
						ENDIF
					ELSE //Need to get in to deliver stuff
						IF IS_VEHICLE_DRIVEABLE(tempVeh)
							IF IS_ENTITY_A_PED(tempEnt)
								IF IS_PED_IN_VEHICLE(GET_PED_INDEX_FROM_ENTITY_INDEX(tempEnt),tempVeh)
									bEntityIsInArea = TRUE
								ENDIF
							ELIF IS_ENTITY_AN_OBJECT(tempEnt)
								ENTITY_INDEX tempCarrier = GET_ENTITY_ATTACHED_TO(tempEnt)
								IF DOES_ENTITY_EXIST(tempCarrier)
									IF IS_ENTITY_A_PED(tempCarrier)
										IF IS_PED_IN_VEHICLE(GET_PED_INDEX_FROM_ENTITY_INDEX(tempCarrier),tempVeh)
											bEntityIsInArea = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bEntityIsInArea
	
ENDFUNC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: GENERIC BLIP STUFF !
//
//************************************************************************************************************************************************************



PROC SET_BLIP_TEAM_SECONDARY_COLOUR(BLIP_INDEX tempBlip,BOOL bSet,INT iteam)
	
	HUD_COLOURS teamcolour
	INT R,G,B,A
	
	IF bSet
		
		teamcolour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iteam, PlayerToUse)
		
		GET_HUD_COLOUR(teamcolour,R,G,B,A)
		APPLY_SECONDARY_COLOUR_TO_BLIP(tempBlip,R,G,B)
	ELSE
		CLEAR_SECONDARY_COLOUR_FROM_BLIP(tempBlip)
	ENDIF

ENDPROC

PROC INITIALISE_CACHED_BLIP_NAME_INDEXES()
	CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][CACHE] Initialising cached blip name indexes to starting values.")

	INT index
	REPEAT FMMC_MAX_PEDS index
		iPedBlipCachedNamePriority[index] = -1
	ENDREPEAT
	REPEAT FMMC_MAX_VEHICLES index
		iVehBlipCachedNamePriority[index] = -1
	ENDREPEAT
	REPEAT FMMC_MAX_NUM_OBJECTS index
		iObjBlipCachedNamePriority[index] = -1
	ENDREPEAT
ENDPROC

FUNC TEXT_LABEL_23 GET_BLIP_LITERAL_STRING_NAME(INT iteam, INT ipriority, INT &icached_priority)
	TEXT_LABEL_23 tl23LiteralStringName = ""

	//First check the passed priority for a name.
	IF ipriority < FMMC_MAX_RULES
		IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[iteam].tl23ObjBlip[ipriority])
		AND NOT g_FMMC_STRUCT.bPlayerLangNotEqualCreated
			//Set the current priority name.
			tl23LiteralStringName = g_FMMC_STRUCT.sFMMCEndConditions[iteam].tl23ObjBlip[ipriority]
			
			//Cache the priority if it doesn't already match.
			IF icached_priority != ipriority
				CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][CACHE] Caching blip name priority for team ", iteam, " at priority ", ipriority, ".")
				CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][CACHE] Caching blip priority as ", ipriority, ".")
			
				icached_priority = ipriority
			ENDIF
		ENDIF
	ENDIF
	
	//If we haven't got a name yet check to see if we have a cached priority we can use.
	//This means entities will retain their blip names across rules.
	IF IS_STRING_NULL_OR_EMPTY(tl23LiteralStringName)
		IF icached_priority != -1 AND icached_priority < FMMC_MAX_RULES
			IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[iteam].tl23ObjBlip[icached_priority])
			AND NOT g_FMMC_STRUCT.bPlayerLangNotEqualCreated
				//Set the cached blip name.
				tl23LiteralStringName = g_FMMC_STRUCT.sFMMCEndConditions[iteam].tl23ObjBlip[icached_priority]
			ENDIF
		ENDIF
	ENDIF
	
	RETURN tl23LiteralStringName
ENDFUNC

FUNC BOOL SET_BLIP_CUSTOM_NAME(BLIP_INDEX temp_blip,INT iteam, INT ipriority, INT &icached_priority)

	TEXT_LABEL_23 tl23LiteralStringName = GET_BLIP_LITERAL_STRING_NAME(iteam, ipriority, icached_priority)

	IF NOT IS_STRING_NULL_OR_EMPTY(tl23LiteralStringName)
		CDEBUG2LN(DEBUG_MC_BLIP, "[RCC MISSION][CACHE] Setting cutstom blip name to ", tl23LiteralStringName, ".")
		
		BEGIN_TEXT_COMMAND_SET_BLIP_NAME("MCUSTBLIP")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tl23LiteralStringName)
		END_TEXT_COMMAND_SET_BLIP_NAME(temp_blip)
		RETURN TRUE
	ELSE
		IF IS_BIT_SET( iLocalBoolCheck3, LBOOL3_CTF_OBJECT_IS_BAG )
			SET_BLIP_NAME_FROM_TEXT_FILE( temp_blip,"CTF_BBLIP")
		ELIF IS_BIT_SET( iLocalBoolCheck3, LBOOL3_CTF_OBJECT_IS_CASE )
			SET_BLIP_NAME_FROM_TEXT_FILE( temp_blip,"CTF_CBLIP")
		ELIF IS_BIT_SET( iLocalBoolCheck5, LBOOL5_CTF_OBJECT_IS_FLAG )
			SET_BLIP_NAME_FROM_TEXT_FILE( temp_blip, "CTF_DBLIP" )
		ELIF IS_BIT_SET( iLocalBoolCheck5, LBOOL5_CTF_OBJECT_IS_TARGET )
			SET_BLIP_NAME_FROM_TEXT_FILE( temp_blip, "CTF_DBLIP" )
		ELIF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_CTF_OBJECT_IS_DRUGS)
			SET_BLIP_NAME_FROM_TEXT_FILE( temp_blip, "CTF_DRBLIP" )
		ELIF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_CTF_OBJECT_IS_LAPTOP)
			SET_BLIP_NAME_FROM_TEXT_FILE( temp_blip, "CTF_LBLIP" )
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC REMOVE_ANY_EXISTING_BLIP_FROM_ENTITY(ENTITY_INDEX EntityID)
	BLIP_INDEX TempBlip = GET_BLIP_FROM_ENTITY(EntityID)
	IF DOES_BLIP_EXIST(TempBlip)
		CDEBUG2LN(DEBUG_MC_BLIP, "[RCC MISSION] Removing any existing blip from entity ", NATIVE_TO_INT(EntityID), ". Blip was found and removed.")
		REMOVE_BLIP(TempBlip)
	ENDIF
ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: PHOTO RULE PROCESSING !
//
//************************************************************************************************************************************************************



PROC INITIALISE_PHOTO_TRACKED_POINT(INT &iTrackedPointID)
	iTrackedPointID = -1
ENDPROC

PROC CREATE_PHOTO_TRACKED_POINT_FOR_COORD(INT &iTrackedPointID, VECTOR vPosition, FLOAT fRadius)

	IF ( iTrackedPointID = -1  )
		iTrackedPointID = CREATE_TRACKED_POINT()
		SET_TRACKED_POINT_INFO(iTrackedPointID, vPosition, fRadius)
		PRINTLN("[RCC MISSION] Created tracked point with id: ", iTrackedPointID, " for coord ", vPosition, " and radius ", fRadius, ".")
	ENDIF

ENDPROC

PROC CLEANUP_PHOTO_TRACKED_POINT(INT &iTrackedPointID)

	IF ( iTrackedPointID != -1  )
		DESTROY_TRACKED_POINT(iTrackedPointID)
		PRINTLN("Destroyed tracked point with id: ", iTrackedPointID, ".")
		iTrackedPointID = -1
	ENDIF

ENDPROC

FUNC BOOL DOES_PHOTO_TRACKED_POINT_EXIST(INT &iTrackedPointID)
	RETURN iTrackedPointID != -1
ENDFUNC 

FUNC BOOL IS_PHOTO_TRACKED_POINT_VISIBLE(INT &iTrackedPointID)
	RETURN IS_TRACKED_POINT_VISIBLE(iTrackedPointID)
ENDFUNC

PROC RESET_PHOTO_DATA(INT iveh = -1)
	IF iveh = -1
		CLEANUP_PHOTO_TRACKED_POINT(iPhotoTrackedPoint)
	ELSE
		CLEANUP_PHOTO_TRACKED_POINT(iVehPhotoTrackedPoint[iveh])
		CLEANUP_PHOTO_TRACKED_POINT(iVehPhotoTrackedPoint2[iveh])
	ENDIF
ENDPROC

FUNC BOOL IS_POINT_IN_CELLPHONE_CAMERA_VIEW(VECTOR vPoint, FLOAT fRadius)

	FLOAT fXPosition, fYPosition

	IF IS_CELLPHONE_CAMERA_IN_USE()
		IF NOT IS_SNAPMATIC_NOT_IN_VIEWFINDER_MODE()
			IF IS_SPHERE_VISIBLE(vPoint, fRadius)
				IF GET_SCREEN_COORD_FROM_WORLD_COORD(vPoint, fXPosition, fYPosition)
					IF 	(fXPosition > 0.15) AND (fXPosition < 0.85)
					AND (fYPosition > 0.10) AND (fYPosition < 0.90)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF	

	RETURN FALSE

ENDFUNC

FUNC BOOL SHOULD_PUT_AWAY_PHONE_AFTER_PHOTO(INT iPriority, INT iEntityType = ci_TARGET_NONE)
	
	BOOL bPutAway = TRUE
	INT iTeam = MC_playerBD[iPartToUse].iteam
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iPriority], ciBS_RULE4_PHOTO_DOESNT_CLOSE_PHONE)
		
		bPutAway = FALSE
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iPriority], ciBS_RULE4_STILL_CLOSE_PHONE_ON_LAST_PHOTO)
			
			INT iLeft // Number of entities left on the rule
			
			SWITCH iEntityType
				CASE ci_TARGET_LOCATION
					iLeft = MC_serverBD.iNumLocHighestPriority[iTeam] - 1
				BREAK
				CASE ci_TARGET_OBJECT
					iLeft = MC_serverBD.iNumObjHighestPriority[iTeam] - 1
				BREAK
				CASE ci_TARGET_PED
					iLeft = MC_serverBD.iNumPedHighestPriority[iTeam] - 1
				BREAK
				CASE ci_TARGET_VEHICLE
					iLeft = MC_serverBD.iNumVehHighestPriority[iTeam] - 1
				BREAK
			ENDSWITCH
			
			IF iLeft <= 0
				bPutAway = TRUE
			ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTargetScore[iPriority] != FMMC_TARGET_SCORE_OFF
				//Check if the score will pass this rule:
				INT iNewScore = MC_serverBD.iScoreOnThisRule[iTeam] + GET_FMMC_POINTS_FOR_TEAM(iTeam, iPriority)
				INT iTarget
				
				IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTargetScore[iPriority] = FMMC_TARGET_SCORE_PLAYER
					iTarget = MC_serverBD.iNumberOfPlayingPlayers[iTeam]
				ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTargetScore[iPriority] = FMMC_TARGET_SCORE_PLAYER_HALF
					iTarget = IMAX(FLOOR(MC_serverBD.iNumberOfPlayingPlayers[iTeam] / 2.0), 1)
				ELSE
					iTarget = ROUND(GET_FMMC_OBJECTIVE_SCORE_LIMIT_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTargetScore[iPriority], iTeam)*GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING(iTeam, iPriority))
				ENDIF
				
				IF iNewScore >= iTarget
					bPutAway = TRUE
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
	
	RETURN bPutAway
	
ENDFUNC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: PED DIRTY FLAG / RETASK COMMANDS !
//
//************************************************************************************************************************************************************



PROC SET_PED_RETASK_DIRTY_FLAG(INT iPed)
	
	#IF IS_DEBUG_BUILD
	IF iPed < 0
		SCRIPT_ASSERT("[RCC MISSION] - [DIRTY FLAG] - SET_PED_RETASK_DIRTY_FLAG has been called with iPed < 0. Please pass a valid ped identifier.")
	ENDIF
	#ENDIF
	
	INT iLongIndex = GET_LONG_BITSET_INDEX(iPed)
	INT iLongBit = GET_LONG_BITSET_BIT(iPed)
	
	IF NOT IS_BIT_SET(sRetaskDirtyFlagData.iDirtyFlagPedNeedsRetask[iLongIndex], iLongBit)
		PRINTLN("[RCC MISSION] - [DIRTY FLAG] - iDirtyFlagPedNeedsRetask[", iLongIndex, "], bit ", iLongBit, " is not set. Setting dirty flag.")
		SET_BIT(sRetaskDirtyFlagData.iDirtyFlagPedNeedsRetask[iLongIndex], iLongBit)
		RESET_NET_TIMER(maxAccelerationUpdateTimers[iPed])
		
		IF (NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iped]))
		AND (NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iped]))
			BROADCAST_FMMC_PED_DIRTY_FLAG_SET(iped)
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL PED_RETASK_NEEDS_HANDSHAKE(PED_INDEX tempPed, INT iPed)
	IF IS_PED_IN_ANY_HELI(tempPed)
	AND MC_serverBD_2.iPedState[iped] = ciTASK_GOTO_COORDS
		// url:bugstar:3585281
		PRINTLN("[RCC MISSION] - [DIRTY FLAG] - iPed", iPed, " is in a heli mission task, needs handshake")
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC
PROC CLEAR_RETASK_DIRTY_FLAG(PED_INDEX tempPed, INT iPed)
	
	#IF IS_DEBUG_BUILD
	IF iPed < 0
		SCRIPT_ASSERT("[RCC MISSION] - [DIRTY FLAG] - CLEAR_RETASK_DIRTY_FLAG has been called with iPed < 0. Please pass a valid ped identifier.")
	ENDIF
	#ENDIF
	
	INT iLongIndex = GET_LONG_BITSET_INDEX(iPed)
	INT iLongBit = GET_LONG_BITSET_BIT(iPed)
	
	IF IS_BIT_SET(sRetaskDirtyFlagData.iDirtyFlagPedNeedsRetask[iLongIndex], iLongBit)
		IF PED_RETASK_NEEDS_HANDSHAKE(tempPed, iPed)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iped])
				BROADCAST_FMMC_PED_RETASK_HANDSHAKE_EVENT(iPed)
			ELSE
				IF NOT IS_BIT_SET(sRetaskDirtyFlagData.iDirtyFlagPedRetaskHandshake[iLongIndex], iLongBit)
					PRINTLN("[RCC MISSION] - [DIRTY FLAG] - iDirtyFlagPedNeedsRetask[", iLongIndex, "], bit ", iLongBit, " is set, but iDirtyFlagPedRetaskHandshake is not! Waiting for handshake before clearing dirty flag.")
					DEBUG_PRINTCALLSTACK()
					EXIT
				ENDIF
			ENDIF
		ENDIF
		PRINTLN("[RCC MISSION] - [DIRTY FLAG] - iDirtyFlagPedNeedsRetask[", iLongIndex, "], bit ", iLongBit, " is set. Clearing dirty flag.")
		DEBUG_PRINTCALLSTACK()
		CLEAR_BIT(sRetaskDirtyFlagData.iDirtyFlagPedNeedsRetask[iLongIndex], iLongBit)
		CLEAR_BIT(sRetaskDirtyFlagData.iDirtyFlagPedRetaskHandshake[iLongIndex], iLongBit)
	ENDIF
	
ENDPROC

PROC MAINTAIN_RETASK_DIRTY_FLAG_DATA_COMPARISONS(INT iPed)
	
	// Retask if ped gets spooked.
	IF IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed)) 
		IF NOT IS_BIT_SET(sRetaskDirtyFlagData.iPedSpookedBitsetCopy[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			PRINTLN("[RCC MISSION] - [DIRTY FLAG] - ped ", iPed, " has been spooked, setting retask dirty flag.")
			
			SET_PED_RETASK_DIRTY_FLAG(iPed)
			SET_BIT(sRetaskDirtyFlagData.iPedSpookedBitsetCopy[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		ENDIF
	ELSE
		IF IS_BIT_SET(sRetaskDirtyFlagData.iPedSpookedBitsetCopy[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			PRINTLN("[RCC MISSION] - [DIRTY FLAG] - ped ", iPed, " has been unspooked, setting retask dirty flag.")
			
			SET_PED_RETASK_DIRTY_FLAG(iPed)
			CLEAR_BIT(sRetaskDirtyFlagData.iPedSpookedBitsetCopy[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		ENDIF
	ENDIF
	
	// Retask if ped gets aggroed.
	IF IS_BIT_SET(MC_serverBD.iPedAggroedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		IF NOT IS_BIT_SET(sRetaskDirtyFlagData.iPedAggroedBitsetCopy[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			PRINTLN("[RCC MISSION] - [DIRTY FLAG] - ped ", iPed, " has been aggroed, setting retask dirty flag.")
			
			SET_PED_RETASK_DIRTY_FLAG(iPed)
			SET_BIT(sRetaskDirtyFlagData.iPedAggroedBitsetCopy[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		ENDIF
	ELSE
		IF IS_BIT_SET(sRetaskDirtyFlagData.iPedAggroedBitsetCopy[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			PRINTLN("[RCC MISSION] - [DIRTY FLAG] - ped ", iPed, " has been un-aggroed, setting retask dirty flag.")
			
			SET_PED_RETASK_DIRTY_FLAG(iPed)
			CLEAR_BIT(sRetaskDirtyFlagData.iPedAggroedBitsetCopy[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		ENDIF
	ENDIF
	
	
	// Retask if ped changes go to progress.
	IF sRetaskDirtyFlagData.iPedGotoProgressCopy[iPed] != MC_serverBD_2.iPedGotoProgress[iped]
		
		PRINTLN("[RCC MISSION] - [DIRTY FLAG] - ped ", iPed, " has changed goto progress, setting retask dirty flag.")
		
		SET_PED_RETASK_DIRTY_FLAG(iPed)
		sRetaskDirtyFlagData.iPedGotoProgressCopy[iPed] = MC_serverBD_2.iPedGotoProgress[iped]
		
	ENDIF
	
ENDPROC

FUNC BOOL IS_PED_PERFORMING_TASK(PED_INDEX tempPed, SCRIPT_TASK_NAME eScriptTask #IF IS_DEBUG_BUILD , BOOL bPrintTaskInfo = FALSE #ENDIF)
	
	SCRIPTTASKSTATUS eTaskStatus = GET_SCRIPT_TASK_STATUS(tempPed, eScriptTask)
	
	#IF IS_DEBUG_BUILD
	IF bPrintTaskInfo
		PRINTLN("[LM][IS_PED_PERFORMING_TASK] - bPrintTaskInfo ------------------")
		IF eTaskStatus = PERFORMING_TASK		PRINTLN("[LM][IS_PED_PERFORMING_TASK][SHOULD_PED_BE_GIVEN_TASK] - PERFORMING_TASK")				ENDIF
		IF eTaskStatus = WAITING_TO_START_TASK	PRINTLN("[LM][IS_PED_PERFORMING_TASK][SHOULD_PED_BE_GIVEN_TASK] - WAITING_TO_START_TASK")		ENDIF
		IF eTaskStatus = FINISHED_TASK			PRINTLN("[LM][IS_PED_PERFORMING_TASK][SHOULD_PED_BE_GIVEN_TASK] - FINISHED_TASK")				ENDIF
		IF eTaskStatus = DORMANT_TASK			PRINTLN("[LM][IS_PED_PERFORMING_TASK][SHOULD_PED_BE_GIVEN_TASK] - DORMANT_TASK")				ENDIF
	ENDIF
	#ENDIF
	
	IF eTaskStatus = PERFORMING_TASK
	OR eTaskStatus = WAITING_TO_START_TASK
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_PED_BE_GIVEN_TASK(PED_INDEX tempPed, INT iPed, SCRIPT_TASK_NAME eScriptTask = SCRIPT_TASK_INVALID, BOOL bDirtyFlagOverride = FALSE, VEHICLE_MISSION eVehicleMission = MISSION_NONE, VEHICLE_INDEX pedVehForMission = NULL #IF IS_DEBUG_BUILD , BOOL bPrintTaskInfo = FALSE #ENDIF)
	
	#IF IS_DEBUG_BUILD
		IF bLMhelpfulScriptAIPrints
			bPrintTaskInfo = TRUE
		ENDIF
	#ENDIF
	
	IF tempPed = NULL
		ASSERTLN("[RCC MISSION] - SHOULD_PED_BE_GIVEN_TASK - iPed: ", iPed, " tempPed = NULL")
		RETURN FALSE
	ENDIF
	
	IF tempPed = LocalPlayerPed
		ASSERTLN("[RCC MISSION] - SHOULD_PED_BE_GIVEN_TASK - iPed: ", iPed, " tempPed = LocalPlayerPed")
		RETURN FALSE
	ENDIF
	
	IF iPed < 0 OR iPed >= FMMC_MAX_PEDS
		ASSERTLN("[RCC MISSION] - SHOULD_PED_BE_GIVEN_TASK - iPed: ", iPed, " - out of range")
		RETURN FALSE
	ENDIF

	IF eScriptTask = SCRIPT_TASK_INVALID
		ASSERTLN("[RCC MISSION] - SHOULD_PED_BE_GIVEN_TASK - iPed: ", iPed, " eScriptTask = SCRIPT_TASK_INVALID")
		RETURN FALSE
	ENDIF

	IF NOT bDirtyFlagOverride
		IF IS_BIT_SET(sRetaskDirtyFlagData.iDirtyFlagPedNeedsRetask[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
			PRINTLN("[RCC MISSION] SHOULD_PED_BE_GIVEN_TASK - Called on ped ", iPed, " iDirtyFlagPedNeedsRetask so RETURN TRUE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iPedHitByStunGun[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
			PRINTLN("[RCC MISSION] SHOULD_PED_BE_GIVEN_TASK - Called on ped ", iPed, " iPedHitByStunGun so RETURN FALSE")
		RETURN FALSE
	ENDIF
	
	IF eScriptTask = SCRIPT_TASK_START_SCENARIO_IN_PLACE
		IF (MC_serverBD_2.iPedGotoProgress[iped] < MAX_ASSOCIATED_GOTO_TASKS)
		AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData.sPointData[MC_serverBD_2.iPedGotoProgress[iped]].iWaitTime > 0)
			IF IS_PED_IN_ANY_VEHICLE(tempPed, TRUE)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(tempPed)
					IF IS_PED_RUNNING_MOBILE_PHONE_TASK(tempPed)
						PRINTLN("[RCC MISSION] SHOULD_PED_BE_GIVEN_TASK - Called on ped ", iPed, " IS_PED_RUNNING_MOBILE_PHONE_TASK so RETURN FALSE")
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	IF IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed], PED_ASSOCIATED_GOTO_TASK_TYPE_PLAY_IDLE_ANIM_ON_WAIT) 
		IF HAS_NET_TIMER_STARTED(tdAssociatedGOTOWaitIdleAnimTimer[iPed])
		AND NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(tdAssociatedGOTOWaitIdleAnimTimer[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData.sPointData[MC_serverBD_2.iPedGotoProgress[iped]].iWaitTime)
			PRINTLN("[RCC MISSION] SHOULD_PED_BE_GIVEN_TASK - Called on ped ", iPed, " tdAssociatedGOTOWaitIdleAnimTimer has not expired so RETURN FALSE")
			RETURN FALSE		
		ENDIF
	ENDIF

	IF IS_PED_PERFORMING_TASK(tempPed, eScriptTask #IF IS_DEBUG_BUILD , bPrintTaskInfo #ENDIF)
		
		// There are actually quite a few different tasks that come under the 'SCRIPT_TASK_VEHICLE_MISSION' task name, but we can check for what type they are if we've passed in the VEHICLE_MISSION type and the ped's vehicle
		IF eScriptTask = SCRIPT_TASK_VEHICLE_MISSION
			IF eVehicleMission != MISSION_NONE
				IF DOES_ENTITY_EXIST(pedVehForMission)
					
					// If the ped is not doing the VEHICLE_MISSION type that we want them to here, then we should retask them with this new type
					IF GET_ACTIVE_VEHICLE_MISSION_TYPE(pedVehForMission) != eVehicleMission
						PRINTLN("[RCC MISSION] SHOULD_PED_BE_GIVEN_TASK - Returning TRUE for ",iped," w SCRIPT_TASK_VEHICLE_MISSION, not on eVehicleMission ",ENUM_TO_INT(eVehicleMission),", but currently on ",ENUM_TO_INT(GET_ACTIVE_VEHICLE_MISSION_TYPE(pedVehForMission)))
						RETURN TRUE
					ENDIF
					
				#IF IS_DEBUG_BUILD
				ELSE
					CASSERTLN(DEBUG_CONTROLLER, "[RCC MISSION] SHOULD_PED_BE_GIVEN_TASK - Called on ped ",iped," with SCRIPT_TASK_VEHICLE_MISSION but no vehicle")
					PRINTLN("[RCC MISSION] SHOULD_PED_BE_GIVEN_TASK - Called on ped ",iped," with SCRIPT_TASK_VEHICLE_MISSION but no vehicle, callstack:")
					DEBUG_PRINTCALLSTACK()
				#ENDIF
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				CASSERTLN(DEBUG_CONTROLLER, "[RCC MISSION] SHOULD_PED_BE_GIVEN_TASK - Called on ped ",iped," with SCRIPT_TASK_VEHICLE_MISSION but no eVehicleMission")
				PRINTLN("[RCC MISSION] SHOULD_PED_BE_GIVEN_TASK - Called on ped ",iped," with SCRIPT_TASK_VEHICLE_MISSION but no eVehicleMission, callstack:")
				DEBUG_PRINTCALLSTACK()
			#ENDIF
			ENDIF
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	PRINTLN("[RCC MISSION] SHOULD_PED_BE_GIVEN_TASK - Called on ped ", iPed, " RETURN TRUE")
		
	RETURN TRUE
	
ENDFUNC

FUNC BOOL CAN_PED_TASKS_BE_CLEARED(PED_INDEX tempPed)

	ENTITY_INDEX tempEntity

	IF IS_PED_IN_WRITHE(tempPed)
	OR IS_PED_EVASIVE_DIVING(tempPed, tempEntity)
		RETURN FALSE
	ENDIF

	RETURN TRUE

ENDFUNC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: SPECIAL PICKUP STATES !
//
//************************************************************************************************************************************************************



#IF IS_DEBUG_BUILD

FUNC STRING DEBUG_GET_SPECIAL_PICKUP_STATE_AS_STRING( eSpecialPickupState eState )
	STRING sState
	SWITCH( eState )
		CASE eSpecialPickupState_BEFORE_PICKUP			sState = "BEFORE_PICKUP"		BREAK
		CASE eSpecialPickupState_ALIGNING				sState = "ALIGNING"				BREAK
		CASE eSpecialPickupState_REQUESTING				sState = "REQUESTING"			BREAK
		CASE eSpecialPickupState_ATTACH_OBJECT_TO_PED	sState = "ATTACH_OBJECT_TO_PED"	BREAK
		CASE eSpecialPickupState_CARRYING				sState = "CARRYING"				BREAK
		CASE eSpecialPickupState_PUTTING_DOWN			sState = "PUTTING_DOWN"			BREAK
		CASE eSpecialPickupState_ALIGNING_FOR_THROW		sState = "ALIGNING_FOR_THROW"	BREAK
		CASE eSpecialPickupState_THROWING				sState = "THROWING"				BREAK
		CASE eSpecialPickupState_WAIT_FOR_DELIVERY		sState = "WAIT_FOR_DELIVERY"	BREAK
		CASE eSpecialPickupState_RESET					sState = "RESET"				BREAK
		CASE eSpecialPickupState_MAX					sState = "MAX"					BREAK
		DEFAULT											sState = "UNKNOWN"				BREAK
	ENDSWITCH
	RETURN sState
ENDFUNC

#ENDIF

PROC SET_CURRENT_SPECIAL_PICKUP_STATE( eSpecialPickupState eNewState )
	#IF IS_DEBUG_BUILD
	STRING sOldState = DEBUG_GET_SPECIAL_PICKUP_STATE_AS_STRING( sCurrentSpecialPickupState.ePickupState )
	STRING sNewState = DEBUG_GET_SPECIAL_PICKUP_STATE_AS_STRING( eNewState )
	CPRINTLN( DEBUG_SIMON, "SET_CURRENT_SPECIAL_PICKUP_STATE - Moving from state ", sOldState, " to state ", sNewState )
	#ENDIF
	sCurrentSpecialPickupState.ePickupState = eNewState
	MPGlobalsInteractions.ePickupState = eNewState
ENDPROC

FUNC eSpecialPickupState GET_CURRENT_SPECIAL_PICKUP_STATE()
	RETURN sCurrentSpecialPickupState.ePickupState
ENDFUNC

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: PED SPAWNING/CLEANUP !
//
//************************************************************************************************************************************************************



FUNC BOOL SHOULD_CLEANUP_PED(INT iped, PED_INDEX tempPed)

	IF GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
	AND IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iped ].iPedBitsetSix, ciPED_BSSix_CleanupAtMissionEnd )
		PRINTLN("[RCC MISSION] SHOULD_CLEANUP_PED: Cleanup ped ",iped, "GAME_STATE_END") 
		RETURN TRUE
	ENDIF
	IF IS_BIT_SET(MC_serverBD.iPedAtSecondaryGotoLocBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetNine, ciPed_BSNine_CleanupOnSecondaryTaskCompleted)
		PRINTLN("[RCC MISSION] SHOULD_CLEANUP_PED: Cleanup ped ",iped, " Secondary task completed") 
		RETURN TRUE
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwelve, ciPed_BSTwelve_ShouldCleanupPedWhenTrailerDetached)
	AND IS_BIT_SET(iPedTrailerDetachedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
	AND (NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwelve, ciPed_BSTwelve_ClearPedTasksWhenTrailerDetached) OR IS_BIT_SET(MC_serverBD.iPedTaskStopped[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed)))
		PRINTLN("[RCC MISSION] SHOULD_CLEANUP_PED: Cleanup ped ",iped, " ciPed_BSTwelve_ShouldCleanupPedWhenTrailerDetached and iPedTrailerDetachedBitset is set.") 
		RETURN TRUE
	ENDIF

	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCleanupObjective != -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCleanupTeam != -1
			IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCleanupTeam] < FMMC_MAX_RULES
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCleanupObjective <= MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCleanupTeam]
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset,ciPED_BS_CleanupAtMidpoint)
						//PRINTLN("[RCC MISSION] SHOULD_CLEANUP_PED check midpoint flag set: ",iped) 
						IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCleanupTeam],g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCleanupObjective)
							//PRINTLN("[RCC MISSION] SHOULD_CLEANUP_PED: at midpoint for rule: ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCleanupObjective) 
							IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCleanupVehicle != -1
								PRINTLN("[MMacK][CleanupVeh] g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCleanupVehicle = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCleanupVehicle)
								VEHICLE_INDEX viTempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCleanupVehicle])
								IF NOT IS_VEHICLE_FUCKED_MP(viTempVeh)
									PRINTLN("[MMacK][CleanupVeh] not fucked")
									IF IS_PED_A_PLAYER(GET_PED_IN_VEHICLE_SEAT(viTempVeh)) 
										PRINTLN("[MMacK][CleanupVeh] a player is driving")
										IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCleanupRange <= 0
											RETURN TRUE
										ELSE
											INT iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY(tempPed)
											IF iClosestPlayer != -1
												PLAYER_INDEX ClosestPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
												PED_INDEX playerPed = GET_PLAYER_PED(ClosestPlayer)
												IF tempPed != NULL
													IF GET_DISTANCE_BETWEEN_ENTITIES(playerPed,tempPed) > g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCleanupRange
													AND NOT (IS_PLAYER_IN_PROPERTY(ClosestPlayer, TRUE) OR IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(ClosestPlayer))
														PRINTLN("[RCC MISSION] SHOULD_CLEANUP_PED: Cleanup ped ",iped, " is too far away (in veh ped)") 
														RETURN TRUE
													ENDIF
												ELSE
													IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(playerPed,g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vPos) > g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCleanupRange
													AND NOT (IS_PLAYER_IN_PROPERTY(ClosestPlayer, TRUE) OR IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(ClosestPlayer))
														PRINTLN("[RCC MISSION] SHOULD_CLEANUP_PED: Cleanup ped ",iped, " is too far away (in veh spawn pos)") 
														RETURN TRUE
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCleanupRange <= 0
								PRINTLN("[RCC MISSION] SHOULD_CLEANUP_PED: Cleanup ped ",iped, " iCleanupRange <= 0 (midpoint)") 
								RETURN TRUE
							ELSE
								INT iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY(tempPed)
								IF iClosestPlayer != -1
									PLAYER_INDEX ClosestPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
									PED_INDEX playerPed = GET_PLAYER_PED(ClosestPlayer)
									IF tempPed != NULL
										IF GET_DISTANCE_BETWEEN_ENTITIES(playerPed,tempPed) > g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCleanupRange
										AND NOT (IS_PLAYER_IN_PROPERTY(ClosestPlayer, TRUE) OR IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(ClosestPlayer))
											PRINTLN("[RCC MISSION] SHOULD_CLEANUP_PED: Cleanup at midpoint ",iped, " is too far away (ped)") 
											RETURN TRUE
										ENDIF
									ELSE
										IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(playerPed,g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vPos) > g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCleanupRange
										AND NOT (IS_PLAYER_IN_PROPERTY(ClosestPlayer, TRUE) OR IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(ClosestPlayer))
											PRINTLN("[RCC MISSION] SHOULD_CLEANUP_PED: Cleanup at midpoint ",iped, " is too far away (spawn pos)") 
											RETURN TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						//PRINTLN("[RCC MISSION] SHOULD_CLEANUP_PED: midpoint not set ",iped) 
						//Midpoint doesn't matter
						IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCleanupVehicle != -1
							PRINTLN("[MMacK][CleanupVeh] g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCleanupVehicle = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCleanupVehicle)
							VEHICLE_INDEX viTempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCleanupVehicle])
							IF NOT IS_VEHICLE_FUCKED_MP(viTempVeh)
								PRINTLN("[MMacK][CleanupVeh] not fucked")
								IF IS_PED_A_PLAYER(GET_PED_IN_VEHICLE_SEAT(viTempVeh)) 
									PRINTLN("[MMacK][CleanupVeh] a player is driving")
									IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCleanupRange <= 0
										RETURN TRUE
									ELSE
										INT iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY(tempPed)
										IF iClosestPlayer != -1
											PLAYER_INDEX ClosestPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
											PED_INDEX playerPed = GET_PLAYER_PED(ClosestPlayer)
											IF tempPed != NULL
												IF GET_DISTANCE_BETWEEN_ENTITIES(playerPed,tempPed) > g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCleanupRange
												AND NOT (IS_PLAYER_IN_PROPERTY(ClosestPlayer, TRUE) OR IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(ClosestPlayer))
													PRINTLN("[RCC MISSION] SHOULD_CLEANUP_PED: Cleanup ped ",iped, " is too far away (in veh ped)") 
													RETURN TRUE
												ENDIF
											ELSE
												IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(playerPed,g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vPos) > g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCleanupRange
												AND NOT (IS_PLAYER_IN_PROPERTY(ClosestPlayer, TRUE) OR IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(ClosestPlayer))
													PRINTLN("[RCC MISSION] SHOULD_CLEANUP_PED: Cleanup ped ",iped, " is too far away (in veh spawn pos)") 
													RETURN TRUE
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCleanupRange <= 0
							PRINTLN("[RCC MISSION] SHOULD_CLEANUP_PED: Cleanup ped ",iped, " iCleanupRange <= 0") 
							RETURN TRUE
						ELSE
							INT iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY(tempPed)
							IF iClosestPlayer != -1
								PLAYER_INDEX ClosestPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
								PED_INDEX playerPed = GET_PLAYER_PED(ClosestPlayer)
								IF tempPed != NULL
									IF GET_DISTANCE_BETWEEN_ENTITIES(playerPed,tempPed) > g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCleanupRange
									AND NOT (IS_PLAYER_IN_PROPERTY(ClosestPlayer, TRUE) OR IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(ClosestPlayer))
										PRINTLN("[RCC MISSION] SHOULD_CLEANUP_PED: Cleanup ped ",iped, " is too far away (ped)") 
										RETURN TRUE
									ENDIF
								ELSE
									IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(playerPed,g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vPos) > g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCleanupRange
									AND NOT (IS_PLAYER_IN_PROPERTY(ClosestPlayer, TRUE) OR IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(ClosestPlayer))
										PRINTLN("[RCC MISSION] SHOULD_CLEANUP_PED: Cleanup ped ",iped, " is too far away (spawn pos)") 
										RETURN TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE: This resets SOME of the ped with index iPed's state back to their defaults
PROC RESET_PED_SERVER_STATE_DATA( INT iped )

	MC_serverBD.isearchingforped 	= -1				// Clearing their search
	MC_serverBD.niTargetID[ iped ] 	= NULL				// learing their target ID
	MC_serverBD_2.iPedState[ iped ] = ciTASK_CHOOSE_NEW_TASK	// resetting their state
	MC_serverBD_2.iPedGotoProgress[ iPed ] = 0			// resetting the goto progress
	vEntityRespawnLoc = <<0, 0, 0>>
	
	CLEAR_BIT( MC_serverBD.iPedDelivered[ GET_LONG_BITSET_INDEX( iped ) ], GET_LONG_BITSET_BIT( iPed ) ) // They haven't been delivered
	CLEAR_BIT( MC_serverBD.iPedNearDropOff[ 0 ][ GET_LONG_BITSET_INDEX( iPed ) ], GET_LONG_BITSET_BIT( iPed ) )
	CLEAR_BIT( MC_serverBD.iPedNearDropOff[ 1 ][ GET_LONG_BITSET_INDEX( iPed ) ], GET_LONG_BITSET_BIT( iPed ) )
	CLEAR_BIT( MC_serverBD.iPedNearDropOff[ 2 ][ GET_LONG_BITSET_INDEX( iPed ) ], GET_LONG_BITSET_BIT( iPed ) )
	CLEAR_BIT( MC_serverBD.iPedNearDropOff[ 3 ][ GET_LONG_BITSET_INDEX( iPed ) ], GET_LONG_BITSET_BIT( iPed ) )
	CLEAR_BIT( MC_serverBD.iPedShouldFleeDropOff[ GET_LONG_BITSET_INDEX( iPed ) ], GET_LONG_BITSET_BIT( iPed ) )
	CLEAR_BIT( MC_serverBD.iPedAtGotoLocBitset[ GET_LONG_BITSET_INDEX( iped ) ], GET_LONG_BITSET_BIT( iPed ) ) // They need to redo their goto
	CLEAR_BIT( MC_serverBD.iPedAtSecondaryGotoLocBitset[ GET_LONG_BITSET_INDEX( iped ) ], GET_LONG_BITSET_BIT( iPed ) )
	
	IF NOT IS_BIT_SET(MC_serverBD.iObjSpawnPedBitset[ GET_LONG_BITSET_INDEX( iPed ) ], GET_LONG_BITSET_BIT( iPed ))
		PRINTLN("[RCC MISSION] RESET_PED_SERVER_STATE_DATA - Setting iObjSpawnPedBitset for ped ",iPed)
		SET_BIT( MC_serverBD.iObjSpawnPedBitset[ GET_LONG_BITSET_INDEX( iPed ) ], GET_LONG_BITSET_BIT( iPed ) )
	ENDIF
	
	CLEAR_BIT( MC_serverBD.iPedGunOnRuleGiven[GET_LONG_BITSET_INDEX( iPed )], GET_LONG_BITSET_BIT( iPed ) ) // They need to be given a gun again
	
ENDPROC

FUNC FLOAT GET_RESPAWN_RANGE_CHECK(INT iped)

	SWITCH iPedSpawnFailCount[iped]
	
		CASE 0
			RETURN 120.0
		BREAK
		
		CASE 1
			RETURN 60.0
		BREAK
		
		CASE 2
			RETURN 50.0
		BREAK
		
		CASE 3
			RETURN 40.0
		BREAK
		
		CASE 4
			RETURN 30.0
		BREAK
		
		CASE 5
			RETURN 20.0
		BREAK
		
		CASE 6
			RETURN 10.0
		BREAK
		
		CASE 7
			RETURN 5.0
		BREAK
		
	ENDSWITCH
	
	RETURN 1.0

ENDFUNC

FUNC FLOAT GET_AIR_VEHICLE_VISIBLE_RANGE_CHECK(INT iveh)
	
	SWITCH iVehSpawnFailCount[iveh]
		CASE 0
			RETURN 300.0
		BREAK
		
		CASE 1
			RETURN 250.0
		BREAK
		
		CASE 2
			RETURN 200.0
		BREAK
		
		CASE 3
			RETURN 150.0
		BREAK
		
		CASE 4
			RETURN 100.0
		BREAK
		
		CASE 5
			RETURN 75.0
		BREAK
		
		CASE 6
			RETURN 50.0
		BREAK
		
		CASE 7
			RETURN 25.0
		BREAK
		
		CASE 8
			RETURN 10.0
		BREAK
	ENDSWITCH
	
	RETURN 1.0
	
ENDFUNC

FUNC BOOL IS_PED_VISIBLE_FOR_RESPAWN(INT iPed)

	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitset,ciPED_BS_IgnoreVisCheck)
		PRINTLN("[RCC MISSION] ignore vis check on ped:",iPed)
		RETURN FALSE
	ELSE
		INT iTemp
		IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(GET_PED_SPAWN_LOCATION(iped, MC_ServerBD.iHeistPresistantPropertyID, iTemp),6,1,1,5,TRUE,TRUE,TRUE,GET_RESPAWN_RANGE_CHECK(iPed),FALSE,-1,TRUE,30)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE

ENDFUNC

FUNC BOOL SHOULD_PED_DO_DEFENSIVE_AREA_GOTO(PED_INDEX tempPed, INT iped)
	
	BOOL bDefensiveAreaGoto = FALSE
	
	IF MC_serverBD_2.iTargetID[iped] = ciINITIAL_GOTO
	AND (MC_serverBD_2.iPedGotoProgress[iped] < MAX_ASSOCIATED_GOTO_TASKS)
	AND IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed], PED_ASSOCIATED_GOTO_TASK_TYPE_COMBAT_GOTO)
		IF (NOT IS_PED_IN_ANY_VEHICLE(tempPed))
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].ipedBitsetEight, ciPED_BSEight_CanDoCombatGotoInVeh)
			bDefensiveAreaGoto = TRUE
		ENDIF
	ENDIF
	
	RETURN bDefensiveAreaGoto
	
ENDFUNC

//bGunOnRuleOverride being TRUE means that it won't bother checking if they're already armed, and will return TRUE if they will ever get armed
FUNC BOOL IS_PED_ARMED_BY_FMMC(INT iped, BOOL bGunOnRuleOverride = FALSE)
	
	BOOL bArmed = FALSE
	
	IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].gun != WEAPONTYPE_UNARMED AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].gun != WEAPONTYPE_INVALID)
		//The ped will be armed straight awway
		bArmed = TRUE
	ENDIF
	
	IF NOT bArmed
		IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iGunOnRuleTeam != -1)
		AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iGunOnRuleRule != -1)
		AND ((g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].wtGunOnRule != WEAPONTYPE_UNARMED) AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].wtGunOnRule != WEAPONTYPE_INVALID))
			IF bGunOnRuleOverride
				bArmed = TRUE
			ELSE
				IF IS_BIT_SET(MC_serverBD.iPedGunOnRuleGiven[GET_LONG_BITSET_INDEX(iped)],GET_LONG_BITSET_BIT(iped))
					bArmed = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bArmed
	
ENDFUNC

FUNC INT GET_PED_COMBAT_STYLE(INT iped)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_Team != -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_Rule != -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_NewStyle != -1
		IF IS_BIT_SET(MC_serverBD.iPedCombatStyleChanged[GET_LONG_BITSET_INDEX(iped)],GET_LONG_BITSET_BIT(iped))
			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_NewStyle
		ENDIF
	ENDIF
	
	RETURN g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyle
	
ENDFUNC

FUNC BOOL GET_PED_HAS_FREE_MOVEMENT_IN_COMBAT(INT iPed)
	
	IF IS_BIT_SET(iPedZoneAggroed[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_Team != -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_Rule != -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_NewStyle != -1
		IF IS_BIT_SET(MC_serverBD.iPedCombatStyleChanged[GET_LONG_BITSET_INDEX(iped)],GET_LONG_BITSET_BIT(iped))
			RETURN IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEleven, ciPED_BSEleven_CombatStyleChange_FreeMovementInCombat)
		ENDIF
	ENDIF
	
	RETURN IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_FreeMovementInCombat)
	
ENDFUNC

FUNC BOOL WILL_PED_EVER_BE_HOSTILE(INT iped)
	
	IF IS_BIT_SET(iPedZoneAggroed[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFlee = ciPED_FLEE_ON
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyle = ciPED_AGRESSIVE
	OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyle = ciPED_BERSERK
	OR ( g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyle = ciPED_DEFENSIVE AND IS_PED_ARMED_BY_FMMC(iped, TRUE) )
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_Team != -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_Rule != -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_NewStyle != -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_NewStyle = ciPED_AGRESSIVE
		OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_NewStyle = ciPED_BERSERK
		OR ( g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_NewStyle = ciPED_DEFENSIVE AND IS_PED_ARMED_BY_FMMC(iped, TRUE) )
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_GIVING_PED_WEAPON_ON_RULE(PED_INDEX tempPed, INT iped)
	
	WEAPON_TYPE wtGunToGive = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].wtGunOnRule
	
	IF (NOT IS_BIT_SET(MC_serverBD.iPedGunOnRuleGiven[GET_LONG_BITSET_INDEX(iped)],GET_LONG_BITSET_BIT(iped)))
	AND (NOT IS_PED_INJURED(tempPed))
		
		INT iTeam = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iGunOnRuleTeam
		INT iRule = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iGunOnRuleRule
		
		IF (MC_serverBD_4.iCurrentHighestPriority[iTeam] >= iRule)
		AND (MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES)
			
			BOOL bAtRightPoint = FALSE
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,ciPED_BSThree_GunOnRule_OnMidpoint)
				IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam],iRule)
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,ciPED_BSThree_GunOnRule_OnAggro)
						IF IS_BIT_SET(MC_serverBD.iObjectivePedAggroedBitset[iTeam],iRule)
							PRINTLN("[RCC MISSION] PROCESS_GIVING_PED_WEAPON_ON_RULE - Ped ", iped," should get a gun because at midpoint and someone has been aggroed, team ",iTeam," rule ",iRule)
							bAtRightPoint = TRUE
						ENDIF
					ELSE
						PRINTLN("[RCC MISSION] PROCESS_GIVING_PED_WEAPON_ON_RULE - Ped ", iped," should get a gun because at midpoint, team ",iTeam," rule ",iRule)
						bAtRightPoint = TRUE
					ENDIF
				ENDIF
			ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,ciPED_BSThree_GunOnRule_OnAggro)
				IF IS_BIT_SET(MC_serverBD.iObjectivePedAggroedBitset[iTeam],iRule)
					PRINTLN("[RCC MISSION] PROCESS_GIVING_PED_WEAPON_ON_RULE - Ped ", iped," should get a gun someone has been aggroed, team ",iTeam," rule ",iRule)
					bAtRightPoint = TRUE
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION] PROCESS_GIVING_PED_WEAPON_ON_RULE - Ped ", iped," should get a gun because past rule, team ",iTeam," rule ",iRule)
				bAtRightPoint = TRUE
			ENDIF
			
			
			IF bAtRightPoint
				
				BOOL bGroup = IS_PED_IN_GROUP(tempPed)
				
				IF bGroup
				OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour, ciPED_BSFour_UseActionModeWhenArmedByGOR)
					SET_PED_USING_ACTION_MODE(tempPed, TRUE, -1, "DEFAULT_ACTION")
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour, ciPED_BSFour_UseActionModeWhenArmedByGOR)
					SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_PLAY_REACTION_ANIMS, FALSE)
					SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_DISABLE_ENTRY_REACTIONS, TRUE)
				ENDIF
				
				IF bGroup
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,FALSE)
				ENDIF
				
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].gun = WEAPONTYPE_UNARMED
					INT iCombatStyle = GET_PED_COMBAT_STYLE(iped)
					
					IF iCombatStyle = ciPED_DEFENSIVE
						SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_ALWAYS_FLEE, FALSE)
						SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_ALWAYS_FIGHT, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_CAN_FIGHT_ARMED_PEDS_WHEN_NOT_ARMED, TRUE)
					ENDIF
					
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFlee = ciPED_FLEE_ON
					OR iCombatStyle = ciPED_DEFENSIVE
						INT iTeamLoop
						FOR iTeamLoop = 0 TO (FMMC_MAX_TEAMS-1)
							IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iTeam[iTeamLoop] = ciPED_RELATION_SHIP_DISLIKE
								IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFlee != ciPED_FLEE_ON
									IF iCombatStyle = ciPED_DEFENSIVE
										SET_PED_CAN_BE_TARGETTED_BY_TEAM(tempPed, iTeamLoop, TRUE)
									ENDIF
								ELSE
									SET_PED_CAN_BE_TARGETTED_BY_TEAM(tempPed, iTeamLoop, TRUE)
								ENDIF
							ENDIF
						ENDFOR
					ENDIF
				ENDIF
				
				IF wtGunToGive = WEAPONTYPE_MINIGUN
					SET_PED_FIRING_PATTERN(tempPed,FIRING_PATTERN_FULL_AUTO)
				ELIF wtGunToGive = WEAPONTYPE_RPG
					IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_RocketPedsAndVehicles)
						GIVE_DELAYED_WEAPON_TO_PED(tempPed, WEAPONTYPE_PISTOL, 25000, FALSE)
						SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_ROCKETS_AGAINST_VEHICLES_ONLY, TRUE)
						PRINTLN("[RCC MISSION] PROCESS_GIVING_PED_WEAPON_ON_RULE - setting ped ", iped," with RPG to only fire it at vehicles")
					ENDIF
				ELIF wtGunToGive = WEAPONTYPE_STUNGUN
					SET_PED_DROPS_WEAPONS_WHEN_DEAD(tempPed,FALSE)
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,ciPED_BSThree_GunOnRule_Holstered)
					PRINTLN("[RCC MISSION] PROCESS_GIVING_PED_WEAPON_ON_RULE - giving ped ", iped," a holstered gun")
					GIVE_DELAYED_WEAPON_TO_PED(tempPed, wtGunToGive, 25000, FALSE)
				ELSE
					PRINTLN("[RCC MISSION] PROCESS_GIVING_PED_WEAPON_ON_RULE - giving ped ", iped," a non-holstered gun")
					GIVE_DELAYED_WEAPON_TO_PED(tempPed, wtGunToGive, 25000, TRUE)
					SET_CURRENT_PED_WEAPON(tempPed, wtGunToGive, TRUE)
				ENDIF
				
				SET_PED_RETASK_DIRTY_FLAG(iped)
				
				IF bIsLocalPlayerHost
					PRINTLN("[RCC MISSION] PROCESS_GIVING_PED_WEAPON_ON_RULE - I am the server; setting iPedGunOnRuleGiven bit for ped ", iped," myself")
					SET_BIT(MC_serverBD.iPedGunOnRuleGiven[GET_LONG_BITSET_INDEX(iped)],GET_LONG_BITSET_BIT(iped))
				ELSE
					PRINTLN("[RCC MISSION] PROCESS_GIVING_PED_WEAPON_ON_RULE - I am a client; broadcasting iPedGunOnRuleGiven bit change for ped ", iped)
					BROADCAST_FMMC_PED_GIVEN_GUNONRULE(iped)
				ENDIF
				
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_CHANGING_PED_COMBAT_STYLE_ON_RULE(PED_INDEX tempPed, INT iped)
	
	IF (NOT IS_BIT_SET(MC_serverBD.iPedCombatStyleChanged[GET_LONG_BITSET_INDEX(iped)],GET_LONG_BITSET_BIT(iped)))
	AND (NOT IS_PED_INJURED(tempPed))
		
		INT iTeam = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_Team
		INT iRule = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_Rule
		
		IF (MC_serverBD_4.iCurrentHighestPriority[iTeam] >= iRule)
		AND (MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES)
			
			BOOL bAtRightPoint = FALSE
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEleven, ciPED_BSEleven_CombatStyleChange_OnMidpoint)
				IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], iRule)
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEleven, ciPED_BSEleven_CombatStyleChange_OnAggro)
						IF IS_BIT_SET(MC_serverBD.iObjectivePedAggroedBitset[iTeam], iRule)
							PRINTLN("[RCC MISSION] PROCESS_CHANGING_PED_COMBAT_STYLE_ON_RULE - Ped ", iped," should get new combat style because at midpoint and someone has been aggroed, team ",iTeam," rule ",iRule)
							bAtRightPoint = TRUE
						ENDIF
					ELSE
						PRINTLN("[RCC MISSION] PROCESS_CHANGING_PED_COMBAT_STYLE_ON_RULE - Ped ", iped," should get new combat style because at midpoint, team ",iTeam," rule ",iRule)
						bAtRightPoint = TRUE
					ENDIF
				ENDIF
			ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEleven, ciPED_BSEleven_CombatStyleChange_OnAggro)
				IF IS_BIT_SET(MC_serverBD.iObjectivePedAggroedBitset[iTeam],iRule)
					PRINTLN("[RCC MISSION] PROCESS_CHANGING_PED_COMBAT_STYLE_ON_RULE - Ped ", iped," should get new combat style someone has been aggroed, team ",iTeam," rule ",iRule)
					bAtRightPoint = TRUE
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION] PROCESS_CHANGING_PED_COMBAT_STYLE_ON_RULE - Ped ", iped," should get new combat style because past rule, team ",iTeam," rule ",iRule)
				bAtRightPoint = TRUE
			ENDIF
			
			
			IF bAtRightPoint
				
				INT iCombatStyle = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_NewStyle
				
				IF iCombatStyle != g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyle
					
					WEAPON_TYPE wtGun = GET_BEST_PED_WEAPON(tempPed, TRUE)
					
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFlee != ciPED_FLEE_ON
						INT iTeamLoop
						FOR iTeamLoop = 0 TO (FMMC_MAX_TEAMS-1)
							IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iTeam[iTeamLoop] = ciPED_RELATION_SHIP_DISLIKE
								//Only auto-target if they're going to respond with hostility
								IF iCombatStyle = ciPED_AGRESSIVE
								OR iCombatStyle = ciPED_BERSERK
								OR ( iCombatStyle = ciPED_DEFENSIVE AND wtGun != WEAPONTYPE_UNARMED AND wtGun != WEAPONTYPE_INVALID )
									SET_PED_CAN_BE_TARGETTED_BY_TEAM(tempPed, iTeamLoop, TRUE)
								ELSE
									SET_PED_CAN_BE_TARGETTED_BY_TEAM(tempPed, iTeamLoop, FALSE)
								ENDIF
							ENDIF
						ENDFOR
					ENDIF
					
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyle = ciPED_BERSERK // If this ped used to be berserk
						SET_PED_CONFIG_FLAG(tempPed, PCF_ShouldChargeNow, FALSE)
						// If we ever let peds go back to being berserk again after this removal, we need to make sure we clear this ped's bit in iPedHasCharged across all machines
					ENDIF
					
					BOOL bCombatGoto
					
					IF MC_serverBD_2.iPedState[iped] = ciTASK_GOTO_COORDS
					AND SHOULD_PED_DO_DEFENSIVE_AREA_GOTO(tempPed, iped)
						bCombatGoto = TRUE
					ENDIF
					
					IF NOT bCombatGoto
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_FreeMovementInCombat)
						AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEleven, ciPED_BSEleven_CombatStyleChange_FreeMovementInCombat)
							PRINTLN("[RCC MISSION] PROCESS_CHANGING_PED_COMBAT_STYLE_ON_RULE - Ped ",iped," turning off combat free movement, call SET_UP_FMMC_PED_DEFENSIVE_AREA")
							SET_UP_FMMC_PED_DEFENSIVE_AREA(tempPed, iped, GET_ENTITY_COORDS(tempPed), wtGun)
						ENDIF
					ENDIF
					
					APPLY_PED_COMBAT_STYLE(tempPed, iCombatStyle, wtGun, iped, bCombatGoto)
					
					SET_PED_RETASK_DIRTY_FLAG(iped)
					
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[RCC MISSION] PROCESS_CHANGING_PED_COMBAT_STYLE_ON_RULE - Ped ",iped," already has combat style ",iCombatStyle,", don't need to do anything")
				#ENDIF	
				ENDIF
				
				IF bIsLocalPlayerHost
					PRINTLN("[RCC MISSION] PROCESS_CHANGING_PED_COMBAT_STYLE_ON_RULE - I am the server; setting iPedCombatStyleChanged bit for ped ", iped," myself")
					SET_BIT(MC_serverBD.iPedCombatStyleChanged[GET_LONG_BITSET_INDEX(iped)],GET_LONG_BITSET_BIT(iped))
				ELSE
					PRINTLN("[RCC MISSION] PROCESS_CHANGING_PED_COMBAT_STYLE_ON_RULE - I am a client; broadcasting iPedCombatStyleChanged bit change for ped ", iped)
					BROADCAST_FMMC_PED_COMBAT_STYLE_CHANGED(iped)
				ENDIF
				
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC

FUNC VECTOR GET_PED_FMMC_SPAWN_LOCATION(INT iPed, BOOL& bForceAreaCheck, INT &iRandomHeadingSelected)
	
	VECTOR vSpawnLoc = GET_PED_SPAWN_LOCATION(iped, MC_ServerBD.iHeistPresistantPropertyID, iRandomHeadingSelected)
	
	bForceAreaCheck = FALSE
	
	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vOverrideSpawnPos)
		PRINTLN("[PED] GET_PED_FMMC_SPAWN_LOCATION - Ped ", iPed, " using vOverrideSpawnPos")
		bForceAreaCheck = TRUE
		RETURN g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vOverrideSpawnPos
	ENDIF
	
	IF NOT IS_BIT_SET(MC_serverBD.iPedFirstSpawnBitset[GET_LONG_BITSET_INDEX(iped)],GET_LONG_BITSET_BIT(iped)) // Isn't this ped's first spawn
	AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vSecondSpawnPos)
		IF GET_RANDOM_INT_IN_RANGE(0, 100) < 50
			SET_BIT(iLocalBoolCheck10, LBOOL10_PED_SPAWN_AT_SECOND_POSITION)
			vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vSecondSpawnPos
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSpawnNearType != ciRULE_TYPE_NONE
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSpawnNearEntityID > -1
		
		IF (NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_FirstSpawnNotNearEntity))
		OR IS_BIT_SET(MC_serverBD.iObjSpawnPedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)) // Gets set once a ped has spawned
			
			VECTOR vNewSpawn = GET_FMMC_ENTITY_LOCATION(vSpawnLoc, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSpawnNearType, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSpawnNearEntityID)
			
			IF NOT IS_VECTOR_ZERO(vNewSpawn)
				vSpawnLoc = vNewSpawn
				bForceAreaCheck = TRUE
			ENDIF
			
		ENDIF
	ENDIF
	
	IF( IS_THIS_A_QUICK_RESTART_JOB() 
	OR IS_CORONA_INITIALISING_A_QUICK_RESTART() )
		IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(3)
			IF NOT IS_VECTOR_ZERO( g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 3 ] )
				vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 3 ]
				PRINTLN("[RCC MISSION] GET_PED_FMMC_SPAWN_LOCATION - on a quick restart and ped ", iPed, " has a restart vector = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 3 ] )
			ENDIF
		ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(2)
			IF NOT IS_VECTOR_ZERO( g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 2 ] )
				vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 2 ]
				PRINTLN("[RCC MISSION] GET_PED_FMMC_SPAWN_LOCATION - on a quick restart and ped ", iPed, " has a restart vector = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 2 ] )
			ENDIF
		ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(1)
			IF NOT IS_VECTOR_ZERO( g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 1 ] )
				vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 1 ]
				PRINTLN("[RCC MISSION] GET_PED_FMMC_SPAWN_LOCATION - on a quick restart and ped ", iPed, " has a restart vector = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 1 ] )
			ENDIF
		ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(0)
			IF NOT IS_VECTOR_ZERO( g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 0 ] )
				vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 0 ]
				PRINTLN("[RCC MISSION] GET_PED_FMMC_SPAWN_LOCATION - on a quick restart and ped ", iPed, " has a restart vector = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPed ].vRestartPos[ 0 ] )
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFourteen, ciPED_BSFourteen_UseRandomPoolPosAsRespawnSpawn)	
		INT iSpawnPool = GET_RANDOM_SELECTION_FROM_SPAWN_POOL(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sWarpLocationSettings.vPosition)		
		vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sWarpLocationSettings.vPosition[iSpawnPool]
		PRINTLN( "[RCC MISSION] GET_PED_FMMC_SPAWN_LOCATION - Grabbing random position from pool for ped ", iPed, " iSpawnPool: ", iSpawnPool, " vSpawnLoc: ", vSpawnLoc)
	ENDIF
	
	RETURN vSpawnLoc
	
ENDFUNC

FUNC BOOL IS_PED_A_PRIORITY_FOR_ANY_TEAM(INT iPed)
	
	INT iTeam
	FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
		IF MC_serverBD_4.iPedPriority[iPed][iTeam] < FMMC_MAX_RULES
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PED_RESPAWN_NOW(INT i)

	PRINTLN("[RCC MISSION] SHOULD_PED_RESPAWN_NOW - Ped: ", i)
		
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedTeam > -1
		BOOL bHasTeamReachedEntitySpawnConditions = FALSE
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedObjective, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedAlwaysForceSpawnOnRule)			
			bHasTeamReachedEntitySpawnConditions = TRUE
		ELSE
			PRINTLN("[RCC MISSION] SHOULD_PED_RESPAWN_NOW - Ped: ", i, " RETURN FALSE, HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS = FALSE,", 
			" iAssociatedSpawn: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedSpawn,
			" iAssociatedTeam: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedTeam,
			" iAssociatedObjective: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedObjective,
			" iAssociatedScoreRequired: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedScoreRequired,
			" iAssociatedAlwaysSpawnOnRule: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedAlwaysForceSpawnOnRule)
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondAssociatedObjective > -1
		AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondAssociatedTeam > -1
			IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondAssociatedObjective, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondAlwaysForceSpawnOnRule)						
				bHasTeamReachedEntitySpawnConditions = TRUE
			ELSE
				PRINTLN("[RCC MISSION] SHOULD_PED_RESPAWN_NOW - Ped: ", i, " RETURN FALSE, HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS = FALSE,", 
				" iSecondAssociatedSpawn: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondAssociatedSpawn,
				" iSecondAssociatedTeam: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondAssociatedTeam,
				" iSecondAssociatedObjective: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondAssociatedObjective, 
				" iSecondScoreRequired: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondScoreRequired,
				" iSecondAlwaysSpawnOnRule: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondAlwaysForceSpawnOnRule)	
			ENDIF	
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iThirdAssociatedObjective > -1
		AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iThirdAssociatedTeam > -1
			IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iThirdAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iThirdAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iThirdAssociatedObjective, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iThirdScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iThirdAlwaysForceSpawnOnRule)				
				bHasTeamReachedEntitySpawnConditions = TRUE
			ELSE
				PRINTLN("[RCC MISSION] SHOULD_PED_RESPAWN_NOW - Ped: ", i, " RETURN FALSE, HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS = FALSE,", 
				" iThirdAssociatedSpawn: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iThirdAssociatedSpawn,
				" iThirdAssociatedTeam: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iThirdAssociatedTeam,
				" iThirdAssociatedObjective: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iThirdAssociatedObjective,
				" iThirdScoreRequired: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iThirdScoreRequired,
				" iThirdAlwaysSpawnOnRule: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iThirdAlwaysForceSpawnOnRule)			
			ENDIF	
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFourthAssociatedObjective > -1
		AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFourthAssociatedTeam > -1
			IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFourthAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFourthAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFourthAssociatedObjective, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFourthScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFourthAlwaysForceSpawnOnRule)				
				bHasTeamReachedEntitySpawnConditions = TRUE
			ELSE
				PRINTLN("[RCC MISSION] SHOULD_PED_RESPAWN_NOW - Ped: ", i, " HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS = FALSE,", 
				" iFourthAssociatedSpawn: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFourthAssociatedSpawn,
				" iFourthAssociatedTeam: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFourthAssociatedTeam,
				" iFourthAssociatedObjective: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFourthAssociatedObjective,
				" iFourthScoreRequired: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFourthScoreRequired,
				" iFourthAlwaysSpawnOnRule: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFourthAlwaysForceSpawnOnRule)							
			ENDIF	
		ENDIF
		IF NOT bHasTeamReachedEntitySpawnConditions
			RETURN FALSE
		ENDIF
	ENDIF
		
	IF SHOULD_CLEANUP_PED(i,NULL)
		PRINTLN("[RCC MISSION] SHOULD_PED_RESPAWN_NOW - SHOULD_CLEANUP_PED for ped : ",i)
		RETURN FALSE
	ENDIF
	
	#IF NOT IS_NEXTGEN_BUILD
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetFive, ciPED_BSFive_NGSpawnOnly)
			PRINTLN("[RCC MISSION] SHOULD_PED_RESPAWN_NOW, ciPED_BSFive_NGSpawnOnly for ped : ",i)
			RETURN FALSE
		ENDIF
	#ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_PedDeathTracking)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iContinuityId > -1
		AND IS_LONG_BIT_SET(MC_serverBD_1.sLEGACYMissionContinuityVars.iPedDeathBitset, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iContinuityId)
			PRINTLN("[JS][CONTINUITY] - SHOULD_PED_RESPAWN_NOW - Ped ", i, " with continuity ID ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iContinuityId, " not spawning due to death of previous stand mission")
			RETURN FALSE
		ENDIF
	ENDIF
	
	//Don't respawn if you were told to not respawn after an objective that is before this objective
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iStopRespawningRule != -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iStopRespawningTeam != -1
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iStopRespawningRule <= MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iStopRespawningTeam]
				
				IF MC_serverBD_2.iCurrentPedRespawnLives[i] > 0
					MC_serverBD_2.iCurrentPedRespawnLives[i] = 0
					PRINTLN("[RCC MISSION] SHOULD_PED_RESPAWN_NOW - setting lives for ped ", i, " to 0 because it shouldn't respawn past rule ", MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iStopRespawningTeam])
				ENDIF
				
				PRINTLN("[RCC MISSION] SHOULD_PED_RESPAWN_NOW - Refusing ped spawn for ped ", i, " because we are past the rule where they were told to clean up")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF	
	
	INT iPlayersPlaying
	IF IS_BIT_SET(MC_serverbd.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
	AND NOT IS_PED_A_PRIORITY_FOR_ANY_TEAM(i)
		iPlayersPlaying = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]
	ELSE
		iPlayersPlaying = MC_serverBD.iTotalNumStartingPlayers
	ENDIF
	
	IF NOT ARE_ENOUGH_PLAYERS_PLAYING_TO_SPAWN_PED(i, iPlayersPlaying)
		PRINTLN("[RCC MISSION] SHOULD_PED_RESPAWN_NOW - Ped: ", i, " - ARE_ENOUGH_PLAYERS_PLAYING_TO_SPAWN_PED = FALSE")
		IF IS_BIT_SET(MC_serverbd.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
		AND MC_serverBD_2.iCurrentPedRespawnLives[i] > 0
			MC_serverBD_2.iCurrentPedRespawnLives[i] = 0
			PRINTLN("[RCC MISSION] SHOULD_PED_RESPAWN_NOW - setting lives for ped ", i, " to 0 because it can't spawn due to player count changing.")
		ENDIF
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].fPedPlayerSpawnBlockrange > 0
		INT iPlayer
		FOR iPlayer = 0 TO MAX_NUM_MC_PLAYERS-1
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayer))
				PED_INDEX piPlayerPed = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPlayer)))
				IF DOES_ENTITY_EXIST(piPlayerPed)
				AND NOT IS_PED_INJURED(piPlayerPed)
					IF (VDIST2(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].vPos, GET_ENTITY_COORDS(piPlayerPed)) < POW(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].fPedPlayerSpawnBlockrange, 2))
						PRINTLN("[RCC MISSION] SHOULD_PED_RESPAWN_NOW - Participant ", iPlayer, " is too close. Not spawning due to ciPed_BSEleven_PreventSpawnPlayerInRange")
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	IF DOES_PED_HAVE_PLAYER_VARIABLE_SETTING(i)
		IF IS_BIT_SET(MC_serverbd.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedObjective > -1
			AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedTeam > -1
				IF MC_serverBD_1.sCreatedCount.iNumPedCreated[g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedObjective] >= MC_serverBD.iNumberOfPart[g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedTeam]
					PRINTLN("[RCC MISSION] SHOULD_PED_RESPAWN_NOW - Ped: ",i," - RETURN FALSE, number create is more than players : ",MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedTeam]," FOR TEAM: ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedTeam)
					SET_BIT(MC_serverBD_1.sCreatedCount.iSkipPedBitset[GET_LONG_BITSET_INDEX(i)],GET_LONG_BITSET_BIT(i))
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(i, MC_serverBD_4.rsgSpawnSeed, eSGET_Ped)
		PRINTLN("[RCC MISSION] SHOULD_PED_RESPAWN_NOW - Ped: ", i, " - RETURN FALSE, Ped does not have suitable spawn group")
		RETURN FALSE
	ENDIF
	
	IF IS_ANY_TEAM_ATTEMPTING_TO_RESTART_RULE()
		PRINTLN("[RCC MISSION] SHOULD_PED_RESPAWN_NOW - SBBOOL5_RETRY_CURRENT_RULE_TEAM")
		RETURN FALSE
	ENDIF
	
	IF IS_ANY_TEAM_BEING_WARPED_TO_START_POINT()
	AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_ALL_PLAYERS_RENDERPHASES_PAUSED)
		PRINTLN("[RCC MISSION] SHOULD_PED_RESPAWN_NOW - Waiting for renderphases to be paused")
		RETURN FALSE
	ENDIF
	
	IF IS_ENTITY_SPAWN_TRIGGERED_BY_ZONE_ENTRY(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iZoneBlockEntitySpawn)
		IF HAS_ENTITY_SPAWN_ZONE_REQUIREMENT_BEEN_MET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iZoneBlockEntitySpawn, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iZoneBlockEntitySpawnPlayerReq)
			PRINTLN("[RCC MISSION] SHOULD_PED_RESPAWN_NOW - Ped: ", i, " - RETURN TRUE, HAS_ENTITY_SPAWN_ZONE_REQUIREMENT_BEEN_MET = TRUE.")
			RETURN TRUE
		ENDIF
		PRINTLN("[RCC MISSION] SHOULD_PED_RESPAWN_NOW - Ped: ", i, " - RETURN FALSE, HAS_ENTITY_SPAWN_ZONE_REQUIREMENT_BEEN_MET = FALSE.. iZone: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iZoneBlockEntitySpawn, " iPlayerReq: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iZoneBlockEntitySpawnPlayerReq)
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iCSRespawnSceneIndex > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iCSRespawnShotIndex > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iCSRespawnSceneIndex = MC_serverBD.iSpawnScene
		AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iCSRespawnShotIndex <= MC_serverBD.iSpawnShot
		AND (iCamShotLoaded >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iCSRespawnShotIndex AND IS_BIT_SET(iLocalBoolCheck7, LBOOL7_CURRENT_SCRIPTED_CAM_AREA_LOADED))
			RETURN TRUE
		ELSE
			PRINTLN("[RCC MISSION] SHOULD_PED_RESPAWN_NOW Ped ",i," - RETURNING FALSE - iCSRespawnSceneIndex: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iCSRespawnSceneIndex, " SpawnScene: ", MC_serverBD.iSpawnScene,
					" iCSRespawnShotIndex: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iCSRespawnShotIndex, " SpawnShot: ", MC_serverBD.iSpawnShot, 
					" Camshot: ", iCamShotLoaded, " LBOOL7_CURRENT_SCRIPTED_CAM_AREA_LOADED: ", IS_BIT_SET(iLocalBoolCheck7, LBOOL7_CURRENT_SCRIPTED_CAM_AREA_LOADED))
			RETURN FALSE
		ENDIF
	ENDIF
		
	RETURN TRUE

ENDFUNC

FUNC BOOL CAN_PED_STILL_SPAWN_IN_MISSION(INT iped, INT iTeamOverride = -1, INT iRuleOverride = -1)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedTeam > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedSpawn = ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY
			
			IF iTeamOverride = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedTeam
			AND iRuleOverride != -1
				IF iRuleOverride <= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedObjective
					// The ped will have a chance to spawn:
					RETURN TRUE
				ENDIF
			ELSE
				IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedObjective
					// The ped still has a chance to spawn:
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE // Spawn type won't prevent ped from spawning after the rule
			RETURN TRUE
		ENDIF
	ELSE
		//Ped will just spawn at mission start!
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSecondAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSecondAssociatedTeam > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSecondAssociatedSpawn = ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY
			IF iTeamOverride = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSecondAssociatedTeam
			AND iRuleOverride != -1
				IF iRuleOverride <= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSecondAssociatedObjective
					RETURN TRUE
				ENDIF
			ELSE
				IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSecondAssociatedTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSecondAssociatedObjective
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iThirdAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iThirdAssociatedTeam > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iThirdAssociatedSpawn = ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY
			IF iTeamOverride = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iThirdAssociatedTeam
			AND iRuleOverride != -1
				IF iRuleOverride <= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iThirdAssociatedObjective
					RETURN TRUE
				ENDIF
			ELSE
				IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iThirdAssociatedTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iThirdAssociatedObjective
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFourthAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFourthAssociatedTeam > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFourthAssociatedSpawn = ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY
			IF iTeamOverride = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFourthAssociatedTeam
			AND iRuleOverride != -1
				IF iRuleOverride <= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFourthAssociatedObjective
					RETURN TRUE
				ENDIF
			ELSE
				IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFourthAssociatedTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFourthAssociatedObjective
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC SET_PED_TO_SPAWN_NEXT(INT iped)
	
	PRINTLN("[RCC MISSION] SET_PED_TO_SPAWN_NEXT - Called with ped ",iped)
	
	MC_serverBD.isearchingforPed = iped
	iPedSpawnFailDelay[iped] = -1
	iPedSpawnFailCount[iped] = 0
	CLEAR_BIT(MC_serverBD.iPedDelayRespawnBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
	
ENDPROC

PROC GET_NEXT_SPAWNING_PED_AFTER_VEHICLE_SPAWN(INT iveh)
	
	INT iped
	
	INT iDriverPedFound = -1
	INT iAnySeatPedFound = -1
	INT iForcedOtherSeatPedFound = -1
	
	FOR iped = 0 TO (MC_serverBD.iNumPedCreated - 1)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle = iveh
		AND SHOULD_PED_SPAWN_IN_VEHICLE(iped)
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iped])
			AND MC_serverBD_2.iCurrentPedRespawnLives[iped] > 0
			AND SHOULD_PED_RESPAWN_NOW(iped)
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicleSeat = ENUM_TO_INT(VS_DRIVER)
					iDriverPedFound = iped
					BREAKLOOP // We've found the driver, we don't need to look for anyone else. Break out!
				ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicleSeat = -3
					IF iAnySeatPedFound = -1
						iAnySeatPedFound = iped
					ENDIF
				ELSE
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iForcedVehicleSeat != -1
						IF iForcedOtherSeatPedFound = -1
							iForcedOtherSeatPedFound = iped
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	ENDFOR
	
	IF iDriverPedFound != -1
		
		PRINTLN("[RCC MISSION] GET_NEXT_SPAWNING_PED_AFTER_VEHICLE_SPAWN - found driver ",iDriverPedFound)
		SET_PED_TO_SPAWN_NEXT(iDriverPedFound)
		
	ELIF iAnySeatPedFound != -1
		
		PRINTLN("[RCC MISSION] GET_NEXT_SPAWNING_PED_AFTER_VEHICLE_SPAWN - found ped in any seat ",iAnySeatPedFound)
		SET_PED_TO_SPAWN_NEXT(iAnySeatPedFound)
		
	ELIF iForcedOtherSeatPedFound != -1
		
		PRINTLN("[RCC MISSION] GET_NEXT_SPAWNING_PED_AFTER_VEHICLE_SPAWN - found ped forced into other seat ",iForcedOtherSeatPedFound)
		SET_PED_TO_SPAWN_NEXT(iForcedOtherSeatPedFound)
		
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[RCC MISSION] GET_NEXT_SPAWNING_PED_AFTER_VEHICLE_SPAWN - Didn't find any peds for vehicle ",iveh)
	#ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL ARE_WE_WAITING_FOR_PEDS_TO_CLEAN_UP()
	
	BOOL bWaiting
	
	IF DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME(TRUE)
	OR IS_BIT_SET(iLocalBoolCheck7, LBOOL7_HOST_APPLIED_NEW_PRIORITY_MIDFRAME)
	OR IS_BIT_SET(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
	OR MC_serverBD.iPedCleanup_NeedOwnershipBS[0] != 0
	OR MC_serverBD.iPedCleanup_NeedOwnershipBS[1] != 0
	OR MC_serverBD.iPedCleanup_NeedOwnershipBS[2] != 0
		bWaiting = TRUE
	ENDIF
	
	RETURN bWaiting
	
ENDFUNC

FUNC BOOL CAN_SPAWN_AND_PLAY_ANIM_CLOSE_TO_VEHICLE(int iAnimation)
	
	BOOL bSpawn = FALSE
	
	SWITCH iAnimation
		CASE ciPED_IDLE_ANIM__PRISONPLANE_AGENT_B_IDLE
		CASE ciPED_IDLE_ANIM__PRISONPLANE_AGENT_B_REACT
		CASE ciPED_IDLE_ANIM__BIKE_INSPECTION
		CASE ciPED_IDLE_ANIM__BROKEN_CAR
		CASE ciPED_IDLE_ANIM__WELDING_KNEELING
			bSpawn = TRUE
		BREAK
	ENDSWITCH
	
	RETURN bSpawn
ENDFUNC



FUNC BOOL SHOULD_CLEANUP_VEH(INT iVeh, VEHICLE_INDEX tempVeh)
	
	IF GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
	AND IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[ iveh ].iVehBitsetTwo, ciFMMC_VEHICLE3_CleanupAtMissionEnd )
		PRINTLN("[RCC MISSION] SHOULD_CLEANUP_VEH ",iVeh," - Cleaning up as game state = GAME_STATE_END")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iAmbientOverrideVehicle,iveh)
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupDetachedTrailerOnRule != -1
		IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam] >= g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupDetachedTrailerOnRule
			IF NOT IS_BIT_SET(bsCleanupVehicleRule, iVeh)
				IF NOT IS_ENTITY_ATTACHED(tempVeh)
					PRINTLN("[SHOULD_CLEANUP_VEH] iVeh = ", iVeh, " | Model = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(tempVeh)), " | iCleanupDetachedTrailerOnRule ... ", MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam], " >= ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupDetachedTrailerOnRule)
					
					SET_BIT(bsCleanupVehicleRule, iVeh)
					
					RETURN TRUE
				ELSE
					PRINTLN("[SHOULD_CLEANUP_VEH] iVeh = ", iVeh, " | Model = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(tempVeh)), " | iCleanupDetachedTrailerOnRule ... Attached!")
					
					SET_BIT(bsCleanupVehicleRule, iVeh)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupVehicleDistance > 0
		IF CHECK_ALL_PLAYERS_EXCEED_DISTANCE_FROM_COORDS(GET_ENTITY_COORDS(tempVeh), g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupVehicleDistance)
			IF IS_BIT_SET(bsCleanupVehicleDistance, iVeh)
				PRINTLN("[SHOULD_CLEANUP_VEH] iVeh = ", iVeh, " | Model = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(tempVeh)), " | iCleanupVehicleDistance ... CHECK_ALL_PLAYERS_EXCEED_DISTANCE_FROM_COORDS = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupVehicleDistance)
				
				RETURN TRUE
			ENDIF
		ELIF NOT CHECK_ALL_PLAYERS_EXCEED_DISTANCE_FROM_COORDS(GET_ENTITY_COORDS(tempVeh), ROUND((g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupVehicleDistance / 4.0) * 3.0))	//75% of the distance to create a theshold to ensure it won't trigger incorrectly
			IF NOT IS_BIT_SET(bsCleanupVehicleDistance, iVeh)
				PRINTLN("[SHOULD_CLEANUP_VEH] iVeh = ", iVeh, " | Model = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(tempVeh)), " | iCleanupVehicleDistance ... SET_BIT bsCleanupVehicleDistance")
				
				SET_BIT(bsCleanupVehicleDistance, iVeh)
			ENDIF
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iCleanupObjective != -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iCleanupTeam != -1
			INT iTeamToCheckCleanUp = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iCleanupTeam
			INT iRuleToCleanUpOn = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iCleanupObjective
			
			IF iRuleToCleanUpOn <= MC_serverBD_4.iCurrentHighestPriority[ iTeamToCheckCleanUp ]
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetTwo, ciFMMC_VEHICLE2_CleanupAtMidpoint )
					IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeamToCheckCleanUp],iRuleToCleanUpOn)
						IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iCleanupVehicle != -1
							PRINTLN("[MMacK][CleanupVeh] g_FMMC_STRUCT.sPlacedVeh[iveh].iCleanupVehicle = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iCleanupVehicle)
							VEHICLE_INDEX viTempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iCleanupVehicle])
							IF NOT IS_VEHICLE_FUCKED_MP(viTempVeh)
								PRINTLN("[MMacK][CleanupVeh] not fucked")
								IF IS_PED_A_PLAYER(GET_PED_IN_VEHICLE_SEAT(viTempVeh)) 
									PRINTLN("[MMacK][CleanupVeh] a player is driving")
									IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iCleanupRange <= 0
										PRINTLN("[SHOULD_CLEANUP_VEH] - Midpoint Cleaning up in vehicle  iCleanupRange <= 0")
										RETURN TRUE
									ELSE
										INT iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY(tempVeh)
										PLAYER_INDEX ClosestPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
										PED_INDEX playerPed = GET_PLAYER_PED(ClosestPlayer)
										IF tempVeh != NULL
											IF GET_DISTANCE_BETWEEN_ENTITIES(playerPed,tempVeh) > g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iCleanupRange
												PRINTLN("[SHOULD_CLEANUP_VEH] - Midpoint Distance check cleanup range 1")
												RETURN TRUE
											ENDIF
										ELSE
											IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(playerPed,g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].vPos) > g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iCleanupRange
												PRINTLN("[SHOULD_CLEANUP_VEH] - Midpoint Distance check cleanup range 2")
												RETURN TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iCleanupRange <= 0
							PRINTLN("[SHOULD_CLEANUP_VEH] - Midpoint base cleanup range <= 0")
							RETURN TRUE
						ELSE
							INT iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY(tempVeh)
							PLAYER_INDEX ClosestPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
							PED_INDEX playerPed = GET_PLAYER_PED(ClosestPlayer)
							IF tempVeh != NULL
								IF GET_DISTANCE_BETWEEN_ENTITIES(playerPed,tempVeh) > g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupRange
									PRINTLN("[SHOULD_CLEANUP_VEH] - Midpoint Else 1")
									RETURN TRUE
								ENDIF
							ELSE
								IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(playerPed,g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vPos) > g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupRange
									PRINTLN("[SHOULD_CLEANUP_VEH] - Midpoint Else 2")
									RETURN TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iCleanupRange <= 0
						PRINTLN("[SHOULD_CLEANUP_VEH] - Cleanup range <= 0")
						RETURN TRUE
					ELSE
						INT iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY(tempVeh)
						PLAYER_INDEX ClosestPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
						PED_INDEX playerPed = GET_PLAYER_PED(ClosestPlayer)
						IF tempVeh != NULL
							IF GET_DISTANCE_BETWEEN_ENTITIES(playerPed,tempVeh) > g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupRange
								PRINTLN("[SHOULD_CLEANUP_VEH] - Distance check 1")
								RETURN TRUE
							ENDIF
						ELSE
							IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(playerPed,g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vPos) > g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupRange
								PRINTLN("[SHOULD_CLEANUP_VEH] - Distance check 2")
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_FORCE_IMMEDIATE_ENTITY_SPAWN(INT iEntity, INT iType)
	
	IF g_bInMissionControllerCutscene
	OR IS_THIS_RULE_A_CUTSCENE()
		
		INT i = 0
		INT iCutscene = iCutsceneIndexPlaying
		
		PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE - SHOULD_FORCE_IMMEDIATE_ENTITY_SPAWN - iEntity: ", iEntity, " iType: ", iType, " iCutscene: ", iCutscene, " eCutsceneTypePlaying: ", eCutsceneTypePlaying)
		
		IF iCutscene > -1			
			FOR i = 0 TO FMMC_MAX_CUTSCENE_ENTITIES-1
				IF eCutsceneTypePlaying = FMMCCUT_SCRIPTED
					IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iType = iType
						IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iIndex = iEntity
							PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE - SHOULD_FORCE_IMMEDIATE_ENTITY_SPAWN - iEntity: ", iEntity, " returning TRUE (FMMCCUT_SCRIPTED)")
							RETURN TRUE
						ENDIF
					ENDIF
				ELIF eCutsceneTypePlaying = FMMCCUT_MOCAP
					IF g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].sCutsceneEntities[i].iType = iType
						IF g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].sCutsceneEntities[i].iIndex = iEntity
							PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE - SHOULD_FORCE_IMMEDIATE_ENTITY_SPAWN - iEntity: ", iEntity, " returning TRUE (FMMCCUT_MOCAP)")
							RETURN TRUE
						ENDIF
					ENDIF
				ELIF eCutsceneTypePlaying = FMMCCUT_ENDMOCAP
					IF g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iType = iType
						IF g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iIndex = iEntity
							PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE - SHOULD_FORCE_IMMEDIATE_ENTITY_SPAWN - iEntity: ", iEntity, " returning TRUE (FMMCCUT_ENDMOCAP)")
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ENDIF		
		
		PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE - SHOULD_FORCE_IMMEDIATE_ENTITY_SPAWN - iEntity: ", iEntity, " trying to respawn during a cutscene but it's not a registered entity so going through normal procedures.")
	ENDIF
		
	RETURN FALSE
ENDFUNC

FUNC BOOL RESPAWN_MISSION_PED(INT iped)

	FLOAT fspawnhead
	VEHICLE_INDEX tempVeh
	PED_INDEX tempPed
	VEHICLE_SEAT tempseat
	INT iVeh = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle
	BOOL bVehSpawn
	BOOL bSpawn,bIninterior
	BOOL bForceAreaCheck
	BOOL bShouldForceSpawn = SHOULD_FORCE_IMMEDIATE_ENTITY_SPAWN(iVeh, CREATION_TYPE_PEDS)
	
	PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - Calling for iPed: ", iped)

	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iped])
		
		PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - Ped Does not exist: ", iped)
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn != DUMMY_MODEL_FOR_SCRIPT
			
			// If we want to instantly respawn this ped
			IF (NOT IS_BIT_SET(MC_serverBD.iPedDelayRespawnBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed)))
			AND (iPedSpawnFailDelay[iped] = -1)
				
				PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - requesting model for ped in respawn : ", iped)
				REQUEST_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn)
				
				BOOL bLoaded = TRUE
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iInventoryBS > 0
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iInventoryBS,ciPED_INV_Briefcase_prop_ld_case_01)
						REQUEST_MODEL(PROP_LD_CASE_01)
						REQUEST_ANIM_DICT("weapons@misc@jerrycan@mp_male")
						IF (NOT HAS_MODEL_LOADED(PROP_LD_CASE_01))
						OR (NOT HAS_ANIM_DICT_LOADED("weapons@misc@jerrycan@mp_male"))
							bLoaded = FALSE
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT HAS_MODEL_LOADED(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn)
					bLoaded = FALSE
				ENDIF
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn = INT_TO_ENUM(MODEL_NAMES, HASH("U_M_Y_Juggernaut_01"))
					IF NOT HAVE_JUGGERNAUT_ANIMS_LOADED_FOR_CREATION()
						PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - FAILING ON - juggernaut anims ", iped)
						bLoaded = FALSE
					ENDIF
				ENDIF
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn = IG_LESTERCREST
					REQUEST_CLIP_SET("move_heist_lester")
					IF NOT HAS_CLIP_SET_LOADED("move_heist_lester")
						PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - FAILING ON - lester ped clip set ", iped)
						bLoaded = FALSE
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_StartDead)
					REQUEST_ANIM_DICT(GET_DEAD_PED_ANIM_DICT_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDeadAnim))
					IF NOT HAS_ANIM_DICT_LOADED(GET_DEAD_PED_ANIM_DICT_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDeadAnim))
						PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - FAILING ON - dead ped ",iped," anim dict ",GET_DEAD_PED_ANIM_DICT_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDeadAnim))
						bLoaded = FALSE
					ENDIF
				ENDIF
				
				IF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_SUBMARINE_FINALE) 
					IF IS_VALID_INTERIOR(GET_INTERIOR_AT_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vPos))
						IF NOT IS_INTERIOR_READY(GET_INTERIOR_AT_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vPos))
							PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - FAILING ON - submarine not ready yet, ped ",iped)
							bLoaded = FALSE
						ENDIF
					ENDIF
				ENDIF
				
				BOOL bRespawnInFreshVeh = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo, ciPED_BSTwo_RespawnInNewVehicle)
				
				IF bLoaded
					IF CAN_REGISTER_MISSION_PEDS(1)
					AND NOT ARE_WE_WAITING_FOR_PEDS_TO_CLEAN_UP()
						IF (MC_serverBD.isearchingforPed = -1 AND MC_serverBD.isearchingforObj = -1 AND MC_serverBD.isearchingforVeh = -1)
						OR (MC_serverBD.isearchingforPed = iped AND MC_serverBD.isearchingforObj = -1 AND MC_serverBD.isearchingforVeh = -1)
							
 							IF SHOULD_PED_SPAWN_IN_VEHICLE(iped)
							AND NOT SHOULD_CLEANUP_VEH(iVeh, NULL)
								BOOL bVehFail
								
								IF iVeh != -1
								AND bRespawnInFreshVeh
									IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicleSeat = ENUM_TO_INT(VS_DRIVER)
										IF NOT IS_BIT_SET(MC_serverBD.iPedRespawnVehBitset, iVeh)
										AND IS_BIT_SET(MC_serverBD.iPedFirstSpawnVehBitset, iVeh)
											
											IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
												PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - ped: ", iped," needs a new copy of vehicle ",iveh,", cleanup old one")
												CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
												
												IF IS_BIT_SET(MC_serverBD.iVehSpawnBitset, iVeh)
													PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - Clear iVehSpawnBitset for veh ",iveh,", cleaned up for a new copy")
													CLEAR_BIT(MC_serverBD.iVehSpawnBitset, iVeh)
												ENDIF
											ENDIF
											
											PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - ped: ", iped," cleaned up vehicle ",iveh,", set iPedRespawnVehBitset")
											SET_BIT(MC_serverBD.iPedRespawnVehBitset, iVeh)
										ELSE
											PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - ped: ", iped," in veh: ", iVeh, " bit (ciPED_BSTwo_RespawnInNewVehicle) has been set. We are waiting for the vehicle to be respawned.")
										ENDIF
									ENDIF
								ENDIF
								
								/*IF bRespawnInFreshVeh
								// IF Passenger.
									IF iVeh != -1
									AMD IS_BIT_SET(MC_serverBD.iPedRespawnVehBitset, iVeh)
									AND IS_BIT_SET(MC_serverBD.iPedFirstSpawnVehBitset, iVeh)
										PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - ped: ", iped," in veh: ", iVeh, " We are a passenger to a fresh boat being respawned.. bVehFail=TRUE until the boat has respawned. (ciPED_BSTwo_RespawnInNewVehicle)")
										bVehFail = TRUE
									ENDIF
								ENDIF*/
								
								// This bit gets set if the vehicle we're spawning into exists							
								IF NOT IS_BIT_SET(MC_serverBD.iVehSpawnBitset, iVeh)
									PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - calling create respawn ped in vehicle but vehicle not created, so waiting for ped: ",iped," in veh: ",iVeh)
									bVehFail = TRUE
								ELSE
									IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicleSeat != ENUM_TO_INT(VS_DRIVER)
									AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicleSeat != -3
										
										IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
										AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]))
											
											tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
											
											IF IS_VEHICLE_SEAT_FREE(tempVeh, VS_DRIVER) AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iForcedVehicleSeat = -1
												PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - calling create respawn ped in vehicle but driver not yet spawned, so waiting for ped: ",iped," in veh: ",iVeh)
												bVehFail = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								IF bVehFail
									
									INT iteam
									BOOL crit
									
									//Check if this ped is marked as critical for any teams
									FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
										IF IS_BIT_SET(MC_serverBD.iMissionCriticalPed[iteam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
											crit = TRUE
										ENDIF
									ENDFOR
									
									IF NOT crit
										IF iPedSpawnFailCount[iped] < 4
											iPedSpawnFailCount[iped]++
										ENDIF
										iPedSpawnFailDelay[iped] = 0 //Start the delay timer
										PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - vehicle spawn failed, setting iPedSpawnFailDelay counting ",iped)
									ELSE
										iPedSpawnFailCount[iped]++
									ENDIF
									
									SET_BIT(MC_serverBD.iPedDelayRespawnBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
									
									MC_serverBD.isearchingforPed = -1
									vEntityRespawnLoc = <<0, 0, 0>>
									SpawnInterior = NULL
									
									RETURN FALSE
								ENDIF
								
								// Do we want to place this ped in a vehicle?
								IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
								AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]))
									
									tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
									
									IF NOT IS_ANY_PLAYER_IN_VEHICLE(tempVeh)
										IF NOT IS_VEHICLE_FULL(tempVeh)
											FLOAT froll = GET_ENTITY_ROLL(tempVeh)
											IF froll > -35.0
											AND froll < 35.0
												IF Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer)
													IF NOT IS_SPHERE_VISIBLE_TO_ANOTHER_MACHINE(GET_ENTITY_COORDS(tempVeh),1.0)	
													AND NOT IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(tempVeh),1.0)	
														bVehSpawn = TRUE
													ENDIF
												ELSE
													bVehSpawn = TRUE
												ENDIF
												
												PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - Ped ",iped," should spawn in vehicle ",iveh,", seat ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicleSeat)
												
												IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicleSeat = -3
													tempseat = GET_FIRST_FREE_VEHICLE_SEAT_RC(tempVeh)
													PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - Ped ",iped," doesn't have a seat preference set, pick first free seat - tempseat = ", ENUM_TO_INT(tempseat))
												ELSE
													IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicleSeat = VS_ANCHORED_1
													OR IS_VEHICLE_SEAT_FREE(tempVeh,INT_TO_ENUM(VEHICLE_SEAT,g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicleSeat))
														tempseat = INT_TO_ENUM(VEHICLE_SEAT,g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicleSeat)
														PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - Ped ",iped," has a seating preference, and the seat is free - tempseat = ", ENUM_TO_INT(tempseat))
													ELSE
														tempseat = GET_FIRST_FREE_VEHICLE_SEAT_RC(tempVeh)
														PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - Ped ",iped," has a seating preference, but the seat isn't free so get first free seat - tempseat = ", ENUM_TO_INT(tempseat))
													ENDIF
												ENDIF
												
											#IF IS_DEBUG_BUILD
											ELSE
												PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - Ped ",iped," not spawning in vehicle ",iveh,", froll is over 35 - froll = ",froll)
											#ENDIF
											ENDIF
										#IF IS_DEBUG_BUILD
										ELSE
											PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - Ped ",iped," not spawning in vehicle ",iveh,", vehicle is full")
										#ENDIF
										ENDIF
									#IF IS_DEBUG_BUILD
									ELSE
										PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - Ped ",iped," not spawning in vehicle ",iveh,", a player is inside")
									#ENDIF
									ENDIF
								#IF IS_DEBUG_BUILD
								ELSE
									IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
										PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - Ped ",iped," not spawning in vehicle ",iveh,", net ID doesn't exist")
									ELSE
										PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - Ped ",iped," not spawning in vehicle ",iveh,", undriveable")
									ENDIF
								#ENDIF
								ENDIF // Else this vehicle isn't driveable
								
							ENDIF // SHOULD_PED_SPAWN_IN_VEHICLE
							
							MC_serverBD.isearchingforped = iped
							
							// If we're spawning this ped in a vehicle
							IF bVehSpawn
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
									
									PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - spawning ped ",iped," into vehicle ",iveh,", seat = ",ENUM_TO_INT(tempseat))
									BOOL bSpawned = FALSE
									
									IF ENUM_TO_INT(tempSeat) = VS_ANCHORED_1
										INT iTemp
										vEntityRespawnLoc = GET_PED_FMMC_SPAWN_LOCATION(iped, bForceAreaCheck, iTemp)
										FLOAT heading = GET_PED_SPAWN_HEADING(iped, MC_ServerBD.iHeistPresistantPropertyID, IS_BIT_SET(iLocalBoolCheck10, LBOOL10_PED_SPAWN_AT_SECOND_POSITION), 0)
										IF CREATE_NET_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped], PEDTYPE_MISSION, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn, vEntityRespawnLoc, heading )
											VECTOR offset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]), vEntityRespawnLoc)
											offset = offset + <<0.0,0.0, 1.85*0.5>>
											heading = heading - GET_ENTITY_HEADING(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]))
											//GET_MODEL_DIMENSIONS
											ATTACH_ENTITY_TO_ENTITY(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped]), NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]), 0, offset, <<0,0,heading>>, TRUE, TRUE)
											//ATTACH_ENTITY_TO_ENTITY_PHYSICALLY(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped]), NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]), 0, 0, offset, <<0,0,0>>, <<0,0,heading>>, 10000, FALSE)
											bSpawned = TRUE											
										ENDIF
									ELSE
										IF NOT IS_VEHICLE_SEAT_FREE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]), tempseat)
											tempseat = GET_FIRST_FREE_VEHICLE_SEAT_RC(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]))
										ENDIF
										IF CREATE_NET_PED_IN_VEHICLE(MC_serverBD_1.sFMMC_SBD.niPed[iped], MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh], PEDTYPE_MISSION, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn,tempseat)
											bSpawned = TRUE
										ENDIF
									ENDIF
									IF bSpawned
										//Make sure the AI blip struct is clear for this index.
										AI_BLIP_STRUCT emptyAIBlipStruct
										biHostilePedBlip[iped] = emptyAIBlipStruct
										
										tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped])

										SET_UP_FMMC_PED_RC(tempPed, iped, MC_serverBD.iTotalNumStartingPlayers, MC_serverBD_1.sLEGACYMissionContinuityVars, DEFAULT, NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]))
										
										IF iVeh != -1
										AND bRespawnInFreshVeh
											IF IS_BIT_SET(MC_serverBD.iPedRespawnVehBitset, iVeh)
												CLEAR_BIT(MC_serverBD.iPedRespawnVehBitset, iVeh)
												PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - ped: ", iped," in veh: ", iVeh, " Clearing Bit iPedRespawnVehBitset (ciPED_BSTwo_RespawnInNewVehicle)")
											ENDIF
											
											IF NOT IS_BIT_SET(MC_serverBD.iPedFirstSpawnVehBitset, iVeh)
												SET_BIT(MC_serverBD.iPedFirstSpawnVehBitset, iVeh)
												PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - ped: ", iped," in veh: ", iVeh, " SET Bit iPedFirstSpawnVehBitset (ciPED_BSTwo_RespawnInNewVehicle)")
											ENDIF
										ENDIF
										
										#IF IS_DEBUG_BUILD
										SET_BIT(iPedGivenDebugName[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
										CLEAR_PED_IS_SPOOKED_DEBUG(iPed)
										CLEAR_PED_IS_AGGROD_DEBUG(iPed)
										#ENDIF
										
										IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset , ciPED_BS_TrackDamage)
											MC_serverBD_2.iVipOriginalHealth = GET_ENTITY_HEALTH(tempPed)
											PRINTLN("[NETCELEBRATION] - [SAC] - [RCC MISSION] - saved VIP health. MC_serverBD_2.iVipOriginalHealth = ", MC_serverBD_2.iVipOriginalHealth)
										ENDIF
										SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn)
										IF NETWORK_ENTITY_AREA_DOES_EXIST(MC_serverBD.iSpawnArea)
											NETWORK_REMOVE_ENTITY_AREA(MC_serverBD.iSpawnArea)
										ENDIF
										MC_serverBD.iSpawnArea = -1
										RESET_NET_TIMER(MC_serverBD.tdSpawnAreaTimeout)
										iPedSpawnFailCount[iped] = 0
										iPedSpawnFailDelay[iped] = -1
										SpawnInterior = NULL
										vEntityRespawnLoc = <<0, 0, 0>>
										
										// Clear some of this ped's data
										RESET_PED_SERVER_STATE_DATA( iped )
										
										IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iGunOnRuleTeam != -1)
										AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iGunOnRuleRule != -1)
										AND ((g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].wtGunOnRule != WEAPONTYPE_UNARMED) AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].wtGunOnRule != WEAPONTYPE_INVALID))
											PROCESS_GIVING_PED_WEAPON_ON_RULE(tempPed,iped)
										ENDIF
										
										IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
											SET_PED_CONFIG_FLAG(tempPed, PCF_DontCryForHelpOnStun, TRUE)
										ENDIF
										
										IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_Team != -1
										AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_Rule != -1
										AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_NewStyle != -1
											PROCESS_CHANGING_PED_COMBAT_STYLE_ON_RULE(tempPed, iped)
										ENDIF
										
										SET_ENTITY_VISIBLE(tempPed,TRUE)
										IF NOT IS_BIT_SET(MC_serverBD.iPedFirstSpawnBitset[GET_LONG_BITSET_INDEX(iped)],GET_LONG_BITSET_BIT(iped))
											SET_PED_HIGHLY_PERCEPTIVE(tempPed,TRUE)
											TASK_COMBAT_HATED_TARGETS_AROUND_PED(tempPed,299)
											PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - Putting ped ",iped," into combat immediately with TASK_COMBAT_HATED_TARGETS_AROUND_PED, this isn't the ped's first time spawning (iPedFirstSpawnBitset is already cleared)")
										ELSE
											CLEAR_BIT(MC_serverBD.iPedFirstSpawnBitset[GET_LONG_BITSET_INDEX(iped)],GET_LONG_BITSET_BIT(iped))
											PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - This is ped ",iped,"'s first time spawning, clear iPedFirstSpawnBitset")
										ENDIF
										
										IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iTeam[MC_playerBD[iPartToUse].iteam] = ciPED_RELATION_SHIP_DISLIKE
										OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iTeam[MC_playerBD[iPartToUse].iteam] = ciPED_RELATION_SHIP_HATE
											IF WILL_PED_EVER_BE_HOSTILE(iped)
												MC_serverBD.iNumPedsSpawned++
											ENDIF
										ENDIF

										IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_StartDead)
											REMOVE_ANIM_DICT(GET_DEAD_PED_ANIM_DICT_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDeadAnim))
										ENDIF

										GET_NEXT_SPAWNING_PED_AFTER_VEHICLE_SPAWN(iveh)										
									
										IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwelve, ciPed_BSTwelve_SpawnPedInCover)
											IF NOT IS_PED_IN_ANY_VEHICLE(tempPed)
												VECTOR vCoverCoords = GET_ENTITY_COORDS(tempPed)
												PRINTLN("[LM][RCC MISSION] SET_UP_FMMC_PED_RC - Entering Cover.")
												SET_PED_TO_LOAD_COVER(tempPed, TRUE)
												COVERPOINT_INDEX cpIndex = GET_CLOSEST_COVER_POINT_TO_LOCATION(vCoverCoords)
												TASK_PUT_PED_DIRECTLY_INTO_COVER(tempPed, vCoverCoords, -1, TRUE, DEFAULT, TRUE, FALSE, cpIndex)
												FORCE_PED_AI_AND_ANIMATION_UPDATE(tempPed, TRUE)
											ENDIF
										ENDIF
										
										RETURN TRUE
									ENDIF
								ELSE
									NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
								ENDIF
							ELSE // Else this ped is spawning outside a vehicle
								
								IF iEntityRespawnType != ci_TARGET_PED
								OR iEntityRespawnID != iped
									vEntityRespawnLoc = <<0, 0, 0>>
									SpawnInterior = NULL
									iEntityRespawnType = ci_TARGET_PED
									iEntityRespawnID = iped
								ENDIF
																
								IF IS_VECTOR_ZERO(vEntityRespawnLoc)
									CLEAR_BIT(iLocalBoolCheck10, LBOOL10_PED_SPAWN_AT_SECOND_POSITION)
									vEntityRespawnLoc = GET_PED_FMMC_SPAWN_LOCATION(iped, bForceAreaCheck, iEntityRespawnRandomSelected)
									IF bForceAreaCheck
										SET_BIT(iLocalBoolCheck4, LBOOL4_RESPAWN_SHOULD_FORCE_AREA_CHECK)
									ELSE
										CLEAR_BIT(iLocalBoolCheck4, LBOOL4_RESPAWN_SHOULD_FORCE_AREA_CHECK)
									ENDIF
								ELSE
									bForceAreaCheck = IS_BIT_SET(iLocalBoolCheck4, LBOOL4_RESPAWN_SHOULD_FORCE_AREA_CHECK)
								ENDIF
								
								IF NOT bForceAreaCheck
								AND NOT NETWORK_ENTITY_AREA_DOES_EXIST(MC_serverBD.iSpawnArea)
								AND NOT CAN_SPAWN_AND_PLAY_ANIM_CLOSE_TO_VEHICLE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnim)
									PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED ",iped," - network add entity area")
									MC_serverBD.iSpawnArea = NETWORK_ADD_ENTITY_AREA((vEntityRespawnLoc-<<0.35,0.35,0.35>>),(vEntityRespawnLoc+<<0.35,0.35,0.35>>))
									REINIT_NET_TIMER(MC_serverBD.tdSpawnAreaTimeout)
								ELSE
									IF bForceAreaCheck // This actually means to IGNORE the network area check...
									OR NETWORK_ENTITY_AREA_HAVE_ALL_REPLIED(MC_serverBD.iSpawnArea)
									OR CAN_SPAWN_AND_PLAY_ANIM_CLOSE_TO_VEHICLE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnim)
									OR bShouldForceSpawn
										IF (NOT bForceAreaCheck
										AND NOT NETWORK_ENTITY_AREA_IS_OCCUPIED(MC_serverBD.iSpawnArea)	
										AND NOT IS_PED_VISIBLE_FOR_RESPAWN(iPed))
										OR CAN_SPAWN_AND_PLAY_ANIM_CLOSE_TO_VEHICLE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnim)
										OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPed_BSEight_ForceSpawnAtAuthoredCoords)
										OR bShouldForceSpawn
											PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - spawning ped ",iped," in original location, coords: ",vEntityRespawnLoc, ", forceSpawning: ", IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPed_BSEight_ForceSpawnAtAuthoredCoords))
											IF CREATE_NET_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped], PEDTYPE_MISSION, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn, vEntityRespawnLoc, GET_PED_SPAWN_HEADING(iped, MC_ServerBD.iHeistPresistantPropertyID, IS_BIT_SET(iLocalBoolCheck10, LBOOL10_PED_SPAWN_AT_SECOND_POSITION), iEntityRespawnRandomSelected))
												
												//Make sure the AI blip struct is clear for this index.
												AI_BLIP_STRUCT emptyAIBlipStruct
												biHostilePedBlip[iped] = emptyAIBlipStruct
										
												tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
												
												SET_UP_FMMC_PED_RC(tempPed, iped,MC_serverBD.iTotalNumStartingPlayers, MC_serverBD_1.sLEGACYMissionContinuityVars, DEFAULT)
												
												IF iVeh != -1
												AND bRespawnInFreshVeh
													IF IS_BIT_SET(MC_serverBD.iPedRespawnVehBitset, iVeh) 
														CLEAR_BIT(MC_serverBD.iPedRespawnVehBitset, iVeh)
														PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - ped: ", iped," in veh: ", iVeh, " Clearing Bit iPedRespawnVehBitset (ciPED_BSTwo_RespawnInNewVehicle)")
													ENDIF
													
													IF NOT IS_BIT_SET(MC_serverBD.iPedFirstSpawnVehBitset, iVeh)
														SET_BIT(MC_serverBD.iPedFirstSpawnVehBitset, iVeh)
														PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - ped: ", iped," in veh: ", iVeh, " SET Bit iPedFirstSpawnVehBitset (ciPED_BSTwo_RespawnInNewVehicle)")
													ENDIF
												ENDIF
									
												#IF IS_DEBUG_BUILD
												SET_BIT(iPedGivenDebugName[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
												CLEAR_PED_IS_SPOOKED_DEBUG(iPed)
												CLEAR_PED_IS_AGGROD_DEBUG(iPed)
												#ENDIF
												
												IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset , ciPED_BS_TrackDamage)
													MC_serverBD_2.iVipOriginalHealth = GET_ENTITY_HEALTH(tempPed)
												ENDIF
												SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn)
												NETWORK_REMOVE_ENTITY_AREA(MC_serverBD.iSpawnArea)
												RESET_NET_TIMER(MC_serverBD.tdSpawnAreaTimeout)
												
												// Clear some of this ped's data
												RESET_PED_SERVER_STATE_DATA( iped )
												
												IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iGunOnRuleTeam != -1)
												AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iGunOnRuleRule != -1)
												AND ((g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].wtGunOnRule != WEAPONTYPE_UNARMED) AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].wtGunOnRule != WEAPONTYPE_INVALID))
													PROCESS_GIVING_PED_WEAPON_ON_RULE(tempPed,iped)
												ENDIF
												
												IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_Team != -1
												AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_Rule != -1
												AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_NewStyle != -1
													PROCESS_CHANGING_PED_COMBAT_STYLE_ON_RULE(tempPed, iped)
												ENDIF
												
												MC_serverBD.iSpawnArea = -1
												iPedSpawnFailCount[iped] = 0
												iPedSpawnFailDelay[iped] = -1
												
												vEntityRespawnLoc = <<0, 0, 0>>
												SpawnInterior = NULL
												iEntityRespawnRandomSelected = 0
												
												SET_ENTITY_VISIBLE(tempPed,TRUE)
												
												IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_StartDead)
													REMOVE_ANIM_DICT(GET_DEAD_PED_ANIM_DICT_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDeadAnim))
												ENDIF
												
												IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
													SET_PED_CONFIG_FLAG(tempPed, PCF_DontCryForHelpOnStun, TRUE)
												ENDIF
												
												IF NOT IS_BIT_SET(MC_serverBD.iPedFirstSpawnBitset[GET_LONG_BITSET_INDEX(iped)],GET_LONG_BITSET_BIT(iped))
													SET_PED_HIGHLY_PERCEPTIVE(tempPed,TRUE)
													TASK_COMBAT_HATED_TARGETS_AROUND_PED(tempPed,299)
													PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - Putting ped ",iped," into combat immediately with TASK_COMBAT_HATED_TARGETS_AROUND_PED, this isn't the ped's first time spawning (iPedFirstSpawnBitset is already cleared)")
												ELSE
													CLEAR_BIT(MC_serverBD.iPedFirstSpawnBitset[GET_LONG_BITSET_INDEX(iped)],GET_LONG_BITSET_BIT(iped))
													PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - This is ped ",iped,"'s first time spawning, clear iPedFirstSpawnBitset")
												ENDIF
												
												IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iTeam[MC_playerBD[iPartToUse].iteam] = ciPED_RELATION_SHIP_DISLIKE
												OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iTeam[MC_playerBD[iPartToUse].iteam] = ciPED_RELATION_SHIP_HATE
													IF WILL_PED_EVER_BE_HOSTILE(iped)
														MC_serverBD.iNumPedsSpawned++
													ENDIF
												ENDIF
												
												IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwelve, ciPed_BSTwelve_SpawnPedInCover)
													IF NOT IS_PED_IN_ANY_VEHICLE(tempPed)					
														VECTOR vCoverCoords = GET_ENTITY_COORDS(tempPed)
														PRINTLN("[LM][RCC MISSION] SET_UP_FMMC_PED_RC - Entering Cover.")
														COVERPOINT_INDEX cpIndex = GET_CLOSEST_COVER_POINT_TO_LOCATION(vCoverCoords)
														SET_PED_TO_LOAD_COVER(tempPed, TRUE)
														TASK_PUT_PED_DIRECTLY_INTO_COVER(tempPed, vCoverCoords, -1, TRUE, DEFAULT, TRUE, FALSE, cpIndex)
														FORCE_PED_AI_AND_ANIMATION_UPDATE(tempPed, TRUE)
													ENDIF
												ENDIF
												
												RETURN TRUE
											ENDIF
										ELSE
											
											IF NETWORK_ENTITY_AREA_IS_OCCUPIED(MC_serverBD.iSpawnArea)	
												PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED ", iped, " - Network Entity Area is Occupied returning TRUE.")
											ENDIF
											IF IS_PED_VISIBLE_FOR_RESPAWN(iPed)
												PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED ", iped, " - IS_PED_VISIBLE_FOR_RESPAWN = TRUE.")
											ENDIF											
											IF bForceAreaCheck
												PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED ", iped, " - bForceAreaCheck = TRUE")
											ENDIF
											
											IF SpawnInterior = NULL
												SpawnInterior =  GET_INTERIOR_AT_COORDS(vEntityRespawnLoc)
											ENDIF
											
											IF SpawnInterior = NULL
												bIninterior = FALSE
											ELSE
												bIninterior = TRUE
											ENDIF
											
											SPAWN_SEARCH_PARAMS SpawnSearchParams
											SpawnSearchParams.bConsiderInteriors = bIninterior
											SpawnSearchParams.fMinDistFromPlayer = 40
											
											IF bForceAreaCheck
												SpawnSearchParams.vFacingCoords = vEntityRespawnLoc
												//Want to spawn outside of a range from vEntityRespawnLoc:
												SpawnSearchParams.vAvoidCoords[0] = vEntityRespawnLoc
												SpawnSearchParams.fAvoidRadius[0] = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fRespawnMinRange
											ENDIF
											
											PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - Calling GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY with iPed: ",iped, " vEntityRespawnLoc: ", vEntityRespawnLoc, " fRespawnRange: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fRespawnRange, " fAvoidRadius: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fRespawnMinRange)
											
											IF GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY(vEntityRespawnLoc,g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fRespawnRange,vEntityRespawnLoc,fspawnhead,SpawnSearchParams)
												IF NOT IS_OBJECTIVE_PED(iped)
												AND NOT IS_CARRIER_PED(iped)
													
													FLOAT fVehRad = 6.0 * (1.0 / (iPedSpawnFailCount[iped] + 1))
													FLOAT fVisionRangeCheck = GET_RESPAWN_RANGE_CHECK(iped)
													
													IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(vEntityRespawnLoc,fVehRad,1,1,1,TRUE,TRUE,TRUE,fVisionRangeCheck,FALSE,-1,TRUE,(fVisionRangeCheck / 2.0))
														bSpawn = TRUE
													ELSE
														PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - coords visible so delaying ped spawn: ",iped)
														//SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn)
														NETWORK_REMOVE_ENTITY_AREA(MC_serverBD.iSpawnArea)
														RESET_NET_TIMER(MC_serverBD.tdSpawnAreaTimeout)
														MC_serverBD.iSpawnArea = -1
														MC_serverBD.isearchingforped = -1
														
														vEntityRespawnLoc = <<0, 0, 0>>
														SpawnInterior = NULL
														iEntityRespawnRandomSelected = 0
														
														INT iteam
														BOOL crit
														
														//Check if this ped is marked as critical for any teams
														FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
															IF IS_BIT_SET(MC_serverBD.iMissionCriticalPed[iteam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
																crit = TRUE
															ENDIF
														ENDFOR
														
														IF NOT crit
															IF iPedSpawnFailCount[iped] < 4
																iPedSpawnFailCount[iped]++
															ENDIF
															iPedSpawnFailDelay[iped] = 0 //Start the delay timer
															PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - coords visible, setting iPedSpawnFailDelay timer running ",iped)
														ELSE
															iPedSpawnFailCount[iped]++
														ENDIF
														
														SET_BIT(MC_serverBD.iPedDelayRespawnBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
													ENDIF
												ELSE
													bSpawn = TRUE
												ENDIF
												
												IF bSpawn
													PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - spawning ped ",iped," at new coords ",vEntityRespawnLoc,", not at the creator-placed coords!")
													IF CREATE_NET_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped], PEDTYPE_MISSION, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn, vEntityRespawnLoc, GET_PED_SPAWN_HEADING(iped, MC_ServerBD.iHeistPresistantPropertyID, IS_BIT_SET(iLocalBoolCheck10, LBOOL10_PED_SPAWN_AT_SECOND_POSITION), iEntityRespawnRandomSelected) )
														
														//Make sure the AI blip struct is clear for this index.
														AI_BLIP_STRUCT emptyAIBlipStruct
														biHostilePedBlip[iped] = emptyAIBlipStruct

														tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
														
														SET_UP_FMMC_PED_RC(tempPed, iped,MC_serverBD.iTotalNumStartingPlayers, MC_serverBD_1.sLEGACYMissionContinuityVars, DEFAULT)
														
														IF iVeh != -1
														AND bRespawnInFreshVeh
															IF IS_BIT_SET(MC_serverBD.iPedRespawnVehBitset, iVeh)
																CLEAR_BIT(MC_serverBD.iPedRespawnVehBitset, iVeh)
																PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - ped: ", iped," in veh: ", iVeh, " Clearing Bit iPedRespawnVehBitset (ciPED_BSTwo_RespawnInNewVehicle)")
															ENDIF
															
															IF NOT IS_BIT_SET(MC_serverBD.iPedFirstSpawnVehBitset, iVeh)
																SET_BIT(MC_serverBD.iPedFirstSpawnVehBitset, iVeh)
																PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - ped: ", iped," in veh: ", iVeh, " SET Bit iPedFirstSpawnVehBitset (ciPED_BSTwo_RespawnInNewVehicle)")
															ENDIF
														ENDIF
														
														#IF IS_DEBUG_BUILD
														SET_BIT(iPedGivenDebugName[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
														#ENDIF
														
														IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset , ciPED_BS_TrackDamage)
															MC_serverBD_2.iVipOriginalHealth = GET_ENTITY_HEALTH(tempPed)
														ENDIF
														SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn)
														NETWORK_REMOVE_ENTITY_AREA(MC_serverBD.iSpawnArea)
														RESET_NET_TIMER(MC_serverBD.tdSpawnAreaTimeout)
														MC_serverBD.iSpawnArea = -1
														iPedSpawnFailDelay[iped] = -1
														iPedSpawnFailCount[iped] = 0
														vEntityRespawnLoc = <<0,0,0>>
														SpawnInterior = NULL
														iEntityRespawnRandomSelected = 0
														// Clear some of this ped's data
														RESET_PED_SERVER_STATE_DATA( iped )
														
														IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iGunOnRuleTeam != -1)
														AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iGunOnRuleRule != -1)
														AND ((g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].wtGunOnRule != WEAPONTYPE_UNARMED) AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].wtGunOnRule != WEAPONTYPE_INVALID))
															PROCESS_GIVING_PED_WEAPON_ON_RULE(tempPed,iped)
														ENDIF
														
														IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_Team != -1
														AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_Rule != -1
														AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCombatStyleChange_NewStyle != -1
															PROCESS_CHANGING_PED_COMBAT_STYLE_ON_RULE(tempPed, iped)
														ENDIF
														
														SET_ENTITY_VISIBLE(tempPed,TRUE)
														
														IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_StartDead)
															REMOVE_ANIM_DICT(GET_DEAD_PED_ANIM_DICT_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDeadAnim))
														ENDIF
														
														IF NOT IS_BIT_SET(MC_serverBD.iPedFirstSpawnBitset[GET_LONG_BITSET_INDEX(iped)],GET_LONG_BITSET_BIT(iped))
															SET_PED_HIGHLY_PERCEPTIVE(tempPed,TRUE)
															TASK_COMBAT_HATED_TARGETS_AROUND_PED(tempPed,299)
															PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - Putting ped ",iped," into combat immediately with TASK_COMBAT_HATED_TARGETS_AROUND_PED, this isn't the ped's first time spawning (iPedFirstSpawnBitset is already cleared)")
														ELSE
															CLEAR_BIT(MC_serverBD.iPedFirstSpawnBitset[GET_LONG_BITSET_INDEX(iped)],GET_LONG_BITSET_BIT(iped))
															PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - This is ped ",iped,"'s first time spawning, clear iPedFirstSpawnBitset")
														ENDIF
														
														IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iTeam[MC_playerBD[iPartToUse].iteam] = ciPED_RELATION_SHIP_DISLIKE
														OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iTeam[MC_playerBD[iPartToUse].iteam] = ciPED_RELATION_SHIP_HATE
															IF WILL_PED_EVER_BE_HOSTILE(iped)
																MC_serverBD.iNumPedsSpawned++
															ENDIF
														ENDIF
														
														IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwelve, ciPed_BSTwelve_SpawnPedInCover)
															IF NOT IS_PED_IN_ANY_VEHICLE(tempPed)	
																VECTOR vCoverCoords = GET_ENTITY_COORDS(tempPed)
																PRINTLN("[LM][RCC MISSION] SET_UP_FMMC_PED_RC - Entering Cover.")
																COVERPOINT_INDEX cpIndex = GET_CLOSEST_COVER_POINT_TO_LOCATION(vCoverCoords)
																SET_PED_TO_LOAD_COVER(tempPed, TRUE)
																TASK_PUT_PED_DIRECTLY_INTO_COVER(tempPed, vCoverCoords, -1, TRUE, DEFAULT, TRUE, FALSE, cpIndex)
																FORCE_PED_AI_AND_ANIMATION_UPDATE(tempPed, TRUE)
															ENDIF
														ENDIF
														
														RETURN TRUE
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ELSE
										IF (NOT HAS_NET_TIMER_STARTED(MC_serverBD.tdSpawnAreaTimeout))
										OR GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdSpawnAreaTimeout) >= ciFMMCSpawnAreaTimeout
											
											PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED ",iped," - waited for everyone to reply back on entity area for over 10s, delete and try again")
											NETWORK_REMOVE_ENTITY_AREA(MC_serverBD.iSpawnArea)
											MC_serverBD.iSpawnArea = -1
											RESET_NET_TIMER(MC_serverBD.tdSpawnAreaTimeout)
											
										#IF IS_DEBUG_BUILD
										ELSE
											PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED ",iped," - waiting for everyone to reply back on entity area, waited for ",GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdSpawnAreaTimeout),"ms")
										#ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						#IF IS_DEBUG_BUILD
						ELSE
							PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - failing to spawn ped ",iped," because attempting to spawn ped ", MC_serverBD.isearchingforPed," / veh ", MC_serverBD.isearchingforVeh ," / obj ", MC_serverBD.isearchingforObj )
						#ENDIF
						ENDIF
					#IF IS_DEBUG_BUILD
					ELSE
						IF NOT CAN_REGISTER_MISSION_PEDS(1)
							PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - failing to spawn ped because of CAN_REGISTER_MISSION_PEDS ", iped)
						ENDIF
						IF ARE_WE_WAITING_FOR_PEDS_TO_CLEAN_UP()
							IF DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME(TRUE)
								PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - failing to spawn ped ",iped," because of ARE_WE_WAITING_FOR_PEDS_TO_CLEAN_UP: new priority / midpoint this frame")
							ENDIF
							IF IS_BIT_SET(iLocalBoolCheck7, LBOOL7_HOST_APPLIED_NEW_PRIORITY_MIDFRAME)
							OR IS_BIT_SET(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
								PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - failing to spawn ped ",iped," because of ARE_WE_WAITING_FOR_PEDS_TO_CLEAN_UP: new priority / midpoint set by host")
							ENDIF
							IF MC_serverBD.iPedCleanup_NeedOwnershipBS[0] != 0
							OR MC_serverBD.iPedCleanup_NeedOwnershipBS[1] != 0
							OR MC_serverBD.iPedCleanup_NeedOwnershipBS[2] != 0
								PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - failing to spawn ped ",iped," because of ARE_WE_WAITING_FOR_PEDS_TO_CLEAN_UP: iPedCleanup_NeedOwnershipBS[0] = ",MC_serverBD.iPedCleanup_NeedOwnershipBS[0],", iPedCleanup_NeedOwnershipBS[1] = ",MC_serverBD.iPedCleanup_NeedOwnershipBS[1],", MC_serverBD.iPedCleanup_NeedOwnershipBS[2] = ",MC_serverBD.iPedCleanup_NeedOwnershipBS[2])
							ENDIF
						ENDIF
					#ENDIF
					ENDIF // End of if we can register a new network ped/entity
				ENDIF // If the model name for this ped has loaded
			ELSE // iPedDelayRespawnBitset is set, or iPedSpawnFailDelay is running
				
				PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - Delay Respawn is set to: ", iPedSpawnFailDelay[iped], " for ped: ", iped)
				
				IF (iPedSpawnFailDelay[iped] >= 1000)
					iPedSpawnFailDelay[iped] = -1
					PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - Reached iPedSpawnFailDelay of 1 second, resetting spawn delay timer on ped ", iped)
				ELIF iPedSpawnFailDelay[iped] != -1
					iPedSpawnFailDelay[iped] += ROUND(fLastFrameTime * 1000)
				ENDIF
				
			ENDIF
		ENDIF // If this ped's model name isn't a dummy
	ELSE // Else this ped already exists
		RETURN TRUE
	ENDIF
	
	PRINTLN("[RCC MISSION] RESPAWN_MISSION_PED - Returning FALSE for Ped ", iped)
	
	RETURN FALSE

ENDFUNC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: OBJECT SPAWNING/CLEANUP !
//
//************************************************************************************************************************************************************



FUNC BOOL SHOULD_CLEANUP_OBJ(INT iobj, OBJECT_INDEX tempObj)

	IF GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
	AND IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedObject[ iobj ].iObjectBitSet, cibsOBJ_CleanupAtMissionEnd )
		RETURN TRUE
	ENDIF

	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iCleanupObjective !=-1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iCleanupTeam !=-1
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iCleanupObjective <= MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iCleanupTeam]
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitset,cibsOBJ_CleanupAtMidpoint)
					IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iCleanupTeam],g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iCleanupObjective)				
						IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iCleanupRange <= 0
							RETURN TRUE
						ELSE
							INT iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY(tempObj)
							PLAYER_INDEX ClosestPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
							PED_INDEX playerPed = GET_PLAYER_PED(ClosestPlayer)
							IF tempObj != NULL
								IF GET_DISTANCE_BETWEEN_ENTITIES(playerPed,tempObj) > g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iCleanupRange
									RETURN TRUE
								ENDIF
							ELSE
								IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(playerPed,g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vPos) > g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iCleanupRange
									RETURN TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					//Midpoint doesn't matter
					IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iCleanupRange <= 0
						RETURN TRUE
					ELSE
						INT iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY(tempObj)
						PLAYER_INDEX ClosestPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
						PED_INDEX playerPed = GET_PLAYER_PED(ClosestPlayer)
						IF tempObj != NULL
							IF GET_DISTANCE_BETWEEN_ENTITIES(playerPed,tempObj) > g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iCleanupRange
								RETURN TRUE
							ENDIF
						ELSE
							IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(playerPed,g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vPos) > g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iCleanupRange
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL SHOULD_OBJ_RESPAWN_NOW(INT i)

	IF IS_ANY_TEAM_ATTEMPTING_TO_RESTART_RULE()
		PRINTLN("[RCC MISSION] SHOULD_OBJ_RESPAWN_NOW - SBBOOL5_RETRY_CURRENT_RULE_TEAM (OBJECT ", i, ")")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_IN_BMBFB_SUDDEN_DEATH)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitSetThree, cibsOBJ3_SpawnInSuddenDeath)
			PRINTLN("[RCC MISSION] SHOULD_OBJ_RESPAWN_NOW - ciOptionsBS22_SpawnInSuddenDeath (OBJECT ", i, ")")
		ELSE
			IF IS_THIS_BOMB_FOOTBALL()
				PRINTLN("[RCC MISSION] SHOULD_OBJ_RESPAWN_NOW - Returning false because it's sudden death in Bomb Football and I'm not the final ball (OBJECT ", i, ")")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_THIS_BOMB_FOOTBALL()
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedObjective > -1
		AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedTeam > -1
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedObjective > MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedTeam]
				PRINTLN("[RCC MISSION] SHOULD_OBJ_RESPAWN_NOW - HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS")
				RETURN FALSE
			ENDIF
		ENDIF

		IF g_bMissionEnding
			PRINTLN("[RCC MISSION] SHOULD_OBJ_RESPAWN_NOW - Returning false because Bomb Football is ending (1) (OBJECT ", i, ")")
			RETURN FALSE
		ENDIF
		
		IF GET_TIME_REMAINING_ON_MULTIRULE_TIMER() < 150
			IF NOT ARE_MULTIPLE_TEAMS_ON_SAME_SCORE()
				PRINTLN("[RCC MISSION] SHOULD_OBJ_RESPAWN_NOW - Returning false because Bomb Football is ending (2) (OBJECT ", i, ")")
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF HAS_OBJECTIVE_TIMER_EXPIRED(0)
		OR HAS_OBJECTIVE_TIMER_EXPIRED(1)
		OR GET_REMAINING_TIME_ON_RULE(0, MC_serverBD_4.iCurrentHighestPriority[0]) < 3000
		OR GET_REMAINING_TIME_ON_RULE(1, MC_serverBD_4.iCurrentHighestPriority[1]) < 3000
			PRINTLN("[RCC MISSION] SHOULD_OBJ_RESPAWN_NOW - Returning false because the objective timer has expired or is nearly expired (OBJECT ", i, ") || GET_REMAINING_TIME_ON_RULE(0): ", GET_REMAINING_TIME_ON_RULE(0, MC_serverBD_4.iCurrentHighestPriority[0]))
			RETURN FALSE
		ENDIF
	ENDIF

	IF SHOULD_CLEANUP_OBJ(i,NULL)
		PRINTLN("[RCC MISSION] SHOULD_OBJ_RESPAWN_NOW - SHOULD_CLEANUP_OBJ (OBJECT ", i, ")")
		RETURN FALSE
	ENDIF
	
	IF IS_ANY_TEAM_BEING_WARPED_TO_START_POINT()
	AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_ALL_PLAYERS_RENDERPHASES_PAUSED)
		PRINTLN("[RCC MISSION] SHOULD_OBJ_RESPAWN_NOW - Waiting for renderphases to be paused (OBJECT ", i, ")")
		RETURN FALSE
	ENDIF
	
	IF DOES_OBJ_HAVE_PLAYER_VARIABLE_SETTING(i)
		IF IS_BIT_SET(MC_serverbd.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedObjective > -1
			AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedTeam > -1
				IF MC_serverBD_1.sCreatedCount.iNumObjCreated[g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedObjective] >= MC_serverBD.iNumberOfPart[g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedTeam]
					SET_BIT(MC_serverBD_1.sCreatedCount.iSkipObjBitset,i)
					PRINTLN("[RCC MISSION] SHOULD_OBJ_RESPAWN_NOW - DOES_OBJ_HAVE_PLAYER_VARIABLE_SETTING (OBJECT ", i, ")")
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	IF SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].eSpawnConditionFlag)
		PRINTLN("[RCC MISSION] SHOULD_VEH_RESPAWN_NOW veh ",i," - RETURNING FALSE - SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS")
		RETURN TRUE
	ENDIF
	
	INT iPlayersPlaying
	IF IS_BIT_SET(MC_serverbd.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
		iPlayersPlaying = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]
	ELSE
		iPlayersPlaying = MC_serverBD.iTotalNumStartingPlayers
	ENDIF
	IF NOT ARE_ENOUGH_PLAYERS_PLAYING_TO_SPAWN_OBJECT(i, iPlayersPlaying)
		PRINTLN("[RCC MISSION] SHOULD_OBJ_RESPAWN_NOW - Not enough players (OBJECT ", i, ")")
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(i, MC_serverBD_4.rsgSpawnSeed, eSGET_Object)
		PRINTLN("[RCC MISSION] SHOULD_OBJ_RESPAWN_NOW - No suitable spawn group (OBJECT ", i, ")")
		RETURN FALSE
	ENDIF
		
	IF IS_ENTITY_SPAWN_TRIGGERED_BY_ZONE_ENTRY(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iZoneBlockEntitySpawn)
		IF HAS_ENTITY_SPAWN_ZONE_REQUIREMENT_BEEN_MET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iZoneBlockEntitySpawn, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iZoneBlockEntitySpawnPlayerReq)
			PRINTLN("[RCC MISSION] SHOULD_OBJ_RESPAWN_NOW - Object: ", i, " - RETURN TRUE, HAS_ENTITY_SPAWN_ZONE_REQUIREMENT_BEEN_MET = TRUE.")
			RETURN TRUE
		ENDIF
		PRINTLN("[RCC MISSION] SHOULD_OBJ_RESPAWN_NOW - Object: ", i, " - RETURN FALSE, HAS_ENTITY_SPAWN_ZONE_REQUIREMENT_BEEN_MET = FALSE.. iZone: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iZoneBlockEntitySpawn, " iPlayerReq: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iZoneBlockEntitySpawnPlayerReq)
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedObjective)
			RETURN TRUE
		ENDIF
	ELSE
		PRINTLN("[RCC MISSION] SHOULD_OBJ_RESPAWN_NOW - Object SHOULD SPAWN: ",i," SINCE NO ASSOCIATED RULES (OBJECT ", i, ")")
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSecondAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSecondAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSecondAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSecondAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSecondAssociatedObjective)
			PRINTLN("SHOULD_OBJ_RESPAWN_NOW - TRUE HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS (1) (OBJECT ", i, ")")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iThirdAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iThirdAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iThirdAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iThirdAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iThirdAssociatedObjective)
			PRINTLN("SHOULD_OBJ_RESPAWN_NOW - TRUE HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS (2) (OBJECT ", i, ")")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iFourthAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iFourthAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iFourthAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iFourthAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iFourthAssociatedObjective)
			PRINTLN("SHOULD_OBJ_RESPAWN_NOW - TRUE HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS (3) (OBJECT ", i, ")")
			RETURN TRUE
		ENDIF
	ENDIF
	
	PRINTLN("[OBJSPAM] - (OBJECT ", i, ") || SHOULD_OBJ_RESPAWN_NOW returning FALSE")
	RETURN FALSE

ENDFUNC

FUNC BOOL CAN_OBJ_STILL_SPAWN_IN_MISSION(INT iobj, INT iTeamOverride = -1, INT iRuleOverride = -1)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAssociatedTeam > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAssociatedSpawn = ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY
			
			IF iTeamOverride = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAssociatedTeam
			AND iRuleOverride != -1
				IF iRuleOverride <= g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAssociatedObjective
					// The object will have a chance to spawn:
					RETURN TRUE
				ENDIF
			ELSE
				IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAssociatedTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAssociatedObjective
					// The object still has a chance to spawn:
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE // Spawn type won't prevent object from spawning after the rule
			RETURN TRUE
		ENDIF
	ELSE
		//Object will just spawn at mission start!
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iSecondAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iSecondAssociatedTeam > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iSecondAssociatedSpawn = ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY
			IF iTeamOverride = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iSecondAssociatedTeam
			AND iRuleOverride != -1
				IF iRuleOverride <= g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iSecondAssociatedObjective
					RETURN TRUE
				ENDIF
			ELSE
				IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iSecondAssociatedTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iSecondAssociatedObjective
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iThirdAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iThirdAssociatedTeam > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iThirdAssociatedSpawn = ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY
			IF iTeamOverride = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iThirdAssociatedTeam
			AND iRuleOverride != -1
				IF iRuleOverride <= g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iThirdAssociatedObjective
					RETURN TRUE
				ENDIF
			ELSE
				IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iThirdAssociatedTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iThirdAssociatedObjective
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iFourthAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iFourthAssociatedTeam > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iFourthAssociatedSpawn = ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY
			IF iTeamOverride = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iFourthAssociatedTeam
			AND iRuleOverride != -1
				IF iRuleOverride <= g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iFourthAssociatedObjective
					RETURN TRUE
				ENDIF
			ELSE
				IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iFourthAssociatedTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iFourthAssociatedObjective
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC VECTOR GET_OBJ_FMMC_SPAWN_LOCATION(INT iObj, BOOL& bForceAreaCheck)
	
	VECTOR vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].vPos
	
	bForceAreaCheck = FALSE
	
	IF IS_BIT_SET(MC_serverBD_2.iObjSpawnedOnce, iobj)
	AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].vSecondSpawnPos)
		IF GET_RANDOM_INT_IN_RANGE(0, 100) < 50
			SET_BIT(iLocalBoolCheck10, LBOOL10_OBJ_SPAWN_AT_SECOND_POSITION)
			vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].vSecondSpawnPos
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iSpawnNearType != ciRULE_TYPE_NONE
	AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iSpawnNearEntityID > -1
		PRINTLN("[JS] [SPAWNNEAR] - GET_OBJ_FMMC_SPAWN_LOCATION - 1")
		IF (NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitset, cibsOBJ_FirstSpawnNotNearEntity))
		OR IS_BIT_SET(MC_serverBD_2.iObjSpawnedOnce, iobj)
			PRINTLN("[JS] [SPAWNNEAR] - GET_OBJ_FMMC_SPAWN_LOCATION - 2")
			VECTOR vNewSpawn = GET_FMMC_ENTITY_LOCATION(vSpawnLoc, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iSpawnNearType, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iSpawnNearEntityID)
			
			IF NOT IS_VECTOR_ZERO(vNewSpawn)
				vSpawnLoc = vNewSpawn
				PRINTLN("[JS] [SPAWNNEAR] - GET_OBJ_FMMC_SPAWN_LOCATION - 3 vSpawnLoc = ", vSpawnLoc)
				bForceAreaCheck = TRUE
			ENDIF
		ENDIF
		
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitsetFour, cibsOBJ4_UseRandomPoolPosAsRespawnSpawn)	
		INT iSpawnPool = GET_RANDOM_SELECTION_FROM_SPAWN_POOL(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sWarpLocationSettings.vPosition)
		vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sWarpLocationSettings.vPosition[iSpawnPool]
		PRINTLN( "[RCC MISSION] GET_OBJ_FMMC_SPAWN_LOCATION - Grabbing random position from pool for Obj ", iObj, " iSpawnPool: ", iSpawnPool, " vSpawnLoc: ", vSpawnLoc)
	ENDIF
	
	RETURN vSpawnLoc
	
ENDFUNC

FUNC BOOL ARE_WE_WAITING_FOR_OBJECTS_TO_CLEAN_UP()
	
	BOOL bWaiting
	
	IF DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME(TRUE)
	OR IS_BIT_SET(iLocalBoolCheck7, LBOOL7_HOST_APPLIED_NEW_PRIORITY_MIDFRAME)
	OR IS_BIT_SET(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
	OR MC_serverBD.iObjCleanup_NeedOwnershipBS != 0
		bWaiting = TRUE
	ENDIF
	
	RETURN bWaiting
	
ENDFUNC

FUNC BOOL RESPAWN_MISSION_OBJ(INT iobj)

	BOOL bPedSpawn, bOnGround, bVisible
	FLOAT fareaSize,fspawnhead
	BOOL bForceAreaCheck
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn != DUMMY_MODEL_FOR_SCRIPT
			REQUEST_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn)
			IF HAS_MODEL_LOADED(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn)
				IF CAN_REGISTER_MISSION_OBJECTS(1)
				AND NOT ARE_WE_WAITING_FOR_OBJECTS_TO_CLEAN_UP()
					IF (MC_serverBD.isearchingforPed = -1 AND MC_serverBD.isearchingforObj = -1 AND MC_serverBD.isearchingforVeh = -1)
					OR (MC_serverBD.isearchingforObj = iobj AND MC_serverBD.isearchingforPed = -1 AND MC_serverBD.isearchingforVeh = -1)
						
						IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectAttachmentPed >= 0
							IF NOT IS_BIT_SET(MC_serverBD.iObjSpawnPedBitset[GET_LONG_BITSET_INDEX(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectAttachmentPed)], GET_LONG_BITSET_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectAttachmentPed))
								PRINTLN("[RCC MISSION] RESPAWN_MISSION_OBJ - calling create respawn obj on ped but ped not created so waiting for obj: ", iobj, " on ped: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectAttachmentPed)
								vEntityRespawnLoc = <<0, 0, 0>>
								RETURN FALSE
							ENDIF
							IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectAttachmentPed])
								
								PRINTLN("[RCC MISSION] RESPAWN_MISSION_OBJ - Object ",iobj," needs to be placed on ped ",g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectAttachmentPed, ", and they exist so this is fine")
								bPedSpawn = TRUE
								
							#IF IS_DEBUG_BUILD
							ELSE
								PRINTLN("[RCC MISSION] RESPAWN_MISSION_OBJ - Object ",iobj," needs to be placed on ped ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectAttachmentPed,", but they are injured or don't exist...")
							#ENDIF
							ENDIF
						ENDIF
						
						MC_serverBD.isearchingforObj = iobj
						IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn = PROP_CONTR_03B_LD
						OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn = Prop_Container_LD_PU 
							fareaSize = 8.0
						ELSE
							fareaSize = 0.5
						ENDIF
						
						IF bPedSpawn
						OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSet, cibsOBJ_Airborne)
							bOnGround = FALSE
						ELSE
							bOnGround = TRUE
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetTwo, cibsOBJ2_SetInvisible)
						OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciDAWN_RAID_ENABLE_MODE)
						AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn = INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_adv_hdsec")))
							bVisible = FALSE
						ELSE
							bVisible = TRUE
						ENDIF
						
						IF iEntityRespawnType != ci_TARGET_OBJECT
						OR iEntityRespawnID != iobj
							vEntityRespawnLoc = <<0, 0, 0>>
							iEntityRespawnType = ci_TARGET_OBJECT
							iEntityRespawnID = iobj
						ENDIF
						
						IF IS_VECTOR_ZERO(vEntityRespawnLoc)
							PRINTLN("[JS] [SPAWNNEAR] - RESPAWN_MISSION_OBJ - 1")
							CLEAR_BIT(iLocalBoolCheck10, LBOOL10_OBJ_SPAWN_AT_SECOND_POSITION)
							vEntityRespawnLoc = GET_OBJ_FMMC_SPAWN_LOCATION(iobj, bForceAreaCheck)
							IF bForceAreaCheck
								SET_BIT(iLocalBoolCheck4, LBOOL4_RESPAWN_SHOULD_FORCE_AREA_CHECK)
							ELSE
								CLEAR_BIT(iLocalBoolCheck4, LBOOL4_RESPAWN_SHOULD_FORCE_AREA_CHECK)
							ENDIF
						ELSE
							PRINTLN("[JS] [SPAWNNEAR] - RESPAWN_MISSION_OBJ - 2")
							bForceAreaCheck = IS_BIT_SET(iLocalBoolCheck4, LBOOL4_RESPAWN_SHOULD_FORCE_AREA_CHECK)
						ENDIF
						
						IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent >= 0
						AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParentType = CREATION_TYPE_VEHICLES
							IF NOT IS_BIT_SET(MC_serverBD.iVehSpawnBitset, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent)
								PRINTLN("[RCC MISSION] RESPAWN_MISSION_OBJ - calling create respawn obj in vehicle but vehicle not created so waiting for obj: ",iobj," on vehicle: ",g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent)
								vEntityRespawnLoc = <<0, 0, 0>>
								RETURN FALSE
							ENDIF
							bForceAreaCheck = TRUE
						ENDIF
						
						IF NOT bForceAreaCheck
						AND NOT NETWORK_ENTITY_AREA_DOES_EXIST(MC_serverBD.iSpawnArea)
							MC_serverBD.iSpawnArea = NETWORK_ADD_ENTITY_AREA((vEntityRespawnLoc - <<fareaSize,fareaSize,fareaSize>>),(vEntityRespawnLoc + <<fareaSize,fareaSize,fareaSize>>))
							REINIT_NET_TIMER(MC_serverBD.tdSpawnAreaTimeout)
							PRINTLN("[RCC MISSION] RESPAWN_MISSION_OBJ - adding spawn area for object ",iobj)
						ELSE
							IF bForceAreaCheck
							OR NETWORK_ENTITY_AREA_HAVE_ALL_REPLIED(MC_serverBD.iSpawnArea)
								IF NOT bForceAreaCheck
								AND NOT NETWORK_ENTITY_AREA_IS_OCCUPIED(MC_serverBD.iSpawnArea)
									IF NOT IS_MINI_GAME_ACTIVE_ON_THIS_OBJECT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet)
									AND SHOULD_PLACED_OBJECT_BE_PORTABLE(iobj)
										PRINTLN("[RCC MISSION] RESPAWN_MISSION_OBJ - calling create respawn object at original coords: ",iobj,"at coords: ",vEntityRespawnLoc)
										// Creates the pickup
										MC_serverBD_1.sFMMC_SBD.niObject[iobj] = CREATE_PORTABLE_PICKUP_BASED_ON_SETTINGS( iObj, bOnGround, vEntityRespawnLoc )
										
										SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(MC_serverBD_1.sFMMC_SBD.niObject[iobj],TRUE)
										SET_UP_FMMC_OBJ_RC(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj]), iobj,MC_ServerBD_1.sFMMC_SBD.niPed, MC_ServerBD_1.sLEGACYMissionContinuityVars, bOnGround, bVisible, IS_BIT_SET(iLocalBoolCheck10, LBOOL10_OBJ_SPAWN_AT_SECOND_POSITION) )
										SET_MODEL_AS_NO_LONGER_NEEDED( g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn)
										NETWORK_REMOVE_ENTITY_AREA(MC_serverBD.iSpawnArea)
										RESET_NET_TIMER(MC_serverBD.tdSpawnAreaTimeout)
										IF bPedSpawn
											ATTACH_PORTABLE_PICKUP_TO_PED(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj]), NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectAttachmentPed]))
											SET_BIT(iAICarryBitset,iobj)
										ENDIF
										
										PROCESS_REPOSITION_OBJ_BASED_ON_SUB_SPAWN_GROUP_SETTINGS(iObj, NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj]), MC_serverBD_4.rsgSpawnSeed)
										
										IF NOT IS_THIS_A_SPECIAL_PICKUP(iobj)
											INT iTeamLoop
											FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
												IF MC_serverBD_4.iObjPriority[iobj][iTeamLoop] > MC_serverBD_4.iCurrentHighestPriority[iTeamLoop]
												OR NOT DOES_OBJECT_RULE_TYPE_REQUIRE_PICKUP(MC_serverBD_4.iObjRule[iobj][iTeamLoop])
												OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciDAWN_RAID_ENABLE_MODE)
												AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn = INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_adv_hdsec")))
													PRINTLN("[RCC MISSION] RESPAWN_MISSION_OBJ - respawn object ",iobj," doesn't require pickup for team ",iTeamLoop)
													SET_TEAM_PICKUP_OBJECT(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj]), iTeamLoop, FALSE)
												ENDIF
											ENDFOR
										ENDIF
										
										IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent != -1
											IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent])
												IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent]))
													ATTACH_OBJECT_TO_FLATBED(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj]),NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent]), g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vAttachOffset, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vAttachOffsetRotation, TRUE)
													PRINTLN("[RCC MISSION] RESPAWN_MISSION_OBJ - Object ",iobj, " successfully attached to vehicle ",g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent)
												ELSE
													PRINTLN("[RCC MISSION] RESPAWN_MISSION_OBJ - Unable to attach object ", iobj, " to vehicle ",g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent,", as the vehicle is not driveable!")
												ENDIF
											ELSE
												PRINTLN("[RCC MISSION] RESPAWN_MISSION_OBJ - Unable to attach object ", iobj, " to vehicle ",g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent,", as the vehicle does not exist!")
											ENDIF
										ENDIF
										
										PRINTLN("[RCC MISSION] RESPAWN_MISSION_OBJ -     ---------->  RESPAWN PICKUP OBJECT CREATED    <----------     ", "FMMC: ",iobj)
										MC_serverBD.iSpawnArea = -1
										MC_serverBD.isearchingforObj = -1
										
										vEntityRespawnLoc = <<0, 0, 0>>
										
										RETURN TRUE
									ELSE
										IF CREATE_NET_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj],g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn , vEntityRespawnLoc)
											
											SET_UP_FMMC_MINI_GAME_OBJECT_RC(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj]), iobj, bVisible, IS_BIT_SET(iLocalBoolCheck10, LBOOL10_OBJ_SPAWN_AT_SECOND_POSITION) )
											
											IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_MinigameObjAttachToPed)
												IF bPedSpawn
													IF DOES_ENTITY_EXIST(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectAttachmentPed]))
													AND DOES_ENTITY_EXIST(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj]))
														ATTACH_ENTITY_TO_ENTITY(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj]), NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectAttachmentPed]), -1, <<0,0,0>>, <<0,0,0>>)
														SET_BIT(iAICarryBitset,iobj)
													ENDIF
												ENDIF
											ENDIF
											
											CLEAR_BIT(MC_serverBD_3.iBombFB_ExplodedBS, iObj)
											CLEAR_BIT(MC_serverBD_3.iBombFB_GoalProcessedBS, iObj)
											PRINTLN("[TMS][BMBFB] Clearing iBombFB_ExplodedBS and iBombFB_GoalProcessedBS for bomb ", iObj)
											
											IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn = Prop_Contr_03b_LD
											OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn = prop_container_ld_pu
												SET_ENTITY_LOD_DIST(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj]),500)
											ENDIF
											
											IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent != -1
												IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent])
													IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent]))
														ATTACH_OBJECT_TO_FLATBED(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj]),NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent]), g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vAttachOffset, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vAttachOffsetRotation, TRUE)
														PRINTLN("[RCC MISSION] RESPAWN_MISSION_OBJ - Object ",iobj, " successfully attached to vehicle ",g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent)
													ELSE
														PRINTLN("[RCC MISSION] RESPAWN_MISSION_OBJ - Unable to attach object ", iobj, " to vehicle ",g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent,", as the vehicle is not driveable!")
													ENDIF
												ELSE
													PRINTLN("[RCC MISSION] RESPAWN_MISSION_OBJ - Unable to attach object ", iobj, " to vehicle ",g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent,", as the vehicle does not exist!")
												ENDIF
											ENDIF
											
											PROCESS_REPOSITION_OBJ_BASED_ON_SUB_SPAWN_GROUP_SETTINGS(iObj, NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj]), MC_serverBD_4.rsgSpawnSeed)
											
											SET_UP_TROLLEY_STARTING_AMOUNT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn, NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj]))											
											
											SET_MODEL_AS_NO_LONGER_NEEDED( g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn)
											
											PRINTLN("[RCC MISSION] RESPAWN_MISSION_OBJ -      ---------->  RESPAWN   FMMC MINI GAME OBJECT CREATED    <----------     ", "FMMC: ",iobj)
											NETWORK_REMOVE_ENTITY_AREA(MC_serverBD.iSpawnArea)
											RESET_NET_TIMER(MC_serverBD.tdSpawnAreaTimeout)
											MC_serverBD.iSpawnArea = -1
											MC_serverBD.isearchingforObj = -1
											
											vEntityRespawnLoc = <<0, 0, 0>>
											
											RETURN TRUE
										ELSE
											PRINTLN("[RCC MISSION] RESPAWN_MISSION_OBJECT - Failing to CREATE_NET_OBJ 1 for object ",iobj)
										ENDIF
									ENDIF
								ELSE
									
									SPAWN_SEARCH_PARAMS SpawnSearchParams
									SpawnSearchParams.bConsiderInteriors = TRUE
									SpawnSearchParams.fMinDistFromPlayer = 2
									
									FLOAT fSearchRadius = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iobj].fRespawnRange
									
									IF bForceAreaCheck
										SpawnSearchParams.vFacingCoords = vEntityRespawnLoc
										//Want to spawn outside of a range from vEntityRespawnLoc:
										SpawnSearchParams.vAvoidCoords[0] = vEntityRespawnLoc
										SpawnSearchParams.fAvoidRadius[0] = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].fRespawnMinRange
										
										SpawnSearchParams.bConsiderOriginAsValidPoint = FALSE
										SpawnSearchParams.bCloseToOriginAsPossible = FALSE																				
										IF (fSearchRadius < SpawnSearchParams.fAvoidRadius[0])
											fSearchRadius += 10.0
											PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE - fSearchRadius was smaller than avoid radius, increasing.")
										ENDIF
									ENDIF
									
									IF GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY(vEntityRespawnLoc,g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].fRespawnRange,vEntityRespawnLoc,fspawnhead,SpawnSearchParams)
										IF NOT IS_MINI_GAME_ACTIVE_ON_THIS_OBJECT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet)
										AND SHOULD_PLACED_OBJECT_BE_PORTABLE(iobj)
										
											PRINTLN("[RCC MISSION] RESPAWN_MISSION_OBJ - calling create respawn object at new coords: ",iobj,"at coords: ",vEntityRespawnLoc )
											// Creates the pickup
											MC_serverBD_1.sFMMC_SBD.niObject[iobj] = CREATE_PORTABLE_PICKUP_BASED_ON_SETTINGS( iObj, bOnGround, vEntityRespawnLoc )
											
											SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(MC_serverBD_1.sFMMC_SBD.niObject[iobj],TRUE)
											
											SET_UP_FMMC_OBJ_RC(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj]), iobj,MC_ServerBD_1.sFMMC_SBD.niPed, MC_ServerBD_1.sLEGACYMissionContinuityVars, bOnGround, bVisible, IS_BIT_SET(iLocalBoolCheck10, LBOOL10_OBJ_SPAWN_AT_SECOND_POSITION) )
										
											SET_MODEL_AS_NO_LONGER_NEEDED( g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn)
											NETWORK_REMOVE_ENTITY_AREA(MC_serverBD.iSpawnArea)
											RESET_NET_TIMER(MC_serverBD.tdSpawnAreaTimeout)
											IF bPedSpawn
												ATTACH_PORTABLE_PICKUP_TO_PED(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj]),NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectAttachmentPed]))
												SET_BIT(iAICarryBitset,iobj)
											ENDIF
											
											IF NOT IS_THIS_A_SPECIAL_PICKUP(iobj)
												INT iTeamLoop
												FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
													IF MC_serverBD_4.iObjPriority[iobj][iTeamLoop] > MC_serverBD_4.iCurrentHighestPriority[iTeamLoop]
													OR NOT DOES_OBJECT_RULE_TYPE_REQUIRE_PICKUP(MC_serverBD_4.iObjRule[iobj][iTeamLoop])
													OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciDAWN_RAID_ENABLE_MODE)
													AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn = INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_adv_hdsec")))
														PRINTLN("[RCC MISSION] RESPAWN_MISSION_OBJ - respawn object ",iobj," doesn't require pickup for team ",iTeamLoop)
														SET_TEAM_PICKUP_OBJECT(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj]), iTeamLoop, FALSE)
													ENDIF
												ENDFOR
											ENDIF
											
											IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent != -1
												IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent])
													IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent]))
														ATTACH_OBJECT_TO_FLATBED(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj]),NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent]),g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vAttachOffset, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vAttachOffsetRotation, TRUE)
														PRINTLN("[RCC MISSION] RESPAWN_MISSION_OBJ - Object ",iobj, " successfully attached to vehicle ",g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent)
													ELSE
														PRINTLN("[RCC MISSION] RESPAWN_MISSION_OBJ - Unable to attach object ", iobj, " to vehicle ",g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent,", as the vehicle is not driveable!")
													ENDIF
												ELSE
													PRINTLN("[RCC MISSION] RESPAWN_MISSION_OBJ - Unable to attach object ", iobj, " to vehicle ",g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent,", as the vehicle does not exist!")
												ENDIF
											ENDIF
											
											PROCESS_REPOSITION_OBJ_BASED_ON_SUB_SPAWN_GROUP_SETTINGS(iObj, NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj]), MC_serverBD_4.rsgSpawnSeed)
											
											PRINTLN("[RCC MISSION] RESPAWN_MISSION_OBJ -    ---------->  RESPAWN PICKUP OBJECT CREATED    <----------     ", "FMMC: ",iobj)
											MC_serverBD.iSpawnArea = -1
											MC_serverBD.isearchingforObj = -1
											
											vEntityRespawnLoc = <<0, 0, 0>>
											
											RETURN TRUE
										ELSE
											IF CREATE_NET_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj],g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn , vEntityRespawnLoc)
																								
												SET_UP_FMMC_MINI_GAME_OBJECT_RC(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj]), iobj, bVisible, IS_BIT_SET(iLocalBoolCheck10, LBOOL10_OBJ_SPAWN_AT_SECOND_POSITION) )
												IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn = Prop_Contr_03b_LD
												OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn = prop_container_ld_pu
													SET_ENTITY_LOD_DIST(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj]),500)
												ENDIF
																							
												IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent != -1
													IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent])
														IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent]))
															ATTACH_OBJECT_TO_FLATBED(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj]),NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent]), g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vAttachOffset, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vAttachOffsetRotation, TRUE)
															PRINTLN("[RCC MISSION] RESPAWN_MISSION_OBJ - Object ",iobj, " successfully attached to vehicle ",g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent)
														ELSE
															PRINTLN("[RCC MISSION] RESPAWN_MISSION_OBJ - Unable to attach object ", iobj, " to vehicle ",g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent,", as the vehicle is not driveable!")
														ENDIF
													ELSE
														PRINTLN("[RCC MISSION] RESPAWN_MISSION_OBJ - Unable to attach object ", iobj, " to vehicle ",g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAttachParent,", as the vehicle does not exist!")
													ENDIF
												ENDIF
												
												PROCESS_REPOSITION_OBJ_BASED_ON_SUB_SPAWN_GROUP_SETTINGS(iObj, NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj]), MC_serverBD_4.rsgSpawnSeed)
												
												SET_UP_TROLLEY_STARTING_AMOUNT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn, NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj]))
												
												SET_MODEL_AS_NO_LONGER_NEEDED( g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn)
												
												PRINTLN("    ---------->  RESPAWN_MISSION_OBJ -    FMMC MINI GAME OBJECT CREATED    <----------     ", "FMMC: ",iobj)
												NETWORK_REMOVE_ENTITY_AREA(MC_serverBD.iSpawnArea)
												RESET_NET_TIMER(MC_serverBD.tdSpawnAreaTimeout)
												MC_serverBD.iSpawnArea = -1
												MC_serverBD.isearchingforObj = -1
												
												vEntityRespawnLoc = <<0, 0, 0>>
												
												RETURN TRUE
											#IF IS_DEBUG_BUILD
											ELSE
												PRINTLN("[RCC MISSION] RESPAWN_MISSION_OBJ - Failing to CREATE_NET_OBJ 1 for object ",iobj)
											#ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF (NOT HAS_NET_TIMER_STARTED(MC_serverBD.tdSpawnAreaTimeout))
								OR GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdSpawnAreaTimeout) >= ciFMMCSpawnAreaTimeout
									
									PRINTLN("[RCC MISSION] RESPAWN_MISSION_OBJ ",iobj," - waited for everyone to reply back on entity area for over 10s, delete and try again")
									NETWORK_REMOVE_ENTITY_AREA(MC_serverBD.iSpawnArea)
									MC_serverBD.iSpawnArea = -1
									RESET_NET_TIMER(MC_serverBD.tdSpawnAreaTimeout)
									
								#IF IS_DEBUG_BUILD
								ELSE
									PRINTLN("[RCC MISSION] RESPAWN_MISSION_OBJ ",iobj," - waiting for everyone to reply back on entity area, waited for ",GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdSpawnAreaTimeout),"ms")
								#ENDIF
								ENDIF
							ENDIF
						ENDIF
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("[RCC MISSION] RESPAWN_MISSION_OBJ - failing to spawn obj ",iobj," because attempting to spawn ped ", MC_serverBD.isearchingforPed," / veh ", MC_serverBD.isearchingforVeh ," / obj ", MC_serverBD.isearchingforObj )
					#ENDIF
					ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					IF NOT CAN_REGISTER_MISSION_OBJECTS(1)
						PRINTLN("[RCC MISSION] RESPAWN_MISSION_OBJ - failing on CAN_REGISTER_MISSION_OBJECTS for object ",iobj)
					ENDIF
					IF ARE_WE_WAITING_FOR_OBJECTS_TO_CLEAN_UP()
						IF DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME(TRUE)
							PRINTLN("[RCC MISSION] RESPAWN_MISSION_OBJ - failing to spawn obj ",iobj," because of ARE_WE_WAITING_FOR_OBJECTS_TO_CLEAN_UP: new priority / midpoint this frame")
						ENDIF
						IF IS_BIT_SET(iLocalBoolCheck7, LBOOL7_HOST_APPLIED_NEW_PRIORITY_MIDFRAME)
						OR IS_BIT_SET(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
							PRINTLN("[RCC MISSION] RESPAWN_MISSION_OBJ - failing to spawn obj ",iobj," because of ARE_WE_WAITING_FOR_OBJECTS_TO_CLEAN_UP: new priority / midpoint set by host")
						ENDIF
						IF MC_serverBD.iObjCleanup_NeedOwnershipBS != 0
							PRINTLN("[RCC MISSION] RESPAWN_MISSION_OBJ - failing to spawn obj ",iobj," because of ARE_WE_WAITING_FOR_OBJECTS_TO_CLEAN_UP: iObjCleanup_NeedOwnershipBS = ",MC_serverBD.iObjCleanup_NeedOwnershipBS)
						ENDIF
					ENDIF
				#ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
				

	RETURN FALSE

ENDFUNC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: VEHICLE SPAWNING/CLEANUP !
//
//************************************************************************************************************************************************************

FUNC BOOL SHOULD_RENDERPHASE_RESET_WAIT_FOR_VEHICLE_RESPAWN(INT i)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedObjective)
			PRINTLN("[RCC MISSION] SHOULD_RENDERPHASE_RESET_WAIT_FOR_VEHICLE_RESPAWN veh ",i," - RETURNING TRUE - Team has reached entity spawn conditions (1)")
			RETURN TRUE
		ENDIF
	ELSE
		//PRINTLN("[RCC MISSION] Vehicle SHOULD SPAWN: ",i," SINCE NO ASSOCIATED RULES: ") 
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSecondAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSecondAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSecondAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSecondAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSecondAssociatedObjective)
			PRINTLN("[RCC MISSION] SHOULD_RENDERPHASE_RESET_WAIT_FOR_VEHICLE_RESPAWN veh ",i," - RETURNING TRUE - Team has reached entity spawn conditions (2)")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iThirdAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iThirdAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iThirdAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iThirdAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iThirdAssociatedObjective)
			PRINTLN("[RCC MISSION] SHOULD_RENDERPHASE_RESET_WAIT_FOR_VEHICLE_RESPAWN veh ",i," - RETURNING TRUE - Team has reached entity spawn conditions (3)")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iFourthAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iFourthAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iFourthAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iFourthAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iFourthAssociatedObjective)
			PRINTLN("[RCC MISSION] SHOULD_RENDERPHASE_RESET_WAIT_FOR_VEHICLE_RESPAWN veh ",i," - RETURNING TRUE - Team has reached entity spawn conditions (4)")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_VEH_RESPAWN_NOW( INT i )
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		PRINTLN("[RCC MISSION] SHOULD_VEH_RESPAWN_NOW veh ",i," - SBBOOL_MISSION_OVER")
		RETURN FALSE
	ENDIF
	
	IF IS_ANY_TEAM_ATTEMPTING_TO_RESTART_RULE()
		PRINTLN("[RCC MISSION] SHOULD_VEH_RESPAWN_NOW veh ",i," - SBBOOL5_RETRY_CURRENT_RULE_TEAM")
		RETURN FALSE
	ENDIF
	
	IF IS_ANY_TEAM_BEING_WARPED_TO_START_POINT()
	AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_ALL_PLAYERS_RENDERPHASES_PAUSED)
		PRINTLN("[RCC MISSION] SHOULD_VEH_RESPAWN_NOW veh ",i," - Waiting for renderphases to be paused")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_VehicleDestroyedTracking)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iContinuityId > -1
		AND IS_BIT_SET(MC_serverBD_1.sLEGACYMissionContinuityVars.iVehicleDestroyedTrackingBitset, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iContinuityId)
			PRINTLN("[JS][CONTINUITY] - SHOULD_VEH_RESPAWN_NOW - Veh ", i, " with continuity ID ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iContinuityId, " not spawning due to death of previous stand mission")
			RETURN FALSE
		ENDIF
	ENDIF

	IF SHOULD_CLEANUP_VEH(i, NULL)
		PRINTLN("[RCC MISSION] SHOULD_VEH_RESPAWN_NOW ",i," - Should be cleaning up")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetSeven, ciFMMC_VEHICLE7_SPAWN_WITH_LINKED_VEH)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iLinkedDestroyVeh  != -1
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn = TRAILERSMALL2
				IF NOT IS_NET_VEHICLE_DRIVEABLE(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iLinkedDestroyVeh])
					PRINTLN("[RCC MISSION] SHOULD_VEH_RESPAWN_NOW ",i," - Waiting for linked vehicle")
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetSeven, ciFMMC_VEHICLE7_ONLY_RESPAWN_WITH_PEDS)
	AND IS_BIT_SET(MC_serverBD_2.iVehSpawnedOnce, i) // Vehicle has already spawned once
		INT iped
		FOR iped = 0 TO (MC_serverBD.iNumPedCreated  - 1)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle = i
			AND SHOULD_PED_SPAWN_IN_VEHICLE(iped)
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iped])
					PRINTLN("[RCC MISSION] SHOULD_VEH_RESPAWN_NOW veh ",i," - RETURNING FALSE - Linked ped ",iped," still exists")
					RETURN FALSE
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	IF DOES_VEH_HAVE_PLAYER_VARIABLE_SETTING(i)
		IF IS_BIT_SET(MC_serverbd.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedObjective > -1
			AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedTeam > -1
				IF MC_serverBD_1.sCreatedCount.iNumVehCreated[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedObjective] >= MC_serverBD.iNumberOfPart[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedTeam]
					SET_BIT(MC_serverBD_1.sCreatedCount.iSkipVehBitset,i)
					PRINTLN("[RCC MISSION] SHOULD_VEH_RESPAWN_NOW veh ",i," - RETURNING FALSE - DOES_VEH_HAVE_PLAYER_VARIABLE_SETTING")
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	INT iPlayersPlaying
	IF IS_BIT_SET(MC_serverbd.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
		iPlayersPlaying = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]
		PRINTLN("[RCC MISSION] SHOULD_VEH_RESPAWN_NOW: iPlayersPlaying: ", iPlayersPlaying)
	ELSE
		iPlayersPlaying = MC_serverBD.iTotalNumStartingPlayers
		PRINTLN("[RCC MISSION] SHOULD_VEH_RESPAWN_NOW: MC_serverBD.iTotalNumStartingPlayers", MC_serverBD.iTotalNumStartingPlayers)
	ENDIF
	
	BOOL bCheckTeams
	INT iTeamToCheck
	IF (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iTeamSpawnReq != -1)
		bCheckTeams = TRUE
		iTeamToCheck = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iTeamSpawnReq
	ENDIF
	
	IF NOT ARE_ENOUGH_PLAYERS_PLAYING_TO_SPAWN_VEHICLE(i, iPlayersPlaying, bCheckTeams, MC_serverBD.iNumberOfPlayingPlayers[iTeamToCheck])
		PRINTLN("[RCC MISSION] SHOULD_VEH_RESPAWN_NOW veh ",i," - RETURNING FALSE - ARE_ENOUGH_PLAYERS_PLAYING_TO_SPAWN_VEHICLE")
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(i, MC_serverBD_4.rsgSpawnSeed, eSGET_Vehicle)
		PRINTLN("[RCC MISSION] SHOULD_VEH_RESPAWN_NOW veh ",i," - RETURNING FALSE - Vehicle does not have suitable spawn group")
		RETURN FALSE
	ENDIF
	
	IF BLOCK_SPAWN_FOR_VEHICLE_MISSION_OVERRIDE(i)
		PRINTLN("[RCC MISSION] SHOULD_VEH_RESPAWN_NOW veh ",i," - RETURNING FALSE - BLOCK_SPAWN_FOR_VEHICLE_MISSION_OVERRIDE")
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iCSRespawnSceneIndex > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iCSRespawnShotIndex > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iCSRespawnSceneIndex = MC_serverBD.iSpawnScene
		AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iCSRespawnShotIndex <= MC_serverBD.iSpawnShot
		AND (iCamShotLoaded >= g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iCSRespawnShotIndex AND IS_BIT_SET(iLocalBoolCheck7, LBOOL7_CURRENT_SCRIPTED_CAM_AREA_LOADED))
			RETURN TRUE
		ELSE
			PRINTLN("[RCC MISSION] SHOULD_VEH_RESPAWN_NOW veh ",i," - RETURNING FALSE - iCSRespawnSceneIndex: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iCSRespawnSceneIndex, " SpawnScene: ", MC_serverBD.iSpawnScene,
					" iCSRespawnShotIndex: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iCSRespawnShotIndex, " SpawnShot: ", MC_serverBD.iSpawnShot, 
					" Camshot: ", iCamShotLoaded, " LBOOL7_CURRENT_SCRIPTED_CAM_AREA_LOADED: ", IS_BIT_SET(iLocalBoolCheck7, LBOOL7_CURRENT_SCRIPTED_CAM_AREA_LOADED))
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMBUSHKA(g_FMMC_STRUCT.iAdversaryModeType)
		IF IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_TEAM_0_SHOULD_RESPAWN_BACK_AT_START_NOW)
		AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_ALL_PLAYERS_RENDERPHASES_PAUSED)
			PRINTLN("[RCC MISSION][LM] SHOULD_VEH_RESPAWN_NOW veh ",i," - RETURNING FALSE - Bombushka vehicle can't respawn yet, teams should respawn back at start but renderphases aren't paused yet")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_ENTITY_SPAWN_TRIGGERED_BY_ZONE_ENTRY(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iZoneBlockEntitySpawn)
		IF HAS_ENTITY_SPAWN_ZONE_REQUIREMENT_BEEN_MET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iZoneBlockEntitySpawn, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iZoneBlockEntitySpawnPlayerReq)
			PRINTLN("[RCC MISSION] SHOULD_VEH_RESPAWN_NOW - veh: ", i, " - RETURN TRUE, HAS_ENTITY_SPAWN_ZONE_REQUIREMENT_BEEN_MET = TRUE.")
			RETURN TRUE
		ENDIF
		PRINTLN("[RCC MISSION] SHOULD_VEH_RESPAWN_NOW - veh: ", i, " - RETURN FALSE, HAS_ENTITY_SPAWN_ZONE_REQUIREMENT_BEEN_MET = FALSE.. iZone: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iZoneBlockEntitySpawn, " iPlayerReq: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iZoneBlockEntitySpawnPlayerReq)
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedObjective, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedAlwaysForceSpawnOnRule)
			PRINTLN("[RCC MISSION] SHOULD_VEH_RESPAWN_NOW veh ",i," - RETURNING TRUE - Team has reached entity spawn conditions (1)")
			RETURN TRUE
		ENDIF
	ELSE
		//PRINTLN("[RCC MISSION] Vehicle SHOULD SPAWN: ",i," SINCE NO ASSOCIATED RULES: ") 
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSecondAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSecondAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSecondAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSecondAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSecondAssociatedObjective, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSecondAssociatedScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSecondAssociatedAlwaysForceSpawnOnRule)
			PRINTLN("[RCC MISSION] SHOULD_VEH_RESPAWN_NOW veh ",i," - RETURNING TRUE - Team has reached entity spawn conditions (2)")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iThirdAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iThirdAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iThirdAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iThirdAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iThirdAssociatedObjective, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iThirdAssociatedScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iThirdAssociatedAlwaysForceSpawnOnRule)
			PRINTLN("[RCC MISSION] SHOULD_VEH_RESPAWN_NOW veh ",i," - RETURNING TRUE - Team has reached entity spawn conditions (3)")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iFourthAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iFourthAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iFourthAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iFourthAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iFourthAssociatedObjective, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iFourthAssociatedScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iFourthAssociatedAlwaysForceSpawnOnRule)
			PRINTLN("[RCC MISSION] SHOULD_VEH_RESPAWN_NOW veh ",i," - RETURNING TRUE - Team has reached entity spawn conditions (4)")
			RETURN TRUE
		ENDIF
	ENDIF
	
	
	RETURN FALSE

ENDFUNC

FUNC BOOL CAN_VEH_STILL_SPAWN_IN_MISSION(INT iveh, INT iTeamOverride = -1, INT iRuleOverride = -1)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iAssociatedTeam > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iAssociatedSpawn = ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY
			
			IF iTeamOverride = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iAssociatedTeam
			AND iRuleOverride != -1
				IF iRuleOverride <= g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iAssociatedObjective
					// The vehicle will have a chance to spawn:
					RETURN TRUE
				ENDIF
			ELSE
				IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iAssociatedTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iAssociatedObjective
					// The vehicle still has a chance to spawn:
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE // Spawn type won't prevent vehicle from spawning after the rule
			RETURN TRUE
		ENDIF
	ELSE
		//Vehicle will just spawn at mission start!
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iSecondAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iSecondAssociatedTeam > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iSecondAssociatedSpawn = ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY
			IF iTeamOverride = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iSecondAssociatedTeam
			AND iRuleOverride != -1
				IF iRuleOverride <= g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iSecondAssociatedObjective
					RETURN TRUE
				ENDIF
			ELSE
				IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iSecondAssociatedTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iSecondAssociatedObjective
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iThirdAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iThirdAssociatedTeam > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iThirdAssociatedSpawn = ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY
			IF iTeamOverride = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iThirdAssociatedTeam
			AND iRuleOverride != -1
				IF iRuleOverride <= g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iThirdAssociatedObjective
					RETURN TRUE
				ENDIF
			ELSE
				IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iThirdAssociatedTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iThirdAssociatedObjective
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iFourthAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iFourthAssociatedTeam > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iFourthAssociatedSpawn = ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY
			IF iTeamOverride = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iFourthAssociatedTeam
			AND iRuleOverride != -1
				IF iRuleOverride <= g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iFourthAssociatedObjective
					RETURN TRUE
				ENDIF
			ELSE
				IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iFourthAssociatedTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iFourthAssociatedObjective
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL HAVE_VEHICLE_MODELS_LOADED(INT iveh)
	
	BOOL bModelLoaded = TRUE
	
	REQUEST_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn)
	
	IF NOT HAS_MODEL_LOADED(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn)
		bModelLoaded = FALSE
	ENDIF
	
	MODEL_NAMES crateModel = GET_VEHICLE_CRATE_MODEL_RC(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetTwo, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSeven, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn)
	
	IF crateModel != DUMMY_MODEL_FOR_SCRIPT
		PRINTLN("[RCC MISSION] HAVE_VEHICLE_MODELS_LOADED - VEHICLE HAS VALID CRATE:",iveh)
		REQUEST_MODEL(crateModel)
		IF NOT HAS_MODEL_LOADED(crateModel)
			bModelLoaded = FALSE
		ENDIF
	ENDIF
	
	RETURN bModelLoaded

ENDFUNC

FUNC BOOL RUN_VEH_VISIBLE_CHECK(INT iveh)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet,ciFMMC_VEHICLE_IgnoreVisCheck)
		PRINTLN("[RCC MISSION] RUN_VEH_VISIBLE_CHECK - Ignoring vis check on veh: ",iveh, " because of ciFMMC_VEHICLE_IgnoreVisCheck")
		RETURN TRUE
	ELSE
		FLOAT fVisDist = 120.0
		
		IF (IS_THIS_MODEL_A_HELI(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn) OR IS_THIS_MODEL_A_PLANE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn))
		AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetThree, ciFMMC_VEHICLE3_VehicleStartsAirborne)
		AND NOT (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = TITAN AND IS_THIS_ROCKSTAR_MISSION_WVM_DUNE3(g_FMMC_STRUCT.iRootContentIDHash))
			fVisDist = GET_AIR_VEHICLE_VISIBLE_RANGE_CHECK(iveh)
		ENDIF
		
		IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(vEntityRespawnLoc, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, fVisDist)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC VECTOR GET_VEHICLE_FMMC_SPAWN_LOCATION(INT iVeh, BOOL& bForceAreaCheck, INT &iSpawnInterior, INT iSpecialOverridePosSlot)
	
	VECTOR vSpawnLoc = GET_VEH_SPAWN_LOCATION(iVeh, MC_ServerBD.iHeistPresistantPropertyID, iSpawnRandomHeadingToSelect, iSpawnInterior, iSpecialOverridePosSlot)
	bForceAreaCheck = FALSE
	
	IF IS_BIT_SET(MC_serverBD_2.iVehSpawnedOnce, iveh)
	AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vSecondSpawnPos)
		IF GET_RANDOM_INT_IN_RANGE(0, 100) < 50
			SET_BIT(iLocalBoolCheck10, LBOOL10_VEH_SPAWN_AT_SECOND_POSITION)
			vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vSecondSpawnPos
			iSpawnInterior = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iSecondInterior
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iSpawnNearType != ciRULE_TYPE_NONE
	AND (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iSpawnNearEntityID > -1 OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iSpawnNearType = ciSPAWN_NEAR_ENTITY_TYPE_LAST_PLAYER)
		IF (NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetTwo, ciFMMC_VEHICLE2_FirstSpawnNotNearEntity))
		OR IS_BIT_SET(MC_serverBD_2.iVehSpawnedOnce, iveh)
			
			VECTOR vNewSpawn = GET_FMMC_ENTITY_LOCATION(vSpawnLoc, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iSpawnNearType, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iSpawnNearEntityID, DEFAULT, iVeh)
			
			IF NOT IS_VECTOR_ZERO(vNewSpawn)
				vSpawnLoc = vNewSpawn
				bForceAreaCheck = TRUE
			ENDIF
		
		ENDIF
		
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix, ciFMMC_VEHICLE6_ALWAYS_FORCE_ORIGINAL_RESPAWN_POS)
		vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].vPos
		iSpawnInterior = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehInterior
	ENDIF
		
	IF IS_BIT_SET(MC_ServerBD.iServerBitSet6, SBBOOL6_USING_CYCLE_VEHICLE_PLACEMENT)
		INT iTeamLoop, iSpawnPoint
		FOR iTeamLoop = 0 TO (FMMC_MAX_TEAMS-1)
			FOR iSpawnPoint = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[iTeamLoop]-1
				IF MC_serverBD_3.iPlacedVehiclePlayerPart[iTeamLoop][iSpawnPoint] = iVeh				
					vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeamLoop][iSpawnPoint].vPos
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] - [GET_VEHICLE_FMMC_SPAWN_LOCATION] - vStartPos overriden to: ", vSpawnLoc, " iveh: ", iveh)
				ENDIF
			ENDFOR
		ENDFOR
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetEight, ciFMMC_VEHICLE8_UseRandomPoolPosAsRespawnSpawn)	
		INT iSpawnPool = GET_RANDOM_SELECTION_FROM_SPAWN_POOL(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].sWarpLocationSettings.vPosition)		
		vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].sWarpLocationSettings.vPosition[iSpawnPool]
		PRINTLN( "[RCC MISSION] GET_VEHICLE_FMMC_SPAWN_LOCATION - Grabbing random position from pool for veh ", iVeh, " iSpawnPool: ", iSpawnPool, " vSpawnLoc: ", vSpawnLoc)
	ENDIF
	
	RETURN vSpawnLoc
	
ENDFUNC

FUNC BOOL ARE_WE_WAITING_FOR_VEHICLES_TO_CLEAN_UP()
	
	BOOL bWaiting
	
	IF DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME(TRUE)
	OR IS_BIT_SET(iLocalBoolCheck7, LBOOL7_HOST_APPLIED_NEW_PRIORITY_MIDFRAME)
	OR IS_BIT_SET(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
	OR MC_serverBD.iVehCleanup_NeedOwnershipBS != 0
		bWaiting = TRUE
	ENDIF
	
	RETURN bWaiting
	
ENDFUNC

FUNC BOOL RESPAWN_MISSION_VEHICLE(INT iveh)

	FLOAT fspawnhead
	BOOL bwaternode
	BOOL broadnode
	BOOL bForFlyingVehicle
	BOOL bIninterior
	FLOAT fMaxZUnderRaw
	VEHICLE_INDEX tempveh
	MODEL_NAMES crateModel
	BOOL bForceAreaCheck
	BOOL bShouldForceSpawn = (SHOULD_FORCE_IMMEDIATE_ENTITY_SPAWN(iVeh, CREATION_TYPE_VEHICLES) OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetEight, ciFMMC_VEHICLE8_AlwaysSpawnAtAuthoredPosition))
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn != DUMMY_MODEL_FOR_SCRIPT
			
			// If we want to instantly respawn this veh
			IF (NOT IS_BIT_SET(MC_serverBD.iVehDelayRespawnBitset, iveh))
			AND (iVehSpawnFailDelay[iveh] = -1)
				
				PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE - requesting model for veh in respawn : ", iveh, " model = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn))
				IF HAVE_VEHICLE_MODELS_LOADED( iveh )
					PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE - checking CAN_REGISTER_MISSION_VEHICLES for veh in respawn : ", iveh)
					IF CAN_REGISTER_MISSION_VEHICLES(1)
					AND NOT ARE_WE_WAITING_FOR_VEHICLES_TO_CLEAN_UP()
						PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE - can register ok for veh in respawn : ", iveh)
						IF (MC_serverBD.isearchingforPed = -1 AND MC_serverBD.isearchingforObj = -1 AND MC_serverBD.isearchingforVeh = -1)
						OR (MC_serverBD.isearchingforVeh = iveh AND MC_serverBD.isearchingforPed = -1 AND MC_serverBD.isearchingforObj = -1)
							MC_serverBD.isearchingforVeh = iveh
							
							IF iEntityRespawnType != ci_TARGET_VEHICLE
							OR iEntityRespawnID != iveh
								vEntityRespawnLoc = <<0, 0, 0>>
								iEntityRespawnInterior = -1
								SpawnInterior = NULL
								iEntityRespawnType = ci_TARGET_VEHICLE
								iEntityRespawnID = iveh
							ENDIF
																			
							IF IS_VECTOR_ZERO(vEntityRespawnLoc)
								CLEAR_BIT(iLocalBoolCheck10, LBOOL10_VEH_SPAWN_AT_SECOND_POSITION)
								vEntityRespawnLoc = GET_VEHICLE_FMMC_SPAWN_LOCATION(iveh, bForceAreaCheck, iEntityRespawnInterior, MC_ServerBD_4.iFacilityPosVehSpawned)
								PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE - can register ok for veh in respawn : ", iveh, " Using vEntityRespawnLoc = ", vEntityRespawnLoc)
								IF bForceAreaCheck
									SET_BIT(iLocalBoolCheck4, LBOOL4_RESPAWN_SHOULD_FORCE_AREA_CHECK)
								ELSE
									CLEAR_BIT(iLocalBoolCheck4, LBOOL4_RESPAWN_SHOULD_FORCE_AREA_CHECK)
								ENDIF
							ELSE
								bForceAreaCheck = IS_BIT_SET(iLocalBoolCheck4, LBOOL4_RESPAWN_SHOULD_FORCE_AREA_CHECK)
							ENDIF
							
							fSpawnHead = GET_VEH_SPAWN_HEADING(iveh, MC_ServerBD.iHeistPresistantPropertyID, IS_BIT_SET(iLocalBoolCheck10, LBOOL10_VEH_SPAWN_AT_SECOND_POSITION), iSpawnRandomHeadingToSelect)
							
							PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE - creating spawn for veh: ", iveh, " at fSpawnHead: ",fSpawnHead)
							
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFive, ciFMMC_VEHICLE5_CONSTRAINT_Z_SPAWN)
								FLOAT fDiff =  ABSF(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].vPos.z - vEntityRespawnLoc.z)
								
								IF fDiff > 10.0
								
								ENDIF
							ENDIF
							
							IF NOT bForceAreaCheck
							AND NOT NETWORK_ENTITY_AREA_DOES_EXIST(MC_serverBD.iSpawnArea)
							AND NOT bShouldForceSpawn
								PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE - creating spawn for veh: ",iveh,"at coords: ",vEntityRespawnLoc)
								//Old SpawnArea, just a 4x4x4 box around the vehicle's intended spawn location:
								//MC_serverBD.iSpawnArea = NETWORK_ADD_ENTITY_AREA((g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].vPos-<<2.0,2.0,2.0>>),(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].vPos+<<2.0,2.0,2.0>>))
								
								VECTOR vMin, vMax
								
								GET_MODEL_DIMENSIONS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn,vMin,vMax)
								
								FLOAT fWidth = vMax.x - vMin.x
								
								vMax = <<0,vMax.y,vMax.z>>
								vMin = <<0,vMin.y,vMin.z>>
								
								vMax = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vEntityRespawnLoc,fSpawnHead,vMax)
								vMin = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vEntityRespawnLoc,fSpawnHead,vMin)
								
								MC_serverBD.iSpawnArea = NETWORK_ADD_ENTITY_ANGLED_AREA(vMax,vMin,fWidth)
								REINIT_NET_TIMER(MC_serverBD.tdSpawnAreaTimeout)
								
							ELSE
								IF bForceAreaCheck
								OR NETWORK_ENTITY_AREA_HAVE_ALL_REPLIED(MC_serverBD.iSpawnArea)
								OR bShouldForceSpawn
									IF NOT bForceAreaCheck
									AND NOT NETWORK_ENTITY_AREA_IS_OCCUPIED(MC_serverBD.iSpawnArea)
									AND RUN_VEH_VISIBLE_CHECK(iveh)
									OR bShouldForceSpawn
										PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE - calling create respawn veh at original position: ",iveh,"at coords: ",vEntityRespawnLoc)
										CLEAR_AREA_LEAVE_VEHICLE_HEALTH( vEntityRespawnLoc,10.0,FALSE,FALSE,FALSE,FALSE )
										IF CREATE_NET_VEHICLE(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh], g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn, vEntityRespawnLoc, fSpawnHead, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, SHOULD_VEHICLE_CREATION_IGNORE_GROUND_CHECK(iveh, vEntityRespawnLoc, iEntityRespawnInterior))
											
											tempveh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
											IF IS_VEHICLE_MODEL(tempVeh, MULE)
												FMMC_SET_THIS_VEHICLE_EXTRAS(tempveh, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iLivery)
											ELSE
												FMMC_SET_THIS_VEHICLE_EXTRAS(tempveh, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet)
											ENDIF
											FMMC_SET_THIS_VEHICLE_COLOURS(tempveh, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iColour,g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iLivery, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iColourCombination, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iColour2, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iColour3, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fEnvEff)
											INT iNumPlayers = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]
											SET_UP_FMMC_VEH_RC(tempveh,iveh, MC_ServerBD_1.sFMMC_SBD.niVehicle, vEntityRespawnLoc, (iSpawnRandomHeadingToSelect > 0), iEntityRespawnInterior, DEFAULT, iNumPlayers)
											SET_ENTITY_VISIBLE(tempveh,TRUE)
											SET_MODEL_AS_NO_LONGER_NEEDED( g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn)
											NETWORK_REMOVE_ENTITY_AREA(MC_serverBD.iSpawnArea)
											RESET_NET_TIMER(MC_serverBD.tdSpawnAreaTimeout)
											MC_serverBD.iSpawnArea = -1
											MC_serverBD.isearchingforVeh = -1
											crateModel = GET_VEHICLE_CRATE_MODEL_RC(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitset,g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetTwo,g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSeven, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn)
											IF crateModel != DUMMY_MODEL_FOR_SCRIPT
												FMMC_SET_THIS_VEHICLE_CRATES_ROWAN_C(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]),g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet,g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetTwo,g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSeven,MC_serverBD_1.sFMMC_SBD.niVehicleCrate[iveh])
											ENDIF
											SET_BIT(MC_serverBD.iVehSpawnBitset, iveh)
											iVehSpawnFailCount[iveh] = 0
											iVehSpawnFailDelay[iveh] = -1
											vEntityRespawnLoc = <<0.0, 0.0, 0.0 >>
											iSpawnRandomHeadingToSelect = -1
											SpawnInterior = NULL
											iEntityRespawnInterior = -1
											GET_NEXT_SPAWNING_PED_AFTER_VEHICLE_SPAWN(iVeh)
											
											IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSeven, ciFMMC_VEHICLE7_DYNAMIC_FACILITY_SPAWN_OVERRIDE)
											AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = STROMBERG
												PRINTLN("[LM] (1) RESPAWN_MISSION_VEHICLE - Incrementing the iFacilityPosVehSpawned to: ", (MC_ServerBD_4.iFacilityPosVehSpawned+1))
												MC_ServerBD_4.iFacilityPosVehSpawned++
											ENDIF
											
											RETURN TRUE
										ENDIF
									ELSE
										IF IS_THIS_MODEL_A_BOAT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn)
										OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = SUBMERSIBLE
											bwaternode = TRUE
										ELIF (IS_THIS_MODEL_A_HELI(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn) OR IS_THIS_MODEL_A_PLANE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn))
										AND (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetThree, ciFMMC_VEHICLE3_VehicleStartsAirborne)
										AND NOT (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = TITAN AND IS_THIS_ROCKSTAR_MISSION_WVM_DUNE3(g_FMMC_STRUCT.iRootContentIDHash)))
											broadnode = TRUE
											bForFlyingVehicle = TRUE 
										ELSE
											broadnode = TRUE
										ENDIF
										
										IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFive, ciFMMC_VEHICLE5_CONSTRAINT_Z_SPAWN)
											FLOAT fDiff =  ABSF(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].vPos.z - vEntityRespawnLoc.z)
											
											IF fDiff > 10.0
												fMaxZUnderRaw = 5.0
											ENDIF
										ENDIF
							
										// for ctf missions don't allow the vehicle to spawn more than 5m below the raw coords (see 1791736)
										IF Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer)
											fMaxZUnderRaw = 5.0
										ENDIF
										
										IF SpawnInterior = NULL
											SpawnInterior = GET_INTERIOR_AT_COORDS(vEntityRespawnLoc)
										ENDIF
										
										IF SpawnInterior = NULL
											bIninterior = FALSE
										ELSE
											bIninterior = TRUE
										ENDIF
										
										SPAWN_SEARCH_PARAMS SpawnSearchParams
										SpawnSearchParams.bConsiderInteriors = bIninterior
										SpawnSearchParams.fMinDistFromPlayer = 20
										SpawnSearchParams.bCloseToOriginAsPossible = TRUE
										SpawnSearchParams.bConsiderOriginAsValidPoint = TRUE
										SpawnSearchParams.bSearchVehicleNodesOnly = broadnode
										SpawnSearchParams.bUseOnlyBoatNodes = bwaternode
										SpawnSearchParams.fHeadingForConsideredOrigin = GET_VEH_SPAWN_HEADING(iveh, MC_ServerBD.iHeistPresistantPropertyID, DEFAULT, DEFAULT, MC_ServerBD_4.iFacilityPosVehSpawned)
										SpawnSearchParams.fMaxZBelowRaw = fMaxZUnderRaw
										SpawnSearchParams.bIsForFlyingVehicle = bForFlyingVehicle
										
										FLOAT fSearchRadius
										fSearchRadius = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fRespawnRange
										
										IF bForceAreaCheck
											SpawnSearchParams.vFacingCoords = vEntityRespawnLoc
											//Want to spawn outside of a range from vEntityRespawnLoc:
											SpawnSearchParams.vAvoidCoords[0] = vEntityRespawnLoc
											SpawnSearchParams.fAvoidRadius[0] = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fRespawnMinRange
											SpawnSearchParams.bConsiderOriginAsValidPoint = FALSE
											SpawnSearchParams.bCloseToOriginAsPossible = FALSE																				
											IF (fSearchRadius < SpawnSearchParams.fAvoidRadius[0])
												fSearchRadius += 100.0
												PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE - fSearchRadius was smaller than avoid radius, increasing.")
											ENDIF
										ENDIF
										
										#IF IS_DEBUG_BUILD
										DEBUG_PRINT_SPAWN_SEARCH_PARAMS(SpawnSearchParams)
										#ENDIF
										
										//Generating a facing towards co-ords based on fspawnhead
										SpawnSearchParams.vFacingCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vEntityRespawnLoc,fspawnhead,<<0,1,0>>)
										PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE - SpawnSearchParams.vFacingCoords is set to: ", SpawnSearchParams.vFacingCoords)
										
										PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE - Calling GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY with iVeh: ", iVeh, " vEntityRespawnLoc: ", vEntityRespawnLoc, " fSearchRadius: ", fSearchRadius, " fAvoidRadius: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fRespawnMinRange)
										
										VECTOR vSpawnLoc
										IF GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY(vEntityRespawnLoc,fSearchRadius,vSpawnLoc,fspawnhead,SpawnSearchParams)
											
											IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix, ciFMMC_VEHICLE6_ALWAYS_FORCE_ORIGINAL_RESPAWN_POS)
												PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE - ciFMMC_VEHICLE6_ALWAYS_FORCE_ORIGINAL_RESPAWN_POS - Setting vPos: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].vPos)
												vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].vPos												
												
												IF IS_BIT_SET(MC_ServerBD.iServerBitSet6, SBBOOL6_USING_CYCLE_VEHICLE_PLACEMENT)
													INT iTeamLoop, iSpawnPoint
													FOR iTeamLoop = 0 TO (FMMC_MAX_TEAMS-1)
														FOR iSpawnPoint = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[iTeamLoop]-1
															IF MC_serverBD_3.iPlacedVehiclePlayerPart[iTeamLoop][iSpawnPoint] = iVeh				
																vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeamLoop][iSpawnPoint].vPos
																//vSpawnLoc = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vSpawnLoc, fspawnhead, <<-2.5, 0.0, 0.0>>)
																CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] - [GET_VEHICLE_FMMC_SPAWN_LOCATION] ciFMMC_VEHICLE6_ALWAYS_FORCE_ORIGINAL_RESPAWN_POS - vStartPos overriden to: ", vSpawnLoc, " iveh: ", iveh)
															ENDIF
														ENDFOR
													ENDFOR
												ENDIF
											ENDIF
											
											// if we're creating the vehicle in the air then add 100m to z
											IF (bForFlyingVehicle)
												vSpawnLoc.z += 100.0
												PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE - for a flying vehicle, adding 100 to z.")
												IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet,ciFMMC_VEHICLE_IgnoreVisCheck)
												AND CAN_ANY_PLAYER_SEE_POINT(vSpawnLoc, 5.0, TRUE, TRUE, GET_AIR_VEHICLE_VISIBLE_RANGE_CHECK(iveh))
													
													PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE - a player can see new point in air at ",vSpawnLoc,", range check was ",GET_AIR_VEHICLE_VISIBLE_RANGE_CHECK(iveh))
													
													NETWORK_REMOVE_ENTITY_AREA(MC_serverBD.iSpawnArea)
													RESET_NET_TIMER(MC_serverBD.tdSpawnAreaTimeout)
													MC_serverBD.iSpawnArea = -1
													MC_serverBD.isearchingforveh = -1
													
													vEntityRespawnLoc = << 0.0, 0.0, 0.0 >> 
													iSpawnRandomHeadingToSelect = -1
													SpawnInterior = NULL
													iEntityRespawnInterior = -1
													
													INT iteam
													BOOL crit
													
													//Check if this ped is marked as critical for any teams
													FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
														IF IS_BIT_SET(MC_serverBD.iMissionCriticalVeh[iteam], iveh)
															crit = TRUE
															BREAKLOOP
														ENDIF
													ENDFOR
													
													IF NOT crit
														IF iVehSpawnFailCount[iveh] < 2
															iVehSpawnFailCount[iveh]++
														ENDIF
														iVehSpawnFailDelay[iveh] = 0 //Start the delay timer
														PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE - coords visible, setting iVehSpawnFailDelay timer running ",iveh)
													ELSE
														iVehSpawnFailCount[iveh]++
													ENDIF
													
													SET_BIT(MC_serverBD.iVehDelayRespawnBitset, iveh)
													
													RETURN FALSE
												ENDIF
											ENDIF
											
											PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE - repawn has gone through - GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY ")
											
											FLOAT fSpawnHeading 											
											IF NOT IS_VECTOR_ZERO(SpawnSearchParams.vFacingCoords)
												fSpawnHeading = GET_HEADING_BETWEEN_VECTORS(vSpawnLoc, SpawnSearchParams.vFacingCoords)
												PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE - fSpawnHeading: ", fSpawnHeading)
											ELSE
												fSpawnHeading = fspawnhead
												PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE - SpawnSearchParams.vFacingCoords is <<0,0,0>> setting fSpawnheading with fspawnhead : ", fSpawnHeading)
											ENDIF
											
											PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE - calling create respawn veh: ",iveh,"at coords: ",vSpawnLoc)
											IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetThree, ciFMMC_VEHICLE3_DONT_CLEAR_AREA)
												FLOAT fClearRadius = 10.0
												IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSeven, ciFMMC_VEHICLE7_LARGER_CLEAR_AREA)
													PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE - Using larger clear area on respawn.")
													fClearRadius = 30.0
												ENDIF
												CLEAR_AREA_LEAVE_VEHICLE_HEALTH(vSpawnLoc,fClearRadius,FALSE,FALSE,FALSE,FALSE )
											ENDIF
											
											IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix, ciFMMC_VEHICLE6_ALWAYS_FORCE_ORIGINAL_RESPAWN_POS)
												PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE - ciFMMC_VEHICLE6_ALWAYS_FORCE_ORIGINAL_RESPAWN_POS - Setting Heading: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fHead)
												fSpawnHeading = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fHead
											ENDIF
											
											IF CREATE_NET_VEHICLE(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh], g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn, vSpawnLoc, fSpawnHeading, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, SHOULD_VEHICLE_CREATION_IGNORE_GROUND_CHECK(iveh, vSpawnLoc, iEntityRespawnInterior))
												
												tempveh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
												IF IS_VEHICLE_MODEL(tempVeh, MULE)
													FMMC_SET_THIS_VEHICLE_EXTRAS(tempveh, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iLivery)
												ELSE
													FMMC_SET_THIS_VEHICLE_EXTRAS(tempveh, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet)
												ENDIF
												INT iNumPlayers = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]
												FMMC_SET_THIS_VEHICLE_COLOURS(tempveh, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iColour,g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iLivery, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iColourCombination, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iColour2, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iColour3, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fEnvEff)
												SET_UP_FMMC_VEH_RC(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]),iveh, MC_ServerBD_1.sFMMC_SBD.niVehicle, vSpawnLoc, FALSE, iEntityRespawnInterior, DEFAULT, iNumPlayers)
												SET_ENTITY_VISIBLE(tempveh,TRUE)
												SET_MODEL_AS_NO_LONGER_NEEDED( g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn)
												NETWORK_REMOVE_ENTITY_AREA(MC_serverBD.iSpawnArea)
												RESET_NET_TIMER(MC_serverBD.tdSpawnAreaTimeout)
												MC_serverBD.iSpawnArea = -1
												MC_serverBD.isearchingforVeh = -1
												crateModel = GET_VEHICLE_CRATE_MODEL_RC(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitset, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetTwo, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSeven, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn)
												IF crateModel != DUMMY_MODEL_FOR_SCRIPT
													FMMC_SET_THIS_VEHICLE_CRATES_ROWAN_C(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]),g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet,g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetTwo,g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSeven,MC_serverBD_1.sFMMC_SBD.niVehicleCrate[iveh])
												ENDIF
												SET_BIT(MC_serverBD.iVehSpawnBitset, iveh)
												iVehSpawnFailCount[iveh] = 0
												iVehSpawnFailDelay[iveh] = -1
												vEntityRespawnLoc = << 0.0, 0.0, 0.0 >> 
												iSpawnRandomHeadingToSelect = -1
												SpawnInterior = NULL
												iEntityRespawnInterior = -1
												
												IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSeven, ciFMMC_VEHICLE7_SPAWN_WITH_LINKED_VEH)
													IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iLinkedDestroyVeh  != -1
														IF IS_VEHICLE_MODEL(tempVeh, TRAILERSMALL2)
															INT iAttachVeh
															iAttachVeh = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iLinkedDestroyVeh
															IF IS_NET_VEHICLE_DRIVEABLE(MC_serverBD_1.sFMMC_SBD.niVehicle[iAttachVeh])
																VEHICLE_INDEX trailer = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
																FLOAT fOffset = 4.0
																IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iAttachVeh].mn = HAULER
																OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iAttachVeh].mn = HAULER2
																	fOffset = 8.55 // Length of the hauler
																ENDIF
																SET_ENTITY_COORDS(trailer, GET_ENTITY_COORDS(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iAttachVeh])) - (GET_ENTITY_FORWARD_VECTOR(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iAttachVeh]))*fOffset))
																SET_ENTITY_HEADING(trailer, GET_ENTITY_HEADING(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iAttachVeh])))
																SET_VEHICLE_ON_GROUND_PROPERLY(trailer, 8.0)
																ATTACH_VEHICLE_TO_TRAILER(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iAttachVeh]), trailer)
																PRINTLN("[RCC MISSION] - RESPAWN_WITH_TRAILER - Trailer attached!")
															ENDIF
														ENDIF
													ENDIF
												ENDIF
												
												IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSeven, ciFMMC_VEHICLE7_DYNAMIC_FACILITY_SPAWN_OVERRIDE)
												AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = STROMBERG
													PRINTLN("[LM] (2) RESPAWN_MISSION_VEHICLE - Incrementing the iFacilityPosVehSpawned to: ", (MC_ServerBD_4.iFacilityPosVehSpawned+1))
													MC_ServerBD_4.iFacilityPosVehSpawned++
												ENDIF
												
												GET_NEXT_SPAWNING_PED_AFTER_VEHICLE_SPAWN(iveh)
												
												IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iAttachParent > -1
												AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iAttachParentType = CREATION_TYPE_VEHICLES
							
													IF IS_NET_VEHICLE_DRIVEABLE(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iAttachParent])
														IF GET_ENTITY_MODEL(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])) = CARGOBOB2
															FMMC_ATTACH_CARGOBOB_TO_VEHICLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]),NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAttachParent]),TRUE)
														ELSE
															FMMC_ATTACH_VEHICLE_TO_VEHICLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]),NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAttachParent]),TRUE)
														ENDIF
														PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE attaching vehicle: ",iVeh, "to trailer: ",g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAttachParent)
													ENDIF
												
												ENDIF
												
												
												RETURN TRUE
											ENDIF
										ENDIF
									ENDIF
								ELSE
									IF (NOT HAS_NET_TIMER_STARTED(MC_serverBD.tdSpawnAreaTimeout))
									OR GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdSpawnAreaTimeout) >= ciFMMCSpawnAreaTimeout
										
										PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE ",iveh," - waited for everyone to reply back on entity area for over 10s, delete and try again")
										NETWORK_REMOVE_ENTITY_AREA(MC_serverBD.iSpawnArea)
										MC_serverBD.iSpawnArea = -1
										RESET_NET_TIMER(MC_serverBD.tdSpawnAreaTimeout)
										
									#IF IS_DEBUG_BUILD
									ELSE
										PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE ",iveh," - waiting for everyone to reply back on entity area, waited for ",GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdSpawnAreaTimeout),"ms")
									#ENDIF
									ENDIF
								ENDIF
							ENDIF
						#IF IS_DEBUG_BUILD
						ELSE
							PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE - failing to spawn veh ",iveh," because attempting to spawn ped ", MC_serverBD.isearchingforPed," / veh ", MC_serverBD.isearchingforVeh ," / obj ", MC_serverBD.isearchingforObj )
						#ENDIF
						ENDIF
					#IF IS_DEBUG_BUILD
					ELSE
						IF NOT CAN_REGISTER_MISSION_VEHICLES(1)
							PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE - failing to spawn veh ",iveh," as CAN_REGISTER_MISSION_VEHICLES is failing")
						ENDIF
						IF ARE_WE_WAITING_FOR_VEHICLES_TO_CLEAN_UP()
							IF DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME(TRUE)
								PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE - failing to spawn veh ",iveh," because of ARE_WE_WAITING_FOR_VEHICLES_TO_CLEAN_UP: new priority / midpoint this frame")
							ENDIF
							IF IS_BIT_SET(iLocalBoolCheck7, LBOOL7_HOST_APPLIED_NEW_PRIORITY_MIDFRAME)
							OR IS_BIT_SET(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
								PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE - failing to spawn veh ",iveh," because of ARE_WE_WAITING_FOR_VEHICLES_TO_CLEAN_UP: new priority / midpoint set by host")
							ENDIF
							IF MC_serverBD.iVehCleanup_NeedOwnershipBS != 0
								PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE - failing to spawn veh ",iveh," because of ARE_WE_WAITING_FOR_VEHICLES_TO_CLEAN_UP: iVehCleanup_NeedOwnershipBS = ",MC_serverBD.iVehCleanup_NeedOwnershipBS)
							ENDIF
						ENDIF
					#ENDIF
					ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE - waiting for model to load for veh in respawn : ", iveh, " model = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn))
				#ENDIF
				ENDIF // If the vehicle model hasn't been loaded
				
			ELSE // iVehDelayRespawnBitset is set, or iVehSpawnFailDelay is running
				
				IF (iVehSpawnFailDelay[iveh] >= 1000)
					iVehSpawnFailDelay[iveh] = -1
					PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE - Reached iVehSpawnFailDelay of 1 second, resetting spawn delay timer on veh ", iveh)
				ELIF iVehSpawnFailDelay[iveh] != -1
					iVehSpawnFailDelay[iveh] += ROUND(fLastFrameTime * 1000)
				ENDIF
				
			ENDIF
			
		ENDIF // If the vehicle model isn't a dummy
	ELSE // Else this vehicle already exists
		// Double checking this bit is correct (that is, set) at this point!
		SET_BIT(MC_serverBD.iVehSpawnBitset, iveh)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC


//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: WEAPON SPAWNING/CLEANUP !
//
//************************************************************************************************************************************************************

FUNC BOOL SHOULD_CLEANUP_WEAPON( INT iwep)

	IF GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
	AND IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwep].iPlacedBitset, ciFMMC_WEP_CleanupAtMissionEnd )
		RETURN TRUE
	ENDIF

	IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[ iwep ].iCleanupObjective !=-1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[ iwep ].iCleanupTeam !=-1
			IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[ iwep ].iCleanupObjective <= MC_serverBD_4.iCurrentHighestPriority[ g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[ iwep ].iCleanupTeam ]
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwep].iPlacedBitset, ciFMMC_WEP_SPAWN_IF_OPPONENT_SCORES )
		IF MC_serverBD_3.iTeamToScoreLast = MC_playerBD[iPartToUse].iteam
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwep].iPlacedBitset, ciFMMC_WEP_ONLY_SPAWN_WHEN_LOSING )
		IF MC_serverBD.iWinningTeam = MC_playerBD[iPartToUse].iteam
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck17, LBOOL17_TEAM_SWAP_PICKUP_PROCESS)
		IF MC_playerBD[iPartToUse].iTeam = 0	//Losing Team
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciCONDEMNED_MODE_ENABLED)
		RETURN NOT IS_THIS_PICKUP_VISIBLE_TO_THIS_TEAM( iwep, MC_playerBD[iPartToUse].iteam )
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC MODEL_NAMES GET_LATE_MODEL_FOR_RAGE_PICKUP(INT iType)
	SWITCH(iType)
		CASE ciCUSTOM_PICKUP_TYPE__BEAST_MODE 
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_BMD"))
		CASE ciCUSTOM_PICKUP_TYPE__BULLSHARK_TEST	
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_SHARK"))
		CASE ciCUSTOM_PICKUP_TYPE__THERMAL_VISION
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MK_THERMAL"))
		CASE ciCUSTOM_PICKUP_TYPE__SWAP	
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_SWAP"))
		CASE ciCUSTOM_PICKUP_TYPE__FILTER	
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_WEED"))
		CASE ciCUSTOM_PICKUP_TYPE__SLOW_DOWN 
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MK_S_TIME"))
		CASE ciCUSTOM_PICKUP_TYPE__BULLET_TIME
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_TIME"))
		CASE ciCUSTOM_PICKUP_TYPE__HIDE_BLIPS
			//[JS] Add blip model
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_HIDDEN"))
		CASE ciCUSTOM_PICKUP_TYPE__RANDOM
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MK_RANDOM"))
		DEFAULT
			RETURN DUMMY_MODEL_FOR_SCRIPT
	ENDSWITCH
ENDFUNC

PROC PROCESS_RAGE_PICKUP_VISIBILITY()
	INT iwep
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons iwep
		IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwep].iCustomPickupType != ciCUSTOM_PICKUP_TYPE__NONE
		OR g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwep].iVehicleWeaponPickupType != -1
			IF DOES_PICKUP_EXIST(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[ iwep ].id)
			AND DOES_PICKUP_OBJECT_EXIST(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[ iwep ].id)
				IF bPowerUpActive
					SET_PICKUP_TRANSPARENT_WHEN_UNCOLLECTABLE(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[ iwep ].id, TRUE)
					SET_PICKUP_OBJECT_ALPHA_WHEN_TRANSPARENT(50)	
				ELSE
					SET_PICKUP_TRANSPARENT_WHEN_UNCOLLECTABLE(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[ iwep ].id, FALSE)
				ENDIF
				
				IF IS_BIT_SET(iLocalBoolCheck17, LBOOL17_MAX_BEASTS_ACTIVE)
					IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[ iwep ].iCustomPickupType = ciCUSTOM_PICKUP_TYPE__BEAST_MODE
						SET_PICKUP_TRANSPARENT_WHEN_UNCOLLECTABLE(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[ iwep ].id, TRUE)
						SET_PICKUP_OBJECT_ALPHA_WHEN_TRANSPARENT(50)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC ACTIVATE_WEAPON_PICKUP(INT iwep)
	
	PRINTLN("[KH][WEAPON PICKUPS] ACTIVATE_WEAPON_PICKUP - [ ",iwep," ] Making visible and collectable")
	SET_PICKUP_UNCOLLECTABLE(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[ iwep ].id, FALSE)
	SET_PICKUP_HIDDEN_WHEN_UNCOLLECTABLE(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[ iwep ].id, FALSE)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciENABLE_POWER_DESPAWN_SOUND)
		PLAY_SOUND_FROM_COORD(-1, "Powerup_Respawn", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[ iwep ].vPos, "POWER_PLAY_General_Soundset")
	ENDIF
	
	IF NOT IS_BIT_SET(iActiveWeapons[GET_LONG_BITSET_INDEX(iwep)],GET_LONG_BITSET_BIT(iwep))
		SET_BIT(iActiveWeapons[GET_LONG_BITSET_INDEX(iwep)],GET_LONG_BITSET_BIT(iwep))
	ENDIF
	
ENDPROC

PROC DEACTIVATE_WEAPON_PICKUP(INT iwep)
	
	STRING sPickUpSoundSet
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA (g_FMMC_STRUCT.iAdversaryModeType)
		sPickUpSoundSet = "DLC_IE_VV_General_Sounds"
	ELSE
		sPickUpSoundSet = "POWER_PLAY_General_Soundset"
	ENDIF

	PRINTLN("[KH][WEAPON PICKUPS] DEACTIVATE_WEAPON_PICKUP - [ ",iwep," ] Making invisible and uncollectable")
	SET_PICKUP_UNCOLLECTABLE(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[ iwep ].id, TRUE)
	SET_PICKUP_HIDDEN_WHEN_UNCOLLECTABLE(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[ iwep ].id, TRUE)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciENABLE_POWER_DESPAWN_SOUND)
	AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA (g_FMMC_STRUCT.iAdversaryModeType)
		PLAY_SOUND_FROM_COORD(-1, "Powerup_Despawn", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[ iwep ].vPos, sPickUpSoundSet)
		PRINTLN("[KH][WEAPON PICKUPS] DEACTIVATE_WEAPON_PICKUP - playing despawn sound")
	ENDIF
	
	IF DOES_BLIP_EXIST(biPickup[iwep])
		REMOVE_BLIP(biPickup[iwep])
	ENDIF
	
	IF IS_BIT_SET(iActiveWeapons[GET_LONG_BITSET_INDEX(iwep)],GET_LONG_BITSET_BIT(iwep))
		CLEAR_BIT(iActiveWeapons[GET_LONG_BITSET_INDEX(iwep)],GET_LONG_BITSET_BIT(iwep))
	ENDIF
	
ENDPROC

PROC PROCESS_WEAPON_SPAWNING(INT iwep)

	// If this prop has an associated rule
	IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[ iwep ].iAssociatedObjective != -1
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwep].iPlacedBitset, ciFMMC_WEP_SPAWN_IF_OPPONENT_SCORES)
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciCONDEMNED_MODE_ENABLED)
	OR IS_BIT_SET(iLocalBoolCheck17, LBOOL17_TEAM_SWAP_PICKUP_PROCESS)
		
		IF DOES_PICKUP_EXIST( g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[ iwep ].id )

			IF NOT IS_BIT_SET(iActiveWeapons[GET_LONG_BITSET_INDEX(iwep)], GET_LONG_BITSET_BIT(iwep))
	
				IF SHOULD_WEP_SPAWN_NOW( iwep )
					PRINTLN("[KH][WEAPON PICKUPS] Weapon should load now - ", iwep)
					IF NOT SHOULD_CLEANUP_WEAPON( iwep )
						ACTIVATE_WEAPON_PICKUP( iwep )
					ELSE
						PRINTLN("[KH][WEAPON PICKUPS] The following weapon should cleanup now! - ", iwep)
					ENDIF
				ELSE							
					//PRINTLN("[LH][WEAPONZ] The following weapon should not spawn now! - ", iwep)
				ENDIF
			ELSE								
				IF SHOULD_CLEANUP_WEAPON( iwep )					
					DEACTIVATE_WEAPON_PICKUP( iwep )
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_POWER_PLAY(g_FMMC_STRUCT.iAdversaryModeType)
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA(g_FMMC_STRUCT.iAdversaryModeType)
		PROCESS_RAGE_PICKUP_VISIBILITY()
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_PROP_SPAWN_NOW( INT iprop )
	
	BOOL bReturn = FALSE
	
	IF SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].eSpawnConditionFlag)
		PRINTLN("[RCC MISSION] SHOULD_PROP_SPAWN_NOW Prop ", iProp, " - RETURNING TRUE - SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS - eSpawnConditionFlag: ", ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].eSpawnConditionFlag[0]))
		RETURN FALSE
	ENDIF	
	
	IF IS_ANY_TEAM_ATTEMPTING_TO_RESTART_RULE()
		PRINTLN("[RCC MISSION] SHOULD_PROP_SPAWN_NOW - SBBOOL5_RETRY_CURRENT_RULE_TEAM iprop: ", iprop)
		RETURN FALSE
	ENDIF
	
	IF IS_ANY_TEAM_BEING_WARPED_TO_START_POINT()
	AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_ALL_PLAYERS_RENDERPHASES_PAUSED)
		PRINTLN("[RCC MISSION] SHOULD_PROP_SPAWN_NOW - Waiting for renderphases to be paused iprop: ", iprop)
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(iPropCleanedupBS[ GET_LONG_BITSET_INDEX(iprop)], GET_LONG_BITSET_BIT(iprop))
		PRINTLN("[RCC MISSION] SHOULD_PROP_SPAWN_NOW prop ", iprop, " should not respawn now as iPropCleanedupBS")
		RETURN FALSE
	ENDIF
		
	IF NOT DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(iprop, MC_serverBD_4.rsgSpawnSeed, eSGET_Prop)
		PRINTLN("[RCC MISSION] SHOULD_PROP_SPAWN_NOW Prop ",iprop," - RETURNING FALSE - DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP = FALSE")
		RETURN FALSE
	ENDIF	
	
	IF IS_BIT_SET(iPropRespawnNowBS[GET_LONG_BITSET_INDEX(iProp)], GET_LONG_BITSET_BIT(iProp))
		PRINTLN("[RCC MISSION] SHOULD_PROP_SPAWN_NOW prop ", iprop, " should respawn now")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iPropBitset, ciFMMC_PROP_AssociatedSpawn)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iPropSpawnNumber > -1
			IF IS_BIT_SET(MC_ServerBD.iSpawnPointsUsed[g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iPropTeamNumber], g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iPropSpawnNumber)
			OR IS_BIT_SET(LocalRandomSpawnBitset[g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iPropTeamNumber], g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iPropSpawnNumber)
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		ELSE
			IF MC_ServerBD.iNumberOfTeams > g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iPropTeamNumber
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iAssociatedObjective)
			bReturn = TRUE
		ENDIF
	ELSE
		bReturn = TRUE
	ENDIF
	
	IF NOT bReturn // If we already know it's going to spawn, don't bother with checking again
	AND g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iSecondAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iSecondAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iSecondAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iSecondAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iSecondAssociatedObjective)
			bReturn = TRUE
		ENDIF
	ENDIF
	
	IF NOT bReturn
	AND g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iThirdAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iThirdAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iThirdAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iThirdAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iThirdAssociatedObjective)
			bReturn = TRUE
		ENDIF
	ENDIF
	
	IF NOT bReturn
	AND g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iFourthAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iFourthAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iFourthAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iFourthAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iFourthAssociatedObjective)
			bReturn = TRUE
		ENDIF
	ENDIF
	
	RETURN bReturn
	
ENDFUNC

FUNC BOOL SHOULD_CLEANUP_DYNOPROP( INT iprop, OBJECT_INDEX  tempProp )

	IF GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
	AND IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[ iprop ].iDynoPropBitset, ciFMMC_DYNOPROP_CleanupAtMissionEnd )
		RETURN TRUE
	ENDIF

	IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[ iprop ].iCleanupObjective !=-1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[ iprop ].iCleanupTeam !=-1
			IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[ iprop ].iCleanupObjective <= MC_serverBD_4.iCurrentHighestPriority[ g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[ iprop ].iCleanupTeam ]
				
				IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[ iprop ].iDynoPropBitset, ciFMMC_DYNOPROP_CleanupAtMidpoint )
					IF IS_BIT_SET( MC_serverBD.iObjectiveMidPointBitset[ g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[ iprop ].iCleanupTeam ], g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[ iprop ].iCleanupObjective )
						IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iprop].iCleanupRange <= 0
							RETURN TRUE
						ELSE
							IF tempProp != NULL
								INT iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY(tempProp)
								PLAYER_INDEX ClosestPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
								PED_INDEX playerPed = GET_PLAYER_PED(ClosestPlayer)
								IF GET_DISTANCE_BETWEEN_ENTITIES(playerPed,tempProp) > g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iprop].iCleanupRange
									RETURN TRUE
								ENDIF
							ELSE
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					//Midpoint doesn't matter
					IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iprop].iCleanupRange <= 0
						RETURN TRUE
					ELSE
						IF tempProp != NULL
							INT iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY(tempProp)
							PLAYER_INDEX ClosestPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
							PED_INDEX playerPed = GET_PLAYER_PED(ClosestPlayer)
							IF GET_DISTANCE_BETWEEN_ENTITIES(playerPed,tempProp) > g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iprop].iCleanupRange
								RETURN TRUE
							ENDIF
						ELSE
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL ARE_WE_WAITING_FOR_DYNOPROPS_TO_CLEAN_UP()
	
	BOOL bWaiting
	
	IF DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME(TRUE)
	OR IS_BIT_SET(iLocalBoolCheck7, LBOOL7_HOST_APPLIED_NEW_PRIORITY_MIDFRAME)
	OR IS_BIT_SET(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
	OR MC_serverBD.iDynoPropCleanup_NeedOwnershipBS != 0
		bWaiting = TRUE
	ENDIF
	
	RETURN bWaiting
	
ENDFUNC

FUNC BOOL SHOULD_DYNOPROP_SPAWN_NOW( INT iprop )
	
	IF ARE_WE_WAITING_FOR_DYNOPROPS_TO_CLEAN_UP()
		RETURN FALSE
	ENDIF
	
	IF IS_ANY_TEAM_ATTEMPTING_TO_RESTART_RULE()
		PRINTLN("[RCC MISSION] SHOULD_DYNOPROP_SPAWN_NOW - SBBOOL5_RETRY_CURRENT_RULE_TEAM")
		RETURN FALSE
	ENDIF
	
	IF IS_ANY_TEAM_BEING_WARPED_TO_START_POINT()
	AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_ALL_PLAYERS_RENDERPHASES_PAUSED)
		PRINTLN("[RCC MISSION] SHOULD_DYNOPROP_SPAWN_NOW - Waiting for renderphases to be paused")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(sTrapInfo_Local.iTrapExistsBS, iprop)
		PRINTLN("[RCC MISSION] SHOULD_DYNOPROP_SPAWN_NOW - Returning FALSE due to iTrapExistsBS - let the trap system handle respawning this one")
		RETURN FALSE
	ENDIF
				
	IF NOT DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(iprop, MC_serverBD_4.rsgSpawnSeed, eSGET_DynoProp)
		PRINTLN("[RCC MISSION] SHOULD_DYNOPROP_SPAWN_NOW DynoProp ",iprop," - RETURNING FALSE - DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP = FALSE")
		RETURN FALSE
	ENDIF	
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_DynoPropDestroyedTracking)
		PRINTLN("[RCC MISSION] SHOULD_DYNOPROP_SPAWN_NOW DynoProp ",iprop," - Has a iContinuityId of ", g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iprop].iContinuityId)
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iprop].iContinuityId > -1
			IF IS_BIT_SET(MC_serverBD_1.sLEGACYMissionContinuityVars.iDynoPropDestroyedTrackingBitset, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iprop].iContinuityId)
				PRINTLN("[RCC MISSION] SHOULD_DYNOPROP_SPAWN_NOW DynoProp ",iprop," - RETURNING FALSE - Dynoprop was destroyed in a previous mission")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iprop].iAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iprop].iAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iprop].iAssociatedSpawn, 
												    g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iprop].iAssociatedTeam, 
													g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iprop].iAssociatedObjective)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iprop].iSecondAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iprop].iSecondAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iprop].iSecondAssociatedSpawn, 
												    g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iprop].iSecondAssociatedTeam, 
													g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iprop].iSecondAssociatedObjective)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iprop].iThirdAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iprop].iThirdAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iprop].iThirdAssociatedSpawn, 
		                                            g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iprop].iThirdAssociatedTeam, 
													g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iprop].iThirdAssociatedObjective)								
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iprop].iFourthAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iprop].iFourthAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iprop].iFourthAssociatedSpawn, 
											        g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iprop].iFourthAssociatedTeam, 
													g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iprop].iFourthAssociatedObjective)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iDynoPropShouldRespawnNowBS, iprop)
		PRINTLN("[JS] dynopropprop ", iprop, " should respawn now")
		RETURN TRUE
	ENDIF
	
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL ARE_ALL_RESPAWN_ON_RULE_DYNOPROPS_READY()
	IF IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_ALL_RESPAWN_RULE_DPROPS_READY)
		RETURN TRUE
	ENDIF
	IF bIsLocalPlayerHost
		IF g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps > 0
			INT i
			FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps-1)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iRespawnOnRuleChangeBS > 0
					INT iTeam
					FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iRespawnOnRuleChangeBS, iTeam)
						AND NOT SHOULD_CLEANUP_DYNOPROP(i, NULL)
							IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niDynoProps[i])
							AND SHOULD_DYNOPROP_SPAWN_NOW(i)
								RETURN FALSE
							ENDIF
							BREAKLOOP
						ENDIF
					ENDFOR
				ENDIF
			ENDFOR
			PRINTLN("[JS] ARE_ALL_RESPAWN_ON_RULE_DYNOPROPS_READY - all dynoprops exist - TRUE")
			SET_BIT(MC_serverBD.iServerBitSet5, SBBOOL5_ALL_RESPAWN_RULE_DPROPS_READY)
			RETURN TRUE
		ELSE
			PRINTLN("[JS] ARE_ALL_RESPAWN_ON_RULE_DYNOPROPS_READY - no dynoprops in the mission - TRUE")
			SET_BIT(MC_serverBD.iServerBitSet5, SBBOOL5_ALL_RESPAWN_RULE_DPROPS_READY)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_ALL_RESPAWN_ON_RULE_OBJECTS_READY()
	IF IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_ALL_RESPAWN_RULE_OBJS_READY)
		RETURN TRUE
	ENDIF
	
	IF bIsLocalPlayerHost
		IF MC_serverBD.iNumObjCreated > 0
			INT i
			FOR i = 0 TO (MC_serverBD.iNumObjCreated-1)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iRespawnOnRuleChangeBS > 0
					INT iTeam
					FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iRespawnOnRuleChangeBS, iTeam)
						AND NOT SHOULD_CLEANUP_OBJ(i, NULL)
							IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[i])
							AND SHOULD_OBJ_RESPAWN_NOW(i)
								PRINTLN("[JS] ARE_ALL_RESPAWN_ON_RULE_OBJECTS_READY - RETURNING FALSE - all objs don't exist yet")
								RETURN FALSE
							ENDIF
							BREAKLOOP
						ENDIF
					ENDFOR
				ENDIF
				
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMB_FOOTBALL(g_FMMC_STRUCT.iAdversaryModeType)
					IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[i])
					
						INT iPlayersPlaying = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]
						
						IF SHOULD_OBJ_RESPAWN_NOW(i)
						OR (g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedObjective = MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedTeam]
						AND NOT IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_IN_BMBFB_SUDDEN_DEATH) 
						AND ARE_ENOUGH_PLAYERS_PLAYING_TO_SPAWN_OBJECT(i, iPlayersPlaying))
							
							PRINTLN("[JS] ARE_ALL_RESPAWN_ON_RULE_OBJECTS_READY - RETURNING FALSE - waiting for object ", i)
							
							TEXT_LABEL_63 tlDebugText
							tlDebugText = "Waiting for obj "
							tlDebugText += i
							DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.5, 0.5, 0.5>>)

							RETURN FALSE
						ELSE
							PRINTLN("[JS] ARE_ALL_RESPAWN_ON_RULE_OBJECTS_READY - Object ", i, " is already out")
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
			
			PRINTLN("[JS] ARE_ALL_RESPAWN_ON_RULE_OBJECTS_READY - all objs exist - TRUE")
			SET_BIT(MC_serverBD.iServerBitSet5, SBBOOL5_ALL_RESPAWN_RULE_OBJS_READY)
			RETURN TRUE
		ELSE
			PRINTLN("[JS] ARE_ALL_RESPAWN_ON_RULE_OBJECTS_READY - no objs in the mission - TRUE")
			SET_BIT(MC_serverBD.iServerBitSet5, SBBOOL5_ALL_RESPAWN_RULE_OBJS_READY)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_ALL_RESPAWN_ON_RULE_VEHICLES_READY()
	IF IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_ALL_RESPAWN_RULE_VEHS_READY)
		RETURN TRUE
	ENDIF
	IF bIsLocalPlayerHost
		IF MC_serverBD.iNumVehCreated > 0
			INT i
			FOR i = 0 TO (MC_serverBD.iNumVehCreated-1)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iRespawnOnRuleChangeBS > 0
					INT iTeam
					FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iRespawnOnRuleChangeBS, iTeam)
						AND NOT SHOULD_CLEANUP_VEH(i, NULL)
						AND NOT IS_BIT_SET(MC_serverBD.iVehCleanup_NeedOwnershipBS, i) // url:bugstar:3774756 - Bombushka Run I - After the Bombushka was destroyed, Buzzards from the previous turn dropped from the sky and there was an approximately 10 second delay before the Buzzards spawned in for the attacking team.
							IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
							AND SHOULD_VEH_RESPAWN_NOW(i)
								RETURN FALSE
							ENDIF
							BREAKLOOP
						ENDIF
					ENDFOR
				ENDIF
			ENDFOR
			PRINTLN("[JS] ARE_ALL_RESPAWN_ON_RULE_VEHICLES_READY - all vehs exist - TRUE")
			SET_BIT(MC_serverBD.iServerBitSet5, SBBOOL5_ALL_RESPAWN_RULE_VEHS_READY)
			RETURN TRUE
		ELSE
			PRINTLN("[JS] ARE_ALL_RESPAWN_ON_RULE_VEHICLES_READY - no vehs in the mission - TRUE")
			SET_BIT(MC_serverBD.iServerBitSet5, SBBOOL5_ALL_RESPAWN_RULE_VEHS_READY)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_ALL_RESPAWN_ON_RULE_PEDS_READY()
	IF IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_ALL_RESPAWN_RULE_PEDS_READY)
		RETURN TRUE
	ENDIF
	IF bIsLocalPlayerHost
		IF g_FMMC_STRUCT_ENTITIES.iNumberOfPeds > 0
			INT i
			FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfPeds-1)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iRespawnOnRuleChangeBS > 0
					INT iTeam
					FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iRespawnOnRuleChangeBS, iTeam)
						AND NOT SHOULD_CLEANUP_PED(i, NULL)
							IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[i])
							AND SHOULD_PED_RESPAWN_NOW(i)
								RETURN FALSE
							ENDIF
							BREAKLOOP
						ENDIF
					ENDFOR
				ENDIF
			ENDFOR
			PRINTLN("[JS] ARE_ALL_RESPAWN_ON_RULE_PEDS_READY - all peds exist - TRUE")
			SET_BIT(MC_serverBD.iServerBitSet5, SBBOOL5_ALL_RESPAWN_RULE_PEDS_READY)
			RETURN TRUE
		ELSE
			PRINTLN("[JS] ARE_ALL_RESPAWN_ON_RULE_PEDS_READY - no peds in the mission - TRUE")
			SET_BIT(MC_serverBD.iServerBitSet5, SBBOOL5_ALL_RESPAWN_RULE_PEDS_READY)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_ALL_RESPAWN_ON_RULE_ENTITIES_READY()
	IF ARE_ALL_RESPAWN_ON_RULE_PEDS_READY()
		IF ARE_ALL_RESPAWN_ON_RULE_VEHICLES_READY()
			IF ARE_ALL_RESPAWN_ON_RULE_OBJECTS_READY()
				IF ARE_ALL_RESPAWN_ON_RULE_DYNOPROPS_READY()
					RETURN TRUE
				ELSE
					PRINTLN("[JS] ARE_ALL_RESPAWN_ON_RULE_ENTITIES_READY - Nope waiting on dynoprops")
				ENDIF
			ELSE
				PRINTLN("[JS] ARE_ALL_RESPAWN_ON_RULE_ENTITIES_READY - Nope waiting on objects")
			ENDIF
		ELSE
			PRINTLN("[JS] ARE_ALL_RESPAWN_ON_RULE_ENTITIES_READY - Nope waiting on vehicles")
		ENDIF
	ELSE
		PRINTLN("[JS] ARE_ALL_RESPAWN_ON_RULE_ENTITIES_READY - Nope waiting on peds")
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC INT GET_OVERTIME_TEAM_COLOUR_FOR_BLIMP(INT iPart)
	IF iPart != -1
		IF MC_playerBD[iPart].iTeam = 0
			RETURN ENUM_TO_INT(HUD_COLOUR_ORANGE)
		ELIF MC_playerBD[iPart].iTeam = 1
			RETURN ENUM_TO_INT(HUD_COLOUR_PURPLE)
		ENDIF
	ENDIF
	RETURN ENUM_TO_INT(HUD_COLOUR_WHITE)
ENDFUNC

PROC DO_MISSION_BLIMP_SIGNS(BOOL bCleanup = FALSE)
	
	IF bCleanup
		sBlimpSign.iState = BLIMP_CLEANUP
	ENDIF

	SWITCH(sBlimpSign.iState)
		CASE BLIMP_ASSIGN_RENDER_TARGETS
			INT iModelHash 
			iModelHash = HASH("sr_mp_Spec_Races_Blimp_Sign")
			IF NOT sBlimpSign.bKnowIfBlimpExists
				INT i
				FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfProps - 1
					IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn = INT_TO_ENUM(MODEL_NAMES, iModelHash)	
						sBlimpSign.bBlimpExists = TRUE
						BREAKLOOP
					ENDIF
				ENDFOR
				
				sBlimpSign.bKnowIfBlimpExists = TRUE
			ENDIF
			
			IF sBlimpSign.bBlimpExists
				PRINTLN("[JS] - DO_BLIMP_SIGNS - BLIMP_ASSIGN_RENDER_TARGETS")
				IF NOT IS_NAMED_RENDERTARGET_REGISTERED("blimp_text")
					REGISTER_NAMED_RENDERTARGET("blimp_text")
					IF NOT IS_NAMED_RENDERTARGET_LINKED(INT_TO_ENUM(MODEL_NAMES, iModelHash))
						LINK_NAMED_RENDERTARGET(INT_TO_ENUM(MODEL_NAMES, iModelHash))
						IF sBlimpSign.iRenderTarget = -1
							sBlimpSign.iRenderTarget = GET_NAMED_RENDERTARGET_RENDER_ID("blimp_text")
							PRINTLN("[JS] - DO_BLIMP_SIGNS - BLIMP_ASSIGN_RENDER_TARGETS Done")
							sBlimpSign.iState = BLIMP_LOAD_SCALEFORM
						ENDIF
					ENDIF
				ENDIF
			ELSE
				sBlimpSign.iState = BLIMP_CLEANUP
			ENDIF
		BREAK
		
		CASE BLIMP_LOAD_SCALEFORM
			PRINTLN("[JS] - DO_BLIMP_SIGNS - Requesting Scaleform Movie")
			sBlimpSign.siBlimp = REQUEST_SCALEFORM_MOVIE("BLIMP_TEXT")
			sBlimpSign.iState = BLIMP_INIT
		BREAK
		
		CASE BLIMP_INIT
			IF HAS_SCALEFORM_MOVIE_LOADED(sBlimpSign.siBlimp)
				BEGIN_SCALEFORM_MOVIE_METHOD(sBlimpSign.siBlimp, "SET_MESSAGE")
					STRING stBlimpText
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciBLIMP_PLAYER_NAME)
					AND (NOT IS_THIS_CURRENTLY_OVERTIME_RUMBLE() OR IS_THIS_CURRENTLY_OVERTIME_RUMBLE() AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciBLIMP_TEAM_SCORES))
						IF IS_THIS_CURRENTLY_OVERTIME_TURNS()
						AND iOTPartToMove != -1
						AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iOTPartToMove))
							PLAYER_INDEX tempPlayer
							tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iOTPartToMove))
							stBlimpText = GET_PLAYER_NAME(tempPlayer)
							sBlimpSign.iOTPlayerNamePart = iOTPartToMove
						ELSE
							IF PlayerToUse != INVALID_PLAYER_INDEX()
								stBlimpText = GET_PLAYER_NAME(PlayerToUse)
							ENDIF
						ENDIF
					ENDIF
										
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciBLIMP_PLAYER_NAME)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(stBlimpText)
					ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciBLIMP_TEAM_SCORES)						
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("OVT_BLIMP_T")
							ADD_TEXT_COMPONENT_INTEGER(MC_ServerBD.iTeamScore[0])
							ADD_TEXT_COMPONENT_INTEGER(MC_ServerBD.iTeamScore[1])
						END_TEXT_COMMAND_SCALEFORM_STRING()
					ELSE
						stBlimpText = "SR_BLIMPTX"
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(stBlimpText)
					ENDIF
				END_SCALEFORM_MOVIE_METHOD()
				BEGIN_SCALEFORM_MOVIE_METHOD(sBlimpSign.siBlimp, "SET_COLOUR")
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciBLIMP_PLAYER_NAME)
					AND IS_THIS_CURRENTLY_OVERTIME_TURNS()
					AND sBlimpSign.iOTPlayerNamePart != -1
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OVERTIME_TEAM_COLOUR_FOR_BLIMP(sBlimpSign.iOTPlayerNamePart))
					ELSE
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_WHITE))
					ENDIF
				END_SCALEFORM_MOVIE_METHOD()	
				BEGIN_SCALEFORM_MOVIE_METHOD(sBlimpSign.siBlimp, "SET_SCROLL_SPEED")		
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(100.0)
					PRINTLN("[JS] - DO_BLIMP_SIGNS - BLIMP_INIT HERE... SCROLL SPEED")
				END_SCALEFORM_MOVIE_METHOD()
				SET_SCALEFORM_MOVIE_TO_USE_LARGE_RT(sBlimpSign.siBlimp, TRUE)
				PRINTLN("[JS] - DO_BLIMP_SIGNS - BLIMP_INIT Done")
				sBlimpSign.iState = BLIMP_DRAW
			ENDIF
		BREAK
		
		CASE BLIMP_DRAW
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciBLIMP_PLAYER_NAME)
			AND IS_THIS_CURRENTLY_OVERTIME_TURNS()
			AND (NOT IS_THIS_CURRENTLY_OVERTIME_RUMBLE() OR IS_THIS_CURRENTLY_OVERTIME_RUMBLE() AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciBLIMP_TEAM_SCORES))
				IF iOTPartToMove != sBlimpSign.iOTPlayerNamePart
				AND iOTPartToMove != -1
					BEGIN_SCALEFORM_MOVIE_METHOD(sBlimpSign.siBlimp, "SET_MESSAGE")
						IF iOTPartToMove != -1
						AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iOTPartToMove))
							PLAYER_INDEX tempPlayer
							tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iOTPartToMove))
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(GET_PLAYER_NAME(tempPlayer))
							sBlimpSign.iOTPlayerNamePart = iOTPartToMove
						ELSE
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("SR_BLIMPTX")
						ENDIF
					END_SCALEFORM_MOVIE_METHOD()
					BEGIN_SCALEFORM_MOVIE_METHOD(sBlimpSign.siBlimp, "SET_COLOUR")
						IF sBlimpSign.iOTPlayerNamePart != -1
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_OVERTIME_TEAM_COLOUR_FOR_BLIMP(sBlimpSign.iOTPlayerNamePart))
						ELSE
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_WHITE))
						ENDIF
					END_SCALEFORM_MOVIE_METHOD()	
					PRINTLN("[JS] - DO_BLIMP_SIGNS - iOTPartToMove has changed")
				ENDIF
			ENDIF
			
			IF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciBLIMP_TEAM_SCORES) 
			AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciBLIMP_PLAYER_NAME) 
			AND IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_RUMBLE(g_FMMC_STRUCT.iAdversaryModeType))
				IF iBlimpScoreTeam1 != MC_ServerBD.iTeamScore[0]
				OR iBlimpScoreTeam2 != MC_ServerBD.iTeamScore[1]
					BEGIN_SCALEFORM_MOVIE_METHOD(sBlimpSign.siBlimp, "SET_MESSAGE")
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("OVT_BLIMP_T")
							ADD_TEXT_COMPONENT_INTEGER(MC_ServerBD.iTeamScore[0])
							ADD_TEXT_COMPONENT_INTEGER(MC_ServerBD.iTeamScore[1])
						END_TEXT_COMMAND_SCALEFORM_STRING()
					END_SCALEFORM_MOVIE_METHOD()
					
					iBlimpScoreTeam1 = MC_ServerBD.iTeamScore[0]
					iBlimpScoreTeam2 = MC_ServerBD.iTeamScore[1]
				ENDIF
			ENDIF
			
			SET_TEXT_RENDER_ID(sBlimpSign.iRenderTarget)
			SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
			SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
			SET_SCALEFORM_MOVIE_TO_USE_SUPER_LARGE_RT(sBlimpSign.siBlimp, TRUE)
			DRAW_SCALEFORM_MOVIE(sBlimpSign.siBlimp, fBlimpCentreX, fBlimpCentreY, fBlimpWidth, fBlimpHeight, 255, 255, 255, 255)
			SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
		BREAK
		
		CASE BLIMP_CLEANUP
			IF HAS_SCALEFORM_MOVIE_LOADED(sBlimpSign.siBlimp)
				SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sBlimpSign.siBlimp)
			ENDIF
			IF IS_NAMED_RENDERTARGET_REGISTERED("blimp_text")
				RELEASE_NAMED_RENDERTARGET("blimp_text")   
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC
