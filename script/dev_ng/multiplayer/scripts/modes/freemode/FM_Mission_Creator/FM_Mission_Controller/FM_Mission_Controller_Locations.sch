
USING "FM_Mission_Controller_USING.sch"
USING "FM_Mission_Controller_Utility.sch"
USING "FM_Mission_Controller_Entities.sch"
USING "FM_Mission_Controller_ObjectiveCompletion.sch"
USING "FM_Mission_Controller_SpecialCase.sch"
USING "FM_Mission_Controller_Rules.sch"
USING "FM_Mission_Controller_Doors.sch"
USING "FM_Mission_Controller_Cutscene.sch"
USING "FM_Mission_Controller_Debug.sch"
USING "am_common_ui.sch"



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: LOCATION-BASED OBJECTIVES !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************

//TODO(Owain): I don't like that this is a function
PROC SET_LOCATION_DATA_BASED_ON_TYPE( INT iTeam )
	IF  iTeam > -1
		// if this locate is my current priority
	
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Team ", iTeam, " has progressed onto a locate which is an apartment dropoff - setting dropoff flags!"  )
	
		SET_APARTMENT_DROPOFF_WARP_RANGE_BASED_ON_CUTSCENE( iTeam )
		
		g_bPropertyDropOff = TRUE

		IF NOT g_bShouldThisPlayerGetPulledIntoApartment
			g_bShouldThisPlayerGetPulledIntoApartment = TRUE
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] g_bShouldThisPlayerGetPulledIntoApartment = TRUE " )
		ENDIF
		
		IF NOT IS_BIT_SET( iLocalBoolCheck10, LBOOL10_CURRENTLY_ON_APARTMENT_GO_TO )
			SET_BIT( iLocalBoolCheck10, LBOOL10_CURRENTLY_ON_APARTMENT_GO_TO )
		ENDIF

		IF NOT IS_BIT_SET( iLocalBoolCheck7, LBOOL7_SHOULD_HOLD_UP_MOCAP_FOR_CUTSCENE )
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Setting LBOOL7_SHOULD_HOLD_UP_MOCAP_FOR_CUTSCENE because we're going to an apartment" )
			SET_BIT( iLocalBoolCheck7, LBOOL7_SHOULD_HOLD_UP_MOCAP_FOR_CUTSCENE )
		ENDIF
	ENDIF
ENDPROC


//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: SERVER LOCATION PROCESSING / SERVER PROCESS LOCATION OBJECTIVES !
//
//************************************************************************************************************************************************************



FUNC BOOL IS_ANYONE_IN_SECONDARY_LOCATE_IF_THEY_NEED_TO_BE(INT iloc, INT iTeamsToCheck)
	
	BOOL bInLocate = FALSE
	FLOAT fSecondaryRadius = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fSecondaryRadius
	
	IF fSecondaryRadius > 0
		
		INT iPlayersToCheck, iPlayersChecked
		INT i
		
		FOR i = 0 TO (MC_serverBD.iNumberOfTeams - 1)
			IF IS_BIT_SET(iTeamsToCheck, i)
				iPlayersToCheck += MC_serverBD.iNumberOfPlayingPlayers[i]
			ENDIF
		ENDFOR
		
		IF iPlayersToCheck > 0
			
			PARTICIPANT_INDEX tempPart
			PLAYER_INDEX tempPlayer
			
			FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
				
				tempPart = INT_TO_PARTICIPANTINDEX(i)
				
				IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				AND IS_BIT_SET(iTeamsToCheck, MC_playerBD[i].iteam)
					
					tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
					
					iPlayersChecked++
					
					IF IS_NET_PLAYER_OK(tempPlayer)
						
						IF VDIST2(GET_PLAYER_COORDS(tempPlayer), GET_LOCATION_VECTOR(iloc)) <= (fSecondaryRadius * fSecondaryRadius)
							bInLocate = TRUE
							i = MAX_NUM_MC_PLAYERS //Break out!
						ENDIF
						
					ENDIF
					
					IF iPlayersChecked >= iPlayersToCheck
						i = MAX_NUM_MC_PLAYERS //Break out!
					ENDIF
					
				ENDIF
				
			ENDFOR
			
		ENDIF
	ELSE
		bInLocate = TRUE
	ENDIF
	
	RETURN bInLocate
	
ENDFUNC

FUNC FLOAT GET_CONTROL_MULTIPLIER_FROM_MENU_SELECTION(INT imenu)

	SWITCH iMenu
	
		CASE ciCAPTURE_LOCATION_MULTIPLER_0
			RETURN 0.0
		BREAK
		CASE ciCAPTURE_LOCATION_MULTIPLER_10
			RETURN 0.1
		BREAK
		CASE ciCAPTURE_LOCATION_MULTIPLER_20
			RETURN 0.2
		BREAK
		CASE ciCAPTURE_LOCATION_MULTIPLER_30
			RETURN 0.3
		BREAK
		CASE ciCAPTURE_LOCATION_MULTIPLER_50
			RETURN 0.5
		BREAK
		CASE ciCAPTURE_LOCATION_MULTIPLER_100
			RETURN 1.0
		BREAK
		
	ENDSWITCH

	RETURN 0.1

ENDFUNC

FUNC BOOL IS_AREA_ABLE_TO_BE_CAPTURED(INT iTeam, INT iLoc, BOOL bIsHeld = FALSE)
	IF Is_Player_Currently_On_MP_Versus_Mission(LocalPlayer)
		INT i
		
		//if we have competition in the capture zone, its a stalemate. 
		// This is now turned off as standard, needs to be turned on for each capture location if we want it using this system. 
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS2, ciLoc_BS2_UseAltCaptureLogic)
			FOR i = 0 TO FMMC_MAX_TEAMS-1
				IF NOT DOES_TEAM_LIKE_TEAM(iTeam, i)
				AND iTeam != i
					IF MC_serverBD_1.inumberOfTeamInArea[iLoc][i] > 0
					OR iHostileTakeoverInZoneCount[iloc][i] > 0
						//RESET_NET_TIMER(MC_serverBD.tdAreaTimer[iloc][iteam])
						RESET_NET_TIMER(MC_serverBD.tdHoldTimer[iloc][iteam])
						RETURN FALSE
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
		
		IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
			IF NOT bIsHeld
				//check if we have already captured the area (this stops capture and held logic both running)
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAreaRetained, MC_ServerBD_4.iCurrentHighestPriority[iTeam]) 
					IF MC_serverBD.iLocOwner[iLoc] = iTeam
						RETURN FALSE 
					ENDIF
				ENDIF

			ELSE
				
				//if we have already captured, we have to remain inside to retain
				IF MC_serverBD_1.inumberOfTeamInArea[iLoc][iTeam] = 0
				AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAreaRetained, MC_ServerBD_4.iCurrentHighestPriority[iTeam])
					//RESET_NET_TIMER(MC_serverBD.tdAreaTimer[iloc][iteam])
					RESET_NET_TIMER(MC_serverBD.tdHoldTimer[iloc][iteam])
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	RETURN TRUE	
ENDFUNC

PROC CLEAR_CAP_SCAN_VFX(BOOL bOnNewRule = FALSE)
	IF bOnNewRule = TRUE
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iLocalPart].iteam)
			IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxCapScanEffect)
				REMOVE_PARTICLE_FX(ptfxCapScanEffect)							
				PRINTLN("[STROMBERG][JR] - Clearing up the laser scan effect on new rule.")
			ENDIF
			
			IF iCapScanSfx != -1
				PRINTLN("[STROMBERG][JR] - (Clearing) iCapScanSfx = ", iCapScanSfx)
				STOP_SOUND(iCapScanSfx)
				RELEASE_SOUND_ID(iCapScanSfx)
				iCapScanSfx = -1
			ENDIF
		ENDIF
	ELSE
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxCapScanEffect)
			REMOVE_PARTICLE_FX(ptfxCapScanEffect)					
			PRINTLN("[STROMBERG][JR] - Clearing up the laser scan effect.")
		ENDIF
		
		IF iCapScanSfx != -1
			PRINTLN("[STROMBERG][JR] - (Clearing) iCapScanSfx = ", iCapScanSfx)
			STOP_SOUND(iCapScanSfx)
			RELEASE_SOUND_ID(iCapScanSfx)
			iCapScanSfx = -1
		ENDIF
	ENDIF
ENDPROC

PROC SERVER_RESET_CAPTURE_LOCATION_DATA(INT iLoc, INT iTeamToUse, INT iCapturingTeam, INT iPriority, BOOL &bWasObjective)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciCHANGE_BLIP_COLOUR_TO_CONTROLLING_TEAM)
		MC_serverBD_1.inumberOfTeamInArea[iloc][iTeamToUse] = 0
	ENDIF
	
	RESET_NET_TIMER(tdtimesincelastupdate[iloc][iTeamToUse])
	RESET_NET_TIMER(MC_serverBD.tdAreaTimer[iloc][iTeamToUse])
	RESET_NET_TIMER(MC_serverBD.tdHoldTimer[iloc][iTeamToUse])
	MC_serverBD_1.iNumTeamControlKills[iloc][iTeamToUse] = 0
	MC_serverBD_1.iOldTeamControlkills[iloc][iTeamToUse] = 0
	IF iPriority < FMMC_MAX_RULES
		IF MC_serverBD.iRuleReCapture[iTeamToUse][ipriority]<= 0
			IF iTeamToUse = iCapturingTeam
				IF IS_BIT_SET(MC_serverBD.iLocteamFailBitset[iloc], iCapturingTeam)
					CLEAR_BIT(MC_serverBD.iLocteamFailBitset[iloc], iCapturingTeam)
					bwasobjective = TRUE
				ENDIF
			ELSE
				IF DOES_TEAM_LIKE_TEAM(iTeamToUse, iCapturingTeam)
					IF IS_BIT_SET(MC_serverBD.iLocteamFailBitset[iloc],iTeamToUse)
						CLEAR_BIT(MC_serverBD.iLocteamFailBitset[iloc],iTeamToUse)
						bwasobjective = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC DISPLAY_CAP_SCAN_VFX()		
	VEHICLE_INDEX tempVeh
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		tempVeh = GET_VEHICLE_PED_IS_USING(LocalPlayerPed)
		
		VECTOR vOffset = <<0,1.5,0>>
		VECTOR vRot    = <<0,0,0>>
		
		PRINTLN("[STROM][JR] - TRYING TO CREATE VFX")
		IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxCapScanEffect)
			USE_PARTICLE_FX_ASSET("scr_xm_submarine")
			ptfxCapScanEffect = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY("scr_xm_stromberg_scanner", tempVeh, vOffset, vRot)
			PRINTLN("[STROM][JR] - VFX CREATED")			
		ENDIF
		
		IF iCapScanSfx = -1
			iCapScanSfx = GET_SOUND_ID()
			PLAY_SOUND_FROM_ENTITY(iCapScanSfx, "sub_scanner_loop", tempVeh, "dlc_xm_stromberg_sounds")
		ENDIF
	ENDIF	
ENDPROC

PROC SERVER_PROCESS_LOCATION_OBJECTIVES(INT iloc)

INT iteam
INT iteamrepeat
FLOAT ftimemultiplier
INT ipriority[FMMC_MAX_TEAMS]
BOOL bwasobjective[FMMC_MAX_TEAMS]

IF NOT bIsLocalPlayerHost
	FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
		IF HAS_NET_TIMER_STARTED(tdtimesincelastupdate[iloc][iteam])
			RESET_NET_TIMER(tdtimesincelastupdate[iloc][iteam])
		ENDIF
	ENDFOR
	EXIT
ENDIF
	
	FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
		PRINTLN("[SERVER_PROCESS_LOCATION_OBJECTIVES] - iTeam: ", iTeam, " iloc: ", iloc, " iGotoLocationDataRule: ", MC_serverBD_4.iGotoLocationDataRule[iloc][iteam])
		IF MC_serverBD_4.iGotoLocationDataRule[iloc][iteam] != FMMC_OBJECTIVE_LOGIC_NONE
			IF MC_serverBD_4.iGotoLocationDataPriority[iloc][iteam] <= iCurrentHighPriority[iteam]
			AND MC_serverBD_4.iGotoLocationDataPriority[iloc][iteam] < FMMC_MAX_RULES
				icurrentHighPriority[iteam] = MC_serverBD_4.iGotoLocationDataPriority[iloc][iteam]
				itempLocMissionLogic[iteam] = MC_serverBD_4.iGotoLocationDataRule[iloc][iteam] 
				IF iOldHighPriority[iteam] > icurrentHighPriority[iteam]
					iNumHighPriorityLoc[iteam] = 0
					iOldHighPriority[iteam] = icurrentHighPriority[iteam]
				ENDIF
				iNumHighPriorityObj[iteam] = 0
				itempObjMissionLogic[iteam] = FMMC_OBJECTIVE_LOGIC_NONE
				itempObjMissionSubLogic[iteam] = ci_HACK_SUBLOGIC_NONE
				iNumHighPriorityVeh[iteam] = 0
				itempVehMissionLogic[iteam] = FMMC_OBJECTIVE_LOGIC_NONE
				iNumHighPriorityPed[iteam] = 0
				iNumHighPriorityDeadPed[iteam] = 0
				itempPedMissionLogic[iteam] = FMMC_OBJECTIVE_LOGIC_NONE
				iNumHighPriorityPlayerRule[iteam] = 0
				itempPlayerRuleMissionLogic[iteam] = FMMC_OBJECTIVE_LOGIC_NONE
				iNumHighPriorityLoc[iteam]++
			ENDIF
		ENDIF
	ENDFOR
	
	IF NOT IS_RUNNING_BACK_REMIX_SUDDEN_DEATH()
		IF (IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_SUDDEN_DEATH_KILLALLENEMIES)
		OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN))
		AND IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH)
			EXIT
		ENDIF
	ENDIF
	
	FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
		
		IF (NOT HAS_TEAM_FAILED(iTeam))
		AND (NOT HAS_TEAM_FINISHED(iTeam))
			
			IF NOT IS_LOCATION_READY_FOR_PROCESSING(iLoc)
				EXIT
			ENDIF
			
			#IF IS_DEBUG_BUILD
				// Dave W, trying to get to the bottom of 2066321
				IF bWdLocDebug
					PRINTLN("[LeaveArea]   MC_serverBD_4.iGotoLocationDataRule[iloc][iteam] = ", MC_serverBD_4.iGotoLocationDataRule[iloc][iteam], " iteam = ", iteam, " iloc = ", iloc)
				ENDIF
			#ENDIF
			
			IF MC_serverBD_4.iGotoLocationDataRule[iloc][iteam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
				IF MC_serverBD_4.iGotoLocationDataPriority[iloc][iteam] < FMMC_MAX_RULES				
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iAreaRetained, MC_ServerBD_4.iCurrentHighestPriority[iTeam])
					AND IS_AREA_ABLE_TO_BE_CAPTURED(iTeam, iLoc, TRUE)					
						IF MC_serverBD.iLocOwner[iloc] = iteam							
							IF NOT HAS_NET_TIMER_STARTED(MC_serverBD.tdHoldTimer[iloc][iteam])
								START_NET_TIMER(MC_serverBD.tdHoldTimer[iloc][iteam])
								
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciSYNCED_CAPTURE_SCORING)
									IF NOT HAS_NET_TIMER_STARTED(MC_serverBD.tdCapturePointsTimer)
										START_NET_TIMER(MC_serverBD.tdCapturePointsTimer)
										SET_BIT(MC_serverBD.iServerBitset6, SBBOOL6_USING_SYNCED_SCORE_TIMER)
									ENDIF
								ENDIF
							ELSE
								IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdHoldTimer[iloc][iteam]) > MC_serverBD.iGotoLocationDataTakeoverTime[iteam][MC_serverBD_4.iGotoLocationDataPriority[iloc][iteam]]
									
									BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM_HELD_AREA,0,iteam,-1,NULL,ci_TARGET_LOCATION,iloc)
									
									IF NOT IS_BIT_SET(MC_serverBD.iServerBitset6, SBBOOL6_USING_SYNCED_SCORE_TIMER)
										INCREMENT_SERVER_TEAM_SCORE(iteam, MC_serverBD_4.iGotoLocationDataPriority[iloc][iteam], GET_FMMC_POINTS_FOR_TEAM(iteam,MC_serverBD_4.iGotoLocationDataPriority[iloc][iteam]))
									
										PRINTLN("[MC] SERVER_PROCESS_LOCATION_OBJECTIVES - Increment score call 1")
									ENDIF
									IF MC_serverBD.iRuleReCapture[iteam][MC_serverBD_4.iGotoLocationDataPriority[iloc][iteam]]<= 0
										FOR iteamrepeat = 0 TO (MC_serverBD.iNumberOfTeams-1)
										
											ipriority[iteamrepeat] = MC_serverBD_4.iGotoLocationDataPriority[iloc][iteamrepeat] 
											
											MC_serverBD_1.inumberOfTeamInArea[iloc][iteamrepeat] = 0
											RESET_NET_TIMER(tdtimesincelastupdate[iloc][iteamrepeat])
											RESET_NET_TIMER(MC_serverBD.tdAreaTimer[iloc][iteamrepeat])
											MC_serverBD_1.iNumTeamControlKills[iloc][iteamrepeat] = 0
											MC_serverBD_1.iOldTeamControlkills[iloc][iteamrepeat] = 0
											
											IF iteamrepeat=iteam
												IF IS_BIT_SET(MC_serverBD.iLocteamFailBitset[iloc],iteam)
													CLEAR_BIT(MC_serverBD.iLocteamFailBitset[iloc],iteam)
												ENDIF
											ELSE
												IF DOES_TEAM_LIKE_TEAM(iteamrepeat,iteam)
													IF IS_BIT_SET(MC_serverBD.iLocteamFailBitset[iloc],iteamrepeat)
														CLEAR_BIT(MC_serverBD.iLocteamFailBitset[iloc],iteamrepeat)
													ENDIF
												ENDIF
											ENDIF
										ENDFOR
										FOR iteamrepeat = 0 TO (MC_serverBD.iNumberOfTeams-1)
											IF iteamrepeat!=iteam
												IF IS_BIT_SET(MC_serverBD.iLocteamFailBitset[iloc],iteamrepeat)
													IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteamrepeat].iTeamFailBitset,ipriority[iteamrepeat])
														
														SET_TEAM_FAILED(iteam,mFail_LOC_CAPTURED)
														NET_PRINT(" MISSION FAILED FOR TEAM:")NET_PRINT_INT(iteam) NET_PRINT(" BECAUSE loc CAPTURED: ") NET_PRINT_INT(iloc) NET_NL()
													ENDIF
													bwasobjective[iteamrepeat] = TRUE
													CLEAR_BIT(MC_serverBD.iLocteamFailBitset[iloc],iteamrepeat)
												ENDIF
												IF DOES_TEAM_LIKE_TEAM(iteamrepeat,iteam)
													SET_TEAM_OBJECTIVE_OUTCOME(iteamrepeat,ipriority[iteamrepeat],TRUE)
													PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_LOCATION,iloc,iteamrepeat,TRUE)
												ELSE
													SET_TEAM_OBJECTIVE_OUTCOME(iteamrepeat,ipriority[iteamrepeat], FALSE)
													PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_LOCATION,iloc,iteamrepeat,FALSE)
												ENDIF
											ELSE
												SET_TEAM_OBJECTIVE_OUTCOME(iteamrepeat,ipriority[iteamrepeat],TRUE)
												PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_LOCATION,iloc,iteamrepeat,TRUE)
											ENDIF
											MC_serverBD.iReasonForObjEnd[iteamrepeat] = OBJ_END_REASON_LOC_CAPTURED
	
											MC_serverBD.iLocOwner[iloc]=-1

											NET_PRINT("SETTING IGNORE CAPTURE LOC FOR TEAM :  ") NET_PRINT_INT(iteamrepeat) NET_NL()
										ENDFOR
									ENDIF
									
									IF MC_serverBD_4.iGotoLocationDataPriority[iloc][iteam] < FMMC_MAX_RULES
										IF MC_serverBD.iRuleReCapture[iteam][MC_serverBD_4.iGotoLocationDataPriority[iloc][iteam]]!= UNLIMITED_CAPTURES_KILLS
											MC_serverBD.iRuleReCapture[iteam][MC_serverBD_4.iGotoLocationDataPriority[iloc][iteam]]--
										ENDIF
									ENDIF
									FOR iteamrepeat = 0 TO (MC_serverBD.iNumberOfTeams-1)
										IF bwasobjective[iteamrepeat]
											IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_LOCATION,iteamrepeat,ipriority[iteamrepeat])
												SET_LOCAL_OBJECTIVE_END_MESSAGE(iteamrepeat,ipriority[iteamrepeat],MC_serverBD.iReasonForObjEnd[iteamrepeat])
												BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iteamrepeat],iteamrepeat,ipriority[iteamrepeat],DID_TEAM_PASS_OBJECTIVE(iteamrepeat,ipriority[iteamrepeat]))
											ENDIF
										ENDIF
									ENDFOR
									RESET_NET_TIMER(MC_serverBD.tdHoldTimer[iloc][iteam])
								ENDIF
							ENDIF
						ELSE
							IF HAS_NET_TIMER_STARTED(MC_serverBD.tdHoldTimer[iloc][iteam])
								RESET_NET_TIMER(MC_serverBD.tdHoldTimer[iloc][iteam])
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF MC_serverBD_1.inumberOfTeamInArea[iloc][iteam] > 0 AND IS_AREA_ABLE_TO_BE_CAPTURED(iteam, iloc) AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
				OR (IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType) AND iHostileTakeoverInZoneCount[iloc][iteam] != 0 AND IS_AREA_ABLE_TO_BE_CAPTURED(iteam, iloc))
					IF NOT HAS_NET_TIMER_STARTED(MC_serverBD.tdAreaTimer[iloc][iteam])
						
						REINIT_NET_TIMER(MC_serverBD.tdAreaTimer[iloc][iteam])										
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciCOUNT_PERSONAL_LOCATE_SCORE)
							IF MC_serverBD_2.playerLocOwner[iloc][iteam] != INVALID_PLAYER_INDEX()
								MC_serverBD_2.playerLocOwner[iloc][iteam] = INVALID_PLAYER_INDEX()
								PRINTLN("[RCC MISSION] Clearing MC_serverBD_2.playerLocOwner[",iloc,"][",iteam,"] to INVALID_PLAYER_INDEX()")
							ENDIF
						ENDIF
					ELSE
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciCOUNT_PERSONAL_LOCATE_SCORE)
							IF GET_PLAYER_IN_CAPTURE_AREA(iloc) != INVALID_PLAYER_INDEX()
							AND (MC_serverBD_2.playerLocOwner[iloc][iteam] = INVALID_PLAYER_INDEX() OR IS_PED_INJURED(GET_PLAYER_PED(MC_serverBD_2.playerLocOwner[iloc][iteam]))) 
								MC_serverBD_2.playerLocOwner[iloc][iteam] = GET_PLAYER_IN_CAPTURE_AREA(iloc)
								PRINTLN("[MC] SERVER_PROCESS_LOCATION_OBJECTIVES - Setting MC_serverBD_2.playerLocOwner[",iLoc,"][",iTeam,"] to: ", NATIVE_TO_INT(MC_serverBD_2.playerLocOwner[iloc][iteam]))
							ENDIF
						ENDIF
						
						IF NOT HAS_NET_TIMER_STARTED(tdtimesincelastupdate[iloc][iteam])
							REINIT_NET_TIMER(tdtimesincelastupdate[iloc][iteam])
						ELIF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdtimesincelastupdate[iloc][iteam]) > 333
							
							IF MC_serverBD_1.inumberOfTeamInArea[iloc][iteam] > 1
								
								FLOAT fcontrolmulti = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iCaptureMultiplier[iteam]) / 100.0
								ftimemultiplier = (MC_serverBD_1.inumberOfTeamInArea[iloc][iteam]-1) * fcontrolmulti
								
								//NET_PRINT("setting team number time adjust for Area: ") NET_PRINT_INT(iloc) NET_PRINT(" team: ") NET_PRINT_INT(iteam) NET_PRINT(" multiplier: ")  NET_PRINT_FLOAT(ftimemultiplier) NET_NL()
								//NET_PRINT("setting offset to timer of: ")  NET_PRINT_INT(ROUND(-1*ftimemultiplier*GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdtimesincelastupdate[iloc][iteam]))) NET_NL()
								//IF HAS_NET_TIMER_STARTED(tdtimesincelastupdate[iloc][iteam])
									//MC_serverBD.tdAreaTimer[iloc][iteam].Timer = GET_TIME_OFFSET(MC_serverBD.tdAreaTimer[iloc][iteam].Timer, ROUND(-1*ftimemultiplier*GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdtimesincelastupdate[iloc][iteam])))
								//ENDIF
								//NET_PRINT("time difference from current: ")	NET_PRINT_INT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdAreaTimer[iloc][iteam])) NET_NL()
								
								MC_serverBD.tdAreaTimer[iloc][iteam].Timer = GET_TIME_OFFSET(MC_serverBD.tdAreaTimer[iloc][iteam].Timer, ROUND(-1*ftimemultiplier*GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdtimesincelastupdate[iloc][iteam])))
								
							ENDIF
							
							REINIT_NET_TIMER(tdtimesincelastupdate[iloc][iteam])
							
							
							IF MC_serverBD_1.iNumTeamControlKills[iloc][iteam] > MC_serverBD_1.iOldTeamControlkills[iloc][iteam]
								//NET_PRINT("old number of kills in prop: ") NET_PRINT_INT(iproperty) NET_PRINT(" for team: ") NET_PRINT_INT(iteamrepeat) NET_PRINT(" kills: ")  NET_PRINT_INT(GP_serverBD.ServerGangProp[iproperty].ioldkillsinprop[iteamrepeat]) NET_NL()
								//NET_PRINT("new number of kills in prop: ") NET_PRINT_INT(iproperty) NET_PRINT(" for team: ") NET_PRINT_INT(iteamrepeat) NET_PRINT(" kills: ")  NET_PRINT_INT(GP_serverBD.ServerGangProp[iproperty].ikillsinprop[iteamrepeat]) NET_NL()
								MC_serverBD.tdAreaTimer[iloc][iteam].Timer = GET_TIME_OFFSET(MC_serverBD.tdAreaTimer[iloc][iteam].Timer ,(-1*(MC_serverBD_1.iNumTeamControlKills[iloc][iteam] - MC_serverBD_1.iOldTeamControlkills[iloc][iteam])*2*1000))
								NET_PRINT("setting kill time adjust for Area: ") NET_PRINT_INT(iloc) NET_PRINT(" team: ") NET_PRINT_INT(iteam) NET_PRINT(" seconds gained: ")  NET_PRINT_INT((MC_serverBD_1.iNumTeamControlKills[iloc][iteam] - MC_serverBD_1.iOldTeamControlkills[iloc][iteam])) NET_NL()
								MC_serverBD_1.iOldTeamControlkills[iloc][iteam] = MC_serverBD_1.iNumTeamControlKills[iloc][iteam]
							ENDIF
							IF MC_serverBD_4.iGotoLocationDataPriority[iloc][iteam] < FMMC_MAX_RULES
								IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdAreaTimer[iloc][iteam]) >(2*MC_serverBD.iGotoLocationDataTakeoverTime[iteam][MC_serverBD_4.iGotoLocationDataPriority[iloc][iteam]]) 
									BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM_CON_AREA,0,iteam,-1,NULL,ci_TARGET_LOCATION,iloc)
									INCREMENT_SERVER_TEAM_SCORE(iteam, MC_serverBD_4.iGotoLocationDataPriority[iloc][iteam], GET_FMMC_POINTS_FOR_TEAM(iteam,MC_serverBD_4.iGotoLocationDataPriority[iloc][iteam]))
									
									IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciCOUNT_PERSONAL_LOCATE_SCORE)
										IF MC_serverBD_2.playerLocOwner[iloc][iteam] = INVALID_PLAYER_INDEX()
											PLAYER_INDEX tempPlayer = GET_PLAYER_IN_CAPTURE_AREA(iloc, FALSE)
											IF tempPlayer != INVALID_PLAYER_INDEX()
												PRINTLN("[RCC MISSION] ciCOUNT_PERSONAL_LOCATE_SCORE - Grabbing a backup player for scores as one wasn't set.")
												MC_serverBD_2.playerLocOwner[iloc][iteam] = tempPlayer
											ENDIF
										ENDIF
									ENDIF
									PRINTLN("[MC] SERVER_PROCESS_LOCATION_OBJECTIVES - Increment score call 2")
									IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
										IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iAreaRetained, MC_ServerBD_4.iCurrentHighestPriority[iTeam])
										OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciCHANGE_BLIP_COLOUR_TO_CONTROLLING_TEAM)
											NET_PRINT(" LOCATION: ") NET_PRINT_INT(iloc) NET_PRINT(" IS NOW HELD BY TEAM: ") NET_PRINT_INT(iteam) NET_NL()
											MC_serverBD.iLocOwner[iloc] = iteam
										ELSE
											MC_serverBD.iLocOwner[iloc]=-1
										ENDIF
									ENDIF
									
									
									IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
										FOR iteamrepeat = 0 TO (MC_serverBD.iNumberOfTeams-1)
											iPriority[iteamrepeat] = MC_serverBD_4.iGotoLocationDataPriority[iloc][iteamrepeat] 
											SERVER_RESET_CAPTURE_LOCATION_DATA(iLoc, iteamrepeat, iteam, iPriority[iteamrepeat], bwasobjective[iteamrepeat])
										ENDFOR
									ELSE
										iPriority[iteam] = MC_serverBD_4.iGotoLocationDataPriority[iloc][iteam] 
										SERVER_RESET_CAPTURE_LOCATION_DATA(iLoc, iteam, iteam, iPriority[iteam], bwasobjective[iteam])
									ENDIF
									FOR iteamrepeat = 0 TO (MC_serverBD.iNumberOfTeams-1)
										IF ipriority[iteamrepeat] < FMMC_MAX_RULES
											IF MC_serverBD.iRuleReCapture[iteamrepeat][MC_serverBD_4.iGotoLocationDataPriority[iloc][iteamrepeat]]<= 0
												IF iteamrepeat!=iteam
													IF IS_BIT_SET(MC_serverBD.iLocteamFailBitset[iloc],iteamrepeat)
														IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteamrepeat].iTeamFailBitset,ipriority[iteamrepeat])
														
															SET_TEAM_FAILED(iteam,mFail_LOC_CAPTURED)
															NET_PRINT(" MISSION FAILED FOR TEAM:")NET_PRINT_INT(iteam) NET_PRINT(" BECAUSE loc CAPTURED: ") NET_PRINT_INT(iloc) NET_NL()
														ENDIF
														bwasobjective[iteamrepeat] = TRUE
														CLEAR_BIT(MC_serverBD.iLocteamFailBitset[iloc],iteamrepeat)
													ENDIF
													IF DOES_TEAM_LIKE_TEAM(iteamrepeat, iteam)
														SET_TEAM_OBJECTIVE_OUTCOME(iteamrepeat,ipriority[iteamrepeat],TRUE)
														PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_LOCATION,iloc,iteamrepeat,TRUE)
													ELSE
														SET_TEAM_OBJECTIVE_OUTCOME(iteamrepeat,ipriority[iteamrepeat],FALSE)
														PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_LOCATION,iloc,iteamrepeat,FALSE)
													ENDIF
												ELSE
													SET_TEAM_OBJECTIVE_OUTCOME(iteamrepeat,ipriority[iteamrepeat],TRUE)
													PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_LOCATION,iloc,iteamrepeat,TRUE)
												ENDIF
												MC_serverBD.iReasonForObjEnd[iteamrepeat] = OBJ_END_REASON_LOC_CAPTURED
												NET_PRINT("1  server processing JOB_COMP_CONTROL_AREA: ") NET_PRINT_INT(iloc)  NET_PRINT(" for Team: ") NET_PRINT_INT(iteamrepeat) NET_NL()
											ENDIF
										ENDIF
									ENDFOR
									IF MC_serverBD_4.iGotoLocationDataPriority[iloc][iteam] < FMMC_MAX_RULES
										IF MC_serverBD.iRuleReCapture[iteam][MC_serverBD_4.iGotoLocationDataPriority[iloc][iteam]]!= UNLIMITED_CAPTURES_KILLS
											MC_serverBD.iRuleReCapture[iteam][MC_serverBD_4.iGotoLocationDataPriority[iloc][iteam]]--
										ENDIF
									ENDIF
									FOR iteamrepeat = 0 TO (MC_serverBD.iNumberOfTeams-1)
										IF bwasobjective[iteamrepeat]
											IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_LOCATION,iteamrepeat,ipriority[iteamrepeat])
												SET_LOCAL_OBJECTIVE_END_MESSAGE(iteamrepeat,ipriority[iteamrepeat],MC_serverBD.iReasonForObjEnd[iteamrepeat])
												BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iteamrepeat],iteamrepeat,ipriority[iteamrepeat],DID_TEAM_PASS_OBJECTIVE(iteamrepeat,ipriority[iteamrepeat]))
											ENDIF
										ENDIF
									ENDFOR
									EXIT
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciCHANGE_BLIP_COLOUR_TO_CONTROLLING_TEAM)
						AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iAreaRetained, MC_ServerBD_4.iCurrentHighestPriority[iTeam])
							IF MC_serverBD.iLocOwner[iloc] > -1
							AND MC_serverBD.iLocOwner[iloc] < FMMC_MAX_TEAMS
								IF MC_serverBD_1.inumberOfTeamInArea[iloc][MC_serverBD.iLocOwner[iloc]] <= 0
									IF MC_serverBD.iLocOwner[iloc] != -1
										MC_serverBD.iLocOwner[iloc] = -1
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdtimesincelastupdate[iloc][iteam]) > 333
						IF HAS_NET_TIMER_STARTED(tdtimesincelastupdate[iloc][iteam])
							//NET_PRINT("reducing take over time for property: ") NET_PRINT_INT(iproperty) NET_PRINT("by: ") NET_PRINT_INT((inetworktimer - itimesincelastupdate[iproperty][iteamrepeat])) NET_NL()
							MC_serverBD.tdAreaTimer[iloc][iteam].Timer  = GET_TIME_OFFSET(MC_serverBD.tdAreaTimer[iloc][iteam].Timer , GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdtimesincelastupdate[iloc][iteam]))
						ENDIF
						//NET_PRINT("setting time since last update for property: ") NET_PRINT_INT(iproperty) NET_NL()
						REINIT_NET_TIMER(tdtimesincelastupdate[iloc][iteam])
					ENDIF
				ENDIF
			ELIF MC_serverBD_4.iGotoLocationDataRule[iloc][iteam] = FMMC_OBJECTIVE_LOGIC_GO_TO			
				IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iWholeTeamAtLocation[iteam] != ciGOTO_LOCATION_INDIVIDUAL
					//The more than one player stuff here is a backup, it should be handled by clients
					
					INT iPlayersNeeded, iPlayersInLoc
					INT iTeamsBS = 0
					
					IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iWholeTeamAtLocation[iteam] = ciGOTO_LOCATION_WHOLE_TEAM
						
						iPlayersNeeded = MC_serverBD.iNumberOfPlayingPlayers[iteam]
						iPlayersInLoc = MC_serverBD_1.iNumberOfTeamInArea[iloc][iteam]
						
						SET_BIT(iTeamsBS, iteam)
						
					ELIF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iWholeTeamAtLocation[iteam] = ciGOTO_LOCATION_ALL_PLAYERS
						
						iPlayersNeeded = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]
						iPlayersInLoc = MC_serverBD_1.inumberOfTeamInAnyArea[iloc][0] + MC_serverBD_1.inumberOfTeamInAnyArea[iloc][1] + MC_serverBD_1.inumberOfTeamInAnyArea[iloc][2] + MC_serverBD_1.inumberOfTeamInAnyArea[iloc][3]
						
						iTeamsBS = 15 //Set bit 0, 1, 2 and 3
						
					ELIF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iWholeTeamAtLocation[iteam] = ciGOTO_LOCATION_CUSTOM_TEAMS
						
						INT iTeamLoop
						
						FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS,ciLoc_BS_CustomTeams_T0_Needs_T0 + (iTeam * 4) + iTeamLoop)
								iPlayersNeeded += MC_serverBD.iNumberOfPlayingPlayers[iTeamLoop]
								iPlayersInLoc = MC_serverBD_1.inumberOfTeamInAnyArea[iloc][iTeamLoop]
								
								SET_BIT(iTeamsBS, iTeamLoop)
							ENDIF
						ENDFOR
						
					ELSE //Custom no. players:
						
						iPlayersNeeded = 2 + (g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iWholeTeamAtLocation[iTeam] - ciGOTO_LOCATION_2_PLAYERS)
						iPlayersInLoc = MC_serverBD_1.inumberOfTeamInAnyArea[iloc][0] + MC_serverBD_1.inumberOfTeamInAnyArea[iloc][1] + MC_serverBD_1.inumberOfTeamInAnyArea[iloc][2] + MC_serverBD_1.inumberOfTeamInAnyArea[iloc][3]
						
						iTeamsBS = 15 //Set bit 0, 1, 2 and 3
						
					ENDIF
					
					IF iPlayersNeeded > 0
					AND iPlayersInLoc > 0
						
						IF iPlayersInLoc >= iPlayersNeeded
						AND IS_ANYONE_IN_SECONDARY_LOCATE_IF_THEY_NEED_TO_BE(iloc, iTeamsBS)
							
							ipriority[iteam] = MC_serverBD_4.iGotoLocationDataPriority[iloc][iteam] 
							
							IF ipriority[iteam] <= MC_serverBD_4.iCurrentHighestPriority[iteam]
								IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[ipriority[iteam]], ciBS_RULE9_ALL_PLAYERS_IN_ANY_LOCATE)
									PRINTLN("[RCC MISSION] - SERVER setting arrive at non-individual location complete, iWholeTeamAtLocation ",g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iWholeTeamAtLocation[iteam],", loc: ",iloc," team:  ",iteam)
									MC_serverBD.iReasonForObjEnd[iteam] = OBJ_END_REASON_ARV_LOC
									
									IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iWholeTeamAtLocation[iteam] = ciGOTO_LOCATION_WHOLE_TEAM
										BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM_ARV_LOC,0,iteam,iloc,NULL,ci_TARGET_LOCATION,iloc)
									ENDIF

									INCREMENT_SERVER_TEAM_SCORE(iteam, ipriority[iteam], GET_FMMC_POINTS_FOR_TEAM(iteam,ipriority[iteam]))
									
									SET_TEAM_OBJECTIVE_OUTCOME(iteam,ipriority[iteam],TRUE)
									
									//Add an event here for speed boost
									
									PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_LOCATION,iloc,iteam,TRUE)
									
									IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_LOCATION,iteam,ipriority[iteam])
										SET_LOCAL_OBJECTIVE_END_MESSAGE(iteam,ipriority[iteam],MC_serverBD.iReasonForObjEnd[iteam])
										BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iteam],iteam,ipriority[iteam],DID_TEAM_PASS_OBJECTIVE(iteam,ipriority[iteam]))
									ENDIF
									
									RECALCULATE_OBJECTIVE_LOGIC(iteam)
								ENDIF
							ENDIF
							
						ENDIF
					ENDIF
					
				ENDIF
			ELIF MC_serverBD_4.iGotoLocationDataRule[iloc][iteam] = FMMC_OBJECTIVE_LOGIC_LEAVE_LOCATION
				#IF IS_DEBUG_BUILD
					// Dave W, trying to get to the bottom of 2066321
					IF bWdLocDebug
						PRINTLN("[LeaveArea] Hit FMMC_OBJECTIVE_LOGIC_LEAVE_LOCATION for iteam = ", iteam)
					ENDIF
				#ENDIF
				
				IF MC_serverBD_4.iGotoLocationDataPriority[iloc][iteam] = MC_serverBD_4.iCurrentHighestPriority[iteam]
				AND IS_LOCATION_READY_FOR_PROCESSING(iLoc)
				
					#IF IS_DEBUG_BUILD
						// Dave W, trying to get to the bottom of 2066321
						IF bWdLocDebug
							PRINTLN("[LeaveArea] Hit MC_serverBD_4.iGotoLocationDataPriority[iloc][iteam] = MC_serverBD_4.iCurrentHighestPriority[iteam] for iteam = ", iteam, " and iloc = ", iloc)
						ENDIF
					#ENDIF
				
					IF IS_BIT_SET(MC_serverBD.iServerBitSet2,SBBOOL2_NEW_PARTICIPANT_LOOP_FINISHED)
						
						INT iPlayersInLoc = -1 // Number of players currently in the location
						INT iPlayersNeeded = -1 // Number of players (or less) there needs to be to pass the rule
						
						IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iWholeTeamAtLocation[iteam] = ciGOTO_LOCATION_INDIVIDUAL
							iPlayersNeeded = MC_serverBD.iNumberOfPlayingPlayers[iteam] - 1
							iPlayersInLoc = MC_serverBD_1.inumberOfTeamInLeaveArea[iloc][iteam]
						ELIF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iWholeTeamAtLocation[iteam] = ciGOTO_LOCATION_WHOLE_TEAM
							iPlayersNeeded = 0
							iPlayersInLoc = MC_serverBD_1.inumberOfTeamInLeaveArea[iloc][iteam]
						ELIF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iWholeTeamAtLocation[iteam] = ciGOTO_LOCATION_ALL_PLAYERS
							iPlayersNeeded = 0
							iPlayersInLoc = (MC_serverBD_1.inumberOfTeamInLeaveArea[iloc][0] + MC_serverBD_1.inumberOfTeamInLeaveArea[iloc][1]+ MC_serverBD_1.inumberOfTeamInLeaveArea[iloc][2]+ MC_serverBD_1.inumberOfTeamInLeaveArea[iloc][3])
						ELIF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iWholeTeamAtLocation[iteam] = ciGOTO_LOCATION_CUSTOM_TEAMS
							
							iPlayersNeeded = 0
							iPlayersInLoc = 0 // Initialise
							
							INT iTeamLoop
							
							FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS,ciLoc_BS_CustomTeams_T0_Needs_T0 + (iTeam * 4) + iTeamLoop)
									iPlayersInLoc = MC_serverBD_1.inumberOfTeamInLeaveArea[iloc][iTeamLoop]
								ENDIF
							ENDFOR
							
						ELSE //Custom no. players need to leave the locate:
							
							//iPlayersNeeded = total no. players - custom no. to leave
							iPlayersNeeded = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3] - (2 + (g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iWholeTeamAtLocation[iTeam] - ciGOTO_LOCATION_2_PLAYERS))
							iPlayersInLoc = MC_serverBD_1.inumberOfTeamInLeaveArea[iloc][0] + MC_serverBD_1.inumberOfTeamInLeaveArea[iloc][1] + MC_serverBD_1.inumberOfTeamInLeaveArea[iloc][2] + MC_serverBD_1.inumberOfTeamInLeaveArea[iloc][3]
							
						ENDIF
						
						#IF IS_DEBUG_BUILD
							// Dave W, trying to get to the bottom of 2066321
							//IF bWdLocDebug
								PRINTLN("[LeaveArea] about to check if (iLocalBoolCheck6,LBOOL6_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC + iteam should be set for iteam = ", iteam)
								PRINTLN("[LeaveArea] about to check if (iLocalBoolCheck6,LBOOL6_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC + iteam should be set iPlayersInLoc = ", iPlayersInLoc)
								PRINTLN("[LeaveArea] about to check if (iLocalBoolCheck6,LBOOL6_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC + iteam should be set iPlayersNeeded = ", iPlayersNeeded)								
								PRINTLN("[LeaveArea] about to check if (iLocalBoolCheck6,LBOOL6_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC + iteam should be set g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iWholeTeamAtLocation[iteam] = ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iWholeTeamAtLocation[iteam])
							//ENDIF
						#ENDIF
						
						IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iWholeTeamAtLocation[iteam] != ciGOTO_LOCATION_INDIVIDUAL
						AND iPlayersInLoc > iPlayersNeeded
							#IF IS_DEBUG_BUILD
								// Dave W, trying to get to the bottom of 2066321
								IF bWdLocDebug
									PRINTLN("[LeaveArea] Setting (iLocalBoolCheck6,LBOOL6_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC + iteam for iteam = ", iteam)
								ENDIF
							#ENDIF
							
							SET_BIT(iLocalBoolCheck6,LBOOL6_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC + iteam)
						ENDIF
						
						IF iPlayersInLoc != -1
						AND iPlayersNeeded != -1
							IF iPlayersInLoc <= iPlayersNeeded
								ipriority[iteam] = MC_serverBD_4.iGotoLocationDataPriority[iloc][iteam] 
								IF ipriority[iteam] <= MC_serverBD_4.iCurrentHighestPriority[iteam]
									PRINTLN("[RCC MISSION] - SERVER setting leave location complete for leave area w WholeTeamAtLocation ",g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iWholeTeamAtLocation[iteam],", loc: ",iloc," team:  ",iteam)
									
									MC_serverBD.iReasonForObjEnd[iteam] = OBJ_END_REASON_LVE_LOC
									
									BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM_LVE_LOC,0,iteam,iloc,NULL,ci_TARGET_LOCATION,iloc)

									INCREMENT_SERVER_TEAM_SCORE(iteam, ipriority[iteam], GET_FMMC_POINTS_FOR_TEAM(iteam,ipriority[iteam]))
									
									SET_TEAM_OBJECTIVE_OUTCOME(iteam,ipriority[iteam],TRUE)
									PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_LOCATION,iloc,iteam,TRUE)
									
									IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_LOCATION,iteam,ipriority[iteam])
										SET_LOCAL_OBJECTIVE_END_MESSAGE(iteam,ipriority[iteam],MC_serverBD.iReasonForObjEnd[iteam])
										BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iteam],iteam,ipriority[iteam],DID_TEAM_PASS_OBJECTIVE(iteam,ipriority[iteam]))
									ENDIF
									
									RECALCULATE_OBJECTIVE_LOGIC(iteam)
									
								ENDIF
							ELSE
								PRINTLN("[RCC MISSION] - SERVER setting leave location complete for leave area but we havent left yet")
							ENDIF
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR

ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: CLIENT LOCATION PROCESSING / LOCAL PROCESS LOCATION OBJECTIVES !
//
//************************************************************************************************************************************************************

FUNC FLOAT CALCULATE_MINIMUM_DISTANCE_BETWEEN_TEAM_MATES(INT iLoc, INT iTeam)

FLOAT fDistance
INT iTeamSizeThreshold

	IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iTeamSizeThreshold = - 1
		iTeamSizeThreshold = 4
	ELSE
		iTeamSizeThreshold = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iTeamSizeThreshold
	ENDIF
	
	IF MC_serverBD.iNumberOfPlayingPlayers[iTeam] < iTeamSizeThreshold + 1 
		fDistance = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fTeamMembersNeededWithin
	ELSE
		fDistance = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fTeamMembersNeededWithin + ((MC_serverBD.iNumberOfPlayingPlayers[iTeam] - iTeamSizeThreshold) * g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fTeamMembersNeededToleranceIncrease)
	ENDIF
	
	
RETURN fDistance

ENDFUNC

FUNC BLIP_SPRITE GET_BLIP_SPRITE_FOR_LOCATE(INT iBlipSprite)
	SWITCH iBlipSprite
		CASE LOCATE_BLIP_VOLTIC 		RETURN RADAR_TRACE_EX_VECH_6
		CASE LOCATE_BLIP_RUINER			RETURN RADAR_TRACE_EX_VECH_3
		CASE LOCATE_BLIP_DUNE4			RETURN RADAR_TRACE_EX_VECH_4
		CASE LOCATE_BLIP_PHANTOM		RETURN RADAR_TRACE_EX_VECH_1
		CASE LOCATE_BLIP_TECHNICAL		RETURN RADAR_TRACE_EX_VECH_7
		CASE LOCATE_BLIP_BOXVILLE		RETURN RADAR_TRACE_EX_VECH_2
		CASE LOCATE_BLIP_WASTELANDER	RETURN RADAR_TRACE_EX_VECH_5
		CASE LOCATE_BLIP_BLAZER			RETURN RADAR_TRACE_QUAD
		CASE LOCATE_BLIP_NUM_1			RETURN RADAR_TRACE_CAPTURE_1
		CASE LOCATE_BLIP_NUM_2			RETURN RADAR_TRACE_CAPTURE_2
		CASE LOCATE_BLIP_NUM_3			RETURN RADAR_TRACE_CAPTURE_3
		CASE LOCATE_BLIP_NUM_4			RETURN RADAR_TRACE_CAPTURE_4
		CASE LOCATE_BLIP_NUM_5			RETURN RADAR_TRACE_CAPTURE_5
		CASE LOCATE_BLIP_TAR_A			RETURN RADAR_TRACE_TARGET_A
		CASE LOCATE_BLIP_TAR_B			RETURN RADAR_TRACE_TARGET_B
		CASE LOCATE_BLIP_TAR_C			RETURN RADAR_TRACE_TARGET_C
		CASE LOCATE_BLIP_TAR_D			RETURN RADAR_TRACE_TARGET_D
		CASE LOCATE_BLIP_TAR_E			RETURN RADAR_TRACE_TARGET_E
		CASE LOCATE_BLIP_TAR_F			RETURN RADAR_TRACE_TARGET_F
		CASE LOCATE_BLIP_TAR_G			RETURN RADAR_TRACE_TARGET_G
		CASE LOCATE_BLIP_TAR_H			RETURN RADAR_TRACE_TARGET_H
		CASE LOCATE_BLIP_RAPPEL			RETURN RADAR_TRACE_RAPPEL
	ENDSWITCH
	RETURN RADAR_TRACE_OBJECTIVE
ENDFUNC

PROC SET_LOCATE_BLIP_SPRITE_OVERRIDE(BLIP_INDEX BlipToChange, INT iBlipIndex)
	IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iBlipIndex].iLocateBlipSprite != 0
		PRINTLN("JT BLIPPIN ELL blip num: ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iBlipIndex].iLocateBlipSprite)
		BLIP_SPRITE tempBlipSprite
		
		tempBlipSprite = GET_BLIP_SPRITE_FOR_LOCATE(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iBlipIndex].iLocateBlipSprite)
		SET_BLIP_SPRITE(BlipToChange, tempBlipSprite)
		
		PRINTLN("JT BLIPPIN ELL ", GET_BLIP_SPRITE_DEBUG_STRING(tempBlipSprite))
		PRINTLN("JT BLIPPIN ELL ", GET_BLIP_SPRITE_DEBUG_STRING(GET_BLIP_SPRITE(BlipToChange)))
		
		IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iBlipIndex].iLocateBlipSprite >= LOCATE_BLIP_VOLTIC
		AND g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iBlipIndex].iLocateBlipSprite <= LOCATE_BLIP_BLAZER
			SET_BLIP_COLOUR(BlipToChange, BLIP_COLOUR_BLUEDARK)
		ELSE
			SET_BLIP_COLOUR(BlipToChange, GET_BLIP_COLOUR_FROM_CREATOR(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iBlipIndex].sLocBlipStruct.iBlipColour))
		ENDIF
	ENDIF
ENDPROC
HUD_COLOURS WinningTeamColour

PROC CREATE_BLIP_FOR_MP_AREA( INT iLoc, INT iPlayerTeam )
	
	

	CDEBUG2LN(DEBUG_MC_BLIP, "[RCC MISSION] Attempting to creating blip for MP area.")
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS, ciLoc_BS_HideCaptAreaOnMap)
	
		CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION] Hide area bit not set. Creating blip for MP area. Location = ", iLoc, " PlayerTeam = ", iPlayerTeam, ".")
	
		IF NOT DOES_BLIP_EXIST(LocBlip1[iloc])
			LocBlip1[iloc] = ADD_BLIP_FOR_RADIUS(GET_LOCATION_VECTOR(iloc), g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fRadius[0])
		ENDIF
		
		IF MC_serverBD.iLocOwner[iloc] = -1
			SET_BLIP_COLOUR( LocBlip1[iloc], BLIP_COLOUR_YELLOW )
		ELIF MC_serverBD.iLocOwner[iloc] = iPlayerTeam	
			WinningTeamColour = GET_HUD_COLOUR_FOR_FMMC_TEAM( MC_serverBD.iLocOwner[iloc], PlayerToUse)
			SET_BLIP_COLOUR( LocBlip1[iloc],  ENUM_TO_INT(WinningTeamColour))
			PRINTLN("[RCC MISSION][AW SMOKE] SETTING BLIP COLOUR - 1")
			//SET_BLIP_COLOUR( LocBlip1[iloc], BLIP_COLOUR_BLUE )
		ELSE
			SET_BLIP_COLOUR_FROM_HUD_COLOUR( LocBlip1[iloc], GET_HUD_COLOUR_FOR_FMMC_TEAM( MC_serverBD.iLocOwner[iloc], PlayerToUse) )
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].sLocBlipStruct.iBlipColour != BLIP_COLOUR_DEFAULT
			PRINTLN("[RCC MISSION][AW SMOKE] SETTING BLIP COLOUR - 2")
			SET_BLIP_COLOUR( LocBlip1[iloc], GET_BLIP_COLOUR_FROM_CREATOR( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].sLocBlipStruct.iBlipColour ) )
		ENDIF
		
		
		
		IF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_CONTROL_AREA
		AND IS_BIT_SET(iLocalBoolCheck14, LBOOL14_VFX_CAPTURE_OWNER)
			IF g_iTheWinningTeam = -1
				//Show Yellow = Tied
				SET_BLIP_COLOUR(LocBlip1[iloc], BLIP_COLOUR_YELLOW)
			ELSE
				WinningTeamColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(g_iTheWinningTeam, PlayerToUse)
				// Winning team
				// 0 - Orange 
				// 1 - Green
				// 2 - Pink
				// 3 - Purple
				PRINTLN( "[RCC MISSION][AW SMOKE] TEAM ",g_iTheWinningTeam, "is winning")
				PRINTLN("[RCC MISSION][AW SMOKE] SETTING BLIP COLOUR - 3")
				SET_BLIP_COLOUR(LocBlip1[iloc], ENUM_TO_INT(WinningTeamColour))
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_CONTESTED_COLOUR_FOR_LOCATES)
				IF IS_ZONE_BEING_CONTESTED(iLoc)
					SET_BLIP_COLOUR(LocBlip1[iloc], BLIP_COLOUR_RED)
				ENDIF
			ENDIF
		ENDIF

		SET_BLIP_ALPHA(LocBlip1[iloc], 150)
		SET_BLIP_PRIORITY(LocBlip1[iloc], BLIPPRIORITY_LOW)
				
	ENDIF
ENDPROC

FUNC BOOL SHOULD_REMOVE_LOCATION_BLIP(INT iLoc, FLOAT fDistToLoc)
	FLOAT fRange = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].sLocBlipStruct.fBlipRange
	FLOAT fHeightRange = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.fBlipHeightDifference
	VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
	VECTOR vLocBlipPos = GET_LOCATION_VECTOR(iloc)
	FLOAT fHeightDifference = ABSF(vLocBlipPos.z - vPlayerPos.z)
	PRINTLN("[ML][SHOULD_REMOVE_LOCATION_BLIP] iLoc: ", iloc, "   Distance between player and loc blip: ", fDistToLoc, "    Height between player and loc blip: ", fHeightDifference, "  fRange: ", fRange, "  fHeightRange: ", fHeightRange)
	
	// Both 'show blip in range' and 'show blip in height range' are set
	IF fRange > 0.0
	AND fHeightRange > 0.0
		PRINTLN("[ML][SHOULD_REMOVE_LOCATION_BLIP] Blip: ", iloc, " - Both show range and show height are set")
		IF fDistToLoc <= fRange
			IF fHeightDifference <= fHeightRange
				RETURN FALSE
			ELSE
				RETURN TRUE
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	
	// Just 'show blip in range' is set
	ELIF fRange > 0.0
		PRINTLN("[ML][SHOULD_REMOVE_LOCATION_BLIP] Blip: ", iloc, " - Just show range is set")
		IF fDistToLoc <= fRange
			RETURN FALSE
		ELSE
			RETURN TRUE
		ENDIF
	
	// Just 'show blip in height range' is set
	ELIF fHeightRange > 0.0
		PRINTLN("[ML][SHOULD_REMOVE_LOCATION_BLIP] Blip: ", iloc, " - Just show height range is set")
		IF fHeightDifference <= fHeightRange
			RETURN FALSE
		ELSE
			RETURN TRUE
		ENDIF
	
	// Neither 'show blip in range' or 'show blip in height range' are set
	ELSE
		RETURN FALSE
	ENDIF
	
ENDFUNC

PROC CREATE_BLIP_FOR_MP_COORD( INT iLoc, INT iPlayerTeam, FLOAT fDistToloc, INT iBlipColour, BOOL bSetRoute, BOOL bEnemyBlip = FALSE )
	INT i
	
	// This function is called every frame even after the blip exists. So needs to be here too.
	IF SHOULD_REMOVE_LOCATION_BLIP(iLoc, fDistToLoc)
		CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION] Location ", iLoc, " SHOULD_REMOVE_LOCATION_BLIP = TRUE")
		
		IF DOES_BLIP_EXIST(LocBlip[iloc])
			CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION] Location ", iLoc, " Removing blip. We are at fDistToLoc: ", fDistToLoc)
			REMOVE_BLIP(LocBlip[iloc])
		ENDIF
		
		EXIT
	ENDIF
	
	IF NOT DOES_BLIP_EXIST( LocBlip[ iloc ] )
		IF NOT IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].iLocBS, ciLoc_BS_HideBlip )	
			
			#IF IS_DEBUG_BUILD
				VECTOR vLoc = GET_LOCATION_VECTOR(iloc)
				CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION] Location ", iLoc, " blip(", vLoc, "): Creating blip.")
			#ENDIF
						
			IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].iLocBS2, ciLoc_BS2_UseLineMarker )	
				//Line blip stuff here

				LocBlip[ iloc ] = ADD_BLIP_FOR_AREA (GET_LOCATION_VECTOR(iloc),1,1)
				//SET_BLIP_ALPHA(LocBlip[iLoc],1)
				SET_BLIP_SPRITE(LocBlip[ iloc ],RADAR_TRACE_EDGE_POINTER) // RADAR_TRACE_EDGE_CROSSTHELINE
				SET_BLIP_ALPHA(LocBlip[iLoc],160)
				
				FOR i = 0 TO  i_NUMBER_OF_GRADIENT_BLIPS -1
					IF NOT DOES_BLIP_EXIST(LineGradientBlips[ i ][ iloc ])
					
						//FLOAT multiplier = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fRadius[0] - (i * (g_FMMC_STRT.sGotoLocationData[iloc].fRadius[0] / 12) )
						//multiplier *= g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].sLocBlipStruct.fBlipScale
						
						//top line - 66
						// + 8.25
						// + 8.25
						// + 8.25
						// + 8.25
						
						FLOAT multiplier
												
						IF i = 0
							multiplier =  g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fRadius[0] 
							multiplier *= g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].sLocBlipStruct.fBlipScale
						ELSE
							multiplier =  g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fRadius[0] - ( i * (g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fRadius[0] / 8))
							multiplier *= g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].sLocBlipStruct.fBlipScale
						ENDIF
						
						IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].iLocBS2, ciLoc_BS2_RunningBackMarker )
							IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].fLineThickness > 0.0
								PRINTLN("[AW_MARKER - Overriding Blip Thickness to - ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].fLineThickness)
								LineGradientBlips[ i ][ iloc ] = ADD_BLIP_FOR_AREA(GET_LOCATION_VECTOR(iloc), multiplier, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].fLineThickness)
							ELSE
								LineGradientBlips[ i ][ iloc ] = ADD_BLIP_FOR_AREA(GET_LOCATION_VECTOR(iloc), multiplier, 5)
							ENDIF
						ELSE
							LineGradientBlips[ i ][ iloc ] = ADD_BLIP_FOR_AREA(GET_LOCATION_VECTOR(iloc), multiplier, 5)
						ENDIF
						
						IF i = i_NUMBER_OF_GRADIENT_BLIPS 
							IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].iLocBS2, ciLoc_BS2_RunningBackMarker )
								IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].iLineOpacity > 0
									SET_BLIP_ALPHA(LineGradientBlips[ i ][ iloc ],g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].iLineOpacity)
									PRINTLN("[AW_MARKER - Overriding Blip Alpha to - ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].iLineOpacity)
								ELSE
									SET_BLIP_ALPHA(LineGradientBlips[ i ][ iloc ], 255) //255
								ENDIF
							ELSE
								SET_BLIP_ALPHA(LineGradientBlips[ i ][ iloc ], 255) //255
							ENDIF
						ELSE
							IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].iLocBS2, ciLoc_BS2_RunningBackMarker )
								IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].iLineOpacity > 0
									SET_BLIP_ALPHA(LineGradientBlips[ i ][ iloc ],g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].iLineOpacity)
									PRINTLN("[AW_MARKER - Overriding Blip Alpha to - ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].iLineOpacity)
								ELSE
									SET_BLIP_ALPHA(LineGradientBlips[ i ][ iloc ], 70) //255
								ENDIF
							ELSE
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciSET_LINES_AS_RUGBY_LINES)
									SET_BLIP_ALPHA(LineGradientBlips[ i ][ iloc ], 128) //255
								ELSE
									SET_BLIP_ALPHA(LineGradientBlips[ i ][ iloc ], 70) //255
								ENDIF
							ENDIF
						ENDIF
						
						SET_BLIP_AS_SHORT_RANGE(LineGradientBlips[i][iloc],TRUE)
						SET_BLIP_ROTATION(LineGradientBlips[i][iloc],ROUND(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fDirection))
						
						IF CONTENT_IS_USING_ARENA()
							SET_BLIP_DISPLAY(LineGradientBlips[i][iloc], DISPLAY_RADAR_ONLY)
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciSET_LINES_AS_RUGBY_LINES)
							IF bEnemyBlip
								PRINTLN("[AW_MARKER -BLIP_COLOUR_ORANGE")
								SET_BLIP_COLOUR( LineGradientBlips[ i ][ iloc ],  BLIP_COLOUR_ORANGE )
							ELSE
								PRINTLN("[AW_MARKER -BLIP_COLOUR_PURPLE")
								SET_BLIP_COLOUR( LineGradientBlips[ i ][ iloc ],  BLIP_COLOUR_PURPLE )
								SET_BLIP_PRIORITY( LineGradientBlips[ i ][ iloc ],BLIP_PRIORITY_HIGHEST_SPECIAL_LOW)
							ENDIF
						ELSE						
							BOOL bBlueBlip = bEnemyBlip
							IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUNNING_BACK_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
								bBlueBlip = (bEnemyBlip
									AND NOT ((IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS3, ciLoc_BS3_RunningBackMarkerAsRedT1)
									AND MC_PlayerBD[iPartToUse].iTeam = 0)
									OR MC_PlayerBD[iPartToUse].iTeam != 0)
									OR NOT ((IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS3, ciLoc_BS3_RunningBackMarkerAsRedT2)
									AND MC_PlayerBD[iPartToUse].iTeam = 1)
									OR MC_PlayerBD[iPartToUse].iTeam != 1))
								PRINTLN("CREATE_BLIP_FOR_MP_COORD - Running back remix - bBlueBlip: ", BOOL_TO_STRING(bBlueBlip))
							ENDIF
					
							IF bBlueBlip
								PRINTLN("[AW_MARKER - BLIP_COLOUR_BLUE 2")
								SET_BLIP_COLOUR( LineGradientBlips[ i ][ iloc ],  BLIP_COLOUR_BLUE )
							ELSE
								PRINTLN("[AW_MARKER - BLIP_COLOUR_RED 2")
								SET_BLIP_COLOUR( LineGradientBlips[ i ][ iloc ],  BLIP_COLOUR_RED )
								SET_BLIP_PRIORITY( LineGradientBlips[ i ][ iloc ],BLIP_PRIORITY_HIGHEST_SPECIAL_LOW)
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
				
				#IF IS_DEBUG_BUILD
					VECTOR vTemp = GET_LOCATION_VECTOR(iloc)
					PRINTLN("[AW_MARKER - CREATE_BLIP_FOR_MP_COORD - CREATING BLIP FOR AREA- v : ", vTemp)
					IF bEnemyBlip
						PRINTLN("[AW_MARKER - CREATE_BLIP_FOR_MP_COORD - CREATING BLIP FOR AREA- enemy f : ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fRadius[iPlayerTeam])
					ELSE
						PRINTLN("[AW_MARKER - CREATE_BLIP_FOR_MP_COORD - CREATING BLIP FOR AREA- f : ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fRadius[iPlayerTeam])
					ENDIF
					PRINTLN("[AW_MARKER - CREATE_BLIP_FOR_MP_COORD - CREATING BLIP FOR AREA- heading : ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fDirection)
				#ENDIF
				//SET_BLIP_ROTATION(LocBlip[ iloc ],ROUND(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fDirection))
			ELSE
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS2, ciLoc_BS2_OnlyShowBlipWhenOffRadar)
					PRINTLN("[AW_MARKER - CREATE_BLIP_FOR_MP_COORD - ciLoc_BS2_OnlyShowBlipWhenOffRadar is set")
					PRINTLN("CREATE_BLIP_FOR_MP_COORD - iloc",iloc)
					//Hack to only show the off screen blip sprite and not show the actual blip on the radar this is why the blip size is so small - AW
					LocBlip[ iloc ] = ADD_BLIP_FOR_AREA( GET_LOCATION_VECTOR(iloc) ,0.01,0.01 ) // ???
					SET_BLIP_SPRITE(LocBlip[ iloc ],RADAR_TRACE_EDGE_POINTER)
					SET_BLIP_ALPHA(LocBlip[ iloc ], 255) 
				ELSE
					PRINTLN("[AW_MARKER - CREATE_BLIP_FOR_MP_COORD - g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].iLocBS is not set")
					PRINTLN("CREATE_BLIP_FOR_MP_COORD - iloc",iloc)
					PRINTLN("[KH] ADDING BLIP(1) - LOC: ", iloc)
					LocBlip[ iloc ] = ADD_BLIP_FOR_COORD( GET_LOCATION_VECTOR(iloc) ) // ???
				ENDIF
				//LocBlip[ iloc ] = ADD_BLIP_FOR_AREA( GET_LOCATION_VECTOR(iloc),1,1 ) // ???
			ENDIF

			SET_BLIP_PRIORITY( LocBlip[ iloc ], BLIPPRIORITY_HIGHEST )
						
			IF bSetRoute
				IF fDisttoLoc > g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].iGPSCutOff[ 0 ]
				AND g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].iGPSCutOff[ 0 ] != -1 //???
					IF NOT USING_HEIST_SPECTATE() AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
						CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] Location ", iLoc, " blip(", vLoc, "): Setting GPS route on for blip.")
						CLEAR_GPS_FLAGS()
						SET_BLIP_ROUTE( LocBlip[ iloc ], TRUE )
						
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS,ciLoc_BS_AvoidHighwayGPS)
							CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] Location ", iLoc, " blip(", vLoc, "): GPS routine using AVOID_HIGHWAY flag.")
							SET_GPS_FLAGS(GPS_FLAG_AVOID_HIGHWAY)
						ELSE
							CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] Location ", iLoc, " blip(", vLoc, "): GPS routine using no flags.")
							SET_GPS_FLAGS(GPS_FLAG_NONE)
						ENDIF
					ELSE
						CLEAR_GPS_FLAGS()
						SET_BLIP_ROUTE( LocBlip[ iloc ], FALSE)
						CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] Location ", iLoc, " blip(", vLoc, "): Setting GPS route OFF for blip because player is spectating")
					ENDIF
				ENDIF
			ENDIF
						
			IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].sLocBlipStruct.iBlipColour != BLIP_COLOUR_DEFAULT
				iBlipColour = GET_BLIP_COLOUR_FROM_CREATOR(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].sLocBlipStruct.iBlipColour)
			ENDIF

			// url:bugstar:2185410
			IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetThree[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE3_IGNORE_GPS_BLOCKING)
					CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] Location ", iLoc, " blip(", vLoc, "): Calling SET_IGNORE_NO_GPS_FLAG(TRUE).")
					SET_IGNORE_NO_GPS_FLAG(TRUE)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].iLocBS2, ciLoc_BS2_UseLineMarker )	
				PRINTLN("[AW_MARKER -iLoc",iLoc)
				PRINTLN("[AW_MARKER -iPlayerTeam",iPlayerTeam)
				PRINTLN("[AW_MARKER -MC_playerBD[ iPartToUse ].iteam ",MC_playerBD[ iPartToUse ].iteam)
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciSET_LINES_AS_RUGBY_LINES)
					IF bEnemyBlip
						PRINTLN("[AW_MARKER -BLIP_COLOUR_ORANGE")
						SET_BLIP_COLOUR( LocBlip[ iLoc ],  BLIP_COLOUR_ORANGE )
					ELSE
						PRINTLN("[AW_MARKER -BLIP_COLOUR_PURPLE")
						SET_BLIP_COLOUR(LocBlip[ iLoc ],  BLIP_COLOUR_PURPLE )
						SET_BLIP_PRIORITY(LocBlip[ iLoc ],BLIP_PRIORITY_HIGHEST_SPECIAL_LOW)
					ENDIF
				ELSE
					BOOL bBlueBlip = bEnemyBlip
					IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUNNING_BACK_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
						bBlueBlip = (bEnemyBlip
							AND NOT ((IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS3, ciLoc_BS3_RunningBackMarkerAsRedT1)
							AND MC_PlayerBD[iPartToUse].iTeam = 0)
							OR MC_PlayerBD[iPartToUse].iTeam != 0)
							OR NOT ((IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS3, ciLoc_BS3_RunningBackMarkerAsRedT2)
							AND MC_PlayerBD[iPartToUse].iTeam = 1)
							OR MC_PlayerBD[iPartToUse].iTeam != 1))
						PRINTLN("CREATE_BLIP_FOR_MP_COORD - Running back remix - bBlueBlip: ", BOOL_TO_STRING(bBlueBlip))
					ENDIF
					
					IF bBlueBlip
						PRINTLN("[AW_MARKER - BLIP_COLOUR_BLUE 1")
						SET_BLIP_COLOUR( LocBlip[ iLoc ],  BLIP_COLOUR_BLUE )
					ELSE
						PRINTLN("[AW_MARKER - BLIP_COLOUR_RED 1")
						SET_BLIP_COLOUR( LocBlip[ iLoc ],  BLIP_COLOUR_RED )
						SET_BLIP_PRIORITY( LocBlip[ iLoc ],BLIP_PRIORITY_HIGHEST_SPECIAL_LOW)
					ENDIF
				ENDIF
				iBlipTeam[iLoc] = MC_playerBD[iPartToUse].iteam
			ELSE
				SET_BLIP_COLOUR( LocBlip[ iLoc ], iBlipColour )
			ENDIF
						
			SET_LOCATE_BLIP_SPRITE_OVERRIDE(LocBlip[iloc], iloc)
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS2, ciLoc_BS2_LowBlipHeightThres)
				SET_BLIP_SHORT_HEIGHT_THRESHOLD(LocBlip[iloc], TRUE)
			ENDIF
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS3, ciLoc_BS3_HighBlipHeightThres)
				SET_BLIP_EXTENDED_HEIGHT_THRESHOLD(LocBlip[iloc], TRUE)
			ENDIF

			IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].sLocBlipStruct.fBlipScale != 1.0
			AND NOT IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].iLocBS2, ciLoc_BS2_UseLineMarker )	
				IF DOES_BLIP_EXIST(LocBlip[iloc])
					SET_BLIP_SCALE(LocBlip[iloc], g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].sLocBlipStruct.fBlipScale)
				ENDIF
			ENDIF
			
			INT iNullCachedPriority = -1 //We don't cache blip names for coords.
			SET_BLIP_CUSTOM_NAME( LocBlip[iloc], iPlayerTeam, MC_serverBD_4.iGotoLocationDataPriority[ iloc ][ iPlayerTeam ], iNullCachedPriority)
			
			IF CONTENT_IS_USING_ARENA()
				SET_BLIP_DISPLAY(LocBlip[iloc], DISPLAY_RADAR_ONLY)
			ENDIF
		ENDIF
	ELSE
		PRINTLN("CREATE_BLIP_FOR_MP_COORD -iloc blip exists",iloc)
		IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].iLocBS2, ciLoc_BS2_UseLineMarker )	
			IF iBlipTeam[iLoc] != MC_playerBD[iPartToUse].iteam
				FOR i = 0 TO  i_NUMBER_OF_GRADIENT_BLIPS -1
					IF DOES_BLIP_EXIST(LineGradientBlips[ i ][ iloc ])
						REMOVE_BLIP(LineGradientBlips[ i ][ iloc ])
					ENDIF
				ENDFOR
				REMOVE_BLIP(LocBlip[ iLoc ])
				PRINTLN("[RCC MISSION] removing blips for line since player team has changed from: ",iBlipTeam[iLoc],"MC_playerBD[iPartToUse].iteam", " for location: ",iloc )
				iBlipTeam[iLoc] = MC_playerBD[iPartToUse].iteam
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CREATE_BLIP_FOR_ENEMY_MARKER_LINES(INT iLoc, Bool bMakeRed = FALSE, BOOL bIgnoreRule = FALSE)

INT iPlayerTeam = MC_playerBD[ iPartToUse ].iteam
INT i

	PRINTLN("[AW_MARKER - CREATE_BLIP_FOR_ENEMY_MARKER_LINES -  ")
	
	IF (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS3, ciLoc_BS3_RunningBackMarkerAsRedT1)
	AND MC_PlayerBD[iPartToUse].iTeam = 0)
	OR (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS3, ciLoc_BS3_RunningBackMarkerAsRedT2)
	AND MC_PlayerBD[iPartToUse].iTeam = 1)
		bMakeRed = TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].iLocBS2, ciLoc_BS2_DisplayOtherTeamsMarker)
		FOR i = 0 TO MC_ServerBD.iNumberOfTeams -1
			IF i != iPlayerTeam
				IF MC_serverBD_4.iGotoLocationDataPriority[iloc][ i ] = MC_serverBD_4.iCurrentHighestPriority[i]
				OR bIgnoreRule
				OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].iLocBS3, ciLoc_BS3_IgnoreRuleForThisBlip)
					IF NOT DOES_BLIP_EXIST( LocBlip[ iloc ] )
						//Line Stuff
						PRINTLN("[AW_MARKER - CREATE_BLIP_FOR_ENEMY_MARKER_LINES - iPlayerTeam  ", iPlayerTeam)
						PRINTLN("[AW_MARKER - CREATE_BLIP_FOR_ENEMY_MARKER_LINES - i  ", i)
						PRINTLN("[AW_MARKER - CREATE_BLIP_FOR_ENEMY_MARKER_LINES - iLoc  ", iLoc)
						IF bMakeRed
							CREATE_BLIP_FOR_MP_COORD( iLoc, i, 0, BLIP_COLOUR_RED, FALSE, TRUE )
							PRINTLN("[AW_MARKER - SETTING BLIP TO BE RED  ", iLoc)
						ELSE
							CREATE_BLIP_FOR_MP_COORD( iLoc, i, 0, BLIP_COLOUR_BLUE, FALSE, TRUE )
							PRINTLN("[AW_MARKER - SETTING BLIP TO BE BLUE  ", iLoc)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
ENDPROC

PROC CREATE_BLIP_FOR_RUGBY_LINE(INT iLoc, BOOL bIgnoreRule = FALSE, BOOL bDisplayRugbyOrangeTeam = FALSE)

INT iPlayerTeam = MC_playerBD[ iPartToUse ].iteam
	PRINTLN("[AW_MARKER - CREATE_BLIP_FOR_RUGBY_LINE -  ")

	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciSET_LINES_AS_RUGBY_LINES)
		IF bIgnoreRule
			IF NOT DOES_BLIP_EXIST( LocBlip[ iloc ] )
				//Line Stuff
				PRINTLN("[AW_MARKER - CREATE_BLIP_FOR_RUGBY_LINE - iLoc  ", iLoc)
				IF bDisplayRugbyOrangeTeam
					CREATE_BLIP_FOR_MP_COORD( iLoc, iPlayerTeam, 0, BLIP_COLOUR_ORANGE, FALSE, bDisplayRugbyOrangeTeam )
					PRINTLN("[AW_MARKER - CREATE_BLIP_FOR_RUGBY_LINE- SETTING BLIP TO BE ORANGE  ", iLoc)
				ELSE
					CREATE_BLIP_FOR_MP_COORD( iLoc, iPlayerTeam, 0, BLIP_COLOUR_PURPLE, FALSE, bDisplayRugbyOrangeTeam )
					PRINTLN("[AW_MARKER - CREATE_BLIP_FOR_RUGBY_LINE- SETTING BLIP TO BE PURPLE  ", iLoc)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

CONST_INT ciAlphaChangeTime	500
INT iTempActivePlayers

PROC DRAW_LOCATE_RADIUS_BLIP(INT iRule, INT iLoc)
	
	INT iAlphaChange
	
	
	IF iRule < FMMC_MAX_RULES
	AND iRule > -1
	AND iLoc < FMMC_MAX_GO_TO_LOCATIONS
	AND iLoc > -1
	AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iPartToUse].iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_PLAYER_LOCATE_RADIUS_BLIP)
		IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fTeamMembersNeededWithin > 0
			INT iAlpha = 175
			
			IF iLoc != iCurrentLocateBlipped
				IF DOES_BLIP_EXIST(biLocateRadiusBlip)
					REMOVE_BLIP(biLocateRadiusBlip)
					PRINTLN("[JS][LOCATEBLIP] - DRAW_LOCATE_RADIUS_BLIP - Removing blip")
				ENDIF
			ENDIF
			
			iAlphaChange = 100 / MC_serverBD.iNumberOfPlayingPlayers[MC_PlayerBD[iPartToUse].iTeam]
			
			IF NOT DOES_BLIP_EXIST(biLocateRadiusBlip)
				biLocateRadiusBlip = ADD_BLIP_FOR_RADIUS(GET_ENTITY_COORDS(PlayerPedToUse,FALSE), CALCULATE_MINIMUM_DISTANCE_BETWEEN_TEAM_MATES(iLoc,MC_playerBD[iPartToUse].iteam))
				PRINTLN("[JS][LOCATEBLIP] - DRAW_LOCATE_RADIUS_BLIP - Adding blip, Radius = ", CALCULATE_MINIMUM_DISTANCE_BETWEEN_TEAM_MATES(iLoc,MC_playerBD[iPartToUse].iteam))
				SET_BLIP_COLOUR_FROM_HUD_COLOUR(biLocateRadiusBlip, GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_playerBD[iPartToUse].iteam, PlayerToUse))
				iTempActivePlayers = MC_serverBD.iNumberOfPlayingPlayers[MC_PlayerBD[iPartToUse].iTeam]
				iCurrentLocateBlipped = iLoc
			ELSE
				IF IS_ENTITY_ALIVE(PlayerPedToUse)
					SET_BLIP_COORDS(biLocateRadiusBlip, GET_ENTITY_COORDS(PlayerPedToUse))
				ENDIF
				
				IF iTempActivePlayers != MC_serverBD.iNumberOfPlayingPlayers[MC_PlayerBD[iPartToUse].iTeam]
					REMOVE_BLIP(biLocateRadiusBlip)
				ENDIF
				
				IF NOT HAS_NET_TIMER_STARTED(tdRadiusBlipAlphaTimer)
					START_NET_TIMER(tdRadiusBlipAlphaTimer)
				ELSE
					IF HAS_NET_TIMER_EXPIRED(tdRadiusBlipAlphaTimer, ciAlphaChangeTime)
						INT i
						FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
							IF i != iPartToUse
								PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(i)
								IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
									IF MC_playerBD[iPartToUse].iteam = MC_PlayerBD[i].iteam
										IF VDIST2(GET_ENTITY_COORDS(PlayerPedToUse), GET_ENTITY_COORDS(GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(tempPart)),FALSE)) <= POW(CALCULATE_MINIMUM_DISTANCE_BETWEEN_TEAM_MATES(iLoc,MC_playerBD[iPartToUse].iteam), 2)
											iAlpha = CLAMP_INT((iAlpha - iAlphaChange), 75, 175)
											PRINTLN("[JS][LOCATEBLIP] - DRAW_LOCATE_RADIUS_BLIP - Part ", i," is in range, decreasing alpha to ", iAlpha)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
						IF DOES_BLIP_EXIST(biLocateRadiusBlip)
							SET_BLIP_ALPHA(biLocateRadiusBlip, iAlpha)
						ENDIF
						REINIT_NET_TIMER(tdRadiusBlipAlphaTimer)
					ENDIF
				ENDIF
			ENDIF
		ELSE	
			IF DOES_BLIP_EXIST(biLocateRadiusBlip)
				REMOVE_BLIP(biLocateRadiusBlip)
				PRINTLN("[JS][LOCATEBLIP] - DRAW_LOCATE_RADIUS_BLIP - Removing blip fTeamMembersNeededWithin <= 0 (", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fTeamMembersNeededWithin,")")
			ENDIF
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(biLocateRadiusBlip)
			REMOVE_BLIP(biLocateRadiusBlip)
			PRINTLN("[JS][LOCATEBLIP] - DRAW_LOCATE_RADIUS_BLIP - Invalid rule removing blip")
		ENDIF
	ENDIF
ENDPROC

// url:bugstar:3265534
FUNC BOOL IS_WRONG_BLIP_LOCATION_BEING_USED(BLIP_INDEX biBlip, INT iLoc)
	RETURN VDIST2(GET_BLIP_COORDS(biBlip), GET_LOCATION_VECTOR(iloc)) > 1.0
ENDFUNC

FUNC BOOL SHOULD_RED_FILTER_SHOW(INT iTeam, INT iRule, INT iLoc)
	IF ((NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[iRule], ciBS_RULE10_ONLY_USE_VFX_WHEN_CAPTURED))
	OR (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[iRule], ciBS_RULE10_ONLY_USE_VFX_WHEN_CAPTURED) AND MC_serverBD.iLocOwner[iLoc] = iTeam))
	AND NOT HAS_TEAM_FINISHED(iTeam)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PLAY_RED_FILTER_SOUND()
	STRING sSoundSet
	DEBUG_PRINTCALLSTACK()
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_HOSTILE_TAKEOVER_SOUNDS)
		sSoundSet = "DLC_Apartments_Drop_Zone_Sounds"
		PLAY_SOUND_FRONTEND(-1,"Enter_Capture_Zone",sSoundSet,FALSE)
		PRINTLN("[RCC MISSION] PLAY_RED_FILTER_SOUND - Playing sound from Soundset: ", sSoundSet)
	ELSE
		sSoundSet = "dlc_xm_hota_Sounds"
		PLAY_SOUND_FRONTEND(-1,"Zone_Enter",sSoundSet,FALSE)
		PRINTLN("[RCC MISSION] PLAY_RED_FILTER_SOUND - Playing sound from Soundset: ", sSoundSet)
	ENDIF
	SET_BIT(iLocalBoolCheck27, LBOOL27_RED_FILTER_SOUND_PLAYED)
ENDPROC

PROC LOCAL_PROCESS_LOCATION_OBJECTIVES(INT iloc)

	FLOAT fDisttoLoc
	INT iPlayerTeam = MC_playerBD[ iPartToUse ].iteam

	IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_OBJECTIVE_BLOCKER)
		IF MC_serverBD_4.iGotoLocationDataPriority[iloc][ iPlayerTeam ] <= MC_serverBD_4.iCurrentHighestPriority[ iPlayerTeam ]
		AND MC_serverBD_4.iGotoLocationDataPriority[iloc][ iPlayerTeam ] < FMMC_MAX_RULES
			fDisttoLoc = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed,GET_LOCATION_VECTOR(iloc))
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS, ciLoc_BS_HideBlip)
			AND IS_PED_IN_REQUIRED_LOCATION_VEHICLE(PlayerPedToUse, iPlayerTeam, iloc)
				IF fDisttoLoc < fNearestTargetDistTemp
					iNearestTargetTemp = iloc
					iNearestTargetTypeTemp = ci_TARGET_LOCATION
					fNearestTargetDistTemp = fDisttoLoc
					PRINTLN("[JS] - STAGGERED_POST_ENTITY_PROCESSING - ci_TARGET_LOCATION Setting iNearestTargetTemp to ", iloc, " dist = ", fDisttoLoc)
					
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(localPlayerPed)
				IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
					IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocDisableVehSpecWeapRadius > 0
						VECTOR vPlayerPos = GET_ENTITY_COORDS(localPlayerPed)
					
						IF VDIST2(vPlayerPos, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].vLoc[iPlayerTeam]) > POW(TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocDisableVehSpecWeapRadius), 2)
							PRINTLN("[LM][LOCAL_PROCESS_LOCATION_OBJECTIVES] - Disabling Special Vehicle Attacks/Abilities We are within Loc Radius: ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocDisableVehSpecWeapRadius)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SPECIAL, TRUE)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_JUMP, TRUE)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK, TRUE)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2, TRUE)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN, TRUE)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_LOOK_BEHIND, TRUE)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CAR_JUMP, TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF			
			
			IF MC_serverBD_4.iGotoLocationDataRule[iloc][ iPlayerTeam ]	 = FMMC_OBJECTIVE_LOGIC_GO_TO
			OR MC_serverBD_4.iGotoLocationDataRule[iloc][ iPlayerTeam ]	 = FMMC_OBJECTIVE_LOGIC_PHOTO
				IF NOT DOES_BLIP_EXIST(LocBlip[iloc])
					IF NOT (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS2, ciLoc_BS2_RemoveBlipWhenInLocate)
					AND IS_PED_IN_LOCATION(LocalPlayerPed, iloc,  iPlayerTeam))
						
						IF IS_PED_IN_REQUIRED_LOCATION_VEHICLE(PlayerPedToUse, iPlayerTeam, iloc)
						AND (MC_playerBD[ iPartToUse ].iCurrentLoc != iloc OR MC_serverBD_4.iGotoLocationDataRule[iloc][ iPlayerTeam ]	 = FMMC_OBJECTIVE_LOGIC_PHOTO)
							PRINTLN("[AW_MARKER - LOCAL_PROCESS_LOCATION_OBJECTIVES- iPlayerTeam: ",iPlayerTeam)
							PRINTLN("[AW_MARKER - LOCAL_PROCESS_LOCATION_OBJECTIVES- MC_playerBD[ iPartToUse ].iteam: ",MC_playerBD[ iPartToUse ].iteam)
							CREATE_BLIP_FOR_MP_COORD( iLoc, iPlayerTeam, fDistToLoc, BLIP_COLOUR_YELLOW, TRUE )
						ENDIF
					ENDIF
				ELSE
					IF fDisttoLoc <= g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iGPSCutOff[0]
					OR g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iGPSCutOff[0] = -1
						IF DOES_BLIP_HAVE_GPS_ROUTE(LocBlip[iloc])
							SET_BLIP_ROUTE(LocBlip[iloc],FALSE)
						ENDIF
					ELSE
						IF MC_serverBD.iNumLocHighestPriority[ iPlayerTeam ] = 1
							IF NOT MPGlobalsAmbience.R2Pdata.bOnRaceToPoint
							AND NOT USING_HEIST_SPECTATE()
								IF NOT DOES_BLIP_HAVE_GPS_ROUTE(LocBlip[iloc])
									SET_BLIP_ROUTE(LocBlip[iloc],TRUE)
									IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS,ciLoc_BS_AvoidHighwayGPS)
										SET_GPS_FLAGS(GPS_FLAG_AVOID_HIGHWAY)
									ELSE
										SET_GPS_FLAGS(GPS_FLAG_NONE)
									ENDIF
								ENDIF
							ENDIF
							
							IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
								SET_BLIP_ROUTE(LocBlip[iloc], FALSE)
								PRINTLN("[TMS] Setting blip route to FALSE because I am a spectator")
							ENDIF
						ENDIF
					ENDIF
					IF NOT IS_PED_IN_REQUIRED_LOCATION_VEHICLE(PlayerPedToUse, iPlayerTeam, iloc)
					AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitset, PBBOOL_HEIST_SPECTATOR)
					OR (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS2, ciLoc_BS2_RemoveBlipWhenInLocate) AND IS_PED_IN_LOCATION(LocalPlayerPed, iloc,  iPlayerTeam))
					OR IS_WRONG_BLIP_LOCATION_BEING_USED(LocBlip[iloc], iloc) // url:bugstar:3265534
					OR SHOULD_REMOVE_LOCATION_BLIP(iLoc, fDistToLoc)	
						PRINTLN("[LOCAL_PROCESS_LOCATION_OBJECTIVES] (LocBlip) Removing Blip 2")
						REMOVE_BLIP(LocBlip[iloc])
					ENDIF
				ENDIF
			ELIF MC_serverBD_4.iGotoLocationDataRule[iloc][ iPlayerTeam ] = FMMC_OBJECTIVE_LOGIC_CAPTURE
			
				IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS,ciLoc_BS_HideBlipWhenInLocation)
				OR (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS,ciLoc_BS_HideBlipWhenInLocation) AND MC_playerBD[iPartToUse].iCurrentLoc != iloc)
					CREATE_BLIP_FOR_MP_COORD( iLoc, iPlayerTeam, fDistToLoc, BLIP_COLOUR_YELLOW, TRUE )
				ENDIF
				IF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_CONTROL_AREA
				AND IS_BIT_SET(iLocalBoolCheck14, LBOOL14_VFX_CAPTURE_OWNER)
					IF DOES_BLIP_EXIST(LocBlip[iloc])
						IF g_iTheWinningTeam = -1
							//Show Yellow = Tied
							SET_BLIP_COLOUR(LocBlip[iloc], BLIP_COLOUR_YELLOW)
						ELSE
							IF g_iTheWinningTeam = -1
								//Show Yellow = Tied
								SET_BLIP_COLOUR(LocBlip[iloc], BLIP_COLOUR_YELLOW)
							ELIF g_iTheWinningTeam = 0
								//Team 0 - Orange
								SET_BLIP_COLOUR(LocBlip[iloc], BLIP_COLOUR_ORANGE)
							ELIF g_iTheWinningTeam = 1
								//Team 1 - Green
								SET_BLIP_COLOUR(LocBlip[iloc], BLIP_COLOUR_GREEN)
							ELIF g_iTheWinningTeam = 2
								//Team 2 - Pink
								SET_BLIP_COLOUR(LocBlip[iloc], BLIP_COLOUR_PINK)
							ELIF g_iTheWinningTeam = 3
								//Team 3 - Purple
								SET_BLIP_COLOUR(LocBlip[iloc], BLIP_COLOUR_PURPLE)						
							ENDIF
	//						
							IF ENUM_TO_INT(WinningTeamColour) = ENUM_TO_INT(WinningTeamColour)
							
							ENDIF
							
							WinningTeamColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(g_iTheWinningTeam, PlayerToUse)
							//Team 0 - Orange
							//Team 1 - Green
							//Team 2 - Pink
							//Team 3 - Purple
							PRINTLN( "[RCC MISSION][AW SMOKE] TEAM ",g_iTheWinningTeam, "is winning")
							//SET_BLIP_COLOUR(LocBlip[iloc], ENUM_TO_INT(WinningTeamColour))
						ENDIF
					ENDIF
				ENDIF
				
				IF MC_playerBD[iPartToUse].iCurrentLoc != iloc
					IF DOES_BLIP_EXIST(LocBlip[iloc])
						IF fDisttoLoc <= g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iGPSCutOff[0]
							IF DOES_BLIP_HAVE_GPS_ROUTE(LocBlip[iloc])
								SET_BLIP_ROUTE(LocBlip[iloc],FALSE)
							ENDIF
						ELSE
							IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS, ciLoc_BS_HideBlip)
								IF fDisttoLoc > g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iGPSCutOff[0]
								AND g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iGPSCutOff[0] != -1
									IF NOT MPGlobalsAmbience.R2Pdata.bOnRaceToPoint
									AND NOT USING_HEIST_SPECTATE()
										IF NOT DOES_BLIP_HAVE_GPS_ROUTE(LocBlip[iloc])
											SET_BLIP_ROUTE(LocBlip[iloc],TRUE)
											IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS,ciLoc_BS_AvoidHighwayGPS)
												SET_GPS_FLAGS(GPS_FLAG_AVOID_HIGHWAY)
											ELSE
												SET_GPS_FLAGS(GPS_FLAG_NONE)
											ENDIF
										ENDIF
									ELSE
										IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
											SET_BLIP_ROUTE(LocBlip[iloc], FALSE)
											PRINTLN("[TMS] Clearing blip route now because I'm a spectator")
										ENDIF
										
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciEVERY_FRAME_GOTO_LOC_PROCESSING)
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iPlayerTeam].iRuleBitsetFive[MC_serverBD_4.iCurrentHighestPriority[iPlayerTeam]], ciBS_RULE5_RED_FILTER_IN_CAP_ZONE)
						AND NOT IS_BIT_SET( MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER )
							IF SHOULD_RED_FILTER_SHOW(iPlayerTeam, MC_serverBD_4.iCurrentHighestPriority[iPlayerTeam], iloc)
								IF NOT ANIMPOSTFX_IS_RUNNING("CrossLine")
									PRINTLN("[JS] Turning on capture filter early")
									IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_RED_FILTER_SOUND_PLAYED)
										PLAY_RED_FILTER_SOUND()
									ENDIF
									ANIMPOSTFX_PLAY("CrossLine", 0, TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT DOES_BLIP_EXIST(LocBlip1[iloc])
				OR ioldLocOwner[iloc]!= MC_serverBD.iLocOwner[iloc]
					CREATE_BLIP_FOR_MP_AREA( iLoc, iPlayerTeam )
					
					ioldLocOwner[iloc] = MC_serverBD.iLocOwner[iloc]
				ELSE	
					IF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_CONTROL_AREA
					AND IS_BIT_SET(iLocalBoolCheck14, LBOOL14_VFX_CAPTURE_OWNER)
						IF DOES_BLIP_EXIST(LocBlip1[iloc])
							IF g_iTheWinningTeam = -1
								//Show Yellow = Tied
								SET_BLIP_COLOUR(LocBlip1[iloc], BLIP_COLOUR_YELLOW)
							ELSE
							
								INT iTeamOverrideBlip
								iTeamOverrideBlip = MC_serverBD.iLocOwner[iloc]
							
								IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_ZONE(g_FMMC_STRUCT.iRootContentIDHash)
									INT i = 0
									FOR i = 0 TO FMMC_MAX_TEAMS-1
										IF MC_serverBD_1.inumberOfTeamInArea[iLoc][i] > 0
										AND ((IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_ZONE(g_FMMC_STRUCT.iRootContentIDHash) AND (iTeamOverrideBlip = -1 OR i = MC_PlayerBD[iParttoUse].iteam)) OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_ZONE(g_FMMC_STRUCT.iRootContentIDHash))
											iTeamOverrideBlip = i
										ENDIF
									ENDFOR
								ENDIF
								
								IF iTeamOverrideBlip > -1
									IF iTeamOverrideBlip = -1
										//Show Yellow = Tied
										SET_BLIP_COLOUR(LocBlip1[iloc], BLIP_COLOUR_YELLOW)
									ELIF iTeamOverrideBlip = 0
										//Team 0 - Orange
										SET_BLIP_COLOUR(LocBlip1[iloc], BLIP_COLOUR_ORANGE)
									ELIF iTeamOverrideBlip = 1
										//Team 1 - Green
										SET_BLIP_COLOUR(LocBlip1[iloc], BLIP_COLOUR_GREEN)
									ELIF iTeamOverrideBlip = 2
										//Team 2 - Pink
										SET_BLIP_COLOUR(LocBlip1[iloc], BLIP_COLOUR_PINK)
									ELIF iTeamOverrideBlip = 3
										//Team 3 - Purple
										SET_BLIP_COLOUR(LocBlip1[iloc], BLIP_COLOUR_PURPLE)						
									ENDIF
								ELSE
									
									IF g_iTheWinningTeam = -1
										//Show Yellow = Tied
										SET_BLIP_COLOUR(LocBlip1[iloc], BLIP_COLOUR_YELLOW)
									ELIF g_iTheWinningTeam = 0
										//Team 0 - Orange
										SET_BLIP_COLOUR(LocBlip1[iloc], BLIP_COLOUR_ORANGE)
									ELIF g_iTheWinningTeam = 1
										//Team 1 - Green
										SET_BLIP_COLOUR(LocBlip1[iloc], BLIP_COLOUR_GREEN)
									ELIF g_iTheWinningTeam = 2
										//Team 2 - Pink
										SET_BLIP_COLOUR(LocBlip1[iloc], BLIP_COLOUR_PINK)
									ELIF g_iTheWinningTeam = 3
										//Team 3 - Purple
										SET_BLIP_COLOUR(LocBlip1[iloc], BLIP_COLOUR_PURPLE)						
									ENDIF
								ENDIF
								
								WinningTeamColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(g_iTheWinningTeam, PlayerToUse)
								//Team 0 - Orange
								//Team 1 - Green
								//Team 2 - Pink
								//Team 3 - Purple
								PRINTLN( "[RCC MISSION][AW SMOKE] TEAM ",g_iTheWinningTeam, "is winning - here1")
								//SET_BLIP_COLOUR(LocBlip1[iloc], ENUM_TO_INT(WinningTeamColour))
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
				
			ELIF MC_serverBD_4.iGotoLocationDataRule[iloc][ iPlayerTeam ] = FMMC_OBJECTIVE_LOGIC_LEAVE_LOCATION
				IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS,ciLoc_BS_HideBlipWhenInLocation)
				OR (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS,ciLoc_BS_HideBlipWhenInLocation) AND MC_playerBD[iPartToUse].iCurrentLoc != iloc)
					CREATE_BLIP_FOR_MP_COORD( iLoc, iPlayerTeam, fDistToLoc, BLIP_COLOUR_YELLOW, FALSE )
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)		
		IF iPartToUse > -1		
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartToUse))
				IF NOT GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()
				OR IS_PED_INJURED(localPlayerPed)
				OR IS_PED_INJURED(GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartToUse))))
					EXIT
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciCHANGE_BLIP_COLOUR_TO_CONTROLLING_TEAM)
		IF DOES_BLIP_EXIST(LocBlip[iLoc])
			IF MC_serverBD.iLocOwner[iloc] > -1
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_CONTESTED_COLOUR_FOR_LOCATES)
				AND IS_ZONE_BEING_CONTESTED(iLoc)
					SET_BLIP_COLOUR(LocBlip[iLoc], BLIP_COLOUR_RED)
					SET_BLIP_COLOUR(LocBlip1[iLoc], BLIP_COLOUR_RED)
				ELSE
					SET_BLIP_COLOUR(LocBlip[iLoc], GET_INT_FROM_HUD_COLOUR(GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_serverBD.iLocOwner[iloc],LocalPlayer)))
					SET_BLIP_COLOUR(LocBlip1[iLoc], GET_INT_FROM_HUD_COLOUR(GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_serverBD.iLocOwner[iloc],LocalPlayer)))
				ENDIF
			ELSE
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_CONTESTED_COLOUR_FOR_LOCATES)
				AND IS_ZONE_BEING_CONTESTED(iLoc, FALSE, TRUE)
					SET_BLIP_COLOUR(LocBlip[iLoc], BLIP_COLOUR_RED)
					SET_BLIP_COLOUR(LocBlip1[iLoc], BLIP_COLOUR_RED)
				ELSE
					SET_BLIP_COLOUR(LocBlip[iLoc], BLIP_COLOUR_YELLOW)
					SET_BLIP_COLOUR(LocBlip1[iLoc], BLIP_COLOUR_YELLOW)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF (MC_serverBD_4.iGotoLocationDataPriority[iloc][MC_playerBD[iPartToUse].iteam] > MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUNNING_BACK_REMIX(g_FMMC_STRUCT.iAdversaryModeType))
	OR (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS,ciLoc_BS_HideBlipWhenInLocation) AND MC_playerBD[iPartToUse].iCurrentLoc = iloc)
		IF DOES_BLIP_EXIST(LocBlip[iloc])
			PRINTLN("[LOCAL_PROCESS_LOCATION_OBJECTIVES] (LocBlip) Removing Blip 1")
			REMOVE_BLIP(LocBlip[iloc])
		ENDIF
		//Looks like this mode is setup a weird way and is causing the blips to be cleared up and created every frame
		IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TOUR_DE_FORCE(g_FMMC_STRUCT.iAdversaryModeType)
		AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_KEEP_THE_PACE(g_FMMC_STRUCT.iAdversaryModeType)
			IF DOES_BLIP_EXIST(LocBlip1[iloc])			
				PRINTLN("[LOCAL_PROCESS_LOCATION_OBJECTIVES] (LocBlip) Removing Blip 3")
				REMOVE_BLIP(LocBlip1[iloc])
			ENDIF
		ELSE
//			IF DOES_BLIP_EXIST(LocBlip1[iloc])
//				REMOVE_BLIP(LocBlip1[iloc])
//			ENDIF
		ENDIF
	ENDIF
	
ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: EVERY FRAME LOCATION CHECKS / LOCATION HUD !
//
//************************************************************************************************************************************************************



FUNC BOOL IS_LOCATION_AN_OBJECTIVE_FOR_ANY_TEAM(INT iloc)

	INT iteam
	
	FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
		IF MC_serverBD_4.iGotoLocationDataPriority[iloc][iteam] < FMMC_MAX_RULES
			RETURN TRUE
		ENDIF
	ENDFOR

	RETURN FALSE
	
ENDFUNC

FUNC INT THIS_LOCATION_BELONGS_TO_TEAM(INT iloc)

	INT iteam
	INT iThisTeam
	
	FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
		
		IF MC_serverBD_4.iGotoLocationDataPriority[iloc][iteam] < FMMC_MAX_RULES
		AND MC_serverBD_4.iGotoLocationDataPriority[iloc][iteam] != -1
			PRINTLN("[RCC MISSION][LOCATES][THIS_LOCATION_BELONGS_TO_TEAM]iloc : ", iloc, " iTeam: ", iTeam)
			iThisTeam = iteam
			BREAKLOOP
		ENDIF

	ENDFOR
	
	RETURN iThisTeam

ENDFUNC

FUNC BOOL IS_THIS_A_LINE_STYLE_LOCATE_MAKER(INT iLoc)
	BOOL bLineStyle = FALSE
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS2, ciLoc_BS2_UseLineMarker)
		bLineStyle = TRUE
	ENDIF
	
	RETURN bLineStyle
ENDFUNC

FUNC BOOL IS_THIS_A_RACE_STYLE_LOCATE_MARKER(INT iLoc)
	
	BOOL bRaceStyle = FALSE
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS2, ciLoc_BS2_RaceLocate_Chckpt1)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS2, ciLoc_BS2_RaceLocate_Chckpt2)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS2, ciLoc_BS2_RaceLocate_Chckpt3)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS2, ciLoc_BS2_RaceLocate_Lap)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS2, ciLoc_BS2_RaceLocate_Finish)
		bRaceStyle = TRUE
	ENDIF
	
	RETURN bRaceStyle
	
ENDFUNC

PROC CREATE_TAG_TEAM_BLIP(INT iLoc)
	INT iBlip
	FOR iBlip = 0 TO ci_MaxTagTeamBlips-1
		IF iGarageLocLink[iBlip] = iLoc
			BREAKLOOP
		ENDIF
		
		IF NOT DOES_BLIP_EXIST(biTagTeamGarageBlip[iBlip])		
			#IF IS_DEBUG_BUILD
			VECTOR vLoc = GET_LOCATION_VECTOR(iloc)
				PRINTLN("[LM][Blips][TAG_TEAM] - Creating Blip - iBlip: ", iBlip, " iLoc: ", iLoc, " Vector: ", vLoc)
			#ENDIF
			
			biTagTeamGarageBlip[iBlip] = ADD_BLIP_FOR_COORD(GET_LOCATION_VECTOR(iloc))
			
			SET_BLIP_ALPHA(biTagTeamGarageBlip[iBlip], 255)
			SET_BLIP_AS_SHORT_RANGE(biTagTeamGarageBlip[iBlip], FALSE)
			//SET_BLIP_ROTATION(biTagTeamGarageBlip[iBlip], ROUND(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fDirection))
			
			SET_BLIP_SPRITE(biTagTeamGarageBlip[iBlip], RADAR_TRACE_GARAGE)				
			SET_BLIP_PRIORITY(biTagTeamGarageBlip[iBlip], BLIP_PRIORITY_HIGHEST_SPECIAL_LOW)				
			SET_BLIP_SCALE(biTagTeamGarageBlip[iBlip], 0.75)
			
			
			SET_BLIP_DISPLAY(biTagTeamGarageBlip[iBlip], DISPLAY_RADAR_ONLY)
			
			IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iFakeOwnedByTeam = 0
				SET_BLIP_COLOUR(biTagTeamGarageBlip[iBlip], BLIP_COLOUR_ORANGE)
			ELIF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iFakeOwnedByTeam = 1
				SET_BLIP_COLOUR(biTagTeamGarageBlip[iBlip], BLIP_COLOUR_PURPLE)
			ELIF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iFakeOwnedByTeam = 2
				SET_BLIP_COLOUR(biTagTeamGarageBlip[iBlip], BLIP_COLOUR_PINK)
			ELIF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iFakeOwnedByTeam = 3
				SET_BLIP_COLOUR(biTagTeamGarageBlip[iBlip], BLIP_COLOUR_GREEN)
			ENDIF
			iGarageLocLink[iBlip] = iLoc
			BREAKLOOP
		ENDIF
	ENDFOR
ENDPROC

PROC CLEANUP_TAG_TEAM_BLIPS()
	INT iBlip
	FOR iBlip = 0 TO ci_MaxTagTeamBlips-1
		IF DOES_BLIP_EXIST(biTagTeamGarageBlip[iBlip])
			PRINTLN("[LM][Blips][TAG_TEAM] - Deleting the Garage Blips - they are not valid any longer.")
			REMOVE_BLIP(biTagTeamGarageBlip[iBlip])
			iGarageLocLink[iBlip] = -1
		ENDIF
	ENDFOR
ENDPROC

FUNC BOOL SHOULD_EXIT_FROM_DRAW_LOCATE(INT iLoc)
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Enable_Turn_Rotor_Tag_Team)		
		IF NOT HAS_NET_TIMER_STARTED(MC_serverBD_3.tdMultiObjectiveLimitTimer[MC_PlayerBD[iPartToUse].iteam])
		OR GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[MC_PlayerBD[iPartToUse].iteam]) < 5000
			PRINTLN("[LM][DRAW_ONE_LOCATION_MARKER][TAG_TEAM] - Mode hasn't fully initialized with server data yet. Exiting.")
			RETURN TRUE
		ENDIF
	
		IF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_DisableLocateCoronasWhileCantTagOut)
		AND ((g_FMMC_STRUCT.iTagTeamCooldownTime*1000)-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdTagOutCooldown[MC_PlayerBD[iPartToUse].iteam])) > 0
		AND HAS_NET_TIMER_STARTED(MC_serverBD_3.tdTagOutCooldown[MC_PlayerBD[iPartToUse].iteam]))
		OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_DisableLocateCoronasWhileCantTagOut) AND g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iFakeOwnedByTeam != MC_playerBD[iLocalPart].iteam)
		OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_DisableLocateCoronasWhileCantTagOut) AND IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH))
		OR IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
		OR IS_LOCAL_PLAYER_LAST_ALIVE_ON_TEAM(MC_playerBD[iLocalPart].iteam)
			IF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_DisableLocateCoronasWhileCantTagOut)
			AND ((g_FMMC_STRUCT.iTagTeamCooldownTime*1000)-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdTagOutCooldown[MC_PlayerBD[iPartToUse].iteam])) > 0
			AND HAS_NET_TIMER_STARTED(MC_serverBD_3.tdTagOutCooldown[MC_PlayerBD[iPartToUse].iteam]))
			OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_DisableLocateCoronasWhileCantTagOut) AND IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH))
			OR IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
			OR IS_LOCAL_PLAYER_LAST_ALIVE_ON_TEAM(MC_playerBD[iLocalPart].iteam)
				CLEANUP_TAG_TEAM_BLIPS()
			ENDIF
			
			PRINTLN("[LM][DRAW_ONE_LOCATION_MARKER][TAG_TEAM] - Don't draw markers yet. Haven't had 30 sec pass.")
			RETURN TRUE
		ELSE
			IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iFakeOwnedByTeam = MC_playerBD[iLocalPart].iteam
			AND HAS_NET_TIMER_STARTED(MC_serverBD_3.tdTagOutCooldown[MC_PlayerBD[iPartToUse].iteam])
				CREATE_TAG_TEAM_BLIP(iLoc)
			ENDIF
		ENDIF
	ENDIF
	
	// IF other reasons?
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Draws a single DRAW_LOCATE_MARKER or DRAW_AIR_MARKER
///    
/// PARAMS:
///    iLoc - The index of the location we're going to draw
///    iOverrideAlpha - A value from 0-255 for overriding the alpha channel
PROC DRAW_ONE_LOCATION_MARKER( INT iLoc, INT iOverrideAlpha = -1, BOOL bEnemyMarker = FALSE, INT iTeamWhoOwnThis = -1, BOOL bShowRugbyOrangeTeam = FALSE )
	// Rob B - allow Cross the Line lines to display while out of bounds
	BOOL bShowLineMarkerOutOfBounds = FALSE
	IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS2,ciLoc_BS2_UseLineMarker )
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Enable_Turn_Rotor_Tag_Team)
		bShowLineMarkerOutOfBounds = TRUE
	ENDIF
	
	IF SHOULD_EXIT_FROM_DRAW_LOCATE(iLoc)
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bGotoLocPrints
		PRINTLN("[RCC MISSION] DRAW_ONE_LOCATION_MARKER - iLoc: ", iLoc)
	ENDIF
	#ENDIF
	
	IF (NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_OBJECTIVE_BLOCKER)
	OR bShowLineMarkerOutOfBounds)
	AND NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_FINISHED)
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS, ciLoc_BS_HideMarker )
			IF IS_THIS_A_RACE_STYLE_LOCATE_MARKER(iloc)
				DRAW_RACE_STYLE_MARKER(GET_LOCATION_VECTOR(iloc), iloc, iOverrideAlpha)
			ELSE // Not a race locate:
	
				IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iArial[0] = 1
					DRAW_AIR_MARKER(GET_LOCATION_VECTOR(iloc), FALSE, iloc)
				ELSE
					
					VECTOR vLocPos = (GET_LOCATION_VECTOR(iloc) - <<0.0, 0.0, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fLocateDepth>>)
					
					IF IS_THIS_A_LINE_STYLE_LOCATE_MAKER(iLoc)
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciSET_LINES_AS_RUGBY_LINES)
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_DisplayMarkerAsVisualOnly )
								
								PRINTLN("[RCC MISSION] DRAW_ONE_LOCATION_MARKER - iOverrideAlpha",iOverrideAlpha)
								DRAW_LOCATE_MARKER( vLocPos,							        				// Location
								g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fRadius[0],								// Radius
								FALSE,																	        // is a dropoff?
								iloc,																	        // Index
								iOverrideAlpha, 														        // Alpha Override	
								bEnemyMarker, 																	// bEnemyMarker
								iTeamWhoOwnThis , bShowRugbyOrangeTeam)											    // iTeamWhoOwnThis    	
								PRINTLN("AW LINE MARKER: [LOCATES] DRAW_ONE_LOCATION_MARKER | MARKER IS A LINE MARKER ")
							ENDIF
						ELSE
							PRINTLN("[RCC MISSION] DRAW_ONE_LOCATION_MARKER - iOverrideAlpha",iOverrideAlpha)
							DRAW_LOCATE_MARKER( vLocPos,							        				// Location
							g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fRadius[0],								// Radius
							FALSE,																	        // is a dropoff?
							iloc,																	        // Index
							iOverrideAlpha, 														        // Alpha Override	
							bEnemyMarker, 																	// bEnemyMarker
							iTeamWhoOwnThis )															    // iTeamWhoOwnThis    	
							PRINTLN("AW LINE MARKER: [LOCATES] DRAW_ONE_LOCATION_MARKER | MARKER IS A LINE MARKER ")
						ENDIF
					ELSE
						DRAW_LOCATE_MARKER( vLocPos,											        // Location
											g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fRadius[0], 	        // Radius
											FALSE,												        // is a dropoff?
											iloc,												        // Index
											iOverrideAlpha, 									        // Alpha Override
											DEFAULT,													// bEnemyMarker
											iTeamWhoOwnThis)											// iTeamWhoOwnThis
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_OBJECTIVE_BLOCKER)
			PRINTLN("[RCC MISSION] DRAW_ONE_LOCATION_MARKER - Not drawing location markers due to: PBBOOL_OBJECTIVE_BLOCKER")
		ENDIF
		IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_FINISHED)
			PRINTLN("[RCC MISSION] DRAW_ONE_LOCATION_MARKER - Not drawing location markers due to: PBBOOL_FINISHED")
		ENDIF
	ENDIF
	
ENDPROC

PROC CREATE_MARKER_FOR_ENEMY_TEAM(INT iLoc, INT iTeamWhoOwnThis = -1)

INT iPlayerTeam = MC_playerBD[ iPartToUse ].iteam
INT i

PRINTLN("[AW_MARKER - CREATE_MARKER_FOR_ENEMY_TEAM -  ")
	
	IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].iLocBS2, ciLoc_BS2_DisplayOtherTeamsMarker )
		FOR i = 0 TO MC_ServerBD.iNumberOfTeams -1
			IF i != iPlayerTeam
				IF MC_serverBD_4.iGotoLocationDataPriority[iloc][ i ] = MC_serverBD_4.iCurrentHighestPriority[i]
					PRINTLN("[AW_MARKER [LOCATES]- CREATE_MARKER_FOR_ENEMY_TEAM -  MC_serverBD_4.iGotoLocationDataPriority[iloc][ i ] = MC_serverBD_4.iCurrentHighestPriority[i]  ")
					PRINTLN("[LOCATES]g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].iLocBS, ciLoc_BS2_UseLineMarker is set ")
					PRINTLN("[LOCATES]g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].iLocBS2, ciLoc_BS2_DisplayOtherTeamsMarker is set ")
					//Line Stuff
					DRAW_ONE_LOCATION_MARKER( iLoc ,-1, TRUE, iTeamWhoOwnThis)
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
ENDPROC


FUNC BOOL SHOULD_COUNT_LEAVE_LOCATION(INT iloc)

	IF MC_serverBD_4.iGotoLocationDataRule[iloc][MC_playerBD[iPartToUse].iteam] != FMMC_OBJECTIVE_LOGIC_LEAVE_LOCATION
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iWholeTeamAtLocation[MC_playerBD[iPartToUse].iteam] != ciGOTO_LOCATION_INDIVIDUAL
	AND g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iWholeTeamAtLocation[MC_playerBD[iPartToUse].iteam] != ciGOTO_LOCATION_WHOLE_TEAM
		//i.e. it could be a locate that requires my existence even without me being on its rule
		RETURN TRUE
	ELSE
		IF MC_serverBD_4.iGotoLocationDataPriority[iloc][MC_playerBD[iPartToUse].iteam] < iLowestLeaveLocationPriority
			iLowestLeaveLocationPriority = MC_serverBD_4.iGotoLocationDataPriority[iloc][MC_playerBD[iPartToUse].iteam]
			RETURN TRUE
		ENDIF

	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL SHOULD_MARK_THIS_GOTO_AS_DONE_FOR_APARTMENT_DROPOFF()
	IF g_bShouldThisPlayerGetPulledIntoApartment
	AND g_bPropertyDropOff
	AND g_HeistApartmentDropoffPanStarted
		#IF IS_DEBUG_BUILD
			IF bGotoLocPrints
				PRINTLN("[RCC MISSION] - SHOULD_MARK_THIS_GOTO_AS_DONE_FOR_APARTMENT_DROPOF - TRUE")
			ENDIF
		#ENDIF
		
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC CHECK_FADED_OUT_CHECKPOINT( INT iLoc, INT iTeam, INT iRule, BOOL bLaps = FALSE)
	
	IF IS_BIT_SET(iCheckpointBS, iloc)
		
		BOOL bDelete
		
		IF sFadeOutLocations[ iLoc ].eState = eLocateFadeOutState_AFTER_FADE_OUT
			bDelete = TRUE
			PRINTLN("[RCC MISSION] CHECK_FADED_OUT_CHECKPOINT - Delete faded out checkpoint for iloc ",iloc)
		ELIF sFadeOutLocations[ iLoc ].eState != eLocateFadeOutState_FADING_OUT
			IF MC_serverBD_4.iGotoLocationDataPriority[iloc][ iTeam ] > iRule
				//If it's not an objective right now, clear it!
				bDelete = TRUE

				IF bLaps 
					sFadeOutLocations[ iLoc ].eState = eLocateFadeOutState_BEFORE_FADE_IN
					sFadeOutLocations[ iLoc ].iAlphaValue = -1
				ELSE
					sFadeOutLocations[ iLoc ].eState = eLocateFadeOutState_AFTER_FADE_OUT
					sFadeOutLocations[ iLoc ].iAlphaValue = 0
				ENDIF
				PRINTLN("[RCC MISSION] CHECK_FADED_OUT_CHECKPOINT - Getting rid of locate ",iloc," as it isn't an objective any more!")
			ENDIF
		ENDIF
		
		IF bDelete
			DELETE_CHECKPOINT(ciLocCheckpoint[iloc])
			CLEAR_BIT(iCheckpointBS, iloc)
		ENDIF
	ENDIF
	
ENDPROC

STRUCT ANGLED_AREA_CORNER_STRUCT
	VECTOR vCorner[4]
ENDSTRUCT

FUNC ANGLED_AREA_CORNER_STRUCT GET_CORNERS_OF_ANGLED_AREA(VECTOR vLoc1, VECTOR vLoc2, FLOAT fWidth)
	ANGLED_AREA_CORNER_STRUCT vAngledAreaCorners
	
	vLoc1.Z = 0.0
	vLoc2.Z = 0.0
	
	VECTOR vNorm = NORMALISE_VECTOR(vLoc2 - vLoc1)
	
	FLOAT fTemp
	
	vAngledAreaCorners.vCorner[0] = vNorm
	fTemp = vAngledAreaCorners.vCorner[0].X
	vAngledAreaCorners.vCorner[0].X = vAngledAreaCorners.vCorner[0].Y
	vAngledAreaCorners.vCorner[0].Y = 0 - fTemp
	vAngledAreaCorners.vCorner[0] = vLoc1 + (vAngledAreaCorners.vCorner[0] * (fWidth / 2))
	
	vAngledAreaCorners.vCorner[1] = vNorm
	fTemp = vAngledAreaCorners.vCorner[1].X
	vAngledAreaCorners.vCorner[1].X = 0 - vAngledAreaCorners.vCorner[1].Y
	vAngledAreaCorners.vCorner[1].Y = fTemp
	vAngledAreaCorners.vCorner[1] = vLoc1 + (vAngledAreaCorners.vCorner[1] * (fWidth / 2))
	
	vAngledAreaCorners.vCorner[2] = vNorm
	fTemp = vAngledAreaCorners.vCorner[2].X
	vAngledAreaCorners.vCorner[2].X = vAngledAreaCorners.vCorner[2].Y
	vAngledAreaCorners.vCorner[2].Y = 0 - fTemp
	vAngledAreaCorners.vCorner[2] = vLoc2 + (vAngledAreaCorners.vCorner[2] * (fWidth / 2))
	
	vAngledAreaCorners.vCorner[3] = vNorm
	fTemp = vAngledAreaCorners.vCorner[3].X
	vAngledAreaCorners.vCorner[3].X = 0 - vAngledAreaCorners.vCorner[3].Y
	vAngledAreaCorners.vCorner[3].Y = fTemp
	vAngledAreaCorners.vCorner[3] = vLoc2 + (vAngledAreaCorners.vCorner[3] * (fWidth / 2))
	
	RETURN vAngledAreaCorners
ENDFUNC

FUNC FLOAT MINIMUM_DISTANCE_POINT_TO_LINE(VECTOR vLine1, VECTOR vLine2, VECTOR vPoint)
	// Return minimum distance between line segment vw and point vPoint
	FLOAT fLengthSquared = VDIST2(vLine1, vLine2) // Avoid a sqrt
	
	IF (fLengthSquared = 0.0) 
		RETURN GET_DISTANCE_BETWEEN_COORDS(vPoint, vLine1)   // vLine1 == vLine2 case
	ENDIF
	
	// Consider the line extending the segment, parameterized as vLine1 + t (vLine2 - vLine1).
	// We find projection of point vPoint onto the line. 
	// It falls where t = [(vPoint-vLine1) . (vLine2-vLine1)] / |vLine2-vLine1|^2
	FLOAT fDotProduct = DOT_PRODUCT(vPoint - vLine1, vLine2 - vLine1) / fLengthSquared
	
	IF (fDotProduct < 0.0)
		RETURN GET_DISTANCE_BETWEEN_COORDS(vPoint, vLine1)	// Beyond the 'vLine1' end of the segment
	ELIF (fDotProduct > 1.0)
		RETURN GET_DISTANCE_BETWEEN_COORDS(vPoint, vLine2)	// Beyond the 'vLine2' end of the segment
	ENDIF
	
	VECTOR vProjection = vLine1 + fDotProduct * (vLine2 - vLine1)  // Projection falls on the segment
	
	RETURN GET_DISTANCE_BETWEEN_COORDS(vPoint, vProjection)
ENDFUNC

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: SHARED PROCS + STRUCT FOR 'OUT OF BOUNDS TIMER LIST' AND 'CROSS THE LINE HUD' !
//
//************************************************************************************************************************************************************

STRUCT PLAYER_STATE_LIST_DATA
	UI_SLOT uiSlot[MAX_NUM_MC_PLAYERS]
	INT iCurrentPlayerCheck[FMMC_MAX_TEAMS]
	PLAYER_INDEX piPlayerStateList[MAX_NUM_MC_PLAYERS]
	INT iPlayerStateList[MAX_NUM_MC_PLAYERS]
	HUD_COLOURS hcTeam[FMMC_MAX_TEAMS]
	BOOL bSortLocalPlayerTeamTop = TRUE
	INT iNumberOfPlayersToDisplayPerTeam = 4
	INT iNumberOfTeamsToCheck = FMMC_MAX_TEAMS
	INT iNumberOfPlayersToCheck = MAX_NUM_MC_PLAYERS
ENDSTRUCT

PROC PLAYER_STATE_LIST_INIT(PLAYER_STATE_LIST_DATA &playerStateListData, BOOL bSortLocalPlayerTeamTop = TRUE, INT iNumberOfPlayersToDisplayPerTeam = 4, INT iNumberOfTeamsToCheck = FMMC_MAX_TEAMS, INT iNumberOfPlayersToCheck = MAX_NUM_MC_PLAYERS)
	//Initialise Variables
	INT i
	
	FOR i = 0 TO MAX_NUM_MC_PLAYERS-1
		playerStateListData.uiSlot[i].sName = NULL_STRING()
		playerStateListData.uiSlot[i].iValue = -3	//None
		
		playerStateListData.piPlayerStateList[i] = INVALID_PLAYER_INDEX()
		playerStateListData.iPlayerStateList[i] = -3	//None
	ENDFOR
	
	FOR i = 0 TO FMMC_MAX_TEAMS - 1
		playerStateListData.iCurrentPlayerCheck[i] = 0
		
		playerStateListData.hcTeam[i] = HUD_COLOUR_PURE_WHITE
	ENDFOR
	
	playerStateListData.bSortLocalPlayerTeamTop = bSortLocalPlayerTeamTop
	playerStateListData.iNumberOfPlayersToDisplayPerTeam = iNumberOfPlayersToDisplayPerTeam
	playerStateListData.iNumberOfTeamsToCheck = iNumberOfTeamsToCheck
	playerStateListData.iNumberOfPlayersToCheck = iNumberOfPlayersToCheck
ENDPROC

PROC PLAYER_STATE_LIST_ADD_PLAYER(PLAYER_STATE_LIST_DATA &playerStateListData, INT iParticipant, INT iValue)
	PARTICIPANT_INDEX participantIndex = INT_TO_PARTICIPANTINDEX(iParticipant)
	
	IF NETWORK_IS_PARTICIPANT_ACTIVE(participantIndex)
		PLAYER_INDEX playerIndex = NETWORK_GET_PLAYER_INDEX(participantIndex)
		
		IF MC_playerBD[iParticipant].iTeam != -1 AND MC_playerBD[iParticipant].iTeam < FMMC_MAX_TEAMS	//Valid Team
			IF playerStateListData.iCurrentPlayerCheck[MC_playerBD[iParticipant].iTeam] < playerStateListData.iNumberOfPlayersToDisplayPerTeam	//
				INT iTeamCheckOffset = MC_playerBD[iParticipant].iTeam * playerStateListData.iNumberOfPlayersToDisplayPerTeam
				
				playerStateListData.piPlayerStateList[playerStateListData.iCurrentPlayerCheck[MC_playerBD[iParticipant].iTeam] + iTeamCheckOffset] = playerIndex
				playerStateListData.iPlayerStateList[playerStateListData.iCurrentPlayerCheck[MC_playerBD[iParticipant].iTeam] + iTeamCheckOffset] = iValue
				
				playerStateListData.iCurrentPlayerCheck[MC_playerBD[iParticipant].iTeam]++
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL PLAYER_STATE_LIST_SORT(PLAYER_STATE_LIST_DATA &playerStateListData)
	INT i
	
	INT iSwapTeam = -1
	
	STRING sPlayerName[MAX_NUM_MC_PLAYERS]
	
	FOR i = 0 TO playerStateListData.iNumberOfPlayersToCheck - 1
		IF playerStateListData.piPlayerStateList[i] != INVALID_PLAYER_INDEX()
			sPlayerName[i] = GET_PLAYER_NAME(playerStateListData.piPlayerStateList[i])
			
			//Place the local player and their team at the top of Player State List
			IF playerStateListData.bSortLocalPlayerTeamTop
				IF NOT IS_STRING_NULL_OR_EMPTY(sPlayerName[i]) AND NOT IS_STRING_NULL_OR_EMPTY(GET_PLAYER_NAME(PlayerToUse))
					IF ARE_STRINGS_EQUAL(sPlayerName[i], GET_PLAYER_NAME(PlayerToUse))
						playerStateListData.uiSlot[0].sName = sPlayerName[i]
						playerStateListData.uiSlot[0].iValue = playerStateListData.iPlayerStateList[i]
						
						IF i > playerStateListData.iNumberOfPlayersToDisplayPerTeam - 1
							iSwapTeam = FLOOR(TO_FLOAT(i) / TO_FLOAT(playerStateListData.iNumberOfPlayersToDisplayPerTeam))
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	INT iCurrentSlotTeam[FMMC_MAX_TEAMS]
	
	FOR i = 0 TO playerStateListData.iNumberOfTeamsToCheck - 1
		iCurrentSlotTeam[i] = i * playerStateListData.iNumberOfPlayersToDisplayPerTeam
	ENDFOR
	
	IF playerStateListData.bSortLocalPlayerTeamTop
		iCurrentSlotTeam[0] = 1	//Local player has slot 0...
	ENDIF
	
	FOR i = 0 TO playerStateListData.iNumberOfTeamsToCheck - 1
		playerStateListData.hcTeam[i] = GET_HUD_COLOUR_FOR_FMMC_TEAM(i, PlayerToUse)
	ENDFOR
	
	IF iSwapTeam != -1
		HUD_COLOURS hcTemp = playerStateListData.hcTeam[0]
		playerStateListData.hcTeam[0] = playerStateListData.hcTeam[iSwapTeam]
		playerStateListData.hcTeam[iSwapTeam] = hcTemp
	ENDIF
	
	FOR i = 0 TO MAX_NUM_MC_PLAYERS-1
		IF (playerStateListData.bSortLocalPlayerTeamTop
		AND NOT IS_STRING_NULL_OR_EMPTY(sPlayerName[i]) 
		AND NOT IS_STRING_NULL_OR_EMPTY(playerStateListData.uiSlot[0].sName) 
		AND NOT ARE_STRINGS_EQUAL(sPlayerName[i], playerStateListData.uiSlot[0].sName))	//Not local player
		OR (NOT playerStateListData.bSortLocalPlayerTeamTop)
			INT iTeamIndex = FLOOR(TO_FLOAT(i) / TO_FLOAT(playerStateListData.iNumberOfPlayersToDisplayPerTeam))
			
			IF iSwapTeam != -1
				IF iTeamIndex = 0
					iTeamIndex = iSwapTeam
				ELIF iTeamIndex = iSwapTeam
					iTeamIndex = 0
				ENDIF
			ENDIF
			
			playerStateListData.uiSlot[iCurrentSlotTeam[iTeamIndex]].sName = sPlayerName[i]
			playerStateListData.uiSlot[iCurrentSlotTeam[iTeamIndex]].iValue = playerStateListData.iPlayerStateList[i]
			
			iCurrentSlotTeam[iTeamIndex]++
		ENDIF
	ENDFOR
	
	IF NOT IS_STRING_NULL_OR_EMPTY(playerStateListData.uiSlot[0].sName)
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

PROC PLAYER_STATE_LIST_DISPLAY(PLAYER_STATE_LIST_DATA &playerStateListData, INT iEventTimerDisplay, HUD_COLOURS EventTimerColour, BOOL bIsTimerNotDistance = FALSE)
	BOTTOM_RIGHT_UI_PLAYER_STATE_TIMER(playerStateListData.uiSlot[0].sName, playerStateListData.uiSlot[0].iValue,
									   playerStateListData.uiSlot[1].sName, playerStateListData.uiSlot[1].iValue,
									   playerStateListData.uiSlot[2].sName, playerStateListData.uiSlot[2].iValue,
									   playerStateListData.uiSlot[3].sName, playerStateListData.uiSlot[3].iValue,
									   playerStateListData.uiSlot[4].sName, playerStateListData.uiSlot[4].iValue,
									   playerStateListData.uiSlot[5].sName, playerStateListData.uiSlot[5].iValue,
									   playerStateListData.uiSlot[6].sName, playerStateListData.uiSlot[6].iValue,
									   playerStateListData.uiSlot[7].sName, playerStateListData.uiSlot[7].iValue,
									   playerStateListData.hcTeam[0], playerStateListData.hcTeam[1],
									   iEventTimerDisplay,
									   EventTimerColour,
									   "MC_TIMEOL", 
									   IS_LOCAL_PLAYER_ANY_SPECTATOR(), 
									   bIsTimerNotDistance)
ENDPROC

PLAYER_STATE_LIST_DATA playerStateListDataCrossTheLine

BOOL bIsOverLine[8 /*MAX_NUM_MC_PLAYERS*/]

PROC CALCULATE_DISTANCES_TO_LOCATE(INT iLoc)
	#IF IS_DEBUG_BUILD	IF bDebugCrossTheLineHUDPrints	PRINTLN("CALCULATE_DISTANCES_TO_LOCATE - - - - - - - iLoc = ", iLoc)	ENDIF	#ENDIF
	INT iParticipant
	
	FLOAT fDistanceToCrossline
	
	PARTICIPANT_INDEX participantIndex
	
	INT iTeamsToCheck, iTeamLoop
	
	FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_CrossLineDistUseMe_T0 + iTeamLoop)
			SET_BIT(iTeamsToCheck, iTeamLoop)
			
			#IF IS_DEBUG_BUILD	IF bDebugCrossTheLineHUDPrints	PRINTLN("CALCULATE_DISTANCES_TO_LOCATE - SET_BIT(iTeamsToCheck, iTeamLoop) ... iTeamLoop = ", iTeamLoop)	ENDIF	#ENDIF
		ENDIF
	ENDFOR
	
	FLOAT fDistanceToCrosslineLoop[6]
	
	FOR iParticipant = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		participantIndex = INT_TO_PARTICIPANTINDEX(iParticipant)

		IF NETWORK_IS_PARTICIPANT_ACTIVE(participantIndex)
			PLAYER_INDEX playerIndex = NETWORK_GET_PLAYER_INDEX(participantIndex)
			
			IF GET_PLAYER_TEAM(playerIndex) != -1			
				IF NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(playerIndex)
				AND NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_ANY_SPECTATOR)
				AND NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_HEIST_SPECTATOR)
					IF IS_BIT_SET(iTeamsToCheck, MC_playerBD[iParticipant].iTeam)
						IF NOT (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_CrossLineLocalTeamOnly) 
						AND (MC_playerBD[iParticipant].iTeam) <> MC_playerBD[iPartToUse].iTeam)
							IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][MC_playerBD[iParticipant].iTeam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iParticipant].iTeam]
								#IF IS_DEBUG_BUILD	IF bDebugCrossTheLineHUDPrints	PRINTLN("CALCULATE_DISTANCES_TO_LOCATE - iParticipant = ", iParticipant)	ENDIF	#ENDIF
								#IF IS_DEBUG_BUILD	IF bDebugCrossTheLineHUDPrints	PRINTLN("CALCULATE_DISTANCES_TO_LOCATE - MC_playerBD[iParticipant].iTeam = ", MC_playerBD[iParticipant].iTeam)	ENDIF	#ENDIF
								
								PED_INDEX pedIndex = GET_PLAYER_PED(playerIndex)
								
								IF NOT IS_ENTITY_DEAD(pedIndex)
									IF MC_playerBD[iParticipant].iCurrentLoc = iLoc
										fDistanceToCrossline = -1	//DISPLAY TICK
									ELSE
										VECTOR vPedCoords = GET_ENTITY_COORDS(pedIndex)
										
										vPedCoords = <<vPedCoords.X, vPedCoords.Y, 0.0>>
										
										ANGLED_AREA_CORNER_STRUCT vAngledAreaCorners = GET_CORNERS_OF_ANGLED_AREA(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].vLoc1, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].vLoc2, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fWidth)
										
										vAngledAreaCorners.vCorner[0] = <<vAngledAreaCorners.vCorner[0].X, vAngledAreaCorners.vCorner[0].Y, 0.0>>
										vAngledAreaCorners.vCorner[1] = <<vAngledAreaCorners.vCorner[1].X, vAngledAreaCorners.vCorner[1].Y, 0.0>>
										vAngledAreaCorners.vCorner[2] = <<vAngledAreaCorners.vCorner[2].X, vAngledAreaCorners.vCorner[2].Y, 0.0>>
										vAngledAreaCorners.vCorner[3] = <<vAngledAreaCorners.vCorner[3].X, vAngledAreaCorners.vCorner[3].Y, 0.0>>
										
										#IF IS_DEBUG_BUILD	IF bDebugCrossTheLineHUDPrints
										PRINTLN("CALCULATE_DISTANCES_TO_LOCATE - vAngledAreaCorners.vCorner[0] = ", vAngledAreaCorners.vCorner[0])
										PRINTLN("CALCULATE_DISTANCES_TO_LOCATE - vAngledAreaCorners.vCorner[1] = ", vAngledAreaCorners.vCorner[1])
										PRINTLN("CALCULATE_DISTANCES_TO_LOCATE - vAngledAreaCorners.vCorner[2] = ", vAngledAreaCorners.vCorner[2])
										PRINTLN("CALCULATE_DISTANCES_TO_LOCATE - vAngledAreaCorners.vCorner[3] = ", vAngledAreaCorners.vCorner[3])
										ENDIF	#ENDIF
											
										
										
										fDistanceToCrosslineLoop[0] = MINIMUM_DISTANCE_POINT_TO_LINE(vAngledAreaCorners.vCorner[0], vAngledAreaCorners.vCorner[1], vPedCoords)
										fDistanceToCrosslineLoop[1] = MINIMUM_DISTANCE_POINT_TO_LINE(vAngledAreaCorners.vCorner[0], vAngledAreaCorners.vCorner[2], vPedCoords)
										fDistanceToCrosslineLoop[2] = MINIMUM_DISTANCE_POINT_TO_LINE(vAngledAreaCorners.vCorner[0], vAngledAreaCorners.vCorner[3], vPedCoords)
										fDistanceToCrosslineLoop[3] = MINIMUM_DISTANCE_POINT_TO_LINE(vAngledAreaCorners.vCorner[1], vAngledAreaCorners.vCorner[2], vPedCoords)
										fDistanceToCrosslineLoop[4] = MINIMUM_DISTANCE_POINT_TO_LINE(vAngledAreaCorners.vCorner[1], vAngledAreaCorners.vCorner[3], vPedCoords)
										fDistanceToCrosslineLoop[5] = MINIMUM_DISTANCE_POINT_TO_LINE(vAngledAreaCorners.vCorner[2], vAngledAreaCorners.vCorner[3], vPedCoords)
										
										fDistanceToCrossline = 99999
										
										INT iCornerLoop
										
										FOR iCornerLoop = 0 TO COUNT_OF(fDistanceToCrosslineLoop) - 1
											IF fDistanceToCrosslineLoop[iCornerLoop] < fDistanceToCrossline
												fDistanceToCrossline = fDistanceToCrosslineLoop[iCornerLoop]
											ENDIF
										ENDFOR
										
										fDistanceToCrossline = ABSF(fDistanceToCrossline)
									ENDIF
								ELSE
									fDistanceToCrossline = -2	//Dead
								ENDIF
								
								IF NOT SHOULD_USE_METRIC_MEASUREMENTS()
									IF fDistanceToCrossline >= 0
										//fDistanceToCrossline = fDistanceToCrossline * 3.2808399	//1 metre = 3.2808399 feet
										
										fDistanceToCrossline = CONVERT_METERS_TO_FEET(fDistanceToCrossline)
									ENDIF
								ENDIF
								
								IF playerIndex != LocalPlayer
									INT iTeamCheckOffset = MC_playerBD[iParticipant].iTeam * playerStateListDataCrossTheLine.iNumberOfPlayersToDisplayPerTeam
									
									IF NOT bIsOverLine[playerStateListDataCrossTheLine.iCurrentPlayerCheck[MC_playerBD[iParticipant].iTeam] + iTeamCheckOffset]
										IF fDistanceToCrossline = -1
											bIsOverLine[playerStateListDataCrossTheLine.iCurrentPlayerCheck[MC_playerBD[iParticipant].iTeam] + iTeamCheckOffset] = TRUE
											
											INT iTeamToCheck = MC_playerBD[iParticipant].iTeam
											INT iCTLSoundID = GET_SOUND_ID()
											FLOAT fIntensity = TO_FLOAT(MC_serverBD_1.iNumberOfTeamInArea[iLoc][iTeamToCheck]) / TO_FLOAT(MC_serverBD.iNumberOfPlayingPlayers[iTeamToCheck])
											
											IF MC_playerBD[iParticipant].iTeam = MC_playerBD[iPartToUse].iTeam
												PRINTLN("[CTLSFX] Playing Friendly with Intensity ", fIntensity)
												PLAY_SOUND_FRONTEND(iCTLSoundID,"Remote_Friendly_Enter_Line","GTAO_FM_Cross_The_Line_Soundset", FALSE)
											ELSE
												PRINTLN("[CTLSFX] Playing Enemy with Intensity ", fIntensity)
												PLAY_SOUND_FRONTEND(iCTLSoundID,"Remote_Enemy_Enter_Line","GTAO_FM_Cross_The_Line_Soundset", FALSE)
											ENDIF
											
											SET_VARIABLE_ON_SOUND(iCTLSoundID, "intensity", fIntensity)
										ENDIF
									ELSE
										IF fDistanceToCrossline >= 0
											bIsOverLine[playerStateListDataCrossTheLine.iCurrentPlayerCheck[MC_playerBD[iParticipant].iTeam] + iTeamCheckOffset] = FALSE
										ENDIF
									ENDIF
								ENDIF
								
								PLAYER_STATE_LIST_ADD_PLAYER(playerStateListDataCrossTheLine, iParticipant, ROUND(fDistanceToCrossline))
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

FUNC INT RETURN_DISTANCE_FROM_LINE_FOR_LOCAL_PLAYER(PLAYER_STATE_LIST_DATA &playerStateListData)
	IF NOT IS_STRING_NULL_OR_EMPTY(playerStateListData.uiSlot[0].sName)
		IF playerStateListData.bSortLocalPlayerTeamTop
			RETURN playerStateListData.uiSlot[0].iValue
		ELSE
			INT i
			
			FOR i = 0 TO playerStateListData.iNumberOfPlayersToCheck - 1
				IF playerStateListData.piPlayerStateList[i] = PLAYER_ID()
					RETURN playerStateListData.iPlayerStateList[i]
				ENDIF
			ENDFOR
		ENDIF
	ENDIF
	
	RETURN -3	//None
ENDFUNC

FLOAT fDistToLoc = 9999999.9
FUNC BOOL SHOULD_WRONG_WAY_BE_DISPLAYED(INT iLoc)
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[ iTeam ]
	
	IF (NOT IS_TEAM_ACTIVE(iTeam))
	OR (GET_MC_SERVER_GAME_STATE() = GAME_STATE_END)
	OR IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_FINISHED)
	OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_SUB_TEAM_ROLES) AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRolesBitset[iRule][MC_playerBD[iLocalPart].iCoronaRole], ciBS_ROLE_Cant_Complete_This_Rule))
		IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_WRONG_WAY)
			CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_WRONG_WAY)
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
		fDistToLoc = 9999999.9
		PRINTLN("[MMacK][WrongWay] Reset fDistToLoc, new priority this frame")
	ENDIF
	
	IF iRule < FMMC_MAX_RULES
		IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam] = iRule
			PRINTLN("[MMacK][WrongWay] SHOULD_WRONG_WAY_BE_DISPLAYED - STARTING")
			PRINTLN("[MMacK][WrongWay] SHOULD_WRONG_WAY_BE_DISPLAYED - Checking ", iLoc)
			VECTOR vPlayerPos, vNextLocationPos
			vPlayerPos = GET_ENTITY_COORDS(LocalPlayerPed)
			vNextLocationPos = GET_LOCATION_VECTOR(iLoc) 
			
			FLOAT fCurrDistToLoc = VDIST2(vPlayerPos, vNextLocationPos)
			
			IF fCurrDistToLoc >= fDistToLoc
				PRINTLN("[MMacK][WrongWay] SHOULD_WRONG_WAY_BE_DISPLAYED - fCurrDistToLoc ", fCurrDistToLoc)
				PRINTLN("[MMacK][WrongWay] SHOULD_WRONG_WAY_BE_DISPLAYED - fDistToLoc ", fDistToLoc)
				PRINTLN("[MMacK][WrongWay] SHOULD_WRONG_WAY_BE_DISPLAYED - YES")
				fDistToLoc = fCurrDistToLoc
				RETURN TRUE
			ELSE
				IF GET_ENTITY_SPEED(LocalPlayerPed) > 1
					IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_WRONG_WAY)
						CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_WRONG_WAY)
					ENDIF	
					
					fDistToLoc = fCurrDistToLoc + 8.25
					
					PRINTLN("[MMacK][WrongWay] SHOULD_WRONG_WAY_BE_DISPLAYED - fDistToLoc set ", fDistToLoc)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_WRONG_WAY)
			CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_WRONG_WAY)
		ENDIF	
	ENDIF
	
	PRINTLN("[MMacK][WrongWay] SHOULD_WRONG_WAY_BE_DISPLAYED - NO")
	RETURN FALSE
ENDFUNC

PROC DISPLAY_WRONG_WAY_SHARD(INT iTeam, INT iRule)
	IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63WrongWayMessage[iRule])
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_TOUR_DE_FORCE (g_FMMC_STRUCT.iAdversaryModeType)
		PRINTLN("[MMacK][WrongWay] DISPLAY_WRONG_WAY_SHARD - Text OK")
		IF !IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_WRONG_WAY)
			PRINTLN("[MMacK][WrongWay] DISPLAY_WRONG_WAY_SHARD - Setting up Message")
			SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_WRONG_WAY, "MC_RTN_OBJ_C",g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63WrongWayMessage[iRule],DEFAULT, 999999)
		ENDIF
		CLEAR_PRINTS()
	ELSE
		PRINTLN("[MMacK][WrongWay] DISPLAY_WRONG_WAY_SHARD - Text not set")
		SCRIPT_ASSERT("DISPLAY_WRONG_WAY_SHARD called but no Wrong Way message entered by content. Bug for *Default Online Content Creation*")
	ENDIF
ENDPROC

PROC PROCESS_LOCATION_OUTFIT_BLIP_RANGE_RESET(INT iLoc)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS3, ciLoc_BS3_RequiresStartingOutfitForUnlimitedBlipRange)
		EXIT
	ENDIF
	
	IF MC_playerBD[iParttoUse].iStartingOutfit = -1 	
		EXIT
	ENDIF
	
	IF MC_playerBD[iParttoUse].iOutfit != MC_playerBD[iLocalPart].iStartingOutfit
	AND g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.fBlipRange != 10.0
		g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.fBlipRange = 10.0
		g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.fBlipScale -= 0.3
		PRINTLN("[LM][PROCESS_LOCATION_OUTFIT_BLIP_RANGE_RESET] - Setting fBlipRange to: ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.fBlipRange)
	ENDIF
ENDPROC

SCRIPT_TIMER stStopGoHUD
SCRIPT_TIMER stStopHUD

PROC DISPLAY_LOCATION_HUD( INT iloc )
	
	INT icontolHUDFlash
	BOOL bInLoc 
	INT i

	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iPriority = MC_serverBD_4.iCurrentHighestPriority[ iTeam ]
	INT iTeamLoop
	
	TEXT_LABEL_63 tlCaptureBarText

	IF bIsLocalPlayerHost
		FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
			IF MC_serverBD_4.iGotoLocationDataRule[iloc][iTeamLoop] = FMMC_OBJECTIVE_LOGIC_GO_TO
				IF MC_serverBD_4.iGotoLocationDataPriority[iloc][iTeamLoop] < FMMC_MAX_RULES
					IF MC_serverBD_4.iGotoLocationDataPriority[iloc][iTeamLoop] = MC_serverBD_4.iCurrentHighestPriority[iTeamLoop]
						IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].mnVehicleNeeded[iTeamLoop] != DUMMY_MODEL_FOR_SCRIPT
							iTempPriorityLocation[iTeamLoop] = iloc
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF

	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS2, ciLoc_BS2_OnlyShowBlipWhenOffRadar)
		IF DOES_BLIP_EXIST(LocBlip[iloc])
			IF IS_ENTITY_AT_COORD(LocalPlayerPed,GET_LOCATION_VECTOR(iloc),<<100,100,100>>)	
				PRINTLN("[RCC MISSION][LOCATES] Inside Locate alpha blip sprite to ZERO " )
				SET_BLIP_ALPHA(LocBlip[iloc], 0) 
			ELSE
				SET_BLIP_ALPHA(LocBlip[iloc], 255) 
			ENDIF
		ENDIF
	ENDIF	

	IF DOES_BLIP_EXIST(LocBlip[iloc])
		IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fTeamMembersNeededWithin > 0
			IF ARE_ALL_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE(iTeam)
				SET_BLIP_COLOUR(LocBlip[iloc], BLIP_COLOUR_YELLOW)
			ELSE
				SET_BLIP_COLOUR(LocBlip[iloc], BLIP_COLOUR_RED)
			ENDIF
		ENDIF
	ENDIF
	
	FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF MC_serverBD_4.iGotoLocationDataRule[iloc][iTeamLoop] = FMMC_OBJECTIVE_LOGIC_CAPTURE
			IF MC_serverBD_4.iGotoLocationDataPriority[iloc][iTeamLoop] < FMMC_MAX_RULES
				IF MC_serverBD_4.iGotoLocationDataPriority[iloc][iTeamLoop] = MC_serverBD_4.iCurrentHighestPriority[iTeamLoop]
					iTempCaptureLocation = iloc
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].iLocBS2, ciLoc_BS2_DisplayMarkerAsVisualOnly )
		UPDATE_LOCATE_FADE_STRUCT( sFadeOutLocations[ iLoc ], iLoc, iTeam )
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ALL_TEAMS_SHARE_LOCATES)
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME+iTeam)
			MC_playerBD_1[iLocalPart].iLocBitset = 0
		ENDIF
	ENDIF
	
	PROCESS_LOCATION_OUTFIT_BLIP_RANGE_RESET(iloc)	
	
	//Rugby lines
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciSET_LINES_AS_RUGBY_LINES)
		//On looping through locates find those that are display as visual only and assign iLoc references
		IF iLine1iLoc = - 1
			IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].iLocBS2, ciLoc_BS2_DisplayMarkerAsVisualOnly )
				iLine1iLoc = iLoc
				PRINTLN("[RCC MISSION][RUGBY LINE] iLine1iLoc: ",iLine1iLoc ) 
			ENDIF
		ELSE
			IF iLine2iLoc = - 1
				IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].iLocBS2, ciLoc_BS2_DisplayMarkerAsVisualOnly )
					iLine2iLoc = iLoc
					PRINTLN("[RCC MISSION][RUGBY LINE] iLine2iLoc: ",iLine2iLoc ) 
				ENDIF
			ENDIF
		ENDIF
		//Compare distance from the local players start point to work out which is theirs
		IF iLine1iLoc  != - 1
		AND iLine2iLoc != - 1
			IF GET_DISTANCE_BETWEEN_COORDS(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[MC_playerBD[iPartToUse].iteam][0].vPos, GET_LOCATION_VECTOR(iLine1iLoc)) < GET_DISTANCE_BETWEEN_COORDS(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[MC_playerBD[iPartToUse].iteam][0].vPos, GET_LOCATION_VECTOR(iLine2iLoc))
				//sFadeOutLocations[ iLine1iLoc ].iAlphaValue
				//The enemies line - ORANGE
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iLine1iLoc ].iLocBS2, ciLoc_BS2_DisplayMarkerAsVisualOnly )
					DRAW_ONE_LOCATION_MARKER( iLine1iLoc, 125,TRUE,-1,TRUE)
					CREATE_BLIP_FOR_RUGBY_LINE(iLine1iLoc, TRUE,TRUE)
					PRINTLN("[RCC MISSION][RUGBY LINE] iLine1iLoc: ",iLine1iLoc ) 
					PRINTLN("[RCC MISSION][RUGBY LINE] iLine1iLoc set to be Friendly.")
				ENDIF

				//Your objective - PURPLE
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iLine2iLoc ].iLocBS2, ciLoc_BS2_DisplayMarkerAsVisualOnly )
					DRAW_ONE_LOCATION_MARKER( iLine2iLoc,125,FALSE)
					CREATE_BLIP_FOR_RUGBY_LINE(iLine2iLoc,TRUE,FALSE)
					PRINTLN("[RCC MISSION][RUGBY LINE] iLine2iLoc: ",iLine2iLoc ) 
					PRINTLN("[RCC MISSION][RUGBY LINE] iLine2iLoc set to be Enemy")
				ENDIF
			ELSE
				//The enemies line - PURPLE
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iLine2iLoc ].iLocBS2, ciLoc_BS2_DisplayMarkerAsVisualOnly )
					DRAW_ONE_LOCATION_MARKER( iLine2iLoc, 125,TRUE,-1,FALSE)
					CREATE_BLIP_FOR_RUGBY_LINE(iLine2iLoc,TRUE,FALSE)
					PRINTLN("[RCC MISSION][RUGBY LINE] iLine2iLoc: ",iLine2iLoc ) 
					PRINTLN("[RCC MISSION][RUGBY LINE] iLine2iLoc set to be Friendly.")
				ENDIF
				//Your objective - ORANGE
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iLine1iLoc ].iLocBS2, ciLoc_BS2_DisplayMarkerAsVisualOnly )
					DRAW_ONE_LOCATION_MARKER( iLine1iLoc, 125,FALSE,-1,TRUE)
					CREATE_BLIP_FOR_RUGBY_LINE(iLine1iLoc,TRUE,TRUE)
					PRINTLN("[RCC MISSION][RUGBY LINE] iLine1iLoc: ",iLine1iLoc ) 
					PRINTLN("[RCC MISSION][RUGBY LINE] iLine1iLoc set to be Enemy")
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	
	IF iSpectatorTarget = -1
		IF bPlayerToUseOK
			IF NOT IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].iLocBS2, ciLoc_BS2_DisplayMarkerAsVisualOnly )
				IF  iPriority < FMMC_MAX_RULES
				AND iPriority != -1
					IF NOT IS_VECTOR_ZERO(GET_LOCATION_VECTOR(iloc))
						
						IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fTeamMembersNeededWithin > 0
							fTeamMembers_ReqDist = CALCULATE_MINIMUM_DISTANCE_BETWEEN_TEAM_MATES(iLoc,iTeam) 
						ENDIF
						
						//IF IS_ENTITY_AT_COORD(LocalPlayerPed,GET_LOCATION_VECTOR(iloc,0),<<g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fRadius[0],g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fRadius[0],g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fRadius[0]>>)
						BOOL bPedInLocation = IS_PED_IN_LOCATION(LocalPlayerPed, iloc,  iTeam)
						
						IF bPedInLocation
						OR SHOULD_MARK_THIS_GOTO_AS_DONE_FOR_APARTMENT_DROPOFF()
						OR HAS_ANY_ONE_HIT_LOCATE(iLoc, iPriority)
							bInLoc = TRUE
						
							#IF IS_DEBUG_BUILD
							IF bGotoLocPrints
								PRINTLN("[RCC MISSION] DISPLAY_LOCATION_HUD ",iloc," - Player is in location")
							ENDIF
							#ENDIF
							
							IF MC_playerBD[iPartToUse].iObjectiveTypeCompleted =-1
								PRINTLN("[MMacK][LeaveArea] 4")
								IF IS_LOCATION_AN_OBJECTIVE_FOR_ANY_TEAM(iloc)
									iInAnyLocTemp = iloc
									IF SHOULD_COUNT_LEAVE_LOCATION(iloc)
										MC_playerBD[iPartToUse].iLeaveLoc = iloc
										IF MC_serverBD_4.iGotoLocationDataPriority[iloc][ iTeam ] <= iPriority
											SET_BIT(iLocalBoolCheck6,LBOOL6_IN_MY_PRIORITY_LEAVE_LOC)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
													
							IF MC_serverBD_4.iGotoLocationDataPriority[ iloc ][ iTeam ] = iPriority // Note(Owain): this might cause locates to stay around forever
								IF sFadeOutLocations[ iLoc ].eState = eLocateFadeOutState_DISPLAYING
									IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].fSecondaryRadius = 0
									OR ( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].iWholeTeamAtLocation[ iTeam ] != ciGOTO_LOCATION_INDIVIDUAL
										AND AM_I_IN_SECONDARY_LOCATE_IF_I_NEED_TO_BE( iloc ) )
										
										PRINTLN("[RCC MISSION] DISPLAY_LOCATION_HUD ",iloc," Fading out locate radius = ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].fRadius[0] )
										
										SET_LOCATE_TO_FADE_OUT( sFadeOutLocations[ iLoc ], iLoc, iTeam )
										
									ENDIF
								ENDIF
							ELIF sFadeOutLocations[ iLoc ].iAlphaValue = 0
							AND sFadeOutLocations[ iLoc ].eState = eLocateFadeOutState_BEFORE_FADE_IN
								SET_LOCATE_TO_NOT_DISPLAY( sFadeOutLocations[ iLoc ], iLoc, iTeam )
							ENDIF
							
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ALL_TEAMS_SHARE_LOCATES)
								SET_BIT(MC_playerBD_1[iLocalPart].iLocBitset, iLoc)
								PRINTLN("[RCC MISSION] Setting MC_playerBD_1[iLocalPart].iLocBitset, ", iLoc)
							ENDIF
							iLocateFromPreviousFrame = iLoc
							
						ELIF MC_serverBD_4.iGotoLocationDataPriority[ iloc ][ iTeam ] = iPriority
						AND sFadeOutLocations[ iLoc ].eState = eLocateFadeOutState_AFTER_FADE_OUT
						AND (g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].iWholeTeamAtLocation[ iTeam ] != ciGOTO_LOCATION_INDIVIDUAL
							OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_REACTIVATE_LOCATES))
							#IF IS_DEBUG_BUILD
							IF bGotoLocPrints
								PRINTLN("[RCC MISSION] DISPLAY_LOCATION_HUD ",iloc," - Player is not in location, locate should fade back in")
							ENDIF
							#ENDIF
							
							// If this locate is still the current rule for an all-team-at-locate, fade it back in?
							RESET_LOCATE( iLoc, sFadeOutLocations[ iLoc ], iTeam )
							 
							SET_LOCATE_TO_FADE_IN( sFadeOutLocations[ iLoc ], iLoc, iTeam )
						#IF IS_DEBUG_BUILD
						ELIF bGotoLocPrints
							PRINTLN("[RCC MISSION] DISPLAY_LOCATION_HUD ",iloc," - Player is not in location, B")
						#ENDIF
						
						ENDIF
						
						IF NOT bPedInLocation
						AND iLocateFromPreviousFrame = iloc
							
							iCurrCapTime[iloc][iTeam] = -1
							
							// Broadcast that we just left a locate.
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetSixteen, ciDISABLE_RECPATURE_OF_LOCATES_WHEN_LEAVING_ONE)	
								PRINTLN("[RCC MISSION] [LM]DISPLAY_LOCATION_HUD - IN")
								IF NOT HAS_NET_TIMER_STARTED(tdDisableLocateRecaptureTimer[iLocalPart])
									PRINTLN("[RCC MISSION] DISPLAY_LOCATION_HUD - [Delay Recapture] - Left Locate")
									iLocateFromPreviousFrame = -1
									BROADCAST_FMMC_LEFT_LOCATE_DELAY_RECAPTURE(iLocalPart, iloc)
								ENDIF
							ELSE	
								iLocateFromPreviousFrame = -1
							ENDIF
						ENDIF
					#IF IS_DEBUG_BUILD
					ELIF bGotoLocPrints
						PRINTLN("[RCC MISSION] DISPLAY_LOCATION_HUD ",iloc," - Location vector is zero")
					#ENDIF
					ENDIF
				ENDIF
			ELSE
				UPDATE_LOCATE_FADE_STRUCT_FOR_LINE_MARKER( sFadeOutLocations[ iLoc ], iLoc, iTeam, RETURN_DISTANCE_FROM_LINE_FOR_LOCAL_PLAYER(playerStateListDataCrossTheLine))
			ENDIF
		ELSE
			MC_playerBD[iPartToUse].iLeaveLoc = -1
			
			#IF IS_DEBUG_BUILD
			IF bGotoLocPrints
				PRINTLN("[RCC MISSION] DISPLAY_LOCATION_HUD ",iloc," - Player isn't okay")
			ENDIF
			#ENDIF
		ENDIF
		
		IF  iPriority < FMMC_MAX_RULES
		AND iPriority != -1
			IF MC_serverBD_4.iGotoLocationDataPriority[iloc][iTeam] <= iPriority
			AND MC_serverBD_4.iGotoLocationDataPriority[iloc][ iTeam ] < FMMC_MAX_RULES
				IF MC_playerBD[iPartToUse].iObjectiveTypeCompleted = -1
				OR (IS_THIS_ROCKSTAR_MISSION_NEW_VS_KEEP_THE_PACE(g_FMMC_STRUCT.iAdversaryModeType) AND MC_playerBD[iPartToUse].iCurrentLoc != iloc)
					
					IF bPlayerToUseOK

						//this is only to display the chevron at the right height - not very efficent but was what I was told to do.
						IF NOT IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].iLocBS2, ciLoc_BS2_DisplayMarkerAsVisualOnly )
							IF MC_playerBD[iPartToUse].iCurrentLoc != iloc								
							
								IF bInLoc
								
									MC_playerBD[iPartToUse].iCurrentLoc = iloc								
									
									IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].iLocBS2, ciLoc_BS2_ScanVFXOnCapture)
										IF IS_PED_IN_MODEL(LocalPlayerPed, STROMBERG)
											VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_USING(LocalPlayerPed)
											IF IS_PED_SITTING_IN_VEHICLE_SEAT(LocalPlayerPed,tempVeh,VS_DRIVER)
												SET_BIT(iLocalBoolCheck27, LBOOL27_STARTED_SUB_SCAN)
												PRINTLN("[SubScan] LBOOL27_STARTED_SUB_SCAN has been set!")
												PRINTLN("[STROM][JR] - DOES VFX EXIST ", BOOL_TO_INT(DOES_PARTICLE_FX_LOOPED_EXIST(ptfxCapScanEffect)))																												
												IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxCapScanEffect)
													DISPLAY_CAP_SCAN_VFX()
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									
									IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
										IF NOT IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitset3, PBBOOL3_OVERTIME_LANDED_SUCCESSFULLY)
											PRINTLN("[LM][OVERTIME] - LBOOL22_OVERTIME_LANDING_SUCCESS. Landing sucessful.")
											SET_BIT(MC_PlayerBD[iLocalPart].iClientBitset3, PBBOOL3_OVERTIME_LANDED_SUCCESSFULLY)
											BROADCAST_FMMC_OVERTIME_LANDING(TRUE, iLocalPart, MC_PlayerBD[iLocalPart].iTeam)
										ENDIF
									ENDIF
									
									#IF IS_DEBUG_BUILD
									IF bGotoLocPrints
										PRINTLN("[RCC MISSION] DISPLAY_LOCATION_HUD ",iloc," - Setting iCurrentLoc as this locate!")
									ENDIF
									#ENDIF
									
									IF MC_serverBD_4.iGotoLocationDataRule[iloc][ iTeam ] != FMMC_OBJECTIVE_LOGIC_PHOTO
										
										IF iOldLoc != MC_playerBD[iPartToUse].iCurrentLoc
											IF NOT IS_BIT_SET(iLocalBoolCheck2,LBOOL2_REQUEST_UPDATE)
												CLEAR_BIT(iLocalBoolCheck2,LBOOL2_WHOLE_UPDATE_DONE)
												SET_BIT(iLocalBoolCheck2,LBOOL2_REQUEST_UPDATE)
												PRINTLN("requesting update at loc")
											ENDIF
											iOldLoc = MC_playerBD[iPartToUse].iCurrentLoc
										ENDIF
										
									ENDIF								
								ENDIF
							#IF IS_DEBUG_BUILD
							ELIF bGotoLocPrints
								PRINTLN("[RCC MISSION] DISPLAY_LOCATION_HUD ",iloc," - iCurrentLoc is already set to this locate")
							#ENDIF
							ENDIF
						ENDIF
						
						IF (NOT bInLoc)
						OR (g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iWholeTeamAtLocation[ iTeam ] = ciGOTO_LOCATION_INDIVIDUAL)
						OR ((g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].fSecondaryRadius > 0) AND NOT AM_I_IN_SECONDARY_LOCATE_IF_I_NEED_TO_BE( iloc ))
						//OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_KEEP_THE_PACE(g_FMMC_STRUCT.iAdversaryModeType)
														
							IF ENUM_TO_INT( sFadeOutLocations[ iLoc ].eState ) < ENUM_TO_INT( eLocateFadeOutState_AFTER_FADE_OUT )
								 
								IF MC_serverBD_4.iGotoLocationDataPriority[ iloc ][ iTeam ] = iPriority // Only fade in if it's our current rule
									SET_LOCATE_TO_FADE_IN( sFadeOutLocations[ iLoc ], iLoc, iTeam )
								ENDIF
								IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciSET_LINES_AS_RUGBY_LINES)	
									INT iTeamWhoControlsLoc = -1
									IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iPriority], ciBS_RULE9_TEAM_COLOURED_LOCATES)
									OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_ZONE(g_FMMC_STRUCT.iRootContentIDHash)
										iTeamWhoControlsLoc = MC_serverBD.iLocOwner[iloc]
										
										// It doesn't matter if this sets a team twice, because we override that with the contested colour
										IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_INSTANT_COLOUR_CHANGES_FOR_INSIDE_LOCATE)
											FOR i = 0 TO FMMC_MAX_TEAMS-1
												IF MC_serverBD_1.inumberOfTeamInArea[iLoc][i] > 0												
													iTeamWhoControlsLoc = i
												ENDIF
											ENDFOR
										ENDIF
									ENDIF
									
									DRAW_ONE_LOCATION_MARKER( iLoc, sFadeOutLocations[ iLoc ].iAlphaValue, DEFAULT, iTeamWhoControlsLoc )
								ENDIF
							#IF IS_DEBUG_BUILD
							ELSE
								IF b4517649Debug
									PRINTLN("[4517649Debug] iLoc:", iLoc," Enum: ", ENUM_TO_INT(sFadeOutLocations[ iLoc ].eState))
								ENDIF
							#ENDIF
							ENDIF
						#IF IS_DEBUG_BUILD
						ELSE
							IF b4517649Debug
								PRINTLN("[4517649Debug] iLoc:", iLoc," NOT bInLoc: ", BOOL_TO_STRING((NOT bInLoc)))
								PRINTLN("[4517649Debug] iLoc:", iLoc," Check 2: ", BOOL_TO_STRING(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iWholeTeamAtLocation[ iTeam ] = ciGOTO_LOCATION_INDIVIDUAL))
								PRINTLN("[4517649Debug] iLoc:", iLoc," Check 3: ", BOOL_TO_STRING(((g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].fSecondaryRadius > 0) AND NOT AM_I_IN_SECONDARY_LOCATE_IF_I_NEED_TO_BE( iloc ))))
							ENDIF
						#ENDIF
						ENDIF
						
						IF MC_serverBD_4.iGotoLocationDataPriority[ iloc ][ iTeam ] = iPriority
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_DrawNextCP) 
								
								INT iLocToDraw = FMMC_MAX_GO_TO_LOCATIONS + 1
								INT iNextPriority = GET_OBJECTIVE_TO_JUMP_TO_FROM_CREATOR_DATA(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iJumpToObjectivePass[iTeam])
								
								IF iNextPriority = -1
								AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRandomNextRuleBS[iPriority] != 0
									iNextPriority = GET_FIRST_SET_BIT_IN_BITSET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRandomNextRuleBS[iPriority])
								ENDIF
								
								IF iNextPriority = -1
									iNextPriority = iPriority + 1
								ENDIF
								
								FOR i = 0 TO FMMC_MAX_GO_TO_LOCATIONS - 1
									IF MC_serverBD_4.iGotoLocationDataPriority[ i ][ iTeam ] = iNextPriority
									AND MC_serverBD_4.iGotoLocationDataPriority[ i ][ iTeam ] != FMMC_PRIORITY_IGNORE
										#IF IS_DEBUG_BUILD
										IF bDebugBlipPrints
											PRINTLN("[MMacK][NextCpBlip] next blip found")
										ENDIF #ENDIF
										iLocToDraw = i
										BREAKLOOP
									ENDIF
								ENDFOR
								
								IF (iLocToDraw > FMMC_MAX_GO_TO_LOCATIONS)
									IF MC_ServerBD_4.iTeamLapsCompleted[iTeam] >= 0
									AND MC_ServerBD_4.iTeamLapsCompleted[iTeam] < g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMissionLapLimit
									AND MC_ServerBD_4.iTeamLapsCompleted[iTeam] != (g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMissionLapLimit-1)
										//we have laps, so lets show the first checkpoint marker
										//also, lets not do this on the last lap either.
										#IF IS_DEBUG_BUILD
										IF bDebugBlipPrints
											PRINTLN("[MMacK][NextCpBlip] Laps!")
										ENDIF #ENDIF
										INT iLowestRule = 99999
										FOR i = 0 TO FMMC_MAX_GO_TO_LOCATIONS - 1
											IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iPriority[iTeam] < iLowestRule // Want to check the priority in the globals rather than that on the server, as we've already completed these rules
											AND g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iPriority[iTeam] >= iNextPriority
												iLowestRule = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iPriority[iTeam]
												iLocToDraw = i
											ENDIF
										ENDFOR
									ENDIF
								ENDIF
								
								#IF IS_DEBUG_BUILD
								IF bDebugBlipPrints
									PRINTLN("[MMacK][NextCpBlip] iLocToDraw = ", iLocToDraw)
								ENDIF #ENDIF
								
								IF (iLocToDraw < FMMC_MAX_GO_TO_LOCATIONS)
									IF NOT IS_VECTOR_ZERO(GET_LOCATION_VECTOR(iLocToDraw))
									AND NOT DOES_BLIP_EXIST(LocBlip1[iLocToDraw])
									AND IS_PED_IN_REQUIRED_LOCATION_VEHICLE(PlayerPedToUse, iTeam, iLocToDraw)
										#IF IS_DEBUG_BUILD
										IF bDebugBlipPrints
											PRINTLN("[MMacK][NextCpBlip] Added Blip")
											PRINTLN("[KH] ADDING BLIP(2) - LOC: ", iLocToDraw)
										ENDIF #ENDIF
										LocBlip1[iLocToDraw] = ADD_BLIP_FOR_COORD(GET_LOCATION_VECTOR(iLocToDraw))
										SET_BLIP_SCALE(LocBlip1[iLocToDraw], BLIP_SIZE_OUT_OF_TIME_SLOT)
										SET_BLIP_NAME_FROM_TEXT_FILE(LocBlip1[iLocToDraw], "FMMC_SELC" )
										SET_BLIP_COLOUR(LocBlip1[iLocToDraw], BLIP_COLOUR_YELLOW)
									ENDIF
									IF DOES_BLIP_EXIST(LocBlip1[iLocToDraw])
										IF  iPriority < FMMC_MAX_RULES
										AND iPriority != -1
											IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iPriority].fTeamMembersNeededWithin > 0
												IF ARE_ALL_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE(iTeam)
													SET_BLIP_COLOUR(LocBlip1[iLocToDraw], BLIP_COLOUR_YELLOW) 
												ELSE
													SET_BLIP_COLOUR(LocBlip1[iLocToDraw], BLIP_COLOUR_RED) 
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									
									//If we have a new loc to draw
									IF iPreviousLocToDraw >= 0
									AND iLocToDraw != iPreviousLocToDraw
										//Check the new blip exists so that the blip doesn't disappear briefly
										//IF DOES_BLIP_EXIST(LocBlip[iLocToDraw])
											//If the small blip exists, remove it
											IF DOES_BLIP_EXIST(LocBlip1[iPreviousLocToDraw])
												PRINTLN("[KH][NextCpBlip] Removed previous blip - iPreviousLocToDraw")
												REMOVE_BLIP(LocBlip1[iPreviousLocToDraw])
												iPreviousLocToDraw = iLocToDraw
											ENDIF
											iPreviousLocToDraw = iLocToDraw
										//ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF (MC_playerBD[iPartToUse].iCurrentLoc = iloc)
							IF MC_serverBD_4.iGotoLocationDataRule[iloc][ iTeam ] = FMMC_OBJECTIVE_LOGIC_PHOTO
								
								VECTOR vLoc = GET_LOCATION_VECTOR(iloc)
								
								IF NOT DOES_PHOTO_TRACKED_POINT_EXIST(iPhotoTrackedPoint)
									//create new tracked point for current target coords
									CREATE_PHOTO_TRACKED_POINT_FOR_COORD(iPhotoTrackedPoint, vLoc, 0.2)
									vLocalPhotoCoords = vLoc
								ELSE
									IF NOT ARE_VECTORS_EQUAL(vLoc, vLocalPhotoCoords)
										vLocalPhotoCoords = vLoc
										SET_TRACKED_POINT_INFO(iPhotoTrackedPoint,vLoc,0.2)
									ENDIF
								ENDIF
								
								IF DOES_PHOTO_TRACKED_POINT_EXIST(iPhotoTrackedPoint) AND IS_PHOTO_TRACKED_POINT_VISIBLE(iPhotoTrackedPoint)
									IF IS_POINT_IN_CELLPHONE_CAMERA_VIEW(vLoc, 0.2)
										IF HAS_CELLPHONE_CAM_JUST_TAKEN_PIC()
											MC_playerBD[iPartToUse].iLocPhoto = iloc
											PRINTLN("[RCC MISSION] DISPLAY_LOCATION_HUD - Photo taken of location ",iloc)
											
											IF SHOULD_PUT_AWAY_PHONE_AFTER_PHOTO(MC_serverBD_4.iGotoLocationDataPriority[iloc][iTeam], ci_TARGET_LOCATION)
												SET_BIT(iLocalBoolCheck3, LBOOL3_PHOTOTAKEN)
												REINIT_NET_TIMER(tdPhotoTakenTimer)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						// Broadcast that we just left a locate.
						IF MC_playerBD[iPartToUse].iCurrentLoc > -1
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetSixteen, ciDISABLE_RECPATURE_OF_LOCATES_WHEN_LEAVING_ONE)						
								IF NOT HAS_NET_TIMER_STARTED(tdDisableLocateRecaptureTimer[iLocalPart])
									iLocateFromPreviousFrame = -1
									BROADCAST_FMMC_LEFT_LOCATE_DELAY_RECAPTURE(iLocalPart, MC_playerBD[iLocalPart].iCurrentLoc)
								ENDIF
							ENDIF
						ENDIF
						
						MC_playerBD[iPartToUse].iCurrentLoc = -1						
											
						#IF IS_DEBUG_BUILD
						IF bGotoLocPrints
							PRINTLN("[RCC MISSION] DISPLAY_LOCATION_HUD ",iloc," - Unable to set iCurrentLoc as player isn't okay")
						ENDIF
						#ENDIF
					ENDIF
				ELIF MC_playerBD[iPartToUse].iCurrentLoc != iLoc
				
					#IF IS_DEBUG_BUILD
					IF bGotoLocPrints
						PRINTLN("[RCC MISSION] DISPLAY_LOCATION_HUD ",iloc," - Unable to set iCurrentLoc as iObjectiveTypeCompleted = ",MC_playerBD[iPartToUse].iObjectiveTypeCompleted = -1)
					ENDIF
					#ENDIF
					
					// but we still want to draw aerial markers! So if we find one, still draw it. So long as it isn't the one we just flew through
					IF ENUM_TO_INT( sFadeOutLocations[ iLoc ].eState ) < ENUM_TO_INT( eLocateFadeOutState_AFTER_FADE_OUT )
					AND ENUM_TO_INT( sFadeOutLocations[ iLoc ].eState ) > ENUM_TO_INT( eLocateFadeOutState_BEFORE_FADE_IN )
						DRAW_ONE_LOCATION_MARKER( iLoc, sFadeOutLocations[ iLoc ].iAlphaValue )
					ENDIF
				ENDIF
			#IF IS_DEBUG_BUILD
			ELIF bGotoLocPrints
				PRINTLN("[RCC MISSION] DISPLAY_LOCATION_HUD ",iloc," - Location is not a priority, priority for my team (",iTeam,") = ",MC_serverBD_4.iGotoLocationDataPriority[iloc][iTeam])
			#ENDIF
			ENDIF
		ENDIF
	ELIF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitset, PBBOOL_HEIST_SPECTATOR)
		
		#IF IS_DEBUG_BUILD
		IF bGotoLocPrints
			PRINTLN("[RCC MISSION] DISPLAY_LOCATION_HUD ",iloc," - Player is a heist spectator")
		ENDIF
		#ENDIF
		
		IF bLocalPlayerPedOk
			IF MC_playerBD[iLocalPart].iObjectiveTypeCompleted = -1
			AND NOT IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS2, ciLoc_BS2_DisplayMarkerAsVisualOnly )
				IF NOT IS_VECTOR_ZERO(GET_LOCATION_VECTOR(iloc))
					IF IS_PED_IN_LOCATION(LocalPlayerPed, iloc,  MC_playerBD[iLocalPart].iteam )
					OR SHOULD_MARK_THIS_GOTO_AS_DONE_FOR_APARTMENT_DROPOFF()
						
						IF IS_LOCATION_AN_OBJECTIVE_FOR_ANY_TEAM(iloc)
							iInAnyLocTemp = iloc
							IF SHOULD_COUNT_LEAVE_LOCATION(iloc)
								MC_playerBD[iLocalPart].iLeaveLoc = iloc
							ENDIF
						ENDIF
						
						IF MC_serverBD_4.iGotoLocationDataPriority[iloc][MC_playerBD[iLocalPart].iteam] <= iPriority
						AND MC_serverBD_4.iGotoLocationDataPriority[iloc][MC_playerBD[iLocalPart].iteam] < FMMC_MAX_RULES
							IF MC_playerBD[iLocalPart].iCurrentLoc != iloc
								MC_playerBD[iLocalPart].iCurrentLoc = iloc
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			// Broadcast that we just left a locate.
			IF MC_playerBD[iLocalPart].iCurrentLoc > -1
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetSixteen, ciDISABLE_RECPATURE_OF_LOCATES_WHEN_LEAVING_ONE)							
					IF NOT HAS_NET_TIMER_STARTED(tdDisableLocateRecaptureTimer[iLocalPart])
						iLocateFromPreviousFrame = -1
						BROADCAST_FMMC_LEFT_LOCATE_DELAY_RECAPTURE(iLocalPart, MC_playerBD[iLocalPart].iCurrentLoc)
					ENDIF
				ENDIF
			ENDIF
			
			MC_playerBD[iLocalPart].iCurrentLoc = -1
			MC_playerBD[iLocalPart].iLeaveLoc = -1
			
		ENDIF
	ENDIF
	
	IF iSpectatorTarget != -1
		IF NOT g_bMissionEnding
			IF  iPriority < FMMC_MAX_RULES
			AND iPriority != -1
				#IF IS_DEBUG_BUILD
					IF (GET_FRAME_COUNT() % 10) = 0
						PRINTLN("[RCC MISSION] DISPLAY_LOCATION_HUD ",iloc," - Player is a spectator")
					ENDIF
				#ENDIF
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciSET_LINES_AS_RUGBY_LINES)
					IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS, ciLoc_BS_HideBlip)
						IF MC_serverBD_4.iGotoLocationDataPriority[iloc][ iTeam ] <= iPriority
							IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iArial[0] = 1
								DRAW_AIR_MARKER(GET_LOCATION_VECTOR(iloc),FALSE,iloc)
							ELSE
							//	DRAW_LOCATE_MARKER(GET_LOCATION_VECTOR(iloc),g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fRadius[0],FALSE,iloc)
								IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iSpectatorTarget))
									PED_INDEX pedSpecPlayer = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iSpectatorTarget)))
									IF IS_PED_IN_LOCATION(pedSpecPlayer, iloc,  iTeam )
										PRINTLN("[RCC MISSION] DISPLAY_LOCATION_HUD I'm spectator, spectating player", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iSpectatorTarget))), " iLoc = ",iloc," Fading out locate radius = ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fRadius[0])
										SET_LOCATE_TO_FADE_OUT( sFadeOutLocations[ iLoc ], iLoc, iTeam )
									ELSE
										SET_LOCATE_TO_DISPLAY(sFadeOutLocations[ iLoc ], iLoc, iTeam )
										
										IF MC_serverBD_4.iGotoLocationDataPriority[ iloc ][ iTeam ] = iPriority
											IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_DrawNextCP) 
												
												INT iLocToDraw = FMMC_MAX_GO_TO_LOCATIONS + 1
												
												INT iNextPriority = GET_OBJECTIVE_TO_JUMP_TO_FROM_CREATOR_DATA(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iJumpToObjectivePass[iTeam])
												
												IF iNextPriority = -1
												AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRandomNextRuleBS[iPriority] != 0
													iNextPriority = GET_FIRST_SET_BIT_IN_BITSET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRandomNextRuleBS[iPriority])
												ENDIF
												
												IF iNextPriority = -1
													iNextPriority = iPriority + 1
												ENDIF
												
												FOR i = 0 TO FMMC_MAX_GO_TO_LOCATIONS - 1
													IF MC_serverBD_4.iGotoLocationDataPriority[ i ][ iTeam ] = iNextPriority
													AND MC_serverBD_4.iGotoLocationDataPriority[ i ][ iTeam ] != FMMC_PRIORITY_IGNORE
														#IF IS_DEBUG_BUILD
														IF bDebugBlipPrints
															PRINTLN("[MMacK][NextCpBlip] next blip found")
														ENDIF #ENDIF
														iLocToDraw = i
														BREAKLOOP
													ENDIF
												ENDFOR
												
												IF (iLocToDraw > FMMC_MAX_GO_TO_LOCATIONS)
													IF MC_ServerBD_4.iTeamLapsCompleted[iTeam] >= 0
													AND MC_ServerBD_4.iTeamLapsCompleted[iTeam] < g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMissionLapLimit
													AND MC_ServerBD_4.iTeamLapsCompleted[iTeam] != (g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMissionLapLimit-1)
														//we have laps, so lets show the first checkpoint marker
														//also, lets not do this on the last lap either.
														#IF IS_DEBUG_BUILD
														IF bDebugBlipPrints
															PRINTLN("[MMacK][NextCpBlip] Laps!")
														ENDIF #ENDIF
														INT iLowestRule = 99999
														FOR i = 0 TO FMMC_MAX_GO_TO_LOCATIONS - 1
															IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iPriority[iTeam] < iLowestRule // Want to check the priority in the globals rather than that on the server, as we've already completed these rules
															AND g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iPriority[iTeam] >= iNextPriority
																iLowestRule = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iPriority[iTeam]
																iLocToDraw = i
															ENDIF
														ENDFOR
													ENDIF
												ENDIF
												
												#IF IS_DEBUG_BUILD
												IF bDebugBlipPrints
													PRINTLN("[MMacK][NextCpBlip] iLocToDraw = ", iLocToDraw)
												ENDIF #ENDIF
												
												IF (iLocToDraw < FMMC_MAX_GO_TO_LOCATIONS) 
													IF NOT IS_VECTOR_ZERO(GET_LOCATION_VECTOR(iLocToDraw))
													AND NOT DOES_BLIP_EXIST(LocBlip1[iLocToDraw])
													AND IS_PED_IN_REQUIRED_LOCATION_VEHICLE(pedSpecPlayer, iTeam, iLocToDraw)	
														#IF IS_DEBUG_BUILD
														IF bDebugBlipPrints
															PRINTLN("[MMacK][NextCpBlip] Added Blip")
														ENDIF #ENDIF
														LocBlip1[iLocToDraw] = ADD_BLIP_FOR_COORD(GET_LOCATION_VECTOR(iLocToDraw))
														SET_BLIP_COLOUR(LocBlip1[iLocToDraw], BLIP_COLOUR_YELLOW)
														SET_BLIP_SCALE(LocBlip1[iLocToDraw], BLIP_SIZE_OUT_OF_TIME_SLOT)
														SET_BLIP_NAME_FROM_TEXT_FILE(LocBlip1[iLocToDraw], "FMMC_SELC" )
													ENDIF
													
													//If we have a new loc to draw
													IF iPreviousLocToDraw >= 0
													AND iLocToDraw != iPreviousLocToDraw
														//Check the new blip exists so that the blip doesn't disappear briefly
														//IF DOES_BLIP_EXIST(LocBlip[iLocToDraw])
															//If the small blip exists, remove it
															IF DOES_BLIP_EXIST(LocBlip1[iPreviousLocToDraw])
																#IF IS_DEBUG_BUILD
																IF bDebugBlipPrints
																	PRINTLN("[KH][NextCpBlip] Removed previous blip - iPreviousLocToDraw")
																ENDIF #ENDIF
																REMOVE_BLIP(LocBlip1[iPreviousLocToDraw])
																iPreviousLocToDraw = iLocToDraw
															ENDIF
															iPreviousLocToDraw = iLocToDraw
														//ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									INT iTeamWhoControlsLoc = -1
									IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iPriority], ciBS_RULE9_TEAM_COLOURED_LOCATES)
									OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_ZONE(g_FMMC_STRUCT.iRootContentIDHash)
										iTeamWhoControlsLoc = MC_serverBD.iLocOwner[iloc]
										
										// It doesn't matter if this sets a team twice, because we override that with the contested colour
										IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_INSTANT_COLOUR_CHANGES_FOR_INSIDE_LOCATE)
											FOR i = 0 TO FMMC_MAX_TEAMS-1
												IF MC_serverBD_1.inumberOfTeamInArea[iLoc][i] > 0												
													iTeamWhoControlsLoc = i
												ENDIF
											ENDFOR
										ENDIF
									ENDIF
									DRAW_ONE_LOCATION_MARKER( iLoc, sFadeOutLocations[ iLoc ].iAlphaValue, DEFAULT, iTeamWhoControlsLoc )
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(tdcontolHUDFlash)
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdcontolHUDFlash) > 500
			RESET_NET_TIMER(tdcontolHUDFlash)
		ELSE
			icontolHUDFlash = 500
		ENDIF
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
		FOR i = 0 TO FMMC_MAX_TEAMS -1
			iHostileTakeoverInZoneCount[iLoc][i] = 0
		ENDFOR
		INT iParticipantLoop
		FOR iParticipantLoop = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipantLoop))
			AND NOT IS_PED_INJURED(GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipantLoop))))
				IF MC_playerBD[iParticipantLoop].iCurrentLoc != -1
				AND MC_playerBD[iParticipantLoop].iCurrentLoc = iLoc
					iHostileTakeoverInZoneCount[iLoc][MC_playerBD[iParticipantLoop].iteam]++
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	//Running back team details
	
	//0 - Defence
	//1 - Offence
	//2 - Runner
	IF MC_serverBD_4.iCurrentHighestPriority[ MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ MC_playerBD[iPartToUse].iteam ].sHideLocationMarkerStruct[ MC_serverBD_4.iCurrentHighestPriority[ MC_playerBD[iPartToUse].iteam] ].iHideBS[ iloc ], ciBS_HIDE_LOCATION_MARKER_STRUCT)
			IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].iLocBS2, ciLoc_BS2_DisplayMarkerAsVisualOnly )
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciSET_LINES_AS_RUGBY_LINES)
					PRINTLN("[RCC MISSION][LOCATES] iLoc: ",iLoc, " != -1 " ) 
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].iLocBS2, ciLoc_BS2_RunningBackMarker)
						//If you are the defender you want to see the line to be Blue
						IF iTeam = 0
							PRINTLN("[RCC MISSION][LOCATES] iLoc: ",iLoc, " THIS_LOCATION_BELONGS_TO_TEAM(iLoc) = iTeam " ) 
							DRAW_ONE_LOCATION_MARKER( iLoc, sFadeOutLocations[ iLoc ].iAlphaValue,TRUE)
							CREATE_BLIP_FOR_ENEMY_MARKER_LINES(iLoc )
						ELSE
							//Otherwise draw the line as being red
							
							PRINTLN("[RCC MISSION][LOCATES] iLoc: ",iLoc, " THIS_LOCATION_BELONGS_TO_TEAM(iLoc) != iTeam " ) 
							DRAW_ONE_LOCATION_MARKER( iLoc, sFadeOutLocations[ iLoc ].iAlphaValue)
							//The Offence team need to see the blip for the line so draw it but in red
							IF iTeam = 1
								CREATE_BLIP_FOR_ENEMY_MARKER_LINES(iLoc,TRUE)
							ELSE
							ENDIF
						ENDIF
					ELSE
						IF THIS_LOCATION_BELONGS_TO_TEAM(iLoc) != iTeam
							//IF MC_serverBD_4.iGotoLocationDataPriority[iloc][iteam] < FMMC_MAX_RULES
							PRINTLN("[RCC MISSION][LOCATES] iLoc: ",iLoc, " THIS_LOCATION_BELONGS_TO_TEAM(iLoc) = iTeam " ) 
							DRAW_ONE_LOCATION_MARKER( iLoc, sFadeOutLocations[ iLoc ].iAlphaValue,TRUE)
							CREATE_BLIP_FOR_ENEMY_MARKER_LINES(iLoc,TRUE)
							//ENDIF
						ELSE
							PRINTLN("[RCC MISSION][LOCATES] iLoc: ",iLoc, " THIS_LOCATION_BELONGS_TO_TEAM(iLoc) = iTeam " ) 
							DRAW_ONE_LOCATION_MARKER( iLoc, sFadeOutLocations[ iLoc ].iAlphaValue)
							CREATE_BLIP_FOR_ENEMY_MARKER_LINES(iLoc)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciSET_LINES_AS_RUGBY_LINES)
					IF sFadeOutLocations[ iLoc ].eState = eLocateFadeOutState_FADING_OUT
						DRAW_ONE_LOCATION_MARKER( iLoc, sFadeOutLocations[ iLoc ].iAlphaValue )
					ELSE
						BOOL bIsThisALapsBasedMission = (g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iMissionLapLimit > 0)
						CHECK_FADED_OUT_CHECKPOINT(iloc, iTeam, iPriority, bIsThisALapsBasedMission) 
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF iPriority < FMMC_MAX_RULES
	AND NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_FINISHED)
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitset[ iPriority ], ciBS_RULE_HIDE_HUD)
			IF MC_playerBD[iPartToUse].iCurrentLoc = iloc
			OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetFour[ iPriority ], ciBS_RULE4_SHOW_CAPTURE_BARS_AT_ALL_TIMES)
				IF MC_serverBD_4.iGotoLocationDataRule[iloc][ iTeam ] = FMMC_OBJECTIVE_LOGIC_CAPTURE
					FOR i = 0 TO FMMC_MAX_TEAMS -1
						IF MC_serverBD_4.iGotoLocationDataPriority[iloc][i] < FMMC_MAX_RULES
							IF HAS_NET_TIMER_STARTED(MC_serverBD.tdAreaTimer[iloc][i])
								IF MC_serverBD_4.iGotoLocationDataPriority[iloc][i] = MC_serverBD_4.iCurrentHighestPriority[i]
									IF bPlayerToUseOK
										IF NOT IS_SPECTATOR_HUD_HIDDEN()
										AND NOT IS_SPECTATOR_SHOWING_NEWS_HUD()
										AND NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_OBJECTIVE_BLOCKER)
										AND NOT IS_STRING_NULL_OR_EMPTY(g_sMission_TeamName[ i ])
										
											IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].iLocBS2, ciLoc_BS2_ScanVFXOnCapture)
												IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxCapScanEffect)													
													EXIT													
												ENDIF
											ENDIF
										
											g_b_ChangePlayerNameToTeamName = TRUE
											INT iCurrentCapTarget = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdAreaTimer[iloc][i])
											INT iMaxCapTime = 2*MC_serverBD.iGotoLocationDataTakeoverTime[i][MC_serverBD_4.iGotoLocationDataPriority[iloc][i]]
											FLOAT iCurrentDifference = iCurrentCapTarget - iCurrCapTime[iloc][i]
											
											IF IS_AREA_ABLE_TO_BE_CAPTURED(iteam, iLoc, (MC_serverBD.iLocOwner[iloc] != -1))
												IF iCurrentDifference > 0
												OR iCurrCapTime[iloc][i] = -1
													FLOAT fProportionDiff = iCurrentDifference / TO_FLOAT(iMaxCapTime)
													
													FLOAT fMaxBarFillRate = 0.5 * fLastFrameTime // This should be the fastest rate at which any bar will fill (a 2 second capture time)
													
													IF iCurrCapTime[iloc][i] != -1
													AND fProportionDiff > fMaxBarFillRate
														FLOAT fChange = iCurrentDifference
														
														FLOAT fMaxFillForThisBar = (fMaxBarFillRate + 0.02) * iMaxCapTime
														
														PRINTLN("[[AW SMOKE]] DISPLAY_LOCATION_HUD - iCurrCapTime for loc ",iloc," = ", iCurrCapTime[iloc][i])
														PRINTLN("[[AW SMOKE]] DISPLAY_LOCATION_HUD - iCurrentCapTarget: ", iCurrentCapTarget)
														PRINTLN("[[AW SMOKE]] DISPLAY_LOCATION_HUD - iCurrentDifference: ", iCurrentDifference)
														PRINTLN("[[AW SMOKE]] DISPLAY_LOCATION_HUD - iMaxCapTime: ", iMaxCapTime)
														PRINTLN("[[AW SMOKE]] DISPLAY_LOCATION_HUD - fLastFrameTime ", fLastFrameTime)
														PRINTLN("[[AW SMOKE]] DISPLAY_LOCATION_HUD - fChange ",fChange,", fMaxFillForThisBar ",fMaxFillForThisBar)
														
														fChange = FMIN(fChange, fMaxFillForThisBar)
														
														iCurrCapTime[iloc][i] += fChange
														iPrevCapTime[iloc][i] = iCurrCapTime[iloc][i]
														PRINTLN("[[AW SMOKE]] DISPLAY_LOCATION_HUD - Newly interped iCurrCapTime[",i,"] = ",iCurrCapTime[iloc][i]," thanks to fChange of ",fChange)
														
													ELSE
														PRINTLN("[[AW SMOKE]] DISPLAY_LOCATION_HUD - Just set iCurrCapTime[",i,"] equal to iCurrentCapTarget for loc ",iloc," of ",iCurrentCapTarget)
														iCurrCapTime[iloc][i] = TO_FLOAT(iCurrentCapTarget)
														iPrevCapTime[iloc][i] = TO_FLOAT(iCurrentCapTarget)
													ENDIF
												ELSE
													PRINTLN("[[AW SMOKE]] DISPLAY_LOCATION_HUD - No time difference for loc ",iloc,"?")
												ENDIF
											ELSE
												iCurrCapTime[iloc][i] = iPrevCapTime[iloc][i]
											ENDIF
											
											INT iTimeNumber = iMaxCapTime
											
											IF iMaxCapTime = 0
												PRINTLN("[[AW SMOKE]]iMaxCapTime is zero - setting to default value 240000")
												//Setting iMaxCapTime to the default value if it's zero - to fix 2601090
												iTimeNumber = 240000
											ENDIF
											
											BOOL bShowCaptureBar = TRUE
											
											IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_NEVER_SHOW_BOTTOM_RIGHT_CAPTURE_BARS)
												bShowCaptureBar = FALSE
											ENDIF
											
											IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciONLY_SHOW_CAPTURE_BARS_IN_AREA)
												IF (NOT HAS_NET_TIMER_STARTED(MC_serverBD.tdAreaTimer[iloc][i])
												AND MC_serverBD.iLocOwner[iloc] != iTeam)
												OR MC_serverBD.iLocOwner[iloc] = iTeam
													bShowCaptureBar = FALSE
												ENDIF
											ENDIF
											
											IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
												IF iTeam != i
													bShowCaptureBar = FALSE
												ENDIF
											ENDIF
											
											IF bShowCaptureBar
												IF iTeam = i
												AND NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_VFX_CAPTURE_OWNER)
													tlCaptureBarText = g_sMission_TeamName[ iTeam ]
													
													IF (GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION() OR IS_FAKE_MULTIPLAYER_MODE_SET())
													AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63ObjectiveEntityHUDMessage[iPriority])
														tlCaptureBarText = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63ObjectiveEntityHUDMessage[iPriority]
													ENDIF
													PRINTLN("[JR][STROM] Drawing capture meter")
													DRAW_GENERIC_METER(ROUND(iCurrCapTime[iloc][i]),iMaxCapTime,tlCaptureBarText,GET_HUD_COLOUR_FOR_FMMC_TEAM(i, PlayerToUse),-1,HUDORDER_DONTCARE,-1,-1,FALSE,TRUE,HUDFLASHING_FLASHWHITE,icontolHUDFlash, DEFAULT, DEFAULT, TRUE)
												ELSE
													IF iTeam = i
														DRAW_GENERIC_METER(ROUND(iCurrCapTime[iloc][i]),iTimeNumber,g_sMission_TeamName[ i ],GET_HUD_COLOUR_FOR_FMMC_TEAM(i, PlayerToUse),-1,HUDORDER_SEVENTHBOTTOM,-1,-1,FALSE,TRUE,HUDFLASHING_FLASHWHITE,icontolHUDFlash, DEFAULT, DEFAULT, TRUE)
													ELSE
														DRAW_GENERIC_METER(ROUND(iCurrCapTime[iloc][i]),iTimeNumber,g_sMission_TeamName[ i ],GET_HUD_COLOUR_FOR_FMMC_TEAM(i, PlayerToUse),-1,INT_TO_ENUM(HUDORDER, ENUM_TO_INT(HUDORDER_SIXTHBOTTOM) - i),-1,-1,FALSE,TRUE,HUDFLASHING_FLASHWHITE,icontolHUDFlash, DEFAULT, DEFAULT, TRUE)
													ENDIF
												ENDIF
											ENDIF
											
											//Calculate the percentage of the capture bar for other modes
											FLOAT fMaxCapTime = TO_FLOAT(iMaxCapTime)
											FLOAT fCurrentCapTime = iCurrCapTime[iloc][i]
											FLOAT fCaptureBarPercentage = (fCurrentCapTime/fMaxCapTime)*100
											MC_ServerBD.iCaptureBarPercentage[i] = ROUND(fCaptureBarPercentage)
										
										ENDIF
									ENDIF
									//Turn on the red filter if this is the active rule
									IF MC_playerBD[iPartToUse].iCurrentLoc = iloc
										IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetFour[ iPriority ], ciBS_RULE4_SHOW_CAPTURE_BARS_AT_ALL_TIMES)
										AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_NEVER_SHOW_BOTTOM_RIGHT_CAPTURE_BARS)
											
											IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFive[iPriority], ciBS_RULE5_RED_FILTER_IN_CAP_ZONE)
											AND NOT IS_BIT_SET( MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER )
												IF SHOULD_RED_FILTER_SHOW(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], iloc)
													IF NOT ANIMPOSTFX_IS_RUNNING("CrossLine")
														IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_RED_FILTER_SOUND_PLAYED)
															PLAY_RED_FILTER_SOUND()
														ENDIF
														ANIMPOSTFX_PLAY("CrossLine", 0, TRUE)
													ENDIF
												ENDIF
											ENDIF
										ENDIF
										//[JS]Enable timer for LBD
										//Spectator check?
										IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciLBD_SHOW_TIME_COLUMN)
										AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
											fTempCaptureTime += fLastFrameTime
											IF fTempCaptureTime > 1.0
												MC_playerBD[iLocalPart].fCaptureTime += fTempCaptureTime
												fTempCaptureTime = 0
											ENDIF
										ENDIF
									ENDIF
								ELSE
									//Turn off the red filter if it is turned on and we are no longer on the correct rule
									IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFive[iPriority], ciBS_RULE5_RED_FILTER_IN_CAP_ZONE)
										IF ANIMPOSTFX_IS_RUNNING("CrossLine")
											ANIMPOSTFX_STOP("CrossLine")
											STRING sSoundSet 
											sSoundSet = "DLC_Apartments_Drop_Zone_Sounds"
											IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_HOSTILE_TAKEOVER_SOUNDS)
												sSoundSet = "dlc_xm_hota_Sounds"
											ENDIF
											PLAY_SOUND_FRONTEND(-1,"Exit_Capture_Zone",sSoundSet,FALSE)
											ANIMPOSTFX_PLAY("CrossLineOut", 0, FALSE)
											CLEAR_BIT(iLocalBoolCheck27, LBOOL27_RED_FILTER_SOUND_PLAYED)
										ENDIF 
									ENDIF
									//[JS]Pause timer for LBD
									IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciLBD_SHOW_TIME_COLUMN)
									AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
										IF fTempCaptureTime > 0
											MC_playerBD[iLocalPart].fCaptureTime += fTempCaptureTime
											fTempCaptureTime = 0
										ENDIF
									ENDIF
								ENDIF
							ELSE
								//iPrevCapTime[iloc][i] = 0
								iCurrCapTime[iloc][i] = 0
								IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetFour[ iPriority ], ciBS_RULE4_SHOW_CAPTURE_BARS_AT_ALL_TIMES)
								AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_NEVER_SHOW_BOTTOM_RIGHT_CAPTURE_BARS) 
									IF MC_serverBD_4.iGotoLocationDataPriority[iloc][i] = MC_serverBD_4.iCurrentHighestPriority[i]
										IF bPlayerToUseOK
											IF NOT IS_SPECTATOR_HUD_HIDDEN()
											AND NOT IS_SPECTATOR_SHOWING_NEWS_HUD()
											AND NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_OBJECTIVE_BLOCKER)
											AND NOT IS_STRING_NULL_OR_EMPTY(g_sMission_TeamName[ i ])
												g_b_ChangePlayerNameToTeamName = TRUE
												IF iTeam = i
													PRINTLN("[[AW]][DRAW ON BOTTOM] - 1 - iTeam: ",iTeam, " i: ", i )
													
													tlCaptureBarText = g_sMission_TeamName[ iTeam ]
													
													IF (GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION() OR IS_FAKE_MULTIPLAYER_MODE_SET())
													AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63ObjectiveEntityHUDMessage[iPriority])
														tlCaptureBarText = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63ObjectiveEntityHUDMessage[iPriority]
													ENDIF
													
													//2593820 - Always have your teams bar on top in drop zone and opponent bars below that.
													DRAW_GENERIC_METER(0,(2*MC_serverBD.iGotoLocationDataTakeoverTime[i][MC_serverBD_4.iGotoLocationDataPriority[iloc][i]]) ,tlCaptureBarText,GET_HUD_COLOUR_FOR_FMMC_TEAM(i, PlayerToUse),-1,HUDORDER_SEVENTHBOTTOM,-1,-1,TRUE,TRUE,HUDFLASHING_FLASHWHITE,icontolHUDFlash)
												ELSE
													PRINTLN("[[AW]][DRAW ON BOTTOM] - 2 - iTeam: ",iTeam, " i: ", i )
													DRAW_GENERIC_METER(0,(2*MC_serverBD.iGotoLocationDataTakeoverTime[i][MC_serverBD_4.iGotoLocationDataPriority[iloc][i]]) ,g_sMission_TeamName[ i ],GET_HUD_COLOUR_FOR_FMMC_TEAM(i, PlayerToUse),-1,INT_TO_ENUM(HUDORDER, ENUM_TO_INT(HUDORDER_SIXTHBOTTOM) - i),-1,-1,TRUE,TRUE,HUDFLASHING_FLASHWHITE,icontolHUDFlash)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								// url:bugstar:3887146 || Fixed for Drop Zone - Should this not have a bitset set/check or a HAS_NET_TIMER_EXPIRED?
								IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
									IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFive[iPriority], ciBS_RULE5_RED_FILTER_IN_CAP_ZONE)
									AND NOT IS_BIT_SET( MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER )
										IF SHOULD_RED_FILTER_SHOW(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], iloc)
											IF NOT ANIMPOSTFX_IS_RUNNING("CrossLine")
												IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_RED_FILTER_SOUND_PLAYED)
													PLAY_RED_FILTER_SOUND()
												ENDIF
												ANIMPOSTFX_PLAY("CrossLine", 0, TRUE)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset2, ciBS2_STOP_GO_HUD)
		IF MC_playerBD[iPartToUse].iCurrentLoc != -1
			IF HAS_NET_TIMER_STARTED(stStopHUD)
				IF HAS_NET_TIMER_EXPIRED(stStopHUD,200)
				AND (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH( "appinternet" ) ) = 0 )
					SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
					DRAW_SCALEFORM_MOVIE_FULLSCREEN(cduiIntro.uiCountdown, 255, 255, 255, 100)
					
					INT iR, iG, iB, iA
					GET_HUD_COLOUR(HUD_COLOUR_RED, iR, iG, iB, iA)
								
					BEGIN_SCALEFORM_MOVIE_METHOD(cduiIntro.uiCountdown, "SET_MESSAGE")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("STOP_DASH")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iR)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iG)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iB)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)  
					END_SCALEFORM_MOVIE_METHOD()
				ENDIF
			ELSE
				START_NET_TIMER(stStopHUD)
			ENDIF
		ELSE
			IF iPriority > 0
				IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME+iTeam)
					START_NET_TIMER(stStopGoHUD)
					PRINTLN("[MMacK][StopGo] Started go timer")
				ELSE
					IF HAS_NET_TIMER_STARTED(stStopGoHUD)
						IF HAS_NET_TIMER_EXPIRED(stStopGoHUD,3000)
							PRINTLN("[MMacK][StopGo] Killed go timer")
							RESET_NET_TIMER(stStopGoHUD)
							RESET_NET_TIMER(stStopHUD)
						ELSE
							SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
							DRAW_SCALEFORM_MOVIE_FULLSCREEN(cduiIntro.uiCountdown, 255, 255, 255, 100)
							
							INT iR, iG, iB, iA
							GET_HUD_COLOUR(HUD_COLOUR_GREEN, iR, iG, iB, iA)
							
							BEGIN_SCALEFORM_MOVIE_METHOD(cduiIntro.uiCountdown, "SET_MESSAGE")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CNTDWN_GO")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iR)
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iG)
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iB)
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
							END_SCALEFORM_MOVIE_METHOD()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	icontolHUDFlash = 0
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_CrossLineDistUseMe_T0)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_CrossLineDistUseMe_T1)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_CrossLineDistUseMe_T2)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_CrossLineDistUseMe_T3)
		IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_FINISHED)
		AND IS_TEAM_ACTIVE(MC_playerBD[iPartToUse].iTeam)
		AND iPriority < FMMC_MAX_RULES
			SET_BIT(iLocalBoolCheck13, LBOOL13_DRAW_DISTANCE_TO_CROSS_LINE)
			
			CALCULATE_DISTANCES_TO_LOCATE(iloc)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS2, ciLoc_BS2_AppearAsBlue)
		IF DOES_BLIP_EXIST(LocBlip[iloc])
			INT iR, iG, iB, iA
			GET_HUD_COLOUR(HUD_COLOUR_BLUE, iR, iG, iB, iA)
			APPLY_SECONDARY_COLOUR_TO_BLIP(LocBlip[iloc], iR, iG, iB)
			PRINTLN("[url:bugstar:3342736] BLIP_CURRENT_CHECKPOINT, APPLY_SECONDARY_COLOUR_TO_BLIP, iloc =  ", iloc)
		ENDIF
	ENDIF
	
	IF  iPriority < FMMC_MAX_RULES
	AND iPriority != -1
	AND MC_serverBD_4.iGotoLocationDataPriority[ iloc ][ iTeam ] = iPriority
		DRAW_LOCATE_RADIUS_BLIP(iPriority, iLoc)
		IF IS_BIT_SET(MC_serverbd.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
			IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fTeamMembersNeededWithin > 0
				IF NOT ARE_ALL_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE(MC_PlayerBD[iPartToUse].iTeam)
					INT iPart
					INT iPlayersChecked
					PARTICIPANT_INDEX tempPart
					FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
						IF iPart != iPartToUse
							IF MC_playerBD[iPart].iteam = MC_playerBD[iPartToUse].iteam
								tempPart = INT_TO_PARTICIPANTINDEX(iPart)
								IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
									IF (NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_ANY_SPECTATOR)
										OR IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_HEIST_SPECTATOR))
									AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_PLAYER_FAIL)
										IF IS_BIT_SET(MC_playerBD[iPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
											IF NOT IS_BIT_SET( MC_playerBD[iPart].iClientBitSet, PBBOOL_FINISHED )
												PRINTLN("[JS] [TDFMARKER] - drawing over part: ", iPart)
												GB_DRAW_CRITICAL_PLAYER_MARKER(NETWORK_GET_PLAYER_INDEX(tempPart), DEFAULT, TRUE)
												iPlayersChecked++
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							iPlayersChecked++
						ENDIF
						IF iPlayersChecked >= MC_serverBD.iNumberOfPlayingPlayers[MC_playerBD[iPartToUse].iteam]
							BREAKLOOP
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
		ENDIF
	ELSE

//		PRINTLN("[Panic][BIKERBUG] iPriority = ", iPriority)
//		PRINTLN("[Panic][BIKERBUG] iloc = ", iloc)
//		PRINTLN("[Panic][BIKERBUG] iTeam = ", iTeam)
//		PRINTLN("[Panic][BIKERBUG] MC_serverBD_4.iGotoLocationDataPriority[ iloc ][ iTeam ] = ", MC_serverBD_4.iGotoLocationDataPriority[ iloc ][ iTeam ])
	ENDIF

	IF iLoc < FMMC_MAX_RULES
		IF IS_BIT_SET(bsCheckPointCreated,iLoc)
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_AngleCheckpoint)
				
				IF NOT IS_BIT_SET(bsRepositionCheckpoint,iLoc)
					IF MAINTAIN_CLIP_CHECK_POINTS(ciLocCheckpoint[iLoc],GET_LOCATION_VECTOR(iLoc),stiCheckpoint[iLoc])
						PRINTLN("[RCC MISSION] DRAW_RACE_STYLE_MARKER | ANGLED CHECKPOINT DONE : SETTING RGBA ")
						
						INT iR, iG, iB, iA
						
						IF DOES_BLIP_EXIST(LocBlip[iLoc])
							IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fTeamMembersNeededWithin > 0
								IF ARE_ALL_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE(iTeam)
									GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
								ELSE
									GET_HUD_COLOUR(HUD_COLOUR_RED, iR, iG, iB, iA)
								ENDIF
							ENDIF
						ENDIF
						
						SET_CHECKPOINT_RGBA(ciLocCheckpoint[iLoc],iR, iG, iB, sFadeOutLocations[iLoc].iAlphaValue)
						SET_BIT(bsRepositionCheckpoint,iLoc)
						CLEAR_BIT(bsCheckPointCreated,iLoc)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF iLoc < FMMC_MAX_RULES

		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iLoc], ciBS_RULE4_WRONG_WAY_CHECK)
			PRINTLN("[MMacK][WrongWay] bit is set")
			IF SHOULD_WRONG_WAY_BE_DISPLAYED(iLoc)
				PRINTLN("[MMacK][WrongWay] should display")
				DISPLAY_WRONG_WAY_SHARD(MC_playerBD[iPartToUse].iTeam, iPriority)
			ENDIF
		ENDIF
	
	
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciBIRDS_EYE_ENABLED)
			
		ENDIF
	
	ENDIF
	
ENDPROC

PROC HIDE_LOCATES(INT iLoc)
	SET_CHECKPOINT_RGBA(ciLocCheckpoint[iloc], 0, 0, 0, 0)
	DELETE_CHECKPOINT(ciLocCheckpoint[iloc])
	PRINTLN("[RCC MISSION] hiding locates")
ENDPROC

FUNC BOOL SHOULD_PROCESS_RUNNING_BACK_SUDDEN_DEATH_LOCATIONS()	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUNNING_BACK_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
	AND HAS_NET_TIMER_STARTED(tdRBSuddenDeathActivation)
	AND HAS_NET_TIMER_EXPIRED(tdRBSuddenDeathActivation, 12500)
	OR IS_BIT_SET(iLocalBoolCheck29, LBOOL29_READY_TO_PROCESS_GOTO_LOCATIONS)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_SHOW_LOCATIONS_HUD()
	IF NOT g_bMissionOver
		IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMissionLapLimit > 0 
		AND NOT (IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_SUDDEN_DEATH_OVERTIME_RUMBLE)
		AND IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH))
		AND NOT (g_FMMC_STRUCT.iSuddenDeathStartRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] AND IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUNNING_BACK_REMIX(g_FMMC_STRUCT.iAdversaryModeType))
		AND NOT (MC_ServerBD_4.iTeamLapsCompleted[MC_playerBD[iPartToUse].iteam] >= g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMissionLapLimit AND IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUNNING_BACK_REMIX(g_FMMC_STRUCT.iAdversaryModeType))
			IF MC_ServerBD_4.iTeamLapsCompleted[MC_playerBD[iPartToUse].iteam] >= g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMissionLapLimit
				PRINTLN("SHOULD_SHOW_LOCATIONS_HUD - FALSE")
				RETURN FALSE
			ELSE
				RETURN TRUE
			ENDIF
		ELSE
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUNNING_BACK_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
				RETURN SHOULD_PROCESS_RUNNING_BACK_SUDDEN_DEATH_LOCATIONS()
			ENDIF
				
			RETURN TRUE
		ENDIF
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

PROC PROCESS_SYNCED_CAPTURE_SCORING(INT iLoc)
	IF NOT g_bMissionEnding
	AND (NOT IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH) AND (MC_playerBD[iLocalPart].iteam != -1 AND (GET_LOCAL_TEAM_PERSONAL_LOCATE_TOTAL() != MC_serverBD.iTeamScore[MC_playerBD[iLocalPart].iteam])))
		IF HAS_NET_TIMER_STARTED(MC_serverBD.tdCapturePointsTimer)
			IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][0] < FMMC_MAX_RULES
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdCapturePointsTimer) > MC_serverBD.iGotoLocationDataTakeoverTime[0][MC_serverBD_4.iGotoLocationDataPriority[iLoc][0]]

					IF bIsLocalPlayerHost
						IF MC_serverBD.iLocOwner[iLoc] != -1
							INT iTeam
							FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
								IF MC_serverBD.iLocOwner[iLoc] = iteam
								AND MC_serverBD_4.iGotoLocationDataPriority[iLoc][iteam] < FMMC_MAX_RULES
									iNumberLocsChecked++
									PRINTLN("PROCESS_SYNCED_CAPTURE_SCORING - Incrementing for captured location: ", iLoc)
									IF MC_serverBD.iTeamScore[iTeam] < g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMissionTargetScore
									AND NOT IS_ZONE_BEING_CONTESTED(iLoc, FALSE)
										INCREMENT_SERVER_TEAM_SCORE(iteam, MC_serverBD_4.iGotoLocationDataPriority[iLoc][iteam], GET_FMMC_POINTS_FOR_TEAM(iteam,MC_serverBD_4.iGotoLocationDataPriority[iLoc][iteam]))
									ENDIF
								ENDIF
							ENDFOR
						ELSE
							iNumberLocsChecked++
							PRINTLN("PROCESS_SYNCED_CAPTURE_SCORING - Incrementing for location: ", iLoc)
						ENDIF
					ENDIF
					
				ELSE
					IF iCapturePointBroadcastBitSet != 0
						iCapturePointBroadcastBitSet = 0
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF bIsLocalPlayerHost
			IF iNumberLocsChecked >= (MC_serverBD.iNumLocCreated)
				REINIT_NET_TIMER(MC_serverBD.tdCapturePointsTimer)
				iNumberLocsChecked = 0
				iCapturePointBroadcastBitSet = 0
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_IN_OUTFIT_FOR_LOCATION(INT iLocation)
	
	IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].eOutfitRequiredForLocation = OUTFIT_FOR_LOCATION_SPAWN_CHECK_NONE
		RETURN TRUE
	ENDIF
	
	SWITCH g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].eOutfitRequiredForLocation
		CASE OUTFIT_FOR_LOCATION_SPAWN_CHECK_FIREMAN			RETURN IS_LOCAL_PLAYER_WEARING_A_FIREMAN_OUTFIT()
		CASE OUTFIT_FOR_LOCATION_SPAWN_CHECK_SWAT				RETURN IS_LOCAL_PLAYER_WEARING_A_NOOSE_OUTFIT()
		CASE OUTFIT_FOR_LOCATION_SPAWN_CHECK_HIGH_ROLLER		RETURN IS_LOCAL_PLAYER_WEARING_A_HIGH_ROLLER_OUTFIT()
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_PROCESS_LOCATION(INT iLocation)
	IF NOT IS_PLAYER_IN_OUTFIT_FOR_LOCATION(iLocation)
		PRINTLN("SHOULD_PROCESS_LOCATION - location: ", iLocation, " is being suppressed. The player is not in the correct outfit here.")
		RETURN FALSE
	ENDIF
		
	RETURN TRUE
ENDFUNC

PROC CLEANUP_NON_PROCESSED_LOCATIONS(INT iLocation)
	IF DOES_BLIP_EXIST(LocBlip[iLocation])
		REMOVE_BLIP(LocBlip[iLocation])
		PRINTLN("CLEANUP_NON_PROCESSED_LOCATIONS - location: ", iLocation, " Removing Blip - Not being processed at this time.")
	ENDIF
ENDPROC

FUNC BOOL SHOULD_START_LOCATION_DELAY_TIMER(INT iLocation)

	UNUSED_PARAMETER(iLocation)
	
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
		PRINTLN("[LeaveArea] SHOULD_START_LOCATION_DELAY_TIMER - SBBOOL_FIRST_UPDATE_DONE isn't set")
		RETURN FALSE
	ENDIF
	
	IF IS_CUTSCENE_PLAYING()
		PRINTLN("[LeaveArea] SHOULD_START_LOCATION_DELAY_TIMER - IS_CUTSCENE_PLAYING")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC START_PROCESSING_LOCATION(INT iLocation)

	IF NOT SHOULD_START_LOCATION_DELAY_TIMER(iLocation)
		EXIT
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(tdLocationProcDelayTimer[iLocation])
		START_NET_TIMER(tdLocationProcDelayTimer[iLocation])
		PRINTLN("[LeaveArea] PROCESS_LOCATION_EVERY_FRAME -  location: ", iLocation, " Starting processing delay timer.")
	ENDIF
ENDPROC

PROC PROCESS_LOCATION_EVERY_FRAME()
	INT i = 0
	IF SHOULD_SHOW_LOCATIONS_HUD()
		IF MC_serverBD.iNumLocCreated > 0
		AND MC_serverBD.iNumLocCreated <= FMMC_MAX_GO_TO_LOCATIONS
			FOR i = 0 TO (MC_serverBD.iNumLocCreated-1)
				START_PROCESSING_LOCATION(i)
				IF GET_FRAME_COUNT() % 60 = 0
					PRINTLN("PROCESS_LOCATION_EVERY_FRAME - location: ", i)
				ENDIF
				IF SHOULD_PROCESS_LOCATION(i)
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciEVERY_FRAME_GOTO_LOC_PROCESSING)
						IF IS_LOCATION_READY_FOR_PROCESSING(i)
							SERVER_PROCESS_LOCATION_OBJECTIVES(i)
						ENDIF
						LOCAL_PROCESS_LOCATION_OBJECTIVES(i)
					ENDIF
					DISPLAY_LOCATION_HUD(i)
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciSYNCED_CAPTURE_SCORING)
						PROCESS_SYNCED_CAPTURE_SCORING(i)
					ENDIF
					IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
						CALCULATE_CAPTURE_LOCATION_STATE_AND_COLOUR(i)
					ENDIF
				ELSE
					CLEANUP_NON_PROCESSED_LOCATIONS(i)
				ENDIF
			ENDFOR
			
			IF MC_playerBD[iLocalPart].iCurrentLoc = -1
				CLEAR_CAP_SCAN_VFX()
			ENDIF
		ENDIF
	ELSE
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TOUR_DE_FORCE (g_FMMC_STRUCT.iAdversaryModeType)
			IF MC_serverBD.iNumLocCreated > 0
			AND MC_serverBD.iNumLocCreated <= FMMC_MAX_GO_TO_LOCATIONS
				FOR i = 0 TO (MC_serverBD.iNumLocCreated-1)
					HIDE_LOCATES(i)
				ENDFOR
			ENDIF
		ENDIF
	ENDIF
ENDPROC

