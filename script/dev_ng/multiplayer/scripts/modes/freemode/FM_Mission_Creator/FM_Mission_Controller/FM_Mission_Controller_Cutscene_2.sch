
USING "FM_Mission_Controller_USING.sch"
USING "FM_Mission_Controller_Utility.sch"
USING "FM_Mission_Controller_Entities.sch"
USING "FM_Mission_Controller_ObjectiveCompletion.sch"
USING "FM_Mission_Controller_SpecialCase.sch"
USING "FM_Mission_Controller_Rules.sch"
USING "FM_Mission_Controller_Doors.sch"
USING "FM_Mission_Controller_Cutscene.sch"
USING "FM_Mission_Controller_Debug.sch"
USING "FM_Mission_Controller_Locations.sch"
USING "FM_Mission_Controller_Vehicles.sch"
USING "FM_Mission_Controller_Peds_Brain.sch"
USING "FM_Mission_Controller_Peds_Body.sch"
USING "FM_Mission_Controller_Peds.sch"
USING "FM_Mission_Controller_GangChase.sch"
USING "FM_Mission_Controller_Minigame.sch"
USING "FM_Mission_Controller_Objects.sch"
USING "FM_Mission_Controller_Props.sch"
USING "FM_Mission_Controller_Audio.sch"
USING "FM_Mission_Controller_HUD.sch"
USING "FM_Mission_Controller_Players.sch"
USING "FM_Mission_Controller_Zones.sch"
USING "FM_Mission_Controller_Bounds.sch"
USING "FM_Mission_Controller_Events.sch"
USING "FM_Mission_Controller_Ending.sch"
USING "commands_hud.sch"


//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: MOCAP CUTSCENES !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************



FUNC BOOL SHOULD_TEAM_SEE_SAME_CUTSCENE_AS_TEAM(INT iteam, INT iteam2)

INT iCutsceneToUse
BOOL bIsMocap

	IF iteam = iteam2
		RETURN TRUE
	ENDIF
	IF g_iFMMCScriptedCutscenePlaying= -1 // end of mission cutscene
	OR g_bMissionEnding
		IF DOES_TEAM_LIKE_TEAM(iteam,iteam2)
			RETURN TRUE
		ENDIF
	ELSE
		IF g_iFMMCScriptedCutscenePlaying >= FMMC_GET_MAX_SCRIPTED_CUTSCENES()
		AND g_iFMMCScriptedCutscenePlaying < FMMC_GET_MAX_CUTSCENES_WITH_MOCAP()
			iCutsceneToUse = (g_iFMMCScriptedCutscenePlaying - FMMC_GET_MAX_SCRIPTED_CUTSCENES()) 
			IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].tlName) 
				bIsMocap = TRUE 
			ENDIF
		ELSE
			iCutsceneToUse = g_iFMMCScriptedCutscenePlaying
		ENDIF
		IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
			IF bIsMocap
				SWITCH iteam
				
					CASE 0
						IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitset,ci_CSBS_Pull_Team0_In)
							RETURN TRUE
						ENDIF
					BREAK
					CASE 1
						IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitset,ci_CSBS_Pull_Team1_In)
							RETURN TRUE
						ENDIF		
					BREAK
					CASE 2
						IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitset,ci_CSBS_Pull_Team2_In)
							RETURN TRUE
						ENDIF		
					BREAK
					CASE 3
						IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitset,ci_CSBS_Pull_Team3_In)
							RETURN TRUE
						ENDIF		
					BREAK
				ENDSWITCH		
			ELSE
				SWITCH iteam
				
					CASE 0
						IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset,ci_CSBS_Pull_Team0_In)
							RETURN TRUE
						ENDIF
					BREAK
					CASE 1
						IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset,ci_CSBS_Pull_Team1_In)
							RETURN TRUE
						ENDIF		
					BREAK
					CASE 2
						IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset,ci_CSBS_Pull_Team2_In)
							RETURN TRUE
						ENDIF		
					BREAK
					CASE 3
						IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset,ci_CSBS_Pull_Team3_In)
							RETURN TRUE
						ENDIF		
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

///PURPOSE: This function returns the string name of the next cutscene to stream

PROC REGISTER_PLAYER_FOR_CUTSCENE( INT iplayer, INT iCutsceneTeam, INT& iRegisterCount )

	TEXT_LABEL_23 sPlayerString
	TEXT_LABEL_23 sWeaponHandleString
	BOOL bStrandMission = IS_A_STRAND_MISSION_BEING_INITIALISED()

	IF MC_serverBD.piCutscenePlayerIDs[iCutsceneTeam][iplayer] != INVALID_PLAYER_INDEX()
		IF IS_NET_PLAYER_OK(MC_serverBD.piCutscenePlayerIDs[iCutsceneTeam][iplayer])
	        IF MC_serverBD.piCutscenePlayerIDs[iCutsceneTeam][iplayer] != PlayerToUse //not equal to player_ped_id()
			OR IS_PLAYER_SCTV(LocalPlayer)
			OR IS_LOCAL_PLAYER_ANY_SPECTATOR()
			or ARE_STRINGS_EQUAL(sMocapCutscene, "mph_pri_sta_mcs1") //prison - station / casco
			OR ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PAC_WIT_MCS1") //Pacific Standard Boat Cutscene
			or are_strings_equal(sMocapCutscene, "mph_tut_ext")
			OR ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_HUM_FIN_EXT")
			
				IF NOT IS_PED_INJURED(MocapPlayerPed[iplayer])
				AND MocapPlayerPed[iplayer] !=NULL
					
					sPlayerString = GET_CUTSCENE_PLAYER_PREFIX(iRegisterCount,iplayer)
					
					//Update handle for exceptions where the player is a clone
					IF MC_serverBD.piCutscenePlayerIDs[iCutsceneTeam][iplayer] = PlayerToUse
						tlPlayerSceneHandle = sPlayerString
					ENDIF
					
					//SET_ENTITY_VISIBLE(MocapPlayerPed[iplayer],TRUE)
					//SET_ENTITY_VISIBLE_IN_CUTSCENE(MocapPlayerPed[iplayer], true)
					
					IF NOT IS_STRING_NULL(tlPlayerSceneHandle)
							
						IF ARE_STRINGS_EQUAL(sMocapCutscene, "mph_pac_fin_mcs0")
						OR ARE_STRINGS_EQUAL(sMocapCutscene, "mph_pac_fin_mcs1")
						
							INT iWeaponDetails = -1
							INT iWeapLoop
							
							FOR iWeapLoop = 0 TO (ciMAX_CUT_PLAYERS - 1)
								IF sPlayerWeaponDetailsForCutscene[iWeapLoop].bSet
								AND sPlayerWeaponDetailsForCutscene[iWeapLoop].iParticipant = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(MC_serverBD.piCutscenePlayerIDs[iCutsceneTeam][iplayer]))
									iWeaponDetails = iWeapLoop
									PRINTLN("REGISTER_PLAYER_FOR_CUTSCENE - iWeaponDetails = ", iWeaponDetails, " for participant = ", sPlayerWeaponDetailsForCutscene[iWeapLoop].iParticipant)
									PRINTLN("REGISTER_PLAYER_FOR_CUTSCENE - has got weapon = ", sPlayerWeaponDetailsForCutscene[iWeaponDetails].bHasThisWeapon)
									PRINTLN("REGISTER_PLAYER_FOR_CUTSCENE - weapon = ", GET_WEAPON_NAME(sPlayerWeaponDetailsForCutscene[iWeaponDetails].sWeaponInfo.eWeaponType))
									iWeapLoop = ciMAX_CUT_PLAYERS // Exit loop
								ELSE
									PRINTLN("REGISTER_PLAYER_FOR_CUTSCENE NOT setting weapon details. sPlayerWeaponDetailsForCutscene[", iWeapLoop, "].iParticipant = ", sPlayerWeaponDetailsForCutscene[iWeapLoop].iParticipant)
									PRINTLN("REGISTER_PLAYER_FOR_CUTSCENE iPlayer participant ID = ", NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(MC_serverBD.piCutscenePlayerIDs[iCutsceneTeam][iplayer])))
								ENDIF
							ENDFOR
							
							PED_INDEX tempPed = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_PARTICIPANT_INDEX(MC_serverBD.piCutscenePlayerIDs[iCutsceneTeam][iplayer])))
							BOOL bHasGotWeapon = HAS_PED_GOT_WEAPON(tempPed, WEAPONTYPE_DLC_SPECIALCARBINE)
							BOOL bGotWeaponEventData = (iWeaponDetails != -1 AND sPlayerWeaponDetailsForCutscene[iWeaponDetails].bHasThisWeapon AND sPlayerWeaponDetailsForCutscene[iWeaponDetails].sWeaponInfo.eWeaponType = WEAPONTYPE_DLC_SPECIALCARBINE)
										
							IF bHasGotWeapon
							// b* 2230126
							// Use the broadcast data to check if the ped has the weapon here 
							// as clones don't sync weapons unless they're equipped by that player
							OR bGotWeaponEventData
							
								//Create a weapon object with all mods etc. 
								//register it and carry it through.						
								// b* 2230126 use cached data instead
								IF bGotWeaponEventData
									MocapPlayerWeaponObjects[iplayer] = CREATE_WEAPON_OBJECT_FROM_WEAPON_INFO( sPlayerWeaponDetailsForCutscene[iWeaponDetails].sWeaponInfo, GET_ENTITY_COORDS( tempPed ) - <<0,0,10>> )
								ELSE
									MocapPlayerWeaponObjects[iplayer] = CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(tempPed, WEAPONTYPE_DLC_SPECIALCARBINE)
								ENDIF
							
								SET_ENTITY_COORDS(MocapPlayerWeaponObjects[iplayer], <<0.0, 0.0, 10.0>>)
								GIVE_WEAPON_OBJECT_TO_PED(MocapPlayerWeaponObjects[iplayer], MocapPlayerPed[iplayer])						
								
								//this needs to be here
								// b* 2230126 use cached data instead
								IF bGotWeaponEventData
									MocapPlayerWeaponObjects[iplayer] = CREATE_WEAPON_OBJECT_FROM_WEAPON_INFO( sPlayerWeaponDetailsForCutscene[iWeaponDetails].sWeaponInfo, GET_ENTITY_COORDS( MocapPlayerPed[iplayer] ) - <<0,0,10>> )
								ELSE
									MocapPlayerWeaponObjects[iplayer] = CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(MocapPlayerPed[iplayer], WEAPONTYPE_DLC_SPECIALCARBINE)
								ENDIF

								SET_ENTITY_VISIBLE(MocapPlayerWeaponObjects[iplayer], FALSE)
								
								SET_CURRENT_PED_WEAPON(MocapPlayerPed[iplayer], WEAPONTYPE_DLC_SPECIALCARBINE, FALSE)
								
								IF bGotWeaponEventData
									PRINTLN("[PED CUT] [ROSSW] REGISTER_PLAYER_FOR_CUTSCENE Using event data for weapon (bGotWeaponEventData): ", sPlayerString)
								ELSE
									PRINTLN("[PED CUT] [ROSSW] REGISTER_PLAYER_FOR_CUTSCENE Using weapon from ped (bHasGotWeapon): ", sPlayerString)
								ENDIF
								
							ELSE
								GIVE_WEAPON_TO_PED(MocapPlayerPed[iplayer], WEAPONTYPE_DLC_SPECIALCARBINE, GET_WEAPON_CLIP_SIZE(WEAPONTYPE_DLC_SPECIALCARBINE) * 10, TRUE)
								MocapPlayerWeaponObjects[iplayer] = CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_PARTICIPANT_INDEX(MC_serverBD.piCutscenePlayerIDs[iCutsceneTeam][iplayer]))), WEAPONTYPE_DLC_SPECIALCARBINE)   															
								
								SET_ENTITY_VISIBLE(MocapPlayerWeaponObjects[iplayer], FALSE)
								
								PRINTLN("[PED CUT] [ROSSW] REGISTER_PLAYER_FOR_CUTSCENE player DID NOT HAVE weapon in inventory already", sPlayerString)
							ENDIF
																				
							sWeaponHandleString = "rifle^"
							sWeaponHandleString += iRegisterCount + 1//(iplayer + 1)
							
							PRINTLN("[PED CUT] [ROSSW] REGISTER_PLAYER_FOR_CUTSCENE sWeaponHandleString = ", sWeaponHandleString, "iRegisterCount: ", iRegisterCount, " player ", sPlayerString)
							
							IF bStrandMission
								PASS_OVER_CUT_SCENE_OBJECT(MocapPlayerWeaponObjects[iplayer], sWeaponHandleString)
							ELSE
								REGISTER_ENTITY_FOR_CUTSCENE(MocapPlayerWeaponObjects[iplayer], sWeaponHandleString, CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
							ENDIF
							
						ENDIF
						
						IF bStrandMission
							PASS_OVER_CUT_SCENE_PED(MocapPlayerPed[iplayer], sPlayerString, iMocapPlayerPedMask[iplayer], MC_serverBD.piCutscenePlayerIDs[iCutsceneTeam][iplayer])
							MocapPlayerPed[iplayer] = NULL
						ELIF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_CUTSCENE_ON_FOOT_VISIBLE_CHECK)
							PRINTLN("[RCC MISSION] REGISTER_PLAYER_FOR_CUTSCENE REMOVING PLAYER FROM CUTSCENE")
							REGISTER_ENTITY_FOR_CUTSCENE(NULL, sPlayerString, CU_DONT_ANIMATE_ENTITY,DUMMY_MODEL_FOR_SCRIPT,CEO_IGNORE_MODEL_NAME)
							SET_ENTITY_VISIBLE(MocapPlayerPed[iplayer],FALSE)
						ELSE
							PRINTLN("RCC MISSION] [PED CUT] * REGISTER_ENTITY_FOR_CUTSCENE(MocapPlayerPed[", iplayer, "], ", sPlayerString, ", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY,DUMMY_MODEL_FOR_SCRIPT,CEO_IGNORE_MODEL_NAME)")
							REGISTER_ENTITY_FOR_CUTSCENE(MocapPlayerPed[iplayer], sPlayerString, CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY,DUMMY_MODEL_FOR_SCRIPT,CEO_IGNORE_MODEL_NAME)
						ENDIF		
						
						iRegisterCount++
						CPRINTLN( DEBUG_CONTROLLER, " [PED CUT] REGISTER_PLAYER_FOR_CUTSCENE Registering player for cutscene: ",NATIVE_TO_INT(MC_serverBD.piCutscenePlayerIDs[iCutsceneTeam][iplayer])," in as character: ",sPlayerString," iCutRegisterPlayerNum: ",iRegisterCount)
					ELSE
						CPRINTLN( DEBUG_CONTROLLER, " [PED CUT] REGISTER_PLAYER_FOR_CUTSCENE NOT Registering player for cutscene due to NULL handle: ",NATIVE_TO_INT(MC_serverBD.piCutscenePlayerIDs[iCutsceneTeam][iplayer])," in as character: ",sPlayerString," iCutRegisterPlayerNum: ",iRegisterCount)
					ENDIF
		        
				ELSE
					CPRINTLN( DEBUG_CONTROLLER, " [PED CUT] REGISTER_PLAYER_FOR_CUTSCENE - IS_PED_INJURED(MocapPlayerPed[iplayer])")
				ENDIF 
			ELSE
			
				//the player
				sPlayerString = GET_CUTSCENE_PLAYER_PREFIX(iRegisterCount,iplayer)
				tlPlayerSceneHandle = sPlayerString
				
				IF ARE_STRINGS_EQUAL(sMocapCutscene, "mph_pac_fin_mcs0")
				OR ARE_STRINGS_EQUAL(sMocapCutscene, "mph_pac_fin_mcs1")		
					//Create a weapon object with all mods etc. for local player
					//register it and carry it through.
								
					IF HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_SPECIALCARBINE)								
						SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_SPECIALCARBINE, FALSE)
					ELSE
						GIVE_WEAPON_TO_PED(LocalPlayerPed, WEAPONTYPE_DLC_SPECIALCARBINE, GET_WEAPON_CLIP_SIZE(WEAPONTYPE_DLC_SPECIALCARBINE) * 10, TRUE)
					ENDIF
											
					sWeaponHandleString = "rifle^"
					sWeaponHandleString += (iRegisterCount + 1)
					
					PRINTLN("[PED CUT] [ROSSW] REGISTER_PLAYER_FOR_CUTSCENE sWeaponHandleString = ", sWeaponHandleString, "for LOCAL iRegisterCount: ", iRegisterCount, " player ", sPlayerString)
											
					playersWeaponObject = CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(LocalPlayerPed, WEAPONTYPE_DLC_SPECIALCARBINE)   
					SET_ENTITY_COORDS(playersWeaponObject, <<0.0, 0.0, 10.0>>)
					
					SET_ENTITY_VISIBLE(playersWeaponObject, FALSE)
					
					IF bStrandMission
						PASS_OVER_CUT_SCENE_OBJECT(playersWeaponObject, sWeaponHandleString)
					ELSE
						REGISTER_ENTITY_FOR_CUTSCENE(playersWeaponObject, sWeaponHandleString, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)					
					ENDIF
										
					g_sTransitionSessionData.sStrandMissionData.localPlayersWeaponObject = playersWeaponObject
				
				ENDIF
				
				//Arm player after cut with hsi current weapon.
				IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_HUM_FIN_MCS1")	
					//Create a weapon object with all mods etc. for local player
					//register it and carry it through.
																				
					playersWeaponObject = CREATE_WEAPON_OBJECT_FROM_CURRENT_PED_WEAPON_WITH_COMPONENTS(LocalPlayerPed)   
					SET_ENTITY_COORDS(playersWeaponObject, <<0.0, 0.0, 10.0>>)
																
					g_sTransitionSessionData.sStrandMissionData.localPlayersWeaponObject = playersWeaponObject					
				ENDIF
				
				IF bStrandMission
					PASS_OVER_CUT_SCENE_PED(LocalPlayerPed, sPlayerString, GET_PLAYER_CHOSEN_MASK(LocalPlayer), LocalPlayer)
				ELIF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_CUTSCENE_ON_FOOT_VISIBLE_CHECK)
					REGISTER_ENTITY_FOR_CUTSCENE(NULL, sPlayerString, CU_DONT_ANIMATE_ENTITY,DUMMY_MODEL_FOR_SCRIPT,CEO_IGNORE_MODEL_NAME)
					SET_ENTITY_VISIBLE(localplayerped, FALSE)
					PRINTLN("[RCC MISSION] REGISTER_PLAYER_FOR_CUTSCENE SETTING LOCAL PLAYER TO NOT BE VISIBLE")
				ELSE
				
					PRINTLN("******************CUTSCENE VALUES*****************************")
					PRINTLN("[RCC MISSION] REGISTER_PLAYER_FOR_CUTSCENE LocalPlayer: ", NATIVE_TO_INT(LocalPlayer))
					PRINTLN("[RCC MISSION] REGISTER_PLAYER_FOR_CUTSCENE PLAYER_ID(): ", NATIVE_TO_INT(PLAYER_ID()))
					PRINTLN("[RCC MISSION] REGISTER_PLAYER_FOR_CUTSCENE LocalPlayerPed: ", NATIVE_TO_INT(LocalPlayerPed))
					PRINTLN("[RCC MISSION] REGISTER_PLAYER_FOR_CUTSCENE PLAYER_PED_ID(): ", NATIVE_TO_INT(PLAYER_PED_ID()))
					
					PRINTLN("[RCC MISSION] REGISTER_PLAYER_FOR_CUTSCENEPLAYER PED FROM GET_PLAYER_PED(PLAYER_ID()): ", NATIVE_TO_INT(GET_PLAYER_PED(PLAYER_ID())))
					PRINTLN("[RCC MISSION] REGISTER_PLAYER_FOR_CUTSCENEPLAYER PED FROM GET_PLAYER_PED(LocalPlayer): ", NATIVE_TO_INT(GET_PLAYER_PED(LocalPlayer)))
					
					IF NETWORK_IS_PLAYER_IN_MP_CUTSCENE(LocalPlayer)
						NETWORK_SET_IN_MP_CUTSCENE(FALSE,FALSE)
						PRINTLN("[TS] [MSRAND] - REGISTER_PLAYER_FOR_CUTSCENE NETWORK_SET_IN_MP_CUTSCENE(FALSE, FALSE)")
						CPRINTLN( DEBUG_CONTROLLER," REGISTER_PLAYER_FOR_CUTSCENE [PED CUT] player already in MP cutscene killing it")
					ENDIF
					SET_ENTITY_VISIBLE_IN_CUTSCENE(LocalPlayerPed, true)
					SET_BIT(iLocalBoolCheck10, LBOOL10_LOCAL_PLAYER_IN_CUTSCENE)
					
					REGISTER_ENTITY_FOR_CUTSCENE(LocalPlayerPed, sPlayerString, CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
					PRINTLN("RCC MISSION] [PED CUT] * REGISTER_ENTITY_FOR_CUTSCENE(LocalPlayerPed, ", sPlayerString, ", CU_ANIMATE_EXISTING_SCRIPT_ENTITY,DUMMY_MODEL_FOR_SCRIPT,CEO_IGNORE_MODEL_NAME)")
				ENDIF
				
				iRegisterCount++
				CPRINTLN( DEBUG_CONTROLLER," [PED CUT] REGISTER_PLAYER_FOR_CUTSCENERegistering local player for cutscene: ",NATIVE_TO_INT(MC_serverBD.piCutscenePlayerIDs[iCutsceneTeam][iplayer])," in as character: ",sPlayerString," iCutRegisterPlayerNum: ",iRegisterCount)
			ENDIF
		ELSE
			CPRINTLN( DEBUG_CONTROLLER," [PED CUT] REGISTER_PLAYER_FOR_CUTSCENE - IS_NET_PLAYER_OK(MC_serverBD.piCutscenePlayerIDs[", iCutsceneTeam, "][", iplayer, "])")
		ENDIF
	ELSE
		sPlayerString = GET_CUTSCENE_PLAYER_PREFIX(iplayer,iplayer) // url:bugstar:2184594 //Additional change for b*2172521 (was just looping MP_1)
		// Updated for url:bugstar:2184594 (Joseph Woods comment on 09-01-2015)
		
		IF IS_STRING_NULL_OR_EMPTY(sPlayerString)
			sPlayerString = "MP_"
			sPlayerString += (iplayer + 1)
			PRINTLN("[RCC MISSION] REGISTER_PLAYER_FOR_CUTSCENE GET_CUTSCENE_PLAYER_PREFIX RETURNED NULL STRING SETTING TO ", sPlayerString)
		ENDIF
		
		IF NOT IS_STRING_NULL_OR_EMPTY(sPlayerString)
			IF NOT bStrandMission
				REGISTER_ENTITY_FOR_CUTSCENE(NULL, sPlayerString, CU_DONT_ANIMATE_ENTITY,DUMMY_MODEL_FOR_SCRIPT,CEO_IGNORE_MODEL_NAME) //b*2172521
			ELSE
				//Strand need to pass over ped can't register a different frame
				PASS_OVER_CUT_SCENE_PED(NULL, sPlayerString, 0)
			ENDIF
		ENDIF
		
		CPRINTLN( DEBUG_CONTROLLER," [PED CUT] REGISTER_PLAYER_FOR_CUTSCENE - MC_serverBD.piCutscenePlayerIDs[", iCutsceneTeam, "][", iplayer, "] = INVALID_PLAYER_INDEX()")
	ENDIF
ENDPROC

PROC REMOVE_SWAT_FOR_mph_pac_fin_mcs1(STRING sMocapCutscenePassed)
	//Remove SWAT If neccessary.
	IF ARE_STRINGS_EQUAL(sMocapCutscenePassed, "mph_pac_fin_mcs1")
	
		IF MC_serverBD.iNextMission = 0	//Only cops
		
			CPRINTLN( DEBUG_CONTROLLER," [PED CUT] [ROSSW] remove SWAT from cutscene ")
			
			TEXT_LABEL_23 sTempTextLabel
			
			sTempTextLabel = "MPH_Riot"
			PASS_OVER_CUT_SCENE_VEHICLE(NULL, sTempTextLabel)
			sTempTextLabel = "MPH_Riot^1"
			PASS_OVER_CUT_SCENE_VEHICLE(NULL, sTempTextLabel)
			
			INT i
			FOR i = 1 TO 8
				sTempTextLabel = "MPH_Swat_"
				sTempTextLabel += (i)				
				PASS_OVER_CUT_SCENE_PED(NULL, sTempTextLabel)			
			ENDFOR
			
			FOR i = 1 TO 8
			
				//2223168 - objects removed for last gen.
				IF i <> 2
				OR (IS_XBOX_PLATFORM() OR IS_PLAYSTATION_PLATFORM())
							
					sTempTextLabel = "Cop_Rifle^"
					sTempTextLabel += (i)				
					PASS_OVER_CUT_SCENE_OBJECT(NULL, sTempTextLabel)			
								
				ENDIF
				
				
			ENDFOR
			
			FOR i = 1 TO 8
				sTempTextLabel = "cop_Rifle_Mag^"
				sTempTextLabel += (i)				
				PASS_OVER_CUT_SCENE_OBJECT(NULL, sTempTextLabel)			
			ENDFOR

		ELSE
			CPRINTLN( DEBUG_CONTROLLER," [PED CUT] [ROSSW] keep SWAT in cutscene")
		ENDIF
	
	ENDIF
	
	REMOVE_WEAPON_ASSET(WEAPONTYPE_DLC_SPECIALCARBINE)
	
ENDPROC

FUNC BOOL REMOVE_CUTSCENE_PLAYER(INT iplayer)

	IF iPlayer < 0
	OR iPlayer >= FMMC_MAX_CUTSCENE_PLAYERS
		ASSERTLN("REMOVE_CUTSCENE_PLAYER_PROPS - iPlayer: ", iPlayer, " is out of range")
		RETURN FALSE
	ENDIF

	TEXT_LABEL_23 sPlayerString
	sPlayerString = GET_CUTSCENE_PLAYER_PREFIX(iplayer,0)
	IF IS_A_STRAND_MISSION_BEING_INITIALISED()
		PASS_OVER_CUT_SCENE_PED(NULL, sPlayerString)
	ELSE
		PRINTLN("[RCC MISSION] [PED CUT] * REGISTER_ENTITY_FOR_CUTSCENE(NULL, ", sPlayerString, ", CU_DONT_ANIMATE_ENTITY,DUMMY_MODEL_FOR_SCRIPT,CEO_IGNORE_MODEL_NAME)")
		REGISTER_ENTITY_FOR_CUTSCENE(NULL, sPlayerString, CU_DONT_ANIMATE_ENTITY,DUMMY_MODEL_FOR_SCRIPT,CEO_IGNORE_MODEL_NAME)
	ENDIF
	PRINTLN("removing entity from cutscene : ",sPlayerString)
	CPRINTLN(DEBUG_MISSION, " Removing player from cutscene ", iplayer)

	RETURN TRUE
ENDFUNC

FUNC BOOL REMOVE_CUTSCENE_PLAYER_AND_PROPS(INT iPlayer, TEXT_LABEL_23 &tlPropHandles[])

	IF NOT REMOVE_CUTSCENE_PLAYER(iPlayer)
		RETURN FALSE
	ENDIF

	// Associated player props
	INT i
	FOR i = 0 TO COUNT_OF(tlPropHandles)-1
		
		TEXT_LABEL_23 propHandle = tlPropHandles[i]
		
		IF IS_STRING_NULL_OR_EMPTY(propHandle)
			PRINTLN("[RCC MISSION] REMOVE_CUTSCENE_PLAYER - propHandle is empty/null, skipping")
			RELOOP
		ENDIF
		
		IF DOES_CUTSCENE_HANDLE_EXIST(propHandle) != CHSR_HANDLE_EXISTS
			PRINTLN("[RCC MISSION] REMOVE_CUTSCENE_PLAYER - propHandle: ", propHandle," - does not exit, skipping")
			RELOOP
		ENDIF
	
		IF IS_A_STRAND_MISSION_BEING_INITIALISED()
			PRINTLN("[RCC MISSION] REMOVE_CUTSCENE_PLAYER - propHandle: ", propHandle," - Prop removed (strand)")
			PASS_OVER_CUT_SCENE_OBJECT(NULL, propHandle)
		ELSE
			PRINTLN("[RCC MISSION] REMOVE_CUTSCENE_PLAYER - propHandle: ", propHandle," - Prop removed")
			REGISTER_ENTITY_FOR_CUTSCENE(NULL, propHandle, CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
		ENDIF
		
	ENDFOR
	
	RETURN TRUE
ENDFUNC



PROC REMOVE_CUTSCENE_PLAYER_FROM_EMPTY_SEATS_PRISON_FINALE()
	
	VEHICLE_INDEX tempVeh = NULL
	TEXT_LABEL_23 sPlayerString
	INT i
	
	tempVeh = GET_FIRST_END_CUTSCENE_VEHICLE()
	
	IF NOT IS_VEHICLE_DRIVEABLE(tempVeh)
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			tempVeh= GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(tempVeh)
		INT iMaxSeats = (GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(tempVeh) + 1) //Add 1 for the driver
		REPEAT iMaxSeats i
			VEHICLE_SEAT tempSeat = INT_TO_ENUM(VEHICLE_SEAT, i-1)
			IF IS_VEHICLE_SEAT_FREE(tempVeh, tempSeat)
				sPlayerString  = GET_CUTSCENE_PREFIX_FROM_SEAT(tempSeat)
				PRINTLN("[RCC MISSION] [PED CUT] * REGISTER_ENTITY_FOR_CUTSCENE(NULL, ", sPlayerString, ", CU_DONT_ANIMATE_ENTITY,DUMMY_MODEL_FOR_SCRIPT,CEO_IGNORE_MODEL_NAME)")
				REGISTER_ENTITY_FOR_CUTSCENE(NULL, sPlayerString, CU_DONT_ANIMATE_ENTITY,DUMMY_MODEL_FOR_SCRIPT,CEO_IGNORE_MODEL_NAME)
			ENDIF
		ENDREPEAT
	
	ENDIF


ENDPROC

PROC REMOVE_CUTSCENE_PLAYER_FROM_EMPTY_SEATS_PRISON_MPH_PRI_FIN_MCS2()
	
	VEHICLE_INDEX tempVeh = NULL
	TEXT_LABEL_23 sPlayerString
	INT i
	
	tempVeh = GET_FIRST_MID_CUTSCENE_VEHICLE()
	
	IF NOT IS_VEHICLE_DRIVEABLE(tempVeh)
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			tempVeh= GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(tempVeh)
		INT iMaxSeats = 6 //Add 1 for the driver
		REPEAT iMaxSeats i
			VEHICLE_SEAT tempSeat = INT_TO_ENUM(VEHICLE_SEAT, i-1)
			PRINTLN("[RCC MISSION] [PED CUT] * REGISTER_ENTITY_FOR_CUTSCENE tempSeat = )", ENUM_TO_INT(tempSeat))
				
			IF IS_VEHICLE_SEAT_FREE(tempVeh, tempSeat)
				sPlayerString  = GET_CUTSCENE_MPH_PRI_FIN_MCS2_PREFIX_FROM_SEAT(tempSeat)
				IF tempSeat != VS_EXTRA_LEFT_1
					REGISTER_ENTITY_FOR_CUTSCENE(NULL, sPlayerString, CU_DONT_ANIMATE_ENTITY,DUMMY_MODEL_FOR_SCRIPT,CEO_IGNORE_MODEL_NAME)
				ENDIF
				PRINTLN("[RCC MISSION] [PED CUT] * REGISTER_ENTITY_FOR_CUTSCENE Seat not occupied)")
			
			ELIF NOT IS_VEHICLE_SEAT_FREE(tempVeh, tempSeat)		//check if ai ped is sitting on seat
				PED_INDEX temp_passenger =  GET_PED_IN_VEHICLE_SEAT(tempVeh, tempSeat)
				sPlayerString  = GET_CUTSCENE_MPH_PRI_FIN_MCS2_PREFIX_FROM_SEAT(tempSeat)
				IF DOES_ENTITY_EXIST(temp_passenger)
					IF IS_PED_A_PLAYER(temp_passenger)
						PRINTLN("[RCC MISSION] [PED CUT] 2* REGISTER_ENTITY_FOR_CUTSCENE(PLAYER, ", sPlayerString, ", There is someone sitting here")
					ELSE

						REGISTER_ENTITY_FOR_CUTSCENE(NULL, sPlayerString, CU_DONT_ANIMATE_ENTITY,DUMMY_MODEL_FOR_SCRIPT,CEO_IGNORE_MODEL_NAME)
						PRINTLN("[RCC MISSION] [PED CUT] * REGISTER_ENTITY_FOR_CUTSCENE Person sitting here is not a player)")
					ENDIF
				ELSE
					REGISTER_ENTITY_FOR_CUTSCENE(NULL, sPlayerString, CU_DONT_ANIMATE_ENTITY,DUMMY_MODEL_FOR_SCRIPT,CEO_IGNORE_MODEL_NAME)
					PRINTLN("[RCC MISSION] [PED CUT] 2* REGISTER_ENTITY_FOR_CUTSCENE(PLAYER, ", sPlayerString, ", Entity does not exist There is someone sitting here")
				ENDIF
			ENDIF
		ENDREPEAT
	
	ENDIF


ENDPROC

PROC REMOVE_CUTSCENE_PLAYERS_FOR_MPH_TUT_CAR_EXT()
	
	VEHICLE_INDEX tempVeh = GET_FIRST_END_CUTSCENE_VEHICLE()

	IF IS_VEHICLE_SEAT_FREE(tempVeh, VS_FRONT_RIGHT)
	AND IS_VEHICLE_SEAT_FREE(tempVeh, VS_BACK_LEFT)
	AND IS_VEHICLE_SEAT_FREE(tempVeh, VS_BACK_RIGHT)
		REGISTER_ENTITY_FOR_CUTSCENE(NULL, "MP_2", CU_DONT_ANIMATE_ENTITY,DUMMY_MODEL_FOR_SCRIPT,CEO_IGNORE_MODEL_NAME)
		PRINTLN("[RCC MISSION] [PED CUT] REGISTER_CUTSCENE_PLAYERS VS_FRONT_RIGHT Empty - Removing MP_2 ")
		CPRINTLN(DEBUG_MISSION, "REGISTER_CUTSCENE_PLAYERS VS_FRONT_RIGHT Empty - Removing MP_2")
	ENDIF

ENDPROC

PROC REGISTER_CUTSCENE_PLAYERS(INT iCutsceneTeam, INT iCutsceneId, FMMC_CUTSCENE_TYPE eCutType, BOOL bIncludePlayers = TRUE)
	
	INT iNumberOfPlayers = GET_MOCAP_NUMBER_OF_PLAYERS( sMocapCutscene )
	
	IF iNumberOfPlayers > 0
		REGISTER_PLAYER_FOR_CUTSCENE( 0, iCutsceneTeam, iCutRegisterPlayerNum )
	ENDIF
	
	PRINTLN("[RCC MISSION] [PED CUT] REGISTER_PLAYER_FOR_CUTSCENE GET_MOCAP_NUMBER_OF_PLAYERS( sMocapCutscene ) = ", iNumberOfPlayers)
	IF iNumberOfPlayers > 1
		REGISTER_PLAYER_FOR_CUTSCENE( 1, iCutsceneTeam, iCutRegisterPlayerNum )
		
		IF iNumberOfPlayers >2
			PRINTLN("[RCC MISSION] [PED CUT] REGISTER_PLAYER_FOR_CUTSCENE (2) ")
			REGISTER_PLAYER_FOR_CUTSCENE( 2, iCutsceneTeam, iCutRegisterPlayerNum )
			IF iNumberOfPlayers >3
				PRINTLN("[RCC MISSION] [PED CUT] REGISTER_PLAYER_FOR_CUTSCENE (3) ")
				REGISTER_PLAYER_FOR_CUTSCENE( 3, iCutsceneTeam, iCutRegisterPlayerNum )
			ENDIF
		ENDIF
	ENDIF
	if does_entity_exist(envelope.obj)
		
		SET_ENTITY_VISIBLE(envelope.obj, true)

		REGISTER_ENTITY_FOR_CUTSCENE(envelope.obj, envelope.scene_handle, CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
		
		printstring("envelope test 3")
		printnl()
	endif 
	
	IF iCutRegisterPlayerNum = 0
	AND iNumberOfPlayers > 0
		IF bIncludePlayers
			TEXT_LABEL_23 sPlayerString
			IF bLocalPlayerPedOk
				sPlayerString = GET_CUTSCENE_PLAYER_PREFIX( 0, 0 )
				IF IS_A_STRAND_MISSION_BEING_INITIALISED()
					PASS_OVER_CUT_SCENE_PED( LocalPlayerPed, sPlayerString, GET_PLAYER_CHOSEN_MASK(LocalPlayer), LocalPlayer)
				ELSE
					PRINTLN("[RCC MISSION] [PED CUT] * REGISTER_ENTITY_FOR_CUTSCENE( LocalPlayerPed, ", sPlayerString, ", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME )")
					REGISTER_ENTITY_FOR_CUTSCENE( LocalPlayerPed, sPlayerString, CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME )
				ENDIF
				iCutRegisterPlayerNum++
				PRINTLN("[PED CUT] Registering fallback player for cutscene: ",NATIVE_TO_INT(PlayerToUse)," in as character: ",iCutRegisterPlayerNum)
	        ENDIF
		ENDIF
	ENDIF
	

	IF NOT are_strings_equal(sMocapCutscene, "MPH_PRI_FIN_EXT")
		
		INT iPlayer
		FOR iPlayer = 0 TO iNumberOfPlayers-1
		
			// valid player, do not remove, skip
			IF iPlayer < iCutRegisterPlayerNum
			 	RELOOP
			ENDIF
		
			IF eCutType = FMMCCUT_MOCAP
			AND iCutsceneId >= 0
			AND iCutsceneId < MAX_MOCAP_CUTSCENES
				REMOVE_CUTSCENE_PLAYER_AND_PROPS(iPlayer, g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneId].sPlayerInfo[iPlayer].tlPropHandles)
				PRINTLN("[RCC MISSION] [PED CUT] REGISTER_CUTSCENE_PLAYERS - iPlayer: ", iPlayer, " Removing player & props")
			ELSE
				REMOVE_CUTSCENE_PLAYER(iPlayer)
				PRINTLN("[RCC MISSION] [PED CUT] REGISTER_CUTSCENE_PLAYERS - iPlayer: ", iPlayer, " Removing player only")
			ENDIF

		ENDFOR

	ELSE				
		IF are_strings_equal(sMocapCutscene, "MPH_PRI_FIN_EXT")
			REMOVE_CUTSCENE_PLAYER_FROM_EMPTY_SEATS_PRISON_FINALE()
		ENDIF
	ENDIF
	
ENDPROC

PROC SETUP_CAM_PARAMS(INT icutscene)
	
	BOOL bDidCameraShotCut = TRUE

	IF iCamShot - 1 >= 0
	
		VECTOR vLastShotEnd = g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].vCamEndPos[iCamShot-1]
		VECTOR vNextShotStart = g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].vCamStartPos[iCamShot]
		
		VECTOR vLastShotEndRot = g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].vCamEndRot[iCamShot-1]
		VECTOR vNextShotStartRot = g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].vCamStartRot[iCamShot]
		
		FLOAT fLastShotEndFOV = g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].fEndCamFOV[iCamShot-1]
		FLOAT fNextShotStartFOV = g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].fStartCamFOV[iCamShot]
		
		IF ARE_VECTORS_EQUAL(vLastShotEnd, vNextShotStart)
		AND ARE_VECTORS_EQUAL(vLastShotEndRot, vNextShotStartRot)
		AND fLastShotEndFOV = fNextShotStartFOV
			bDidCameraShotCut = FALSE
			PRINTLN("SETUP_CAM_PARAMS - icutscene: ", icutscene, " - SET bDidCameraShotCut FALSE")
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].iCutsceneShotBitSet[iCamShot - 1], ciCSS_BS_Attach_Enable)
			DETACH_CAM(cutscenecam)
			DESTROY_CAM(cutscenecam)
			cutscenecam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE) 
			PRINTLN("SETUP_CAM_PARAMS - icutscene: ", icutscene, " - detaching camera")
		ENDIF

	ENDIF

	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].vCamStartPos[iCamShot])
		SET_CAM_PARAMS(cutscenecam,g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].vCamStartPos[iCamShot],
			g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].vCamStartRot[iCamShot],
			g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].fStartCamFOV[iCamShot])
		PRINTLN("SETUP_CAM_PARAMS - icutscene: ", icutscene, " - setting new params" )
	ENDIF
	
	IF( IS_BIT_SET( g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].iCutsceneShotBitSet[iCamShot], ciCSS_BS_Attach_Enable ) )
		PRINTLN("SETUP_CAM_PARAMS - icutscene: ", icutscene, " - attaching camera" )
	
		INT 			iVehIndex 	= g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].iVehicleToAttachTo[iCamShot]
		NETWORK_INDEX	niVeh		= MC_serverBD_1.sFMMC_SBD.niVehicle[ iVehIndex ]
		VEHICLE_INDEX 	veh			= NULL
		
		IF( NETWORK_DOES_NETWORK_ID_EXIST( niVeh ) )
			veh = NET_TO_VEH( niVeh )
			
			ATTACH_CAM_TO_ENTITY( cutscenecam, veh, g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].vOffsetPos[iCamShot], TRUE )
			POINT_CAM_AT_ENTITY( cutscenecam, veh, g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].vLookAtPos[iCamShot], TRUE )
		ELSE
			ASSERTLN( "SETUP_CAM_PARAMS - icutscene: ", icutscene, " - Couldn't get cam attach entity" )
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].iShakeType[iCamShot] != -1
	AND g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].fShakeMag[iCamShot] != 0
	
		BOOL bApplyShake = TRUE
		
		// Do not apply this shots shake if there was not change in the camera shot and the shake is the same as the previous shot
		IF NOT bDidCameraShotCut
		AND iCamShot - 1 >= 0
			PRINTLN("SETUP_CAM_PARAMS - icutscene: ", icutscene, " - shot: ", iCamShot, ", iShakeType: ", g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].iShakeType[iCamShot])
			PRINTLN("SETUP_CAM_PARAMS - icutscene: ", icutscene, " - shot: ", iCamShot-1, ", iShakeType: ", g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].iShakeType[iCamShot-1])
			PRINTLN("SETUP_CAM_PARAMS - icutscene: ", icutscene, " - shot: ", iCamShot, ", fShakeMag: ", g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].fShakeMag[iCamShot])
			PRINTLN("SETUP_CAM_PARAMS - icutscene: ", icutscene, " - shot: ", iCamShot-1, ", fShakeMag: ", g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].fShakeMag[iCamShot-1])
			
			IF g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].iShakeType[iCamShot] = g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].iShakeType[iCamShot-1]
			//AND g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].fShakeMag[iCamShot] = g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].fShakeMag[iCamShot-1]
				bApplyShake = FALSE
				PRINTLN("SETUP_CAM_PARAMS - icutscene: ", icutscene, " - Set bApplyShake FALSE" )
			ENDIF
		ENDIF
	
		IF bApplyShake
			SHAKE_CAM(cutscenecam,GET_CAM_SHAKE_TYPE_FROM_INT(g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].iShakeType[iCamShot]),g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].fShakeMag[iCamShot])
			PRINTLN("SETUP_CAM_PARAMS - icutscene: ", icutscene, " - shaking cam" )
		ENDIF
	ELSE
		STOP_CAM_SHAKING(cutscenecam,TRUE)
		PRINTLN("SETUP_CAM_PARAMS - icutscene: ", icutscene, " - stopping shaking cam" )
	ENDIF
	

	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].vCamEndPos[iCamShot])
		PRINTLN("SETUP_CAM_PARAMS - icutscene: ", icutscene, " - setting interp data" )
		SET_CAM_PARAMS(cutscenecam,g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].vCamEndPos[iCamShot],
			g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].vCamEndRot[iCamShot],
			g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].fEndCamFOV[iCamShot],
			g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].iInterpTime[iCamShot],
			g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].CamType[iCamShot],
			g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].CamType[iCamShot])	
	ENDIF
	
ENDPROC

FUNC NETWORK_INDEX GET_CUTSCNE_VISIBLE_NET_ID(INT itype, INT ientity)

	NETWORK_INDEX TargetNetID= NULL
	PRINTLN("[KW_CUTSCNES]GET_CUTSCNE_VISIBLE_NET_ID itype , =",itype )
	PRINTLN("[KW_CUTSCNES]GET_CUTSCNE_VISIBLE_NET_ID ientity =",ientity )
	
	SWITCH itype

		CASE CREATION_TYPE_PEDS		
			IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[ientity])
				PRINTLN("[KW_CUTSCNES]GET_CUTSCNE_VISIBLE_NET_ID CREATION_TYPE_PEDS ientity =",ientity, " Exists." )
				TargetNetID = MC_serverBD_1.sFMMC_SBD.niPed[ientity]
			ELSE
				PRINTLN("[KW_CUTSCNES]GET_CUTSCNE_VISIBLE_NET_ID CREATION_TYPE_PEDS ientity =",ientity, " is dead or does not exist.")
			ENDIF			
		BREAK
		
		CASE CREATION_TYPE_VEHICLES
			IF IS_NET_VEHICLE_DRIVEABLE(MC_serverBD_1.sFMMC_SBD.niVehicle[ientity])
				PRINTLN("[KW_CUTSCNES]GET_CUTSCNE_VISIBLE_NET_ID CREATION_TYPE_VEHICLES ientity =",ientity, " Exists.")
				TargetNetID = MC_serverBD_1.sFMMC_SBD.niVehicle[ientity]
			ELSE
				PRINTLN("[KW_CUTSCNES]GET_CUTSCNE_VISIBLE_NET_ID CREATION_TYPE_VEHICLES ientity =",ientity, " is dead or does not exist.")
			ENDIF
		BREAK
		
		CASE CREATION_TYPE_OBJECTS
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[ientity])
			AND NOT IS_ENTITY_DEAD(NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niObject[ientity]))
				PRINTLN("[KW_CUTSCNES]GET_CUTSCNE_VISIBLE_NET_ID CREATION_TYPE_OBJECTS ientity =",ientity, "Exists" )
				TargetNetID = MC_serverBD_1.sFMMC_SBD.niObject[ientity]
			ELSE
				PRINTLN("[KW_CUTSCNES]GET_CUTSCNE_VISIBLE_NET_ID CREATION_TYPE_OBJECTS ientity =",ientity, " is dead or does not exist.")
			ENDIF			
		BREAK
		
		CASE CREATION_TYPE_DYNOPROPS			
			IF NOT IS_ENTITY_DEAD(NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niDynoProps[ientity]))
				PRINTLN("[KW_CUTSCNES]GET_CUTSCNE_VISIBLE_NET_ID CREATION_TYPE_DYNOPROPS ientity =",ientity, " Exists" )
				TargetNetID = MC_serverBD_1.sFMMC_SBD.niDynoProps[ientity]
			ELSE
				PRINTLN("[KW_CUTSCNES]GET_CUTSCNE_VISIBLE_NET_ID CREATION_TYPE_DYNOPROPS ientity =",ientity, " is dead or does not exist.")
			ENDIF
		BREAK
		
	ENDSWITCH

	RETURN TargetNetID

ENDFUNC

FUNC ENTITY_INDEX GET_CUTSCNE_VISIBLE_PROP(INT ientity)

	ENTITY_INDEX TargetEntityID= NULL
	IF NOT IS_ENTITY_DEAD(oiProps[ ientity ])
		TargetEntityID = oiProps[ ientity ]
	ENDIF
			
	RETURN TargetEntityID		
	
ENDFUNC

FUNC BOOL DOES_CUTSCENE_HAVE_REGISTERED_ENTITIES(INT iCutscene, FMMC_CUTSCENE_TYPE cutType)

	INT i
	
	IF (cutType = FMMCCUT_MOCAP OR cutType = FMMCCUT_SCRIPTED)
	AND (iCutscene < 0 OR iCutScene >= PICK_INT(cutType = FMMCCUT_SCRIPTED, FMMC_GET_MAX_SCRIPTED_CUTSCENES(), MAX_MOCAP_CUTSCENES))
		ASSERTLN("[RCC MISSION] DOES_CUTSCENE_HAVE_REGISTERED_ENTITIES - cutType: ", cutType, ", iCutscene: ", iCutscene, " - iCutscene is out of range for the given cutType")
		RETURN FALSE
	ENDIF

	SWITCH cutType
	
		CASE FMMCCUT_SCRIPTED 
			PRINTLN("[RCC MISSION] DOES_CUTSCENE_HAVE_REGISTERED_ENTITIES - Checking Scripted Cutscene")
			
			FOR i = 0 TO (FMMC_MAX_CUTSCENE_ENTITIES-1)
				IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iType !=-1
				AND g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iIndex != -1
					CPRINTLN(DEBUG_CONTROLLER,"[RCC MISSION] WE HAVE A VALID ENTITY IN THE LIST SCRIPTED CUTSCENE")
					RETURN TRUE
				ENDIF
			ENDFOR
		
		BREAK
		
		CASE FMMCCUT_MOCAP
			PRINTLN("[RCC MISSION] DOES_CUTSCENE_HAVE_REGISTERED_ENTITIES - Checking Mid Mission Mocap")
			
			FOR i = 0 TO (FMMC_MAX_CUTSCENE_ENTITIES-1)
				IF g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].sCutsceneEntities[i].iType !=-1
				AND g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].sCutsceneEntities[i].iIndex != -1
					CPRINTLN( DEBUG_CONTROLLER,"[RCC MISSION] WE HAVE A VALID ENTITY IN THE LIST MID MOCAP")
					RETURN TRUE
				ENDIF	
			ENDFOR
		
		BREAK
		
		CASE FMMCCUT_ENDMOCAP
			PRINTLN("[RCC MISSION] DOES_CUTSCENE_HAVE_REGISTERED_ENTITIES - Checking End Mocap")
			
			FOR i = 0 TO (FMMC_MAX_CUTSCENE_ENTITIES-1)				
				IF g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iType !=-1
				AND g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iIndex != -1
					CPRINTLN( DEBUG_CONTROLLER,"[RCC MISSION] WE HAVE A VALID ENTITY IN THE LIST END MOCAP")
					RETURN TRUE
				ENDIF
			ENDFOR
		
		BREAK
	
	ENDSWITCH
	
	PRINTLN("[RCC MISSION] DOES_CUTSCENE_HAVE_REGISTERED_ENTITIES - WE HAVE NO VALID ENTITIES IN THE LIST")
	RETURN FALSE
	
ENDFUNC

//Special case for B* 2073716, so that only one truck appears in the end cutscene
FUNC INT GET_CLOSEST_PHANTOM_FOR_METH_END_CUTSCENE()
	INT i, iClosestEntityIndex
	FLOAT fTempDistance = 100
	ENTITY_INDEX EntID = NULL
	NETWORK_INDEX phantomNetID = NULL
	FOR i = 0 TO (FMMC_MAX_CUTSCENE_ENTITIES - 1)
		IF ARE_STRINGS_EQUAL(g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].tlCutsceneHandle, "PHANTOM")
			phantomNetID = GET_CUTSCNE_VISIBLE_NET_ID(g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iType, g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iIndex)
			
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID( phantomNetID )
				EntID = NET_TO_ENT( phantomNetID )
				IF GET_DISTANCE_BETWEEN_COORDS(GET_CUTSCENE_COORDS(-1), GET_ENTITY_COORDS(EntID)) < fTempDistance
					fTempDistance = GET_DISTANCE_BETWEEN_COORDS(GET_CUTSCENE_COORDS(-1), GET_ENTITY_COORDS(EntID))
					iClosestEntityIndex = g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iIndex
				ENDIF
			ELSE
			
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN iClosestEntityIndex

ENDFUNC

//In some instance the cutscene is complete on one machine before the others start
//Or the vehicle is destroyed but hasn't failed
//This checks if we should add a fail safe vehicle
FUNC BOOL CHECK_TO_ADD_FAIL_SAFE_VEHICLE(STRING sCutsceneHandle, MODEL_NAMES &vehmod)

	//**********************************HUMANE LABS*******************************
	
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_HUM_ARM_EXT")
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "insurgent1")
			vehmod = INSURGENT
			RETURN TRUE
		ENDIF
		
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "insurgent2")
			vehmod = INSURGENT2
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_HUM_DEL_EXT")
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "MPH_Boxville")
			vehmod = BOXVILLE3
			RETURN TRUE
		ENDIF
		
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "EXL_mesa3") // They renamed Lead_Mesa
		OR ARE_STRINGS_EQUAL(sCutsceneHandle, "MPH_Lead_Mesa")
		OR ARE_STRINGS_EQUAL(sCutsceneHandle, "MPH_Middle_Mesa")
		OR ARE_STRINGS_EQUAL(sCutsceneHandle, "MPH_Rear_Mesa")
			vehmod = MESA3
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_HUM_EMP_EXT")
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "insurgent2")
			vehmod = INSURGENT2
			RETURN TRUE
		ENDIF
		
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "hydra")
			vehmod = hydra
			RETURN TRUE
		ENDIF
		
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "valkyrie")
			vehmod = valkyrie
			RETURN TRUE
		ENDIF
		
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "Karen_Car")
		OR ARE_STRINGS_EQUAL(sCutsceneHandle, "Karen_Car_2")
			vehmod = zion
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_HUM_KEY_MCS1")
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "Karen_Car")
			vehmod = zion
			RETURN TRUE
		ENDIF
		
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "Parked_Car")
		OR ARE_STRINGS_EQUAL(sCutsceneHandle, "Emperor")
			vehmod = EMPEROR
			RETURN TRUE
		ENDIF
		
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "Regina_1")
		OR ARE_STRINGS_EQUAL(sCutsceneHandle, "Regina_2")
			vehmod = REGINA
			RETURN TRUE
		ENDIF
		
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "zion_1")
		OR ARE_STRINGS_EQUAL(sCutsceneHandle, "zion_2")
			vehmod = ZION
			RETURN TRUE
		ENDIF
		
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "Baller")
			vehmod = BALLER
			RETURN TRUE
		ENDIF
		
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "FBI_Car")
		OR ARE_STRINGS_EQUAL(sCutsceneHandle, "FBI_Car_1")
		OR ARE_STRINGS_EQUAL(sCutsceneHandle, "FBI_Car_2")
		OR ARE_STRINGS_EQUAL(sCutsceneHandle, "FBI_Car_3")
			vehmod = FBI
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_HUM_VAL_EXT")
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "Valkyrie")
			vehmod = VALKYRIE
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_HUM_FIN_EXT")
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "Valkyrie")
			vehmod = VALKYRIE
			RETURN TRUE
		ENDIF
	ENDIF
	
	//****************************************************************************



	//**********************************NARCOTICS*********************************
	
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_NAR_BIK_EXT")
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "Gang_Van")
		OR ARE_STRINGS_EQUAL(sCutsceneHandle, "Gang_Van_2")
			vehmod = GBURRITO
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_NAR_MET_EXT")
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "Tanker")
			vehmod = TANKER2
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_NAR_TRA_EXT")
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "MPH_Trash")
			vehmod = TRASH
			RETURN TRUE
		ENDIF
	ENDIF
	
	//b*2145781
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_NAR_WEE_EXT")
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "Benson_1")
		OR ARE_STRINGS_EQUAL(sCutsceneHandle, "Benson_2")
			vehmod = BENSON
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_NAR_FIN_EXT")
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "Mule_01")
		OR ARE_STRINGS_EQUAL(sCutsceneHandle, "Mule_02")
			vehmod = MULE3
			RETURN TRUE
		ENDIF
	ENDIF
	
	//****************************************************************************
	
	
	
	//********************************PACIFIC STANDARD****************************
	
	//b*2114326
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PAC_BIK_EXT")
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "MPH_bike_1")
		OR ARE_STRINGS_EQUAL(sCutsceneHandle, "MPH_bike_2")
		OR ARE_STRINGS_EQUAL(sCutsceneHandle, "MPH_bike_3")
		OR ARE_STRINGS_EQUAL(sCutsceneHandle, "MPH_bike_4")
			vehmod = LECTRO
			RETURN TRUE
		ENDIF
	ENDIF
	
	//b*2113350
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PAC_HAC_MCS1")
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "Server_Van1")
		OR ARE_STRINGS_EQUAL(sCutsceneHandle, "Server_Van2")
			vehmod = BURRITO2
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PAC_CON_EXT")
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "Truck")
			vehmod = BARRACKS3
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PAC_FIN_EXT")
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "MPH_Boat")
			vehmod = DINGHY3
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PAC_HAC_EXT")
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "Server_Van1")
			vehmod = GBURRITO2
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PAC_PHO_EXT")
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "Van")
			vehmod = BOXVILLE4
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PAC_WIT_MCS1")
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "Money_Boat")
			vehmod = PREDATOR
			RETURN TRUE
		ENDIF
	ENDIF
	
	//****************************************************************************


	//**********************************PRISON************************************
	
	//b*2156102
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PRI_BUS_EXT")
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "BUS")
			vehmod = PBUS
			RETURN TRUE
		ENDIF
	ENDIF
	
	//b*2158322
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PRI_FIN_MCS2")
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "PLANE")
			vehmod = VELUM2
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PRI_FIN_EXT")
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "MPH_Helicopter")
			vehmod = BUZZARD2
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PRI_PLA_EXT")
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "PLANE")
			vehmod = VELUM2
			RETURN TRUE
		ENDIF
		
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "CIA_Car_1")
		OR ARE_STRINGS_EQUAL(sCutsceneHandle, "CIA_Car_2")
			vehmod = FBI
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PRI_STA_MCS2")
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "CASCO")
			vehmod = CASCO
			RETURN TRUE
		ENDIF
	ENDIF
	
	//*****************************************************************************


	//**********************************TUTORIAL**********************************
	
	//b*2114722
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_TUT_CAR_EXT")
	AND ARE_STRINGS_EQUAL(sCutsceneHandle, "MPH_Car")
		vehmod = KURUMA
		RETURN TRUE
	ENDIF
	
	
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_TUT_EXT")
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "MPH_Heli")
			vehmod = CARGOBOB
			RETURN TRUE
		ENDIF
		
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "policecar_1")
		OR ARE_STRINGS_EQUAL(sCutsceneHandle, "policecar_2")
		OR ARE_STRINGS_EQUAL(sCutsceneHandle, "policecar_3")
		OR ARE_STRINGS_EQUAL(sCutsceneHandle, "policecar_4")
			vehmod = POLICE3
			RETURN TRUE
		ENDIF
		
		IF ARE_STRINGS_EQUAL(sCutsceneHandle, "riot_1")
			vehmod = RIOT
			RETURN TRUE
		ENDIF
	ENDIF
	
	//****************************************************************************

	RETURN FALSE
ENDFUNC

//Return the cutscene entity with a handle
FUNC NETWORK_INDEX GET_CUTSCENE_NET_ID_WITH_HANDLE(STRING sCutHandle, INT iCutscene, BOOL bEndCut = FALSE)

	NETWORK_INDEX netreturn = NULL
	INT iCutsceneEntityType = -1
	INT iCutsceneEntityIndex = -1
	INT i
	TEXT_LABEL_31 sCutsceneHandle

	//Mocap Cutscene
	FOR i = 0 TO ( FMMC_MAX_CUTSCENE_ENTITIES - 1 )
	
		IF NOT bEndCut
			iCutsceneEntityType = g_FMMC_STRUCT.sMocapCutsceneData[ iCutscene ].sCutsceneEntities[ i ].iType
			iCutsceneEntityIndex = g_FMMC_STRUCT.sMocapCutsceneData[ iCutscene ].sCutsceneEntities[ i ].iIndex
			sCutsceneHandle = g_FMMC_STRUCT.sMocapCutsceneData[ iCutscene ].sCutsceneEntities[ i ].tlCutsceneHandle
		ELSE
			iCutsceneEntityType = g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iType
			iCutsceneEntityIndex = g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[ i ].iIndex
			sCutsceneHandle = g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[ i ].tlCutsceneHandle
		ENDIF
		
		if ARE_STRINGS_EQUAL(sCutsceneHandle, sCutHandle)
		AND iCutsceneEntityType != -1
		AND iCutsceneEntityIndex != -1
			netreturn = GET_CUTSCNE_VISIBLE_NET_ID( iCutsceneEntityType, iCutsceneEntityIndex )
			PRINTLN("[RCC MISSION] GOT A NET ID WITH GIVEN HANDLE ", sCutHandle)
		ENDIF
	ENDFOR

	RETURN netreturn

ENDFUNC

//Check if the cutscene should create the registered entity
FUNC BOOL CUTSCENE_SHOULD_CREATE_REGISTERED_ENTITY(STRING tlHandle, STRING sMocapName)

	IF (ARE_STRINGS_EQUAL(tlHandle, "Money_Launderer") AND ARE_STRINGS_EQUAL(sMocapName, "MPH_PAC_WIT_MCS1"))
	OR (ARE_STRINGS_EQUAL(tlHandle, "Money_Boat") AND ARE_STRINGS_EQUAL(sMocapName, "MPH_PAC_WIT_MCS1"))
	OR (ARE_STRINGS_EQUAL(tlHandle, "MPH_Car") AND ARE_STRINGS_EQUAL(sMocapName, "MPH_TUT_EXT"))
	OR (ARE_STRINGS_EQUAL(tlHandle, "MPH_Heli") AND ARE_STRINGS_EQUAL(sMocapName, "MPH_TUT_EXT"))
	OR (ARE_STRINGS_EQUAL(tlHandle, "Driver_Selection") AND ARE_STRINGS_EQUAL(sMocapName, "MPH_TUT_EXT"))
	OR (ARE_STRINGS_EQUAL(tlHandle, "Rashcovski") AND ARE_STRINGS_EQUAL(sMocapName, "MPH_PRI_FIN_MCS1"))
	OR (ARE_STRINGS_EQUAL(tlHandle, "PLANE") AND ARE_STRINGS_EQUAL(sMocapName, "MPH_PRI_FIN_MCS2"))
	OR (ARE_STRINGS_EQUAL(tlHandle, "PLANE") AND ARE_STRINGS_EQUAL(sMocapName, "MPH_PRI_PLA_EXT"))
	OR (ARE_STRINGS_EQUAL(tlHandle, "technical") AND ARE_STRINGS_EQUAL(sMocapName, "MPH_NAR_FIN_EXT"))
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC STRING GET_PLAYER_SPECIFIC_ANIMATION_NAME_FROM_CREATOR_INT(INT iAnimation, INT iPlayer)
	SWITCH iAnimation
		CASE ci_CS_PS_ANIM_APARTMENT_DOOR_ENTRY
			SWITCH iPlayer
				CASE 0 RETURN "ext_player"	 	BREAK
				CASE 1 RETURN "ext_a"			BREAK
				CASE 2 RETURN "ext_b" 			BREAK
				CASE 3 RETURN "ext_c"			BREAK
			ENDSWITCH
		BREAK
		CASE ci_CS_PS_HINGE_L_EXIT1
			SWITCH iPlayer
				CASE 0 RETURN "player_exit" 	BREAK
				CASE 1 RETURN "player_exit"		BREAK
				CASE 2 RETURN "player_exit" 	BREAK
				CASE 3 RETURN "player_exit"		BREAK
			ENDSWITCH
		BREAK
		CASE ci_CS_PS_HINGE_L_EXIT2
			SWITCH iPlayer
				CASE 0 RETURN "ext_player"	 	BREAK
				CASE 1 RETURN "ext_player"		BREAK
				CASE 2 RETURN "ext_player" 		BREAK
				CASE 3 RETURN "ext_player"		BREAK
			ENDSWITCH
		BREAK
		CASE ci_CS_PS_HINGE_L_STEALTH_EXIT
			SWITCH iPlayer
				CASE 0 RETURN "player_exit"	 	BREAK
				CASE 1 RETURN "player_exit"		BREAK
				CASE 2 RETURN "player_exit" 	BREAK
				CASE 3 RETURN "player_exit"		BREAK
			ENDSWITCH
		BREAK
		CASE ci_CS_PS_HINGE_R_EXIT1
			SWITCH iPlayer
				CASE 0 RETURN "ext_player"	 	BREAK
				CASE 1 RETURN "ext_player"		BREAK
				CASE 2 RETURN "ext_player" 		BREAK
				CASE 3 RETURN "ext_player"		BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_PLAYER_SPECIFIC_ANIMATION_DICT_FROM_CREATOR_INT(INT iAnimation)
	SWITCH iAnimation
		CASE ci_CS_PS_ANIM_APARTMENT_DOOR_ENTRY   	RETURN	"anim@BUILDING_TRANS@HINGE_L"		
		CASE ci_CS_PS_HINGE_L_EXIT1				 	RETURN	"anim@apt_trans@hinge_l_action"
		CASE ci_CS_PS_HINGE_L_EXIT2				 	RETURN	"anim@apt_trans@hinge_l"
		CASE ci_CS_PS_HINGE_L_STEALTH_EXIT		 	RETURN	"anim@apt_trans@hinge_l_stealth"
		CASE ci_CS_PS_HINGE_R_EXIT1				 	RETURN	"anim@apt_trans@hinge_r"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

PROC ASSIGN_PLAYER_SPECIFIC_ANIMATIONS_PED_POOL(PED_INDEX &pedCutscene[], PLAYER_INDEX &playerCutscene[], INT &iPlayer[])
	INT i, iPlayerCount
	FOR i = 0 TO NUM_NETWORK_PLAYERS-1
		IF iPlayerCount < FMMC_MAX_CUTSCENE_PLAYERS
			PARTICIPANT_INDEX piPart = INT_TO_PARTICIPANTINDEX(i)
			
			IF NETWORK_IS_PARTICIPANT_ACTIVE(piPart)
				PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)
				
				IF NETWORK_IS_PLAYER_ACTIVE(piPlayer)
					IF NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(piPlayer)
					AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_ANY_SPECTATOR)
						PED_INDEX pedPlayer = GET_PLAYER_PED(piPlayer)
						
						IF NOT IS_PED_INJURED(pedPlayer)
							pedCutscene[iPlayerCount] = pedPlayer
							playerCutscene[iPlayerCount] = piPlayer
							iPlayer[iPlayerCount] = i
							iPlayerCount++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			BREAKLOOP
		ENDIF
	ENDFOR
ENDPROC

FUNC STRING GET_ANIM_NAME_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE_PLAYER(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType, INT iIndex)
	SWITCH eType
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_CASINO_GRUPPE_SECHS_ENTRY
			SWITCH iIndex
				CASE 0	RETURN "MISSING_DELIVERY_PED1"
				CASE 1	RETURN "MISSING_DELIVERY_PED2"
				CASE 2	RETURN "MISSING_DELIVERY_PED3"
				CASE 3	RETURN "MISSING_DELIVERY_PED4"
			ENDSWITCH
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_CASINO_APARTMENT_TRANS_LEFT 
			SWITCH iIndex
				CASE 0	RETURN "ext_player"
				CASE 1	RETURN "ext_a"
				CASE 2	RETURN "ext_b"
				CASE 3	RETURN "ext_c"
			ENDSWITCH
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_CASINO_APARTMENT_TRANS_RIGHT
			SWITCH iIndex
				CASE 0	RETURN "ext_player"
				CASE 1	RETURN "ext_a"
				CASE 2	RETURN "ext_b"
				CASE 3	RETURN "ext_c"
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC STRING GET_ANIM_NAME_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE_PED(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType, INT iIndex)
	SWITCH eType
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_CASINO_GRUPPE_SECHS_ENTRY
			SWITCH iIndex
				CASE 0 RETURN ""
			ENDSWITCH
		BREAK
	ENDSWITCH	
	RETURN ""
ENDFUNC

FUNC STRING GET_ANIM_NAME_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE_OBJECT(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType, INT iIndex)
	SWITCH eType
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_CASINO_GRUPPE_SECHS_ENTRY
			SWITCH iIndex
				CASE 0 RETURN ""
			ENDSWITCH
		BREAK
	ENDSWITCH	
	RETURN ""
ENDFUNC

FUNC STRING GET_ANIM_NAME_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE_PROP(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType, INT iIndex)
	SWITCH eType
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_CASINO_GRUPPE_SECHS_ENTRY
			SWITCH iIndex
				CASE 0 RETURN ""
			ENDSWITCH
		BREAK
	ENDSWITCH	
	RETURN ""
ENDFUNC

FUNC STRING GET_ANIM_NAME_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE_VEHICLE(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType, INT iIndex)
	SWITCH eType
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_CASINO_GRUPPE_SECHS_ENTRY
			SWITCH iIndex
				CASE 0 RETURN "MISSING_DELIVERY_Stockade"
			ENDSWITCH
		BREAK
	ENDSWITCH	
	RETURN ""
ENDFUNC

FUNC STRING GET_ANIM_NAME_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE_CAM(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType)
	SWITCH eType
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_CASINO_GRUPPE_SECHS_ENTRY 	RETURN "MISSING_DELIVERY_Cam"
	ENDSWITCH	
	RETURN ""
ENDFUNC

FUNC STRING GET_ANIM_DICT_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType)
	SWITCH eType
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_CASINO_GRUPPE_SECHS_ENTRY	RETURN "ANIM@CASINO@ANIMATED_CAMS@"
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_CASINO_APARTMENT_TRANS_LEFT 	RETURN "ANIM@APT_TRANS@HINGE_L"
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_CASINO_APARTMENT_TRANS_RIGHT RETURN "ANIM@APT_TRANS@HINGE_R"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC BOOL SHOULD_WAIT_FOR_SYNC_SCENE_PARTICIPANTS(INT &iPlayers[])

	IF HAS_NET_TIMER_STARTED(tdScriptedCutsceneSyncSceneSafetyTimer)
	AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdScriptedCutsceneSyncSceneSafetyTimer, 6000)
		PRINTLN("[LM][RCC MISSION][ScriptedAnimSyncScene] - SHOULD_WAIT_FOR_SYNC_SCENE_PARTICIPANTS - Safety Timer hit. Bailing.")
		RETURN FALSE
	ENDIF
	
	INT i
	FOR i = 0 TO FMMC_MAX_CUTSCENE_PLAYERS-1
		IF NOT IS_BIT_SET(MC_playerBD[iPlayers[i]].iClientBitSet2, PBBOOL2_STARTED_CUTSCENE)
			IF NOT HAS_NET_TIMER_STARTED(tdScriptedCutsceneSyncSceneSafetyTimer)
				START_NET_TIMER(tdScriptedCutsceneSyncSceneSafetyTimer)
			ENDIF
			PRINTLN("[LM][RCC MISSION][ScriptedAnimSyncScene] - SHOULD_WAIT_FOR_SYNC_SCENE_PARTICIPANTS - Returning TRUE - Waiting for iPlayer: ", iPlayers[i])
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RESET_NET_TIMER(tdScriptedCutsceneSyncSceneSafetyTimer)
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_ALL_SYNC_SCENE_PARTICIPANTS_READY(INT &iPlayers[])

	IF HAS_NET_TIMER_STARTED(tdScriptedCutsceneSyncSceneSafetyTimer2)
	AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdScriptedCutsceneSyncSceneSafetyTimer2, 4000)
		PRINTLN("[LM][RCC MISSION][ScriptedAnimSyncScene] - ARE_ALL_SYNC_SCENE_PARTICIPANTS_READY - Safety Timer hit. Bailing.")
		RETURN TRUE
	ENDIF
	
	INT i
	FOR i = 0 TO FMMC_MAX_CUTSCENE_PLAYERS-1
		IF NOT IS_BIT_SET(MC_playerBD[iPlayers[i]].iClientBitSet4, PBBOOL4_READY_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE)
			IF NOT HAS_NET_TIMER_STARTED(tdScriptedCutsceneSyncSceneSafetyTimer2)
				START_NET_TIMER(tdScriptedCutsceneSyncSceneSafetyTimer2)
			ENDIF
			PRINTLN("[LM][RCC MISSION][ScriptedAnimSyncScene] - ARE_ALL_SYNC_SCENE_PARTICIPANTS_READY - Returning TRUE - Waiting for iPlayer: ", iPlayers[i])
			RETURN FALSE
		ENDIF
	ENDFOR
	
	RESET_NET_TIMER(tdScriptedCutsceneSyncSceneSafetyTimer2)
	
	RETURN TRUE
ENDFUNC

PROC CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE(FMMC_CUTSCENE_SYNC_SCENE_DATA sCutsceneSyncSceneData, INT iShot, INT iCutscene, PED_INDEX &pedCutscene[])
	UNUSED_PARAMETER(iShot)
	PRINTLN("[LM][RCC MISSION][ScriptedAnimSyncScene] - CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Calling to play an Anim Scene for this Scripted Cutscene.")
	
	SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType = sCutsceneSyncSceneData.eType
	VECTOR vCoords = sCutsceneSyncSceneData.vCoord
	VECTOR vRot = <<0.0, 0.0, sCutsceneSyncSceneData.fHeading>>	
	STRING animDict = GET_ANIM_DICT_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(eType)	
	STRING animName
	
	//Horrible hack for telemetry
	IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
	AND eType = SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_CASINO_GRUPPE_SECHS_ENTRY
	AND MC_serverBD_1.iEntranceUsed != 9
		MC_serverBD_1.iEntranceUsed = 9
		PRINTLN("[JS] - CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - MC_serverBD_1.iEntranceUsed (1)= ", MC_serverBD_1.iEntranceUsed)
	ENDIF
	
	PRINTLN("[LM][RCC MISSION][ScriptedAnimSyncScene] - CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - eType: ", ENUM_TO_INT(eType), " vCoords: ", vCoords, " vRot: ", vRot, " animDict: ", animDict)
	
	IK_CONTROL_FLAGS ikFlags
	ikFlags = AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK | AIK_DISABLE_HEAD_IK | AIK_DISABLE_TORSO_IK	| AIK_DISABLE_TORSO_REACT_IK
	
	SYNCED_SCENE_PLAYBACK_FLAGS sceneFlags
	sceneFlags = SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_PRESERVE_VELOCITY | SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_NET_DISREGARD_ATTACHMENT_CHECKS
		
	MC_ServerBD_1.iScriptedCutsceneSyncScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCoords, vRot, EULER_XYZ, FALSE, FALSE)	
	
	INT i
	FOR i = 0 TO FMMC_MAX_CUTSCENE_PLAYERS-1		
		IF IS_PED_INJURED(pedCutscene[i])
		OR NOT DOES_ENTITY_EXIST(pedCutscene[i])
			PRINTLN("[LM][RCC MISSION][ScriptedAnimSyncScene] - CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Player: ", i, " is null.")
			RELOOP
		ENDIF
		
		animName = GET_ANIM_NAME_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE_PLAYER(eType, i)
		IF NOT IS_STRING_NULL_OR_EMPTY(animName)
			SET_ENTITY_LOCALLY_VISIBLE(pedCutscene[i])
			SET_ENTITY_VISIBLE_IN_CUTSCENE(pedCutscene[i], TRUE)				
			PRINTLN("[LM][RCC MISSION][ScriptedAnimSyncScene] - CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - (PLAYER) Setting Player Ped: ", i, " visible")
			
			NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedCutscene[i], MC_ServerBD_1.iScriptedCutsceneSyncScene, animDict, animName, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, sceneFlags, DEFAULT, DEFAULT, ikFlags)	
			PRINTLN("[LM][RCC MISSION][ScriptedAnimSyncScene] - CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - (PLAYER) Adding iPlayer: ", i, " with AnimName: ", animName)
		ELSE
			PRINTLN("[LM][RCC MISSION][ScriptedAnimSyncScene] - CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - (PLAYER) Not Adding iPlayer: ", i, " No anim name exists for them.")
		ENDIF
	ENDFOR
	
	NETWORK_INDEX netid
	PED_INDEX pedIndex
	ENTITY_INDEX entIndex
	
	INT iProp, iObj, iVeh, iPed	
	FOR i = 0 TO FMMC_MAX_CUTSCENE_ENTITIES-1
		IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iType = -1
		OR g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iIndex = -1
			RELOOP
		ENDIF
		
		IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iType = CREATION_TYPE_PROPS			
			entIndex = GET_CUTSCNE_VISIBLE_PROP(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iIndex)
			IF entIndex != NULL
				animName = GET_ANIM_NAME_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE_PROP(eType, iProp)				
				NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(entIndex, MC_ServerBD_1.iScriptedCutsceneSyncScene, animDict, animName, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, sceneFlags)
				PRINTLN("[LM][RCC MISSION][ScriptedAnimSyncScene] - CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - (PROP) Adding iCutsceneEntity: ", i, " iProp: ", iProp, " with AnimName: ", animName)
				iProp++
			ENDIF
		ELSE
			netid = GET_CUTSCNE_VISIBLE_NET_ID(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iType, g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iIndex)
			IF netid != NULL 
				IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iType = CREATION_TYPE_OBJECTS
					entIndex = NET_TO_ENT(netid)					
					animName = GET_ANIM_NAME_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE_OBJECT(eType, iObj)
					NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(entIndex, MC_ServerBD_1.iScriptedCutsceneSyncScene, animDict, animName, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, sceneFlags)
					PRINTLN("[LM][RCC MISSION][ScriptedAnimSyncScene] - CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - (OBJECT) Adding iCutsceneEntity: ", i, " iObj: ", iObj, " with AnimName: ", animName)
					iObj++
				ELIF g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iType = CREATION_TYPE_VEHICLES
					entIndex = NET_TO_ENT(netid)
					animName = GET_ANIM_NAME_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE_VEHICLE(eType, iVeh)
					NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(entIndex, MC_ServerBD_1.iScriptedCutsceneSyncScene, animDict, animName, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, sceneFlags)
					PRINTLN("[LM][RCC MISSION][ScriptedAnimSyncScene] - CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - (VEHICLE) Adding iCutsceneEntity: ", i, " iVeh: ", iVeh, " with AnimName: ", animName)
					iVeh++
				ELIF g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iType = CREATION_TYPE_PEDS
					pedIndex = NET_TO_PED(netid)					
					animName = GET_ANIM_NAME_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE_PED(eType, iPed)
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedIndex, MC_ServerBD_1.iScriptedCutsceneSyncScene, animDict, animName, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, sceneFlags, DEFAULT, DEFAULT, ikFlags)
					PRINTLN("[LM][RCC MISSION][ScriptedAnimSyncScene] - CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - (PED) Adding iCutsceneEntity: ", i, " iPed: ", iPed, " with AnimName: ", animName)
					iPed++
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].iCutsceneBitSet2, ci_CSBS2_UsePlacedCameraInCustomSyncScene)		
		animName = GET_ANIM_NAME_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE_CAM(eType)
		NETWORK_ADD_SYNCHRONISED_SCENE_CAMERA(MC_ServerBD_1.iScriptedCutsceneSyncScene, animDict, animName)	
		NETWORK_FORCE_LOCAL_USE_OF_SYNCED_SCENE_CAMERA(MC_ServerBD_1.iScriptedCutsceneSyncScene)
		PRINTLN("[LM][RCC MISSION][ScriptedAnimSyncScene] - CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - (CAM) Using an Animated Camera. animName: ", animName)
	ELSE
		PRINTLN("[LM][RCC MISSION][ScriptedAnimSyncScene] - CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - (CAM) Not Using an Animated Camera.")
	ENDIF
	
	PRINTLN("[LM][RCC MISSION][ScriptedAnimSyncScene] - CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Sync Scene Creation Finished.")
ENDPROC

PROC PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE(FMMC_CUTSCENE_SYNC_SCENE_DATA sCutsceneSyncSceneData, INT iShot, INT iCutscene, PED_INDEX &pedCutscene[], INT &iPlayers[])
	UNUSED_PARAMETER(iShot)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].iCutsceneBitSet2, ci_CSBS2_UseCustomSyncScene)	
		EXIT
	ENDIF
	
	INT iSyncSceneID = MC_ServerBD_1.iScriptedCutsceneSyncScene
	
	IF iSyncSceneID != -1
		PRINTLN("[LM][RCC MISSION][ScriptedAnimSyncScene] - PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Processing every frame checks.")
		SET_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_READY_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE)
		
		IF MC_playerBD[iLocalPart].iSynchSceneID > -1
			PRINTLN("[LM][RCC MISSION][ScriptedAnimSyncScene] - PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - I am running a sync scene - cleaning up")
			STOP_SYNC_SCENE(MC_playerBD[iPartToUse].iSynchSceneID)
			MC_playerBD[iLocalPart].iSynchSceneID = -1
		ENDIF
		
		IF bIsLocalPlayerHost
		AND ARE_ALL_SYNC_SCENE_PARTICIPANTS_READY(iPlayers)
			IF NOT IS_SYNC_SCENE_RUNNING(iSyncSceneID)
				IF NOT IS_BIT_SET(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_PlayedCutsceneSyncScene)
					PRINTLN("[LM][RCC MISSION][ScriptedAnimSyncScene] - PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Starting the Sync scene!!!")
					NETWORK_START_SYNCHRONISED_SCENE(iSyncSceneID)
					SET_BIT(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_PlayedCutsceneSyncScene)
				ELSE
					PRINTLN("[LM][RCC MISSION][ScriptedAnimSyncScene] - PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Already Played the Sync Scene - Perhaps we're waiting for other clients to finish.")
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_SYNC_SCENE_RUNNING(iSyncSceneID)
			#IF IS_DEBUG_BUILD
			FLOAT fPhase = GET_SYNC_SCENE_PHASE(MC_ServerBD_1.iScriptedCutsceneSyncScene)
			PRINTLN("[LM][RCC MISSION][ScriptedAnimSyncScene] - PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Anim Scene is running, fPhase: ", fPhase)
			#ENDIF
			
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].iCutsceneBitSet2, ci_CSBS2_UsePlacedCameraInCustomSyncScene)
			AND NOT IS_BIT_SET(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_ForceLocalUseofSyncSceneCam)
				PRINTLN("[LM][RCC MISSION][ScriptedAnimSyncScene] - PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Using Sync Scene Camera")
				NETWORK_FORCE_LOCAL_USE_OF_SYNCED_SCENE_CAMERA(iSyncSceneID)
				SET_BIT(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_ForceLocalUseofSyncSceneCam)
			ENDIF
			
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].iCutsceneBitSet2, ci_CSBS2_UsePlacedCameraInCustomSyncScene)
			AND NOT IS_BIT_SET(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_RenderingSyncSceneCam)
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				SET_BIT(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_RenderingSyncSceneCam)
				PRINTLN("[LM][RCC MISSION][ScriptedAnimSyncScene] - PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Turning off Script Cams.")
			ELIF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].iCutsceneBitSet2, ci_CSBS2_UsePlacedCameraInCustomSyncScene)			
			AND NOT IS_BIT_SET(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_RenderingSyncSceneCam)
				SET_CAM_ACTIVE(cutscenecam, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				SET_BIT(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_RenderingSyncSceneCam)
				PRINTLN("[LM][RCC MISSION][ScriptedAnimSyncScene] - PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Turning On Script Cams.")
			ENDIF
		ELSE
			PRINTLN("[LM][RCC MISSION][ScriptedAnimSyncScene] - PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Anim Scene is not running.")
			
			IF IS_BIT_SET(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_RenderingSyncSceneCam)
				SET_CAM_ACTIVE(cutscenecam, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				CLEAR_BIT(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_RenderingSyncSceneCam)
				PRINTLN("[LM][RCC MISSION][ScriptedAnimSyncScene] - PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Anim Scene is no longer running. Turning On Script Cams.")
			ENDIF
		ENDIF
		
		EXIT
	ELIF IS_BIT_SET(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_RenderingSyncSceneCam)
		SET_CAM_ACTIVE(cutscenecam, TRUE)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
		CLEAR_BIT(iScriptedCutsceneSyncSceneBS, ciScriptedCutsceneBitset_RenderingSyncSceneCam)
		PRINTLN("[LM][RCC MISSION][ScriptedAnimSyncScene] - PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE - Anim Scene is no longer running. Turning On Script Cams (2).")
	ENDIF
	
	IF bIsLocalPlayerHost
		CREATE_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE(sCutsceneSyncSceneData, iShot, iCutscene, pedCutscene)
	ENDIF
ENDPROC

FUNC BOOL PROCESS_PRE_LOAD_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE(FMMC_CUTSCENE_SYNC_SCENE_DATA sCutsceneSyncSceneData, INT iShot, INT iCutscene, PED_INDEX &pedCutscene[], INT &iPlayers[])	
	PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE(sCutsceneSyncSceneData, iShot, iCutscene, pedCutscene, iPlayers)
	IF IS_SYNC_SCENE_RUNNING(MC_ServerBD_1.iScriptedCutsceneSyncScene) 
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PROCESS_PLAY_PLAYER_SPECIFIC_ANIMATIONS_FOR_SCENE(FMMC_CUTSCENE_PLAYER_SPECIFIC_DATA	&sPlayerSpecificData[], INT iShot, PED_INDEX &pedCutscene[])
	
	STRING animDict, animName
	INT i
	FOR i = 0 TO FMMC_MAX_CUTSCENE_PLAYERS-1
	
		// Validity Checks
		IF IS_PED_INJURED(pedCutscene[i])
		OR NOT DOES_ENTITY_EXIST(pedCutscene[i])
			PRINTLN("[LM][RCC MISSION] - PROCESS_PLAY_PLAYER_SPECIFIC_ANIMATIONS_FOR_SCENE - iPedCutscene: ", i, " is null.")
			RELOOP
		ENDIF
		
		IF pedCutscene[i] != localPlayerPed
		// Or NOT network has control of entity if we ever use this for mocap cutscenes would be better.
			PRINTLN("[LM][RCC MISSION] - PROCESS_PLAY_PLAYER_SPECIFIC_ANIMATIONS_FOR_SCENE - iPedCutscene: ", i, " is not ME!")
			RELOOP
		ENDIF
		
		// Load up
		animDict = GET_PLAYER_SPECIFIC_ANIMATION_DICT_FROM_CREATOR_INT(sPlayerSpecificData[i].iAnimation)	
		IF NOT IS_STRING_NULL_OR_EMPTY(animDict)
			REQUEST_ANIM_DICT(animDict)
			IF NOT HAS_ANIM_DICT_LOADED(animDict)
				PRINTLN("[LM][RCC MISSION] - PROCESS_PLAY_PLAYER_SPECIFIC_ANIMATIONS_FOR_SCENE - iPedCutscene: ", i, "Waiting for AnimDict: ", animDict, " to load.")
				RELOOP
			ENDIF
		ELSE
			PRINTLN("[LM][RCC MISSION] - PROCESS_PLAY_PLAYER_SPECIFIC_ANIMATIONS_FOR_SCENE - iPedCutscene: ", i, "AnimDict Has not been set up. Is null.")
			RELOOP
		ENDIF
		
		// Ready to play!
		IF sPlayerSpecificData[i].iPlayAnimOnShot = iShot
			animName = GET_PLAYER_SPECIFIC_ANIMATION_NAME_FROM_CREATOR_INT(sPlayerSpecificData[i].iAnimation, i)
			
			IF NOT IS_STRING_NULL_OR_EMPTY(animName)
				IF NOT IS_ENTITY_PLAYING_ANIM(pedCutscene[i], animDict, animName)
				AND GET_SCRIPT_TASK_STATUS(pedCutscene[i], SCRIPT_TASK_PLAY_ANIM) != WAITING_TO_START_TASK
				AND GET_SCRIPT_TASK_STATUS(pedCutscene[i], SCRIPT_TASK_PLAY_ANIM) != PERFORMING_TASK
					PRINTLN("[LM][RCC MISSION] - PROCESS_PLAY_PLAYER_SPECIFIC_ANIMATIONS_FOR_SCENE - iPedCutscene: ", i, "Playing animation: ", animName, " from animdict: ", animDict)
					CLEAR_PED_TASKS_IMMEDIATELY(pedCutscene[i])	
					ANIMATION_FLAGS animFlags = AF_NOT_INTERRUPTABLE | AF_FORCE_START | AF_USE_KINEMATIC_PHYSICS
					TASK_PLAY_ANIM(pedCutscene[i], animDict, animName, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, animFlags)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedCutscene[i])
					SET_BIT(iLocalBoolCheck32, LBOOL32_SCRIPTED_CUTSCENE_PLAYING_ANIM)
				ENDIF
			ENDIF
		ELIF sPlayerSpecificData[i].iPlayAnimOnShot > -1
			PRINTLN("[LM][RCC MISSION] - PROCESS_PLAY_PLAYER_SPECIFIC_ANIMATIONS_FOR_SCENE - iPedCutscene: ", i, " is waiting for shot: ", sPlayerSpecificData[i].iPlayAnimOnShot, " we are currently on: ", iCamShot)
		ENDIF
	ENDFOR
	
	
ENDPROC

//Manually register specific entities for cutscenes that haven't been passed in from the creator
//e.g. world created object
PROC MANUALLY_REGISTER_SPECIFIC_ENTITIES(STRING sMocapName, FMMC_CUTSCENE_TYPE eCutType)
	
	IF ARE_STRINGS_EQUAL(sMocapName, "MPH_HUM_DEL_EXT")
	AND IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_CUTSCENE_ON_FOOT_VISIBLE_CHECK)
		REGISTER_ENTITY_FOR_CUTSCENE(NULL, "MPH_Boxville", CU_DONT_ANIMATE_ENTITY,DUMMY_MODEL_FOR_SCRIPT,CEO_IGNORE_MODEL_NAME)
		PRINTLN("[RCC MISSION] BOXVILLE SET INVISIBLE - NO ENTITIES")
	ENDIF

	//Biolab Finale
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_HUM_FIN_MCS1")
	AND CAN_REGISTER_MISSION_OBJECTS(1)
		//Set chair not from script into cutscene
		OBJECT_INDEX tempObj
			
		tempObj = GET_CLOSEST_OBJECT_OF_TYPE(<<3536.0427, 3659.6311, 27.1219>>, 10.0, V_CORP_OFFCHAIR, TRUE, FALSE, FALSE)
		
		IF tempObj != NULL
			PRINTLN("[RCC MISSION] REGISTER_ENTITY_FOR_CUTSCENE MPH_HUM_FIN_MCS1 CHAIR IS REGISTERED")
			//Register the chair
			REGISTER_ENTITY_FOR_CUTSCENE(tempObj, "Kicked_chair", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, V_CORP_OFFCHAIR)
		ELSE
			PRINTLN("[RCC MISSION] MPH_HUM_FIN_MCS1 KICKED CHAIR IS NULL!")
		ENDIF
	ENDIF
	
	TEXT_LABEL_23 tl23_a
	TEXT_LABEL_23 tl23_b
	BOOL bStrandMission
	INT i = 0

	IF ARE_STRINGS_EQUAL(sMocapName, "HS3F_MUL_RP1")
		PRINTLN("[RCC MISSION] HS3F_MUL_RP1 Running t his cutscene which requires specific entity registration (Ropes are not entities...)")
		INT iNumOfPlayers = GET_MOCAP_NUMBER_OF_PLAYERS(sMocapName)
		INT iCutsceneTeam
		IF iScriptedCutsceneTeam != -1
		AND eCutType != FMMCCUT_ENDMOCAP
			iCutsceneTeam = iScriptedCutsceneTeam
		ELSE
			iCutsceneTeam = MC_PlayerBD[iPartToUse].iTeam
		ENDIF
		
		bStrandMission = IS_A_STRAND_MISSION_BEING_INITIALISED()
		
		FOR i = 0 TO iNumOfPlayers-1
			IF MC_serverBD.piCutscenePlayerIDs[iCutsceneTeam][i] = INVALID_PLAYER_INDEX()
				tl23_a = "Player_Rope_"
				tl23_a += i+1
				tl23_b = "Player_Pulley_"
				tl23_b += i+1
				IF bStrandMission
					PASS_OVER_CUT_SCENE_OBJECT(NULL, tl23_a)
					PASS_OVER_CUT_SCENE_OBJECT(NULL, tl23_b)
				ELSE
					PRINTLN("[RCC MISSION] REGISTER_ENTITY_FOR_CUTSCENE Registering: ", tl23_a, " With NULL...")				
					REGISTER_ENTITY_FOR_CUTSCENE(NULL, tl23_a, CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
					PRINTLN("[RCC MISSION] REGISTER_ENTITY_FOR_CUTSCENE Registering: ", tl23_b, " With NULL...")
					REGISTER_ENTITY_FOR_CUTSCENE(NULL, tl23_b, CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	IF ARE_STRINGS_EQUAL(sMocapName, "HS3F_DIR_ENT")
		INT iNumOfPlayers = GET_MOCAP_NUMBER_OF_PLAYERS(sMocapName)
		INT iCutsceneTeam
		IF iScriptedCutsceneTeam != -1
		AND eCutType != FMMCCUT_ENDMOCAP
			iCutsceneTeam = iScriptedCutsceneTeam
		ELSE
			iCutsceneTeam = MC_PlayerBD[iPartToUse].iTeam
		ENDIF
		
		bStrandMission = IS_A_STRAND_MISSION_BEING_INITIALISED()

		FOR i = 0 TO iNumOfPlayers-1
			IF MC_serverBD.piCutscenePlayerIDs[iCutsceneTeam][i] = INVALID_PLAYER_INDEX()
				tl23_a = "Player_SMG_"
				tl23_a += i+1
				tl23_b = "Player_Mag_"
				tl23_b += i+1
				IF bStrandMission
					PASS_OVER_CUT_SCENE_OBJECT(NULL, tl23_a)
					PASS_OVER_CUT_SCENE_OBJECT(NULL, tl23_b)
				ELSE
					PRINTLN("[RCC MISSION] REGISTER_ENTITY_FOR_CUTSCENE Registering: ", tl23_a, " With NULL...")				
					REGISTER_ENTITY_FOR_CUTSCENE(NULL, tl23_a, CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
					PRINTLN("[RCC MISSION] REGISTER_ENTITY_FOR_CUTSCENE Registering: ", tl23_b, " With NULL...")
					REGISTER_ENTITY_FOR_CUTSCENE(NULL, tl23_b, CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

///PURPOSE: This function loops through all the entities tagged in the creator and registers them to the current cutscene
PROC REGISTER_CUTSCENE_ENTITIES(INT iCutscene, BOOL bshow, FMMC_CUTSCENE_TYPE eCutType)

	INT i
	NETWORK_INDEX netid = NULL
	ENTITY_INDEX EntID = NULL
	BOOL bStrand = IS_A_STRAND_MISSION_BEING_INITIALISED()

	IF iCutscene = -1
	AND eCutType != FMMCCUT_ENDMOCAP
		PRINTLN("[RCC MISSION] REGISTER_CUTSCENE_ENTITIES - iCutscene: ", iCutscene, " eCutType: ", GET_FMMC_CUTSCENE_TYPE_NAME(eCutType), " - invalid iCutscene/eCutType combination")
		EXIT
	ENDIF
	
	IF eCutType = FMMCCUT_SCRIPTED
		
		//Scripted Cutscene
		FOR i = 0 TO FMMC_MAX_CUTSCENE_ENTITIES-1
			
			IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iType =-1
			OR g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iIndex = -1
				RELOOP
			ENDIF
			
			IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iType = CREATION_TYPE_PROPS
				EntID = GET_CUTSCNE_VISIBLE_PROP(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iIndex)
				IF EntID != NULL
					//SET_ENTITY_VISIBLE_IN_CUTSCENE(EntID,bshow,FALSE)
					SET_ENTITY_VISIBLE(EntID, TRUE)
				ENDIF
			ELSE
				netid = GET_CUTSCNE_VISIBLE_NET_ID(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iType,g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iIndex)
				PRINTLN("[RCC MISSION] REGISTER_CUTSCENE_ENTITIES - GET_CUTSCNE_VISIBLE_NET_ID has returned a thing: NULL = ",netid = NULL,", IS_CUTSCENE_PLAYING = ",IS_CUTSCENE_PLAYING())
				IF netid != NULL AND NOT IS_CUTSCENE_PLAYING()
					PRINTLN("[RCC MISSION] REGISTER_CUTSCENE_ENTITIES - calling SET_NETWORK_ID_VISIBLE_IN_CUTSCENE with ",bShow)
					SET_NETWORK_ID_VISIBLE_IN_CUTSCENE(netid,bshow,FALSE)
				ENDIF
			ENDIF
			
		ENDFOR
	
	ELIF eCutType = FMMCCUT_MOCAP
	OR eCutType = FMMCCUT_ENDMOCAP

		#IF IS_DEBUG_BUILD
		IF IS_A_STRAND_MISSION_BEING_INITIALISED()
			PRINTLN("[MSRAND] - REGISTER_CUTSCENE_ENTITIES")
		ENDIF
		#ENDIF
		
		INT iCutsceneEntityType = -1
		INT iCutsceneEntityIndex = -1
		TEXT_LABEL_23 sCutsceneHandle = ""
		
		//Mocap Cutscene
		FOR i = 0 TO FMMC_MAX_CUTSCENE_ENTITIES - 1
			
			// end mocap
			IF eCutType = FMMCCUT_ENDMOCAP
			
				iCutsceneEntityType = g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iType
				iCutsceneEntityIndex = g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iIndex
				sCutsceneHandle = g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].tlCutsceneHandle

				#IF IS_DEBUG_BUILD
				IF IS_A_STRAND_MISSION_BEING_INITIALISED()
					PRINTLN("[MSRAND] - REGISTER_CUTSCENE_ENTITIES - iCutsceneEntityType  = ", iCutsceneEntityType)
					PRINTLN("[MSRAND] - REGISTER_CUTSCENE_ENTITIES - iCutsceneEntityIndex = ", iCutsceneEntityIndex)
					PRINTLN("[MSRAND] - REGISTER_CUTSCENE_ENTITIES - sCutsceneHandle      = ", sCutsceneHandle)
				ENDIF
				#ENDIF
			// mid mocap
			ELIF eCutType = FMMCCUT_MOCAP
			
				iCutsceneEntityType = g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].sCutsceneEntities[i].iType
				iCutsceneEntityIndex = g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].sCutsceneEntities[i].iIndex
				sCutsceneHandle = g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].sCutsceneEntities[i].tlCutsceneHandle
				
			ENDIF
			
			IF iCutsceneEntityType = -1
			OR iCutsceneEntityIndex = -1
				PRINTLN("[RCC MISSION] REGISTER_CUTSCENE_ENTITIES - i: ", i, " iCutsceneEntityType: ", iCutsceneEntityType, " iCutsceneEntityIndex: ", iCutsceneEntityIndex, " invalid type/index, reloop")
				RELOOP
			ENDIF
			
			IF IS_STRING_NULL_OR_EMPTY(sCutsceneHandle)
				PRINTLN("REGISTER_CUTSCENE_ENTITIES - i: ", i, " iCutscene: ", iCutscene, ", eCutType: ", GET_FMMC_CUTSCENE_TYPE_NAME(eCutType), ", cutscene entity: ", i, " - sCutsceneHandle = NULL/EMPTY, please set a valid handle in the creator")
				RELOOP
			ELSE
				CUTSCENE_HANDLE_SEARCH_RESULT eDoesCutHandleExit = DOES_CUTSCENE_HANDLE_EXIST(sCutsceneHandle)
				IF eDoesCutHandleExit != CHSR_HANDLE_EXISTS
					PRINTLN("REGISTER_CUTSCENE_ENTITIES - i: ", i, " iCutscene: ", iCutscene, ", eCutType: ", GET_FMMC_CUTSCENE_TYPE_NAME(eCutType), ", cutscene entity: ", i, " - sCutsceneHandle: ", sCutsceneHandle, " does not exist for cutscene, please set a valid handle in the creator")
					RELOOP
				ENDIF
			ENDIF
			
			IF iCutsceneEntityType = CREATION_TYPE_PROPS
				IF bStrand
					EntID = GET_CUTSCNE_VISIBLE_PROP( iCutsceneEntityIndex )
					PASS_OVER_CUT_SCENE_OBJECT(GET_OBJECT_INDEX_FROM_ENTITY_INDEX(EntID), sCutsceneHandle)
				ELSE
				
					PRINTLN("REGISTER_CUTSCENE_ENTITIES - i: ", i, " iCutscene: ", iCutscene, ", eCutType: ", GET_FMMC_CUTSCENE_TYPE_NAME(eCutType), ", cutscene entity: ", i, " - sCutsceneHandle: ", sCutsceneHandle, " - PROP")
					
					IF ARE_STRINGS_EQUAL(sCutsceneHandle, "Shack_Door")
					AND ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PAC_WIT_MCS1")
						EntID = GET_CUTSCNE_VISIBLE_PROP( iCutsceneEntityIndex )
						IF EntID != NULL
						AND DOES_ENTITY_EXIST( EntID )
							REGISTER_ENTITY_FOR_CUTSCENE( EntID, sCutsceneHandle, CU_ANIMATE_EXISTING_SCRIPT_ENTITY, GET_ENTITY_MODEL( EntID ) )
						ENDIF
					ELSE
						EntID = GET_CUTSCNE_VISIBLE_PROP( iCutsceneEntityIndex )
						IF EntID != NULL
						AND DOES_ENTITY_EXIST( EntID )
							SET_ENTITY_VISIBLE(EntID, TRUE)
							//SET_ENTITY_VISIBLE_IN_CUTSCENE( EntID, bshow, FALSE )
						ENDIF
					ENDIF
				ENDIF
			ELSE
				netid = GET_CUTSCNE_VISIBLE_NET_ID( iCutsceneEntityType, iCutsceneEntityIndex )
				IF netid = NULL
					PRINTLN("REGISTER_CUTSCENE_ENTITIES - i: ", i, " iCutscene: ", iCutscene, ", eCutType: ", GET_FMMC_CUTSCENE_TYPE_NAME(eCutType), ", cutscene entity: ", i, " - sCutsceneHandle: ", sCutsceneHandle, " - GET_CUTSCNE_VISIBLE_NET_ID return NULL!")
				ENDIF
							
				IF netid != NULL
				AND NETWORK_DOES_NETWORK_ID_EXIST(netid)
					
					//Register the cutscene entity for the cutscene if we have a handle
					IF NOT IS_STRING_NULL_OR_EMPTY( sCutsceneHandle )
						IF NOT IS_CUTSCENE_PLAYING()
							IF NOT CUTSCENE_SHOULD_CREATE_REGISTERED_ENTITY(sCutsceneHandle, sMocapCutscene)
								SET_NETWORK_ID_VISIBLE_IN_CUTSCENE( netid, bshow, bshow )
							ELSE
								SET_NETWORK_ID_VISIBLE_IN_CUTSCENE( netid, FALSE, FALSE)
								IF NETWORK_HAS_CONTROL_OF_ENTITY(NET_TO_ENT(netid))
									SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_ENT(netid), FALSE) //b*2205307
									IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PRI_PLA_EXT") 
										IF ARE_STRINGS_EQUAL(sCutsceneHandle, "PLANE")
											//url:bugstar:4263263 - Prison Break - Pilot - Players are seeing "You alerted an agent" at the end of the mission set up cut scene which fails the heist. 
											SET_NETWORK_ID_CAN_MIGRATE(netid, FALSE)
											PRINTLN("[JS] REGISTER_CUTSCENE_ENTITIES - setting plane unable to migrate!")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						EntID = NET_TO_ENT( netid )
						
						MODEL_NAMES tempModel = GET_ENTITY_MODEL( EntID )
						
						PRINTLN("REGISTER_CUTSCENE_ENTITIES - i: ", i, " sCutsceneHandle: ", sCutsceneHandle," iCutsceneEntityType: ", iCutsceneEntityType, " - valid handle, attempting to register")
						
						IF NOT bStrand

							//Check vehicle is not dead and give it health
							IF DOES_ENTITY_EXIST( EntID )
							AND NETWORK_HAS_CONTROL_OF_ENTITY(EntID)

								IF IS_ENTITY_A_VEHICLE(EntID)
								
									PRINTLN("REGISTER_CUTSCENE_ENTITIES - i: ", i, " sCutsceneHandle: ", sCutsceneHandle," iCutsceneEntityType: ", iCutsceneEntityType, " - valid handle, IS A VEHICLE")
									
									VEHICLE_INDEX vehID
									vehID = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(EntID)	

									IF NOT IS_VEHICLE_DRIVEABLE(vehID)
									OR IS_ENTITY_DEAD(vehID)
										PRINTLN("REGISTER_CUTSCENE_ENTITIES - i: ", i, " sCutsceneHandle: ", sCutsceneHandle," iCutsceneEntityType: ", iCutsceneEntityType, " - valid handle, VEHICLE not drivable")
										SET_ENTITY_HEALTH(vehID, 1000)
									ENDIF
										
									IF IS_ENTITY_ON_FIRE(vehID)
										
										PRINTLN("REGISTER_CUTSCENE_ENTITIES - i: ", i, " sCutsceneHandle: ", sCutsceneHandle," iCutsceneEntityType: ", iCutsceneEntityType, " - valid handle, VEHICLE on FIRE")
										STOP_VEHICLE_FIRE(vehID)
									
										IF ARE_STRINGS_EQUAL(sCutsceneHandle, "Mule_01")
										AND ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_NAR_FIN_EXT")
											SET_VEHICLE_FIXED(vehID)
										ENDIF
									ENDIF
									
									IF GET_VEHICLE_ENGINE_HEALTH(vehID) < 200
										PRINTLN("REGISTER_CUTSCENE_ENTITIES - i: ", i, " sCutsceneHandle: ", sCutsceneHandle," iCutsceneEntityType: ", iCutsceneEntityType, " - valid handle, Engine health is low (<200)")
										SET_ENTITY_HEALTH(vehID, 1000)
									ENDIF
									
									IF ARE_STRINGS_EQUAL(sCutsceneHandle, "Benson_2")
									AND ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_NAR_WEE_EXT")
										CPRINTLN( DEBUG_CONTROLLER,"[RCC MISSION] MPH_NAR_WEE_EXT - FIXING CUTSCENE VEHICLE BENSON_2")
										SET_ENTITY_HEALTH(vehID, 1000)
										SET_VEHICLE_FIXED(vehID)
									ENDIF
									
									IF (ARE_STRINGS_EQUAL(sCutsceneHandle, "Gang_Van")
									OR ARE_STRINGS_EQUAL(sCutsceneHandle, "Gang_Van_2"))
									AND ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_NAR_BIK_EXT")
										SET_VEHICLE_FIXED(vehID)
									ENDIF
									
								ELIF IS_ENTITY_DEAD(EntID)
								
									PRINTLN("REGISTER_CUTSCENE_ENTITIES - i: ", i, " sCutsceneHandle: ", sCutsceneHandle," iCutsceneEntityType: ", iCutsceneEntityType, " - valid handle, entity is DEAD")
									SET_ENTITY_HEALTH(EntID, 200)
									
								ENDIF
							
							ENDIF
							
							IF CUTSCENE_SHOULD_CREATE_REGISTERED_ENTITY(sCutsceneHandle, sMocapCutscene)
								
								//Let the cutscene create the entity and set the passed in entity to not be visible
								PRINTLN("REGISTER_CUTSCENE_ENTITIES - i: ", i, " sCutsceneHandle: ", sCutsceneHandle," iCutsceneEntityType: ", iCutsceneEntityType, " - valid handle, CUTSCENE_SHOULD_CREATE_REGISTERED_ENTITY")
								SET_ENTITY_VISIBLE_IN_CUTSCENE( EntID, FALSE)	
								
							ELIF ARE_STRINGS_EQUAL(sCutsceneHandle, "PHANTOM")
							AND ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_NAR_MET_EXT")
							
								IF iCutsceneEntityIndex = GET_CLOSEST_PHANTOM_FOR_METH_END_CUTSCENE()
									PRINTLN("REGISTER_CUTSCENE_ENTITIES - i: ", i, " sCutsceneHandle: ", sCutsceneHandle," iCutsceneEntityType: ", iCutsceneEntityType, " - valid handle, PHANTOM SPECIAL CASE")
									REGISTER_ENTITY_FOR_CUTSCENE( EntID, sCutsceneHandle, CU_ANIMATE_EXISTING_SCRIPT_ENTITY, tempModel )
								ENDIF
								
							ELIF ARE_STRINGS_EQUAL(sCutsceneHandle, "Valkyrie")
							AND ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_HUM_FIN_EXT")
								PRINTLN("[RCC MISSION] VALKYRIE SPECIAL CASE")
								
								VEHICLE_INDEX vehHeli
								
								IF IS_ENTITY_A_VEHICLE(EntID)
									vehHeli = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(EntID)
								ENDIF
								
								//Set the existing network heli engine off before cutscene creates local heli
								IF NETWORK_HAS_CONTROL_OF_ENTITY(vehHeli)
									SET_VEHICLE_ENGINE_ON(vehHeli, FALSE, TRUE)
									SET_HELI_BLADES_SPEED(vehHeli, 0.0)
								ENDIF

								//Have cutscene create new local heli
								REGISTER_ENTITY_FOR_CUTSCENE( NULL, sCutsceneHandle, CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, tempModel )
								
								//Have the passed in network id heli to not be visible
								SET_NETWORK_ID_VISIBLE_IN_CUTSCENE( netid, FALSE, FALSE )
							ELSE
								PRINTLN("REGISTER_CUTSCENE_ENTITIES - i: ", i, " sCutsceneHandle: ", sCutsceneHandle," iCutsceneEntityType: ", iCutsceneEntityType, " - valid handle, REGISTER_ENTITY_FOR_CUTSCENE")
								PRINTLN("REGISTER_CUTSCENE_ENTITIES - REGISTER_ENTITY_FOR_CUTSCENE( EntID, ", sCutsceneHandle, ", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, tempModel )")
								
								// hide weapons for placed peds.
								/*IF iCutsceneEntityType = CREATION_TYPE_PEDS
									OBJECT_INDEX entWeapon
									entWeapon = GET_WEAPON_OBJECT_FROM_PED(EntID, FALSE)
									SET_ENTITY_VISIBLE_IN_CUTSCENE(EntID, FALSE, FALSE)
								ENDIF*/
								
								REGISTER_ENTITY_FOR_CUTSCENE(EntID, sCutsceneHandle, CU_ANIMATE_EXISTING_SCRIPT_ENTITY, tempModel )
							ENDIF
							
							
							//obtain the plane vehicle index for the cutscene mph_pri_pla_ext mission
							if are_strings_equal(sMocapCutscene, "mph_pri_pla_ext") 
								if are_strings_equal(sCutsceneHandle, "PLANE") 
									if network_has_control_of_entity(EntID)
									
										if does_entity_exist(EntID)
											if is_entity_a_vehicle(EntID)
				
												prison_break_plane = get_vehicle_index_from_entity_index(entid)
									
												if is_this_model_a_plane(get_entity_model(prison_break_plane))
													
													IF NETWORK_HAS_CONTROL_OF_ENTITY(prison_break_plane)
														SET_VEHICLE_ENGINE_ON(prison_break_plane, FALSE, TRUE)
													ENDIF
													
													control_landing_gear(prison_break_plane, lgc_deploy_instant)
												
													println("rcc mission] [ped cut] * register_entity_for_cutscene mph_pri_pla_ext plane obtained")
													
												else 
												
													println("rcc mission] [ped cut] * register_entity_for_cutscene mph_pri_pla_ext not a plane")
													
												endif 

											endif 
										endif 
									endif 
								endif 
							endif 
							
						ELSE
						
							PRINTLN("REGISTER_CUTSCENE_ENTITIES - i: ", i, " sCutsceneHandle: ", sCutsceneHandle," iCutsceneEntityType: ", iCutsceneEntityType, " - valid handle - strand, doing PASS_OVER_CUT_SCENE_")

							IF iCutsceneEntityType = CREATION_TYPE_PEDS
								PASS_OVER_CUT_SCENE_PED(NET_TO_PED(netid), sCutsceneHandle)
							ELIF iCutsceneEntityType = CREATION_TYPE_VEHICLES
								PASS_OVER_CUT_SCENE_VEHICLE(NET_TO_VEH(netid), sCutsceneHandle)
							ELIF iCutsceneEntityType = CREATION_TYPE_OBJECTS
							OR iCutsceneEntityType = CREATION_TYPE_PROPS
								PASS_OVER_CUT_SCENE_OBJECT(NET_TO_OBJ(netid), sCutsceneHandle)
							ENDIF
							
						ENDIF
						//SET_ENTITY_VISIBLE_IN_CUTSCENE(EntID,bshow,FALSE)
					ELSE
					
						//Show entities that haven't been registered with a handle
						IF NOT bStrand
							IF NOT IS_CUTSCENE_PLAYING()
								SET_NETWORK_ID_VISIBLE_IN_CUTSCENE( netid, bshow, bshow )
								PRINTLN("REGISTER_CUTSCENE_ENTITIES - i: ", i, " sCutsceneHandle: ", sCutsceneHandle," iCutsceneEntityType: ", iCutsceneEntityType, " - invalid handle - SET_NETWORK_ID_VISIBLE_IN_CUTSCENE")
							ENDIF
						ELSE
							PRINTLN("REGISTER_CUTSCENE_ENTITIES - i: ", i, " sCutsceneHandle: ", sCutsceneHandle," iCutsceneEntityType: ", iCutsceneEntityType, " - invalid handle - PASS_OVER_CUT_SCENE_ ")
							IF iCutsceneEntityType = CREATION_TYPE_PEDS
								PASS_OVER_CUT_SCENE_PED(NET_TO_PED(netid), sCutsceneHandle)
							ELIF iCutsceneEntityType = CREATION_TYPE_VEHICLES
								PASS_OVER_CUT_SCENE_VEHICLE(NET_TO_VEH(netid), sCutsceneHandle)
							ELIF iCutsceneEntityType = CREATION_TYPE_OBJECTS
							OR  iCutsceneEntityType = CREATION_TYPE_PROPS
								PASS_OVER_CUT_SCENE_OBJECT(NET_TO_OBJ(netid), sCutsceneHandle)
							ENDIF
						ENDIF
	
					ENDIF
				ELIF iCutsceneEntityType = CREATION_TYPE_VEHICLES
				
					MODEL_NAMES vehMod = DUMMY_MODEL_FOR_SCRIPT
				
					//Fail safe for b*2114722 when cutscene has finished for one player before other has started
					//Also if vehicle is undriveable this will let the cutscene create the vehicle for people who aren't owners
					IF CHECK_TO_ADD_FAIL_SAFE_VEHICLE( sCutsceneHandle, vehMod )

						PRINTLN("REGISTER_CUTSCENE_ENTITIES - i: ", i, " sCutsceneHandle: ", sCutsceneHandle," iCutsceneEntityType: ", iCutsceneEntityType, " - entity does not exist - CHECK_TO_ADD_FAIL_SAFE_VEHICLE = TRUE")
						
					ELIF ARE_STRINGS_EQUAL( sCutsceneHandle, "PHANTOM" )
					AND ARE_STRINGS_EQUAL( sMocapCutscene, "MPH_NAR_MET_EXT" )
						
						//Do nothing already will have been registered above with GET_CLOSEST_PHANTOM_FOR_METH_END_CUTSCENE()
						//Has 3 in list but may not have NETID here if left behind
						PRINTLN("REGISTER_CUTSCENE_ENTITIES - i: ", i, " sCutsceneHandle: ", sCutsceneHandle," iCutsceneEntityType: ", iCutsceneEntityType, " - entity does not exist - MPH_NAR_MET_EXT - HIT PHANTOM FAIL SAFE DOESNT HAVE NETWORK ID")
						
					ELSE
						IF IS_A_STRAND_MISSION_BEING_INITIALISED()
							PRINTLN("REGISTER_CUTSCENE_ENTITIES - i: ", i, " sCutsceneHandle: ", sCutsceneHandle," iCutsceneEntityType: ", iCutsceneEntityType, " - entity does not exist - Is strand, PASS_OVER_CUT_SCENE_VEHICLE = NULL")
							PASS_OVER_CUT_SCENE_VEHICLE(NULL, sCutsceneHandle) 
						ELSE
							//THIS WILL NOT HAVE THE ENTITY IN THE CUTSCENE
							//IF REQUIRED ADD CUTSCENE TO LIST IN CHECK_TO_ADD_FAIL_SAFE_VEHICLE TO AVOID THIS
							REGISTER_ENTITY_FOR_CUTSCENE(NULL, sCutsceneHandle, CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME )
							PRINTLN("REGISTER_CUTSCENE_ENTITIES - i: ", i, " sCutsceneHandle: ", sCutsceneHandle," iCutsceneEntityType: ", iCutsceneEntityType, " - entity does not exist - REGISTER_ENTITY_FOR_CUTSCENE = NULL")
							PRINTLN("REGISTER_CUTSCENE_ENTITIES - REGISTER_ENTITY_FOR_CUTSCENE(NULL, ", sCutsceneHandle, ", CU_DONT_ANIMATE_ENTITY,DUMMY_MODEL_FOR_SCRIPT,CEO_IGNORE_MODEL_NAME)")
						ENDIF
					ENDIF
					
				ENDIF // This is checking the specific cutscene entity type
			ENDIF // This is checking the cutscene entity type
		
		ENDFOR // For every cutscene entity
	ENDIF
	
ENDPROC

FUNC BOOL ARE_TEAM_CUTSCENE_PLAYERS_SELECTED(INT iteam)


	SWITCH iteam
		CASE 0
			IF IS_BIT_SET(MC_serverBD.iServerBitSet,SBBOOL_CUTSCENE_TEAM0_PLAYERS_SELECTED)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 1
			IF IS_BIT_SET(MC_serverBD.iServerBitSet,SBBOOL_CUTSCENE_TEAM1_PLAYERS_SELECTED)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 2
			IF IS_BIT_SET(MC_serverBD.iServerBitSet,SBBOOL_CUTSCENE_TEAM2_PLAYERS_SELECTED)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 3
			IF IS_BIT_SET(MC_serverBD.iServerBitSet,SBBOOL_CUTSCENE_TEAM3_PLAYERS_SELECTED)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

//==============================================
//		   FEMALE PED FEET FOR MOCAPS
//==============================================

//Store the cutscene peds feet
FUNC BOOL STORE_FEMALE_PED_FEET(PED_INDEX pedID)

	INT iTempDraw
	INT iTempTex

	// If we are female, reset to no heels
	IF GET_ENTITY_MODEL(pedID) = MP_F_FREEMODE_01
		IF DOES_ENTITY_EXIST(pedID)
		AND NOT IS_ENTITY_DEAD(pedID)
			if iPedFeetDrawable = -1
			AND iPedFeetTexture = -1
				iTempDraw = GET_PED_DRAWABLE_VARIATION(pedID, PED_COMP_FEET) 
				iTempTex = GET_PED_TEXTURE_VARIATION(pedID, PED_COMP_FEET)
		
				iPedFeetDrawable = iTempDraw
				iPedFeetTexture = iTempTex
				
				#IF IS_DEBUG_BUILD
					PRINTLN("STORING FEMALE PED FEET!")
					PRINTLN("STORED DRAWABLE: ", iTempDraw, " STORED TEXTURE: ", iTempTex )
				#ENDIF
				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("PED ISNT FEMALE NOT STORING PED FEET!")
	#ENDIF
	
	RETURN FALSE
ENDFUNC

//Restore the female ped feet to the variation stored
PROC RESTORE_FEMALE_PED_FEET(PED_INDEX pedID)
	
	// If we are female, reset to the stored variation
	IF GET_ENTITY_MODEL(pedID) = MP_F_FREEMODE_01
		IF DOES_ENTITY_EXIST(pedID)
		AND NOT IS_ENTITY_DEAD(pedID)
			
			//Check valid component is stored
			if iPedFeetDrawable != -1
			AND iPedFeetTexture != -1
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_FEET, iPedFeetDrawable, iPedFeetTexture)
				
				#IF IS_DEBUG_BUILD
					PRINTLN("RESTORED FEMALE PED FEET!")
					PRINTLN("RESTORED VARIATION DRAWABLE : ", iPedFeetDrawable, " TEXTURE : ", iPedFeetTexture)
				#ENDIF
				
				//Reset vars
				iPedFeetDrawable = -1
				iPedFeetTexture = -1
				
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_REMOVE_HELMETS_FOR_SCENE(STRING sMocapName)

	IF ARE_STRINGS_EQUAL( sMocapName, "MPH_PAC_BIK_EXT")
	OR ARE_STRINGS_EQUAL( sMocapName, "LOW_DRV_EXT" ) //NOTE(Owain): all lowrider cutscenes leave your masks/hats on
	OR ARE_STRINGS_EQUAL( sMocapName, "LOW_FIN_EXT" )
	OR ARE_STRINGS_EQUAL( sMocapName, "LOW_FIN_MCS1" )
	OR ARE_STRINGS_EQUAL( sMocapName, "LOW_FUN_EXT" )
	OR ARE_STRINGS_EQUAL( sMocapName, "LOW_FUN_MCS1" )
	OR ARE_STRINGS_EQUAL( sMocapName, "LOW_PHO_EXT" )
	OR ARE_STRINGS_EQUAL( sMocapName, "LOW_TRA_EXT" )
	OR ARE_STRINGS_EQUAL( sMocapName, "SIL_PRED_MCS1" )
	OR ARE_STRINGS_EQUAL( sMocapName, "SILJ_MCS2" )
	OR ARE_STRINGS_EQUAL( sMocapName, "SILJ_MCS1" )
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC

FUNC BOOL CLONE_MOCAP_PLAYERS(STRING sMocapName, INT iteam, BOOL bIncludePlayers = TRUE, FMMC_CUTSCENE_TYPE eCutType = FMMCCUT_MOCAP, INT iCutsceneToUse = -1)

	IF NOT bIncludePlayers
		PRINTLN("[RCC MISSION] CLONE_MOCAP_PLAYERS - NO NEED TO CLONE PEDS THEY ARENT IN CUTSCENE")
		RETURN TRUE
	ENDIF
	
	PED_INDEX tempPed
	MODEL_NAMES tempModel
	BOOL bReturn = TRUE
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 tl63PedNameDBG
	#ENDIF
	
	INT iplayer
	FOR iplayer = 0 TO (FMMC_MAX_CUTSCENE_PLAYERS-1)
		IF MC_serverBD.piCutscenePlayerIDs[iteam][iplayer] != INVALID_PLAYER_INDEX()
			IF IS_NET_PLAYER_OK(MC_serverBD.piCutscenePlayerIDs[iteam][iplayer])
				IF MC_serverBD.piCutscenePlayerIDs[iteam][iplayer] != PlayerToUse
				OR IS_PLAYER_SCTV(LocalPlayer)
				OR IS_LOCAL_PLAYER_ANY_SPECTATOR()
				or ARE_STRINGS_EQUAL(sMocapCutscene, "mph_pri_sta_mcs1") //prison - station / casco
				OR ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PAC_WIT_MCS1") //Pacific Standard Boat Cutscene
				or are_strings_equal(sMocapCutscene, "mph_tut_ext") //fleeca bank finale
				OR ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_HUM_FIN_EXT")
					
					tempPed = GET_PLAYER_PED(MC_serverBD.piCutscenePlayerIDs[iteam][iplayer])
					tempModel = GET_ENTITY_MODEL(tempPed)
					
					REQUEST_MODEL(tempModel)
					
					IF HAS_MODEL_LOADED(tempModel)
						IF NOT DOES_ENTITY_EXIST(MocapPlayerPed[iplayer])
							PRINTLN("[PED CUT] * CLONE_MOCAP_PLAYERS - MC_serverBD.piCutscenePlayerIDs[", iteam, "][", iplayer, "] - GET_PLAYER_NAME() =  ", GET_PLAYER_NAME(MC_serverBD.piCutscenePlayerIDs[iteam][iplayer]))
							IF IS_PLAYER_PED_FEMALE(MC_serverBD.piCutscenePlayerIDs[iteam][iplayer])
								MocapPlayerPed[iplayer] = CREATE_PED(PEDTYPE_CIVFEMALE, tempModel,GET_ENTITY_COORDS(tempPed), GET_ENTITY_HEADING(tempPed), FALSE, FALSE)
							ELSE
								MocapPlayerPed[iplayer] = CREATE_PED(PEDTYPE_CIVMALE, tempModel,GET_ENTITY_COORDS(tempPed), GET_ENTITY_HEADING(tempPed), FALSE, FALSE)
							ENDIF
							CLONE_PED_TO_TARGET(tempPed, MocapPlayerPed[iplayer])
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(MocapPlayerPed[iplayer], TRUE)
							SET_PED_RELATIONSHIP_GROUP_HASH(MocapPlayerPed[iplayer], rgFM_AiLike)
							SET_ENTITY_INVINCIBLE(MocapPlayerPed[iplayer],TRUE)
							SET_ENTITY_VISIBLE(MocapPlayerPed[iplayer],FALSE)
							SET_ENTITY_COLLISION(MocapPlayerPed[iplayer],FALSE)
							
							IF ARE_STRINGS_EQUAL(sMocapName, "MPH_HUM_KEY_EXT")
							OR ARE_STRINGS_EQUAL(sMocapName, "MPH_HUM_VAL_EXT")
								FREEZE_ENTITY_POSITION(MocapPlayerPed[iplayer], TRUE) //b*2168304
							ENDIF
							
							#IF IS_DEBUG_BUILD
							tl63PedNameDBG = "CLONE_"
							tl63PedNameDBG += iplayer
							tl63PedNameDBG += " - "
							tl63PedNameDBG += GET_PLAYER_NAME(MC_serverBD.piCutscenePlayerIDs[iteam][iplayer])
							SET_PED_NAME_DEBUG(MocapPlayerPed[iplayer], tl63PedNameDBG)
							#ENDIF

							iMocapPlayerPedMask[iplayer] = GET_PLAYER_CHOSEN_MASK(MC_serverBD.piCutscenePlayerIDs[iteam][iplayer])
							
							IF CAN_REMOVE_MASKS_FOR_HEIST_CUTSCENE(sMocapName, DEFAULT, eCutType, iCutsceneToUse)
								//Now passing in the player index so that we dont have the issue with same player faces. CV
								REMOVE_MASKS_AND_REBREATHERS_FOR_CUTSCENES(MocapPlayerPed[iplayer], MC_serverBD.piCutscenePlayerIDs[iteam][iplayer], INT_TO_ENUM(mp_outfit_mask_enum,iMocapPlayerPedMask[iplayer]))
							ENDIF
							
//							IF SHOULD_REMOVE_HELMETS_FOR_SCENE(sMocapName)	//2219902
//								REMOVE_PED_HELMET(MocapPlayerPed[iplayer], TRUE)//2097921
//							ENDIF
							
							
							BOOL bKeepHelmets							
							IF iCutsceneToUse > -1
							AND eCutType = FMMCCUT_MOCAP
								IF IS_BIT_SET(g_fmmc_struct.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_KeepHatsAndGlassesEtc)
									bKeepHelmets = TRUE
								ENDIF
							ELIF eCutType = FMMCCUT_ENDMOCAP
								IF IS_BIT_SET(g_fmmc_struct.sEndMocapSceneData.iCutsceneBitSet2, ci_CSBS2_KeepHatsAndGlassesEtc)
									bKeepHelmets = TRUE
								ENDIF
							ENDIF

							BOOL bApplyHeadProp = TRUE
							IF SHOULD_REMOVE_HELMETS_FOR_SCENE(sMocapName)
							AND NOT bKeepHelmets
								IF IS_PED_WEARING_A_HELMET(MocapPlayerPed[iplayer])
									PRINTLN("[RCC MISSION] CLONE_MOCAP_PLAYERS - REMOVE_PED_HELMET, bApplyHeadProp = FALSE ")
									REMOVE_PED_HELMET(MocapPlayerPed[iplayer], TRUE)	//2191482
									SET_PED_PROP_INDEX( MocapPlayerPed[iplayer], ANCHOR_HEAD, 0, 0 ) 
									bApplyHeadProp = FALSE
								ENDIF
							ENDIF
							
							IF bApplyHeadProp
								//NOTE(Owain): copy hats over
								INT iHatIndex = GET_PED_PROP_INDEX( tempPed, ANCHOR_HEAD )
								INT iHatTexture = GET_PED_PROP_TEXTURE_INDEX( tempPed, ANCHOR_HEAD )
								
								PRINTLN("[RCC MISSION] CLONE_MOCAP_PLAYERS - iHatIndex: ", iHatIndex)
								PRINTLN("[RCC MISSION] CLONE_MOCAP_PLAYERS - iHatTexture: ", iHatTexture)
								
								IF iHatIndex > -1 AND iHatTexture > -1
									PRINTLN("[RCC MISSION] CLONE_MOCAP_PLAYERS - Calling SET_PED_PROP_INDEX on ANCHOR_HEAD")
									SET_PED_PROP_INDEX( MocapPlayerPed[iplayer], ANCHOR_HEAD, iHatIndex, iHatTexture ) 
								ENDIF
							ENDIF
							
							// Give player parachute
							IF NOT IS_STRING_NULL_OR_EMPTY(sMocapName)
							AND ARE_STRINGS_EQUAL( "MPH_PRI_FIN_MCS2", sMocapName )
								IF NOT HAS_PED_GOT_WEAPON(MocapPlayerPed[iplayer], GADGETTYPE_PARACHUTE)
									SET_CURRENT_PED_WEAPON(MocapPlayerPed[iplayer], WEAPONTYPE_UNARMED)
									GIVE_WEAPON_TO_PED(MocapPlayerPed[iplayer], GADGETTYPE_PARACHUTE, 1, TRUE, FALSE)
									PRINTLN("[RCC MISSION] CLONE_MOCAP_PLAYERS TASK_PARACHUTE TRUE 2 ")
									SET_CURRENT_PED_WEAPON(MocapPlayerPed[iplayer], GADGETTYPE_PARACHUTE)
									SET_PED_COMPONENT_VARIATION(MocapPlayerPed[iplayer], PED_COMP_HAND, 1, 0)
								ELSE
									SET_CURRENT_PED_WEAPON(MocapPlayerPed[iplayer], GADGETTYPE_PARACHUTE)
									SET_PED_COMPONENT_VARIATION(MocapPlayerPed[iplayer], PED_COMP_HAND, 1, 0)
									PRINTLN("[RCC MISSION] CLONE_MOCAP_PLAYERS TASK_PARACHUTE TRUE 1")
								ENDIF
								PRINTLN("[RCC MISSION] CLONE_MOCAP_PLAYERS TASK_PARACHUTE TRUE ")
							ENDIF
							
							// Remove player parachute for Pacific Standard Finale cutscene
							IF ARE_STRINGS_EQUAL("MPH_PAC_FIN_EXT", sMocapName)
								SET_PED_COMPONENT_VARIATION(MocapPlayerPed[iplayer], PED_COMP_HAND, 0, 0)
							ENDIF
										
							bReturn =  FALSE
						ELSE
							//Check the ped is ready for the cutscene
							IF NOT IS_PED_INJURED(MocapPlayerPed[iplayer])
							AND NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(MocapPlayerPed[iplayer]) 
							AND NOT ARE_STRINGS_EQUAL(sMocapName, "MPH_PRI_FIN_MCS1") // Bypassing this to speed up the triggering of cutscene for url:bugstar:2227450 - We were able to run up on Rashkovsky here
								PRINTLN("[RCC MISSION] CLONE_MOCAP_PLAYERS HAVE_ALL_STREAMING_REQUESTS_COMPLETED FALSE ")
								bReturn = FALSE
							ENDIF
						ENDIF
					ELSE
						PRINTLN("[RCC MISSION] CLONE_MOCAP_PLAYERS WAITING FOR MODEL LOAD: ", ENUM_TO_INT(tempModel))
						bReturn =  FALSE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION] CLONE_MOCAP_PLAYERS iteam: ", iteam, " iPlayer: ", iPlayer, " MC_serverBD.piCutscenePlayerIDs[iteam][iplayer] = INVALID_PLAYER_INDEX()")
		ENDIF
	ENDFOR

	IF bReturn = FALSE
		RETURN FALSE
	ENDIF
	
	if not does_entity_exist(envelope.obj)
		if MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_MADRAZO
		
			IF not IS_STRING_NULL_OR_EMPTY(sMocapName)
			
				printstring("envelope test = ")
				printstring(sMocapName)
				printnl()
													 
				if are_strings_equal(sMocapName, "MP_INTRO_MCS_14_B")
				or are_strings_equal(sMocapName, "MP_INT_MCS_15_A1_B") 
				or are_strings_equal(sMocapName, "MP_INT_MCS_15_A2B")

					envelope.obj = create_object(PROP_CS_CASHENVELOPE, get_offset_from_entity_in_world_coords(LocalPlayerPed, <<0.0, 0.0, 1.0>>), false, false) 
						
					printstring("envelope test = OBJECT CREATED")
					printnl()
					
				else
				
					printstring("envelope test = NO OBJECT CREATED")
					printnl()
				
				endif 
				
			endif 
			
		endif 
	endif 
	
	RETURN TRUE

ENDFUNC

FUNC BOOL CLONE_SCRIPTED_PLAYERS(INT iteam, BOOL bIncludePlayers = TRUE)

	IF NOT bIncludePlayers
		PRINTLN("[RCC MISSION] CLONE_SCRIPTED_PLAYERS - No need to clone peds")
		RETURN TRUE
	ENDIF
	
	PED_INDEX tempPed, tempPed2
	VECTOR vPedCoords
	FLOAT fPedHeading
	MODEL_NAMES tempModel
	PED_TYPE pedType
	BOOL bReturn = TRUE
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 tl63PedNameDBG
	#ENDIF
	
	INT iplayer, iplayer2
	
	FOR iplayer = 0 TO (FMMC_MAX_CUTSCENE_PLAYERS - 1)
		
		IF MC_serverBD.piCutscenePlayerIDs[iteam][iplayer] = INVALID_PLAYER_INDEX()
		OR (NOT IS_NET_PLAYER_OK(MC_serverBD.piCutscenePlayerIDs[iteam][iplayer]))
			RELOOP
		ENDIF
		
		tempPed = GET_PLAYER_PED(MC_serverBD.piCutscenePlayerIDs[iteam][iplayer])
		tempModel = GET_ENTITY_MODEL(tempPed)
		
		REQUEST_MODEL(tempModel)
		
		IF NOT HAS_MODEL_LOADED(tempModel)
			PRINTLN("[RCC MISSION] CLONE_SCRIPTED_PLAYERS - Waiting for player ",iplayer,"'s model ",ENUM_TO_INT(tempModel))
			bReturn = FALSE
			RELOOP
		ENDIF
		
		IF DOES_ENTITY_EXIST(MocapPlayerPed[iplayer])
			//Check the ped is ready for the cutscene
			IF NOT IS_PED_INJURED(MocapPlayerPed[iplayer])
			AND NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(MocapPlayerPed[iplayer]) 
				PRINTLN("[RCC MISSION] CLONE_SCRIPTED_PLAYERS - Waiting for streaming requests on player ",iplayer,"'s clone")
				bReturn = FALSE
			ENDIF
			
			RELOOP
		ENDIF
		
		vPedCoords = GET_ENTITY_COORDS(tempPed)
		fPedHeading = GET_ENTITY_HEADING(tempPed)
		
		PRINTLN("[RCC MISSION] CLONE_SCRIPTED_PLAYERS - Cloning MC_serverBD.piCutscenePlayerIDs[", iteam, "][", iplayer, "], player name = ",GET_PLAYER_NAME(MC_serverBD.piCutscenePlayerIDs[iteam][iplayer]))
		PRINTLN("[RCC MISSION] CLONE_SCRIPTED_PLAYERS - Player vPedCoords ",vPedCoords,", fPedHeading ",fPedHeading)
		
		IF IS_PLAYER_PED_FEMALE(MC_serverBD.piCutscenePlayerIDs[iteam][iplayer])
			pedType = PEDTYPE_CIVFEMALE
		ELSE
			pedType = PEDTYPE_CIVMALE
		ENDIF
		
		MocapPlayerPed[iplayer] = CREATE_PED(pedType, tempModel, vPedCoords, fPedHeading, FALSE, FALSE)
		
		CLONE_PED_TO_TARGET(tempPed, MocapPlayerPed[iplayer])
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(MocapPlayerPed[iplayer], TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(MocapPlayerPed[iplayer], rgFM_AiLike)
		SET_ENTITY_INVINCIBLE(MocapPlayerPed[iplayer], TRUE)
		SET_ENTITY_VISIBLE(MocapPlayerPed[iplayer], FALSE)
		
		#IF IS_DEBUG_BUILD
		tl63PedNameDBG = "CLONE_"
		tl63PedNameDBG += iplayer
		tl63PedNameDBG += " - "
		tl63PedNameDBG += GET_PLAYER_NAME(MC_serverBD.piCutscenePlayerIDs[iteam][iplayer])
		SET_PED_NAME_DEBUG(MocapPlayerPed[iplayer], tl63PedNameDBG)
		#ENDIF
		
		//NOTE(Owain): copy hats over
		INT iHatIndex = GET_PED_PROP_INDEX( tempPed, ANCHOR_HEAD )
		INT iHatTexture = GET_PED_PROP_TEXTURE_INDEX( tempPed, ANCHOR_HEAD )
		
		IF iHatIndex > -1 AND iHatTexture > -1
			SET_PED_PROP_INDEX( MocapPlayerPed[iplayer], ANCHOR_HEAD, iHatIndex, iHatTexture ) 
		ENDIF
		
		FOR iPlayer2 = 0 TO (FMMC_MAX_CUTSCENE_PLAYERS - 1)
			
			IF MC_serverBD.piCutscenePlayerIDs[iteam][iPlayer2] = INVALID_PLAYER_INDEX()
			OR (NOT IS_NET_PLAYER_OK(MC_serverBD.piCutscenePlayerIDs[iteam][iPlayer2]))
				RELOOP
			ENDIF
			
			tempPed2 = GET_PLAYER_PED(MC_serverBD.piCutscenePlayerIDs[iteam][iPlayer2])
			
			PRINTLN("[RCC MISSION] CLONE_SCRIPTED_PLAYERS - Setting ped ",iplayer," no collision with cutscene player ",iPlayer2,", name ",GET_PLAYER_NAME(MC_serverBD.piCutscenePlayerIDs[iteam][iPlayer2]))
			SET_ENTITY_NO_COLLISION_ENTITY(MocapPlayerPed[iplayer], tempPed2, FALSE)
		ENDFOR
		
		bReturn = FALSE // Need to wait for streaming requests to complete before moving on
		
	ENDFOR

	IF bReturn = FALSE
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL IS_CAM_CURRENTLY_FIRST_PERSON()

	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		IF IS_PED_ON_ANY_BIKE(LocalPlayerPed)
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT( CAM_VIEW_MODE_CONTEXT_ON_BIKE ) = CAM_VIEW_MODE_FIRST_PERSON
				CPRINTLN(DEBUG_MISSION, "IS_CAM_CURRENTLY_FIRST_PERSON - TRUE, On bike with bike view mode set to first person.")
				RETURN TRUE
			ENDIF
		ELIF IS_PED_IN_ANY_BOAT(LocalPlayerPed)
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT( CAM_VIEW_MODE_CONTEXT_IN_BOAT ) = CAM_VIEW_MODE_FIRST_PERSON
				CPRINTLN(DEBUG_MISSION, "IS_CAM_CURRENTLY_FIRST_PERSON - TRUE, In boat with boat view mode set to first person.")
				RETURN TRUE
			ENDIF
		ELIF IS_PED_IN_ANY_PLANE(LocalPlayerPed)
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT( CAM_VIEW_MODE_CONTEXT_IN_AIRCRAFT ) = CAM_VIEW_MODE_FIRST_PERSON
				CPRINTLN(DEBUG_MISSION, "IS_CAM_CURRENTLY_FIRST_PERSON - TRUE, In plane with aircraft view mode set to first person.")
				RETURN TRUE
			ENDIF
		ELIF IS_PED_IN_ANY_SUB(LocalPlayerPed)
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT( CAM_VIEW_MODE_CONTEXT_IN_SUBMARINE ) = CAM_VIEW_MODE_FIRST_PERSON
				CPRINTLN(DEBUG_MISSION, "IS_CAM_CURRENTLY_FIRST_PERSON - TRUE, In sub with sub view mode set to first person.")
				RETURN TRUE
			ENDIF
		ELIF IS_PED_IN_ANY_HELI(LocalPlayerPed)
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT( CAM_VIEW_MODE_CONTEXT_IN_HELI ) = CAM_VIEW_MODE_FIRST_PERSON
				CPRINTLN(DEBUG_MISSION, "IS_CAM_CURRENTLY_FIRST_PERSON - TRUE, In heli with heli view mode set to first person.")
				RETURN TRUE
			ENDIF
		ELSE
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT( CAM_VIEW_MODE_CONTEXT_IN_VEHICLE ) = CAM_VIEW_MODE_FIRST_PERSON
				CPRINTLN(DEBUG_MISSION, "IS_CAM_CURRENTLY_FIRST_PERSON - TRUE, In vehicle with vehicle view mode set to first person.")
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		IF GET_CAM_VIEW_MODE_FOR_CONTEXT( CAM_VIEW_MODE_CONTEXT_ON_FOOT ) = CAM_VIEW_MODE_FIRST_PERSON
			CPRINTLN(DEBUG_MISSION, "IS_CAM_CURRENTLY_FIRST_PERSON - TRUE, Not in vehicle with on foot view mode set to first person.")
			RETURN TRUE
		ENDIF
	ENDIF 

	RETURN FALSE

ENDFUNC

proc clone_gameplay_camera()
	
	IF NOT IS_CAM_CURRENTLY_FIRST_PERSON()
	AND NOT IS_CINEMATIC_CAM_RENDERING()
		camera_a = create_cam_with_params("default_scripted_camera", GET_FINAL_RENDERED_CAM_COORD(), GET_FINAL_RENDERED_CAM_ROT(), GET_FINAL_RENDERED_CAM_FOV()) 
		set_cam_near_clip(camera_a, GET_FINAL_RENDERED_CAM_NEAR_CLIP())
		set_cam_far_clip(camera_a, GET_FINAL_RENDERED_CAM_FAR_CLIP())
		set_cam_motion_blur_strength(camera_a, GET_FINAL_RENDERED_CAM_MOTION_BLUR_STRENGTH())

		set_cam_active(camera_a, true)
		
		render_script_cams(true, false)
	
	ENDIF

endproc 

proc reduce_voice_chat_for_specific_cutscenes(STRING sMocapName)

	IF not IS_STRING_NULL_OR_EMPTY(sMocapName)
								
		if MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_MADRAZO  
			if are_strings_equal(sMocapName, "MP_INTRO_MCS_14_B")
				NETWORK_SET_VOICE_ACTIVE(false)
			endif 
		endif 
		
		if MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_GERALD  
			if are_strings_equal(sMocapName, "MP_INTRO_MCS_10_A1")
				NETWORK_SET_VOICE_ACTIVE(false)
			endif 
		endif
		
	endif 
	
	// url:bugstar:5820427 - Casino Story Cut scenes - We can hear remote players over cutscenes, we normally mute them so everyone gets to hear them.
	// I could not find anything in script that would suggest we mute them for other pack cutscenes. Further to this my local repro's on older missions indicated that voice chat was not blocked.
	// I think something weird/special case was put in for the nightclub pack.
	IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO) 
		NETWORK_SET_VOICE_ACTIVE(false)
	ENDIF
endproc 

PROC MANAGE_PLAYERS_TALKING()

INT iplayer 

	FOR iplayer = 0 TO (FMMC_MAX_CUTSCENE_PLAYERS-1)
		IF MC_serverBD.piCutscenePlayerIDs[MC_PlayerBD[iPartToUse].iteam][iplayer] != INVALID_PLAYER_INDEX()
			IF IS_NET_PLAYER_OK(MC_serverBD.piCutscenePlayerIDs[MC_PlayerBD[iPartToUse].iteam][iplayer])
		       	IF MC_serverBD.piCutscenePlayerIDs[MC_PlayerBD[iPartToUse].iteam][iplayer] != PlayerToUse
					IF NOT IS_PED_INJURED(MocapPlayerPed[iplayer])
						IF NETWORK_PLAYER_HAS_HEADSET(MC_serverBD.piCutscenePlayerIDs[MC_PlayerBD[iPartToUse].iteam][iplayer])
							IF NETWORK_IS_PLAYER_TALKING(MC_serverBD.piCutscenePlayerIDs[MC_PlayerBD[iPartToUse].iteam][iplayer]) 
								SET_PED_RESET_FLAG(MocapPlayerPed[iplayer], PRF_EnableVoiceDrivenMouthMovement, TRUE)
							ENDIF 
						ENDIF 
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

PROC CLEANUP_MOCAP_PLAYER_CLONES()
	
	INT iplayer
	FOR iplayer = 0 TO (FMMC_MAX_CUTSCENE_PLAYERS-1)
		IF DOES_ENTITY_EXIST(MocapPlayerPed[iplayer])
			DELETE_PED(MocapPlayerPed[iplayer])
		ENDIF
	ENDFOR
	
ENDPROC

/// PURPOSE: Broadcast to all other players that I am playing an animation
/// PARAMS:
///    iAnimationType - Type of animation (Player or Crew)
///    iAnimation - The actual animation to play
PROC BROADCAST_MOCAP_PLAYER_ANIMATION(INT iAnimationType, INT iAnimation, BOOL bPlaying)
	
	EVENT_STRUCT_MOCAP_PLAYER_ANIM Event
	Event.Details.Type 				= SCRIPT_EVENT_FMMC_MOCAP_PLAYER_ANIMATION
	Event.Details.FromPlayerIndex	= LocalPlayer
	Event.iAnimationType 			= iAnimationType
	Event.iAnimation 				= iAnimation
	Event.bPlaying					= bPlaying
		
	INT iSendTo = ALL_PLAYERS(FALSE)
	IF NOT (iSendTo = 0)		
		
		#IF IS_DEBUG_BUILD
			PRINTLN("[RCC MISSION]  BROADCAST_MOCAP_PLAYER_ANIMATION - called...")
			PRINTLN("[RCC MISSION] 					From script: ", GET_THIS_SCRIPT_NAME())
			PRINTLN("[RCC MISSION] 					iAnimationType: ", Event.iAnimationType)
			PRINTLN("[RCC MISSION] 					iAnimation: ", Event.iAnimation)
			PRINTLN("[RCC MISSION] 					bPlaying: ", Event.bPlaying)
		#ENDIF

		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iSendTo)
	ELSE
		PRINTLN("[RCC MISSION] BROADCAST_MOCAP_PLAYER_ANIMATION - playerflags = 0 so not broadcasting") 
	ENDIF	
	
ENDPROC

PROC MAINTAIN_REMOTE_PLAYER_MOCAP_ANIMATIONS()

	INT i,iplayer
	// Process all other players animations, playing on their clones
	FOR i= 0 TO (FMMC_MAX_CUTSCENE_PLAYERS-1)
		IF MC_serverBD.piCutscenePlayerIDs[MC_PlayerBD[iPartToUse].iteam][i] != INVALID_PLAYER_INDEX()
			IF IS_NET_PLAYER_OK(MC_serverBD.piCutscenePlayerIDs[MC_PlayerBD[iPartToUse].iteam][i])
				iplayer = NATIVE_TO_INT(MC_serverBD.piCutscenePlayerIDs[MC_PlayerBD[iPartToUse].iteam][i])
				IF MC_serverBD.piCutscenePlayerIDs[MC_PlayerBD[iPartToUse].iteam][i] != PlayerToUse
				OR IS_PLAYER_SCTV(LocalPlayer)
					IF DOES_ENTITY_EXIST(MocapPlayerPed[i])
						UPDATE_INTERACTION_ANIM_FOR_PED(MocapPlayerPed[i], iplayer, gInteractionsPedsData[iplayer])
					ENDIF
				ELSE	
					IF DOES_ENTITY_EXIST(LocalPlayerPed)
						UPDATE_INTERACTION_ANIM_FOR_PED(LocalPlayerPed, iplayer, gInteractionsPedsData[iplayer])
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
	ENDFOR

ENDPROC

func bool is_cutscene_on_interaction_anim_exclusion_list()
				
	if are_strings_equal(sMocapCutscene, "MP_INTRO_MCS_17_A8")
	or are_strings_equal(sMocapCutscene, "MP_INT_MCS_12_A3_3")
	or are_strings_equal(sMocapCutscene, "MP_INT_MCS_12_A3_4")
	
		return true 
		
	endif 
	
	return false 

endfunc 


PROC MANAGE_PLAYER_GESTURES()

	IF IS_BIT_SET(iLocalBoolCheck2, LBOOL2_MOCAP_PLAYER_PLAYING_ANIM)
		IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LS)
		OR NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RS)
		OR NOT HAS_ANIM_EVENT_FIRED(LocalPlayerPed,GET_HASH_KEY("ALLOW_MP_GESTURE"))
			// Mark ourselves as not playing
			MPGlobalsInteractions.PedInteraction[NATIVE_TO_INT(LocalPlayer)].bHoldLoop = FALSE 
			//MPGlobalsInteractions.PedInteraction[NATIVE_TO_INT(LocalPlayer)].bPlayInteractionAnim = FALSE
			
			CLEAR_BIT(iLocalBoolCheck2, LBOOL2_MOCAP_PLAYER_PLAYING_ANIM)
			IF NOT IS_MP_PLAYER_ANIM_SETTING_CREW_DEFAULT()  
				//makes sure the player and clones can't to the bird anim on certain mocaps where there is not enough space to open their arms.
				IF ((GET_MP_PLAYER_ANIM_SETTING() = ENUM_TO_INT(PLAYER_INTERACTION_FINGER)) and (is_cutscene_on_interaction_anim_exclusion_list()))
					BROADCAST_MOCAP_PLAYER_ANIMATION(ENUM_TO_INT(INTERACTION_ANIM_TYPE_SPECIAL), ENUM_TO_INT(SPECIAL_INTERACTION_SHAKE_HEAD) , FALSE)
				ELSE 
					BROADCAST_MOCAP_PLAYER_ANIMATION(ENUM_TO_INT(INTERACTION_ANIM_TYPE_PLAYER), GET_MP_PLAYER_ANIM_SETTING() , FALSE)
				ENDIF 
			ELSE
				IF ((MPGlobalsInteractions.iMyCrewInteractionAnim = ENUM_TO_INT(CREW_INTERACTION_FINGER)) and (is_cutscene_on_interaction_anim_exclusion_list()))
					BROADCAST_MOCAP_PLAYER_ANIMATION(ENUM_TO_INT(INTERACTION_ANIM_TYPE_SPECIAL), ENUM_TO_INT(SPECIAL_INTERACTION_SHAKE_HEAD) , FALSE)
				ELSE
					BROADCAST_MOCAP_PLAYER_ANIMATION(ENUM_TO_INT(INTERACTION_ANIM_TYPE_CREW), MPGlobalsInteractions.iMyCrewInteractionAnim , FALSE)
				ENDIF
		    ENDIF
		ENDIF
	ENDIF
	IF NOT IS_BIT_SET(iLocalBoolCheck2, LBOOL2_MOCAP_PLAYER_PLAYING_ANIM)
	AND HAS_ANIM_EVENT_FIRED(LocalPlayerPed,GET_HASH_KEY("ALLOW_MP_GESTURE"))	
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LS)
		AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RS)
			
			SET_BIT(iLocalBoolCheck2, LBOOL2_MOCAP_PLAYER_PLAYING_ANIM)

			IF NOT IS_MP_PLAYER_ANIM_SETTING_CREW_DEFAULT() 
				//makes sure the player and clones can't to the bird anim on certain mocaps where there is not enough space to open their arms.
				IF ((GET_MP_PLAYER_ANIM_SETTING() = ENUM_TO_INT(PLAYER_INTERACTION_FINGER)) and (is_cutscene_on_interaction_anim_exclusion_list()))
					MPGlobalsInteractions.PedInteraction[NATIVE_TO_INT(LocalPlayer)].iInteractionType = ENUM_TO_INT(INTERACTION_ANIM_TYPE_SPECIAL)                            
					MPGlobalsInteractions.PedInteraction[NATIVE_TO_INT(LocalPlayer)].iInteractionAnim = ENUM_TO_INT(SPECIAL_INTERACTION_SHAKE_HEAD)
				ELSE 
					MPGlobalsInteractions.PedInteraction[NATIVE_TO_INT(LocalPlayer)].iInteractionType = ENUM_TO_INT(INTERACTION_ANIM_TYPE_PLAYER) 
				    MPGlobalsInteractions.PedInteraction[NATIVE_TO_INT(LocalPlayer)].iInteractionAnim = GET_MP_PLAYER_ANIM_SETTING()
				ENDIF 
			ELSE
				IF ((MPGlobalsInteractions.iMyCrewInteractionAnim = ENUM_TO_INT(CREW_INTERACTION_FINGER)) and (is_cutscene_on_interaction_anim_exclusion_list()))
					MPGlobalsInteractions.PedInteraction[NATIVE_TO_INT(LocalPlayer)].iInteractionType = ENUM_TO_INT(INTERACTION_ANIM_TYPE_SPECIAL)
					MPGlobalsInteractions.PedInteraction[NATIVE_TO_INT(LocalPlayer)].iInteractionAnim = ENUM_TO_INT(SPECIAL_INTERACTION_SHAKE_HEAD)
				ELSE
					MPGlobalsInteractions.PedInteraction[NATIVE_TO_INT(LocalPlayer)].iInteractionType = ENUM_TO_INT(INTERACTION_ANIM_TYPE_CREW)
					MPGlobalsInteractions.PedInteraction[NATIVE_TO_INT(LocalPlayer)].iInteractionAnim = MPGlobalsInteractions.iMyCrewInteractionAnim
				ENDIF 
		    ENDIF
			
			MPGlobalsInteractions.PedInteraction[NATIVE_TO_INT(LocalPlayer)].bPlayInteractionAnim = TRUE
	        MPGlobalsInteractions.PedInteraction[NATIVE_TO_INT(LocalPlayer)].bHoldLoop = TRUE
			BROADCAST_MOCAP_PLAYER_ANIMATION(MPGlobalsInteractions.PedInteraction[NATIVE_TO_INT(LocalPlayer)].iInteractionAnim, MPGlobalsInteractions.PedInteraction[NATIVE_TO_INT(LocalPlayer)].iInteractionAnim , TRUE)
			
		ENDIF
	ENDIF
	
	MAINTAIN_REMOTE_PLAYER_MOCAP_ANIMATIONS()
		
ENDPROC

PROC CONCEAL_ALL_OTHER_PLAYERS(BOOL bHide, bool include_local_player = false, bool b_remotely_visible = false)
	
INT iparticipant
PARTICIPANT_INDEX tempPart
PLAYER_INDEX tempplayer

	PRINTLN("[RCC MISSION] -CONCEAL_ALL_OTHER_PLAYERS- ( bHide = ", bHide, "include_local_player = ", include_local_player)

	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iparticipant
		tempPart = INT_TO_PARTICIPANTINDEX(iparticipant)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			tempplayer = NETWORK_GET_PLAYER_INDEX(tempPart)
			IF IS_NET_PLAYER_OK(tempplayer)
		       	IF tempplayer!= LocalPlayer
					PRINTLN("[RCC MISSION] -CONCEAL_ALL_OTHER_PLAYERS- NETWORK_CONCEAL_PLAYER - SET TO : ", bHide)
					NETWORK_CONCEAL_PLAYER(tempplayer, bHide)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	//Was setting player invisible when they should be visible b*2199298
	IF include_local_player
		SET_ENTITY_VISIBLE_IN_CUTSCENE(LocalPlayerPed, !bHide, b_remotely_visible)
		PRINTLN("[RCC MISSION] -CONCEAL_ALL_OTHER_PLAYERS- SET_ENTITY_VISIBLE_IN_CUTSCENE - SET TO : ", !bHide, "b_remotely_visible:", b_remotely_visible)
	ENDIF
	
ENDPROC

//Use this one once we get code support for Fleeca cutscene.
PROC CONCEAL_ALL_PLAYERS_LOCALLY(BOOL bHide)
	
INT iparticipant
PARTICIPANT_INDEX tempPart
PLAYER_INDEX tempplayer

	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iparticipant
		tempPart = INT_TO_PARTICIPANTINDEX(iparticipant)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			tempplayer = NETWORK_GET_PLAYER_INDEX(tempPart)
			IF IS_NET_PLAYER_OK(tempplayer)
		       	IF tempplayer!= LocalPlayer
					PRINTLN("[RCC MISSION] -CONCEAL_ALL_PLAYERS_LOCALLY- NETWORK_CONCEAL_PLAYER - SET TO : ", bHide)
					NETWORK_CONCEAL_PLAYER(tempplayer, bHide)
				ELSE
					PRINTLN("[RCC MISSION] -CONCEAL_ALL_PLAYERS_LOCALLY SET_ENTITY_VISIBLE_IN_CUTSCENE - SET TO : ", bHide)
					SET_ENTITY_VISIBLE_IN_CUTSCENE(LocalPlayerPed, !bHide, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

///PURPOSE:
///   plays garage door sounds whilst the door is moving during the mocap.
///    
proc garage_door_audio_system()
					
	switch garage_door_audio_system_status  

		case 0

			IF not IS_STRING_NULL_OR_EMPTY(sMocapCutscene)
			
				printstring("mocap string = ")
				printstring(sMocapCutscene)
				printnl()
			
				if are_strings_equal(sMocapCutscene, "MP_INTRO_MCS_12_A1") 
				
					if get_cutscene_time() >= 0
					
						play_sound_frontend(garage_door_sound_id, "OPENING", "DOOR_GARAGE", FALSE)

						garage_door_audio_system_status++
					
					endif 

				elif are_strings_equal(sMocapCutscene, "MP_INTRO_MCS_12_A2")
				
					if get_cutscene_time() >= 0
						
						play_sound_frontend(garage_door_sound_id, "OPENING", "DOOR_GARAGE", FALSE)

						garage_door_audio_system_status++
					
					endif 

				elif are_strings_equal(sMocapCutscene, "MP_INTRO_MCS_12_A3")
				
					if get_cutscene_time() >= 0
					
						play_sound_frontend(garage_door_sound_id, "OPENING", "DOOR_GARAGE", FALSE)

						printstring("garage door test 0")
						printnl()
						
						garage_door_audio_system_status++
					
					endif 
				
				elif are_strings_equal(sMocapCutscene, "MP_INT_MCS_12_A3_3") 
				
					if get_cutscene_time() >= 0
					
						play_sound_frontend(garage_door_sound_id, "OPENING", "DOOR_GARAGE", FALSE)

						garage_door_audio_system_status++
					
					endif 
				
				elif are_strings_equal(sMocapCutscene, "MP_INT_MCS_12_A3_4") 
				
					if get_cutscene_time() >= 0
					
						play_sound_frontend(garage_door_sound_id, "OPENING", "DOOR_GARAGE", FALSE)

						garage_door_audio_system_status++
					
					endif 

				endif 
				
			endif
			
		break 
		
		case 1
		
			if are_strings_equal(sMocapCutscene, "MP_INTRO_MCS_12_A1") 
			
				if get_cutscene_time() > 4649
				
					stop_sound(garage_door_sound_id)
					
					play_sound_frontend(garage_door_sound_id, "OPENED", "DOOR_GARAGE", FALSE)
					
					printstring("garage door test 1")
					printnl()
					
					garage_door_audio_system_status++
					
				endif 
					
				
			elif are_strings_equal(sMocapCutscene, "MP_INTRO_MCS_12_A2") 
			
				if get_cutscene_time() > 3853
				
					stop_sound(garage_door_sound_id)
					
					play_sound_frontend(garage_door_sound_id, "OPENED", "DOOR_GARAGE", FALSE)
					
					printstring("garage door test 1")
					printnl()
					
					garage_door_audio_system_status++
					
				endif 
			
			elif are_strings_equal(sMocapCutscene, "MP_INTRO_MCS_12_A3")
			
				if get_cutscene_time() > 2620
				
					stop_sound(garage_door_sound_id)
					
					play_sound_frontend(garage_door_sound_id, "OPENED", "DOOR_GARAGE", FALSE)
					
					printstring("garage door test 1")
					printnl()
					
					garage_door_audio_system_status++
					
				endif 
			
			elif are_strings_equal(sMocapCutscene, "MP_INT_MCS_12_A3_3") 
			
				if get_cutscene_time() > 6100
				
					stop_sound(garage_door_sound_id)
					
					play_sound_frontend(garage_door_sound_id, "OPENED", "DOOR_GARAGE", FALSE)
					
					printstring("garage door test 1")
					printnl()
					
					garage_door_audio_system_status++
					
				endif 
			
			elif are_strings_equal(sMocapCutscene, "MP_INT_MCS_12_A3_4") 
			
				if get_cutscene_time() > 4900
				
					stop_sound(garage_door_sound_id)
					
					play_sound_frontend(garage_door_sound_id, "OPENED", "DOOR_GARAGE", FALSE)
					
					printstring("garage door test 1")
					printnl()
					
					garage_door_audio_system_status++
					
				endif 

			endif 
				
		break 		

		case 2
		
			if has_sound_finished(garage_door_sound_id)
				
				stop_sound(garage_door_sound_id)
				
				printstring("garage door test 2")
				printnl()
				
				garage_door_audio_system_status++
				
			endif 
		
		break 
		
		case 3
		
			//CLOSING
		
		break 
		
	endswitch 

endproc 

proc register_madrazo_assets()
								
	IF MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_MADRAZO
	
		IF not IS_STRING_NULL_OR_EMPTY(sMocapCutscene)
			
			if are_strings_equal(sMocapCutscene, "MP_INTRO_MCS_14_B")
			or are_strings_equal(sMocapCutscene, "MP_INT_MCS_15_A3") 
			or are_strings_equal(sMocapCutscene, "MP_INT_MCS_15_A3")
			or are_strings_equal(sMocapCutscene, "MP_INT_MCS_15_A1_B")
			or are_strings_equal(sMocapCutscene, "MP_INT_MCS_15_A2B")
			
				register_entity_for_cutscene(null, "mp_car", CU_DONT_ANIMATE_ENTITY, tailgater)
			
			endif 
			
		endif 
		
	endif 
	
endproc 

proc hide_garage_door()

	garage_door_model = int_to_enum(model_names, get_hash_key("v_15_garg_delta_doordown"))

	create_model_hide(<<-29.32, -1086.63, 26.95>>, 1.0, garage_door_model, false) 

endproc 

proc set_cutscene_vehicles_visible_in_cutscenes()
	IF AM_I_ON_A_HEIST()
	OR IS_FAKE_MULTIPLAYER_MODE_SET()
		//On heist set previously for cutscene entities
		PRINTLN("[RCC MISSION] ON HEIST VEHICLES ALREADY SET UP VEHICLES")
	ELSE
		PRINTLN("[RCC MISSION] NOT ON HEIST SETTING UP VEHICLES")
		
		int i = 0
		int j = 0
	
		object_index network_obj
	
		BOOL bStrandMission = IS_A_STRAND_MISSION_BEING_INITIALISED()

		//Non heist
		for i = 0 to count_of(cutscene_vehicle) - 1
			PRINTLN("[RCC MISSION] check cutscene vehicle: ",i)
			if does_entity_exist(cutscene_vehicle[i])
				PRINTLN("[RCC MISSION] cutscene vehicle exists: ",i)
				if is_vehicle_driveable(cutscene_vehicle[i])
					PRINTLN("[RCC MISSION] cutscene vehicle drivable: ",i)
					//set vehicle proofs
					IF bStrandMission
						//PASS_OVER_CUT_SCENE_VEHICLE(cutscene_vehicle[i], sCutsceneHandle)
					ELSE
						BOOL bRemotelyVisible = FALSE
						IF GET_ENTITY_MODEL(cutscene_vehicle[i]) = AVENGER
							bRemotelyVisible = TRUE
							PRINTLN("[RCC MISSION] cutscene vehicle: ",i, " is Avenger setting bRemotelyVisible")
						ENDIF
						SET_ENTITY_VISIBLE_IN_CUTSCENE(cutscene_vehicle[i], true, bRemotelyVisible)
					ENDIF

					for j = 0 to count_of(MC_serverBD_1.sFMMC_SBD.niVehicleCrate) - 1
					
						IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[j])

							network_obj = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[j])
						
							if does_entity_exist(network_obj)
								if is_entity_attached_to_entity(network_obj, cutscene_vehicle[i]) 
									IF bStrandMission
										//PASS_OVER_CUT_SCENE_OBJECT(network_obj, sCutsceneHandle)									
									ELSE
										SET_ENTITY_VISIBLE_IN_CUTSCENE(network_obj, true)
									ENDIF
								endif 
							
							endif 
						endif 
						
					endfor 

				endif 
				
			endif 

		endfor 
	ENDIF
	
endproc 

proc clear_specific_contact_mocap_area_of_vehicles()

	switch MC_ServerBD.iEndCutscene
	
		case ciMISSION_CUTSCENE_MADRAZO
		
			CLEAR_ANGLED_AREA_OF_VEHICLES(<<-1024.496, 691.973, 160.267>>, <<-1037.496, 691.973, 164.667>>, 13.8)

		break 
		
		case ciMISSION_CUTSCENE_TREVOR
		
			CLEAR_ANGLED_AREA_OF_VEHICLES(<<1985.020, 3817.717, 31.405>>, <<1977.745, 3830.949, 34.405>>, 13.9)

		break 
		
		case ciMISSION_CUTSCENE_GERALD
		
			//not required - no vehicles for this contact
		
		break 
		
		case ciMISSION_CUTSCENE_SIMEON
		
			CLEAR_ANGLED_AREA_OF_VEHICLES(<<-34.802, -1077.292, 25.591>>, <<-19.955, -1082.696, 48.591>>, 15.200)
		
		break 
		
		
	endswitch 

endproc 

func bool is_delivered_vehicles_in_cutscene_area()

	int i
		
	switch MC_ServerBD.iEndCutscene

		case ciMISSION_CUTSCENE_SIMEON
		
			for i = 0 to count_of(cutscene_vehicle) - 1

				if does_entity_exist(cutscene_vehicle[i])
					
					if is_vehicle_driveable(cutscene_vehicle[i])
					
						if is_entity_in_angled_area(cutscene_vehicle[i], <<-34.802, -1077.292, 25.591>>, <<-19.955, -1082.696, 48.591>>, 15.200) //area of cutscene safe zone
				
							return true 

						endif 
				
					endif 
					
				endif 
			endfor  
		
		break 
		
	endswitch 
	
	return false
	
endfunc

proc reposition_specific_contact_vehicles_for_mocap()

	int i

	switch MC_ServerBD.iEndCutscene
	
		case ciMISSION_CUTSCENE_MADRAZO
		
			for i = 0 to count_of(cutscene_vehicle) - 1
			
				if does_entity_exist(cutscene_vehicle[i])
					if is_vehicle_driveable(cutscene_vehicle[i])

						set_vehicle_as_no_longer_needed(cutscene_vehicle[i])
					
					endif 
					
				endif 
			
			endfor 
		
		break 
		
		case ciMISSION_CUTSCENE_TREVOR
		
		break 
		
		case ciMISSION_CUTSCENE_GERALD
		
		break 
		
		case ciMISSION_CUTSCENE_SIMEON
		
			if is_delivered_vehicles_in_cutscene_area()
			
				for i = 0 to count_of(cutscene_vehicle) - 1
			
					if does_entity_exist(cutscene_vehicle[i])
						if is_vehicle_driveable(cutscene_vehicle[i])

							switch i 
							
								case 0
							
									set_entity_coords(cutscene_vehicle[i], <<-8.8850, -1081.7825, 25.6721>>)
									set_entity_heading(cutscene_vehicle[i], 343.9662)
								
								break 
								
								case 1
								
									set_entity_coords(cutscene_vehicle[i], <<-11.4605, -1080.9419, 25.6721>>)
									set_entity_heading(cutscene_vehicle[i], 341.1419)
								
								break 
								
							endswitch 

							set_vehicle_as_no_longer_needed(cutscene_vehicle[i])
						
						endif 
						
					endif 
				
				endfor 
			
			endif 

		break 
		
	endswitch 

endproc

PROC CLEANUP_CUTSCENE_SPECIAL_CASE_LOGIC(STRING mocapName)
	IF ARE_STRINGS_EQUAL(mocapName, "MPCAS5_EXT")
		PRINTLN("[RCC MISSION][CLEANUP_CUTSCENE_SPECIAL_CASE_LOGIC][RUN_MOCAP_CUTSCENE] - MPCAS5_EXT - casino_manager_workout")
				
		// Ensure interior index is valid
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO)
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE( structInteriorData.vPos, structInteriorData.sInteriorName )
		IF iInteriorIndex = NULL
			PRINTLN("[RCC MISSION] INTERIOR_V_CASINO_APARTMENT iInteriorIndex = NULL Set up failed")
		ENDIF		
		
		IF IS_VALID_INTERIOR(iInteriorIndex)
			IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "casino_manager_workout")
				DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "casino_manager_workout")
				PRINTLN("REMOVE_CASINO_ENTITY_SET - Deactivating: 'casino_manager_workout'")
			ELSE
				PRINTLN("REMOVE_CASINO_ENTITY_SET - BAIL: Entity set 'casino_manager_workout' is not active so can't deactivate")
			ENDIF
		ELSE
			PRINTLN("REMOVE_CASINO_ENTITY_SET - BAIL: Interior is invalid")
		ENDIF
	ENDIF
ENDPROC

PROC GIVE_PLAYER_WEAPON_AFTER_HEIST_CUTSCENE(STRING mocapscene)

	// Give player guns after  MPH_PAC_FIN_MCS0
	
	IF are_strings_equal (mocapscene, "MPH_PAC_FIN_MCS0")
	OR  are_strings_equal (mocapscene, "MPH_PAC_FIN_MCS1")
		IF( IS_ENTITY_ALIVE( LocalPlayerPed ) )
			IF NOT HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_SPECIALCARBINE)
				PRINTLN("[RSS_MISSION] MANAGE_MOCAP_CUTSCENE GIVE_PLAYER_WEAPON_AFTER_HEIST_CUTSCENE Give player a special carbine after MPH_PAC_FIN_MCS0 ")
				GIVE_WEAPON_TO_PED(LocalPlayerPed, WEAPONTYPE_DLC_SPECIALCARBINE, GET_AMOUNT_OF_AMMO_FOR_NEW_WEAPON(WEAPONTYPE_DLC_SPECIALCARBINE), TRUE)
				SET_CURRENT_PED_WEAPON(LocalPlayerPed,WEAPONTYPE_DLC_SPECIALCARBINE, true) 
			
			ELSE
				
				IF  GET_AMMO_IN_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_SPECIALCARBINE) < GET_AMOUNT_OF_AMMO_FOR_NEW_WEAPON(WEAPONTYPE_DLC_SPECIALCARBINE)
				
					
					ADD_AMMO_TO_PED(LocalPlayerPed,WEAPONTYPE_DLC_SPECIALCARBINE, GET_AMOUNT_OF_AMMO_FOR_NEW_WEAPON(WEAPONTYPE_DLC_SPECIALCARBINE))
				ENDIF
				
				SET_CURRENT_PED_WEAPON(LocalPlayerPed,WEAPONTYPE_DLC_SPECIALCARBINE, true) 
				PRINTLN("[RSS_MISSION] MANAGE_MOCAP_CUTSCENE GIVE_PLAYER_WEAPON_AFTER_HEIST_CUTSCENE Set special carbine after MPH_PAC_FIN_MCS0 ")
			ENDIF	
		ENDIF
		
	ELSE
	PRINTLN("[RSS_MISSION] MANAGE_MOCAP_CUTSCENE GIVE_PLAYER_WEAPON_AFTER_HEIST_CUTSCENE NOT Set special carbine mocapscene = ", mocapscene)
	ENDIF
ENDPROC

//PURPOSE: This function will try and put the ped into the seat
FUNC BOOL PLACE_PED_IN_VEHICLE_SEAT_IF_POSSIBLE(VEHICLE_INDEX& veh, PED_INDEX ped, VEHICLE_SEAT seat)
	
	IF NOT IS_VEHICLE_SEAT_FREE(veh, seat)
		BOOL bResult = IS_PED_SITTING_IN_VEHICLE_SEAT(ped, veh, seat)
		PRINTLN("PLACE_PED_IN_VEHICLE_SEAT_IF_POSSIBLE - seat is occupied, returning: ", BOOL_TO_STRING(bResult))
		RETURN bResult
	ENDIF
	
	IF seat != VS_DRIVER
		IF NETWORK_HAS_CONTROL_OF_ENTITY(ped)
			SET_PED_CONFIG_FLAG(ped, PCF_PreventAutoShuffleToDriversSeat, TRUE)
		ENDIF
	ENDIF

	CLEAR_PED_TASKS_IMMEDIATELY(ped) //b*2219517

	//SET_PED_INTO_VEHICLE( ped, veh, seat )
	TASK_ENTER_VEHICLE(ped, veh, -1, seat, PEDMOVEBLENDRATIO_RUN, ECF_WARP_PED | ECF_BLOCK_SEAT_SHUFFLING)

	IF NETWORK_HAS_CONTROL_OF_ENTITY(ped)
		FORCE_PED_AI_AND_ANIMATION_UPDATE(ped, TRUE, TRUE)
	ENDIF

	PRINTLN("PLACE_PED_IN_VEHICLE_SEAT_IF_POSSIBLE - ped put in seat")
	RETURN TRUE
ENDFUNC


///PURPOSE: This function will get the local player's index from the cutscene, otherwise return -1
FUNC INT GET_LOCAL_PLAYER_CUTSCENE_INDEX(INT iCutsceneTeam)

	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		RETURN 0
	ENDIF

	INT iCutscenePlayer
	FOR iCutscenePlayer = 0 TO FMMC_MAX_CUTSCENE_PLAYERS - 1
		IF MC_serverBD.piCutscenePlayerIDs[iCutsceneTeam][iCutscenePlayer] = LocalPlayer
			RETURN iCutscenePlayer
		ENDIF
	ENDFOR
	
	RETURN -1
ENDFUNC

FUNC INT GET_PLAYER_FALLBACK_CUTSCENE_INDEX()

	INT iPlayerLoop
	PLAYER_INDEX tempPlayer
	INT iIndexToReturn
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		RETURN 0
	ENDIF

	iPlayerIndexForCutscene = Get_Bitfield_Of_Players_On_Or_Joining_Mission( Get_UniqueID_For_This_Players_Mission( LocalPlayer ) )

	FOR iPlayerLoop = 0 TO ( NUM_NETWORK_PLAYERS - 1 )
		
		tempPlayer = INT_TO_PLAYERINDEX( iPlayerLoop )
		IF IS_NET_PLAYER_OK( tempPlayer, FALSE )
	       	IF tempPlayer != LocalPlayer
				IF IS_BIT_SET( iPlayerIndexForCutscene, iPlayerLoop )
					IF NOT IS_PLAYER_SCTV(tempPlayer)
					AND NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(tempPlayer)
					AND NOT IS_PLAYER_IN_PROCESS_OF_JOINING_MISSION_AS_SPECTATOR(tempPlayer)
						iIndexToReturn++
					ENDIF
				ENDIF
			ELSE // Break out if we find the local player
				iPlayerLoop = NUM_NETWORK_PLAYERS
			ENDIF
		ENDIF
	ENDFOR

	RETURN iIndexToReturn

ENDFUNC

//Turn on the vehicle Radio after the cutscenes
PROC TURN_ON_CUTSCENE_VEHICLE_RADIO()
	IF NOT IS_PED_INJURED(localPlayerPed)
		IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
			VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(localPlayerPed)			
			IF IS_VEHICLE_DRIVEABLE(vehPlayer)
				IF NOT IS_STRING_NULL_OR_EMPTY(sCurrentRadioStation)
				AND NOT ARE_STRINGS_EQUAL(sCurrentRadioStation, "OFF")
					SET_RADIO_TO_STATION_NAME(sCurrentRadioStation)
					PRINTLN("[LM][TURN_ON_CUTSCENE_VEHICLE_RADIO] - Setting Radio Station to ", sCurrentRadioStation)
				ENDIF
			ENDIF
		ENDIF		
	ENDIF
ENDPROC

//Turn off the vehicle Radio on the cutscene
PROC TURN_OFF_CUTSCENE_VEHICLE_RADIO()
	IF NOT IS_PED_INJURED(localPlayerPed)
		IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
			VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(localPlayerPed)
			
			IF IS_VEHICLE_DRIVEABLE(vehPlayer)
				STRING sStationName = GET_PLAYER_RADIO_STATION_NAME()
				IF IS_STRING_NULL_OR_EMPTY(sStationName)
				OR NOT ARE_STRINGS_EQUAL(sStationName, "OFF")
					// Cache it to re-enable it after cutscene.
					sCurrentRadioStation = sStationName
					SET_RADIO_TO_STATION_NAME("OFF")
					PRINTLN("[LM][TURN_OFF_CUTSCENE_VEHICLE_RADIO] - Setting Radio Station to OFF and caching the previous station as :", sCurrentRadioStation)
				ENDIF
			ENDIF
		ENDIF		
	ENDIF
ENDPROC

//Turn off the vehicle engine on the cutscene
PROC TURN_OFF_CUTSCENE_VEHICLE_ENGINE(STRING sHandle, BOOL bImmediately = TRUE)

	ENTITY_INDEX entH = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY( sHandle ) 
	
	IF( DOES_ENTITY_EXIST( entH ) )
		IF( IS_ENTITY_A_VEHICLE( entH ) )
			VEHICLE_INDEX vH = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX( entH )
			IF IS_VEHICLE_DRIVEABLE(vH)
			AND GET_IS_VEHICLE_ENGINE_RUNNING(vH)
			AND NETWORK_HAS_CONTROL_OF_ENTITY(vH)
				SET_VEHICLE_ENGINE_ON(vH, FALSE, bImmediately)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//Turn off the vehicle engine on the cutscene
PROC TURN_ON_CUTSCENE_HELI_ENGINE_AND_BLADES(STRING sHandle, BOOL bImmediately = TRUE)

	ENTITY_INDEX entH = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY( sHandle ) 
	
	IF( DOES_ENTITY_EXIST( entH ) )
		IF( IS_ENTITY_A_VEHICLE( entH ) )
			VEHICLE_INDEX vH = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX( entH )
			IF NOT IS_ENTITY_DEAD(vH)

				SET_VEHICLE_ENGINE_ON(vH, TRUE, bImmediately)
				PRINTLN("[PED CUT] [ROSSW] TURN_ON_CUTSCENE_HELI_ENGINE_AND_BLADES - ENGINE ON!!")
				SET_HELI_BLADES_FULL_SPEED(vH)
				PRINTLN("[PED CUT] [ROSSW] TURN_ON_CUTSCENE_HELI_ENGINE_AND_BLADES - BLADES ON!")

			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

VEHICLE_INDEX bioLabcutsceneVehicles[4]

PROC HANDLE_CUTSCENE_ENTITIES(STRING sMocapName)
		STRING strHumaneDelivery 		= "MPH_HUM_DEL_EXT"
		ENTITY_INDEX eiTemp
		IF ARE_STRINGS_EQUAL( sMocapName, strHumaneDelivery )
			IF NOT DOES_ENTITY_EXIST(bioLabcutsceneVehicles[0])
				eiTemp = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MPH_rear_mesa", MESA3)
				IF DOES_ENTITY_EXIST(eiTemp)
					bioLabcutsceneVehicles[0] = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(eiTemp)
					IF IS_VEHICLE_DRIVEABLE(bioLabcutsceneVehicles[0])
						SET_VEHICLE_LIGHTS(bioLabcutsceneVehicles[0], FORCE_VEHICLE_LIGHTS_ON)
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(bioLabcutsceneVehicles[1])
				eiTemp = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MPH_middle_mesa", MESA3)
				IF DOES_ENTITY_EXIST(eiTemp)
					bioLabcutsceneVehicles[1] = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(eiTemp)
					IF IS_VEHICLE_DRIVEABLE(bioLabcutsceneVehicles[1])
						SET_VEHICLE_LIGHTS(bioLabcutsceneVehicles[1], FORCE_VEHICLE_LIGHTS_ON)
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(bioLabcutsceneVehicles[2])
				eiTemp = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("EXL_mesa3", MESA3)
				IF DOES_ENTITY_EXIST(eiTemp)
					bioLabcutsceneVehicles[2] = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(eiTemp)
					IF IS_VEHICLE_DRIVEABLE(bioLabcutsceneVehicles[2])
						SET_VEHICLE_LIGHTS(bioLabcutsceneVehicles[2], FORCE_VEHICLE_LIGHTS_ON)
					ENDIF
				ELSE
					eiTemp = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MPH_lead_mesa", MESA3) // Just in case CS decide to rename the vehicle back to this
					
					IF DOES_ENTITY_EXIST(eiTemp)
						bioLabcutsceneVehicles[2] = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(eiTemp)
						IF IS_VEHICLE_DRIVEABLE(bioLabcutsceneVehicles[2])
							SET_VEHICLE_LIGHTS(bioLabcutsceneVehicles[2], FORCE_VEHICLE_LIGHTS_ON)
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			IF NOT DOES_ENTITY_EXIST(bioLabcutsceneVehicles[3])
				eiTemp = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MPH_Boxville", BOXVILLE3)
				IF DOES_ENTITY_EXIST(eiTemp)
					bioLabcutsceneVehicles[3] = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(eiTemp)
					IF IS_VEHICLE_DRIVEABLE(bioLabcutsceneVehicles[3])
						SET_VEHICLE_LIGHTS(bioLabcutsceneVehicles[3], FORCE_VEHICLE_LIGHTS_ON)
					ENDIF
				ENDIF
			ENDIF
			
			// Rob B - 2164697 - extend light distance on convoy vehicles
			SET_LIGHTS_CUTOFF_DISTANCE_TWEAK(300)
		ENDIF

ENDPROC

BOOL bRemoveHeistBagForCutscene

///PURPOSE: This function deals with all cutscene events for mocaps
PROC LISTEN_FOR_CUTSCENE_EVENTS( STRING sMocapName )
	
	PED_INDEX tempPed
	ENTITY_INDEX tempEntity
	
	STRING strTutMid 			= "MPH_TUT_MID"
	STRING strTutExt 			= "MPH_TUT_EXT"
	STRING strHumaneEMPExt		= "MPH_HUM_EMP_EXT"
	STRING strHumaneFinale 		= "MPH_HUM_FIN_EXT"
	STRING strHumaneValcFinale	= "MPH_HUM_VAL_EXT"
	STRING strNarcoticsFinale	= "MPH_NAR_FIN_EXT"
	STRING strFleecaFinaleExt 	= "mph_tut_ext"				
	STRING strPacificHackMid    = "mph_pac_hac_mcs1"
	STRING strPacificConvoyExt  = "mph_pac_con_ext"
	STRING strPacificFinaleMid 	= "MPH_PAC_FIN_MCS0"
	STRING strPacificFinale 	= "MPH_PAC_FIN_EXT"
	
	STRING strPacificBikeExt 	= "mph_pac_bik_ext"
	
			
	IF ARE_STRINGS_EQUAL(sMocapName, strTutExt)
		IF GET_CUTSCENE_SECTION_PLAYING() >= 2
		OR GET_CUTSCENE_TIME() > 17000
			IF GET_PLAYER_WANTED_LEVEL(localPLayer) > 0
				SET_PLAYER_WANTED_LEVEL(LocalPlayer, 0)
				SET_PLAYER_WANTED_LEVEL_NOW(LocalPlayer)
			ENDIF
			
			SCRIPT_TRIGGER_MUSIC_EVENT_STOP()
							
		ENDIF		
	ENDIF
	
	
	//2106652
	IF ARE_STRINGS_EQUAL(sMocapName, strPacificHackMid)
		IF (GET_CUTSCENE_TIME() > 5579 AND GET_CUTSCENE_TIME() < 10699)
		OR (GET_CUTSCENE_TIME() > 13670)
			SET_PARTICLE_FX_CAM_INSIDE_VEHICLE(TRUE)
		ELSE
			SET_PARTICLE_FX_CAM_INSIDE_VEHICLE(FALSE)
		ENDIF		
		
		//NG only - 2200031
		tempEntity = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("server1")		
		IF DOES_ENTITY_EXIST(tempEntity)			
			SET_ENTITY_IS_IN_VEHICLE(tempEntity)
		ENDIF
		
		tempEntity = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("server_2")		
		IF DOES_ENTITY_EXIST(tempEntity)						
			SET_ENTITY_IS_IN_VEHICLE(tempEntity)
		ENDIF
		
	ENDIF
	
	//Fix the hydra for: 2137746
	IF  ARE_STRINGS_EQUAL( sMocapName, "mph_hum_emp_ext")		
		
		tempEntity = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("hydra")		
		IF DOES_ENTITY_EXIST(tempEntity)
			
			IF NOT IS_ENTITY_DEAD(tempEntity)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(tempEntity)
					SET_VEHICLE_DEFORMATION_FIXED(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(tempEntity))	
				ENDIF
			ENDIF
		ENDIF
		
		//Fix for 2121654
		tempEntity = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("insurgent2")		
		IF DOES_ENTITY_EXIST(tempEntity)
			IF NOT IS_ENTITY_DEAD(tempEntity)
			AND IS_ENTITY_A_VEHICLE(tempEntity)
				
				VEHICLE_INDEX tempVeh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(tempEntity)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
					SET_VEHICLE_DEFORMATION_FIXED(tempVeh)
				ENDIF
				//SET_VEHICLE_DOORS_SHUT(tempVeh, TRUE)
			ENDIF
		ENDIF
		
	ENDIF
	
	IF ARE_STRINGS_EQUAL( sMocapName, strFleecaFinaleExt )
	OR ARE_STRINGS_EQUAL( sMocapName, strPacificFinaleMid )
	
		IF DOES_ENTITY_EXIST(GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_1")))
		AND bRemoveHeistBagForCutscene = FALSE
			IF NOT IS_ENTITY_DEAD(GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_1")))
				REMOVE_DUFFEL_BAG_HEIST_GEAR(GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_1")), iLocalPart)
				PRINTLN("[PED CUT] [ROSSW] REMOVE_DUFFEL_BAG_HEIST_GEAR(GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY(\"MP_1\"))))")
			ENDIF
			bRemoveHeistBagForCutscene = TRUE
		ENDIF
	ENDIF
	
	IF ARE_STRINGS_EQUAL( sMocapName, strPacificFinaleMid )
		
		IF GET_CUTSCENE_TEAM() <> -1
				
			//Listen for mask event
			tempEntity = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_1")		
			IF DOES_ENTITY_EXIST(tempEntity)
				IF NOT IS_ENTITY_DEAD(tempEntity)
					IF HAS_ANIM_EVENT_FIRED(tempEntity, GET_HASH_KEY("MP_M_Freemode_01_Mask_On"))					
						sApplyDataPacificBankCut1.pedID        = GET_PED_INDEX_FROM_ENTITY_INDEX(tempEntity)
						sApplyDataPacificBankCut1.eApplyStage  = AOS_SET         // Force stage to setting stage (skips the preload)		
						sApplyDataPacificBankCut1.iPlayerIndex = NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_PARTICIPANT_INDEX(MC_serverBD.piCutscenePlayerIDs[GET_CUTSCENE_TEAM()][0])))	
						
						sApplyDataPacificBankCut1.eMask        = INT_TO_ENUM(MP_OUTFIT_MASK_ENUM, GET_PLAYER_CHOSEN_MASK(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_PARTICIPANT_INDEX(MC_serverBD.piCutscenePlayerIDs[GET_CUTSCENE_TEAM()][0]))))
						
						SET_PED_MP_OUTFIT(sApplyDataPacificBankCut1, TRUE, TRUE)														
						//Put the component bag back on after.
						SET_MP_HEIST_GEAR(sApplyDataPacificBankCut1.pedID, MC_playerBD_1[iLocalPart].eHeistGearBagType,sApplyDataPacificBankCut1.iPlayerIndex)										
						PRINTLN("[PED CUT] [ROSSW] mask 1 on MP_1 ", GET_MP_OUTFIT_MASK_NAME(sApplyDataPacificBankCut1.eMask))
					ENDIF
				ENDIF
			ENDIF
			tempEntity	= GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_2")
			IF DOES_ENTITY_EXIST(tempEntity)
				IF NOT IS_ENTITY_DEAD(tempEntity)
					IF HAS_ANIM_EVENT_FIRED(tempEntity, GET_HASH_KEY("MP_M_Freemode_01^1_Mask_On"))
						sApplyDataPacificBankCut2.pedID        	= GET_PED_INDEX_FROM_ENTITY_INDEX(tempEntity)
						sApplyDataPacificBankCut2.eApplyStage 	= AOS_SET         // Force stage to setting stage (skips the preload)
						sApplyDataPacificBankCut2.iPlayerIndex 	= NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_PARTICIPANT_INDEX(MC_serverBD.piCutscenePlayerIDs[GET_CUTSCENE_TEAM()][1])))	
						sApplyDataPacificBankCut2.eMask        	= INT_TO_ENUM(MP_OUTFIT_MASK_ENUM, GET_PLAYER_CHOSEN_MASK(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_PARTICIPANT_INDEX(MC_serverBD.piCutscenePlayerIDs[GET_CUTSCENE_TEAM()][1]))))
						
						SET_PED_MP_OUTFIT(sApplyDataPacificBankCut2, TRUE, TRUE)
						PRINTLN("[PED CUT] [ROSSW] mask 2 on MP_2 ", GET_MP_OUTFIT_MASK_NAME(sApplyDataPacificBankCut2.eMask))
					ENDIF
				ENDIF
			ENDIF
			tempEntity = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_3")
			IF DOES_ENTITY_EXIST(tempEntity)
				IF NOT IS_ENTITY_DEAD(tempEntity)
					IF HAS_ANIM_EVENT_FIRED(tempEntity, GET_HASH_KEY("MP_M_Freemode_01^2_Mask_On"))
						sApplyDataPacificBankCut3.pedID        	= GET_PED_INDEX_FROM_ENTITY_INDEX(tempEntity)
						sApplyDataPacificBankCut3.eApplyStage 	= AOS_SET         // Force stage to setting stage (skips the preload)
						sApplyDataPacificBankCut3.iPlayerIndex 	= NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_PARTICIPANT_INDEX(MC_serverBD.piCutscenePlayerIDs[GET_CUTSCENE_TEAM()][2])))	
						sApplyDataPacificBankCut3.eMask        	= INT_TO_ENUM(MP_OUTFIT_MASK_ENUM, GET_PLAYER_CHOSEN_MASK(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_PARTICIPANT_INDEX(MC_serverBD.piCutscenePlayerIDs[GET_CUTSCENE_TEAM()][2]))))
						
						SET_PED_MP_OUTFIT(sApplyDataPacificBankCut3, TRUE, TRUE)
						PRINTLN("[PED CUT] [ROSSW] mask 3 on MP_3 ", GET_MP_OUTFIT_MASK_NAME(sApplyDataPacificBankCut3.eMask))
					ENDIF		
				ENDIF
			ENDIF
			tempEntity= GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_4")
			IF DOES_ENTITY_EXIST(tempEntity)
				IF NOT IS_ENTITY_DEAD(tempEntity)					
					IF HAS_ANIM_EVENT_FIRED(tempEntity, GET_HASH_KEY("MP_F_Freemode_01_Mask_On"))
						sApplyDataPacificBankCut4.pedID        	= GET_PED_INDEX_FROM_ENTITY_INDEX(tempEntity)
						sApplyDataPacificBankCut4.eApplyStage 	= AOS_SET         // Force stage to setting stage (skips the preload)
						sApplyDataPacificBankCut4.iPlayerIndex 	= NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_PARTICIPANT_INDEX(MC_serverBD.piCutscenePlayerIDs[GET_CUTSCENE_TEAM()][3])))	
						sApplyDataPacificBankCut4.eMask        	= INT_TO_ENUM(MP_OUTFIT_MASK_ENUM, GET_PLAYER_CHOSEN_MASK(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_PARTICIPANT_INDEX(MC_serverBD.piCutscenePlayerIDs[GET_CUTSCENE_TEAM()][3]))))
						
						SET_PED_MP_OUTFIT(sApplyDataPacificBankCut4, TRUE, TRUE)
						PRINTLN("[PED CUT] [ROSSW] mask 4 on MP_4 ", GET_MP_OUTFIT_MASK_NAME(sApplyDataPacificBankCut4.eMask))
					ENDIF					
				ENDIF
			ENDIF
		
			IF sApplyDataPacificBankCut5.pedID = NULL
			AND sApplyDataPacificBankCut4.pedID <> NULL	//Wait until all clones have masks on then put on local mask.
			AND sApplyDataPacificBankCut3.pedID <> NULL
			AND sApplyDataPacificBankCut2.pedID <> NULL
			AND sApplyDataPacificBankCut1.pedID <> NULL
				sApplyDataPacificBankCut5.pedID        = LocalPlayerPed
				sApplyDataPacificBankCut5.eMask        = INT_TO_ENUM(MP_OUTFIT_MASK_ENUM, GET_PLAYER_CHOSEN_MASK(LocalPlayer))
				sApplyDataPacificBankCut5.eApplyStage = AOS_SET         // Force stage to setting stage (skips the preload)
				SET_PED_MP_OUTFIT(sApplyDataPacificBankCut5, TRUE, TRUE)
				PRINTLN("[PED CUT] [ROSSW] Mask onto Local player ", GET_MP_OUTFIT_MASK_NAME(sApplyDataPacificBankCut4.eMask))
			ENDIF
			
		ENDIF
		
	ELIF ARE_STRINGS_EQUAL( sMocapName, strPacificFinale )

		IF IS_CUTSCENE_PLAYING()
			IF GET_CUTSCENE_TIME() > 5000 //Fix for 2148747
				SET_WIDESCREEN_BORDERS(FALSE, 0)
				DISPLAY_HUD_WHEN_NOT_IN_STATE_OF_PLAY_THIS_FRAME()
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WANTED_STARS)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
				
	
				//Get rid of masks after fade
				tempEntity = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_1")		
				IF DOES_ENTITY_EXIST(tempEntity)
					IF NOT IS_ENTITY_DEAD(tempEntity)
						sApplyDataPacificBankCut1.pedID       	= GET_PED_INDEX_FROM_ENTITY_INDEX(tempEntity)
						sApplyDataPacificBankCut1.iPlayerIndex 	= NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_PARTICIPANT_INDEX(MC_serverBD.piCutscenePlayerIDs[MC_playerBD[iPartToUse].iteam][0])))	
						sApplyDataPacificBankCut1.eMask        	= OUTFIT_MASK_NONE
						IF sApplyDataPacificBankCut1.bOutfitPrevSet = FALSE
							IF SET_PED_MP_OUTFIT(sApplyDataPacificBankCut1, TRUE, TRUE)											
								sApplyDataPacificBankCut1.bOutfitPrevSet = TRUE
							ENDIF
						ENDIF
						PRINTLN("[PED CUT] [ROSSW] pacific finale mask 1 OFF MP_1 ", GET_MP_OUTFIT_MASK_NAME(sApplyDataPacificBankCut1.eMask))					
					ENDIF
				ENDIF
				tempEntity	= GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_2")
				IF DOES_ENTITY_EXIST(tempEntity)
					IF NOT IS_ENTITY_DEAD(tempEntity)					
						sApplyDataPacificBankCut2.pedID        	= GET_PED_INDEX_FROM_ENTITY_INDEX(tempEntity)
						sApplyDataPacificBankCut2.iPlayerIndex 	= NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_PARTICIPANT_INDEX(MC_serverBD.piCutscenePlayerIDs[MC_playerBD[iPartToUse].iteam][1])))	
						sApplyDataPacificBankCut2.eMask        	= OUTFIT_MASK_NONE						
						IF sApplyDataPacificBankCut2.bOutfitPrevSet = FALSE
							IF SET_PED_MP_OUTFIT(sApplyDataPacificBankCut2, TRUE, TRUE)											
								sApplyDataPacificBankCut2.bOutfitPrevSet = TRUE
							ENDIF
						ENDIF
						PRINTLN("[PED CUT] [ROSSW] pacific finale mask 2 OFF MP_2 ", GET_MP_OUTFIT_MASK_NAME(sApplyDataPacificBankCut2.eMask))					
					ENDIF
				ENDIF
				tempEntity = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_3")
				IF DOES_ENTITY_EXIST(tempEntity)
					IF NOT IS_ENTITY_DEAD(tempEntity)					
						sApplyDataPacificBankCut3.pedID        	= GET_PED_INDEX_FROM_ENTITY_INDEX(tempEntity)
						sApplyDataPacificBankCut3.eMask        	= OUTFIT_MASK_NONE
						sApplyDataPacificBankCut3.iPlayerIndex 	= NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_PARTICIPANT_INDEX(MC_serverBD.piCutscenePlayerIDs[MC_playerBD[iPartToUse].iteam][2])))	
						IF sApplyDataPacificBankCut3.bOutfitPrevSet = FALSE
							IF SET_PED_MP_OUTFIT(sApplyDataPacificBankCut3, TRUE, TRUE)											
								sApplyDataPacificBankCut3.bOutfitPrevSet = TRUE
							ENDIF
						ENDIF
						PRINTLN("[PED CUT] [ROSSW] pacific finale mask 3 OFF MP_3 ", GET_MP_OUTFIT_MASK_NAME(sApplyDataPacificBankCut3.eMask))					
					ENDIF
				ENDIF
				tempEntity= GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_4")
				IF DOES_ENTITY_EXIST(tempEntity)
					IF NOT IS_ENTITY_DEAD(tempEntity)										
						sApplyDataPacificBankCut4.pedID        	= GET_PED_INDEX_FROM_ENTITY_INDEX(tempEntity)
						sApplyDataPacificBankCut4.eMask        	= OUTFIT_MASK_NONE
						sApplyDataPacificBankCut4.iPlayerIndex 	= NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_PARTICIPANT_INDEX(MC_serverBD.piCutscenePlayerIDs[MC_playerBD[iPartToUse].iteam][3])))	
						IF sApplyDataPacificBankCut4.bOutfitPrevSet = FALSE
							IF SET_PED_MP_OUTFIT(sApplyDataPacificBankCut4, TRUE, TRUE)											
								sApplyDataPacificBankCut4.bOutfitPrevSet = TRUE
							ENDIF
						ENDIF
						PRINTLN("[PED CUT] [ROSSW] pacific finale mask 4 OFF MP_4 ", GET_MP_OUTFIT_MASK_NAME(sApplyDataPacificBankCut4.eMask))					
					ENDIF
				ENDIF		
			ENDIF
			
			IF GET_CUTSCENE_SECTION_PLAYING() >= 2
			OR GET_CUTSCENE_TIME() > 10000
				CASCADE_SHADOWS_INIT_SESSION()
			ENDIF
			
			IF GET_CUTSCENE_SECTION_PLAYING() >= 2
			OR GET_CUTSCENE_TIME() > 17000
				IF GET_PLAYER_WANTED_LEVEL(localPLayer) > 0
					SET_PLAYER_WANTED_LEVEL(LocalPlayer, 0)
					SET_PLAYER_WANTED_LEVEL_NOW(LocalPlayer)
				ENDIF
			ENDIF
			
			//Lukasz: For MPH_PAC_FIN_EXT just remove bags and parachutes from peds in the cutscene, see B*2174310
			//Call the native command here to force the variation,
			//might need some extra checks or script commands if it causes other side effects with variations
			IF GET_CUTSCENE_SECTION_PLAYING() >= 2
			OR GET_CUTSCENE_TIME() > 3000
				IF GET_PED_DRAWABLE_VARIATION(LocalPlayerPed, PED_COMP_HAND) != 0
					SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_HAND, 0, 0)
					PRINTLN("[Heist][Cutscene] Removing player PED_COMP_HAND variation.") 
				ENDIF
			ENDIF
			
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
			IF HAS_ANIM_EVENT_FIRED( LocalPlayerPed, GET_HASH_KEY( "CASH" ) )
			OR HAS_ANIM_EVENT_FIRED( LocalPlayerPed, GET_HASH_KEY( "RULES" ) )
			OR HAS_ANIM_EVENT_FIRED( LocalPlayerPed, GET_HASH_KEY( "EVERYTHING" ) )
			OR HAS_ANIM_EVENT_FIRED( LocalPlayerPed, GET_HASH_KEY( "AROUND_ME" ) )
				PRINTLN("[ROSSW][Heist][Cutscene] giving money during cutscene") 
				
#IF FEATURE_GTAO_MEMBERSHIP
				PLAYER_INDEX piHeistLeader = INVALID_PLAYER_INDEX()
				INT iPart
				REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iPart
				
					IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_HEIST_HOST)
						RELOOP
					ENDIF
					
					piHeistLeader = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart))
					BREAKLOOP
				ENDREPEAT
#ENDIF
				
				USE_FAKE_MP_CASH(TRUE)
				CHANGE_FAKE_MP_CASH(0, CALCULATE_HEIST_FINALE_CASH_REWARD( TRUE, TRUE, MC_serverBD.iTotalMissionEndTime, #IF FEATURE_GTAO_MEMBERSHIP piHeistLeader, TRUE, #ENDIF MC_playerBD[NATIVE_TO_INT(LocalPlayer)].TSPlayerData.iCost,MC_serverBD.iCashGrabTotalDrop)) 					
				PLAY_SOUND_FRONTEND(-1, "Payment_Player", "DLC_HEISTS_GENERIC_SOUNDS", FALSE)
			ENDIF
		ENDIF
		
		//b*2165572
		TEXT_LABEL_7 tlEventPlayerHandle
		INT icount
		
		FOR icount = 1 to 4
			tlEventPlayerHandle = "MP_"
			tlEventPlayerHandle += icount
			
			ENTITY_INDEX entEvent = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY(tlEventPlayerHandle)
			PED_INDEX pedEvent = GET_PED_INDEX_FROM_ENTITY_INDEX(entEvent)
			
			IF LocalPlayerPed != pedEvent
				IF NOT IS_ENTITY_DEAD(entEvent)
					IF HAS_ANIM_EVENT_FIRED( entEvent, GET_HASH_KEY( "CASH" ) )
					OR HAS_ANIM_EVENT_FIRED( entEvent, GET_HASH_KEY( "RULES" ) )
					OR HAS_ANIM_EVENT_FIRED( entEvent, GET_HASH_KEY( "EVERYTHING" ) )
					OR HAS_ANIM_EVENT_FIRED( entEvent, GET_HASH_KEY( "AROUND_ME" ) )
						PRINTLN("[Heist][Cutscene] Giving Money to Non-Player during cutscene")
						PLAY_SOUND_FRONTEND(-1, "Payment_Non_Player", "DLC_HEISTS_GENERIC_SOUNDS", FALSE)
					ENDIF
				ENDIF
			ENDIF
			
		ENDFOR
		
	ELIF ARE_STRINGS_EQUAL( sMocapName, strTutMid )
		IF NOT ( HAS_CUTSCENE_FINISHED() )
				
			DISABLE_HUD()
			
			tempEntity = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Lester") 
			
			IF DOES_ENTITY_EXIST(tempEntity) 
				tempPed = GET_PED_INDEX_FROM_ENTITY_INDEX(tempEntity) 

				SWITCH GET_HEIST_TUT_CUTS_MIDPOINT_BOARD_STATE()
					CASE HEIST_TUT_CUTS_NONE
						PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_PLAYING_CUTSCENE - Tut cuts state is DEFAULT, move to EMPTY.")
						SET_HEIST_TUT_CUTS_MIDPOINT_BOARD_STATE(HEIST_TUT_CUTS_EMPTY)
					BREAK
					
					CASE HEIST_TUT_CUTS_EMPTY
						IF HAS_ANIM_EVENT_FIRED(tempPed, GET_HASH_KEY("MARKER_SCALEFORM"))
							PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_PLAYING_CUTSCENE - HAS_ANIM_EVENT_FIRED = TRUE for event MARKER_SCALEFORM, setting board cuts state to TEXT.")
							SET_HEIST_TUT_CUTS_MIDPOINT_BOARD_STATE(HEIST_TUT_CUTS_TEXT)
						ENDIF
					BREAK
						
					CASE HEIST_TUT_CUTS_TEXT
						IF HAS_ANIM_EVENT_FIRED(tempPed, GET_HASH_KEY("FOTOS_SCALEFORM"))
							PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_PLAYING_CUTSCENE - HAS_ANIM_EVENT_FIRED = TRUE for event FOTOS_SCALEFORM, setting board cuts state to PICTURES.")
							SET_HEIST_TUT_CUTS_MIDPOINT_BOARD_STATE(HEIST_TUT_CUTS_PICTURES)
						ENDIF
					BREAK
						
					CASE HEIST_TUT_CUTS_PICTURES
						IF HAS_ANIM_EVENT_FIRED(tempPed, GET_HASH_KEY("MAP_SCALEFORM"))
							PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_PLAYING_CUTSCENE - HAS_ANIM_EVENT_FIRED = TRUE for event MAP_SCALEFORM, setting board cuts state to MAP.")
							SET_HEIST_TUT_CUTS_MIDPOINT_BOARD_STATE(HEIST_TUT_CUTS_MAP)
						ENDIF
					BREAK
				ENDSWITCH
			ELSE 
				PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_PLAYING_CUTSCENE - 'Lester' entity does not exist; cannot check for events!")
			ENDIF

		ENDIF
	ELIF ARE_STRINGS_EQUAL( sMocapName, strHumaneFinale )
		PRINTLN("[MJM][Cutscene] - LISTEN_FOR_CUTSCENE_EVENTS - Humane Finale")
		
		ENTITY_INDEX entHeli = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY( "valkyrie" )
		ENTITY_INDEX entPreCutHeli = NET_TO_ENT(GET_CUTSCENE_NET_ID_WITH_HANDLE("valkyrie", -1, TRUE))

		IF DOES_ENTITY_EXIST(entPreCutHeli)
			VEHICLE_INDEX vehPreCutHeli = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entPreCutHeli)
			
			IF GET_IS_VEHICLE_ENGINE_RUNNING(vehPreCutHeli)
				SET_VEHICLE_ENGINE_ON(vehPreCutHeli, FALSE, TRUE)
				SET_HELI_BLADES_SPEED(vehPreCutHeli, 0.0)
				PRINTLN("[RCC MISSION] mph_hum_fin_ext - PRE CUT HELI ENGINES TURNED OFF")
			ENDIF
		ENDIF
		
		IF( DOES_ENTITY_EXIST( entHeli ) )
			
			TURN_OFF_CUTSCENE_VEHICLE_ENGINE("valkyrie")
			
			BOOL bBlowHeliUpNow
		
			PRINTLN("[MJM][Cutscene] - LISTEN_FOR_CUTSCENE_EVENTS - DOES_ENTITY_EXIST( entHeli )")
			IF NOT IS_ENTITY_DEAD(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_1"))
				IF HAS_ANIM_EVENT_FIRED(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_1"), GET_HASH_KEY("KABOOM")) 
					bBlowHeliUpNow = TRUE
				ENDIF
			ENDIF
			IF NOT IS_ENTITY_DEAD(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_2"))
				IF HAS_ANIM_EVENT_FIRED(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_2"), GET_HASH_KEY("KABOOM"))
					bBlowHeliUpNow = TRUE
				ENDIF
			ENDIF
			IF NOT IS_ENTITY_DEAD(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_3"))
				IF HAS_ANIM_EVENT_FIRED(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_3"), GET_HASH_KEY("KABOOM")) 
					bBlowHeliUpNow = TRUE
				ENDIF
			ENDIF
			IF NOT IS_ENTITY_DEAD(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_4"))
				IF HAS_ANIM_EVENT_FIRED(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_4"), GET_HASH_KEY("KABOOM")) 
					bBlowHeliUpNow = TRUE
				ENDIF
			ENDIF
			
			IF bBlowHeliUpNow
				
				PRINTLN("[MJM][Cutscene] - LISTEN_FOR_CUTSCENE_EVENTS - HAS_ANIM_EVENT_FIRED")
				IF( IS_ENTITY_A_VEHICLE( entHeli ) )
					PRINTLN("[MJM][Cutscene] - LISTEN_FOR_CUTSCENE_EVENTS - IS_ENTITY_A_VEHICLE( entHeli )")
					VEHICLE_INDEX vehHeli = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX( entHeli )
					IF( IS_VEHICLE_DRIVEABLE( vehHeli ) )
						PRINTLN("[MJM][Cutscene] - LISTEN_FOR_CUTSCENE_EVENTS - IS_VEHICLE_DRIVEABLE( vehHeli )")
						#IF IS_NEXTGEN_BUILD
						ADD_EXPLOSION_WITH_USER_VFX(GET_ENTITY_COORDS( vehHeli, FALSE ), EXP_TAG_PLANE,	HASH("EXP_VFXTAG_HUMANE_VAL"), 1.0)
						#ENDIF
						#IF NOT IS_NEXTGEN_BUILD
						EXPLODE_VEHICLE_IN_CUTSCENE( vehHeli )
						#ENDIF
						PRINTLN("[MJM] EXPLODING HELICOPTER")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELIF ARE_STRINGS_EQUAL( sMocapName, strNarcoticsFinale )
		
//		INT i
		
		ENTITY_INDEX gangVan = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY( "gang_Van" ) 
		IF DOES_ENTITY_EXIST(gangVan) 
			IF NOT IS_ENTITY_DEAD(gangVan)
				IF GET_VEHICLE_LIVERY_COUNT(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(gangVan)) > 0			
					SET_VEHICLE_LIVERY(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(gangVan), 1)
					SET_VEHICLE_COLOUR_COMBINATION(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(gangVan), 4)				
				ENDIF
			ENDIF
		ENDIF
		
		TURN_OFF_CUTSCENE_VEHICLE_ENGINE( "gang_Van" )	
		
		
		TURN_OFF_CUTSCENE_VEHICLE_ENGINE( "Mule_01" )	
		TURN_OFF_CUTSCENE_VEHICLE_ENGINE( "Mule_02" )	
		TURN_OFF_CUTSCENE_VEHICLE_ENGINE( "technical" )	
		
		ENTITY_INDEX entTrevor = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY( "TREVOR" ) 
		IF( DOES_ENTITY_EXIST( entTrevor ) )
			IF( IS_ENTITY_A_PED( entTrevor ) )
				PED_INDEX pedTrevor = GET_PED_INDEX_FROM_ENTITY_INDEX( entTrevor )
				IF( NOT IS_PED_DEAD_OR_DYING( pedTrevor ) )
					IF( HAS_ANIM_EVENT_FIRED( pedTrevor, GET_HASH_KEY( "TREVOR_WET" ) ) )
						CPRINTLN( DEBUG_SIMON, "LISTEN_FOR_CUTSCENE_EVENTS - Wetting Trevor" )													
						SET_PED_WETNESS_HEIGHT( pedTrevor, 2.0 )				
					ENDIF
				ENDIF

				IF( HAS_ANIM_EVENT_FIRED( pedTrevor, GET_HASH_KEY( "TIME_ADVANCE" ) ) )
					SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciFORCE_SPECIFIC_TOD)
					g_FMMC_STRUCT.iTODOverrideHours = 17
					iTODh = 17
					g_FMMC_STRUCT.iTimeOfDay = TIME_OFF
					MC_serverBD.iTimeOfDay = g_FMMC_STRUCT.iTimeOfDay
					NETWORK_OVERRIDE_CLOCK_TIME(g_FMMC_STRUCT.iTODOverrideHours,  g_FMMC_STRUCT.iTODOverrideMinutes, 0)
				ENDIF
			ENDIF
		ENDIF

	ELIF ARE_STRINGS_EQUAL( sMocapName, "mph_nar_met_ext")
		TURN_OFF_CUTSCENE_VEHICLE_ENGINE( "PHANTOM" )			
	ELIF ARE_STRINGS_EQUAL( sMocapName,"mph_nar_bik_ext")
		TURN_OFF_CUTSCENE_VEHICLE_ENGINE( "gang_van" )	
		TURN_OFF_CUTSCENE_VEHICLE_ENGINE( "gang_van_2" )	
	ELIF ARE_STRINGS_EQUAL( sMocapName, strHumaneValcFinale )
		TURN_ON_CUTSCENE_HELI_ENGINE_AND_BLADES( "valkyrie" )
	ELIF ARE_STRINGS_EQUAL( sMocapName, strPacificConvoyExt)
		TURN_OFF_CUTSCENE_VEHICLE_ENGINE( "TRUCK" )		
	ELIF ARE_STRINGS_EQUAL( sMocapName,strPacificBikeExt)
		TURN_OFF_CUTSCENE_VEHICLE_ENGINE( "MPH_bike_1" )		
		TURN_OFF_CUTSCENE_VEHICLE_ENGINE( "MPH_bike_2" )		
		TURN_OFF_CUTSCENE_VEHICLE_ENGINE( "MPH_bike_3" )		
		TURN_OFF_CUTSCENE_VEHICLE_ENGINE( "MPH_bike_4" )		
	ELIF ARE_STRINGS_EQUAL( sMocapName, strHumaneEMPExt )
		ENTITY_INDEX entInsurg = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("insurgent2")
		
		// Reset flag for peds in vehicle
		IF DOES_ENTITY_EXIST(entInsurg)
			VEHICLE_INDEX vehInsurg = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entInsurg)
			IF DOES_ENTITY_EXIST(vehInsurg)
				PRINTLN("[RCC MISSION] MPH_HUM_EMP_EXT Setting flag for ped in Insurgent")
				PED_INDEX pedAgent
				INT i
				FOR i = ENUM_TO_INT(VS_DRIVER) TO ENUM_TO_INT(VS_BACK_RIGHT)
					pedAgent = GET_PED_IN_VEHICLE_SEAT(vehInsurg, INT_TO_ENUM(VEHICLE_SEAT, i))
					
					IF DOES_ENTITY_EXIST(pedAgent)
					AND NOT IS_PED_INJURED(pedAgent)
					AND NETWORK_HAS_CONTROL_OF_ENTITY(pedAgent)
						SET_PED_RESET_FLAG(pedAgent, PRF_DisableInVehicleActions, TRUE)
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
	ELIF ARE_STRINGS_EQUAL( sMocapName, "mph_pac_pho_ext" )
		ENTITY_INDEX entVan = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Van")
		
		// Reset flag for peds in vehicle
		IF DOES_ENTITY_EXIST(entVan)
			VEHICLE_INDEX vehVan = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entVan)
			IF DOES_ENTITY_EXIST(vehVan)
				PRINTLN("[RCC MISSION] MPH_PAC_PHO_EXT Setting flag for peds in van")
				PED_INDEX pedAgent
				INT i
				FOR i = ENUM_TO_INT(VS_DRIVER) TO ENUM_TO_INT(VS_BACK_RIGHT)
					pedAgent = GET_PED_IN_VEHICLE_SEAT(vehVan, INT_TO_ENUM(VEHICLE_SEAT, i))
					
					IF DOES_ENTITY_EXIST(pedAgent)
					AND NOT IS_PED_INJURED(pedAgent)
					AND NETWORK_HAS_CONTROL_OF_ENTITY(pedAgent)
						SET_PED_RESET_FLAG(pedAgent, PRF_DisableInVehicleActions, TRUE)
						PRINTLN("[RCC MISSION] SETTING PRF_DisableInVehicleActions for ped in seat : ", i)
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
	ELIF ARE_STRINGS_EQUAL(sMocapName, "SUBJ_MCS1")
		
		ENTITY_INDEX entSub = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_1")
		IF DOES_ENTITY_EXIST(entSub)
			
			IF HAS_ANIM_EVENT_FIRED(entSub, GET_HASH_KEY("ALARM_OFF"))
				
				PRINTLN("[RCC MISSION] SUBJ_MCS1 Anim event triggered. Turn off alarm")
				SET_BIT(iLocalBoolCheck28, LBOOL28_SUPPRESS_SUBMARINE_ALARM_FOR_CUTSCENE)				
			ENDIF
			
			IF HAS_ANIM_EVENT_FIRED(entSub, GET_HASH_KEY("ALARM_ON"))
				
				PRINTLN("[RCC MISSION] SUBJ_MCS1 Anim event triggered. Turn on alarm")
				CLEAR_BIT(iLocalBoolCheck28, LBOOL28_SUPPRESS_SUBMARINE_ALARM_FOR_CUTSCENE)
			ENDIF
		ENDIF
		
	ELIF ARE_STRINGS_EQUAL(sMocapName, "MPCAS6_EXT")
			
		PRINTLN("[RCC MISSION] MPCAS6_EXT - Checking events. Time: ", (TO_FLOAT(GET_CUTSCENE_TIME()) / TO_FLOAT(GET_CUTSCENE_END_TIME())))
		
		IF GET_MOCAP_PLAYER_COUNT() <= 1
			ENTITY_INDEX entID = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Champ_Flute_1")
			IF (TO_FLOAT(GET_CUTSCENE_TIME()) / TO_FLOAT(GET_CUTSCENE_END_TIME())) >= 0.365			
				PRINTLN("[RCC MISSION] MPCAS6_EXT - Hiding Extra glass.")
				SET_ENTITY_VISIBLE(entID, FALSE)
			ENDIF
		ENDIF
		
	ELIF ARE_STRINGS_EQUAL(sMocapName, "MPCAS2_MCS1")
		PRINTLN("[RCC MISSION] MPCAS2_MCS1 Checking for Anim events.")
		
		BOOL bHideBike
		ENTITY_INDEX entSub = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Player_Bike_3")
		IF DOES_ENTITY_EXIST(entSub)
		AND NOT IS_ENTITY_DEAD(entSub)
			IF HAS_ANIM_EVENT_FIRED(entSub, GET_HASH_KEY("hide_bike"))
				PRINTLN("[RCC MISSION] MPCAS2_MCS1 - hide_bike for Player_Bike_3.")
				SET_ENTITY_VISIBLE(entSub, FALSE)
				bHideBike = TRUE
			ENDIF
		ENDIF
		entSub = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Player_Bike_4")
		IF DOES_ENTITY_EXIST(entSub)
		AND NOT IS_ENTITY_DEAD(entSub)
			IF HAS_ANIM_EVENT_FIRED(entSub, GET_HASH_KEY("hide_bike"))
				PRINTLN("[RCC MISSION] MPCAS2_MCS1 - hide_bike for Player_Bike_4.")
				SET_ENTITY_VISIBLE(entSub, FALSE)
				bHideBike = TRUE
			ENDIF
		ENDIF

		IF bHideBike						
			IF GET_MOCAP_PLAYER_COUNT() <= 1
				entSub = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Player_Bike_2")
				IF DOES_ENTITY_EXIST(entSub)
				AND NOT IS_ENTITY_DEAD(entSub)
					SET_ENTITY_VISIBLE(entSub, FALSE)
					PRINTLN("[RCC MISSION] MPCAS2_MCS1 - Hiding Player_Bike_2.")
				ENDIF
			ENDIF
		ENDIF
		
	ELIF ARE_STRINGS_EQUAL(sMocapName, "HS3F_SUB_CEL")
		PRINTLN("[RCC MISSION] HS3F_SUB_CEL Checking for Anim events.")
		
		ENTITY_INDEX entSub = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("brucie")
		IF IS_ENTITY_ALIVE(entSub)
			PRINTLN("[RCC MISSION] HS3F_SUB_CEL Entity Handle 'Brucie' Exists.")
			IF HAS_ANIM_EVENT_FIRED(entSub, GET_HASH_KEY("brucie_shot"))
				PRINTLN("[RCC MISSION] HS3F_SUB_CEL - Brucie_Shot for Brucie.")				
				PED_INDEX pedSub = GET_PED_INDEX_FROM_ENTITY_INDEX(entSub)
				IF NOT IS_PED_INJURED(pedSub)
					APPLY_BLOOD_TO_PED(10, pedSub)
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION] HS3F_SUB_CEL Entity Handle 'Brucie' did not give a valid entity.")
		ENDIF
	ENDIF		
	
ENDPROC

FUNC BOOL SHOULD_CUTSCENE_ENTITY_BE_SPAWNED(INT iEntityType, INT iEntityIndex)
	SWITCH iEntityType
		CASE CREATION_TYPE_PEDS
			IF MC_serverBD_2.iCurrentPedRespawnLives[iEntityIndex] > 0
				IF SHOULD_PED_RESPAWN_NOW(iEntityIndex)
					IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iEntityIndex])
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF					
		BREAK		
		CASE CREATION_TYPE_VEHICLES
			IF GET_VEHICLE_RESPAWNS(iEntityIndex) > 0
				IF SHOULD_VEH_RESPAWN_NOW(iEntityIndex)		
					IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iEntityIndex])
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE CREATION_TYPE_OBJECTS
			IF MC_serverBD_2.iCurrentObjRespawnLives[iEntityIndex] > 0
				IF SHOULD_OBJ_RESPAWN_NOW(iEntityIndex)
					IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iEntityIndex])
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

/// PURPOSE: This fuction returns TRUE unless we've got a special dropoff type
///    eg apartment/garage and we want to wait for the apartment cutscenes to finish
///    before starting the mocaps
FUNC BOOL HAVE_MOCAP_START_CONDITIONS_BEEN_MET(INT iCutscene, FMMC_CUTSCENE_TYPE ecutType)
	
	IF IS_BIT_SET(iLocalBoolCheck7, LBOOL7_SHOULD_HOLD_UP_MOCAP_FOR_CUTSCENE)
	AND NOT IS_BIT_SET(iLocalBoolCheck7, LBOOL7_TOGGLED_RENDERPHASES_FOR_DROPOFF)
	AND NOT IS_BIT_SET(iLocalBoolCheck8, LBOOL8_NO_MOCAP_AFTER_THIS_DROPOFF)
		RETURN FALSE
	ENDIF
	
	// Player is using the heli_cam turret system
	IF GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].iTurretSeat != -1
		RETURN FALSE
	ENDIF
	
	//Attempted fix for 2107550
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "mph_pac_fin_msc0") 
	
		INT iPlayerLoop
		PLAYER_INDEX tempPlayer
		
		FOR iPlayerLoop = 0 TO ( NUM_NETWORK_PLAYERS - 1 )		
			tempPlayer = INT_TO_PLAYERINDEX( iPlayerLoop )
			IF NETWORK_IS_PLAYER_ACTIVE(tempPlayer)
				IF NOT IS_NET_PLAYER_OK( tempPlayer, TRUE )
		       		RETURN FALSE
				ENDIF
			ENDIF
		ENDFOR
	
	ENDIF
	
	IF are_strings_equal(sMocapCutscene, "mph_pri_sta_mcs1")
		INT i
		vehicle_index carToDetach

		FOR i = 0 TO ( MC_serverBD.iNumObjCreated - 1 )
			IF IS_THIS_A_HACK_CONTAINER(i)
				carToDetach = get_attached_car_from_container(i)
				IF does_entity_exist(carToDetach)
					IF is_entity_attached(carToDetach)
						RETURN FALSE
					ENDIF 
				ENDIF 
				
			ENDIF 
		ENDFOR 
	ENDIF 
	
	// Have cutscene entities spawned?
	IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO) 
		IF NOT HAS_NET_TIMER_STARTED(tdCutsceneWaitForEntitiesTimer)
			START_NET_TIMER(tdCutsceneWaitForEntitiesTimer)
			PRINTLN("[LM][HAVE_MOCAP_START_CONDITIONS_BEEN_MET] - Starting safety Timer...")
		ENDIF
		
		IF HAS_NET_TIMER_STARTED(tdCutsceneWaitForEntitiesTimer)
		AND NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(tdCutsceneWaitForEntitiesTimer, ciCUTSCENE_WAIT_FOR_ENTITIES_TIME)
			PRINTLN("[LM][HAVE_MOCAP_START_CONDITIONS_BEEN_MET] - Checking Entities.")
			INT i = 0
			FOR i = 0 TO (FMMC_MAX_CUTSCENE_ENTITIES-1)
				SWITCH ecutType
					CASE FMMCCUT_SCRIPTED 
						IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iType != -1
						AND g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iIndex != -1
							IF SHOULD_CUTSCENE_ENTITY_BE_SPAWNED(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iType, g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iIndex)
								PRINTLN("[LM][HAVE_MOCAP_START_CONDITIONS_BEEN_MET] - (Scripted) iType: ", g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iType, " Index: ", g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iIndex, " Return False.")
								RETURN FALSE
							ELSE
								PRINTLN("[LM][HAVE_MOCAP_START_CONDITIONS_BEEN_MET] - (Scripted) iType: ", g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iType, " Index: ", g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iIndex, " Is Spawned/Ready")
							ENDIF
						ENDIF
					BREAK
					CASE FMMCCUT_MOCAP
						IF g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].sCutsceneEntities[i].iType != -1
						AND g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].sCutsceneEntities[i].iIndex != -1
							IF SHOULD_CUTSCENE_ENTITY_BE_SPAWNED(g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].sCutsceneEntities[i].iType, g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].sCutsceneEntities[i].iIndex)
								PRINTLN("[LM][HAVE_MOCAP_START_CONDITIONS_BEEN_MET] - (MidMocap) iType: ", g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].sCutsceneEntities[i].iType, " Index: ", g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].sCutsceneEntities[i].iIndex, " Return False.")
								RETURN FALSE
							ELSE
								PRINTLN("[LM][HAVE_MOCAP_START_CONDITIONS_BEEN_MET] - (MidMocap) iType: ", g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].sCutsceneEntities[i].iType, " Index: ", g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].sCutsceneEntities[i].iIndex, " Is Spawned/Ready")
							ENDIF
						ENDIF
					BREAK
					CASE FMMCCUT_ENDMOCAP
						IF g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iType != -1
						AND g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iIndex != -1
							IF SHOULD_CUTSCENE_ENTITY_BE_SPAWNED(g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iType, g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iIndex)
								PRINTLN("[LM][HAVE_MOCAP_START_CONDITIONS_BEEN_MET] - (EndMocap) iType: ", g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iType, " Index: ", g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iIndex, " Return False.")
								RETURN FALSE
							ELSE
								PRINTLN("[LM][HAVE_MOCAP_START_CONDITIONS_BEEN_MET] - (EndMocap) iType: ", g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iType, " Index: ", g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iIndex, " Is Spawned/Ready")
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			ENDFOR
		ELSE
			PRINTLN("[LM][HAVE_MOCAP_START_CONDITIONS_BEEN_MET] - Timeout, returning true...")
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC DO_PRE_MOCAP_ENDING_CAMERA_CHECKS()
	// B* 2035843 - make sure the cs entities are parked in the same order left to right as gameplay
	IF NOT IS_STRING_NULL_OR_EMPTY( sMocapCutscene )
	AND ARE_STRINGS_EQUAL( sMocapCutscene, "MPH_HUM_ARM_EXT")
		CPRINTLN( DEBUG_SIMON, "DO_PRE_MOCAP_ENDING_CAMERA_CHECKS - Entering" )
		ENTITY_INDEX 	entInsurgentOne, entInsurgentTwo
		INT 			iInsurgentOneIndex, iInsurgentTwoIndex, iCutsceneEntityType, iCutsceneEntityIndex
		TEXT_LABEL_31 	sCutsceneHandle 
		NETWORK_INDEX 	netid
		INT 			i
		
		// iterate through cutscene entites and find the two insurgents
		FOR i = 0 TO FMMC_MAX_CUTSCENE_ENTITIES - 1
			iCutsceneEntityType 	= g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[ i ].iType
			iCutsceneEntityIndex 	= g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[ i ].iIndex
			sCutsceneHandle 		= g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[ i ].tlCutsceneHandle
			IF( iCutsceneEntityType = CREATION_TYPE_VEHICLES )
				IF( ARE_STRINGS_EQUAL( sCutsceneHandle, "insurgent1" ) ) // 766.98, 4182.60, 40.54
					netid = GET_CUTSCNE_VISIBLE_NET_ID( iCutsceneEntityType, iCutsceneEntityIndex )
					IF( netid <> NULL AND NETWORK_DOES_NETWORK_ID_EXIST( netid ) )
						entInsurgentOne = NET_TO_ENT( netid )
						iInsurgentOneIndex = i
						CPRINTLN( DEBUG_SIMON, "DO_PRE_MOCAP_ENDING_CAMERA_CHECKS - Got insurgent1" )
					ENDIF
				ENDIF
				IF( ARE_STRINGS_EQUAL( sCutsceneHandle, "insurgent2" ) ) // 761.42, 4183.75, 40.41
					netid = GET_CUTSCNE_VISIBLE_NET_ID( iCutsceneEntityType, iCutsceneEntityIndex )
					IF( netid <> NULL AND NETWORK_DOES_NETWORK_ID_EXIST( netid ) )
						entInsurgentTwo = NET_TO_ENT( netid )
						iInsurgentTwoIndex = i
						CPRINTLN( DEBUG_SIMON, "DO_PRE_MOCAP_ENDING_CAMERA_CHECKS - Got insurgent2" )
					ENDIF
				ENDIF
			ENDIF
			
			IF( DOES_ENTITY_EXIST( entInsurgentOne ) AND DOES_ENTITY_EXIST( entInsurgentTwo ) )
				i = FMMC_MAX_CUTSCENE_ENTITIES 
			ENDIF
		ENDFOR
		
		IF( NOT DOES_ENTITY_EXIST( entInsurgentOne ) OR NOT DOES_ENTITY_EXIST( entInsurgentTwo ) )
			ASSERTLN( "DO_PRE_MOCAP_ENDING_CAMERA_CHECKS - Failed to find one or more cutscene entities! Ask Simon Bramwell" )
		ELSE
			FLOAT fInsurgent1Dist, fInsurgent2Dist
			fInsurgent1Dist = VDIST( GET_ENTITY_COORDS( entInsurgentOne, FALSE ), <<766.98, 4182.60, 40.54>> ) // left
			fInsurgent2Dist = VDIST( GET_ENTITY_COORDS( entInsurgentTwo, FALSE ), <<766.98, 4182.60, 40.54>> ) // left
			// figure out which insurgent is closer to the left parking space, if necessary swap the handles
			IF( fInsurgent2Dist < fInsurgent1Dist )
				CPRINTLN( DEBUG_SIMON, "DO_PRE_MOCAP_ENDING_CAMERA_CHECKS - Insurgent2 should be on the left" )
				g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[ iInsurgentTwoIndex ].tlCutsceneHandle = "insurgent1"
				g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[ iInsurgentOneIndex ].tlCutsceneHandle = "insurgent2"
			ELSE	
				CPRINTLN( DEBUG_SIMON, "DO_PRE_MOCAP_ENDING_CAMERA_CHECKS - Insurgent1 should be on the left" )
			ENDIF
		ENDIF
	ENDIF
ENDPROC

///PURPOSE: requests assets for a specific cutscene. 
proc request_assets_for_specific_cutscene()

	if not is_string_null_or_empty(sMocapCutscene)
			
		if are_strings_equal(sMocapCutscene, "mph_pac_fin_mcs0") 
			
			request_model(HEI_PROP_ZIP_TIE_POSITIONED)
				
		endif 
			
	endif 

endproc 

proc delete_assets_for_specific_cutscene()

	if not is_string_null_or_empty(sMocapCutscene)
		
		if are_strings_equal(sMocapCutscene, "mph_pac_fin_mcs1") 
		
			if does_entity_exist(zip_tie_obj)
				
				delete_object(zip_tie_obj)
			
				set_model_as_no_longer_needed(HEI_PROP_ZIP_TIE_POSITIONED)
				
			endif 
			
		endif 
		
	endif 

endproc 

proc create_zip_tie_for_cutscene()
			
	if not does_entity_exist(zip_tie_obj)
		
		if not is_string_null_or_empty(sMocapCutscene)
			
			if are_strings_equal(sMocapCutscene, "mph_pac_fin_mcs0") 
		
				if has_model_loaded(HEI_PROP_ZIP_TIE_POSITIONED)
			
					zip_tie_obj = create_object_no_offset(HEI_PROP_ZIP_TIE_POSITIONED, <<232.095, 215.355, 106.416>>, false)
					set_entity_rotation(zip_tie_obj, <<0.0, 180.0, 115.6>>)
					freeze_entity_position(zip_tie_obj, true)
					
				endif 
			
			endif 
				
		endif 
		
	endif 

endproc

// url:bugstar:2239709
PROC PAC_FIN_SET_LOW_BUDGETS( BOOL bLow )
	IF bLow
		PRINTLN("PAC_FIN_SET_LOW_BUDGETS - True")
		SET_REDUCE_VEHICLE_MODEL_BUDGET(TRUE)
		SET_REDUCE_PED_MODEL_BUDGET(TRUE)
		SET_VEHICLE_POPULATION_BUDGET(1)
		SET_PED_POPULATION_BUDGET(1)
		SET_BIT(iLocalBoolCheck11, LBOOL11_PAC_FIN_LOW_BUDGETS)
	ELSE
		PRINTLN("PAC_FIN_SET_LOW_BUDGETS - False")
		SET_REDUCE_VEHICLE_MODEL_BUDGET(FALSE)
		SET_REDUCE_PED_MODEL_BUDGET(FALSE)
		SET_VEHICLE_POPULATION_BUDGET(3)
		SET_PED_POPULATION_BUDGET(3)
		CLEAR_BIT(iLocalBoolCheck11, LBOOL11_PAC_FIN_LOW_BUDGETS)
	ENDIF
ENDPROC

//Override water values to make smoother
PROC DO_WATER_OVERRIDE()
	WATER_OVERRIDE_SET_SHOREWAVEAMPLITUDE(0) 
		WATER_OVERRIDE_SET_SHOREWAVEMINAMPLITUDE(0) 
	WATER_OVERRIDE_SET_SHOREWAVEMAXAMPLITUDE(0) 
	WATER_OVERRIDE_SET_OCEANNOISEMINAMPLITUDE(0) 
	WATER_OVERRIDE_SET_OCEANWAVEAMPLITUDE(0) 
	WATER_OVERRIDE_SET_OCEANWAVEMINAMPLITUDE(0) 
	WATER_OVERRIDE_SET_OCEANWAVEMAXAMPLITUDE(0) 
	WATER_OVERRIDE_SET_RIPPLEBUMPINESS(0) 
	WATER_OVERRIDE_SET_RIPPLEMINBUMPINESS(0) 
	WATER_OVERRIDE_SET_RIPPLEMAXBUMPINESS(0) 
	WATER_OVERRIDE_SET_RIPPLEDISTURB(0) 
	WATER_OVERRIDE_SET_STRENGTH(0.5)
ENDPROC

//Check if the local player is in the cutscene
FUNC BOOL IS_LOCAL_PLAYER_PED_IN_CUTSCENE()

	IF IS_STRING_NULL_OR_EMPTY(tlPlayerSceneHandle)
		PRINTLN("[RCC MISSION] IS_LOCAL_PLAYER_PED_IN_CUTSCENE() FALSE")
		RETURN FALSE
	ELSE
		PRINTLN("[RCC MISSION] IS_LOCAL_PLAYER_PED_IN_CUTSCENE() TRUE")
		RETURN TRUE
	ENDIF
ENDFUNC

//Do checks immediately on running mocap
PROC DO_RUN_MOCAP_CHECKS()

	PRINTLN("[RCC MISSION] DOING MOCAP RUN CHECKS FOR ", sMocapCutscene)

	IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_JUST_DONE_DROPOFF_POSTFX)
		ANIMPOSTFX_STOP("HeistLocate")
		PRINTLN("[RCC MISSION] STOPPING POST FX CALLED WITH LBOOL10_JUST_DONE_DROPOFF_POSTFX")
	ENDIF
	
	//Pacific Standard Job Witsec
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PAC_WIT_MCS1")
		//Hide bushes in the hut
		CREATE_MODEL_HIDE(<<-2166.4595, 5197.9614, 15.8854>>, 10.0, INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("PROP_BUSH_LRG_01")), FALSE)
		CREATE_MODEL_HIDE(<<-2166.4595, 5197.9614, 15.8854>>, 10.0, INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("PROP_BUSH_LRG_02b")), FALSE)
		CREATE_MODEL_HIDE(<<-2166.4595, 5197.9614, 15.8854>>, 10.0, INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("PROP_BUSH_IVY_01_1L")), FALSE)
		CREATE_MODEL_HIDE(<<-2166.4595, 5197.9614, 15.8854>>, 10.0, INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("PROP_BUSH_IVY_01_1M")), FALSE)
	ENDIF
	
	//Get on boat cutscene
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PAC_WIT_MCS1")
		DO_WATER_OVERRIDE()
		DISPLAY_HUD( FALSE )
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_LOCAL_PLAYER_IN_CUTSCENE)
		SET_ENTITY_VISIBLE(LocalPlayerPed, TRUE)
	ENDIF
	
	//url:bugstar:2232618 - [Pacific Standard Job - Finale] mph_pac_fin_ext - Spray visual effects from player boat are offset from cutscene boat entity.
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PAC_FIN_EXT") 
		REMOVE_PARTICLE_FX_IN_RANGE(<<-1988.8, 4718.2, 3.7>>, 100)
	ENDIF
	
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PAC_FIN_MCS0")
		PAC_FIN_SET_LOW_BUDGETS(TRUE)
	ENDIF
	
	// url:bugstar:2239705
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PAC_FIN_MCS1")
		REQUEST_ACTION_MODE_ASSET("MICHAEL_ACTION")
	ENDIF
	
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_NAR_COK_EXT")
		REMOVE_PARTICLE_FX_IN_RANGE(g_FMMC_STRUCT.sEndMocapSceneData.vClearCoords, g_FMMC_STRUCT.sEndMocapSceneData.fClearRadius)
		PRINTLN("[RCC MISSION] DO_RUN_MOCAP_CHECKS() - REMOVE_PARTICLE_FX_IN_RANGE(", g_FMMC_STRUCT.sEndMocapSceneData.vClearCoords, ", ", g_FMMC_STRUCT.sEndMocapSceneData.fClearRadius,")")
		CPRINTLN(DEBUG_MISSION, "Clearing area of ptfx for MPH_NAR_COK_EXT, ", g_FMMC_STRUCT.sEndMocapSceneData.vClearCoords, ", ", g_FMMC_STRUCT.sEndMocapSceneData.fClearRadius, ")")
	ENDIF
	
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PRI_FIN_EXT")
		ENTITY_INDEX entHeli = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY( "MPH_Helicopter" )
		
		IF DOES_ENTITY_EXIST(entHeli)
			IF NOT IS_ENTITY_DEAD(entHeli)
				VEHICLE_INDEX vehHeli = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX( entheli )
			
				SET_VEHICLE_ENGINE_ON(vehHeli, FALSE, TRUE)
				SET_HELI_BLADES_SPEED(vehHeli, 0.0)
				PRINTLN("[RCC MISSION] MPH_PRI_FIN_EXT - SETTING CUTSCENE HELI BLADES TO STOP")
			ELSE
				PRINTLN("[RCC MISSION] MPH_PRI_FIN_EXT - ENTITY IS DEAD")
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION] MPH_PRI_FIN_EXT - HELI DOESN'T EXIST") 
		ENDIF
		
	ENDIF
	
	//Helicopter landed cutscene on shore
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_HUM_FIN_EXT")
		ENTITY_INDEX entHeli = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY( "VALKYRIE" )
		
		IF DOES_ENTITY_EXIST(entHeli)
			VEHICLE_INDEX vehHeli = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX( entheli)
		
			SET_VEHICLE_ENGINE_ON(vehHeli, FALSE, TRUE)
			SET_HELI_BLADES_SPEED(vehHeli, 0.0)
			
			// url:bugstar:2153657
			INT iLoop
			FOR iLoop = 12 TO 14
				SET_VEHICLE_EXTRA(vehHeli, iLoop, TRUE)
			ENDFOR
			
			PRINTLN("[RCC MISSION] MPH_HUM_FIN_EXT - SETTING CUTSCENE HELI BLADES TO STOP") 
		ELSE
			PRINTLN("[RCC MISSION] MPH_HUM_FIN_EXT - HELI DOESN'T EXIST") 
		ENDIF
	ENDIF
	
	//Ensure there isn't rain on the servers during the cutscene b*2149909
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PAC_HAC_MCS1")
	
		ENTITY_INDEX server1 = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Server1")
		ENTITY_INDEX server2 = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Server2")
		
		IF DOES_ENTITY_EXIST(server1)
			SET_ENTITY_IS_IN_VEHICLE(server1)
		ENDIF
		
		IF DOES_ENTITY_EXIST(server2)
			SET_ENTITY_IS_IN_VEHICLE(server2)
		ENDIF
	ENDIF
	
	//Narcotics Finale
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_NAR_FIN_EXT")
		DO_WATER_OVERRIDE()
		
		SET_SRL_READAHEAD_TIMES(6,6,6,6) //b*2110924
		
		PROCGRASS_ENABLE_CULLSPHERE(1, <<3202, 5091, 2>>, 200.0) //b*2139318
		
		//Ensure Trevor doesn't have a weapon
		ENTITY_INDEX entTrevor = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY( "TREVOR" ) 
		IF( DOES_ENTITY_EXIST( entTrevor ) )
		AND NETWORK_HAS_CONTROL_OF_ENTITY(entTrevor)
			IF( IS_ENTITY_A_PED( entTrevor ) )
				PED_INDEX pedTrevor = GET_PED_INDEX_FROM_ENTITY_INDEX( entTrevor )
				IF( NOT IS_PED_DEAD_OR_DYING( pedTrevor ) )
					SET_CURRENT_PED_WEAPON(pedTrevor, WEAPONTYPE_UNARMED, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// Humane EMP exit
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_HUM_EMP_EXT")
		// Turn off hydra radio
		ENTITY_INDEX entHydra = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("hydra")
		
		IF DOES_ENTITY_EXIST(entHydra)
		AND NETWORK_HAS_CONTROL_OF_ENTITY(entHydra)
			VEHICLE_INDEX vehHydra = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entHydra)
			
			IF DOES_ENTITY_EXIST(vehHydra)
				PRINTLN("[RCC MISSION] MPH_HUM_EMP_EXT Turning off Hydra radio")
				SET_VEHICLE_RADIO_ENABLED(vehHydra, FALSE)
				
				PRINTLN("[RCC MISSION] MPH+HUM_EMP_EXT Disabling Hyrda Weapons")
				DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_VEHICLE_ROCKET, vehHydra, LocalPlayerPed)
				DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_VEHICLE_PLAYER_LASER, vehHydra, LocalPlayerPed)
				DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_VEHICLE_PLAYER_BULLET, vehHydra, LocalPlayerPed)
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION] MPH_HUM_EMP_EXT Hydra DOESNT EXIST")
		ENDIF
	ENDIF
	
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PRI_BUS_EXT")
		DISTANT_COP_CAR_SIRENS(FALSE)
	ENDIF
	
	// url:bugstar:2208851 Access livery
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_HUM_VAL_EXT")
		ENTITY_INDEX entValkyrie = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Valkyrie")
		
		IF DOES_ENTITY_EXIST(entValkyrie)
			VEHICLE_INDEX vehValkyrie = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entValkyrie)
			
			IF DOES_ENTITY_EXIST(vehValkyrie)
			
				SET_VEHICLE_COLOURS(vehValkyrie, 14, 14)
				PRINTLN("MPH_HUM_VAL_EXT - Setting cutscene Valkyrie colour to: primary: ", 14, "; secondary: ", 14)
			
				INT iExtra
				FOR iExtra = 12 TO 14
					IF iExtra = iValkyrieDesign
						SET_VEHICLE_EXTRA(vehValkyrie, iExtra, FALSE)
					ELSE
						SET_VEHICLE_EXTRA(vehValkyrie, iExtra, TRUE)
					ENDIF
				ENDFOR
				PRINTLN("MPH_HUM_VAL_EXT - Setting cutscene Valkyrie extra to ", iValkyrieDesign)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

VEHICLE_INDEX vi_PrisonFinaleBuzzard
PED_INDEX pi_PrisonFinaleBuzzardPilot

//Do checks immediately after running mocap
PROC DO_POST_RUN_MOCAP_CHECKS()

	CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Doing post-mocap checks for ", sMocapCutscene )
	
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PAC_FIN_MCS1")
	AND IS_BIT_SET(iLocalBoolCheck11, LBOOL11_PAC_FIN_LOW_BUDGETS)
		PAC_FIN_SET_LOW_BUDGETS(FALSE)
	ENDIF

	//Narcotics Finale
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_NAR_FIN_EXT")
		//Reset water
		WATER_OVERRIDE_SET_STRENGTH(0)
		PROCGRASS_DISABLE_CULLSPHERE(1) //b*2139318
	ENDIF
	
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PAC_WIT_MCS1")
		//Set water back to normal
		WATER_OVERRIDE_SET_STRENGTH(0)
		DISPLAY_HUD( TRUE )
	ENDIF
	
	//Pacific Finale
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PAC_FIN_EXT")
		PRINTLN("[RCC MISSION] RESETTING SNOW MPH_PAC_FIN_EXT")
		SET_WEATHER_PTFX_USE_OVERRIDE_SETTINGS(false)
		CLEAR_BIT(iLocalBoolCheck9, LBOOL9_CUTSCENE_SNOW_OFF)
	ENDIF

	//Unpin any existing interior
	IF IS_VALID_INTERIOR(iCutInterior)
		UNPIN_INTERIOR(iCutInterior)
	ENDIF
	
	//Allow distant sirens again
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PRI_BUS_EXT")
		DISTANT_COP_CAR_SIRENS(TRUE)
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_CUTSCENE_REMOVE_PLAYER_FROM_VEHICLE_AT_START(INT iCutsceneIndex, BOOL bIsScriptedCut)
	
	IF iCutsceneIndex < 0 
	OR (bIsScriptedCut AND iCutsceneIndex >= FMMC_GET_MAX_SCRIPTED_CUTSCENES())
	OR (!bIsScriptedCut AND iCutsceneIndex >= MAX_MOCAP_CUTSCENES)
		CPRINTLN(DEBUG_MISSION, "SHOULD_CUTSCENE_REMOVE_PLAYER_FROM_VEHICLE_AT_START - iCutsceneIndex: ", iCutsceneIndex, " is out of range")
		RETURN FALSE
	ENDIF
	
	BOOL bResult
	IF bIsScriptedCut
		bResult = IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneIndex].iCutsceneBitset, ci_CSBS_RemoveFromVehStart)
	ELSE
		bResult = IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneIndex].iCutsceneBitset, ci_CSBS_RemoveFromVehStart)
	ENDIF

	CPRINTLN(DEBUG_MISSION, "SHOULD_CUTSCENE_REMOVE_PLAYER_FROM_VEHICLE_AT_START - iCutsceneIndex: ", iCutsceneIndex, " bResult: ", BOOL_TO_STRING(bResult))
	RETURN bResult
ENDFUNC

PROC REMOVE_PLAYER_FROM_VEHICLE_AT_CUTSCENE_START(INT iCutscene, BOOL bIsScriptedCut)
	
	IF SHOULD_CUTSCENE_REMOVE_PLAYER_FROM_VEHICLE_AT_START(iCutscene, bIsScriptedCut)
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)		
			VEHICLE_INDEX vehIndex = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
			
			IF IS_PED_DRIVING_ANY_VEHICLE(LocalPlayerPed)
			AND (GET_ENTITY_MODEL(vehIndex) = DELUXO OR GET_ENTITY_MODEL(vehIndex) = OPPRESSOR2)
				SET_SPECIAL_FLIGHT_MODE_TARGET_RATIO(vehIndex, 0)				
				PRINTLN("[REMOVE_PLAYER_FROM_VEHICLE_AT_CUTSCENE_START] - Calling SET_SPECIAL_FLIGHT_MODE_TARGET_RATIO on Deluxo Vehicle")
			ENDIF
			
			IF NETWORK_HAS_CONTROL_OF_ENTITY(vehIndex)
				IF IS_ENTITY_IN_AIR(vehIndex)
					PRINTLN("[REMOVE_PLAYER_FROM_VEHICLE_AT_CUTSCENE_START] - Entity is in the air, setting vehicle on ground.")
					SET_VEHICLE_ON_GROUND_PROPERLY(vehIndex, 1.0)
				ENDIF
			ENDIF			
			
			//Switching to this as the car was carrying on after being taken out of vehicle
			TASK_LEAVE_ANY_VEHICLE(LocalPlayerPed, 0, ECF_DONT_CLOSE_DOOR)
		ENDIF
	ENDIF
	
ENDPROC

PROC SET_PLAYER_ROLLING_START(PED_MOTION_STATE eOnFootSpeed = MS_ON_FOOT_WALK, FLOAT fVehSpeed = 6.0)
	
	IF NOT bLocalPlayerOK
		EXIT
	ENDIF
	
	// VEHICLE
	IF IS_PED_IN_ANY_VEHICLE(localPlayerPed, TRUE)
		
		VEHICLE_INDEX vehIndex = GET_VEHICLE_PED_IS_IN(localPlayerPed, TRUE)
		
		IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(localPlayerPed,  TRUE))
			PRINTLN("[RCC MISSION] SET_PLAYER_ROLLING_START - IN VEHICLE - No net control of entity")
			EXIT
		ENDIF
		
		SET_VEHICLE_ENGINE_ON(vehIndex, TRUE, TRUE, FALSE)
		
		MODEL_NAMES eModel = GET_ENTITY_MODEL(vehIndex)
		IF IS_THIS_MODEL_A_HELI(eModel)
		OR IS_THIS_MODEL_A_PLANE(eModel)
			PRINTLN("[RCC MISSION] SET_PLAYER_ROLLING_START - IN VEHICLE - Is heli, set blades full")
			SET_HELI_BLADES_FULL_SPEED(vehIndex)
		ENDIF
		SET_VEHICLE_FORWARD_SPEED(vehIndex, fVehSpeed)
		PRINTLN("[RCC MISSION] SET_PLAYER_ROLLING_START - IN VEHICLE - Complete with fVehSpeed = ", fVehSpeed)
	
	// ON FOOT
	ELSE
		INT iWalkTime = 2500
		IF IS_THIS_MOCAP_IN_AN_APARTMENT(sMocapCutscene)
			iWalkTime = 1000
		ENDIF
		
		FLOAT fMoveBlendRatio
		SWITCH eOnFootSpeed
			CASE MS_ON_FOOT_WALK	fMoveBlendRatio = PEDMOVEBLENDRATIO_WALK		BREAK
			CASE MS_ON_FOOT_RUN		fMoveBlendRatio = PEDMOVEBLENDRATIO_RUN			BREAK
			CASE MS_ON_FOOT_SPRINT	fMoveBlendRatio = PEDMOVEBLENDRATIO_SPRINT		BREAK
		ENDSWITCH
		
		FORCE_PED_MOTION_STATE(LocalPlayerPed, eOnFootSpeed) 
		SIMULATE_PLAYER_INPUT_GAIT(LocalPlayer, fMoveBlendRatio, iWalkTime, GET_ENTITY_HEADING(LocalPlayerPed), FALSE)
		PRINTLN("[RCC MISSION] SET_PLAYER_ROLLING_START - ON FOOT - Complete with fMoveBlendRatio = ", fMoveBlendRatio)
	ENDIF
	
ENDPROC

FUNC BOOL SET_PLAYER_INTO_COVER_AFTER_CUTSCENE(INT iCutsceneToUse)
	FLOAT fHead = -90.0	
	
	IF NOT IS_BIT_SET(iCutsceneBSEnteredCover, iCutsceneToUse)
	AND NOT IS_PED_IN_COVER(localPlayerPed)
	AND NOT IS_PED_GOING_INTO_COVER(localPlayerPed)
		PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - SET_PLAYER_INTO_COVER_AFTER_CUTSCENE - Setting player to enter cover NOW!")

		COVERPOINT_INDEX cpIndex
		cpIndex = GET_CLOSEST_COVER_POINT_TO_LOCATION(GET_ENTITY_COORDS(LocalPlayerPed))	
		SET_PED_TO_LOAD_COVER(LocalPlayerPed, TRUE)
		TASK_PUT_PED_DIRECTLY_INTO_COVER(LocalPlayerPed, GET_ENTITY_COORDS(LocalPlayerPed), -1, TRUE, DEFAULT, TRUE, FALSE, cpIndex)
		//FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed, TRUE)
		SET_ENTITY_HEADING(localPlayerPed, GET_ENTITY_HEADING(localPlayerPed)+fHead)		
		FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed, TRUE)
		
		SET_BIT(iCutsceneBSEnteredCover, iCutsceneToUse)
		RESET_NET_TIMER(tdEnterCoverEndingCutsceneTimeOut)
		START_NET_TIMER(tdEnterCoverEndingCutsceneTimeOut)
	ENDIF
	IF NOT HAS_NET_TIMER_EXPIRED(tdEnterCoverEndingCutsceneTimeOut, 5000)
	AND NOT IS_BIT_SET(iCutsceneBSFinishedCover, iCutsceneToUse)
		IF IS_PED_GOING_INTO_COVER(LocalPlayerPed)
			PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - SET_PLAYER_INTO_COVER_AFTER_CUTSCENE - Entering Cover.")
			RETURN FALSE
		ENDIF
		IF NOT IS_PED_IN_COVER(LocalPlayerPed, TRUE)
			PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - SET_PLAYER_INTO_COVER_AFTER_CUTSCENE - Not in Cover....")
			RETURN FALSE
		ENDIF
		
		fHead = 90.0
		IF IS_PED_IN_COVER_FACING_LEFT(localPlayerPed)
			fHead = -90.0
		ENDIF
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(fHead)
		
		PRINTLN("[RCC MISSION] - PROCESS_MOCAP_CUTSCENE - SET_PLAYER_INTO_COVER_AFTER_CUTSCENE - Calling SET_GAMEPLAY_CAM_RELATIVE_HEADING: ", fHead)
		
		SET_BIT(iCutsceneBSFinishedCover, iCutsceneToUse)
	ELSE
		PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - SET_PLAYER_INTO_COVER_AFTER_CUTSCENE - Timed out let's just progress from this...")
	ENDIF
	RETURN TRUE
ENDFUNC

PROC SET_HELI_HOVERING(VEHICLE_INDEX vehHeli, BOOL bRetractLandingGear = TRUE)

	IF NOT IS_VEHICLE_DRIVEABLE(vehHeli)
		PRINTLN("[RCC MISSION] SET_HELI_HOVERING - vehicle is not drivable")
		EXIT
	ENDIF

	MODEL_NAMES eModel = GET_ENTITY_MODEL(vehHeli)
	IF NOT IS_THIS_MODEL_A_HELI(eModel)
	AND eModel != HYDRA
	AND eModel != TULA
	AND eModel != AVENGER
		PRINTLN("[RCC MISSION] SET_HELI_HOVERING - model: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(vehHeli))," is not a heli")
		EXIT
	ENDIF

	IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(vehHeli)
		PRINTLN("[RCC MISSION] SET_HELI_HOVERING - do not have net control of entity")
		EXIT
	ENDIF
	
	IF (eModel = HYDRA
	OR eModel = TULA
	OR eModel = AVENGER)
	AND GET_VEHICLE_FLIGHT_NOZZLE_POSITION(vehHeli) != 1.0
		SET_VEHICLE_FLIGHT_NOZZLE_POSITION_IMMEDIATE(vehHeli, 1.0)
		PRINTLN("[RCC MISSION] SET_HELI_HOVERING - Setting flight nozzle position to 1.0")
	ENDIF
	
	//vEndPos.z += 10.0
	SET_VEHICLE_ENGINE_ON(vehHeli, TRUE, TRUE, FALSE)
	
	IF bRetractLandingGear
	AND GET_VEHICLE_HAS_LANDING_GEAR(vehHeli)
		CONTROL_LANDING_GEAR(vehHeli, LGC_RETRACT_INSTANT)
		PRINTLN("[RCC MISSION] SET_HELI_HOVERING - Retracting landing gear")
	ENDIF

	SET_VEHICLE_FORWARD_SPEED(vehHeli, 1.0)
	SET_HELI_BLADES_FULL_SPEED(vehHeli)
	
	PRINTLN("[RCC MISSION] SET_HELI_HOVERING - Completed")
ENDPROC

FUNC BOOL HAS_POST_CUTSCENE_IDLE_ANIMATION(FMMC_CUTSCENE_TYPE eCutType, INT iCutsceneToUse)
	
	SWITCH eCutType
		
		CASE FMMCCUT_MOCAP
			SWITCH iScriptedCutsceneTeam
				CASE 0 RETURN IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Team0_IdleAnims)
				CASE 1 RETURN IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Team1_IdleAnims)
				CASE 2 RETURN IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Team2_IdleAnims)
				CASE 3 RETURN IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Team3_IdleAnims)
			ENDSWITCH
		BREAK
		
		CASE FMMCCUT_SCRIPTED
			SWITCH iScriptedCutsceneTeam
				CASE 0 RETURN IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Team0_IdleAnims) 
				CASE 1 RETURN IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Team1_IdleAnims) 
				CASE 2 RETURN IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Team2_IdleAnims) 
				CASE 3 RETURN IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Team3_IdleAnims) 
			ENDSWITCH	
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

PROC DO_POST_CUTSCENE_COMMON_PLAYER_EXIT(INT iCutsceneToUse, FMMC_CUTSCENE_TYPE eCutType)
	
	IF NOT bLocalPlayerOK
		EXIT
	ENDIF
	
	PRINTLN("[RCC MISSION] DO_POST_CUTSCENE_COMMON_PLAYER_EXIT - Started")
	
	FLOAT fVehicleRollingStartSpeed = 6.0
	
	INT iCutsceneBitset, iCutsceneBitset2
	SWITCH eCutType
		CASE FMMCCUT_MOCAP
			iCutsceneBitset = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitset
			iCutsceneBitset2 = g_fmmc_struct.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet2
			fVehicleRollingStartSpeed = g_fmmc_struct.sMocapCutsceneData[iCutsceneToUse].fCutsceneRollingStartSpeed
		BREAK
		CASE FMMCCUT_SCRIPTED 
			iCutsceneBitset = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset
			iCutsceneBitset2 = g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2
			fVehicleRollingStartSpeed = g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].fCutsceneRollingStartSpeed
		BREAK
	ENDSWITCH
	
	IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
		
		VEHICLE_INDEX vehIndex = GET_VEHICLE_PED_IS_IN(localPlayerPed)
		IF IS_BIT_SET(iCutsceneBitset2, ci_CSBS2_Hover_Vehicle_On_End)	
			PRINTLN("[RCC MISSION] DO_POST_CUTSCENE_COMMON_PLAYER_EXIT - ci_CSBS2_Hover_Vehicle_On_End")
			SET_HELI_HOVERING(vehIndex)
		ENDIF
	
	ENDIF
	
	IF IS_BIT_SET(iCutsceneBitset, ci_CSBS_Rolling_Start)
		PRINTLN("[RCC MISSION] DO_POST_CUTSCENE_COMMON_PLAYER_EXIT - ci_CSBS_Rolling_Start")
		SET_PLAYER_ROLLING_START(DEFAULT, fVehicleRollingStartSpeed)
	ENDIF
	
	IF HAS_POST_CUTSCENE_IDLE_ANIMATION(eCutType, iCutsceneToUse) 
	
		PRINTLN("[RCC MISSION] DO_POST_CUTSCENE_COMMON_PLAYER_EXIT - giving random re-entry idle anim.")
		START_RANDOM_RE_ENTRY_IDLE_ANIM()	
		
	ENDIF
	
	IF IS_BIT_SET(iCutsceneBitset2, ci_CSBS2_EquipWeaponAtEnd)
		
		PRINTLN("[RCC MISSION] DO_POST_CUTSCENE_COMMON_PLAYER_EXIT - cutscene should equip weapon at end")
		IF wtPreCutsceneWeapon = WEAPONTYPE_UNARMED
		OR wtPreCutsceneWeapon = WEAPONTYPE_INVALID
			PUT_WEAPON_IN_HAND( WEAPONINHAND_LASTWEAPON_BOTH )
			PRINTLN("DO_POST_CUTSCENE_COMMON_PLAYER_EXIT - wtPreCutsceneWeapon is an invalid weapon. Putting last known weapon into hand.")
		ELSE
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtPreCutsceneWeapon, TRUE)
			PRINTLN("DO_POST_CUTSCENE_COMMON_PLAYER_EXIT - Putting weapon back in player's hand: ", GET_WEAPON_NAME(wtPreCutsceneWeapon))
		ENDIF
		
		WEAPON_TYPE wtWeapon
		GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtWeapon)
		IF wtWeapon = WEAPONTYPE_UNARMED
		OR wtWeapon = WEAPONTYPE_INVALID
			PUT_WEAPON_IN_HAND( WEAPONINHAND_BEST_WEAPON )
			PRINTLN("[RCC MISSION] DO_POST_CUTSCENE_COMMON_PLAYER_EXIT - last weapon was unequipped. Put best weapon in hand")
		ENDIF		
	ENDIF	
ENDPROC

PROC HANDLE_PLAYER_CUTSCENE_WARP_BETWEEN_AVENGER_HOLDS(INT iCutsceneTeam, INT iWarpVeh, INT iSeat)
	
	IF IS_PARTICIPANT_A_NON_HEIST_SPECTATOR(iLocalPart)
		PRINTLN("[RCC MISSION] HANDLE_PLAYER_CUTSCENE_WARP_BETWEEN_AVENGER_HOLDS - Player is a non-heist spectator")
		EXIT
	ENDIF
	
	INT iLocalCutsceneIdx = GET_LOCAL_PLAYER_CUTSCENE_INDEX(iCutsceneTeam)
	
	IF iLocalCutsceneIdx = -1
		PRINTLN("[RCC MISSION] HANDLE_PLAYER_CUTSCENE_WARP_BETWEEN_AVENGER_HOLDS - Player is not part of the cutscene, iLocalCutsceneIdx = ",iLocalCutsceneIdx)
		EXIT
	ENDIF
	
	IF iWarpVeh = -1
		PRINTLN("[RCC MISSION] HANDLE_PLAYER_CUTSCENE_WARP_BETWEEN_AVENGER_HOLDS - No warp vehicle set up for this cutscene")
		EXIT
	ENDIF
	
	IF iSeat != ciFMMC_Cutscene_SeatPreference_SameAsCurrentVeh
		PRINTLN("[RCC MISSION] HANDLE_PLAYER_CUTSCENE_WARP_BETWEEN_AVENGER_HOLDS - Cutscene player index ",iLocalCutsceneIdx," doesn't get the same seat, iSeat = ", iSeat)
		EXIT
	ENDIF
	
	IF NOT GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		PRINTLN("[RCC MISSION] HANDLE_PLAYER_CUTSCENE_WARP_BETWEEN_AVENGER_HOLDS - Not a gangops mission")
		EXIT
	ENDIF
	
	IF GANGOPS_FLOW_GET_CURRENT_GANGOPS_MISSION() != ciGANGOPS_FLOW_MISSION_STEALOSPREY
		PRINTLN("[RCC MISSION] HANDLE_PLAYER_CUTSCENE_WARP_BETWEEN_AVENGER_HOLDS - Not the Avenger mission, current mission = ",GANGOPS_FLOW_GET_CURRENT_GANGOPS_MISSION())
		EXIT
	ENDIF
	
	IF NOT IS_PLAYER_IN_CREATOR_AIRCRAFT(LocalPlayer)
		PRINTLN("[RCC MISSION] HANDLE_PLAYER_CUTSCENE_WARP_BETWEEN_AVENGER_HOLDS - Not in the hold, IS_PLAYER_IN_CREATOR_AIRCRAFT returned FALSE")
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iWarpVeh].mn != AVENGER
		PRINTLN("[RCC MISSION] HANDLE_PLAYER_CUTSCENE_WARP_BETWEEN_AVENGER_HOLDS - Warp vehicle ",iWarpVeh," is not an avenger, mn = ",GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iWarpVeh].mn),"/",ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iWarpVeh].mn))
		EXIT
	ENDIF
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iWarpVeh])
		PRINTLN("[RCC MISSION] HANDLE_PLAYER_CUTSCENE_WARP_BETWEEN_AVENGER_HOLDS - Warp vehicle ",iWarpVeh," net ID doesn't exist")
		EXIT
	ENDIF
	
	VEHICLE_INDEX tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iWarpVeh])
	
	IF NOT DOES_ENTITY_EXIST(tempVeh)
		PRINTLN("[RCC MISSION] HANDLE_PLAYER_CUTSCENE_WARP_BETWEEN_AVENGER_HOLDS - Warp vehicle ",iWarpVeh," vehicle index doesn't exist")
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF MPGlobalsAmbience.vehCreatorAircraft = tempVeh
		PRINTLN("[RCC MISSION] HANDLE_PLAYER_CUTSCENE_WARP_BETWEEN_AVENGER_HOLDS - MPGlobalsAmbience.vehCreatorAircraft is already warp vehicle ",iWarpVeh)
	ENDIF
	IF NOT DOES_ENTITY_EXIST(MPGlobalsAmbience.vehCreatorAircraft)
		PRINTLN("[RCC MISSION] HANDLE_PLAYER_CUTSCENE_WARP_BETWEEN_AVENGER_HOLDS - MPGlobalsAmbience.vehCreatorAircraft doesn't exist yet")
	ENDIF
	#ENDIF
	
	PRINTLN("[RCC MISSION] HANDLE_PLAYER_CUTSCENE_WARP_BETWEEN_AVENGER_HOLDS - Set MPGlobalsAmbience.vehCreatorAircraft to equal creator vehicle ",iWarpVeh)
	MPGlobalsAmbience.vehCreatorAircraft = tempVeh
	
ENDPROC

/// PURPOSE:
///    Get the vehicle to use for warping from the cutscene ped warp data
FUNC VEHICLE_INDEX GET_CUTSCENE_PED_WARP_VEHICLE(FMMC_CUTSCENE_PED_WARP_DATA &sWarpData)
	
	IF sWarpData.iVehID = -1
		PRINTLN("[RCC MISSION] GET_CUTSCENE_PED_WARP_VEHICLE - iVehId is invalid")
		RETURN NULL
	ENDIF
	
	NETWORK_INDEX vehNetId = MC_serverBD_1.sFMMC_SBD.niVehicle[sWarpData.iVehID]
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(vehNetId)
	
		PRINTLN("[RCC MISSION] GET_CUTSCENE_PED_WARP_VEHICLE - iVehID: ",  sWarpData.iVehID, " - Primary vehicle does not exist, falling back to secondary")
	
		IF sWarpData.iVehIDSecondary = -1
			PRINTLN("[RCC MISSION] GET_CUTSCENE_PED_WARP_VEHICLE - no secondary id found, bail")
			RETURN NULL
		ENDIF
		
		vehNetId = MC_serverBD_1.sFMMC_SBD.niVehicle[sWarpData.iVehIDSecondary]
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(vehNetId)
			PRINTLN("[RCC MISSION] GET_CUTSCENE_PED_WARP_VEHICLE - iVehIDSecondary: ", sWarpData.iVehIDSecondary, " - secondary vehicle does not exist, bail")
			RETURN NULL
		ENDIF
	ENDIF
	
	ENTITY_INDEX entity = NET_TO_ENT(vehNetId)
	IF NOT DOES_ENTITY_EXIST(entity)
		PRINTLN("[RCC MISSION] GET_CUTSCENE_PED_WARP_VEHICLE - DOES_ENTITY_EXIST = FALSE")
		RETURN NULL
	ENDIF
	
	VEHICLE_INDEX veh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entity)
	IF NOT IS_VEHICLE_DRIVEABLE(veh)
		PRINTLN("[RCC MISSION] GET_CUTSCENE_PED_WARP_VEHICLE - veh is undrivable")
		RETURN NULL
	ENDIF
	
	PRINTLN("[RCC MISSION] GET_CUTSCENE_PED_WARP_VEHICLE - veh: ", NATIVE_TO_INT(veh)," - Vehicle found")
	RETURN veh
ENDFUNC

/// PURPOSE:
///    Get the vehicle seat to use for warping from the veh seat preference
FUNC VEHICLE_SEAT GET_CUTSCENE_PED_WARP_VEHICLE_SEAT(PED_INDEX ped, VEHICLE_INDEX veh, INT iVehSeatPreference, VEHICLE_SEAT eCurrentSeat = VS_ANY_PASSENGER)

	IF iVehSeatPreference = ciFMMC_SeatPreference_None
		PRINTLN("[RCC MISSION] GET_CUTSCENE_PED_WARP_VEHICLE_SEAT - iSeat = ciFMMC_SeatPreference_None, bail")
		RETURN VS_ANY_PASSENGER
	ENDIF
	
	VEHICLE_SEAT vehSeat = VS_ANY_PASSENGER
	IF iVehSeatPreference = ciFMMC_Cutscene_SeatPreference_SameAsCurrentVeh
		
		IF IS_PED_IN_ANY_VEHICLE(ped)
			VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(ped)
			INT iMaxSeats = (GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(tempVeh) + 1) //Add 1 for the driver
			INT i
			REPEAT iMaxSeats i
				VEHICLE_SEAT vehSeatTemp = INT_TO_ENUM(VEHICLE_SEAT, i-1)
				IF GET_PED_IN_VEHICLE_SEAT(tempVeh, vehSeatTemp) = ped
					vehSeat = vehSeatTemp
				ENDIF
			ENDREPEAT
		ELSE
			vehSeat = eCurrentSeat
		ENDIF
	ELSE
		SWITCH iVehSeatPreference
			CASE ciFMMC_SeatPreference_Driver		vehSeat = VS_DRIVER			BREAK
			CASE ciFMMC_SeatPreference_Passenger	vehSeat = VS_ANY_PASSENGER	BREAK 
			CASE ciFMMC_SeatPreference_Rear_right	vehSeat = VS_BACK_RIGHT		BREAK 
			CASE ciFMMC_SeatPreference_Rear_left	vehSeat = VS_BACK_LEFT		BREAK 
			CASE ciFMMC_SeatPreference_Front_Seats	vehSeat = VS_FRONT_RIGHT	BREAK 
		ENDSWITCH
	ENDIF

	// Figure out seat to use if non-specfic of seat is not available
	IF vehSeat = VS_ANY_PASSENGER
	OR NOT IS_VEHICLE_SEAT_FREE(veh, vehSeat)

		IF vehSeat = VS_ANY_PASSENGER
			PRINTLN("[RCC MISSION] GET_CUTSCENE_PED_WARP_VEHICLE_SEAT - unable to find my vehicle seat in previous vehicle (?) - need to grab a free one")
		ELSE
			PRINTLN("[RCC MISSION] GET_CUTSCENE_PED_WARP_VEHICLE_SEAT - vehicle seat ",ENUM_TO_INT(vehSeat)," in previous vehicle isn't free in new vehicle - need to grab a free one")
		ENDIF
		vehSeat = GET_FIRST_FREE_VEHICLE_SEAT_RC(veh)
		PRINTLN("[RCC MISSION] GET_CUTSCENE_PED_WARP_VEHICLE_SEAT - GET_FIRST_FREE_VEHICLE_SEAT_RC returned ",ENUM_TO_INT(vehSeat))
	ENDIF
	
	RETURN vehSeat
ENDFUNC

/// PURPOSE:
///  Warps a ped to a vehicle using the cutscene ped warp data  
/// PARAMS:
///    ped - 
///    sWarpData - 
///    eCurrentSeat - 
/// RETURNS:
///    TRUE if the ped was warped to a vehicle
FUNC BOOL CUTSCENE_WARP_PED_TO_VEHICLE_COMMON(PED_INDEX ped, FMMC_CUTSCENE_PED_WARP_DATA &sWarpData, VEHICLE_SEAT eCurrentSeat = VS_ANY_PASSENGER)

	IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
		RETURN FALSE
	ENDIF

	VEHICLE_INDEX vehWarpTo = GET_CUTSCENE_PED_WARP_VEHICLE(sWarpData)
	
	// No vehicle - no further action
	IF vehWarpTo = NULL
		PRINTLN("[RCC MISSION] CUTSCENE_WARP_PED_TO_VEHICLE_COMMON - vehWarpTo = NULL, bail")
		RETURN FALSE
	ENDIF

	VEHICLE_SEAT vehSeat = GET_CUTSCENE_PED_WARP_VEHICLE_SEAT(ped, vehWarpTo, sWarpData.iVehSeat, eCurrentSeat)
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
	OR (!GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION() AND  vehSeat != VS_ANY_PASSENGER)
		IF NOT PLACE_PED_IN_VEHICLE_SEAT_IF_POSSIBLE(vehWarpTo, ped, vehSeat)
			// failed to place ped in vehicle
			PRINTLN("[RCC MISSION] CUTSCENE_WARP_PED_TO_VEHICLE_COMMON - failed to place ped in vehicle seat: ", vehSeat, ", bail")
			RETURN FALSE
		ENDIF
	ENDIF
	
	PRINTLN("[RCC MISSION] CUTSCENE_WARP_PED_TO_VEHICLE_COMMON - Placed ped in vehicle")
	
	IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(vehWarpTo)
		// no controller, can't manipulate engine, etc
		PRINTLN("[RCC MISSION] CUTSCENE_WARP_PED_TO_VEHICLE_COMMON - no net control, no further work required")
		RETURN TRUE
	ENDIF
	
	//
	// Engine on stuff
	//
	BOOL bEngineOn = IS_BIT_SET(sWarpData.iBitset1, ciCSPW_BS1_VEHICLE_START_ENGINE)
	BOOL bEngineOnInstantly = IS_BIT_SET(sWarpData.iBitset1, ciCSPW_BS1_VEHICLE_START_ENGINE_INSTANTLY)

	IF bEngineOn
		
		SET_VEHICLE_ENGINE_ON(vehWarpTo, TRUE, bEngineOnInstantly)
		IF bEngineOnInstantly
			MODEL_NAMES model = GET_ENTITY_MODEL(vehWarpTo)
			IF IS_THIS_MODEL_A_HELI(model)
			OR IS_THIS_MODEL_A_PLANE(model)	
				SET_HELI_BLADES_FULL_SPEED(vehWarpTo)
			ENDIF
		ENDIF
		
		PRINTLN("[RCC MISSION] CUTSCENE_WARP_PED_TO_VEHICLE_COMMON - Engine on - bEngineOn: ", BOOL_TO_STRING(bEngineOn), " bEngineOnInstantly: ", BOOL_TO_STRING(bEngineOnInstantly))
	ENDIF

	RETURN TRUE
ENDFUNC

PROC CUTSCENE_WARP_PLACED_PEDS_TO_VEHICLES(FMMC_CUTSCENE_PLACED_PED_WARP_DATA& sPeds[])

	IF NOT IS_BIT_SET(iLocalBoolCheck8, LBOOL8_MANUAL_PLACE_PED_IN_VEH)
		EXIT
	ENDIF

	INT i
	
	// Placed Peds
	FOR i = 0 TO COUNT_OF(sPeds)-1

		IF sPeds[i].iPedID <= -1
			PRINTLN("[RCC MISSION] CUTSCENE_WARP_PLACED_PEDS_TO_VEHICLES - i: ", i, " - iPedId: ", sPeds[i].iPedID, " - invalid id")
			RELOOP
		ENDIF
	
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[sPeds[i].iPedID])
			PRINTLN("[RCC MISSION] CUTSCENE_WARP_PLACED_PEDS_TO_VEHICLES - i: ", i, " - iPedId: ", sPeds[i].iPedID, " - invalid net id")
			RELOOP
		ENDIF
		
		PED_INDEX ped = NET_TO_PED( MC_serverBD_1.sFMMC_SBD.niPed[sPeds[i].iPedID])
		IF NETWORK_HAS_CONTROL_OF_ENTITY(ped)
			CUTSCENE_WARP_PED_TO_VEHICLE_COMMON(ped, sPeds[i].sTarget)
		ENDIF
	ENDFOR

ENDPROC

PROC GET_SCRIPTED_CUTSCENE_END_WARP_TARGET(INT iCutscene, INT iPlayerCutId, VECTOR &vWarpCoord, FLOAT &fWarpHeading, VEHICLE_INDEX &vehWarpTarget)

	// always grab this, we just dont necessarily use it's coords for the warp
	vehWarpTarget = GET_CUTSCENE_PED_WARP_VEHICLE(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sPlayerEndWarp[iPlayerCutId])

	// Only try and use the vehicle coords if there wasn't already valid coords
	IF IS_VECTOR_ZERO(vWarpCoord)
		IF DOES_ENTITY_EXIST(vehWarpTarget)
		AND NOT IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutscene].iCutsceneBitSet2, ci_CSBS2_RemoveFromVehEnd)
			
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			AND GET_VEHICLE_PED_IS_IN(LocalPlayerPed) = vehWarpTarget
				PRINTLN("[RCC MISSION] GET_SCRIPTED_CUTSCENE_END_WARP_TARGET - Already inside this vehicle, do not use it as target for warp.")
				EXIT
			ENDIF
			
			vWarpCoord = GET_ENTITY_COORDS(vehWarpTarget, FALSE)
			fWarpHeading = GET_ENTITY_HEADING(vehWarpTarget)
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL DOES_THIS_CUTSCENE_WARP_INTO_AN_INTERIOR(INT iCutsceneToUse, VECTOR vPlayerPos, VECTOR vTargetCoord)
	
	// Script automated.
	IF NOT IS_VECTOR_ZERO(vPlayerPos)
	AND NOT IS_VECTOR_ZERO(vTargetCoord)
	
		INTERIOR_INSTANCE_INDEX interiorPlayer = GET_INTERIOR_AT_COORDS(vPlayerPos)
		INTERIOR_INSTANCE_INDEX interiorTarget = GET_INTERIOR_AT_COORDS(vTargetCoord)
		
		// if the interior instances do not match then we know we are either going in/out of an interior
		IF interiorPlayer != interiorTarget
			PRINTLN("PROCESS_SCRIPTED_CUTSCENE - DOES_THIS_CUTSCENE_WARP_INTO_AN_INTERIOR - Returning TRUE. player is moving between interior & outside or 2 different interiors")
			RETURN TRUE
		
		// they match so no interior warp is needed
		ELSE
			PRINTLN("PROCESS_SCRIPTED_CUTSCENE - DOES_THIS_CUTSCENE_WARP_INTO_AN_INTERIOR - Returning FALSE. player is not moving between interiors or interior/outside.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Old Manual setting which content used to do.
	RETURN IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Warp_Into_Interior)
ENDFUNC

#IF IS_DEBUG_BUILD

FUNC STRING GET_SCRIPTED_CUTSCENE_END_WARP_STATE_NAME(SCRIPTED_CUTSCENE_END_WARP_LOCAL_STATE eState)
	SWITCH eState
		CASE SCUT_EWLS_INIT 				RETURN "SCUT_EWLS_INIT"
		CASE SCUT_EWLS_WARP 				RETURN "SCUT_EWLS_WARP"
		CASE SCUT_EWLS_WAIT_FOR_VEHICLE 	RETURN "SCUT_EWLS_WAIT_FOR_VEHICLE"
		CASE SCUT_EWLS_WAIT_FOR_INTERIOR	RETURN "SCUT_EWLS_WAIT_FOR_INTERIOR"
		CASE SCUT_EWLS_END 					RETURN "SCUT_EWLS_END"
	ENDSWITCH
	RETURN "INVALID"
ENDFUNC

#ENDIF

PROC SET_PROCESS_SCRIPTED_CUTSCENE_END_WARP_STATE(SCRIPTED_CUTSCENE_END_WARP_LOCAL_STATE eState)
	IF sScriptedCutsceneEndWarp.eState = eState
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	PRINTLN("SET_PROCESS_SCRIPTED_CUTSCENE_END_WARP_STATE - Changing state: ", GET_SCRIPTED_CUTSCENE_END_WARP_STATE_NAME(sScriptedCutsceneEndWarp.eState), " -> ", GET_SCRIPTED_CUTSCENE_END_WARP_STATE_NAME(eState))
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	sScriptedCutsceneEndWarp.eState = eState
ENDPROC

FUNC BOOL PROCESS_SCRIPTED_CUTSCENE_END_WARP(INT iCutscene, INT iPlayerCutID, VECTOR vEndPos, FLOAT fEndPos, BOOL bQuickWarp)
	
	IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
		// Bail and finish for spectators
		PRINTLN("PROCESS_SCRIPTED_CUTSCENE_END_WARP - iCutscene: ", iCutscene, " iPlayerCutID: ", iPlayerCutID, " - IS_LOCAL_PLAYER_ANY_SPECTATOR = TRUE, Returning TRUE")
		RETURN TRUE
	ENDIF

	//
	// STATE: Init
	//
	IF sScriptedCutsceneEndWarp.eState = SCUT_EWLS_INIT
	
		IF bIsLocalPlayerHost
			IF MC_ServerBD_1.iScriptedCutsceneSyncScene > -1
				NETWORK_STOP_SYNCHRONISED_SCENE(MC_ServerBD_1.iScriptedCutsceneSyncScene)
				MC_ServerBD_1.iScriptedCutsceneSyncScene = -1
				PRINTLN("[LM][RCC MISSION][ScriptedAnimSyncScene] - PROCESS_SCRIPTED_CUTSCENE_END_WARP - Cleaned up sync scene as we are warping...")
			ENDIF
		ENDIF
		
		IF IS_NEW_LOAD_SCENE_ACTIVE()
			NEW_LOAD_SCENE_STOP()
		ENDIF
		
		// set the end pos
		sScriptedCutsceneEndWarp.vWarpCoord = vEndPos
		sScriptedCutsceneEndWarp.fWarpHeading = fEndPos
		
		PRINTLN("PROCESS_SCRIPTED_CUTSCENE_END_WARP - 	(before)	vEndPos: ", vEndPos, " fEndPos: ", fEndPos)
		
		// check the end target (this may overwrite the end pos to be that of the vehicle if the existing end pos was invalid)
		GET_SCRIPTED_CUTSCENE_END_WARP_TARGET(iCutscene, iPlayerCutID, 
			sScriptedCutsceneEndWarp.vWarpCoord,
			sScriptedCutsceneEndWarp.fWarpHeading,
			sScriptedCutsceneEndWarp.vehTarget)
			
		PRINTLN("PROCESS_SCRIPTED_CUTSCENE_END_WARP - iCutscene: ", iCutscene, " iPlayerCutID: ", iPlayerCutID, " - Initialized end target")
		PRINTLN("PROCESS_SCRIPTED_CUTSCENE_END_WARP - 		vWarpCoord: ", sScriptedCutsceneEndWarp.vWarpCoord)
		PRINTLN("PROCESS_SCRIPTED_CUTSCENE_END_WARP - 		fWarpHeading: ", sScriptedCutsceneEndWarp.fWarpHeading)
		PRINTLN("PROCESS_SCRIPTED_CUTSCENE_END_WARP - 		vehTarget: ", NATIVE_TO_INT(sScriptedCutsceneEndWarp.vehTarget))

		// If we have a vehicle move it
		IF NOT IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutscene].iCutsceneBitSet2, ci_CSBS2_DoNotWarpToEndCoords)
		AND DOES_ENTITY_EXIST(sScriptedCutsceneEndWarp.vehTarget)
		AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(NETWORK_GET_NETWORK_ID_FROM_ENTITY(sScriptedCutsceneEndWarp.vehTarget))
			PRINTLN("PROCESS_SCRIPTED_CUTSCENE_END_WARP - iCutscene: ", iCutscene, " iPlayerCutID: ", iPlayerCutID, " - Target vehicle found, moving to end coords")
			SET_ENTITY_COORDS(sScriptedCutsceneEndWarp.vehTarget, sScriptedCutsceneEndWarp.vWarpCoord)
			SET_ENTITY_HEADING(sScriptedCutsceneEndWarp.vehTarget, sScriptedCutsceneEndWarp.fWarpHeading)
			SET_VEHICLE_ON_GROUND_PROPERLY(sScriptedCutsceneEndWarp.vehTarget)
		ENDIF
		
		// cache the current seat case the net warp removes them from the seat and we need this info later
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			sScriptedCutsceneEndWarp.eCurrentSeat = GET_SEAT_PED_IS_IN(LocalPlayerPed)
			PRINTLN("PROCESS_SCRIPTED_CUTSCENE_END_WARP - iCutscene: ", iCutscene, " iPlayerCutID: ", iPlayerCutID, " - player currently in a vehicle, setting seat: ", sScriptedCutsceneEndWarp.eCurrentSeat)
		ENDIF
		
		SET_PROCESS_SCRIPTED_CUTSCENE_END_WARP_STATE(SCUT_EWLS_WARP)
	ENDIF
	
	//
	// STATE: Warp
	//
	IF sScriptedCutsceneEndWarp.eState = SCUT_EWLS_WARP
	
		PRINTLN("PROCESS_SCRIPTED_CUTSCENE_END_WARP - SCUT_EWLS_WARP")
		
		// Wait on building swap
		IF g_bPerformingBuildingSwap
			PRINTLN("PROCESS_SCRIPTED_CUTSCENE_END_WARP - iCutscene: ", iCutscene, " iPlayerCutID: ", iPlayerCutID, " - waiting on building swap, g_bPerformingBuildingSwap: ", BOOL_TO_STRING(g_bPerformingBuildingSwap))
			RETURN FALSE
		ENDIF		
		
		// WARP
		IF NOT IS_VECTOR_ZERO(sScriptedCutsceneEndWarp.vWarpCoord)
		AND NOT IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutscene].iCutsceneBitSet2, ci_CSBS2_DoNotWarpToEndCoords)
		
			BOOL bKeepVehicle
			IF NOT IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutscene].iCutsceneBitSet2, ci_CSBS2_RemoveFromVehEnd)
				bKeepVehicle = TRUE
			ENDIF			
			IF sScriptedCutsceneEndWarp.vehTarget != NULL
			
				IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				AND GET_VEHICLE_PED_IS_IN(LocalPlayerPed) = sScriptedCutsceneEndWarp.vehTarget
					bKeepVehicle = TRUE
				ELSE				
					bKeepVehicle = FALSE
				ENDIF
				
			ENDIF
			
			PRINTLN("PROCESS_SCRIPTED_CUTSCENE_END_WARP - iCutscene: ", iCutscene, " bKeepVehicle: ", bKeepVehicle)
						
			IF NOT NET_WARP_TO_COORD(
				sScriptedCutsceneEndWarp.vWarpCoord, 
				sScriptedCutsceneEndWarp.fWarpHeading, bKeepVehicle, DEFAULT, DEFAULT, DEFAULT, DEFAULT, bQuickWarp)
				RETURN FALSE
			ENDIF
			
		ENDIF
		
		IF eCSLift != eCSLift_None
		AND IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].iCutsceneBitset2, ci_CSBS2_Scripted_UsePlayerClones)
		AND ENUM_TO_INT(eLiftState) > ENUM_TO_INT(eLMS_AttachClones)
		AND g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].fLiftZOffset_Start != 0
			PRINTLN("[RCC MISSION][CSLift] PROCESS_SCRIPTED_CUTSCENE_END_WARP - Unfreeze the player now")
			FREEZE_ENTITY_POSITION(LocalPlayerPed, FALSE)
		ENDIF
		
		// POST WARP - Need to place in target vehicle
		IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].iCutsceneBitset, ci_CSBS_Rolling_Start)
		AND CUTSCENE_WARP_PED_TO_VEHICLE_COMMON(LocalPlayerPed, g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sPlayerEndWarp[iPlayerCutID], sScriptedCutsceneEndWarp.eCurrentSeat)
		AND sScriptedCutsceneEndWarp.vehTarget != NULL
			// B*4238761: only go to SCUT_EWLS_WAIT_FOR_VEHICLE if we're going to need a rolling start
			// otherwise this will cause a delay for situations where we don't care about passing the ped in vehicle check
			SET_PROCESS_SCRIPTED_CUTSCENE_END_WARP_STATE(SCUT_EWLS_WAIT_FOR_VEHICLE)
		ELIF GET_INTERIOR_AT_COORDS(GET_ENTITY_COORDS(LocalPlayerPed)) != NULL
			SET_PROCESS_SCRIPTED_CUTSCENE_END_WARP_STATE(SCUT_EWLS_WAIT_FOR_INTERIOR)
		ELSE
			SET_PROCESS_SCRIPTED_CUTSCENE_END_WARP_STATE(SCUT_EWLS_END)
		ENDIF
	
		PRINTLN("PROCESS_SCRIPTED_CUTSCENE_END_WARP - SCUT_EWLS_WARP END")
	ENDIF
	
	//
	// STATE: Wait for vehicle
	//
	IF sScriptedCutsceneEndWarp.eState = SCUT_EWLS_WAIT_FOR_VEHICLE
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			SET_PROCESS_SCRIPTED_CUTSCENE_END_WARP_STATE(SCUT_EWLS_END)
		ELSE
			PRINTLN("[RCC MISSION][CSLift] PROCESS_SCRIPTED_CUTSCENE_END_WARP - Ped not in Vehicle. Waiting...")
		ENDIF		
		
		IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, TRUE)
		AND GET_SCRIPT_TASK_STATUS(LocalPlayerPed, SCRIPT_TASK_ENTER_VEHICLE) != WAITING_TO_START_TASK
		AND GET_SCRIPT_TASK_STATUS(LocalPlayerPed, SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK
			PRINTLN("[RCC MISSION][CSLift] PROCESS_SCRIPTED_CUTSCENE_END_WARP - Ped not in Vehicle. We also don't have a task, escaping limbo, attempting again.")
			SET_PROCESS_SCRIPTED_CUTSCENE_END_WARP_STATE(SCUT_EWLS_WARP)
		ENDIF
	ENDIF
	
	IF sScriptedCutsceneEndWarp.eState = SCUT_EWLS_WAIT_FOR_INTERIOR
		
		//Casino
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_EnableCasino_InteriorScripts)
			IF NOT g_bInitCasinoPedsCreated
			AND IS_PLAYER_WITHIN_SIMPLE_INTERIOR_BOUNDS(SIMPLE_INTERIOR_CASINO)
				IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_REQUEST_CASINO_INTERIOR_SCRIPT)
						REQUEST_RELAUCHING_OF_SIMPLE_INTERIOR_INT_SCRIPT(SIMPLE_INTERIOR_CASINO)
						SET_BIT(iLocalBoolCheck31, LBOOL31_REQUEST_CASINO_INTERIOR_SCRIPT)
						PRINTLN("[RCC MISSION]PROCESS_SCRIPTED_CUTSCENE_END_WARP - REQUEST_RELAUCHING_OF_SIMPLE_INTERIOR_INT_SCRIPT(SIMPLE_INTERIOR_CASINO), LBOOL31_REQUEST_CASINO_INTERIOR_SCRIPT SET")
					ENDIF
				
				PRINTLN("[RCC MISSION]PROCESS_SCRIPTED_CUTSCENE_END_WARP - waiting for the casino peds to be created")
				RETURN FALSE
			ENDIF
		ENDIF
		
		SET_PROCESS_SCRIPTED_CUTSCENE_END_WARP_STATE(SCUT_EWLS_END)
		
	ENDIF
	
	//
	// STATE: End
	//
	IF sScriptedCutsceneEndWarp.eState = SCUT_EWLS_END
	
		DO_POST_CUTSCENE_COMMON_PLAYER_EXIT(iCutscene, FMMCCUT_SCRIPTED)
		
		// RESET
		SCRIPTED_CUTSCENE_END_WARP_LOCAL sEmpty
		sScriptedCutsceneEndWarp = sEmpty
		
		PRINTLN("PROCESS_SCRIPTED_CUTSCENE_END_WARP - iCutscene: ", iCutscene, " iPlayerCutID: ", iPlayerCutID, " - COMPLETE")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC clone_player_for_fade()
	PED_INDEX tempPed
	MODEL_NAMES tempModel
	tempPed = LocalPlayerPed
	tempModel = GET_ENTITY_MODEL(tempPed)
	
	REQUEST_MODEL(tempModel)
	IF HAS_MODEL_LOADED(tempModel)
		IF NOT DOES_ENTITY_EXIST(ClonePlayerForCutscene)
			VECTOR vCoOrd = GET_ENTITY_COORDS(tempPed)
			vCoOrd.z -= 1
			IF IS_PLAYER_PED_FEMALE(LocalPlayer)
				ClonePlayerForCutscene = CREATE_PED(PEDTYPE_CIVFEMALE, tempModel,vCoOrd, GET_ENTITY_HEADING(tempPed), FALSE, FALSE)
			ELSE
				ClonePlayerForCutscene = CREATE_PED(PEDTYPE_CIVMALE, tempModel,vCoOrd, GET_ENTITY_HEADING(tempPed), FALSE, FALSE)
			ENDIF
			
			SET_NETWORK_ID_CAN_MIGRATE(PED_TO_NET(ClonePlayerForCutscene), FALSE)
			CLONE_PED_TO_TARGET(tempPed, ClonePlayerForCutscene)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ClonePlayerForCutscene, TRUE)
			SET_PED_RELATIONSHIP_GROUP_HASH(ClonePlayerForCutscene, rgFM_AiLike)
			SET_ENTITY_INVINCIBLE(ClonePlayerForCutscene,TRUE)
			SET_ENTITY_VISIBLE_IN_CUTSCENE(ClonePlayerForCutscene,FALSE)
			SET_ENTITY_VISIBLE(ClonePlayerForCutscene, FALSE)
			//NETWORK_FADE_OUT_ENTITY(ClonePlayerForCutscene, FALSE, TRUE)
		ENDIF
	ENDIF
ENDPROC

PROC update_clone_player(BOOL bStartCut)
	IF bStartCut
		SET_ENTITY_VISIBLE(ClonePlayerForCutscene, TRUE)
	ELSE
		IF DOES_ENTITY_EXIST(ClonePlayerForCutscene) 
			IF NOT IS_CUTSCENE_PLAYING()
				PED_INDEX tempPed = LocalPlayerPed
				VECTOR vCoOrd = GET_ENTITY_COORDS(tempPed)
				vCoOrd.z -= 1
				SET_ENTITY_COORDS(ClonePlayerForCutscene, vCoOrd)
			ELSE
				SET_ENTITY_VISIBLE(ClonePlayerForCutscene, TRUE)
				SET_ENTITY_VISIBLE_IN_CUTSCENE(ClonePlayerForCutscene,FALSE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_SPECIFIC_PLAYER_WATCH_TEAM_CUTSCENE( PLAYER_INDEX piPlayer, INT iteam, INT iParticipant, INT icutscene, BOOL bIsMocap )
	
	IF IS_PLAYER_SPECTATING(piPlayer)
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(piPlayer)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bInMissionCreatorApartment) = TRUE
			RETURN FALSE
		ENDIF
	ENDIF

	IF HAS_TEAM_FAILED(MC_playerBD[iParticipant].iteam)
	AND NOT IS_BIT_SET( MC_playerBD[iParticipant].iClientBitSet, PBBOOL_PLAYING_CUTSCENE )
		RETURN FALSE
	ENDIF
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iParticipant].iteam] >= FMMC_MAX_RULES
	AND NOT IS_BIT_SET( MC_playerBD[iParticipant].iClientBitSet, PBBOOL_PLAYING_CUTSCENE )
		RETURN FALSE
	ENDIF

	IF iteam = MC_playerBD[iParticipant].iteam
		RETURN TRUE
	ENDIF

	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		IF bIsMocap
			SWITCH MC_playerBD[iParticipant].iteam
			
				CASE 0
					IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[icutscene].iCutsceneBitset,ci_CSBS_Pull_Team0_In)
						RETURN TRUE
					ENDIF
				BREAK
				CASE 1
					IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[icutscene].iCutsceneBitset,ci_CSBS_Pull_Team1_In)
						RETURN TRUE
					ENDIF		
				BREAK
				CASE 2
					IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[icutscene].iCutsceneBitset,ci_CSBS_Pull_Team2_In)
						RETURN TRUE
					ENDIF		
				BREAK
				CASE 3
					IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[icutscene].iCutsceneBitset,ci_CSBS_Pull_Team3_In)
						RETURN TRUE
					ENDIF		
				BREAK
			ENDSWITCH		
		ELSE
			SWITCH MC_playerBD[iParticipant].iteam
			
				CASE 0
					IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].iCutsceneBitset,ci_CSBS_Pull_Team0_In)
						RETURN TRUE
					ENDIF
				BREAK
				CASE 1
					IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].iCutsceneBitset,ci_CSBS_Pull_Team1_In)
						RETURN TRUE
					ENDIF		
				BREAK
				CASE 2
					IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].iCutsceneBitset,ci_CSBS_Pull_Team2_In)
						RETURN TRUE
					ENDIF		
				BREAK
				CASE 3
					IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].iCutsceneBitset,ci_CSBS_Pull_Team3_In)
						RETURN TRUE
					ENDIF		
				BREAK
			ENDSWITCH
		ENDIF
		
	ENDIF
	
	RETURN FALSE

ENDFUNC

//Check if the player is in the cutscene list
FUNC BOOL IS_PLAYER_IN_CUTSCENE_LIST(PLAYER_INDEX piPlayer, INT iCutsceneTeam)
	
	INT iCount
	FOR iCount = 0 TO FMMC_MAX_CUTSCENE_PLAYERS - 1
		IF MC_serverBD.piCutscenePlayerIDs[iCutsceneTeam][iCount] != INVALID_PLAYER_INDEX()
			IF MC_serverBD.piCutscenePlayerIDs[iCutsceneTeam][iCount] = piPlayer
				RETURN TRUE
				PRINTLN("[RCC MISSION] IS_PLAYER_IN_CUTSCENE_LIST TRUE ", NATIVE_TO_INT(piPlayer))
			ENDIF
		ENDIF
	ENDFOR
	
	PRINTLN("[RCC MISSION] IS_PLAYER_IN_CUTSCENE_LIST FALSE ", NATIVE_TO_INT(piPlayer))
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_MOCAP_SHORT_WARP_AT_END(INT iCutsceneNum)

	IF iCutsceneNum = -1
		RETURN FALSE
	ENDIF

	IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
		RETURN FALSE
	ENDIF

	IF NOT GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()	
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, TRUE)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF GET_SCRIPT_TASK_STATUS(LocalPlayerPed, SCRIPT_TASK_ENTER_VEHICLE ) = WAITING_TO_START_TASK
	OR GET_SCRIPT_TASK_STATUS(LocalPlayerPed, SCRIPT_TASK_ENTER_VEHICLE ) = PERFORMING_TASK
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_MOCAP_SHORT_WARP_REMOVE_FROM_VEHICLE(INT iCutsceneToUse)

	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
		PRINTLN("[RCC MISSION] SHOULD_MOCAP_SHORT_WARP_REMOVE_FROM_VEHICLE - Is gang ops, using result from ci_CSBS2_RemoveFromVehEnd: ", BOOL_TO_STRING(IS_BIT_SET(g_fmmc_struct.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_RemoveFromVehEnd)))
		RETURN IS_BIT_SET(g_fmmc_struct.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_RemoveFromVehEnd)
	ENDIF
	
	PRINTLN("[RCC MISSION] SHOULD_MOCAP_SHORT_WARP_REMOVE_FROM_VEHICLE - not gang ops don't remove from vehicle")
	RETURN FALSE
ENDFUNC

//Function to update on the player exit state
FUNC BOOL UPDATE_ON_PLAYER_EXIT_STATE(INT iCutsceneNum, INT iCutsceneTeam, STRING sMocapName, VECTOR vEndPos, FLOAT fEndHeading, BOOL bForce = FALSE)
	IF IS_BIT_SET(iLocalBoolCheck8, LBOOL8_MANUAL_PLAYER_EXIT_STATE_DONE)	//Manual Exit State from above has been done for player
	OR CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(tlPlayerSceneHandle)		//Exit State can be set for local player
	OR bForce = TRUE														//Where we want to force an update on the player e.g. when having no handle
		INT iApartmentWarpLocation
		INT iParticipantCounter
		INT i
		MP_PROP_OFFSET_STRUCT offsetStruct
		
		PRINTLN("[RCC MISSION] PLAYER SCENE HANDLE tlPlayerSceneHandle =", tlPlayerSceneHandle)
		
		IF NOT ARE_STRINGS_EQUAL( "MPH_PRI_FIN_MCS2", sMocapName ) //Bypassing for url:bugstar:2113204 - Causing the parachute to appear before it has been pulled.
			IF IS_LOCAL_PLAYER_PED_IN_CUTSCENE()
				FORCE_PLAYER_LOCALLY_VISIBLE_FOR_FADE()
				SET_ENTITY_LOCALLY_VISIBLE(LocalPlayerPed)
			ENDIF
		ENDIF
		
		create_zip_tie_for_cutscene()

			//IF( NOT IS_BIT_SET( iLocalBoolCheck7, LBOOL7_PRISON_BREAK_CARGO_SCENE_INIT ) )
			IF iCutsceneNum >= 0 AND iCutsceneNum < MAX_MOCAP_CUTSCENES
		
				CUTSCENE_WARP_PLACED_PEDS_TO_VEHICLES(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneNum].sPedEndWarp)
				
				INT iLocalPlayerCutId = GET_LOCAL_PLAYER_CUTSCENE_INDEX(iCutsceneTeam)
				IF iLocalPlayerCutId >= 0 AND iLocalPlayerCutId < FMMC_MAX_CUTSCENE_PLAYERS
					CUTSCENE_WARP_PED_TO_VEHICLE_COMMON(LocalPlayerPed, g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneNum].sPlayerEndWarp[iLocalPlayerCutId])
				ENDIF
					
			ENDIF
		
		
		IF CAN_MOCAP_SHORT_WARP_AT_END(iCutsceneNum)
		
			IF (IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneNum].iCutsceneBitset, ci_CSBS_WarpPlayersEnd) OR ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PAC_HAC_MCS1")) //Placeholder test fix for url:bugstar:2149906 to sure positions are set post cutscene.
			AND NOT IS_VECTOR_ZERO(vEndPos)
			AND fEndHeading != ciINVALID_CUTSCENE_WARP_HEADING
				IF NOT ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PRI_FIN_MCS2")//Temp
					IF NET_WARP_TO_COORD(vEndPos, fEndHeading, NOT SHOULD_MOCAP_SHORT_WARP_REMOVE_FROM_VEHICLE(iCutsceneNum), FALSE, FALSE, FALSE, TRUE, TRUE)
						DO_POST_CUTSCENE_COMMON_PLAYER_EXIT(iCutsceneNum, FMMCCUT_MOCAP)
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed, TRUE) //b*2151144
						PRINTSTRING("[RCC MISSION] WARPED AT THE END OF THE CUTSCENE TO: Position = ")
						PRINTVECTOR(vEndPos)
						PRINTLN(", Heading = ", fEndHeading)
					ENDIF
				ENDIF
			ELIF GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].propertyDetails.iCurrentlyInsideProperty > 0
				
				REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
					IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
						IF NOT IS_PARTICIPANT_A_SPECTATOR(i)
							IF INT_TO_PARTICIPANTINDEX(i) = PARTICIPANT_ID()
								iApartmentWarpLocation = iParticipantCounter
							ENDIF
							iParticipantCounter++
						ENDIF
					ENDIF
				ENDREPEAT
				IF iApartmentWarpLocation >=3
					iApartmentWarpLocation = 3
				ENDIF
				GET_APARTMENT_HEIST_INTERIOR_CUTSCENE_END_COORDS(offsetStruct,GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].propertyDetails.iCurrentlyInsideProperty,iApartmentWarpLocation)
				IF NET_WARP_TO_COORD(offsetStruct.vLoc, offsetStruct.vRot.z, TRUE,FALSE,FALSE,FALSE,TRUE,TRUE)
					PRINTSTRING("[RCC MISSION] WARPED INTERIOR_CUTSCENE_END_COORDS: Position = ")
					PRINTVECTOR(offsetStruct.vLoc)
					PRINTLN(", Heading = ", offsetStruct.vRot.z)
					
					//b*2151144
					FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed, TRUE)
				ENDIF
				
			ENDIF
		ENDIF

		//Restore the female ped feet to stored at start
		RESTORE_FEMALE_PED_FEET( LocalPlayerPed )
	
		//restore the player weapon if not invalid
		if weapMocap != WEAPONTYPE_INVALID
			SET_CURRENT_PED_WEAPON( LocalPlayerPed, weapMocap, TRUE )
			weapMocap = WEAPONTYPE_INVALID
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Cutscene handles manually being checked for exit states
PROC HANDLE_MANUAL_EXIT_STATES( INT iCutsceneNum, INT iCutsceneTeam, STRING sMocapName, VECTOR vEndPos, FLOAT fEndHeading, FMMC_CUTSCENE_TYPE eCutType)//, WEAPON_TYPE weaponToArmOnExitState)


		//************************************CUTSCENE SPECIFIC EXIT STATES***************************
		// If doing manual exit state for a player make sure bit LBOOL8_MANUAL_PLAYER_EXIT_STATE_DONE
		// is set as CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY will only return once and won't enter
		// the default checks below otherwise.
		// In the event the cutscene has no player handle then the player logic is handled within
		// the camera exit state e.g. cutscene that don't include the player
		//********************************************************************************************

		IF iCutsceneNum != -1
			IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PAC_WIT_MCS1")
			
				IF NOT HAS_CLIP_SET_LOADED("clipset@veh@boat@predator@ps@base")
					REQUEST_CLIP_SET("clipset@veh@boat@predator@ps@base")
				ENDIF
				
				//Called when manually placing peds into vehicle on cutscene
				IF NOT IS_BIT_SET(iLocalBoolCheck8, LBOOL8_MANUAL_PLACE_PED_IN_VEH)
					SET_BIT(iLocalBoolCheck8, LBOOL8_MANUAL_PLACE_PED_IN_VEH)
				ENDIF
			
				INT iVehId = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneNum].sPlayerEndWarp[0].iVehID
				IF iVehId > -1
			
					VEHICLE_INDEX veh = NET_TO_VEH( MC_serverBD_1.sFMMC_SBD.niVehicle[ iVehId ] )

					IF IS_VEHICLE_DRIVEABLE( veh )
						
						//Post cutscene Avi
						ENTITY_INDEX entLaundPostCut = NET_TO_ENT(GET_CUTSCENE_NET_ID_WITH_HANDLE("Money_Launderer", iCutsceneNum))
						PED_INDEX pedLaundPostCut = GET_PED_INDEX_FROM_ENTITY_INDEX(entLaundPostCut)
						
						//Have player in control of Avi Take control of the boat
						IF NETWORK_HAS_CONTROL_OF_ENTITY(entLaundPostCut)
							SET_PED_FLEE_ATTRIBUTES(pedLaundPostCut, FA_NEVER_FLEE, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(pedLaundPostCut, CA_ALWAYS_FLEE, FALSE)
							SET_PED_COMBAT_ATTRIBUTES(pedLaundPostCut, CA_DISABLE_FLEE_FROM_COMBAT, TRUE)
							SET_PED_FLEE_ATTRIBUTES(pedLaundPostCut, FA_COWER_INSTEAD_OF_FLEE, FALSE)
							SET_PED_FLEE_ATTRIBUTES(pedLaundPostCut, FA_DISABLE_COWER, TRUE)
							
							PRINTLN("[RCC MISSION] SETTING AVI TO NEVER FLEE - FA_NEVER_FLEE(TRUE) / CA_ALWAYS_FLEE(FALSE) / CA_DISABLE_FLEE_FROM_COMBAT(TRUE) / FA_COWER_INSTEAD_OF_FLEE(FALSE) / FA_DISABLE_COWER (TRUE)")
						
							//Place Post cutscene Avi in the boat
							if GET_CUTSCENE_TIME() >= 500
								IF NOT IS_PED_IN_ANY_VEHICLE( pedLaundPostCut, TRUE)
								AND GET_SCRIPT_TASK_STATUS( pedLaundPostCut, SCRIPT_TASK_ENTER_VEHICLE ) != WAITING_TO_START_TASK
								AND GET_SCRIPT_TASK_STATUS( pedLaundPostCut, SCRIPT_TASK_ENTER_VEHICLE ) != PERFORMING_TASK
									CUTSCENE_WARP_PLACED_PEDS_TO_VEHICLES(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneNum].sPedEndWarp)
								ENDIF
							ENDIF
						ENDIF
						
						IF NETWORK_HAS_CONTROL_OF_ENTITY(veh)
							//Check the vehicle doors aren't locked b*2150180
							IF GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(veh, localplayer)
								SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(veh, FALSE)
								PRINTLN("[RCC MISSION] VEHICLE DOOR WAS LOCKED SETTING TO UNLOCKED")
							ENDIF
							
							IF NOT IS_BIT_SET(iLocalBoolCheck9, LBOOL9_PREDATOR_RADIO_OFF)
								SET_VEH_RADIO_STATION(veh, "OFF")
								SET_BIT(iLocalBoolCheck9, LBOOL9_PREDATOR_RADIO_OFF)
							ENDIF
						ENDIF
						
						//Place player in boat
						if GET_CUTSCENE_TIME() >= 500
							IF ARE_STRINGS_EQUAL(tlPlayerSceneHandle, "MP_1")
								IF NOT IS_PED_IN_ANY_VEHICLE( LocalPlayerPed, TRUE)
								AND GET_SCRIPT_TASK_STATUS( LocalPlayerPed, SCRIPT_TASK_ENTER_VEHICLE ) != WAITING_TO_START_TASK
								AND GET_SCRIPT_TASK_STATUS( LocalPlayerPed, SCRIPT_TASK_ENTER_VEHICLE ) != PERFORMING_TASK
									INT iPlayer = GET_LOCAL_PLAYER_CUTSCENE_INDEX(iCutsceneTeam)
									IF bLocalPlayerPedOk
									AND iPlayer >= 0
									AND iPlayer < FMMC_MAX_CUTSCENE_PLAYERS
										CUTSCENE_WARP_PED_TO_VEHICLE_COMMON(
											LocalPlayerPed,
											g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneNum].sPlayerEndWarp[iPlayer])
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						//Exit state for Avi
						IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Money_Launderer")
							SET_GAMEPLAY_CAM_RELATIVE_HEADING()
							SET_GAMEPLAY_CAM_RELATIVE_PITCH()
						ENDIF

						//Exit state for Boat
						IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Money_Boat")
							IF NETWORK_HAS_CONTROL_OF_ENTITY(veh)
								IF NOT IS_ENTITY_DEAD(veh)
									SET_VEHICLE_ON_GROUND_PROPERLY(veh)														
									SET_VEHICLE_ON_GROUND_PROPERLY( veh )
								ENDIF
							ENDIF
						ELSE
							//Move gameplay boat into position
							IF DOES_ENTITY_EXIST(veh)				
								IF NOT IS_ENTITY_DEAD(veh)
									IF NETWORK_HAS_CONTROL_OF_ENTITY(veh)
										SET_ENTITY_COORDS(veh, <<-2173.9836, 5140.3438, -0.8097>>)
										SET_ENTITY_HEADING(veh, -136.43)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//Custom logic for cutscene as local player is clone CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY will never return true
				IF CAN_SET_EXIT_STATE_FOR_CAMERA()
					SET_BIT(iLocalBoolCheck8, LBOOL8_MANUAL_PLAYER_EXIT_STATE_DONE) //Flag to do exit state
					SET_BIT(iLocalBoolCheck11, LBOOL11_FORCE_CAMERA_EXIT_STATE)
				ENDIF
				
			ENDIF
		ENDIF
		
		
		ENTITY_INDEX eiTemp 
		
		IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_NAR_FIN_EXT")
			INT i
			FOR i = 1 TO 4
				TEXT_LABEL_23 sLabel = "MP_"
				sLabel += i
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sLabel)
					eiTemp = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY(sLabel)
					IF DOES_ENTITY_EXIST(eiTemp)
						IF GET_PED_INDEX_FROM_ENTITY_INDEX(eiTemp) = PLAYER_PED_ID()
							SET_ENTITY_COORDS(eiTemp, <<2828.7, 4810.4, 48.5>>)
							SET_BIT(iLocalBoolCheck8, LBOOL8_MANUAL_PLAYER_EXIT_STATE_DONE)
						ENDIF
					ENDIF
				ENDIF
			ENDFOR	
		ENDIF
		
		IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_HUM_FIN_MCS1")
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MP_1")
				PRINTLN("[ROSSW] Trying to set exit state on MP_1")
				eiTemp = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_1")
				IF DOES_ENTITY_EXIST(eiTemp)
					IF GET_PED_INDEX_FROM_ENTITY_INDEX(eiTemp) = PLAYER_PED_ID()
						GIVE_WEAPON_OBJECT_TO_PED(playersWeaponObject, GET_PED_INDEX_FROM_ENTITY_INDEX(eiTemp))
						SET_ENTITY_COORDS(eiTemp, <<3536.0027, 3659.7473, 27.1219>>)
						//TASK_SWAP_WEAPON(GET_PED_INDEX_FROM_ENTITY_INDEX(eiTemp), TRUE)
						SET_PED_USING_ACTION_MODE(GET_PED_INDEX_FROM_ENTITY_INDEX(eiTemp), TRUE, -1, "DEFAULT_ACTION")
						FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_PED_INDEX_FROM_ENTITY_INDEX(eiTemp), TRUE)
						SET_BIT(iLocalBoolCheck8, LBOOL8_MANUAL_PLAYER_EXIT_STATE_DONE)
						PRINTSTRING("GIVEN WEAPON AND FORCED ANIM AND AI UPDATE - MPH_HUM_FIN_MCS1 - MP_1")
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MP_2")
				PRINTLN("[ROSSW] Trying to set exit state on MP_2")
				eiTemp = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_2")
				IF DOES_ENTITY_EXIST(eiTemp)
					IF GET_PED_INDEX_FROM_ENTITY_INDEX(eiTemp) = PLAYER_PED_ID()
						GIVE_WEAPON_OBJECT_TO_PED(playersWeaponObject, GET_PED_INDEX_FROM_ENTITY_INDEX(eiTemp))
						SET_ENTITY_COORDS(eiTemp, <<3533.7163, 3664.5588, 27.1219>>)
						//TASK_SWAP_WEAPON(GET_PED_INDEX_FROM_ENTITY_INDEX(eiTemp), TRUE)
						SET_PED_USING_ACTION_MODE(GET_PED_INDEX_FROM_ENTITY_INDEX(eiTemp), TRUE, -1, "DEFAULT_ACTION")
						FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_PED_INDEX_FROM_ENTITY_INDEX(eiTemp), TRUE)
						SET_BIT(iLocalBoolCheck8, LBOOL8_MANUAL_PLAYER_EXIT_STATE_DONE)
						PRINTSTRING("GIVEN WEAPON AND FORCED ANIM AND AI UPDATE - MPH_HUM_FIN_MCS1 - MP_2")
					ENDIF
				ENDIF
			ENDIF
				
		ENDIF
		
		//Turn the snow off 10 seconds into the cutscene
		IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PAC_FIN_EXT")
			
			PRINTLN("[RCC MISSION] MPH_PAC_FIN_EXT TIME: ", GET_CUTSCENE_TIME())
		
			IF GET_CUTSCENE_TIME() >= 10000
				PRINTLN("[RCC MISSION] HIT MID POINT IN CUTSCENE MPH_PAC_FIN_EXT")
				
				IF NOT IS_BIT_SET(iLocalBoolCheck9, LBOOL9_CUTSCENE_SNOW_OFF)
					PRINTLN("[RCC MISSION] SET SNOW AS OFF FOR MPH_PAC_FIN_EXT")
					SET_WEATHER_PTFX_USE_OVERRIDE_SETTINGS(true) // TODO(owain): find out why this is only iN NG and not in LG
					SET_WEATHER_PTFX_OVERRIDE_CURR_LEVEL(0.0)

					SET_BIT(iLocalBoolCheck9, LBOOL9_CUTSCENE_SNOW_OFF)
				ENDIF
				
				INT iCount
				
				//Remove the mask midcutscene b*2166403
				IF NOT IS_BIT_SET(iLocalBoolCheck9, LBOOL9_MASK_REMOVE_MID_CUT)
					IF CAN_REMOVE_MASKS_FOR_HEIST_CUTSCENE(sMocapCutscene, DEFAULT, eCutType, iCutsceneNum)
						FOR icount = 0 to 3
							IF DOES_ENTITY_EXIST(MocapPlayerPed[icount])
								REMOVE_MASKS_AND_REBREATHERS_FOR_CUTSCENES(MocapPlayerPed[icount], MC_serverBD.piCutscenePlayerIDs[iCutsceneTeam][icount], INT_TO_ENUM(mp_outfit_mask_enum,iMocapPlayerPedMask[icount]))
							ENDIF
						ENDFOR
						SET_BIT(iLocalBoolCheck9, LBOOL9_MASK_REMOVE_MID_CUT)
						PRINTLN("[RCC MISSION] MASKS SHOULD BE REMOVED MID CUT MPH_PAC_FIN_EXT")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
			
		IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PAC_FIN_MCS0")
		
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MP_1")
				PRINTLN("[ROSSW] Trying to set exit state on MP_1")
				eiTemp = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_1")
				IF DOES_ENTITY_EXIST(eiTemp)
					IF GET_PED_INDEX_FROM_ENTITY_INDEX(eiTemp) = PLAYER_PED_ID()
						GIVE_WEAPON_OBJECT_TO_PED(playersWeaponObject, GET_PED_INDEX_FROM_ENTITY_INDEX(eiTemp))				

						FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_PED_INDEX_FROM_ENTITY_INDEX(eiTemp))
						SET_BIT(iLocalBoolCheck8, LBOOL8_MANUAL_PLAYER_EXIT_STATE_DONE)
						PRINTSTRING("GIVEN WEAPON AND FORCED ANIM AND AI UPDATE")
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MP_2")
				PRINTLN("[ROSSW] Trying to set exit state on MP_2")			
				eiTemp = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_2")
				IF DOES_ENTITY_EXIST(eiTemp)
					IF GET_PED_INDEX_FROM_ENTITY_INDEX(eiTemp) = PLAYER_PED_ID()
						GIVE_WEAPON_OBJECT_TO_PED(playersWeaponObject, GET_PED_INDEX_FROM_ENTITY_INDEX(eiTemp))				
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_PED_INDEX_FROM_ENTITY_INDEX(eiTemp))
						SET_BIT(iLocalBoolCheck8, LBOOL8_MANUAL_PLAYER_EXIT_STATE_DONE)
						PRINTSTRING("GIVEN WEAPON AND FORCED ANIM AND AI UPDATE")
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MP_3")
				PRINTLN("[ROSSW] Trying to set exit state on MP_3")			
				eiTemp = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_3")
				IF DOES_ENTITY_EXIST(eiTemp)
					IF GET_PED_INDEX_FROM_ENTITY_INDEX(eiTemp) = PLAYER_PED_ID()
						GIVE_WEAPON_OBJECT_TO_PED(playersWeaponObject, GET_PED_INDEX_FROM_ENTITY_INDEX(eiTemp))				
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_PED_INDEX_FROM_ENTITY_INDEX(eiTemp))
						SET_BIT(iLocalBoolCheck8, LBOOL8_MANUAL_PLAYER_EXIT_STATE_DONE)
						PRINTSTRING("GIVEN WEAPON AND FORCED ANIM AND AI UPDATE")
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MP_4")
				PRINTLN("[ROSSW] Trying to set exit state on MP_4")			
				eiTemp = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_4")
				IF DOES_ENTITY_EXIST(eiTemp)
					IF GET_PED_INDEX_FROM_ENTITY_INDEX(eiTemp) = PLAYER_PED_ID()
						GIVE_WEAPON_OBJECT_TO_PED(playersWeaponObject, GET_PED_INDEX_FROM_ENTITY_INDEX(eiTemp))				
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_PED_INDEX_FROM_ENTITY_INDEX(eiTemp))
						SET_BIT(iLocalBoolCheck8, LBOOL8_MANUAL_PLAYER_EXIT_STATE_DONE)
						PRINTSTRING("GIVEN WEAPON AND FORCED ANIM AND AI UPDATE")
					ENDIF
				ENDIF
			ENDIF
		
		ENDIF
		
		IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PAC_HAC_MCS1")
		
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Server_Van2")
				ENTITY_INDEX entvan
				VEHICLE_INDEX vehvan
			
				entvan = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Server_Van2")
				IF entvan != NULL
					IF IS_ENTITY_A_VEHICLE(entvan)
						vehvan = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entvan)
						SHUT_VEHICLE_DOORS(vehvan)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_HUM_FIN_MCS1")
			//Grab cutscene chair for after cut
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("kicked_chair")
				ENTITY_INDEX entchair
				OBJECT_INDEX objchair
				
				entchair = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("kicked_chair")
				IF DOES_ENTITY_EXIST(entchair)
					objchair = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(entchair)
					SET_ENTITY_COORDS_NO_OFFSET(entchair, <<3359.2710, 3659.1433, 27.4559>>)
				ENDIF
				
				IF DOES_ENTITY_EXIST(objchair)
					FORCE_OBJECT_ROOM_TO_MATCH_PLAYER(objchair)
					SET_OBJECT_AS_NO_LONGER_NEEDED(objChair)
					PRINTLN("MPH_HUM_FIN_MCS1: Setting chair as no longer needed")
				ENDIF
			ENDIF
		ENDIF
		
		IF ARE_STRINGS_EQUAL(sMocapCutscene, "mph_hum_fin_ext")
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("valkyrie")
			
				PRINTLN("[RCC MISSION] mph_hum_fin_ext - SETTING EXIT STATE FOR valkyrie")
				//Fix for assert 2113441 - Don't actually want to do anything with the heli
				ENTITY_INDEX entHeli
				
				entHeli = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("valkyrie")
				SET_ENTITY_COLLISION(entHeli, TRUE)
			ENDIF
		ENDIF
		
		//Clear ped tasks on exit state
		IF ARE_STRINGS_EQUAL(sMocapCutscene, "mph_pac_hac_ext")
		
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MP_1")
				PRINTLN("[ROSSW] Trying to set exit state on MP_1 mph_pac_hac_ext")			
				eiTemp = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_1")
				IF NOT IS_ENTITY_DEAD(eiTemp)
					CLEAR_PED_TASKS_IMMEDIATELY(GET_PED_INDEX_FROM_ENTITY_INDEX(eiTemp))
				ENDIF
				SET_BIT(iLocalBoolCheck8, LBOOL8_MANUAL_PLAYER_EXIT_STATE_DONE)
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MP_2")
				PRINTLN("[ROSSW] Trying to set exit state on MP_2 mph_pac_hac_ext")			
				eiTemp = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_2")
				IF NOT IS_ENTITY_DEAD(eiTemp)
					CLEAR_PED_TASKS_IMMEDIATELY(GET_PED_INDEX_FROM_ENTITY_INDEX(eiTemp))
				ENDIF
				SET_BIT(iLocalBoolCheck8, LBOOL8_MANUAL_PLAYER_EXIT_STATE_DONE)
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MP_3")
				PRINTLN("[ROSSW] Trying to set exit state on MP_3 mph_pac_hac_ext")			
				eiTemp = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_3")
				IF NOT IS_ENTITY_DEAD(eiTemp)
					CLEAR_PED_TASKS_IMMEDIATELY(GET_PED_INDEX_FROM_ENTITY_INDEX(eiTemp))
				ENDIF
				SET_BIT(iLocalBoolCheck8, LBOOL8_MANUAL_PLAYER_EXIT_STATE_DONE)
			ENDIF
		
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MP_4")
				PRINTLN("[ROSSW] Trying to set exit state on MP_4 mph_pac_hac_ext")			
				eiTemp = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_4")
				IF NOT IS_ENTITY_DEAD(eiTemp)
					CLEAR_PED_TASKS_IMMEDIATELY(GET_PED_INDEX_FROM_ENTITY_INDEX(eiTemp))
				ENDIF
				SET_BIT(iLocalBoolCheck8, LBOOL8_MANUAL_PLAYER_EXIT_STATE_DONE)
			ENDIF
		
		ENDIF	
		
		IF iCutsceneNum != -1
			IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PRI_FIN_MCS1")
				 
				VECTOR vpostcutRash = <<1704.5222, 2517.5498, 44.5649>>
				ENTITY_INDEX entpostcutRash = NET_TO_ENT(GET_CUTSCENE_NET_ID_WITH_HANDLE("Rashcovski", iCutsceneNum))
				PED_INDEX pedpostcutRash = GET_PED_INDEX_FROM_ENTITY_INDEX(entpostcutRash)
				
				//Move the post cutscene Rashcovski
				//Cutscene is creating it's own entity so we can move him without pops
				if GET_CUTSCENE_TIME() >= 500
					IF NETWORK_HAS_CONTROL_OF_ENTITY(entpostcutRash)
						IF NOT IS_ENTITY_IN_RANGE_COORDS(entpostcutRash, vpostcutRash, 1.0)
							CLEAR_PED_TASKS(pedpostcutRash)
							SET_ENTITY_COORDS(entpostcutRash, vpostcutRash)
							SET_ENTITY_HEADING(entpostcutRash, 68.1016)
							SET_PED_USING_ACTION_MODE(pedpostcutRash, TRUE, -1, "DEFAULT_ACTION")
							GIVE_WEAPON_TO_PED(pedpostcutRash, WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedpostcutRash, TRUE)
							CPRINTLN(DEBUG_MISSION, "MPH_PRI_FIN_MCS1 - Setting Rash's new coords and action state.")
						ENDIF
					ELSE
						//Override rashcovski's position for players not in control b*2236158
						PRINTLN("[RCC MISSION] OVERRIDING RASHCOVSKI POSITION FOR PLAYER NOT IN CONTROL")
						NETWORK_OVERRIDE_COORDS_AND_HEADING(pedpostcutRash, vpostcutRash, 68.1016)
					ENDIF
				ENDIF

				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MP_1")
					PRINTLN("[ROSSW] Trying to set exit state on MP_1 mph_pac_hac_ext")
					SET_BIT(iLocalBoolCheck8, LBOOL8_MANUAL_PLAYER_EXIT_STATE_DONE)
					eiTemp = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_1")
					
					IF playerCutsceneWeap != WEAPONTYPE_UNARMED
					AND playerCutsceneWeap != WEAPONTYPE_INVALID
						SET_CURRENT_PED_WEAPON(GET_PED_INDEX_FROM_ENTITY_INDEX(eiTemp), playerCutsceneWeap, FALSE)
					ELSE
						SET_CURRENT_PED_WEAPON(GET_PED_INDEX_FROM_ENTITY_INDEX(eiTemp), WEAPONTYPE_APPISTOL, FALSE)
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
				
		IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PRI_FIN_MCS2")
		
			//Force peds out of cutscene
			IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
			
				VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
			
				IF GET_ENTITY_MODEL(veh) = VELUM2
					PRINTLN("[RCC MISSION] MPH_PRI_FIN_MCS2 - TASK_LEAVE_VEHICLE")
					TASK_LEAVE_VEHICLE(localPlayerPed, veh, ECF_WARP_PED)
				ENDIF
			ENDIF
	
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MP_1")
				//Clear the area to get rid of plane marked as no longer needed
				CLEAR_AREA(GET_ENTITY_COORDS(LocalPlayerPed), 1000.0, FALSE)
			
				PRINTLN("[ROSSW] Trying to set exit state on MP_1")			
				eiTemp = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_1")
				IF DOES_ENTITY_EXIST(eiTemp)
					IF GET_PED_INDEX_FROM_ENTITY_INDEX(eiTemp) = PLAYER_PED_ID()
						IF NOT IS_VECTOR_ZERO(vEndPos)
							VECTOR vTempPos = vEndPos - <<0,0,20>>
							SET_ENTITY_COORDS(eiTemp, vTempPos)
							CPRINTLN(DEBUG_MISSION, "Setting exit state coords to vEndPos for MP_1 - ", vTempPos)
						ELSE
							VECTOR vTempPos =  GET_ENTITY_COORDS(eiTemp, FALSE) - <<0,0,20>>
							CPRINTLN(DEBUG_MISSION, "Setting exit state coords for MP_1 - ", vTempPos)
							SET_ENTITY_COORDS(eiTemp, vTempPos)
						ENDIF
						CLEAN_UP_PLAYER_PED_PARACHUTE()
						TASK_PARACHUTE(GET_PED_INDEX_FROM_ENTITY_INDEX(eiTemp), FALSE, TRUE)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_PED_INDEX_FROM_ENTITY_INDEX(eiTemp), TRUE, TRUE)
						SET_BIT(iLocalBoolCheck8, LBOOL8_MANUAL_PLAYER_EXIT_STATE_DONE)
						CPRINTLN(DEBUG_MISSION, "Setting exit state for MP_1")
					ENDIF
				ENDIF
			ENDIF
		
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MP_2")
			
				//Clear the area to get rid of plane marked as no longer needed
				CLEAR_AREA(GET_ENTITY_COORDS(LocalPlayerPed), 1000.0, FALSE)
			
				PRINTLN("[ROSSW] Trying to set exit state on MP_2")			
				eiTemp = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_2")
				IF DOES_ENTITY_EXIST(eiTemp)
					IF GET_PED_INDEX_FROM_ENTITY_INDEX(eiTemp) = PLAYER_PED_ID()
						IF NOT IS_VECTOR_ZERO(vEndPos)
							VECTOR vTempPos = vEndPos - <<0,0,10>>
							SET_ENTITY_COORDS(eiTemp, vTempPos)
							CPRINTLN(DEBUG_MISSION, "Setting exit state coords to vEndPos for MP_2 - ", vTempPos)
						ELSE
							VECTOR vTempPos =  GET_ENTITY_COORDS(eiTemp, FALSE) - <<0,0,10>>
							CPRINTLN(DEBUG_MISSION, "Setting exit state coords for MP_2 - ", vTempPos)
							SET_ENTITY_COORDS(eiTemp, vTempPos)
						ENDIF
						CLEAN_UP_PLAYER_PED_PARACHUTE()
						TASK_PARACHUTE(GET_PED_INDEX_FROM_ENTITY_INDEX(eiTemp), FALSE, TRUE)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_PED_INDEX_FROM_ENTITY_INDEX(eiTemp), TRUE, TRUE)
						SET_BIT(iLocalBoolCheck8, LBOOL8_MANUAL_PLAYER_EXIT_STATE_DONE)
						CPRINTLN(DEBUG_MISSION, "Setting exit state for MP_2")
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MP_3")
			
				//Clear the area to get rid of plane marked as no longer needed
				CLEAR_AREA(GET_ENTITY_COORDS(LocalPlayerPed), 1000.0, FALSE)
			
				PRINTLN("[ROSSW] Trying to set exit state on MP_3")			
				eiTemp = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_3")
				IF DOES_ENTITY_EXIST(eiTemp)
					IF GET_PED_INDEX_FROM_ENTITY_INDEX(eiTemp) = PLAYER_PED_ID()
						IF NOT IS_VECTOR_ZERO(vEndPos)
							VECTOR vTempPos = vEndPos
							SET_ENTITY_COORDS(eiTemp, vTempPos)
							CPRINTLN(DEBUG_MISSION, "Setting exit state coords to vEndPos for MP_3 - ", vTempPos)
						ELSE
							VECTOR vTempPos =  GET_ENTITY_COORDS(eiTemp, FALSE)
							CPRINTLN(DEBUG_MISSION, "Setting exit state coords for MP_3 - ", vTempPos)
							SET_ENTITY_COORDS(eiTemp, vWarpPos)
						ENDIF
						CLEAN_UP_PLAYER_PED_PARACHUTE()
						TASK_PARACHUTE(GET_PED_INDEX_FROM_ENTITY_INDEX(eiTemp), FALSE, TRUE)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_PED_INDEX_FROM_ENTITY_INDEX(eiTemp), TRUE, TRUE)
						SET_BIT(iLocalBoolCheck8, LBOOL8_MANUAL_PLAYER_EXIT_STATE_DONE)
						CPRINTLN(DEBUG_MISSION, "Setting exit state for MP_3")
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
		
		IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PAC_FIN_MCS1")
		
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("server_van1")
				PRINTLN("[ROSSW] Trying to latch doors on server_van1")			
				eiTemp = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("server_van1")
				IF DOES_ENTITY_EXIST(eiTemp)
					SET_VEHICLE_DOOR_LATCHED(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(eiTemp), SC_DOOR_REAR_LEFT, TRUE, TRUE)
					SET_VEHICLE_DOOR_LATCHED(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(eiTemp), SC_DOOR_REAR_RIGHT, TRUE, TRUE)
				ENDIF
			ENDIF

		ENDIF
		
		IF ARE_STRINGS_EQUAL(sMocapCutscene, "subj_mcs0")
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MP_1")
				PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE subj_mcs0 - can set exit state - MP_1")
				IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneNum].iCutsceneBitSet2, ci_CSBS2_EquipOutfitForNextRule)
					SET_BIT(iLocalBoolCheck26, LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE)
					PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - subj_mcs0 - Setting LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE")
				ENDIF
				
				//-- Remove scuba tank & mask
				IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01 
					SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 15, 0) 
				ELIF GET_ENTITY_MODEL(PLAYER_PED_ID())= MP_F_FREEMODE_01 
					SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 3, 0) 
				ENDIF
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE, TRUE)
				
				SET_BIT(iLocalBoolCheck8, LBOOL8_MANUAL_PLAYER_EXIT_STATE_DONE)
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MP_2")
				PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE subj_mcs0 - can set exit state - MP_2")
				IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneNum].iCutsceneBitSet2, ci_CSBS2_EquipOutfitForNextRule)
					SET_BIT(iLocalBoolCheck26, LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE)
					PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - subj_mcs0 - Setting LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE")
				ENDIF
				
				//-- Remove scuba tank & mask
				IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01 
					SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 15, 0) 
				ELIF GET_ENTITY_MODEL(PLAYER_PED_ID())= MP_F_FREEMODE_01 
					SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 3, 0) 
				ENDIF
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE, TRUE)
				
				SET_BIT(iLocalBoolCheck8, LBOOL8_MANUAL_PLAYER_EXIT_STATE_DONE)
			ENDIF
		ENDIF
		
		
		IF ARE_STRINGS_EQUAL(sMocapCutscene, "subj_mcs1")
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MP_1")
				PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE subj_mcs1 - can set exit state - MP_1")
				// Remove the parachute
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 0, 0)


				// Flippers
				scrShopPedComponent componentItem
				MODEL_NAMES ePedModel = GET_ENTITY_MODEL(PLAYER_PED_ID())
				IF ePedModel = MP_M_FREEMODE_01
				      GET_SHOP_PED_COMPONENT(HASH("DLC_MP_X17_M_FEET_0_0"), componentItem)
				ELIF ePedModel = MP_F_FREEMODE_01
				      GET_SHOP_PED_COMPONENT(HASH("DLC_MP_X17_F_FEET_0_0"), componentItem)
				ENDIF
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_FEET, componentItem.m_drawableIndex, componentItem.m_textureIndex)

				// Forced components all set up in the meta data
				PED_COMP_NAME_ENUM eFlipperFeet = GET_PED_COMP_ITEM_FROM_VARIATIONS(PLAYER_PED_ID(), componentItem.m_drawableIndex, componentItem.m_textureIndex, COMP_TYPE_FEET)
				IF eFlipperFeet != DUMMY_PED_COMP
				      SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_FEET, eFlipperFeet, FALSE) 
				ENDIF
				CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)

				IF ePedModel = MP_M_FREEMODE_01
				      SET_PLAYER_PREVIOUS_VARIATION_DATA(PLAYER_ID(), PED_COMP_SPECIAL, 15, 0, 0, 0)
				ELIF ePedModel = MP_F_FREEMODE_01
				      SET_PLAYER_PREVIOUS_VARIATION_DATA(PLAYER_ID(), PED_COMP_SPECIAL, 3, 0, 0, 0)
				ENDIF

				SET_ENABLE_SCUBA(PLAYER_PED_ID(), TRUE)
				
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableTakeOffScubaGear, TRUE)
				
				SET_BIT(iLocalBoolCheck8, LBOOL8_MANUAL_PLAYER_EXIT_STATE_DONE)
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MP_2")
				PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE subj_mcs1 - can set exit state - MP_2")
				// Remove the parachute
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 0, 0)


				// Flippers
				scrShopPedComponent componentItem
				MODEL_NAMES ePedModel = GET_ENTITY_MODEL(PLAYER_PED_ID())
				IF ePedModel = MP_M_FREEMODE_01
				      GET_SHOP_PED_COMPONENT(HASH("DLC_MP_X17_M_FEET_0_0"), componentItem)
				ELIF ePedModel = MP_F_FREEMODE_01
				      GET_SHOP_PED_COMPONENT(HASH("DLC_MP_X17_F_FEET_0_0"), componentItem)
				ENDIF
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_FEET, componentItem.m_drawableIndex, componentItem.m_textureIndex)

				// Forced components all set up in the meta data
				PED_COMP_NAME_ENUM eFlipperFeet = GET_PED_COMP_ITEM_FROM_VARIATIONS(PLAYER_PED_ID(), componentItem.m_drawableIndex, componentItem.m_textureIndex, COMP_TYPE_FEET)
				IF eFlipperFeet != DUMMY_PED_COMP
				      SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_FEET, eFlipperFeet, FALSE) 
				ENDIF
				CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)

				IF ePedModel = MP_M_FREEMODE_01
				      SET_PLAYER_PREVIOUS_VARIATION_DATA(PLAYER_ID(), PED_COMP_SPECIAL, 15, 0, 0, 0)
				ELIF ePedModel = MP_F_FREEMODE_01
				      SET_PLAYER_PREVIOUS_VARIATION_DATA(PLAYER_ID(), PED_COMP_SPECIAL, 3, 0, 0, 0)
				ENDIF

				SET_ENABLE_SCUBA(PLAYER_PED_ID(), TRUE)
				
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableTakeOffScubaGear, TRUE)
				
				SET_BIT(iLocalBoolCheck8, LBOOL8_MANUAL_PLAYER_EXIT_STATE_DONE)

			ENDIF
			
			
		ENDIF
		 // FEATURE_GANG_OPS
		
		//************************************DEFAULT EXIT STATE CHECK FOR PLAYERS****************************
		IF UPDATE_ON_PLAYER_EXIT_STATE(iCutsceneNum, iCutsceneTeam, sMocapName, vEndPos, fEndHeading)
			//Clear manual exit state bit
			IF IS_BIT_SET(iLocalBoolCheck8, LBOOL8_MANUAL_PLAYER_EXIT_STATE_DONE)
				CLEAR_BIT(iLocalBoolCheck8, LBOOL8_MANUAL_PLAYER_EXIT_STATE_DONE)
			ENDIF
			
			//Set the bit that a player exit has been carried out
			IF NOT IS_BIT_SET(iLocalBoolCheck8, LBOOL8_PLAYER_EXIT_STATE_CALLED)
				PRINTLN("[RCC MISSION] SETTING LBOOL8_PLAYER_EXIT_STATE_CALLED")
				SET_BIT(iLocalBoolCheck8, LBOOL8_PLAYER_EXIT_STATE_CALLED)
			ENDIF
		ENDIF
	
ENDPROC

PROC FREEZE_TOD_FOR_CUTSCENE()

	IF (iTODh = 0 and iTODm = 0 and  iTODs = 0)
		iTODh = GET_CLOCK_HOURS()
		iTODm = GET_CLOCK_MINUTES()
		iTODs = GET_CLOCK_SECONDS()
		PRINTLN("[RCC MISSION] 2 getting network TOD hours: ",iTODh," mins: ",iTODm," secs: ",iTODs)
		NETWORK_OVERRIDE_CLOCK_TIME(iTODh,iTODm,iTODs)
		SET_BIT(iLocalBoolCheck7, LBOOL7_BLOCK_TOD_EVERY_FRAME)
	ELSE
		NETWORK_OVERRIDE_CLOCK_TIME(iTODh,iTODm,iTODs)
	ENDIF

ENDPROC

PROC CHECK_CUTSCENE_FOR_DROPOFF_OBJECTS( STRING strCutsceneName )
	IF ARE_STRINGS_EQUAL( strCutsceneName, "MPH_PRI_STA_EXT" ) // Dropoff timetable
		SET_BIT( g_iApartmentDropoffObjectsBitset, ciAPARTMENT_DROPOFF_OBJECT_TIMETABLE )
		CPRINTLN( DEBUG_CONTROLLER, "[RCC mission] Dropped off a timetable" )
	ELIF ARE_STRINGS_EQUAL( strCutsceneName, "MPH_PRI_UNF_EXT" ) // Dropoff keycodes
		SET_BIT( g_iApartmentDropoffObjectsBitset, ciAPARTMENT_DROPOFF_OBJECT_KEYCODES )
		CPRINTLN( DEBUG_CONTROLLER, "[RCC mission] Dropped off the keycodes" )
	ENDIF
ENDPROC

STRUCT sExcludedCutsceneEntities
	INT iType
	INT iIndex
	//ENTITY_INDEX ent
ENDSTRUCT

CONST_INT ciMAX_EXCLUDED_CUTSCENE_ENTITIES 5

sExcludedCutsceneEntities excludedEntities[ ciMAX_EXCLUDED_CUTSCENE_ENTITIES ]


//Handle clearing ambient peds, vehicles and moving script vehicles
PROC CLEAR_CUTSCENE_AREA(FLOAT fRadius,VECTOR vClearArea,VECTOR vNewPosCentre, BOOL bClearVehicles = TRUE)
	//Check if clear area is set
	IF fRadius <= 0
		EXIT
	ENDIF
	
	INT iTemp
	VECTOR vCurrentPos
	
	CLEAR_AREA(vClearArea,fRadius,TRUE,FALSE,FALSE,FALSE)	//Clear ambient peds and vehicles
	CLEAR_AREA_LEAVE_VEHICLE_HEALTH(vClearArea, fRadius, FALSE, FALSE, TRUE, FALSE) // make sure we delete wrecked and abandoned vehicles.
	
	INT iNumExcludedEntities
	
	INT iCutsceneToUse
	INT iCutsceneEntity
	IF g_iFMMCScriptedCutscenePlaying> -1
		IF g_iFMMCScriptedCutscenePlaying >= FMMC_GET_MAX_SCRIPTED_CUTSCENES()
		AND g_iFMMCScriptedCutscenePlaying < FMMC_GET_MAX_CUTSCENES_WITH_MOCAP()
			iCutsceneToUse = (g_iFMMCScriptedCutscenePlaying - FMMC_GET_MAX_SCRIPTED_CUTSCENES()) 
		ELSE
			iCutsceneToUse = g_iFMMCScriptedCutscenePlaying
		ENDIF
		
		
		REPEAT FMMC_MAX_CUTSCENE_ENTITIES iCutsceneEntity
			INT iType = g_fmmc_struct.sScriptedCutsceneData[ iCutsceneToUse ].sCutsceneEntities[ iCutsceneEntity ].iType
			INT iIndex = g_fmmc_struct.sScriptedCutsceneData[ iCutsceneToUse ].sCutsceneEntities[ iCutsceneEntity ].iIndex
			
			IF iType = -1
				BREAKLOOP
			ENDIF
			
			SWITCH( iType )
				CASE CREATION_TYPE_VEHICLES
					excludedEntities[iNumExcludedEntities].iType = iType
					excludedEntities[iNumExcludedEntities].iIndex = iIndex
					//excludedEntities[iNumExcludedEntities].ent = ent
					
					iNumExcludedEntities++
					
					IF iNumExcludedEntities >= ciMAX_EXCLUDED_CUTSCENE_ENTITIES
						BREAKLOOP
					ENDIF
				BREAK
				
				DEFAULT
				BREAK
			ENDSWITCH
			
		ENDREPEAT
	ENDIF

	//Relocate script vehicles
	IF NOT IS_VECTOR_ZERO( vNewPosCentre )
		//Relocate script vehicles
		IF bClearVehicles
			REPEAT FMMC_MAX_VEHICLES iTemp
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iTemp])
					VEHICLE_INDEX currentVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iTemp])
					
					BOOL bSkip = FALSE
					
					//NOTE(Owain): we're going to loop through all the vehicles registered for this cutscene
					REPEAT iNumExcludedEntities iCutsceneEntity
						IF excludedEntities[ iCutsceneEntity ].iType = CREATION_TYPE_VEHICLES
						AND excludedEntities[ iCutsceneEntity ].iIndex = iTemp
							bSkip = TRUE
							BREAKLOOP // Break out of this loop
						ENDIF
					ENDREPEAT
					
					IF bSkip 
						RELOOP // skip to the next vehicle
					ENDIF
					
					IF DOES_ENTITY_EXIST(currentVeh)
					AND NETWORK_HAS_CONTROL_OF_ENTITY(currentVeh)
						vCurrentPos = GET_ENTITY_COORDS(currentVeh, FALSE)
						
						IF GET_DISTANCE_BETWEEN_COORDS(vCurrentPos,vClearArea) <= fRadius
						
							vCurrentPos.x = vCurrentPos.x - vClearArea.x
							vCurrentPos.y = vCurrentPos.y - vClearArea.y
							
							//Move vehicle to inside safe zone, relative to its previous position
							SET_ENTITY_COORDS(currentVeh,<<vCurrentPos.x + vNewPosCentre.x, vCurrentPos.y + vNewPosCentre.y,vNewPosCentre.z>>, FALSE)
						
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ELSE
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Clear cutscene area new position is zero - not relocating any entities." )
	ENDIF

	
	// cleanup my personal vehicle if inside area
	IF DOES_ENTITY_EXIST(MPGlobals.RemotePV[NATIVE_TO_INT(PLAYER_ID())])
		IF NOT IS_ENTITY_DEAD(MPGlobals.RemotePV[NATIVE_TO_INT(PLAYER_ID())])
			vCurrentPos = GET_ENTITY_COORDS(MPGlobals.RemotePV[NATIVE_TO_INT(PLAYER_ID())])
			IF GET_DISTANCE_BETWEEN_COORDS(vCurrentPos,vClearArea) <= fRadius
				CLEANUP_MP_SAVED_VEHICLE(TRUE, FALSE, TRUE, TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	// cleanup any old pv nearby
	DELETE_ANY_OLD_PV_NEARBY()

ENDPROC

proc setup_assests_for_end_of_cutscene()

	//prison_break_plane obtained from REGISTER_CUTSCENE_ENTITIES()
	if does_entity_exist(prison_break_plane)
	
		PRINTLN("[RCC MISSION] landing gear test 1")
	
		if is_vehicle_driveable(prison_break_plane)
	
			CONTROL_LANDING_GEAR(prison_break_plane, lgc_deploy_instant)
			
			PRINTLN("[RCC MISSION] landing gear test 2 ")
			
		else 
		
			PRINTLN("[RCC MISSION] landing gear test 3 ")
		
		endif 
		
	else 
	
		PRINTLN("[RCC MISSION] landing gear test 4 ")
		
	endif 
	
	PRINTLN("[RCC MISSION] landing gear test 5 ")

endproc

//List of cutscene co-ords for remote player
PROC GET_REMOTE_PLAYER_CUTSCENE_COORDS(STRING sMocapName, INT iplayer, VECTOR &vPos, FLOAT &fHeading, INT iCutscene, INT iPlayerPart)
	
	IF ARE_STRINGS_EQUAL( sMocapName, "MPH_PAC_FIN_MCS0" ) //Entering Bank
		SWITCH iplayer
			CASE 0
				vPos = <<233.8224, 218.4195, 106.2934>>
				fHeading = -47.52
			BREAK
			
			CASE 1
				vPos = <<235.1891, 218.6098, 106.2709>>
				fHeading = -43.06
			BREAK	
			CASE 2
				vPos = <<235.8487, 216.0541, 106.2908>>
				fHeading = -160.69
			BREAK
			CASE 3
				vPos = <<235.1123, 215.3171, 106.2831>>
				fHeading = -141.69
			BREAK
		ENDSWITCH
	ELIF ARE_STRINGS_EQUAL( sMocapName, "MPH_PAC_HAC_MCS1" )
		IF iPlayerPart >= 0
		AND iPlayerPart < FMMC_MAX_TEAMS
			vPos = g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].vPlayerEndPos[iPlayerPart]
			fHeading = g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].fPlayerEndHeading[iPlayerPart]
			
			IF iPlayerPart = 1
				vPos.Z = 104.7735
			ELIF iPlayerPart = 2
				vPos.Z = 104.7570
			ELIF iPlayerPart = 3
				vPos.Z = 104.7816
			ENDIF
		ENDIF
	ELIF ARE_STRINGS_EQUAL( sMocapName, "MPH_HUM_FIN_MCS1" )
		SWITCH iplayer
			CASE 0
				vPos = <<3536.0027, 3659.7473, 27.1219>>
				fHeading = 0
			BREAK
			
			CASE 1
				vPos = <<3533.7163, 3664.5588, 27.1219>>
				fHeading = 0
			BREAK
		ENDSWITCH
	ELIF ARE_STRINGS_EQUAL( sMocapName, "mph_pri_sta_mcs2" )
		IF iPlayerPart >= 0
		AND iPlayerPart < FMMC_MAX_TEAMS
			vPos = g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].vPlayerEndPos[iPlayerPart]
			fHeading = g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].fPlayerEndHeading[iPlayerPart]
			
			vPos.Z += 1.0014
		ENDIF
	ENDIF
ENDPROC

//List of cutscenes requiring override
FUNC BOOL SHOULD_CUTSCENE_OVERRIDE_REMOTE_PLAYERS(STRING sMocapName)
	IF ARE_STRINGS_EQUAL( sMocapName, "MPH_PAC_FIN_MCS0" )
	OR ARE_STRINGS_EQUAL( sMocapName, "MPH_PRI_STA_EXT")
	OR ARE_STRINGS_EQUAL( sMocapName, "MPH_PAC_HAC_MCS1" )	// url:bugstar:2214705
	OR ARE_STRINGS_EQUAL( sMocapName, "MPH_HUM_FIN_MCS1" )	// url:bugstar:2214705
	OR ARE_STRINGS_EQUAL( sMocapName, "mph_pri_sta_mcs2" )	// url:bugstar:2227335
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC OVERRIDE_REMOTE_PLAYERS_COORDINATES(STRING sMocapName, INT iCutscene)

	PED_INDEX tempPlayerIndex
	PLAYER_INDEX tempPlayerID
	VECTOR vPos
	FLOAT fHeading
	INT iPosCount = 0
	INT icount
	
	PRINTLN("[RCC MISSION] INSIDE OVERRIDE_REMOTE_PLAYERS_COORDINATES!")

	IF GET_CUTSCENE_TEAM() > -1 //lets not fade in SCTV
	AND SHOULD_CUTSCENE_OVERRIDE_REMOTE_PLAYERS(sMocapName)	
		
		IF NOT IS_THIS_MID_MISSION_MOCAP_IN_AN_APARTMENT( sMocapname )
		
			PRINTLN("[RCC MISSION] OVERRIDE_REMOTE_PLAYERS_COORDINATES NOT IN APARTMENT")
			
			INT iPlayer
			FOR iPlayer = 0 TO 3
				IF MC_serverBD.piCutscenePlayerIDs[GET_CUTSCENE_TEAM()][iplayer] != INVALID_PLAYER_INDEX()
					IF IS_NET_PLAYER_OK(MC_serverBD.piCutscenePlayerIDs[GET_CUTSCENE_TEAM()][iplayer])
						IF LocalPlayer <> NETWORK_GET_PLAYER_INDEX(NETWORK_GET_PARTICIPANT_INDEX(MC_serverBD.piCutscenePlayerIDs[GET_CUTSCENE_TEAM()][iplayer]))
							
							tempPlayerIndex = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_PARTICIPANT_INDEX(MC_serverBD.piCutscenePlayerIDs[GET_CUTSCENE_TEAM()][iplayer])))
							GET_ENTITY_COORDS(tempPlayerIndex)
							
							vPos = <<0,0,0>>
							fHeading = 0.0
							
							tempPlayerID = NETWORK_GET_PLAYER_INDEX(NETWORK_GET_PARTICIPANT_INDEX(MC_serverBD.piCutscenePlayerIDs[GET_CUTSCENE_TEAM()][iplayer]))
							
							GET_REMOTE_PLAYER_CUTSCENE_COORDS(sMocapName, iPlayer, vPos, fHeading, iCutscene, NATIVE_TO_INT(tempPlayerID))
							
							IF NOT ARE_VECTORS_EQUAL(vPos, <<0,0,0>>)
								NETWORK_OVERRIDE_COORDS_AND_HEADING(tempPlayerIndex, vPos, fHeading)
								PRINTLN("[RCC MISSION] NETWORK_OVERRIDE_COORDS_AND_HEADING for iPlayer ", iPlayer, " vPos is <<", vPos.X, ", ", vPos.Y,", ", vPos.Z,">>")
							ELSE
								PRINTLN("[RCC MISSION] NETWORK_OVERRIDE_COORDS_AND_HEADING vPos is <<0,0,0>>")
							ENDIF	
							
							iPosCount++
							
							PRINTLN("[ROSSW] NETWORK_OVERRIDE_COORDS_AND_HEADING ", iPlayer)
						ELSE
							PRINTLN("[RCC MISSION] IS THE LOCAL PLAYER NOT USING NETWORK_OVERRIDE_COORDS_AND_HEADING ")
							iPosCount++
						ENDIF
					ELSE
						PRINTLN("[ROSSW] cant set Player ", iPlayer, " to fade in they are invalid!")
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION] PLAYER DOESNT EXIST FOR FADE")
				ENDIF
			ENDFOR
			
		ELSE //Check for apartment players
			
			MP_PROP_OFFSET_STRUCT offsetStruct
			
			PRINTLN("[RCC MISSION] OVERRIDE_REMOTE_PLAYERS_COORDINATES IN APARTMENT")
			
			REPEAT NETWORK_GET_NUM_PARTICIPANTS() icount
				IF NETWORK_IS_PARTICIPANT_ACTIVE( INT_TO_PARTICIPANTINDEX(icount) )
					IF NOT IS_PARTICIPANT_A_SPECTATOR(icount)
						
						PRINTLN("[RCC MISSION] HAVE PARTICIPANT THAT ISN'T SPECTATING ", icount)
						PARTICIPANT_INDEX temppart = INT_TO_PARTICIPANTINDEX(icount)
						PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(temppart)
					
						IF NETWORK_IS_PLAYER_ACTIVE(tempPlayer)
						AND GlobalplayerBD_FM[NATIVE_TO_INT(tempPlayer)].propertyDetails.iCurrentlyInsideProperty > 0
							PRINTLN("[RCC MISSION] PARTICIPANT IS IN A PROPERTY")
							IF SHOULD_SPECIFIC_PLAYER_WATCH_TEAM_CUTSCENE(tempPlayer, GET_CUTSCENE_TEAM(), icount, iCutscene, TRUE)
								PRINTLN("[RCC MISSION] PARTICIPANT PLAYER IS WATCHING CUTSCENE")
								
								IF localPlayer != tempPlayer
									PED_INDEX tempPPed = GET_PLAYER_PED(tempPlayer)
									
									GET_APARTMENT_HEIST_INTERIOR_CUTSCENE_END_COORDS(offsetStruct, GlobalplayerBD_FM[NATIVE_TO_INT(tempPlayer)].propertyDetails.iCurrentlyInsideProperty, iPosCount)
									IF NOT IS_PED_DEAD_OR_DYING(tempPPed)
										//Adding 1.0 to correct height for apartment manually
										NETWORK_OVERRIDE_COORDS_AND_HEADING(tempPPed, <<offsetStruct.vLoc.X, offsetStruct.vLoc.Y, offsetStruct.vLoc.Z + 1.0>> , offsetStruct.vRot.z)
									ENDIF
									
									iPosCount++
									
									PRINTLN("[RCC MISSION] iPosCount ", iPosCount)
									PRINTLN("[RCC MISSION] NETWORK_OVERRIDE_COORDS_AND_HEADING Pos <<", offsetStruct.vLoc.X, ", ", offsetStruct.vLoc.Y, ", ", offsetStruct.vLoc.Z  + 1.0, ">>")
								ELSE
									iPosCount++
									PRINTLN("[RCC MISSION] IS THE LOCAL PLAYER NOT USING NETWORK_OVERRIDE_COORDS_AND_HEADING ")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	
	ENDIF
ENDPROC


//Used to list premocap cinematics for end cutscenes
FUNC BOOL DOES_MOCAP_HAVE_PRE_SPLASH(STRING strMocap)
	
	//LIST OF END CUTSCENES WE DON'T WANT THE PRE SPLASH STUFF
	IF NOT IS_STRING_NULL_OR_EMPTY(strMocap)
		IF ARE_STRINGS_EQUAL( strMocap, "MPH_HUM_KEY_MCS1" )
		OR ARE_STRINGS_EQUAL( strMocap, "MPH_PAC_FIN_MCS1" )
		OR ARE_STRINGS_EQUAL( strMocap, "MPH_TUT_MID" )
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT AM_I_ON_A_HEIST() //Don't want presplash for non-heist missions
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
		RETURN FALSE
	ENDIF
	
	//Don't need pre splash for strand mission
	IF IS_A_STRAND_MISSION_BEING_INITIALISED()
		RETURN FALSE
	ENDIF
	
	IF IS_THIS_MOCAP_IN_AN_APARTMENT( strMocap )
		RETURN FALSE
	ENDIF
	
	//b*2212802
	IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_JUST_DONE_DROPOFF_POSTFX)
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC STRING GET_POST_FX_STRING_FOR_SPLASH(STRING strMocap)
	UNUSED_PARAMETER( strMocap ) // TODO(owain): remove this param
	
	RETURN "HeistLocate" //Changed to "HeistLocate" for all b*2212622
	
ENDFUNC

FUNC BOOL SHOULD_TURN_OFF_CONTROL_AT_END_OF_MISSION( STRING strMocap = NULL )

	IF MC_serverBD.iNextMission < FMMC_MAX_STRAND_MISSIONS
	AND MC_serverBD.iNextMission >= 0
	AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl23NextContentID[MC_serverBD.iNextMission])
		IF g_FMMC_STRUCT.eBranchingTransitionType[MC_serverBD.iNextMission] = FMMC_BRANCHING_MISSION_TRANSITION_TYPE_AIRLOCK
			RETURN FALSE
		ENDIF
	ENDIF

	// NOTE(Owain): We still want to remove control if we fail during these missions 
	// B* 2239979
	IF HAS_TEAM_PASSED_MISSION( MC_playerBD[iLocalPart].iTeam )
		//Check if string is provided
		
		IF NOT IS_STRING_NULL_OR_EMPTY(strMocap)
			IF NOT ARE_STRINGS_EQUAL( strMocap, "MPH_PAC_FIN_MCS1" )
			AND NOT ARE_STRINGS_EQUAL( strMocap, "MPH_HUM_DEL_EXT" )
			AND NOT ARE_STRINGS_EQUAL( strMocap, "MPH_PRI_FIN_EXT" )
			AND NOT ARE_STRINGS_EQUAL( strMocap, "MPH_HUM_KEY_MCS1" ) //Note(Owain): B* 2237944
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_PRI_FIN_EXT
		OR MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_HUM_DEL_EXT
		OR MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_PAC_FIN_EXT
		OR MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_HUM_KEY_MCS1 //Note(Owain): B* 2237944
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

//Start the pre mocap cinematic cam
PROC START_PRE_MOCAP_SPLASH(STRING strMocap, BOOL bEndOfMission)

	IF NOT IS_BIT_SET(iLocalBoolCheck9, LBOOL9_PRE_MOCAP_CINE_TRIGGERED)
		REINIT_NET_TIMER(mocapPreCinematic)
		SET_BIT(iLocalBoolCheck9, LBOOL9_PRE_MOCAP_CINE_TRIGGERED)
		
		//Want to leave control on for b*2196035
		IF SHOULD_TURN_OFF_CONTROL_AT_END_OF_MISSION(strMocap)
			NET_SET_PLAYER_CONTROL(localPlayer, FALSE)
		ENDIF
		
		PRINTLN("[RCC MISSION] PRE_MOCAP_CINEMATIC - STARTING PRE MOCAP CINEMATIC")
		IF bEndOfMission
			ANIMPOSTFX_PLAY(GET_POST_FX_STRING_FOR_SPLASH(strMocap), 0, FALSE)
			PLAY_SOUND_FRONTEND(-1,  "Mission_Pass_Notify",  "DLC_HEISTS_GENERAL_FRONTEND_SOUNDS", FALSE)
		ENDIF
	ENDIF
ENDPROC

//Stop the pre mocap cinematic cam
PROC STOP_PRE_MOCAP_SPLASH(STRING sMocapName)

	//Check for pre mocap cinematic camera
	IF IS_BIT_SET(iLocalBoolCheck9, LBOOL9_PRE_MOCAP_CINE_TRIGGERED)
		STRING postFXname = GET_POST_FX_STRING_FOR_SPLASH(sMocapName)
	
		RESET_NET_TIMER(mocapPreCinematic)
		IF ANIMPOSTFX_IS_RUNNING(postFXname)
			ANIMPOSTFX_STOP(postFXname)
			PRINTLN("[RCC MISSION] POST FX STOPPED ", postFXname)
		ENDIF
		//ANIMPOSTFX_STOP_ALL()
		PRINTLN("[RCC MISSION] PRE_MOCAP_CINEMATIC - CINEMATIC CAMERA ENDED")
	ENDIF
ENDPROC

/// PURPOSE: makes all casco team players visible when they are sitting in the casco veh. Noramally during the 
///    		 playing of the cutscene only the local player is visible + clones. Then once the cutscene is finished 
///    		 that player becomes visible to all other machines. In the event machine 0 finishes before machine 1 
///    		 then machine 1 player will be invisible as they will still be playing their cutscne. This command 
///    		 forces that participant to be visible. This is called every frame once the cutscene is finished.
///    		 We need this to ensure both players render when we cutscene to the casco scripted cam and we 
///    		 don't get a pop showing machine 1 participant becoming visible when the cutscene finsihes. 
proc make_all_casco_players_visible_locally(string mocap_name)

	int i 
	participant_index particapant_ped 
	player_index mp_player_index
	ped_index mp_player_ped_index 
	vehicle_index casco_veh

	IF ARE_STRINGS_EQUAL(mocap_name, "MPH_PRI_STA_MCS1")

		PRINTLN("[RCC MISSION] make_all_casco_players_visible_locally - PH_PRI_STA_MCS1")
		
		for i = 0 to (max_num_mc_players - 1)
		
			//if MC_playerBD[i].iteam = 1
			if MC_playerBD[iPartToUse].iteam = MC_playerBD[i].iteam 
			
				particapant_ped = INT_TO_PARTICIPANTINDEX(i)

				mp_player_index = NETWORK_GET_PLAYER_INDEX(particapant_ped)

				mp_player_ped_index = get_player_ped(mp_player_index)
				
				casco_veh = net_to_veh(MC_serverBD_1.sFMMC_SBD.niVehicle[MC_serverBD_1.sFMMC_SBD.casco_index])

				if network_is_participant_active(particapant_ped)
				
					PRINTLN("[RCC MISSION] make_all_casco_players_visible_locally - network_is_participant_active")

					if not is_ped_injured(mp_player_ped_index)
					
						if is_ped_sitting_in_vehicle(mp_player_ped_index, casco_veh)
					
							set_entity_locally_visible(mp_player_ped_index)
						
							PRINTLN("[RCC MISSION] make_all_casco_players_visible_locally - set_entity_locally_visible")
							
						endif 
						
					endif 
				endif 
			endif 
			
		endfor 

	endif
	
endproc

/// PURPOSE: returns true if the player was registered for the cutscene by the server. 
FUNC BOOL IS_PLAYER_REGISTERED_FOR_CUTSCENE(INT iCutsceneTeam)
					
	INT iPlayer 
	FOR iPlayer = 0 to FMMC_MAX_CUTSCENE_PLAYERS - 1
	
		IF MC_ServerBD.piCutscenePlayerIDs[iCutsceneTeam][iPlayer] = PLAYER_ID()
			RETURN TRUE
		ENDIF
		
	ENDFOR 

	RETURN FALSE
ENDFUNC


PROC KILL_ALARM_SOUNDS()
	INT iAlarms
	FOR iAlarms = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfProps-1)
	    IF isoundid[iAlarms] <> -1
			PRINTLN("[RCC MISSION] [ALARM] - KILL_ALARM_SOUNDS STOP_SOUND(isoundid[", iAlarms, "]")
            STOP_SOUND(isoundid[iAlarms])
        ENDIF
	ENDFOR
ENDPROC

PROC KILL_FLEECA_ALARMS()
	INT iAlarms
	FOR iAlarms = 0 TO (ciCC_MaxAlarms -1)
	    IF sCCLocalData.iSFXAlarms[iAlarms] <> -1
			PRINTLN("[RCC MISSION] [ALARM] - KILL_ALARM_SOUNDS STOP_SOUND(sCCLocalData.iSFXAlarms[", iAlarms, "]")
            STOP_SOUND(sCCLocalData.iSFXAlarms[iAlarms])
		ELSE
			PRINTLN("[RCC MISSION] [ALARM] - sCCLocalData.iSFXAlarms[",iAlarms,"] = ", 0)
        ENDIF
	ENDFOR	
ENDPROC

SCRIPT_TIMER stGracePeriodAfterCutscene // Used to stop people getting shot just after a cutscene
CONST_INT	ciGRACE_PERIOD_AFTER_CUTSCENE 4 * 1000

PROC STOP_LOCAL_PLAYER_FROM_GETTING_SHOT_THIS_FRAME()
	SET_PED_RESET_FLAG( LocalPlayerPed, PRF_CannotBeTargetedByAI, TRUE )
ENDPROC

PROC CUTSCENE_INTERIOR_INIT_SPECIAL_CASE(STRING mocapName)
	
	IF ARE_STRINGS_EQUAL(mocapName, "MPCAS5_EXT")
		PRINTLN("[RCC MISSION][CUTSCENE_INIT_SPECIAL_CASE][RUN_MOCAP_CUTSCENE] - MPCAS5_EXT - casino_manager_workout")
		
		// Ensure interior index is valid
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO)
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE( structInteriorData.vPos, structInteriorData.sInteriorName )
		IF iInteriorIndex = NULL
			PRINTLN("[RCC MISSION] INTERIOR_V_CASINO_APARTMENT iInteriorIndex = NULL Set up failed")
		ENDIF
		
		IF IS_VALID_INTERIOR(iInteriorIndex)
			IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorIndex, "casino_manager_workout")
				ACTIVATE_INTERIOR_ENTITY_SET(iInteriorIndex, "casino_manager_workout")
				REFRESH_INTERIOR(iInteriorIndex)
				PRINTLN("[RCC MISSION][CUTSCENE_INIT_SPECIAL_CASE][RUN_MOCAP_CUTSCENE] - MPCAS5_EXT - Activating: 'casino_manager_workout'")
			ELSE
				PRINTLN("[RCC MISSION][CUTSCENE_INIT_SPECIAL_CASE][RUN_MOCAP_CUTSCENE] - MPCAS5_EXT - BAIL: Entity set 'casino_manager_workout' is already active")
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION][CUTSCENE_INIT_SPECIAL_CASE][RUN_MOCAP_CUTSCENE] - MPCAS5_EXT - BAIL: Interior is invalid")
		ENDIF
	ELIF ARE_STRINGS_EQUAL(mocapName, "HS3F_ALL_DRP1")
	OR ARE_STRINGS_EQUAL(mocapName, "HS3F_ALL_DRP2")
	OR ARE_STRINGS_EQUAL(mocapName, "HS3F_ALL_DRP3")
		IF NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_HIDE_HEIST_BAGS_AT_END_MISSION_CUTSCENE)
			PRINTLN("[RCC MISSION][CUTSCENE_INIT_SPECIAL_CASE][RUN_MOCAP_CUTSCENE] - HS3F_ALL_DRP1 - HS3F_ALL_DRP2 - HS3F_ALL_DRP3 - Broadcasting event to hide the heist bag for participant: ", iLocalPart)
			BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_HideHeistBagForPart)
			SET_BIT(iLocalBoolCheck33, LBOOL33_HIDE_HEIST_BAGS_AT_END_MISSION_CUTSCENE)
		ELSE
			PRINTLN("[RCC MISSION][CUTSCENE_INIT_SPECIAL_CASE][RUN_MOCAP_CUTSCENE] - HS3F_ALL_DRP1 - HS3F_ALL_DRP2 - HS3F_ALL_DRP3 - Heist bag already hidden for participant: ", iLocalPart)
		ENDIF
	ENDIF
	
ENDPROC

PROC CUTSCENE_INIT_SPECIAL_CASE( STRING mocapName, BOOL bCreateHeistDoors )
	IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		if ARE_STRINGS_EQUAL( mocapName, "mph_pri_sta_mcs1") //prison - station / casco
		OR ARE_STRINGS_EQUAL( mocapName, "MPH_PAC_WIT_MCS1") //Pacific Standard Boat Cutscene
		or are_strings_equal( mocapName, "mph_tut_ext") //fleeca finale
		OR ARE_STRINGS_EQUAL( mocapName, "MPH_HUM_FIN_EXT")
			CONCEAL_ALL_OTHER_PLAYERS(TRUE, true)
		else 
			CONCEAL_ALL_OTHER_PLAYERS(TRUE)	
		endif 
	ENDIF
	
	//stop the population budgets form updating. helps with streaming. bug 2217780
	//if bIsLocalPlayerHost
		if are_strings_equal( mocapName, "mph_tut_ext") 
			
			MC_CLEAR_PED_AND_TRAFFIC_DENSITIES(iPopulationHandle, bVehicleGenerator)
			MC_SET_PED_AND_TRAFFIC_DENSITIES(iPopulationHandle, bVehicleGenerator, 0, 0)
			
			println("[rcc mission] is_Cutscene_playing - MC_SET_PED_AND_TRAFFIC_DENSITIES")
		endif
	//endif
	
	//helps fix streaming issue on bridge 2239956
	if are_strings_equal( mocapName, "mph_tut_ext") 
		set_entity_coords(player_ped_id(), <<-2610.4485, 2941.6841, 15.6698>>)
		set_entity_heading(player_ped_id(), 348.7153)
		println("[rcc mission] is_Cutscene_playing - mph_tut_ext reposition player")
	endif 
	
	IF ARE_STRINGS_EQUAL( mocapName, "MPH_HUM_EMP_EXT")
		CLEAR_CUTSCENE_AREA(20, <<1729.3, 3320.3, 41.9>>, <<1754.1, 3292.5, 41.2>>, FALSE)
		REMOVE_PARTICLE_FX_IN_RANGE(<<1729.3, 3320.3, 41.9>>, 20)
		PRINTLN("[RCC MISSION] RUN_MOCAP_CUTSCENE() - End of mission cutscene CLEAR_AREA")
		CPRINTLN(DEBUG_MISSION, "Clearing area for MPH_HUM_EMP_EXT, <<1729.3, 3320.3, 41.9>>, 20.0m")
	ENDIF
		
	// url:bugstar:2168236
	IF bCreateHeistDoors
	AND DOES_ENTITY_EXIST(g_heistMocap.appartment_doors)
		SET_ENTITY_VISIBLE(g_heistMocap.appartment_doors, TRUE)
		CREATE_MODEL_HIDE_EXCLUDING_SCRIPT_OBJECTS(GET_ENTITY_COORDS(g_heistMocap.appartment_doors),10.0, GET_ENTITY_MODEL(g_heistMocap.appartment_doors), TRUE)
	ENDIF
	
	IF MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_TREVOR
		IPL_GROUP_SWAP_FINISH()
	endif 
	
	IF MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_SIMEON	
		hide_garage_door()
	ENDIF
ENDPROC

PROC CUTSCENE_RUNNING_SPECIAL_CASE( STRING mocapName, INT iCutsceneTeam, INT iCutsceneToUse )
	
	if ARE_STRINGS_EQUAL( mocapName, "mph_tut_ext")
		
		if not is_bit_set(ilocalboolcheck11, LBOOL11_fleeca_finale_warp_player_for_streaming)
			//-----
			if get_cutscene_time() > 6000
				set_game_pauses_for_streaming(false)
				clear_ped_tasks_immediately(player_ped_id())
				
				switch iPartToUse
					case 0
						set_entity_coords(player_ped_id(), <<718.0, -979.1, 23.00>>)
						set_entity_heading(player_ped_id(), 355.1782)
					break 
					
					case 1
						set_entity_coords(player_ped_id(), <<718.5, -979.1, 23.00>>)
						set_entity_heading(player_ped_id(), 355.1782)
					break 
					
					default
						set_entity_coords(player_ped_id(), <<718.0, -979.1, 23.00>>)
						set_entity_heading(player_ped_id(), 355.1782)
					break 
				endswitch 
				
				freeze_entity_position(player_ped_id(), true)
				force_ped_ai_and_animation_update(player_ped_id())
				PRINTLN("[RCC MISSION] PLACE_PLAYERS_IN_A_VEHICLE_AFTER_MID_MOCAP participant = ", iPartToUse)
			
				set_bit(ilocalboolcheck11, LBOOL11_fleeca_finale_warp_player_for_streaming)
			endif 

		else
			if not is_bit_set(ilocalboolcheck11, LBOOL11_fleeca_finale_end_mocap_fade_in)

				//------
				if get_cutscene_time() > 7500
					if not is_screen_faded_out()
						if not is_screen_fading_out()
							do_screen_fade_out(default_fade_time)
							
							PRINTLN("[RCC MISSION] LBOOL11_fleeca_finale_warp_player_for_streaming - do_screen_fade_out ")
						endif 
					endif 
				endif
				//-----
				if get_cutscene_time() > 16250 
					do_screen_fade_in(default_fade_time)
					set_bit(ilocalboolcheck11, LBOOL11_fleeca_finale_end_mocap_fade_in)
					PRINTLN("[RCC MISSION] LBOOL11_fleeca_finale_end_mocap_fade_in")
				endif 
			endif
		endif 
	endif 
	
	//Takes a frame to get assigned.
	IF iCutsceneTeam > -1
		if ARE_STRINGS_EQUAL( mocapName, "mph_pri_sta_mcs1") //prison - station / casco
			if NOT IS_BIT_SET(iLocalBoolCheck8, LBOOL8_local_player_warped_into_casco)
				if GET_CUTSCENE_TIME() >= 500
					if MC_serverBD_1.sFMMC_SBD.casco_index != -1

						vehicle_index casco_veh
							
						casco_veh = net_to_veh(MC_serverBD_1.sFMMC_SBD.niVehicle[MC_serverBD_1.sFMMC_SBD.casco_index])
							
						if does_entity_exist(casco_veh)
							if is_vehicle_driveable(casco_veh)
							
								clear_ped_tasks_immediately(player_ped_id()) //if the player is climbing then the task_enter_vehicle will not work. 
					
								INT iLocalPlayerCutID = GET_LOCAL_PLAYER_CUTSCENE_INDEX(iCutsceneTeam)
								IF iLocalPlayerCutID > -1
									CUTSCENE_WARP_PED_TO_VEHICLE_COMMON(LocalPlayerPed, g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sPlayerEndWarp[iLocalPlayerCutID])
								
									PRINTLN("[RCC MISSION] PLACE_PLAYERS_IN_A_VEHICLE_AFTER_MID_MOCAP participant = ", iPartToUse)
								
									set_bit(iLocalBoolCheck8, LBOOL8_local_player_warped_into_casco)
								ELSE
									PRINTLN("[RCC MISSION] PLACE_PLAYERS_IN_A_VEHICLE_AFTER_MID_MOCAP iLocalPlayerCutID = ", iLocalPlayerCutID, " something went wrong")
								ENDIF
							endif 
						endif 
					endif 
				endif 
			endif
		endif
	ENDIF
	
	// 2221384 - set trevor's RIP michael tattoo
	IF NOT IS_BIT_SET( iLocalBoolCheck11, LBOOL11_TREVOR_TATTOO_APPLIED )
		IF DOES_CUTSCENE_ENTITY_EXIST( "TREVOR" )
			IF ARE_STRINGS_EQUAL( mocapName, "mph_nar_fin_ext")
			OR ARE_STRINGS_EQUAL( mocapName, "MPH_FIN_CEL_TRE")
				PED_INDEX pedTemp
				pedTemp = GET_PED_INDEX_FROM_ENTITY_INDEX( GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY( "TREVOR", PLAYER_TWO ) )
				IF NOT IS_PED_INJURED( pedTemp )
					SETUP_PLAYER_PED_DEFAULTS_FOR_MP( pedTemp )
					PRINTLN("RUN_MOCAP_CUTSCENE - Set Trevor's tattoo on ped ", NATIVE_TO_INT( pedTemp ) )
					SET_BIT( iLocalBoolCheck11, LBOOL11_TREVOR_TATTOO_APPLIED )
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

//Do pre mocap checks prior to cutscene launching
PROC DO_PRE_MOCAP_CHECKS(INT iCutsceneToUse)
	
	CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] DOING PRE MOCAP CHECKS FOR ", sMocapCutscene)
	
	
	
	//Check for cutscene in hide mocap block list
	IF NOT DOES_MOCAP_CUTSCENE_NEED_PLAYERS(sMocapCutscene, iCutsceneToUse)
		//Block players from being cloned and passed into the cutscene
		SET_BIT(iLocalBoolCheck7, LBOOL7_HIDE_MOCAP_PLAYERS)
		PRINTLN("[RCC MISSION] HIDE MOCAP PLAYERS SET")
	ENDIF
	
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PRI_FIN_MCS2")
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)) = BUZZARD
				vi_PrisonFinaleBuzzard = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
				IF NETWORK_HAS_CONTROL_OF_ENTITY( vi_PrisonFinaleBuzzard )
					pi_PrisonFinaleBuzzardPilot = LocalPlayerPed
					FREEZE_ENTITY_POSITION(vi_PrisonFinaleBuzzard, TRUE)
					SET_ENTITY_VISIBLE_IN_CUTSCENE(vi_PrisonFinaleBuzzard, FALSE)
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] This is the plane cutscene and I'm in the buzzard. Freezing the buzzard so I can't move" )
				ENDIF
			ENDIF			
		ENDIF
	ENDIF
	
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PAC_WIT_MCS1")
		//Post cutscene Avi
		ENTITY_INDEX entLaundPostCut = NET_TO_ENT(GET_CUTSCENE_NET_ID_WITH_HANDLE("Money_Launderer", iCutsceneToUse))
		PED_INDEX pedLaundPostCut = GET_PED_INDEX_FROM_ENTITY_INDEX(entLaundPostCut)
		
		//Have player in control of Avi Take control of the boat
		IF NETWORK_HAS_CONTROL_OF_ENTITY(entLaundPostCut)
			SET_PED_FLEE_ATTRIBUTES(pedLaundPostCut, FA_NEVER_FLEE, TRUE)
		ENDIF
		
		INT iVehId = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sPlayerEndWarp[0].iVehID
		IF iVehId > -1
			VEHICLE_INDEX veh = NET_TO_VEH( MC_serverBD_1.sFMMC_SBD.niVehicle[ iVehId ] )

			IF NETWORK_HAS_CONTROL_OF_ENTITY(veh)
				//Check the vehicle doors aren't locked b*2150180
				IF GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(veh, localplayer)
					SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(veh, FALSE)
					PRINTLN("[RCC MISSION] VEHICLE DOOR WAS LOCKED SETTING TO UNLOCKED")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_PLAY_MOCAP()
		
	PRINTLN("[RCC MISSION] SHOULD_PLAY_MOCAP - [NETCELEBRATION] - MC_ServerBD.iEndCutscene: ", MC_ServerBD.iEndCutscene, " MC_serverBD.iEndCutsceneNum[ ", MC_playerBD[iPartToUse].iteam, " ]: ", MC_serverBD.iEndCutsceneNum[ MC_playerBD[iPartToUse].iteam ])
	
	#IF IS_DEBUG_BUILD
	IF MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_PLACE_HOLDER
		IF MC_serverBD.iNextMission < FMMC_MAX_STRAND_MISSIONS
		AND MC_serverBD.iNextMission >= 0
		AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl23NextContentID[MC_serverBD.iNextMission])
			IF g_FMMC_STRUCT.eBranchingTransitionType[MC_serverBD.iNextMission] = FMMC_BRANCHING_MISSION_TRANSITION_TYPE_FADEOUT
			OR g_FMMC_STRUCT.eBranchingTransitionType[MC_serverBD.iNextMission] = FMMC_BRANCHING_MISSION_TRANSITION_TYPE_AIRLOCK
				PRINTLN("[RCC MISSION] SHOULD_PLAY_MOCAP - [NETCELEBRATION] - Returning False due to FADEOUT or AIRLOCK transition being set. MC_serverBD.iNextMission: ", MC_serverBD.iNextMission)
				RETURN FALSE
			ENDIF
		ENDIF
		
		PRINTLN("[RCC MISSION] SHOULD_PLAY_MOCAP - [NETCELEBRATION] - Returning True (1)")
		
		RETURN TRUE
	ENDIF
	#ENDIF
	
	IF NOT IS_VECTOR_ZERO( GET_CUTSCENE_COORDS(-1) )
	OR AM_I_ON_A_HEIST() OR IS_LOADED_MISSION_LOW_RIDER_FLOW_MISSION()
		IF MC_serverBD.iEndCutsceneNum[ MC_playerBD[iPartToUse].iteam ] != -1
		AND ( GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed,GET_CUTSCENE_COORDS(-1)) < 100 
				OR AM_I_ON_A_HEIST() OR IS_LOADED_MISSION_LOW_RIDER_FLOW_MISSION() OR GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION() OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_PlayEndCutsceneForAllPlayers))
				
				PRINTLN("[RCC MISSION] SHOULD_PLAY_MOCAP - [NETCELEBRATION] - Returning True (2)")
			RETURN TRUE
		ENDIF
		IF MC_serverBD.iEndCutsceneNum[ MC_playerBD[iPartToUse].iteam ] != -1
			IF IS_PLAYER_SCTV(LocalPlayer)
				PRINTLN("[RCC MISSION] SHOULD_PLAY_MOCAP - [NETCELEBRATION] - Returning True (3)")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	PRINTLN("[RCC MISSION] SHOULD_PLAY_MOCAP - [NETCELEBRATION] - Returning False...")

	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_WALK_TO_MOCAP_COORDS()
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		RETURN FALSE
	ENDIF
	
	IF MC_serverBD_4.iInteractWith_BombPlantParticipant = iLocalPart
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

//Do post mocap checks to position objects etc...
PROC DO_POST_MOCAP_CHECKS()

	PRINTLN("[RCC MISSION] DOING POST MOCAP CHECKS FOR ", sMocapCutscene)

	//Pacific Standard Job Witsec
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PAC_WIT_MCS1")
	
		//Remove the hide for bushes
		REMOVE_MODEL_HIDE(<<-2166.4595, 5197.9614, 15.8854>>, 10.0, INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("PROP_BUSH_LRG_01")), TRUE)
		REMOVE_MODEL_HIDE(<<-2166.4595, 5197.9614, 15.8854>>, 10.0, INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("PROP_BUSH_LRG_02b")), TRUE)
		REMOVE_MODEL_HIDE(<<-2166.4595, 5197.9614, 15.8854>>, 10.0, INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("PROP_BUSH_IVY_01_1L")), TRUE)
		REMOVE_MODEL_HIDE(<<-2166.4595, 5197.9614, 15.8854>>, 10.0, INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("PROP_BUSH_IVY_01_1M")), TRUE)
		
		IF HAS_CLIP_SET_LOADED("clipset@veh@boat@predator@ps@base")
			REMOVE_CLIP_SET("clipset@veh@boat@predator@ps@base")
		ENDIF
		
	ENDIF
	
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PAC_FIN_MCS0" )
		CLEAR_BIT(iLocalBoolCheck11, LBOOL11_PAC_FIN_GIVEN_WEAPON)
	ENDIF
	

	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PRI_FIN_MCS2")
		//Clear the area to get rid of plane marked as no longer needed
		CLEAR_AREA(GET_ENTITY_COORDS(LocalPlayerPed), 10000.0, FALSE)

	ENDIF
	
ENDPROC

///purpose: applies forces to the casco vehicle to force it to land safely.
///    		The driver of the vehicle will always own the casco
///    		in the event the player does not own the casco then the function will return true.   		
func bool apply_force_to_caso_system(vehicle_index mission_veh)

	vector casco_pos

	PRINTLN("[RCC MISSION] apply_force_to_caso_system : ")
									
	if does_entity_exist(mission_veh)
		if is_vehicle_driveable(mission_veh)
			IF NETWORK_HAS_CONTROL_OF_entity(mission_veh)
				//safety backup incase the player decides to drive the casco off another part of the container.
				//we want the cutscene misison flow to progress 
				casco_pos = get_entity_coords(mission_veh)
				if casco_pos.z < 13.00
				
					PRINTLN("[RCC MISSION] apply_force_to_caso 0 : ")
				
					return true
				endif 

				switch apply_force_to_caso_system_status
					case 0
						if is_entity_in_angled_area(mission_veh, <<878.38, -2898.845, 15.346>>, <<864.98, -2898.745, 21.446>>, 5.5)
						
							apply_force_to_entity(mission_veh, apply_type_force, <<0.0, 0.0, 4.5>>, <<0.0, 1.2, 0.0>>, 0, true, true, true) //4.5
						
							apply_force_to_caso_system_status++
							
							PRINTLN("[RCC MISSION] apply_force_to_caso 1 : ")
						endif 
					break 
					
					case 1
						apply_force_to_entity(mission_veh, apply_type_force, <<0.0, 0.0, 4.5>>, <<0.0, 1.2, 0.0>>, 0, true, true, true) //4.5
						
						PRINTLN("[RCC MISSION] apply_force_to_caso 2 : ")
			
						if not is_entity_in_angled_area(mission_veh, <<878.38, -2898.845, 15.346>>, <<864.98, -2898.745, 21.446>>, 5.5)
							PRINTLN("[RCC MISSION] apply_force_to_caso 3 : ")
							apply_force_to_caso_system_status++
							return true 
						endif
					break 
					
					case 2
						return true
					break
				endswitch 
			else 
				//if you are not the driver then stop processing the casco force
				if (GET_PED_IN_VEHICLE_SEAT(casco_vehicle, VS_DRIVER) != LocalPlayerPed)
					PRINTLN("[RCC MISSION] speed 4 NOT DRIVER" )
					return true 
				endif 
			
				PRINTLN("[RCC MISSION] speed 4" )
			endif 
		else
			PRINTLN("[RCC MISSION] speed 5" )
		endif
	else
		PRINTLN("[RCC MISSION] speed 6" )
	endif
	
	return false
	
endfunc

PROC SET_JCS_STATE(JUGGERNAUT_CUTSCENE_STATE jcsNewState, INT iJuggernautIndex)
	jcs_CurCutsceneState[iJuggernautIndex] = jcsNewState
	PRINTLN("[JuggCutscene] Setting state to: ", jcsNewState, " for Juggernaut: ", iJuggernautIndex)
ENDPROC

PROC GET_JUGGERNAUT_CUTSCENE_REFERENCES()
	IF NOT DOES_ENTITY_EXIST(piCutsceneJuggs[0])
		piCutsceneJuggs[0] = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Juggernaut1")
	ENDIF
	IF NOT DOES_ENTITY_EXIST(piCutsceneJuggs[1])
		piCutsceneJuggs[1] = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Juggernaut2")
	ENDIF
	IF NOT DOES_ENTITY_EXIST(piCutsceneJuggs[2])
		piCutsceneJuggs[2] = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Juggernaut3")
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(piCutsceneJuggs_Guns[0])
		piCutsceneJuggs_Guns[0] = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Minigun1")
	ENDIF
	IF NOT DOES_ENTITY_EXIST(piCutsceneJuggs_Guns[1])
		piCutsceneJuggs_Guns[1] = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Minigun2")
	ENDIF
	IF NOT DOES_ENTITY_EXIST(piCutsceneJuggs_Guns[2])
		piCutsceneJuggs_Guns[2] = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Minigun3")
	ENDIF
ENDPROC

//SIL_PRED_MCS1
PROC PROCESS_JUGGERNAUT_INTRODUCTION_CUTSCENE()
	
	INT iJuggernautIndex
	
	//REFERENCES
	GET_JUGGERNAUT_CUTSCENE_REFERENCES()
	
	//JUGGERNAUT LOOP
	FOR iJuggernautIndex = 0 TO ci_JuggernautsInCutscene - 1
		
		IF NOT DOES_ENTITY_EXIST(piCutsceneJuggs[iJuggernautIndex])
			PRINTLN("[JuggCutscene] No reference to juggernaut ", iJuggernautIndex)
			EXIT // Don't have a reference to this ped yet
		ENDIF
		
		IF NOT IS_ENTITY_ALIVE(piCutsceneJuggs[iJuggernautIndex])
			PRINTLN("[JuggCutscene] Juggernaut is dead: ", iJuggernautIndex)
			EXIT // Don't have a reference to this ped yet
		ENDIF
		
		//EVENTS
		IF HAS_ANIM_EVENT_FIRED(piCutsceneJuggs[iJuggernautIndex], GET_HASH_KEY("On"))
			SET_JCS_STATE(JCS_FLICKER, iJuggernautIndex)
			
			#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
				DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(piCutsceneJuggs[iJuggernautIndex]), 1.3, 255, 0, 0, 150)
			ENDIF
			#ENDIF
		ENDIF
		
		IF HAS_ANIM_EVENT_FIRED(piCutsceneJuggs[iJuggernautIndex], GET_HASH_KEY("Off"))
			SET_JCS_STATE(JCS_NONE, iJuggernautIndex)
			SET_ENTITY_ALPHA(piCutsceneJuggs[iJuggernautIndex], 255, FALSE)
			SET_ENTITY_ALPHA(piCutsceneJuggs_Guns[iJuggernautIndex], 255, FALSE)
			
			#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
				DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(piCutsceneJuggs[iJuggernautIndex]), 1.3, 0, 0, 255, 150)
			ENDIF
			#ENDIF
		ENDIF
		
		IF HAS_ANIM_EVENT_FIRED(piCutsceneJuggs[iJuggernautIndex], GET_HASH_KEY("Active"))
			SET_JCS_STATE(JCS_FADEOUT, iJuggernautIndex)
			
			PRINTLN("Calling CLEAR_COVER_POINT_FOR_PED etc to get player ready for what happens after the cutscene")
			CLEAR_PED_TASKS(LocalPlayerPed)
			CLEAR_COVER_POINT_FOR_PED(LocalPlayerPed)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed)
		ENDIF
		
		INT iNewAlpha = 255
		
		//PROCESSING
		SWITCH jcs_CurCutsceneState[iJuggernautIndex]
			CASE JCS_NONE
				fCutsceneJugg_MaxOpacity[iJuggernautIndex] = 255 //Initialises the value to be used in the FADEOUT state
			BREAK
			
			CASE JCS_FLICKER
				//Lowers the opacity here until told to turn it back up again
				iNewAlpha = 100
				SET_ENTITY_ALPHA(piCutsceneJuggs[iJuggernautIndex], iNewAlpha, FALSE)
				SET_ENTITY_ALPHA(piCutsceneJuggs_Guns[iJuggernautIndex], iNewAlpha, FALSE)
			BREAK
			
			CASE JCS_FADEOUT
				//Gradually turn invisible for real
				iNewAlpha = GET_RANDOM_INT_IN_RANGE(ROUND(fCutsceneJugg_MaxOpacity[iJuggernautIndex] * 0.25), ROUND(fCutsceneJugg_MaxOpacity[iJuggernautIndex]))
				
				SET_ENTITY_ALPHA(piCutsceneJuggs[iJuggernautIndex], iNewAlpha, FALSE)
				SET_ENTITY_ALPHA(piCutsceneJuggs_Guns[iJuggernautIndex], iNewAlpha, FALSE)
				
				fCutsceneJugg_MaxOpacity[iJuggernautIndex] = fCutsceneJugg_MaxOpacity[iJuggernautIndex] -@ cf_CutsceneJugg_OpacityFadeSpeed
				
				IF fCutsceneJugg_MaxOpacity[iJuggernautIndex] <= 0
					fCutsceneJugg_MaxOpacity[iJuggernautIndex] = 0
				ENDIF
				
				#IF IS_DEBUG_BUILD
				IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
					DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(piCutsceneJuggs[iJuggernautIndex]), 1.3, 0, 255, 0, ROUND(fCutsceneJugg_MaxOpacity[iJuggernautIndex]))
				ENDIF
				#ENDIF
			BREAK
		ENDSWITCH
	ENDFOR
	
ENDPROC

func bool process_ingame_features_for_mph_pri_sta_mcs1()

	if not is_bit_set(iLocalBoolCheck8, LBOOL8_skipped_to_mid_mission_cutscene_stage_5)
		
		IF( IS_BIT_SET( iLocalBoolCheck7, LBOOL7_PRISON_BREAK_CARGO_SCENE_TRIGGERED ) )
		or ((are_strings_equal(sMocapCutscene, "mph_pri_sta_mcs1")) and (not is_bit_set(iLocalBoolCheck8, LBOOL8_forces_applied_to_casco_vehicle)))
		
			return true 
			
		endif 

	endif 
	
	return false
endfunc  

FUNC BOOL IS_MID_MISSION_CUTSCENE_IN_APARTMENT()
	IF g_bPropertyDropOff
	OR g_bGarageDropOff
	OR IS_BIT_SET( iLocalBoolCheck7, LBOOL7_SHOULD_HOLD_UP_MOCAP_FOR_CUTSCENE )
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC DO_HUMANE_DELIVER_EMP_SPECIAL_CASE_CHECKS( INT iCutsceneToUse )
	IF( g_FMMC_STRUCT.iRootContentIDHash = g_sMPTunables.iroot_id_HASH_Humane_Labs_Deliver_EMP )
		IF( NOT IS_BIT_SET( iLocalBoolCheck8, LBOOL8_HUMANE_DELIVER_EMP_PER_SHOT_FLAG ) )
			IF( iCamShot < g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].iNumCamShots )
				IF( iCamShot = 6 )
					CPRINTLN( DEBUG_SIMON, "[RCC MISSION] DO_HUMANE_DELIVER_EMP_SPECIAL_CASE_CHECKS - CLEAR_ROOM_FOR_GAME_VIEWPORT() CALLED FOR SHOT ", iCamShot )
					CLEAR_ROOM_FOR_GAME_VIEWPORT()
				ENDIF
				SET_FOCUS_POS_AND_VEL( g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].vCamStartPos[ iCamShot ], <<0.0, 0.0, 0.0>> )
				CPRINTLN( DEBUG_SIMON, "[RCC MISSION] DO_HUMANE_DELIVER_EMP_SPECIAL_CASE_CHECKS - SET_FOCUS_POS_AND_VEL() CALLED FOR SHOT ", iCamShot )
			ELSE
				CLEAR_FOCUS()
				CPRINTLN( DEBUG_SIMON, "[RCC MISSION] DO_HUMANE_DELIVER_EMP_SPECIAL_CASE_CHECKS - CLEAR_FOCUS() CALLED FOR SHOT ", iCamShot )
			ENDIF
			SET_BIT( iLocalBoolCheck8, LBOOL8_HUMANE_DELIVER_EMP_PER_SHOT_FLAG )
		ENDIF
	ENDIF
ENDPROC

//Unlock a specific vehicle before a cutscene
PROC UNLOCK_SPECIFIC_VEHICLE_BEFORE_CUTSCENE(INT iCutsceneNum)

	//Pacific Standard Signal - Boat Cutscene
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PAC_WIT_MCS1")
		INT iVehId = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneNum].sPlayerEndWarp[0].iVehID
		IF iVehId > -1
			VEHICLE_INDEX veh = NET_TO_VEH( MC_serverBD_1.sFMMC_SBD.niVehicle[ iVehId ] )
			IF NETWORK_HAS_CONTROL_OF_ENTITY(veh)
				IF GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(veh, localplayer)
					SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(veh, FALSE)
					PRINTLN("[RCC MISSION] VEHICLE DOOR WAS LOCKED SETTING TO UNLOCKED!")
				ELSE
					PRINTLN("[RCC MISSION] VEHICLE DOORS AREADY UNLOCKED!")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// Really rough check
FUNC BOOL IS_PLAYER_AT_ORNATE_BANK(BOOL bVaultDoor = FALSE,INT iThisCutscene = 0,INT iThisCamShot = 0)
	
	IF bVaultDoor
		IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].iCutsceneShotBitSet[iThisCamShot], ciCSS_BS_SecurityCamFilter)
			IF IS_ENTITY_AT_COORD(LocalPlayerPed,<<253.2825, 228.4422, 100.6832>>,<<10,10,5>>)
			OR g_FMMC_STRUCT.iRootContentIDHash = g_sMPTUNABLES.iroot_id_HASH_The_Pacific_Standard_Job 
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
	
		IF IS_ENTITY_AT_COORD(LocalPlayerPed,<<253.2825, 228.4422, 100.6832>>,<<10,10,5>>)
		OR g_FMMC_STRUCT.iRootContentIDHash = g_sMPTUNABLES.iroot_id_HASH_The_Pacific_Standard_Job 
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

///PURPOSE: This function returns true if we want to skip the ending cutscene if the player is too far.
FUNC BOOL SHOULD_END_CAMERA_MOCAP_BE_SKIPPED_IF_TOO_FAR()
	
	IF ARE_STRINGS_EQUAL( sMocapCutscene, "MPH_TUT_CAR_EXT" )
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

// Player respawning on missions that can have end cutscenes
// This stops the situation where we die as we start END_STAGE (where the team can't fail)
// but the end cutscene doesn't play and other bad things happen because we're dead
PROC RESPAWN_LOCAL_PLAYER_IF_DEAD_AND_GOING_INTO_END_CUTSCENE()
	IF AM_I_ON_A_HEIST()
	OR Is_Player_Currently_On_MP_Contact_Mission( Player_ID() )
		IF NOT g_bFailedMission
			IF MC_ServerBD.iEndCutscene > -1
			AND NOT IS_BIT_SET( iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE_WARP )
			
				IF NOT bPlayerToUseOK
				OR IS_PLAYER_RESPAWNING( PLAYER_ID() )
					IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_PLAYER_RESURRECTED_FOR_END_CUTSCENE)
						NETWORK_RESURRECT_LOCAL_PLAYER( GET_ENTITY_COORDS( PLAYER_PED_ID(), FALSE ),
														GET_ENTITY_HEADING( PLAYER_PED_ID() ),
														1000,
														FALSE,
														FALSE ) // We don't want to unpause the renderphases here
						CACHE_LOCAL_PLAYER_VARIABLES()
						SET_BIT( iLocalBoolCheck7, LBOOL7_TOGGLED_RENDERPHASES_FOR_DROPOFF ) // Say the renderphases are toggled, so the cutscene unpauses them
						SET_BIT(iLocalBoolCheck31, LBOOL31_PLAYER_RESURRECTED_FOR_END_CUTSCENE)
						CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Force respawning the player for the end mocap" )
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

///PURPOSE: This function warps the player to the vector returned by GET_CUTSCENE_COORDS if players are >100m away
FUNC BOOL WARP_TO_END_MOCAP()

	VECTOR vMocapWarpPos
	BOOL b3dCheck = TRUE
	
	RESPAWN_LOCAL_PLAYER_IF_DEAD_AND_GOING_INTO_END_CUTSCENE()
	
	// Wait for the player to respawn
	IF NOT bLocalPlayerOK
		RETURN FALSE
	ENDIF
	
	// Skip warping people to the apartment
	IF IS_BIT_SET( iLocalBoolCheck7, LBOOL7_SHOULD_HOLD_UP_MOCAP_FOR_CUTSCENE )
		RETURN TRUE
	ENDIF
	
	IF IS_CUTSCENE_PLAYING()
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET( iLocalBoolCheck2, LBOOL2_CUTSCENE_WARP_DONE )
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitset2, PBBOOL2_PLAYING_MOCAP_CUTSCENE)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerHeistwinnerCutscene)
		RETURN TRUE
	ENDIF
	
	INT iMyTeam = MC_playerBD[iPartToUse].iteam
	
	IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
	OR AM_I_ON_A_HEIST()
	OR GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FINALE()
	OR IS_FAKE_MULTIPLAYER_MODE_SET()
	OR IS_BIT_SET(g_fmmc_struct.sEndMocapSceneData.iCutsceneBitset, ci_CSBS_Pull_Team0_In + iMyTeam )
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_PlayEndCutsceneForAllPlayers)
		IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
			vMocapWarpPos = GET_FINAL_DROP_OFF_CENTER( MC_playerBD[iPartToUse].iteam )
		ELSE
			vMocapWarpPos = GET_CUTSCENE_COORDS(-1)
		ENDIF
		INT iWarpRange = 100
		IF MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_TUT_EXT
			iWarpRange = 500
		ELIF MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_PRI_FIN_EXT
		OR MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_SILJ_MCS1
			iWarpRange = 1000
			b3dCheck = FALSE
		ELIF MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_HUM_DEL_EXT
			iWarpRange = 300
		ENDIF
		
		IF NOT IS_VECTOR_ZERO( vMocapWarpPos )
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD( LocalPlayerPed, vMocapWarpPos,b3dCheck ) > iWarpRange
				IF NOT IS_BIT_SET(iLocalBoolCheck2,LBOOL2_CUTSCENE_WARP_DONE)
				AND NOT IS_BIT_SET(iLocalBoolCheck2,LBOOL2_ACTIVATE_MOCAP_CUTSCENE_WARP)	
					IF MC_serverBD.iEndCutsceneNum[ iMyTeam ] != -1
					AND NOT IS_PLAYER_SCTV( LocalPlayer )
					AND HAS_TEAM_PASSED_MISSION( iMyTeam )
						SET_BIT( iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE_WARP )
						IF ARE_STRINGS_EQUAL( sMocapCutscene, "MPH_TUT_CAR_EXT" )
							SET_BIT( iLocalBoolCheck10, LBOOL10_TUT_CAR_EXT_PLAYER_TOO_FAR )
						ENDIF
						IF AM_I_ON_A_HEIST()
							SETUP_SPECIFIC_SPAWN_LOCATION( vMocapWarpPos, 0 )
						ENDIF
						PRINTLN("[RCC MISSION] WARP_TO_END_MOCAP - LBOOL2_ACTIVATE_MOCAP_CUTSCENE_WARP SET ")
					ENDIF
					
					// url:bugstar:2197663
					IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
						SET_BIT( iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE_WARP )
						SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_WARP_TO_END_MOCAP_IN_PROGRESS )
						PRINTLN("[RCC MISSION] WARP_TO_END_MOCAP - iABI_WARP_TO_END_MOCAP_IN_PROGRESS - SET")
						IF AM_I_ON_A_HEIST()
							SETUP_SPECIFIC_SPAWN_LOCATION( vMocapWarpPos, 0 )
						ENDIF
						PRINTLN("[RCC MISSION] WARP_TO_END_MOCAP - Setting LBOOL2_ACTIVATE_MOCAP_CUTSCENE_WARP for SPECTATOR ")
					ENDIF
				ENDIF
			ENDIF 
		ENDIF


		IF IS_BIT_SET( iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE_WARP )
			IF NOT IS_SCREEN_FADED_OUT()
			AND NOT IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_OUT( 1500 )
			ENDIF
			IF IS_SCREEN_FADED_OUT()
			
				IF NOT busyspinner_is_on()

					BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("FMMC_PLYLOAD") 
					END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_LOADING))
					
				ENDIF
				IF (IS_ENTITY_DEAD(LocalPlayerPed)
				OR GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iRespawnState != RESPAWN_STATE_PLAYING)
				AND NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
					SET_MANUAL_RESPAWN_STATE(MRS_NULL)
					RESET_GAME_STATE_ON_DEATH()
					FORCE_RESPAWN(TRUE)
					PRINTLN("[RCC MISSION] WARP_TO_END_MOCAP - LBOOL2_ACTIVATE_MOCAP_CUTSCENE_WARP SET BUT PLAYER DEAD FORCING STATE")
				ELSE
					IF AM_I_ON_A_HEIST()
						IF WARP_TO_SPAWN_LOCATION( SPAWN_LOCATION_AT_SPECIFIC_COORDS, FALSE, FALSE, FALSE, FALSE )
							// url:bugstar:2197663
							IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
								SET_SPECTATOR_OVERRIDE_COORDS(vMocapWarpPos)
							ENDIF
							CLEAR_BIT( iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE_WARP )
							SET_BIT( iLocalBoolCheck2, LBOOL2_CUTSCENE_WARP_DONE )
							IF busyspinner_is_on()
								BusySpinner_off()
							ENDIF
						ELSE
							CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] WARP_TO_END_MOCAP Waiting for warp to work." )
						ENDIF
						
						IF NOT IS_LOCAL_PLAYER_PED_IN_CUTSCENE()
							SET_ENTITY_VISIBLE( LocalPlayerPed, FALSE)
							SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE( FALSE, FALSE )
							PRINTLN("[RCC MISSION] WARP_TO_END_MOCAP PLAYER NOT REGISTERED FOR CUTSCENE SETTING INVISIBLE ")
						ENDIF	
					ELSE						
						IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_CANCEL_LOAD_SCENE_FOR_END_CUTSCENE_WARP)
							IF IS_NEW_LOAD_SCENE_ACTIVE()
								PRINTLN("[RCC MISSION] WARP_TO_END_MOCAP Stopping load scene so that we can warp.")
								NEW_LOAD_SCENE_STOP()
							ENDIF
							SET_BIT(iLocalBoolCheck31, LBOOL31_CANCEL_LOAD_SCENE_FOR_END_CUTSCENE_WARP)
						ENDIF
						
						IF NET_WARP_TO_COORD( vMocapWarpPos, 0.0, FALSE, FALSE )
							IF busyspinner_is_on()
								BusySpinner_off()
							ENDIF
							CLEAR_BIT( iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE_WARP )
							SET_BIT( iLocalBoolCheck2, LBOOL2_CUTSCENE_WARP_DONE )
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
		ELSE // End of warp checks
			IF IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_WARP_TO_END_MOCAP_IN_PROGRESS)
				CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_WARP_TO_END_MOCAP_IN_PROGRESS)
				PRINTLN("[RCC MISSION] WARP_TO_END_MOCAP - iABI_WARP_TO_END_MOCAP_IN_PROGRESS - CLEARED - A")
			ENDIF
			RETURN TRUE
		ENDIF
	ELSE // IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL() OR AM_I_ON_A_HEIST() OR IS_FAKE_MULTIPLAYER_MODE_SET()
		IF IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_WARP_TO_END_MOCAP_IN_PROGRESS)
			CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_WARP_TO_END_MOCAP_IN_PROGRESS)
			PRINTLN("[RCC MISSION] WARP_TO_END_MOCAP - iABI_WARP_TO_END_MOCAP_IN_PROGRESS - CLEARED - B")
		ENDIF
		RETURN TRUE
	ENDIF

	RETURN FALSE
	
ENDFUNC

PROC SET_APARTMENT_LOCATION()
	

	// Get the post heist apartment location for the player. 
	INT iPlayerId = NATIVE_TO_INT(LocalPlayer)
	
	SWITCH iPlayerId
		CASE 0 
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.iApartmentLocation = MP_END_HEIST_WINNER_SCENE_PED_0_COORDS // MP_PROP_ELEMENT_HEIST_PLANNING_POST_MISSION_APT_POS_0 
			PRINTLN("[RCC] - [PMC] - [SAC] - SET_APARTMENT_LOCATION set my apartment location to location 0.")
		BREAK
		CASE 1 
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.iApartmentLocation = MP_END_HEIST_WINNER_SCENE_PED_1_COORDS // MP_PROP_ELEMENT_HEIST_PLANNING_POST_MISSION_APT_POS_1 
			PRINTLN("[RCC] - [PMC] - [SAC] - SET_APARTMENT_LOCATION set my apartment location to location 1.")
		BREAK
		CASE 2 
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.iApartmentLocation = MP_END_HEIST_WINNER_SCENE_PED_2_COORDS // MP_PROP_ELEMENT_HEIST_PLANNING_POST_MISSION_APT_POS_2 
			PRINTLN("[RCC] - [PMC] - [SAC] - SET_APARTMENT_LOCATION set my apartment location to location 2.")
		BREAK
		CASE 3 
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.iApartmentLocation = MP_END_HEIST_WINNER_SCENE_PED_3_COORDS // MP_PROP_ELEMENT_HEIST_PLANNING_POST_MISSION_APT_POS_3 
			PRINTLN("[RCC] - [PMC] - [SAC] - SET_APARTMENT_LOCATION set my apartment location to location 3.")
		BREAK
		DEFAULT
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.iApartmentLocation = MP_END_HEIST_WINNER_SCENE_PED_0_COORDS // MP_PROP_ELEMENT_HEIST_PLANNING_POST_MISSION_APT_POS_0 
			PRINTLN("[RCC] - [PMC] - [SAC] - SET_APARTMENT_LOCATION my player ID is > 3, not setup for this, setting my apartment location to location 0 for now.")
		BREAK
	ENDSWITCH
	
	
ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: RUN MOCAP CUTSCENES !
//
//************************************************************************************************************************************************************

PROC PROCESS_BANK_CUTSCENE_FAILSAFE_WARP()
	VECTOR vPos
	FLOAT fHead
	INT iMySlot
	BOOL bSuccessWarp
	
	FOR iMySlot = 0 TO 3
		GET_REMOTE_PLAYER_CUTSCENE_COORDS("MPH_PAC_FIN_MCS0", iMySlot, vPos, fHead, 0, 0)
		
		IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed, vPos) < 1.5
			bSuccessWarp = TRUE
			iMySlot = 5
		ENDIF
	ENDFOR
	
	IF NOT bSuccessWarp
		PRINTLN("[BankWarp] Player isn't in the bank! Forcing them in")
		iMySlot = GET_PARTICIPANT_NUMBER_IN_TEAM() + MC_PlayerBD[iLocalPart].iTeam
		
		IF iMySlot > 3
			iMySlot = 3 //just make sure we arent overbounds
		ENDIF
	
		GET_REMOTE_PLAYER_CUTSCENE_COORDS("MPH_PAC_FIN_MCS0", iMySlot, vPos, fHead, 0, 0)
	
		SET_ENTITY_COORDS(LocalPlayerPed, vPos)
	ELSE
		PRINTLN("[BankWarp] Player was in the bank!")
	ENDIF
ENDPROC


PROC RUN_IAAJ_EXT_CUTSCENE()
	IF NOT bIAAJ_EXTInited
		PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - Running IAAJ_EXT cutscene, setting up resources")
		REQUEST_STREAMED_TEXTURE_DICT("CS_NHP_IAAJ_EXT")
		IF NOT IS_NAMED_RENDERTARGET_REGISTERED("Prop_x17_TV_Ceil_Scn_01")
			REGISTER_NAMED_RENDERTARGET("Prop_x17_TV_Ceil_Scn_01")
			IF NOT IS_NAMED_RENDERTARGET_LINKED(INT_TO_ENUM(MODEL_NAMES, HASH("xm_Prop_x17_TV_Ceiling_Scn_01")))
				LINK_NAMED_RENDERTARGET(INT_TO_ENUM(MODEL_NAMES, HASH("xm_Prop_x17_TV_Ceiling_Scn_01")))
				IF iIAAJ_EXTRenderTargetIDs[0] = 0
					iIAAJ_EXTRenderTargetIDs[0] = GET_NAMED_RENDERTARGET_RENDER_ID("Prop_x17_TV_Ceil_Scn_01")
					PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - Running IAAJ_EXT cutscene, setting up ceiling screen 1")
				ELSE
					PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - Fail 1")
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - Fail 2")
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - Fail 3")
		ENDIF
		IF NOT IS_NAMED_RENDERTARGET_REGISTERED("Prop_x17_TV_Ceil_Scn_02")
			REGISTER_NAMED_RENDERTARGET("Prop_x17_TV_Ceil_Scn_02")
			IF NOT IS_NAMED_RENDERTARGET_LINKED(INT_TO_ENUM(MODEL_NAMES, HASH("xm_Prop_x17_TV_Ceiling_Scn_02")))
				LINK_NAMED_RENDERTARGET(INT_TO_ENUM(MODEL_NAMES, HASH("xm_Prop_x17_TV_Ceiling_Scn_02")))
				IF iIAAJ_EXTRenderTargetIDs[1] = 0
					iIAAJ_EXTRenderTargetIDs[1] = GET_NAMED_RENDERTARGET_RENDER_ID("Prop_x17_TV_Ceil_Scn_02")
					PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - Running IAAJ_EXT cutscene, setting up ceiling screen 2")
				ELSE
					PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - Fail 4")
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - Fail 5")
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - Fail 6")
		ENDIF
		bIAAJ_EXTInited = TRUE
	ENDIF
	PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - Running IAAJ_EXT cutscene")
	ENTITY_INDEX tempEntity = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_1")
	IF DOES_ENTITY_EXIST(tempEntity)
		IF NOT IS_ENTITY_DEAD(tempEntity)
			IF (NOT bIAAJ_EXTDrawProbabilityGraphic)
			AND HAS_ANIM_EVENT_FIRED(tempEntity, HASH("PROBABILITY"))
				bIAAJ_EXTDrawProbabilityGraphic = TRUE
			ENDIF
			IF (NOT bIAAJ_EXTDrawBogdanGraphics)
			AND HAS_ANIM_EVENT_FIRED(tempEntity, HASH("BOGDAN_GRAPHIC"))
				bIAAJ_EXTDrawBogdanGraphics = TRUE
				bIAAJ_EXTDrawProbabilityGraphic = FALSE
			ENDIF
			INT x = 0
			INT y = 1
			// Draw to screens
			IF bIAAJ_EXTDrawProbabilityGraphic
				
				PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - Running IAAJ_EXT cutscene, drawing graphics 0")
				SET_TEXT_RENDER_ID(iIAAJ_EXTRenderTargetIDs[x])
				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
				SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
				REQUEST_STREAMED_TEXTURE_DICT("CS_NHP_IAAJ_EXT")
				IF HAS_STREAMED_TEXTURE_DICT_LOADED("CS_NHP_IAAJ_EXT")
					DRAW_SPRITE("CS_NHP_IAAJ_EXT", "IAAJ_EXT_01", 0.5, 0.5, 1.0, 1.0, 0.0, 255, 255, 255, 255)
				ENDIF
				SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())

			ELIF bIAAJ_EXTDrawBogdanGraphics
				
				PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - Running IAAJ_EXT cutscene, drawing graphics 1")
				// 2 images for this
				SET_TEXT_RENDER_ID(iIAAJ_EXTRenderTargetIDs[x])
				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
				SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
				REQUEST_STREAMED_TEXTURE_DICT("CS_NHP_IAAJ_EXT")
				IF HAS_STREAMED_TEXTURE_DICT_LOADED("CS_NHP_IAAJ_EXT")
					DRAW_SPRITE("CS_NHP_IAAJ_EXT", "IAAJ_EXT_02", 0.5, 0.5, 1.0, 1.0, 0.0, 255, 255, 255, 255)
				ELSE
					PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - Sprite 1 has not loaded")
				ENDIF
				SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
				
				SET_TEXT_RENDER_ID(iIAAJ_EXTRenderTargetIDs[y])
				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
				SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
				REQUEST_STREAMED_TEXTURE_DICT("CS_NHP_IAAJ_EXT")
				IF HAS_STREAMED_TEXTURE_DICT_LOADED("CS_NHP_IAAJ_EXT")
					DRAW_SPRITE("CS_NHP_IAAJ_EXT", "IAAJ_EXT_03", 0.5, 0.5, 1.0, 1.0, 0.0, 255, 255, 255, 255)
				ELSE
					PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - Sprite 2 has not loaded")
				ENDIF
				SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
				
				// Do same for other screens, see (see url:bugstar:4210832)
			ELSE
				PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - Running IAAJ_EXT cutscene, drawing graphics NONE")
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - TempEntity is dead")
		ENDIF
	ELSE
		PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - TempEntity does not exists")
	ENDIF
ENDPROC

FUNC INT GET_MOCAP_CUTSCENE_FADE_IN_TIME(STRING sMocapName)

	// These cutscenes require fast fade ins. See first instance where this was necessary... url:bugstar:5835140
	IF ARE_STRINGS_EQUAL(sMocapName, "MPCAS2_EXT")
		RETURN MOCAP_START_FADE_IN_DURATION_QUICK
	ENDIF
	
	RETURN MOCAP_START_FADE_IN_DURATION_NORMAL
ENDFUNC

PROC SET_REGISTERED_ENTITIES_VISIBLE_FOR_CUTSCENE(FMMC_CUTSCENE_TYPE eCutType, INT iCutsceneToUse)
	
	IF eCutType != FMMCCUT_MOCAP
	AND eCutType != FMMCCUT_ENDMOCAP	
		EXIT
	ENDIF
	
	IF eCutType = FMMCCUT_MOCAP
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_ForceRegisteredEntitiesVisible)
		EXIT
	ENDIF
	
	IF eCutType = FMMCCUT_ENDMOCAP
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet2, ci_CSBS2_ForceRegisteredEntitiesVisible)
		EXIT
	ENDIF
	
	PRINTLN("[LM][RCC MISSION] PROCESS_MOCAP_CUTSCENE - SET_REGISTERED_ENTITIES_VISIBLE_FOR_CUTSCENE - Checking for entities that should be visible.")
	
	TEXT_LABEL_31 tl31Handle 
	ENTITY_INDEX entID
	NETWORK_INDEX netID
	INT i
	
	FOR i = 0 TO FMMC_MAX_CUTSCENE_ENTITIES-1
			
		IF eCutType = FMMCCUT_MOCAP
			IF iCutsceneToUse <= -1
				EXIT
			ENDIF
			IF g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sCutsceneEntities[i].iType = -1
			OR g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sCutsceneEntities[i].iIndex = -1
				RELOOP
			ENDIF
			tl31Handle = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sCutsceneEntities[i].tlCutsceneHandle
			
			IF IS_STRING_NULL_OR_EMPTY(tl31Handle)
				IF g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sCutsceneEntities[i].iType = CREATION_TYPE_PROPS
					EntID = GET_CUTSCNE_VISIBLE_PROP(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sCutsceneEntities[i].iIndex)
					IF EntID != NULL
						//SET_ENTITY_VISIBLE_IN_CUTSCENE(EntID,bshow,FALSE)
						SET_ENTITY_VISIBLE(EntID, TRUE)
					ENDIF
				ELSE
					netID = GET_CUTSCNE_VISIBLE_NET_ID(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sCutsceneEntities[i].iType, g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sCutsceneEntities[i].iIndex)
					PRINTLN("[LM][RCC MISSION] PROCESS_MOCAP_CUTSCENE - SET_REGISTERED_ENTITIES_VISIBLE_FOR_CUTSCENE - Checking for entities that should be visible.")
					IF netID != NULL
						PRINTLN("[LM][RCC MISSION] PROCESS_MOCAP_CUTSCENE - SET_REGISTERED_ENTITIES_VISIBLE_FOR_CUTSCENE - Registered Cutscene Prop without Handle. Calling SET_ENTITY_VISIBLE with TRUE")
						SET_NETWORK_ID_VISIBLE_IN_CUTSCENE(netID, TRUE, TRUE)
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[LM][RCC MISSION] PROCESS_MOCAP_CUTSCENE - SET_REGISTERED_ENTITIES_VISIBLE_FOR_CUTSCENE - Prop has a handle...")
			ENDIF
			
		ELIF eCutType = FMMCCUT_ENDMOCAP
			IF g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iType =-1
			OR g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iIndex = -1
				RELOOP
			ENDIF
			tl31Handle = g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].tlCutsceneHandle
			
			IF IS_STRING_NULL_OR_EMPTY(tl31Handle)
				IF g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iType = CREATION_TYPE_PROPS
					EntID = GET_CUTSCNE_VISIBLE_PROP(g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iIndex)
					IF EntID != NULL
						//SET_ENTITY_VISIBLE_IN_CUTSCENE(EntID,bshow,FALSE)
						SET_ENTITY_VISIBLE(EntID, TRUE)
					ENDIF
				ELSE
					netID = GET_CUTSCNE_VISIBLE_NET_ID(g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iType, g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iIndex)					
					IF netID != NULL
						PRINTLN("[LM][RCC MISSION] PROCESS_MOCAP_CUTSCENE - SET_REGISTERED_ENTITIES_VISIBLE_FOR_CUTSCENE - Registered Cutscene Entity without Handle. Calling SET_NETWORK_ID_VISIBLE_IN_CUTSCENE with TRUE, TRUE")						
						SET_NETWORK_ID_VISIBLE_IN_CUTSCENE(netID, TRUE, TRUE)
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[LM][RCC MISSION] PROCESS_MOCAP_CUTSCENE - SET_REGISTERED_ENTITIES_VISIBLE_FOR_CUTSCENE - Entity has a handle...")
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

FUNC BOOL ARE_ALL_PLAYERS_SYNCED_FOR_ENDMOCAP_PASSOVER_ENTITY_REGISTRATION()
	
	INT iPart
	FOR iPart = 0 TO NUM_NETWORK_PLAYERS-1
		PARTICIPANT_INDEX participantIndex = INT_TO_PARTICIPANTINDEX(iPart)
				
		IF NETWORK_IS_PARTICIPANT_ACTIVE(participantIndex)		
			PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(participantIndex)
			
			IF DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(piPlayer)
				RELOOP
			ENDIF
			
			IF IS_PLAYER_SCTV(piPlayer)
				RELOOP
			ENDIF
			
			IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet4, PBBOOL4_REGISTERED_ENTITIES_FOR_END_MOCAP_PASS_OVER)
				PRINTLN("[LM][ARE_ALL_PLAYERS_SYNCED_FOR_ENDMOCAP_PASSOVER_ENTITY_REGISTRATION] Waiting for ", iPart, " so returning FALSE") 
				RETURN FALSE
			ENDIF		
		ENDIF
	ENDFOR
	
	PRINTLN("[LM][ARE_ALL_PLAYERS_SYNCED_FOR_ENDMOCAP_PASSOVER_ENTITY_REGISTRATION] SYNCED, returning TRUE")
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_RUN_MOCAP_MPH_PAC_FIN_MCS(STRING sMocapName)	
	IF NOT IS_BIT_SET(iLocalBoolCheck11, LBOOL11_PAC_FIN_GIVEN_WEAPON)
		IF ARE_STRINGS_EQUAL(sMocapName, "MPH_PAC_FIN_MCS0")
		OR ARE_STRINGS_EQUAL(sMocapName, "MPH_PAC_FIN_MCS1")
			IF HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_SPECIALCARBINE)
				BOOL bForce
				
				IF ARE_STRINGS_EQUAL(sMocapName, "MPH_PAC_FIN_MCS1")
					bForce = TRUE
				ENDIF
			
				SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_SPECIALCARBINE, bForce)
			ELSE
				GIVE_WEAPON_TO_PED(LocalPlayerPed, WEAPONTYPE_DLC_SPECIALCARBINE, GET_WEAPON_CLIP_SIZE(WEAPONTYPE_DLC_SPECIALCARBINE) * 10, TRUE)
			ENDIF
			PRINTLN("[RUN_MOCAP_CUTSCENE][MPH_PAC_FIN] Equipping rifle for ", sMocapCutscene)
			SET_BIT(iLocalBoolCheck11, LBOOL11_PAC_FIN_GIVEN_WEAPON)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_RUN_MOCAP_MPH_PAC_FIN_MC0(STRING sMocapName)			
	//Cache weapon data for weapon components.
	IF NOT IS_BIT_SET(iLocalBoolCheck9, LBOOL9_CACHED_WEAPON_DATA)
	AND ARE_STRINGS_EQUAL(sMocapName, "MPH_PAC_FIN_MCS0")
		IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()	
			SEND_MY_WEAPONDATA_TO_REMOTE_PLAYERS(WEAPONTYPE_DLC_SPECIALCARBINE)						
			SET_BIT( iLocalBoolCheck9, LBOOL9_CACHED_WEAPON_DATA )
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][ROSSW][RUN_MOCAP_CUTSCENE] Sent weapon data to remote players")
		ELSE
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][ROSSW][RUN_MOCAP_CUTSCENE] IS_FAKE_MULTIPLAYER_MODE_SET do not send weapon data ")
		ENDIF
	ENDIF
ENDPROC

// [FIX_2020_CONTROLLER] - This function needs to be turned into the legacy version and a new one should be made for new content which excludes all of the zany weird special case "this_specific_cutscene" logic.
// It's worth noting that most cutscenes return true on MOCAPRUNNING_STAGE_3 and the other states are for specific cutscenes. We could also be able to convert MOCAPRUNNING_STAGE into enum state names that make sense..
///PURPOSE: This is the main mocap function that loads and runs mocap cutscenes
FUNC BOOL RUN_MOCAP_CUTSCENE(STRING sMocapName,
							FMMC_CUTSCENE_TYPE eCutType, 
							BOOL bShowCelebrationScreen, 
							VECTOR vStartPos, 
							FLOAT fStartHeading, 
							VECTOR vEndPos, 
							FLOAT fEndHeading, 
							BOOL bIncludePlayers = TRUE, 
							BOOL bCrowdControlOverride = FALSE, 
							BOOL bClearAreaAndParticles = TRUE, 
							BOOL bCreateHeistDoors = FALSE,
							BOOL bShouldFadeAtEnd = FALSE)

	INT iCutsceneTeam
	//FLOAT fCutsceneTime
	
	BOOL bGivePlayerControl
	
	STOP_LOCAL_PLAYER_FROM_GETTING_SHOT_THIS_FRAME()
	
	IF MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_PLACE_HOLDER
	AND eCutType = FMMCCUT_ENDMOCAP
		sMocapName = ""
	ENDIF
	
	IF iScriptedCutsceneTeam != -1
	AND eCutType != FMMCCUT_ENDMOCAP
		iCutsceneTeam = iScriptedCutsceneTeam
	ELSE
		iCutsceneTeam = MC_PlayerBD[iPartToUse].iTeam
	ENDIF
	
	IF IS_A_STRAND_MISSION_BEING_INITIALISED()
		bShowCelebrationScreen = FALSE
	ENDIF
		
	IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
	AND IS_CUTSCENE_PLAYING()
	AND GET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS() < MOCAPRUNNING_STAGE_3
		SET_SPECTATOR_OVERRIDE_COORDS( GET_FINAL_RENDERED_CAM_COORD() )
		SHOW_HUD_COMPONENT_THIS_FRAME( NEW_HUD_SUBTITLE_TEXT )
	ENDIF
	
	IF ARE_STRINGS_EQUAL(sMocapName, "MPH_HUM_KEY_MCS1")
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_AIM )
	ENDIF
	
	IF IS_CUTSCENE_PLAYING()
	AND GET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS() < MOCAPRUNNING_STAGE_3
		DISABLE_FRONTEND_THIS_FRAME()
	ENDIF
							
	INT iCutsceneToUse
	
	IF g_iFMMCScriptedCutscenePlaying >= FMMC_GET_MAX_SCRIPTED_CUTSCENES() 
	AND g_iFMMCScriptedCutscenePlaying < FMMC_GET_MAX_CUTSCENES_WITH_MOCAP() 
		iCutsceneToUse = (g_iFMMCScriptedCutscenePlaying - FMMC_GET_MAX_SCRIPTED_CUTSCENES())
	ELSE 
		iCutsceneToUse = g_iFMMCScriptedCutscenePlaying
	ENDIF
	
	IF g_eIAAGunCamerasEnabled != IAA_FULL_ACTIVITY_KICK
		g_eIAAGunCamerasEnabled = IAA_FULL_ACTIVITY_KICK
	ENDIF
	
	// NOTE(Owain): by default we want to skip a cutscene if we're too far away 
	BOOL bCheckSkipIfTooFar = TRUE
	
	IF ARE_STRINGS_EQUAL(sMocapName, "IAAJ_EXT")
		RUN_IAAJ_EXT_CUTSCENE()
	ENDIF
	
	CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][RUN_MOCAP_CUTSCENE] sMocapName: ", sMocapName, " eCutType: ", ENUM_TO_INT(eCutType), " iCutsceneTeam: ", iCutsceneTeam, " iCutsceneToUse: ", iCutsceneToUse)
	
	SWITCH eMocapRunningCutsceneProgress
	
		CASE MOCAPRUNNING_STAGE_0
			
			CUTSCENE_INTERIOR_INIT_SPECIAL_CASE(sMocapName)
			
			PROCESS_RUN_MOCAP_MPH_PAC_FIN_MCS(sMocapName)
			
			PROCESS_RUN_MOCAP_MPH_PAC_FIN_MC0(sMocapName)			

			IF IS_PLAYSTATION_PLATFORM()
				SET_CONTROL_LIGHT_EFFECT_COLOR(PLAYER_CONTROL, 255, 255, 255)
			ENDIF
			
			IF NOT bLocalPlayerOK
			OR (IS_PLAYER_IN_ANY_SHOP() AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED())		// Ignore shop check for Strand missions (2502614)				
				PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] IS_PLAYER_IN_ANY_SHOP or dead skipping cutscene ")
				SET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS(MOCAPRUNNING_STAGE_2)
				RETURN FALSE
			ENDIF

			// NOTE(Owain): Don't skip if we're too far away if we want to include our team in this cutscene
			IF eCutType = FMMCCUT_ENDMOCAP
			AND (IS_BIT_SET( g_fmmc_struct.sEndMocapSceneData.iCutsceneBitset, ci_CSBS_Pull_Team0_In + MC_PlayerBD[iPartToUse].iTeam )
			OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_PlayEndCutsceneForAllPlayers))
				bCheckSkipIfTooFar = FALSE 
			ENDIF
			
			// NOTE(Owain): Don't skip this cutscene if we're in a heist or if we're doing a crowd control fail cutscene
			IF bCrowdControlOverride
			OR AM_I_ON_A_HEIST()
				bCheckSkipIfTooFar = FALSE
			ENDIF
			
			IF bCheckSkipIfTooFar
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD( LocalPlayerPed, GET_CUTSCENE_COORDS(iCutsceneTeam, eCutType)) > 200
				AND NOT IS_BIT_SET( iLocalBoolCheck2, LBOOL2_CUTSCENE_WARP_DONE )
				AND NOT IS_BIT_SET( iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE_WARP )
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][RUN_MOCAP_CUTSCENE] Skipping cutscene - too far away!")					
					SET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS(MOCAPRUNNING_STAGE_2)
					RETURN FALSE
				ENDIF
			ENDIF
			
			IF bCrowdControlOverride
			AND IS_PLAYER_SCTV( LocalPlayer )
				IF iCutsceneToUse != -1
					CLEAR_CUTSCENE_AREA(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].fClearRadius, g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].vClearCoOrds,g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].vReplacementArea)
					PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] - iCutsceneToUse != -1 CLEAR_AREA")
				ELSE
					CLEAR_AREA(vStartPos, 100.0, TRUE, TRUE, DEFAULT, FALSE)
					PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] - Generical CLEAR_AREA")
				ENDIF
			ENDIF
			
			IF IS_THIS_MOCAP_IN_AN_APARTMENT( sMocapName )
				IF NOT SHOULD_SEE_APARTMENT_CUTSCENE( sMocapName )
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][RUN_MOCAP_CUTSCENE] Skipping cutscene ", sMocapName, " because I was not warped into the apartment." )
					SET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS(MOCAPRUNNING_STAGE_2)
					RETURN FALSE
				ENDIF
				
				IF IS_LOCAL_PLAYER_ANY_SPECTATOR()					
					SET_BIT( MC_playerBD[ iLocalPart ].iClientBitSet2, PBBOOL2_I_AM_SPECTATOR_WATCHING_MOCAP )
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][RUN_MOCAP_CUTSCENE] Skipping cutscene ", sMocapName, " because I'm SCTV." )
					SET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS(MOCAPRUNNING_STAGE_2)
					RETURN FALSE
				ENDIF
			ENDIF
			
			IF IS_THIS_MID_MISSION_MOCAP_IN_AN_APARTMENT( sMocapName )
			AND (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_MP_PROPERTY_INT")) <= 0
			AND NOT IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_AcceptedInvite))				
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][RUN_MOCAP_CUTSCENE] Skipping cutscene ", sMocapName, " because I'm not loaded into property." )
				SET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS(MOCAPRUNNING_STAGE_2)
				RETURN FALSE
			ENDIF
			
			#IF IS_DEBUG_BUILD
			IF NOT IS_CUTSCENE_AUTHORIZED( sMocapName )
			AND MC_ServerBD.iEndCutscene != ciMISSION_CUTSCENE_PLACE_HOLDER
				CWARNINGLN( DEBUG_CONTROLLER, "Cutscene ", sMocapName, " IS NOT AUTHORIZED - it might not play. Either you don't have latest, or you're playing a cutscene that is not marked as ready yet." )
			ENDIF
			#ENDIF
			
			CLEAR_BIT( iLocalBoolCheck11, LBOOL11_TREVOR_TATTOO_APPLIED )

			IF NOT bIncludePlayers
			OR ARE_TEAM_CUTSCENE_PLAYERS_SELECTED( iCutsceneTeam )
				
				IF iMocapCutsceneConcatParts = -1
				AND AM_I_ON_A_HEIST()
				#IF IS_DEBUG_BUILD
				AND NOT (MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_PLACE_HOLDER)
				#ENDIF
					IF NOT IS_STRING_NULL_OR_EMPTY( sMocapName )
						IF NOT Downloaded_Heist_Cutscene_Data_File( sMocapName, iMocapCutsceneConcatParts )
							CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][RUN_MOCAP_CUTSCENE] waiting for Downloaded_Heist_Cutscene_Data_File")
							RETURN FALSE
						ENDIF
					ELSE
						CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][RUN_MOCAP_CUTSCENE] waiting for IS_STRING_NULL_OR_EMPTY on sMocapName")
					ENDIF
				ENDIF
				
				g_bAbortPropertyMenus = TRUE
				PRINTLN("[RUN_MOCAP_CUTSCENE] - g_bAbortPropertyMenus = TRUE")
				SET_BIT( iLocalBoolCheck5, LBOOL5_PED_INVOLVED_IN_CUTSCENE )
				SET_BIT( MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYING_CUTSCENE )
				SET_BIT( iLocalBoolCheck9, LBOOL9_FORCE_STREAM_CUTSCENE )
																				
				// Remove player parachute for Pacific Standard Finale cutscene
				IF ARE_STRINGS_EQUAL("MPH_PAC_FIN_EXT", sMocapName)
					CLEAN_UP_PLAYER_PED_PARACHUTE()
				ENDIF
				
				IF HAS_CUTSCENE_LOADED()
				AND (IS_BIT_SET(iLocalBoolCheck2, LBOOL2_CUTSCENE_STREAMED) OR HAS_NET_TIMER_EXPIRED(streamCSFailsafeTimer, 10000, TRUE))
				#IF IS_DEBUG_BUILD
				OR MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_PLACE_HOLDER
				#ENDIF
				
					//wait untill FMMC_Launcher is ready #2 frames
					IF IS_A_STRAND_MISSION_BEING_INITIALISED()
						IF g_sTransitionSessionData.sStrandMissionData.iSwitchState < ciMAINTAIN_STRAND_MISSION_START_CUTSCENE
							CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][RUN_MOCAP_CUTSCENE] waiting for g_sTransitionSessionData.sStrandMissionData.iSwitchState < ciMAINTAIN_STRAND_MISSION_START_CUTSCENE")
							RETURN FALSE
						ENDIF
					ENDIF
					
					IF HAVE_MOCAP_START_CONDITIONS_BEEN_MET(iCutsceneToUse, eCutType)
						IF NOT bIncludePlayers
						OR CLONE_MOCAP_PLAYERS( sMocapName, iCutsceneTeam, bIncludePlayers, eCutType, iCutsceneToUse)
						
							PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] CURRENT NETWORK TIMER: ", NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()))
							PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] GET_NET_TIMER_DIFF", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_ServerBD.tdCutsceneStartTime[iCutsceneTeam],FALSE,TRUE),"[RCC MISSION] is host: ", bIsLocalPlayerHost)
							
							CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][RUN_MOCAP_CUTSCENE] case 0" )
							
							IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_ServerBD.tdCutsceneStartTime[iCutsceneTeam],FALSE,TRUE) >= 1000
								
								SET_BIT(MC_playerBD[iLocalPart].iClientBitset2, PBBOOL2_PLAYING_MOCAP_CUTSCENE)
								
								// url:bugstar:2208851 Access livery
								IF ARE_STRINGS_EQUAL(sMocapName, "MPH_HUM_VAL_EXT")
									iValkyrieDesign = -1
		
									INT iPart
									iPart = 0
									REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iPart
										IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
										AND iValkyrieDesign = -1
											PLAYER_INDEX tempPlayer
											tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart))
											
											VEHICLE_INDEX vehValkyrie
											vehValkyrie = GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(tempPlayer))
											
											IF DOES_ENTITY_EXIST(vehValkyrie)
												INT iExtra
												iExtra = 12
												FOR iExtra = 12 TO 14
													IF IS_VEHICLE_EXTRA_TURNED_ON(vehValkyrie, iExtra)
														iValkyrieDesign = iExtra
														iExtra = 14 // Exit loop
													ENDIF
												ENDFOR
												PRINTLN("[RUN_MOCAP_CUTSCENE] MPH_HUM_VAL_EXT - Valkyrie extra = ", iValkyrieDesign)
											ELSE
												PRINTLN("[RUN_MOCAP_CUTSCENE] MPH_HUM_VAL_EXT - Valkyrie check FAILED")
											ENDIF
										ENDIF
									ENDREPEAT
								ENDIF
								
								IF NOT ARE_STRINGS_EQUAL( "MPH_PRI_FIN_MCS2", sMocapName ) //Bypassing for bug url:bugstar:2093383
								AND NOT ARE_STRINGS_EQUAL( "MPH_PRI_FIN_EXT", sMocapName ) //Bypassing for bug url:bugstar:2167627
								AND NOT ARE_STRINGS_EQUAL( "MPH_TUT_EXT", sMocapName )
								AND NOT ARE_STRINGS_EQUAL( "MPH_PAC_FIN_EXT", sMocapName ) //b*2200189
									clone_gameplay_camera() //stops camera pops when the clones are created by copying the gameplay camera before the camera update.
								ENDIF
								
								ANIMPOSTFX_STOP_ALL()
								
								reduce_voice_chat_for_specific_cutscenes( sMocapName ) 
							
						//NOTE(Owain): Extra IF for NG only for B* 2196291
						#IF IS_NEXTGEN_BUILD
								IF NOT ARE_STRINGS_EQUAL( sMocapName, "MPH_HUM_EMP_EXT" )
						#ENDIF
								CLEAR_BIT( iLocalBoolCheck7, LBOOL7_WAS_VEH_RADIO_ENABLED_AT_CS_START )
								sMyRadioStation = ""
								IF( NOT IS_ENTITY_DEAD( LocalPlayerPed ) )
									IF( IS_PED_SITTING_IN_ANY_VEHICLE( LocalPlayerPed ) )
										VEHICLE_INDEX veh
										veh = GET_VEHICLE_PED_IS_IN( LocalPlayerPed )
										IF( NOT IS_ENTITY_DEAD( veh ) )
											IF( IS_PLAYER_VEH_RADIO_ENABLE() ) 
												IF NETWORK_HAS_CONTROL_OF_ENTITY( veh )
													sMyRadioStation = GET_PLAYER_RADIO_STATION_NAME()
													SET_VEH_RADIO_STATION(veh, "OFF")
													PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] SET_VEH_RADIO_STATION(veh, OFF) model = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(veh)), " sMyRadioStation = ", sMyRadioStation)
												ENDIF
												SET_BIT( iLocalBoolCheck7, LBOOL7_WAS_VEH_RADIO_ENABLED_AT_CS_START )
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
						#IF IS_NEXTGEN_BUILD
								ENDIF
						#ENDIF
								
								PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] Mid-mission cutscene is set to:" , iCutsceneToUse )

								IF IS_PLAYER_SCTV(LocalPlayer)
									SET_BIT( MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_SCTV_IN_MOCAP )
									PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] GLOBAL_SPEC_BS_SCTV_IN_MOCAP SET")
								ENDIF
								IF IS_BIT_SET( MC_playerBD[ iLocalPart].iClientBitSet, PBBOOL_ANY_SPECTATOR ) 
									SET_SPECTATOR_RUNNING_CUTSCENE(TRUE)
									DISABLE_SPECTATOR_RADIO(TRUE)
									PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] - SET SPECTATOR FLAGS")
								ENDIF
								
								// Very few cutscenes actually utilize gameplay radio, we might want to add an option to prevent this logic from killing the radio.
								IF eCutType = FMMCCUT_ENDMOCAP
								AND IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO)
									PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] - End Mocap Cutscene - Killing Radios.")
									IF NOT IS_PED_INJURED( LocalPlayerPed )
										IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
											VEHICLE_INDEX veh
											veh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
											IF NOT IS_ENTITY_DEAD(veh)
												IF IS_PLAYER_VEH_RADIO_ENABLE()
													IF NETWORK_HAS_CONTROL_OF_ENTITY(veh)
														sMyRadioStation = GET_PLAYER_RADIO_STATION_NAME()
														SET_VEH_RADIO_STATION(veh, "OFF")
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									SET_AUDIO_FLAG("AllowRadioDuringSwitch", FALSE) 
									SET_MOBILE_PHONE_RADIO_STATE(FALSE)
									SET_AUDIO_FLAG("MobileRadioInGame", FALSE)
								ENDIF
								
								DO_PRE_MOCAP_ENDING_CAMERA_CHECKS()
								
								IF DOES_CUTSCENE_HAVE_REGISTERED_ENTITIES(iCutsceneToUse, eCutType)
									
									REGISTER_CUTSCENE_ENTITIES(iCutsceneToUse, TRUE, eCutType)
										
									CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][RUN_MOCAP_CUTSCENE] MOCAP CUTSCENE ENTITIES SHOULD BE INVISIBLE")
									
									INT iCutsceneBitset
									
									IF eCutType = FMMCCUT_ENDMOCAP
										iCutsceneBitset = g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitset
									ELIF iCutsceneToUse != -1
										iCutsceneBitset = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitset
									ENDIF
								    IF NOT IS_BIT_SET( MC_playerBD[ iLocalPart].iClientBitSet, PBBOOL_ANY_SPECTATOR ) 
									    IF NOT IS_BIT_SET( iCutsceneBitset, ci_CSBS_Hide_Your_Player )
											IF IS_LOCAL_PLAYER_PED_IN_CUTSCENE()
										   		SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE( TRUE, FALSE ) //2088726 changed to false we dont want to be seen remotely
										    	PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE( TRUE, FALSE ) ")
											ELSE
												SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE( FALSE, FALSE ) //2165011 When player isn't in the cutscene
										    	PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE( FALSE, FALSE ) - NOT IN CUTSCENE ")
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								MANUALLY_REGISTER_SPECIFIC_ENTITIES(sMocapName, eCutType)
								
								IF bCreateHeistDoors
									Create_Heist_Cutscene_Doors(TRUE, FALSE)
								ENDIF
															
								Register_Appartment_Door_Cutscene()
								
								RESET_DROPOFF_GLOBALS( 1 )
								
								
									IF NOT IS_BIT_SET(iLocalBoolCheck5, LBOOL5_MOCAP_REMOVED_LOCAL_PLAYER_MASK)
										IF CAN_REMOVE_MASKS_FOR_HEIST_CUTSCENE(sMocapName, DEFAULT, eCutType, iCutsceneToUse)
											REMOVE_MASKS_AND_REBREATHERS_FOR_CUTSCENES(LocalPlayerPed, LocalPlayer, INT_TO_ENUM(MP_OUTFIT_MASK_ENUM, GET_PLAYER_CHOSEN_MASK(LocalPlayer)))
											SET_BIT(iLocalBoolCheck5, LBOOL5_MOCAP_REMOVED_LOCAL_PLAYER_MASK)
										ENDIF
									ENDIF
									
									//REMOVE REBREATHERS
									IF DOES_ENTITY_EXIST( objFlare ) AND NOT IS_MP_HEIST_GEAR_EQUIPPED( LocalPlayerPed, HEIST_GEAR_REBREATHER )
										STOP_PARTICLE_FX_LOOPED( Players_Underwater_Flare)
										REMOVE_NAMED_PTFX_ASSET( "scr_biolab_heist" )
										DETACH_ENTITY( objFlare )
										DELETE_OBJECT( objFlare )
										SET_OBJECT_AS_NO_LONGER_NEEDED( objFlare )	
									ENDIF

									
								IF (IS_A_STRAND_MISSION_BEING_INITIALISED() AND eCutType = FMMCCUT_ENDMOCAP)						
									
									IF NOT IS_BIT_SET(iLocalBoolCheck3,LBOOL3_LATE_SPECTATE)
									AND NOT IS_BIT_SET(iLocalBoolCheck3,LBOOL3_WAIT_END_SPECTATE)
						           		CLEAR_MISSION_CONTROLLER_JOINED_AS_SPECTATOR_FOR_CUTSCENE()
									ELSE
										SET_MISSION_CONTROLLER_JOINED_AS_SPECTATOR_FOR_CUTSCENE()
									ENDIF
									SET_MY_STRAND_MISSION_TEAM(GET_CUTSCENE_TEAM())
									REGISTER_CUTSCENE_PLAYERS(iCutsceneTeam, iCutsceneToUse, eCutType, bIncludePlayers)
									REMOVE_SWAT_FOR_mph_pac_fin_mcs1(sMocapName)
									SET_MODEL_AS_NO_LONGER_NEEDED(MP_F_FREEMODE_01)
									SET_MODEL_AS_NO_LONGER_NEEDED(MP_M_FREEMODE_01)
									
									SET_STRAND_MISSION_CUTSCENE_NAME(sMocapName)
									SET_SCRIPT_CAN_START_CUTSCENE(GET_THREADID_OF_SCRIPT_WITH_THIS_NAME("FMMC_Launcher"))
									
									IF NOT IS_LOCAL_PLAYER_PED_IN_CUTSCENE()
										IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
											SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE( FALSE, FALSE ) // url:bugstar:2229965
										ELSE
											SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE( FALSE, TRUE )
										ENDIF
										PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] PLAYER NOT REGISTERED FOR CUTSCENE SETTING INVISIBLE ")
									ENDIF									
									
									CPRINTLN( DEBUG_CONTROLLER, "[TS] [MSRAND][RUN_MOCAP_CUTSCENE] - SET_SCRIPT_CAN_START_CUTSCENE - GET_THREADID_OF_SCRIPT_WITH_THIS_NAME(\"FMMC_Launcher\")")
								ELSE
									clear_prints()
									clear_help()
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
									
									IF bIncludePlayers
										REGISTER_CUTSCENE_PLAYERS(iCutsceneTeam, iCutsceneToUse, eCutType, bIncludePlayers)
									ENDIF
									
									register_madrazo_assets()
									
									IF NOT IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO)
										set_cutscene_vehicles_visible_in_cutscenes()
									ENDIF
									
									IF NOT IS_PLAYER_SCTV(LocalPlayer)
									AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
										NET_SET_PLAYER_CONTROL(LocalPlayer,FALSE)
									ENDIF
							
									IF NOT IS_LOCAL_PLAYER_PED_IN_CUTSCENE()
										IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
											SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE( FALSE, FALSE ) // url:bugstar:2229965
										ELSE
											SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE( FALSE, TRUE )
										ENDIF
										PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] PLAYER NOT REGISTERED FOR CUTSCENE SETTING INVISIBLE ")
									ENDIF

									SET_STORE_ENABLED(FALSE)
									
									SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
									CPRINTLN(DEBUG_MIKE, "[RCC MISSION][RUN_MOCAP_CUTSCENE] SET_SCRIPTS_SAFE_FOR_CUTSCENE = TRUE")
								ENDIF
																	
								IF IS_BIT_SET( MC_playerBD[ iLocalPart ].iClientBitSet, PBBOOL_ANY_SPECTATOR )
									SET_BIT( MC_playerBD[ iLocalPart ].iClientBitSet2, PBBOOL2_I_AM_SPECTATOR_WATCHING_MOCAP )
									CPRINTLN(DEBUG_MIKE, "[RCC MISSION][RUN_MOCAP_CUTSCENE] PBBOOL2_I_AM_SPECTATOR_WATCHING_MOCAP = TRUE")
								ENDIF

								g_sTransitionSessionData.bMissionCutsceneInProgress = TRUE
								PRINTLN("[RCC MISSION][AMEC][RUN_MOCAP_CUTSCENE] Setting bMissionCutsceneInProgress to TRUE.")
								
								IF eCutType = FMMCCUT_ENDMOCAP
									SET_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_REGISTERED_ENTITIES_FOR_END_MOCAP_PASS_OVER)
								ENDIF
								
								// [FIX_2020_CONTROLLER] - EndMOCAP Passover Issue - Do not do this on the 4A Casino Heist Missions - url:bugstar:6198712
								IF (IS_A_STRAND_MISSION_BEING_INITIALISED() AND eCutType = FMMCCUT_ENDMOCAP)
								AND g_sMPTunables.iCasinoHeistRootContentID[ENUM_TO_INT(ciCASINO_HEIST_MISSION_DIRECT_STAGE_4A)] != g_FMMC_STRUCT.iRootContentIDHash
								AND g_sMPTunables.iCasinoHeistRootContentID[ENUM_TO_INT(ciCASINO_HEIST_MISSION_SUBTERFUGE_STAGE_4A)] != g_FMMC_STRUCT.iRootContentIDHash
								AND g_sMPTunables.iCasinoHeistRootContentID[ENUM_TO_INT(ciCASINO_HEIST_MISSION_STEALTH_STAGE_4A)] != g_FMMC_STRUCT.iRootContentIDHash
									PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] SET_MISSION_CONTROLLER_READY_TO_START_CUTSCENE called")
									SET_MISSION_CONTROLLER_READY_TO_START_CUTSCENE()
								ELSE
									g_sTransitionSessionData.sStrandMissionData.bCutsceneEntityPassoverMustWaitForAllClients = TRUE
								ENDIF
																
								IF iCutsceneToUse != -1		
								
									IF NOT ARE_STRINGS_EQUAL(sMocapName, "MPH_PAC_FIN_EXT")
									AND NOT ARE_STRINGS_EQUAL( sMocapName, "MPH_PRI_EMP_EXT" ) // NOTE(Owain): B* 2196291
										REMOVE_PLAYER_FROM_VEHICLE_AT_CUTSCENE_START(iCutsceneToUse, FALSE)
									ENDIF
									
									IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitset,ci_CSBS_WarpPlayersStart)
									AND NOT IS_VECTOR_ZERO(vStartPos)
									AND fStartHeading != ciINVALID_CUTSCENE_WARP_HEADING
									AND NOT IS_PLAYER_SCTV( LocalPlayer )
										
										IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(LocalPlayerPed), vStartPos) > 100.0
											PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] WARPED AT THE BEGINNING OF THE CUTSCENE TO: Position = ", vStartPos," , Heading = ", fStartHeading)
											NET_WARP_TO_COORD( vStartPos,		//
															fStartHeading,	//
															!SHOULD_CUTSCENE_REMOVE_PLAYER_FROM_VEHICLE_AT_START( iCutsceneToUse, FALSE ),	// bKeepVehicle
															FALSE,																	// bLeavePedBehind
															FALSE,																	// bDontSetCameraBehindPlayer
															FALSE,																	// bKillLeftBehindPed
															TRUE,																	// bKeepPortableObjects
															TRUE )																	// bDoQuickWarp
										ENDIF
									ENDIF
								ENDIF
							
							#IF IS_NEXTGEN_BUILD
								IF ARE_STRINGS_EQUAL( sMocapName, "MPH_HUM_EMP_EXT" )
									IF IS_BIT_SET(iLocalBoolCheck9, LBOOL9_DANGER_ZONE_TRIGGERED)
										IF PREPARE_MUSIC_EVENT("MP_MC_DZ_FADE_OUT_RADIO")
											
											IF TRIGGER_MUSIC_EVENT( "MP_MC_DZ_FADE_OUT_RADIO" )
												PRINTLN("[RUN_MOCAP_CUTSCENE] Music event MP_MC_DZ_FADE_OUT_RADIO has been triggered")
											ELSE
												PRINTLN("[RUN_MOCAP_CUTSCENE] Unable to trigger music event MP_MC_DZ_FADE_OUT_RADIO")
											ENDIF
									
											// If cutscene at the end of humane labs, stop the dangerzone audio scene
											IF IS_AUDIO_SCENE_ACTIVE("DLC_HEISTS_BIOLAB_EMP_DANGERZONE_SCENE")
												PRINTLN("[RUN_MOCAP_CUTSCENE] STOP_AUDIO_SCENE(DLC_HEISTS_BIOLAB_EMP_DANGERZONE_SCENE)")
												STOP_AUDIO_SCENE("DLC_HEISTS_BIOLAB_EMP_DANGERZONE_SCENE") 
											ENDIF
											
											SET_BIT( iLocalBoolCheck10, LBOOL10_TRIGGERED_DANGER_ZONE_FADE_OUT )
										ELSE
											PRINTLN("[RUN_MOCAP_CUTSCENE] Unable to prepare music event MP_MC_DZ_FADE_OUT_RADIO")
										ENDIF
									ENDIF									
								ENDIF
							#ENDIF
								
								IF eCutType = FMMCCUT_ENDMOCAP
								AND MC_ServerBD.iLastCompletedGoToLocation > -1
								AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[MC_ServerBD.iLastCompletedGoToLocation].iLocBS3, ciLoc_BS3_UseFirstAdditionalSpawnPosAsEndCutscenePos)	
									VECTOR vCoord
									vCoord = GET_CUTSCENE_COORDS(iCutsceneTeam, FMMCCUT_ENDMOCAP)
									PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] start_cutscene_at_coords called with Coord: ", vCoord)				
									
									REQUEST_CUT_FILE(sMocapName)
									
									IF HAS_CUT_FILE_LOADED(sMocapName)
										START_CUTSCENE_AT_COORDS(vCoord)
										
										INT iLoopMax
										iLoopMax = GET_CUT_FILE_CONCAT_COUNT(sMocapName)
										IF iLoopMax > 0
											PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] Cutscene has ", iLoopMax, " concats.")
											INT i
											FOR i = 0 TO iLoopMax-1
												PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] Setting Concat ", i, " to vCoord: ", vCoord)
												SET_CUTSCENE_ORIGIN(vCoord, 0.0, i)
											ENDFOR
											REMOVE_CUT_FILE(sMocapName)
										ELSE
											PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] Cutscene has no concats")
										ENDIF
									ELSE
										PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] Waiting for Cut File to load....")
										RETURN FALSE
									ENDIF
										
								ELIF MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_SIMEON
								AND eCutType = FMMCCUT_ENDMOCAP
									PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] start_cutscene_at_coords called")
									start_cutscene_at_coords( <<-21.158, -1083.188, 25.599>> ) //need to move the cutscene by + 5cm on the y axis to stop the garage door having portal issues where it is going between the interior portal and the exterior portal when being animated during the mocap.	
								
								elif are_strings_equal(sMocapName, "mph_pri_sta_mcs1") 
									
									CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][RUN_MOCAP_CUTSCENE] mph_pri_sta_mcs1")
									CLEAR_PRINTS()
									
									START_CUTSCENE()
									
									set_bit(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_PLAYED_CONTAINER_CUTSCENE)
								
								ELIF ARE_STRINGS_EQUAL( sMocapName, "MPH_PRI_FIN_MCS2" )
									PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] start_cutscene_at_coords called for MPH_PRI_FIN_MCS2")
									start_cutscene_at_coords( << 2907.0, -842.0, 617.0 >> )
									//Ensuring script cams are off for bug url:bugstar:2093383
									destroy_all_cams()
									render_script_cams(false, false)
								ELIF ARE_STRINGS_EQUAL( sMocapName, "MPH_PRI_FIN_EXT" )
									PICK_CLOSEST_PRISON_FINALE_CUTSCENE_LOCATION_AND_PLAY()	
								ELIF ARE_STRINGS_EQUAL( sMocapName, "MPH_TUT_EXT"  ) 
									PICK_CLOSEST_FLEECA_FINALE_CUTSCENE_LOCATION_AND_PLAY()	
								
								ELIF IS_THIS_MOCAP_IN_AN_APARTMENT( sMocapname )
									CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][RUN_MOCAP_CUTSCENE] moving station cutscene" )
									Move_Heist_Cutscene_To_Heist_Apartment( iMocapCutsceneConcatParts, MC_ServerBD.iHeistPresistantPropertyID ) //g_iHeistPropertyID = MC_ServerBD.iHeistPresistantPropertyID 
								ELIF  ( MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_HEIST 
									OR MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_HUM_KEY_EXT
									OR MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_TUT_MID
									OR MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_PRI_UNF_EXT )
									AND eCutType = FMMCCUT_ENDMOCAP 
					
									Move_Heist_Cutscene_To_Heist_Apartment(iMocapCutsceneConcatParts , MC_ServerBD.iHeistPresistantPropertyID) //g_iHeistPropertyID = MC_ServerBD.iHeistPresistantPropertyID 
									
									PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] Move_Heist_Cutscene_To_Heist_Apartment called")
								ELIF (IS_A_STRAND_MISSION_BEING_INITIALISED() AND eCutType = FMMCCUT_ENDMOCAP)
									CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][RUN_MOCAP_CUTSCENE] START_CUTSCENE not called because it's a strand mission cut scene called")
								ELSE
									// 2153812
									IF ARE_STRINGS_EQUAL( sMocapName, "mph_pac_fin_ext" ) 
										SET_SRL_LONG_JUMP_MODE( TRUE )
									ENDIF
									
									CLEAR_PRINTS()
									
									IF NOT IS_STRING_EMPTY(sMocapName)//url:bugstar:4628425
										CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][RUN_MOCAP_CUTSCENE] START_CUTSCENE called")
										START_CUTSCENE()
									ELSE
										CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][RUN_MOCAP_CUTSCENE] Not calling START_CUTSCENE, but that's okay if this is placeholder.")
									ENDIF
								ENDIF
								
								IF IS_LOCAL_PLAYER_USING_DRONE()
									SET_DRONE_FORCE_EXTERNAL_CLEANING_UP(TRUE)
								ENDIF
								
								IF ARE_STRINGS_EQUAL( sMocapName, "MPH_PRI_FIN_MCS2" )
								 	SET_PED_INTERIOR_WALLA_DENSITY(0,1)
									PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] SET_PED_INTERIOR_WALLA_DENSITY(0,1) called: ")
								ENDIF
								
								IF eCutType = FMMCCUT_ENDMOCAP
								 	CLEANUP_FLARE_PROPS_FOR_CUTSCENE()
								ENDIF
								
								//If we're on the end mo cap then set that we're over so STCTV doesn't clean up
								IF eCutType = FMMCCUT_ENDMOCAP
								AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
									SET_MISSION_OVER_AND_ON_LB_FOR_SCTV()
								ENDIF
								
								// For the post mission cleanup so we know to spawn the player in the alley.
								IF (MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_GERALD)
									g_TransitionSessionNonResetVars.sPostMissionCleanupData.bCompletedGeraldMission = TRUE
								ENDIF
								
								IF IS_A_STRAND_MISSION_BEING_INITIALISED()
									INITIALISE_CELEBRATION_SCREEN_DATA(sCelebrationData)
								ENDIF
								CLEAR_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bStartedCelebrationBeforeCutsceneEnded)
								
								//Store the female ped feet variation
								IF STORE_FEMALE_PED_FEET( LocalPlayerPed)
									//SET_FEMALE_PED_FEET_TO_FLATS(LocalPlayerPed)
								ENDIF
								IF eCutType = FMMCCUT_ENDMOCAP
								AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
									FREEZE_TOD_FOR_CUTSCENE()
								ENDIF
								
								//Give local player a parachute for cutscene.
								IF ARE_STRINGS_EQUAL(sMocapName, "MPH_PRI_FIN_MCS2" )
									IF NOT HAS_PED_GOT_WEAPON(LocalPlayerPed, GADGETTYPE_PARACHUTE)
										GIVE_WEAPON_TO_PED(LocalPlayerPed, GADGETTYPE_PARACHUTE, 1, TRUE, TRUE)
									ENDIF
									SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_HAND, 1, 0)
								ENDIF
								
								// 2161860, if the player was made inivisible for the warp make them visible again now
								IF NOT IS_ENTITY_VISIBLE(PLAYER_PED_ID())
									if not is_player_spectating(player_id())
										SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
									endif 
								ENDIF
								
								SET_REGISTERED_ENTITIES_VISIBLE_FOR_CUTSCENE(eCutType, iCutsceneToUse)
								
								CLEAR_BIT(iLocalBoolCheck28, LBOOL28_END_MOCAP_TRIED_TURNING_OFF_VOLATOL)
								
								IF IS_CUTSCENE_PLAYING()
									CPRINTLN( DEBUG_CONTROLLER,"[RCC MISSION][RUN_MOCAP_CUTSCENE] IS_CUTSCENE_PLAYING - true moving to 1")			
								ENDIF
								
								SET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS(MOCAPRUNNING_STAGE_1)
							ELSE
								CPRINTLN( DEBUG_CONTROLLER,"[RCC MISSION][RUN_MOCAP_CUTSCENE] waiting for cutscene timer")
							ENDIF
						ELSE
							CPRINTLN( DEBUG_CONTROLLER,"[RCC MISSION] waiting for CLONE_MOCAP_PLAYERS")
						ENDIF // End of IF HAVE_MOCAP_START_CONDITIONS_BEEN_MET()
					ELSE
						CPRINTLN( DEBUG_CONTROLLER,"[RCC MISSION][RUN_MOCAP_CUTSCENE] waiting for Conor's apartment cutscene to complete")
						PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] CURRENT NETWORK TIMER: ", NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()))
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					IF NOT HAS_CUTSCENE_LOADED()
						IF NOT IS_STRING_NULL_OR_EMPTY(sMocapName)
							IF eCutType = FMMCCUT_MOCAP
							AND g_FMMC_STRUCT.sMocapCutsceneData[ iCutsceneToUse ].iVariationBS != 0
								CUTSCENE_SECTION cutSectionFlags
								cutSectionFlags = GET_CUTSCENE_SECTION_FLAGS(iCutsceneToUse)						
								PRINTLN("[RCC MISSION] HANDLE_STREAMING_MOCAP - REQUEST_CUTSCENE_WITH_PLAYBACK_LIST (3) with creator cutscene section flags: ", ENUM_TO_INT(cutSectionFlags))						
								REQUEST_CUTSCENE_WITH_PLAYBACK_LIST( sMocapName, cutSectionFlags)
							ELSE
								REQUEST_CUTSCENE(sMocapName)
							ENDIF
						ENDIF
						CPRINTLN( DEBUG_CONTROLLER,"[RCC MISSION][RUN_MOCAP_CUTSCENE] waiting for HAS_CUTSCENE_LOADED")
					ELIF NOT IS_BIT_SET( iLocalBoolCheck2, LBOOL2_CUTSCENE_STREAMED )
						CPRINTLN( DEBUG_CONTROLLER,"[RCC MISSION][RUN_MOCAP_CUTSCENE] waiting for LBOOL2_CUTSCENE_STREAMED ")
					ENDIF
					#ENDIF
					
					IF NOT HAS_NET_TIMER_STARTED( streamCSFailsafeTimer )
						CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][RUN_MOCAP_CUTSCENE] Starting the cutscene failsafe timer." )
						START_NET_TIMER( streamCSFailsafeTimer )
					ENDIF
					
					PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] waiting for HAS_CUTSCENE_LOADED or LBOOL2_CUTSCENE_STREAMED - CURRENT NETWORK TIMER: ", NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()))
				ENDIF
			ELSE
				CPRINTLN( DEBUG_CONTROLLER,"[RCC MISSION] waiting for ARE_TEAM_CUTSCENE_PLAYERS_SELECTED")
				PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] waiting for  ARE_TEAM_CUTSCENE_PLAYERS_SELECTED - CURRENT NETWORK TIMER: ", NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()))
			ENDIF
			
		BREAK
		
		CASE MOCAPRUNNING_STAGE_1
			// [FIX_2020_CONTROLLER] - EndMOCAP Passover Issue - ONLY do this on the 4A Casino Heist Missions - url:bugstar:6198712
			// The issue here was that the mission host was getting to the Start_Cutscene in FMMC_Launcher.sc before remote clients has registered entities via: REGISTER_CUTSCENE_ENTITIES.
			// This made the NetID's unusable for remote clients and therefore passing over null values.
			IF (IS_A_STRAND_MISSION_BEING_INITIALISED() AND eCutType = FMMCCUT_ENDMOCAP)
			AND (g_sMPTunables.iCasinoHeistRootContentID[ENUM_TO_INT(ciCASINO_HEIST_MISSION_DIRECT_STAGE_4A)] = g_FMMC_STRUCT.iRootContentIDHash
			OR g_sMPTunables.iCasinoHeistRootContentID[ENUM_TO_INT(ciCASINO_HEIST_MISSION_SUBTERFUGE_STAGE_4A)] = g_FMMC_STRUCT.iRootContentIDHash
			OR g_sMPTunables.iCasinoHeistRootContentID[ENUM_TO_INT(ciCASINO_HEIST_MISSION_STEALTH_STAGE_4A)] = g_FMMC_STRUCT.iRootContentIDHash)
				IF ARE_ALL_PLAYERS_SYNCED_FOR_ENDMOCAP_PASSOVER_ENTITY_REGISTRATION()
					PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] SET_MISSION_CONTROLLER_READY_TO_START_CUTSCENE called (special)")
					SET_MISSION_CONTROLLER_READY_TO_START_CUTSCENE()
				ELSE					
					PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] Waiting to call SET_MISSION_CONTROLLER_READY_TO_START_CUTSCENE some players are not ready.")
					RETURN FALSE
				ENDIF
			ENDIF
			
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][RUN_MOCAP_CUTSCENE] case 1" )

			IF NOT IS_BIT_SET( MC_playerBD[ iLocalPart].iClientBitSet, PBBOOL_ANY_SPECTATOR )
	       		IF IS_LOCAL_PLAYER_PED_IN_CUTSCENE()
	       			MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(FALSE,FALSE,FALSE,TRUE, FALSE)
				ELSE
					MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(FALSE,FALSE,FALSE,TRUE, FALSE) //Set invisible param to false was causing b*2166616
				ENDIF
			ELSE
				MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(FALSE,TRUE,FALSE,TRUE, FALSE)
			ENDIF

			IF NOT bLocalPlayerOK
				PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] - NOT bLocalPlayerOK")
				SET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS(MOCAPRUNNING_STAGE_2)
				RETURN FALSE
			ENDIF
			
			IF eCutType = FMMCCUT_ENDMOCAP
			AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
				FREEZE_TOD_FOR_CUTSCENE()
			ENDIF
			
			IF NOT IS_BIT_SET(iLocalBoolCheck5, LBOOL5_MOCAP_REMOVED_LOCAL_PLAYER_MASK)
				IF CAN_REMOVE_MASKS_FOR_HEIST_CUTSCENE(sMocapName, FALSE, eCutType, iCutsceneToUse)
					REMOVE_MASKS_AND_REBREATHERS_FOR_CUTSCENES(LocalPlayerPed, LocalPlayer, INT_TO_ENUM(MP_OUTFIT_MASK_ENUM, GET_PLAYER_CHOSEN_MASK(LocalPlayer)))
					SET_BIT(iLocalBoolCheck5, LBOOL5_MOCAP_REMOVED_LOCAL_PLAYER_MASK)
				ENDIF
			ENDIF

			IF IS_CUTSCENE_PLAYING()
			#IF IS_DEBUG_BUILD
			OR (MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_PLACE_HOLDER AND eCutType = FMMCCUT_ENDMOCAP)
			#ENDIF
				
				IF MC_playerBD[iPartToUse].iteam = iScriptedCutsceneTeam
				AND iCutsceneToUse != -1
					IF NOT IS_BIT_SET( MC_serverBD.iObjectiveMidPointBitset[ MC_playerBD[iPartToUse].iteam], MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] )
						IF g_FMMC_STRUCT.sMocapCutsceneData[ iCutsceneToUse ].iMidpointTimer != 0
							IF NOT HAS_NET_TIMER_STARTED(tdCutsceneMidpoint)
								CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][RUN_MOCAP_CUTSCENE] - Case 1 - Initialising tdCutsceneMidpoint, cutscene midpoint timer" )
								REINIT_NET_TIMER(tdCutsceneMidpoint)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF ANIMPOSTFX_IS_RUNNING("MP_Celeb_Preload_Fade")
					ANIMPOSTFX_STOP("MP_Celeb_Preload_Fade")
					PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] - [SAC] - stopped MP_Celeb_Preload_Fade for mocap cutscene.")
				ENDIF
								
					
					//Avoid any 1 frame pops.
					LISTEN_FOR_CUTSCENE_EVENTS( sMocapName )
					HANDLE_CUTSCENE_ENTITIES( sMocapName )
					
				
				IF eCutType != FMMCCUT_ENDMOCAP
					//Clear area of objects first - 2158121
					CLEAR_AREA_OF_OBJECTS(vStartPos, 100, CLEAROBJ_FLAG_FORCE )
					CLEAR_AREA_OF_PROJECTILES(vStartPos, 100, TRUE) //2159381
					PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] DEFAULT - CLEARING AREA OF PROJECTILES AT <<", vStartPos.X, ", ", vStartPos.Y, ", ", vStartPos.Z, ">> with radius: 100")
				ELSE
					PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] END OF MISSION CUTSCENE NOT DOING DEFAULT CLEAR ATTEMPTING SPECIFIC...")
				ENDIF
				
				//Clear specified projectile area from creator variables
				IF eCutType = FMMCCUT_ENDMOCAP
					IF g_FMMC_STRUCT.sEndMocapSceneData.fClearRadius > 0.0
						CLEAR_AREA_OF_PROJECTILES(g_FMMC_STRUCT.sEndMocapSceneData.vClearCoOrds, g_FMMC_STRUCT.sEndMocapSceneData.fClearRadius, TRUE)
						PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] CLEARING AREA OF PROJECTILES AT <<", g_FMMC_STRUCT.sEndMocapSceneData.vClearCoOrds.X, ", ", g_FMMC_STRUCT.sEndMocapSceneData.vClearCoOrds.Y, ", ", g_FMMC_STRUCT.sEndMocapSceneData.vClearCoOrds.Z, ">> with radius: ", g_FMMC_STRUCT.sEndMocapSceneData.fClearRadius)
					ELSE
						PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] NO CLEAR AREA SETUP IN CREATOR!")
					ENDIF
				ELSE
					IF iCutsceneToUse > -1
						IF g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].fClearRadius > 0.0
							CLEAR_AREA_OF_PROJECTILES(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].vClearCoOrds, g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].fClearRadius, TRUE)
							PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] CLEARING AREA OF PROJECTILES AT <<", g_FMMC_STRUCT.sEndMocapSceneData.vClearCoOrds.X, ", ", g_FMMC_STRUCT.sEndMocapSceneData.vClearCoOrds.Y, ", ", g_FMMC_STRUCT.sEndMocapSceneData.vClearCoOrds.Z, ">> with radius: ", g_FMMC_STRUCT.sEndMocapSceneData.fClearRadius)
						ELSE
							PRINTLN("[RCC MISSION] NO CLEAR AREA SETUP IN CREATOR!")
						ENDIF
					ENDIF
				ENDIF
				
				CUTSCENE_INIT_SPECIAL_CASE( sMocapName, bCreateHeistDoors )
				
				IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()			
					IF IS_NET_PLAYER_OK(PLAYER_ID())	
						//Any entities created during a cutscene should be networked
						IF NOT NETWORK_IS_PLAYER_IN_MP_CUTSCENE(PLAYER_ID())
							NETWORK_SET_IN_MP_CUTSCENE(TRUE, TRUE) 
							PRINTLN("[TS] [MSRAND][RUN_MOCAP_CUTSCENE] - NETWORK_SET_IN_MP_CUTSCENE(TRUE, TRUE)")
						ELSE
							PRINTLN("[TS] [MSRAND][RUN_MOCAP_CUTSCENE] - Player already in MP cutscene")
						ENDIF
						SET_NETWORK_CUTSCENE_ENTITIES(TRUE)
						CPRINTLN(DEBUG_MIKE, "[RCC MISSION][RUN_MOCAP_CUTSCENE] NETWORK_SET_IN_MP_CUTSCENE(TRUE, TRUE)")
					ELSE
						PRINTLN("[TS] [MSRAND][RUN_MOCAP_CUTSCENE] - player NOT OK not settting MP_CUTSCENE")
					ENDIF
				ENDIF

				int i

				for i = 0 to (FMMC_MAX_CUTSCENE_PLAYERS - 1)
					if does_entity_exist(MocapPlayerPed[i])
						if not is_ped_injured(MocapPlayerPed[i])
							IF i < GET_MOCAP_NUMBER_OF_PLAYERS(sMocapName)
								PRINTLN("[MSRAND] [PED CUT][RUN_MOCAP_CUTSCENE] * - SET_ENTITY_VISIBLE(MocapPlayerPed[", i, "], TRUE)")
								set_entity_visible(MocapPlayerPed[i], true)
							ENDIF
							
							IF DOES_ENTITY_EXIST(MocapPlayerWeaponObjects[i])
								SET_ENTITY_VISIBLE(MocapPlayerWeaponObjects[i], TRUE)
							ENDIF
						endif 
					endif 
				endfor
				
				IF DOES_ENTITY_EXIST(playersWeaponObject)
					SET_ENTITY_VISIBLE(playersWeaponObject, TRUE)
				ENDIF
					
				SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisableVoiceDrivenMouthMovement, TRUE)
				
				CLEAR_TIMECYCLE_MODIFIER()	//Added for Bug 2102303

				request_assets_for_specific_cutscene()
				
				delete_assets_for_specific_cutscene()
				
				PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] MC_ServerBD.iEndCutscene = ", MC_ServerBD.iEndCutscene)
				
				destroy_all_cams()
				render_script_cams(false, false)
				
				DO_RUN_MOCAP_CHECKS()
				
				garage_door_audio_system()
				
				//Any entities created during a cutscene should be networked (should already have been called, this is a backup)
				IF NETWORK_IS_IN_MP_CUTSCENE() // url:bugstar:2238772
					SET_NETWORK_CUTSCENE_ENTITIES(TRUE)
					PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] Calling SET_NETWORK_CUTSCENE_ENTITIES(TRUE)")
				ELSE
					PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] Not calling SET_NETWORK_CUTSCENE_ENTITIES(TRUE) as NETWORK_IS_IN_MP_CUTSCENE() is false")
				ENDIF
				
				//Check for pre mocap cinematic camera
				STOP_PRE_MOCAP_SPLASH( sMocapName )
								
				//populating results early so LB is correct
				IF eCutType = FMMCCUT_ENDMOCAP
					POPULATE_RESULTS()
				ENDIF
				
				TOGGLE_PAUSED_RENDERPHASES(TRUE)
				
				PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] IS_CUTSCENE_PLAYING - true moving to 2")
				
				SET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS(MOCAPRUNNING_STAGE_2)
			ELSE
			
				IF NOT HAS_CUTSCENE_LOADED() 
				AND NOT IS_STRING_NULL_OR_EMPTY( sMocapName )
					PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] unloaded mocap after starting, re-requesting cutscene: ", sMocapName)
					
					IF eCutType = FMMCCUT_MOCAP
					AND g_FMMC_STRUCT.sMocapCutsceneData[ iCutsceneToUse ].iVariationBS != 0
						CUTSCENE_SECTION cutSectionFlags
						cutSectionFlags = GET_CUTSCENE_SECTION_FLAGS(iCutsceneToUse)						
						PRINTLN("[RCC MISSION] HANDLE_STREAMING_MOCAP - REQUEST_CUTSCENE_WITH_PLAYBACK_LIST (3) with creator cutscene section flags: ", ENUM_TO_INT(cutSectionFlags))						
						REQUEST_CUTSCENE_WITH_PLAYBACK_LIST( sMocapName, cutSectionFlags)
					ELSE
						REQUEST_CUTSCENE( sMocapName )
					ENDIF

				ELSE
					PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] mocap is not playing ", sMocapName)
				ENDIF
				
				PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] waiting for IS_CUTSCENE_PLAYING")
				IF NOT HAS_NET_TIMER_STARTED(mocapFailsafe)
					REINIT_NET_TIMER(mocapFailsafe)
				ELSE
					IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(mocapFailsafe) > 30000
						SET_BIT(ilocalboolcheck,LBOOL_HIT_MOCAP_FAIL_SAFE)
						PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] waiting for IS_CUTSCENE_PLAYING - FAIL SAFE TIMER!!!!!!")
						SET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS(MOCAPRUNNING_STAGE_2)
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE MOCAPRUNNING_STAGE_2
			SET_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_REACHED_CUT_PROCESS_SHOTS)
			SET_BIT(iLocalBoolCheck31, LBOOL31_PRIZED_VEHICLE_SHOULD_HIDE)
			
			IF MC_playerBD[iPartToUse].iteam = iScriptedCutsceneTeam
			AND iCutsceneToUse != -1
				IF NOT IS_BIT_SET( MC_serverBD.iObjectiveMidPointBitset[ MC_playerBD[iPartToUse].iteam  ], MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] )
					
					IF IS_STRING_NULL_OR_EMPTY(sMocapName)
					OR NOT ARE_STRINGS_EQUAL(sMocapName, "MPH_PRI_FIN_MCS2")
						IF g_FMMC_STRUCT.sMocapCutsceneData[ iCutsceneToUse ].iMidpointTimer != 0
							IF NOT HAS_NET_TIMER_STARTED(tdCutsceneMidpoint)
								CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][RUN_MOCAP_CUTSCENE] - Case 2 - Initialising tdCutsceneMidpoint, cutscene midpoint timer" )
								REINIT_NET_TIMER(tdCutsceneMidpoint)
							ELSE
								IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdCutsceneMidpoint) >= (g_FMMC_STRUCT.sMocapCutsceneData[ iCutsceneToUse ].iMidpointTimer * 1000)
									CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] RUN_MOCAP_CUTSCENE, Case 2 - Midpoint timer has passed value in creator (", g_FMMC_STRUCT.sMocapCutsceneData[ iCutsceneToUse ].iMidpointTimer, " seconds), broadcast midpoint!" )
									BROADCAST_OBJECTIVE_MID_POINT(MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam],MC_playerBD[iPartToUse].iteam)
									REINIT_NET_TIMER(tdCutsceneMidpoint)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF NOT HAS_NET_TIMER_STARTED(tdCutsceneMidpoint)
							CPRINTLN(DEBUG_CONTROLLER,"[RCC MISSION][RUN_MOCAP_CUTSCENE] - Case 2 - Initialising tdCutsceneMidpoint, cutscene midpoint timer for MPH_PRI_FIN_MCS2 cutscene")
							REINIT_NET_TIMER(tdCutsceneMidpoint)
						ELSE
							IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdCutsceneMidpoint) >= 2000
								BROADCAST_OBJECTIVE_MID_POINT(MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam],MC_playerBD[iPartToUse].iteam)
								
								NETWORK_INDEX niPlane
								
								niPlane = GET_CUTSCENE_NET_ID_WITH_HANDLE("PLANE", iCutsceneToUse)
								
								IF NETWORK_DOES_NETWORK_ID_EXIST(niPlane)
									
									CPRINTLN(DEBUG_CONTROLLER,"[RCC MISSION][RUN_MOCAP_CUTSCENE] - Cutscene MPH_PRI_FIN_MCS2 is running, send out that we should get rid of plane network index ",NATIVE_TO_INT(niPlane))
									
									IF NOT bIsLocalPlayerHost
										BROADCAST_FMMC_PRI_FIN_MCS2_PLANE_CLEANUP(niPlane)
									ELSE
										CLEANUP_PRI_FIN_MCS2_PLANE(niPlane)
									ENDIF
								ENDIF
								
								REINIT_NET_TIMER(tdCutsceneMidpoint)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(iLocalBoolCheck4, LBOOL4_MOCAP_START_FADE_IN_DONE)
			
				// Don't need the fade
				IF IS_SCREEN_FADED_IN()
				OR IS_SCREEN_FADING_IN()
				
					PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] - Mocap fade start - No fade in to perform, sealed off logic")
					SET_BIT(iLocalBoolCheck4, LBOOL4_MOCAP_START_FADE_IN_DONE)

				// need the fade
				ELIF IS_CUTSCENE_PLAYING()
				AND GET_CUTSCENE_TIME() > 0 
				
					//we did a warp, hide the screen until the cutscene is ready
					IF IS_BIT_SET(iLocalBoolCheck2,LBOOL2_CUTSCENE_WARP_DONE)
						IF NOT (IS_THIS_MOCAP_IN_AN_APARTMENT( sMocapName )
						AND IS_LOCAL_PLAYER_ANY_SPECTATOR())
							PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] - Mocap fade start - warp complete - Calling DO_SCREEN_FADE_IN")
							DO_SCREEN_FADE_IN(GET_MOCAP_CUTSCENE_FADE_IN_TIME(sMocapName))
							SET_BIT(iLocalBoolCheck4, LBOOL4_MOCAP_START_FADE_IN_DONE)
						ENDIF
					ENDIF

					//b*2193331
					IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
					AND NOT IS_THIS_MOCAP_IN_AN_APARTMENT( sMocapName )
						PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE]- Mocap fade start - Specator not in apartment - Called DO_SCREEN_FADE_IN")
						DO_SCREEN_FADE_IN(GET_MOCAP_CUTSCENE_FADE_IN_TIME(sMocapName))
						SET_BIT(iLocalBoolCheck4, LBOOL4_MOCAP_START_FADE_IN_DONE)
					ENDIF	
					
				ENDIF

			ELIF NOT IS_BIT_SET(iLocalBoolCheck5, LBOOL5_MOCAP_END_FADE_OUT_DONE)
				
				IF IS_SCREEN_FADED_OUT()
				OR IS_SCREEN_FADING_OUT()
				OR NOT bShouldFadeAtEnd
				
					PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] - Mocap fade end - No fade out to perform, sealed off logic, bShouldFadeAtEnd: ", BOOL_TO_STRING(bShouldFadeAtEnd))
					SET_BIT(iLocalBoolCheck5, LBOOL5_MOCAP_END_FADE_OUT_DONE)
				
				ELIF IS_CUTSCENE_PLAYING()
				AND GET_CUTSCENE_TIME() >= GET_CUTSCENE_END_TIME() - 1000

					PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] - Mocap fade end - bShouldFadeAtEnd = TRUE")
					
					IF IS_SCREEN_FADED_IN() OR IS_SCREEN_FADING_IN()
						PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] - Mocap fade end - bShouldFadeAtEnd = TRUE - Called DO_SCREEN_FADE_OUT")
						DO_SCREEN_FADE_OUT(MOCAP_END_FADE_DURATION)
					ENDIF
					
					SET_BIT(iLocalBoolCheck5, LBOOL5_MOCAP_END_FADE_OUT_DONE)
				ENDIF
				
			ENDIF

			//If player has warped over stop cinematic cam
			STOP_PRE_MOCAP_SPLASH( sMocapName )
			
			//Clear area one frame after START_CUTSCENE to fix things like 2089738
			IF bCutsceneAreaClearedLate = FALSE
			AND IS_CUTSCENE_PLAYING()
			AND GET_CUTSCENE_TIME() > 1200			
				//Clear the area for most scenes.
				IF bClearAreaAndParticles	
				
				/* 2156084: Changed this up for a cleaner new version below because;
					- some clear areas being called multiple times in a single frame here
					- also these should NOT be broadcast, this logic runs on everyone's machine, broadcasting will make things disappear on other machines if their cutscene players late
					- object, cops, peds and projectiles are covered by the CLEAR_AREA call any way, removing
				*/

					// 2156084: see comment above
					IF eCutType = FMMCCUT_ENDMOCAP
						CLEAR_CUTSCENE_AREA(g_FMMC_STRUCT.sEndMocapSceneData.fClearRadius, g_FMMC_STRUCT.sEndMocapSceneData.vClearCoOrds,g_FMMC_STRUCT.sEndMocapSceneData.vReplacementArea)
						PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] - End of mission cutscene CLEAR_AREA")
					ELSE
						IF iCutsceneToUse != (-1)
							CLEAR_CUTSCENE_AREA(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].fClearRadius, g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].vClearCoOrds,g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].vReplacementArea)
							PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] - NON End of mission cutscene CLEAR_AREA")
						ELSE
							CLEAR_AREA(vStartPos, 100.0, TRUE, TRUE, DEFAULT, FALSE)
							PRINTLN("[RCC MISSION] RUN_MOCAP_CUTSCENE() - Generical fall back CLEAR_AREA")
						ENDIF
					ENDIF					
					
					//b*2158106
					//REMOVE_PARTICLE_FX_IN_RANGE(vStartPos, 100.0)
					REMOVE_DECALS_IN_RANGE(vStartPos, 100)
					
					clear_specific_contact_mocap_area_of_vehicles()					
				ENDIF
				
				bCutsceneAreaClearedLate = TRUE
			ENDIF
			
			IF (NOT IS_A_STRAND_MISSION_BEING_INITIALISED() AND eCutType = FMMCCUT_ENDMOCAP)
				KILL_ALARM_SOUNDS()
			ENDIF	
			
			IF (IS_A_STRAND_MISSION_BEING_INITIALISED() AND eCutType = FMMCCUT_ENDMOCAP)
				PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] -  IS_A_STRAND_MISSION_BEING_INITIALISED skipping mocap going to case 4 ")
				SET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS(MOCAPRUNNING_STAGE_4)
				RETURN FALSE
			ENDIF
			
			IF eCutType = FMMCCUT_ENDMOCAP
				FREEZE_TOD_FOR_CUTSCENE()
			ENDIF
			
			garage_door_audio_system()
			
			LISTEN_FOR_CUTSCENE_EVENTS( sMocapName )
			HANDLE_CUTSCENE_ENTITIES( sMocapName )
			CUTSCENE_RUNNING_SPECIAL_CASE( sMocapName, iCutsceneTeam, iCutsceneToUse )
			
			CHECK_EARLY_CELEBRATION_SCREEN( eCutType = FMMCCUT_ENDMOCAP, sMocapName )
			
			START_CELEBRATION_SCREEN( bShowCelebrationScreen )

			HANDLE_MANUAL_EXIT_STATES( iCutsceneToUse, iCutsceneTeam, sMocapName, vEndPos, fEndHeading, eCutType)
			
			IF ARE_STRINGS_EQUAL(sMocapName, "SIL_PRED_MCS1")
				PROCESS_JUGGERNAUT_INTRODUCTION_CUTSCENE()
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA(TRUE)
			OR NOT bLocalPlayerOK
			OR IS_BIT_SET(ilocalboolcheck,LBOOL_HIT_MOCAP_FAIL_SAFE)		
			OR NOT IS_CUTSCENE_PLAYING() //Fail safe to progress
			OR IS_BIT_SET(iLocalBoolCheck11, LBOOL11_FORCE_CAMERA_EXIT_STATE) //Force exit as checked in HANDLE_MANUAL_EXIT_STATES
			OR (IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration) AND GET_TOGGLE_PAUSED_RENDERPHASES_STATUS() = FALSE AND HAS_TEAM_FAILED(MC_playerBD[iPartToUse].iteam))
				
				#IF IS_DEBUG_BUILD
					IF CAN_SET_EXIT_STATE_FOR_CAMERA(TRUE)
						PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] Finish Reasons: CAN_SET_EXIT_STATE_FOR_CAMERA(TRUE) is true")
					ENDIF
					IF NOT IS_CUTSCENE_PLAYING()
						PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] Finish Reasons: IS_CUTSCENE_PLAYING is false")
					ENDIF
					IF NOT bLocalPlayerOK
						PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] Finish Reasons: bLocalPlayerOK is false")
					ENDIF
					IF IS_BIT_SET(iLocalBoolCheck11, LBOOL11_FORCE_CAMERA_EXIT_STATE)
						PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] Finish Reasons: LBOOL11_FORCE_CAMERA_EXIT_STATE is set")
					ENDIF
					IF (IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration) AND GET_TOGGLE_PAUSED_RENDERPHASES_STATUS() = FALSE AND HAS_TEAM_FAILED(MC_playerBD[iPartToUse].iteam))
						PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] Finish Reasons:  ciCELEBRATION_BIT_SET_TriggerEndCelebration: ", IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration),
							" GET_TOGGLE_PAUSED_RENDERPHASES_STATUS(): ", GET_TOGGLE_PAUSED_RENDERPHASES_STATUS(),
							" HAS_TEAM_FAILED: ", HAS_TEAM_FAILED(MC_playerBD[iPartToUse].iteam))
					ENDIF
				#ENDIF				
				
				if ARE_STRINGS_EQUAL(sMocapName, "mph_tut_ext")
					set_game_pauses_for_streaming(true)
					freeze_entity_position(player_ped_id(), false)
					PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] set_game_pauses_for_streaming - mph_tut_ext")
				endif 
				
				IF ARE_STRINGS_EQUAL(sMocapName, "MPH_PAC_FIN_MCS0")
					PROCESS_BANK_CUTSCENE_FAILSAFE_WARP()					
				ENDIF
						
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][RUN_MOCAP_CUTSCENE] finished cutscene, cleaning up" )

				//NOTE(Owain): Don't clear this flag if we're in the keycodes delivery cutscene B* 2233889
				IF NOT ARE_STRINGS_EQUAL( sMocapName, "MPH_HUM_KEY_EXT" )
					// This will hide the HUD after a cutscene until godtext has been shown again
					CLEAR_BIT( iLocalBoolCheck9, LBOOL9_HAS_FIRST_OBJECTIVE_TEXT_BEEN_SHOWN )
				ENDIF

				USE_FAKE_MP_CASH(FALSE)	
			
				//No player exit state was hit in HANDLE_MANUAL_EXIT_STATES
				//This should only be hit in an instance where the player doesn't take part in a cutscene
				IF NOT IS_BIT_SET( iLocalBoolCheck8, LBOOL8_PLAYER_EXIT_STATE_CALLED)
					UPDATE_ON_PLAYER_EXIT_STATE(iCutsceneToUse, iCutsceneTeam, sMocapName, vEndPos, fEndHeading, TRUE)
					PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] NO PLAYER EXIT STATE WAS HIT IN HANDLE_MANUAL_EXIT_STATES DOING LOGIC IN CAMERA EXIT")
				ENDIF
				
				//Player exit states should be done in HANDLE_MANUAL_EXIT_STATES
				//This should rely solely on camera exit state
			
				CHECK_CUTSCENE_FOR_DROPOFF_OBJECTS( sMocapName )
				
				OVERRIDE_REMOTE_PLAYERS_COORDINATES(sMocapName, iCutsceneToUse)
			
				IF IS_BIT_SET( iLocalBoolCheck7, LBOOL7_TOGGLED_RENDERPHASES_FOR_DROPOFF )
					TOGGLE_RENDERPHASES( TRUE )
					CLEAR_BIT( iLocalBoolCheck7, LBOOL7_TOGGLED_RENDERPHASES_FOR_DROPOFF )
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][RUN_MOCAP_CUTSCENE] - LBOOL7_TOGGLED_RENDERPHASES_FOR_DROPOFF set false- 2")
				ENDIF
				
				CLEAR_APARTMENT_DROPOFF_SPINNER(3)

				// Special case camera for a car driving out of a cargo container - Prison Break Setup: Station Team 2
				IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()// ( NOT IS_PLAYER_SCTV( LocalPlayer ) AND NOT DID_I_JOIN_MISSION_AS_SPECTATOR() )
				AND bPlayerToUseOK
				and is_player_registered_for_cutscene(iCutsceneTeam) //bug 2231837 - don't create cam if player is not registered for cutscene

					IF( ARE_STRINGS_EQUAL( sMocapName, "MPH_PRI_STA_MCS1" ) )
						//third person only
						if is_allowed_independent_camera_modes() and get_cam_view_mode_for_context(cam_view_mode_context_in_vehicle) != cam_view_mode_first_person
						or ((not is_allowed_independent_camera_modes()) and (get_cam_view_mode_for_context(cam_view_mode_context_on_foot) != cam_view_mode_first_person))
						
							cutscenecam = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<871.636475,-2885.569092,19.007753>>,<<0.553330,-0.018307,-14.566518>>, 39.489414, true)
							
							if MC_serverBD_1.sFMMC_SBD.casco_index != -1
								vehicle_index casco_veh
							
								casco_veh = net_to_veh(MC_serverBD_1.sFMMC_SBD.niVehicle[MC_serverBD_1.sFMMC_SBD.casco_index])
							
								if does_entity_exist(casco_veh)
									point_cam_at_entity(cutscenecam, casco_veh, <<0.0, 0.0, 0.0>>)
								endif
								
							else 
								PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] MC_serverBD_1.sFMMC_SBD.casco_index does not exist")
							endif 
							
							shake_cam(cutscenecam, "HAND_SHAKE", 0.1)
							SET_CAM_CONTROLS_MINI_MAP_HEADING(cutscenecam, true)
							RENDER_SCRIPT_CAMS( TRUE, FALSE )
						
							PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] PRISON_BREAK_CARGO_SCENE - THIRD PERSON")
						else 
							PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] PRISON_BREAK_CARGO_SCENE - FIRST PERSON")
						endif 
						
						SET_BIT( iLocalBoolCheck7, LBOOL7_PRISON_BREAK_CARGO_SCENE_TRIGGERED )
						
						PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] casco cutscene cam rendered.")
					ENDIF
				ENDIF
				
				IF IS_CUTSCENE_PLAYING()
					STOP_CUTSCENE_IMMEDIATELY()
				ENDIF

				REMOVE_CUTSCENE()

				RESET_DROPOFF_GLOBALS(2)

				iCutRegisterPlayerNum = 0
				
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][RUN_MOCAP_CUTSCENE] - Clearing LBOOL7_SHOULD_HOLD_UP_MOCAP_FOR_CUTSCENE ")
				CLEAR_BIT( iLocalBoolCheck5, LBOOL5_MID_MISSION_MOCAP )	
				CLEAR_BIT( iLocalBoolCheck7, LBOOL7_SHOULD_HOLD_UP_MOCAP_FOR_CUTSCENE )
								
				CLEAR_BIT( iLocalBoolCheck9, LBOOL9_FORCE_STREAM_CUTSCENE)
				
				CLEAR_BIT(iLocalBoolCheck9, LBOOL9_PRE_MOCAP_CINE_TRIGGERED)
				
				CLEAR_BIT(iLocalBoolCheck11, LBOOL11_FORCE_CAMERA_EXIT_STATE)
				
				CLEAR_BIT(iLocalBoolCheck4, LBOOL4_MOCAP_START_FADE_IN_DONE)
				CLEAR_BIT(iLocalBoolCheck5, LBOOL5_MOCAP_END_FADE_OUT_DONE)
				
				IF eCutType != FMMCCUT_ENDMOCAP
				AND NOT bShowCelebrationScreen
					bGivePlayerControl = TRUE
				ENDIF
					
				CLEANUP_MP_CUTSCENE(IS_HELP_MESSAGE_BEING_DISPLAYED(), bGivePlayerControl)
				
				IF IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_WARP_TO_END_MOCAP_IN_PROGRESS)
					CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_WARP_TO_END_MOCAP_IN_PROGRESS)
					PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] MOCAP - iABI_WARP_TO_END_MOCAP_IN_PROGRESS - CLEARED - C")
				ENDIF

				g_sTransitionSessionData.bMissionCutsceneInProgress = FALSE
				PRINTLN("[RCC MISSION][AMEC][RUN_MOCAP_CUTSCENE] Setting bMissionCutsceneInProgress to FALSE.")

				IF( IS_BIT_SET( iLocalBoolCheck7, LBOOL7_WAS_VEH_RADIO_ENABLED_AT_CS_START ) )
					IF( NOT IS_ENTITY_DEAD( LocalPlayerPed ) )
						IF( IS_PED_SITTING_IN_ANY_VEHICLE( LocalPlayerPed ) )
							VEHICLE_INDEX veh
							veh = GET_VEHICLE_PED_IS_IN( LocalPlayerPed )
							IF( NOT IS_ENTITY_DEAD( veh ) )
								IF NETWORK_HAS_CONTROL_OF_ENTITY( veh )
									IF NOT IS_STRING_NULL_OR_EMPTY(sMyRadioStation)
										SET_VEH_RADIO_STATION(veh, sMyRadioStation)
										PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] SET_VEH_RADIO_STATION(veh, sMyRadioStation) model = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(veh)), " sMyRadioStation = ", sMyRadioStation)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				CLEAR_BIT( iLocalBoolCheck7, LBOOL7_WAS_VEH_RADIO_ENABLED_AT_CS_START )
				CLEAR_BIT(iLocalBoolCheck5, LBOOL5_MOCAP_REMOVED_LOCAL_PLAYER_MASK)
				
				IF MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_SIMEON
					NEW_LOAD_SCENE_STOP()
				ENDIF
				
				if not has_sound_finished(garage_door_sound_id)
					stop_sound(garage_door_sound_id)
				endif 
				
				IF MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_TREVOR
					if IPL_GROUP_SWAP_IS_ACTIVE()
						IPL_GROUP_SWAP_CANCEL()
					endif 
					REMOVE_IPL("TrevorsMP")
					REQUEST_IPL("TrevorsTrailer")
				ENDIF
				
				//Start the celebration screen early.
				IF ( bShowCelebrationScreen )
				OR NOT bIncludePlayers
					IF bShowCelebrationScreen
						//Start the celebration screen early.
						IF NOT Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer) // Added this as MP_Celeb_Win was triggering as part of the celebration screen for heists, causing a double-pulse.
						AND NOT Is_Player_Currently_On_MP_Heist(LocalPlayer)
						AND NOT GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
						AND NOT DOES_CURRENT_MISSION_USE_GANG_BOSS()
							SET_SKYFREEZE_FROZEN()
							IF SHOULD_POSTFX_BE_WINNER_VERSION()
								PLAY_CELEB_WIN_POST_FX()
								CPRINTLN(DEBUG_MIKE, "[RCC MISSION][ROSSW][RUN_MOCAP_CUTSCENE] ANIMPOSTFX_PLAY(MP_Celeb_Win, 0, TRUE) : bShowCelebrationScreen", bShowCelebrationScreen)
							ELSE
								PLAY_CELEB_LOSE_POST_FX()
								CPRINTLN(DEBUG_MIKE, "[RCC MISSION][ROSSW][RUN_MOCAP_CUTSCENE] ANIMPOSTFX_PLAY(MP_Celeb_Lose, 0, TRUE): bShowCelebrationScreen", bShowCelebrationScreen)
							ENDIF
							
							PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS", FALSE)
						ENDIF
					ENDIF
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					
					IF eCutType != FMMCCUT_ENDMOCAP
					AND NOT bShowCelebrationScreen
						IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
							NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
						ENDIF
					ENDIF
					
					NETWORK_SET_VOICE_ACTIVE(true)
					NETWORK_SET_IN_MP_CUTSCENE(FALSE, FALSE)
					PRINTLN("[TS] [MSRAND][RUN_MOCAP_CUTSCENE] - NETWORK_SET_IN_MP_CUTSCENE(FALSE, FALSE)")
					IF DOES_ENTITY_EXIST(ClonePlayerForCutscene)
						DELETE_PED(ClonePlayerForCutscene)
					ENDIF

					CPRINTLN(DEBUG_MIKE, "[RCC MISSION][RUN_MOCAP_CUTSCENE] SET_SCRIPTS_SAFE_FOR_CUTSCENE = FALSE")
				ENDIF
				
				//Moved conceal for b*2190572
				if ARE_STRINGS_EQUAL(sMocapName, "mph_pri_sta_mcs1") //prison - station / casco
				OR ARE_STRINGS_EQUAL(sMocapName, "MPH_PAC_WIT_MCS1") //Pacific Standard Boat Cutscene
				or are_strings_equal(sMocapName, "mph_tut_ext") //fleeca finale
				OR ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_HUM_FIN_EXT")
					CONCEAL_ALL_OTHER_PLAYERS(FALSE, true)
					
					PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] CONCEAL_ALL_OTHER_PLAYERS(FALSE, true)")
				else 
					CONCEAL_ALL_OTHER_PLAYERS(FALSE)	
					
					PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] CONCEAL_ALL_OTHER_PLAYERS(FALSE)")
				endif
				
				make_all_casco_players_visible_locally(sMocapName) //MUST be called after a conceal. Unconceal will set the visibility state back to the opposite of the current visibility state. By forcing the visibility state of the ped to be TRUE via make_all_casco_players_visible_locally and then calling un conceal after that would make the ped invisible again. 

				IF IS_PLAYER_SCTV(LocalPlayer)
					NETWORK_CONCEAL_PLAYER(LocalPlayer, TRUE, FALSE)
					CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_SCTV_IN_MOCAP)
					PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] GLOBAL_SPEC_BS_SCTV_IN_MOCAP CLEARED")
				ENDIF
				IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_ANY_SPECTATOR)
					NETWORK_CONCEAL_PLAYER(LocalPlayer, TRUE, FALSE)
					DISABLE_SPECTATOR_RADIO(FALSE)
					PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] - CLEAR SPECTATOR FLAGS")
				ENDIF
				
				//Moved : 2172811
				SET_SPECTATOR_RUNNING_CUTSCENE(FALSE)
					
				// url:bugstar:5832345 * If we're not already fading the screen out and our spectator target has not finish the cutscene then fadeout. 
				// This should hide all the popping and transition issues related to net syncing of player positions and clients being further/behind on processing the cutscene.
				IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
				AND IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitset2, PBBOOL2_PLAYING_MOCAP_CUTSCENE)
				AND NOT IS_SCREEN_FADING_OUT()
				AND NOT IS_SCREEN_FADED_OUT()
					PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] - Player we are Spectating is still in a Mocap Cutscene. To avoid weird popping/streaming issues we need to fadeout.")
					START_INSTANCED_CONTENT_FADE_SCREEN_OUT_FOR_PERIOD_OF_TIME(0, 3000)
				ENDIF
				
				IF HAVE_SPECTATOR_COORDS_BEEN_OVERRIDDEN()
					CLEAR_SPECTATOR_OVERRIDE_COORDS()
				ENDIF
				
				DO_POST_RUN_MOCAP_CHECKS()
				
				if are_strings_equal(sMocapName, "mph_pri_sta_mcs1") //prison - station / casco deactivation via PROCESS_BLOW_OPEN_DOOR()
					set_entity_proofs(player_ped_id(), false, false, false, false, false)
				endif 
			
				CLEANUP_MOCAP_PLAYER_CLONES()
			
				//NEVER CALL FORCE_PED_AI_AND_ANIMATION_UPDATE HERE. Always handle this for each player 
				//within HANDLE_MANUAL_EXIT_STATES()
				//Pre Camera set to true for peds being placed in vehicles causing camera pops over a frame

				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()

				CLEAR_BIT( iLocalBoolCheck8, LBOOL8_PLAYER_EXIT_STATE_CALLED )
				
				CLEAR_BIT( MC_playerBD[iLocalPart].iClientBitset2, PBBOOL2_PLAYING_MOCAP_CUTSCENE )
				tlPlayerSceneHandle = ""
				RESET_NET_TIMER(streamCSFailsafeTimer)
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][RUN_MOCAP_CUTSCENE] IS_CUTSCENE_PLAYING - FALSE / CAN_SET_EXIT_STATE_FOR_CAMERA = TRUE moving to 3" )
				
				SET_SRL_LONG_JUMP_MODE( FALSE )
				
				SET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS(MOCAPRUNNING_STAGE_3)
			ELSE // Else we have not finished this cutscene yet
				IF NOT IS_BIT_SET(iLocalBoolCheck28, LBOOL28_END_MOCAP_TRIED_TURNING_OFF_VOLATOL)
					SET_BIT(iLocalBoolCheck28, LBOOL28_END_MOCAP_TRIED_TURNING_OFF_VOLATOL)
					
					IF eCutType = FMMCCUT_ENDMOCAP
					AND ARE_STRINGS_EQUAL(sMocapName, "silj_int")
						PRINTLN("[RUN_MOCAP_CUTSCENE] Update: Cutscene is silj_int")
						
						INT iCutEntLoop
						FOR iCutEntLoop = 0 TO FMMC_MAX_CUTSCENE_ENTITIES-1							
							IF g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[iCutEntLoop].iIndex > -1
							AND g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[iCutEntLoop].iIndex < FMMC_MAX_VEHICLES 
							AND g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[iCutEntLoop].iType = CREATION_TYPE_VEHICLES
								VEHICLE_INDEX viEnt
								viEnt = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[iCutEntLoop].iIndex])
							
								IF DOES_ENTITY_EXIST(viEnt)
								AND GET_ENTITY_MODEL(viEnt) = VOLATOL
									
									IF GET_IS_VEHICLE_ENGINE_RUNNING(viEnt)
										PRINTLN("[RUN_MOCAP_CUTSCENE] Update: Cutscene ent ", iCutEntLoop, " is volatol. Turning engine off as it was on when coming into the cutscene.")
										SET_VEHICLE_ENGINE_ON(viEnt, FALSE, FALSE)
									ELSE
										PRINTLN("[RUN_MOCAP_CUTSCENE] Update: Cutscene ent ", iCutEntLoop, " is volatol. Engine was already off. No need to do anything.")
									ENDIF
								ELSE
									PRINTLN("[RUN_MOCAP_CUTSCENE] Update: Cutscene ent ", iCutEntLoop, " Cutscene ent type = ", g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[iCutEntLoop].iType)
									IF g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[iCutEntLoop].iType = CREATION_TYPE_VEHICLES
										PRINTLN("[RUN_MOCAP_CUTSCENE] Update: DoesExist = ", DOES_ENTITY_EXIST(viEnt), ", EntModel = ", ENUM_TO_INT(GET_ENTITY_MODEL(viEnt)))
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
					ELSE
						PRINTLN("[RUN_MOCAP_CUTSCENE] Update: eCutType = ", eCutType, ", sMocapName = ", sMocapName)
					ENDIF
				ENDIF
				
				IF IS_BIT_SET( iLocalBoolCheck8, LBOOL8_NO_MOCAP_AFTER_THIS_DROPOFF )
					CLEAR_BIT( iLocalBoolCheck8, LBOOL8_NO_MOCAP_AFTER_THIS_DROPOFF )
					
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][RUN_MOCAP_CUTSCENE] Clearing LBOOL8_NO_MOCAP_AFTER_THIS_DROPOFF" )
				ENDIF
			
				IF IS_BIT_SET( iLocalBoolCheck7, LBOOL7_TOGGLED_RENDERPHASES_FOR_DROPOFF )
					TOGGLE_RENDERPHASES( TRUE )
					CLEAR_BIT( iLocalBoolCheck7, LBOOL7_TOGGLED_RENDERPHASES_FOR_DROPOFF )

					IF NOT (IS_THIS_MOCAP_IN_AN_APARTMENT( sMocapName )
					AND IS_LOCAL_PLAYER_ANY_SPECTATOR())
						PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] - Untoggle renderphases in this dropoff cutscene now that it's started. ")
						IF IS_SCREEN_FADED_OUT()
						OR IS_SCREEN_FADING_OUT()
							DO_SCREEN_FADE_IN(1500)
						ENDIF
					ENDIF
				ENDIF
				
				CLEAR_APARTMENT_DROPOFF_SPINNER(4)
				
				IF bLocalPlayerPedOk
					SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisableVoiceDrivenMouthMovement , TRUE)
				ENDIF
				MANAGE_PLAYER_GESTURES()
				
			ENDIF
		BREAK
		
		CASE MOCAPRUNNING_STAGE_3
		
			make_all_casco_players_visible_locally(sMocapName)
			
			OVERRIDE_REMOTE_PLAYERS_COORDINATES(sMocapName, iCutsceneToUse)
			
			g_bAbortPropertyMenus = FALSE
			PRINTLN("[RUN_MOCAP_CUTSCENE]- 3 g_bAbortPropertyMenus = FALSE ")
			IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
				IF bShowCelebrationScreen
					IF NOT AM_I_ON_A_HEIST()
					AND NOT GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
					AND NOT CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
						IF IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bStartedCelebrationBeforeCutsceneEnded)
							IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration)
								DO_RENDER_PHASE_PAUSE_FOR_CELEBRATION_SCREEN()
								SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration)
								PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] - [NETCELEBRATION] - [SAC] - ciCELEBRATION_BIT_SET_TriggerEndCelebration - call 1.")
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerHeistwinnerCutscene)
							TRIGGER_END_HEIST_WINNER_CELEBRATION((NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoFacePlayerCamCut))
							PRINTLN("[RCC MISSION][RUN_MOCAP_CUTSCENE] - [NETCELEBRATION] - [SAC] - TRIGGER_END_HEIST_WINNER_CELEBRATION - call A.")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			if does_entity_exist(prison_break_plane)
				if is_entity_a_vehicle(prison_break_plane)
					if is_vehicle_driveable(prison_break_plane)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(prison_break_plane)
							CONTROL_LANDING_GEAR(prison_break_plane, lgc_deploy_instant)
							PRINTLN("RCC MISSION][RUN_MOCAP_CUTSCENE] [PED CUT] * REGISTER_ENTITY_FOR_CUTSCENE mph_pri_pla_ext plane obtained 2")
						else 
							NETWORK_REQUEST_CONTROL_OF_ENTITY(prison_break_plane)
							PRINTLN("RCC MISSION][RUN_MOCAP_CUTSCENE] [PED CUT] * prison_break_plane test 1")
						endif 
					else 
						PRINTLN("RCC MISSION][RUN_MOCAP_CUTSCENE] [PED CUT] * prison_break_plane test 2")	
					endif 
				else 
					PRINTLN("RCC MISSION][RUN_MOCAP_CUTSCENE] [PED CUT] * prison_break_plane test 3")
				endif 
			else
				PRINTLN("RCC MISSION][RUN_MOCAP_CUTSCENE] [PED CUT] * prison_break_plane test 4")
			endif
			
			IF ARE_STRINGS_EQUAL(sMocapName, "MPH_PRI_FIN_MCS2")
				IF NOT IS_PED_INJURED( pi_PrisonFinaleBuzzardPilot )
				AND IS_VEHICLE_DRIVEABLE( vi_PrisonFinaleBuzzard )
					
					SET_PED_INTO_VEHICLE( pi_PrisonFinaleBuzzardPilot, vi_PrisonFinaleBuzzard )
					SET_HELI_BLADES_FULL_SPEED( vi_PrisonFinaleBuzzard )
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][RUN_MOCAP_CUTSCENE] I am the buzzard player in ", sMocapName, " and will unfreeze next frame." )

					SET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS(MOCAPRUNNING_STAGE_5)
					RETURN FALSE
				ENDIF
			ENDIF
			
			// url:bugstar:2214705
			IF ARE_STRINGS_EQUAL( sMocapName, "MPH_PAC_HAC_MCS1" )
			OR ARE_STRINGS_EQUAL( sMocapName, "MPH_HUM_FIN_MCS1" )
				iOverridePositionCount = 0
				SET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS(MOCAPRUNNING_STAGE_6)
				RETURN FALSE
			ENDIF
			
			IF HAVE_SPECTATOR_COORDS_BEEN_OVERRIDDEN()
				CLEAR_SPECTATOR_OVERRIDE_COORDS()
			ENDIF

			IF IS_PLAYSTATION_PLATFORM()
				CLEAR_CONTROL_LIGHT_EFFECT(PLAYER_CONTROL)
			ENDIF
			
			CPRINTLN(DEBUG_CONTROLLER, "[RCC MISSION] [RUN_MOCAP_CUTSCENE] - eMocapRunningCutsceneProgress: ", GET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS_NAME(eMocapRunningCutsceneProgress), " - returned TRUE")
			RETURN TRUE
		
		BREAK
		
		//For the strand missions!
		CASE MOCAPRUNNING_STAGE_4
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] [RUN_MOCAP_CUTSCENE] - STRAND MISSION - ENTERED CASE 4")

			g_bAbortPropertyMenus = FALSE
			PRINTLN("RUN_MOCAP_CUTSCENE- 4 g_bAbortPropertyMenus = FALSE ")
			IF HAVE_SPECTATOR_COORDS_BEEN_OVERRIDDEN()
				CLEAR_SPECTATOR_OVERRIDE_COORDS()
			ENDIF

			IF IS_PLAYSTATION_PLATFORM()
				CLEAR_CONTROL_LIGHT_EFFECT(PLAYER_CONTROL)
			ENDIF

			CPRINTLN(DEBUG_CONTROLLER, "[RCC MISSION] [RUN_MOCAP_CUTSCENE] - eMocapRunningCutsceneProgress: ", GET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS_NAME(eMocapRunningCutsceneProgress), " - returned TRUE")
			RETURN TRUE
		BREAK
		
		// For MPH_PRI_FIN_MCS2's helicopter player
		CASE MOCAPRUNNING_STAGE_5
			
			g_bAbortPropertyMenus = FALSE
			PRINTLN("RUN_MOCAP_CUTSCENE- 5 g_bAbortPropertyMenus = FALSE ")
			IF NETWORK_HAS_CONTROL_OF_ENTITY( vi_PrisonFinaleBuzzard )
				IF NOT IS_PED_INJURED( pi_PrisonFinaleBuzzardPilot )
				AND IS_VEHICLE_DRIVEABLE( vi_PrisonFinaleBuzzard )
					
					FREEZE_ENTITY_POSITION(vi_PrisonFinaleBuzzard, FALSE)
					vi_PrisonFinaleBuzzard = NULL
					pi_PrisonFinaleBuzzardPilot = NULL
					
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][RUN_MOCAP_CUTSCENE] Unfreezing buzzard for ", sMocapName )
					
					IF IS_PLAYSTATION_PLATFORM()
						CLEAR_CONTROL_LIGHT_EFFECT(PLAYER_CONTROL)
					ENDIF
					
					CPRINTLN(DEBUG_CONTROLLER, "[RCC MISSION][RUN_MOCAP_CUTSCENE] - eMocapRunningCutsceneProgress: ", GET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS_NAME(eMocapRunningCutsceneProgress), " - returned TRUE")
					RETURN TRUE
				ELSE
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][RUN_MOCAP_CUTSCENE] The Buzzard or the player are broken " )
				ENDIF
			ELSE
				NETWORK_REQUEST_CONTROL_OF_ENTITY( vi_PrisonFinaleBuzzard )
				IF NOT IS_PED_IN_ANY_VEHICLE( pi_PrisonFinaleBuzzardPilot )
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][RUN_MOCAP_CUTSCENE] Buzzard player is still not in the buzzard yet - continuing to put them in.")
					SET_PED_INTO_VEHICLE( pi_PrisonFinaleBuzzardPilot, vi_PrisonFinaleBuzzard )
				ENDIF
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][RUN_MOCAP_CUTSCENE] Waiting for control over the buzzard for ", sMocapName )
			ENDIF
		
		BREAK
		
		// url:bugstar:2214705
		// MPH_PAC_HAC_MCS1 and MPH_HUM_FIN_MCS1 come here
		CASE MOCAPRUNNING_STAGE_6
			OVERRIDE_REMOTE_PLAYERS_COORDINATES(sMocapName, iCutsceneToUse)
			iOverridePositionCount++
			
			IF iOverridePositionCount >= 3
				CPRINTLN(DEBUG_CONTROLLER, "[RCC MISSION][RUN_MOCAP_CUTSCENE] - eMocapRunningCutsceneProgress: ", GET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS_NAME(eMocapRunningCutsceneProgress), " - returned TRUE")
				RETURN TRUE
			ENDIF
		BREAK
			
	ENDSWITCH

	RETURN FALSE

ENDFUNC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: MANAGE END CUTSCENE !
//
//************************************************************************************************************************************************************

PROC PROCESS_INITIALISING_STRAND_MISSION()
	
	//Call the function to set up the strand details
	SET_UP_STRAND_DETAILS_AT_END_OF_MISSION(MC_serverBD.iEndCutscene, MC_serverBD.iNextMission)
	SET_BIT(iLocalBoolCheck5, LBOOL5_SET_UP_STRAND_DETAILS_AT_END_OF_MISSION)
	PRINTLN("[RCC MISSION][MSRAND] PROCESS_INITIALISING_STRAND_MISSION - called SET_UP_STRAND_DETAILS_AT_END_OF_MISSION for next mission: ", MC_serverBD.iNextMission)
	
	g_TransitionSessionNonResetVars.iStrandTotalStartingPlayers = MC_serverBD.iTotalNumStartingPlayers
	PRINTLN("[RCC MISSION][MSRAND] PROCESS_INITIALISING_STRAND_MISSION - Caching starting players for next part  = ",MC_serverBD.iTotalNumStartingPlayers) 
	
	INT iTeam
	FOR iteam = 0 TO (FMMC_MAX_TEAMS-1)
		g_TransitionSessionNonResetVars.iStrandStartingPlayers[iteam] = MC_serverBD.iNumStartingPlayers[iteam]
		PRINTLN("[RCC MISSION][MSRAND] PROCESS_INITIALISING_STRAND_MISSION - team = ",iteam," caching from previous strand part = ",MC_serverBD.iNumStartingPlayers[iteam])
	ENDFOR
	
ENDPROC

///PURPOSE: This function does the end mocap cutscene as well as celebration screen stuff.
PROC MANAGE_END_CUTSCENE()
	
	IF IS_BIT_SET( iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE )
	AND NOT IS_BIT_SET( iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE_OVER )
		IF NOT IS_STRING_EMPTY(sMocapCutscene)
			IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PAC_FIN_MCS0")
			OR ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PAC_FIN_MCS1")
				DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_SELECT_WEAPON)
				DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_WEAPON_WHEEL_UD)
				DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_WEAPON_WHEEL_LR)
				DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_SELECT_NEXT_WEAPON)
				DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_SELECT_PREV_WEAPON)
				
				DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_SELECT_WEAPON_UNARMED)
				DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_SELECT_WEAPON_MELEE)
				DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_SELECT_WEAPON_HANDGUN)
				DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_SELECT_WEAPON_SHOTGUN)
				DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_SELECT_WEAPON_SMG)
				DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_SELECT_WEAPON_AUTO_RIFLE)
				DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_SELECT_WEAPON_SNIPER)
				DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_SELECT_WEAPON_HEAVY)
				DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_SELECT_WEAPON_SPECIAL)
				HUD_SUPPRESS_WEAPON_WHEEL_RESULTS_THIS_FRAME()
				PRINTLN("[MCS_PAC_FIN] Blocking weapon changing 2")
			ENDIF
		ENDIF
	
		IF NOT IS_BIT_SET( iLocalBoolCheck5, LBOOL5_SET_UP_STRAND_DETAILS_AT_END_OF_MISSION )
			PROCESS_INITIALISING_STRAND_MISSION()
			SET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS(MOCAPRUNNING_STAGE_0)
		ELSE
			//If this is a strnd mission then set that we are ready to go
			IF IS_A_STRAND_MISSION_BEING_INITIALISED()
			AND NOT IS_MISSION_CONTROLLER_READY_FOR_CUT()               
				SET_MISSION_CONTROLLER_READY_FOR_CUT()
			ENDIF
			
			DISABLE_HUD()
			STOP_PLAYER_LEAVING_VEHICLE_THIS_FRAME()

			PRINTLN("[RCC MISSION] TEST MB BEFORE LBOOL9_PRE_MOCAP_CINE_TRIGGERED CHECK ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(mocapPreCinematic))
			IF NOT IS_BIT_SET(iLocalBoolCheck9, LBOOL9_PRE_MOCAP_CINE_TRIGGERED)
			OR (IS_BIT_SET(iLocalBoolCheck9, LBOOL9_PRE_MOCAP_CINE_TRIGGERED) AND GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(mocapPreCinematic) > GET_HEIST_PRE_MOCAP_CINE_TIME())
			PRINTLN("[RCC MISSION] TEST MB AFTER LBOOL9_PRE_MOCAP_CINE_TRIGGERED CHECK")
		        IF WARP_TO_END_MOCAP()
					
					IF NOT IS_STRING_EMPTY(sMocapCutscene)
					AND ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_TUT_MID")
						
						CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Waiting for pan to be finished for MPH_TUT_MID in order to trigger special celebration screen." )
					
						IF g_HeistGarageDropoffSplashReady
						OR g_HeistGarageDropoffPanComplete // failsafe
						OR IS_LOCAL_PLAYER_ANY_SPECTATOR() // The above flags don't get set on spectators
						#IF IS_DEBUG_BUILD
						OR iPlayerPressedS > -1 //has player S passed
						#ENDIF
						
							SET_LAUNCH_HEIST_TUTORIAL_MID_STRAND_CUT_SCENE()
							g_TransitionSessionNonResetVars.bPhilsPlanningBoardPropSuppressor = TRUE
							IF NOT IS_BIT_SET( iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerHeistwinnerCutscene )
								IF NOT IS_BIT_SET( iLocalBoolCheck7, LBOOL7_TOGGLED_RENDERPHASES_FOR_DROPOFF )
									SET_BIT( iLocalBoolCheck7, LBOOL7_TOGGLED_RENDERPHASES_FOR_DROPOFF )
									TOGGLE_RENDERPHASES( FALSE )
								ENDIF
								
								CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Triggering MPH_TUT_MID celebration stuff " )
								TRIGGER_END_HEIST_WINNER_CELEBRATION((NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoFacePlayerCamCut))
							ENDIF
						ENDIF
						
					ELSE
						IF IS_STRING_EMPTY(sMocapCutscene)//url:bugstar:4628425
							sMocapCutscene = ""
						ENDIF
					
						CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] sMocapCutscene missing this may cause problems ")
						BOOL bCreateDoors
						IF GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].propertyDetails.iCurrentlyInsideProperty != -1
						OR IS_THIS_MOCAP_IN_AN_APARTMENT(sMocapCutscene)
							bCreateDoors = TRUE
						ENDIF
						
						IF NOT IS_BIT_SET( iLocalBoolCheck20, LBOOL20_TURNED_OFF_RADIO_FOR_END_CUTSCENE )
							IF IS_PED_IN_ANY_VEHICLE( LocalPlayerPed )
								IF NETWORK_HAS_CONTROL_OF_ENTITY( GET_VEHICLE_PED_IS_IN( LocalPlayerPed ) )
									CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] MANAGE_END_CUTSCENE - We're in a vehicle, so we should turn the radio off. ")
									
									SET_VEHICLE_RADIO_ENABLED( GET_VEHICLE_PED_IS_IN( LocalPlayerPed ), FALSE )
								ENDIF
							ENDIF
							
							SET_BIT( iLocalBoolCheck20, LBOOL20_TURNED_OFF_RADIO_FOR_END_CUTSCENE )
						ENDIF
						
						SET_BIT( iLocalBoolCheck31, LBOOL31_END_CUTSCENE_STARTED)
						
						IF RUN_MOCAP_CUTSCENE(sMocapCutscene, FMMCCUT_ENDMOCAP, TRUE, <<0,0,0>>, ciINVALID_CUTSCENE_WARP_HEADING, <<0,0,0>>, ciINVALID_CUTSCENE_WARP_HEADING, DEFAULT, DEFAULT, DEFAULT, bCreateDoors)
							CPRINTLN( DEBUG_CONTROLLER,"[RCC MISSION] MANAGE_END_CUTSCENE - end cutscene finished")
							CLEAR_BIT( MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYING_CUTSCENE )
					        SET_BIT( iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE_OVER )
					        CLEAR_PRINTS()
							IF IS_NAMED_RENDERTARGET_REGISTERED("Prop_x17_Sec_Panel_01")
								RELEASE_NAMED_RENDERTARGET("Prop_x17_Sec_Panel_01")
							ENDIF
						ENDIF
					
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE // Else there's not an end mocap on this mission
		IF IS_BIT_SET( iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE_OVER )
		OR IS_BIT_SET( iLocalBoolCheck2, LBOOL2_NORMAL_END )
			BOOL bSkip
			
			IF HAS_TEAM_PASSED_MISSION(MC_playerBD[iLocalPart].iteam)
				IF NOT IS_BIT_SET( iLocalBoolCheck5, LBOOL5_SET_UP_STRAND_DETAILS_AT_END_OF_MISSION )
					PROCESS_INITIALISING_STRAND_MISSION()
					IF IS_A_STRAND_MISSION_BEING_INITIALISED()
					AND NOT IS_MISSION_CONTROLLER_READY_FOR_CUT()
						SET_MISSION_CONTROLLER_READY_FOR_CUT()
						POPULATE_RESULTS()
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_A_STRAND_MISSION_BEING_INITIALISED()
				SET_TRANSITION_SESSIONS_SKIP_LB_AND_NJVS()
				SET_BIT(iLocalBoolCheck2,LBOOL2_LB_CAM_READY)
				SET_BIT(iLocalBoolCheck3,LBOOL3_SHOWN_RESULTS)
				EXIT
			ENDIF
			
			// Sort heists.			
			GET_HEIST_COMPLETION_RANKINGS()
			POPULATE_RESULTS()
			COPY_CELEBRATION_DATA_TO_GLOBALS()
			
			IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
			OR Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
			OR GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
			OR CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
			
				// Sort out if we are doing the back to free mode transition behind the celebration screen.
				IF HAS_TEAM_PASSED_MISSION(MC_playerBD[iLocalPart].iteam)
					bSkip = TRUE
					IF IS_BIT_SET(iLocalBoolCheck2, LBOOL2_NORMAL_END)
						g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoFacePlayerCamCut = TRUE
						PRINTLN("[RCC MISSION] - [SAC] - LBOOL2_NORMAL_END = TRUE, setting = TRUE.")
					ENDIF
				ENDIF
			ENDIF
			
			IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
			OR Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
				
				// Sort out if going back to apartment or not.
				IF NOT IS_BIT_SET(MC_serverbd.iServerBitSet, SBBOOL_HEIST_LEADER_LEFT)

					IF IS_PLAYER_IN_MP_PROPERTY(LocalPlayer, FALSE)
					AND NOT IS_PLAYER_IN_MP_GARAGE(LocalPlayer, FALSE)
						SET_POST_MISSION_PLACE_IN_APARTMENT_FLAG(TRUE)
						PRINTLN("[RCC MISSION] - [PMC] - MANAGE_END_CUTSCENE - in mp apartment at end of mission, set bPlaceInApartment = TRUE.") 			
					ELSE
						SET_POST_MISSION_PLACE_IN_APARTMENT_FLAG(FALSE)
						PRINTLN("[RCC MISSION] - [PMC] - MANAGE_END_CUTSCENE - not in mp apartment at end of mission, set bPlaceInApartment = FALSE.") 
					ENDIF
					
					IF SHOULD_LAUNCH_HEIST_TUTORIAL_MID_STRAND_CUT_SCENE()
						SET_POST_MISSION_PLACE_IN_APARTMENT_FLAG(TRUE)
						PRINTLN("[RCC MISSION] - [PMC] - MANAGE_END_CUTSCENE - SHOULD_LAUNCH_HEIST_TUTORIAL_MID_STRAND_CUT_SCENE() = TRUE, set bPlaceInApartment = TRUE.")
					ENDIF
					
					IF HAS_TEAM_PASSED_MISSION(MC_playerBD[iLocalPart].iteam)
						IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId = SIS_POST_APARTMENT
							SET_POST_MISSION_PLACE_IN_APARTMENT_FLAG(TRUE)
							PRINTLN("[RCC MISSION] - [PMC] - MANAGE_END_CUTSCENE - iPostMissionSceneId = SIS_POST_APARTMENT and team has passed mission, set bPlaceInApartment = TRUE.")
						ENDIF
					ENDIF
					
				ELSE
				
					SET_POST_MISSION_PLACE_IN_APARTMENT_FLAG(FALSE)
					PRINTLN("[RCC MISSION] - [PMC] - MANAGE_END_CUTSCENE - heist leader has left so cannot go back to their apartment, set bPlaceInApartment = FALSE.") 
					
				ENDIF
				
				// Set where in the apartment I'm going to.
				IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlaceInApartment
					PRINTLN("[RCC MISSION] - [PMC] - MANAGE_END_CUTSCENE - bPlaceInApartment = TRUE, calling SET_APARTMENT_LOCATION().") 
					SET_APARTMENT_LOCATION()
				ELSE
					PRINTLN("[RCC MISSION] - [PMC] - MANAGE_END_CUTSCENE - bPlaceInApartment = FALSE, not calling SET_APARTMENT_LOCATION().") 
				ENDIF
				
			ENDIF
			
			
			IF bSkip
			OR IS_FAKE_MULTIPLAYER_MODE_SET()

				// Skip celebrations on CnC missions.
				IF IS_FAKE_MULTIPLAYER_MODE_SET()
					IF HAS_CELEBRATION_SCREEN_SKIP_FINISHED()
						SET_BIT(iLocalBoolCheck2,LBOOL2_LB_CAM_READY)
						SET_BIT(iLocalBoolCheck3,LBOOL3_SHOWN_RESULTS)
					ENDIF
				ENDIF
				
				
				// Trigger the winner heist cutscene.
				IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
				AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
					IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerHeistwinnerCutscene)
						TRIGGER_END_HEIST_WINNER_CELEBRATION((NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoFacePlayerCamCut))
					ENDIF
				ENDIF
	
			ELSE
				
				IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration)
				AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
				
					// cutscene not running
					IF NOT IS_CUTSCENE_PLAYING()
					AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYING_CUTSCENE)
//						IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
//							IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerHeistwinnerCutscene)
//								TRIGGER_END_HEIST_WINNER_CELEBRATION((NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoFacePlayerCamCut))
//							ENDIF
//						ELSE
							SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration)
							CPRINTLN(DEBUG_CONTROLLER, "[NETCELEBRATION] - MANAGE_END_CUTSCENE - SET ciCELEBRATION_BIT_SET_TriggerEndCelebration - no cutscene running")
//						ENDIF
					// cutscene running
					ELSE
						
						INT iTeam = MC_playerBD[iLocalPart].iteam
						IF iTeam >= 0
						AND iTeam < FMMC_MAX_TEAMS
						
							BOOL bInterruptOnFail
							IF MC_serverBD.iCutsceneID[iTeam] >= FMMC_GET_MAX_SCRIPTED_CUTSCENES()
							AND MC_serverBD.iCutsceneID[iTeam] < FMMC_GET_MAX_CUTSCENES_WITH_MOCAP()
								INT iCutscene = MC_serverBD.iCutsceneID[iTeam] - FMMC_GET_MAX_SCRIPTED_CUTSCENES()
								bInterruptOnFail = IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].iCutsceneBitSet2, ci_CSBS2_InterruptOnFail)
								PRINTLN("[RCC MISSION] MANAGE_END_CUTSCENE - ci_CSBS2_InterruptOnFail = TRUE for MOCAP")
							ENDIF
						
							IF bInterruptOnFail
								SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration)
								CPRINTLN(DEBUG_CONTROLLER, "[NETCELEBRATION] - MANAGE_END_CUTSCENE - SET ciCELEBRATION_BIT_SET_TriggerEndCelebration during cutscene, Allowed by bShouldSkipOnFail(ci_CSBS2_InterruptOnFail)")
							ELSE
								PRINTLN("[RCC MISSION] MANAGE_END_CUTSCENE - Not setting celebration trigger - IS_CUTSCENE_PLAYING = ", BOOL_TO_STRING(IS_CUTSCENE_PLAYING()), 
									" PBBOOL_PLAYING_CUTSCENE = ", BOOL_TO_STRING(IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYING_CUTSCENE)))
							ENDIF
						
						ENDIF
							
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_EndCelebrationFinished)
				OR IS_FAKE_MULTIPLAYER_MODE_SET()
					//REQUEST_LEADERBOARD_CAM()
					PRINTLN("[RCC MISSION] MANAGE_END_CUTSCENE - HAS_CELEBRATION_SCREEN_FINISHED - TRUE")
					CLEAR_PRINTS()
					
					//IF IS_LEADERBOARD_CAM_RENDERING_OR_INTERPING()	
						SET_BIT(iLocalBoolCheck2,LBOOL2_LB_CAM_READY)
					//ENDIF
				ELSE
					PRINTLN("[RCC MISSION] MANAGE_END_CUTSCENE - ciCELEBRATION_BIT_SET_EndCelebrationFinished = FALSE")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: PROCESS MOCAP CUTSCENE !
//
//************************************************************************************************************************************************************

FUNC BOOL ARE_ALL_PLAYERS_SYNCED_FOR_END_OF_CUTSCENE()

	INT iPart
	FOR iPart = 0 TO NUM_NETWORK_PLAYERS-1
		PARTICIPANT_INDEX participantIndex = INT_TO_PARTICIPANTINDEX(iPart)
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(participantIndex)
			IF IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_PLAYING_CUTSCENE)
				IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet4, PBBOOL4_SYNC_PLAYERS_FOR_END_OF_CUTSCENE)
					PRINTLN("[ML][ARE_ALL_PLAYERS_SYNCED_FOR_END_OF_CUTSCENE] Waiting for ", iPart, " so returning FALSE") 
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	PRINTLN("[ML][ARE_ALL_PLAYERS_SYNCED_FOR_END_OF_CUTSCENE] All players synced at end of cutscene, returning TRUE")
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_FADE_MOCAP_IN_AT_END(BOOL bIgnoreSpectator = FALSE)
	
	IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
	AND NOT bIgnoreSpectator
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_RUNNING_WALL_RAPPEL_NOT_ON_RAPPEL_RULE)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_INIT_MPH_PRI_STA_MCS2()
	IF NOT IS_STRING_NULL_OR_EMPTY( sMocapCutscene )
		IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PRI_STA_MCS2")
			//Making sure players weapon doesn't pop pre casco drop off cutscene:
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)) = CASCO
					SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED)
					CPRINTLN(DEBUG_CONTROLLER, "[RCC MISSION] Setting LocalPlayerPed weapon to unarmed.")
				ENDIF
			ENDIF		
		ENDIF
	ENDIF
ENDPROC
			
PROC PROCESS_INIT_MPH_PRI_FIN_MCS2(INT iCutsceneToUse)
	IF NOT IS_STRING_NULL_OR_EMPTY( sMocapCutscene )		
		IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PRI_FIN_MCS2")
			//Set the plane magic invincible and stuff for the end of prison finale:
			
			NETWORK_INDEX niPlane
			niPlane = GET_CUTSCENE_NET_ID_WITH_HANDLE("PLANE", iCutsceneToUse)
			
			IF NETWORK_DOES_NETWORK_ID_EXIST(niPlane)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(niPlane)
					VEHICLE_INDEX vehPlane
					vehPlane = NET_TO_VEH(niPlane)
					
					PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - Running MPH_PRI_FIN_MCS2 cutscene, fix the velum and make it invincible")
					
					SET_ENTITY_INVINCIBLE(vehPlane, TRUE)
					SET_ENTITY_PROOFS(vehPlane, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, DEFAULT, TRUE)
					SET_VEHICLE_CAN_BREAK(vehPlane, FALSE)
					SET_ENTITY_CAN_BE_DAMAGED(vehPlane, FALSE)
					SET_ENTITY_HEALTH(vehPlane, GET_ENTITY_MAX_HEALTH(vehPlane))
					SET_VEHICLE_ENGINE_HEALTH(vehPlane, 1000)
					SET_VEHICLE_PETROL_TANK_HEALTH(vehPlane, 1000)
					SET_VEHICLE_BODY_HEALTH(vehPlane, 1000)
					SET_VEHICLE_FIXED(vehPlane)
					
				ENDIF
				
				INT iVeh
				iVeh = IS_ENTITY_A_MISSION_CREATOR_ENTITY(NET_TO_VEH(niPlane))
				
				IF iVeh != -1
					
					BOOL bCritical
					INT iteam
					
					FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
						IF IS_BIT_SET(MC_serverBD.iVehteamFailBitset[iveh],iteam)
						OR IS_BIT_SET(MC_serverBD.iMissionCriticalVeh[iteam], iveh)
							bCritical = TRUE
							iteam = MC_serverBD.iNumberOfTeams
						ENDIF
					ENDFOR
					
					IF bCritical
						IF bIsLocalPlayerHost
							PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - Running MPH_PRI_FIN_MCS2 cutscene, make the velum non-critical")
							FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
								CLEAR_BIT(MC_serverBD.iVehteamFailBitset[iveh],iteam)
								CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iteam], iveh)
							ENDFOR
						ELSE
							PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - Running MPH_PRI_FIN_MCS2 cutscene, broadcast that the server should make the velum non-critical")
							BROADCAST_FMMC_PRI_FIN_MCS2_PLANE_NON_CRITICAL(iVeh)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC
	
// [FIX_2020_CONTROLLER] - This function needs to be turned into the legacy version and a new one should be made for new content which excludes all of the zany weird special case "this_specific_cutscene" logic.
// I also believe a few areas of this function could do with being re-written as we won't have to worry about breaking old content. Such as Placing in vehicles and warping the peds. Also cloning the LOCALPLAYERPED for the cutscene instead of using the actual entity.
// Further to this allowing warping player in the middle of the cutscene so we can do what we like with the actual player entity while the cutscene is running, this will stop us having to use black screen fadeouts to hide us doing stuff after the cutscene has finished..
PROC PROCESS_MOCAP_CUTSCENE(INT iCutsceneToUse, INT iCutsceneTeam, FMMC_CUTSCENE_TYPE eCutType)

	STOP_LOCAL_PLAYER_FROM_GETTING_SHOT_THIS_FRAME()

	IF GET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS() = MOCAPPROG_INIT
		PRINTLN("[RCC MISSION] MANAGE_MID_MISSION_CUTSCENE - bIsMocap = TRUE" )
	ENDIF
	
	FLOAT fWarpRadius = GET_CUTSCENE_PLAYER_INCLUSION_RANGE(sMocapCutscene, MC_playerBD[iPartToUse].iteam, iCutsceneTouse, eCutType, TRUE )
	
	IF GET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS() <= MOCAPRUNNING_STAGE_2
	AND GET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS() > MOCAPPROG_INIT
		STOP_PLAYER_LEAVING_VEHICLE_THIS_FRAME()
	ENDIF
	
	IF GET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS() <= MOCAPRUNNING_STAGE_3
	AND GET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS() > MOCAPPROG_INIT
		IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PAC_FIN_MCS0")
		OR ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PAC_FIN_MCS1")
			DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_SELECT_WEAPON)
			DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_WEAPON_WHEEL_UD)
			DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_WEAPON_WHEEL_LR)
			DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_SELECT_NEXT_WEAPON)
			DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_SELECT_PREV_WEAPON)
			HUD_SUPPRESS_WEAPON_WHEEL_RESULTS_THIS_FRAME()
			DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_SELECT_WEAPON_UNARMED)
			DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_SELECT_WEAPON_MELEE)
			DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_SELECT_WEAPON_HANDGUN)
			DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_SELECT_WEAPON_SHOTGUN)
			DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_SELECT_WEAPON_SMG)
			DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_SELECT_WEAPON_AUTO_RIFLE)
			DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_SELECT_WEAPON_SNIPER)
			DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_SELECT_WEAPON_HEAVY)
			DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_SELECT_WEAPON_SPECIAL)
		ENDIF
	ENDIF
			
	INT iPlayerIndex = -1  
	IF ARE_TEAM_CUTSCENE_PLAYERS_SELECTED(iCutsceneTeam)
		iPlayerIndex = GET_LOCAL_PLAYER_CUTSCENE_INDEX(iCutsceneTeam)
		PRINTLN("PROCESS_MOCAP_CUTSCENE - SET iPlayerIndex: ", iPlayerIndex, " from GET_LOCAL_PLAYER_CUTSCENE_INDEX")
	ELSE
		PRINTLN("PROCESS_MOCAP_CUTSCENE - NOT SET iPlayerIndex: ", iPlayerIndex, " from GET_LOCAL_PLAYER_CUTSCENE_INDEX - due to ARE_TEAM_CUTSCENE_PLAYERS_SELECTED = FALSE")
	ENDIF
	
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PRI_FIN_MCS1") //Fix for b*2220417
		IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
			iPlayerIndex = GET_PLAYER_TEAM(localPlayer)
			PRINTLN("PROCESS_MOCAP_CUTSCENE - SET iPlayerIndex: ", iPlayerIndex, " from MPH_PRI_FIN_MCS1 (NON-SPECATOR)")
		ELSE
			iPlayerIndex = GET_PLAYER_TEAM(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_CURRENT_FOCUS_PED()))
			PRINTLN("PROCESS_MOCAP_CUTSCENE - SET iPlayerIndex: ", iPlayerIndex, " from MPH_PRI_FIN_MCS1 (SPECATOR)")
		ENDIF
	ENDIF
	
	// fallback
	IF iPlayerIndex = -1
		iPlayerIndex = GET_PLAYER_FALLBACK_CUTSCENE_INDEX()
		PRINTLN("PROCESS_MOCAP_CUTSCENE - iPlayerIndex: ", iPlayerIndex, " from GET_PLAYER_FALLBACK_CUTSCENE_INDEX")
	ENDIF

	// clamp
	IF iPlayerIndex < 0 OR iPlayerIndex >= FMMC_MAX_CUTSCENE_PLAYERS
		ASSERTLN("PROCESS_MOCAP_CUTSCENE - iPlayerIndex: ", iPlayerIndex, " is out of range 0 - FMMC_MAX_CUTSCENE_PLAYERS(", FMMC_MAX_CUTSCENE_PLAYERS, "), Clamping")
		iPlayerIndex = CLAMP_INT(iPlayerIndex, 0, FMMC_MAX_CUTSCENE_PLAYERS-1)
	ENDIF
	
	VECTOR vStartPos = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].vPlayerStartPos[iPlayerIndex]
	FLOAT fStartPos = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].fPlayerStartHeading[iPlayerIndex]
	IF IS_VECTOR_ZERO(vStartPos)
		vStartPos = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].vPlayerStartPos[0]
		fStartPos = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].fPlayerStartHeading[0]
		PRINTLN("PROCESS_MOCAP_CUTSCENE - iPlayerIndex: ", iPlayerIndex, " vStartPos: ", vStartPos, " fallback to idx 0 used!")
	ELSE
		PRINTLN("PROCESS_MOCAP_CUTSCENE - iPlayerIndex: ", iPlayerIndex, " vStartPos: ", vStartPos, " used")
	ENDIF

	VECTOR vEndPos = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].vPlayerEndPos[iPlayerIndex]
	FLOAT fEndPos = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].fPlayerEndHeading[iPlayerIndex]
	IF IS_VECTOR_ZERO(vEndPos)
		vEndPos = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].vPlayerEndPos[0]
		fEndPos = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].fPlayerEndHeading[0]
		PRINTLN("PROCESS_MOCAP_CUTSCENE - iPlayerIndex: ", iPlayerIndex, " vEndPos: ", vEndPos, " fallback to idx 0 used!")
	ELSE
		PRINTLN("PROCESS_MOCAP_CUTSCENE - iPlayerIndex: ", iPlayerIndex, " vEndPos: ", vEndPos, " used")
	ENDIF
	
	eCutsceneTypePlaying = eCutType
	iCutsceneIndexPlaying = iCutsceneToUse
	
	SWITCH eMocapManageCutsceneProgress
		
		CASE MOCAPPROG_INIT
			
			TURN_OFF_CUTSCENE_VEHICLE_RADIO()
			
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Starting a mocap cutscene." )	
			
			PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE CURRENT NETWORK TIMER: ", NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()))
			PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE GET_NET_TIMER_DIFF", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_ServerBD.tdCutsceneStartTime[iScriptedCutsceneTeam],FALSE,TRUE),"[RCC MISSION] is host: ", bIsLocalPlayerHost)
			
			UNLOCK_SPECIFIC_VEHICLE_BEFORE_CUTSCENE( iCutsceneToUse )
				
			TOGGLE_PLAYER_DAMAGE_OVERLAY( FALSE )
			
			IF NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
				IF NOT bPlayerToUseOK
				OR IS_PLAYER_RESPAWNING( LocalPlayer )
					NETWORK_RESURRECT_LOCAL_PLAYER( GET_ENTITY_COORDS( PLAYER_PED_ID(), FALSE ),
													GET_ENTITY_HEADING( PLAYER_PED_ID() ),
													10,
													FALSE )
					CACHE_LOCAL_PLAYER_VARIABLES()
				ENDIF
			ENDIF

			SET_PLAYER_INVINCIBLE( LocalPlayer, TRUE )
			SET_ENTITY_PROOFS( LocalPlayerPed, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, TRUE )
			
			//Get the local player peds weapon before cutscene
			playerCutsceneWeap = GET_PEDS_CURRENT_WEAPON(LocalPlayerPed)
			
			g_bInMissionControllerCutscene = TRUE
			
			PROCESS_INIT_MPH_PRI_STA_MCS2()
			
			PROCESS_INIT_MPH_PRI_FIN_MCS2(iCutsceneToUse)
			
			sEarlyCelebrationData sData
			sData = SHOULD_CELEBRATION_SCREEN_BE_SHOWN_PREMATURELY()
			IF sData.bShouldShowPrematurely
			AND NOT IS_THIS_MOCAP_IN_AN_APARTMENT( sMocapCutscene )
				g_bEndOfMissionCleanUpHUD = TRUE
				CPRINTLN(DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_MOCAP_CUTSCENE Setting g_bEndOfMissionCleanUp - HUD SHOULD_CELEBRATION_SCREEN_BE_SHOWN_PREMATURELY true for cutscene ")
			ENDIF

			SET_IM_WATCHING_A_MOCAP()
			
			SET_BIT( iLocalBoolCheck5, LBOOL5_PED_INVOLVED_IN_CUTSCENE )
			
			SET_BIT( MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_STARTED_CUTSCENE )
			
			SET_BIT( MC_playerBD[ iLocalPart ].iClientBitSet2, PBBOOL2_IN_MID_MISSION_CUTSCENE )

			CLEAN_UP_SYNC_LOCK_OBJECTS(TRUE,TRUE)
			
			IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR() // NOTE(Owain): We don't want spectators doing this because of B* 2235583
			AND NOT IS_PED_INJURED(localPlayerPed)
			AND (NOT IS_PLAYER_RESPAWNING(LocalPlayer) OR IS_FAKE_MULTIPLAYER_MODE_SET())
				IF MC_playerBD[ iPartToUse ].iTeam = iScriptedCutsceneTeam
					SET_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_REQUEST_CUTSCENE_PLAYERS)
					BROADCAST_CUTSCENE_PLAYER_REQUEST(MC_playerBD[iPartToUse].iteam, FMMCCUT_MOCAP)
					PRINTLN("[LM][url:bugstar:5609784] - Setting PBBOOL_REQUEST_CUTSCENE_PLAYERS (1)")
				ENDIF
			ENDIF

			IF IS_THIS_MOCAP_IN_AN_APARTMENT( sMocapCutscene )
				SET_PLAYER_WANTED_LEVEL(LocalPlayer, 0)
				SET_PLAYER_WANTED_LEVEL_NOW(LocalPlayer)
				IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
					set_bit(iLocalBoolCheck8, LBOOL8_skipped_to_mid_mission_cutscene_stage_5)
					SET_BIT( MC_playerBD[ iLocalPart ].iClientBitSet2, PBBOOL2_I_AM_SPECTATOR_WATCHING_MOCAP )
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_MOCAP_CUTSCENE - Skipping because I'm SCTV and trying to watch an apartment cutscene")
					SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(MOCAPPROG_CLEANUP)				
					EXIT
				ENDIF
			ENDIF
			
			// These things only happen if we DON'T skip the cutscene
			IF NOT IS_BIT_SET(iLocalBoolCheck8, LBOOL8_skipped_to_mid_mission_cutscene_stage_5)
				IF Is_MP_Objective_Text_On_Display()
					Clear_Any_Objective_Text()
				ENDIF
				
				KILL_FACE_TO_FACE_CONVERSATION()
			ENDIF
			
			SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(MOCAPPROG_WAIT_PLAYER_SELECTION)
		BREAK
		
		CASE MOCAPPROG_WAIT_PLAYER_SELECTION			
			
			IF ARE_TEAM_CUTSCENE_PLAYERS_SELECTED(iCutsceneTeam)
		
				IF NOT IS_VECTOR_ZERO(vStartPos)
				OR (IS_THIS_MOCAP_IN_AN_APARTMENT(sMocapCutscene) AND NOT g_HeistGarageDropoffPanStarted AND NOT g_HeistApartmentDropoffPanStarted)

					IF IS_THIS_MOCAP_IN_AN_APARTMENT(sMocapCutscene) AND NOT g_HeistGarageDropoffPanStarted AND NOT g_HeistApartmentDropoffPanStarted
						vStartPos = GET_CUTSCENE_COORDS(iScriptedCutsceneTeam, eCutType)
						CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_MOCAP_CUTSCENE setting start position based on property drop off")
					ENDIF
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_MOCAP_CUTSCENE START POS VALID <<", vStartPos.X, ", ", vStartPos.Y, ", ",vStartPos.Z, ">>")
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD( PlayerPedToUse, vStartPos ) > fWarpRadius
						IF IS_BIT_SET( g_FMMC_STRUCT.sMocapCutsceneData[ iCutsceneToUse ].iCutsceneBitset, ci_CSBS_SkipIfTooFar )
							set_bit(iLocalBoolCheck8, LBOOL8_skipped_to_mid_mission_cutscene_stage_5)
							CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_MOCAP_CUTSCENE - Skipping because I'm too far")
							SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(MOCAPPROG_CLEANUP)
						ELSE
							IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
								SET_BIT(iLocalBoolCheck2,LBOOL2_ACTIVATE_MOCAP_CUTSCENE_WARP)
								CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_MOCAP_CUTSCENE - NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()")
								SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(MOCAPPROG_FADEOUT)
							ELSE
								CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_MOCAP_CUTSCENE IS_LOCAL_PLAYER_ANY_SPECTATOR() = TRUE")
								SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(MOCAPPROG_INTRO)
							ENDIF
						ENDIF
					ELSE
						CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_MOCAP_CUTSCENE GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PlayerPedToUse, vStartPos)(", GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PlayerPedToUse, vStartPos), ") <= fWarpRadius( ", fWarpRadius, ")")
						SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(MOCAPPROG_INTRO)
					ENDIF
					
				ELSE
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_MOCAP_CUTSCENE Start pos NOT valid, not doing distance checks - running cutscene anyway")
					SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(MOCAPPROG_INTRO)
				ENDIF
			
			ENDIF
		
		BREAK

		CASE MOCAPPROG_FADEOUT
		
			CPRINTLN( DEBUG_OWAIN, "[RCC MISSION] PROCESS_MOCAP_CUTSCENE Doing warp - Mocap stage: ", GET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS_NAME(GET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS()))
			PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE CASE 1 CURRENT NETWORK TIMER: ", NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()))
			PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE CASE 1 GET_NET_TIMER_DIFF", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_ServerBD.tdCutsceneStartTime[iScriptedCutsceneTeam],FALSE,TRUE),"[RCC MISSION] is host: ", bIsLocalPlayerHost)
			
			IF NOT IS_SCREEN_FADED_OUT()
			AND NOT IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_OUT(2000)
			ELSE
				IF IS_SCREEN_FADED_OUT()
					REMOVE_PLAYER_FROM_VEHICLE_AT_CUTSCENE_START(iCutsceneToUse, FALSE)
					
					// 2161860, make the warping player invisible 
					IF IS_ENTITY_VISIBLE( PLAYER_PED_ID() )
						SET_ENTITY_VISIBLE( PLAYER_PED_ID(), FALSE )
					ENDIF

					IF NOT busyspinner_is_on()

						BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("FMMC_PLYLOAD") 
						END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_LOADING))
						
					ENDIF

					IF NET_WARP_TO_COORD( vStartPos, fStartPos, !SHOULD_CUTSCENE_REMOVE_PLAYER_FROM_VEHICLE_AT_START( iCutsceneToUse, FALSE ), FALSE )
										
						IF IS_BIT_SET( iLocalBoolCheck2, LBOOL2_CUTSCENE_STREAMED )	
						OR IS_BIT_SET(iLocalBoolCheck31, LBOOL31_NOT_STREAMING_CUTSCENE)
							IF busyspinner_is_on()
								BusySpinner_off()
							ENDIF
							PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE LBOOL2_CUTSCENE_STREAMED")
							SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(MOCAPPROG_INTRO)
						ELSE
							PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE waiting for streaming: ", sMocapCutscene)
						ENDIF
					ENDIF
				ENDIF	
			ENDIF
		BREAK		
			
		CASE MOCAPPROG_INTRO
			
			IF IS_NEW_LOAD_SCENE_ACTIVE()
				NEW_LOAD_SCENE_STOP()
			ENDIF

			//Do specific mocap checks per cutscene
			DO_PRE_MOCAP_CHECKS(iCutsceneToUse)
			
			CLEAR_BIT(iLocalBoolCheck2,LBOOL2_ACTIVATE_MOCAP_CUTSCENE_WARP)
			SET_BIT(iLocalBoolCheck2,LBOOL2_CUTSCENE_WARP_DONE)
			
			SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(MOCAPPROG_APARTMENT)
		BREAK
		
		CASE MOCAPPROG_APARTMENT

			IF IS_THIS_MOCAP_IN_AN_APARTMENT(sMocapCutscene)
				PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE IS_THIS_MOCAP_IN_AN_APARTMENT = TRUE")
				IF Create_Heist_Cutscene_Doors(TRUE)
					PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE Create_Heist_Cutscene_Doors = TRUE")
					SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(MOCAPPROG_PROCESS_SHOTS)
				ELSE
					PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE 200 waiting for Create_Heist_Cutscene_Doors ")
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE IS_THIS_MOCAP_IN_AN_APARTMENT = FALSE")
				SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(MOCAPPROG_PROCESS_SHOTS)
			ENDIF

		BREAK
		
		CASE MOCAPPROG_PROCESS_SHOTS
			
			PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE CASE 3 CURRENT NETWORK TIMER: ", NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()))
			PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE CASE 3 GET_NET_TIMER_DIFF", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_ServerBD.tdCutsceneStartTime[iScriptedCutsceneTeam],FALSE,TRUE),"[RCC MISSION] is host: ", bIsLocalPlayerHost)
			
			BOOL bWarpAtEnd, bFadeAtEnd
			IF NOT IS_VECTOR_ZERO(vEndPos)
			AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed,vEndPos) > fWarpRadius
				AND NOT ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PRI_FIN_MCS2")
					bWarpAtEnd = TRUE
					bFadeAtEnd = TRUE
					PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - bWarpAtEnd = TRUE (Distance...)")
				ENDIF
						
				// There are certain conditions that require us to do a "WARPEND". Since there is no creator option we should use the conditions we know that will require it. Such as Rolling Start / Waiting for being in Vehicle.
				IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[ iCutsceneToUse ].iCutsceneBitset, ci_CSBS_Rolling_Start)
				AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
					bWarpAtEnd = TRUE
					bFadeAtEnd = TRUE
					PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - bWarpAtEnd = TRUE (Rolling Start...)")
				ENDIF
				IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[ iCutsceneToUse ].iCutsceneBitset2, ci_CSBS2_EndCutsceneInCover)
				AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
					bWarpAtEnd = TRUE
					bFadeAtEnd = TRUE
					PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - bWarpAtEnd = TRUE (End in Cover...)")
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[ iCutsceneToUse ].iCutsceneBitset2, ci_CSBS2_EndCutsceneRappelTask)
			AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
				bFadeAtEnd = TRUE
				PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - bFadeAtEnd = TRUE (End in Rappel...)")
			ENDIF
			
			IF DOES_ENTITY_EXIST(ClonePlayerForCutscene) 
			AND IS_ENTITY_VISIBLE(ClonePlayerForCutscene)
				NETWORK_FADE_OUT_ENTITY(ClonePlayerForCutscene, FALSE, TRUE)
			ENDIF
						
			IF RUN_MOCAP_CUTSCENE(sMocapCutscene, FMMCCUT_MOCAP, FALSE, 
				vStartPos, fStartPos, vEndPos, fEndPos,
				NOT IS_BIT_SET(iLocalBoolCheck7, LBOOL7_HIDE_MOCAP_PLAYERS), DEFAULT, DEFAULT, DEFAULT, bFadeAtEnd)
			
				IF NOT IS_BIT_SET(iLocalBoolCheck7, LBOOL7_HIDE_MOCAP_PLAYERS)
					LOAD_RANDOM_RE_ENTRY_INTERACTION_ANIM() // neilf. so the re-entry anims will be loaded by the time gameplay resumes.
				ENDIF
				
				PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE RUN_MOCAP_CUTSCENE = TRUE.")
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_EquipOutfitForNextRule)
					SET_BIT(iLocalBoolCheck26, LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE)
					PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - Setting LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE")
				ENDIF
				
				IF bWarpAtEnd
					PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE Warping the local ped after a cutscene to their end location")
					SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(MOCAPPROG_WARPEND)
				ELSE
				
					IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
						PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE IS_LOCAL_PLAYER_ANY_SPECTATOR = false.")
						DO_POST_CUTSCENE_COMMON_PLAYER_EXIT(iCutsceneToUse, FMMCCUT_MOCAP)
					ENDIF
				
					PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE Cleaning up. We're too close to the warp radius or MPH_PRI_FIN_MCS2 is being used.")
					SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(MOCAPPROG_ENDING_PROCEDURES)
				ENDIF

				IF GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].propertyDetails.iCurrentlyInsideProperty != -1
					Destroy_Heist_Cutscene_Doors()
				ENDIF
			
			ELSE

				//-- If we're changing outfits at the end of the cutscene, attenpt to preload the outfit (bit rough!)
				IF NOT IS_PLAYER_SPECTATING(LocalPlayer)
				AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)
					IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_EquipOutfitForNextRule)
						IF NOT IS_BIT_SET(iLocalBoolCheck28, LBOOL28_PRELOAD_OUTFIT_CHANGE_AT_END_OF_CUTSCENE)
							
							INT iRuleOutfit
							iRuleOutfit = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iOutfit[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]]
							IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iOutfitVariations[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] > 0
								iRuleOutfit += NATIVE_TO_INT(LocalPlayer) % g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iOutfitVariations[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]]
							ENDIF
						//	BOOL bOutfitSet = FALSE
							

							IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] + 1 < FMMC_MAX_RULES
								IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iOutfit[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] + 1] != -1
									iRuleOutfit =  g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iOutfit[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] + 1]
									
									IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iOutfitVariations[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] + 1] > 0
										iRuleOutfit += NATIVE_TO_INT(LocalPlayer) % g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iOutfitVariations[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] + 1]
									ENDIF
								ENDIF
							ENDIF
							
							
							sApplyOutfitData.pedID 		= LocalPlayerPed
							sApplyOutfitData.eOutfit 	= int_to_enum(MP_OUTFIT_ENUM,iRuleOutfit)
							
							IF iRuleOutfit > -1
								
								if PRELOAD_MP_OUTFIT(sApplyOutfitData,g_sOutfitsData)
									PRINTLN("[RCC MISSION] [PROCESS_MOCAP_CUTSCENE] [DSW] Preloaded! Set LBOOL28_PRELOAD_OUTFIT_CHANGE_AT_END_OF_CUTSCENE")
									SET_BIT(iLocalBoolCheck28, LBOOL28_PRELOAD_OUTFIT_CHANGE_AT_END_OF_CUTSCENE)
								ELSE
									PRINTLN("[RCC MISSION] [PROCESS_MOCAP_CUTSCENE] [DSW] Trying to preload outfit SET_PED_MP_OUTFIT... ePreloadStage: ", sApplyOutfitData.ePreloadStage, "	iRuleOutfit: ", iRuleOutfit)
								ENDIF
							ENDIF
						ENDIF	
					ENDIF 
				ENDIF
			ENDIF
			
			// Initialise the grace period timer after a mocap has finished
			REINIT_NET_TIMER(stGracePeriodAfterCutscene)
			
		BREAK
		
		CASE MOCAPPROG_WARPEND
			// Never fade out if the celebration screen is up
			IF IS_BIT_SET( iCelebrationBitSet, ciCELEBRATION_BIT_SET_bStartedCelebrationBeforeCutsceneEnded )
			OR IS_BIT_SET( iLocalBoolCheck8, LBOOL8_SHOULD_CELEBRATION_BE_SHOWN_PREMATURELY )	
			OR GET_TOGGLE_PAUSED_RENDERPHASES_STATUS() = FALSE // B*4151222
				PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - Skipping Fade out/in")
				SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(MOCAPPROG_ENDING_PROCEDURES)
				EXIT
			ENDIF
		
			IF NOT IS_SCREEN_FADED_OUT()
			AND NOT IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_OUT(MOCAP_END_FADE_DURATION)
			ELSE
				IF IS_SCREEN_FADED_OUT()
					IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
						SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(MOCAPPROG_ENDING_PROCEDURES)
					ELSE
						
						IF IS_PED_IN_ANY_VEHICLE(localPlayerPed, TRUE)		
							VEHICLE_INDEX vehIndex
							vehIndex = GET_VEHICLE_PED_IS_IN(localPlayerPed, TRUE)
							IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(vehIndex)
								PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - MOCAPPROG_WARPEND - IN VEHICLE - No net control of entity")
								NETWORK_REQUEST_CONTROL_OF_ENTITY(vehIndex)
								EXIT
							ELSE
								PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - MOCAPPROG_WARPEND - IN VEHICLE - We now have control!")
							ENDIF
						ENDIF
						
						// B*4251737 - added to hold up the net warp until the Enter Vehicle task has finished
						// See Paulius's comments in B*4212309
						SCRIPTTASKSTATUS taskStatus
						taskStatus = GET_SCRIPT_TASK_STATUS(LocalPlayerPed, SCRIPT_TASK_ENTER_VEHICLE)
						IF taskStatus != WAITING_TO_START_TASK
						AND taskStatus != PERFORMING_TASK
						
							IF NET_WARP_TO_COORD(vEndPos, fEndPos, !SHOULD_CUTSCENE_REMOVE_PLAYER_FROM_VEHICLE_AT_START(iCutsceneToUse, FALSE), FALSE )								
								DO_POST_CUTSCENE_COMMON_PLAYER_EXIT(iCutsceneToUse, FMMCCUT_MOCAP)
								SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(MOCAPPROG_ENDING_PROCEDURES)
							ENDIF
							
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF	
			
		BREAK
		
		CASE MOCAPPROG_ENDING_PROCEDURES
			IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
				IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitset2, ci_CSBS2_EndCutsceneInCover)
					PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - MOCAPPROG_ENDING_PROCEDURES - ci_CSBS2_EndCutsceneInCover")
					IF NOT SET_PLAYER_INTO_COVER_AFTER_CUTSCENE(iCutsceneToUse)
						EXIT
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_fmmc_struct.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_UseDamagedCasinoDoorsIPL)
				PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - MOCAPPROG_ENDING_PROCEDURES - ci_CSBS2_UseDamagedCasinoDoorsIPL is SET, calling TOGGLE_HIDE_CASINO_FRONT_DOOR_IPL(TRUE)")
				TOGGLE_HIDE_CASINO_FRONT_DOOR_IPL(TRUE)
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_EndCutsceneRappelTask)
				SET_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_RUNNING_WALL_RAPPEL_NOT_ON_RAPPEL_RULE)
				PRINTLN("[WallRappel] MOCAPPROG_ENDING_PROCEDURES - Setting PBBOOL4_RUNNING_WALL_RAPPEL_NOT_ON_RAPPEL_RULE")
			ENDIF
			
			IF IS_BIT_SET(g_fmmc_struct.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_SyncAllPlayersAtEnd)
				IF NOT IS_BIT_SET(MC_playerBD[iLocaLPart].iClientBitSet4, PBBOOL4_SYNC_PLAYERS_FOR_END_OF_CUTSCENE)
					PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - MOCAPPROG_ENDING_PROCEDURES - setting PBBOOL4_SYNC_PLAYERS_FOR_END_OF_CUTSCENE")
					SET_BIT(MC_playerBD[iLocaLPart].iClientBitSet4, PBBOOL4_SYNC_PLAYERS_FOR_END_OF_CUTSCENE)
				ENDIF
				
				IF NOT ARE_ALL_PLAYERS_SYNCED_FOR_END_OF_CUTSCENE()
					PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - MOCAPPROG_ENDING_PROCEDURES - iCutsceneToUse: ", iCutsceneToUse, ", exiting syncing players")
					EXIT
				ENDIF
			ENDIF
			
			SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(MOCAPPROG_CLEANUP)
		BREAK
		
		CASE MOCAPPROG_CLEANUP
		
			// Special case camera for a car driving out of a cargo container - Prison Break Setup: Station Team 2
			
			make_all_casco_players_visible_locally(sMocapCutscene)
			
			RESET_NET_TIMER(tdEnterCoverEndingCutsceneTimeOut)
			
			if are_strings_equal(sMocapCutscene, "mph_pri_sta_mcs1") //prison - station casco. Proof deactivation via PROCESS_BLOW_OPEN_DOOR()
				set_entity_proofs(player_ped_id(), false, false, false, false, false)
			endif 
			
			if process_ingame_features_for_mph_pri_sta_mcs1()
			and is_player_registered_for_cutscene(iScriptedCutsceneTeam) 
			AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
			AND bLocalPlayerOK
			AND NOT g_bMissionOver // Doesn't matter if the other team fails first, we'll get set as failed soon after
				
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_LR)

								DISABLE_CAMERA_VIEW_MODE_CYCLE(player_id())

				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_MOCAP_CUTSCENE LBOOL7_PRISON_BREAK_CARGO_SCENE_TRIGGERED " )

				
				//only cache the vehicle index of the casco once
				if not does_entity_exist(casco_vehicle)
					if MC_serverBD_1.sFMMC_SBD.casco_index != -1
		
						casco_vehicle = net_to_veh(MC_serverBD_1.sFMMC_SBD.niVehicle[MC_serverBD_1.sFMMC_SBD.casco_index])
						
						PRINTLN("[RCC MISSION] casco_vehicle", native_to_int(casco_vehicle))
					endif 
				endif 

				if does_entity_exist(casco_vehicle)
					if is_vehicle_driveable(casco_vehicle)
				
						IF( GET_ENTITY_SPEED( casco_vehicle ) < 3.0 )
							IF( GET_PED_IN_VEHICLE_SEAT( casco_vehicle, VS_DRIVER ) = LocalPlayerPed )
								DISPLAY_HELP_TEXT_THIS_FRAME( "HEIST_HELP_26", FALSE )
							ENDIF
						ENDIF

						IF NETWORK_HAS_CONTROL_OF_entity(casco_vehicle)
							modify_vehicle_top_speed(casco_vehicle, 25.00) 
							PRINTLN("[RCC MISSION] speed PRISON_BREAK_CARGO_SCENE - speed 70%" )
						else 
							PRINTLN("[RCC MISSION] speed 0" )
						endif
						
						//we are in third person mode. First person does not use the static scripted camera
						IF( VDIST2( GET_ENTITY_COORDS( LocalPlayerPed, FALSE ), <<872.91, -2878.82, 18.41>> ) > 65.0 ) 
		
							//player is in third person mode if the scripted camera exists
							if does_cam_exist(cutscenecam)
							
								SET_FOLLOW_VEHICLE_CAM_VIEW_MODE(CAM_VIEW_MODE_THIRD_PERSON_MEDIUM)
								
								SET_GAMEPLAY_CAM_RELATIVE_HEADING()
								
								// calculate the interp time for the switch between scripted cam and gameplay cam
								INT iInterpTime
								
								IF IS_ENTITY_ALIVE(casco_vehicle) 
									FLOAT fSpeedUnit
									// the faster the vehicle the quicker the interp min 500, max 3000
									fSpeedUnit = CLAMP( GET_ENTITY_SPEED( casco_vehicle ), 0.0, 13.0 ) / 13.0
										
									iInterpTime = 750 + ROUND( ( 1.0 - fSpeedUnit ) * 2500.0 )
								ENDIF
								
								RENDER_SCRIPT_CAMS( FALSE, TRUE, iInterpTime )
								
								DESTROY_CAM( cutscenecam )
								
								PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE player using third person camera")
							else
								PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE player using first person camera")
							endif 
												
							CLEAR_BIT( iLocalBoolCheck7, LBOOL7_PRISON_BREAK_CARGO_SCENE_TRIGGERED )
							CPRINTLN( DEBUG_SIMON, " PROCESS_MOCAP_CUTSCENE PRISON_BREAK_CARGO_SCENE - Finished 0" )			
							
						endif
						
					else 
						
						RENDER_SCRIPT_CAMS(false, false)
						DESTROY_CAM( cutscenecam )
						CLEAR_BIT( iLocalBoolCheck7, LBOOL7_PRISON_BREAK_CARGO_SCENE_TRIGGERED )
						
						set_bit(iLocalBoolCheck8, LBOOL8_forces_applied_to_casco_vehicle)
						
						CPRINTLN( DEBUG_SIMON, " PROCESS_MOCAP_CUTSCENE PRISON_BREAK_CARGO_SCENE - Finished 1 HACK FIX" )
						
						PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE speed 1" )
					endif 
					
				else 
							
					RENDER_SCRIPT_CAMS(false, false)
					DESTROY_CAM( cutscenecam )
					CLEAR_BIT( iLocalBoolCheck7, LBOOL7_PRISON_BREAK_CARGO_SCENE_TRIGGERED )
					
					set_bit(iLocalBoolCheck8, LBOOL8_forces_applied_to_casco_vehicle)
					
					CPRINTLN( DEBUG_SIMON, " PROCESS_MOCAP_CUTSCENE PRISON_BREAK_CARGO_SCENE - Finished 2 HACK FIX" )
					
					PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE speed 2" )
				
				endif 

				if not IS_BIT_SET(iLocalBoolCheck8, LBOOL8_forces_applied_to_casco_vehicle)
					if apply_force_to_caso_system(casco_vehicle)
						set_bit(iLocalBoolCheck8, LBOOL8_forces_applied_to_casco_vehicle)
					endif 
				endif 
				
			ELSE
				IF NOT IS_CUTSCENE_PLAYING()
					
					CLEANUP_CUTSCENE_SPECIAL_CASE_LOGIC(sMocapCutscene)
					
					//********casco cutscene, mission = prison break setup station. LK ********
					if are_strings_equal(sMocapCutscene, "mph_pri_sta_mcs1") 
						
						if does_entity_exist(casco_vehicle)
							if is_vehicle_driveable(casco_vehicle)
								if network_has_control_of_entity(casco_vehicle)
								
									modify_vehicle_top_speed(casco_vehicle, 0)
									
									SET_ENTITY_PROOFS(casco_vehicle, FALSE, TRUE, FALSE, TRUE, FALSE)
									
									PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE vehicle speed RESET%")
								endif 
							endif 
						endif 
					endif
					
					//NOTE(Owain): Fix for 2476052 - if a player has died before the bank entry cutscene, we want
					// to ensure they are inside the bank by the end of the cutscene.
					IF ARE_STRINGS_EQUAL( sMocapCutscene, "mph_pac_fin_mcs1" )
						IF GET_INTERIOR_FROM_ENTITY( LocalPlayerPed ) = NULL
							CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_MOCAP_CUTSCENE Local player ped is not in any interior after mph_pac_fin_mcs1- moving them inside the bank." )
							SET_ENTITY_COORDS( LocalPlayerPed , << 236.7164, 213.4339, 105.2868 >>  )
							SET_ENTITY_HEADING( LocalPlayerPed, 270.6 )
						ELSE
							CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_MOCAP_CUTSCENE Local player ped is correctly inside an interior after mph_pac_fin_mcs1" )
						ENDIF
					ENDIF
					
					SET_PED_CONFIG_FLAG(LocalPlayerPed,PCF_PreventAutoShuffleToDriversSeat,FALSE)
					PRINTLN("[RCC MISSION] - PROCESS_MOCAP_CUTSCENE - Set PCF_PreventAutoShuffleToDriversSeat, false")
					
					IF ARE_STRINGS_EQUAL(sMocapCutscene, "subj_mcs1")
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableTakeOffScubaGear, FALSE)
						CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] - PROCESS_MOCAP_CUTSCENE - PCF_DisableTakeOffScubaGear FALSE")
					ENDIF
										
					// url:bugstar:5781840 | A fadeout happened in the run mocap state, but I can't find anything that's supposed to fade it back in. (mid-mission)
					IF eCutType != FMMCCUT_ENDMOCAP
					AND SHOULD_FADE_MOCAP_IN_AT_END()
						CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] - PROCESS_MOCAP_CUTSCENE - Calling DO_SCREEN_FADE_IN.")
						DO_SCREEN_FADE_IN(MOCAP_END_FADE_DURATION)
					ENDIF
			
					TOGGLE_PLAYER_DAMAGE_OVERLAY(TRUE)
					SET_PLAYER_INVINCIBLE( LocalPlayer, FALSE )
					RUN_LOCAL_PLAYER_PROOF_SETTING(TRUE)
				
					PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE finished ")
					
					GIVE_PLAYER_WEAPON_AFTER_HEIST_CUTSCENE(sMocapCutscene)
					
					DO_POST_MOCAP_CHECKS()
					
					SET_BIT( MC_playerBD[iLocalPart].iCutsceneFinishedBitset, g_iFMMCScriptedCutscenePlaying)					
					SET_BIT( MC_playerBD[ iLocalPart ].iClientBitSet2, PBBOOL2_FINISHED_CUTSCENE )				
					CLEAR_BIT( MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYING_CUTSCENE )
					CLEAR_BIT(iLocalBoolCheck31, LBOOL31_PRIZED_VEHICLE_SHOULD_HIDE)
					
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_MOCAP_CUTSCENE Setting cutscene as finished: ", g_iFMMCScriptedCutscenePlaying)
					
					CLEAR_BIT(iLocalBoolCheck9,LBOOL9_CACHED_WEAPON_DATA)					
					CLEAR_BIT(iLocalBoolCheck5,LBOOL5_PED_INVOLVED_IN_CUTSCENE)
					CLEAR_BIT(iLocalBoolCheck5,LBOOL5_MID_MISSION_MOCAP)
					CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_REQUEST_CUTSCENE_PLAYERS)					
					CLEAR_BIT( iLocalBoolCheck7, LBOOL7_SHOULD_HOLD_UP_MOCAP_FOR_CUTSCENE )
					CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_REACHED_CUT_PROCESS_SHOTS)
					RESET_NET_TIMER(tdCutsceneWaitForEntitiesTimer)
					
					g_bInMissionControllerCutscene = FALSE
					
					IF NOT (IS_THIS_MOCAP_IN_AN_APARTMENT( sMocapCutscene )
					AND IS_LOCAL_PLAYER_ANY_SPECTATOR())
					AND SHOULD_FADE_MOCAP_IN_AT_END(TRUE)
						IF IS_SCREEN_FADED_OUT()
							DO_SCREEN_FADE_IN( 500 )
						ENDIF
					ENDIF

					sMocapCutscene = NULL
					PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE SETTING sMocapCutscene to NULL - cutscene finished 1")
					
					NETWORK_SET_VOICE_ACTIVE(true)
					
					REMOVE_CUTSCENE()
					DISABLE_SPECTATOR_FILTER_OPTION( FALSE )
					SET_MODEL_AS_NO_LONGER_NEEDED( MP_F_FREEMODE_01 )
					SET_MODEL_AS_NO_LONGER_NEEDED( MP_M_FREEMODE_01 )
					SET_MODEL_AS_NO_LONGER_NEEDED( PROP_CS_CASHENVELOPE )
					CLEAR_BIT( iLocalBoolCheck2, LBOOL2_CUTSCENE_STREAMED )
					
					iCutComponentPlayerNum = 0
					SET_MOCAP_STREAMING_STAGE(MOCAP_STREAMING_STAGE_INIT)
					iScriptedCutsceneProgress = 0
					iMocapCutsceneProgress = 0
					
					SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(MOCAPPROG_INIT)
					SET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS(MOCAPRUNNING_STAGE_0)
					
					RESET_NET_TIMER(mocapFailsafe)
					
					iScriptedCutsceneTeam = -1
					g_iFMMCScriptedCutscenePlaying= -1
					iCutsceneStreamingTeam = -1
					RESET_NET_TIMER(tdCutsceneMidpoint)
					
					CLEAR_BIT(iLocalBoolCheck7, LBOOL7_HIDE_MOCAP_PLAYERS)
					CLEAR_BIT(iLocalBoolCheck2,LBOOL2_ACTIVATE_MOCAP_CUTSCENE_WARP)
					CLEAR_BIT(iLocalBoolCheck2,LBOOL2_CUTSCENE_WARP_DONE)
					//CLEAR_BIT(iLocalBoolCheck7, LBOOL7_FORCE_NIGHTVISION_MOCAP)
					
					// Force update godtext after a cutscene 
					PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_MOCAP_CUTSCENE")				
					SET_BIT( iLocalBoolCheck3, LBOOL3_UPDATE_VEHICLE_OBJECTIVE ) 
					PRINTLN("[JS][UOBJ - PED] - Requested Objective Update - PROCESS_MOCAP_CUTSCENE")				
					SET_BIT( iLocalBoolCheck3, LBOOL3_UPDATE_PED_OBJECTIVE ) 
					PRINTLN("[JS][UOBJ - OBJECT] - Requested Objective Update - PROCESS_MOCAP_CUTSCENE")				
					SET_BIT( iLocalBoolCheck3, LBOOL3_UPDATE_OBJECT_OBJECTIVE ) 
					SET_BIT( MC_playerBD[ iLocalPart].iClientBitSet2, PBBOOL2_FINISHED_CUTSCENE )
					
					CLEAR_BIT( MC_playerBD[ iLocalPart ].iClientBitSet2, PBBOOL2_IN_MID_MISSION_CUTSCENE )
					
					//gdisablerankupmessage = FALSE
					SET_DISABLE_RANK_UP_MESSAGE(FALSE)					

					clear_bit(iLocalBoolCheck8, LBOOL8_skipped_to_mid_mission_cutscene_stage_5)					
					
					IF IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
						SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY(FALSE)
						PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE setting SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY false - 1")
					ENDIF
					CLEAR_IM_WATCHING_A_MOCAP()
					CLEANUP_MP_CUTSCENE()
					
					sMocapCutscene = null
					PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE SETTING sMocapCutscene to NULL - cutscene finished 2")
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH

ENDPROC


//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: SCRIPTED CUTSCENES !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: CCTV CAMERA CUTSCENES !
//
//************************************************************************************************************************************************************



PROC SETUP_MANUAL_CCTV_HELP()
	
	CLEAR_MENU_DATA()
	SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT)

	SET_MENU_ITEM_TOGGLEABLE(FALSE)
	
	LOAD_MENU_ASSETS()
	REMOVE_MENU_HELP_KEYS()
	IF iSpectatorTarget = -1
		ADD_MENU_HELP_KEY_INPUT( INPUT_SCRIPT_RIGHT_AXIS_X, "CCTV_PAN")	//pan
		ADD_MENU_HELP_KEY_INPUT( INPUT_SCRIPT_LEFT_AXIS_Y, "CCTV_ZOO")	//zoom
		ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_DPAD_LR, "CCTV_CH")	//change feed
	ENDIF
	
	PRINTLN("SETUP_MANUAL_CCTV_HELP() - adding menu" )

ENDPROC

VECTOR vCCTVRot[MAX_CAMERA_SHOTS]
FLOAT fCCTVFOV[MAX_CAMERA_SHOTS]

PROC UPDATE_CAM_PARAMS_FOR_CCTV(INT iThisCutscene,INT iThisCamShot)
	IF( iThisCamShot - 1 >= 0 )
		IF( IS_BIT_SET( g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].iCutsceneShotBitSet[iThisCamShot - 1], ciCSS_BS_Attach_Enable ) )
			DETACH_CAM( cutscenecam )
			DESTROY_CAM( cutscenecam )
			cutscenecam = CREATE_CAM( "DEFAULT_SCRIPTED_CAMERA", TRUE ) 
			PRINTLN("UPDATE_CAM_PARAMS_FOR_CCTV - detaching cam" )
		ENDIF
	ENDIF
	
	VECTOR vCCTVUpdateCamRot
	FLOAT fCCTVUpdateFov
	
	vCCTVUpdateCamRot = g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].vCamStartRot[iThisCamShot]
	IF NOT IS_VECTOR_ZERO(vCCTVRot[iThisCamShot])
		vCCTVUpdateCamRot.z = vCCTVRot[iThisCamShot].z
	ENDIF
	
	fCCTVUpdateFov = fCCTVFOV[iThisCamShot]
	
	IF fCCTVUpdateFov > 0
		SET_CAM_PARAMS(cutscenecam,g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].vCamStartPos[iThisCamShot],vCCTVUpdateCamRot,fCCTVUpdateFov)
		
		PRINTLN("UPDATE_CAM_PARAMS_FOR_CCTV - setting new params" )
		PRINTLN("UPDATE_CAM_PARAMS_FOR_CCTV - iThisCamShot:", iThisCamShot)
		PRINTLN("UPDATE_CAM_PARAMS_FOR_CCTV - vCCTVUpdateCamRot: ", vCCTVUpdateCamRot )
		PRINTLN("UPDATE_CAM_PARAMS_FOR_CCTV - fCCTVUpdateFov: ",fCCTVUpdateFov )
	ELSE
		SET_CAM_PARAMS(cutscenecam,g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].vCamStartPos[iThisCamShot],vCCTVUpdateCamRot,g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].fStartCamFOV[iThisCamShot])
		PRINTLN("UPDATE_CAM_PARAMS_FOR_CCTV - iThisCamShot:", iThisCamShot)
		PRINTLN("UPDATE_CAM_PARAMS_FOR_CCTV - setting new params" )
		PRINTLN("UPDATE_CAM_PARAMS_FOR_CCTV - vCCTVUpdateCamRot: ", vCCTVUpdateCamRot )
		PRINTLN("UPDATE_CAM_PARAMS_FOR_CCTV - fCCTVUpdateFov: ",g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].fStartCamFOV[iThisCamShot] )
	ENDIF
	
	IF( IS_BIT_SET( g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].iCutsceneShotBitSet[iCamShot], ciCSS_BS_Attach_Enable ) )
		PRINTLN("UPDATE_CAM_PARAMS_FOR_CCTV - attaching cam" )
	
		INT 			iVehIndex 	= g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].iVehicleToAttachTo[iThisCamShot]
		NETWORK_INDEX	niVeh		= MC_serverBD_1.sFMMC_SBD.niVehicle[ iVehIndex ]
		VEHICLE_INDEX 	veh			= NULL
		
		IF( NETWORK_DOES_NETWORK_ID_EXIST( niVeh ) )
			veh = NET_TO_VEH( niVeh )
			
			ATTACH_CAM_TO_ENTITY( cutscenecam, veh, g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].vOffsetPos[iThisCamShot], TRUE )
			POINT_CAM_AT_ENTITY( cutscenecam, veh, g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].vLookAtPos[iThisCamShot], TRUE )
		ELSE
			ASSERTLN( "UPDATE_CAM_PARAMS_FOR_CCTV - Couldn't get cam attach entity" )
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].iShakeType[iThisCamShot] != -1
	AND g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].fShakeMag[iThisCamShot] != 0
		SHAKE_CAM(cutscenecam,GET_CAM_SHAKE_TYPE_FROM_INT(g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].iShakeType[iThisCamShot]),g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].fShakeMag[iThisCamShot])
		PRINTLN("UPDATE_CAM_PARAMS_FOR_CCTV - shaking cam" )
	ELSE
		STOP_CAM_SHAKING(cutscenecam,TRUE)
		PRINTLN("UPDATE_CAM_PARAMS_FOR_CCTV - stop shaking cam" )
	ENDIF

ENDPROC

PROC UPDATE_CCTV_WITH_OVERRIDDEN_CONSTRAINTS(INT iCutscene, INT iShot, FLOAT fMovementX)
	FLOAT fMinBound = 360 - g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].fOverrideRotMin[iShot]
	FLOAT fMaxBound = 360 - g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].fOverrideRotMax[iShot]

	//If out of 360 to -360 bounds, cap it to back within those. We do calculations with the assumption that
	//	the rotation is without those bounds.
	IF vCCTVRot[iShot].z > 360
		vCCTVRot[iShot].z -= 360 * ABSI(FLOOR(vCCTVRot[iShot].z / 360))
	ELIF vCCTVRot[iShot].z < -360
		vCCTVRot[iShot].z += 360 * ABSI(FLOOR(vCCTVRot[iShot].z / 360))
	ENDIF
	
	FLOAT fActualRot = ABSF(360 - vCCTVRot[iShot].z)
	
	IF fActualRot > 360
		fActualRot -= 360
	ENDIF
	
	IF (fMovementX > 0 AND ABSF(fActualRot - fMaxBound) > 1) //Panning to the right
	OR (fMovementX < 0 AND ABSF(fActualRot - fMinBound) > 1) //Panning to the left
		vCCTVRot[iShot].z += (fMovementX * -1)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF bOverrideCCTVRotDebug
			
			TEXT_LABEL_63 tl63
			
			tl63 = "Cutscene "
			tl63 += iCutscene
			tl63 += ", Shot "
			tl63 += iShot
			tl63 += ". fMovementX: "
			tl63 += GET_STRING_FROM_FLOAT(fMovementX, 3)
			DRAW_DEBUG_TEXT_2D(tl63, << 0.4, 0.4, 0 >>)
			
			tl63 = "Start Rot: "
			tl63 += GET_STRING_FROM_VECTOR(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].vCamStartRot[iShot])
			DRAW_DEBUG_TEXT_2D(tl63, << 0.6, 0.4, 0 >>)
			
			tl63 = "Rot: "
			tl63 += GET_STRING_FROM_FLOAT(vCCTVRot[iShot].z, 3)
			tl63 += ", ActualRot: "
			tl63 += GET_STRING_FROM_FLOAT(fActualRot, 3)
			DRAW_DEBUG_TEXT_2D(tl63, << 0.4, 0.416, 0 >>)
			
			tl63 = "Min: "
			tl63 += GET_STRING_FROM_FLOAT(fMinBound)
			tl63 += ", Max: "
			tl63 += GET_STRING_FROM_FLOAT(fMaxBound)
			DRAW_DEBUG_TEXT_2D(tl63, << 0.4, 0.432, 0 >>)
			
			VECTOR vMinLineEndPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].vCamStartPos[iShot], g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].fOverrideRotMin[iShot], <<0,13.5,0>>)
			VECTOR vMaxLineEndPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].vCamStartPos[iShot], g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].fOverrideRotMax[iShot], <<0,13.5,0>>)
			
			DRAW_DEBUG_LINE(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].vCamStartPos[iShot], vMinLineEndPos)
			DRAW_DEBUG_LINE(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].vCamStartPos[iShot], vMaxLineEndPos)
		ENDIF
	#ENDIF
ENDPROC

FLOAT fCamMoveSensitivity = 0.5
FLOAT fThisPadRightX, fThisPadLeftY
FLOAT fXMove, fYMove
//FLOAT fRightBounds = 4.5
//FLOAT fLeftBounds = 49

PROC MANAGE_MANUAL_CCTV_CONTROL(INT iThisCutscene,INT iThisCamShot)

	fThisPadRightX = GET_DISABLED_CONTROL_NORMAL (FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X)
	fThisPadLeftY = GET_DISABLED_CONTROL_NORMAL (FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y)

	fXMove = fThisPadRightX * fCamMoveSensitivity 
    fYMove = fThisPadLeftY * fCamMoveSensitivity 
	fYMove = fYMove * - 1
	
	BOOL bUsingOverriddenRotConstraints = FALSE
	
	IF iThisCutscene > -1
	AND iThisCamShot > -1
		bUsingOverriddenRotConstraints = IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].iCutsceneShotBitSet[iThisCamShot], ciCSS_BS_OverrideRotationBounds)
	ENDIF
	
	IF bUsingOverriddenRotConstraints
		UPDATE_CCTV_WITH_OVERRIDDEN_CONSTRAINTS(iThisCutscene, iThisCamShot, fXMove)
	ELSE
		IF iThisCamShot = 0 
			//- right
			IF fXMove > 0
				PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - trying to pan right")
				IF vCCTVRot[iThisCamShot].z > 5
					vCCTVRot[iThisCamShot].z = vCCTVRot[iThisCamShot].z  +  (fXMove * - 1)
				ELSE
					PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - bounds met for panning right")
					PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - CCTV Rot is: ", vCCTVRot[iThisCamShot].z)
					PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - CCTV max rot is: 5 ")
				ENDIF
			ENDIF
			
			//-left
			IF fXMove < 0 
				PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - trying to pan left")
				IF vCCTVRot[iThisCamShot].z < 96
					vCCTVRot[iThisCamShot].z = vCCTVRot[iThisCamShot].z  + (fXMove * - 1)
				ELSE
					PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - bounds met for panning left")
					PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - CCTV Rot is: ", vCCTVRot[iThisCamShot].z)
					PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - CCTV left bound is: 96")
				ENDIF
			ENDIF
		ENDIF
		
		IF iThisCamShot = 1 
			//- right
			IF fXMove > 0
				PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - trying to pan right")
				IF vCCTVRot[iThisCamShot].z > 107
					vCCTVRot[iThisCamShot].z = vCCTVRot[iThisCamShot].z  +  (fXMove * - 1)
				ELSE
					PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - bounds met for panning right")
					PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - CCTV Rot is: ", vCCTVRot[iThisCamShot].z)
					PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - CCTV max rot is: 107 ")
				ENDIF
			ENDIF
			
			//-left
			IF fXMove < 0 
				PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - trying to pan left")
				IF vCCTVRot[iThisCamShot].z < 180
					vCCTVRot[iThisCamShot].z = vCCTVRot[iThisCamShot].z  + (fXMove * - 1)
					PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - greater than 0 pan left freely")
				ELSE
					IF vCCTVRot[iThisCamShot].z < 208
						vCCTVRot[iThisCamShot].z = vCCTVRot[iThisCamShot].z  + (fXMove * - 1)
					ELSE
						PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - bounds met for panning left")
						PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - CCTV Rot is: ", vCCTVRot[iThisCamShot].z)
						PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - CCTV left bound is: -153")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF iThisCamShot = 2 
			//- right
			IF fXMove > 0
				PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - trying to pan right")
				IF vCCTVRot[iThisCamShot].z > 304
					vCCTVRot[iThisCamShot].z = vCCTVRot[iThisCamShot].z  +  (fXMove * - 1)
				ELSE
					PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - bounds met for panning right")
					PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - CCTV Rot is: ", vCCTVRot[iThisCamShot].z)
					PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - CCTV max rot is: 5 ")
				ENDIF
			ENDIF
			
			//-left
			IF fXMove < 0 
				PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - trying to pan left")
				IF vCCTVRot[iThisCamShot].z < 432
					vCCTVRot[iThisCamShot].z = vCCTVRot[iThisCamShot].z  + (fXMove * - 1)
				ELSE
					PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - bounds met for panning left")
					PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - CCTV Rot is: ", vCCTVRot[iThisCamShot].z)
					PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - CCTV left bound is: 49")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// Zoom
	//In
	IF fYMove > 0
		PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - trying to zoom in")
		IF fCCTVFOV[iThisCamShot] > g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].fStartCamFOV[iThisCamShot] - 20
			fCCTVFOV[iThisCamShot] = fCCTVFOV[iThisCamShot]  + (fYMove * - 1)
			PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - decreasing FOV by: ", (fYMove * - 1))
			PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - Zoom in - New FOV value: ", fCCTVFOV[iThisCamShot])
		ELSE
			PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - bounds met for zooming in")
			PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - CCTV zoom is: ", fCCTVFOV[iThisCamShot])
			PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - CCTV max zoom is: ", g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].fStartCamFOV[iThisCamShot] - 20)
		ENDIF
	ENDIF
	
	//Out
	IF fYMove < 0
		PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - trying to zoom out")
		IF fCCTVFOV[iThisCamShot] < g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].fStartCamFOV[iThisCamShot] + 20
			fCCTVFOV[iThisCamShot] = fCCTVFOV[iThisCamShot]  + (fYMove * - 1)
			PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - increasing FOV by: ", (fYMove * - 1))
			PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - Zoom out - New FOV value: ", fCCTVFOV[iThisCamShot])
		ELSE
			PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - bounds met for zooming out")
			PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - CCTV zoom is: ", fCCTVFOV[iThisCamShot])
			PRINTLN("[MANAGE_MANUAL_CCTV_CONTROL] - CCTV min zoom is: ", g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].fStartCamFOV[iThisCamShot] + 20)
		ENDIF
	ENDIF

ENDPROC

PROC MANAGE_CAM_AUDIO(INT iThisCutscene,INT iThisCamShot, BOOL bManualControl)
		
	IF eLiftState > eLMS_WaitForStart
		PRINTLN("[MANAGE_CAM_AUDIO] - PLAYING LIFT CUTSCENE MOVEMENT. BLOCKING CCTV SOUNDS")
		EXIT
	ENDIF
	
	IF iSoundIDCamBackground = -1
		iSoundIDCamBackground = GET_SOUND_ID()
	ENDIF
	IF iSoundPan = -1
		iSoundPan = GET_SOUND_ID()
	ENDIF
	IF iSoundZoom = -1
		iSoundZoom = GET_SOUND_ID()
	ENDIF
	IF iCamSound = -1
		iCamSound = GET_SOUND_ID()
	ENDIF
	
	IF NOT IS_BIT_SET(iCamSoundBitSet,1)
		IF NOT IS_GAMEPLAY_CAM_RENDERING()
			PLAY_SOUND_FRONTEND(iSoundIDCamBackground,"Background","MP_CCTV_SOUNDSET", FALSE)
			START_AUDIO_SCENE("DLC_HEIST_BANK_CCTV_SCENE") 
			SET_BIT(iCamSoundBitSet,1)
			PRINTLN("[MANAGE_CAM_AUDIO] - PLAYING BACKGROUND SOUND")
		ENDIF
	ENDIF
	
	IF bManualControl
		IF NOT IS_BIT_SET(iCamSoundBitSet,2)
			IF fXMove != 0
				IF HAS_SOUND_FINISHED(iSoundPan)
				OR iSoundPan = -1
					IF NOT IS_GAMEPLAY_CAM_RENDERING()
						IF NOT IS_VECTOR_ZERO(vCCTVRot[iThisCamShot])
							PLAY_SOUND_FRONTEND(iSoundPan,"Pan","MP_CCTV_SOUNDSET", FALSE)
							PRINTLN("[MANAGE_CAM_AUDIO] - PLAYING PAN")
							SET_BIT(iCamSoundBitSet,2)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF fXMove = 0
				STOP_SOUND(iSoundPan)
				PRINTLN("[MANAGE_CAM_AUDIO] - STOPPING PAN")
				CLEAR_BIT(iCamSoundBitSet,2)
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(iCamSoundBitSet,2)
			IF NOT ARE_VECTORS_EQUAL(g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].vCamStartRot[iThisCamShot],g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].vCamEndRot[iThisCamShot])
				IF HAS_SOUND_FINISHED(iSoundPan)
				OR iSoundPan = -1
					IF NOT IS_GAMEPLAY_CAM_RENDERING()
						PLAY_SOUND_FRONTEND(iSoundPan,"Pan","MP_CCTV_SOUNDSET", FALSE)
						PRINTLN("[MANAGE_CAM_AUDIO] - PLAYING PAN")
						SET_BIT(iCamSoundBitSet,2)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF ARE_VECTORS_EQUAL(g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].vCamStartRot[iThisCamShot],g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].vCamEndRot[iThisCamShot])
				STOP_SOUND(iSoundPan)
				PRINTLN("[MANAGE_CAM_AUDIO] - STOPPING PAN")
				CLEAR_BIT(iCamSoundBitSet,2)
			ENDIF
		ENDIF
	ENDIF
	
	
	IF bManualControl
		IF NOT IS_BIT_SET(iCamSoundBitSet,3)
			IF fYMove != 0
				IF HAS_SOUND_FINISHED(iSoundZoom)
				OR iSoundZoom = -1
					IF NOT IS_GAMEPLAY_CAM_RENDERING()
						PLAY_SOUND_FRONTEND(iSoundZoom,"Zoom","MP_CCTV_SOUNDSET", FALSE)
						PRINTLN("[MANAGE_CAM_AUDIO] - PLAYING ZOOM")
						SET_BIT(iCamSoundBitSet,3)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF fYMove = 0
				STOP_SOUND(iSoundZoom)
				CLEAR_BIT(iCamSoundBitSet,3)
				PRINTLN("[MANAGE_CAM_AUDIO] - STOPPING ZOOM")
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(iCamSoundBitSet,3)
			IF g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].fStartCamFOV[iThisCamShot] != g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].fEndCamFOV[iThisCamShot]
				IF HAS_SOUND_FINISHED(iSoundZoom)
				OR iSoundZoom = -1
					IF NOT IS_GAMEPLAY_CAM_RENDERING()
						PLAY_SOUND_FRONTEND(iSoundZoom,"Zoom","MP_CCTV_SOUNDSET", FALSE)
						PRINTLN("[MANAGE_CAM_AUDIO] - PLAYING ZOOM")
						SET_BIT(iCamSoundBitSet,3)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].fStartCamFOV[iThisCamShot] = g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].fEndCamFOV[iThisCamShot]
				STOP_SOUND(iSoundZoom)
				CLEAR_BIT(iCamSoundBitSet,3)
				PRINTLN("[MANAGE_CAM_AUDIO] - STOPPING ZOOM")
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(iCamSoundBitSet,4)
		IF iStoreLastCam != iThisCamShot
			IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iThisCutscene].iCutsceneShotBitSet[iThisCamShot],ciCSS_BS_NewShot)
				IF HAS_SOUND_FINISHED(iCamSound)
				OR iCamSound = -1
					IF NOT IS_GAMEPLAY_CAM_RENDERING()
						PLAY_SOUND_FRONTEND(iCamSound,"Change_Cam","MP_CCTV_SOUNDSET", FALSE)
						SET_BIT(iCamSoundBitSet,4)
						iLocalCamSoundTimer = GET_GAME_TIMER()
						PRINTLN("[MANAGE_CAM_AUDIO] - PLAYING CHANGE CAM")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF MANAGE_THIS_MINIGAME_TIMER(iLocalCamSoundTimer,100)
			STOP_SOUND(iCamSound)
			PRINTLN("[MANAGE_CAM_AUDIO] - STOPPING CHANGE CAM")
			CLEAR_BIT(iCamSoundBitSet,4)
		ENDIF
	ENDIF

	iStoreLastCam = iThisCamShot
ENDPROC

PROC REQUEST_CCTV_ASSETS()
	SI_SecurityCam = REQUEST_SCALEFORM_MOVIE("SECURITY_CAM")
	CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Requesting scaleform movie SECURITY_CAM" )
ENDPROC

FUNC BOOL HAVE_CCTV_ASSETS_LOADED()
	IF HAS_SCALEFORM_MOVIE_LOADED(SI_SecurityCam)
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] scaleform movie SECURITY_CAM has loaded!" )
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC RENDER_CCTV_OVERLAY()
	
	INT iThisHours,iThisMinutes

	iThisHours = GET_CLOCK_HOURS()
	iThisMinutes = GET_CLOCK_MINUTES()
	
	PRINTLN("[RCC MISSION] RENDER_CCTV_OVERLAY -iThisHours: ", iThisHours)
	PRINTLN("[RCC MISSION] RENDER_CCTV_OVERLAY -iThisMinutes: ", iThisMinutes)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(SI_SecurityCam, "SET_TIME")		
		IF iThisHours >= 0 AND iThisHours <= 12
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iThisHours)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iThisHours - 12)
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iThisMinutes)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(00)
		IF iThisHours >= 0 AND iThisHours < 12
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPH_TIMEAM")
			PRINTLN("[RCC MISSION] SETTING MPH_TIMEAM ")
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPH_TIMEPM")
			PRINTLN("[RCC MISSION] SETTING MPH_TIMEPM ")
		ENDIF
		
	END_SCALEFORM_MOVIE_METHOD()
	
	IF DOES_CURRENT_MISSION_USE_GANG_BOSS()
		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	ENDIF

	DRAW_SCALEFORM_MOVIE_FULLSCREEN(SI_SecurityCam, 255, 255, 255, 255)
	
ENDPROC

PROC SET_CCTV_LOCATION(STRING sCamDetails , STRING sCamLocale )
	
	BEGIN_SCALEFORM_MOVIE_METHOD(SI_SecurityCam, "SET_DETAILS")
		IF IS_STRING_EMPTY(sCamDetails)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPH_CAMDETAILS1")
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sCamDetails)
		ENDIF
		PRINTLN("[RCC MISSION] SET_CCTV_LOCATION -SET_DETAILS : ")
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(SI_SecurityCam, "SET_LOCATION")
		IF IS_STRING_EMPTY(sCamLocale)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPH_CAMLOCALE11")
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sCamLocale)
		ENDIF
		PRINTLN("[RCC MISSION] SET_CCTV_LOCATION -SET_LOCATION : ")
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: CAMERA SHOTS !
//
//************************************************************************************************************************************************************



//Pre load a scripted cutscene cam shot
PROC PRE_LOAD_CAM_SHOT_AREA(INT iCShot, VECTOR vloc, FLOAT fRadius)
	IF iCShot > iCamShotLoaded
	
		IF NOT IS_VECTOR_ZERO( vloc )
		AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
			//Only clear the load area if we are going to be loading a new one.
			IF IS_NEW_LOAD_SCENE_ACTIVE()
				PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - PRE_LOAD_CAM_SHOT_AREA - Calling to stop Load Scene.")
				NEW_LOAD_SCENE_STOP()
			ENDIF
			
			PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - PRE_LOAD_CAM_SHOT_AREA - Calling to start a new Load Scene.")
			
			//Load the designated area
			NEW_LOAD_SCENE_START_SPHERE(vloc, fRadius)
			CLEAR_BIT(iLocalBoolCheck7, LBOOL7_CURRENT_SCRIPTED_CAM_AREA_LOADED)
		ELSE
			PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - PRE_LOAD_CAM_SHOT_AREA - Calling to start a new Load Scene.")
			SET_BIT(iLocalBoolCheck7, LBOOL7_CURRENT_SCRIPTED_CAM_AREA_LOADED)
		ENDIF
		
		PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - PRE_LOAD_CAM_SHOT_AREA - iCamShotLoaded is now: ", iCShot)
		
		//Update pre loaded shot		
		iCamShotLoaded = iCShot
	ELSE
		if iCShot = iCamShotLoaded
			IF NOT IS_BIT_SET(iLocalBoolCheck7, LBOOL7_CURRENT_SCRIPTED_CAM_AREA_LOADED)
				PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - PRE_LOAD_CAM_SHOT_AREA - checking if iCShot: ", iCShot, " is loaded")
				
				IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
				OR IS_NEW_LOAD_SCENE_LOADED()
					PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - PRE_LOAD_CAM_SHOT_AREA - Cam Shot ", iCamShotLoaded, " Load Scene is loaded")
					SET_BIT(iLocalBoolCheck7, LBOOL7_CURRENT_SCRIPTED_CAM_AREA_LOADED)
				ENDIF
			ENDIF
		ENDIF
	
	ENDIF

ENDPROC

//PURPOSE: Init and update the cutscene intro focus cam
PROC DO_FOCUS_INTRO_CAM( INT iCutsceneToUse )
	// Init the focus intro
	IF( NOT IS_BIT_SET( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_START_FOCUS_INTRO ) )
		IF( NOT IS_BIT_SET( g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Hint_At_Start )
		OR IS_PLAYER_SCTV( LocalPlayer ) )
			PRINTLN( "[RCC MISSION] DO_FOCUS_INTRO_CAM - NO HINT INTRO SETTING TO SKIP" )
			SET_BIT( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_START_FOCUS_INTRO )
			SET_BIT( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_FOCUS_INTRO )
		ELSE
			// Kill any other hint camera
			KILL_CHASE_HINT_CAM( sIntroHintCam )
			
			// Force the chase hint cam
			FORCE_CHASE_HINT_CAM( sIntroHintCam, TRUE )	
			
			// Set interp times
			SET_CUSTOM_CHASE_HINT_CAM_INTERP_IN_TIME( sIntroHintCam, g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iTransitionInTime )
			INT iTransitionTime = g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iTransitionInTime + g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iTransitionHoldTime
			SET_CUSTOM_CHASE_HINT_CAM_INTERP_OUT_TIME( sIntroHintCam, iTransitionTime * 2 )
			
			SET_BIT( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_START_FOCUS_INTRO )
		ENDIF
	ENDIF

	// Update the focus intro
	IF( IS_BIT_SET( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_START_FOCUS_INTRO )
	AND NOT IS_BIT_SET( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_FOCUS_INTRO ) )
		IF( IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_OUT() )
			DO_SCREEN_FADE_IN( 500 )
		ENDIF
		
		PRINTLN( "[RCC MISSION] DO_FOCUS_INTRO_CAM - RUNNING FOCUS INTRO" )
		
		// Update the hint cam
		CONTROL_COORD_CHASE_HINT_CAM_ANY_MEANS( sIntroHintCam, g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].vIntroCoOrds, 
			"", HINTTYPE_DEFAULT, FALSE, FALSE )
		
		// Check the timer has started
		IF( HAS_NET_TIMER_STARTED( tHintCam ) )
			INT iTransitionTime = g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iTransitionInTime + g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iTransitionHoldTime
			IF( GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( tHintCam ) > iTransitionTime )
				CPRINTLN( DEBUG_SIMON, "kill" )
				IF( IS_GAMEPLAY_HINT_ACTIVE() )
					STOP_GAMEPLAY_HINT( TRUE )
					ANIMPOSTFX_STOP( "FocusIn" )
					STOP_AUDIO_SCENE( "HINT_CAM_SCENE" )
					sIntroHintCam.bPostFXActive = FALSE
					//PLAY_SOUND_FRONTEND( -1, "FocusOut", "HintCamSounds", FALSE)
				ENDIF
				KILL_CHASE_HINT_CAM( sIntroHintCam )
				SET_BIT( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_FOCUS_INTRO )
				RESET_NET_TIMER( tHintCam )
			ENDIF
		ELSE	
			// Set the focus cam timer
			REINIT_NET_TIMER( tHintCam )
		ENDIF
	ENDIF
ENDPROC

FUNC VECTOR GET_ATTACH_CAMERA_VEHICLE_POSITION( INT iCutscene, INT iShot )
	VECTOR 			vVehPos 	= <<0.0, 0.0, 0.0>>
	INT 			iVehIndex 	= g_FMMC_STRUCT.sScriptedCutsceneData[ iCutscene ].iVehicleToAttachTo[ iShot ]
	NETWORK_INDEX	niVeh		= MC_serverBD_1.sFMMC_SBD.niVehicle[ iVehIndex ]
	VEHICLE_INDEX 	veh			= NULL
	
	IF( NETWORK_DOES_NETWORK_ID_EXIST( niVeh ) )
		veh = NET_TO_VEH( niVeh )
		IF( DOES_ENTITY_EXIST( veh ) )
			vVehPos = GET_ENTITY_COORDS( veh, FALSE )
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF( IS_VECTOR_ZERO( vVehPos ) )
		ASSERTLN( "MANAGE_MID_MISSION_CUTSCENE - Failed to get position of camera attach vehicle for streaming!" )
	ENDIF
	#ENDIF
	
	RETURN vVehPos
ENDFUNC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: PHONE HACKING CUTSCENE !
//
//************************************************************************************************************************************************************



FUNC BOOL HAS_ANY_REMOTE_PLAYER_COMPLETED_HACKING_INTRO()
	INT i
	REPEAT GET_MAXIMUM_PLAYERS_ON_MISSION() i
		IF( IS_BIT_SET( MC_playerBD[ i ].iClientBitSet2, PBBOOL2_HACKING_INTRO_COMPLETED ) )
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

PROC DISABLE_CONTROL_FOR_HACKING_INTRO()
	INT i
	FOR i = ENUM_TO_INT( INPUT_SPRINT ) TO ENUM_TO_INT( INPUT_VEH_SHUFFLE )
		CONTROL_ACTION action = INT_TO_ENUM( CONTROL_ACTION, i )
		IF( action <> INPUT_PHONE )
			DISABLE_CONTROL_ACTION( PLAYER_CONTROL, action )
		ENDIF
	ENDFOR
ENDPROC

FUNC BOOL HAS_THIS_PLAYER_COMPLETED_HACKING_MINGAME()
	RETURN IS_BIT_SET( MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_HACKING_MINIGAME_COMPLETED )
ENDFUNC

FUNC BOOL IS_CUTSCENE_USING_HACKING_INTRO( INT iCutsceneToUse )
	RETURN IS_BIT_SET( g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].iCutsceneBitset, ci_CSBS_Hacking_App_intro )
ENDFUNC	
	
FUNC BOOL IS_HACKING_INTRO_COMPLETE()
	RETURN IS_BIT_SET( iLocalBoolCheck7, LBOOL7_DONE_CUTSCENE_HACKING_INTRO )
ENDFUNC

PROC DO_CUTSCENE_PHONE_INTRO( INT iCutsceneToUse )
	// hacking app launch
	IF( IS_BIT_SET( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_FOCUS_INTRO )
	AND NOT IS_BIT_SET( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_PHONE_INTRO )
	AND NOT IS_BIT_SET( iLocalBoolCheck7, LBOOL7_DONE_CUTSCENE_HACKING_INTRO ) )
		// if the content creator flag isn't set to start the hacking intro we're done
		IF( NOT IS_CUTSCENE_USING_HACKING_INTRO( iCutsceneToUse ) )
			CPRINTLN( DEBUG_SIMON, "DO_CUTSCENE_PHONE_INTRO - Not in use" )				
			SET_BIT( iLocalBoolCheck7, LBOOL7_DONE_CUTSCENE_HACKING_INTRO_INIT )
			SET_BIT( iLocalBoolCheck7, LBOOL7_DONE_CUTSCENE_HACKING_INTRO )
		ELSE
			IF( NOT IS_BIT_SET( iLocalBoolCheck7, LBOOL7_DONE_CUTSCENE_HACKING_INTRO_INIT ) )
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				IF( NOT IS_PHONE_ONSCREEN() )
					RESET_NET_TIMER( tdPhoneCutSafetyTimer )
					START_NET_TIMER( tdPhoneCutSafetyTimer )
					IF( HAS_THIS_PLAYER_COMPLETED_HACKING_MINGAME() )
						g_bEnableHackingApp = TRUE
					ENDIF
					SET_BIT( iLocalBoolCheck7, LBOOL7_DONE_CUTSCENE_HACKING_INTRO_INIT )
					CPRINTLN( DEBUG_SIMON, "DO_CUTSCENE_PHONE_INTRO - Init done" )
				ENDIF
			ELSE
				// if the local player completed the hacking minigame
				IF( HAS_THIS_PLAYER_COMPLETED_HACKING_MINGAME() )
				AND NOT IS_PLAYER_SCTV( LocalPlayer )
					CPRINTLN( DEBUG_SIMON, "DO_CUTSCENE_PHONE_INTRO - This player is hacker" )
					CLEAR_BIT( iLocalBoolCheck7, LBOOL7_SHOULD_CUTSCENE_DISABLE_PHONE_THIS_FRAME )
					DISPLAY_HELP_TEXT_THIS_FRAME( "HEIST_HELP_29", FALSE ) // "Bring up your phone and launch the VLSI Unlock app."
					PRINT_NOW( "HEIST_OBJ_1", 10, 1 ) // "Unlock the vault."
				
					// wait for the player to launch the unlock app
					IF( g_HackingAppHasBeenLaunched )
						CPRINTLN( DEBUG_SIMON, "DO_CUTSCENE_PHONE_INTRO - App Launched" )
						DISABLE_CELLPHONE_THIS_FRAME_ONLY()
						IF( NOT IS_PHONE_ONSCREEN() )
							CPRINTLN( DEBUG_SIMON, "DO_CUTSCENE_PHONE_INTRO - Done" )
							CLEAR_THIS_PRINT( "HEIST_OBJ_1" )
							SET_BIT( MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_HACKING_INTRO_COMPLETED )
							SET_BIT( iLocalBoolCheck7, LBOOL7_DONE_CUTSCENE_HACKING_INTRO )
							g_bEnableHackingApp = FALSE
							g_HackingAppHasBeenLaunched = FALSE
						ENDIF
					ENDIF
				ELSE
					
					// non hacker players wait for hacker to unlock door
					CPRINTLN( DEBUG_SIMON, "DO_CUTSCENE_PHONE_INTRO - This player is not hacker" )

					IF( HAS_ANY_REMOTE_PLAYER_COMPLETED_HACKING_INTRO() )
					OR IS_FAKE_MULTIPLAYER_MODE_SET()
						CLEAR_THIS_PRINT( "HEIST_OBJ_2" )
						SET_BIT( iLocalBoolCheck7, LBOOL7_DONE_CUTSCENE_HACKING_INTRO )
					ENDIF
				ENDIF
				
				// timed out so progress
				IF( GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( tdPhoneCutSafetyTimer ) > 40000 )
					DISABLE_CELLPHONE_THIS_FRAME_ONLY()
					IF( NOT IS_PHONE_ONSCREEN() )
						CPRINTLN( DEBUG_SIMON, "DO_CUTSCENE_PHONE_INTRO - Timed out" )
						CLEAR_THIS_PRINT( "HEIST_OBJ_1" )
						CLEAR_THIS_PRINT( "HEIST_OBJ_2" )
						g_bEnableHackingApp = FALSE
						g_HackingAppHasBeenLaunched = FALSE
						SET_BIT( iLocalBoolCheck7, LBOOL7_DONE_CUTSCENE_HACKING_INTRO )
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	// phone intro init
	IF IS_BIT_SET( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_FOCUS_INTRO )
	AND IS_BIT_SET( iLocalBoolCheck7, LBOOL7_DONE_CUTSCENE_HACKING_INTRO )
	AND NOT IS_BIT_SET( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_START_PHONE_INTRO )
		
		IF NOT GET_USINGNIGHTVISION()
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset,ci_CSBS_Phone_Intro)
			OR IS_PLAYER_SCTV(LocalPlayer)
				g_FMMC_STRUCT.bSecurityCamActive = FALSE
				
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_HidePhone_DontHangup)
					HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
				ELSE
					HIDE_ACTIVE_PHONE(TRUE)
				ENDIF
				
				IF IS_SCREEN_FADED_OUT()
				OR IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_IN(500)
				ENDIF

				PRINTLN("[RCC MISSION] DO_CUTSCENE_PHONE_INTRO - NO PHONE INTRO SETTING SO SKIPPING")
				SET_BIT(iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_PHONE_INTRO)
				SET_BIT(iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_START_PHONE_INTRO)
			ELSE
				CLEAR_BIT( iLocalBoolCheck7, LBOOL7_SHOULD_CUTSCENE_DISABLE_PHONE_THIS_FRAME )
			
				g_FMMC_STRUCT.bSecurityCamActive = TRUE
				IF NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_READY_FOR_PHONE_INTRO)
					IF( GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH( "cellphone_flashhand" ) ) = 0
					AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH( "appinternet" ) ) = 0 )
						IF g_Cellphone.PhoneDS >= PDS_RUNNINGAPP
							PRINTLN("[RCC MISSION] DO_CUTSCENE_PHONE_INTRO - APP IS RUNNING, HANGING UP PHONE")
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_HidePhone_DontHangup)
								HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
							ELSE
								HIDE_ACTIVE_PHONE(TRUE)
							ENDIF
							SET_BIT(iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_READY_FOR_PHONE_INTRO)
						ELSE
							PRINTLN("[RCC MISSION] DO_CUTSCENE_PHONE_INTRO - APP ISN'T RUNNING SO READY TO START INTRO")
							LAUNCH_CELLPHONE_APPLICATION(AppCamera, FALSE, TRUE, FALSE)
							SET_BIT(iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_START_PHONE_INTRO)
							REINIT_NET_TIMER(tdPhoneCutSafetyTimer)
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_PHONE_ONSCREEN()
						PRINTLN("[RCC MISSION] DO_CUTSCENE_PHONE_INTRO - PHONE NOW ISN'T ON SCREEN SO READY TO START INTRO")
						LAUNCH_CELLPHONE_APPLICATION(AppCamera, FALSE, TRUE, FALSE)
						SET_BIT(iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_START_PHONE_INTRO)
						REINIT_NET_TIMER(tdPhoneCutSafetyTimer)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	// phone intro update
	IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_START_PHONE_INTRO)
	AND NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_PHONE_INTRO)
	AND IS_BIT_SET(iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_FOCUS_INTRO)
	AND IS_BIT_SET( iLocalBoolCheck7, LBOOL7_DONE_CUTSCENE_HACKING_INTRO )
		CLEAR_BIT( iLocalBoolCheck7, LBOOL7_SHOULD_CUTSCENE_DISABLE_PHONE_THIS_FRAME )
		
		IF IS_SCREEN_FADED_OUT()
		OR IS_SCREEN_FADING_OUT()
			DO_SCREEN_FADE_IN(500)
		ENDIF
		
		IF bLocalPlayerPedOk
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				IF NETWORK_HAS_CONTROL_OF_ENTITY(LocalPlayerPed)	
					IF NOT IS_PED_RUNNING_MOBILE_PHONE_TASK(LocalPlayerPed)
						CLEAR_PED_TASKS(LocalPlayerPed)
						TASK_USE_MOBILE_PHONE(LocalPlayerPed,TRUE,Mode_ToText)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		VECTOR g_Test3dPhonePosVec
		GET_MOBILE_PHONE_POSITION(g_Test3dPhonePosVec)
		
	   	PRINTLN( "[RCC MISSION] DO_CUTSCENE_PHONE_INTRO - phone position: ", g_Test3dPhonePosVec, " g_Phone_Active_but_Hidden: ", g_Phone_Active_but_Hidden )
		
		IF g_Test3dPhonePosVec.x = 1.5 AND g_Test3dPhonePosVec.z = -17.0
			PRINTLN("[RCC MISSION] DO_CUTSCENE_PHONE_INTRO - PHONE HAS FILLED SCREEN, DONE")
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_HidePhone_DontHangup)
				HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
			ELSE
				HIDE_ACTIVE_PHONE(TRUE)
			ENDIF
			SET_BIT(iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_PHONE_INTRO)
			RESET_NET_TIMER(tdPhoneCutSafetyTimer)
		ELSE
			//Safety backup for if the phone refuses to open up:
			BOOL bPhone 	= IS_PHONE_ONSCREEN()
			INT  iTimerDiff = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdPhoneCutSafetyTimer)
			
			PRINTLN("[RCC MISSION] DO_CUTSCENE_PHONE_INTRO - bPhone:", bPhone, ", iTimerDiff:", iTimerDiff)
			
			IF NOT IS_BIT_SET(iLocalBoolCheck7,LBOOL7_CUTSCENE_PHONE_SAFETY_TRIGGERED)
				IF NOT bPhone
					IF iTimerDiff > 2000
						//Try again:
						LAUNCH_CELLPHONE_APPLICATION(AppCamera, FALSE, TRUE, FALSE)
						SET_BIT(iLocalBoolCheck7,LBOOL7_CUTSCENE_PHONE_SAFETY_TRIGGERED)
						REINIT_NET_TIMER(tdPhoneCutSafetyTimer)
						PRINTLN("[RCC MISSION] DO_CUTSCENE_PHONE_INTRO - Phone not appeared on screen, try again...")
					ENDIF
				ELSE
					IF iTimerDiff > 6000
						//Give up:
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_HidePhone_DontHangup)
							HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
						ELSE
							HIDE_ACTIVE_PHONE(TRUE)
						ENDIF
						SET_BIT(iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_PHONE_INTRO)
						RESET_NET_TIMER(tdPhoneCutSafetyTimer)
						PRINTLN("[RCC MISSION] DO_CUTSCENE_PHONE_INTRO - Phone on screen but not opened camera app quickly enough, giving up")
					ENDIF
				ENDIF
			ELSE
				IF (bPhone AND iTimerDiff > 6000)
				OR ((NOT bPhone) AND iTimerDiff > 2000)
					//Give up:
					HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
					SET_BIT(iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_PHONE_INTRO)
					RESET_NET_TIMER(tdPhoneCutSafetyTimer)
					PRINTLN("[RCC MISSION] DO_CUTSCENE_PHONE_INTRO - Phone taken too long, giving up")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC RESET_PER_SHOT_PHONE_INTRO( INT iShot = -1, BOOL bBitFlagsOnly = TRUE )
	CLEAR_BIT( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_START_PHONE_INTRO )
	CLEAR_BIT( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_PHONE_INTRO )
	CLEAR_BIT( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_READY_FOR_PHONE_INTRO )
	CLEAR_BIT( iLocalBoolCheck7, LBOOL7_CUTSCENE_PHONE_SAFETY_TRIGGERED )
	IF( NOT bBitFlagsOnly )
		iPhoneIntroDoneForShot = iShot
	ENDIF
ENDPROC

FUNC BOOL IS_PHOTO_INTRO_DONE_FOR_SHOT( INT iCutsceneToUse, INT iShot )
	RETURN iPhoneIntroDoneForShot = iCamShot 
	AND IS_BIT_SET( g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].iCutsceneShotBitSet[ iShot ], ciCSS_BS_Use_Phone_Intro )
ENDFUNC

PROC DO_PER_SHOT_PHONE_INTRO( INT iCutsceneToUse, INT iShot )
	// make sure current shot doesn't end before phone fill screen finished

	IF( iPhoneIntroDoneForShot <> iShot )
		// if the phone intro hasn't been started
		IF( NOT IS_BIT_SET( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_START_PHONE_INTRO ) )
			// if we don't want a phone intro or the player is spectating then skip it
			IF( NOT IS_BIT_SET( g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].iCutsceneShotBitSet[ iShot ], ciCSS_BS_Use_Phone_Intro )
			OR IS_PLAYER_SCTV( LocalPlayer ) )
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_HidePhone_DontHangup)
					HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
				ELSE
					HIDE_ACTIVE_PHONE(TRUE)
				ENDIF
				IF( IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_OUT() )
					DO_SCREEN_FADE_IN( 500 )
				ENDIF
				
				PRINTLN( "[RCC MISSION] DO_PER_SHOT_PHONE_INTRO - NO PHONE INTRO FOR SHOT: ", iShot )
				CPRINTLN( DEBUG_SIMON, "[RCC MISSION] DO_PER_SHOT_PHONE_INTRO - NO PHONE INTRO FOR SHOT: ", iShot )
				RESET_PER_SHOT_PHONE_INTRO( iShot, FALSE )
			ELSE // we want a phone intro so init
				CLEAR_BIT( iLocalBoolCheck7, LBOOL7_SHOULD_CUTSCENE_DISABLE_PHONE_THIS_FRAME )
				
				// if the init isn't finished
				IF( NOT IS_BIT_SET( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_READY_FOR_PHONE_INTRO ) )
					// put phone away safely
					IF( g_Cellphone.PhoneDS >= PDS_RUNNINGAPP )
						HANG_UP_AND_PUT_AWAY_PHONE( TRUE )
						SET_BIT( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_READY_FOR_PHONE_INTRO )
					ELSE
						LAUNCH_CELLPHONE_APPLICATION( AppCamera, FALSE, TRUE, FALSE )
						SET_BIT( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_START_PHONE_INTRO )
						REINIT_NET_TIMER( tdPhoneCutSafetyTimer )
					ENDIF
				ELSE // init finished
					IF( NOT IS_PHONE_ONSCREEN() )
						LAUNCH_CELLPHONE_APPLICATION( AppCamera, FALSE, TRUE, FALSE )
						SET_BIT( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_START_PHONE_INTRO )
						REINIT_NET_TIMER( tdPhoneCutSafetyTimer )
					ENDIF
				ENDIF
			ENDIF
		ELSE // do the phone intro
			CLEAR_BIT( iLocalBoolCheck7, LBOOL7_SHOULD_CUTSCENE_DISABLE_PHONE_THIS_FRAME )
		
			IF( IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_OUT() )
				DO_SCREEN_FADE_IN( 500 )
			ENDIF
			
			IF bLocalPlayerPedOk
				IF NETWORK_HAS_CONTROL_OF_ENTITY(LocalPlayerPed)	
					IF( NOT IS_PED_RUNNING_MOBILE_PHONE_TASK( LocalPlayerPed ) )
						CLEAR_PED_TASKS( LocalPlayerPed )
						TASK_USE_MOBILE_PHONE( LocalPlayerPed, TRUE, Mode_ToText )
					ENDIF
				ENDIF
			ENDIF
			
			VECTOR g_Test3dPhonePosVec
			GET_MOBILE_PHONE_POSITION( g_Test3dPhonePosVec )
			
	       	PRINTLN( "[RCC MISSION] DO_PER_SHOT_PHONE_INTRO - phone position: ", g_Test3dPhonePosVec, " g_Phone_Active_but_Hidden: ", g_Phone_Active_but_Hidden )
			CPRINTLN( DEBUG_SIMON, "[RCC MISSION] DO_PER_SHOT_PHONE_INTRO - phone position: ", g_Test3dPhonePosVec, " g_Phone_Active_but_Hidden: ", g_Phone_Active_but_Hidden )
			
			IF( g_Test3dPhonePosVec.x = 1.5 AND g_Test3dPhonePosVec.z = -17.0 )
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_HidePhone_DontHangup)
					HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
				ELSE
					HIDE_ACTIVE_PHONE(TRUE)
				ENDIF
				SET_BIT( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_PHONE_INTRO )
				RESET_NET_TIMER( tdPhoneCutSafetyTimer )
			ELSE
				//Safety backup for if the phone refuses to open up:
				BOOL bPhone = IS_PHONE_ONSCREEN()
				INT iTimerDiff = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( tdPhoneCutSafetyTimer )
				
				IF( NOT IS_BIT_SET( iLocalBoolCheck7, LBOOL7_CUTSCENE_PHONE_SAFETY_TRIGGERED ) )
					IF( NOT bPhone )
						IF( iTimerDiff > 2000 )
							//Try again
							LAUNCH_CELLPHONE_APPLICATION( AppCamera, FALSE, TRUE, FALSE )
							SET_BIT( iLocalBoolCheck7, LBOOL7_CUTSCENE_PHONE_SAFETY_TRIGGERED )
							REINIT_NET_TIMER( tdPhoneCutSafetyTimer )
							PRINTLN( "[RCC MISSION] DO_PER_SHOT_PHONE_INTRO - Phone not appeared on screen, try again..." )
							CPRINTLN( DEBUG_SIMON, "[RCC MISSION] DO_PER_SHOT_PHONE_INTRO - Phone not appeared on screen, try again..." )
						ENDIF
					ELSE
						IF( iTimerDiff > 6000 )
							//Give up
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_HidePhone_DontHangup)
								HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
							ELSE
								HIDE_ACTIVE_PHONE(TRUE)
							ENDIF
							SET_BIT( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_PHONE_INTRO )
							RESET_NET_TIMER( tdPhoneCutSafetyTimer )
							PRINTLN( "[RCC MISSION] DO_PER_SHOT_PHONE_INTRO - Phone not appeared on screen, try again..." )
							CPRINTLN( DEBUG_SIMON, "[RCC MISSION] DO_PER_SHOT_PHONE_INTRO - Phone not appeared on screen, try again..." )
						ENDIF
					ENDIF
				ELSE
					IF( ( bPhone AND iTimerDiff > 6000 ) OR ( NOT bPhone AND iTimerDiff > 2000 ) )
						//Give up						
						HANG_UP_AND_PUT_AWAY_PHONE( TRUE )
						SET_BIT( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_PHONE_INTRO )
						RESET_NET_TIMER( tdPhoneCutSafetyTimer )
						PRINTLN( "[RCC MISSION] DO_PER_SHOT_PHONE_INTRO - Phone taken too long, giving up" )
						CPRINTLN( DEBUG_SIMON, "[RCC MISSION] DO_PER_SHOT_PHONE_INTRO - Phone taken too long, giving up" )
					ENDIF
				ENDIF
			ENDIF
			
			IF( IS_BIT_SET( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_PHONE_INTRO ) )
				RESET_PER_SHOT_PHONE_INTRO( iShot, FALSE )
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: SCRIPTED CUTSCENE CLEANUP !
//
//************************************************************************************************************************************************************

#IF IS_DEBUG_BUILD
FUNC STRING GET_SCRIPTED_CUTSCENE_LIFT_DEBUG_NAME(eCutsceneLift eLiftType)
	
	SWITCH eLiftType
		CASE eCSLift_None		RETURN "eCSLift_None"
		CASE eCSLift_ServerFarm	RETURN "eCSLift_ServerFarm"
		CASE eCSLift_IAABase	RETURN "eCSLift_IAABase"
	ENDSWITCH
	
	RETURN "Unknown"
	
ENDFUNC
#ENDIF

PROC GET_SCRIPTED_CUTSCENE_LIFT_MODELS(eCutsceneLift eLiftType, MODEL_NAMES &mnBase, MODEL_NAMES &mnDoorL, MODEL_NAMES &mnDoorR, MODEL_NAMES &mnDoorLO, MODEL_NAMES &mnDoorRO)
	
	SWITCH eLiftType
		CASE eCSLift_ServerFarm
		CASE eCSLift_IAABase
			mnBase = INT_TO_ENUM(MODEL_NAMES, HASH("XM_Prop_IAA_BASE_Elevator"))
			mnDoorL = INT_TO_ENUM(MODEL_NAMES, HASH("XM_Prop_AGT_CIA_Door_El_R"))
			mnDoorR = INT_TO_ENUM(MODEL_NAMES, HASH("XM_Prop_AGT_CIA_Door_El_L"))
			mnDoorLO = INT_TO_ENUM(MODEL_NAMES, HASH("XM_Prop_AGT_CIA_Door_El_02_R"))
			mnDoorRO = INT_TO_ENUM(MODEL_NAMES, HASH("XM_Prop_AGT_CIA_Door_El_02_L"))
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC BOOL LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS(INT iCutsceneToUse)
	
	BOOL bLoaded = TRUE
	BOOL bLoadLiftModels
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset2, ci_CSBS2_UseServerFarmLift)
		
		bLoadLiftModels = TRUE
		eCSLift = eCSLift_ServerFarm
		
	ELIF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset2, ci_CSBS2_UseIAABaseLift)
		
		bLoadLiftModels = TRUE
		eCSLift = eCSLift_IAABase
		
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[RCC MISSION][CSLift] LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - No cutscene lift setup")
	#ENDIF
	ENDIF
	
	IF bLoadLiftModels
		
		MODEL_NAMES mnBase, mnDoorL, mnDoorR, mnDoorLO, mnDoorRO
		GET_SCRIPTED_CUTSCENE_LIFT_MODELS(eCSLift, mnBase, mnDoorL, mnDoorR, mnDoorLO, mnDoorRO)
		
		PRINTLN("[RCC MISSION][CSLift] LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Loading assets for lift ",GET_SCRIPTED_CUTSCENE_LIFT_DEBUG_NAME(eCSLift))
		
		REQUEST_MODEL(mnBase)
		
		IF NOT HAS_MODEL_LOADED(mnBase)
			PRINTLN("[RCC MISSION][CSLift] LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Waiting for Lift Base model")
			bLoaded = FALSE
		ELSE
			PRINTLN("[RCC MISSION][CSLift] LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Lift Base model has loaded")
		ENDIF
		
		REQUEST_MODEL(mnDoorL)
		
		IF NOT HAS_MODEL_LOADED(mnDoorL)
			PRINTLN("[RCC MISSION][CSLift] LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Waiting for Lift Left Door model")
			bLoaded = FALSE
		ELSE
			PRINTLN("[RCC MISSION][CSLift] LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Lift Left Door model has loaded")
		ENDIF
		
		REQUEST_MODEL(mnDoorR)
		
		IF NOT HAS_MODEL_LOADED(mnDoorR)
			PRINTLN("[RCC MISSION][CSLift] LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Waiting for Lift Right Door model")
			bLoaded = FALSE
		ELSE
			PRINTLN("[RCC MISSION][CSLift] LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Lift Right Door model has loaded")
		ENDIF
		
		REQUEST_MODEL(mnDoorLO)
		
		IF NOT HAS_MODEL_LOADED(mnDoorLO)
			PRINTLN("[RCC MISSION][CSLift] LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Waiting for Lift Outer Left Door model")
			bLoaded = FALSE
		ELSE
			PRINTLN("[RCC MISSION][CSLift] LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Lift Outer Left Door model has loaded")
		ENDIF
		
		REQUEST_MODEL(mnDoorRO)
		
		IF NOT HAS_MODEL_LOADED(mnDoorRO)
			PRINTLN("[RCC MISSION][CSLift] LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Waiting for Lift Outer Right Door model")
			bLoaded = FALSE
		ELSE
			PRINTLN("[RCC MISSION][CSLift] LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Lift Outer Right Door model has loaded")
		ENDIF
		
	ENDIF
	
	RETURN bLoaded
	
ENDFUNC

PROC UNLOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS()
	
	IF eCSLift = eCSLift_None
		EXIT
	ENDIF
	
	PRINTLN("[RCC MISSION][CSLift] UNLOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Unloading assets for lift ",GET_SCRIPTED_CUTSCENE_LIFT_DEBUG_NAME(eCSLift))
	
	IF DOES_ENTITY_EXIST(objCutsceneLift_Body)
		PRINTLN("[RCC MISSION][CSLift] UNLOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Deleting lift body")
		DELETE_OBJECT(objCutsceneLift_Body)
	ENDIF
	IF DOES_ENTITY_EXIST(objCutsceneLift_DoorL)
		PRINTLN("[RCC MISSION][CSLift] UNLOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Deleting lift left door")
		DELETE_OBJECT(objCutsceneLift_DoorL)
	ENDIF
	IF DOES_ENTITY_EXIST(objCutsceneLift_DoorR)
		PRINTLN("[RCC MISSION][CSLift] UNLOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Deleting lift right door")
		DELETE_OBJECT(objCutsceneLift_DoorR)
	ENDIF
	IF DOES_ENTITY_EXIST(objCutsceneLift_DoorLO)
		PRINTLN("[RCC MISSION][CSLift] UNLOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Deleting lift outer left door")
		DELETE_OBJECT(objCutsceneLift_DoorLO)
	ENDIF
	IF DOES_ENTITY_EXIST(objCutsceneLift_DoorRO)
		PRINTLN("[RCC MISSION][CSLift] UNLOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Deleting lift outer right door")
		DELETE_OBJECT(objCutsceneLift_DoorRO)
	ENDIF
	
	MODEL_NAMES mnBase, mnDoorL, mnDoorR, mnDoorLO, mnDoorRO
	GET_SCRIPTED_CUTSCENE_LIFT_MODELS(eCSLift, mnBase, mnDoorL, mnDoorR, mnDoorLO, mnDoorRO)
	
	SET_MODEL_AS_NO_LONGER_NEEDED(mnBase)
	SET_MODEL_AS_NO_LONGER_NEEDED(mnDoorL)
	SET_MODEL_AS_NO_LONGER_NEEDED(mnDoorR)
	SET_MODEL_AS_NO_LONGER_NEEDED(mnDoorLO)
	SET_MODEL_AS_NO_LONGER_NEEDED(mnDoorRO)
	
	VECTOR vLift = GET_SCRIPTED_CUTSCENE_LIFT_COORDS(eCSLift)
	
	REMOVE_MODEL_HIDE(vLift, 0.5, mnBase)
	REMOVE_MODEL_HIDE(vLift, 0.5, mnDoorL)
	REMOVE_MODEL_HIDE(vLift, 0.5, mnDoorR)
	REMOVE_MODEL_HIDE(vLift, 0.5, mnDoorLO)
	REMOVE_MODEL_HIDE(vLift, 0.5, mnDoorRO)
	
	PRINTLN("[RCC MISSION][CSLift] UNLOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Removed cutscene lift model hides from vLift ",vLift)
	
	eCSLift = eCSLift_None
	
ENDPROC

PROC CLEANUP_CCTV_ASSETS()
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(SI_SecurityCam)
ENDPROC

FUNC BOOL SHOULD_PARTICIPANT_KEEP_TASK_AFTER_CUTSCENE()
	INT i_Counter
	
	REPEAT GET_MAXIMUM_PLAYERS_ON_MISSION() i_Counter
		IF IS_BIT_SET(MC_playerBD[i_Counter].iClientBitSet2, PBBOOL2_TASK_PLAYER_CUTANIM_INTRO)
			PRINTLN("[CUTSC][JR] - RETURNING TRUE - BIT SET")
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	INT iTeam = MC_PlayerBD[iLocalPart].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam] + 1
	
	IF iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[iRule], ciBS_RULE10_POST_CUTSCENE_COVER)
			PRINTLN("[CUTSC][JR] - Ped Should Keep Task IAA - SERVER FARM - RETURNING TRUE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	PRINTLN("[CUTSC][JR] - RETURNING FALSE")
	RETURN FALSE
ENDFUNC

//Clear up the cutscene
PROC CLEANUP_SCRIPTED_CUTSCENE(BOOL bFinalCleanUp = FALSE, BOOL bInterpBackToGameplayCam = FALSE, BOOL bClearFade = TRUE)
	//only clear it up if it's not all ready been!
	IF IS_BIT_SET(iLocalBoolCheck4, LBOOL4_SCRIPTED_CUT_NEEDS_CLEANED_UP)
		PRINTLN("[RCC MISSION] CLEANUP_SCRIPTED_CUTSCENE called")
		
		CLEANUP_CCTV_ASSETS()
		
		STOP_SOUND(iSoundIDCamBackground)
		STOP_SOUND(iSoundPan)
		STOP_SOUND(iSoundZoom)
		STOP_SOUND(iCamSound)
		CLEAR_BIT(iCamSoundBitSet,0)
		CLEAR_BIT(iCamSoundBitSet,1)
		CLEAR_BIT(iCamSoundBitSet,2)
		CLEAR_BIT(iCamSoundBitSet,3)
		
		IF IS_AUDIO_SCENE_ACTIVE("DLC_HEIST_BANK_CCTV_SCENE")
			STOP_AUDIO_SCENE("DLC_HEIST_BANK_CCTV_SCENE")
		ENDIF
		
		HIDE_SPECTATOR_HUD(FALSE)
		HIDE_SPECTATOR_BUTTONS(FALSE)
		
		CLEAR_TIMECYCLE_MODIFIER()
		ENABLE_INTERACTION_MENU()
		
		CLEANUP_MP_CUTSCENE()

		RENDER_SCRIPT_CAMS(FALSE, bInterpBackToGameplayCam)
		
		IF DOES_CAM_EXIST(cutscenecam)
			DESTROY_CAM(cutscenecam)
		ENDIF
		
		IF NOT SHOULD_PARTICIPANT_KEEP_TASK_AFTER_CUTSCENE()
		AND NOT IS_PED_INJURED(LocalPlayerPed)
			CLEAR_PED_TASKS(LocalPlayerPed)
		ENDIF
		
		// 1955908
		IF NOT IS_PED_INJURED(LocalPlayerPed)
			IF IS_ENTITY_ATTACHED(LocalPlayerPed)
			AND NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				PRINTLN("[RCC MISSION] CLEANUP_SCRIPTED_CUTSCENE, DETACH_ENTITY")
				DETACH_ENTITY(LocalPlayerPed)
			ENDIF
		ENDIF
		
		IF NOT bFinalCleanUp
		AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
			NET_SET_PLAYER_CONTROL(LocalPlayer,TRUE)
		ENDIF
		
		IF NOT IS_PED_INJURED(LocalPlayerPed)
			PED_PARACHUTE_STATE eCurrentParachuteState = GET_PED_PARACHUTE_STATE(LocalPlayerPed)
			IF eCurrentParachuteState != PPS_PARACHUTING 
			AND eCurrentParachuteState != PPS_DEPLOYING
			AND NOT IS_PED_IN_ANY_VEHICLE( LocalPlayerPed )
				WEAPON_TYPE weapcur
				GET_CURRENT_PED_WEAPON(LocalPlayerPed, weapcur)
				
				IF weapcur != WEAPONTYPE_INVALID
					//Force weapon into peds hand
					SET_CURRENT_PED_WEAPON(LocalPlayerPed, weapcur , TRUE)
				ENDIF
				
				SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed,TRUE)
			ENDIF
		ENDIF
		
		IF bClearFade
			IF (IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_OUT())
			AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
				DO_SCREEN_FADE_IN(500)
			ENDIF
		ENDIF
		
		CLEAR_BIT(iLocalBoolCheck7,LBOOL7_CUTSCENE_PHONE_SAFETY_TRIGGERED)
		RESET_NET_TIMER(tdPhoneCutSafetyTimer)
		
		UNLOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS()
		eLiftState = eLMS_WaitForStart
		
//		#IF FEATURE_HEIST_PLANNING
//		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_USING_NIGHTVISION_WHEN_ENTERING_CUTSCENE)
//			TURN_NIGHTVISION_ON()
//			CLEAR_BIT(iLocalBoolCheck6, LBOOL6_USING_NIGHTVISION_WHEN_ENTERING_CUTSCENE)
//		ENDIF
//		#ENDIF
		
		NETWORK_SET_VOICE_ACTIVE(true)
		
		iCamShot = 0
		SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_INIT)
		iScriptedCutsceneTeam = -1
		g_iFMMCScriptedCutscenePlaying= -1
		bWarpPlayerAfterScriptCameaSetup = FALSE
		CLEAR_BIT(iLocalBoolCheck4, LBOOL4_SCRIPTED_CUT_NEEDS_CLEANED_UP)
		g_FMMC_STRUCT.bSecurityCamActive = FALSE
		CLEAR_IM_WATCHING_A_MOCAP()
	ENDIF
ENDPROC


FUNC BOOL IS_THIS_VEH_PART_OF_CUTSCENE_VEHICLE_WARP( INT iCutsceneToUse)
	BOOL bWarpInVeh
	
	INT index
	NETWORK_INDEX niVeh
	VEHICLE_INDEX veh
	
	INT iSpawnPosition
	REPEAT FMMC_MAX_TEAMS iSpawnPosition
		index = g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].iVehicleIndexForPosWarp[ iSpawnPosition ]
	
		IF index < 0
		OR index >= FMMC_MAX_VEHICLES
			RELOOP
		ENDIF
		
		niVeh = MC_serverBD_1.sFMMC_SBD.niVehicle[ index ]
			
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST( niVeh )
			RELOOP
		ENDIF
						
		veh = NET_TO_VEH( niVeh )
		
		IF IS_ENTITY_DEAD( veh )
			RELOOP
		ENDIF
		
		IF IS_PED_IN_THIS_VEHICLE(localPlayerPed, veh)
			bWarpInVeh = TRUE
		ENDIF
	ENDREPEAT
		
	RETURN bWarpInVeh
ENDFUNC

FUNC BOOL SPECIAL_CUTSCENE_VEHICLE_WARP( INT iCutsceneToUse, INT iBackupPlayerSpawnPos )
	BOOL bWarpVehicles
	
	INT index
	NETWORK_INDEX niVeh
	VEHICLE_INDEX veh
	
	INT iSpawnPosition
	REPEAT FMMC_MAX_TEAMS iSpawnPosition
		index = g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].iVehicleIndexForPosWarp[ iSpawnPosition ]
	
		IF index < 0
		OR index >= FMMC_MAX_VEHICLES
			RELOOP
		ENDIF
		
		niVeh = MC_serverBD_1.sFMMC_SBD.niVehicle[ index ]
			
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST( niVeh )
			RELOOP
		ENDIF
		
		// we know at this point that we at least want to warp a vehicle
		// so don't do the normal cutscene start pos stuff
		bWarpVehicles = TRUE
		
		IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID( niVeh )
			RELOOP
		ENDIF
		
		veh = NET_TO_VEH( niVeh )
		
		IF IS_ENTITY_DEAD( veh )
			RELOOP
		ENDIF
		
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Vehicle ",index, " has a special vehicle warp - it's going to player spawn pos ", iSpawnPosition, " which is at ", g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vPlayerStartPos[ iSpawnPosition ] )
		
		// always move the vehicle to the start position
		SET_ENTITY_COORDS( veh, g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vPlayerStartPos[ iSpawnPosition ] )
		SET_ENTITY_HEADING( veh, g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fPlayerStartHeading[ iSpawnPosition ] )		
	ENDREPEAT
	
	IF bWarpVehicles // only do a backup warp if we have the data set up
		IF NOT IS_PED_IN_ANY_VEHICLE( LocalPlayerPed )
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] The local player isn't in a vehicle, so we're going to put them in a backup position here: ", g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vPlayerEndPos[ iBackupPlayerSpawnPos ] )
		
			SET_ENTITY_COORDS( LocalPlayerPed, g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vPlayerEndPos[ iBackupPlayerSpawnPos ] )
			SET_ENTITY_HEADING( LocalPlayerPed, g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fPlayerEndHeading[ iBackupPlayerSpawnPos ] )
		ENDIF
	ENDIF
	
	RETURN bWarpVehicles
ENDFUNC

FUNC BOOL DOES_THIS_CUTSCENE_USE_A_CCTV_FILTER( INT iCutsceneToUse )
	INT iShotLoop
	REPEAT MAX_CAMERA_SHOTS iShotLoop
		IF IS_BIT_SET( g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].iCutsceneShotBitSet[ iShotLoop ], ciCSS_BS_SecurityCamFilter )
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_THIS_MOCAP_WARP_INTO_AN_INTERIOR(INT iCutsceneToUse)
	RETURN IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Warp_Into_Interior)
ENDFUNC

PROC MOVE_SCRIPTED_CUTSCENE_LIFT(FLOAT fChange, VECTOR vLift)
	
	SET_ENTITY_COORDS(objCutsceneLift_Body, <<vLift.x, vLift.y, vLift.z + fChange>>)
	
	VECTOR vDoorL = GET_ENTITY_COORDS(objCutsceneLift_DoorL)
	SET_ENTITY_COORDS(objCutsceneLift_DoorL, <<vDoorL.x, vDoorL.y, vDoorL.z + fChange>>)
	
	VECTOR vDoorR = GET_ENTITY_COORDS(objCutsceneLift_DoorR)
	SET_ENTITY_COORDS(objCutsceneLift_DoorR, <<vDoorR.x, vDoorR.y, vDoorR.z + fChange>>)
	
ENDPROC

PROC INIT_SCRIPTED_CUTSCENE_LIFT(INT iCutsceneToUse)
	
	IF eCSLift = eCSLift_None
		EXIT
	ENDIF
	
	VECTOR vLift = GET_SCRIPTED_CUTSCENE_LIFT_COORDS(eCSLift)
	fCutsceneLift_OriginZ = vLift.z
	
	MODEL_NAMES mnBase, mnDoorL, mnDoorR, mnDoorLO, mnDoorRO
	GET_SCRIPTED_CUTSCENE_LIFT_MODELS(eCSLift, mnBase, mnDoorL, mnDoorR, mnDoorLO, mnDoorRO)
	
	PRINTLN("[RCC MISSION][CSLift] INIT_SCRIPTED_CUTSCENE_LIFT - Creating lift ",GET_SCRIPTED_CUTSCENE_LIFT_DEBUG_NAME(eCSLift)," model hides at vLift ",vLift)
	CREATE_MODEL_HIDE_EXCLUDING_SCRIPT_OBJECTS(vLift, 0.5, mnBase, TRUE)
	CREATE_MODEL_HIDE_EXCLUDING_SCRIPT_OBJECTS(vLift, 0.5, mnDoorL, TRUE)
	CREATE_MODEL_HIDE_EXCLUDING_SCRIPT_OBJECTS(vLift, 0.5, mnDoorR, TRUE)
	CREATE_MODEL_HIDE_EXCLUDING_SCRIPT_OBJECTS(vLift, 0.5, mnDoorLO, TRUE)
	CREATE_MODEL_HIDE_EXCLUDING_SCRIPT_OBJECTS(vLift, 0.5, mnDoorRO, TRUE)
	
	FLOAT fLiftBodyHeading
	GET_SCRIPTED_CUTSCENE_LIFT_HEADINGS(eCSLift, fLiftBodyHeading, fCutsceneLift_DoorLOpenHeading, fCutsceneLift_DoorROpenHeading, fCutsceneLift_DoorsClosedHeading)
	
	PRINTLN("[RCC MISSION][CSLift] INIT_SCRIPTED_CUTSCENE_LIFT - Creating lift ",GET_SCRIPTED_CUTSCENE_LIFT_DEBUG_NAME(eCSLift)," objects at ",vLift)
	objCutsceneLift_Body = CREATE_OBJECT(mnBase, vLift, FALSE, FALSE)
	SET_ENTITY_HEADING(objCutsceneLift_Body, fLiftBodyHeading)
	FREEZE_ENTITY_POSITION(objCutsceneLift_Body, TRUE)
	SET_ENTITY_NO_COLLISION_ENTITY(LocalPlayerPed, objCutsceneLift_Body, FALSE)
	
	objCutsceneLift_DoorL = CREATE_OBJECT(mnDoorL, vLift, FALSE, FALSE)
	SET_ENTITY_HEADING(objCutsceneLift_DoorL, fCutsceneLift_DoorLOpenHeading)
	FREEZE_ENTITY_POSITION(objCutsceneLift_DoorL, TRUE)
	
	objCutsceneLift_DoorR = CREATE_OBJECT(mnDoorR, vLift, FALSE, FALSE)
	SET_ENTITY_HEADING(objCutsceneLift_DoorR, fCutsceneLift_DoorROpenHeading)
	FREEZE_ENTITY_POSITION(objCutsceneLift_DoorR, TRUE)
	
	objCutsceneLift_DoorLO = CREATE_OBJECT(mnDoorLO, vLift, FALSE, FALSE)
	SET_ENTITY_HEADING(objCutsceneLift_DoorLO, fCutsceneLift_DoorLOpenHeading)
	FREEZE_ENTITY_POSITION(objCutsceneLift_DoorLO, TRUE)
	
	objCutsceneLift_DoorRO = CREATE_OBJECT(mnDoorRO, vLift, FALSE, FALSE)
	SET_ENTITY_HEADING(objCutsceneLift_DoorRO, fCutsceneLift_DoorROpenHeading)
	FREEZE_ENTITY_POSITION(objCutsceneLift_DoorRO, TRUE)
	
	IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fLiftZOffset_Start != 0
		// The lift will start off the ground, so close the doors and raise it up
		PRINTLN("[RCC MISSION][CSLift] INIT_SCRIPTED_CUTSCENE_LIFT - The lift starts ",g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fLiftZOffset_Start," off the floor, close doors & move up")
		SET_ENTITY_HEADING(objCutsceneLift_DoorL, fCutsceneLift_DoorsClosedHeading)
		SET_ENTITY_HEADING(objCutsceneLift_DoorLO, fCutsceneLift_DoorsClosedHeading)
		SET_ENTITY_HEADING(objCutsceneLift_DoorR, fCutsceneLift_DoorsClosedHeading)
		SET_ENTITY_HEADING(objCutsceneLift_DoorRO, fCutsceneLift_DoorsClosedHeading)
		
		MOVE_SCRIPTED_CUTSCENE_LIFT(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fLiftZOffset_Start, vLift)
	ELSE
		#IF IS_DEBUG_BUILD
		VECTOR vOldLift = GET_ENTITY_COORDS(objCutsceneLift_Body)
		PRINTLN("[RCC MISSION][CSLift] INIT_SCRIPTED_CUTSCENE_LIFT - Moving lift back to vLift ",vLift,", coords before this warp = ",vOldLift)
		#ENDIF
		SET_ENTITY_COORDS(objCutsceneLift_Body, vLift)
	ENDIF
	
ENDPROC

#IF IS_DEBUG_BUILD
FUNC STRING GET_SCRIPTED_CUTSCENE_LIFT_STATE_STRING(eLiftMovementState eState)
	SWITCH eState
		CASE eLMS_WaitForStart	RETURN "eLMS_WaitForStart"
		CASE eLMS_AttachClones	RETURN "eLMS_AttachClones"
		CASE eLMS_CloseDoors	RETURN "eLMS_CloseDoors"
		CASE eLMS_MoveLiftUp	RETURN "eLMS_MoveLiftUp"
		CASE eLMS_MoveLiftDown	RETURN "eLMS_MoveLiftDown"
		CASE eLMS_OpenDoors		RETURN "eLMS_OpenDoors"
		CASE eLMS_Complete		RETURN "eLMS_Complete"
	ENDSWITCH
	RETURN "Unknown"
ENDFUNC
#ENDIF

PROC SET_SCRIPTED_CUTSCENE_LIFT_STATE(eLiftMovementState eNewState)
	IF eNewState != eLiftState
		PRINTLN("[RCC MISSION][CSLift] SET_SCRIPTED_CUTSCENE_LIFT_STATE - Moving from lift state ",GET_SCRIPTED_CUTSCENE_LIFT_STATE_STRING(eLiftState)," to ",GET_SCRIPTED_CUTSCENE_LIFT_STATE_STRING(eNewState))
		eLiftState = eNewState
	ENDIF
ENDPROC

PROC MOVE_AND_ATTACH_CUTSCENE_CLONES_TO_SCRIPTED_CUTSCENE_LIFT(INT iCutsceneToUse)
	
	VECTOR vLiftRot = GET_ENTITY_ROTATION(objCutsceneLift_Body)
	VECTOR vOffset, vRotOffset, vPedCoords, vPedRot, vEndPos
	FLOAT fEndHeading
	INT i
	
	#IF IS_DEBUG_BUILD
	VECTOR vLift = GET_ENTITY_COORDS(objCutsceneLift_Body)
	PRINTLN("[RCC MISSION][CSLift] MOVE_AND_ATTACH_CUTSCENE_CLONES_TO_SCRIPTED_CUTSCENE_LIFT - Called w lift at ",vLift,", vLiftRot ",vLiftRot)
	#ENDIF
	
	INTERIOR_DATA_STRUCT structInteriorData = GET_INTERIOR_DATA(GET_SCRIPTED_CUTSCENE_LIFT_INTERIOR(eCSLift))
	INTERIOR_INSTANCE_INDEX iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
	
	FOR i = 0 to (FMMC_MAX_CUTSCENE_PLAYERS - 1)
		IF DOES_ENTITY_EXIST(MocapPlayerPed[i])
		AND NOT IS_PED_INJURED(MocapPlayerPed[i])

			CLEAR_PED_TASKS_IMMEDIATELY(MocapPlayerPed[i])
			
			IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fLiftZOffset_Start != 0 // If the lift is coming in to land:
				vEndPos = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vPlayerEndPos[i]
				fEndHeading = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fPlayerEndHeading[i]
				PRINTLN("[RCC MISSION][CSLift] MOVE_AND_ATTACH_CUTSCENE_CLONES_TO_SCRIPTED_CUTSCENE_LIFT - Ped ",i," has vEndPos ",vEndPos,", fEndHeading ",fEndHeading)
				
				vPedCoords = vEndPos + <<0, 0, g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fLiftZOffset_Start + 1>> // +1 in z as the end pos is on the ground
				PRINTLN("[RCC MISSION][CSLift] MOVE_AND_ATTACH_CUTSCENE_CLONES_TO_SCRIPTED_CUTSCENE_LIFT - Ped ",i," adjust end pos z by fLiftZOffset_Start ",g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fLiftZOffset_Start," to get vPedCoords ",vPedCoords," & call SET_ENTITY_COORDS + SET_ENTITY_HEADING")
				
				SET_ENTITY_COORDS(MocapPlayerPed[i], vPedCoords)
				SET_ENTITY_HEADING(MocapPlayerPed[i], fEndHeading)
				
				RETAIN_ENTITY_IN_INTERIOR(MocapPlayerPed[i], iInteriorIndex)
			ELSE
				vPedCoords = GET_ENTITY_COORDS(MocapPlayerPed[i])
				PRINTLN("[RCC MISSION][CSLift] MOVE_AND_ATTACH_CUTSCENE_CLONES_TO_SCRIPTED_CUTSCENE_LIFT - Ped ",i," not warping, grab vPedCoords as ",vPedCoords)
			ENDIF
			
			vOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(objCutsceneLift_Body, vPedCoords)
			PRINTLN("[RCC MISSION][CSLift] MOVE_AND_ATTACH_CUTSCENE_CLONES_TO_SCRIPTED_CUTSCENE_LIFT - Ped ",i," offset from lift = ",vOffset)
			
			vPedRot = GET_ENTITY_ROTATION(MocapPlayerPed[i])
			vRotOffset = vPedRot - vLiftRot
			PRINTLN("[RCC MISSION][CSLift] MOVE_AND_ATTACH_CUTSCENE_CLONES_TO_SCRIPTED_CUTSCENE_LIFT - Ped ",i," rotation ",vPedRot," -> vRotOffset ",vRotOffset)
			
			PRINTLN("[RCC MISSION][CSLift] MOVE_AND_ATTACH_CUTSCENE_CLONES_TO_SCRIPTED_CUTSCENE_LIFT - Ped ",i," call ATTACH_ENTITY_TO_ENTITY")
			
			ATTACH_ENTITY_TO_ENTITY(MocapPlayerPed[i], objCutsceneLift_Body, -1, vOffset, vRotOffset)
			
			FORCE_PED_AI_AND_ANIMATION_UPDATE(MocapPlayerPed[i])
		ELSE
			IF i = 0
				PRINTLN("[RCC MISSION][CSLift] MOVE_AND_ATTACH_CUTSCENE_CLONES_TO_SCRIPTED_CUTSCENE_LIFT - Ped 0 no good? DOES_ENTITY_EXIST = ",DOES_ENTITY_EXIST(MocapPlayerPed[i]))
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

PROC PROCESS_SCRIPTED_CUTSENE_LIFT_MOVING(INT iCutsceneToUse, INT iCurrentShot, INT iPlayerCutId)
	
	IF NOT DOES_ENTITY_EXIST(objCutsceneLift_Body)
		EXIT
	ENDIF
	
	VECTOR vLift = GET_ENTITY_COORDS(objCutsceneLift_Body)
	FLOAT fNewZ, fChangeZ
	
	FLOAT fStartZOffset = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fLiftZOffset_Start
	FLOAT fEndZOffset = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fLiftZOffset_End
	FLOAT fStartZ = fCutsceneLift_OriginZ + fStartZOffset
	FLOAT fEndZ = fCutsceneLift_OriginZ + fEndZOffset
	
	INT iMovementOnShot = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iLiftMovementOnShot
	FLOAT fSpeed = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fLiftSpeed
	
	FLOAT fDoorSpeed = 20
	FLOAT fDoorLHeading = GET_ENTITY_HEADING(objCutsceneLift_DoorL)
	FLOAT fDoorRHeading = GET_ENTITY_HEADING(objCutsceneLift_DoorR)
	FLOAT fDoorChange

	IF iSoundIDCamBackground = -1
		iSoundIDCamBackground = GET_SOUND_ID()
	ENDIF
	IF iSoundPan = -1
		iSoundPan = GET_SOUND_ID()
	ENDIF
	IF iSoundZoom = -1
		iSoundZoom = GET_SOUND_ID()
	ENDIF
	IF iCamSound = -1
		iCamSound = GET_SOUND_ID()
	ENDIF
	
	SWITCH eLiftState
		CASE eLMS_WaitForStart
			IF iMovementOnShot >= 0
				IF iCurrentShot >= iMovementOnShot
					SET_SCRIPTED_CUTSCENE_LIFT_STATE(eLMS_AttachClones)
				ELSE
					PRINTLN("[RCC MISSION][CSLift] PROCESS_SCRIPTED_CUTSENE_LIFT_MOVING - Waiting for shot ",iMovementOnShot," before moving, iCurrentShot = ",iCurrentShot)
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION][CSLift] PROCESS_SCRIPTED_CUTSENE_LIFT_MOVING - No movement shot set, finish")
				SET_SCRIPTED_CUTSCENE_LIFT_STATE(eLMS_Complete)
			ENDIF
		BREAK
		
		CASE eLMS_AttachClones
			IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset2, ci_CSBS2_Scripted_UsePlayerClones)
				MOVE_AND_ATTACH_CUTSCENE_CLONES_TO_SCRIPTED_CUTSCENE_LIFT(iCutsceneToUse)
				
				IF fStartZOffset != 0
					// Move the player ped now to remove the long load time at the end of this cutscene
					PRINTLN("[RCC MISSION][CSLift] PROCESS_SCRIPTED_CUTSENE_LIFT_MOVING - Warping the player to ",g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vPlayerEndPos[iPlayerCutId]," & freeze them there, to get ready for cutscene end")
					
					NET_WARP_TO_COORD(
						g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vPlayerEndPos[iPlayerCutId],
						g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fPlayerEndHeading[iPlayerCutId],
						FALSE, FALSE, DEFAULT, DEFAULT, DEFAULT, TRUE)
					
					FREEZE_ENTITY_POSITION(LocalPlayerPed, TRUE)
				ENDIF
			ENDIF
			
			IF iSoundIDCamBackground > -1
				PRINTLN("[RCC MISSION][CSLift] PROCESS_SCRIPTED_CUTSENE_LIFT_MOVING - iSoundIDCamBackground > -1 and we are moving the Lift. Stopping CCTV sound.")
				STOP_SOUND(iSoundIDCamBackground)
				//SCRIPT_RELEASE_SOUND_ID(iSoundIDCamBackground)
			ENDIF					
			IF iSoundPan > -1
				PRINTLN("[RCC MISSION][CSLift] PROCESS_SCRIPTED_CUTSENE_LIFT_MOVING - iSoundPan > -1 and we are moving the Lift. Stopping CCTV sound.")
				STOP_SOUND(iSoundPan)
				//SCRIPT_RELEASE_SOUND_ID(iSoundPan)
			ENDIF
			IF iSoundZoom > -1
				PRINTLN("[RCC MISSION][CSLift] PROCESS_SCRIPTED_CUTSENE_LIFT_MOVING - iSoundZoom > -1 and we are moving the Lift. Stopping CCTV sound.")
				STOP_SOUND(iSoundZoom)
				//SCRIPT_RELEASE_SOUND_ID(iSoundZoom)
			ENDIF
			
			IF fStartZOffset = 0
				SET_SCRIPTED_CUTSCENE_LIFT_STATE(eLMS_CloseDoors)
			ELSE
				IF fEndZOffset < fStartZOffset
					SET_SCRIPTED_CUTSCENE_LIFT_STATE(eLMS_MoveLiftDown)										
				ELSE
					SET_SCRIPTED_CUTSCENE_LIFT_STATE(eLMS_MoveLiftUp)
				ENDIF
			ENDIF
		BREAK
		
		CASE eLMS_CloseDoors
			IF fDoorLHeading <= fCutsceneLift_DoorsClosedHeading
			AND fDoorRHeading >= fCutsceneLift_DoorsClosedHeading
				IF fEndZOffset < fStartZOffset
					SET_SCRIPTED_CUTSCENE_LIFT_STATE(eLMS_MoveLiftDown)
				ELSE
					SET_SCRIPTED_CUTSCENE_LIFT_STATE(eLMS_MoveLiftUp)
				ENDIF
			ELSE
				fDoorChange = (fDoorSpeed * fLastFrameTime)
				
				fDoorLHeading -= fDoorChange
				fDoorLHeading = FMAX(fDoorLHeading, fCutsceneLift_DoorsClosedHeading)
				
				fDoorRHeading += fDoorChange
				fDoorRHeading = FMIN(fDoorRHeading, fCutsceneLift_DoorsClosedHeading)
				
				PRINTLN("[RCC MISSION][CSLift] PROCESS_SCRIPTED_CUTSENE_LIFT_MOVING - Set new fDoorLHeading ",fDoorLHeading,", fDoorRHeading ",fDoorRHeading)
				SET_ENTITY_HEADING(objCutsceneLift_DoorL, fDoorLHeading)
				SET_ENTITY_HEADING(objCutsceneLift_DoorLO, fDoorLHeading)
				SET_ENTITY_HEADING(objCutsceneLift_DoorR, fDoorRHeading)
				SET_ENTITY_HEADING(objCutsceneLift_DoorRO, fDoorRHeading)
			ENDIF
		BREAK
		
		CASE eLMS_MoveLiftUp
			IF vLift.z >= fEndZ
				PRINTLN("[RCC MISSION][CSLift] PROCESS_SCRIPTED_CUTSENE_LIFT_MOVING - Lift z ",vLift.z," reached target end z ",fEndZ)
				
				IF fEndZOffset = 0
					SET_SCRIPTED_CUTSCENE_LIFT_STATE(eLMS_OpenDoors)
				ELSE
					SET_SCRIPTED_CUTSCENE_LIFT_STATE(eLMS_Complete)
				ENDIF
			ELSE
				fChangeZ = (fSpeed * fLastFrameTime)
				fNewZ = CLAMP(vLift.z + fChangeZ, fStartZ, fEndZ) // Make sure we don't go over the top
				fChangeZ = fNewZ - vLift.z
				
				PRINTLN("[RCC MISSION][CSLift] PROCESS_SCRIPTED_CUTSENE_LIFT_MOVING - Moving lift up by ",fChangeZ," to new z ",fNewZ)
				MOVE_SCRIPTED_CUTSCENE_LIFT(fChangeZ, vLift)
			ENDIF
		BREAK
		
		CASE eLMS_MoveLiftDown
			IF iElevatorDescendSound = -1
				PRINTLN("[RCC MISSION][CSLift] PROCESS_SCRIPTED_CUTSENE_LIFT_MOVING - iElevatorDescendSound = -1 The lift has started moving (Start Sound). elevator_descend_loop")
				iElevatorDescendSound = GET_SOUND_ID()
				PLAY_SOUND_FRONTEND(iElevatorDescendSound, "elevator_descend_loop", "dlc_xm_IAA_Finale_sounds")
			ENDIF
			
			IF vLift.z <= fEndZ
				PRINTLN("[RCC MISSION][CSLift] PROCESS_SCRIPTED_CUTSENE_LIFT_MOVING - Lift z ",vLift.z," reached target end z ",fEndZ)
				
				IF fEndZOffset = 0
					SET_SCRIPTED_CUTSCENE_LIFT_STATE(eLMS_OpenDoors)
				ELSE
					SET_SCRIPTED_CUTSCENE_LIFT_STATE(eLMS_Complete)
				ENDIF
			ELSE
				fChangeZ = -(fSpeed * fLastFrameTime)
				fNewZ = CLAMP(vLift.z + fChangeZ, fEndZ, fStartZ) // Make sure we don't go out the bottom
				fChangeZ = fNewZ - vLift.z
				
				PRINTLN("[RCC MISSION][CSLift] PROCESS_SCRIPTED_CUTSENE_LIFT_MOVING - Moving lift down by ",fChangeZ," to new z ",fNewZ)
				MOVE_SCRIPTED_CUTSCENE_LIFT(fChangeZ, vLift)
			ENDIF
		BREAK
		
		CASE eLMS_OpenDoors
			IF iElevatorDescendSound > -1
				PRINTLN("[RCC MISSION][CSLift] PROCESS_SCRIPTED_CUTSENE_LIFT_MOVING - iElevatorDescendSound > -1 The lift has stopped moving (Stop Sound). elevator_descend_loop")
				STOP_SOUND(iElevatorDescendSound)
				SCRIPT_RELEASE_SOUND_ID(iElevatorDescendSound)
			ENDIF
			
			IF fDoorLHeading >= fCutsceneLift_DoorLOpenHeading
			AND fDoorRHeading <= fCutsceneLift_DoorROpenHeading
				SET_SCRIPTED_CUTSCENE_LIFT_STATE(eLMS_Complete)
			ELSE
				fDoorChange = (fDoorSpeed * fLastFrameTime)
				
				fDoorLHeading += fDoorChange
				fDoorLHeading = FMIN(fDoorLHeading, fCutsceneLift_DoorLOpenHeading)
				
				fDoorRHeading -= fDoorChange
				fDoorRHeading = FMAX(fDoorRHeading, fCutsceneLift_DoorROpenHeading)
				
				PRINTLN("[RCC MISSION][CSLift] PROCESS_SCRIPTED_CUTSENE_LIFT_MOVING - Set new fDoorLHeading ",fDoorLHeading,", fDoorRHeading ",fDoorRHeading)
				SET_ENTITY_HEADING(objCutsceneLift_DoorL, fDoorLHeading)
				SET_ENTITY_HEADING(objCutsceneLift_DoorLO, fDoorLHeading)
				SET_ENTITY_HEADING(objCutsceneLift_DoorR, fDoorRHeading)
				SET_ENTITY_HEADING(objCutsceneLift_DoorRO, fDoorRHeading)
			ENDIF
		BREAK
		
		CASE eLMS_Complete
			IF iElevatorDescendSound > -1
				PRINTLN("[RCC MISSION][CSLift] PROCESS_SCRIPTED_CUTSENE_LIFT_MOVING - iElevatorDescendSound > -1 The lift has stopped moving (Stop Sound). elevator_descend_loop")
				STOP_SOUND(iElevatorDescendSound)
				SCRIPT_RELEASE_SOUND_ID(iElevatorDescendSound)
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC INT GET_INTERIOR_WARP_LOAD_SCENE_TIME(VECTOR vCutsceneWarpPos)
	PRINTLN("[RCC MISSION] GET_INTERIOR_WARP_LOAD_SCENE_TIME vCutsceneWarpPos:", vCutsceneWarpPos)
	IF GET_INTERIOR_AT_COORDS(vCutsceneWarpPos) = GET_INTERIOR_AT_COORDS(vServerFarmInteriorCoords)
	OR GET_INTERIOR_AT_COORDS(vCutsceneWarpPos) = GET_INTERIOR_AT_COORDS(vHangarInteriorCoords)
		RETURN 5000
	ENDIF
	IF GET_INTERIOR_AT_COORDS(vCutsceneWarpPos) = GET_INTERIOR_AT_COORDS(vSiloTunnelEntranceInteriorCoords)
		RETURN 3000
	ENDIF
	RETURN 1500
ENDFUNC

FUNC BOOL REQUEST_RELEASE_SCRIPTED_CUTSCENE_CANNED_SCENE(INT iAnimation, BOOL bRequest)

	IF iAnimation = ci_CS_ANIM_NONE
	OR iAnimation = ci_CS_ANIM_MAX
		ASSERTLN("REQUEST_SCRIPTED_CUTSCENE_CANNED_SCENE - iAnimation: ", iAnimation, " - not a valid canned animation")
		RETURN FALSE
	ENDIF

	// REQUEST (Load)
	IF bRequest
	
		SWITCH iAnimation
			
			CASE ci_CS_ANIM_APARTMENT_DOOR_ENTRY
				
				REQUEST_ANIM_DICT("anim@BUILDING_TRANS@HINGE_L")
				IF HAS_ANIM_DICT_LOADED("anim@BUILDING_TRANS@HINGE_L")
					RETURN TRUE
				ENDIF
			
			BREAK
			
			// No specific assets to load, return true
			DEFAULT
				RETURN TRUE
			BREAK
	
		ENDSWITCH

	// RELEASE (Unload)
	ELSE
	
		SWITCH iAnimation
			
			CASE ci_CS_ANIM_APARTMENT_DOOR_ENTRY
				REMOVE_ANIM_DICT("anim@BUILDING_TRANS@HINGE_L")
			BREAK
			
		ENDSWITCH
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Canned scene for apartment door entry
/// PARAMS:
///    iCutsceneID - 
PROC PROCESS_SCRIPTED_CUTSCENE_CANNED_SCENE_APARTMENT_DOOR_ENTRY(INT iCutsceneID)

	// Not running
	IF iScriptedCutsceneCannedSyncedScene = -1
	AND NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_SCRIPTED_CUTSCENE_SYNCSCENE_STARTED)

		MODEL_NAMES doorModel = INT_TO_ENUM(MODEL_NAMES, ci_CS_MODEL_APARTMENT_DOOR)
		OBJECT_INDEX doorProp = NULL
		
		INT iCutsceneEntityID
		REPEAT FMMC_MAX_CUTSCENE_ENTITIES iCutsceneEntityID
			FMMC_CUTSCENE_ENTITY sEntityData = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneID].sCutsceneEntities[iCutsceneEntityID]
			
			// no valid prop info
			IF sEntityData.iIndex < 0
			OR sEntityData.iIndex >= GET_FMMC_MAX_NUM_PROPS()
				RELOOP
			ENDIF
			
			// not a prop
			IF sEntityData.iType != CREATION_TYPE_PROPS
				RELOOP
			ENDIF
			
			// prop doesnt exit
			OBJECT_INDEX prop = oiProps[sEntityData.iIndex]
			IF NOT DOES_ENTITY_EXIST(prop)
				RELOOP
			ENDIF
			
			// not the correct model
			MODEL_NAMES model = GET_ENTITY_MODEL(prop)
			IF model != doorModel
				RELOOP
			ENDIF
			
			// Door prop found, break loop
			doorProp = prop
			BREAKLOOP
			
		ENDREPEAT
		
		IF NOT DOES_ENTITY_EXIST(doorProp)
			ASSERTLN("PROCESS_SCRIPTED_CUTSCENE_CANNED_SCENE_APARTMENT_DOOR_ENTRY - unable to find door prop, make sure you have added a suitable prop as a cutscene entity")
			EXIT
		ENDIF
		
		VECTOR sceneCoord = GET_ENTITY_COORDS(doorProp)
		// TEMP HACK - origin of the door is at the top for some reason
//		VECTOR vMin, vMax
//		GET_MODEL_DIMENSIONS(doorModel, vMin, vMax)
//		sceneCoord.Z -= vMax.Z
		// END TEMP HACK
		VECTOR sceneRotation = GET_ENTITY_ROTATION(doorProp)
		sceneRotation.z += 90
	
		iScriptedCutsceneCannedSyncedScene = CREATE_SYNCHRONIZED_SCENE(sceneCoord, sceneRotation)
		SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iScriptedCutsceneCannedSyncedScene, FALSE)
		
		STRING strAnimDict = "anim@BUILDING_TRANS@HINGE_L"
		
		// Player anims
		INT iPlayerPed
		REPEAT FMMC_MAX_CUTSCENE_PLAYERS iPlayerPed
		
			PED_INDEX ped = MocapPlayerPed[iPlayerPed]
			IF NOT DOES_ENTITY_EXIST(ped)
				RELOOP
			ENDIF
			
			IF IS_PED_INJURED(ped)
				RELOOP
			ENDIF
			
			STRING strAnim
			SWITCH iPlayerPed
				CASE 0 strAnim = "ext_player" BREAK
				CASE 1 strAnim = "ext_a" BREAK
				CASE 2 strAnim = "ext_b" BREAK
				CASE 3 strAnim = "ext_c" BREAK
			ENDSWITCH
			TASK_SYNCHRONIZED_SCENE(ped, iScriptedCutsceneCannedSyncedScene, strAnimDict, strAnim, 
				INSTANT_BLEND_IN, INSTANT_BLEND_OUT, 
				SYNCED_SCENE_DONT_INTERRUPT)
		
		ENDREPEAT

		// Door anim
		PLAY_SYNCHRONIZED_ENTITY_ANIM(doorProp, iScriptedCutsceneCannedSyncedScene, "ext_door" , strAnimDict, 
			INSTANT_BLEND_IN, INSTANT_BLEND_OUT, 
			ENUM_TO_INT(SYNCED_SCENE_DONT_INTERRUPT))
			
		REPEAT FMMC_MAX_CUTSCENE_PLAYERS iPlayerPed
			PED_INDEX ped = MocapPlayerPed[iPlayerPed]
			IF NOT DOES_ENTITY_EXIST(ped)
				RELOOP
			ENDIF
			
			IF IS_PED_INJURED(ped)
				RELOOP
			ENDIF
		
			FORCE_PED_AI_AND_ANIMATION_UPDATE(ped)
		ENDREPEAT
		
		FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(doorProp)

		SET_BIT(iLocalBoolCheck27, LBOOL27_SCRIPTED_CUTSCENE_SYNCSCENE_STARTED)
	ENDIF

ENDPROC


PROC PROCESS_SCRIPTED_CUTSCENE_CANNED_SCENE_VTOL(INT iCutsceneID)

	IF iCutsceneID < 0 OR iCutsceneID >= FMMC_GET_MAX_SCRIPTED_CUTSCENES()
		EXIT
	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		EXIT
	ENDIF
	
	VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
	IF NOT IS_VEHICLE_DRIVEABLE(veh)
		EXIT
	ENDIF
	
	IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(veh)
		EXIT
	ENDIF
	
	MODEL_NAMES eModel = GET_ENTITY_MODEL(veh)
	IF NOT IS_THIS_MODEL_A_HELI(eModel)
	AND eModel != AVENGER
	AND eModel != TULA
	AND eModel != HYDRA
		EXIT
	ENDIF

	IF GET_VEHICLE_FLIGHT_NOZZLE_POSITION(veh) != 1.0
		EXIT
	ENDIF
	
	INT iCutSceneLength
	INT i
	FOR i = 0 TO g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneID].iNumCamShots
		iCutSceneLength += g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneID].iInterpTime[i]
	ENDFOR

	IF NOT HAS_NET_TIMER_STARTED(stCannedTVOL)
		REINIT_NET_TIMER(stCannedTVOL)
	ENDIF
	
	FLOAT fAlpha = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stCannedTVOL)) / TO_FLOAT(iCutSceneLength)
	FLOAT fEndPosZ = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneID].vPlayerEndPos[0].Z
	FLOAT fStartPosZ = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneID].vPlayerStartPos[0].Z
	FLOAT fTargetPosZ = INTERP_FLOAT(fStartPosZ, fEndPosZ, fAlpha, INTERPTYPE_DECEL)
	
	VECTOR vCurrentPos = GET_ENTITY_COORDS(veh, FALSE)
	FLOAT fDistFromTargetZ = fTargetPosZ - vCurrentPos.Z
	FLOAT fZVelToUse = INTERP_FLOAT(0, 10, CLAMP(fDistFromTargetZ, 0, 1))
	
	SET_ENTITY_VELOCITY(veh, <<0,0,fZVelToUse>>)
	
ENDPROC

/// PURPOSE:
///    Process playing any cutscenes that are orchestrated entirely by script with minimal paramatisation from the creator side.
/// PARAMS:
///    iCutsceneID - 
///    iAnimation - 
PROC PROCESS_SCRIPTED_CUTSCENE_CANNED_SCENE(INT iCutsceneID, INT iAnimation)
	// Wait for assets to load
	IF NOT REQUEST_RELEASE_SCRIPTED_CUTSCENE_CANNED_SCENE(iAnimation, TRUE)
		PRINTLN("PROCESS_SCRIPTED_CUTSCENE_CANNED_SCENE - iCutsceneID: ", iCutsceneID, " - iAnimation: ", iAnimation, " - Waiting for assets to laod")
		EXIT
	ENDIF

	SWITCH iAnimation
		CASE ci_CS_ANIM_APARTMENT_DOOR_ENTRY
			PROCESS_SCRIPTED_CUTSCENE_CANNED_SCENE_APARTMENT_DOOR_ENTRY(iCutsceneID)
		BREAK
		CASE ci_CS_CANNED_VTOL
			PROCESS_SCRIPTED_CUTSCENE_CANNED_SCENE_VTOL(iCutsceneID)
		BREAK
		DEFAULT
			ASSERTLN("PROCESS_SCRIPTED_CUTSCENE_CANNED_SCENE - iAnimation: ", iAnimation, " - not a valid canned animation")
		BREAK
	ENDSWITCH

ENDPROC

PROC CLEANUP_SCRIPTED_CUTSCENE_CANNED_SCENE()
	iScriptedCutsceneCannedSyncedScene = -1
ENDPROC

PROC PROCESS_DEAD_PLAYERS_WATCHING_CUTSCENE()
	IF IS_PED_INJURED(PLAYER_PED_ID())
		PRINTLN("[PROCESS_DEAD_PLAYERS_WATCHING_CUTSCENE][PROCESS_SCRIPTED_CUTSCENE] - Resurrecting Local Player for Cutscene - Preventing netcode Asserts")
		VECTOR vSpawnPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		FLOAT fSpawnHead = GET_ENTITY_HEADING(PLAYER_PED_ID())
		NETWORK_RESURRECT_LOCAL_PLAYER(vSpawnPos, fSpawnHead, 0, TRUE, FALSE)
		CACHE_LOCAL_PLAYER_VARIABLES()
		NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_SET_INVISIBLE|NSPC_FREEZE_POSITION|NSPC_NO_COLLISION)
	ENDIF
ENDPROC

//************************************************************************************************************************************************************
//
//! NEW SECTION BELOW: PROCESS SCRIPTED CUTSCENE !
//
//************************************************************************************************************************************************************

PROC PROCESS_SCRIPTED_CUTSCENE(INT iCutsceneToUse, INT iTeam)

	INT i
	PED_INDEX pedPlayers[FMMC_MAX_CUTSCENE_PLAYERS]
	PLAYER_INDEX piPlayers[FMMC_MAX_CUTSCENE_PLAYERS]
	INT iPlayers[FMMC_MAX_CUTSCENE_PLAYERS]
	
	INT iPlayerIndex = -1  
	IF ARE_TEAM_CUTSCENE_PLAYERS_SELECTED(iTeam)
		iPlayerIndex = GET_LOCAL_PLAYER_CUTSCENE_INDEX(iTeam)
	ENDIF
	
	// fallback
	IF iPlayerIndex = -1
		iPlayerIndex = GET_PLAYER_FALLBACK_CUTSCENE_INDEX()
		PRINTLN("PROCESS_SCRIPTED_CUTSCENE - iPlayerIndex = -1, using fallback index: ", iPlayerIndex)
	ENDIF
	
	// clamp
	IF iPlayerIndex < 0 OR iPlayerIndex >= FMMC_MAX_CUTSCENE_PLAYERS
		ASSERTLN("PROCESS_SCRIPTED_CUTSCENE - iPlayerIndex: ", iPlayerIndex, " is out of range 0 - FMMC_MAX_CUTSCENE_PLAYERS(", FMMC_MAX_CUTSCENE_PLAYERS, "), Clamping")
		iPlayerIndex = CLAMP_INT(iPlayerIndex, 0, FMMC_MAX_CUTSCENE_PLAYERS-1)
	ENDIF
	
	VECTOR vStartPos = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vPlayerStartPos[iPlayerIndex]
	FLOAT fStartPos = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fPlayerStartHeading[iPlayerIndex]
	IF IS_VECTOR_ZERO(vStartPos)
		vStartPos = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vPlayerStartPos[0]
		fStartPos = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fPlayerStartHeading[0]
	ENDIF

	VECTOR vEndPos = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vPlayerEndPos[iPlayerIndex]
	FLOAT fEndPos = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fPlayerEndHeading[iPlayerIndex]
	IF IS_VECTOR_ZERO(vEndPos)
		vEndPos = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vPlayerEndPos[0]
		fEndPos = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fPlayerEndHeading[0]
	ENDIF
	
	BOOL bUseClones = IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset2, ci_CSBS2_Scripted_UsePlayerClones)
	
	IF IS_BIT_SET(iLocalBoolCheck27, LBOOL27_SCRIPTED_CUTSCENE_LOCALLY_HIDE_PLAYERS)
		FOR i = 0 to (FMMC_MAX_CUTSCENE_PLAYERS - 1)
			IF IS_NET_PLAYER_OK(MC_serverBD.piCutscenePlayerIDs[iteam][i])
				SET_ENTITY_LOCALLY_INVISIBLE(GET_PLAYER_PED(MC_serverBD.piCutscenePlayerIDs[iteam][i]))
			ENDIF
		ENDFOR
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_Allow_manual_control)
		IF GET_SCRIPTED_CUTSCENE_STATE_PROGRESS() > SCRIPTEDCUTPROG_WARPINTRO
			PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE DISABLE_HUD_FOR_MANUAL_CCTV()")
			DISABLE_HUD(TRUE)
		ENDIF
	ENDIF

	SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)
	IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].iCutsceneShotBitSet[iCamShot], ciCSS_BS_Allow_manual_control)
		IF GET_SCRIPTED_CUTSCENE_STATE_PROGRESS() > SCRIPTEDCUTPROG_WARPINTRO
			SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_HELP_TEXT)
			SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FLOATING_HELP_TEXT_1)
			SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FLOATING_HELP_TEXT_2)
		ENDIF
	ENDIF
	
	eCutsceneTypePlaying = FMMCCUT_SCRIPTED
	iCutsceneIndexPlaying = iCutsceneToUse
	
	SWITCH eScriptedCutsceneProgress
		
		CASE SCRIPTEDCUTPROG_INIT
								
			IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
				VEHICLE_INDEX viVeh
				viVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
				
				IF IS_VEHICLE_DRIVEABLE(viVeh)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(viVeh)
						PRINTLN("[LM] PROCESS_SCRIPTED_CUTSCENE - Preventing vehicle from stopping due to loss of player control (1)")	
						SET_VEHICLE_STOP_INSTANTLY_WHEN_PLAYER_INACTIVE(viVeh, FALSE)
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_PlayRandomClothesAnim)
				REQUEST_ANIM_DICT("clothingtrousers")
				IF NOT HAS_ANIM_DICT_LOADED("clothingtrousers")
					EXIT
				ENDIF
				REQUEST_ANIM_DICT("clothingtie")
				IF NOT HAS_ANIM_DICT_LOADED("clothingtie")
					EXIT
				ENDIF
			ENDIF
			
			g_bInMissionControllerCutscene = TRUE
			CLEAR_BIT(iLocalBoolCheck27, LBOOL27_SCRIPTED_CUTSCENE_SYNCSCENE_STARTED)
			CLEAR_BIT(iLocalBoolCheck30, LBOOL30_PROCESSED_LEAVE_VEHICLE_FOR_CUTSCENE)
			
			IF DOES_THIS_CUTSCENE_USE_A_CCTV_FILTER( iCutsceneToUse )
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE Case 0 of a scripted cutscene - requesting CCTV assets" )
				
				REQUEST_CCTV_ASSETS()
				
				IF NOT HAVE_CCTV_ASSETS_LOADED()
					EXIT
				ENDIF
				
				SET_CCTV_LOCATION( g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].sCamName[0], g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].sLocation[0] )
				
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE CCTV assets loaded " )
			ELSE
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE Case 0 of a scripted cutscene - no CCTV assets in this scripted cutscene" )
			ENDIF
			
			IF IS_THIS_ROCKSTAR_MISSION_SVM_DUNE4(g_FMMC_STRUCT.iRootContentIDHash)
				IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_IE_SVM_dune4_Bank_Cutscene_Scene")
					START_AUDIO_SCENE("DLC_IE_SVM_dune4_Bank_Cutscene_Scene")
					PRINTLN("[AUDIO_SCENE_CUTSCENE] PROCESS_SCRIPTED_CUTSCENE - Playing DLC_IE_SVM_dune4_Bank_Cutscene_Scene")
				ENDIF
			ENDIF
			
			IF WVM_FLOW_IS_CURRENT_MISSION_WVM_MISSION()
				IF IS_WARNING_MESSAGE_ACTIVE()
				    FORCE_PLAYER_OFF_WARNING_SCREEN(TRUE)
				ENDIF 
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_UseCustomSyncScene)
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE ci_CSBS2_UseCustomSyncScene is set setting NETWORK_ALLOW_REMOTE_SYNCED_SCENE_LOCAL_PLAYER_REQUESTS to true")
				NETWORK_ALLOW_REMOTE_SYNCED_SCENE_LOCAL_PLAYER_REQUESTS(TRUE)
			ENDIF
			
			//Disable phone instructions - so we can see the CCTV controls
			g_FMMC_STRUCT.bDisablePhoneInstructions = TRUE
			g_TransitionSessionNonResetVars.contactRequests.bDisableContactRequestMenuNextFrame = TRUE
			
			SET_BIT( MC_playerBD[ iLocalPart ].iClientBitSet2, PBBOOL2_STARTED_CUTSCENE )
			SET_BIT( MC_playerBD[ iLocalPart ].iClientBitSet2, PBBOOL2_IN_MID_MISSION_CUTSCENE )
			
			CLEAR_BIT(bsSpecialCaseCutscene,BS_SCC_MANUAL_CCTV)
			CLEAR_BIT(bsSpecialCaseCutscene,BS_SCC_ADD_MANUAL_CCTV_HELP)
			CLEAR_BIT(bsSpecialCaseCutscene,BS_SCC_RESET_ADAPTATION)
			iCutsceneID_GotoShot = -1
				
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE SETTING_CCTV_LOCATION")
			
			//Pre load the first cam shot area, but only if we're <100m away, or always if we're on a heist
			VECTOR vCamStartPos
			IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[0], ciCSS_BS_Attach_Enable)
				vCamStartPos = GET_ATTACH_CAMERA_VEHICLE_POSITION(iCutsceneToUse, 0)
			ELSE
				vCamStartPos = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vCamStartPos[0]
			ENDIF		
			
			BOOL bPreloading
			IF (NOT IS_VECTOR_ZERO(vCamStartPos) AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed, vCamStartPos) <= 100)
			OR AM_I_ON_A_HEIST()
			OR WVM_FLOW_IS_CURRENT_MISSION_WVM_MISSION()				
				PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - calling PRE_LOAD_CAM_SHOT_AREA. (0)")
				bPreloading = TRUE
				PRE_LOAD_CAM_SHOT_AREA(0, vCamStartPos, 100.0)
			ENDIF
			
			IF NOT IS_BIT_SET(iLocalBoolCheck7, LBOOL7_CURRENT_SCRIPTED_CAM_AREA_LOADED)
			AND bPreloading
				PRINTLN("[PROCESS_SCRIPTED_CUTSCENE] - EXITTING - this is because - LBOOL7_CURRENT_SCRIPTED_CAM_AREA_LOADED")
				EXIT
			ENDIF
			
			SET_IM_WATCHING_A_MOCAP()
			SET_BIT( iLocalBoolCheck5, LBOOL5_PED_INVOLVED_IN_CUTSCENE )
			SET_BIT( MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYING_CUTSCENE)
		
			//Load animations:
			IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iAnimation != 0
				//ANIMATION_ON_PLAYER(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iAnimation,TRUE)
				REQUEST_RELEASE_SCRIPTED_CUTSCENE_CANNED_SCENE(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iAnimation, TRUE)
			ENDIF
			
			IF Is_MP_Objective_Text_On_Display()
				Clear_Any_Objective_Text()
			ENDIF
			
			REINIT_NET_TIMER(tdStartWarpSafetyTimer)
			
			CLEAR_AREA_OF_PROJECTILES(GET_ENTITY_COORDS(LocalPlayerPed),10.0)
			
			SET_FRONTEND_ACTIVE(FALSE)
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Lock_Player_Vehicle)
				IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)) 
						SET_VEHICLE_DOORS_LOCKED(GET_VEHICLE_PED_IS_IN(LocalPlayerPed), VEHICLELOCK_LOCKED)
						PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE LOCKING VEHICLE FOR CUTSCENE")
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR() // NOTE(Owain): We don't want spectators doing this because of B* 2235583
			AND NOT IS_PED_INJURED(localPlayerPed)
			AND (NOT IS_PLAYER_RESPAWNING(LocalPlayer) OR IS_FAKE_MULTIPLAYER_MODE_SET())
				IF MC_playerBD[iPartToUse].iTeam = iScriptedCutsceneTeam
					SET_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_REQUEST_CUTSCENE_PLAYERS)
					BROADCAST_CUTSCENE_PLAYER_REQUEST(MC_playerBD[iPartToUse].iteam, FMMCCUT_SCRIPTED)
					PRINTLN("[LM][url:bugstar:5609784] - Setting PBBOOL_REQUEST_CUTSCENE_PLAYERS (2)")
				ENDIF
			ENDIF
			PRINTLN("[RCC MISSION] IS_PED_INJURED: ", IS_PED_INJURED(localPlayerPed), " IS_PLAYER_RESPAWNING: ", IS_PLAYER_RESPAWNING(LocalPlayer), " IS_LOCAL_PLAYER_ANY_SPECTATOR: ", IS_PLAYER_RESPAWNING(LocalPlayer))
			
			SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_WAIT_PLAYER_SELECTION)
		BREAK
		
		CASE SCRIPTEDCUTPROG_WAIT_PLAYER_SELECTION
			
			BOOL bPlayersSelected, bPlayersCloned, bLiftCutsceneAssetsLoaded, bForceBail
			bPlayersSelected = ARE_TEAM_CUTSCENE_PLAYERS_SELECTED(iTeam)
			bPlayersCloned = CLONE_SCRIPTED_PLAYERS(iTeam, bUseClones) 
			bLiftCutsceneAssetsLoaded = LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS(iCutsceneToUse)
			
			IF NOT bPlayersSelected
			AND IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - SBBOOL_MISSION_OVER and NOT bPlayersSelected - Force Bail.")
				bForceBail = TRUE
			ENDIF
			
			IF bPlayersSelected
			AND bPlayersCloned
			AND bLiftCutsceneAssetsLoaded
				
				INT iCutsceneWarpRange
				iCutsceneWarpRange = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneWarpRange
				IF iCutsceneWarpRange = 0
					iCutsceneWarpRange = 125 // default range
				ENDIF
							
				IF NOT IS_VECTOR_ZERO(vStartPos)
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE start pos valid: ", vStartPos)
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed, vStartPos) > iCutsceneWarpRange
					AND NOT (IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_ANY_SPECTATOR) OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
						IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_SkipIfTooFar)
							CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE skiping as it's too far and ci_CSBS_SkipIfTooFar")
							SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_CLEANUP)
						ELSE
							CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE In Range.")
							SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_FADEOUT)
						ENDIF
					ELSE
						IF NOT (IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_ANY_SPECTATOR) OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
							bWarpPlayerAfterScriptCameaSetup = TRUE
						ENDIF
						CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE Going Straight to Warp 1.")
						SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_WARPINTRO)
					ENDIF
				ELSE
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE start pos NOT valid: ")
					IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset,ci_CSBS_SkipIfTooFar)
					AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed,g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].vCamStartPos[ 0 ]) > iCutsceneWarpRange
						CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE skiping as it's too far and ci_CSBS_SkipIfTooFar")
						SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_CLEANUP)
					ELSE
						CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE Going Straight to Warp 2.")
						IF NOT (IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_ANY_SPECTATOR) OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
							bWarpPlayerAfterScriptCameaSetup = TRUE
						ENDIF
						SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_WARPINTRO)
					ENDIF
				ENDIF
			
			ELIF bForceBail
				SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_CLEANUP)
			ENDIF
			
			IF bPlayersSelected
				PRINTLN("[LM][PROCESS_SCRIPTED_CUTSCENE] - ARE_TEAM_CUTSCENE_PLAYERS_SELECTED = true)")
			ELSE
				PRINTLN("[LM][PROCESS_SCRIPTED_CUTSCENE] - ARE_TEAM_CUTSCENE_PLAYERS_SELECTED = false")
			ENDIF			
			IF bPlayersCloned
				PRINTLN("[LM][PROCESS_SCRIPTED_CUTSCENE] - CLONE_SCRIPTED_PLAYERS = true)")
			ELSE
				PRINTLN("[LM][PROCESS_SCRIPTED_CUTSCENE] - CLONE_SCRIPTED_PLAYERS = false")
			ENDIF			
			IF bLiftCutsceneAssetsLoaded
				PRINTLN("[LM][PROCESS_SCRIPTED_CUTSCENE] - LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS = true)")
			ELSE
				PRINTLN("[LM][PROCESS_SCRIPTED_CUTSCENE] - LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS = false")
			ENDIF
		BREAK
				
		CASE SCRIPTEDCUTPROG_FADEOUT
				
			IF DOES_THIS_CUTSCENE_USE_A_CCTV_FILTER( iCutsceneToUse )
				REQUEST_CCTV_ASSETS()
			ENDIF
			
			//Load animations:
			IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iAnimation != 0
				//ANIMATION_ON_PLAYER(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iAnimation,TRUE)
				REQUEST_RELEASE_SCRIPTED_CUTSCENE_CANNED_SCENE(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iAnimation, TRUE)
			ENDIF
			
			IF NOT IS_SCREEN_FADED_OUT()
			AND NOT IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_OUT(500)
				PRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE Doing fadeout.")
			ELSE
				IF NOT IS_ENTITY_ALIVE(LocalPlayerPed)
					FORCE_RESPAWN()
					PRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE SCRIPTEDCUTPROG_FADEOUT - Forcing player respawn")
				ELIF IS_SCREEN_FADED_OUT()
					#IF IS_DEBUG_BUILD
						VECTOR vPlayerTempPos
						vPlayerTempPos = GET_ENTITY_COORDS(LocalPlayerPed)
						PRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE Player Coords: ", vPlayerTempPos, " Target vStartPos: ", vStartPos, " ci_CSBS2_RemoveFromVehEnd: ", IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_RemoveFromVehEnd) , "In Vehicle: ", IS_PED_IN_ANY_VEHICLE( LocalPlayerPed ))
					#ENDIF
					
					CLEAR_AREA_OF_PROJECTILES( vStartPos, 20.0, TRUE ) //to kill flares from smoking out the cutscene
					
					IF NOT IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_RemoveFromVehEnd)
						REMOVE_PLAYER_FROM_VEHICLE_AT_CUTSCENE_START(iCutsceneToUse, TRUE)	
					ENDIF
					
					IF IS_PED_IN_ANY_VEHICLE( LocalPlayerPed )
					AND NOT NETWORK_HAS_CONTROL_OF_ENTITY( GET_VEHICLE_PED_IS_IN( LocalPlayerPed ) )
						IF GET_DISTANCE_BETWEEN_COORDS( vStartPos, GET_ENTITY_COORDS( LocalPlayerPed ) ) < 20
						OR GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdScriptedCutsceneTimer) > 5000
							CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE We're close enough Or timed out. Going to Warp.")
							SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_WARPINTRO)
						ENDIF
					ELSE
						BOOL bQuickWarp
						
						IF DOES_THIS_CUTSCENE_WARP_INTO_AN_INTERIOR(iCutsceneToUse, GET_ENTITY_COORDS(LocalPlayerPed), vEndPos)
							PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - SCRIPTEDCUTPROG_FADEOUT - Setting bQuickWarp, iCutsceneToUse: ", iCutsceneToUse)
							bQuickWarp = TRUE							
						ENDIF

						IF NET_WARP_TO_COORD(vStartPos, fStartPos, TRUE, FALSE,DEFAULT, DEFAULT, DEFAULT, bQuickWarp)	 					
							CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE Warp Successful.")
							SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_WARPINTRO)
						ENDIF
					ENDIF
				ENDIF	
			ENDIF
		BREAK		
		
		//Intro shot
		CASE SCRIPTEDCUTPROG_WARPINTRO
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_START_IN_CUTSCENE)
				IF NOT IS_SKYSWOOP_AT_GROUND()
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - Dont progress til skyswoop is down - ScriptedCutsceneProgress: ", GET_SCRIPTED_CUTSCENE_STATE_PROGRESS_NAME(GET_SCRIPTED_CUTSCENE_STATE_PROGRESS()))
					EXIT
				ENDIF
			ENDIF
			
			PROCESS_DEAD_PLAYERS_WATCHING_CUTSCENE()
			
			// Added because this is normally done if we go into FADEOUT stage, so it should be fine to perform this function before the rest of WARPINTRO.
			
			BOOL bCasinoDirect
			bCasinoDirect = (g_sMPTunables.iCasinoHeistRootContentID[ENUM_TO_INT(ciCASINO_HEIST_MISSION_DIRECT_STAGE_1A)] = g_FMMC_STRUCT.iRootContentIDHash)
			IF bCasinoDirect
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - running scripted cutscene in casino direct mission.")
			ENDIF
			
			IF NOT bWarpPlayerAfterScriptCameaSetup
			OR (bCasinoDirect AND SHOULD_CUTSCENE_REMOVE_PLAYER_FROM_VEHICLE_AT_START(iCutsceneToUse, TRUE))
				IF NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_PROCESSED_LEAVE_VEHICLE_FOR_CUTSCENE)
					IF SHOULD_CUTSCENE_REMOVE_PLAYER_FROM_VEHICLE_AT_START(iCutsceneToUse, TRUE)
						CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - Called: REMOVE_PLAYER_FROM_VEHICLE_AT_CUTSCENE_START.")
						REMOVE_PLAYER_FROM_VEHICLE_AT_CUTSCENE_START(iCutsceneToUse, TRUE)	
						SET_BIT(iLocalBoolCheck30, LBOOL30_PROCESSED_LEAVE_VEHICLE_FOR_CUTSCENE)
					ENDIF
				ENDIF			
				IF (IS_BIT_SET(iLocalBoolCheck30, LBOOL30_PROCESSED_LEAVE_VEHICLE_FOR_CUTSCENE)
				AND IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, bCasinoDirect))
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - Waiting to leave the vehicle.")
					EXIT
				ENDIF
			ENDIF
		
			TOGGLE_PLAYER_DAMAGE_OVERLAY(FALSE)
			SET_PLAYER_INVINCIBLE(LocalPlayer, TRUE)
			
			DISABLE_INTERACTION_MENU()
			
			DO_FOCUS_INTRO_CAM( iCutsceneToUse )
			
			DO_CUTSCENE_PHONE_INTRO( iCutsceneToUse )
			
			IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
				IF( IS_CUTSCENE_USING_HACKING_INTRO( iCutsceneToUse ) AND NOT IS_HACKING_INTRO_COMPLETE() AND HAS_THIS_PLAYER_COMPLETED_HACKING_MINGAME() )
					DISABLE_CONTROL_FOR_HACKING_INTRO()
				ELSE
					NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_PREVENT_EVERYBODY_BACKOFF|NSPC_LEAVE_CAMERA_CONTROL_ON|NSPC_ALLOW_PAD_SHAKE|NSPC_REENABLE_CONTROL_ON_DEATH)
				ENDIF
			ENDIF
			
			IF DOES_THIS_CUTSCENE_USE_A_CCTV_FILTER( iCutsceneToUse )
				IF NOT IS_BIT_SET(bsSpecialCaseCutscene,BS_SCC_MANUAL_CCTV)
					vCCTVRot[iCamShot] = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vCamStartRot[iCamShot]
					iCoolDownTimerForManualCCTV = GET_GAME_TIMER()
					SET_BIT(bsSpecialCaseCutscene,BS_SCC_MANUAL_CCTV)
				ENDIF
			ENDIF
			
			INIT_SCRIPTED_CUTSCENE_LIFT(iCutsceneToUse)
			
			PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - INSIDE CUTSCENE INTRO SECTION ScriptedCutsceneProgress: ", GET_SCRIPTED_CUTSCENE_STATE_PROGRESS_NAME(GET_SCRIPTED_CUTSCENE_STATE_PROGRESS()))
			
			//After Intro Completion
			IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_PHONE_INTRO)
			AND IS_BIT_SET(iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_FOCUS_INTRO) 
			AND IS_BIT_SET(iLocalBoolCheck7, LBOOL7_DONE_CUTSCENE_HACKING_INTRO)
				//Load animations:
				IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iAnimation != 0
					//ANIMATION_ON_PLAYER(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iAnimation,TRUE)
					REQUEST_RELEASE_SCRIPTED_CUTSCENE_CANNED_SCENE(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iAnimation, TRUE)
				ENDIF
				
				SET_BIT(iLocalBoolCheck4, LBOOL4_SCRIPTED_CUT_NEEDS_CLEANED_UP)
				
				iLocalDialoguePriority = -1
				
				//Kill any existing hint cams
				IF IS_GAMEPLAY_HINT_ACTIVE()
					CPRINTLN( DEBUG_SIMON, "next shot" )
					KILL_CHASE_HINT_CAM(sIntroHintCam)
				ENDIF

				IF NOT DOES_CAM_EXIST(cutscenecam)			
					cutscenecam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",FALSE) 
				ENDIF
				
				IF DOES_CAM_EXIST(cutscenecam)	
					SET_CAM_ACTIVE(cutscenecam, TRUE)
					SETUP_CAM_PARAMS(iCutsceneToUse)
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_UseCustomSyncScene)	
						// Maybe check camshot?
						PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - Rendering Script Cams.")
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
					ELSE
						SET_CAM_ACTIVE(cutscenecam, FALSE)
					ENDIF
				ENDIF
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_START_IN_CUTSCENE)
					ANIMPOSTFX_STOP("MP_job_load")
					CLEANUP_ALL_CORONA_FX(FALSE)
					SET_SKYFREEZE_CLEAR()
					SET_HIDOF_OVERRIDE(FALSE,FALSE,0,0,0,0)
				ENDIF
				
				CLEAR_CUTSCENE_AREA(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fClearRadius, g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vClearCoOrds,g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vReplacementArea)
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Hide_Other_Players)
					IF ARE_STRINGS_EQUAL(sMocapCutscene, "mph_pri_sta_mcs1") //prison - station / casco
					OR ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PAC_WIT_MCS1") //Pacific Standard Boat Cutscene
						CONCEAL_ALL_OTHER_PLAYERS(TRUE, TRUE)
					ELSE
						IF NOT IS_PLAYER_AT_ORNATE_BANK(TRUE,iCutsceneToUse,iCamShot)
							CONCEAL_ALL_OTHER_PLAYERS(TRUE)	
						ENDIF
					ENDIF
				ELIF bUseClones
					
					PED_INDEX OriginalPlayerPed
					VECTOR vPlayer
					
					//Release the peds from whatever scene they are currently in
					STOP_SYNC_SCENE(iScriptedCutsceneCannedSyncedScene)
					SET_BIT(iLocalBoolCheck27, LBOOL27_SCRIPTED_CUTSCENE_LOCALLY_HIDE_PLAYERS)
					
					FOR i = 0 to (FMMC_MAX_CUTSCENE_PLAYERS - 1)
						IF IS_NET_PLAYER_OK(MC_serverBD.piCutscenePlayerIDs[iteam][i])
							
							// Hide the players and show the clones!
							
							OriginalPlayerPed = GET_PLAYER_PED(MC_serverBD.piCutscenePlayerIDs[iteam][i])
							SET_ENTITY_LOCALLY_INVISIBLE(OriginalPlayerPed)
							
							IF DOES_ENTITY_EXIST(MocapPlayerPed[i])
							AND NOT IS_PED_INJURED(MocapPlayerPed[i])
								vPlayer = GET_ENTITY_COORDS(OriginalPlayerPed)
								SET_ENTITY_COORDS_NO_OFFSET(MocapPlayerPed[i], vPlayer)
								SET_ENTITY_HEADING(MocapPlayerPed[i], GET_ENTITY_HEADING(OriginalPlayerPed))
								PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - Setting clone ",i," at player coords ",vPlayer)
								
								SET_ENTITY_VISIBLE(MocapPlayerPed[i], TRUE)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(MocapPlayerPed[i])
								
								IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_PlayRandomClothesAnim)
									STRING sAnim
									TEXT_LABEL_63 tlDict
									tlDict = ""
									sAnim = GET_RANDOM_CLOTHING_ANIMATION_FOR_CUTSCENE(tlDict)
									TASK_PLAY_ANIM(MocapPlayerPed[i], sAnim, tlDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_FORCE_START)
									
									PRINTLN("[LM][RandomClothesAnim] - iPed: ", i, " sAnim: ", sAnim, " tlDict: ", tlDict)
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
				
				//HIDE PLAYER AND OTHERS FOR ORNATE BANK VAULT OPENING
				IF IS_PLAYER_AT_ORNATE_BANK(TRUE,iCutsceneToUse,iCamShot)
					PRINTLN("[RCC MISSION][CUTSCENE_VISIBILITY] PROCESS_SCRIPTED_CUTSCENE HIDE PLAYER AND OTHERS FOR ORNATE BANK VAULT OPENING")
					CONCEAL_ALL_OTHER_PLAYERS(TRUE, TRUE, TRUE)
					SET_BIT(iLocalBoolCheck28, LBOOL28_PLAYERS_CONCEALED_DUE_TO_BANK_VAULT_SHOT)
				ENDIF
				
				IF ANIMPOSTFX_IS_RUNNING("MP_Celeb_Preload_Fade")
					ANIMPOSTFX_STOP("MP_Celeb_Preload_Fade")
					PRINTLN("[RCC MISSION] - [SAC] - PROCESS_SCRIPTED_CUTSCENE  MP_Celeb_Preload_Fade for mid mission cutscene.")
				ENDIF
				
				REINIT_NET_TIMER(tdScriptedCutsceneTimer)
				
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset2, ci_CSBS2_ShowPlayerWeapon)
					SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed,FALSE)
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset2, ci_CSBS2_EquipWeaponAtEnd)
					WEAPON_TYPE tempWeapon
					GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), tempWeapon)
					IF tempWeapon != WEAPONTYPE_UNARMED
					AND tempWeapon != WEAPONTYPE_INVALID
						GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtPreCutsceneWeapon)
						PRINTLN("[RCC MISSION] - PROCESS_SCRIPTED_CUTSCENE - Storing weapon ",GET_WEAPON_NAME(wtPreCutsceneWeapon), " to re-equip.")
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_SecurityCamFilter)
					IF NOT IS_BIT_SET(iLocalBoolCheck7, LBOOL7_SECURITY_CAM_FILTER_USED)
						SET_BIT(iLocalBoolCheck7, LBOOL7_SECURITY_CAM_FILTER_USED)
					ENDIF
					
					g_FMMC_STRUCT.bSecurityCamActive = TRUE
					SET_TIMECYCLE_MODIFIER("CAMERA_secuirity")
					REQUEST_CCTV_ASSETS()
					IF HAVE_CCTV_ASSETS_LOADED()
						IF HAS_SCALEFORM_MOVIE_LOADED(SI_SecurityCam)
							SET_CCTV_LOCATION(g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].sCamName[iCamShot],g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].sLocation[iCamShot])
							PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE SETTING LOCATION DATA FOR THIS SHOT")
						ENDIF
					ENDIF
					
					g_FMMC_STRUCT.bSecurityCamActive = TRUE
					SET_TIMECYCLE_MODIFIER("CAMERA_secuirity")
				ELSE
					g_FMMC_STRUCT.bSecurityCamActive = FALSE
					CLEAR_TIMECYCLE_MODIFIER()
				ENDIF
				
				IF DOES_CUTSCENE_HAVE_REGISTERED_ENTITIES(iCutsceneToUse, FMMCCUT_SCRIPTED)
					PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE DOES_CUTSCENE_HAVE_REGISTERED_ENTITIES TRUE ")
					REGISTER_CUTSCENE_ENTITIES(iCutsceneToUse,TRUE, FMMCCUT_SCRIPTED)
					IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Hide_Your_Player)
						
						SET_ENTITY_VISIBLE_IN_CUTSCENE( LocalPlayerPed, FALSE )
						PRINTLN("[MJM] PROCESS_SCRIPTED_CUTSCENE IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Hide_Your_Player)")
					ELSE
						SET_ENTITY_VISIBLE_IN_CUTSCENE( LocalPlayerPed, TRUE )
						PRINTLN("[MJM] PROCESS_SCRIPTED_CUTSCENE NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Hide_Your_Player)")
					ENDIF
					START_MP_CUTSCENE(TRUE,TRUE)
					SET_NETWORK_CUTSCENE_ENTITIES( TRUE )
				ELSE
					PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE PROCESS_SCRIPTED_CUTSCENE DOES_CUTSCENE_HAVE_REGISTERED_ENTITIES FALSE ")
					START_MP_CUTSCENE(FALSE)
				ENDIF
				
				
				HIDE_SPECTATOR_HUD(TRUE)
				HIDE_SPECTATOR_BUTTONS(TRUE)
				
				IF MC_playerBD[iPartToUse].iteam = iScriptedCutsceneTeam
					IF iCamShot >= g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iMidPoint
					AND NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[iPartToUse].iteam], MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
						BROADCAST_OBJECTIVE_MID_POINT(MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam],MC_playerBD[iPartToUse].iteam)
					ENDIF
					IF MC_serverBD.iSpawnScene != iCutsceneToUse
					OR MC_serverBD.iSpawnShot != iCamShot
						IF NOT HAS_NET_TIMER_STARTED(tdCutsceneEventDelay)
						OR HAS_NET_TIMER_EXPIRED(tdCutsceneEventDelay, ciCUTSCENE_EVENT_DELAY)
							REINIT_NET_TIMER(tdCutsceneEventDelay)
							BROADCAST_CUTSCENE_SPAWN_PROGRESS(iCutsceneToUse,iCamShot)
						ENDIF
					ENDIF
				ENDIF
				
				RESET_PER_SHOT_PHONE_INTRO()
				
				IF bWarpPlayerAfterScriptCameaSetup
					
					IF NOT SPECIAL_CUTSCENE_VEHICLE_WARP( iCutsceneToUse, iPlayerIndex )
						REMOVE_PLAYER_FROM_VEHICLE_AT_CUTSCENE_START(iCutsceneToUse, TRUE)
						
						BOOL bShouldPlayerUseWarp 
						bShouldPlayerUseWarp = TRUE				
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_RemoveFromVehStart)
						AND IS_PED_IN_ANY_VEHICLE(localPlayerPed)
						AND NOT NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(localPlayerPed))
							bShouldPlayerUseWarp = FALSE
						ENDIF
												
						BOOL bKeepVehicle
						bKeepVehicle = (NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_RemoveFromVehStart))
						
						IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION() OR IS_FAKE_MULTIPLAYER_MODE_SET()
							IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_RemoveFromVehStart) 
							AND IS_PED_IN_ANY_VEHICLE(localPlayerPed) OR (IS_PED_IN_ANY_VEHICLE(localPlayerPed) AND NOT IS_PED_SITTING_IN_ANY_VEHICLE(localPlayerPed))
								bKeepVehicle = FALSE
							ENDIF
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset,ci_CSBS_WarpPlayersStart)
						AND bShouldPlayerUseWarp
							IF NOT (IS_PED_IN_ANY_VEHICLE(PlayerPedToUse) AND NOT NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(PlayerPedToUse)))
								IF NET_WARP_TO_COORD(vStartPos, fStartPos, bKeepVehicle, FALSE, FALSE, FALSE, TRUE, TRUE)
									PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE SET TO WARP with bKeepVehicle: ", bKeepVehicle)
									PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE, start valid warping, ScriptedCutsceneProgress: ", GET_SCRIPTED_CUTSCENE_STATE_PROGRESS_NAME(GET_SCRIPTED_CUTSCENE_STATE_PROGRESS()))
								ENDIF
							ENDIF
						ELSE
							PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE ScriptedCutsceneProgress: ", GET_SCRIPTED_CUTSCENE_STATE_PROGRESS_NAME(GET_SCRIPTED_CUTSCENE_STATE_PROGRESS()))
						ENDIF
					ENDIF
					SET_BIT(iLocalBoolCheck30, LBOOL30_PROCESSED_LEAVE_VEHICLE_FOR_CUTSCENE)
					bWarpPlayerAfterScriptCameaSetup = FALSE
				ENDIF
				
				IF NOT IS_BIT_SET(bsSpecialCaseCutscene,BS_SCC_ADD_MANUAL_CCTV_HELP)
					IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].iCutsceneShotBitSet[iCamShot], ciCSS_BS_Allow_manual_control)
						SETUP_MANUAL_CCTV_HELP()
						SET_BIT(bsSpecialCaseCutscene,BS_SCC_ADD_MANUAL_CCTV_HELP)
					ENDIF
				ENDIF
				
				IF NOT IS_VECTOR_ZERO(vStartPos)
					
					PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - Player Start Vehicle Warp - Valid vStartPos: ", vStartPos)
					
					VEHICLE_INDEX vehStartWarp
					vehStartWarp = GET_CUTSCENE_PED_WARP_VEHICLE(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].sPlayerStartWarp[iPlayerIndex])
					IF DOES_ENTITY_EXIST(vehStartWarp)
						
						PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - Player Start Vehicle Warp - Vehicle exists")
					
						IF NETWORK_HAS_CONTROL_OF_ENTITY(vehStartWarp)
							
							PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - Player Start Vehicle Warp - Setting vehicle coords: ", vStartPos, ", heading", fStartPos)
							
							SET_ENTITY_COORDS(vehStartWarp, vStartPos, FALSE)
							SET_ENTITY_HEADING(vehStartWarp, fStartPos)
							
							FLOAT fGround
							IF NOT GET_GROUND_Z_FOR_3D_COORD(vStartPos, fGround, TRUE)
							OR vStartPos.Z - fGround > 2.0
							
								MODEL_NAMES eModel
								eModel = GET_ENTITY_MODEL(vehStartWarp)
								IF IS_THIS_MODEL_A_HELI(eModel)
								OR IS_THIS_MODEL_A_PLANE(eModel)
									SET_HELI_HOVERING(vehStartWarp)
								ENDIF
							
							ENDIF
							
						ENDIF
						
					ELSE
						PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - Player Start Vehicle Warp - no vehicle target")
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - Player Start Vehicle Warp - Invalid start coords")
				ENDIF
				CUTSCENE_WARP_PED_TO_VEHICLE_COMMON(LocalPlayerPed, g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].sPlayerStartWarp[iPlayerIndex])
				
				PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE Warped and Setup.")
				IF IS_LOCAL_PLAYER_USING_DRONE()
					SET_DRONE_FORCE_EXTERNAL_CLEANING_UP(TRUE)
				ENDIF
				SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_PROCESS_SHOTS)
			ENDIF
			
		BREAK
		
		//Process Shots
		CASE SCRIPTEDCUTPROG_PROCESS_SHOTS
			ASSIGN_PLAYER_SPECIFIC_ANIMATIONS_PED_POOL(pedPlayers, piPlayers, iPlayers)
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_UseCustomSyncScene)
				IF SHOULD_WAIT_FOR_SYNC_SCENE_PARTICIPANTS(iPlayers)
					PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - SCRIPTEDCUTPROG_PROCESS_SHOTS - Waiting for SHOULD_WAIT_FOR_SYNC_SCENE_PARTICIPANTS")
					EXIT					
				ENDIF
			ENDIF			
			
			PROCESS_PLAY_ANIM_SYNC_SCENE_FOR_SCRIPTED_CUTSCENE(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].sCutsceneSyncSceneData, iCamShot, iCutsceneToUse, pedPlayers, iPlayers)
			
			IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( MC_ServerBD.tdCutsceneStartTime[ iScriptedCutsceneTeam ],FALSE,TRUE ) < 1000
				PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE CURRENT NETWORK TIMER: ", NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()))
				PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE GET_NET_TIMER_DIFF", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_ServerBD.tdCutsceneStartTime[ iScriptedCutsceneTeam ],FALSE,TRUE),"[RCC MISSION] is host: ", bIsLocalPlayerHost)
				
				EXIT
			ENDIF
			
			SET_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_REACHED_CUT_PROCESS_SHOTS)
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].iCutsceneShotBitSet[iCamShot], ciCSS_BS_Allow_manual_control)
				IF NOT IS_BIT_SET(bsSpecialCaseCutscene,BS_SCC_ADD_MANUAL_CCTV_HELP)
					IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].iCutsceneShotBitSet[iCamShot], ciCSS_BS_Allow_manual_control)
						SETUP_MANUAL_CCTV_HELP()
						SET_BIT(bsSpecialCaseCutscene,BS_SCC_ADD_MANUAL_CCTV_HELP)
					ENDIF
				ENDIF
				
				IF LOAD_MENU_ASSETS()
					//DRAW_MENU()
					SETUP_MANUAL_CCTV_HELP()
					PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE TRYING TO DRAW SCALEFORM HELP - 1846")
					INT iScreenX, iScreenY
					GET_SCREEN_RESOLUTION(iScreenX, iScreenY)
					DRAW_MENU_HELP_SCALEFORM(iScreenX)
				ELSE
					PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE TRYING TO LOAD_MENU_ASSETS()")
				ENDIF
			ELSE
				PRINTLN("iCamShot: ",iCamShot)
				PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE MANUAL CCTV IS TURNED OFF")
			ENDIF
			
			//IF NOT IS_BIT_SET(bsSpecialCaseCutscene,BS_SCC_RESET_ADAPTATION)
			IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].iCutsceneShotBitSet[iCamShot], ciCSS_BS_Interior_to_Exterior)
				IF NOT IS_BIT_SET(bsSpecialCaseCutscene,BS_SCC_RESET_ADAPTATION)
					RESET_ADAPTATION(1)
					PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE RESET_ADAPTATION CALLED")
					SET_BIT(bsSpecialCaseCutscene,BS_SCC_RESET_ADAPTATION)
				ENDIF
			ENDIF
				
			//Play animations / dialogue:
			IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iAnimation != 0
				PROCESS_SCRIPTED_CUTSCENE_CANNED_SCENE(iCutsceneToUse, g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iAnimation)
			ENDIF
			
			ENTITY_INDEX EntID
			NETWORK_INDEX netID
			
			REPEAT FMMC_MAX_CUTSCENE_ENTITIES i
				IF NOT IS_BIT_SET( iScriptedEntitiesRegsitered, i )		
					IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].sCutsceneEntities[i].iType !=-1
					AND g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].sCutsceneEntities[i].iIndex != -1
						
						IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].sCutsceneEntities[i].iType = CREATION_TYPE_PROPS
							EntID = GET_CUTSCNE_VISIBLE_PROP(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].sCutsceneEntities[i].iIndex)
							IF EntID != NULL
								SET_ENTITY_VISIBLE(EntID, TRUE)
								SET_BIT( iScriptedEntitiesRegsitered, i )
							ENDIF
						ELSE
							netid = GET_CUTSCNE_VISIBLE_NET_ID(	g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].sCutsceneEntities[i].iType,
																g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].sCutsceneEntities[i].iIndex)
							PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE REGISTER_CUTSCENE_ENTITIES - GET_CUTSCNE_VISIBLE_NET_ID has returned a thing: NULL = ",netid = NULL,", IS_CUTSCENE_PLAYING = ",IS_CUTSCENE_PLAYING())
							IF netid != NULL
								SET_NETWORK_ID_VISIBLE_IN_CUTSCENE_HACK( netID, TRUE, FALSE )
								SET_BIT( iScriptedEntitiesRegsitered, i )
							ENDIF
						ENDIF
						
					ELSE
						SET_BIT( iScriptedEntitiesRegsitered, i )
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_PlayRandomClothesAnim)
				STRING sAnim
				TEXT_LABEL_63 tlDict 
				sAnim = GET_RANDOM_CLOTHING_ANIMATION_FOR_CUTSCENE(tlDict)
				IF NOT HAS_NET_TIMER_STARTED(tdRandomClothingAnimDelay)
					START_NET_TIMER(tdRandomClothingAnimDelay)
				ENDIF
				IF NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_CUTSCENE_PLAYED_CLOTHING_ANIM)								
					FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed)
					TASK_PLAY_ANIM(LocalPlayerPed, tlDict, sAnim, SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_FORCE_START)
					PRINTLN("[LM][RandomClothesAnim] - sAnim: ", sAnim, " tlDict: ", tlDict)
					SET_BIT(iLocalBoolCheck30, LBOOL30_CUTSCENE_PLAYED_CLOTHING_ANIM)
				ENDIF
				IF NOT IS_ENTITY_PLAYING_ANIM(LocalPlayerPed, tlDict, sAnim, ANIM_DEFAULT)
				AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdRandomClothingAnimDelay, 4000)
					RESET_NET_TIMER(tdRandomClothingAnimDelay)
					CLEAR_BIT(iLocalBoolCheck30, LBOOL30_CUTSCENE_PLAYED_CLOTHING_ANIM)
				ENDIF
			ENDIF
			
			INT iNumCamShots
			iNumCamShots = g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].iNumCamShots
			
			DO_HUMANE_DELIVER_EMP_SPECIAL_CASE_CHECKS( iCutsceneToUse )
			
			PROCESS_PLAY_PLAYER_SPECIFIC_ANIMATIONS_FOR_SCENE(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].sPlayerSpecificData, iCamShot, pedPlayers)			
			
			BOOL bProgress			
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].iCutsceneShotBitSet[iCamShot], ciCSS_BS_Allow_manual_control)
				
				IF iCamShot < iNumCamShots
					
					VECTOR vStreamPos
					FLOAT fStreamRadius
					
					IF( IS_BIT_SET( g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].iCutsceneShotBitSet[ iCamShot + 1 ], ciCSS_BS_Attach_Enable ) )
						vStreamPos = GET_ATTACH_CAMERA_VEHICLE_POSITION( iCutsceneToUse, iCamShot + 1 )
					ELSE
						vStreamPos = g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].vStreamStartPos[ iCamShot + 1 ]
					ENDIF
					
					fStreamRadius = g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].fStreamRadius[ iCamShot + 1 ]

					//Pre load the next cam shot area
					IF ( iCamShot + 1 ) < iNumCamShots
						PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - calling PRE_LOAD_CAM_SHOT_AREA. (1)")
						PRE_LOAD_CAM_SHOT_AREA( iCamShot + 1, vStreamPos, fStreamRadius )
					ELSE
						// This seems to just get bad coordinates like -180 under the floor and causing us to load bad places for spectators.
						IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
							PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - calling PRE_LOAD_CAM_SHOT_AREA. (2)")
							PRE_LOAD_CAM_SHOT_AREA( iCamShot + 1, GET_ENTITY_COORDS(localPlayerPed), 50.0 )
						ENDIF
					ENDIF
						
					DO_PER_SHOT_PHONE_INTRO( iCutsceneToUse, iCamShot )
					
					INT iInterpTime
					iInterpTime = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iInterpTime[iCamShot]
					IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_EarlyFinish)
						iInterpTime -= SCRIPTED_CUTSCENE_END_FADE_DURATION
					ENDIF

					IF ((GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdScriptedCutsceneTimer) >= iInterpTime) 
						AND (NOT IS_CAM_INTERPOLATING(cutscenecam) OR IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_EarlyFinish)))
					OR IS_PHOTO_INTRO_DONE_FOR_SHOT( iCutsceneToUse, iCamShot ) // phone intro done for this shot
						iCamShot++
						CLEAR_BIT( iLocalBoolCheck8, LBOOL8_HUMANE_DELIVER_EMP_PER_SHOT_FLAG )
						IF MC_playerBD[iPartToUse].iteam = iScriptedCutsceneTeam
							IF NOT IS_BIT_SET( MC_serverBD.iObjectiveMidPointBitset[ MC_playerBD[iPartToUse].iteam ], MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] )
								IF iCamShot >= g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iMidPoint
									BROADCAST_OBJECTIVE_MID_POINT(MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam],MC_playerBD[iPartToUse].iteam)
								ENDIF
							ENDIF
							IF MC_serverBD.iSpawnScene != iCutsceneToUse
							OR MC_serverBD.iSpawnShot != iCamShot
								IF NOT HAS_NET_TIMER_STARTED(tdCutsceneEventDelay)
								OR HAS_NET_TIMER_EXPIRED(tdCutsceneEventDelay, ciCUTSCENE_EVENT_DELAY)
									REINIT_NET_TIMER(tdCutsceneEventDelay)
									BROADCAST_CUTSCENE_SPAWN_PROGRESS(iCutsceneToUse,iCamShot)
								ENDIF
							ENDIF
						ENDIF
						IF iCamShot < iNumCamShots
							SETUP_CAM_PARAMS(iCutsceneToUse)										
							IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_SecurityCamFilter) 
								g_FMMC_STRUCT.bSecurityCamActive = TRUE
								SET_TIMECYCLE_MODIFIER("CAMERA_secuirity")
								REQUEST_CCTV_ASSETS()
								IF HAVE_CCTV_ASSETS_LOADED()
									IF HAS_SCALEFORM_MOVIE_LOADED(SI_SecurityCam)
										SET_CCTV_LOCATION(g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].sCamName[iCamShot],g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].sLocation[iCamShot])
										PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE SETTING LOCATION DATA FOR THIS SHOT")
									ENDIF
								ENDIF
							ELSE
								g_FMMC_STRUCT.bSecurityCamActive = FALSE
								CLEAR_TIMECYCLE_MODIFIER()
							ENDIF
							REINIT_NET_TIMER(tdScriptedCutsceneTimer)
						ENDIF
					ENDIF
					
					IF iCutsceneID_GotoShot < iCamShot
						IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_LocalPlayerDrive)
						AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vDriveToPosition[iCamShot])
						AND IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, TRUE)
							
							VEHICLE_INDEX playerVeh
							playerVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed, TRUE)
							PED_INDEX driverPed
							driverPed = GET_PED_IN_VEHICLE_SEAT(playerVeh, VS_DRIVER, TRUE)
							
							IF driverPed = LocalPlayerPed
								
								FLOAT fCruiseSpeed, fVehArriveRange
								DRIVINGMODE dmFlags
								TASK_GO_TO_COORD_ANY_MEANS_FLAGS tgcamFlags
								BOOL bLongRange
								
								IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fDriveSpeed[iCamShot] > 0.0
									fCruiseSpeed = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fDriveSpeed[iCamShot]
								ELSE
									fCruiseSpeed = 5.0
								ENDIF
								
								fVehArriveRange = 4.0
								dmFlags = DF_SwerveAroundAllCars | DF_SteerAroundStationaryCars | DF_SteerAroundObjects | DF_SteerAroundPeds | DF_UseShortCutLinks | DF_ChangeLanesAroundObstructions | DF_DriveIntoOncomingTraffic | DF_UseSwitchedOffNodes | DF_AdjustCruiseSpeedBasedOnRoadSpeed | DF_ForceJoinInRoadDirection
								tgcamFlags = TGCAM_IGNORE_VEHICLE_HEALTH | TGCAM_REMAIN_IN_VEHICLE_AT_DESTINATION
								
								IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_LocalPlayerDrive_PreferNavmesh)
									PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - Player drive task add DF_PreferNavmeshRoute for iCamShot ",iCamShot," in iCutsceneToUse ",iCutsceneToUse)
									dmFlags = dmFlags | DF_PreferNavmeshRoute
								ENDIF
								
								IF VDIST2(GET_ENTITY_COORDS(LocalPlayerPed), g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vDriveToPosition[iCamShot]) >= 90000 // If we need to travel more than 300m
									bLongRange = TRUE
								ENDIF
								
								PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - Calling TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS_WITH_CRUISE_SPEED w fCruiseSpeed ",fCruiseSpeed," bLongRange ",bLongRange,", to vDriveToPosition ",g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vDriveToPosition[iCamShot],", iCamShot ",iCamShot,", iCutsceneToUse ",iCutsceneToUse)
								TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS_WITH_CRUISE_SPEED(LocalPlayerPed, g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vDriveToPosition[iCamShot], PEDMOVEBLENDRATIO_RUN, playerVeh, bLongRange, dmFlags, -1, 0, 10, tgcamFlags, fCruiseSpeed, fVehArriveRange)

								iCutsceneID_GotoShot = iCamShot
							ELSE
								PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - I'm not in the driver seat; vDriveToPosition ",g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vDriveToPosition[iCamShot],", iCamShot ",iCamShot,", iCutsceneToUse ",iCutsceneToUse)
								iCutsceneID_GotoShot = iCamShot
							ENDIF
							
						ELIF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_LocalPlayerWalk)
						OR IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_LocalPlayerRun)
						OR IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_LocalPlayerSprint)
							FLOAT fPedMoveBlendRatio
							fPedMoveBlendRatio = PEDMOVEBLENDRATIO_WALK
							IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_LocalPlayerRun)
								fPedMoveBlendRatio = PEDMOVEBLENDRATIO_RUN
							ENDIF
							IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_LocalPlayerSprint)
								fPedMoveBlendRatio = PEDMOVEBLENDRATIO_SPRINT
							ENDIF
							
							IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vWalkToPosition[iCamShot])
								IF NOT bUseClones									
									IF NOT IS_PED_INJURED(LocalPlayerPed)
										IF NOT IS_PED_IN_ANY_VEHICLE(localPlayerPed, TRUE)
											IF NOT IS_BIT_SET(iLocalBoolCheck32, LBOOL32_SCRIPTED_CUTSCENE_PLAYING_ANIM)											
												IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fWalkToPositionRadius[iCamShot] < 1.0										
													PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - Calling TASK_GO_TO_COORD_ANY_MEANS on player to vWalkToPosition ",g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vWalkToPosition[iCamShot],", iCamShot ",iCamShot,", iCutsceneToUse ",iCutsceneToUse, " fPedMoveBlendRatio: ", fPedMoveBlendRatio)
													TASK_GO_TO_COORD_ANY_MEANS(LocalPlayerPed, g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vWalkToPosition[iCamShot], fPedMoveBlendRatio, NULL)
												ELSE
													PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - Calling TASK_GO_STRAIGHT_TO_COORD on player to vWalkToPosition ",g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vWalkToPosition[iCamShot],", iCamShot ",iCamShot,", iCutsceneToUse ",iCutsceneToUse, " fPedMoveBlendRatio: ", fPedMoveBlendRatio, "fRadius = ", g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fWalkToPositionRadius[iCamShot])
													TASK_GO_STRAIGHT_TO_COORD(LocalPlayerPed, g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vWalkToPosition[iCamShot], fPedMoveBlendRatio, DEFAULT, DEFAULT, g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fWalkToPositionRadius[iCamShot])
												ENDIF
											ELSE
												PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - Cannot call TASK_GO_TO_COORD_ANY_MEANS on player to vWalkToPosition ",g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vWalkToPosition[iCamShot],", iCamShot ",iCamShot,", iCutsceneToUse ",iCutsceneToUse, " PLAYER IS PERFORMING SPECIFIC ANIM...")
											ENDIF
										ELSE
											IF GET_SCRIPT_TASK_STATUS(localPlayerPed, SCRIPT_TASK_LEAVE_VEHICLE) != WAITING_TO_START_TASK
											AND GET_SCRIPT_TASK_STATUS(localPlayerPed, SCRIPT_TASK_LEAVE_VEHICLE) != PERFORMING_TASK
												PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - Calling TASK_LEAVE_ANY_VEHICLE on local player ",g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vWalkToPosition[iCamShot],", iCamShot ",iCamShot,", iCutsceneToUse ",iCutsceneToUse)
												TASK_LEAVE_ANY_VEHICLE(LocalPlayerPed, 0, ECF_DONT_DEFAULT_WARP_IF_DOOR_BLOCKED)
											ELSE
												PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - Waiting for player to have left the vehicle ",g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vWalkToPosition[iCamShot],", iCamShot ",iCamShot,", iCutsceneToUse ",iCutsceneToUse)
											ENDIF
										ENDIF
									ELSE
										PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - Cannot call TASK_GO_TO_COORD_ANY_MEANS on player to vWalkToPosition ",g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vWalkToPosition[iCamShot],", iCamShot ",iCamShot,", iCutsceneToUse ",iCutsceneToUse, " PLAYER IS INJURED...")
									ENDIF
								ELSE
									FOR i = 0 to (FMMC_MAX_CUTSCENE_PLAYERS - 1)
										IF DOES_ENTITY_EXIST(MocapPlayerPed[i])
										AND NOT IS_PED_INJURED(MocapPlayerPed[i])
										OR IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_LocalPlayerRun)
										OR IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_LocalPlayerSprint)
											IF NOT IS_BIT_SET(iLocalBoolCheck32, LBOOL32_SCRIPTED_CUTSCENE_PLAYING_ANIM)
											OR MocapPlayerPed[i] != LocalPlayerPed
												IF NOT IS_PED_IN_ANY_VEHICLE(MocapPlayerPed[i], TRUE)
													IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fWalkToPositionRadius[iCamShot] < 1.0
														PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - ped: ", i, " Calling TASK_GO_TO_COORD_ANY_MEANS on clone ",i," to vWalkToPosition ",g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vWalkToPosition[iCamShot],", iCamShot ",iCamShot,", iCutsceneToUse ",iCutsceneToUse, " fPedMoveBlendRatio: ", fPedMoveBlendRatio)
														TASK_GO_TO_COORD_ANY_MEANS(MocapPlayerPed[i], g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vWalkToPosition[iCamShot], fPedMoveBlendRatio, NULL)
													ELSE
														PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - ped: ", i, " Calling TASK_GO_STRAIGHT_TO_COORD on clone ",i," to vWalkToPosition ",g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vWalkToPosition[iCamShot],", iCamShot ",iCamShot,", iCutsceneToUse ",iCutsceneToUse, " fPedMoveBlendRatio: ", fPedMoveBlendRatio, "fRadius = ", g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fWalkToPositionRadius[iCamShot])
														TASK_GO_STRAIGHT_TO_COORD(MocapPlayerPed[i], g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vWalkToPosition[iCamShot], fPedMoveBlendRatio, DEFAULT, DEFAULT, g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fWalkToPositionRadius[iCamShot])
													ENDIF												
												ELSE
													IF GET_SCRIPT_TASK_STATUS(MocapPlayerPed[i], SCRIPT_TASK_LEAVE_VEHICLE) != WAITING_TO_START_TASK
													AND GET_SCRIPT_TASK_STATUS(MocapPlayerPed[i], SCRIPT_TASK_LEAVE_VEHICLE) != PERFORMING_TASK
														PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - ped: ", i, " Calling TASK_LEAVE_ANY_VEHICLE on clone player ",g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vWalkToPosition[iCamShot],", iCamShot ",iCamShot,", iCutsceneToUse ",iCutsceneToUse)
														TASK_LEAVE_ANY_VEHICLE(MocapPlayerPed[i], 0, ECF_DONT_DEFAULT_WARP_IF_DOOR_BLOCKED)
													ELSE
														PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - ped: ", i, " Waiting for clone player to have left the vehicle ",g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vWalkToPosition[iCamShot],", iCamShot ",iCamShot,", iCutsceneToUse ",iCutsceneToUse)
													ENDIF
												ENDIF
											ELSE
												PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - ped: ", i, " Preventing call to TASK_GO_STRAIGHT_TO_COORD - We are performing a player specific animation.")
											ENDIF
											
										ENDIF
									ENDFOR
								ENDIF
								
								iCutsceneID_GotoShot = iCamShot
							ENDIF
							
						ELSE
							IF iCutsceneID_GotoShot != -1
								PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - Call CLEAR_PED_TASKS & set iCutsceneID_GotoShot back to -1, iCamShot ",iCamShot,", iCutsceneToUse ",iCutsceneToUse)
								
								IF NOT bUseClones
									IF NOT IS_PED_INJURED(LocalPlayerPed)
									AND NOT IS_BIT_SET(iLocalBoolCheck32, LBOOL32_SCRIPTED_CUTSCENE_PLAYING_ANIM)
										CLEAR_PED_TASKS(LocalPlayerPed)
									ENDIF
								ELSE
									FOR i = 0 to (FMMC_MAX_CUTSCENE_PLAYERS - 1)
										IF DOES_ENTITY_EXIST(MocapPlayerPed[i])
										AND NOT IS_PED_INJURED(MocapPlayerPed[i])
											IF NOT IS_BIT_SET(iLocalBoolCheck32, LBOOL32_SCRIPTED_CUTSCENE_PLAYING_ANIM)
											OR MocapPlayerPed[i] != LocalPlayerPed
												CLEAR_PED_TASKS(MocapPlayerPed[i])
											ENDIF
										ENDIF
									ENDFOR
								ENDIF
								
								iCutsceneID_GotoShot = -1
							ENDIF
						ENDIF
					ENDIF

					PROCESS_SCRIPTED_CUTSENE_LIFT_MOVING(iCutsceneToUse, iCamShot, iPlayerIndex)
					
				ELSE
					IF IS_BIT_SET(iCamSoundBitSet,1)
						PLAY_SOUND_FRONTEND(iCamSound,"Change_Cam","MP_CCTV_SOUNDSET", FALSE)
						SET_BIT(iCamSoundBitSet,4)
						iLocalCamSoundTimer = GET_GAME_TIMER()
						PRINTLN("[MANAGE_CAM_AUDIO] PROCESS_SCRIPTED_CUTSCENE - PLAYING FINAL CHANGE CAM")
					ENDIF
					
					LOAD_RANDOM_RE_ENTRY_INTERACTION_ANIM() // neilf. so the re-entry anims will be loaded by the time gameplay resumes.
					
					PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE: Final shot has finished, progressing cutscene.")
					//SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_WARPEND_SHORT)
					bProgress = TRUE
				ENDIF
				
				IF iCamShot = ( iNumCamShots - 1 )
					IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdScriptedCutsceneTimer) > (g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iInterpTime[iCamShot] - 450)
						IF MC_serverBD_4.iCurrentHighestPriority[mc_playerBD[iPartToUse].iteam] = MC_serverBD.iMaxObjectives[mc_playerBD[iPartToUse].iteam]
							IF NOT Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer) 
							AND NOT Is_Player_Currently_On_MP_Heist(LocalPlayer)
							AND NOT GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
							AND NOT CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
								IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
								AND CHECK_TO_SEE_IF_THIS_IS_THE_LAST_STRAND_MISSION()
									IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration)
										DO_RENDER_PHASE_PAUSE_FOR_CELEBRATION_SCREEN()
										SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration)
										PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - [NETCELEBRATION] - [SAC] - ciCELEBRATION_BIT_SET_TriggerEndCelebration - call 2.")
									ENDIF
								ENDIF
							ELSE
								IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
									IF NOT SHOULD_PLAY_MOCAP()
									AND NOT (MC_serverBD.iNextMission < FMMC_MAX_STRAND_MISSIONS
									AND MC_serverBD.iNextMission >= 0
									AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl23NextContentID[MC_serverBD.iNextMission])
									AND g_FMMC_STRUCT.eBranchingTransitionType[MC_serverBD.iNextMission] != FMMC_BRANCHING_MISSION_TRANSITION_TYPE_MOCAP)
										IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerHeistwinnerCutscene)
											TRIGGER_END_HEIST_WINNER_CELEBRATION((NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoFacePlayerCamCut))
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_FadeForTransitionAtEnd)
					AND g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iFadeBeforeEndTime > 0
						IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdScriptedCutsceneTimer) > (g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iInterpTime[iCamShot] - (g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iFadeBeforeEndTime * 1000))
							IF NOT IS_SCREEN_FADED_OUT()
								IF NOT IS_SCREEN_FADING_OUT()
									PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - iCutsceneToUse: ", iCutsceneToUse, " Set to end faded out - fading (early).")
									DO_SCREEN_FADE_OUT(SCRIPTED_CUTSCENE_END_FADE_DURATION)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE

				//populate rotation
				IF IS_VECTOR_ZERO(vCCTVRot[iCamShot])
					IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vCamStartRot[iCamShot].z > 0
						vCCTVRot[iCamShot] = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vCamStartRot[iCamShot]
					ELSE
						g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vCamStartRot[iCamShot].z = 360 - g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vCamStartRot[iCamShot].z
						PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE MANUAL_CCTV CONTROL - Capping rotation value.")
					ENDIF
				ENDIF
				
				//populate FOV
				IF fCCTVFOV[iCamShot] = 0
					fCCTVFOV[iCamShot] = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fStartCamFOV[iCamShot]
				ENDIF
				
				UPDATE_CAM_PARAMS_FOR_CCTV(iCutsceneToUse,iCamShot)
				
				//Manage manual control of shots
				IF iSpectatorTarget = -1
					MANAGE_MANUAL_CCTV_CONTROL(iCutsceneToUse,iCamShot)
				ENDIF
				
				IF WVM_FLOW_IS_CURRENT_MISSION_WVM_MISSION()
					SET_SCRIPTED_CONVERSION_COORD_THIS_FRAME(g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].vStreamStartPos[ iCamShot ])
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciDISABLE_CINEMATIC_TOGGLE_CUTSCENE)
					PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE Option ciDISABLE_CINEMATIC_TOGGLE_CUTSCENE is enabled, Disabling cinematic button")
					SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
				ENDIF
				
				PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE MANUAL_CCTV CONTROL - CUTSCENE WILL RUN FOR: ", g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iInterpTime[0]) 
				PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE MANUAL_CCTV CONTROL - iCamShot :  ", iCamShot) 
				
				//Change Cameras
				IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
				OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
					IF MANAGE_HACKING_TIMER(iCoolDownTimerForManualCCTV,500)
						
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
							IF iCamShot < iNumCamShots -1
								iCamShot++
							ELSE
								iCamShot = 0
							ENDIF
						ENDIF
						
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
							IF iCamShot > 0
								iCamShot --
							ELSE
								iCamShot = iNumCamShots -1
							ENDIF
						ENDIF
						
						PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE CHANGING CCTV SHOT")
						PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE CHANGING CCTV SHOT - iCamShot",iCamShot)
						PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE CHANGING CCTV SHOT - iNumCamShots",iNumCamShots)
						
						iCoolDownTimerForManualCCTV = GET_GAME_TIMER()
					ENDIF
									
					IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_SecurityCamFilter)
						g_FMMC_STRUCT.bSecurityCamActive = TRUE
						SET_TIMECYCLE_MODIFIER("CAMERA_secuirity")
						REQUEST_CCTV_ASSETS()
						IF HAVE_CCTV_ASSETS_LOADED()
							IF HAS_SCALEFORM_MOVIE_LOADED(SI_SecurityCam)
								SET_CCTV_LOCATION(g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].sCamName[iCamShot],g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].sLocation[iCamShot])
								PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE SETTING LOCATION DATA FOR THIS SHOT")
							ENDIF
						ENDIF
					ELSE
						g_FMMC_STRUCT.bSecurityCamActive = FALSE
						CLEAR_TIMECYCLE_MODIFIER()
					ENDIF
				ENDIF
				
				//Custom progression logic
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdScriptedCutsceneTimer) > g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iInterpTime[0]
					IF IS_BIT_SET(iCamSoundBitSet,1)
						PLAY_SOUND_FRONTEND(iCamSound,"Change_Cam","MP_CCTV_SOUNDSET", FALSE)
						SET_BIT(iCamSoundBitSet,4)
						iLocalCamSoundTimer = GET_GAME_TIMER()
						PRINTLN("[MANAGE_CAM_AUDIO] PROCESS_SCRIPTED_CUTSCENE - PLAYING FINAL CHANGE CAM")
					ENDIF
				
					LOAD_RANDOM_RE_ENTRY_INTERACTION_ANIM() // neilf. so the re-entry anims will be loaded by the time gameplay resumes.
					
					IF IS_NEW_LOAD_SCENE_ACTIVE()
						NEW_LOAD_SCENE_STOP()
					ENDIF
					
					IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
						NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
					ENDIF
					
					CLEAR_FOCUS()
					
					PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE Timer exceeded progressing.")
					//SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_WARPEND_SHORT)
					bProgress = TRUE
				ENDIF
				
				//______________________________________________________________________________________________________________
				
				VECTOR vStreamPos
				FLOAT fStreamRadius
				
				IF( IS_BIT_SET( g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].iCutsceneShotBitSet[ iCamShot + 1 ], ciCSS_BS_Attach_Enable ) )
					vStreamPos = GET_ATTACH_CAMERA_VEHICLE_POSITION( iCutsceneToUse, iCamShot + 1 )
				ELSE
					IF iCamShot < iNumCamShots -1
						vStreamPos = g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].vStreamStartPos[ iCamShot + 1 ]
					ELSE
						vStreamPos = g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].vStreamStartPos[0]
					ENDIF
				ENDIF
				
				IF iCamShot < iNumCamShots -1
					fStreamRadius = g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].fStreamRadius[ iCamShot + 1 ]
				ELSE
					fStreamRadius = g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].fStreamRadius[0]
				ENDIF
								
				//Pre load the next cam shot area
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdScriptedCutsceneTimer) > (g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iInterpTime[0] - 2000)
					IF iCamShot < iNumCamShots -1
						PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - calling PRE_LOAD_CAM_SHOT_AREA. (3)")
						PRE_LOAD_CAM_SHOT_AREA( iCamShot + 1, vStreamPos, fStreamRadius )
					ELSE
						PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - calling PRE_LOAD_CAM_SHOT_AREA. (4)")						
						PRE_LOAD_CAM_SHOT_AREA(0, vStreamPos, fStreamRadius )
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - calling PRE_LOAD_CAM_SHOT_AREA. (5)")
					PRE_LOAD_CAM_SHOT_AREA( iCamShot + 1, GET_ENTITY_COORDS( playerPedToUse ), 25.0 )
				ENDIF
				
				DO_PER_SHOT_PHONE_INTRO( iCutsceneToUse, iCamShot )
				
				CLEAR_BIT( iLocalBoolCheck8, LBOOL8_HUMANE_DELIVER_EMP_PER_SHOT_FLAG )
				IF MC_playerBD[iPartToUse].iteam = iScriptedCutsceneTeam
					IF NOT IS_BIT_SET( MC_serverBD.iObjectiveMidPointBitset[ MC_playerBD[iPartToUse].iteam  ], MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] )
						IF iCamShot >= g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iMidPoint
							BROADCAST_OBJECTIVE_MID_POINT(MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam],MC_playerBD[iPartToUse].iteam)
						ENDIF
					ENDIF
					IF MC_serverBD.iSpawnScene != iCutsceneToUse
					OR MC_serverBD.iSpawnShot != iCamShot
						IF NOT HAS_NET_TIMER_STARTED(tdCutsceneEventDelay)
						OR HAS_NET_TIMER_EXPIRED(tdCutsceneEventDelay, ciCUTSCENE_EVENT_DELAY)
							REINIT_NET_TIMER(tdCutsceneEventDelay)
							BROADCAST_CUTSCENE_SPAWN_PROGRESS(iCutsceneToUse,iCamShot)
						ENDIF
					ENDIF
				ENDIF
				
				IF iCamShot = ( iNumCamShots - 1 )
					IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdScriptedCutsceneTimer) > (g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iInterpTime[iCamShot] - 450)
						IF MC_serverBD_4.iCurrentHighestPriority[mc_playerBD[iPartToUse].iteam] = MC_serverBD.iMaxObjectives[mc_playerBD[iPartToUse].iteam]
							IF NOT Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer) 
							AND NOT Is_Player_Currently_On_MP_Heist(LocalPlayer)
							AND NOT GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
							AND NOT CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
								IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
									IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration)
										DO_RENDER_PHASE_PAUSE_FOR_CELEBRATION_SCREEN()
										SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration)
										PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - [NETCELEBRATION] - [SAC] - ciCELEBRATION_BIT_SET_TriggerEndCelebration - call 2.")
									ENDIF
								ENDIF
							ELSE
								IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
									IF NOT SHOULD_PLAY_MOCAP()
										IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerHeistwinnerCutscene)
											TRIGGER_END_HEIST_WINNER_CELEBRATION((NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoFacePlayerCamCut))
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					
					IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_FadeForTransitionAtEnd)
					AND g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iFadeBeforeEndTime > 0
						IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdScriptedCutsceneTimer) > (g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iInterpTime[iCamShot] - (g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iFadeBeforeEndTime * 1000))
							IF NOT IS_SCREEN_FADED_OUT()
								IF NOT IS_SCREEN_FADING_OUT()
									PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - iCutsceneToUse: ", iCutsceneToUse, " Set to end faded out - fading (early).")
									DO_SCREEN_FADE_OUT(SCRIPTED_CUTSCENE_END_FADE_DURATION)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
			
			IF bProgress
				
				IF bIsLocalPlayerHost
					IF MC_ServerBD_1.iScriptedCutsceneSyncScene > -1
						NETWORK_STOP_SYNCHRONISED_SCENE(MC_ServerBD_1.iScriptedCutsceneSyncScene)
						MC_ServerBD_1.iScriptedCutsceneSyncScene = -1
						PRINTLN("[LM][RCC MISSION][ScriptedAnimSyncScene] - PROCESS_SCRIPTED_CUTSCENE - Cleaned up sync scene as we need to ensure it's safe to warp...")
					ENDIF
				ENDIF	
				
				IF MC_ServerBD_1.iScriptedCutsceneSyncScene > -1
					PRINTLN("[LM][RCC MISSION][ScriptedAnimSyncScene] - PROCESS_SCRIPTED_CUTSCENE - Waiting for Sync Scene to be finished.")
					EXIT
				ENDIF
				
				//Unload animations:
				IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iAnimation != 0
					REQUEST_RELEASE_SCRIPTED_CUTSCENE_CANNED_SCENE(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iAnimation, FALSE)
				ENDIF
								
				IF IS_BIT_SET(iCamSoundBitSet,4)
					IF MANAGE_THIS_MINIGAME_TIMER(iLocalCamSoundTimer,100)
						STOP_SOUND(iCamSound)
						PRINTLN("[MANAGE_CAM_AUDIO] PROCESS_SCRIPTED_CUTSCENE STOPPING CAM CHANGE SOUDN")
						CLEAR_BIT(iCamSoundBitSet,4)
					ENDIF
				ENDIF
				
				//Do this early if the mission is going to end
				CACHE_VEHICLE_SEAT_FOR_CONTINUITY()
				
				VEHICLE_INDEX vehDummy
				GET_SCRIPTED_CUTSCENE_END_WARP_TARGET(iCutsceneToUse, iPlayerIndex, vEndPos, fEndPos, vehDummy)

				IF NOT IS_VECTOR_ZERO(vEndPos)
				AND g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iVehicleIndexForPosWarp[0] = -1	
				AND NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration)
				AND (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed, vEndPos) > 100 OR DOES_THIS_CUTSCENE_WARP_INTO_AN_INTERIOR(iCutsceneToUse, GET_ENTITY_COORDS(LocalPlayerPed), vEndPos))
					
					SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_WARPEND_LONG)
					
				ELSE
				
					SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_WARPEND_SHORT)
				
				ENDIF
				
			ENDIF

		BREAK
		
		CASE SCRIPTEDCUTPROG_WARPEND_SHORT
			
			IF PROCESS_SCRIPTED_CUTSCENE_END_WARP(iCutsceneToUse, iPlayerIndex, vEndPos, fEndPos, TRUE)
				
				IF eCSLift != eCSLift_None
				AND IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset2, ci_CSBS2_Scripted_UsePlayerClones)
				AND g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].fLiftZOffset_End = 0
					// The player is going to be inside the lift here, we need to call this a frame before turning off the scripted cam to make sure the gameplay cam has worked out any collision with the lift body
					PRINTLN("[RCC MISSION][CSLift] PROCESS_SCRIPTED_CUTSCENE - Call SET_GAMEPLAY_CAM_RELATIVE_HEADING early to sort lift cam collision")
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				ENDIF
				
				SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_ENDING_PROCEDURES)
			ENDIF
			
		BREAK
		
		CASE SCRIPTEDCUTPROG_WARPEND_LONG
		
			IF NOT IS_SCREEN_FADED_OUT()
			AND NOT IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_OUT(SCRIPTED_CUTSCENE_END_FADE_DURATION)
			ELSE
				IF IS_SCREEN_FADED_OUT()
					
					IF PROCESS_SCRIPTED_CUTSCENE_END_WARP(iCutsceneToUse, iPlayerIndex, vEndPos, fEndPos, FALSE)
						SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_ENDING_PROCEDURES)						
					ENDIF
	
				ENDIF
			ENDIF						
		BREAK
		
		CASE SCRIPTEDCUTPROG_ENDING_PROCEDURES
			IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset2, ci_CSBS2_EndCutsceneInCover)
				PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - iCutsceneToUse: ", iCutsceneToUse, " Set to end cutscene in cover.")
				
				IF NOT IS_BIT_SET(iCutsceneBSEnteredCover, iCutsceneToUse)
					PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - iCutsceneToUse: ", iCutsceneToUse, " Setting player to enter cover NOW!")
					
					COVERPOINT_INDEX cpIndex
					cpIndex = GET_CLOSEST_COVER_POINT_TO_LOCATION(GET_ENTITY_COORDS(LocalPlayerPed))	
					SET_PED_TO_LOAD_COVER(LocalPlayerPed, TRUE)
					TASK_PUT_PED_DIRECTLY_INTO_COVER(LocalPlayerPed, GET_ENTITY_COORDS(LocalPlayerPed), -1, TRUE, DEFAULT, TRUE, DEFAULT, cpIndex)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed, TRUE)
					SET_BIT(iCutsceneBSEnteredCover, iCutsceneToUse)
					RESET_NET_TIMER(tdEnterCoverEndingCutsceneTimeOut)
					START_NET_TIMER(tdEnterCoverEndingCutsceneTimeOut)
				ENDIF
				IF NOT HAS_NET_TIMER_EXPIRED(tdEnterCoverEndingCutsceneTimeOut, 5000)
					IF IS_PED_GOING_INTO_COVER(LocalPlayerPed)
						PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - iCutsceneToUse: ", iCutsceneToUse, " Entering Cover.")
						EXIT
					ENDIF					
					IF NOT IS_PED_IN_COVER(LocalPlayerPed)
						PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - iCutsceneToUse: ", iCutsceneToUse, " Not in Cover....")
						EXIT
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - iCutsceneToUse: ", iCutsceneToUse, " Timed out let's just progress from this...")
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_UseDamagedCasinoDoorsIPL)
				PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - ci_CSBS2_UseDamagedCasinoDoorsIPL is SET, calling TOGGLE_HIDE_CASINO_FRONT_DOOR_IPL(TRUE)")
				TOGGLE_HIDE_CASINO_FRONT_DOOR_IPL(TRUE)
			ENDIF
			
			IF IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_UseDamagedCasinoVaultDoor)
				PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - ci_CSBS2_UseDamagedCasinoVaultDoor is SET, calling TOGGLE_HIDE_CASINO_FRONT_DOOR_IPL(TRUE)") // set local bit
				SET_BIT(iLocalBoolCheck31, LBOOL31_READY_TO_USE_DAMAGED_VAULT_DOOR)
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_EndCutsceneRappelTask)
				SET_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_RUNNING_WALL_RAPPEL_NOT_ON_RAPPEL_RULE)
				PRINTLN("[WallRappel] PROCESS_SCRIPTED_CUTSCENE - Setting PBBOOL4_RUNNING_WALL_RAPPEL_NOT_ON_RAPPEL_RULE")
			ENDIF
			
			IF IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_FadeForTransitionAtEnd)
				IF NOT IS_SCREEN_FADED_OUT()
					IF NOT IS_SCREEN_FADING_OUT()
						PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - iCutsceneToUse: ", iCutsceneToUse, " Set to end faded out - fading.")
						DO_SCREEN_FADE_OUT(SCRIPTED_CUTSCENE_END_FADE_DURATION)
					ENDIF
					EXIT
				ENDIF
				PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - iCutsceneToUse: ", iCutsceneToUse, " Set to end faded out - done.")
			ENDIF
			
			IF IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_SyncAllPlayersAtEnd)
				IF NOT IS_BIT_SET(MC_playerBD[iLocaLPart].iClientBitSet4, PBBOOL4_SYNC_PLAYERS_FOR_END_OF_CUTSCENE)
					PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - setting PBBOOL4_SYNC_PLAYERS_FOR_END_OF_CUTSCENE")
					SET_BIT(MC_playerBD[iLocaLPart].iClientBitSet4, PBBOOL4_SYNC_PLAYERS_FOR_END_OF_CUTSCENE)
				ENDIF
				
				IF NOT ARE_ALL_PLAYERS_SYNCED_FOR_END_OF_CUTSCENE()
					PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - iCutsceneToUse: ", iCutsceneToUse, ", exiting syncing players")
					EXIT
				ENDIF
			ENDIF
			
			SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(SCRIPTEDCUTPROG_CLEANUP)
		BREAK
		
		CASE SCRIPTEDCUTPROG_CLEANUP
			BOOL bReadytoCleanup
			bReadytoCleanup = TRUE
			
			IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
				IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_PLAYING_CUTSCENE)
					PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - iPartToUse: ", iPartToUse, " still watching cutscene, waiting before cleanup (PBBOOL_PLAYING_CUTSCENE)")
					bReadytoCleanup = FALSE
				ENDIF
			ENDIF
			
			IF bReadyToCleanup
				g_bInMissionControllerCutscene = FALSE
				
				iScriptedEntitiesRegsitered = 0
				
				TOGGLE_PLAYER_DAMAGE_OVERLAY(TRUE)
				SET_PLAYER_INVINCIBLE(LocalPlayer, FALSE)
				
				g_FMMC_STRUCT.bDisablePhoneInstructions = FALSE
				
				IF NETWORK_IS_IN_MP_CUTSCENE()
					SET_NETWORK_CUTSCENE_ENTITIES( FALSE )
				ENDIF
				
				IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].sPlayerStartWarp[iPlayerIndex].iVehID > -1
					HANDLE_PLAYER_CUTSCENE_WARP_BETWEEN_AVENGER_HOLDS(
						iScriptedCutsceneTeam, 
						g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].sPlayerStartWarp[iPlayerIndex].iVehID, 
						g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].sPlayerStartWarp[iPlayerIndex].iVehSeat)
				ELIF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].sPlayerEndWarp[iPlayerIndex].iVehID > -1
					HANDLE_PLAYER_CUTSCENE_WARP_BETWEEN_AVENGER_HOLDS(
						iScriptedCutsceneTeam, 
						g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].sPlayerEndWarp[iPlayerIndex].iVehID, 
						g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].sPlayerEndWarp[iPlayerIndex].iVehSeat)
				ENDIF
				
				SET_BIT( MC_playerBD[iLocalPart].iCutsceneFinishedBitset, g_iFMMCScriptedCutscenePlaying)
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE Setting cutscene as finished: ", g_iFMMCScriptedCutscenePlaying)
				
				CLEAR_BIT( MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYING_CUTSCENE )
				CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_SYNC_PLAYERS_FOR_END_OF_CUTSCENE)
				
				IF IS_THIS_ROCKSTAR_MISSION_SVM_DUNE4(g_FMMC_STRUCT.iRootContentIDHash)
					IF IS_AUDIO_SCENE_ACTIVE("DLC_IE_SVM_dune4_Bank_Cutscene_Scene")
						STOP_AUDIO_SCENE("DLC_IE_SVM_dune4_Bank_Cutscene_Scene")
						PRINTLN("[AUDIO_SCENE_CUTSCENE] PROCESS_SCRIPTED_CUTSCENE - Stopping DLC_IE_SVM_dune4_Bank_Cutscene_Scene")
					ENDIF
				ENDIF
				RESET_NET_TIMER(tdEnterCoverEndingCutsceneTimeOut)
				TURN_ON_CUTSCENE_VEHICLE_RADIO()
				
				IF IS_BIT_SET(iLocalBoolCheck7, LBOOL7_SECURITY_CAM_FILTER_USED)
					REFRESH_SPEC_HUD_FILTER()
					CLEAR_BIT(iLocalBoolCheck7, LBOOL7_SECURITY_CAM_FILTER_USED)
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Hide_Other_Players)
					if ARE_STRINGS_EQUAL(sMocapCutscene, "mph_pri_sta_mcs1") //prison - station / casco
					OR ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PAC_WIT_MCS1") //Pacific Standard Boat Cutscene
						CONCEAL_ALL_OTHER_PLAYERS(FALSE, true)
					else 
						CONCEAL_ALL_OTHER_PLAYERS(FALSE)	
					endif
				ELIF bUseClones
					FOR i = 0 to (FMMC_MAX_CUTSCENE_PLAYERS - 1)
						IF IS_NET_PLAYER_OK(MC_serverBD.piCutscenePlayerIDs[iteam][i])
							SET_ENTITY_LOCALLY_VISIBLE(GET_PLAYER_PED(MC_serverBD.piCutscenePlayerIDs[iteam][i]))
						ENDIF
					ENDFOR
					CLEAR_BIT(iLocalBoolCheck27, LBOOL27_SCRIPTED_CUTSCENE_LOCALLY_HIDE_PLAYERS)
					CLEANUP_MOCAP_PLAYER_CLONES()
				ENDIF
				
				RESET_NET_TIMER(tdScriptedCutsceneSyncSceneSafetyTimer)
				RESET_NET_TIMER(tdScriptedCutsceneSyncSceneSafetyTimer2)
				
				IF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_PLAYERS_CONCEALED_DUE_TO_BANK_VAULT_SHOT)
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - Unconcealing due to LBOOL28_PLAYERS_CONCEALED_DUE_TO_BANK_VAULT_SHOT")
					CLEAR_BIT(iLocalBoolCheck28, LBOOL28_PLAYERS_CONCEALED_DUE_TO_BANK_VAULT_SHOT)
					CONCEAL_ALL_OTHER_PLAYERS(FALSE, TRUE, TRUE)	
				ENDIF
				
				CLEAR_BIT(iLocalBoolCheck5,LBOOL5_PED_INVOLVED_IN_CUTSCENE)
				CLEAR_BIT(iLocalBoolCheck32, LBOOL32_SCRIPTED_CUTSCENE_PLAYING_ANIM)
				
				CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_READY_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE)
				
				//Phone Intro
				CLEAR_BIT(iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_START_PHONE_INTRO)
				CLEAR_BIT(iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_READY_FOR_PHONE_INTRO)
											
				//Focus Intro				
				CLEAR_BIT(iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_START_FOCUS_INTRO)
				CLEAR_BIT(iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_FOCUS_INTRO)
				CLEAR_BIT(iLocalBoolCheck7, LBOOL7_SCRIPTED_CUT_INIT_FOCUS_INTRO)
				CLEAR_BIT( iLocalBoolCheck8, LBOOL8_HUMANE_DELIVER_EMP_PER_SHOT_FLAG )
				SET_BIT( MC_playerBD[ iLocalPart].iClientBitSet2, PBBOOL2_FINISHED_CUTSCENE )
				CLEAR_BIT( MC_playerBD[ iLocalPart ].iClientBitSet2, PBBOOL2_IN_MID_MISSION_CUTSCENE )
				CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_REQUEST_CUTSCENE_PLAYERS)
				
				// This will hide the HUD after a cutscene until godtext has been shown again
				CLEAR_BIT( iLocalBoolCheck9, LBOOL9_HAS_FIRST_OBJECTIVE_TEXT_BEEN_SHOWN )
				CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_REACHED_CUT_PROCESS_SHOTS)
				//Stop any active load scenes
				IF IS_NEW_LOAD_SCENE_ACTIVE()
					PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - Calling NEW_LOAD_SCENE_STOP")
					NEW_LOAD_SCENE_STOP()				
				ENDIF
				
				RESET_NET_TIMER(tdCutsceneWaitForEntitiesTimer)
				
				//Pre load cutscene shots
				iCamShotLoaded = -1
				CLEAR_BIT(iLocalBoolCheck7, LBOOL7_CURRENT_SCRIPTED_CAM_AREA_LOADED)
				
				iPhoneIntroDoneForShot = -1
				CLEAR_IM_WATCHING_A_MOCAP()

				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				
				iScriptedCutsceneSyncSceneBS = 0
				
				IF bIsLocalPlayerHost
					IF MC_ServerBD_1.iScriptedCutsceneSyncScene > -1
						NETWORK_STOP_SYNCHRONISED_SCENE(MC_ServerBD_1.iScriptedCutsceneSyncScene)
						MC_ServerBD_1.iScriptedCutsceneSyncScene = -1					
					ENDIF
				ENDIF
				
				NETWORK_ALLOW_REMOTE_SYNCED_SCENE_LOCAL_PLAYER_REQUESTS(FALSE)
				
				//CLEANUP_CAM_AUDIO()
				
				CLEANUP_SCRIPTED_CUTSCENE(FALSE, 
							IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_InterpBackToGameplay),
							NOT IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_FadeForTransitionAtEnd))
				
				CLEAR_BIT(iLocalBoolCheck7, LBOOL7_SHOULD_CUTSCENE_DISABLE_PHONE_THIS_FRAME)
				PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - Cleared iLocalBoolCheck7 bit: LBOOL7_SHOULD_CUTSCENE_DISABLE_PHONE_THIS_FRAME")
				
				MP_FORCE_TERMINATE_INTERNET_CLEAR()
				PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE - Calling MP_FORCE_TERMINATE_INTERNET_CLEAR()")

				// url:bugstar:2198931
				DISABLE_SPECTATOR_FILTER_OPTION(FALSE)
				
				//gdisablerankupmessage = FALSE
				SET_DISABLE_RANK_UP_MESSAGE(FALSE)
				
				IF IS_THIS_ROCKSTAR_MISSION_WVM_HALFTRACK(g_FMMC_STRUCT.iRootContentIDHash)
					RESET_HUD_COMPONENT_VALUES(NEW_HUD_SUBTITLE_TEXT)
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciDISABLE_CINEMATIC_TOGGLE_CUTSCENE)
					PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE Option ciDISABLE_CINEMATIC_TOGGLE_CUTSCENE is enabled, Re-enabling cinematic button")
					SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Lock_Player_Vehicle)
					IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)) 
							SET_VEHICLE_DOORS_LOCKED(GET_VEHICLE_PED_IS_IN(LocalPlayerPed), VEHICLELOCK_UNLOCKED)
							PRINTLN("[RCC MISSION] PROCESS_SCRIPTED_CUTSCENE UNLOCKING VEHICLE FOR CUTSCENE")
						ENDIF
					ENDIF
				ENDIF
				
				SCRIPTED_CUTSCENE_END_WARP_LOCAL sEmpty
				sScriptedCutsceneEndWarp = sEmpty
			ENDIF
		BREAK
		
	ENDSWITCH

ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SECTION BELOW: ANIMATIONS FOR FLEECA SCOPE OUT CUTSCENE !
//
//************************************************************************************************************************************************************

/// PURPOSE:
///    Handles the player ped anims for Fleeca - Scope Out mid mission cutscene
PROC MANAGE_PLAYER_CUTSCENE_ANIM_TASK()
	
	// HANDLE INTRO ANIMS
	IF NOT IS_BIT_SET(iLocalBoolCheck11, LBOOL11_FLEECA_SCOPEOUT_CUTSCENE_INTRO_ANIM_TASKED)
		IF g_iFMMCScriptedCutscenePlaying > -1
		AND SHOULD_PARTICIPANT_LOOK_AT_CELLPHONE()
			REQUEST_ANIM_DICT("ANIM@HEISTS@FLEECA_BANK@SCOPE_OUT@LESTER_IN_CAR")
			
			IF HAS_ANIM_DICT_LOADED("ANIM@HEISTS@FLEECA_BANK@SCOPE_OUT@LESTER_IN_CAR")
				IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				AND NOT IS_PED_INJURED(LocalPlayerPed)
					// HANDLE DRIVER PLAYER PED INTRO ANIMS
					IF GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(LocalPlayerPed, TRUE), VS_DRIVER) = LocalPlayerPed
						PRINTLN("[RCC MISSION] MANAGE_PLAYER_CUTSCENE_ANIM_TASK - ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(LocalPlayerPed)), ": in Driver Seat")
						
						CLEAR_SEQUENCE_TASK(temp_sequence)
						OPEN_SEQUENCE_TASK(temp_sequence)
							TASK_PLAY_ANIM(NULL, "ANIM@HEISTS@FLEECA_BANK@SCOPE_OUT@LESTER_IN_CAR", "DRIVER_INTRO", NORMAL_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY | AF_NOT_INTERRUPTABLE | AF_HOLD_LAST_FRAME)
						CLOSE_SEQUENCE_TASK(temp_sequence)
						
						TASK_PERFORM_SEQUENCE(LocalPlayerPed, temp_sequence)
						PRINTLN("[RCC MISSION] MANAGE_PLAYER_CUTSCENE_ANIM_TASK - ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(LocalPlayerPed)), ": Tasked with Driver Intro Anim")
						
						SET_BIT(iLocalBoolCheck11, LBOOL11_FLEECA_SCOPEOUT_CUTSCENE_INTRO_ANIM_TASKED)
					ENDIF
					
					// HANDLE PASSENGER PLAYER PED INTRO ANIMS
					IF GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(LocalPlayerPed, TRUE), VS_FRONT_RIGHT) = LocalPlayerPed
						PRINTLN("[RCC MISSION] MANAGE_PLAYER_CUTSCENE_ANIM_TASK - ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(LocalPlayerPed)), ": in Passenger Seat")
						
						CLEAR_SEQUENCE_TASK(temp_sequence)
						OPEN_SEQUENCE_TASK(temp_sequence)
							TASK_PLAY_ANIM(NULL, "ANIM@HEISTS@FLEECA_BANK@SCOPE_OUT@LESTER_IN_CAR", "PASSENGER_INTRO", NORMAL_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY | AF_NOT_INTERRUPTABLE | AF_HOLD_LAST_FRAME)
						CLOSE_SEQUENCE_TASK(temp_sequence)
						
						TASK_PERFORM_SEQUENCE(LocalPlayerPed, temp_sequence)
						PRINTLN("[RCC MISSION] MANAGE_PLAYER_CUTSCENE_ANIM_TASK - ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(LocalPlayerPed)), ": Tasked with Passenger Intro Anim")
						
						SET_BIT(iLocalBoolCheck11, LBOOL11_FLEECA_SCOPEOUT_CUTSCENE_INTRO_ANIM_TASKED)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ELSE
		// HANDLE LOOP ANIMS- FAILSAFE: MAY NEVER GET USED AS INTRO ANIMS SEEM TO LAST THROUGH THE WHOLE CUTSCENE
		IF NOT IS_BIT_SET(iLocalBoolCheck11, LBOOL11_FLEECA_SCOPEOUT_CUTSCENE_OUTRO_ANIM_TASKED)
		AND NOT IS_BIT_SET(iLocalBoolCheck11, LBOOL11_FLEECA_SCOPEOUT_CUTSCENE_LOOP_ANIM_TASKED)
		AND g_iFMMCScriptedCutscenePlaying> -1
			// HANDLE DRIVER PLAYER PED LOOP ANIMS
			IF IS_ENTITY_PLAYING_ANIM(LocalPlayerPed, "ANIM@HEISTS@FLEECA_BANK@SCOPE_OUT@LESTER_IN_CAR", "DRIVER_INTRO")
			AND GET_ENTITY_ANIM_CURRENT_TIME(LocalPlayerPed, "ANIM@HEISTS@FLEECA_BANK@SCOPE_OUT@LESTER_IN_CAR", "DRIVER_INTRO") >= 0.99
				PRINTLN("[RCC MISSION] MANAGE_PLAYER_CUTSCENE_ANIM_TASK - ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(LocalPlayerPed)), " Driver Intro Anim Phase >= 0.99")
				
				CLEAR_SEQUENCE_TASK(temp_sequence)
				OPEN_SEQUENCE_TASK(temp_sequence)
					TASK_PLAY_ANIM(NULL, "ANIM@HEISTS@FLEECA_BANK@SCOPE_OUT@LESTER_IN_CAR", "DRIVER_LOOP", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY | AF_NOT_INTERRUPTABLE | AF_LOOPING | AF_HOLD_LAST_FRAME)
				CLOSE_SEQUENCE_TASK(temp_sequence)
				
				TASK_PERFORM_SEQUENCE(LocalPlayerPed, temp_sequence)
				PRINTLN("[RCC MISSION] MANAGE_PLAYER_CUTSCENE_ANIM_TASK - ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(LocalPlayerPed)), ": Tasked with Driver Loop Anim")
				
				SET_BIT(iLocalBoolCheck11, LBOOL11_FLEECA_SCOPEOUT_CUTSCENE_LOOP_ANIM_TASKED)
			ENDIF
			
			// HANDLE PASSENGER PLAYER PED LOOP ANIMS
			IF IS_ENTITY_PLAYING_ANIM(LocalPlayerPed, "ANIM@HEISTS@FLEECA_BANK@SCOPE_OUT@LESTER_IN_CAR", "PASSENGER_INTRO")
			AND GET_ENTITY_ANIM_CURRENT_TIME(LocalPlayerPed, "ANIM@HEISTS@FLEECA_BANK@SCOPE_OUT@LESTER_IN_CAR", "PASSENGER_INTRO") >= 0.99
				PRINTLN("[RCC MISSION] MANAGE_PLAYER_CUTSCENE_ANIM_TASK - ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(LocalPlayerPed)), " Passenger Intro Anim Phase >= 0.99")
				
				CLEAR_SEQUENCE_TASK(temp_sequence)
				OPEN_SEQUENCE_TASK(temp_sequence)
					TASK_PLAY_ANIM(NULL, "ANIM@HEISTS@FLEECA_BANK@SCOPE_OUT@LESTER_IN_CAR", "PASSENGER_LOOP", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY | AF_NOT_INTERRUPTABLE | AF_LOOPING | AF_HOLD_LAST_FRAME)
				CLOSE_SEQUENCE_TASK(temp_sequence)
				
				TASK_PERFORM_SEQUENCE(LocalPlayerPed, temp_sequence)
				PRINTLN("[RCC MISSION] MANAGE_PLAYER_CUTSCENE_ANIM_TASK - ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(LocalPlayerPed)), ": Tasked with Passenger Loop Anim")
				
				SET_BIT(iLocalBoolCheck11, LBOOL11_FLEECA_SCOPEOUT_CUTSCENE_LOOP_ANIM_TASKED)
			ENDIF
		ENDIF
	ENDIF
	
	// HANDLE OUTRO ANIMS
	IF NOT IS_BIT_SET(iLocalBoolCheck11, LBOOL11_FLEECA_SCOPEOUT_CUTSCENE_OUTRO_ANIM_TASKED)
	AND IS_BIT_SET(iLocalBoolCheck11, LBOOL11_FLEECA_SCOPEOUT_CUTSCENE_INTRO_ANIM_TASKED)
		
		PRINTLN("[RCC MISSION] MANAGE_PLAYER_CUTSCENE_ANIM_TASK - Waiting to clean up Fleeca Scopeout anims - g_iFMMCScriptedCutscenePlaying= ",g_iFMMCScriptedCutscenePlaying,", SHOULD_PARTICIPANT_STOP_LOOKING_AT_CELLPHONE = ",SHOULD_PARTICIPANT_STOP_LOOKING_AT_CELLPHONE())
		
		IF g_iFMMCScriptedCutscenePlaying < 0
		OR SHOULD_PARTICIPANT_STOP_LOOKING_AT_CELLPHONE()
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			AND NOT IS_PED_INJURED(LocalPlayerPed)
				// HANDLE DRIVER PLAYER PED OUTRO ANIMS
				IF GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(LocalPlayerPed, TRUE), VS_DRIVER) = LocalPlayerPed
					PRINTLN("[RCC MISSION] MANAGE_PLAYER_CUTSCENE_ANIM_TASK - ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(LocalPlayerPed)), ": in Driver Seat")
					
					CLEAR_SEQUENCE_TASK(temp_sequence)
					OPEN_SEQUENCE_TASK(temp_sequence)
						TASK_PLAY_ANIM(NULL, "ANIM@HEISTS@FLEECA_BANK@SCOPE_OUT@LESTER_IN_CAR", "DRIVER_OUTRO", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY)
					CLOSE_SEQUENCE_TASK(temp_sequence)
					
					TASK_PERFORM_SEQUENCE(LocalPlayerPed,temp_sequence)
					PRINTLN("[RCC MISSION] MANAGE_PLAYER_CUTSCENE_ANIM_TASK - ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(LocalPlayerPed)), ": Tasked with Driver Outro Anim")
					
					SET_BIT(iLocalBoolCheck11, LBOOL11_FLEECA_SCOPEOUT_CUTSCENE_OUTRO_ANIM_TASKED)
				ENDIF
				
				// HANDLE PASSENGER PLAYER PED OUTRO ANIMS
				IF GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(LocalPlayerPed, TRUE), VS_FRONT_RIGHT) = LocalPlayerPed
					PRINTLN("[RCC MISSION] MANAGE_PLAYER_CUTSCENE_ANIM_TASK - ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(LocalPlayerPed)), ": in Passenger Seat")
					
					CLEAR_SEQUENCE_TASK(temp_sequence)
					OPEN_SEQUENCE_TASK(temp_sequence)
						TASK_PLAY_ANIM(NULL, "ANIM@HEISTS@FLEECA_BANK@SCOPE_OUT@LESTER_IN_CAR", "PASSENGER_OUTRO", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY)
					CLOSE_SEQUENCE_TASK(temp_sequence)
					
					TASK_PERFORM_SEQUENCE(LocalPlayerPed,temp_sequence)
					PRINTLN("[RCC MISSION] MANAGE_PLAYER_CUTSCENE_ANIM_TASK - ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(LocalPlayerPed)), ": Tasked with Passenger Outro Anim")
					
					SET_BIT(iLocalBoolCheck11, LBOOL11_FLEECA_SCOPEOUT_CUTSCENE_OUTRO_ANIM_TASKED)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// REMOVES ANIM DICT LOADED BY PLAYER PEDS
	IF IS_BIT_SET(iLocalBoolCheck11, LBOOL11_FLEECA_SCOPEOUT_CUTSCENE_OUTRO_ANIM_TASKED)
	AND NOT IS_BIT_SET(iLocalBoolCheck11, LBOOL11_FLEECA_SCOPEOUT_CUTSCENE_ANIM_REMOVED)
	AND g_iFMMCScriptedCutscenePlaying< 0
		PRINTLN("[RCC MISSION] MANAGE_PLAYER_CUTSCENE_ANIM_TASK - ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(LocalPlayerPed)), ": Attempting ANIM@HEISTS@FLEECA_BANK@SCOPE_OUT@LESTER_IN_CAR Anim Dict Cleanup")
		
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			IF GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(LocalPlayerPed, TRUE), VS_DRIVER) = LocalPlayerPed
			OR GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(LocalPlayerPed, TRUE), VS_FRONT_RIGHT) = LocalPlayerPed
				IF HAS_ANIM_DICT_LOADED("ANIM@HEISTS@FLEECA_BANK@SCOPE_OUT@LESTER_IN_CAR")
				AND (NOT IS_ENTITY_PLAYING_ANIM(LocalPlayerPed, "ANIM@HEISTS@FLEECA_BANK@SCOPE_OUT@LESTER_IN_CAR", "DRIVER_OUTRO")
				AND NOT IS_ENTITY_PLAYING_ANIM(LocalPlayerPed, "ANIM@HEISTS@FLEECA_BANK@SCOPE_OUT@LESTER_IN_CAR", "PASSENGER_OUTRO"))
					REMOVE_ANIM_DICT("ANIM@HEISTS@FLEECA_BANK@SCOPE_OUT@LESTER_IN_CAR")
					PRINTLN("[RCC MISSION] MANAGE_PLAYER_CUTSCENE_ANIM_TASK - ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(LocalPlayerPed)), ": Removing ANIM@HEISTS@FLEECA_BANK@SCOPE_OUT@LESTER_IN_CAR Anim Dict")
					
					SET_BIT(iLocalBoolCheck11, LBOOL11_FLEECA_SCOPEOUT_CUTSCENE_ANIM_REMOVED)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: GENERIC CUTSCENE LOGIC (used for both mocap and scripted cutscenes) !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************



//RIP Joe Debug


PROC MANAGE_MID_MISSION_CUTSCENE()

	IF IS_THIS_A_CAPTURE()
	OR IS_THIS_A_LTS()
		EXIT
	ENDIF

	INT iCutsceneToUse
	FMMC_CUTSCENE_TYPE eCutType = FMMCCUT_SCRIPTED
	
	IF HAS_NET_TIMER_STARTED( stGracePeriodAfterCutscene )
	AND GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( stGracePeriodAfterCutscene ) < ciGRACE_PERIOD_AFTER_CUTSCENE
		STOP_LOCAL_PLAYER_FROM_GETTING_SHOT_THIS_FRAME()
	ENDIF
	
	IF g_iFMMCScriptedCutscenePlaying= -1
		iScriptedCutsceneTeam = GET_CUTSCENE_TEAM()
		IF iScriptedCutsceneTeam != -1
			PRINTLN("[RCC MISSION] -MANAGE_MID_MISSION_CUTSCENE iScriptedCutsceneTeam : ",iScriptedCutsceneTeam)
			#IF IS_DEBUG_BUILD
			IF HAS_NET_TIMER_STARTED(MC_serverBD.tdCutsceneStartTime[iScriptedCutsceneTeam])
				PRINTLN("[RCC MISSION] -MANAGE_MID_MISSION_CUTSCENE MC_serverBD.tdCutsceneStartTime on local player is (+1000) : ",(NATIVE_TO_INT(MC_serverBD.tdCutsceneStartTime[iScriptedCutsceneTeam].Timer)+1000) )
				PRINTLN("[RCC MISSION] -MANAGE_MID_MISSION_CUTSCENE cutscene GET_NETWORK_TIME_ACCURATE : ",NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()))
			ENDIF
			#ENDIF

			g_iFMMCScriptedCutscenePlaying= MC_serverBD.iCutsceneID[ iScriptedCutsceneTeam ]
			
			IF g_iFMMCScriptedCutscenePlaying!= -1
				
				TURN_OFF_CUTSCENE_VEHICLE_RADIO()
			
				PRINTLN("[RCC MISSION] MANAGE_MID_MISSION_CUTSCENE g_iFMMCScriptedCutscenePlaying= ", g_iFMMCScriptedCutscenePlaying," CURRENT NETWORK TIMER: ", NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()))
				PRINTLN("[RCC MISSION] MANAGE_MID_MISSION_CUTSCENE g_iFMMCScriptedCutscenePlaying= ", g_iFMMCScriptedCutscenePlaying," GET_NET_TIMER_DIFF", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_ServerBD.tdCutsceneStartTime[iScriptedCutsceneTeam],FALSE,TRUE),"[RCC MISSION] is host: ", bIsLocalPlayerHost)
				
				IF IS_BIT_SET( MC_playerBD[ iLocalPart ].iCutsceneFinishedBitset, g_iFMMCScriptedCutscenePlaying)
					STOP_LOCAL_PLAYER_FROM_GETTING_SHOT_THIS_FRAME()
					
					iScriptedCutsceneTeam = -1
					g_iFMMCScriptedCutscenePlaying= -1
					EXIT
				ENDIF
			ENDIF
		ENDIF
		
	ELSE
		IF g_iFMMCScriptedCutscenePlaying >= FMMC_GET_MAX_SCRIPTED_CUTSCENES()
		AND g_iFMMCScriptedCutscenePlaying < FMMC_GET_MAX_CUTSCENES_WITH_MOCAP()
			iCutsceneToUse = (g_iFMMCScriptedCutscenePlaying - FMMC_GET_MAX_SCRIPTED_CUTSCENES()) 
			IF NOT IS_STRING_NULL_OR_EMPTY( g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].tlName ) 
				IF IS_STRING_NULL_OR_EMPTY(sMocapCutscene)
					sMocapCutscene = Get_String_From_TextLabel(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].tlName)
					PRINTLN("[RCC MISSION] SETTING sMocapCutscene to '", sMocapCutscene, "' - was empty 2!")
				ENDIF
				eCutType = FMMCCUT_MOCAP 
			ELSE 
				PRINTLN("[RCC MISSION] CUTSCENE NAME IS EMPTY iCutsceneToUse : ", iCutsceneToUse)
	
			ENDIF 
		ELSE 
			iCutsceneToUse = g_iFMMCScriptedCutscenePlaying
		ENDIF
		
		IF (eCutType = FMMCCUT_MOCAP AND iCutsceneToUse >= MAX_MOCAP_CUTSCENES)
		OR (eCutType = FMMCCUT_SCRIPTED AND iCutsceneToUse >= FMMC_GET_MAX_SCRIPTED_CUTSCENES())
			PRINTLN("[RCC MISSION] CUTSCENE OUT OF BOUNDS EXITING BAD NEWS")
			EXIT
		ENDIF
	
		IF GET_SCRIPTED_CUTSCENE_STATE_PROGRESS() > SCRIPTEDCUTPROG_INIT
			INVALIDATE_CINEMATIC_VEHICLE_IDLE_MODE()
		ENDIF
		
		//Manage sounds
		IF iCamShot != -1
			IF GET_SCRIPTED_CUTSCENE_STATE_PROGRESS() < SCRIPTEDCUTPROG_CLEANUP
				IF iCutsceneToUse < FMMC_GET_MAX_SCRIPTED_CUTSCENES()
					IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_SecurityCamFilter) 
						MANAGE_CAM_AUDIO(iCutsceneToUse,iCamShot,IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].iCutsceneShotBitSet[iCamShot], ciCSS_BS_Allow_manual_control))
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF SHOULD_LOCAL_PLAYER_WATCH_TEAM_CUTSCENE(iScriptedCutsceneTeam, iCutsceneToUse, eCutType)
			
			//Render CCTV
			IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneShotBitSet[iCamShot], ciCSS_BS_SecurityCamFilter) 
				REQUEST_CCTV_ASSETS()
				DISABLE_SPECTATOR_FILTER_OPTION(TRUE)
				IF HAVE_CCTV_ASSETS_LOADED()
					IF DOES_CAM_EXIST(cutscenecam)	
						IF IS_CAM_ACTIVE(cutscenecam)
							IF NOT IS_SKYSWOOP_IN_SKY()
								RENDER_CCTV_OVERLAY()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET( MC_playerBD[ iLocalPart].iClientBitSet, PBBOOL_ANY_SPECTATOR ) 
				IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[ iCutsceneToUse ].iCutsceneShotBitSet[iCamShot], ciCSS_BS_Allow_manual_control)
					g_FMMC_STRUCT.bDisablePhoneInstructions = TRUE
					IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
						SETUP_MANUAL_CCTV_HELP()
					ENDIF
					PRINTLN("[RCC MISSION] MANAGE_MID_MISSION_CUTSCENE - SETTING g_FMMC_STRUCT.bDisablePhoneInstructions to TRUE")
					PRINTLN("[RCC MISSION] MANAGE_MID_MISSION_CUTSCENE - g_FMMC_STRUCT.bDisablePhoneInstructions =",g_FMMC_STRUCT.bDisablePhoneInstructions)
				ENDIF
			ENDIF

			//Disable pause menu on cutscenes
			//gdisablerankupmessage = TRUE
			SET_DISABLE_RANK_UP_MESSAGE(TRUE)
			
			DISABLE_FRONTEND_THIS_FRAME()
	
			STOP_IMPROMPTU_RACE_SETUP()
			CONTACT_REQUEST_MENU_DISABLE_THIS_FRAME()
			SET_BIT( iLocalBoolCheck7, LBOOL7_SHOULD_CUTSCENE_DISABLE_PHONE_THIS_FRAME )
			
			// 2064356
			SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(LocalPlayer)

			IF NOT IS_BIT_SET( MC_playerBD[iLocalPart].iCutsceneFinishedBitset, g_iFMMCScriptedCutscenePlaying)
				//Set flag to register cutscene has started for player
				SET_BIT( MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYING_CUTSCENE )
				
				CONTACT_REQUEST_MENU_DISABLE_THIS_FRAME()
				
				IF eCutType = FMMCCUT_MOCAP
					PROCESS_MOCAP_CUTSCENE(iCutsceneToUse, iScriptedCutsceneTeam, FMMCCUT_MOCAP)
				ELSE  
					PROCESS_SCRIPTED_CUTSCENE(iCutsceneToUse, iScriptedCutsceneTeam)
				ENDIF
			ELSE
				iScriptedCutsceneTeam = -1
				g_iFMMCScriptedCutscenePlaying= -1				
			ENDIF
			
			IF( IS_BIT_SET( iLocalBoolCheck7, LBOOL7_SHOULD_CUTSCENE_DISABLE_PHONE_THIS_FRAME ) )
				IF NOT MP_FORCE_TERMINATE_INTERNET_ACTIVE()
					MP_FORCE_TERMINATE_INTERNET()
					PRINTLN("[RCC MISSION] MANAGE_MID_MISSION_CUTSCENE - MP_FORCE_TERMINATE_INTERNET_ACTIVE() is FALSE - Calling MP_FORCE_TERMINATE_INTERNET()")
				ENDIF
				
				DISABLE_CELLPHONE_THIS_FRAME_ONLY(IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Hide_Phone_Instantly))

			ENDIF
			
		ELSE // Else of the SHOULD_LOCAL_PLAYER_WATCH_TEAM_CUTSCENE() check
			//This makes sure the wasted shard will be shown on death in this mission
			IF IS_THIS_ROCKSTAR_MISSION_WVM_TAMPA(g_FMMC_STRUCT.iRootContentIDHash)
				IF gdisablerankupmessage
					SET_DISABLE_RANK_UP_MESSAGE(FALSE)
				ENDIF
			ENDIF
			
			//Set flag to register cutscene has started for player
			iScriptedCutsceneTeam = -1
			g_iFMMCScriptedCutscenePlaying= -1
		ENDIF
	ENDIF
ENDPROC
