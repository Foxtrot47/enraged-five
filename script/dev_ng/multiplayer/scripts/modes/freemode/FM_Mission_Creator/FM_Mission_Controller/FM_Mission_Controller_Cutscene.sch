
USING "FM_Mission_Controller_USING.sch"
USING "FM_Mission_Controller_Utility.sch"
USING "FM_Mission_Controller_Entities.sch"
USING "FM_Mission_Controller_ObjectiveCompletion.sch"
USING "FM_Mission_Controller_SpecialCase.sch"
USING "FM_Mission_Controller_Rules.sch"
USING "FM_Mission_Controller_Doors.sch"

#IF IS_DEBUG_BUILD
	SCRIPT_TIMER stDebugOutputTimer

	PROC PRINT_DEBUG_APARTMENT_DROPOFF_FLAGS()
		IF IS_BIT_SET( iLocalBoolCheck7, LBOOL7_SHOULD_HOLD_UP_MOCAP_FOR_CUTSCENE )
			IF NOT HAS_NET_TIMER_STARTED( stDebugOutputTimer )
			OR GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( stDebugOutputTimer ) > 2000
				REINIT_NET_TIMER( stDebugOutputTimer )
				
				CPRINTLN( debug_controller, "[rcc mission] LBOOL10_CURRENTLY_ON_APARTMENT_GO_TO ", IS_BIT_SET( iLocalBoolCheck10, LBOOL10_CURRENTLY_ON_APARTMENT_GO_TO ) )
				CPRINTLN( debug_controller, "[rcc mission] g_bPropertyDropOff ", g_bPropertyDropOff )
				CPRINTLN( debug_controller, "[rcc mission] g_bGarageDropOff ", g_bGarageDropOff )
				CPRINTLN( debug_controller, "[rcc mission] IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY() ", IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY() )
				CPRINTLN( debug_controller, "[rcc mission] g_bShouldThisPlayerGetPulledIntoApartment ", g_bShouldThisPlayerGetPulledIntoApartment )
				CPRINTLN( debug_controller, "[rcc mission] g_HeistGarageDropoffPanComplete ", g_HeistGarageDropoffPanComplete )
				CPRINTLN( debug_controller, "[rcc mission] g_HeistApartmentReadyForDropOffCut ", g_HeistApartmentReadyForDropOffCut )
			
			ENDIF
		ENDIF
	ENDPROC
	
#ENDIF

FUNC STRING GET_FMMC_CUTSCENE_TYPE_NAME(FMMC_CUTSCENE_TYPE cutType)
	SWITCH cutType
		CASE FMMCCUT_MOCAP			RETURN "FMMCCUT_MOCAP"
		CASE FMMCCUT_ENDMOCAP		RETURN "FMMCCUT_ENDMOCAP"
		CASE FMMCCUT_SCRIPTED		RETURN "FMMCCUT_SCRIPTED"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

PROC RESET_CONOR_DROPOFF_GLOBALS( INT iID )
	IF g_HeistApartmentDropoffPanStarted
		g_HeistApartmentDropoffPanStarted = FALSE
	ENDIF
	
	IF g_HeistGarageDropoffSplashReady
		g_HeistGarageDropoffSplashReady = FALSE
	ENDIF
	
	IF g_HeistGarageDropoffPanStarted
		g_HeistGarageDropoffPanStarted = FALSE
	ENDIF
	
	IF g_HeistGarageDropoffPanComplete
		g_HeistGarageDropoffPanComplete = FALSE
		globalPropertyEntryData.bInteriorWarpDone = TRUE
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] globalPropertyEntryData.bInteriorWarpDone = TRUE" )
	ENDIF
	
	IF g_HeistApartmentReadyForDropOffCut
		g_HeistApartmentReadyForDropOffCut = FALSE
		globalPropertyEntryData.bInteriorWarpDone = TRUE
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] globalPropertyEntryData.bInteriorWarpDone = TRUE")
	ENDIF
	
	UNUSED_PARAMETER( iID )
	CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Resetting Conor's dropoff globals # ",iID )
ENDPROC

PROC RESET_DROPOFF_GLOBALS( INT iID )

	RESET_CONOR_DROPOFF_GLOBALS( iID ) // Resets the globals that Conor deals with
	
	// Everything else here is mission controller stuff
	g_bPropertyDropOff = FALSE
	g_bGarageDropOff = FALSE
	
	g_bShouldThisPlayerGetPulledIntoApartment = FALSE
	
	IF IS_BIT_SET( iLocalBoolCheck10, LBOOL10_CURRENTLY_ON_APARTMENT_GO_TO )
		CLEAR_BIT( iLocalBoolCheck10, LBOOL10_CURRENTLY_ON_APARTMENT_GO_TO )
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF bIsLocalPlayerHost
			mc_serverBD.dropOffAtApartmentPlayer = INVALID_PLAYER_INDEX()
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Resetting mc_serverBD.dropOffAtApartmentPlayer " )
		ENDIF
	ENDIF
	
	UNUSED_PARAMETER( iID )
	CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Resetting dropoff globals # ",iID )
	
ENDPROC
	
INT iNarMetDriver = -1

FUNC INT SERVER_CHOOSE_MOCAP_END_CUTSCENE(INT iCutsceneType,INT iDropoffEntityType)

INT iRandom = -1

	SWITCH iCutsceneType
	
		CASE ciMISSION_CUTSCENE_GERALD
		
			iRandom = 0
			
			IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL() 
				iRandom = ci_INTRO_CUTSCENE
			ELSE
				SWITCH iDropoffEntityType

					CASE ci_TARGET_OBJECT
						iRandom = GET_RANDOM_INT_IN_RANGE(0,4)
					BREAK	
					
				ENDSWITCH	
				IF iRandom = gGeraldLastMocap
					iRandom = -1
				ELSE
					gGeraldLastMocap =iRandom
				ENDIF
				
				printstring("iRandom =")
				printint(iRandom)
				printnl()
			ENDIF

		BREAK
		
		CASE ciMISSION_CUTSCENE_SIMEON
		
			iRandom = GET_RANDOM_INT_IN_RANGE(0,4)	
			
			IF iRandom = gSimeonLastMocap
				iRandom = -1
			ELSE
				gSimeonLastMocap = iRandom
			ENDIF
			//iRandom = 0
		BREAK
		
		CASE ciMISSION_CUTSCENE_MADRAZO
		
			iRandom = 0
			
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_MADRAZO_INTRO_CUTSCENE_PLAYED)
				iRandom = ci_INTRO_CUTSCENE
				SET_PACKED_STAT_BOOL(PACKED_MP_MADRAZO_INTRO_CUTSCENE_PLAYED,TRUE)
			ELSE			
				SWITCH iDropoffEntityType
				
					CASE ci_TARGET_LOCATION
						iRandom = 0
					BREAK
				
					CASE ci_TARGET_PED
						iRandom = GET_RANDOM_INT_IN_RANGE(0,2)
					BREAK
					
					CASE ci_TARGET_VEHICLE
						iRandom = GET_RANDOM_INT_IN_RANGE(0,2)
					BREAK
					
					CASE ci_TARGET_OBJECT
						iRandom = GET_RANDOM_INT_IN_RANGE(0,2)
					BREAK	
					
				ENDSWITCH
				
				IF iRandom = gMadrazoLastMocap
				AND iDropoffEntityType != ci_TARGET_LOCATION
					iRandom = -1
				ELSE
					gMadrazoLastMocap = iRandom
				ENDIF
			ENDIF
			
		BREAK
		
		CASE ciMISSION_CUTSCENE_TREVOR

			SWITCH iDropoffEntityType
			
				CASE ci_TARGET_PED
				CASE ci_TARGET_LOCATION
					iRandom = GET_RANDOM_INT_IN_RANGE(0,4)
				BREAK
				
				CASE ci_TARGET_VEHICLE
					iRandom = GET_RANDOM_INT_IN_RANGE(0,6)
				BREAK
				
				CASE ci_TARGET_OBJECT
					iRandom = GET_RANDOM_INT_IN_RANGE(0,6)
				BREAK	
				
			ENDSWITCH
			
			IF iRandom = gTrevorLastMocap
				iRandom = -1
			ELSE
				gTrevorLastMocap=iRandom
			ENDIF
		BREAK
		
		CASE ciMISSION_CUTSCENE_HEIST
		CASE ciMISSION_CUTSCENE_PLACE_HOLDER
		CASE ciMISSION_CUTSCENE_PRISON_PLANE_END
		CASE ciMISSION_CUTSCENE_ORNATE_CONVOY_END
		CASE ciMISSION_CUTSCENE_FLEECA_CROWD_FAIL
		CASE ciMISSION_CUTSCENE_HUM_ARM_EXT
		CASE ciMISSION_CUTSCENE_TUT_CAR_EXT
		CASE ciMISSION_CUTSCENE_NAR_FIN_EXT
		CASE ciMISSION_CUTSCENE_PRI_BUS_EXT
		CASE ciMISSION_CUTSCENE_PRI_PLA_EXT
		CASE ciMISSION_CUTSCENE_PAC_CON_EXT
		CASE ciMISSION_CUTSCENE_TUT_EXT
		CASE ciMISSION_CUTSCENE_HUM_EMP_EXT
		CASE ciMISSION_CUTSCENE_PAC_HAC_EXT
		CASE ciMISSION_CUTSCENE_PAC_BIK_EXT
		CASE ciMISSION_CUTSCENE_NAR_BIK_EXT
		CASE ciMISSION_CUTSCENE_NAR_WEE_EXT
		CASE ciMISSION_CUTSCENE_NAR_COK_EXT
		CASE ciMISSION_CUTSCENE_PAC_FIN_EXT
		CASE ciMISSION_CUTSCENE_PRI_STA_EXT
		CASE ciMISSION_CUTSCENE_HUM_FIN_MCS1
		CASE ciMISSION_CUTSCENE_HUM_FIN_EXT
		CASE ciMISSION_CUTSCENE_HUM_VAL_EXT
		CASE ciMISSION_CUTSCENE_NAR_MET_EXT
		CASE ciMISSION_CUTSCENE_HUM_KEY_EXT 
		CASE ciMISSION_CUTSCENE_PAC_WIT_MCS2
		CASE ciMISSION_CUTSCENE_PAC_PHO_EXT
		CASE ciMISSION_CUTSCENE_NAR_TRA_EXT 
		CASE ciMISSION_CUTSCENE_TUT_MID 
		CASE ciMISSION_CUTSCENE_PAC_FIN_MCS1
		CASE ciMISSION_CUTSCENE_HUM_KEY_MCS1
		CASE ciMISSION_CUTSCENE_PRI_UNF_EXT
		CASE ciMISSION_CUTSCENE_PRI_FIN_EXT
		CASE ciMISSION_CUTSCENE_HUM_DEL_EXT
		CASE ciMISSION_CUTSCENE_LOW_DRV_EXT
		CASE ciMISSION_CUTSCENE_LOW_FIN_EXT
		CASE ciMISSION_CUTSCENE_LOW_FIN_MCS1
		CASE ciMISSION_CUTSCENE_LOW_FUN_EXT
		CASE ciMISSION_CUTSCENE_LOW_FUN_MCS1
		CASE ciMISSION_CUTSCENE_LOW_PHO_EXT
		CASE ciMISSION_CUTSCENE_LOW_TRA_EXT
		CASE ciMISSION_CUTSCENE_SILJ_MCS1
		CASE ciMISSION_CUTSCENE_SILJ_EXT
		CASE ciMISSION_CUTSCENE_IAAJ_EXT
		CASE ciMISSION_CUTSCENE_SUBJ_MCS0
		CASE ciMISSION_CUTSCENE_SUBJ_EXT
		CASE ciMISSION_CUTSCENE_SILJ_INT
		CASE ciMISSION_CUTSCENE_CASINO_1_EXT
		CASE ciMISSION_CUTSCENE_CASINO_3_EXT
		CASE ciMISSION_CUTSCENE_CASINO_2_EXT
		CASE ciMISSION_CUTSCENE_CASINO_5_EXT
		CASE ciMISSION_CUTSCENE_CASINO_6_EXT
		CASE ciMISSION_CUTSCENE_CASINO_HEIST_DRP1
		CASE ciMISSION_CUTSCENE_CASINO_HEIST_DRP2
		CASE ciMISSION_CUTSCENE_CASINO_HEIST_DRP3
		CASE ciMISSION_CUTSCENE_CASINO_HEIST_ESC
		CASE ciMISSION_CUTSCENE_CASINO_HEIST_RP1
		CASE ciMISSION_CUTSCENE_CASINO_HEIST_ENT
		CASE ciMISSION_CUTSCENE_CASINO_HEIST_VLT
		CASE ciMISSION_CUTSCENE_CASINO_HEIST_SEW
		CASE ciMISSION_CUTSCENE_CASINO_HEIST_CEL
		CASE ciMISSION_CUTSCENE_CASINO_HEIST_ESC_B
			iRandom = 1
		BREAK
		
	ENDSWITCH

	RETURN iRandom

ENDFUNC

FUNC TEXT_LABEL_23 GET_CUTSCENE_MPH_PRI_FIN_MCS2_PREFIX_FROM_SEAT(VEHICLE_SEAT tempSeat)
	
	TEXT_LABEL_23 sreturn
	PRINTLN("[RCC MISSION] [PED CUT] GET_CUTSCENE_MPH_PRI_FIN_MCS2_PREFIX_FROM_SEAT tempSeat = ", ENUM_TO_INT(tempSeat))
	SWITCH tempSeat
	
		CASE VS_FRONT_RIGHT// 0
			sreturn = "MP_1"
			PRINTLN("[RCC MISSION] GET_CUTSCENE_MPH_PRI_FIN_MCS2_PREFIX_FROM_SEAT  1 ")
		BREAK

		CASE VS_EXTRA_RIGHT_1 //4
			sreturn = "MP_3"
			PRINTLN("[RCC MISSION] GET_CUTSCENE_MPH_PRI_FIN_MCS2_PREFIX_FROM_SEAT  2 ")
		BREAK
		
		CASE VS_EXTRA_LEFT_1 //3	// back seat
			sreturn = "MP_2"
			PRINTLN("[RCC MISSION] GET_CUTSCENE_MPH_PRI_FIN_MCS2_PREFIX_FROM_SEAT  3 ")
		BREAK
		
		DEFAULT
			sreturn = "MP_4"
		BREAK
		
	ENDSWITCH
	
	RETURN sreturn
	
ENDFUNC

FUNC TEXT_LABEL_23 GET_CUTSCENE_PREFIX_FROM_SEAT(VEHICLE_SEAT tempSeat)

	TEXT_LABEL_23 sreturn
	
	SWITCH tempSeat
	
		CASE VS_DRIVER
			sreturn = "MP_2"
		BREAK
		CASE VS_FRONT_RIGHT
			sreturn = "MP_3"
		BREAK
		CASE VS_BACK_LEFT
			sreturn = "MP_4"
		BREAK
		CASE VS_BACK_RIGHT
			sreturn = "MP_1"
		BREAK
		
	ENDSWITCH

	RETURN sreturn
	
ENDFUNC

FUNC VEHICLE_INDEX GET_FIRST_END_CUTSCENE_VEHICLE()

	INT i
	INT iCutsceneEntityIndex
	
	FOR i = 0 TO ( FMMC_MAX_CUTSCENE_ENTITIES - 1 )	
		iCutsceneEntityIndex = g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[ i ].iIndex
		IF g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[ i ].iType = CREATION_TYPE_VEHICLES
		AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[ i ].tlCutsceneHandle)
			iCutsceneEntityIndex = g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[ i ].iIndex
			IF iCutsceneEntityIndex > -1
				IF IS_NET_VEHICLE_DRIVEABLE(MC_serverBD_1.sFMMC_SBD.niVehicle[iCutsceneEntityIndex])
					RETURN NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iCutsceneEntityIndex])
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN NULL

ENDFUNC

FUNC VEHICLE_INDEX GET_FIRST_MID_CUTSCENE_VEHICLE()

	INT i
	INT iCutsceneEntityIndex

	INT iCutsceneToUse

	IF g_iFMMCScriptedCutscenePlaying >= FMMC_GET_MAX_SCRIPTED_CUTSCENES()
	AND g_iFMMCScriptedCutscenePlaying < FMMC_GET_MAX_CUTSCENES_WITH_MOCAP()
		iCutsceneToUse = (g_iFMMCScriptedCutscenePlaying - FMMC_GET_MAX_SCRIPTED_CUTSCENES()) 
	ELSE
		iCutsceneToUse = g_iFMMCScriptedCutscenePlaying
	ENDIF
	
	PRINTLN("[rcc mission]GET_FIRST_MID_CUTSCENE_VEHICLE iCutsceneToUse ", iCutsceneToUse)
	PRINTLN("[rcc mission]GET_FIRST_MID_CUTSCENE_VEHICLE g_iFMMCScriptedCutscenePlaying", g_iFMMCScriptedCutscenePlaying)
	IF iCutsceneToUse != -1
		FOR i = 0 TO ( FMMC_MAX_CUTSCENE_ENTITIES - 1 )	
			iCutsceneEntityIndex = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sCutsceneEntities[ i ].iIndex
			IF g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sCutsceneEntities[ i ].iType = CREATION_TYPE_VEHICLES
			AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sCutsceneEntities[ i ].tlCutsceneHandle)
				iCutsceneEntityIndex = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sCutsceneEntities[ i ].iIndex
				IF iCutsceneEntityIndex > -1
					IF IS_NET_VEHICLE_DRIVEABLE(MC_serverBD_1.sFMMC_SBD.niVehicle[iCutsceneEntityIndex])
						RETURN NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iCutsceneEntityIndex])
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ELSE
	
		SCRIPT_ASSERT("GET_FIRST_MID_CUTSCENE_VEHICLE iCutsceneToUse is invalid; ")
	ENDIF
	
	RETURN NULL

ENDFUNC

FUNC TEXT_LABEL_23 GET_CUTSCENE_PLAYER_PREFIX_FOR_PRISON_FINALE(INT iplayer)

	TEXT_LABEL_23 sreturn
	
	PED_INDEX tempPed = GET_PLAYER_PED(MC_serverBD.piCutscenePlayerIDs[MC_playerBD[iPartToUse].iteam][iplayer])
	
	VEHICLE_INDEX tempVeh = GET_FIRST_END_CUTSCENE_VEHICLE()
	
	IF NOT IS_VEHICLE_DRIVEABLE(tempVeh)
		IF NOT IS_PED_INJURED(tempPed)
			IF IS_PED_IN_ANY_VEHICLE(tempPed)
				tempVeh = GET_VEHICLE_PED_IS_IN(tempPed)
			ENDIF
		ENDIF
	ENDIF

	IF IS_VEHICLE_DRIVEABLE(tempVeh)
		VEHICLE_SEAT tempSeat = GET_SEAT_PED_IS_IN(tempPed)
		sreturn = GET_CUTSCENE_PREFIX_FROM_SEAT(tempSeat)
	ENDIF

	RETURN sreturn
	
ENDFUNC

FUNC TEXT_LABEL_23 GET_CUTSCENE_PLAYER_PREFIX_FOR_CASINO_HEIST_TUNNEL_EXPLOSION(INT iPlayer)

	TEXT_LABEL_23 sReturn
	PRINTLN("[InteractWith][CasinoTunnelExplosion] GET_CUTSCENE_PLAYER_PREFIX_FOR_CASINO_HEIST_TUNNEL_EXPLOSION | Calling for player ", iPlayer)
	
	IF MC_serverBD_4.iInteractWith_BombPlantParticipant = iPlayer
		sReturn = "MP_2"
		PRINTLN("[InteractWith][CasinoTunnelExplosion] GET_CUTSCENE_PLAYER_PREFIX_FOR_CASINO_HEIST_TUNNEL_EXPLOSION | This player was the planter")
		
	ELSE
	
		IF iPlayer = 1
			//This player would usually be "MP_2" but someone else stole their slot because they're the one who planted the bomb
			sReturn = "MP_"
			sReturn += MC_serverBD_4.iInteractWith_BombPlantParticipant + 1
			PRINTLN("[InteractWith][CasinoTunnelExplosion] GET_CUTSCENE_PLAYER_PREFIX_FOR_CASINO_HEIST_TUNNEL_EXPLOSION | This player has to switch up")
			
		ELSE
			SWITCH iPlayer
				CASE 0	sReturn = "MP_1" BREAK
				CASE 2	sReturn = "MP_3" BREAK
				CASE 3	sReturn = "MP_4" BREAK
			ENDSWITCH
			
			PRINTLN("[InteractWith][CasinoTunnelExplosion] GET_CUTSCENE_PLAYER_PREFIX_FOR_CASINO_HEIST_TUNNEL_EXPLOSION | Using switch statement")
		ENDIF
	ENDIF
	
	PRINTLN("[InteractWith][CasinoTunnelExplosion] GET_CUTSCENE_PLAYER_PREFIX_FOR_CASINO_HEIST_TUNNEL_EXPLOSION | Returning ", sReturn, " for iPlayer ", iPlayer)
	RETURN sReturn
	
ENDFUNC

FUNC TEXT_LABEL_23 GET_CUTSCENE_PLAYER_PREFIX_FOR_PRISON_MPH_PRI_FIN_MCS2(INT iplayer)

	TEXT_LABEL_23 sReturn
	PLAYER_INDEX tempPlayer = MC_serverBD.piCutscenePlayerIDs[1][iplayer]
	PED_INDEX tempPed = GET_PLAYER_PED(MC_serverBD.piCutscenePlayerIDs[1][iplayer])
	VEHICLE_INDEX tempVeh = GET_FIRST_MID_CUTSCENE_VEHICLE()
	
	IF MC_serverBD.piCutscenePlayerIDs[1][iplayer] != INVALID_PLAYER_INDEX()
		IF NOT IS_VEHICLE_DRIVEABLE(tempVeh)
		AND NOT IS_PED_INJURED(tempPed)
		AND IS_PED_IN_ANY_VEHICLE(tempPed)
			tempVeh = GET_VEHICLE_PED_IS_IN(tempPed)
			PRINTLN( "[RCC MISSION] [PED CUT] GET_CUTSCENE_PLAYER_PREFIX_FOR_PRISON_MPH_PRI_FIN_MCS2 Accessing vehicle player ", iplayer, " is in")
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(tempVeh)
			IF GET_ENTITY_MODEL(tempVeh) = VELUM2
				PRINTLN("[RCC MISSION] [PED CUT] GET_CUTSCENE_PLAYER_PREFIX_FOR_PRISON_MPH_PRI_FIN_MCS2 It's a Velum!")
			ELSE
				PRINTLN("[RCC MISSION] [PED CUT] GET_CUTSCENE_PLAYER_PREFIX_FOR_PRISON_MPH_PRI_FIN_MCS2 NOT a Velum")
			ENDIF
			
			PRINTLN("[RCC MISSION] [PED CUT] GET_CUTSCENE_PLAYER_PREFIX_FOR_PRISON_MPH_PRI_FIN_MCS2 Player ", iplayer," is in seat ", ENUM_TO_INT(GET_PED_VEHICLE_SEAT(tempPed, tempVeh)))
			
			IF NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(tempVeh, VS_DRIVER)) = tempPlayer
				sReturn = "MP_1"
			ELIF NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(tempVeh, VS_BACK_RIGHT)) = tempPlayer
			OR NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(tempVeh, VS_BACK_LEFT)) = tempPlayer
				sReturn = "MP_2"
			ELIF NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(tempVeh, VS_EXTRA_LEFT_1)) = tempPlayer
				sReturn = "MP_3"
			ELSE
				sReturn = "MP_4"
			ENDIF
		ELSE
			PRINTLN( "[RCC MISSION] [PED CUT] GET_CUTSCENE_PLAYER_PREFIX_FOR_PRISON_MPH_PRI_FIN_MCS2 Vehicle not driveable")
		ENDIF

		PRINTLN("[RCC MISSION] [PED CUT] GET_CUTSCENE_PLAYER_PREFIX_FOR_PRISON_MPH_PRI_FIN_MCS2 tempPlayer = ", NATIVE_TO_INT(tempPlayer), " callled ", GET_PLAYER_NAME(tempPlayer))
		IF DOES_ENTITY_EXIST(tempVeh)
			PRINTLN("GET_CUTSCENE_PLAYER_PREFIX_FOR_PRISON_MPH_PRI_FIN_MCS2 VS_DRIVER = ", NATIVE_TO_INT(GET_PED_IN_VEHICLE_SEAT(tempVeh, VS_DRIVER)))
			PRINTLN("GET_CUTSCENE_PLAYER_PREFIX_FOR_PRISON_MPH_PRI_FIN_MCS2 VS_FRONT_RIGHT = ", NATIVE_TO_INT(GET_PED_IN_VEHICLE_SEAT(tempVeh, VS_FRONT_RIGHT)))
			PRINTLN("GET_CUTSCENE_PLAYER_PREFIX_FOR_PRISON_MPH_PRI_FIN_MCS2 VS_BACK_LEFT = ", NATIVE_TO_INT(GET_PED_IN_VEHICLE_SEAT(tempVeh, VS_BACK_LEFT)))
			PRINTLN("GET_CUTSCENE_PLAYER_PREFIX_FOR_PRISON_MPH_PRI_FIN_MCS2 VS_BACK_RIGHT = ", NATIVE_TO_INT(GET_PED_IN_VEHICLE_SEAT(tempVeh, VS_BACK_RIGHT)))
			PRINTLN("GET_CUTSCENE_PLAYER_PREFIX_FOR_PRISON_MPH_PRI_FIN_MCS2 VS_EXTRA_LEFT_1 = ", NATIVE_TO_INT(GET_PED_IN_VEHICLE_SEAT(tempVeh, VS_EXTRA_LEFT_1)))
			PRINTLN("GET_CUTSCENE_PLAYER_PREFIX_FOR_PRISON_MPH_PRI_FIN_MCS2 VS_EXTRA_RIGHT_1 = ", NATIVE_TO_INT(GET_PED_IN_VEHICLE_SEAT(tempVeh, VS_EXTRA_RIGHT_1)))
		ENDIF
		
		PRINTLN( "[RCC MISSION] [PED CUT] GET_CUTSCENE_PLAYER_PREFIX_FOR_PRISON_MPH_PRI_FIN_MCS2 iPlayer ", iplayer, " set as ", sreturn)
	ELSE
		PRINTLN("[RCC MISSION] [PED CUT] GET_CUTSCENE_PLAYER_PREFIX_FOR_PRISON_MPH_PRI_FIN_MCS2 - MC_serverBD.piCutscenePlayerIDs[1][", iplayer, "] IS INVALID!")
	ENDIF
	
	RETURN sreturn
	
ENDFUNC

FUNC TEXT_LABEL_23 GET_CUTSCENE_PLAYER_PREFIX_FOR_MPH_NAR_MET_EXT(INT iplayer)

	TEXT_LABEL_23 sreturn

	IF iNarMetDriver = -1
		INT i
		FLOAT fTempDistance = 100
		VEHICLE_INDEX tempVeh
		ENTITY_INDEX EntID = NULL
		FOR i = 0 TO (FMMC_MAX_CUTSCENE_ENTITIES - 1)
			IF ARE_STRINGS_EQUAL(g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].tlCutsceneHandle, "PHANTOM")
				EntID = NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iIndex])
				IF GET_DISTANCE_BETWEEN_COORDS(<<1375.2, 3611.1, 34.9>>, GET_ENTITY_COORDS(EntID)) < fTempDistance
					fTempDistance = GET_DISTANCE_BETWEEN_COORDS(<<1375.2, 3611.1, 34.9>>, GET_ENTITY_COORDS(EntID))
					tempVeh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(EntID)
				ENDIF
			ENDIF
		ENDFOR
		IF tempVeh <> NULL
			REPEAT 4 i
				PED_INDEX tempPed = GET_PLAYER_PED(MC_serverBD.piCutscenePlayerIDs[MC_playerBD[iPartToUse].iteam][i])
				CPRINTLN(DEBUG_MISSION, "tempPed = MC_serverBD.piCutscenePlayerIDs[",MC_playerBD[iPartToUse].iteam,"][",i,"]")
				IF NOT IS_PED_INJURED(tempPed)
					IF IS_PED_IN_VEHICLE(tempPed, tempVeh)
						VEHICLE_SEAT tempSeat = GET_SEAT_PED_IS_IN(tempPed)
						IF tempSeat = VS_DRIVER
							iNarMetDriver = i
							i = 4
							CPRINTLN(DEBUG_MISSION, "iNarMetDriver: ", iNarMetDriver)
						ELSE
							CPRINTLN(DEBUG_MISSION, "tempPed is not driver, player: ", i)
						ENDIF
					ELSE
						CPRINTLN(DEBUG_MISSION, "tempPed is not in tempVeh, player: ", i)
					ENDIF
				ELSE
					CPRINTLN(DEBUG_MISSION, "tempPed = NULL for player: ", i)
				ENDIF
			ENDREPEAT
		ELSE
			CPRINTLN(DEBUG_MISSION, "tempVeh not found")
		ENDIF
	ELSE
		CPRINTLN(DEBUG_MISSION, "iNarMetDriver found: ", iNarMetDriver)
	ENDIF

	SWITCH iNarMetDriver
		
		CASE -1 //Failed to find a driver.
			CPRINTLN(DEBUG_MISSION, "Failed to find driver.")
		FALLTHRU
		CASE 0
			IF iplayer = 0 		sreturn = "MP_1"
			ELIF iplayer = 1	sreturn = "MP_2"
			ELIF iplayer = 2	sreturn = "MP_3"
			ELIF iplayer = 3	sreturn = "MP_4"
			ENDIF
		BREAK
		
		CASE 1
			IF iplayer = 0 		sreturn = "MP_2"
			ELIF iplayer = 1	sreturn = "MP_1"
			ELIF iplayer = 2	sreturn = "MP_3"
			ELIF iplayer = 3	sreturn = "MP_4"
			ENDIF
		BREAK
		
		CASE 2
			IF iplayer = 0 		sreturn = "MP_2"
			ELIF iplayer = 1	sreturn = "MP_3"
			ELIF iplayer = 2	sreturn = "MP_1"
			ELIF iplayer = 3	sreturn = "MP_4"
			ENDIF
		BREAK
		
		CASE 3
			IF iplayer = 0 		sreturn = "MP_2"
			ELIF iplayer = 1	sreturn = "MP_3"
			ELIF iplayer = 2	sreturn = "MP_4"
			ELIF iplayer = 3	sreturn = "MP_1"
			ENDIF
		BREAK
	
	ENDSWITCH
	
	PRINTLN( "[RCC MISSION] [PED CUT] GET_CUTSCENE_PLAYER_PREFIX_FOR_MPH_NAR_MET_EXT iPlayer ", iplayer, " set as ", sreturn) 
	
	RETURN sreturn
	
ENDFUNC

FUNC INT GET_MOCAP_NUMBER_OF_PLAYERS(STRING sCutscenestring)
		
	IF IS_STRING_EMPTY(Scutscenestring)//url:bugstar:4628425
		PRINTLN("[RCC MISSION] NULL GET_MOCAP_NUMBER_OF_PLAYERS STRING = " ,Scutscenestring )
		RETURN 0
	ENDIF
	
	IF are_strings_equal(Scutscenestring, "MPH_FIN_CEL_TRE")    
	OR are_strings_equal(Scutscenestring, "MPH_PAC_FIN_MCS2")   
	OR are_strings_equal(Scutscenestring, "MPH_PRI_UNF_MCS1")  
	OR are_strings_equal(Scutscenestring, "MPH_TUT_MCS1")     	
	OR are_strings_equal(sCutsceneString, "LOW_FUN_MCS1")
	OR ARE_STRINGS_EQUAL(sCutsceneString, "sil_pred_mcs1")
	OR ARE_STRINGS_EQUAL(sCutsceneString, "HS3F_ALL_ESC")
	OR ARE_STRINGS_EQUAL(sCutsceneString, "HS3F_ALL_ESC_B")
		RETURN 0  
	ENDIF

	IF are_strings_equal (Scutscenestring, "MPH_HUM_KEY_EXT"  ) 
	OR are_strings_equal (Scutscenestring, "HEIST_INT"        ) 
	OR are_strings_equal (Scutscenestring, "MPH_PAC_HAC_MCS1" ) 
	OR are_strings_equal (Scutscenestring, "MPH_PRI_FIN_MCS1" )
		RETURN 1 
	ENDIF

	IF are_strings_equal (Scutscenestring, "MPH_FIN_CEL_APT"  ) 
	OR are_strings_equal (Scutscenestring, "MPH_FIN_CEL_BAR"  ) 
	OR are_strings_equal (Scutscenestring, "MPH_FIN_CEL_BEA"  ) 
	OR are_strings_equal (Scutscenestring, "MPH_HUM_FIN_MCS1" ) 
	OR are_strings_equal (Scutscenestring, "MPH_PRI_STA_EXT"  ) 
	OR are_strings_equal (Scutscenestring, "MPH_PRI_STA_MCS1" ) 
	OR are_strings_equal (Scutscenestring, "MPH_PRI_UNF_EXT"  ) 
	OR are_strings_equal (Scutscenestring, "MPH_TUT_CAR_EXT"  ) 
	OR are_strings_equal (Scutscenestring, "MPH_PRI_PLA_EXT"  ) 
	OR are_strings_equal (Scutscenestring, "MPH_TUT_EXT"      ) 
	OR are_strings_equal (Scutscenestring, "MPH_TUT_FIN_INT"  ) 
	OR are_strings_equal (Scutscenestring, "MPH_TUT_INT"      ) 
	OR are_strings_equal (Scutscenestring, "MPH_TUT_MID"      ) 
	OR are_strings_equal (Scutscenestring, "MPH_HUM_DEL_EXT"  ) 
	OR ARE_STRINGS_EQUAL (sCutsceneString, "LOW_PHO_EXT" )
	OR ARE_STRINGS_EQUAL (sCutsceneString, "LOW_TRA_EXT" )
	OR ARE_STRINGS_EQUAL (sCutsceneString, "SUBJ_MCS0" )
	OR ARE_STRINGS_EQUAL (sCutsceneString, "SUBJ_MCS1" )
		RETURN 2 
	ENDIF

	IF are_strings_equal (Scutscenestring, "MPH_PRI_FIN_MCS2")	
	OR are_strings_equal (Scutscenestring, "MPH_HUM_KEY_MCS1" ) 
		RETURN 3 
	ENDIF

	PRINTLN("[RCC MISSION]GET_MOCAP_NUMBER_OF_PLAYERS STRING = " ,Scutscenestring )
	//SCRIPT_ASSERT("GET_MOCAP_NUMBER_OF_PLAYERS invalid string passed.")
	RETURN 4
	
ENDFUNC

FUNC INT GET_MOCAP_PLAYER_COUNT()
	INT iPlayerCount, i
	FOR i = 0 TO GET_MOCAP_NUMBER_OF_PLAYERS(sMocapCutscene)-1
		IF MC_serverBD.piCutscenePlayerIDs[0][i] != INVALID_PLAYER_INDEX() // We know that only team 0 matters here.
			iPlayerCount++
		ENDIF
	ENDFOR
	RETURN iPlayerCount
ENDFUNC

FUNC TEXT_LABEL_23 GET_CUTSCENE_PLAYER_PREFIX(INT iTempPlayerNum,INT iplayer)

	INT iplayerNum = (iTempPlayerNum+1)
	TEXT_LABEL_23 sreturn

	sreturn = "MP_"
	sreturn +=iplayerNum
	
	IF NOT are_strings_equal(sMocapCutscene, "MP_INT_MCS_17_A7")
		IF iplayerNum = 1
			IF IS_BIT_SET(iLocalBoolCheck2,LBOOL2_MOCAP_MALE)
				sreturn = "MP_Male_Character"
			ELIF IS_BIT_SET(iLocalBoolCheck2,LBOOL2_MOCAP_FEMALE)
				sreturn = "MP_Female_Character"
			ENDIF
		ENDIF
	ENDIF
	
	IF are_strings_equal(sMocapCutscene, "MPH_PRI_FIN_EXT")
		sreturn = GET_CUTSCENE_PLAYER_PREFIX_FOR_PRISON_FINALE(iplayer)
	ENDIF
			
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_NAR_MET_EXT")
		sreturn = GET_CUTSCENE_PLAYER_PREFIX_FOR_MPH_NAR_MET_EXT(iTempPlayerNum)
	ENDIF
	
	IF are_strings_equal(sMocapCutscene, "HS3F_DIR_SEW")
		sreturn = GET_CUTSCENE_PLAYER_PREFIX_FOR_CASINO_HEIST_TUNNEL_EXPLOSION(iTempPlayerNum)
	ENDIF
		
	IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPCAS6_EXT")
	AND GET_MOCAP_PLAYER_COUNT() <= 1
		IF iplayerNum = 1
			sreturn = "MP_2"
		ELIF iplayerNum = 2
			sreturn = "MP_1"
		ENDIF
	ENDIF
			
	PRINTLN("GET_CUTSCENE_PLAYER_PREFIX returning ", sreturn)
	
	RETURN sreturn
	
ENDFUNC

PROC SET_PLAYER_COMPONENTS_FOR_CUTSCENE(INT iplayer)

TEXT_LABEL_23 sPlayerString

	sPlayerString = GET_CUTSCENE_PLAYER_PREFIX(iCutComponentPlayerNum,iplayer)
	SET_CUTSCENE_ENTITY_STREAMING_FLAGS(sPlayerString, DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
	PRINTLN("Registering player for component variations for cutscene: ",iplayer," handle: ",sPlayerString)
	iCutComponentPlayerNum++

ENDPROC

FUNC BOOL MANUALLY_SET_PLAYER_COMPONENTS_FOR_CUTSCENE()
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sMocapCutscene)
		
		IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_TUT_CAR_EXT") //Manually streaming variations for this cutscene.
			SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_1", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS) 
			SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_2", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
			
			RETURN TRUE
			
		ELIF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PRI_FIN_MCS2") //Manually streaming variations for this cutscene.
			SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_1", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS) 
			SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_2", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
			SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_3", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
			
			RETURN TRUE
			
		ELIF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PRI_FIN_EXT") //Manually streaming variations for this cutscene.
			SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_1", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS) 
			SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_2", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
			SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_3", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
			SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_4", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
			
			RETURN TRUE
			
		ENDIF
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC SETUP_CUTSCENE_PLAYERS_COMPONENTS()

	IF NOT MANUALLY_SET_PLAYER_COMPONENTS_FOR_CUTSCENE()
		SET_PLAYER_COMPONENTS_FOR_CUTSCENE(0)
		SET_PLAYER_COMPONENTS_FOR_CUTSCENE(1)
		SET_PLAYER_COMPONENTS_FOR_CUTSCENE(2)
		SET_PLAYER_COMPONENTS_FOR_CUTSCENE(3)
	ENDIF
	
ENDPROC

PROC CLEAR_CUTSCENE_PRIMARY_PLAYER(INT iTeam)

	IF MC_serverBD.piCutscenePlayerIDs[iTeam][0] != INVALID_PLAYER_INDEX()
	
		INT iOldPart = -1
	
		IF IS_NET_PLAYER_OK(MC_serverBD.piCutscenePlayerIDs[iTeam][0], FALSE)
			PARTICIPANT_INDEX oldPart = NETWORK_GET_PARTICIPANT_INDEX(MC_serverBD.piCutscenePlayerIDs[iTeam][0])
			iOldPart = NATIVE_TO_INT(oldPart)
		ENDIF
		
		IF iOldPart > -1
			CLEAR_BIT(MC_serverBD.iCutscenePlayerBitset[iTeam],iOldPart)
			
			INT iOtherTeam
			FOR iOtherTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
				IF DOES_TEAM_LIKE_TEAM(iOtherTeam, iTeam)
					CLEAR_BIT(MC_serverBD.iCutscenePlayerBitset[iOtherTeam], iOldPart)
				ENDIF
			ENDFOR
			
			PRINTLN("[RCC MISSION] clearing old primary cutscene part bitset: ", iOldPart," for team: ", iTeam)
		ENDIF
		
	ENDIF

ENDPROC

PROC SET_CUTSCENE_PRIMARY_PLAYER(INT iPart, INT iteam)

	PARTICIPANT_INDEX partID = INT_TO_PARTICIPANTINDEX(iPart)
	IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(partID)
		PRINTLN("[RCC MISSION] SET_CUTSCENE_PRIMARY_PLAYER - iteam: ", iteam, " iPart: ", iPart, " - Participant not active, BAIL")
		EXIT
	ENDIF
	
	PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX(partID)								
	IF NOT IS_NET_PLAYER_OK(playerID)
		PRINTLN("[RCC MISSION] SET_CUTSCENE_PRIMARY_PLAYER - iteam: ", iteam, " iPart: ", iPart, " partID: ", NATIVE_TO_INT(partID), " playerID: ", NATIVE_TO_INT(playerID)," - player NOT OK, BAIL")
		EXIT
	ENDIF

	CLEAR_CUTSCENE_PRIMARY_PLAYER(iTeam)

	//Get out on a hesit
	IF AM_I_ON_A_HEIST()
	OR IS_LOADED_MISSION_LOW_RIDER_FLOW_MISSION()
		PRINTLN("[RCC MISSION] SET_CUTSCENE_PRIMARY_PLAYER - iteam: ", iteam, " iPart: ", iPart, " partID: ", NATIVE_TO_INT(partID), " playerID: ", NATIVE_TO_INT(playerID)," - HEIST/LOWRIDER BAIL")
		EXIT
	ENDIF
	
	MC_serverBD.piCutscenePlayerIDs[iteam][0] = playerID
	SET_BIT(MC_serverBD.iCutscenePlayerBitset[iteam], iPart)
	PRINTLN("[RCC MISSION] SET_CUTSCENE_PRIMARY_PLAYER - iteam: ", iteam, " iPart: ", iPart, " partID: ", NATIVE_TO_INT(partID), " playerID: ", NATIVE_TO_INT(playerID)," - SET as PRIMARY PLAYER")
	
	INT iOtherTeam
	FOR iOtherTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
		IF DOES_TEAM_LIKE_TEAM(iOtherTeam, iteam)
		
			MC_serverBD.piCutscenePlayerIDs[iOtherTeam][0] = playerID
			SET_BIT(MC_serverBD.iCutscenePlayerBitset[iOtherTeam], iPart)
			
			PRINTLN("[RCC MISSION] SET_CUTSCENE_PRIMARY_PLAYER - iteam: ", iteam, " iPart: ", iPart, " partID: ", NATIVE_TO_INT(partID), " playerID: ", NATIVE_TO_INT(playerID)," - SET as PRIMARY PLAYER for iOtherTeam: ", iOtherTeam)
		ENDIF
	ENDFOR

ENDPROC

//Set data of a particular weapon, INT iPlayer is the position in the array you want the reciever to use. 
PROC SEND_MY_WEAPONDATA_TO_REMOTE_PLAYERS(WEAPON_TYPE aWeapon)	
	
	SCRIPT_EVENT_DATA_FMMC_MY_WEAPON_DATA sWeaponData
	
	//Set up the event details
	sWeaponData.Details.Type 			= SCRIPT_EVENT_FMMC_MY_WEAPON_DATA
	sWeaponData.Details.FromPlayerIndex = PLAYER_ID()
	sWeaponData.iParticipant = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(PLAYER_ID()))
	
	IF DOES_PLAYER_HAVE_WEAPON(aWeapon)
	
		CPRINTLN( DEBUG_CONTROLLER, " SEND_MY_WEAPONDATA_TO_REMOTE_PLAYERS - DOES_PLAYER_HAVE_WEAPON TRUE - player has weapon = ", GET_WEAPON_NAME( aWeapon ) )
		sWeaponData.bHasWeapon = TRUE
	
		PED_WEAPONS_MP_STRUCT sWeapons
		GET_PED_WEAPONS_MP( PLAYER_PED_ID(), sWeapons )

		INT i
		FOR i = 0 TO NUM_PLAYER_PED_WEAPON_SLOTS-1
			IF sWeapons.sWeaponInfo[i].eWeaponType = aWeapon
				sWeaponData.sWeaponInfo = sWeapons.sWeaponInfo[i]
				i = NUM_PLAYER_PED_WEAPON_SLOTS // break loop
			ENDIF
		ENDFOR
		
		FOR i = 0 TO NUMBER_OF_DLC_WEAPONS-1
			IF sWeapons.sDLCWeaponInfo[i].eWeaponType = aWeapon
				sWeaponData.sWeaponInfo = sWeapons.sDLCWeaponInfo[i]
				i = NUMBER_OF_DLC_WEAPONS // break loop
			ENDIF
		ENDFOR
		
	ELSE
		sWeaponData.bHasWeapon = FALSE
		CPRINTLN( DEBUG_CONTROLLER, " SEND_MY_WEAPONDATA_TO_REMOTE_PLAYERS - DOES_PLAYER_HAVE_WEAPON FALSE - player does not have weapon = ", GET_WEAPON_NAME( aWeapon ) )
	ENDIF
	
	//trigger that event
	INT iPlayerFlags = ALL_PLAYERS_ON_SCRIPT() // any player could be the host 
	IF NOT (iPlayerFlags = 0)
		SEND_TU_SCRIPT_EVENT( SCRIPT_EVENT_QUEUE_NETWORK, sWeaponData, SIZE_OF(sWeaponData), iPlayerFlags )	
	ELSE
		NET_PRINT("SEND_MY_WEAPONDATA_TO_OTHER_PLAYERS - playerflags = 0 so not broadcasting") NET_NL()		
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_STREAM_APARTMENT_CUTSCENE( STRING sCutsceneName )
	BOOL bReturn = TRUE
	
	IF IS_BIT_SET( iLocalBoolCheck7, LBOOL7_SHOULD_HOLD_UP_MOCAP_FOR_CUTSCENE )
		IF NOT IS_BIT_SET( iLocalBoolCheck7, LBOOL7_TOGGLED_RENDERPHASES_FOR_DROPOFF )
		OR IS_LOCAL_PLAYER_ANY_SPECTATOR() // Never stream apartment cutscenes if we're SCTV
			bReturn = FALSE
		ENDIF
	ENDIF
	
	// Never stream this particular mocap cutscene
	IF ARE_STRINGS_EQUAL( sCutsceneName, "MPH_TUT_MID" )
		bReturn = FALSE
	ENDIF
	
	//#2109668  hold up streaming mocap
	IF ( g_bPropertyDropOff OR g_bGarageDropOff )
	AND g_bShouldThisPlayerGetPulledIntoApartment
		
		// B* 2230895
		IF NOT g_HeistGarageDropoffPanComplete AND NOT g_HeistApartmentReadyForDropOffCut
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Not streaming ", sCutsceneName, " because NOT g_HeistGarageDropoffPanComplete OR g_HeistApartmentReadyForDropOffCut")
			bReturn = FALSE
		ENDIF
	
		INT iTeam = MC_playerBD[ iPartToUse ].iteam
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[ iTeam ]
		
		IF iRule < FMMC_MAX_RULES
			IF g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffType[ iRule ] = ciFMMC_DROP_OFF_TYPE_PROPERTY
			AND NOT g_HeistApartmentReadyForDropOffCut
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Not streaming ", sCutsceneName, " because NOT g_HeistApartmentReadyForDropOffCut")
				bReturn = FALSE
			ENDIF
			IF g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffType[ iRule ] = ciFMMC_DROP_OFF_TYPE_LEADER_GARAGE
			AND NOT g_HeistGarageDropoffPanComplete
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Not streaming ", sCutsceneName, " because NOT g_HeistGarageDropoffPanComplete")
				bReturn = FALSE
			ENDIF
		ENDIF
		
	ENDIF
	
	RETURN bReturn
ENDFUNC

FUNC BOOL SHOULD_SEE_APARTMENT_CUTSCENE( STRING sCutsceneName )
	BOOL bReturn = TRUE
	
	UNUSED_PARAMETER( sCutsceneName )

	// B* 2230895
	IF ( g_bPropertyDropOff OR g_bGarageDropOff )
	
		IF NOT g_HeistGarageDropoffPanStarted AND NOT g_HeistApartmentReadyForDropOffCut
		AND NOT g_HeistGarageDropoffPanComplete AND NOT g_HeistApartmentReadyForDropOffCut
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Holding up watching  ", sCutsceneName, " because NOT g_HeistGarageDropoffPanComplete OR g_HeistApartmentReadyForDropOffCut")
			bReturn = FALSE
		ENDIF
		
	ENDIF

	RETURN bReturn
ENDFUNC

FUNC BOOL IS_PROPERTY_DROP_COMPLETE()
	IF (g_bPropertyDropOff OR g_bGarageDropOff)
	AND (g_HeistApartmentReadyForDropOffCut OR g_HeistGarageDropoffPanComplete)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_MOCAP_IN_AN_APARTMENT( STRING strMocapName  )
	PRINTLN("[RCC MISSION] IS_THIS_MOCAP_IN_AN_APARTMENT strMocapName = ", strMocapName)
	
	IF IS_STRING_EMPTY(strMocapName)//url:bugstar:4628425
		PRINTLN("[RCC MISSION] [IS_THIS_MOCAP_IN_AN_APARTMENT] strMocapName IS NULL")
		RETURN FALSE
	ENDIF
	
	IF ARE_STRINGS_EQUAL( strMocapName, "MPH_PRI_STA_EXT" ) // Station
	OR ARE_STRINGS_EQUAL( strMocapName, "MPH_PRI_UNF_EXT" ) // Unfinished Buisiness
	OR ARE_STRINGS_EQUAL( strMocapName, "MPH_HUM_KEY_EXT" ) // Humane key
		PRINTLN("[RCC MISSION] IS_THIS_MOCAP_IN_AN_APARTMENT TRUE")
		RETURN TRUE
	ENDIF
	PRINTLN("[RCC MISSION] IS_THIS_MOCAP_IN_AN_APARTMENT FALSE")
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_SEE_MOCAP_INTERIOR(STRING strMocapName, VECTOR &vLocation)
	IF ARE_STRINGS_EQUAL(strMocapName, "MPH_HUM_ARM_EXT")
		vLocation = <<778.7206, 4183.9946, 40.7943>>
		RETURN TRUE
	ENDIF
	
	IF ARE_STRINGS_EQUAL(strMocapName, "MPH_PAC_PHO_EXT")
		vLocation = <<717.4654, -972.8856, 24.5331>>		//Lester's Factory
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Some mocaps require an interior streamed for the spectator
//For cutscenes that aren't in apartments
FUNC BOOL LOAD_INTERIOR_FOR_SPECTATOR_MOCAP( STRING strMocapName )

	VECTOR vInteriorLocation

	IF IS_PLAYER_SPECTATING(PLAYER_ID())
		IF SHOULD_SEE_MOCAP_INTERIOR(strMocapName, vInteriorLocation)
			PRINTLN("[RCC MISSION] LOAD_INTERIOR_FOR_SPECTATOR_MOCAP HAVE VALID CUTSCENE TO STREAM INTERIOR")
			IF IS_VALID_INTERIOR(iCutInterior)
				IF IS_INTERIOR_READY(iCutInterior)
					PRINTLN("[RCC MISSION] LOAD_INTERIOR_FOR_SPECTATOR_MOCAP INTERIOR IS READY")
					RETURN TRUE
				ELSE
					PRINTLN("[RCC MISSION] LOAD_INTERIOR_FOR_SPECTATOR_MOCAP INTERIOR ISN'T READY")
				ENDIF
			ELSE
				IF NOT ARE_VECTORS_EQUAL(vInteriorLocation, <<0,0,0>>)
					iCutInterior = GET_INTERIOR_AT_COORDS(vInteriorLocation)
					IF IS_VALID_INTERIOR(iCutInterior)
						PRINTLN("[RCC MISSION] LOAD_INTERIOR_FOR_SPECTATOR_MOCAP PINNING INTERIOR IN MEMORY")
						PIN_INTERIOR_IN_MEMORY(iCutInterior)
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION] LOAD_INTERIOR_FOR_SPECTATOR_MOCAP INVALID INTERIOR SUPPLIED")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_THE_CROWD_CONTROL_FAIL_CUTSCENE_CURRENTLY_PLAYING()
	BOOL bReturn = FALSE

	IF IS_BIT_SET( MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_CC_FAILCUT_START )
	AND NOT IS_BIT_SET( MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_CC_FAILCUT_FINISH ) 
		bReturn = TRUE
	ENDIF

	RETURN bReturn
ENDFUNC

FUNC BOOL SHOULD_LOCAL_PLAYER_WATCH_TEAM_CUTSCENE( INT iteam, INT icutscene, FMMC_CUTSCENE_TYPE eCutType)
	
	INT iTeamCheck = MC_playerBD[iPartToUse].iteam
	
	IF IS_BIT_SET( MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYING_CUTSCENE )
	AND NOT IS_THE_CROWD_CONTROL_FAIL_CUTSCENE_CURRENTLY_PLAYING()
		RETURN TRUE // Always watch a cutscene we've already started, even if something below changes
	ENDIF
	
	IF IS_PLAYER_SPECTATING(LocalPlayer)
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bInMissionCreatorApartment) = TRUE
			RETURN FALSE
		ENDIF
	ENDIF
	
	//Get the spectated peds team
	IF USING_HEIST_SPECTATE()
		PED_INDEX spectatorTarget = GET_SPECTATOR_CURRENT_FOCUS_PED()
		
		IF NOT DOES_ENTITY_EXIST( spectatorTarget )
		OR IS_PED_INJURED( spectatorTarget )
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Checking if I should watch a cutscene as a Heist Spectator, and my spectator target isn't valid!" )
			RETURN FALSE
		ENDIF
	
		iTeamCheck = GET_PLAYER_TEAM( NETWORK_GET_PLAYER_INDEX_FROM_PED( spectatorTarget ) )
		PRINTLN("[RCC MISSION] USING HEIST SPECTATE CHECKING AGAINST SPECTATED PLAYERS TEAM : ", iTeamCheck )
	ENDIF

	IF iTeamCheck < 0 OR iTeamCheck >= FMMC_MAX_TEAMS
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] SHOULD_LOCAL_PLAYER_WATCH_TEAM_CUTSCENE - iTeamCheck is invalid! The value is ", iTeamCheck )
		RETURN FALSE
	ENDIF

	IF HAS_TEAM_FAILED( iTeamCheck )
		RETURN FALSE
	ENDIF
	
	IF MC_serverBD_4.iCurrentHighestPriority[ iTeamCheck ] >= FMMC_MAX_RULES
		RETURN FALSE
	ENDIF

	IF iteam = iTeamCheck
		RETURN TRUE
	ENDIF

	//MC_serverBD_4.iPlayerRule[iPlayerRule][iteam] = FMMC_OBJECTIVE_LOGIC_SCRIPTED_CUTSCENE 
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		IF eCutType = FMMCCUT_MOCAP
			SWITCH iTeamCheck
			
				CASE 0
					IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[icutscene].iCutsceneBitset,ci_CSBS_Pull_Team0_In)
						RETURN TRUE
					ENDIF
				BREAK
				CASE 1
					IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[icutscene].iCutsceneBitset,ci_CSBS_Pull_Team1_In)
						RETURN TRUE
					ENDIF		
				BREAK
				CASE 2
					IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[icutscene].iCutsceneBitset,ci_CSBS_Pull_Team2_In)
						RETURN TRUE
					ENDIF		
				BREAK
				CASE 3
					IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[icutscene].iCutsceneBitset,ci_CSBS_Pull_Team3_In)
						RETURN TRUE
					ENDIF		
				BREAK
			ENDSWITCH		
		ELSE
			SWITCH MC_playerBD[iPartToUse].iteam
			
				CASE 0
					IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].iCutsceneBitset,ci_CSBS_Pull_Team0_In)
						RETURN TRUE
					ENDIF
				BREAK
				CASE 1
					IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].iCutsceneBitset,ci_CSBS_Pull_Team1_In)
						RETURN TRUE
					ENDIF		
				BREAK
				CASE 2
					IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].iCutsceneBitset,ci_CSBS_Pull_Team2_In)
						RETURN TRUE
					ENDIF		
				BREAK
				CASE 3
					IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].iCutsceneBitset,ci_CSBS_Pull_Team3_In)
						RETURN TRUE
					ENDIF		
				BREAK
			ENDSWITCH
		ENDIF
		
	ENDIF
	
	RETURN FALSE

ENDFUNC


FUNC INT GET_CUTSCENE_TEAM( BOOL bFindStreamingTeam = FALSE )

INT iteamloop
INT iteam = -1
INT iCutsceneToUse
INT iCutsceneIndexToUse[ FMMC_MAX_TEAMS ]
INT iLoop
	
	IF NOT bFindStreamingTeam
	OR IS_BIT_SET( iLocalBoolCheck9, LBOOL9_FORCE_STREAM_CUTSCENE )
		FOR iLoop = 0 TO FMMC_MAX_TEAMS - 1
			iCutsceneIndexToUse[iLoop] = MC_serverBD.iCutsceneID[ iLoop ]
			PRINTLN("[RCC MISSION] GET_CUTSCENE_TEAM - (1) iCutsceneIndexToUse for iTeam: ", iLoop, " = MC_serverBD.iCutsceneID: ", MC_serverBD.iCutsceneID[iLoop])
		ENDFOR
	ELSE
		FOR iLoop = 0 TO FMMC_MAX_TEAMS - 1
			PRINTLN("[RCC MISSION] GET_CUTSCENE_TEAM - (2) iCutsceneIndexToUse for iTeam: ", iLoop, " = MC_serverBD.iCutsceneStreamingIndex: ", MC_serverBD.iCutsceneStreamingIndex[iLoop])
			iCutsceneIndexToUse[iLoop] = MC_serverBD.iCutsceneStreamingIndex[ iLoop ]
		ENDFOR
	ENDIF
	
	
	IF iCutsceneIndexToUse[MC_playerBD[iPartToUse].iteam] != -1
	OR IS_FAKE_MULTIPLAYER_MODE_SET()
		PRINTLN("[RCC MISSION] GET_CUTSCENE_TEAM - (3) Assiging Team here.")
		iteam = MC_playerBD[iPartToUse].iteam
	ELSE
		FOR iteamloop = 0 TO ( g_FMMC_STRUCT.iNumberOfTeams - 1 )
			
			FMMC_CUTSCENE_TYPE eCutType
			IF iCutsceneIndexToUse[ iteamloop ] >= FMMC_GET_MAX_SCRIPTED_CUTSCENES()
			AND iCutsceneIndexToUse[ iteamloop ] < FMMC_GET_MAX_CUTSCENES_WITH_MOCAP()
				iCutsceneToUse = ( iCutsceneIndexToUse[ iteamloop ] - FMMC_GET_MAX_SCRIPTED_CUTSCENES() )
				eCutType = FMMCCUT_MOCAP
				IF iCutsceneToUse >= MAX_MOCAP_CUTSCENES
					RELOOP
				ENDIF
			ELSE
				iCutsceneToUse = iCutsceneIndexToUse[ iteamloop ]
				eCutType = FMMCCUT_SCRIPTED
				IF iCutsceneToUse >= FMMC_GET_MAX_SCRIPTED_CUTSCENES()
					RELOOP
				ENDIF
			ENDIF
			
			IF iCutsceneToUse > -1
			AND NOT IS_BIT_SET(MC_playerBD[iPartToUse].iCutsceneFinishedBitset, iCutsceneIndexToUse[iteamloop])
			
				IF SHOULD_LOCAL_PLAYER_WATCH_TEAM_CUTSCENE(iteamloop, iCutsceneToUse, eCutType)
					PRINTLN("[RCC MISSION] GET_CUTSCENE_TEAM - (4) Assiging Team here.")
					iteam = iteamloop
				ENDIF
			ENDIF
		ENDFOR
	ENDIF

	IF bFindStreamingTeam
	//	 CERRORLN( DEBUG_OWAIN, "GET_CUTSCENE_TEAM returning team: ", iteam )
	ENDIF
	
	// PRINTLN( "GET_CUTSCENE_TEAM returning team: ", iteam )

	RETURN iteam

ENDFUNC

/// PURPOSE:
///    Checking if we can remove masks for the given cutscene
/// PARAMS:
///    cutscene - cutscene name
///    bEarly - specifies that we're checking this on the frame the cutscene starts
/// RETURNS:
///    TRUE when you want to remove it for the cutscene at the specified point
FUNC BOOL CAN_REMOVE_MASKS_FOR_HEIST_CUTSCENE(STRING cutscene, BOOL bEarly = TRUE, FMMC_CUTSCENE_TYPE eCutType = FMMCCUT_MOCAP, INT iCutsceneToUse = -1)
	
	IF eCutType = FMMCCUT_MOCAP
		IF iCutsceneToUse > -1
			IF IS_BIT_SET(g_fmmc_struct.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_KeepHatsAndGlassesEtc)
				PRINTLN("[RCC MISSION] CAN_REMOVE_MASKS_FOR_HEIST_CUTSCENE - cutscene: ", cutscene, ", bEarly: ", bEarly, " FMMCCUT_MOCAP - ci_CSBS2_KeepHatsAndGlassesEtc is set RETURN FALSE")
				RETURN FALSE
			ENDIF
		ENDIF
	ELIF eCutType = FMMCCUT_ENDMOCAP
		IF IS_BIT_SET(g_fmmc_struct.sEndMocapSceneData.iCutsceneBitSet2, ci_CSBS2_KeepHatsAndGlassesEtc)
			PRINTLN("[RCC MISSION] CAN_REMOVE_MASKS_FOR_HEIST_CUTSCENE - cutscene: ", cutscene, ", bEarly: ", bEarly, " FMMCCUT_ENDMOCAP - ci_CSBS2_KeepHatsAndGlassesEtc is set RETURN FALSE")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF ARE_STRINGS_EQUAL(cutscene, "MPH_PRI_STA_MCS1") // players steal Rashkovski’s car from freight container
	OR ARE_STRINGS_EQUAL(cutscene, "MPH_HUM_FIN_MCS1") // players are in nightgear stealing computer data during humane labs heist
	OR ARE_STRINGS_EQUAL(cutscene, "MPH_PAC_FIN_MCS0") // players enter bank and put on masks, before robbery
	OR ARE_STRINGS_EQUAL(cutscene, "MPH_PAC_FIN_MCS1") // players see cops outside bank, and prepare to battle, after robbery
	OR ARE_STRINGS_EQUAL(cutscene, "MPH_PAC_HAC_MCS1") // player moves computer server between vans
	OR ARE_STRINGS_EQUAL(cutscene, "MPH_PAC_WIT_MCS1") //
	OR ARE_STRINGS_EQUAL(cutscene, "MPH_PAC_WIT_MCS1") 
	OR ARE_STRINGS_EQUAL(cutscene, "MPH_PAC_FIN_MCS2")
	OR ARE_STRINGS_EQUAL(cutscene, "MPH_PRI_UNF_MCS1")	//b*2133357
	//OR ARE_STRINGS_EQUAL(cutscene, "mph_pac_fin_ext") //AND GET_CUTSCENE_TIME() < 10000)//b*2166403
	OR ARE_STRINGS_EQUAL(cutscene, "MPH_PAC_BIK_EXT")
	OR ARE_STRINGS_EQUAL(cutscene, "LOW_DRV_EXT") //NOTE(Owain): all lowrider cutscenes leave your masks/hats on
	OR ARE_STRINGS_EQUAL(cutscene, "LOW_FIN_EXT")
	OR ARE_STRINGS_EQUAL(cutscene, "LOW_FIN_MCS1")
	OR ARE_STRINGS_EQUAL(cutscene, "LOW_FUN_EXT")
	OR ARE_STRINGS_EQUAL(cutscene, "LOW_FUN_MCS1")
	OR ARE_STRINGS_EQUAL(cutscene, "LOW_PHO_EXT")
	OR ARE_STRINGS_EQUAL(cutscene, "LOW_TRA_EXT")
	OR ARE_STRINGS_EQUAL(cutscene, "SILJ_MCS1")
	OR ARE_STRINGS_EQUAL(cutscene, "SIL_PRED_MCS1")
	OR ARE_STRINGS_EQUAL(cutscene, "SILJ_MCS2" )
	OR ARE_STRINGS_EQUAL(cutscene, "MPCAS1_EXT" )
	OR ARE_STRINGS_EQUAL(cutscene, "MPCAS3_EXT" )
	OR ARE_STRINGS_EQUAL(cutscene, "MPCAS2_EXT" )
	OR ARE_STRINGS_EQUAL(cutscene, "MPCAS5_EXT" )
	OR ARE_STRINGS_EQUAL(cutscene, "MPCAS6_EXT" )	
	OR ARE_STRINGS_EQUAL(cutscene, "HS3F_ALL_DRP1" )
	OR ARE_STRINGS_EQUAL(cutscene, "HS3F_ALL_DRP2" )
	OR ARE_STRINGS_EQUAL(cutscene, "HS3F_ALL_DRP3" )
	OR ARE_STRINGS_EQUAL(cutscene, "HS3F_ALL_ESC" )
	OR ARE_STRINGS_EQUAL(cutscene, "HS3F_MUL_RP1" )
	OR ARE_STRINGS_EQUAL(cutscene, "HS3F_DIR_ENT" )
	OR ARE_STRINGS_EQUAL(cutscene, "HS3F_SUB_VLT" )
	OR ARE_STRINGS_EQUAL(cutscene, "HS3F_DIR_SEW" )
	OR ARE_STRINGS_EQUAL(cutscene, "HS3F_SUB_CEL" )
	OR ARE_STRINGS_EQUAL(cutscene, "HS3F_ALL_ESC_B" )
		PRINTLN("[RCC MISSION] CAN_REMOVE_MASKS_FOR_HEIST_CUTSCENE - cutscene: ", cutscene, ", bEarly: ", bEarly, " RETURN FALSE")
		RETURN FALSE
	
	ELSE
	
		IF bEarly
		
			IF ARE_STRINGS_EQUAL(cutscene, "IAAJ_EXT")
				PRINTLN("[RCC MISSION] CAN_REMOVE_MASKS_FOR_HEIST_CUTSCENE - cutscene: ", cutscene, ", bEarly: ", bEarly, " RETURN FALSE")
				RETURN FALSE
			ENDIF
		
		ELSE
		ENDIF

	ENDIF
	
	PRINTLN("[RCC MISSION] CAN_REMOVE_MASKS_FOR_HEIST_CUTSCENE - cutscene: ", cutscene, ", bEarly: ", bEarly, " RETURN TRUE")
	RETURN TRUE

ENDFUNC

PROC REMOVE_MASKS_AND_REBREATHERS_FOR_CUTSCENES(PED_INDEX mocap_ped, PLAYER_INDEX PlayerIndex, MP_OUTFIT_MASK_ENUM eMask = OUTFIT_MASK_NONE)
	
	INT iPlayerIndex = NATIVE_TO_INT(PlayerIndex)
	
	IF IS_MP_HEIST_GEAR_EQUIPPED(mocap_ped, HEIST_GEAR_REBREATHER)
		REMOVE_MP_HEIST_GEAR(mocap_ped,HEIST_GEAR_REBREATHER,iPlayerIndex)
		PRINTLN("[RCC MISSION] HIDE_MASKS_AND_REBREATHERS_FOR_CUTSCENES take off masks 	REMOVE_MP_HEIST_GEAR(mocap_ped,HEIST_GEAR_REBREATHER)")
	ENDIF
	
	IF IS_MP_HEIST_GEAR_EQUIPPED(mocap_ped,  HEIST_GEAR_BALACLAVA)
		REMOVE_MP_HEIST_GEAR(mocap_ped,HEIST_GEAR_BALACLAVA,iPlayerIndex)
		PRINTLN("[RCC MISSION] HIDE_MASKS_AND_REBREATHERS_FOR_CUTSCENES take off masks 	REMOVE_MP_HEIST_GEAR(mocap_ped,HEIST_GEAR_BALACLAVA)")
	ENDIF
	
	IF IS_MP_HEIST_GEAR_EQUIPPED(mocap_ped,  HEIST_GEAR_GAS_MASK)
		REMOVE_MP_HEIST_GEAR(mocap_ped,HEIST_GEAR_GAS_MASK,iPlayerIndex)
		PRINTLN("[RCC MISSION] HIDE_MASKS_AND_REBREATHERS_FOR_CUTSCENES take off masks 	REMOVE_MP_HEIST_GEAR(mocap_ped,HEIST_GEAR_GAS_MASK)")
	ENDIF
	
	IF eMask != OUTFIT_MASK_NONE
		IF IS_MP_HEIST_GEAR_EQUIPPED(mocap_ped,  HEIST_GEAR_CORONA_MASK,eMask)
			REMOVE_MP_HEIST_GEAR(mocap_ped,HEIST_GEAR_CORONA_MASK,iPlayerIndex)
			PRINTLN("[RCC MISSION] HIDE_MASKS_AND_REBREATHERS_FOR_CUTSCENES take off masks 	REMOVE_MP_HEIST_GEAR(mocap_ped,HEIST_GEAR_CORONA_MASK)")
		ENDIF
	ELSE
		PRINTLN("[RCC MISSION] HIDE_MASKS_AND_REBREATHERS_FOR_CUTSCENES no mask set")
	ENDIF
	
	IF IS_MP_HEIST_GEAR_EQUIPPED(mocap_ped,  HEIST_GEAR_NIGHTVISION)
		REMOVE_MP_HEIST_GEAR(mocap_ped,HEIST_GEAR_NIGHTVISION,iPlayerIndex)
		PRINTLN("[RCC MISSION] HIDE_MASKS_AND_REBREATHERS_FOR_CUTSCENES take off masks 	REMOVE_MP_HEIST_GEAR(mocap_ped,HEIST_GEAR_NIGHTVISION)")
	ENDIF

	MP_OUTFITS_APPLY_DATA sApplyDataRemoveMask
	sApplyDataRemoveMask.pedID        	= mocap_ped
    sApplyDataRemoveMask.eMask        	= OUTFIT_MASK_NONE
    sApplyDataRemoveMask.eApplyStage  	= AOS_SET
	sApplyDataRemoveMask.iPlayerIndex  	= iPlayerIndex
	
	PRINTLN("[RCC MISSION] HIDE_MASKS_AND_REBREATHERS_FOR_CUTSCENES - calling SET_PED_MP_OUTFIT with OUTFIT_MASK_NONE")
	SET_PED_MP_OUTFIT(sApplyDataRemoveMask, TRUE, TRUE, FALSE)

ENDPROC

FUNC CUTSCENE_SECTION GET_CUTSCENE_SECTION_FLAGS(INT iCutsceneToUse)
	CUTSCENE_SECTION cutSectionFlags
	INT iBS = 0, iBit = 1
	FOR iBS = 0 TO 31
		IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[ iCutsceneToUse ].iVariationBS, iBS)
			cutSectionFlags = cutSectionFlags | INT_TO_ENUM(CUTSCENE_SECTION, iBit)
		ENDIF		
		iBit *= 2
	ENDFOR
	
	RETURN cutSectionFlags
ENDFUNC

//PROC RESTORE_MASKS_AND_REBREATHERS_FOR_CUTSCENES(MP_HEIST_GEAR_ENUM mask_or_rebreather)
//	
//	SWITCH mask_or_rebreather
//	
//		CASE HEIST_GEAR_REBREATHER
//		CASE HEIST_GEAR_BALACLAVA
//		CASE HEIST_GEAR_GAS_MASK
//		CASE HEIST_GEAR_NIGHTVISION
//		CASE HEIST_GEAR_CORONA_MASK
//			SET_MP_HEIST_GEAR(LocalPlayerPed, mask_or_rebreather)
//		
//			PRINTLN("[RCC MISSION] RESTORE_MASKS_AND_REBREATHERS_FOR_CUTSCENES mask_or_rebreather equiped = ", enum_to_int(mask_or_rebreather))
//		BREAK
//		
//		CASE HEIST_GEAR_NONE
//			PRINTLN("[RCC MISSION] RESTORE_MASKS_AND_REBREATHERS_FOR_CUTSCENES mask_or_rebreather = none do nothing")
//		BREAK
//		
//	ENDSWITCH
//		
//ENDPROC

FUNC STRING GET_MOCAP_STREAMING_STAGE_DEBUG_NAME(MOCAP_STREAMING_STAGE eState)
	SWITCH eState
		CASE MOCAP_STREAMING_STAGE_INIT							RETURN "MOCAP_STREAMING_STAGE_INIT" 
		CASE MOCAP_STREAMING_STAGE_REGISTER_ENTITIES			RETURN "MOCAP_STREAMING_STAGE_REGISTER_ENTITIES" 
		CASE MOCAP_STREAMING_STAGE_REQUEST_MODELS				RETURN "MOCAP_STREAMING_STAGE_REQUEST_MODELS" 
		CASE MOCAP_STREAMING_STAGE_ENDING						RETURN "MOCAP_STREAMING_STAGE_ENDING" 
	ENDSWITCH
	RETURN "?????"
ENDFUNC

PROC SET_MOCAP_STREAMING_STAGE(MOCAP_STREAMING_STAGE eNewState)
	PRINTLN("[LM][SET_MOCAP_STREAMING_STAGE] Moving From ", GET_MOCAP_STREAMING_STAGE_DEBUG_NAME(eMocapStreamingStage), " to >>>>>>>> ", GET_MOCAP_STREAMING_STAGE_DEBUG_NAME(eNewState))
	eMocapStreamingStage = eNewState
ENDPROC

// This function streams all mocap cutscenes, and returns TRUE when done, as well as
// SET_BIT( iLocalBoolCheck2, LBOOL2_CUTSCENE_STREAMED ), which is required to play a mocap
FUNC BOOL HANDLE_STREAMING_MOCAP( VECTOR vStreamPos, FLOAT fStreamInDistance, FLOAT fStreamOutDistance, FMMC_CUTSCENE_TYPE cutType)
	
	INT i
	INT iCutsceneToUse 
	IF g_iFMMCScriptedCutscenePlaying= -1
		INT iTeam = GET_CUTSCENE_TEAM( TRUE )
		IF iTeam > -1
			iCutsceneToUse = MC_serverBD.iCutsceneStreamingIndex[ iTeam ]
		ENDIF
	ELSE
		INT iTeam = GET_CUTSCENE_TEAM( FALSE )
		IF iTeam > -1
			iCutsceneToUse = MC_serverBD.iCutsceneID[ iTeam ]
		ENDIF
	ENDIF
	
	IF iCutsceneToUse < FMMC_GET_MAX_SCRIPTED_CUTSCENES() 
	AND cutType != FMMCCUT_ENDMOCAP
		PRINTLN("[RCC MISSION] HANDLE_STREAMING_MOCAP RETURN FALSE currently set to a scripted cutscene: ", iCutsceneToUse)
		RETURN FALSE
	ENDIF
	
	IF iCutsceneToUse >= FMMC_GET_MAX_SCRIPTED_CUTSCENES() 
	AND iCutsceneToUse < FMMC_GET_MAX_CUTSCENES_WITH_MOCAP() 
		iCutsceneToUse = ( iCutsceneToUse - FMMC_GET_MAX_SCRIPTED_CUTSCENES() )
	ENDIF
	
	IF g_iFMMCScriptedCutscenePlaying >= 0
	AND cutType != FMMCCUT_ENDMOCAP
		IF NOT IS_STRING_NULL_OR_EMPTY( g_FMMC_STRUCT.sMocapCutsceneData[ iCutsceneToUse ].tlName ) 
			IF IS_STRING_NULL_OR_EMPTY( sMocapCutscene )
				sMocapCutscene = Get_String_From_TextLabel( g_FMMC_STRUCT.sMocapCutsceneData[ iCutsceneToUse ].tlName )
				PRINTLN("[RCC MISSION] HANDLE_STREAMING_MOCAP SETTING sMocapCutscene to '", sMocapCutscene, "' - was empty!")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET( iLocalBoolCheck9, LBOOL9_FORCE_STREAM_CUTSCENE )
	AND NOT IS_BIT_SET( iLocalBoolCheck2, LBOOL2_CUTSCENE_STREAMED )
	AND NOT IS_CUTSCENE_ACTIVE() // Don't print this out if we're already streaming
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] HANDLE_STREAMING_MOCAP Force streaming ", sMocapCutscene )
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_PLACE_HOLDER
	AND cutType = FMMCCUT_ENDMOCAP
		SET_BIT( iLocalBoolCheck2, LBOOL2_CUTSCENE_STREAMED )
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] HANDLE_STREAMING_MOCAP MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_PLACE_HOLDER, setting LBOOL2_CUTSCENE_STREAMED" )
		RETURN TRUE
	ENDIF
	#ENDIF
		
	IF IS_STRING_NULL_OR_EMPTY( strPreviousMocapCutsceneName )
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] HANDLE_STREAMING_MOCAP strPrevousMocap is empty - setting. This will be the first time HANDLE_STREAMING_MOCAP is called." )
		strPreviousMocapCutsceneName = sMocapCutscene
	ENDIF
	
	IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
	AND IS_THIS_MOCAP_IN_AN_APARTMENT( sMocapCutscene )
		// Never stream this mocap if we're a spectator
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] HANDLE_STREAMING_MOCAP Returning False we are a spctator in apartment." )
		RETURN FALSE
	ENDIF
	
	INT iMyTeam = MC_playerBD[iPartToUse].iteam
		
	CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] HANDLE_STREAMING_MOCAP Calling function...   eMocapStreamingStage: ", GET_MOCAP_STREAMING_STAGE_DEBUG_NAME(eMocapStreamingStage))
	IF cutType = FMMCCUT_ENDMOCAP
		IF ( MC_serverBD.piCutscenePlayerIDs[ iMyTeam ][0] != INVALID_PLAYER_INDEX()
		AND LocalMainCutscenePlayer[ iMyTeam ] != INVALID_PLAYER_INDEX() )
		OR NOT ARE_STRINGS_EQUAL( strPreviousMocapCutsceneName, sMocapCutscene )
			IF LocalMainCutscenePlayer[ iMyTeam ] != MC_serverBD.piCutscenePlayerIDs[ iMyTeam ][0]
			OR NOT ARE_STRINGS_EQUAL( strPreviousMocapCutsceneName, sMocapCutscene )
				IF NOT IS_CUTSCENE_PLAYING()
				AND NOT IS_BIT_SET( iLocalBoolCheck5, LBOOL5_PED_INVOLVED_IN_CUTSCENE )
				AND NOT IS_BIT_SET( MC_playerBD[iLocalPart].iClientBitset2, PBBOOL2_PLAYING_MOCAP_CUTSCENE )
				AND NOT g_bMissionEnding
				AND GET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS() < MOCAPRUNNING_STAGE_1

					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] HANDLE_STREAMING_MOCAP Mocap reset: ", sMocapCutscene )
					CLEAR_BIT( iLocalBoolCheck2, LBOOL2_CUTSCENE_STREAMED )
					
					strPreviousMocapCutsceneName = NULL
					sMocapCutscene = NULL
					PRINTLN("[RCC MISSION] HANDLE_STREAMING_MOCAP SETTING sMocapCutscene to NULL - Mocap reset")
					
					iCutComponentPlayerNum = 0
					SET_MOCAP_STREAMING_STAGE(MOCAP_STREAMING_STAGE_INIT)
					LocalMainCutscenePlayer[ iMyTeam ] = MC_serverBD.piCutscenePlayerIDs[ iMyTeam ][0]
					
					REMOVE_WEAPON_ASSET( WEAPONTYPE_DLC_SPECIALCARBINE )
					REMOVE_CUTSCENE()
					SET_MODEL_AS_NO_LONGER_NEEDED( MP_F_FREEMODE_01 )
					SET_MODEL_AS_NO_LONGER_NEEDED( MP_M_FREEMODE_01 )
					SET_MODEL_AS_NO_LONGER_NEEDED( PROP_CS_CASHENVELOPE )
					NEW_LOAD_SCENE_STOP()
					
					IF IS_SRL_LOADED()
						END_SRL()
					ENDIF
					
					if IPL_GROUP_SWAP_IS_ACTIVE()
						IPL_GROUP_SWAP_CANCEL()
					endif 
					
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	VECTOR vStreamDistance = <<fStreamInDistance, fStreamInDistance, fStreamInDistance>>
	
	VECTOR vSpecStreamDistance = vStreamDistance * << 2, 2, 2 >>
	
	SWITCH eMocapStreamingStage
	
		CASE MOCAP_STREAMING_STAGE_INIT
						
			IF NOT HAS_CUTSCENE_LOADED()
			AND NOT IS_PLAYER_IN_ANY_SHOP()
				
				IF ( ( IS_ENTITY_AT_COORD( LocalPlayerPed, vStreamPos, vStreamDistance )
					OR IS_PROPERTY_DROP_COMPLETE() 
					OR ( IS_LOCAL_PLAYER_ANY_SPECTATOR() AND IS_ENTITY_AT_COORD( LocalPlayerPed, vStreamPos, vSpecStreamDistance ) ) // Give spectator more leeway with streaming
					AND SHOULD_STREAM_APARTMENT_CUTSCENE( sMocapCutscene )) //AND AM_I_ON_A_HEIST() ) 	// and we actually do want them to see this mocap //b*2204934 - Prevents non-heist missions from playing cutscene
				
				OR IS_BIT_SET( iLocalBoolCheck2, LBOOL2_CUTSCENE_WARP_DONE )				// Warped to a cutscene location
				OR IS_BIT_SET( iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE_WARP ) )	// End mocap request
				OR IS_BIT_SET( iLocalBoolCheck9, LBOOL9_FORCE_STREAM_CUTSCENE ) 			// Panic stream the cutscene
				
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] HANDLE_STREAMING_MOCAP Starting a stream for mocap ", sMocapCutscene )
					
					LocalMainCutscenePlayer[ iMyTeam ] = MC_serverBD.piCutscenePlayerIDs[ iMyTeam ][0]
				
					IF ARE_STRINGS_EQUAL(sMocapCutscene, "mph_pac_fin_mcs0")
					OR ARE_STRINGS_EQUAL(sMocapCutscene, "mph_pac_fin_mcs1")
						REQUEST_WEAPON_ASSET(WEAPONTYPE_DLC_SPECIALCARBINE, ENUM_TO_INT(WRF_REQUEST_ALL_ANIMS) + ENUM_TO_INT(WRF_REQUEST_ALL_MOVEMENT_VARIATION_ANIMS), WEAPON_COMPONENT_FLASH | WEAPON_COMPONENT_GRIP | WEAPON_COMPONENT_SCLIP2| WEAPON_COMPONENT_SCOPE | WEAPON_COMPONENT_SUPP)						
					ENDIF
					
					//Special case for 	"mph_pac_fin_mcs1"				
					IF ARE_STRINGS_EQUAL(sMocapCutscene, "mph_pac_fin_mcs1")
						IF (MC_serverBD.iNextMission = 1)
							//Include swat
							PRINTLN("[RCC MISSION] [ROSSW] HANDLE_STREAMING_MOCAP streaming whole cutscene as SWAT are coming")
							REQUEST_CUTSCENE( sMocapCutscene )
						ELSE
							//Miss out swat
							PRINTLN("[RCC MISSION] [ROSSW] HANDLE_STREAMING_MOCAP streaming sectioned cut as SWAT are not coming")
							REQUEST_CUTSCENE_WITH_PLAYBACK_LIST( sMocapCutscene, CS_SECTION_1 | CS_SECTION_2 | CS_SECTION_3 | CS_SECTION_5 )
						ENDIF		
					ELIF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_TUT_EXT")
						PREFETCH_SRL("MPH_TUT_EXT_SRL")
						REQUEST_CUTSCENE(sMocapCutscene)
						PRINTLN("[RCC MISSION] HANDLE_STREAMING_MOCAP - PREFETCH_SRL  MPH_TUT_EXT_SRL called for MPH_TUT_EXT  ")
					ELIF cutType = FMMCCUT_MOCAP
					AND g_FMMC_STRUCT.sMocapCutsceneData[ iCutsceneToUse ].iVariationBS != 0
						CUTSCENE_SECTION cutSectionFlags
						cutSectionFlags = GET_CUTSCENE_SECTION_FLAGS(iCutsceneToUse)						
						PRINTLN("[RCC MISSION] HANDLE_STREAMING_MOCAP - REQUEST_CUTSCENE_WITH_PLAYBACK_LIST (1) with creator cutscene section flags: ", ENUM_TO_INT(cutSectionFlags))						
						REQUEST_CUTSCENE_WITH_PLAYBACK_LIST( sMocapCutscene, cutSectionFlags)
					ELSE
						//request all other cutscens normally.
						REQUEST_CUTSCENE( sMocapCutscene )
					ENDIF
				
				
					REQUEST_MODEL(MP_F_FREEMODE_01)
					REQUEST_MODEL(MP_M_FREEMODE_01)
					
					switch MC_ServerBD.iEndCutscene
					
						case ciMISSION_CUTSCENE_MADRAZO
						
							request_model( PROP_CS_CASHENVELOPE )
						
						break 
						
						case ciMISSION_CUTSCENE_TREVOR
						
							IF NOT IS_IPL_ACTIVE("TrevorsMP")
								IF IS_IPL_ACTIVE("TrevorsTrailer")
									IPL_GROUP_SWAP_START("TrevorsTrailer", "TrevorsMP")
								ELSE
									REQUEST_IPL("TrevorsMP")
								ENDIF
							ENDIF
						
						break 
						
						case ciMISSION_CUTSCENE_GERALD
						
						break 
						
						case ciMISSION_CUTSCENE_SIMEON
						
						
							NEW_LOAD_SCENE_START(<<-28.2, -1082.7, 27.00>>, CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<0.0, 0.0, 159.4248>>), 13.00)//22.00

						break 
						
						// removed extra cases since special case not needed for other cutscenes
						
					endswitch 

					PRINTLN("[RCC MISSION] HANDLE_STREAMING_MOCAP PRE-STREAMING MOCAP: ", sMocapCutscene)
					
					SET_MOCAP_STREAMING_STAGE(MOCAP_STREAMING_STAGE_REGISTER_ENTITIES)
				#IF IS_DEBUG_BUILD
				ELSE
					IF NOT IS_ENTITY_AT_COORD( LocalPlayerPed, vStreamPos, <<fStreamInDistance, fStreamInDistance, fStreamInDistance>> )
						PRINTLN("[RCC MISSION] HANDLE_STREAMING_MOCAP: NOT IS_ENTITY_AT_COORD( -waiting")
					ENDIF
					IF NOT IS_PROPERTY_DROP_COMPLETE()
						PRINTLN("[RCC MISSION] HANDLE_STREAMING_MOCAP: NOT IS_PROPERTY_DROP_COMPLETE -waiting")
					ENDIF
					IF NOT SHOULD_STREAM_APARTMENT_CUTSCENE( sMocapCutscene ) 
						PRINTLN("[RCC MISSION] HANDLE_STREAMING_MOCAP: NOT SHOULD_SEE_APARTMENT_CUTSCENE -waiting")
					ENDIF
					IF NOT IS_BIT_SET( iLocalBoolCheck2, LBOOL2_ACTIVATE_MOCAP_CUTSCENE_WARP )
						PRINTLN("[RCC MISSION] HANDLE_STREAMING_MOCAP: NOT LBOOL2_ACTIVATE_MOCAP_CUTSCENE_WARP -waiting")
					ENDIF
					IF NOT IS_BIT_SET( iLocalBoolCheck9, LBOOL9_FORCE_STREAM_CUTSCENE )
						PRINTLN("[RCC MISSION] HANDLE_STREAMING_MOCAP: NOT LBOOL9_FORCE_STREAM_CUTSCENE -waiting")
					ENDIF
				#ENDIF
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				IF HAS_CUTSCENE_LOADED()
					PRINTLN("[RCC MISSION] HANDLE_STREAMING_MOCAP: HAS_CUTSCENE_LOADED -waiting")
				ENDIF
				IF IS_PLAYER_IN_ANY_SHOP()
					PRINTLN("[RCC MISSION] HANDLE_STREAMING_MOCAP: IS_PLAYER_IN_ANY_SHOP -waiting - g_bKickPlayerOutOfShop = TRUE")
					g_bKickPlayerOutOfShop = TRUE
				ENDIF
			#ENDIF
			ENDIF
			 
		BREAK

		CASE MOCAP_STREAMING_STAGE_REGISTER_ENTITIES
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()

				SETUP_CUTSCENE_PLAYERS_COMPONENTS()
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] HANDLE_STREAMING_MOCAP MOCAP Moving to stage 2: ", sMocapCutscene )
				
				CPRINTLN( DEBUG_CONTROLLER,  "[RCC MISSION] HANDLE_STREAMING_MOCAP iCutsceneToUse = ", iCutsceneToUse) 
				
				//Make sure ped component variations are streamed.
				REPEAT FMMC_MAX_CUTSCENE_ENTITIES i
					IF iCutsceneToUse != -1 // mid mission mocap 
					AND cutType != FMMCCUT_ENDMOCAP	
						IF g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sCutsceneEntities[i].iType = CREATION_TYPE_PEDS
							IF g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sCutsceneEntities[i].iIndex != -1
								STRING handle 
								handle = TEXT_LABEL_TO_STRING( g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sCutsceneEntities[i].tlCutsceneHandle)
								SET_CUTSCENE_ENTITY_STREAMING_FLAGS(handle, DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
								
								// WTF?!? - eiCutsceneEntity - you cant pull a fucking entity index from cloud data
								//SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED(handle, GET_PED_INDEX_FROM_ENTITY_INDEX(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sCutsceneEntities[i].eiCutsceneEntity))						
																
								CPRINTLN(DEBUG_CONTROLLER, "[RCC MISSION] HANDLE_STREAMING_MOCAP ped ", i, " for cutscene ", iCutsceneToUse, " ped index is ", g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sCutsceneEntities[i].iIndex, " Calling SET_CUTSCENE_ENTITY_STREAMING_FLAGS with CES_DONT_STREAM_AND_APPLY_VARIATIONS, handle: ", handle)
							ENDIF
						ENDIF
						
						CPRINTLN(DEBUG_CONTROLLER, "[RCC MISSION] HANDLE_STREAMING_MOCAP Entity ", i, " type : ", g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sCutsceneEntities[i].iType, ", index: ", g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sCutsceneEntities[i].iIndex)
						
					ELSE // end of mission mocap
						IF NOT IS_BIT_SET( iLocalBoolCheck5, LBOOL5_MID_MISSION_MOCAP ) 
							IF g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iType = CREATION_TYPE_PEDS
							AND g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iIndex != -1						
								STRING handle 
								handle = TEXT_LABEL_TO_STRING(g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].tlCutsceneHandle)
								SET_CUTSCENE_ENTITY_STREAMING_FLAGS(handle, DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
								
								// WTF?!? - eiCutsceneEntity - you cant pull a fucking entity index from cloud data
								//SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED(g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].tlCutsceneHandle, GET_PED_INDEX_FROM_ENTITY_INDEX(g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].eiCutsceneEntity))						
								
								CPRINTLN(DEBUG_CONTROLLER, "[RCC MISSION] HANDLE_STREAMING_MOCAP ped ", i, " for cutscene ", iCutsceneToUse, " ped index is ", g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iIndex, " Calling SET_CUTSCENE_ENTITY_STREAMING_FLAGS with CES_DONT_STREAM_AND_APPLY_VARIATIONS, handle: ", handle)
							ENDIF
						ELSE
							CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] HANDLE_STREAMING_MOCAP iCutsceneTouse is -1 and we're not an end of mission cutscene. Bad times." )
						ENDIF
					ENDIF
				ENDREPEAT
				
				PRINTLN("[RCC MISSION] HANDLE_STREAMING_MOCAP MOCAP Moving to stage 2: ",sMocapCutscene)
				SET_MOCAP_STREAMING_STAGE(MOCAP_STREAMING_STAGE_REQUEST_MODELS)
			ELSE
				PRINTLN("[RCC MISSION] HANDLE_STREAMING_MOCAP waiting for CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY: ",sMocapCutscene)
			ENDIF
		BREAK
	
		CASE MOCAP_STREAMING_STAGE_REQUEST_MODELS
			IF HAS_CUTSCENE_LOADED()
			AND HAS_MODEL_LOADED( MP_F_FREEMODE_01 )
			AND HAS_MODEL_LOADED( MP_M_FREEMODE_01 )
			and ( ( MC_ServerBD.iEndCutscene != ciMISSION_CUTSCENE_MADRAZO ) or ( has_model_loaded( PROP_CS_CASHENVELOPE ) ) )		
			//AND LOAD_APARTMENT_INTERIOR_FOR_SPECTATOR( sMocapCutscene )
			AND LOAD_INTERIOR_FOR_SPECTATOR_MOCAP(sMocapCutscene)
			
			 	IF ARE_STRINGS_EQUAL( sMocapCutscene, "mph_pac_fin_mcs0" ) 
				OR ARE_STRINGS_EQUAL( sMocapCutscene, "mph_pac_fin_mcs1" )
					IF HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_DLC_SPECIALCARBINE)
						CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] HANDLE_STREAMING_MOCAP - MOCAP AND WEAPONS ARE STREAMED AND READY: ",sMocapCutscene)
						SET_MOCAP_STREAMING_STAGE(MOCAP_STREAMING_STAGE_ENDING)
					ENDIF
				ELIF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_TUT_EXT") 
					IF IS_SRL_LOADED()
						CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] HANDLE_STREAMING_MOCAP - MOCAP IS STREAMED AND READY: ",sMocapCutscene)
						SET_MOCAP_STREAMING_STAGE(MOCAP_STREAMING_STAGE_ENDING)	
					ELSE
						PRINTLN( "[RCC MISSION] HANDLE_STREAMING_MOCAP - IS_SRL_LOADED  MPH_TUT_EXT_SRL FALSE!!! ")
					ENDIF
				ELSE
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] HANDLE_STREAMING_MOCAP - MOCAP IS STREAMED AND READY: ",sMocapCutscene)
					SET_MOCAP_STREAMING_STAGE(MOCAP_STREAMING_STAGE_ENDING)
				ENDIF
			ELSE
				IF cutType = FMMCCUT_MOCAP
				AND g_FMMC_STRUCT.sMocapCutsceneData[ iCutsceneToUse ].iVariationBS != 0
					CUTSCENE_SECTION cutSectionFlags
					cutSectionFlags = GET_CUTSCENE_SECTION_FLAGS(iCutsceneToUse)						
					PRINTLN("[RCC MISSION] HANDLE_STREAMING_MOCAP - REQUEST_CUTSCENE_WITH_PLAYBACK_LIST (2) with creator cutscene section flags: ", ENUM_TO_INT(cutSectionFlags))						
					REQUEST_CUTSCENE_WITH_PLAYBACK_LIST( sMocapCutscene, cutSectionFlags)
				ELSE
					REQUEST_CUTSCENE( sMocapCutscene )
				ENDIF
								
				REQUEST_MODEL( MP_F_FREEMODE_01 )
				REQUEST_MODEL( MP_M_FREEMODE_01 )
				IF MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_MADRAZO
					REQUEST_MODEL( PROP_CS_CASHENVELOPE )
				ENDIF
				
				IF ARE_STRINGS_EQUAL( sMocapCutscene, "mph_pac_fin_mcs0" ) 
				OR ARE_STRINGS_EQUAL( sMocapCutscene, "mph_pac_fin_mcs1" )
					EXTRA_WEAPON_COMPONENT_RESOURCE_FLAGS weaponFlags
					
					weaponFlags |= WEAPON_COMPONENT_FLASH 
					weaponFlags |= WEAPON_COMPONENT_GRIP 
					weaponFlags |= WEAPON_COMPONENT_SCLIP2 
					weaponFlags |= WEAPON_COMPONENT_SCOPE 
					weaponFlags |= WEAPON_COMPONENT_SUPP 
					
					REQUEST_WEAPON_ASSET( WEAPONTYPE_DLC_SPECIALCARBINE, ENUM_TO_INT( WRF_REQUEST_ALL_ANIMS ), weaponFlags )
				ENDIF
				
				#IF IS_DEBUG_BUILD
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] HANDLE_STREAMING_MOCAP - CUTSCENE HASNT LOADED!")
				IF NOT HAS_CUTSCENE_LOADED() // HAS_THIS_CUTSCENE_LOADED(sMocapCutscene)
					CPRINTLN( DEBUG_CONTROLLER,"[RCC MISSION] HANDLE_STREAMING_MOCAP -  HAS_THIS_CUTSCENE_LOADED = FALSE. Mocap = ",sMocapCutscene)
				ENDIF
				IF NOT HAS_MODEL_LOADED( MP_F_FREEMODE_01 )
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] HANDLE_STREAMING_MOCAP - HAS_MODEL_LOADED(MP_F_FREEMODE_01) = FALSE.")
				ENDIF
				IF NOT HAS_MODEL_LOADED( MP_M_FREEMODE_01 )
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] HANDLE_STREAMING_MOCAP - HAS_MODEL_LOADED(MP_M_FREEMODE_01) = FALSE.")
				ENDIF
				IF ( MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_MADRAZO )
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] HANDLE_STREAMING_MOCAP - MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_MADRAZO.")
				ENDIF
				
				IF ( MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_MADRAZO )
					IF NOT has_model_loaded( PROP_CS_CASHENVELOPE )
						CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] HANDLE_STREAMING_MOCAP - has_model_loaded(PROP_CS_CASHENVELOPE) = FALSE.")
					ENDIF
				ENDIF
				#ENDIF
				
			ENDIF
		BREAK
		
		CASE MOCAP_STREAMING_STAGE_ENDING
			IF ( NOT IS_ENTITY_AT_COORD( LocalPlayerPed, vStreamPos, <<fStreamOutDistance, fStreamOutDistance, fStreamOutDistance>> )
				OR IS_PLAYER_IN_ANY_SHOP() )
			AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
			AND NOT IS_CUTSCENE_PLAYING()
			AND NOT IS_BIT_SET( iLocalBoolCheck7, LBOOL7_SHOULD_HOLD_UP_MOCAP_FOR_CUTSCENE )
			AND NOT IS_BIT_SET( iLocalBoolCheck5, LBOOL5_PED_INVOLVED_IN_CUTSCENE )
			AND NOT IS_BIT_SET( MC_playerBD[iLocalPart].iClientBitset2, PBBOOL2_PLAYING_MOCAP_CUTSCENE )
			AND NOT g_bMissionEnding
			AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bPlayingPropertyTransitionCutscene)
			
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] HANDLE_STREAMING_MOCAP - unloading ", sMocapCutscene )
				
				IF NOT IS_ENTITY_AT_COORD( LocalPlayerPed, vStreamPos, <<fStreamOutDistance, fStreamOutDistance, fStreamOutDistance>> )
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] HANDLE_STREAMING_MOCAP - unloading cutscene because we're too far away." )
				ENDIF
				
				IF IS_PLAYER_IN_ANY_SHOP()
					CPRINTLN( DEBUG_CONTROLLER,"[RCC MISSION] HANDLE_STREAMING_MOCAP - unloading cutscene because we're in a shop." )
				ENDIF

				IF IS_SRL_LOADED()
					END_SRL()
				ENDIF
				
				REMOVE_CUTSCENE()
				
				REMOVE_WEAPON_ASSET( WEAPONTYPE_DLC_SPECIALCARBINE )
				SET_MODEL_AS_NO_LONGER_NEEDED( MP_F_FREEMODE_01 )
				SET_MODEL_AS_NO_LONGER_NEEDED( MP_M_FREEMODE_01 )
				SET_MODEL_AS_NO_LONGER_NEEDED( PROP_CS_CASHENVELOPE )

				IF MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_SIMEON
					NEW_LOAD_SCENE_STOP()
				ENDIF
				
				CLEAR_BIT( iLocalBoolCheck2, LBOOL2_CUTSCENE_STREAMED )
				
				iCutComponentPlayerNum = 0
				SET_MOCAP_STREAMING_STAGE(MOCAP_STREAMING_STAGE_INIT)
				sMocapCutscene = ""
				PRINTLN("[RCC MISSION] HANDLE_STREAMING_MOCAP SETTING sMocapCutscene to \"\"(EMPTY) - unloading cutscene")
			ELSE
				IF NOT IS_BIT_SET( iLocalBoolCheck2, LBOOL2_CUTSCENE_STREAMED )	
					SET_BIT( iLocalBoolCheck2, LBOOL2_CUTSCENE_STREAMED )	
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] HANDLE_STREAMING_MOCAP Mocap has finished streaming: ", sMocapCutscene )
				ENDIF
				
				IF MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_SIMEON
					NEW_LOAD_SCENE_STOP()
				ENDIF
				
				IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bPlayingPropertyTransitionCutscene) 
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] HANDLE_STREAMING_MOCAP GlobalPlayerBroadcastDataFM_BS_bPlayingPropertyTransitionCutscene can't cleanup" )
				ENDIF
				
				RETURN TRUE
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

///PURPOSE: This function picks the closest of 5 possible cutscene locations
///    for the cutscene MPH_PRI_FIN_EXT then plays it 
FUNC VECTOR PICK_CLOSEST_PRISON_FINALE_CUTSCENE_LOCATION_AND_PLAY(BOOL bPlay = TRUE)
	VECTOR vPlayerLoc = GET_ENTITY_COORDS( LocalPlayerPed ) 

	VECTOR vCutsceneOffsets[ 5 ]
	FLOAT fCutsceneHeadings[ 5 ]
	
	vCutsceneOffsets[ 0 ] = << 1928.7, -2154, 181.75 >>
	vCutsceneOffsets[ 1 ] = << 2001.5, -813.2, 181.75 >>
	vCutsceneOffsets[ 2 ] = << 1404.2, 801.5, 287.5 >>
	vCutsceneOffsets[ 3 ] = << 253.95, 1292.7, 265.75 >>
	vCutsceneOffsets[ 4 ] = << -1414.0, 989.0, 274.0 >>
	
	fCutsceneHeadings[ 0 ] = 59.5
	fCutsceneHeadings[ 1 ] = 91.0
	fCutsceneHeadings[ 2 ] = 129.5
	fCutsceneHeadings[ 3 ] = 179.0
	fCutsceneHeadings[ 4 ] = 221.0
	
	VECTOR vDefault = << 1928.7, -2154, 181.75 >>
	VECTOR vOffsetToChoose = vDefault
	FLOAT fHeadingToChoose = 59.5
	
	INT iMaxOffsets = 5
	
	INT iLoop = 0
	
	FOR iLoop = 0 TO iMaxOffsets - 1
		IF GET_DISTANCE_BETWEEN_COORDS( vPlayerLoc, vCutsceneOffsets[ iLoop ] ) < GET_DISTANCE_BETWEEN_COORDS( vPlayerLoc, vOffsetToChoose ) 
			vOffsetToChoose = vCutsceneOffsets[ iLoop ]
			fHeadingToChoose = fCutsceneHeadings[ iLoop ]
		ENDIF
	ENDFOR
	
	PRINTLN("[RCC MISSION] PICK_CLOSEST_PRISON_FINALE_CUTSCENE_LOCATION_AND_PLAY returning: ",vOffsetToChoose)
	
	IF bPlay
		start_cutscene()
		SET_CUTSCENE_ORIGIN( vOffsetToChoose, fHeadingToChoose, 0 )
		PRINTLN("[RCC MISSION] PICK_CLOSEST_PRISON_FINALE_CUTSCENE_LOCATION_AND_PLAY Starting cutscene ")
	ENDIF
	
	RETURN vOffsetToChoose
	
ENDFUNC

///PURPOSE: This function picks the closest of 5 possible cutscene locations
///    for the cutscene MPH_TUT_EXT then plays it 
FUNC VECTOR PICK_CLOSEST_FLEECA_FINALE_CUTSCENE_LOCATION_AND_PLAY(BOOL bPlay = TRUE)
	
	VECTOR vPlayerLoc = GET_ENTITY_COORDS( LocalPlayerPed ) 

	VECTOR vCutsceneOffsets[ 7 ]
	FLOAT fCutsceneHeadings[ 7 ]
	
	vCutsceneOffsets[ 0 ] = <<-2693.6497, 2391.2339, 16.9>>
	vCutsceneOffsets[ 1 ] = <<-2679.8584, 2485.5493, 16.9>>
	vCutsceneOffsets[ 2 ] = <<-2666.9417, 2573.9192, 16.9>>
	vCutsceneOffsets[ 3 ] = <<-2656.1257, 2647.8506, 16.9>>
	vCutsceneOffsets[ 4 ] = <<-2647.9058, 2704.8296, 16.9>>
	vCutsceneOffsets[ 5 ] = <<-2630.2793, 2824.1274, 16.9>>
	vCutsceneOffsets[ 6 ] = <<-2616.1500, 2887.0500, 16.9>>
	
	fCutsceneHeadings[ 0 ] = 360.0
	fCutsceneHeadings[ 1 ] = 360.0
	fCutsceneHeadings[ 2 ] = 360.0
	fCutsceneHeadings[ 3 ] = 360.0
	fCutsceneHeadings[ 4 ] = 360.0
	fCutsceneHeadings[ 5 ] = 360.0
	fCutsceneHeadings[ 6 ] = 360.0
	
	VECTOR vDefault = <<-2652.8,2668.5,16.9>>
	VECTOR vOffsetToChoose = vDefault
	FLOAT fHeadingToChoose =  360.0 //353.3136
	
	INT iMaxOffsets = 7
	
	INT iLoop = 0
	
	FOR iLoop = 0 TO iMaxOffsets - 1
		IF GET_DISTANCE_BETWEEN_COORDS( vPlayerLoc, vCutsceneOffsets[ iLoop ] ) < GET_DISTANCE_BETWEEN_COORDS( vPlayerLoc, vOffsetToChoose ) 
			vOffsetToChoose = vCutsceneOffsets[ iLoop ]
			fHeadingToChoose = fCutsceneHeadings[ iLoop ]
		ENDIF
	ENDFOR
	
	IF bPlay
		start_cutscene()
		SET_CUTSCENE_ORIGIN( vOffsetToChoose, fHeadingToChoose, 0 )
		
		CASCADE_SHADOWS_SET_CASCADE_BOUNDS( 0, TRUE, vOffsetToChoose.x, vOffsetToChoose.y, vOffsetToChoose.z, 50.0)
		CASCADE_SHADOWS_SET_CASCADE_BOUNDS( 1, TRUE, vOffsetToChoose.x, vOffsetToChoose.y, vOffsetToChoose.z, 100.0)
		CASCADE_SHADOWS_SET_CASCADE_BOUNDS( 2, TRUE, vOffsetToChoose.x, vOffsetToChoose.y, vOffsetToChoose.z, 200.0)
		CASCADE_SHADOWS_SET_CASCADE_BOUNDS( 3, TRUE, vOffsetToChoose.x, vOffsetToChoose.y, vOffsetToChoose.z, 400.0)
	ENDIF
	
	RETURN vOffsetToChoose
	
ENDFUNC

FUNC BOOL IS_THIS_MID_MISSION_MOCAP_IN_AN_APARTMENT( STRING strMocapName )
	BOOL bReturn = FALSE

	IF ARE_STRINGS_EQUAL( strMocapName, "MPH_PRI_STA_EXT" ) // Station
	OR ARE_STRINGS_EQUAL( strMocapName, "MPH_PRI_UNF_EXT" ) // Unfinished Buisiness
		bReturn = TRUE
	ENDIF

	RETURN bReturn
ENDFUNC

//Check if we should return the passenger for a cutscene for the handle
FUNC BOOL SHOULD_CUTSCENE_GET_PASSENGER(FMMC_CUTSCENE_TYPE cutType, INT iCutsceneToUse)
	
	SWITCH cutType

		CASE FMMCCUT_MOCAP
		
			//Get the mid mission mocap name
			TEXT_LABEL_63 sMocapName
			IF NOT IS_STRING_NULL_OR_EMPTY( g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].tlName )
				sMocapName = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].tlName
			ENDIF
			
			IF ARE_STRINGS_EQUAL(sMocapName, "MPH_PRI_FIN_MCS2")
				PRINTLN("[RCC MISSION] SHOULD_CUTSCENE_GET_PASSENGER - MPH_PRI_FIN_MCS2 - RETURNING FALSE")
				RETURN FALSE
			ENDIF
			
		BREAK
	
	ENDSWITCH
	
	PRINTLN("[RCC MISSION] SHOULD_CUTSCENE_GET_PASSENGER - RETURNING TRUE")
	RETURN TRUE
ENDFUNC

//Get the end cutscene vehicle
FUNC STRING GET_CUTSCENE_VEHICLE_STRING(FMMC_CUTSCENE_TYPE cutType, INT iCutsceneToUse)

	SWITCH cutType
	
		CASE FMMCCUT_ENDMOCAP
		
			SWITCH MC_ServerBD.iEndCutscene
				CASE ciMISSION_CUTSCENE_HUM_EMP_EXT		RETURN "hydra"
				CASE ciMISSION_CUTSCENE_PAC_CON_EXT 	RETURN "TRUCK"
				CASE ciMISSION_CUTSCENE_PRI_BUS_EXT		RETURN "BUS"
				CASE ciMISSION_CUTSCENE_PAC_PHO_EXT		RETURN "Van"
				CASE ciMISSION_CUTSCENE_PAC_BIK_EXT		RETURN "ANY"		//Override to get the driver of any vehcile on the registered entites list
				CASE ciMISSION_CUTSCENE_HUM_FIN_EXT		RETURN "valkyrie"
				CASE ciMISSION_CUTSCENE_TUT_CAR_EXT		RETURN "MPH_Car"
			ENDSWITCH
		
		BREAK
		
		CASE FMMCCUT_MOCAP
		
			//Get the mid mission mocap name
			TEXT_LABEL_63 sMocapName
			IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].tlName)
				sMocapName = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].tlName
			ENDIF
			
			IF ARE_STRINGS_EQUAL(sMocapName, "MPH_PAC_HAC_MCS1")
				RETURN "Server_Van2"
			ENDIF
			
			IF ARE_STRINGS_EQUAL(sMocapName, "MPH_PRI_FIN_MCS2")
				RETURN "PLANE"
			ENDIF

		BREAK
		
		CASE FMMCCUT_SCRIPTED	RETURN "ANY"
	
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC VECTOR GET_CUTSCENE_COORDINATE_FROM_LAST_LOCATION(INT iLocation)
	IF iLocation > -1
		RETURN g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].sWarpLocationSettings.vPosition[0]	
	ENDIF
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

///purpose: obtains the coords for the teams mocap. 
FUNC VECTOR GET_CUTSCENE_COORDS(int iteam, FMMC_CUTSCENE_TYPE cutType = FMMCCUT_ENDMOCAP)

	VECTOR vReturn = <<0,0,0>>
	INT iCutsceneToUse = -1
	INT theApartment = -1
	FLOAT fGroundZ
	SWITCH cutType
	
		CASE FMMCCUT_SCRIPTED
		
			IF iteam < 0
			OR iTeam >= FMMC_MAX_TEAMS
				ASSERTLN("GET_CUTSCENE_COORDS - iTeam: ", iTeam, ", cutType: ", cutType," - iTeam is out of range, returning <<0,0,0>>")
				RETURN <<0,0,0>>
			ENDIF
			
			iCutsceneToUse = MC_serverBD.iCutsceneID[iteam]
			
			IF iCutsceneToUse < 0 
			OR iCutsceneToUse >= FMMC_GET_MAX_SCRIPTED_CUTSCENES()
				ASSERTLN("GET_CUTSCENE_COORDS - iTeam: ", iTeam, ", cutType: ", cutType," - iCutsceneToUse: ", iCutsceneToUse, " is out of range, returning <<0,0,0>>")
				RETURN <<0,0,0>>
			ENDIF

			vReturn = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].vPlayerStartPos[0]
			BREAK
		
		CASE FMMCCUT_MOCAP
			
			IF iteam < 0
			OR iTeam >= FMMC_MAX_TEAMS
				ASSERTLN("GET_CUTSCENE_COORDS - iTeam: ", iTeam, ", cutType: ", cutType," - iTeam is out of range, returning <<0,0,0>>")
				RETURN <<0,0,0>>
			ENDIF
			
			//Pre-Streamed Mocap
			IF MC_serverBD.iCutsceneStreamingIndex[iteam] >= FMMC_GET_MAX_SCRIPTED_CUTSCENES()
			AND MC_serverBD.iCutsceneStreamingIndex[iteam] < FMMC_GET_MAX_CUTSCENES_WITH_MOCAP()
			
				iCutsceneToUse = MC_serverBD.iCutsceneStreamingIndex[iteam] - FMMC_GET_MAX_SCRIPTED_CUTSCENES()
	
			// Mocap cutscene
			ELIF MC_serverBD.iCutsceneID[iteam] >= FMMC_GET_MAX_SCRIPTED_CUTSCENES()
			AND MC_serverBD.iCutsceneID[iteam] < FMMC_GET_MAX_CUTSCENES_WITH_MOCAP()
				iCutsceneToUse = MC_serverBD.iCutsceneID[iteam] - FMMC_GET_MAX_SCRIPTED_CUTSCENES()
			ENDIF
			
			IF iCutsceneToUse < 0 
			OR iCutsceneToUse >= MAX_MOCAP_CUTSCENES
				ASSERTLN("GET_CUTSCENE_COORDS - iTeam: ", iTeam, ", cutType: ", cutType," - iCutsceneToUse: ", iCutsceneToUse, " is out of range, returning <<0,0,0>>")
				RETURN <<0,0,0>>
			ENDIF

			IF IS_THIS_MOCAP_IN_AN_APARTMENT(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].tlName)
				vReturn = GET_PROPERTY_DROP_OFF_COORDS(eApartmentBuzzerEntrance, TRUE)
			ELSE
				vReturn = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].vPlayerStartPos[0]
			ENDIF
			
			IF IS_VECTOR_ZERO(vReturn)
				IF ARE_STRINGS_EQUAL(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].tlName, "MPH_PRI_FIN_MCS2" )// when player is flying in the plane
					vReturn = GET_ENTITY_COORDS(LocalPlayerPed)
				ENDIF
			ENDIF
			
		BREAK
		
		CASE FMMCCUT_ENDMOCAP
		
			SWITCH MC_ServerBD.iEndCutscene
				CASE ciMISSION_CUTSCENE_GERALD
					vReturn=<<-61.1,-1529.2,34.2 >>
				BREAK
				CASE ciMISSION_CUTSCENE_MADRAZO
					vReturn=<<-1035.2,687.8,161.3>>
				BREAK
				CASE ciMISSION_CUTSCENE_TREVOR
					vReturn=<<1975.7,3826.3,32.4>>
				BREAK
				CASE ciMISSION_CUTSCENE_SIMEON
					vReturn=<<-28.9,-1086.4,26.6>>
				BREAK
				
				CASE ciMISSION_CUTSCENE_HEIST
					GET_APARTMENT_INTERIOR_I_AM_IN_BASED_ON_COORDS(GET_PLAYER_COORDS(LocalPlayer), theApartment)
					
					IF theApartment > -1
						vReturn = mpProperties[theApartment].house.vSafeCoronaLoc
					ELSE
						vReturn = GET_PROPERTY_DROP_OFF_COORDS( eApartmentBuzzerEntrance ,TRUE)
					ENDIF
				BREAK
				
				CASE ciMISSION_CUTSCENE_PLACE_HOLDER
					vReturn = GET_ENTITY_COORDS(LocalPlayerPed)
				BREAK
				
				// === Tutorial/Fleeca ===
				
				case ciMISSION_CUTSCENE_FLEECA_CROWD_FAIL // MPH_TUT_MCS1
					vReturn=<<-2961.2, 482.1, 15.7>>
				break
				
				case ciMISSION_CUTSCENE_TUT_EXT
					vReturn = PICK_CLOSEST_FLEECA_FINALE_CUTSCENE_LOCATION_AND_PLAY(FALSE)
				break
				
				case ciMISSION_CUTSCENE_TUT_MID
					GET_APARTMENT_INTERIOR_I_AM_IN_BASED_ON_COORDS(GET_PLAYER_COORDS(LocalPlayer), theApartment)
					
					IF theApartment > -1
						vReturn = GET_ENTITY_COORDS(LocalPlayerPed) 
					ELSE
						vReturn = GET_PROPERTY_DROP_OFF_COORDS( eApartmentBuzzerEntrance,TRUE)
					ENDIF
				break
				
				case ciMISSION_CUTSCENE_TUT_CAR_EXT
					vReturn = <<693.7, -1005.1, 22.5>>
				break
				
				// === Prison ===
				
				case ciMISSION_CUTSCENE_PRI_BUS_EXT
					vReturn = <<2058.4, 3178.1, 44.4>>
				break
				
				case ciMISSION_CUTSCENE_PRI_PLA_EXT
					vReturn = <<-1253.7, -3347, 30.7>>
				break
				
				case ciMISSION_CUTSCENE_PRI_STA_EXT
					GET_APARTMENT_INTERIOR_I_AM_IN_BASED_ON_COORDS(GET_PLAYER_COORDS(LocalPlayer), theApartment)
					
					IF theApartment > -1
						vReturn = GET_ENTITY_COORDS(LocalPlayerPed) 
					ELSE
						vReturn = GET_PROPERTY_DROP_OFF_COORDS( eApartmentBuzzerEntrance,TRUE)
					ENDIF
				break
				
				case ciMISSION_CUTSCENE_PRI_UNF_EXT
					GET_APARTMENT_INTERIOR_I_AM_IN_BASED_ON_COORDS(GET_PLAYER_COORDS(LocalPlayer), theApartment)
					
					IF theApartment > -1
						vReturn = GET_ENTITY_COORDS(LocalPlayerPed) 
					ELSE
						vReturn = GET_PROPERTY_DROP_OFF_COORDS( eApartmentBuzzerEntrance,TRUE)
					ENDIF
				break 
				
				case ciMISSION_CUTSCENE_PRISON_PLANE_END
					vReturn = <<-1253.7, -3347, 30.7>>
				break 
				
				// === Pacific Standard/Ornate ===
				
				case ciMISSION_CUTSCENE_PAC_HAC_EXT
					vReturn = <<695.5, -1002.8, 23.1>>
				break
				
				case ciMISSION_CUTSCENE_PAC_BIK_EXT
					vReturn = <<11.6, 114.5, 79>>
				break
				
				case ciMISSION_CUTSCENE_PAC_CON_EXT
					vReturn = <<1975, 5168, 47>>
				break
				
				case ciMISSION_CUTSCENE_ORNATE_CONVOY_END
					vReturn=<<1975.0 ,5168.0, 47.0>>
				break
				
				case ciMISSION_CUTSCENE_PAC_WIT_MCS2
					vReturn = <<1960.8, 5168.8, 48.2 >>
				BREAK
				
				case ciMISSION_CUTSCENE_PAC_PHO_EXT
					vReturn = <<692.91,-1010.79,23.68 >>
				break
				
				case ciMISSION_CUTSCENE_PAC_FIN_MCS1
					vReturn = <<244.8, 212.2, 106.3>>
				BREAK
				
				case ciMISSION_CUTSCENE_PAC_FIN_EXT
					//Vreturn = <<717.5, -964.4, 37.2>> // Top of Lester's Warehouse
					vReturn = GET_ENTITY_COORDS(LocalPlayerPed)
				break
				
				// === Narcotics ===
				
				case ciMISSION_CUTSCENE_NAR_FIN_EXT
					vReturn = <<3318, 5146, 18>>
				break
				
				case ciMISSION_CUTSCENE_NAR_BIK_EXT
					vReturn = <<601, -421, 24>>
				break
				
				case ciMISSION_CUTSCENE_NAR_WEE_EXT
					vReturn = <<601, -430, 24>>
				break
				
				case ciMISSION_CUTSCENE_NAR_COK_EXT
					vReturn = <<605, -409.4, 24.7>>
				break
				
				case ciMISSION_CUTSCENE_NAR_MET_EXT
					vReturn = <<1375.2, 3611.1, 34.9>>
				break
				
				case ciMISSION_CUTSCENE_NAR_TRA_EXT
					vReturn = <<601, -431, 24 >>
				break
				
				// === Humane Labs ===
				
				case ciMISSION_CUTSCENE_HUM_ARM_EXT
					vReturn = <<764, 4188, 40>>
				break
				
				case ciMISSION_CUTSCENE_HUM_FIN_MCS1
					vReturn = <<3540.1, 3363.3, 28.7>>
				break
				
				case ciMISSION_CUTSCENE_HUM_FIN_EXT
					vReturn = <<1876.4, 292.6, 194>>
				break
				
				case ciMISSION_CUTSCENE_HUM_VAL_EXT
					vReturn = <<710.7, 4176.2, 45.1>>
				break 	
				
				case ciMISSION_CUTSCENE_HUM_EMP_EXT
					vReturn = <<1732, 3303, 44>>
				break
				
				case ciMISSION_CUTSCENE_HUM_KEY_EXT 
					GET_APARTMENT_INTERIOR_I_AM_IN_BASED_ON_COORDS(GET_PLAYER_COORDS(LocalPlayer), theApartment)
					
					IF theApartment > -1
						vReturn = GET_ENTITY_COORDS(LocalPlayerPed) 
					ELSE
						vReturn = GET_PROPERTY_DROP_OFF_COORDS( eApartmentBuzzerEntrance,TRUE)
					ENDIF
				break 	

				case ciMISSION_CUTSCENE_HUM_KEY_MCS1
					vReturn = <<134, -1072, 52>>
				BREAK
				
				case ciMISSION_CUTSCENE_HUM_DEL_EXT
					vReturn = <<3335,3668,50.5>>
				BREAK	
				
				case ciMISSION_CUTSCENE_PRI_FIN_EXT
					vReturn = PICK_CLOSEST_PRISON_FINALE_CUTSCENE_LOCATION_AND_PLAY(FALSE)	
				BREAK
		
				//Lowrider
				CASE ciMISSION_CUTSCENE_LOW_DRV_EXT
					vReturn = << -205.0, -1306.0, 30.42 >>
				BREAK
				
				CASE ciMISSION_CUTSCENE_LOW_FIN_EXT
					vReturn = <<-205.0, -1306.0, 30.42 >>
				BREAK
				
				CASE ciMISSION_CUTSCENE_LOW_FIN_MCS1
					vReturn = <<-205.0, -1306.0, 30.42 >>
				BREAK
				
				CASE ciMISSION_CUTSCENE_LOW_FUN_EXT
					vReturn = << 1530.400024, 3764.1001, 33.6 >>
				BREAK
				
				CASE ciMISSION_CUTSCENE_LOW_FUN_MCS1
					vReturn = << -55.0, -1505.0, 31.5 >>
				BREAK
				
				CASE ciMISSION_CUTSCENE_LOW_PHO_EXT
					vReturn = <<-55.1000, -1505.0000, 30.4774>> 
				BREAK
				
				CASE ciMISSION_CUTSCENE_LOW_TRA_EXT
					vReturn = << 997.8, -1957.0, 29.81 >>
				BREAK
				
				//Heists 2
				CASE ciMISSION_CUTSCENE_SILJ_MCS1
					vReturn = << 244.252,6163.913,-147.423 >>
				BREAK
				
				CASE ciMISSION_CUTSCENE_SILJ_EXT
					vReturn = << 408.136, 1080.440, 323.448 >>
				BREAK
				
				CASE ciMISSION_CUTSCENE_IAAJ_EXT
					vReturn = << 2056.761,2970.531,-65.386 >>
				BREAK
				
				CASE ciMISSION_CUTSCENE_SUBJ_MCS0
					vReturn = << -1682.577,5926.310,-62.092 >>
				BREAK
				
				CASE ciMISSION_CUTSCENE_SUBJ_EXT
					vReturn = << -958.456,-2991.145,28.884 >>
				BREAK
				
				CASE ciMISSION_CUTSCENE_SILJ_INT
					vReturn = <<1732.4158, 3287.9795, 40.1348>>
				BREAK
				
				// Casino 1
				CASE ciMISSION_CUTSCENE_CASINO_1_EXT
					vReturn = <<1380, 200, -50>>
				BREAK		
				CASE ciMISSION_CUTSCENE_CASINO_3_EXT
					vReturn = <<-3003, 100, 10.59>>
				BREAK
				CASE ciMISSION_CUTSCENE_CASINO_2_EXT
					vReturn = <<930, 44.5, 80.089996>>
				BREAK
				CASE ciMISSION_CUTSCENE_CASINO_5_EXT
					vReturn = <<1100.0, 220.0, -50.0>>
				BREAK
				CASE ciMISSION_CUTSCENE_CASINO_6_EXT
					vReturn = <<1100.0, 220.0, -50.0>>
				BREAK
				
				// Casino Heist 2
				CASE ciMISSION_CUTSCENE_CASINO_HEIST_DRP1
					vReturn = GET_CUTSCENE_COORDINATE_FROM_LAST_LOCATION(MC_ServerBD.iLastCompletedGoToLocation)					
					vReturn.z += 1.5
					GET_GROUND_Z_FOR_3D_COORD(vReturn, fGroundZ, FALSE)
					vReturn.z = fGroundZ
					PRINTLN("GET_CUTSCENE_COORDS - 		iLastCompletedGoToLocation: ", MC_ServerBD.iLastCompletedGoToLocation, " vReturn: ", vReturn)
				BREAK
				CASE ciMISSION_CUTSCENE_CASINO_HEIST_DRP2
					vReturn = GET_CUTSCENE_COORDINATE_FROM_LAST_LOCATION(MC_ServerBD.iLastCompletedGoToLocation)
					vReturn.z += 1.5
					GET_GROUND_Z_FOR_3D_COORD(vReturn, fGroundZ, FALSE)
					vReturn.z = fGroundZ
					PRINTLN("GET_CUTSCENE_COORDS - 		iLastCompletedGoToLocation: ", MC_ServerBD.iLastCompletedGoToLocation, " vReturn: ", vReturn)
				BREAK
				CASE ciMISSION_CUTSCENE_CASINO_HEIST_DRP3
					vReturn = GET_CUTSCENE_COORDINATE_FROM_LAST_LOCATION(MC_ServerBD.iLastCompletedGoToLocation)
					vReturn.z += 1.5
					GET_GROUND_Z_FOR_3D_COORD(vReturn, fGroundZ, FALSE)
					vReturn.z = fGroundZ
					PRINTLN("GET_CUTSCENE_COORDS - 		iLastCompletedGoToLocation: ", MC_ServerBD.iLastCompletedGoToLocation, " vReturn: ", vReturn)
				BREAK
				CASE ciMISSION_CUTSCENE_CASINO_HEIST_ESC
					vReturn = <<835.352, 8.298, 78.543>>	// Done
				BREAK 
				CASE ciMISSION_CUTSCENE_CASINO_HEIST_RP1
					vReturn = <<2519.875977, 255.302994, -25.112000>> // Done
				BREAK
				CASE ciMISSION_CUTSCENE_CASINO_HEIST_ENT
					vReturn = <<1100.0, 220.0, -50.0>>
				BREAK
				CASE ciMISSION_CUTSCENE_CASINO_HEIST_VLT
					vReturn = <<2488.347900, -267.364014, -71.646004>>
				BREAK
				CASE ciMISSION_CUTSCENE_CASINO_HEIST_SEW
					vReturn = <<2497.708008, -312.770996, -71.470001>>
				BREAK
				CASE ciMISSION_CUTSCENE_CASINO_HEIST_CEL
					vReturn = <<2479.268066, -273.873993, -59.283001>>
				BREAK
				CASE ciMISSION_CUTSCENE_CASINO_HEIST_ESC_B
					vReturn = <<835.352, 8.298, 78.543>>
				BREAK			
			ENDSWITCH
				
			IF MC_ServerBD.iLastCompletedGoToLocation > -1
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[MC_ServerBD.iLastCompletedGoToLocation].iLocBS3, ciLoc_BS3_UseFirstAdditionalSpawnPosAsEndCutscenePos)
					vReturn = GET_CUTSCENE_COORDINATE_FROM_LAST_LOCATION(MC_ServerBD.iLastCompletedGoToLocation)
					vReturn.z += 1.5
					GET_GROUND_Z_FOR_3D_COORD(vReturn, fGroundZ, FALSE)
					vReturn.z = fGroundZ
					PRINTLN("GET_CUTSCENE_COORDS - 		iLastCompletedGoToLocation: ", MC_ServerBD.iLastCompletedGoToLocation, " vReturn: ", vReturn, " USING ciLoc_BS3_UseFirstAdditionalSpawnPosAsEndCutscenePos")
				ENDIF
			ENDIF
			
		BREAK

	ENDSWITCH

	
	PRINTLN("GET_CUTSCENE_COORDS - iTeam: ", iteam, " cutType: ", cutType, " - vReturn = ", vReturn)
	PRINTLN("GET_CUTSCENE_COORDS - 		iCutsceneToUse: ", iCutsceneToUse)
	PRINTLN("GET_CUTSCENE_COORDS - 		MC_ServerBD.iEndCutscene: ", MC_ServerBD.iEndCutscene)
	
	IF IS_VECTOR_ZERO(vReturn)
		CERRORLN(DEBUG_CONTROLLER, "GET_CUTSCENE_COORDS - iTeam: ", iteam, " cutType: ", cutType, " - Returning a ZERO vector!")
	ENDIF
	RETURN vReturn
ENDFUNC

FUNC PLAYER_INDEX GET_HEIST_LEADER_PLAYER_FOR_TEAM(INT iteam, FMMC_CUTSCENE_TYPE cutType, INT iCutsceneToUse, INT iPlayer = 0)
	
	INT iBitsetToCheck
	IF iPlayer <= 1
		SWITCH cutType
			CASE FMMCCUT_ENDMOCAP	iBitsetToCheck = g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet						BREAK
			CASE FMMCCUT_MOCAP	 	iBitsetToCheck = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet 		BREAK
			CASE FMMCCUT_SCRIPTED 	iBitsetToCheck = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet 	BREAK
		ENDSWITCH
	ELSE
		SWITCH cutType
			CASE FMMCCUT_ENDMOCAP	iBitsetToCheck = g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet2						BREAK
			CASE FMMCCUT_MOCAP	 	iBitsetToCheck = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet2 		BREAK
			CASE FMMCCUT_SCRIPTED 	iBitsetToCheck = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet2 	BREAK
		ENDSWITCH
	ENDIF
	
	IF iPlayer = 0
	AND NOT IS_BIT_SET(iBitsetToCheck, ci_CSBS_Heist_Leader_Primary)
		RETURN INVALID_PLAYER_INDEX()
	ENDIF
	
	IF iPlayer = 1
	AND NOT IS_BIT_SET(iBitsetToCheck, ci_CSBS_Heist_Leader_Secondary)
		RETURN INVALID_PLAYER_INDEX()
	ENDIF
	
	IF iPlayer = 2
	AND NOT IS_BIT_SET(iBitsetToCheck, ci_CSBS2_Heist_Leader_Third)
		RETURN INVALID_PLAYER_INDEX()
	ENDIF
	
	IF iPlayer = 3
		RETURN INVALID_PLAYER_INDEX()
	ENDIF
	
	INT iPart
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iPart
	
		IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_HEIST_HOST)
			RELOOP
		ENDIF
	
		IF IS_BIT_SET(MC_serverBD.iCutscenePlayerBitset[iteam], iPart)
			PRINTLN("[RCC MISSION] GET_HEIST_LEADER_PLAYER_FOR_TEAM - iTeam: ", iteam, " iPlayer: ", iPlayer, " - Heist leader already selected for another slot")
			RELOOP
		ENDIF
		
		PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iPart)
		PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
		PRINTLN("[RCC MISSION] GET_HEIST_LEADER_PLAYER_FOR_TEAM - iTeam: ", iteam, " iPlayer: ", iPlayer, " - Found heist leader")
		RETURN tempPlayer
	ENDREPEAT

	PRINTLN("[RCC MISSION] GET_HEIST_LEADER_PLAYER_FOR_TEAM - iTeam: ", iteam, " iPlayer: ", iPlayer, " - No player selected")
	RETURN INVALID_PLAYER_INDEX()
ENDFUNC

FUNC BOOL SHOULD_SELECT_PLAYER_OF_TEAM(INT iteam, INT iCutsceneToUse, FMMC_CUTSCENE_TYPE cutType = FMMCCUT_MOCAP, INT iplayer = 0 )
	
	#IF IS_DEBUG_BUILD
	IF g_SHOULD_SELECT_PLAYER_OF_TEAMReturnTrue 
		RETURN TRUE
	ENDIF
	#ENDIF

	SWITCH cutType
	
		CASE FMMCCUT_MOCAP
		
			SWITCH iTeam
				CASE 0
					SWITCH iPlayer
						CASE 0 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet, ci_CSBS_Team0_Closest)
						CASE 1 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet, ci_CSBS_Team0_Closest_Secondary)
						CASE 2 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_Team0_Closest_Third)
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH iPlayer
						CASE 0 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet, ci_CSBS_Team1_Closest)
						CASE 1 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet, ci_CSBS_Team1_Closest_Secondary)
						CASE 2 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_Team1_Closest_Third)
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iPlayer
						CASE 0 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet, ci_CSBS_Team2_Closest)
						CASE 1 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet, ci_CSBS_Team2_Closest_Secondary)
						CASE 2 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_Team2_Closest_Third)
					ENDSWITCH
				BREAK
				CASE 3
					SWITCH iPlayer
						CASE 0 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet, ci_CSBS_Team3_Closest)
						CASE 1 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet, ci_CSBS_Team3_Closest_Secondary)
						CASE 2 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitSet2, ci_CSBS2_Team3_Closest_Third)
					ENDSWITCH
				BREAK
			ENDSWITCH
		
		BREAK
	
		CASE FMMCCUT_ENDMOCAP
		
			SWITCH iTeam
				CASE 0
					SWITCH iPlayer
						CASE 0 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet, ci_CSBS_Team0_Closest)
						CASE 1 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet, ci_CSBS_Team0_Closest_Secondary)
						CASE 2 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet2, ci_CSBS2_Team0_Closest_Third)
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH iPlayer
						CASE 0 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet, ci_CSBS_Team1_Closest)
						CASE 1 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet, ci_CSBS_Team1_Closest_Secondary)
						CASE 2 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet2, ci_CSBS2_Team1_Closest_Third)
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iPlayer
						CASE 0 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet, ci_CSBS_Team2_Closest)
						CASE 1 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet, ci_CSBS_Team2_Closest_Secondary)
						CASE 2 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet2, ci_CSBS2_Team2_Closest_Third)
					ENDSWITCH
				BREAK
				CASE 3
					SWITCH iPlayer
						CASE 0 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet, ci_CSBS_Team3_Closest)
						CASE 1 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet, ci_CSBS_Team3_Closest_Secondary)
						CASE 2 	RETURN IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet2, ci_CSBS2_Team3_Closest_Third)
					ENDSWITCH
				BREAK
			ENDSWITCH
		
		BREAK
	
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_CUTSCENE_STREAMING_RANGE(INT iCutsceneIndex, FMMC_CUTSCENE_TYPE cutType)
	
	INT iReturn = 0

	IF cutType != FMMCCUT_ENDMOCAP
		IF iCutsceneIndex < 0 
			RETURN 0
		ENDIF
	ENDIF
	
	SWITCH cutType
		
		CASE FMMCCUT_ENDMOCAP
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_PlayEndCutsceneForAllPlayers) 
				iReturn = 99999
				PRINTLN("[RCC MISSION] GET_CUTSCENE_STREAMING_RANGE ciOptionsBS24_PlayEndCutsceneForAllPlayers iReturn = ", iReturn)
			ENDIF
		BREAK
		
		CASE FMMCCUT_MOCAP
			IF iCutsceneIndex >= MAX_MOCAP_CUTSCENES
				RETURN 0
			ENDIF
			
			IF g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneIndex].iCutsceneWarpRange > 0
				iReturn = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneIndex].iCutsceneWarpRange
				PRINTLN("[RCC MISSION] GET_CUTSCENE_STREAMING_RANGE test 0 iReturn = ", iReturn)
			ENDIF
		BREAK
		
		CASE FMMCCUT_SCRIPTED
			IF iCutsceneIndex >= FMMC_GET_MAX_SCRIPTED_CUTSCENES()
				RETURN 0
			ENDIF
			
			IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneIndex].iCutsceneWarpRange > 0
				iReturn = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneIndex].iCutsceneWarpRange
				PRINTLN("[RCC MISSION] GET_CUTSCENE_STREAMING_RANGE test 1 iReturn = ", iReturn)
			ENDIF
		BREAK
		
	ENDSWITCH

	PRINTLN("[RCC MISSION] GET_CUTSCENE_STREAMING_RANGE do not use custom value ", iReturn)	
	RETURN iReturn
	
ENDFUNC

FUNC FLOAT GET_CUTSCENE_PLAYER_INCLUSION_RANGE(STRING strCutscene, INT iTeam, INT iCutsceneIndex, FMMC_CUTSCENE_TYPE cutType, BOOL bPlayCheck = FALSE)
	
	// Default value
	FLOAT fReturn = 101.0
	
	IF ARE_STRINGS_EQUAL( strCutscene, "MPH_PRI_FIN_MCS1" ) // Raskovsky cutscene
		
		fReturn = 250.0
		
	ELIF ARE_STRINGS_EQUAL(strCutscene,"MPH_PRI_FIN_MCS2" ) // Get out of plane cutscene
	
		// pilot(helicopter) team shouldn't be selected for the cutscene
		IF iTeam = 3
			IF bPlayCheck
				fReturn = 1000.0
			ELSE
				fReturn = 0.0
			ENDIF
		ELSE
			fReturn = 999999.0
		ENDIF
		
	ELIF ARE_STRINGS_EQUAL(strCutscene, "MPH_NAR_FIN_EXT" ) // Narcotics Finale
		fReturn = 999999.0
	ELIF ARE_STRINGS_EQUAL(strCutscene, "MPH_PRI_FIN_EXT" ) // Prison Finale
		fReturn = 999999.0
	ELIF ARE_STRINGS_EQUAL(strCutscene, "MPH_HUM_DEL_EXT" ) // Deliver EMP exit
		fReturn = 999999.0
	ELIF ARE_STRINGS_EQUAL(strCutscene, "MPH_PAC_FIN_EXT") // Pacific Finale
		fReturn = 999999.0
	ELIF ARE_STRINGS_EQUAL(strCutscene, "MPH_HUM_FIN_EXT") // Humane Finale
		fReturn = 999999.0
	ELIF ARE_STRINGS_EQUAL(strCutscene, "MPH_HUM_KEY_MCS1") // Humane key codes 
		fReturn = 999999.0
	ELSE
		INT iStreamingRange = GET_CUTSCENE_STREAMING_RANGE(iCutsceneIndex, cutType)
		IF iStreamingRange > 0
			fReturn = TO_FLOAT(iStreamingRange)
		ENDIF
	ENDIF
	
	PRINTLN("[RCC MISSION] GET_CUTSCENE_PLAYER_INCLUSION_RANGE fReturn = ", fReturn)
	RETURN fReturn
	
ENDFUNC

//Force to check specific team
FUNC BOOL FORCE_CLOSEST_PLAYER_FOR_CUTSCENE_TEAM(STRING strCutsceneName, INT iteam)
	IF ARE_STRINGS_EQUAL(strCutsceneName, "MPH_PRI_FIN_MCS2")
		IF iteam != 3
			PRINTLN("[RCC MISSION] FORCE_CLOSEST_PLAYER_FOR_CUTSCENE_TEAM TRUE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Get the closest cutscne 
FUNC PLAYER_INDEX GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM(STRING strCutsceneName, FMMC_CUTSCENE_TYPE cutType, INT iteam, INT iCutsceneToUse, INT iCutsceneplayer)

	INT icount
	FLOAT fdist
	FLOAT fSmallestDist
	
	fSmallestDist = GET_CUTSCENE_PLAYER_INCLUSION_RANGE(strCutsceneName, iTeam, iCutsceneToUse, cutType)
	
	INT iReturnPlayer = -1
	PARTICIPANT_INDEX tempPart
	PLAYER_INDEX tempPlayer  = INVALID_PLAYER_INDEX()
	PLAYER_INDEX closestPlayer = INVALID_PLAYER_INDEX()
	VECTOR vPlayerPos
	VECTOR vPos = GET_CUTSCENE_COORDS(iteam, cutType)
	
	PRINTLN("[RCC MISSION] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM - START")
	PRINTLN("[RCC MISSION] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM 		vCutPos = ", vPos)
	PRINTLN("[RCC MISSION] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM 		cutType = ", cutType)
	PRINTLN("[RCC MISSION] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM 		inclusionRange(fSmallestDist) = ", fSmallestDist)
 
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() icount
		
		IF IS_BIT_SET(MC_playerBD[icount].iClientBitSet,PBBOOL_START_SPECTATOR)
		OR IS_BIT_SET(MC_playerBD[icount].iClientBitSet,PBBOOL_EARLY_END_SPECTATOR)
			PRINTLN("[RCC MISSION] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM - iPart[", icount, "] - RELOOP(CONTINUE)" )
			PRINTLN("[RCC MISSION] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM		IS_BIT_SET(MC_playerBD[icount].iClientBitSet,PBBOOL_START_SPECTATOR) = ", IS_BIT_SET(MC_playerBD[icount].iClientBitSet,PBBOOL_START_SPECTATOR) )
			PRINTLN("[RCC MISSION] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM 		IS_BIT_SET(MC_playerBD[icount].iClientBitSet,PBBOOL_EARLY_END_SPECTATOR) = ", IS_BIT_SET(MC_playerBD[icount].iClientBitSet,PBBOOL_EARLY_END_SPECTATOR) )
			RELOOP
		ENDIF
		
		IF IS_BIT_SET(MC_serverBD.iCutscenePlayerBitset[iteam],icount)
			PRINTLN("[RCC MISSION] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM - iPart[", icount, "] - RELOOP(CONTINUE)" )
			PRINTLN("[RCC MISSION] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM		IS_BIT_SET(MC_serverBD.iCutscenePlayerBitset[iteam],icount) = ", IS_BIT_SET(MC_serverBD.iCutscenePlayerBitset[iteam],icount) )
			RELOOP
		ENDIF
		
		IF NOT SHOULD_SELECT_PLAYER_OF_TEAM(MC_playerBD[icount].iteam, iCutsceneToUse, cutType, iCutsceneplayer)
		AND NOT FORCE_CLOSEST_PLAYER_FOR_CUTSCENE_TEAM(strCutsceneName, MC_playerBD[icount].iteam)
			PRINTLN("[RCC MISSION] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM - iPart[", icount, "] - RELOOP(CONTINUE)" )
			PRINTLN("[RCC MISSION] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM		SHOULD_SELECT_PLAYER_OF_TEAM() = FALSE" )
			RELOOP
		ENDIF
		
		tempPart = INT_TO_PARTICIPANTINDEX(icount)
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			PRINTLN("[RCC MISSION] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM - iPart[", icount, "] - RELOOP(CONTINUE)" )
			PRINTLN("[RCC MISSION] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM		NETWORK_IS_PARTICIPANT_ACTIVE() = FALSE" )
			RELOOP
		ENDIF
		
		tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
		IF NOT IS_NET_PLAYER_OK(tempPlayer)
			PRINTLN("[RCC MISSION] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM - iPart[", icount, "] - RELOOP(CONTINUE)" )
			PRINTLN("[RCC MISSION] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM		IS_NET_PLAYER_OK() = FALSE" )
			RELOOP
		ENDIF
		
		IF tempPlayer != LocalPlayer
			vPlayerPos = NETWORK_GET_LAST_PLAYER_POS_RECEIVED_OVER_NETWORK(tempPlayer)
			PRINTLN("[RCC MISSION] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM - Grabbing player's last network pos.")
		ENDIF
		
		IF IS_VECTOR_ZERO(vPlayerPos)
			vPlayerPos = GET_ENTITY_COORDS(GET_PLAYER_PED(tempPlayer))
			PRINTLN("[RCC MISSION] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM - vPlayerPos was ZERO, getting player ped pos")
		ENDIF
		
		fDist = GET_DISTANCE_BETWEEN_COORDS(vPlayerPos, vPos)
		//Check if the player is closer than stored

		#IF IS_DEBUG_BUILD
			PRINTLN("[RCC MISSION] [PED CUT] part = ", icount, " coords = ", vPlayerPos)
		#endif 

		IF fDist >= fSmallestDist
			PRINTLN("[RCC MISSION] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM - iPart[", icount, "] - RELOOP(CONTINUE)" )
			PRINTLN("[RCC MISSION] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM		fDist too great, fDist = ", fDist, " fSmallestDist = ", fSmallestDist )
			RELOOP
		ENDIF
		
		fSmallestDist = fDist
		closestPlayer = tempPlayer
		iReturnPlayer = icount
	ENDREPEAT
	
	//Set that we've grabbed this player!
	IF iReturnPlayer != -1
		PRINTLN("[RCC MISSION] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM - Found a player")
	ELSE
		PRINTLN("[RCC MISSION] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM - Failed to find a player")
	ENDIF
	PRINTLN("[RCC MISSION] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM		fSmallestDist = ", fSmallestDist )
	PRINTLN("[RCC MISSION] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM		closestPlayer = ", NATIVE_TO_INT( closestPlayer ) )
	PRINTLN("[RCC MISSION] [PED CUT] GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM		iReturnPlayer = ", iReturnPlayer )
	RETURN closestPlayer
ENDFUNC

//Get the cutscene vehicle driver
//Can override the seat if required
FUNC PLAYER_INDEX GET_CUTSCENE_PLAYER_FROM_VEHICLE(FMMC_CUTSCENE_TYPE cutType, INT iteam, STRING sVehicle, INT iCutsceneToUse, VEHICLE_SEAT vsSeat = VS_DRIVER)

	PRINTLN("[RCC MISSION] INSIDE GET_CUTSCENE_PLAYER_FROM_VEHICLE")

	IF IS_STRING_NULL_OR_EMPTY(sVehicle)
		PRINTLN("[RCC MISSION] GET_CUTSCENE_PLAYER_FROM_VEHICLE - sVehicle = null or empty")
		RETURN INVALID_PLAYER_INDEX()
	ENDIF
	PRINTLN("[RCC MISSION] GET_CUTSCENE_PLAYER_FROM_VEHICLE - HAVE VALID HANDLE FOR VEHICLE ", sVehicle)
	PRINTLN("[RCC MISSION] GET_CUTSCENE_PLAYER_FROM_VEHICLE - SEAT: ", ENUM_TO_INT(vsSeat))

	INT icount
	FOR icount = 0 TO (FMMC_MAX_CUTSCENE_ENTITIES - 1)
	
		TEXT_LABEL_31 tlCutsceneHandle
		INT iCutsceneEntityIndex
	
		SWITCH cutType

			CASE FMMCCUT_ENDMOCAP
				IF g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[icount].iType = CREATION_TYPE_VEHICLES
					tlCutsceneHandle = g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[icount].tlCutsceneHandle
					iCutsceneEntityIndex = g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[icount].iIndex
				ENDIF
			BREAK
			
			CASE FMMCCUT_MOCAP
				IF g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sCutsceneEntities[icount].iType = CREATION_TYPE_VEHICLES
					tlCutsceneHandle = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sCutsceneEntities[icount].tlCutsceneHandle
					iCutsceneEntityIndex = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].sCutsceneEntities[icount].iIndex
				ENDIF
			BREAK
			
			CASE FMMCCUT_SCRIPTED
				IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].sCutsceneEntities[icount].iType = CREATION_TYPE_VEHICLES
					iCutsceneEntityIndex = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].sCutsceneEntities[icount].iIndex
				ENDIF
			BREAK
			
			DEFAULT
				tlCutsceneHandle = ""
				iCutsceneEntityIndex = -1
			BREAK
			
		ENDSWITCH
		
		// No valid entity index, skip to next
		IF iCutsceneEntityIndex = -1
		OR iCutsceneEntityIndex >= FMMC_MAX_VEHICLES
			RELOOP
		ENDIF
		
		// handle doesn't match, skip to next
		IF NOT ARE_STRINGS_EQUAL(tlCutsceneHandle, sVehicle) 
		AND NOT ARE_STRINGS_EQUAL(sVehicle, "ANY")
			RELOOP
		ENDIF
		
		// Invalid/no existent entity, skip to next
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iCutsceneEntityIndex])
			RELOOP
		ENDIF

		VEHICLE_INDEX veh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iCutsceneEntityIndex])
		IF NOT DOES_ENTITY_EXIST(veh)
			RELOOP
		ENDIF

		INT iPart
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iPart

			IF IS_BIT_SET(MC_serverBD.iCutscenePlayerBitset[iteam], iPart)
				RELOOP
			ENDIF

			IF IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_START_SPECTATOR)
				RELOOP
			ENDIF
			
			IF IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_EARLY_END_SPECTATOR)
				RELOOP
			ENDIF
			
			PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iPart)
			IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				RELOOP
			ENDIF
						
			PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
			IF NOT IS_NET_PLAYER_OK(tempPlayer)
				RELOOP
			ENDIF
			
			PED_INDEX tempPed = GET_PLAYER_PED(tempPlayer)
			IF NOT IS_PED_IN_VEHICLE(TempPed, veh)
				RELOOP
			ENDIF
			
			IF tempPed != GET_PED_IN_VEHICLE_SEAT(veh, vsSeat)
				RELOOP
			ENDIF
			
			PRINTLN("[RCC MISSION] GET_CUTSCENE_PLAYER_FROM_VEHICLE - iTeam: ", iTeam, " - found player in iCutsceneEntityIndex: ", iCutsceneEntityIndex, " seat: ", vsSeat)
			RETURN tempPlayer

		ENDREPEAT
		
	ENDFOR
	
	PRINTLN("[RCC MISSION] GET_CUTSCENE_PLAYER_FROM_VEHICLE - iTeam: ", iTeam, " - could not find player in vehicle seat")
	RETURN INVALID_PLAYER_INDEX()
ENDFUNC

///PURPOSE: This function returns true if the inputted iTeam should be in the inputted iCutscene
FUNC BOOL SHOULD_TEAM_BE_IN_CUTSCENE(FMMC_CUTSCENE_TYPE cutType, INT icutscene, INT iteam, INT iCutsceneTeam)
	PRINTLN("[RCC MISSION] SHOULD_TEAM_BE_IN_CUTSCENE icutscene = ", icutscene)	
	PRINTLN("[RCC MISSION] SHOULD_TEAM_BE_IN_CUTSCENE cutType = ", cutType)	
	PRINTLN("[RCC MISSION] SHOULD_TEAM_BE_IN_CUTSCENE iteam = ", iteam)

	INT iBitsetToCheck = -1
	
	IF icutscene = -1
	AND cutType != FMMCCUT_ENDMOCAP
		RETURN FALSE
	ENDIF
	
	IF icutscene = ciMISSION_CUTSCENE_PAC_PHO_EXT
		RETURN TRUE
	ENDIF
		
	IF iteam = iCutsceneTeam
		RETURN TRUE
	ENDIF
	
	SWITCH cutType
	
		CASE FMMCCUT_MOCAP
			iBitsetToCheck = g_FMMC_STRUCT.sMocapCutsceneData[icutscene].iCutsceneBitset
		BREAK
		
		CASE FMMCCUT_ENDMOCAP
			IF NOT AM_I_ON_A_HEIST()
				RETURN TRUE
				PRINTLN("[RCC MISSION] SHOULD_TEAM_BE_IN_CUTSCENE NOT ON HEIST")
			ENDIF
			iBitsetToCheck = g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitset
		BREAK

		CASE FMMCCUT_SCRIPTED
			iBitsetToCheck = g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].iCutsceneBitset
		BREAK

	ENDSWITCH

	SWITCH iteam
		CASE 0
			IF IS_BIT_SET(iBitsetToCheck, ci_CSBS_Pull_Team0_In)
				CPRINTLN(DEBUG_CONTROLLER, "[RCC MISSION] SHOULD_TEAM_BE_IN_CUTSCENE sMocapCutsceneData ci_CSBS_Pull_Team0_In TRUE ")	
				RETURN TRUE
			ENDIF
		BREAK
		CASE 1
			IF IS_BIT_SET(iBitsetToCheck, ci_CSBS_Pull_Team1_In)
				CPRINTLN(DEBUG_CONTROLLER, "[RCC MISSION] SHOULD_TEAM_BE_IN_CUTSCENE sMocapCutsceneData ci_CSBS_Pull_Team1_In TRUE ")
				RETURN TRUE
			ENDIF		
		BREAK
		CASE 2
			IF IS_BIT_SET(iBitsetToCheck, ci_CSBS_Pull_Team2_In)
				CPRINTLN(DEBUG_CONTROLLER, "[RCC MISSION] SHOULD_TEAM_BE_IN_CUTSCENE sMocapCutsceneData ci_CSBS_Pull_Team2_In TRUE ")
				RETURN TRUE
			ENDIF		
		BREAK
		CASE 3
			IF IS_BIT_SET(iBitsetToCheck, ci_CSBS_Pull_Team3_In)
				CPRINTLN(DEBUG_CONTROLLER, "[RCC MISSION] SHOULD_TEAM_BE_IN_CUTSCENE sMocapCutsceneData ci_CSBS_Pull_Team3_In TRUE ")
				RETURN TRUE
			ENDIF		
		BREAK
	ENDSWITCH

	RETURN FALSE
ENDFUNC


FUNC STRING GET_MOCAP_CUTSCENE_NAME(INT icutsceneNum, INT iCutsceneType, INT iDropoffEntityType, INT iTeam)
	
	STRING sCutsceneName
	INT iLoop
	INT iCutsceneIndexToUse[ FMMC_MAX_TEAMS ]
	
	// This was stopping the cutscene from streaming on the rule of the cutscene which we need to do because some rules JUMP from one rule or another so "pre-streaming" is impossible.
	/*IF NOT IS_BIT_SET(iLocalBoolCheck5, LBOOL5_PED_INVOLVED_IN_CUTSCENE)
	AND NOT IS_BIT_SET(iLocalBoolCheck2, LBOOL2_CUTSCENE_STREAMED)
		iCutsceneStreamingTeam = GET_CUTSCENE_TEAM(TRUE)
	ENDIF*/
	
	iCutsceneStreamingTeam = iTeam
	
	IF IS_BIT_SET(iLocalBoolCheck9, LBOOL9_FORCE_STREAM_CUTSCENE)
		FOR iLoop = 0 TO FMMC_MAX_TEAMS - 1
			iCutsceneIndexToUse[ iLoop ] = MC_serverBD.iCutsceneID[ iLoop ]
		ENDFOR
	ELSE
		FOR iLoop = 0 TO FMMC_MAX_TEAMS - 1
			iCutsceneIndexToUse[ iLoop ] = MC_serverBD.iCutsceneStreamingIndex[ iLoop ]
		ENDFOR
	ENDIF
	
	PRINTLN("[LM][GET_MOCAP_CUTSCENE_NAME] - icutsceneNum: ", icutsceneNum, " icutsceneType: ", iCutsceneType, " iCutsceneStreamingTeam: ", iCutsceneStreamingTeam, " LBOOL2_CUTSCENE_STREAMED: ", IS_BIT_SET(iLocalBoolCheck2, LBOOL2_CUTSCENE_STREAMED))
	
	IF iCutsceneStreamingTeam >= 0
	AND NOT g_bMissionEnding
		
		PRINTLN("[LM][GET_MOCAP_CUTSCENE_NAME] - iCutsceneIndexToUse: ",  iCutsceneIndexToUse[ iCutsceneStreamingTeam ])
		
		IF iCutsceneIndexToUse[ iCutsceneStreamingTeam ] >= FMMC_GET_MAX_SCRIPTED_CUTSCENES()
		AND iCutsceneIndexToUse[ iCutsceneStreamingTeam ] < FMMC_GET_MAX_CUTSCENES_WITH_MOCAP()
			INT iMocap = ( iCutsceneIndexToUse[  iCutsceneStreamingTeam ] - FMMC_GET_MAX_SCRIPTED_CUTSCENES() )
			
			IF NOT IS_STRING_NULL_OR_EMPTY( g_FMMC_STRUCT.sMocapCutsceneData[ iMocap ].tlName )
			AND NOT IS_BIT_SET( MC_playerBD[ iLocalPart ].iCutsceneFinishedBitset, iCutsceneIndexToUse[ iCutsceneStreamingTeam ] )	
				sCutsceneName = Get_String_From_TextLabel( g_FMMC_STRUCT.sMocapCutsceneData[ iMocap ].tlName )
				
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][GET_MOCAP_CUTSCENE_NAME] - Mid-mission cutscene found, use iMocap ", iMocap, " name is ", sCutsceneName )
				IF NOT IS_BIT_SET( iLocalBoolCheck5, LBOOL5_MID_MISSION_MOCAP )
					SET_BIT( iLocalBoolCheck5, LBOOL5_MID_MISSION_MOCAP )
					
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][GET_MOCAP_CUTSCENE_NAME] - Mid-mission cutscene found, use iMocap ", iMocap, " name is ", sCutsceneName )
				ENDIF
				RETURN sCutsceneName
			ELSE
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][GET_MOCAP_CUTSCENE_NAME] - Invalid name or this cutscene has been played already: ", sCutsceneName )				
			ENDIF
		ELSE
		//	CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][GET_MOCAP_CUTSCENE_NAME] - Not found a valid cutscene to stream - not a mocap" )			
		ENDIF
	ENDIF
	
	IF iCutsceneNum = -1
		RETURN ""
	ENDIF
		

	SWITCH iCutsceneType
	
		CASE ciMISSION_CUTSCENE_MADRAZO
		
			IF icutsceneNum = ci_INTRO_CUTSCENE
				sCutsceneName = "MP_INTRO_MCS_14_B" //generic intro
			ELSE

				sCutsceneName = "MP_INT_MCS_15_A3"
				
				SWITCH iDropoffEntityType

					CASE ci_TARGET_LOCATION
					
						sCutsceneName = "MP_INT_MCS_15_A3"  // generic veh/package

					BREAK

					CASE ci_TARGET_VEHICLE
					
						IF icutsceneNum = 0 //icutsceneNum randomly generated between 0 - 1
							sCutsceneName = "MP_INT_MCS_15_A3"  // generic veh/package
						ELSE
							sCutsceneName = "MP_INT_MCS_15_A4"   //  generic veh/package
						ENDIF

					BREAK
					
					CASE ci_TARGET_OBJECT
					
						IF icutsceneNum = 0
							sCutsceneName = "MP_INT_MCS_15_A1_B"  //generic package
						ELIF icutsceneNum = 1 
							sCutsceneName = "MP_INT_MCS_15_A2B"  //generic package
						else 
							sCutsceneName = "MP_INT_MCS_15_A1_B"  //generic packag
						ENDIF

					BREAK
					
					CASE ci_TARGET_PED
					
						IF icutsceneNum = 0
							sCutsceneName = "MP_INT_MCS_15_A3"  // generic veh/package
						ELSE
							sCutsceneName = "MP_INT_MCS_15_A4"   //  generic veh/package
						ENDIF

					BREAK	
					
				ENDSWITCH
			ENDIF
			
		BREAK
		
		CASE ciMISSION_CUTSCENE_TREVOR
		
			sCutsceneName = "MP_INT_MCS_17_A3"
			
			SWITCH iDropoffEntityType
			
			
				CASE ci_TARGET_PED
				CASE ci_TARGET_LOCATION
				
					IF icutsceneNum = 0
						IF MC_serverBD.piCutscenePlayerIDs[MC_PlayerBD[iPartToUse].iteam][0] != INVALID_PLAYER_INDEX()
							IF IS_NET_PLAYER_OK(MC_serverBD.piCutscenePlayerIDs[MC_PlayerBD[iPartToUse].iteam][0])
								//IF IS_PED_MALE(GET_PLAYER_PED(MC_serverBD.piCutscenePlayerIDs[MC_PlayerBD[iPartToUse].iteam][0]))
								IF NOT IS_PLAYER_PED_FEMALE(MC_serverBD.piCutscenePlayerIDs[MC_PlayerBD[iPartToUse].iteam][0])
									SET_BIT(iLocalBoolCheck2,LBOOL2_MOCAP_MALE)
									CLEAR_BIT(iLocalBoolCheck2,LBOOL2_MOCAP_FEMALE)
									sCutsceneName = "MP_INT_MCS_17_A4"
								ELSE
									SET_BIT(iLocalBoolCheck2,LBOOL2_MOCAP_FEMALE)
									CLEAR_BIT(iLocalBoolCheck2,LBOOL2_MOCAP_MALE)
									sCutsceneName = "MP_INT_MCS_17_A7"
								ENDIF
							ELSE
								sCutsceneName = NULL
							ENDIF
						ELSE
							sCutsceneName = NULL
						ENDIF
					ELIF icutsceneNum = 1
						sCutsceneName = "MP_INT_MCS_17_A3"
					ELIF icutsceneNum = 2
						sCutsceneName = "MP_INTRO_MCS_17_A5"
					ELSE
						sCutsceneName = "MP_INTRO_MCS_17_A9"
					ENDIF
					
				BREAK
				
				CASE ci_TARGET_VEHICLE
				
					IF icutsceneNum = 0
						IF MC_serverBD.piCutscenePlayerIDs[MC_PlayerBD[iPartToUse].iteam][0] != INVALID_PLAYER_INDEX()
							IF IS_NET_PLAYER_OK(MC_serverBD.piCutscenePlayerIDs[MC_PlayerBD[iPartToUse].iteam][0])
								IF NOT IS_PLAYER_PED_FEMALE(MC_serverBD.piCutscenePlayerIDs[MC_PlayerBD[iPartToUse].iteam][0])
									SET_BIT(iLocalBoolCheck2,LBOOL2_MOCAP_MALE)
									CLEAR_BIT(iLocalBoolCheck2,LBOOL2_MOCAP_FEMALE)
									sCutsceneName = "MP_INT_MCS_17_A4"
								ELSE
									SET_BIT(iLocalBoolCheck2,LBOOL2_MOCAP_FEMALE)
									CLEAR_BIT(iLocalBoolCheck2,LBOOL2_MOCAP_MALE)
									sCutsceneName = "MP_INT_MCS_17_A7"
								ENDIF
							ELSE
								sCutsceneName = NULL
							ENDIF
						ELSE
							sCutsceneName = NULL
						ENDIF
					ELIF icutsceneNum = 1
						sCutsceneName = "MP_INT_MCS_17_A3"
					ELIF icutsceneNum = 2
						sCutsceneName = "MP_INTRO_MCS_17_A5"
					ELIF icutsceneNum = 3
						sCutsceneName = "MP_INTRO_MCS_17_A9"
					ELIF icutsceneNum = 4
						sCutsceneName = "MP_INT_MCS_17_A1"
					ELSE
						sCutsceneName = "MP_INTRO_MCS_17_A8"
					ENDIF
					
				BREAK
				
				CASE ci_TARGET_OBJECT
				
					IF icutsceneNum = 0
						IF MC_serverBD.piCutscenePlayerIDs[MC_PlayerBD[iPartToUse].iteam][0] != INVALID_PLAYER_INDEX()
							IF IS_NET_PLAYER_OK(MC_serverBD.piCutscenePlayerIDs[MC_PlayerBD[iPartToUse].iteam][0])
								IF NOT IS_PLAYER_PED_FEMALE(MC_serverBD.piCutscenePlayerIDs[MC_PlayerBD[iPartToUse].iteam][0])
									SET_BIT(iLocalBoolCheck2,LBOOL2_MOCAP_MALE)
									CLEAR_BIT(iLocalBoolCheck2,LBOOL2_MOCAP_FEMALE)
									sCutsceneName = "MP_INT_MCS_17_A4"
								ELSE
									SET_BIT(iLocalBoolCheck2,LBOOL2_MOCAP_FEMALE)
									CLEAR_BIT(iLocalBoolCheck2,LBOOL2_MOCAP_MALE)
									sCutsceneName = "MP_INT_MCS_17_A7"
								ENDIF
							ELSE
								sCutsceneName = NULL
							ENDIF
						ELSE
							sCutsceneName = NULL
						ENDIF
					ELIF icutsceneNum = 1
						sCutsceneName = "MP_INT_MCS_17_A3"
					ELIF icutsceneNum = 2
						sCutsceneName = "MP_INTRO_MCS_17_A5"
					ELIF icutsceneNum = 3
						sCutsceneName = "MP_INTRO_MCS_17_A9"
					ELIF icutsceneNum = 4
						sCutsceneName = "MP_INT_MCS_17_A2"
					ELSE
						sCutsceneName = "MP_INT_MCS_17_A6"
					ENDIF

				BREAK				
			ENDSWITCH
				
			
		BREAK
		
		CASE ciMISSION_CUTSCENE_SIMEON

			sCutsceneName = "MP_INTRO_MCS_12_A3"
			
			IF icutsceneNum = 0
				IF MC_serverBD.piCutscenePlayerIDs[MC_PlayerBD[iPartToUse].iteam][0] != INVALID_PLAYER_INDEX()
					IF IS_NET_PLAYER_OK(MC_serverBD.piCutscenePlayerIDs[MC_PlayerBD[iPartToUse].iteam][0])
						IF NOT IS_PLAYER_PED_FEMALE(MC_serverBD.piCutscenePlayerIDs[MC_PlayerBD[iPartToUse].iteam][0])
							sCutsceneName = "MP_INTRO_MCS_12_A1"
							SET_BIT(iLocalBoolCheck2,LBOOL2_MOCAP_MALE)
							CLEAR_BIT(iLocalBoolCheck2,LBOOL2_MOCAP_FEMALE)
						ELSE
							sCutsceneName = "MP_INTRO_MCS_12_A2"  
							SET_BIT(iLocalBoolCheck2,LBOOL2_MOCAP_FEMALE)
							CLEAR_BIT(iLocalBoolCheck2,LBOOL2_MOCAP_MALE)
						ENDIF
					ELSE
						sCutsceneName = NULL
					ENDIF
				ELSE
					sCutsceneName = NULL
				ENDIF
			ELIF icutsceneNum = 1
				sCutsceneName = "MP_INTRO_MCS_12_A3"
			ELIF icutsceneNum = 2
				sCutsceneName = "MP_INT_MCS_12_A3_3" 
			ELSE
				sCutsceneName = "MP_INT_MCS_12_A3_4"
			ENDIF

		BREAK
		
		CASE ciMISSION_CUTSCENE_GERALD
		
		//Gerald Drop-off variation cutscenes
			sCutsceneName = "MP_INTRO_MCS_10_A5" // generic (Package)
			
			IF icutsceneNum = ci_INTRO_CUTSCENE
				sCutsceneName = "MP_INTRO_MCS_10_A1" //generic (Package) - intro
			ELSE
				sCutsceneName = "MP_INTRO_MCS_10_A5" // generic (Package)
				IF icutsceneNum = 0
					sCutsceneName = "MP_INTRO_MCS_10_A2"  // generic (Package)
				ELIF icutsceneNum = 1
					sCutsceneName = "MP_INTRO_MCS_10_A3"  // generic (Package)
				ELIF icutsceneNum = 2
					sCutsceneName = "MP_INTRO_MCS_10_A4"  // generic (Package)
				ELSE
					sCutsceneName = "MP_INTRO_MCS_10_A5"  // generic (Package)	
				ENDIF

			ENDIF

		BREAK
		
		CASE ciMISSION_CUTSCENE_HEIST
			sCutsceneName = "HEIST_EXT_CONCAT"  
		BREAK
		
		CASE ciMISSION_CUTSCENE_PLACE_HOLDER
			sCutsceneName = ""  
		BREAK
		
		case ciMISSION_CUTSCENE_PRISON_PLANE_END
		
			sCutsceneName = "MPH_PRI_PLA_EXT" 
			
		break 
		
		case ciMISSION_CUTSCENE_ORNATE_CONVOY_END
		
			sCutsceneName = "MPH_PAC_CON_EXT" 

		break 
		
		case ciMISSION_CUTSCENE_FLEECA_CROWD_FAIL
		
			sCutsceneName = "MPH_TUT_MCS1" 
			
		break 
		
		case ciMISSION_CUTSCENE_HUM_ARM_EXT
		
			sCutsceneName = "MPH_HUM_ARM_EXT" 
			
		break 
		case ciMISSION_CUTSCENE_TUT_CAR_EXT
		
			sCutsceneName = "MPH_TUT_CAR_EXT" 
			
		break 
		case ciMISSION_CUTSCENE_NAR_FIN_EXT
			sCutsceneName = "MPH_NAR_FIN_EXT" 
		break 
		case ciMISSION_CUTSCENE_PRI_BUS_EXT
			sCutsceneName = "MPH_PRI_BUS_EXT" 
		break 		
		case ciMISSION_CUTSCENE_PRI_PLA_EXT
			sCutsceneName = "MPH_PRI_PLA_EXT" 
		break 
		case ciMISSION_CUTSCENE_PAC_CON_EXT
			sCutsceneName = "MPH_PAC_CON_EXT" 
		break 	
		case ciMISSION_CUTSCENE_TUT_EXT
			sCutsceneName = "MPH_TUT_EXT" 
		break 
		case ciMISSION_CUTSCENE_HUM_EMP_EXT
			sCutsceneName = "MPH_HUM_EMP_EXT" 
		break 	
		
		case ciMISSION_CUTSCENE_PAC_HAC_EXT
		
			sCutsceneName = "MPH_PAC_HAC_EXT" 
			
		break 
		case ciMISSION_CUTSCENE_PAC_BIK_EXT
		
			sCutsceneName = "MPH_PAC_BIK_EXT"  
			
		break 	
		case ciMISSION_CUTSCENE_NAR_BIK_EXT
		
			sCutsceneName = "MPH_NAR_BIK_EXT"  
			
		break 
		case ciMISSION_CUTSCENE_NAR_WEE_EXT
		
			sCutsceneName = "MPH_NAR_WEE_EXT"  
			
		break 	
		case ciMISSION_CUTSCENE_NAR_COK_EXT
		
			sCutsceneName = "MPH_NAR_COK_EXT"  
			
		break 
		case ciMISSION_CUTSCENE_PAC_FIN_EXT
		
			sCutsceneName = "MPH_PAC_FIN_EXT"  
			
		break 	
		case ciMISSION_CUTSCENE_PRI_STA_EXT
		
			sCutsceneName = "MPH_PRI_STA_EXT"   
			
		break 	
		
		case ciMISSION_CUTSCENE_HUM_FIN_MCS1
		
			sCutsceneName = "MPH_HUM_FIN_MCS1"
		
		break		
		
		case ciMISSION_CUTSCENE_HUM_FIN_EXT 
		
			sCutsceneName = "MPH_HUM_FIN_EXT"   
			
		break 
		case ciMISSION_CUTSCENE_HUM_VAL_EXT 
		
			sCutsceneName = "MPH_HUM_VAL_EXT"    
			
		break 	
		case ciMISSION_CUTSCENE_NAR_MET_EXT 
		
			sCutsceneName = "MPH_NAR_MET_EXT"  
			
		break 
		case ciMISSION_CUTSCENE_HUM_KEY_EXT 
			
			sCutsceneName = "MPH_HUM_KEY_EXT"  
			
		break 	
		
		case ciMISSION_CUTSCENE_PAC_WIT_MCS2
		
			sCutsceneName = "MPH_PAC_WIT_MCS2" 
		
		BREAK
		
		case ciMISSION_CUTSCENE_PAC_PHO_EXT 
		
			sCutsceneName = "MPH_PAC_PHO_EXT"    
			
		break 	
		case ciMISSION_CUTSCENE_NAR_TRA_EXT 
		
			sCutsceneName = "MPH_NAR_TRA_EXT"  
			
		break 
		case ciMISSION_CUTSCENE_TUT_MID 
			
			sCutsceneName = "MPH_TUT_MID"  
			
		break 	
		
		case ciMISSION_CUTSCENE_PAC_FIN_MCS1
		
			sCutsceneName = "MPH_PAC_FIN_MCS1" 
		
		BREAK
		
		case ciMISSION_CUTSCENE_HUM_KEY_MCS1
		
			sCutsceneName = "MPH_HUM_KEY_MCS1" 
		
		BREAK
		
		case ciMISSION_CUTSCENE_PRI_UNF_EXT
		
			sCutsceneName = "MPH_PRI_UNF_EXT" 
		
		BREAK
		
		case ciMISSION_CUTSCENE_PRI_FIN_EXT
			sCutsceneName = "MPH_PRI_FIN_EXT" 
		BREAK
		
		case ciMISSION_CUTSCENE_HUM_DEL_EXT
			sCutsceneName = "MPH_HUM_DEL_EXT" 
		BREAK
		
		//Lowrider
		
		CASE ciMISSION_CUTSCENE_LOW_DRV_EXT
			sCutsceneName = "LOW_DRV_EXT" 
		BREAK
		
		CASE ciMISSION_CUTSCENE_LOW_FIN_EXT
			sCutsceneName = "LOW_FIN_EXT" 
		BREAK
		
		CASE ciMISSION_CUTSCENE_LOW_FIN_MCS1
			sCutsceneName = "LOW_FIN_MCS1" 
		BREAK
		
		CASE ciMISSION_CUTSCENE_LOW_FUN_EXT
			sCutsceneName = "LOW_FUN_EXT" 
		BREAK
		
		CASE ciMISSION_CUTSCENE_LOW_FUN_MCS1
			sCutsceneName = "LOW_FUN_MCS1" 
		BREAK
		
		CASE ciMISSION_CUTSCENE_LOW_PHO_EXT
			sCutsceneName = "LOW_PHO_EXT" 
		BREAK
		
		CASE ciMISSION_CUTSCENE_LOW_TRA_EXT
			sCutsceneName = "LOW_TRA_EXT" 
		BREAK
		
		//Heists 2
		CASE ciMISSION_CUTSCENE_SILJ_MCS1
			sCutsceneName = "SILJ_MCS1" 
		BREAK
		
		CASE ciMISSION_CUTSCENE_SILJ_EXT
			sCutsceneName = "SILJ_EXT" 
		BREAK
		
		CASE ciMISSION_CUTSCENE_IAAJ_EXT
			sCutsceneName = "IAAJ_EXT" 
		BREAK
		
		CASE ciMISSION_CUTSCENE_SUBJ_MCS0
			sCutsceneName = "SUBJ_MCS0" 
		BREAK
		
		CASE ciMISSION_CUTSCENE_SUBJ_EXT
			sCutsceneName = "SUBJ_EXT" 
		BREAK
		
		CASE ciMISSION_CUTSCENE_SILJ_INT
			sCutsceneName = "SILJ_INT" 
		BREAK
		
		//Casino 1
		CASE ciMISSION_CUTSCENE_CASINO_1_EXT
			sCutsceneName = "MPCAS1_EXT" 
		BREAK		
		CASE ciMISSION_CUTSCENE_CASINO_3_EXT
			sCutsceneName = "MPCAS3_EXT" 
		BREAK
		CASE ciMISSION_CUTSCENE_CASINO_2_EXT
			sCutsceneName = "MPCAS2_EXT" 
		BREAK
		CASE ciMISSION_CUTSCENE_CASINO_5_EXT
			sCutsceneName = "MPCAS5_EXT" 
		BREAK
		CASE ciMISSION_CUTSCENE_CASINO_6_EXT
			sCutsceneName = "MPCAS6_EXT" 
		BREAK
		
		//Casino 2 - Heist		
		CASE ciMISSION_CUTSCENE_CASINO_HEIST_DRP1
			sCutsceneName = "HS3F_ALL_DRP1" 
		BREAK
		CASE ciMISSION_CUTSCENE_CASINO_HEIST_DRP2
			sCutsceneName = "HS3F_ALL_DRP2" 
		BREAK
		CASE ciMISSION_CUTSCENE_CASINO_HEIST_DRP3
			sCutsceneName = "HS3F_ALL_DRP3" 
		BREAK
		CASE ciMISSION_CUTSCENE_CASINO_HEIST_ESC
			sCutsceneName = "HS3F_ALL_ESC" 
		BREAK
		CASE ciMISSION_CUTSCENE_CASINO_HEIST_RP1
			sCutsceneName = "HS3F_MUL_RP1" 
		BREAK
		CASE ciMISSION_CUTSCENE_CASINO_HEIST_ENT
			sCutsceneName = "HS3F_DIR_ENT" 
		BREAK
		CASE ciMISSION_CUTSCENE_CASINO_HEIST_VLT
			sCutsceneName = "HS3F_SUB_VLT" 
		BREAK
		CASE ciMISSION_CUTSCENE_CASINO_HEIST_SEW
			sCutsceneName = "HS3F_DIR_SEW" 
		BREAK
		CASE ciMISSION_CUTSCENE_CASINO_HEIST_CEL
			sCutsceneName = "HS3F_SUB_CEL" 
		BREAK
		CASE ciMISSION_CUTSCENE_CASINO_HEIST_ESC_B
			sCutsceneName = "HS3F_ALL_ESC_B"
		BREAK
		
	ENDSWITCH
	
	//PRINTLN("[RCC MISSION] GET_MOCAP_CUTSCENE_NAME - Returning sCutsceneName = ",sCutsceneName)
	
	RETURN sCutsceneName

ENDFUNC

///PURPOSE: This function gets the closest player to the cutscene location and returns that player
FUNC PLAYER_INDEX GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM(STRING strCutsceneName, INT iteam, FMMC_CUTSCENE_TYPE cutType, INT icutscene, BOOL bUsedFallbackRange)
	
	VECTOR vCutPos = GET_CUTSCENE_COORDS(iteam, cutType)
	FLOAT fSmallestDist
	
	IF ARE_STRINGS_EQUAL(strCutsceneName, "MPH_PRI_FIN_MCS2")
		PRINTLN("[RCC MISSION] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - Overriding fSmallestDist for MPH_PRI_FIN_MCS2 - WAS = ", fSmallestDist, " NOW = ", 999999)
		fSmallestDist = 999999
	ELIF bUsedFallbackRange
		fSmallestDist = 999999
	ELSE
		fSmallestDist = GET_CUTSCENE_PLAYER_INCLUSION_RANGE(strCutsceneName, iteam, iCutscene, cutType)
	ENDIF
	
	PRINTLN("[RCC MISSION] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - START")
	PRINTLN("[RCC MISSION] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM 		vCutPos = ", vCutPos)
	PRINTLN("[RCC MISSION] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM 		bUsedFallbackRange = ", BOOL_TO_STRING(bUsedFallbackRange))
	PRINTLN("[RCC MISSION] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM 		cutType = ", cutType)
	PRINTLN("[RCC MISSION] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM 		strCutsceneName = ", strCutsceneName)
	PRINTLN("[RCC MISSION] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM 		inclusionRange(fSmallestDist) = ", fSmallestDist)
	
	INT iPart
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() ipart
		
		IF IS_BIT_SET(MC_playerBD[ipart].iClientBitSet, PBBOOL_START_SPECTATOR)
			PRINTLN("[RCC MISSION] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - PBBOOL_START_SPECTATOR = TRUE")
			RELOOP
		ENDIF
		
		IF IS_BIT_SET(MC_playerBD[ipart].iClientBitSet, PBBOOL_EARLY_END_SPECTATOR)
			PRINTLN("[RCC MISSION] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - PBBOOL_EARLY_END_SPECTATOR = TRUE")
			RELOOP
		ENDIF
		
		IF NOT IS_BIT_SET(MC_playerBD[ipart].iClientBitSet2, PBBOOL2_REACHED_GAME_STATE_RUNNING) // prevent very early jips
			PRINTLN("[RCC MISSION] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - PBBOOL2_REACHED_GAME_STATE_RUNNING = FALSE")
			RELOOP
		ENDIF		
		
		IF IS_BIT_SET(MC_serverBD.iCutscenePlayerBitset[iteam], ipart)
			PRINTLN("[RCC MISSION] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - IS_BIT_SET(MC_serverBD.iCutscenePlayerBitset[iteam], ipart) = TRUE")
			RELOOP
		ENDIF
			
		IF MC_playerBD[ipart].iteam = iteam
		OR (DOES_TEAM_LIKE_TEAM(MC_playerBD[ipart].iteam,iteam)
		AND SHOULD_TEAM_BE_IN_CUTSCENE(cutType, icutscene, MC_playerBD[ipart].iteam , iteam))
			
			//b*2194619
			IF ARE_STRINGS_EQUAL(strCutsceneName, "MPH_PRI_FIN_MCS2")
			AND MC_playerBD[ipart].iteam = 3
				PRINTLN("[RCC MISSION] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - Special case or MPH_PRI_FIN_MCS2 for team 3")
				RELOOP
			ENDIF
			
			PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(ipart)
			IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				PRINTLN("[RCC MISSION] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - Inactive participant")
				RELOOP
			ENDIF
			
			PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
			IF NOT IS_NET_PLAYER_OK(tempPlayer, NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_PlayEndCutsceneForAllPlayers))
				PRINTLN("[RCC MISSION] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - net player not OK")
				RELOOP
			ENDIF
			
			IF IS_PLAYER_IN_PROCESS_OF_JOINING_MISSION_AS_SPECTATOR(tempPlayer)
				PRINTLN("[RCC MISSION] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - ciFMMC_MISSION_DATA_BitSet_Set_Up_Join_As_Spectator reloop")
				RELOOP
			ENDIF			
			
			PED_INDEX tempPed = GET_PLAYER_PED(tempPlayer)
			
			FLOAT fDist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(tempPed, vCutPos)
			IF fDist > fSmallestDist
			AND NOT IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()			
				PRINTLN("[RCC MISSION] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - Out of range of fSmallestDist: ", fSmallestDist)
				RELOOP
			ENDIF

			PRINTLN("[RCC MISSION] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - Found a player = ReturnPlayer")
			RETURN tempPlayer

		ENDIF

	ENDREPEAT
	
	PRINTLN("[RCC MISSION] [PED CUT] GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - Failed to find a player")
	RETURN INVALID_PLAYER_INDEX()
ENDFUNC

FUNC BOOL IS_CUTSCENE_REQUESTED_FOR_TEAM( INT iteam )

	SWITCH iteam
	
		CASE 0
			IF IS_BIT_SET( MC_ServerBD.iServerBitSet1, SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_0 )
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE 1
			IF IS_BIT_SET( MC_ServerBD.iServerBitSet1, SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_1 )
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE 2
			IF IS_BIT_SET( MC_ServerBD.iServerBitSet1, SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_2 )
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE 3
			IF IS_BIT_SET( MC_ServerBD.iServerBitSet1, SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_3 )
				RETURN TRUE
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE


ENDFUNC

FUNC BOOL SHOULD_THIS_TEAM_SEE_THIS_CUTSCENE( INT iTeam, BOOL bIsEndMocap, INT iCutscene )
	
	IF bIsEndMocap
		RETURN TRUE
	ENDIF
	
	IF iCutscene < 0
		RETURN TRUE
	ENDIF
	
	SWITCH iTeam
		CASE 0
			IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[ icutscene ].iCutsceneBitset,ci_CSBS_Pull_Team0_In)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 1
			IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[ icutscene ].iCutsceneBitset,ci_CSBS_Pull_Team1_In)
				RETURN TRUE
			ENDIF		
		BREAK
		CASE 2
			IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[ icutscene ].iCutsceneBitset,ci_CSBS_Pull_Team2_In)
				RETURN TRUE
			ENDIF		
		BREAK
		CASE 3
			IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[ icutscene ].iCutsceneBitset,ci_CSBS_Pull_Team3_In)
				RETURN TRUE
			ENDIF		
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

///PURPOSE: Clears data stating which teams should watch iTeamToClear's cutscene
PROC CLEAR_CUTSCENE_TEAM_CONSIDERATION_BITSET( INT iTeamToClear )
	
	SWITCH iTeamToClear
		CASE 0
			CLEAR_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM0_CUTSCENE_SEEN_BY_TEAM0 )
			CLEAR_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM0_CUTSCENE_SEEN_BY_TEAM1 )
			CLEAR_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM0_CUTSCENE_SEEN_BY_TEAM2 )
			CLEAR_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM0_CUTSCENE_SEEN_BY_TEAM3 )
		BREAK
		
		CASE 1
			CLEAR_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM1_CUTSCENE_SEEN_BY_TEAM0 )
			CLEAR_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM1_CUTSCENE_SEEN_BY_TEAM1 )
			CLEAR_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM1_CUTSCENE_SEEN_BY_TEAM2 )
			CLEAR_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM1_CUTSCENE_SEEN_BY_TEAM3 )
		BREAK
		
		CASE 2
			CLEAR_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM2_CUTSCENE_SEEN_BY_TEAM0 )
			CLEAR_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM2_CUTSCENE_SEEN_BY_TEAM1 )
			CLEAR_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM2_CUTSCENE_SEEN_BY_TEAM2 )
			CLEAR_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM2_CUTSCENE_SEEN_BY_TEAM3 )
		BREAK
		
		CASE 3
			CLEAR_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM3_CUTSCENE_SEEN_BY_TEAM0 )
			CLEAR_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM3_CUTSCENE_SEEN_BY_TEAM1 )
			CLEAR_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM3_CUTSCENE_SEEN_BY_TEAM2 )
			CLEAR_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM3_CUTSCENE_SEEN_BY_TEAM3 )
		BREAK
	ENDSWITCH
	
	CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene has been cleared for team ", iTeamToClear )
	CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene is now ", MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene )
ENDPROC

///PURPOSE: Set that iThisTeam should watch iCutsceneTeam's cutscene
///    
///PARAMETERS:
///    	* iThisTeam - The team that wants to watch the cutscene
///     * iCutsceneTeam - The team that owns the cutscene
///    
PROC SET_THIS_TEAM_SHOULD_WATCH_OTHER_TEAM_CUTSCENE( INT iThisTeam, INT iCutsceneTeam )
	
	IF iThisTeam < 0 OR iThisTeam > 3
		CASSERTLN( DEBUG_CONTROLLER, "[RCC MISSION] SET_THIS_TEAM_SHOULD_WATCH_OTHER_TEAM_CUTSCENE - iThisTeam is invalid - something has gone terribly wrong" )
	ENDIF
	
	IF iCutsceneTeam < 0 OR iCutsceneTeam > 3
		CASSERTLN( DEBUG_CONTROLLER, "[RCC MISSION] SET_THIS_TEAM_SHOULD_WATCH_OTHER_TEAM_CUTSCENE - iCutsceneTeam is invalid - something has gone terribly wrong" )
	ENDIF
	
	SWITCH iCutsceneTeam
		CASE 0
			SET_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM0_CUTSCENE_SEEN_BY_TEAM0 + iThisTeam )	
		BREAK
		CASE 1
			SET_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM1_CUTSCENE_SEEN_BY_TEAM0 + iThisTeam )	
		BREAK
		CASE 2
			SET_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM2_CUTSCENE_SEEN_BY_TEAM0 + iThisTeam )	
		BREAK
		CASE 3
			SET_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM3_CUTSCENE_SEEN_BY_TEAM0 + iThisTeam )	
		BREAK
	ENDSWITCH
	
	CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Setting team ", iThisTeam, " should watch team ", iCutsceneTeam,"'s cutscene." )
	CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene is now ", MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene )
ENDPROC

///PURPOSE: Returns TRUE if iThisTeam should watch iCutsceneTeam's cutscene
///    
///PARAMETERS:
///    	* iThisTeam - The team that wants to watch the cutscene
///     * iCutsceneTeam - The team that owns the cutscene
///    
///RETURNS: TRUE if iThisTeam should watch iCutsceneTeam's cutscene, otherwise FALSE
FUNC BOOL SHOULD_THIS_TEAM_WATCH_OTHER_TEAM_CUTSCENE( INT iThisTeam, INT iCutsceneTeam )
	
	IF iThisTeam < 0 OR iThisTeam > 3
		CASSERTLN( DEBUG_CONTROLLER, "[RCC MISSION] SET_THIS_TEAM_SHOULD_WATCH_OTHER_TEAM_CUTSCENE - iThisTeam is invalid - something has gone terribly wrong" )
	ENDIF
	
	IF iCutsceneTeam < 0 OR iCutsceneTeam > 3
		CASSERTLN( DEBUG_CONTROLLER, "[RCC MISSION] SET_THIS_TEAM_SHOULD_WATCH_OTHER_TEAM_CUTSCENE - iCutsceneTeam is invalid - something has gone terribly wrong" )
	ENDIF
	
	BOOL bReturn = FALSE
	
	IF iThisTeam = iCutsceneTeam
		bReturn = TRUE
	ENDIF
	
	SWITCH iCutsceneTeam
		CASE 0
			IF IS_BIT_SET( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM0_CUTSCENE_SEEN_BY_TEAM0 + iThisTeam )
				bReturn = TRUE
			ENDIF
		BREAK
		CASE 1
			IF IS_BIT_SET( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM1_CUTSCENE_SEEN_BY_TEAM0 + iThisTeam )	
				bReturn = TRUE
			ENDIF
		BREAK
		CASE 2
			IF IS_BIT_SET( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM2_CUTSCENE_SEEN_BY_TEAM0 + iThisTeam )	
				bReturn = TRUE
			ENDIF
		BREAK
		CASE 3
			IF IS_BIT_SET( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM3_CUTSCENE_SEEN_BY_TEAM0 + iThisTeam )	
				bReturn = TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN bReturn
ENDFUNC

FUNC INT GET_NUMBER_OF_PEOPLE_SHOULD_BE_WATCHING_CUTSCENE( INT iTeam )
	INT iReturn = 0
	
	INT iTeamLoop = 0
	FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS - 1
		IF SHOULD_THIS_TEAM_WATCH_OTHER_TEAM_CUTSCENE( iTeamLoop, iTeam )
			iReturn += MC_ServerBD_2.iNumPartPlayingAndFinished[ iTeamLoop ] + MC_ServerBD_1.iNumCutsceneSpectatorPlayers[ iTeamLoop ]
		ENDIF
	ENDFOR
	
	RETURN iReturn
ENDFUNC

FUNC INT GET_NUMBER_OF_PEOPLE_FINISHED_CUTSCENE( INT iTeam )
	INT iReturn = 0
	
	INT iTeamLoop = 0
	FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS - 1
		IF SHOULD_THIS_TEAM_WATCH_OTHER_TEAM_CUTSCENE( iTeamLoop, iTeam )
			iReturn += MC_serverBD.iNumCutscenePlayersFinished[ iTeamLoop ]
		ENDIF
	ENDFOR
	
	RETURN iReturn
ENDFUNC

FUNC BOOL ARE_ANY_PLAYERS_ON_TEAM_REQUESTING_CUTSCENE(INT iTeam)
	INT i
	
	FOR i = 0 TO NUM_NETWORK_PLAYERS-1
		IF IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_REQUEST_CUTSCENE_PLAYERS)
		AND MC_PlayerBD[i].iTeam = iTeam
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] ARE_ANY_PLAYERS_ON_TEAM_REQUESTING_CUTSCENE team ", iTeam , " iPlayer: ", i, " Requesting cutscene still...")
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_EVERYONE_FINISHED_WATCHING_THIS_TEAMS_CUTSCENE( INT iTeam )
	IF GET_NUMBER_OF_PEOPLE_SHOULD_BE_WATCHING_CUTSCENE( iTeam ) > 0
		IF GET_NUMBER_OF_PEOPLE_SHOULD_BE_WATCHING_CUTSCENE( iTeam ) <= GET_NUMBER_OF_PEOPLE_FINISHED_CUTSCENE( iTeam )
			IF NOT ARE_ANY_PLAYERS_ON_TEAM_REQUESTING_CUTSCENE(iTeam)
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] HAS_EVERYONE_FINISHED_WATCHING_THIS_TEAMS_CUTSCENE returning TRUE for team ", iTeam )
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] GET_NUMBER_OF_PEOPLE_SHOULD_BE_WATCHING_CUTSCENE( iTeam ) is ",
					GET_NUMBER_OF_PEOPLE_SHOULD_BE_WATCHING_CUTSCENE( iTeam ) ," GET_NUMBER_OF_PEOPLE_FINISHED_CUTSCENE( iTeam ) is ", GET_NUMBER_OF_PEOPLE_FINISHED_CUTSCENE( iTeam ) )
				RETURN TRUE
			ELSE
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] HAS_EVERYONE_FINISHED_WATCHING_THIS_TEAMS_CUTSCENE team ", iTeam , " Players are still requesting cutscene!!!!")
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC RESET_SERVER_CUTSCENE_DATA( INT iCutsceneTeam )
	#IF IS_DEBUG_BUILD
	PRINTLN("RESET_SERVER_CUTSCENE_DATA")
	DEBUG_PRINTCALLSTACK()
	#ENDIF

	INT iplayer

	FOR iplayer = 0 TO ( FMMC_MAX_CUTSCENE_PLAYERS - 1 )
		MC_serverBD.piCutscenePlayerIDs[ iCutsceneTeam ][ iplayer ] = INVALID_PLAYER_INDEX()
	ENDFOR
	MC_serverBD.iCutscenePlayerBitset[ iCutsceneTeam ] = 0
	MC_serverBD.iCutsceneStreamingIndex[ iCutsceneTeam ] = -1
	MC_serverBD.iCutsceneID[iCutsceneTeam] = -1
	
	MC_serverBD.dropOffAtApartmentPlayer = INVALID_PLAYER_INDEX()
	
	CLEAR_BIT(MC_serverBD.iServerBitSet, SBBOOL_CUTSCENE_TEAM0_PLAYERS_SELECTED)
	CLEAR_BIT(MC_serverBD.iServerBitSet, SBBOOL_CUTSCENE_TEAM1_PLAYERS_SELECTED)
	CLEAR_BIT(MC_serverBD.iServerBitSet, SBBOOL_CUTSCENE_TEAM2_PLAYERS_SELECTED)
	CLEAR_BIT(MC_serverBD.iServerBitSet, SBBOOL_CUTSCENE_TEAM3_PLAYERS_SELECTED)
	
	CLEAR_BIT(MC_ServerBD.iServerBitSet1, SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_0)
	CLEAR_BIT(MC_ServerBD.iServerBitSet1, SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_1)
	CLEAR_BIT(MC_ServerBD.iServerBitSet1, SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_2)
	CLEAR_BIT(MC_ServerBD.iServerBitSet1, SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_3)

ENDPROC

///PURPOSE: This function sets all the teams that should watch iCutsceneTeam's cutscene
PROC SET_TEAMS_THAT_WATCH_THIS_TEAMS_CUTSCENE( INT iCutsceneTeam )
	IF MC_serverBD.iCutsceneID[iCutsceneTeam] > -1
	
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] SET_TEAMS_THAT_WATCH_THIS_TEAMS_CUTSCENE() iCutsceneID value passed is ",  MC_serverBD.iCutsceneID[ iCutsceneTeam ], " for team " , iCutsceneTeam )
		
		INT iCutsceneIndex = -1
		FMMC_CUTSCENE_TYPE eCutType
		IF MC_serverBD.iCutsceneID[ iCutsceneTeam ] >= FMMC_GET_MAX_SCRIPTED_CUTSCENES() AND MC_serverBD.iCutsceneID[ iCutsceneTeam ] < FMMC_GET_MAX_CUTSCENES_WITH_MOCAP()
			iCutsceneIndex = MC_serverBD.iCutsceneID[ iCutsceneTeam ] - FMMC_GET_MAX_SCRIPTED_CUTSCENES()
			eCutType = FMMCCUT_MOCAP
		ELIF MC_serverBD.iCutsceneID[ iCutsceneTeam ] < FMMC_GET_MAX_SCRIPTED_CUTSCENES()
			iCutsceneIndex = MC_serverBD.iCutsceneID[ iCutsceneTeam ]
			eCutType = FMMCCUT_SCRIPTED
		ENDIF
		
		INT iTeamLoop = 0
		FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS -1

			IF iCutsceneIndex > -1
				IF SHOULD_TEAM_BE_IN_CUTSCENE(eCutType, iCutsceneIndex, iTeamLoop ,iCutsceneTeam)
					SET_THIS_TEAM_SHOULD_WATCH_OTHER_TEAM_CUTSCENE( iTeamLoop, iCutsceneTeam )
				ENDIF
			ENDIF

		ENDFOR
	
	ENDIF
ENDPROC

PROC PROCESS_CUTSCENE_OBJECTIVE_LOGIC( INT iTeam, INT iPlayerRule )

	IF NOT HAS_TEAM_FAILED(iteam)

		IF MC_serverBD.iCutsceneID[iteam] = -1
			IF MC_serverBD.iPlayerRuleLimit[iPlayerRule][iteam] >= 0
			AND MC_serverBD.iPlayerRuleLimit[iPlayerRule][iteam] < FMMC_GET_MAX_SCRIPTED_CUTSCENES()
				IF NOT IS_BIT_SET(MC_serverBD.iCutsceneFinished[iteam], MC_serverBD_4.iPlayerRulePriority[iPlayerRule][iteam])
					INT iScriptedCutscene = MC_serverBD.iPlayerRuleLimit[iPlayerRule][iteam]
					
					IF MC_serverBD.iOverriddenScriptedCutsceneRule[iTeam] >-1
					AND MC_serverBD.iOverriddenScriptedCutsceneIndex[iTeam] > -1
						IF MC_serverBD.iOverriddenScriptedCutsceneRule[iTeam] = MC_serverBD_4.iPlayerRulePriority[iPlayerRule][iTeam]
							iScriptedCutscene = MC_serverBD.iOverriddenScriptedCutsceneIndex[iTeam]
							PRINTLN( "[RCC MISSION] PROCESS_CUTSCENE_OBJECTIVE_LOGIC() iScriptedCutscene being overridden from ", MC_serverBD.iPlayerRuleLimit[iPlayerRule][iteam]," to ", iScriptedCutscene, " for team ", iTeam, " on player rule ", iPlayerRule)
						ENDIF
					ENDIF
					
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sScriptedCutsceneData[iScriptedCutscene].tlName)
						MC_serverBD.iCutsceneID[iteam] = iScriptedCutscene
						PRINTLN( "[RCC MISSION] PROCESS_CUTSCENE_OBJECTIVE_LOGIC() iCutsceneID value passed is ",  MC_serverBD.iCutsceneID[ iteam ], " for team " , iteam )
						SET_TEAMS_THAT_WATCH_THIS_TEAMS_CUTSCENE( iteam )
						REINIT_NET_TIMER(MC_serverBD.tdCutsceneStartTime[iteam],FALSE,TRUE)
					ENDIF
				ENDIF
			ELIF MC_serverBD.iPlayerRuleLimit[iPlayerRule][iteam] < FMMC_GET_MAX_CUTSCENES_WITH_MOCAP()
				IF NOT IS_BIT_SET(MC_serverBD.iCutsceneFinished[iteam], MC_serverBD_4.iPlayerRulePriority[iPlayerRule][iteam])
					INT iMocap = (MC_serverBD.iPlayerRuleLimit[iPlayerRule][iteam] - FMMC_GET_MAX_SCRIPTED_CUTSCENES())
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sMocapCutsceneData[iMocap].tlName)
						MC_serverBD.iCutsceneID[iteam] = MC_serverBD.iPlayerRuleLimit[iPlayerRule][iteam]
						SET_TEAMS_THAT_WATCH_THIS_TEAMS_CUTSCENE( iteam )
						PRINTLN( "[RCC MISSION] PROCESS_CUTSCENE_OBJECTIVE_LOGIC() MOCAP iCutsceneID value passed is ",  MC_serverBD.iCutsceneID[ iteam ], " for team " , iteam )
						REINIT_NET_TIMER(MC_serverBD.tdCutsceneStartTime[iteam],FALSE,TRUE)
					ENDIF
				ENDIF

			ENDIF

		ENDIF
	ENDIF
	
ENDPROC

PROC SERVER_SELECT_END_MOCAP(INT iteam)
	
	INT iteam2
	
	IF MC_serverBD.iEndCutsceneNum[iteam] = -1
		IF MC_ServerBD.iEndCutscene != ciMISSION_CUTSCENE_DEFAULT
		OR IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
			IF MC_serverBD_4.iCurrentHighestPriority[iteam] >= MC_serverBD.iMaxObjectives[iteam]
			#IF IS_DEBUG_BUILD
			OR iPlayerPressedS != -1
			#ENDIF
				INT iEntityType = -1
				IF MC_serverBD.iNumLocHighestPriority[iteam] > 0
					iEntityType = ci_TARGET_LOCATION
				ENDIF
				IF MC_serverBD.iNumPedHighestPriority[iteam] > 0
					iEntityType = ci_TARGET_PED
				ENDIF
				IF MC_serverBD.iNumVehHighestPriority[iteam] > 0
					iEntityType = ci_TARGET_VEHICLE
				ENDIF
				IF MC_serverBD.iNumObjHighestPriority[iteam] > 0
					iEntityType = ci_TARGET_OBJECT
				ENDIF
				
				MC_serverBD.iEndCutsceneEntityType[iteam] = iEntityType
				MC_serverBD.iEndCutsceneNum[iteam] = SERVER_CHOOSE_MOCAP_END_CUTSCENE( MC_ServerBD.iEndCutscene, iEntityType )
				PRINTLN("[RCC MISSION] SERVER_SELECT_END_MOCAP - MC_serverBD.iEndCutsceneNum[iteam] = ",MC_serverBD.iEndCutsceneNum[iteam]," for team ",iteam)
				
				FOR iteam2 = 0 TO (FMMC_MAX_TEAMS-1)
					IF DOES_TEAM_LIKE_TEAM(iteam2,iteam)
						MC_serverBD.iEndCutsceneNum[iteam2] = MC_serverBD.iEndCutsceneNum[iteam] 
						MC_serverBD.iEndCutsceneEntityType[iteam2] = MC_serverBD.iEndCutsceneEntityType[iteam]
						PRINTLN("[RCC MISSION] SERVER_SELECT_END_MOCAP - 2; MC_serverBD.iEndCutsceneNum[iteam2] = ",MC_serverBD.iEndCutsceneNum[iteam2]," for team ",iteam2)
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

//Check if the cutscene needs players
FUNC BOOL DOES_MOCAP_CUTSCENE_NEED_PLAYERS(STRING cutname, INT iCutsceneToUse)

	IF NOT IS_STRING_NULL_OR_EMPTY(cutname)
		//Manual block list for cutscenes not requiring players
		//IF ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_PRI_UNF_MCS1") // commented out. KW 
		IF ARE_STRINGS_EQUAL(cutname, "MPH_PRI_UNF_MCS1")
		OR ARE_STRINGS_EQUAL(cutname, "sil_pred_mcs1")		
			PRINTLN("[RCC MISSION] DOES_MOCAP_CUTSCENE_NEED_PLAYERS - CUTSCENE Special Case - Returning False")
			RETURN FALSE
		ENDIF
		
		IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO)
			IF IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Hide_Your_Player) AND IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].iCutsceneBitset, ci_CSBS_Hide_Other_Players)
				PRINTLN("[RCC MISSION] DOES_MOCAP_CUTSCENE_NEED_PLAYERS - CUTSCENE All Players Set to hidden - Returning False")
				RETURN FALSE
			ENDIF
		ENDIF
		
		PRINTLN("[RCC MISSION] DOES_MOCAP_CUTSCENE_NEED_PLAYERS - Returning TRUE")
		
		RETURN TRUE
	ENDIF

	//Shouldn't return this
	PRINTLN("[RCC MISSION] DOES_MOCAP_CUTSCENE_NEED_PLAYERS - CUTSCENE STRING WAS EMPTY")
	RETURN TRUE

ENDFUNC

FUNC BOOL SHOULD_USE_THIRD_PLAYER_FOR_MOCAP(BOOL bEndMocap, INT iCutsceneToUse)
	IF bEndMocap
		RETURN IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitset2, ci_CSBS2_Heist_Leader_Third)
	ELIF iCutsceneToUse != -1
		RETURN IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitset2, ci_CSBS2_Heist_Leader_Third)
	ENDIF
	RETURN FALSE
ENDFUNC

//Update the bit set for the apartment player selected
PROC UPDATE_APARTMENT_PLAYER_FOR_CUTSCENE(PLAYER_INDEX apartPlayer, INT iteam)

	INT iPartID = 0
	PARTICIPANT_INDEX partID = NULL
	
	INT iToCount = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]
	INT iCounted

	FOR iPartID = 0 TO MAX_NUM_MC_PLAYERS - 1
	
		partID = INT_TO_PARTICIPANTINDEX( iPartID )

		IF NETWORK_IS_PARTICIPANT_ACTIVE( partID )
			
			PLAYER_INDEX tPlayer
			tPlayer = NETWORK_GET_PLAYER_INDEX( partID )
			
			IF tPlayer = apartPlayer
				SET_BIT( MC_serverBD.iCutscenePlayerBitset[ iteam ], iPartID )
				iPartID = MAX_NUM_MC_PLAYERS // Break out!
			ELIF IS_NET_PLAYER_OK(tPlayer)
			AND NOT IS_PLAYER_SPECTATOR_ONLY(tPlayer)
				iCounted++
				
				IF iCounted >= iToCount
					iPartID = MAX_NUM_MC_PLAYERS // Break out!
				ENDIF
			ENDIF
			
		ENDIF
	ENDFOR
	
ENDPROC

FUNC PLAYER_INDEX GET_CUTSCENE_PLAYER_SPECIAL_CASE(INT iTeam, INT iPlayer, INT iCutsceneToUse, STRING strCutscene, FMMC_CUTSCENE_TYPE cutType)

	PLAYER_INDEX TempPlayer = INVALID_PLAYER_INDEX()

	// Apartment cutscenes
	IF iPlayer = 0
	AND NOT IS_STRING_NULL_OR_EMPTY(strCutscene)
	AND IS_THIS_MOCAP_IN_AN_APARTMENT(strCutscene)		
	AND MC_serverBD.dropOffAtApartmentPlayer != INVALID_PLAYER_INDEX()

		#IF IS_DEBUG_BUILD
		
		IF IS_NET_PLAYER_OK(mc_serverBD.dropOffAtApartmentPlayer)
			PRINTLN("[RCC MISSION] [PED CUT] GET_CUTSCENE_PLAYER_SPECIAL_CASE - setting player to be last objective player for apartment dropoff = ", GET_PLAYER_NAME(mc_serverBD.dropOffAtApartmentPlayer))
		ELSE
			PRINTLN("[RCC MISSION] [PED CUT] GET_CUTSCENE_PLAYER_SPECIAL_CASE - setting player to be last objective player for apartment dropoff = ", NATIVE_TO_INT(mc_serverBD.dropOffAtApartmentPlayer))
		ENDIF
		
		#ENDIF
		
		TempPlayer = mc_serverBD.dropOffAtApartmentPlayer
		UPDATE_APARTMENT_PLAYER_FOR_CUTSCENE(TempPlayer, iteam)
		
		PRINTLN("[RCC mission] GET_CUTSCENE_PLAYER_SPECIAL_CASE - SET_BIT(MC_serverBD.iCutscenePlayerBitset[",iteam,"], ", NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(TempPlayer)), ")" )
		RETURN tempPlayer

	// PRISON FINALE
	ELIF ARE_STRINGS_EQUAL(strCutscene, "MPH_PRI_FIN_MCS2")
			
		PRINTLN("GET_CUTSCENE_PLAYER_SPECIAL_CASE - MPH_PRI_FIN_MCS2 SPECIAL")
		
		IF iPlayer = 1
	
			IF TempPlayer = INVALID_PLAYER_INDEX()
				TempPlayer = GET_CUTSCENE_PLAYER_FROM_VEHICLE(cutType, iteam, GET_CUTSCENE_VEHICLE_STRING(cutType, iCutsceneToUse), iCutsceneToUse, VS_BACK_RIGHT)
			ENDIF
			
			IF TempPlayer = INVALID_PLAYER_INDEX()
				TempPlayer = GET_CUTSCENE_PLAYER_FROM_VEHICLE(cutType, iteam, GET_CUTSCENE_VEHICLE_STRING(cutType, iCutsceneToUse), iCutsceneToUse, VS_BACK_LEFT)
			ENDIF
			
			IF TempPlayer = INVALID_PLAYER_INDEX()
				TempPlayer = GET_HEIST_LEADER_PLAYER_FOR_TEAM(iteam, cutType, iCutsceneToUse,iplayer)
			ENDIF
		
		ELIF iPlayer = 2
		
			IF TempPlayer = INVALID_PLAYER_INDEX()
				TempPlayer = GET_CUTSCENE_PLAYER_FROM_VEHICLE(cutType, iteam, GET_CUTSCENE_VEHICLE_STRING(cutType, iCutsceneToUse), iCutsceneToUse, VS_EXTRA_LEFT_1)
			ENDIF
			
			IF TempPlayer = INVALID_PLAYER_INDEX()
				TempPlayer = GET_HEIST_LEADER_PLAYER_FOR_TEAM(iteam, cutType, iCutsceneToUse, iplayer)
			ENDIF
			
		ENDIF

	ENDIF	

	RETURN TempPlayer
ENDFUNC

///PURPOSE: This function checks creator data and picks the correct player for each role in the cutscene
///    e.g. if the heist leader should be the primary it'll pick them first etc
FUNC PLAYER_INDEX GET_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM(INT iteam, INT iplayer, INT iCutsceneToUse, FMMC_CUTSCENE_TYPE cutType, STRING strCutsceneName)

	PRINTLN("[RCC mission] GET_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM iteam = ", iteam)
	PRINTLN("[RCC mission] GET_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM iplayer = ", iplayer)
	PRINTLN("[RCC mission] GET_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM iCutsceneToUse = ", iCutsceneToUse)
	PRINTLN("[RCC mission] GET_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM cutType = ", GET_FMMC_CUTSCENE_TYPE_NAME(cutType))
	PRINTLN("[RCC mission] GET_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM strCutsceneName = ", strCutsceneName)
	PRINTLN("[RCC mission] GET_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM sMocapCutscene = ", sMocapCutscene)

	// New behaviour - has to be enabled on a case by case basis
	IF AM_I_ON_A_HEIST()
	OR IS_LOADED_MISSION_LOW_RIDER_FLOW_MISSION()
	OR GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
	//OR IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST) [FIX_2020_CONTROLLER] - Use the Pack after casino heist.... So we never forget to do this. 
		PLAYER_INDEX tempPlayer = INVALID_PLAYER_INDEX()
		
		//
		// SPECIAL CASES
		//
		tempPlayer = GET_CUTSCENE_PLAYER_SPECIAL_CASE(iTeam, iPlayer, iCutsceneToUse, strCutsceneName, cutType)
		IF tempPlayer != INVALID_PLAYER_INDEX()
			PRINTLN("[RCC MISSION] GET_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - iTeam: ", iteam, " iPlayer: ", iPlayer, " Found player: ", NATIVE_TO_INT(tempPlayer), " using GET_CUTSCENE_PLAYER_SPECIAL_CASE")
			RETURN tempPlayer
		ENDIF
		
		//
		// HEIST LEADER
		//
		tempPlayer = GET_HEIST_LEADER_PLAYER_FOR_TEAM(iteam, cutType, iCutsceneToUse, iplayer)
		IF tempPlayer != INVALID_PLAYER_INDEX()
			PRINTLN("[RCC MISSION] GET_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - iTeam: ", iteam, " iPlayer: ", iPlayer, " Found player: ", NATIVE_TO_INT(tempPlayer), " using GET_HEIST_LEADER_PLAYER_FOR_TEAM")
			RETURN tempPlayer
		ENDIF

		//
		// VEHICLE 
		//
		VEHICLE_SEAT vehicleSeat
		SWITCH iplayer
			CASE 0	vehicleSeat = VS_DRIVER			BREAK
			CASE 1	vehicleSeat = VS_FRONT_RIGHT	BREAK
			CASE 2	vehicleSeat = VS_BACK_LEFT		BREAK
			CASE 3	vehicleSeat = VS_BACK_RIGHT		BREAK
		ENDSWITCH
		
		IF vehicleSeat = VS_DRIVER 
		OR SHOULD_CUTSCENE_GET_PASSENGER(cutType, iCutsceneToUse)
			TempPlayer = GET_CUTSCENE_PLAYER_FROM_VEHICLE(cutType, iteam, GET_CUTSCENE_VEHICLE_STRING(cutType, iCutsceneToUse), iCutsceneToUse, vehicleSeat)
			IF tempPlayer != INVALID_PLAYER_INDEX()
				PRINTLN("[RCC MISSION] GET_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - iTeam: ", iteam, " iPlayer: ", iPlayer, " Found player: ", NATIVE_TO_INT(tempPlayer), " using GET_CUTSCENE_PLAYER_FROM_VEHICLE")
				RETURN tempPlayer
			ENDIF
		ENDIF

		//
		// CLOSEST PLAYER
		//
		tempPlayer = GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM(strCutsceneName, cutType, iteam, iCutsceneToUse, iplayer)
		IF tempPlayer != INVALID_PLAYER_INDEX()
			PRINTLN("[RCC MISSION] GET_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - iTeam: ", iteam, " iPlayer: ", iPlayer, " Found player: ", NATIVE_TO_INT(tempPlayer), " using GET_CLOSEST_CUTSCENE_PLAYER_OF_TEAM")
			RETURN tempPlayer
		ENDIF
		
		// 
		// OBJECTIVE PLAYER (PRIMARY ONLY)
		//
		IF iplayer = 0
		AND MC_serverBD.piCutscenePlayerIDs[iteam][0] != INVALID_PLAYER_INDEX()
			tempPlayer = MC_serverBD.piCutscenePlayerIDs[iteam][0]
			PRINTLN("[RCC MISSION] GET_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - iTeam: ", iteam, " iPlayer: ", iPlayer, " Found player: ", NATIVE_TO_INT(tempPlayer), " using Objective Completed by this player")
			RETURN tempPlayer
		ENDIF
		
		//
		// ANY SUITIBLE PLAYER FOR TEAM
		//
		tempPlayer = GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM(strCutsceneName, iteam, cutType, iCutsceneToUse, FALSE)
		IF tempPlayer != INVALID_PLAYER_INDEX()
			PRINTLN("[RCC MISSION] GET_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - iTeam: ", iteam, " iPlayer: ", iPlayer, " Found player: ", NATIVE_TO_INT(tempPlayer), " using GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM")
			RETURN tempPlayer
		ENDIF
		
		IF iplayer = 0
		OR (GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		AND NOT g_bBlockInfiniteRangeCheck)
			//
			// ANY SUITIBLE PLAYER FOR TEAM (with fallback range)
			//
			tempPlayer = GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM(strCutsceneName, iteam, cutType, iCutsceneToUse, TRUE)
			IF tempPlayer != INVALID_PLAYER_INDEX()
				PRINTLN("[RCC MISSION] GET_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - iTeam: ", iteam, " iPlayer: ", iPlayer, " Found player: ", NATIVE_TO_INT(tempPlayer), " using GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM(FALLBACK RANGE)")
				RETURN tempPlayer
			ENDIF
		ENDIF
		
	// Legacy behaviour
	ELSE
		
		IF MC_serverBD.piCutscenePlayerIDs[iteam][iplayer] = INVALID_PLAYER_INDEX()
			
			PLAYER_INDEX tempPlayer = INVALID_PLAYER_INDEX()
			tempPlayer = GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM(strCutsceneName, iteam, cutType, iCutsceneToUse, FALSE)
			IF tempPlayer != INVALID_PLAYER_INDEX()
				PRINTLN("[RCC MISSION] GET_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - team: ", iteam, " iPlayer: ", iPlayer, ", tempPlayer: ", NATIVE_TO_INT(tempPlayer), " using GET_ANY_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - Legacy")
				RETURN tempPlayer
			ENDIF
			
		ENDIF
	ENDIF
	
	PRINTLN("[RCC MISSION] GET_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM - team: ", iteam, " iPlayer: ", iPlayer, " failed to find player index")
	RETURN INVALID_PLAYER_INDEX()
ENDFUNC

PROC PROCESS_CUTSCENE_POST_PLAYER_SELECTION(PLAYER_INDEX playerId, INT iPlayer, FMMC_CUTSCENE_TYPE cutType, STRING strMocapName)

	SWITCH cutType
	
		CASE FMMCCUT_ENDMOCAP

			IF IS_STRING_NULL_OR_EMPTY(strMocapName)
				EXIT
			ENDIF
			
			//Specific bit for b*2103194
			IF iPlayer = 0
			AND ARE_STRINGS_EQUAL(strMocapName, "MPH_HUM_DEL_EXT")
				
				PRINTLN("[RCC MISSION] SELECTING FOR CUTSCENE MPH_HUM_DEL_EXT")
				
				IF NOT IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(playerId))
					PRINTLN("[RCC MISSION] SET SBBOOL2_CUTSCENE_ON_FOOT_VISIBLE_CHECK")
					SET_BIT(MC_serverBD.iServerBitSet2, SBBOOL2_CUTSCENE_ON_FOOT_VISIBLE_CHECK)
				ENDIF
				
			ENDIF
		
		BREAK
	
	ENDSWITCH

ENDPROC

PROC SELECT_CUTSCENE_PLAYERS_FOR_TEAM(INT iteam)

	INT iplayer
	INT iCutsceneToUse = -1
	FMMC_CUTSCENE_TYPE cutType
	
	TEXT_LABEL_63 sCutName
	BOOL bUsePlayers = TRUE
	PLAYER_INDEX tempPlayer
	
	IF iteam < 0 AND iteam >= FMMC_MAX_TEAMS
		EXIT
	ENDIF
	
	// Players have already been selected
	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_CUTSCENE_TEAM0_PLAYERS_SELECTED + iTeam)
		EXIT
	ENDIF

	IF NOT IS_CUTSCENE_REQUESTED_FOR_TEAM(iteam)
		EXIT
	ENDIF
		
	PRINTLN("[LM][url:bugstar:5609784] - MC_serverBD.iCutsceneID[iteam]: ", MC_serverBD.iCutsceneID[iteam])
		
	// Scripted cut
	IF MC_serverBD.iCutsceneID[iteam] >= 0 
	AND MC_serverBD.iCutsceneID[iteam] < FMMC_GET_MAX_SCRIPTED_CUTSCENES()

		cutType = FMMCCUT_SCRIPTED
		iCutsceneToUse = MC_serverBD.iCutsceneID[iteam]
		bUsePlayers = TRUE
	
	// Mocap
	ELIF MC_serverBD.iCutsceneID[iteam] >= FMMC_GET_MAX_SCRIPTED_CUTSCENES()
	AND MC_serverBD.iCutsceneID[iteam] < FMMC_GET_MAX_CUTSCENES_WITH_MOCAP()
		
		cutType = FMMCCUT_MOCAP
		iCutsceneToUse = MC_serverBD.iCutsceneID[iteam] - FMMC_GET_MAX_SCRIPTED_CUTSCENES()
		sCutName = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneToUse].tlName
		
		IF NOT IS_STRING_NULL_OR_EMPTY(sCutName) 
			bUsePlayers = DOES_MOCAP_CUTSCENE_NEED_PLAYERS(sCutName, iCutsceneToUse)
			PRINTLN("[RCC MISSION] [PED CUT] SELECT_CUTSCENE_PLAYERS_FOR_TEAM - iTeam: ", iTeam, " - DOES_MOCAP_CUTSCENE_NEED_PLAYERS : ", BOOL_TO_STRING(bUsePlayers))
		ELSE
			PRINTLN("[RCC MISSION] [PED CUT] SELECT_CUTSCENE_PLAYERS_FOR_TEAM - iTeam: ", iTeam, " - sCutName is empty")
		ENDIF
		
	// End Mocap
	ELIF MC_serverBD.iEndCutsceneNum[iteam] != -1
		
		cutType = FMMCCUT_ENDMOCAP
		
		IF IS_STRING_NULL_OR_EMPTY(sCutName)
		AND NOT IS_STRING_NULL_OR_EMPTY(sMocapCutscene)
//			PRINTLN("[RCC MISSION] [PED CUT] SELECT_CUTSCENE_PLAYERS_FOR_TEAM - bEndMocap = TRUE")
			PRINTLN("[RCC MISSION] [PED CUT] SELECT_CUTSCENE_PLAYERS_FOR_TEAM - iTeam: ", iTeam, " - setting sCutName to sMocapCutscene, (", sMocapCutscene, ")")
			sCutName = sMocapCutscene
		ELSE
			PRINTLN("[RCC MISSION] SELECT_CUTSCENE_PLAYERS_FOR_TEAM - iTeam: ", iTeam, " - bEndMocap = TRUE, but either sCutName is filled or sMocapCutscene is empty")
		ENDIF
		
	ELSE
		ASSERTLN("SELECT_CUTSCENE_PLAYERS_FOR_TEAM - iTeam: ", iTeam, " - could not determine cutscene that was requested for this team")
		EXIT
	ENDIF

	PRINTLN("[RCC MISSION] [PED CUT] SELECT_CUTSCENE_PLAYERS_FOR_TEAM - iTeam: ", iteam, 
		" cutType: ", GET_FMMC_CUTSCENE_TYPE_NAME(cutType), 
		" iCutsceneID: ", MC_serverBD.iCutsceneID[iteam],
		" iEndCutsceneNum: ", MC_serverBD.iEndCutsceneNum[iteam])

	IF bUsePlayers
		
		
		IF AM_I_ON_A_HEIST()
		OR IS_LOADED_MISSION_LOW_RIDER_FLOW_MISSION()
		OR GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
			// reset selected participant flags so everyones an option
			MC_serverBD.iCutscenePlayerBitset[iteam] = 0	
		ENDIF
		
		FOR iplayer = 0 TO (FMMC_MAX_CUTSCENE_PLAYERS - 1)
			tempplayer = GET_SUITABLE_CUTSCENE_PLAYER_FOR_TEAM(iteam, iplayer, iCutSceneToUse, cutType, sCutName)
			IF tempplayer != INVALID_PLAYER_INDEX()
			
				MC_serverBD.piCutscenePlayerIDs[iteam][iplayer] = tempplayer
				SET_BIT(MC_serverBD.iCutscenePlayerBitset[iteam], NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(tempplayer)))
			
				PROCESS_CUTSCENE_POST_PLAYER_SELECTION(tempplayer, iplayer, cutType, sCutName)
				
				PRINTLN("[RCC MISSION] [PED CUT] SSELECT_CUTSCENE_PLAYERS_FOR_TEAM - iTeam: ", iTeam, " - ASSINGING PLAYER SLOT - piCutscenePlayerIDs[", iTeam, "][", iPlayer, "] = ", NATIVE_TO_INT(MC_serverBD.piCutscenePlayerIDs[iteam][iplayer]))
			ELSE
				PRINTLN("[RCC MISSION] [PED CUT] SELECT_CUTSCENE_PLAYERS_FOR_TEAM - iTeam: ", iTeam, " - Failed to fill slot - piCutscenePlayerIDs[", iTeam, "][", iPlayer, "] = ", NATIVE_TO_INT(MC_serverBD.piCutscenePlayerIDs[iteam][iplayer]))
			ENDIF
		ENDFOR

	ELSE
		PRINTLN("[RCC MISSION] [PED CUT] SELECT_CUTSCENE_PLAYERS_FOR_TEAM - iTeam: ", iTeam, " - Cutscene set not to use players")
		
		//Set initial player to invalid index
		MC_serverBD.piCutscenePlayerIDs[iteam][0] = INVALID_PLAYER_INDEX()
		MC_serverBD.dropOffAtApartmentPlayer = INVALID_PLAYER_INDEX()
	ENDIF
	
	#IF IS_DEBUG_BUILD
		
		FOR iplayer = 0 TO FMMC_MAX_CUTSCENE_PLAYERS - 1
			IF MC_serverBD.piCutscenePlayerIDs[iteam][iplayer] != INVALID_PLAYER_INDEX()
				PRINTLN("[RCC MISSION] [PED CUT] SELECT_CUTSCENE_PLAYERS_FOR_TEAM - iTeam: ", iTeam, " - MC_serverBD.piCutscenePlayerIDs[", iteam, "][", iplayer, "] = ", NATIVE_TO_INT(MC_serverBD.piCutscenePlayerIDs[iteam][iplayer]), " (", GET_PLAYER_NAME(MC_serverBD.piCutscenePlayerIDs[iteam][iplayer]), ")")
			ELSE
				PRINTLN("[RCC MISSION] [PED CUT] SELECT_CUTSCENE_PLAYERS_FOR_TEAM - iTeam: ", iTeam, " - MC_serverBD.piCutscenePlayerIDs[", iteam, "][", iplayer, "] = ", NATIVE_TO_INT(MC_serverBD.piCutscenePlayerIDs[iteam][iplayer]))
			ENDIF
		ENDFOR
	
	#ENDIF
	
	SET_BIT(MC_serverBD.iServerBitSet, SBBOOL_CUTSCENE_TEAM0_PLAYERS_SELECTED + iTeam)

ENDPROC

FUNC BOOL IS_NEXT_RULE_A_CUTSCENE_RULE_FOR_TEAM( INT iTeam )
	BOOL bReturn = FALSE
	
	INT iPlayerRule = 0
	
	iF iTeam < 0
		RETURN bReturn
	ENDIF

	INT iPriorityToCheck = MC_serverBD_4.iCurrentHighestPriority[ iTeam ] + 1
	
	CPRINTLN( DEBUG_CONTROLLER, " [RCC MISSION] Priority of their next rule is ", iPriorityToCheck )

	FOR iPlayerRule = 0 TO ( FMMC_MAX_RULES - 1 )
	
		IF MC_serverBD_4.iPlayerRulePriority[ iPlayerRule ][ iteam ] <= iPriorityToCheck
		AND iPriorityToCheck < FMMC_MAX_RULES
	
			IF MC_serverBD_4.iPlayerRule[ iPlayerRule ][ iteam ] = FMMC_OBJECTIVE_LOGIC_SCRIPTED_CUTSCENE
			
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Found a potential mocap cutscene: ", MC_serverBD.iPlayerRuleLimit[ iPlayerRule ][ iteam ] ) 

				IF  MC_serverBD.iPlayerRuleLimit[ iPlayerRule ][ iteam ] >= FMMC_GET_MAX_SCRIPTED_CUTSCENES()
				AND MC_serverBD.iPlayerRuleLimit[ iPlayerRule ][ iteam ] < FMMC_GET_MAX_CUTSCENES_WITH_MOCAP()
					
					IF NOT IS_BIT_SET( MC_serverBD.iCutsceneFinished[ iteam ], MC_serverBD_4.iPlayerRulePriority[ iPlayerRule ][ iteam ] )
						INT iMocap = ( MC_serverBD.iPlayerRuleLimit[ iPlayerRule ][ iteam ] - FMMC_GET_MAX_SCRIPTED_CUTSCENES() )
						IF NOT IS_STRING_NULL_OR_EMPTY( g_FMMC_STRUCT.sMocapCutsceneData[ iMocap ].tlName )
							CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] MOCAP is called ", g_FMMC_STRUCT.sMocapCutsceneData[ iMocap ].tlName )
							bReturn = TRUE
							CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] MOCAP MC_serverBD.iCutsceneStreamingIndex[iteam] = ", MC_serverBD.iCutsceneStreamingIndex[ iteam ]," for team: ", iteam ) 
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN bReturn
ENDFUNC

// This function is used by the Heist Apartment Dropoff stuff to remove a busy spinner whilst the screen has renderphases toggled.
// For more information, look at where LBOOL12_LOADING_SPINNER_FOR_DROPOFF is set.
// Author: Owain Davies
PROC CLEAR_APARTMENT_DROPOFF_SPINNER(INT iID)
	UNUSED_PARAMETER( iID )
	
	IF IS_BIT_SET(iLocalBoolCheck12,LBOOL12_LOADING_SPINNER_FOR_DROPOFF)
		BUSYSPINNER_OFF()
		CLEAR_BIT(iLocalBoolCheck12,LBOOL12_LOADING_SPINNER_FOR_DROPOFF)
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] CLEARED busy spinner for apartment dropoff - ",iID)
	ENDIF
ENDPROC

PROC ASSIGN_MOCAP_CUTSCENE_TO_HAVE_EXTENDED_STREAMING_RANGES(FLOAT &fStreamOut, FLOAT &fStreamIn)

	IF ARE_STRINGS_EQUAL( sMocapCutscene, "MPH_PRI_FIN_MCS2" )
		// 2152701 - MW don't know if this was causing this bug but have upped the ranges as the cutscene triggers further out than 300 (between 300-400 from the logs)
		// so we definitely want to be streaming this earlier any way
		fStreamIn = 600
		fStreamOut = 700
	ELIF ARE_STRINGS_EQUAL( sMocapCutscene, "MPH_PRI_FIN_EXT" )
		fStreamIn = 1200
		fStreamOut = 1400
	
	// Casino
	ELIF COMPARE_STRINGS(sMocapCutscene, "MPCAS", FALSE, 5) = 0	// Apparently this is supposed to be "infinite".
		fStreamIn = 6000
		fStreamOut = 9999
		
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_PlayEndCutsceneForAllPlayers) 	
		fStreamIn = 6000
		fStreamOut = 9999
		
	ENDIF

	PRINTLN("[LM][ASSIGN_MOCAP_CUTSCENE_TO_HAVE_EXTENDED_STREAMING_RANGES] fStreamOut: ", fStreamOut, " fStreamIn: ", fStreamIn)
	
ENDPROC

SCRIPT_TIMER stDropoffSafetyTimer
INT iLastFrameStreamingTeam = -1

PROC MANAGE_STREAMING_MOCAP()

#IF IS_DEBUG_BUILD	
	PRINT_DEBUG_APARTMENT_DROPOFF_FLAGS()
#ENDIF

	INT iCurrentCutsceneStreamingTeam = GET_CUTSCENE_TEAM( TRUE )

	IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
	AND IS_CURRENT_RULE_AN_APARTMENT_OR_GARAGE_DROPOFF()
	
		IF NOT IS_BIT_SET( iLocalBoolCheck7, LBOOL7_SHOULD_HOLD_UP_MOCAP_FOR_CUTSCENE )
		OR iCurrentCutsceneStreamingTeam != iLastFrameStreamingTeam
			
			IF NOT IS_NEXT_RULE_A_CUTSCENE_RULE_FOR_TEAM( iCurrentCutsceneStreamingTeam )
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] I'm on a dropoff rule and I DO NOT have a cutscene coming up!" )
				SET_BIT( iLocalBoolCheck8, LBOOL8_NO_MOCAP_AFTER_THIS_DROPOFF )
			ELSE
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] I'm on a dropoff rule and I have a cutscene coming up!" )
				IF IS_BIT_SET( iLocalBoolCheck8, LBOOL8_NO_MOCAP_AFTER_THIS_DROPOFF )
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] We already thought there wasn't one, turns out we were wrong. Clearing the bit." )
					CLEAR_BIT( iLocalBoolCheck8, LBOOL8_NO_MOCAP_AFTER_THIS_DROPOFF )
				ENDIF
			ENDIF
			
			iLastFrameStreamingTeam = iCurrentCutsceneStreamingTeam
			
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Cutscene streaming team has been updated to ", iLastFrameStreamingTeam )
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Setting LBOOL7_SHOULD_HOLD_UP_MOCAP_FOR_CUTSCENE because we're on a dropoff rule" )
			SET_BIT( iLocalBoolCheck7, LBOOL7_SHOULD_HOLD_UP_MOCAP_FOR_CUTSCENE )
		ENDIF
	ENDIF	
	
	INT iMyTeam = MC_playerBD[iPartToUse].iteam
	INT iCutsceneTeam = iCurrentCutsceneStreamingTeam

	// Check to see if we've skipped the rule before a cutscene
	// and are now on the mocap rule toensure we stream the mocap anyway
	IF iCutsceneTeam = -1
		iCutsceneTeam = GET_CUTSCENE_TEAM( FALSE )
	ENDIF

	sMocapCutscene = GET_MOCAP_CUTSCENE_NAME( MC_serverBD.iEndCutsceneNum[ iMyTeam ],MC_ServerBD.iEndCutscene, MC_serverBD.iEndCutsceneEntityType[ iMyTeam ], iCutsceneTeam)
	PRINTLN("[RCC MISSION] SETTING sMocapCutscene to '", sMocapCutscene, "' - calling GET_MOCAP_CUTSCENE_NAME")

	FMMC_CUTSCENE_TYPE cutType
	IF IS_BIT_SET( iLocalBoolCheck31, LBOOL31_END_CUTSCENE_STARTED)
		cutType = FMMCCUT_ENDMOCAP
	ELIF IS_BIT_SET( iLocalBoolCheck5, LBOOL5_MID_MISSION_MOCAP)
		cutType = FMMCCUT_MOCAP
	ELSE
		cutType = FMMCCUT_ENDMOCAP
	ENDIF
	
#IF IS_DEBUG_BUILD
	IF NOT (MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_PLACE_HOLDER AND cutType = FMMCCUT_ENDMOCAP)
#ENDIF
		SET_STRAND_MISSION_CUTSCENE_NAME( sMocapCutscene )
		
		IF NOT IS_STRING_NULL_OR_EMPTY( sMocapCutscene )
			IF SHOULD_STREAM_APARTMENT_CUTSCENE( sMocapCutscene )
				CLEAR_BIT(iLocalBoolCheck31, LBOOL31_NOT_STREAMING_CUTSCENE)
				
				FLOAT iStreamingInDistance = 175
				FLOAT iStreamingOutDistance = 225
				
				ASSIGN_MOCAP_CUTSCENE_TO_HAVE_EXTENDED_STREAMING_RANGES(iStreamingOutDistance, iStreamingInDistance)
								
				HANDLE_STREAMING_MOCAP(GET_CUTSCENE_COORDS(iCutsceneTeam, cutType),
										iStreamingInDistance,
										iStreamingOutDistance, cutType)
										
			ELIF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
			AND NOT IS_CUTSCENE_ACTIVE()
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Not streaming ", sMocapCutscene, " and I'm not a spectator, and a cutscene isn't active.")
			ELIF NOT IS_CUTSCENE_ACTIVE()
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Not streaming ", sMocapCutscene, " because I'm a spectator.")
			ENDIF
		ELSE
			SET_BIT(iLocalBoolCheck31, LBOOL31_NOT_STREAMING_CUTSCENE)
		ENDIF
#IF IS_DEBUG_BUILD
	ENDIF	
#ENDIF
	
	// This is a failsafe to ensure we don't get stuck on toggled renderphases
	// without a cutscene on the current/next rule
	IF HAS_NET_TIMER_STARTED( stDropoffSafetyTimer )
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( stDropoffSafetyTimer ) > 5000
			IF NOT IS_BIT_SET(iLocalBoolCheck12,LBOOL12_LOADING_SPINNER_FOR_DROPOFF)
				BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("")
				END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_LOADING))
				SET_BIT(iLocalBoolCheck12,LBOOL12_LOADING_SPINNER_FOR_DROPOFF)
				PRINTLN("[RCC MISSION] Added busy spinner for apartment dropoff")
			ENDIF
			IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( stDropoffSafetyTimer ) > 15000
				IF IS_BIT_SET( iLocalBoolCheck7, LBOOL7_TOGGLED_RENDERPHASES_FOR_DROPOFF )
				AND IS_BIT_SET( iLocalBoolCheck8, LBOOL8_NO_MOCAP_AFTER_THIS_DROPOFF )
					CLEAR_APARTMENT_DROPOFF_SPINNER(2)
					RESET_CONOR_DROPOFF_GLOBALS( 1 )
					CLEAR_BIT( iLocalBoolCheck7, LBOOL7_TOGGLED_RENDERPHASES_FOR_DROPOFF )
					TOGGLE_RENDERPHASES( TRUE )
					RESET_NET_TIMER( stDropoffSafetyTimer )
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] - LBOOL7_TOGGLED_RENDERPHASES_FOR_DROPOFF set false because LBOOL8_NO_MOCAP_AFTER_THIS_DROPOFF is true")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF ( g_HeistGarageDropoffPanComplete OR g_HeistApartmentReadyForDropOffCut )
	AND g_bShouldThisPlayerGetPulledIntoApartment
	AND IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
	AND NOT IS_CUTSCENE_PLAYING()
		IF NOT IS_BIT_SET( iLocalBoolCheck7, LBOOL7_TOGGLED_RENDERPHASES_FOR_DROPOFF )
			SET_BIT( iLocalBoolCheck7, LBOOL7_TOGGLED_RENDERPHASES_FOR_DROPOFF )
			TOGGLE_RENDERPHASES( FALSE )
			REINIT_NET_TIMER( stDropoffSafetyTimer )
			BROADCAST_START_STREAMING_END_MOCAP_AFTER_APARTMENT_DROPOFF()
			//
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Clearing Conor's garage/apartment dropoff & toggling renderphases" )
		ELSE
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] garage/apartment dropoff & toggling renderphases already set" )
		ENDIF
	ENDIF
	
ENDPROC
