USING "FM_Mission_Controller_USING.sch"
USING "FM_Mission_Controller_Utility.sch"
USING "FM_Mission_Controller_Entities.sch"
USING "FM_Mission_Controller_ObjectiveCompletion.sch"
USING "FM_Mission_Controller_SpecialCase.sch"
USING "FM_Mission_Controller_Rules.sch"
USING "FM_Mission_Controller_Doors.sch"
USING "FM_Mission_Controller_Cutscene.sch"
USING "FM_Mission_Controller_Debug.sch"
USING "FM_Mission_Controller_Locations.sch"
USING "FM_Mission_Controller_Vehicles.sch"
USING "FM_Mission_Controller_Peds_Brain.sch"
USING "FM_Mission_Controller_Peds_Body.sch"
USING "FM_Mission_Controller_Peds.sch"
USING "FM_Mission_Controller_GangChase.sch"
USING "FM_Mission_Controller_Minigame.sch"
USING "FM_Mission_Controller_Objects.sch"
USING "FM_Mission_Controller_Props.sch"
USING "FM_Mission_Controller_Audio.sch"
USING "FM_Mission_Controller_HUD.sch"
USING "FM_Mission_Controller_Players.sch"
USING "FM_Mission_Controller_Zones.sch"
USING "FM_Mission_Controller_Bounds.sch"
USING "FM_Mission_Controller_Events.sch"
USING "FM_Mission_Controller_Ending.sch"
USING "FM_Mission_Controller_Cutscene_2.sch"
USING "FM_Mission_Controller_Variations.sch"
USING "net_car_rockets.sch"
USING "net_car_boost.sch"
USING "net_car_spikes.sch"
USING "safehouse_activity_anims.sch"

//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: EVERY FRAME AND MAIN FUNCTIONS !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************



//Pass over the variables to globals for the BG script
PROC MAINTAIN_BG_GLOBALS_PASS_OVER()

	INT iLoop	
	
	//The local variables
	gMC_LocalVariables_VARS.eScriptedCutsceneProgress		= eScriptedCutsceneProgress
	gMC_LocalVariables_VARS.iScriptedCutsceneProgress		= iScriptedCutsceneProgress
	gMC_LocalVariables_VARS.iMocapCutsceneProgress			= iMocapCutsceneProgress
	gMC_LocalVariables_VARS.eMocapRunningCutsceneProgress	= eMocapRunningCutsceneProgress
	gMC_LocalVariables_VARS.iScriptedCutsceneTeam			= iScriptedCutsceneTeam
	gMC_LocalVariables_VARS.iScriptedCutscenePlaying		= g_iFMMCScriptedCutscenePlaying
	gMC_LocalVariables_VARS.iLocalBoolCheck					= iLocalBoolCheck
	gMC_LocalVariables_VARS.iLocalBoolCheck2				= iLocalBoolCheck2
	gMC_LocalVariables_VARS.iLocalBoolCheck3				= iLocalBoolCheck3
	gMC_LocalVariables_VARS.iLocalBoolCheck4				= iLocalBoolCheck4
	gMC_LocalVariables_VARS.iLocalBoolCheck5				= iLocalBoolCheck5
	gMC_LocalVariables_VARS.iLocalBoolCheck6				= iLocalBoolCheck6
	gMC_LocalVariables_VARS.iLocalBoolCheck7				= iLocalBoolCheck7
	gMC_LocalVariables_VARS.iLocalBoolCheck8				= iLocalBoolCheck8
	gMC_LocalVariables_VARS.iLocalBoolCheck9				= iLocalBoolCheck9
	gMC_LocalVariables_VARS.iLocalBoolCheck10				= iLocalBoolCheck10
	gMC_LocalVariables_VARS.iLocalBoolCheck11				= iLocalBoolCheck11
	gMC_LocalVariables_VARS.iLocalBoolCheck12				= iLocalBoolCheck12
	gMC_LocalVariables_VARS.iLocalBoolCheck13				= iLocalBoolCheck13
	gMC_LocalVariables_VARS.iLocalBoolCheck14				= iLocalBoolCheck14
	gMC_LocalVariables_VARS.iLocalBoolCheck15				= iLocalBoolCheck15
	gMC_LocalVariables_VARS.iLocalBoolCheck16				= iLocalBoolCheck16
	gMC_LocalVariables_VARS.iLocalBoolCheck17				= iLocalBoolCheck17
	gMC_LocalVariables_VARS.iLocalBoolCheck18				= iLocalBoolCheck18
	gMC_LocalVariables_VARS.iLocalBoolCheck19				= iLocalBoolCheck19
	gMC_LocalVariables_VARS.iLocalBoolCheck20				= iLocalBoolCheck20
	gMC_LocalVariables_VARS.iLocalBoolCheck21				= iLocalBoolCheck21
	gMC_LocalVariables_VARS.iLocalBoolCheck22				= iLocalBoolCheck22
	gMC_LocalVariables_VARS.iLocalBoolCheck23				= iLocalBoolCheck23
	gMC_LocalVariables_VARS.iLocalBoolCheck24				= iLocalBoolCheck24
	gMC_LocalVariables_VARS.iLocalBoolCheck25				= iLocalBoolCheck25
	gMC_LocalVariables_VARS.iLocalBoolCheck26				= iLocalBoolCheck26
	gMC_LocalVariables_VARS.iLocalBoolCheck27				= iLocalBoolCheck27
	gMC_LocalVariables_VARS.iLocalBoolCheck28				= iLocalBoolCheck28
	gMC_LocalVariables_VARS.iLocalBoolCheck29				= iLocalBoolCheck29
	gMC_LocalVariables_VARS.iLocalBoolCheck30				= iLocalBoolCheck30
	gMC_LocalVariables_VARS.iLocalBoolCheck31				= iLocalBoolCheck31
	gMC_LocalVariables_VARS.iLocalBoolCheck32				= iLocalBoolCheck32
	gMC_LocalVariables_VARS.iLocalBoolCheck33				= iLocalBoolCheck33
	
	//The player BD variables
	IF iLocalPart != -1
		gMC_PlayerBD_VARS.iClientLogicStage						= MC_playerBD[iLocalPart].iClientLogicStage
		gMC_PlayerBD_VARS.iGameState							= MC_playerBD[iLocalPart].iGameState
		gMC_PlayerBD_VARS.iCurrentLoc							= MC_playerBD[iLocalPart].iCurrentLoc
		gMC_PlayerBD_VARS.iInAnyLoc 							= MC_playerBD[iLocalPart].iInAnyLoc 
		gMC_PlayerBD_VARS.iLeaveLoc								= MC_playerBD[iLocalPart].iLeaveLoc
		gMC_PlayerBD_VARS.iPedNear								= MC_playerBD[iLocalPart].iPedNear
		gMC_PlayerBD_VARS.iVehNear								= MC_playerBD[iLocalPart].iVehNear
		gMC_PlayerBD_VARS.iObjNear								= MC_playerBD[iLocalPart].iObjNear
		gMC_PlayerBD_VARS.iObjHacking							= MC_playerBD[iLocalPart].iObjHacking
		gMC_PlayerBD_VARS.iPedCarryCount						= MC_playerBD[iLocalPart].iPedCarryCount
		gMC_PlayerBD_VARS.iVehCarryCount						= MC_playerBD[iLocalPart].iVehCarryCount
		gMC_PlayerBD_VARS.iObjCarryCount						= MC_playerBD[iLocalPart].iObjCarryCount
		gMC_PlayerBD_VARS.iNumPlayerDeaths						= MC_playerBD[iLocalPart].iNumPlayerDeaths
		gMC_PlayerBD_VARS.iPlayerScore							= MC_playerBD[iLocalPart].iPlayerScore
		gMC_PlayerBD_VARS.iClientBitSet							= MC_playerBD[iLocalPart].iClientBitSet
		gMC_PlayerBD_VARS.iClientBitSet2 						= MC_playerBD[iLocalPart].iClientBitSet2
		gMC_PlayerBD_VARS.iClientBitSet3						= MC_playerBD[iLocalPart].iClientBitSet3
		gMC_PlayerBD_VARS.iClientBitSet4						= MC_playerBD[iLocalPart].iClientBitSet4
		gMC_PlayerBD_VARS.iTinyRacersState						= ENUM_TO_INT(MC_playerBD[iLocalPart].ePlayerTinyRacersRoundState)
		gMC_PlayerBD_VARS.iRestartState							= ENUM_TO_INT(MC_playerBD[iLocalPart].ePlayerRespawnBackAtStartState)
	ENDIF
	
	//The server BD globals
	gBG_MC_serverBD_VARS.iServerGameState					= MC_serverBD.iServerGameState
	gBG_MC_serverBD_VARS.iServerBitSet						= MC_serverBD.iServerBitSet
	gBG_MC_serverBD_VARS.iServerBitSet1						= MC_serverBD.iServerBitSet1
	gBG_MC_serverBD_VARS.iServerBitSet2						= MC_serverBD.iServerBitSet2
	gBG_MC_serverBD_VARS.iServerBitSet3						= MC_serverBD.iServerBitSet3
	gBG_MC_serverBD_VARS.iServerBitSet4						= MC_serverBD.iServerBitSet4
	gBG_MC_serverBD_VARS.iServerBitSet5						= MC_serverBD.iServerBitSet5
	gBG_MC_serverBD_VARS.iServerBitSet6						= MC_serverBD.iServerBitSet6
	gBG_MC_serverBD_VARS.iServerBitSet7						= MC_serverBD.iServerBitSet7
	gBG_MC_serverBD_VARS.iServerBitSet8						= MC_serverBD.iServerBitSet8
	gBG_MC_serverBD_VARS.iOptionsMenuBitSet					= MC_serverBD.iOptionsMenuBitSet
	gBG_MC_serverBD_VARS.iNumberOfTeams					 	= MC_serverBD.iNumberOfTeams
	gBG_MC_serverBD_VARS.iNumActiveTeams					= MC_serverBD.iNumActiveTeams
	gBG_MC_serverBD_VARS.iEndCutscene					 	= MC_serverBD.iEndCutscene
	gBG_MC_serverBD_VARS.iServerEventKey					= MC_ServerBD.iSessionScriptEventKey
	
	IF (GET_FRAME_COUNT() % 30) = 0
	
		IF gMC_LocalVariables_VARS.iLocalAssignStagger = 0		
			gBG_MC_serverBD_VARS.iNumPeds = 0
			FOR iLoop = 0 TO FMMC_MAX_PEDS-1
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iLoop])
					gBG_MC_serverBD_VARS.piPeds[gBG_MC_serverBD_VARS.iNumPeds] = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iLoop])
					gBG_MC_serverBD_VARS.iNumPeds++
				ENDIF
			ENDFOR
			gMC_LocalVariables_VARS.iLocalAssignStagger = 1		
			
		ELIF gMC_LocalVariables_VARS.iLocalAssignStagger = 1		
			gBG_MC_serverBD_VARS.iNumVehicles = 0
			FOR iLoop = 0 TO FMMC_MAX_VEHICLES-1
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iLoop])
					gBG_MC_serverBD_VARS.viVehicles[gBG_MC_serverBD_VARS.iNumVehicles]			= NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iLoop])
					gBG_MC_serverBD_VARS.iNumVehicles++
				ENDIF
			ENDFOR	
			gMC_LocalVariables_VARS.iLocalAssignStagger = 2
			
		ELIF gMC_LocalVariables_VARS.iLocalAssignStagger = 2
			gBG_MC_serverBD_VARS.iNumObjects = 0
			FOR iLoop = 0 TO FMMC_MAX_NUM_OBJECTS-1
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iLoop])
					gBG_MC_serverBD_VARS.oiObjects[gBG_MC_serverBD_VARS.iNumObjects]			= NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iLoop])
					gBG_MC_serverBD_VARS.iNumObjects++
				ENDIF
			ENDFOR			
			gMC_LocalVariables_VARS.iLocalAssignStagger = 0	
			
		ENDIF
	ENDIF
	
	iLoop = 0
	
	REPEAT FMMC_MAX_TEAMS iLoop
		gBG_MC_serverBD_VARS.iCurrentHighestPriority[iLoop]			= MC_serverBD_4.iCurrentHighestPriority[iLoop]
		gBG_MC_serverBD_VARS.iMaxObjectives[iLoop]					= MC_serverBD.iMaxObjectives[iLoop]
		gBG_MC_serverBD_VARS.iNumPlayerRuleHighestPriority[iLoop]	= MC_serverBD.iNumPlayerRuleHighestPriority[iLoop]
		gBG_MC_serverBD_VARS.iNumLocHighestPriority[iLoop]		 	= MC_serverBD.iNumLocHighestPriority[iLoop]
		gBG_MC_serverBD_VARS.iNumPedHighestPriority[iLoop]		 	= MC_serverBD.iNumPedHighestPriority[iLoop]
		gBG_MC_serverBD_VARS.iNumVehHighestPriority[iLoop]		 	= MC_serverBD.iNumVehHighestPriority[iLoop]
		gBG_MC_serverBD_VARS.iNumObjHighestPriority[iLoop]		 	= MC_serverBD.iNumObjHighestPriority[iLoop]
		gBG_MC_serverBD_VARS.iNumPedHighestPriorityHeld[iLoop]		= MC_serverBD.iNumPedHighestPriorityHeld[iLoop]
		gBG_MC_serverBD_VARS.iNumVehHighestPriorityHeld[iLoop]		= MC_serverBD.iNumVehHighestPriorityHeld[iLoop]
		gBG_MC_serverBD_VARS.iNumObjHighestPriorityHeld[iLoop]		= MC_serverBD.iNumObjHighestPriorityHeld[iLoop]
		gBG_MC_serverBD_VARS.iPlayerRuleMissionLogic[iLoop]		 	= MC_serverBD_4.iPlayerRuleMissionLogic[iLoop]
		gBG_MC_serverBD_VARS.iLocMissionLogic[iLoop]		 		= MC_serverBD_4.iLocMissionLogic[iLoop]
		gBG_MC_serverBD_VARS.iPedMissionLogic[iLoop]				= MC_serverBD_4.iPedMissionLogic[iLoop]
		gBG_MC_serverBD_VARS.iVehMissionLogic[iLoop]				= MC_serverBD_4.iVehMissionLogic[iLoop]
		gBG_MC_serverBD_VARS.iObjMissionLogic[iLoop]		 		= MC_serverBD_4.iObjMissionLogic[iLoop]
		gBG_MC_serverBD_VARS.iObjMissionSubLogic[iLoop]		 		= MC_serverBD_4.iObjMissionSubLogic[iLoop]
		gBG_MC_serverBD_VARS.iNumberOfPlayingPlayers[iLoop]		 	= MC_serverBD.iNumberOfPlayingPlayers[iLoop]
		gBG_MC_serverBD_VARS.iTeamScore[iLoop]		 				= MC_serverBD.iTeamScore[iLoop]
		gBG_MC_serverBD_VARS.iScoreOnThisRule[iLoop]		 		= MC_serverBD.iScoreOnThisRule[iLoop]
		gBG_MC_serverBD_VARS.iTeamDeaths[iLoop]		 				= MC_serverBD.iTeamDeaths[iLoop]
		gBG_MC_serverBD_VARS.iCutsceneID[iLoop]						= MC_serverBD.iCutsceneID[iLoop]
	ENDREPEAT
ENDPROC

PROC ADD_MISSION_JOB_POINTS_ROUNDS()
	INT iPoints
	INT iLocalPlayerJP = -1
	
	IF NOT IS_BIT_SET(iLocalBoolCheck7, LBOOL7_ADDED_JP)
		IF IS_CORONA_READY_TO_START_WITH_JOB()
			IF IS_THIS_A_ROUNDS_MISSION()
			AND SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
			
				INT iLBDIndexWinner = GET_LEADERBOARD_INDEX_OF_TOP_PLAYER_ON_TEAM(MC_playerBD[iLocalPart].iteam)
				INT iteamwinner 	= NATIVE_TO_INT(g_MissionControllerserverBD_LB.sleaderboard[iLBDIndexWinner].playerID)
				PRINTLN("[RCC MISSION] [JOB POINT] ADD_MISSION_JOB_POINTS_ROUNDS -iteamwinner lb index=  ",iteamwinner, " iLBDIndexWinner = ", iLBDIndexWinner)
				PRINTLN("[RCC MISSION] [JOB POINT] ADD_MISSION_JOB_POINTS_ROUNDS local player: ",NATIVE_TO_INT(LocalPlayer))
					
				// Add MVP Job Point
				IF MC_serverBD.iNumActiveTeams > 1	
				AND NOT g_bOnCoopMission
					iLocalPlayerJP = NETWORK_PLAYER_ID_TO_INT()
					IF iLocalPlayerJP <> -1
						GlobalplayerBD_FM[iLocalPlayerJP].sPlaylistVars.iMVPThisJob = iteamwinner
						PRINTLN("[RCC MISSION] [JOB POINT] ADD_MISSION_JOB_POINTS_ROUNDS iLocalPlayerJP = ", iLocalPlayerJP, " iteamwinner = ", iteamwinner)
					ENDIF
				ENDIF
			
				IF DOES_TEAM_LIKE_TEAM(MC_playerBD[iPartToUse].iteam,MC_serverBD.iWinningTeam)
					iPoints = GET_POINTS_FOR_POSITION(GET_TEAM_FINISH_POSITION(MC_serverBD.iWinningTeam), iLocalPlayerJP)
				ELSE
					iPoints = GET_POINTS_FOR_POSITION(GET_TEAM_FINISH_POSITION(MC_playerBD[iLocalPart].iteam), iLocalPlayerJP)
				ENDIF
				sCelebrationStats.iLocalPlayerJobPoints = iPoints
				PRINTLN("[RCC MISSION][JOB POINT] ADD_MISSION_JOB_POINTS_ROUNDS :  ", iPoints)
				
				SET_BIT(iLocalBoolCheck7, LBOOL7_ADDED_JP)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC GET_SNACKS_STATUS(INT iTeam)
	IF MC_serverBD_4.iCurrentHighestPriority[ MC_playerBD[iPartToUse].iteam ] < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE3_NO_SNACKING)
			IF NOT ARE_SNACKS_BLOCKED()
				BLOCK_SNACKS(TRUE)
			ENDIF
		ELSE
			IF ARE_SNACKS_BLOCKED()
				BLOCK_SNACKS(FALSE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC BLOCK_VEHICLE_REWARD_WEAPONS_DURING_LOCKED_CAPTURE()
      IF IS_JOB_FORCED_WEAPON_ONLY()
      OR IS_JOB_FORCED_WEAPON_PLUS_PICKUPS()
            DISABLE_PLAYER_VEHICLE_REWARDS(LocalPlayer)
      ENDIF 
ENDPROC

PROC MAINTAIN_REDUCED_APARTMENT_CREATION_FLAG()
	BOOL bOn = FALSE
	#IF IS_DEBUG_BUILD
	INT iReason
	#ENDIF
	IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
	OR Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
		IF (g_bPropertyDropOff OR g_bGarageDropOff)
		AND g_bShouldThisPlayerGetPulledIntoApartment
			IF IS_THIS_MOCAP_IN_AN_APARTMENT(sMocapCutscene)
			AND (g_HeistGarageDropoffPanStarted
			OR g_HeistApartmentDropoffPanStarted)
				bOn = TRUE
			ELSE
				#IF IS_DEBUG_BUILD
				IF NOT IS_THIS_MOCAP_IN_AN_APARTMENT(sMocapCutscene)
					iReason = 1
				ENDIF
				IF NOT (g_HeistGarageDropoffPanStarted
				OR g_HeistApartmentDropoffPanStarted)
					iReason = 4
				ENDIF
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			iReason = 2
			#ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD 
		iReason = 3
		#ENDIF
	ENDIF
	
	IF bOn != g_bReducedApartmentCreationTurnedOn
		g_bReducedApartmentCreationTurnedOn = bOn
		PRINTLN("g_bReducedApartmentCreationTurnedOn set to: ",g_bReducedApartmentCreationTurnedOn, " reason: ",iReason)
	ENDIF
ENDPROC

PROC MAINTAIN_DISABLED_APARTMENT_ACCESS_FOR_COPS(BOOL bNeedsRepair)
	IF CURRENT_OBJECTIVE_LOSE_COPS() 
		IF NOT g_bDisablePropertyAccessForLoseCopMissionObj
			g_bDisablePropertyAccessForLoseCopMissionObj = TRUE
			PRINTLN("g_bDisablePropertyAccessForLoseCopMissionObj set to true - 1")
		ENDIF
	ELIF CURRENT_OBJECTIVE_LOSE_GANG_BACKUP()
		IF NOT g_bDisablePropertyAccessForLoseCopMissionObj
			g_bDisablePropertyAccessForLoseCopMissionObj = TRUE
			PRINTLN("g_bDisablePropertyAccessForLoseCopMissionObj set to true - 2")
		ENDIF
	ELIF CURRENT_OBJECTIVE_REPAIR_CAR()
	AND NOT bNeedsRepair
		IF NOT g_bDisablePropertyAccessForLoseCopMissionObj
			g_bDisablePropertyAccessForLoseCopMissionObj = TRUE
			PRINTLN("g_bDisablePropertyAccessForLoseCopMissionObj set to true - 3")
		ENDIF
	ELSE
		IF g_bDisablePropertyAccessForLoseCopMissionObj
			g_bDisablePropertyAccessForLoseCopMissionObj = FALSE
			PRINTLN("g_bDisablePropertyAccessForLoseCopMissionObj set to FALSE - 1")
		ENDIF
	ENDIF
ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: DISABLING CONTROLS !
//
//************************************************************************************************************************************************************



PROC DISABLE_WEAPON(WEAPON_TYPE wtWeapon)
	
	WEAPON_TYPE wtEquipped
	
	IF GET_CURRENT_PED_WEAPON(LocalPlayerPed, wtEquipped)
		IF wtEquipped = wtWeapon
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_ATTACK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
			PRINTLN("[RCC MISSION] DISABLE_WEAPON - Disabling attacks as weapon ",GET_WEAPON_NAME_FROM_WEAPON_TYPE(wtWeapon)," is equipped")
		ENDIF
	ENDIF
	
	IF wtWeapon = WEAPONTYPE_STICKYBOMB
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
	ENDIF
	
ENDPROC

PROC DISABLE_VEHICLE_ENTRY_FOR_ALL_BUT_THIS_VEHICLE()
	INT iVehicleNear = MC_playerBD[iPartToUse].iVehNear
	INT iTeam = MC_playerBD[ iPartToUse ].iteam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[ iTeam ]
	VEHICLE_INDEX lastVehicle = NULL
	
	IF iVehicleNear > -1
	AND IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitset[ iRule + 1 ], ciBS_RULE_CANNOT_EXIT_VEHICLE )
		IF NETWORK_DOES_NETWORK_ID_EXIST( MC_serverBD_1.sFMMC_SBD.niVehicle[ iVehicleNear ] )
			VEHICLE_INDEX vehToGoTo = NET_TO_VEH( MC_serverBD_1.sFMMC_SBD.niVehicle[ iVehicleNear ] )
			IF IS_VEHICLE_DRIVEABLE(vehToGoTo)
				SET_PLAYER_MAY_ONLY_ENTER_THIS_VEHICLE(LocalPlayer, vehToGoTo)
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		lastVehicle = GET_LAST_DRIVEN_VEHICLE()
		IF DOES_ENTITY_EXIST(lastVehicle)
			IF lastVehicle != NULL
			AND IS_VEHICLE_DRIVEABLE(lastVehicle)
				SET_PLAYER_MAY_ONLY_ENTER_THIS_VEHICLE(LocalPlayer, lastVehicle)
			ELSE
				SET_PLAYER_MAY_ONLY_ENTER_THIS_VEHICLE(LocalPlayer, NULL) //#2148781
				SET_PLAYER_MAY_NOT_ENTER_ANY_VEHICLE(LocalPlayer)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

///PURPOSE: This function runs every frame and checks which controls ought be disabled
///    given some situations
PROC PROCESS_DISABLED_CONTROLS()
	
	INT iTeam = MC_playerBD[ iPartToUse ].iteam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[ iTeam ]
	
	IF iSpectatorTarget = -1
		IF iRule < FMMC_MAX_RULES
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitset[ iRule ],ciBS_RULE_DISABLE_CONTROL)
				
				BOOL bDisable
				
				IF (g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDisableControlTimer[ iRule ] = 0)
					//No timer, just remove control for the whole rule:
					bDisable = TRUE
				ELSE
					IF NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_DISABLECONTROLTIMER_COMPLETE)
						IF NOT HAS_NET_TIMER_STARTED(tdDisableControlTimer)
							REINIT_NET_TIMER(tdDisableControlTimer)
							bDisable = TRUE
						ELSE
							IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdDisableControlTimer) < ( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDisableControlTimer[ iRule ] * 1000)
								bDisable = TRUE
							ELSE
								SET_BIT(iLocalBoolCheck6, LBOOL6_DISABLECONTROLTIMER_COMPLETE)
								RESET_NET_TIMER(tdDisableControlTimer)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF bDisable
					DISABLE_PLAYER_CONTROLS_THIS_FRAME()
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetFour[ iRule ], ciBS_RULE4_DISABLE_VEHICLE_MOVEMENT_CONTROLS )
				DISABLE_VEHICLE_MOVEMENT_CONTROLS_THIS_FRAME()
			ENDIF			
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetFour[ iRule ],ciBS_RULE4_DISABLE_HANDBRAKE_CONTROL)
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA(g_FMMC_STRUCT.iAdversaryModeType)
					IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
						IF NOT IS_ENTITY_IN_AIR(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HANDBRAKE)
						ENDIF
					ENDIF
				ELSE
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HANDBRAKE)
				ENDIF
			ENDIF
			
			BOOL bDisableVehExit = FALSE
			
			// If we're going to a vehicle and the next rule disables you getting out,
			// disable you getting out once you actually get into the vehicle
			IF (iRule + 1) < FMMC_MAX_RULES
			AND IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitset[ iRule + 1 ], ciBS_RULE_CANNOT_EXIT_VEHICLE )
				
				IF GET_MC_CLIENT_MISSION_STAGE( iPartToUse ) = CLIENT_MISSION_STAGE_GOTO_VEH
					
					INT iVehicleNear = MC_playerBD[iPartToUse].iVehNear
					
					IF iVehicleNear > -1
					AND IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitset[ iRule + 1 ], ciBS_RULE_CANNOT_EXIT_VEHICLE )
						IF NETWORK_DOES_NETWORK_ID_EXIST( MC_serverBD_1.sFMMC_SBD.niVehicle[ iVehicleNear ] )
							VEHICLE_INDEX vehToGoTo = NET_TO_VEH( MC_serverBD_1.sFMMC_SBD.niVehicle[ iVehicleNear ] )
							
							IF IS_VEHICLE_DRIVEABLE( vehToGoTo )
								IF IS_PED_IN_VEHICLE( PLAYER_PED_ID(), vehToGoTo )
									bDisableVehExit = TRUE
									PRINTLN("[RCC MISSION] PROCESS_DISABLED_CONTROLS - Disabling control as on goto and in vehicle, next rule is cannot exit")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[iRule] = ciFMMC_DROP_OFF_TYPE_VEHICLE
				AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOff_Vehicle[iRule] != -1
				AND IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], iRule) // The AI is in our possession
					
					NETWORK_INDEX niVeh = MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOff_Vehicle[iRule]]
					
					IF NETWORK_DOES_NETWORK_ID_EXIST(niVeh)
						VEHICLE_INDEX vehToGoTo = NET_TO_VEH(niVeh)
						
						IF IS_VEHICLE_DRIVEABLE(vehToGoTo)
						AND IS_PED_IN_VEHICLE(LocalPlayerPed, vehToGoTo)
							bDisableVehExit = TRUE
							PRINTLN("[RCC MISSION] PROCESS_DISABLED_CONTROLS - Disabling control as vehicle dropoff and in vehicle, next rule is cannot exit")
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
			
			IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitset[ iRule ], ciBS_RULE_CANNOT_EXIT_VEHICLE )
				
				BOOL bExitCheck = FALSE
				
				IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitset[ iRule ], ciBS_RULE_CANNOT_EXIT_VEHICLE_MIDPOINT ) 
					IF IS_BIT_SET( MC_serverBD.iObjectiveMidPointBitset[ iTeam ], iRule )
						CPRINTLN( DEBUG_OWAIN, "bExitCheck = true" )
						bExitCheck = TRUE
					ENDIF
				ELSE
					CPRINTLN( DEBUG_OWAIN, "bExitCheck = true" )
					bExitCheck = TRUE
				ENDIF
				
				IF bExitCheck
					//Should we only be locked into specific vehicles?
					IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLockedInVehBS[iRule] != 0
						IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
							VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
							INT iveh = IS_ENTITY_A_MISSION_CREATOR_ENTITY(tempVeh)
							
							IF iveh > -1
							AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLockedInVehBS[iRule], iveh)
								CPRINTLN( DEBUG_OWAIN, "bDisableVehExit = true, we're in a vehicle that should lock players in - iveh = " , iveh )
								bDisableVehExit = TRUE
							ENDIF
						ENDIF
					ELSE // Lock into all vehicles!
						CPRINTLN( DEBUG_OWAIN, "bDisableVehExit = true, all vehicles should lock players in" )
						bDisableVehExit = TRUE
					ENDIF
				ENDIF
				
			ENDIF
			
			IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetThree[ iRule ], ciBS_RULE3_CANT_GET_OUT_NEAR_DELIVERY )
			AND MC_playerBD[iPartToUse].iVehNear != -1
			AND IS_PLAYER_NEAR_DROP_OFF()
				PRINTLN("[RCC MISSION] bDisableVehExit = TRUE as Player near drop off")
				bDisableVehExit = TRUE
			ENDIF
			
			VEHICLE_INDEX vehTempPlayer
			IF IS_CURRENT_VEHICLE_RULE_OF_TYPE_CARGOBOB_FOR_TEAM(iTeam, iRule)
				IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					vehTempPlayer = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
					INT iTempDeliveryVeh = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOff_Vehicle[ iRule ]
					IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iTempDeliveryVeh])
						VEHICLE_INDEX vehTempCargo =  NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iTempDeliveryVeh])
						IF IS_VEHICLE_DRIVEABLE(vehTempCargo)
							IF IS_ENTITY_ATTACHED_TO_ENTITY(vehTempPlayer,vehTempCargo)
								PRINTLN("[RCC MISSION] bDisableVehExit = TRUE as Player attached to cargobob (1)")
								IF NOT IS_BIT_SET(iLocalBoolCheck11, LBOOL11_WAS_ATTACHED_TO_CARGOBOB)
									SET_BIT(iLocalBoolCheck11, LBOOL11_WAS_ATTACHED_TO_CARGOBOB)
									PRINTLN("[RCC MISSION] bDisableVehExit = TRUE as Player attached to cargobob - Set LBOOL11_WAS_ATTACHED_TO_CARGOBOB")
								ENDIF
								bDisableVehExit = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(iLocalBoolCheck11, LBOOL11_WAS_ATTACHED_TO_CARGOBOB)
					IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed) 
						vehTempPlayer = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
						IF IS_ENTITY_ATTACHED(vehTempPlayer)
							bDisableVehExit = TRUE
							PRINTLN("[RCC MISSION] bDisableVehExit = TRUE as Player attached to cargobob (2)")
						ELSE
							PRINTLN("[RCC MISSION] LBOOL11_WAS_ATTACHED_TO_CARGOBOB set but player veh not attached!")
						ENDIF
					ELSE
						PRINTLN("[RCC MISSION] LBOOL11_WAS_ATTACHED_TO_CARGOBOB set but player not in a vehicle!")
					ENDIF
				ENDIF
			ENDIF
					
			IF bDisableVehExit
				IF NOT IS_PLAYER_BLOCKED_FROM_EXITING_VEHICLE_IN_MISSION()
					SET_PLAYER_BLOCKED_FROM_EXITING_VEHICLE_IN_MISSION_FLAG(TRUE)
					SET_PED_CONFIG_FLAG(LocalPlayerPed,PCF_PlayersDontDragMeOutOfCar, TRUE)
					PRINTLN("[RCC MISSION] setting PCF_PlayersDontDragMeOutOfCar")
				ENDIF
				DISABLE_VEHICLE_EXIT_THIS_FRAME()
				
				IF g_bVSMission
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciFORCE_START_VEHICLE)
						DISABLE_VEHICLE_ENTRY_FOR_ALL_BUT_THIS_VEHICLE()
					ENDIF
				ENDIF
			ELSE
				IF IS_PLAYER_BLOCKED_FROM_EXITING_VEHICLE_IN_MISSION()
					SET_PLAYER_BLOCKED_FROM_EXITING_VEHICLE_IN_MISSION_FLAG(FALSE)
					SET_PED_CONFIG_FLAG(LocalPlayerPed,PCF_PlayersDontDragMeOutOfCar, FALSE)
					PRINTLN("[RCC MISSION] clearing PCF_PlayersDontDragMeOutOfCar")
				ENDIF
			ENDIF
	
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitset[ iRule ],ciBS_RULE_CANNOT_JUMP)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_JUMP)
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_DISABLE_JUMPING_ONLY)
				SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisablePlayerJumping, TRUE)
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[iRule], ciBS_RULE11_DISABLE_VAULTING_ONLY)
				SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisablePlayerVaulting, TRUE)
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[iRule], ciBS_RULE11_DISABLE_COMBAT_ROLL_ONLY)
				SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisablePlayerCombatRoll, TRUE)
			ENDIF
			
			IF (g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDisabledWeapon[ iRule ] != 0)
				WEAPON_TYPE wtDisabled = INT_TO_ENUM(WEAPON_TYPE, g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDisabledWeapon[ iRule ])
				DISABLE_WEAPON(wtDisabled)
			ENDIF
			
				// CMcM - Fix for 2076901
			IF bLocalPlayerPedOk
				IF IS_PED_ON_ANY_BIKE(LocalPlayerPed)
					IF IS_ENTITY_IN_WATER(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
						IF IS_PLAYER_BLOCKED_FROM_EXITING_VEHICLE_IN_MISSION()
							ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
							SET_PLAYER_BLOCKED_FROM_EXITING_VEHICLE_IN_MISSION_FLAG(FALSE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[iRule], ciBS_RULE12_DISABLE_VEHICLE_ENTRY)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
			ENDIF
			
		ENDIF
		
		IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iTeamBitset, ciBS_FORCE_RESPAWN_WHEN_NOT_IN_RESPAWN_VEHICLE )
		AND (g_FMMC_STRUCT.mnVehicleModel[ iTeam ] != DUMMY_MODEL_FOR_SCRIPT)
		AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
			DISABLE_VEHICLE_EXIT_THIS_FRAME()
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset3, ciBS3_TEAM_PLAYER_PREVENT_FALLS_OUT_WHEN_KILLED)
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_FallsOutOfVehicleWhenKilled, FALSE)
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_GetOutUndriveableVehicle, FALSE)
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_GetOutBurningVehicle, FALSE)
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_RunFromFiresAndExplosions, FALSE)
			SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisableAutoForceOutWhenBlowingUpCar, TRUE)
		ENDIF
	ENDIF
	
	BOOL bEnableRadioControl = TRUE
	IF( AM_I_ON_A_HEIST() )
		IF( IS_PED_IN_ANY_VEHICLE( LocalPlayerPed, TRUE ) )
			VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN( LocalPlayerPed, TRUE )
			IF( DOES_ENTITY_EXIST( veh ) )
				VEHICLE_SEAT seat
				IF( IS_PED_SITTING_IN_VEHICLE( LocalPlayerPed, veh ) )
					seat = GET_PED_VEHICLE_SEAT( LocalPlayerPed, veh )
				ELSE
					seat = INT_TO_ENUM( VEHICLE_SEAT, GET_SEAT_PED_IS_TRYING_TO_ENTER( LocalPlayerPed ) )
				ENDIF
				IF( seat = VS_FRONT_RIGHT )
					bEnableRadioControl = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	SET_USER_RADIO_CONTROL_ENABLED( bEnableRadioControl )
	
	//Disable the radio when a countdown to the mission end is playing:
	IF iRule < FMMC_MAX_RULES
		IF Is_Player_Currently_On_MP_CTF_Mission( LocalPlayer )
		OR IS_BIT_SET( g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciUSE_COUNTDOWN )
			IF HAS_NET_TIMER_STARTED( MC_serverBD_3.tdObjectiveLimitTimer[ MC_playerBD[ iPartToUse ].iteam ] )
				INT ms = ((GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iObjectiveTimeLimitRule[ iRule ]))  - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdObjectiveLimitTimer[ iTeam ])- MC_serverBD_3.iTimerPenalty[iTeam])
				IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					IF ms < 30000
						DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_RADIO_WHEEL ) 
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// Disable most inputs if we're carrying a binbag
	IF GET_CURRENT_SPECIAL_PICKUP_STATE() = eSpecialPickupState_ALIGNING
	OR GET_CURRENT_SPECIAL_PICKUP_STATE() = eSpecialPickupState_REQUESTING
	OR GET_CURRENT_SPECIAL_PICKUP_STATE() = eSpecialPickupState_ATTACH_OBJECT_TO_PED
	OR GET_CURRENT_SPECIAL_PICKUP_STATE() = eSpecialPickupState_CARRYING
	OR GET_CURRENT_SPECIAL_PICKUP_STATE() = eSpecialPickupState_PUTTING_DOWN
	OR GET_CURRENT_SPECIAL_PICKUP_STATE() = eSpecialPickupState_ALIGNING_FOR_THROW
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_AIM )
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_ATTACK )
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_ATTACK2 )
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_MELEE_ATTACK1 )
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_MELEE_ATTACK2 )
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT )
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY )
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE )
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_COVER )
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_JUMP )
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_NEXT_WEAPON )
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_PREV_WEAPON )
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_DUCK )
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_ENTER )
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_PHONE )
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_SELECT_WEAPON )
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_WEAPON_SPECIAL )
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO )
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_WEAPON_WHEEL_LR )
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT )
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV )
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_WEAPON_WHEEL_UD )

		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_PICKUP )
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_SNIPER_ZOOM )
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_SNIPER_ZOOM_IN_ONLY )
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_SNIPER_ZOOM_OUT_ONLY )
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_COVER )
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_RELOAD )

		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_DETONATE )
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_HUD_SPECIAL )
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_ARREST )
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_ACCURATE_AIM )
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_DIVE )

	ENDIF
	
	//Disable the cinematic camera if Sumo is being played locked in first person mode
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_SUMO(g_FMMC_STRUCT.iRootContentIDHash)
		IF iSpectatorTarget = -1
			IF g_FMMC_STRUCT.iFixedCamera = 1
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DCTL(g_FMMC_STRUCT.iAdversaryModeType)
		IF iSpectatorTarget = -1
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciDISABLE_VISOR_ANIMATION)
		IF IS_PED_ON_ANY_BIKE(LocalPlayerPed)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SWITCH_VISOR)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset3, ciBS3_TEAM_DISABLE_SEAT_SWAPPING)
		IF iSpectatorTarget = -1
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SHUFFLE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HELP_TUR")
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HELP_APC_TUR")
				CLEAR_HELP(TRUE)
				PRINTLN("[RCC MISSION][PROCESS_DISABLED_CONTROLS] CLEAR_HELP For ciBS3_TEAM_DISABLE_SEAT_SWAPPING")
			ENDIF		
			PRINTLN("[RCC MISSION][PROCESS_DISABLED_CONTROLS] DISABLE_CONTROL_ACTION For ciBS3_TEAM_DISABLE_SEAT_SWAPPING")
		ENDIF
	ENDIF
	
	IF g_bMissionEnding
	OR iRule >= FMMC_MAX_RULES
		DISABLE_VEHICLE_EXIT_THIS_FRAME()
	ENDIF
ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: EVERY FRAME PLAYER CHECKS !
//
//************************************************************************************************************************************************************




FUNC BOOL IS_PLAYER_WEARING_COP_OUTFIT(INT iPart)
	
	BOOL bCop = FALSE
	
	SWITCH INT_TO_ENUM(MP_OUTFIT_ENUM, MC_playerBD[iPart].iOutfit)
		
		CASE OUTFIT_HEIST_POLICE_0
		CASE OUTFIT_HEIST_PRISON_OFFICER_0
		CASE OUTFIT_PRISON_POLICE_OFFICER_VS
			bCop = TRUE
		BREAK
		
	ENDSWITCH
	
	RETURN bCop
	
ENDFUNC

FUNC BOOL IS_PLAYER_WEARING_SCUBA_GEAR(INT iPart)
	RETURN INT_TO_ENUM(MP_OUTFIT_ENUM, MC_playerBD[iPart].iOutfit) = OUTFIT_GANGOPS_HEIST_SCUBA
ENDFUNC

FUNC BOOL IS_CURRENT_OUTFIT_A_SLOWING_OUTFIT()
	
	SWITCH GET_STYLE_FROM_OUTFIT(MC_playerBD[iLocalPart].iOutfit)
		CASE OUTFIT_STYLE_CASINO_HEIST_DIRECT_HEAVY_I
		CASE OUTFIT_STYLE_CASINO_HEIST_DIRECT_HEAVY_II
		CASE OUTFIT_STYLE_CASINO_HEIST_DIRECT_HEAVY_III
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

///PURPOSE: This function is called every frame and checks to see if the player's outfit should affect anything
PROC PROCESS_HEIST_OUTFIT_BEHAVIOURS()
	
	IF( Is_Player_Currently_On_MP_Heist_Planning( PLAYER_ID() ) OR Is_Player_Currently_On_MP_Heist( PLAYER_ID() ) )
	OR ( g_sMPTunables.bEnable_Heavy_Utility_Vest = TRUE AND NOT IS_PLAYER_ON_ANY_FM_JOB( PLAYER_ID() ) )
	OR IS_CURRENT_OUTFIT_A_SLOWING_OUTFIT()
		
		g_bSlowingArmour = IS_CURRENT_OUTFIT_A_SLOWING_OUTFIT()
		
		DO_HEIST_HEAVY_ARMOUR_SLOWDOWN_EFFECT()
		
		IF g_bSlowingArmour
			IF NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_HEAVY_ARMOUR_CHECK)
			AND bLocalPlayerOK
				DO_HEIST_HEAVY_ARMOUR_CHECKS()
				SET_BIT(iLocalBoolCheck33, LBOOL33_HEAVY_ARMOUR_CHECK)
			ENDIF
			IF !bLocalPlayerOK
			AND IS_BIT_SET(iLocalBoolCheck33, LBOOL33_HEAVY_ARMOUR_CHECK)
				CLEAR_BIT(iLocalBoolCheck33, LBOOL33_HEAVY_ARMOUR_CHECK)
			ENDIF
		ENDIF
	ENDIF
	
	IF AM_I_ON_A_HEIST()
		//Cop outfits allow ambient vehicles to no longer be set as stolen
		IF IS_PLAYER_WEARING_COP_OUTFIT(iLocalPart)
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, TRUE)
				VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed, TRUE)
				IF IS_VEHICLE_STOLEN(tempVeh)
				AND NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
					IF IS_VEHICLE_DRIVEABLE(tempVeh)
						SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(tempVeh, TRUE)
						SET_VEHICLE_IS_STOLEN(tempVeh, FALSE)
						SET_VEHICLE_IS_WANTED(tempVeh, FALSE)
						SET_POLICE_FOCUS_WILL_TRACK_VEHICLE(tempVeh, FALSE)
						SET_VEHICLE_INFLUENCES_WANTED_LEVEL(tempVeh, FALSE)
						PRINTLN("[RCC MISSION] PROCESS_HEIST_OUTFIT_BEHAVIOURS - Player is dressed as a cop, setting current vehicle as not stolen")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_PLAYER_WEARING_SCUBA_GEAR(iLocalPart)
		IF NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_SCUBA_GEAR_ON)
			PRINTLN("[JS] PROCESS_HEIST_OUTFIT_BEHAVIOURS - Player in scuba gear, setting unable to drown")
			SET_BIT(iLocalBoolCheck26, LBOOL26_SCUBA_GEAR_ON)
			SET_PED_UNABLE_TO_DROWN(LocalPlayerPed)
		ENDIF
	ELIF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_SCUBA_GEAR_ON)
		SET_PED_ABLE_TO_DROWN(LocalPlayerPed)
		PRINTLN("[JS] PROCESS_HEIST_OUTFIT_BEHAVIOURS - Player no longer in scuba gear, setting able to drown")
		CLEAR_BIT(iLocalBoolCheck26, LBOOL26_SCUBA_GEAR_ON)
	ENDIF
	
ENDPROC


PROC PROCESS_VERSUS_ROLES_GHOSTS(INT iTeam, INT iRule, INT iRole)
	IF iRule < FMMC_MAX_RULES AND iRule > -1	
	AND iTeam < FMMC_MAX_TEAMS AND iTeam > -1
	AND iRole < FMMC_MAX_ROLES AND iRole > -1
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRolesBitset[iRule][iRole], ciBS_ROLE_SET_AS_GHOST)
			PRINTLN("[MMacK][Ghost] Setting Passive")
			SET_TEMP_PASSIVE_MODE(TRUE, TRUE)
			TURN_OFF_PASSIVE_MODE_OPTION(TRUE)
		ELSE
			IF g_MultiplayerSettings.g_bTempPassiveModeSetting
				PRINTLN("[MMacK][Ghost] Cleaning Passive")
				CLEANUP_TEMP_PASSIVE_MODE()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_VERSUS_ROLES(INT iTeam, INT iRule, INT iRole)
	IF iRule < FMMC_MAX_RULES AND iRule > -1	
	AND iTeam < FMMC_MAX_TEAMS AND iTeam > -1
	AND iRole < FMMC_MAX_ROLES AND iRole > -1
		PROCESS_VERSUS_ROLES_GHOSTS(iTeam, iRule, iRole)
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRolesBitset[iRule][iRole], ciBS_ROLE_DISABLE_CONTROL)
			DISABLE_PLAYER_CONTROLS_THIS_FRAME()
			PRINTLN("[MMacK] Player Control Disabled Due To Role Setting")
		ENDIF
	
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRolesBitset[iRule][iRole], ciBS_ROLE_SHOW_STOP_START)
			IF HAS_SCALEFORM_MOVIE_LOADED(uiGoStopDisplay)
				INT iR, iG, iB, iA
								
				IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
				AND (IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet2, PBBOOL2_IN_MISSION_BOUNDS)
				OR	 IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet2, PBBOOL2_IN_MISSION_BOUNDS2))
				AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
					SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
							
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRolesBitset[iRule][iRole], ciBS_ROLE_DISABLE_CONTROL)
					OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRolesBitset[iRule][iRole], ciBS_ROLE_SET_AS_GHOST)
						IF iRule > 0 
						OR MC_ServerBD_4.iTeamLapsCompleted[iTeam] > 0
							IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH( "appinternet" ) ) = 0 )
								IF NOT (iRule = 0 AND IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME+iTeam))
									IF (g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iMissionLapLimit > 0
									AND MC_ServerBD_4.iTeamLapsCompleted[MC_playerBD[iLocalPart].iteam] < g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iMissionLapLimit)
									OR g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iMissionLapLimit = 0
										DRAW_SCALEFORM_MOVIE_FULLSCREEN(uiGoStopDisplay, 255, 255, 255, 100)
										
										PRINTLN("[MMacK] Showing STOP Message")
										RESET_NET_TIMER(tmrGoStopDisplay)
										
										GET_HUD_COLOUR(HUD_COLOUR_RED, iR, iG, iB, iA)
										
										BEGIN_SCALEFORM_MOVIE_METHOD(uiGoStopDisplay, "SET_MESSAGE")
											SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("STOP_DASH")
											SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iR)
											SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iG)
											SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iB)
											SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE) 
										END_SCALEFORM_MOVIE_METHOD()
										
										IF NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_PLAYED_STOP_HUD_SFX)
											SET_BIT(iLocalBoolCheck13, LBOOL13_PLAYED_STOP_HUD_SFX)
											PLAY_SOUND_FRONTEND(-1, "Enter_Area", "DLC_Lowrider_Relay_Race_Sounds", FALSE)
											PRINTLN("[MMacK] Playing STOP SFX")
										ENDIF
										
										CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_GENERIC_TEXT)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF MC_ServerBD_4.iTeamLapsCompleted[iTeam] != 0
						AND ((g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMissionLapLimit = 0)
							OR (MC_ServerBD_4.iTeamLapsCompleted[iTeam] < g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMissionLapLimit))
							DRAW_SCALEFORM_MOVIE_FULLSCREEN(uiGoStopDisplay, 255, 255, 255, 100)
							
							IF NOT HAS_NET_TIMER_STARTED(tmrGoStopDisplay)
								PRINTLN("[MMacK] Starting GO Timer")
								REINIT_NET_TIMER(tmrGoStopDisplay)
							ELSE
								IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tmrGoStopDisplay) <= 500
								AND NOT IS_PED_DEAD_OR_DYING(LocalPlayerPed)
									PRINTLN("[MMacK] Showing GO Message")
									GET_HUD_COLOUR(HUD_COLOUR_GREEN, iR, iG, iB, iA)
									
									BEGIN_SCALEFORM_MOVIE_METHOD(uiGoStopDisplay, "SET_MESSAGE")
										SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CNTDWN_GO")
										SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iR)
										SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iG)
										SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iB)
										SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE) 
									END_SCALEFORM_MOVIE_METHOD()
									
									IF IS_BIT_SET(iLocalBoolCheck13, LBOOL13_PLAYED_STOP_HUD_SFX)
										CLEAR_BIT(iLocalBoolCheck13, LBOOL13_PLAYED_STOP_HUD_SFX)
										PLAY_SOUND_FRONTEND(-1, "Out_Of_Area", "DLC_Lowrider_Relay_Race_Sounds", FALSE)
										PRINTLN("[MMacK] Playing STOP SFX 2")
									ENDIF
									
									IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
										STORE_RADIO_STATION_FOR_RESPAWN()
										NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
									ENDIF
								ELSE
									CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_GENERIC_TEXT)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF IS_BIT_SET(iLocalBoolCheck13, LBOOL13_PLAYED_STOP_HUD_SFX)
						CLEAR_BIT(iLocalBoolCheck13, LBOOL13_PLAYED_STOP_HUD_SFX)
						PLAY_SOUND_FRONTEND(-1, "Out_Of_Area", "DLC_Lowrider_Relay_Race_Sounds", FALSE)
						PRINTLN("[MMacK] Playing STOP SFX 2")
					ENDIF
				ENDIF
			ELSE
				uiGoStopDisplay = REQUEST_SCALEFORM_MOVIE("COUNTDOWN")
			ENDIF
		ENDIF		
	ELSE
		IF HAS_SCALEFORM_MOVIE_LOADED(uiGoStopDisplay)
			SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(uiGoStopDisplay)
		ENDIF
	ENDIF
ENDPROC


FUNC BOOL GET_TEAM_VEHICLE_FOR_SPEED_CHECK(INT iTeam, VEHICLE_INDEX &returnVeh, INT iSpecificVehIndex)
	
	iExplodeDriverPart = -1
	
	//Check yo'self:
	IF iTeam = MC_playerBD[iPartToUse].iteam
	AND IS_PED_IN_ANY_VEHICLE(PlayerPedToUse)
	AND iSpecificVehIndex = -1
		returnVeh = GET_VEHICLE_PED_IS_USING(PlayerPedToUse)
		iExplodeDriverPart = iPartToUse
		RETURN TRUE
	ENDIF
	
	
	//Check everyone else:
	INT iPart
	INT iPlayersChecked
	INT iSlowestPart = -1
	FLOAT fSlowestSpeed = 9999
	
	FLOAT fTempSpeed
	PLAYER_INDEX tempPlayer
	VEHICLE_INDEX tempVeh
	VEHICLE_INDEX tempTrailerVeh
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF (MC_playerBD[iPart].iteam = iTeam)
		AND (NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR))
		AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
			
			iPlayersChecked++
			
			tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart))
			
			IF IS_NET_PLAYER_OK(tempplayer)
				PED_INDEX tempPed = GET_PLAYER_PED(tempplayer)
				
				IF IS_PED_IN_ANY_VEHICLE(tempPed)
				
					tempVeh = GET_VEHICLE_PED_IS_USING(tempPed)
					
					IF IS_ENTITY_ALIVE(tempVeh)
						//Get the player moving slowest on this team:
						fTempSpeed = ABSF(GET_ENTITY_SPEED(tempVeh))
					ENDIF
					
					IF iSpecificVehIndex != -1
						PRINTLN("[RCC MISSION] GET_TEAM_VEHICLE_FOR_SPEED_CHECK - Checking a specific vehicle")
						
						
						
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iSpecificVehIndex])
						
							IF IS_VEHICLE_ATTACHED_TO_TRAILER(tempVeh)
								GET_VEHICLE_TRAILER_VEHICLE(tempVeh, tempTrailerVeh)
								PRINTLN("[RCC MISSION] GET_TEAM_VEHICLE_FOR_SPEED_CHECK  - Vehicle has a trailer attached")
							ENDIF
							
							IF tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iSpecificVehIndex])
							OR tempTrailerVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iSpecificVehIndex])
								PRINTLN("[RCC MISSION] GET_TEAM_VEHICLE_FOR_SPEED_CHECK  - Vehicle is valid")
								
								IF tempTrailerVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iSpecificVehIndex])
									returnVeh = tempTrailerVeh
									PRINTLN("[RCC MISSION] GET_TEAM_VEHICLE_FOR_SPEED_CHECK  DEBUG - returning trailer: ",NATIVE_TO_INT(returnVeh))
								ELSE
									returnVeh = tempVeh
									PRINTLN("[RCC MISSION] GET_TEAM_VEHICLE_FOR_SPEED_CHECK  - returning drivable vehicle: ", NATIVE_TO_INT(returnVeh))
								ENDIF
								PRINTLN("[RCC MISSION] GET_TEAM_VEHICLE_FOR_SPEED_CHECK  - MODEL NAME: ", GET_CREATOR_NAME_FOR_VEHICLE_MODEL(GET_ENTITY_MODEL(returnVeh)))
								iExplodeDriverPart = iPart
								PRINTLN("[RCC MISSION] GET_TEAM_VEHICLE_FOR_SPEED_CHECK  - returning part: ", iPart)
								

								RETURN TRUE
							ENDIF
						ENDIF
						
					ELIF fTempSpeed < fSlowestSpeed
						iSlowestPart = iPart
						fSlowestSpeed = fTempSpeed
					ENDIF
				ENDIF
			ENDIF
			
			IF iPlayersChecked >= MC_serverBD.iNumberOfPlayingPlayers[iTeam]
				iPart = MAX_NUM_MC_PLAYERS // Break out, we've checked everyone on the team!
				IF iSpecificVehIndex != -1
					PRINTLN("[RCC MISSION] GET_TEAM_VEHICLE_FOR_SPEED_CHECK - Checking a specific vehicle")
					
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iSpecificVehIndex])
					
						VEHICLE_INDEX viSpecificVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iSpecificVehIndex])
						
						IF IS_ENTITY_ALIVE(viSpecificVeh)
							IF IS_VEHICLE_A_TRAILER(viSpecificVeh)
								IF IS_ENTITY_ALIVE(GET_ENTITY_ATTACHED_TO(viSpecificVeh))
									tempVeh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_ATTACHED_TO(viSpecificVeh))
									tempTrailerVeh = viSpecificVeh
								ENDIF
							ENDIF
						ENDIF
										
						IF tempTrailerVeh = viSpecificVeh
						OR tempVeh = viSpecificVeh
							PRINTLN("[RCC MISSION] GET_TEAM_VEHICLE_FOR_SPEED_CHECK  - Vehicle is valid")
							
							IF tempTrailerVeh = viSpecificVeh
								returnVeh = tempTrailerVeh
								PRINTLN("[RCC MISSION] GET_TEAM_VEHICLE_FOR_SPEED_CHECK  DEBUG - returning trailer: ",NATIVE_TO_INT(returnVeh))
							ELSE
								returnVeh = tempVeh
								PRINTLN("[RCC MISSION] GET_TEAM_VEHICLE_FOR_SPEED_CHECK  - returning drivable vehicle: ", NATIVE_TO_INT(returnVeh))
							ENDIF
							PRINTLN("[RCC MISSION] GET_TEAM_VEHICLE_FOR_SPEED_CHECK  - MODEL NAME: ", GET_CREATOR_NAME_FOR_VEHICLE_MODEL(GET_ENTITY_MODEL(returnVeh)))

							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	ENDFOR
	
	IF iSlowestPart != -1
		returnVeh = GET_VEHICLE_PED_IS_USING(GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iSlowestPart))))
		iExplodeDriverPart = iSlowestPart
		RETURN TRUE
	ENDIF
	
	IF iSpecificVehIndex != -1
		PRINTLN("[RCC MISSION] GET_TEAM_VEHICLE_FOR_SPEED_CHECK - Checking a specific vehicle")
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iSpecificVehIndex])
		
			PRINTLN("[RCC MISSION] GET_TEAM_VEHICLE_FOR_SPEED_CHECK  - Vehicle is valid, returning")
			
			returnVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iSpecificVehIndex])

			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_PLANE_ALTITUDE(VEHICLE_INDEX vi)
	FLOAT fGroundHeight
	VECTOR vVehCoords = GET_ENTITY_COORDS(vi)
	IF GET_GROUND_Z_FOR_3D_COORD(vVehCoords, fGroundHeight, TRUE, TRUE)
		RETURN vVehCoords.z - fGroundHeight
	ELSE
		RETURN vVehCoords.z
	ENDIF
ENDFUNC

PROC PLAY_MIN_SPEED_SOUND(INT soundId, STRING soundName, ENTITY_INDEX ei, INT iCooldown = 0)
	STRING sSoundSet = "GTAO_Speed_Race_Sounds"
	BOOL bSoundsOverNetwork = TRUE
	INT iSoundNetworkRange = 20
	
	IF g_bMissionEnding
	OR NOT GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()
		PRINTLN("PLAY_MIN_SPEED_SOUND - Exiting early because the mission is ending or the renderphase is paused")
		EXIT
	ENDIF
	
	IF WVM_FLOW_IS_CURRENT_MISSION_WVM_MISSION()
		sSoundSet = "DLC_GR_WVM_MOC_Soundset"
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(stMinSpeedSndCooldown)
	OR HAS_NET_TIMER_EXPIRED(stMinSpeedSndCooldown, iCooldown)
		IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
			PLAY_SOUND_FRONTEND(soundId,soundName, sSoundSet)
		ELSE
			PLAY_SOUND_FROM_ENTITY(soundId, soundName, ei, sSoundSet, bSoundsOverNetwork, iSoundNetworkRange)
		ENDIF
		
		IF iCooldown > 0
			REINIT_NET_TIMER(stMinSpeedSndCooldown)
		ENDIF
	ENDIF
	
	PRINTLN("PLAY_MIN_SPEED_SOUND - soundId: ", soundId, " soundName: ", soundName)
	DEBUG_PRINTCALLSTACK()
ENDPROC

PROC PROCESS_SPEED_BAR_SOUNDS(BOOL bInDangerZone, VEHICLE_INDEX viSpeedVehicle, STRING sSoundSet)
	IF bInDangerZone
		IF NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_MIN_SPEED_EXPLODE_BAR_INCREASING)
			IF IS_SOUND_ID_VALID(iSoundIDVehBomb)
				STOP_SOUND(iSoundIDVehBomb)
			ENDIF
			IF IS_SOUND_ID_VALID(iSoundIDVehBomb2)
				STOP_SOUND(iSoundIDVehBomb2)
			ENDIF
			
			IF IS_PLAYER_IN_CREATOR_TRAILER(PlayerToUse)
			OR IS_PLAYER_IN_CREATOR_AIRCRAFT(PlayerToUse)
				IF NOT HAS_NET_TIMER_STARTED(stMinSpeedSndCooldown)
				OR HAS_NET_TIMER_EXPIRED(stMinSpeedSndCooldown, 2500)
					PLAY_SOUND_FRONTEND(iSoundIDVehBomb, "Armed", sSoundSet)
					REINIT_NET_TIMER(stMinSpeedSndCooldown)
				ENDIF
			ELSE
				PLAY_MIN_SPEED_SOUND(iSoundIDVehBomb, "Armed", viSpeedVehicle, 2500)
			ENDIF
			
			SET_BIT(iLocalBoolCheck13, LBOOL13_MIN_SPEED_EXPLODE_BAR_INCREASING)
		ELSE
			IF IS_SOUND_ID_VALID(iSoundIDVehBomb)
			AND HAS_SOUND_FINISHED(iSoundIDVehBomb)
				IF IS_SOUND_ID_VALID(iSoundIDVehBomb2)
				AND HAS_SOUND_FINISHED(iSoundIDVehBomb2)
					IF IS_PLAYER_IN_CREATOR_TRAILER(PlayerToUse)
					OR IS_PLAYER_IN_CREATOR_AIRCRAFT(PlayerToUse)
						PLAY_SOUND_FRONTEND(iSoundIDVehBomb2, "Countdown", sSoundSet)
					ELSE
						PLAY_MIN_SPEED_SOUND(iSoundIDVehBomb2, "Countdown", viSpeedVehicle)
					ENDIF						
				ENDIF
				SET_VARIABLE_ON_SOUND(iSoundIDVehBomb2, "Ctrl", SQRT(fDisplayExplodeProgress / 100.0))
			ENDIF
		ENDIF
		
		RESET_NET_TIMER(tdMOCCountdownStopTimer)
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck13, LBOOL13_MIN_SPEED_EXPLODE_BAR_INCREASING)
			IF NOT HAS_NET_TIMER_STARTED(tdMOCCountdownStopTimer)
				START_NET_TIMER(tdMOCCountdownStopTimer)
			ENDIF
			
			IF HAS_NET_TIMER_STARTED(tdMOCCountdownStopTimer)
				IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdMOCCountdownStopTimer, ciMOC_COUNTDOWN_STOP_TIME)
					IF IS_SOUND_ID_VALID(iSoundIDVehBomb)
						STOP_SOUND(iSoundIDVehBomb)
					ENDIF
					IF IS_SOUND_ID_VALID(iSoundIDVehBomb2)
						STOP_SOUND(iSoundIDVehBomb2)
					ENDIF
					
					IF IS_PLAYER_IN_CREATOR_TRAILER(PlayerToUse)
					OR IS_PLAYER_IN_CREATOR_AIRCRAFT(PlayerToUse)
						IF NOT HAS_NET_TIMER_STARTED(stMinSpeedSndCooldown)
						OR HAS_NET_TIMER_EXPIRED(stMinSpeedSndCooldown, 1000)
							PLAY_SOUND_FRONTEND(-1, "Count_Stop", sSoundSet)
							REINIT_NET_TIMER(stMinSpeedSndCooldown)
						ENDIF
					ELSE
						PLAY_MIN_SPEED_SOUND(-1, "Count_Stop", viSpeedVehicle, 1000)
					ENDIF											
					CLEAR_BIT(iLocalBoolCheck13, LBOOL13_MIN_SPEED_EXPLODE_BAR_INCREASING)
					RESET_NET_TIMER(tdMOCCountdownStopTimer)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF fDisplayExplodeProgress >= 100
		IF IS_SOUND_ID_VALID(iSoundIDVehBomb)
			STOP_SOUND(iSoundIDVehBomb)
		ENDIF
		IF IS_SOUND_ID_VALID(iSoundIDVehBomb2)
			STOP_SOUND(iSoundIDVehBomb2)
		ENDIF
		
		IF IS_PLAYER_IN_CREATOR_TRAILER(PlayerToUse)
		OR IS_PLAYER_IN_CREATOR_AIRCRAFT(PlayerToUse)
			IF NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_SPEED_BAR_AIRSTRIKE_CALLED)
			AND NOT g_bMissionEnding
				PLAY_SOUND_FRONTEND(iSoundIDVehBeacon, "Beacon", sSoundSet)
				SET_BIT(iLocalBoolCheck23, LBOOL23_SPEED_BAR_AIRSTRIKE_CALLED)
				MC_serverBD_4.fExplodeProgress = 100.0
				MC_serverBD_4.iExplodeUpdateIter++
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DRAW_VEHICLE_MIN_SPEED_EXPLOSION_CHECK_HUD()
	
	INT iTeam
	INT iRule
	
	BOOL bIsLiteralString = FALSE
	VEHICLE_INDEX tempVeh
	TEXT_LABEL_63 tl63_Temp
	INT iMinSpeed
	INT iMeterMax
	FLOAT fVehSpeed
	HUDORDER eHUDOrder = HUDORDER_DONTCARE
	HUDORDER eHUDOrder_Explode = HUDORDER_DONTCARE
	STRING sSoundSet = "GTAO_Speed_Race_Sounds"
	
	BOOL bUseAltitude
	
	IF WVM_FLOW_IS_CURRENT_MISSION_WVM_MISSION()
		sSoundSet = "DLC_GR_WVM_MOC_Soundset"
	ENDIF
	
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		
		iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		
		IF iRule < FMMC_MAX_RULES
		
			bUseAltitude = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[iRule], ciBS_RULE11_USE_ALTITUDE_SPEED_BAR)
		
			
			PRINTLN("[MMacK][ExplodeHUD] 1 - team ",iTeam)
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iRule], ciBS_RULE2_SPEEDBAR_T0 + iTeam)
			OR iTeam = MC_playerBD[iPartToUse].iteam //this will keep this working for legacy missions if any used this
				
				IF IS_PLAYER_IN_CREATOR_TRAILER(LocalPlayer)
				OR IS_PLAYER_IN_CREATOR_AIRCRAFT(PlayerToUse)
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_AIRSTRIKE_ON_SPEED_FAIL)
					AND IS_BIT_SET(iLocalBoolCheck23, LBOOL23_SPEED_BAR_AIRSTRIKE_CALLED)
					AND NOT bDisableExplosionBarKill
						IF HAS_NET_TIMER_STARTED(tdAirstrikeExplodeTimer)
							IF HAS_NET_TIMER_EXPIRED(tdAirstrikeExplodeTimer, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iSpeedFailTimerLength)							
								STOP_SOUND(iSoundIDVehBeacon)
							ENDIF
						ELSE							
							START_NET_TIMER(tdAirstrikeExplodeTimer)
						ENDIF
					ENDIF
				ENDIF
					
				PRINTLN("[MMacK][ExplodeHUD] 2")
				
				iMinSpeed = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinSpeed[iRule] 
				
				IF MC_ServerBD_4.iTeamLapsCompleted[iTeam] > 0
					iMinSpeed += (MC_ServerBD_4.iTeamLapsCompleted[iTeam] * g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinSpeed_AddPerLap[iRule])
				ENDIF
				
				iMeterMax = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinSpeed_MeterMax[iRule]
				
				IF GET_TEAM_VEHICLE_FOR_SPEED_CHECK(iTeam, tempVeh, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinSpeed_SpecificVehicle[iRule])
					
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iRule], ciBS_RULE11_SPEED_BAR_NOT_STEALTHED)
						IF DOES_ENTITY_EXIST(tempVeh)
						AND GET_ENTITY_MODEL(tempVeh) = AKULA
						AND NOT ARE_FOLDING_WINGS_DEPLOYED(tempVeh)
							PRINTLN("[JS][ExplodeHUD] - DRAW_VEHICLE_MIN_SPEED_EXPLOSION_CHECK_HUD - Akula not in stealth mode")
							EXIT
						ENDIF
					ENDIF
					
					BOOL bHasRealSpeed = TRUE
					IF IS_ENTITY_ALIVE(tempVeh) 
						IF VDIST2(GET_ENTITY_COORDS(LocalPlayerPed, FALSE),GET_ENTITY_COORDS(tempVeh, FALSE)) > POW(250,2)
						OR IS_LOCAL_PLAYER_IN_ANY_SIMPLE_INTERIOR()
							IF IS_LOCAL_PLAYER_IN_ANY_SIMPLE_INTERIOR()
								PRINTLN("[ExplodeHUD] - Setting bHasRealSpeed false. Inside an interior")
							ELSE
								PRINTLN("[ExplodeHUD] - Setting bHasRealSpeed false. Player far away from vehicle")
							ENDIF
							bHasRealSpeed = FALSE
						ENDIF
					ELSE
						PRINTLN("[ExplodeHUD] - Setting bHasRealSpeed false. Vehicle is dead.")
						bHasRealSpeed = FALSE
					ENDIF
					IF bHasRealSpeed
						IF bUseAltitude
							fVehSpeed = GET_PLANE_ALTITUDE(tempVeh)
						ELSE
							fVehSpeed = ABSF(GET_ENTITY_SPEED(tempVeh))
						ENDIF
						IF bExplodeSpeedUpdateThisFrame
							MC_playerBD[iLocalPart].fBombVehicleSpeed = fVehSpeed
						ENDIF
						fVehSpeedForAudioTrigger = fVehSpeed
					ELSE
						IF NOT (NOT IS_ENTITY_ALIVE(tempVeh)
						AND IS_PED_IN_VEHICLE(LocalPlayerPed, tempVeh))
							
							IF iExplodeDriverPart != -1
								fVehSpeed = MC_playerBD[iExplodeDriverPart].fBombVehicleSpeed
							ENDIF
							MC_playerBD[iLocalPart].fBombVehicleSpeed = 0.0
						ENDIF
					ENDIF
										
					PRINTLN("[MMacK][ExplodeHUD] 3a - iExplodeDriverPart = ",iExplodeDriverPart,", tempVeh = ",NATIVE_TO_INT(tempVeh),", fVehSpeed = ",fVehSpeed)
					
					IF iMeterMax <= 0
						IF tempVeh != NULL
						AND NOT bUseAltitude
							iMeterMax = ROUND(GET_VEHICLE_ESTIMATED_MAX_SPEED(tempVeh))
						ELSE
							iMeterMax = iMinSpeed * 4
						ENDIF
					ENDIF
					
					PRINTLN("[MMacK][ExplodeHUD] 3b - iMeterMax = ",iMeterMax," (globals value = ",g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinSpeed_MeterMax[iRule],")")
					
					IF NOT bUseAltitude
						IF iTeam = MC_playerBD[iPartToUse].iteam
							tl63_Temp = "SPDBR_SPEED"
						ELSE
							PED_INDEX driverPed = GET_PED_IN_VEHICLE_SEAT(tempVeh)
							
							IF (NOT IS_PED_INJURED(driverPed))
							AND IS_PED_A_PLAYER(driverPed)
								PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(driverPed)
								tl63_Temp = GET_PLAYER_NAME(tempplayer)
								bIsLiteralString = TRUE
							ELSE
								IF IS_BIT_SET(iCelebrationTeamNameIsNotLiteral, iTeam)
									tl63_Temp = TEXT_LABEL_TO_STRING(g_sMission_TeamName[iteam])
								ELSE
									tl63_Temp = g_sMission_TeamName[iteam]
									bIsLiteralString = TRUE
								ENDIF
							ENDIF
						ENDIF
					ELSE
						tl63_Temp = "SPDBR_ALT"
					ENDIF
					
					//url:bugstar:2503087
					eHUDOrder_Explode = HUDORDER_EIGHTHBOTTOM
					eHUDOrder = HUDORDER_SEVENTHBOTTOM
					
					//Scaling to get the min speed line at a multiple of 10% along the meter (as we've only got textures at 10% intervals)
					INT iLine = CEIL((TO_FLOAT(iMinSpeed) / TO_FLOAT(iMeterMax)) * 10) * 10 // 0 - 100 in steps of 10
					iMeterMax = CEIL( TO_FLOAT(iMinSpeed) * (100.0 / TO_FLOAT(iLine) ) )
					
					PERCENTAGE_METER_LINE percentLine = GET_PERCENTAGE_METER_LINE_FROM_PERCENTAGE_INT(iLine)
					
					PRINTLN("[MMacK][ExplodeHUD] 3c - iLine (% line will be at) = ",iLine,", new scaled iMeterMax = ",iMeterMax)
					
					FLOAT fBarMax = TO_FLOAT(iMeterMax)
					FLOAT fBarFill = CLAMP(fVehSpeed, 0, fBarMax)
					
					PRINTLN("[MMacK][ExplodeHUD] 3d - fBarFill (clamped fVehSpeed) = ",fBarFill,", fBarMax = ",fBarMax)
					
					IF iLastMinSpeed[iTeam] != iMinSpeed
					OR iLastMeterMax[iTeam] != iMeterMax
						fMinSpeedLerpProgress[iTeam] += 2 * fLastFrameTime // 0.5s to lerp
						PRINTLN("[MMacK][ExplodeHUD] 4a - LERPING! fMinSpeedLerpProgress = ",fMinSpeedLerpProgress[iTeam])
						
						IF fMinSpeedLerpProgress[iTeam] >= 1.0
							fMinSpeedLerpProgress[iTeam] = 0
							iLastMinSpeed[iTeam] = iMinSpeed
							iLastMeterMax[iTeam] = iMeterMax
							PRINTLN("[MMacK][ExplodeHUD] 4b - Lerping complete")
						ELSE
							FLOAT fOldBarFill = CLAMP(fVehSpeed, 0, TO_FLOAT(iLastMeterMax[iTeam]))
							
							PRINTLN("[MMacK][ExplodeHUD] 4c - Pre-lerp: fOldBarFill (bar fill with old meter max) = ",fOldBarFill,", fBarFill (bar fill with new meter max) = ",fBarFill)
							PRINTLN("[MMacK][ExplodeHUD] 4c - Pre-lerp: iLastMeterMax (old meter max) = ",iLastMeterMax[iTeam],", fBarMax (new meter max) = ",fBarMax)
							
							fBarFill = LERP_FLOAT(fOldBarFill, fBarFill, fMinSpeedLerpProgress[iTeam])
							fBarMax = LERP_FLOAT(TO_FLOAT(iLastMeterMax[iTeam]), fBarMax, fMinSpeedLerpProgress[iTeam])
							//fRedBar = LERP_FLOAT(TO_FLOAT(iLastMinSpeed[iTeam]) / TO_FLOAT(iLastMeterMax[iTeam]), fRedBar, fMinSpeedLerpProgress[iTeam])
							
							PRINTLN("[MMacK][ExplodeHUD] 4c - Post-lerp: fBarFill = ",fBarFill,", fBarMax = ",fBarMax)
						ENDIF
					ENDIF
					
					HUD_COLOURS eHUDColour
					
					IF (NOT bUseAltitude AND fVehSpeed <= iMinSpeed)
					OR (bUseAltitude AND fVehSpeed >= iMinSpeed)
						SET_BIT(iLocalBoolCheck27, LBOOL27_FLYING_TOO_HIGH)
						eHUDColour = HUD_COLOUR_RED
					ELSE
						CLEAR_BIT(iLocalBoolCheck27, LBOOL27_FLYING_TOO_HIGH)
						eHUDColour = HUD_COLOUR_GREEN
					ENDIF
					
					PRINTLN("[MMacK][ExplodeHUD] 3e (draw time!) - ROUND(fBarFill * 100) = ",ROUND(fBarFill * 100),", ROUND(fBarMax * 100) = ",ROUND(fBarMax * 100))
					
					BOOL bDrawAltimeter = bUseAltitude
					
					VEHICLE_INDEX tempSpecificVeh
					IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinSpeed_SpecificVehicle[iRule] > -1
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinSpeed_SpecificVehicle[iRule]])
							tempSpecificVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinSpeed_SpecificVehicle[iRule]])
							
							IF IS_PED_IN_VEHICLE(LocalPlayerPed, tempSpecificVeh)
								bDrawAltimeter = TRUE
								PRINTLN("[AltitudeGangChase] bDrawAltimeter to TRUE because we're in the specific vehicle")
							ELSE
								bDrawAltimeter = FALSE
								PRINTLN("[AltitudeGangChase] bDrawAltimeter to FALSE because we're not in the specific vehicle")
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
						bDrawAltimeter = FALSE
						SET_FAKE_MINIMAP_MAX_ALTIMETER_HEIGHT(0)
						PRINTLN("[AltitudeGangChase] bDrawAltimeter to FALSE because we're not in ANY vehicle")
					ENDIF
					IF NOT bDrawAltimeter
						IF IS_PLAYER_IN_CREATOR_AIRCRAFT(PlayerToUse)
						OR IS_PED_IN_ANY_VEHICLE(PlayerPedToUse)
							DRAW_GENERIC_METER(ROUND(fBarFill * 100), ROUND(fBarMax * 100), tl63_Temp, eHUDColour, DEFAULT, eHUDOrder, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 0, bIsLiteralString, percentLine)
						ENDIF
						
						SET_FAKE_MINIMAP_MAX_ALTIMETER_HEIGHT(0)
					ELSE
						VEHICLE_INDEX viVehToUse = NULL
						
						IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinSpeed_SpecificVehicle[iRule] = -1
							IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
								viVehToUse = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
							ENDIF
						ELSE
							viVehToUse = tempSpecificVeh
						ENDIF
						
						IF DOES_ENTITY_EXIST(viVehToUse)
							PRINTLN("[AltitudeGangChase][1] Drawing SET_FAKE_MINIMAP_MAX_ALTIMETER_HEIGHT with min speed of ", TO_FLOAT(iMinSpeed))
							
							FLOAT fGroundZ = 0
							IF GET_GROUND_Z_FOR_3D_COORD(GET_ENTITY_COORDS(viVehToUse), fGroundZ, TRUE, TRUE)
								#IF IS_DEBUG_BUILD
								IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
									VECTOR vDebugPos = GET_ENTITY_COORDS(LocalPlayerPed)
									vDebugPos.z = fGroundZ
									DRAW_DEBUG_SPHERE(vDebugPos, 3, 255, 0, 0, 200)
									DRAW_DEBUG_LINE(GET_ENTITY_COORDS(LocalPlayerPed), vDebugPos)
								ENDIF
								#ENDIF
								IF fGroundZ > 0.0
									iMinSpeed += ROUND(fGroundZ)
								ENDIF
							ENDIF
						
							IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
							AND (IS_PED_IN_ANY_HELI(LocalPlayerPed) OR IS_PED_IN_ANY_PLANE(LocalPlayerPed))
								SET_FAKE_MINIMAP_MAX_ALTIMETER_HEIGHT(TO_FLOAT(iMinSpeed), TRUE)
								bShowingAltitudeLimit = TRUE
							ELSE
								SET_FAKE_MINIMAP_MAX_ALTIMETER_HEIGHT(0)
								bShowingAltitudeLimit = FALSE
							ENDIF
						ELSE
							SET_FAKE_MINIMAP_MAX_ALTIMETER_HEIGHT(0)
							PRINTLN("[AltitudeGangChase] Not drawing altimeter because the vehicle we want to base it on doesn't exist")
						ENDIF
					ENDIF					
					
					IF iExplodeDriverPart = iLocalPart
					OR IS_VEHICLE_A_TRAILER(tempSpecificVeh)
					OR ((iSpectatorTarget != -1) AND (iExplodeDriverPart = iSpectatorTarget))
					OR (iExplodeDriverPart != -1 AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinSpeed_SpecificVehicle[iRule] != -1 AND MC_playerBD[iExplodeDriverPart].iteam = MC_playerBD[iLocalPart].iteam)
						///EXPLODE BAR:
						
						FLOAT fBDProgress
						BOOL bInDangerZone = FALSE
						
						IF iExplodeDriverPart != -1
							fBDProgress =  TO_FLOAT(MC_playerBD[iExplodeDriverPart].iMinSpeed_ExplodeProgress)
						ELSE
							fBDProgress = fRealExplodeProgress
						ENDIF	
						
						IF SHOULD_USE_LOCAL_MINIMUM_VEH_SPEED_CHECK()
						
							FLOAT fDisplayMultiplier = 1.0075 // Make the bar display a value slightly ahead of what it actually is, so that the player sees the bar being full for a few frames before exploding, rather than exploding with the bar at about 95% visually
							
							IF fDisplayExplodeProgress < fBDProgress * fDisplayMultiplier
								bInDangerZone = TRUE
							ENDIF
							
							fDisplayExplodeProgress = LERP_FLOAT(fDisplayExplodeProgress, fBDProgress, 2.0 * GET_FRAME_TIME())
							fDisplayExplodeProgress *= fDisplayMultiplier
						ELSE
							IF fDisplayExplodeProgress != fBDProgress
								IF fDisplayExplodeProgress < fBDProgress
									bInDangerZone = TRUE
									fDisplayExplodeProgress = CLAMP( fDisplayExplodeProgress + (((fLastFrameTime*1.25) / TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iFailSpeedTimer[iRule])) * 100), 0, fBDProgress)
								ELSE
									fDisplayExplodeProgress = CLAMP( fDisplayExplodeProgress - (((fLastFrameTime*1.25) / TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iFailSpeedTimer[iRule])) * 100), fBDProgress, 100)
								ENDIF
								
							ENDIF
						ENDIF
					
						PRINTLN("[ExplodeHUD] 3h Spectator player Explode Percentage =  ",fBDProgress)
						
						IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
							PROCESS_SPEED_BAR_SOUNDS(bInDangerZone, tempVeh, sSoundSet)
						ENDIF
				
						IF IS_BIT_SET(iLocalBoolCheck23, LBOOL23_SPEED_BAR_AIRSTRIKE_CALLED)
							fDisplayExplodeProgress = 100.0
							PRINTLN("[ExplodeHUD] Setting explode percentage to 100, airstrike called")
						ENDIF
						PRINTLN("[ExplodeHUD] 3i Drawing Explode Percentage = ", fDisplayExplodeProgress)
						TEXT_LABEL_63 tl63 = "SPDBR_EXPLD"
						BOOL bLiteral = FALSE
						IF MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iPartToUse].iTeam] < FMMC_MAX_RULES
						AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iPartToUse].iTeam].iRuleBitsetNine[MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iPartToUse].iTeam]], ciBS_RULE9_EXPLODE_BAR_TEAM_TARGET_STRING)
							tl63 = g_FMMC_STRUCT.tl23CustomTargetString[MC_PlayerBD[iPartToUse].iTeam]
							bLiteral = TRUE
						ENDIF
						
						DRAW_GENERIC_METER(ROUND(fDisplayExplodeProgress), 100, tl63, HUD_COLOUR_RED, DEFAULT, eHUDOrder_Explode, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, bLiteral)
						
						#IF IS_DEBUG_BUILD
						IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
							TEXT_LABEL_63 tlDebug = "DisplayProgress = "
							tlDebug += FLOAT_TO_STRING(fDisplayExplodeProgress)
							draw_debug_text_2d(tlDebug, <<0.2, 0.2, 0.0>>)
							
							TEXT_LABEL_63 tlDebug2 = "Real Progress = "
							tlDebug2 += FLOAT_TO_STRING(fRealExplodeProgress)
							draw_debug_text_2d(tlDebug2, <<0.2, 0.265, 0.0>>)
						ENDIF
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDFOR	
ENDPROC

PROC PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED()

	BOOL bUseAltitude
	bExplodeSpeedUpdateThisFrame = FALSE
	IF HAS_NET_TIMER_STARTED(tdExplodeSpeedUpdateTimer)
		IF HAS_NET_TIMER_EXPIRED(tdExplodeSpeedUpdateTimer, 500)
			bExplodeSpeedUpdateThisFrame = TRUE
			REINIT_NET_TIMER(tdExplodeSpeedUpdateTimer)
		ENDIF
		
		IF fRealExplodeProgress >= 100
			bExplodeSpeedUpdateThisFrame = TRUE
		ENDIF 
		
	ELSE
		START_NET_TIMER(tdExplodeSpeedUpdateTimer)
		bExplodeSpeedUpdateThisFrame = TRUE
	ENDIF

	#IF IS_DEBUG_BUILD
	IF bSRDebugToggleOffMeterLogic
		EXIT
	ENDIF
	#ENDIF
	
	IF bDisableExplosionBarKill
		EXIT
	ENDIF
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	INT iMinSpeed
	FLOAT fVehSpeed
	VEHICLE_INDEX tempVeh, tempTrailer, tempSpecificVeh
	BOOL bInCar, bProcessSpecificVehicle, bHasSpecificVeh, bProcessAsPassenger
	
	iMinSpeed = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinSpeed[iRule]
	
	IF MC_ServerBD_4.iTeamLapsCompleted[iTeam] > 0
		iMinSpeed += (MC_ServerBD_4.iTeamLapsCompleted[iTeam] * g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinSpeed_AddPerLap[iRule])
	ENDIF
	
	IF iRule < FMMC_MAX_RULES
		bUseAltitude = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[iRule], ciBS_RULE11_USE_ALTITUDE_SPEED_BAR)
		
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinSpeed_SpecificVehicle[iRule] > -1
			bProcessSpecificVehicle = TRUE
			
			IF iSpectatorTarget = -1
				IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
					GET_VEHICLE_TRAILER_VEHICLE(tempVeh, tempTrailer)
				ENDIF
				
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinSpeed_SpecificVehicle[iRule]])
					tempSpecificVeh = (NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinSpeed_SpecificVehicle[iRule]]))
					IF tempTrailer = NULL
						IF IS_VEHICLE_A_TRAILER(tempSpecificVeh)
							tempTrailer = tempSpecificVeh
						ENDIF
					ENDIF
					bHasSpecificVeh = TRUE					
				ELSE
					PRINTLN("[JS][ExplodeHUD][MINIMUMSPEED] - PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - Specific vehicle does not exist")
				ENDIF
				
			ENDIF
		ELSE
			IF bUseAltitude
				IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					SET_FAKE_MINIMAP_MAX_ALTIMETER_HEIGHT(0)
				ENDIF
			ENDIF
		ENDIF 
	ENDIF
	
	IF iSpectatorTarget = -1
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		OR bProcessSpecificVehicle
			
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
			ENDIF
			
			IF bHasSpecificVeh
			AND tempSpecificVeh != NULL
				tempVeh = tempSpecificVeh
			ELSE
				IF bProcessSpecificVehicle
					PRINTLN("[RCC MISSION] PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - bProcessSpecificVehicle and tempSpecificVeh = NULL")
					IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_SPEED_FAIL_TRIGGERED)
						PRINTLN("[RCC MISSION] PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - clearing speed fail triggered 3")
						CLEAR_BIT(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_SPEED_FAIL_TRIGGERED)
					ENDIF
					EXIT
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(tempVeh)
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iRule], ciBS_RULE11_SPEED_BAR_NOT_STEALTHED)
					IF DOES_ENTITY_EXIST(tempVeh)
					AND GET_ENTITY_MODEL(tempVeh) = AKULA
					AND NOT ARE_FOLDING_WINGS_DEPLOYED(tempVeh)
						PRINTLN("[JS][ExplodeHUD][MINIMUMSPEED] - PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - Akula not in stealth mode")
						EXIT
					ENDIF
				ENDIF
				
				IF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_PREDATOR)
				AND GET_ENTITY_MODEL(tempVeh) = AKULA
					bProcessAsPassenger = TRUE
				ENDIF
			
				IF IS_VEHICLE_DRIVEABLE(tempVeh)
					
					IF IS_LOCAL_PLAYER_IN_ANY_SIMPLE_INTERIOR()
					OR VDIST2(GET_ENTITY_COORDS(LocalPlayerPed, FALSE),GET_ENTITY_COORDS(tempVeh, FALSE)) > POW(250, 2)
						IF iExplodeDriverPart != -1
							fVehSpeed = MC_playerBD[iExplodeDriverPart].fBombVehicleSpeed
						ENDIF
						MC_playerBD[iLocalPart].fBombVehicleSpeed = 0.0
					ELSE
						IF bUseAltitude
							fVehSpeed = GET_PLANE_ALTITUDE(tempVeh)
						ELSE
							fVehSpeed = ABSF(GET_ENTITY_SPEED(tempVeh))
						ENDIF
						IF bExplodeSpeedUpdateThisFrame
							MC_playerBD[iLocalPart].fBombVehicleSpeed = fVehSpeed
						ENDIF
						fVehSpeedForAudioTrigger = fVehSpeed
					ENDIF
					
					IF bProcessSpecificVehicle
						IF bExplodeSpeedUpdateThisFrame
						AND bIsLocalPlayerHost
							MC_serverBD_4.fVehSpeed = fVehSpeed
						ELSE
							fVehSpeed = MC_serverBD_4.fVehSpeed
						ENDIF
					ENDIF
					
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iRule], ciBS_RULE4_DontExplodeWhenUnderMinSpeed)
					AND ((IS_PLAYER_IN_VEH_SEAT(LocalPlayer,VS_DRIVER)
					AND NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)))
					OR bProcessSpecificVehicle
					OR bProcessAsPassenger)

						bInCar = TRUE
						
						BOOL bExplode
						
						IF g_FMMC_STRUCT.iSpeedRaceCheckpointTimer != -1
							IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
							OR (NOT HAS_NET_TIMER_STARTED(tdSpeedRaceCheckpointTimer))
								PRINTLN("[RCC MISSION][MINIMUMSPEED] PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - New rule ",iRule," for my team ",iTeam,", reinit checkpoint timer")
								REINIT_NET_TIMER(tdSpeedRaceCheckpointTimer)
							ENDIF
							
							IF HAS_NET_TIMER_STARTED(tdSpeedRaceCheckpointTimer)
							AND (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdSpeedRaceCheckpointTimer) > (g_FMMC_STRUCT.iSpeedRaceCheckpointTimer * 1000))
								PRINTLN("[RCC MISSION][MINIMUMSPEED] PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - Out of checkpoint time (",g_FMMC_STRUCT.iSpeedRaceCheckpointTimer," seconds), explode!")
								bExplode = TRUE
							ENDIF
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_AIRSTRIKE_ON_SPEED_FAIL)
						AND IS_BIT_SET(iLocalBoolCheck23, LBOOL23_SPEED_BAR_AIRSTRIKE_CALLED)
						AND NOT bDisableExplosionBarKill
							IF HAS_NET_TIMER_STARTED(tdAirstrikeExplodeTimer)

								IF IS_SOUND_ID_VALID(iSoundIDVehBeacon)
									IF NOT HAS_SOUND_FINISHED(iSoundIDVehBeacon)
										IF (GET_FRAME_COUNT() % 5) = 0
											VECTOR vSfxAirstrikeTargetCoords = GET_ENTITY_COORDS(tempVeh)
											VECTOR vSfxForwardVec = GET_ENTITY_FORWARD_VECTOR(tempVeh)
											vSfxForwardVec.x = vSfxForwardVec.x * GET_ENTITY_SPEED(tempVeh)
											vSfxForwardVec.y = vSfxForwardVec.y * GET_ENTITY_SPEED(tempVeh)

											vSfxAirstrikeTargetCoords.x += vSfxForwardVec.x
											vSfxAirstrikeTargetCoords.y += vSfxForwardVec.y
											
											UPDATE_SOUND_COORD(iSoundIDVehBeacon, vSfxAirstrikeTargetCoords)
										ENDIF
									ENDIF
								ENDIF
								
								IF HAS_NET_TIMER_EXPIRED(tdAirstrikeExplodeTimer, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iSpeedFailTimerLength)
									
									STOP_SOUND(iSoundIDVehBeacon)
									
									IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
									AND IS_ENTITY_ALIVE(tempVeh)
										IF NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_AIRSTRIKE_SPEED_BAR_FIRED)
											vAirstrikeTargetCoords = GET_ENTITY_COORDS(tempVeh)
											VECTOR vForwardVec = GET_ENTITY_FORWARD_VECTOR(tempVeh)
											vForwardVec.x = vForwardVec.x * GET_ENTITY_SPEED(tempVeh)
											vForwardVec.y = vForwardVec.y * GET_ENTITY_SPEED(tempVeh)

											vAirstrikeTargetCoords.x += vForwardVec.x
											vAirstrikeTargetCoords.y += vForwardVec.y
											
											SET_BIT(iLocalBoolCheck23, LBOOL23_AIRSTRIKE_SPEED_BAR_FIRED)
											PRINTLN("[RCC MISSION][MINIMUMSPEED][AIRSTRIKE] Setting LBOOL23_AIRSTRIKE_SPEED_BAR_FIRED")
										ELSE
											PRINTLN("[RCC MISSION][MINIMUMSPEED][AIRSTRIKE][CALL] Calling DO_SCRIPTED_AIRSTRIKE from: AIRSTRIKE BAR FULL")
											DO_SCRIPTED_AIRSTRIKE(vAirstrikeTargetCoords, 5)
										ENDIF
										
										IF HAS_NET_TIMER_EXPIRED(tdAirstrikeExplodeTimer, (g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iSpeedFailTimerLength + 1500))
											ADD_EXPLOSION(vAirstrikeTargetCoords, EXP_TAG_HI_OCTANE, 1.0)
											NETWORK_EXPLODE_VEHICLE(tempVeh, TRUE, TRUE)
										ENDIF
									ENDIF
								ENDIF
							ELSE
								REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("w_lr_rpg_rocket")))	
								START_NET_TIMER(tdAirstrikeExplodeTimer)
								PRINTLN("[RCC MISSION][MINIMUMSPEED][AIRSTRIKE] Starting Explode timer!")
								// Only WVM Airstrike thing.
								IF WVM_FLOW_IS_CURRENT_MISSION_WVM_MISSION()
								AND NOT g_bMissionEnding
									PLAY_SOUND_FROM_COORD(iSoundIDVehBeacon, "Beacon", GET_ENTITY_COORDS(tempVeh), "DLC_GR_WVM_MOC_Soundset", TRUE, 20)
								ENDIF
							ENDIF
						ENDIF

						IF (NOT bUseAltitude AND fVehSpeed <= iMinSpeed)
						OR (bUseAltitude AND fVehSpeed >= iMinSpeed)
						OR bExplode
							IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iFailSpeedTimer[iRule] > 0
								
								// Cache prev frame progress
								fRealExplodeProgress = CLAMP(fRealExplodeProgress + ((fLastFrameTime / TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iFailSpeedTimer[iRule])) * 100), 0, 100)
								PRINTLN("[RCC MISSION][MINIMUMSPEED] - Increasing fRealExplodeProgress to ", fRealExplodeProgress)
								
								IF bExplodeSpeedUpdateThisFrame
									PRINTLN("[RCC MISSION][MINIMUMSPEED] - Increasing update")
									IF bIsLocalPlayerHost
									AND NOT SHOULD_USE_LOCAL_MINIMUM_VEH_SPEED_CHECK()
										MC_serverBD_4.fExplodeProgress = fRealExplodeProgress
										MC_serverBD_4.iExplodeUpdateIter++
									ENDIF
									MC_playerBD[iLocalPart].fBombVehicleSpeed = fVehSpeed
									MC_playerBD[iLocalPart].iMinSpeed_ExplodeProgress = ROUND(fRealExplodeProgress)
								ENDIF
								
								IF NOT bIsLocalPlayerHost
								AND iExplodeProgressUpdate != MC_serverBD_4.iExplodeUpdateIter
								AND NOT SHOULD_USE_LOCAL_MINIMUM_VEH_SPEED_CHECK()
									iExplodeProgressUpdate = MC_serverBD_4.iExplodeUpdateIter
									fRealExplodeProgress = MC_serverBD_4.fExplodeProgress
									PRINTLN("[RCC MISSION][MINIMUMSPEED] - Setting fRealExplodeProgress to MC_serverBD_4.fExplodeProgress which is: ", fRealExplodeProgress)
								ENDIF
								
								IF fRealExplodeProgress >= 100.0
								AND (IS_PED_IN_VEHICLE(LocalPlayerPed, tempVeh)
									OR (IS_VEHICLE_EMPTY(tempVeh) AND bHasSpecificVeh))
									PRINTLN("[RCC MISSION][MINIMUMSPEED] PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - Out of safety time, explode!")
									bExplode = TRUE
								ENDIF
								
							ELSE
								PRINTLN("[RCC MISSION][MINIMUMSPEED] PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - No safety timer allowed, explode!")
								bExplode = TRUE
							ENDIF
													
							IF NOT bExplode
								IF NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_MIN_SPEED_EXPLODE_BAR_INCREASING)
									IF IS_SOUND_ID_VALID(iSoundIDVehBomb)
										STOP_SOUND(iSoundIDVehBomb)
									ENDIF
									IF IS_SOUND_ID_VALID(iSoundIDVehBomb2)
										STOP_SOUND(iSoundIDVehBomb2)
									ENDIF
									
									PLAY_MIN_SPEED_SOUND(iSoundIDVehBomb, "Armed", tempVeh, 2500)
									
									SET_BIT(iLocalBoolCheck13, LBOOL13_MIN_SPEED_EXPLODE_BAR_INCREASING)
								ELSE
									IF IS_SOUND_ID_VALID(iSoundIDVehBomb)
									AND HAS_SOUND_FINISHED(iSoundIDVehBomb)
										IF IS_SOUND_ID_VALID(iSoundIDVehBomb2)
										AND HAS_SOUND_FINISHED(iSoundIDVehBomb2)
											PLAY_MIN_SPEED_SOUND(iSoundIDVehBomb2, "Countdown", tempVeh)
										ENDIF
										SET_VARIABLE_ON_SOUND(iSoundIDVehBomb2, "Ctrl", SQRT(fRealExplodeProgress / 100.0))
									ENDIF
								ENDIF
								IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_SPEED_FAIL_TRIGGERED)
									PRINTLN("[RCC MISSION][MINIMUMSPEED] PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - clearing speed fail triggered")
									CLEAR_BIT(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_SPEED_FAIL_TRIGGERED)
								ENDIF
							ENDIF
							
							SET_CONTROL_SHAKE(PLAYER_CONTROL, 130, ROUND(fRealExplodeProgress) + 80)
							
							IF IS_PLAYSTATION_PLATFORM()
								SET_CONTROL_LIGHT_EFFECT_COLOR(PLAYER_CONTROL, 255, 0, 0)
								SET_BIT(iLocalBoolCheck13, LBOOL13_CONTROL_PAD_COLOUR_SHOULD_BE_RESET) // Resets the pad colour in PROCESS_PRE_MAIN_CHECKS (it's already done in MC_SCRIPT_CLEANUP)
							ENDIF
							
							IF bExplode
							OR IS_VEHICLE_IN_WATER( tempVeh )
								IF IS_SOUND_ID_VALID(iSoundIDVehBomb)
									STOP_SOUND(iSoundIDVehBomb)
								ENDIF
								IF IS_SOUND_ID_VALID(iSoundIDVehBomb2)
									STOP_SOUND(iSoundIDVehBomb2)
								ENDIF
								
								IF IS_SOUND_ID_VALID(iSoundIDVehBombCountdown)
									STOP_SOUND(iSoundIDVehBombCountdown)
								ENDIF
								
								CLEAR_BIT(iLocalBoolCheck13, LBOOL13_MIN_SPEED_EXPLODE_BAR_INCREASING)
								
								IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinSpeed_SpecificVehicle[iRule] = -1
								AND NOT bUseAltitude // gang chase instead.
								AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_STUNTING_PACK(g_FMMC_STRUCT.iAdversaryModeType)
								
									IF NOT IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetFour[ iRule ], ciBS_RULE4_HIDE_DEATH_TICKERS )
									OR IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetFive[ iRule ], ciBS_RULE5_OVERRIDE_DEATH_TICKERS_TO_SHOW_SPEED_BLOW_UP )
										
										SCRIPT_EVENT_DATA_TICKER_CUSTOM_MESSAGE TickerMessage
										
										IF g_FMMC_STRUCT.iSpeedRaceCheckpointTimer != -1
										AND HAS_NET_TIMER_STARTED(tdSpeedRaceCheckpointTimer)
										AND (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdSpeedRaceCheckpointTimer) > (g_FMMC_STRUCT.iSpeedRaceCheckpointTimer * 1000))
											TickerMessage.TickerEvent = TICKER_EVENT_CHECKPOINT_FAIL_TIMER_EXPLODE
										ELSE
											TickerMessage.TickerEvent = TICKER_EVENT_BELOW_SPEED_EXPLODE
										ENDIF
										
										TickerMessage.TeamInt = iTeam
										TickerMessage.iTeamToUse = iTeam
										TickerMessage.iRule = iRule
										TickerMessage.playerID = LocalPlayer
										TickerMessage.iSubType = -1
										
										BROADCAST_CUSTOM_TICKER_EVENT(TickerMessage, ALL_PLAYERS_ON_SCRIPT(FALSE))
									ENDIF
								ENDIF
								
								#IF IS_DEBUG_BUILD
								IF NOT GET_PLAYER_INVINCIBLE(LocalPlayer)
								#ENDIF
									//They're about to die from being blown up, so set the following bit to stop the suicide ticker from showing up:
									SET_BIT(g_i_IgnoreSuicideTicker_PlayerBS, NATIVE_TO_INT(LocalPlayer))
								#IF IS_DEBUG_BUILD
								ELSE
									PRINTLN("[RCC MISSION][MINIMUMSPEED] PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - We're exploding, but WE'RE INVINCIBLE!")
								ENDIF
								#ENDIF
								
								#IF IS_DEBUG_BUILD
								IF g_FMMC_STRUCT.iSpeedRaceCheckpointTimer != -1
								AND HAS_NET_TIMER_STARTED(tdSpeedRaceCheckpointTimer)
								AND (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdSpeedRaceCheckpointTimer) > (g_FMMC_STRUCT.iSpeedRaceCheckpointTimer * 1000))
									PRINTLN("[RCC MISSION][MINIMUMSPEED] PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - Checkpoint timer took too long, call NETWORK_EXPLODE_VEHICLE")
								ELSE
									PRINTLN("[RCC MISSION][MINIMUMSPEED] PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - Speed dropped below threshold of ",iMinSpeed," and safety timer expired, call NETWORK_EXPLODE_VEHICLE")
								ENDIF
								#ENDIF
								
								IF NOT IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_SPEED_FAIL_TRIGGERED)
									PRINTLN("[RCC MISSION][MINIMUMSPEED] PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - Setting speed fail triggered")
									SET_BIT(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_SPEED_FAIL_TRIGGERED)
								ENDIF
								
								IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_AIRSTRIKE_ON_SPEED_FAIL)
									IF NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_SPEED_BAR_AIRSTRIKE_CALLED)
										SET_BIT(iLocalBoolCheck23, LBOOL23_SPEED_BAR_AIRSTRIKE_CALLED)
										IF bIsLocalPlayerHost
											MC_serverBD_4.fExplodeProgress = 100.0
											MC_serverBD_4.iExplodeUpdateIter++
										ENDIF
									ENDIF
									
								ELSE
									IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
										IF bUseAltitude
											PRINTLN("[MINIMUMSPEED] PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - Allowing Gang Chase to spawn peds rather than exploding vehicle")
										ELSE
											NETWORK_EXPLODE_VEHICLE(tempVeh, TRUE, TRUE)
											
											IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_STUNTING_PACK(g_FMMC_STRUCT.iAdversaryModeType)
												ADD_EXPLOSION(GET_ENTITY_COORDS(tempVeh), EXP_TAG_HI_OCTANE, 1.0, DEFAULT, DEFAULT, DEFAULT, TRUE)
											ENDIF
											PRINTLN("[MINIMUMSPEED] PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - Exploding vehicle now")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_SPEED_FAIL_TRIGGERED)
								PRINTLN("[RCC MISSION][MINIMUMSPEED] PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - clearing speed fail triggered 2")
								CLEAR_BIT(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_SPEED_FAIL_TRIGGERED)
							ENDIF
							IF fRealExplodeProgress > 0
							OR MC_playerBD[iLocalPart].iMinSpeed_ExplodeProgress > 0

								// Cache prev frame progress
								
								fRealExplodeProgress = CLAMP(fRealExplodeProgress - ((fLastFrameTime / TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iFailSpeedTimer[iRule])) * 100), 0, 100)
								PRINTLN("[RCC MISSION][MINIMUMSPEED] - Decreasing fRealExplodeProgress to ", fRealExplodeProgress)
								
								IF bExplodeSpeedUpdateThisFrame
									PRINTLN("[RCC MISSION][MINIMUMSPEED] - Decreasing update")
									IF bIsLocalPlayerHost
										MC_serverBD_4.fExplodeProgress = fRealExplodeProgress
										MC_serverBD_4.iExplodeUpdateIter++
									ENDIF
									MC_playerBD[iLocalPart].fBombVehicleSpeed = fVehSpeed
									MC_playerBD[iLocalPart].iMinSpeed_ExplodeProgress = ROUND(fRealExplodeProgress)
								ENDIF
								
								IF NOT bIsLocalPlayerHost
								AND NOT SHOULD_USE_LOCAL_MINIMUM_VEH_SPEED_CHECK()
								AND iExplodeProgressUpdate != MC_serverBD_4.iExplodeUpdateIter
									iExplodeProgressUpdate = MC_serverBD_4.iExplodeUpdateIter
									fRealExplodeProgress = MC_serverBD_4.fExplodeProgress
								ENDIF
								
								IF IS_BIT_SET(iLocalBoolCheck13, LBOOL13_MIN_SPEED_EXPLODE_BAR_INCREASING)
									IF IS_SOUND_ID_VALID(iSoundIDVehBomb)
										STOP_SOUND(iSoundIDVehBomb)
									ENDIF
									IF IS_SOUND_ID_VALID(iSoundIDVehBomb2)
										STOP_SOUND(iSoundIDVehBomb2)
									ENDIF
									
									PLAY_MIN_SPEED_SOUND(iSoundIDVehBomb, "Count_Stop", tempVeh, 1000)
									
									CLEAR_BIT(iLocalBoolCheck13, LBOOL13_MIN_SPEED_EXPLODE_BAR_INCREASING)
								ENDIF
								
								IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
									IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciALLOW_MISSION_AIRSTRIKES)
										IF ROUND(fRealExplodeProgress) > g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iStartPercentage
										AND NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_IN_PROGRESS)
											IF NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_AIRSTRIKE_FIFTY_PERCENT_STRIKE)
												PRINTLN("[RCC MISSION][AIRSTRIKE][OFFSET] Setting LBOOL23_AIRSTRIKE_FIFTY_PERCENT_STRIKE")
												SET_BIT(iLocalBoolCheck23, LBOOL23_AIRSTRIKE_FIFTY_PERCENT_STRIKE)
												IF NOT HAS_NET_TIMER_STARTED(tdAirstrikeStartTimer)
													START_NET_TIMER(tdAirstrikeStartTimer)
												ENDIF
											ENDIF
										ELSE
											IF IS_BIT_SET(iLocalBoolCheck23, LBOOL23_AIRSTRIKE_FIFTY_PERCENT_STRIKE)
												PRINTLN("[RCC MISSION][AIRSTRIKE][OFFSET] Clearing LBOOL23_AIRSTRIKE_FIFTY_PERCENT_STRIKE")
												CLEAR_BIT(iLocalBoolCheck23, LBOOL23_AIRSTRIKE_FIFTY_PERCENT_STRIKE)
											ENDIF
										ENDIF								
									ENDIF
								ENDIF
								
								SET_CONTROL_SHAKE(PLAYER_CONTROL, 130, ROUND(fRealExplodeProgress) + 80)
								
								IF IS_PLAYSTATION_PLATFORM()
									SET_CONTROL_LIGHT_EFFECT_COLOR(PLAYER_CONTROL, 255, 0, 0)
									SET_BIT(iLocalBoolCheck13, LBOOL13_CONTROL_PAD_COLOUR_SHOULD_BE_RESET) // Resets the pad colour in PROCESS_PRE_MAIN_CHECKS (it's already done in MC_SCRIPT_CLEANUP)
								ENDIF
								
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_SPECTATOR_HUD_HIDDEN()
	AND NOT g_bMissionEnding
		DRAW_VEHICLE_MIN_SPEED_EXPLOSION_CHECK_HUD()
	ELSE
		PRINTLN("[TMS] Not drawing explosion HUD this frame due to HUD being hidden or the mission ending")
	ENDIF
	
	IF NOT bInCar
		fRealExplodeProgress = 0
	ENDIF
	
ENDPROC

PROC PROCESS_WEAPON_RESPAWN_AND_COLLECTION()
	INT iWep
//	INT iPlacementFlags

	FOR iWep = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons-1
		IF DOES_PICKUP_EXIST(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWep].id)
			IF DOES_PICKUP_OBJECT_EXIST(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWep].id)
			AND NOT HAS_PICKUP_BEEN_COLLECTED(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWep].id)
				IF IS_BIT_SET(iPickupCollected[GET_LONG_BITSET_INDEX(iWep)], GET_LONG_BITSET_BIT(iWep))
				
					PRINTLN("[LM][PROCESS_WEAPON_RESPAWN_AND_COLLECTION] - Respawned Pickup.")
					CLEAR_BIT(iPickupCollected[GET_LONG_BITSET_INDEX(iWep)], GET_LONG_BITSET_BIT(iWep))
					
					IF bIsLocalPlayerHost
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_LAND_GRAB_SOUNDS)
						OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_RESURRECTION_SOUNDS)
						OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_VEHICULAR_WARFARE_SOUNDS)
						OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_HOSTILE_TAKEOVER_SOUNDS)
						OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_HARD_TARGET_AUDIO)
							BROADCAST_FMMC_RESPAWNED_WEAPON(iLocalPart, iWep)
						ENDIF
				
						IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARZONE(g_FMMC_STRUCT.iAdversaryModeType)						
							IF iWep < ciMAX_PTFX_FOR_WEAPONS
								IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxWeaponGlow[iWep])
									USE_PARTICLE_FX_ASSET("scr_sr_adversary")
									ptfxWeaponGlow[iWep] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_sr_lg_weapon_highlight", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWep].vPos, <<0.0, 0.0, 0.0>>, 1.0)
									PRINTLN("[LM][PROCESS_WEAPON_RESPAWN_AND_COLLECTION] - Creating Glow")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					

				ENDIF
			ELSE
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARZONE(g_FMMC_STRUCT.iAdversaryModeType)
					IF bIsLocalPlayerHost
						IF iWep < ciMAX_PTFX_FOR_WEAPONS						
							IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxWeaponGlow[iWep])
								STOP_PARTICLE_FX_LOOPED(ptfxWeaponGlow[iWep])
								PRINTLN("[LM][PROCESS_WEAPON_RESPAWN_AND_COLLECTION] - Removing Glow")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF HAS_PICKUP_BEEN_COLLECTED(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWep].id)
				IF NOT IS_BIT_SET(iPickupCollected[GET_LONG_BITSET_INDEX(iWep)], GET_LONG_BITSET_BIT(iWep))
					SET_BIT(iPickupCollected[GET_LONG_BITSET_INDEX(iWep)], GET_LONG_BITSET_BIT(iWep))
					
					PRINTLN("[LM][PROCESS_WEAPON_RESPAWN_AND_COLLECTION] - Collected Pickup.")
					
					IF bIsLocalPlayerHost
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_LAND_GRAB_SOUNDS)
						OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_RESURRECTION_SOUNDS)
						OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_VEHICULAR_WARFARE_SOUNDS)
						OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_CONDEMNED_SOUNDS)
							//BROADCAST_FMMC_COLLECTED_WEAPON(iLocalPart, iWep) now done in PROCESS_PICKUP
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			// Recreate the pickup in Tiny Racers when respawning.
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
				IF TINY_RACERS_GET_ROUND_STATE(iPartToUse) != TR_Racing
					IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWep].iVehicleWeaponPickupType != -1
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_PICKUPS_CORONA_OPTION) AND g_FMMC_STRUCT.bAllowPickUpsCorona
						OR NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_PICKUPS_CORONA_OPTION)
							PRINTLN("[LM][WEAPON PROCESS_WEAPON_RESPAWN_AND_COLLECTION] FORCE_PICKUP_REGENERATE - being called on iWep: ", iWep)
							IF SHOULD_WEP_SPAWN_NOW(iWep)
								FORCE_PICKUP_REGENERATE(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWep].id)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC PROCESS_RULE_WEAPON_DISABLING()
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
	
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciMISSION_BLOCK_DROP_WEAPON_ON_WHEEL)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DROP_AMMO)	
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetFourteen[ iRule ], ciBS_RULE14_DISABLE_MELEE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)	
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)	
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)	
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)	
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)	
			
			WEAPON_TYPE weapCurrent
			IF GET_CURRENT_PED_WEAPON(LocalPlayerPed, weapCurrent)
				IF (weapCurrent = WEAPONTYPE_UNARMED)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
				ENDIF
			ENDIF
			IF NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_MELEE_PCF_IS_RULE_BLOCKED)
				PRINTLN("[JS] - PROCESS_RULE_WEAPON_DISABLING - Setting PCF_DisableMelee")
				SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DisableMelee, TRUE)			
				SET_BIT(iLocalBoolCheck33, LBOOL33_MELEE_PCF_IS_RULE_BLOCKED)
			ENDIF
		ELSE
			IF IS_BIT_SET(iLocalBoolCheck33, LBOOL33_MELEE_PCF_IS_RULE_BLOCKED)
				PRINTLN("[JS] - PROCESS_RULE_WEAPON_DISABLING - Clearing PCF_DisableMelee")
				SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DisableMelee, FALSE)
				CLEAR_BIT(iLocalBoolCheck33, LBOOL33_MELEE_PCF_IS_RULE_BLOCKED)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetFour[ iRule ], ciBS_RULE4_REMOVE_WEAPONS )
		OR (IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetFour[ iRule ], ciBS_RULE4_START_UNARMED ) AND IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam))
		OR IS_PARTICIPANT_A_BEAST(iLocalPart)
			WEAPON_TYPE currentWeaponType
			IF GET_CURRENT_PED_WEAPON(LocalPlayerPed, currentWeaponType)
				IF (currentWeaponType != WEAPONTYPE_UNARMED)
//					IF IS_BIT_SET(MC_playerBD[iLocalPart].iRagePickupBS, ciCUSTOM_PICKUP_TYPE__BEAST_MODE)
//						PRINTLN("[JS] - PROCESS_RULE_WEAPON_DISABLING - Setting weapon before beastmode was activated")
//						GET_CURRENT_PED_WEAPON(LocalPlayerPed, wtWeaponBeforeBeastmode)
//					ENDIF
					SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED, TRUE)
					PRINTLN("[JS] - PROCESS_RULE_WEAPON_DISABLING - Setting current weapon to unarmed")
				ENDIF
			ENDIF
			
			IF GET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, currentWeaponType)
				IF (currentWeaponType != WEAPONTYPE_UNARMED)
					SET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED)
					SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED, TRUE)
					PRINTLN("[JS] - PROCESS_RULE_WEAPON_DISABLING - Setting current vehicle weapon to unarmed")
				ENDIF
			ENDIF
			IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetFour[ iRule ], ciBS_RULE4_REMOVE_WEAPONS )
			OR IS_PARTICIPANT_A_BEAST(iLocalPart)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)	
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PLAY_RULE_OUTFIT_CHANGE_PTFX()
	IF NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_PLAYED_OUTFIT_CHANGE_PTFX)
	AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_SMOKE_ON_OUTFIT_CHANGE)
	AND NOT IS_PED_INJURED(LocalPlayerPed)
	AND NOT IS_PLAYER_SPECTATING(LocalPlayer)
	AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)
	AND NOT g_bMissionEnding
	AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		IF HAS_NAMED_PTFX_ASSET_LOADED("scr_sm_trans")
			USE_PARTICLE_FX_ASSET("scr_sm_trans")
			START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_sm_con_trans", LocalPlayerPed, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON
				USE_PARTICLE_FX_ASSET("scr_sm_trans")
				START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_sm_con_trans_fp", LocalPlayerPed, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			ENDIF
			SET_BIT(iLocalBoolCheck26, LBOOL26_PLAYED_OUTFIT_CHANGE_PTFX)
		ENDIF					
	ELSE
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_SMOKE_ON_OUTFIT_CHANGE)
			PRINTLN("[RCC MISSION] Outfits - Don't play PTFX. Spectating: ", IS_PLAYER_SPECTATING(LocalPlayer), " Spectating2: ", IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_ANY_SPECTATOR), " Injured: ", IS_PED_INJURED(LocalPlayerPed))
		ENDIF
	ENDIF
ENDPROC


PROC PROCESS_RULE_OUTFITS()
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iPartToUse].iteam)
		OR IS_BIT_SET(iLocalBoolCheck7, LBOOL7_OUTFIT_ON_RULE_PROCESSING)
		OR IS_BIT_SET(iLocalBoolCheck26, LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE)
			IF NOT IS_PLAYER_SPECTATING(LocalPlayer)
			AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)
				INT iRuleOutfit = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iOutfit[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]]
				IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iOutfitVariations[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] > 0
					iRuleOutfit += NATIVE_TO_INT(LocalPlayer) % g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iOutfitVariations[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]]
				ENDIF
				BOOL bOutfitSet = FALSE
				
				IF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE)
					IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] + 1 < FMMC_MAX_RULES
						IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iOutfit[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] + 1] != -1
							iRuleOutfit =  g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iOutfit[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] + 1]
							
							IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iOutfitVariations[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] + 1] > 0
								iRuleOutfit += NATIVE_TO_INT(LocalPlayer) % g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iOutfitVariations[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] + 1]
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF GET_PED_STEALTH_MOVEMENT(LocalPlayerPed)
					PRINTLN("[RCC MISSION][PROCESS_RULE_OUTFITS] was in stealth. setting LBOOL26_PUT_BACK_INTO_STEALTH_MODE")
					SET_BIT(iLocalBoolCheck26, LBOOL26_PUT_BACK_INTO_STEALTH_MODE_OUTFIT_CHANGE)
				ENDIF
				
				PRINTLN("[RCC MISSION] [PROCESS_RULE_OUTFITS] - iRuleOutfit: ", iRuleOutfit, " Default Outfit: ", GET_LOCAL_DEFAULT_OUTFIT(MC_playerBD[iPartToUse].iteam), " LBOOL7_OUTFIT_ON_RULE_PROCESSING: ", IS_BIT_SET(iLocalBoolCheck7, LBOOL7_OUTFIT_ON_RULE_PROCESSING), " LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE: ", IS_BIT_SET(iLocalBoolCheck26, LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE))
				
				//in case a rule gets processed immediately whilst clothes are still loading and the new rule has no clothes  
				IF iRuleOutfit = -1
				AND iRuleOutfitLoading != -1
					iRuleOutfit = iRuleOutfitLoading
					PRINTLN("[RCC MISSION] [PROCESS_RULE_OUTFITS] iRuleOutfit = iRuleOutfitLoading, iRuleOutfit: ", iRuleOutfit)
				ENDIF
				
				IF iRuleOutfit > -1
				AND iRuleOutfit != iLastOutfit
					//Need to equip a new outfit this rule				
					IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iPartToUse].iteam)
					AND (iRuleOutfit != iRuleOutfitLoading)
						CLEAR_BIT(iLocalBoolCheck7, LBOOL7_OUTFIT_ON_RULE_LOADED)
						SET_BIT(iLocalBoolCheck7, LBOOL7_OUTFIT_ON_RULE_PROCESSING)
						PRINTLN("[RCC MISSION] [PROCESS_RULE_OUTFITS] iRuleOutfit > -1 Set LBOOL7_OUTFIT_ON_RULE_PROCESSING as LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME and  iRuleOutfit != iRuleOutfitLoading, iRuleOutfitLoading: ", iRuleOutfitLoading, " iRuleOutfit = ", iRuleOutfit)
						iRuleOutfitLoading = iRuleOutfit
					ENDIF
					
					PLAY_RULE_OUTFIT_CHANGE_PTFX()
					
					//set local struct to use this outfit data to set   
					sApplyOutfitData.pedID 		= LocalPlayerPed
					sApplyOutfitData.eOutfit 	= int_to_enum(MP_OUTFIT_ENUM,iRuleOutfit)				
					IF SET_PED_MP_OUTFIT(sApplyOutfitData)
						CLEAR_BIT(iLocalBoolCheck7, LBOOL7_OUTFIT_ON_RULE_PROCESSING)
						iRuleOutfitLoading = -1
						iLastOutfit = iRuleOutfit
						MC_playerBD[iLocalPart].iOutfit = iRuleOutfit
						PRINTLN("[RCC MISSION] [PROCESS_RULE_OUTFITS] Set Outfit iRuleOutfit > -1 Outfits - MC_playerBD[",iLocalPart,"].iOutfit set as ",iRuleOutfit)
						bOutfitSet = TRUE
						CLEAR_BIT(iLocalBoolCheck26, LBOOL26_PLAYED_OUTFIT_CHANGE_PTFX)
						BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_PLAYER_CHANGED_APPEARANCE, ALL_PLAYERS())
						IF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE)
							CLEAR_BIT(iLocalBoolCheck26, LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE)
							PRINTLN("[RCC MISSION] [PROCESS_RULE_OUTFITS] iRuleOutfit > -1 CLEAR LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE")
						ENDIF
						IF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_PRELOAD_OUTFIT_CHANGE_AT_END_OF_CUTSCENE)
							CLEAR_BIT(iLocalBoolCheck28, LBOOL28_PRELOAD_OUTFIT_CHANGE_AT_END_OF_CUTSCENE)
						ENDIF
						START_NET_TIMER(tdChangeOutfitAppearance)
					ELSE
						PRINTLN("[RCC MISSION] [PROCESS_RULE_OUTFITS] - Still trying to set outfit  iRuleOutfit: ", iRuleOutfit , " LBOOL7_OUTFIT_ON_RULE_PROCESSING: ", IS_BIT_SET(iLocalBoolCheck7, LBOOL7_OUTFIT_ON_RULE_PROCESSING), " LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE: ", IS_BIT_SET(iLocalBoolCheck26, LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE), " LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME: ", IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iPartToUse].iteam))
					ENDIF				
					
				ELIF iRuleOutfit = -2
					//Need to equip a new outfit this rule				
					IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iPartToUse].iteam)
					AND (iRuleOutfit != iRuleOutfitLoading)
						CLEAR_BIT(iLocalBoolCheck7, LBOOL7_OUTFIT_ON_RULE_LOADED)
						SET_BIT(iLocalBoolCheck7, LBOOL7_OUTFIT_ON_RULE_PROCESSING)
						iRuleOutfitLoading = iRuleOutfit
						PRINTLN("[RCC MISSION] [PROCESS_RULE_OUTFITS] iRuleOutfit = -2 Set LBOOL7_OUTFIT_ON_RULE_PROCESSING as LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME and  iRuleOutfit != iRuleOutfitLoading, iRuleOutfitLoading: ", iRuleOutfitLoading)
					ENDIF
					
					PLAY_RULE_OUTFIT_CHANGE_PTFX()
									
					//set local struct to use this outfit data to set   
					sApplyOutfitData.pedID 		= LocalPlayerPed
					sApplyOutfitData.eOutfit 	= INT_TO_ENUM(MP_OUTFIT_ENUM, GET_LOCAL_DEFAULT_OUTFIT(MC_playerBD[iPartToUse].iteam))
					sApplyOutfitData.eMask   	= INT_TO_ENUM(MP_OUTFIT_MASK_ENUM, GET_LOCAL_DEFAULT_MASK(MC_playerBD[iPartToUse].iteam))
					
					IF SET_PED_MP_OUTFIT(sApplyOutfitData)
						CLEAR_BIT(iLocalBoolCheck7, LBOOL7_OUTFIT_ON_RULE_PROCESSING)
						iRuleOutfitLoading = -1
						iLastOutfit = iRuleOutfit
						MC_playerBD[iLocalPart].iOutfit = iRuleOutfit
						PRINTLN("[RCC MISSION] [PROCESS_RULE_OUTFITS] Set outfit iRuleOutfit = -2 Outfits - MC_playerBD[",iLocalPart,"].iOutfit set as ",iRuleOutfit)
						bOutfitSet = TRUE
						CLEAR_BIT(iLocalBoolCheck26, LBOOL26_PLAYED_OUTFIT_CHANGE_PTFX)
						
						START_NET_TIMER(tdChangeOutfitAppearance)
						BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_PLAYER_CHANGED_APPEARANCE, ALL_PLAYERS())
						IF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE)
							CLEAR_BIT(iLocalBoolCheck26, LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE)
							PRINTLN("[RCC MISSION] [PROCESS_RULE_OUTFITS] iRuleOutfit = -2 CLEAR LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE")
						ENDIF
						IF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_PRELOAD_OUTFIT_CHANGE_AT_END_OF_CUTSCENE)
							CLEAR_BIT(iLocalBoolCheck28, LBOOL28_PRELOAD_OUTFIT_CHANGE_AT_END_OF_CUTSCENE)
						ENDIF
					ELSE
						PRINTLN("[RCC MISSION] [PROCESS_RULE_OUTFITS] - Still trying to set outfit iRuleOutfit: ", iRuleOutfit , " LBOOL7_OUTFIT_ON_RULE_PROCESSING: ", IS_BIT_SET(iLocalBoolCheck7, LBOOL7_OUTFIT_ON_RULE_PROCESSING), " LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE: ", IS_BIT_SET(iLocalBoolCheck26, LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE), " LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME: ", IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iPartToUse].iteam))
					ENDIF	
				ELSE
					IF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE)
						CLEAR_BIT(iLocalBoolCheck26, LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE)
						PRINTLN("[RCC MISSION] [PROCESS_RULE_OUTFITS] iRuleOutfit: ", iRuleOutfit, " No outfit change required CLEAR LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE")
					ENDIF
					IF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_PRELOAD_OUTFIT_CHANGE_AT_END_OF_CUTSCENE)
						CLEAR_BIT(iLocalBoolCheck28, LBOOL28_PRELOAD_OUTFIT_CHANGE_AT_END_OF_CUTSCENE)
					ENDIF
				ENDIF
			
				IF bOutfitSet
				AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_SMOKE_ON_OUTFIT_CHANGE)
				AND NOT IS_PED_INJURED(LocalPlayerPed)
				AND NOT g_bMissionEnding
				AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
					IF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_PUT_BACK_INTO_STEALTH_MODE_OUTFIT_CHANGE)
						SET_PED_STEALTH_MOVEMENT(LocalPlayerPed, TRUE)
						CLEAR_BIT(iLocalBoolCheck26, LBOOL26_PUT_BACK_INTO_STEALTH_MODE_OUTFIT_CHANGE)
						PRINTLN("[LM][LOADOUT] [PROCESS_RULE_OUTFITS] Putting back into stealth.")
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_SLASHERS_AUDIO)
					AND g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iShardText[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam]] = 28 // SHD_TOTH - "Take Out The Hunted!"
						PRINTLN("[LM][AUDIO] [PROCESS_RULE_OUTFITS] Outfit Changed. ciENABLE_SLASHERS_AUDIO - Playing 'Become_Slasher'")
						PLAY_SOUND_FRONTEND(-1, "Become_Slasher", "dlc_xm_sls_Sounds")
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_SLASHERS_AUDIO)
					AND g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iShardText[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam]] = 1 // SHD_TOTH - "Take Out The Hunted!"
						PRINTLN("[LM][AUDIO] [PROCESS_RULE_OUTFITS] Outfit Changed. ciENABLE_SLASHERS_AUDIO - Playing 'Become_Hunted'")
						PLAY_SOUND_FRONTEND(-1, "Become_Hunted", "dlc_xm_sls_Sounds")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF (IS_PLAYER_SPECTATING(LocalPlayer) OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_ANY_SPECTATOR))
			IF (IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iPartToUse].iteam) OR HAS_NET_TIMER_STARTED(tdChangeOutfitAppearance))
			AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_SLASHERS_AUDIO)
				
				IF NOT HAS_NET_TIMER_STARTED(tdChangeOutfitAppearance)
					START_NET_TIMER(tdChangeOutfitAppearance)
				ENDIF
				
				IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdChangeOutfitAppearance, 500)
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_SMOKE_ON_OUTFIT_CHANGE)
					AND NOT g_bMissionEnding
					AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
						IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iShardText[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] = 28 // SHD_TOTH - "Take Out The Hunted!"
							PRINTLN("[LM][AUDIO][SPEC] Outfit Changed. ciENABLE_SLASHERS_AUDIO - Playing 'Become_Slasher'")
							PLAY_SOUND_FRONTEND(-1, "Become_Slasher", "dlc_xm_sls_Sounds")
						ENDIF
						IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iShardText[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] = 1 // SHD_TOTH - "Take Out The Hunted!"
							PRINTLN("[LM][AUDIO][SPEC] Outfit Changed. ciENABLE_SLASHERS_AUDIO - Playing 'Become_Hunted'")
							PLAY_SOUND_FRONTEND(-1, "Become_Hunted", "dlc_xm_sls_Sounds")					
						ENDIF
						RESET_NET_TIMER(tdChangeOutfitAppearance)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_SLIPSTREAM(Bool bfinalCleanup = FALSE)

	IF IS_BIT_SET(iLocalBoolCheck7,LBOOL7_SLIPSTREAM_SOUND_ACTIVE)
		
		IF IS_SOUND_ID_VALID(iSlipStreamSoundId)
		AND NOT HAS_SOUND_FINISHED(iSlipStreamSoundId)
			PRINTLN("[CLEANUP_SLIPSTREAM] STOP_SOUND(iSlipStreamSoundId)")
			STOP_SOUND(iSlipStreamSoundId)
		ENDIF
		
		SCRIPT_RELEASE_SOUND_ID(iSlipStreamSoundId)
		
		IF IS_AUDIO_SCENE_ACTIVE("DLC_Biker_SL_Slipstream_Follower_Scene")
			STOP_AUDIO_SCENE("DLC_Biker_SL_Slipstream_Follower_Scene")
		ENDIF
		IF IS_AUDIO_SCENE_ACTIVE("RACES_SLIPSTREAM_SCENE")
			STOP_AUDIO_SCENE("RACES_SLIPSTREAM_SCENE")
		ENDIF
    	CLEAR_BIT(iLocalBoolCheck7,LBOOL7_SLIPSTREAM_SOUND_ACTIVE)
		PRINTLN("[CLEANUP_SLIPSTREAM] cleaning up")
	ENDIF
	IF bfinalCleanup
		IF IS_BIT_SET(iLocalBoolCheck7,LBOOL7_SLIPSTREAM_ACTIVE)
			SET_ENABLE_VEHICLE_SLIPSTREAMING(FALSE)
			CLEAR_BIT(iLocalBoolCheck7,LBOOL7_SLIPSTREAM_ACTIVE)
			PRINTLN("[CLEANUP_SLIPSTREAM] cleaning up [FINAL]")
		ENDIF
	ENDIF

ENDPROC

FUNC FLOAT GET_SLIPSTREAM_INTENSITY()

	FLOAT fSlipStreamAmount
	
	fSlipStreamAmount = GET_VEHICLE_CURRENT_TIME_IN_SLIP_STREAM(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
	
	fSlipStreamAmount = fSlipStreamAmount / 3
	
	RETURN fSlipStreamAmount 
	
ENDFUNC

PROC PROCESS_VEHICLE_SLIPSTREAMING()
	
	
	FLOAT fSlipStreamIntensity
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam] < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetTwo[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] ] , ciBS_RULE2_SLIPSTREAM )
		AND NOT g_bMissionEnding
		AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		AND GET_MC_CLIENT_GAME_STATE(iLocalPart) = GAME_STATE_RUNNING
			IF NOT IS_BIT_SET(iLocalBoolCheck7,LBOOL7_SLIPSTREAM_ACTIVE)
				SET_ENABLE_VEHICLE_SLIPSTREAMING(TRUE)
				SET_BIT(iLocalBoolCheck7,LBOOL7_SLIPSTREAM_ACTIVE)
			ENDIF
	        IF bLocalPlayerOK
	            IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciENABLE_NEW_SLIPSTREAM_FX)
							PRINTLN("[PROCESS_VEHICLE_SLIPSTREAMING] [RACE_AUDIO][SS] PLAY_SOUND_FRONTEND, Slipstream_Leader ")
							
							IF IS_VEHICLE_PRODUCING_SLIP_STREAM(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
								IF NOT IS_BIT_SET(iLocalBoolCheck19,LBOOL19_PLAY_LEADER_SLIPSTREAM_AUDIO)
									IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_Biker_SL_Slipstream_Leader_Scene")
										START_AUDIO_SCENE("DLC_Biker_SL_Slipstream_Leader_Scene")
									ENDIF
									PRINTLN("[PROCESS_VEHICLE_SLIPSTREAMING] [RACE_AUDIO][SS] PLAY_SOUND_FRONTEND, Slipstream_Leader ")
									IF iSlipStreamLeaderSoundId = -1
										iSlipStreamLeaderSoundId = GET_SOUND_ID()
									ENDIF
									PLAY_SOUND_FRONTEND(iSlipStreamLeaderSoundId,  "Slipstream_Leader", "DLC_Biker_SL_Sounds",FALSE)
									SET_BIT(iLocalBoolCheck19,LBOOL19_PLAY_LEADER_SLIPSTREAM_AUDIO)
								ENDIF
							ELSE
								IF IS_BIT_SET(iLocalBoolCheck19,LBOOL19_PLAY_LEADER_SLIPSTREAM_AUDIO)
									IF IS_AUDIO_SCENE_ACTIVE("DLC_Biker_SL_Slipstream_Leader_Scene")
										STOP_AUDIO_SCENE("DLC_Biker_SL_Slipstream_Leader_Scene")
									ENDIF
									PRINTLN("[PROCESS_VEHICLE_SLIPSTREAMING] [RACE_AUDIO][SS] STOPPING SOUND, Slipstream_Leader ")
									STOP_SOUND(iSlipStreamLeaderSoundId)
									CLEAR_BIT(iLocalBoolCheck19,LBOOL19_PLAY_LEADER_SLIPSTREAM_AUDIO)
								ENDIF
							ENDIF
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciENABLE_NEW_SLIPSTREAM_FX)
							PRINTLN("[PROCESS_VEHICLE_SLIPSTREAMING] [RACE_AUDIO][SS TIME] Current time in slipstream - ", GET_VEHICLE_CURRENT_TIME_IN_SLIP_STREAM(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)))
							
							IF GET_VEHICLE_CURRENT_TIME_IN_SLIP_STREAM(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)) > 0.0
		                    AND GET_ENTITY_SPEED(LocalPlayerPed) > 0
								
		                        IF NOT IS_BIT_SET(iLocalBoolCheck7,LBOOL7_SLIPSTREAM_SOUND_ACTIVE)
		                           	IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_Biker_SL_Slipstream_Follower_Scene")
										START_AUDIO_SCENE("DLC_Biker_SL_Slipstream_Follower_Scene")
									ENDIF
									IF iSlipStreamSoundId = -1
										iSlipStreamSoundId = GET_SOUND_ID()
									ENDIF
		                            PLAY_SOUND_FRONTEND(iSlipStreamSoundId,"Slipstream_Follower", "DLC_Biker_SL_Sounds", FALSE)         
		                            SET_BIT(iLocalBoolCheck7,LBOOL7_SLIPSTREAM_SOUND_ACTIVE)
		                        ELSE
									fSlipStreamIntensity = GET_SLIPSTREAM_INTENSITY()
									PRINTLN("[PROCESS_VEHICLE_SLIPSTREAMING] [RACE_AUDIO][SS TIME]fSlipStreamIntensity -",fSlipStreamIntensity) 
									SET_VARIABLE_ON_SOUND(iSlipStreamSoundId, "Intensity",fSlipStreamIntensity)
								ENDIF
		                    ELSE
								CLEANUP_SLIPSTREAM()
		                    ENDIF
						ELSE
		                    IF GET_VEHICLE_CURRENT_TIME_IN_SLIP_STREAM(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)) > 0.0
		                    AND GET_ENTITY_SPEED(LocalPlayerPed) > 0
		                        IF NOT IS_BIT_SET(iLocalBoolCheck7,LBOOL7_SLIPSTREAM_SOUND_ACTIVE)
									PRINTLN("[PROCESS_VEHICLE_SLIPSTREAMING] [RACE_AUDIO] Playing SlipStream Audio Scene.")
		                            START_AUDIO_SCENE("RACES_SLIPSTREAM_SCENE")
									IF iSlipStreamSoundId = -1
										iSlipStreamSoundId = GET_SOUND_ID()
									ENDIF
		                            PLAY_SOUND_FRONTEND(iSlipStreamSoundId,"SLIPSTREAM_MASTER", DEFAULT, FALSE)         
		                            SET_BIT(iLocalBoolCheck7,LBOOL7_SLIPSTREAM_SOUND_ACTIVE)
		                        ENDIF
		                    ELSE
								CLEANUP_SLIPSTREAM()
		                    ENDIF
						ENDIF
					ELSE
						CLEANUP_SLIPSTREAM()
					ENDIF
	            ENDIF
	        ENDIF
	    ELSE
			CLEANUP_SLIPSTREAM(TRUE)
	    ENDIF
	ELSE
		CLEANUP_SLIPSTREAM(TRUE)
	ENDIF
ENDPROC


FUNC BOOL CAN_ATTACH_FLARE_TO_PLAYER()
	
	INT iTeam = MC_playerBD[iLocalPart].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFourteen[iRule], ciBS_RULE14_ATTACH_FLARE_TO_BACK)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_CUTSCENE_PLAYING()
		PRINTLN("CAN_ATTACH_FLARE_TO_PLAYER  IS_CUTSCENE_PLAYING FALSE")
		RETURN FALSE
	ENDIF

	IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
		IF IS_ENTITY_IN_WATER( LocalPlayerPed ) 
		AND IS_MP_HEIST_GEAR_EQUIPPED( LocalPlayerPed, HEIST_GEAR_REBREATHER )
			PRINTLN("CAN_ATTACH_FLARE_TO_PLAYER  TRUE 1")
			RETURN TRUE
		ENDIF

		IF IS_PED_SWIMMING( LocalPlayerPed ) 
		AND IS_MP_HEIST_GEAR_EQUIPPED( LocalPlayerPed, HEIST_GEAR_REBREATHER )
			PRINTLN("CAN_ATTACH_FLARE_TO_PLAYER  TRUE 2")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE


ENDFUNC

PROC MANAGE_UNDERWATER_FLARE()
	
	STRING strHeistFX = "scr_biolab_heist"

	IF CAN_ATTACH_FLARE_TO_PLAYER()
		REQUEST_NAMED_PTFX_ASSET(strHeistFX)
	
		IF HAS_NAMED_PTFX_ASSET_LOADED(strHeistFX)
			// Create a flare object on the body
			IF NOT DOES_ENTITY_EXIST(objFlare)
				IF NOT IS_BIT_SET(iLocalBoolCheck12, LBOOL12_OBJ_RES_UNDERWATER_FLARE)
					MC_RESERVE_NETWORK_MISSION_OBJECTS(1)
					SET_BIT(iLocalBoolCheck12, LBOOL12_OBJ_RES_UNDERWATER_FLARE)
				ENDIF
				
				IF IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_UNDERWATER_FLARE)
					IF CAN_REGISTER_MISSION_OBJECTS(1)
						objFlare = create_object( PROP_FLARE_01B, get_offset_from_entity_in_world_coords( LocalPlayerPed, <<0.0, 0.0, 1.0>> ), true, bIsLocalPlayerHost )
						SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(OBJ_TO_NET(objFlare), TRUE)
						attach_entity_to_entity( objFlare, LocalPlayerPed, get_ped_bone_index( LocalPlayerPed, BONETAG_SPINE3 ), <<-0.060, -0.120, -0.165>>, <<0.0, 0.00, 345.00>>)
						SET_BIT( iLocalBoolCheck10, LBOOL10_HUMANE_FLARE_CREATED )
						CLEAR_BIT( iLocalBoolCheck12,LBOOL12_OBJ_RES_UNDERWATER_FLARE)
						
						IF DOES_PARTICLE_FX_LOOPED_EXIST(players_underwater_flare)
							PRINTLN("MANAGE_UNDERWATER_FLARE | Removing existing invalid flare PTFX so that we can recreate it on the fresh flare object")
							REMOVE_PARTICLE_FX(players_underwater_flare, FALSE)
							RESET_NET_TIMER(stFlarePTFXDelayTimer)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_JOB_IN_CASINO()
				IF HAS_NET_TIMER_STARTED(stFlarePTFXDelayTimer)
					IF NOT HAS_NET_TIMER_EXPIRED(stFlarePTFXDelayTimer, ciFlarePTFXDelayTimerLength)
						PRINTLN("MANAGE_UNDERWATER_FLARE | Waiting for stFlarePTFXDelayTimer")
						EXIT
					ENDIF
				ELSE
					PRINTLN("MANAGE_UNDERWATER_FLARE | Starting stFlarePTFXDelayTimer")
					REINIT_NET_TIMER(stFlarePTFXDelayTimer)
					EXIT
				ENDIF
			ENDIF
			
			// Create PTFX for that
			IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(players_underwater_flare)
			AND DOES_ENTITY_EXIST(objFlare)
			AND DOES_ENTITY_HAVE_DRAWABLE(objFlare)
				PRINTLN("[RCC MISSION] USE_PARTICLE_FX_ASSET - 1 ")
				USE_PARTICLE_FX_ASSET(strHeistFX)
				
				PRINTLN("MANAGE_UNDERWATER_FLARE | Starting Flare FX now!")
				players_underwater_flare =  START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY( "scr_heist_biolab_flare_underwater", objFlare, <<0.11, 0.00, 0.0>>, <<0.0, 0.0, 0.0>> ) 
			ENDIF
		ENDIF
		
	ELSE
		IF DOES_ENTITY_EXIST( objFlare ) 
			STOP_PARTICLE_FX_LOOPED( players_underwater_flare )
			
			IF NOT IS_JOB_IN_CASINO()
				REMOVE_NAMED_PTFX_ASSET( strHeistFX )
			ENDIF
			
			DETACH_ENTITY( objFlare )
			DELETE_OBJECT( objFlare )
			SET_OBJECT_AS_NO_LONGER_NEEDED( objFlare )
			REMOVE_MP_HEIST_GEAR( LocalPlayerPed, HEIST_GEAR_REBREATHER )
			PRINTLN("MANAGE_UNDERWATER_FLARE REMOVE objFlare ")
		ELSE
			IF( IS_BIT_SET( iLocalBoolCheck10, LBOOL10_HUMANE_FLARE_CREATED ) )
				IF(	HAVE_ALL_STREAMING_REQUESTS_COMPLETED( LocalPlayerPed )									
				AND HAS_PED_HEAD_BLEND_FINISHED( LocalPlayerPed ) )
					FINALIZE_HEAD_BLEND( LocalPlayerPed )
					SET_BIT( iLocalBoolCheck10, LBOOL10_HUMANE_FLARE_PROCESSING_DONE )
					PRINTLN("MANAGE_UNDERWATER_FLARE REMOVE objFlare 2 ")
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC


PROC MAINTAIN_STAMINA_BAR()
	INT iTeam 
	INT iPriority 
	
	IF iPartToUse > -1
	AND iPartToUse < NUM_NETWORK_PLAYERS
		iTeam = MC_playerBD[iPartToUse].iteam
		iPriority = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
	ENDIF

	IF iTeam > -1
	AND iPriority > -1 AND iPriority < FMMC_MAX_RULES
	AND NOT g_bMissionEnding
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iPriority], ciBS_RULE2_SHOW_STAMINA_BAR)
			DRAW_STAMINA_BAR_FOR_PED(LocalPlayerPed, DEFAULT)
		ENDIF
	ENDIF	
ENDPROC

FUNC BOOL IS_LOCAL_PLAYER_ON_PACIFIC_FINALE()

	IF g_FMMC_STRUCT.iRootContentIDHash = g_sMPTUNABLES.iroot_id_HASH_The_Pacific_Standard_Job 
	OR g_FMMC_STRUCT.iRootContentIDHash = g_sMPTUNABLES.iroot_id_HASH_The_Pacific_Standard_Job2 
	OR g_FMMC_STRUCT.iRootContentIDHash = g_sMPTUNABLES.iroot_id_HASH_The_Pacific_Standard_Job2a
		
		RETURN TRUE
		
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC BOOL SHOULD_REMOVE_MASK_FOR_PACIFIC_BIKES()

	IF GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD) = 0  //None
	OR GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD) = 35 //Balaclava1
	OR GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD) = 37 //Balaclava2
	OR GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD) = 52 //Balaclava3
	OR GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD) = 57 //Balaclava4
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL IS_PLAYER_WEARING_BLACK_FULL_FACE_HELMET()

	IF GET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD) = 50
	OR GET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD) = 51
	OR GET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD) = 52
	OR GET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD) = 53
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
	
	
ENDFUNC

PROC MAINTAIN_PLAYER_HELMET_AND_PARACHUTE_FOR_PACIFIC_BIKES()

	IF ( IS_LOCAL_PLAYER_ON_PACIFIC_FINALE() AND MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] > 4 )
	OR (( g_FMMC_STRUCT.iRootContentIDHash = g_sMPTUNABLES.iroot_id_HASH_Pacific_Standard_Bike )
	AND MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] > 0 )
	
		IF bLocalPlayerPedOk
		
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			AND GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)) = LECTRO 
				MP_OUTFIT_ENUM eCurrentOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, GET_LOCAL_DEFAULT_OUTFIT(MC_playerBD[iPartToUse].iteam))
				IF eCurrentOutfit <> OUTFIT_MP_FREEMODE
					IF NOT IS_BIT_SET(iLocalBoolCheck9, LBOOL9_MASK_REPLACED_WITH_HELMET)
						IF NOT IS_PLAYER_WEARING_BLACK_FULL_FACE_HELMET()
							IF SHOULD_REMOVE_MASK_FOR_PACIFIC_BIKES()
								SET_BIT(iLocalBoolCheck10, LBOOL10_PLAYER_WEARING_MASK)
								REMOVE_MASK(LocalPlayerPed, eHairToRestore)
							ELSE
								CLEAR_BIT(iLocalBoolCheck10, LBOOL10_PLAYER_WEARING_MASK)
							ENDIF
							SET_HOODED_JACKET_STATE(LocalPlayerPed, JACKET_HOOD_DOWN)
							REMOVE_PED_HELMET(LocalPlayerPed, TRUE)
							SET_PED_PROP_INDEX(LocalPlayerPed, ANCHOR_HEAD, 50, 0)
							FINALIZE_HEAD_BLEND(LocalPlayerPed)
							BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_PLAYER_CHANGED_APPEARANCE, ALL_PLAYERS())
							CPRINTLN(DEBUG_MISSION, "Setting Bit LBOOL9_MASK_REPLACED_WITH_HELMET")
							SET_BIT(iLocalBoolCheck9, LBOOL9_MASK_REPLACED_WITH_HELMET)
						ELSE
							CPRINTLN(DEBUG_MISSION, "Setting Bit LBOOL9_MASK_REPLACED_WITH_HELMET - Player is already wearing a helmet.")
							SET_BIT(iLocalBoolCheck10, LBOOL10_PLAYER_WEARING_MASK)
							SET_BIT(iLocalBoolCheck9, LBOOL9_MASK_REPLACED_WITH_HELMET)
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_LOCAL_PLAYER_ON_PACIFIC_FINALE()
					IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_PAC_FIN_PARACHUTE_GIVEN)
						IF MC_playerBD[iPartToUse].iteam <> 0 AND MC_playerBD[iPartToUse].iteam <> 1
							SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_HAND, 32, 0)
						ENDIF
						GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE, 1, FALSE, FALSE)
						SET_BIT(iLocalBoolCheck10, LBOOL10_PAC_FIN_PARACHUTE_GIVEN)
						CPRINTLN(DEBUG_MISSION, "Setting Bit LBOOL10_PAC_FIN_PARACHUTE_GIVEN")
					ENDIF
				ENDIF
				
			ELSE
			
				IF IS_LOCAL_PLAYER_ON_PACIFIC_FINALE()
					IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_PAC_FIN_PARACHUTE_GIVEN)
						IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] > 10
							IF MC_playerBD[iPartToUse].iteam <> 0 AND MC_playerBD[iPartToUse].iteam <> 1
								SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_HAND, 0, 0)
							ENDIF
						ENDIF
						CLEAR_BIT(iLocalBoolCheck10, LBOOL10_PAC_FIN_PARACHUTE_GIVEN)
					ENDIF
					IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] <= 10
						IF MC_playerBD[iPartToUse].iteam <> 0 AND MC_playerBD[iPartToUse].iteam <> 1
							SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisableTakeOffParachutePack, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(iLocalBoolCheck9, LBOOL9_MASK_REPLACED_WITH_HELMET)
				OR IS_PLAYER_WEARING_BLACK_FULL_FACE_HELMET()
					IF ( IS_LOCAL_PLAYER_ON_PACIFIC_FINALE() AND MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] > 10 )
						REMOVE_PED_HELMET(LocalPlayerPed, TRUE)
						CLEAR_PED_STORED_HAT_PROP(PLAYER_PED_ID())
						CLEAR_PED_PROP(LocalPlayerPed, ANCHOR_HEAD)
						IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_PLAYER_WEARING_MASK)
						OR IS_LOCAL_PLAYER_ON_PACIFIC_FINALE()
							MP_OUTFITS_APPLY_DATA   sApplyData
							sApplyData.eApplyStage 	= AOS_SET
						    sApplyData.pedID        = PLAYER_PED_ID()
						    sApplyData.eMask        = INT_TO_ENUM(MP_OUTFIT_MASK_ENUM, GET_LOCAL_DEFAULT_MASK(MC_playerBD[iPartToUse].iteam))
							SET_PED_MP_OUTFIT(sApplyData, TRUE, TRUE, FALSE, TRUE)
							CLEAR_BIT(iLocalBoolCheck10, LBOOL10_PLAYER_WEARING_MASK)
						ENDIF
						FINALIZE_HEAD_BLEND(LocalPlayerPed)
						BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_PLAYER_CHANGED_APPEARANCE, ALL_PLAYERS())
						CPRINTLN(DEBUG_MISSION, "Clearing Bit LBOOL9_MASK_REPLACED_WITH_HELMET")
						CLEAR_BIT(iLocalBoolCheck9, LBOOL9_MASK_REPLACED_WITH_HELMET)
					ENDIF
				ENDIF
			ENDIF
		ELSE //bLocalPlayerPedOk = FALSE
			IF IS_BIT_SET(iLocalBoolCheck9, LBOOL9_MASK_REPLACED_WITH_HELMET)
				CLEAR_BIT(iLocalBoolCheck9, LBOOL9_MASK_REPLACED_WITH_HELMET)
			ENDIF
			IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_PAC_FIN_PARACHUTE_GIVEN)
				CLEAR_BIT(iLocalBoolCheck10, LBOOL10_PAC_FIN_PARACHUTE_GIVEN)
			ENDIF
		ENDIF
	ENDIF //Not of Pacific Finale or Pacific Bikes
	
ENDPROC


//For transitioning corona colours states set in flare prop, transitions in objectivecompeltion.sch

FUNC BOOL IS_SUDDEN_DEATH_ACTIVE()
	IF IS_BIT_SET( MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH )
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_CORONA_NEW_RGB(INT iFlare, FLOAT fR, FLOAT fG, FLOAT fB )
	corona_flare_data[iFlare].fNextRed = fR
	corona_flare_data[iFlare].fNextGreen = fG
	corona_flare_data[iFlare].fNextBlue = fB
	PRINTLN("[RCC MISSION][AW SMOKE]SET_CORONA_NEW_RGB | fR: ",fR," fG:  ",fG," fB: " , fB )
ENDPROC

PROC SET_FLARE_NEW_RGB(INT iFlare, FLOAT fR, FLOAT fG, FLOAT fB, BOOL bAndCorona = TRUE  )
	flare_data[iFlare].fNextRed = fR
	flare_data[iFlare].fNextGreen = fG
	flare_data[iFlare].fNextBlue = fB
	PRINTLN("[RCC MISSION][AW SMOKE]SET_FLARE_NEW_RGB | fR: ",fR," fG:  ",fG," fB: " , fB )
	
	IF bAndCorona
		SET_CORONA_NEW_RGB(iFlare,fR,fG,fB)
	ENDIF
	
ENDPROC

PROC SET_CORONA_TO_TRANSITION(INT iFlare, FLARE_STATE fsNew )
	corona_flare_data[iFlare].fTransitionProgress = 0
	corona_flare_data[iFlare].iNewState = fsNew
	PRINTLN("[RCC MISSION][AW SMOKE]SET_CORONA_TO_TRANSITION | flare_data[iFlare].iNewState: ", corona_flare_data[iFlare].iNewState, "flare_data[iFlare].iFlareState (Current):", flare_data[iFlare].iFlareState  )
ENDPROC

PROC SET_FLARE_TO_TRANSITION(INT iFlare, FLARE_STATE fsNew,BOOL bAndCorona = TRUE )
	flare_data[iFlare].fTransitionProgress = 0
	flare_data[iFlare].iNewState = fsNew
	
	IF fsNew = FLARE_STATE_NEUTRAL
		flare_data[iFlare].iWinningTeam = -1
	ELIF  fsNew = FLARE_STATE_TEAM_0
		flare_data[iFlare].iWinningTeam = 0
	ELIF fsNew = FLARE_STATE_TEAM_1
		flare_data[iFlare].iWinningTeam = 1
	ELIF fsNew = FLARE_STATE_TEAM_2
		flare_data[iFlare].iWinningTeam = 2
	ELIF fsNew = FLARE_STATE_TEAM_3
		flare_data[iFlare].iWinningTeam = 3
	ENDIF
	
	PRINTLN("[RCC MISSION] [AW SMOKE]SET_FLARE_TO_TRANSITION | flare_data[iFlare].iNewState: ", flare_data[iFlare].iNewState, "flare_data[iFlare].iFlareState (Current):", flare_data[iFlare].iFlareState  )
	
	IF bAndCorona
		SET_CORONA_TO_TRANSITION(iFlare, fsNew)
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(INT iFlare, FLARE_STATE fsNewState)

	IF flare_data[iFlare].iNewState  != fsNewState
	AND flare_data[iFlare].iFlareState != fsNewState
		PRINTLN("[RCC MISSION] [AW SMOKE] SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE | flare_data[iFlare].iNewState: ", flare_data[iFlare].iNewState, "flare_data[iFlare].iFlareState (Current):", flare_data[iFlare].iFlareState  )
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF

ENDFUNC

FUNC BOOL SHOULD_CORONA_SET_TO_TRANSITION_INTO_NEW_STATE(INT iFlare, FLARE_STATE fsNewState)

	IF corona_flare_data[iFlare].iNewState != fsNewState
	AND corona_flare_data[iFlare].iFlareState != fsNewState
		PRINTLN("[RCC MISSION] [AW SMOKE] SHOULD_CORONA_SET_TO_TRANSITION_INTO_NEW_STATE | flare_data[iFlare].iNewState: ", flare_data[iFlare].iNewState, "flare_data[iFlare].iFlareState (Current):", flare_data[iFlare].iFlareState  )
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF

ENDFUNC

FUNC BOOL HAS_FLARE_REACHED_NEW_STATE(INT iFlare)

	IF flare_data[iFlare].fTransitionProgress >= 1.0
		PRINTLN("AW SMOKE - HAS_FLARE_REACHED_NEW_STATE - TRUE")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC PROCESS_EVERY_FRAME_FLARE_OBJECT()	
	
	INT i
	
	INT iPTFX
	
	IF iNumCreatedFlarePTFX >= 0
		IF IS_SUDDEN_DEATH_ACTIVE()
			FOR iPTFX = 0 TO iNumCreatedFlarePTFX -1
				IF DOES_PARTICLE_FX_LOOPED_EXIST( ptfxFlareIndex[ iPTFX ] )
					PRINTLN("[RCC MISSION][AW SMOKE] PROCESS_EVERY_FRAME_FLARE_OBJECT() - Clearing up smoke")
					STOP_PARTICLE_FX_LOOPED(ptfxFlareIndex[ iPTFX ])
					
					IF FlareSoundID[iPTFX] != -1
						PRINTLN("[RCC MISSION][AW SMOKE] PROCESS_EVERY_FRAME_FLARE_OBJECT() - Clearing up Sound ID: ", FlareSoundID[iPTFX])
						IF NOT HAS_SOUND_FINISHED(FlareSoundID[iPTFX])
							STOP_SOUND(FlareSoundID[iPTFX])
						ENDIF
						SCRIPT_RELEASE_SOUND_ID(FlareSoundID[iPTFX])
					ENDIF
				ENDIF
			ENDFOR
			
			FOR i = 0 TO FMMC_MAX_GO_TO_LOCATIONS -1
				IF DOES_BLIP_EXIST(LocBlip[i])
					REMOVE_BLIP(LocBlip[i])
				ENDIF
				IF DOES_BLIP_EXIST(LocBlip1[i])
					REMOVE_BLIP(LocBlip1[i])
				ENDIF
			ENDFOR
			
			
		ENDIF
	ENDIF
	
	FOR i = 0 TO iNumCreatedDynamicFlarePTFX -1
				
		IF flare_data[i].iPTFXval < ciTOTAL_PTFX
		AND flare_data[i].iPTFXval >= 0
			//if the current state isn't equal to the new state that has been set
			IF flare_data[i].iFlareState !=  flare_data[i].iNewState
				
				IF HAS_FLARE_REACHED_NEW_STATE(i)
					flare_data[i].iFlareState =  flare_data[i].iNewState
				ELSE
					IF flare_data[i].fRed != flare_data[i].fNextRed
						flare_data[i].fRed   = LERP_FLOAT(flare_data[i].fRed,flare_data[i].fNextRed,flare_data[i].fTransitionProgress)
					ENDIF
					IF flare_data[i].fGreen != flare_data[i].fNextGreen
						flare_data[i].fGreen = LERP_FLOAT(flare_data[i].fGreen,flare_data[i].fNextGreen,flare_data[i].fTransitionProgress)
					ENDIF
					IF flare_data[i].fBlue != flare_data[i].fNextBlue
						flare_data[i].fBlue  = LERP_FLOAT(flare_data[i].fBlue,flare_data[i].fNextBlue,flare_data[i].fTransitionProgress)
					ENDIF
					PRINTLN("AW SMOKE - PROCESS_EVERY_FRAME_FLARE_OBJECT - fTransitionProgress:", flare_data[i].fTransitionProgress)
					flare_data[i].fTransitionProgress = flare_data[i].fTransitionProgress + 0.01
				ENDIF
				
				IF DOES_PARTICLE_FX_LOOPED_EXIST( ptfxFlareIndex[flare_data[i].iPTFXval])
					PRINTLN("AW SMOKE -PROCESS_EVERY_FRAME_FLARE_OBJECT - fRed:", flare_data[i].fRed, " fGreen:", flare_data[i].fGreen," fBlue:", flare_data[i].fBlue)
					PRINTLN("AW SMOKE -PROCESS_EVERY_FRAME_FLARE_OBJECT - flare_data[",i,"].iPTFXval = ", flare_data[i].iPTFXval)
					SET_PARTICLE_FX_LOOPED_COLOUR(ptfxFlareIndex[flare_data[i].iPTFXval],flare_data[i].fRed, flare_data[i].fGreen, flare_data[i].fBlue ,TRUE)
				ELSE
					PRINTLN("AW SMOKE - PTFX DOES NOT EXIST")
				ENDIF
			ENDIF
		ELSE
			PRINTLN("AW SMOKE - iPTFXval:",flare_data[i]. iPTFXval)
		ENDIF
		
	ENDFOR

ENDPROC



//Tied to colour changing flares
PROC PROCESS_EVERY_FRAME_CAPTURE_LOGIC(INT iLoc )

	INT iTeam = MC_playerBD[ iLocalPart ].iteam
	
	HUD_COLOURS TeamColour
	
	FLOAT fNRed
	FLOAT fNGreen
	FLOAT fNBlue
	
	INT iRed,iGreen,iBlue,iAlpha
	
	INT iPTFX, iTeamsToCheck
	INT iNumberofCaptureTeams = 0
	INT iCaptureTeam = -1
	
	IF iNumCreatedFlarePTFX >= 0
		FOR iPTFX = 0 TO iNumCreatedFlarePTFX -1
			IF DOES_PARTICLE_FX_LOOPED_EXIST( ptfxFlareIndex[ iPTFX ] )
				PRINTLN( "[RCC MISSION][AW SMOKE] Particle FX Looped Exists  ")
				PRINTLN( "[RCC MISSION][AW SMOKE] flare_data[iPTFX].iPTFXval : ," ,flare_data[iPTFX].iPTFXval)
				
				IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_VFX_CAPTURE_OWNER)
					IF flare_data[iPTFX].iConnectedLocate = -1
					OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_ZONE(g_FMMC_STRUCT.iRootContentIDHash)
						IF NOT IS_SUDDEN_DEATH_ACTIVE()						
							FOR iTeamsToCheck = 0 TO MC_ServerBD.iNumberOfTeams -1
								IF iLoc < FMMC_MAX_GO_TO_LOCATIONS
								AND iLoc >= 0
									IF MC_serverBD_1.inumberOfTeamInArea[iLoc][iTeamsToCheck] != 0
									
										//If a team is in the area, increment the team count
										iNumberofCaptureTeams++
										
										#IF IS_DEBUG_BUILD
										bTeamInLocDebug[iTeamsToCheck] = TRUE
										#ENDIF
										
										//Capture team only gets used if only one team is inside of the location e.g. iNumberofCaptureTeams = 1
										iCaptureTeam = iTeamsToCheck
										
										PRINTLN("[RCC MISSION] [AW SMOKE] Team [",iTeamsToCheck,"] percentage at: ", MC_ServerBD.iCaptureBarPercentage[iTeamsToCheck])
										PRINTLN("[RCC MISSION] [AW SMOKE] MC_serverBD_1.iTeamWinningCapturePoint: ", iTeamsToCheck)									
										
										IF MC_ServerBD.iCaptureBarPercentage[iTeamsToCheck] > iMaxCaptureAmount
											iMaxCaptureAmount = MC_ServerBD.iCaptureBarPercentage[iTeamsToCheck]
											g_iTheWinningTeam = iTeamsToCheck
											
											PRINTLN( "[RCC MISSION] [AW SMOKE] iMaxCaptureAmount:", iMaxCaptureAmount)
											PRINTLN( "[RCC MISSION] [AW SMOKE] g_iTheWinningTeam:", g_iTheWinningTeam)
										ENDIF
									ELSE
										#IF IS_DEBUG_BUILD
											bTeamInLocDebug[iTeamsToCheck] = FALSE
										#ENDIF
										
										IF iTeamsToCheck = g_iTheWinningTeam
											g_iTheWinningTeam = -1
											iMaxCaptureAmount = 0
										ENDIF
									ENDIF 
								ENDIF
							ENDFOR
							
							#IF IS_DEBUG_BUILD
								iCaptureTeamDebug = iCaptureTeam
								iNumberofCaptureTeamsDebug = iNumberofCaptureTeams
							#ENDIF
							
							//If the iNumberofCaptureTeams = 0, g_iTheWinningTeam = the default of iCaptureTeam which is -1 to reset
							//	the zone back to neutral colour (yellow) since no one is inside of it
							//
							//If iNumberofCaptureTeams = 1, g_iTheWinningTeam = the team that set iCaptureTeam = iTeamsToCheck above,
							//	and because they are the only one in control of the zone it is set to their colour, even if another team
							//	has a higher score
							IF iNumberofCaptureTeams <= 1
								g_iTheWinningTeam = iCaptureTeam
								iMaxCaptureAmount = 0
							ENDIF
							
							
							
							//Reset 
							IF flare_data[iPTFX].iWinningTeam != g_iTheWinningTeam
								IF flare_data[iPTFX].fTransitionProgress > 0
								OR corona_flare_data[iPTFX].fTransitionProgress > 0
									PRINTLN( "[RCC MISSION][AW SMOKE] Reset Flare State - Transition not complete" )
									flare_data[iPTFX].iFlareState =  FLARE_STATE_INIT
									corona_flare_data[iPTFX].iFlareState =  FLARE_STATE_INIT
								ENDIF
							ENDIF
							
							PRINTLN( "[RCC MISSION][AW SMOKE] g_iTheWinningTeam: ", g_iTheWinningTeam) 
							
							IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
								IF iloc > -1 AND iloc < FMMC_MAX_GO_TO_LOCATIONS
									INT iTeamOverride = -1
									INT i = 0
									IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[MC_ServerBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE9_TEAM_COLOURED_LOCATES)
									OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_ZONE(g_FMMC_STRUCT.iRootContentIDHash)
										// It doesn't matter if this sets a team twice, because we override that with the contested colour
										IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_INSTANT_COLOUR_CHANGES_FOR_INSIDE_LOCATE)
										OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_ZONE(g_FMMC_STRUCT.iRootContentIDHash)
											FOR i = 0 TO FMMC_MAX_TEAMS-1
												IF MC_serverBD_1.inumberOfTeamInArea[iLoc][i] > 0
												AND ((IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_ZONE(g_FMMC_STRUCT.iRootContentIDHash) AND (iTeamOverride = -1 OR i = MC_PlayerBD[iParttoUse].iteam)) OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_ZONE(g_FMMC_STRUCT.iRootContentIDHash))
													iTeamOverride = i				
												ENDIF
											ENDFOR
										ENDIF
									ENDIF						
								
									IF g_iTheWinningTeam = -1
									AND (NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAreaRetained, MC_ServerBD_4.iCurrentHighestPriority[iTeam]) OR MC_serverBD.iLocOwner[iloc] = -1)
									AND iTeamOverride = -1
										//Show Yellow = Tied
										PRINTLN( "[RCC MISSION][AW SMOKE] YELLOW TIED -1 new")
										IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX,FLARE_STATE_NEUTRAL)
											SET_FLARE_TO_TRANSITION(iPTFX, FLARE_STATE_NEUTRAL)
										ENDIF
										
										SET_FLARE_NEW_RGB(iPTFX, 1.0, 1.0, 0)
										
										IF iWinningState != -1
											STRING sSoundSet 
											sSoundSet = "DLC_Apartments_Drop_Zone_Sounds"
											IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_HOSTILE_TAKEOVER_SOUNDS)
												sSoundSet = "dlc_xm_hota_Sounds"
											ENDIF
											PLAY_SOUND_FRONTEND(-1,"Zone_Neutral",sSoundSet,FALSE)
											PRINTLN( "[RCC MISSION][AW SMOKE] PLAYING NEUTRAL SOUND")
											iWinningState = -1
										ENDIF
									ELSE 
										//A team is winning
										INT iTeamToUse = g_iTheWinningTeam
										
										IF g_iTheWinningTeam = -1
											IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAreaRetained, MC_ServerBD_4.iCurrentHighestPriority[iTeam])
												iTeamToUse = MC_serverBD.iLocOwner[iloc]
											ENDIF
										ENDIF
										
										IF iTeamOverride > -1
											iTeamToUse = iTeamOverride
										ENDIF
										
										TeamColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeamToUse, PlayerToUse)
										GET_HUD_COLOUR(TeamColour, iRed, iGreen, iBlue, iAlpha)
									
										fNRed = TO_FLOAT(iRed) / 255.0
										fNGreen = TO_FLOAT(iGreen) / 255.0
										fNBlue = TO_FLOAT(iBlue) / 255.0
										
										#IF IS_DEBUG_BUILD
											IF iTeamToUse  = 0
												PRINTLN( "[RCC MISSION][AW SMOKE] TEAM 0 WINNING (ORANGE) ")
											ELIF iTeamToUse  = 1
												PRINTLN( "[RCC MISSION][AW SMOKE] TEAM 1 WINNING (GREEN) ")
											ELIF iTeamToUse  = 2
												PRINTLN( "[RCC MISSION][AW SMOKE] TEAM 2 WINNING (PINK) ")
											ELIF iTeamToUse  = 3
												PRINTLN( "[RCC MISSION][AW SMOKE] TEAM 3 WINNING (PURPLE) ")
											ENDIF
											IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX,FLARE_STATE_TEAM_0)
												PRINTLN( "[RCC MISSION][AW SMOKE] TRANSITIONING TO STATE - TEAM 0 ")
											ENDIF
											IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX,FLARE_STATE_TEAM_1)
												PRINTLN( "[RCC MISSION][AW SMOKE] TRANSITIONING TO STATE - TEAM 1 ")
											ENDIF
											IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX,FLARE_STATE_TEAM_2)
												PRINTLN( "[RCC MISSION][AW SMOKE] TRANSITIONING TO STATE - TEAM 2 ")
											ENDIF
											IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX,FLARE_STATE_TEAM_3)
												PRINTLN( "[RCC MISSION][AW SMOKE] TRANSITIONING TO STATE - TEAM 3 ")
											ENDIF
											PRINTLN( "[RCC MISSION][AW SMOKE] iRed: ",iRed)
											PRINTLN( "[RCC MISSION][AW SMOKE] iGreen: ",iGreen)
											PRINTLN( "[RCC MISSION][AW SMOKE] iBlue: ",iBlue)
											
											PRINTLN( "[RCC MISSION][AW SMOKE] fNRed: ",fNRed)
											PRINTLN( "[RCC MISSION][AW SMOKE] fNGreen: ",fNGreen)
											PRINTLN( "[RCC MISSION][AW SMOKE] fNBlue: ",fNBlue)
										#ENDIF
										
										IF iTeamToUse = 0
											IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX,FLARE_STATE_TEAM_0)
												SET_FLARE_TO_TRANSITION(iPTFX, FLARE_STATE_TEAM_0)
											ENDIF
										ELIF iTeamToUse = 1
											IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX,FLARE_STATE_TEAM_1)
												SET_FLARE_TO_TRANSITION(iPTFX, FLARE_STATE_TEAM_1)
											ENDIF
										ELIF iTeamToUse = 2
											IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX,FLARE_STATE_TEAM_2)
												SET_FLARE_TO_TRANSITION(iPTFX, FLARE_STATE_TEAM_2)
											ENDIF
										ELIF iTeamToUse = 3
											IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX,FLARE_STATE_TEAM_3)
												SET_FLARE_TO_TRANSITION(iPTFX, FLARE_STATE_TEAM_3)
											ENDIF
										ENDIF
										
										SET_FLARE_NEW_RGB(iPTFX, fNRed, fNGreen, fNBlue)
										
									ENDIF					
									
									IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_CONTESTED_COLOUR_FOR_LOCATES)
										IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
											IF g_iTheWinningTeam > -1
											OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_INSTANT_COLOUR_CHANGES_FOR_INSIDE_LOCATE)
												INT iHostileTeamsInsideLocate = 0
												
												IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_INSTANT_COLOUR_CHANGES_FOR_INSIDE_LOCATE)										
													iHostileTeamsInsideLocate = -1
													FOR i = 0 TO FMMC_MAX_TEAMS-1
														IF MC_serverBD_1.inumberOfTeamInArea[iLoc][i] > 0
															iHostileTeamsInsideLocate++
														ENDIF
													ENDFOR
												ELSE
													FOR i = 0 TO FMMC_MAX_TEAMS-1
														IF NOT DOES_TEAM_LIKE_TEAM(g_iTheWinningTeam, i)
														AND g_iTheWinningTeam != i
															IF MC_serverBD_1.inumberOfTeamInArea[iLoc][i] > 0
																iHostileTeamsInsideLocate++
															ENDIF
														ENDIF
													ENDFOR
												ENDIF
												
												PRINTLN("[RCC MISSION][AW SMOKE] - Contested Smoke check 1 - iHostileTeamsInsideLocate: ", iHostileTeamsInsideLocate)
												IF iHostileTeamsInsideLocate > 0
													iRed = 255
													iGreen = 0
													iBlue = 0
													
													fNRed = TO_FLOAT(iRed) / 255.0
													fNGreen = TO_FLOAT(iGreen) / 255.0
													fNBlue = TO_FLOAT(iBlue) / 255.0
													
													IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX,FLARE_STATE_TRANSITION)
														SET_FLARE_TO_TRANSITION(iPTFX, FLARE_STATE_TRANSITION)
													ENDIF
													
													SET_FLARE_NEW_RGB(iPTFX, fNRed, fNGreen, fNBlue)
												ENDIF
											ENDIF
										ELSE
											PRINTLN("[RCC MISSION] Processing Hostile Takeover Smoke Colours - 1 - Location: ", iLoc)
											fNRed = TO_FLOAT(iCaptureLocateRed[iLoc]) / 255.0
											fNGreen = TO_FLOAT(iCaptureLocateGreen[iLoc]) / 255.0
											fNBlue = TO_FLOAT(iCaptureLocateBlue[iLoc]) / 255.0

											IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX,FLARE_STATE_TRANSITION)
												SET_FLARE_TO_TRANSITION(iPTFX, FLARE_STATE_TRANSITION)
											ENDIF
											
											SET_FLARE_NEW_RGB(iPTFX, fNRed, fNGreen, fNBlue)

										ENDIF
									ENDIF						
								ENDIF
							ENDIF
							
							//Handle audio
							IF g_iTheWinningTeam != - 1
								STRING sSoundSet 
								sSoundSet = "DLC_Apartments_Drop_Zone_Sounds"
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_HOSTILE_TAKEOVER_SOUNDS)
									sSoundSet = "dlc_xm_hota_Sounds"
								ENDIF
								IF iTeam = g_iTheWinningTeam
									IF iWinningState != 1
										PLAY_SOUND_FRONTEND(-1,"Zone_Team_Capture", sSoundSet,FALSE)
										PRINTLN( "[RCC MISSION][AW SMOKE] PLAYING WINNING SOUND")
										iWinningState = 1
									ENDIF
								ELSE
									IF iWinningState != 2
										PLAY_SOUND_FRONTEND(-1,"Zone_Enemy_Capture", sSoundSet,FALSE)
										PRINTLN( "[RCC MISSION][AW SMOKE] PLAYING LOSING SOUND")
										iWinningState = 2
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						PRINTLN("[RCC MISSION] PROCESS_EVERY_FRAME_CAPTURE_LOGIC - Flare: ", iPTFX, " ConnectedLoc: ", flare_data[iPTFX].iConnectedLocate, " MC_serverBD.iLocOwner: ", MC_serverBD.iLocOwner[flare_data[iPTFX].iConnectedLocate])
						IF MC_serverBD.iLocOwner[flare_data[iPTFX].iConnectedLocate] = -1
							IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
								IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_CONTESTED_COLOUR_FOR_LOCATES)
									IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX,FLARE_STATE_NEUTRAL)
										SET_FLARE_TO_TRANSITION(iPTFX, FLARE_STATE_NEUTRAL)
									ENDIF
									SET_FLARE_NEW_RGB(iPTFX, 1.0, 1.0, 0)
								ELSE
									
									INT iHostileTeamsInsideLocate = 0
									INT i = 0
									IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_INSTANT_COLOUR_CHANGES_FOR_INSIDE_LOCATE)									
										iHostileTeamsInsideLocate = -1
										FOR i = 0 TO FMMC_MAX_TEAMS-1
											IF MC_serverBD_1.inumberOfTeamInArea[flare_data[iPTFX].iConnectedLocate][i] > 0
												iHostileTeamsInsideLocate++
											ENDIF
										ENDFOR
									ELSE
										FOR i = 0 TO FMMC_MAX_TEAMS-1
											IF NOT DOES_TEAM_LIKE_TEAM(MC_playerBD[iLocalPart].iteam, i)
											AND MC_playerBD[iLocalPart].iteam != i
												IF MC_serverBD_1.inumberOfTeamInArea[flare_data[iPTFX].iConnectedLocate][i] > 0
													iHostileTeamsInsideLocate++
												ENDIF
											ENDIF
										ENDFOR
									ENDIF
									PRINTLN("[RCC MISSION][AW SMOKE] - Contested Smoke check 2 - iHostileTeamsInsideLocate: ", iHostileTeamsInsideLocate)
									IF (iHostileTeamsInsideLocate > 0
									AND MC_serverBD_1.inumberOfTeamInArea[flare_data[iPTFX].iConnectedLocate][iTeam] > 0)
									OR (IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType) AND IS_ZONE_BEING_CONTESTED(flare_data[iPTFX].iConnectedLocate))
										IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX, FLARE_STATE_TRANSITION)
											SET_FLARE_TO_TRANSITION(iPTFX, FLARE_STATE_TRANSITION)
										ENDIF
										SET_FLARE_NEW_RGB(iPTFX, 1.0, 0.0, 0.0)
									ELSE
										IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX, FLARE_STATE_NEUTRAL)
											SET_FLARE_TO_TRANSITION(iPTFX, FLARE_STATE_NEUTRAL)
										ENDIF

										SET_FLARE_NEW_RGB(iPTFX, 1.0, 1.0, 0.0)
									ENDIF
								ENDIF
							ELSE
								PRINTLN("[RCC MISSION] Processing Hostile Takeover Smoke Colours - 2 - Location: ", flare_data[iPTFX].iConnectedLocate)
								FLARE_STATE tempFlareState
								IF eCaptureLocateState[flare_data[iPTFX].iConnectedLocate] = eCapture_neutral
									tempFlareState = FLARE_STATE_NEUTRAL
								ELIF eCaptureLocateState[flare_data[iPTFX].iConnectedLocate] = eCapture_contested
									tempFlareState = FLARE_STATE_TRANSITION
								ENDIF
								
								IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX, tempFlareState)
									SET_FLARE_TO_TRANSITION(iPTFX, tempFlareState)
								ENDIF
								
								fNRed = TO_FLOAT(iCaptureLocateRed[flare_data[iPTFX].iConnectedLocate]) / 255.0
								fNGreen = TO_FLOAT(iCaptureLocateGreen[flare_data[iPTFX].iConnectedLocate]) / 255.0
								fNBlue = TO_FLOAT(iCaptureLocateBlue[flare_data[iPTFX].iConnectedLocate]) / 255.0

								SET_FLARE_NEW_RGB(iPTFX, fNRed, fNGreen, fNBlue)
							ENDIF
						ELSE
							INT iCurrentOwners = MC_serverBD.iLocOwner[flare_data[iPTFX].iConnectedLocate]
							
							IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
							OR (IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType) AND eCaptureLocateState[flare_data[iPTFX].iConnectedLocate] != eCapture_contested)
							
								TeamColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iCurrentOwners, PlayerToUse)
								GET_HUD_COLOUR(TeamColour, iRed, iGreen, iBlue, iAlpha)
								PRINTLN("[RCC MISSION] PROCESS_EVERY_FRAME_CAPTURE_LOGIC - MC_serverBD.iLocOwner[flare_data[iPTFX].iConnectedLocate]: ", MC_serverBD.iLocOwner[flare_data[iPTFX].iConnectedLocate])
								fNRed = TO_FLOAT(iRed) / 255.0
								fNGreen = TO_FLOAT(iGreen) / 255.0
								fNBlue = TO_FLOAT(iBlue) / 255.0
								
								IF iCurrentOwners = 0
									IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX,FLARE_STATE_TEAM_0)
										SET_FLARE_TO_TRANSITION(iPTFX, FLARE_STATE_TEAM_0)
									ENDIF
								ELIF iCurrentOwners = 1
									IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX,FLARE_STATE_TEAM_1)
										SET_FLARE_TO_TRANSITION(iPTFX, FLARE_STATE_TEAM_1)
									ENDIF
								ELIF iCurrentOwners = 2
									IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX,FLARE_STATE_TEAM_2)
										SET_FLARE_TO_TRANSITION(iPTFX, FLARE_STATE_TEAM_2)
									ENDIF
								ELIF iCurrentOwners = 3
									IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX,FLARE_STATE_TEAM_3)
										SET_FLARE_TO_TRANSITION(iPTFX, FLARE_STATE_TEAM_3)
									ENDIF
								ENDIF
								
								SET_FLARE_NEW_RGB(iPTFX, fNRed, fNGreen, fNBlue)
							ENDIF
							
							IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_CONTESTED_COLOUR_FOR_LOCATES)
									IF iCurrentOwners > -1
									OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_INSTANT_COLOUR_CHANGES_FOR_INSIDE_LOCATE)
										INT iHostileTeamsInsideLocate = 0
										INT i = 0
										IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_INSTANT_COLOUR_CHANGES_FOR_INSIDE_LOCATE)										
											iHostileTeamsInsideLocate = -1
											FOR i = 0 TO FMMC_MAX_TEAMS-1
												IF MC_serverBD_1.inumberOfTeamInArea[flare_data[iPTFX].iConnectedLocate][i] > 0
													iHostileTeamsInsideLocate++
												ENDIF
											ENDFOR
										ELSE
											FOR i = 0 TO FMMC_MAX_TEAMS-1
												IF NOT DOES_TEAM_LIKE_TEAM(iCurrentOwners, i)
												AND iCurrentOwners != i
													IF MC_serverBD_1.inumberOfTeamInArea[flare_data[iPTFX].iConnectedLocate][i] > 0
														iHostileTeamsInsideLocate++
													ENDIF
												ENDIF
											ENDFOR
										ENDIF
										PRINTLN("[RCC MISSION][AW SMOKE] - Contested Smoke check 3 - iHostileTeamsInsideLocate: ", iHostileTeamsInsideLocate)
										IF iHostileTeamsInsideLocate > 0
										OR (IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType) AND IS_ZONE_BEING_CONTESTED(flare_data[iPTFX].iConnectedLocate))
											iRed = 255
											iGreen = 0
											iBlue = 0
											
											fNRed = TO_FLOAT(iRed) / 255.0
											fNGreen = TO_FLOAT(iGreen) / 255.0
											fNBlue = TO_FLOAT(iBlue) / 255.0
											
											IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX,FLARE_STATE_TRANSITION)
												SET_FLARE_TO_TRANSITION(iPTFX, FLARE_STATE_TRANSITION)
											ENDIF
											
											SET_FLARE_NEW_RGB(iPTFX, fNRed, fNGreen, fNBlue)
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF eCaptureLocateState[flare_data[iPTFX].iConnectedLocate] = eCapture_contested
									PRINTLN("[RCC MISSION] Processing Hostile Takeover Smoke Colours - 3 (Contested) - Location: ", flare_data[iPTFX].iConnectedLocate)
									fNRed = TO_FLOAT(iCaptureLocateRed[flare_data[iPTFX].iConnectedLocate]) / 255.0
									fNGreen = TO_FLOAT(iCaptureLocateGreen[flare_data[iPTFX].iConnectedLocate]) / 255.0
									fNBlue = TO_FLOAT(iCaptureLocateBlue[flare_data[iPTFX].iConnectedLocate]) / 255.0

									IF SHOULD_FLARE_SET_TO_TRANSITION_INTO_NEW_STATE(iPTFX,FLARE_STATE_TRANSITION)
										SET_FLARE_TO_TRANSITION(iPTFX, FLARE_STATE_TRANSITION)
									ENDIF
									
									SET_FLARE_NEW_RGB(iPTFX, fNRed, fNGreen, fNBlue)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				PRINTLN( "[RCC MISSION][AW SMOKE] PTFX doesn't exist")
			ENDIF
		ENDFOR
	ENDIF
	
	IF bIsLocalPlayerHost
	AND IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
		IF iLoc > -1
			INT iTempTeam
			FOR iTempTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
				IF MC_serverBD_1.inumberOfTeamInArea[iLoc][iTempTeam] != GET_NUMBER_OF_PARTICIPANTS_FROM_TEAM_IN_THIS_AREA(iLoc, iTempTeam)
					MC_serverBD_1.inumberOfTeamInArea[iLoc][iTempTeam] = GET_NUMBER_OF_PARTICIPANTS_FROM_TEAM_IN_THIS_AREA(iLoc, iTempTeam)
					PRINTLN("PROCESS_EVERY_FRAME_CAPTURE_LOGIC - MC_serverBD_1.inumberOfTeamInArea[iLoc][iTempTeam] = ", MC_serverBD_1.inumberOfTeamInArea[iLoc][iTempTeam])
				ENDIF
			ENDFOR
		ENDIF
	ENDIF

ENDPROC


PROC PROCESS_EVERY_FRAME_PLAYER_ACTION_MODE( BOOL bForceInstantUpdate )

	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
		
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iPartToUse].iteam)
			CLEAR_BIT(iLocalBoolCheck33, LBOOL33_WEAPON_FORCED_EQUIPPED_THIS_RULE)
		ENDIF
		
		// 2079480 - Added ability to set combat mode on a per rule basis.
		// updated for bugs 2227329 & 2235826
		
		IF IS_PED_IN_COVER(localPlayerPed)
		OR IS_PED_GOING_INTO_COVER(localPlayerPed)
		OR (IS_BIT_SET(iLocalBoolCheck30, LBOOL30_HAS_BEEN_SET_INTO_COVER_START_POSITION)
		AND NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_HAS_CORRECTED_COVER_START_CAMERA_HEADING))
			PRINTLN("[RCC MISSION] - SET_PED_USING_ACTION_MODE - In or going in cover, not setting action mode yet... ")
			EXIT
		ENDIF
		
		IF IS_PED_IN_ANY_VEHICLE(localPlayerPed, TRUE)
		OR IS_PED_GETTING_INTO_A_VEHICLE(LocalPlayerPed)
			PRINTLN("[RCC MISSION] - SET_PED_USING_ACTION_MODE - In or getting in a vehicle, not setting action mode yet... ")
			EXIT
		ENDIF
		
		// FORCE ON
		IF NOT (IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
		AND (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iTeamBitset2, ciBS2_JUGGERNAUTS) OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_SUDDEN_DEATH_JUGGERNAUT)))
			IF NOT IS_CELLPHONE_CAMERA_IN_USE()
				IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetTwo[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE2_FORCE_COMBAT_MODE)
					
					IF NOT IS_PED_USING_ACTION_MODE( LocalPlayerPed )
					AND NOT GET_PED_STEALTH_MOVEMENT( LocalPlayerPed )
						IF CAN_FORCE_PLAYER_ACTION_MODE_FOR_MP( TRUE )
							SET_PED_USING_ACTION_MODE( LocalPlayerPed, TRUE )
							IF bForceInstantUpdate
								SET_PED_RESET_FLAG( LocalPlayerPed, PRF_SkipOnFootIdleIntro, TRUE )
								FORCE_PED_MOTION_STATE( LocalPlayerPed, MS_ON_FOOT_IDLE )
							ENDIF
							PRINTLN("[RCC MISSION] - SET_PED_USING_ACTION_MODE - Forcing Action mode ON - bForceInstantUpdate = ", bForceInstantUpdate)
							CLEAR_BIT(iLocalBoolCheck10, LBOOL10_PLAYER_ACTION_MODE_RESET)
							SET_BIT(iLocalBoolCheck33, LBOOL33_USING_ACTION_MODE)
						ENDIF
					ENDIF

				// FORCE OFF
				ELIF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetThree[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE3_FORCE_COMBAT_MODE_OFF)
				
					PRINTLN("[RCC MISSION] - SET_PED_RESET_FLAG( PRF_DisableActionMode ) - Forcing Action mode OFF ")
					SET_PED_RESET_FLAG( LocalPlayerPed, PRF_DisableActionMode, TRUE )
				
					IF IS_PED_USING_ACTION_MODE( LocalPlayerPed )
						IF CAN_FORCE_PLAYER_ACTION_MODE_FOR_MP( FALSE )
							SET_PED_USING_ACTION_MODE( LocalPlayerPed, FALSE )
							IF bForceInstantUpdate
								SET_PED_RESET_FLAG( LocalPlayerPed, PRF_SkipOnFootIdleIntro, TRUE )
								FORCE_PED_MOTION_STATE( LocalPlayerPed, MS_ON_FOOT_IDLE )
							ENDIF
							PRINTLN("[RCC MISSION] - SET_PED_USING_ACTION_MODE - Forcing Action mode OFF - bForceInstantUpdate = ", bForceInstantUpdate)
							CLEAR_BIT(iLocalBoolCheck10, LBOOL10_PLAYER_ACTION_MODE_RESET)
							SET_BIT(iLocalBoolCheck33, LBOOL33_USING_ACTION_MODE_FORCE_OFF)
						ENDIF
					ENDIF
					
				// DO NOT FORCE EITHER
				ELSE
					IF NOT IS_BIT_SET( iLocalBoolCheck10, LBOOL10_PLAYER_ACTION_MODE_RESET )
					AND (IS_BIT_SET(iLocalBoolCheck33, LBOOL33_USING_ACTION_MODE) OR IS_BIT_SET(iLocalBoolCheck33, LBOOL33_USING_ACTION_MODE_FORCE_OFF))
						// Reset the action mode with a short duration (to overwrite it previously having been set to -1)
						// by calling SET_PED_USING_ACTION_MODE with the same state it's already in.
						SET_PED_USING_ACTION_MODE( LocalPlayerPed, IS_PED_USING_ACTION_MODE( LocalPlayerPed ), 1000 ) 
						PRINTLN("[RCC MISSION] - SET_PED_USING_ACTION_MODE - Reset Action mode with ", BOOL_TO_STRING(IS_PED_USING_ACTION_MODE( LocalPlayerPed )), " to naturally end")
						
						SET_BIT(iLocalBoolCheck10, LBOOL10_PLAYER_ACTION_MODE_RESET)
						CLEAR_BIT(iLocalBoolCheck33, LBOOL33_USING_ACTION_MODE)
						CLEAR_BIT(iLocalBoolCheck33, LBOOL33_USING_ACTION_MODE_FORCE_OFF)
					ENDIF
				ENDIF
				
				IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetFourteen[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE14_FORCE_WEAPON_IN_HAND_ON_RULE)									
					IF NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_WEAPON_FORCED_EQUIPPED_THIS_RULE)
						WEAPON_TYPE weapType
						
						IF NOT IS_PED_CLIMBING(LocalPlayerPed)
						AND NOT IS_PED_JUMPING(LocalPlayerPed)
						AND NOT IS_PED_SWIMMING(LocalPlayerPed)
						AND NOT IS_PED_FALLING(LocalPlayerPed)	
							weapType = GET_BEST_PED_WEAPON(LocalPlayerPed)
							IF weapType != WEAPONTYPE_INVALID
							AND weapType != WEAPONTYPE_UNARMED
								PRINTLN("[RCC MISSION] - SET_PED_USING_ACTION_MODE - ciBS_RULE14_FORCE_WEAPON_IN_HAND_ON_RULE - (1) - Setting Ped Weapon as: ", GET_WEAPON_NAME_FROM_WEAPON_TYPE(weapType), " Int: ", ENUM_TO_INT(weapType))
								SET_CURRENT_PED_WEAPON(LocalPlayerPed, weapType, TRUE)
							ELSE
								PRINTLN("[RCC MISSION] - SET_PED_USING_ACTION_MODE - ciBS_RULE14_FORCE_WEAPON_IN_HAND_ON_RULE - (1) - No weapon to equip. Weapon is: ", GET_WEAPON_NAME_FROM_WEAPON_TYPE(weapType), " Int: ", ENUM_TO_INT(weapType))
							ENDIF
						ENDIF
						
						IF GET_CURRENT_PED_WEAPON(LocalPlayerPed, weapType, FALSE)
							IF weapType != WEAPONTYPE_INVALID
							AND weapType != WEAPONTYPE_UNARMED
								PRINTLN("[RCC MISSION] - SET_PED_USING_ACTION_MODE - ciBS_RULE14_FORCE_WEAPON_IN_HAND_ON_RULE - (2) - Weapon is: ", GET_WEAPON_NAME_FROM_WEAPON_TYPE(weapType), " Int: ", ENUM_TO_INT(weapType), " Success")
								SET_BIT(iLocalBoolCheck33, LBOOL33_WEAPON_FORCED_EQUIPPED_THIS_RULE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF				
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION] - JUGGERNAUT SET_PED_RESET_FLAG( PRF_DisableActionMode ) - Forcing Action mode OFF ")
			SET_PED_RESET_FLAG( LocalPlayerPed, PRF_DisableActionMode, TRUE )
		
			IF IS_PED_USING_ACTION_MODE( LocalPlayerPed )
				IF CAN_FORCE_PLAYER_ACTION_MODE_FOR_MP( FALSE )
					SET_PED_USING_ACTION_MODE( LocalPlayerPed, FALSE )
					IF bForceInstantUpdate
						SET_PED_RESET_FLAG( LocalPlayerPed, PRF_SkipOnFootIdleIntro, TRUE )
						FORCE_PED_MOTION_STATE( LocalPlayerPed, MS_ON_FOOT_IDLE )
					ENDIF
					PRINTLN("[RCC MISSION] - SET_PED_USING_ACTION_MODE - Forcing Action mode OFF - bForceInstantUpdate = ", bForceInstantUpdate)
					CLEAR_BIT( iLocalBoolCheck10, LBOOL10_PLAYER_ACTION_MODE_RESET )
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL IS_AIMING_WITH_SCOPE()
	WEAPON_TYPE weap = GET_SELECTED_PED_WEAPON(localPlayerPed)
	IF IS_FIRST_PERSON_AIM_CAM_ACTIVE()
		IF weap = WEAPONTYPE_HEAVYSNIPER
		OR weap = WEAPONTYPE_SNIPERRIFLE
		OR weap = WEAPONTYPE_REMOTESNIPER
		OR weap = WEAPONTYPE_DLC_ASSAULTSNIPER
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

// [FIX_2020_CONTROLLER] - A lot of these adversary mode process functions could be moved to a new "FM_Mission_Controller_AdversaryMode.sch header file.
// Very necessary but inefficient way of managing whether or not the particle should be on. Micmic some (But not all) of the functionality of WEAPON_EQUIP_ON_RULE_CHANGE
PROC MAINTAIN_GUNSMITH_LAST_KILL_PARTICLES(INT iTeam, INT iRule)

	IF iRule >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_ENABLE_GUN_ROULETTE )
		// If the ped dies, we want to refresh the ammunition
		IF IS_PED_INJURED(localPlayerPed)
			IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_LoopedGunsmithLastKill)
				PRINTLN("[LM][MAINTAIN_GUNSMITH_LAST_KILL_PARTICLES]- Removing Particle Effect.")
				REMOVE_PARTICLE_FX(ptfx_LoopedGunsmithLastKill)
			ENDIF
		ENDIF
		
		// Check to see if the player ped is ready to have a new weapon equipped.
		IF NOT IS_PED_INJURED(localPlayerPed)
		AND NOT IS_PED_IN_ANY_VEHICLE(localPlayerPed, TRUE)
		AND NOT IS_PED_CLIMBING(localPlayerPed)
		AND NOT IS_PED_SWIMMING(localPlayerPed)
		AND NOT IS_ANY_INTERACTION_ANIM_PLAYING()
			PRINTLN("[LM][MAINTAIN_GUNSMITH_LAST_KILL_PARTICLES] - Ped Checks Pass")

			WEAPON_TYPE weapToChoose
			FLOAT fR, fG, fB
			INT iR, iG, iB, iA
			
			// Initialise the weapon & ammo values.
			weapToChoose = GET_WEAPON_FOR_GUNSMITH(iRule, iTeam)
						
			INT iScore 				= MC_serverBD.iScoreOnThisRule[iTeam]
			INT iRequiredScore		= ROUND(GET_FMMC_OBJECTIVE_SCORE_LIMIT_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTargetScore[iRule], MC_ServerBD.iNumberOfPlayingPlayers[iTeam])*GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING(iTeam, iRule))
			
			// Spawn the Particles over the weapon if we only need 1 point to progress.
			IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_LoopedGunsmithLastKill)
				PRINTLN("[LM][MAINTAIN_GUNSMITH_LAST_KILL_PARTICLES] - Exists")
				IF NOT ((iScore+1) = iRequiredScore)
				OR GET_SELECTED_PED_WEAPON(localPlayerPed) != weapToChoose
				OR IS_AIMING_WITH_SCOPE()
					PRINTLN("[LM][MAINTAIN_GUNSMITH_LAST_KILL_PARTICLES] - Removing Particle Effect.")
					REMOVE_PARTICLE_FX(ptfx_LoopedGunsmithLastKill)
				ENDIF
			ENDIF
			IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_LoopedGunsmithLastKill)
				PRINTLN("[LM][MAINTAIN_GUNSMITH_LAST_KILL_PARTICLES] - Does not Exist")
				IF (iScore+1) = iRequiredScore
				AND GET_SELECTED_PED_WEAPON(localPlayerPed) = weapToChoose
					ENTITY_INDEX entWeapon = GET_CURRENT_PED_WEAPON_ENTITY_INDEX(LocalPlayerPed)
					IF DOES_ENTITY_EXIST(entWeapon)
						IF NOT HAS_NET_TIMER_STARTED(tdLastKillPTFXTimer)
							START_NET_TIMER(tdLastKillPTFXTimer)
						ENDIF
						IF HAS_NET_TIMER_STARTED(tdLastKillPTFXTimer)
							IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdLastKillPTFXTimer, tdLastKillPTFXDelay)
								USE_PARTICLE_FX_ASSET("scr_bike_adversary")
								
								FLOAT fParticleScale = 0.5
								
								// Particle effect should be smaller. The weapons listed here are not all encompassing of the smaller weapons, but the ones which are used in Gunsmith.
								SWITCH weapToChoose
									CASE		WEAPONTYPE_MICROSMG
									CASE		WEAPONTYPE_PISTOL
									CASE		WEAPONTYPE_COMBATPISTOL
									CASE		WEAPONTYPE_APPISTOL
									CASE		WEAPONTYPE_GRENADE
									CASE		WEAPONTYPE_MOLOTOV
									CASE		WEAPONTYPE_KNIFE
									CASE		WEAPONTYPE_NIGHTSTICK
									CASE		WEAPONTYPE_DLC_HEAVYPISTOL
									CASE		WEAPONTYPE_DLC_SNSPISTOL
									CASE		WEAPONTYPE_DLC_DAGGER
									CASE		WEAPONTYPE_DLC_VINTAGEPISTOL
									CASE		WEAPONTYPE_DLC_PISTOL50
									CASE		WEAPONTYPE_DLC_KNUCKLE
									CASE		WEAPONTYPE_DLC_MARKSMANPISTOL
									CASE		WEAPONTYPE_DLC_COMPACTLAUNCHER
									CASE		WEAPONTYPE_DLC_MINISMG
									CASE		WEAPONTYPE_DLC_POOLCUE
									CASE		WEAPONTYPE_DLC_AUTOSHOTGUN
									CASE		WEAPONTYPE_DLC_REVOLVER
									CASE		WEAPONTYPE_DLC_MACHINEPISTOL
									CASE		WEAPONTYPE_DLC_MACHETE
										fParticleScale = 0.25
									BREAK
								ENDSWITCH
								
								HUD_COLOURS tempColour
								tempColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_playerBD[iLocalPart].iteam, LocalPlayer)
								GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(tempColour), iR, iG, iB, iA)
								fR = TO_FLOAT(iR)/255.0
								fG = TO_FLOAT(iG)/255.0
								fB = TO_FLOAT(iB)/255.0
				
								ptfx_LoopedGunsmithLastKill = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY_BONE("scr_adversary_gunsmith_weap_smoke", entWeapon, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 0, fParticleScale, DEFAULT, DEFAULT, DEFAULT, fR, fG, fB)
								PRINTLN("[LM][MAINTAIN_GUNSMITH_LAST_KILL_PARTICLES] - Last Kill on Rule: Adding Looping Particle Effect.")
								RESET_NET_TIMER(tdLastKillPTFXTimer)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_LoopedGunsmithLastKill)
				REMOVE_PARTICLE_FX(ptfx_LoopedGunsmithLastKill)
				PRINTLN("[LM][MAINTAIN_GUNSMITH_LAST_KILL_PARTICLES] - Removing Particle Effect.")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC WEAPON_EQUIP_ON_RULE_CHANGE_AMMO(INT iTeam, INT iRule, WEAPON_TYPE weapToChoose, INT iAmmoMax, INT iAmmoToGive)
	UNUSED_PARAMETER(iTeam)
	UNUSED_PARAMETER(iRule)
	// If the Clips we want to give in the creator exceeds the ammoMax, then just give max ammo instead.
	IF iAmmoToGive < iAmmoMax	
		SET_PED_AMMO(LocalPlayerPed, weapToChoose, iAmmoToGive)
		PRINTLN("[LM][GUNSMITH] - Give creator set Ammo")
	ELSE
		SET_PED_AMMO(LocalPlayerPed, weapToChoose, iAmmoMax)
		PRINTLN("[LM][GUNSMITH] - Give Max Ammo")
	ENDIF
	
	PRINTLN("[LM][GUNSMITH] - Creator set ammo clips: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sGRouletteStruct[iRule].iClips)
	
	// If we have the clip size in the creator set to 50 then give infinite ammo instead.
	//IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sGRouletteStruct[iRule].iClips >= 50 url:bugstar:3066515
	IF SHOULD_GUNSMITH_WEAPON_HAVE_UNLIMITED_AMMO(weapToChoose)
		SET_PED_INFINITE_AMMO(LocalPlayerPed, TRUE, weapToChoose)
		PRINTLN("[LM][GUNSMITH] - Infinite Ammo")
	ELSE
		SET_PED_INFINITE_AMMO(LocalPlayerPed, FALSE, weapToChoose)
		PRINTLN("[LM][GUNSMITH] - Finite Ammo")
	ENDIF	
	
	SET_PED_AMMO_TO_DROP(LocalPlayerPed, 0)
	SET_PED_DROPS_WEAPONS_WHEN_DEAD(LocalPlayerPed, FALSE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DROP_AMMO)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DROP_WEAPON)
ENDPROC

PROC WEAPON_EQUIP_ON_RULE_CHANGE()
	INT iTeam = MC_PlayerBD[iLocalPart].iTeam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	MAINTAIN_GUNSMITH_LAST_KILL_PARTICLES(iTeam, iRule)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_ENABLE_GUN_ROULETTE )
	AND NOT IS_PARTICIPANT_A_SPECTATOR(iLocalPart)
	AND NOT HAS_MULTI_RULE_TIMER_EXPIRED(iTeam)
		PRINTLN("[LM][WEAPON_EQUIP_ON_RULE_CHANGE] - iRule: ", iRule)
		PRINTLN("[LM][WEAPON_EQUIP_ON_RULE_CHANGE] - g_FMMC_STRUCT.sFMMCEndConditions[iteam].iNumberOfTeamRules: ", g_FMMC_STRUCT.sFMMCEndConditions[iteam].iNumberOfTeamRules)
						
		// Load the audio sounds.
		IF NOT IS_BIT_SET(iLocalBoolCheck18, LBOOL18_ENABLE_GUNSMITH_AUDIO_SOUNDS)
			PRINTLN("[LM][GUNSMITH][AUDIO] - Audio not loaded. Requesting audio banks")
			BOOL bLoadedAudio = FALSE
			IF REQUEST_SCRIPT_AUDIO_BANK("DLC_BIKER/BKR_KQ_01")
				PRINTLN("[LM][GUNSMITH][AUDIO] - DLC_BIKER/BKR_KQ_01 Loaded")
				bLoadedAudio = TRUE
			ELSE
				PRINTLN("[LM][GUNSMITH][AUDIO] - DLC_BIKER/BKR_KQ_01 Not Loaded")
				bLoadedAudio = FALSE
			ENDIF
			
			IF bLoadedAudio
				IF REQUEST_SCRIPT_AUDIO_BANK("DLC_BIKER/BKR_KQ_02")
					PRINTLN("[LM][GUNSMITH][AUDIO] - DLC_BIKER/BKR_KQ_02 Loaded")
					bLoadedAudio = TRUE
				ELSE
					PRINTLN("[LM][GUNSMITH][AUDIO] - DLC_BIKER/BKR_KQ_02 Not Loaded")
					bLoadedAudio = FALSE
				ENDIF
				
				// If both of the audio banks are loaded, set the bit to stop requesting them.
				IF bLoadedAudio
					PRINTLN("[LM][GUNSMITH][AUDIO] - Setting bit: LBOOL18_ENABLE_GUNSMITH_AUDIO_SOUNDS")
					SET_BIT(iLocalBoolCheck18, LBOOL18_ENABLE_GUNSMITH_AUDIO_SOUNDS)
				ENDIF
			ENDIF
		ENDIF
		
		// The Rule has changed, we want to now check and see if we should be equipping a new weapon
		IF IS_BIT_SET(iLocalBoolCheck6, (LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam))
			PRINTLN("[LM][WEAPON_EQUIP_ON_RULE_CHANGE] - New Rule, New Weapon.")
			SET_BIT(iLocalBoolCheck18, LBOOL18_GUNSMITH_NEW_WEAPON_READY)
		ENDIF

		// If the ped dies, we want to refresh the ammunition
		IF IS_PED_INJURED(localPlayerPed)
			PRINTLN("[LM][WEAPON_EQUIP_ON_RULE_CHANGE] - Player Died replenish ammo")
			SET_BIT(iLocalBoolCheck18, LBOOL18_GUNSMITH_REPLENISH_AMMO_REQUIRED)
		ENDIF
					
		// Check to see if the player ped is ready to have a new weapon equipped.
		IF NOT IS_PED_INJURED(localPlayerPed)
		AND NOT IS_PED_IN_ANY_VEHICLE(localPlayerPed, TRUE)
		AND NOT IS_ENTITY_IN_AIR(localPlayerPed)
		AND NOT IS_PED_CLIMBING(localPlayerPed)
		AND NOT IS_PED_SWIMMING(localPlayerPed)
			PRINTLN("[LM][WEAPON_EQUIP_ON_RULE_CHANGE] - Ped Checks Pass")
			
			IF NOT HAS_TEAM_FINISHED(iTeam)
			AND NOT HAS_TEAM_FAILED(iTeam)
			AND IS_TEAM_ACTIVE(iTeam)
				PRINTLN("[LM][WEAPON_EQUIP_ON_RULE_CHANGE] - Team Checks pass")
				WEAPON_TYPE weapToChoose
				
				// Initialise the weapon & ammo values.
				weapToChoose = GET_WEAPON_FOR_GUNSMITH(iRule, iTeam)
				
				INT iAmmoMax
				GET_MAX_AMMO(LocalPlayerPed, weapToChoose, iAmmoMax)
				INT iAmmoToGive = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sGRouletteStruct[iRule].iClips * GET_WEAPON_CLIP_SIZE(weapToChoose)
				INT iClipAmmo = GET_WEAPON_CLIP_SIZE(weapToChoose)
				
				PRINTLN("[LM][GUNSMITH] - Weapon to choose = ", GET_WEAPON_NAME(weapToChoose))
				PRINTLN("[LM][GUNSMITH] - iAmmoMax = ", iAmmoMax)
				PRINTLN("[LM][GUNSMITH] - iAmmoToGive = ", iAmmoToGive)
				PRINTLN("[LM][GUNSMITH] - Clip Ammo = ", iClipAmmo)
				
				IF HAS_PED_GOT_WEAPON(LocalPlayerPed, weapToChoose)
					PRINTLN("[LM][GUNSMITH] - Ped has weapon")
				ELSE 
					PRINTLN("[LM][GUNSMITH] - Ped does not have weapon")
				ENDIF

				// The Rule has changed, so we should check if we need to equip a new weapon.
				IF IS_BIT_SET(iLocalBoolCheck18, LBOOL18_GUNSMITH_NEW_WEAPON_READY)
				AND NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
					// If the player has not got the weapon which we're supposed to be equipping, then remove their inventory and equip them with the weapon.
					
					IF NOT HAS_NET_TIMER_STARTED(tdGunsmithWeaponEquipTimeoutFail)
						START_NET_TIMER(tdGunsmithWeaponEquipTimeoutFail)
					ENDIF
					IF HAS_NET_TIMER_STARTED(tdGunsmithWeaponEquipTimeoutFail)
						IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdGunsmithWeaponEquipTimeoutFail, 2000)
							CLEAR_BIT(iLocalBoolCheck18, LBOOL18_GUNSMITH_EQUIPPING_WEAPON)
							RESET_NET_TIMER(tdGunsmithWeaponEquipTimeoutFail)
							PRINTLN("[LM][GUNSMITH] - CALLING TIMEOUT FAIL for equipping new weapon.")
							PRINTLN("[LM][GUNSMITH] - CLEARING: LBOOL18_GUNSMITH_EQUIPPING_WEAPON")
						ENDIF
					ENDIF
					
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_UD)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_LR)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_NEXT_WEAPON)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_PREV_WEAPON)
						
					IF NOT IS_BIT_SET(iLocalBoolCheck18, LBOOL18_GUNSMITH_EQUIPPING_WEAPON)
						PRINTLN("[LM][GUNSMITH] - Unequiping weapons and giving the ped a new one.")
						IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_LoopedGunsmithLastKill)
							PRINTLN("[LM][GUNSMITH] - Removing Particle Effect.")
							REMOVE_PARTICLE_FX(ptfx_LoopedGunsmithLastKill)
						ENDIF
						
						// If our weapon does not equal the new weapon we should have, then set to play the particle effects when it is equipped.
						IF GET_SELECTED_PED_WEAPON(localPlayerPed) != weapToChoose
							SET_BIT(iLocalBoolCheck19, LBOOL19_EQUIP_REQUIRES_PTFX)
						ENDIF
			
						IF weapToChoose = WEAPONTYPE_MINIGUN
						AND IS_PED_IN_COVER(localPlayerPed)
							PRINT_HELP("HELP_MINIG", 5000)
						ENDIF
						
						REQUEST_WEAPON_ASSET(weapToChoose, ENUM_TO_INT(WRF_REQUEST_ALL_ANIMS))
						GIVE_WEAPON_TO_PED(LocalPlayerPed, weapToChoose, iClipAmmo, FALSE)
						
						SET_BIT(iLocalBoolCheck18, LBOOL18_GUNSMITH_EQUIPPING_WEAPON)
					ENDIF			
					
					// If the player now has the weapon they're supposed to, clear the bit.
					IF HAS_PED_GOT_WEAPON(LocalPlayerPed, weapToChoose)
					AND IS_BIT_SET(iLocalBoolCheck18, LBOOL18_GUNSMITH_EQUIPPING_WEAPON)
					AND HAS_WEAPON_ASSET_LOADED(weapToChoose)
						PRINTLN("[LM][GUNSMITH] - Equipping ped with weapon and ammo")
						// We are not equipping the inital weapon and we want to play the particle effects on this weapon equip.
						IF NOT IS_BIT_SET(iLocalBoolCheck19, LBOOL19_GUNSMITH_WEAPON_EQUIP_EFFECTS_BLOCKER)						
						AND IS_BIT_SET(iLocalBoolCheck19, LBOOL19_EQUIP_REQUIRES_PTFX)
							PRINTLN("[LM][GUNSMITH] - Playing Particles and Sound Effects...")
							IF IS_BIT_SET(iLocalBoolCheck18, LBOOL18_ENABLE_GUNSMITH_AUDIO_SOUNDS)
								PRINTLN("[LM][GUNSMITH][AUDIO] - LBOOL18_ENABLE_GUNSMITH_AUDIO_SOUNDS is set, determining sound to play.")
								IF GET_WEAPON_DAMAGE_TYPE(weapToChoose) = DAMAGE_TYPE_BULLET
									PLAY_SOUND_FRONTEND(-1, "Next_Level_Gun", "DLC_Biker_KQ_Sounds")
									PRINTLN("[LM][GUNSMITH][AUDIO] - Play Next_Level_Gun")
								ELIF GET_WEAPON_DAMAGE_TYPE(weapToChoose) = DAMAGE_TYPE_FIRE
								OR GET_WEAPON_DAMAGE_TYPE(weapToChoose) = DAMAGE_TYPE_EXPLOSIVE
									PLAY_SOUND_FRONTEND(-1, "Next_Level_Explosive", "DLC_Biker_KQ_Sounds")
									PRINTLN("[LM][GUNSMITH][AUDIO] - Play Next_Level_Explosive")
								ELIF GET_WEAPON_DAMAGE_TYPE(weapToChoose) = DAMAGE_TYPE_MELEE
									PLAY_SOUND_FRONTEND(-1, "Next_Level_Melee", "DLC_Biker_KQ_Sounds")
									PRINTLN("[LM][GUNSMITH][AUDIO] - Play Next_Level_Melee")
								ELSE
									PLAY_SOUND_FRONTEND(-1, "Next_Level_Generic", "DLC_Biker_KQ_Sounds")
									PRINTLN("[LM][GUNSMITH][AUDIO] - Play Next_Level_Generic")
								ENDIF
							ENDIF							
							
							FLOAT fR, fG, fB
							INT iR, iG, iB, iA
							HUD_COLOURS tempColour
							tempColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_playerBD[iLocalPart].iteam, LocalPlayer)
							GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(tempColour), iR, iG, iB, iA)
							fR = TO_FLOAT(iR)/255.0
							fG = TO_FLOAT(iG)/255.0
							fB = TO_FLOAT(iB)/255.0
							SET_PARTICLE_FX_NON_LOOPED_COLOUR(fR, fG, fB)
							SET_PARTICLE_FX_NON_LOOPED_ALPHA(0.8)
							USE_PARTICLE_FX_ASSET("scr_bike_adversary")
							START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_adversary_gunsmith_weap_change", localPlayerPed, <<0, 0, 0.0>>, <<0, 0, 0>>, BONETAG_PH_R_HAND, 0.6)
							
							CLEAR_BIT(iLocalBoolCheck19, LBOOL19_EQUIP_REQUIRES_PTFX)
						ELSE
							// We don't want to do the effects for the first equip.
							PRINTLN("[LM][GUNSMITH] - Skipping Particle and sound effects for initial equip.")
							CLEAR_BIT(iLocalBoolCheck19, LBOOL19_GUNSMITH_WEAPON_EQUIP_EFFECTS_BLOCKER)
						ENDIF
						
						IF HAS_PED_GOT_WEAPON(LocalPlayerPed, wtGunsmithPreviousWeapon)
						AND wtGunsmithPreviousWeapon != WEAPONTYPE_UNARMED
							REMOVE_WEAPON_FROM_PED(localPlayerPed, wtGunsmithPreviousWeapon)
						ENDIF
						
						SET_CURRENT_PED_WEAPON(LocalPlayerPed, weapToChoose, TRUE)
						wtGunsmithPreviousWeapon = weapToChoose
						gweapon_type_CurrentlyHeldWeapon = weapToChoose
						
						WEAPON_EQUIP_ON_RULE_CHANGE_AMMO(iTeam, iRule, weapToChoose, iAmmoMax, iAmmoToGive)

						CLEAR_BIT(iLocalBoolCheck18, LBOOL18_GUNSMITH_NEW_WEAPON_READY)
						CLEAR_BIT(iLocalBoolCheck18, LBOOL18_GUNSMITH_REPLENISH_AMMO_REQUIRED)
						CLEAR_BIT(iLocalBoolCheck18, LBOOL18_GUNSMITH_EQUIPPING_WEAPON)
						IF HAS_NET_TIMER_STARTED(tdGunsmithWeaponEquipTimeoutFail)
							RESET_NET_TIMER(tdGunsmithWeaponEquipTimeoutFail)
						ENDIF
					ENDIF
				ELSE
					// If the player has died and requires an ammo replenishment, then give the ammo to the player.
					IF IS_BIT_SET(iLocalBoolCheck18, LBOOL18_GUNSMITH_REPLENISH_AMMO_REQUIRED)
					AND HAS_PED_GOT_WEAPON(LocalPlayerPed, weapToChoose)
						PRINTLN("[LM][GUNSMITH] - Player has the correct Weapon, and they should have an Ammo replenishment")							
						WEAPON_EQUIP_ON_RULE_CHANGE_AMMO(iTeam, iRule, weapToChoose, iAmmoMax, iAmmoToGive)
						CLEAR_BIT(iLocalBoolCheck18, LBOOL18_GUNSMITH_REPLENISH_AMMO_REQUIRED)
					ENDIF	
					
					// Give the weapon if the player does not have the correct one.
					IF NOT HAS_PED_GOT_WEAPON(LocalPlayerPed, weapToChoose)
					AND NOT IS_BIT_SET(iLocalBoolCheck18, LBOOL18_GUNSMITH_NEW_WEAPON_READY)
						PRINTLN("[LM][GUNSMITH] - Ped does not have the weapon they are supposed to have.")
						SET_BIT(iLocalBoolCheck18, LBOOL18_GUNSMITH_NEW_WEAPON_READY)
						SET_BIT(iLocalBoolCheck19, LBOOL19_GUNSMITH_WEAPON_EQUIP_EFFECTS_BLOCKER)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL ARE_YOU_IN_THE_CHECKPOINT(INT iRule)
	
	VECTOR vLoc
	FLOAT fDist

	IF (iRule < FMMC_MAX_RULES)
		IF NOT IS_PED_INJURED(LocalPlayerPed)
			vLoc = GET_LOCATION_VECTOR(iRule)
			fDist = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iRule].fRadius[0] * g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iRule].fRadius[0]
			IF VDIST2(GET_ENTITY_COORDS(LocalPlayerPed),vLoc) < fDist
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
	
ENDFUNC 

FUNC BOOL IS_ANY_OTHER_TEAM_MEMBER_IN_CHECKPOINT(INT iTeam, INT iRule)

	//AND (MC_serverBD_4.iGotoLocationDataPriority[MC_playerBD[iPart].iCurrentLoc][iTeam] = iRule)

	VECTOR vLoc
	VECTOR vPlayer
	FLOAT fDist
	PLAYER_INDEX tempPlayer
	PED_INDEX tempPed

	INT iPart
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF MC_PlayerBD[iPart].iteam = iTeam
			//not you
			IF iPart != iPartToUse
				IF MC_playerBD[iPart].iCurrentLoc !=-1
					IF (iRule < FMMC_MAX_RULES)
						PARTICIPANT_INDEX Part = INT_TO_PARTICIPANTINDEX(iPart)
						IF NETWORK_IS_PARTICIPANT_ACTIVE(Part)
							IF IS_NET_PLAYER_OK(NETWORK_GET_PLAYER_INDEX(Part))
								IF (NETWORK_GET_PLAYER_INDEX(Part) != INVALID_PLAYER_INDEX())
									vLoc = GET_LOCATION_VECTOR(iRule)
									fDist = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iRule].fRadius[0] * g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iRule].fRadius[0]
									
									tempPlayer = NETWORK_GET_PLAYER_INDEX(Part)
									tempPed = GET_PLAYER_PED(tempPlayer)
									vPlayer = GET_ENTITY_COORDS(tempPed)
									
									IF VDIST2(vPlayer,vLoc) < fDist
										RETURN TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE

	
ENDFUNC 


PROC MANAGE_AUDIO_FOR_SLIPSTREAM()
	INT iTeam = MC_PlayerBD[iLocalPart].iTeam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	
		PRINTLN("[MANAGE_AUDIO_FOR_TOUR_DE_FORCE] - iRule: ", iRule)
				
		// Load the audio sounds.
		IF NOT IS_BIT_SET(iLocalBoolCheck18, LBOOL18_LOADED_TOUR_DE_FORCE_SOUNDS)
			PRINTLN("[MANAGE_AUDIO_FOR_TOUR_DE_FORCE] - Audio not loaded. Requesting audio banks")
			BOOL bLoadedAudio = FALSE
			IF REQUEST_SCRIPT_AUDIO_BANK("DLC_BIKER/BKR_SL_01")
				PRINTLN("[MANAGE_AUDIO_FOR_TOUR_DE_FORCE] - DLC_BIKER/BKR_SL_01")
				bLoadedAudio = TRUE
			ELSE
				PRINTLN("[MANAGE_AUDIO_FOR_TOUR_DE_FORCE]  - DLC_BIKER/BKR_SL_01 Not Loaded")
				bLoadedAudio = FALSE
			ENDIF
			
			IF bLoadedAudio
				IF REQUEST_SCRIPT_AUDIO_BANK("DLC_BIKER/BKR_SL_02")
					PRINTLN("[MANAGE_AUDIO_FOR_TOUR_DE_FORCE] - DLC_BIKER/BKR_SL_02")
					bLoadedAudio = TRUE
				ELSE
					PRINTLN("[MANAGE_AUDIO_FOR_TOUR_DE_FORCE] - DLC_BIKER/BKR_SL_02 Not Loaded")
					bLoadedAudio = FALSE
				ENDIF
				
				// If both of the audio banks are loaded, set the bit to stop requesting them.
				IF bLoadedAudio
					PRINTLN("[MANAGE_AUDIO_FOR_TOUR_DE_FORCE] - Setting bit: LBOOL18_LOADED_TOUR_DE_FORCE_SOUNDS")
					SET_BIT(iLocalBoolCheck18, LBOOL18_LOADED_TOUR_DE_FORCE_SOUNDS)
				ENDIF
			ENDIF
		ELSE
			
			//Boost sound
			IF IS_BIT_SET(iLocalBoolCheck18, LBOOL18_SPEED_BOOST_SUCCESS)
				IF NOT IS_BIT_SET(iLocalBoolCheck18, LBOOL18_TDF_PLAY_BOOST_SOUND)
					PRINTLN("[MANAGE_AUDIO_FOR_TOUR_DE_FORCE] - playing boost")
					PLAY_SOUND_FRONTEND(-1, "R2_Boost",  "DLC_Biker_SL_Sounds")
					SET_BIT(iLocalBoolCheck18, LBOOL18_TDF_PLAY_BOOST_SOUND)
				ENDIF
			ENDIF
			
			//Team Checkpoint
			IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_TDF_PLAY_PLAYER_ENTERS_CHECKPOINT)
				IF IS_ANY_OTHER_TEAM_MEMBER_IN_CHECKPOINT(iTeam,iRule)
					PRINTLN("[MANAGE_AUDIO_FOR_TOUR_DE_FORCE] - playing player enters checkpoint")
					PLAY_SOUND_FRONTEND(-1, "Teammate_Checkpoint",  "DLC_Biker_SL_Sounds")
					SET_BIT(iLocalBoolCheck16, LBOOL16_TDF_PLAY_PLAYER_ENTERS_CHECKPOINT)
				ELSE
					IF NOT ARE_YOU_IN_THE_CHECKPOINT(iRule)
						CLEAR_BIT(iLocalBoolCheck16, LBOOL16_TDF_PLAY_PLAYER_ENTERS_CHECKPOINT)
					ENDIF
				ENDIF
			ENDIF
			
			//Passed Checkpoint
			IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL19_SLIPSTREAM_PASSED_CHECKPOINT)
				IF IS_BIT_SET( iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam )
					PRINTLN("[MANAGE_AUDIO_FOR_TOUR_DE_FORCE] - playing passed checkpoint")
					PLAY_SOUND_FRONTEND(-1, "CHECKPOINT_PERFECT","HUD_MINI_GAME_SOUNDSET", FALSE)
					SET_BIT(iLocalBoolCheck19, LBOOL19_SLIPSTREAM_PASSED_CHECKPOINT)
				ELSE
					IF NOT ARE_YOU_IN_THE_CHECKPOINT(iRule)
						CLEAR_BIT(iLocalBoolCheck19, LBOOL19_SLIPSTREAM_PASSED_CHECKPOINT)
					ENDIF
				ENDIF
			ENDIF

			//Gain First
			IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_TDF_PLAY_TEAM_TAKES_LEAD)
				IF MC_serverBD.iWinningTeam = MC_playerBD[iPartToUse].iteam
					PRINTLN("[MANAGE_AUDIO_FOR_TOUR_DE_FORCE] - Playing take first")
					PLAY_SOUND_FRONTEND(-1, "Take_First" ,  "DLC_Biker_SL_Sounds")
					CLEAR_BIT(iLocalBoolCheck5, LBOOL5_TDF_PLAY_LOSE_FIRST)
					SET_BIT(iLocalBoolCheck16, LBOOL16_TDF_PLAY_TEAM_TAKES_LEAD)
				ENDIF
			ENDIF
			
			//Loses first
			IF NOT IS_BIT_SET(iLocalBoolCheck5, LBOOL5_TDF_PLAY_LOSE_FIRST)
				IF IS_BIT_SET(iLocalBoolCheck16, LBOOL16_TDF_PLAY_TEAM_TAKES_LEAD)
					IF MC_serverBD.iWinningTeam != MC_playerBD[iPartToUse].iteam
						PRINTLN("[MANAGE_AUDIO_FOR_TOUR_DE_FORCE] - Playing lose first")
						PLAY_SOUND_FRONTEND(-1, "Lose_First" ,  "DLC_Biker_SL_Sounds")
						SET_BIT(iLocalBoolCheck5, LBOOL5_TDF_PLAY_LOSE_FIRST)
						CLEAR_BIT(iLocalBoolCheck16, LBOOL16_TDF_PLAY_TEAM_TAKES_LEAD)
					ENDIF
				ENDIF
			ENDIF
			
			//Out of Range
			IF NOT IS_BIT_SET(iLocalBoolCheck5, LBOOL5_TDF_PLAY_OUT_OF_RANGE)
				IF IS_BIT_SET(iLocalBoolCheck, LBOOL_TDF_PLAY_IN_RANGE)
					IF NOT ARE_ALL_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE(iTeam)
						PRINTLN("[MANAGE_AUDIO_FOR_TOUR_DE_FORCE] - Playing out of range")
						PLAY_SOUND_FRONTEND(-1, "Out_Of_Range" ,  "DLC_Biker_SL_Sounds")
						SET_BIT(iLocalBoolCheck5, LBOOL5_TDF_PLAY_OUT_OF_RANGE)
						CLEAR_BIT(iLocalBoolCheck, LBOOL_TDF_PLAY_IN_RANGE)
					ENDIF
				ENDIF
			ENDIF
			
			//In Range
			IF NOT IS_BIT_SET(iLocalBoolCheck, LBOOL_TDF_PLAY_IN_RANGE)
				IF ARE_ALL_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE(iTeam)
					PRINTLN("[MANAGE_AUDIO_FOR_TOUR_DE_FORCE] - Playing in range")
					PLAY_SOUND_FRONTEND(-1,  "In_Range" ,  "DLC_Biker_SL_Sounds")
					SET_BIT(iLocalBoolCheck, LBOOL_TDF_PLAY_IN_RANGE)
					CLEAR_BIT(iLocalBoolCheck5, LBOOL5_TDF_PLAY_OUT_OF_RANGE)
				ENDIF
			ENDIF
		
		ENDIF

ENDPROC

PROC STUNT_RACE_VEHICLE_DAMAGE_SCALE(VEHICLE_INDEX viVeh)
	IF DOES_ENTITY_EXIST(viVeh)
	AND NOT IS_ENTITY_DEAD(viVeh)
	AND NETWORK_HAS_CONTROL_OF_ENTITY(viVeh)
		IF GET_ENTITY_MODEL(viVeh) = SANCTUS 
		OR GET_ENTITY_MODEL(viVeh) = AVARUS
			SET_VEHICLE_DAMAGE_SCALE(viVeh, g_sMPTunables.fStuntBikerVehDamageScale)
			PRINTLN("[PROPS] STUNT_RACE_PREP, SET_VEHICLE_DAMAGE_SCALE - g_sMPTunables.fStuntBikerVehDamageScale = ", g_sMPTunables.fStuntBikerVehDamageScale)
		ELSE
			SET_VEHICLE_DAMAGE_SCALE(viVeh, g_sMPTunables.fStuntVehDamageScale)
			PRINTLN("[PROPS] STUNT_RACE_PREP, SET_VEHICLE_DAMAGE_SCALE - g_sMPTunables.fStuntVehDamageScale = ", g_sMPTunables.fStuntVehDamageScale)
		ENDIF
	ELSE
		PRINTLN("[PROPS] STUNT_RACE_PREP, SET_VEHICLE_DAMAGE_SCALE - DOES_ENTITY_EXIST")
	ENDIF
ENDPROC

FUNC BOOL IS_ANY_KILLSTREAK_POWERUP_ACTIVE_OR_COLLECTED(BOOL bCollected, BOOL bActive)
	INT i
	FOR i = 0 TO ci_KILLSTREAK_POWERUP_MAXNUM-1
		IF bActive
			IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iKSPowerupActiveBS, i)
				PRINTLN("[LM][IS_ANY_KILLSTREAK_POWERUP_ACTIVE_OR_COLLECTED] - We have an active Killstreak Powerup: ", GET_DEBUG_STRING_FOR_KILLSTREAK_POWERUP_TYPE(i))
				RETURN TRUE
			ENDIF
		ENDIF
		IF bCollected
			IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iKSPowerupCollectedBS, i)
				PRINTLN("[LM][IS_ANY_KILLSTREAK_POWERUP_ACTIVE_OR_COLLECTED] - We are currently holding a Killstreak Powerup: ", GET_DEBUG_STRING_FOR_KILLSTREAK_POWERUP_TYPE(i))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

PROC INCREMENT_REWARD_COUNTER_KILLSTREAK_POWERUPS(INT iIndex)
	SWITCH iIndex
		CASE ci_KILLSTREAK_POWERUP_TURF_BOMB
			iKSPowerupCounterTurfBomb++
			PRINTLN("[LM][INCREMENT_REWARD_COUNTER_KILLSTREAK_POWERUPS] - Incrementing Reward Counter for: ", GET_DEBUG_STRING_FOR_KILLSTREAK_POWERUP_TYPE(iIndex), " Now equals: ", iKSPowerupCounterTurfBomb)
		BREAK
		
		CASE ci_KILLSTREAK_POWERUP_TURF_MISSILE
			iKSPowerupCounterTurfMissile++
			PRINTLN("[LM][INCREMENT_REWARD_COUNTER_KILLSTREAK_POWERUPS] - Incrementing Reward Counter for: ", GET_DEBUG_STRING_FOR_KILLSTREAK_POWERUP_TYPE(iIndex), " Now equals: ", iKSPowerupCounterTurfMissile)
		BREAK
		
		CASE ci_KILLSTREAK_POWERUP_TURF_TRADE
			iKSPowerupCounterTurfTrade++
			PRINTLN("[LM][INCREMENT_REWARD_COUNTER_KILLSTREAK_POWERUPS] - Incrementing Reward Counter for: ", GET_DEBUG_STRING_FOR_KILLSTREAK_POWERUP_TYPE(iIndex), " Now equals: ", iKSPowerupCounterTurfTrade)
		BREAK
		
		CASE ci_KILLSTREAK_POWERUP_FREEZE_POS
			iKSPowerupCounterFreezePos++
			PRINTLN("[LM][INCREMENT_REWARD_COUNTER_KILLSTREAK_POWERUPS] - Incrementing Reward Counter for: ", GET_DEBUG_STRING_FOR_KILLSTREAK_POWERUP_TYPE(iIndex), " Now equals: ", iKSPowerupCounterFreezePos)
		BREAK
	ENDSWITCH	
ENDPROC

PROC COLLECT_KILLSTREAK_POWERUP(INT iIndexToCollect, BOOL bClearCurrent = FALSE)
	IF bClearCurrent
		INT i = 0
		FOR i = 0 TO ci_KILLSTREAK_POWERUP_MAXNUM-1
			IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iKSPowerupCollectedBS, i)
				PRINTLN("[LM][COLLECT_KILLSTREAK_POWERUP] - Clearing: ", GET_DEBUG_STRING_FOR_KILLSTREAK_POWERUP_TYPE(i))
				CLEAR_BIT(MC_PlayerBD[iLocalPart].iKSPowerupCollectedBS, i)
			ENDIF
		ENDFOR
	ENDIF
	
	SET_BIT(MC_PlayerBD[iLocalPart].iKSPowerupCollectedBS, iIndexToCollect)
	PRINTLN("[LM][COLLECT_KILLSTREAK_POWERUP] - Collected: ", GET_DEBUG_STRING_FOR_KILLSTREAK_POWERUP_TYPE(iIndexToCollect))
	
	CLEAR_ALL_BIG_MESSAGES()
	
	CREATE_KILLSTREAK_POWERUP_BIG_MESSAGE(g_FMMC_STRUCT.iKSPowerupActivateType[iIndexToCollect], iIndexToCollect, g_FMMC_STRUCT.iKSPowerupActivateReq[iIndexToCollect])
	
	INCREMENT_REWARD_COUNTER_KILLSTREAK_POWERUPS(iIndexToCollect)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_KILLSTREAK_REPEAT)		
		SET_BIT(MC_PlayerBD[iLocalPart].iKSPowerupGivenBS, iIndexToCollect)
		PRINTLN("[LM][COLLECT_KILLSTREAK_POWERUP] - Setting that the player was given this powerup and won't receive it again: ", GET_DEBUG_STRING_FOR_KILLSTREAK_POWERUP_TYPE(iIndexToCollect))
	ENDIF
ENDPROC

PROC PROCESS_KILLSTREAK_POWERUPS()
	IF NOT IS_PED_INJURED(localPlayerPed)
	AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
		INT iPowerupToGive
		IF CHECK_DOES_PLAYER_QUALIFY_FOR_KILLSTREAK_POWERUP(iPowerupToGive)
			COLLECT_KILLSTREAK_POWERUP(iPowerupToGive, TRUE)
		ENDIF
		
		IF NOT IS_ANY_KILLSTREAK_POWERUP_ACTIVE_OR_COLLECTED(FALSE, TRUE)
			IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_VEH_HORN)
				PROCESS_BROADCAST_KILLSTREAK_POWERUPS()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_TURF_WAR_KILLSTREAK_POWERUP_TURF_BOMB(INT iProp)
	IF DOES_ENTITY_EXIST(oiProps[iProp])		
		IF IS_POINT_IN_CYLINDER(GET_ENTITY_COORDS(oiProps[iProp]), vTurfBombCachedPoint, 25.0, 20.0, FALSE)
			IF NOT DOES_THIS_TEAM_OWN_CLAIMED_PROP(MC_PlayerBD[iLocalPart].iteam, iProp)
			AND IS_THIS_PROP_CLAIMABLE(iProp)
				PRINTLN("[LM][TURF WAR][PROCESS_TURF_WAR_KILLSTREAK_POWERUP_TURF_BOMB] - iProp is in Radius: ", iProp)
				
				// Ipart is -1 because we don't want to increment our props claimed with this.
				BROADCAST_FMMC_CLAIM_PROP(iProp, MC_PlayerBD[iLocalPart].iteam, -1, GET_NETWORK_TIME())
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL PROCESS_TURF_WAR_KILLSTREAK_POWERUP_TURF_MISSILE()
	BOOL bPerformCheck = FALSE
	
	IF HAS_NET_TIMER_STARTED(tdKillStreakTurfMissileTimer)
		IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdKillStreakTurfMissileTimer, ci_Turf_War_Missile_Interval*iTurfMissileIncrements)
			vTurfMissileCachedPoint += vTurfMissileCachedDirection*5.0
			iTurfMissileIncrements++
			
			FLOAT fR, fG, fB
			INT iR, iG, iB, iA
			HUD_COLOURS tempColour
			tempColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_playerBD[iLocalPart].iteam, LocalPlayer)
			GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(tempColour), iR, iG, iB, iA)
			fR = TO_FLOAT(iR)/255.0
			fG = TO_FLOAT(iG)/255.0
			fB = TO_FLOAT(iB)/255.0
			SET_PARTICLE_FX_NON_LOOPED_COLOUR(fR, fG, fB)
			SET_PARTICLE_FX_NON_LOOPED_ALPHA(0.8)
			USE_PARTICLE_FX_ASSET("scr_bike_adversary")
			START_NETWORKED_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_adversary_gunsmith_weap_change", vTurfMissileCachedPoint, <<0, 0, 0.0>>, 5.0)
		ENDIF
		
		// End
		IF iTurfMissileIncrements >= 20
			RESET_NET_TIMER(tdKillStreakTurfMissileTimer)
			CLEAR_BIT(MC_PlayerBD[iLocalPart].iKSPowerupActiveBS, ci_KILLSTREAK_POWERUP_TURF_MISSILE)
		ENDIF
	ELSE
		START_NET_TIMER(tdKillStreakTurfMissileTimer)
	ENDIF
	
	RETURN bPerformCheck
ENDFUNC

//Add custom logic for specific modes if an initial count is required out of the staggered loop
FUNC BOOL PERFORM_INITIAL_SCORE_COUNT()

	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciPOINTLESS_MODE_ENABLED)
	AND HAVE_ALL_STARTING_PACKAGES_BEEN_GRABBED()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PERFORM_TURF_ARROW_CLAIM(INT iProp)
	IF DOES_ENTITY_EXIST(oiProps[iProp])
		IF IS_POINT_IN_CYLINDER(GET_ENTITY_COORDS(oiProps[iProp]), vTurfMissileCachedPoint, 15.0, 5.0, FALSE)
			IF IS_THIS_PROP_CLAIMABLE(iProp)
				PRINTLN("[LM][TURF WAR][PROCESS_TURF_WAR_KILLSTREAK_POWERUP_TURF_BOMB] - iProp is in Radius: ", iProp)
				// Ipart is -1 because we don't want to increment our props claimed with this.
				BROADCAST_FMMC_CLAIM_PROP(iProp, MC_PlayerBD[iLocalPart].iteam, -1, GET_NETWORK_TIME())
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_KILLSTREAK_POWERUP_FREEZE_POSITION()
	IF NOT HAS_NET_TIMER_STARTED(tdKillStreakFreezePosition)
		RESET_NET_TIMER(tdKillStreakFreezePosition)
		START_NET_TIMER(tdKillStreakFreezePosition)
	ELSE
		IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdKillStreakFreezePosition, ci_KillStreak_Freeze_Position)
			
			IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
				VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
				IF IS_VEHICLE_DRIVEABLE(vehPlayer)
					SET_VEHICLE_HANDBRAKE(vehPlayer, FALSE)
				ENDIF
			ENDIF
			
			CLEAR_BIT(MC_PlayerBD[iLocalPart].iKSPowerupActiveBS, ci_KILLSTREAK_POWERUP_FREEZE_POS)
		ELSE
			IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE, FALSE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HANDBRAKE, FALSE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE, FALSE)
				
				VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
				IF IS_VEHICLE_DRIVEABLE(vehPlayer)
					SET_VEHICLE_HANDBRAKE(vehPlayer, TRUE)
				ENDIF
			ELSE
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR, FALSE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD, FALSE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC REPOSITION_PICKUPS_FOR_BIRDSEYE_CAM()
	IF DOES_CAM_EXIST(g_birdsEyeCam)
		IF IS_CAM_ACTIVE(g_birdsEyeCam)
			IF IS_BIRDS_EYE_CAM_ACTIVE()
				
				// Camera is very far out for these modes.
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARZONE(g_FMMC_STRUCT.iAdversaryModeType)
				OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
					IF GET_PICKUP_GENERATION_RANGE_MULTIPLIER() < 20.0
						SET_PICKUP_GENERATION_RANGE_MULTIPLIER(20.0)
					ENDIF
					
				// Use a normal version.
				ELSE
					IF GET_PICKUP_GENERATION_RANGE_MULTIPLIER() < 4.0
						SET_PICKUP_GENERATION_RANGE_MULTIPLIER(4.0)
					ENDIF
				ENDIF
				
				FORCE_PICKUP_ROTATE_FACE_UP()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_BLIP_COLOUR_FOR_PROP_CLAIM_TEAM(INT iProp)
	SWITCH MC_ServerBD_4.ePropClaimState[iProp]
		CASE ePropClaimingState_Unclaimed			RETURN BLIP_COLOUR_WHITE
		CASE ePropClaimingState_Contested			RETURN BLIP_COLOUR_RED
		CASE ePropClaimingState_Claimed_Team_0		RETURN BLIP_COLOUR_ORANGE
		CASE ePropClaimingState_Claimed_Team_1		RETURN BLIP_COLOUR_BLUE
		CASE ePropClaimingState_Claimed_Team_2		RETURN BLIP_COLOUR_PINK
		CASE ePropClaimingState_Claimed_Team_3		RETURN BLIP_COLOUR_GREEN
	ENDSWITCH
	
	RETURN BLIP_COLOUR_YELLOW
ENDFUNC

PROC PROCESS_PROP_RADAR_BLIP(INT iProp)
		
	IF NOT DOES_BLIP_EXIST(biPropShapeBlip[iProp])
		VECTOR vPos = g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].vPos
		VECTOR vRot = GET_ENTITY_ROTATION(oiProps[iProp])
		
		IF vRot.y >= 10 OR vRot.y <= -10
		OR vRot.x >= 10 OR vRot.x <= -10
			// Ignoring these.
		ELSE
			VECTOR vDimensionsMin
			VECTOR vDimensionsMax
			FLOAT fWidth
			FLOAT fLength
			
			GET_MODEL_DIMENSIONS(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn, vDimensionsMin, vDimensionsMax)

			fLength = (vDimensionsMax.y + -vDimensionsMin.y)
			fWidth = (vDimensionsMax.x + -vDimensionsMin.x)		
			
			vDimensionsMin += vPos
			vDimensionsMax += vPos
			
			biPropShapeBlip[iProp] = ADD_BLIP_FOR_AREA(vPos, fwidth, flength)
			
			SET_BLIP_AS_SHORT_RANGE(biPropShapeBlip[iProp], TRUE)
			
			SET_BLIP_ROTATION_WITH_FLOAT(biPropShapeBlip[iProp], GET_ENTITY_HEADING(oiProps[iProp]))
			
			SET_BLIP_COLOUR(biPropShapeBlip[iProp],	GET_BLIP_COLOUR_FOR_PROP_CLAIM_TEAM(iProp))
			
			SET_BLIP_ALPHA(biPropShapeBlip[iProp], 160)
		ENDIF
	ELSE
		SET_BLIP_COLOUR(biPropShapeBlip[iProp],	GET_BLIP_COLOUR_FOR_PROP_CLAIM_TEAM(iProp))
	ENDIF
	
ENDPROC

// [FIX_2020_CONTROLLER] - A lot of these adversary mode process functions could be moved to a new "FM_Mission_Controller_AdversaryMode.sch header file.
PROC PROCESS_TURF_WAR(INT iTeam, INT iRule)
	
	// Host Broadcast when the mission is over to disable prop claiming.
	IF bIsLocalPlayerHost
		IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
			IF NOT IS_BIT_SET(iLocalBoolCheck20, LBOOL20_TURF_WAR_DISABLE_PROP_ROUND_END)
				PRINTLN("[LM][PROCESS_TURF_WAR] - Round has ended SBBOOL_MISSION_OVER - Forcing Prop Claiming off.")
				SET_BIT(iLocalBoolCheck20, LBOOL20_TURF_WAR_DISABLE_PROP_ROUND_END)
				BROADCAST_FMMC_TOGGLE_PROP_CLAIMING(FALSE, -1, -1, TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_TURF_WAR_TOP_DOWN_CAMERA)
		REPOSITION_PICKUPS_FOR_BIRDSEYE_CAM()
	ELSE
		IF GET_PICKUP_GENERATION_RANGE_MULTIPLIER() > 1.0
			SET_PICKUP_GENERATION_RANGE_MULTIPLIER(1.0)
		ENDIF
	ENDIF
	
	// Reset Props on Rule Change
	IF IS_BIT_SET( iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
		PRINTLN("[LM][TURF WAR][PROCESS_TURF_WAR] - Rule has Changed.")
		IF bIsLocalPlayerHost
			IF iRule > 0
			AND NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
				// Add score based on a Rule Change.
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_GIVE_POINTS_TURF_WAR_RULE_CHANGE)
					PRINTLN("[LM][TURF WAR][PROCESS_TURF_WAR] - Rule Changed and Creator setting is set to give teams points.")
					CALCULATE_TEAM_PROP_OWNERSHIP()
					SET_BIT(MC_serverBD.iServerBitSet4, SBBOOL4_TW_SHOULD_GIVE_POINTS)
				ENDIF

				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_RESET_TURF_WAR_PROPS_RULE_CHANGE)
					PRINTLN("[LM][TURF WAR][PROCESS_TURF_WAR] - Calling to reset the claimed props. (Rule Change) setting bit (SBBOOL4_SHOULD_RESET_ALL_PROPS)")
					SET_BIT(MC_serverBD.iServerBitSet4, SBBOOL4_TW_SHOULD_RESET_ALL_PROPS)
				ENDIF
			ENDIF
		ENDIF
		
		MC_PlayerBD[iLocalPart].iCurrentPropHit[0] = -1
		MC_PlayerBD[iLocalPart].iCurrentPropHit[1] = -1
	ENDIF
	
	// GIVE TEAM SCORES POINTS BASED ON A CUSTOM TIMER...
	IF bIsLocalPlayerHost
		IF(g_FMMC_STRUCT.iTurfWarTimeInterval*ci_TURF_WAR_SCORE_ADD_TIME) > 0
			// Start Timer
			IF NOT HAS_NET_TIMER_STARTED(MC_serverBD.tdTurfWarScoreAddTimer)
				PRINTLN("[LM][TURF WAR][PROCESS_TURF_WAR] - Initializing Timer: MC_serverBD.tdTurfWarScoreAddTimer")
				START_NET_TIMER(MC_serverBD.tdTurfWarScoreAddTimer)
			ENDIF
			
			// Timer Expired - Add score.
			IF HAS_NET_TIMER_STARTED(MC_serverBD.tdTurfWarScoreAddTimer)
				IF HAS_NET_TIMER_EXPIRED_READ_ONLY(MC_serverBD.tdTurfWarScoreAddTimer, (g_FMMC_STRUCT.iTurfWarTimeInterval*ci_TURF_WAR_SCORE_ADD_TIME))
					IF NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
					AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
						PRINTLN("[LM][TURF WAR][PROCESS_TURF_WAR] - Timer Expired: MC_serverBD.tdTurfWarScoreAddTimer")
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_TURF_WAR_CUSTOM_SCORING)
						AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciENABLE_TURF_WAR_INCREMENT_SCORE_LOGIC)
							CALCULATE_TEAM_PROP_OWNERSHIP()
							SET_BIT(MC_serverBD.iServerBitSet4, SBBOOL4_TW_SHOULD_GIVE_POINTS)
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_TURF_WAR_RESET_PROPS_ON_POINT_ALLOCATION)
							PRINTLN("[LM][TURF WAR][PROCESS_TURF_WAR] - Calling to reset the claimed props. (Custom score Logic) setting bit (SBBOOL4_SHOULD_RESET_ALL_PROPS)")
							SET_BIT(MC_serverBD.iServerBitSet4, SBBOOL4_TW_SHOULD_RESET_ALL_PROPS)
						ELSE
							PRINTLN("[LM][TURF WAR][PROCESS_TURF_WAR] - ciENABLE_TURF_WAR_RESET_PROPS_ON_POINT_ALLOCATION false, not resetting props.")
						ENDIF
					ENDIF
					
					RESET_NET_TIMER(MC_serverBD.tdTurfWarScoreAddTimer)
					START_NET_TIMER(MC_serverBD.tdTurfWarScoreAddTimer)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
				
	// Process the Shape test to find out which prop we are placed on, and add it into our desired props array.
	IF NOT IS_PED_INJURED(localPlayerPed)
	AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
	AND NOT IS_BIT_SET(iLocalBoolCheck20, LBOOL20_TURF_WAR_DISABLE_PROP_CLAIM_LOCALLY)
		PROCESS_LOCAL_PLAYER_PROP_CLAIMING()		
	ELSE
		// Play is dead. Reset his current prop claim cache.
		MC_PlayerBD[iLocalPart].iCurrentPropHit[0] = -1	
		MC_PlayerBD[iLocalPart].iCurrentPropHit[1] = -1
	ENDIF
	
	#IF IS_DEBUG_BUILD	
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_9, KEYBOARD_MODIFIER_SHIFT, "Calling bEnableWidgetPropClaimTable")
			IF bEnableWidgetPropClaimTable
				bEnableWidgetPropClaimTable = FALSE
			ELIF NOT bEnableWidgetPropClaimTable
				PRINT_PROP_CLAIMING_DATA()
				bEnableWidgetPropClaimTable = TRUE
			ENDIF 
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(bEnableWidgetPropClaimTable)
		ENDIF
	#ENDIF
	
	IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iKSPowerupActiveBS, ci_KILLSTREAK_POWERUP_TURF_MISSILE)
		PROCESS_TURF_WAR_KILLSTREAK_POWERUP_TURF_MISSILE()
	ENDIF
	
	/*INT iCountPlayers = 0
	INT iPart
	INT iPartMax = GET_NUMBER_OF_PLAYING_PLAYERS()
	IF bIsLocalPlayerHost
		IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
			FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
				iLocalPropAmountOwnedByPart[iPart] = 0
				
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, iPart))
				AND NOT IS_PARTICIPANT_A_SPECTATOR(iPart)
					iCountPlayers++
				ENDIF
				IF iCountPlayers >= iPartMax
					BREAKLOOP
				ENDIF
			ENDFOR
		ENDIF
	ENDIF*/
		
	INT iProp
	FOR iProp = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfProps-1
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_Is_Claimable)		
			// Change the Props Colour based on it's current state. Every Frame.
			HANDLE_PROP_CLAIMING_CLIENT(iProp)
			
			IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iKSPowerupActiveBS, ci_KILLSTREAK_POWERUP_TURF_BOMB)
				PROCESS_TURF_WAR_KILLSTREAK_POWERUP_TURF_BOMB(iProp)
			ENDIF
			IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iKSPowerupActiveBS, ci_KILLSTREAK_POWERUP_TURF_MISSILE)
				PERFORM_TURF_ARROW_CLAIM(iProp)
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciALLOW_RADAR_AREA_BLIPS_FOR_PROPS)
				PROCESS_PROP_RADAR_BLIP(iProp)
			ENDIF
		
			/*IF bIsLocalPlayerHost
				iCountPlayers = 0
				FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
					IF MC_ServerBD_4.iPropOwnedByPart[iProp] = iPart
						iLocalPropAmountOwnedByPart[iPart]++
					ENDIF
					
					IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, iPart))
					AND NOT IS_PARTICIPANT_A_SPECTATOR(iPart)
						iCountPlayers++
					ENDIF
					IF iCountPlayers >= iPartMax
						BREAKLOOP
					ENDIF
				ENDFOR
			ENDIF*/
		ENDIF
		// Purely DebugStuff - Shows on a window in the top LEFT the claiming state of all the props in the mission.
		#IF IS_DEBUG_BUILD		
			IF bEnableWidgetPropClaimTable
				//MAINTAIN_DEBUG_TURFWARS_INFO_PROP_STATES(iProp)
				MAINTAIN_DEBUG_TURFWARS_INFO_PROP_STATES_CONDENSED(iProp)
			ENDIF
		#ENDIF		
		// Purely DebugStuff
	ENDFOR
	
	/*#IF IS_DEBUG_BUILD		
	IF bEnableWidgetPropClaimTable
		INT iCountPlayers = 0
		INT iPart = 0
		FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
			PRINTLN("[LM][TURF WAR][PROPS OWNED] - iPart: ", iPart, " Props owned: ", MC_ServerBD_4.iPropAmountOwnedByPart[iPart])
			
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, iPart))
			AND NOT IS_PARTICIPANT_A_SPECTATOR(iPart)
				iCountPlayers++
			ENDIF
			IF iCountPlayers >= iPartMax
				BREAKLOOP
			ENDIF
		ENDFOR
	ENDIF
	#ENDIF*/
	
	IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iKSPowerupActiveBS, ci_KILLSTREAK_POWERUP_TURF_BOMB)
		CLEAR_BIT(MC_PlayerBD[iLocalPart].iKSPowerupActiveBS, ci_KILLSTREAK_POWERUP_TURF_BOMB)
	ENDIF
	
	IF GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
		FLOAT fTimeProgressed = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdTurfWarScoreAddTimer))
		FLOAT fTimeMax = TO_FLOAT(g_FMMC_STRUCT.iTurfWarTimeInterval*ci_TURF_WAR_SCORE_ADD_TIME)
		
		IF IS_BIT_SET(MC_serverBD.iServerBitSet4, SBBOOL4_SUDDEN_DEATH_TURFWAR)
		AND ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
			fTimeMax = TO_FLOAT(GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iObjectiveSuddenDeathTimeLimit[iRule]))
		ENDIF
		
		IF HAS_NET_TIMER_STARTED(MC_serverBD.tdTurfWarScoreAddTimer)
			IF NOT IS_BIT_SET(iLocalBoolCheck20, LBOOL20_START_TURFWAR_TIMER_COUNTDOWN)
				IF (fTimeMax - fTimePRogressed) <= 10000
					PRINTLN("[LM][TWHUD] - MANAGE_TURF_WAR_UI - Playing Sound 'Reset_Timer' and setting bit. ")
					
					STRING sSoundSetName
					
					sSoundSetName = "DLC_IE_TW_Player_Sounds"
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_LAND_GRAB_SOUNDS)
						sSoundSetName = "DLC_SR_LG_Player_Sounds"
					ENDIF
				
					PLAY_SOUND_FRONTEND(-1, "Reset_Timer", sSoundSetName)
					SET_BIT(iLocalBoolCheck20, LBOOL20_START_TURFWAR_TIMER_COUNTDOWN)			
				ENDIF
			ELSE
				IF (fTimeMax - fTimePRogressed) > 10000
					PRINTLN("[LM][TWHUD] - MANAGE_TURF_WAR_UI - Timer should have reset - Clearing bit.")
					CLEAR_BIT(iLocalBoolCheck20, LBOOL20_START_TURFWAR_TIMER_COUNTDOWN)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
			
	IF bPlayerToUseOK
	AND NOT IS_PLAYER_RESPAWNING(PlayerToUse)
	AND GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
		IF NOT SHOULD_HIDE_THE_HUD_THIS_FRAME()
			FLOAT fTimeProgressed
			IF HAS_NET_TIMER_STARTED(MC_serverBD.tdTurfWarScoreAddTimer)
				fTimeProgressed = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdTurfWarScoreAddTimer))
			ELSE
				fTimeProgressed = 0.0
			ENDIF
			
			FLOAT fTimeMax = TO_FLOAT(g_FMMC_STRUCT.iTurfWarTimeInterval*ci_TURF_WAR_SCORE_ADD_TIME)
			
			IF IS_BIT_SET(MC_serverBD.iServerBitSet4, SBBOOL4_SUDDEN_DEATH_TURFWAR)
			AND ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
				fTimeMax = TO_FLOAT(GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iObjectiveSuddenDeathTimeLimit[iRule]))
			ENDIF
			
			IF GET_SKYSWOOP_STAGE() >= SKYSWOOP_GOINGUP
				IF HAS_SCALEFORM_MOVIE_LOADED(SF_TWH_MovieIndex)
					IF bTurfWarScaleFormActive
						IF NOT IS_BIT_SET(iLocalBoolCheck19, LBOOL19_TW_HUD_HAS_BEEN_CLEARED_UP)
							BEGIN_SCALEFORM_MOVIE_METHOD (SF_TWH_MovieIndex, "SET_MESSAGE_VISIBILITY")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
							END_SCALEFORM_MOVIE_METHOD()
							SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(SF_TWH_MovieIndex)
							SET_BIT(iLocalBoolCheck19, LBOOL19_TW_HUD_HAS_BEEN_CLEARED_UP)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_HUD_COMPONENT_ACTIVE(NEW_HUD_WEAPON_WHEEL)
					MANAGE_TURF_WAR_UI(fTimeProgressed, fTimeMax, iPropsHighestTeamForHUD)
				ELSE
					PRINTLN("[RCC MISSION] Blocking UI while the Weapon Wheel HUD is Active.")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF bEnableWidgetPropClaimNumberCount
			MAINTAIN_DEBUG_TURFWARS_OWNERSHIP_NUMBERS()
		ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		IF bEnableWidgetPropClaimSimple
			MAINTAIN_DEBUG_TURFWARS_INFO_PROP_CACHE(iLocalPart)
		ENDIF
	#ENDIF	
ENDPROC

FUNC BOOL ARE_CONTROLS_DISABLED_BY_CURRENT_RULE()
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitset[ iRule ],ciBS_RULE_DISABLE_CONTROL)
		OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetFour[ iRule ], ciBS_RULE4_DISABLE_VEHICLE_MOVEMENT_CONTROLS )
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE

ENDFUNC

PROC FIND_EMPTY_RUINER()
	INT i
	VEHICLE_INDEX TempVeh
	FOR i = 0 TO MC_serverBD.iNumVehCreated - 1
		IF DOES_ENTITY_EXIST(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]))
			tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
			IF GET_ENTITY_MODEL(tempVeh) = RUINER2
				IF IS_VEHICLE_SEAT_FREE(tempVeh)
					IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
						TASK_ENTER_VEHICLE(LocalPlayerPed, tempVeh, 1, DEFAULT, DEFAULT, ECF_WARP_PED )
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
ENDPROC


FUNC INT PARTITION_ARRAY(INT &Array[], INT iLow, INT iHigh)
	INT i = iLow
	INT j = iHigh
	INT iPivot = g_FMMC_STRUCT.sPlacedOvertimeZones[Array[(iLow + iHigh) / 2]].iPriorityLayer
	INT iTemp
	WHILE(i <= j)
		WHILE(g_FMMC_STRUCT.sPlacedOvertimeZones[Array[i]].iPriorityLayer < iPivot)
			i++
		ENDWHILE
		WHILE (g_FMMC_STRUCT.sPlacedOvertimeZones[Array[j]].iPriorityLayer > iPivot)
			j--
		ENDWHILE
		IF (i <= j)
			iTemp = Array[i]
			Array[i] = Array[j]
			Array[j] = iTemp
			i++
			j--
		ENDIF
	ENDWHILE
	
	RETURN i
ENDFUNC

PROC SORT_OVERTIME_ZONE_ARRAY(INT &Array[], INT iLow, INT iHigh)
	INT iIndex = PARTITION_ARRAY(Array, iLow, iHigh)
	IF iLow < iIndex - 1
		SORT_OVERTIME_ZONE_ARRAY(Array, iLow, iIndex - 1)
	ENDIF
	IF iIndex < iHigh
		SORT_OVERTIME_ZONE_ARRAY(Array, iIndex, iHigh)
	ENDIF
ENDPROC

FUNC VECTOR GET_OVERTIME_CAR_POINT_VECTOR(INT iPoint, VECTOR vForward, VECTOR vPlayerPos)
	RETURN vPlayerPos + (vOvertimeCarOffsets[iPoint].y * vForward) + (vOvertimeCarOffsets[iPoint].x * <<vForward.y, -vForward.x, vForward.z>>)
ENDFUNC

#IF IS_DEBUG_BUILD
PROC DRAW_OVERTIME_DEBUG_ZONES(VEHICLE_INDEX tempVeh)
	INT i
	VECTOR vPlayerPos = GET_ENTITY_COORDS(tempVeh)
	VECTOR vForward = GET_ENTITY_FORWARD_VECTOR(tempVeh)
	FOR i = 0 TO g_FMMC_STRUCT.iNumberOfOvertimeZones - 1
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sPlacedOvertimeZones[i].iBitset, ciBitset_OvertimeZone_ShapeForCreator)
		AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sPlacedOvertimeZones[i].vPos[0])
			DRAW_DEBUG_SPHERE(g_FMMC_STRUCT.sPlacedOvertimeZones[i].vPos[0], g_FMMC_STRUCT.sPlacedOvertimeZones[i].fRadiusWidth, 255, 0, 255, 150)
		ELIF IS_BIT_SET(g_FMMC_STRUCT.sPlacedOvertimeZones[i].iBitset, ciBitset_OvertimeZone_ShapeForCreator)
		AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sPlacedOvertimeZones[i].vPos[0])
		AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sPlacedOvertimeZones[i].vPos[1])
			DRAW_DEBUG_ANGLED_AREA(g_FMMC_STRUCT.sPlacedOvertimeZones[i].vPos[0], g_FMMC_STRUCT.sPlacedOvertimeZones[i].vPos[1], g_FMMC_STRUCT.sPlacedOvertimeZones[i].fRadiusWidth, 255, 0, 255, 150)
		ENDIF
		PRINTLN("[JS WTF vPlayerPos = ", vPlayerPos)
		PRINTLN("[JS WTF vForward = ", vForward)
		INT j
		FOR j = 0 TO ciOVERTIME_CAR_MAX - 1
			VECTOR vLineBottom = GET_OVERTIME_CAR_POINT_VECTOR(j, vForward, vPlayerPos)
			vLineBottom.z -= 2.5
			VECTOR vLineTop = GET_OVERTIME_CAR_POINT_VECTOR(j, vForward, vPlayerPos)
			vLineTop.z +=2.5
			DRAW_DEBUG_LINE(vLineBottom, vLineTop)
		ENDFOR
	ENDFOR
ENDPROC
#ENDIF

FUNC BOOL IS_PLAYER_IN_PROCESSING_RANGE_OF_ZONE(INT iZone, VECTOR vPlayerPos)
	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sPlacedOvertimeZones[iZone].vPos[1])
		FLOAT fLongestRadius = PICK_FLOAT(VDIST2(g_FMMC_STRUCT.sPlacedOvertimeZones[iZone].vPos[0], g_FMMC_STRUCT.sPlacedOvertimeZones[iZone].vPos[1]) > g_FMMC_STRUCT.sPlacedOvertimeZones[iZone].fRadiusWidth, VDIST2(g_FMMC_STRUCT.sPlacedOvertimeZones[iZone].vPos[0], g_FMMC_STRUCT.sPlacedOvertimeZones[iZone].vPos[1]), g_FMMC_STRUCT.sPlacedOvertimeZones[iZone].fRadiusWidth)
		
		RETURN VDIST2(vPlayerPos, g_FMMC_STRUCT.sPlacedOvertimeZones[iZone].vPos[0]) < POW(fLongestRadius * 2, 2)
	ELSE
		RETURN VDIST2(vPlayerPos, g_FMMC_STRUCT.sPlacedOvertimeZones[iZone].vPos[0]) < POW(g_FMMC_STRUCT.sPlacedOvertimeZones[iZone].fRadiusWidth * 2, 2)
	ENDIF
	
	RETURN FALSE
ENDFUNC
// [FIX_2020_CONTROLLER] - A lot of these adversary mode process functions could be moved to a new "FM_Mission_Controller_AdversaryMode.sch header file.
FUNC INT PROCESS_OVERTIME_ZONES(VEHICLE_INDEX tempVeh)
	#IF IS_DEBUG_BUILD
		// Turn on.
		IF bEnableWidgetOvertimeZoneMarkers
			DRAW_OVERTIME_DEBUG_ZONES(tempVeh)
			
			IF NOT bWasEnableWidgetOvertimeZoneMarkers
				bWasEnableWidgetOvertimeZoneMarkers = TRUE
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
				PRINTLN("[LM][OVERTIME][PROCESS_OVERTIME_ZONES] - SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE ON via bEnableWidgetOvertimeZoneMarkers")
			ENDIF
		ENDIF
		
		// Turn off when rag widget is off.
		IF bWasEnableWidgetOvertimeZoneMarkers
		AND NOT bEnableWidgetOvertimeZoneMarkers
			bWasEnableWidgetOvertimeZoneMarkers = FALSE
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
			PRINTLN("[LM][OVERTIME][PROCESS_OVERTIME_ZONES] - SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE OFF via bEnableWidgetOvertimeZoneMarkers")
		ENDIF
	#ENDIF
	
	INT i, iMostPoints = -1, iMostPointsZone = -1, iPointInAZoneBS
	VECTOR vPlayerPos = GET_ENTITY_COORDS(tempVeh)
	VECTOR vForward = GET_ENTITY_FORWARD_VECTOR(tempVeh)
	FOR i = 0 TO g_FMMC_STRUCT.iNumberOfOvertimeZones - 1
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sPlacedOvertimeZones[iSortedOvertimeZones[i]].vPos[0])
		
			PRINTLN("[LM][OVERTIME TEMP] (Sorted Zones) iZone: ", iSortedOvertimeZones[i])
			PRINTLN("[LM][OVERTIME TEMP] (Sorted Zones) iPointsToGive: ", g_FMMC_STRUCT.sPlacedOvertimeZones[iSortedOvertimeZones[i]].iPointsToGive)


			IF IS_PLAYER_IN_PROCESSING_RANGE_OF_ZONE(iSortedOvertimeZones[i], vPlayerPos)
				INT iNumPointsInZone = 0
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sPlacedOvertimeZones[iSortedOvertimeZones[i]].iBitset, ciBitset_OvertimeZone_ShapeForCreator)
					INT j
					FOR j = 0 TO ciOVERTIME_CAR_MAX - 1
						IF NOT IS_BIT_SET(iPointInAZoneBS, j)
							IF VDIST2(GET_OVERTIME_CAR_POINT_VECTOR(j, vForward, vPlayerPos), g_FMMC_STRUCT.sPlacedOvertimeZones[iSortedOvertimeZones[i]].vPos[0]) <= POW(g_FMMC_STRUCT.sPlacedOvertimeZones[iSortedOvertimeZones[i]].fRadiusWidth, 2)								
								iNumPointsInZone++
								SET_BIT(iPointInAZoneBS, j)
							ENDIF
						ENDIF
						IF iNumPointsInZone > FLOOR(TO_FLOAT(ciOVERTIME_CAR_MAX) / 2.0)
							RETURN iSortedOvertimeZones[i]
						ENDIF
					ENDFOR
				ELIF IS_BIT_SET(g_FMMC_STRUCT.sPlacedOvertimeZones[iSortedOvertimeZones[i]].iBitset, ciBitset_OvertimeZone_ShapeForCreator)
				AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sPlacedOvertimeZones[iSortedOvertimeZones[i]].vPos[1])
					INT j
					FOR j = 0 TO ciOVERTIME_CAR_MAX - 1
						IF NOT IS_BIT_SET(iPointInAZoneBS, j)
							IF IS_POINT_IN_ANGLED_AREA(GET_OVERTIME_CAR_POINT_VECTOR(j, vForward, vPlayerPos), g_FMMC_STRUCT.sPlacedOvertimeZones[iSortedOvertimeZones[i]].vPos[0], g_FMMC_STRUCT.sPlacedOvertimeZones[iSortedOvertimeZones[i]].vPos[1], g_FMMC_STRUCT.sPlacedOvertimeZones[iSortedOvertimeZones[i]].fRadiusWidth)
								iNumPointsInZone++
								SET_BIT(iPointInAZoneBS, j)
							ENDIF
						ENDIF
						IF iNumPointsInZone > FLOOR(TO_FLOAT(ciOVERTIME_CAR_MAX) / 2.0)
							RETURN iSortedOvertimeZones[i]
						ENDIF
					ENDFOR
				ENDIF
				IF iNumPointsInZone > iMostPoints
					iMostPoints = iNumPointsInZone
					iMostPointsZone = iSortedOvertimeZones[i]
				ENDIF
			ENDIF
		ENDIF
	ENDFOR	
	IF iMostPoints = 0
		iMostPointsZone = -1
	ENDIF
	RETURN iMostPointsZone
ENDFUNC
// [FIX_2020_CONTROLLER] - A lot of these adversary mode process functions could be moved to a new "FM_Mission_Controller_AdversaryMode.sch header file.
PROC PROCESS_CURRENT_OVERTIME_ZONE()

	BOOL bCanProcess = TRUE
	
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		
		IF NOT IS_ENTITY_UPSIDEDOWN(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
			iCurrentOTZoneScoreToGive = g_FMMC_STRUCT.sPlacedOvertimeZones[iCurrentOTZone].iPointsToGive
		ENDIF
		
		IF IS_ENTITY_UPSIDEDOWN(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
			iCurrentOTZoneScoreToGive = 0
		ENDIF
		
		PRINTLN("[LM][PROCESS_OVERTIME_ZONES][OVERTIME] - Blocking vehicle controls as ciBitSet_OvertimeZone_DisableCarActions is set for this zone: ", iCurrentOTZone)
		
		#IF IS_DEBUG_BUILD
			IF bTestOvertimeZones
				bCanProcess = FALSE

				TEXT_LABEL_63 sText
				sText = ""
				sText = "Zone we are in: "
				SET_TEXT_SCALE(0.4, 0.4)
				SET_TEXT_COLOUR(50, 225, 50, 200)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.35, 0.4, "STRING", sText)
				
				sText = ""
				sText += iCurrentOTZone
				SET_TEXT_SCALE(0.4, 0.4)
				SET_TEXT_COLOUR(50, 225, 50, 200)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.6, 0.4, "STRING", sText)
				
				sText = ""
				sText = "Points for this Zone: "
				SET_TEXT_SCALE(0.4, 0.4)
				SET_TEXT_COLOUR(50, 225, 50, 200)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.35, 0.425, "STRING", sText)
				
				sText = ""
				sText += g_FMMC_STRUCT.sPlacedOvertimeZones[iCurrentOTZone].iPointsToGive
				SET_TEXT_SCALE(0.4, 0.4)
				SET_TEXT_COLOUR(50, 225, 50, 200)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.6, 0.425, "STRING", sText)
				
				
				sText = "Current Prop we are Touching (1): "
				SET_TEXT_SCALE(0.4, 0.4)
				SET_TEXT_COLOUR(50, 225, 50, 200)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.35, 0.45, "STRING", sText)
				
				sText = ""
				sText += MC_PlayerBD[iLocalPart].iCurrentPropHit[0]
				SET_TEXT_SCALE(0.4, 0.4)
				SET_TEXT_COLOUR(50, 225, 50, 200)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.625, 0.45, "STRING", sText)
				
				sText = "Current Prop we are Touching (2): "
				SET_TEXT_SCALE(0.4, 0.4)
				SET_TEXT_COLOUR(50, 225, 50, 200)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.35, 0.475, "STRING", sText)
				
				sText = ""
				sText += MC_PlayerBD[iLocalPart].iCurrentPropHit[1]
				SET_TEXT_SCALE(0.4, 0.4)
				SET_TEXT_COLOUR(50, 225, 50, 200)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.625, 0.475, "STRING", sText)
				
				sText = "Current Prop we are Touching (3): "
				SET_TEXT_SCALE(0.4, 0.4)
				SET_TEXT_COLOUR(50, 225, 50, 200)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.35, 0.5, "STRING", sText)
				
				sText = ""
				sText += MC_PlayerBD[iLocalPart].iCurrentPropHit[2]
				SET_TEXT_SCALE(0.4, 0.4)
				SET_TEXT_COLOUR(50, 225, 50, 200)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.625, 0.5, "STRING", sText)
			ENDIF
		#ENDIF
		
		IF bCanProcess
			IF IS_BIT_SET(g_FMMC_STRUCT.sPlacedOvertimeZones[iCurrentOTZone].iBitset, ciBitSet_OvertimeZone_DisableCarActions_Accelerating)
				PRINTLN("[LM][PROCESS_OVERTIME_ZONES][OVERTIME] - Disabling Accelerating")
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE, TRUE)
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sPlacedOvertimeZones[iCurrentOTZone].iBitset, ciBitSet_OvertimeZone_DisableCarActions_Braking)
				PRINTLN("[LM][PROCESS_OVERTIME_ZONES][OVERTIME] - Disabling Braking")
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HANDBRAKE, TRUE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE, TRUE)
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sPlacedOvertimeZones[iCurrentOTZone].iBitset, ciBitSet_OvertimeZone_DisableCarActions_Jumping)
				PRINTLN("[LM][PROCESS_OVERTIME_ZONES][OVERTIME] - Disabling Jump")
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SPECIAL, TRUE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_JUMP, TRUE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK, TRUE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2, TRUE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN, TRUE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CAR_JUMP, TRUE)
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sPlacedOvertimeZones[iCurrentOTZone].iBitset, ciBitSet_OvertimeZone_DisableCarActions_Steering)
			AND NOT IS_ENTITY_UPSIDEDOWN(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
				PRINTLN("[LM][PROCESS_OVERTIME_ZONES][OVERTIME] - Disabling Steering")
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_LR, TRUE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_UD, TRUE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_CARGOBOB_CATCH()
	INT iveh
	VECTOR tempLoc, vtempCoords
	OBJECT_INDEX oiMagnet
	IF NOT IS_BIT_SET(iLocalBoolCheck20,LBOOL20_CARGOBOB_CATCH_FINISHED)
		SWITCH iCargobobCatchState
			CASE ciCARGOBOB_INIT
			
				IF viCargobob != NULL AND viAttachedVeh != NULL
					PRINTLN("[JT CBC] viCargobob: ", NATIVE_TO_INT(viCargobob), " viAttachedVeh: ", NATIVE_TO_INT(viAttachedVeh))
					PRINTLN("[JT CBC] PROCESS_CARGOBOB_CATCH - PROGRESSING STATE TO ciCARGOBOB_HELI_SETUP")
					iCargobobCatchState = ciCARGOBOB_HELI_SETUP
				ENDIF
				
				FOR iVeh = 0 TO MC_serverBD.iNumVehCreated - 1
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFive, ciFMMC_VEHICLE5_IS_CARGOBOB_ATTACHED_TO_VEH)
						AND DOES_ENTITY_EXIST(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]))
							IF IS_THIS_ROCKSTAR_MISSION_SVM_RUINER2(g_FMMC_STRUCT.iRootContentIDHash)
								IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iSpawnPlayerNum = GET_ORDERED_PARTICIPANT_NUMBER(iPartToUse)
									
									viCargobob = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]) 
									IF DOES_CARGOBOB_HAVE_PICKUP_MAGNET(viCargobob)
										IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
											viAttachedVeh = GET_VEHICLE_PED_IS_IN(localPlayerPed)
										ELSE
											IF GET_PLAYERS_LAST_VEHICLE() != NULL
												viAttachedVeh = GET_PLAYERS_LAST_VEHICLE()
											ELSE
												FIND_EMPTY_RUINER()
												viAttachedVeh = GET_VEHICLE_PED_IS_IN(localPlayerPed)
											ENDIF
										ENDIF
									ENDIF
									
									iCargobobTimerLength = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCargobobDropTime * 1000
									iCargobobAltitude = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCargobobAltitude
								ENDIF
							ELSE
								IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iSpawnPlayerNum = GET_PARTICIPANT_NUMBER_IN_TEAM()
									viCargobob = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
									IF DOES_CARGOBOB_HAVE_PICKUP_MAGNET(viCargobob)
										IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
											viAttachedVeh = GET_VEHICLE_PED_IS_IN(localPlayerPed)
										ELSE
											viAttachedVeh = GET_PLAYERS_LAST_VEHICLE() 
										ENDIF
									ENDIF
									iCargobobTimerLength = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCargobobDropTime * 1000
									iCargobobAltitude = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCargobobAltitude
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
			BREAK
			
			CASE ciCARGOBOB_HELI_SETUP
				IF NETWORK_HAS_CONTROL_OF_ENTITY(viCargobob)
					IF NOT IS_BIT_SET(iLocalBoolCheck20, LBOOL20_CARGOBOB_POSITIONED)
						
						vtempCoords = GET_ENTITY_COORDS(viCargobob)
						vtempCoords.z = vtempCoords.z + iCargobobAltitude
						SET_ENTITY_COORDS(viCargobob,vtempCoords)
					
						SET_BIT(iLocalBoolCheck20, LBOOL20_CARGOBOB_POSITIONED)
					ELSE
						PRINTLN("[JT CBC] PROCESS_CARGOBOB_CATCH - PROGRESSING STATE TO ciCARGOBOB_VEH_SET_UP")
						iCargobobCatchState = ciCARGOBOB_VEH_SET_UP
					ENDIF
				ELSE
					NETWORK_REQUEST_CONTROL_OF_ENTITY(viCargobob)
				ENDIF
			BREAK
			
			CASE ciCARGOBOB_VEH_SET_UP
				IF NETWORK_HAS_CONTROL_OF_ENTITY(viAttachedVeh)
					
					IF NOT IS_PED_IN_VEHICLE(localPlayerPed,viAttachedVeh)
						TASK_ENTER_VEHICLE(LocalPlayerPed, viAttachedVeh, 1, DEFAULT, DEFAULT, ECF_WARP_PED )
					ENDIF
					IF IS_ENTITY_UPSIDEDOWN(viAttachedVeh)
						SET_VEHICLE_ON_GROUND_PROPERLY(viAttachedVeh)
					ENDIF
					
					IF NOT HAS_NET_TIMER_STARTED(MC_serverBD.stCBwaitTimer)
						IF bIsLocalPlayerHost
							IF IS_THIS_ROCKSTAR_MISSION_SVM_RUINER2(g_FMMC_STRUCT.iRootContentIDHash)
								IF IS_BIT_SET(MC_serverBD.iCutsceneStarted[0], MC_serverBD_4.iCurrentHighestPriority[0]) //Only checking team 0 as team 0 controls the cutscene in the ruiner mission.
									START_NET_TIMER(MC_serverBD.stCBwaitTimer)
								ENDIF
							ELSE
								START_NET_TIMER(MC_serverBD.stCBwaitTimer)
							ENDIF
							PRINTLN("[JT CBC] Timer started")
						ELSE
							PRINTLN("[JT CBC] Waiting for host to start timer")
						ENDIF
					ELSE
						PRINTLN("[JT CBC] PROCESS_CARGOBOB_CATCH - PROGRESSING STATE TO ciCARGOBOB_MAGNET_ACTIVATE")
						
						iCargobobCatchState = ciCARGOBOB_MAGNET_ACTIVATE
					ENDIF
				ELSE								
					NETWORK_REQUEST_CONTROL_OF_ENTITY(viAttachedVeh)
				ENDIF
			BREAK
			
			CASE ciCARGOBOB_MAGNET_ACTIVATE
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.stCBwaitTimer) > 4000 //Waiting for the magnet to be fully loaded, else game will crash trying to use GetSkeleton() on it without a skeleton
					PRINTLN("[JT CBC] Timer passed")
					IF DOES_ENTITY_EXIST(viCargobob)
					AND DOES_ENTITY_EXIST(viAttachedVeh)
						PRINTLN("[JT CBC] exists")
						IF NETWORK_HAS_CONTROL_OF_ENTITY(viCargobob)
							PRINTLN("[JT CBC] WE HAVE Control of cb")
							IF NETWORK_HAS_CONTROL_OF_ENTITY(viAttachedVeh)
								PRINTLN("[JT CBC] WE HAVE control of attach")
								IF DOES_CARGOBOB_HAVE_PICKUP_MAGNET(viCargobob)
									IF NOT HAS_NET_TIMER_STARTED(stCargobob)
										START_NET_TIMER(stCargobob)
									ENDIF
									PRINTLN("[JT CBC] WE HAVE A MAGNET")
									tempLoc = GET_ATTACHED_PICK_UP_HOOK_POSITION(viCargobob)
									oiMagnet = GET_CLOSEST_OBJECT_OF_TYPE(tempLoc, 10.0, HEI_PROP_HEIST_MAGNET, false, false, false)
									
									IF DOES_ENTITY_EXIST(oiMagnet)
										ATTACH_ENTITY_TO_ENTITY(viAttachedVeh,oiMagnet,-1,<<0,0,-2.5>>,<<0,0,0>>)
										SHUT_VEHICLE_DOORS(viAttachedVeh)
									ENDIF
									IF NOT IS_PED_IN_VEHICLE(localPlayerPed,viAttachedVeh)
										TASK_ENTER_VEHICLE(LocalPlayerPed, viAttachedVeh, 1, DEFAULT, DEFAULT, ECF_WARP_PED)
									ENDIF
									IF IS_ENTITY_ATTACHED_TO_ENTITY(viAttachedVeh,oiMagnet)
										PRINTLN("[JT CBC] PROCESS_CARGOBOB_CATCH - PROGRESSING STATE TO ciCARGOBOB_MAGNET_DEACTIVATE")
										iCargobobCatchState = ciCARGOBOB_MAGNET_DEACTIVATE
									ENDIF
								ELSE
									PRINTLN("[JT CBC] WE HAVE A MAGNET")
								ENDIF
							ELSE
								NETWORK_REQUEST_CONTROL_OF_ENTITY(viAttachedVeh)
							ENDIF								
						ELSE
							NETWORK_REQUEST_CONTROL_OF_ENTITY(viCargobob)
						ENDIF
					ELSE
						PRINTLN("[JT CBC] EXIST ELSE: cb", DOES_ENTITY_EXIST(viCargobob), " veh ", DOES_ENTITY_EXIST(viAttachedVeh))
					ENDIF
				ENDIF
			BREAK
			
			CASE ciCARGOBOB_MAGNET_DEACTIVATE
				
				IF ((HAS_NET_TIMER_STARTED(stCargobob) AND GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stCargobob) >= iCargobobTimerLength)
				OR (IS_PED_IN_VEHICLE(localPlayerPed, viAttachedVeh) AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT) AND NOT ARE_CONTROLS_DISABLED_BY_CURRENT_RULE()))
					
					tempLoc = GET_ATTACHED_PICK_UP_HOOK_POSITION(viCargobob)
					oiMagnet = GET_CLOSEST_OBJECT_OF_TYPE(tempLoc, 10.0, HEI_PROP_HEIST_MAGNET, false, false, false)
					
					SET_BIT(iLocalBoolCheck20, LBOOL20_SKIP_HELP_TEXT)
					tempLoc = GET_ENTITY_COORDS(oiMagnet)
					tempLoc.z = tempLoc.z - 3.2
					DETACH_ENTITY(viAttachedVeh, FALSE)
					SET_ENTITY_NO_COLLISION_ENTITY(viAttachedVeh, oiMagnet, FALSE)
					SET_ENTITY_COORDS(viAttachedVeh, tempLoc)
					
					PRINTLN("[JT CBC] DROP!")
					
					PRINTLN("[JT CBC] PROCESS_CARGOBOB_CATCH - PROGRESSING STATE TO ciCARGOBOB_RESET")
					
					iCargobobCatchState = ciCARGOBOB_RESET
				ENDIF
			BREAK
			CASE ciCARGOBOB_RESET
				IF HAS_NET_TIMER_STARTED(stCargobob)
					RESET_NET_TIMER(stCargobob)
					IF bIsLocalPlayerHost
						RESET_NET_TIMER(MC_serverBD.stCBwaitTimer)
					ENDIF
					SET_BIT(iLocalBoolCheck20, LBOOL20_CARGOBOB_CATCH_FINISHED)
					PRINTLN("[JT CBC] PROCESS_CARGOBOB_CATCH - SETTING BIT, LBOOL20_CARGOBOB_CATCH_FINISHED ")
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC PROCESS_CARGOBOB_ATTACHMENT()
	INT i
	FOR i = 0 TO MC_serverBD.iNumVehCreated - 1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAttachParent != -1
		AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn = CARGOBOB2
		AND NOT bDone
			PRINTLN("[JT SET CB] VALID VEH")
			IF	NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
				PRINTLN("[JT SET CB] VEHICLE VALID")
				viCargobob = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
				viAttachedVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAttachParent])
				
				VECTOR vtempCoords = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].vPos
				vtempCoords.z = vtempCoords.z + g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iCargobobAltitude
				SET_ENTITY_COORDS(viCargobob,vtempCoords)
				PRINTLN("[JT SET CB] Moved the cargobob")
				
				VECTOR hookLoc = GET_ATTACHED_PICK_UP_HOOK_POSITION(viCargobob)
				hookLoc.z -= 3.5
				SET_ENTITY_COORDS(viAttachedVeh,hookLoc)
				PRINTLN("[JT SET CB] Moved attach vehicle")
				
				iCargobobTimerLength = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iCargobobDropTime * 1000
				START_NET_TIMER(stCargobob)
				PRINTLN("[JT SET CB] Timer started")
				bDone = TRUE
			ENDIF
		ENDIF
	ENDFOR
	
	IF HAS_NET_TIMER_STARTED(stCargobob)
		IF (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stCargobob) >= iCargobobTimerLength
		OR (IS_PED_IN_VEHICLE(localPlayerPed, viAttachedVeh) AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)))
		AND NOT bDone3
			SET_CARGOBOB_PICKUP_MAGNET_ACTIVE(viCargobob, FALSE)
			SET_CARGOBOB_PICKUP_MAGNET_PULL_STRENGTH(viCargobob, 0)
			SET_CARGOBOB_PICKUP_MAGNET_STRENGTH(viCargobob, 0.1)
			SET_CARGOBOB_PICKUP_MAGNET_REDUCED_STRENGTH(viCargobob, 0.05)
			PRINTLN("[JT SET CB] DROP!")
			bDone3 = TRUE
		ELIF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stCargobob) > 200
		AND NOT bDone2
			IF DOES_ENTITY_EXIST(viCargobob)
			AND DOES_ENTITY_EXIST(viAttachedVeh)
				
				PRINTLN("[JT SET CB] GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME")
				SET_CARGOBOB_PICKUP_MAGNET_SET_TARGETED_MODE(viCargobob,viAttachedVeh)
				
				PRINTLN("[JT CARGO] ***** MAGNETS")
				SET_CARGOBOB_PICKUP_MAGNET_ACTIVE(viCargobob, TRUE)
				SET_CARGOBOB_PICKUP_MAGNET_PULL_STRENGTH(viCargobob, 1)
				SET_CARGOBOB_PICKUP_MAGNET_STRENGTH(viCargobob, 1.5) 
				SET_CARGOBOB_PICKUP_MAGNET_REDUCED_STRENGTH(viCargobob, 0.8)
				IF NOT IS_ENTITY_DEAD(viAttachedVeh)
					SET_ENTITY_PROOFS(viAttachedVeh, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, TRUE)
				ENDIF
				SET_CARGOBOB_PICKUP_MAGNET_ENSURE_PICKUP_ENTITY_UPRIGHT(viCargobob, TRUE)
				PRINTLN("[JT CARGO] HOW DO THEY WORK? ***** no really how does this work...")
				
				PED_INDEX tempPilot = GET_PED_IN_VEHICLE_SEAT(viCargobob, VS_DRIVER,TRUE)
				VECTOR vCargobobHeight = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].vPos
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPilot, TRUE)
				TASK_HELI_MISSION(tempPilot,viCargobob,NULL,NULL,g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].vPos,MISSION_GOTO,1,-1,0, ROUND(vCargobobHeight.z),ROUND(vCargobobHeight.z))

				bDone2 = TRUE
			ENDIF
		ELSE
			PRINTLN("[JT SET CB] Current time dif: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stCargobob))
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_VEHICLES_FLIPPED_BY_DUNE()
INT i
FLOAT fTime, fVelocity
	FOR i = 0 TO DUNE_MAX_FLIPS -1
		IF viFlippedVeh[i] != NULL
			IF HAS_NET_TIMER_STARTED(stFlipTimer[i])
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stFlipTimer[i]) > 5000
					viFlippedVeh[i] = NULL
					RESET_NET_TIMER(stFlipTimer[i])
					CLEAR_BIT(iLocalVehicleExplosionBitSet, (ciVeh_up_0+i))
					PRINTLN("[JT FLIP] timer over 5 seconds, clearing vehicle and timer arrays")
					BREAKLOOP
				ENDIF
			ENDIF
			IF IS_BIT_SET(iLocalVehicleExplosionBitSet, (ciVeh_up_0+i))
				IF IS_ENTITY_ALIVE(viFlippedVeh[i])
					IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stFlipTimer[i]) > 750
						IF NOT IS_ENTITY_IN_AIR(viFlippedVeh[i])
							
							IF GET_CREATOR_VEHICLE_LIBRARY_FROM_MODEL(GET_ENTITY_MODEL(viFlippedVeh[i])) != VEH_LIBRARY_MOTORCYCLES
								fTime = g_FMMC_STRUCT.fEasyExplosionTime
								fVelocity = g_FMMC_STRUCT.fEasyExplosionVelocity
								PRINTLN("CAR EET")
							ELSE
								fTime = g_FMMC_STRUCT.fEasyExplosionTimeBike
								fVelocity = g_FMMC_STRUCT.fEasyExplosionVelocityBike
								PRINTLN("BIKE EET")
							ENDIF
							
							IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stFlipTimer[i]) > (fTime * 1000)
							OR VMAG(GET_ENTITY_VELOCITY(viFlippedVeh[i])) > fVelocity
								IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(viFlippedVeh[i])
									PRINTLN("[JT FLIP] BOOM - REQUESTING CONTROL of Vehicle: ", i, " ID: ", NATIVE_TO_INT(viFlippedVeh[i]))
									NETWORK_REQUEST_CONTROL_OF_ENTITY(viFlippedVeh[i])
								ELSE
									PRINTLN("[JT FLIP] BOOM")
									PRINTLN("[JT FLIP] BOOM - Timer: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stFlipTimer[i]), " Velocity: ", (VMAG(GET_ENTITY_VELOCITY(viFlippedVeh[i]))))
									PRINTLN("[JT FLIP] BOOM - Timer set: ", g_FMMC_STRUCT.fEasyExplosionTime * 1000, "Velocity: ", g_FMMC_STRUCT.fEasyExplosionVelocity)
									RESET_NET_TIMER(stFlipTimer[i])
									
									PRINTLN("[JT FLIP] BOOM - Timer ",i ," reset: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stFlipTimer[i]))
									
									IF DOES_ENTITY_EXIST(viFlippedVeh[i])
										NETWORK_EXPLODE_VEHICLE(viFlippedVeh[i] , TRUE, TRUE)
									ENDIF
									viFlippedVeh[i] = NULL
									CLEAR_BIT(iLocalVehicleExplosionBitSet, (ciVeh_up_0+i))
									BREAKLOOP
								ENDIF
							ELSE
								PRINTLN("[JT FLIP] DUD - Resetting")
								PRINTLN("[JT FLIP] DUD - Timer: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stFlipTimer[i]), " Velocity: ", (VMAG(GET_ENTITY_VELOCITY(viFlippedVeh[i]))))
								viFlippedVeh[i] = NULL
								RESET_NET_TIMER(stFlipTimer[i])
								CLEAR_BIT(iLocalVehicleExplosionBitSet, (ciVeh_up_0+i))
								PRINTLN("[JT FLIP] BOOM - Timer ",i ," reset: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stFlipTimer[i]))
								BREAKLOOP
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_ENTITY_ALIVE(viFlippedVeh[i])
					IF IS_ENTITY_IN_AIR(viFlippedVeh[i])
						REINIT_NET_TIMER(stFlipTimer[i])
						SET_BIT(iLocalVehicleExplosionBitSet, (ciVeh_up_0+i))
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC PROCESS_UPDATE_SPECIAL_VEHICLE_WEAPONS()
	INT iTeam = MC_PlayerBD[iLocalPart].iteam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
		
	IF iTeam < FMMC_MAX_TEAMS
	AND iRule < FMMC_MAX_RULES
		
		IF IS_BIT_SET(iLocalBoolCheck6, (LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iLocalPart].iteam))
		OR IS_PED_INJURED(LocalPlayerPed)
			IF NOT IS_BIT_SET(iLocalBoolCheck22, LBOOL22_VEHICLE_AMMO_RELOAD_ENGAGED)
				PRINTLN("[LM][MISSION][PROCESS_UPDATE_SPECIAL_VEHICLE_WEAPONS] - Rule Changed. Setting vehicle Ammo Reload engaged. LBOOL22_VEHICLE_AMMO_RELOAD_ENGAGED")
				SET_BIT(iLocalBoolCheck22, LBOOL22_VEHICLE_AMMO_RELOAD_ENGAGED)				
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(iLocalBoolCheck22, LBOOL22_VEHICLE_AMMO_RELOAD_ENGAGED)
			IF NOT IS_PED_INJURED(localPlayerPed)
			AND IS_PED_IN_ANY_VEHICLE(localPlayerPed)
			
				VEHICLE_INDEX playerVeh = GET_VEHICLE_PED_IS_IN(localPlayerPed)
				WEAPON_TYPE wtCurrentRuinerWeapon
				
				IF GET_CURRENT_PED_VEHICLE_WEAPON(localPlayerPed, wtCurrentRuinerWeapon)
					IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSVWAmmoMG[iRule] > -1
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[iRule], ciBS_RULE8_SV_WEAPON_RELOAD_ON_RULE_MG)
							IF iRuinerMGAmmo = -1
								iRuinerMGAmmo++
							ENDIF
							iRuinerMGAmmo += g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSVWAmmoMG[iRule]
						ELSE
							iRuinerMGAmmo = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSVWAmmoMG[iRule]						
						ENDIF
						iRuinerMGAmmoMax = iRuinerMGAmmo
					ENDIF
					
					IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSVWAmmoRockets[iRule] > -1
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[iRule], ciBS_RULE8_SV_WEAPON_RELOAD_ON_RULE_ROCKETS)
							IF iRuinerRocketsAmmo = -1
								iRuinerRocketsAmmo++
							ENDIF
							iRuinerRocketsAmmo += g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSVWAmmoRockets[iRule]
						ELSE
							iRuinerRocketsAmmo = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSVWAmmoRockets[iRule]
						ENDIF
						iRuinerRocketsAmmoMax = iRuinerRocketsAmmo
					ENDIF
					
					IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSVWAmmoHRockets[iRule] > -1
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[iRule], ciBS_RULE8_SV_WEAPON_RELOAD_ON_RULE_HOMING_ROCKETS)
							IF iRuinerRocketsHomingAmmo = -1
								iRuinerRocketsHomingAmmo++
							ENDIF
							iRuinerRocketsHomingAmmo += g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSVWAmmoHRockets[iRule]
						ELSE
							iRuinerRocketsHomingAmmo = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSVWAmmoHRockets[iRule]
						ENDIF
						iRuinerRocketsHomingAmmoMax = iRuinerRocketsHomingAmmo
					ENDIF					
					
					IF iRuinerMGAmmo > -1
						SET_VEHICLE_WEAPON_RESTRICTED_AMMO(playerVeh, 0, iRuinerMGAmmo)							
					ENDIF					
					IF iRuinerRocketsHomingAmmo > -1
						SET_VEHICLE_WEAPON_RESTRICTED_AMMO(playerVeh, 1, iRuinerRocketsHomingAmmo)
					ENDIF
					
					SET_BIT(iLocalBoolCheck22, LBOOL22_USING_CUSTOM_VEHICLE_AMMO)
					CLEAR_BIT(iLocalBoolCheck22, LBOOL22_VEHICLE_AMMO_RELOAD_ENGAGED)
					PRINTLN("[LM][MISSION][PROCESS_UPDATE_SPECIAL_VEHICLE_WEAPONS] - (Cached) Weapon Switch. Ammos - MG: ", iRuinerMGAmmo, " Rockets: ", iRuinerRocketsAmmo, " Homing Rockets: ", iRuinerRocketsHomingAmmo)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// Rockets and Homing Rockets by default share ammo... So we have to fudge it.
PROC PROCESS_CURRENT_RUINER_ROCKET_AMMO()
	IF IS_BIT_SET(iLocalBoolCheck22, LBOOL22_USING_CUSTOM_VEHICLE_AMMO)
		IF NOT IS_PED_INJURED(localPlayerPed)
		AND IS_PED_IN_ANY_VEHICLE(localPlayerPed)
			
			VEHICLE_INDEX playerVeh = GET_VEHICLE_PED_IS_IN(localPlayerPed)
			WEAPON_TYPE wtCurrentRuinerWeapon
			
			IF GET_CURRENT_PED_VEHICLE_WEAPON(localPlayerPed, wtCurrentRuinerWeapon)
				
				IF wtCurrentRuinerWeapon = WEAPONTYPE_DLC_VEHICLE_RUINER_ROCKET
				AND IS_PLAYER_VEHICLE_WEAPON_TOGGLED_TO_NON_HOMING(PLAYER_ID())
					IF NOT IS_BIT_SET(iLocalBoolCheck22, LBOOL22_SWITCHED_ROCKET_TYPE)
						SET_BIT(iLocalBoolCheck22, LBOOL22_SWITCHED_ROCKET_TYPE)
						
						SET_BIT(iLocalBoolCheck22, LBOOL22_SWITCHED_ROCKET_AMMO)
						
						PRINTLN("[LM][MISSION][PROCESS_CURRENT_RUINER_ROCKET_AMMO] - (Bitset) Rockets Switched To Regular.")
					ENDIF
				ELIF wtCurrentRuinerWeapon = WEAPONTYPE_DLC_VEHICLE_RUINER_ROCKET
				AND NOT IS_PLAYER_VEHICLE_WEAPON_TOGGLED_TO_NON_HOMING(PLAYER_ID())
					IF IS_BIT_SET(iLocalBoolCheck22, LBOOL22_SWITCHED_ROCKET_TYPE)
						CLEAR_BIT(iLocalBoolCheck22, LBOOL22_SWITCHED_ROCKET_TYPE)
						
						SET_BIT(iLocalBoolCheck22, LBOOL22_SWITCHED_ROCKET_AMMO)
						
						PRINTLN("[LM][MISSION][PROCESS_CURRENT_RUINER_ROCKET_AMMO] - (Bitset) Rockets Switched To Homing.")
					ENDIF			
				ENDIF
				
				IF wtCurrentRuinerWeaponCache != wtCurrentRuinerWeapon
				OR IS_BIT_SET(iLocalBoolCheck22, LBOOL22_SWITCHED_ROCKET_AMMO)
				
					wtCurrentRuinerWeaponCache = wtCurrentRuinerWeapon
					
					IF wtCurrentRuinerWeapon = WEAPONTYPE_DLC_VEHICLE_RUINER_BULLET
						IF iRuinerMGAmmo > -1
							iRuinerRocketsAmmo = GET_VEHICLE_WEAPON_RESTRICTED_AMMO(playerVeh, 1)
							
							SET_VEHICLE_WEAPON_RESTRICTED_AMMO(playerVeh, 0, iRuinerMGAmmo)							
						ENDIF
						PRINTLN("[LM][MISSION][PROCESS_CURRENT_RUINER_ROCKET_AMMO] - Weapon Switched to MG")
						
					ELIF wtCurrentRuinerWeapon = WEAPONTYPE_DLC_VEHICLE_RUINER_ROCKET
					AND IS_PLAYER_VEHICLE_WEAPON_TOGGLED_TO_NON_HOMING(PLAYER_ID())
						IF iRuinerRocketsAmmo > -1
							iRuinerRocketsHomingAmmo = GET_VEHICLE_WEAPON_RESTRICTED_AMMO(playerVeh, 1)
							
							SET_VEHICLE_WEAPON_RESTRICTED_AMMO(playerVeh, 1, iRuinerRocketsAmmo)
						ENDIF
						PRINTLN("[LM][MISSION][PROCESS_CURRENT_RUINER_ROCKET_AMMO] - Weapon Switched to Rockets")
						
					ELIF wtCurrentRuinerWeapon = WEAPONTYPE_DLC_VEHICLE_RUINER_ROCKET
					AND NOT IS_PLAYER_VEHICLE_WEAPON_TOGGLED_TO_NON_HOMING(PLAYER_ID())
						IF iRuinerRocketsHomingAmmo > -1
							iRuinerMGAmmo = GET_VEHICLE_WEAPON_RESTRICTED_AMMO(playerVeh, 0)
							
							SET_VEHICLE_WEAPON_RESTRICTED_AMMO(playerVeh, 1, iRuinerRocketsHomingAmmo)
						ENDIF
						PRINTLN("[LM][MISSION][PROCESS_CURRENT_RUINER_ROCKET_AMMO] - Weapon Switched to Homing")
						
					ENDIF
					
					CLEAR_BIT(iLocalBoolCheck22, LBOOL22_SWITCHED_ROCKET_AMMO)
					
					PRINTLN("[LM][MISSION][PROCESS_CURRENT_RUINER_ROCKET_AMMO] - Weapon Switch. Ammos - MG: ", iRuinerMGAmmo, " Rockets: ", iRuinerRocketsAmmo, " Homing Rockets: ", iRuinerRocketsHomingAmmo)					
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_UPDATE_VEHICLE_HEALTH_ON_RULE()
	INT iTeam = MC_PlayerBD[iLocalPart].iteam
	
	IF iTeam < FMMC_MAX_TEAMS
		INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
		IF iRule < FMMC_MAX_RULES
			IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iVehicleHealthSwap[iRule] > 0 // just incase...
				IF sVehicleSwap.eVehicleSwapState = eVehicleSwapState_IDLE
					IF IS_BIT_SET(iLocalBoolCheck6, (LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iPartToUse].iteam))
						IF NOT IS_BIT_SET(iLocalBoolCheck21, LBOOL21_VEHICLE_HEALTH_UDPATED)
							SET_BIT(iLocalBoolCheck21, LBOOL21_VEHICLE_HEALTH_UDPATED)
						ENDIF
					ENDIF
					IF IS_BIT_SET(iLocalBoolCheck21, LBOOL21_VEHICLE_HEALTH_UDPATED)
						IF NOT IS_PED_INJURED(localPlayerPed)
						AND IS_PED_IN_ANY_VEHICLE(localPlayerPed)
						
							VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(localPlayerPed)
						
							IF IS_ENTITY_ALIVE(vehPlayer)
								PRINTLN("[LM][MISSION][PROCESS_UPDATE_VEHICLE_HEALTH_ON_RULE] - iVehicleHealthSwap is set. Applying Team Respawn Health Settings to new Vehicle. Setting: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iVehicleHealthSwap[iRule])
								SET_VEHICLE_HEALTH_FROM_RULE_AND_TEAM(iTeam, iRule, vehPlayer)
								CLEAR_BIT(iLocalBoolCheck21, LBOOL21_VEHICLE_HEALTH_UDPATED)
							ELSE
								CLEAR_BIT(iLocalBoolCheck21, LBOOL21_VEHICLE_HEALTH_UDPATED)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_ANY_VEHICLE_SWAPS_BEEN_SET()
	INT iTeam = 0
	INT iRule = 0
	
	FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
		FOR iRule = 0 TO FMMC_MAX_RULES-1
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[iRule], ciBS_RULE8_VEHICLE_SWAP_ENABLED)
				PRINTLN("[LM][MISSION][PROCESS_SWAP_VEHICLE_ON_RULE] - Using Vehicle Swaps.")
				RETURN TRUE
			ENDIF
		ENDFOR
	ENDFOR
	RETURN FALSE
ENDFUNC

PROC PROCESS_SWAP_VEHICLE_ON_RULE()
	INT iTeam = MC_PlayerBD[iLocalPart].iteam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	
	BOOL bDisableTransforming = FALSE
	
	IF iRule < FMMC_MAX_RULES
	
		//For modes where all transforming stuff should be ignored when in sudden death
		IF IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
		AND sVehicleSwap.eVehicleSwapState = eVehicleSwapState_IDLE
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRQUOTA(g_FMMC_STRUCT.iAdversaryModeType)
				PRINTLN("[TMS][VehSwap] Not starting any VehSwap functionality because we're currently in sudden death in Air Quota")
				bDisableTransforming = TRUE
			ENDIF
		ENDIF
	
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[iRule], ciBS_RULE8_VEHICLE_SWAP_ENABLED)
		AND sVehicleSwap.eVehicleSwapState = eVehicleSwapState_IDLE
			IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iLocalPart].iteam)
			AND IS_BIT_SET(iLocalBoolCheck16, LBOOL16_TEAM_0_IS_PAST_THEIR_FIRST_RULE + MC_playerBD[iLocalPart].iteam)
			AND bDisableTransforming = FALSE
				PRINTLN("[JS][VehSwap] - PROCESS_SWAP_VEHICLE_ON_RULE - Initialising Vehicle Swap. Rule: ", iRule)
				
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRQUOTA(g_FMMC_STRUCT.iAdversaryModeType)
					IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxAirQuotaLastKillEffect)
						REMOVE_PARTICLE_FX(ptfxAirQuotaLastKillEffect)
						PRINTLN("[AIRQUOTA_PTFX] - Clearing up the effect because we are swapping now!")
					ENDIF
				ENDIF
				
				IF DOES_PARTICLE_FX_LOOPED_EXIST(sVehicleSwap.ptfxID)
					REMOVE_PARTICLE_FX(sVehicleSwap.ptfxID)
					PRINTLN("[AIRQUOTA_PTFX] - Clearing up the smokey effect because we are swapping now!")
				ENDIF
				
				IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					VEHICLE_INDEX viPlayerVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
					REMOVE_PARTICLE_FX_FROM_ENTITY(viPlayerVeh) // 4003739
					REMOVE_PARTICLE_FX_FROM_ENTITY(LocalPlayerPed) // 3979316
					PRINTLN("[TMS][VehSwap] - PROCESS_SWAP_VEHICLE_ON_RULE - Removing particle FX from the vehicle the player is currently in")
				ENDIF
				
				REMOVE_PARTICLE_FX_FROM_ENTITY(LocalPlayerPed) // 3979316
				PRINTLN("[TMS][VehSwap] - PROCESS_SWAP_VEHICLE_ON_RULE - Removing particle FX from the local player's ped")
				
				SET_VEHICLE_SWAP_STATE(sVehicleSwap.eVehicleSwapState, eVehicleSwapState_INITIALISE)
			ELSE
				IF iRule < FMMC_MAX_RULES
					IF MC_serverBD_4.sVehicleSwaps[iTeam][iRule + 1].mnVehicleModelSwap != DUMMY_MODEL_FOR_SCRIPT
						MODEL_NAMES mnNextVeh = MC_serverBD_4.sVehicleSwaps[iTeam][iRule + 1].mnVehicleModelSwap
						
						PRINTLN("[VehSwapOnRule][TMS] Next vehicle is ", mnNextVeh, " pre-loading now")
						
						REQUEST_MODEL(mnNextVeh)
					ENDIF
				ENDIF
				
				IF bDisableTransforming
					PRINTLN("[VehSwap] Not starting new transform due to bDisableTransforming!")
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PLAYER_RESPAWNING(LocalPlayer)
			bReadyToSwap = FALSE
		ENDIF
		
		IF bLocalPlayerPedOK
		AND NOT IS_PLAYER_RESPAWNING(LocalPlayer)
			
			IF NOT bReadyToSwap // 4016888
				bReadyToSwap = TRUE // Delays the vehicle swapping by a frame
				PRINTLN("[TMS][VehSwap] Waiting for a frame before to swapping, setting bReadyToSwap to TRUE")
				EXIT
			ELSE
				//PRINTLN("[TMS][VehSwap] Done waiting a frame - bReadyToSwap is TRUE")
			ENDIF
		
			VEHICLE_SWAP_STATE ePreState = sVehicleSwap.eVehicleSwapState
			PROCESS_VEHICLE_SWAP(sVehicleSwap, MC_serverBD_4.sVehicleSwaps[iTeam][iRule].mnVehicleModelSwap)
			IF MC_serverBD_4.sVehicleSwaps[iTeam][iRule].mnVehicleModelSwap != DUMMY_MODEL_FOR_SCRIPT
				IF (ePreState = eVehicleSwapState_CREATE
				OR ePreState = eVehicleSwapState_INITIALISE)
				AND sVehicleSwap.eVehicleSwapState != eVehicleSwapState_CREATE
				AND sVehicleSwap.eVehicleSwapState != eVehicleSwapState_INITIALISE
					PRINTLN("[JS][VehSwap] - PROCESS_SWAP_VEHICLE_ON_RULE - Create state over, doing mission controller specific setup")
					INT iColour1, iColour2
					IF DOES_ENTITY_EXIST(sVehicleSwap.viPrevVeh)
						IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
							GET_VEHICLE_COLOURS(sVehicleSwap.viPrevVeh, iColour1, iColour2)
						ENDIF
						FMMC_SET_THIS_VEHICLE_COLOURS(sVehicleSwap.viNewVeh, iColour1, -1, -1, iColour2, iColour1)
					ENDIF
					INT iHealth = GET_FMMC_PLAYER_VEHICLE_HEALTH_FOR_TEAM(MC_playerBD[iLocalPart].iteam)
					IF iHealth >= 3000 
						SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(sVehicleSwap.viNewVeh, FALSE)
					ENDIF
					SET_ENTITY_HEALTH(sVehicleSwap.viNewVeh, iHealth)
					SET_VEHICLE_WEAPON_DAMAGE_SCALE(sVehicleSwap.viNewVeh, GET_FMMC_PLAYER_VEHICLE_WEAPON_DAMAGE_SCALE_FOR_TEAM(MC_playerBD[iLocalPart].iTeam))
					SET_VEHICLE_ENGINE_HEALTH(sVehicleSwap.viNewVeh, TO_FLOAT(iHealth))
					SET_VEHICLE_PETROL_TANK_HEALTH(sVehicleSwap.viNewVeh, TO_FLOAT(iHealth))
					SET_VEHICLE_BODY_HEALTH(sVehicleSwap.viNewVeh, TO_FLOAT(iHealth))
					IF IS_BIT_SET(MC_playerBD[iLocalPart].iVehicleWeaponEffectActiveBS, ciVEH_WEP_FLIPPED_CONTROLS)
						IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
							SET_INVERT_VEHICLE_CONTROLS(GET_VEHICLE_PED_IS_IN(LocalPlayerPed), TRUE)
						ENDIF
					ENDIF
					IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iVehicleHealthSwap[iRule] > -1							
						SET_VEHICLE_HEALTH_FROM_RULE_AND_TEAM(iTeam, iRule, sVehicleSwap.viNewVeh)
					ENDIF
					SET_VEHICLE_WEAPON_MODS_FROM_RULE_AND_TEAM(iTeam, iRule, sVehicleSwap.viNewVeh)
					
					IF sVehicleSwap.wtPrevWep != WEAPONTYPE_INVALID
						IF SET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, sVehicleSwap.wtPrevWep)
							PRINTLN("[TMS][VehSwap] Setting new vehicle's weapon type to ", sVehicleSwap.wtPrevWep)
						ELSE
							PRINTLN("[TMS][VehSwap] Failed to set vehicle's weapon type to ", sVehicleSwap.wtPrevWep)
							RETAIN_APPROXIMATE_WEAPON(sVehicleSwap, MC_serverBD_4.sVehicleSwaps[iTeam][iRule].mnVehicleModelSwap)
						ENDIF
						
						IF sVehicleSwap.bMissilesHoming
						AND sVehicleSwap.wtPrevWep != WEAPONTYPE_DLC_VEHICLE_HUNTER_BARRAGE
							PRINTLN("{TMS][VehSwap] Leaving new vehicle's weapon on 'homing'")
						ELSE
							IF MC_serverBD_4.sVehicleSwaps[iTeam][iRule].mnVehicleModelSwap = HUNTER
								SET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_VEHICLE_HUNTER_BARRAGE)
								PRINTLN("[TMS][VehSwap] Setting Hunter's weapon to Barrage rather than the homing missile")
							ELSE
								SET_PLAYER_VEHICLE_WEAPON_TO_NON_HOMING(LocalPlayer)
								PRINTLN("[TMS][VehSwap] Setting the vehicle to not use homing missiles (calling SET_PLAYER_VEHICLE_WEAPON_TO_NON_HOMING)")
							ENDIF
						ENDIF
					ENDIF
					
					IF MC_serverBD_4.sVehicleSwaps[iTeam][iRule].iVehicleAirCounterCooldown > -1
						SET_OVERRIDE_FLARE_COOLDOWN(TRUE, MC_serverBD_4.sVehicleSwaps[iTeam][iRule].iVehicleAirCounterCooldown)
						SET_OVERRIDE_CHAFF_COOLDOWN(TRUE, MC_serverBD_4.sVehicleSwaps[iTeam][iRule].iVehicleAirCounterCooldown)
						PRINTLN("[TMS][VehSwap] Setting the new vehicle's countermeasure cooldown to ", MC_serverBD_4.sVehicleSwaps[iTeam][iRule].iVehicleAirCounterCooldown)
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[iRule], ciBS_RULE8_VEHICLE_SWAP_RESPAWN_IN)
						mnPlayerSwapVehicleRespawn = MC_serverBD_4.sVehicleSwaps[iTeam][iRule].mnVehicleModelSwap
					ENDIF
					IF sVehicleSwap.eVehicleSwapState = eVehicleSwapState_APPLY_FORCES
						VS_PROCESS_APPLY_FORCES(sVehicleSwap, MC_serverBD_4.sVehicleSwaps[iTeam][iRule].mnVehicleModelSwap)
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_AIR_QUOTA_AUDIO)
						PLAY_SOUND_FROM_ENTITY(-1, "Vehicle_Transform", sVehicleSwap.viNewVeh ,"dlc_xm_aqo_sounds", TRUE)
					ENDIF
					
					IF iTransformRadioStation = -1
						//iTransformRadioStation = GET_PLAYER_RADIO_STATION_INDEX()
					ENDIF
	
					//SET_MOBILE_RADIO_ENABLED_DURING_GAMEPLAY(TRUE)
					//SET_MOBILE_PHONE_RADIO_STATE(TRUE)
					
					g_SpawnData.MissionSpawnDetails.SpawnModel = MC_serverBD_4.sVehicleSwaps[iTeam][iRule].mnVehicleModelSwap
					PRINTLN("[TMS][VehSwap] Setting g_SpawnData.MissionSpawnDetails.SpawnModel to ", MC_serverBD_4.sVehicleSwaps[iTeam][iRule].mnVehicleModelSwap)
				ENDIF
			ELSE
				IF sVehicleSwap.eVehicleSwapState != eVehicleSwapState_IDLE
					IF sVehicleSwap.eVehicleSwapState = eVehicleSwapState_CREATE
					OR sVehicleSwap.eVehicleSwapState = eVehicleSwapState_INITIALISE
					OR sVehicleSwap.eVehicleSwapState = eVehicleSwapState_APPLY_FORCES
						SET_VEHICLE_SWAP_STATE(sVehicleSwap.eVehicleSwapState, eVehicleSwapState_CLEANUP)	
					ENDIF
					PROCESS_VEHICLE_SWAP(sVehicleSwap, DUMMY_MODEL_FOR_SCRIPT)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SWAP_VEHICLE_RETRACTABLE_WHEEL_BUTTON()
	IF NOT IS_BIT_SET(iLocalBoolCheck21, LBOOL21_VEHICLE_SWAP_RETRACTABLE_WHEEL_BUTTON)
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX vehIndex = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
			IF GET_HAS_RETRACTABLE_WHEELS(vehIndex)
				IF(GET_ENTITY_MODEL(vehIndex) = BLAZER5)
					SET_VEHICLE_USE_BOOST_BUTTON_FOR_WHEEL_RETRACT(TRUE)
					SET_BIT(iLocalBoolCheck21, LBOOL21_VEHICLE_SWAP_RETRACTABLE_WHEEL_BUTTON)
					PRINTLN("[MISSION] LBOOL21_VEHICLE_SWAP_RETRACTABLE_WHEEL_BUTTON, YES ")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAVE_ALL_PLAYERS_TAKEN_A_TURN()
	INT iPart
	INT iPlayersChecked
	INT iPlayersToCheck = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF ((NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)) OR IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
		AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_PLAYER_FAIL)
		AND IS_BIT_SET(MC_playerBD[iPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
		AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_FINISHED)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
				iPlayersChecked++
				IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet3, PBBOOL3_TAKEN_A_TURN)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
		IF iPlayersChecked >= iPlayersToCheck
			BREAKLOOP
		ENDIF
	ENDFOR
	RETURN TRUE
ENDFUNC

PROC OVERTIME_WRAP_TEAM_TO_MOVE()
	IF MC_serverBD_1.iTeamToMove >= MC_serverBD.iNumberOfTeams
		IF g_FMMC_STRUCT.iOvertimeRounds != ciOVERTIME_ROUNDS_OFF
			PRINTLN("[JS] [TURNS] - OVERTIME_WRAP_TEAM_TO_MOVE - Moving TeamToMove back to 0, incrementing iCurrentRound")
			SET_BIT(iLocalBoolCheck22, LBOOL22_NEW_OT_ROUND_THIS_FRAME)
			SET_BIT(iLocalBoolCheck23, LBOOL23_FIRST_TURN_OF_NEW_ROUND)
			SET_BIT(iLocalBoolCheck23, LBOOL23_OVERTIME_SCORE_SHARD_ON_SCREEN)		
			PRINTLN("[JS] [TURNS] - OVERTIME_WRAP_TEAM_TO_MOVE - LBOOL23_OVERTIME_SCORE_SHARD_ON_SCREEN is set, caching LBOOL22_NEW_OT_ROUND_THIS_FRAME")
			SET_BIT(iLocalBoolCheck23, LBOOL23_OVERTIME_CHECK_SCORE_AFTER_SHARD)
			BROADCAST_FMMC_OVERTIME_ROUND_CHANGE_TURNS()
			
			RESET_NET_TIMER(tdOvertimeScoredShardTimer)
			START_NET_TIMER(tdOvertimeScoredShardTimer)
			
			MC_serverBD_1.iCurrentRound++				
		ENDIF
		MC_serverBD_1.iTeamToMove = 0
	ENDIF
ENDPROC

PROC PROCESS_SERVER_PLAYER_TURNS()
	IF NOT g_bMissionEnding
		IF MC_serverBD.iServerOTPartToMove = -1
			MC_serverBD.iServerOTPartToMove = OVERTIME_GET_PARTICIPANT_NUMBER_FROM_TEAM_PARTICIPANT_NUMBER(MC_serverBD_1.iTeamToMove, MC_serverBD_1.iPartToMove[MC_serverBD_1.iTeamToMove])
		ENDIF
		IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
		AND IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_EVERYONE_RUNNING)
		
			IF IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_SUDDEN_DEATH_OVERTIME_RUMBLE)
			AND IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
			AND (MC_serverBD_4.iCurrentHighestPriority[MC_serverBD_1.iTeamToMove] < g_FMMC_STRUCT.iSuddenDeathStartRule
			OR (NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_OVERTIME_SUDDEN_DEATH_REACHED_FIRST_RULE)
			AND (MC_serverBD_4.iCurrentHighestPriority[MC_serverBD_1.iTeamToMove] = g_FMMC_STRUCT.iSuddenDeathStartRule
			AND IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_serverBD_1.iTeamToMove))))
				IF MC_serverBD_4.iCurrentHighestPriority[MC_serverBD_1.iTeamToMove] = g_FMMC_STRUCT.iSuddenDeathStartRule
					SET_BIT(iLocalBoolCheck23, LBOOL23_OVERTIME_SUDDEN_DEATH_REACHED_FIRST_RULE)
				ENDIF
				EXIT
			ENDIF
			
			IF HAS_TEAM_FINISHED(MC_serverBD_1.iTeamToMove)
			OR HAS_TEAM_FAILED(MC_serverBD_1.iTeamToMove)
			OR NOT IS_TEAM_ACTIVE(MC_serverBD_1.iTeamToMove)
				PRINTLN("[JS] [TURNS] - PROCESS_SERVER_PLAYER_TURNS - Team ", MC_serverBD_1.iTeamToMove, " is in a bad state, moving to next team")
				MC_serverBD_1.iTeamToMove++
				
				IF NOT IS_BIT_SET(iLocalBoolCheck22, LBOOL22_NEW_OT_ROUND_THIS_FRAME)
					PRINTLN("[JS] [TURNS] - Clearing LBOOL23_FIRST_TURN_OF_NEW_ROUND (1)")
					CLEAR_BIT(iLocalBoolCheck23, LBOOL23_FIRST_TURN_OF_NEW_ROUND)
				ENDIF
			ENDIF
			
			OVERTIME_WRAP_TEAM_TO_MOVE()
			
			IF MC_serverBD.iServerOTPartToMove != -1
				IF (IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
				AND NOT IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_TEAM_0_SHOULD_RESPAWN_BACK_AT_START_NOW + MC_serverBD_1.iTeamToMove))
				OR NOT IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
					PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(MC_serverBD.iServerOTPartToMove)
					IF (NOT NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
					OR (IS_BIT_SET(MC_playerBD[MC_serverBD.iServerOTPartToMove].iClientBitSet, PBBOOL_ANY_SPECTATOR) AND NOT IS_BIT_SET(MC_playerBD[MC_serverBD.iServerOTPartToMove].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
					OR IS_BIT_SET(MC_playerBD[MC_serverBD.iServerOTPartToMove].iClientBitSet, PBBOOL_PLAYER_FAIL)
					OR NOT IS_BIT_SET(MC_playerBD[MC_serverBD.iServerOTPartToMove].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
					OR IS_BIT_SET(MC_playerBD[MC_serverBD.iServerOTPartToMove].iClientBitSet, PBBOOL_FINISHED)
					OR NOT IS_NET_PLAYER_OK(NETWORK_GET_PLAYER_INDEX(tempPart)))
					AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_OVERTIME_PROGRESSED_DUE_TO_DEATH)
					AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet6, SBBOOL6_OVERTIME_PROGRESSED_DUE_TO_UPSIDEDOWN)
						IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[MC_serverBD.iServerOTPartToMove].iTeam] < FMMC_MAX_RULES
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[MC_serverBD.iServerOTPartToMove].iTeam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[MC_serverBD.iServerOTPartToMove].iTeam]], ciBS_RULE8_OVERTIME_PROGRESS_ON_DEATH)
								PRINTLN("[JS] [TURNS] - PROCESS_SERVER_PLAYER_TURNS - PartToMove: ", MC_serverBD_1.iPartToMove[MC_serverBD_1.iTeamToMove], " (", MC_serverBD.iServerOTPartToMove, ") is dead, progressing rule")
								SET_PROGRESS_OBJECTIVE_FOR_TEAM(MC_playerBD[MC_serverBD.iServerOTPartToMove].iTeam, TRUE)
								SET_BIT(MC_serverBD.iServerBitSet5, SBBOOL5_OVERTIME_PROGRESSED_DUE_TO_DEATH)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF MC_serverBD.iServerOTPartToMove != -1
				IF (IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
				AND NOT IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_TEAM_0_SHOULD_RESPAWN_BACK_AT_START_NOW + MC_serverBD_1.iTeamToMove))
				OR NOT IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
					PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(MC_serverBD.iServerOTPartToMove)					
					VEHICLE_INDEX vehPlayer				
					PED_INDEX pedPlayer
					
					IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
					AND NOT IS_BIT_SET(MC_playerBD[MC_serverBD.iServerOTPartToMove].iClientBitSet, PBBOOL_ANY_SPECTATOR) 
					AND NOT IS_BIT_SET(MC_playerBD[MC_serverBD.iServerOTPartToMove].iClientBitSet, PBBOOL_HEIST_SPECTATOR)
					AND IS_BIT_SET(MC_playerBD[MC_serverBD.iServerOTPartToMove].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)					
					AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet6, SBBOOL6_OVERTIME_PROGRESSED_DUE_TO_UPSIDEDOWN)
					AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_OVERTIME_PROGRESSED_DUE_TO_DEATH)
					AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_OVERTIME_WAIT_FOR_RESET)						
						pedPlayer = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(tempPart))						
						
						IF IS_PED_IN_ANY_VEHICLE(pedPlayer)
							VehPlayer = GET_VEHICLE_PED_IS_IN(pedPlayer)								
							IF IS_ENTITY_UPSIDEDOWN(VehPlayer)
							AND NOT IS_VEHICLE_DRIVEABLE(VehPlayer)	
								IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[MC_serverBD.iServerOTPartToMove].iTeam] < FMMC_MAX_RULES								
									
									IF NOT HAS_NET_TIMER_STARTED(tdOvertimeLocateDelayForUpsideDown)									
										START_NET_TIMER(tdOvertimeLocateDelayForUpsideDown)
									ENDIF
									
									IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdOvertimeLocateDelayForUpsideDown, ci_OVERTIME_LOCATE_DELAY)
										PRINTLN("[LM] [TURNS] - PROCESS_SERVER_PLAYER_TURNS - PartToMove: ", MC_serverBD_1.iPartToMove[MC_serverBD_1.iTeamToMove], " (", MC_serverBD.iServerOTPartToMove, ") is UPSIDEDOWN and Undriveable, progressing rule")
										SET_PROGRESS_OBJECTIVE_FOR_TEAM(MC_playerBD[MC_serverBD.iServerOTPartToMove].iTeam, TRUE)
										SET_BIT(MC_serverBD.iServerBitSet6, SBBOOL6_OVERTIME_PROGRESSED_DUE_TO_UPSIDEDOWN)
										RESET_NET_TIMER(tdOvertimeLocateDelayForUpsideDown)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF MC_serverBD_1.iPartToMove[MC_serverBD_1.iTeamToMove] >= MAX_PENALTY_PLAYERS
				MC_serverBD_1.iPartToMove[MC_serverBD_1.iTeamToMove] = 0
			ENDIF
			
			IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_serverBD_1.iTeamToMove)
			AND IS_BIT_SET(iLocalBoolCheck16, LBOOL16_TEAM_0_IS_PAST_THEIR_FIRST_RULE + MC_serverBD_1.iTeamToMove)
				INT iPreviousTurnRule = MC_serverBD_4.iCurrentHighestPriority[MC_serverBD_1.iTeamToMove] - 1
				IF IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_SUDDEN_DEATH_OVERTIME_RUMBLE)
				AND IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
					IF iPreviousTurnRule < g_FMMC_STRUCT.iSuddenDeathStartRule
						iPreviousTurnRule = g_FMMC_STRUCT.sFMMCEndConditions[MC_serverBD_1.iTeamToMove].iNumberOfTeamRules - 1
					ENDIF
				ELSE
					IF iPreviousTurnRule <= -1
						iPreviousTurnRule = g_FMMC_STRUCT.sFMMCEndConditions[MC_serverBD_1.iTeamToMove].iNumberOfTeamRules - 1
					ENDIF
				ENDIF
				
				IF iPreviousTurnRule < FMMC_MAX_RULES
				AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_serverBD_1.iTeamToMove].iRuleBitsetEight[iPreviousTurnRule], ciBS_RULE8_OVERTIME_DONT_COUNT_AS_TURN)
					PRINTLN("[JS] [TURNS] - PROCESS_SERVER_PLAYER_TURNS - Team ", MC_serverBD_1.iTeamToMove, " has a new priority this frame (", MC_serverBD_4.iCurrentHighestPriority[MC_serverBD_1.iTeamToMove],"), moving to next team")
					MC_serverBD_1.iPartToMove[MC_serverBD_1.iTeamToMove]++
					IF MC_serverBD_1.iPartToMove[MC_serverBD_1.iTeamToMove] >= MAX_PENALTY_PLAYERS
						MC_serverBD_1.iPartToMove[MC_serverBD_1.iTeamToMove] = 0
					ENDIF
					MC_serverBD_1.iTeamToMove++
					CLEAR_BIT(MC_serverBD.iServerBitSet5, SBBOOL5_OVERTIME_PROGRESSED_DUE_TO_DEATH)
					CLEAR_BIT(MC_serverBD.iServerBitSet6, SBBOOL6_OVERTIME_PROGRESSED_DUE_TO_UPSIDEDOWN)
					
					OVERTIME_WRAP_TEAM_TO_MOVE()
					PRINTLN("[JS] [TURNS] - PROCESS_SERVER_PLAYER_TURNS - iTeamToMove ", MC_serverBD_1.iTeamToMove, " iPartToMove", MC_serverBD_1.iPartToMove[MC_serverBD_1.iTeamToMove])
					SET_BIT(MC_serverBD.iServerBitSet5, SBBOOL5_OVERTIME_WAIT_FOR_RESET)
					
					MC_serverBD.iServerOTPartToMove = OVERTIME_GET_PARTICIPANT_NUMBER_FROM_TEAM_PARTICIPANT_NUMBER(MC_serverBD_1.iTeamToMove, MC_serverBD_1.iPartToMove[MC_serverBD_1.iTeamToMove])
					
					IF NOT IS_BIT_SET(iLocalBoolCheck22, LBOOL22_NEW_OT_ROUND_THIS_FRAME)
						PRINTLN("[JS] [TURNS] - Clearing LBOOL23_FIRST_TURN_OF_NEW_ROUND (2)")
						CLEAR_BIT(iLocalBoolCheck23, LBOOL23_FIRST_TURN_OF_NEW_ROUND)
					ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[JS] [TURNS] - PROCESS_SERVER_PLAYER_TURNS - Team ", MC_serverBD_1.iTeamToMove, " has a new priority this frame (", MC_serverBD_4.iCurrentHighestPriority[MC_serverBD_1.iTeamToMove],") but previous rule (",iPreviousTurnRule,") set not to count as a turn")
				#ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
ENDPROC
// [FIX_2020_CONTROLLER] - A lot of these adversary mode process functions could be moved to a new "FM_Mission_Controller_AdversaryMode.sch header file.
PROC PROCESS_OVERTIME_LANDING()
	IF g_FMMC_STRUCT.iNumberOfOvertimeZones > 0
		iCurrentOTZone = -1
		IF NOT IS_PED_INJURED(localPlayerPed)
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
				IF NOT IS_ENTITY_DEAD(tempVeh)
					MODEL_NAMES mnVeh = GET_ENTITY_MODEL(tempVeh)
					IF mnVeh != mnOvertimeModelForOffsets
						mnOvertimeModelForOffsets = mnVeh
						VECTOR vMin, vMax
						GET_MODEL_DIMENSIONS(mnOvertimeModelForOffsets, vMin, vMax)
						vMin = vMin * 0.8
						vMax = vMax * 0.8
						vOvertimeCarOffsets[ciOVERTIME_CAR_MID_FRONT] = <<0, vMax.y/2, 0>>
						vOvertimeCarOffsets[ciOVERTIME_CAR_MID_BACK] = <<0, vMin.y/2, 0>>
						vOvertimeCarOffsets[ciOVERTIME_CAR_MID_LEFT] = <<vMin.x,0,0>>
						vOvertimeCarOffsets[ciOVERTIME_CAR_MID_RIGHT] = <<vMax.x,0,0>>
						vOvertimeCarOffsets[ciOVERTIME_CAR_FRONT_LEFT] = <<vMin.x, vMax.y, 0>> 
						vOvertimeCarOffsets[ciOVERTIME_CAR_FRONT_RIGHT] = <<vMax.x, vMax.y, 0>>
						vOvertimeCarOffsets[ciOVERTIME_CAR_BACK_LEFT] = <<vMin.x, vMin.y, 0>>
						vOvertimeCarOffsets[ciOVERTIME_CAR_BACK_RIGHT] = <<vMax.x, vMin.y, 0>>
					ENDIF
					IF iSortedOvertimeZones[0] = -1
						iSortedOvertimeZones[0] = 0
						SORT_OVERTIME_ZONE_ARRAY(iSortedOvertimeZones, 0, g_FMMC_STRUCT.iNumberOfOvertimeZones - 1)
					ENDIF
					
					iCurrentOTZone = PROCESS_OVERTIME_ZONES(tempVeh)
					IF iCurrentOTZone > -1
						IF MC_PlayerBD[iLocalPart].iCurrentPropHit[0] != -1
						OR MC_PlayerBD[iLocalPart].iCurrentPropHit[1] != -1
						OR MC_PlayerBD[iLocalPart].iCurrentPropHit[2] != -1
							IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam] < FMMC_MAX_RULES
								IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]], ciBS_RULE9_OVERTIME_RUMBLE_HUD)
								AND NOT IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_TEAM_0_SHOULD_RESPAWN_BACK_AT_START_NOW + MC_playerBD[iLocalPart].iTeam)
									IF MC_PlayerBD[iLocalPart].iMyTurnScore[0] != g_FMMC_STRUCT.sPlacedOvertimeZones[iCurrentOTZone].iPointsToGive
									AND NOT IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_TEAM_0_SHOULD_RESPAWN_BACK_AT_START_NOW + MC_playerBD[iLocalPart].iTeam)
									AND NOT IS_BIT_SET(iLocalBoolCheck22, LBOOL22_OVERTIME_DELAYED_SCORE_SHARD)
									AND ((NOT HAS_NET_TIMER_STARTED(tdOvertimeScoredShardTimer) AND IS_THIS_CURRENTLY_OVERTIME_TURNS()) OR IS_THIS_CURRENTLY_OVERTIME_RUMBLE())
										MC_PlayerBD[iLocalPart].iMyTurnScore[0] = g_FMMC_STRUCT.sPlacedOvertimeZones[iCurrentOTZone].iPointsToGive
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iLocalPart].iTeam)
							AND IS_THIS_CURRENTLY_OVERTIME_RUMBLE()
								PRINTLN("PROCESS_OVERTIME_LANDING 1 - resetting iMyTurnScore[",MC_serverBD_1.iPartToMove[MC_serverBD_1.iTeamToMove],"] to 0")
								MC_PlayerBD[iLocalPart].iMyTurnScore[0] = 0
							ENDIF
						ENDIF					
					ELSE
						IF NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iLocalPart].iTeam)
						AND IS_THIS_CURRENTLY_OVERTIME_RUMBLE()
							PRINTLN("PROCESS_OVERTIME_LANDING 2 - resetting iMyTurnScore[",MC_serverBD_1.iPartToMove[MC_serverBD_1.iTeamToMove],"] to 0")
							MC_PlayerBD[iLocalPart].iMyTurnScore[0] = 0
						ENDIF
					ENDIF
					
					IF IS_ENTITY_UPSIDEDOWN(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
						IF MC_PlayerBD[iLocalPart].iMyTurnScore[0] != 0
							MC_PlayerBD[iLocalPart].iMyTurnScore[0] = 0
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF iCurrentOTZone > -1
			PROCESS_CURRENT_OVERTIME_ZONE()
		ELSE
			iCurrentOTZoneScoreToGive = -1
		ENDIF
	ELSE
		PRINTLN("[LM][PROCESS_OVERTIME_ZONES][OVERTIME] - NO OVERTIME ZONES SET iNumberOfOvertimeZones: ", g_FMMC_STRUCT.iNumberOfOvertimeZones)
	ENDIF
ENDPROC

PROC PROCESS_RUINER_PARACHUTE()
	IF NOT IS_PED_INJURED(localPlayerPed)
		IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
			VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(localPlayerPed)
			
			IF GET_ENTITY_MODEL(vehPlayer) = RUINER2
			AND NETWORK_HAS_CONTROL_OF_ENTITY(vehPlayer)			
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciENABLE_TEAM_COLOURED_PARACHUTES)
					SWITCH MC_playerBD[iLocalPart].iteam
						CASE 0
							iMyParachuteMC = 0
						BREAK
						CASE 1
							iMyParachuteMC = 1
						BREAK
						CASE 2
							iMyParachuteMC = 2
						BREAK
						CASE 3
							iMyParachuteMC = 3
						BREAK
					ENDSWITCH
					VEHICLE_SET_PARACHUTE_MODEL_OVERRIDE(vehPlayer, HASH("gr_Prop_GR_Para_S_01"))
					VEHICLE_SET_PARACHUTE_MODEL_TINT_INDEX(vehPlayer, iMyParachuteMC)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_FMMC_OVERTIME_HELP_MESSAGE_POINTS_NEEDED()
	
	IF HAS_NET_TIMER_STARTED(tdOvertimePointsNeededToStayInTheGame)
	AND NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_OVERTIME_HELP_MESSAGE_PLAYED_THIS_TURN)
		IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdOvertimePointsNeededToStayInTheGame, ci_OVERTIME_POINTS_NEEDED_TIME)
			
			PRINTLN("[LM][OVERTIME] - PROCESS_FMMC_OVERTIME_HELP_MESSAGE_POINTS_NEEDED - Timer Expired")
			
			INT iMaxScore = ci_OVERTIME_ZONE_POINTS_MAX - 1
			TEXT_LABEL_15 tl15 = "OVT_SC_HLP_"
			TEXT_LABEL_15 tl15_2
			HUD_COLOURS colHud
			
			INT iTeam = 1
			INT iEnemyTeam = 0
			IF MC_playerBD[iLocalPart].iteam = iEnemyTeam
				iEnemyTeam = 1
				iTeam = 0
			ENDIF
			
			INT iThisTeamScore = MC_serverBD.iTeamScore[iTeam]
			INT iEnemyTeamScore = MC_serverBD.iTeamScore[iEnemyTeam]
			
			// Sudden Death Version.
			IF IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
				INT iDifference = (MC_serverBD.iTeamScore[MC_playerBD[iLocalPart].iteam] - MC_serverBD.iTeamScore[iEnemyTeam])
				
				PRINTLN("[LM][OVERTIME] - PROCESS_FMMC_OVERTIME_HELP_MESSAGE_POINTS_NEEDED - Sudden Death. iDifference is = ", iDifference)
				
				// invert so it appears on the shard.
				IF iDifference < 0
					iDifference *= -1
				ENDIF
				
				IF iEnemyTeamScore != iThisTeamScore
					IF MC_serverBD_1.iTeamToMove = MC_playerBD[iLocalPart].iteam // We are team 1. We need to score X amount of points in order to stay in the game etc.					
						IF iThisTeamScore < iEnemyTeamScore
							tl15 += "a"
						ELIF iThisTeamScore > iEnemyTeamScore	
							tl15 += "c"
						ENDIF
					ELSE
						IF iEnemyTeamScore > iThisTeamScore
							tl15 += "b"
							IF iEnemyTeam = 0
								tl15_2 = "OVT_ORA"
								colHud = HUD_COLOUR_ORANGE
							ELSE
								tl15_2 = "OVT_PRP"
								colHud = HUD_COLOUR_PURPLE
							ENDIF
						ELIF iEnemyTeamScore < iThisTeamScore
							tl15 += "d"
							IF iEnemyTeam = 0
								tl15_2 = "OVT_ORA"
								colHud = HUD_COLOUR_ORANGE
							ELSE
								tl15_2 = "OVT_PRP"
								colHud = HUD_COLOUR_PURPLE
							ENDIF
						ENDIF
					ENDIF
				
					IF iDifference = 1
						tl15 += 1
					ENDIF
					
					// Play Message
					INT R,G,B,A
					GET_HUD_COLOUR(colHud,R,G,B,A)
					SET_SCRIPT_VARIABLE_HUD_COLOUR(R,G,B,A)
					CLEAR_ALL_BIG_MESSAGES()
					SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING_AND_COLOUR(BIG_MESSAGE_GENERIC_MIDSIZED, iDifference, tl15, "OVT_SC_HLP_T", DEFAULT, 5000, DEFAULT, DEFAULT, colHud, Tl15_2)
				ENDIF
				SET_BIT(iLocalBoolCheck23, LBOOL23_OVERTIME_HELP_MESSAGE_PLAYED_THIS_TURN)
				RESET_NET_TIMER(tdOvertimePointsNeededToStayInTheGame)
			ELSE
				INT iRoundsRemaining = (MAX_PENALTY_PLAYERS - MC_serverBD_1.iPartToMove[iTeam])
				INT iRoundsRemainingEnemy = (MAX_PENALTY_PLAYERS - MC_serverBD_1.iPartToMove[iEnemyTeam])

				INT iTeamScoreMax = iThisTeamScore + (iRoundsRemaining * iMaxScore)
				INT iEnemyTeamScoreMax = iEnemyTeamScore + (iRoundsRemainingEnemy * iMaxScore)
				
				PRINTLN("[LM][OVERTIME] - PROCESS_FMMC_OVERTIME_HELP_MESSAGE_POINTS_NEEDED - iThisTeamScore = ", iThisTeamScore,
																						 " - iEnemyTeamScore = ", iEnemyTeamScore, 
																						 " - iRoundsRemaining = ", iRoundsRemaining,
																						 " - iRoundsRemainingEnemy = ", iRoundsRemainingEnemy,
																						 " - iTeamScoreMax = ", iTeamScoreMax,
																						 " - iEnemyTeamScoreMax = ", iEnemyTeamScoreMax)
				
				
				INT iMinimumRequired = 0
				INT iDifference = 0
				
				// Our Turn - Tells the losing player how muych it will require tpo stay in the game or be knocked out of th e game.
				IF MC_serverBD_1.iTeamToMove = MC_playerBD[iLocalPart].iteam
					FOR iDifference = 0 TO iMaxScore
					
						IF iThisTeamScore < iEnemyTeamScore
							PRINTLN("[LM][OVERTIME] - PROCESS_FMMC_OVERTIME_HELP_MESSAGE_POINTS_NEEDED - (1) Step iDifference = ", iDifference,
													 " Remainder of (iTeamScoreMax-iDifference) = ", (iTeamScoreMax-iDifference))
							
							// We Need to score X amount or else we will be knocked out.
							IF (iTeamScoreMax-iDifference) >= iEnemyTeamScore
								iMinimumRequired = iMaxScore-iDifference
								tl15 = "OVT_SC_HLP_"
								tl15 += "a"
							ENDIF
						ELIF iThisTeamScore > iEnemyTeamScore
							PRINTLN("[LM][OVERTIME] - PROCESS_FMMC_OVERTIME_HELP_MESSAGE_POINTS_NEEDED - (2) Step iDifference = ", iDifference,
													 " Remainder of (iThisTeamScore+iDifference) = ", (iThisTeamScore+iDifference))
							
							// We can knock the opponent out of the game this turn.
							IF (iThisTeamScore+iDifference) > iEnemyTeamScoreMax
								iMinimumRequired = iDifference
								tl15 += "c"
								BREAKLOOP
							ENDIF
						ENDIF
					ENDFOR
				ELSE
					FOR iDifference = 0 TO iMaxScore						
						
						IF iEnemyTeamScore > iThisTeamScore
							PRINTLN("[LM][OVERTIME] - PROCESS_FMMC_OVERTIME_HELP_MESSAGE_POINTS_NEEDED - (3) Step iDifference = ", iDifference,
													 " Remainder of (iEnemyTeamScore+iDifference) = ", (iEnemyTeamScore+iDifference))
							
							// The enemy can knock us out of the game this turn with x points.
							IF (iEnemyTeamScore+iDifference) > iTeamScoreMax
								iMinimumRequired = iDifference
								tl15 += "b"
								IF iEnemyTeam = 0
									tl15_2 = "OVT_ORA"
									colHud = HUD_COLOUR_ORANGE
								ELSE
									tl15_2 = "OVT_PRP"
									colHud = HUD_COLOUR_PURPLE
								ENDIF
								BREAKLOOP
							ENDIF
						ELIF iEnemyTeamScore < iThisTeamScore
							PRINTLN("[LM][OVERTIME] - PROCESS_FMMC_OVERTIME_HELP_MESSAGE_POINTS_NEEDED - (4) Step iDifference = ", iDifference,
													 " Remainder of (iEnemyTeamScoreMax-iDifference) = ", (iEnemyTeamScoreMax-iDifference))
							
							// The enemy will lose if they score less than X points.
							IF (iEnemyTeamScoreMax-iDifference) <= iThisTeamScore
								iMinimumRequired = iMaxScore-iDifference
								tl15 = "OVT_SC_HLP_"
								tl15 += "d"
								IF iEnemyTeam = 0
									tl15_2 = "OVT_ORA"
									colHud = HUD_COLOUR_ORANGE
								ELSE
									tl15_2 = "OVT_PRP"
									colHud = HUD_COLOUR_PURPLE
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
				
				// iMinimumRequired is now the minimum amount we need to score or else we are knocked out.
				PRINTLN("[LM][OVERTIME] - PROCESS_FMMC_OVERTIME_HELP_MESSAGE_POINTS_NEEDED - iMinimumRequired = ", iMinimumRequired)
				
				// Play Message. If the iMinimumRequired is greater than 5 then we will be losing the game and do not need to play this.
				IF iMinimumRequired <= 5
				AND iMinimumRequired > 0			
					PRINTLN("[LM][OVERTIME] - PROCESS_FMMC_OVERTIME_HELP_MESSAGE_POINTS_NEEDED - Printing Help Text")
					
					
					PRINTLN("[LM][OVERTIME] - PROCESS_FMMC_OVERTIME_HELP_MESSAGE_POINTS_NEEDED - ColHud: ", ENUM_TO_INT(colHud))
					PRINTLN("[LM][OVERTIME] - PROCESS_FMMC_OVERTIME_HELP_MESSAGE_POINTS_NEEDED - HUD_COLOUR_ENEMY: ", ENUM_TO_INT(HUD_COLOUR_ENEMY))
					PRINTLN("[LM][OVERTIME] - PROCESS_FMMC_OVERTIME_HELP_MESSAGE_POINTS_NEEDED - HUD_COLOUR_FRIENDLY: ", ENUM_TO_INT(HUD_COLOUR_FRIENDLY))
					
					IF iMinimumRequired = 1
						tl15 += 1
					ENDIF
					
					CLEAR_ALL_BIG_MESSAGES()
					
					INT R,G,B,A
					GET_HUD_COLOUR(colHud,R,G,B,A)
					SET_SCRIPT_VARIABLE_HUD_COLOUR(R,G,B,A)
					SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING_AND_COLOUR(BIG_MESSAGE_GENERIC_MIDSIZED, iMinimumRequired, tl15, "OVT_SC_HLP_T", DEFAULT, 5000, DEFAULT, DEFAULT, colHud, Tl15_2)
				ENDIF
				
				SET_BIT(iLocalBoolCheck23, LBOOL23_OVERTIME_HELP_MESSAGE_PLAYED_THIS_TURN)
				RESET_NET_TIMER(tdOvertimePointsNeededToStayInTheGame)
			ENDIF
		ENDIF
	ELIF NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_OVERTIME_HELP_MESSAGE_PLAYED_THIS_TURN)
		PRINTLN("[LM][OVERTIME] - PROCESS_FMMC_OVERTIME_HELP_MESSAGE_POINTS_NEEDED - Start Timer.")
		START_NET_TIMER(tdOvertimePointsNeededToStayInTheGame)
	ENDIF
ENDPROC
// [FIX_2020_CONTROLLER] - A lot of these adversary mode process functions could be moved to a new "FM_Mission_Controller_AdversaryMode.sch header file.
PROC PLAY_OVERTIME_ROUND_CHANGE_SHARD(BOOL bStartOfNewRoundCycle, INT iPart, INT iTeam)
	CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_LEAVE_AREA)
	CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_OO_BOUNDS)

	STRING sSoundSetName
	PRINTLN("[OVERTIME][PLAY_OVERTIME_ROUND_CHANGE_SHARD] bStartOfNewRoundCycle: ", bStartOfNewRoundCycle)
	PRINTLN("[OVERTIME][PLAY_OVERTIME_ROUND_CHANGE_SHARD] iPart: ", iPart)
	PRINTLN("[OVERTIME][PLAY_OVERTIME_ROUND_CHANGE_SHARD] iTeam: ", iTeam)
	
	IF iPart = iLocalPart 
		sSoundSetName = "DLC_GR_OT_Player_Sounds"
	ELIF iTeam = MC_PlayerBD[iLocalPart].iTeam
		sSoundSetName = "DLC_GR_OT_Team_Sounds"
	ELSE
		sSoundSetName = "DLC_GR_OT_Enemy_Sounds"
	ENDIF
	
	IF bStartOfNewRoundCycle
		// (When a new round begins)		
		// Assign Sound ID if we don't have one already.
		IF iOTSoundNewRound = -1
			iOTSoundNewRound = GET_SOUND_ID()
		ENDIF
		
		PLAY_SOUND_FRONTEND(iOTSoundNewRound, "Round_End", sSoundSetName)
		SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, "OVT_SC_PLA_NR", DEFAULT, DEFAULT, (BIG_MESSAGE_DISPLAY_TIME + 1500))
	ELSE
		// (When a new turn is set)
		
		PLAYER_INDEX tempPlayer
		PARTICIPANT_INDEX tempPart
		
		INT iPartForShard = iPart		
		
		IF iPartForShard > -1
			tempPart = INT_TO_PARTICIPANTINDEX(iPartForShard)
					
			IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
				
				
				HUD_COLOURS hcPlayerTeamColour
				
				IF MC_PlayerBD[iPartForShard].iTeam = 0
					hcPlayerTeamColour = HUD_COLOUR_ORANGE
				ELSE
					hcPlayerTeamColour = HUD_COLOUR_PURPLE
				ENDIF
				
				// We don't want the New Turn Sound to play, if it's a new round.
				IF HAS_SOUND_FINISHED(iOTSoundNewRound)
					PLAY_SOUND_FRONTEND(-1, "Round_Start", sSoundSetName)	
				ENDIF
				
				IF iPartForShard = iLocalPart 
					INT iEnemyTeam = 0
					IF MC_playerBD[iPartToUse].iteam = iEnemyTeam
						iEnemyTeam = 1
					ENDIF
					INT iDifference = (MC_serverBD.iTeamScore[MC_playerBD[iPartToUse].iteam] - MC_serverBD.iTeamScore[iEnemyTeam])
					IF iDifference > -1
						TEXT_LABEL_15 tl15 = "OVT_SC_PLA_NTb"
						IF iDifference = 0
							tl15 = "OVT_SC_PLA_TIE"
						ELIF iDifference = 1
							tl15 = "OVT_SC_PLA_NTb1"
						ENDIF
						SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_GENERIC_TEXT, iDifference, tl15, "OVT_SC_PLA_NT", hcPlayerTeamColour, (BIG_MESSAGE_DISPLAY_TIME + 1500))
					ELSE
						TEXT_LABEL_15 tl15 = "OVT_SC_PLA_NTc"
						IF iDifference = 1
							tl15 = "OVT_SC_PLA_NTc1"
						ENDIF
						SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_GENERIC_TEXT, (iDifference * -1) + 1, tl15, "OVT_SC_PLA_NT", hcPlayerTeamColour, (BIG_MESSAGE_DISPLAY_TIME + 1500))
					ENDIF
				ELSE
					SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GENERIC_TEXT, tempPlayer, 0, "OVT_SC_PLA_NTa", "OVT_SC_PLA_NT", hcPlayerTeamColour, (BIG_MESSAGE_DISPLAY_TIME + 1500))
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_OVERTIME_STUCK_VEHICLE()
	IF IS_PED_IN_ANY_VEHICLE(localPlayerPed, FALSE)
		VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(localPlayerPed)
		
		IF DOES_ENTITY_EXIST(vehPlayer)
		AND NOT IS_ENTITY_IN_AIR(vehPlayer)
			IF IS_VEHICLE_DRIVEABLE(vehPlayer)
				IF IS_VEHICLE_STUCK_TIMER_UP(vehPlayer, VEH_STUCK_ON_ROOF, ci_OVERTIME_STUCK_VEHICLE)
				OR IS_VEHICLE_STUCK_TIMER_UP(vehPlayer, VEH_STUCK_ON_SIDE, ci_OVERTIME_STUCK_VEHICLE)
					PRINTLN("[LM][PROCESS_OVERTIME_EVERY_FRAME] - Setting local Player vehicle to undriveable as they have been stuck in overtime for too long.")
					SET_VEHICLE_UNDRIVEABLE(vehPlayer, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_FMMC_OVERTIME_HELP_MESSAGE_ON_ROOF()
	IF NOT IS_PED_INJURED(localPlayerPed)
		IF IS_PED_IN_ANY_VEHICLE(localPlayerPed, FALSE)
			VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(localPlayerPed)		
			IF DOES_ENTITY_EXIST(vehPlayer)
			AND NOT IS_ENTITY_IN_AIR(vehPlayer)
			AND NOT IS_ENTITY_IN_WATER(vehPlayer)
				IF IS_VEHICLE_DRIVEABLE(vehPlayer)
					IF IS_ENTITY_UPSIDEDOWN(vehPlayer)
						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()					
							PRINT_HELP("OVT_ROOF_HLP", 6000)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC
// [FIX_2020_CONTROLLER] - A lot of these adversary mode process functions could be moved to a new "FM_Mission_Controller_AdversaryMode.sch header file.
PROC PROCESS_OVERTIME_EVERY_FRAME()

	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)		
		SET_RADIO_TO_STATION_INDEX(255)
		IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
			VEHICLE_INDEX viTemp = GET_VEHICLE_PED_IS_IN(localPlayerPed)
			IF IS_ENTITY_A_MISSION_ENTITY(viTemp) //b* 2677017
				IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(viTemp, FALSE) // url:bugstar:3641934
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(VEH_TO_NET(viTemp))
						SET_VEHICLE_RADIO_ENABLED(viTemp, FALSE)				
						g_s_PlayersRadioStation = ""
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
		
		IF HAS_NET_TIMER_STARTED(tdOvertimeRenderphasePauseStart)	
			INT iTimeToCheck = ciOVERTIME_RENDERPHASE_PAUSE_START		
			IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_ANY_SPECTATOR) 
				iTimeToCheck = ciOVERTIME_RENDERPHASE_PAUSE_START_SPEC
			ENDIF	
			
			IF NOT GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()
				PRINTLN("PROCESS_FMMC_OVERTIME_PAUSE_RENDERPHASE - tdOvertimeRenderphasePauseStart was active but we have already paused!")
				RESET_NET_TIMER(tdOvertimeRenderphasePauseStart)
			ENDIF
			
			IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdOvertimeRenderphasePauseStart, iTimeToCheck)
				PRINTLN("PROCESS_FMMC_OVERTIME_PAUSE_RENDERPHASE - PAUSING THE RENDERPHASE (ACTUAL PAUSE)")
				
				// This needs to be here twice. ############ DO NOT REMOVE IT ############# !
				TOGGLE_PAUSED_RENDERPHASES(FALSE)
				TOGGLE_PAUSED_RENDERPHASES(FALSE)
				
				RESET_NET_TIMER(tdOvertimeRenderphasePauseStart)
			ENDIF
		ENDIF
		
		// url:bugstar:3617977 url:bugstar:3632178
		IF NOT GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()
		OR IS_BIT_SET(iLocalBoolCheck23, LBOOL23_OVERTIME_SCORE_SHARD_ON_SCREEN)
		OR IS_BIT_SET(iLocalBoolCheck22, LBOOL22_OVERTIME_DELAYED_SCORE_SHARD)
		OR (HAS_NET_TIMER_STARTED(tdOvertimeScoredShardTimer) AND NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(tdOvertimeScoredShardTimer, ci_OVERTIME_SCORED_SHARD_TIME))
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_ALL_OVERHEADS)
			Delete_MP_Objective_Text()
		ENDIF
	
		IF NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
			IF IS_BIT_SET(g_iMissionMiscBitset1, ciMMBITSET_OVERTIME_PLAYER_LEFT_CLEAR_AREA_REQUIRED)
				IF iTeamSlotOvertimeCache > -1
					IF HAS_NET_TIMER_STARTED(tdOvertimeClearAreaTimer)			
						IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdOvertimeClearAreaTimer, ciOVERTIME_CLEAR_AREA_TIME)				
							PRINTLN("[LM][OVERTIME][PLAYER_LEFT] - Clearing Area, as a player has left the game.")
							INT iPlayerTeam = GET_PLAYER_TEAM_FOR_INTRO()
							VECTOR vStartPos = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[ iPlayerTeam ][ iTeamSlotOvertimeCache ].vPos + <<0,0,1>>
							
							CLEAR_AREA(vStartPos, 100.0, TRUE, TRUE, TRUE, FALSE)
						
							iOvertimeClearAreaCounter++
							
							IF iOvertimeClearAreaCounter = 4
								iOvertimeClearAreaCounter = 0
								RESET_NET_TIMER(tdOvertimeClearAreaTimer)
								CLEAR_BIT(g_iMissionMiscBitset1, ciMMBITSET_OVERTIME_PLAYER_LEFT_CLEAR_AREA_REQUIRED)
							ELSE
								RESET_NET_TIMER(tdOvertimeClearAreaTimer)
								START_NET_TIMER(tdOvertimeClearAreaTimer)
							ENDIF
						ENDIF				
					ENDIF
				ELSE
					PRINTLN("[LM][OVERTIME][PLAYER_LEFT] - Clearing Area, as a player has left the game.")
					START_NET_TIMER(tdOvertimeClearAreaTimer)
				ENDIF
			ENDIF
		ENDIF
		
		IF DID_I_JOIN_MISSION_AS_SPECTATOR()
		AND iPartToUse > -1
			IF IS_BIT_SET(iLocalBoolCheck24, LBOOL24_SPECTATOR_RENDERPHASE_PAUSED_VIA_EVENT)			
				IF HAS_NET_TIMER_STARTED(tdOvertimeRenderphaseUnpauseForSpectators)			
					IF NOT IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_TEAM_0_SHOULD_RESPAWN_BACK_AT_START_NOW + MC_PlayerBD[iPartToUse].iTeam)
					AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdOvertimeRenderphaseUnpauseForSpectators, ciOVERTIME_RENDERPHASE_UNPAUSE_FOR_SPECTATORS)					
						IF NOT GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()
							PRINTLN("[LM][OVERTIME] Spectator is unpausing renderphase LBOOL24_SPECTATOR_RENDERPHASE_PAUSED_VIA_EVENT")
							TOGGLE_PAUSED_RENDERPHASES(TRUE)
							TOGGLE_PAUSED_RENDERPHASES(TRUE)
						ELSE
							PRINTLN("[LM][OVERTIME] Spectator Something else unpaused us. Clearing timer and bit. LBOOL24_SPECTATOR_RENDERPHASE_PAUSED_VIA_EVENT")
							CLEAR_BIT(iLocalBoolCheck24, LBOOL24_SPECTATOR_RENDERPHASE_PAUSED_VIA_EVENT)		
							RESET_NET_TIMER(tdOvertimeRenderphaseUnpauseForSpectators)
						ENDIF				
					ELSE
						PRINTLN("[LM][OVERTIME] Spectator is renderphase paused LBOOL24_SPECTATOR_RENDERPHASE_PAUSED_VIA_EVENT - waiting for players to respawn.")
					ENDIF
				ELSE
					PRINTLN("[LM][OVERTIME] LBOOL24_SPECTATOR_RENDERPHASE_PAUSED_VIA_EVENT Bitset but timer hasn't started.")
				ENDIF
			ENDIF
		ELSE
			// If we are a Spectator, but we did not join as one, AND we died in Rumble, then we want to unpause our renderphase. Else we want to just let the process back at restart stuff sort it out.
			IF NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
			AND IS_LOCAL_PLAYER_ANY_SPECTATOR()
				IF IS_BIT_SET(iLocalBoolCheck24, LBOOL24_RENDERPHASE_PAUSED_VIA_RUMBLE_DEATH)
					IF HAS_NET_TIMER_STARTED(tdOvertimeRenderphaseUnpauseForRumbleDeath)	
						IF NOT IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_TEAM_0_SHOULD_RESPAWN_BACK_AT_START_NOW + MC_PlayerBD[iPartToUse].iTeam)
						AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdOvertimeRenderphaseUnpauseForRumbleDeath, ciOVERTIME_RENDERPHASE_UNPAUSE_FOR_SPECTATORS)
							IF iSpectatorTarget != -1
								IF NOT GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()
									PRINTLN("[LM][OVERTIME] Dead Rumble Player Unpausing renderphase LBOOL24_SPECTATOR_RENDERPHASE_PAUSED_VIA_EVENT")
									TOGGLE_PAUSED_RENDERPHASES(TRUE)
									TOGGLE_PAUSED_RENDERPHASES(TRUE)
									RESET_NET_TIMER(tdOvertimeRenderphaseUnpauseForRumbleDeath)
								ELSE
									CLEAR_BIT(iLocalBoolCheck24, LBOOL24_RENDERPHASE_PAUSED_VIA_RUMBLE_DEATH)							
									RESET_NET_TIMER(tdOvertimeRenderphaseUnpauseForRumbleDeath)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		// We don't want this leftover / queued message playing as a spectator.
		IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
			IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_OO_BOUNDS)
			OR IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_LEAVE_AREA)
			OR IS_BIG_MESSAGE_ALREADY_REQUESTED(BIG_MESSAGE_OO_BOUNDS)
			OR IS_BIG_MESSAGE_ALREADY_REQUESTED(BIG_MESSAGE_LEAVE_AREA)
				CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_OO_BOUNDS)
				CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_LEAVE_AREA)
			ENDIF
		ENDIF
	ENDIF
		
	// To stop fringe case bugs such as url:bugstar:3519596
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER, TRUE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT, TRUE)
	
	PROCESS_OVERTIME_STUCK_VEHICLE()
	
	IF MC_PlayerBD[iLocalPart].iCurrentOTZoneBD != iCurrentOTZone
		MC_PlayerBD[iLocalPart].iCurrentOTZoneBD = iCurrentOTZone
	ENDIF

	IF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciOVERTIME_WAIT_YOUR_TURN)
		AND iOTPartToMove = iLocalPart)
	OR NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciOVERTIME_WAIT_YOUR_TURN)
		PROCESS_OVERTIME_LANDING()
	ENDIF	
	
	IF NOT IS_PLAYER_SCTV(LocalPlayer)
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_OT_START_SPECTATING)
			IF CAN_SPECTATOR_QUIT()
				SET_SPECTATOR_CAN_QUIT(FALSE)
			ENDIF
			IF NOT IS_BIT_SET(g_BossSpecData.specHUDData.iBitset, SPEC_HUD_BS_BLOCK_CIRCLE_INSTRUCTION)
				SET_SPECTATOR_HUD_BLOCK_CIRCLE_INSTRUCTION(g_BossSpecData.specHUDData, TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	INT iTeam = MC_playerBD[iLocalPart].iteam
			
	IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
		CLEAR_BIT(MC_PlayerBD[iLocalPart].iClientBitset3, PBBOOL3_OVERTIME_LANDED_SUCCESSFULLY)
		CLEAR_BIT(iLocalBoolCheck22, LBOOL22_OVERTIME_LANDING_FAILED)
	ENDIF
	
	BOOL bShouldHideHud = SHOULD_HIDE_THE_HUD_THIS_FRAME()
	
	IF bShouldHideHud
		PRINTLN("[LM][OVERTIME] - HUD HIDDEN - bShouldHideHud Set Reason: (0)")
	ENDIF
	
	bShouldHideHud = SHOULD_OVERTIME_HUD_ELEMENTS_SHOW(bShouldHideHud)
	
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)		
		IF IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_OVERTIME_SUDDEN_DEATH_ROUNDS)
			IF NOT IS_BIT_SET(iLocalBoolCheck3,LBOOL3_DISPLAY_SUDDEN_DEATH)
				INT i
				FOR i = 0 TO MAX_PENALTY_PLAYERS - 1
					MC_PlayerBD[iLocalPart].iMyTurnScore[i] = -1
				ENDFOR
				SET_BIT(iLocalBoolCheck3,LBOOL3_DISPLAY_SUDDEN_DEATH)
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF bIsLocalPlayerHost
				IF IS_THIS_CURRENTLY_OVERTIME_TURNS()
				AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_OVERTIME_SUDDEN_DEATH_ROUNDS)
					IF bPutMeInSuddenDeath
						PRINTLN("[JS] DEBUG - FORCING PLAYERS INTO SUDDEN DEATH!")
						MC_serverBD_1.iCurrentRound = MAX_PENALTY_PLAYERS
					ENDIF
				ENDIF
			ENDIF
		#ENDIF
		IF iOTPartToMove > -1 // This should prevent rare cases of asserts caused by -1 being passed into the function.
			IF NOT bShouldHideHud
				IF IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_OVERTIME_SUDDEN_DEATH_ROUNDS)
					IF NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_OVERTIME_SUDDEN_DEATH_SHARD)
						IF NOT HAS_TEAM_FAILED(MC_playerBD[iPartToUse].iteam)
						AND ARE_MULTIPLE_TEAMS_ON_SAME_SCORE()
							SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, "CTF_SD", "SUDDTH_PTL")	
							PRINTLN("[LM][OVERTIME] - Playing Sudden Death Shard")
						ENDIF
						CLEAR_BIT(iLocalBoolCheck22, LBOOL22_OVERTIME_DELAYED_SCORE_SHARD)
						SET_BIT(iLocalBoolCheck23, LBOOL23_OVERTIME_SUDDEN_DEATH_SHARD)
					ENDIF
					IF iOTPartToMove = iLocalPart
						IF NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_OVERTIME_SUDDEN_DEATH_HELP_SHOWN)
						AND NOT IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_TEAM_0_SHOULD_RESPAWN_BACK_AT_START_NOW + MC_playerBD[iLocalPart].iTeam)
							PRINT_HELP("SUDDTH_OT", 7500)
							SET_BIT(iLocalBoolCheck23, LBOOL23_OVERTIME_SUDDEN_DEATH_HELP_SHOWN)
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(iLocalboolCheck22, LBOOL22_OVERTIME_NEW_TURN_SHARD)
				AND NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GENERIC_TEXT)
					IF IS_BIT_SET(iLocalBoolCheck22, LBOOL22_OVERTIME_ROUND_START_SHARD)
						PRINTLN("[LM][OVERTIME] - (1) iPart: ", iLocalPart, " is host, broadcasting new round. iOTPartToMove: ", iOTPartToMove)
						PLAY_OVERTIME_ROUND_CHANGE_SHARD(TRUE, iOTPartToMove, MC_PlayerBD[iOTPartToMove].iTeam)
						CLEAR_BIT(iLocalBoolCheck22, LBOOL22_OVERTIME_ROUND_START_SHARD)
					ENDIF
				ENDIF
				
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciOVERTIME_DISABLE_NEW_TURN_SHARD)
				AND NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GENERIC_TEXT)
					IF IS_BIT_SET(iLocalboolCheck22, LBOOL22_OVERTIME_NEW_TURN_SHARD)
						PRINTLN("[LM][OVERTIME] - (2) iPart: ", iLocalPart, " is host, broadcasting new round. iOTPartToMove: ", iOTPartToMove)
						PLAY_OVERTIME_ROUND_CHANGE_SHARD(FALSE, iOTPartToMove, MC_PlayerBD[iOTPartToMove].iTeam)
						CLEAR_BIT(iLocalboolCheck22, LBOOL22_OVERTIME_NEW_TURN_SHARD)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		// Broadcast that we scored
		IF IS_THIS_CURRENTLY_OVERTIME_TURNS()
			IF IS_BIT_SET(iLocalBoolCheck22, LBOOL22_OVERTIME_DELAYED_SCORE_SHARD)
			AND NOT bShouldHideHud
				PRINTLN("[LM][OVERTIME][TURNS] - Playing Scored Shard. iLocalPart: ", iLocalPart)
				CLEAR_BIT(iLocalBoolCheck22, LBOOL22_OVERTIME_DELAYED_SCORE_SHARD)
				
				BROADCAST_FMMC_OVERTIME_SCORED_SHARD(iDelayedOvertimeShardTeam, iDelayedOvertimeShardRule, iDelayedOvertimeShardScore, iDelayedOvertimePartToMove, iDelayedOvertimeCurrentRound, TRUE, FALSE)						
			ENDIF
		
			// This is used as a timer so that the player can see the last scored shard for a duration of time before the game ends. Once the bit is cleared, we will pass the mission in RULES header.
			IF HAS_NET_TIMER_STARTED(tdOvertimeScoredShardTimer)
				IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdOvertimeScoredShardTimer, ci_OVERTIME_SCORED_SHARD_TIME)
					CLEAR_BIT(iLocalBoolCheck23, LBOOL23_OVERTIME_SCORE_SHARD_ON_SCREEN)
					RESET_NET_TIMER(tdOvertimeScoredShardTimer)
				ENDIF
			ELSE
				IF NOT bShouldHideHud
				AND NOT IS_BIT_SET(iLocalBoolCheck22, LBOOL22_OVERTIME_DELAYED_SCORE_SHARD)
				AND NOT IS_BIT_SET(iLocalboolCheck22, LBOOL22_OVERTIME_NEW_TURN_SHARD)
				AND NOT IS_BIT_SET(iLocalBoolCheck22, LBOOL22_OVERTIME_ROUND_START_SHARD)
				AND NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GENERIC_TEXT)
					PROCESS_FMMC_OVERTIME_HELP_MESSAGE_POINTS_NEEDED()
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(iLocalBoolCheck22, LBOOL22_OVERTIME_DELAYED_SCORE_SHARD)
			AND NOT bShouldHideHud
				PRINTLN("[LM][OVERTIME][RUMBLE] - Playing Scored Shard. iLocalPart: ", iLocalPart)
				CLEAR_ALL_BIG_MESSAGES()
				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, "OVT_SC_PLA_TM", "OVT_SC_PLA_0c")
				CLEAR_BIT(iLocalBoolCheck22, LBOOL22_OVERTIME_DELAYED_SCORE_SHARD)
			ENDIF
		ENDIF
		
		VEHICLE_INDEX vehPlayer = GET_PLAYERS_LAST_VEHICLE()
		IF NOT IS_ENTITY_ALIVE(vehPlayer)
			IF NOT IS_BIT_SET(iLocalBoolCheck22, LBOOL22_OVERTIME_VEHICLE_DESTROYED)
				// This is no longer required!!! Keeping in case we want to re-enable it, currently it plays along with other sound effects which is causing too much overlap. Speak with Sarah Scott before re-implementing.
				/*PRINTLN("[LM][OVERTIME] - iPart: ", iLocalPart, " broadcasting Vehicle Destroyed event.")				
				BROADCAST_FMMC_OVERTIME_VEHICLE_DESTROYED(iLocalPart, iTeam)*/
				SET_BIT(iLocalBoolCheck22, LBOOL22_OVERTIME_VEHICLE_DESTROYED)
			ENDIF
		ENDIF
		
		PROCESS_FMMC_OVERTIME_HELP_MESSAGE_ON_ROOF()
	ENDIF
	
	IF bShouldHideHud
	AND IS_BIT_SET(iLocalBoolCheck22, LBOOL22_OVERTIME_ROUND_START_SHARD)
		PRINTLN("[LM][OVERTIME] - HUD HIDDEN - Preventing Shard Playing (1)(New Round)")
	ENDIF
	IF bShouldHideHud
	AND IS_BIT_SET(iLocalboolCheck22, LBOOL22_OVERTIME_NEW_TURN_SHARD)
		PRINTLN("[LM][OVERTIME] - HUD HIDDEN - Preventing Shard Playing (2)(New Turn)")
	ENDIF
	IF bShouldHideHud
	AND IS_BIT_SET(iLocalBoolCheck22, LBOOL22_OVERTIME_DELAYED_SCORE_SHARD)
		PRINTLN("[LM][OVERTIME] - HUD HIDDEN - Preventing Shard Playing (3)(Scored)")	
	ENDIF
	IF bShouldHideHud
	AND IS_BIT_SET(iLocalBoolCheck22, LBOOL23_OVERTIME_SUDDEN_DEATH_SHARD)
		PRINTLN("[LM][OVERTIME] - HUD HIDDEN - Preventing Shard Playing (4)(Sudden Death)")	
	ENDIF
ENDPROC

PROC PROCESS_OVERTIME_RUMBLE_SUDDEN_DEATH_SETTINGS()
	IF NOT g_bMissionEnding
		IF IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH)
		AND IS_BIT_SET(MC_serverBD.iServerBitSet5,SBBOOL5_SUDDEN_DEATH_OVERTIME_RUMBLE)		
			IF bIsLocalPlayerHost
				INT iTeam
				FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
					PROCESS_OVERTIME_RUMBLE_SUDDEN_DEATH_LOGIC(iTeam)
				ENDFOR
			ENDIF
			
			//Wait for player turn
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciOVERTIME_WAIT_YOUR_TURN)
				SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciOVERTIME_WAIT_YOUR_TURN)
				SET_BIT(iLocalBoolCheck23, LBOOL23_OVERTIME_MODE_SWITCH_OBJ_BLOCK)
				SET_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
				CLEAR_BIT(iLocalBoolCheck22, LBOOL22_OVERTIME_DELAYED_SCORE_SHARD)
				
				PRINTLN("[LM][OVERTIME] - Setting : LBOOL23_OVERTIME_MODE_SWITCH_OBJ_BLOCK and PBBOOL_OBJECTIVE_BLOCKER")
				
				INT i
				FOR i = 0 TO MAX_PENALTY_PLAYERS - 1
					MC_playerBD[iLocalPart].iMyTurnScore[i] = -1
				ENDFOR
				
				g_FMMC_STRUCT.iOvertimeRounds = 1
				g_FMMC_STRUCT.iOvertimeSDRounds = PICK_INT(MC_serverBD.iNumberOfPlayingPlayers[0] > MC_serverBD.iNumberOfPlayingPlayers[1], MC_serverBD.iNumberOfPlayingPlayers[0], MC_serverBD.iNumberOfPlayingPlayers[1])
				
				IF bIsLocalPlayerHost
					MC_serverBD_1.iCurrentRound = 1
					SET_BIT(MC_serverBD.iServerBitSet5, SBBOOL5_OVERTIME_SUDDEN_DEATH_ROUNDS)
				ENDIF
				
				INT iTeam
				FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
					g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPlayerLives = 0
				ENDFOR
			ENDIF
			
			//Ghost Inactive Players
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciOVERTIME_GHOST_INACTIVE_PLAYERS)
				SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciOVERTIME_GHOST_INACTIVE_PLAYERS)
			ENDIF
			
			//Start in Spectate
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_OT_START_SPECTATING)
				SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_OT_START_SPECTATING)
			ELSE
				IF NOT IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_TEAM_0_SHOULD_RESPAWN_BACK_AT_START_NOW + MC_PlayerBD[iLocalPart].iteam)
					IF bLocalPlayerPedOk
						IF iOTPartToMove > -1
						AND iOTPartToMove != iLocalPart
						AND NOT USING_HEIST_SPECTATE()
							PARTICIPANT_INDEX tempPartToMove
							tempPartToMove = INT_TO_PARTICIPANTINDEX(iOTPartToMove)
							IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPartToMove)
								PLAYER_INDEX tempPlayer 
								tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPartToMove)
								IF IS_NET_PLAYER_OK(tempPlayer)
									DO_SCREEN_FADE_OUT(500)
									BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("") 
									END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_LOADING)) 
									PRINTLN("[RCC MISSION] ciENABLE_OT_START_SPECTATING - Going to heist spectate thing, DO SCREEN FADE OUT")
									SET_BIT(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iSpecInfoBitset, SPEC_INFO_BS_USING_HEIST_SPECTATE)
									SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_DO_HEIST_SPECTATE)
									MPGlobalsAmbience.piHeistSpectateTarget = tempPlayer
									NETWORK_SET_OVERRIDE_SPECTATOR_MODE(TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//Hide new turn shard
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciOVERTIME_DISABLE_NEW_TURN_SHARD)
				CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciOVERTIME_DISABLE_NEW_TURN_SHARD)
			ENDIF
			
			IF NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_OVERTIME_SUDDEN_DEATH_LOCATES_CHANGED)
				INT i
				FOR i = 0 TO MC_serverBD.iNumLocCreated - 1
					INT iTeam
					FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
						IF MC_serverBD_4.iGotoLocationDataRule[i][iTeam] = FMMC_OBJECTIVE_LOGIC_GO_TO
						AND g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iWholeTeamAtLocation[iTeam] != ciGOTO_LOCATION_INDIVIDUAL
							g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iWholeTeamAtLocation[iTeam] = ciGOTO_LOCATION_INDIVIDUAL
						ENDIF
					ENDFOR
				ENDFOR
				SET_BIT(iLocalBoolCheck23, LBOOL23_OVERTIME_SUDDEN_DEATH_LOCATES_CHANGED)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCRESS_RESPAWN_VEH_HELP()
	IF MC_PlayerBD[iLocalPart].iLastVeh != -1
		IF GET_VEHICLE_RESPAWNS(MC_PlayerBD[iLocalPart].iLastVeh) > 0
			BOOL bExists = NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[MC_PlayerBD[iLocalPart].iLastVeh])
			IF NOT bExists
			OR (bExists AND IS_ENTITY_DEAD(NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicle[MC_PlayerBD[iLocalPart].iLastVeh])))
				IF NOT IS_BIT_SET(iLocalBooLCheck24, LBOOL24_SHOW_VEHICLE_RESPAWN_TEXT)
					SET_BIT(iLocalBooLCheck24, LBOOL24_SHOW_VEHICLE_RESPAWN_TEXT)
					PRINTLN("[JS] - PROCRESS_RESPAWN_VEH_HELP - last veh: ", MC_PlayerBD[iLocalPart].iLastVeh," destroyed. Showing help on respawn")
				ENDIF
			ELSE
				IF IS_BIT_SET(iLocalBooLCheck24, LBOOL24_SHOW_VEHICLE_RESPAWN_TEXT)
					PRINTLN("[JS] - PROCRESS_RESPAWN_VEH_HELP - last veh: ", MC_PlayerBD[iLocalPart].iLastVeh," alive again. Showing help on respawn")
					CLEAR_BIT(iLocalBooLCheck24, LBOOL24_SHOW_VEHICLE_RESPAWN_TEXT)
					PRINT_HELP("WVMWDHELP", 7500)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_PLAYER_CANT_DAMAGE_PLACED_VEH_WITH_COLLISION()
	INT iTeam = MC_PlayerBD[iLocalPart].iTeam
	
	IF iTeam > -1
	AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
	AND NOT IS_PED_INJURED(localPlayerPed)
		IF g_FMMC_STRUCT.iVehicleIndexForColImmunityTeam[iTeam] > -1
		AND g_FMMC_STRUCT.iVehicleIndexForColImmunityTeam[iTeam] < FMMC_MAX_VEHICLES
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.iVehicleIndexForColImmunityTeam[iTeam]])
				PRINTLN("[LM][PROCESS_VEH_BODY] - We have control of the ImmuneToColDamage Entity. We will now loop through players and make their vehicle unable to damage the entity.")
				
				INT i = 0
				FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
					PARTICIPANT_INDEX pi = INT_TO_PARTICIPANTINDEX(i)
					
					IF NETWORK_IS_PARTICIPANT_ACTIVE(pi)
					AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_ANY_SPECTATOR)
						PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(pi)
						PED_INDEX pedPlayer = GET_PLAYER_PED(piPlayer)
						
						IF NOT IS_PED_INJURED(pedPlayer)
						AND IS_PED_IN_ANY_VEHICLE(pedPlayer)
							VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(localPlayerPed)
							VEHICLE_INDEX myVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.iVehicleIndexForColImmunityTeam[iTeam]])
							IF IS_ENTITY_ALIVE(myVeh)
							AND IS_ENTITY_ALIVE(vehPlayer)
								IF NOT IS_BIT_SET(iRespVehCannotColDmgEntity, i)
									SET_ENTITY_CANT_CAUSE_COLLISION_DAMAGED_ENTITY(myVeh, vehPlayer)									
									SET_BIT(iRespVehCannotColDmgEntity, i)
									PRINTLN("[LM][PROCESS_VEH_BODY] - Setting iRespVehCannotColDmgEntity for iPlayersVeh: ", i)
								ENDIF
							ELSE
								IF IS_BIT_SET(iRespVehCannotColDmgEntity, i)
									CLEAR_BIT(iRespVehCannotColDmgEntity, i)
									PRINTLN("[LM][PROCESS_VEH_BODY] - Clearing iRespVehCannotColDmgEntity for iPlayersVeh: ", i, " One of the vehicles is destroyed.")
								ENDIF
							ENDIF
						ELSE
							IF IS_BIT_SET(iRespVehCannotColDmgEntity, i)
								CLEAR_BIT(iRespVehCannotColDmgEntity, i)
								PRINTLN("[LM][PROCESS_VEH_BODY] - Clearing iRespVehCannotColDmgEntity for iPlayersVeh: ", i, " Not in a Vehicle")
							ENDIF
						ENDIF
					ELSE
						IF IS_BIT_SET(iRespVehCannotColDmgEntity, i)
							CLEAR_BIT(iRespVehCannotColDmgEntity, i)
							PRINTLN("[LM][PROCESS_VEH_BODY] - Clearing iRespVehCannotColDmgEntity for iPlayersVeh: ", i, " Part not active")
						ENDIF
					ENDIF
				ENDFOR
			ELSE
				INT i = 0
				FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
					IF IS_BIT_SET(iRespVehCannotColDmgEntity, i)
						CLEAR_BIT(iRespVehCannotColDmgEntity, i)
						PRINTLN("[LM][PROCESS_VEH_BODY] - Clearing iRespVehCannotColDmgEntity for iPlayersVeh: ", i, " We don't have ownership anymore.")
					ENDIF
				ENDFOR
			ENDIF
		ELSE
			INT i = 0
			FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
				IF IS_BIT_SET(iRespVehCannotColDmgEntity, i)
					CLEAR_BIT(iRespVehCannotColDmgEntity, i)
					PRINTLN("[LM][PROCESS_VEH_BODY] - Clearing LBOOL25_COL_DAMAGE_AGAINST_ENTITY_DISABLED for iPlayersVeh: ", i, " We don't have ownership anymore.")
				ENDIF
			ENDFOR
		ENDIF
	ENDIF		
ENDPROC	

PROC PROCESS_CAMERA_SHAKE_ON_RULE()
	INT iTeam = MC_playerBD[iPartToUse].iteam	
	IF iTeam < FMMC_MAX_TEAMS
	AND iTeam > -1
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]		
		IF iRule < FMMC_MAX_RULES
			
			IF IS_BIT_SET(iLocalBoolCheck6, (LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam))
				IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_FINISHED_CAMERA_SHAKING_ON_RULE)
					PRINTLN("[LM][PROCESS_CAMERA_SHAKE_ON_RULE] - New Rule. Clearing 'Finished' bitset.")
					CLEAR_BIT(iLocalBoolCheck25, LBOOL25_FINISHED_CAMERA_SHAKING_ON_RULE)
					CLEAR_BIT(iLocalBoolCheck25, LBOOL25_USING_CAMERA_SHAKING_ON_RULE)
					RESET_NET_TIMER(td_CameraShakingOnRuleTimer)
				ENDIF
			ENDIF
			
			IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iCameraShakeDuration[iRule] != 0
			AND NOT IS_BIT_SET(iLocalBoolCheck25, LBOOL25_FINISHED_CAMERA_SHAKING_ON_RULE)
								
				IF NOT IS_BIT_SET(iLocalBoolCheck25, LBOOL25_USING_CAMERA_SHAKING_ON_RULE)
					PRINTLN("[LM][PROCESS_CAMERA_SHAKE_ON_RULE] - Using Camera Shaking on Rule.")
					SET_BIT(iLocalBoolCheck25, LBOOL25_USING_CAMERA_SHAKING_ON_RULE)										
				ENDIF
				
				// Do Shake
				IF NOT IS_GAMEPLAY_CAM_SHAKING()
					FLOAT fCameraShakeIntensity = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fCameraShakeIntensity[iRule]
					IF NOT IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_DLC_2_2022)
						fCameraShakeIntensity = fCameraShakeIntensity / 10
					ENDIF
					SHAKE_GAMEPLAY_CAM(GET_CAMERA_SHAKING_TYPE_FROM_CREATOR_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iCameraShakeType[iRule]), fCameraShakeIntensity)
				ENDIF
				
				// Timers for Reset.
				IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iCameraShakeDuration[iRule] != -1
					IF NOT HAS_NET_TIMER_STARTED(td_CameraShakingOnRuleTimer)
						START_NET_TIMER(td_CameraShakingOnRuleTimer)
					ENDIF
					
					IF HAS_NET_TIMER_STARTED(td_CameraShakingOnRuleTimer)
					AND HAS_NET_TIMER_EXPIRED_READ_ONLY(td_CameraShakingOnRuleTimer, (g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iCameraShakeDuration[iRule]*1000))
						PRINTLN("[LM][PROCESS_CAMERA_SHAKE_ON_RULE] - Camera shaking has finished now on this Rule. Duration is up.")
						SET_BIT(iLocalBoolCheck25, LBOOL25_FINISHED_CAMERA_SHAKING_ON_RULE)
						RESET_NET_TIMER(td_CameraShakingOnRuleTimer)
					ENDIF
				ENDIF
			ELIF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_USING_CAMERA_SHAKING_ON_RULE)
				// Cleanup
				PRINTLN("[LM][PROCESS_CAMERA_SHAKE_ON_RULE] - Cleaning up Camera Shaking on Rule.")
				CLEAR_BIT(iLocalBoolCheck25, LBOOL25_USING_CAMERA_SHAKING_ON_RULE)
				STOP_GAMEPLAY_CAM_SHAKING()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC()	
	INT iTeam = MC_PlayerBD[iLocalPart].iTeam
	BOOL bDoWeNeedToPromote
	
	#IF IS_DEBUG_BUILD
		IF bMultiPersonVehiclePrints
			PRINTLN("[SEAT_SWAP_DEBUG] ***************** MULTI VEHICLE PRINT SPAM *****************")
			PRINTLN("[SEAT_SWAP_DEBUG] Local participant ", iLocalPart)
			PRINTLN("[SEAT_SWAP_DEBUG] GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER(): ",GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER())
			PRINTLN("[SEAT_SWAP_DEBUG] Driver part: ", GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()))
			
			PRINTLN("[SEAT_SWAP_DEBUG] My preference: ", GET_PLAYER_RESPAWN_VEHICLE_SEAT_PREFERENCE())
			IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()) > -1
				PRINTLN("[SEAT_SWAP_DEBUG] Driver seat prefrence: ", GlobalplayerBD[GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER())].iRespawnSeatPreference)
				PRINTLN("[SEAT_SWAP_DEBUG] Driver participant number from partners array", GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()))
			ELSE
				PRINTLN("[SEAT_SWAP_DEBUG] Driver seat prefrence: CANNOT PRINT AS DRIVER INDEX IS -1")
			ENDIF
			
			PRINTLN("[SEAT_SWAP_DEBUG] Has cooldown timer started? ", HAS_NET_TIMER_STARTED(tdVehicleSeatSwapCooldownTimer))
			PRINTLN("[SEAT_SWAP_DEBUG] Is seat swap in progress? ", IS_MULTI_TEAM_VEHICLE_SEAT_SWAP_IN_PROGRESS())
		ENDIF
	#ENDIF
	
	IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()) > -1
	AND (NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER())))
	OR IS_BIT_SET(MC_playerBD[GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER())].iClientBitSet, PBBOOL_ANY_SPECTATOR))
		PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - Setting bDoWeNeedToPromote due to player leaving")
		bDoWeNeedToPromote = TRUE
	ENDIF
	
	IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()) > -1
	AND GlobalplayerBD[GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER())].iRespawnSeatPreference != -1
	AND NOT IS_MULTI_TEAM_VEHICLE_SEAT_SWAP_IN_PROGRESS()
	AND NOT HAS_NET_TIMER_STARTED(tdVehicleSeatSwapCooldownTimer)
		PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - Setting bDoWeNeedToPromote due to driver having incorrect preference")
		bDoWeNeedToPromote = TRUE
	ENDIF
	// If the Driver Participant leaves or is inactive, then promote the next person in the array.

	IF bDoWeNeedToPromote
 
		INT iOldDriverPointer = GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()
		INT iNewDriverPointer = (iOldDriverPointer+1)
		
		IF iNewDriverPointer >= MAX_VEHICLE_PARTNERS
			iNewDriverPointer = 0
		ENDIF
		
		PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - DriverPointer is old and is pointing to an invalid participant. Reassigning iOldDriverPointer: ", iOldDriverPointer)			
		PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - iNewDriverPointer: ", iNewDriverPointer, " PartNum: ", GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(iNewDriverPointer))
		
		SET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER(iNewDriverPointer)			
		
		// If we are the new Driver then we need to call some stuff...
		IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()) = iLocalPart
			SET_RACE_PASSENGER_GLOBAL(FALSE)
			SET_RACE_DRIVER_GLOBAL(TRUE)
			SET_PLAYER_RESPAWN_VEHICLE_SEAT_PREFERENCE(ENUM_TO_INT(VS_DRIVER))
			PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - I am now promoted as the new Driver")
		ENDIF
		
		// Cleanup the inactive participant from our list of partners. This should fix respawn issues and seat swapping issues.
		IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(iOldDriverPointer) > -1
		AND (NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(iOldDriverPointer)))
		OR IS_BIT_SET(MC_playerBD[GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(iOldDriverPointer)].iClientBitSet, PBBOOL_ANY_SPECTATOR))
			PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - Invalididating the old driver from our list...")
			SET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(iOldDriverPointer, -1)
		ENDIF
		IF IS_MULTI_TEAM_VEHICLE_SEAT_SWAP_IN_PROGRESS()
			DO_SCREEN_FADE_IN(250)
			CLEANUP_MULTI_VEHICLE_SEAT_SWAP()
		ENDIF
	ENDIF
	
	// Cleanup the inactive participant from our list of partners. This should fix respawn issues and seat swapping issues.
	INT iPart = 0 
	FOR iPart = 0 TO MAX_VEHICLE_PARTNERS-1
		IF GlobalplayerBD[iPart].iRespawnSeatPreference != -1
			IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(iPart) > -1
			AND (NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(iPart)))
			OR IS_BIT_SET(MC_playerBD[GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(iPart)].iClientBitSet, PBBOOL_ANY_SPECTATOR))
				PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - Invalididating and old passenger from our list...")
				SET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(iPart, -1)
				
				IF IS_MULTI_TEAM_VEHICLE_SEAT_SWAP_IN_PROGRESS()
					DO_SCREEN_FADE_IN(250)
					CLEANUP_MULTI_VEHICLE_SEAT_SWAP()
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	IF NOT IS_MULTI_TEAM_VEHICLE_SEAT_SWAP_IN_PROGRESS()
		IF NOT IS_PED_INJURED(localPlayerPed)
		AND INT_TO_ENUM(VEHICLE_SEAT, GET_PLAYER_RESPAWN_VEHICLE_SEAT_PREFERENCE()) = VS_DRIVER
		AND NOT IS_PLAYER_RESPAWNING(LocalPlayer)
			IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
				VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(localPlayerPed)
				
				// Sometimes the Mission Controller did not claim ownership of the Respawn System created vehicle. This is to fix that. This needs to happen FAST because passengers who try to respawn in the Drivers vehicle when it's not a MC owned veh will spawn outside of it!
				
				IF DOES_ENTITY_EXIST(vehPlayer)
					IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehPlayer)
					OR NOT IS_ENTITY_A_MISSION_ENTITY(vehPlayer)
					OR IS_BIT_SET(iLocalBoolCheck25, LBOOL25_DRIVER_DIED_OR_RESPAWNED_INTO_NEW_VEH)
						PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - Calling to grab ownership for FM_MISSION_CONTROLLER.")
						SET_ENTITY_AS_MISSION_ENTITY(vehPlayer, FALSE, TRUE)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(vehPlayer)
							SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(VEH_TO_NET(vehPlayer), TRUE)
						ENDIF
						CLEAR_BIT(iLocalboolCheck25, LBOOL25_DRIVER_DIED_OR_RESPAWNED_INTO_NEW_VEH)
					ELSE
						PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - FM_MISSION_CONTROLLER already owns this.")
					ENDIF
				ENDIF
				
				// Clean Up Old vehicle.
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitSet3, ciBS3_ENABLE_OLD_TEAM_VEHICLE_CLEANUP)
				OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitSet3, ciBS3_ENABLE_OLD_TEAM_VEHICLE_DELETION)
					
					IF DOES_ENTITY_EXIST(vehOldTeamMultiRespawnVehicle)
						PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - vehOldTeamMultiRespawnVehicle exists ", NETWORK_ENTITY_GET_OBJECT_ID(vehOldTeamMultiRespawnVehicle))
						IF IS_ENTITY_A_MISSION_ENTITY(vehOldTeamMultiRespawnVehicle)
							PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - Is a mission Entity")
							IF GET_VEHICLE_PED_IS_IN(localPlayerPed) != vehOldTeamMultiRespawnVehicle
								PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - Not the vehicle player is in")
								IF IS_ENTITY_ALIVE(vehOldTeamMultiRespawnVehicle)
									PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - Vehicle is alive")
									IF GET_VEHICLE_NUMBER_OF_PASSENGERS(vehOldTeamMultiRespawnVehicle) <= 0
										PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - No passengers")
										IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehOldTeamMultiRespawnVehicle)
											PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - Belongs to Mission Controller")
											IF NETWORK_HAS_CONTROL_OF_ENTITY(vehOldTeamMultiRespawnVehicle)
												PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - We have control of the entity")
												IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitSet3, ciBS3_ENABLE_OLD_TEAM_VEHICLE_DELETION)
													PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - Deleting vehicle (vehOldTeamMultiRespawnVehicle): ", NATIVE_TO_INT(vehOldTeamMultiRespawnVehicle))
													
													SET_ENTITY_AS_MISSION_ENTITY(vehOldTeamMultiRespawnVehicle, FALSE, TRUE)
													
													IF IS_ENTITY_A_MISSION_ENTITY(vehOldTeamMultiRespawnVehicle)
														IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehOldTeamMultiRespawnVehicle, FALSE)
															PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - CALLING DELETION! (vehOldTeamMultiRespawnVehicle): ", NATIVE_TO_INT(vehOldTeamMultiRespawnVehicle))
															DELETE_VEHICLE(vehOldTeamMultiRespawnVehicle)
														ELSE
															PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - Deletion failed (1) (vehOldTeamMultiRespawnVehicle): ", NATIVE_TO_INT(vehOldTeamMultiRespawnVehicle))
														ENDIF
													ELSE
														PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - Deletion failed (2) (vehOldTeamMultiRespawnVehicle): ", NATIVE_TO_INT(vehOldTeamMultiRespawnVehicle))
													ENDIF
												ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitSet3, ciBS3_ENABLE_OLD_TEAM_VEHICLE_CLEANUP)
													PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - cleaning up vehicle (vehOldTeamMultiRespawnVehicle): ", NATIVE_TO_INT(vehOldTeamMultiRespawnVehicle))
													SET_VEHICLE_AS_NO_LONGER_NEEDED(vehOldTeamMultiRespawnVehicle)
												ENDIF
											ELSE
												NETWORK_REQUEST_CONTROL_OF_ENTITY(vehOldTeamMultiRespawnVehicle)
												PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - requesting control (vehOldTeamMultiRespawnVehicle): ", NATIVE_TO_INT(vehOldTeamMultiRespawnVehicle))
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
							vehOldTeamMultiRespawnVehicle = GET_VEHICLE_PED_IS_IN(localPlayerPed)
							PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - Assigned new vehicle (vehOldTeamMultiRespawnVehicle): ", NATIVE_TO_INT(vehOldTeamMultiRespawnVehicle))
						ENDIF
					ENDIF
				ENDIF	
			ENDIF 
			IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iManualRespawnBitset, ciBS_MAN_RESPAWN_FORCED)
				BOOl bShouldClear = TRUE
				INT i = 0 
				FOR i = 0 TO MAX_VEHICLE_PARTNERS-1
					IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i) > -1
					AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i)))
						IF NOT IS_PART_IN_SAME_VEHICLE_AS_PART(iLocalPart, GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i))				
							PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - Everyone has returned to the vehicle. Cleaing: ciBS_MAN_RESPAWN_FORCED")
							bShouldClear = FALSE
						ENDIF
					ENDIF
				ENDFOR
				
				If bShouldClear
					CLEAR_BIT(MC_PlayerBD[iLocalPart].iManualRespawnBitset, ciBS_MAN_RESPAWN_FORCED)
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PLAYER_RESPAWNING(LocalPlayer)
			IF NOT IS_PED_INJURED(localPlayerPed)
				SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_SuppressInAirEvent, TRUE)
				IF CAN_PED_RAGDOLL(localPlayerPed)
					SET_PED_CAN_RAGDOLL(localPlayerPed, FALSE)
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_PED_INJURED(localPlayerPed)
				IF NOT CAN_PED_RAGDOLL(localPlayerPed)
					SET_PED_CAN_RAGDOLL(localPlayerPed, TRUE)
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitSet3, ciBS3_ENABLE_TEAM_VEHICLE_PASS_PRIORITIZE_WEAPON_SEATS)
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[ iLocalPart ].iteam].iTeamBitSet3, ciBS3_ENABLE_CHOOSE_SEAT_PREFERENCE_IN_CORONA)
			IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()) > -1
				IF NOT IS_PED_INJURED(localPlayerPed)
				AND INT_TO_ENUM(VEHICLE_SEAT, GET_PLAYER_RESPAWN_VEHICLE_SEAT_PREFERENCE()) != VS_DRIVER
				AND NOT IS_PLAYER_RESPAWNING(LocalPlayer)
					IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
						VEHICLE_SEAT vehTurretSeat = GET_SEAT_PED_IS_IN(localPlayerPed)
						VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(localPlayerPed)
						
						IF NOT IS_TURRET_SEAT(vehPlayer, vehTurretSeat)
							IF IS_PART_IN_SAME_VEHICLE_AS_PART(iLocalPart, GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()))
								IF GET_FREE_TURRET_VEHICLE_SEAT(vehPlayer, vehTurretSeat)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(localPlayerPed)
									SET_ENTITY_COLLISION(localPlayerPed, TRUE)	
									FREEZE_ENTITY_POSITION(localPlayerPed, FALSE)	
									SET_PED_CAN_RAGDOLL(localPlayerPed, FALSE)
									SET_PED_RESET_FLAG(localPlayerPed, PRF_SuppressInAirEvent, TRUE)
									TASK_ENTER_VEHICLE(LocalPlayerPed, vehPlayer, 1, vehTurretSeat, DEFAULT, ECF_WARP_PED)
									SET_PED_RESET_FLAG(localPlayerPed, PRF_SuppressInAirEvent, TRUE)
									PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - There is a free turret seat. Repositioning player to that one.")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	IF NOT IS_PLAYER_RESPAWNING(localPlayer)
	AND NOT IS_PED_INJURED(localPlayerPed)
	AND IS_PED_IN_ANY_VEHICLE(localPlayerPEd)
		IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_INITIALIZED_SEATING_DATA_FOR_MULTI_VEH)
			IF NOT IS_BIT_SET(iLocalBoolCheck25, LBOOL25_SAVED_ACTUAL_VEHICLE_SEAT_TO_MULTI_VEH_DATA)
				CACHE_VEHICLE_SEATING_PREFERENCES_FOR_MULTI_VEH()
				SET_BIT(iLocalBoolCheck25, LBOOL25_SAVED_ACTUAL_VEHICLE_SEAT_TO_MULTI_VEH_DATA)
			ENDIF
		ENDIF
	ENDIF
	
	BOOL bCooldown = FALSE
	
	IF HAS_NET_TIMER_STARTED(tdVehicleSeatSwapCooldownTimer)
		IF NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(tdVehicleSeatSwapCooldownTimer, ci_VEH_SEAT_SWAP_COOLDOWN_TIME)
			bCooldown = TRUE
		ELSE
			RESET_NET_TIMER(tdVehicleSeatSwapCooldownTimer)
		ENDIF
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitSet3, ciBS3_ENABLE_TEAM_VEHICLE_ALLOW_VEHICLE_SEAT_SWAPPING)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT, TRUE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SHUFFLE, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitSet3, ciBS3_ENABLE_TEAM_VEHICLE_ALLOW_VEHICLE_SEAT_SWAPPING)
	AND NOT bCooldown		
		IF NOT IS_PLAYER_RESPAWNING(localPlayer)
		AND NOT IS_PED_INJURED(localPlayerPed)
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				IF GET_NUM_PEDS_IN_VEHICLE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)) < 2 
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HELP_TUR")
					OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("STOC_HELP_1")
						HIDE_HELP_TEXT_THIS_FRAME()
					ENDIF
				ENDIF
			ENDIF
			IF NOT IS_MULTI_TEAM_VEHICLE_SEAT_SWAP_IN_PROGRESS()
				IF IS_DISABLED_CONTROL_HELD(INPUT_VEH_HEADLIGHT, 1000) // Right d-pad.
				AND IS_OK_TO_MULTI_TEAM_VEHICLE_SEAT_SWAP()
					PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - We want to make a vehicle seat switch!!")
						
					// If button pressed, send the event:
					SET_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_INITIATED_VEHICLE_SEAT_SWAP)
					BROADCAST_FMMC_INITIATE_SEAT_SWAP_TEAM_VEHICLE(GlobalplayerBD[iLocalPart].iVehiclePartners)
				ENDIF
			ENDIF
			
			// Swap Seat on Right-dpad press starts here....
		
			IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_INITIATED_VEHICLE_SEAT_SWAP)
				
				BOOL bGaveConsent = TRUE
				
				// Option, ask for consent.
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitSet3, ciBS3_ENABLE_TEAM_VEHICLE_SWAP_REQUIRES_CONSENT)
										
					// If Participants are still on the first stage, then we are not ready!
					INT i = 0 
					FOR i = 0 TO MAX_VEHICLE_PARTNERS-1
						IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i) > -1
						AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i)))
							IF NOT IS_BIT_SET(MC_playerBD[GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i)].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_GAVE_CONSENT)
								bGaveConsent = FALSE
								
								PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - Waiting for Vehicle Partner: ", i, " PartNumber: ", GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i), " To give consent in vehicle seat swap.")
							ELSE
								PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - Vehicle Partner: ", i, " PartNumber: ", GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i), " has given consent")
							ENDIF
						ENDIF
					ENDFOR
					
					IF NOT HAS_NET_TIMER_STARTED(tdVehicleSeatSwapConsentTimer)
						START_NET_TIMER(tdVehicleSeatSwapConsentTimer)
					ELSE
						IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdVehicleSeatSwapConsentTimer, ci_VEH_SEAT_SWAP_CONSENT_TIME)
							IF NOT HAS_NET_TIMER_STARTED(tdVehicleSeatSwapSafetyTimer)
							OR (HAS_NET_TIMER_STARTED(tdVehicleSeatSwapSafetyTimer) AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdVehicleSeatSwapSafetyTimer, ci_VEH_SEAT_SWAP_SAFETY_TIME))							
								IF NOT bGaveConsent
									PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - Consent Timer Expired. Resetting.")
									DO_SCREEN_FADE_IN(250)
									CLEANUP_MULTI_VEHICLE_SEAT_SWAP()
									EXIT
								ENDIf
							ENDIF						
						ENDIF
					ENDIF
					
					IF HAS_NET_TIMER_STARTED(tdVehicleSeatSwapConsentTimer)
						IF NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(tdVehicleSeatSwapConsentTimer, ci_VEH_SEAT_SWAP_CONSENT_TIME)
							IF IS_DISABLED_CONTROL_HELD(INPUT_VEH_HEADLIGHT, 1000) // Right d-pad.
							AND IS_OK_TO_MULTI_TEAM_VEHICLE_SEAT_SWAP()
								IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_GAVE_CONSENT)
									SET_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_GAVE_CONSENT)
									g_bMissionSeatSwapping = TRUE
									PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - I have just given consent.")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_GAVE_CONSENT)
					AND bGaveConsent
						IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_STOCKPILE(g_FMMC_STRUCT.iAdversaryModeType)
							SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(FALSE)
						ENDIF
						IF NOT HAS_NET_TIMER_STARTED(tdVehicleSeatSwapSafetyTimer)
							START_NET_TIMER(tdVehicleSeatSwapSafetyTimer)
							PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - Starting safety sync timer for consent.")	
						ENDIF
					ENDIF
				ENDIF
				
				IF (bGaveConsent
				AND IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_GAVE_CONSENT))
				OR NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitSet3, ciBS3_ENABLE_TEAM_VEHICLE_SWAP_REQUIRES_CONSENT)
					
					IF IS_SCREEN_FADED_OUT()
						PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - Screen Has faded...")
						
						// This check makes sure that the Driver leaves the vehicle first.
						BOOL bCanProceed						
						IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
							VEHICLE_SEAT vehSeat = INT_TO_ENUM(VEHICLE_SEAT, GET_SEAT_PED_IS_IN(localPlayerPed, TRUE))
							IF IS_VEHICLE_SEAT_FREE(GET_VEHICLE_PED_IS_IN(localPlayerPed), VS_DRIVER, TRUE)
							OR vehSeat = VS_DRIVER
								bCanProceed = TRUE
							ENDIF
						ELSE 
							bCanProceed = TRUE
						ENDIF
						
						IF bCanProceed
							IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)							
								vehOurMultiSwitchVehicle = GET_VEHICLE_PED_IS_IN(localPlayerPed)
								SET_ENTITY_INVINCIBLE(localPlayerPed, TRUE)
								SET_ENTITY_VISIBLE(localPlayerPed, FALSE)
								CLEAR_PED_TASKS_IMMEDIATELY(localPlayerPed)
								PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - Clearing tasks and caching vehicle we need to get back in.")
							ELSE
								IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGE_READY_TO_MOVE_VEHICLE)
									SET_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGE_READY_TO_MOVE_VEHICLE)
									CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_INITIATED_VEHICLE_SEAT_SWAP)
									SET_ENTITY_COLLISION(localPlayerPed, FALSE)
									PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - Data has finished being set, Left old vehicle. Clearing bit and getting ready to make a move! changing to next stage. (PBBOOL3_DATA_CHANGE_READY_TO_MOVE_VEHICLE)")
								ENDIF
							ENDIF
						ENDIF
					ELIF NOT IS_SCREEN_FADING_OUT()
						PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - Fading Screen Out to hide the process...")
						SET_ENTITY_INVINCIBLE(localPlayerPed, TRUE)
						SET_ENTITY_VISIBLE(localPlayerPed, FALSE)
						DO_SCREEN_FADE_OUT(250)
					ENDIF
				ELSE
					IF HAS_NET_TIMER_STARTED(tdVehicleSeatSwapSafetyTimer) 
						IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdVehicleSeatSwapSafetyTimer, ci_VEH_SEAT_SWAP_SAFETY_TIME)
						AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdVehicleSeatSwapConsentTimer, ci_VEH_SEAT_SWAP_CONSENT_TIME)
							PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - Cleaning Up. Someone went out of Sync.")						
							DO_SCREEN_FADE_IN(250)
							CLEANUP_MULTI_VEHICLE_SEAT_SWAP()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGE_READY_TO_MOVE_VEHICLE)
				BOOL bReady = TRUE
				
				// If Participants are still on the first stage, then we are not ready!
				INT i = 0 
				FOR i = 0 TO MAX_VEHICLE_PARTNERS-1
					IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i) > -1
					AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i)))
						IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGE_READY_TO_MOVE_VEHICLE)
							bReady = FALSE
							PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - Not quite ready to make the seat swap. We are still waiting for a data change on Vehicle Partner: ", i, " PartNumber: ", GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i))
						ENDIF
					ENDIF
				ENDFOR
							
				IF bReady
					IF SHUFFLE_MULTIPLE_PLAYER_SAME_VEHICLE_DATA_CYCLE_SEATS()
						CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGE_READY_TO_MOVE_VEHICLE)
						SET_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_FINAL)
						PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - changing to next stage. (PBBOOL3_DATA_CHANGING_SEAT_FINAL)")
					ENDIF
				ENDIF				
			ENDIF
			
			IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_DONE)
				BOOL bDoneMultiVehSeatSwap = TRUE
					
				INT i = 0 
				FOR i = 0 TO MAX_VEHICLE_PARTNERS-1
					IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i) > -1
					AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i)))
						IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_DONE)
							bDoneMultiVehSeatSwap = FALSE
								
							PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - We are in our new seat and just waiting for Vehicle Partner: ", i, " PartNumber: ", GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i)," to get to also enter theirs before cleaning this up.")
						ENDIF
					ENDIF
				ENDFOR
				
				IF bDoneMultiVehSeatSwap
					CACHE_VEHICLE_SEATING_PREFERENCES_FOR_MULTI_VEH()
					PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - Success!")
					DO_SCREEN_FADE_IN(250)
					CLEANUP_MULTI_VEHICLE_SEAT_SWAP()
				ENDIF
			ELIF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_FINAL)
				BOOL bReady = TRUE
				
				INT i = 0 
				FOR i = 0 TO MAX_VEHICLE_PARTNERS-1
					IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i) > -1
					AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i)))
						IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_FINAL)
							bReady = FALSE
							
							PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - Not quite ready to make the seat swap. We are still waiting for Vehicle Partner: ", i, " PartNumber: ", GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i), " to get to the final stage.")
						ENDIF
					ENDIF
				ENDFOR
				
				IF bReady
					IF iCachedNewVehSeatPref > -3
					AND IS_ENTITY_ALIVE(vehOurMultiSwitchVehicle)
						VEHICLE_SEAT vehSeat = INT_TO_ENUM(VEHICLE_SEAT, iCachedNewVehSeatPref)
						
						IF NOT IS_VEHICLE_SEAT_FREE(vehOurMultiSwitchVehicle, VS_DRIVER, TRUE)
						OR vehSeat = VS_DRIVER							
							IF NOT IS_PED_IN_ANY_VEHICLE(localPlayerPed, TRUE)
								PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - Setting to enter vehicle in Vehicle Seat: ", iCachedNewVehSeatPref)
								CLEAR_PED_TASKS_IMMEDIATELY(localPlayerPed)
								SET_ENTITY_COLLISION(localPlayerPed, TRUE)
								FREEZE_ENTITY_POSITION(localPlayerPed, FALSE)
								SET_PED_CAN_RAGDOLL(localPlayerPed, FALSE)
								SET_PED_RESET_FLAG(localPlayerPed, PRF_SuppressInAirEvent, TRUE)
								TASK_ENTER_VEHICLE(LocalPlayerPed, vehOurMultiSwitchVehicle, 1, vehSeat, DEFAULT, ECF_WARP_PED)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(localPlayerPed)
								SET_PED_RESET_FLAG(localPlayerPed, PRF_SuppressInAirEvent, TRUE)							
							ENDIF
							
							IF IS_PED_IN_ANY_VEHICLE(localPlayerPed, FALSE)
							AND GET_SEAT_PED_IS_IN(LocalPlayerPed, FALSE) = vehSeat
								PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - Ped is now back in the vehicle and in their new seat, changing to next stage. (PBBOOL3_DATA_CHANGING_SEAT_DONE). In Seat: ", ENUM_TO_INT(GET_SEAT_PED_IS_IN(LocalPlayerPed, FALSE)))
								SET_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_DONE)
							ELSE
								IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
									IF GET_SEAT_PED_IS_IN(LocalPlayerPed, FALSE) != vehSeat
										CLEAR_PED_TASKS_IMMEDIATELY(localPlayerPed)
										PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - SOMEHOW in the wrong seat. Exiting... ")
									ENDIF
								ENDIF
							ENDIF
						ELSE
							PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - Waiting for Driver to get in first...")
						ENDIF
					ELSE
						PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - We got here with iCachedNewVehSeatPref: ", iCachedNewVehSeatPref, " Or the vehicle died... Something went terribly wrong.")
						DO_SCREEN_FADE_IN(250)
						CLEANUP_MULTI_VEHICLE_SEAT_SWAP()
					ENDIF
				ENDIF
			ENDIF
		ELSE
			// cleanup Player Dead.		
			IF IS_MULTI_TEAM_VEHICLE_SEAT_SWAP_IN_PROGRESS()
				IF IS_PED_INJURED(localPlayerPEd)
				OR IS_PLAYER_RESPAWNING(localPlayer)
					PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - Emergancy cleanup. Something went terribly wrong.. (player Dead)")
					DO_SCREEN_FADE_IN(250)
					CLEANUP_MULTI_VEHICLE_SEAT_SWAP()
				ENDIF
			ENDIF
			
			// cleanup Veh Destroyed.
			IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_DONE)
				IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_FINAL)
				OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGE_READY_TO_MOVE_VEHICLE)
				OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_FINAL)
					IF NOT DOES_ENTITY_EXIST(vehOurMultiSwitchVehicle)
					OR NOT IS_VEHICLE_DRIVEABLE(vehOurMultiSwitchVehicle)
						PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - Emergancy cleanup. Something went terribly wrong.. (Vehicle Destroyed)")
						DO_SCREEN_FADE_IN(250)
						CLEANUP_MULTI_VEHICLE_SEAT_SWAP()
					ENDIF
				ENDIF
			ENDIF
			
			// Cleanup Loop....
			// If someone has set this BD, then we seriously need to clean this up and reset the array pointers.
			
		ENDIF
		//Timeout??
		// Swap Seat Ends here.
	ENDIF
ENDPROC

PROC INCREMENT_HARD_TARGET_POINTER(INT i)
	
	PRINTLN("[PROCESS_HARD_TARGET_MODE][LM][INCREMENT_HARD_TARGET_POINTER] - iTeam: ", i, " iHardTargetPointer is: ", MC_ServerBD.iHardTargetPointer[i], " Making sure it matches the current HardTargetPArticipant: ", MC_ServerBD.iHardTargetParticipant[i])
	
	// If people have left, we need to do this so that we don't end up with a situation where we're the next hard target after just being one.
	MC_ServerBD.iHardTargetPointer[i] = GET_TEAM_PARTICIPANT_NUMBER_FROM_PARTICIPANT_NUMBER_MC(i, MC_ServerBD.iHardTargetParticipant[i])
	
	PRINTLN("[PROCESS_HARD_TARGET_MODE][LM][INCREMENT_HARD_TARGET_POINTER] - iTeam: ", i, " iHardTargetPointer is now: ", MC_ServerBD.iHardTargetPointer[i], " after running 'GET_TEAM_PARTICIPANT_NUMBER_FROM_PARTICIPANT_NUMBER_MC'")
	
	MC_ServerBD.iHardTargetPointer[i]++
	PRINTLN("[PROCESS_HARD_TARGET_MODE][LM][INCREMENT_HARD_TARGET_POINTER] - iTeam: ", i, " Incrementing iHardTargetPointer to: ", MC_ServerBD.iHardTargetPointer[i])
	
	IF MC_ServerBD.iHardTargetPointer[i] >= MC_serverBD.iNumberOfPlayingPlayers[i]
		MC_ServerBD.iHardTargetPointer[i] = 0
		PRINTLN("[PROCESS_HARD_TARGET_MODE][LM][INCREMENT_HARD_TARGET_POINTER] - iTeam: ", i, " iHardTargetPointer >= iNumberOfPlayingPlayers. (", MC_serverBD.iNumberOfPlayingPlayers[i], ") Setting back to 0.")
	ENDIF
ENDPROC

FUNC INT GET_OUTFIT_INT_FOR_HARD_TARGET(INT iTeam)	
	IF iTeam = 0
		RETURN enum_to_int(OUTFIT_VERSUS_HIDDEN_GO_HT_BLACK_TARGET_0)
	ENDIF
	RETURN enum_to_int(OUTFIT_VERSUS_HIDDEN_GO_HT_GREEN_TARGET_0)
ENDFUNC

FUNC BOOL PROCESS_HARD_TARGET_OUTFIT_CHANGE(INT iOutfit)			
	INT iRuleOutfit = iOutfit
	BOOL bOutfitSet = FALSE
	
	PRINTLN("[RCC MISSION] [PROCESS_HARD_TARGET_OUTFIT_CHANGE] - iRuleOutfit: ", iRuleOutfit, " Default Outfit: ", GET_LOCAL_DEFAULT_OUTFIT(MC_playerBD[iPartToUse].iteam))
	
	IF GET_PED_STEALTH_MOVEMENT(LocalPlayerPed)
		PRINTLN("[RCC MISSION][PROCESS_HARD_TARGET_OUTFIT_CHANGE] was in stealth. setting LBOOL26_PUT_BACK_INTO_STEALTH_MODE")
		SET_BIT(iLocalBoolCheck26, LBOOL26_PUT_BACK_INTO_STEALTH_MODE_OUTFIT_CHANGE)
	ENDIF
	
	//in case a rule gets processed immediately whilst clothes are still loading and the new rule has no clothes  
	IF iRuleOutfit = -1
	AND iRuleOutfitLoading != -1
		iRuleOutfit = iRuleOutfitLoading
	ENDIF
	
	IF iRuleOutfit > -1
	
		//Need to equip a new outfit this rule				
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iPartToUse].iteam)
		AND (iRuleOutfit != iRuleOutfitLoading)
			CLEAR_BIT(iLocalBoolCheck7, LBOOL7_OUTFIT_ON_RULE_LOADED)
			SET_BIT(iLocalBoolCheck7, LBOOL7_OUTFIT_ON_RULE_PROCESSING)
			iRuleOutfitLoading = iRuleOutfit
		ENDIF
		
		//set local struct to use this outfit data to set   
		sApplyOutfitData.pedID 		= LocalPlayerPed
		sApplyOutfitData.eOutfit 	= int_to_enum(MP_OUTFIT_ENUM,iRuleOutfit)				
		IF SET_PED_MP_OUTFIT(sApplyOutfitData)
			CLEAR_BIT(iLocalBoolCheck7, LBOOL7_OUTFIT_ON_RULE_PROCESSING)
			iRuleOutfitLoading = -1
			MC_playerBD[iLocalPart].iOutfit = iRuleOutfit
			PRINTLN("[RCC MISSION][PROCESS_HARD_TARGET_OUTFIT_CHANGE] Outfits - MC_playerBD[",iLocalPart,"].iOutfit set as ",iRuleOutfit)
			bOutfitSet = TRUE
			
			START_NET_TIMER(tdChangeOutfitAppearance)
		ENDIF				
		
	ELIF iRuleOutfit = -2
		
		//Need to equip a new outfit this rule				
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iPartToUse].iteam)
		AND (iRuleOutfit != iRuleOutfitLoading)
			CLEAR_BIT(iLocalBoolCheck7, LBOOL7_OUTFIT_ON_RULE_LOADED)
			SET_BIT(iLocalBoolCheck7, LBOOL7_OUTFIT_ON_RULE_PROCESSING)
			iRuleOutfitLoading = iRuleOutfit
		ENDIF
		
		//set local struct to use this outfit data to set   
		sApplyOutfitData.pedID 		= LocalPlayerPed
		sApplyOutfitData.eOutfit 	= INT_TO_ENUM(MP_OUTFIT_ENUM, GET_LOCAL_DEFAULT_OUTFIT(MC_playerBD[iPartToUse].iteam))
		sApplyOutfitData.eMask   	= INT_TO_ENUM(MP_OUTFIT_MASK_ENUM, GET_LOCAL_DEFAULT_MASK(MC_playerBD[iPartToUse].iteam))
		
		IF SET_PED_MP_OUTFIT(sApplyOutfitData)
			CLEAR_BIT(iLocalBoolCheck7, LBOOL7_OUTFIT_ON_RULE_PROCESSING)
			iRuleOutfitLoading = -1
			MC_playerBD[iLocalPart].iOutfit = iRuleOutfit
			PRINTLN("[RCC MISSION][PROCESS_HARD_TARGET_OUTFIT_CHANGE] Outfits - MC_playerBD[",iLocalPart,"].iOutfit set as ",iRuleOutfit)
			bOutfitSet = TRUE
			
			START_NET_TIMER(tdChangeOutfitAppearance)
		ENDIF
	ENDIF
	
	IF bOutfitSet
	AND NOT IS_PED_INJURED(LocalPlayerPed)
	AND NOT IS_PLAYER_SPECTATING(LocalPlayer)
	AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)
	AND NOT g_bMissionEnding
	AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		IF HAS_NAMED_PTFX_ASSET_LOADED("scr_powerplay")
			USE_PARTICLE_FX_ASSET("scr_powerplay")
			SET_PARTICLE_FX_NON_LOOPED_COLOUR(0.75, 0.75, 0.75)
			START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_powerplay_beast_appear", LocalPlayerPed, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)					
		ENDIF
		IF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_PUT_BACK_INTO_STEALTH_MODE_OUTFIT_CHANGE)
			SET_PED_STEALTH_MOVEMENT(LocalPlayerPed, TRUE)
			CLEAR_BIT(iLocalBoolCheck26, LBOOL26_PUT_BACK_INTO_STEALTH_MODE_OUTFIT_CHANGE)
			PRINTLN("[JT][PROCESS_HARD_TARGET_OUTFIT_CHANGE] Putting back into stealth.")
		ENDIF
	ELSE
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_SMOKE_ON_OUTFIT_CHANGE)
			PRINTLN("[RCC MISSION][PROCESS_HARD_TARGET_OUTFIT_CHANGE] Outfits - Don't play PTFX. bOutfitSet: ", bOutfitSet, " Spectating: ", IS_PLAYER_SPECTATING(LocalPlayer), " Spectating2: ", IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_ANY_SPECTATOR), " Injured: ", IS_PED_INJURED(LocalPlayerPed))
		ENDIF
	ENDIF
	
	RETURN bOutfitSet
ENDFUNC

// [FIX_2020_CONTROLLER] - A lot of these adversary mode process functions could be moved to a new "FM_Mission_Controller_AdversaryMode.sch header file.
PROC PROCESS_HARD_TARGET_MODE()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciHARD_TARGET_ENABLE_MODE)
		IF HAS_NET_TIMER_STARTED(tdChangeOutfitAppearance)
			IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdChangeOutfitAppearance, ci_ChangeOutfitAppearance)
			AND NOT IS_PED_INJURED(localPlayerPed)
			AND NOT IS_PLAYER_RESPAWNING(localPlayer)
				PRINTLN("[RCC MISSION] - Broadcasting to change appearance for headshots..")
				BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_PLAYER_CHANGED_APPEARANCE, ALL_PLAYERS())
				UPDATE_PED_WEAPON_ANIMATION_OVERRIDE(PLAYER_PED_ID())
				RESET_NET_TIMER(tdChangeOutfitAppearance)
			ELSE
				IF IS_PED_INJURED(localPlayerPed)
				OR IS_PLAYER_RESPAWNING(localPlayer)
					// Re-change outfit, sometimes it doesn't work if we die very close to the change.
					IF NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_HARD_TARGET_OUTFIT_CHANGED)
						SET_BIT(iLocalBoolCHeck26, LBOOL26_HARD_TARGET_OUTFIT_CHANGED)
					ELSE
						CLEAR_BIT(iLocalBoolCHeck26, LBOOL26_HARD_TARGET_OUTFIT_CHANGED)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciHARD_TARGET_ENABLE_MODE)
	AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet,SBBOOL_MISSION_OVER)
	AND IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_EVERYONE_RUNNING)
	AND IS_BIT_SET(MC_serverbd.iServerBitSet, SSBOOL_ALL_READY_INTRO_CUT)
		INT i
		INT iTeam = MC_PlayerBD[iLocalPart].iteam
		INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
		
		IF iRule < FMMC_MAX_RULES
			// Host Only - Progress the target.
			IF bIsLocalPlayerHost
				IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
					IF IS_BIT_SET(MC_ServerBD.iServerBitSet6, SBBOOL6_HARD_TARGET_TIMER_BIT)
						INT iTimeRemaining = (g_FMMC_STRUCT.iHRDTARPickTargetTimer * MC_ServerBD_4.iHardTargetMiniRound) - (GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveTimeLimitRule[iRule]) - GET_REMAINING_TIME_ON_RULE(iTeam, iRule))
						
						IF iTimeRemaining <= 0
							PRINTLN("[PROCESS_HARD_TARGET_MODE][LM] - Timer Expiring (tdNewHardTargetTimer)")
							CLEAR_BIT(MC_ServerBD.iServerBitSet6, SBBOOL6_HARD_TARGET_TIMER_BIT)
							FOR i = 0 TO MC_serverBD.iNumberOfTeams - 1
								INCREMENT_HARD_TARGET_POINTER(i)
							ENDFOR
						ENDIF
					ENDIF
					
					// Set the new Hard Targets.
					IF NOT IS_BIT_SET(MC_ServerBD.iServerBitSet6, SBBOOL6_HARD_TARGET_TIMER_BIT)
						PRINTLN("[PROCESS_HARD_TARGET_MODE][LM] - Starting Timer (tdNewHardTargetTimer)")
						
						BOOL bUsePart = TRUE
						
						FOR i = 0 TO MC_serverBD.iNumberOfTeams - 1
							MC_ServerBD.iHardTargetParticipant[i] = GET_PARTICIPANT_NUMBER_FROM_TEAM_PARTICIPANT_NUMBER(i, MC_ServerBD.iHardTargetPointer[i], TRUE)						
							PRINTLN("[PROCESS_HARD_TARGET_MODE][LM] - iTeam: ", i, " iHardTargetParticipant: ", MC_ServerBD.iHardTargetParticipant[i])
							
							// Verify that the targets are valid.
							IF MC_ServerBD.iHardTargetParticipant[i] > -1
								IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(MC_ServerBD.iHardTargetParticipant[i]))
								OR IS_PARTICIPANT_A_SPECTATOR(MC_ServerBD.iHardTargetParticipant[i])
									bUsePart = FALSE
									PRINTLN("[PROCESS_HARD_TARGET_MODE][LM] - iTeam: ", i, " iHardTargetParticipant: ", MC_ServerBD.iHardTargetParticipant[i], " Do not use this part, we need to try and assign again (1)")
								ENDIF
							ELSE
								bUsePart = FALSE
								PRINTLN("[PROCESS_HARD_TARGET_MODE][LM] - iTeam: ", i, " iHardTargetParticipant: ", MC_ServerBD.iHardTargetParticipant[i], " Do not use this part, we need to try and assign again (2)")
							ENDIF
						ENDFOR
						
						IF bUsePart
							MC_ServerBD_4.iHardTargetMiniRound++
							SET_BIT(MC_ServerBD.iServerBitSet6, SBBOOL6_HARD_TARGET_TIMER_BIT)
							RESET_NET_TIMER(tdHardTargetSwitchDelay)
							START_NET_TIMER(tdHardTargetSwitchDelay)
						ELSE
							// Reassign if targets are invalid.
							SET_BIT(MC_ServerBD.iServerBitSet6, SBBOOL6_HARD_TARGET_TIMER_BIT)
							PRINTLN("[PROCESS_HARD_TARGET_MODE][LM] - Invalid Selection (1) Finding new hard Target.")
						ENDIF
					ENDIF
					
					// Delay to fix issues where the Shard appeared after a round before certain data could be set.
					IF HAS_NET_TIMER_STARTED(tdHardTargetSwitchDelay)
						IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdHardTargetSwitchDelay, ci_HARD_TARGET_SWITCH_DELAY)
							
							// Verify that the targets are valid.
							BOOL bUsePart = TRUE					
							FOR i = 0 TO MC_serverBD.iNumberOfTeams - 1
								IF MC_ServerBD.iHardTargetParticipant[i] > -1
									IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(MC_ServerBD.iHardTargetParticipant[i]))
									OR IS_PARTICIPANT_A_SPECTATOR(MC_ServerBD.iHardTargetParticipant[i])
										bUsePart = FALSE
										PRINTLN("[PROCESS_HARD_TARGET_MODE][LM] - iTeam: ", i, " iHardTargetParticipant: ", MC_ServerBD.iHardTargetParticipant[i], " Do not use this part, we need to try and assign again (1)")
									ENDIF
								ELSE
									bUsePart = FALSE
									PRINTLN("[PROCESS_HARD_TARGET_MODE][LM] - iTeam: ", i, " iHardTargetParticipant: ", MC_ServerBD.iHardTargetParticipant[i], " Do not use this part, we need to try and assign again (2)")
								ENDIF
							ENDFOR
							
							IF bUsePart
								RESET_NET_TIMER(tdHardTargetSwitchDelay)
								BROADCAST_FMMC_HARD_TARGET_NEW_TARGET_SHARD(MC_ServerBD.iHardTargetParticipant, GET_NUM_ACTIVE_TEAMS())	
							ELSE
								// Reassign if targets are invalid.
								SET_BIT(MC_ServerBD.iServerBitSet6, SBBOOL6_HARD_TARGET_TIMER_BIT)
								PRINTLN("[PROCESS_HARD_TARGET_MODE][LM] - Invalid Selection (2) Finding new hard Target.")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			// Required so that it's not reset until we revive. To stop weird issues such as killing a player and then they are not the target, therefore you don't get points.
			IF NOT IS_PED_INJURED(playerPedToUse)
			AND NOT IS_PLAYER_RESPAWNING(playerToUse)		
				IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
					IF MC_ServerBD.iHardTargetParticipant[iTeam] = iPartToUse					
						IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
							IF NOT IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet3, PBBOOL3_PLAYER_IS_A_HARD_TARGET)
								SET_BIT(MC_PlayerBD[iLocalPart].iClientBitSet3, PBBOOL3_PLAYER_IS_A_HARD_TARGET)
								SET_BIT(iLocalboolCheck27, LBOOL27_WAS_HARD_TARGET)
								PRINTLN("[PROCESS_HARD_TARGET_MODE][LM] - Local Player is a Hard target. Setting bit.")	
							ENDIF
							IF NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_HARD_TARGET_OUTFIT_CHANGED)
								IF PROCESS_HARD_TARGET_OUTFIT_CHANGE(GET_OUTFIT_INT_FOR_HARD_TARGET(MC_PlayerBD[iLocalPart].iteam))
									SET_BIT(iLocalBoolCheck26, LBOOL26_HARD_TARGET_OUTFIT_CHANGED)
									PRINTLN("[PROCESS_HARD_TARGET_MODE][LM] Outfit Changed to hard target.")
								ENDIF
							ENDIF
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_HARD_TARGET_AUDIO)
							IF NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_HARD_TARGET_CHANGED_SOUND)
								SET_BIT(iLocalBoolCHeck26, LBOOL26_HARD_TARGET_CHANGED_SOUND)
								PLAY_SOUND_FRONTEND(-1, "Become_Target" ,"dlc_xm_hata_Sounds", false)								
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
							IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet3, PBBOOL3_PLAYER_IS_A_HARD_TARGET)						
								CLEAR_BIT(MC_PlayerBD[iLocalPart].iClientBitSet3, PBBOOL3_PLAYER_IS_A_HARD_TARGET)
								PRINTLN("[PROCESS_HARD_TARGET_MODE][LM] - Local Player is no longer a Hard target. Clearing bit.")
							ENDIF
							
							IF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_HARD_TARGET_OUTFIT_CHANGED)
								IF PROCESS_HARD_TARGET_OUTFIT_CHANGE(-2)
									CLEAR_BIT(iLocalBoolCheck26, LBOOL26_HARD_TARGET_OUTFIT_CHANGED)
									PRINTLN("[PROCESS_HARD_TARGET_MODE][LM] Outfit Changed back to Default.")
								ENDIF 
							ENDIF
							
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_HARD_TARGET_AUDIO)
								IF IS_BIT_SET(iLocalboolCheck27, LBOOL27_WAS_HARD_TARGET)
									CLEAR_BIT(iLocalboolCheck27, LBOOL27_WAS_HARD_TARGET)
									PLAY_SOUND_FRONTEND(-1, "No_Longer_Target" ,"dlc_xm_hata_Sounds", false)
								ENDIF
							ENDIF
						ELSE
							// so the sound doesn't play when switching specataing a hard target.
							IF NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_HARD_TARGET_CHANGED_SOUND)
								SET_BIT(iLocalBoolCHeck26, LBOOL26_HARD_TARGET_CHANGED_SOUND)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_HARD_TARGET_OUTFIT_CHANGED)
						IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
							IF PROCESS_HARD_TARGET_OUTFIT_CHANGE(GET_OUTFIT_INT_FOR_HARD_TARGET(MC_PlayerBD[iLocalPart].iteam))
								SET_BIT(iLocalBoolCheck26, LBOOL26_HARD_TARGET_OUTFIT_CHANGED)
								PRINTLN("[PROCESS_HARD_TARGET_MODE][LM] Outfit Changed to hard target (Sudden Death).")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			// Clean up dirty data. Player is now a Spectator.
			IF IS_PLAYER_SPECTATING(LocalPlayer)
				IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet3, PBBOOL3_PLAYER_IS_A_HARD_TARGET)
					CLEAR_BIT(MC_PlayerBD[iLocalPart].iClientBitSet3, PBBOOL3_PLAYER_IS_A_HARD_TARGET)
					PRINTLN("[PROCESS_HARD_TARGET_MODE][LM] - Local Player is no longer a Hard target. Clearing bit. (now spectating)")
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
				CLEAR_BIT(MC_PlayerBD[iLocalPart].iClientBitSet3, PBBOOL3_PLAYER_IS_A_HARD_TARGET)
			ENDIF

			BOOL bTeamHasChevron[FMMC_MAX_TEAMS]
			
			// This has not been optimized to breakloop upon reaching a player = totalnumofplayingplayers because it was causing issues when we had gaps in part / player numbers, due to people leaving.
			// Set Special Blip and other target specific elements.
			INT iPlayer = 0
			FOR iPlayer = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
				PARTICIPANT_INDEX piPart = INT_TO_PARTICIPANTINDEX(iPlayer)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(piPart)
					PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)
					IF NOT IS_PLAYER_SPECTATING(piPlayer)
						PRINTLN("[PROCESS_HARD_TARGET_MODE][LM] - iPlayer: ", iPlayer)
						
						IF IS_MP_GAMER_TAG_ACTIVE(NATIVE_TO_INT(piPlayer))
							IF IS_BIT_SET(MC_PlayerBD[iPlayer].iClientBitSet3, PBBOOL3_PLAYER_IS_A_HARD_TARGET)
							AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
							AND NOT IS_PED_INJURED(GET_PLAYER_PED(piPlayer))
							AND NOT IS_PLAYER_RESPAWNING(localPlayer)
							AND NOT bTeamHasChevron[MC_PlayerBD[iPlayer].iteam]
								PRINTLN("[PROCESS_HARD_TARGET_MODE][LM] - Showing Custom Hard Target Icon.")
								SET_ICON_VISABILITY(NATIVE_TO_INT(piPlayer), MP_TAG_BIKER_ARROW, TRUE)
								bTeamHasChevron[MC_PlayerBD[iPlayer].iteam] = TRUE
							ELSE
								PRINTLN("[PROCESS_HARD_TARGET_MODE][LM] - Hiding Hard Target Icon")							
								SET_ICON_VISABILITY(NATIVE_TO_INT(piPlayer), MP_TAG_BIKER_ARROW, FALSE)
							ENDIF
						ENDIF
						
						IF IS_BIT_SET(MC_PlayerBD[iPlayer].iClientBitSet3, PBBOOL3_PLAYER_IS_A_HARD_TARGET)
						OR IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
							PRINTLN("[PROCESS_HARD_TARGET_MODE][LM] - Showing Custom Hard Target Blip.")
							SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(piPlayer, RADAR_TRACE_TEMP_5, TRUE)
						ELSE
							PRINTLN("[PROCESS_HARD_TARGET_MODE][LM] - Hiding Hard Target Blip.")
							SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(piPlayer, RADAR_TRACE_TEMP_5, FALSE)
						ENDIF
						
						IF IS_MP_GAMER_TAG_ACTIVE(NATIVE_TO_INT(piPlayer))
							SET_ICON_VISABILITY(NATIVE_TO_INT(piPlayer), MP_TAG_ARROW, FALSE)
						ENDIF
						
						// Net_Blip for Players was not calling a refresh for some reason. So when we detect that we have changed who the hard target is, we will refresh blips HERE.
						// url:bugstar:3954847 - Hard Target - The blips for the "target" players are inconsistent after a remote player leaves the game
						IF IS_BIT_SET(MC_PlayerBD[iPlayer].iClientBitSet3, PBBOOL3_PLAYER_IS_A_HARD_TARGET) != IS_BIT_SET(iPlayerHardTargetBit, iPlayer)
							IF IS_BIT_SET(MC_PlayerBD[iPlayer].iClientBitSet3, PBBOOL3_PLAYER_IS_A_HARD_TARGET)
								PRINTLN("[PROCESS_HARD_TARGET_MODE][LM] - Blip and Hard Target Difference Detected. Forcing Update. (SET): ", iPlayer)
								SET_BIT(iPlayerHardTargetBit, iPlayer)
								DO_FORCED_UPDATE_OF_ALL_PLAYER_BLIPS_THIS_FRAME()
							ENDIF
							IF NOT IS_BIT_SET(MC_PlayerBD[iPlayer].iClientBitSet3, PBBOOL3_PLAYER_IS_A_HARD_TARGET)
								PRINTLN("[PROCESS_HARD_TARGET_MODE][LM] - Blip and Hard Target Difference Detected. Forcing Update. (CLEAR): ", iPlayer)
								CLEAR_BIT(iPlayerHardTargetBit, iPlayer)
								DO_FORCED_UPDATE_OF_ALL_PLAYER_BLIPS_THIS_FRAME()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
			
			// Reassign a Hard Target if the current target left the mission or became a Spectator.
			IF bIsLocalPlayerHost
				INT iTimeRemaining = (g_FMMC_STRUCT.iHRDTARPickTargetTimer * MC_ServerBD_4.iHardTargetMiniRound) - (GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveTimeLimitRule[iRule]) - GET_REMAINING_TIME_ON_RULE(iTeam, iRule))				
				
				IF iTimeRemaining > 5000
					FOR i = 0 TO MC_serverBD.iNumberOfTeams - 1
						PRINTLN("Team: ", i, "'s Hard Target Part = ", MC_ServerBD.iHardTargetParticipant[i])
						
						BOOL bShouldReassign = FALSE
						
						IF MC_ServerBD.iHardTargetParticipant[i] > -1
							PARTICIPANT_INDEX piPart = INT_TO_PARTICIPANTINDEX(MC_ServerBD.iHardTargetParticipant[i])
							IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(piPart) 
							OR IS_PLAYER_SPECTATING(NETWORK_GET_PLAYER_INDEX(piPart))
							OR MC_PlayerBD[MC_ServerBD.iHardTargetParticipant[i]].iteam != i
								bShouldReassign = TRUE
							ENDIF
						ENDIF
						
						IF MC_ServerBD.iHardTargetParticipant[i] = -1
							bShouldReassign = TRUE
						ENDIF
						
						IF bShouldReassign
							PRINTLN("[PROCESS_HARD_TARGET_MODE][LM] - Detected inactive Hard Target iPlayer: ", MC_ServerBD.iHardTargetParticipant[i], " Calling to increment prematurely.")
							INCREMENT_HARD_TARGET_POINTER(i)
							
							INT iPart = GET_PARTICIPANT_NUMBER_FROM_TEAM_PARTICIPANT_NUMBER(i, MC_ServerBD.iHardTargetPointer[i], TRUE)
							
							IF iPart != -1
							AND MC_ServerBD.iHardTargetParticipant[i] != iPart
								MC_ServerBD.iHardTargetParticipant[i] = iPart
								PRINTLN("[PROCESS_HARD_TARGET_MODE][LM] - iTeam: ", i, " iHardTargetParticipant: ", MC_ServerBD.iHardTargetParticipant[i])								
								BROADCAST_FMMC_HARD_TARGET_NEW_TARGET_SHARD(MC_ServerBD.iHardTargetParticipant, GET_NUM_ACTIVE_TEAMS(), MC_ServerBD.iHardTargetParticipant[i])
							ELSE
								PRINTLN("[PROCESS_HARD_TARGET_MODE][LM] - Something went wrong -1")
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
				IF NOT g_bMissionHideRespawnBar 
					PRINTLN("[PROCESS_HARD_TARGET_MODE][LM] - Setting to hide the respawn bar as it's sudden death now.")
					g_bMissionHideRespawnBar = TRUE
				ENDIF
			ENDIF
			
			// Keep the global synced with our MC version.
			IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet3, PBBOOL3_PLAYER_IS_A_HARD_TARGET)
				IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iMissionModeBit, ciADVERSARY_IS_A_HARD_TARGET)
					PRINTLN("[PROCESS_HARD_TARGET_MODE][LM] - Setting ciADVERSARY_IS_A_HARD_TARGET")
					SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iMissionModeBit, ciADVERSARY_IS_A_HARD_TARGET)
				ENDIF
			ELSE
				IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iMissionModeBit, ciADVERSARY_IS_A_HARD_TARGET)
					PRINTLN("[PROCESS_HARD_TARGET_MODE][LM] - Clearing ciADVERSARY_IS_A_HARD_TARGET")
					CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iMissionModeBit, ciADVERSARY_IS_A_HARD_TARGET)
				ENDIF
			ENDIF
			
			// Draw the Timer for when the Hard Target changes.
			IF NOT SHOULD_HIDE_THE_HUD_THIS_FRAME()
			AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
			AND IS_NET_PLAYER_OK(PlayerToUse)
				DRAW_HARD_TARGET_TIMER()
			ELSE
				IF IS_SOUND_ID_VALID(iHardTargetCountdownTimerSoundID)
					PRINTLN("[PROCESS_HARD_TARGET_MODE][LM] - Clearing 5s Timer for Hard Target Change.")
					iHardTargetCountdownTimerSoundID = -1
					STOP_SOUND(iHardTargetCountdownTimerSoundID)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Creates and maintains a large copy of the pickup which is passed in, making a large version of it which is only visible in top-down mode
PROC PROCESS_TOPDOWN_PICKUP_COPY(INT i)
	OBJECT_INDEX oiPickup = GET_PICKUP_OBJECT(pipickup[i])
	MODEL_NAMES mnPickupModel
	
	VECTOR vRot = GET_CAM_ROT(g_birdsEyeCam, EULER_XYZ)

	IF DOES_ENTITY_EXIST(oiPickup) //If there's a pickup in this slot that we should be maintaining a large version of...
	
		IF DOES_ENTITY_EXIST(oiLargePowerups[i]) //If our large powerup object already exists...
			SET_ENTITY_ROTATION(oiLargePowerups[i], <<270.0, 0.0, vRot.z>>) //Rotate our object to face the top-down camera
			
			SET_ENTITY_VISIBLE(oiLargePowerups[i], bUsingBirdsEyeCam) //Large object visible only in top-down
			SET_ENTITY_VISIBLE(oiPickup, !bUsingBirdsEyeCam) //Real pickup visible only in third person
		ELSE
			mnPickupModel = GET_LARGE_VERSION_OF_POWERUP(GET_ENTITY_MODEL(oiPickup))

			REQUEST_MODEL(mnPickupModel)
			
			IF HAS_MODEL_LOADED(mnPickupModel)
				oiLargePowerups[i] = CREATE_OBJECT(mnPickupModel, GET_ENTITY_COORDS(oiPickup) + <<0.0, 0.0, -3.0>>, FALSE, FALSE, FALSE)
				PRINTLN("PROCESS_TOPDOWN_PICKUP_COPY - Large fake pickup object with model ", GET_MODEL_NAME_FOR_DEBUG(mnPickupModel), " has been created! (Pickup ", i, ")")
				
				SET_ENTITY_VISIBLE(oiLargePowerups[i], FALSE) //Make it invisible to start with
				SET_ENTITY_LOD_DIST(oiLargePowerups[i], 99999) //Give it a huge LOD distance
				SET_MODEL_AS_NO_LONGER_NEEDED(mnPickupModel)
			ELSE
				PRINTLN("PROCESS_TOPDOWN_PICKUP_COPY - Loading model ", GET_MODEL_NAME_FOR_DEBUG(mnPickupModel))
			ENDIF
		ENDIF
	ELSE
		//If our large powerup object still exists even while the real pickup doesn't, delete it now
		IF DOES_ENTITY_EXIST(oiLargePowerups[i])
			PRINTLN("PROCESS_TOPDOWN_PICKUP_COPY - Deleting large powerup for pickup ", i)
			DELETE_OBJECT(oiLargePowerups[i])
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_STATIC_PICKUPS(BOOL bForceRotation = TRUE)
	INT i 
	FOR i = 0 TO FMMC_MAX_WEAPONS - 1
		IF DOES_ENTITY_EXIST(GET_PICKUP_OBJECT(pipickup[i]))
			
			//Making the powerups face upwards
			IF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciBIRDS_EYE_ENABLED) OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType))
			AND DOES_CAM_EXIST(g_birdsEyeCam)
				
				IF bForceRotation
					VECTOR vRot = GET_CAM_ROT(g_birdsEyeCam, EULER_XYZ)
					SET_ENTITY_ROTATION(GET_PICKUP_OBJECT(pipickup[i]), <<270.0,0,vRot.z>>)
				ENDIF
			ELSE
				IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
				AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPlacedBitset, ciFMMC_WEP_Rotation_Override)
					SET_ENTITY_ROTATION(GET_PICKUP_OBJECT(pipickup[i]), <<0,0,0>>)
				ENDIF
			ENDIF
			
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
				
				MODEL_NAMES mnPickupModel = GET_ENTITY_MODEL(GET_PICKUP_OBJECT(pipickup[i]))
				
				//Increases
				IF IS_DROP_THE_BOMB_BOMBCOUNT_INCREASE_PICKUP(mnPickupModel)
					IF (MC_playerBD_1[iLocalPart].iBmbCurrentAmountOfBombs) >= ciBMB_MAX_TOTAL_BOMBS_PER_PLAYER
						SET_PICKUP_UNCOLLECTABLE(pipickup[i], TRUE)
					ELSE
						SET_PICKUP_UNCOLLECTABLE(pipickup[i], FALSE)
					ENDIF
				ENDIF
				
				IF IS_DROP_THE_BOMB_BOMBLENGTH_INCREASE_PICKUP(mnPickupModel)
					IF (MC_playerBD_1[iLocalPart].iBmbCurrentRadius) >= ciDROPTHEBOMB_Max_Bomb_Length
						SET_PICKUP_UNCOLLECTABLE(pipickup[i], TRUE)
					ELSE
						SET_PICKUP_UNCOLLECTABLE(pipickup[i], FALSE)
					ENDIF
				ENDIF
				
				//Decreases
				IF IS_DROP_THE_BOMB_BOMBCOUNT_DECREASE_PICKUP(mnPickupModel)
					IF (MC_playerBD_1[iLocalPart].iBmbCurrentAmountOfBombs) = 1
						SET_PICKUP_UNCOLLECTABLE(pipickup[i], TRUE)
					ELSE
						SET_PICKUP_UNCOLLECTABLE(pipickup[i], FALSE)
					ENDIF
				ENDIF
				
				IF IS_DROP_THE_BOMB_BOMBLENGTH_DECREASE_PICKUP(mnPickupModel)
					IF (MC_playerBD_1[iLocalPart].iBmbCurrentRadius) = 1
						SET_PICKUP_UNCOLLECTABLE(pipickup[i], TRUE)
					ELSE
						SET_PICKUP_UNCOLLECTABLE(pipickup[i], FALSE)
					ENDIF
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
				TEXT_LABEL_63 tlPowerupFinder = "Powerup "
				tlPowerupFinder += i
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(GET_PICKUP_OBJECT(pipickup[i]), tlPowerupFinder, 1.5, 255, 255, 255, 255)
			ENDIF
			#ENDIF
		ENDIF
		
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
			IF DOES_CAM_EXIST(g_birdsEyeCam)
				PROCESS_TOPDOWN_PICKUP_COPY(i)
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

#IF IS_DEBUG_BUILD
PROC DRAW_BMBFB_DEBUG()
	
	BOOL bShowDebug = bbombfootballdebug
	BOOL bExtendedDebug = SHOULD_SHOW_BMBFB_EXTRA_DEBUG()
	
	IF bShowDebug
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	ENDIF
	
	IF IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_IN_BMBFB_SUDDEN_DEATH)
	AND bExtendedDebug
		DRAW_DEBUG_TEXT_2D("SBBOOL7_IN_BMBFB_SUDDEN_DEATH", <<0.25, 0.25, 0.5>>, 255, 255, 0)
	ENDIF

	IF bIsLocalPlayerHost
	AND bExtendedDebug
		DRAW_DEBUG_TEXT_2D("HOST", <<0.05, 0.05, 0.5>>, 255, 255, 0)
	ENDIF
	
	INT i
	
	FOR i = 0 TO FMMC_MAX_NUM_OBJECTS - 1
		IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
		OR bShowDebug
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[i])
			
				#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 tlFB = "f"
				tlFB += i
				
				IF IS_BIT_SET(MC_serverBD_3.iBombFB_ExplodedBS, i)
					tlFB += " (Exploded)"
				ENDIF
				IF IS_BIT_SET(MC_serverBD_3.iBombFB_GoalProcessedBS, i)
					tlFB += " (Goal'd)"
				ENDIF
				
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[i]), tlFB, 0.1)
				#ENDIF
			ENDIF
		
		
//			IF GET_LAST_PART_TO_HIT_FOOTBALL(i) = iLocalPart
//				TEXT_LABEL_63 tlDB = "Last one to hit ball "
//				tlDB += i
//				DRAW_DEBUG_TEXT_ABOVE_ENTITY(LocalPlayerPed, tlDB, 0.5)
//			ENDIF
			
//			IF GET_LAST_PART_TO_HIT_FOOTBALL_ON_TEAM(i, MC_playerBD[iLocalPart].iteam) = iLocalPart
//				DRAW_DEBUG_TEXT_ABOVE_ENTITY(LocalPlayerPed, "Last to hit a ball on my team!", 0.25, 255, 255, 255)
//			ENDIF
		ENDIF
		
		IF IS_BIT_SET(MC_serverBD_3.iBombFB_ExplodedBS, i)
			PRINTLN("[BMBFB_SPAM][BALLSTATUS] - Football ", i, " iBombFB_ExplodedBS is set")
		ENDIF
		
		IF IS_BIT_SET(MC_serverBD_3.iBombFB_GoalProcessedBS, i)
			PRINTLN("[BMBFB_SPAM][BALLSTATUS] - Football ", i, " iBombFB_GoalProcessedBS is set")
		ENDIF
	ENDFOR
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD5)
	AND IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD1)
		BROADCAST_FMMC_BMBFB_EXPLODE_BALL(0, GET_TEAM_THAT_WILL_SCORE_FROM_BALL_EXPLODING(0))
	ENDIF
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD4)
	AND IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD1)
		INCREMENT_SERVER_TEAM_SCORE(0, 0, 1)
		DRAW_DEBUG_TEXT_2D("INCREMENT_SERVER_TEAM_SCORE", <<0.5, 0.5, 0.5>>)
	ENDIF
	
	/////////////////////////////////// EXTENDED DEBUG /////////////////////////////////////////////////
	IF NOT bExtendedDebug
		EXIT
	ENDIF
	
	TEXT_LABEL_63 tlDebugTextTimer = "My Score: "
	tlDebugTextTimer += MC_Playerbd[iLocalPart].iPlayerScore
	DRAW_DEBUG_TEXT_2D(tlDebugTextTimer, <<0.1, 0.58, 0.5>>)
	
	TEXT_LABEL_63 tlDebugFullyScored = ""
	tlDebugFullyScored += iTeamPotentialExplosionsThisMiniRound[0]
	tlDebugFullyScored += " | "
	tlDebugFullyScored += iTeamPotentialExplosionsThisMiniRound[1]
	DRAW_DEBUG_TEXT_2D(tlDebugFullyScored, <<0.1, 0.615, 0.5>>, 255)
	
	TEXT_LABEL_63 tlDebugTextTimer2 = "Goals: "
	tlDebugTextTimer2 += MC_playerBD_1[iLocalPart].iBmbFb_Goals
	DRAW_DEBUG_TEXT_2D(tlDebugTextTimer2, <<0.1, 0.66, 0.5>>)
	
	IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
		FOR i = 0 TO FMMC_MAX_TEAMS - 1
			
			INT j
			
			FOR j = 0 TO FMMC_MAX_NUM_OBJECTS - 1
				
				TEXT_LABEL_63 tlDB = "Football "
				tlDB += j
				tlDB += " 'held by' Team "
				tlDB += i
				IF IS_BIT_SET(MC_serverBD.iObjAtYourHolding[i], j)
					DRAW_DEBUG_TEXT_2D(tlDB, <<0.25 + (TO_FLOAT(i) * 0.2), 0.1 + (TO_FLOAT(j) * 0.02), 0.5>>)
				ENDIF
			ENDFOR
		ENDFOR
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_GIVEN_BFOOTBALL_LOCAL_SCORE_THIS_MINIROUND)
		DRAW_DEBUG_TEXT_ABOVE_ENTITY(LocalPlayerPed, "LBOOL29_GIVEN_BFOOTBALL_LOCAL_SCORE_THIS_MINIROUND!", 1.5)
	ENDIF
ENDPROC
#ENDIF

PROC PROCESS_ARENA_MODES()
	IF NOT CONTENT_IS_USING_ARENA()
		EXIT
	ENDIF
ENDPROC
// [FIX_2020_CONTROLLER] - A lot of these adversary mode process functions could be moved to a new "FM_Mission_Controller_AdversaryMode.sch header file.
PROC PROCESS_BOMB_FOOTBALL_MODE()
	IF NOT IS_THIS_BOMB_FOOTBALL()
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	DRAW_BMBFB_DEBUG()
	#ENDIF
	
	SET_PED_RESET_FLAG(LocalPlayerPed, PRF_SuppressInAirEvent, TRUE)
	
	IF IS_ENTITY_ALIVE(LocalPlayerPed)
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			VEHICLE_INDEX viPlayerVeh = GET_VEHICLE_PED_IS_USING(LocalPlayerPed)
			
			IF IS_ENTITY_ALIVE(viPlayerVeh)
				IF GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()
					FREEZE_ENTITY_POSITION(viPlayerVeh, FALSE)
				ELSE
					SET_ENTITY_VELOCITY(viPlayerVeh, <<0.0, 0.0, 0.0>>)
					FREEZE_ENTITY_POSITION(viPlayerVeh, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
	AND GET_REMAINING_TIME_ON_RULE(0, MC_serverBD_4.iCurrentHighestPriority[0]) > 0
	AND GET_REMAINING_TIME_ON_RULE(0, MC_serverBD_4.iCurrentHighestPriority[0]) <= 55000
	
		IF iTeamPotentialExplosionsThisMiniRound[0] != iTeamPotentialExplosionsThisMiniRound[1]
		AND NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_BMBFB_BALLS_HAVE_BEEN_UNEVEN)
			SET_BIT(iLocalBoolCheck30, LBOOL30_BMBFB_BALLS_HAVE_BEEN_UNEVEN)
			PRINTLN("[BMBFBAnnouncer] LBOOL30_BMBFB_BALLS_HAVE_BEEN_UNEVEN")
		ENDIF
		
		IF GET_REMAINING_TIME_ON_RULE(0, MC_serverBD_4.iCurrentHighestPriority[0]) <= 30000
		AND GET_REMAINING_TIME_ON_RULE(0, MC_serverBD_4.iCurrentHighestPriority[0]) > 0
		AND iTeamPotentialExplosionsThisMiniRound[0] > 0
		AND iTeamPotentialExplosionsThisMiniRound[1] > 0
		AND iTeamPotentialExplosionsThisMiniRound[0] = iTeamPotentialExplosionsThisMiniRound[1]
			BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_ModeSpecific, g_iAA_PlaySound_Detonation_EvenScoreAfter30Sec, TRUE)
			PRINTLN("[BMBFBAnnouncer] g_iAA_PlaySound_Detonation_EvenScoreAfter30Sec")
		ENDIF
	
		IF iTeamPotentialExplosionsThisMiniRound[0] > 0
		OR iTeamPotentialExplosionsThisMiniRound[1] > 0
			IF iTeamPotentialExplosionsThisMiniRound[0] = 0
				BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_ModeSpecific, g_iAA_PlaySound_Detonation_Team2FullyScored, TRUE)
				PRINTLN("[BMBFBAnnouncer] g_iAA_PlaySound_Detonation_Team2FullyScored")
			ELIF iTeamPotentialExplosionsThisMiniRound[1] = 0
				BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_ModeSpecific, g_iAA_PlaySound_Detonation_Team1FullyScored, TRUE)
				PRINTLN("[BMBFBAnnouncer] g_iAA_PlaySound_Detonation_Team1FullyScored")
			ENDIF
		ENDIF
	ENDIF
	
	iTeamPotentialExplosionsThisMiniRound[0] = 0
	iTeamPotentialExplosionsThisMiniRound[1] = 0
	
	IF MC_serverBD_3.iBombFB_OfficialRuleIndex = -1
	AND iBmbFBCurrentMiniRound < 1
		START_BOMB_FOOTBALL_MINIROUND()
	ENDIF
	
	//Staggered loop
	IF DOES_ENTITY_EXIST(GET_BOMB_FOOTBALL_BOMB(iBombStaggeredIndex))
		APPLY_BOMB_FOOTBALL_PROPERTIES_TO_FOOTBALL(GET_BOMB_FOOTBALL_BOMB(iBombStaggeredIndex), iBombStaggeredIndex)
	ENDIF
	iBombStaggeredIndex++
	IF iBombStaggeredIndex >= FMMC_MAX_NUM_OBJECTS
		iBombStaggeredIndex = 0
	ENDIF
ENDPROC

// [FIX_2020_CONTROLLER] - A lot of these adversary mode process functions could be moved to a new "FM_Mission_Controller_AdversaryMode.sch header file.
PROC PROCESS_DROP_THE_BOMB_MODE()
	IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD6)
			DESTROY_ALL_BOMBABLE_BLOCKS()
		ENDIF
	ENDIF
	#ENDIF
	
	REPOSITION_PICKUPS_FOR_BIRDSEYE_CAM()
	SET_LIGHT_OVERRIDE_MAX_INTENSITY_SCALE(fBloomScale)
	PROCESS_STATIC_PICKUPS(IS_BIRDS_EYE_CAM_ACTIVE())
	
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		PROCESS_CARGO_BOMBS()
	ENDIF
	
	IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
		IF NOT IS_BIT_SET(iLocalBoolCheck28, LBOOL28_STARTING_SPECTATOR_IN_TOP_DOWN)
			bUsingBirdsEyeCam = TRUE
			SET_BIT(iLocalBoolCheck28, LBOOL28_STARTING_SPECTATOR_IN_TOP_DOWN)
		ELSE
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_FRONTEND_SELECT))
				PRINTLN("PROCESS_DROP_THE_BOMB_MODE - INPUT_FRONTEND_SELECT has been pressed, disabling bUsingBirdsEyeCam now")
				bUsingBirdsEyeCam = FALSE
			ENDIF
		ENDIF
		
		IF IS_PLAYER_SCTV(LocalPlayer)
			IF NOT GET_ENTITY_COLLISION_DISABLED(LocalPlayerPed)
				SET_ENTITY_COLLISION(LocalPlayerPed, FALSE)
				PRINTLN("PROCESS_DROP_THE_BOMB_MODE - Disabled collision for local player as SCTV spectator")
			ENDIF
		ENDIF
	ENDIF
	
	IF (g_bCelebrationScreenIsActive
	OR NOT GET_TOGGLE_PAUSED_RENDERPHASES_STATUS())
	AND bUsingBirdsEyeCam
		PRINTLN("PROCESS_DROP_THE_BOMB_MODE - Turning off bUsingBirdsEyeCam due to g_bCelebrationScreenIsActive")
		CLEANUP_BIRDSEYE_CAM()
	ENDIF
	
	IF bUsingBirdsEyeCam
		fCurrentSphereRadius_VisualOffset = ciSUMO_SD_SIZE_OFFSET_TOPDOWN //Gives the sphere a different size when in top down to account for the size looking wrong from above
	ELSE
		fCurrentSphereRadius_VisualOffset = 0.0
	ENDIF

	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_VEH_CIN_CAM)
	DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_VEH_CIN_CAM)
	SET_CINEMATIC_MODE_ACTIVE(FALSE)
	
ENDPROC

// [FIX_2020_CONTROLLER] - A lot of these adversary mode process functions could be moved to a new "FM_Mission_Controller_AdversaryMode.sch header file.
PROC PROCESS_SUMO_REMIX_MODE()
	IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_SUMO_RUN(g_FMMC_STRUCT.iAdversaryModeType)
		EXIT
	ENDIF
	
	IF bIsLocalPlayerHost
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iLocalPart].iteam)
			REINIT_NET_TIMER(tdSumoTeamsLeftTimer)
			PRINTLN("PROCESS_SUMO_REMIX_MODE - Reinitialising tdSumoTeamsLeftTimer for new rule")
		ENDIF
		
		IF HAS_NET_TIMER_STARTED(tdSumoTeamsLeftTimer)
			IF HAS_NET_TIMER_EXPIRED(tdSumoTeamsLeftTimer, ciSumoTeamsLeftTimerLength)
				INT iTeam, iTeamCount, iLastTeam
				FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
					IF DOES_TEAM_HAVE_ANY_PLAYERS_ALIVE(iTeam)
						iTeamCount++
						iLastTeam = iTeam
					ENDIF
				ENDFOR
				 
				IF iTeamCount = 1
					MC_serverBD.iNumberOfPlayingTeams = iTeamCount
					MC_serverBD.iLastTeamAlive = iLastTeam
					PRINTLN("PROCESS_SUMO_REMIX_MODE - MODE POTENTIALLY ENDING ON THIS RULE - MC_serverBD.iNumberOfPlayingTeams is now: ", MC_serverBD.iNumberOfPlayingTeams)
					PRINTLN("PROCESS_SUMO_REMIX_MODE - Only one team left. MC_serverBD.iLastTeamAlive = ", MC_serverBD.iLastTeamAlive)
				ENDIF
				
				RESET_NET_TIMER(tdSumoTeamsLeftTimer)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

// [FIX_2020_CONTROLLER] - A lot of these adversary mode process functions could be moved to a new "FM_Mission_Controller_AdversaryMode.sch header file.
PROC PROCESS_TRADING_PLACES_REMIX_MODE()
	IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TRADING_PLACES_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_PLAYED_CELEB_PRE_LOAD_FOR_HUD_CLEAR)
	AND GET_ENTITY_ALPHA(LocalPlayerPed) != 255
		SET_ENTITY_ALPHA(LocalPlayerPed, 255, FALSE)
		PRINTLN("PROCESS_TRADING_PLACES_REMIX_MODE - Resetting alpha for beast.")
	ENDIF
	
ENDPROC

// [FIX_2020_CONTROLLER] - A lot of these adversary mode process functions could be moved to a new "FM_Mission_Controller_AdversaryMode.sch header file.
PROC PROCESS_STUNTING_PACK()
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_STUNTING_PACK(g_FMMC_STRUCT.iAdversaryModeType)
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed) 
			PRINTLN("PROCESS_STUNTING_PACK - Setting flags due to stunting pack")
			VEHICLE_INDEX v = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
			
			SET_VEHICLE_DOORS_LOCKED(v, VEHICLELOCK_LOCKED_PLAYER_INSIDE)
		ENDIF
		
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_GetOutUndriveableVehicle, FALSE)
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_FallsOutOfVehicleWhenKilled, FALSE)
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_GetOutBurningVehicle, FALSE)
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_RunFromFiresAndExplosions, FALSE)
		
		SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisableAutoForceOutWhenBlowingUpCar, TRUE)
		
		IF IS_BIT_SET(iLocalBoolCheck28, LBBOOL28_REACHED_END_OF_STUNTING_PACK)
		AND NOT IS_BIT_SET(iLocalBoolCheck29, LBOOL29_DONE_STUNTING_PACK_ENDING_FIREWORKS)
			SET_BIT(iLocalBoolCheck29, LBOOL29_DONE_STUNTING_PACK_ENDING_FIREWORKS)
			
			INT iProp
			FOR iProp = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfProps - 1
				IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = IND_PROP_FIREWORK_01
					IF NOT IS_BIT_SET(iCreatedFireworkBitset[GET_LONG_BITSET_INDEX(iProp)], GET_LONG_BITSET_BIT(iProp))
						IF CREATE_FIREWORK_FX(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAlarmDelay, oiProps[iProp], g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAlarmSound)
							SET_BIT(iCreatedFireworkBitset[GET_LONG_BITSET_INDEX(iProp)], GET_LONG_BITSET_BIT(iProp))
							PRINTLN("PROCESS_STUNTING_PACK - Firework has been created for prop ", iProp)
						ELSE
							//Clear this bit so that we keep trying
							CLEAR_BIT(iLocalBoolCheck29, LBOOL29_DONE_STUNTING_PACK_ENDING_FIREWORKS) 
							PRINTLN("PROCESS_STUNTING_PACK - Attempting to create firework for prop ", iProp, " but failing this time")
						ENDIF
					ELSE
						PRINTLN("PROCESS_STUNTING_PACK - Prop ", iProp, " has either not loaded the firework effect or isn't a firework")
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
		
		IF MC_playerBD[iLocalPart].iteam = ciHuntingPackRemix_RUNNER						
			IF MPGlobals.g_bSuppressWastedBM = FALSE
				PRINTLN("PROCESS_STUNTING_PACK - Suppress wasted due to stunting pack")
				SET_BIG_MESSAGE_SUPPRESS_WASTED(TRUE)
			ENDIF
			
			IF IS_PED_IN_LOCATION(LocalPlayerPed, 0,  ciHuntingPackRemix_RUNNER)
				IF NOT IS_BIT_SET(iLocalBoolCheck28, LBBOOL28_REACHED_END_OF_STUNTING_PACK)
					SET_BIT(iLocalBoolCheck28, LBBOOL28_REACHED_END_OF_STUNTING_PACK)
					PRINTLN("PROCESS_STUNTING_PACK - Setting LBBOOL28_REACHED_END_OF_STUNTING_PACK because we have reached our location!")
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(iLocalBoolCheck28, LBBOOL28_REACHED_END_OF_STUNTING_PACK)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_LR, TRUE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE, TRUE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE, TRUE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HANDBRAKE, TRUE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// [FIX_2020_CONTROLLER] - A lot of these adversary mode process functions could be moved to a new "FM_Mission_Controller_AdversaryMode.sch header file.
PROC PROCESS_MONSTER_JAM_MODE()
	
IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_MONSTER_JAM(g_FMMC_STRUCT.iAdversaryModeType)
	EXIT
ENDIF
	
INT iTeam = MC_playerBD[iPartToUse].iteam
	IF iTeam > -1 AND iTeam < FMMC_MAX_TEAMS
		INT iTeamLoop
		INT iPart
			// [MJL] Clearing this in case a monser truck destruction suddenly reduces time, causing "Final minute" to play much closer to zero	
			IF HAS_NET_TIMER_STARTED(MC_serverBD_3.tdObjectiveLimitTimer[iTeam])
				IF GET_REMAINING_TIME_ON_RULE(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam]) <= 55000		
					CLEAR_ARENA_ANNOUNCER_BS_MODE_SPECIFIC(g_iAA_PlaySound_MonsterJam_FinalMinute)
				ELIF GET_REMAINING_TIME_ON_RULE(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam]) <= 60000
					SET_ARENA_ANNOUNCER_BS_MODE_SPECIFIC(g_iAA_PlaySound_MonsterJam_FinalMinute, TRUE, default, default, TRUE)
				ENDIF
			ENDIF
			
			// Final contender remaining.
			IF bIsLocalPlayerHost
			
				INT iPlayersLeft[FMMC_MAX_TEAMS]
				INT iPlayersMax[FMMC_MAX_TEAMS]
				FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
					PARTICIPANT_INDEX piPart = INT_TO_PARTICIPANTINDEX(iPart)
					IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
						PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)
						IF NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(piPlayer)
						AND NOT IS_PLAYER_SCTV(piPlayer)
							IF MC_playerBD[iPart].iTeam > -1 AND MC_playerBD[iPart].iTeam < FMMC_MAX_TEAMS
								IF IS_BIT_SET(MC_playerBD[iPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
								AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_PLAYER_FAIL)
								AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES)
								AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_FINISHED)
									iPlayersLeft[MC_playerBD[iPart].iTeam]++
								ENDIF
								iPlayersMax[MC_playerBD[iPart].iTeam]++
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
				
				iTeamLoop = 0 	// Contenders
				IF iPlayersLeft[iTeamLoop] = 1
				AND iPlayersMax[iTeamLoop] > 0
					IF NOT IS_ARENA_ANNOUNCER_BS_MODE_SPECIFIC_SET(g_iAA_PlaySound_MonsterJam_FirstBloodContenderNormal)
					AND NOT IS_ARENA_ANNOUNCER_BS_MODE_SPECIFIC_SET(g_iAA_PlaySound_MonsterJam_FirstBloodContenderCrushed)
						BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_ModeSpecific, g_iAA_PlaySound_MonsterJam_LastContenderAlive, TRUE) 
						SET_BIT(iLocalBoolCheck30, LBOOL30_LAST_CONTENDER_MONSTER_JAM)
					ENDIF
				ENDIF					
			ENDIF				
		
	ENDIF	
ENDPROC

PROC PROCESS_MISSION_ORBITAL_CANNON()
	
	IF HAS_TEAM_FAILED(MC_PlayerBD[iLocalPart].iTeam)
	OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_FAIL)
	OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_FINISHED)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet,SBBOOL_MISSION_OVER)
		PRINTLN("[PROCESS_MISSION_ORBITAL_CANNON] - Setting g_bMissionPlacedOrbitalCannon to FALSE.")
		g_bMissionPlacedOrbitalCannon = FALSE
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iMissionModeBit, ciMISSION_USING_ORBITAL_CANNON)
	AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet,SBBOOL_MISSION_OVER)
	AND g_bMissionPlacedOrbitalCannon
	AND NOT IS_SCREEN_FADING_OUT()		
		DRAW_PLACED_PED_TRACKERS()
		DRAW_PLACED_VEHICLE_TRACKERS()
	ENDIF
ENDPROC

FUNC FLOAT GET_PTFX_WING_VALUE(MODEL_NAMES mn)
	SWITCH mn
		CASE MOLOTOK	RETURN 1.0
		CASE LAZER		RETURN 0.0

	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

PROC PROCESS_INTERIORS_EVERY_FRAME()
	INT iTeam = MC_PlayerBD[iPartToUse].iteam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule < FMMC_MAX_RULES
		INTERIOR_INSTANCE_INDEX playerInterior = GET_INTERIOR_FROM_ENTITY(LocalPlayerPed)
		
		VECTOR vPos
		INT interiorNameHash = -1
		INT interiorGroupId = -1
		IF IS_VALID_INTERIOR(playerInterior)
			GET_INTERIOR_LOCATION_AND_NAMEHASH(playerInterior, vPos, interiorNameHash)
			interiorGroupId = GET_INTERIOR_GROUP_ID(playerInterior)
		ELSE
			playerInterior = NULL
		ENDIF
		
		PROCESS_EVERY_FRAME_INTERIOR_AUDIO(playerInterior, interiorNameHash, interiorGroupId)

		// HEISTS 2 - SUBMARINE
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SUB)
		AND playerInterior != NULL
		AND interiorNameHash = HASH("xm_x17dlc_int_sub") // GET_INTERIOR_AT_COORDS(<<512.7328, 4881.1152, -63.5867>>)
		
			// Use room key to narrow it down
			INT roomKey = GET_ROOM_KEY_FROM_ENTITY(LocalPlayerPed)
			IF roomKey = HASH("Rm_Escape")
				SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisableEasyLadderConditions, TRUE)
				CDEBUG3LN(DEBUG_NET_MISSION,"[RCC MISSION] - PROCESS_INTERIORS_EVERY_FRAME - Player ped reset flag set this frame, PCF_DisableEasyLadderConditions")
			ENDIF

		ENDIF
		
		// HEISTS 2 - HANGAR
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_HANGAR)
		
			IF NOT DOES_ENTITY_EXIST(oiHangarWayfinding)
			
				INTERIOR_DATA_STRUCT structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_HANGAR)			
				INTERIOR_INSTANCE_INDEX iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
				IF NOT IS_VALID_INTERIOR(iInteriorIndex)
					ASSERTLN("[RCC MISSION] - PROCESS_INTERIORS_EVERY_FRAME - iInteriorIndex is INVALID - cannot create wayfinding prop")
				ELSE
					VECTOR vHangarCoords
					INT vHangarHash
					GET_INTERIOR_LOCATION_AND_NAMEHASH(iInteriorIndex, vHangarCoords, vHangarHash)

					IF NOT IS_VECTOR_ZERO(vHangarCoords)					
						MODEL_NAMES wayfindingProp = INT_TO_ENUM(MODEL_NAMES, HASH("xm_Prop_xm17_Wayfinding"))
						REQUEST_MODEL(wayfindingProp)
						IF HAS_MODEL_LOADED(wayfindingProp)
						AND IS_INTERIOR_READY(iInteriorIndex)
							oiHangarWayfinding = CREATE_OBJECT_NO_OFFSET(wayfindingProp, vHangarCoords, FALSE, FALSE)
							RETAIN_ENTITY_IN_INTERIOR(oiHangarWayfinding, iInteriorIndex)
							FORCE_ROOM_FOR_ENTITY(oiHangarWayfinding, iInteriorIndex, HASH("GtaMloRoom001"))
							SET_MODEL_AS_NO_LONGER_NEEDED(wayfindingProp)
							PRINTLN("[RCC MISSION] - PROCESS_INTERIORS_EVERY_FRAME - Hangar - Created wayfinding prop @ coords: ", vHangarCoords)
						ENDIF					
					ENDIF
				ENDIF
			ENDIF		
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO)
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetTwentyFour, ciOptionsBS24_EnableCasino_InteriorScripts)
				MAINTAIN_PRIZE_VEHICLE_MISSION()
			ENDIF
			
			#IF IS_DEBUG_BUILD
			g_PlayerBlipsData.bDebugBlipOutput = TRUE
			#ENDIF
			PRINTLN("interiorNameHash: ", interiorNameHash, " Casino hash: ", HASH("vw_dlc_casino_main"))
			IF IS_VALID_INTERIOR(playerInterior)
			AND interiorNameHash = HASH("vw_dlc_casino_main")
				IF NOT g_bInInteriorThatRequiresInstantFadeout
					PRINTLN("[RCC MISSION] - PROCESS_INTERIORS_EVERY_FRAME - g_bInInteriorThatRequiresInstantFadeout = TRUE")
					g_bInInteriorThatRequiresInstantFadeout = TRUE
				ENDIF
			ELSE
				IF g_bInInteriorThatRequiresInstantFadeout
					PRINTLN("[RCC MISSION] - PROCESS_INTERIORS_EVERY_FRAME - g_bInInteriorThatRequiresInstantFadeout = FALSE")
					g_bInInteriorThatRequiresInstantFadeout = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_LOCK_CASINO_DOORS)
		AND (GET_FRAME_COUNT() % 10) = 0
			IF IS_ENTITY_ALIVE(LocalPlayerPed)
				VECTOR vPlayerPos = GET_ENTITY_COORDS(LocalPlayerPed)			
				IF VDIST2(vPlayerPos, <<925.04, 46.48, 80.0960>>) < 100.0
					ADD_CASINO_DOOR_TO_SYSTEM_AND_LOCK(CED_LEFT_SIDE_LEFT_DOOR, TRUE)
					ADD_CASINO_DOOR_TO_SYSTEM_AND_LOCK(CED_LEFT_SIDE_RIGHT_DOOR, TRUE)
					ADD_CASINO_DOOR_TO_SYSTEM_AND_LOCK(CED_CENTRAL_LEFT_DOOR, TRUE)
					ADD_CASINO_DOOR_TO_SYSTEM_AND_LOCK(CED_CENTRAL_RIGHT_DOOR, TRUE)
					ADD_CASINO_DOOR_TO_SYSTEM_AND_LOCK(CED_RIGHT_SIDE_LEFT_DOOR, TRUE)
					ADD_CASINO_DOOR_TO_SYSTEM_AND_LOCK(CED_RIGHT_SIDE_RIGHT_DOOR, TRUE)				
					SET_BIT(iLocalBoolCheck31, LBOOL31_LOCK_CASINO_DOORS)
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_ENABLE_DESTROYED_CASINO_IPL_DOORS)
			PRINTLN("[RCC MISSION] PROCESS_MOCAP_CUTSCENE - ci_CSBS2_UseDamagedCasinoDoorsIPL is SET, calling TOGGLE_HIDE_CASINO_FRONT_DOOR_IPL(TRUE)")
			TOGGLE_HIDE_CASINO_FRONT_DOOR_IPL(TRUE)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_CASINO_ALARMS()

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_TriggerMissionAlarmOnAggro)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_TriggerMissionAlarmAtStart)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_CASINO_ALARM_STARTED)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_TriggerMissionAlarmAtStart)
		IF NOT HAS_ANY_TEAM_TRIGGERED_AGGRO()
			EXIT
		ENDIF
	ELSE
		PRINTLN("[JS][ALARM] - PROCESS_CASINO_ALARMS - Casino Alarm set to trigger on start")
	ENDIF
	
	PRINTLN("[JS][ALARM] - PROCESS_CASINO_ALARMS - Starting Casino Alarm Zones")
	SET_AMBIENT_ZONE_STATE_PERSISTENT("AZ_H3_Casino_Alarm_Zone_01_Exterior", TRUE, TRUE)
	SET_AMBIENT_ZONE_STATE_PERSISTENT("AZ_H3_Casino_Alarm_Zone_02_Interior", TRUE, TRUE)
	SET_BIT(iLocalBoolCheck32, LBOOL32_CASINO_ALARM_STARTED)
	
ENDPROC





FUNC BOOL CREATE_DRONE_PICKUP()
	INT iModelHash =  HASH("ch_Prop_Casino_Drone_01a") 
	
	
	REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, iModelHash) )
	
	IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, iModelHash))
		IF CAN_REGISTER_MISSION_OBJECTS(1)
			IF NOT DOES_ENTITY_EXIST(oiDronePickup)
				VECTOR tempDronePos =  GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].vDroneCoords 
				GET_GROUND_Z_FOR_3D_COORD(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].vDroneCoords, tempDronePos.z)
				
				vDronePosCache = tempDronePos
				tempDronePos.z  = tempDronePos.z + 1.0
				
				
				oiDronePickup = CREATE_OBJECT(INT_TO_ENUM(MODEL_NAMES, iModelHash), tempDronePos, FALSE, FALSE, TRUE)
				
				PRINTLN("PROCESS_DRONE_USAGE - CREATE DRONE PICKUP: CREATING")
			ENDIF
			
			IF DOES_ENTITY_EXIST(oiDronePickup)
				SET_ENTITY_COLLISION(oiDronePickup, FALSE)
				SET_USE_KINEMATIC_PHYSICS(oiDronePickup, TRUE)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, iModelHash))
				
				
				IF NOT DOES_BLIP_EXIST(biDronePickup)
					biDronePickup = ADD_BLIP_FOR_ENTITY(oiDronePickup)
					SET_BLIP_SPRITE(biDronePickup, RADAR_TRACE_BAT_DRONE)
				ENDIF
					
				PRINTLN("PROCESS_DRONE_USAGE - CREATE DRONE PICKUP: SUCCESS")
				RETURN TRUE
			ENDIF
		ELSE
			PRINTLN("PROCESS_DRONE_USAGE - CAN'T REGISTER PROP")
		ENDIF
	ELSE
		PRINTLN("PROCESS_DRONE_USAGE - CREATE DRONE PICKUP: LOADING ASSET")
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC BOOL HAS_PLAYER_COLLECTED_DRONE_PICKUP()
	IF IS_BIT_SET(iLocalBoolCheck33, LBOOL33_DRONE_PICKUP_SPAWNED)
		IF DOES_ENTITY_EXIST(oiDronePickup)
		
			IF IS_ENTITY_AT_COORD(LocalPlayerPed, vDronePosCache, <<2.0,2.0,2.0>>,FALSE, TRUE,TM_ON_FOOT )
				PRINTLN("PROCESS_DRONE_USAGE - HAS_PLAYER_COLLECTED_DRONE_PICKUP: SUCCESS")				
				CLEANUP_BLIP(biDronePickup)
				
				DELETE_OBJECT(oiDronePickup)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


PROC REENABLE_DRONE_USAGE()
	CLEAR_BIT(sLEGACYMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciLEGACY_ContinuityGenericTrackingBS_DroneUsed)
	CLEAR_BIT(iLocalBoolCheck32, LBOOL32_USING_DRONE)
	CLEAR_BIT(iLocalBoolCheck33, LBOOL33_DRONE_PICKUP_SPAWNED)
	CLEAR_BIT(iLocalBoolCheck33, LBOOL33_DRONE_PICKUP_COLLECTED)
	OVERRIDE_DRONE_COOL_DOWN_TIMER(0)
	SET_ENABLE_DRONE_USE_FROM_PI_MENU_IN_MISSION(TRUE)
	PRINTLN("[NM] PROCESS_DRONE_USAGE - REENABLE_DRONE_USAGE()")
ENDPROC



PROC PROCESS_DRONE_USAGE()

	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_EnableDroneDeployment)
	OR (g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__STEALTH) AND (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_AllowDroneAfterBranch))
		//Drone has been used.
		IF IS_BIT_SET(iLocalBoolCheck33, LBOOL33_SHOULD_CREATE_PICKUP)
			PRINTLN("PROCESS_DRONE_USAGE - SHOULD CREATE PICKUP")
			IF NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_DRONE_PICKUP_SPAWNED)
				PRINTLN("PROCESS_DRONE_USAGE - SHOULD CREATE PICKUP - NOT LBOOL33_DRONE_PICKUP_SPAWNED ")
				IF CREATE_DRONE_PICKUP()
					SET_BIT(iLocalBoolCheck33, LBOOL33_DRONE_PICKUP_SPAWNED)
					CLEAR_BIT(iLocalBoolCheck33, LBOOL33_SHOULD_CREATE_PICKUP)
				ENDIF
			ENDIF
		ENDIF
		
		
		// Collection logic here.
		IF HAS_PLAYER_COLLECTED_DRONE_PICKUP()
			SET_BIT(iLocalBoolCheck33, LBOOL33_DRONE_PICKUP_COLLECTED)
			PRINTLN("[NM] - PROCESS_DRONE_USAGE - PICKUP COLLECTED")
		ENDIF
		
		IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL33_EMP_BLOCKING_DRONE_ACTIVE)
			IF IS_PHONE_EMP_CURRENTLY_ACTIVE()
				IF IS_LOCAL_PLAYER_USING_DRONE()
					SET_BIT(iLocalBoolCheck33, LBOOL33_SHOULD_CREATE_PICKUP)
					PRINTLN("[NM] - PROCESS_DRONE_USAGE - EMP DISABLED DRONES")
					SET_DRONE_FORCE_EXTERNAL_CLEANING_UP(TRUE)
				ENDIF
				
				SET_BIT(sLEGACYMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciLEGACY_ContinuityGenericTrackingBS_DroneUsed)
				SET_BIT(sLEGACYMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciLEGACY_ContinuityGenericTrackingBS_DroneUsedTelemetryTracker)
				CLEAR_BIT(iLocalBoolCheck32, LBOOL32_USING_DRONE)	
				SET_ENABLE_DRONE_USE_FROM_PI_MENU_IN_MISSION(FALSE)
				SET_BIT(iLocalBoolCheck31, LBOOL33_EMP_BLOCKING_DRONE_ACTIVE)
				PRINTLN("[NM] - PROCESS_DRONE_USAGE - SETTING LBOOL33_EMP_BLOCKING_DRONE_ACTIVE")
				EXIT
			ENDIF
		ELSE
			IF IS_PHONE_EMP_CURRENTLY_ACTIVE()
				EXIT
			ELSE
				//Re-enable the drone
				
				//re enable the option if it's not got a pickup already available.
				IF NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_DRONE_TRANQUILISER_AMMO_ASSIGNED)
					REENABLE_DRONE_USAGE()
				ENDIF
				
				PRINTLN("[NM] - PROCESS_DRONE_USAGE - EMP EXPIRED -  CLEARING LBOOL33_EMP_BLOCKING_DRONE_ACTIVE")
				CLEAR_BIT(iLocalBoolCheck31, LBOOL33_EMP_BLOCKING_DRONE_ACTIVE)
			ENDIF
		ENDIF
		
		
		IF IS_BIT_SET(iLocalBoolCheck33, LBOOL33_DRONE_PICKUP_COLLECTED)
		AND NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL33_EMP_BLOCKING_DRONE_ACTIVE)
			REENABLE_DRONE_USAGE()
		ENDIF
		
		IF HAS_DRONE_BEEN_USED()
		OR IS_BIT_SET(iLocalBoolCheck31, LBOOL33_EMP_BLOCKING_DRONE_ACTIVE)
			EXIT
		ENDIF
		
		IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_USING_DRONE)
			IF NOT IS_LOCAL_PLAYER_USING_DRONE()
				SET_BIT(iLocalBoolCheck33, LBOOL33_SHOULD_CREATE_PICKUP)
				SET_BIT(sLEGACYMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciLEGACY_ContinuityGenericTrackingBS_DroneUsed)
				SET_BIT(sLEGACYMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciLEGACY_ContinuityGenericTrackingBS_DroneUsedTelemetryTracker)
				CLEAR_BIT(iLocalBoolCheck32, LBOOL32_USING_DRONE)
				PRINTLN("[JS][CONTINUITY] - PROCESS_DRONE_USAGE - Finished using drone - setting as used")
				CLEAR_BIT(iLocalBoolCheck33, LBOOL33_DRONE_PICKUP_SPAWNED)
				SET_ENABLE_DRONE_USE_FROM_PI_MENU_IN_MISSION(FALSE)
			ENDIF
		ELSE
			IF IS_LOCAL_PLAYER_USING_DRONE()
				IF NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_DRONE_TRANQUILISER_AMMO_ASSIGNED)
					SET_DRONE_TRANQUILIZER_AMMO(3)
					SET_BIT(iLocalBoolCheck33, LBOOL33_DRONE_TRANQUILISER_AMMO_ASSIGNED)
					PRINTLN("[NM] - PROCESS_DRONE_USAGE - Started using drone - setting as being used")
				ENDIF
				
				PRINTLN("[JS][CONTINUITY] - PROCESS_DRONE_USAGE - Started using drone - setting as being used")
				SET_BIT(iLocalBoolCheck32, LBOOL32_USING_DRONE)
			 	
			ENDIF
		ENDIF
		
		
	ELSE
		//Drones not enabled
		//PRINTLN("[CONTINUITY] - PROCESS_DRONE_USAGE - Drones not set as enabled on this mission")
		EXIT
	ENDIF
	
ENDPROC

// [FIX_2020_CONTROLLER] - A lot of these adversary mode process functions could be moved to a new "FM_Mission_Controller_AdversaryMode.sch header file.
PROC PROCESS_AIR_QUOTA_PTFX()
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRQUOTA(g_FMMC_STRUCT.iAdversaryModeType)
	AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
	AND MC_playerBD[iLocalPart].iteam > -1
		INT iTeam = MC_playerBD[iLocalPart].iteam
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		
		IF iRule > -1 AND iRule < FMMC_MAX_RULES
			INT iScore = MC_serverBD.iScoreOnThisRule[iTeam]
			INT iTargetScore = ROUND(GET_FMMC_OBJECTIVE_SCORE_LIMIT_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTargetScore[iRule], MC_ServerBD.iNumberOfPlayingPlayers[iTeam])*GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING(iTeam, iRule))
			PRINTLN("[AIRQUOTA_PTFX] - iScore: ", iScore, " iTargetScore: ", iTargetScore)
			
			IF iScore = (iTargetScore - 1)
			AND IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)

				IF DOES_ENTITY_EXIST(veh)
				AND NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxAirQuotaLastKillEffect)
					IF HAS_NAMED_PTFX_ASSET_LOADED("scr_xm_aq")
						USE_PARTICLE_FX_ASSET("scr_xm_aq")
						
						FLOAT fScale = 1.0
						VECTOR vOffset = <<0.0, 0.0, 0.0>>
						VECTOR vRot
						STRING sEffectName = "scr_xm_aq_final_kill_plane_delta"
						
						SWITCH GET_ENTITY_MODEL(veh)
							CASE MOLOTOK
							CASE HYDRA
								sEffectName = "scr_xm_aq_final_kill_plane_sweep"
							BREAK
						ENDSWITCH
						
						IF GET_ENTITY_MODEL(veh) = THRUSTER
						OR IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(veh))
							sEffectName = "scr_xm_aq_final_kill_thruster"
						ENDIF
						
						IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(veh))
						AND GET_ENTITY_MODEL(veh) != THRUSTER
							fScale = 2.0
							vRot.x = 90.0
							vRot.y = 0.0
							vRot.z = 0.0
						ENDIF
						
						IF GET_ENTITY_MODEL(veh) = AKULA
							fScale = 6.5
							vOffset.y = 10.0
						ENDIF
						
						ptfxAirQuotaLastKillEffect = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY(sEffectName, veh, vOffset, vRot, fScale, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
						vehAirQuotaLocalVehicle = veh
						PRINTLN("[AIRQUOTA_PTFX] - starting particles for rule ", iRule)
					ELSE
						PRINTLN("[AIRQUOTA_PTFX] - NOT starting networked particles as the asset 'scr_xm_aq' is not loaded!")
					ENDIF
				ELSE
					IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxAirQuotaLastKillEffect)
						PRINTLN("[AIRQUOTA_PTFX] Effect already exists!")
						
						IF DOES_ENTITY_EXIST(veh)
							FLOAT fWingEvolution = 1.0
							
							SWITCH GET_ENTITY_MODEL(veh)
								CASE NOKOTA
									fWingEvolution = 8.0
								BREAK
								
								CASE PYRO
									fWingEvolution = 7.8
								BREAK
							ENDSWITCH
						
							//IF fWingDebug <> 0.0
								//fWingEvolution = fWingDebug
							//ENDIF
							
							SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxAirQuotaLastKillEffect, "wings", fWingEvolution) //GET_PTFX_WING_VALUE(GET_ENTITY_MODEL(veh)))
						ENDIF
					ENDIF
					
					IF vehAirQuotaLocalVehicle = veh
						PRINTLN("[AIRQUOTA_PTFX] vehAirQuotaLocalVehicle = veh")
					ELSE
						IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxAirQuotaLastKillEffect)
							REMOVE_PARTICLE_FX(ptfxAirQuotaLastKillEffect)
							PRINTLN("[AIRQUOTA_PTFX] - Clearing up the effect because we are in a different vehicle to vehAirQuotaLocalVehicle!")
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxAirQuotaLastKillEffect)
					REMOVE_PARTICLE_FX(ptfxAirQuotaLastKillEffect)
					PRINTLN("[AIRQUOTA_PTFX] - Clearing up the effect in PROCESS_EVERY_FRAME_PLAYER_CHECKS because we're not a kill away any more")
				ENDIF
			ENDIF
		ELSE
			IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxAirQuotaLastKillEffect)
				REMOVE_PARTICLE_FX(ptfxAirQuotaLastKillEffect)
				PRINTLN("[AIRQUOTA_PTFX] - Clearing up the effect because the match is over!")
			ENDIF
			
			IF DOES_PARTICLE_FX_LOOPED_EXIST(sVehicleSwap.ptfxID)
				REMOVE_PARTICLE_FX(sVehicleSwap.ptfxID)
				PRINTLN("[AIRQUOTA_PTFX] - Clearing up the smokey effect because the match is over!")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// [FIX_2020_CONTROLLER] - A lot of these adversary mode process functions could be moved to a new "FM_Mission_Controller_AdversaryMode.sch header file.
PROC PROCESS_POINTLESS_MODE()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciPOINTLESS_MODE_ENABLED)
		IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] > -1 AND MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
			IF IS_BIT_SET(iLocalBoolCheck6, (LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iPartToUse].iteam))
				MC_playerBD[iPartToUse].iPlayerScore = 1
				PRINTLN("[JT POINTLESS] Giving starting score")
			ENDIF
		ENDIF
		IF HAVE_ALL_STARTING_PACKAGES_BEEN_GRABBED()
			IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)	
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_POINTLESS_SOUNDS)
					PLAY_POINTLESS_SOUND_ON_TEAM_SCORE_CHANGE()
					PLAY_POINTLESS_COUNTDOWN_SOUND()
				ENDIF
				PROCESS_POINTLESS_SCORES()
				PROCESS_POINTLESS_PORTABLE_PICKUP_BLIPS()
			ENDIF
			SET_POINTLESS_OWNED_PACKAGE_NUMBER()
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_PTL_WIN_TIME_LIMIT)
				SET_UP_TIMER_FOR_POINTLESS_ENDING()
			ENDIF
			KEEP_POINTLESS_PACKAGES_IN_BOUNDS()
		ENDIF
	ENDIF	
ENDPROC

PROC PROCESS_HACKING_CLEANUP()
	IF g_FMMC_STRUCT.bRememberToCleanUpHacking
		PRINTLN("[AW_MISSION] SHOULD CLEAN UP HACKING ")
		IF IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID))
			PRINTLN("[AW_MISSION] SS RUNNING ")
			IF GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID)) > 0.99
				DELETE_HACKING_KEYCARD_OBJECT()
				DELETE_LAPTOP_OBJECT()
				APPLY_DUFFEL_BAG_HEIST_GEAR(LocalPlayerPed, iLocalPart)
				RUN_LOCAL_PLAYER_PROOF_SETTING(TRUE)
				DELETE_HACK_BAG_OBJECT()
				SET_PED_USING_ACTION_MODE(LocalPlayerPed,TRUE)
				REMOVE_ANIM_DICT("anim@heists@ornate_bank@hack")
				SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed,TRUE,FALSE)
				PRINTLN("[AW_MISSION] LAPTOP HACK CLEAN UP ")
				g_FMMC_STRUCT.bRememberToCleanUpHacking = FALSE
			ENDIF
		ELSE
			DELETE_HACKING_KEYCARD_OBJECT()
			DELETE_LAPTOP_OBJECT()
			APPLY_DUFFEL_BAG_HEIST_GEAR(LocalPlayerPed, iLocalPart)
			RUN_LOCAL_PLAYER_PROOF_SETTING(TRUE)
			DELETE_HACK_BAG_OBJECT()
			SET_PED_USING_ACTION_MODE(LocalPlayerPed,TRUE)
			REMOVE_ANIM_DICT("anim@heists@ornate_bank@hack")
			SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed,TRUE,FALSE)
			PRINTLN("[AW_MISSION] SS NOT RUNNING LAPTOP HACK CLEAN UP ")
			g_FMMC_STRUCT.bRememberToCleanUpHacking = FALSE
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_PLAYER_MISSION_PARACHUTES()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciRESPAWN_WITH_PARACHUTE_EQUIPPED)
		BOOL bEquipped
		PRINTLN("{TMS] ciRESPAWN_WITH_PARACHUTE_EQUIPPED")
		
		IF NOT IS_PLAYER_WEARING_PARACHUTE()
			MPGlobalsAmbience.bDisableTakeOffChute = TRUE
			
			EQUIP_STORED_MP_PARACHUTE(PLAYER_ID(), g_FMMC_STRUCT.iTeamParachuteTint[g_i_Mission_team], g_FMMC_STRUCT.iTeamParachutePackTint[g_i_Mission_team])			
			
			bEquipped = TRUE
		ENDIF
		
		IF bEquipped
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(g_FMMC_STRUCT.iAdversaryModeType) AND g_sMPTunables.bENABLE_INDEPENDENCE_DAWNRAID
				PRINTLN( "[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - Dawn raid independence parachute variation!" )
				g_FMMC_STRUCT.iTeamParachuteTint[g_i_Mission_team] = 4
				g_FMMC_STRUCT.iTeamParachutePackTint[g_i_Mission_team] = 1
				SET_PLAYER_PARACHUTE_PACK_TINT_INDEX(LocalPlayer, g_FMMC_STRUCT.iTeamParachutePackTint[g_i_Mission_team])
				SET_PLAYER_PARACHUTE_CANOPY(LocalPlayer, g_FMMC_STRUCT.iTeamParachuteTint[g_i_Mission_team])
				SET_PLAYER_PARACHUTE_SMOKE_TRAIL_COLOR(LocalPlayer, 0, 0, 0)			
			ENDIF	
			
		ENDIF
		
		MAINTAIN_CHUTE_ON()
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_ENABLE_TEAM_COLOUR_PARACHUTES)
		IF NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_PARACHUTE_OVERRIDDEN_TO_TEAM_COLOUR)
			SET_PLAYER_PARACHUTE_CANOPY(LocalPlayer, g_i_Mission_team, PARACHUTE_CANOPY_TEAM_COLOUR)
			SET_BIT(iLocalBoolCheck26, LBOOL26_PARACHUTE_OVERRIDDEN_TO_TEAM_COLOUR)
			PRINTLN("[RCC MISSION] Parachute model and colour have been overridden to use team colour parachutes.")
		ELSE
			IF IS_PED_INJURED(LocalPlayerPed)
				CLEAR_BIT(iLocalBoolCheck26, LBOOL26_PARACHUTE_OVERRIDDEN_TO_TEAM_COLOUR)
			ENDIF
		ENDIF
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciDONT_REMOVE_PARACHUTE)
		SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisableTakeOffParachutePack, TRUE)
	ENDIF
ENDPROC

PROC PROCESS_WEAPON_AND_LOADOUTS()

	IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
	OR USING_HEIST_SPECTATE()
	
		INT iTeam = MC_playerBD[iPartToUse].iteam
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		
		// Nothing here checks that the player is alive ?
		IF DOES_TEAM_HAVE_AN_FMMC_GIVEN_INVENTORY( MC_playerBD[iPartToUse].iTeam, FALSE, TRUE )
		AND NOT IS_BIT_SET( iLocalBoolCheck4, LBOOL4_BEEN_GIVEN_MIDMISSION_INVENTORY ) 
			//Haven't been given my starting inventory yet:
			INT iTriggerTeam = g_FMMC_STRUCT.sFMMCEndConditions[ MC_playerBD[iPartToUse].iteam ].iMidMissionInventoryGiveOnTeam
			INT iTriggerRule = g_FMMC_STRUCT.sFMMCEndConditions[ MC_playerBD[iPartToUse].iteam ].iMidMissionInventoryGiveOnTeamRule
			
			BOOL bOnlyTriggerOnQuickRestart = IS_BIT_SET( g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciMID_MISSION_INVENTORY_ON_RESTART )
						
			IF iTriggerTeam != -1
			AND iTriggerRule != -1
				PRINTLN("[LM][LOADOUT][MID] - (1) iTriggerTeam: ", iTriggerTeam, " iTriggerRule: ", iTriggerRule, " bOnlyTriggerOnQuickRestart: ", bOnlyTriggerOnQuickRestart)
				IF IS_BIT_SET( iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTriggerTeam )				
					PRINTLN("[LM][LOADOUT][MID] - (1) New Frame")
					
					IF MC_serverBD_4.iCurrentHighestPriority[iTriggerTeam] >= iTriggerRule
					AND MC_serverBD_4.iCurrentHighestPriority[iTriggerTeam] < FMMC_MAX_RULES
						IF NOT bOnlyTriggerOnQuickRestart
						OR ( bOnlyTriggerOnQuickRestart AND IS_THIS_A_QUICK_RESTART_JOB() )
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciMID_MISSION_INVENTORY_REMOVE_WEAPONS)
								REMOVE_ALL_PLAYERS_WEAPONS()
							ENDIF
							GIVE_PLAYER_STARTING_MISSION_WEAPONS( WEAPONINHAND_LASTWEAPON_BOTH, TRUE, TRUE, DEFAULT, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciWEAPONS_FORCE_NO_ADDONS_MID_MISSION_INVENTORY))
							SET_BIT(iLocalBoolCheck4, LBOOL4_BEEN_GIVEN_MIDMISSION_INVENTORY)
							PRINTLN("[LM][LOADOUT][MID] - (1) Setting LBOOL4_BEEN_GIVEN_MIDMISSION_INVENTORY")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		// Nothing here checks that the player is alive ?
		IF DOES_TEAM_HAVE_AN_FMMC_GIVEN_INVENTORY( MC_playerBD[iPartToUse].iTeam, FALSE, FALSE, TRUE )
		AND NOT IS_BIT_SET( iLocalBoolCheck13, LBOOL13_BEEN_GIVEN_MIDMISSION_INVENTORY_2 )
			//Haven't been given my starting inventory yet:
			INT iTriggerTeam = g_FMMC_STRUCT.sFMMCEndConditions[ MC_playerBD[iPartToUse].iteam ].iMidMissionInventory2GiveOnTeam
			INT iTriggerRule = g_FMMC_STRUCT.sFMMCEndConditions[ MC_playerBD[iPartToUse].iteam ].iMidMissionInventory2GiveOnTeamRule
			
			BOOL bOnlyTriggerOnQuickRestart = IS_BIT_SET( g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciMID_MISSION_INVENTORY_2_ON_RESTART )
			
			IF iTriggerTeam != -1
			AND iTriggerRule != -1
				PRINTLN("[LM][LOADOUT][MID] - (2) iTriggerTeam: ", iTriggerTeam, " iTriggerRule: ", iTriggerRule, " bOnlyTriggerOnQuickRestart: ", bOnlyTriggerOnQuickRestart)
				IF IS_BIT_SET( iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTriggerTeam )			
					PRINTLN("[LM][LOADOUT][MID] - (2) New Frame")
				
					IF MC_serverBD_4.iCurrentHighestPriority[iTriggerTeam] >= iTriggerRule
					AND MC_serverBD_4.iCurrentHighestPriority[iTriggerTeam] < FMMC_MAX_RULES
						IF NOT bOnlyTriggerOnQuickRestart
						OR ( bOnlyTriggerOnQuickRestart AND IS_THIS_A_QUICK_RESTART_JOB() )
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciMID_MISSION_INVENTORY_2_REMOVE_WEAPONS)
								REMOVE_ALL_PLAYERS_WEAPONS()
							ENDIF
							GIVE_PLAYER_STARTING_MISSION_WEAPONS( WEAPONINHAND_LASTWEAPON_BOTH, TRUE, FALSE, FALSE, TRUE, IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciWEAPONS_FORCE_NO_ADDONS_MID_MISSION_INVENTORY_2))
							SET_BIT(iLocalBoolCheck13, LBOOL13_BEEN_GIVEN_MIDMISSION_INVENTORY_2)
							PRINTLN("[LM][LOADOUT][MID] - (2) Setting LBOOL13_BEEN_GIVEN_MIDMISSION_INVENTORY_2")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
			
		// Equipping the players weapons on a Rule-To-Rule basis: Right-hand menu
		IF iRule < FMMC_MAX_RULES		
			SET_WEAPON_LOADOUT_FOR_RULE(iTeam, iRule)
			WEAPON_EQUIP_ON_RULE_CHANGE()
			
			// Create Spawn Points when the Rule Changes.
			// Checks to see if there are any Valid custom Respawn Points on the Rule.
			IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
				IF SHOULD_USE_CUSTOM_SPAWN_POINTS()	
					PRINTLN("[spawning][RCC MISSION][LM][USE_CUSTOM_SPAWN_POINTS] calling from main ADD_ALL_CUSTOM_SPAWN_POINTS")
					ADD_ALL_CUSTOM_SPAWN_POINTS()
				ELSE
					IF IS_BIT_SET(iLocalBoolCheck21, LBOOL21_USING_CUSTOM_RESPAWN_POINTS)
						PRINTLN("[spawning][RCC MISSION][LM][USE_CUSTOM_SPAWN_POINTS] We are using Custom Respawn Points, but they are not valid on this Rule: ", iRule, " Setting should clear")
						SET_BIT(iLocalBoolCheck21, LBOOL21_SHOULD_CLEAR_CUSTOM_RESPAWN_POINTS)
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(iLocalBoolCheck21, LBOOL21_USING_CUSTOM_RESPAWN_POINTS)
			AND IS_BIT_SET(iLocalBoolCheck21, LBOOL21_SHOULD_CLEAR_CUSTOM_RESPAWN_POINTS)
				IF NOT IS_PED_INJURED(localPlayerPed)
				AND NOT IS_PLAYER_RESPAWNING(LocalPlayer)
					PRINTLN("[spawning][RCC MISSION][LM][USE_CUSTOM_SPAWN_POINTS] Removing custom respawn points")
					CLEAR_CUSTOM_SPAWN_POINTS()
					USE_CUSTOM_SPAWN_POINTS(FALSE)
					CLEAR_BIT(iLocalBoolCheck21, LBOOL21_USING_CUSTOM_RESPAWN_POINTS)
					CLEAR_BIT(iLocalBoolCheck21, LBOOL21_SHOULD_CLEAR_CUSTOM_RESPAWN_POINTS)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// [FIX_2020_CONTROLLER] - A lot of these adversary mode process functions could be moved to a new "FM_Mission_Controller_AdversaryMode.sch header file.
PROC PROCESS_RUNNING_BACK_MODE()
	
	
	PROCESS_RUNNING_BACK_SHARDS()

	PROCESS_RUNNER_BEST_TIME()
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUNNING_BACK_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
		IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_SPAWNED_IN_A_PLACED_VEHICLE)
			g_bMissionHideRespawnBar = TRUE
		ELSE
			g_bMissionHideRespawnBar = FALSE
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_PLAYER_PASSENGER_BOMBING()
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		IF MC_ServerBD.iNumberOfPlayingPlayers[0]+MC_ServerBD.iNumberOfPlayingPlayers[1]+MC_ServerBD.iNumberOfPlayingPlayers[2]+MC_ServerBD.iNumberOfPlayingPlayers[3] > 3
			IF NOT MPGlobalsAmbience.bEnablePassengerBombing
				MPGlobalsAmbience.bEnablePassengerBombing = TRUE
				PRINTLN("[LM][RCC MISSION] - bEnablePassengerBombing Setting this to TRUE.")
			ENDIF
		ELSE
			IF MPGlobalsAmbience.bEnablePassengerBombing
				MPGlobalsAmbience.bEnablePassengerBombing = FALSE
				PRINTLN("[LM][RCC MISSION] - bEnablePassengerBombing Setting this to FALSE.")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_CLEAR_CUSTOM_PERSONAL_VEHICLE_NODE_FOR_FACILITY()
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
	AND NOT IS_BIT_SET(iLocalBoolCheck28, LBOOL28_CLEARED_PERSONAL_VEHICLE_INITIAL_HEIST2_NODES)
	AND ((IS_BIT_SET(g_FMMC_STRUCT.iRuleOptionsBitSet, ciALLOW_VEHICLE_CHOICE) AND DOES_ENTITY_EXIST(PERSONAL_VEHICLE_ID()))
	OR NOT IS_BIT_SET(g_FMMC_STRUCT.iRuleOptionsBitSet, ciALLOW_VEHICLE_CHOICE))
		// Timer for some breathing room.
		IF NOT HAS_NET_TIMER_STARTED(tdTimer_ClearCustomVehicleNodesHeist2)
			START_NET_TIMER(tdTimer_ClearCustomVehicleNodesHeist2)
		ENDIF
		
		IF HAS_NET_TIMER_STARTED(tdTimer_ClearCustomVehicleNodesHeist2)
		AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdTimer_ClearCustomVehicleNodesHeist2, 5000)
			PRINTLN("[LM][PROCESS_EVERY_FRAME_PLAYER_CHECKS] - Clearing the start Personal Vehicle nodes as our vehicle has been created, or we are through the initial spawning.")
			SET_BIT(iLocalBoolCheck28, LBOOL28_CLEARED_PERSONAL_VEHICLE_INITIAL_HEIST2_NODES)
			RESET_NET_TIMER(tdTimer_ClearCustomVehicleNodesHeist2)
			CLEAR_CUSTOM_VEHICLE_NODES()
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_DESTROYED_TURRET_BITSET()
	INT iTeam = MC_playerBD[iLocalPart].iteam
	
	IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES

		IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_JUST_REACHED_QUICK_RESTART_POINT_1)
		AND HAVE_WE_PASSED_THE_CHECKPOINT(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam])
			g_TransitionSessionNonResetVars.iDestroyedTurretBitSet = MC_serverBD_4.iDestroyedTurretBitSet
			PRINTLN("[RCC MISSION] UPDATE_DESTROYED_TURRET_BITSET - updated g_TransitionSessionNonResetVars.iDestroyedTurretBitSet to: ", g_TransitionSessionNonResetVars.iDestroyedTurretBitSet, " for checkpoint 1")
			g_TransitionSessionNonResetVars.iObjectsDestroyedForSpecialHUD[iTeam] = MC_ServerBD_4.iObjectsDestroyedForSpecialHUD[iTeam] 
			PRINTLN("[RCC MISSION] UPDATE_DESTROYED_TURRET_BITSET - updated g_TransitionSessionNonResetVars.iObjectsDestroyedForSpecialHUD[",iTeam,"] to: ", g_TransitionSessionNonResetVars.iObjectsDestroyedForSpecialHUD[iTeam], " for checkpoint 1")
			SET_BIT(iLocalBoolCheck27, LBOOL27_JUST_REACHED_QUICK_RESTART_POINT_1)
		ENDIF
		IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_JUST_REACHED_QUICK_RESTART_POINT_2)
		AND HAVE_WE_PASSED_THE_SECOND_CHECKPOINT(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam])
			g_TransitionSessionNonResetVars.iDestroyedTurretBitSet = MC_serverBD_4.iDestroyedTurretBitSet
			PRINTLN("[RCC MISSION] UPDATE_DESTROYED_TURRET_BITSET - updated g_TransitionSessionNonResetVars.iDestroyedTurretBitSet to: ", g_TransitionSessionNonResetVars.iDestroyedTurretBitSet, " for checkpoint 2")
			g_TransitionSessionNonResetVars.iObjectsDestroyedForSpecialHUD[iTeam] = MC_ServerBD_4.iObjectsDestroyedForSpecialHUD[iTeam]
			PRINTLN("[RCC MISSION] UPDATE_DESTROYED_TURRET_BITSET - updated g_TransitionSessionNonResetVars.iObjectsDestroyedForSpecialHUD[",iTeam,"] to: ", g_TransitionSessionNonResetVars.iObjectsDestroyedForSpecialHUD[iTeam], " for checkpoint 2")
			SET_BIT(iLocalBoolCheck27, LBOOL27_JUST_REACHED_QUICK_RESTART_POINT_2)
		ENDIF
		IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_JUST_REACHED_QUICK_RESTART_POINT_3)
		AND HAVE_WE_PASSED_THE_THIRD_CHECKPOINT(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam]) 
			g_TransitionSessionNonResetVars.iDestroyedTurretBitSet = MC_serverBD_4.iDestroyedTurretBitSet
			PRINTLN("[RCC MISSION] UPDATE_DESTROYED_TURRET_BITSET - updated g_TransitionSessionNonResetVars.iDestroyedTurretBitSet to: ", g_TransitionSessionNonResetVars.iDestroyedTurretBitSet, " for checkpoint 3")
			g_TransitionSessionNonResetVars.iObjectsDestroyedForSpecialHUD[iTeam] = MC_ServerBD_4.iObjectsDestroyedForSpecialHUD[iTeam]
			PRINTLN("[RCC MISSION] UPDATE_DESTROYED_TURRET_BITSET - updated g_TransitionSessionNonResetVars.iObjectsDestroyedForSpecialHUD[",iTeam,"] to: ", g_TransitionSessionNonResetVars.iObjectsDestroyedForSpecialHUD[iTeam], " for checkpoint 3")
			SET_BIT(iLocalBoolCheck27, LBOOL27_JUST_REACHED_QUICK_RESTART_POINT_3)
		ENDIF
		IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_JUST_REACHED_QUICK_RESTART_POINT_4)
		AND HAVE_WE_PASSED_THE_FOURTH_CHECKPOINT(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam]) 
			g_TransitionSessionNonResetVars.iDestroyedTurretBitSet = MC_serverBD_4.iDestroyedTurretBitSet
			PRINTLN("[RCC MISSION] UPDATE_DESTROYED_TURRET_BITSET - updated g_TransitionSessionNonResetVars.iDestroyedTurretBitSet to: ", g_TransitionSessionNonResetVars.iDestroyedTurretBitSet, " for checkpoint 4")
			g_TransitionSessionNonResetVars.iObjectsDestroyedForSpecialHUD[iTeam] = MC_ServerBD_4.iObjectsDestroyedForSpecialHUD[iTeam]
			PRINTLN("[RCC MISSION] UPDATE_DESTROYED_TURRET_BITSET - updated g_TransitionSessionNonResetVars.iObjectsDestroyedForSpecialHUD[",iTeam,"] to: ", g_TransitionSessionNonResetVars.iObjectsDestroyedForSpecialHUD[iTeam], " for checkpoint 4")
			SET_BIT(iLocalBoolCheck27, LBOOL27_JUST_REACHED_QUICK_RESTART_POINT_4)
		ENDIF
		
	ENDIF
		
ENDPROC

PROC PROCESS_PLAYER_FORCED_SEAT_PREF_SPECIAL
	
	IF iVehSeatPrefOverrideVeh > -1
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVehSeatPrefOverrideVeh])
			VEHICLE_INDEX tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVehSeatPrefOverrideVeh])
		
			IF DOES_ENTITY_EXIST(tempVeh)
				INT iDriver = 0
				IF NOT IS_VEHICLE_SEAT_FREE(tempVeh, VS_DRIVER, TRUE)
				OR GET_PED_IN_VEHICLE_SEAT(tempVeh, VS_DRIVER, TRUE) != null				
					iDriver = 1
				ENDIF
				
				IF (NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_OVERRIDING_SEAT_PREF_SPECIAL_VEH) AND NOT IS_PED_IN_VEHICLE(localPlayerPed, tempveh))
				OR iPlayersInsideSeatPrefOverrideVeh != (GET_VEHICLE_NUMBER_OF_PASSENGERS(tempVeh)+iDriver)
					INT iFlags
					INT iSlotPref
					
					iPlayersInsideSeatPrefOverrideVeh = (GET_VEHICLE_NUMBER_OF_PASSENGERS(tempVeh)+iDriver)
					
					IF iPlayersInsideSeatPrefOverrideVeh = 0
						iPlayersInsideSeatPref = ENUM_TO_INT(VS_DRIVER)
						iSlotPref = 0
						iFlags = ENUM_TO_INT(VC_FORCE_USE_FRONT_SEATS)
						SET_PED_VEHICLE_FORCED_SEAT_USAGE(LocalPlayerPed, tempVeh, iSlotPref, iFlags)
					ELIF iPlayersInsideSeatPrefOverrideVeh = 1
						iPlayersInsideSeatPref = ENUM_TO_INT(VS_BACK_LEFT)
						iSlotPref = 0
						iFlags = ENUM_TO_INT(VC_FORCE_USE_REAR_SEATS)
						SET_PED_VEHICLE_FORCED_SEAT_USAGE(LocalPlayerPed, tempVeh, iSlotPref, iFlags)
					ELIF iPlayersInsideSeatPrefOverrideVeh = 2
						iPlayersInsideSeatPref = ENUM_TO_INT(VS_BACK_RIGHT)
						iSlotPref = 1
						iFlags = ENUM_TO_INT(VC_FORCE_USE_REAR_SEATS)
						SET_PED_VEHICLE_FORCED_SEAT_USAGE(LocalPlayerPed, tempVeh, iSlotPref, iFlags)						
					ELIF iPlayersInsideSeatPrefOverrideVeh = 3
						iPlayersInsideSeatPref = ENUM_TO_INT(VS_FRONT_RIGHT)
						IF NOT IS_PED_IN_VEHICLE(localPlayerPed, tempveh)
							CLEAR_ALL_PED_VEHICLE_FORCED_SEAT_USAGE(LocalPlayerPed)
						ENDIF
					ENDIF
					
					IF IS_PED_IN_VEHICLE(localPlayerPed, tempVeh, FALSE)
						iPlayersInsideSeatPref = -3
					ENDIf
										
					SET_BIT(iLocalBoolCheck26, LBOOL26_OVERRIDING_SEAT_PREF_SPECIAL_VEH)					
					PRINTLN("[LM][PROCESS_VEH_BODY][LBOOL26_OVERRIDING_SEAT_PREF_SPECIAL_VEH] - Calling Vehicle Seat Prefs on iVeh: ", iVehSeatPrefOverrideVeh, " --- iSlotPref: ", iSlotPref, " iFlags:", iFlags, " iPlayersInsideSeatPrefOverrideVeh: ", iPlayersInsideSeatPrefOverrideVeh)
				ENDIF
				
				IF IS_PED_IN_VEHICLE(localPlayerPed, tempveh)
					CLEAR_BIT(iLocalBoolCheck26, LBOOL26_OVERRIDING_SEAT_PREF_SPECIAL_VEH)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_KILL_PLAYER_BY_TOUCH(INT iRule, INT iTeam, INT iPart, PED_INDEX otherPed)
	IF NOT  IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[iRule], ciBS_RULE12_KILL_PLAYER_BY_TOUCH)
		EXIT
	ENDIF
	PRINTLN("[PROCESS_KILL_PLAYER_BY_TOUCH] Kill the player ",iLocalPart," for touching: ",iPart)
	SET_ENTITY_HEALTH(LocalPlayerPed,0,otherPed)
ENDPROC

FUNC BOOL CAN_DROP_OBJECT()
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_DROP_OBJECT)
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC BOOL CAN_STEAL_OBJECT()
	
	IF NOT CAN_DROP_OBJECT()
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_STEAL_OBJECT)
		RETURN FALSE
	ENDIF
	
	IF iSmashPickup != -1
		PRINTLN("[SMASH_FORCE][CAN_STEAL_OBJECT] Already stealing an item ",iSmashPickup)
		RETURN FALSE
	ENDIF
	
	INT iobj
	OBJECT_INDEX tempObj
	REPEAT FMMC_MAX_NUM_OBJECTS iobj
		IF MC_serverBD.iObjCarrier[iobj] = iLocalPart
			tempObj = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
			IF IS_ENTITY_ATTACHED_TO_ENTITY(tempObj, LocalPlayerPed)
				PRINTLN("[SMASH_FORCE][CAN_STEAL_OBJECT] Can't steal a package because the player own iObj ",iobj)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

PROC BROADCAST_SMASH_BY_TOUCH(PLAYER_INDEX otherPlayer,FLOAT fFactor,BOOL bDropItem, BOOL bStealItem)
	bUsingSmashPower = TRUE
	SET_CONTROL_SHAKE(PLAYER_CONTROL, 130, 256)
	BROADCAST_FMMC_SMASHED_BY_TOUCH_EVENT(otherPlayer,fFactor,bDropItem,bStealItem)
	PRINTLN("[SMASH_FORCE][BROADCAST_SMASH_BY_TOUCH] Smash")
ENDPROC

PROC USE_SMASH_ABILITY()
	REINIT_NET_TIMER(timerSmashForce)
	PRINTLN("[SMASH_FORCE][USE_SMASH_ABILITY]")
ENDPROC

PROC PROCESS_SMASH_CAR_FORWARD(VEHICLE_INDEX vIndex, PLAYER_INDEX otherPlayer, FLOAT fFactor)
	VECTOR vLocalPos = GET_ENTITY_COORDS(vIndex)
	PED_INDEX playerPed = GET_PLAYER_PED(otherPlayer)
	VEHICLE_INDEX playerVeh = GET_VEHICLE_PED_IS_IN(playerPed)
	IF fFactor <= g_FMMC_STRUCT.fMinSpeedForceForward
		PRINTLN("[SMASH_FORCE][PROCESS_SMASH_CAR_FORWARD] Can't smash speed factor to low: ",fFactor)
		EXIT
	ENDIF
	
	INT iHitSomething
	VECTOR vHitPos
	VECTOR vHitNormal
	ENTITY_INDEX hitEntity
	VEHICLE_INDEX vehProbeTest
	
	SHAPETEST_INDEX shapeTest = START_EXPENSIVE_SYNCHRONOUS_SHAPE_TEST_LOS_PROBE(vLocalPos, GET_ENTITY_COORDS(playerVeh),SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_VEHICLE, vIndex, SCRIPT_SHAPETEST_OPTION_DEFAULT) 
	IF GET_SHAPE_TEST_RESULT( shapeTest, iHitSomething, vHitPos, vHitNormal, hitEntity) != SHAPETEST_STATUS_RESULTS_READY
		PRINTLN("[SMASH_FORCE][PROCESS_SMASH_CAR_FORWARD] Can't smash NOT shape result ")			
		EXIT
	ENDIF
	
	IF NOT IS_ENTITY_A_VEHICLE(hitEntity)
		PRINTLN("[SMASH_FORCE][PROCESS_SMASH_CAR_FORWARD]  Can't smash ENTITY NOT A VEHICLE")
		EXIT
	ENDIF
	
	vehProbeTest = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(hitEntity)
	IF vehProbeTest != playerVeh
		PRINTLN("[SMASH_FORCE][PROCESS_SMASH_CAR_FORWARD] Can't smash NOT SAME ENTITY ")
		EXIT
	ENDIF
	
	IF GET_PLAYER_RESPAWN_BACK_AT_START_STATE(iLocalPart) != PRBSS_PlayingNormally
		PRINTLN("[SMASH_FORCE][PROCESS_SMASH_CAR_FORWARD] Restarting, Exit function.")
		EXIT
	ENDIF
	
	VECTOR vForward = NORMALISE_VECTOR(GET_ENTITY_FORWARD_VECTOR(vIndex))
	VECTOR vDistanceDir = NORMALISE_VECTOR((vHitPos - vLocalPos))
	FLOAT fAngle = GET_ANGLE_BETWEEN_2D_VECTORS(vForward.x,vForward.y,vDistanceDir.x,vDistanceDir.y)
	IF ABSF(fAngle) >= cfMinAngleToForwardSmash
		PRINTLN("[SMASH_FORCE][PROCESS_SMASH_CAR_FORWARD] Can't smash because angle ",fAngle)
		EXIT
	ENDIF
	
	BROADCAST_SMASH_BY_TOUCH(otherPlayer,fFactor, CAN_DROP_OBJECT(),CAN_STEAL_OBJECT())
	USE_SMASH_ABILITY()
	PRINTLN("[SMASH_FORCE][PROCESS_SMASH_CAR_FORWARD] Smashing in forward by fFactor ",fFactor," and angle ",fAngle)
ENDPROC
PROC PROCESS_SMASH_BY_TOUCH(PLAYER_INDEX otherPlayer)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_SMASH_CAR)
		EXIT
	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		PRINTLN("[SMASH_FORCE][PROCESS_SMASH_BY_TOUCH] Local Player is not in any vehicle ")
		EXIT
	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(otherPlayer))
		PRINTLN("[SMASH_FORCE][PROCESS_SMASH_BY_TOUCH] Other Player is not in any vehicle ")
		EXIT
	ENDIF	
	IF GET_PLAYER_RESPAWN_BACK_AT_START_STATE(iLocalPart) != PRBSS_PlayingNormally
		PRINTLN("[SMASH_FORCE][PROCESS_SMASH_BY_TOUCH] Restarting, Exit function.")
		EXIT
	ENDIF
	
	VEHICLE_INDEX vIndex = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
	FLOAT fFactor = GET_VEHICLE_SPEED_FACTOR(vIndex)
	IF bUsingSmashPower
		PRINTLN("[SMASH_FORCE][PROCESS_SMASH_BY_TOUCH] Already used the smash.")
		EXIT
	ENDIF
	IF HAS_NET_TIMER_STARTED(timerSmashForce)
		BROADCAST_SMASH_BY_TOUCH(otherPlayer,fFactor,CAN_DROP_OBJECT(),CAN_STEAL_OBJECT())
	ELIF g_FMMC_STRUCT.fMinSpeedForceForward > 0.0
		PROCESS_SMASH_CAR_FORWARD(vIndex,otherPlayer,fFactor)
	ENDIF
ENDPROC

PROC PROCESS_STEAL_OBJECT_ATTACH()
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_STEAL_OBJECT)
		EXIT
	ENDIF
	
	IF iSmashPickup = -1
		EXIT
	ENDIF
	
	IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niObject[iSmashPickup])
		NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niObject[iSmashPickup])
		PRINTLN("[SMASH_FORCE][PROCESS_STEAL_OBJECT_ATTACH] Requesting control of object ",iSmashPickup)
		EXIT
	ENDIF
	
	OBJECT_INDEX tempObj = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iSmashPickup])

	ATTACH_PORTABLE_PICKUP_TO_PED(tempObj,LocalPlayerPed)
	PRINTLN("[SMASH_FORCE][PROCESS_STEAL_OBJECT_ATTACH] Attaching object ",iSmashPickup)
	iSmashPickup = -1
ENDPROC

PROC PROCESS_SMASH_CAR()
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_SMASH_CAR)
		EXIT
	ENDIF

	PROCESS_STEAL_OBJECT_ATTACH()
		
	#IF IS_DEBUG_BUILD
	IF IS_CONTROL_ENABLED(PLAYER_CONTROL,INPUT_VEH_DUCK)
		PRINTLN("[SMASH_FORCE][PROCESS_SMASH_CAR] DISABLE_CONTROL_ACTION INPUT_VEH_DUCK")
	ENDIF
	#ENDIF
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_DUCK)
	
	//if I was hitted
	IF HAS_NET_TIMER_STARTED(timerSmashForceHit)
	AND HAS_NET_TIMER_EXPIRED(timerSmashForceHit,ciTimeToBeHitAgain)
		RESET_NET_TIMER(timerSmashForceHit)
		STOP_GAMEPLAY_CAM_SHAKING()
		PRINTLN("[SMASH_FORCE][PROCESS_SMASH_CAR] Reset my hit")
	ENDIF

	IF HAS_NET_TIMER_STARTED(timerSmashForce)
		IF NOT HAS_NET_TIMER_EXPIRED(timerSmashForce,g_FMMC_STRUCT.iCooldDownSmash)
			EXIT
		ENDIF
		RESET_NET_TIMER(timerSmashForce)
		PRINTLN("[SMASH_FORCE][PROCESS_SMASH_CAR] Reset Smashing")
	ENDIF
	
	bUsingSmashPower = FALSE
	INT iRX = 0
	IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_MELEE_HOLD)
		#IF IS_DEBUG_BUILD
		IF IS_CONTROL_ENABLED(PLAYER_CONTROL,INPUT_VEH_HANDBRAKE)
			PRINTLN("[SMASH_FORCE][PROCESS_SMASH_CAR] DISABLE_CONTROL_ACTION INPUT_VEH_HANDBRAKE")
		ENDIF
		#ENDIF
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_HANDBRAKE)
		
		IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_MELEE_LEFT)
			iRX = -1
		ENDIF
		IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_MELEE_RIGHT)
			iRX = 1
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		IF NOT IS_CONTROL_ENABLED(PLAYER_CONTROL,INPUT_VEH_HANDBRAKE)
			PRINTLN("[SMASH_FORCE][PROCESS_SMASH_CAR] ENABLE_CONTROL_ACTION INPUT_VEH_HANDBRAKE Not pressing the melee hold")
		ENDIF
		#ENDIF
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_HANDBRAKE)
	ENDIF
	IF iRX = 0
		EXIT
	ENDIF
	IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		EXIT
	ENDIF
	
	FLOAT fFactor = GET_VEHICLE_SPEED_FACTOR(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
	FLOAT iForceH = iRX*g_FMMC_STRUCT.fMaxLateralForceSmash*fFactor
	FLOAT iForceV = g_FMMC_STRUCT.fMaxVerticalForceSmash*fFactor
	
	IF ABSF(iForceH) < g_FMMC_STRUCT.fMinLateralForceSmash
		iForceH = g_FMMC_STRUCT.fMinLateralForceSmash*iRX
	ENDIF
	IF iForceV < g_FMMC_STRUCT.fMinVerticalForceSmash
		iForceV = g_FMMC_STRUCT.fMinVerticalForceSmash
	ENDIF
	
	VECTOR vNormalDir = <<iForceH,0,iForceV>>
	
	VEHICLE_INDEX vIndex = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
	SET_ENTITY_INVINCIBLE(vIndex,TRUE)
	APPLY_FORCE_TO_ENTITY(vIndex,APPLY_TYPE_IMPULSE,vNormalDir,<<0,0,0>>,0,TRUE,TRUE,TRUE)
	SET_ENTITY_INVINCIBLE(vIndex,FALSE)
	USE_SMASH_ABILITY()
	PRINTLN("[SMASH_FORCE][PROCESS_SMASH_CAR] Smashing in direction ",vNormalDir)
ENDPROC

PROC PROCESS_PLAYER_TOUCH(INT iRule, INT iTeam)
	IF NOT bLocalPlayerOK
		EXIT
	ENDIF
	IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
		EXIT
	ENDIF
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[iRule], ciBS_RULE12_KILL_PLAYER_BY_TOUCH)  
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_SMASH_CAR)
		EXIT
	ENDIF
	
	INT iPart
	PARTICIPANT_INDEX tempPart
	PLAYER_INDEX tempPlayer
	PED_INDEX temPed
	
	REPEAT GET_MAXIMUM_PLAYERS_ON_MISSION() iPart
		IF iPart = iLocalPart
			RELOOP
		ENDIF
		IF DOES_TEAM_LIKE_TEAM(iTeam ,MC_playerBD[iPart].iteam)
			RELOOP
		ENDIF
		tempPart = INT_TO_PARTICIPANTINDEX(iPart)
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			RELOOP
		ENDIF
		tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
		IF NOT IS_NET_PLAYER_OK(tempPlayer)
			RELOOP
		ENDIF
		temPed = GET_PLAYER_PED(tempPlayer)
		IF NOT IS_ENTITY_TOUCHING_ENTITY(LocalPlayerPed,temPed)
			RELOOP
		ENDIF
	
		PROCESS_KILL_PLAYER_BY_TOUCH(iRule,iTeam,iPart,temPed)
		PROCESS_SMASH_BY_TOUCH(tempPlayer)
	ENDREPEAT
ENDPROC

PROC PROCESS_SERVER_DESTROY_PLACED_VEHICLE_FOR_LEAVER()

	IF NOT IS_BIT_SET(MC_ServerBD.iServerBitSet6, SBBOOL6_USING_CYCLE_VEHICLE_PLACEMENT)
	AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
		EXIT
	ENDIF
	
	IF GET_MC_CLIENT_GAME_STATE(iLocalPart) = GAME_STATE_RUNNING
	AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		BOOL bEmptyVehicleCleanup
		VEHICLE_INDEX veh
		
		INT iTeam
		INT iPlayerSlot
		FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
			FOR iPlayerSlot = 0 TO FMMC_MAX_TEAMSPAWNPOINTS-1
				INT iVehicleIndexToSpawnInto = MC_serverBD_3.iPlacedVehiclePlayerPart[iTeam][iPlayerSlot]
		
				IF iVehicleIndexToSpawnInto > -1
					NETWORK_INDEX niVeh = MC_serverBD_1.sFMMC_SBD.niVehicle[iVehicleIndexToSpawnInto]
					IF NETWORK_DOES_NETWORK_ID_EXIST( niVeh )
						veh = NET_TO_VEH( niVeh )
						IF IS_VEHICLE_DRIVEABLE( veh )
						AND NOT IS_ANY_PLAYER_IN_VEHICLE(veh, FALSE)
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(niVeh)
								PRINTLN("[LM][PROCESS_SERVER_DESTROY_PLACED_VEHICLE_FOR_LEAVER] - bEmptyVehicleCleanup = TRUE")
								bEmptyVehicleCleanup = TRUE
								BREAKLOOP
							ELSE
								PRINTLN("[LM][PROCESS_SERVER_DESTROY_PLACED_VEHICLE_FOR_LEAVER] - We do not have control of the entity...")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ENDFOR
		
		IF bEmptyVehicleCleanup
		AND GET_PLAYER_RESPAWN_BACK_AT_START_STATE(iLocalPart) = PRBSS_PlayingNormally
			IF NOT HAS_NET_TIMER_STARTED(tdExplodeVehicleForRunningBack)		
				PRINTLN("[LM][PROCESS_SERVER_DESTROY_PLACED_VEHICLE_FOR_LEAVER] - Starting Timer")
				START_NET_TIMER(tdExplodeVehicleForRunningBack)
			ELIF HAS_NET_TIMER_STARTED(tdExplodeVehicleForRunningBack)
			AND  HAS_NET_TIMER_EXPIRED_READ_ONLY(tdExplodeVehicleForRunningBack, ci_EXPLODE_VEHICLE_FOR_RUNNING_BACK)
				PRINTLN("[LM][PROCESS_SERVER_DESTROY_PLACED_VEHICLE_FOR_LEAVER] - Calling NETWORK_EXPLODE_VEHICLE")				
				IF DOES_ENTITY_EXIST(veh)
					NETWORK_EXPLODE_VEHICLE(veh)
				ENDIF
				RESET_NET_TIMER(tdExplodeVehicleForRunningBack)
			ENDIF
		ELSE
			IF HAS_NET_TIMER_STARTED(tdExplodeVehicleForRunningBack)		
				PRINTLN("[LM][PROCESS_SERVER_DESTROY_PLACED_VEHICLE_FOR_LEAVER] - Resetting Timer")
				RESET_NET_TIMER(tdExplodeVehicleForRunningBack)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_DISABLE_HEADLIGHT_CONTROL()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_DISABLE_HEADLIGHT_CONTROL)
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, TRUE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_MANUAL_RESPAWN_RADIO()
	IF IS_BIT_SET(MC_serverBD.iServerBitSet,SBBOOL_MISSION_OVER)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck29, LBOOL29_SHOULD_TURN_RADIO_BACK_ON_AFTER_ROUND_RESTART)		
		//Cache the current radio station (MANUAL RESPAWN)
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMBUSHKA(g_FMMC_STRUCT.iAdversaryModeType)
		OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_STOCKPILE(g_FMMC_STRUCT.iAdversaryModeType)
		OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRQUOTA(g_FMMC_STRUCT.iAdversaryModeType)
		OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
		OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_STUNTING_PACK(g_FMMC_STRUCT.iAdversaryModeType)
		OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUNNING_BACK_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
		OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMB_FOOTBALL(g_FMMC_STRUCT.iAdversaryModeType)
		OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_KEEP_RADIO_STATION_ON_RESPAWN)
			IF manualRespawnState = eManualRespawnState_OKAY_TO_SPAWN
			AND GET_PLAYER_RESPAWN_BACK_AT_START_STATE(iLocalPart) = PRBSS_PlayingNormally
				IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					IF DOES_PLAYER_VEH_HAVE_RADIO()
					AND IS_PLAYER_VEH_RADIO_ENABLE()
						IF bPlayerToUseOK
							IF NOT IS_PED_INJURED(LocalPlayerPed)
								IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
									IF iManRespawnLastRadioStation != GET_PLAYER_RADIO_STATION_INDEX()
										IF IS_VEHICLE_RADIO_ON(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
											iManRespawnLastRadioStation = GET_PLAYER_RADIO_STATION_INDEX()
											g_SpawnData.bRadioStationOverScore = TRUE
											PRINTLN("[RADIO] storing radio station as : ", GET_PLAYER_RADIO_STATION_INDEX())
										ELSE
											iManRespawnLastRadioStation = 255
											g_SpawnData.bRadioStationOverScore = FALSE
											PRINTLN("[RADIO] storing radio station as 255")
										ENDIF
										SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
										PRINTLN( "[RADIO][JJT][ManRespawn] Setting radio station before respawn to: ", iManRespawnLastRadioStation )
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_ACTUAL_DEATH_RESPAWN_RADIO()
	IF IS_BIT_SET(MC_serverBD.iServerBitSet,SBBOOL_MISSION_OVER)
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_KEEP_RADIO_STATION_ON_RESPAWN)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck29, LBOOL29_SHOULD_TURN_RADIO_BACK_ON_AFTER_ROUND_RESTART)
		//Cache the current radio station (ACTUAL RESPAWN)
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA(g_FMMC_STRUCT.iAdversaryModeType)
		OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARS(g_FMMC_STRUCT.iAdversaryModeType)
		OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
		OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_STOCKPILE(g_FMMC_STRUCT.iAdversaryModeType)
		OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMBUSHKA(g_FMMC_STRUCT.iAdversaryModeType)
		OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRQUOTA(g_FMMC_STRUCT.iAdversaryModeType)
		OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
		OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_STUNTING_PACK(g_FMMC_STRUCT.iAdversaryModeType)
		OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUNNING_BACK_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
		OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMB_FOOTBALL(g_FMMC_STRUCT.iAdversaryModeType)
		OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_KEEP_RADIO_STATION_ON_RESPAWN)
			IF NOT g_bMissionEnding
			AND GET_PLAYER_RESPAWN_BACK_AT_START_STATE(iLocalPart) = PRBSS_PlayingNormally
				IF IS_SCREEN_FADED_IN()
					IF bPlayerToUseOK
						IF NOT IS_PED_INJURED(LocalPlayerPed)
						AND NOT IS_PED_DEAD_OR_DYING(localPlayerPed)
							IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
								IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
								
									INT iRadioStationIndex = 255
									
									IF DOES_PLAYER_VEH_HAVE_RADIO()
									AND IS_PLAYER_VEH_RADIO_ENABLE()
										iRadioStationIndex = GET_PLAYER_RADIO_STATION_INDEX()
										SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
									ENDIF
									
									IF g_SpawnData.iRadioStation != iRadioStationIndex
										g_SpawnData.bRadioStationOverScore = TRUE						
										g_SpawnData.iRadioStation = iRadioStationIndex
										PRINTLN("[RADIO] Mission - Storing Radio Station ... g_SpawnData.iRadioStation = ", iRadioStationIndex)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			g_SpawnData.bRadioStationOverScore = FALSE
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_ROUND_RESTART_LOGIC_RADIO()	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet,SBBOOL_MISSION_OVER)
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bbmbfbRadioDebug
		DRAW_DEBUG_TEXT_2D("PROCESS_ROUND_RESTART_LOGIC_RADIO", <<0.5, 0.1, 0.5>>)
		PRINTLN(" PROCESS_ROUND_RESTART_LOGIC_RADIO ================================================================================")

		TEXT_LABEL_63 tlDebugText
		tlDebugText = "iRoundRestartLogicRadio: "
		tlDebugText += iRoundRestartLogicRadio
		PRINTLN(" iRoundRestartLogicRadio: ", iRoundRestartLogicRadio)
		DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.5, 0.2, 0.5>>)
		
		IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_SHOULD_TURN_RADIO_BACK_ON_AFTER_ROUND_RESTART)
			DRAW_DEBUG_TEXT_2D("LBOOL29_SHOULD_TURN_RADIO_BACK_ON_AFTER_ROUND_RESTART is set", <<0.5, 0.25, 0.5>>)
			PRINTLN(" LBOOL29_SHOULD_TURN_RADIO_BACK_ON_AFTER_ROUND_RESTART is set")
		ELSE
			DRAW_DEBUG_TEXT_2D("LBOOL29_SHOULD_TURN_RADIO_BACK_ON_AFTER_ROUND_RESTART is NOT set", <<0.5, 0.25, 0.5>>, 255, 0, 0)
			PRINTLN(" LBOOL29_SHOULD_TURN_RADIO_BACK_ON_AFTER_ROUND_RESTART is NOT set")
		ENDIF
		
		IF IS_VEHICLE_RADIO_ON(GET_VEHICLE_PED_IS_IN(localPlayerPed))
			DRAW_DEBUG_TEXT_2D("IS_VEHICLE_RADIO_ON", <<0.5, 0.3, 0.5>>)
			PRINTLN(" IS_VEHICLE_RADIO_ON")
		ELSE
			DRAW_DEBUG_TEXT_2D("IS_VEHICLE_RADIO_ON = FALSE", <<0.5, 0.3, 0.5>>, 255, 0, 0)
			PRINTLN(" IS_VEHICLE_RADIO_ON = FALSE")
		ENDIF
	ENDIF
	#ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_KEEP_RADIO_STATION_ON_RESPAWN)
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMBUSHKA(g_FMMC_STRUCT.iAdversaryModeType)
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_STOCKPILE(g_FMMC_STRUCT.iAdversaryModeType)
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_STUNTING_PACK(g_FMMC_STRUCT.iAdversaryModeType)
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUNNING_BACK_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMB_FOOTBALL(g_FMMC_STRUCT.iAdversaryModeType)
		IF NOT g_bMissionEnding
			g_SpawnData.bRadioStationOverScore = TRUE
			
			IF NOT IS_BIT_SET(iLocalBoolCheck29, LBOOL29_SHOULD_TURN_RADIO_BACK_ON_AFTER_ROUND_RESTART)
				IF GET_PLAYER_RESPAWN_BACK_AT_START_STATE(iLocalPart) = PRBSS_PlayingNormally
					IF NOT IS_PED_INJURED(LocalPlayerPed)
					AND IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					AND NOT IS_PED_DEAD_OR_DYING(localPlayerPed)
						IF DOES_PLAYER_VEH_HAVE_RADIO()
							IF IS_VEHICLE_RADIO_ON(GET_VEHICLE_PED_IS_IN(localPlayerPed))
								IF IS_PLAYER_VEH_RADIO_ENABLE() // This will only prevent url:bugstar:5057265 from happening, if the check for a valid radio station code side differentiates between player radio and veh radio.
									IF iRoundRestartLogicRadio != GET_PLAYER_RADIO_STATION_INDEX()
										PRINTLN("[RADIO][PROCESS_ROUND_RESTART_LOGIC_RADIO] - Caching player Radio iRadioStation / setting iRoundRestartLogicRadio to: ", GET_PLAYER_RADIO_STATION_INDEX())
									ENDIF
									iRoundRestartLogicRadio = GET_PLAYER_RADIO_STATION_INDEX()
								ELSE
									iRoundRestartLogicRadio = 255
									PRINTLN("[RADIO][PROCESS_ROUND_RESTART_LOGIC_RADIO] - (veh radio disabled) IS_PLAYER_VEH_RADIO_ENABLE() = FALSE, setting iRoundRestartLogicRadio to 255")
								ENDIF
							ELSE
								iRoundRestartLogicRadio = 255
								PRINTLN("[RADIO][PROCESS_ROUND_RESTART_LOGIC_RADIO] - (Vehicle radio not on) IS_VEHICLE_RADIO_ON() = FALSE, setting iRoundRestartLogicRadio to 255")
							ENDIF
						ELSE
							iRoundRestartLogicRadio = 255
							PRINTLN("[RADIO][PROCESS_ROUND_RESTART_LOGIC_RADIO] - (no radio vehicle) Caching player Radio iRadioStation: ", GET_PLAYER_RADIO_STATION_INDEX())
						ENDIF
						
						g_SpawnData.iRadioStation = iRoundRestartLogicRadio
						
					ENDIF
				ENDIF
				
				IF GET_PLAYER_RESPAWN_BACK_AT_START_STATE(iLocalPart) = PRBSS_Init				
					PRINTLN("[RADIO][PROCESS_ROUND_RESTART_LOGIC_RADIO](1) - PRBSS_Init and iRoundRestartLogicRadio = ", iRoundRestartLogicRadio)
					
					IF iRoundRestartLogicRadio != 255
					AND NOT IS_BIT_SET(iLocalBoolCheck29, LBOOL29_SHOULD_TURN_RADIO_BACK_ON_AFTER_ROUND_RESTART)
						SET_RADIO_FRONTEND_FADE_TIME(0.0)
						IF NOT IS_PED_INJURED(LocalPlayerPed)
						AND NOT IS_PED_DEAD_OR_DYING(localPlayerPed)
							SET_MOBILE_RADIO_ENABLED_DURING_GAMEPLAY(TRUE)
							SET_MOBILE_PHONE_RADIO_STATE(TRUE)
							SET_AUDIO_FLAG("MobileRadioInGame", TRUE)
						ENDIF
						
						IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
							SET_VEHICLE_RADIO_ENABLED(GET_VEHICLE_PED_IS_IN(localPlayerPed), FALSE)
						ENDIF					
						SET_RADIO_TO_STATION_INDEX(iRoundRestartLogicRadio)
						SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
						PRINTLN("[RADIO][PROCESS_ROUND_RESTART_LOGIC_RADIO] - Starting player Radio with iRadioStation: ", iRoundRestartLogicRadio)
						SET_BIT(iLocalBoolCheck29, LBOOL29_SHOULD_TURN_RADIO_BACK_ON_AFTER_ROUND_RESTART)
					ELSE
						PRINTLN("[RADIO][PROCESS_ROUND_RESTART_LOGIC_RADIO](2) - PRBSS_Init and iRoundRestartLogicRadio = ", iRoundRestartLogicRadio)
					ENDIF
					
					g_SpawnData.bRadioStationOverScore = TRUE
					g_SpawnData.iRadioStation = iRoundRestartLogicRadio
				ENDIF
			ENDIF
			
			IF GET_PLAYER_RESPAWN_BACK_AT_START_STATE(iLocalPart) = PRBSS_PlayingNormally
			AND IS_BIT_SET(iLocalBoolCheck29, LBOOL29_SHOULD_TURN_RADIO_BACK_ON_AFTER_ROUND_RESTART)
				IF NOT IS_PED_INJURED(LocalPlayerPed)
				AND NOT IS_PED_DEAD_OR_DYING(localPlayerPed)
					IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
						SET_MOBILE_RADIO_ENABLED_DURING_GAMEPLAY(FALSE)
						SET_MOBILE_PHONE_RADIO_STATE(FALSE)
						SET_AUDIO_FLAG("MobileRadioInGame", FALSE)
						SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
						
						IF NOT DOES_PLAYER_VEH_HAVE_RADIO()
							PRINTLN("[RADIO][PROCESS_ROUND_RESTART_LOGIC_RADIO] - Does not have a radio, setting iRoundRestartLogicRadio to 255.")
							iRoundRestartLogicRadio = 255
							SET_RADIO_TO_STATION_INDEX(255)							
						ELSE
							PRINTLN("[RADIO][PROCESS_ROUND_RESTART_LOGIC_RADIO] - Has a radio, calling SET_RADIO_TO_STATION_INDEX with iRoundRestartLogicRadio: ", iRoundRestartLogicRadio )
							g_SpawnData.iRadioStation = iRoundRestartLogicRadio
							g_SpawnData.bRadioStationOverScore = TRUE
							SET_RADIO_FRONTEND_FADE_TIME(0.0)
							SET_RADIO_TO_STATION_INDEX(iRoundRestartLogicRadio)
							SET_VEH_RADIO_STATION(GET_VEHICLE_PED_IS_IN(localPlayerPed), GET_RADIO_STATION_NAME(iRoundRestartLogicRadio))
							SET_VEHICLE_RADIO_ENABLED(GET_VEHICLE_PED_IS_IN(localPlayerPed), TRUE)
							CLEAR_BIT(iLocalBoolCheck29, LBOOL29_SHOULD_TURN_RADIO_BACK_ON_AFTER_ROUND_RESTART)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SET_SHUNT_OBJ_DROP_STATE(eSHUNT_DROP_STATE eNewState)
	eShuntDropState = eNewState
	PRINTLN("SET_SHUNT_OBJ_DROP_STATE - Setting Shunt drop state to : ", ENUM_TO_INT(eShuntDropState))
ENDPROC

FUNC PLAYER_INDEX GET_SHUNT_VEHICLE_PLAYER(VEHICLE_INDEX viShunter)
	IF DOES_ENTITY_EXIST(viShunter)
		PED_INDEX piShuntDriver = GET_PED_IN_VEHICLE_SEAT(viShunter)
		IF DOES_ENTITY_EXIST(piShuntDriver)
			PLAYER_INDEX playerShuntDriver = NETWORK_GET_PLAYER_INDEX_FROM_PED(piShuntDriver)
			IF NETWORK_IS_PLAYER_ACTIVE(playerShuntDriver)
				RETURN playerShuntDriver
			ELSE
				PRINTLN("[PROCESS_SHUNT_PICKUP_RETURN] GET_SHUNT_VEHICLE_PLAYER - Shunt Driver is not active")
			ENDIF
		ELSE
			PRINTLN("[PROCESS_SHUNT_PICKUP_RETURN] GET_SHUNT_VEHICLE_PLAYER - Shunt driver ped doesn't exist")
		ENDIF
	ELSE
		PRINTLN("[PROCESS_SHUNT_PICKUP_RETURN] GET_SHUNT_VEHICLE_PLAYER - Shunter vehicle doesn't exist")
	ENDIF
	RETURN INVALID_PLAYER_INDEX()
ENDFUNC

FUNC INT GET_SHUNT_VEHICLE_TEAM(VEHICLE_INDEX viShunter)
	PLAYER_INDEX playerShuntDriver = GET_SHUNT_VEHICLE_PLAYER(viShunter)
	
	IF playerShuntDriver != INVALID_PLAYER_INDEX()
		PRINTLN("[PROCESS_SHUNT_PICKUP_RETURN] GET_SHUNT_VEHICLE_TEAM - Shunt vehicle team: ", GET_PLAYER_TEAM(playerShuntDriver))
		RETURN GET_PLAYER_TEAM(playerShuntDriver)
	ENDIF
	
	PRINTLN("[PROCESS_SHUNT_PICKUP_RETURN] GET_SHUNT_VEHICLE_TEAM - Returning -1")
	RETURN -1
ENDFUNC

PROC PROCESS_SHUNT_PICKUP_RETURN()
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_EnableShuntPickupReturn)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_DROP_OBJECT)
		EXIT
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
	AND NOT IS_BIT_SET(iLocalBoolCheck29, LBOOL29_MARKED_FOR_SHUNT_OBJ_DROP)
		VEHICLE_INDEX viMyVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		IF DOES_ENTITY_EXIST(viMyVeh)
		AND GET_HAS_VEHICLE_BEEN_HIT_BY_SHUNT(viMyVeh)
			PRINTLN("PROCESS_SHUNT_PICKUP_RETURN - I have been shunted")
			VEHICLE_INDEX viShunter = GET_LAST_SHUNT_VEHICLE(viMyVeh)
			INT iShuntTeam = GET_SHUNT_VEHICLE_TEAM(viShunter)
			
			IF NOT IS_VEHICLE_FUCKED_MP(viShunter)
				IF iShuntTeam != -1
				AND iShuntTeam != MC_playerBD[iLocalPart].iteam
				AND GET_IS_VEHICLE_SHUNTING(viShunter)
					PRINTLN("PROCESS_SHUNT_PICKUP_RETURN - I have been shunted by someone on the opposing team.")
					//Set up all the data to use in switch
					INT iObjDropped = GET_OBJECT_INDEX_ATTACHED_TO_ENTITY(LocalPlayerPed)
					IF iObjDropped != -1
						PRINTLN("PROCESS_SHUNT_PICKUP_RETURN - About to drop & return object ", iObjDropped)
						OBJECT_INDEX tempObj = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iObjDropped])
						IF DOES_ENTITY_EXIST(tempObj)
							oiShuntObjToDrop = tempObj
							
							iShuntObjToDrop = iObjDropped	
							IF DOES_ENTITY_EXIST(oiShuntObjToDrop)
							AND iShuntObjToDrop != -1
								PRINTLN("PROCESS_SHUNT_PICKUP_RETURN - Setting LBOOL29_MARKED_FOR_SHUNT_OBJ_DROP. Begin return flag process.")
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_SHARD_ON_OBJECT_STOLEN)
									BROADCAST_FMMC_RETURNED_FLAG_SHARD(iShuntTeam, GET_SHUNT_VEHICLE_PLAYER(viShunter), TRUE)
								ENDIF
								SET_BIT(iLocalBoolCheck29, LBOOL29_MARKED_FOR_SHUNT_OBJ_DROP)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_MARKED_FOR_SHUNT_OBJ_DROP)
		IF DOES_ENTITY_EXIST(oiShuntObjToDrop)
			IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(oiShuntObjToDrop)
				NETWORK_REQUEST_CONTROL_OF_ENTITY(oiShuntObjToDrop)			
			ENDIF
		ELSE
			// url:bugstar:5361953 = Must have been some syncing issues between remote and local player and whether they had an object or not...
			PRINTLN("[OBJ][PROCESS_SHUNT_PICKUP_RETURN] - oiShuntObjToDrop OBJECT DOES NOT EXIST - CLEANING UP STATE AND CLEARING: LBOOL29_MARKED_FOR_SHUNT_OBJ_DROP")
			SET_SHUNT_OBJ_DROP_STATE(eShuntDrop_CLEANUP)
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(oiShuntObjToDrop)
	AND NETWORK_HAS_CONTROL_OF_ENTITY(oiShuntObjToDrop)
		SWITCH eShuntDropState
			CASE eShuntDrop_IDLE
				IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_MARKED_FOR_SHUNT_OBJ_DROP)
					SET_SHUNT_OBJ_DROP_STATE(eShuntDrop_DROP)
				ENDIF
			BREAK
			CASE eShuntDrop_DROP
				SET_ENTITY_VISIBLE(oiShuntObjToDrop, FALSE)
				
				IF IS_ENTITY_ATTACHED_TO_ANY_PED(oiShuntObjToDrop)
					DETACH_PORTABLE_PICKUP_FROM_PED(oiShuntObjToDrop)
				ENDIF		
				SET_TEAM_PICKUP_OBJECT(oiShuntObjToDrop, MC_playerBD[iLocalPart].iteam, FALSE)
				
				PRINTLN("PROCESS_SHUNT_PICKUP_RETURN - Dropping object: ", iShuntObjToDrop)
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_EnableShuntPickupReturn)
					SET_SHUNT_OBJ_DROP_STATE(eShuntDrop_RETURN)
	//			ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_DROP_OBJECT)
	//				SET_SHUNT_OBJ_DROP_STATE(eShuntDrop_REVALIDATEPICKUP)
				ENDIF
			BREAK
			CASE eShuntDrop_RETURN			
				IF NOT IS_ENTITY_ATTACHED(oiShuntObjToDrop)
					PRINTLN("PROCESS_SHUNT_PICKUP_RETURN - Attempting to move object back to it's spawn")
					SET_ENTITY_COORDS(oiShuntObjToDrop, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iShuntObjToDrop].vPos)
					SET_ENTITY_ROTATION(oiShuntObjToDrop, <<0.0,0.0,g_FMMC_STRUCT_ENTITIES.sPlacedObject[iShuntObjToDrop].fHead>>)
				ELSE
					PRINTLN("PROCESS_SHUNT_PICKUP_RETURN - Object is still attached")
				ENDIF
				
				IF ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(oiShuntObjToDrop), g_FMMC_STRUCT_ENTITIES.sPlacedObject[iShuntObjToDrop].vPos, 5)
					PRINTLN("PROCESS_SHUNT_PICKUP_RETURN - Object successfully moved")
					SET_SHUNT_OBJ_DROP_STATE(eShuntDrop_REVALIDATEPICKUP)
				ENDIF
			BREAK
			CASE eShuntDrop_REVALIDATEPICKUP
				IF NOT HAS_NET_TIMER_STARTED(tdShuntObjDropTimer)
					PRINTLN("PROCESS_SHUNT_PICKUP_RETURN - Starting revalidate timer")
					REINIT_NET_TIMER(tdShuntObjDropTimer)
				ELSE
					IF HAS_NET_TIMER_EXPIRED(tdShuntObjDropTimer, ciShuntObjDropTimeLength)
						PRINTLN("PROCESS_SHUNT_PICKUP_RETURN - Timer expired, revalidating pickup")
						SET_TEAM_PICKUP_OBJECT(oiShuntObjToDrop, MC_playerBD[iLocalPart].iteam, TRUE)
						SET_ENTITY_VISIBLE(oiShuntObjToDrop, TRUE)
						SET_SHUNT_OBJ_DROP_STATE(eShuntDrop_CLEANUP)
					ENDIF
				ENDIF
			BREAK
			CASE eShuntDrop_CLEANUP
				SET_TEAM_PICKUP_OBJECT(oiShuntObjToDrop, MC_playerBD[iLocalPart].iteam, TRUE)
				SET_ENTITY_VISIBLE(oiShuntObjToDrop, TRUE)
				RESET_NET_TIMER(tdShuntObjDropTimer)
				oiShuntObjToDrop = NULL
				iShuntObjToDrop = -1
				CLEAR_BIT(iLocalBoolCheck29, LBOOL29_MARKED_FOR_SHUNT_OBJ_DROP)
				SET_SHUNT_OBJ_DROP_STATE(eShuntDrop_IDLE)
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

PROC PROCESS_TONY_HAWKS_MODE()
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_StuntRunMode)
		EXIT
	ENDIF
	IF HAS_NET_TIMER_STARTED(MC_playerBD_1[iLocalPart].tdComboTimer)
		IF HAS_NET_TIMER_EXPIRED(MC_playerBD_1[iLocalPart].tdComboTimer, ciTHPSTimer)
			MC_playerBD_1[iLocalPart].iStuntComboMultiplier = 1
		ENDIF
	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		MC_playerBD_1[iLocalPart].iStuntComboMultiplier = 1
		RESET_NET_TIMER(MC_playerBD_1[iLocalPart].tdComboTimer)
	ENDIF
	
	IF bStuntLanded
		
	ENDIF
ENDPROC

FUNC BOOL SHOULD_TEAM_BE_PASSIVE(INT iTeam)
	IF MC_playerBD[iLocalPart].iTeam != iTeam
	AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
		SWITCH iTeam
			CASE 0	RETURN IS_BIT_SET(g_FMMC_STRUCT.iBS_MissionTeamPassive, FMMC_TEAM_PASSIVE_0_0 + MC_playerBD[iLocalPart].iTeam)
			CASE 1	RETURN IS_BIT_SET(g_FMMC_STRUCT.iBS_MissionTeamPassive, FMMC_TEAM_PASSIVE_1_0 + MC_playerBD[iLocalPart].iTeam)
			CASE 2	RETURN IS_BIT_SET(g_FMMC_STRUCT.iBS_MissionTeamPassive, FMMC_TEAM_PASSIVE_2_0 + MC_playerBD[iLocalPart].iTeam)
			CASE 3	RETURN IS_BIT_SET(g_FMMC_STRUCT.iBS_MissionTeamPassive, FMMC_TEAM_PASSIVE_3_0 + MC_playerBD[iLocalPart].iTeam)
		ENDSWITCH
	ENDIF	
	RETURN FALSE
ENDFUNC
PROC SET_TEAMS_AS_PASSIVE()
	INT iPlayerLoop
	PED_INDEX tempPlayerPed
	PLAYER_INDEX tempPlayer
	FOR iPlayerLoop = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		tempPlayer = INT_TO_NATIVE(PLAYER_INDEX, iPlayerLoop)
		IF NETWORK_IS_PLAYER_ACTIVE(tempPlayer)
		AND SHOULD_TEAM_BE_PASSIVE(MC_playerBD[iPlayerLoop].iteam)
			tempPlayerPed = GET_PLAYER_PED(tempPlayer)
			IF NOT IS_PED_INJURED(tempPlayerPed)
				IF NOT IS_BIT_SET(iLocalGhostRemotePlayerBitSet, iPlayerLoop)
					SET_REMOTE_PLAYER_AS_GHOST(tempPlayer, TRUE)
					PRINTLN("[RCC MISSION] SET_TEAMS_AS_PASSIVE - Setting player ", iPlayerLoop, " as ghosted")
					SET_BIT(iLocalGhostRemotePlayerBitSet, iPlayerLoop)
				ENDIF
			ELSE
				CLEAR_BIT(iLocalGhostRemotePlayerBitSet, iPlayerLoop)
				PRINTLN("[RCC MISSION] SET_TEAMS_AS_PASSIVE - Clearing bit ", iPlayerLoop, " as player died")
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

FUNC BOOL GRAB_ARENA_WINNER_VEHICLE_NAME()
	IF IS_STRING_NULL_OR_EMPTY(tl23WinnerVehicle)
		IF CONTENT_IS_USING_ARENA() 
		
			INT iWinnerLBDIndex = GET_LEADERBOARD_INDEX_OF_TOP_WINNER()
			INT iWinningPlayerID = NATIVE_TO_INT(GET_PLAYER_IN_LEADERBOARD_POS(iWinnerLBDIndex))
			PLAYER_INDEX playerWinner = INT_TO_PLAYERINDEX(iWinningPlayerID)
		
			IF IS_PLAYER_SELECTING_CUSTOM_VEHICLE(playerWinner)
				IF g_bMissionOver
				
					PARTICIPANT_INDEX partWinner
					INT iWinningParticipant = -1
					IF NETWORK_IS_PLAYER_ACTIVE(playerWinner)
						partWinner = NETWORK_GET_PARTICIPANT_INDEX(playerWinner)
						iWinningParticipant = NATIVE_TO_INT(partWinner)
					ENDIF
					PRINTLN("GRAB_ARENA_WINNER_VEHICLE_NAME 5456221- Grabbing winning player details from top player")
					PRINTLN("GRAB_ARENA_WINNER_VEHICLE_NAME 5456221- iWinnerLBDIndex: ", iWinnerLBDIndex, " iWinningPlayerID: ", iWinningPlayerID)
					PRINTLN("GRAB_ARENA_WINNER_VEHICLE_NAME 5456221- playerWinner: ", GET_PLAYER_NAME(playerWinner))
					
					INT iSlot 
					IF iWinningParticipant != -1
						iSlot = MC_playerBD_1[iWinningParticipant].iDisplaySlot
						PRINTLN("GRAB_ARENA_WINNER_VEHICLE_NAME, 5456221 iSlot = ", iSlot, " iWinningParticipant = ", iWinningParticipant)
					ENDIF
					
					IF playerWinner = PLAYER_ID()
					
						tl23WinnerVehicle = GET_ARENA_VEHICLE_NAME(DEFAULT, MC_playerBD_1[iLocalPart].iDisplaySlot)
						PRINTLN("GRAB_ARENA_WINNER_VEHICLE_NAME, 5456221 tl23WinnerVehicle = ", tl23WinnerVehicle)
					ELSE
						INT iPlayer = NATIVE_TO_INT(playerWinner)
						IF iPlayer != -1
						AND GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.eLanguage = GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.eLanguage
							BOOL bRestricted
							IF NOT REQUEST_REMOTE_PLAYER_ARENA_VEHICLE_NAME(playerWinner, iSlot, tl23WinnerVehicle, bRestricted)
								RETURN FALSE
							ELSE
								IF bRestricted
									tl23WinnerVehicle = ""
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PROC_MISSION_ARENA_ANNOUNCER_TRACKING()
	IF NOT CONTENT_IS_USING_ARENA()
		EXIT
	ENDIF
	IF NOT g_bMissionClientGameStateRunning
		EXIT
	ENDIF
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	IF iTeam > -1 AND iTeam < FMMC_MAX_TEAMS
		INT iTeamLoop
		INT iPart
		INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
		
		INT iTeamsLeft = 0
		INT iTeamCountedBS = 0
		
		IF iRule < FMMC_MAX_RULES
			
			// Round Almost Over (time)
			IF HAS_NET_TIMER_STARTED(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeam])
				INT ms = MC_serverBD_3.iMultiObjectiveTimeLimit[MC_playerBD[iPartToUse].iteam] - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[MC_playerBD[iPartToUse].iTeam])				
				IF ms <= 30000
					SET_ARENA_ANNOUNCER_BS_GENERAL(g_iAA_PlaySound_Generic_RoundAlmostOver, TRUE)
				ENDIF
			ENDIF
			
			// Last/Half Players Remaining.
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Enable_Turn_Rotor_Tag_Team)
			AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_MONSTER_JAM(g_FMMC_STRUCT.iAdversaryModeType)
			AND NOT IS_MC_PLAYER_MINI_ROUND_RESTARTING()
			AND NOT (IS_ARENA_WARS_JOB(TRUE) AND IS_ARENA_WARS_JOB())
				IF NOT IS_ARENA_ANNOUNCER_BS_GENERAL_LOCK_SET(g_iAA_PlaySound_Generic_HalfPlayerRemaining)
				OR NOT IS_ARENA_ANNOUNCER_BS_GENERAL_LOCK_SET(g_iAA_PlaySound_Generic_LastPlayerRemaining)
					INT iPlayersLeft[FMMC_MAX_TEAMS]
					INT iPlayersMax[FMMC_MAX_TEAMS]
					INT iPlayersLeftTotal
					INT iPlayersMaxTotal
					FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
						PARTICIPANT_INDEX piPart = INT_TO_PARTICIPANTINDEX(iPart)
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
							PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)
							IF NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(piPlayer)
							AND NOT IS_PLAYER_SCTV(piPlayer)
							AND IS_BIT_SET(MC_playerBD[iPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
								IF MC_playerBD[iPart].iTeam > -1 AND MC_playerBD[iPart].iTeam < FMMC_MAX_TEAMS
									IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_PLAYER_FAIL)
									AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES)
									AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_FINISHED)
										iPlayersLeft[MC_playerBD[iPart].iTeam]++
										iPlayersLeftTotal++
										
										IF NOT IS_BIT_SET(iTeamCountedBS, MC_playerBD[iPart].iTeam)
											iTeamsLeft++
											SET_BIT(iTeamCountedBS, MC_playerBD[iPart].iTeam)
										ENDIF
									ENDIF
									iPlayersMax[MC_playerBD[iPart].iTeam]++
									iPlayersMaxTotal++
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_EnableGameMastersMode)
						FOR iTeamLoop = 0 TO MC_serverBD.iNumberOfTeams - 1
							IF iPlayersLeft[iTeamLoop] = iPlayersMax[iTeamLoop]/2
							AND iPlayersMax[iTeamLoop]/2 > 1
								SET_ARENA_ANNOUNCER_BS_GENERAL(g_iAA_PlaySound_Generic_HalfPlayerRemaining, TRUE)
							ENDIF					
							IF iPlayersLeft[iTeamLoop] = 1
							AND iPlayersMax[iTeamLoop] > 1
								SET_ARENA_ANNOUNCER_BS_GENERAL(g_iAA_PlaySound_Generic_LastPlayerRemaining, TRUE)
							ENDIF
						ENDFOR
					ELSE
						IF iPlayersLeftTotal = iPlayersMaxTotal/2
						AND iPlayersMaxTotal/2 > 1
							SET_ARENA_ANNOUNCER_BS_GENERAL(g_iAA_PlaySound_Generic_HalfPlayerRemaining, TRUE)
						ENDIF					
						IF iPlayersLeftTotal = 1
						AND iPlayersMaxTotal > 1
							SET_ARENA_ANNOUNCER_BS_GENERAL(g_iAA_PlaySound_Generic_LastPlayerRemaining, TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			INT iGMActiveTeam = -1
			IF NOT IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_GAME_MASTERS_TEAM_SWAPPED_ROLES_TEAM_0)
			AND NOT IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_GAME_MASTERS_TEAM_SWAPPED_ROLES_TEAM_1)
			AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet7, SBBOOL7_MULTIRULE_TIMER_REINIT)
				iGMActiveTeam = MC_serverBD_3.iGameMasterCurrentTeamActive
			ENDIF
			
			IF NOT IS_MC_PLAYER_MINI_ROUND_RESTARTING()
				PROCESS_SHARED_ARENA_ANNOUNCER_TRACKING(td_ArenaWarsBigAir, iGMActiveTeam, iTeamsLeft)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_CASINO_HEIST_FUNCTIONALITY()
	PROCESS_CASINO_VAULT_DOOR()
	PROCESS_CASINO_TUNNEL_RUBBLE_PROP()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_CASINO_LOCK_OUTER_MANTRAP_DOORS)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_CASINO_LOCK_INNER_MANTRAP_DOORS)
		PROCESS_CASINO_HEIST_MANTRAP_DOORS(IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_CASINO_LOCK_OUTER_MANTRAP_DOORS), IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_CASINO_LOCK_INNER_MANTRAP_DOORS))
		SET_BIT(g_TransitionSessionNonResetVars.iFMMCMissionBS, iFMMCMissionBS__MISSION_CONTROLLER_HANDLING_MANTRAP_DOORS)
	ELSE
		IF IS_BIT_SET(g_TransitionSessionNonResetVars.iFMMCMissionBS, iFMMCMissionBS__MISSION_CONTROLLER_HANDLING_MANTRAP_DOORS)
			CLEANUP_CASINO_HEIST_MANTRAP_DOORS_FROM_TRANSITION()
			CLEAR_BIT(g_TransitionSessionNonResetVars.iFMMCMissionBS, iFMMCMissionBS__MISSION_CONTROLLER_HANDLING_MANTRAP_DOORS)
		ENDIF
	ENDIF
	
	IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE()
		IF MC_playerBD[iLocalPart].iGameState != GAME_STATE_RUNNING
			IF GET_USINGNIGHTVISION()
			OR GET_REQUESTINGNIGHTVISION()
				ENABLE_NIGHTVISION(VISUALAID_OFF)
				PRINTLN("[PLAYER VISUAL AIDS][PhoneEMP] PROCESS_CASINO_HEIST_FUNCTIONALITY - Binning nightvision if it is on")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_PREVENT_TAKEDOWNS_ON_PLAYER()
	IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
		EXIT
	ENDIF
	
	INT iPart
	INT iPlayersChecked
	INT iPlayersToCheck = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		
		IF iPlayersChecked > iPlayersToCheck
			BREAKLOOP
		ENDIF	
		
		IF IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)
			RELOOP
		ENDIF
				
		PARTICIPANT_INDEX piPart = INT_TO_PARTICIPANTINDEX(iPart)
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(piPart)
			RELOOP
		ENDIF
		
		PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)
		
		IF NOT NETWORK_IS_PLAYER_ACTIVE(piPlayer)
			RELOOP
		ENDIF
		
		PED_INDEX pedPlayer = GET_PLAYER_PED(piPlayer)
		
		IF IS_PED_INJURED(pedPlayer)
			RELOOP
		ENDIF
		
		INT iTeam = MC_PlayerBD[iPart].iteam
		
		IF iTeam <= -1
		AND iTeam > FMMC_MAX_TEAMS
			RELOOP
		ENDIF
			
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		
		IF iRule >= FMMC_MAX_RULES
			RELOOP
		ENDIF
		
		iPlayersChecked++

		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFourteen[iRule], ciBS_RULE14_PREVENT_TAKEDOWNS_FOR_TEAM_THIS_RULE)		
			RELOOP
		ENDIF
		
		
		SET_PED_RESET_FLAG(pedPlayer, PRF_PreventAllStealthKills, TRUE)
		SET_PED_RESET_FLAG(pedPlayer, PRF_PreventAllMeleeTakedowns, TRUE)

		PRINTLN("[LM] PROCESS_PREVENT_TAKEDOWNS_ON_PLAYER - Preventing Takedowns from being performed on iPart: ", iPart)
		
	ENDFOR
		
ENDPROC

// // [FIX_2020_CONTROLLER] - Loose Logic could be functionalised so that it's easier to read execution order of logic. (I started doing this  about 100 lines down.)
PROC PROCESS_EVERY_FRAME_PLAYER_CHECKS()	
	
	INT iTeam = MC_playerBD[iLocalPart].iteam
	
	PROCESS_PREVENT_TAKEDOWNS_ON_PLAYER()
	
	GRAB_ARENA_WINNER_VEHICLE_NAME()
	
	PROCESS_SERVER_DESTROY_PLACED_VEHICLE_FOR_LEAVER()	
	
	IF GET_PED_DENSITY_FROM_INT(g_FMMC_STRUCT.iPedDensity) = 0
		SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	ENDIF
	IF GET_MISSION_VEHICLE_DENSITY_FROM_INT(g_FMMC_STRUCT.iTraffic) = 0		
		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	ENDIF
		
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARZONE(g_FMMC_STRUCT.iAdversaryModeType)
		#IF IS_DEBUG_BUILD
			PRINT_CUSTOM_SPAWN_POINT_INFO(g_SpawnData.CustomSpawnPointInfo) 
		#ENDIF
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet,SBBOOL_MISSION_OVER)					
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitSet3, ciBS3_ENABLE_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN)
			SET_ON_TEAM_RACE_RACE_GLOBAL(FALSE)
		ENDIF
	ENDIF
		
	// For checks to see if any player is an mp property. I need to maintain my own bitset in player bd.
	CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_HEIST_I_AM_INSIDE_ANY_MP_PROPERTY)
	IF bLocalPlayerOK
		IF IS_PLAYER_IN_MP_PROPERTY(LocalPlayer, FALSE)
			SET_BIT(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_HEIST_I_AM_INSIDE_ANY_MP_PROPERTY)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciRUGBY_HEARTBEAT)
		PROCESS_INCH_BY_INCH_HEARTBEAT()
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_LOST_DAMNED(g_FMMC_STRUCT.iAdversaryModeType)
		IF NOT IS_LOCAL_PLAYER_SPECTATOR()
			PROCESS_HALLOWEEN_MODE_LOGIC()
		ENDIF
	ELSE
		//Halloween mode logic
		PROCESS_HALLOWEEN_MODE_LOGIC()
	ENDIF
	
	IF iTeam >= 0 AND iTeam < FMMC_MAX_TEAMS
		INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
		IF iRule >= 0 AND iRule < FMMC_MAX_RULES	
			PROCESS_PLAYER_RESPAWN_BAR_HUD(iTeam,iRule)
			PROCESS_PLAYER_TOUCH(iRule,iTeam)
			PROCESS_SMASH_CAR()
			PROCESS_EXTRA_VEHICLE_COLLISION_DAMAGE(timerExtraVehDamageForceHitting, ciTimeExtraVehDamageToBeHittingAgain) // Might only need this temp until code solution. Bug: -- url:bugstar:5338680 - [SCRIPT WORKAROUND] Vamos2 - The weapons on this vehicle don't seem to do extra damage to other vehicles
		ENDIF
	ENDIF
	
	BLOCK_INVALID_CLIMBABLE_AREAS_IN_OLD_INTERIORS()
	
	HANDLE_LADDER_DISABLED_ON_RULE()
	//MANAGE_CRATE_INVESTIGATION_LOGIC()
	
	//Boss Type (CEO & MC President etc.)
	IF iBossType = -1
		IF DOES_CURRENT_MISSION_USE_GANG_BOSS()
		OR VCM_FLOW_IS_THIS_MISSION_VCM_FLOW(g_FMMC_STRUCT.iRootContentIDHash)
			PLAYER_INDEX bossIndex = GB_GET_LOCAL_PLAYER_GANG_BOSS()
			IF bossIndex != INVALID_PLAYER_INDEX()
				IF NETWORK_IS_PLAYER_ACTIVE(bossIndex)
				AND NETWORK_IS_PLAYER_A_PARTICIPANT(bossIndex)
					IF GB_IS_PLAYER_BOSS_OF_A_GANG_TYPE(bossIndex, GT_BIKER)
						iBossType = ciBOSSTYPE_MC_PRESIDENT
					ELSE
						iBossType = ciBOSSTYPE_CEO
					ENDIF
					
					PRINTLN("[BOSSTYPE] iBossType = ", iBossType)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bIsLocalPlayerHost
		IF PERFORM_INITIAL_SCORE_COUNT()
		AND NOT IS_BIT_SET(iLocalBoolCheck20, LBOOL20_INITIAL_SCORE_COUNT_DONE)
			INT i
			FOR i = 0 TO NETWORK_GET_NUM_PARTICIPANTS()
				COUNT_PLAYER_SCORES(i)
				IF i = NETWORK_GET_NUM_PARTICIPANTS()
					SET_BIT(iLocalBoolCheck20, LBOOL20_INITIAL_SCORE_COUNT_DONE)
				ENDIF
			ENDFOR
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciALTERNATIVE_HOLDING_PACKAGE_SCORING)
			IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_ALTERNATIVE_GD_SCORE_ADDED)
				IF IS_BIT_SET(iLocalBoolCheck6, (LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iLocalPart].iteam))
					CLEAR_BIT(iLocalBoolCheck25, LBOOL25_ALTERNATIVE_GD_SCORE_ADDED)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciREMOVE_PLAYER_HELMETS)
		REMOVE_PLAYER_HELMET(GET_PLAYER_INDEX(), TRUE)
		PRINTLN("Calling REMOVE_PLAYER_HELMET due to ciREMOVE_PLAYER_HELMETS")
	ENDIF
		
	PROCESS_PLAYER_FORCED_SEAT_PREF_SPECIAL()
	
	PROCESS_PLAYER_PASSENGER_BOMBING()	
		
	PROCESS_AIR_QUOTA_PTFX()
	
	PROCESS_POINTLESS_MODE()
	
	PROCESS_HARD_TARGET_MODE()
	
	PROCESS_DROP_THE_BOMB_MODE()
	
	PROCESS_BOMB_FOOTBALL_MODE()
	
	PROCESS_SUMO_REMIX_MODE()
	
	PROCESS_TRADING_PLACES_REMIX_MODE()
	
	PROCESS_STUNTING_PACK()
	
	PROCESS_DISABLE_HEADLIGHT_CONTROL()
	
	PROCESS_MONSTER_JAM_MODE()
	
	PROCESS_ARENA_MODES()
	
	PROCESS_CASINO_INTERIOR_SOUND_SCENE()
		
	IF NOT SHOULD_HIDE_THE_HUD_THIS_FRAME()
		PROCESS_VEHICLE_WEAPON_FAKE_AMMO(fVehicleWeaponShootingTime, fVehicleWeaponTimeSinceLastfired, bRegenVehicleWeapons, iFakeAmmoRechargeSoundID)
	ENDIF
	
	IF USING_SHOWDOWN_POINTS()
	AND MC_playerBD[iLocalPart].iGameState = GAME_STATE_RUNNING
	AND GET_MC_SERVER_GAME_STATE() = GAME_STATE_RUNNING
		USE_VEHICLE_CAM_STUNT_SETTINGS_THIS_UPDATE()
		PROCESS_SHOWDOWN_POINTS()
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_TINY_RACERS_CHECKPOINT_LOGIC)
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
		REPOSITION_PICKUPS_FOR_BIRDSEYE_CAM()
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_STATIC_PICKUPS)
		PROCESS_STATIC_PICKUPS()
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMBUSHKA(g_FMMC_STRUCT.iAdversaryModeType)
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(GET_PLAYER_INDEX()))
			IF NOT GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()
				SET_PED_CONFIG_FLAG(localPlayerPed, PCF_PreventAutoShuffleToDriversSeat, TRUE)
				SET_PED_CONFIG_FLAG(localPlayerPed, PCF_PreventAutoShuffleToTurretSeat, TRUE)
				SET_PED_CONFIG_FLAG(localPlayerPed, PCF_AllowAutoShuffleToDriversSeat, FALSE)
			ELSE
				SET_PED_CONFIG_FLAG(localPlayerPed, PCF_PreventAutoShuffleToDriversSeat, FALSE)
				SET_PED_CONFIG_FLAG(localPlayerPed, PCF_AllowAutoShuffleToDriversSeat, TRUE)
			ENDIF
		ENDIF
		PRINTLN("[LM][BOMBUSHKA][SEAT SHUFFLE CHECK] - PCF_PreventAutoShuffleToDriversSeat: ", GET_PED_CONFIG_FLAG(localPlayerPed, PCF_PreventAutoShuffleToDriversSeat, TRUE),
				"  PCF_PreventAutoShuffleToTurretSeat: ", GET_PED_CONFIG_FLAG(localPlayerPed, PCF_PreventAutoShuffleToTurretSeat, TRUE), 
				"  PCF_AllowAutoShuffleToDriversSeat: ", GET_PED_CONFIG_FLAG(localPlayerPed, PCF_AllowAutoShuffleToDriversSeat, FALSE))
	ENDIF
	
	IF IS_PED_INJURED(localPlayerPed)
	OR NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_BLOCK_MANUAL_RESPAWN_DUE_TO_VEHICLE)
			PRINTLN("[LM][MANUAL RESPAWN] - Not in a vehicle anymore, clearing: LBOOL25_BLOCK_MANUAL_RESPAWN_DUE_TO_VEHICLE")
			CLEAR_BIT(iLocalBoolCheck25, LBOOL25_BLOCK_MANUAL_RESPAWN_DUE_TO_VEHICLE)
		ENDIF
	ENDIF		
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_TINY_RACERS_CHECKPOINT_LOGIC)
	AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		TINY_RACERS_PROCESS_MINI_ROUND()
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_DUNE_EXPLOSIONS)
		PROCESS_VEHICLES_FLIPPED_BY_DUNE()
	ENDIF
	
	PROCESS_CLEAR_CUSTOM_PERSONAL_VEHICLE_NODE_FOR_FACILITY()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciHIDE_AIM_RETICLE)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RETICLE)
	ENDIF
	
	//PROCESS_CARGOBOB_ATTACHMENT()
	IF NOT IS_LOCAL_PLAYER_SPECTATOR()
		PROCESS_CARGOBOB_CATCH()
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck20, LBOOL20_SKIP_HELP_TEXT)
		SKIP_SKIPPABLE_HELP_TEXT()
	ENDIF
	
	// Storing assists
	IF MC_PlayerBD[iPartToUse].iAssists != g_iMyAssists
		MC_PlayerBD[iPartToUse].iAssists = g_iMyAssists
	ENDIF

	//Clean up for laptop hacking
	PROCESS_HACKING_CLEANUP()
	
	INT iPriority = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF AM_I_ON_A_HEIST()
	OR IS_FAKE_MULTIPLAYER_MODE_SET()
	OR (iPriority < FMMC_MAX_RULES AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFourteen[iPriority], ciBS_RULE14_ATTACH_FLARE_TO_BACK))
		IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_HUMANE_FLARE_PROCESSING_DONE)
			MANAGE_UNDERWATER_FLARE()
		ENDIF
	ENDIF
	
	PROCESS_HEIST_OUTFIT_BEHAVIOURS()	
	
	IF iPriority > -1 AND iPriority < FMMC_MAX_RULES
	AND NOT g_bMissionEnding
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TOUR_DE_FORCE (g_FMMC_STRUCT.iAdversaryModeType)
			MANAGE_AUDIO_FOR_SLIPSTREAM()
		ENDIF
	ENDIF
	
	PROCESS_PLAYER_SECONDARY_ANIMS()
	
	PROCESS_WEAPON_AND_LOADOUTS()
	
	PROCESS_PLAYER_MISSION_PARACHUTES()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciENABLE_STUNT_DRIVING)
		IF NOT IS_BIT_SET(iLocalBoolCheck19, LBOOL19_SET_UP_VEHICLE_FOR_STUNT_RACE)
			VEHICLE_INDEX viPlayerVeh
			IF DOES_ENTITY_EXIST(LocalPlayerPed)
				IF NOT IS_PED_DEAD_OR_DYING(LocalPlayerPed)
					IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
						viPlayerVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
						IF NOT CONTENT_IS_USING_ARENA()
							STUNT_RACE_VEHICLE_DAMAGE_SCALE(viPlayerVeh)
						ENDIF
						SET_IN_STUNT_MODE(TRUE)
						SET_BIT(iLocalBoolCheck19, LBOOL19_SET_UP_VEHICLE_FOR_STUNT_RACE)
						PRINTLN("[AW_MISSION] SETTING UP VEHICLE FOR STUNT RACE")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	PROCESS_NAVMESH_UNLOADING_WHEN_UNSAFE()
	
	PROCESS_RULE_WEAPON_DISABLING()
	
	IF NOT g_bMissionEnding
	AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
	AND NOT HAS_TEAM_FINISHED(MC_playerBD[iPartToUse].iteam)
	AND NOT HAS_TEAM_FAILED(MC_playerBD[iPartToUse].iteam)
	AND NOT IS_PED_INJURED(PlayerPedToUse)
		PROCESS_RULE_OUTFITS()
	ENDIF
		
	// Update the Vehicle, then update the health, then update the weapons.
	IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
		PROCESS_SWAP_VEHICLE_ON_RULE()
		PROCESS_UPDATE_VEHICLE_HEALTH_ON_RULE()
		PROCESS_UPDATE_SPECIAL_VEHICLE_WEAPONS()
		PROCESS_CURRENT_RUINER_ROCKET_AMMO()
	ENDIF
	
	PROCESS_SWAP_VEHICLE_RETRACTABLE_WHEEL_BUTTON()
	
	//2316125
	IF IS_TEAM_DEATHMATCH()
	OR IS_TEAM_COOP_MISSION()
	OR IS_TEAM_VS_MISSION()
		UPDATE_PC_TEXT_CHAT_COLOUR(g_FMMC_STRUCT.iTeamColourOverride[MC_playerBD[iPartToUse].iTeam],IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciDROP_ZONE_TEAM_COLOURS))
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciENABLE_BIKER_BOOST_IF_STATIONARY)
		BOOST_AT_START_OF_RULE_CHANGE()
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_Struct.iTeamVehicleBitSet, ciFMMC_TeamVehBS_DISABLE_TEAM_RESPAWN_VEHICLE_SPACE_ROCKETS)
		IF NOT IS_PED_DEAD_OR_DYING(LocalPlayerPed)
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				VEHICLE_INDEX viPlayerVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
				IF GET_ENTITY_MODEL(viPlayerVeh) = BUZZARD
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_NEXT_WEAPON)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_PREV_WEAPON)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_UD)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_LR)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
		IF iTeam < FMMC_MAX_TEAMS
			INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
			
			IF iRule < FMMC_MAX_RULES
		
				IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sDrainStruct[iRule].iMissedShotPerc > 0
					
					IF NOT IS_PED_INJURED(localPlayerPed)
						WEAPON_TYPE wtWeap
						IF GET_CURRENT_PED_WEAPON(localPlayerPed, wtWeap)
							
							IF HAS_PLAYER_DAMAGED_AT_LEAST_ONE_PED(LocalPlayer)
								RESET_NET_TIMER(td_MissedShotTakeDamageTimer)
								CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(LocalPlayer)
								PRINTLN("[LM][iMissedShotPerc] Hit someone, reset timer (2)")
							ENDIF
							
							IF HAS_NET_TIMER_STARTED(td_MissedShotTakeDamageTimer)
							AND HAS_NET_TIMER_EXPIRED_READ_ONLY(td_MissedShotTakeDamageTimer, (ROUND(GET_WEAPON_TIME_BETWEEN_SHOTS(wtWeap)*1000)))
								
								INT iHealth = GET_ENTITY_HEALTH(localPlayerPed)
								INT iNewHealth = ROUND((TO_FLOAT((GET_PED_MAX_HEALTH(localPlayerPed)) / 100 ) * g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sDrainStruct[iRule].iMissedShotPerc))
								
								PRINTLN("[LM][iMissedShotPerc] Timer Expired: iHealth = ", iHealth, " iNewHealth = ", iNewHealth, " EndHealth = ", (iHealth - iNewHealth))

								iHealth -= iNewHealth
								
								SET_ENTITY_HEALTH(localPlayerPed, iHealth)
								
								RESET_NET_TIMER(td_MissedShotTakeDamageTimer)
							ENDIF
							
							IF IS_PED_SHOOTING(localPlayerPed)
								IF NOT HAS_NET_TIMER_STARTED(td_MissedShotTakeDamageTimer)
									START_NET_TIMER(td_MissedShotTakeDamageTimer)
									CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(LocalPlayer)
									PRINTLN("[LM][iMissedShotPerc] Fired Weapon: GET_WEAPON_TIME_BETWEEN_SHOTS(wtWeap) = ", (GET_WEAPON_TIME_BETWEEN_SHOTS(wtWeap)*1000))								
								ENDIF
							ENDIF							
						ENDIF
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
		
	/*
	// Maintains a Bitset so that we know if we are in the cinematic camera while spectating.
	IF NOT IS_PED_INJURED(localPlayerPed)
	AND IS_LOCAL_PLAYER_ANY_SPECTATOR()
		IF IS_FOLLOW_PED_CAM_ACTIVE()
			IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_CINEMATIC
				//SET_FOLLOW_PED_CAM_VIEW_MODE(CAM_VIEW_MODE_CINEMATIC)
				IF NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_WAS_USING_SPECTATOR_CINEMATIC_CAM)
					SET_BIT(iLocalBoolCheck23, LBOOL23_WAS_USING_SPECTATOR_CINEMATIC_CAM)
					PRINTLN("[LM][PROCESS_EVERY_FRAME_PLAYER_CHECKS] - Caching that the player is using Cinematic Cam.")
				ENDIF
			ELSE
				IF IS_BIT_SET(iLocalBoolCheck23, LBOOL23_WAS_USING_SPECTATOR_CINEMATIC_CAM)
					CLEAR_BIT(iLocalBoolCheck23, LBOOL23_WAS_USING_SPECTATOR_CINEMATIC_CAM)
					PRINTLN("[LM][PROCESS_EVERY_FRAME_PLAYER_CHECKS] - Caching that the player is No Longer using Cinematic Cam.")
				ENDIF
			ENDIF
		ELIF IS_FOLLOW_VEHICLE_CAM_ACTIVE()
			IF GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() = CAM_VIEW_MODE_CINEMATIC
				IF NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_WAS_USING_SPECTATOR_CINEMATIC_CAM)
					SET_BIT(iLocalBoolCheck23, LBOOL23_WAS_USING_SPECTATOR_CINEMATIC_CAM)
					PRINTLN("[LM][PROCESS_EVERY_FRAME_PLAYER_CHECKS] - Caching that the player is using Cinematic Cam.")					
				ENDIF
			ELSE
				IF IS_BIT_SET(iLocalBoolCheck23, LBOOL23_WAS_USING_SPECTATOR_CINEMATIC_CAM)
					CLEAR_BIT(iLocalBoolCheck23, LBOOL23_WAS_USING_SPECTATOR_CINEMATIC_CAM)
					PRINTLN("[LM][PROCESS_EVERY_FRAME_PLAYER_CHECKS] - Caching that the player is No Longer using Cinematic Cam.")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	*/
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE8_SHOW_YACHT_BLIP_THIS_RULE)
						
			// via SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER
			YACHT_DATA YachtData    
			YachtData.iLocation = 34            // Paleto Bay
			YachtData.Appearance.iOption = 2    // The Aquaris
			YachtData.Appearance.iTint = 12     // Vintage
			YachtData.Appearance.iLighting = 5 // Blue Vivacious
			YachtData.Appearance.iRailing = 1   // Gold Fittings
			YachtData.Appearance.iFlag = -1     // no flag 
			UPDATE_YACHT_FOR_MISSION_CREATOR(YachtData)			
		ENDIF
	ENDIF
	
	// Remove the particle effect if it exists and we've become a spectator.
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_LoopedGunsmithLastKill)
	AND IS_LOCAL_PLAYER_ANY_SPECTATOR()
		REMOVE_PARTICLE_FX(ptfx_LoopedGunsmithLastKill)
		PRINTLN("[LM][MAINTAIN_GUNSMITH_LAST_KILL_PARTICLES] - Removing Particle Effect.")
		PRINTLN("[LM][MAINTAIN_GUNSMITH_LAST_KILL_PARTICLES] - ... Because we are a spectator.")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciENABLE_TURF_WAR_CLAIMABLE_PROPS)	
		INT iTeamProp = 0
		FOR iTeamProp = 0 TO FMMC_MAX_TEAMS-1
			IF bIsLocalPlayerHost
				IF MC_ServerBD_4.iPropsAmountOwnedByTeam[iTeamProp] > 0
					IF NOT IS_TEAM_ACTIVE(iTeamProp)
						PRINTLN("[LM][RESET_THIS_TEAMS_PROPS] - This team is not active: ", iTeamProp, " but they have props claimed: ", MC_ServerBD_4.iPropsAmountOwnedByTeam[iTeamProp])
						IF WAS_TEAM_EVER_ACTIVE(iTeamProp)
						AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
							RESET_THIS_TEAMS_PROPS(iTeamProp)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR

		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]		
		
		IF iRule < FMMC_MAX_RULES
			PROCESS_TURF_WAR(iTeam, iRule)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET( iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iPartToUse].iteam)
		IF IS_BIT_SET(iLocalBoolCheck24, LBOOL24_BLOCK_VEHICLE_KERS_ON_RULE_CHECK)
			CLEAR_BIT(iLocalBoolCheck24, LBOOL24_BLOCK_VEHICLE_KERS_ON_RULE_CHECK)
			PRINTLN("[RCC MISSION] Clearing Bit: LBOOL24_BLOCK_VEHICLE_KERS_ON_RULE_CHECK")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciOVERTIME_PROCESS_PROP_TARGETING)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciENABLE_TURF_WAR_CLAIMABLE_PROPS)
		SELECT_PROP_TO_TARGET()
//		PROCESS_PROP_FRICTION()
	ENDIF
	
	IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()	
		PROCESS_RUINER_PARACHUTE()
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
		PROCESS_OVERTIME_EVERY_FRAME()
	ENDIF
	
	PROCESS_WEAPON_RESPAWN_AND_COLLECTION()
	
	// Allows the players vehicle to keep moving when they disconnect the controller for example.	
	IF MC_PlayerBD[iLocalPart].iTeam > -1
	AND NOT IS_PLAYER_SPECTATING(LocalPlayer)
		IF MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iTeam] < FMMC_MAX_RULES
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
			OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iTeam].iRuleBitsetEleven[MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iTeam]], ciBS_RULE11_ALLOW_PLAYER_VEH_MOVEMENT_WITHOUT_CTRL)
				IF NOT IS_PED_INJURED(localPlayerPed)
					IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
						IF NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_VEHICLE_ALLOW_MOVEMENT_WHILE_INACTIVE)
							VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(localPlayerPed)
							IF NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(vehPlayer)
							AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(VEH_TO_NET(vehPlayer))
								IF IS_VEHICLE_DRIVEABLE(vehPlayer)
									SET_VEHICLE_LIMIT_SPEED_WHEN_PLAYER_INACTIVE(vehPlayer, FALSE)
									SET_VEHICLE_STOP_INSTANTLY_WHEN_PLAYER_INACTIVE(vehPlayer, FALSE)
									PRINTLN("[LM] SET_VEHICLE_STOP_INSTANTLY_WHEN_PLAYER_INACTIVE / SET_VEHICLE_LIMIT_SPEED_WHEN_PLAYER_INACTIVE - Setting Bit.")
									SET_BIT(iLocalBoolCheck24, LBOOL24_VEHICLE_ALLOW_MOVEMENT_WHILE_INACTIVE)
								ENDIF	
							ENDIF
						ENDIF
					ELSE
						PRINTLN("[LM] SET_VEHICLE_STOP_INSTANTLY_WHEN_PLAYER_INACTIVE / SET_VEHICLE_LIMIT_SPEED_WHEN_PLAYER_INACTIVE - Clearing Bit (1).")
						CLEAR_BIT(iLocalBoolCheck24, LBOOL24_VEHICLE_ALLOW_MOVEMENT_WHILE_INACTIVE)
					ENDIF
				ELSE
					PRINTLN("[LM] SET_VEHICLE_STOP_INSTANTLY_WHEN_PLAYER_INACTIVE / SET_VEHICLE_LIMIT_SPEED_WHEN_PLAYER_INACTIVE - Clearing Bit (2).")
					CLEAR_BIT(iLocalBoolCheck24, LBOOL24_VEHICLE_ALLOW_MOVEMENT_WHILE_INACTIVE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// To cache the rule a player is on, and what rule they end on.
	IF bIsLocalPlayerHost
		INT iTeamCount
		FOR iTeamCount = 0 TO FMMC_MAX_TEAMS-1
			IF IS_BIT_SET(iLocalBoolCheck6, (LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeamCount))
			AND NOT HAS_MULTI_RULE_TIMER_EXPIRED(iTeamCount)
			AND MC_serverBD_4.iCurrentHighestPriority[iTeamCount] < FMMC_MAX_RULES
				PRINTLN("[LM] - New Rule, Cache it. ")
				PRINTLN("[LM] - iRule: ", MC_serverBD_4.iCurrentHighestPriority[iTeamCount])
				PRINTLN("[LM] - Team: ", iTeamCount)

				MC_ServerBD_4.iLastHighestPriority[iTeamCount] = MC_serverBD_4.iCurrentHighestPriority[iTeamCount]
			ENDIF
		ENDFOR
	ENDIF
	
	// Cache the starting point.
	INT iTeamBS = MC_playerBD[iPartToUse].iteam
	INT iRuleBS = MC_serverBD_4.iCurrentHighestPriority[iTeamBS]
	
	IF iRuleBS < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeamBS].iRuleBitsetEight[iRuleBS], ciBS_RULE8_GPS_TRACKING_DISTANCE_METER)
			IF IS_BIT_SET(iLocalBoolCheck6, (LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_PlayerBD[iPartToUse].iteam))
			
			//end rule = current rule
				PRINTLN("[LM][MAINTAIN_GPS_DISTANCE_METER_BAR] - New Rule.")
				INT i = 0
				FOR i = 0 TO ci_TOTAL_DISTANCE_CHECK_BARS-1
					iGPSTrackingMeterPedIndex[i] = -1
					vGPSTrackingMeterStart[i] = <<0.0, 0.0, 0.0>>
					vGPSTrackingMeterEnd[i] = <<0.0, 0.0, 0.0>>
					iGPSTrackingMeterDistCache[i] = 0
					iGPSTrackingMeterRegisteredPedBitset[0] = 0
					iGPSTrackingMeterRegisteredPedBitset[1] = 0
				ENDFOR
				
				RESET_NET_TIMER(tdWaitForPedRuleSpawn)
				START_NET_TIMER(tdWaitForPedRuleSpawn)
			ENDIF
			IF HAS_NET_TIMER_STARTED(tdWaitForPedRuleSpawn)
				IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdWaitForPedRuleSpawn, ci_WAIT_FOR_PED_RULE_SPAWN)
					RESET_NET_TIMER(tdWaitForPedRuleSpawn)
					SET_BIT(iLocalBoolCheck19, LBOOL19_CACHE_PLAYER_POS_FOR_GPS_TRACKING)
				ENDIF
			ENDIF
			IF IS_BIT_SET(iLocalBoolCheck19, LBOOL19_CACHE_PLAYER_POS_FOR_GPS_TRACKING)
				INT i = 0
				INT ii = 0
				FOR i = 0 TO (MC_serverBD.iNumPedCreated-1)
//					PRINTLN("[LM][MAINTAIN_GPS_DISTANCE_METER_BAR] - Checking iPed: ", i)
					
					INT iIndex = GET_FREE_INDEX_FOR_DISTANCE_TRACKING_BAR()
					
//					PRINTLN("[LM][MAINTAIN_GPS_DISTANCE_METER_BAR] - I index we will assign to: ", iIndex)
					
					IF iIndex > -1
						IF DOES_PED_REQUIRE_DISTANCE_TRACKING(i, iRuleBS)
							PRINTLN("[LM][MAINTAIN_GPS_DISTANCE_METER_BAR] - iPed: ", i, " Is involved with this teams Rule.")
							
							IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[i])
								IF NOT IS_BIT_SET(iGPSTrackingMeterRegisteredPedBitset[i / 32], i % 32)
									PRINTLN("[LM][MAINTAIN_GPS_DISTANCE_METER_BAR] - iPed: ", i, " exists, let's check his go-tos")
									
									BOOL bAssignedCheck = FALSE
									// We want the last valid item.
									FOR ii = 0 TO MAX_ASSOCIATED_GOTO_TASKS-1									
										vGPSTrackingMeterEnd[iIndex] = GET_ASSOCIATED_GOTO_TASK_DATA__POSITION(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sAssociatedGotoTaskData, MAX_ASSOCIATED_GOTO_TASKS-(ii+1))
										
										IF NOT IS_VECTOR_ZERO(vGPSTrackingMeterEnd[iIndex])									
											iGPSTrackingMeterPedIndex[iIndex] = i
											iGPSTrackingMeterDistCache[iIndex] = ROUND(VDIST(GET_ENTITY_COORDS(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[i])), vGPSTrackingMeterEnd[iIndex]))
											vGPSTrackingMeterStart[iIndex] = g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].vPos
											PRINTLN("[LM][MAINTAIN_GPS_DISTANCE_METER_BAR] - Assigned final Loc - Breaking Loop.")
											bAssignedCheck = TRUE
											BREAKLOOP
										ENDIF
									ENDFOR
									
									IF NOT bAssignedCheck
										PRINTLN("[LM][MAINTAIN_GPS_DISTANCE_METER_BAR] - Nothing was assigned here...")
									ENDIF
									
									SET_BIT(iGPSTrackingMeterRegisteredPedBitset[i / 32], i % 32)
								ENDIF
							ELSE
								
								PRINTLN("[LM][MAINTAIN_GPS_DISTANCE_METER_BAR] - iPed: ", i, " Is dead or does not exist.")
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
				
//				FOR i = 0 TO ci_TOTAL_DISTANCE_CHECK_BARS-1
//					PRINTLN("[LM][MAINTAIN_GPS_DISTANCE_METER_BAR] - Readout - iPed: 			", iGPSTrackingMeterPedIndex[i])
//					PRINTLN("[LM][MAINTAIN_GPS_DISTANCE_METER_BAR] - Readout - vLoc Start:	", vGPSTrackingMeterStart[i])
//					PRINTLN("[LM][MAINTAIN_GPS_DISTANCE_METER_BAR] - Readout - vLoc End: 		", vGPSTrackingMeterEnd[i])
//				ENDFOR
				
				//CLEAR_BIT(iLocalBoolCheck19, LBOOL19_CACHE_PLAYER_POS_FOR_GPS_TRACKING)		
//				PRINTLN("[LM][MAINTAIN_GPS_DISTANCE_METER_BAR] - ~Assigned Successfully")	
			ENDIF
		ENDIF
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitSet3, ciBS3_ENABLE_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN)
		IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)		
			IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_INITIALIZED_SEATING_DATA_FOR_MULTI_VEH)
				IF MC_playerBD[iLocalPart].iGameState = GAME_STATE_RUNNING
					PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	PROCESS_CAMERA_SHAKE_ON_RULE()
	
	// Play Audio Scenes for Special Vehicle Mission Vehicles.
	IF SVM_FLOW_IS_CURRENT_MISSION_SVM_MISSION()	
		BOOL bCheckToClear = FALSE
		
		IF NOT IS_PED_INJURED(localPlayerPed)
			IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
				VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(localPlayerPed)
				
				IF DOES_ENTITY_EXIST(vehPlayer)
				AND IS_VEHICLE_DRIVEABLE(vehPlayer)
					MODEL_NAMES vehModel = GET_ENTITY_MODEL(vehPlayer)					
					STRING sAudScene = GET_SVM_AUDIO_SCENE_FROM_MODEL_NAME(vehModel)
					IF NOT ARE_STRINGS_EQUAL(sAudScene, "")
						iLocalAudioSceneSVM = GET_SVM_INT_FROM_AUDIO_SCENE_FROM_MODEL_NAME(vehModel)
						IF iLocalAudioSceneSVM > -1
							IF NOT IS_AUDIO_SCENE_ACTIVE(sAudScene)							
								PRINTLN("[LM][SVM_AUDIO_SCENE] - Starting Scene: ", sAudScene, " Saving Index: ",  iLocalAudioSceneSVM)
								START_AUDIO_SCENE(sAudScene)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					bCheckToClear = TRUE
				ENDIF	
			ELSE
				bCheckToClear = TRUE
			ENDIF
		ELSE
			bCheckToClear = TRUE
		ENDIF
		
		IF bCheckToClear
			STRING sAudScene = GET_SVM_AUDIO_SCENE_FROM_CACHE_INT(iLocalAudioSceneSVM)
			IF NOT ARE_STRINGS_EQUAL(sAudScene, "")
				IF iLocalAudioSceneSVM > -1
					IF IS_AUDIO_SCENE_ACTIVE(sAudScene)
						PRINTLN("[LM][SVM_AUDIO_SCENE] - Stopping Scene: ", sAudScene, " From Index: ",  iLocalAudioSceneSVM)
						STOP_AUDIO_SCENE(sAudScene)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	CHECK_IF_NEW_RESPAWN_POINTS_SHOULD_BE_ADDED()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciENABLE_NEW_SLIPSTREAM_FX)
		OVERRIDE_SLIPSTREAM_VFX()
	ENDIF		
	
	//Reduce the bloom on pickups if the neon filter is on
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA(g_FMMC_STRUCT.iAdversaryModeType)
		IF ANIMPOSTFX_IS_RUNNING("DeadlineNeon")
			SET_LIGHT_OVERRIDE_MAX_INTENSITY_SCALE(fBloomScale)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciHUD_HIDE_RP_BAR)
		HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
	ENDIF
	
	PROCESS_VEHICLE_SLIPSTREAMING()
		
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam] < FMMC_MAX_RULES	
		MAINTAIN_STAMINA_BAR()
		
			IF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) != CLIENT_MISSION_STAGE_HACK_SYNC_LOCK
				IF HAS_THIS_ADDITIONAL_TEXT_LOADED("MC_PLAY", MISSION_TEXT_SLOT)
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HPSLOCK")
					OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HQPSLOCK")
						PRINTLN("[RCC MISSION] PROCESS_EVERY_FRAME_PLAYER_CHECKS - RESET_SYNC_LOCK - HELP TEXT CLEARED")
						CLEAR_HELP(TRUE)
					ENDIF
				ENDIF
			ENDIF
		
		MAINTAIN_FORCED_CAMERA(g_FMMC_STRUCT.iFixedCamera)
		
		SET_TIME_BASED_ON_RULE()
		
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_LOST_DAMNED(g_FMMC_STRUCT.iAdversaryModeType)
		AND NOT IS_BIT_SET(iLocalBoolCheck18, LBOOL18_TLAD_SD_UI_CLEARED)
			IF bPlayerToUseOK
				IF NOT SHOULD_HIDE_THE_HUD_THIS_FRAME()
					IF GET_SKYSWOOP_STAGE() >= SKYSWOOP_GOINGUP
						IF HAS_SCALEFORM_MOVIE_LOADED(SF_TLAD_MovieIndex)
							IF bLostAndDamnedScaleformActive
								IF NOT IS_BIT_SET(iLocalBoolCheck18, LBOOL18_TLAD_HUD_HAS_BEEN_CLEARED_UP)
									BEGIN_SCALEFORM_MOVIE_METHOD (SF_TLAD_MovieIndex, "SET_MESSAGE_VISIBILITY")
										SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
									END_SCALEFORM_MOVIE_METHOD()
									SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(SF_TLAD_MovieIndex)
									SET_BIT(iLocalBoolCheck18, LBOOL18_TLAD_HUD_HAS_BEEN_CLEARED_UP)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						MANAGE_LOST_AND_DAMNED_UI()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciHWN_HEARTBEAT_VIBRATE_ENABLED)
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciHWN_ENABLE_HEARTBEAT_BOTH_TEAMS)
				private_FIND_OTHER_TEAM_FOR_HEARTBEAT()
			ENDIF
		ENDIF
		
		MANAGE_TRACKIFY_LOGIC(IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitset[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE_ENABLETRACKIFY))
		
		//option is on in all Dawn Raid variations
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(g_FMMC_STRUCT.iAdversaryModeType)
			HANDLE_CONTAINER_MINIGAME_EXTERNAL_LOGIC()
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitSetFive[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE5_BEASTMODE_ENABLE_TRACKIFY_ON_BEAST)
			PROCESS_TRACKIFY_FOR_BEASTS()
		ELSE
			//PROCESS_TRACKIFY_FOR_BEASTS(FALSE)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciBIKES_DONT_WIPE_OUT)
		OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciDISABLE_BIKE_COMBAT)
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, TRUE)
				IF IS_PED_ON_ANY_BIKE(LocalPlayerPed)
					VEHICLE_INDEX vIndex = GET_VEHICLE_PED_IS_IN(LocalPlayerPed, TRUE)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(vIndex)
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciBIKES_DONT_WIPE_OUT)
							SET_BIKE_EASY_TO_LAND(vIndex, TRUE)
							PRINTLN("[RCC MISSION] - SET_BIKE_EASY_TO_LAND VEHICLE FOR CURRENT BIKE")
						ENDIF
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciDISABLE_BIKE_COMBAT)
							SET_PEDS_CAN_FALL_OFF_THIS_VEHICLE_FROM_LARGE_FALL_DAMAGE(vIndex,TRUE,250)
							PRINTLN("[RCC MISSION] - SET_PEDS_CAN_FALL_OFF_THIS_VEHICLE_FROM_LARGE_FALL_DAMAGE VEHICLE FOR CURRENT BIKE")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
			IF MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam] < FMMC_MAX_RULES

				IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_REQUEST_AVENGER_HOLD_TRANSITION_BLOCK)
					
					BOOL bUnblock
					INT iCurrentStage = GET_MC_CLIENT_MISSION_STAGE(iLocalPart)
					
					// can unblock for definite now
					IF iCurrentStage != CLIENT_MISSION_STAGE_SCRIPTED_CUTSCENE
					AND iCurrentStage != CLIENT_MISSION_STAGE_DELIVER_VEH
					
						PRINTLN("[RCC MISSION] - PROCESS_EVERY_FRAME_PLAYER_CHECKS - REQUEST_AVENGER_HOLD_TRANSITION_BLOCKED - iCurrentStage: ", GET_CLIENT_STATE_STRING(iCurrentStage), " - UNBLOCK IMMEDIATELY")
						bUnblock = TRUE
					
					// On deliver vehicle, and not in drop off/dropping off, start a timer
					ELIF iCurrentStage = CLIENT_MISSION_STAGE_DELIVER_VEH
					AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
					AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_DROPPING_OFF)
					
						IF NOT HAS_NET_TIMER_STARTED(tdAvengerHoldUnlockTimer)
							
							PRINTLN("[RCC MISSION] - PROCESS_EVERY_FRAME_PLAYER_CHECKS - REQUEST_AVENGER_HOLD_TRANSITION_BLOCKED - iCurrentStage: ", GET_CLIENT_STATE_STRING(iCurrentStage), " - START UNBLOCK TIMER")
							REINIT_NET_TIMER(tdAvengerHoldUnlockTimer)
							
						ELIF HAS_NET_TIMER_EXPIRED(tdAvengerHoldUnlockTimer, 2000)
							
							PRINTLN("[RCC MISSION] - PROCESS_EVERY_FRAME_PLAYER_CHECKS - REQUEST_AVENGER_HOLD_TRANSITION_BLOCKED - iCurrentStage: ", GET_CLIENT_STATE_STRING(iCurrentStage), " - UNBLOCK TIMER EXPIRED")
							bUnblock = TRUE
							
						ENDIF
					
					// needs to remain blocked
					ELSE
						IF HAS_NET_TIMER_STARTED(tdAvengerHoldUnlockTimer)
							PRINTLN("[RCC MISSION] - PROCESS_EVERY_FRAME_PLAYER_CHECKS - REQUEST_AVENGER_HOLD_TRANSITION_BLOCKED - iCurrentStage: ", GET_CLIENT_STATE_STRING(iCurrentStage), " - REMAIN BLOCKED")
							RESET_NET_TIMER(tdAvengerHoldUnlockTimer)
						ENDIF
					ENDIF

					IF bUnblock	
						PRINTLN("[RCC MISSION] - PROCESS_EVERY_FRAME_PLAYER_CHECKS - REQUEST_AVENGER_HOLD_TRANSITION_BLOCKED - UNBLOCKING")
						REQUEST_AVENGER_HOLD_TRANSITION_BLOCKED(FALSE)
						RESET_NET_TIMER(tdAvengerHoldUnlockTimer)
					ENDIF
					
				ENDIF

				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iRuleBitsetTen[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam]], ciBS_RULE10_PROGRESS_RULE_ALL_DRIVER_ACCEL)
				AND GET_SEAT_PED_IS_IN(localPlayerPed) = VS_DRIVER
				
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE, TRUE)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_UP, TRUE)
					
					// Print Help
					IF NOT AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH()
					AND IS_AVENGER_HOLD_TRANSITION_BLOCKED_AND_INACTIVE(MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam], MC_playerBD[iLocalPart].iteam)
					AND (NOT IS_BIT_SET(iLocalBoolCheck,LBOOL_ON_LAST_LIFE)
					OR NOT DOES_TEAM_HAVE_ANY_PLAYERS_DEAD(MC_playerBD[iLocalPart].iteam))
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HLP_PRS_R2")
							PRINT_HELP_FOREVER("HLP_PRS_R2")
						ENDIF
					ELSE
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HLP_PRS_R2")
							CLEAR_HELP(TRUE)
						ENDIF
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HLP_PRS_R2")
						CLEAR_HELP(TRUE)
					ENDIF
				ENDIF
				
				
			ENDIF
		ENDIF	
		
		
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciALLOW_MISSION_AIRSTRIKES)

			INT iRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
			
			IF iRule < FMMC_MAX_RULES
				IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iSafetyRange > 0
				AND NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_AIRSTRIKE_SPEED_BAR_FIRED)
					IF IS_BIT_SET(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_IN_PROGRESS)
						FLOAT fDist = VDIST2(GET_PLAYER_COORDS(LocalPlayer), g_vAirstrikeCoords)
						IF fDist < POW(TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iSafetyRange), 2)
						AND NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_SAFETY_RANGE_STOP)
							SET_BIT(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_SAFETY_RANGE_STOP)
							SET_BIT(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_FINAL_SHOT_FIRED)
							PRINTLN("[RCC MISSION][AIRSTRIKE] - SETTING LBOOL24_AIRSTRIKE_SAFETY_RANGE_STOP")
							REINIT_NET_TIMER(tdAirstrikeSafetyTimer)
							REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("w_lr_rpg_rocket")))	
						ENDIF
						
						IF IS_BIT_SET(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_DIALOGUE_TRIGGERED)
							fDist = VDIST2(GET_PLAYER_COORDS(LocalPlayer), vDialogueAirstrikeCoords)
							IF fDist < POW(TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iSafetyRange), 2)
							AND NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_SAFETY_RANGE_STOP)
								SET_BIT(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_SAFETY_RANGE_STOP)
								SET_BIT(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_FINAL_SHOT_FIRED)
								PRINTLN("[RCC MISSION][AIRSTRIKE] - SETTING LBOOL24_AIRSTRIKE_SAFETY_RANGE_STOP")
								REINIT_NET_TIMER(tdAirstrikeSafetyTimer)
								REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("w_lr_rpg_rocket")))	
							ENDIF
						ENDIF
					ENDIF

				ENDIF
							
				IF HAS_NET_TIMER_STARTED(tdAirstrikeSafetyTimer)
					IF HAS_NET_TIMER_EXPIRED(tdAirstrikeSafetyTimer, ci_iAirstrikeStartDelay)
						PRINTLN("[RCC MISSION][AIRSTRIKE] - SETTING LBOOL24_AIRSTRIKE_FINAL_SHOT_FIRED - SAFETY RANGE TRIGGERED")
						CLEAR_BIT(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_SAFETY_RANGE_STOP)
						RESET_NET_TIMER(tdAirstrikeSafetyTimer)
					ENDIF
				ENDIF
				
				//Dialogue trigger airstrikes
				IF IS_BIT_SET(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_DIALOGUE_TRIGGERED)
				AND NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_IN_PROGRESS)
					IF NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_SPEED_BAR_AIRSTRIKE_CALLED)
					AND NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_SAFETY_RANGE_STOP)					
						IF NOT IS_BIT_SET(iLocalBoolCheck25, LBOOL25_ORBITAL_CANNON_DIALOGUE_AIRSTRIKE)
							PRINTLN("[RCC MISSION][AIRSTRIKE][CALL] Calling DO_SCRIPTED_AIRSTRIKE from: STARTING DIALOGUE")
							DO_SCRIPTED_AIRSTRIKE(vDialogueAirstrikeCoords, 10)
						ELSE
							PRINTLN("[RCC MISSION][AIRSTRIKE][CALL] Calling FIRE_ORBITAL_CANNON from: STARTING DIALOGUE")
							FIRE_ORBITAL_CANNON(vDialogueAirstrikeCoords)
						ENDIF
					ENDIF
				ENDIF
				
				IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iEntityType > -1				
					
					IF NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_IN_PROGRESS)
										
						IF IS_BIT_SET(iLocalBoolCheck23, LBOOL23_AIRSTRIKE_FIFTY_PERCENT_STRIKE)
						AND NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_SPEED_BAR_AIRSTRIKE_CALLED)
						AND NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_SAFETY_RANGE_STOP)
							IF HAS_NET_TIMER_STARTED(tdAirstrikeStartTimer)
								IF HAS_NET_TIMER_EXPIRED(tdAirstrikeStartTimer, ci_iAirstrikeStartDelay)
									PRINTLN("[RCC MISSION][AIRSTRIKE] ACTIVATE_AIRSTRIKE_AND_SET_COORDS - PERCENTAGE LAUNCH")
									UPDATE_AIRSTRIKE_OFFSET_VECTOR(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iEntityType, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iEntityID, iTeam, iRule)
									
									PRINTLN("[RCC MISSION][AIRSTRIKE][OFFSET] Getting offset from GET_RANDOM_OFFSET_ON_RING - vAirstrikeOffset: ", vAirstrikeOffset)
									ACTIVATE_AIRSTRIKE_AND_SET_COORDS(vAirstrikeOffset.x, vAirstrikeOffset.y)								
								ENDIF
							ELSE
								REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("w_lr_rpg_rocket")))	
								START_NET_TIMER(tdAirstrikeStartTimer)
							ENDIF
						ENDIF				
						
					ENDIF				
					
					IF bAirstrikeSpheres
						VECTOR tempTargetCoords

						GET_AIRSTRIKE_TARGET_COORDS(tempTargetCoords, MC_playerBD[iPartToUse].iTeam, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam])
						//tempTargetCoords = GET_ENTITY_COORDS(LocalPlayerPed)
						IF bAirstrikeSphereMax
							DRAW_DEBUG_SPHERE(tempTargetCoords, fRAGAirstrikeMax,255, 170, 0, 175)
						ELIF bAirstrikeSphereMin
							DRAW_DEBUG_SPHERE(tempTargetCoords, fRAGAirstrikeMin,255, 170, 0, 175)
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_IN_PROGRESS)
					#IF IS_DEBUG_BUILD
						DRAW_DEBUG_SPHERE(g_vAirstrikeCoords, 10, 255, 0 ,0 ,150)
						DRAW_DEBUG_LINE(GET_ENTITY_COORDS(LocalPlayerPed), g_vAirstrikeCoords, 255, 0 ,0)
					#ENDIF
					IF IS_BIT_SET(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_FINAL_SHOT_FIRED)
						PRINTLN("[RCC MISSION][AIRSTRIKE] - Clearing bits and vector")
						CLEAR_BIT(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_IN_PROGRESS)
						CLEAR_BIT(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_SAFETY_RANGE_STOP)
						CLEAR_BIT(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_DIALOGUE_TRIGGERED)
						vAirstrikeOffset = <<0,0,0>>
						iAirstrikeExplosionCount = 0
						iAirstrikeRocketFiredBitSet = 0
						RESET_NET_TIMER(tdAirstrikeStartTimer)
						RESET_NET_TIMER(tdAirstrikeSafetyTimer)
						STOP_SOUND(iSoundIDVehBeacon)
					ELSE
						IF NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_SAFETY_RANGE_STOP)
						AND NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_AIRSTRIKE_SPEED_BAR_FIRED)
							IF NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_DIALOGUE_TRIGGERED)							
								IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iAirstrikeType = ciAIRSTRIKE_TYPE_MISSILES
									PRINTLN("[RCC MISSION][AIRSTRIKE][CALL] Calling DO_SCRIPTED_AIRSTRIKE from: Airstrike in progress, NOT DIALOGUE")
									DO_SCRIPTED_AIRSTRIKE(g_vAirstrikeCoords, 10)
								ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iAirstrikeType = ciAIRSTRIKE_TYPE_ORBITAL_CANNON
									PRINTLN("[RCC MISSION][AIRSTRIKE][CALL] Calling FIRE_ORBITAL_CANNON from: Airstrike in progress, NOT DIALOGUE")
									FIRE_ORBITAL_CANNON(g_vAirstrikeCoords)
								ENDIF
							ELSE
								IF NOT IS_BIT_SET(iLocalBoolCheck25, LBOOL25_ORBITAL_CANNON_DIALOGUE_AIRSTRIKE)
									PRINTLN("[RCC MISSION][AIRSTRIKE][CALL] Calling DO_SCRIPTED_AIRSTRIKE from: Airstrike in progress, DIALOGUE")
									DO_SCRIPTED_AIRSTRIKE(vDialogueAirstrikeCoords, 10)
								ELSE
									PRINTLN("[RCC MISSION][AIRSTRIKE][CALL] Calling FIRE_ORBITAL_CANNON from: Airstrike in progress, NOT DIALOGUE")
									FIRE_ORBITAL_CANNON(vDialogueAirstrikeCoords)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iPartToUse].iTeam)
			
			BLOCK_PLAYER_HEALTH_REGEN(IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE8_BLOCK_HEALTH_REGEN))
			
			SET_MINIGUN_PLAYER_DAMAGE(TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iMinigunDamage[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]])/100)	
			
			IGNORE_NO_GPS_FLAG(IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE8_SET_IGNORE_NO_GPS))
			
			REMOVE_REBREATHER_ON_RULE(MC_playerBD[iPartToUse].iTeam, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam])
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciALLOW_MISSION_AIRSTRIKES)
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE9_AIRSTRIKE_ON_RULE_PROGRESSION)
					
					IF bIsLocalPlayerHost
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE9_AIRSTRIKE_SPECIFIC_COORD)
						AND NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_SAFETY_RANGE_STOP)
							PRINTLN("[RCC MISSION][AIRSTRIKE][OFFSET] Launching Airstrike at specific coord")
							ACTIVATE_AIRSTRIKE_AND_SET_COORDS(DEFAULT, DEFAULT, TRUE)
							SET_BIT(iLocalBoolCheck23, LBOOL23_AIRSTRIKE_COORDS_SET)
						ENDIF
					ENDIF
					
					
				ENDIF
				IF bUseRAGValues
					fRAGAirstrikeMin = TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].sAirstrikeStruct[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]].iOffsetDistanceMin)
					fRAGAirstrikeMax = TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].sAirstrikeStruct[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]].iOffsetDistanceMax)
					PRINTLN("[RCC MISSION][AIRSTRIKE] Min range: ", fRAGAirstrikeMin)
					PRINTLN("[RCC MISSION][AIRSTRIKE] Max Range: ", fRAGAirstrikeMax)
				ENDIF
				PRINTLN("[RCC MISSION][AIRSTRIKE] Moved onto Rule ", MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam])
				PRINTLN("[RCC MISSION][AIRSTRIKE] Target Entity Type: ", g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].sAirstrikeStruct[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]].iEntityType)
				PRINTLN("[RCC MISSION][AIRSTRIKE] Target Entity ID: ", g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].sAirstrikeStruct[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]].iEntityID)
			ENDIF
			
			IF g_FMMC_STRUCT.iPerRuleVehicleRockets[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]][MC_playerBD[iPartToUse].iTeam] != 0
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitsetSix[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE6_VEHICLE_MENU_WEAPON_ADD_WEAPON1)
					iVehicleRockets += g_FMMC_STRUCT.iPerRuleVehicleRockets[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]][MC_playerBD[iPartToUse].iTeam]
				ELSE
					iVehicleRockets = g_FMMC_STRUCT.iPerRuleVehicleRockets[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]][MC_playerBD[iPartToUse].iTeam]
				ENDIF
				SET_BIT(iLocalBoolCheck15, LBOOL15_VEHICLE_MENU_WEAPON_ACTIVE_ROCKETS)
			ENDIF
			IF g_FMMC_STRUCT.iPerRuleVehicleBoost[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]][MC_playerBD[iPartToUse].iTeam] != 0
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitsetSix[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE6_VEHICLE_MENU_WEAPON_ADD_WEAPON2)
					iVehicleBoost += g_FMMC_STRUCT.iPerRuleVehicleBoost[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]][MC_playerBD[iPartToUse].iTeam]
				ELSE
					iVehicleBoost = g_FMMC_STRUCT.iPerRuleVehicleBoost[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]][MC_playerBD[iPartToUse].iTeam]
				ENDIF
				SET_BIT(iLocalBoolCheck15, LBOOL15_VEHICLE_MENU_WEAPON_ACTIVE_BOOST)
			ENDIF
			IF g_FMMC_STRUCT.iPerRuleVehicleRepair[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]][MC_playerBD[iPartToUse].iTeam] != 0
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitsetSix[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE6_VEHICLE_MENU_WEAPON_ADD_WEAPON3)
					iVehicleRepair += g_FMMC_STRUCT.iPerRuleVehicleRepair[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]][MC_playerBD[iPartToUse].iTeam]
				ELSE
					iVehicleRepair = g_FMMC_STRUCT.iPerRuleVehicleRepair[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]][MC_playerBD[iPartToUse].iTeam]
				ENDIF
				SET_BIT(iLocalBoolCheck15, LBOOL15_VEHICLE_MENU_WEAPON_ACTIVE_REPAIR)
			ENDIF
			IF g_FMMC_STRUCT.iPerRuleVehicleSpikes[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]][MC_playerBD[iPartToUse].iTeam] != 0
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitsetSix[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE6_VEHICLE_MENU_WEAPON_ADD_WEAPON4)
					iVehicleSpikes += g_FMMC_STRUCT.iPerRuleVehicleSpikes[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]][MC_playerBD[iPartToUse].iTeam]
				ELSE
					iVehicleSpikes = g_FMMC_STRUCT.iPerRuleVehicleSpikes[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]][MC_playerBD[iPartToUse].iTeam]
				ENDIF
				SET_BIT(iLocalBoolCheck15, LBOOL15_VEHICLE_MENU_WEAPON_ACTIVE_SPIKES)
			ENDIF
			PRINTLN("[RCC MISSION] - iVehicleRockets = ", iVehicleRockets)
			PRINTLN("[RCC MISSION] - iVehicleBoost = ", iVehicleBoost)
			PRINTLN("[RCC MISSION] - iVehicleRepair = ", iVehicleRepair)
			PRINTLN("[RCC MISSION] - iVehicleSpikes = ", iVehicleSpikes)
			
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUNNING_BACK_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
				IF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_DELIVERED_THIS_RULE)
					CLEAR_BIT(iLocalBoolCheck28, LBOOL28_DELIVERED_THIS_RULE)
					PRINTLN("[RCC MISSION] CLEARING iLocalBoolCheck28, LBOOL28_DELIVERED_THIS_RULE")
				ENDIF
			ENDIF
		ENDIF
		
		//Vehicle Rockets
		IF IS_BIT_SET(iLocalBoolCheck15, LBOOL15_VEHICLE_MENU_WEAPON_ACTIVE_ROCKETS)
			REQUEST_WEAPON_ASSET(g_wtPickupRocketType)
			
			SET_BIT(iLocalBoolCheck14, LBOOL14_VEHICLE_ROCKET_ASSET_REQUEST)
			
			IF HAS_WEAPON_ASSET_LOADED(g_wtPickupRocketType)
				IF NOT g_VehicleRocketInfo.bIsCollected
					IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(VehicleDelayTimer) > g_FMMC_STRUCT.iPerRuleVehicleDelayTimer[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]][MC_playerBD[iPartToUse].iTeam]
						IF iVehicleRockets != 0
							g_VehicleRocketInfo.bIsCollected = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				UPDATE_VEHICLE_ROCKETS()
				
				IF NOT g_VehicleRocketInfo.bIsCollected
					IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(VehicleDelayTimer) > g_FMMC_STRUCT.iPerRuleVehicleDelayTimer[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]][MC_playerBD[iPartToUse].iTeam]
						IF iVehicleRockets > 0
							iVehicleRockets--
						ENDIF
						
						REINIT_NET_TIMER(VehicleDelayTimer)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Vehicle Boost
		IF IS_BIT_SET(iLocalBoolCheck15, LBOOL15_VEHICLE_MENU_WEAPON_ACTIVE_BOOST)
			IF NOT g_VehicleBoostInfo.bCollected
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(VehicleDelayTimer) > g_FMMC_STRUCT.iPerRuleVehicleDelayTimer[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]][MC_playerBD[iPartToUse].iTeam]
					IF iVehicleBoost != 0
						g_VehicleBoostInfo.bCollected = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			UPDATE_VEHICLE_BOOSTING(DEFAULT, 
									g_FMMC_STRUCT.iPerRuleVehicleBoostDuration[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]][MC_playerBD[iPartToUse].iTeam], 
									TO_FLOAT(g_FMMC_STRUCT.iPerRuleVehicleBoostSpeed[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]][MC_playerBD[iPartToUse].iTeam]))
			
			IF NOT g_VehicleBoostInfo.bCollected
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(VehicleDelayTimer) > g_FMMC_STRUCT.iPerRuleVehicleDelayTimer[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]][MC_playerBD[iPartToUse].iTeam]
					IF iVehicleBoost > 0
						iVehicleBoost--
					ENDIF
					
					REINIT_NET_TIMER(VehicleDelayTimer)
				ENDIF
			ENDIF
		ENDIF
		
		//Vehicle Repair
		IF IS_BIT_SET(iLocalBoolCheck15, LBOOL15_VEHICLE_MENU_WEAPON_ACTIVE_REPAIR)
			iVehicleRepair = iVehicleRepair
		ENDIF
		
		//Vehicle Spikes
		IF IS_BIT_SET(iLocalBoolCheck15, LBOOL15_VEHICLE_MENU_WEAPON_ACTIVE_SPIKES)
			IF NOT g_VehicleSpikeInfo.bIsCollected
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(VehicleDelayTimer) > g_FMMC_STRUCT.iPerRuleVehicleDelayTimer[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]][MC_playerBD[iPartToUse].iTeam]
					IF iVehicleSpikes != 0
						g_VehicleSpikeInfo.bIsCollected = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			UPDATE_VEHICLE_SPIKES()
			
			IF NOT g_VehicleSpikeInfo.bIsCollected
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(VehicleDelayTimer) > g_FMMC_STRUCT.iPerRuleVehicleDelayTimer[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]][MC_playerBD[iPartToUse].iTeam]
					IF iVehicleSpikes > 0
						iVehicleSpikes--
					ENDIF
					
					REINIT_NET_TIMER(VehicleDelayTimer)
				ENDIF
			ENDIF
		ENDIF
		
		//Vehicle Combat
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitsetSeven[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE7_TURN_ON_BIKE_COMBAT)
			IF IS_ENTITY_ALIVE(LocalPlayerPed)
				IF NOT GET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DisableVehicleCombat)
					PRINTLN("[KH] PROCESS_EVERY_FRAME_PLAYER_CHECKS - Disabling Vehicle Combat for local player")
					SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DisableVehicleCombat, TRUE)
				ENDIF
			ENDIF
		ELSE
			IF IS_ENTITY_ALIVE(LocalPlayerPed)
				IF GET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DisableVehicleCombat)
					PRINTLN("[KH] PROCESS_EVERY_FRAME_PLAYER_CHECKS - Enabling Vehicle Combat for local player")
					SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DisableVehicleCombat, FALSE)
				ENDIF
			ENDIF
		ENDIF

		
		// check if the finalise head blend check needs to run
		if IS_BIT_SET(iLocalBoolCheck12,LBOOL12_FINALISE_HEADBLEND)
			if HAS_PED_HEAD_BLEND_FINISHED(LocalPlayerPed)
			and HAVE_ALL_STREAMING_REQUESTS_COMPLETED(LocalPlayerPed)
				FINALIZE_HEAD_BLEND(LocalPlayerPed)
				CLEAR_BIT(iLocalBoolCheck12,LBOOL12_FINALISE_HEADBLEND)
			ENDIF
		endif
		
		PROCESS_EVERY_FRAME_PLAYER_ACTION_MODE( FALSE )
		
		IF NOT IS_PED_INJURED(localPlayerPed)
			IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iKSPowerupActiveBS, ci_KILLSTREAK_POWERUP_FREEZE_POS)
				PROCESS_KILLSTREAK_POWERUP_FREEZE_POSITION()
			ENDIF
		ELSE
			IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iKSPowerupActiveBS, ci_KILLSTREAK_POWERUP_FREEZE_POS)
				CLEAR_BIT(MC_PlayerBD[iLocalPart].iKSPowerupActiveBS, ci_KILLSTREAK_POWERUP_FREEZE_POS)
			ENDIF
		ENDIF		
		
		IF iNumCreatedDynamicFlarePTFX > 0
			PROCESS_EVERY_FRAME_FLARE_OBJECT()
		ENDIF
		
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iPartToUse].iteam)
			PRINTLN("[RCC MISSION] LBOOL10_DROP_OFF_LOC_DISPLAY is set")
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetFour[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE4_HIDE_DEATH_TICKERS)
				IF NOT g_bNoDeathTickers
					PRINTLN("[RCC MISSION] Setting g_bNoDeathTickers = TRUE")
					g_bNoDeathTickers = TRUE
				ENDIF
			ELSE
				IF g_bNoDeathTickers
					PRINTLN("[RCC MISSION] Setting g_bNoDeathTickers = FALSE")
					g_bNoDeathTickers = FALSE
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetFour[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE4_RESPAWN_WITH_LIMITED_Z)
				SET_CAR_NODE_SEARCH_LOWER_Z_LIMIT(5.0)
				PRINTLN("[RCC MISSION][RESPAWNING] 5 limit on respawning")
			ELSE
				SET_CAR_NODE_SEARCH_LOWER_Z_LIMIT(0.0)
				PRINTLN("[RCC MISSION][RESPAWNING] 0 limit on respawning")
			ENDIF
			
			IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iMinigunDamage[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]] != 100
				PRINTLN("[RCC MISSION] PROCESS_EVERY_FRAME_PLAYER_CHECKS - Setting up Minigun defence mod - ",TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iMinigunDamage[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]])/100,"%")
				SET_MINIGUN_PLAYER_DAMAGE(TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iMinigunDamage[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]])/100)
				SET_BIT(iLocalBoolCheck26, LBOOL26_MINIGUN_DEFENCE_MODIFIED)
			ELSE
				IF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_MINIGUN_DEFENCE_MODIFIED)
					SET_MINIGUN_PLAYER_DAMAGE(TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iMinigunDamage[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]])/100)
					CLEAR_BIT(iLocalBoolCheck26, LBOOL26_MINIGUN_DEFENCE_MODIFIED)
					PRINTLN("[RCC MISSION] PROCESS_EVERY_FRAME_PLAYER_CHECKS - Resetting minigun damage mod")
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck3, LBOOL3_PHOTOTAKEN) //Put away the phone after a successful photo rule
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdPhotoTakenTimer) > 400
			CLEAR_BIT(iLocalBoolCheck3, LBOOL3_PHOTOTAKEN)
			RESET_NET_TIMER(tdPhotoTakenTimer)
			HANG_UP_AND_PUT_AWAY_PHONE(FALSE)
		ENDIF
	ENDIF
	BOOL bClearVelum2DoorOpening = TRUE
	
	MAINTAIN_PLAYER_HELMET_AND_PARACHUTE_FOR_PACIFIC_BIKES()
	
	// When in the Pacific Standard Finale, places props to fix bug 3729570 (if they don't already exist)
	IF IS_LOCAL_PLAYER_ON_PACIFIC_FINALE()
		IF NOT DOES_ENTITY_EXIST(oi_pacificStandardWallBlockers[0])
		
			PRINTLN("[TMS] Creating pacific standard wall blockers")
			oi_pacificStandardWallBlockers[0] = CREATE_OBJECT_NO_OFFSET(PROP_ELECBOX_01A, <<-1551.134, -578.333, 32.908>>, FALSE, FALSE)
			oi_pacificStandardWallBlockers[1] = CREATE_OBJECT_NO_OFFSET(PROP_ELECBOX_01A, <<-1551.134, -578.333, 35.2908>>, FALSE, FALSE)
			
			oi_pacificStandardWallBlockers[2] = CREATE_OBJECT_NO_OFFSET(PROP_ELECBOX_01A, <<-1552.1854, -578.4654, 32.9187>>, FALSE, FALSE)
			oi_pacificStandardWallBlockers[3] = CREATE_OBJECT_NO_OFFSET(PROP_ELECBOX_01A, <<-1552.1854, -578.4654, 35.29187>>, FALSE, FALSE)
			
			INT PSwallBlockerLoopVar = 0
			REPEAT ciPacificStandardWallBlockers PSwallBlockerLoopVar
				SET_ENTITY_VISIBLE(oi_pacificStandardWallBlockers[PSwallBlockerLoopVar], FALSE)
				SET_ENTITY_INVINCIBLE(oi_pacificStandardWallBlockers[PSwallBlockerLoopVar], TRUE)
				SET_ENTITY_COLLISION(oi_pacificStandardWallBlockers[PSwallBlockerLoopVar], TRUE)
				FREEZE_ENTITY_POSITION(oi_pacificStandardWallBlockers[PSwallBlockerLoopVar], TRUE)
			ENDREPEAT
		ENDIF
	ENDIF
	
	// CMcM - Fix for 2076901
	IF bLocalPlayerPedOk
			
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			
			VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
			MODEL_NAMES mnVeh = GET_ENTITY_MODEL(tempVeh)
			
			IF IS_THIS_MODEL_A_BIKE(mnVeh)
				
				IF MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] < FMMC_MAX_RULES
					IF mnVeh = OPPRESSOR
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]], ciBS_RULE9_DISABLE_OPPRESSOR_WINGS)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BIKE_WINGS)
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]], ciBS_RULE9_DISABLE_OPPRESSOR_BOOST)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ROCKET_BOOST)
						ENDIF
					ENDIF
				ENDIF
				IF IS_ENTITY_IN_WATER(tempVeh)
					IF IS_PLAYER_BLOCKED_FROM_EXITING_VEHICLE_IN_MISSION()
						ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
						SET_PLAYER_BLOCKED_FROM_EXITING_VEHICLE_IN_MISSION_FLAG(FALSE)
					ENDIF
				ENDIF
			ELIF mnVeh = VELUM2
				IF IS_VEHICLE_DRIVEABLE(tempVeh)
				AND (GET_PED_IN_VEHICLE_SEAT(tempVeh) = LocalPlayerPed)
				AND NOT IS_ENTITY_IN_AIR(tempVeh)
				AND ABSF(GET_ENTITY_SPEED(tempVeh)) < 0.5
				AND (GET_PED_USING_VEHICLE_DOOR(tempVeh, SC_DOOR_REAR_LEFT) = NULL) // If nobody else is using the door
				AND g_FMMC_STRUCT.iRootContentIDHash != g_sMPTUNABLES.iroot_id_HASH_Prison_Break_Plane
					
					FLOAT fDoorRatio = GET_VEHICLE_DOOR_ANGLE_RATIO(tempVeh, SC_DOOR_REAR_LEFT)
					
					IF fDoorRatio < 0.05
						bClearVelum2DoorOpening = FALSE
						
						IF NOT IS_BIT_SET(iLocalBoolCheck3, LBOOL3_VELUM2_DOOR_MOVING)
							IF NOT HAS_NET_TIMER_STARTED(tdVelumDoorDelayTimer)
								REINIT_NET_TIMER(tdVelumDoorDelayTimer)
							ELIF (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdVelumDoorDelayTimer) >= iVelumDoorOpenDelay)
								
								DISPLAY_HELP_TEXT_THIS_FRAME("HEIST_HELP_45", FALSE)
								
								IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
									SET_VEHICLE_DOOR_OPEN(tempVeh, SC_DOOR_REAR_LEFT)
									SET_BIT(iLocalBoolCheck3, LBOOL3_VELUM2_DOOR_MOVING)
								ENDIF
								
							ENDIF
						ENDIF
						
					ELIF fDoorRatio > 0.95
						bClearVelum2DoorOpening = FALSE
						
						IF NOT IS_BIT_SET(iLocalBoolCheck3, LBOOL3_VELUM2_DOOR_MOVING)
							IF NOT HAS_NET_TIMER_STARTED(tdVelumDoorDelayTimer)
								REINIT_NET_TIMER(tdVelumDoorDelayTimer)
							ELIF (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdVelumDoorDelayTimer) >= iVelumDoorCloseDelay)
								
								DISPLAY_HELP_TEXT_THIS_FRAME("HEIST_HELP_46", FALSE)
								
								IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
									SET_VEHICLE_DOOR_SHUT(tempVeh, SC_DOOR_REAR_LEFT, FALSE)
									SET_BIT(iLocalBoolCheck3, LBOOL3_VELUM2_DOOR_MOVING)
								ENDIF
								
							ENDIF
						ENDIF
						
					ENDIF
					
				ENDIF
			ELIF (mnVeh = BUZZARD
			OR mnVeh = BUZZARD2)
				IF IS_BIT_SET(g_FMMC_Struct.iTeamVehicleBitSet, ciFMMC_TeamVehBS_DISABLE_TEAM_RESPAWN_VEHICLE_SPACE_ROCKETS)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_NEXT_WEAPON)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_PREV_WEAPON)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_UD)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_LR)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
				ENDIF
			ENDIF
			
			IF MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] < FMMC_MAX_RULES
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitsetTen[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]], ciBS_RULE10_DISABLE_VEHICLE_JUMP)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CAR_JUMP)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_JUMP)
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_Struct.iTeamVehicleBitSet, ciFMMC_TeamVehBS_DISABLE_TEAM_RESPAWN_VEHICLE_SPACE_ROCKETS)
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_IHELP_BUZ")
				HIDE_HELP_TEXT_THIS_FRAME()
			ENDIF
		ENDIF
		
		PROCESS_PLAYER_FORCE_LOCK_ON()
				
		IF MC_PlayerBD[iLocalPart].iteam < FMMC_MAX_TEAMS
		AND MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] < FMMC_MAX_RULES
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]], ciBS_RULE8_EXPLOSIONS_DONT_RAGDOLL)
			OR IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet3, PBBOOL3_SUDDEN_DEATH_JUGGERNAUT)	
			OR IS_PARTICIPANT_A_JUGGERNAUT(iLocalPart)
				SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DontActivateRagdollFromExplosions, TRUE)
			ELSE
				SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DontActivateRagdollFromExplosions, FALSE)
			ENDIF
			
			IF g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iMaxHealth[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]] != 0
			AND NOT IS_PARTICIPANT_A_BEAST(iLocalPart)
				INT iNewMaxHealth = g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iMaxHealth[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]]
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]], ciBS_RULE8_SCALE_MAX_PLAYER_HEALTH)
					INT i
					INT iEnemyPlayers = 0
					FOR i = 0 TO FMMC_MAX_TEAMS - 1
						iEnemyPlayers = iEnemyPlayers + MC_serverBD.iNumberOfPlayingPlayers[i]
					ENDFOR
					iNewMaxHealth = (iNewMaxHealth * iEnemyPlayers)
					INCREASE_PLAYER_HEALTH(iNewMaxHealth)
				ELSE
					INCREASE_PLAYER_HEALTH(iNewMaxHealth)
				ENDIF
			ENDIF
		
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ MC_PlayerBD[iLocalPart].iteam ].iRuleBitSetFive[ MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] ], ciBS_RULE5_SLOW_PLAYER)
				DO_HEIST_HEAVY_ARMOUR_SLOWDOWN_EFFECT()
			ENDIF
		ENDIF
		
		PROCESS_SWAP_TO_KILLERS_TEAM()
		PROCESS_KILL_SELF_ON_TEAM_FAIL()
		
		PROCESS_BLIP_PLAYER_ARRIVED_AT_LOCATION()
		PROCESS_DZ_DISABLE_SPAWN_ACTIONS()
		
		PROCESS_SET_PLAYER_MOVESPEED_OVERRIDE_FOR_THIS_RULE()
	ENDIF
	
	//Don't apply damage override if powerups are effecting it
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam] < FMMC_MAX_RULES
		IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iRagePickupBS, ciCUSTOM_PICKUP_TYPE__BULLSHARK_TEST)
		AND NOT IS_PARTICIPANT_A_BEAST(iLocalPart)
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ MC_playerBD[iPartToUse].iTeam ].iRuleBitSetFive[ MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] ], ciBS_RULE5_BEASTMODE_INCREASE_DAMAGE)
			PROCESS_MELEE_DAMAGE_OVERRIDE_FOR_THIS_RULE()
			PROCESS_MELEE_WEAPON_DAMAGE_OVERRIDE_FOR_THIS_RULE()
			PROCESS_VEHICLE_WEAPON_DAMAGE_OUTPUT_OVERRIDE_FOR_THIS_RULE()
			PROCESS_VEHICLE_WEAPON_DAMAGE_INTAKE_OVERRIDE_FOR_THIS_RULE()			
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iSpawnInvincibility > -1
		SET_NEXT_RESPAWN_INVINCIBLE_TIME(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iSpawnInvincibility * 1000, g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iSpawnInvincibility * 1000)
	ENDIF
	PROCESS_GET_AND_DELIVER_FILTER()
	PROCESS_TEAM_REGEN()
	PROCESS_BEAST_MODE_PLAYER()

	PROCESS_PLAYER_VISUAL_AIDS()
	
	PROCESS_WEAPON_AUTO_FLASHLIGHT()
	
	IF USING_POISON_GAS()
		PROCESS_POISON_GAS_CHAMBER()
	ENDIF

	PROCESS_PARACHUTE_EQUIP_LAST()
	PROCESS_PLAYER_HEALTH_DRAIN()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciLOAD_POWER_PLAY_ASSETS)
		PROCESS_POWER_PLAY_PICKUPS()
	ENDIF
	PROCESS_MIDPOINT_BLOCKING()
	PROCESS_SWAN_DIVE_ANIMATIONS()
	
	PROCESS_SET_PLAYER_DRUNK_LEVEL()
	
	PROCESS_FORCED_STEALTH()
		
	PROCESS_POST_CUTSCENE_COVER()
	
	PROCESS_ON_RULE_EMAIL()
	
	PROCESS_CONTROLLER_LIGHT()
	
	PROCESS_FORCED_HEIST_SPECTATOR()
	
	PROCESS_RUNNING_BACK_MODE()
	
	PROCESS_ACTUAL_DEATH_RESPAWN_RADIO()
	
	PROCESS_ROUND_RESTART_LOGIC_RADIO()
	
	PROCESS_MANUAL_RESPAWN_RADIO()
	
	PROCESS_SHUNT_PICKUP_RETURN()
	
	PROCESS_TONY_HAWKS_MODE()
	
	PROC_MISSION_ARENA_ANNOUNCER_TRACKING()
	
	PROCESS_PHONE_EMP()
	
	PROCESS_LIMITED_STUN_GUN()
	
	IF IS_BIT_SET(iLocalBoolCheck23, LBOOL23_LOCATE_TEAM_SWAP_REQUESTED)
		PROCESS_CLIENT_LOCATE_TEAM_SWAP()
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_ALL_PLAYERS_HEALTH_BARS_ON_TEAM)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_ALL_PLAYERS_HEALTH_BARS_ON_TEAM_FOR_VEHICLE)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_TEAM_SETTINGS_HEALTH_BARS)
		PROCESS_PLAYER_HEALTH_BAR_DISPLAY()
	ENDIF
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TRANSFORM_ASSETS)
		PROCESS_JUGGERNAUT_EVERY_FRAME()
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRSHOOTOUT(g_FMMC_STRUCT.iAdversaryModeType)
		PROCESS_AIR_SHOOTOUT_EVERY_FRAME()
	ENDIF
	
	PROCESS_AKULA_STEALTH_MODE_IN_MISSION()
	
	PROCESS_TEAM_TURNS()
	
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_RUMBLE(g_FMMC_STRUCT.iAdversaryModeType)
			PROCESS_OVERTIME_RUMBLE_SUDDEN_DEATH_SETTINGS()		
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciOVERTIME_WAIT_YOUR_TURN)
			PROCESS_PLAYER_TURNS()
		ENDIF
	ENDIF
	
	/////////
	//Interact-With safeguards
	IF IS_BIT_SET(iLocalBoolCheck27, LBOOL27_WEAPON_HAS_BEEN_HIDDEN_BY_INTERACTION)
	AND sIWInfo.eInteractWith_CurrentState = IW_STATE_IDLE
	AND bhaBeamHackAnimState = BHA_STATE_NONE
	AND fkhaFingerprintKeypadHackAnimState = FKHA_STATE_NONE
		SET_CURRENT_PED_WEAPON(LocalPlayerPed, sIWInfo.wtInteractWith_StoredWeapon, TRUE)
		CLEAR_BIT(iLocalBoolCheck27, LBOOL27_WEAPON_HAS_BEEN_HIDDEN_BY_INTERACTION)
		PRINTLN("[InteractWith] Setting weapon to be visible again because of sIWInfo.eInteractWith_CurrentState")
	ENDIF
	
	IF MC_playerBD[iLocalPart].iObjHacking = -1
	AND sIWInfo.eInteractWith_CurrentState != IW_STATE_IDLE
	AND sIWInfo.eInteractWith_CurrentState != IW_STATE_ANIMATING
		BAIL_INTERACT_WITH(sIWInfo.iInteractWith_CachedIndex)
		CLEAR_PED_TASKS_IMMEDIATELY(LocalPlayerPed)
		PRINTLN("[InteractWith] We're not interacting with anything any more so setting sIWInfo.eInteractWith_CurrentState to NONE")
	ENDIF

	IF sIWInfo.iInteractWith_LastFrameProcessed < (GET_FRAME_COUNT() - ciInteractWith_PostAnimResetFrameDelay)
		IF sIWInfo.eInteractWith_CurrentState != IW_STATE_IDLE
			BAIL_INTERACT_WITH(sIWInfo.iInteractWith_CachedIndex)
			CLEAR_PED_TASKS_IMMEDIATELY(LocalPlayerPed)
			PRINTLN("[InteractWith] We haven't processed interact-with in ages, so stop interacting now")
		ENDIF
		
		// Removing phone contact for phone activation
		IF IS_CONTACT_IN_PHONEBOOK(CHAR_MP_DETONATEPHONE, MULTIPLAYER_BOOK)
			REMOVE_CONTACT_FROM_INDIVIDUAL_PHONEBOOK(CHAR_MP_DETONATEPHONE, MULTIPLAYER_BOOK)
			HANG_UP_AND_PUT_AWAY_PHONE(FALSE)
			PRINTLN("[InteractWith] Removing CHAR_MP_DETONATEPHONE!")
		ENDIF
	ENDIF
	
	/////////
	//Beam Hack Animation safeguards
	IF bhaBeamHackAnimState != BHA_STATE_NONE
		IF MC_playerBD[iLocalPart].iObjHacking = -1
			CLEAN_UP_PHYSICAL_BEAM_HACK_ANIM()
		ENDIF
	ENDIF
	
	/////////
	//Fingerprint Keypad Animation safeguards
	IF fkhaFingerprintKeypadHackAnimState != FKHA_STATE_NONE
		IF fkhaFingerprintKeypadHackAnimState  != FKHA_STATE_EXIT
		AND fkhaFingerprintKeypadHackAnimState != FKHA_STATE_SUCCESS_EXIT
		AND fkhaFingerprintKeypadHackAnimState != FKHA_STATE_QUICK_EXIT
			IF MC_playerBD[iLocalPart].iObjHacking = -1
				CLEAN_UP_PHYSICAL_FINGERPRINT_KEYPAD_HACK_ANIM()
			ENDIF
		ELSE
			HANDLE_PHYSICAL_FINGERPRINT_KEYPAD_HACK_ANIM_FALLBACK_CLEANUP()
		ENDIF
	ENDIF
	
	IF MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iPartToUse].iTeam] < FMMC_MAX_RULES
		IF g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iPartToUse].iTeam].iAmbientPoliceAccuracy[MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iPartToUse].iTeam]] > -1
			FLOAT fAccuracy = TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iPartToUse].iTeam].iAmbientPoliceAccuracy[MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iPartToUse].iTeam]])/100
			SET_AMBIENT_LAW_PED_ACCURACY_MODIFIER(fAccuracy)
		ENDIF
	ENDIF
	
	IF NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(LocalPlayer)
	OR DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(LocalPlayer) AND (PlayerToUse != LocalPlayer)
	OR g_eSpecialSpectatorState != SSS_IDLE
	
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciUSE_POWER_MAD_HUD)
			IF (bPlayerToUseOK OR IS_LOCAL_PLAYER_ANY_SPECTATOR())
			AND GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
			AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
				PROCESS_POWER_MAD_HUD()
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciUSE_CIRCLE_SCORE_HUD)
		OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciUSE_CIRCLE_HUD_TIMER)
		OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_CIRCLE_HUD_SCORES_WITH_TIMER)
			IF g_bMissionEnding
			AND NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_ENDING_HUD_UPDATED)
				IF NOT HAS_NET_TIMER_STARTED(tdEndingTimer)
					REINIT_NET_TIMER(tdEndingTimer)
				ELSE
					IF HAS_NET_TIMER_EXPIRED(tdEndingTimer, ciEndingTimerLength)
						SET_BIT(iLocalBoolCheck26, LBOOL26_ENDING_HUD_UPDATED)
						RESET_NET_TIMER(tdEndingTimer)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciUSE_CIRCLE_SCORE_HUD)
		AND NOT (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciSD_DISABLE_CIRCULAR_TIMER) AND IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH))
		AND ((NOT SHOULD_HIDE_THE_HUD_THIS_FRAME()) OR (g_bMissionEnding AND NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_ENDING_HUD_UPDATED)))
			IF (bPlayerToUseOK OR IS_LOCAL_PLAYER_ANY_SPECTATOR())
			AND GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
			AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
				PROCESS_CIRCLE_SCORE_HUD()	
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciUSE_CIRCLE_HUD_TIMER)
		AND (bPlayerToUseOK OR IS_LOCAL_PLAYER_ANY_SPECTATOR())
		AND GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
		AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
			PROCESS_CIRCLE_TIMER()
			IF (NOT SHOULD_HIDE_THE_HUD_THIS_FRAME() 
			 OR (g_bMissionEnding AND NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_ENDING_HUD_UPDATED)))
			AND NOT (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciSD_DISABLE_CIRCULAR_TIMER)
			 AND IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH))
				PROCESS_CIRCLE_TIMER_HUD()
			ENDIF
			PROCESS_CIRCLE_TIMER_SOUND()
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_CIRCLE_HUD_SCORES_WITH_TIMER)
		AND NOT (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciSD_DISABLE_CIRCULAR_TIMER) AND IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH))
		AND ((NOT SHOULD_HIDE_THE_HUD_THIS_FRAME()) OR (g_bMissionEnding AND NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_ENDING_HUD_UPDATED)))
			IF (bPlayerToUseOK OR (IS_LOCAL_PLAYER_ANY_SPECTATOR() OR g_eSpecialSpectatorState != SSS_IDLE))
			AND GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
			AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
				PROCESS_CIRCLE_SCORE_HUD_WITH_TIMER()
			ENDIF
		ENDIF
		
		IF USING_SHOWDOWN_POINTS()
			PROCESS_SHOWDOWN_HUD()
		ENDIF
		
		PROCESS_TONY_HAWKS_HUD()
		
		PROCESS_ALL_TEAM_MULTIRULE_TIMER_HUD()
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_USE_FLOATING_SCORE)
			TARGET_DRAW_FLOATING_SCORES(sFloatingScore)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_SMOKE_TRAILS)
		PROCESS_JET_SMOKE_TRAIL()
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_WVM_OPPRESSOR_2(g_FMMC_STRUCT.iRootContentIDHash)
		//Specific help text for Work Dispute respawning Oppressors
		PROCRESS_RESPAWN_VEH_HELP()
	ENDIF
	
	IF DOES_ENTITY_EXIST(eiSubmarineProp)
		PROCESS_SUBMARINE_PROP(eiSubmarineProp)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_STROMBERG_SUB_GPS)
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			IF NOT IS_BIT_SET(iLocalBoolCheck25, LBOOL25_STROMBERG_FORCE_GPS_ACTIVE)
				IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)) = STROMBERG
				AND IS_VEHICLE_IN_SUBMARINE_MODE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
					SET_FORCE_SHOW_GPS(TRUE)
					SET_BIT(iLocalBoolCheck25, LBOOL25_STROMBERG_FORCE_GPS_ACTIVE)
					PRINTLN("[RCC MISSION] ciENABLE_STROMBERG_SUB_GPS - In a Stromberg, force showing gps and setting bit")
				ENDIF
			ELSE
				IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)) = STROMBERG
				AND NOT IS_VEHICLE_IN_SUBMARINE_MODE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
					CLEAR_BIT(iLocalBoolCheck25, LBOOL25_STROMBERG_FORCE_GPS_ACTIVE)
					SET_FORCE_SHOW_GPS(FALSE)
					PRINTLN("[RCC MISSION] ciENABLE_STROMBERG_SUB_GPS - Not in sub mode. clearing bit and force show gps")
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_STROMBERG_FORCE_GPS_ACTIVE)
				CLEAR_BIT(iLocalBoolCheck25, LBOOL25_STROMBERG_FORCE_GPS_ACTIVE)
				SET_FORCE_SHOW_GPS(FALSE)
				PRINTLN("[RCC MISSION] ciENABLE_STROMBERG_SUB_GPS - Not in a vehicle clearing bit and force show gps")
			ENDIF
		ENDIF
	ENDIF
	
	PROCESS_HIDE_SPECIFIC_HELP_TEXT_IN_PHONE()
	
	DISPLAY_VEHICLE_RESPAWN_POOL()
	
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
	AND NOT g_bCelebrationScreenIsActive		
	AND NOT g_bEndOfMissionCleanUpHUD
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciMISSION_CUSTOM_SHARD_ON_TEAM_POINT)
			IF (IS_THIS_ROCKSTAR_MISSION_NEW_VS_POWER_MAD(g_FMMC_STRUCT.iAdversaryModeType) AND IS_BIT_SET(iLocalBoolCheck24, LBOOL24_WAIT_FOR_SCORE_SHARD))
			OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_POWER_MAD(g_FMMC_STRUCT.iAdversaryModeType)
				INT iTeamLoop
				FOR iTeamLoop = 0 TO MC_serverBD.iNumberOfTeams - 1
					IF MC_serverBD.iTeamScore[iTeamLoop] > 0
					AND MC_serverBD.iTeamScore[iTeamLoop] != iTeamScoreLast[iTeamLoop]
						IF NOT (IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH) AND IS_THIS_ROCKSTAR_MISSION_NEW_VS_POWER_MAD(g_FMMC_STRUCT.iAdversaryModeType))
							PRINTLN("[JS] Attempting to print shard for team", iTeamLoop," previous score: ", iTeamScoreLast[iTeamLoop], " new score: ", MC_serverBD.iTeamScore[iTeamLoop])
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciCUSTOM_SHARD_HAS_LITERAL_STRING)
							AND NOT IS_STRING_NULL_OR_EMPTY(g_sMission_TeamName[iTeamLoop])
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciCUSTOM_SHARD_COLOUR_REPLACEMENT_FOR_LITERAL)
									INT R = 0,G = 0,B = 0,A = 0
									HUD_COLOURS TempTeamColour
									TempTeamColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeamLoop, PlayerToUse)
									GET_HUD_COLOUR(TempTeamColour,R,G,B,A)
									SET_SCRIPT_VARIABLE_HUD_COLOUR(R,G,B,A)
								ENDIF
								SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_AND_INT(BIG_MESSAGE_GENERIC_MIDSIZED, iTeamLoop, g_FMMC_STRUCT.tlCustomShardStrapline, g_sMission_TeamName[iTeamLoop], g_FMMC_STRUCT.tlCustomShardTitle)
							ELSE
								SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_GENERIC_MIDSIZED, iTeamLoop, g_FMMC_STRUCT.tlCustomShardStrapline, g_FMMC_STRUCT.tlCustomShardTitle)
							ENDIF
							CLEAR_BIT(iLocalBoolCheck24, LBOOL24_WAIT_FOR_SCORE_SHARD)
							RESET_NET_TIMER(tdWaitForScoreShardTimeOut)
							IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_POWER_MAD(g_FMMC_STRUCT.iAdversaryModeType)
							AND MC_ServerBD_4.iCurrentHighestPriority[iTeamLoop] < FMMC_MAX_RULES
								IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeamLoop].iRuleBitsetEight[MC_ServerBD_4.iCurrentHighestPriority[iTeamLoop]],ciBS_RULE8_PAUSE_ON_PLAYER_DURING_RESTART)
									IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
										SET_BIT(iLocalBoolCheck22, LBOOL22_START_PAUSE_CAM_FOR_RESET)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						iTeamScoreLast[iTeamLoop] = MC_serverBD.iTeamScore[iTeamLoop]
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciUSE_MORE_ACCURATE_RACE_POSITION)
		PROCESS_ACCURATE_RACE_POSITION()
	ENDIF
	PROCESS_LAST_PLAYER_ALIVE()
	IF bIsLocalPlayerHost
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciOVERTIME_WAIT_YOUR_TURN)
			PROCESS_SERVER_PLAYER_TURNS()
		ENDIF
		IF iRezQTeamIterator < MC_serverBD.iNumberOfTeams - 1
			iRezQTeamIterator++
		ELSE
			iRezQTeamIterator = 0
		ENDIF
		PROCESS_SERVER_RESPAWN_QUEUE(iRezQTeamIterator)
		INT i
		IF g_bPlayerLeftCheckScore
			IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
			AND IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_EVERYONE_RUNNING)
			AND NOT g_bMissionEnding
				FOR i = 0 TO MC_serverBD.iNumberOfTeams - 1
					IF MC_serverBD_4.iCurrentHighestPriority[i] < FMMC_MAX_RULES
					AND g_FMMC_STRUCT.sFMMCEndConditions[i].iTeamEliminationScore[MC_serverBD_4.iCurrentHighestPriority[i]] > 0
						IF NOT IS_BIT_SET(MC_serverBD_1.iServerRespawnBitset, SRBS_RESPAWN_CHECK_ALL_DEAD_NOW_TEAM_0 + MC_PlayerBD[i].iteam)
							SET_BIT(MC_serverBD_1.iServerRespawnBitset, SRBS_RESPAWN_CHECK_ALL_DEAD_NOW_TEAM_0 + MC_PlayerBD[i].iteam)
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
			g_bPlayerLeftCheckScore = FALSE
		ENDIF
		
		IF IS_BIT_SET(MC_ServerBD.iServerBitSet6, SBBOOL6_PLAYER_HAS_BEEN_SPOTTED_CCTV)
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_RespectCCTVSpottedDelay)
			IF HAS_NET_TIMER_STARTED(CCTVSpottedResetTimer)
				IF HAS_NET_TIMER_EXPIRED(CCTVSpottedResetTimer, 2000)
					CLEAR_BIT(MC_ServerBD.iServerBitSet6, SBBOOL6_PLAYER_HAS_BEEN_SPOTTED_CCTV)
					PRINTLN("[RCC MISSION] - Clearing SBBOOL6_PLAYER_HAS_BEEN_SPOTTED_CCTV, Timer has expired")
				ENDIF
			ELSE
				CLEAR_BIT(MC_ServerBD.iServerBitSet6, SBBOOL6_PLAYER_HAS_BEEN_SPOTTED_CCTV)
				PRINTLN("[RCC MISSION] - Clearing SBBOOL6_PLAYER_HAS_BEEN_SPOTTED_CCTV, Timer isn't running")
			ENDIF
		ENDIF
	ENDIF
		
	PROCESS_RULE_ATTEMPTS_DISPLAY()
	
	IF MC_serverBD.iCleanedUpUndeliveredVehiclesBS != g_TransitionSessionNonResetVars.iDoNotRespawnVehBitSet
		g_TransitionSessionNonResetVars.iDoNotRespawnVehBitSet = MC_serverBD.iCleanedUpUndeliveredVehiclesBS
	ENDIF
	
	UPDATE_DESTROYED_TURRET_BITSET()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_ALL_PLAYERS_HEALTH_BARS_ON_TEAM)
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_ALL_PLAYERS_HEALTH_BARS_ON_TEAM_FOR_VEHICLE)
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_TEAM_SETTINGS_HEALTH_BARS)
		PROCESS_PLAYER_HEALTH_BAR_DISPLAY_FOR_RULE()
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_ShowVehicleHealthinFP)
	AND NOT IS_ON_SCREEN_WHEEL_SPIN_MINIGAME_ACTIVE()
	AND NOT IS_BIT_SET(iLocalBoolResetCheck1, LBOOL_RESET1_SHOWING_VEH_HEALTH_BARS)
		IF PROCESS_FP_VEHICLE_HEALTHBAR(ROUND(GET_FMMC_PLAYER_VEHICLE_BODY_HEALTH_FOR_TEAM(MC_PlayerBD[iLocalPart].iteam)))
			SET_BIT(iLocalBoolResetCheck1, LBOOL_RESET1_SHOWING_VEH_HEALTH_BARS)
		ENDIF
	ENDIF
	
	IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciBLIP_AFTER_ENTERING_VEHICLE)
		AND NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_BLIP_FORCED_ON_BY_ZONE)
			IF MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iTeam] < FMMC_MAX_RULES
				IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					SET_BIT(iLocalBoolCheck23, LBOOL23_BLIP_FORCED_ON_BY_ZONE)
					PROCESS_MY_PLAYER_BLIP_VISIBILITY(MC_PlayerBD[iLocalPart].iTeam, MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iTeam])
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	PROCESS_LOCK_PEGASUS_VEHICLE()
	
	PROCESS_PLAYER_IN_MOC()
	
	PROCESS_PLAYER_CANT_DAMAGE_PLACED_VEH_WITH_COLLISION()
	
	PROCESS_PLAYER_RESET_SPAWN_PROTECTION()
	
	IF IS_BIT_SET(iLocalboolCheck25, LBOOL25_CANNOT_BE_DAMAGED_DUE_TO_VEHICLE)
	AND iVehLastInsideGrantedInvulnability != -1
	AND (IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER) OR (IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMBUSHKA(g_FMMC_STRUCT.iAdversaryModeType) AND g_mnOfLastVehicleWeWasIn = BOMBUSHKA))
		BOOL bResetInvulnerability
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVehLastInsideGrantedInvulnability])
			VEHICLE_INDEX viPlayer = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVehLastInsideGrantedInvulnability])
			
			IF IS_VEHICLE_FUCKED(viPlayer)
				bResetInvulnerability = TRUE
			ENDIF
		ELSE
			bResetInvulnerability = TRUE
		ENDIF
		
		IF bResetInvulnerability
			PRINTLN("[LM][EVERY_FRAME_CHECKS] - ciFMMC_VEHICLE7_PLAYER_INVINCIBLE_IN_VEHICLE is set when mission is over. Setting SET_ENTITY_VISIBLE to false because of - iVeh: ", iVehLastInsideGrantedInvulnability)	
			
			CLEAR_BIT(iLocalBoolCheck25, LBOOL25_CANNOT_BE_DAMAGED_DUE_TO_VEHICLE)
			iVehLastInsideGrantedInvulnability = -1
			
			CLEANUP_PLAYER_FOR_WINNER_LOSER_SHARD_APPEARANCE(TRUE, TRUE)
		ENDIF
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_RESURRECTION(g_FMMC_STRUCT.iAdversaryModeType)
		IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
			IF bUsingBirdsEyeCam
			AND g_iFMMCCurrentStaticCam != -1
				IF g_FMMC_STRUCT.iShowMarkersOnCam > -1
				AND g_iFMMCCurrentStaticCam = g_FMMC_STRUCT.iShowMarkersOnCam
					IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
					AND NOT USING_HEIST_SPECTATE()
						DRAW_SPECTATOR_PLAYER_MARKERS(MC_PlayerBD[iPartToUse].iTeam)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA(g_FMMC_STRUCT.iAdversaryModeType)
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
		USE_VEHICLE_CAM_STUNT_SETTINGS_THIS_UPDATE()
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciOPTION_VEHICLE_WEAPON_ENABLED)
		PROCESS_VEHICLE_WEAPON_PICKUPS()
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_VEHICLE_WEAPON_MG)
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
			PROCESS_VEHICLE_WEAPON_MACHINE_GUN(DEFAULT, FALSE)
		ELSE
			PROCESS_VEHICLE_WEAPON_MACHINE_GUN()
		ENDIF
		PROCESS_REMOTE_PLAYER_MACHINE_GUNS()
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciEXPLODE_VEHICLE_ON_ZERO_HEALTH)
		PROCESS_VEHICLE_WEAPON_EXPLODE_ON_ZERO_HEALTH()
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_KILLSTREAK_OPTIONS)
		PROCESS_KILLSTREAK_POWERUPS()
	ENDIF

	PROCESS_GHOSTING_THROUGH_PLAYER_VEHICLES()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciLBD_SHOW_SLIPSTREAM_TIME)
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			IF GET_VEHICLE_CURRENT_TIME_IN_SLIP_STREAM(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)) > 0.0
				UPDATE_TIME_SLIPSTREAMING(iSlipstreamTime)
				bVehicleIsSlipstreaming = TRUE
			ELSE
				IF bVehicleIsSlipstreaming
					PRINTLN("[KH] PROCESS_EVERY_FRAME_PLAYER_CHECKS - Updating time player has been slipstreaming for to: ", iSlipstreamTime)
					IF iSlipstreamTime > 500
						MC_playerBD[iPartToUse].iSlipstreamTime = iSlipstreamTime
					ENDIF
					bVehicleIsSlipstreaming = FALSE
				ENDIF
			ENDIF
		ELSE
			bVehicleIsSlipstreaming = FALSE
		ENDIF
	ENDIF
	
	PROCESS_SERVER_DRAG_AMOUNTS()
	PROCESS_AIR_DRAG_CATCH_UP()
	
	IF bClearVelum2DoorOpening
		CLEAR_BIT(iLocalBoolCheck3, LBOOL3_VELUM2_DOOR_MOVING)
		
		IF HAS_NET_TIMER_STARTED(tdVelumDoorDelayTimer)
			RESET_NET_TIMER(tdVelumDoorDelayTimer)
		ENDIF
	ENDIF
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetFour[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE4_CAN_DROP_PACKAGE)
		AND MC_playerBD[iPartToUse].iObjCarryCount > 0
			PROCESS_PACKAGE_HANDLING()
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck4, LBOOL4_DELIVERY_WAIT)
		IF IS_PLAYER_IN_VEH_SEAT(LocalPlayer,VS_DRIVER)
		AND NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
			VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
			IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdDeliverTimer) > 1800
				IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
					IF IS_VEHICLE_DRIVEABLE(tempVeh)
						SET_VEHICLE_HANDBRAKE(tempVeh,FALSE)
					ENDIF
					NET_SET_PLAYER_CONTROL(LocalPlayer,TRUE)
					CLEAR_BIT(iLocalBoolCheck4, LBOOL4_DELIVERY_WAIT)
				ELSE
					PRINTLN("[RCC MISSION] LBOOL4_DELIVERY_WAIT Want to turn on player controle but IS_NEW_LOAD_SCENE_ACTIVE (1) ")
				ENDIF
			ELSE
				IF IS_VEHICLE_DRIVEABLE(tempVeh)
					IF GET_ENTITY_SPEED(tempVeh) > 0.2
						SET_ENTITY_VELOCITY(tempVeh,<<0,0,0>>)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
				NET_SET_PLAYER_CONTROL(LocalPlayer,TRUE)
				CLEAR_BIT(iLocalBoolCheck4, LBOOL4_DELIVERY_WAIT)
			ELSE
				PRINTLN("[RCC MISSION] LBOOL4_DELIVERY_WAIT Want to turn on player controle but IS_NEW_LOAD_SCENE_ACTIVE (2) ")
			ENDIF
		ENDIF
	ELIF HAS_NET_TIMER_STARTED(tdDeliverBackupTimer)
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdDeliverBackupTimer) > 10000
			//Safety thing
			NET_SET_PLAYER_CONTROL(LocalPlayer,TRUE)
			
			IF DeliveryVeh != NULL
				SET_VEHICLE_HANDBRAKE(DeliveryVeh,FALSE)
			ELIF ( IS_PLAYER_IN_VEH_SEAT(LocalPlayer,VS_DRIVER)
				   AND NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)) )
				SET_VEHICLE_HANDBRAKE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed),FALSE)
			ENDIF
			
			IF IS_PED_IN_ANY_BOAT(LocalPlayerPed)
			OR IS_PED_IN_ANY_AMPHIBIOUS_VEHICLE(LocalPlayerPed) // url:bugstar:3498788
			VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
				IF IS_VEHICLE_DRIVEABLE(tempVeh)
					IF GET_PED_IN_VEHICLE_SEAT(tempVeh) = LocalPlayerPed
						IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
							SET_BOAT_ANCHOR(tempVeh,FALSE)
							SET_BOAT_REMAINS_ANCHORED_WHILE_PLAYER_IS_DRIVER(tempVeh,FALSE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			MC_playerBD[iPartToUse].iVehDeliveryId =-1
			DeliveryVeh = NULL
			
			RESET_NET_TIMER(tdDeliverBackupTimer)
			CLEAR_BIT(iLocalBoolCheck2,LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
			CLEAR_BIT(iLocalBoolCheck2,LBOOL2_PED_DROPPED_OFF)
			CLEAR_BIT(iLocalBoolCheck,LBOOL_CARGOBOB_DROP)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck5, LBOOL5_KEEP_FORCE_EVERYONE_FROM_MY_CAR)
		IF FORCE_EVERYONE_FROM_MY_CAR(TRUE, TRUE)
			CLEAR_BIT(iLocalBoolCheck5, LBOOL5_KEEP_FORCE_EVERYONE_FROM_MY_CAR)
		ENDIF
	ENDIF
	
	IF iDeliveryVehForcingOut != -1
		IF IS_BIT_SET(MC_serverBD.iProcessJobCompBitset, iPartSending_DeliveryVehForcingOut)
			//Delivery has finished, and we've got the server update saying it's all good!
			iDeliveryVehForcingOut = -1
			iPartSending_DeliveryVehForcingOut = -1
			RESET_NET_TIMER(tdForceEveryoneOutBackupTimer)
		ELSE
			IF NOT HAS_NET_TIMER_STARTED(tdForceEveryoneOutBackupTimer)
				REINIT_NET_TIMER(tdForceEveryoneOutBackupTimer)
			ELSE
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdForceEveryoneOutBackupTimer) > 10000
					PRINTLN("[RCC MISSION] tdForceEveryoneOutBackupTimer has been running for 10 seconds with no job completion event, clear it and iDeliveryVehForcingOut!")
					iDeliveryVehForcingOut = -1
					iPartSending_DeliveryVehForcingOut = -1
					RESET_NET_TIMER(tdForceEveryoneOutBackupTimer)
				ENDIF
			ENDIF
		ENDIF
	ELIF HAS_NET_TIMER_STARTED(tdForceEveryoneOutBackupTimer)
		RESET_NET_TIMER(tdForceEveryoneOutBackupTimer)
	ENDIF
	
	IF bStopDurationBlockSeatShuffle
		IF GET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_PreventAutoShuffleToDriversSeat)
			IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_PreventAutoShuffleToDriversSeat, FALSE)
				
				bStopDurationBlockSeatShuffle = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF bPlayerToUseOK		
		IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_FINISHED)
		AND IS_TEAM_ACTIVE(MC_playerBD[iPartToUse].iteam)
		AND MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
		AND (g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMinSpeed[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] != -1)
		AND IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
		AND NOT g_bMissionEnding
		AND IS_PLAYER_CONTROL_ON(LocalPlayer)
		AND NOT IS_BIT_SET(iLocalBoolCheck28, LBBOOL28_REACHED_END_OF_STUNTING_PACK)
		AND IS_SKYSWOOP_AT_GROUND()

			//Only process if player hasn't actually finished the race
			
			PRINTLN("[RCC MISSION] [BLOW UP] MC_ServerBD_4.iTeamLapsCompleted[MC_playerBD[iPartToUse].iteam]= ",MC_ServerBD_4.iTeamLapsCompleted[MC_playerBD[iPartToUse].iteam])
			PRINTLN("[RCC MISSION] [BLOW UP] g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMissionLapLimit= ",g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMissionLapLimit)
			IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMissionLapLimit = 0
			OR (MC_ServerBD_4.iTeamLapsCompleted[MC_playerBD[iPartToUse].iteam] < g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMissionLapLimit)
				PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED()
			ENDIF
		ELSE
			IF NOT HAS_SOUND_FINISHED(iSoundIDVehBomb)
				STOP_SOUND(iSoundIDVehBomb)
			ENDIF
			IF NOT HAS_SOUND_FINISHED(iSoundIDVehBomb2)
				STOP_SOUND(iSoundIDVehBomb2)
			ENDIF
			IF NOT HAS_SOUND_FINISHED(iSoundIDVehBombCountdown)
				STOP_SOUND(iSoundIDVehBombCountdown)
			ENDIF
			fRealExplodeProgress = 0
			
			//Clear HUD thing
			IF bShowingAltitudeLimit
				SET_FAKE_MINIMAP_MAX_ALTIMETER_HEIGHT(0)
				bShowingAltitudeLimit = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF bLocalPlayerOK
		IF GET_PLAYER_UNDERWATER_TIME_REMAINING( LocalPlayer ) > 0 // We're not drowning
			IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_I_AM_DROWNING)
				CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_I_AM_DROWNING)
				PRINTLN("[RCC MISSION] PROCESS_EVERY_FRAME_PLAYER_CHECKS - I'm no longer drowning, clearing ClientBitset bit...")
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_I_AM_DROWNING)
				SET_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_I_AM_DROWNING)
				PRINTLN("[RCC MISSION] PROCESS_EVERY_FRAME_PLAYER_CHECKS - I'm drowning! Set the ClientBitset bit")
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_I_AM_DROWNING)
			CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_I_AM_DROWNING)
			PRINTLN("[RCC MISSION] PROCESS_EVERY_FRAME_PLAYER_CHECKS - I'm not okay, clearing ClientBitset drowning bit...")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_SD_DROP_PORTABLE_PICKUPS) AND IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
		IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
			IF NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_SUDDEN_DEATH_DROP_PICKUPS)
				PRINTLN("[RCC MISSION][SD][PROCESS_GENERIC_SUDDEN_DEATH_LOGIC] SUDDEN DEATH - ciENABLE_SD_DROP_PORTABLE_PICKUPS Calling to detach.")
				DETACH_PORTABLE_PICKUP_FROM_SPECIFIC_PED(localPlayerPed)
				DETACH_AND_RESET_ANY_PICKUPS()
				MC_playerBD[iLocalPart].iObjCarryCount = 0
				SET_BIT(iLocalBoolCheck24, LBOOL24_SUDDEN_DEATH_DROP_PICKUPS)
				
				INT iObj
				FOR iobj = 0 TO ( MC_serverBD.iNumObjCreated - 1 )					
					OBJECT_INDEX tempObj
					IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
						tempObj = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
						PREVENT_COLLECTION_OF_PORTABLE_PICKUP(tempObj, TRUE, TRUE)
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
	ENDIF
	
	IF (IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH) AND IS_BIT_SET(MC_ServerBD.iServerBitSet6, SBBOOL6_SUDDEN_DEATH_STOCKPILE))
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciNEXT_DELIVERY_WINS_SD_ONLY)
		IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
			INT iObj
			FOR iobj = 0 TO ( MC_serverBD.iNumObjCreated - 1 )					
				OBJECT_INDEX tempObj
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
					tempObj = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
					IF NOT IS_ENTITY_ATTACHED_TO_ANY_PED(tempObj)
						PREVENT_COLLECTION_OF_PORTABLE_PICKUP(tempObj, TRUE, TRUE)
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
	ENDIF
			
	BOOL bIsPlayerInTrash = FALSE
	IF( IS_PED_SITTING_IN_ANY_VEHICLE( LocalPlayerPed ) )
		VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN( LocalPlayerPed, FALSE )
		IF( DOES_ENTITY_EXIST( veh ) )
			IF( GET_ENTITY_MODEL( veh ) = TRASH
			OR  GET_ENTITY_MODEL( veh ) = TRASH2 )
				bIsPlayerInTrash = TRUE
			ENDIF
		ENDIF
	ENDIF
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRSHOOTOUT(g_FMMC_STRUCT.iAdversaryModeType)
		bIsPlayerInTrash = TRUE
	ENDIF
	IF( bIsPlayerInTrash )
		IF( NOT bWasKnockOffSettingChangedForTrash )
			SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE( LocalPlayerPed, KNOCKOFFVEHICLE_NEVER )
			bWasKnockOffSettingChangedForTrash = TRUE
		ENDIF
	ELSE
		IF( bWasKnockOffSettingChangedForTrash )
			SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE( LocalPlayerPed, KNOCKOFFVEHICLE_DEFAULT )
			bWasKnockOffSettingChangedForTrash = FALSE
		ENDIF
	ENDIF
	
	IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
		IF MC_playerBD[iPartToUse].bCelebrationScreenIsActive != g_bCelebrationScreenIsActive
			MC_playerBD[iPartToUse].bCelebrationScreenIsActive = g_bCelebrationScreenIsActive
			//dont want sctv coming in during celebration and seeing weirdness
		ENDIF
	ENDIF	
	
	IF g_FMMC_STRUCT.fMeleeDamageModifier <> 1
		INT iDamageModLoop
		FOR iDamageModLoop = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
			PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iDamageModLoop)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
				IF g_FMMC_STRUCT.fMeleeDamageModifier <= 0.1
					PRINTLN("[LH][MELEE_DAMAGE] g_FMMC_STRUCT.fMeleeDamageModifier = ",g_FMMC_STRUCT.fMeleeDamageModifier)
					SET_PLAYER_WEAPON_TAKEDOWN_DEFENSE_MODIFIER(tempPlayer, 0.11) //minimum value
				ELSE
					PRINTLN("[LH][MELEE_DAMAGE] g_FMMC_STRUCT.fMeleeDamageModifier = ",g_FMMC_STRUCT.fMeleeDamageModifier)
					SET_PLAYER_WEAPON_TAKEDOWN_DEFENSE_MODIFIER(tempPlayer, g_FMMC_STRUCT.fMeleeDamageModifier)
				ENDIF
			ENDIF
		ENDFOR
		
		bTakedownDamageModifier = TRUE
	ENDIF
	
	WEAPON_TYPE wtWeaponType
	GET_CURRENT_PED_WEAPON(LocalPlayerPed, wtWeaponType)
	IF wtWeaponType != WEAPONTYPE_UNARMED
		GET_CURRENT_PED_WEAPON(LocalPlayerPed, wtCachedLastWeaponHeld)
	ENDIF

	IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
		IF NOT IS_BIT_SET(iLocalBoolCheck18, LBOOL18_WEAPONS_ENABLED_SHARD_PLAY)
			IF (g_FMMC_STRUCT.fTimeWeaponsDisabledAtStart*1000) > 0
				IF HAS_NET_TIMER_STARTED(tdMissionStartWeaponsDisabled)
					IF NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(tdMissionStartWeaponsDisabled, ROUND(g_FMMC_STRUCT.fTimeWeaponsDisabledAtStart*1000))
						// Stop Attacking.
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PARACHUTE_DEPLOY)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
						IF NOT GET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DisableVehicleCombat)
							SET_PED_CONFIG_FLAG(LocalPlayerPed,PCF_DisableVehicleCombat,TRUE)
						ENDIF
												
						SET_PED_CAN_SWITCH_WEAPON(LocalPlayerPed, FALSE)
						SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED, TRUE)
					ELSE
						// Play Shard
						IF GET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DisableVehicleCombat)
							SET_PED_CONFIG_FLAG(LocalPlayerPed,PCF_DisableVehicleCombat,FALSE)
						ENDIF
						SET_PED_CAN_SWITCH_WEAPON(LocalPlayerPed, TRUE)
						SET_CURRENT_PED_WEAPON(LocalPlayerPed, wtCachedLastWeaponHeld, TRUE)
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, "SHD_TAG_WPN")
						SET_BIT(iLocalBoolCheck18, LBOOL18_WEAPONS_ENABLED_SHARD_PLAY)					
					ENDIF
				ELSE
					START_NET_TIMER(tdMissionStartWeaponsDisabled)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
	AND NOT HAS_TEAM_FINISHED(MC_playerBD[iLocalPart].iteam)
	AND NOT HAS_TEAM_FAILED(MC_playerBD[iLocalPart].iteam)
	AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
		IF NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iLocalPart].iteam)
		AND NOT IS_PLAYER_IN_RESPAWN_BACK_AT_START_LOGIC(iLocalPart)
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciTRANSFORM_TO_JUGGERNAUT_ON_SUDDEN_DEATH)
				IF NOT IS_PARTICIPANT_A_JUGGERNAUT(iLocalPart)
					TRANSFORM_LOCAL_PLAYER_INTO_JUGGERNAUT(DEFAULT, 3000)
				ENDIF
			ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciTRANSFORM_TO_BEAST_ON_SUDDEN_DEATH)
				IF NOT IS_PARTICIPANT_A_BEAST(iLocalPart)
					TRANSFORM_LOCAL_PLAYER_INTO_BEAST()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	// This bug does not seem to affect other modes. But, nothing special is set for Spectators in turf Wars. url:bugstar:3217923
	IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
	AND IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH)
	AND IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARS(g_FMMC_STRUCT.iAdversaryModeType)
	AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
	
		IF NOT HAS_NET_TIMER_STARTED(tdTurfWarCollisionSpectatorFixTimer)
			START_NET_TIMER(tdTurfWarCollisionSpectatorFixTimer)
		ENDIF
		
		IF HAS_NET_TIMER_STARTED(tdTurfWarCollisionSpectatorFixTimer)
			IF NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(tdTurfWarCollisionSpectatorFixTimer, ci_TURF_WAR_COLLISION_SPECTATOR_TIME)
				//IF GET_ENTITY_COLLISION_DISABLED(localPlayerPed) = FALSE
					SET_ENTITY_COLLISION(localPlayerPed, FALSE)
					PRINTLN("[LM][url:bugstar:3217923] - Setting no Collision to fix the Spectator Collision bug.")
				//ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
	AND IS_THIS_ROCKSTAR_MISSION_NEW_VS_STOCKPILE(g_FMMC_STRUCT.iAdversaryModeType)
	AND IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH)
	AND NOT g_bMissionEnding
		SET_ENTITY_COLLISION(localPlayerPed, FALSE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciALLOW_NON_HEIST_REBREATHERS)
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iPartToUse].iteam)
			
			IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
			AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetThree[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE3_QUICK_EQUIP_REBREATHER)
				IF NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_HAS_PLAYER_BEEN_GIVEN_A_REBREATHER)
					INT iBreatherCount = GET_MP_INT_CHARACTER_STAT(MP_STAT_BREATHING_APPAR_BOUGHT, DEFAULT, TRUE)
					INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_BREATHING_APPAR_BOUGHT, g_FMMC_STRUCT.iWVMAmountOfRebreatherCanisters - iBreatherCount)
					PRINTLN("[RCC MISSION] - Giving ", g_FMMC_STRUCT.iWVMAmountOfRebreatherCanisters," rebreathers")
					SET_BIT(iLocalBoolCheck23, LBOOL23_HAS_PLAYER_BEEN_GIVEN_A_REBREATHER)
					PREVENT_GANG_OUTFIT_CHANGING(TRUE)
				ENDIF
			ELSE
				PREVENT_GANG_OUTFIT_CHANGING(FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciALLOW_SPRINTING_IN_INTERIORS)
		IF NOT bLocalPlayerPedOk
			bIgnoreInteriorCheckForSprinting = FALSE
		ELSE
			IF NOT bIgnoreInteriorCheckForSprinting
			OR NOT GET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_IgnoreInteriorCheckForSprinting)
				SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_IgnoreInteriorCheckForSprinting, TRUE)
				PRINTLN("[RCC MISSION] PROCESS_EVERY_FRAME_PLAYER_CHECKS - SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_IgnoreInteriorCheckForSprinting, TRUE)")
				
				bIgnoreInteriorCheckForSprinting = TRUE
			ENDIF
		ENDIF
	ENDIF

	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_TAGGING_TARGETS)
		PROCESS_ENTITY_MARKER_TAGGING()
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
		IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		AND IS_ENTITY_ALIVE(LocalPlayerPed)
			IF SHOULD_TRANSFORM_PED_SKY_DIVE()
			AND IS_ENTITY_IN_AIR(LocalPlayerPed)
			AND IS_PED_TRYING_TO_ENTER_OR_EXIT_VEHICLE(LocalPlayerPed, TRUE)
				IF NOT IS_BIT_SET(iLocalBoolCheck25, LBOOL25_JUMPED_FROM_OPPRESSOR_SKY_DIVE )
					CLEAR_PED_TASKS_IMMEDIATELY(LocalPlayerPed)
					TASK_SKY_DIVE(LocalPlayerPed, TRUE)
					SET_BIT(iLocalBoolCheck25, LBOOL25_JUMPED_FROM_OPPRESSOR_SKY_DIVE)
				ENDIF
			ELSE
				CLEAR_BIT(iLocalBoolCheck25, LBOOL25_JUMPED_FROM_OPPRESSOR_SKY_DIVE)
			ENDIF
		ENDIF
	ENDIF
	
	IF (GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION() AND (g_FMMC_STRUCT.iInteriorBS != 0 OR g_FMMC_STRUCT.iInteriorBS2 != 0))
		IF GET_INTERIOR_FROM_ENTITY(LocalPlayerPed) != NULL
			IF NOT g_SpawnData.MissionSpawnDetails.bConsiderInteriors	
				IF NOT IS_PED_INJURED(LocalPlayerPed)
					SET_PLAYER_RESPAWN_TO_CONSIDER_INTERIORS_ON_THIS_MISSION(TRUE)
					CPRINTLN(DEBUG_MISSION,"[RCC MISSION] PROCESS_EVERY_FRAME_PLAYER_CHECKS - setting dead spawn to consider interiors ")
				ENDIF
			ENDIF
		ELSE
			IF g_SpawnData.MissionSpawnDetails.bConsiderInteriors
				IF NOT IS_PED_INJURED(LocalPlayerPed)
				AND NOT IS_PLAYER_RESPAWNING(LocalPlayer)
					SET_PLAYER_RESPAWN_TO_CONSIDER_INTERIORS_ON_THIS_MISSION(FALSE)
					CPRINTLN(DEBUG_MISSION,"[RCC MISSION] PROCESS_EVERY_FRAME_PLAYER_CHECKS - Clearing dead spawn to consider interiors ")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SUB)
		IF GET_INTERIOR_AT_COORDS(GET_ENTITY_COORDS(LocalPlayerPed)) = GET_INTERIOR_AT_COORDS(vSubmarineInteriorCoords)
			SET_BLIP_COORDS(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(LocalPlayer)], <<-1682.577,5926.310,-62.092>>)
		ENDIF
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
		IF MC_playerBD[iLocalPart].iCurrentLoc != -1
		AND GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
			fTempCaptureTime += fLastFrameTime
		ELSE
			IF fTempCaptureTime > 0
				MC_playerBD[iLocalPart].fCaptureTime +=  fTempCaptureTime
				fTempCaptureTime = 0
				PRINTLN("[RCC MISSION] - Not in a locate, updating capture time to: ", MC_playerBD[iLocalPart].fCaptureTime)
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_STROMBERG_TRANSFORM_SWEETENED)
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			VEHICLE_INDEX tempStromberg = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
			IF GET_ENTITY_MODEL(tempStromberg) = STROMBERG
				IF IS_VEHICLE_IN_SUBMARINE_MODE(tempStromberg)
				AND IS_ENTITY_IN_WATER(tempStromberg)
					PLAY_SOUND_FRONTEND(-1,"transform_oneshot", "dlc_xm_stromberg_sounds")
					SET_BIT(iLocalBoolCheck27, LBOOL27_STROMBERG_TRANSFORM_SWEETENED)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam] < FMMC_MAX_RULES
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iRuleBitsetTen[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam]], ciBS_RULE10_PROGRESS_RULE_ALL_DRIVER_ACCEL)
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iRuleBitsetEleven[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam]], ciBS_RULE11_DISABLE_AVENGER_TAKEOFF_UNTIL_FULL)
				IF NOT ARE_REMAINING_PLAYERS_IN_VEHICLE_INTERIOR(TRUE)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_UP)
					PRINTLN("[RCC MISSION] Disabling PLAYER_CONTROL, INPUT_VEH_ACCELERATE. Need everyone to be in the vehicle/ vehicle interior.")
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iRuleBitsetEleven[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam]], ciBS_RULE11_BLOCK_VEHICLE_EXIT)
			
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
				PRINTLN("[RCC MISSION] Disabling PLAYER_CONTROL, INPUT_VEH_EXIT. ciBS_RULE11_BLOCK_VEHICLE_EXIT")
			ENDIF
			
			IF IS_PLAYER_IN_CREATOR_AIRCRAFT(LocalPlayer)
				IF NOT IS_SIMPLE_INTERIOR_EXIT_BLOCKED()
					PRINTLN("[RCC MISSION] Set BLOCK_SIMPLE_INTERIOR_EXIT = TRUE, because IS_PLAYER_IN_CREATOR_AIRCRAFT = TRUE ")
					BLOCK_SIMPLE_INTERIOR_EXIT(TRUE)
				ENDIF
			ELSE
				IF IS_SIMPLE_INTERIOR_EXIT_BLOCKED()
				AND NOT IS_AVENGER_HOLD_TRANSITION_BLOCKED()
					PRINTLN("[RCC MISSION] Set BLOCK_SIMPLE_INTERIOR_EXIT = FALSE, because IS_PLAYER_IN_CREATOR_AIRCRAFT = FALSE ")
					BLOCK_SIMPLE_INTERIOR_EXIT(FALSE)
				ENDIF
			ENDIF
		
		ELSE
			
			IF IS_SIMPLE_INTERIOR_EXIT_BLOCKED()
			AND NOT IS_AVENGER_HOLD_TRANSITION_BLOCKED()
			AND NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_AVENGER_EXIT_BLOCKED)
				PRINTLN("[RCC MISSION] Set BLOCK_SIMPLE_INTERIOR_EXIT = FALSE, because ciBS_RULE11_BLOCK_VEHICLE_EXIT = FALSE ")
				BLOCK_SIMPLE_INTERIOR_EXIT(FALSE)
			ENDIF

		ENDIF
		PROCESS_RESPAWN_DELUXO_VEHICLE()		
	ENDIF
	
	#IF IS_DEBUG_BUILD 
	IF bShowVehSpawnNumberCycle
		TEXT_LABEL_15 tl15 
		INT iTeamLoopa, iSpawnPoint
		FOR iTeamLoopa = 0 TO FMMC_MAX_TEAMS-1									
			IF g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[iTeamLoopa] > 0
				INT iLoopMax
				iLoopMax = MC_serverBD.iNumberOfPlayingPlayers[iTeamLoopa]-1
				tl15 = "iLoopMax:"
				tl15 += iLoopMax
				DRAW_DEBUG_TEXT_2D(tl15 , <<0.3+(0.35*iTeamLoopa), 0.25, 0.25>>)
				
				FOR iSpawnPoint = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[iTeamLoopa]-1
					tl15 = "SP:"
					tl15 += iSpawnPoint
					DRAW_DEBUG_TEXT_2D(tl15 , <<0.3+(0.35*iTeamLoopa), 0.35+(0.03*iSpawnPoint), 0.35+(0.03*iSpawnPoint)>>)
					
					tl15 = "Veh: "
					tl15 += MC_serverBD_3.iPlacedVehiclePlayerPart[iTeamLoopa][iSpawnPoint]
					DRAW_DEBUG_TEXT_2D(tl15 , <<0.38+(0.35*iTeamLoopa), 0.35+(0.03*iSpawnPoint), 0.35+(0.03*iSpawnPoint)>>)
				ENDFOR
				
				FOR iSpawnPoint = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[iTeamLoopa]-1
					tl15 = "SP:"
					tl15 += iSpawnPoint
					DRAW_DEBUG_TEXT_2D(tl15 , <<0.3+(0.35*iTeamLoopa), 0.6+(0.03*iSpawnPoint), 0.6+(0.03*iSpawnPoint)>>)
					
					tl15 = "Player Part: "
					tl15 += iPartNumbersArrayedByTeamOrder[iTeamLoopa][iSpawnPoint]
					DRAW_DEBUG_TEXT_2D(tl15 , <<0.38+(0.35*iTeamLoopa), 0.6+(0.03*iSpawnPoint), 0.6+(0.03*iSpawnPoint)>>)
				ENDFOR
				
			ENDIF	
		ENDFOR
	ENDIF
	IF bRunningBackDiveCamLine 
		IF NOT IS_VECTOR_ZERO(vDebugForDiveCamStartPos)
			DRAW_DEBUG_LINE(vDebugForDiveCamStartPos, vDebugForDiveCamStartEndPos)
			PRINTLN("vDebugForDiveCamStartPos: ", vDebugForDiveCamStartPos)
			PRINTLN("vDebugForDiveCamStartEndPos: ", vDebugForDiveCamStartEndPos)
		ENDIF
	ENDIF
	#ENDIF
	

	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_CTF_SCREEN_FILTER_ON_HOLDING_FLAG)
			
		IF MC_playerBD[iLocalPart].iObjCarryCount != 0
		AND MC_serverBD.iServerGameState = GAME_STATE_RUNNING
		AND IS_PED_CARRYING_ANY_OBJECTS(LocalPlayerPed)
		AND NOT HAS_TEAM_FINISHED(MC_PlayerBD[iLocalPart].iteam)
		AND NOT IS_HEAVY_FILTER_ARENA_LIGHTING()
			IF NOT ANIMPOSTFX_IS_RUNNING("CrossLine")
				PRINTLN("[RCC MISSION] ciOptionsBS21_CTF_SCREEN_FILTER_ON_HOLDING_FLAG - Playing CrossLine VFX")
				ANIMPOSTFX_PLAY("CrossLine", 0, TRUE)
				PLAY_SOUND_FRONTEND(-1,"Player_Enter_Line","GTAO_FM_Cross_The_Line_Soundset", FALSE)
			ENDIF
		ELSE 
			IF ANIMPOSTFX_IS_RUNNING("CrossLine")
				PRINTLN("[RCC MISSION] ciOptionsBS21_CTF_SCREEN_FILTER_ON_HOLDING_FLAG - Stopping CrossLine VFX")
				ANIMPOSTFX_STOP("CrossLine")
			ENDIF
			
			IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_DO_DELIVER_FX)
				CLEAR_BIT(iLocalBoolCheck29, LBOOL29_DO_DELIVER_FX)
				ANIMPOSTFX_PLAY("CrossLineOut", 0, FALSE)
				PRINTLN("[CTLSFX] Stopping Local")
				IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_VFX_CAPTURE_OWNER)
					STRING sSoundSet 
					sSoundSet = "DLC_Apartments_Drop_Zone_Sounds"
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_HOSTILE_TAKEOVER_SOUNDS)
						sSoundSet = "dlc_xm_hota_Sounds"
					ENDIF
					PLAY_SOUND_FRONTEND(-1,"Exit_Capture_Zone",sSoundSet, FALSE)
				ELSE	
					PLAY_SOUND_FRONTEND(-1,"Player_Exit_Line","GTAO_FM_Cross_The_Line_Soundset", FALSE)
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
	
	SET_TEAMS_AS_PASSIVE()
	
	IF NOT IS_BIT_SET(iLocalBoolCheck29, LBOOL29_HAS_LAST_VEHICLE_BEEN_STORED)
	AND CONTENT_IS_USING_ARENA()
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			GET_LAST_VEHICLE_INFO(TRUE)
		ENDIF
	ENDIF
	
	
	IF NOT IS_BIT_SET(iLocalBoolCheck29, LBOOL29_ARENA_VEHICLE_WEAPON_SET)
	AND CONTENT_IS_USING_ARENA()
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			SET_CURRENT_PED_VEHICLE_WEAPON(PLAYER_PED_ID(), GET_ARENA_WARS_WEAPON_TYPE_FROM_VEHICLE(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))))
			PRINTLN("[RCC MISSION] Calling SET_CURRENT_PED_VEHICLE_WEAPON - Arena weapon set to to MG")
			SET_BIT(iLocalBoolCheck29, LBOOL29_ARENA_VEHICLE_WEAPON_SET)
		ENDIF
	ENDIF
	
	IF CONTENT_IS_USING_ARENA()
		PROCESS_SHOULD_TURRET_BE_AVAILABLE()
		ARENA_CONTESTANT_TURRET_UPDATE(arenaTurretContext, MC_serverBD_3.arenaContestantTurretServer, bIsLocalPlayerHost)
		
		IF IS_LOCAL_SUDDEN_DEATH_ACTIVE()
			IF NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_UPPED_SD_TELEMETRY)
				SET_BIT(iLocalBoolCheck30, LBOOL30_UPPED_SD_TELEMETRY)
				g_sArena_Telemetry_data.m_suddenDeath++
				PRINTLN("[ARENA][TEL] - Entered SD ", g_sArena_Telemetry_data.m_suddenDeath)
			ENDIF
		ENDIF
	ENDIF
	
	PROCESS_RAPPELLING_FROM_HELI()
	
	PROCESS_RAPPEL_DOWN_WALL()
	
	PROCESS_PLAYER_STOLEN_GOODS_SLOW_DOWN_MC()
	
	PROCESS_PLAYER_COP_DECOY()
	
	PROCESS_ELEVATORS()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_DisablePIMOutfitSwap)
		IF NOT IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_BLOCK_MASKS_AND_GEAR)
			SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_BLOCK_MASKS_AND_GEAR)
			PRINTLN("[RCC MISSION] PROCESS_EVERY_FRAME_PLAYER_CHECKS - Setting MPGlobalsAmbience.iAmbBitSet, iABI_BLOCK_MASKS_AND_GEAR")
		ENDIF
	ENDIF
	
ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: CROWD CONTROL MINIGAME !
//
//************************************************************************************************************************************************************



PROC MANAGE_MID_MISSION_CROWD_CONTROL_CUTSCENE()

	// Fail stuff
//------------------------------------------------------------------------------------------

	IF IS_BIT_SET_ENUM( MC_serverBD_3.sCCServerData.iBitset, CCDATA_Active )
	AND IS_BIT_SET_ENUM( MC_serverBD_3.sCCServerData.iBitset, CCDATA_Failed )
	#IF IS_DEBUG_BUILD
		AND NOT IS_BIT_SET_ENUM( MC_serverBD_3.sCCServerData.iBitset, CCDATA_DebugDisableFails )
	#ENDIF
	AND sCCLocalData.eFailReason != CCFAIL_NONE
	
		BOOL bFailMission
		IF sCCLocalData.eHeistType = CCHEIST_CUSTOM_FLEECA
			bFailMission = TRUE
		ELSE
			bFailMission = FALSE
		ENDIF
		
		IF NOT IS_BIT_SET( MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_CC_FAILCUT_FINISH )
		AND NOT IS_BIT_SET( MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_MID_MISSION_CUTSCENE )
	
			SWITCH sCCLocalData.eFailReason
			
				CASE CCFAIL_PEDKILLED
				CASE CCFAIL_PEDSKILLED
				CASE CCFAIL_PEDHEROALARM
				CASE CCFAIL_PEDHEROGUN
				CASE CCFAIL_FEARLOW
				CASE CCFAIL_TOOMANYKNOCKOUTS
				CASE CCFAIL_NOHITSREMAINING
		
				// Fail cutscene
				//------------------------------------------------------------------------------------------
					
					// Local update of fail cutscene
					IF NOT IS_BIT_SET_ENUM( sCCLocalData.iBitSet, CCLOCALDATA_StartedFailCutscene )

						INT iHashCompare
						TEXT_LABEL_63 tlFailCutscene
						CUTSCENE_SECTION eCutscenesection
						
						// Those who are not to seet the cutscene, mark them as finished.
						IF NOT SHOULD_PLAYER_PLAY_CROWD_CONTROL_CUTSCENE( LocalPlayerPed, sCCLocalData, MC_serverBD_4.iCurrentHighestPriority, MC_playerBD[iPartToUse].iteam )
							
							CPRINTLN( DEBUG_NET_MINIGAME, "MANAGE_MID_MISSION_CROWD_CONTROL_CUTSCENE() - FAIL - Should not play fail cutscene for this player" )
							
							iHashCompare = 0
							tlFailCutscene = ""
							
						ELSE
						
							iHashCompare = GET_CROWD_CONTROL_FAIL_CUTSCENE( 
								sCCLocalData.eHeistType, 
								MC_serverBD_1.sFMMC_SBD.niPed, 
								MC_serverBD_3.sCCServerData.iCrowdControlCloudIndex, 
								sCCLocalPedData, 
								MC_serverBD_3.sCCServerData.iNumCrowdControlPeds, 
								tlFailCutscene, eCutscenesection )
						
						ENDIF
						
						// Hashes are different, unload any current cutscene and load the new one.
						IF iHashCompare = 0
						OR sCCLocalData.iFailCutsceneHash != iHashCompare
						AND NOT IS_CUTSCENE_PLAYING()
						
							CPRINTLN( DEBUG_NET_MINIGAME, "MANAGE_MID_MISSION_CROWD_CONTROL_CUTSCENE() - FAIL - Cutscene hashes differ" )
										
							IF IS_CUTSCENE_ACTIVE()
								CPRINTLN( DEBUG_NET_MINIGAME, "MANAGE_MID_MISSION_CROWD_CONTROL_CUTSCENE() - FAIL - Cutscene was streaming, removing now" )
								//Reset_Cutscene_Ped_Variation_Streaming( sCrowdControlCashierVariation )
								REMOVE_CUTSCENE()
							ENDIF
							
							IF NOT IS_STRING_NULL_OR_EMPTY( tlFailCutscene )
							
								CPRINTLN( DEBUG_NET_MINIGAME, "MANAGE_MID_MISSION_CROWD_CONTROL_CUTSCENE() - FAIL - Cutscene string not empty, requesting new cutscene" )
													
								IF eCutscenesection = INT_TO_ENUM( CUTSCENE_SECTION, -1 )
									REQUEST_CUTSCENE( tlFailCutscene )
								ELSE
									REQUEST_CUTSCENE_WITH_PLAYBACK_LIST( tlFailCutscene, eCutscenesection )
								ENDIF
								
								
								PED_INDEX pedCutscne
								TEXT_LABEL_63 tlHandle
								INT iDummy
								
								IF GET_CROWD_CONTROL_PED_DETAILS_FOR_FAIL_CUTSCENE_FROM_HASH( 
									iHashCompare, 
									MC_serverBD_1.sFMMC_SBD.niPed,  
									MC_serverBD_3.sCCServerData.iCrowdControlCloudIndex, 
									sCCLocalPedData, 
									MC_serverBD_3.sCCServerData.iNumCrowdControlPeds,
									pedCutscne,
									tlHandle,
									iDummy )
									
									CPRINTLN( DEBUG_NET_MINIGAME, "MANAGE_MID_MISSION_CROWD_CONTROL_CUTSCENE() - FAIL - Requesting variations for cashier ped" )
								
									Register_Ped_Variation_For_Cutscene_From_Ped( sCrowdControlCashierVariation, pedCutscne, tlHandle )
																	
								ENDIF
								
								// Store new hash
								sCCLocalData.iFailCutsceneHash = iHashCompare
								
							ELSE
												
								// No cutscene to play, bail
								SET_BIT_ENUM( sCCLocalData.iBitSet, CCLOCALDATA_StartedFailCutscene )
								SET_BIT( MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_CC_FAILCUT_START )
								SET_BIT( MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_CC_FAILCUT_FINISH )								
								IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
									CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
									PRINTLN("[RCC MISSION] PROCESS_PRE_MAIN_CHECKS - (10) Clearing - PBBOOL_OBJECTIVE_BLOCKER")
								ENDIF
								CPRINTLN( DEBUG_NET_MINIGAME, "MANAGE_MID_MISSION_CROWD_CONTROL_CUTSCENE() - FAIL - No cutscene wanted any more, bail " )
								CPRINTLN( DEBUG_MISSION, "MANAGE_MID_MISSION_CROWD_CONTROL_CUTSCENE() - FAIL - No cutscene wanted any more, bail " )
							ENDIF
							
						// Hashes match, continue with loading and start if loaded
						ELSE
															
							// cutscene steaming
							IF IS_CUTSCENE_ACTIVE()
							AND HAS_CUTSCENE_LOADED()
							AND IS_TIME_MORE_THAN( GET_NETWORK_TIME(), sCCLocalData.timeFailDelay )
							
								CPRINTLN( DEBUG_NET_MINIGAME, "MANAGE_MID_MISSION_CROWD_CONTROL_CUTSCENE() - FAIL - Cutscene has loaded and timer expired, start the cutscene " )
								
								IF Is_MP_Objective_Text_On_Display()
									Clear_Any_Objective_Text()
								ENDIF
								
								SET_BIT( MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER )	
								SET_BIT( iLocalBoolCheck2, LBOOL2_CUTSCENE_STREAMED )
								SET_AUDIO_FLAG( "AllowPainAndAmbientSpeechToPlayDuringCutscene", TRUE )
								SET_BIT_ENUM( sCCLocalData.iBitSet, CCLOCALDATA_StartedFailCutscene )
							ENDIF
						
						ENDIF
				
					ENDIF
					
					Update_Cutscene_Prestreaming( sCrowdControlCashierVariation )
					
					// Run the actual cutscene
					IF IS_BIT_SET_ENUM( sCCLocalData.iBitSet, CCLOCALDATA_StartedFailCutscene )
													
						IF NOT IS_BIT_SET( MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_CC_FAILCUT_FINISH )
													
							TEXT_LABEL_63 tlCutsceneName
							CUTSCENE_SECTION eDummy
							IF GET_CROWD_CONTROL_FAIL_CUTSCENE_FROM_HASH( sCCLocalData.iFailCutsceneHash, tlCutsceneName, eDummy )
															
								tl63_CrowdControlCutscene = tlCutsceneName
								
								IF NOT IS_BIT_SET_ENUM( sCCLocalData.iBitSet, CCLOCALDATA_RaiseAlarms )
								AND IS_CUTSCENE_PLAYING()
								
									PED_INDEX pedDummy
									TEXT_LABEL_63 tlHandle
									INT iAnimEventHashAlarm
									
									IF GET_CROWD_CONTROL_PED_DETAILS_FOR_FAIL_CUTSCENE_FROM_HASH( 
										sCCLocalData.iFailCutsceneHash, 
										MC_serverBD_1.sFMMC_SBD.niPed,  
										MC_serverBD_3.sCCServerData.iCrowdControlCloudIndex, 
										sCCLocalPedData, 
										MC_serverBD_3.sCCServerData.iNumCrowdControlPeds,
										pedDummy,
										tlHandle,
										iAnimEventHashAlarm )
										
										IF DOES_CUTSCENE_ENTITY_EXIST( tlHandle )
											ENTITY_INDEX entityCutscene
											entityCutscene = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY( tlHandle )
											IF DOES_ENTITY_EXIST( entityCutscene )
											AND NOT IS_PED_INJURED( GET_PED_INDEX_FROM_ENTITY_INDEX( entityCutscene ) )
												IF HAS_ANIM_EVENT_FIRED( entityCutscene, iAnimEventHashAlarm )
													CPRINTLN( DEBUG_NET_MINIGAME, "MANAGE_MID_MISSION_CROWD_CONTROL_CUTSCENE() - FAIL - Alarm anim event alarm fired" )
													SET_BIT_ENUM( sCCLocalData.iBitSet, CCLOCALDATA_RaiseAlarms )
												ENDIF
											ENDIF
										ENDIF
																		
									ENDIF
								
									
								ENDIF
											
								// Run cutscene
								IF RUN_MOCAP_CUTSCENE(tlCutsceneName, FMMCCUT_GENERIC_MOCAP, FALSE, <<0,0,0>>, ciINVALID_CUTSCENE_WARP_HEADING, <<0,0,0>>, ciINVALID_CUTSCENE_WARP_HEADING, FALSE, TRUE, FALSE )
								
									CPRINTLN( DEBUG_NET_MINIGAME, "MANAGE_MID_MISSION_CROWD_CONTROL_CUTSCENE() - FAIL - Cutscene has finished." )
									
									CLEAR_BIT(iLocalBoolCheck5,LBOOL5_PED_INVOLVED_IN_CUTSCENE)
									IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
										CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
										PRINTLN("[RCC MISSION] PROCESS_PRE_MAIN_CHECKS - (9) Clearing - PBBOOL_OBJECTIVE_BLOCKER")
									ENDIF
									CLEAR_BIT( MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYING_CUTSCENE )
									
									SET_BIT( MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_CC_FAILCUT_START )
									SET_BIT( MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_CC_FAILCUT_FINISH )
									CPRINTLN( DEBUG_MISSION, "MANAGE_MID_MISSION_CROWD_CONTROL_CUTSCENE() - FAIL - Cutscene finished. " )
									IF NOT IS_BIT_SET_ENUM( sCCLocalData.iBitSet, CCLOCALDATA_RaiseAlarms )
										CPRINTLN( DEBUG_NET_MINIGAME, "MANAGE_MID_MISSION_CROWD_CONTROL_CUTSCENE() - FAIL - End of cutscene alarm fired" )
										SET_BIT_ENUM( sCCLocalData.iBitSet, CCLOCALDATA_RaiseAlarms )
									ENDIF
									
									SET_AUDIO_FLAG( "AllowPainAndAmbientSpeechToPlayDuringCutscene", FALSE )
									
									SET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS(MOCAPRUNNING_STAGE_0)
								ELSE
									
									IF NOT IS_BIT_SET( MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_CC_FAILCUT_START )
										IF IS_CUTSCENE_PLAYING()
											SET_BIT( MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_CC_FAILCUT_START )
										ENDIF
									ENDIF
																
									IF bFailMission
										IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
											IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration)

												IF IS_CUTSCENE_PLAYING()
												
													INT iCutTime
													iCutTime = GET_CUTSCENE_TIME()
													
	//												IF GET_CUTSCENE_TOTAL_DURATION() - iCutTime < 100
	//													SET_DRAW_BLACK_RECTANGLE_FOR_FADED_MOCAP(TRUE)
	//												ENDIF
													
													IF GET_CUTSCENE_TOTAL_DURATION() - iCutTime <= 450
														CPRINTLN( DEBUG_NET_MINIGAME, "MANAGE_MID_MISSION_CROWD_CONTROL_CUTSCENE() - FAIL - Celebration screen cutscene interrupt triggered @ ", iCutTime, "ms which is <= 450ms before the end" )
														DO_RENDER_PHASE_PAUSE_FOR_CELEBRATION_SCREEN()
														SET_BIT(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration)
														PRINTLN("[RCC MISSION] - [NETCELEBRATION] - [SAC] - ciCELEBRATION_BIT_SET_TriggerEndCelebration - call 4.")
													ENDIF
													
												ENDIF
											
											ENDIF
										ENDIF
										
									ENDIF
								ENDIF
							
							ELSE
								CPRINTLN( DEBUG_NET_MINIGAME, "MANAGE_MID_MISSION_CROWD_CONTROL_CUTSCENE() - FAIL - Unable to find cutscene details" )
							ENDIF
							
						ENDIF
					
					ENDIF	
			
				BREAK
				
				CASE CCFAIL_PEDHEROPHONE
					SET_BIT( MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_CC_FAILCUT_START )
					SET_BIT( MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_CC_FAILCUT_FINISH )
					CPRINTLN( DEBUG_MISSION, "MANAGE_MID_MISSION_CROWD_CONTROL_CUTSCENE() - FAIL - CCFAIL_PEDHEROPHONE. " )
				BREAK
		
			ENDSWITCH
		
		ENDIF // end has crowd control failed check#
	
	// Actual Fail Logic *HOST*
	//------------------------------------------------------------------------------------------
		
	// Server deals with issuing the fail.
		IF bIsLocalPlayerHost
		AND NOT IS_BIT_SET_ENUM( MC_serverBD_3.sCCServerData.iBitset, CCDATA_FailProcessed )

			BOOL bEveryoneReadyToFail = TRUE
			INT iPart
			REPEAT GET_MAXIMUM_PLAYERS_ON_MISSION() iPart
				
				IF NETWORK_IS_PARTICIPANT_ACTIVE( INT_TO_PARTICIPANTINDEX( iPart ) )
					// Wait for everyone to have at least started the cutscene before we start the actual fail
					IF NOT IS_BIT_SET( MC_playerBD[iPart].iClientBitSet2, PBBOOL2_CC_FAILCUT_START )
					//OR ( NOT bFailMission AND NOT IS_BIT_SET( MC_playerBD[iPart].iClientBitSet, PBBOOL_CC_FAILCUT_FINISH ) )
						CPRINTLN( DEBUG_NET_MINIGAME, "MANAGE_MID_MISSION_CROWD_CONTROL_CUTSCENE() - FAIL - bEveryoneReadyToFail set to FALSE - iPart[", iPart, "]" )
						CPRINTLN( DEBUG_NET_MINIGAME, "MANAGE_MID_MISSION_CROWD_CONTROL_CUTSCENE() - FAIL			PBBOOL2_CC_FAILCUT_START = ", IS_BIT_SET( MC_playerBD[iPart].iClientBitSet2, PBBOOL2_CC_FAILCUT_START ) )
						CPRINTLN( DEBUG_NET_MINIGAME, "MANAGE_MID_MISSION_CROWD_CONTROL_CUTSCENE() - FAIL			PBBOOL_CC_FAILCUT_FINISH = ", IS_BIT_SET( MC_playerBD[iPart].iClientBitSet, PBBOOL_CC_FAILCUT_FINISH ) )
						bEveryoneReadyToFail = FALSE
					ENDIF
				ENDIF
				
			ENDREPEAT
				
			// All finished the cutscene, process objective fail
			IF bEveryoneReadyToFail
			
				CPRINTLN( DEBUG_NET_MINIGAME, "MANAGE_MID_MISSION_CROWD_CONTROL_CUTSCENE() - Everyone ready to fail" )
			
				// 2157439, process setting the next mission and setting the mocap when the mission fails
				INT iPedToUse = MC_serverBD_3.sCCServerData.iCrowdControlCloudIndex[0]
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedToUse].sEndMissionData.iNextMissionToPlayFail > 0
					MC_serverBD.iNextMission = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedToUse].sEndMissionData.iNextMissionToPlayFail					
					CPRINTLN( DEBUG_NET_MINIGAME, "MANAGE_MID_MISSION_CROWD_CONTROL_CUTSCENE() - FAIL - Crowd control fail, set next mission: ",MC_serverBD.iNextMission)
				ENDIF
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedToUse].sEndMissionData.iEndFailMocap >0
					MC_serverBD.iEndMocapVariation = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedToUse].sEndMissionData.iEndFailMocap
					CPRINTLN( DEBUG_NET_MINIGAME, "MANAGE_MID_MISSION_CROWD_CONTROL_CUTSCENE() - FAIL - Crowd control fail, set mocap variation: ",MC_serverBD.iEndMocapVariation)
				ENDIF
								
				INT iTeam, iCrowdID
				FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
					
					// Do a full fail
					IF bFailMission
					
						CPRINTLN( DEBUG_NET_MINIGAME, "MANAGE_MID_MISSION_CROWD_CONTROL_CUTSCENE() - FAIL - Team = ", iTeam, " - Doing hard fail" )
					
						
						SWITCH sCCLocalData.eFailReason
							CASE CCFAIL_PEDKILLED
								SET_TEAM_FAILED(iTeam,mFail_CC_PED_DEAD)	
							BREAK
							CASE CCFAIL_PEDSKILLED
								SET_TEAM_FAILED(iTeam,mFail_CC_PEDS_DEAD)
							BREAK
							CASE CCFAIL_PEDHEROGUN		
								SET_TEAM_FAILED(iTeam,mFail_CC_PED_HERO_GUN)
							BREAK
							CASE CCFAIL_PEDHEROPHONE	
								SET_TEAM_FAILED(iTeam,mFail_CC_PED_HERO_PHONE)
							BREAK
							CASE CCFAIL_PEDHEROALARM
							CASE CCFAIL_FEARLOW
							CASE CCFAIL_TOOMANYKNOCKOUTS
							CASE CCFAIL_NOHITSREMAINING
								SET_TEAM_FAILED(iTeam,mFail_CC_PED_HERO_ALARM)
							BREAK
						ENDSWITCH
						
						MC_serverBD.iEntityCausingFail[iTeam] = sCCLocalData.iFailCausedByPed
					ELSE
						SET_BIT(MC_serverBD.iServerBitSet2,SBBOOL2_HEIST_REWARD_SWAT_INVOLVED)
						CPRINTLN( DEBUG_NET_MINIGAME, "MANAGE_MID_MISSION_CROWD_CONTROL_CUTSCENE() - FAIL - Team = ", iTeam, " - Doing soft fail (move to next objective)" )
					ENDIF
					
					FOR iCrowdID = 0 TO MC_serverBD_3.sCCServerData.iNumCrowdControlPeds -1
					
						INT iPed = MC_serverBD_3.sCCServerData.iCrowdControlCloudIndex[iCrowdID]
						IF MC_serverBD_4.iPedRule[iPed][iTeam] = FMMC_OBJECTIVE_LOGIC_CROWD_CONTROL
							PROGRESS_SPECIFIC_OBJECTIVE( ci_TARGET_PED, iPed, iTeam, FALSE )
						ENDIF
					 
					ENDFOR
					
					IF MC_serverBD_4.iPedMissionLogic[iTeam] = FMMC_OBJECTIVE_LOGIC_CROWD_CONTROL
						SET_TEAM_OBJECTIVE_OUTCOME( iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], FALSE )	
						SET_LOCAL_OBJECTIVE_END_MESSAGE( iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], MC_serverBD.iReasonForObjEnd[iTeam], bFailMission )
					ENDIF
						
				ENDFOR
				
				SET_BIT_ENUM( MC_serverBD_3.sCCServerData.iBitset, CCDATA_FailProcessed )
			
			ELSE
				CPRINTLN( DEBUG_NET_MINIGAME, "MANAGE_MID_MISSION_CROWD_CONTROL_CUTSCENE() - FAIL - Waiting on everyone to be ready before processing the fail." )
			ENDIF
			
		ENDIF
	
	ENDIF	

ENDPROC

PROC RUN_BACKGROUND_CC_PED_LOGIC( PED_INDEX tempPed, INT iped, INT iCrowdNum, CROWD_CONTROL_HEIST_TYPE eHeistType, BOOL bRestrictPeds, BOOL bIsInjured )
	
	IF ((g_iFMMCScriptedCutscenePlaying != -1) AND (g_iFMMCScriptedCutscenePlaying >= 2))
		//CONSTRAIN_PED_MOVEMENT_SPEED(tempPed,0,0)
	ELSE
					
		sCCLocalPedData[iCrowdNum].iUpdateTimer++
		
		
		// Initialise crowd control ped data
		// Wait for local data cache first
		IF IS_BIT_SET_ENUM( sCCLocalData.iBitSet, CCLOCALDATA_Initialised )
			IF NOT IS_BIT_SET_ENUM( sCCLocalPedDecorCopy[iCrowdNum].iBitSet, CCPEDDATA_HasBeenInitialised )
			
				IF SETUP_CROWD_CONTROL_PED_DATA( tempPed, sCCLocalPedDecorCopy[iCrowdNum], sCCLocalPedData[iCrowdNum], eHeistType )
					SET_BIT_ENUM( sCCLocalPedDecorCopy[iCrowdNum].iBitSet, CCPEDDATA_HasBeenInitialised )
					CPRINTLN( DEBUG_NET_MINIGAME, "RUN_BACKGROUND_CC_PED_LOGIC() - CrowdPed[", iCrowdNum, "] initialised" )		
				ENDIF
					
			ENDIF
		ENDIF
		
		IF IS_BIT_SET_ENUM( sCCLocalPedDecorCopy[iCrowdNum].iBitSet, CCPEDDATA_HasBeenInitialised )
		AND IS_BIT_SET_ENUM( sCCLocalPedData[iCrowdNum].iBitSet, CCLOCALPEDDATA_LocallyInitialised )
		
			PROCESS_CROWD_CONTROL_PED_AI( tempPed, MC_serverBD_3.sCCServerData, sCCLocalData, 
				sCCLocalPedDecorCopy[iCrowdNum], sCCLocalPedData[iCrowdNum], iPed, iCrowdNum, bRestrictPeds, bIsInjured )
				
		ENDIF

	ENDIF

ENDPROC

PROC RUN_CC_PED_BODY(PED_INDEX tempPed, INT iPed, INT iCrowdNum, CROWD_CONTROL_HEIST_TYPE eHeistType, BOOL bRestrictPeds, BOOL bIsOwned, BOOL bIsInjured )
		
	//We're running a crowd control, so add this guy/gal into the server
	IF NOT IS_BIT_SET(MC_serverBD_3.sCCServerData.iPedIsNowInvolvedInCrowdControl[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
		IF bIsLocalPlayerHost
			SET_BIT(MC_serverBD_3.sCCServerData.iPedIsNowInvolvedInCrowdControl[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
			IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[iPartToUse].iTeam],g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPriority[MC_playerBD[iPartToUse].iTeam])
				PRINTLN("[RCC MISSION] RUN_CC_PED_BODY - Adding crowd ped to crowd control & setting midpoint iObjectiveMidPointBitset for team ",MC_playerBD[iPartToUse].iTeam,", rule ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPriority[MC_playerBD[iPartToUse].iTeam])
				SET_BIT(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[iPartToUse].iTeam],g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPriority[MC_playerBD[iPartToUse].iTeam])
				SET_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
			ENDIF
		ELSE
			BROADCAST_FMMC_PED_ADDED_TO_CC(iPed,MC_playerBD[iPartToUse].iTeam)
		ENDIF
	ENDIF
			
	IF IS_BIT_SET(MC_serverBD_3.sCCServerData.iPedIsNowInvolvedInCrowdControl[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))			
		
		IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iPedCrowdControlBits[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
			IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed,tempPed) < ciCC_Range
				IF iSpectatorTarget = -1
					PRINTLN("[RCC MISSION] RUN_CC_PED_BODY - Crowd Control ped ",iPed," triggered")
					//timer starts
					//iCCDefaultStartTimer = GET_GAME_TIMER()
					SET_BIT(MC_playerBD[iPartToUse].iPedCrowdControlBits[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
				ENDIF
			ENDIF
		ENDIF
		
		
		IF IS_BIT_SET(MC_playerBD[iPartToUse].iPedCrowdControlBits[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
			
			IF bIsOwned
				
				RUN_BACKGROUND_CC_PED_LOGIC(tempPed, iPed, iCrowdNum, eHeistType, bRestrictPeds, bIsInjured )
				
				// Update (if we are allowed) the ped decorators and reset the timer
				IF CAN_WE_UPDATE_CROWD_CONTROL_DATA( tempPed, sCCLocalPedDecorCopy[iCrowdNum], sCCLocalPedData[iCrowdNum].iUpdateTimer )
				
					SET_CROWD_CONTROL_PED_DECORATORS( tempPed, sCCLocalPedDecorCopy[iCrowdNum] )
					sCCLocalPedData[iCrowdNum].iUpdateTimer = 0
					
					CPRINTLN( DEBUG_NET_MINIGAME, "RUN_CC_PED_BODY() - ", GET_CROWD_CONTROL_PED_NAME_FROM_ANIM_ID( sCCLocalPedData[iCrowdNum].eCreatorAssignedPedID ),
						"[", iCrowdNum, "] Updated ped decorator data" )
					
				ENDIF
				
			ENDIF
			
			// Spawn local props
			// Wait for local data to be cached first
			IF IS_BIT_SET_ENUM( sCCLocalData.iBitSet, CCLOCALDATA_Initialised )
				
				IF NOT IS_BIT_SET_ENUM( sCCLocalPedData[iCrowdNum].iBitSet, CCLOCALPEDDATA_LocallyInitialised )
					IF SETUP_CROWD_CONTROL_PED_LOCAL_PROPS( tempPed, sCCLocalPedData[iCrowdNum], eHeistType )
						CPRINTLN( DEBUG_NET_MINIGAME, "RUN_CC_PED_BODY() - CrowdPed[", iCrowdNum, "] local props initialised" )	
						SET_BIT_ENUM( sCCLocalPedData[iCrowdNum].iBitSet, CCLOCALPEDDATA_LocallyInitialised )
					ENDIF
				ENDIF
					
			ENDIF
			
		ENDIF
		
	ENDIF
	
ENDPROC

PROC PROCESS_CROWD_CONTROL_MINIGAME()

	#IF IS_DEBUG_BUILD
		
		BOOL bDebugChange
		IF IS_DEBUG_KEY_JUST_PRESSED( KEY_HOME, KEYBOARD_MODIFIER_CTRL, "Close Crowd control debug." )
			
			sCCLocalData.eDebugDisplay = CCDEBUG_OFF
			bDebugChange = TRUE
		
		ELIF IS_DEBUG_KEY_JUST_PRESSED( KEY_HOME, KEYBOARD_MODIFIER_SHIFT, "Cycle back through Crowd control debug." )
		
			sCCLocalData.eDebugDisplay = INT_TO_ENUM( CROWD_CONTROL_DEBUG_DISPLAY, ENUM_TO_INT( sCCLocalData.eDebugDisplay ) - 1 )
			IF sCCLocalData.eDebugDisplay <= CCDEBUG_OFF
				sCCLocalData.eDebugDisplay = INT_TO_ENUM( CROWD_CONTROL_DEBUG_DISPLAY, ENUM_TO_INT(  CCDEBUG_NUM_DEBUG ) - 1 )
			ENDIF
			bDebugChange = TRUE
	
		ELIF IS_DEBUG_KEY_JUST_PRESSED( KEY_HOME, KEYBOARD_MODIFIER_NONE, "Cycle through Crowd control debug." )
		
			sCCLocalData.eDebugDisplay = INT_TO_ENUM( CROWD_CONTROL_DEBUG_DISPLAY, ENUM_TO_INT( sCCLocalData.eDebugDisplay ) + 1 )
			IF sCCLocalData.eDebugDisplay >= CCDEBUG_NUM_DEBUG
				sCCLocalData.eDebugDisplay = CCDEBUG_OFF
			ENDIF
			bDebugChange = TRUE
			
		ENDIF
		
		IF bDebugChange
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE( sCCLocalData.eDebugDisplay != CCDEBUG_OFF )
		ENDIF

	#ENDIF

	INT iCrowdNum
	
// Cache some data from the cloud
//----------------------------------------------------------------------------------------------------

	IF NOT IS_BIT_SET_ENUM( sCCLocalData.iBitSet, CCLOCALDATA_Initialised )
	
		RESET_CROWD_CONTROL_LOCAL_DATA( sCCLocalData )
		
		CACHE_CROWD_CONTROL_LOCAL_DATA( sCCLocalData )
		
		// Collate and cache anim IDs to pass through
		INT iSeqCount
		FOR iCrowdNum = 0 TO (MC_serverBD_3.sCCServerData.iNumCrowdControlPeds-1)
		
			CACHE_CROWD_CONTROL_LOCAL_PED_DATA( 
				sCCLocalPedData[iCrowdNum],
				MC_serverBD_3.sCCServerData.iCrowdControlCloudIndex[iCrowdNum] )
				
			iSeqCount += CREATE_CROWD_CONTROL_PED_SEQUENCES( sCCLocalData, sCCLocalPedData[iCrowdNum] )
			
		ENDFOR
		
		CPRINTLN( DEBUG_NET_MINIGAME, "PROCESS_CROWD_CONTROL_MINIGAME() - Attempted to create ", iSeqCount, " sequences for crowd control peds." )
		
		SET_BIT_ENUM( sCCLocalData.iBitSet, CCLOCALDATA_Initialised )
	
	ENDIF
	
// Run the minigame main control function (mostly server side)
//----------------------------------------------------------------------------------------------------
		
	PROCESS_CROWD_CONTROL_MINIGAME_STATE(
			MC_serverBD_3.sCCServerData,			// Main minigame data (broadcast)
			sCCLocalData,							// Main minigame data (local)
			sCCLocalPedDecorCopy, 					// Per ped data (copy of the decorator data)
			sCCLocalPedData,						// Per ped data (local)
			
			// Pass throughs for look up purposes
			MC_serverBD_3.sCCServerData.iCrowdControlCloudIndex,	// Cached positions of crowd control peds in the global arrays
			MC_serverBD_3.sCCServerData.iNumCrowdControlPeds, 		// Cached number of crowd control peds
			MC_serverBD_1.sFMMC_SBD.niPed,							// Global ped array (for checking the dead cashiers for the cutscene)
			MC_serverBD_4.iCurrentHighestPriority,					// Each teams current priority
			MC_playerBD[iPartToUse].iteam
		)
	
	
	#IF IS_DEBUG_BUILD
		DISPLAY_CROWD_CONTROL_DEBUG(
			MC_serverBD_3.sCCServerData,
			sCCLocalData, 
			MC_playerBD[iPartToUse].iThreateningCrowdMember, 
			MC_serverBD_3.sCCServerData.iNumCrowdControlPeds, 
			MC_serverBD_4.iCurrentHighestPriority,
			MC_playerBD[iPartToUse].iteam )
	#ENDIF
	
	
// Easy Pass stuff
//------------------------------------------------------------------------------------------

	IF bIsLocalPlayerHost
		
		IF IS_BIT_SET_ENUM( MC_serverBD_3.sCCServerData.iBitset, CCDATA_EasyPassComplete )
		AND NOT IS_BIT_SET_ENUM( MC_serverBD_3.sCCServerData.iBitset, CCDATA_Failed )
		//AND NOT IS_BIT_SET( MC_serverBD_3.sCCServerData.iBitset, CCDATA_EasyPassProcessed )

			INT iTeam, iCrowdID
			FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
			
				// Team has crowd control rule
				// Is easy pass set on this rule for this team
				IF MC_serverBD_4.iPedMissionLogic[iTeam] = FMMC_OBJECTIVE_LOGIC_CROWD_CONTROL
				AND IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE_ADVANCE_ON_AIM )

					// Pass the objective for the peds
					FOR iCrowdID = 0 TO MC_serverBD_3.sCCServerData.iNumCrowdControlPeds -1
					
						INT iPed = MC_serverBD_3.sCCServerData.iCrowdControlCloudIndex[iCrowdID]
					
						IF MC_serverBD_4.iPedRule[iPed][iTeam] = FMMC_OBJECTIVE_LOGIC_CROWD_CONTROL
							PROGRESS_SPECIFIC_OBJECTIVE( ci_TARGET_PED, iPed, iTeam, TRUE )
						ENDIF
					 
					ENDFOR

					// Pass the objective for the team
					SET_TEAM_OBJECTIVE_OUTCOME( iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], TRUE )
					SET_LOCAL_OBJECTIVE_END_MESSAGE( iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], MC_serverBD.iReasonForObjEnd[iTeam], FALSE )
				
				ENDIF

			ENDFOR
			
			SET_BIT_ENUM( MC_serverBD_3.sCCServerData.iBitset, CCDATA_EasyPassProcessed )
			
		ENDIF
		
	ENDIF
	
// fail stuff was here

	IF HAS_PED_ENTERED_CROWD_CONTROL_TRIGGER_ZONE( LocalPlayerPed, sCCLocalData, TRUE )
		// Blocking was removed for bug 2195539
		// Has now been re-enabled for bug 2212801 as the start of the animations for the player hitting peds have the player standing
		// in the regular idle so the action mode poses won't match them.
		SET_PED_RESET_FLAG( LocalPlayerPed, PRF_DisableActionMode, TRUE )
		IF IS_PED_USING_ACTION_MODE( LocalPlayerPed )
			SET_PED_USING_ACTION_MODE( LocalPlayerPed, FALSE )
		ENDIF
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_DUCK )
		IF GET_PED_STEALTH_MOVEMENT( LocalPlayerPed )
			SET_PED_STEALTH_MOVEMENT( LocalPlayerPed, FALSE )
		ENDIF
		
		SET_BIT_ENUM( sCCLocalData.iBitSet, CCLOCALDATA_BlockingActionMode )
	ELSE	
		CLEAR_BIT_ENUM( sCCLocalData.iBitSet, CCLOCALDATA_BlockingActionMode )
	ENDIF
	
// Process the beat downs (PROTOTYPE)
//----------------------------------------------------------------------------------------------------
	
	PROCESS_CROWD_CONTROL_BEATDOWNS(
		MC_serverBD_1.sFMMC_SBD.niPed,
		MC_serverBD_3.sCCServerData,
		sCCLocalData,
		sCCLocalPedDecorCopy,
		sCCLocalPedData )
		
		
// Process the ped's threats
//----------------------------------------------------------------------------------------------------
	
	PED_INDEX pedBeingBeat
	IF sCCLocalData.iPlayerBeatingThisPed != -1
	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID( MC_serverBD_1.sFMMC_SBD.niPed[MC_serverBD_3.sCCServerData.iCrowdControlCloudIndex[sCCLocalData.iPlayerBeatingThisPed]] )
		pedBeingBeat = NET_TO_PED( MC_serverBD_1.sFMMC_SBD.niPed[MC_serverBD_3.sCCServerData.iCrowdControlCloudIndex[sCCLocalData.iPlayerBeatingThisPed]] )
	ENDIF

	INT iCrowdPed
	FOR iCrowdPed = 0 TO MC_serverBD_3.sCCServerData.iNumCrowdControlPeds-1
		INT iPed = MC_serverBD_3.sCCServerData.iCrowdControlCloudIndex[iCrowdPed]
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID( MC_serverBD_1.sFMMC_SBD.niPed[iPed] )

			PED_INDEX tempPed = NET_TO_PED( MC_serverBD_1.sFMMC_SBD.niPed[iPed] )
			IF NOT IS_PED_INJURED( tempPed )
			
				// Determine the threat posed by this player
				CROWD_CONTROL_THREAT_TYPE eThreatNew = CCTHREAT_NONE
				IF IS_BIT_SET_ENUM( sCCLocalPedDecorCopy[iCrowdPed].iBitSet, CCPEDDATA_HasBeenInitialised )
				
					IF NOT IS_BIT_SET_ENUM( MC_serverBD_3.sCCServerData.iBitSet, CCDATA_Active )					
					AND ( HAS_PED_RECEIVED_EVENT( tempPed, EVENT_SHOT_FIRED )
					OR HAS_PED_RECEIVED_EVENT( tempPed, EVENT_SHOT_FIRED_BULLET_IMPACT )
					OR HAS_PED_RECEIVED_EVENT( tempPed, EVENT_SHOT_FIRED_WHIZZED_BY ) )
					
						eThreatNew = CCTHREAT_SHOOTING
				
					ELIF IS_CROWD_CONTROL_PLAYER_ABLE_TO_THREATEN_FROM_CURRENT_LOCATION( PLAYER_PED_ID(), sCCLocalData )

						IF NOT IS_BIT_SET_ENUM( MC_serverBD_3.sCCServerData.iBitSet, CCDATA_Active )
							IF IS_PLAYER_STARTING_CROWD_CONTROL_PED_REACTION( PLAYER_PED_ID(), sCCLocalData )
								eThreatNew = CCTHREAT_PROXIMITY
							ENDIF
						ENDIF
						IF eThreatNew = CCTHREAT_NONE
							eThreatNew = IS_PLAYER_THREATENING_CROWD_CONTROL_PED( PLAYER_ID(), PLAYER_PED_ID(),
										tempPed, 
										iCrowdPed,
										MC_serverBD_3.sCCServerData, 
										sCCLocalData, 
										sCCLocalPedDecorCopy, 
										sCCLocalPedData, 
										pedBeingBeat )
						ENDIF
					
					ENDIF
					
				ENDIF
				
				CROWD_CONTROL_THREAT_TYPE eThreatStored
				eThreatStored = INT_TO_ENUM( CROWD_CONTROL_THREAT_TYPE, GET_PACKED_BITFIELD_VALUE_FROM_ARRAY( MC_playerBD[iPartToUse].iThreateningCrowdMember, iCrowdPed, ciCC_ThreatTypeDataBitWidth) )
				
				IF eThreatNew > eThreatStored // jump immediately to the higher priority threat
				OR ( eThreatNew < eThreatStored	// if a lesser threat make sure that lesser threat is present for at least 1 second before dropping
				AND IS_TIME_MORE_THAN( GET_NETWORK_TIME(), GET_TIME_OFFSET( sCCLocalPedData[iCrowdPed].timeThreatUpdate, 1000 ) ) )

					// Store new threat
					SET_PACKED_BITFIELD_VALUE_IN_ARRAY_FROM_ENUM( MC_playerBD[iPartToUse].iThreateningCrowdMember, iCrowdPed, ciCC_ThreatTypeDataBitWidth, eThreatNew )
					sCCLocalPedData[iCrowdPed].timeThreatUpdate = GET_NETWORK_TIME()
				
				// Threat has remained the same, reset the timer
				ELIF eThreatNew = eThreatStored
				
					sCCLocalPedData[iCrowdPed].timeThreatUpdate = GET_NETWORK_TIME()
					
				ENDIF
			
				// HOST & OWNER ONLY: Work out this peds biggest threat
				IF bIsLocalPlayerHost
				OR NETWORK_HAS_CONTROL_OF_NETWORK_ID( MC_serverBD_1.sFMMC_SBD.niPed[iPed] )
					
					///I want to be able to pass in the closest player threatening the ped:
					FLOAT fClosestDist2 = 999999999
					INT iPartLoop
					INT iClosestPart = -1
					CROWD_CONTROL_THREAT_TYPE eBiggestThreat = CCTHREAT_NONE
					
					INT iTotalPlayers = 0
					
					FOR iPartLoop = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartLoop))
						
							PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartLoop))
						 	IF NOT IS_PLAYER_SPECTATOR_ONLY( tempPlayer )
							
								iTotalPlayers++
							
								CROWD_CONTROL_THREAT_TYPE eThreat = INT_TO_ENUM( CROWD_CONTROL_THREAT_TYPE, GET_PACKED_BITFIELD_VALUE_FROM_ARRAY( MC_playerBD[iPartLoop].iThreateningCrowdMember, iCrowdPed, ciCC_ThreatTypeDataBitWidth ) )
									
								IF eThreat != CCTHREAT_NONE
									
									IF IS_NET_PLAYER_OK(tempPlayer)
									
										// Bigger or equal threat to the ped
										IF eThreat >= eBiggestThreat

											FLOAT fDist2 = VDIST2(GET_PLAYER_COORDS(tempPlayer),GET_ENTITY_COORDS(tempPed, FALSE))
											
											IF eThreat > eBiggestThreat		// this player is a bigger threat
											OR fDist2 < fClosestDist2		// or must be equal threat, check if closer
												eBiggestThreat = eThreat
												fClosestDist2 = fDist2
												iClosestPart = iPartLoop
											ENDIF
										
										ENDIF
										
									ENDIF
								ENDIF
							
							ENDIF
						ENDIF
						
						IF iTotalPlayers >= (MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3])
							//Break out of the loop, as we've checked everybody playing already
							iPartLoop = MAX_NUM_MC_PLAYERS
						ENDIF
					ENDFOR
					
					
					IF iClosestPart != -1
						//Somebody is threatening them!
						sCCLocalPedData[iCrowdPed].eBiggestThreat = eBiggestThreat
						sCCLocalPedData[iCrowdPed].pedBiggestThreat = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iClosestPart)))
					ELSE
					
						//Nobody is threatening them, just get the closest player
						sCCLocalPedData[iCrowdPed].eBiggestThreat = CCTHREAT_NONE

						PLAYER_INDEX threatPlayerID = INT_TO_PLAYERINDEX(GET_NEAREST_PLAYER_TO_ENTITY(tempPed))
						IF (threatPlayerID != INVALID_PLAYER_INDEX())
							sCCLocalPedData[iCrowdPed].pedBiggestThreat = GET_PLAYER_PED(threatPlayerID)
						ELSE
							sCCLocalPedData[iCrowdPed].pedBiggestThreat = PLAYER_PED_ID()
						ENDIF
												
					ENDIF
					
				ENDIF
			
			ENDIF
		ENDIF
	
	ENDFOR

// Process updating the ped's contribution to the fear level and determine each peds biggest threat
//----------------------------------------------------------------------------------------------------

	PROCESS_CROWD_CONTROL_FEAR( MC_serverBD_3.sCCServerData, 
		sCCLocalData,
		MC_serverBD_1.sFMMC_SBD.niPed,
		sCCLocalPedDecorCopy,
		sCCLocalPedData, 
		IS_BIT_SET_ENUM( MC_serverBD_3.sCCServerData.iBitset, CCDATA_Restricted ) )
	
// Process the per ped AI state control
//----------------------------------------------------------------------------------------------------

	INT iPackedDialogueConversations[2]
	INT iPackedDialoguePainFlags
	INT iPackedDialogueCriticalFlags
	
	FOR iCrowdNum = 0 TO (MC_serverBD_3.sCCServerData.iNumCrowdControlPeds-1)
		
		INT iPed = MC_serverBD_3.sCCServerData.iCrowdControlCloudIndex[iCrowdNum]
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID( MC_serverBD_1.sFMMC_SBD.niPed[iPed] )
	
			BOOL bOwnThisPed = NETWORK_HAS_CONTROL_OF_NETWORK_ID( MC_serverBD_1.sFMMC_SBD.niPed[iPed] )
			
			IF MC_serverBD_4.iPedRule[iped][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_CROWD_CONTROL
			//AND MC_serverBD_4.iPedPriority[iped][MC_playerBD[iPartToUse].iteam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] )
			OR IS_BIT_SET(MC_serverBD_3.sCCServerData.iPedIsNowInvolvedInCrowdControl[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			
				PED_INDEX tempPed = NET_TO_PED( MC_serverBD_1.sFMMC_SBD.niPed[iPed] )
				BOOL bIsPedInjured = IS_PED_INJURED( tempPed )
			
				// Keep the local copy up to date 
				// the first frame after a ped migrates we need to pull the decor data and store it locally 
				// to make sure that it is the very latest data after a migration.
				IF NOT bOwnThisPed
				OR NOT IS_BIT_SET_ENUM( sCCLocalPedData[iCrowdNum].iBitSet, CCLOCALPEDDATA_OwnThisPed )
				
					#IF IS_DEBUG_BUILD
						IF ( NOT bOwnThisPed 	AND IS_BIT_SET_ENUM( sCCLocalPedData[iCrowdNum].iBitSet, CCLOCALPEDDATA_OwnThisPed ) )
						OR ( bOwnThisPed 		AND NOT IS_BIT_SET_ENUM( sCCLocalPedData[iCrowdNum].iBitSet, CCLOCALPEDDATA_OwnThisPed ) )
					
							CPRINTLN( DEBUG_NET_MINIGAME, "PROCESS_CROWD_CONTROL_MINIGAME() - ", GET_CROWD_CONTROL_PED_NAME_FROM_ANIM_ID( sCCLocalPedData[iCrowdNum].eCreatorAssignedPedID ),
								"[", iCrowdNum, "] - Retrieved Decorator data from ped.")
								
							IF bOwnThisPed
								CPRINTLN( DEBUG_NET_MINIGAME, "PROCESS_CROWD_CONTROL_MINIGAME()		... because we have just taken ownership." )
							ELSE
								CPRINTLN( DEBUG_NET_MINIGAME, "PROCESS_CROWD_CONTROL_MINIGAME()		... because we just lost ownership(will update each from from now on)." )
							ENDIF
							
						ENDIF
					#ENDIF

					IF bOwnThisPed
						SET_BIT_ENUM( sCCLocalPedData[iCrowdNum].iBitSet, CCLOCALPEDDATA_OwnThisPed )
					ELSE
						CLEAR_BIT_ENUM( sCCLocalPedData[iCrowdNum].iBitSet, CCLOCALPEDDATA_OwnThisPed )
					ENDIF
					
					sCCLocalPedDecorCopy[iCrowdNum] = GET_CROWD_CONTROL_PED_DECORATOR_DATA( tempPed )
					
				ENDIF

				RUN_CC_PED_BODY(tempPed,
					iped, 
					iCrowdNum, 
					sCCLocalData.eHeistType, 
					IS_BIT_SET_ENUM( MC_serverBD_3.sCCServerData.iBitset, CCDATA_Restricted ),
					bOwnThisPed,
					bIsPedInjured)
				
				IF NOT bIsPedInjured
				
					PROCESS_CROWD_CONTROL_PED_LOCAL(
						tempPed, 
						iCrowdNum,
						sCCLocalPedDecorCopy[iCrowdNum],
						sCCLocalPedData[iCrowdNum],
						MC_serverBD_3.sCCServerData,
						sCCLocalData )
						
					// Update this peds current dialogue state
					IF NOT IS_ANY_SPEECH_PLAYING( tempPed )
						sCCLocalPedData[iCrowdNum].eDialogueCurrentlyPlaying = CCDIA_NONE
					ENDIF
						
					// Queue up everything this machine owns, and send it to the host
					PROCESS_CROWD_CONTROL_DIALOGUE_CLIENT_TO_HOST_QUEUE(
						iCrowdNum,
						sCCLocalData,
						sCCLocalPedData[iCrowdNum],
						iPackedDialogueConversations,
						iPackedDialoguePainFlags,
						iPackedDialogueCriticalFlags)
						
					// Each client receives what it is to actually play on each ped locally
					PROCESS_CROWD_CONTROL_DIALOGUE_HOST_TO_CLIENT_QUEUE( 
						tempPed,
						sCCLocalPedData[iCrowdNum],
						sCCLocalData )
						
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
			DISPLAY_CROWD_CONTROL_PED_DEBUG( 
				NET_TO_PED( MC_serverBD_1.sFMMC_SBD.niPed[iped] ), 
				MC_serverBD_3.sCCServerData, 
				sCCLocalData,
				sCCLocalPedDecorCopy[iCrowdNum], 
				sCCLocalPedData[iCrowdNum],
				iCrowdNum,
				bOwnThisPed )
			#ENDIF
			
		ENDIF
		
	ENDFOR

	// Send the data to the host (only if there is something to send)
	IF iPackedDialogueConversations[0] != 0
	OR iPackedDialogueConversations[1] != 0
		
		IF NOT bIsLocalPlayerHost
		
			BROADCAST_FMMC_CC_DIALOGUE_CLIENT_TO_HOST( iPackedDialogueConversations, iPackedDialoguePainFlags, iPackedDialogueCriticalFlags )
			#IF IS_DEBUG_BUILD
				IF IS_BIT_SET_ENUM( sCCLocalData.iBitset, CCLOCALDATA_DebugLogDialogue )
					CDEBUG1LN( DEBUG_NET_MINIGAME, "PROCESS_CROWD_CONTROL_DIALOGUE_CLIENT_TO_HOST_QUEUE() - Not the host, called BROADCAST_FMMC_CC_DIALOGUE_CLIENT_TO_HOST()" )
				ENDIF
			#ENDIF
		
		ELSE
//			CPRINTLN( DEBUG_NET_MINIGAME, "PROCESS_CROWD_CONTROL_DIALOGUE_CLIENT_TO_HOST_QUEUE() - Would normally need to broadcast, but this is the Host" )
		ENDIF
	ENDIF
	
	
	
// Process the crowd dialogue and ambient speech
//----------------------------------------------------------------------------------------------------

	IF IS_BIT_SET_ENUM( MC_serverBD_3.sCCServerData.iBitSet, CCDATA_Active )
		
	// Host decides what can play and whats ignored and sends it back to the clients
		IF bIsLocalPlayerHost

			PROCESS_CROWD_CONTROL_DIALOGUE_SELECTION(
				MC_serverBD_1.sFMMC_SBD.niPed,
				MC_ServerBD_3.sCCServerData,
				sCCLocalData,
				sCCLocalPedData,
				MC_serverBD_3.sCCServerData.iCrowdControlCloudIndex,
				MC_serverBD_3.sCCServerData.iNumCrowdControlPeds )
			
		ENDIF

	ENDIF
	
// Process the HUD - always call, will decide internally whether to draw or not
//----------------------------------------------------------------------------------------------------

	PROCESS_CROWD_CONTROL_MINIGAME_HUD( 
		MC_serverBD_3.sCCServerData,  
		sCCLocalData,
		MC_serverBD_4.iPedMissionLogic[MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_CROWD_CONTROL )
		
ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: GENERIC MAIN STATE FUNCTIONS !
//
//************************************************************************************************************************************************************



PROC EVERY_FRAME_PRE_ENTITY_PROCESSING()

INT iRule, iTeam
	
	// reset local player player carry countF
	iMyPedCarryCount = 0
	
	iInAnyLocTemp = -1
	
	iAHighPriorityVehicle = -1
	iTempCaptureLocation = -1
	
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		iTempPriorityLocation[iTeam] = -1
		
		
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
			iFirstHighPriorityVehicleThisRule[iTeam] = -1
		ENDIF
	ENDFOR
	
	//handle opening doors
	FOR iRule = 0 TO (FMMC_MAX_RULES-1)
		IF NOT IS_BIT_SET(iLocalHackDoorBitset,iRule)
		OR NOT IS_BIT_SET(iLocalHackDoorBitset2,iRule)
			IF IS_BIT_SET(MC_serverBD.iHackDoorOpenBitset,iRule)
			OR (IS_BIT_SET(MC_serverBD.iHackDoorThermiteDamageBitset, iRule) AND NOT IS_BIT_SET(iLocalHackDoorThermiteBitset, iRule))
				IF iHackDoorID[iRule] > -1 AND iHackDoorID2[iRule] = -1
					IF OPEN_HACK_DOOR(iRule,iHackDoorID[iRule], IS_BIT_SET(MC_serverBD.iHackDoorBlownBitset,iRule), IS_BIT_SET(iDoor1Swingfree, iRule), 
									  fHackDoorOpenRatio[iRule], IS_BIT_SET(MC_serverBD.iHackDoorThermiteDamageBitset,iRule))
						PRINTLN("[RCC MISSION] OPENING DOOR: ",iHackDoorID[iRule]," for RULE: ",iRule) 
						SET_BIT(iLocalHackDoorBitset,iRule)
						SET_BIT(iLocalHackDoorBitset2,iRule)
					ENDIF
				ELIF iHackDoorID[iRule] > -1 AND iHackDoorID2[iRule] > -1
					IF NOT IS_BIT_SET(iLocalHackDoorBitset,iRule)
						IF OPEN_HACK_DOOR(iRule,iHackDoorID[iRule], IS_BIT_SET(MC_serverBD.iHackDoorBlownBitset,iRule), IS_BIT_SET(iDoor1Swingfree, iRule), 
										  fHackDoorOpenRatio[iRule], IS_BIT_SET(MC_serverBD.iHackDoorThermiteDamageBitset,iRule))
							PRINTLN("[RCC MISSION] OPENING 2 DOORS 1st: ",iHackDoorID[iRule]," and door 2: ",iHackDoorID2[iRule]," for RULE: ",iRule) 
							SET_BIT(iLocalHackDoorBitset,iRule)
						ENDIF
					ENDIF
					IF NOT IS_BIT_SET(iLocalHackDoorBitset2,iRule)
						IF OPEN_HACK_DOOR(iRule,iHackDoorID2[iRule], IS_BIT_SET(MC_serverBD.iHackDoorBlownBitset,iRule), IS_BIT_SET(iDoor2Swingfree, iRule), 
										  fHackDoor2OpenRatio[iRule], IS_BIT_SET(MC_serverBD.iHackDoorThermiteDamageBitset,iRule))
							SET_BIT(iLocalHackDoorBitset2,iRule)
							PRINTLN("[RCC MISSION] OPENING 2 DOORS 2nd: ",iHackDoorID[iRule]," and door 2: ",iHackDoorID2[iRule]," for RULE: ",iRule) 
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	IF NOT bDoorsInited
		SET_DOOR_START_STATE()
	ELIF MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
		FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
			IF iDoorPriority[iteam] != MC_ServerBD_4.iCurrentHighestPriority[iteam]
				PRINTLN("UPDATE DOOR STATE LOOP")
				IF MC_ServerBD_4.iCurrentHighestPriority[iteam] = FMMC_PRIORITY_IGNORE
					iDoorPriority[iteam] = MC_ServerBD_4.iCurrentHighestPriority[iteam]
				ELif UPDATE_DOOR_STATE(iteam,MC_ServerBD_4.iCurrentHighestPriority[iteam])
					iDoorPriority[iteam] = MC_ServerBD_4.iCurrentHighestPriority[iteam]
				endif
			ELIF bShouldCheckAnyDoorsEveryFrame
				PRINTLN("UPDATE DOOR STATE LOOP for Doors that update every frame.")
				UPDATE_DOOR_STATE(iteam, MC_ServerBD_4.iCurrentHighestPriority[iteam])
			ENDIF
		ENDFOR
	ENDIF
	
	IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_FINISHED)
	OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_START_SPECTATOR)
		MC_playerBD[iLocalPart].iObjectiveTypeCompleted =-1
	ENDIF
	
	IF MC_playerBD[iLocalPart].iObjectiveTypeCompleted =-1
		MC_playerBD[iLocalPart].iCurrentLoc = -1
		MC_playerBD[iLocalPart].iLeaveLoc =-1
		MC_playerBD[iLocalPart].iPedNear = -1
		MC_playerBD[iLocalPart].iVehNear = -1
		MC_playerBD[iLocalPart].iObjNear = -1
		PRINTLN("[RCC MISSION] EVERY_FRAME_PRE_ENTITY_PROCESSING - MC_playerBD[iLocalPart].iObjectiveTypeCompleted = -1, resetting iCurrentLoc, iLeaveLoc, iPedNear, iVehNear, iObjNear")
	ENDIF
	
	// reset local player vehicle carry count
	iMyVehicleCarryCount = 0
	
	// reset local player object carry count
	iMyObjectCarryCount = 0
	
	iLowestLeaveLocationPriority = FMMC_PRIORITY_IGNORE
	
	CLEAR_BIT(iLocalBoolCheck6,LBOOL6_IN_MY_PRIORITY_LEAVE_LOC)
	CLEAR_BIT(iLocalBoolCheck12, LBOOL12_SERVER_STILL_THINKS_IM_HACKING_TEMP)
	
	IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iPartToUse].iteam)
		iVehTargetingCheckedThisRuleBS = 0 //Recheck all vehicle targeting!
		CLEAR_BIT(iLocalBoolCheck6, LBOOL6_DISABLECONTROLTIMER_COMPLETE)
		RESET_NET_TIMER(tdDisableControlTimer)
	ENDIF
	
	CLEAR_BIT(iLocalBoolCheck13, LBOOL13_DRAW_DISTANCE_TO_CROSS_LINE)
	CLEAR_BIT(iLocalBoolCheck13, LBOOL13_PLAYERINPEDBLOCKINGZONE)
	
	CLEAR_BIT(iLocalBoolCheck18, LBOOL18_TEAM_MEMBER_REQDIST_PASSED_THIS_FRAME)
	CLEAR_BIT(iLocalBoolCheck18, LBOOL18_TEAM_MEMBER_REQDIST_CHECKED_THIS_FRAME)
	fTeamMembers_ReqDist = 0.0
	
	IF bIsLocalPlayerHost

		FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
			MC_serverBD.iNumVehSeatsHighestPriority[iTeam] = 0
		ENDFOR
		
		IF iLocalTrevorBodhiBreakoutSynchSceneID != -1
		AND MC_serverBD_1.iTrevorBodhiBreakoutSynchSceneID != iLocalTrevorBodhiBreakoutSynchSceneID
			MC_serverBD_1.iTrevorBodhiBreakoutSynchSceneID = iLocalTrevorBodhiBreakoutSynchSceneID
		ENDIF
		
		IF iLocalGOFoundryHostageSynchSceneID != -1
		AND MC_serverBD_1.iGOFoundryHostageSynchSceneID != iLocalGOFoundryHostageSynchSceneID
			MC_serverBD_1.iGOFoundryHostageSynchSceneID = iLocalGOFoundryHostageSynchSceneID
			PRINTLN("[RCC MISSION][HostageSync] EVERY_FRAME_PRE_ENTITY_PROCESSING - Set host iGOFoundryHostageSynchSceneID as ",iLocalGOFoundryHostageSynchSceneID)
		ENDIF
		
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_DISABLE_TEAM_SPECTATE()
	IF AM_I_ON_A_HEIST()
	OR IS_PHONE_ONSCREEN()
	OR USING_HEIST_SPECTATE()
	OR IS_PED_DEAD_OR_DYING(LocalPlayerPed)
	OR HAS_NET_TIMER_STARTED( MC_playerBD[ iPartToUse ].tdBoundstimer )
	OR HAS_NET_TIMER_STARTED( MC_playerBD[ iPartToUse ].tdBounds2timer )
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciDISABLE_SPECTATOR_CAMERA)
	OR GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH( "appinternet" ) ) != 0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_ALLOW_TEAM_SPECTATE_THIS_MODE()
	INT iTeam = MC_playerBD[iLocalPart].iteam
	INT iPriority = MC_ServerBD_4.iCurrentHighestPriority[ iTeam ]
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciOVERTIME_WAIT_YOUR_TURN)
		IF iOTPartToMove = iLocalPart
			RETURN FALSE
		ENDIF
	ENDIF
	
	BOOL bFinishedLaps = FALSE
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iMissionLapLimit > 0
		IF MC_ServerBD_4.iTeamLapsCompleted[ iTeam ] >= g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iMissionLapLimit
			bFinishedLaps = TRUE
		ENDIF
	ENDIF
	
	INT iCoronaRole = MC_playerBD[iLocalPart].iCoronaRole
	
	IF iPriority < FMMC_MAX_RULES
	AND NOT HAS_TEAM_FAILED( iTeam )
	AND GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitset[ iPriority ], ciBS_RULE_ENABLE_SPECTATE )
		AND NOT SHOULD_DISABLE_TEAM_SPECTATE()
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciOVERTIME_WAIT_YOUR_TURN)
			OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_TEAM_TURNS)
				RETURN TRUE
			ENDIF
		
			IF IS_LOADED_MISSION_LOW_RIDER_FLOW_MISSION()
				RETURN TRUE
			ENDIF
			
			IF iCoronaRole != -1
			AND (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRolesBitset[ iPriority ][ iCoronaRole ], ciBS_ROLE_Cant_Complete_This_Rule ) // this
			OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRolesBitset[ iPriority ][ iCoronaRole ], ciBS_ROLE_LAST_RULE_FOR_TEAMMATE_OVERRIDE ) ) // this
			AND (NOT bFinishedLaps) // this
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
	
ENDFUNC

PROC EVERY_FRAME_POST_ENTITY_PROCESSING()
	
	IF IS_BIT_SET(iLocalBoolCheck12, LBOOL12_SERVER_STILL_THINKS_IM_HACKING_TEMP)
		IF NOT IS_BIT_SET(iLocalBoolCheck12, LBOOL12_SERVER_STILL_THINKS_IM_HACKING)
			PRINTLN("[RCC MISSION] EVERY_FRAME_POST_ENTITY_PROCESSING - Set bit LBOOL12_SERVER_STILL_THINKS_IM_HACKING")
			SET_BIT(iLocalBoolCheck12, LBOOL12_SERVER_STILL_THINKS_IM_HACKING)
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck12, LBOOL12_SERVER_STILL_THINKS_IM_HACKING)
			PRINTLN("[RCC MISSION] EVERY_FRAME_POST_ENTITY_PROCESSING - Clear bit LBOOL12_SERVER_STILL_THINKS_IM_HACKING")
			CLEAR_BIT(iLocalBoolCheck12, LBOOL12_SERVER_STILL_THINKS_IM_HACKING)
		ENDIF
	ENDIF
	
	//IF bIsLocalPlayerHost
	//	IF g_FMMC_STRUCT.sFMMCEndConditions[0].iMaxWanted[MC_serverBD_4.iCurrentHighestPriority[0]] = 1
	//		MC_serverBD.iPolice = g_FMMC_STRUCT.iPolice
	//	ELSE
	//		MC_serverBD.iPolice = g_FMMC_STRUCT.sFMMCEndConditions[0].iMaxWanted[MC_serverBD_4.iCurrentHighestPriority[0]]
	//	ENDIF
	//ENDIF
	
	IF MC_serverBD.iPolice > 1
		IF IS_BIT_SET(iLocalBoolCheck13, LBOOL13_PLAYERINPEDBLOCKINGZONE)
			IF NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_PLAYERINPEDBLOCKINGZONE_NEWMAXWANTEDSET)
				PRINTLN("[RCC MISSION] EVERY_FRAME_POST_ENTITY_PROCESSING - Entering a ped's wanted blocking zone, call SET_MAX_WANTED_LEVEL to my current wanted level of ",GET_PLAYER_WANTED_LEVEL(LocalPlayer))
				SET_MAX_WANTED_LEVEL(GET_PLAYER_WANTED_LEVEL(LocalPlayer))
				SET_BIT(iLocalBoolCheck13, LBOOL13_PLAYERINPEDBLOCKINGZONE_NEWMAXWANTEDSET)
			ENDIF
		ELSE
			IF IS_BIT_SET(iLocalBoolCheck13, LBOOL13_PLAYERINPEDBLOCKINGZONE_NEWMAXWANTEDSET)
				IF NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_IN_BLOCK_WANTED_LEVEL_ZONE) OR IS_BIT_SET(iLocalBoolCheck13, LBOOL13_IN_BLOCK_AND_CLEAR_WANTED_LEVEL_ZONE)
					
					INT iMaxWanted = 5
					
					IF GET_POLICE_MAX_WANTED_FROM_MENU_SETTING(MC_serverBD.iPolice) > 0
						iMaxWanted = GET_POLICE_MAX_WANTED_FROM_MENU_SETTING(MC_serverBD.iPolice)
					ENDIF
					
					PRINTLN("[RCC MISSION] EVERY_FRAME_POST_ENTITY_PROCESSING - Leaving a ped's wanted blocking zone, call SET_MAX_WANTED_LEVEL back to mission max of ",iMaxWanted)
					SET_MAX_WANTED_LEVEL(iMaxWanted)
				ENDIF
				
				CLEAR_BIT(iLocalBoolCheck13, LBOOL13_PLAYERINPEDBLOCKINGZONE_NEWMAXWANTEDSET)
			ENDIF
		ENDIF
	ENDIF
	
	IF iSpectatorTarget = -1
		
		MC_playerBD[iPartToUse].iInAnyLoc = iInAnyLocTemp
		
		IF MC_playerBD[iPartToUse].iPedCarryCount != iMyPedCarryCount
			MC_playerBD[iPartToUse].iPedCarryCount = iMyPedCarryCount
			PRINTLN("[RCC MISSION] EVERY_FRAME_POST_ENTITY_PROCESSING - MC_playerBD[iPartToUse].iPedCarryCount updated to ",MC_playerBD[iPartToUse].iPedCarryCount,", iPartToUse ",iPartToUse)
		ENDIF
		
		IF MC_playerBD[iPartToUse].iVehDeliveryId = -1
			IF MC_playerBD[iPartToUse].iVehCarryCount != iMyVehicleCarryCount
				MC_playerBD[iPartToUse].iVehCarryCount = iMyVehicleCarryCount
				PRINTLN("[RCC MISSION] EVERY_FRAME_POST_ENTITY_PROCESSING - MC_playerBD[iPartToUse].iVehCarryCount updated to ",MC_playerBD[iPartToUse].iVehCarryCount,", iPartToUse ",iPartToUse)
			ENDIF
		ENDIF
		
		IF MC_playerBD[iPartToUse].iObjCarryCount != iMyObjectCarryCount
			MC_playerBD[iPartToUse].iObjCarryCount = iMyObjectCarryCount
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TRANSFORM_ASSETS)
			AND MC_playerBD[iPartToUse].iObjCarryCount = 0
			AND SHOULD_CLEANUP_OBJECT_TRANSFORMATION_NOW()
				CLEANUP_OBJECT_TRANSFORMATIONS()
			ENDIF
			IF MC_playerBD[iPartToUse].iObjCarryCount = 0
				CLEANUP_OBJECT_INVENTORIES()
			ENDIF
			PRINTLN("[RCC MISSION] EVERY_FRAME_POST_ENTITY_PROCESSING - MC_playerBD[iPartToUse].iObjCarryCount updated to ",MC_playerBD[iPartToUse].iObjCarryCount,", iPartToUse ",iPartToUse)
		ENDIF
		
	ELIF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitset, PBBOOL_HEIST_SPECTATOR)
		MC_playerBD[iLocalPart].iInAnyLoc = iInAnyLocTemp
	ENDIF
	
	IF bIsLocalPlayerHost
		INT iTeam
		
		FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
			IF MC_serverBD.iPriorityLocation[iTeam] != iTempPriorityLocation[iTeam]
				MC_serverBD.iPriorityLocation[iTeam] = iTempPriorityLocation[iTeam]
				PRINTLN("[RCC MISSION] MC_serverBD.iPriorityLocation[iteam]    updated to: ",MC_serverBD.iPriorityLocation[iTeam] ," for team: ",iTeam)
			ENDIF
		ENDFOR
	ENDIF
	
	// 2196520
	IF i_Pri_Sta_IG2_AudioStreamPedID != -1
		BOOL bKillStream
		IF NETWORK_DOES_NETWORK_ID_EXIST( MC_serverBD_1.sFMMC_SBD.niPed[i_Pri_Sta_IG2_AudioStreamPedID] )
			
			PED_INDEX pedTemp = NET_TO_PED( MC_serverBD_1.sFMMC_SBD.niPed[i_Pri_Sta_IG2_AudioStreamPedID] )
			IF NOT IS_PED_INJURED( pedTemp )
		
				IF NOT IS_BIT_SET( iLocalBoolCheck11, LBOOL12_STATION_DRUNK_PED_STREAM_STARTED )
				 
					IF LOAD_STREAM( "CUTSCENES_MPH_PRI_STA_IG2_SYNC_MASTERED_ONLY" )
						
						IF IS_ENTITY_PLAYING_ANIM( pedTemp, "anim@heists@prison_heiststation@cop_reactions", "drunk_talk", ANIM_SCRIPT )
							PLAY_STREAM_FROM_PED( pedTemp )	
							SET_BIT( iLocalBoolCheck11, LBOOL12_STATION_DRUNK_PED_STREAM_STARTED )
							PRINTLN("[RCC MISSION] EVERY_FRAME_POST_ENTITY_PROCESSING - MPH_PRI_STA_IG2 - Played stream from ped = ", i_Pri_Sta_IG2_AudioStreamPedID, " stream = CUTSCENES_MPH_PRI_STA_IG2_SYNC_MASTERED_ONLY" )
						ENDIF
						
					ELSE
						PRINTLN("[RCC MISSION] EVERY_FRAME_POST_ENTITY_PROCESSING - MPH_PRI_STA_IG2 - Waiting for audio stream PRI_STA_IG2_AudioStream(for drunk lady ped) to load" )
					ENDIF
					
				ELSE
					
					// Ped not playing either of the drunk anims, stop the audio stream (they must have been interrupted)
					IF GET_SCRIPT_TASK_STATUS( pedTemp, SCRIPT_TASK_PERFORM_SEQUENCE ) != PERFORMING_TASK
					OR ( NOT IS_ENTITY_PLAYING_ANIM( pedTemp, "anim@heists@prison_heiststation@cop_reactions", "drunk_talk", ANIM_SCRIPT )
					AND NOT IS_ENTITY_PLAYING_ANIM( pedTemp, "anim@heists@prison_heiststation@cop_reactions", "drunk_idle", ANIM_SCRIPT ) )
						PRINTLN("[RCC MISSION] EVERY_FRAME_POST_ENTITY_PROCESSING - MPH_PRI_STA_IG2 - drunk_idle and drunk_talk are not playing, stop the stream " )
						bKillStream = TRUE
					ENDIF
					
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION] EVERY_FRAME_POST_ENTITY_PROCESSING - MPH_PRI_STA_IG2  ped injured/dead, stop stream" )
				bKillStream = TRUE
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION] EVERY_FRAME_POST_ENTITY_PROCESSING - MPH_PRI_STA_IG2 - ped does not exist, stop stream" )
			bKillStream = TRUE
		ENDIF
		
		IF bKillStream
		OR ( IS_BIT_SET( iLocalBoolCheck11, LBOOL12_STATION_DRUNK_PED_STREAM_STARTED ) AND NOT IS_STREAM_PLAYING() )
		
			PRINTLN("[RCC MISSION] EVERY_FRAME_POST_ENTITY_PROCESSING - MPH_PRI_STA_IG2 - bKillStream = ", BOOL_TO_STRING(bKillStream), " LBOOL12_STATION_DRUNK_PED_STREAM_STARTED = ", BOOL_TO_STRING( IS_BIT_SET( iLocalBoolCheck11, LBOOL12_STATION_DRUNK_PED_STREAM_STARTED ) ), " IS_STREAM_PLAYING() = ", BOOL_TO_STRING(IS_STREAM_PLAYING()), ", cleaning up stream" )
		
			IF IS_STREAM_PLAYING()
				STOP_STREAM()
				PRINTLN("[RCC MISSION] EVERY_FRAME_POST_ENTITY_PROCESSING - MPH_PRI_STA_IG2 - STOP_STREAM() called" )
			ELSE
				PRINTLN("[RCC MISSION] EVERY_FRAME_POST_ENTITY_PROCESSING - MPH_PRI_STA_IG2 - STOP_STREAM() not called, wasn't playing a stream." )
			ENDIF
			
			CLEAR_BIT( iLocalBoolCheck11, LBOOL12_STATION_DRUNK_PED_STREAM_STARTED )
			i_Pri_Sta_IG2_AudioStreamPedID = -1
		ENDIF
		
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iPartToUse].iteam)
		
		IF MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam] < FMMC_MAX_RULES
		AND NOT HAS_TEAM_FAILED(MC_playerBD[iLocalPart].iteam)
		AND GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iRuleBitsetThree[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam]],ciBS_RULE3_DISABLE_SHOPS)
				IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_ALL_SHOPS_LOCKED)
					SET_ALL_SHOP_LOCATES_ARE_BLOCKED(TRUE, TRUE)
					SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(TRUE)
					HIDE_ALL_SHOP_BLIPS(TRUE)
					SET_BIT(iLocalBoolCheck10, LBOOL10_ALL_SHOPS_LOCKED)
					PRINTLN("[RCC MISSION] EVERY_FRAME_POST_ENTITY_PROCESSING - SET_ALL_SHOP_LOCATES_ARE_BLOCKED updated to TRUE ")
				ENDIF
			ELSE
				IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_ALL_SHOPS_LOCKED)
					SET_ALL_SHOP_LOCATES_ARE_BLOCKED(FALSE, TRUE)
					SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)
					HIDE_ALL_SHOP_BLIPS(FALSE)
					PRINTLN("[RCC MISSION] EVERY_FRAME_POST_ENTITY_PROCESSING - SET_ALL_SHOP_LOCATES_ARE_BLOCKED updated to FALSE ")
					CLEAR_BIT(iLocalBoolCheck10, LBOOL10_ALL_SHOPS_LOCKED)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iRuleBitset[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam]], ciBS_RULE_BLOCK_MECHANIC_DELIVERY)
				IF NOT IS_MECHANIC_DELIVERY_BLOCKED()
					BLOCK_MECHANIC_DELIVERY(TRUE)
				ENDIF
			ELSE
				IF IS_MECHANIC_DELIVERY_BLOCKED()
					BLOCK_MECHANIC_DELIVERY(FALSE)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iRuleBitset[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam]],ciBS_RULE_ENABLE_SPECTATE)
				IF IS_HEIST_SPECTATE_DISABLED()
					DISABLE_HEIST_SPECTATE(FALSE)
					IF AM_I_ON_A_HEIST()
						PRINT_HELP("HSPECHELP", 15000) // Display longer as per 2133288
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(iLocalBoolCheck21, LBOOL21_SPECTATOR_RESPAWN_WAIT_IN_SPECTATE)
				AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciOVERTIME_WAIT_YOUR_TURN)
				AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_TEAM_TURNS)
					IF NOT IS_HEIST_SPECTATE_DISABLED()
						PRINTLN("PIM - HEIST SPECTATE - DISABLE_HEIST_SPECTATE - A")
						DISABLE_HEIST_SPECTATE(TRUE)
					ENDIF
					IF USING_HEIST_SPECTATE()
						PRINTLN("PIM - HEIST SPECTATE - CLEANUP_HEIST_SPECTATE - A")
						CLEANUP_HEIST_SPECTATE()
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_HEIST_SPECTATE_DISABLED()
				PRINTLN("PIM - HEIST SPECTATE - DISABLE_HEIST_SPECTATE - B")
				DISABLE_HEIST_SPECTATE(TRUE)
			ENDIF
			IF USING_HEIST_SPECTATE()
				PRINTLN("PIM - HEIST SPECTATE - CLEANUP_HEIST_SPECTATE - B")
				CLEANUP_HEIST_SPECTATE()
			ENDIF
		ENDIF
		
		IF iSpectatorTarget = -1
			IF IS_BIT_SET(iLocalBoolCheck4, LBOOL4_I_HAVE_PUSHED_SYNC_LOCK)
				SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_ACTIVATE_2_PERSON_KEY, TRUE)
			ENDIF
		ENDIF
		CLEAR_BIT(iLocalBoolCheck4, LBOOL4_I_HAVE_PUSHED_SYNC_LOCK)
		
		CLEAR_BIT(MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_DROPPING_OFF)
		CLEAR_BIT( iLocalBoolCheck8, LBOOL8_SKIPPED_CUTSCENE_BECAUSE_TOO_FAR )
		
	ENDIF
	
	INT iTeam = MC_playerBD[iLocalPart].iteam

	IF CAN_ALLOW_TEAM_SPECTATE_THIS_MODE()
				
		IF IS_SAFE_TO_USE_HEIST_SPECTATE()
		AND NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_SHOW_HEIST_SPECTATE_HELP)
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_OT_START_SPECTATING)
			SET_BIT(iLocalBoolCheck13, LBOOL13_SHOW_HEIST_SPECTATE_HELP)
		ENDIF
		
		IF IS_BIT_SET(iLocalBoolCheck13, LBOOL13_SHOW_HEIST_SPECTATE_HELP)
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciOVERTIME_WAIT_YOUR_TURN)
				DISPLAY_HELP_TEXT_THIS_FRAME("OTSPEHELP", FALSE)
			ELIF MC_serverBD.iTotalPlayingCoopPlayers[ iTeam ] > 1
				DISPLAY_HELP_TEXT_THIS_FRAME("STSPEHELP", FALSE)
			ENDIF
		ENDIF
		
		IF IS_SAFE_TO_USE_HEIST_SPECTATE()
		AND ((MC_serverBD.iTotalPlayingCoopPlayers[ iTeam ] > 1) // It's more than just me!
			OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciOVERTIME_WAIT_YOUR_TURN))
			DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_ROOF )
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
			PRINTLN("[JS] Disabling convertible roof control this frame")
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
				PLAYER_INDEX toSpectate = INVALID_PLAYER_INDEX()
				INT iPart
				PARTICIPANT_INDEX tempPart
				PLAYER_INDEX tempPlayer
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciOVERTIME_WAIT_YOUR_TURN)
					tempPart = INT_TO_PARTICIPANTINDEX(iOTPartToMove)
					IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
						tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
						IF IS_NET_PLAYER_OK(tempPlayer)
							toSpectate = tempPlayer
						ENDIF
					ENDIF
				ELSE
					REPEAT GET_MAXIMUM_PLAYERS_ON_MISSION() iPart
						IF iPart != iLocalPart
						AND DOES_TEAM_LIKE_TEAM( iTeam , MC_playerBD[iPart].iteam )
							tempPart = INT_TO_PARTICIPANTINDEX(iPart)
							
							IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
								tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
								
								IF IS_NET_PLAYER_OK(tempPlayer)
									//NOTE(Owain): if we're a flow mission, we only want to spectate 
									// people who aren't on our team
									IF IS_LOADED_MISSION_LOW_RIDER_FLOW_MISSION()
										IF( iTeam != MC_playerBD[iPart].iteam )
											toSpectate = tempPlayer
											BREAKLOOP
										ENDIF
									// Otherwise we're a speed race mission and we only want to see people 
									// who are on our team
									ELSE 
										IF( iTeam = MC_playerBD[iPart].iteam )
										OR( MC_serverBD.iNumberOfPlayingPlayers[ iTeam ] <= 1 )
											toSpectate = tempPlayer
											BREAKLOOP
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
				ENDIF
				
				IF toSpectate != INVALID_PLAYER_INDEX()
				AND IS_NET_PLAYER_OK( toSpectate )
					DO_SCREEN_FADE_OUT(500)
					BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("") 
					END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_LOADING)) 
					PRINTLN("[RCC MISSION] EVERY_FRAME_POST_ENTITY_PROCESSING - Going to heist spectate thing, DO SCREEN FADE OUT")
					SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iSpecInfoBitset, SPEC_INFO_BS_USING_HEIST_SPECTATE)
					SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_DO_HEIST_SPECTATE)
					MPGlobalsAmbience.piHeistSpectateTarget = toSpectate
					NETWORK_SET_OVERRIDE_SPECTATOR_MODE(TRUE)
					PRINTLN("[RCC MISSION] EVERY_FRAME_POST_ENTITY_PROCESSING - Going to heist spectate thing, chosen player ",GET_PLAYER_NAME(toSpectate))
				ENDIF
			ENDIF
		ENDIF
	ELSE
		CLEAR_BIT(iLocalBoolCheck13, LBOOL13_SHOW_HEIST_SPECTATE_HELP)
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_MINIGAME_HELP_TEXT_SHOWING_THIS_FRAME)
		CLEAR_BIT(iLocalBoolCheck32, LBOOL32_MINIGAME_HELP_TEXT_SHOWING_THIS_FRAME)
	ENDIF
	
	iInteractWithAssetLoadStagger++
	IF iInteractWithAssetLoadStagger >= FMMC_MAX_NUM_OBJECTS
		iInteractWithAssetLoadStagger = 0
	ENDIF

ENDPROC

FUNC BOOL SHOULD_SKIP_TEAM_UPDATE(INT iteam)

	IF iteam = 0
		IF IS_BIT_SET(MC_serverBD.iServerBitSet,SBBOOL_SKIP_TEAM_0_UPDATE)
			CLEAR_BIT(MC_serverBD.iServerBitSet,SBBOOL_SKIP_TEAM_0_UPDATE)
			NET_PRINT("TEAM 0 skipping update this cycle") NET_NL()
			RETURN TRUE
		ENDIF
	ELIF iteam = 1
		IF IS_BIT_SET(MC_serverBD.iServerBitSet,SBBOOL_SKIP_TEAM_1_UPDATE)
			CLEAR_BIT(MC_serverBD.iServerBitSet,SBBOOL_SKIP_TEAM_1_UPDATE)
			NET_PRINT("TEAM 1 skipping update this cycle") NET_NL()
			RETURN TRUE
		ENDIF
	ELIF iteam = 2
		IF IS_BIT_SET(MC_serverBD.iServerBitSet,SBBOOL_SKIP_TEAM_2_UPDATE)
			CLEAR_BIT(MC_serverBD.iServerBitSet,SBBOOL_SKIP_TEAM_2_UPDATE)
			NET_PRINT("TEAM 2 skipping update this cycle") NET_NL()
			RETURN TRUE
		ENDIF
	ELIF iteam = 3
		IF IS_BIT_SET(MC_serverBD.iServerBitSet,SBBOOL_SKIP_TEAM_3_UPDATE)
			CLEAR_BIT(MC_serverBD.iServerBitSet,SBBOOL_SKIP_TEAM_3_UPDATE)
			NET_PRINT("TEAM 3 skipping update this cycle") NET_NL()
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE

ENDFUNC

PROC STAGGERED_PRE_ENTITY_PROCESSING()
	
	iPlacedVehicleIAmIn = -1
	iNearestTargetTemp =-1
	iNearestTargetTypeTemp = ci_TARGET_NONE
	fNearestTargetDistTemp = 9999999.0
	
	//Kill chase hint cam, Bug 1921219 - maintain throughout the staggered loop after hitting an objective, so we wait for the next iNearestTarget
	IF IS_BIT_SET(iLocalBoolCheck5, LBOOL5_OBJCOMP_KILLCHASEHINTCAM_1)
		CLEAR_BIT(iLocalBoolCheck5, LBOOL5_OBJCOMP_KILLCHASEHINTCAM_1)
		SET_BIT(iLocalBoolCheck5, LBOOL5_OBJCOMP_KILLCHASEHINTCAM_2)
	ELSE
		CLEAR_BIT(iLocalBoolCheck5, LBOOL5_OBJCOMP_KILLCHASEHINTCAM_2)
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck2,LBOOL2_REQUEST_UPDATE)
		SET_BIT(iLocalBoolCheck2,LBOOL2_STARTED_UPDATE)
		PRINTLN("request objective update true so starting update")
	ENDIF
	
	IF NOT bIsLocalPlayerHost
		EXIT
	ENDIF
	
//	IF IS_BIT_SET(MC_serverBD.iServerBitSet2,SBBOOL2_NEED_FULL_LOCATION_UPDATE)
//		SET_BIT(MC_serverBD.iServerBitSet2,SBBOOL2_STARTED_FULL_LOCATION_UPDATE)
//	ENDIF
	
	INT iteam

	FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
	
		SERVER_PRE_ENTITY_PROCESSING_FOR_TEAM(iteam)
		
	ENDFOR

ENDPROC

PROC STAGGERED_POST_ENTITY_PROCESSING()

INT iteam
FLOAT fDropOffDist

	IF DOES_BLIP_EXIST(DeliveryBlip)
		IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
			VECTOR vDropOffCenter = GET_DROP_OFF_CENTER(TRUE)
			IF NOT IS_VECTOR_ZERO(vDropOffCenter)
				fDropOffDist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed,vDropOffCenter)
				//IF fDropOffDist < fNearestTargetDistTemp
					iNearestTargetTypeTemp = ci_TARGET_DROP_OFF
					fNearestTargetDistTemp = fDropOffDist
					iNearestTargetTemp = 0
					PRINTLN("[JS] - STAGGERED_POST_ENTITY_PROCESSING - ci_TARGET_DROP_OFF Setting iNearestTargetTemp to ", 0, " dist = ", fDropOffDist)
				//ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_DUMMY_BLIP_OVERRIDE)	
			IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
			AND MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] = g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipCurrentOverride].iRule
				IF NOT IS_VECTOR_ZERO(GET_DUMMY_OVERRIDE_CENTER(iDummyBlipCurrentOverride))
					fDropOffDist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed,GET_DUMMY_OVERRIDE_CENTER(iDummyBlipCurrentOverride))
					IF fDropOffDist < fNearestTargetDistTemp
						iNearestTargetTypeTemp = ci_TARGET_DROP_OFF
						fNearestTargetDistTemp = fDropOffDist
						iNearestTargetTemp = 0
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF iNearestTarget != iNearestTargetTemp
		iNearestTarget = iNearestTargetTemp
		PRINTLN("hint iNearestTarget updated to: ",iNearestTarget)
	ENDIF
	
	IF iNearestTargetType != iNearestTargetTypeTemp
		iNearestTargetType = iNearestTargetTypeTemp
		PRINTLN("hint iNearestTargetType updated to: ",iNearestTargetType)
	ENDIF
	
	IF fNearestTargetDist != fNearestTargetDistTemp
		fNearestTargetDist = fNearestTargetDistTemp
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck2,LBOOL2_STARTED_UPDATE)
		SET_BIT(iLocalBoolCheck2,LBOOL2_WHOLE_UPDATE_DONE)
		CLEAR_BIT(iLocalBoolCheck2,LBOOL2_REQUEST_UPDATE)
		CLEAR_BIT(iLocalBoolCheck2,LBOOL2_STARTED_UPDATE)
		PRINTLN("started update true so setting update done")
	ENDIF
	
	CLEAR_BIT(iLocalBoolCheck32, LBOOL32_SHOULD_USE_CUSTOM_HELP_WAIT_TEXT_TAXI)

	//Accuracy tracking for Heist celebration screen
	IF iSpectatorTarget = -1
	AND (  Is_Player_Currently_On_MP_Heist(LocalPlayer)
		OR Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer) )
		
		MC_playerBD[iPartToUse].iLatestHits = calculate_hits()
		MC_playerBD[iPartToUse].iLatestShots = calculate_shots()
		
		PRINTLN("[LK] MC_playerBD[iPartToUse].iLatestHits ", MC_playerBD[iPartToUse].iLatestHits) 
		PRINTLN("[LK] MC_playerBD[iPartToUse].iLatestShots ", MC_playerBD[iPartToUse].iLatestShots)  
		PRINTLN("[LK] calculating iLatestHits and iLatestShots")
	ENDIF
	
	IF bIsLocalPlayerHost
		INT iPedBSLoop = 0
		FOR iPedBSLoop = 0 TO FMMC_MAX_PEDS_BITSET-1
			MC_serverBD.iPedDelayRespawnBitset[iPedBSLoop] = 0			
		ENDFOR
		MC_serverBD.iVehDelayRespawnBitset = 0
		
		FOR iteam =  0 TO (MC_serverBD.iNumberOfTeams-1)
			
			IF NOT SHOULD_SKIP_TEAM_UPDATE(iteam)
				
				INT iOldHighestPriority = MC_serverBD_4.iCurrentHighestPriority[iteam]
				
				SERVER_POST_ENTITY_PROCESSING_FOR_TEAM(iteam)
				
				IF NOT (IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_SUDDEN_DEATH_OVERTIME_RUMBLE)
				AND (IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH) OR IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_IN_BMBFB_SUDDEN_DEATH))
				AND iOldHighestPriority < g_FMMC_STRUCT.iSuddenDeathStartRule)
				
					IF MC_serverBD_4.iCurrentHighestPriority[iteam] != iOldHighestPriority
						
						IF iOldHighestPriority > -1
						AND iOldHighestPriority < FMMC_MAX_RULES
							
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iOldHighestPriority], ciBS_RULE4_LAP_PASSED_ON_THIS_RULE)
								MC_ServerBD_4.iTeamLapsCompleted[iTeam]++
								PRINTLN("[RCC MISSION] STAGGERED_POST_ENTITY_PROCESSING - Team ",iTeam," has passed rule ",iOldHighestPriority,", increment MC_ServerBD_4.iTeamLapsCompleted[iTeam] to become ",MC_ServerBD_4.iTeamLapsCompleted[iTeam])
							ENDIF
							
							//Picking a random rule to move to:
							INT iRandomNextRuleBS = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRandomNextRuleBS[iOldHighestPriority]
							
							IF iRandomNextRuleBS != 0
								INT iCount = COUNT_SET_BITS(iRandomNextRuleBS)
								INT iNewRule = FMMC_MAX_RULES
								INT iBitChoice // Which set bit do we want to choose? The first set bit, the second set bit, etc in the bitset? - ignores unset bits
								
								IF iCount > 1
									//Setting the random seed to iSessionScriptEventKey - a network synced random number generated at the start of each session
									//This means we'll always get the same sequence of pseudo-random numbers sharing the session, but it will be different each round
									IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iOldHighestPriority], ciBS_RULE9_USE_COMMON_RANDOM_NEXT_OBJECTIVE_SEED)
										IF iRandomNextObjectiveSeed[iTeam] = -1
											SET_RANDOM_SEED(MC_ServerBD.iSessionScriptEventKey)
											PRINTLN("[STAGGERED_POST_ENTITY_PROCESSING] SET_RANDOM_SEED ... MC_ServerBD.iSessionScriptEventKey = ", MC_ServerBD.iSessionScriptEventKey)
											iRandomNextObjectiveSeed[iTeam] = GET_RANDOM_INT_IN_RANGE(1000, 65535)
											PRINTLN("[STAGGERED_POST_ENTITY_PROCESSING] GET_RANDOM_INT_IN_RANGE ... iRandomNextObjectiveSeed[", iTeam, "] = ", iRandomNextObjectiveSeed[iTeam])
										ELSE
											SET_RANDOM_SEED(iRandomNextObjectiveSeed[iTeam])
											PRINTLN("[STAGGERED_POST_ENTITY_PROCESSING] SET_RANDOM_SEED ... iRandomNextObjectiveSeed[", iTeam, "] = ", iRandomNextObjectiveSeed[iTeam])
											iRandomNextObjectiveSeed[iTeam] = GET_RANDOM_INT_IN_RANGE(1000, 65535)
											PRINTLN("[STAGGERED_POST_ENTITY_PROCESSING] GET_RANDOM_INT_IN_RANGE ... iRandomNextObjectiveSeed[", iTeam, "] = ", iRandomNextObjectiveSeed[iTeam])
										ENDIF
									ENDIF
									
									iBitChoice = GET_RANDOM_INT_IN_RANGE(1000, ((iCount + 1) * 1000) )
									PRINTLN("[RECALCULATE_OBJECTIVE_LOGIC] iCount = ", iCount)
									PRINTLN("[RECALCULATE_OBJECTIVE_LOGIC] GET_RANDOM_INT_IN_RANGE iBitChoice = ", iBitChoice)
									iBitChoice = FLOOR(TO_FLOAT(iBitChoice) / 1000.0)
									PRINTLN("[RECALCULATE_OBJECTIVE_LOGIC] FLOOR iBitChoice = ", iBitChoice)
								ELSE
									iBitChoice = 1
								ENDIF
								
								INT iTempCount = 0
								INT iLoop
								
								FOR iLoop = 0 TO (FMMC_MAX_RULES - 1)
									IF IS_BIT_SET(iRandomNextRuleBS, iLoop)
										iTempCount++
										
										IF iTempCount = iBitChoice
											iNewRule = iLoop
											iLoop = FMMC_MAX_RULES // Break out, we've found the right one!
										ENDIF
									ENDIF
									
									IF iTempCount >= iCount
										iLoop = FMMC_MAX_RULES // Break out, we've checked all the set bits!
									ENDIF
								ENDFOR
								
								IF iNewRule < FMMC_MAX_RULES
									IF iNewRule > MC_ServerBD_4.iCurrentHighestPriority[iTeam]
										PRINTLN("[RCC MISSION][ran] STAGGERED_POST_ENTITY_PROCESSING - Picked new rule for team ",iTeam," just finishing objective ",iOldHighestPriority,": new rule = ",iNewRule)
										INVALIDATE_UP_TO_AND_INCLUDING_OBJECTIVE(iTeam, iNewRule - 1)
										RECALCULATE_OBJECTIVE_LOGIC(iTeam)
									ELIF iNewRule < MC_ServerBD_4.iCurrentHighestPriority[iTeam]
										PRINTLN("[RCC MISSION][ran] STAGGERED_POST_ENTITY_PROCESSING - Picked new rule for team ",iTeam," just finishing objective ",iOldHighestPriority,": new rule (RETURNING TO AN OLDER RULE!) = ",iNewRule)
										INVALIDATE_UP_TO_AND_INCLUDING_OBJECTIVE(iTeam, iNewRule, DEFAULT, DEFAULT, TRUE)
										RECALCULATE_OBJECTIVE_LOGIC(iTeam)
									#IF IS_DEBUG_BUILD
									ELSE
										PRINTLN("[RCC MISSION][ran] STAGGERED_POST_ENTITY_PROCESSING - Failed to apply new rule for team ",iTeam," just finishing objective ",iOldHighestPriority,": new unapplied rule = ",iNewRule,", current rule = ",MC_serverBD_4.iCurrentHighestPriority[iTeam])
									#ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF (MC_serverBD.iMaxObjectives[iteam] - MC_serverBD_4.iCurrentHighestPriority[iteam])  <= 1
					SET_BIT(MC_serverBD.iServerBitSet, SBBOOL_SET_NOT_JOINABLE)
				ENDIF
				
			ENDIF
			
			IF MC_serverBD_4.iCurrentHighestPriority[iteam] < FMMC_MAX_RULES
				// Artificial lights.
				PROCESS_ARTIFICIAL_LIGHTS_SERVER(iTeam, MC_serverBD_4.iCurrentHighestPriority[iteam])
			ENDIF
			
		ENDFOR
		
	ELSE //Not the host:
		
		FOR iteam =  0 TO (MC_serverBD.iNumberOfTeams-1)
			
			//If server migrates, be prepared:
			IF IS_BIT_SET(MC_serverBD.iServerBitSet2,SBBOOL2_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC + iteam)
				SET_BIT(iLocalBoolCheck6,LBOOL6_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC + iteam)
			ELSE
				CLEAR_BIT(iLocalBoolCheck6,LBOOL6_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC + iteam)
			ENDIF
			
		ENDFOR
		
	ENDIF

ENDPROC

PROC CHECK_FOR_END_CUTSCENE_VARIATION_CHANGE()

	//Check for a variation change and update the mocap for all teams
	IF MC_ServerBD.iEndMocapVariation != -1
		IF MC_ServerBD.iEndCutscene != MC_ServerBD.iEndMocapVariation
			PRINTLN("[RCC MISSION] CHECK_FOR_END_CUTSCENE_VARIATION_CHANGE - Updating MC_ServerBD.iEndCutscene from ",MC_ServerBD.iEndCutscene," to equal MC_ServerBD.iEndMocapVariation = ",MC_ServerBD.iEndMocapVariation)
			MC_ServerBD.iEndCutscene = MC_ServerBD.iEndMocapVariation
		ENDIF
	ENDIF
ENDPROC

PROC SHOWROOM_INTERIOR_ENTITY_SET_SYSTEM()
	
	IF NOT bShowroomInteriorEntitySetChecked
	
		IF MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_SIMEON
	
			IF IS_VALID_INTERIOR(interiorID)
				
				ACTIVATE_INTERIOR_ENTITY_SET(interiorID, "shutter_closed")
					
				ACTIVATE_INTERIOR_ENTITY_SET(interiorID, "csr_beforeMission")
					
				REFRESH_INTERIOR(interiorID)
				
				bShowroomInteriorEntitySetChecked = TRUE
				
				PRINTSTRING("interior entity set setup")
				PRINTNL()
			ELSE
			
				interiorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-30.8793, -1088.3356, 25.4221>>, "v_carshowroom") 
			ENDIF
			
		ELSE
			bShowroomInteriorEntitySetChecked = TRUE
		ENDIF
	
	ENDIF 

ENDPROC

FUNC BOOL IS_CREATOR_VOICE_CHAT_RESTRICTED()

	INT iTeam 
	INT iPriority 
	
	IF iPartToUse > -1
	AND iPartToUse < NUM_NETWORK_PLAYERS
		iTeam = MC_playerBD[iPartToUse].iteam
		iPriority = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
	ENDIF

	IF iTeam > -1
	AND iPriority > -1 AND iPriority < FMMC_MAX_RULES
	AND NOT g_bMissionEnding
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iPriority], ciBS_RULE2_RESTRICT_VOICE_CHAT)
			PRINTLN("IS_CREATOR_VOICE_CHAT_RESTRICTED - Returning True")
			RETURN TRUE
		ENDIF
	ENDIF

	PRINTLN("IS_CREATOR_VOICE_CHAT_RESTRICTED - Returning False")
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_MISSION_CHAT_PROXIMITY()
	
	IF IS_CREATOR_VOICE_CHAT_RESTRICTED()
		
		RETURN 100.00
	ENDIF

	RETURN 0.0
ENDFUNC

PROC HANDLE_DROPOFF_CUTSCENE( INT iTeam )
	CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] HANDLE_DROPOFF_CUTSCENE for team ", iTeam )
	
	g_bGarageDropOff = FALSE
	g_bPropertyDropOff = FALSE
	g_bShouldThisPlayerGetPulledIntoApartment = FALSE

	IF IS_BIT_SET( MC_ServerBD.iServerBitSet4, SBBOOL4_TEAM_0_APARTMENT_GET_AND_DELIVER + iTeam )
		g_bPropertyDropOff = TRUE
		IF NOT g_bShouldThisPlayerGetPulledIntoApartment
			g_bShouldThisPlayerGetPulledIntoApartment = TRUE
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Property dropoff! g_bShouldThisPlayerGetPulledIntoApartment = TRUE " )
		ENDIF
		SET_APARTMENT_DROPOFF_WARP_RANGE_BASED_ON_CUTSCENE( iTeam )
	ELIF IS_BIT_SET( MC_ServerBD.iServerBitSet4, SBBOOL4_TEAM_0_GARAGE_GET_AND_DELIVER + iTeam )
		g_bGarageDropOff = TRUE
		IF NOT g_bShouldThisPlayerGetPulledIntoApartment
			g_bShouldThisPlayerGetPulledIntoApartment = TRUE
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Garage dropoff! g_bShouldThisPlayerGetPulledIntoApartment = TRUE " )
		ENDIF
		SET_APARTMENT_DROPOFF_WARP_RANGE_BASED_ON_CUTSCENE( iTeam )
	ELIF IS_BIT_SET( MC_ServerBD.iServerBitSet4, SBBOOL4_TEAM_0_SET_LOCATE_DROPOFF + iTeam )
		SET_LOCATION_DATA_BASED_ON_TYPE( iTeam )
	ENDIF
	
	SET_BIT( iLocalBoolCheck19, LBOOL19_CHECKED_DROPOFF_CUTSCENE )
ENDPROC


FLOAT fNewVehicleDensity = -1
FLOAT fNewPedDensity = -1 

PROC PROCESS_PRE_MAIN_CHECKS()
	
	BOOL bHost = bIsLocalPlayerHost
	INT iTeam
	
	CLEAR_BIT(iLocalBoolCheck7, LBOOL7_HOST_APPLIED_NEW_PRIORITY_MIDFRAME)
	CLEAR_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)	
	
	FOR iTeam = 0 TO ( MC_serverBD.iNumberOfTeams - 1 )
		CLEAR_BIT(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
		CLEAR_BIT(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_MIDPOINT_THIS_FRAME + iTeam)
		
		IF iOldPriority[iTeam] != MC_ServerBD_4.iCurrentHighestPriority[iTeam]
			
			IF bHost
				CLEAR_BIT(MC_serverBD.iServerBitSet2,SBBOOL2_NEW_PARTICIPANT_LOOP_STARTED)
				CLEAR_BIT(MC_serverBD.iServerBitSet2,SBBOOL2_NEW_PARTICIPANT_LOOP_FINISHED)
				IF IS_BIT_SET(MC_serverBD.iServerBitSet4, SBBOOL4_ALL_PLAYERS_IN_ANY_LOCATE_PASSED)
					CLEAR_BIT(MC_serverBD.iServerBitSet4, SBBOOL4_ALL_PLAYERS_IN_ANY_LOCATE_PASSED)
				ENDIF
				
				MC_serverBD_4.iNumberOfFiresExtinguished[iTeam] = 0
				PRINTLN("[TMS][RiotVanFire] Resetting MC_serverBD_4.iNumberOfFiresExtinguished[iTeam] as this is a new rule for team ", iTeam)
				
				CLEAR_BIT(MC_serverBD.iServerBitSet6, SBBOOL6_COUNTED_VEH_HACK_TARGETS)
				PRINTLN("[TMS][SecuroHack] Resetting SBBOOL6_COUNTED_VEH_HACK_TARGETS")
				
				MC_serverBD.iObjectCaptureOffset[iTeam] = 0
				
				//Vehicle density per rule:
				IF MC_serverBD_4.iCurrentHighestPriority[iTeam] > -1
				AND MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES

					IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleTraffic[MC_serverBD_4.iCurrentHighestPriority[iTeam]] != RULE_TRAFFIC_DEFAULT
						MC_serverBD.fVehicleDensity  = GET_MISSION_VEHICLE_DENSITY_FROM_INT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleTraffic[MC_serverBD_4.iCurrentHighestPriority[iTeam]])
					ENDIF
					
					IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRulePedDensity[MC_serverBD_4.iCurrentHighestPriority[iTeam]] != RULE_TRAFFIC_DEFAULT
						MC_serverBD.fPedDensity = GET_PED_DENSITY_FROM_INT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRulePedDensity[MC_serverBD_4.iCurrentHighestPriority[iTeam]])
					ENDIF
					
				ENDIF
				MC_serverBD.iGranularCurrentPoints[iTeam] = 0

				IF MC_serverBD_4.iCurrentHighestPriority[iTeam] > -1
				AND MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
					INT i
					IF MC_serverBD.iUndeliveredVehCleanupBS[iTeam] > 0
						FOR i = 0 TO MC_serverBD.iNumVehCreated - 1
							IF IS_BIT_SET(MC_serverBD.iUndeliveredVehCleanupBS[iTeam], i)
								IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
									INT iteam2
									FOR iteam2 = 0 TO (MC_serverBD.iNumberOfTeams-1)
										CLEAR_BIT(MC_serverBD.iVehteamFailBitset[i],iteam2)
										CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iteam2], i)
									ENDFOR
									IF IS_VEHICLE_USING_RESPAWN_POOL(i)
										SET_VEHICLE_RESPAWNS(i, 0)
									ENDIF
									CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
									SET_BIT(MC_serverBD.iCleanedUpUndeliveredVehiclesBS, i)
									PRINTLN("[JS] PROCESS_PRE_MAIN_CHECKS - Veh ",i," was not delivered last rule, marking as no longer needed")
								ENDIF
							ENDIF
						ENDFOR
						MC_serverBD.iUndeliveredVehCleanupBS[iTeam] = 0
					ENDIF
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE9_CLEANUP_UNDELIVERED_VEHICLES)
						FOR i = 0 TO MC_serverBD.iNumVehCreated - 1
							IF MC_serverBD_4.ivehRule[i][iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
							AND MC_serverBD_4.iVehPriority[i][iTeam] = MC_serverBD_4.iCurrentHighestPriority[iTeam]
								IF NOT IS_BIT_SET(MC_serverBD.iUndeliveredVehCleanupBS[iTeam], i)
									PRINTLN("[JS] PROCESS_PRE_MAIN_CHECKS - Veh ",i," will be cleaned up if not delivered this rule")
									SET_BIT(MC_serverBD.iUndeliveredVehCleanupBS[iTeam], i)
								ENDIF
							ENDIF
						ENDFOR
					ENDIF
					
					//Option to reset multirule timer of other teams
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[MC_ServerBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE11_INVALIDATE_MULTIRULE_TIMERS)
						PRINTLN("[RCC MISSION][MultiRuleTimer] PROCESS_PRE_MAIN_CHECKS - Team ", iteam, " have reached a rule that resets multirule timers")
						INT iTempTeam
						FOR iTempTeam = 0 TO FMMC_MAX_TEAMS - 1
							MC_serverBD_3.iMultiObjectiveTimeLimit[iTempTeam] = 0
							RESET_NET_TIMER(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTempTeam])
							PRINTLN("[RCC MISSION][MultiRuleTimer] PROCESS_PRE_MAIN_CHECKS - team ", iTempTeam, " having multirule timer reset")
						ENDFOR
					ENDIF
					
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(iLocalBoolCheck24, LBOOL24_BLOCK_OBJECTIVE_TEXT_UNTIL_RULE_CHANGE)
				PRINTLN("[JS] PROCESS_PRE_MAIN_CHECKS - CLEARING LBOOL24_BLOCK_OBJECTIVE_TEXT_UNTIL_RULE_CHANGE")
				CLEAR_BIT(iLocalBoolCheck24, LBOOL24_BLOCK_OBJECTIVE_TEXT_UNTIL_RULE_CHANGE)
			ENDIF
			
			IF iTeam = MC_playerBD[iLocalPart].iTeam 
				MC_playerBD[iLocalPart].iGranularCurrentPoints = 0
				MC_playerBD[iLocalPart].iGranularObjCapturingBS = 0
				RESET_NET_TIMER(tdGranularCaptureTimer)
			ENDIF
			
			IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_TEAM_0_IS_PAST_THEIR_FIRST_RULE + iTeam)
			AND iOldPriority[iTeam] != -1
				SET_BIT(iLocalBoolCheck16, LBOOL16_TEAM_0_IS_PAST_THEIR_FIRST_RULE + iTeam)
			ENDIF
						
			iOldPriority[iTeam] = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
			SET_BIT( iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam )
			CLEAR_BIT( iLocalBoolCheck18, LBOOL18_RULE_CHANGE_SHARD_PLAYED_TEAM_0 + iTeam )
			CLEAR_BIT( iLocalBoolCheck19, LBOOL19_CHECKED_DROPOFF_CUTSCENE )
			
			CHECK_IF_NEXT_RULE_IS_CUTSCENE_RULE_FOR_ANY_FRIENDLY_TEAM( iteam )
			
			PRINTLN("[RCC MISSION] OBJECTIVE_TEXT - LBOOL6_TEAM_", iTeam, "_HAS_NEW_PRIORITY_THIS_FRAME - SET We are now on Rule Priority: ", MC_serverBD_4.iCurrentHighestPriority[iTeam])			
			RESET_NET_TIMER(tdScoreAndLapSyncTimer)
			START_NET_TIMER(tdScoreAndLapSyncTimer)
		ENDIF
		
		IF iOldMidpoint[iTeam] != MC_serverBD.iObjectiveMidPointBitset[iTeam]
			iOldMidpoint[iTeam] = MC_serverBD.iObjectiveMidPointBitset[iTeam]
			SET_BIT(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_MIDPOINT_THIS_FRAME + iTeam)
			PRINTLN("[RCC MISSION] OBJECTIVE_TEXT - LBOOL6_TEAM_", iTeam, "_HAS_NEW_MIDPOINT_THIS_FRAME - SET")
		ENDIF

		//Handle team wanted level
		IF NOT HAS_NET_TIMER_STARTED(tdRespawnTimer)
		OR GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdRespawnTimer) >= 7000
			IF MC_serverBD.iNumOfWanted[iTeam] > 0
			OR MC_serverBD.iNumOfFakeWanted[iTeam] > 0
				PRINTLN("[MJM] MC_serverBD.iNumOfWanted[",iTeam,"] = ",MC_serverBD.iNumOfWanted[iTeam])
				IF NOT IS_BIT_SET(iWasTeamWantedBS,iTeam)
					SET_BIT(iWasTeamWantedBS,iTeam)
					SET_BIT(iLocalBoolCheck4,LBOOL4_T0GOTWANTED + iTeam)
					PRINTLN("[MJM] SET_BIT(iLocalBoolCheck4,LBOOL4_T0GOTWANTED + iTeam)")
				ENDIF
			ELSE
				IF IS_BIT_SET(iWasTeamWantedBS,iTeam)
					CLEAR_BIT(iWasTeamWantedBS,iTeam)
					SET_BIT(iLocalBoolCheck4,LBOOL4_T0LOSTWANTED + iTeam)
					PRINTLN("[MJM] SET_BIT(iLocalBoolCheck4,LBOOL4_T0LOSTWANTED + iTeam)")
				ENDIF
			ENDIF
			IF HAS_NET_TIMER_STARTED(tdRespawnTimer)
				RESET_NET_TIMER(tdRespawnTimer)
			ENDIF
		ENDIF
		
		//Dialogue points:
		IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
		AND (IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
			OR IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_MIDPOINT_THIS_FRAME + iTeam))
			
			//Handle team wanted level
			IF iOldRule2[iTeam] != iNewRule2[iTeam]
				PRINTLN("[MJM] iOldRule2[",iTeam,"] != iNewRule2[",iTeam,"] ",iOldRule2[iTeam]," != ",iNewRule2[iTeam])
				IF MC_serverBD.iNumOfWanted[iTeam] > 0
				OR MC_serverBD.iNumOfFakeWanted[iTeam] > 0
					PRINTLN("[MJM] MC_serverBD.iNumOfWanted[",iTeam,"] = ",MC_serverBD.iNumOfWanted[iTeam])
					SET_BIT(iLocalBoolCheck4,LBOOL4_T0GOTWANTED + iTeam)
					PRINTLN("[MJM] SET_BIT(iLocalBoolCheck4,LBOOL4_T0GOTWANTED + iTeam)")
				ELSE
					CLEAR_BIT(iLocalBoolCheck4,LBOOL4_T0GOTWANTED + iTeam)
					PRINTLN("[MJM] CLEAR_BIT(iLocalBoolCheck4,LBOOL4_T0GOTWANTED + iTeam)")
				ENDIF
				CLEAR_BIT(iLocalBoolCheck4,LBOOL4_T0LOSTWANTED + iTeam)
				iOldRule2[iTeam] = iNewRule2[iTeam]
			ENDIF
							
			IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam],MC_serverBD_4.iCurrentHighestPriority[iTeam]) //If we've reached the midpoint
				iOldDialoguePoints[iTeam] = MC_serverBD.iTeamScore[iTeam]
			ENDIF
		ENDIF
		
		//If I'm entering/leaving spectate, re-check all these things:
		IF NOT IS_BIT_SET(iLocalBoolCheck21, LBOOL21_SPECTATOR_RESPAWN_WAIT_IN_SPECTATE)
		AND NOT CAN_SPECTATING_PLAYER_RESPAWN_NEXT_RULE(iLocalPart)
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciOVERTIME_WAIT_YOUR_TURN)
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_TEAM_TURNS)
		AND NOT (MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iTeam] < FMMC_MAX_RULES AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iTeam].iRuleBitsetTen[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iTeam]], ciBS_RULE10_FORCE_HEIST_SPECTATE))
			IF iSpectatorTarget != -1
				IF NOT IS_BIT_SET(iLocalBoolCheck7, LBOOL7_PRE_MAIN_WAS_A_SPECTATOR_CACHE)
					PRINTLN("SPECTATOR IS SETTING THEMSELVES AS BEING ON A NEW PRIORITY 1")
					SET_BIT(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
					SET_BIT(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_MIDPOINT_THIS_FRAME + iTeam)
				ENDIF
			ELSE
				IF IS_BIT_SET(iLocalBoolCheck7, LBOOL7_PRE_MAIN_WAS_A_SPECTATOR_CACHE)
					PRINTLN("SPECTATOR IS SETTING THEMSELVES AS BEING ON A NEW PRIORITY 2")
					SET_BIT(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
					SET_BIT(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_MIDPOINT_THIS_FRAME + iTeam)
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	CLEAR_BIT(iLocalBoolCheck22, LBOOL22_NEW_OT_ROUND_THIS_FRAME)
	IF NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_OVERTIME_SCORE_SHARD_ON_SCREEN)
		CLEAR_BIT(iLocalBoolCheck23, LBOOL23_OVERTIME_CHECK_SCORE_AFTER_SHARD)
	ENDIF
	IF IS_BIT_SET( MC_serverBD.iServerBitSet4, SBBOOL4_TEAM_0_CHECKED_CUTSCENE_DROPOFF + MC_PlayerBD[ iLocalPart ].iTeam )
		IF NOT IS_BIT_SET( iLocalBoolCheck19, LBOOL19_CHECKED_DROPOFF_CUTSCENE )
			HANDLE_DROPOFF_CUTSCENE( MC_PlayerBD[ iLocalPart ].iTeam )
		ENDIF
	ENDIF
	
	IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TOUR_DE_FORCE (g_FMMC_STRUCT.iAdversaryModeType)
		IF iOldLaps != MC_ServerBD_4.iTeamLapsCompleted[MC_playerBD[iPartToUse].iteam]
			iOldLaps = MC_ServerBD_4.iTeamLapsCompleted[MC_playerBD[iPartToUse].iteam]
			SET_BIT(iLocalBoolCheck13, LBOOL13_MY_TEAM_HAS_A_NEW_LAP)
			SET_BIT(iLocalBoolCheck9,LBOOL9_DO_INSTANT_DIALOGUE_LOOP)
		ELSE
			CLEAR_BIT(iLocalBoolCheck13, LBOOL13_MY_TEAM_HAS_A_NEW_LAP)
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(iLocalBoolCheck19,LBOOL19_SHOW_LAST_LAP_HELP_TEXT)
			PRINTLN("[RCC MISSION][LAP HELP] FINAL LAP EVAL")
			IF MC_ServerBD_4.iTeamLapsCompleted[MC_playerBD[iPartToUse].iteam] +1 = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMissionLapLimit
				PRINTLN("[RCC MISSION][LAP HELP] FINAL LAP TRIGGERED - ", MC_ServerBD_4.iTeamLapsCompleted[MC_playerBD[iPartToUse].iteam] +1 , " / ", g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMissionLapLimit )
				PRINT_HELP("TDF_HELP_2")
				SET_BIT(iLocalBoolCheck19,LBOOL19_SHOW_LAST_LAP_HELP_TEXT)
			ENDIF
		ENDIF
	ENDIF
	
	IF ((MC_serverBD.fVehicleDensity != fNewVehicleDensity) OR (MC_serverBD.fPedDensity != fNewPedDensity))
		
		IF iPopulationHandle > -1 //it exists, so better clear it
			PRINTLN("[RCC MISSION][POP] PROCESS_PRE_MAIN_CHECKS - We have a global density iPopulationHandle of ",iPopulationHandle,", clear the existing densities before re-adding")
			MC_CLEAR_PED_AND_TRAFFIC_DENSITIES(iPopulationHandle, bVehicleGenerator)
			MC_SET_PED_AND_TRAFFIC_DENSITIES(iPopulationHandle, bVehicleGenerator, MC_serverBD.fPedDensity, MC_serverBD.fVehicleDensity)
		ELSE
			PRINTLN("[RCC MISSION][POP] PROCESS_PRE_MAIN_CHECKS - We have a global density iPopulationHandle of ",iPopulationHandle,", just add the new global density")
			MC_SET_PED_AND_TRAFFIC_DENSITIES(iPopulationHandle, bVehicleGenerator, MC_serverBD.fPedDensity, MC_serverBD.fVehicleDensity)
		ENDIF
		
		IF iLocalZoneBitset != 0
			PRINTLN("[RCC MISSION][POP] PROCESS_PRE_MAIN_CHECKS - The global density has changed, so we need to re-add any vehicle gens & roads from zones (as they will have just been clobbered by the global change)")
			
			INT iZone
			FOR iZone = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfZones - 1)
				IF IS_BIT_SET(iLocalZoneBitset, iZone)
					IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iType = ciFMMC_ZONE_TYPE__CLEAR_VEHICLES
					OR g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iType = ciFMMC_ZONE_TYPE__CLEAR_ALL
						SET_VEHICLE_GENERATORS_AND_ROADS_INACTIVE_FOR_ZONE_LEGACY(iZone, TRUE, TRUE)
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
		
		IF bHost
			IF MC_serverBD.fPedDensity = 0
			AND MC_serverBD.fVehicleDensity = 0
				IF MC_serverBD_3.scenBlockServer = NULL
					SERVER_BLOCK_ALL_SCENARIOS(MC_serverBD_3.scenBlockServer)
				ENDIF
			ELSE
				IF MC_serverBD_3.scenBlockServer != NULL
					SERVER_CLEAR_BLOCKING_ALL_SCENARIOS(MC_serverBD_3.scenBlockServer)
				ENDIF
			ENDIF
		ENDIF
		
		fNewPedDensity 		= MC_serverBD.fPedDensity
		fNewVehicleDensity = MC_serverBD.fVehicleDensity
		PRINTLN("[RCC MISSION][POP] PROCESS_PRE_MAIN_CHECKS - Setting new vehicle density of ",fNewVehicleDensity,", new ped density of ",fNewPedDensity)
	ENDIF
	
	IF iSpectatorTarget != -1
		SET_BIT(iLocalBoolCheck7, LBOOL7_PRE_MAIN_WAS_A_SPECTATOR_CACHE)
	ELSE
		CLEAR_BIT(iLocalBoolCheck7, LBOOL7_PRE_MAIN_WAS_A_SPECTATOR_CACHE)
		
		IF NOT HAS_NET_TIMER_STARTED(MC_playerBD[iPartToUse].tdBoundstimer)
		AND NOT HAS_NET_TIMER_STARTED(MC_playerBD[iPartToUse].tdBounds2timer)
		AND NOT CURRENT_OBJECTIVE_LOSE_COPS() 
		AND NOT CURRENT_OBJECTIVE_LOSE_GANG_BACKUP()
		AND NOT CURRENT_OBJECTIVE_REPAIR_CAR()
		AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_SUDDEN_DEATH_KILLALLENEMIES)  //url:bugstar:2500338
		AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN)
		AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_BE_MY_VALENTINES)
		AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN) 
		AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_LOST_AND_DAMNED)
		AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet4, SBBOOL4_SUDDEN_DEATH_JUGGERNAUT)
		AND NOT IS_BIT_SET(iLocalBoolCheck32, LBOOL32_BLOCKING_OBJECTIVE_FROM_DIALOGUE_TRIGGER)
			IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
				CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
				PRINTLN("[RCC MISSION] PROCESS_PRE_MAIN_CHECKS - (1) Clearing - PBBOOL_OBJECTIVE_BLOCKER")
			ENDIF
		ENDIF
		
		// 2172439
		IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
		AND IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iPartToUse].iteam)
			
			PRINTLN("[CHAT_FUNC] PROCESS_PRE_MAIN_CHECKS, CLIENT_SET_UP_TEAM_ONLY_CHAT, GET_MISSION_CHAT_PROXIMITY = ", GET_MISSION_CHAT_PROXIMITY())
			CLIENT_SET_UP_TEAM_ONLY_CHAT(GET_MISSION_CHAT_PROXIMITY())
		
			IF IS_CREATOR_VOICE_CHAT_RESTRICTED()
				PRINTLN("[CHAT_FUNC] PROCESS_PRE_MAIN_CHECKS, IS_CREATOR_VOICE_CHAT_RESTRICTED, NETWORK_SET_PROXIMITY_AFFECTS_TEAM(TRUE)")
				NETWORK_SET_PROXIMITY_AFFECTS_TEAM(TRUE)
			ENDIF
			
			IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_PLAYER_IS_AIMING_AT_PROTECT_PED)
				CLEAR_BIT(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_PLAYER_IS_AIMING_AT_PROTECT_PED)
				PRINTLN("[RCC MISSION] PROCESS_PRE_MAIN_CHECKS - Clearing PBBOOL4_PLAYER_IS_AIMING_AT_PROTECT_PED due to new rule")
			ENDIF
		ENDIF
	ENDIF
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
	AND IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iPartToUse].iteam)
		IF IS_THIS_MODE_ALLOWING_SPECTATOR_CROSS_TALK()
			ALLOW_SPECTATORS_TO_CHAT(TRUE)
		ENDIF
	ENDIF
	
	IF iVehBlipCreatedThisFrameBitset != 0
		CDEBUG3LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Vehicle blip creation was blocked for some vehicles last frame. Clearing blocks for new frame.")
		iVehBlipCreatedThisFrameBitset = 0
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck13, LBOOL13_CONTROL_PAD_COLOUR_SHOULD_BE_RESET)
		CLEAR_CONTROL_LIGHT_EFFECT(PLAYER_CONTROL)
		CLEAR_BIT(iLocalBoolCheck13, LBOOL13_CONTROL_PAD_COLOUR_SHOULD_BE_RESET)
	ENDIF
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
	AND NOT g_bInMultiplayer
	AND IS_BIT_SET(BitSet_CellphoneTU, g_BSTU_ENABLE_MP_TRACKIFY)
		g_bInMultiplayer = TRUE
		PRINTLN("[RCC MISSION] PROCESS_PRE_MAIN_CHECKS - Setting g_bInMultiplayer to true in the creator as it was off")
	ENDIF
	
	IF NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
		PROCESS_RESTRICTION_ZONES()
	ENDIF
	
	IF MC_playerBD[iLocalPart].iteam > -1
	AND MC_playerBD[iLocalPart].iteam < FMMC_MAX_TEAMS
	AND MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam] > -1
	AND MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam] < FMMC_MAX_RULES
		iPrevValidRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam]
	ENDIF
	
	fLastFrameTime = GET_FRAME_TIME()
	
ENDPROC

PROC TRACK_EXTRA_STATS()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iHeistRewardBonusBitset, ci_bs_HeistMMTimerReward)
	AND g_FMMC_STRUCT.iHeistRewardMMTimerEndRule >= g_FMMC_STRUCT.iHeistRewardMMTimerStartRule
	AND IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + g_FMMC_STRUCT.iHeistRewardMMTimerTeam)
	AND bIsLocalPlayerHost
	AND AM_I_ON_A_HEIST()
		
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT.iHeistRewardMMTimerTeam]
		
		IF iRule >= g_FMMC_STRUCT.iHeistRewardMMTimerStartRule
			
			IF iRule <= g_FMMC_STRUCT.iHeistRewardMMTimerEndRule
				IF NOT HAS_NET_TIMER_STARTED(MC_serverBD.tdHeistRewardMMTimer)
					REINIT_NET_TIMER(MC_serverBD.tdHeistRewardMMTimer)
					PRINTLN("[RCC MISSION] TRACK_EXTRA_STATS - Starting heist reward mid-mission timer as team ",g_FMMC_STRUCT.iHeistRewardMMTimerTeam," has reached starting rule ",g_FMMC_STRUCT.iHeistRewardMMTimerStartRule)
				ENDIF
			ELSE
				IF HAS_NET_TIMER_STARTED(MC_serverBD.tdHeistRewardMMTimer)
					MC_serverBD.iHeistRewardMMTimer = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdHeistRewardMMTimer)
					PRINTLN("[RCC MISSION] TRACK_EXTRA_STATS - Stopping heist reward mid-mission timer as team ",g_FMMC_STRUCT.iHeistRewardMMTimerTeam," has passed finishing rule ",g_FMMC_STRUCT.iHeistRewardMMTimerEndRule,"; time value = ",MC_serverBD.iHeistRewardMMTimer,"ms")
					RESET_NET_TIMER(MC_serverBD.tdHeistRewardMMTimer)
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC

PROC DISPLAY_PLAYER_KILL_HUD()

INT itotalplayerkills
INT iPlayersToKill

	
		IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
			IF bPlayerToUseOK
			AND NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_FINISHED)
			AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitset[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE_HIDE_HUD)
			AND NOT (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE8_RESPAWN_PLAYERS_ON_KILL)
				AND (IS_BIT_SET(iLocalBoolCheck21, LBOOL21_RESURRECTION_HUD_SHOW_ENEMY_TEAM) OR IS_LOCAL_PLAYER_ANY_SPECTATOR()))
				INT istage = GET_MC_CLIENT_MISSION_STAGE(iPartToUse)
				IF NOT SHOULD_HIDE_THE_HUD_THIS_FRAME()
					DRAW_KILLSTREAK_DISPLAY(GlobalplayerBD_FM[NATIVE_TO_INT(PlayerToUse)].iKillStreak) 
				ENDIF
				IF MC_serverBD.iPlayerRuleLimit[MC_serverBD.iCurrentPlayerRule[MC_playerBD[iPartToUse].iteam]][MC_playerBD[iPartToUse].iteam] != UNLIMITED_CAPTURES_KILLS
					IF istage = CLIENT_MISSION_STAGE_KILL_PLAYERS
						itotalplayerkills = MC_serverBD.iTeamPlayerKills[MC_playerBD[iPartToUse].iteam][0] + MC_serverBD.iTeamPlayerKills[MC_playerBD[iPartToUse].iteam][1] 
						+ MC_serverBD.iTeamPlayerKills[MC_playerBD[iPartToUse].iteam][2] + MC_serverBD.iTeamPlayerKills[MC_playerBD[iPartToUse].iteam][3] 
						iPlayersToKill = MC_serverBD.iPlayerRuleLimit[MC_serverBD.iCurrentPlayerRule[MC_playerBD[iPartToUse].iteam]][MC_playerBD[iPartToUse].iteam] -itotalplayerkills
						IF  iPlayersToKill > 0
							DRAW_GENERIC_BIG_NUMBER(iPlayersToKill,"MC_PKILLS")
						ENDIF
					ELIF istage = CLIENT_MISSION_STAGE_KILL_TEAM0
						iPlayersToKill = MC_serverBD.iPlayerRuleLimit[MC_serverBD.iCurrentPlayerRule[MC_playerBD[iPartToUse].iteam]][MC_playerBD[iPartToUse].iteam] -MC_serverBD.iTeamPlayerKills[MC_playerBD[iPartToUse].iteam][0] 
						IF  iPlayersToKill > 0
							DRAW_GENERIC_BIG_NUMBER(iPlayersToKill,"MC_PKILLS")
						ENDIF
					ELIF istage = CLIENT_MISSION_STAGE_KILL_TEAM1
						iPlayersToKill = MC_serverBD.iPlayerRuleLimit[MC_serverBD.iCurrentPlayerRule[MC_playerBD[iPartToUse].iteam]][MC_playerBD[iPartToUse].iteam] -MC_serverBD.iTeamPlayerKills[MC_playerBD[iPartToUse].iteam][1] 
						IF  iPlayersToKill > 0
							DRAW_GENERIC_BIG_NUMBER(iPlayersToKill,"MC_PKILLS")
						ENDIF
					ELIF istage = CLIENT_MISSION_STAGE_KILL_TEAM2
						iPlayersToKill = MC_serverBD.iPlayerRuleLimit[MC_serverBD.iCurrentPlayerRule[MC_playerBD[iPartToUse].iteam]][MC_playerBD[iPartToUse].iteam] -MC_serverBD.iTeamPlayerKills[MC_playerBD[iPartToUse].iteam][2] 
						IF  iPlayersToKill > 0
							DRAW_GENERIC_BIG_NUMBER(iPlayersToKill,"MC_PKILLS")
						ENDIF
					ELIF istage = CLIENT_MISSION_STAGE_KILL_TEAM3
						iPlayersToKill = MC_serverBD.iPlayerRuleLimit[MC_serverBD.iCurrentPlayerRule[MC_playerBD[iPartToUse].iteam]][MC_playerBD[iPartToUse].iteam] -MC_serverBD.iTeamPlayerKills[MC_playerBD[iPartToUse].iteam][3] 
						IF  iPlayersToKill > 0
							DRAW_GENERIC_BIG_NUMBER(iPlayersToKill,"MC_PKILLS")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF


ENDPROC

PROC DRAW_ENTITY_RESPAWN_RECAPTURE_HUD()

STRING recaptext

	IF bPlayerToUseOK
	AND NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_FINISHED)
		IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitset[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE_HIDE_HUD)
				IF MC_serverBD.iRuleReCapture[MC_playerBD[iPartToUse].iteam][MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] > 0
					IF MC_serverBD.iNumLocHighestPriority[MC_playerBD[iPartToUse].iteam] > 0
						recaptext = "MC_URCAPL"
					ELIF MC_serverBD.iNumPedHighestPriority[MC_playerBD[iPartToUse].iteam] > 0
						recaptext = "MC_URCAPP"
					ELIF MC_serverBD.iNumVehHighestPriority[MC_playerBD[iPartToUse].iteam] > 0
						recaptext = "MC_URCAPV"
					ELIF MC_serverBD.iNumOBJHighestPriority[MC_playerBD[iPartToUse].iteam] > 0
						recaptext = "MC_URCAPO"
					ENDIF
					
					IF MC_serverBD.iRuleReCapture[MC_playerBD[iPartToUse].iteam][MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] >= UNLIMITED_CAPTURES_KILLS
						//DRAW_GENERIC_BIG_NUMBER(-1,recaptext,-1,HUD_COLOUR_WHITE,HUDORDER_DONTCARE,FALSE,"",HUD_COLOUR_WHITE,TRUE)
					ELSE
						DRAW_GENERIC_BIG_NUMBER(MC_serverBD.iRuleReCapture[MC_playerBD[iPartToUse].iteam][MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] ],recaptext)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC DO_CTF_MISSION_HELP_TEXT()
	IF Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer)

		IF GET_MP_INT_CHARACTER_STAT(MP_STAT_CTF_HELP_COUNTER) > 3
			EXIT
		ENDIF
		IF iCTFMissionHelpProgress > 2
			EXIT
		ENDIF
		IF IS_PAUSE_MENU_ACTIVE()
			EXIT
		ENDIF
		IF IS_HELP_MESSAGE_BEING_DISPLAYED()
			EXIT
		ENDIF
		IF NOT bLocalPlayerPedOk
			EXIT
		ENDIF
		IF NOT IS_PLAYER_CONTROL_ON(LocalPlayer)
			EXIT
		ENDIF
		IF g_bMissionEnding
			EXIT
		ENDIF	
		
		SWITCH iCTFMissionHelpProgress
			CASE 0
				PRINT_HELP("CTF_HELP1")
				iCTFMissionHelpProgress++
			BREAK
			
			CASE 1
				PRINT_HELP("CTF_HELP2")
				iCTFMissionHelpProgress++
			BREAK
			
			CASE 2
				PRINT_HELP("CTF_HELP3")
				INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_CTF_HELP_COUNTER,1)
				iCTFMissionHelpProgress++
			BREAK
			
		ENDSWITCH
		
	ENDIF

ENDPROC

INT iNumPlayersOnMission
SCRIPT_TIMER timeTutMissionHelp
INT iTutMissionHelpProg

PROC DO_TUTORIAL_MISSION_HELP_TEXT()
	VEHICLE_INDEX veh
	INT iStatInt
	IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
		
		IF IS_HELP_MESSAGE_BEING_DISPLAYED()
			EXIT
		ENDIF
		IF NOT bLocalPlayerPedOk
			EXIT
		ENDIF
		
		IF NOT IS_PLAYER_CONTROL_ON(LocalPlayer)
			EXIT
		ENDIF
		
		IF g_bMissionEnding
			EXIT
		ENDIF
		
		SWITCH iTutMissionHelpProg
			CASE 0
				//-- Bad sport

				
				iTutMissionHelpProg++
			BREAK
			CASE 1
				iTutMissionHelpProg++
				PRINTLN("[dsw] [DO_TUTORIAL_MISSION_HELP_TEXT] iTutMissionHelpProg = ", iTutMissionHelpProg)
				//-- Bad sport

			
			BREAK
			CASE 2
				IF iNumPlayersOnMission > 1
					IF NOT IS_CUSTOM_MENU_ON_SCREEN()
						IF GET_DPADDOWN_ACTIVATION_STATE() = DPADDOWN_NONE
							IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
								//-- other players help from freemode.sc
								ADD_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS(TRUE)
								PRINT_HELP("FM_IHELP_OTP")
								RESET_NET_TIMER(timeTutMissionHelp)
								
								iTutMissionHelpProg++
								PRINTLN("[dsw] [DO_TUTORIAL_MISSION_HELP_TEXT] iTutMissionHelpProg = ", iTutMissionHelpProg)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					iTutMissionHelpProg++
					PRINTLN("[dsw] [DO_TUTORIAL_MISSION_HELP_TEXT] Only player on mission  iTutMissionHelpProg = ", iTutMissionHelpProg)
				ENDIF
			BREAK
			
			
			
			CASE 3
				IF NOT IS_PAUSE_MENU_ACTIVE()
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					#IF IS_DEBUG_BUILD OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT) #ENDIF
						IF NOT HAS_NET_TIMER_STARTED(timeTutMissionHelp)
							START_NET_TIMER(timeTutMissionHelp)
						ELSE
							IF HAS_NET_TIMER_EXPIRED(timeTutMissionHelp,4000)
								IF NOT IS_CUSTOM_MENU_ON_SCREEN()
									IF GET_DPADDOWN_ACTIVATION_STATE() = DPADDOWN_NONE
										ADD_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS(TRUE)
										PRINT_HELP("FM_IHELP_BXP", 12000) //  You will gain more XP for doing Jobs together. Do a Job with 1 other and gain 50% more, do a Job with 2 others and gain 75% more, do a Job with 3 others and gain double XP.
										RESET_NET_TIMER(timeTutMissionHelp)
										
										iTutMissionHelpProg++
										PRINTLN("[dsw] [DO_TUTORIAL_MISSION_HELP_TEXT] iTutMissionHelpProg = ", iTutMissionHelpProg)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 4
				//-- Phone help 
				IF NOT IS_PAUSE_MENU_ACTIVE()
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					#IF IS_DEBUG_BUILD OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT) #ENDIF
						IF NOT HAS_NET_TIMER_STARTED(timeTutMissionHelp)
							START_NET_TIMER(timeTutMissionHelp)
						ELSE
							IF HAS_NET_TIMER_EXPIRED(timeTutMissionHelp,4000)
								IF NOT IS_CUSTOM_MENU_ON_SCREEN()
									IF GET_DPADDOWN_ACTIVATION_STATE() = DPADDOWN_NONE
										ADD_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS(TRUE)
										PRINT_HELP("FM_IHELP_PHP", 7000) //  You can use your phone to call other players.
										RESET_NET_TIMER(timeTutMissionHelp)
										
										iTutMissionHelpProg++
										PRINTLN("[dsw] [DO_TUTORIAL_MISSION_HELP_TEXT] iTutMissionHelpProg = ", iTutMissionHelpProg)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 5
				//-- Crew help
				IF NOT IS_PAUSE_MENU_ACTIVE()
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					#IF IS_DEBUG_BUILD OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT) #ENDIF
						IF NOT HAS_NET_TIMER_STARTED(timeTutMissionHelp)
							START_NET_TIMER(timeTutMissionHelp)
						ELSE
							IF HAS_NET_TIMER_EXPIRED(timeTutMissionHelp,4000)
								IF NOT IS_CUSTOM_MENU_ON_SCREEN()
									IF GET_DPADDOWN_ACTIVATION_STATE() = DPADDOWN_NONE
										IF NOT IS_BIT_SET(iTutorialMissionCutBitset, biTut_FoundEnemyBlip)
											ADD_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS(TRUE)
											PRINT_HELP("FM_IHELP_CRW", 7000) //  You can join a Crew at rockstargames.com/socialclub. Select your Crew from the Pause menu.
											RESET_NET_TIMER(timeTutMissionHelp)
											
											iTutMissionHelpProg++
											PRINTLN("[dsw] [DO_TUTORIAL_MISSION_HELP_TEXT] iTutMissionHelpProg = ", iTutMissionHelpProg)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 6

			BREAK
			
			CASE 7

			BREAK
			
			CASE 8
				//-- Waypoint help
				IF NOT IS_PAUSE_MENU_ACTIVE()
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					#IF IS_DEBUG_BUILD OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT) #ENDIF
						IF NOT HAS_NET_TIMER_STARTED(timeTutMissionHelp)
							START_NET_TIMER(timeTutMissionHelp)
						ELSE
							IF HAS_NET_TIMER_EXPIRED(timeTutMissionHelp,4000)
								IF NOT IS_CUSTOM_MENU_ON_SCREEN()
									
									IF GET_DPADDOWN_ACTIVATION_STATE() = DPADDOWN_NONE
							//			PRINT_HELP("FM_IHELP_WAYP")	//~s~Go to the map screen ~INPUT_SELECT~ and press ~INPUT_SPRINT~ to add or remove a custom waypoint with a GPS route.~n~Other team mates in a vehicle with you will also see your custom waypoint.
										iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MISS_HELP_TEXT)
										SET_BIT(iStatInt, biNMH_WaypointHelp)			
										SET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MISS_HELP_TEXT, iStatInt)
										SET_BIT(MPGlobalsAmbience.iFmNmhBitSet, BI_FM_NMH_WAYPOINT)
										RESET_NET_TIMER(timeTutMissionHelp)
										
										iTutMissionHelpProg++
										PRINTLN("[dsw] [DO_TUTORIAL_MISSION_HELP_TEXT] iTutMissionHelpProg = ", iTutMissionHelpProg)
									ENDIF
									
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 9
				//-- Passenger help
				IF NOT IS_PAUSE_MENU_ACTIVE()
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					#IF IS_DEBUG_BUILD OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT) #ENDIF
						IF NOT HAS_NET_TIMER_STARTED(timeTutMissionHelp)
							START_NET_TIMER(timeTutMissionHelp)
						ELSE
							IF HAS_NET_TIMER_EXPIRED(timeTutMissionHelp,4000)
								IF bLocalPlayerPedOk
									IF IS_PED_SITTING_IN_ANY_VEHICLE(LocalPlayerPed)
										IF GET_DPADDOWN_ACTIVATION_STATE() = DPADDOWN_NONE
											veh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
											IF GET_PED_IN_VEHICLE_SEAT(veh,VS_FRONT_RIGHT) <> NULL
											OR GET_PED_IN_VEHICLE_SEAT(veh,VS_BACK_LEFT) <> NULL
											OR GET_PED_IN_VEHICLE_SEAT(veh,VS_BACK_RIGHT) <> NULL
										//		PRINT_HELP("FM_IHELP_PASS") // Passengers can change radio stations
												
												PRINTLN("[dsw] [DO_TUTORIAL_MISSION_HELP_TEXT] Done passenger help")
											ENDIF
											iTutMissionHelpProg++
											PRINTLN("[dsw] [DO_TUTORIAL_MISSION_HELP_TEXT] iTutMissionHelpProg = ", iTutMissionHelpProg)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 10
				//-- Change weapon in vehicle help
				IF NOT IS_PAUSE_MENU_ACTIVE()
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					#IF IS_DEBUG_BUILD OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT) #ENDIF
						IF NOT IS_CUSTOM_MENU_ON_SCREEN()
							IF GET_DPADDOWN_ACTIVATION_STATE() = DPADDOWN_NONE
								iTutMissionHelpProg++
								PRINTLN("[dsw] [DO_TUTORIAL_MISSION_HELP_TEXT] iTutMissionHelpProg = ", iTutMissionHelpProg)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		
		
		//-- Enemy blip help
		IF NOT IS_BIT_SET(iTutorialMissionCutBitset, biTut_DoneEnemyBlipHelp)
			IF NOT IS_PAUSE_MENU_ACTIVE()
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					IF NOT IS_CUSTOM_MENU_ON_SCREEN()
						IF bLocalPlayerPedOk
							IF IS_BIT_SET(iTutorialMissionCutBitset, biTut_FoundEnemyBlip) 
								PRINT_HELP("FM_IHELP_NPCB") // Non-player ~r~enemies ~s~are marked on the Radar. 
								SET_BIT(iTutorialMissionCutBitset, biTut_DoneEnemyBlipHelp) 
								PRINTLN("[dsw] [DO_TUTORIAL_MISSION_HELP_TEXT] doing enemy blip help")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
ENDPROC

PROC MAINTAIN_HANDLER_HELP_TEXT()

	IF IS_BIT_SET(iLocalBoolCheck2, LBOOL2_HANDLER_HELP_TEXT_NEEDED)
	
		INT 			iStatInt
		VEHICLE_INDEX 	veh
		
		IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet5, BI_FM_NMH5_HANDLER4)
		
			iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT4)
			
			IF NOT IS_BIT_SET(iStatInt, biNmh4_Handler)			
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT GET_IS_LEADERBOARD_CAM_ACTIVE_OR_INTERPING()
						IF bLocalPlayerPedOk
							IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
								IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
									veh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
									IF GET_ENTITY_MODEL(veh) = HANDLER
										IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet5, BI_FM_NMH5_HANDLER1)
											PRINT_HELP("FM_IHELP_HND1") // Position the container handler over a container using ~INPUT_SCRIPT_LEFT_AXIS_Y~.
											SET_BIT(MPGlobalsAmbience.iFmNmhBitSet5, BI_FM_NMH5_HANDLER1)
											PRINTLN("[MAINTAIN_HANDLER_HELP_TEXT] [dsw] Set BI_FM_NMH5_HANDLER1 ")
										ELIF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet5, BI_FM_NMH5_HANDLER2)
											PRINT_HELP("FM_IHELP_HND2") // Press ~INPUT_VEH_HEADLIGHT~ to pick up the container.
											SET_BIT(MPGlobalsAmbience.iFmNmhBitSet5, BI_FM_NMH5_HANDLER2)
											PRINTLN("[MAINTAIN_HANDLER_HELP_TEXT] [dsw] Set BI_FM_NMH5_HANDLER2 ")
										ELIF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet5, BI_FM_NMH5_HANDLER3)
											PRINT_HELP("FM_IHELP_HND3") // Once the handler frame is lowered press ~INPUT_VEH_HEADLIGHT~ to release the container.
											SET_BIT(MPGlobalsAmbience.iFmNmhBitSet5, BI_FM_NMH5_HANDLER3)
											PRINTLN("[MAINTAIN_HANDLER_HELP_TEXT] [dsw] Set BI_FM_NMH5_HANDLER3 ")
										ELIF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet5, BI_FM_NMH5_HANDLER4)
											PRINT_HELP("FM_IHELP_HND4") // Use ~INPUT_SCRIPT_LEFT_AXIS_Y~ to move the handler frame.
											SET_BIT(MPGlobalsAmbience.iFmNmhBitSet5, BI_FM_NMH5_HANDLER4)
											SET_BIT(iStatInt, biNmh4_Handler)
											SET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT4, iStatInt)
											PRINTLN("[MAINTAIN_HANDLER_HELP_TEXT] [dsw] Set BI_FM_NMH5_HANDLER4 ")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF		
			ENDIF
		ENDIF
	ENDIF
ENDPROC

///PURPOSE:
///   If the local player has a property drop-off objective but they don't own the apartment, display help text explaining that the owner needs to buzz them in 
PROC MAINTAIN_PROPERTY_DROP_OFF_HELP()
	VECTOR vLoc
	VECTOR vPlayerPos
	INT iPropertyIndex 
	IF NOT IS_BIT_SET(iLocalBoolCheck7, LBOOL7_PROPERTY_DROP_OFF_OWNER_HELP)

		IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
			IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iDropOffType[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] = ciFMMC_DROP_OFF_TYPE_PROPERTY
				IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_HEIST_HOST)

					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT GET_IS_LEADERBOARD_CAM_ACTIVE_OR_INTERPING()
							IF bLocalPlayerPedOk
								IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
									
									IF IS_FAKE_MULTIPLAYER_MODE_SET()
										iPropertyIndex = 1
									ELSE
										iPropertyIndex = MC_ServerBD.iHeistPresistantPropertyID
									ENDIF
									
									IF iPropertyIndex >= 0
									AND iPropertyIndex <= MAX_MP_PROPERTIES // array size is MAX_MP_PROPERTIES + 1
										vLoc 			=  mpProperties[iPropertyIndex].vBlipLocation[0]
										vPlayerPos 		= GET_ENTITY_COORDS(LocalPlayerPed)
										
										IF VDIST2(vLoc, vPlayerPos) < 625.0 // 25 m
											PRINT_HELP("PBUZZ") // Wait for the apartment owner to buzz you in.
											SET_BIT(iLocalBoolCheck7, LBOOL7_PROPERTY_DROP_OFF_OWNER_HELP)
											PRINTLN("[dsw] [MAINTAIN_PROPERTY_DROP_OFF_HELP] Printing help iPropertyIndex = ", iPropertyIndex)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
ENDPROC

PROC MAINTAIN_HEIST_AWARD_HELP()
	IF NOT IS_BIT_SET(iLocalBoolCheck8, LBOOL8_DONE_HEIST_AWARD_HELP_CHECK)
		IF MC_serverBD.iTotalPlayingCoopPlayers[MC_playerBD[iPartToUse].iteam] > 0
			IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
			OR Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
				IF NOT IS_THIS_IS_A_STRAND_MISSION()
				OR (IS_THIS_IS_A_STRAND_MISSION() AND NOT HAS_FIRST_STRAND_MISSION_BEEN_PASSED())
					IF MC_serverBD.iTotalPlayingCoopPlayers[MC_playerBD[iPartToUse].iteam] >= 3
						SET_BIT(MPGlobalsAmbience.iFmNmhBitSet9, BI_FM_NMH9_DO_HST_AWD_HELP)
						PRINTLN("[RCC Mission] Done heist award check, Set BI_FM_NMH9_DO_HST_AWD_HELP")
					ENDIF
				ENDIF
			ENDIF
			
			PRINTLN("[RCC Mission] Done heist award check, MC_serverBD.iTotalPlayingCoopPlayers[iteam] = ", MC_serverBD.iTotalPlayingCoopPlayers[MC_playerBD[iPartToUse].iteam])
			SET_BIT(iLocalBoolCheck8, LBOOL8_DONE_HEIST_AWARD_HELP_CHECK)
		ENDIF
	ENDIF
ENDPROC

///PURPOSE: 2107736 - Requests the drilling audio banks for non-drilling players so they can hear the drilling sound effects.
///2152186 - Use the flag to request other drilling assets too.
PROC MANAGE_DRILLING_AUDIO_REQUESTS()
	IF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_HEIST_DRILL_ASSET_REQUEST)
		IF NOT IS_BIT_SET(iLocalBoolCheck8, LBOOL8_HEIST_DRILL_ASSET_LOADED)
			REQUEST_ANIM_DICT("anim@heists@fleeca_bank@drilling")
			REQUEST_MODEL(hei_prop_heist_drill)
			REQUEST_MODEL(MC_playerBD_1[iLocalPart].mnHeistGearBag)
		
			IF REQUEST_SCRIPT_AUDIO_BANK("DLC_MPHEIST\\HEIST_FLEECA_DRILL")
			AND REQUEST_SCRIPT_AUDIO_BANK("DLC_MPHEIST\\HEIST_FLEECA_DRILL_2")
			AND HAS_ANIM_DICT_LOADED("anim@heists@fleeca_bank@drilling")
			AND HAS_MODEL_LOADED(hei_prop_heist_drill)
			AND HAS_MODEL_LOADED(MC_playerBD_1[iLocalPart].mnHeistGearBag)
				SET_BIT(iLocalBoolCheck8, LBOOL8_HEIST_DRILL_ASSET_LOADED)
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck8, LBOOL8_HEIST_DRILL_ASSET_LOADED)
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_MPHEIST\\HEIST_FLEECA_DRILL")
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_MPHEIST\\HEIST_FLEECA_DRILL_2")
			REMOVE_ANIM_DICT("anim@heists@fleeca_bank@drilling")
			SET_MODEL_AS_NO_LONGER_NEEDED(hei_prop_heist_drill)
			SET_MODEL_AS_NO_LONGER_NEEDED(MC_playerBD_1[iLocalPart].mnHeistGearBag)
			
			CLEAR_BIT(iLocalBoolCheck8, LBOOL8_HEIST_DRILL_ASSET_LOADED)
		ENDIF
	ENDIF
ENDPROC


///PURPOSE: 2107736 - Requests the drilling audio banks for non-drilling players so they can hear the drilling sound effects.
///2152186 - Use the flag to request other drilling assets too.
PROC MANAGE_VAULT_DRILLING_ASSET_REQUESTS__FMMC()
	IF IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_VAULT_DRILL_ASSET_REQUEST)
		IF NOT IS_BIT_SET(iLocalBoolCheck32, LBOOL32_VAULT_DRILL_ASSET_LOADED)
			IF REMOTE_ASSETS_REQUEST_AND_LOAD(IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_VAULT_DRILL_ASSET_REQUEST_LASER), MC_playerBD_1[iLocalPart].mnHeistGearBag)
				SET_BIT(iLocalBoolCheck32, LBOOL32_VAULT_DRILL_ASSET_LOADED)
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_VAULT_DRILL_ASSET_REQUEST)
			REMOTE_CLEAR_ASSETS(IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_VAULT_DRILL_ASSET_REQUEST_LASER), MC_playerBD_1[iLocalPart].mnHeistGearBag)
			CLEAR_BIT(iLocalBoolCheck32, LBOOL32_VAULT_DRILL_ASSET_LOADED)
		ENDIF
	ENDIF
ENDPROC


PROC MAINTAIN_VEHICLE_TURRET_SOUNDS()

	IF iCurrentHeistMissionIndex = HBCA_BS_SERIES_A_FINALE
		BOOL bInSeat = FALSE
		IF bLocalPlayerOK AND bLocalPlayerPedOk
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				VEHICLE_INDEX vehiclePedIsIn = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
				IF NOT IS_VEHICLE_FUCKED(vehiclePedIsIn)
					IF IS_THIS_MODEL_A_TURRET_VEHICLE(GET_ENTITY_MODEL(vehiclePedIsIn))
						IF IS_TURRET_SEAT(vehiclePedIsIn,GET_SEAT_PED_IS_IN(LocalPlayerPed))
							IF NOT IS_BIT_SET(iLocalBoolCheck10,LBOOL10_PLAYER_IN_TURRET_SEAT)
								BROADCAST_HEIST_PLAYER_IN_TURRET(ALL_PLAYERS_ON_SCRIPT(FALSE),TRUE)
								SET_BIT(iLocalBoolCheck10,LBOOL10_PLAYER_IN_TURRET_SEAT)
								PRINTLN("[MJM] - MAINTAIN_VEHICLE_TURRET_SOUNDS - PLAYER ENTERED TURRET")
							ENDIF
							bInSeat = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT bInSeat
			IF IS_BIT_SET(iLocalBoolCheck10,LBOOL10_PLAYER_IN_TURRET_SEAT)
				BROADCAST_HEIST_PLAYER_IN_TURRET(ALL_PLAYERS_ON_SCRIPT(FALSE),FALSE)
				CLEAR_BIT(iLocalBoolCheck10,LBOOL10_PLAYER_IN_TURRET_SEAT)
				PRINTLN("[MJM] - MAINTAIN_VEHICLE_TURRET_SOUNDS - PLAYER LEFT TURRET")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_HEIST_BONUS_RESET_TICKERS()
	IF g_bRunHeistIntroShard
		IF NOT bShardShown
			bShardShown = TRUE
			PRINTLN("[MJM] MAINTAIN_HEIST_BONUS_RESET_TICKERS() bShardShown = TRUE")
		ENDIF
	ENDIF

	IF bShardShown
		IF NOT g_bRunHeistIntroShard
			IF IS_SAFE_TO_DISPLAY_HEIST_BONUS_TICKERS()
				IF g_bDoHeistBonusFlowOrderBrokenTicker
					g_bDoHeistBonusFlowOrderBrokenTicker = FALSE
					PRINT_TICKER("HBCA_RST_FO")
				ENDIF
				IF g_bDoHeistBonusSameTeamBrokenTicker
					g_bDoHeistBonusSameTeamBrokenTicker = FALSE
					PRINT_TICKER("HBCA_RST_ST")
				ENDIF	
				IF g_bDoHeistBonusUltimateChallengeBrokenTicker
					g_bDoHeistBonusUltimateChallengeBrokenTicker = FALSE
					PRINT_TICKER("HBCA_RST_UC")
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC MAINTAIN_EMERGENCY_CALLS()
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetTwo[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]],ciBS_RULE2_ENABLE_EMERGENCY_CALLS)
			IF NOT ARE_EMERGENCY_CALLS_SUPPRESSED()
				SUPPRESS_EMERGENCY_CALLS()
			ENDIF
		ELSE
			IF ARE_EMERGENCY_CALLS_SUPPRESSED()
				RELEASE_SUPPRESSED_EMERGENCY_CALLS()
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA (g_FMMC_STRUCT.iAdversaryModeType)
		IF NOT ARE_EMERGENCY_CALLS_SUPPRESSED()
			SUPPRESS_EMERGENCY_CALLS()
		ENDIF
	ENDIF
	
	IF NOT IS_CONTACT_IN_PHONEBOOK(CHAR_CALL911, MULTIPLAYER_BOOK)
		INT iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT5)
		IF IS_BIT_SET(iStatInt, biNmh5_AddedEmergencyServices)
			ADD_CONTACT_TO_PHONEBOOK (CHAR_CALL911, MULTIPLAYER_BOOK, FALSE)
		ENDIF
	ENDIF
	
ENDPROC

//Set dialogue bs = spectator target if spectating
PROC MAINTAIN_SPECTATOR_DIALOGUE()

	IF iLocalSpectateTarget != iSpectatorTarget
		INT i
		REPEAT ciMAX_DIALOGUE_BIT_SETS_LEGACY i
			iLocalDialoguePlayedBS[i] = MC_playerBD[iPartToUse].iDialoguePlayedBS[i]
		ENDREPEAT
		iLocalSpectateTarget = iSpectatorTarget
		PRINTLN("[MJM] MAINTAIN_SPECTATOR_DIALOGUE - Switching to use dialogue for player: ",iSpectatorTarget)
	ENDIF

ENDPROC

///PURPOSE: This function iterates through all possible components that
///    can be added on a weapon and adds them as necessary
PROC PROCESS_WEAPON_ATTACHMENTS( INT iThisWeapon )
	//-- Note: I had to add some extra checks due to GET_PICKUP_OBJECT() returning -1 if the player isn't near the pickup (see 1997291) - Dave W
	INT iComponent = 0
	OBJECT_INDEX oPickup
	WEAPONCOMPONENT_TYPE eType = WEAPONCOMPONENT_INVALID
	PICKUP_TYPE WEAPON_PICKUPTYPEI =	GET_WEAPON_PICKUP_TYPE(iThisWeapon)			
	FOR iComponent = 0 TO  FMMC_MAX_WEAPON_COMPONENTS - 1
		eType = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[ iThisWeapon ].wctComponents[ iComponent ]
		
		IF eType != WEAPONCOMPONENT_INVALID
			IF DOES_PICKUP_EXIST(pipickup[iThisWeapon])
				oPickup = GET_PICKUP_OBJECT( pipickup[ iThisWeapon ] )
				IF DOES_ENTITY_EXIST(oPickup)
					IF IS_PICKUP_A_WEAPON(WEAPON_PICKUPTYPEI)	// this check was added by Kevin 16/10/2014 to fix 2083999
						IF IS_PICKUP_WEAPON_OBJECT_VALID(oPickup)
							IF NOT HAS_WEAPON_GOT_WEAPON_COMPONENT(oPickup, eType)
								MODEL_NAMES eCompModel = GET_WEAPON_COMPONENT_TYPE_MODEL(eType)
								REQUEST_MODEL(eCompModel)
								IF HAS_MODEL_LOADED(eCompModel)
									GIVE_WEAPON_COMPONENT_TO_WEAPON_OBJECT(oPickup , eType )
									PRINTLN("[RCC MISSION] [dsw] PROCESS_WEAPON_ATTACHMENTS giving component eType = ", eType) 
									CERRORLN( DEBUG_OWAIN, "Valid new component for weapon: ", eType )
									SET_MODEL_AS_NO_LONGER_NEEDED(eCompModel)
								ELSE
									PRINTLN("[RCC MISSION] [dsw] PROCESS_WEAPON_ATTACHMENTS waiting for component model to load eType = ", eType) 
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ENDFOR
ENDPROC

PROC PROCESS_PICKUP_BLIPS_AND_ATTACHMENTS( INT iwpn )
	#IF IS_DEBUG_BUILD
	IF bDebugBlipPrints
		PRINTLN("[LH][WEP_BLIPS] 3 - Looking at weapon pickup [", iwpn,"]")
	ENDIF #ENDIF
	PICKUP_TYPE tempPickup
	
	IF DOES_PICKUP_EXIST(pipickup[iwpn])
		#IF IS_DEBUG_BUILD
		IF bDebugBlipPrints
			PRINTLN("[LH][WEP_BLIPS] 4 - Pickup Exists [", iwpn,"]")
		ENDIF #ENDIF
		IF NOT DOES_PICKUP_OBJECT_EXIST(pipickup[iwpn])
			#IF IS_DEBUG_BUILD
			IF bDebugBlipPrints
				PRINTLN("[LH][WEP_BLIPS] 4.5 - Object no longer exists - deleting pickup blip [", iwpn,"]")
			ENDIF #ENDIF
			IF DOES_BLIP_EXIST(biPickup[iwpn])
				#IF IS_DEBUG_BUILD
				IF bDebugBlipPrints
					PRINTLN("[KH][WEAPON PICKUPS][BLIPS] - PROCESS_PICKUP_BLIPS_AND_ATTACHMENTS [", iwpn,"] - REMOVING BLIP")
				ENDIF #ENDIF
				REMOVE_BLIP(biPickup[iwpn])
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			IF bDebugBlipPrints
				PRINTLN("[LH][WEP_BLIPS] 5 - PROCESS_WEAPON_ATTACHMENTS [", iwpn,"]")
			ENDIF #ENDIF
			PROCESS_WEAPON_ATTACHMENTS(iwpn)

			IF NOT DOES_BLIP_EXIST( biPickup[ iwpn ] )
			//	PROCESS_WEAPON_ATTACHMENTS( iwpn )
				#IF IS_DEBUG_BUILD
				IF bDebugBlipPrints
					PRINTLN("[LH][WEP_BLIPS] 6 - Blip Doesn't exist! [", iwpn,"]")
				ENDIF #ENDIF
				IF (Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)
				OR Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer))
				AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
					#IF IS_DEBUG_BUILD
					IF bDebugBlipPrints
						PRINTLN("[LH][WEP_BLIPS] 7 - GET_WEAPON_PICKUP_TYPE [", iwpn,"]")
					ENDIF #ENDIF
					tempPickup = GET_WEAPON_PICKUP_TYPE(iwpn)
				ELSE
					#IF IS_DEBUG_BUILD
					IF bDebugBlipPrints
						PRINTLN("[LH][WEP_BLIPS] 7 - g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwpn].pt [", iwpn,"]")
					ENDIF #ENDIF
					tempPickup =g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwpn].pt
				ENDIF
				IF Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer) 
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDMWeaponsLocked) 
					AND IS_PICKUP_HEALTH(tempPickup) = FALSE
					AND IS_PICKUP_ARMOUR(tempPickup) = FALSE
					AND IS_PICKUP_PARACHUTE(tempPickup) = FALSE
						tempPickup = GET_PICKUP_TYPE_FROM_WEAPON_TYPE(GET_DEATHMATCH_CREATOR_RESPAWN_WEAPON(g_FMMC_STRUCT_ENTITIES.iWeaponPallet))
					ENDIF
				ENDIF
				
				IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwpn].iPlacedBitset, ciFMMC_WEP_Hide_Blip)
					biPickup[iwpn]=ADD_BLIP_FOR_COORD(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwpn].vPos)
					//biPickup[iwpn]=ADD_BLIP_FOR_PICKUP(pipickup[iwpn])
					
					#IF IS_DEBUG_BUILD
					IF bDebugBlipPrints
						PRINTLN("[LH][WEP_BLIPS] 8 - ADD_BLIP_FOR_COORD [", iwpn,"]")
					ENDIF #ENDIF
					IF tempPickup = PICKUP_CUSTOM_SCRIPT
					OR tempPickup = PICKUP_VEHICLE_CUSTOM_SCRIPT
					OR tempPickup = PICKUP_VEHICLE_CUSTOM_SCRIPT_LOW_GLOW
						#IF IS_DEBUG_BUILD
						IF bDebugBlipPrints
							PRINTLN("[LH][WEP_BLIPS] 9 - PICKUP_CUSTOM_SCRIPT [", iwpn,"]")
						ENDIF #ENDIF
						BLIP_SPRITE rageSprite
						TEXT_LABEL_15 tl15BlipNameOverride
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciRANDOM_PICKUP_TYPES_OPTION)
						AND g_FMMC_STRUCT_ENTITIES.bRandomPickupTypes = TRUE
							rageSprite = RADAR_TRACE_PICKUP_RANDOM
						ELSE
							SWITCH(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwpn].iCustomPickupType)
								CASE ciCUSTOM_PICKUP_TYPE__BULLSHARK_TEST
									tl15BlipNameOverride = "PU_BST"
									PRINTLN("[JS] [POWERPLAY] Setting custom blip name - Raging")
								BREAK
								CASE ciCUSTOM_PICKUP_TYPE__BEAST_MODE
									tl15BlipNameOverride = "PU_BM"
									PRINTLN("[JS] [POWERPLAY] Setting custom blip name - Beasted")
								BREAK
								CASE ciCUSTOM_PICKUP_TYPE__SWAP
									tl15BlipNameOverride = "PU_SW2"
									PRINTLN("[JS] [POWERPLAY] Setting custom blip name - Flipped")
								BREAK
								CASE ciCUSTOM_PICKUP_TYPE__FILTER
									tl15BlipNameOverride = "PU_FI"
									PRINTLN("[JS] [POWERPLAY] Setting custom blip name - Doped")
								BREAK
								CASE ciCUSTOM_PICKUP_TYPE__BULLET_TIME
									tl15BlipNameOverride = "PU_BT"
									PRINTLN("[JS] [POWERPLAY] Setting custom blip name - Zoned")
								BREAK
								CASE ciCUSTOM_PICKUP_TYPE__HIDE_BLIPS
									tl15BlipNameOverride = "PU_HB"
									PRINTLN("[JS] [POWERPLAY] Setting custom blip name - Dark")
								BREAK
								CASE ciCUSTOM_PICKUP_TYPE__PLAYER_LIVES
									tl15BlipNameOverride = "PU_LIVES"
									PRINTLN("[JS] [POWERPLAY] Setting custom blip name - Extra Life")
								BREAK
								CASE ciCUSTOM_PICKUP_TYPE__WEAPON_BAG
									tl15BlipNameOverride = "PU_WPBG"
									PRINTLN("[JS] [POWERPLAY] Setting custom blip name - Weapons Bag")
								BREAK
							ENDSWITCH
							SWITCH(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwpn].iVehicleWeaponPickupType)
								CASE ciVEH_WEP_ROCKETS
									rageSprite = RADAR_TRACE_ROCKETS
									tl15BlipNameOverride = "BLIP_368"
									PRINTLN("[JS] [VEHICLE WEAPONS] Setting custom blip name - Rockets")
								BREAK
								
								CASE ciVEH_WEP_SPEED_BOOST
									rageSprite = RADAR_TRACE_BOOST
									tl15BlipNameOverride = "BLIP_354"
									PRINTLN("[JS] [VEHICLE WEAPONS] Setting custom blip name - Speed Boost")
								BREAK
								
								CASE ciVEH_WEP_GHOST
									rageSprite = RADAR_TRACE_PICKUP_GHOST
									tl15BlipNameOverride = "BLIP_484"
									PRINTLN("[JS] [VEHICLE WEAPONS] Setting custom blip name - Ghost")
								BREAK
								
								CASE ciVEH_WEP_BEAST
									rageSprite = RADAR_TRACE_PICKUP_ARMOURED
									tl15BlipNameOverride = "BLIP_487"
									PRINTLN("[JS] [VEHICLE WEAPONS] Setting custom blip name - Armoured")
								BREAK
								
								CASE ciVEH_WEP_FORCE_ACCELERATE
									rageSprite = RADAR_TRACE_PICKUP_ACCELERATOR
									tl15BlipNameOverride = "BLIP_483"
									PRINTLN("[JS] [VEHICLE WEAPONS] Setting custom blip name - Accelerator")
								BREAK
								
								CASE ciVEH_WEP_FLIPPED_CONTROLS
									rageSprite = RADAR_TRACE_PICKUP_SWAP
									IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
										tl15BlipNameOverride = "FPU_SR"
									ELSE
										tl15BlipNameOverride = "PU_SW2"
									ENDIF
									PRINTLN("[JS] [VEHICLE WEAPONS] Setting custom blip name - Flipped")
								BREAK
								
								CASE ciVEH_WEP_ZONED
									rageSprite = RADAR_TRACE_PICKUP_ZONED
									tl15BlipNameOverride = "PU_BT"
									PRINTLN("[JS] [VEHICLE WEAPONS] Setting custom blip name - Zoned")
								BREAK
								
								CASE ciVEH_WEP_DETONATE
									rageSprite = RADAR_TRACE_PICKUP_DETONATOR
									tl15BlipNameOverride = "BLIP_485"
									PRINTLN("[JS] [VEHICLE WEAPONS] Setting custom blip name - Detonator")
								BREAK
								
								CASE ciVEH_WEP_BOMB
									rageSprite = RADAR_TRACE_PICKUP_BOMB
									tl15BlipNameOverride = "BLIP_486"
									PRINTLN("[JS] [VEHICLE WEAPONS] Setting custom blip name - Bomb")
								BREAK
								
								CASE ciVEH_WEP_BOUNCE
									rageSprite = RADAR_TRACE_PICKUP_JUMP
									tl15BlipNameOverride = "BLIP_515"
									PRINTLN("[JS] [VEHICLE WEAPONS] Setting custom blip name - Hop")
								BREAK
								
								CASE ciVEH_WEP_PRON
									rageSprite = RADAR_TRACE_PICKUP_DEADLINE
									tl15BlipNameOverride = "BLIP_522"
									PRINTLN("[JS] [VEHICLE WEAPONS] Setting custom blip name - Deadline")
								BREAK
								
								CASE ciVEH_WEP_REPAIR
									//repair
									rageSprite = RADAR_TRACE_PICKUP_REPAIR
									tl15BlipNameOverride = "BLIP_524"
									PRINTLN("[JS] [VEHICLE WEAPONS] Setting custom blip name - Repair")
								BREAK
								
								CASE ciVEH_WEP_BOMB_LENGTH
									//Bomb Length
									IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwpn].iVehBombLengthIncrease < 0
										rageSprite = RADAR_TRACE_PICKUP_DTB_BLAST_DECREASE
										tl15BlipNameOverride = "BLIP_623"
									ELSE
										rageSprite = RADAR_TRACE_PICKUP_DTB_BLAST_INCREASE
										tl15BlipNameOverride = "BLIP_622"
									ENDIF
									
									PRINTLN("[JS] [VEHICLE WEAPONS] Setting custom blip name - Bomb Length")
								BREAK
								
								CASE ciVEH_WEP_BOMB_MAX
									//Bomb Max	
									IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwpn].iVehBombMaxIncrease < 0
										rageSprite = RADAR_TRACE_PICKUP_DTB_BOMB_DECREASE
										tl15BlipNameOverride = "BLIP_625"
									ELSE
										rageSprite = RADAR_TRACE_PICKUP_DTB_BOMB_INCREASE
										tl15BlipNameOverride = "BLIP_624"
									ENDIF
									
									PRINTLN("[JS] [VEHICLE WEAPONS] Setting custom blip name - Bomb Max")
								BREAK
								
								CASE ciVEH_WEP_EXTRA_LIFE
									//Increase life
									rageSprite = RADAR_TRACE_PICKUP_DTB_HEALTH
									tl15BlipNameOverride = "BLIP_621"
									PRINTLN("[JS] [VEHICLE WEAPONS] Setting custom blip name - Extra Life")
								BREAK
								
								CASE ciVEH_WEP_RANDOM
									rageSprite = RADAR_TRACE_PICKUP_RANDOM
									PRINTLN("[JS] [VEHICLE WEAPONS] Setting custom blip name - Random")
								BREAK
								
								CASE ciVEH_WEP_MACHINE_GUN
									rageSprite = RADAR_TRACE_PICKUP_MACHINEGUN
									PRINTLN("[JS] [VEHICLE WEAPONS] Setting custom blip name - Machine gun")
								BREAK
								
								CASE ciVEH_WEP_RANDOM_SPECIAL_VEH
									rageSprite = RADAR_TRACE_STEERINGWHEEL
									tl15BlipNameOverride = "BLIP_RSV"
									PRINTLN("[JS] [VEHICLE WEAPONS] Setting custom blip name - Random special Veh")
								BREAK
								
								CASE ciVEH_WEP_RUINER_SPECIAL_VEH
									rageSprite = RADAR_TRACE_EX_VECH_3
									PRINTLN("[JS] [VEHICLE WEAPONS] Setting custom blip name - Ruiner")
								BREAK
								
								CASE ciVEH_WEP_RAMP_SPECIAL_VEH
									RageSprite = RADAR_TRACE_EX_VECH_4
									PRINTLN("[JS] [VEHICLE WEAPONS] Setting custom blip name - Ramp Buggy")
								BREAK
							ENDSWITCH
							
							IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwpn].iVehicleWeaponPickupType = -1
							AND g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwpn].iCustomPickupType = ciCUSTOM_PICKUP_TYPE__NONE
								rageSprite = GET_CORRECT_BLIP_SPRITE_FMMC(tempPickup)
								PRINTLN("[JS] [POWERPLAY][VEHICLE WEAPONS] no rage/vehicle pickup type set")
								
							ELIF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwpn].iCustomPickupType != ciCUSTOM_PICKUP_TYPE__NONE
								rageSprite = GET_CORRECT_BLIP_SPRITE_FMMC(tempPickup, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwpn].iSubType, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwpn].iCustomPickupType)
							ENDIF
								
						ENDIF
						
						SET_BLIP_SPRITE(biPickup[iwpn], rageSprite)
						
						IF NOT IS_STRING_NULL_OR_EMPTY(tl15BlipNameOverride)
							SET_BLIP_NAME_FROM_TEXT_FILE(biPickup[iwpn], tl15BlipNameOverride)
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						IF bDebugBlipPrints
							PRINTLN("[LH][WEP_BLIPS] 9 - PICKUP is not custom - getting normal sprite [", iwpn,"]")
						ENDIF #ENDIF
						SET_BLIP_SPRITE(biPickup[iwpn], GET_CORRECT_BLIP_SPRITE_FMMC(tempPickup))
					ENDIF
				ELSE
					PRINTLN("[ML] Pickup: ", iwpn, " - ciFMMC_WEP_Hide_Blip is SET, so not done blip logic")				
				ENDIF
			
				#IF IS_DEBUG_BUILD
				IF bDebugBlipPrints
					PRINTLN("[LH][WEP_BLIPS] 10 - Finishing up with final settings. [", iwpn,"]")
				ENDIF #ENDIF
				
				IF NOT IS_PICKUP_TYPE_INVALID_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwpn].pt)
					SET_WEAPON_BLIP_NAME_FROM_PICKUP_TYPE(biPickup[iwpn], g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwpn].pt)
				ENDIF
				
				IF DOES_BLIP_EXIST(biPickup[iwpn])
					//Handle showing height on the blip
					BOOL bShowBlipHeight = TRUE
				
					IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA(g_FMMC_STRUCT.iAdversaryModeType)
						VECTOR vPlayerPos = GET_ENTITY_COORDS(LocalPlayerPed)
						IF GET_DISTANCE_BETWEEN_COORDS(vPlayerPos, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwpn].vPos, FALSE) >= 15.0
							bShowBlipHeight = FALSE
						ENDIF
					ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_POWER_PLAY(g_FMMC_STRUCT.iAdversaryModeType)
						bShowBlipHeight = FALSE
					ENDIF
				
				
					SHOW_HEIGHT_ON_BLIP(biPickup[iwpn], bShowBlipHeight)
				
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciUSE_SMALL_WEP_BLIPS)
					AND NOT (tempPickup = PICKUP_CUSTOM_SCRIPT)
						SET_BLIP_SCALE(biPickup[iwpn], BLIP_SIZE_NETWORK_PICKUP)
					ELSE
						IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
						AND (g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwpn].iVehicleWeaponPickupType = ciVEH_WEP_RANDOM_SPECIAL_VEH
						OR g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwpn].iVehicleWeaponPickupType = ciVEH_WEP_RUINER_SPECIAL_VEH
						OR g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwpn].iVehicleWeaponPickupType = ciVEH_WEP_RAMP_SPECIAL_VEH)
							SET_BLIP_SCALE(biPickup[iwpn], 1)
							PRINTLN("[WEP BLIP] Altering scale of special vehicle blips")
						ELSE
							SET_BLIP_SCALE(biPickup[iwpn], BLIP_SIZE_NETWORK_PICKUP_LARGE)
						ENDIF
					ENDIF
					SET_BLIP_AS_SHORT_RANGE(biPickup[iwpn],TRUE)
					IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwpn].iCustomPickupType = ciCUSTOM_PICKUP_TYPE__NONE
					AND g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwpn].iVehicleWeaponPickupType = -1
						SET_BLIP_PRIORITY(biPickup[iwpn],BLIPPRIORITY_LOWEST)
					ELSE
						//Rage pick ups set to have the highest priority
						SET_BLIP_PRIORITY(biPickup[iwpn],BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
						PRINTLN("[JS] [RAGEPICKUPS]Setting Rage pickup - BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH")
					ENDIF
					
					IF IS_PICKUP_HEALTH(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwpn].pt)
						SET_BLIP_COLOUR(biPickup[iwpn], BLIP_COLOUR_GREEN)
					ELIF IS_PICKUP_ARMOUR(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwpn].pt)
					OR g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwpn].pt = PICKUP_CUSTOM_SCRIPT
						IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwpn].iCustomPickupType = ciCUSTOM_PICKUP_TYPE__NONE
							SET_BLIP_COLOUR(biPickup[iwpn], BLIP_COLOUR_BLUE)
						ENDIF
					ENDIF
					
					//If it's a rage pickup now colour it white - 2776907
					IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwpn].iCustomPickupType != ciCUSTOM_PICKUP_TYPE__NONE
						SET_BLIP_COLOUR(biPickup[iwpn], BLIP_COLOUR_WHITE)
					ENDIF
				ENDIF
			ELSE
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_POWER_PLAY(g_FMMC_STRUCT.iAdversaryModeType)	
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(LocalPlayerPed), g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwpn].vPos) <= 15.0
						SHOW_HEIGHT_ON_BLIP(biPickup[iwpn],TRUE)
					ELSE
						SHOW_HEIGHT_ON_BLIP(biPickup[iwpn],FALSE)
					ENDIF
				ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA(g_FMMC_STRUCT.iAdversaryModeType)
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(LocalPlayerPed), g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwpn].vPos, FALSE) >= g_FMMC_STRUCT.iBlipHeightIndicationDistance
						SHOW_HEIGHT_ON_BLIP(biPickup[iwpn],FALSE)
					ELSE
						SHOW_HEIGHT_ON_BLIP(biPickup[iwpn],TRUE)
					ENDIF
				ENDIF
				
				IF g_FMMC_STRUCT.iAdversaryModeType != 0
					IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_MAIN)
					OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_TUNNEL)
					OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_BACK_AREA)
					OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_HOTEL_FLOOR)
					OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_LOADING_BAY)
					OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_VAULT)
					OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_UTILITY_LIFT)
					OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_LIFT_SHAFT)
						OBJECT_INDEX puObject = GET_PICKUP_OBJECT(pipickup[iwpn])
						IF DOES_PICKUP_OBJECT_EXIST(pipickup[iwpn])
						AND DOES_ENTITY_EXIST(puObject)
						AND NOT IS_PAUSE_MENU_ACTIVE()
						AND NOT g_PlayerBlipsData.bBigMapIsActive
							VECTOR vObjPos = GET_ENTITY_COORDS(puObject)
							VECTOR vPlayerPos = GET_ENTITY_COORDS(PlayerPedToUse)
							IF GET_CASINO_INTERIOR_FLOOR(vObjPos.z) = GET_CASINO_INTERIOR_FLOOR(vPlayerPos.z)
								SET_BLIP_DISPLAY(biPickup[iwpn], DISPLAY_BLIP)
							ELSE
								SET_BLIP_DISPLAY(biPickup[iwpn], DISPLAY_NOTHING)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(biPickup[iwpn])
			#IF IS_DEBUG_BUILD
			IF bDebugBlipPrints
				PRINTLN("[KH][WEAPON PICKUPS][BLIPS] - PROCESS_PICKUP_BLIPS_AND_ATTACHMENTS [", iwpn,"] - REMOVING BLIP")
			ENDIF #ENDIF
			REMOVE_BLIP(biPickup[iwpn])
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE: This function checks for the specific subset of gear that is required on the Ornate and
///    Biolabs heists, and if the criteria is met, it will display a help message.
FUNC BOOL HAS_SPECIFIC_HEIST_EQUIPMENT_BEEN_GIVEN_TO_TEAM(INT iTeamToCheck)
	
	BOOL bReturn = FALSE
	
	IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
	OR Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
		// Biolab
		IF (IS_LONG_BIT_SET(g_FMMC_STRUCT.biGearAvailableBitset[iTeamToCheck], ENUM_TO_INT(HEIST_GEAR_REBREATHER))
			AND IS_LONG_BIT_SET(g_FMMC_STRUCT.biGearAvailableBitset[iTeamToCheck], ENUM_TO_INT(HEIST_GEAR_NIGHTVISION)))
			// Ornate
		OR (IS_LONG_BIT_SET(g_FMMC_STRUCT.biGearAvailableBitset[iTeamToCheck], ENUM_TO_INT(HEIST_GEAR_BALACLAVA))
			AND IS_LONG_BIT_SET(g_FMMC_STRUCT.biGearAvailableBitset[iTeamToCheck], ENUM_TO_INT(HEIST_GEAR_WHITE_HOCKEY_MASK)))
				bReturn = TRUE
		ENDIF
	ENDIF
	
	RETURN bReturn
	
ENDFUNC

/// PURPOSE: This function will trigger godtext explaining how to deliver something to an apartment when the player gets near to an apartment drop off
PROC MANAGE_HELP_TEXT_FOR_APARTMENT_DELIVERY()

	
	IF NOT IS_BIT_SET( iLocalBoolCheck5, LBOOL5_SHOWN_APARTMENT_GET_AND_DELIVER_HELP )
	
		INT iTeam = MC_playerBD[ iPartToUse ].iteam
		
		IF (MC_ServerBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES)
			IF (g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[MC_ServerBD_4.iCurrentHighestPriority[iTeam]] = ciFMMC_DROP_OFF_TYPE_PROPERTY)
				IF (NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed,TRUE))
				AND (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD( LocalPlayerPed, GET_PROPERTY_DROP_OFF_COORDS( eApartmentBuzzerEntrance ,TRUE) , FALSE ) <= 50)
					//Commented out for 2066785 
					//SHOW_APARTMENT_GET_AND_DELIVER_HELPTEXT()
					SET_BIT( iLocalBoolCheck5, LBOOL5_SHOWN_APARTMENT_GET_AND_DELIVER_HELP )
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	
ENDPROC

/// PURPOSE: This function is used to bring up any mid-mission help text during heists
PROC DO_HEIST_TUTORIAL_HELP_TEXT()
	
	MANAGE_HELP_TEXT_FOR_APARTMENT_DELIVERY()
	
	IF NOT IS_BIT_SET( iLocalBoolCheck4, LBOOL4_HEIST_HELP_TEXT_1 )
		// This will bring up text for special equipment given out for a particular heist
		IF HAS_SPECIFIC_HEIST_EQUIPMENT_BEEN_GIVEN_TO_TEAM( MC_playerBD[ iPartToUse ].iteam )
		AND HAS_NET_TIMER_STARTED( MC_serverBD.tdMissionLengthTimer ) // And it'll wait 20 seconds after the mission starts
		AND GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( MC_serverBD.tdMissionLengthTimer ) > ( 15 * 1000 )	
			PRINT_HELP( "HEIST_HELP_8", DEFAULT ) // "Equipment for the current Heist has been added to your Inventory menu."
		ENDIF
		SET_BIT( iLocalBoolCheck4, LBOOL4_HEIST_HELP_TEXT_1 )
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_HEIST_HELP_TEXT_AVI_RASHKOVSKY)
		
		IF iCurrentHeistMissionIndex = HBCA_BS_PRISON_BREAK_FINALE
		OR iCurrentHeistMissionIndex = HBCA_BS_PACIFIC_STANDARD_WITSEC
			
			GROUP_INDEX	grpiPlayerGroupIndex
			PED_INDEX	tempPed
			INT 		iHasGroupLeader
			INT 		iNumberOfFollowers
			INT			iGroupLoop = 1
			
			grpiPlayerGroupIndex = GET_PLAYER_GROUP(LocalPlayer)
			GET_GROUP_SIZE(grpiPlayerGroupIndex, iHasGroupLeader, iNumberOfFollowers)
			IF iNumberOfFollowers >= 1
				FOR iGroupLoop = 0 TO (iNumberOfFollowers-1)
					tempPed = GET_PED_AS_GROUP_MEMBER(grpiPlayerGroupIndex, iGroupLoop)
					IF GET_ENTITY_MODEL(tempPed) = IG_MONEY
						PRINT_HELP("HEIST_HELP_60")
						SET_BIT(iLocalBoolCheck10, LBOOL10_HEIST_HELP_TEXT_AVI_RASHKOVSKY)
					ELIF GET_ENTITY_MODEL(tempPed) = IG_RASHCOSVKI
						PRINT_HELP("HESIT_HELP_61")
						SET_BIT(iLocalBoolCheck10, LBOOL10_HEIST_HELP_TEXT_AVI_RASHKOVSKY)
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC


PROC PROCESS_SINGLE_FRAME_PARTICIPANT_UPDATE()
	
	PRE_PARTICIPANT_PROCESSING()
	
	INT iPartLoop = 0
	FOR iPartLoop = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1

		IF bIsLocalPlayerHost
			SERVER_PARTICIPANT_PROCESSING( iPartLoop )
			SERVER_MAINTAIN_MC_LEADERBOARD_FOR_PARTICIPANT( iPartLoop,g_MissionControllerserverBD_LB.sleaderboard )
		ENDIF
		
		LOCAL_PARTICIPANT_PROCESSING( iPartLoop )
		
	ENDFOR
	
	POST_PARTICIPANT_PROCESSING()
	
ENDPROC


PROC MANAGE_HOST_MIGRATION()

	IF bIsLocalPlayerHost
		IF IS_BIT_SET(iLocalBoolCheck,WAS_NOT_THE_HOST)
			
			PROCESS_SINGLE_FRAME_PARTICIPANT_UPDATE()
			
			INT iTeam = 0
			
			FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
				RECALCULATE_OBJECTIVE_LOGIC( iTeam )
			ENDFOR
			
			CWARNINGLN(DEBUG_CONTROLLER, "[RCC MISSION] HOST MIGRATED TO ME") 
			CLEAR_BIT(iLocalBoolCheck,WAS_NOT_THE_HOST)
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(iLocalBoolCheck,WAS_NOT_THE_HOST)
			CWARNINGLN(DEBUG_CONTROLLER, "[RCC MISSION] NO LONGER THE HOST - new host = ", NATIVE_TO_INT(NETWORK_GET_HOST_OF_THIS_SCRIPT()))
		SET_BIT(iLocalBoolCheck,WAS_NOT_THE_HOST)
	ENDIF
	ENDIF

ENDPROC

PROC STORE_PV_PLAYER_IS_USING()
	
	IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
	AND GET_MC_SERVER_GAME_STATE() = GAME_STATE_RUNNING
	
		g_TransitionSessionNonResetVars.iHashOfLastUsedPVInMission = -1
		
		VEHICLE_INDEX VehicleID
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VehicleID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		ELSE
			VehicleID = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
		ENDIF	
		IF NOT DOES_ENTITY_EXIST(VehicleID)
			VehicleID = GET_PLAYERS_LAST_VEHICLE()
		ENDIF
		
		IF DOES_ENTITY_EXIST(VehicleID)
		AND NOT IS_ENTITY_DEAD(VehicleID)
		AND IS_VEHICLE_A_PERSONAL_VEHICLE(VehicleID)
			
			// are we within 50m of the vehicle?
			IF (VDIST2(GET_PLAYER_COORDS(PLAYER_ID()), GET_ENTITY_COORDS(VehicleID)) < 2500.0)
			
				g_TransitionSessionNonResetVars.iHashOfLastUsedPVInMission = GET_HASH_OF_PERSONAL_VEHICLE(VehicleID)
				
				PRINTLN("STORE_PV_PLAYER_IS_USING - setting g_TransitionSessionNonResetVars.iHashOfLastUsedPVInMission = ", g_TransitionSessionNonResetVars.iHashOfLastUsedPVInMission )
			
			ELSE
				PRINTLN("STORE_PV_PLAYER_IS_USING - too far from pv " )
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				IF NOT DOES_ENTITY_EXIST(VehicleID)
					PRINTLN("STORE_PV_PLAYER_IS_USING - entity not exist " )
				ELIF IS_ENTITY_DEAD(VehicleID)
					PRINTLN("STORE_PV_PLAYER_IS_USING - entity dead " )
				ELIF NOT IS_VEHICLE_A_PERSONAL_VEHICLE(VehicleID)
					PRINTLN("STORE_PV_PLAYER_IS_USING - bot a pv " )	
				ENDIF
			#ENDIF
		ENDIF

	ENDIF

ENDPROC

PROC MANAGE_HIDING_HUD_WAITING_FOR_GODTEXT()
	IF GET_MC_SERVER_GAME_STATE() < GAME_STATE_RUNNING AND NOT IS_BIT_SET( iLocalBoolCheck9, LBOOL9_HAS_FIRST_OBJECTIVE_TEXT_BEEN_SHOWN )
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Hiding HUD this frame because godtext hasn't been shown yet.")
		DISABLE_HUD()
	ENDIF
ENDPROC

PROC MAINTAIN_HIDING_HUD_FOR_VERSUS()
	IF Is_Player_Currently_On_MP_Versus_Mission(LocalPlayer)
		INT iMyTeam = MC_playerBD[ iPartToUse ].iteam
		INT iMyRule = MC_serverBD_4.iCurrentHighestPriority[ iMyTeam ]
	
		//if we have a timer, don't display HUD until timer begins
		IF MC_serverBD_4.iCurrentHighestPriority[ iMyTeam ] < FMMC_MAX_RULES
			IF GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION( g_FMMC_STRUCT.sFMMCEndConditions[ iMyTeam ].iObjectiveTimeLimitRule[ iMyRule ] ) > FMMC_OBJECTIVE_TIME_UNLIMITED
				IF NOT HAS_NET_TIMER_STARTED( MC_serverBD_3.tdObjectiveLimitTimer[ iMyTeam ] ) AND iMyRule = 0
				AND NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iMyTeam)
					IF ((GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION( g_FMMC_STRUCT.sFMMCEndConditions[ iMyTeam ].iObjectiveTimeLimitRule[ iMyRule ] ) )  - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( MC_serverBD_3.tdObjectiveLimitTimer[ iMyTeam ] ) - MC_serverBD_3.iTimerPenalty[iMyTeam]) < 0
					AND NOT IS_BIT_SET(iLocalBoolCheck17, LBOOL17_HUD_HIDDEN_AT_START_ONCE) 	// url:bugstar:2794611 - Power Play IV: player's radar briefly disappeared when a Raging pickup active on them expired
						PRINTLN("[MMacK][NoHUD] Versus Mission Timer Not Started")
						DISABLE_DPADDOWN_THIS_FRAME()
						DISABLE_CELLPHONE_THIS_FRAME_ONLY()
						DISABLE_SELECTOR_THIS_FRAME() 
						CLEAR_INVENTORY_BOX_CONTENT()
						
						IF Is_MP_Objective_Text_On_Display()
							Pause_Objective_Text()
						ENDIF
					
						IF NOT IS_THIS_SPECTATOR_CAM_RUNNING(g_BossSpecData.specCamData)
						OR NOT IS_PLAYER_SCTV(LocalPlayer)
							DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_ALL_OVERHEADS)
							HIDE_HUD_AND_RADAR_THIS_FRAME()
						ENDIF
					ENDIF
				ELSE	
					SET_BIT(iLocalBoolCheck17, LBOOL17_HUD_HIDDEN_AT_START_ONCE)	//url:bugstar:2794611 - Power Play IV: player's radar briefly disappeared when a Raging pickup active on them expired
					IF NOT Is_MP_Objective_Text_On_Display()
						Unpause_Objective_Text()
					ENDIF
				ENDIF
			ELSE
				IF NOT Is_MP_Objective_Text_On_Display()
					Unpause_Objective_Text()
				ENDIF
			ENDIF					
		ENDIF
		
		IF GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
		OR IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		OR (SHOULD_HIDE_THE_HUD_THIS_FRAME() AND NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE())
		OR (g_bMissionEnding AND NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE())
			IF GET_REASON_HUD_IS_HIDDEN() != ciHUDHIDDEN_MANUALLY_HIDDEN_HUD //Don't disable the D-pad down menu if the hud is just manually hidden
				DISABLE_DPADDOWN_THIS_FRAME()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_IN_WANTED_BLOCKING_ZONE()
	//AND NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_IN_BLOCK_WANTED_LEVEL_ZONE_TEMP)
	
	IF (IS_BIT_SET(iLocalBoolCheck13, LBOOL13_IN_BLOCK_WANTED_LEVEL_ZONE) OR IS_BIT_SET(iLocalBoolCheck13, LBOOL13_IN_BLOCK_AND_CLEAR_WANTED_LEVEL_ZONE))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_ROUNDS_LBD()
	INT iPartLoop = 0
	FOR iPartLoop = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() - 1
		SERVER_MAINTAIN_ROUNDS_LBD(iPartLoop, g_MissionControllerserverBD_LB.sleaderboard)
	ENDFOR
ENDPROC

FUNC BOOL SHOULD_DRAW_ROUNDS_LBD()
	RETURN IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_ROUNDS_LBD_DO)
ENDFUNC

PROC SERVER_ROUNDS_LBD_LOGIC()

	SWITCH MC_serverBD_3.iRoundLbdProgress

		CASE 0
			IF SHOULD_DRAW_ROUNDS_LBD()
				SET_BIT(MC_serverBD.iServerBitSet2, SBBOOL2_ROUNDS_PAUSE_LBD_SORT) // Stop calls to (SERVER_MAINTAIN_MC_LEADERBOARD_FOR_PARTICIPANT)
				MC_serverBD_3.iRoundLbdProgress++
				PRINTLN("[2460167] [CS_RNDS] [HOST] SERVER_ROUNDS_LBD_LOGIC, SBBOOL2_ROUNDS_PAUSE_LBD_SORT)  ")
			ENDIF
		BREAK
		
		CASE 1
			MC_serverBD_3.iWinningPlayerForRounds = GET_LEADERBOARD_INDEX_OF_TOP_WINNER()
			IF MC_serverBD_3.iWinningPlayerForRounds = -1
				MC_serverBD_3.iWinningPlayerForRounds = 0
			ENDIF
			MC_serverBD_3.iWinningPlayerForRoundsId = g_MissionControllerserverBD_LB.sleaderboard[MC_serverBD_3.iWinningPlayerForRounds].playerID
			CLEAR_THE_LEADERBOARD_STRUCT_SERVER_DATA(FALSE)
			PROCESS_ROUNDS_LBD()
			MC_serverBD_3.iRoundLbdProgress++
			PRINTLN("[2460167] [CS_RNDS] [HOST] SERVER_ROUNDS_LBD_LOGIC, >>> 2 ")
		BREAK
		
		CASE 2 
			PROCESS_ROUNDS_LBD()
			IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_ROUNDS_LBD_GO)
				PRINTLN("[2460167] [CS_RNDS] [HOST] SERVER_ROUNDS_LBD_LOGIC, SBBOOL2_ROUNDS_LBD_GO  ")
				SET_BIT(MC_serverBD.iServerBitSet2, SBBOOL2_ROUNDS_LBD_GO)
			ENDIF
		BREAK
	
	ENDSWITCH
ENDPROC

PROC DELETE_ANY_EMPTY_NEARBY_VEHICLES(BOOL bOnlyDeleteIfAlive = FALSE, BOOL bOnlyDeleteBikesIfFd = FALSE, BOOL bDontDeleteBikes = FALSE)
	      
	// check any nearby vehicles
	VEHICLE_INDEX VehicleID[64] 
	INT iNumVehicles
	iNumVehicles = GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), VehicleID)
	PRINTLN("DELETE_ANY_EMPTY_NEARBY_VEHICLES = iNumVehicles = ", iNumVehicles, "  || bOnlyDeleteIfAlive: ", bOnlyDeleteIfAlive, " | bOnlyDeleteBikesIfFd: ", bOnlyDeleteBikesIfFd, " | bDontDeleteBikes: ", bDontDeleteBikes)
	
	VECTOR vCoords
	
	INT i = 0
	BOOL bSetAsMissionEntity
	REPEAT iNumVehicles i
		IF DOES_ENTITY_EXIST(VehicleID[i])
			
			IF IS_VEHICLE_EMPTY(VehicleID[i], FALSE, DEFAULT, DEFAULT, TRUE)    
				
				IF bOnlyDeleteIfAlive
					IF NOT IS_ENTITY_ALIVE(VehicleID[i])
						PRINTLN("DELETE_ANY_EMPTY_NEARBY_VEHICLES - Relooping because the vehicle is dead and bOnlyDeleteIfAlive is set. Vehicle: ", i)
						RELOOP
					ENDIF
				ENDIF
				
				IF bOnlyDeleteBikesIfFd
					IF NOT IS_VEHICLE_FUCKED_MP(VehicleID[i])
					AND (IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(VehicleID[i])) OR IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(VehicleID[i])))
						PRINTLN("DELETE_ANY_EMPTY_NEARBY_VEHICLES - Relooping because the bike is not f'd and bOnlyDeleteBikesIfFd is set. Vehicle: ", i)
						RELOOP
					ENDIF
				ENDIF
				
				IF bDontDeleteBikes
					IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(VehicleID[i]))
					OR IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(VehicleID[i]))
						PRINTLN("DELETE_ANY_EMPTY_NEARBY_VEHICLES - Relooping because the vehicle is not a bike and bDontDeleteBikes is set. Vehicle: ", i)
						RELOOP
					ENDIF
				ENDIF
				
				#IF IS_DEBUG_BUILD
				vCoords = GET_ENTITY_COORDS(VehicleID[i], FALSE)                        
				PRINTLN("DELETE_ANY_EMPTY_NEARBY_VEHICLES = found an empty car, i = ", i, " at coords ", vCoords)                     
				#ENDIF
				
				IF CAN_EDIT_THIS_ENTITY(VehicleID[i], bSetAsMissionEntity)
					PRINTLN("DELETE_ANY_EMPTY_NEARBY_VEHICLES = good to delete")                                  
					DELETE_VEHICLE(VehicleID[i])
				ELSE
					PRINTLN("DELETE_ANY_EMPTY_NEARBY_VEHICLES - Cannot edit.")
					IF (bSetAsMissionEntity)
					    SET_VEHICLE_AS_NO_LONGER_NEEDED(VehicleID[i])
					    PRINTLN("DELETE_ANY_EMPTY_NEARBY_VEHICLES - Marking as no longer needed.")
					ENDIF                               
				ENDIF
								
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
				OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRSHOOTOUT(g_FMMC_STRUCT.iAdversaryModeType)
				OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_EnableGameMastersMode)
					IF DOES_ENTITY_EXIST(VehicleID[i])
						vCoords = GET_ENTITY_COORDS(VehicleID[i])
						
						IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(VehicleID[i], FALSE)
						AND IS_ENTITY_A_MISSION_ENTITY(VehicleID[i])
							IF NETWORK_HAS_CONTROL_OF_ENTITY(VehicleID[i])
								DELETE_VEHICLE(VehicleID[i])
					    		PRINTLN("DELETE_ANY_EMPTY_NEARBY_VEHICLES - Deleting Vehicle.")
							ELSE
								// Broadcast that this vehicle needs to be deleted after a timer has passed.
								// Make sure to reset the timer after the broadcast has been called. AND after the event is received AND when we finish in the restart state.
								IF NOT HAS_NET_TIMER_STARTED(tdOvertimeVehicleRemovalTimer)									
									START_NET_TIMER(tdOvertimeVehicleRemovalTimer)
								ENDIF
								
								IF HAS_NET_TIMER_STARTED(tdOvertimeVehicleRemovalTimer)
									IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdOvertimeVehicleRemovalTimer, ciCOVERTIME_VEHICLE_REMOVAL_TIME)
										BROADCAST_FMMC_OVERTIME_REMOVE_VEHICLE(VEH_TO_NET(VehicleID[i]))
										RESET_NET_TIMER(tdOvertimeVehicleRemovalTimer)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						CLEAR_AREA_OF_VEHICLES(vCoords, 15.0, DEFAULT, DEFAULT, FALSE, FALSE, TRUE)
					ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
				OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRSHOOTOUT(g_FMMC_STRUCT.iAdversaryModeType)
					PRINTLN("DELETE_ANY_EMPTY_NEARBY_VEHICLES - Vehicle not empty..")					
				ENDIF
				#ENDIF
			ENDIF
		ENDIF       
	ENDREPEAT
ENDPROC

PROC PROCESS_VESPUCCI_REMIX_SPAWN_VEHICLE_CLEANUP()
	
	IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_VESPUCCI_JOB_REMIX()
		EXIT
	ENDIF
	
	IF iLocalPart != -1
	AND MC_playerBD[iLocalPart].iteam != 1
		EXIT
	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		EXIT
	ENDIF
	
	VEHICLE_INDEX viCurrentVehicle = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
	
	IF viVespucciRemixVehicle = NULL
		viVespucciRemixVehicle = viCurrentVehicle
	ENDIF
	
	IF viVespucciRemixVehicle != viCurrentVehicle
		SET_VEHICLE_AS_NO_LONGER_NEEDED(viVespucciRemixVehicle)
		viVespucciRemixVehicle = viCurrentVehicle
		PRINTLN("PROCESS_VESPUCCI_REMIX_SPAWN_VEHICLE_CLEANUP - Updating cached vespucci vehicle and setting old one to no longer needed")
	ENDIF
	
ENDPROC

PROC SET_UP_SVM_HIGHER_JUMP_CAR()
	INT i
	IF NOT IS_BIT_SET(iLocalBoolCheck21, LBOOL21_SET_CAR_HIGHER_JUMP)
		IF MC_serverBD.iNumVehCreated > 0
		AND MC_serverBD.iNumVehCreated <= FMMC_MAX_VEHICLES
			FOR i = 0 TO (MC_serverBD.iNumVehCreated-1)
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
					VEHICLE_INDEX ThisVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
					IF IS_VEHICLE_DRIVEABLE(ThisVeh)
						PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - vehicle: ", i," ciFMMC_VEHICLE5_INCREASE_JUMP_HEIGHT - check: ", IS_BIT_SET(iLocalBoolCheck21, LBOOL21_SET_CAR_HIGHER_JUMP))
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetFive, ciFMMC_VEHICLE5_INCREASE_JUMP_HEIGHT)
							PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - vehicle: ", i," ciFMMC_VEHICLE5_INCREASE_JUMP_HEIGHT - SET")
							
							IF GET_CAR_HAS_JUMP(ThisVeh)		
								PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - vehicle: ", i," Is a jump car, calling: SET_USE_HIGHER_CAR_JUMP")
								SET_USE_HIGHER_CAR_JUMP(ThisVeh, TRUE)
								
							ELSE
								PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - vehicle: ", i," Is not a jump car, this is weird to set this option for this vehicle.")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
			SET_BIT(iLocalBoolCheck21, LBOOL21_SET_CAR_HIGHER_JUMP)
		ENDIF		
	ENDIF
ENDPROC

PROC DISABLE_RUINER_ROCKETS()

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciDISABLE_RUINER_ROCKETS)
		EXIT
	ENDIF
	
	VEHICLE_INDEX vehPlayer
	IF NOT IS_PED_INJURED(LocalPlayerPed)
		IF IS_PED_IN_ANY_VEHICLE( LocalPlayerPed )
			vehPlayer = GET_VEHICLE_PED_IS_IN( LocalPlayerPed )
		ELSE
			IF NETWORK_DOES_NETWORK_ID_EXIST(netRespawnVehicle)
				vehPlayer = NET_TO_VEH(netRespawnVehicle)
			ENDIF
		ENDIF
		
		IF IS_ENTITY_ALIVE(vehPlayer)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(vehPlayer)
			AND IS_VEHICLE_MODEL(vehPlayer, RUINER2)
				CPRINTLN( DEBUG_CONTROLLER,"[RCC MISSION][DISABLE_RUINER_ROCKETS] - DISABLED")
				SET_PED_CAN_SWITCH_WEAPON(PLAYER_PED_ID(), FALSE) 
				DISABLE_VEHICLE_WEAPON(TRUE,WEAPONTYPE_DLC_VEHICLE_RUINER_ROCKET, vehPlayer, LocalPlayerPed)
				SET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_VEHICLE_RUINER_BULLET)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_OBJECT_PICKUP_STATUS(INT iObj, OBJECT_INDEX tempObj)

	IF NOT IS_BIT_SET(MC_serverBD.iFragableCrate, iobj)
	AND NOT IS_BIT_SET(MC_serverBD.iWasHackObj,iobj)
	AND NOT IS_BIT_SET( iObjDeliveredBitset, iobj )
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciPOINTLESS_MODE_ENABLED)
	AND NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
	AND NOT HAS_PLAYER_QUIT_MISSION(iLocalPart)
		INT iTeam
		FOR iteam = 0 to (MC_serverBD.iNumberOfTeams-1)
			IF IS_TEAM_ACTIVE(iteam)
				IF MC_serverBD_4.iObjPriority[iobj][iteam] <= MC_serverBD_4.iCurrentHighestPriority[iteam]
					IF MC_serverBD_4.iObjPriority[iobj][iteam] < FMMC_MAX_RULES
						IF MC_serverBD_4.iObjRule[iobj][iteam] != FMMC_OBJECTIVE_LOGIC_NONE
							IF NOT HAS_TEAM_FAILED(iteam)
								IF DOES_OBJECT_RULE_TYPE_REQUIRE_PICKUP(MC_serverBD_4.iObjRule[iobj][iteam], (IS_BIT_SET(MC_serverBD.iObjAtYourHolding[iteam],iobj) OR IS_BIT_SET(iLocalObjAtHolding, iObj)))
									IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
									OR (IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH) AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciNEXT_DELIVERY_WINS_SD_ONLY))
										PRINTLN("PROCESS_EVERY_FRAME_DROP_OFF - Processing collectable pickup. Team: ", iTeam, " Object: ", iObj)
										IF (g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn != INT_TO_ENUM(MODEL_NAMES, HASH("pil_Prop_Pilot_Icon_01")))
										AND (g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn != INT_TO_ENUM(MODEL_NAMES, HASH("prop_ic_cp_bag")))
										AND (g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn = INT_TO_ENUM(MODEL_NAMES, HASH("ar_prop_ar_cp_bag")))
										AND NOT DOES_PICKUP_NEED_BIGGER_RADIUS(iobj)
											SET_ENTITY_LOD_DIST(tempObj,150)
										ENDIF
									
										IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSet, cibsOBJ_Reset_If_Dropped_Out_Of_Bounds)
										AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
										AND IS_BIT_SET(iObjHeldBS, iobj)
										AND NOT IS_ENTITY_ATTACHED_TO_ANY_PED(tempObj)
										AND IS_OBJECT_OUTSIDE_TEAM_MISSION_BOUNDS(iobj, tempObj, iTeam)
											PRINTLN("[RCC MISSION] PROCESS_EVERY_FRAME_DROP_OFF - RESETTING OBJECT - Object ", iobj)
											//Reset object
											SET_UP_FMMC_OBJ_RC(tempObj,iobj,MC_ServerBD_1.sFMMC_SBD.niPed, MC_ServerBD_1.sLEGACYMissionContinuityVars,TRUE,TRUE)
											RUGBY_CENTRE_LINE_OBJECT_RESPAWN(tempObj)	//CENTRE LINE RESPAWN - Respawn the get & deliver object along the centre line (Bug 2700219)
										ENDIF
										
										IF NOT IS_THIS_A_SPECIAL_PICKUP( iObj )
										AND IS_OBJECT_A_PORTABLE_PICKUP(tempObj)
										AND NOT IS_BIT_SET(iLocalBoolCheck17, LBOOL17_PREVENT_PICKUP_OF_PICKUP)
										
											//SET_ENTITY_ALPHA(tempObj,255,FALSE)
											SET_TEAM_PICKUP_OBJECT(tempObj,iteam,TRUE)
											SET_ENTITY_COLLISION(tempObj, TRUE)
											IF CONTENT_IS_USING_ARENA()
											#IF IS_DEBUG_BUILD
											AND NOT bHideObjects
											#ENDIF
												SET_ENTITY_VISIBLE(tempObj, TRUE)
											ENDIF
											PREVENT_COLLECTION_OF_PORTABLE_PICKUP(tempObj, FALSE)
											PRINTLN("PROCESS_EVERY_FRAME_DROP_OFF - Setting obj ", iObj, " to team pickup for team ", iTeam)
										ELSE
											IF IS_BIT_SET(iLocalBoolCheck17, LBOOL17_PREVENT_PICKUP_OF_PICKUP)
												PRINTLN("PROCESS_EVERY_FRAME_DROP_OFF - ELSE - LBOOL17_PREVENT_PICKUP_OF_PICKUP")
											ENDIF
											IF NOT IS_OBJECT_A_PORTABLE_PICKUP(tempObj)
												PRINTLN("PROCESS_EVERY_FRAME_DROP_OFF - ELSE - NOT IS_OBJECT_A_PORTABLE_PICKUP")
											ENDIF
											IF IS_THIS_A_SPECIAL_PICKUP(iObj)
												PRINTLN("PROCESS_EVERY_FRAME_DROP_OFF - ELSE - IS_THIS_A_SPECIAL_PICKUP")
											ENDIF
										ENDIF
										
										IF IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH) AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciNEXT_DELIVERY_WINS_SD_ONLY)
											INT iTeamPickup
											FOR iTeamPickup = 0 TO (MC_serverBD.iNumberOfTeams-1)
												IF IS_BIT_SET(MC_serverBD.iObjAtYourHolding[iTeamPickup],iObj)
												AND IS_TEAM_ACTIVE(iTeamPickup)
													SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM(tempObj,iobj,iteam)
												ENDIF
											ENDFOR
										ENDIF
									ENDIF
									
								ELSE
									PRINTLN("PROCESS_EVERY_FRAME_DROP_OFF  - SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM Call 1 Team ", iTeam," Obj: ", iObj)
									SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM(tempObj,iobj,iteam)
								ENDIF
							ELSE
								PRINTLN("PROCESS_EVERY_FRAME_DROP_OFF - SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM Call 2 Team ", iTeam," Obj: ", iObj)
								SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM(tempObj,iobj,iteam)
							ENDIF
						ELSE
							PRINTLN("PROCESS_EVERY_FRAME_DROP_OFF - SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM Call 3 Team ", iTeam," Obj: ", iObj)
							SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM(tempObj,iobj,iteam)
						ENDIF
					ELSE
						PRINTLN("PROCESS_EVERY_FRAME_DROP_OFF - SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM Call 4 Team ", iTeam," Obj: ", iObj)
						SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM(tempObj,iobj,iteam)
					ENDIF
				ELSE
					IF NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_RETURNING_A_FLAG)
						PRINTLN("PROCESS_EVERY_FRAME_DROP_OFF - SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM Call 5 Team ", iTeam," Obj: ", iObj)
						SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM(tempObj,iobj,iteam)
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
ENDPROC

FUNC BOOL IS_EVERY_FRAME_DROP_OFF_OK_TO_DROP()
	
	IF MC_serverBD_4.iObjMissionLogic[MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER 
		IF IS_ENTITY_IN_AIR(LocalPlayerPed)
			PRINTLN("IS_EVERY_FRAME_DROP_OFF_OK_TO_DROP - In Air")
			RETURN FALSE
		ENDIF
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			IF NOT IS_VEHICLE_ON_ALL_WHEELS(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
				PRINTLN("IS_EVERY_FRAME_DROP_OFF_OK_TO_DROP - Not on 4 wheels")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_ARENA_CTF(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

PROC PROCESS_EVERY_FRAME_DROP_OFF()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_FAST_DROP_OFF)
		INT iObj
		
		FOR iObj = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfObjects-1
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
				OBJECT_INDEX tempObj = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
				INT iTeam = MC_PlayerBD[iLocalPart].iTeam
				
				PROCESS_OBJECT_PICKUP_STATUS(iObj, tempObj)
				
				IF NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
				AND IS_ENTITY_ATTACHED_TO_ANY_PED(tempObj)
					IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
						IF MC_serverBD_4.iObjPriority[iobj][iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam]
							IF NOT HAS_NET_TIMER_STARTED(tdEveryFrameDropOffDelay[iobj])
								PRINTLN("[RCC MISSION] PROCESS_EVERY_FRAME_DROP_OFF - starting tdEveryFrameDropOffDelay")
								REINIT_NET_TIMER(tdEveryFrameDropOffDelay[iobj])
							ELIF HAS_NET_TIMER_EXPIRED(tdEveryFrameDropOffDelay[iobj], ciEVERY_FRAME_DROP_OFF_DELAY_TIME)	
								IF IS_ENTITY_IN_DROP_OFF_AREA(tempObj, iTeam, MC_serverBD_4.iObjPriority[iobj][iTeam], ciRULE_TYPE_OBJECT, iobj)
									IF HAS_NET_TIMER_STARTED(tdDropOff)
										IF HAS_NET_TIMER_EXPIRED(tdDropOff, iDropTimer)
											IF IS_EVERY_FRAME_DROP_OFF_OK_TO_DROP()
												PRINTLN("[RCC MISSION] PROCESS_EVERY_FRAME_DROP_OFF - Setting bit for iDetachedObjectSuccessfully. iobj: ", iobj)
												SET_BIT(iDetachedObjectSuccessfully, iObj)								
												SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM(tempObj,iObj,iTeam)
												SET_BIT(iLocalObjAtHolding, iObj)
												RESET_NET_TIMER(tdDropOff)
												PRINTLN("[RCC MISSION] PROCESS_EVERY_FRAME_DROP_OFF - Setting bit for iLocalObjAtHolding. iobj: ", iobj)
											ENDIF
										ENDIF
									ELSE
										REINIT_NET_TIMER(tdDropOff)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						IF HAS_TEAM_FAILED(iteam)
							SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM(tempObj,iObj,iTeam)
							PRINTLN("[RCC MISSION] PROCESS_EVERY_FRAME_DROP_OFF - Team has failed, dropping pickup ", iObj," now")
						ENDIF
					ENDIF
				ELSE
					IF HAS_NET_TIMER_STARTED(tdEveryFrameDropOffDelay[iobj])
						PRINTLN("[RCC MISSION] PROCESS_EVERY_FRAME_DROP_OFF - resetting tdEveryFrameDropOffDelay")
						RESET_NET_TIMER(tdEveryFrameDropOffDelay[iobj])
					ENDIF
					IF IS_BIT_SET(iDetachedObjectSuccessfully, iObj)
						PRINTLN("[RCC MISSION] PROCESS_EVERY_FRAME_DROP_OFF - Detatching portable pickup! We are in drop off area. iobj: ", iobj)
						MC_playerBD[iLocalPart].iNumberOfDeliveries++
						MC_PlayerBD[iLocalPart].iPackagesAtHolding++
						PRINTLN("[RCC MISSION] PROCESS_EVERY_FRAME_DROP_OFF - Incrementing my delivery count to: ", MC_playerBD[iLocalPart].iNumberOfDeliveries)
						CLEAR_BIT(iDetachedObjectSuccessfully, iObj)
						INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
						IF iRule > -1
						AND iRule < FMMC_MAX_RULES
						AND NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
							IF NOT IS_ENTITY_IN_DROP_OFF_AREA(tempObj, iTeam, MC_serverBD_4.iObjPriority[iobj][iTeam], ciRULE_TYPE_OBJECT, iobj)
								VECTOR vTempPos = GET_ENTITY_COORDS(tempObj)
								VECTOR vNewPos = GET_SAFE_PICKUP_COORDS(GET_CENTER_OF_AREA(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[iRule], g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff2[iRule]),0.5,3)
								IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[iRule] = ciFMMC_DROP_OFF_TYPE_CYLINDER
									vNewPos = GET_SAFE_PICKUP_COORDS(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[iRule])
								ENDIF
								vNewPos.z = vTempPos.z
								PRINTLN("[RCC MISSION] PROCESS_EVERY_FRAME_DROP_OFF - Object ", iObj, " has been delivered but not in the area. Moving to ", vNewPos)
								SET_ENTITY_COORDS(tempObj, vNewPos)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(MC_serverBD.iObjAtYourHolding[iTeam],iobj)
				AND IS_BIT_SET(iLocalObjAtHolding, iObj)
					CLEAR_BIT(iLocalObjAtHolding, iObj)
					PRINTLN("[RCC MISSION] PROCESS_EVERY_FRAME_DROP_OFF - Clearing iLocalObjAtHolding for obj ", iObj, " letting server take over")
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

PROC PROCESS_SYNC_CHECKPOINT_TRANSITION_VARIABLES()
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet,SBBOOL_MISSION_OVER)
		EXIT
	ENDIF
	
	INT iTeam = 0 	
	FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
		IF g_TransitionSessionNonResetVars.iRetryMultiRuleTimeStamp[iTeam] != MC_ServerBD_4.iTransitionSessionMultirule_TimeStamp[iTeam]
		AND MC_ServerBD_4.iTransitionSessionMultirule_TimeStamp[iTeam] != 0 // We don't want to clear these when the mission starts, or this hasn't yet been set.
			PRINTLN("[PROCESS_SYNC_CHECKPOINT_TRANSITION_VARIABLES] Updating iRetryMultiRuleTimeStamp to = ", MC_ServerBD_4.iTransitionSessionMultirule_TimeStamp[iTeam])
			g_TransitionSessionNonResetVars.iRetryMultiRuleTimeStamp[iTeam] = MC_ServerBD_4.iTransitionSessionMultirule_TimeStamp[iTeam]			
		ENDIF
		
		IF g_TransitionSessionNonResetVars.iRetryMultiRuleTimeStamp_Checkpoint[iTeam] != MC_ServerBD_4.iTransitionSessionMultirule_Checkpoint[iTeam]
		AND MC_ServerBD_4.iTransitionSessionMultirule_Checkpoint[iTeam] != 0 // We don't want to clear these when the mission starts, or this hasn't yet been set.
			PRINTLN("[PROCESS_SYNC_CHECKPOINT_TRANSITION_VARIABLES] Updating iRetryMultiRuleTimeStamp_Checkpoint to = ", MC_ServerBD_4.iTransitionSessionMultirule_Checkpoint[iTeam])
			g_TransitionSessionNonResetVars.iRetryMultiRuleTimeStamp_Checkpoint[iTeam] = MC_ServerBD_4.iTransitionSessionMultirule_Checkpoint[iTeam]
		ENDIF
	ENDFOR
ENDPROC

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: PROCESS MAIN STATE PROCEDURE !
//
//************************************************************************************************************************************************************

FUNC INT GET_FIRST_VEHICLE_FROM_OTHER_PLAYERS_NEEDED_TO_REPAIR()
	INT iPart
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		PARTICIPANT_INDEX piPart = INT_TO_PARTICIPANTINDEX(iPart)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(piPart)
			IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)
				IF MC_playerBD_1[iPart].iVehicleOwnerNeededToRepair > -1
				AND iPart != iPartToUse
				AND MC_PlayerBD[iPart].iTeam = MC_PlayerBD[iPartToUse].iTeam
					
					PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)
					PED_INDEX pedPlayer = GET_PLAYER_PED(piPlayer)
					
					IF NOT IS_PED_INJURED(pedPlayer)
					AND IS_PED_IN_ANY_VEHICLE(pedPlayer)
						PRINTLN("[RCC MISSION][ModShopProg] - iPart: ", iPart, " Needs to repair iVehicle: ", MC_playerBD_1[iPart].iVehicleOwnerNeededToRepair)
						RETURN MC_playerBD_1[iPart].iVehicleOwnerNeededToRepair
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	RETURN -1
ENDFUNC

PROC PROCESS_SHOPS_FOR_INSTANCED_CONTENT()
	INT iTeam = MC_playerBD[iPartToUse].iteam
	IF iTeam > -1 AND iTeam < FMMC_MAX_TEAMS
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		IF iRule < FMMC_MAX_RULES
			
			INT iMissionEntityID = -1
			INT iClientStage 
			iClientStage = GET_MC_CLIENT_MISSION_STAGE(iLocalPart)
			IF iClientStage = CLIENT_MISSION_STAGE_DELIVER_VEH
				PRINTLN("[RCC MISSION][ModShopProg] - CLIENT_MISSION_STAGE_DELIVER_VEH")
				
				IF IS_NET_PLAYER_OK(LocalPlayer)
					PRINTLN("[RCC MISSION][ModShopProg] - Player Ok.")
					
					IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)	
						FLOAT fPercentage
						VEHICLE_INDEX viTempVeh
						viTempVeh = GET_VEHICLE_PED_IS_IN(localPlayerPed)							
						iMissionEntityID = IS_ENTITY_A_MISSION_CREATOR_ENTITY(viTempVeh)
							
						PRINTLN("[RCC MISSION][ModShopProg] - iMissionEntityID = ", iMissionEntityID)
													
						IF iMissionEntityID != -1								
							fPercentage = GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(viTempVeh, iMissionEntityID, MC_serverBD.iTotalNumStartingPlayers)
						ELSE
							fPercentage = GET_VEHICLE_HEALTH_PERCENTAGE(viTempVeh)
						ENDIF
						
						PRINTLN("[MMacK][ModShopProg] Limit is : ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinVehHealth[iRule])
						PRINTLN("[MMacK][ModShopProg] Car Is : ", fPercentage)
						
						IF fPercentage > g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinVehHealth[iRule]
							PRINTLN("[MMacK][ModShopProg] Car Is over Health Limit")
							iMissionEntityID = -1
						ELSE
							PRINTLN("[MMacK][ModShopProg] Car Is under Health Limit")
						ENDIF
					ENDIF
				ENDIF
			
		
				IF MC_playerBD_1[iLocalPart].iVehicleOwnerNeededToRepair != iMissionEntityID
					MC_playerBD_1[iLocalPart].iVehicleOwnerNeededToRepair = iMissionEntityID
					PRINTLN("[LM][[ModShopProg] - new value for iVehicleOwnerNeededToRepair: ", MC_playerBD_1[iLocalPart].iVehicleOwnerNeededToRepair)
				ENDIF
				
				IF iMissionEntityID = -1
					INT iTempV = GET_FIRST_VEHICLE_FROM_OTHER_PLAYERS_NEEDED_TO_REPAIR()
					IF MC_playerBD_1[iLocalPart].iVehicleNeededToRepair != iTempV
						MC_playerBD_1[iLocalPart].iVehicleNeededToRepair = iTempV
						PRINTLN("[LM][[ModShopProg] - new value for iVehicleNeededToRepair: ", 	MC_playerBD_1[iLocalPart].iVehicleNeededToRepair)
					ENDIF					
				ENDIF
			ELSE
				MC_playerBD_1[iLocalPart].iVehicleNeededToRepair = -1
			ENDIF
			
			PRINTLN("[RCC MISSION][ModShopProg] - iVehicleNeededToRepair: ", MC_playerBD_1[iLocalPart].iVehicleNeededToRepair)
			
			// Might want to expand this so that clients can remotely chck that other clients need to respray or change license plate...			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_BLOCK_PROGRESSION_BF_MODSHOP_REPAIR)
				IF (NOT IS_BIT_SET(g_iBS1_Mission, ciBS1_Mission_PurchasedVehicleRespray) AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_BLOCK_PROGRESSION_BF_MODSHOP_RESPRAY))
				OR (NOT IS_BIT_SET(g_iBS1_Mission, ciBS1_Mission_PurchasedVehiclePlateChange) AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_BLOCK_PROGRESSION_BF_MODSHOP_LICENSE_CHNG))
					PRINTLN("[RCC MISSION][ModShopProg] - Setting PBBOOL_OBJECTIVE_BLOCKER. Waiting to modify the vehicle. g_iBS1_Mission: ", g_iBS1_Mission)
					SET_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
				ENDIF				
			ENDIF
		ENDIF
	ENDIF
	
	MAINTAIN_REDUCED_APARTMENT_CREATION_FLAG() //2120195 sets flag to reduce amount of things created by interior script while on heist mission
	MAINTAIN_DISABLED_APARTMENT_ACCESS_FOR_COPS(FALSE) //2120101 sets flag to prevent access to apartment while on team rule to lose wanted
ENDPROC

PROC PROCESS_SECUROHACK_OBJECTIVE_CONTROL()
	
	IF !bShouldStartControl
		IF iHackingPedHelpBitSet[0] + iHackingPedHelpBitSet[1] > 0
		OR iHackingVehHelpBitSet > 0
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ROOF)
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING)
				OR IS_FAKE_MULTIPLAYER_MODE_SET()
					IF NOT IS_PED_BEING_STUNNED(LocalPlayerPed)
						IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH("appSecuroHack")) = 0
						OR IS_FAKE_MULTIPLAYER_MODE_SET()
							IF NOT IS_PLAYER_RESPAWNING(LocalPlayer)
							AND NOT IS_PED_DEAD_OR_DYING(LocalPlayerPed)
								LAUNCH_CELLPHONE_APPLICATION( AppDummyApp0, TRUE, TRUE, FALSE )
								PRINTLN("[JS][SECUROHACK] Launching App")
							ENDIF
						ENDIF
					ENDIF
				ELSE
					bShouldStartControl = TRUE
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciFORCE_OBJECTIVE_MID_POINT_ON_HACK)
					IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[iLocalPart].iTeam], MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam])
						BROADCAST_OBJECTIVE_MID_POINT(MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam], MC_playerBD[iLocalPart].iTeam)
					ENDIF
				ENDIF
			ENDIF
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING)
				IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH("appSecuroHack")) > 0
				AND IS_PHONE_ONSCREEN(TRUE))
				OR IS_FAKE_MULTIPLAYER_MODE_SET()
					bShouldStartControl = TRUE
					PRINTLN("[JS][SECUROHACK] App launched")
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciFORCE_OBJECTIVE_MID_POINT_ON_HACK)
						IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[iLocalPart].iTeam], MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam])
							BROADCAST_OBJECTIVE_MID_POINT(MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam], MC_playerBD[iLocalPart].iTeam)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING)
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH("appSecuroHack")) = 0
			AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
				bShouldStartControl = FALSE
				PRINTLN("[JS][SECUROHACK] Turning bShouldStartControl off as app is closed")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING)
	
		IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH("appSecuroHack")) > 0
			AND IS_PHONE_ONSCREEN(TRUE)
				IF NOT IS_AUDIO_SCENE_ACTIVE("dlc_xm_deluxos_hacking_scene")
					START_AUDIO_SCENE("dlc_xm_deluxos_hacking_scene")
				ENDIF
			ELSE
				IF IS_AUDIO_SCENE_ACTIVE("dlc_xm_deluxos_hacking_scene")
					STOP_AUDIO_SCENE("dlc_xm_deluxos_hacking_scene")
				ENDIF
			ENDIF
		ENDIF
	
		INT iPedLoop
		BOOL bSetProofs = FALSE
		IF (MC_serverBD_4.iPedExplodeKillOnCaptureBS[0] > 0 AND iHackingPedInRangeBitSet[0] > 0)
			FOR iPedLoop = 0 TO 31
				IF IS_BIT_SET(MC_serverBD_4.iPedExplodeKillOnCaptureBS[GET_LONG_BITSET_INDEX(iPedLoop)], GET_LONG_BITSET_BIT(iPedLoop))
					IF IS_BIT_SET(iHackingPedInRangeBitSet[GET_LONG_BITSET_INDEX(iPedLoop)], GET_LONG_BITSET_BIT(iPedLoop))
						bSetProofs = TRUE
						PRINTLN("[JS][SECUROHACK] In range of ped ",iPedLoop," due to explode")
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
		IF (MC_serverBD_4.iPedExplodeKillOnCaptureBS[1] > 0 AND  iHackingPedInRangeBitSet[1] > 0)
			FOR iPedLoop = 32 TO FMMC_MAX_PEDS - 1
				IF IS_BIT_SET(MC_serverBD_4.iPedExplodeKillOnCaptureBS[GET_LONG_BITSET_INDEX(iPedLoop)], GET_LONG_BITSET_BIT(iPedLoop))
					IF IS_BIT_SET(iHackingPedInRangeBitSet[GET_LONG_BITSET_INDEX(iPedLoop)], GET_LONG_BITSET_BIT(iPedLoop))
						bSetProofs = TRUE
						PRINTLN("[JS][SECUROHACK] In range of ped ",iPedLoop," due to explode")
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
		
		IF bSetProofs	
			IF NOT IS_BIT_SET(iLocalBoolCheck20, LBOOL20_PLAYER_PROOFS_ALTERED)
				PRINTLN("[JS][SECUROHACK] SETTING PROOFS")
				RUN_LOCAL_PLAYER_PROOF_SETTING(DEFAULT, TRUE)
				IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_USING(LocalPlayerPed)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
						SET_ENTITY_PROOFS(tempVeh, FALSE, FALSE, TRUE, FALSE, FALSE, FALSE)
					ENDIF
				ENDIF
				SET_BIT(iLocalBoolCheck20, LBOOL20_PLAYER_PROOFS_ALTERED)
			ENDIF
		ELSE
			IF IS_BIT_SET(iLocalBoolCheck20, LBOOL20_PLAYER_PROOFS_ALTERED)
				PRINTLN("[JS][SECUROHACK] CLEARING PROOFS")
				RUN_LOCAL_PLAYER_PROOF_SETTING(TRUE)
				IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_USING(LocalPlayerPed)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
						SET_ENTITY_PROOFS(tempVeh, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)
					ENDIF
				ENDIF
				CLEAR_BIT(iLocalBoolCheck20, LBOOL20_PLAYER_PROOFS_ALTERED)
			ENDIF
		ENDIF
		
		BOOL bPlaySounds = FALSE
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH("appSecuroHack")) > 0
		AND IS_PHONE_ONSCREEN(TRUE)
			bPlaySounds = TRUE
		ENDIF
		
		IF NOT HAS_NET_TIMER_STARTED(tdHackingComplete)
			IF iHackPercentage >= 100
				IF bPlaySounds
					IF IS_BIT_SET(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
						STOP_SOUND(iHackingLoopSoundID)
						RELEASE_SOUND_ID(iHackingLoopSoundID)
						iHackingLoopSoundID = -1
						CLEAR_BIT(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
						
						iHackSuccessSoundProgress++
					ENDIF
					
					IF NOT GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
						PLAY_SOUND_FRONTEND(-1, "Hack_Stop", "DLC_IE_SVM_Voltic2_Hacking_Sounds")
					ENDIF
				ENDIF
				
				PRINTLN("[JS][SECUROHACK] Hack over")
				START_NET_TIMER(tdHackingComplete)
				
				iHackStage = ciSECUROSERV_HACK_STAGE_HACK_COMPLETE
				
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED ("FHHUD_DELUXOHACK")
					CLEAR_HELP()
					PRINTLN("[TMS][SECUROHACK] Clearing FHHUD_DELUXOHACK because the hack is complete!")
				ENDIF
			ELSE 
				IF NOT (iHackPercentage > 0)
					IF IS_BIT_SET(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
						STOP_SOUND(iHackingLoopSoundID)
						RELEASE_SOUND_ID(iHackingLoopSoundID)
						iHackingLoopSoundID = -1
						CLEAR_BIT(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
					ENDIF
				ENDIF
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciDISPLAY_HACK_PROGRESS_BAR)
					IF iHackingPedInRangeBitSet[0] = 0
					AND iHackingPedInRangeBitSet[1] = 0
					AND iHackingVehInRangeBitSet = 0
						IF iHackStage != ciSECUROSERV_HACK_STAGE_NO_SIGNAL
						AND iHackStage != ciSECUROSERV_HACK_STAGE_HACK_COMPLETE
							PRINTLN("[JS][SECUROHACK] not good, setting stage to no signal")
							PLAY_SOUND_FRONTEND(-1, "Hack_Stop", "DLC_IE_SVM_Voltic2_Hacking_Sounds")
							iHackStage = ciSECUROSERV_HACK_STAGE_NO_SIGNAL
							iHackPercentage = 0
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ELSE
			IF iHackPercentage >= 100
			AND HAS_NET_TIMER_EXPIRED(tdHackingComplete, 500)
				IF bPlaySounds
				AND iHackSuccessSoundProgress > -1
					IF NOT IS_BIT_SET(iHackSuccessSoundPlayedBS, iHackSuccessSoundProgress)
					
						IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
							PRINTLN("[SECUROHACK] Playing success sound!")
							PLAY_SOUND_FRONTEND(-1, "Hack_Complete", "dlc_xm_deluxos_hacking_Hacking_Sounds")
						ELSE
							PLAY_SOUND_FRONTEND(-1, "Hack_Complete", "DLC_IE_SVM_Voltic2_Hacking_Sounds")
						ENDIF
						
						SET_BIT(iHackSuccessSoundPlayedBS, iHackSuccessSoundProgress)
					ENDIF
				ENDIF
				
				iHackPercentage = 0
			ENDIF
			
			IF HAS_NET_TIMER_EXPIRED(tdHackingComplete, ciHACKING_COMPLETE_TIME)
				IF iHackStage != ciSECUROSERV_HACK_STAGE_HACKING
				AND iHackStage != ciSECUROSERV_HACK_STAGE_LOSING_SIGNAL
					PRINTLN("[JS][SECUROHACK] Going back to state 0")
					
					IF bPlaySounds
						IF NOT IS_HACK_STOP_SOUND_BLOCKED()
							IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
								PLAY_SOUND_FRONTEND(-1, "Hack_Stop", "dlc_xm_deluxos_hacking_Hacking_Sounds")
							ELSE
								PLAY_SOUND_FRONTEND(-1, "Hack_Stop", "DLC_IE_SVM_Voltic2_Hacking_Sounds")
							ENDIF
						ENDIF
					ENDIF
					
					iHackStage = ciSECUROSERV_HACK_STAGE_NO_SIGNAL
					HANG_UP_AND_PUT_AWAY_PHONE()
				ENDIF
				RESET_NET_TIMER(tdHackingComplete)
			ENDIF
		ENDIF
		IF !bPlaySounds
		OR iHackStage = ciSECUROSERV_HACK_STAGE_NO_SIGNAL
			IF IS_BIT_SET(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
				STOP_SOUND(iHackingLoopSoundID)
				RELEASE_SOUND_ID(iHackingLoopSoundID)
				iHackingLoopSoundID = -1
				CLEAR_BIT(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_INFINITE_AMMO_CREATOR_SETTING()
	IF HAS_CREATOR_AMMO_BEEN_SET_TO_INFINITE(ciSTARTING_WEAPON_GRENADE)
		IF DOES_PLAYER_HAVE_WEAPON(WEAPONTYPE_GRENADE)
			SET_PED_INFINITE_AMMO(PLAYER_PED_ID(), TRUE, WEAPONTYPE_GRENADE)
		ENDIF
	ENDIF
	IF HAS_CREATOR_AMMO_BEEN_SET_TO_INFINITE(ciSTARTING_WEAPON_PARACHUTE)
		IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE, g_FMMC_STRUCT.sFMMCEndConditions[g_i_Mission_team].iStartingInventoryAmmo[ciSTARTING_WEAPON_PARACHUTE], FALSE, FALSE)
			PRINTLN("[TMS] Giving local player a parachute because of HAS_CREATOR_AMMO_BEEN_SET_TO_INFINITE(ciSTARTING_WEAPON_PARACHUTE)")
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_ENABLE_TEAM_COLOUR_PARACHUTES)
				g_FMMC_STRUCT.iTeamParachuteTint[g_i_Mission_team] = g_i_Mission_team
			ENDIF
			
			IF g_FMMC_STRUCT.iTeamParachuteTint[g_i_Mission_team] != -1
				SET_PLAYER_PARACHUTE_CANOPY(PLAYER_ID(), g_FMMC_STRUCT.iTeamParachuteTint[g_i_Mission_team])
			ENDIF
			GIVE_RESERVE_PARACHUTE_TO_PLAYER(g_FMMC_STRUCT.iTeamParachuteTint[g_i_Mission_team])
			SET_PACKED_STAT_BOOL(PACKED_MP_STAT_HAS_RESERVE_PARACHUTE, TRUE)
			IF g_FMMC_STRUCT.iTeamParachutePackTint[g_i_Mission_team] != -1
				SET_PLAYER_PARACHUTE_VARIATION_OVERRIDE(PLAYER_ID(), PED_COMP_HAND, GET_PARACHUTE_DRAWABLE_FOR_MP(PLAYER_PED_ID(), g_FMMC_STRUCT.iTeamParachutePackTint[g_i_Mission_team]), GET_PARACHUTE_TEXTURE_FOR_MP(PLAYER_PED_ID(), g_FMMC_STRUCT.iTeamParachutePackTint[g_i_Mission_team]))
				SET_MP_PARACHUTE_BAG_OVERRIDE(PLAYER_ID(), g_FMMC_STRUCT.iTeamParachutePackTint[g_i_Mission_team])
				SET_PLAYER_PARACHUTE_PACK_TINT_INDEX(PLAYER_ID(), GET_PARACHUTE_PALETTE_FOR_MP(PLAYER_PED_ID(), g_FMMC_STRUCT.iTeamParachutePackTint[g_i_Mission_team]))
				EQUIP_STORED_MP_PARACHUTE(PLAYER_ID(), g_FMMC_STRUCT.iTeamParachuteTint[g_i_Mission_team], g_FMMC_STRUCT.iTeamParachutePackTint[g_i_Mission_team])
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SPECIAL_VEHICLE_WEAPON_GLOBAL_OPTIONS()
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciDISABLE_SPECIAL_VEHICLE_WEAPONS)
		AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciDISABLE_SPECIAL_VEHICLE_WEAPONS_EXCEPT_PICKUPS)
			DISABLE_SPECIAL_VEHICLE_WEAPONS(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
			DRAW_GTA_RACE_WEAPON_INFO()
		ENDIF
	ELSE
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciDISABLE_SPECIAL_VEHICLE_WEAPONS)
		AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciDISABLE_SPECIAL_VEHICLE_WEAPONS_EXCEPT_PICKUPS)
			eLastVehiclePickup = VEHICLE_PICKUP_INIT
		ENDIF
		IF IS_BIT_SET(iLocalBoolCheck23, LBOOL23_VEHICLE_SET_EXPLOSION_PROOF)
			PRINTLN("[JS] - Resetting player proofs, no longer in vehicle")
			CLEAR_BIT(iLocalBoolCheck23, LBOOL23_VEHICLE_SET_EXPLOSION_PROOF)
			RUN_LOCAL_PLAYER_PROOF_SETTING(TRUE)
		ENDIF
	ENDIF
ENDPROC

PROC CLEAR_PLAYER_IN_POSSESSION_OF_MISSION_CRITICAL_ENTITY()
	IF IS_ENTITY_DEAD( PLAYER_PED_ID() )
		IF IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
			SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY(FALSE)
			CPRINTLN( DEBUG_MISSION, "[RCC MISSION] Clearing SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY as the local player is dead" )
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SET_MISSION_AS_NOT_JOINABLE()
	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_SET_NOT_JOINABLE)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		IF NOT IS_BIT_SET(iLocalBoolCheck, LBOOL_MISSION_SET_NOT_JOINABLE)
			SET_FM_MISSION_AS_NOT_JOINABLE()
			SET_BIT(iLocalBoolCheck,LBOOL_MISSION_SET_NOT_JOINABLE)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_WEAPONS_EVERY_FRAME()
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciHIDE_BLIP_PICKUPS)
		#IF IS_DEBUG_BUILD
		IF bDebugBlipPrints
			PRINTLN("[LH][WEP_BLIPS] 0 - We should draw Pickups")
		ENDIF #ENDIF
		IF g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons > 0	
		AND g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons <= FMMC_MAX_WEAPONS
			#IF IS_DEBUG_BUILD
			IF bDebugBlipPrints
				PRINTLN("[LH][WEP_BLIPS] 1 - Entering Iterator")
			ENDIF #ENDIF
			IF iWeaponIterator >= g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons
				#IF IS_DEBUG_BUILD
				IF bDebugBlipPrints
					PRINTLN("[LH][WEP_BLIPS] 1.5 - Resetting Iterator")
				ENDIF #ENDIF
				iWeaponIterator = 0
			ENDIF
			IF iWeaponIterator < g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons
				#IF IS_DEBUG_BUILD
				IF bDebugBlipPrints
					PRINTLN("[LH][WEP_BLIPS] 2 - PROCESS_PICKUP_BLIPS_AND_ATTACHMENTS")
				ENDIF #ENDIF
				PROCESS_PICKUP_BLIPS_AND_ATTACHMENTS( iWeaponiterator )
				iWeaponIterator++
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("After weapon pickup processing")
		#ENDIF
		#ENDIF
		
	ENDIF

	IF g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons > 0	
		IF iWeaponSpawnIterator >= g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons
			iWeaponSpawnIterator = 0
		ENDIF
		
		//Fix for B* 2690347 - Pickups were not being created in same order on different machines 
		IF (IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME) AND IS_TEAM_ACTIVE(0))
		OR (IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_1_HAS_NEW_PRIORITY_THIS_FRAME) AND IS_TEAM_ACTIVE(1))
		OR (IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_2_HAS_NEW_PRIORITY_THIS_FRAME) AND IS_TEAM_ACTIVE(2))
		OR (IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_3_HAS_NEW_PRIORITY_THIS_FRAME) AND IS_TEAM_ACTIVE(3))
			iWeaponSpawnIterator = 0
			INT iTeamMovedOn
			IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME)
				iTeamMovedOn = 1
			ELIF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_1_HAS_NEW_PRIORITY_THIS_FRAME)
				iTeamMovedOn = 2
			ELIF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_2_HAS_NEW_PRIORITY_THIS_FRAME)
				iTeamMovedOn = 3
			ELIF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_3_HAS_NEW_PRIORITY_THIS_FRAME)
				iTeamMovedOn = 4
			ENDIF
			PRINTLN("[KH][WEAPON PICKUPS][SPAWNING] Resetting iWeaponSpawnIterator to 0 as team: ", iTeamMovedOn," has moved to the next rule: ", MC_serverBD_4.iCurrentHighestPriority[iTeamMovedOn - 1])
		ENDIF
		
		IF IS_BIT_SET(iLocalBoolCheck17, LBOOL17_TEAM_SWAP_PICKUP_PROCESS)
			IF NOT IS_BIT_SET(iLocalBoolCheck17, LBOOL17_WEAPON_PICKUP_STAGGERED_LOOP_INDEX_SET)
				iWeaponPickupStaggeredLoopIndex = iWeaponSpawnIterator
				
				SET_BIT(iLocalBoolCheck17, LBOOL17_WEAPON_PICKUP_STAGGERED_LOOP_INDEX_SET)
			ELSE
				IF iWeaponPickupStaggeredLoopIndex = iWeaponSpawnIterator
					CLEAR_BIT(iLocalBoolCheck17, LBOOL17_TEAM_SWAP_PICKUP_PROCESS)
					
					iWeaponPickupStaggeredLoopIndex = -1
					
					CLEAR_BIT(iLocalBoolCheck17, LBOOL17_WEAPON_PICKUP_STAGGERED_LOOP_INDEX_SET)
				ENDIF
			ENDIF
		ENDIF
		
		IF iWeaponSpawnIterator < g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons 
			PROCESS_WEAPON_SPAWNING(iWeaponSpawnIterator)
			iWeaponSpawnIterator++
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_MAIN_STATE()
	INT i		
		
	#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
		OPEN_SCRIPT_PROFILE_MARKER_GROUP("PROCESS_MAIN_STATE")
		#ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_MAIN_STATE ...start")
	#ENDIF
	#ENDIF
		
	CLEAR_PLAYER_IN_POSSESSION_OF_MISSION_CRITICAL_ENTITY()
	
	PROCESS_PRE_MAIN_CHECKS()
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_PRE_MAIN_CHECKS")
	#ENDIF
	#ENDIF
	
	PROCESS_SET_MISSION_AS_NOT_JOINABLE()

	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MISSION_SET_NOT_JOINABLE")
	#ENDIF
	#ENDIF

	PROCESS_DLC_DIALOGUE_SETTINGS()
	
	MANAGE_HIDING_HUD_WAITING_FOR_GODTEXT()
	
	PLAY_CASINO_HOUSE_KEEPING_AMBIENT_DIALOGUE()
	
	PROCESS_PLAYER_USING_PLACED_PED_TAXI()
	
	MANAGE_HOST_MIGRATION()
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MANAGE_HOST_MIGRATION")
	#ENDIF
	#ENDIF
	
	PROCESS_APARTMENT_WARPING()
	
	SHOWROOM_INTERIOR_ENTITY_SET_SYSTEM()
	
	PROCESS_VEHICLE_WARP_ON_RULE_DATA()
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("SHOWROOM_INTERIOR_ENTITY_SET_SYSTEM")
	#ENDIF
	#ENDIF
	
	PROCESS_SYNC_CHECKPOINT_TRANSITION_VARIABLES()	
	
	PROCESS_SHOPS_FOR_INSTANCED_CONTENT()
	
	PROCESS_ARTIFICIAL_LIGHTS_CLIENT() // This function controls the EMP effect in The Humane Labs Raid heist mission
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_ARTIFICIAL_LIGHTS_CLIENT")
	#ENDIF
	#ENDIF
	
	PROCESS_INSTANCED_CONTENT_PORTALS(IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER))
	
	PROCESS_SECUROHACK_OBJECTIVE_CONTROL()	

	//The staggered loop begins:	
	IF iStaggeredIterator >= MC_serverBD.iMaxLoopSize // The loop has finished and we've checked all the entities, return to the beginning and start again
		iStaggeredIterator = 0
		STAGGERED_POST_ENTITY_PROCESSING()
		
		STORE_PV_PLAYER_IS_USING() // This is used to store the PV (Personal Vehicle) for quick restarts, and doesn't need to be called every frame.
	ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("POST_ENTITY_PROCESSING")
	#ENDIF
	#ENDIF
	
	//Render target for Laptop hack
	PROCESS_HACKING_LAPTOP_SCALEFORM()
	
	PROCESS_INFINITE_AMMO_CREATOR_SETTING()
	
	PROCESS_ALWAYS_SHOW_ONE_ENEMY_BLIP()
	
	IF iStaggeredIterator < MC_serverBD.iMaxLoopSize
	
		IF iStaggeredIterator = 0 // A new loop has just begun, initialise variables etc			
			STAGGERED_PRE_ENTITY_PROCESSING()
			MAINTAIN_HANDLER_HELP_TEXT()			
			MAINTAIN_HEIST_AWARD_HELP()
		ENDIF		
		
		//Processing player rules (eg: Kill Players)		
		IF MC_serverBD.iNumPlayerRuleCreated > 0
		AND MC_serverBD.iNumPlayerRuleCreated <= FMMC_MAX_RULES
		AND iStaggeredIterator < MC_serverBD.iNumPlayerRuleCreated
			SERVER_PROCESS_PLAYER_RULES_OBJECTIVE(iStaggeredIterator)
		ENDIF		

		//Processing location-related rules (eg: Go To, Leave, Capture Location)
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciEVERY_FRAME_GOTO_LOC_PROCESSING)		
			IF MC_serverBD.iNumLocCreated > 0
			AND MC_serverBD.iNumLocCreated <= FMMC_MAX_GO_TO_LOCATIONS
			AND iStaggeredIterator < MC_serverBD.iNumLocCreated
				SERVER_PROCESS_LOCATION_OBJECTIVES(iStaggeredIterator)
				LOCAL_PROCESS_LOCATION_OBJECTIVES(iStaggeredIterator)
			ENDIF
		ENDIF
		
		//Processing ped-related rules (eg: Get & Deliver, Kill, Photo Ped) as well as Ped AI/animations and some blipping		
		IF MC_serverBD.iNumPedCreated > 0
		AND MC_serverBD.iNumPedCreated <= FMMC_MAX_PEDS
			IF iStaggeredIterator < MC_serverBD.iNumPedCreated
				PROCESS_PED_BRAIN(iStaggeredIterator)
				PROCESS_PED_BODY(iStaggeredIterator)
			ENDIF
			
			//In the creator, it is possible to set up to three peds to be 'High Priority' which means that PED_BODY and PED_BRAIN will be
			// run on these peds every frame - to avoid the delay to AI that sometimes occurs due to the staggered loop
			PROCESS_HIGH_PRIORITY_PEDS()
		ENDIF		
		
		//Processing vehicle-related rules (eg: Get & Deliver, Destroy, Photo Vehicle) as well as stuck checks, door opening, blipping etc		
		IF MC_serverBD.iNumVehCreated > 0
		AND MC_serverBD.iNumVehCreated <= FMMC_MAX_VEHICLES
		AND iStaggeredIterator < MC_serverBD.iNumVehCreated	
			PROCESS_VEH_BRAIN(iStaggeredIterator)
			PROCESS_VEH_BODY(iStaggeredIterator)
		ENDIF
		
		PROCESS_TRAIN()
		HANDLE_PROPELLORS_WHEN_MISSION_ENDS()
		
		//Processing object-related rules (eg: Get & Deliver, Destroy, Photo Object) as well as special pickups, hacking minigames, blipping etc		
		IF MC_serverBD.iNumObjCreated > 0	
		AND MC_serverBD.iNumObjCreated <= FMMC_MAX_NUM_OBJECTS
		AND iStaggeredIterator < MC_serverBD.iNumObjCreated	
			PROCESS_OBJ_BRAIN(iStaggeredIterator)
			PROCESS_OBJ_BODY(iStaggeredIterator)
		ENDIF
		
		iStaggeredIterator++ // Next frame we'll check the next item along in the loop
	ENDIF
	
	//Seperate staggered loop for object blips
	IF iObjBlipIterator >= MC_serverBD.iNumObjCreated
		iObjBlipIterator = 0
	ENDIF
	
	IF MC_serverBD.iNumObjCreated > 0	
	AND MC_serverBD.iNumObjCreated <= FMMC_MAX_NUM_OBJECTS
	AND iObjBlipIterator < MC_serverBD.iNumObjCreated	
		PROCESS_OBJECT_BLIPS(iObjBlipIterator)
	ENDIF
	iObjBlipIterator++
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("POST iStaggeredIterator")
	#ENDIF
	#ENDIF	
	
	//Every frame processing begins:	
	EVERY_FRAME_PRE_ENTITY_PROCESSING() // Reset any variables which are used to tally stuff across the every frame checks
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PLAYER_PRE_ENTITY_PROCESSING")
	#ENDIF
	#ENDIF
	
	//This function draws the HUD for Kill Player rules	
	IF NOT IS_SPECTATOR_HUD_HIDDEN()
	AND NOT IS_SPECTATOR_SHOWING_NEWS_HUD()
		DISPLAY_PLAYER_KILL_HUD()
	ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("DISPLAY_PLAYER_KILL_HUD")
	#ENDIF
	#ENDIF
	
	//Reset Cross the Line data prior to each loop
	PLAYER_STATE_LIST_INIT(playerStateListDataCrossTheLine)
	
	//This function processes Locations - which Location the player is in, drawing Locations, processing of Photo Location rules, as well as drawing HUD elements for them	
	PROCESS_LOCATION_EVERY_FRAME()
	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxCapScanEffect)
		CLEAR_CAP_SCAN_VFX(TRUE)
	ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("DISPLAY_LOCATION_HUD")
	#ENDIF
	#ENDIF
	
	//This function draws dummy blips (fake blips that can draw on the HUD which aren't related to any mission entity)
	
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfDummyBlips > 0
		MANAGE_DUMMY_BLIPS()
	ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MANAGE_DUMMY_BLIPS")
	#ENDIF
	#ENDIF
	
	//This function processed Peds - Ped spawning, inventory processing, some every frame spook/aggro checks, photo processing and blipping	
	PROCESS_PEDS_EVERY_FRAME()	
	
	//This runs the Crowd Control ped minigame from The Fleeca Job and The Pacific Standard Job heist missions	
	IF ( MC_serverBD_3.sCCServerData.iNumCrowdControlPeds > 0
	AND MC_serverBD_3.sCCServerData.iNumCrowdControlPeds <= FMMC_MAX_CROWD_PEDS )
	OR ( IS_BIT_SET_ENUM( MC_serverBD_3.sCCServerData.iBitset, CCDATA_Failed ) 
	AND ( NOT IS_BIT_SET_ENUM( MC_serverBD_3.sCCServerData.iBitset, CCDATA_FailProcessed )
	OR NOT IS_BIT_SET( MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_CC_FAILCUT_FINISH ) ) )
		PROCESS_CROWD_CONTROL_MINIGAME()
	ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_CROWD_CONTROL_PEDS")
	#ENDIF
	#ENDIF
		
	// Manual respawn vehicles are dealt with separately from creator vehicles
	CHECK_MANUAL_RESPAWN_VEHICLE()

	//This function processes Vehicles - Vehicle spawning, seating preferences, photo processing and some HUD elements	
	SET_UP_SVM_HIGHER_JUMP_CAR()	
	
	PROCESS_SPECIAL_VEHICLE_WEAPON_GLOBAL_OPTIONS()
	
	PROCESS_OBJECT_TRANSFORM_TO_VEHICLE()
	
	IF CONTENT_IS_USING_ARENA()
		IF bIsLocalPlayerHost
			SERVER_CHOOSE_ARENA_ANIMATIONS(MC_serverBD_2.sCelebServer, CELEBRATION_ARENA_ANIM_FLAT, sCelebrationData.iNumPlayerFoundForWinnerScene)
		ENDIF
	ENDIF
	
	//This function processes Vehicles every frame.
	PROCESS_VEHICLES_EVERY_FRAME()	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_VEHICLES_EVERY_FRAME")
	#ENDIF
	#ENDIF	
	
	//Some heist-specific minigame processing - Cash Grab minigame from The Pacific Standard Job, Drilling from The Fleeca Job, Thermite from The Pacific Standard Job	
	MANAGE_CASH_GRAB_TAKE_AND_DROP(SHOULD_HIDE_THE_HUD_THIS_FRAME())
	MANAGE_DRILLING_AUDIO_REQUESTS()
	UPDATE_THERMITE_DRIP_EFFECT()
	UPDATE_LOCAL_THERMITE_EFFECTS() //url:bugstar:6058740
	UPDATE_THERMITE_REMAINING_HUD(SHOULD_HIDE_THE_HUD_THIS_FRAME())
	MANAGE_VAULT_DRILLING_ASSET_REQUESTS__FMMC()
	UPDATE_LOCAL_VAULT_DRILL_PTFX__FMMC(sVaultDrillData, iLocalPart, MC_serverBD_1.sFMMC_SBD.niObject)
	PROCESS_CAMERA_FILTER()
	
	PROCESS_HANDHELD_DRILL_MINIGAME(sHandHeldDrillMiniGame)
	
	//Sniperball 
	//FIND_AND_STORE_SNIPERBALL_PACKAGE done in object body. 
	FIND_AND_STORE_NO_WEAPONS_ZONE()
	PROCESS_OBJECTS_TO_EXPLODE_WHEN_OUT_OF_ZONE()

	//Display_object_hud here process Objects - Object spawning, breakable Objects, photo & other minigame (including hacking minigame) processing, and some HUD elements
	//Also in this section: Special pickup processing - Binbags from Series A Funding - Trash Truck, and the Bus Timetable from Prison Break - Station	
	PROCESS_OBJECTS_EVERY_FRAME()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_OBJECTS_EVERY_FRAME")
	#ENDIF
	#ENDIF
	
	// Various every frame checks on the local player - Outfit effects, Secondary player anims, Restriction Zone processing, vehicle slipstreaming, Trackify
	// minigame processing, Nightvision forcing on/off, Speed Kills processing	
	// Adversary Mode Processing etc.
	PROCESS_DAWN_RAID_PICKUP()
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(g_FMMC_STRUCT.iAdversaryModeType)
		PROCESS_DAWN_RAID_EVERY_FRAME_CHECKS()
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciCONDEMNED_MODE_ENABLED)
		PROCESS_CONDEMNED_MODE()
	ENDIF
	
	PROCESS_RUNNING_BACK_SUDDENDEATH_TIMER_ACTIVATION()
	
	PROCESS_TOWER_DEFENCE_MODE()
	
	PROCESS_TAG_TEAM_MODE()
	
	PROCESS_CASINO_ALARMS()
	
	PROCESS_DRONE_USAGE()
	
	PROCESS_INTERIORS_EVERY_FRAME()
	
	PROCESS_EVERY_FRAME_PLAYER_CHECKS()
		
	PROCESS_CARNAGE_BAR_SERVER()
	
	PROCESS_CARNAGE_BAR_CLIENT()
		
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		MAINTAIN_MISSION_ORBITAL_CANNON_FOR_CREATOR()
	ENDIF
	
	PROCESS_MISSION_ORBITAL_CANNON()
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_EVERY_FRAME_PLAYER_CHECKS")
	#ENDIF
	#ENDIF
		
	//Player blip processing	
	PROCESS_PLAYER_BLIPS()
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_PLAYER_BLIPS")
	#ENDIF
	#ENDIF	
	
	//This function draws the Package HUD for Get & Deliver / Collect & Hold rules
	IF SHOULD_DRAW_PACKAGE_HUD()
		DRAW_PACKAGE_HUD()
	ENDIF	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("DRAW_PACKAGE_HUD")
	#ENDIF
	#ENDIF	
	
	//This function draws the captures HUD for capture rules
	IF NOT IS_SPECTATOR_HUD_HIDDEN()
	AND NOT IS_SPECTATOR_SHOWING_NEWS_HUD()
		IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_OBJECTIVE_BLOCKER)
			DRAW_ENTITY_RESPAWN_RECAPTURE_HUD()
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("DRAW_ENTITY_RESPAWN_RECAPTURE_HUD")
	#ENDIF
	#ENDIF	
	
	//Every frame processing ends:	
	EVERY_FRAME_POST_ENTITY_PROCESSING() // Store any temp variables that were calculated during the every frame loop
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PLAYER_POST_ENTITY_PROCESSING")
	#ENDIF
	#ENDIF	
	
	//Weapon pickup processing (blips and adding attachments), have their own staggered loop		
	PROCESS_WEAPONS_EVERY_FRAME()
	
	//Prop processing - this spawns and cleans up all special props, as well as doing per-prop behaviour, like alarm sounds, flare VFX and monkey behaviours	
	// Start the timer
	IF NOT HAS_NET_TIMER_STARTED(tdspeedBoostFlashTimerCreator)
		START_NET_TIMER(tdspeedBoostFlashTimerCreator)
	ENDIF
	
	// Reset the Timer so that we return false (alpha 100)
	IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdspeedBoostFlashTimerCreator, SPEED_BOOST_FLASH_TIME_CREATOR_OFF)
		RESET_NET_TIMER(tdspeedBoostFlashTimerCreator)
		START_NET_TIMER(tdspeedBoostFlashTimerCreator)
	ENDIF
	
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		IF g_FMMC_STRUCT_ENTITIES.iNumberOfProps > 0	
		AND g_FMMC_STRUCT_ENTITIES.iNumberOfProps <= GET_FMMC_MAX_NUM_PROPS()
			IF iPropIterator >= g_FMMC_STRUCT_ENTITIES.iNumberOfProps 
				iPropIterator = 0
			ENDIF
			
			IF iPropIterator < g_FMMC_STRUCT_ENTITIES.iNumberOfProps 
				PROCESS_PROPS( iPropIterator)
				iPropIterator++
			ENDIF
			
			DO_PROP_LOOP_FOR_PRESSUREPADS(oiProps, MC_serverBD_2.sTrapInfo_Host, sTrapInfo_Local)
		ENDIF
	ELSE
		PROCESS_CLEANUP_PROPS_EARLY()
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfWorldProps > 0
	AND g_FMMC_STRUCT_ENTITIES.iNumberOfWorldProps <= GET_FMMC_MAX_WORLD_PROPS()
		LEGACY_PROCESS_WORLD_PROPS(iWorldPropIterator, sRuntimeWorldPropData)
		iWorldPropIterator++
		IF iWorldPropIterator >= g_FMMC_STRUCT_ENTITIES.iNumberOfWorldProps
			iWorldPropIterator = 0
		ENDIF
	ENDIF
		
	INT ii
	FOR ii = 0 TO FMMC_MAX_TEAMS-1
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + ii)
			SET_BIT(iLocalBoolCheck25, LBOOL25_RESET_ALL_PROPS_ON_RENDERPHASE_PAUSE)
			PRINTLN("[LM][PROCESS_PROP_EVERY_FRAME] - Setting LBOOL25_RESET_ALL_PROPS_ON_RENDERPHASE_PAUSE")
			BREAKLOOP
		ENDIF
	ENDFOR
	
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		INT iProp = 0
		FOR iProp = 0 TO FMMC_MAX_NUM_FADEOUT_PROPS_EVERY_FRAME-1
			PROCESS_PROP_CLEAN_UP_WITH_FADE(iProp)
		ENDFOR
		
		FOR iProp = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfProps-1
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
				IF iPartToUse > -1
					IF DOES_ENTITY_EXIST(oiProps[iProp])
						IF (GET_ENTITY_MODEL(oiProps[iProp]) = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_wideramp")) OR GET_ENTITY_MODEL(oiProps[iProp]) = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_ramp")))
						AND (iProp = MC_PlayerBD[iPartToUse].iCurrentPropHit[0] OR iProp = MC_PlayerBD[iPartToUse].iCurrentPropHit[1] OR iProp = MC_PlayerBD[iPartToUse].iCurrentPropHit[2])
							FORCE_VEHICLE_CAM_STUNT_SETTINGS_THIS_UPDATE()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			PROCESS_PROP_EVERY_FRAME(iProp)
			
			IF iProp >= g_FMMC_STRUCT_ENTITIES.iNumberOfProps
				IF HAS_NET_TIMER_STARTED(tdPropColourChangeDelayTimer)
				AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdPropColourChangeDelayTimer, (g_FMMC_STRUCT.iPropColourChangeDelay*1000))
					PRINTLN("[LM][PROCESS_PROP_EVERY_FRAME] - Resetting Net Timer for Prop Colour Change Delay.")
					RESET_NET_TIMER(tdPropColourChangeDelayTimer)
				ENDIF
			ENDIF
		ENDFOR
	
	// url:bugstar:4469706 - Trap Door V - Local player saw VFX from the red platforms flickering whilst in skycam once the round ended.
	// Can't wait for MC_SCRIPT_CLEANUP...
	ELIF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)	
		INT iPropEF
		FOR iPropEF = 0 TO FMMC_MAX_NUM_FADEOUT_PROPS_EVERY_FRAME-1
			IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_PropFadeoutEveryFrameIndex[iPropEF])
				REMOVE_PARTICLE_FX(ptfx_PropFadeoutEveryFrameIndex[iPropEF], TRUE)
			ENDIF
		ENDFOR
	ENDIF
	
	//url:bugstar:4325494 - Trap Door-  Being in cover when the props vanish the player just slowly floats down
	PROCESS_PROP_CLEANUP_LOGIC()
	
	IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_RESET_ALL_PROPS_ON_RENDERPHASE_PAUSE)
	AND NOT GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()
		CLEAR_BIT(iLocalBoolCheck25, LBOOL25_RESET_ALL_PROPS_ON_RENDERPHASE_PAUSE)
		PRINTLN("[LM][PROCESS_PROP_EVERY_FRAME] - Clearing LBOOL25_RESET_ALL_PROPS_ON_RENDERPHASE_PAUSE")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_FAST_DROP_OFF)
		IF NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
			PROCESS_EVERY_FRAME_DROP_OFF()
		ENDIF
	ENDIF
	
	PROCESS_EVERY_FRAME_CAPTURE_LOGIC(iTempCaptureLocation)
	
	PROCESS_PRELOOP_ARENA_TRAPS(sTrapInfo_Local)
	//Dynamic prop (DynoProp) processing - this cleans up dynoprops, as well as handling processing of any special dynoprop behaviours
	
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps > 0
	AND g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps <= FMMC_MAX_NUM_DYNOPROPS
		
		BOOL bProcessTraps = CAN_PROCESS_TRAPS()
		
		FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps - 1
			IF bIsLocalPlayerHost
				IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iRespawnOnRuleChangeBS > 0
					INT iTeam
					FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
						IF MC_serverBD_4.iCurrentHighestPriority[iTeam] > 0
							IF (MC_serverBD_4.iCurrentHighestPriority[iTeam] > 0
							AND IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam))
							OR IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_RETRY_CURRENT_RULE_TEAM_0 + iTeam)
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iRespawnOnRuleChangeBS, iTeam)
									SET_BIT(MC_serverBD.iDynoPropCleanup_NeedOwnershipBS, i)
									SET_BIT(MC_serverBD.iDynoPropShouldRespawnNowBS, i)
									PRINTLN( "[JS] Cleaning up dynoprop to respawn on rule change ", i )
									BREAKLOOP
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
			
			PROCESS_DYNOPROP_BRAIN(i)
			PROCESS_DYNOPROP_BODY(i, bProcessTraps)
		ENDFOR
		
		PROCESS_PUTTING_OUT_FIREPIT_FIRES(sTrapInfo_Local)
		
	ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("After special prop processing")
	#ENDIF
	#ENDIF
	
	PROCESS_PLACED_PTFX_EVERY_FRAME()
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET() AND Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)
		
		//If we are running an LTS in test mode, skip the normal player processing and use Martin's bots check
		
		HANDLE_BOT_TEAM_NUMBERS(MC_serverBD.iNumberOfPlayingPlayers, tempiNumberOfPlayers, iLocalBoolCheck, iTeams)
		
	ELSE
		
		//If we aren't running an LTS in test mode, run the player processing below:
		PROCESS_PLAYER_TEAM_SWAP()	
				
		IF iPartIterator >= NETWORK_GET_MAX_NUM_PARTICIPANTS() 
			iPartIterator = 0
			POST_PARTICIPANT_PROCESSING() // Store any temp variables that were calculated (eg: server broadcast data)
			
			IF NOT IS_BIT_SET( iLocalBoolCheck13, LBOOL13_FIRST_STAGGERED_LOOP_DONE_CLIENTSIDE )
				SET_BIT(iLocalBoolCheck13, LBOOL13_FIRST_STAGGERED_LOOP_DONE_CLIENTSIDE)
				CPRINTLN( DEBUG_MISSION, "[RCC MISSION] Setting LBOOL13_FIRST_STAGGERED_LOOP_DONE_CLIENTSIDE" )
			ENDIF
			
			SET_BIT(iLocalBoolCheck3,LBOOL3_RADAR_INIT)
		ENDIF
		
		IF iPartIterator < NETWORK_GET_MAX_NUM_PARTICIPANTS() 
			IF iPartIterator = 0
				PRE_PARTICIPANT_PROCESSING() // Initialise temp variables used during the staggered loop below
			ENDIF
			
			IF bIsLocalPlayerHost
				SERVER_ROUNDS_LBD_LOGIC()
				
				//Perform server checks on each participant to check if they're active / what team they're on / what they're up to
				SERVER_PARTICIPANT_PROCESSING(iPartIterator)
				
				SERVER_MAINTAIN_MC_LEADERBOARD_FOR_PARTICIPANT(iPartIterator,g_MissionControllerserverBD_LB.sleaderboard)
			ENDIF
			
			LOCAL_PARTICIPANT_PROCESSING(iPartIterator)
			
			iPartIterator++
		ENDIF
		
		PRE_EVERYFRAME_PARTLOOP_PROCESSING() // Initialise any temp variables we need to use in the every frame loop below
		
		FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
			//This every frame check is used for things that need to update faster than the staggered loop - eg: vehicle held count, cutscene players, some minigame items
			PROCESS_SERVER_PARTICIPANT_EVERY_FRAME_CHECKS(i)
		ENDFOR
		
		POST_EVERYFRAME_PARTLOOP_PROCESSING() // Store temp variables
		
	ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("After Participant processing")
	#ENDIF
	#ENDIF
	
	
	//Gang backup processing:
	
	FIND_BACKUP_SPAWN_COORDS() // If the local player has been chosen as a target for gang backup which is about to spawn, find a relevant spawn position around myself
	
	IF bIsLocalPlayerHost
		IF iGangBackupVehIterator > (MAX_BACKUP_VEHICLES-1)
			iGangBackupVehIterator = 0
		ENDIF
		
		IF iGangBackupVehIterator < MAX_BACKUP_VEHICLES
			SERVER_PROCESS_CHASE_BACKUP_VEH(iGangBackupVehIterator) // Handles spawn setup & cleanup (when too far away / when they're done with) of gang chase vehicles & peds
			
			iGangBackupVehIterator++
		ENDIF
	ENDIF
	
	FOR i = 0 TO (MAX_BACKUP_VEHICLES-1)
		IF bIsLocalPlayerHost
			CHASE_BACKUP_SPAWNING(i) // Handles the actual creation of gang chase vehicles & peds when ready
		ENDIF
		
		CHASE_BACKUP_BLIPS(i) // Shows the gang chase blips
	ENDFOR
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_CHASE_BACKUP")
	#ENDIF
	#ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciENABLE_BMB_MODE)
	
		PROCESS_BMB_UPDATE()
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("PROCESS_BMB_UPDATE")
		#ENDIF
		#ENDIF
	
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciPRON_MODE_ENABLED)
		//Get the active teams for PRON
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciUSE_PRON_POWERUP_LOGIC)
			PLAYER_INDEX CurrentPlayerId
			PED_INDEX CurrentPlayerPedId
			iActivePronTeams = 0
			FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
					CurrentPlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
					CurrentPlayerPedId = GET_PLAYER_PED(CurrentPlayerId)
					IF IS_PED_ON_ANY_BIKE(CurrentPlayerPedId)
						IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(CurrentPlayerPedId)) = SHOTARO
							SET_BIT(iActivePronTeams,MC_playerBD[i].iteam)
							PRINTLN("[PRON][VV] Player on Shotaro on team: ", MC_playerBD[i].iteam)
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
		
		FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
				INT iTeam = MC_playerBD[i].iteam
				PRINTLN("[[LH]][PRON] Participant[",i,"] is Active on team[",iTeam,"] Running PRON.")	
				
				IF NOT IS_PARTICIPANT_A_SPECTATOR(i)
					RUN_PRON_MODE(i,iTeam)
				ENDIF
			ENDIF
		ENDFOR
		IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
			IF IS_PED_ON_ANY_BIKE(LocalPlayerPed)
				VEHICLE_INDEX viLocalPlayerLightBike = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
				IF IS_VEHICLE_DRIVEABLE(viLocalPlayerLightBike)
					vLastFramePos = GET_ENTITY_COORDS(viLocalPlayerLightBike)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("RUN_PRON_MODE")
	#ENDIF
	#ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciBIRDS_EYE_ENABLED)
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciSCRIPTED_RACE_CAMS_ENABLED)
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_TURF_WAR_TOP_DOWN_CAMERA)
	OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TOP_DOWN_CAM_FOR_RESPAWN_SPECTATORS) AND IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN))
	OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciFORCE_STATIC_CAMERA_LOCATIONS) AND (IS_LOCAL_PLAYER_ANY_SPECTATOR() OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)))
		IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
		OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType) AND (IS_TINY_RACERS_INTRO_OVER() OR IS_BIT_SET(iLocalBoolCheck22, LBOOL22_TINY_RACERS_INTRO_CAM_FINISHED))
		OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType) AND (IS_FAKE_MULTIPLAYER_MODE_SET() OR IS_LOCAL_PLAYER_ANY_SPECTATOR())
			PROCESS_SCRIPT_CAMS()
		ENDIF
		
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARZONE(g_FMMC_STRUCT.iAdversaryModeType)
			IF IS_PED_INJURED(LocalPlayerPed)
				RESET_NET_TIMER(tdTopDownBlockAiming)
			ENDIF			
			
			// So we do not get trapped in the GOTO state.
			IF NOT bUsingBirdsEyeCam
				IF GET_SCRIPT_TASK_STATUS(LocalPlayerPed, SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = WAITING_TO_START_TASK
				OR GET_SCRIPT_TASK_STATUS(LocalPlayerPed, SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = PERFORMING_TASK
					CLEAR_PED_TASKS(LocalPlayerPed)
				ENDIF
			ENDIF
			
			IF bUsingBirdsEyeCam
			OR HAS_NET_TIMER_STARTED(tdTopDownBlockAiming) AND NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(tdTopDownBlockAiming, ci_TD_BLOCK_AIMING)
				BLOCK_ACTIONS_FOR_TOP_DOWN_MODE_ON_FOOT()
				
				IF NOT IS_ANY_FIRST_PERSON_CAM_ACTIVE(TRUE, TRUE)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				ELSE
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
		
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_BIRDS_EYE_CAMERA")
	#ENDIF
	#ENDIF
	
	PROCESS_MAP_AND_RADAR()
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_MAP_AND_RADAR")
	#ENDIF
	#ENDIF
	
	HINT_CAM_AND_QUICK_GPS_PROCESSING()
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("HINT_CAM_PROCESSING")
	#ENDIF
	#ENDIF
	
	PROCESS_FILTERS()
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_FILTERS")
	#ENDIF
	#ENDIF
	
	PLAYER_INVENTORY_PROCESSING() // This function handles player carry limits as well as Get Mask rules
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PLAYER_INVENTORY_PROCESSING")
	#ENDIF
	#ENDIF

	MAINTAIN_HEIST_BONUS_RESET_TICKERS() // Displays tickers when the heist bonus challenges are failed (flow order, same team, ultimate challenge)

	MAINTAIN_EMERGENCY_CALLS() // This function disables the emergency services when set in the creator

	MAINTAIN_PHONE_HACK_MINIGAME()
	
	MAINTAIN_VEHICLE_TURRET_SOUNDS()
	
	MAINTAIN_SPECTATOR_DIALOGUE() // When a player moves to spectate someone now, this function invalidates any dialogue the new spectator target has already played

		
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MJM")
	#ENDIF
	#ENDIF
	
	PROCESS_PLAYER_LIVES() // Includes: failing if a player dies with no lives left, logic when a player dies/respawns, drawing lives HUD, manual respawn
	
	//url:bugstar:3135644
	IF HAS_TEAM_FAILED(MC_PlayerBD[iLocalPart].iTeam)
		IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_VEHICLE_MARKED_FOR_CLEAN_UP)
			SET_BIT(iLocalBoolCheck16, LBOOL16_VEHICLE_MARKED_FOR_CLEAN_UP)
			START_NET_TIMER(vehicleCleanUpTimer)
			netVehicleToCleanUp = netRespawnVehicle
			PRINTLN("[RCC MISSION][SD] SETTING VEHICLE TO CLEAN UP AS THIS TEAM FAILED iTeam: ", MC_PlayerBD[iLocalPart].iTeam)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciCLEAR_UP_VEHICLE_ON_DEATH)
		PROCESS_TEAM_VEHICLE_CLEAN_UP()
	ENDIF
	
	// If anyone is using this system then we don't want to aggressively cleanup vehicles because we will clear empty vehicles that are engaging in a seat swap.
	// There's no good way to pass a reference to all clients of this respawn vehicle so that it's not included in the deletion via: DELETE_ANY_EMPTY_NEARBY_VEHICLES
	BOOL bBlockDeleteVehs = FALSE
	BOOL bOnlyDeleteIfAlive = FALSE
	BOOL bOnlyDeleteBikesIfFd = FALSE
	BOOL bDontDeleteBikes = FALSE
	
	FOR i = 0 TO FMMC_MAX_TEAMS-1
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitSet3, ciBS3_ENABLE_TEAM_VEHICLE_ALLOW_VEHICLE_SEAT_SWAPPING)
			bBlockDeleteVehs = TRUE
		ENDIF
	ENDFOR
	
	IF USING_SHOWDOWN_POINTS()
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_STUNTING_PACK(g_FMMC_STRUCT.iAdversaryModeType) // will add creator option if this works but the issue comes up in other modes
		bOnlyDeleteIfAlive = TRUE 
	ENDIF
	
	IF CONTENT_IS_USING_ARENA()
		bOnlyDeleteBikesIfFd = TRUE
	ENDIF
	
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_TEAM_0_SHOULD_RESPAWN_BACK_AT_START_NOW + MC_playerBD[iLocalPart].iTeam)
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciCLEAR_UP_VEHICLE_ON_GAME_LEAVE)
			IF NOT IS_PED_INJURED(localPlayerPed)
				IF ((IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType) OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRSHOOTOUT(g_FMMC_STRUCT.iAdversaryModeType))
					AND IS_PED_IN_ANY_VEHICLE(LocalPlayerPed))
				OR (NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType) AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRSHOOTOUT(g_FMMC_STRUCT.iAdversaryModeType))
					IF NOT bBlockDeleteVehs
						DELETE_ANY_EMPTY_NEARBY_VEHICLES(bOnlyDeleteIfAlive, bOnlyDeleteBikesIfFd, bDontDeleteBikes)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	PROCESS_VESPUCCI_REMIX_SPAWN_VEHICLE_CLEANUP()
	
	IF MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam] < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iRuleBitsetTen[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]], ciBS_RULE10_SHOW_CACHED_SCORES_ON_THIS_RULE)
			PROCESS_CACHED_SCORE_HUD()
		ENDIF
	ENDIF
	
	IF NOT SHOULD_HIDE_THE_HUD_THIS_FRAME()
		PROCESS_VEHICLE_HEALTH_HUD()
	ENDIF
	
	// Fix because GET_PLAYERS_LAST_VEHICLE looks at spectator focus veh.
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMBUSHKA(g_FMMC_STRUCT.iAdversaryModeType)
		IF NOT IS_PED_INJURED(localPlayerPed)
		AND NOT IS_PLAYER_RESPAWNING(localPlayer)
			IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
				VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
				
				IF DOES_ENTITY_EXIST(vehPlayer)
				AND IS_VEHICLE_DRIVEABLE(vehPlayer)
					g_mnOfLastVehicleWeWasIn = GET_ENTITY_MODEL(vehPlayer)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	PROCESS_RESET_DUNE_REAR_MINES()
	
	PROCESS_SILO_ALARM()
	
	IF GET_VCM_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash) = eVCM_BRAWL
		PROCESS_CASINO_BRAWL_AUDIO()
	ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("looking after player deaths & respawns")
	#ENDIF
	#ENDIF
	
	PROCESS_MISSION_BOUNDS() // Fails players for going out of (or into if required) creator-placed areas and world map bounds
	
	PROCESS_SERVER_COUNT_PLAYERS_IN_BOUNDS()
	
	PROCESS_BOUNDS_CENTRE_LINE_RESPAWN()	//CENTRE LINE RESPAWN - Moved outside PROCESS_MISSION_BOUNDS so it runs even if the player is dead.
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciVEHICLE_WEAPON_DEATHMATCH_TOGGLE)
	AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciPRON_DEATHMATCH_TOGGLE_CORONA)
		PROCESS_LAST_DAMAGER_TIMER() //Processes the last damager and resets if the time has expired
	ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_MISSION_BOUNDS")
	#ENDIF
	#ENDIF
	
	PROCESS_MUSIC() // Changes the current playing music scene in the score depending on mission objective settings and combat/driving/flying state, as well as end countdowns
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_MUSIC")
	#ENDIF
	#ENDIF
	
	PROCESS_AUDIO_MIX()
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_AUDIO_MIX")
	#ENDIF
	#ENDIF

	PROCESS_MISSION_OBJECTIVE_LOGIC() // Sets the local player's client mission stage so they can run the correct logic
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_MISSION_OBJECTIVE_LOGIC")
	#ENDIF
	#ENDIF
	
	IF NOT IS_PLAYER_IN_WANTED_BLOCKING_ZONE()
		PROCESS_WANTED_CHANGE_FOR_OBJECTIVE() // Runs wanted level changing when a new objective occurs, as well as other wanted level related logic
	ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_WANTED_CHANGE_FOR_OBJECTIVE")
	#ENDIF
	#ENDIF
	
	
	//This function: a) Draws various HUD elements for the local player (objective/multi-objective/mission timers, team scores HUD)
	//				 b) Fails/moves on teams for running out of time, passing their target score and losing their wanted level
	//				 c) Draws the Cross the Line HUD
	//				 d) basically does everything
	PROCESS_OBJECTIVE_LIMITS()
	
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_OBJECTIVE_LIMITS")
	#ENDIF
	#ENDIF
	
	
	// TURF WAR:
	// This section here is important as we do not want to give points or reset props if the Mission is ending, as we can end up with a clear board, affecting the leadboard stats
	// and also extra points awarded. So we need to wait a frame for these: HAVE_MC_END_CONDITIONS_BEEN_MET & SBBOOL_MISSION_OVER - Once they are set we know we shouldn't be doing the below section.
	IF bIsLocalPlayerHost
		IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
			IF IS_BIT_SET(MC_serverBD.iServerBitSet4, SBBOOL4_TW_GIVE_POINTS)
				IF NOT TURFWAR_BLOCK_POINT_ALLOCATION_AND_PROP_RESET_DUE_TO_MISSION_ENDING()
					CLEAR_BIT(MC_serverBD.iServerBitSet4, SBBOOL4_TW_GIVE_POINTS)
					PRINTLN("[LM][TURF WAR][PROCESS_TURF_WAR] - Giving Points This Frame")
					PROCESS_HOST_SCORE_LOGIC_PROP_CLAIMING(TRUE, TRUE)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(MC_serverBD.iServerBitSet4, SBBOOL4_TW_SHOULD_GIVE_POINTS)
				CLEAR_BIT(MC_serverBD.iServerBitSet4, SBBOOL4_TW_SHOULD_GIVE_POINTS)
				SET_BIT(MC_serverBD.iServerBitSet4, SBBOOL4_TW_GIVE_POINTS)
				PRINTLN("[LM][TURF WAR][PROCESS_TURF_WAR] - Giving Points next frame.")
			ENDIF	
			
			IF IS_BIT_SET(MC_serverBD.iServerBitSet4, SBBOOL4_TW_RESET_ALL_PROPS)
			AND NOT TURFWAR_BLOCK_PROPS_RESET_FOR_LAST_POINTS()
				IF NOT TURFWAR_BLOCK_POINT_ALLOCATION_AND_PROP_RESET_DUE_TO_MISSION_ENDING()
					CLEAR_BIT(MC_serverBD.iServerBitSet4, SBBOOL4_TW_RESET_ALL_PROPS)
					PRINTLN("[LM][TURF WAR][PROCESS_TURF_WAR] - Resetting props this frame.")
					RESET_ALL_CLAIMED_PROPS()
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(MC_serverBD.iServerBitSet4, SBBOOL4_TW_SHOULD_RESET_ALL_PROPS)
				CLEAR_BIT(MC_serverBD.iServerBitSet4, SBBOOL4_TW_SHOULD_RESET_ALL_PROPS)
				SET_BIT(MC_serverBD.iServerBitSet4, SBBOOL4_TW_RESET_ALL_PROPS)
				PRINTLN("[LM][TURF WAR][PROCESS_TURF_WAR] - Resetting props next frame.")
			ENDIF
			
			IF IS_BIT_SET(iLocalBoolCheck20, LBOOL20_TURFWAR_ICON_START_PULSE)
				IF NOT HAS_NET_TIMER_STARTED(tdTurfWarBlockPulseOnReset)
					START_NET_TIMER(tdTurfWarBlockPulseOnReset)
				ENDIF
			
				IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdTurfWarBlockPulseOnReset, ci_TURF_WAR_BLOCK_PULSE_ON_RESET)
					CALCULATE_TEAM_PROP_OWNERSHIP()
					TURF_WAR_RECALCULATE_LEADER_FOR_HUD()
					RESET_NET_TIMER(tdTurfWarBlockPulseOnReset)
					CLEAR_BIT(iLocalBoolCheck20, LBOOL20_TURFWAR_ICON_START_PULSE)	
				ENDIF		
			ENDIF
		ENDIF
	ENDIF
	
	IF bIsLocalPlayerHost
		CHECK_FOR_END_CUTSCENE_VARIATION_CHANGE()
		FOR i = 0 TO ( MC_serverBD.iNumberOfTeams - 1 )
			SERVER_SELECT_END_MOCAP( i )
			SELECT_CUTSCENE_PLAYERS_FOR_TEAM( i )
		ENDFOR
	ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("SERVER_SELECT_END_MOCAP")
	#ENDIF
	#ENDIF
	
	
	//This function processes mission dialogue triggers - which as well as triggering mission dialogue, now also act as a generic system for triggering
	//  other things, such as the phone hack minigame
		
	PROCESS_DIALOGUE_TRIGGERS_PLAYING_EVERY_FRAME()
	
	PROCESS_DIALOGUE_TRIGGERS()
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA (g_FMMC_STRUCT.iAdversaryModeType)
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
		PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS()
	ENDIF
		
	// [FIX_2020_CONTROLLER] - A lot of these adversary mode process functions could be moved to a new "FM_Mission_Controller_AdversaryMode.sch header file.
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciPLAY_POWERPLAY_INTRO_ANNOUNCER)
		PROCESS_POWERPLAY_LOOP_SOUNDS()
		
		IF NOT g_bMissionOver
			PROCESS_CACHED_PP_ANNOUNCER_CALLS()
		ENDIF
		
		IF NOT IS_BIT_SET(iLocalBoolCheck17, LBOOL17_HAS_PLAYED_PP_SUDDEN_DEATH_ANNOUNCER)
			IF ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
				IF NOT IS_BIT_SET(iLocalBoolCheck17, LBOOL17_KILLED_CONVERSATIONS_FOR_PP_SD_ANN)
					SET_BIT(iLocalBoolCheck17, LBOOL17_KILLED_CONVERSATIONS_FOR_PP_SD_ANN)
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				ELSE
					SET_BIT(iLocalBoolCheck17, LBOOL17_HAS_PLAYED_PP_SUDDEN_DEATH_ANNOUNCER)
					
					//remove the cached announcer incase collection and suddendeath is really close together
					sCachedAnnouncement.iPowerupType = -1
					sCachedAnnouncement.iSenderTeam = -1
					sCachedAnnouncement.piSenderPlayer = NULL	
					PLAY_RAGE_PP_ANNOUNCER(ciCUSTOM_PICKUP_TYPE__SUDDEN_DEATH, MC_playerBD[iPartToUse].iteam, LocalPlayer)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_DIALOGUE_TRIGGERS")
	#ENDIF
	#ENDIF
	
	PROCESS_TEXT_MESSAGES()
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_TEXT_MESSAGES")
	#ENDIF
	#ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciDISABLE_VALKYRIE_FRONT_GUN)
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_IHELP_VAL")
			CLEAR_HELP(TRUE)
		ENDIF
	ENDIF
	
	INT iTotalNumPlayers = (MC_serverBD.iNumberOfLBPlayers[0]+ MC_serverBD.iNumberOfLBPlayers[1]+ MC_serverBD.iNumberOfLBPlayers[2] + MC_serverBD.iNumberOfLBPlayers[3]) 
	NUM_DPAD_LBD_PLAYERS_IS(iTotalNumPlayers)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("NUM_DPAD_LBD_PLAYERS_IS") 
	#ENDIF
	#ENDIF
	
	
	//This controls the player list that shows when DPAD_DOWN is pressed
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
	AND IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
	AND IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_EVERYONE_RUNNING)
	AND (NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_FINISHED) OR IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE())
	
		DRAW_DPAD_LEADERBOARD()
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("DRAW_DPAD_LEADERBOARD")
		#ENDIF
		#ENDIF
		
	ENDIF
	
	// KGM_CTF_MARKER: 5/13/12: Maintain the CTF Marker
	//							This allows a 'keep active' flag to be cleared here and then set in the DRAW_LOCATE_MARKER function if still required
	MAINTAIN_CTF_DROPOFF_GROUND_PROJECTION_AND_MARKER()
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CTF_DROPOFF_GROUND_PROJECTION_AND_MARKER")
	#ENDIF
	#ENDIF
	
	PROCESS_IAA_ENABLED_GUN_CAMERAS ()

	//This function processes and broadcasts events for the local player completing various different types of objectives, such as: get & delivers, go tos,
	// and just the events for photo rules, hack rules; as well as the drawing of delivery drop-off locations, and the objective completion event safety system
	
	IF HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE()
		CLEAR_BIT(iLocalBoolCheck22, LBOOL22_OVERTIME_WAIT_A_FRAME_FOR_TIMER_EXPIRE)
		RESET_NET_TIMER(tdOvertimeTimerDelayRuleProgression)
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_IAO_BLOCK_PACKAGE_SOUNDS)
		PROCESS_IN_AND_OUT_PACKAGE_SOUNDS()
	ELSE
		IF HAS_NET_TIMER_STARTED(stBlockPackageSoundsTimer)
			IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stBlockPackageSoundsTimer) > 500
				CLEAR_BIT(iLocalBoolCheck16, LBOOL16_IAO_BLOCK_PACKAGE_SOUNDS)
				RESET_NET_TIMER(stBlockPackageSoundsTimer)
			ENDIF
		ELSE
			REINIT_NET_TIMER(stBlockPackageSoundsTimer)
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE")
	#ENDIF
	#ENDIF
	
	TRACK_EXTRA_STATS()
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("TRACK_EXTRA_STATS")
	#ENDIF
	#ENDIF
	
	MANAGE_MID_MISSION_CUTSCENE()
	MANAGE_MID_MISSION_CROWD_CONTROL_CUTSCENE()
		
	PROCESS_DOWNLOAD_SOUND_AFTER_DIALOGUE_TRIGGER()
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MANAGE_MID_MISSION_CUTSCENE")
	#ENDIF
	#ENDIF	
	
	PROCESS_INCREMENTAL_RULE_FAIL_TIMER()
	
	MANAGE_PLAYER_CUTSCENE_ANIM_TASK()
	
	//PROCESS_MARKER_LINES_BLIPS()
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MANAGE_PLAYER_CUTSCENE_ANIM_TASK")
	#ENDIF
	#ENDIF	
	
	MANAGE_STREAMING_MOCAP()
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MANAGE_STREAMING_MOCAP")
	#ENDIF
	#ENDIF
	
	IF SHOULD_RUN_NEW_VOTING_SYSTEM(sEndOfMission.iQuitProgress, MC_serverBD.sServerFMMC_EOM.iVoteStatus)
		IF NOT READY_TO_MOVE_TO_NEXT_JOBS(nextJobStruct)
			DO_END_JOB_VOTING_SCREEN(g_sMC_serverBDEndJob, nextJobStruct, g_sMC_serverBDEndJob.timerVotingEnds.Timer)
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("CLIENT_JOB_VOTE_GRAB_IMAGES")
			#ENDIF
			#ENDIF
		ENDIF
	ENDIF
	
	HANDLE_SUPPRESSING_WASTED_SHARD()
	
	DO_TUTORIAL_MISSION_HELP_TEXT()
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("DO_TUTORIAL_MISSION_HELP_TEXT")
	#ENDIF
	#ENDIF
	
	DO_HEIST_TUTORIAL_HELP_TEXT()
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("DO_HEIST_TUTORIAL_HELP_TEXT")
	#ENDIF
	#ENDIF
	
	IF NOT Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)
		DO_CTF_MISSION_HELP_TEXT()
	ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("DO_CTF_MISSION_HELP_TEXT")
	#ENDIF
	#ENDIF
	
	//For sharing the VIP marker in Entourage
	SHARE_MARKER_WITH_SPECIFIED_TEAM()
		
	//Processes the display of HUD text at the bottom of the screen telling players their current objective, also draws get & deliver drop-off blip and the objective shard text
	IF NOT g_OfficeHeliDockData.bDoingCutscene
		PROCESS_OBJECTIVE_TEXT()
	ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_OBJECTIVE_TEXT")
	#ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		PROCESS_DEBUG()
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_J_SKIPS")
	#ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
		CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
		#ENDIF
	#ENDIF
	
	#IF FEATURE_CASINO_HEIST
	#IF IS_DEBUG_BUILD
	//PROCESS_BAIL_MISSION_VARIATION_SETTINGS_ARE_OUT_OF_SYNC()
	#ENDIF
	#ENDIF
	
ENDPROC

#IF IS_DEBUG_BUILD

PROC DRAW_COMMON_MISSION_CONTROLLER_DEBUG_STATE_LEGEND()
	TEXT_LABEL_63 tl63_Name
	tl63_Name = "~bold~~g~Local Participant Index:   "
	tl63_Name += iLocalPart
	DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)
	
	tl63_Name = "~bold~~g~Local Player Name:   "
	tl63_Name += GET_PLAYER_NAME(PLAYER_ID())
	DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)
	
	tl63_Name = "~bold~~g~Local Player Team:   "
	tl63_Name += MC_PlayerBD[iLocalPart].iTeam
	DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)
	
	tl63_Name = "~bold~~g~Local Player Rule:   "
	tl63_Name += MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iTeam]
	DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 1, 1)
ENDPROC

PROC PROCESS_TEAM_SPAWN_POINTS_STATE_DEBUG_WINDOW()
	
	IF NOT SET_UP_INITIAL_CONTENT_OVERVIEW_DEBUG_WINDOW_DIMENSIONS()
		EXIT
	ENDIF
	
	INT i
	TEXT_LABEL_63 tl63_EntityName
	TEXT_LABEL_63 tl63_EntityKey1
	TEXT_LABEL_63 tl63_EntityKey2
	TEXT_LABEL_63 tl63_Name
	
	sContentOverviewDebug.iDebugWindowPageMax[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 2
	SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		CASE 0
			tl63_EntityKey1 = "Key: Creator ID : Pos : Heading : Vehicle : Seat"
		BREAK
		CASE 1
			tl63_EntityKey1 = "Key: Creator ID : Attach Type : Attach ID"
		BREAK
	ENDSWITCH
	
	IF NOT IS_BIT_SET(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_HideLegend)
	
		DRAW_COMMON_MISSION_CONTROLLER_DEBUG_STATE_LEGEND()
		sContentOverviewDebug.iLegendPrintY = 0
		sContentOverviewDebug.iLegendPrintX = 1
		
		tl63_Name += "Number of Placed Team Spawn Points: "
		tl63_Name += g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[sContentOverviewDebug.iTeamIndexSelected]
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)
		tl63_Name = "Number of Valid Spawn Points: "
		tl63_Name += sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 1, 0)
		
		// Middle Extra Info Draw ---------------
		
		sContentOverviewDebug.iLegendPrintY = 0
		
		tl63_Name = "Local Player Only Info: "
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
				
		sContentOverviewDebug.iLegendPrintY = 0
		BOOL bSpawnAreaActive		
		SPAWN_AREA SpArea
		INT iArea = 0	
		IF g_SpawnData.MissionSpawnDetails.SpawnArea[iArea].bIsActive
			bSpawnAreaActive = TRUE		
		ENDIF
		
		IF bSpawnAreaActive
			tl63_Name = "Using Spawn Areas"
			DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)							
			
			SpArea = g_SpawnData.MissionSpawnDetails.SpawnArea[iArea]						
			IF SpArea.iShape = SPAWN_AREA_SHAPE_BOX
				DRAW_BOX(SpArea.vCoords1, SpArea.vCoords2, 200, 200, 200, 150)
				tl63_Name = "Axis Aligned Box"
				DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)				
				
				VECTOR vTemp = (SpArea.vCoords1+SpArea.vCoords2)
				vTemp.x /= 2
				vTemp.y /= 2
				vTemp.z /= 2
				tl63_Name = "Pos: "
				tl63_Name += "<"
				tl63_Name += ROUND(vTemp.x)
				tl63_Name += ", "
				tl63_Name += ROUND(vTemp.y)
				tl63_Name += ", "
				tl63_Name += ROUND(vTemp.z)
				tl63_Name += ">"
				DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
				DRAW_DEBUG_TEXT_ABOVE_COORDS(vTemp, "Axis Aligned Spawn Box", 1.5, 100, 250, 100, 200)
				
			ELIF SpArea.iShape = SPAWN_AREA_SHAPE_CIRCLE
				DRAW_MARKER(MARKER_SPHERE, SpArea.vCoords1, (<<0.0, 0.0, 0.0>>), (<<0.0, 0.0, 0.0>>), (<<SpArea.fFloat, SpArea.fFloat, SpArea.fFloat>>), 200, 200, 200, 150)				
				tl63_Name = "Sphere"
				DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)			
				
				tl63_Name = "Pos: "
				tl63_Name += "<"
				tl63_Name += ROUND(SpArea.vCoords1.x)
				tl63_Name += ", "
				tl63_Name += ROUND(SpArea.vCoords1.y)
				tl63_Name += ", "
				tl63_Name += ROUND(SpArea.vCoords1.z)
				tl63_Name += ">"
				DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
				DRAW_DEBUG_TEXT_ABOVE_COORDS(SpArea.vCoords1, "Spawn Sphere", 1.5, 100, 250, 100, 200)
				
			ELIF SpArea.iShape = SPAWN_AREA_SHAPE_ANGLED
				DRAW_ANGLED_AREA_FROM_FACES(SpArea.vCoords1, SpArea.vCoords2, SpArea.fFloat, 200, 200, 200, 150)
				tl63_Name = "Angled Area"
				DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)				
				
				VECTOR vTemp = (SpArea.vCoords1+SpArea.vCoords2)
				vTemp.x /= 2
				vTemp.y /= 2
				vTemp.z /= 2
				tl63_Name = "Pos: "
				tl63_Name += "<"
				tl63_Name += ROUND(vTemp.x)
				tl63_Name += ", "
				tl63_Name += ROUND(vTemp.y)
				tl63_Name += ", "
				tl63_Name += ROUND(vTemp.z)
				tl63_Name += ">"
				DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
				DRAW_DEBUG_TEXT_ABOVE_COORDS(vTemp, "Spawn Angled Area", 1.5, 100, 250, 100, 200)
					
			ENDIF			
		
		ELIF IS_BIT_SET(iLocalBoolCheck21, LBOOL21_USING_CUSTOM_RESPAWN_POINTS)	
			tl63_Name = "Using Custom Spawn Points"	
			DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
			
		
		ELIF g_SpawnData.MissionSpawnGeneralBehaviours.DefaultSpawnLocation = SPAWN_LOCATION_NEAR_DEATH						
			tl63_Name = "Using Default / Near Death default spawning"
			DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
			
		ELSE
			tl63_Name = "Using Ambient DefaultSpawnLocation ENUM_TO_INT: "
			tl63_Name += ENUM_TO_INT(g_SpawnData.MissionSpawnGeneralBehaviours.DefaultSpawnLocation)
			DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)			
			
		ENDIF		
		
		// Legend and Key at the bottom -------------	
		sContentOverviewDebug.iLegendPrintY = 5
		
		sContentOverviewDebug.iLegendPrintX = 0
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey1, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 1, 0)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey2, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
		
		sContentOverviewDebug.iLegendPrintX = 0
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Placed", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.01)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 200, 200, 200, 200)		
	ENDIF
	
	// Main Draw Loop -------------------------
	
	sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 0
	
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[sContentOverviewDebug.iTeamIndexSelected]-1
		FLOAT fTextSize = sContentOverviewDebug.fBaseTextSize
		FLOAT fEntityListColumnSpacingBase = (sContentOverviewDebug.fRectSizeX / GET_NUMBER_OF_COLUMNS_FOR_ITEMS(sContentOverviewDebug.eDebugWindow))
		FLOAT fEntityListColumnSpacing = (fEntityListColumnSpacingBase * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow)))
		FLOAT fEntityListStartPosX = sContentOverviewDebug.fTextPosX
		FLOAT fEntityListStartPosY = sContentOverviewDebug.fTextPosY + 0.02
		
		FLOAT fEntityNamePosX = fEntityListStartPosX + fEntityListColumnSpacing
		FLOAT fEntityNamePosY = fEntityListStartPosY + (sContentOverviewDebug.fYSpacing * i) -  (sContentOverviewDebug.fYSpacing * (GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow) * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow))))
		
		// Warp Click Event (Camera)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_TOP_LEFT, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		// Warp Click Event (Player Ped)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_TOP_RIGHT, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		tl63_EntityName = "  Point: "
		tl63_EntityName += i
			
		DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 200, 200, 200, 200) 	// Placed
				
		SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
			CASE 0
				VECTOR vTemp
				FLOAT fTemp
				vTemp = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sContentOverviewDebug.iTeamIndexSelected][i].vPos
				fTemp = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sContentOverviewDebug.iTeamIndexSelected][i].fHead
				
				tl63_EntityName += " : "
				tl63_EntityName += "<"
				tl63_EntityName += ROUND(vTemp.x)
				tl63_EntityName += ", "
				tl63_EntityName += ROUND(vTemp.y)
				tl63_EntityName += ", "
				tl63_EntityName += ROUND(vTemp.z)
				tl63_EntityName += ">"
				 
				tl63_EntityName += " : "
				tl63_EntityName += ROUND(fTemp)
				tl63_EntityName += " : "
				tl63_EntityName += g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sContentOverviewDebug.iTeamIndexSelected][i].iVehicle
				
				tl63_EntityName += " : "
				tl63_EntityName += GET_SEAT_NAME_FOR_FORCE_OPTION(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sContentOverviewDebug.iTeamIndexSelected][i].iSeat)			
			BREAK
			CASE 1
				tl63_EntityName += " : "
				tl63_EntityName += g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sContentOverviewDebug.iTeamIndexSelected][i].iMoveToEntityType
				
				tl63_EntityName += " : "
				tl63_EntityName += g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sContentOverviewDebug.iTeamIndexSelected][i].iMoveToEntityIndex
			BREAK
		ENDSWITCH
		
		DRAW_ENTITY_DEBUG_WINDOW_TEXT(tl63_EntityName, fEntityNamePosX, fEntityNamePosY, fTextSize)		
	ENDFOR
ENDPROC

PROC PROCESS_DIALOGUE_TRIGGER_STATE_DEBUG_WINDOW()

	IF NOT SET_UP_INITIAL_CONTENT_OVERVIEW_DEBUG_WINDOW_DIMENSIONS()
		EXIT
	ENDIF
	
	TEXT_LABEL_63 tl63_EntityName
	TEXT_LABEL_63 tl63_EntityKey1
	TEXT_LABEL_63 tl63_EntityKey2
	TEXT_LABEL_63 tl63_Name
	
	sContentOverviewDebug.iDebugWindowPageMax[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 2
	SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		CASE 0
			tl63_EntityKey1 = "Trigger Key: Creator ID : Block : Root : Playing"
		BREAK
		CASE 1
			tl63_EntityKey1 = "Trigger Key: Creator ID : Conversation/Help"
		BREAK
	ENDSWITCH
	
	IF NOT IS_BIT_SET(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_HideLegend)
	
		DRAW_COMMON_MISSION_CONTROLLER_DEBUG_STATE_LEGEND()
		sContentOverviewDebug.iLegendPrintY = 0
		sContentOverviewDebug.iLegendPrintX = 1
		
		tl63_Name += "Number of Placed Triggers: "
		tl63_Name += g_FMMC_STRUCT.iNumberOfDialogueTriggers
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 1, 0)
		tl63_Name = "Number of Current Triggers Played: "
		tl63_Name += sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 2)
		sContentOverviewDebug.iLegendPrintX = 0
		
		// Middle Extra Info Draw ---------------
		
		tl63_Name = "Conversation Status: "
		IF NOT IS_CONVERSATION_STATUS_FREE()
		OR IS_SCRIPTED_CONVERSATION_ONGOING()
			tl63_Name += "Playing"
		ELSE
			tl63_Name += "Waiting"
		ENDIF
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
		
		tl63_Name = "Last Played Dialogue: "
		tl63_Name += iDialogueProgressLastPlayed
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
		
		tl63_Name = "Dialogue Progress: "
		tl63_Name += iDialogueProgress
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
			
		// Legend and Key at the bottom -------------	
		sContentOverviewDebug.iLegendPrintY++		
		
		sContentOverviewDebug.iLegendPrintX = 0
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey1, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 1, 0)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey2, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
		
		sContentOverviewDebug.iLegendPrintX = 0
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Played", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Not Played", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.1)		
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+0.1+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 200, 50, 200)		
	ENDIF
	
	// Main Draw Loop -------------------------
	
	INT iTrigStart, iTrigEnd
	IF g_FMMC_STRUCT.iNumberOfDialogueTriggers > 60
		IF sContentOverviewDebug.iToggleEntitiesDueToTooMuchTextDraws = 1
			iTrigStart = 60
			iTrigEnd = g_FMMC_STRUCT.iNumberOfDialogueTriggers
		ELSE	
			iTrigStart = 0
			iTrigEnd = 60
		ENDIF
	ELSE
		iTrigStart = 0
		iTrigEnd = g_FMMC_STRUCT.iNumberOfDialogueTriggers
	ENDIF
	
	sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 0
	INT i = 0
	FOR i = iTrigStart TO iTrigEnd-1
		FLOAT fTextSize = sContentOverviewDebug.fBaseTextSize
		FLOAT fEntityListColumnSpacingBase = (sContentOverviewDebug.fRectSizeX / GET_NUMBER_OF_COLUMNS_FOR_ITEMS(sContentOverviewDebug.eDebugWindow))
		FLOAT fEntityListColumnSpacing = (fEntityListColumnSpacingBase * ((i-iTrigStart) / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow)))
		FLOAT fEntityListStartPosX = sContentOverviewDebug.fTextPosX
		FLOAT fEntityListStartPosY = sContentOverviewDebug.fTextPosY + 0.02
		
		FLOAT fEntityNamePosX = fEntityListStartPosX + fEntityListColumnSpacing
		FLOAT fEntityNamePosY = fEntityListStartPosY + (sContentOverviewDebug.fYSpacing * (i-iTrigStart)) -  (sContentOverviewDebug.fYSpacing * (GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow) * ((i-iTrigStart) / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow))))				
				
		// Click Event Play/Trigger
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_TOP_LEFT, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_PLAY_OR_TRIGGER, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		// Click Event Reset Trigger
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_TOP_RIGHT, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_RESET, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		// Warp Click Event (Camera)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_BOTTOM_LEFT, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		// Warp Click Event (Player Ped)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_BOTTOM_RIGHT, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		tl63_EntityName = "  Trig: "
		tl63_EntityName += i
			
		IF (IS_BIT_SET(iLocalDialoguePlayedBS[i/32], i%32) OR IS_BIT_SET(MC_playerBD[iLocalPart].iDialoguePlayedBS[i/32], i%32) OR IS_BIT_SET(iLocalDialoguePlayingBS[i/32], i%32))	
		 	DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)		// Alive			
		 	sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]++
		 	
		ELSE
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 0, 0, 200)		// Dead/Can't Spawn
			
		ENDIF
				
		SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
			CASE 0				
				tl63_EntityName += " : "
				tl63_EntityName += g_FMMC_STRUCT.sDialogueTriggers[i].tlBlock 	
				 
				tl63_EntityName += " : "				
				tl63_EntityName += g_FMMC_STRUCT.sDialogueTriggers[i].tlRoot
					
				IF iDialogueProgressLastPlayed = i
				AND (NOT IS_CONVERSATION_STATUS_FREE()
				OR IS_SCRIPTED_CONVERSATION_ONGOING())
					tl63_EntityName += " : "
					tl63_EntityName += "Playing this"
				ENDIF
			BREAK
			CASE 1	
				IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[i].iDialogueBitset, iBS_DialogueHelpText)
				OR IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[i].iDialogueBitset2, iBS_Dialogue2HelpTextNGOnly)	
					tl63_EntityName += " : "
					tl63_EntityName += "Help Text"
				ELSE
					tl63_EntityName += " : "
					tl63_EntityName += "Conversation"
				ENDIF
				
				IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sDialogueTriggers[i].tlDialogueTriggerAdditionalHelpText)
					tl63_EntityName += " : "
					tl63_EntityName += "Help Text"
				ENDIF
			BREAK
		ENDSWITCH
		
		DRAW_ENTITY_DEBUG_WINDOW_TEXT(tl63_EntityName, fEntityNamePosX, fEntityNamePosY, fTextSize)		
	ENDFOR
ENDPROC

PROC PROCESS_LOCATION_STATE_DEBUG_WINDOW()

	IF NOT SET_UP_INITIAL_CONTENT_OVERVIEW_DEBUG_WINDOW_DIMENSIONS()
		EXIT
	ENDIF
	
	TEXT_LABEL_63 tl63_EntityName
	TEXT_LABEL_63 tl63_EntityKey1
	TEXT_LABEL_63 tl63_EntityKey2
	TEXT_LABEL_63 tl63_Name
	
	sContentOverviewDebug.iDebugWindowPageMax[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 2
	SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		CASE 0
			tl63_EntityKey1 = "Location Key: Creator ID : Distance "
		BREAK
		CASE 1
			tl63_EntityKey1 = "Location Key: Creator ID : Nearest Locate : Player Count Req."
		BREAK
	ENDSWITCH
		
	IF NOT IS_BIT_SET(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_HideLegend)
		DRAW_COMMON_MISSION_CONTROLLER_DEBUG_STATE_LEGEND()
		sContentOverviewDebug.iLegendPrintY = 0
		sContentOverviewDebug.iLegendPrintX = 1
		
		tl63_Name += "Number of Placed Locations: "
		tl63_Name += g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0]
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.04, FALSE, 1, 0)
		
		tl63_Name = "Number of Current Locations: "
		tl63_Name += sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.04, FALSE, 0, 2)
		sContentOverviewDebug.iLegendPrintX = 0
		
		// Middle Extra Info Draw ---------------
		tl63_Name = "Current Player Location: "
		tl63_Name = MC_playerBD[iPartToUse].iCurrentLoc		
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 1, 0)
		
		// Legend and Key at the bottom -------------
		sContentOverviewDebug.iLegendPrintY++
		sContentOverviewDebug.iLegendPrintY++
		sContentOverviewDebug.iLegendPrintY++
		
		sContentOverviewDebug.iLegendPrintX = 0
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey1, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 1, 0)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey2, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
		
		sContentOverviewDebug.iLegendPrintX = 0
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Exists / Active", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Spawns On Later Rule", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.1)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Dead / Spawn Blocked", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.2)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+0.1+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 200, 50, 200)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+0.2+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 0, 0, 200)
	ENDIF
	
	// Main Draw Loop -------------------------
	sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 0
	INT i = 0
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0]-1
		FLOAT fTextSize = sContentOverviewDebug.fBaseTextSize
		FLOAT fEntityListColumnSpacingBase = (sContentOverviewDebug.fRectSizeX / GET_NUMBER_OF_COLUMNS_FOR_ITEMS(sContentOverviewDebug.eDebugWindow))
		FLOAT fEntityListColumnSpacing = (fEntityListColumnSpacingBase * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow)))
		FLOAT fEntityListStartPosX = sContentOverviewDebug.fTextPosX
		FLOAT fEntityListStartPosY = sContentOverviewDebug.fTextPosY + 0.02
		
		FLOAT fEntityNamePosX = fEntityListStartPosX + fEntityListColumnSpacing
		FLOAT fEntityNamePosY = fEntityListStartPosY + (sContentOverviewDebug.fYSpacing * i) -  (sContentOverviewDebug.fYSpacing * (GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow) * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow))))
		
		// Warp Click Event (Player Ped)		
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_TOP_LEFT, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		// Warp Click Event (Camera)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_TOP_RIGHT, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		
		tl63_EntityName = "  Loc: "
		tl63_EntityName += i
			
		IF MC_serverBD_4.iGotoLocationDataRule[i][0] != FMMC_OBJECTIVE_LOGIC_NONE
		AND MC_serverBD_4.iGotoLocationDataPriority[i][0] < FMMC_MAX_RULES
		AND MC_serverBD_4.iGotoLocationDataPriority[i][0] <= iCurrentHighPriority[0]		
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)		// Alive			
			sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]++
			
		ELIF MC_serverBD_4.iGotoLocationDataRule[i][0] != FMMC_OBJECTIVE_LOGIC_NONE
		AND MC_serverBD_4.iGotoLocationDataPriority[i][0] < FMMC_MAX_RULES
		AND MC_serverBD_4.iGotoLocationDataPriority[i][0] > iCurrentHighPriority[0]
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 200, 50, 200) 	// Not Spawned yet but can.
			
		ELSE
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 0, 0, 200)		// Dead/Can't Spawn
			
		ENDIF
		
		SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
			CASE 0
				tl63_EntityName += " : "
				tl63_EntityName += "Distance: "
				tl63_EntityName += ROUND(SQRT(GET_DIST2_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed, GET_LOCATION_VECTOR(i))))				
			BREAK
			CASE 1	
				IF iNearestTargetTemp = i
					tl63_EntityName += " : "
					tl63_EntityName += "This is Nearest Loc!"
				ENDIF
				
				IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iWholeTeamAtLocation[0] = ciGOTO_LOCATION_WHOLE_TEAM
					tl63_EntityName += " : "
					tl63_EntityName += "Req. Whole Team"
				ELIF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iWholeTeamAtLocation[0] = ciGOTO_LOCATION_ALL_PLAYERS
					tl63_EntityName += " : "
					tl63_EntityName += "Req. All Players"
				ELIF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iWholeTeamAtLocation[0] >= ciGOTO_LOCATION_2_PLAYERS
					tl63_EntityName += " : "
					tl63_EntityName += "Req. "
					tl63_EntityName += g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iWholeTeamAtLocation[0]
					tl63_EntityName += "Players"
				ELSE
					tl63_EntityName += " : "
					tl63_EntityName += "Standard"
				ENDIF				
			BREAK
		ENDSWITCH
		
		DRAW_ENTITY_DEBUG_WINDOW_TEXT(tl63_EntityName, fEntityNamePosX, fEntityNamePosY, fTextSize)
	ENDFOR
ENDPROC

PROC PROCESS_ZONE_STATE_DEBUG_WINDOW()

	IF NOT SET_UP_INITIAL_CONTENT_OVERVIEW_DEBUG_WINDOW_DIMENSIONS()
		EXIT
	ENDIF
	
	TEXT_LABEL_63 tl63_EntityName
	TEXT_LABEL_63 tl63_EntityKey1
	TEXT_LABEL_63 tl63_EntityKey2
	TEXT_LABEL_63 tl63_Name
	
	sContentOverviewDebug.iDebugWindowPageMax[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 2
	SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		CASE 0
			tl63_EntityKey1 = "Zone Key: Creator ID	"
		BREAK
		CASE 1
			tl63_EntityKey1 = "Zone Key: Creator ID : Zone Name"
		BREAK		
		CASE 2
			tl63_EntityKey1 = "Zone Key: Creator ID"
		BREAK
	ENDSWITCH
	
	IF NOT IS_BIT_SET(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_HideLegend)
		DRAW_COMMON_MISSION_CONTROLLER_DEBUG_STATE_LEGEND()
		sContentOverviewDebug.iLegendPrintY = 0
		sContentOverviewDebug.iLegendPrintX = 1
		
		tl63_Name += "Number of Placed Zones: "
		tl63_Name += g_FMMC_STRUCT_ENTITIES.iNumberOfZones
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.04, FALSE, 1, 0)
		
		tl63_Name = "Number of Current Zones: "
		tl63_Name += sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.04, FALSE, 0, 2)
		sContentOverviewDebug.iLegendPrintX = 0
	
		// Middle Extra Info Draw ---------------
		
		
		// Legend and Key at the bottom -------------
		sContentOverviewDebug.iLegendPrintY++
		sContentOverviewDebug.iLegendPrintY++
		sContentOverviewDebug.iLegendPrintY++
		sContentOverviewDebug.iLegendPrintY++
		
		sContentOverviewDebug.iLegendPrintX = 0
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey1, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 1, 0)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey2, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
		
		sContentOverviewDebug.iLegendPrintX = 0
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Exists / Active", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Spawns On Later Rule", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.1)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Dead / Spawn Blocked", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.2)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+0.1+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 200, 50, 200)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+0.2+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 0, 0, 200)
	ENDIF
	
	// Main Draw Loop -------------------------
	sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 0
	INT i = 0
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfZones-1
		FLOAT fTextSize = sContentOverviewDebug.fBaseTextSize
		FLOAT fEntityListColumnSpacingBase = (sContentOverviewDebug.fRectSizeX / GET_NUMBER_OF_COLUMNS_FOR_ITEMS(sContentOverviewDebug.eDebugWindow))
		FLOAT fEntityListColumnSpacing = (fEntityListColumnSpacingBase * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow)))
		FLOAT fEntityListStartPosX = sContentOverviewDebug.fTextPosX
		FLOAT fEntityListStartPosY = sContentOverviewDebug.fTextPosY + 0.02
		
		FLOAT fEntityNamePosX = fEntityListStartPosX + fEntityListColumnSpacing
		FLOAT fEntityNamePosY = fEntityListStartPosY + (sContentOverviewDebug.fYSpacing * i) -  (sContentOverviewDebug.fYSpacing * (GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow) * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow))))

		// Warp Click Event (Player Ped)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_TOP_LEFT, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		// Warp Click Event (Camera)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_TOP_RIGHT, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		// Warp Click Event (Show Debug)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_BOTTOM_LEFT, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_DRAW_MARKER_DEBUG, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		tl63_EntityName = "  Zone: "
		tl63_EntityName += i		
			
		IF SHOULD_CREATE_RESTRICTION_ZONE(i, MC_PlayerBD[iLocalPart].iTeam)
		 	DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)		// Alive			
		 	sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]++
			
		ELIF NOT IS_BIT_SET(iLocalZoneRemovedBitset, i)
		 	DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 200, 50, 200) 	// Not Spawned yet but can.
			
		ELSE
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 0, 0, 200)		// Dead/Can't Spawn
			
		ENDIF
				
		SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
			CASE 0								
				
			BREAK
			CASE 1
				TEXT_LABEL_15 tlZoneType
				tlZoneType = "FMMC_ZN_TY"
				tlZoneType += GET_FMMC_ZONE_TYPE(i)
				STRING sZoneName
				sZoneName = GET_FILENAME_FOR_AUDIO_CONVERSATION(tlZoneType)

				tl63_EntityName += " : "
				tl63_EntityName += sZoneName
			BREAK
			CASE 2	
			
			BREAK
		ENDSWITCH
		
		DRAW_ENTITY_DEBUG_WINDOW_TEXT(tl63_EntityName, fEntityNamePosX, fEntityNamePosY, fTextSize)
	ENDFOR
ENDPROC

PROC PROCESS_PROP_STATE_DEBUG_WINDOW()
	
	IF NOT SET_UP_INITIAL_CONTENT_OVERVIEW_DEBUG_WINDOW_DIMENSIONS()
		EXIT
	ENDIF
		
	TEXT_LABEL_63 tl63_EntityName
	TEXT_LABEL_63 tl63_EntityKey1
	TEXT_LABEL_63 tl63_EntityKey2
	TEXT_LABEL_63 tl63_Name
	
	sContentOverviewDebug.iDebugWindowPageMax[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 2
	SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		CASE 0
			tl63_EntityKey1 = "Prop Key: Creator ID : Model Name"
			tl63_EntityKey2 = ""
		BREAK
		CASE 1
			tl63_EntityKey1 = "Prop Key: Creator ID : Health"
			tl63_EntityKey2 = ""
		BREAK
	ENDSWITCH
			
	IF NOT IS_BIT_SET(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_HideLegend)			
		DRAW_COMMON_MISSION_CONTROLLER_DEBUG_STATE_LEGEND()
		sContentOverviewDebug.iLegendPrintY = 0
		sContentOverviewDebug.iLegendPrintX = 1
		
		tl63_Name += "Number of Placed Prop: "
		tl63_Name += g_FMMC_STRUCT_ENTITIES.iNumberOfProps
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.04, FALSE, 1, 0)		
		
		tl63_Name = "Number of Current Prop: "
		tl63_Name += sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.04, FALSE, 0, 2)
		sContentOverviewDebug.iLegendPrintX = 0
		
		// Middle Extra Info Draw ---------------
		
		
		// Legend and Key at the bottom -------------
		sContentOverviewDebug.iLegendPrintY++
		sContentOverviewDebug.iLegendPrintY++
		sContentOverviewDebug.iLegendPrintY++
		sContentOverviewDebug.iLegendPrintY++		
		
		sContentOverviewDebug.iLegendPrintX = 0
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey1, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 1, 0)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey2, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
		
		sContentOverviewDebug.iLegendPrintX = 0			
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Exists / Active", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0)		
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Dead / Spawn Blocked", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.2)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)		
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+0.2+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 0, 0, 200)
	ENDIF
	
	// Main Draw Loop -------------------------
	
	INT iPropStart, iPropEnd
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfProps > 100
		IF sContentOverviewDebug.iToggleEntitiesDueToTooMuchTextDraws = 1
			iPropStart = 100
			iPropEnd = g_FMMC_STRUCT_ENTITIES.iNumberOfProps
		ELSE	
			iPropStart = 0
			iPropEnd = 100
		ENDIF
	ELSE
		iPropStart = 0
		iPropEnd = g_FMMC_STRUCT_ENTITIES.iNumberOfProps
	ENDIF
	
	sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 0
	OBJECT_INDEX tempEntity
	INT i = 0
	FOR i = iPropStart TO iPropEnd-1
		
		tempEntity = oiProps[i]
		FLOAT fTextSize = sContentOverviewDebug.fBaseTextSize*0.8
		FLOAT fEntityListColumnSpacingBase = (sContentOverviewDebug.fRectSizeX / GET_NUMBER_OF_COLUMNS_FOR_ITEMS(sContentOverviewDebug.eDebugWindow))
		FLOAT fEntityListColumnSpacing = (fEntityListColumnSpacingBase * ((i-iPropStart) / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow)))
		FLOAT fEntityListStartPosX = sContentOverviewDebug.fTextPosX
		FLOAT fEntityListStartPosY = sContentOverviewDebug.fTextPosY + 0.02
		
		FLOAT fEntityNamePosX = fEntityListStartPosX + fEntityListColumnSpacing
		FLOAT fEntityNamePosY = fEntityListStartPosY + (sContentOverviewDebug.fYSpacing * (i-iPropStart)) -  (sContentOverviewDebug.fYSpacing * (GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow) * ((i-iPropStart) / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow))))
		
		// Warp Click Event (Player Ped)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_TOP_LEFT, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		// Warp Click Event (Camera)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_TOP_RIGHT, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		tl63_EntityName = "  Prop: "
		tl63_EntityName += i
		
		IF NOT IS_ENTITY_DEAD(tempEntity)																							
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)		// Alive		
			sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]++
					
		ELSE			
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 0, 0, 200)		// Dead/Can't Spawn anymore
			
		ENDIF
		
		SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
			CASE 0			
				tl63_EntityName += " : "
				tl63_EntityName += FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn)	
			BREAK
			CASE 1
				IF NOT IS_ENTITY_DEAD(tempEntity)
					INT iHealth
					FLOAT fHealthCurrent
					fHealthCurrent = TO_FLOAT(GET_ENTITY_HEALTH(tempEntity))
					FLOAT fHealthMax
					fHealthMax = TO_FLOAT(GET_ENTITY_MAX_HEALTH(tempEntity))
					FLOAT fPercent
					fPercent = (fHealthCurrent / fHealthMax) * 100
					iHealth = ROUND(fPercent)
					
					tl63_EntityName += " : "
					tl63_EntityName += iHealth
					tl63_EntityName += "%"
				ENDIF
			BREAK
		ENDSWITCH
				
		DRAW_ENTITY_DEBUG_WINDOW_TEXT(tl63_EntityName, fEntityNamePosX, fEntityNamePosY, fTextSize)		
	ENDFOR
ENDPROC

PROC PROCESS_DYNOPROP_STATE_DEBUG_WINDOW()

	IF NOT SET_UP_INITIAL_CONTENT_OVERVIEW_DEBUG_WINDOW_DIMENSIONS()
		EXIT
	ENDIF
	
	TEXT_LABEL_63 tl63_EntityName
	TEXT_LABEL_63 tl63_EntityKey1
	TEXT_LABEL_63 tl63_EntityKey2
	TEXT_LABEL_63 tl63_Name
	
	sContentOverviewDebug.iDebugWindowPageMax[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 3
	SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		CASE 0
			tl63_EntityKey1 = "Dyno Prop Key: Creator ID : NET ID"
			tl63_EntityKey2 = ""
		BREAK
		CASE 1
			tl63_EntityKey1 = "Dyno Prop Key: Creator ID : Model Name : Health"
			tl63_EntityKey2 = ""
		BREAK
		CASE 2
			tl63_EntityKey1 = "Dyno Prop Key: Creator ID : Net ID : Network Ownership"
			tl63_EntityKey2 = ""
		BREAK
	ENDSWITCH
		
	IF NOT IS_BIT_SET(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_HideLegend)	
		DRAW_COMMON_MISSION_CONTROLLER_DEBUG_STATE_LEGEND()
		sContentOverviewDebug.iLegendPrintY = 0
		sContentOverviewDebug.iLegendPrintX = 1
		
		tl63_Name += "Number of Placed Dyno Prop: "
		tl63_Name += g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.04, FALSE, 1, 0)
		
		tl63_Name = "Number of Current Dyno Prop: "
		tl63_Name += sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.04, FALSE, 0, 2)
		sContentOverviewDebug.iLegendPrintX = 0
		
		// Middle Extra Info Draw ---------------
		
		
		// Legend and Key at the bottom -------------
		sContentOverviewDebug.iLegendPrintY++
		sContentOverviewDebug.iLegendPrintY++
		sContentOverviewDebug.iLegendPrintY++
		sContentOverviewDebug.iLegendPrintY++
				
		sContentOverviewDebug.iLegendPrintX = 0
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey1, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 1, 0)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey2, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
		
		sContentOverviewDebug.iLegendPrintX = 0
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Exists / Active", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Dead / Spawn Blocked", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.2)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)		
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+0.2+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 0, 0, 200)
	ENDIF
	
	// Main Draw Loop -------------------------
	sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 0
	OBJECT_INDEX tempEntity
	NETWORK_INDEX netEntity
	INT i = 0
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps-1
		
		netEntity = MC_serverBD_1.sFMMC_SBD.niDynoProps[i]
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(netEntity)
			tempEntity = NET_TO_OBJ(netEntity)			
		ELSE
			tempEntity = NULL
		ENDIF
		FLOAT fTextSize = sContentOverviewDebug.fBaseTextSize
		FLOAT fEntityListColumnSpacingBase = (sContentOverviewDebug.fRectSizeX / GET_NUMBER_OF_COLUMNS_FOR_ITEMS(sContentOverviewDebug.eDebugWindow))
		FLOAT fEntityListColumnSpacing = (fEntityListColumnSpacingBase * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow)))
		FLOAT fEntityListStartPosX = sContentOverviewDebug.fTextPosX
		FLOAT fEntityListStartPosY = sContentOverviewDebug.fTextPosY + 0.02
		
		FLOAT fEntityNamePosX = fEntityListStartPosX + fEntityListColumnSpacing
		FLOAT fEntityNamePosY = fEntityListStartPosY + (sContentOverviewDebug.fYSpacing * i) -  (sContentOverviewDebug.fYSpacing * (GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow) * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow))))
		
		// Warp Click Event (Player Ped)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_TOP_LEFT, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		// Warp Click Event (Camera)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_TOP_RIGHT, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		tl63_EntityName = "  Dyno: "
		tl63_EntityName += i
		IF NOT IS_ENTITY_DEAD(tempEntity) AND NETWORK_DOES_NETWORK_ID_EXIST(netEntity)	
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)		// Alive		
			sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]++
			
		ELSE																														
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 0, 0, 200)		// Dead/Can't Spawn anymore
			
		ENDIF
		
		SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
			CASE 0
				tl63_EntityName += " ("
				IF DOES_ENTITY_EXIST(tempEntity)
				AND NETWORK_DOES_NETWORK_ID_EXIST(netEntity)
					tl63_EntityName += NETWORK_ENTITY_GET_OBJECT_ID(tempEntity)
				ELSE
					tl63_EntityName += "--"
				ENDIF
				tl63_EntityName += ")"
				
				IF iDynopropCachedCamIndexes[i] != -1
					tl63_EntityName += " : "
					tl63_EntityName += " Cam Index: "
					tl63_EntityName += iDynopropCachedCamIndexes[i]
				ENDIF
				
				IF HAS_OBJECT_BEEN_BROKEN(tempEntity, FALSE)
					tl63_EntityName += " : "
					tl63_EntityName += " Broken"
				ENDIF
			BREAK
			CASE 1
				tl63_EntityName += " : "
				tl63_EntityName += FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].mn)
				
				IF NOT IS_ENTITY_DEAD(tempEntity)
					IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].iPropHealthOverride != -1
						INT iHealth
						FLOAT fHealthCurrent
						fHealthCurrent = TO_FLOAT(GET_ENTITY_HEALTH(tempEntity))
						FLOAT fHealthMax
						fHealthMax = TO_FLOAT(GET_ENTITY_MAX_HEALTH(tempEntity))
						FLOAT fPercent
						fPercent = (fHealthCurrent / fHealthMax) * 100
						iHealth = ROUND(fPercent)
						
						tl63_EntityName += " : "
						tl63_EntityName += iHealth
						tl63_EntityName += "%"
					ELSE
						tl63_EntityName += " : "
						tl63_EntityName += "100%"
					ENDIF
				ENDIF
			BREAK
			CASE 2
				tl63_EntityName += " : "
				tl63_EntityName += "(NetID "
				IF DOES_ENTITY_EXIST(tempEntity)
				AND NETWORK_DOES_NETWORK_ID_EXIST(netEntity)
					tl63_EntityName += NETWORK_ENTITY_GET_OBJECT_ID(tempEntity)
				ELSE
					tl63_EntityName += "--"
				ENDIF
				tl63_EntityName += ")"
				
				IF NETWORK_DOES_NETWORK_ID_EXIST(netEntity)
					tl63_EntityName += " : "
					IF NETWORK_HAS_CONTROL_OF_ENTITY(tempEntity)
						tl63_EntityName += "Locally Owned"
					ELSE
						tl63_EntityName += "Remotely Owned"
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
				
		DRAW_ENTITY_DEBUG_WINDOW_TEXT(tl63_EntityName, fEntityNamePosX, fEntityNamePosY, fTextSize)		
	ENDFOR
ENDPROC

PROC PROCESS_OBJECTS_STATE_DEBUG_WINDOW()
	
	IF NOT SET_UP_INITIAL_CONTENT_OVERVIEW_DEBUG_WINDOW_DIMENSIONS()
		EXIT
	ENDIF
	
	TEXT_LABEL_63 tl63_EntityName
	TEXT_LABEL_63 tl63_EntityKey1
	TEXT_LABEL_63 tl63_EntityKey2	
	TEXT_LABEL_63 tl63_Name
	
	sContentOverviewDebug.iDebugWindowPageMax[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 3
	SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		CASE 0
			tl63_EntityKey1 = "Object Key: Creator ID : Net ID : Portable : Minigame"
			tl63_EntityKey2 = "+ Camera Index : Attach"
		BREAK
		CASE 1
			tl63_EntityKey1 = "Object Key: Creator ID : Model Name : Respawns : Health"
			tl63_EntityKey2 = ""
		BREAK
		CASE 2
			tl63_EntityKey1 = "Object Key: Creator ID : Net ID : Network Ownership"
			tl63_EntityKey2 = ""
		BREAK
	ENDSWITCH
		
	IF NOT IS_BIT_SET(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_HideLegend)			
		DRAW_COMMON_MISSION_CONTROLLER_DEBUG_STATE_LEGEND()
		sContentOverviewDebug.iLegendPrintY = 0
		sContentOverviewDebug.iLegendPrintX = 1
		
		tl63_Name += "Number of Placed Objects: "
		tl63_Name += g_FMMC_STRUCT_ENTITIES.iNumberOfObjects
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.04, FALSE, 1, 0)
		
		tl63_Name = "Number of Current Objects: "
		tl63_Name += sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.04, FALSE, 0, 2)
		sContentOverviewDebug.iLegendPrintX = 0
		
		// Middle Extra Info Draw ---------------
		tl63_Name = "Player Object Near: "
		IF MC_playerBD[iPartToUse].iObjNear = -1
			tl63_Name += "None"
		ELSE
			tl63_Name += MC_playerBD[iPartToUse].iObjNear
		ENDIF
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
		
		// Legend and Key at the bottom -------------
		sContentOverviewDebug.iLegendPrintY++
		sContentOverviewDebug.iLegendPrintY++
		sContentOverviewDebug.iLegendPrintY++
		
		sContentOverviewDebug.iLegendPrintX = 0
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey1, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 1, 0)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey2, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
		
		sContentOverviewDebug.iLegendPrintX = 0
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Exists / Active", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Spawns On Later Rule", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.1)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Dead / Spawn Blocked", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.2)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+0.1+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 200, 50, 200)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+0.2+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 0, 0, 200)
	ENDIF
	
	// Main Draw Loop -------------------------
	sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 0
	STRING sPlayerName = ""	
	OBJECT_INDEX tempEntity
	NETWORK_INDEX netEntity
	INT i = 0
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfObjects-1
		
		sPlayerName = ""
		
		netEntity = MC_serverBD_1.sFMMC_SBD.niObject[i]
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(netEntity)
			tempEntity = NET_TO_OBJ(netEntity)			
		ELSE
			tempEntity = NULL
		ENDIF
		FLOAT fTextSize = sContentOverviewDebug.fBaseTextSize
		FLOAT fEntityListColumnSpacingBase = (sContentOverviewDebug.fRectSizeX / GET_NUMBER_OF_COLUMNS_FOR_ITEMS(sContentOverviewDebug.eDebugWindow))
		FLOAT fEntityListColumnSpacing = (fEntityListColumnSpacingBase * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow)))
		FLOAT fEntityListStartPosX = sContentOverviewDebug.fTextPosX
		FLOAT fEntityListStartPosY = sContentOverviewDebug.fTextPosY + 0.02
		
		FLOAT fEntityNamePosX = fEntityListStartPosX + fEntityListColumnSpacing
		FLOAT fEntityNamePosY = fEntityListStartPosY + (sContentOverviewDebug.fYSpacing * i) -  (sContentOverviewDebug.fYSpacing * (GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow) * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow))))
		
		// Warp Click Event (Player Ped)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_TOP_LEFT, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		// Warp Click Event (Camera)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_TOP_RIGHT, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		tl63_EntityName = "  Obj: "
		tl63_EntityName += i
		
		IF NOT IS_ENTITY_DEAD(tempEntity) AND NETWORK_DOES_NETWORK_ID_EXIST(netEntity)
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)		// Alive						
			sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]++
			
		ELIF MC_serverBD_2.iCurrentObjRespawnLives[i] > 0
		AND CAN_OBJ_STILL_SPAWN_IN_MISSION(i)
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 200, 50, 200) 	// Not Spawned yet but can.
		
		ELSE																												
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 0, 0, 200)		// Dead/Can't Spawn anymore
			
		ENDIF
				
		SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
			CASE 0
			
				tl63_EntityName += " ("
				IF DOES_ENTITY_EXIST(tempEntity)
				AND NETWORK_DOES_NETWORK_ID_EXIST(netEntity)
					tl63_EntityName += NETWORK_ENTITY_GET_OBJECT_ID(tempEntity)
				ELSE
					tl63_EntityName += "--"
				ENDIF
				tl63_EntityName += ")"
				
				IF SHOULD_PLACED_OBJECT_BE_PORTABLE(i)
					tl63_EntityName += " : "
					tl63_EntityName += "Portable"
				ELSE
					tl63_EntityName += " : "
					tl63_EntityName += "Stationary"
				ENDIF
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectMinigame != ciFMMC_OBJECT_MINIGAME_TYPE__NONE
					tl63_EntityName += " : "
					tl63_EntityName += "Minigame"
				ENDIF
				
				IF iObjCachedCamIndexes[i] != -1
					tl63_EntityName += " : "
					tl63_EntityName += " Cam Index: "
					tl63_EntityName += iObjCachedCamIndexes[i]
				ENDIF
						
				IF NOT IS_ENTITY_DEAD(tempEntity)
				
					ENTITY_INDEX attachEnt
					attachEnt = GET_ENTITY_ATTACHED_TO(tempEntity)
					
					IF IS_ENTITY_A_PED(attachEnt)			
						PED_INDEX attachPed
						attachPed = GET_PED_INDEX_FROM_ENTITY_INDEX(attachEnt)
						IF NOT IS_PED_INJURED(attachPed)
							
							INT iPed
							iPed = IS_ENTITY_A_MISSION_CREATOR_ENTITY(attachPed)
							IF iPed > -1
								tl63_EntityName += " + Ped "
								tl63_EntityName += iPed
								
							ELIF IS_PED_A_PLAYER(attachPed)
								PLAYER_INDEX piPlayer 
								piPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(attachPed)
								sPlayerName = GET_PLAYER_NAME(piPlayer)
								tl63_EntityName += " : "
								tl63_EntityName += sPlayerName
								
							ENDIF
						ENDIF
					ELIF IS_ENTITY_A_VEHICLE(attachEnt)
						VEHICLE_INDEX attachVeh 
						attachVeh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(attachEnt)
						IF IS_VEHICLE_DRIVEABLE(attachVeh)
							INT iVeh
							iVeh = IS_ENTITY_A_MISSION_CREATOR_ENTITY(attachVeh)
							IF iVeh > -1
								tl63_EntityName += " + Veh "
								tl63_EntityName += iVeh			
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 1				
				tl63_EntityName += " : "
				tl63_EntityName += FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn)
				
				tl63_EntityName += " : "				
				tl63_EntityName += MC_serverBD_2.iCurrentObjRespawnLives[i]
				
				IF NOT IS_ENTITY_DEAD(tempEntity)
					IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectHealth != -1
						INT iHealth
						FLOAT fHealthCurrent
						fHealthCurrent = TO_FLOAT(GET_ENTITY_HEALTH(tempEntity))
						FLOAT fHealthMax
						fHealthMax = TO_FLOAT(GET_ENTITY_MAX_HEALTH(tempEntity))
						FLOAT fPercent
						fPercent = (fHealthCurrent / fHealthMax) * 100
						iHealth = ROUND(fPercent)
						
						tl63_EntityName += " : "
						tl63_EntityName += iHealth
						tl63_EntityName += "%"
					ELSE
						tl63_EntityName += " : "
						tl63_EntityName += "100%"
					ENDIF
				ENDIF
			BREAK
			CASE 2
				tl63_EntityName += " : "
				tl63_EntityName += "(NetID "
				IF DOES_ENTITY_EXIST(tempEntity)
				AND NETWORK_DOES_NETWORK_ID_EXIST(netEntity)
					tl63_EntityName += NETWORK_ENTITY_GET_OBJECT_ID(tempEntity)
				ELSE
					tl63_EntityName += "--"
				ENDIF
				tl63_EntityName += ")"
				
				IF NETWORK_DOES_NETWORK_ID_EXIST(netEntity)
					tl63_EntityName += " : "
					IF NETWORK_HAS_CONTROL_OF_ENTITY(tempEntity)
						tl63_EntityName += "Locally Owned"
					ELSE
						tl63_EntityName += "Remotely Owned"
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
			
		DRAW_ENTITY_DEBUG_WINDOW_TEXT(tl63_EntityName, fEntityNamePosX, fEntityNamePosY, fTextSize)	
	ENDFOR
ENDPROC

PROC PROCESS_VEHICLE_STATE_DEBUG_WINDOW()
	
	IF NOT SET_UP_INITIAL_CONTENT_OVERVIEW_DEBUG_WINDOW_DIMENSIONS()
		EXIT
	ENDIF
	
	TEXT_LABEL_63 tl63_EntityName
	TEXT_LABEL_63 tl63_EntityKey1
	TEXT_LABEL_63 tl63_EntityKey2
	TEXT_LABEL_63 tl63_Name
	
	sContentOverviewDebug.iDebugWindowPageMax[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 3
	SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		CASE 0
			tl63_EntityKey1 = "Vehicle Key: Creator ID : Net ID : Vehicle Type : Driver"
			tl63_EntityKey2 = "Name/ID"
		BREAK
		CASE 1
			tl63_EntityKey1 = "Vehicle Key: Creator ID : Model Name : Respawns : Health"
			tl63_EntityKey2 = ""
		BREAK
		CASE 2
			tl63_EntityKey1 = "Vehicle Key: Creator ID : Net ID : Network Ownership"
			tl63_EntityKey2 = ""
		BREAK
	ENDSWITCH
	
	IF NOT IS_BIT_SET(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_HideLegend)			
		DRAW_COMMON_MISSION_CONTROLLER_DEBUG_STATE_LEGEND()
		sContentOverviewDebug.iLegendPrintY = 0
		sContentOverviewDebug.iLegendPrintX = 1
		
		tl63_Name += "Number of Placed Vehicles: "
		tl63_Name += g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.04, FALSE, 1, 0)
		
		tl63_Name = "Number of Current Vehicles: "
		tl63_Name += sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.04, FALSE, 0, 2)
		sContentOverviewDebug.iLegendPrintX = 0
		
		// Middle Extra Info Draw ---------------
		tl63_Name = "Player Vehicle Near: "
		IF MC_playerBD[iPartToUse].iVehNear = -1
			tl63_Name += "None"
		ELSE
			tl63_Name += MC_playerBD[iPartToUse].iVehNear
		ENDIF
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
		
		// Legend and Key at the bottom -------------
		sContentOverviewDebug.iLegendPrintY++
		sContentOverviewDebug.iLegendPrintY++
		sContentOverviewDebug.iLegendPrintY++
		
		sContentOverviewDebug.iLegendPrintX = 0
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey1, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 1, 0)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey2, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
		
		sContentOverviewDebug.iLegendPrintX = 0
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Exists / Active", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Spawns On Later Rule", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.1)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Dead / Spawn Blocked", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.2)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+0.1+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 200, 50, 200)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+0.2+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 0, 0, 200)
	ENDIF
	
	// Main Draw Loop -------------------------
	sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 0
	STRING sPlayerName = ""	
	VEHICLE_INDEX tempEntity
	NETWORK_INDEX netEntity
	INT i = 0
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles-1
		
		sPlayerName = ""
		
		netEntity = MC_serverBD_1.sFMMC_SBD.niVehicle[i]
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(netEntity)
			tempEntity = NET_TO_VEH(netEntity)			
		ELSE
			tempEntity = NULL
		ENDIF
		FLOAT fTextSize = sContentOverviewDebug.fBaseTextSize
		FLOAT fEntityListColumnSpacingBase = (sContentOverviewDebug.fRectSizeX / GET_NUMBER_OF_COLUMNS_FOR_ITEMS(sContentOverviewDebug.eDebugWindow))
		FLOAT fEntityListColumnSpacing = (fEntityListColumnSpacingBase * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow)))
		FLOAT fEntityListStartPosX = sContentOverviewDebug.fTextPosX
		FLOAT fEntityListStartPosY = sContentOverviewDebug.fTextPosY + 0.02
		
		FLOAT fEntityNamePosX = fEntityListStartPosX + fEntityListColumnSpacing
		FLOAT fEntityNamePosY = fEntityListStartPosY + (sContentOverviewDebug.fYSpacing * i) -  (sContentOverviewDebug.fYSpacing * (GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow) * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow))))
		
		// Warp Click Event (Player Ped)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_TOP_LEFT, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		// Warp Click Event (Camera)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_TOP_RIGHT, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		tl63_EntityName = "  Veh: "
		tl63_EntityName += i
			
		IF IS_VEHICLE_DRIVEABLE(tempEntity) AND NETWORK_DOES_NETWORK_ID_EXIST(netEntity)
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)		// Alive
			sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]++
		
		ELIF MC_serverBD_2.iCurrentVehRespawnLives[i] > 0
		AND CAN_VEH_STILL_SPAWN_IN_MISSION(i)
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 200, 50, 200) 	// Not Spawned yet but can.
		
		ELSE
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 0, 0, 200)		// Dead/Can't Spawn anymore
			
		ENDIF
		
		SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
			CASE 0				
				tl63_EntityName += " ("
				IF DOES_ENTITY_EXIST(tempEntity)
				AND NETWORK_DOES_NETWORK_ID_EXIST(netEntity)
					tl63_EntityName += NETWORK_ENTITY_GET_OBJECT_ID(tempEntity)
				ELSE
					tl63_EntityName += "--"
				ENDIF
				tl63_EntityName += ")"
				
				
				IF IS_THIS_MODEL_A_CAR(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
					tl63_EntityName += " : "
					tl63_EntityName += "Car"
				ELIF IS_THIS_MODEL_A_QUADBIKE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
					tl63_EntityName += " : "
					tl63_EntityName += "Quadbike"
				ELIF IS_THIS_MODEL_A_PLANE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
					tl63_EntityName += " : "
					tl63_EntityName += "Plane"
				ELIF IS_THIS_MODEL_A_JETSKI(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
					tl63_EntityName += " : "
					tl63_EntityName += "Jetski"
				ELIF IS_THIS_MODEL_A_BOAT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
					tl63_EntityName += " : "
					tl63_EntityName += "Boat"
				ELIF IS_THIS_MODEL_A_BIKE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
					tl63_EntityName += " : "
					tl63_EntityName += "Motorbike"
				ELIF IS_THIS_MODEL_A_BICYCLE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
					tl63_EntityName += " : "
					tl63_EntityName += "Bicycle"
				ELIF IS_THIS_MODEL_A_HELI(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
					tl63_EntityName += " : "
					tl63_EntityName += "Heli"
				ELIF IS_THIS_MODEL_A_TRUCK(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
					tl63_EntityName += " : "
					tl63_EntityName += "Truck"
				ELSE
					tl63_EntityName += " : "
					tl63_EntityName += "Vehicle"		
				ENDIF
			
				IF IS_VEHICLE_DRIVEABLE(tempEntity)
					PED_INDEX tempPed
					tempPed = GET_PED_IN_VEHICLE_SEAT(tempEntity, VS_DRIVER)
					IF NOT IS_PED_INJURED(tempPed)
						
						INT iPed
						iPed = IS_ENTITY_A_MISSION_CREATOR_ENTITY(tempPed)
						IF iPed > -1
							tl63_EntityName += " + Ped "
							tl63_EntityName += iPed
							
						ELIF IS_PED_A_PLAYER(tempPed)
							PLAYER_INDEX piPlayer
							piPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(tempPed)
							sPlayerName = GET_PLAYER_NAME(piPlayer)
							tl63_EntityName += " : "
							tl63_EntityName += sPlayerName
							
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 1				
				tl63_EntityName += " : "
				tl63_EntityName += FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
				
				tl63_EntityName += " : "
				tl63_EntityName += MC_serverBD_2.iCurrentVehRespawnLives[i]
				
				IF IS_VEHICLE_DRIVEABLE(tempEntity)
					INT iHealth
					iHealth = ROUND(GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(tempEntity, i))
									
					tl63_EntityName += " : "
					tl63_EntityName += iHealth
					tl63_EntityName += "%"					
				ENDIF
			BREAK
			CASE 2
				tl63_EntityName += " : "
				tl63_EntityName += "(NetID "
				IF DOES_ENTITY_EXIST(tempEntity)
				AND NETWORK_DOES_NETWORK_ID_EXIST(netEntity)
					tl63_EntityName += NETWORK_ENTITY_GET_OBJECT_ID(tempEntity)
				ELSE
					tl63_EntityName += "--"
				ENDIF
				tl63_EntityName += ")"
								
				IF NETWORK_DOES_NETWORK_ID_EXIST(netEntity)
					tl63_EntityName += " : "
					IF NETWORK_HAS_CONTROL_OF_ENTITY(tempEntity)
						tl63_EntityName += "Locally Owned"
					ELSE
						tl63_EntityName += "Remotely Owned"
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		
		DRAW_ENTITY_DEBUG_WINDOW_TEXT(tl63_EntityName, fEntityNamePosX, fEntityNamePosY, fTextSize)		
	ENDFOR
ENDPROC

PROC PROCESS_PED_STATE_DEBUG_WINDOW()
		
	IF NOT SET_UP_INITIAL_CONTENT_OVERVIEW_DEBUG_WINDOW_DIMENSIONS()
		EXIT
	ENDIF
	
	TEXT_LABEL_63 tl63_EntityName
	TEXT_LABEL_63 tl63_EntityKey1
	TEXT_LABEL_63 tl63_EntityKey2
	TEXT_LABEL_63 tl63_Name	
	
	sContentOverviewDebug.iDebugWindowPageMax[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 4
	SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		CASE 0
			tl63_EntityKey1 = "Ped Key: Creator ID : Net ID : Stealth and Aggro : "
			tl63_EntityKey2 = "SP = Spooked Order : AG = Aggroed Order : Task Name : Goto Num"
		BREAK
		CASE 1
			tl63_EntityKey1 = "Ped Key: Creator ID : Model Name : Respawns : Health"
			tl63_EntityKey2 = ""
		BREAK
		CASE 2
			tl63_EntityKey1 = "Ped Key: Creator ID : Net ID : Network Ownership"
			tl63_EntityKey2 = ""
		BREAK		
		CASE 3
			tl63_EntityKey1 = "Ped Key: Creator ID : In Veh ID : In Veh Net ID"
			tl63_EntityKey2 = ""
		BREAK	
	ENDSWITCH			
	
	IF NOT IS_BIT_SET(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_HideLegend)
		DRAW_COMMON_MISSION_CONTROLLER_DEBUG_STATE_LEGEND()
		sContentOverviewDebug.iLegendPrintY = 0
		sContentOverviewDebug.iLegendPrintX = 1
		
		tl63_Name += "Number of Placed Peds: "
		tl63_Name += g_FMMC_STRUCT_ENTITIES.iNumberOfPeds		
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.04, FALSE, 1, 0)
		
		tl63_Name = "Number of Current Peds: "
		tl63_Name += sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.04, FALSE, 0, 2)
						
		// Middle Extra Info Draw ---------------
		tl63_Name = "Player Ped Near: "
		IF MC_playerBD[iPartToUse].iPedNear = -1
			tl63_Name += "None"
		ELSE
			tl63_Name += MC_playerBD[iPartToUse].iPedNear
		ENDIF
		sContentOverviewDebug.iLegendPrintX = 0
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)		
			
		tl63_Name = "Player Causing aggro fail: "
		tl63_Name += MC_serverBD.iPartCausingFail[0]	
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)		
			
		tl63_Name = "Has Team Aggroed: "
		IF HAS_ANY_TEAM_TRIGGERED_AGGRO()
			tl63_Name += " Yes   "
		ELSE
			tl63_Name += " No    "
		ENDIF
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 1, 0)
				
		tl63_Name = "[SHIFT NUMPAD MINUS] to bring up Aggro Indexes: "
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 2)		
		
		// Legend and Key at the bottom -------------		
		sContentOverviewDebug.iLegendPrintX = 0
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey1, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 1, 0)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_EntityKey2, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)
		
		sContentOverviewDebug.iLegendPrintX = 0
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Exists / Active", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Spawns On Later Rule", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.1)
		DRAW_LEGEND_DEBUG_WINDOW_TEXT("Dead / Spawn Blocked", sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 0, 0.2)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+0.1+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 200, 50, 200)
		DRAW_RECT(sContentOverviewDebug.vLegendTextDrawPos.x+0.2+sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.vLegendTextDrawPos.y + (sContentOverviewDebug.fDrawSpacingY*sContentOverviewDebug.iLegendPrintY)+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 0, 0, 200)
	ENDIF
	
	// Main Draw Loop -------------------------
	sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 0
	STRING sEntityState = ""
	PED_INDEX tempEntity
	NETWORK_INDEX netEntity
	INT i = 0
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfPeds-1
		
		netEntity = MC_serverBD_1.sFMMC_SBD.niPed[i]
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(netEntity)
			tempEntity = NET_TO_PED(netEntity)
		ELSE
			tempEntity = NULL
		ENDIF
		FLOAT fTextSize = sContentOverviewDebug.fBaseTextSize
		FLOAT fEntityListColumnSpacingBase = (sContentOverviewDebug.fRectSizeX / GET_NUMBER_OF_COLUMNS_FOR_ITEMS(sContentOverviewDebug.eDebugWindow))
		FLOAT fEntityListColumnSpacing = (fEntityListColumnSpacingBase * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow)))
		FLOAT fEntityListStartPosX = sContentOverviewDebug.fTextPosX
		FLOAT fEntityListStartPosY = sContentOverviewDebug.fTextPosY + 0.02
		
		FLOAT fEntityNamePosX = fEntityListStartPosX + fEntityListColumnSpacing
		FLOAT fEntityNamePosY = fEntityListStartPosY + (sContentOverviewDebug.fYSpacing * i) -  (sContentOverviewDebug.fYSpacing * (GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow) * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow))))
		
		// Warp Click Event (Player Ped)		
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_TOP_LEFT, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		// Warp Click Event (Camera)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_TOP_RIGHT, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_INDEX, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		// Warp Click Event (Show Debug)
		CREATE_CONTENT_OVERVIEW_DEBUG_BUTTON(ciCONTENT_OVERVIEW_DEBUG_BUTTON_BOTTOM_LEFT, i, CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_DRAW_MARKER_DEBUG, fEntityNamePosX, fEntityNamePosY, fEntityListColumnSpacingBase, TRUE, "")
		
		tl63_EntityName = "  Ped: "
		tl63_EntityName += i
		
		IF NOT IS_PED_INJURED(tempEntity) AND NETWORK_DOES_NETWORK_ID_EXIST(netEntity)								
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 0, 255, 0, 200)		// Alive
			sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]++
			
		ELIF MC_serverBD_2.iCurrentPedRespawnLives[i] > 0
		AND CAN_PED_STILL_SPAWN_IN_MISSION(i)		
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 200, 50, 200) 	// Not Spawned yet but can.			
			
		ELSE
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX, sContentOverviewDebug.fStatusRectY, 255, 0, 0, 200)		// Dead/Can't Spawn anymore		
			
		ENDIF
		
		sEntityState = ""
		SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
			CASE 0	
				tl63_EntityName += " ("
				IF DOES_ENTITY_EXIST(tempEntity)
				AND NETWORK_DOES_NETWORK_ID_EXIST(netEntity)
					tl63_EntityName += NETWORK_ENTITY_GET_OBJECT_ID(tempEntity)
				ELSE
					tl63_EntityName += "--"
				ENDIF
				tl63_EntityName += ")"
				
				IF NOT IS_PED_INJURED(tempEntity)
									
					IF iPedSpookReasonDebugWindow[i] > 0
					AND NOT FMMC_IS_LONG_BIT_SET(iPedBSDebugWindow_SpookedFromAll, i)
					AND NOT IS_STRING_NULL_OR_EMPTY(GET_PED_SPOOK_REASON_FOR_DEBUG(iPedSpookReasonDebugWindow[i]))
						tl63_EntityName += " : "
						tl63_EntityName += GET_PED_SPOOK_REASON_FOR_DEBUG(iPedSpookReasonDebugWindow[i])
					ENDIF
					
					IF FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedSpookedBitset, i)
						tl63_EntityName += " : "
						IF iPedSpookOrderDebugWindow[i] > 0
							tl63_EntityName += iPedSpookOrderDebugWindow[i]
						ELSE
							tl63_EntityName += "X"
						ENDIF
						
						tl63_EntityName += " SP"
					ENDIF
					
					IF FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedAggroedBitset, i)
						tl63_EntityName += " : "
						IF iPedAggroOrderDebugWindow[i] > 0
							tl63_EntityName += iPedAggroOrderDebugWindow[i]
						ELSE
							tl63_EntityName += "X"
						ENDIF
							
						tl63_EntityName += " AG"
					ENDIF					
					
					tl63_EntityName += " : "
					TEXT_LABEL_15 tl15EntityState
					tl15EntityState = GET_PED_STATE_NAME(MC_serverBD_2.iPedState[i])
					sEntityState = TEXT_LABEL_TO_STRING(tl15EntityState)
					
					tl63_EntityName += sEntityState	
			
					IF MC_serverBD_2.iPedState[i] = ciTASK_GOTO_COORDS
						tl63_EntityName += " : "
						tl63_EntityName += MC_serverBD_2.iPedGotoProgress[i]
					ENDIF
				ENDIF
			BREAK
			CASE 1
				tl63_EntityName += " : "
				tl63_EntityName += FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn)
				
				tl63_EntityName += " : "
				tl63_EntityName += MC_serverBD_2.iCurrentPedRespawnLives[i]
				
				IF NOT IS_PED_INJURED(tempEntity)
					INT iHealth
					FLOAT fHealthCurrent
					fHealthCurrent = TO_FLOAT(GET_ENTITY_HEALTH(tempEntity))
					FLOAT fHealthMax
					fHealthMax = TO_FLOAT(GET_ENTITY_MAX_HEALTH(tempEntity))
					FLOAT fPercent
					fPercent = (fHealthCurrent / fHealthMax) * 100
					iHealth = ROUND(fPercent)
					
					tl63_EntityName += " : "
					tl63_EntityName += iHealth
					tl63_EntityName += "%"
				ENDIF
			BREAK
			CASE 2
				tl63_EntityName += " : "
				tl63_EntityName += "(NetID "
				IF DOES_ENTITY_EXIST(tempEntity)
				AND NETWORK_DOES_NETWORK_ID_EXIST(netEntity)
					tl63_EntityName += NETWORK_ENTITY_GET_OBJECT_ID(tempEntity)
				ELSE
					tl63_EntityName += "--"
				ENDIF
				tl63_EntityName += ")"
				
				IF NETWORK_DOES_NETWORK_ID_EXIST(netEntity)
					tl63_EntityName += " : "
					IF NETWORK_HAS_CONTROL_OF_ENTITY(tempEntity)
						tl63_EntityName += "Locally Owned"
					ELSE
						tl63_EntityName += "Remotely Owned"
					ENDIF
				ENDIF
			BREAK
			CASE 3
				
				IF IS_PED_IN_ANY_VEHICLE(tempEntity)
				
					tl63_EntityName += " + In Veh "
					
					VEHICLE_INDEX tempVeh
					INT iVeh 
					
					tempVeh = GET_VEHICLE_PED_IS_IN(tempEntity)
					iVeh = IS_ENTITY_A_MISSION_CREATOR_ENTITY(tempVeh)
					
					IF iVeh > -1
						tl63_EntityName += iVeh					
						tl63_EntityName += " : "
						tl63_EntityName += "(NetID "
						IF DOES_ENTITY_EXIST(tempVeh)
						AND NETWORK_DOES_NETWORK_ID_EXIST(NETWORK_GET_NETWORK_ID_FROM_ENTITY(tempVeh))
							tl63_EntityName += NETWORK_ENTITY_GET_OBJECT_ID(tempVeh)
							tl63_EntityName += ")"
						ELSE
							tl63_EntityName += "--"
						ENDIF
					ELSE
						tl63_EntityName += "(non-Mission)"
					ENDIF
				ENDIF				
			BREAK
		ENDSWITCH
		
		DRAW_ENTITY_DEBUG_WINDOW_TEXT(tl63_EntityName, fEntityNamePosX, fEntityNamePosY, fTextSize)
			
	ENDFOR

ENDPROC

PROC PROCESS_OVERVIEW_STATE_DEBUG_WINDOW()
		
	IF NOT SET_UP_INITIAL_CONTENT_OVERVIEW_DEBUG_WINDOW_DIMENSIONS()
		EXIT
	ENDIF
		
	TEXT_LABEL_63 tl63_Name
	
	sContentOverviewDebug.iDebugWindowPageMax[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 4

	IF NOT IS_BIT_SET(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_HideLegend)	
				
		DRAW_COMMON_MISSION_CONTROLLER_DEBUG_STATE_LEGEND()
				
		sContentOverviewDebug.iLegendPrintX = 0		
		
		DRAW_LOCAL_PLAYER_INTERIOR_INFO_DEBUG_STATE_LEGEND(PLAYER_PED_ID())	
		
		sContentOverviewDebug.iLegendPrintY = 0
		sContentOverviewDebug.iLegendPrintX = 1
	
		tl63_Name = "Number of Placed Peds: "
		tl63_Name += g_FMMC_STRUCT_ENTITIES.iNumberOfPeds
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)
		
		tl63_Name = "Number of Placed Vehicles: "
		tl63_Name += g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)
		
		tl63_Name = "Number of Placed Objects: "
		tl63_Name += g_FMMC_STRUCT_ENTITIES.iNumberOfObjects
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)
		
		tl63_Name = "Number of Placed Interactables: "
		tl63_Name += g_FMMC_STRUCT_ENTITIES.iNumberOfInteractables
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)
		
		tl63_Name = "Number of Placed Dyno Prop: "
		tl63_Name += g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)		
			
		tl63_Name = "Number of Placed Prop: "
		tl63_Name += g_FMMC_STRUCT_ENTITIES.iNumberOfProps
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)		
		
		tl63_Name = "Number of Placed Zones: "
		tl63_Name += g_FMMC_STRUCT_ENTITIES.iNumberOfZones
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)
		
		tl63_Name = "Number of Placed Locations: "
		tl63_Name += g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0]
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 1, 0)
		sContentOverviewDebug.iLegendPrintY = 0		
			
		tl63_Name = "Number of Placed Dialogue Triggers: "
		tl63_Name += g_FMMC_STRUCT.iNumberOfDialogueTriggers
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 1, 0)		
		sContentOverviewDebug.iLegendPrintY = 0
		
		tl63_Name = "~bold~Local Participant Index: "
		tl63_Name += iLocalPart
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)
		
		tl63_Name = "~bold~Local Player Name: "
		tl63_Name += GET_PLAYER_NAME(PLAYER_ID())
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)
		
		tl63_Name = "~bold~Local Player Team: "
		tl63_Name += MC_PlayerBD[iLocalPart].iTeam
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.03, FALSE, 0, 1)
	ENDIF
	
	// Main Draw Loop -------------------------
	STRING sTemp = ""
	INT iTeam, iPlayer
	INT i = 0
	FOR i = 0 TO GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow) * GET_NUMBER_OF_COLUMNS_FOR_ITEMS(sContentOverviewDebug.eDebugWindow, sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)])
		
		sContentOverviewDebug.fBaseTextSize = 0.225		
		tl63_Name = ""
		sTemp = ""
		FLOAT fTextSize = sContentOverviewDebug.fBaseTextSize*1.1
		FLOAT fEntityListColumnSpacingBase = (sContentOverviewDebug.fRectSizeX / GET_NUMBER_OF_COLUMNS_FOR_ITEMS(sContentOverviewDebug.eDebugWindow, sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]))
		FLOAT fEntityListColumnSpacing = (fEntityListColumnSpacingBase * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow)))
		FLOAT fEntityListStartPosX = sContentOverviewDebug.fTextPosX
		FLOAT fEntityListStartPosY = sContentOverviewDebug.fTextPosY + 0.02
		
		FLOAT fEntityNamePosX = fEntityListStartPosX + fEntityListColumnSpacing
		FLOAT fEntityNamePosY = fEntityListStartPosY + (sContentOverviewDebug.fYSpacing * i) -  (sContentOverviewDebug.fYSpacing * (GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow) * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow))))
	
		INT iCurrentRow
		iCurrentRow = 0
		
		
		// Could make an iSpacing variable that's only incremented when we have a non empty string for a draw call. for now leaving as is.
		
		SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
			CASE 0
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "~bold~Mission Ambient Info"
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Mission Name: "
					tl63_Name += g_FMMC_STRUCT.tl63MissionName
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Mission Description: "
					tl63_Name += g_FMMC_STRUCT.tl63MissionDecription[0]
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = g_FMMC_STRUCT.tl63MissionDecription[1]
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = g_FMMC_STRUCT.tl63MissionDecription[2]
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = g_FMMC_STRUCT.tl63MissionDecription[3]
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = g_FMMC_STRUCT.tl63MissionDecription[4]
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = g_FMMC_STRUCT.tl63MissionDecription[5]
				ENDIF				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = g_FMMC_STRUCT.tl63MissionDecription[6]
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = g_FMMC_STRUCT.tl63MissionDecription[7]
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "FMMC_MS_W0_"
					tl63_Name += MC_serverBD.iWeather
					sTemp = GET_FILENAME_FOR_AUDIO_CONVERSATION(tl63_Name)
					tl63_Name = "Weather: "
					tl63_Name += sTemp	
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "LOB_TIME_DAY_"
					tl63_Name += MC_serverBD.iTimeOfDay
					sTemp = GET_FILENAME_FOR_AUDIO_CONVERSATION(tl63_Name)
					tl63_Name = "Time Of Day: "
					tl63_Name += sTemp	
					
					INT iSecs, iMins, iHrs
					iSecs = 0
					iMins = 0
					iHrs = 0
					NETWORK_GET_GLOBAL_CLOCK(iHrs, iMins, iSecs)					
					tl63_Name += " ("
					tl63_Name += iHrs
					tl63_Name += ":"
					tl63_Name += iMins
					tl63_Name += ":"
					tl63_Name += iSecs		
					tl63_Name += ")"
				ENDIF				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Music Playing: "
					IF IS_BIT_SET(iLocalBoolCheck3, LBOOL3_MUSIC_PLAYING)
						tl63_Name += "Yes"
					ENDIF
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "FMMC_MUST_"
					tl63_Name += g_FMMC_STRUCT.iAudioScore
					sTemp = GET_FILENAME_FOR_AUDIO_CONVERSATION(tl63_Name)
					tl63_Name = "Audio Score: "
					tl63_Name += sTemp	
					tl63_Name += " ("
					tl63_Name += g_FMMC_STRUCT.iAudioScore
					tl63_Name += ")"
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Music Mood: "
					sTemp = GET_MOOD_NAME_FOR_SONG(g_FMMC_STRUCT.iAudioScore, MC_PlayerBD[iLocalPart].iMusicState)
					tl63_Name += sTemp	
					tl63_Name += " ("
					tl63_Name += MC_PlayerBD[iLocalPart].iMusicState
					tl63_Name += ")"
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Alt Vars Config: "
					tl63_Name = "MC_AVA_CON_"
					tl63_Name += ENUM_TO_INT(g_FMMC_STRUCT.sPoolVars.eConfig)
					sTemp = GET_FILENAME_FOR_AUDIO_CONVERSATION(tl63_Name)	
					tl63_Name = "Alt Vars Config: "
					tl63_Name += sTemp
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "~bold~Interior Info"
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Inside Interior: "
					IF IS_VALID_INTERIOR(GET_INTERIOR_FROM_ENTITY(localPlayerPed))
						tl63_Name += "Yes"
					ELSE
						tl63_Name += "No"
					ENDIF
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Interior Hash: "
					IF IS_VALID_INTERIOR(GET_INTERIOR_FROM_ENTITY(localPlayerPed))
						INT iLocalPlayerCurrentInteriorHash
						VECTOR vLocalPlayerCurrentInteriorPos
						iLocalPlayerCurrentInteriorHash = -1
						vLocalPlayerCurrentInteriorPos = <<0.0, 0.0, 0.0>>
						GET_INTERIOR_LOCATION_AND_NAMEHASH(GET_INTERIOR_FROM_ENTITY(localPlayerPed), vLocalPlayerCurrentInteriorPos, iLocalPlayerCurrentInteriorHash)
						IF NOT IS_VECTOR_ZERO(vLocalPlayerCurrentInteriorPos)
						AND iLocalPlayerCurrentInteriorHash != -1
							tl63_Name += iLocalPlayerCurrentInteriorHash
						ENDIF
					ELSE
						tl63_Name = "None"	
					ENDIF
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Interior Room Key: "
					IF IS_VALID_INTERIOR(GET_INTERIOR_FROM_ENTITY(localPlayerPed))
						tl63_Name += GET_ROOM_KEY_FROM_ENTITY(LocalPlayerPed)
					ELSE
						tl63_Name = "None"	
					ENDIF
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Figure out a way to present the actual name.
					
					// sTemp = HASH(iLocalPlayerCurrentInteriorHash)
					// tl63_Name = "Interior Name: "
					// tl63_Name += sTemp
					
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF			
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF					
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF					
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF					
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF
												
				
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "~bold~Cutscene Data"
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Cutscene ID Playing: "
					tl63_Name += MC_ServerBD.iCutsceneID[MC_playerBD[iLocalPart].iTeam]
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Cutscene ID Streamed: "
					tl63_Name += MC_ServerBD.iCutsceneStreamingIndex[MC_playerBD[iLocalPart].iTeam]
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Number Should Be Watching: "
					tl63_Name += GET_NUMBER_OF_PEOPLE_SHOULD_BE_WATCHING_CUTSCENE(MC_playerBD[iLocalPart].iTeam)
				ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Number Finished Watching: "
					tl63_Name += GET_NUMBER_OF_PEOPLE_FINISHED_CUTSCENE(MC_playerBD[iLocalPart].iTeam)
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Mocap Name: "
					tl63_Name += sMocapCutscene
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Mocap Streaming State: "
					tl63_Name += GET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS_NAME(eMocapRunningCutsceneProgress)
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Mocap Cutscene State: "
					tl63_Name += GET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS_NAME(eMocapManageCutsceneProgress)
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Mocap Has Cutscene Loaded: "
					tl63_Name += PICK_STRING(HAS_CUTSCENE_LOADED(), "TRUE", "FALSE")
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Scripted Cutscene State: "
					tl63_Name += GET_SCRIPTED_CUTSCENE_STATE_PROGRESS_NAME(eScriptedCutsceneProgress)
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Scripted Current Camera Shot: "
					tl63_Name += iCamShot
				ENDIF
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					// Gap
				ENDIF				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "FMMC_CUT"		
					tl63_Name += MC_ServerBD.iEndCutscene
					sTemp = GET_FILENAME_FOR_AUDIO_CONVERSATION(tl63_Name)	
					tl63_Name = "End Cutscene: "
					tl63_Name += sTemp
				ENDIF
			BREAK
			CASE 1							
				FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
					IF iTeam = 0
						IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
							tl63_Name = "~bold~Team Data and Stats (1)"
						ENDIF
					ELSE
						IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
							// Gap
						ENDIF
					ENDIF
					
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "~bold~Team: "
						tl63_Name += iTeam
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						// Gap
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "Rule: "
						tl63_Name += MC_ServerBD_4.iCurrentHighestPriority[iTeam]
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = ""
						IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
							tl63_Name += g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63Objective[MC_ServerBD_4.iCurrentHighestPriority[iTeam]]
						ELSE
							tl63_Name += "Invalid"
						ENDIF
					ENDIF
										
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "Rule Score: "
						tl63_Name += MC_ServerBD.iScoreOnThisRule[iTeam]
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "Rule Score Target: "						
						IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
						AND MC_serverBD_4.iCurrentHighestPriority[iTeam] > -1
							tl63_Name += g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTargetScore[MC_serverBD_4.iCurrentHighestPriority[iTeam]]
						ELSE
							tl63_Name += "N/A"
						ENDIF
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "Total Score: "
						tl63_Name += MC_ServerBD.iTeamScore[iTeam]
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "Total Target Score: "
						tl63_Name += g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMissionTargetScore
					ENDIF
										
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "Mid Point Reached: "
						IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], MC_serverBD_4.iCurrentHighestPriority[iTeam])
							tl63_Name += "TRUE"
						ELSE
							tl63_Name += "FALSE"
						ENDIF
					ENDIF
						
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)			
						tl63_Name = "Rule Timer: "
						
						IF iTeam > -1
						AND iTeam < FMMC_MAX_TEAMS
							IF MC_serverBD_4.iCurrentHighestPriority[iTeam] > -1
							AND MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
								IF GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveTimeLimitRule[MC_serverBD_4.iCurrentHighestPriority[iTeam]]) = 0
									tl63_Name += "Unlimited"
								ELIF HAS_NET_TIMER_STARTED(MC_serverBD_3.tdObjectiveLimitTimer[iteam])
									tl63_Name += GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdObjectiveLimitTimer[iteam])
									tl63_Name += " : "
									tl63_Name += GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveTimeLimitRule[MC_serverBD_4.iCurrentHighestPriority[iTeam]])
								ELSE
									tl63_Name += "Not Started"
									tl63_Name += " : "
									tl63_Name += GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveTimeLimitRule[MC_serverBD_4.iCurrentHighestPriority[iTeam]])
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "Multi Rule Timer: "
						
						IF iTeam > -1
						AND iTeam < FMMC_MAX_TEAMS
							IF MC_serverBD_4.iCurrentHighestPriority[iTeam] > -1
							AND MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
								IF GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTimeLimit[MC_serverBD_4.iCurrentHighestPriority[iTeam]]) = 0
									tl63_Name += "Unlimited"
								ELIF HAS_NET_TIMER_STARTED(MC_serverBD_3.tdMultiObjectiveLimitTimer[iteam])
									tl63_Name += GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[iteam])
									tl63_Name += ": "
									tl63_Name += GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTimeLimit[MC_serverBD_4.iCurrentHighestPriority[iTeam]])
								ELSE
									tl63_Name += "Not Started"
									tl63_Name += " : "
									tl63_Name += GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTimeLimit[MC_serverBD_4.iCurrentHighestPriority[iTeam]])
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "Number Starting Players: "
						tl63_Name += MC_ServerBD.iNumStartingPlayers[iTeam]
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "Number Playing Players: "
						tl63_Name += MC_ServerBD.iNumberOfPlayingPlayers[iTeam]
					ENDIF	
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						// Gap
					ENDIF			
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "Number Of Players Wanted: "
						tl63_Name += MC_ServerBD.iNumOfWanted[iTeam]
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)				
						tl63_Name = "Number Of Players Fake Wanted: "
						tl63_Name += MC_ServerBD.iNumOfFakeWanted[iTeam]						
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						//tl63_Name = "Highest Wanted Level: "
						//tl63_Name += MC_ServerBD.iNumOfHighestWanted[iTeam]
						// Gap, ^ was missing.
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						//tl63_Name = "Highest Fake Wanted Level: "
						//tl63_Name += MC_ServerBD.iNumOfHighestFakeWanted[iTeam]
						// Gap, ^ was missing.
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						// Gap
					ENDIF
				ENDFOR
			BREAK
			CASE 2					
				FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
					IF iTeam = 0
						IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
							tl63_Name = "~bold~Team Data and Stats (2)"
						ENDIF
					ELSE
						IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
							// Gap
						ENDIF
					ENDIF
					
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "~bold~Team: "
						tl63_Name += iTeam
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "iNumPlayerRuleHighestPriority: "
						tl63_Name += MC_serverBD.iNumPlayerRuleHighestPriority[iTeam]
					ENDIF	
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "Player Rule Type: "
						IF MC_serverBD.iNumPlayerRuleHighestPriority[iTeam] > 0
							tl63_Name += "needs porting"
							// tl63_Name += GET_MC_CLIENT_MISSION_STAGE_STRING_FOR_DEBUG(GET_PLAYER_CLIENT_MISSION_STAGE_FROM_MISSION_LOGIC(iTeam))
						ENDIF
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)						
						tl63_Name = "~r~iNumPedHighestPriority: "
						tl63_Name += MC_serverBD.iNumPedHighestPriority[iTeam]
					ENDIF	
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "~r~Ped Rule Type: "
						IF MC_serverBD.iNumPedHighestPriority[iTeam] > 0
							tl63_Name += "needs porting"
							//tl63_Name += GET_MC_CLIENT_MISSION_STAGE_STRING_FOR_DEBUG(GET_PED_CLIENT_MISSION_STAGE_FROM_MISSION_LOGIC(iTeam))
						ENDIF
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "~b~iNumVehHighestPriority: "
						tl63_Name += MC_serverBD.iNumVehHighestPriority[iTeam]
					ENDIF	
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "~b~Veh Rule Type: "
						IF MC_serverBD.iNumVehHighestPriority[iTeam] > 0
							tl63_Name += "needs porting"
							//tl63_Name += GET_MC_CLIENT_MISSION_STAGE_STRING_FOR_DEBUG(GET_VEH_CLIENT_MISSION_STAGE_FROM_MISSION_LOGIC(iTeam))
						ENDIF
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "~g~iNumObjHighestPriority: "
						tl63_Name += MC_serverBD.iNumObjHighestPriority[iTeam]
					ENDIF	
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "~g~Obj Rule Type: "
						IF MC_serverBD.iNumObjHighestPriority[iTeam] > 0
							tl63_Name += "needs porting"
							//tl63_Name += GET_MC_CLIENT_MISSION_STAGE_STRING_FOR_DEBUG(GET_OBJ_CLIENT_MISSION_STAGE_FROM_MISSION_LOGIC(iTeam))
						ENDIF
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "~y~iNumLocHighestPriority: "
						tl63_Name += MC_serverBD.iNumLocHighestPriority[iTeam]
					ENDIF	
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)						
						tl63_Name = "~y~iNumberOfTeamInAllAreas: "
						tl63_Name += MC_serverBD_1.iNumberOfTeamInAllAreas[iTeam]
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "~y~Loc Rule Type: "
						IF MC_serverBD.iNumLocHighestPriority[iTeam] > 0
							tl63_Name += "needs porting"
							//tl63_Name += GET_MC_CLIENT_MISSION_STAGE_STRING_FOR_DEBUG(GET_LOC_CLIENT_MISSION_STAGE_FROM_MISSION_LOGIC(iTeam))
						ENDIF
					ENDIF
					
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						// Gap
					ENDIF					
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						// Gap
					ENDIF					
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						// Gap
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						// Gap
					ENDIF	
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						// Gap
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						// Gap
					ENDIF	
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						// Gap
					ENDIF
				ENDFOR
			BREAK
			CASE 3
				FOR iPlayer = 0 TO 3 // "Mission Players" max is 4.
					INT iPart
					iPart = GET_ACTIVE_PARTICIPANT_INDEX_FROM_NUMBER(iPlayer)
					IF iPlayer = 0
						IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
							tl63_Name = "~bold~Mission Player Data and Stats: "
						ENDIF
					ELSE
						IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
							// Gap
						ENDIF
					ENDIF
								
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "~bold~Player "
						tl63_Name += iPlayer						
					ENDIF					
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						tl63_Name = "~bold~Participant: "
						tl63_Name += iPart
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						IF iPart > -1
						AND iPart < NUM_NETWORK_PLAYERS
							tl63_Name = "~bold~Name: "
							tl63_Name += MC_ServerBD_2.tParticipantNames[iPart]
						ENDIF
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)					
						IF iPart > -1
						AND iPart < NUM_NETWORK_PLAYERS
							tl63_Name = ""
							tl63_Name += GET_GAME_STATE_STRING(MC_playerBD[iPart].iGameState)
						ENDIF
					ENDIF		
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						IF iPart > -1
						AND iPart < NUM_NETWORK_PLAYERS
							tl63_Name = ""
							tl63_Name += "needs porting"
							//tl63_Name += GET_MC_CLIENT_MISSION_STAGE_STRING_FOR_DEBUG(GET_MC_CLIENT_MISSION_STAGE(iPart))
						ENDIF
					ENDIF										
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						IF iPart > -1
						AND iPart < NUM_NETWORK_PLAYERS
							tl63_Name = "Score: "
							tl63_Name += MC_ServerBD.iPlayerScore[iPart]
						ENDIF
					ENDIF
						
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						IF iPart > -1
						AND iPart < NUM_NETWORK_PLAYERS
							tl63_Name = "iCurrentLoc: "
							tl63_Name += MC_PlayerBD[iPart].iCurrentLoc
						ENDIF
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						IF iPart > -1
						AND iPart < NUM_NETWORK_PLAYERS
							tl63_Name = "iInAnyLoc: "
							tl63_Name += MC_PlayerBD[iPart].iInAnyLoc
						ENDIF
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						IF iPart > -1
						AND iPart < NUM_NETWORK_PLAYERS
							tl63_Name = "iLeaveLoc: "
							tl63_Name += MC_PlayerBD[iPart].iLeaveLoc
						ENDIF
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						IF iPart > -1
						AND iPart < NUM_NETWORK_PLAYERS
							tl63_Name = "iPedNear: "
							tl63_Name += MC_PlayerBD[iPart].iPedNear
						ENDIF
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						IF iPart > -1
						AND iPart < NUM_NETWORK_PLAYERS
							tl63_Name = "iVehNear: "
							tl63_Name += MC_PlayerBD[iPart].iVehNear
						ENDIF
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						IF iPart > -1
						AND iPart < NUM_NETWORK_PLAYERS
							tl63_Name = "iObjNear: "
							tl63_Name += MC_PlayerBD[iPart].iObjNear
						ENDIF
					ENDIF
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						IF iPart > -1
						AND iPart < NUM_NETWORK_PLAYERS
							tl63_Name = "iObjHacking: "
							tl63_Name += MC_PlayerBD[iPart].iObjHacking
						ENDIF
					ENDIF
				
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						IF iPart > -1
						AND iPart < NUM_NETWORK_PLAYERS
							tl63_Name = "Started Cut: "
							tl63_Name += PICK_STRING(IS_BIT_SET(MC_PlayerBD[iPart].iClientBitset2, PBBOOL2_STARTED_CUTSCENE), "Yes", "No")
							tl63_Name += "  :  Finished Cut: "
							tl63_Name += PICK_STRING(IS_BIT_SET(MC_PlayerBD[iPart].iClientBitset2, PBBOOL2_FINISHED_CUTSCENE), "Yes", "No")
						ENDIF
					ENDIF
					
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						// Gap
					ENDIF
					
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						// Gap
					ENDIF
					
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						// Gap
					ENDIF
					
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						// Gap
					ENDIF
					
					IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
						// Gap
					ENDIF
				ENDFOR
			BREAK
		ENDSWITCH
		
		IF NOT IS_STRING_NULL_OR_EMPTY(tl63_Name)
			DRAW_RECT(fEntityNamePosX, fEntityNamePosY+sContentOverviewDebug.fBringRectInLineWithText, sContentOverviewDebug.fStatusRectX/2, sContentOverviewDebug.fStatusRectY/2, 255, 255, 255, 175)				
		ENDIF
		
		DRAW_ENTITY_DEBUG_WINDOW_TEXT(tl63_Name, fEntityNamePosX, fEntityNamePosY, fTextSize)		
	ENDFOR

ENDPROC

PROC PROCESS_CONTENT_OVERVIEW_DEBUG_WINDOW_MAIN_FOR_SPECIFIC_CONTENT()

	SET_DEFAULT_DEBUG_WINDOW_FOR_SCRIPT(CONTENT_OVERVIEW_DEBUG_WINDOW_MISSION_OVERVIEW)

	// MISSION CONTROLLER LEGACY SPECIFIC ----
	SWITCH sContentOverviewDebug.eDebugWindow
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_MISSION_OVERVIEW
			PROCESS_OVERVIEW_STATE_DEBUG_WINDOW()
		BREAK		
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_PEDS
			PROCESS_PED_STATE_DEBUG_WINDOW()
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_VEHICLES
			PROCESS_VEHICLE_STATE_DEBUG_WINDOW()
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_OBJECTS
			PROCESS_OBJECTS_STATE_DEBUG_WINDOW()
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DYNOPROP
			PROCESS_DYNOPROP_STATE_DEBUG_WINDOW()
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_PROP
			PROCESS_PROP_STATE_DEBUG_WINDOW()
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_ZONES
			PROCESS_ZONE_STATE_DEBUG_WINDOW()
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_LOCATIONS
			PROCESS_LOCATION_STATE_DEBUG_WINDOW()
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DIALOGUE_TRIGGER
			PROCESS_DIALOGUE_TRIGGER_STATE_DEBUG_WINDOW()
		BREAK		
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_TEAM_SPAWN_POINTS
			PROCESS_TEAM_SPAWN_POINTS_STATE_DEBUG_WINDOW()
		BREAK
	ENDSWITCH
	// MISSION CONTROLLER LEGACY SPECIFIC ----
ENDPROC

PROC PROCESS_CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT()
			
	SWITCH sContentOverviewDebug.eEntityStateClickEvent
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_CHANGE_TAB
			sContentOverviewDebug.eDebugWindow = sContentOverviewDebug.eDebugWindowTypeSelected			
			CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()
		BREAK
		
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_INDEX
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_INDEX
			
			FLOAT fHead
			VECTOR vWarp
			VECTOR vWarpOffset
			NETWORK_INDEX niEnt
			ENTITY_INDEX entID
			
			SWITCH sContentOverviewDebug.eDebugWindowTypeSelected			
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_PEDS
					niEnt = MC_serverBD_1.sFMMC_SBD.niPed[sContentOverviewDebug.iEntityIndexSelected]
					IF NETWORK_DOES_NETWORK_ID_EXIST(niEnt)
						entID = NET_TO_ENT(niEnt)
						IF DOES_ENTITY_EXIST(entID)
							vWarp = GET_ENTITY_COORDS(entID)
							fHead = GET_ENTITY_HEADING(entID)-180
							vWarpOffset = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vWarp, fHead, <<2.0, 0.0, 0.0>>)
						ENDIF
					ENDIF
				BREAK
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_VEHICLES
					niEnt = MC_serverBD_1.sFMMC_SBD.niVehicle[sContentOverviewDebug.iEntityIndexSelected]
					IF NETWORK_DOES_NETWORK_ID_EXIST(niEnt)
						entID = NET_TO_ENT(niEnt)
						IF DOES_ENTITY_EXIST(entID)
							vWarp = GET_ENTITY_COORDS(entID)
							fHead = GET_ENTITY_HEADING(entID)-180
							vWarpOffset = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vWarp, fHead, <<2.0, 0.0, 0.0>>)
						ENDIF
					ENDIF
				BREAK
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_OBJECTS
					niEnt = MC_serverBD_1.sFMMC_SBD.niObject[sContentOverviewDebug.iEntityIndexSelected]
					IF NETWORK_DOES_NETWORK_ID_EXIST(niEnt)
						entID = NET_TO_ENT(niEnt)
						IF DOES_ENTITY_EXIST(entID)
							vWarp = GET_ENTITY_COORDS(entID)
							fHead = GET_ENTITY_HEADING(entID)-180
							vWarpOffset = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vWarp, fHead, <<2.0, 0.0, 0.0>>)
						ENDIF
					ENDIF
				BREAK
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DYNOPROP
					niEnt = MC_serverBD_1.sFMMC_SBD.niDynoProps[sContentOverviewDebug.iEntityIndexSelected]
					IF NETWORK_DOES_NETWORK_ID_EXIST(niEnt)
						entID = NET_TO_ENT(niEnt)
						IF DOES_ENTITY_EXIST(entID)
							vWarp = GET_ENTITY_COORDS(entID)
							fHead = GET_ENTITY_HEADING(entID)-180
							vWarpOffset = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vWarp, fHead, <<2.0, 0.0, 0.0>>)
						ENDIF
					ENDIF
				BREAK
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_PROP
					entID = oiProps[sContentOverviewDebug.iEntityIndexSelected]
					IF DOES_ENTITY_EXIST(entID)
						vWarp = GET_ENTITY_COORDS(entID)
						fHead = GET_ENTITY_HEADING(entID)-180
						vWarpOffset = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vWarp, fHead, <<0.0, 0.0, 2.0>>)
					ENDIF
				BREAK	
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_ZONES
					vWarp = g_FMMC_STRUCT_ENTITIES.sPlacedZones[sContentOverviewDebug.iEntityIndexSelected].vPos[0] // GET_ZONE_RUNTIME_CENTRE(sContentOverviewDebug.iEntityIndexSelected)
					fHead = 0.0
				BREAK
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_LOCATIONS
					vWarp = GET_LOCATION_VECTOR(sContentOverviewDebug.iEntityIndexSelected)
					fHead = 0.0
				BREAK
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DIALOGUE_TRIGGER
					vWarp = g_FMMC_STRUCT.sDialogueTriggers[sContentOverviewDebug.iEntityIndexSelected].vPosition
					fHead = 0.0
				BREAK				
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_TEAM_SPAWN_POINTS
					vWarp = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sContentOverviewDebug.iTeamIndexSelected][sContentOverviewDebug.iEntityIndexSelected].vPos
					fHead = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sContentOverviewDebug.iTeamIndexSelected][sContentOverviewDebug.iEntityIndexSelected].fHead
				BREAK
			ENDSWITCH
			
			IF IS_VECTOR_ZERO(vWarp)
				CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()
				EXIT
			ENDIF
			
			IF sContentOverviewDebug.eEntityStateClickEvent = CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_TO_INDEX	
				IF NOT IS_VECTOR_ZERO(vWarpOffset)
					SET_ENTITY_COORDS(PLAYER_PED_ID(), vWarpOffset)
				ELSE
					SET_ENTITY_COORDS(PLAYER_PED_ID(), vWarp)
				ENDIF
				SET_ENTITY_HEADING(PLAYER_PED_ID(), fHead)
				
			ELIF sContentOverviewDebug.eEntityStateClickEvent = CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_WARP_DBG_CAM_TO_INDEX				
				
				IF DOES_CAM_EXIST(sContentOverviewDebug.ciEntityStateCamera)
					DESTROY_CAM(sContentOverviewDebug.ciEntityStateCamera)
				ENDIF
			
				VECTOR vCamPos, vCamPosOffset
				INT iMinOffset, iMaxOffset
				INT iRand
				iRand = GET_RANDOM_INT_IN_RANGE(0, 4)
				
				IF sContentOverviewDebug.eDebugWindowTypeSelected	!= CONTENT_OVERVIEW_DEBUG_WINDOW_VEHICLES
					iMinOffset = 2
					iMaxOffset = 5
				ELSE
					iMinOffset = 5
					iMaxOffset = 8
				ENDIF
				
				SWITCH iRand
					CASE 0
						vCamPosOffset = <<-TO_FLOAT(GET_RANDOM_INT_IN_RANGE(iMinOffset, iMaxOffset)), -TO_FLOAT(GET_RANDOM_INT_IN_RANGE(iMinOffset, iMaxOffset)), TO_FLOAT(GET_RANDOM_INT_IN_RANGE(1, 3))>>
					BREAK
					CASE 1
						vCamPosOffset = <<TO_FLOAT(GET_RANDOM_INT_IN_RANGE(iMinOffset, iMaxOffset)), TO_FLOAT(GET_RANDOM_INT_IN_RANGE(iMinOffset, iMaxOffset)), TO_FLOAT(GET_RANDOM_INT_IN_RANGE(1, 3))>>
					BREAK
					CASE 2
						vCamPosOffset = <<-TO_FLOAT(GET_RANDOM_INT_IN_RANGE(iMinOffset, iMaxOffset)), TO_FLOAT(GET_RANDOM_INT_IN_RANGE(iMinOffset, iMaxOffset)), TO_FLOAT(GET_RANDOM_INT_IN_RANGE(1, 3))>>
					BREAK
					CASE 3
						vCamPosOffset = <<TO_FLOAT(GET_RANDOM_INT_IN_RANGE(iMinOffset, iMaxOffset)), -TO_FLOAT(GET_RANDOM_INT_IN_RANGE(iMinOffset, iMaxOffset)), TO_FLOAT(GET_RANDOM_INT_IN_RANGE(1, 3))>>
					BREAK
				ENDSWITCH
				
				vCamPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vWarp, fHead, vCamPosOffset)
					
				sContentOverviewDebug.ciEntityStateCamera = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vCamPos, <<0.0, 0.0, 0.0>>, 70.0)				
				
				SET_CAM_ACTIVE(sContentOverviewDebug.ciEntityStateCamera, TRUE)
				
				RENDER_SCRIPT_CAMS(TRUE, FALSE, 1500, DEFAULT, TRUE)
				
				SET_CAM_FOV(sContentOverviewDebug.ciEntityStateCamera, 70.0)
							
				IF DOES_ENTITY_EXIST(entID)
					POINT_CAM_AT_ENTITY(sContentOverviewDebug.ciEntityStateCamera, entID, <<0.0, 0.0, 0.0>>)					
				ENDIF
			ENDIF
			
			CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()
		BREAK
		
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_PLAY_OR_TRIGGER
			SWITCH sContentOverviewDebug.eDebugWindowTypeSelected				
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_PEDS
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_VEHICLES
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_OBJECTS
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DYNOPROP
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_PROP									
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_ZONES
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_LOCATIONS
					
				BREAK
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DIALOGUE_TRIGGER
					FMMC_SET_LONG_BIT(iLocalDialogueForcePlay, sContentOverviewDebug.iEntityIndexSelected)
				BREAK	
			ENDSWITCH
			CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()
		BREAK
		
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_RESET		
			SWITCH sContentOverviewDebug.eDebugWindowTypeSelected				
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_PEDS
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_VEHICLES
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_OBJECTS
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DYNOPROP
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_PROP									
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_ZONES
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_LOCATIONS
					
				BREAK
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DIALOGUE_TRIGGER
					
					FMMC_CLEAR_LONG_BIT(iLocalDialoguePending, sContentOverviewDebug.iEntityIndexSelected)
					FMMC_CLEAR_LONG_BIT(iLocalDialoguePlayedBS, sContentOverviewDebug.iEntityIndexSelected) 					
					FMMC_CLEAR_LONG_BIT(iLocalDialoguePlayingBS, sContentOverviewDebug.iEntityIndexSelected)
					FMMC_CLEAR_LONG_BIT(iDialogueKilledBS, sContentOverviewDebug.iEntityIndexSelected)
					FMMC_CLEAR_LONG_BIT(iDialogueVehicleOwnedBS, sContentOverviewDebug.iEntityIndexSelected)
					FMMC_CLEAR_LONG_BIT(iDialogueVehicleNotOwnedBS, sContentOverviewDebug.iEntityIndexSelected)
					FMMC_CLEAR_LONG_BIT(iDialogueProgressSFX1Played, sContentOverviewDebug.iEntityIndexSelected)
					FMMC_CLEAR_LONG_BIT(iDialogueProgressSFX2Played, sContentOverviewDebug.iEntityIndexSelected)
					FMMC_CLEAR_LONG_BIT(iLocalDialogueBlockedBS, sContentOverviewDebug.iEntityIndexSelected)					
					FMMC_CLEAR_LONG_BIT(iLocalDialogueForcePlay, sContentOverviewDebug.iEntityIndexSelected)
					
					RESET_NET_TIMER(tdDialogueTimer[sContentOverviewDebug.iEntityIndexSelected])
					CLEAR_BIT(MC_playerBD[iLocalPart].iDialoguePlayedBS[sContentOverviewDebug.iEntityIndexSelected/32],sContentOverviewDebug.iEntityIndexSelected%32)
					
				BREAK				
			ENDSWITCH
			CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_HIDE_LEGEND			
			IF NOT IS_BIT_SET(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_HideLegend)
				SET_BIT(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_HideLegend)
			ELSE
				CLEAR_BIT(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_HideLegend)
			ENDIF
			CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_DRAW_MARKER_DEBUG
			
			IF sContentOverviewDebug.iEntityIndexSelected <= -1
				CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()
				EXIT
			ENDIF
			
			SWITCH sContentOverviewDebug.eDebugWindowTypeSelected
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_PEDS
					
					IF sContentOverviewDebug.iEntityIndexSelected >= FMMC_MAX_PEDS
						CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()
						EXIT
					ENDIF
					
					IF NOT FMMC_IS_LONG_BIT_SET(sContentOverviewDebug.iEntityStateDrawPedDebugBS, sContentOverviewDebug.iEntityIndexSelected)
						FMMC_SET_LONG_BIT(sContentOverviewDebug.iEntityStateDrawPedDebugBS, sContentOverviewDebug.iEntityIndexSelected)
					ELSE
						FMMC_CLEAR_LONG_BIT(sContentOverviewDebug.iEntityStateDrawPedDebugBS, sContentOverviewDebug.iEntityIndexSelected)	
					ENDIF
					
				BREAK
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_VEHICLES
				BREAK
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_OBJECTS
				BREAK			
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_INTERACTABLE
				BREAK
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DYNOPROP
				BREAK
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_PROP
				BREAK	
				
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_ZONES
					
					IF sContentOverviewDebug.iEntityIndexSelected >= FMMC_MAX_NUM_ZONES
						CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()
						EXIT
					ENDIF
					
					IF NOT FMMC_IS_LONG_BIT_SET(sContentOverviewDebug.iEntityStateDrawZoneDebugBS, sContentOverviewDebug.iEntityIndexSelected)
						FMMC_SET_LONG_BIT(sContentOverviewDebug.iEntityStateDrawZoneDebugBS, sContentOverviewDebug.iEntityIndexSelected)
					ELSE
						FMMC_CLEAR_LONG_BIT(sContentOverviewDebug.iEntityStateDrawZoneDebugBS, sContentOverviewDebug.iEntityIndexSelected)	
					ENDIF
					
				BREAK
				
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_LOCATIONS
				BREAK
				CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DIALOGUE_TRIGGER
				BREAK		
			ENDSWITCH
			
			CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()
		BREAK
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT_ENABLE_ALL_WINDOW_DEBUG							
			CONTENT_OVERVIEW_DEBUG_WINDOW_CLEAR_CLICK_EVENT()
		BREAK
	ENDSWITCH
			
ENDPROC

#ENDIF


