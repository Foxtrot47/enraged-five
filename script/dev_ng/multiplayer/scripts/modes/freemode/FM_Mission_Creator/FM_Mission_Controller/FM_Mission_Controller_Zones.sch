
USING "FM_Mission_Controller_USING.sch"
USING "FM_Mission_Controller_Utility.sch"
USING "FM_Mission_Controller_Entities.sch"
USING "FM_Mission_Controller_ObjectiveCompletion.sch"
USING "FM_Mission_Controller_SpecialCase.sch"
USING "FM_Mission_Controller_Rules.sch"
USING "FM_Mission_Controller_Doors.sch"
USING "FM_Mission_Controller_Cutscene.sch"
USING "FM_Mission_Controller_Debug.sch"
USING "FM_Mission_Controller_Locations.sch"
USING "FM_Mission_Controller_Vehicles.sch"
USING "FM_Mission_Controller_Peds_Brain.sch"
USING "FM_Mission_Controller_Peds_Body.sch"
USING "FM_Mission_Controller_Peds.sch"
USING "FM_Mission_Controller_GangChase.sch"
USING "FM_Mission_Controller_Minigame.sch"
USING "FM_Mission_Controller_Objects.sch"
USING "FM_Mission_Controller_Props.sch"
USING "FM_Mission_Controller_Audio.sch"
USING "FM_Mission_Controller_HUD.sch"
USING "FM_Mission_Controller_Players.sch"



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: RESTRICTION ZONES !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************

#IF IS_DEBUG_BUILD
PROC DRAW_ZONE_DEBUG(INT iZone)
	
	IF NOT GET_COMMANDLINE_PARAM_EXISTS("zoneDebug")
		EXIT
	ENDIF
	
	BOOL bDrawThisZone = IS_BIT_SET(iLocalZoneCheckBitset, iZone)
	
	IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD4)
		// Force to draw
		bDrawThisZone = TRUE
	ENDIF
	
	IF bDrawThisZone
		TEXT_LABEL_63 tlDebugText3D = "Z"
		tlDebugText3D += iZone
		
		INT iR = 255, iG = 255, iB = 255, iA = 180
		
		tlDebugText3D += " / Type: "
		tlDebugText3D += GET_FMMC_ZONE_TYPE(iZone)
		
		IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
			DRAW_FMMC_ZONE(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone], GET_FMMC_ZONE_PLACED_CENTRE(iZone), GET_FMMC_ZONE_PLACED_CENTRE(iZone), g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fRadius, iA * 0.2)
			DRAW_DEBUG_TEXT(tlDebugText3D, GET_FMMC_ZONE_PLACED_CENTRE(iZone) + <<0,0,-2>>, iR, iG, iB, iA)
		ENDIF
	ENDIF
ENDPROC
#ENDIF

FUNC INT MAX_ZONE_COUNT_TO_LOOP_TO()
	IF g_bForceLegacyMissionControllerMaxZoneCount
		RETURN FMMC_MAX_NUM_ZONES
	ENDIF
	
	RETURN g_FMMC_STRUCT_ENTITIES.iNumberOfZones
ENDFUNC

FUNC BOOL IS_PED_IN_RESTRICTION_ZONE(PED_INDEX tempPed, INT iZone, BOOL b3DCheck = TRUE, BOOL bCheckZ = FALSE, FLOAT fZoneGrowthPassed = -1.0)
		
	IF IS_BIT_SET(iZoneBS_ActivatedRemotely, iZone)
		RETURN TRUE
	ENDIF
		
	IF NOT IS_PED_INJURED(tempPed)
		RETURN IS_LOCATION_IN_FMMC_ZONE(GET_ENTITY_COORDS(tempPed), iZone, b3DCheck,FALSE, bCheckZ, fZoneGrowthPassed)
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_BLOCK_JUMP_INPUT_WHEN_IN_BLOCK_CLIMBING_ZONE()
	
	IF IS_JOB_IN_CASINO()
	AND g_FMMC_STRUCT.iAdversaryModeType != 0
		// In the Casino adversary modes, just block climbing using the flag instead
		RETURN FALSE
	ENDIF
	
	// Old content always just blocked the jump input
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_PROCESS_BLOCK_CLIMBING_ZONES_EVERY_FRAME()

	IF NOT SHOULD_BLOCK_JUMP_INPUT_WHEN_IN_BLOCK_CLIMBING_ZONE()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_RESTRICTION_ROCKET_POS_VALID_FOR_ZONE(INT iPos, INT iZone)
	
	INT iLinkedZoneBS = g_FMMC_STRUCT_ENTITIES.iFMMCZoneRocketZoneBS[iPos]
	
	IF iLinkedZoneBS != 0
	AND NOT IS_BIT_SET(iLinkedZoneBS, iZone)
		PRINTLN("[RCC MISSION] IS_RESTRICTION_ROCKET_POS_VALID - Returning FALSE for position ",iPos," not linked to zone ",iZone)
		RETURN FALSE
	ENDIF
	
	INT iLinkedObj = g_FMMC_STRUCT_ENTITIES.iFMMCZoneRocketLinkedObj[iPos]
	
	IF iLinkedObj != -1
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iLinkedObj])
		OR IS_ENTITY_DEAD(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iLinkedObj]))
			PRINTLN("[RCC MISSION] IS_RESTRICTION_ROCKET_POS_VALID - Returning FALSE for position ",iPos," as linked obj ",iLinkedObj," is non-existent or dead")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC VECTOR GET_CLOSEST_ROCKET_POSITION_FOR_ZONE(INT iZone, INT &iRocketPos)
	
	VECTOR vRocketPosition
	VECTOR vPlayer = GET_PLAYER_COORDS(LocalPlayer)
	FLOAT fClosestDist2 = 999999999
	INT iPosLoop
	
	FOR iPosLoop = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfRocketPositions - 1)
		IF IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.vFMMCZoneRocketPositions[iPosLoop])
			RELOOP
		ENDIF
		
		IF NOT IS_RESTRICTION_ROCKET_POS_VALID_FOR_ZONE(iPosLoop, iZone)
			RELOOP
		ENDIF
		
		FLOAT fDist2 = VDIST2(vPlayer,g_FMMC_STRUCT_ENTITIES.vFMMCZoneRocketPositions[iPosLoop])
		
		IF fDist2 < fClosestDist2
			fClosestDist2 = fDist2
			vRocketPosition = g_FMMC_STRUCT_ENTITIES.vFMMCZoneRocketPositions[iPosLoop]
			iRocketPos = iPosLoop
		ENDIF
		
	ENDFOR
	
	RETURN vRocketPosition
	
ENDFUNC

FUNC FLOAT GET_ROCKET_AIMING_INACCURACY_MAX_DISP(FLOAT fAccuracy)
	
	RETURN LERP_FLOAT(30.0, 0.0, (fAccuracy/100.0))
	
ENDFUNC

FUNC FLOAT GET_ROCKET_AIMING_INACCURACY_MIN_DISP(FLOAT fAccuracy)
	
	IF fAccuracy >= 10.0
		RETURN 0.0
	ENDIF
	
	RETURN LERP_FLOAT(10.0, 0.0, (fAccuracy/10.0))
	
ENDFUNC

PROC GET_ROCKET_AIMING_INACURRACY_MODIFIERS(FLOAT fAccuracy, FLOAT &fXMod, FLOAT &fYMod, FLOAT &fZMod)
	
	FLOAT fMaxDisp = GET_ROCKET_AIMING_INACCURACY_MAX_DISP(fAccuracy) // The furthest possible this could be put away from the original position
	FLOAT fMinDisp = GET_ROCKET_AIMING_INACCURACY_MIN_DISP(fAccuracy) // The minimum we need to move this away from the original position, only used at very low accuracies to make sure it doesn't hit
	
	PRINTLN("[RCC MISSION] GET_ROCKET_AIMING_INACURRACY_MODIFIERS - Called with fAccuracy ",fAccuracy,"; fMaxDisp = ",fMaxDisp,", fMinDisp = ",fMinDisp)
	
	SET_RANDOM_SEED(NATIVE_TO_INT(GET_NETWORK_TIME()))
	
	FLOAT fXRan = GET_RANDOM_FLOAT_IN_RANGE(0, 1) // A random number between 0 & 1, to pick a float between fMinDisp and fMaxDisp
	BOOL bXNve = GET_RANDOM_BOOL() // Whether this should be a positive or negative displacement from the original X position
	
	fXMod = LERP_FLOAT(fMinDisp, fMaxDisp, fXRan) * PICK_FLOAT(bXNve, -1, 1)
	PRINTLN("[RCC MISSION] GET_ROCKET_AIMING_INACURRACY_MODIFIERS - X: fXRan ",fXRan," bXNve ",bXNve," -> fXMod ",fXMod)
	
	FLOAT fYRan = GET_RANDOM_FLOAT_IN_RANGE(0, 1)
	BOOL bYNve = GET_RANDOM_BOOL()
	
	fYMod = LERP_FLOAT(fMinDisp, fMaxDisp, fYRan) * PICK_FLOAT(bYNve, -1, 1)
	PRINTLN("[RCC MISSION] GET_ROCKET_AIMING_INACURRACY_MODIFIERS - Y: fYRan ",fYRan," bYNve ",bYNve," -> fYMod ",fYMod)
	
	FLOAT fZRan = GET_RANDOM_FLOAT_IN_RANGE(0, 1)
	BOOL bZNve = GET_RANDOM_BOOL()
	
	fZMod = LERP_FLOAT(fMinDisp, fMaxDisp, fZRan) * PICK_FLOAT(bZNve, -1, 1)
	PRINTLN("[RCC MISSION] GET_ROCKET_AIMING_INACURRACY_MODIFIERS - Z: fZRan ",fZRan," bZNve ",bZNve," -> fZMod ",fZMod)
	
ENDPROC

// ZONE TIMERS // // // // // // // // // // // // // // // // // // // // // // // // // // 
PROC SET_ZONE_TIMER_AS_COMPLETED(INT iZone)
	IF NOT IS_BIT_SET(iZoneTimersCompletedBS, iZone)
		SET_BIT(iZoneTimersCompletedBS, iZone)
		RESET_NET_TIMER(stZoneTimers[iZone])
		PRINTLN("[ZoneTimer] Setting zone ", iZone, " as completed!")
	ENDIF
ENDPROC

PROC SET_ZONE_TIMER_AS_HIDDEN(INT iZone)
	IF NOT IS_BIT_SET(iZoneTimersHiddenBS, iZone)
		SET_BIT(iZoneTimersHiddenBS, iZone)
		PRINTLN("[ZoneTimer] Setting zone ", iZone, " as hidden!")
	ENDIF
ENDPROC

PROC DISPLAY_ZONE_TIMER_HUD(INT iZone)
	
	TEXT_LABEL_15 tl15 = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].tlZoneTimer_TimerName
	HUD_COLOURS eTitleColour = HUD_COLOUR_WHITE
	
	IF GET_ZONE_TIMER_TIME_REMAINING(iZone) < 5000
		eTitleColour = HUD_COLOUR_RED
	ENDIF
	
	DRAW_GENERIC_TIMER(GET_ZONE_TIMER_TIME_REMAINING(iZone), tl15, DEFAULT, TIMER_STYLE_DONTUSEMILLISECONDS, -1,  DEFAULT, HUDORDER_TOP, DEFAULT, eTitleColour, DEFAULT, DEFAULT, DEFAULT, eTitleColour)
ENDPROC

FUNC BOOL SHOULD_ZONE_TIMER_START()
	
	INT iTeam
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
	
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		
		IF iRule < FMMC_MAX_RULES
			IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_START_ALL_ZONE_TIMERS)
					PRINTLN("[ZONETIMER] SHOULD_ZONE_TIMER_START returning TRUE due to ciBS_RULE13_START_ALL_ZONE_TIMERS")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_ZONE_TIMER_STOP(INT iZone)
	
	IF NOT IS_ZONE_TIMER_RUNNING(iZone)
		RETURN FALSE
	ENDIF
	
	INT iTeam
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
	
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		
		IF iRule < FMMC_MAX_RULES
			IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_STOP_ALL_ZONE_TIMERS)
					PRINTLN("[ZONETIMER] SHOULD_ZONE_TIMER_STOP returning TRUE due to ciBS_RULE13_STOP_ALL_ZONE_TIMERS")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_ZONE_TIMER_BE_HIDDEN(INT iZone)
	
	INT iTeam
	FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
	
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		
		IF iRule < FMMC_MAX_RULES
			IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iType = ciFMMC_ZONE_TYPE__HEALTH_DRAIN
				AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_STOP_POISON_GAS)
					PRINTLN("[ZONETIMER] SHOULD_ZONE_TIMER_BE_HIDDEN returning TRUE due to ciFMMC_ZONE_TYPE__HEALTH_DRAIN")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_ZONE_TIMER_SKIP(INT iZone)
	
	UNUSED_PARAMETER(iZone)
	
	INT iTeam
	FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
	
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		
		IF iRule < FMMC_MAX_RULES
			IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iType = ciFMMC_ZONE_TYPE__HEALTH_DRAIN
				AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_STOP_POISON_GAS)
					PRINTLN("[ZONETIMER] SHOULD_ZONE_TIMER_SKIP returning TRUE due to ciFMMC_ZONE_TYPE__HEALTH_DRAIN")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DISPLAY_ZONE_TIMER(INT iZone)

	IF IS_BIT_SET(iZoneTimersHiddenBS, iZone)
		RETURN FALSE
	ENDIF
	
	IF GET_ZONE_TIMER_TIME_REMAINING(iZone) <= 0
		RETURN FALSE
	ENDIF

	RETURN IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_DISPLAY_ZONE_TIMER)
ENDFUNC

PROC PROCESS_ZONE_TIMER(INT iZone)
	IF bIsLocalPlayerHost
		IF NOT IS_ZONE_TIMER_RUNNING(iZone)
			IF SHOULD_ZONE_TIMER_START()
				BROADCAST_FMMC_ZONE_TIMER_EVENT(iZone, TRUE, FALSE)
				PRINTLN("[ZONETIMER] Starting zone timer for Zone ", iZone)
			ENDIF
		ELSE
			IF HAS_ZONE_TIMER_EXPIRED(iZone)
				IF NOT IS_BIT_SET(iZoneTimersCompletedBS, iZone)
					BROADCAST_FMMC_ZONE_TIMER_EVENT(iZone, FALSE, FALSE, FALSE, FALSE, TRUE)
				ENDIF
				
			ELIF SHOULD_ZONE_TIMER_BE_HIDDEN(iZone)
				IF NOT IS_BIT_SET(iZoneTimersHiddenBS, iZone)
					BROADCAST_FMMC_ZONE_TIMER_EVENT(iZone, FALSE, FALSE, FALSE, TRUE, FALSE)
				ENDIF
				
			ELIF SHOULD_ZONE_TIMER_STOP(iZone)
				PRINTLN("[ZONETIMER] Stopping zone timer for Zone ", iZone)
				BROADCAST_FMMC_ZONE_TIMER_EVENT(iZone, FALSE, TRUE)
			ENDIF
		ENDIF
		
		IF SHOULD_ZONE_TIMER_SKIP(iZone)
			BROADCAST_FMMC_ZONE_TIMER_EVENT(iZone, FALSE, FALSE, TRUE)
			PRINTLN("[ZONETIMER] Skipping zone timer for Zone ", iZone)
		ENDIF
	ENDIF
	
	IF IS_ZONE_TIMER_RUNNING(iZone)
		IF SHOULD_DISPLAY_ZONE_TIMER(iZone)
			DISPLAY_ZONE_TIMER_HUD(iZone)
		ENDIF
	ENDIF
ENDPROC
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 


//Assuming the player keeps on moving this velocity, where do I need to aim to hit them?
FUNC VECTOR GET_VECTOR_TO_AIM_AT(VECTOR vPlayerPos, VECTOR vPlayerVelocity, VECTOR vRocketPos, FLOAT fRocketSpeed, FLOAT fAccuracy)
	
	FLOAT t // Time taken for the rocket to hit
	
	VECTOR vDisp = vPlayerPos - vRocketPos
	
	FLOAT fVX = DOT_PRODUCT(vPlayerVelocity, vDisp)
	
	FLOAT fRoot = ( POW(fVX,2) + (4 * VMAG2(vDisp) * ( POW(fRocketSpeed,2) - VMAG2(vPlayerVelocity) ) ) )
	
	IF fRoot >= 0
		// Solve the quadratic equation for t
		t = ( ( fVX + SQRT( fRoot ) ) / ( 2 * ( POW(fRocketSpeed,2) - VMAG2(vPlayerVelocity) ) ) )
	ELSE
		//Shoot at an arbitrary distance in front (square root of a negative number isn't something we want to work with in our real-space game world)
		t = 1
	ENDIF
	
	// Work out the values of our aim at vector:
	VECTOR vAim = vPlayerPos + (vPlayerVelocity * t)
	PRINTLN("[RCC MISSION] GET_VECTOR_TO_AIM_AT - Perfect accuracy vAim calculated as ",vAim)
	
	IF fAccuracy <= 99.9
		FLOAT fXMod, fYMod, fZMod
		GET_ROCKET_AIMING_INACURRACY_MODIFIERS(fAccuracy, fXMod, fYMod, fZMod)
		
		vAim.x += fXMod
		vAim.y += fYMod
		vAim.z += fZMod
		
		PRINTLN("[RCC MISSION] GET_VECTOR_TO_AIM_AT - Imperfect fAccuracy ",fAccuracy," -> new inaccuracte vAim ",vAim)
	ENDIF
	
	RETURN vAim
	
ENDFUNC

PROC SELECT_NEXT_WAVE_DAMPING_SCALER()
	
	INT iZone = -1
	INT iZonesToConsiderBS
	
	INT i
	FLOAT fSmallestLength = 99999
	VECTOR vLengthTemp
	FLOAT fTempLength
	INT iTempDampingZoneBitset = iWaveDampingZoneBitset
	
	FOR i = 0 TO (FMMC_MAX_NUM_ZONES - 1)
		IF IS_BIT_SET(iTempDampingZoneBitset, i)
			//pick ones of smallest length
			//from those, pick one w closest centrepoint
			
			vLengthTemp = g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1] - g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0]
			vLengthTemp.z = 0
			
			fTempLength = VMAG(vLengthTemp)
			
			IF fTempLength < (fSmallestLength * 0.9)
				fSmallestLength = fTempLength
				iZonesToConsiderBS = 0
				SET_BIT(iZonesToConsiderBS, i)
				iZone = i
			ELIF (fTempLength >= (fSmallestLength * 0.9))
			AND (fTempLength <= (fSmallestLength * 1.1))
				SET_BIT(iZonesToConsiderBS, i)
				iZone = -1
			ENDIF
			
			CLEAR_BIT(iTempDampingZoneBitset, i)
		ENDIF
		
		IF iTempDampingZoneBitset = 0
			i = FMMC_MAX_NUM_ZONES //Break out, we've checked all the zones we need to!
		ENDIF
	ENDFOR
	
	IF iZone = -1
	AND iZonesToConsiderBS > 0
		//Have more than one zone to consider, find the one with the closest centrepoint:
		
		VECTOR vPlayer = GET_ENTITY_COORDS(PlayerPedToUse)
		
		fSmallestLength = 999999999
		
		FOR i = 0 TO (FMMC_MAX_NUM_ZONES - 1)
			IF IS_BIT_SET(iZonesToConsiderBS, i)
				
				fTempLength = VDIST2(vPlayer, ((g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0] + g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1]) * 0.5) )
				
				IF fTempLength < fSmallestLength
					fSmallestLength = fTempLength
					iZone = i
				ENDIF
				
				CLEAR_BIT(iZonesToConsiderBS, i)
			ENDIF
			
			IF iZonesToConsiderBS = 0
				i = FMMC_MAX_NUM_ZONES //Break out, we've checked all the zones we need to!
			ENDIF
		ENDFOR
		
	ENDIF
	
	PRINTLN("[RCC MISSION] SELECT_NEXT_WAVE_DAMPING_SCALER - next zone found is ",iZone)
	
	IF iZone != -1
		IF GET_DEEP_OCEAN_SCALER() != g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue2
			PRINTLN("[RCC MISSION] SELECT_NEXT_WAVE_DAMPING_SCALER - calling SET_DEEP_OCEAN_SCALER for ",iZone," with value ",g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue2)
			SET_DEEP_OCEAN_SCALER(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue2)
		ENDIF
	ELSE
		IF GET_DEEP_OCEAN_SCALER() != 1.0
			PRINTLN("[RCC MISSION] SELECT_NEXT_WAVE_DAMPING_SCALER - Calling RESET_DEEP_OCEAN_SCALER")
			RESET_DEEP_OCEAN_SCALER()
		ENDIF
	ENDIF
	
ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: ZONE REMOVAL !
//
//************************************************************************************************************************************************************


/// PURPOSE:
///    Turns off / back on vehicle generators and roads for a specific zone
/// PARAMS:
///    iZone - The zone you want to do this for!
///    bSet - If set to TRUE then the vehicle generators and roads of this zone will be turned off, as if the zone was being activated! If set to FALSE, then these vehicle generators and roads will be turned back on, as if the zone was being deactivated.
///    bForce - WARNING! DO NOT USE THIS UNLESS YOU ARE SURE YOU WANT TO FORCE THE CHANGE! This bypasses the checks for if we have already applied the vehicle generators / roads.
///    bRestore - Uses SET_ROADS_BACK_TO_ORIGINAL instead of SET_ROADS_IN_AREA (this will ignore the bSet parameter)
PROC SET_VEHICLE_GENERATORS_AND_ROADS_INACTIVE_FOR_ZONE_LEGACY(INT iZone, BOOL bSet, BOOL bForce = FALSE, BOOL bRestore = FALSE)
	
	PRINTLN("[RCC MISSION][POP] SET_VEHICLE_GENERATORS_AND_ROADS_INACTIVE_FOR_ZONE_LEGACY - Calling for zone ",iZone," with bSet ",bSet," / bForce ",bForce," / bRestore ", bRestore, " callstack:")
	DEBUG_PRINTCALLSTACK()
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iType != ciFMMC_ZONE_TYPE__CLEAR_ALL
	AND g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iType != ciFMMC_ZONE_TYPE__CLEAR_VEHICLES
	AND g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iType != ciFMMC_ZONE_TYPE__ACTIVATE_ROADS
		CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION][POP] SET_VEHICLE_GENERATORS_AND_ROADS_INACTIVE_FOR_ZONE_LEGACY - Called for zone ",iZone,", but this isn't a zone with vehicle generators / roads - iType is ",g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iType)
		PRINTLN("[RCC MISSION][POP] SET_VEHICLE_GENERATORS_AND_ROADS_INACTIVE_FOR_ZONE_LEGACY - Called for zone ",iZone,", but this isn't a zone with vehicle generators / roads - iType is ",g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iType)
		EXIT
	ENDIF
	
	VECTOR vZonePos0 = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0]
	VECTOR vZonePos1 = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1]
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAreaType != ciFMMC_ZONE_SHAPE__SPHERE
		vZonePos0.z = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0].z - 100.0
		vZonePos1.z = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1].z + 500.0
	ENDIF
	
	BOOL bChangeGenerators
	
	IF bSet
		IF NOT IS_BIT_SET(iBitsetVehicleGeneratorsActiveArea, iZone)
			bChangeGenerators = TRUE
			SET_BIT(iBitsetVehicleGeneratorsActiveArea, iZone)
		ENDIF
	ELSE
		IF IS_BIT_SET(iBitsetVehicleGeneratorsActiveArea, iZone)
			bChangeGenerators = TRUE
			CLEAR_BIT(iBitsetVehicleGeneratorsActiveArea, iZone)
		ENDIF
	ENDIF
	
	IF bChangeGenerators
	OR bForce
		PRINTLN("[RCC MISSION][POP] SET_VEHICLE_GENERATORS_AND_ROADS_INACTIVE_FOR_ZONE_LEGACY - Calling SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA for zone ",iZone," turning generators ",PICK_STRING((NOT bSet),"Active","Inactive"))
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vZonePos0, vZonePos1, (NOT bSet), FALSE)
	ENDIF
	
	BOOL bChangeRoads
	
	IF bSet
		IF NOT IS_BIT_SET(iBitsetSetRoadsArea, iZone)
			bChangeRoads = TRUE
			SET_BIT(iBitsetSetRoadsArea, iZone)
		ENDIF
	ELSE
		IF IS_BIT_SET(iBitsetSetRoadsArea, iZone)
			bChangeRoads = TRUE
			CLEAR_BIT(iBitsetSetRoadsArea, iZone)
		ENDIF
	ENDIF
	
	IF bChangeRoads
	OR bForce
		IF iCurrentHeistMissionIndex != HBCA_BS_PACIFIC_STANDARD_BIKE
		AND iCurrentHeistMissionIndex != HBCA_BS_PRISON_BREAK_PLANE
			IF NOT bRestore
				PRINTLN("[RCC MISSION][POP] SET_VEHICLE_GENERATORS_AND_ROADS_INACTIVE_FOR_ZONE_LEGACY - Calling SET_ROADS_IN_AREA for zone ", iZone, "[v0: ", vZonePos0, ", v1: ", vZonePos1, "] turning generators ",PICK_STRING((NOT bSet),"Active","Inactive"))
				SET_ROADS_IN_AREA(vZonePos0, vZonePos1, (NOT bSet), FALSE)
			ELSE
				PRINTLN("[RCC MISSION][POP] SET_VEHICLE_GENERATORS_AND_ROADS_INACTIVE_FOR_ZONE_LEGACY - Calling SET_ROADS_BACK_TO_ORIGINAL for zone ",iZone, "[v0: ", vZonePos0, ", v1: ", vZonePos1, "] turning generators FALSE")
				SET_ROADS_BACK_TO_ORIGINAL(vZonePos0, vZonePos1, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL IS_ZONE_TRIGGERABLE(INT iZone)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_ONLY_TRIGGER_ZONE_REMOTELY)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_ONLY_TRIGGER_ZONE_REMOTELY)
	AND IS_BIT_SET(iZoneBS_ActivatedRemotely, iZone)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_ZONE_BE_REMOVED_DUE_TO_ENTITY_LINK(INT i)

	IF IS_BIT_SET(iLocalZoneRemovedBitset, i)
		RETURN FALSE
	ENDIF
	
	BOOL bRemoveWhenNotExist = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iZoneBS, ciFMMC_ZONEBS_REMOVE_ZONE_WHEN_NO_ENTITY_LINK)
	BOOL bRemoveWhenNotPriority = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iZoneBS, ciFMMC_ZONEBS_REMOVE_ZONE_WHEN_ENTITY_LINK_NOT_PRIORITY)
	INT iTeam = MC_PlayerBD[iPartToUse].iteam
	
	IF bRemoveWhenNotExist OR bRemoveWhenNotPriority
		IF iTeam > -1 AND iTeam < FMMC_MAX_TEAMS
			INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
			
			IF iRule < FMMC_MAX_RULES
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkType > -1
				AND g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex > -1
					SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkType
						CASE ciENTITY_TYPE_PED
							CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] iZone ", i, " Ped Link: ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex, " Ped Priority: ", MC_ServerBD_4.iPedPriority[g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex][iTeam])
							
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex])
								PED_INDEX pedIndex
								pedIndex = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex])
								IF (IS_PED_INJURED(pedIndex) AND bRemoveWhenNotExist)
								OR ((MC_ServerBD_4.iPedPriority[g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex][iTeam] = 99999 OR MC_ServerBD_4.iPedPriority[g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex][iTeam] < iRule) AND bRemoveWhenNotPriority)
									CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] iZone ", i, " Ped Link Exists... Index: ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex)
									RETURN TRUE
								ENDIF
							ELSE
								IF bRemoveWhenNotExist
									CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] iZone ", i, " Ped Link Does not Exist... Index: ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex)
									RETURN TRUE
								ENDIF
							ENDIF
						BREAK
						CASE ciENTITY_TYPE_VEHICLE
							CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] iZone ", i, " Vehicle Link: ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex, " Veh Priority: ", MC_ServerBD_4.iVehPriority[g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex][iTeam])
						
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex])
								VEHICLE_INDEX vehIndex
								vehIndex = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex])
								IF (NOT IS_VEHICLE_DRIVEABLE(vehIndex) AND bRemoveWhenNotExist)
								OR ((MC_ServerBD_4.iVehPriority[g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex][iTeam] = 99999 OR MC_ServerBD_4.iVehPriority[g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex][iTeam] < iRule) AND bRemoveWhenNotPriority)
									CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] iZone ", i, " Vehicle Link Exists but it's destroyed... Index: ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex)
									RETURN TRUE
								ENDIF
							ELSE
								IF bRemoveWhenNotExist
									CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] iZone ", i, " Vehicle Link Does not Exist... Index: ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex)
									RETURN TRUE
								ENDIF
							ENDIF
						BREAK
						CASE ciENTITY_TYPE_OBJECT						
							CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] iZone ", i, " Object Link: ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex, " Obj Priority: ",  MC_ServerBD_4.iObjPriority[g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex][iTeam])
							
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niObject[g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex])
								OBJECT_INDEX ObjIndex
								ObjIndex = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex])
								IF ((IS_ENTITY_ATTACHED_TO_ANY_PED(ObjIndex) OR IS_ENTITY_ATTACHED(ObjIndex) OR MC_ServerBD.iObjCarrier[g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex] > -1) AND bRemoveWhenNotExist)
								OR ((MC_ServerBD_4.iObjPriority[g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex][iTeam] = 99999 OR MC_ServerBD_4.iObjPriority[g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex][iTeam] < iRule) AND bRemoveWhenNotPriority)
									CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] iZone ", i, " Object Link Exists and is attached/carried... Index: ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex)
									RETURN TRUE
								ENDIF
							ELSE
								IF bRemoveWhenNotExist
									CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] iZone ", i, " Object Link Does not Exists... Index: ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex)
									RETURN TRUE
								ENDIF
							ENDIF
						BREAK
						CASE ciENTITY_TYPE_GOTO
							
						BREAK
						CASE ciENTITY_TYPE_KILL_PLAYER
						
						BREAK
						CASE ciENTITY_TYPE_LAST_PLAYER
						
						BREAK
						CASE ciENTITY_TYPE_ZONES
						
						BREAK
					ENDSWITCH
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_ZONE_BE_REMOVED_DUE_TO_AGGRO(INT iZone)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_REMOVE_ZONE_ON_AGGRO)
		IF IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_0)
		OR IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_1)
		OR IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_2)
		OR IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_3)
			PRINTLN("SHOULD_ZONE_BE_REMOVED_DUE_TO_AGGRO - Returning TRUE for zone ", iZone)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_RESTRICTION_ZONE_MID_MISSION_REMOVAL(BOOL bShouldRemove = FALSE)
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	VECTOR vZonePos0, vZonePos1
	
	INT i
	
	FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfZones-1)
		IF IS_BIT_SET(iLocalZoneBitset, i)
		AND NOT IS_BIT_SET(iLocalZoneRemovedBitset, i)
		
			IF iRule < FMMC_MAX_RULES
			AND (iRule >= g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iZoneEndPriority[iTeam] OR bShouldRemove)
				
				PRINTLN("[RCC MISSION][POP] PROCESS_RESTRICTION_ZONE_MID_MISSION_REMOVAL - Removing zone ",i," as my team (",iTeam,") Rule: ", iRule, " has reached/passed iZoneEndPriority ",g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iZoneEndPriority[iTeam], " bShouldRemove: ", bShouldRemove)
				
				IF iPopMultiArea[i] != -1
					IF DOES_POP_MULTIPLIER_AREA_EXIST(iPopMultiArea[i])
						REMOVE_POP_MULTIPLIER_AREA(iPopMultiArea[i], TRUE)
						PRINTLN("[RCC MISSION][POP] PROCESS_RESTRICTION_ZONE_MID_MISSION_REMOVAL - iPopMultiArea cleaned up: ",i," native area id: ",iPopMultiArea[i])
						iPopMultiArea[i] = -1
					ENDIF
				ENDIF
				
				IF bipopScenarioArea[i] != NULL
					REMOVE_SCENARIO_BLOCKING_AREA(bipopScenarioArea[i])
					PRINTLN("[RCC MISSION][POP] PROCESS_RESTRICTION_ZONE_MID_MISSION_REMOVAL - bipopScenarioArea cleaned up: ",i," native area id: ",NATIVE_TO_INT(bipopScenarioArea[i]))
					bipopScenarioArea[i] = NULL
				ENDIF
				
				vZonePos0 = g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0]
				vZonePos1 = g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1]
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iAreaType != ciFMMC_ZONE_SHAPE__SPHERE
					vZonePos0.z = g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0].z - 100.0
					vZonePos1.z = g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1].z + 500.0
				ENDIF
				
				SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iType
					CASE ciFMMC_ZONE_TYPE__CLEAR_VEHICLES
					CASE ciFMMC_ZONE_TYPE__CLEAR_ALL
					CASE ciFMMC_ZONE_TYPE__ACTIVATE_ROADS
						SET_VEHICLE_GENERATORS_AND_ROADS_INACTIVE_FOR_ZONE_LEGACY(i, FALSE)
					BREAK
					CASE ciFMMC_ZONE_TYPE__CLEAR_ALL_AND_PV
						CLEAR_PERSONAL_VEHICLE_NO_SPAWN_ZONE()
					BREAK
					CASE ciFMMC_ZONE_TYPE__CLEAR_COVER
						IF IS_BIT_SET(iBitsetCoverBlockingArea, i)
							REMOVE_SPECIFIC_COVER_BLOCKING_AREAS(vZonePos0, vZonePos1, FALSE, FALSE, TRUE, TRUE)
							
							CLEAR_BIT(iBitsetCoverBlockingArea, i)
						ENDIF
					BREAK
					CASE ciFMMC_ZONE_TYPE__NO_PLAYER_SPAWNING
						//bug 2123637 - need a way of removing mission spawn occlusion boxes
					BREAK
					CASE ciFMMC_ZONE_TYPE__WATER_DAMPENING
						PRINTLN("[RCC MISSION][POP] PROCESS_RESTRICTION_ZONE_MID_MISSION_REMOVAL - Clearing water dampening zone ",i)
						
						IF iZoneWaterCalmingQuadID[i] != -1
							IF iWaterCalmingQuad[iZoneWaterCalmingQuadID[i]] != -1
								REMOVE_EXTRA_CALMING_QUAD(iWaterCalmingQuad[iZoneWaterCalmingQuadID[i]])
								iWaterCalmingQuad[iZoneWaterCalmingQuadID[i]] = -1
							ENDIF
							iZoneWaterCalmingQuadID[i] = -1
						ENDIF
						
						IF IS_BIT_SET(iWaveDampingZoneBitset, iZoneStaggeredLoop)
							
							CLEAR_BIT(iWaveDampingZoneBitset, iZoneStaggeredLoop)
							
							IF iWaveDampingZoneBitset > 0
								IF GET_DEEP_OCEAN_SCALER() = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].fZoneValue2 //If this one is the zone running right now
									SELECT_NEXT_WAVE_DAMPING_SCALER()
								ENDIF
							ELSE
								IF GET_DEEP_OCEAN_SCALER() != 1.0
									PRINTLN("[RCC MISSION][POP] PROCESS_RESTRICTION_ZONE_MID_MISSION_REMOVAL - Calling RESET_DEEP_OCEAN_SCALER")
									RESET_DEEP_OCEAN_SCALER()
								ENDIF
							ENDIF
							
						ENDIF
					BREAK
					CASE ciFMMC_ZONE_TYPE__GPS
						IF iGPSDisabledZones[i] != -1
							PRINTLN("[RCC MISSION][POP] PROCESS_RESTRICTION_ZONE_MID_MISSION_REMOVAL clearing GPS disabled zone at index ", iGPSDisabledZones[i])
							CLEAR_GPS_DISABLED_ZONE_AT_INDEX(iGPSDisabledZones[i])
							iGPSDisabledZones[i] = -1
						ENDIF
					BREAK
					
					CASE ciFMMC_ZONE_TYPE__DISPATCH_SPAWN_BLOCK
						IF iDispatchBlockZone[i] != -1
							PRINTLN("[RCC MISSION][POP] PROCESS_RESTRICTION_ZONE_MID_MISSION_REMOVAL clearing Dispatch spawn disabled zone at index ", iDispatchBlockZone[i])
							REMOVE_DISPATCH_SPAWN_BLOCKING_AREA(iDispatchBlockZone[i])
							iDispatchBlockZone[i] = -1
						ENDIF
					BREAK
					
					CASE ciFMMC_ZONE_TYPE__EXPLOSION_AREA
						
						BOOL bOtherZoneActive
						INT iLoop
						
						FOR iLoop = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfZones - 1)
							IF iLoop != i
							AND IS_BIT_SET(iLocalZoneBitset, iLoop)
							AND NOT IS_BIT_SET(iLocalZoneRemovedBitset, iLoop)
							AND g_FMMC_STRUCT_ENTITIES.sPlacedZones[iLoop].iType = ciFMMC_ZONE_TYPE__EXPLOSION_AREA
								bOtherZoneActive = TRUE
							ENDIF
						ENDFOR
						
						IF NOT bOtherZoneActive
							CLEAR_BIT(iLocalBoolCheck3, LBOOL3_EXPLOSIVE_ZONE_ACTIVE)
						ENDIF
					BREAK
					
					CASE ciFMMC_ZONE_TYPE__SET_AIR_DRAG_MULTIPLIER
						CLEAR_BIT(iLocalZoneCheckBitset, i)
						CLEAR_BIT(iPlayerVehicleSlowZoneBitset, i)
						PRINTLN("[RCC MISSION][POP] Removing player vehicles slow zone ", i)
					BREAK
					
					CASE ciFMMC_ZONE_TYPE__PLAYER_VEHICLE_MAX_SPEED
						CLEAR_BIT(iLocalZoneCheckBitset, i)
						CLEAR_BIT(iPlayerVehicleMaxSpeedBitset, i)
						PRINTLN("[RCC MISSION][POP] Removing player vehicles max speed ", i)
					BREAK
					
					CASE ciFMMC_ZONE_TYPE__BLOCK_JUMPING_AND_CLIMBING
						CLEAR_BIT(iLocalZoneCheckBitset, i)
						bInBlockedJumpAndClimbZone = FALSE
						CLEAR_BIT(iBlockJumpClimbZoneBitset, i)
						PRINTLN("[RCC MISSION][POP] Removing climbing and jumping blocking zone ", i)
					BREAK
					
					CASE ciFMMC_ZONE_TYPE__BLOCK_RUNNING
						CLEAR_BIT(iLocalZoneCheckBitset, i)
						bInBlockedRunningZone = FALSE
						CLEAR_BIT(iBlockRunningZoneBitset, i)
						PRINTLN("[RCC MISSION][POP] Removing running blocking zone ", i)
					BREAK
					
					CASE ciFMMC_ZONE_TYPE__BLOCK_WEAPONS
						CLEAR_BIT(iLocalZoneCheckBitset, i)
						bInBlockedWeaponsZone = FALSE
						CLEAR_BIT(iBlockWeaponsZoneBitset, i)
						PRINTLN("[RCC MISSION][POP] Removing weapon blocking zone ", i)
					BREAK
					
					CASE ciFMMC_ZONE_TYPE__PLAYER_CANT_EXIT_VEHICLE
						CLEAR_BIT(iLocalZoneCheckBitset, i)
						bInPlayerCantExitVehicleZone = FALSE
						CLEAR_BIT(iPlayerCantExitVehicleZoneBitset, i)
						PRINTLN("[RCC MISSION][POP] Removing Player Can't Exit Vehicle zone ", i)
					BREAK
					
					CASE ciFMMC_ZONE_TYPE__PLAYER_CANT_DRIVE_VEHICLE
						CLEAR_BIT(iLocalZoneCheckBitset, i)
						bInPlayerCantDriveVehicleZone = FALSE
						CLEAR_BIT(iPlayerCantDriveVehicleZoneBitset, i)
						PRINTLN("[RCC MISSION][POP] Removing Player Can't Drive Vehicle zone ", i)
					BREAK
					
					CASE ciFMMC_ZONE_TYPE__BLIP_PLAYER
						CLEAR_BIT(iLocalZoneCheckBitset, i)
						PRINTLN("[RCC MISSION][POP] Removing blip player zone ", i)
					BREAK
					
					CASE ciFMMC_ZONE_TYPE__OBJECT_RESPAWN
						CLEAR_BIT(iLocalZoneCheckBitset, i)
						PRINTLN("[RCC MISSION][POP] Removing object respawn zone ", i)
					BREAK
					
					CASE ciFMMC_ZONE_TYPE__OBJECT_RESPAWN_WITHIN
						CLEAR_BIT(iLocalZoneCheckBitset, i)
						PRINTLN("[RCC MISSION][POP] Removing object respawn within zone ", i)
					BREAK
					
					CASE ciFMMC_ZONE_TYPE__AUTO_PARACHUTE
						CLEAR_BIT(iLocalZoneCheckBitset, i)
						CLEAR_BIT(iLocalBoolCheck19, LBOOL19_AUTO_PARACHUTE_ZONE_ACTIVATED)
						PRINTLN("[RCC MISSION][POP] Removing auto parachute zone ", i)
					BREAK
										
					CASE ciFMMC_ZONE_TYPE__ALERT_PEDS
						CLEAR_BIT(iLocalZoneCheckBitset, i)
						CLEAR_BIT(iLocalBoolCheck23, LBOOL23_ALERT_PED_ZONE_ACTIVATED)
						PRINTLN("[RCC MISSION][POP] Removing Alert Ped Zone ", i)
					BREAK
					
					CASE ciFMMC_ZONE_TYPE__REFILL_SPECIAL_ABILITY
						CLEAR_BIT(iLocalZoneCheckBitset, i)
						CLEAR_BIT(iLocalBoolCheck24, LBOOL24_SPECIAL_ABILITY_REFILLED)
						PRINTLN("[RCC MISSION][POP] Removing Refill Special Ability zone ", i)
					BREAK
					
					CASE ciFMMC_ZONE_TYPE__BLOCK_VTOL_TOGGLE
						CLEAR_BIT(iLocalZoneCheckBitset, i)
						bInBlockVTOLZone = FALSE
						CLEAR_BIT(iBlockVTOLZoneBitset, i)
						PRINTLN("[RCC MISSION][POP] Removing block VTOL zone ", i)
					BREAK
					
					CASE ciFMMC_ZONE_TYPE__BLOCK_ELECTROCUTION
						CLEAR_BIT(iLocalZoneCheckBitset, i)
						PRINTLN("[RCC MISSION][POP] PROCESS_RESTRICTION_ZONES Clearing: ciFMMC_ZONE_TYPE__BLOCK_ELECTROCUTION.")
						CLEAR_BIT(iBlockElectrocutionZoneBitset, iZoneStaggeredLoop)
					BREAK
					
					CASE ciFMMC_ZONE_TYPE__GOAL_AREA
						CLEAR_BIT(iLocalZoneCheckBitset, i)
						PRINTLN("[RCC MISSION][POP] PROCESS_RESTRICTION_ZONES Clearing: ciFMMC_ZONE_TYPE__GOAL_AREA.")
						CLEAR_BIT(iBlockElectrocutionZoneBitset, iZoneStaggeredLoop)
					BREAK
					
					CASE ciFMMC_ZONE_TYPE__BLOCK_PLAYER_CONTROL
						CLEAR_BIT(iLocalZoneCheckBitset, i)
						PRINTLN("[RCC MISSION][POP] PROCESS_RESTRICTION_ZONES Clearing ciFMMC_ZONE_TYPE__BLOCK_PLAYER_CONTROL")
						CLEAR_BIT(iBlockPlayerControlZoneBitset, iZoneStaggeredLoop)
					BREAK
					
					CASE ciFMMC_ZONE_TYPE__KILL_ZONE
						CLEAR_BIT(iLocalZoneCheckBitset, i)
						PRINTLN("[RCC MISSION][POP] PROCESS_RESTRICTION_ZONES Clearing ciFMMC_ZONE_TYPE__KILL_ZONE")
					BREAK
					
					CASE ciFMMC_ZONE_TYPE__SPAWN_PROTECTION
						PRINTLN("[RCC MISSION][POP] PROCESS_RESTRICTION_ZONES Removing ciFMMC_ZONE_TYPE__SPAWN_PROTECTION zone ", i)
						IF iAirDefenseSphereMission[i] != -1
							IF DOES_AIR_DEFENCE_SPHERE_EXIST(iAirDefenseSphereMission[i])
								PRINTLN("[LM][PROCESS_RESTRICTION_ZONES] - Removing air defence sphere.")
								REMOVE_AIR_DEFENCE_SPHERE(iAirDefenseSphereMission[i])
							ENDIF
						ENDIF
					BREAK
					
					CASE ciFMMC_ZONE_TYPE__STUNT_SCORE
						CLEAR_BIT(iLocalZoneCheckBitset, i)
						PRINTLN("[RCC MISSION][POP] PROCESS_RESTRICTION_ZONES Clearing: ciFMMC_ZONE_TYPE__STUNT_SCORE.")
						CLEAR_BIT(iBlockElectrocutionZoneBitset, iZoneStaggeredLoop)
					BREAK
					
					CASE ciFMMC_ZONE_TYPE__RETURN_OBJECT
						CLEAR_BIT(iLocalZoneCheckBitset, i)
						PRINTLN("[RCC MISSION][POP] PROCESS_RESTRICTION_ZONES Clearing: ciFMMC_ZONE_TYPE__RETURN_OBJECT.")
						CLEAR_BIT(iBlockElectrocutionZoneBitset, iZoneStaggeredLoop)
					BREAK

					CASE ciFMMC_ZONE_TYPE__EXPLODE_PLAYER
						CLEAR_BIT(iLocalZoneCheckBitset, i)
						PRINTLN("[RCC MISSION][POP] PROCESS_RESTRICTION_ZONES Clearing: ciFMMC_ZONE_TYPE__EXPLODE_PLAYER.")						
					BREAK
										
					CASE ciFMMC_ZONE_TYPE__HIDE_OBJECTS_SEARCH_AREA
						CLEAR_BIT(iLocalZoneCheckBitset, i)
						PRINTLN("[RCC MISSION][POP] PROCESS_RESTRICTION_ZONES Clearing: ciFMMC_ZONE_TYPE__HIDE_OBJECTS_SEARCH_AREA.")						
					BREAK
					
					CASE ciFMMC_ZONE_TYPE__BLANK
						CLEAR_BIT(iLocalZoneCheckBitset, i)
						PRINTLN("[RCC MISSION][POP] PROCESS_RESTRICTION_ZONES Clearing: ciFMMC_ZONE_TYPE__BLANK.")
					BREAK
					
					CASE ciFMMC_ZONE_TYPE__AGGRO_PEDS_IN_THIS_ZONE
						CLEAR_BIT(iLocalZoneCheckBitset, i)
						PRINTLN("[RCC MISSION][POP] PROCESS_RESTRICTION_ZONES Clearing: ciFMMC_ZONE_TYPE__AGGRO_PEDS_IN_THIS_ZONE.")
					BREAK
					
					CASE ciFMMC_ZONE_TYPE__HEALTH_DRAIN
						CLEAR_BIT(iLocalZoneCheckBitset, i)
						PRINTLN("[RCC MISSION][POP] PROCESS_RESTRICTION_ZONES Clearing: ciFMMC_ZONE_TYPE__HEALTH_DRAIN.")
					BREAK
					
					CASE ciFMMC_ZONE_TYPE__METAL_DETECTOR
						CLEAR_BIT(iLocalZoneCheckBitset, i)
						PRINTLN("[RCC MISSION][POP] PROCESS_RESTRICTION_ZONES Clearing: ciFMMC_ZONE_TYPE__METAL_DETECTOR.")
					BREAK
					
					CASE ciFMMC_ZONE_TYPE__PLAYER_TRIGGERED_EXPLOSION_DANGER_AREA
						CLEAR_BIT(iLocalZoneCheckBitset, i)
						PRINTLN("[RCC MISSION][POP] PROCESS_RESTRICTION_ZONES Clearing: ciFMMC_ZONE_TYPE__PLAYER_TRIGGERED_EXPLOSION_DANGER_AREA.")
					BREAK
				
					CASE ciFMMC_ZONE_TYPE__NAV_BLOCKING
						CLEAR_BIT(iLocalZoneCheckBitset, i)
						PRINTLN("[RCC MISSION][POP] PROCESS_RESTRICTION_ZONES Clearing: ciFMMC_ZONE_TYPE__NAV_BLOCKING for iZone: ", i)
						REMOVE_NAVMESH_BLOCKING_OBJECT(iNavBlockerZone[i])
					BREAK
											
					CASE ciFMMC_ZONE_TYPE__BLOCK_ROAD_NODES
						CLEAR_BIT(iLocalZoneCheckBitset, i)
						SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iAreaType
							CASE ciFMMC_ZONE_SHAPE__ANGLED_AREA
								PRINTLN("[RCC MISSION][POP] PROCESS_RESTRICTION_ZONES Clearing: ciFMMC_ZONE_TYPE__BLOCK_ROAD_NODES for iZone: ", i)
								SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0], g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1], g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].fRadius, FALSE)
							BREAK
							
							CASE ciFMMC_ZONE_SHAPE__AXIS_ALIGNED_BOX
								PRINTLN("[RCC MISSION][POP] PROCESS_RESTRICTION_ZONES Clearing: ciFMMC_ZONE_SHAPE__AXIS_ALIGNED_BOX for iZone: ", i)
								SET_ROADS_BACK_TO_ORIGINAL(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0], g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1], FALSE)
							BREAK
						ENDSWITCH
					BREAK		
				ENDSWITCH
				
				RESET_NET_TIMER(td_ZoneRadiusOverrideTimer[i])
				PRINTLN("[RCC MISSION][POP] PROCESS_RESTRICTION_ZONES Setting iLocalZoneRemovedBitset for iZone; ", i)
				SET_BIT(iLocalZoneRemovedBitset, i)
				CLEAR_BIT(iLocalZoneCheckBitset, i)
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iZoneBS, ciFMMC_ZONEBS_TRIGGER_ZONE_REMOTELY)
				AND g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkType = ciENTITY_TYPE_ZONES
				AND g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex > -1
					IF IS_BIT_SET(iZoneBS_ActivatedRemotely, g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex)
						PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES - (REMOVED) iZone: ", i, " Zone Entity Link Index: ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex, " Is now being de-activated remotely ..")
						CLEAR_BIT(iZoneBS_ActivatedRemotely, g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iEntityLinkIndex)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: ZONE CREATION !
//
//************************************************************************************************************************************************************



FUNC BOOL SHOULD_CREATE_RESTRICTION_ZONE( INT iZone, INT iTeam )
	
	PRINTLN("[RCC MISSION][POP] SHOULD_CREATE_RESTRICTION_ZONE - for ", iZone, " iZoneStartPriority is: ", " iTeam: ", iTeam)	
	
	#IF IS_DEBUG_BUILD
	IF( iZone < 0 OR iZone >= FMMC_MAX_NUM_ZONES )
		ASSERTLN( "SHOULD_CREATE_RESTRICTION_ZONE [POP] - iZone < 0 OR iZone >= FMMC_MAX_NUM_ZONES, iZone: ", iZone )
		RETURN FALSE
	ENDIF
	IF( iTeam < 0 OR iTeam >= FMMC_MAX_TEAMS )
		PRINTLN( "[RCC MISSION] SHOULD_CREATE_RESTRICTION_ZONE [POP] - iTeam < 0 OR iTeam >= FMMC_MAX_TEAMS, iTeam: ", iTeam )
		RETURN FALSE
	ENDIF
	#ENDIF
	
	IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[iTeam]],ciBS_RULE8_PAUSE_ON_PLAYER_DURING_RESTART)
			IF IS_PLAYER_IN_RESPAWN_BACK_AT_START_LOGIC(iLocalPart)
			AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_ALL_PLAYERS_RENDERPHASES_PAUSED)
			AND NOT IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet3, PBBOOL3_RESTART_RENDERPHASE_PAUSED)
				PRINTLN( "[RCC MISSION] SHOULD_CREATE_RESTRICTION_ZONE [POP] - NOPE, in restart logic and renderphases unpaused still for iTeam: ", iTeam )
				RETURN FALSE
			ENDIF
			
			IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
				PRINTLN( "[RCC MISSION] SHOULD_CREATE_RESTRICTION_ZONE [POP] - NOPE, not in restart logic yet for iTeam: ", iTeam )
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF SHOULD_ZONE_BE_REMOVED_DUE_TO_ENTITY_LINK(iZone)
		PRINTLN( "[RCC MISSION] SHOULD_CREATE_RESTRICTION_ZONE [POP] - iZone: ", iZone, " SHOULD_ZONE_BE_REMOVED_DUE_TO_ENTITY_LINK = true." )
		RETURN FALSE
	ENDIF
	
	IF SHOULD_ZONE_BE_REMOVED_DUE_TO_AGGRO(iZone)
		PRINTLN( "[RCC MISSION] SHOULD_CREATE_RESTRICTION_ZONE [POP] - iZone: ", iZone, " SHOULD_ZONE_BE_REMOVED_DUE_TO_AGGRO = true.")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(iLocalZoneRemovedBitset, iZone)
	OR MC_ServerBD_4.iCurrentHighestPriority[ iTeam ] >= g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneEndPriority[iTeam]
		PRINTLN("[RCC MISSION] SHOULD_CREATE_RESTRICTION_ZONE [POP] - Zone ",iZone," has already been removed = ",IS_BIT_SET(iLocalZoneRemovedBitset, iZone),", or zone should be removed on priority ",g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneEndPriority[iTeam]," & we are past it on priority ",MC_ServerBD_4.iCurrentHighestPriority[ iTeam ])
		RETURN FALSE
	ENDIF
	
	INT iZoneStartPriority 	= g_FMMC_STRUCT_ENTITIES.sPlacedZones[ iZone ].iZoneStartPriority[ iTeam ] // '-2' is Off. '-1' is always On. >= 0 is a specific rule.
	INT iZoneEndPriority 	= g_FMMC_STRUCT_ENTITIES.sPlacedZones[ iZone ].iZoneEndPriority[ iTeam ] // '-2' is Off. '-1' is always On. >= 0 is a specific rule.
	INT iZoneDelay 			= g_FMMC_STRUCT_ENTITIES.sPlacedZones[ iZone ].iZoneDelay
	
	PRINTLN("[RCC MISSION][POP] SHOULD_CREATE_RESTRICTION_ZONE - for zone ", iZone, "  iZoneStartPriority is: ", iZoneStartPriority, "  iZoneEndPriority is: ", iZoneEndPriority, "  iTeam: ", iTeam, "  iZoneDelay: ", iZoneDelay)	
	
	IF( iZoneStartPriority = -2 ) // zone data not set
		PRINTLN("[RCC MISSION][POP] SHOULD_CREATE_RESTRICTION_ZONE - Return False, iZoneStartPriority is -2 (no data set) for ", iZone)
		RETURN FALSE
	ENDIF
			
	IF( iZoneStartPriority = -1 ) // create zone on mission start
		PRINTLN("[RCC MISSION][POP] SHOULD_CREATE_RESTRICTION_ZONE - Return TRUE, iZoneStartPriority is -1 (created on mission start) for ", iZone)
		RETURN TRUE
	ENDIF
	
	IF( iZoneDelays[ iZone ] <> 0 ) // ensure that once the delay timer has started we always catch it
		IF( iZoneDelays[ iZone ] < GET_GAME_TIMER() )	
			PRINTLN("[RCC MISSION][POP] SHOULD_CREATE_RESTRICTION_ZONE - Return True, iZoneDelays is less than game timer, for iZone: ", iZone)
			RETURN TRUE
		ELSE
			PRINTLN("[RCC MISSION][POP] SHOULD_CREATE_RESTRICTION_ZONE - Return False, iZonePendingBits Set for iZone: ", iZone)
			SET_BIT( iZonePendingBits, iZone )
			RETURN FALSE
		ENDIF
	ENDIF
	
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	
	IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO)
		IF SHOULD_WE_START_FROM_CHECKPOINT()
			INT iCheckPoint = GET_MISSION_STARTING_CHECKPOINT()
			IF iRule < g_FMMC_Struct.iRestartRule[iTeam][iCheckpoint]
				iRule = g_FMMC_Struct.iRestartRule[iTeam][iCheckpoint]
			ENDIF
		ENDIF
	ENDIF
	
	IF(	iZoneStartPriority >= 0 ) // we want to create the zone on a specific rule
		IF( iZoneStartPriority <= iRule ) // we're on or past the right rule
			PRINTLN("[RCC MISSION][POP] SHOULD_CREATE_RESTRICTION_ZONE [ML] we're on or past the right rule - iZoneStartPriority: ", iZoneStartPriority, " iRule: ", iRule)			
			IF( IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedZones[ iZone ].iZoneBS, ciFMMC_ZONEBS_ACTIVATE_ON_MIDPOINT ) ) // we want to create the zone after a midpoint
				IF( IS_BIT_SET( MC_serverBD.iObjectiveMidPointBitset[ iTeam ], iZoneStartPriority ) // we're past the midpoint
				OR iZoneStartPriority < iRule )
					IF (iRule <= iZoneEndPriority
					AND iZoneEndPriority >= 0)
					OR iZoneEndPriority = -1
						PRINTLN("[RCC MISSION][POP] SHOULD_CREATE_RESTRICTION_ZONE [ML] We're not yet past the zone deactivation rule (1)")
						IF( iZoneDelay > 0 ) // we want to create the zone after a midpoint and with a delay
							SET_BIT( iZonePendingBits, iZone )
							iZoneDelays[ iZone ] = GET_GAME_TIMER() + g_FMMC_STRUCT_ENTITIES.sPlacedZones[ iZone ].iZoneDelay
						ELSE // create zone on rule midpoint without delay
							RETURN TRUE
						ENDIF
					ELSE
						PRINTLN("[RCC MISSION][POP] SHOULD_CREATE_RESTRICTION_ZONE [ML] Not creating zone ", iZone, " as current rule is afer the zone deactivation rule (1)")
					ENDIF												
				ELSE
					SET_BIT( iZonePendingBits, iZone )
				ENDIF
			ELSE
				IF (iRule <= iZoneEndPriority
				AND iZoneEndPriority >= 0)
				OR iZoneEndPriority = -1
					PRINTLN("[RCC MISSION][POP] SHOULD_CREATE_RESTRICTION_ZONE [ML] We're not yet past the zone deactivation rule (2)")
					IF( iZoneDelay > 0 ) // create zone of rule start with delay
						SET_BIT( iZonePendingBits, iZone )
						iZoneDelays[ iZone ] = GET_GAME_TIMER() + g_FMMC_STRUCT_ENTITIES.sPlacedZones[ iZone ].iZoneDelay
						PRINTLN("[RCC MISSION][POP] SHOULD_CREATE_RESTRICTION_ZONE - for ", iZone, " iZoneDelays and iZonePendingBits has been set this frame.")
					ELSE // create zone on rule start without delay
						PRINTLN("[RCC MISSION][POP] SHOULD_CREATE_RESTRICTION_ZONE - Returning True for ", iZone, " Has no need for a delay.")
						RETURN TRUE
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION][POP] SHOULD_CREATE_RESTRICTION_ZONE [ML] Not creating zone ", iZone, " as current rule is afer the zone deactivation rule (2)")			
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	PRINTLN("[RCC MISSION][POP] SHOULD_CREATE_RESTRICTION_ZONE - Returning False for ", iZone, " End of Function")
	
	RETURN FALSE
ENDFUNC

PROC CREATE_RESTRICTION_ZONES(INT iLocalTeam)
	PRINTLN("[CREATE_RESTRICTION_ZONES] iLocalTeam = ", iLocalTeam)
	DEBUG_PRINTCALLSTACK()
	INT i
	
	VECTOR vZonePos0 , vZonePos1
	INT iTeamZ
	
	FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfZones-1)
		
		IF NOT IS_BIT_SET(iLocalZoneBitset, i)
		
			CLEAR_BIT( iZonePendingBits, i )
			
			CLEAR_BIT(iLocalZoneFailedCreationBitset, i)
			
			BOOL bShouldCreateZone = FALSE // B* 2233648
			IF( IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedZones[ i ].iZoneBS, ciFMMC_ZONEBS_APPLY_TO_ALL_TEAMS ) ) // the zone bit for all teams is set
				INT iTeam
				FOR iTeam = 0 TO ( g_FMMC_STRUCT.iNumberOfTeams - 1 )
					IF( SHOULD_CREATE_RESTRICTION_ZONE( i, iTeam ) )
						bShouldCreateZone = TRUE
						iTeam = g_FMMC_STRUCT.iNumberOfTeams
					ENDIF
				ENDFOR
			ELSE
				bShouldCreateZone = SHOULD_CREATE_RESTRICTION_ZONE( i, iLocalTeam )
			ENDIF
		
			IF( bShouldCreateZone )
				PRINTLN("CREATE_RESTRICTION_ZONES - Creating zone this frame iZone: ", i, " Team: ", iLocalTeam, " vPos: ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0])
				IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0])
				
					VECTOR vcenter = (g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0] + g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1])
					vcenter.x = vcenter.x/2
					vcenter.y = vcenter.y/2
					vcenter.z = vcenter.z/2
					
					FLOAT fClearRadius
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iZoneBS, ciFMMC_ZONEBS_DRAW_MARKER)
					OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iZoneBS, ciFMMC_ZONEBS_AREA_BLIP_ZONE)
						PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - Setting iLocalZoneCheckBitset for iZone: ",i, " Needs ever frame processing.")
						SET_BIT(iLocalZoneCheckBitset, i)
					ENDIF
					
					IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iZoneBS, ciFMMC_ZONEBS_DONT_CLEAR_AREA)
						
						VECTOR vClearAreaCoord
						
						IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iAreaType = ciFMMC_ZONE_SHAPE__AXIS_ALIGNED_BOX
						OR g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iAreaType = ciFMMC_ZONE_SHAPE__ANGLED_AREA
							vClearAreaCoord = vcenter
							fClearRadius = GET_DISTANCE_BETWEEN_COORDS(vcenter,g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0],FALSE)
							IF GET_DISTANCE_BETWEEN_COORDS(vcenter,g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1],FALSE) > fClearRadius
								fClearRadius = GET_DISTANCE_BETWEEN_COORDS(vcenter,g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1],FALSE)
							ENDIF
							// vcenter.z can be massive. Need to use a more sensible value for Clear_area coords (2220689)
							GET_GROUND_Z_FOR_3D_COORD(vcenter, vClearAreaCoord.z)
						ELIF g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iAreaType = ciFMMC_ZONE_SHAPE__SPHERE
							vClearAreaCoord = g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0]
							fClearRadius = g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].fRadius
						ENDIF
						
						IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
						OR CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE() //B*6210513
							CLEAR_AREA_LEAVE_VEHICLE_HEALTH(vClearAreaCoord,fClearRadius,TRUE)
							PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - CLEAR_AREA_LEAVE_VEHICLE_HEALTH vClearAreaCoord = ", vClearAreaCoord, " fClearRadius = ", fClearRadius, " zone = ", i)
						ELIF STRAND_MISSION_MPH_PAC_FIN_MCS1_CUT_JUST_FINISHED()//  2191127
							CLEAR_AREA_LEAVE_VEHICLE_HEALTH(vClearAreaCoord,fClearRadius,TRUE)
							PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - CLEAR_AREA_LEAVE_VEHICLE_HEALTH - STRAND_MISSION_MPH_PAC_FIN_MCS1_CUT_JUST_FINISHED vClearAreaCoord = ", vClearAreaCoord, " fClearRadius = ", fClearRadius, " zone = ", i)
						ELSE
							//-- Key code strand mission clear area done in fmmc_launcher.sc
						ENDIF
						
					ENDIF
					
					vZonePos0 = g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0]
					vZonePos1 = g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1]
					
					IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iAreaType != ciFMMC_ZONE_SHAPE__SPHERE
						vZonePos0.z = g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0].z - 100.0
						vZonePos1.z = g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1].z + 500.0
					ENDIF
					
					SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iType
						
						CASE ciFMMC_ZONE_TYPE__CLEAR_PEDS
							PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - iZone: ", i)
							PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - iPopMultiArea: ", iPopMultiArea[i])
							IF iPopMultiArea[i] = -1
								iPopMultiArea[i] = ADD_POP_MULTIPLIER_AREA(vZonePos0, vZonePos1, 0, MC_serverBD.fVehicleDensity, TRUE, FALSE)
							ENDIF
							IF NOT DOES_SCENARIO_BLOCKING_AREA_EXISTS(vZonePos0, vZonePos1)
								IF bipopScenarioArea[i] = NULL
									bipopScenarioArea[i] = ADD_SCENARIO_BLOCKING_AREA(vZonePos0, vZonePos1,FALSE,TRUE,TRUE,FALSE)
								ENDIF
							ELSE
								PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - ADD_SCENARIO_BLOCKING_AREA - ciFMMC_ZONE_TYPE__CLEAR_PEDS - failed - already exists")
							ENDIF
							PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - turning off peds in area: ",i," pos 0: ",vZonePos0," pos 1: ",vZonePos1)  
							PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - area been given id: ",iPopMultiArea[i]) 							
						BREAK
						
						CASE ciFMMC_ZONE_TYPE__CLEAR_VEHICLES
							PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - iZone: ", i)
							PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - iPopMultiArea: ", iPopMultiArea[i])
							IF iPopMultiArea[i] = -1
								iPopMultiArea[i] = ADD_POP_MULTIPLIER_AREA(vZonePos0, vZonePos1, MC_serverBD.fPedDensity, 0, TRUE, FALSE)
							ENDIF
							IF NOT DOES_SCENARIO_BLOCKING_AREA_EXISTS(vZonePos0, vZonePos1)
								IF bipopScenarioArea[i] = NULL
									bipopScenarioArea[i] = ADD_SCENARIO_BLOCKING_AREA(vZonePos0, vZonePos1,FALSE,TRUE,FALSE,TRUE)
								ENDIF
							ELSE
								PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - ADD_SCENARIO_BLOCKING_AREA - ciFMMC_ZONE_TYPE__CLEAR_VEHICLES - failed - already exists")
							ENDIF
							
							SET_VEHICLE_GENERATORS_AND_ROADS_INACTIVE_FOR_ZONE_LEGACY(i, TRUE)
							
							PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - turning off vehs in area: ",i," pos 0: ",vZonePos0," pos 1: ",vZonePos1)
							PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - area been given id: ", iPopMultiArea[i])
						BREAK
						
						CASE ciFMMC_ZONE_TYPE__CLEAR_ALL // Peds + Vehicles
							PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - iZone: ", i)
							PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - iPopMultiArea: ", iPopMultiArea[i])								
							IF iPopMultiArea[i] = -1
								iPopMultiArea[i] = ADD_POP_MULTIPLIER_AREA(vZonePos0, vZonePos1, 0, 0, TRUE, FALSE)
							ENDIF
							IF NOT DOES_SCENARIO_BLOCKING_AREA_EXISTS(vZonePos0, vZonePos1)
								IF bipopScenarioArea[i] = NULL
									bipopScenarioArea[i] = ADD_SCENARIO_BLOCKING_AREA(vZonePos0, vZonePos1)
								ENDIF									
							ELSE
								PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - ADD_SCENARIO_BLOCKING_AREA - ciFMMC_ZONE_TYPE__CLEAR_ALL - failed - already exists")
							ENDIF
							
							SET_VEHICLE_GENERATORS_AND_ROADS_INACTIVE_FOR_ZONE_LEGACY(i, TRUE)
							
							//CLEAR_ANGLED_AREA_OF_VEHICLES(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0],g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1],15)
							
							PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - turning off vehs and peds in area: ",i," pos 0: ",vZonePos0, " pos 1: ",vZonePos1)
							PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - area been given id: ",iPopMultiArea[i])
						BREAK
						
						CASE ciFMMC_ZONE_TYPE__CLEAR_ALL_AND_PV // Player Vehicle
							SET_PERSONAL_VEHICLE_NO_SPAWN_BOX(vZonePos0, vZonePos1)
						BREAK
						
						CASE ciFMMC_ZONE_TYPE__CLEAR_COVER
							IF NOT IS_BIT_SET(iBitsetCoverBlockingArea, i)
								ADD_COVER_BLOCKING_AREA(vZonePos0, vZonePos1,FALSE,FALSE,TRUE,TRUE)
								PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - turning off cover in area: ",i," pos 0: ",vZonePos0," pos 1: ",vZonePos1)
								SET_BIT(iBitsetCoverBlockingArea, i)
							ENDIF
						BREAK
						
						CASE ciFMMC_ZONE_TYPE__FIRE_ROCKETS_AT_PLAYER
						CASE ciFMMC_ZONE_TYPE__MENU_CLEAR
						CASE ciFMMC_ZONE_TYPE__BLOCK_WANTED_LEVEL
						CASE ciFMMC_ZONE_TYPE__BLOCK_JUMPING_AND_CLIMBING
						CASE ciFMMC_ZONE_TYPE__BLOCK_RUNNING
						CASE ciFMMC_ZONE_TYPE__BLOCK_WEAPONS
						CASE ciFMMC_ZONE_TYPE__AUTO_PARACHUTE
						CASE ciFMMC_ZONE_TYPE__BLIP_PLAYER
						CASE ciFMMC_ZONE_TYPE__ALERT_PEDS
						CASE ciFMMC_ZONE_TYPE__SET_AIR_DRAG_MULTIPLIER
						CASE ciFMMC_ZONE_TYPE__PLAYER_CANT_EXIT_VEHICLE
						CASE ciFMMC_ZONE_TYPE__OBJECT_RESPAWN
						CASE ciFMMC_ZONE_TYPE__REFILL_SPECIAL_ABILITY
						CASE ciFMMC_ZONE_TYPE__OBJECT_RESPAWN_WITHIN
						CASE ciFMMC_ZONE_TYPE__BLOCK_VTOL_TOGGLE
						CASE ciFMMC_ZONE_TYPE__BLOCK_PARACHUTE
						CASE ciFMMC_ZONE_TYPE__BLOCK_ELECTROCUTION
						CASE ciFMMC_ZONE_TYPE__PLAYER_CANT_DRIVE_VEHICLE
						CASE ciFMMC_ZONE_TYPE__PLAYER_VEHICLE_MAX_SPEED
						CASE ciFMMC_ZONE_TYPE__BLOCK_PLAYER_CONTROL
						CASE ciFMMC_ZONE_TYPE__KILL_ZONE
						CASE ciFMMC_ZONE_TYPE__STUNT_SCORE
						CASE ciFMMC_ZONE_TYPE__RETURN_OBJECT
						CASE ciFMMC_ZONE_TYPE__EXPLODE_PLAYER
						CASE ciFMMC_ZONE_TYPE__HIDE_OBJECTS_SEARCH_AREA
						CASE ciFMMC_ZONE_TYPE__BLANK
						CASE ciFMMC_ZONE_TYPE__AGGRO_PEDS_IN_THIS_ZONE
						CASE ciFMMC_ZONE_TYPE__HEALTH_DRAIN
						CASE ciFMMC_ZONE_TYPE__METAL_DETECTOR
						CASE ciFMMC_ZONE_TYPE__PLAYER_TRIGGERED_EXPLOSION_DANGER_AREA
						CASE ciFMMC_ZONE_TYPE__CLEAR_ENTITY
							SET_BIT(iLocalZoneCheckBitset, i)
							PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - Every frame check zone w iType ",g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iType," in area: ",i," pos 0: ",g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0]," pos 1: ",g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1])
						BREAK
						
						CASE ciFMMC_ZONE_TYPE__GOAL_AREA
							SET_BIT(iLocalZoneCheckBitset, i)
							PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - Every frame check zone w iType ",g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iType," in area: ",i," pos 0: ",g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0]," pos 1: ",g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1])
							VECTOR vMiddle
							vMiddle = GET_CENTER_OF_AREA(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0], g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1])
							IF vMiddle.x > 2800.0
								iGoalA = i
								PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - ciFMMC_ZONE_TYPE__GOAL_AREA - Zone ", i ," is Goal A")
							ELSE
								iGoalB = i
								PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - ciFMMC_ZONE_TYPE__GOAL_AREA - Zone ", i ," is Goal B")
							ENDIF
						BREAK
						
						CASE ciFMMC_ZONE_TYPE__NO_PLAYER_SPAWNING
							IF iSpawnOcclusionIndex[i] = -1
								SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iAreaType
									CASE ciFMMC_ZONE_SHAPE__AXIS_ALIGNED_BOX
										iSpawnOcclusionIndex[i] = ADD_MISSION_SPAWN_OCCLUSION_BOX(vZonePos0, vZonePos1)
										PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - added axis-aligned no_player_spawning zone ",i," between ",g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0]," and ",g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1])
									BREAK
									CASE ciFMMC_ZONE_SHAPE__SPHERE
										iSpawnOcclusionIndex[i] = ADD_MISSION_SPAWN_OCCLUSION_SPHERE(vZonePos0, g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].fRadius)
										PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - added spherical no_player_spawning zone ",i," centered at ",vZonePos0," w radius ",g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].fRadius)
									BREAK
									CASE ciFMMC_ZONE_SHAPE__ANGLED_AREA
										iSpawnOcclusionIndex[i] = ADD_MISSION_SPAWN_OCCLUSION_ANGLED_AREA(vZonePos0, vZonePos1, g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].fRadius)
										PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - added angled area no_player_spawning zone ",i,", coords 1 ",vZonePos0," coords 2 ",vZonePos1," width ",g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].fRadius)
									BREAK
								ENDSWITCH
							ENDIF
						BREAK

						CASE ciFMMC_ZONE_TYPE__WATER_DAMPENING
							IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].fZoneValue != 1.0
								IF iZoneWaterCalmingQuadID[i] = -1
									INT iQuad
									iQuad = GET_FREE_WATER_CALMING_QUAD()
									
									IF iQuad > -1
									AND iQuad < FMMC_MAX_WATER_CALMING_QUADS
									AND iWaterCalmingQuad[iQuad] = -1
										iWaterCalmingQuad[iQuad] = ADD_EXTRA_CALMING_QUAD(vZonePos0.x,vZonePos0.y,vZonePos1.x,vZonePos1.y,g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].fZoneValue)
										iZoneWaterCalmingQuadID[i] = iQuad
										PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - Added water calming quad ",i," as iQuad ",iQuad)
									ELSE
										CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION] CREATE_RESTRICTION_ZONES - Trying to create too many water dampening zones/quads! There is a maximum of 8.")
										PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - Trying to create too many water dampening zones/quads! There is a maximum of 8.")
									ENDIF
								ENDIF
							ENDIF
							
							IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].fZoneValue2 != 1.0
								SET_BIT(iLocalZoneCheckBitset, i)
								PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - Every frame check zone w iType ",g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iType," in area: ",i," pos 0: ",g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0]," pos 1: ",g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1])
							ENDIF
							
						BREAK
						
						CASE ciFMMC_ZONE_TYPE__GPS
							PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - Trying to add GPS disabled zone.")
							PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - At ZonePos0: ", vZonePos0)
							PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - At ZonePos1: ", vZonePos1)
							
							IF iGPSDisabledZones[i] = -1
								INT iZone
								iZone = GET_NEXT_GPS_DISABLED_ZONE_INDEX()
								IF iZone != -1
									PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - GPS_DISABLED_ZONE SET_GPS_DISABLED_ZONE_AT_INDEX called")
									SET_GPS_DISABLED_ZONE_AT_INDEX(vZonePos0,vZonePos1,iZone)
									iGPSDisabledZones[i] = iZone								
								ELSE
									SET_BIT(iLocalZoneFailedCreationBitset, i)
									PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - GPS_DISABLED_ZONE No free GPS disabled zone slots for zone ", i)
									CASSERTLN(DEBUG_CONTROLLER,"CREATE_RESTRICTION_ZONES - GPS_DISABLED_ZONE No GPS disabled zones available.")
								ENDIF
								
								PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - GPS_DISABLED_ZONE GET_NEXT_GPS_DISABLED_ZONE_INDEX index: ", iZone)
							ENDIF
						BREAK
						
						CASE ciFMMC_ZONE_TYPE__DISPATCH_SPAWN_BLOCK
							PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - Trying to add Block Dispatch zone.")
							IF iDispatchBlockZone[i] = -1
								SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iAreaType
									CASE ciFMMC_ZONE_SHAPE__AXIS_ALIGNED_BOX
										VECTOR v1, v2
										FLOAT fWidth
										fWidth = g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1].x - g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0].x
										
										v1 = vZonePos0 + <<fWidth/2, 0, 0>>
										v2 = vZonePos1 - <<fWidth/2, 0, 0>>
										
										PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - Trying to add Dispatch axis-aligned disabled zone. vPos[0] = ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0], " vPos[1] = ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1],"; converted: v1 ",v1," v2 ",v2," fWidth ",fWidth)
										iDispatchBlockZone[i] = ADD_DISPATCH_SPAWN_ANGLED_BLOCKING_AREA(v1, v2, fWidth)
									BREAK
									CASE ciFMMC_ZONE_SHAPE__SPHERE
										PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - Adding Block Dispatch Spawn Sphere")
										iDispatchBlockZone[i] = ADD_DISPATCH_SPAWN_SPHERE_BLOCKING_AREA(vZonePos0, g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].fRadius)
									BREAK
									CASE ciFMMC_ZONE_SHAPE__ANGLED_AREA
										PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - Adding Block Dispatch Spawn Area")
										iDispatchBlockZone[i] = ADD_DISPATCH_SPAWN_ANGLED_BLOCKING_AREA(vZonePos0, vZonePos1, g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].fRadius)
									BREAK
								ENDSWITCH
							ENDIF
							
							PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - area been given id: ",iDispatchBlockZone[i])
						BREAK
						
						CASE ciFMMC_ZONE_TYPE__FORCE_OBJECT
							SET_BIT(iLocalZoneCheckBitset, i)
							PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - Added Force Object Creation zone number: ",i," pos 0: ",g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0]," radius: ",g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].fRadius)
						BREAK
						
						CASE ciFMMC_ZONE_TYPE__EXPLOSION_AREA
							SET_BIT(iLocalBoolCheck3, LBOOL3_EXPLOSIVE_ZONE_ACTIVE)
						BREAK
						
						CASE ciFMMC_ZONE_TYPE__SPAWN_PROTECTION
							IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_EVERYONE_RUNNING)
							AND NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
								PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - ciFMMC_ZONE_TYPE__SPAWN_PROTECTION: ",i)
								iTeamZ = ROUND(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].fZoneValue)
								
								IF iTeamZ > -1
								AND iTeamZ < FMMC_MAX_TEAMS
									IF MC_serverBD.iNumStartingPlayers[iTeamZ] > 0
										IF iAirDefenseSphereMission[i] = -1
											SET_BIT(iLocalZoneCheckBitset, i)
											IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_STOCKPILE(g_FMMC_STRUCT.iAdversaryModeType)
												IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iAreaType = ciFMMC_ZONE_SHAPE__SPHERE
													iAirDefenseSphereMission[i] = CREATE_AIR_DEFENCE_SPHERE(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0], g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].fRadius, g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0], WEAPONTYPE_INVALID)
													
													IF ROUND(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].fZoneValue) = MC_PlayerBD[iLocalPart].iteam
													AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TRAP_DOOR(g_FMMC_STRUCT.iAdversaryModeType)
														SET_PLAYER_TARGETTABLE_FOR_AIR_DEFENCE_SPHERE(NATIVE_TO_INT(LocalPlayer), iAirDefenseSphereMission[i], FALSE)
													ELSE
														SET_PLAYER_TARGETTABLE_FOR_AIR_DEFENCE_SPHERE(NATIVE_TO_INT(LocalPlayer), iAirDefenseSphereMission[i], TRUE)
													ENDIF
												ELSE
													VECTOR vMidPoint
													vMidPoint = (g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0] + g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1])
													vMidPoint = <<vMidPoint.x / 2, vMidPoint.y / 2, vMidPoint.z / 2>>
													iAirDefenseSphereMission[i] = CREATE_AIR_DEFENCE_ANGLED_AREA(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0], g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1], g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].fRadius, vMidPoint, WEAPONTYPE_INVALID)
													
													IF ROUND(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].fZoneValue) = MC_PlayerBD[iLocalPart].iteam
													AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TRAP_DOOR(g_FMMC_STRUCT.iAdversaryModeType)
														SET_PLAYER_TARGETTABLE_FOR_AIR_DEFENCE_SPHERE(NATIVE_TO_INT(LocalPlayer), iAirDefenseSphereMission[i], FALSE)
													ELSE
														SET_PLAYER_TARGETTABLE_FOR_AIR_DEFENCE_SPHERE(NATIVE_TO_INT(LocalPlayer), iAirDefenseSphereMission[i], TRUE)
													ENDIF
												ENDIF
											ENDIF
											// So we don't play the sound of entering the zone...
											IF IS_PED_IN_RESTRICTION_ZONE(LocalPlayerPed, i, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iZoneBS, ciFMMC_ZONEBS_IncludeZAxisInAreaCheck), fZoneGrowth[i])
											AND IS_ZONE_TRIGGERABLE(i)
												SET_BIT(iLocalBoolCheckZone, LBOOLZONE_PLAYER_JUST_LEFT_DEFENSE_SPHERE + i)
											ENDIF
										ENDIF
										PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - Added Spawn Protection zone number: ",i)
									ELSE
										PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - iTeam: ", iTeamZ, " is not active, therefore we will not create the Zone.")
									ENDIF
								ELSE
									PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - iTeam: ", iTeamZ, " Bad value assigned in the creator: g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].fZoneValue: ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].fZoneValue, " i = ", i)
								ENDIF
							ELSE
								SET_BIT(iZonePendingBits, i)
							ENDIF
						BREAK
						
						CASE ciFMMC_ZONE_TYPE__ACTIVATE_ROADS
							SET_ROADS_IN_AREA(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0],g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1], TRUE, FALSE )
							PRINTLN("[RCC MISSION] CREATE_RESTRICTION_ZONES - Setting roads in area to true: ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0]," - ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1])
						BREAK
						
						CASE ciFMMC_ZONE_TYPE__NAV_BLOCKING
							VECTOR vZoneMiddle, vZoneSize
							FLOAT fHeading
							vZonePos0 = g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0]
							vZonePos1 = g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1]
							vZoneMiddle = (vZonePos0 + vZonePos1)
							vZoneMiddle.x = vZoneMiddle.x/2
							vZoneMiddle.y = vZoneMiddle.y/2
							vZoneMiddle.z = vZoneMiddle.z/2
							vZoneMiddle.z = PICK_FLOAT(vZonePos0.z < vZonePos1.z, vZonePos0.z, vZonePos1.z)
							
							fHeading = GET_HEADING_BETWEEN_VECTORS(vZonePos1, vZonePos0)
							fHeading += 90.0
							
							vZoneSize =  vZonePos1 - vZonePos0
							vZoneSize.x = ABSF(vZoneSize.x)
							vZoneSize.y = g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].fRadius
							vZoneSize.z = ABSF(vZoneSize.z)
							
							PRINTLN("[RCC MISSION] CREATE_RESTRICTION_ZONES - vZoneMiddle: ",vZoneMiddle,", Size: ", vZoneSize, " fHeading: ", fHeading)
							iNavBlockerZone[i] = ADD_NAVMESH_BLOCKING_OBJECT(vZoneMiddle, vZoneSize, DEG_TO_RAD(fHeading))
							PRINTLN("[RCC MISSION] CREATE_RESTRICTION_ZONES - Zone ",i, " Creating ciFMMC_ZONE_TYPE__NAV_BLOCKING")
						BREAK
												
						CASE ciFMMC_ZONE_TYPE__BLOCK_ROAD_NODES
							SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iAreaType
								CASE ciFMMC_ZONE_SHAPE__ANGLED_AREA
									PRINTLN("[RCC MISSION] CREATE_THIS_ZONE - ciFMMC_ZONE_TYPE__BLOCK_ROAD_NODES | Calling SET_ROADS_IN_ANGLED_AREA with vPos[0]: ",g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0],", vPos[1]: ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1], " fRadius: ",  g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].fRadius)
									SET_ROADS_IN_ANGLED_AREA(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0], g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1], g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].fRadius, FALSE, FALSE, FALSE)
								BREAK
								
								CASE ciFMMC_ZONE_SHAPE__AXIS_ALIGNED_BOX
									PRINTLN("[RCC MISSION] CREATE_THIS_ZONE - ciFMMC_ZONE_TYPE__BLOCK_ROAD_NODES | Calling SET_ROADS_IN_AREA with vPos[0]: ",g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0],", vPos[1]: ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1])
									SET_ROADS_IN_AREA(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0], g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1], FALSE, FALSE)
								BREAK
							ENDSWITCH
						BREAK						
						
					ENDSWITCH
				ENDIF
				
				IF IS_BIT_SET(iZonePendingBits, i)
				AND g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iType = ciFMMC_ZONE_TYPE__SPAWN_PROTECTION
					RENDER_SPAWN_PROTECTION_SPHERES_EARLY()
					PRINTLN("[RCC MISSION][POP] CREATE_RESTRICTION_ZONES - ciFMMC_ZONE_TYPE__SPAWN_PROTECTION Delaying creation until everyone is ready for data reasons i: ",i)
				ELSE
					IF NOT IS_BIT_SET(iLocalZoneFailedCreationBitset, i)
						SET_BIT(iLocalZoneBitset, i)
					ELSE
						PRINTLN("[ML] iLocalZoneFailedCreationBitset is SET. This means there is no free slot until a zone is first deleted. Not setting iLocalZoneBitset so that we can loop in here again")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	//Mission specific helicopter blocking - Only to fix issues in released content.
	//Blocking zones should be added in the creator if the mission is in development.
	VECTOR vHeliBlocking0 = <<518.4, -27.5, 90.5>>
	VECTOR vHeliBlocking1 = <<614.5, 22.7, 106.7>>
	IF iCurrentHeistMissionIndex = HBCA_BS_PACIFIC_STANDARD_FINALE
	OR iCurrentHeistMissionIndex = HBCA_BS_PACIFIC_STANDARD_FINALE2
		vHeliBlocking0 = <<-950.38202, -1615.80859,-1.5>>
		vHeliBlocking1 = <<-640.13721, -1379.09839, 125>>
		IF sbiPacificHeliBlocker = NULL
			IF NOT DOES_SCENARIO_BLOCKING_AREA_EXISTS(vHeliBlocking0, vHeliBlocking1)
				sbiPacificHeliBlocker = ADD_SCENARIO_BLOCKING_AREA(vHeliBlocking0, vHeliBlocking1)
				PRINTLN("[JS] Adding pacific standard helicopter blocking area")
			ENDIF
		ENDIF
	ENDIF
	IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE()
		vHeliBlocking0 = <<400.0, -100.0, -1.5>>
		vHeliBlocking1 = <<700.0, 100, 200>>
		IF sbiCasinoHeistHeliBlocker = NULL
			IF NOT DOES_SCENARIO_BLOCKING_AREA_EXISTS(vHeliBlocking0, vHeliBlocking1)
				PRINTLN("[JS] - Adding Casino Heist blocking area")
				sbiCasinoHeistHeliBlocker = ADD_SCENARIO_BLOCKING_AREA(vHeliBlocking0, vHeliBlocking1)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_METAL_DETECTOR_FUNCTIONAL(INT iZone)

	IF IS_BIT_SET(iMetalDetectorZoneDisabledBitset, iZone)
		PRINTLN("[MetalDetector] IS_METAL_DETECTOR_FUNCTIONAL - Returning FALSE due to iMetalDetectorZoneDisabledBitset")
		RETURN FALSE
	ENDIF
	
	IF IS_PHONE_EMP_CURRENTLY_ACTIVE()
		PRINTLN("[MetalDetector][PhoneEMP] IS_METAL_DETECTOR_FUNCTIONAL - Returning FALSE due to IS_PHONE_EMP_CURRENTLY_ACTIVE")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_METAL_DETECTOR_AFFECTED_BY_EMP)
	AND IS_PHONE_EMP_TIMER_RUNNING()
		PRINTLN("[MetalDetector] IS_METAL_DETECTOR_FUNCTIONAL - Returning FALSE due to Phone EMP")
		RETURN FALSE
	ENDIF
		
	RETURN TRUE
ENDFUNC

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: PROCESS RESTRICTION ZONES (calls to create and remove zones when needed, processes logic which needs to run regularly on active zones) !
//
//************************************************************************************************************************************************************

FUNC BOOL IS_PLAYER_IN_CORRECT_VEHICLE_TYPE_FOR_AIR_RESTRICTION_ZONE(INT iZone)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_AirRestrictionRocketsFireOnAllPlayers)
		// This is an air restriction zone, only fire on air vehicles
		IF IS_PED_IN_ANY_HELI(LocalPlayerPed)
		OR IS_PED_IN_ANY_PLANE(LocalPlayerPed)
			IF IS_PLAYER_IN_VEH_SEAT(LocalPlayer, VS_DRIVER)
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		// Fire at everything!
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			// Only fire at the driver of the vehicle, don't want loads of rockets all firing on the same place
			IF IS_PLAYER_IN_VEH_SEAT(LocalPlayer, VS_DRIVER)
				RETURN TRUE
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_ZONE_RADIUS_CHANGES(INT iEveryFrameZoneLoop)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].fGrowthPercent <> 0.0
		FLOAT fGrowthPercent = (g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].fGrowthPercent*0.01)
		// Ready?
		BOOL bReady
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iZoneBS, ciFMMC_ZONEBS_START_ACTIVATION_ON_PLAYER_ENTRY)
		OR IS_BIT_SET(iZoneBS_PlayerPedEntered, iEveryFrameZoneLoop)
			bReady = TRUE
		ELSE
			IF IS_PED_IN_RESTRICTION_ZONE(LocalPlayerPed, iEveryFrameZoneLoop, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iZoneBS, ciFMMC_ZONEBS_IncludeZAxisInAreaCheck), fZoneGrowth[iEveryFrameZoneLoop])
			AND IS_ZONE_TRIGGERABLE(iEveryFrameZoneLoop)
				BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedContentEventType_ActivateZoneRadiusChange, DEFAULT, DEFAULT, DEFAULt, DEFAULT, DEFAULT, iEveryFrameZoneLoop)
				SET_BIT(iZoneBS_PlayerPedEntered, iEveryFrameZoneLoop)
			ENDIF
		ENDIF
		
		// Init for lerping.
		IF NOT HAS_NET_TIMER_STARTED(td_ZoneRadiusOverrideTimer[iEveryFrameZoneLoop])
		AND bReady 
			START_NET_TIMER(td_ZoneRadiusOverrideTimer[iEveryFrameZoneLoop])
			fZoneRadiusOverride[iEveryFrameZoneLoop] = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].fRadius
		ENDIF
		
		// Lerpy time.
		IF HAS_NET_TIMER_EXPIRED_READ_ONLY(td_ZoneRadiusOverrideTimer[iEveryFrameZoneLoop], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iRadiusSecondaryActivationTimer*1000)
		AND HAS_NET_TIMER_STARTED(td_ZoneRadiusOverrideTimer[iEveryFrameZoneLoop])
			// Radius and Width.
			FLOAT fDiff = (ABSF(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].fRadius - (g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].fRadius*fGrowthPercent)) * 1000)						
			FLOAT fDiffThisFrame = (fDiff / (g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iRadiusSecondaryInterpTime * 1000)) * GET_FRAME_TIME()			
			IF (g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].fRadius*fGrowthPercent) > g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].fRadius
				IF fZoneRadiusOverride[iEveryFrameZoneLoop] < (g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].fRadius*fGrowthPercent)
					fZoneRadiusOverride[iEveryFrameZoneLoop] += fDiffThisFrame					
					IF fZoneRadiusOverride[iEveryFrameZoneLoop] > (g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].fRadius*fGrowthPercent)
						fZoneRadiusOverride[iEveryFrameZoneLoop] = (g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].fRadius*fGrowthPercent)
					ENDIF
				ELSE
					fZoneRadiusOverride[iEveryFrameZoneLoop] = (g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].fRadius*fGrowthPercent)
				ENDIF
			ELSE
				IF fZoneRadiusOverride[iEveryFrameZoneLoop] > (g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].fRadius*fGrowthPercent)
					fZoneRadiusOverride[iEveryFrameZoneLoop] -= fDiffThisFrame
					IF fZoneRadiusOverride[iEveryFrameZoneLoop] < (g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].fRadius*fGrowthPercent)
						fZoneRadiusOverride[iEveryFrameZoneLoop] = (g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].fRadius*fGrowthPercent)
					ENDIF
				ELSE
					fZoneRadiusOverride[iEveryFrameZoneLoop] = (g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].fRadius*fGrowthPercent)
				ENDIF
			ENDIF
			
			// Areas
			FLOAT fDiffGrow = (ABSF(1.0 - (1.0*fGrowthPercent)) * 1000)
			FLOAT fDiffGrowThisFrame = (fDiffGrow / (g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iRadiusSecondaryInterpTime * 1000)) * GET_FRAME_TIME()	
			IF fGrowthPercent > 1.0
				IF (fZoneGrowth[iEveryFrameZoneLoop] + fDiffGrowThisFrame) < fGrowthPercent
					fZoneGrowth[iEveryFrameZoneLoop] += fDiffGrowThisFrame
					IF fZoneGrowth[iEveryFrameZoneLoop] > fGrowthPercent
						fZoneGrowth[iEveryFrameZoneLoop] = fGrowthPercent
					ENDIF
				ELSE
					fZoneGrowth[iEveryFrameZoneLoop] = fGrowthPercent
				ENDIF
			ELSE
				IF (fZoneGrowth[iEveryFrameZoneLoop] + fDiffGrowThisFrame) > fGrowthPercent
					fZoneGrowth[iEveryFrameZoneLoop] -= fDiffGrowThisFrame
					IF fZoneGrowth[iEveryFrameZoneLoop] < fGrowthPercent
						fZoneGrowth[iEveryFrameZoneLoop] = fGrowthPercent
					ENDIF
				ELSE
					fZoneGrowth[iEveryFrameZoneLoop] = fGrowthPercent
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_ZONE_BLIPS(INT iEveryFrameZoneLoop)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iZoneBS, ciFMMC_ZONEBS_AREA_BLIP_ZONE)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iZoneBS, ciFMMC_ZONEBS_DRAW_MARKER)
		HUD_COLOURS hudCol = HUD_COLOUR_GREEN
		INT r, g, b, a
		GET_HUD_COLOUR(hudCol, r, g, b, a)
		
		VECTOR vPos1 = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].vPos[0]
		VECTOR vPos2 = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].vPos[1]
		VECTOR vCentre
		vCentre = (vPos2 + vPos1)
		vCentre.x /= 2
		vCentre.y /= 2
		vCentre.z /= 2				
		IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iAreaType = ciFMMC_ZONE_SHAPE__ANGLED_AREA
		OR g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iAreaType = ciFMMC_ZONE_SHAPE__AXIS_ALIGNED_BOX
	 		FLOAT fDistance = GET_DISTANCE_BETWEEN_COORDS(vPos1, vCentre, FALSE)				
			VECTOR vDirection1 = vPos1 - vCentre
			VECTOR vDirection2 = vPos2 - vCentre			
			vDirection1 = NORMALISE_VECTOR(vDirection1)
			vDirection2 = NORMALISE_VECTOR(vDirection2)
			
			vPos1.x = vPos1.x + vDirection1.x*(fDistance*(-1.0+fZoneGrowth[iEveryFrameZoneLoop]))
			vPos1.y = vPos1.y + vDirection1.y*(fDistance*(-1.0+fZoneGrowth[iEveryFrameZoneLoop]))
			
			vPos2.x = vPos2.x + vDirection2.x*(fDistance*(-1.0+fZoneGrowth[iEveryFrameZoneLoop]))
			vPos2.y = vPos2.y + vDirection2.y*(fDistance*(-1.0+fZoneGrowth[iEveryFrameZoneLoop]))
		ENDIF		
		FLOAT fRadius = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].fRadius*fZoneGrowth[iEveryFrameZoneLoop]
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iAreaType = ciFMMC_ZONE_SHAPE__SPHERE 
		OR g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iAreaType = ciFMMC_ZONE_SHAPE__CYLINDER
			IF IS_BIT_SET(iLocalZoneCheckBitset, iEveryFrameZoneLoop)
				IF NOT DOES_BLIP_EXIST(biZoneBlips[iEveryFrameZoneLoop])
				AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iZoneBS, ciFMMC_ZONEBS_AREA_BLIP_ZONE)
					biZoneBlips[iEveryFrameZoneLoop] = ADD_BLIP_FOR_RADIUS(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].vPos[0], fRadius)
					SET_BLIP_SCALE(biZoneBlips[iEveryFrameZoneLoop], fRadius)
					SET_BLIP_COLOUR(biZoneBlips[iEveryFrameZoneLoop], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iBlipColor)
					SET_BLIP_ALPHA(biZoneBlips[iEveryFrameZoneLoop], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iBlipAlpha)
				ELSE
					SET_BLIP_SCALE(biZoneBlips[iEveryFrameZoneLoop], fRadius)
				ENDIF
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iZoneBS, ciFMMC_ZONEBS_DRAW_MARKER)
					IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iAreaType = ciFMMC_ZONE_SHAPE__SPHERE 
						DRAW_MARKER(MARKER_SPHERE, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].vPos[0], <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<fRadius, fRadius, fRadius>>, r, g, b, 40)
						DRAW_MARKER(MARKER_CYLINDER, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].vPos[0], <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<fRadius*2, fRadius*2, fRadius/8>>, r, g, b, 145)
						DRAW_MARKER(MARKER_CYLINDER, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].vPos[0], <<0.0, 0.0, 0.0>>, <<0.0, 180.0, 0.0>>, <<fRadius*2, fRadius*2, fRadius/8>>, r, g, b, 145)
					ELIF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iAreaType = ciFMMC_ZONE_SHAPE__CYLINDER						
						DRAW_MARKER(MARKER_CYLINDER, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].vPos[0], <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<fRadius*2, fRadius*2, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].fHeight>>, r, g, b, 40)
						DRAW_MARKER(MARKER_CYLINDER, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].vPos[0], <<0.0, 0.0, 0.0>>, <<0.0, 180.0, 0.0>>, <<fRadius*2, fRadius*2, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].fHeight>>, r, g, b, 40)
						DRAW_MARKER(MARKER_CYLINDER, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].vPos[0], <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<fRadius*2, fRadius*2, fRadius/8>>, r, g, b, 135)
						DRAW_MARKER(MARKER_CYLINDER, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].vPos[0], <<0.0, 0.0, 0.0>>, <<0.0, 180.0, 0.0>>, <<fRadius*2, fRadius*2, fRadius/8>>, r, g, b, 135)
					ENDIF
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(biZoneBlips[iEveryFrameZoneLoop])					
					REMOVE_BLIP(biZoneBlips[iEveryFrameZoneLoop])
				ENDIF
			ENDIF
		ELIF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iAreaType = ciFMMC_ZONE_SHAPE__ANGLED_AREA 		
			IF IS_BIT_SET(iLocalZoneCheckBitset, iEveryFrameZoneLoop)
				FLOAT length = GET_DISTANCE_BETWEEN_COORDS(vPos2, vPos1, FALSE)
				FLOAT angle = GET_HEADING_FROM_VECTOR_2D(vPos2.x - vPos1.x, vPos2.y - vPos1.y)
				
				IF NOT DOES_BLIP_EXIST(biZoneBlips[iEveryFrameZoneLoop])
				AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iZoneBS, ciFMMC_ZONEBS_AREA_BLIP_ZONE)
					biZoneBlips[iEveryFrameZoneLoop] = ADD_BLIP_FOR_AREA_FROM_EDGES(vPos1, vPos2, fRadius)
					SET_BLIP_COLOUR(biZoneBlips[iEveryFrameZoneLoop], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iBlipColor)
					SET_BLIP_ALPHA(biZoneBlips[iEveryFrameZoneLoop], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iBlipAlpha)
				ELIF DOES_BLIP_EXIST(biZoneBlips[iEveryFrameZoneLoop])
					SET_BLIP_SCALE_2D(biZoneBlips[iEveryFrameZoneLoop], fRadius, length)
				ENDIF
				
				FLOAT fPosZ = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].vPos[0].z
				IF vPos2.z < vPos1.z
					fPosZ = vPos2.z
				ENDIF
				fPosZ -= 0.1
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iZoneBS, ciFMMC_ZONEBS_DRAW_MARKER)
					DRAW_MARKER(MARKER_BOXES, (<<vCentre.x, vCentre.y, fPosZ>>), (<<0.0, 0.0, 0.0>>), (<<0.0, 0.0, angle>>), (<<ABSF(fRadius), ABSF(length), 8.5>>), r, g, b, 80)
					//DRAW_MARKER(MARKER_BOXES, (<<vCentre.x, vCentre.y, fPosZ>>), (<<0.0, 0.0, 0.0>>), (<<0.0, 180, angle>>), (<<ABSF(fRadius), ABSF(length), 8.5>>), r, g, b, 80)
					DRAW_MARKER(MARKER_BOXES, (<<vCentre.x, vCentre.y, fPosZ>>), (<<0.0, 0.0, 0.0>>), (<<0.0, 0.0, angle>>), (<<ABSF(fRadius*1.005), ABSF(length*1.005), 1.5>>), r, g, b, 120)
					//DRAW_MARKER(MARKER_BOXES, (<<vCentre.x, vCentre.y, fPosZ>>), (<<0.0, 0.0, 0.0>>), (<<0.0, 180, angle>>), (<<ABSF(fRadius*1.005), ABSF(length*1.005), 1.5>>), r, g, b, 120)
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(biZoneBlips[iEveryFrameZoneLoop])
					REMOVE_BLIP(biZoneBlips[iEveryFrameZoneLoop])
				ENDIF
			ENDIF
		ELIF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iAreaType = ciFMMC_ZONE_SHAPE__AXIS_ALIGNED_BOX
			IF IS_BIT_SET(iLocalZoneCheckBitset, iEveryFrameZoneLoop)
				FLOAT fScaleX, fScaleY
				
				IF vPos1.x < vPos2.x
					fScaleX = vCentre.x - vPos2.x
				ELIF vPos1.x > vPos2.x
					fScaleX = vCentre.x - vPos1.x
				ENDIF
				IF vPos1.y < vPos2.y
					fScaleY = vCentre.y - vPos2.y
				ELIF vPos1.y > vPos2.y
					fScaleY = vCentre.y - vPos1.y
				ENDIF
				
				fScaleX *= 2
				fScaleY *= 2
				
				IF NOT DOES_BLIP_EXIST(biZoneBlips[iEveryFrameZoneLoop])
				AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iZoneBS, ciFMMC_ZONEBS_AREA_BLIP_ZONE)
					biZoneBlips[iEveryFrameZoneLoop] = ADD_BLIP_FOR_AREA(vCentre, ABSF(fScaleX), ABSF(fScaleY))
					SET_BLIP_ROTATION(biZoneBlips[iEveryFrameZoneLoop], 0)
					SET_BLIP_COLOUR(biZoneBlips[iEveryFrameZoneLoop], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iBlipColor)
					SET_BLIP_ALPHA(biZoneBlips[iEveryFrameZoneLoop], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iBlipAlpha)
				ELIF DOES_BLIP_EXIST(biZoneBlips[iEveryFrameZoneLoop])
					SET_BLIP_SCALE_2D(biZoneBlips[iEveryFrameZoneLoop], ABSF(fScaleX), ABSF(fScaleY))
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iZoneBS, ciFMMC_ZONEBS_DRAW_MARKER)
					FLOAT fPosZ = vPos1.z
					IF vPos2.z < vPos1.z
						fPosZ = vPos2.z
					ENDIF
					fPosZ += 0.4
					DRAW_MARKER(MARKER_BOXES, <<vCentre.x, vCentre.y, fPosZ>>, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<ABSF(fScaleX), ABSF(fScaleY), 8.5>>, r, g, b, 80)
					DRAW_MARKER(MARKER_BOXES, <<vCentre.x, vCentre.y, fPosZ>>, <<0.0, 0.0, 0.0>>, <<0.0, 180.0, 0.0>>, <<ABSF(fScaleX), ABSF(fScaleY), 8.5>>, r, g, b, 80)
					DRAW_MARKER(MARKER_BOXES, <<vCentre.x, vCentre.y, fPosZ>>, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<ABSF(fScaleX*1.005), ABSF(fScaleY*1.005), 1.65>>, r, g, b, 130)
					DRAW_MARKER(MARKER_BOXES, <<vCentre.x, vCentre.y, fPosZ>>, <<0.0, 0.0, 0.0>>, <<0.0, 180.0, 0.0>>, <<ABSF(fScaleX*1.005), ABSF(fScaleY)*1.005, 1.65>>, r, g, b, 130)
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(biZoneBlips[iEveryFrameZoneLoop])					
					REMOVE_BLIP(biZoneBlips[iEveryFrameZoneLoop])
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_PLAYERS_IN_ZONE(INT iZone)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_LEGACY_ZoneTriggerCheckAllPlayers)
		EXIT
	ENDIF
	IF NOT bIsLocalPlayerHost
		EXIT
	ENDIF
	
	INT iPlayersMax = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]
	INT iZonePlayersInside = 0
	INT iPlayersChecked = 0
	INT iPart = 0
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		
		PARTICIPANT_INDEX piPart = INT_TO_PARTICIPANTINDEX(iPart)
		
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(piPart)
		OR IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR) 
		OR IS_BIT_SET(MC_playerBD[iPart].iClientBitSet2, PBBOOL_HEIST_SPECTATOR)
			EXIT
		ENDIF
				
		PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)
		PED_INDEX pedPlayer = GET_PLAYER_PED(piPlayer)
				
		IF IS_PED_INJURED(pedPlayer)
			EXIT
		ENDIF
		
		IF IS_PED_IN_RESTRICTION_ZONE(pedPlayer, iZone, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_IncludeZAxisInAreaCheck), fZoneGrowth[iZone])
			PRINTLN("[LM][PROCESS_PLAYERS_IN_ZONE] - iZone: ", iZone, " iPart: ", iPart, " is inside the zone.")
			iZonePlayersInside++
		ENDIF
		
		IF iZonePlayersInside > 0
			IF NOT IS_BIT_SET(iZoneBS_PlayersInside[iZone], ciZoneBS_PlayersInside_1 + (iZonePlayersInside-1))
				SET_BIT(iZoneBS_PlayersInside[iZone], ciZoneBS_PlayersInside_1 + (iZonePlayersInside-1))
				PRINTLN("[LM][PROCESS_PLAYERS_IN_ZONE] - iZone: ", iZone, " iZonePlayersInside: ", iZonePlayersInside," Setting ciZoneBS_PlayersInside_", iZonePlayersInside)
			ENDIF
			IF iZonePlayersInside = iPlayersMax
				IF NOT IS_BIT_SET(iZoneBS_PlayersInside[iZone], ciZoneBS_PlayersInside_All)
					SET_BIT(iZoneBS_PlayersInside[iZone], ciZoneBS_PlayersInside_All)
					PRINTLN("[LM][PROCESS_PLAYERS_IN_ZONE] - iZone: ", iZone, " iZonePlayersInside: ", iZonePlayersInside," Setting ciZoneBS_PlayersInside_All.")
				ENDIF
			ENDIF
		ENDIF
		
		iPlayersChecked++
		PRINTLN("[LM][PROCESS_PLAYERS_IN_ZONE] - iZone: ", iZone, " playersMax: ", iPlayersMax, " iPlayersChecked: ", iPlayersChecked, " iZonePlayersInside: ", iZonePlayersInside)
		IF iPlayersChecked > iPlayersMax
			BREAKLOOP
		ENDIF
	ENDFOR
ENDPROC

FUNC BOOL SHOULD_ZONE_BE_DISABLED_THIS_FRAME(INT iZone, INT iTeam)
	
	#IF IS_DEBUG_BUILD
	IF bZoneTimerDebug
		TEXT_LABEL_63 tlDebugText3D = "Z"
		tlDebugText3D += iZone
		
		IF HAS_ZONE_TIMER_COMPLETED(iZone)
			tlDebugText3D += " | Completed"
		ELSE
			IF NOT HAS_ZONE_TIMER_EXPIRED(iZone)
				tlDebugText3D += " | Time: "
				tlDebugText3D += g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneTimer_EnableTime - GET_ZONE_TIMER_ELAPSED_TIME(iZone)
			ENDIF
		ENDIF
		
		DRAW_DEBUG_TEXT(tlDebugText3D, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0])
	ENDIF
	#ENDIF
	
	UNUSED_PARAMETER(iTeam)

	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_ENABLE_BASED_ON_ZONE_TIMER)
		IF HAS_ZONE_TIMER_COMPLETED(iZone)
			PRINTLN("[ZONETIMER_SPAM] SHOULD_ZONE_BE_DISABLED_THIS_FRAME | Zone ", iZone, "'s zone timer has been completed")
		ELSE
			PRINTLN("[ZONETIMER_SPAM] SHOULD_ZONE_BE_DISABLED_THIS_FRAME | Disabling zone ", iZone, " this frame due to ciFMMC_ZONEBS_ENABLE_BASED_ON_ZONE_TIMER")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_RESTRICTION_ZONES()

	IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iPartToUse].iteam)
	OR iZonePendingBits <> 0
		CREATE_RESTRICTION_ZONES(MC_playerBD[iPartToUse].iteam)
	ENDIF
	IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iPartToUse].iteam)
		PROCESS_RESTRICTION_ZONE_MID_MISSION_REMOVAL()
	ENDIF
	
	//Staggered checks for stuff like rocket launcher zones/GPS zones:
	IF (iLocalZoneCheckBitset > 0)
	
		INT iEveryFrameZoneLoop
		FOR iEveryFrameZoneLoop = 0 TO (MAX_ZONE_COUNT_TO_LOOP_TO() - 1)
			
			PROCESS_ZONE_TIMER(iEveryFrameZoneLoop)
			
			#IF IS_DEBUG_BUILD
			DRAW_ZONE_DEBUG(iEveryFrameZoneLoop)
			#ENDIF
			
			BOOL bShouldRemove = SHOULD_ZONE_BE_REMOVED_DUE_TO_ENTITY_LINK(iEveryFrameZoneLoop)
			
			IF !bShouldRemove
			AND SHOULD_ZONE_BE_REMOVED_DUE_TO_AGGRO(iEveryFrameZoneLoop)
				bShouldRemove = TRUE
			ENDIF
			
			IF bShouldRemove
				PROCESS_RESTRICTION_ZONE_MID_MISSION_REMOVAL(bShouldRemove)
			ENDIF
			
			PROCESS_ZONE_BLIPS(iEveryFrameZoneLoop)
						
			IF IS_BIT_SET(iLocalZoneCheckBitset, iEveryFrameZoneLoop)
			AND NOT SHOULD_ZONE_BE_DISABLED_THIS_FRAME(iEveryFrameZoneLoop, MC_playerBD[iPartToUse].iteam)
			
				PROCESS_ZONE_RADIUS_CHANGES(iEveryFrameZoneLoop)
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iType = ciFMMC_ZONE_TYPE__SPAWN_PROTECTION
					
					BOOL bProcessSpawnProtectionZone = TRUE
					
					IF ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
						IF NOT IS_BIT_SET(MC_ServerBD.iServerBitSet6, SBBOOL6_SUDDEN_DEATH_STOCKPILE)
							bProcessSpawnProtectionZone = FALSE
						ENDIF
					ENDIF					
					
					IF bProcessSpawnProtectionZone
					AND NOT g_bCelebrationScreenIsActive
						// Doing this early.
						IF MC_playerBD[iLocalPart].iGameState != GAME_STATE_MISSION_OVER
						AND MC_playerBD[iLocalPart].iGameState != GAME_STATE_LEAVE
						AND MC_playerBD[iLocalPart].iGameState != GAME_STATE_END
						AND GET_SKYSWOOP_STAGE() = SKYSWOOP_NONE
						AND NOT IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitset, PBBOOL_ON_LEADERBOARD)
							INT iR = 0, iG = 0, iB = 0, iA = 0
							HUD_COLOURS tempColour
							tempColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(ROUND(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].fZoneValue), LocalPlayer)
							
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iZoneBS, ciFMMC_ZONEBS_APPLY_TO_ALL_TEAMS)
								tempColour = HUD_COLOUR_RED
							ENDIF
							
							GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(tempColour), iR, iG, iB, iA)
							iA = 100
							
							IF IS_PLAYER_DEAD(LocalPlayer)
								tempColour = HUD_COLOUR_GREYLIGHT
								GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(tempColour), iR, iG, iB, iA)
								iA = 100
							ENDIF
							
							BOOL bDrawArea = TRUE
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iZoneBS, ciFMMC_ZONEBS_ONLY_SHOW_WITH_PICKUP)
								IF MC_playerBD[iPartToUse].iObjCarryCount = 0
								OR NOT IS_PED_CARRYING_ANY_OBJECTS(LocalPlayerPed)
									bDrawArea = FALSE
								ENDIF
								IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iTeamBitSet3, ciBS3_ENABLE_TEAM_VEHICLE_ALLOW_VEHICLE_SEAT_SWAPPING)
									IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()) != -1
										IF MC_playerBD[GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER())].iObjCarryCount != 0
											bDrawArea = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							IF g_bMissionEnding
							AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
								bDrawArea = FALSE
							ENDIF
							
							IF bDrawArea
								IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iAreaType = ciFMMC_ZONE_SHAPE__SPHERE
									DRAW_MARKER(MARKER_SPHERE, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].vPos[0], <<0,0,0>>, <<0,0,0>>, <<g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].fRadius,g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].fRadius,g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].fRadius>>, iR, iG, iB, iA)
								ELSE
									DRAW_ANGLED_AREA_FROM_FACES(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].vPos[0], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].vPos[1], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].fRadius, iR, iG, iB, iA)
								ENDIF
							ENDIF
						ENDIF

						IF (iSpectatorTarget = -1)

							IF IS_PED_IN_RESTRICTION_ZONE(LocalPlayerPed, iEveryFrameZoneLoop, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iZoneBS, ciFMMC_ZONEBS_IncludeZAxisInAreaCheck), fZoneGrowth[iEveryFrameZoneLoop])
							AND IS_ZONE_TRIGGERABLE(iEveryFrameZoneLoop)
								BOOL bBlockActions = TRUE
								PRINTLN("[LM][PROCESS_RESTRICTION_ZONES] - Inside of an Air Defense Zone. Blocking Inputs (1)")
								IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_STOCKPILE(g_FMMC_STRUCT.iAdversaryModeType)
									bBlockActions = FALSE
								ENDIF
								
								IF bBlockActions
									DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)	
									DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_UD)
									DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_LR)
									DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT)
									DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV)
									DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
									DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
									
									DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK, TRUE)
									DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM, TRUE)
									
									DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MELEE_LEFT)
									
									IF NOT IS_PED_INJURED(localPlayerPed)
										SET_PED_RESET_FLAG(LocalPlayerPed, PRF_BlockWeaponFire, TRUE)
									ENDIF
									
									SET_PED_CONFIG_FLAG(localPlayerPed, PCF_DontActivateRagdollFromExplosions, TRUE)
									SET_PED_CONFIG_FLAG(localPlayerPed, PCF_DontActivateRagdollFromFire, TRUE)
									SET_PED_CONFIG_FLAG(localPlayerPed, PCF_DontActivateRagdollFromElectrocution, TRUE)
									
									WEAPON_TYPE wt = WEAPONTYPE_INVALID
									GET_CURRENT_PED_WEAPON(LocalPlayerPed, wt)
								
									IF wt != WEAPONTYPE_UNARMED
										DISABLE_PLAYER_THROW_GRENADE_WHILE_USING_GUN()
										DISABLE_PLAYER_FIRING(localPlayer, TRUE)										
									ENDIF
									
									IF wt = WEAPONTYPE_GRENADE
										SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED, TRUE)
									ENDIF
									
									DISABLE_VEHICLE_MINES(TRUE)
								ENDIF								
								
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iZoneBS, ciFMMC_ZONEBS_AD_INVINCIBILITY)
								AND (ROUND(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].fZoneValue) = MC_PlayerBD[iLocalPart].iteam OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iZoneBS, ciFMMC_ZONEBS_APPLY_TO_ALL_TEAMS))
									IF NOT IS_PED_INJURED(localPlayerPed)
										IF NOT IS_ENTITY_A_GHOST(localPlayerPed)
											VEHICULAR_VENDETTA_SET_LOCAL_PLAYER_AS_GHOST(TRUE)
										ENDIF
										
										IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType) //Keeping it in a check incase it will have any effect on old content.
											IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
												IF IS_ENTITY_ALIVE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
													SET_ENTITY_ALPHA(GET_VEHICLE_PED_IS_IN(LocalPlayerPed), 50, TRUE)
												ENDIF
											ENDIF
										ENDIF
										IF NOT HAS_NET_TIMER_STARTED(tdDefenseSphereAlphaTimer)
										OR (HAS_NET_TIMER_STARTED(tdDefenseSphereAlphaTimer) AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdDefenseSphereAlphaTimer, (g_FMMC_STRUCT.iSpawnProtectionDuration*100)))
											RESET_NET_TIMER(tdDefenseSphereAlphaTimer)
											START_NET_TIMER(tdDefenseSphereAlphaTimer)
											IF (NOT IS_PED_FALLING(localPlayerPed) OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TRAP_DOOR(g_FMMC_STRUCT.iAdversaryModeType))
												BROADCAST_FMMC_SPAWN_PROTECTION_ALPHA(TRUE, iLocalPart, GET_FRAME_COUNT())
											ENDIF
										ENDIF
										
										IF GET_ENTITY_HEALTH(localPlayerPed) > 0
											SET_ENTITY_INVINCIBLE(localPlayerPed, TRUE)
										ENDIF
										IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
											IF IS_ENTITY_ALIVE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
												SET_ENTITY_INVINCIBLE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed), TRUE)
											ENDIF
										ENDIF
									ENDIF
									
									RESET_NET_TIMER(tdDefenseSphereInvulnerabilityTimer)
									START_NET_TIMER(tdDefenseSphereInvulnerabilityTimer)
								ENDIF
								
								IF NOT IS_BIT_SET(iLocalBoolCheckZone, LBOOLZONE_PLAYER_JUST_LEFT_DEFENSE_SPHERE + iEveryFrameZoneLoop)
									PRINTLN("[LM][PROCESS_RESTRICTION_ZONES] - Inside of an Air Defense Zone. Setting Bit")
									// So we can process a period of temporary invincibility when we leave.
									SET_BIT(iLocalBoolCheckZone, LBOOLZONE_PLAYER_JUST_LEFT_DEFENSE_SPHERE + iEveryFrameZoneLoop)
									IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_LAND_GRAB_SOUNDS)
										IF NOT IS_PED_INJURED(localPlayerPed)
											PLAY_SOUND_FRONTEND(-1, "Weapon_Disabled", "DLC_SR_LG_Player_Sounds")
										ENDIF
									ENDIF
								ENDIF								
							ELSE
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iZoneBS, ciFMMC_ZONEBS_AD_INVINCIBILITY)
								AND(ROUND(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].fZoneValue) = MC_PlayerBD[iLocalPart].iteam OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iZoneBS, ciFMMC_ZONEBS_APPLY_TO_ALL_TEAMS))
									IF IS_BIT_SET(iLocalBoolCheckZone, LBOOLZONE_PLAYER_JUST_LEFT_DEFENSE_SPHERE + iEveryFrameZoneLoop)

										PRINTLN("[LM][PROCESS_RESTRICTION_ZONES] - Starting Ghost and Invincibility.")
										
										RESET_NET_TIMER(tdDefenseSphereInvulnerabilityTimer)
										START_NET_TIMER(tdDefenseSphereInvulnerabilityTimer)
										
										IF NOT IS_PED_INJURED(localPlayerPed)
											IF NOT IS_ENTITY_A_GHOST(localPlayerPed)
												IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet,SBBOOL_MISSION_OVER)
												AND (NOT IS_PED_FALLING(localPlayerPed) OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TRAP_DOOR(g_FMMC_STRUCT.iAdversaryModeType))
													VEHICULAR_VENDETTA_SET_LOCAL_PLAYER_AS_GHOST(TRUE)
													BROADCAST_FMMC_SPAWN_PROTECTION_ALPHA(TRUE, iLocalPart, GET_FRAME_COUNT())
													DISABLE_VEHICLE_MINES(FALSE)
												ENDIF
											ENDIF
										ENDIF
										
										IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
											IF IS_ENTITY_ALIVE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
												SET_ENTITY_INVINCIBLE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed), FALSE)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								IF IS_BIT_SET(iLocalBoolCheckZone, LBOOLZONE_PLAYER_JUST_LEFT_DEFENSE_SPHERE + iEveryFrameZoneLoop)
									CLEAR_BIT(iLocalBoolCheckZone, LBOOLZONE_PLAYER_JUST_LEFT_DEFENSE_SPHERE + iEveryFrameZoneLoop)
									PRINTLN("[LM][PROCESS_RESTRICTION_ZONES] - Now Outside of an Air Defense Zone. Clearing Bit")		
									IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_LAND_GRAB_SOUNDS)
										IF NOT IS_PED_INJURED(localPlayerPed)
											PLAY_SOUND_FRONTEND(-1, "Weapon_Enabled", "DLC_SR_LG_Player_Sounds")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF NOT g_bCelebrationScreenIsActive
							IF NOT IS_PED_INJURED(localPlayerPed)
							AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
								SET_ENTITY_INVINCIBLE(localPlayerPed, FALSE)
								VEHICULAR_VENDETTA_SET_LOCAL_PLAYER_AS_GHOST(FALSE)
								
								IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
									IF IS_ENTITY_ALIVE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
										IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType) //Keeping it in a check incase it will have any effect on old content.
											SET_ENTITY_ALPHA(GET_VEHICLE_PED_IS_IN(LocalPlayerPed), 255, TRUE)
										ENDIF
										SET_ENTITY_INVINCIBLE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed), FALSE)
									ENDIF
								ENDIF
							ENDIF
							IF iAirDefenseSphereMission[iEveryFrameZoneLoop] != -1
								IF DOES_AIR_DEFENCE_SPHERE_EXIST(iAirDefenseSphereMission[iEveryFrameZoneLoop])
									PRINTLN("[LM][PROCESS_RESTRICTION_ZONES] - Removing air defence sphere.")
									REMOVE_AIR_DEFENCE_SPHERE(iAirDefenseSphereMission[iEveryFrameZoneLoop])
								ENDIF
							ENDIF
							
							// We only need to call this once.
							IF NOT IS_BIT_SET(iLocalboolCheck25, LBOOL25_SUDDEN_DEATH_SPAWN_PROTECTION_DISABLED)
							AND (NOT IS_PED_FALLING(localPlayerPed) OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TRAP_DOOR(g_FMMC_STRUCT.iAdversaryModeType))
								BROADCAST_FMMC_SPAWN_PROTECTION_ALPHA(FALSE, iLocalPart, GET_FRAME_COUNT())
								SET_BIT(iLocalboolCheck25, LBOOL25_SUDDEN_DEATH_SPAWN_PROTECTION_DISABLED)
							ENDIF
						ENDIF
					ENDIF
				ELIF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iType = ciFMMC_ZONE_TYPE__BLOCK_PARACHUTE
					IF IS_BIT_SET(iWaveDampingZoneBitset, iEveryFrameZoneLoop)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PARACHUTE_DEPLOY)
					ENDIF
					
				ELIF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iType = ciFMMC_ZONE_TYPE__BLOCK_JUMPING_AND_CLIMBING
				AND SHOULD_PROCESS_BLOCK_CLIMBING_ZONES_EVERY_FRAME()
					PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES (EVERY-FRAME) Checking the blocked jumping zone (", iEveryFrameZoneLoop, ")")
					
					IF (iSpectatorTarget = -1)
					AND IS_PED_IN_RESTRICTION_ZONE(LocalPlayerPed, iEveryFrameZoneLoop, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iZoneBS, ciFMMC_ZONEBS_IncludeZAxisInAreaCheck), fZoneGrowth[iEveryFrameZoneLoop])
					AND IS_ZONE_TRIGGERABLE(iEveryFrameZoneLoop)
						PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES (EVERY-FRAME) Player in Blocked Jumping/Climbing zone - Blocking jump action (", iEveryFrameZoneLoop, ")")
						bInBlockedJumpAndClimbZone = TRUE
						IF NOT IS_BIT_SET(iBlockJumpClimbZoneBitset, iEveryFrameZoneLoop)
							SET_BIT(iBlockJumpClimbZoneBitset, iEveryFrameZoneLoop)
						ENDIF
					ELSE
						IF IS_BIT_SET(iBlockJumpClimbZoneBitset, iEveryFrameZoneLoop)
							PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES (EVERY-FRAME) Player has left Blocked Jumping/Climbing zone ", iEveryFrameZoneLoop, " - Re-allowing jump action")
							bInBlockedJumpAndClimbZone = FALSE
							CLEAR_BIT(iBlockJumpClimbZoneBitset, iEveryFrameZoneLoop)
						ENDIF
					ENDIF
					
				ELIF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iType = ciFMMC_ZONE_TYPE__KILL_ZONE
					
					IF IS_PED_IN_RESTRICTION_ZONE(LocalPlayerPed, iEveryFrameZoneLoop, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iZoneBS, ciFMMC_ZONEBS_IncludeZAxisInAreaCheck), fZoneGrowth[iEveryFrameZoneLoop])
					AND IS_ZONE_TRIGGERABLE(iEveryFrameZoneLoop)
						IF NOT g_bMissionEnding
						AND IS_SCREEN_FADED_IN()
						AND NOT IS_PED_INJURED(LocalPlayerPed)
						AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
							IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
								VEHICLE_INDEX myVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
								NETWORK_EXPLODE_VEHICLE(myVeh, TRUE)
								PRINTLN("[PROCESS_RESTRICTION_ZONES] ciFMMC_ZONE_TYPE__KILL_ZONE - Exploding player's vehicle")
							ENDIF
							
							IF NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
								SET_ENTITY_HEALTH(LocalPlayerPed, 0)
							ENDIF
							
							PRINTLN("[PROCESS_RESTRICTION_ZONES] ciFMMC_ZONE_TYPE__KILL_ZONE - Killing player")
						ELSE
							PRINTLN("[PROCESS_RESTRICTION_ZONES] ciFMMC_ZONE_TYPE__KILL_ZONE - Not killing player even though they're in the zone")
						ENDIF
					ENDIF

				ELIF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iType =  ciFMMC_ZONE_TYPE__BLOCK_PLAYER_CONTROL
					IF IS_PED_IN_RESTRICTION_ZONE(LocalPlayerPed, iEveryFrameZoneLoop, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iZoneBS, ciFMMC_ZONEBS_IncludeZAxisInAreaCheck), fZoneGrowth[iEveryFrameZoneLoop])
					AND IS_ZONE_TRIGGERABLE(iEveryFrameZoneLoop)
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iZoneBS, ciFMMC_ZONEBS_BlockPlayerControl_InputContext)						
							PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES ciFMMC_ZONE_TYPE__BLOCK_PLAYER_CONTROL Calling: DISABLE_CONTROL_ACTION > INPUT_CONTEXT")
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CONTEXT, TRUE)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT, TRUE)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_TRANSFORM, TRUE)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ROOF, TRUE)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_BOMB_BAY, TRUE)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_COUNTER, TRUE)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CELLPHONE_RIGHT, TRUE)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SHUFFLE, TRUE)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GRAPPLING_HOOK, TRUE)
							
							// Special actions so we don't get trapped in these states.
							IF NOT IS_PED_INJURED(localPlayerPed)
								IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
									VEHICLE_INDEX vehIndex = GET_VEHICLE_PED_IS_IN(localPlayerPed)
									IF GET_ENTITY_MODEL(vehIndex) = STROMBERG
									AND IS_VEHICLE_IN_SUBMARINE_MODE(vehIndex)
										PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES ciFMMC_ZONE_TYPE__BLOCK_PLAYER_CONTROL Calling: TRANSFORM_TO_CAR")
										TRANSFORM_TO_CAR(vehIndex)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ELIF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iType = ciFMMC_ZONE_TYPE__GOAL_AREA
					//All in BMBFB_PROCESS_BALL
					
				ELIF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iType = ciFMMC_ZONE_TYPE__METAL_DETECTOR
					PRINTLN("[RCC MISSION][MetalDetector] ciFMMC_ZONE_TYPE__METAL_DETECTOR - Hitting Metal Detector Zone checks for zone ", iEveryFrameZoneLoop)
					
					IF (iSpectatorTarget = -1)
					AND IS_METAL_DETECTOR_FUNCTIONAL(iEveryFrameZoneLoop)
					AND IS_PED_IN_RESTRICTION_ZONE(LocalPlayerPed, iEveryFrameZoneLoop, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iZoneBS, ciFMMC_ZONEBS_IncludeZAxisInAreaCheck), fZoneGrowth[iEveryFrameZoneLoop])
					AND IS_ZONE_TRIGGERABLE(iEveryFrameZoneLoop)
						PRINTLN("[RCC MISSION][MetalDetector] ciFMMC_ZONE_TYPE__METAL_DETECTOR - Player in Metal Detector zone ", iEveryFrameZoneLoop)
						
						IF iCurrentMetalDetectorZone != iEveryFrameZoneLoop
						
							BROADCAST_FMMC_METAL_DETECTOR_ALERTED(iEveryFrameZoneLoop, iLocalPart, GET_ENTITY_COORDS(LocalPlayerPed), IS_PLAYER_HOLDING_METAL_DETECTOR_EXTREME_WEAPON())
							SET_METAL_DETECTOR_AS_ALERTED(iEveryFrameZoneLoop, GET_ENTITY_COORDS(LocalPlayerPed), IS_PLAYER_HOLDING_METAL_DETECTOR_EXTREME_WEAPON())
							
							iCurrentMetalDetectorZone = iEveryFrameZoneLoop
							PRINTLN("[RCC MISSION][MetalDetector] ciFMMC_ZONE_TYPE__METAL_DETECTOR - SET_METAL_DETECTOR_AS_ALERTED for zone: ", iEveryFrameZoneLoop)
						ENDIF
					ELSE
						IF iCurrentMetalDetectorZone = iEveryFrameZoneLoop
							iCurrentMetalDetectorZone = -1
							PRINTLN("[RCC MISSION][MetalDetector] ciFMMC_ZONE_TYPE__METAL_DETECTOR - Clearing iCurrentMetalDetectorZone for zone: ", iEveryFrameZoneLoop)
						ENDIF
					ENDIF
					
				ELIF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iType = ciFMMC_ZONE_TYPE__STUNT_SCORE
					IF IS_PED_IN_RESTRICTION_ZONE(LocalPlayerPed, iEveryFrameZoneLoop, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iZoneBS, ciFMMC_ZONEBS_IncludeZAxisInAreaCheck), fZoneGrowth[iEveryFrameZoneLoop])
					AND IS_ZONE_TRIGGERABLE(iEveryFrameZoneLoop)
						IF NOT IS_BIT_SET(iStuntScoreBitset, iEveryFrameZoneLoop)
							HANDLE_STUNT_RUN_POINTS(100, eStuntTrick_Hoop)
							SET_BIT(iStuntScoreBitset, iEveryFrameZoneLoop)
						ENDIF
					ELSE
						CLEAR_BIT(iStuntScoreBitset, iEveryFrameZoneLoop)
					ENDIF
				ELIF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iType = ciFMMC_ZONE_TYPE__EXPLODE_PLAYER
					IF NOT IS_PED_INJURED(LocalPlayerPed)
					AND IS_PED_IN_RESTRICTION_ZONE(LocalPlayerPed, iEveryFrameZoneLoop, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iZoneBS, ciFMMC_ZONEBS_IncludeZAxisInAreaCheck), fZoneGrowth[iEveryFrameZoneLoop])
					AND NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
					AND NOT IS_PLAYER_SCTV(LocalPlayer)
					AND IS_ZONE_TRIGGERABLE(iEveryFrameZoneLoop)
						PROCESS_PLAYER_EXPLODE_ZONE(tdExplodePlayerZoneTimer[iEveryFrameZoneLoop], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].fZoneValue)
					ELSE
						IF HAS_NET_TIMER_STARTED(tdExplodePlayerZoneTimer[iEveryFrameZoneLoop])
							CLEAR_ALL_BIG_MESSAGES()
							RESET_NET_TIMER(tdExplodePlayerZoneTimer[iEveryFrameZoneLoop])
						ENDIF
					ENDIF
				ENDIF
				
				// Generic "In Zone" Functionality.
				IF IS_BIT_SET(iLocalZoneCheckBitset, iEveryFrameZoneLoop)
					IF IS_PED_IN_RESTRICTION_ZONE(LocalPlayerPed, iEveryFrameZoneLoop, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iZoneBS, ciFMMC_ZONEBS_IncludeZAxisInAreaCheck), fZoneGrowth[iEveryFrameZoneLoop])
					AND IS_ZONE_TRIGGERABLE(iEveryFrameZoneLoop)
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iZoneBS, ciFMMC_ZONEBS_TRIGGER_ZONE_REMOTELY)
						AND g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iEntityLinkType = ciENTITY_TYPE_ZONES
						AND g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iEntityLinkIndex > -1
							IF NOT IS_BIT_SET(iZoneBS_ActivatedRemotely, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iEntityLinkIndex)
								PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES - Zone Entity Link Index: ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iEntityLinkIndex, " Is now being activated remotely FROM iZone: ", iEveryFrameZoneLoop)
								SET_BIT(iZoneBS_ActivatedRemotely, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iEntityLinkIndex)
							ENDIF
						ENDIF
					ELSE
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iZoneBS, ciFMMC_ZONEBS_TRIGGER_ZONE_REMOTELY)
						AND g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iEntityLinkType = ciENTITY_TYPE_ZONES
						AND g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iEntityLinkIndex > -1
							IF IS_BIT_SET(iZoneBS_ActivatedRemotely, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iEntityLinkIndex)
								PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES - Zone Entity Link Index: ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iEntityLinkIndex, " Is now being de-activated remotely FROM iZone: ", iEveryFrameZoneLoop)
								CLEAR_BIT(iZoneBS_ActivatedRemotely, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iEntityLinkIndex)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		
	
		IF iZoneStaggeredLoop >= FMMC_MAX_NUM_ZONES
			
			iZoneStaggeredLoop = 0
			
			IF NOT IS_BIT_SET(iLocalBoolCheck4, LBOOL4_INSIDE_RESTRICTED_AIRSPACE)
				IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_RESTRICTION_ZONE_ROCKETS_REQUIRED)
					PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES - No longer in any restricted airspace, clearing PBBOOL_RESTRICTION_ZONE_ROCKETS_REQUIRED & resetting rocket timer")
					RESET_NET_TIMER(tdRocketTimer)
					CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_RESTRICTION_ZONE_ROCKETS_REQUIRED)
				ENDIF
			ENDIF
			
			CLEAR_BIT(iLocalBoolCheck4, LBOOL4_INSIDE_RESTRICTED_AIRSPACE)
			
			IF (IS_BIT_SET(iLocalBoolCheck13, LBOOL13_IN_BLOCK_WANTED_LEVEL_ZONE) OR IS_BIT_SET(iLocalBoolCheck13, LBOOL13_IN_BLOCK_AND_CLEAR_WANTED_LEVEL_ZONE))
			AND NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_IN_BLOCK_WANTED_LEVEL_ZONE_TEMP)
				
				INT iMaxWanted = 5
				
				IF MC_serverBD.iPolice > 1
				AND GET_POLICE_MAX_WANTED_FROM_MENU_SETTING(MC_serverBD.iPolice) > 0
					iMaxWanted = GET_POLICE_MAX_WANTED_FROM_MENU_SETTING(MC_serverBD.iPolice)
				ENDIF
				
				PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES - No longer in a block wanted level zone, setting max wanted level back to ",iMaxWanted)
				SET_MAX_WANTED_LEVEL(iMaxWanted)
				
				CLEAR_BIT(iLocalBoolCheck13, LBOOL13_IN_BLOCK_WANTED_LEVEL_ZONE)
				CLEAR_BIT(iLocalBoolCheck13, LBOOL13_IN_BLOCK_AND_CLEAR_WANTED_LEVEL_ZONE)
			ENDIF
			
			CLEAR_BIT(iLocalBoolCheck13, LBOOL13_IN_BLOCK_WANTED_LEVEL_ZONE_TEMP)
			
		ENDIF
		
		IF IS_BIT_SET(iLocalZoneCheckBitset, iZoneStaggeredLoop)
		AND NOT SHOULD_ZONE_BE_DISABLED_THIS_FRAME(iZoneStaggeredLoop, MC_playerBD[iPartToUse].iteam)
		
			INT iObj
			OBJECT_INDEX tempObj
			
			SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].iType
				CASE ciFMMC_ZONE_TYPE__FIRE_ROCKETS_AT_PLAYER
					IF (iSpectatorTarget = -1)
					AND IS_PLAYER_IN_CORRECT_VEHICLE_TYPE_FOR_AIR_RESTRICTION_ZONE(iZoneStaggeredLoop)
					AND IS_PED_IN_RESTRICTION_ZONE(LocalPlayerPed, iZoneStaggeredLoop, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].iZoneBS, ciFMMC_ZONEBS_IncludeZAxisInAreaCheck), fZoneGrowth[iZoneStaggeredLoop])
					AND (GET_ENTITY_HEIGHT_ABOVE_GROUND(LocalPlayerPed) < 400)
					AND IS_ZONE_TRIGGERABLE(iZoneStaggeredLoop)
						
						SET_BIT(iLocalBoolCheck4, LBOOL4_INSIDE_RESTRICTED_AIRSPACE)
						
						IF NOT HAS_NET_TIMER_STARTED(tdRocketTimer)
							PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES, Zone ", iZoneStaggeredLoop, ", Restricted Airspace - Starting timer")
							REINIT_NET_TIMER(tdRocketTimer)
						ENDIF
						
						IF (NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_RESTRICTION_ZONE_ROCKETS_REQUIRED))
						OR (NOT IS_BIT_SET(iLocalBoolCheck5,LBOOL5_RESTRICTION_ROCKET_LOADED))
							IF NOT IS_BIT_SET(iLocalBoolCheck5,LBOOL5_RESTRICTION_ROCKET_LOADED)
								REQUEST_WEAPON_ASSET(WEAPONTYPE_RPG)
								SET_BIT(iLocalBoolCheck5,LBOOL5_RESTRICTION_ROCKET_LOADED)
							ENDIF
							PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES, Zone ", iZoneStaggeredLoop, ", Restricted Airspace - I need restriction rockets")
							SET_BIT(iLocalBoolCheck6,LBOOL6_SOMEONE_NEEDS_RESTRICTION_ROCKETS)
							SET_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_RESTRICTION_ZONE_ROCKETS_REQUIRED)
							BROADCAST_FMMC_REQUEST_RESTRICTION_ROCKETS()
						ELSE
							IF HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_RPG)
								IF (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdRocketTimer) >= 2800)
								AND MC_playerBD[iLocalPart].iGameState = GAME_STATE_RUNNING
									
									INT iRocketPos
									VECTOR vRocketFireFromThisLocation
									vRocketFireFromThisLocation = GET_CLOSEST_ROCKET_POSITION_FOR_ZONE(iZoneStaggeredLoop, iRocketPos)
									
									IF NOT IS_VECTOR_ZERO(vRocketFireFromThisLocation)
										
										ENTITY_INDEX TargetEntity
										
										IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
											TargetEntity = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
										ELSE
											TargetEntity = LocalPlayerPed
										ENDIF
										
										FLOAT fAccuracy
										BOOL bHoming
										INT iCustomAccuracyVeh
										iCustomAccuracyVeh = g_FMMC_STRUCT_ENTITIES.iFMMCZoneRocketCustomAccuracyOnlyInThisVeh[iRocketPos]
										
										IF iCustomAccuracyVeh = -1
										OR (NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iCustomAccuracyVeh]) AND (TargetEntity = NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicle[iCustomAccuracyVeh])))
											fAccuracy = g_FMMC_STRUCT_ENTITIES.fFMMCZoneRocketAccuracy[iRocketPos]
											bHoming = NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.iFMMCZoneRocketNotHomingBS, iRocketPos)
											PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES, Zone ", iZoneStaggeredLoop, ", Restricted Airspace - Loaded globals accuracy ",fAccuracy," / bHoming ",bHoming)
										ELSE
											fAccuracy = 100.0
											bHoming = TRUE
											
											#IF IS_DEBUG_BUILD
											IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iCustomAccuracyVeh])
												PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES, Zone ", iZoneStaggeredLoop, ", Restricted Airspace - Custom accuracy veh ",iCustomAccuracyVeh," doesn't exist, use perfect accuracy")
											ELSE
												PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES, Zone ", iZoneStaggeredLoop, ", Restricted Airspace - Custom accuracy veh ",iCustomAccuracyVeh," does exist")
												
												IF DOES_ENTITY_EXIST(TargetEntity)
													IF IS_ENTITY_A_VEHICLE(TargetEntity)
														INT iveh
														iveh = IS_ENTITY_A_MISSION_CREATOR_ENTITY(TargetEntity)
														PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES, Zone ", iZoneStaggeredLoop, ", Restricted Airspace - Local player is in vehicle ",iveh," / SCRIPT_AUTOMOBILE_",NETWORK_ENTITY_GET_OBJECT_ID(TargetEntity))
													ELSE
														PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES, Zone ", iZoneStaggeredLoop, ", Restricted Airspace - Local player is not in a vehicle")
													ENDIF
												ELSE
													PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES, Zone ", iZoneStaggeredLoop, ", Restricted Airspace - TargetEntity doesn't exist?")
												ENDIF
											ENDIF
											#ENDIF
										ENDIF
										
										FLOAT fRocketSpeed
										fRocketSpeed = 62.80637891
										
										VECTOR vAimAtLocation
										vAimAtLocation = GET_VECTOR_TO_AIM_AT(GET_ENTITY_COORDS(TargetEntity), GET_ENTITY_VELOCITY(TargetEntity), vRocketFireFromThisLocation, fRocketSpeed, fAccuracy)
										
										ENTITY_INDEX IgnoreEntity, HomingEntity
										IgnoreEntity = NULL
										HomingEntity = NULL
										
										IF g_FMMC_STRUCT_ENTITIES.iFMMCZoneRocketLinkedObj[iRocketPos] != -1
											// No need to check if it exists or is alive, it must be for this rocket position to have been chosen
											PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES, Zone ", iZoneStaggeredLoop, ", Restricted Airspace - Rocket pos ",iRocketPos," should ignore linked obj ",g_FMMC_STRUCT_ENTITIES.iFMMCZoneRocketLinkedObj[iRocketPos])
											IgnoreEntity = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[g_FMMC_STRUCT_ENTITIES.iFMMCZoneRocketLinkedObj[iRocketPos]])
										ENDIF
										
										IF bHoming
											
											PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES, Zone ", iZoneStaggeredLoop, ", Restricted Airspace - This rocket pos ",iRocketPos," is set to home in, use player / player veh")
											HomingEntity = TargetEntity
											
										#IF IS_DEBUG_BUILD
										ELSE
											PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES, Zone ", iZoneStaggeredLoop, ", Restricted Airspace - This rocket pos ",iRocketPos," is set not to home in")
										#ENDIF
										ENDIF
										
										IF NOT IS_BIT_SET(iZoneRocketClearToFireBS, iZoneStaggeredLoop)
											PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES, Zone ", iZoneStaggeredLoop, ", Restricted Airspace - Calling CLEAR_AREA_OF_PROJECTILES at ", vRocketFireFromThisLocation)
											CLEAR_AREA_OF_PROJECTILES(vRocketFireFromThisLocation, 10.0, TRUE)
											SET_BIT(iZoneRocketClearToFireBS, iZoneStaggeredLoop)
										ELSE
											PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES, Zone ", iZoneStaggeredLoop, ", Restricted Airspace - Firing rocket from ", vRocketFireFromThisLocation, " to ", vAimAtLocation)
											SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY(vRocketFireFromThisLocation, vAimAtLocation, 99, (fAccuracy > 99.9), WEAPONTYPE_RPG, NULL, TRUE, TRUE, DEFAULT, IgnoreEntity, HomingEntity)
											CLEAR_BIT(iZoneRocketClearToFireBS, iZoneStaggeredLoop)
										ENDIF
									ELSE
										PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES, Zone ", iZoneStaggeredLoop, ", Restricted Airspace - Unable to find a Rocket location!")
									ENDIF
									
									REINIT_NET_TIMER(tdRocketTimer)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE ciFMMC_ZONE_TYPE__MENU_CLEAR
				
					IF NOT IS_BIT_SET( iLocalGearZoneNoneSet, iZoneStaggeredLoop )
						IF (iSpectatorTarget = -1)
						AND IS_PED_IN_RESTRICTION_ZONE(LocalPlayerPed, iZoneStaggeredLoop, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].iZoneBS, ciFMMC_ZONEBS_IncludeZAxisInAreaCheck), fZoneGrowth[iZoneStaggeredLoop])
						AND IS_ZONE_TRIGGERABLE(iZoneStaggeredLoop)
							SET_BIT( iLocalGearZoneNoneSet, iZoneStaggeredLoop )
							PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES, Zone ", iZoneStaggeredLoop, ", ciFMMC_ZONE_TYPE__MENU_CLEAR set ")
							SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_MAKE_PI_GEAR_START_AT_NONE)
						ENDIF
					ENDIF
				BREAK
						
				CASE ciFMMC_ZONE_TYPE__FORCE_OBJECT
					SET_FORCE_OBJECT_THIS_FRAME(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].vPos[0], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].fRadius)
					PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES, Zone ", iZoneStaggeredLoop, ", ciFMMC_ZONE_TYPE__FORCE_OBJECT set ")
				BREAK
				
				CASE ciFMMC_ZONE_TYPE__BLOCK_PARACHUTE
					IF IS_PED_IN_RESTRICTION_ZONE(PlayerPedToUse, iZoneStaggeredLoop, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].iZoneBS, ciFMMC_ZONEBS_IncludeZAxisInAreaCheck), fZoneGrowth[iZoneStaggeredLoop])
					AND IS_ZONE_TRIGGERABLE(iZoneStaggeredLoop)
						IF NOT IS_BIT_SET(iWaveDampingZoneBitset, iZoneStaggeredLoop)
							PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES, Zone ", iZoneStaggeredLoop, " ciFMMC_ZONE_TYPE__BLOCK_PARACHUTE entered")
							SET_BIT(iWaveDampingZoneBitset, iZoneStaggeredLoop)
							SET_BIT(iLocalZoneCheckBitset,iZoneStaggeredLoop)
						ENDIF
					ELSE
						IF IS_BIT_SET(iWaveDampingZoneBitset, iZoneStaggeredLoop)
							PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES, Zone ", iZoneStaggeredLoop, " ciFMMC_ZONE_TYPE__BLOCK_PARACHUTE exited")
							CLEAR_BIT(iWaveDampingZoneBitset, iZoneStaggeredLoop)
							ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PARACHUTE_DEPLOY)
							//CLEAR_BIT(iLocalZoneCheckBitset,iZoneStaggeredLoop)
						ENDIF
					ENDIF
				BREAK
				
				CASE ciFMMC_ZONE_TYPE__WATER_DAMPENING // We'll only get here if fZoneValue2 != 1.0
					
					IF IS_PED_IN_RESTRICTION_ZONE(PlayerPedToUse, iZoneStaggeredLoop, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].iZoneBS, ciFMMC_ZONEBS_IncludeZAxisInAreaCheck), fZoneGrowth[iZoneStaggeredLoop])
					AND IS_ZONE_TRIGGERABLE(iZoneStaggeredLoop)
						
						//don't want to keep switching between this and other zones we're in! use a bitset!!
						IF NOT IS_BIT_SET(iWaveDampingZoneBitset, iZoneStaggeredLoop)
							IF GET_DEEP_OCEAN_SCALER() != g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].fZoneValue2
								PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES, Zone ", iZoneStaggeredLoop, ", Wave Amounts - Calling SET_DEEP_OCEAN_SCALER w value ",g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].fZoneValue2)
								SET_DEEP_OCEAN_SCALER(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].fZoneValue2)
							ENDIF
							
							SET_BIT(iWaveDampingZoneBitset, iZoneStaggeredLoop)
						ENDIF
					ELSE
						IF IS_BIT_SET(iWaveDampingZoneBitset, iZoneStaggeredLoop)
							PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES, Zone ", iZoneStaggeredLoop, ", Wave Amounts - No longer in this zone")
							
							CLEAR_BIT(iWaveDampingZoneBitset, iZoneStaggeredLoop)
							
							IF iWaveDampingZoneBitset > 0
								IF GET_DEEP_OCEAN_SCALER() = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].fZoneValue2 //If this one is the zone running right now
									SELECT_NEXT_WAVE_DAMPING_SCALER()
								ENDIF
							ELSE
								IF GET_DEEP_OCEAN_SCALER() != 1.0
									PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES, Zone ", iZoneStaggeredLoop, ", Wave Amounts - Calling RESET_DEEP_OCEAN_SCALER")
									RESET_DEEP_OCEAN_SCALER()
								ENDIF
							ENDIF
							
						ENDIF
					ENDIF
				BREAK
				
				CASE ciFMMC_ZONE_TYPE__PLAYER_CANT_EXIT_VEHICLE
					IF (iSpectatorTarget = -1)
					AND IS_PED_IN_RESTRICTION_ZONE(PlayerPedToUse, iZoneStaggeredLoop, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].iZoneBS, ciFMMC_ZONEBS_IncludeZAxisInAreaCheck), fZoneGrowth[iZoneStaggeredLoop])
					AND IS_ZONE_TRIGGERABLE(iZoneStaggeredLoop)
						IF NOT IS_BIT_SET(iPlayerCantExitVehicleZoneBitset, iZoneStaggeredLoop)
							PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES, Zone ", iZoneStaggeredLoop, ", \"Can't exit vehicle\" - Entered this zone")
							SET_BIT(iPlayerCantExitVehicleZoneBitset, iZoneStaggeredLoop)
							IF IS_THIS_ROCKSTAR_MISSION_WVM_OPPRESSOR_2(g_FMMC_STRUCT.iRootContentIDHash)
								PRINTLN("[LM] START_AUDIO_SCENE(\"DLC_GR_WVM_Plane_Scene\") Starting Scene.")
								START_AUDIO_SCENE("DLC_GR_WVM_Plane_Scene")
							ENDIF
							bInPlayerCantExitVehicleZone = TRUE
						ENDIF
					ELSE
						IF IS_BIT_SET(iPlayerCantExitVehicleZoneBitset, iZoneStaggeredLoop)
							PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES, Zone ", iZoneStaggeredLoop, ", \"Can't exit vehicle\" - No longer in this zone")
							CLEAR_BIT(iPlayerCantExitVehicleZoneBitset, iZoneStaggeredLoop)
							IF IS_THIS_ROCKSTAR_MISSION_WVM_OPPRESSOR_2(g_FMMC_STRUCT.iRootContentIDHash)
								PRINTLN("[LM] START_AUDIO_SCENE(\"DLC_GR_WVM_Plane_Scene\") Stopping Scene.")
								STOP_AUDIO_SCENE("DLC_GR_WVM_Plane_Scene")
							ENDIF
							bInPlayerCantExitVehicleZone = FALSE
						ENDIF
					ENDIF
				BREAK
				
				CASE ciFMMC_ZONE_TYPE__PLAYER_CANT_DRIVE_VEHICLE
					IF (iSpectatorTarget = -1)
					AND IS_PED_IN_RESTRICTION_ZONE(PlayerPedToUse, iZoneStaggeredLoop, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].iZoneBS, ciFMMC_ZONEBS_IncludeZAxisInAreaCheck), fZoneGrowth[iZoneStaggeredLoop])
					AND IS_ZONE_TRIGGERABLE(iZoneStaggeredLoop)
						IF NOT IS_BIT_SET(iPlayerCantDriveVehicleZoneBitset, iZoneStaggeredLoop)
							PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES, Zone ", iZoneStaggeredLoop, ", \"Can't drive vehicle\" - Entered this zone")
							SET_BIT(iPlayerCantDriveVehicleZoneBitset, iZoneStaggeredLoop)
							bInPlayerCantdriveVehicleZone = TRUE
						ENDIF
					ELSE
						IF IS_BIT_SET(iPlayerCantDriveVehicleZoneBitset, iZoneStaggeredLoop)
							PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES, Zone ", iZoneStaggeredLoop, ", \"Can't drive vehicle\" - No longer in this zone")
							CLEAR_BIT(iPlayerCantDriveVehicleZoneBitset, iZoneStaggeredLoop)
							bInPlayerCantdriveVehicleZone = FALSE
						ENDIF
					ENDIF
				BREAK
				
				CASE ciFMMC_ZONE_TYPE__BLOCK_WANTED_LEVEL
					IF (iSpectatorTarget = -1)
					AND IS_PED_IN_RESTRICTION_ZONE(LocalPlayerPed, iZoneStaggeredLoop, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].iZoneBS, ciFMMC_ZONEBS_IncludeZAxisInAreaCheck), fZoneGrowth[iZoneStaggeredLoop])
					AND IS_ZONE_TRIGGERABLE(iZoneStaggeredLoop)
						
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].iZoneBS, ciFMMC_ZONEBS_BLOCK_WNTD_CLEARS_WNTD)
							PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES - Clear Existing Wanted Level is set. for iZone: ", iZoneStaggeredLoop)
							IF NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_IN_BLOCK_AND_CLEAR_WANTED_LEVEL_ZONE)
								
								IF GET_PLAYER_WANTED_LEVEL(LocalPlayer) != 0
									g_bMissionRemovedWanted = TRUE // Don't want to get XP for losing a wanted level here, the player didn't actually do anything
								ENDIF
								
								SET_PLAYER_WANTED_LEVEL(LocalPlayer, 0)
								SET_PLAYER_WANTED_LEVEL_NOW(LocalPlayer)
								MC_playerBD[iLocalPart].iWanted = 0
								
								SET_FAKE_WANTED_LEVEL(0)
								MC_playerBD[iLocalPart].iFakeWanted = 0
								
								PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES - Entered a block wanted level zone, clear wanted level and set max wanted level to 0 - current max = ",GET_MAX_WANTED_LEVEL())
								SET_MAX_WANTED_LEVEL(0)
								
								IF IS_BIT_SET(iLocalBoolCheck5, LBOOL5_DISPATCHOFF) // If we're running with a fake wanted level
									SET_DISPATCH_SERVICES(TRUE, FALSE) // Make sure these are ready to go once players leave the area
								ENDIF
								
								SET_BIT(iLocalBoolCheck13, LBOOL13_IN_BLOCK_AND_CLEAR_WANTED_LEVEL_ZONE)
							ENDIF
						ELSE
							PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES - Clear Existing Wanted Level is NOT set. for iZone: ", iZoneStaggeredLoop)
							IF NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_IN_BLOCK_WANTED_LEVEL_ZONE)
								PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES - Entered a block wanted level zone, set max wanted level to player current wanted (",GET_PLAYER_WANTED_LEVEL(LocalPlayer),") - current max = ",GET_MAX_WANTED_LEVEL())
								SET_MAX_WANTED_LEVEL(GET_PLAYER_WANTED_LEVEL(LocalPlayer))
								SET_BIT(iLocalBoolCheck13, LBOOL13_IN_BLOCK_WANTED_LEVEL_ZONE)
							ENDIF
						ENDIF
						
						SET_BIT(iLocalBoolCheck13, LBOOL13_IN_BLOCK_WANTED_LEVEL_ZONE_TEMP)
					ENDIF
				BREAK
				
				CASE ciFMMC_ZONE_TYPE__BLOCK_JUMPING_AND_CLIMBING
				
					IF NOT SHOULD_PROCESS_BLOCK_CLIMBING_ZONES_EVERY_FRAME()
						PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES Hitting the blocked jumping zone")
						IF (iSpectatorTarget = -1)
						AND IS_PED_IN_RESTRICTION_ZONE(LocalPlayerPed, iZoneStaggeredLoop, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].iZoneBS, ciFMMC_ZONEBS_IncludeZAxisInAreaCheck), fZoneGrowth[iZoneStaggeredLoop])
						AND IS_ZONE_TRIGGERABLE(iZoneStaggeredLoop)
							PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES Player in Blocked Jumping/Climbing zone - Blocking jump action")
							bInBlockedJumpAndClimbZone = TRUE
							IF NOT IS_BIT_SET(iBlockJumpClimbZoneBitset, iZoneStaggeredLoop)
								SET_BIT(iBlockJumpClimbZoneBitset, iZoneStaggeredLoop)
							ENDIF
						ELSE
							IF IS_BIT_SET(iBlockJumpClimbZoneBitset, iZoneStaggeredLoop)
								PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES Player has left Blocked Jumping/Climbing zone - Re-allowing jump action")
								bInBlockedJumpAndClimbZone = FALSE
								CLEAR_BIT(iBlockJumpClimbZoneBitset, iZoneStaggeredLoop)
							ENDIF
						ENDIF
					ENDIF
					
				BREAK
				
				CASE ciFMMC_ZONE_TYPE__SET_AIR_DRAG_MULTIPLIER
					IF (iSpectatorTarget = -1)
					AND IS_PED_IN_RESTRICTION_ZONE(LocalPlayerPed, iZoneStaggeredLoop, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].iZoneBS, ciFMMC_ZONEBS_IncludeZAxisInAreaCheck), fZoneGrowth[iZoneStaggeredLoop])
					AND IS_ZONE_TRIGGERABLE(iZoneStaggeredLoop)
						IF NOT IS_BIT_SET(iPlayerVehicleSlowZoneBitset, iZoneStaggeredLoop)
							PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES Player has entered Player Vehicle Slow zone - Setting air drag multiplier to ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].fZoneValue)
							SET_BIT(iPlayerVehicleSlowZoneBitset, iZoneStaggeredLoop)
							SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].fZoneValue)
						ENDIF
					ELSE
						IF IS_BIT_SET(iPlayerVehicleSlowZoneBitset, iZoneStaggeredLoop)
							PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES Player has left Player Vehicle Slow zone - Re-setting air drag multiplier")
							CLEAR_BIT(iPlayerVehicleSlowZoneBitset, iZoneStaggeredLoop)
							SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 1.0)
						ENDIF
					ENDIF
				BREAK 
				
				CASE ciFMMC_ZONE_TYPE__PLAYER_VEHICLE_MAX_SPEED
					VEHICLE_INDEX tempVeh
					IF (iSpectatorTarget = -1)
					AND IS_PED_IN_RESTRICTION_ZONE(LocalPlayerPed, iZoneStaggeredLoop, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].iZoneBS, ciFMMC_ZONEBS_IncludeZAxisInAreaCheck), fZoneGrowth[iZoneStaggeredLoop])
					AND IS_ZONE_TRIGGERABLE(iZoneStaggeredLoop)
						IF NOT IS_BIT_SET(iPlayerVehicleMaxSpeedBitset, iZoneStaggeredLoop)
							PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES Player has entered Player Vehicle Max Speed - Setting max speed to ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].fZoneValue)
							SET_BIT(iPlayerVehicleMaxSpeedBitset, iZoneStaggeredLoop)
						ENDIF
						IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
							tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(VEH_TO_NET(tempVeh))
								PRINTLN("[RCC MISSION][LV] PROCESS_RESTRICTION_ZONES ciFMMC_ZONE_TYPE__PLAYER_VEHICLE_MAX_SPEED Current Speed ", GET_ENTITY_SPEED(tempVeh))
								FLOAT fSpeed
								FLOAT fDiff
								fSpeed = GET_ENTITY_SPEED(tempVeh)
								fDiff = fSpeed - g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].fZoneValue
								IF  fDiff > 0
									SET_VEHICLE_FORWARD_SPEED(tempVeh,fSpeed - fDiff*g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].fZoneValue2)
									PRINTLN("[RCC MISSION][LV] PROCESS_RESTRICTION_ZONES ciFMMC_ZONE_TYPE__PLAYER_VEHICLE_MAX_SPEED New Speed ", GET_ENTITY_SPEED(tempVeh))
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF IS_BIT_SET(iPlayerVehicleMaxSpeedBitset, iZoneStaggeredLoop)
							PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES Player has left Player Vehicle MaxSpeed - Re-setting max speed")
							CLEAR_BIT(iPlayerVehicleMaxSpeedBitset, iZoneStaggeredLoop)
						ENDIF
					ENDIF
				BREAK 
				
				CASE ciFMMC_ZONE_TYPE__BLOCK_RUNNING
					PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES Hitting the blocked running zone")
					IF (iSpectatorTarget = -1)
					AND IS_PED_IN_RESTRICTION_ZONE(LocalPlayerPed, iZoneStaggeredLoop, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].iZoneBS, ciFMMC_ZONEBS_IncludeZAxisInAreaCheck), fZoneGrowth[iZoneStaggeredLoop])
					AND IS_ZONE_TRIGGERABLE(iZoneStaggeredLoop)
						PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES Player in Blocked Running zone - Blocking run action")
						bInBlockedRunningZone = TRUE
						fBlockedRunningZoneSpeed = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].fZoneValue
						IF NOT IS_BIT_SET(iBlockRunningZoneBitset, iZoneStaggeredLoop)
							SET_BIT(iBlockRunningZoneBitset, iZoneStaggeredLoop)
						ENDIF
					ELSE
						IF IS_BIT_SET(iBlockRunningZoneBitset, iZoneStaggeredLoop)
							PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES Player has left Blocked Running zone - Re-allowing run action")
							bInBlockedRunningZone = FALSE
							fBlockedRunningZoneSpeed = 0.0
							CLEAR_BIT(iBlockRunningZoneBitset, iZoneStaggeredLoop)
						ENDIF
					ENDIF
				BREAK
				
				CASE ciFMMC_ZONE_TYPE__BLIP_PLAYER
					IF SHOULD_CREATE_RESTRICTION_ZONE(iZoneStaggeredLoop, MC_PlayerBD[iLocalPart].iTeam)
						IF NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_BLIP_FORCED_ON_BY_ZONE)
						AND MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iTeam] < FMMC_MAX_RULES
							IF (iSpectatorTarget = -1)
							AND IS_PED_IN_RESTRICTION_ZONE(LocalPlayerPed, iZoneStaggeredLoop, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].iZoneBS, ciFMMC_ZONEBS_IncludeZAxisInAreaCheck), fZoneGrowth[iZoneStaggeredLoop])
							AND IS_ZONE_TRIGGERABLE(iZoneStaggeredLoop)
								SET_BIT(iLocalBoolCheck23, LBOOL23_BLIP_FORCED_ON_BY_ZONE)
								SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, "GUARD_BP")
								PROCESS_MY_PLAYER_BLIP_VISIBILITY(MC_PlayerBD[iLocalPart].iTeam, MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iTeam])
								PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES Player in blip player zone")
							ENDIF
						ENDIF
					ELSE
						CLEAR_BIT(iLocalZoneCheckBitset, iZoneStaggeredLoop)
						PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES blip player zone no longer valid (teams swapped)")
					ENDIF
				BREAK
				
				CASE ciFMMC_ZONE_TYPE__OBJECT_RESPAWN
					FOR iObj = 0 TO MC_serverBD.iNumObjCreated - 1
						IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iObj])
						AND MC_serverBD.iObjCarrier[iObj] = -1
							tempObj = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iObj])
							
							IF NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
								IF NOT IS_ENTITY_ATTACHED_TO_ANY_PED(tempObj)
									PRINTLN("[JS] PROCESS_RESTRICTION_ZONES - KEEP_OBJECT_IN_ZONE - called Obj - ", iObj, " Zone - ", iZoneStaggeredLoop)
									KEEP_OBJECT_IN_ZONE(tempObj, iZoneStaggeredLoop)
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
				BREAK
				
				CASE ciFMMC_ZONE_TYPE__OBJECT_RESPAWN_WITHIN
					FOR iObj = 0 TO MC_serverBD.iNumObjCreated - 1
						IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iObj])
						AND MC_serverBD.iObjCarrier[iObj] = -1
							tempObj = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iObj])
							
							IF NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
								IF NOT IS_ENTITY_ATTACHED_TO_ANY_PED(tempObj)
									PRINTLN("[JS] PROCESS_RESTRICTION_ZONES - KEEP_OBJECT_OUT_OF_ZONE - called Obj - ", iObj, " Zone - ", iZoneStaggeredLoop)
									KEEP_OBJECT_OUT_OF_ZONE(tempObj, iZoneStaggeredLoop, iObj)
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
				BREAK
				
				CASE ciFMMC_ZONE_TYPE__BLOCK_WEAPONS
					IF SHOULD_CREATE_RESTRICTION_ZONE(iZoneStaggeredLoop, MC_PlayerBD[iLocalPart].iTeam)
						PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES Hitting the blocked weapons zone")
						IF (iSpectatorTarget = -1)
						AND IS_PED_IN_RESTRICTION_ZONE(LocalPlayerPed, iZoneStaggeredLoop, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].iZoneBS, ciFMMC_ZONEBS_IncludeZAxisInAreaCheck), fZoneGrowth[iZoneStaggeredLoop])
						AND IS_ZONE_TRIGGERABLE(iZoneStaggeredLoop)
							PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES Player in Blocked weapons zone - Blocking weapon wheel and setting player to unarmed")
							bInBlockedWeaponsZone = TRUE
							IF NOT IS_BIT_SET(iBlockWeaponsZoneBitset, iZoneStaggeredLoop)
								SET_BIT(iBlockWeaponsZoneBitset, iZoneStaggeredLoop)
							ENDIF
						ELSE
							IF IS_BIT_SET(iBlockWeaponsZoneBitset, iZoneStaggeredLoop)
								PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES Player has left Blocked Running zone - Re-allowing run action")
								bInBlockedWeaponsZone = FALSE
								CLEAR_BIT(iBlockWeaponsZoneBitset, iZoneStaggeredLoop)
								
								IF wtWeaponBlockZonePreviousWeapon != WEAPONTYPE_INVALID
									BOOL bForce
									bForce = (NOT IS_ENTITY_IN_WATER(LocalPlayerPed) AND NOT IS_PED_FALLING(LocalPlayerPed))
									SET_CURRENT_PED_WEAPON(LocalPlayerPed, wtWeaponBlockZonePreviousWeapon, bForce)
									wtWeaponBlockZonePreviousWeapon = WEAPONTYPE_INVALID
								ENDIF
							ENDIF
						ENDIF
					ELSE
						CLEAR_BIT(iLocalZoneCheckBitset, iZoneStaggeredLoop)
						PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES Blocked weapons zone no longer valid")
					ENDIF
				BREAK
				
				CASE ciFMMC_ZONE_TYPE__AUTO_PARACHUTE
				
					IF IS_BIT_SET(iLocalBoolCheck19, LBOOL19_AUTO_PARACHUTE_ZONE_ACTIVATED)
						IF NOT IS_ENTITY_IN_AIR(LocalPlayerPed)
							CLEAR_BIT(iLocalBoolCheck19, LBOOL19_AUTO_PARACHUTE_ZONE_ACTIVATED)
							PRINTLN("[RCC MISSION][AutoParachute] PROCESS_RESTRICTION_ZONES | Clearing LBOOL19_AUTO_PARACHUTE_ZONE_ACTIVATED")
						ENDIF
					ELSE					
						IF (iSpectatorTarget = -1)
						AND IS_PED_IN_RESTRICTION_ZONE(LocalPlayerPed, iZoneStaggeredLoop,TRUE,TRUE, fZoneGrowth[iZoneStaggeredLoop])
						AND IS_ZONE_TRIGGERABLE(iZoneStaggeredLoop)
							
							PRINTLN("[RCC MISSION][AutoParachute_Spam] PROCESS_RESTRICTION_ZONES PARACHUTE ZONE BEING PROCESSD")
							
							IF NOT IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST) // Just in case the zone data is set funny in an old mission
								g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].fZoneValue = cfFMMC_PARACHUTEZONE_VehiclesOnly
							ENDIF
							
							IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].fZoneValue = cfFMMC_PARACHUTEZONE_VehiclesOnly
							OR g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].fZoneValue = cfFMMC_PARACHUTEZONE_Both
								IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
									VEHICLE_INDEX vehIndex
									vehIndex = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
									IF IS_VEHICLE_DRIVEABLE(vehIndex)
										IF GET_VEHICLE_HAS_PARACHUTE(vehIndex)
											IF GET_VEHICLE_CAN_DEPLOY_PARACHUTE(vehIndex)
												VEHICLE_START_PARACHUTING(vehIndex)
												PRINTLN("[RCC MISSION][AutoParachute] PROCESS_RESTRICTION_ZONES PARACHUTE ACTIVATE")
												SET_BIT(iLocalBoolCheck19, LBOOL19_AUTO_PARACHUTE_ZONE_ACTIVATED)
											ENDIF
										ENDIF
										IF GET_ENTITY_MODEL(vehIndex) = OPPRESSOR
											SET_GLIDER_ACTIVE(vehIndex, TRUE)
											PRINTLN("[RCC MISSION][AutoParachute] PROCESS_RESTRICTION_ZONES OPPRESSOR GLIDER ACTIVATE")
											SET_BIT(iLocalBoolCheck19, LBOOL19_AUTO_PARACHUTE_ZONE_ACTIVATED)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].fZoneValue = cfFMMC_PARACHUTEZONE_PedsOnly
							OR g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].fZoneValue = cfFMMC_PARACHUTEZONE_Both
								
								IF IS_PED_IN_PARACHUTE_FREE_FALL(LocalPlayerPed)
									// Release parachute
									FORCE_PED_TO_OPEN_PARACHUTE(LocalPlayerPed)
									PRINTLN("[RCC MISSION][AutoParachute] PROCESS_RESTRICTION_ZONES | Forcing use of parachute now!")
									SET_BIT(iLocalBoolCheck19, LBOOL19_AUTO_PARACHUTE_ZONE_ACTIVATED)
								ENDIF
							ENDIF
						ELSE
							IF IS_BIT_SET(iBlockWeaponsZoneBitset, iZoneStaggeredLoop)
								CLEAR_BIT(iBlockWeaponsZoneBitset, iZoneStaggeredLoop)
							ENDIF
						ENDIF
					ENDIF

				BREAK
				
				CASE ciFMMC_ZONE_TYPE__ALERT_PEDS
					
					PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES ALERT PED - Hitting Alert Peds Zone")
					IF (iSpectatorTarget = -1)
					AND IS_PED_IN_RESTRICTION_ZONE(LocalPlayerPed, iZoneStaggeredLoop, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].iZoneBS, ciFMMC_ZONEBS_IncludeZAxisInAreaCheck), fZoneGrowth[iZoneStaggeredLoop])
					AND IS_ZONE_TRIGGERABLE(iZoneStaggeredLoop)
						PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES ALERT PED - Player in Alert Ped zone")
						IF NOT IS_BIT_SET(iAlertPedsZoneBitSet, iZoneStaggeredLoop)
						AND IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
							SET_BIT(iAlertPedsZoneBitSet, iZoneStaggeredLoop)
							PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES ALERT PED - Setting bit for zone: ", iZoneStaggeredLoop)
						ENDIF
					ELSE
						IF IS_BIT_SET(iAlertPedsZoneBitSet, iZoneStaggeredLoop)
							CLEAR_BIT(iAlertPedsZoneBitSet, iZoneStaggeredLoop)
							PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES ALERT PED - Clearing bit for zone: ", iZoneStaggeredLoop)
						ENDIF
					ENDIF
				BREAK
				
				CASE ciFMMC_ZONE_TYPE__PLAYER_TRIGGERED_EXPLOSION_DANGER_AREA
					IF iSpectatorTarget = -1
					AND IS_ZONE_TRIGGERABLE(iZoneStaggeredLoop)
						IF IS_PED_IN_RESTRICTION_ZONE(LocalPlayerPed, iZoneStaggeredLoop, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].iZoneBS, ciFMMC_ZONEBS_IncludeZAxisInAreaCheck), fZoneGrowth[iZoneStaggeredLoop])
							IF NOT IS_BIT_SET(MC_playerBD_1[iLocalPart].iExplosionDangerZoneInsideBS, iZoneStaggeredLoop)
								SET_BIT(MC_playerBD_1[iLocalPart].iExplosionDangerZoneInsideBS, iZoneStaggeredLoop)
								PRINTLN("[RCC MISSION][CasinoVaultDoor] ciFMMC_ZONE_TYPE__PLAYER_TRIGGERED_EXPLOSION_DANGER_AREA - Setting iExplosionDangerZoneInsideBS for zone: ", iZoneStaggeredLoop)
							ENDIF
						ELSE
							IF IS_BIT_SET(MC_playerBD_1[iLocalPart].iExplosionDangerZoneInsideBS, iZoneStaggeredLoop)
								CLEAR_BIT(MC_playerBD_1[iLocalPart].iExplosionDangerZoneInsideBS, iZoneStaggeredLoop)
								PRINTLN("[RCC MISSION][CasinoVaultDoor] ciFMMC_ZONE_TYPE__PLAYER_TRIGGERED_EXPLOSION_DANGER_AREA - Clearing iExplosionDangerZoneInsideBS for zone: ", iZoneStaggeredLoop)
							ENDIF
						ENDIF
					ELSE
						IF IS_BIT_SET(MC_playerBD_1[iLocalPart].iExplosionDangerZoneInsideBS, iZoneStaggeredLoop)
							CLEAR_BIT(MC_playerBD_1[iLocalPart].iExplosionDangerZoneInsideBS, iZoneStaggeredLoop)
							PRINTLN("[RCC MISSION][CasinoVaultDoor] ciFMMC_ZONE_TYPE__PLAYER_TRIGGERED_EXPLOSION_DANGER_AREA - (2) Clearing iExplosionDangerZoneInsideBS for zone: ", iZoneStaggeredLoop)
						ENDIF
					ENDIF
				BREAK
				
				CASE ciFMMC_ZONE_TYPE__REFILL_SPECIAL_ABILITY
					IF (iSpectatorTarget = -1)
					AND IS_PED_IN_RESTRICTION_ZONE(LocalPlayerPed, iZoneStaggeredLoop, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].iZoneBS, ciFMMC_ZONEBS_IncludeZAxisInAreaCheck), fZoneGrowth[iZoneStaggeredLoop])
					AND IS_ZONE_TRIGGERABLE(iZoneStaggeredLoop)
						IF NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_SPECIAL_ABILITY_REFILLED)
							IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
								VEHICLE_INDEX viPedVeh
								viPedVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
								IF GET_ENTITY_MODEL(viPedVeh) = OPPRESSOR
									SET_ROCKET_BOOST_FILL(viPedVeh, 1.0)
									SET_BIT(iLocalBoolCheck24, LBOOL24_SPECIAL_ABILITY_REFILLED)
									PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES ciFMMC_ZONE_TYPE__REFILL_SPECIAL_ABILITY - Setting bit for zone: ", iZoneStaggeredLoop)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF IS_BIT_SET(iLocalBoolCheck24, LBOOL24_SPECIAL_ABILITY_REFILLED)
							CLEAR_BIT(iLocalBoolCheck24, LBOOL24_SPECIAL_ABILITY_REFILLED)
							PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES ciFMMC_ZONE_TYPE__REFILL_SPECIAL_ABILITY - Clearing bit for zone: ", iZoneStaggeredLoop)
						ENDIF
					ENDIF
				BREAK
				
				CASE ciFMMC_ZONE_TYPE__BLOCK_VTOL_TOGGLE
					PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES Hitting the block VTOL zone")
					IF (iSpectatorTarget = -1)
					AND IS_PED_IN_RESTRICTION_ZONE(LocalPlayerPed, iZoneStaggeredLoop, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].iZoneBS, ciFMMC_ZONEBS_IncludeZAxisInAreaCheck), fZoneGrowth[iZoneStaggeredLoop])
					AND IS_ZONE_TRIGGERABLE(iZoneStaggeredLoop)
						PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES Player in Block VTOL zone - Blocking")
						bInBlockVTOLZone = TRUE
						IF NOT IS_BIT_SET(iBlockVTOLZoneBitset, iZoneStaggeredLoop)
							SET_BIT(iBlockVTOLZoneBitset, iZoneStaggeredLoop)
						ENDIF
					ELSE
						IF IS_BIT_SET(iBlockVTOLZoneBitset, iZoneStaggeredLoop)
							PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES Player has left Block VTOL zone")
							bInBlockVTOLZone = FALSE
							
							IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
								VEHICLE_INDEX viMyVeh
								viMyVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
								IF NETWORK_HAS_CONTROL_OF_ENTITY(viMyVeh)
									CLEAR_BIT(iBlockVTOLZoneBitset, iZoneStaggeredLoop)
									IF DOES_THIS_MODEL_HAVE_VERTICAL_FLIGHT_MODE(GET_ENTITY_MODEL(viMyVeh))
										SET_DISABLE_VERTICAL_FLIGHT_MODE_TRANSITION(viMyVeh, FALSE)
									ENDIF
									PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES Calling: SET_DISABLE_VERTICAL_FLIGHT_MODE_TRANSITION False.")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE ciFMMC_ZONE_TYPE__BLOCK_ELECTROCUTION
					IF iSpectatorTarget = -1
					AND IS_PED_IN_RESTRICTION_ZONE(LocalPlayerPed, iZoneStaggeredLoop, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].iZoneBS, ciFMMC_ZONEBS_IncludeZAxisInAreaCheck), fZoneGrowth[iZoneStaggeredLoop])
					AND IS_ZONE_TRIGGERABLE(iZoneStaggeredLoop)
						IF NOT IS_BIT_SET(iBlockElectrocutionZoneBitset, iZoneStaggeredLoop)
							PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES Calling: PCF_DontActivateRagdollFromElectrocution TRUE.")
							SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DontActivateRagdollFromElectrocution, TRUE)
							SET_BIT(iBlockElectrocutionZoneBitset, iZoneStaggeredLoop)
						ENDIF
					ELSE
						IF IS_BIT_SET(iBlockElectrocutionZoneBitset, iZoneStaggeredLoop)
							PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES Calling: PCF_DontActivateRagdollFromElectrocution FALSE.")
							SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DontActivateRagdollFromElectrocution, FALSE)
							CLEAR_BIT(iBlockElectrocutionZoneBitset, iZoneStaggeredLoop)
						ENDIF
					ENDIF
				BREAK
				
				CASE ciFMMC_ZONE_TYPE__RETURN_OBJECT
					FOR iObj = 0 TO MC_serverBD.iNumObjCreated - 1
						IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iObj])
						AND MC_serverBD.iObjCarrier[iObj] = -1
							tempObj = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iObj])
							
							IF DOES_ENTITY_EXIST(tempObj)
								VECTOR vObjPos
								vObjPos = GET_ENTITY_COORDS(tempObj)
								IF NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
								AND NOT IS_ENTITY_ATTACHED(tempObj)
								AND (IS_LOCATION_IN_FMMC_ZONE(vObjPos, iZoneStaggeredLoop, DEFAULT, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].iZoneBS, ciFMMC_ZONEBS_IncludeZAxisInAreaCheck), fZoneGrowth[iZoneStaggeredLoop]) OR IS_BIT_SET(iZoneBS_ActivatedRemotely, iZoneStaggeredLoop))
									PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES - Object ", iObj, " is in zone: ", iZoneStaggeredLoop, " Moving back to spawn pos.")
									SET_ENTITY_COORDS(tempObj, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vPos)
									SET_ENTITY_ROTATION(tempObj, <<0.0,0.0,g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].fHead>>)
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
				BREAK
				
				CASE ciFMMC_ZONE_TYPE__HIDE_OBJECTS_SEARCH_AREA
					INT iObject
					FOR iObject = 0 TO MC_serverBD.iNumObjCreated - 1
						IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iObject])
							tempObj = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iObject])
							
							IF DOES_ENTITY_EXIST(tempObj)
								VECTOR vObjPos
								vObjPos = GET_ENTITY_COORDS(tempObj)								
								IF NOT IS_ENTITY_ATTACHED(tempObj)
								AND (IS_LOCATION_IN_FMMC_ZONE(vObjPos, iZoneStaggeredLoop, DEFAULT, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].iZoneBS, ciFMMC_ZONEBS_IncludeZAxisInAreaCheck), fZoneGrowth[iZoneStaggeredLoop]) OR IS_BIT_SET(iZoneBS_ActivatedRemotely, iZoneStaggeredLoop))
									IF NOT IS_BIT_SET(iObjectHiddenBS[iZoneStaggeredLoop], iObject)
										PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES - Object ", iObject, " is in zone: ", iZoneStaggeredLoop, " Hiding object")
										SET_ENTITY_ALPHA(tempObj, 0, FALSE)
										SET_BIT(iObjectHiddenBS[iZoneStaggeredLoop], iObject)
									ENDIF
									IF DOES_BLIP_EXIST(biObjBlip[iObject])
									AND GET_BLIP_ALPHA(biObjBlip[iObject]) > 0
										iObjectBlipAlpha[iObject] = GET_BLIP_ALPHA(biObjBlip[iObject])
										SET_BLIP_ALPHA(biObjBlip[iObject], 0)
									ENDIF
								ELSE
									IF IS_BIT_SET(iObjectHiddenBS[iZoneStaggeredLoop], iObject)
										PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES - Object ", iObject, " is in zone: ", iZoneStaggeredLoop, " (1) Clearing Hidden Flag for object")
										RESET_ENTITY_ALPHA(tempObj)
										IF DOES_BLIP_EXIST(biObjBlip[iObject])
											SET_BLIP_ALPHA(biObjBlip[iObject], iObjectBlipAlpha[iObject])
										ENDIF
										CLEAR_BIT(iObjectHiddenBS[iZoneStaggeredLoop], iObject)
									ENDIF
								ENDIF
							ELSE
								IF IS_BIT_SET(iObjectHiddenBS[iZoneStaggeredLoop], iObject)
									PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES - Object ", iObject, " is in zone: ", iZoneStaggeredLoop, " (2) Clearing Hidden Flag for object")
									CLEAR_BIT(iObjectHiddenBS[iZoneStaggeredLoop], iObject)
								ENDIF
							ENDIF
						ELSE
							IF IS_BIT_SET(iObjectHiddenBS[iZoneStaggeredLoop], iObject)
								PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES - Object ", iObject, " is in zone: ", iZoneStaggeredLoop, " (3) Clearing Hidden Flag for object")
								CLEAR_BIT(iObjectHiddenBS[iZoneStaggeredLoop], iObject)
							ENDIF
						ENDIF
					ENDFOR
				BREAK
				
				CASE ciFMMC_ZONE_TYPE__BLANK
				CASE ciFMMC_ZONE_TYPE__CLEAR_ENTITY
					PROCESS_PLAYERS_IN_ZONE(iZoneStaggeredLoop)
				BREAK
				
				CASE ciFMMC_ZONE_TYPE__AGGRO_PEDS_IN_THIS_ZONE
					IF iSpectatorTarget = -1
					AND IS_PED_IN_RESTRICTION_ZONE(LocalPlayerPed, iZoneStaggeredLoop, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].iZoneBS, ciFMMC_ZONEBS_IncludeZAxisInAreaCheck), fZoneGrowth[iZoneStaggeredLoop])
					AND IS_ZONE_TRIGGERABLE(iZoneStaggeredLoop)
						PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES - LocalPlayer Activated ciFMMC_ZONE_TYPE__AGGRO_PEDS_IN_THIS_ZONE iZone: ", iZoneStaggeredLoop)
						INT iEnemyPed
						FOR iEnemyPed = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfPeds-1
							IF NOT IS_BIT_SET(iPedZoneAggroed[GET_LONG_BITSET_INDEX(iEnemyPed)], GET_LONG_BITSET_BIT(iEnemyPed))
								IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iEnemyPed])
									PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES - iEnemyPed: ", iEnemyPed, " Exists.")
									
									PED_INDEX pedIndex
									pedIndex = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iEnemyPed])
									IF NOT IS_PED_INJURED(pedIndex)
										IF IS_PED_IN_RESTRICTION_ZONE(pedIndex, iZoneStaggeredLoop, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].iZoneBS, ciFMMC_ZONEBS_IncludeZAxisInAreaCheck), fZoneGrowth[iZoneStaggeredLoop])
											PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES - iEnemyPed: ", iEnemyPed, " Inside Zone. Aggroing!")
											SET_BIT(iPedLocalCancelTasksBitset[GET_LONG_BITSET_INDEX(iEnemyPed)], GET_LONG_BITSET_BIT(iEnemyPed))
											SET_BIT(iPedZoneAggroed[GET_LONG_BITSET_INDEX(iEnemyPed)], GET_LONG_BITSET_BIT(iEnemyPed))
											BROADCAST_FMMC_PED_CANCEL_TASKS(iEnemyPed)
											BROADCAST_FMMC_PED_AGGROED(iEnemyPed, DEFAULT, TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
					ENDIF
				BREAK
				
				CASE ciFMMC_ZONE_TYPE__HEALTH_DRAIN
					IF iSpectatorTarget = -1
					AND IS_PED_IN_RESTRICTION_ZONE(LocalPlayerPed, iZoneStaggeredLoop, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].iZoneBS, ciFMMC_ZONEBS_IncludeZAxisInAreaCheck), fZoneGrowth[iZoneStaggeredLoop])
						SET_BIT(iPlayerWithinPoisonGasZoneBS, iZoneStaggeredLoop)
					ELSE
						CLEAR_BIT(iPlayerWithinPoisonGasZoneBS, iZoneStaggeredLoop)
					ENDIF
				BREAK
			ENDSWITCH
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].iZoneBS, ciFMMC_ZONEBS_AGGRO_WHEN_ENTERED)
				INT iTeam = MC_playerBD[iPartToUse].iteam
				
				IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_0 + iTeam) 
					IF IS_PED_IN_RESTRICTION_ZONE(LocalPlayerPed, iZoneStaggeredLoop, TRUE, TRUE, fZoneGrowth[iZoneStaggeredLoop])
						IF GET_FRAME_COUNT() % 3 = 0
							BROADCAST_FMMC_TRIGGER_AGGRO_FOR_TEAM_LEGACY(iTeam)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ELSE	
			// If zones are inactive, we might want to clear some of their effects.
			SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].iType
				CASE ciFMMC_ZONE_TYPE__HIDE_OBJECTS_SEARCH_AREA
					IF iObjectHiddenBS[iZoneStaggeredLoop] <> 0
						OBJECT_INDEX tempObj
						INT iObject
						FOR iObject = 0 TO MC_serverBD.iNumObjCreated - 1
							IF IS_BIT_SET(iObjectHiddenBS[iZoneStaggeredLoop], iObject)
								IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iObject])
									tempObj = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iObject])
									
									IF DOES_ENTITY_EXIST(tempObj)
										VECTOR vObjPos
										vObjPos = GET_ENTITY_COORDS(tempObj)								
										IF NOT IS_ENTITY_ATTACHED(tempObj)
										AND (IS_LOCATION_IN_FMMC_ZONE(vObjPos, iZoneStaggeredLoop, DEFAULT, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneStaggeredLoop].iZoneBS, ciFMMC_ZONEBS_IncludeZAxisInAreaCheck), fZoneGrowth[iZoneStaggeredLoop]) OR IS_BIT_SET(iZoneBS_ActivatedRemotely, iZoneStaggeredLoop))
											IF IS_BIT_SET(iObjectHiddenBS[iZoneStaggeredLoop], iObject)
												PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES - Object ", iObject, " is in zone: ", iZoneStaggeredLoop, " (2) un-hiding object")
												RESET_ENTITY_ALPHA(tempObj)
												IF DOES_BLIP_EXIST(biObjBlip[iObject])
													SET_BLIP_ALPHA(biObjBlip[iObject], iObjectBlipAlpha[iObject])
												ENDIF
												CLEAR_BIT(iObjectHiddenBS[iZoneStaggeredLoop], iObject)											
											ENDIF
										ENDIF
									ELSE
										IF IS_BIT_SET(iObjectHiddenBS[iZoneStaggeredLoop], iObject)
											PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES - Object ", iObject, " is in zone: ", iZoneStaggeredLoop, " (5) Clearing Hidden Flag for object")
											CLEAR_BIT(iObjectHiddenBS[iZoneStaggeredLoop], iObject)
										ENDIF
									ENDIF
								ELSE
									IF IS_BIT_SET(iObjectHiddenBS[iZoneStaggeredLoop], iObject)
										PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES - Object ", iObject, " is in zone: ", iZoneStaggeredLoop, " (6) Clearing Hidden Flag for object")
										CLEAR_BIT(iObjectHiddenBS[iZoneStaggeredLoop], iObject)
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
		
		iZoneStaggeredLoop++
	ELSE
		IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_RESTRICTION_ZONE_ROCKETS_REQUIRED)
			PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES - No longer in any restricted airspace (iLocalZoneCheckBitset = 0), clearing PBBOOL_RESTRICTION_ZONE_ROCKETS_REQUIRED & resetting rocket timer")
			RESET_NET_TIMER(tdRocketTimer)
			CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_RESTRICTION_ZONE_ROCKETS_REQUIRED)
		ENDIF
		
		IF (IS_BIT_SET(iLocalBoolCheck13, LBOOL13_IN_BLOCK_WANTED_LEVEL_ZONE) OR IS_BIT_SET(iLocalBoolCheck13, LBOOL13_IN_BLOCK_AND_CLEAR_WANTED_LEVEL_ZONE))
			
			INT iMaxWanted = 5
			
			IF MC_serverBD.iPolice > 1
			AND GET_POLICE_MAX_WANTED_FROM_MENU_SETTING(MC_serverBD.iPolice) > 0
				iMaxWanted = GET_POLICE_MAX_WANTED_FROM_MENU_SETTING(MC_serverBD.iPolice)
			ENDIF
			
			PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES - No longer in a block wanted level zone (iLocalZoneCheckBitset = 0), setting max wanted level back to ",iMaxWanted)
			SET_MAX_WANTED_LEVEL(iMaxWanted)
			
			CLEAR_BIT(iLocalBoolCheck13, LBOOL13_IN_BLOCK_WANTED_LEVEL_ZONE)
			CLEAR_BIT(iLocalBoolCheck13, LBOOL13_IN_BLOCK_AND_CLEAR_WANTED_LEVEL_ZONE)
		ENDIF
	ENDIF
	
	IF bInBlockedJumpAndClimbZone
		PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES - DISABLING JUMP & CLIMB")
		
		IF SHOULD_BLOCK_JUMP_INPUT_WHEN_IN_BLOCK_CLIMBING_ZONE()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
		ELSE
			SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisablePlayerVaulting, TRUE)
			SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisablePlayerAutoVaulting, TRUE)
		ENDIF
	ENDIF
	
	IF bInBlockedRunningZone
		PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES - DISABLING RUN fBlockedRunningZoneSpeed: ", fBlockedRunningZoneSpeed)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
		IF fBlockedRunningZoneSpeed < 0.1
			SET_PED_MAX_MOVE_BLEND_RATIO(LocalPlayerPed, PEDMOVEBLENDRATIO_WALK)
		ELSE
			SET_PED_MAX_MOVE_BLEND_RATIO(LocalPlayerPed, fBlockedRunningZoneSpeed)			
			SET_PED_MOVE_RATE_OVERRIDE(LocalPlayerPed, fBlockedRunningZoneSpeed)
		ENDIF
	ENDIF
	
	IF bInBlockedWeaponsZone
		IF wtWeaponBlockZonePreviousWeapon = WEAPONTYPE_INVALID
			GET_CURRENT_PED_WEAPON(LocalPlayerPed, wtWeaponBlockZonePreviousWeapon)
		ELSE
			PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES - DISABLING WEAPON WHEEL AND EQUIPPING UNARMED")
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_UD)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_LR)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV)
			SET_CURRENT_PED_WEAPON(LocalPlayerPed,WEAPONTYPE_UNARMED,TRUE)
		ENDIF
	ENDIF
	
	IF bInPlayerCantExitVehicleZone
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			DISABLE_VEHICLE_EXIT_THIS_FRAME()
		ENDIF
	ENDIF
	
	IF bInPlayerCantDriveVehicleZone
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			DISABLE_VEHICLE_MOVEMENT_CONTROLS_THIS_FRAME()
		ENDIF
	ENDIF
	
	IF bInBlockVTOLZone
		PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES - BLOCK VTOL TOGGLE")
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			VEHICLE_INDEX viMyVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
			IF DOES_THIS_MODEL_HAVE_VERTICAL_FLIGHT_MODE(GET_ENTITY_MODEL(viMyVeh))
				IF NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
					SET_DISABLE_VERTICAL_FLIGHT_MODE_TRANSITION(GET_VEHICLE_PED_IS_IN(LocalPlayerPed), TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: POISON GAS
//
//************************************************************************************************************************************************************

PROC SET_POISON_GAS_CHAMBER_STATE(POISON_GAS_CHAMBER_STATE eNewState)
	IF eCurrentPoisonGasChamberState != eNewState
		PRINTLN("[PGAS] Setting eCurrentPoisonGasChamberState to ", eCurrentPoisonGasChamberState)
		eCurrentPoisonGasChamberState = eNewState
	ENDIF
ENDPROC

FUNC BOOL USING_POISON_GAS()
	
	IF g_bMissionEnding
	OR g_bCelebrationScreenIsActive
		RETURN FALSE
	ENDIF
	
	IF eCurrentPoisonGasChamberState = POISON_GAS_CHAMBER_STATE__INIT
		
		INT iZone
		FOR iZone = 0 TO (MAX_ZONE_COUNT_TO_LOOP_TO() - 1)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iType = ciFMMC_ZONE_TYPE__HEALTH_DRAIN
				iCachedPoisonGasZoneIndex = iZone
				
				PRINTLN("[PGAS] USING_POISON_GAS - There's a poison gas zone at index ", iZone)
				SET_POISON_GAS_CHAMBER_STATE(POISON_GAS_CHAMBER_STATE__WAITING)
				RETURN TRUE
			ENDIF
		ENDFOR
		
		//If we made it here, we aren't going to be using poison gas at any point
		SET_POISON_GAS_CHAMBER_STATE(POISON_GAS_CHAMBER_STATE__DISABLED)
	
	ELIF eCurrentPoisonGasChamberState != POISON_GAS_CHAMBER_STATE__DISABLED
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_POISON_GAS_POSITION(INT iVentIndex)

	SWITCH iVentIndex
		CASE 0	RETURN <<2521.0, -226.2, -71.7>>
		CASE 1	RETURN <<2533.4, -238.5, -71.7>>
		CASE 2	RETURN <<2521.0, -250.9, -71.7>>
	ENDSWITCH

	RETURN <<0,0,0>>
ENDFUNC
FUNC VECTOR GET_POISON_GAS_ROTATION(INT iVentIndex)
	
	UNUSED_PARAMETER(iVentIndex)
	
	SWITCH iVentIndex
		CASE 1	RETURN <<0.0, 0.0, 90>>
	ENDSWITCH
	
	RETURN <<0, 0.0, 0.0>>
ENDFUNC

PROC START_POISON_GAS_FROM_VENT(INT iVentIndex)
	PRINTLN("[PGAS] Starting poison gas from vent ", iVentIndex)
	USE_PARTICLE_FX_ASSET("scr_ch_finale")
	
	pPoisonGasPTFX[iVentIndex] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_ch_finale_poison_gas", GET_POISON_GAS_POSITION(iVentIndex), GET_POISON_GAS_ROTATION(iVentIndex), 1.0, DEFAULT, DEFAULT, DEFAULT, TRUE)
	SET_PARTICLE_FX_LOOPED_COLOUR(pPoisonGasPTFX[iVentIndex], 255.0, 255.0, 255.0, TRUE)
	SET_PARTICLE_FX_LOOPED_ALPHA(pPoisonGasPTFX[iVentIndex], 255)
ENDPROC

PROC CLEAR_POISON_GAS_FROM_VENT(INT iVentIndex)
	IF DOES_PARTICLE_FX_LOOPED_EXIST(pPoisonGasPTFX[iVentIndex])
		STOP_PARTICLE_FX_LOOPED(pPoisonGasPTFX[iVentIndex], TRUE)
	ENDIF
ENDPROC

FUNC BOOL IS_LOCAL_PLAYER_WITHIN_POISON_GAS()

	IF NOT IS_NET_PLAYER_OK(LocalPlayer)
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bVaultPoisonGasDebug
		IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD2)
		AND IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD6)
			RETURN TRUE
		ENDIF
	ENDIF
	#ENDIF
	
	RETURN iPlayerWithinPoisonGasZoneBS > 0
ENDFUNC

FUNC BOOL SHOULD_POISON_GAS_DAMAGE_LOCAL_PLAYER()

	IF IS_LOCAL_PLAYER_WITHIN_POISON_GAS()
		IF fPoisonGasFillBuildup < cfPoisonGasBuildupRequiredBeforeDamaging
			RETURN FALSE
		ENDIF
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_POISON_GAS_ONLY_TRIGGER_IF_PLAYERS_ARE_IN_THE_AREA()
	IF IS_THIS_CASINO_HEIST_MISSION_A_DIRECT_APPROACH_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_START_POISON_GAS()
	
	#IF IS_DEBUG_BUILD
	IF bVaultPoisonGasDebug
		IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD2)
		AND IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
			RETURN TRUE
		ENDIF
	ENDIF
	#ENDIF
	
	IF SHOULD_POISON_GAS_ONLY_TRIGGER_IF_PLAYERS_ARE_IN_THE_AREA()
		IF NOT IS_LOCAL_PLAYER_WITHIN_POISON_GAS()
			// Only trigger if you're still there
			RETURN FALSE
		ENDIF
	ENDIF
	
	INT iTeam = MC_playerBD[iLocalPart].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iTeam > -1
	AND iRule > -1
	AND iRule < FMMC_MAX_RULES
	
		// Activate if my zone is enabled
		IF iCachedPoisonGasZoneIndex > -1
		
			IF SHOULD_ZONE_BE_DISABLED_THIS_FRAME(iCachedPoisonGasZoneIndex, iTeam)
				RETURN FALSE
			ELSE
				PRINTLN("[PGAS] SHOULD_START_POISON_GAS returning TRUE due to poison gas zone (", iCachedPoisonGasZoneIndex, ") being active!")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
PROC PROCESS_POISON_GAS_DEBUG()

	TEXT_LABEL_63 tlDebugText
	tlDebugText = "eCurrentPoisonGasChamberState = "
	tlDebugText += ENUM_TO_INT(eCurrentPoisonGasChamberState)
	DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.3, 0.6, 0.0>>, 255, 255, 255)
	
	DRAW_DEBUG_TEXT_2D("Numpad2 + Numpad5 to start gas", <<0.3, 0.65, 0.0>>)
	DRAW_DEBUG_TEXT_2D("Numpad2 + Numpad6 to damage player", <<0.3, 0.675, 0.0>>)
	
	IF IS_LOCAL_PLAYER_WITHIN_POISON_GAS()
		tlDebugText = "fPoisonGasDamageBuildup = "
		tlDebugText += FLOAT_TO_STRING(fPoisonGasDamageBuildup)
		DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.3, 0.7, 0.0>>)
		
		tlDebugText = "iPlayerWithinPoisonGasZoneBS = "
		tlDebugText += iPlayerWithinPoisonGasZoneBS
		DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.3, 0.75, 0.0>>)
		
		tlDebugText = "fPoisonGasFillBuildup = "
		tlDebugText += FLOAT_TO_STRING(fPoisonGasFillBuildup)
		DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.3, 0.8, 0.0>>)
		
		DRAW_DEBUG_LINE(GET_ENTITY_COORDS(LocalPlayerPed), GET_POISON_GAS_POSITION(0))
		DRAW_DEBUG_LINE(GET_ENTITY_COORDS(LocalPlayerPed), GET_POISON_GAS_POSITION(1))
	ENDIF
ENDPROC
#ENDIF

PROC APPLY_OR_REMOVE_POISON_GAS_SCREEN_EFFECT(BOOL bApply)

	IF GET_TIMECYCLE_TRANSITION_MODIFIER_INDEX() > -1
	OR GET_IS_TIMECYCLE_TRANSITIONING_OUT()
		EXIT
	ENDIF
	
	IF bApply
		//CLEAR_TIMECYCLE_MODIFIER()
		SET_TRANSITION_TIMECYCLE_MODIFIER("spectator6", cfPoisonGasFXTransitionTime)
		
		SET_BIT(iPoisonGasZoneBS, ciPOISONGASZONEBS_AppliedEffects)
		SHAKE_GAMEPLAY_CAM("DRUNK_SHAKE", 1.0)
		
	ELSE
		SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(cfPoisonGasFXTransitionTime)
		
		CLEAR_BIT(iPoisonGasZoneBS, ciPOISONGASZONEBS_AppliedEffects)
		STOP_GAMEPLAY_CAM_SHAKING()
		
	ENDIF
ENDPROC

FUNC BOOL SHOULD_POISON_GAS_STOP()

	INT iTeam
	FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		
		IF iRule < FMMC_MAX_RULES
			IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_STOP_POISON_GAS)
					PRINTLN("[PGAS] SHOULD_POISON_GAS_STOP returning TRUE due to ciBS_RULE13_STOP_POISON_GAS")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR

	RETURN FALSE
ENDFUNC

PROC CLEANUP_POISON_GAS(BOOL bFinalCleanup = TRUE)
	
	INT iVent
	FOR iVent = 0 TO ciPoisonGasVents - 1
		CLEAR_POISON_GAS_FROM_VENT(iVent)
	ENDFOR
	
	IF bFinalCleanup
	
		IF DOES_PARTICLE_FX_LOOPED_EXIST(pPoisonGasMainPTFX)
			STOP_PARTICLE_FX_LOOPED(pPoisonGasMainPTFX, TRUE)
		ENDIF
	
		IF HAS_NAMED_PTFX_ASSET_LOADED("scr_ch_finale")
			REMOVE_NAMED_PTFX_ASSET("scr_ch_finale")
		ENDIF
		
		REMOVE_ANIM_DICT("anim@fidgets@coughs")
		REMOVE_ANIM_SET("anim@fidgets@coughs")
	ENDIF
	
	iPoisonGasZoneBS = 0
	fPoisonGasDamageBuildup = 0.0
ENDPROC

FUNC BOOL CAN_PLAYER_COUGH()
	
	IF HAS_NET_TIMER_STARTED(stPoisonGasCoughCooldownTimer)
		RETURN FALSE
	ENDIF
	
	IF IS_PED_JUMPING(LocalPlayerPed)
	OR IS_PED_SPRINTING(LocalPlayerPed)
	OR IS_PED_RAGDOLL(LocalPlayerPed)
	OR IS_PED_CLIMBING(LocalPlayerPed)
	OR IS_PED_DEAD_OR_DYING(LocalPlayerPed)
	OR IS_PED_GESTURING(LocalPlayerPed)
	OR IS_PED_GOING_INTO_COVER(LocalPlayerPed)
	OR IS_PED_VAULTING(LocalPlayerPed)
	OR IS_PED_IN_COVER(LocalPlayerPed)
	OR GET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_IsAimingGun)
	OR MC_playerBD[iLocalPart].iObjHacking > -1
	OR IS_BIT_SET(iLocalBoolCheck32, LBOOL32_USING_HANDHELD_DRILL)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC INT GET_POISON_GAS_COUGH_COOLDOWN_LENGTH()
	FLOAT t = LERP_FLOAT(TO_FLOAT(ciPoisonGasCoughCooldownLength_NoGas), TO_FLOAT(ciPoisonGasCoughCooldownLength_FullGas), fPoisonGasFillBuildup)
	RETURN ROUND(t)
ENDFUNC

FUNC FLOAT GET_POISON_GAS_COUGH_CHANCE()
	RETURN LERP_FLOAT(cfPoisonGasCoughChance_NoGas, cfPoisonGasCoughChance_FullGas, fPoisonGasFillBuildup)
ENDFUNC

FUNC STRING GET_POISON_GAS_COUGH_SOUND_EFFECT()

	SWITCH GET_PARTICIPANT_NUMBER_IN_TEAM()
		CASE 0
			IF IS_PLAYER_FEMALE()
				RETURN "Female_01"
			ELSE
				RETURN "Male_01"
			ENDIF
		BREAK
		CASE 1
			IF IS_PLAYER_FEMALE()
				RETURN "Female_02"
			ELSE
				RETURN "Male_02"
			ENDIF
		BREAK
		CASE 2
			IF IS_PLAYER_FEMALE()
				RETURN "Female_03"
			ELSE
				RETURN "Male_03"
			ENDIF
		BREAK
		CASE 3
			IF IS_PLAYER_FEMALE()
				RETURN "Female_04"
			ELSE
				RETURN "Male_04"
			ENDIF
		BREAK
	ENDSWITCH

	IF IS_PLAYER_FEMALE()
		RETURN "Female_01"
	ELSE
		RETURN "Male_01"
	ENDIF
ENDFUNC

PROC PROCESS_POISON_GAS_CHAMBER()

	INT iVent = 0
	INT iNewHealth
	ANIM_DATA sAnimData, sAnimDataNull1, sAnimDataNull2
	sAnimData.type = APT_SINGLE_ANIM
	sAnimData.phase0 = 0.0
	sAnimData.rate0 = 1.0
	sAnimData.filter = GET_HASH_KEY("BONEMASK_UPPERONLY")
	sAnimData.dictionary0 = "anim@fidgets@coughs"
	sAnimData.flags = (AF_UPPERBODY | AF_SECONDARY | AF_ADDITIVE)
	STRING sFacialAnimName
	
	#IF IS_DEBUG_BUILD
	BOOL bPGasDebug = bVaultPoisonGasDebug AND IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD2)
	IF bPGasDebug
		PROCESS_POISON_GAS_DEBUG()
	ENDIF
	#ENDIF

	SWITCH eCurrentPoisonGasChamberState
		CASE POISON_GAS_CHAMBER_STATE__INIT
			IF USING_POISON_GAS()
				SET_POISON_GAS_CHAMBER_STATE(POISON_GAS_CHAMBER_STATE__WAITING)
			ENDIF
		BREAK
		
		CASE POISON_GAS_CHAMBER_STATE__WAITING
			IF IS_BIT_SET(iPoisonGasZoneBS, ciPOISONGASZONEBS_TriggerGas)
				SET_POISON_GAS_CHAMBER_STATE(POISON_GAS_CHAMBER_STATE__DEPLOYING)
			ELSE
				IF SHOULD_START_POISON_GAS()
					BROADCAST_FMMC_POISON_GAS_EVENT(TRUE)
				ENDIF
			ENDIF
		BREAK
		
		CASE POISON_GAS_CHAMBER_STATE__DEPLOYING
		
			USE_PARTICLE_FX_ASSET("scr_ch_finale")
			pPoisonGasMainPTFX = START_PARTICLE_FX_LOOPED_AT_COORD("scr_ch_finale_vault_haze", (<<2527.0, -238.5, -71.8>>), (<<0,0,0>>), 1.0, DEFAULT, DEFAULT, DEFAULT, TRUE)
	
			FOR iVent = 0 TO ciPoisonGasVents - 1
				START_POISON_GAS_FROM_VENT(iVent)
			ENDFOR
			
			IF bIsLocalPlayerHost
				SET_BIT(MC_serverBD.iServerBitSet1, SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_0)
				SET_BIT(MC_serverBD.iServerBitSet1, SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_1)
				SET_BIT(MC_serverBD.iServerBitSet1, SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_2)
				SET_BIT(MC_serverBD.iServerBitSet1, SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_3)
			ENDIF
			
			REINIT_NET_TIMER(stPoisonGasRunningTimer)
			SET_POISON_GAS_CHAMBER_STATE(POISON_GAS_CHAMBER_STATE__RELEASED)
		BREAK
		
		CASE POISON_GAS_CHAMBER_STATE__RELEASED
						
			fPoisonGasFillBuildup += GET_FRAME_TIME() * cfPoisonGasFillBuildupRate
			fPoisonGasFillBuildup = CLAMP(fPoisonGasFillBuildup, 0.0, 1.0)
			SET_PARTICLE_FX_LOOPED_EVOLUTION(pPoisonGasMainPTFX, "fill", fPoisonGasFillBuildup, TRUE)
			SET_PARTICLE_FX_LOOPED_EVOLUTION(pPoisonGasMainPTFX, "fade", fPoisonGasFillBuildup, TRUE)
			
			IF SHOULD_POISON_GAS_DAMAGE_LOCAL_PLAYER()
			
				IF NOT IS_BIT_SET(iPoisonGasZoneBS, ciPOISONGASZONEBS_AppliedEffects)
					APPLY_OR_REMOVE_POISON_GAS_SCREEN_EFFECT(TRUE)
				ENDIF
				
				fPoisonGasDamageBuildup += GET_FRAME_TIME() * cfPoisonGasDamageBuildupRate
				
				IF HAS_NET_TIMER_STARTED(stPoisonGasCoughCooldownTimer)
				AND HAS_NET_TIMER_EXPIRED(stPoisonGasCoughCooldownTimer, GET_POISON_GAS_COUGH_COOLDOWN_LENGTH())
					RESET_NET_TIMER(stPoisonGasCoughCooldownTimer)
				ENDIF
				
				IF fPoisonGasDamageBuildup >= 1.0
					INT iDamage
					iDamage = ROUND(LERP_FLOAT(cfPoisonGasDamagePerTick_Start, cfPoisonGasDamagePerTick_End, fPoisonGasFillBuildup * fPoisonGasFillBuildup_DamageRampSpeedMultiplier))
					iNewHealth = GET_ENTITY_HEALTH(LocalPlayerPed) - iDamage
					
					IF CAN_PLAYER_COUGH()
						IF GET_RANDOM_FLOAT_IN_RANGE(0.0, 1.0) < GET_POISON_GAS_COUGH_CHANCE()
						OR NOT IS_BIT_SET(iLocalBoolCheck32, LBOOL32_DONE_FIRST_COUGH)
							iPoisonGasCoughAnim = GET_RANDOM_INT_IN_RANGE(0, 3)
							
							SWITCH iPoisonGasCoughAnim
								CASE 0	
									sAnimData.anim0 = "COUGH_A"
									sFacialAnimName = "COUGH_A_FACIAL"
								BREAK
								CASE 1	
									sAnimData.anim0 = "COUGH_B"
									sFacialAnimName = "COUGH_B_FACIAL"
								BREAK
								CASE 2	
									sAnimData.anim0 = "COUGH_C"
									sFacialAnimName = "COUGH_C_FACIAL"
								BREAK
							ENDSWITCH
							
							TASK_SCRIPTED_ANIMATION(LocalPlayerPed, sAnimData, sAnimDataNull1, sAnimDataNull2)
							PLAY_FACIAL_ANIM(LocalPlayerPed, sFacialAnimName, sAnimData.dictionary0)
							REINIT_NET_TIMER(stPoisonGasCoughCooldownTimer)
							SET_BIT(iLocalBoolCheck32, LBOOL32_DONE_FIRST_COUGH)
							CLEAR_BIT(iLocalBoolCheck33, LBOOL33_PLAYED_COUGH_SOUND_EFFECT)
						ENDIF
					ENDIF
					
					IF NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
					AND NOT GET_PLAYER_INVINCIBLE(LocalPlayer)
					AND NOT GET_PLAYER_DEBUG_INVINCIBLE(LocalPlayer)
						SET_ENTITY_HEALTH(LocalPlayerPed, iNewHealth)
					ENDIF
					
					fPoisonGasDamageBuildup -= 1.0
					PRINTLN("[PGAS] Damaging the player now for ", iDamage, " damage! HP is now ", iNewHealth)
				ENDIF
				
				IF NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_PLAYED_COUGH_SOUND_EFFECT)
					IF HAS_NET_TIMER_STARTED(stPoisonGasCoughCooldownTimer)
						INT iCoughTime
						SWITCH iPoisonGasCoughAnim
							CASE 0	
								iCoughTime = 750
							BREAK
							CASE 1	
								iCoughTime = 275
							BREAK
							CASE 2	
								iCoughTime = 335
							BREAK
						ENDSWITCH
						
						IF HAS_NET_TIMER_EXPIRED(stPoisonGasCoughCooldownTimer, iCoughTime)
							PRINTLN("[PGAS] Playing cough sound effect: ", GET_POISON_GAS_COUGH_SOUND_EFFECT())
							PLAY_SOUND_FROM_ENTITY(-1, GET_POISON_GAS_COUGH_SOUND_EFFECT(), LocalPlayerPed, "dlc_ch_heist_finale_poison_gas_coughs_sounds", TRUE, 500)
							SET_BIT(iLocalBoolCheck33, LBOOL33_PLAYED_COUGH_SOUND_EFFECT)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF SHOULD_POISON_GAS_STOP()
				PRINTLN("[PGAS] Stopping poison gas and going back to the init state!")
				CLEANUP_POISON_GAS(FALSE)
				SET_POISON_GAS_CHAMBER_STATE(POISON_GAS_CHAMBER_STATE__INIT)
			ENDIF
		BREAK
	ENDSWITCH
	
	IF IS_BIT_SET(iPoisonGasZoneBS, ciPOISONGASZONEBS_AppliedEffects)
		IF NOT SHOULD_POISON_GAS_DAMAGE_LOCAL_PLAYER()
			APPLY_OR_REMOVE_POISON_GAS_SCREEN_EFFECT(FALSE)
		ENDIF
	ENDIF
				
ENDPROC

