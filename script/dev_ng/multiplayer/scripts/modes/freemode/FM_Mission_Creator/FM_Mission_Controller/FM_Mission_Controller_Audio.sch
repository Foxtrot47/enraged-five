
USING "FM_Mission_Controller_USING.sch"
USING "FM_Mission_Controller_Utility.sch"
USING "FM_Mission_Controller_Entities.sch"
USING "FM_Mission_Controller_ObjectiveCompletion.sch"
USING "FM_Mission_Controller_SpecialCase.sch"
USING "FM_Mission_Controller_Rules.sch"
USING "FM_Mission_Controller_Doors.sch"
USING "FM_Mission_Controller_Cutscene.sch"
USING "FM_Mission_Controller_Debug.sch"
USING "FM_Mission_Controller_Locations.sch"
USING "FM_Mission_Controller_Vehicles.sch"
USING "FM_Mission_Controller_Peds_Brain.sch"
USING "FM_Mission_Controller_Peds_Body.sch"
USING "FM_Mission_Controller_Peds.sch"
USING "FM_Mission_Controller_GangChase.sch"
USING "FM_Mission_Controller_Minigame.sch"
USING "FM_Mission_Controller_Objects.sch"
USING "FM_Mission_Controller_Props.sch"



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: MISSION AUDIO !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: AUDIO MIX !
//
//************************************************************************************************************************************************************



FUNC STRING GET_AUDIO_SCENE_FROM_MIX_INDEX(INT iAudioMix)

	SWITCH(INT_TO_ENUM(eAUDIO_MIXES,iAudioMix))
				
		CASE AUDIO_MIX_GUNFIGHT		RETURN "DLC_HEIST_MISSION_GUNFIGHT_SCENE"
		CASE AUDIO_MIX_CAR_CHASE	RETURN "DLC_HEIST_MISSION_DRIVING_SCENE"
	
	ENDSWITCH
	
	RETURN ""

ENDFUNC

PROC STOP_ANY_CURRENTLY_PLAYING_AUDIO_MIX()
	IF iLocalCurrentAudioMix != -1
		STRING sAudio = GET_AUDIO_SCENE_FROM_MIX_INDEX(iLocalCurrentAudioMix)	
		IF IS_AUDIO_SCENE_ACTIVE(sAudio)
			STOP_AUDIO_SCENE(sAudio)
			PRINTLN("[RCC MISSION][MJM] STOP_ANY_CURRENTLY_PLAYING_AUDIO_MIX - STOP_AUDIO_SCENE :",sAudio)
		ENDIF
		iLocalCurrentAudioMix = -1
		PRINTLN("[RCC MISSION][MJM] STOP_ANY_CURRENTLY_PLAYING_AUDIO_MIX - iLocalCurrentAudioMix = -1 :")
	ENDIF
ENDPROC

PROC PROCESS_PLANE_NOISE()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour,ciLOUD_PLANES)
		IF NOT IS_AUDIO_SCENE_ACTIVE("Speed_Race_Airport_Scene")
			START_AUDIO_SCENE("Speed_Race_Airport_Scene")
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_DOWNLOAD_SOUND_AFTER_DIALOGUE_TRIGGER()

	INT iTeam = MC_PlayerBD[iPartToUse].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]

	IF iRule < FMMC_MAX_RULES
	AND iRule > -1
		INT iRequiredDialogue = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPlayDownloadSoundAfterDialogue[iRule]
		
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
			CLEAR_BIT(iLocalBoolCheck27, LBOOL27_PLAYED_DOWNLOAD_SOUND_THIS_RULE)
			RESET_NET_TIMER(stDownloadSoundTimer)
			EXIT
		ENDIF
		
		IF iRequiredDialogue > -1		
			IF IS_BIT_SET(MC_playerBD[iPartToUse].iDialoguePlayedBS[GET_LONG_BITSET_INDEX(iRequiredDialogue)], GET_LONG_BITSET_BIT(iRequiredDialogue)) //chosen dialogue has happened
			AND NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_PLAYED_DOWNLOAD_SOUND_THIS_RULE)
				IF HAS_NET_TIMER_STARTED(stDownloadSoundTimer)
					IF HAS_NET_TIMER_EXPIRED(stDownloadSoundTimer, ciTimeBeforeDownloadSound)
						SET_BIT(iLocalBoolCheck27, LBOOL27_PLAYED_DOWNLOAD_SOUND_THIS_RULE)
						PLAY_SOUND_FRONTEND(-1, "Goggles_Update", "DLC_XM17_Silo_Pred_Sounds")
						PRINTLN("PROCESS_DOWNLOAD_SOUND_AFTER_DIALOGUE_TRIGGER - Playing download sound now because dialogue ", iRequiredDialogue, " has played!")
					ELSE
						PRINTLN("PROCESS_DOWNLOAD_SOUND_AFTER_DIALOGUE_TRIGGER - Waiting for stDownloadSoundTimer to finish...")
					ENDIF
				ELSE
					START_NET_TIMER(stDownloadSoundTimer)
					PRINTLN("PROCESS_DOWNLOAD_SOUND_AFTER_DIALOGUE_TRIGGER - Starting stDownloadSoundTimer...")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC
  
FUNC BOOL REINIT_MUSIC_FOR_SPECTATOR()
	/*	When going into spectator mode it will clear LBOOL3_MUSIC_PLAYING but will 
		not clear LBOOL_MUSIC_CHANGED which PROCESS_MUSIC needs to trigger a score	*/
	IF iSpectatorTarget != -1
	AND NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
		IF NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_REINITTED_MUSIC_FOR_ENTERING_SPECTATE)
			SET_BIT(iLocalBoolCheck13, LBOOL13_REINITTED_MUSIC_FOR_ENTERING_SPECTATE)
			CLEAR_BIT(iLocalBoolCheck3,LBOOL3_MUSIC_PLAYING)
			CLEAR_BIT(iLocalBoolCheck,LBOOL_MUSIC_CHANGED)
			PRINTLN("[JJT] REINIT_MUSIC_FOR_SPECTATOR - Force initialising music for spectator.")
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PLAY_AUDIO_SCENE(STRING sAudioMix)
	
	IF ARE_STRINGS_EQUAL(sAudioMix, "DLC_HEIST_MISSION_GUNFIGHT_SCENE")
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRQUOTA(g_FMMC_STRUCT.iAdversaryModeType)
			PRINTLN("SHOULD_PLAY_AUDIO_SCENE - Not allowing ", sAudioMix, " because this is Air Quota")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_AUDIO_MIX()

	INT iTeam = MC_playerBD[iPartToUse].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	PROCESS_PLANE_NOISE()
	
	IF NOT IS_CUTSCENE_PLAYING()
		IF iRule < FMMC_MAX_RULES AND iRule >= 0
	
			IF iLocalAudioMixPriority != iRule
		
				PRINTLN("[RCC MISSION][MJM] PROCESS_AUDIO_MIX - New rule, checking audio mixes, Rule: ",iRule)

				STRING sAudioMix = GET_AUDIO_SCENE_FROM_MIX_INDEX(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAudioMix[iRule])		
				
				PRINTLN("[RCC MISSION][MJM] PROCESS_AUDIO_MIX - Audio Mix for rule is ",sAudioMix)
				
				IF NOT ARE_STRINGS_EQUAL("",sAudioMix)
					IF NOT IS_AUDIO_SCENE_ACTIVE(sAudioMix)
					AND SHOULD_PLAY_AUDIO_SCENE(sAudioMix)
						STOP_ANY_CURRENTLY_PLAYING_AUDIO_MIX()
						START_AUDIO_SCENE(sAudioMix)
						iLocalCurrentAudioMix = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAudioMix[iRule]
						PRINTLN("[RCC MISSION][MJM] PROCESS_AUDIO_MIX - START_AUDIO_SCENE : ", sAudioMix,": ",iLocalCurrentAudioMix)
					ENDIF
				ELSE
					STOP_ANY_CURRENTLY_PLAYING_AUDIO_MIX()
				ENDIF
				
				iLocalAudioMixPriority = iRule
				CLEAR_BIT(iLocalBoolCheck9,LBOOL9_MIDPOINT_AUDIO_MIX_PROCESSED)

			ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidPointAudioMix[iRule] >= 0
			AND IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam],iRule)
			AND NOT IS_BIT_SET(iLocalBoolCheck9,LBOOL9_MIDPOINT_AUDIO_MIX_PROCESSED)
			
				PRINTLN("[RCC MISSION][MJM] PROCESS_AUDIO_MIX - New midpoint, checking audio mixes, Rule: ",iRule)
				
				STRING sAudioMix = GET_AUDIO_SCENE_FROM_MIX_INDEX(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidPointAudioMix[iRule])		
					
				PRINTLN("[RCC MISSION][MJM] PROCESS_AUDIO_MIX - Audio Mix for rule midpoint is ",sAudioMix)
				
				IF NOT ARE_STRINGS_EQUAL("",sAudioMix)
					IF NOT IS_AUDIO_SCENE_ACTIVE(sAudioMix)
					AND SHOULD_PLAY_AUDIO_SCENE(sAudioMix)
						STOP_ANY_CURRENTLY_PLAYING_AUDIO_MIX()
						START_AUDIO_SCENE(sAudioMix)
						iLocalCurrentAudioMix = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidPointAudioMix[iRule]
						PRINTLN("[RCC MISSION][MJM] PROCESS_AUDIO_MIX - START_AUDIO_SCENE : ", sAudioMix,": ",iLocalCurrentAudioMix)
					ENDIF
				ELSE
					STOP_ANY_CURRENTLY_PLAYING_AUDIO_MIX()
				ENDIF

				SET_BIT(iLocalBoolCheck9,LBOOL9_MIDPOINT_AUDIO_MIX_PROCESSED)
				
			ENDIF
		ENDIF
	ELSE
		//Turn off audio mixes during cutscenes and reset afterwards
		STOP_ANY_CURRENTLY_PLAYING_AUDIO_MIX()
		iLocalAudioMixPriority = -1
	ENDIF
	
ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: MUSIC !
//
//************************************************************************************************************************************************************



///PURPOSE: This function sets a global in the global player broadcast data when the CTF
///    countdown music is playing for anyone to check that information if necessary
PROC SET_CTF_MUSIC_GLOBAL()
	g_bCTFCountdownMusicPlaying = TRUE
ENDPROC

///PURPOSE: This function clears a global in the global player broadcast data that is set
///    when the CTF countdown music is playing for anyone to check that information if necessary
PROC CLEAR_CTF_MUSIC_GLOBAL()
	g_bCTFCountdownMusicPlaying = FALSE
ENDPROC

#IF IS_DEBUG_BUILD
FUNC STRING GET_MUSIC_STATE_NAME(INT istate)

	SWITCH istate

		CASE MUSIC_SILENT
			RETURN "MUSIC_SILENT"
		BREAK
		
		CASE MUSIC_SUSPENSE
			RETURN "MUSIC_SUSPENSE"
		BREAK
		
		CASE MUSIC_ACTION
			RETURN "MUSIC_ACTION"
		BREAK
		
		CASE MUSIC_VEHICLE_CHASE
			RETURN "MUSIC_VEHICLE_CHASE"
		BREAK
		
		CASE MUSIC_AIRBORNE
			RETURN "MUSIC_AIRBORNE"
		BREAK

		CASE MUSIC_COCK_SONG
			RETURN "MUSIC_COCK_SONG"
		BREAK
		
		CASE MUSIC_WAVERY
			RETURN "MUSIC_WAVERY"
		BREAK
		
		CASE MUSIC_SEA_RACE
			RETURN "MUSIC_SEA_RACE"
		BREAK
		
		CASE MUSIC_STOP
			RETURN "MUSIC_STOP"
		BREAK
		
		CASE MUSIC_FAIL
			RETURN "MUSIC_FAIL"
		BREAK
		
		CASE MUSIC_LTS_ENDING
			RETURN "MUSIC_LTS_ENDING"
		BREAK
		
		CASE MUSIC_CTF_ENDING
			RETURN "MUSIC_CTF_ENDING"
		BREAK
		
		CASE MUSIC_MID_COUNTDOWN
			RETURN "MUSIC_MID_COUNTDOWN"
		BREAK
		
		CASE MUSIC_MOOD_NONE
			RETURN "MUSIC_MOOD_NONE"
		BREAK
		
		CASE MUSIC_DRIVING
			RETURN "MUSIC_DRIVING"
		BREAK
		
		CASE MUSIC_MED_INTENSITY
			RETURN "MUSIC_MED_INTENSITY"
		BREAK
		
		CASE MUSIC_SUSPENSE_LOW
			RETURN "MUSIC_SUSPENSE_LOW"
		BREAK
		
		CASE MUSIC_SUSPENSE_HIGH
			RETURN "MUSIC_SUSPENSE_HIGH"
		BREAK

	ENDSWITCH
	
	RETURN "INVALID_MUSIC"
		
ENDFUNC
#ENDIF

FUNC INT GET_ROLE_MUSIC_MOOD()

	INT iTeam = MC_PlayerBD[iPartToUse].iteam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]

	IF iRule < FMMC_MAX_RULES
		IF MC_playerBD[iPartToUse].iCoronaRole = 1
			IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicSecondaryRoleMood[iRule] != -1
				RETURN g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicSecondaryRoleMood[iRule]
			ENDIF
		ENDIF
				
		PRINTLN("GET_ROLE_MUSIC_MOOD iMusicScoreReachedMood: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScoreReachedMood[iRule], " On irule: ", iRule)
				
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScoreReachedMood[iRule] > 0			
			IF MC_serverBD.iScoreOnThisRule[iTeam] >= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScoreReachedMood[iRule]
				PRINTLN("[PROCESS_MUSIC][GET_ROLE_MUSIC_MOOD][RCC MISSION] iMusicScoreReachedMood Set to: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScoreReachedMood[iRule], " and iScoreOnThisRule is: ", MC_serverBD.iScoreOnThisRule[iTeam], " Overriding Music Cue to the midpoint: ",  g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicMid[iRule])
				RETURN g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicMid[iRule]
			ENDIF
		ENDIF
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScoreAggroMood[iRule] > 0
			IF HAS_ANY_TEAM_TRIGGERED_AGGRO()
				PRINTLN("[PROCESS_MUSIC][GET_ROLE_MUSIC_MOOD][RCC MISSION] iMusicScoreAggroMood Set to: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScoreAggroMood[iRule])
		 		RETURN g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScoreAggroMood[iRule]
			ENDIF
		ENDIF
	ENDIF
	
	RETURN g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicStart[iRule]
ENDFUNC

PROC SET_MUSIC_STATE(INT istate)
	IF MC_PlayerBD[iPartToUse].iMusicState != istate
	OR REINIT_MUSIC_FOR_SPECTATOR()	
		
		PRINTLN("[PROCESS_MUSIC][SET_MUSIC_STATE][RCC MISSION] Called, istate: ", istate)
		
		IF MC_PlayerBD[iPartToUse].iMusicState = MUSIC_SILENT
			IF istate = MUSIC_SUSPENSE
			OR istate = MUSIC_ACTION
			OR istate = MUSIC_VEHICLE_CHASE
			OR istate = MUSIC_AIRBORNE
				TRIGGER_MUSIC_EVENT("MP_MC_RADIO_OUT_SCORE_IN") 
				PRINTLN("[PROCESS_MUSIC][SET_MUSIC_STATE][RCC MISSION] MUSIC CALLED : MP_MC_RADIO_OUT_SCORE_IN ")
			ENDIF
		ENDIF
		
		//-- For score help text
		IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet9, BI_FM_NMH9_LISTEN_SCORE)
			IF istate = MUSIC_SILENT
				IF IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet9, BI_FM_NMH9_DO_SCORE_HELP)
					CLEAR_BIT(MPGlobalsAmbience.iFmNmhBitSet9, BI_FM_NMH9_DO_SCORE_HELP)
					PRINTLN("[PROCESS_MUSIC][SET_MUSIC_STATE][RCC MISSION] listen To Score help - cleared BI_FM_NMH9_DO_SCORE_HELP ")
				ENDIF
			ELSE
				// B*2221389 - Clear the bit if the player is doing Humane Labs EMP and in a plane to stop help text
				IF g_FMMC_STRUCT.iRootContentIdHash = g_sMPTUNABLES.iroot_id_HASH_Humane_Labs_EMP
				AND IS_PED_IN_ANY_PLANE(LocalPlayerPed)
					CLEAR_BIT(MPGlobalsAmbience.iFmNmhBitSet9, BI_FM_NMH9_DO_SCORE_HELP)
					PRINTLN("[PROCESS_MUSIC][SET_MUSIC_STATE][RCC MISSION] listen To Score help - cleared BI_FM_NMH9_DO_SCORE_HELP for Humane Labs - EMP")
				ELSE
					IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet9, BI_FM_NMH9_DO_SCORE_HELP)
						SET_BIT(MPGlobalsAmbience.iFmNmhBitSet9, BI_FM_NMH9_DO_SCORE_HELP)
						PRINTLN("[PROCESS_MUSIC][SET_MUSIC_STATE][RCC MISSION] listen To Score help - Set BI_FM_NMH9_DO_SCORE_HELP istate = ", istate)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF iSpectatorTarget = -1
			MC_PlayerBD[iPartToUse].iMusicState = istate
		ENDIF
		
		REINIT_NET_TIMER(tdMusicTimer)
		CLEAR_BIT(iLocalBoolCheck,LBOOL_MUSIC_CHANGED) 
		PRINTLN("[PROCESS_MUSIC][SET_MUSIC_STATE][RCC MISSION] MUSIC STATE CHANGE TO: ",GET_MUSIC_STATE_NAME(istate))
	ENDIF 
ENDPROC

PROC PROCESS_COUNTDOWN_TIMER_SOUNDS()

	IF NOT IS_SOUND_ID_VALID(iSoundIDCountdown)
		iSoundIDCountdown = GET_SOUND_ID()
		PRINTLN("[PROCESS_MUSIC][PROCESS_COUNTDOWN_TIMER_SOUNDS] - GET_SOUND_ID iSoundIDCountdown ",iSoundIDCountdown)
	ENDIF
	
	PED_INDEX PedToUse = LocalPlayerPed
	INT iCountdownPartToUse = iPartToUse
	IF (IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN) OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_ANY_SPECTATOR) OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitset, PBBOOL_HEIST_SPECTATOR))
	AND iSpectatorTarget != -1
		iCountdownPartToUse = iSpectatorTarget
		
		IF iCountdownPartToUse > -1
			PedToUse = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iCountdownPartToUse)))
		ENDIF
		PRINTLN("[PROCESS_MUSIC][PCTS] PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN set.")
	ENDIF
	
	IF GET_PLAYER_RESPAWN_BACK_AT_START_STATE(iPartToUse) != PRBSS_PlayingNormally
	AND GET_PLAYER_RESPAWN_BACK_AT_START_STATE(iPartToUse) != PRBSS_Invalid
		PRINTLN("[PROCESS_MUSIC][PCTS] GET_PLAYER_RESPAWN_BACK_AT_START_STATE != PRBSS_PlayingNormally or PRBSS_Invalid.")
		IF IS_SOUND_ID_VALID(iSoundIDCountdown)
		AND NOT HAS_SOUND_FINISHED(iSoundIDCountdown)
			PRINTLN("[PROCESS_MUSIC][PCTS] Stopping iSoundIDCountdown: ", iSoundIDCountdown)
			STOP_SOUND(iSoundIDCountdown)
		ENDIF
		CLEAR_BIT(iLocalBoolCheck5, LBOOL5_STARTED_COUNTDOWN_TIMER_SOUNDS)
		EXIT
	ENDIF
	
	INT iTeam = MC_playerBD[iCountdownPartToUse].iteam
	INT iPriority = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iPriority < FMMC_MAX_RULES
		INT iTimeleft = GET_REMAINING_TIME_ON_RULE(iTeam, iPriority)
				
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
			PRINTLN("[PROCESS_MUSIC][PCTS] Rule Changed to: ", iPriority, ". Stopping the countdown music sound from playing.")
			
			IF IS_SOUND_ID_VALID(iSoundIDCountdown)
			AND NOT HAS_SOUND_FINISHED(iSoundIDCountdown)
				PRINTLN("[PROCESS_MUSIC][PCTS] Stopping iSoundIDCountdown: ", iSoundIDCountdown)
				STOP_SOUND(iSoundIDCountdown)
			ELSE
				PRINTLN("[PROCESS_MUSIC][PCTS] Already Stopped/Finished?")
			ENDIF
			
			iTimeleft = GET_REMAINING_TIME_ON_RULE(iTeam, iPriority)
			CLEAR_BIT(iLocalBoolCheck5, LBOOL5_STARTED_COUNTDOWN_TIMER_SOUNDS)
			
			PRINTLN("[PROCESS_MUSIC][PCTS] iTimeleft: ", iTimeleft)
			EXIT
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_EnableGameMastersMode)
			iTimeleft = GET_TIME_REMAINING_ON_MULTIRULE_TIMER(iTeam)
			PRINTLN("[PROCESS_MUSIC][PCTS] (multirule) iTimeleft: ", iTimeleft)
		ENDIF
		
		IF iTimeLeft <= 0
		AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_EnableGameMastersMode)
			PRINTLN("[PROCESS_MUSIC][PCTS] (Multi) iTimeleft: ", iTimeleft, " Exitting, as we are <= 0.")
			EXIT
		ENDIF
		
		IF iTimeLeft <= 0
		AND HAS_NET_TIMER_STARTED(MC_serverBD_3.tdObjectiveLimitTimer[iTeam])	
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_EnableGameMastersMode)
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VENETIAN_JOB(g_FMMC_STRUCT.iAdversaryModeType)
				IF IS_BIT_SET(MC_serverBD.iServerBitSet6, SBBOOL6_TIME_DEDUCTED_INTO_30S)
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciUSE_FREEMODE_30S_COUNTDOWN)
						TRIGGER_MUSIC_EVENT("FM_COUNTDOWN_30S_KILL")
					ELSE
						TRIGGER_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
					ENDIF
				ENDIF
			ENDIF
			
			PRINTLN("[PROCESS_MUSIC][PCTS] iTimeleft: ", iTimeleft, " Exitting, as we are <= 0.")
			EXIT
		ENDIF
		
		//[KH] stops the bounds timer sound if the player dies during the countdown
		IF IS_PED_INJURED(PedToUse)
			IF IS_SOUND_ID_VALID(iBoundsTimerSound)
			AND NOT HAS_SOUND_FINISHED(iBoundsTimerSound)
				PRINTLN("[PROCESS_MUSIC][PCTS] PROCESS_COUNTDOWN_TIMER_SOUNDS - Died outside of bounds, stopping bounds timer sound.")
				STOP_SOUND(iBoundsTimerSound)				
			ELIF SHOULD_RULE_EXPLODE_ON_FAIL(iTeam, iPriority, DEFAULT, TRUE)
			AND IS_SOUND_ID_VALID(iBoundsExplodeSound)
			AND NOT HAS_SOUND_FINISHED(iBoundsExplodeSound)
				PRINTLN("[PROCESS_MUSIC][PCTS] PROCESS_COUNTDOWN_TIMER_SOUNDS - Player is dead, stop explosion timer countdown sound.")
				STOP_SOUND(iBoundsExplodeSound)
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(iLocalBoolCheck5, LBOOL5_STARTED_COUNTDOWN_TIMER_SOUNDS)
		AND ( HAS_NET_TIMER_STARTED(MC_serverBD_3.tdObjectiveLimitTimer[iTeam])
			  OR HAS_NET_TIMER_STARTED(MC_serverBD_3.tdMultiObjectiveLimitTimer[iteam]) )

			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iPriority], ciBS_RULE6_COUNTDOWN_TIMER_SOUND_THIRTYSECONDS)
			OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciALLOW_30S_COUNTDOWN_END_OF_ANY_ROUND)
			OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_EnableGameMastersMode)
				AND (MC_serverBD_3.iMultiObjectiveTimeLimit[iTeam] - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeam])< 60000))
		
				IF HAS_NET_TIMER_STARTED( MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeam] )
				AND MC_serverBD_3.iMultiObjectiveTimeLimit[iTeam] - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeam]) > 50
				AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_EnableGameMastersMode)
					iTimeLeft = MC_serverBD_3.iMultiObjectiveTimeLimit[iTeam] - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeam])
				ENDIF
				
				IF IS_BIT_SET(iLocalBoolCheck15, LBOOL15_RESET_30S_CD_BOOLS_NEXT_RULE)
				AND iTimeleft > 30000
					PRINTLN("[PROCESS_MUSIC][PCTS] LBOOL14_APT_STARTED_PRE_COUNTDOWN: > 30000 time left. (30S_KILL) ...")
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciUSE_FREEMODE_30S_COUNTDOWN)
						PRINTLN("[PROCESS_MUSIC][PCTS] Triggering FM_COUNTDOWN_30S_KILL")
						TRIGGER_MUSIC_EVENT("FM_COUNTDOWN_30S_KILL")
					ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciUSE_IE_30S_COUNTDOWN)
						PRINTLN("[PROCESS_MUSIC][PCTS] Triggering IE_COUNTDOWN_30S_KILL")
						TRIGGER_MUSIC_EVENT("IE_COUNTDOWN_30S_KILL")
					ELSE
						PRINTLN("[PROCESS_MUSIC][PCTS] Triggering APT_COUNTDOWN_30S_KILL")
						TRIGGER_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
					ENDIF
					
					CLEAR_BIT(iLocalBoolCheck,LBOOL_MUSIC_CHANGED)
					SET_MISSION_MUSIC(g_FMMC_STRUCT.iAudioScore)
					SET_BIT(iLocalBoolCheck3,LBOOL3_MUSIC_PLAYING)
					
					CLEAR_BIT(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC)
					CLEAR_BIT(iLocalBoolCheck15, LBOOL15_RESET_30S_CD_BOOLS_NEXT_RULE)
				ENDIF

				IF iTimeleft <= 35000
				AND iTimeleft > 10000 //Hacky fix for 2884001, seems like we were getting into here when iTimeleft = 788 :(
					IF NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_APT_STARTED_PRE_COUNTDOWN)
						SET_BIT(iLocalBoolCheck14, LBOOL14_APT_STARTED_PRE_COUNTDOWN)
						
						PRINTLN("[PROCESS_MUSIC][PCTS] LBOOL14_APT_STARTED_PRE_COUNTDOWN: playing countdown music for < 35000 time left.")
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciENABLE_VALENTINES_COUNTDOWN_MUSIC)
							PRINTLN("[PROCESS_MUSIC][PCTS] Starting 35 second pre-countdown for val")
							TRIGGER_MUSIC_EVENT("VAL2_PRE_COUNTDOWN_STOP")
						ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciUSE_FREEMODE_30S_COUNTDOWN)
							TRIGGER_MUSIC_EVENT("FM_PRE_COUNTDOWN_30S")
							PRINTLN("[PROCESS_MUSIC][PCTS] Starting 35 second pre-countdown for fm countdown")
						ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciUSE_IE_30S_COUNTDOWN)
							TRIGGER_MUSIC_EVENT("IE_PRE_COUNTDOWN_STOP")
							PRINTLN("[PROCESS_MUSIC][PCTS] Starting 35 second pre-countdown for IE_PRE_COUNTDOWN_STOP")
						ELIF IS_ARENA_WARS_JOB()
							PRINTLN("[PROCESS_MUSIC][PCTS] Starting 35 second pre-countdown for Arena War")
							TRIGGER_MUSIC_EVENT("AW_PRE_COUNTDOWN_STOP")
						ELSE
							IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet6, SBBOOL6_TIME_DEDUCTED_INTO_30S)
								PRINTLN("[PROCESS_MUSIC][PCTS] Starting 35 second pre-countdown for apt")
								TRIGGER_MUSIC_EVENT("APT_PRE_COUNTDOWN_STOP")
							ENDIF
						ENDIF
						
						PRINTLN("[PROCESS_MUSIC][PCTS] 1 iPriority = ", iPriority, ", iTimeleft = ", iTimeleft,", LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC = ", IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC),
								", LBOOL15_HAS_STARTED_VAL_COUNTDOWN_MUSIC = ", IS_BIT_SET(iLocalBoolCheck15, LBOOL15_HAS_STARTED_VAL_COUNTDOWN_MUSIC))
					ENDIF
				ENDIF
				
				IF iTimeleft <= 30000
				AND iTimeleft > 10000 //Hacky fix for 2884001, seems like we were getting into here when iTimeleft = 788 :(
				AND NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC)
				AND NOT IS_BIT_SET(iLocalBoolCheck15, LBOOL15_HAS_STARTED_VAL_COUNTDOWN_MUSIC)
				AND NOT IS_BIT_SET(iLocalBoolCheck20, LBOOL20_HAS_STARTED_IE_COUNTDOWN_MUSIC)
				AND NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_PLAYED_30s_COUNTDOWN_FOR_ARENA_WARS)
					#IF IS_DEBUG_BUILD
						PRINTLN("*-*-*-*-*-*-*-* [PROCESS_MUSIC][PCTS][JJT] PROCESS_COUNTDOWN_TIMER_SOUNDS - Starting 30 second countdown timer *-*-*-*-*-*-*-*")
						PRINTLN("*-*-* [PROCESS_MUSIC][PCTS] PROCESS_COUNTDOWN_TIMER_SOUNDS - iTimeleft = ", iTimeleft)
						PRINTLN("*-*-* [PROCESS_MUSIC][PCTS] LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC = ", IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC))
						PRINTLN("*-*-* [PROCESS_MUSIC][PCTS] LBOOL15_HAS_STARTED_VAL_COUNTDOWN_MUSIC = ", IS_BIT_SET(iLocalBoolCheck14, LBOOL15_HAS_STARTED_VAL_COUNTDOWN_MUSIC))
						PRINTLN("*-*-* [PROCESS_MUSIC][PCTS] LBOOL20_HAS_STARTED_IE_COUNTDOWN_MUSIC = ", IS_BIT_SET(iLocalBoolCheck20, LBOOL20_HAS_STARTED_IE_COUNTDOWN_MUSIC))
						PRINTLN("*-*-* [PROCESS_MUSIC][PCTS] iPriority = ", iPriority)
						PRINTLN("*-*-* [PROCESS_MUSIC][PCTS] iTeam = ", iTeam)
					#ENDIF
					
					IF CONTENT_IS_USING_ARENA()
					
						PRINTLN("[PROCESS_MUSIC][PCTS] Starting 30s countdown APT: AW_COUNTDOWN_30S")
						TRIGGER_MUSIC_EVENT("AW_COUNTDOWN_30S")
						PREPARE_MUSIC_EVENT("AW_COUNTDOWN_30S_KILL")
						CANCEL_ARENA_AUDIO_SCORE()
						SET_BIT(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC)
						
					ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciENABLE_VALENTINES_COUNTDOWN_MUSIC)
						PRINTLN("[PROCESS_MUSIC][PCTS] Starting 30 second countdown for Valentines: VAL2_COUNTDOWN_30S")
						SET_USER_RADIO_CONTROL_ENABLED(FALSE) 
						SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE)
						TRIGGER_MUSIC_EVENT("VAL2_COUNTDOWN_30S")
						PREPARE_MUSIC_EVENT("VAL2_COUNTDOWN_30S_KILL")
						SET_BIT(iLocalBoolCheck15, LBOOL15_HAS_STARTED_VAL_COUNTDOWN_MUSIC )
					ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciUSE_FREEMODE_30S_COUNTDOWN)
						PRINTLN("[PROCESS_MUSIC][PCTS] Starting 30s countdown Freemode: FM_COUNTDOWN_30S")
						TRIGGER_MUSIC_EVENT("FM_COUNTDOWN_30S")
						PREPARE_MUSIC_EVENT("FM_COUNTDOWN_30S_KILL")
						SET_BIT(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC)
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciALLOW_30S_CD_MUSIC_FOR_MULTIPLE_RULES)
							PRINTLN("[PROCESS_MUSIC][PCTS] Setting LBOOL15_RESET_30S_CD_BOOLS_NEXT_RULE so we can reset the CD next rule.")
							SET_BIT(iLocalBoolCheck15, LBOOL15_RESET_30S_CD_BOOLS_NEXT_RULE)
						ENDIF
					ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciUSE_IE_30S_COUNTDOWN)
						PRINTLN("[PROCESS_MUSIC][PCTS] Starting 30s countdown IE: IE_COUNTDOWN_30S")
						TRIGGER_MUSIC_EVENT("IE_COUNTDOWN_30S")
						PREPARE_MUSIC_EVENT("IE_COUNTDOWN_30S_KILL")
						SET_BIT(iLocalBoolCheck20, LBOOL20_HAS_STARTED_IE_COUNTDOWN_MUSIC)
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciALLOW_30S_CD_MUSIC_FOR_MULTIPLE_RULES)
							PRINTLN("[PROCESS_MUSIC][PCTS] Setting LBOOL15_RESET_30S_CD_BOOLS_NEXT_RULE so we can reset the CD next rule.")
							SET_BIT(iLocalBoolCheck15, LBOOL15_RESET_30S_CD_BOOLS_NEXT_RULE)
						ENDIF
					ELSE
						PRINTLN("[PROCESS_MUSIC][PCTS] Starting 30s countdown APT: APT_COUNTDOWN_30S")
						TRIGGER_MUSIC_EVENT("APT_COUNTDOWN_30S")
						PREPARE_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
						SET_BIT(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC)
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciALLOW_30S_CD_MUSIC_FOR_MULTIPLE_RULES)
							PRINTLN("[PROCESS_MUSIC][PCTS] Setting LBOOL15_RESET_30S_CD_BOOLS_NEXT_RULE so we can reset the CD next rule.")
							SET_BIT(iLocalBoolCheck15, LBOOL15_RESET_30S_CD_BOOLS_NEXT_RULE)
						ENDIF
					ENDIF
					
					SET_BIT(iLocalBoolCheck30, LBOOL30_PLAYED_30s_COUNTDOWN_FOR_ARENA_WARS)
				#IF IS_DEBUG_BUILD	
				ELSE
					IF GET_FRAME_COUNT() % 10 = 0
						PRINTLN("[PROCESS_MUSIC][PCTS] 2 iPriority = ", iPriority, ", iTimeleft = ", iTimeleft,", LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC = ", IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC),
								", LBOOL15_HAS_STARTED_VAL_COUNTDOWN_MUSIC = ", IS_BIT_SET(iLocalBoolCheck15, LBOOL15_HAS_STARTED_VAL_COUNTDOWN_MUSIC), "LBOOL20_HAS_STARTED_IE_COUNTDOWN_MUSIC = ",IS_BIT_SET(iLocalBoolCheck20, LBOOL20_HAS_STARTED_IE_COUNTDOWN_MUSIC))
					ENDIF
				#ENDIF
				ENDIF
				
				//Turn on Radio Control etc.
				IF iTimeleft <= 27000
				AND NOT IS_BIT_SET(iLocalBoolCheck15,LBOOL15_TURN_BACK_ON_RADIO_CONTROL)	
					SET_USER_RADIO_CONTROL_ENABLED(TRUE)
					SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
					SET_BIT(iLocalBoolCheck15,LBOOL15_TURN_BACK_ON_RADIO_CONTROL)
				ENDIF
				
				IF iTimeleft <= 5000
					IF NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_PLAYED_5S_COUNTDOWN_ON_30S_SETTING_CD)
						SET_BIT(iLocalBoolCheck14, LBOOL14_PLAYED_5S_COUNTDOWN_ON_30S_SETTING_CD)
						PRINTLN("[PROCESS_MUSIC][PCTS] Playing 5s countdown: 5S, MP_MISSION_COUNTDOWN_SOUNDSET")
						PLAY_SOUND_FRONTEND(iSoundIDCountdown, "5S","MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
					ENDIF
				ENDIF
				
				IF NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_COUNTDOWN_EARLY_MISSION_END) AND iTimeleft >= 1500
					PRINTLN("[PROCESS_MUSIC][PCTS] Setting LBOOL14_COUNTDOWN_EARLY_MISSION_END")
					SET_BIT(iLocalBoolCheck14, LBOOL14_COUNTDOWN_EARLY_MISSION_END)
				ELIF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_COUNTDOWN_EARLY_MISSION_END) AND iTimeleft < 1500
					PRINTLN("[PROCESS_MUSIC][PCTS] Clearing LBOOL14_COUNTDOWN_EARLY_MISSION_END")
					CLEAR_BIT(iLocalBoolCheck14, LBOOL14_COUNTDOWN_EARLY_MISSION_END)					
				ENDIF
			ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iPriority], ciBS_RULE2_COUNTDOWN_TIMER_SOUND_TENSECOND)
				IF iTimeleft <= 10 * 1000
					IF IS_SOUND_ID_VALID(iSoundIDCountdown)
					AND HAS_SOUND_FINISHED(iSoundIDCountdown)
					AND iTimeleft > 0 //Added to stop a timer playing on both sides of a rule if there are multiple 10 second countdowns occurring during the mission.
						PRINTLN("[PROCESS_MUSIC][PCTS] Playing 10 second countdown sound...")
						
						IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_Ebc( g_FMMC_STRUCT.iRootContentIDHash ) 
							//NOTE(Owain): This is a special countdown for Every Bullet Counts B* 2541172
							PRINTLN("[PROCESS_MUSIC][PCTS] Timer_10s, DLC_TG_Dinner_Sounds")
							PLAY_SOUND_FRONTEND(iSoundIDCountdown,"Timer_10s","DLC_TG_Dinner_Sounds", FALSE)
						ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_LOST_DAMNED(g_FMMC_STRUCT.iAdversaryModeType)
							IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSpecificTODHour[iPriority] = 12 //Going into night
								PRINTLN("[PROCESS_MUSIC][PCTS] Timer_To_Night, DLC_Biker_LostAndDamned_Sounds")
								PLAY_SOUND_FRONTEND(iSoundIDCountDown,"Timer_To_Night","DLC_Biker_LostAndDamned_Sounds", FALSE)
							ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSpecificTODHour[iPriority] = 0 //Going into Day
								PRINTLN("[PROCESS_MUSIC][PCTS] Timer_To_Day, DLC_Biker_LostAndDamned_Sounds")
								PLAY_SOUND_FRONTEND(iSoundIDCountDown,"Timer_To_Day", "DLC_Biker_LostAndDamned_Sounds", FALSE)
							ENDIF
						ELSE
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_VEHICULAR_WARFARE_SOUNDS)
								IF NOT Is_Player_Currently_On_MP_Heist(LocalPlayer)
									PRINTLN("[PROCESS_MUSIC][PCTS] 10S, MP_MISSION_COUNTDOWN_SOUNDSET")
									PLAY_SOUND_FRONTEND(iSoundIDCountdown,"10S","MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
								ELSE
									PRINTLN("[PROCESS_MUSIC][PCTS] Not playing 10s countdown as we're on a heist!")
								ENDIF
							ENDIF
						ENDIF
						
						SET_BIT(iLocalBoolCheck5, LBOOL5_STARTED_COUNTDOWN_TIMER_SOUNDS)
					ELSE
						PRINTLN("[PROCESS_MUSIC][PCTS] 10s Sound stopped due to it being finished, no time left, or the ID is now invalid.")
						STOP_SOUND(iSoundIDCountdown)
					ENDIF
				ELSE
					IF IS_SOUND_ID_VALID(iSoundIDCountdown)
					AND NOT HAS_SOUND_FINISHED(iSoundIDCountdown)
						PRINTLN("[PROCESS_MUSIC][PCTS] 10s Sound stopped, Valid ID and it has not finished. Because Time ran out. Time left: ", iTimeleft)
						STOP_SOUND(iSoundIDCountdown)
					ENDIF
				ENDIF
			ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iPriority], ciBS_RULE2_COUNTDOWN_TIMER_SOUND_FIVESECOND)
				IF iTimeleft <= 5 * 1000
					IF IS_SOUND_ID_VALID(iSoundIDCountdown)
					AND HAS_SOUND_FINISHED(iSoundIDCountdown)
						PRINTLN("[PROCESS_MUSIC][PCTS] Playing 5 second countdown sound...")						
						
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_ENABLE_TRAP_DOOR_SOUNDS) AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_SUMO_RUN_SOUNDS)
							IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_Ebc( g_FMMC_STRUCT.iRootContentIDHash )
								//NOTE(Owain): This is a special countdown for Every Bullet Counts B* 2541172
								PRINTLN("[PROCESS_MUSIC][PCTS] Timer_5s, DLC_TG_Dinner_Sounds")
								PLAY_SOUND_FRONTEND(iSoundIDCountdown,"Timer_5s","DLC_TG_Dinner_Sounds", FALSE)
							ELSE
								PRINTLN("[PROCESS_MUSIC][PCTS] 5S, MP_MISSION_COUNTDOWN_SOUNDSET")
								PLAY_SOUND_FRONTEND(iSoundIDCountdown,"5S","MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
							ENDIF
						ENDIF							
							
						SET_BIT(iLocalBoolCheck5, LBOOL5_STARTED_COUNTDOWN_TIMER_SOUNDS)
					ELSE
						PRINTLN("[PROCESS_MUSIC][PCTS] 5s Sound stopped due to it being finished, no time left, or the ID is now invalid.")
						STOP_SOUND(iSoundIDCountdown)
					ENDIF 
				ELSE
					IF IS_SOUND_ID_VALID(iSoundIDCountdown)
					AND NOT HAS_SOUND_FINISHED(iSoundIDCountdown)
						PRINTLN("[PROCESS_MUSIC][PCTS] 5s Sound stopped, Valid ID and it has not finished. Because Time ran out. Time left: ", iTimeleft)
						STOP_SOUND(iSoundIDCountdown)
					ENDIF
				ENDIF
			ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iPriority], ciBS_RULE2_COUNTDOWN_TIMER_SOUND_ONESHOT)
				IF iTimeleft <= 1000
					IF IS_SOUND_ID_VALID(iSoundIDCountdown)
					AND HAS_SOUND_FINISHED(iSoundIDCountdown)
						PRINTLN("[PROCESS_MUSIC][PCTS] Playing one shot countdown sound")
						PLAY_SOUND_FRONTEND(-1,"Oneshot_Final","MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
						SET_BIT(iLocalBoolCheck5, LBOOL5_STARTED_COUNTDOWN_TIMER_SOUNDS)
					ELSE
						PRINTLN("[PROCESS_MUSIC][PCTS] Stopping sound Oneshot_Final as the ID is invalid or it's finished.")
						STOP_SOUND(iSoundIDCountdown)
					ENDIF
				ELSE
					IF IS_SOUND_ID_VALID(iSoundIDCountdown)
					AND NOT HAS_SOUND_FINISHED(iSoundIDCountdown)
						PRINTLN("[PROCESS_MUSIC][PCTS] Oneshot_Final Sound stopped, Valid ID and it has not finished. Because Time ran out. Time left: ", iTimeleft)
						STOP_SOUND(iSoundIDCountdown)
					ENDIF
				ENDIF
			ELSE
				IF IS_SOUND_ID_VALID(iSoundIDCountdown)
				AND NOT HAS_SOUND_FINISHED(iSoundIDCountdown)
					PRINTLN("[PROCESS_MUSIC][PCTS] calling stop_sound as no bitset was set to decide a countdown timer sound.")
					STOP_SOUND(iSoundIDCountdown)
				ENDIF
			ENDIF
			
		ENDIF
		
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		IF IS_SOUND_ID_VALID(iSoundIDCountdown)
		AND NOT HAS_SOUND_FINISHED(iSoundIDCountdown)
			PRINTLN("[PROCESS_MUSIC][PCTS] Mission is over. Sound is still playing, stop the sound: ", iSoundIDCountdown)
			STOP_SOUND(iSoundIDCountdown)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_MISSION_GOT_SUDDEN_DEATH()
	IF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_SUDDEN_DEATH_MOREINLOC)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_SUDDEN_DEATH_KILLALLENEMIES)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_BE_MY_VALENTINES)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_IN_AND_OUT)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_POWER_PLAY)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_TARGET)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_PACKAGE_HELD)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_LOST_AND_DAMNED)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_GUNSMITH)
	OR IS_BIT_SET(MC_ServerBD.iServerBitSet4, SBBOOL4_SUDDEN_DEATH_TURFWAR)
	OR IS_BIT_SET(MC_ServerBD.iServerBitSet4, SBBOOL4_SUDDEN_DEATH_JUGGERNAUT)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet6, SBBOOL6_SUDDEN_DEATH_RUNNING_BACK_REMIX)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet6, SBBOOL6_SUDDEN_DEATH_STOCKPILE)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet7, SBBOOL7_SUDDEN_DEATH_TAG_TEAM)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet7, SBBOOL7_SUDDEN_DEATH_BOMB_FOOTBALL)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
	IF IS_BIT_SET( MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH )	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

ENUM eSuddenDeathState
	eSuddenDeathState_WAIT_TO_START,
	eSuddenDeathState_START,
	eSuddenDeathState_WAIT_FOR_END,
	eSuddenDeathState_END
ENDENUM

SCRIPT_TIMER stSuddenDeathMusic

eSuddenDeathState sudden_death_music_state = eSuddenDeathState_WAIT_TO_START

PROC SET_SUDDEN_DEATH_MUSIC_STATE(eSuddenDeathState eNewState)
	PRINTLN("SET_SUDDEN_DEATH_MUSIC_STATE - Setting to state ", ENUM_TO_INT(eNewState), " Previous state was: ", ENUM_TO_INT(sudden_death_music_state))
	sudden_death_music_state = eNewState
ENDPROC

PROC PROCESS_SUDDEN_DEATH_MUSIC()
	BOOL bFakeSuddenDeath
	IF IS_BIT_SET(MC_serverBD.iServerBitSet7, SBBOOL7_SUDDEN_DEATH_BOMB_FOOTBALL)
	AND IS_BIT_SET(MC_serverBD.iServerBitSet7, SBBOOL7_IN_BMBFB_SUDDEN_DEATH)
		bFakeSuddenDeath = TRUE
	ENDIF
	
	SWITCH( sudden_death_music_state )
	
		CASE eSuddenDeathState_WAIT_TO_START
			IF ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
			OR bFakeSuddenDeath
				START_NET_TIMER( stSuddenDeathMusic )
				SET_SUDDEN_DEATH_MUSIC_STATE(eSuddenDeathState_START)
			ENDIF
		BREAK
		
		CASE eSuddenDeathState_START
			IF NOT IS_SOUND_ID_VALID(iSoundIDCountdown)
				iSoundIDCountdown = GET_SOUND_ID()
				PRINTLN("PROCESS_SUDDEN_DEATH_MUSIC - GET_SOUND_ID iSoundIDCountdown ",iSoundIDCountdown)
			ENDIF
			IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( stSuddenDeathMusic ) > 3000
				IF NOT IS_BIT_SET(iLocalBoolCheck, ciPOPULATE_RESULTS) // Once populate results is called the music should stop
					//HACK - remove if this will be used for other modes
					IF CONTENT_IS_USING_ARENA()
						TRIGGER_MUSIC_EVENT( "DLC_AWXM2018_SUDDEN_DEATH_START_MUSIC" )
						SET_BIT(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_SUDDEN_DEATH_MUSIC)
						CPRINTLN(DEBUG_CONTROLLER, "[RCC MISSION] Starting sudden death music - DLC_AWXM2018_SUDDEN_DEATH_START_MUSIC")
					ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_ZONE(g_FMMC_STRUCT.iRootContentIdHash)
					OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_BVS1(g_FMMC_STRUCT.iRootContentIdHash)
					OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_EXTRACTION(g_FMMC_STRUCT.iRootContentIdHash)
					OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_IN_AND_OUT_SOUNDS)
					OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_PACKAGE_HELD)
					OR (IS_THIS_ROCKSTAR_MISSION_NEW_VS_DCTL(g_FMMC_STRUCT.iAdversaryModeType) AND IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN))
					OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(g_FMMC_STRUCT.iAdversaryModeType)
					OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
						PRINTLN("[JJT] PROCESS_SUDDEN_DEATH_MUSIC - Started apartment mode APT_SUDDEN_DEATH_START_MUSIC")
						SET_BIT(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_SUDDEN_DEATH_MUSIC)
						TRIGGER_MUSIC_EVENT( "APT_SUDDEN_DEATH_START_MUSIC" )
						
						#IF IS_DEBUG_BUILD
							IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_ZONE(g_FMMC_STRUCT.iRootContentIdHash)
								PRINTLN("[JJT] PROCESS_SUDDEN_DEATH_MUSIC - IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_ZONE")
							ENDIF
							IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_BVS1(g_FMMC_STRUCT.iRootContentIdHash)
								PRINTLN("[JJT] PROCESS_SUDDEN_DEATH_MUSIC - IS_THIS_ROCKSTAR_MISSION_NEW_VS_BVS1")
							ENDIF
							IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_EXTRACTION(g_FMMC_STRUCT.iRootContentIdHash)
								PRINTLN("[JJT] PROCESS_SUDDEN_DEATH_MUSIC - IS_THIS_ROCKSTAR_MISSION_NEW_VS_EXTRACTION")
							ENDIF
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_IN_AND_OUT_SOUNDS)
								PRINTLN("[JJT] PROCESS_SUDDEN_DEATH_MUSIC - ciENABLE_IN_AND_OUT_SOUNDS")
							ENDIF
						#ENDIF
					ELSE
						TRIGGER_MUSIC_EVENT( "FM_SUDDEN_DEATH_START_MUSIC" )
						CPRINTLN(DEBUG_CONTROLLER, "[RCC MISSION] Starting sudden death music - FM_SUDDEN_DEATH_START_MUSIC")
					ENDIF

				#IF IS_DEBUG_BUILD
				ELSE
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] We would start the sudden death music, but ciPOPULATE_RESULTS is set already!" )
				#ENDIF
				ENDIF

				RESET_NET_TIMER( stSuddenDeathMusic )
				SET_SUDDEN_DEATH_MUSIC_STATE(eSuddenDeathState_WAIT_FOR_END)
			ELSE
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARS(g_FMMC_STRUCT.iAdversaryModeType)
					TRIGGER_MUSIC_EVENT("GTA_ONLINE_STOP_SCORE")
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH	

	IF sudden_death_music_state = eSuddenDeathState_WAIT_FOR_END
		INT iTeam = MC_playerBD[iPartToUse].iTeam
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		//More easily to read bMultiRuleTimer this way...
		BOOL bMultiRuleTimer = FALSE
		
		IF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_PACKAGE_HELD)
			bMultiRuleTimer = TRUE
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciMULTIRULE_TIMER_SUDDEN_DEATH)
		AND IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_SUDDEN_DEATH_POWER_PLAY)
			bMultiRuleTimer = TRUE
		ENDIF
		
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(g_FMMC_STRUCT.iAdversaryModeType)
		OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
		OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_SUMO_RUN(g_FMMC_STRUCT.iAdversaryModeType)
		OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_ARENA_CTF(g_FMMC_STRUCT.iAdversaryModeType)
		OR IS_BIT_SET(MC_serverBD.iServerBitSet7, SBBOOL7_SUDDEN_DEATH_TAG_TEAM)
		OR IS_BIT_SET(MC_serverBD.iServerBitSet7, SBBOOL7_SUDDEN_DEATH_BOMB_FOOTBALL)
			bMultiRuleTimer = TRUE
		ENDIF
		
		INT iTimeRemaining = GET_REMAINING_TIME_ON_RULE_SUDDENDEATH(iTeam, iRule, bMultiRuleTimer)
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciMULTIRULE_TIMER_SUDDEN_DEATH)
		AND IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_SUDDEN_DEATH_GUNSMITH)
		OR (IS_THIS_ROCKSTAR_MISSION_NEW_VS_DCTL(g_FMMC_STRUCT.iAdversaryModeType) AND IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN))
		OR IS_BIT_SET(MC_serverBD.iServerBitSet4, SBBOOL4_SUDDEN_DEATH_TURFWAR)
		OR IS_BIT_SET(MC_serverBD.iServerBitSet4, SBBOOL4_SUDDEN_DEATH_JUGGERNAUT)
			PRINTLN("[PROCESS_SUDDEN_DEATH_MUSIC][RH] doing it again")
			iTimeRemaining = GET_REMAINING_TIME_ON_RULE_SUDDENDEATH(iTeam, iRule, TRUE)
		ENDIF	
		
		PRINTLN("[PROCESS_SUDDEN_DEATH_MUSIC][RH] setting time remaining to: ", iTimeRemaining)
	
		IF iRule < FMMC_MAX_RULES 
		AND iTeam < FMMC_MAX_TEAMS
			PRINTLN("[RCC MISSION][JJT] PROCESS_SUDDEN_DEATH_MUSIC: iTimeRemaining = ", iTimeRemaining)
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule], ciBS_RULE6_SD_CD_10_AND_5_SECONDS)
			OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_VEHICULAR_WARFARE_SOUNDS)
				IF iTimeRemaining <= 10000 
					IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_PLAYED_10S_SD_SFX)
						SET_BIT(iLocalBoolCheck16, LBOOL16_PLAYED_10S_SD_SFX)
						PLAY_SOUND_FRONTEND(iSoundIDCountdown,"10S","MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
						PRINTLN("[RCC MISSION][JJT] PROCESS_SUDDEN_DEATH_MUSIC - Playing 10s Sudden Death CD SFX (1) - iTimeRemaining = ", iTimeRemaining)
					ENDIF
				ENDIF
				IF iTimeRemaining <= 5000 
					IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_PLAYED_5S_SD_SFX)
						SET_BIT(iLocalBoolCheck16, LBOOL16_PLAYED_5S_SD_SFX)
						PLAY_SOUND_FRONTEND(iSoundIDCountdown,"5S","MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
						PRINTLN("[RCC MISSION][JJT] PROCESS_SUDDEN_DEATH_MUSIC - Playing 5s Sudden Death CD SFX (1) - iTimeRemaining = ", iTimeRemaining)
					ENDIF
				ENDIF
			ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule], ciBS_RULE6_SD_CD_10_SECONDS)
				IF iTimeRemaining <= 10000 + (GET_FRAME_TIME() * 1000)
					IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_PLAYED_10S_SD_SFX)					
						SET_BIT(iLocalBoolCheck16, LBOOL16_PLAYED_10S_SD_SFX)
						PLAY_SOUND_FRONTEND(iSoundIDCountdown,"10S","MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
						PRINTLN("[RCC MISSION][JJT] PROCESS_SUDDEN_DEATH_MUSIC - Playing 10s Sudden Death CD SFX (2) - iTimeRemaining = ", iTimeRemaining,
								", SBBOOL3_SUDDEN_DEATH_PACKAGE_HELD = ", IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_PACKAGE_HELD))
					ENDIF
				ENDIF
			ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule], ciBS_RULE6_SD_CD_5_SECONDS)
				IF iTimeRemaining <= 5000 
					IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_PLAYED_5S_SD_SFX)
						SET_BIT(iLocalBoolCheck16, LBOOL16_PLAYED_5S_SD_SFX)
						PLAY_SOUND_FRONTEND(iSoundIDCountdown,"5S","MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
					ENDIF
				ENDIF
			ENDIF

			//HACK - remove if this will be used for other modes
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_ZONE(g_FMMC_STRUCT.iRootContentIdHash)
			OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_BVS1(g_FMMC_STRUCT.iRootContentIdHash)
			OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_EXTRACTION(g_FMMC_STRUCT.iRootContentIdHash)
			OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(g_FMMC_STRUCT.iAdversaryModeType)
				IF iTimeRemaining <= 10000
					IF NOT IS_BIT_SET(iLocalBoolCheck15, LBOOL15_HAS_STARTED_APT_SUDDEN_DEATH_END)
						SET_BIT(iLocalBoolCheck15, LBOOL15_HAS_STARTED_APT_SUDDEN_DEATH_END)
						PRINTLN("[JJT] PROCESS_SUDDEN_DEATH_MUSIC - TRIGGER_MUSIC_EVENT('APT_SUDDEN_DEATH_MUSIC_END')")
						TRIGGER_MUSIC_EVENT("APT_SUDDEN_DEATH_MUSIC_END")
					ENDIF
				ENDIF
				
				IF iTimeRemaining <= 5000 
					IF NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_PLAYED_5S_COUNTDOWN_ON_30S_SETTING_SD)
						SET_BIT(iLocalBoolCheck14, LBOOL14_PLAYED_5S_COUNTDOWN_ON_30S_SETTING_SD)
						PLAY_SOUND_FRONTEND(iSoundIDCountdown,"5S","MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
					ENDIF
				ENDIF	
			ELIF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_IN_AND_OUT_SOUNDS)
				  OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_PACKAGE_HELD))
				IF iTimeRemaining <= 10000
					IF NOT IS_BIT_SET(iLocalBoolCheck15, LBOOL15_HAS_STARTED_APT_SUDDEN_DEATH_END)
						SET_BIT(iLocalBoolCheck15, LBOOL15_HAS_STARTED_APT_SUDDEN_DEATH_END)
						PRINTLN("[JJT] PROCESS_SUDDEN_DEATH_MUSIC - TRIGGER_MUSIC_EVENT('APT_SUDDEN_DEATH_MUSIC_END')")
						TRIGGER_MUSIC_EVENT("APT_SUDDEN_DEATH_MUSIC_END")
					ENDIF
				ENDIF
			ELIF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_BE_MY_VALENTINES)	
			OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_POWER_PLAY)
			OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_GUNSMITH)
			OR IS_BIT_SET(MC_serverBD.iServerBitSet4, SBBOOL4_SUDDEN_DEATH_TURFWAR)
			OR IS_BIT_SET(MC_serverBD.iServerBitSet4, SBBOOL4_SUDDEN_DEATH_JUGGERNAUT)
				IF iTimeRemaining <= 10000
				AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_POWER_PLAY)
				AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_GUNSMITH)
				AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet4, SBBOOL4_SUDDEN_DEATH_TURFWAR)
				AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet4, SBBOOL4_SUDDEN_DEATH_JUGGERNAUT)
					IF NOT IS_BIT_SET(iLocalBoolCheck15, LBOOL15_HAS_STARTED_VAL_SUDDEN_DEATH_END)
					AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_VEHICULAR_WARFARE_SOUNDS)
						SET_BIT(iLocalBoolCheck15, LBOOL15_HAS_STARTED_VAL_SUDDEN_DEATH_END)
						PRINTLN("[JJT] PROCESS_SUDDEN_DEATH_MUSIC - VAL - PLAY_SOUND_FRONTEND(10s,MP_MISSION_COUNTDOWN_SOUNDSET)")
						PLAY_SOUND_FRONTEND(-1, "10s",  "MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
					ENDIF
				ENDIF

				IF iTimeRemaining <= 5000
					IF NOT IS_BIT_SET(iLocalBoolCheck15, LBOOL15_HAS_STARTED_VAL_SUDDEN_DEATH_FADE)
						SET_BIT(iLocalBoolCheck15, LBOOL15_HAS_STARTED_VAL_SUDDEN_DEATH_FADE)
						PRINTLN("[JJT] PROCESS_SUDDEN_DEATH_MUSIC - VAL/PP - TRIGGER_MUSIC_EVENT(FM_SUDDEN_DEATH_STOP_MUSIC)")
						TRIGGER_MUSIC_EVENT("FM_SUDDEN_DEATH_STOP_MUSIC")
					ENDIF
				ENDIF
			ELSE
				IF iTimeRemaining <= 10000
					IF NOT IS_BIT_SET(iLocalBoolCheck15, LBOOL15_HAS_STARTED_APT_SUDDEN_DEATH_END)
						SET_BIT(iLocalBoolCheck15, LBOOL15_HAS_STARTED_APT_SUDDEN_DEATH_END)
						IF CONTENT_IS_USING_ARENA()
							TRIGGER_MUSIC_EVENT( "DLC_AWXM2018_SUDDEN_DEATH_MUSIC_END" )
							CPRINTLN(DEBUG_CONTROLLER, "[JJT] PROCESS_SUDDEN_DEATH_MUSIC - TRIGGER_MUSIC_EVENT('DLC_AWXM2018_SUDDEN_DEATH_MUSIC_END')")
						ELSE
							PRINTLN("[JJT] PROCESS_SUDDEN_DEATH_MUSIC - TRIGGER_MUSIC_EVENT('APT_SUDDEN_DEATH_MUSIC_END') for all modes.")
							TRIGGER_MUSIC_EVENT("APT_SUDDEN_DEATH_MUSIC_END")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_USING_SECONDARY_ROLE_MOOD()
	
	INT iTeam = MC_PlayerBD[iPartToUse].iteam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]

	IF MC_playerBD[iPartToUse].iCoronaRole = 1
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicSecondaryRoleMood[iRule] != -1
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL POINTLESS_MUSIC_STATE_CHANGE(INT iMusicType)
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_POINTLESS(g_FMMC_STRUCT.iAdversaryModeType)
	AND g_FMMC_STRUCT.iPointlessEndingMusic = iMusicType 
	AND DOES_ANY_TEAM_HAVE_MAX_POINTS()
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PROCESS_MUSIC()

PED_INDEX tempped
BOOL bUseDefault,bUseDefaultHigh
BOOL bStart
BOOL bMid
INT ms
	IF (IS_BIT_SET(MC_Playerbd[iPartToUse].iClientBitSet,PBBOOL_TERMINATED_SCRIPT)
	OR IS_BIT_SET(MC_PlayerBD[iPartToUse].iClientBitSet,PBBOOL_ON_LEADERBOARD)
	OR (IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL() AND NOT IS_BIT_SET(MC_playerBD[iPartToUse].iTutorialBitset, TUTORIAL_PLAYER_FINISHED_TUTORIAL_CUT))
	OR GET_MC_SERVER_GAME_STATE() = GAME_STATE_END)		
		IF NOT HAS_NET_TIMER_STARTED(tdEndMissionStopMusicDelay)
			START_NET_TIMER(tdEndMissionStopMusicDelay)
		ENDIF		
		IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdEndMissionStopMusicDelay, 2000) // Sometimes we get in here before the next strand was being initialized, meaning music cut out during cutscenes.
			IF MC_PlayerBD[iPartToUse].iMusicState != MUSIC_STOP
			OR NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_HAS_STOPPED_MUSIC_ON_LEADERBOARD)
			
				#IF IS_DEBUG_BUILD
					IF MC_PlayerBD[iPartToUse].iMusicState != MUSIC_STOP
						PRINTLN("[PROCESS_MUSIC] Got in bc MC_PlayerBD[iPartToUse].iMusicState != MUSIC_STOP")
					ELIF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_HAS_STOPPED_MUSIC_ON_LEADERBOARD)
						PRINTLN("[PROCESS_MUSIC] Got in bc NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_HAS_STOPPED_MUSIC_ON_LEADERBOARD)")
					ENDIF
				#ENDIF
			
				SET_BIT(iLocalBoolCheck16, LBOOL16_HAS_STOPPED_MUSIC_ON_LEADERBOARD)
				MC_PlayerBD[iPartToUse].iMusicState = MUSIC_STOP
				
				STOP_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
				//url:bugstar:3027594			
				IF NOT IS_THIS_AN_ADVERSARY_MODE_MISSION(g_FMMC_STRUCT.iRootContentIDHash, g_FMMC_STRUCT.iAdversaryModeType)
				OR IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH)
					SCRIPT_TRIGGER_MUSIC_EVENT_STOP()
					PRINTLN("TRIGGER_MUSIC_EVENT(\"MP_MC_STOP\") - PROCESS_MUSIC")
				ELSE
					PRINTLN("[PROCESS_MUSIC] [RCC MISSION][JT][MUSIC] PROCESS_MUSIC - Skipping TRIGGER_MUSIC_EVENT(\"MP_MC_STOP\") if in an Adversary mode")
				ENDIF
				SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE)
				PRINTLN("[PROCESS_MUSIC] [RCC MISSION][JJT][MUSIC] PROCESS_MUSIC - Setting music state to stop bc either PBBOOL_TERMINATED_SCRIPT or PBBOOL_ON_LEADERBOARD")
				PRINTLN("[PROCESS_MUSIC] [RCC MISSION][JJT][MUSIC] PROCESS_MUSIC - IS_A_STRAND_MISSION_BEING_INITIALISED(): ", IS_A_STRAND_MISSION_BEING_INITIALISED() )
			ENDIF
		ENDIF	
		EXIT
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DCTL(g_FMMC_STRUCT.iAdversaryModeType)
		IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
			IF NOT AUDIO_IS_SCRIPTED_MUSIC_PLAYING()
				IF NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC)
					PRINTLN("[PROCESS_MUSIC][KH] Starting audio score for local spectator")
					SET_MISSION_MUSIC(g_FMMC_STRUCT.iAudioScore, FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] = g_FMMC_STRUCT.iDroneSoundsRule
		IF iDroneCutsceneSound = -1
			PRINTLN("[PROCESS_MUSIC] Starting audio scene for camera drone cutscene")
			START_AUDIO_SCENE("DLC_GR_APC_Drone_View_Scene")
			iDroneCutsceneSound = GET_SOUND_ID()
			PLAY_SOUND_FRONTEND(iDroneCutsceneSound, "Drone_View", "DLC_GR_WVM_APC_Sounds")
		ENDIF
	ELSE
		IF iDroneCutsceneSound != -1
			PRINTLN("[PROCESS_MUSIC] Ending audio scene for camera drone cutscene")
			STOP_SOUND(iDroneCutsceneSound)
			STOP_AUDIO_SCENE("DLC_GR_APC_Drone_View_Scene")
			RELEASE_SOUND_ID(iDroneCutsceneSound)
			iDroneCutsceneSound = -1
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciENABLE_SUMO_SOUNDS)
	AND IS_THIS_ROCKSTAR_MISSION_NEW_VS_SUMO(g_FMMC_STRUCT.iRootContentIdHash)
	AND GET_MC_SERVER_GAME_STATE() != GAME_STATE_END // Once the server is in game_state_end, all the teams empty of players anyway
		INT i, iNumAlivePlayers[FMMC_MAX_TEAMS]
		FOR i = 0 TO (FMMC_MAX_TEAMS - 1)
			iNumAlivePlayers[i] = MC_serverBD.iNumberOfPlayingPlayers[i]
			IF iLastHUDPlayerLivesCount[i] > 0 // Initialised as -1 in PRE_GAME
				//someone has died since last frame
				IF iLastHUDPlayerLivesCount[i] > iNumAlivePlayers[i]
					PRINTLN("[PROCESS_MUSIC] [RCC MISSION] PROCESS_MUSIC - (Team_?) Vehicle_Destroyed sound is firing! For team = ",i,"; iNumAlivePlayers = ",iNumAlivePlayers[i],", iLastHUDPlayerLivesCount = ",iLastHUDPlayerLivesCount[i])
					
					IF i = MC_playerBD[iPartToUse].iteam
						PLAY_SOUND(-1, "Team_Vehicle_Destroyed", "DLC_LOW2_Sumo_Soundset",FALSE)
					ELSE
						PLAY_SOUND(-1, "Vehicle_Destroyed", "DLC_LOW2_Sumo_Soundset",FALSE)
					ENDIF
				ENDIF
			ENDIF
			
			iLastHUDPlayerLivesCount[i] = iNumAlivePlayers[i]
		ENDFOR
	ENDIF
	
	IF g_bMissionOver	
		IF iHalloweenSoundID > -1
		AND NOT HAS_SOUND_FINISHED(iHalloweenSoundID)
			STOP_SOUND(iHalloweenSoundID)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iTeamBitset2, ciBS2_EnableRadioOnly)
	OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciSTOP_SCORE_OVERRIDING_RADIO) AND GET_PLAYER_RADIO_STATION_INDEX() != 255)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet6,SBBOOL6_SUDDEN_DEATH_TRIGGERED_EARLY)
	AND NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_HOSTILE_TAKEOVER_EARLY_SD_COUNTDOWN_STOP)
		IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC)
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciUSE_FREEMODE_30S_COUNTDOWN)
				TRIGGER_MUSIC_EVENT("FM_COUNTDOWN_30S_KILL")
			ELSE
				TRIGGER_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
			ENDIF
			PRINTLN("[PROCESS_MUSIC][PCTS] - Stopping countdown early due to SBBOOL6_SUDDEN_DEATH_TRIGGERED_EARLY")
			SET_BIT(iLocalBoolCheck27, LBOOL27_HOSTILE_TAKEOVER_EARLY_SD_COUNTDOWN_STOP)
		ENDIF
	ENDIF
	IF HAS_MISSION_GOT_SUDDEN_DEATH()
	AND NOT g_bMissionOver
		PROCESS_SUDDEN_DEATH_MUSIC()
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
	OR g_bMissionOver
		IF IS_SOUND_ID_VALID(iSoundIDCountdown)
		AND NOT HAS_SOUND_FINISHED(iSoundIDCountdown)
			PRINTLN("PROCESS_SUDDEN_DEATH_MUSIC Mission is over. Sound is still playing, stop the sound: ", iSoundIDCountdown)
			STOP_SOUND(iSoundIDCountdown)
		ENDIF
	ENDIF
	
	IF ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
		EXIT
	ENDIF
	
	IF iSpectatorTarget = -1
	AND bLocalPlayerOK
		IF MC_playerBD[iPartToUse].bHasPlayerReceivedBattleEvent != GET_PLAYER_RECEIVED_BATTLE_EVENT_RECENTLY(LocalPlayer,100,FALSE)
			MC_playerBD[iPartToUse].bHasPlayerReceivedBattleEvent = GET_PLAYER_RECEIVED_BATTLE_EVENT_RECENTLY(LocalPlayer,100,FALSE)
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bEnableScriptAudioSpammyPrints
		PRINTLN("[PROCESS_MUSIC][DEBUG] MC_PlayerBD[iPartToUse].iMusicState: ", MC_PlayerBD[iPartToUse].iMusicState)
		PRINTLN("[PROCESS_MUSIC][DEBUG] g_FMMC_STRUCT.iAudioScore: 			 ", g_FMMC_STRUCT.iAudioScore)
		
		IF mc_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
			PRINTLN("[PROCESS_MUSIC][DEBUG] Rule Setting: 						 ", g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMusicStart[mc_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]])
		ENDIF
		PRINTLN("[PROCESS_MUSIC][DEBUG] Rule: 								 ", mc_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
	ENDIF
	#ENDIF
	
	INT iTeam = MC_PlayerBD[iPartToUse].iteam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
		
	BOOL bOverride, bOverrideWithAggro

	IF iRule < FMMC_MAX_RULES
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScoreAggroMood[iRule] > 0
			IF HAS_ANY_TEAM_TRIGGERED_AGGRO()
				bOverride = TRUE
				bOverrideWithAggro = TRUE
			ELSE
				PRINTLN("[PROCESS_MUSIC][RCC MISSION] iMusicScoreAggroMood Set to: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScoreAggroMood[iRule], " But we have not yet aggroed.")
			ENDIF
		ENDIF
	ENDIF
	
	IF MC_PlayerBD[iPartToUse].iMusicState != MUSIC_LTS_ENDING
	AND MC_PlayerBD[iPartToUse].iMusicState != MUSIC_CTF_ENDING
	AND MC_PlayerBD[iPartToUse].iMusicState != MUSIC_MID_COUNTDOWN
		
		IF g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_DEFAULT
			#IF IS_DEBUG_BUILD
			IF bEnableScriptAudioSpammyPrints
				PRINTLN("[PROCESS_MUSIC][DEBUG] 0b - bUseDefault = TRUE        bUSeDefaultHigh = FALSE")
			ENDIF
			#ENDIF
			
			bUseDefault = TRUE
			bUSeDefaultHigh = FALSE
		ELIF g_FMMC_STRUCT.iAudioScore = MUSIC_TYPE_DEFAULT_HIGH
			#IF IS_DEBUG_BUILD
			IF bEnableScriptAudioSpammyPrints
				PRINTLN("[PROCESS_MUSIC][DEBUG] 0b - bUseDefaultHigh = TRUE    bUseDefault = FALSE")
			ENDIF
			#ENDIF
			
			bUseDefaultHigh = TRUE
			bUseDefault = FALSE
		ELSE
			IF mc_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
				IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[iPartToUse].iteam],mc_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
					IF IS_USING_SECONDARY_ROLE_MOOD()
						#IF IS_DEBUG_BUILD
						IF bEnableScriptAudioSpammyPrints
							PRINTLN("[PROCESS_MUSIC][DEBUG] 1a - bStart = TRUE")
						ENDIF
						#ENDIF
						
						bStart = TRUE												
					ELIF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMusicStart[mc_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] = MUSIC_DEFAULT
						#IF IS_DEBUG_BUILD
						IF bEnableScriptAudioSpammyPrints
							PRINTLN("[PROCESS_MUSIC][DEBUG] 1b - bUseDefault = TRUE")
						ENDIF
						#ENDIF
							
						bUseDefault = TRUE
					ELIF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMusicStart[mc_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] = MUSIC_DEFAULT_HIGH
						#IF IS_DEBUG_BUILD
						IF bEnableScriptAudioSpammyPrints
							PRINTLN("[PROCESS_MUSIC][DEBUG] 1c - bUseDefaultHigh = TRUE")
						ENDIF
						#ENDIF
						
						bUseDefaultHigh = TRUE
					ELSE
						#IF IS_DEBUG_BUILD
						IF bEnableScriptAudioSpammyPrints
							PRINTLN("[PROCESS_MUSIC][DEBUG] 1d - bStart = TRUE")
						ENDIF
						#ENDIF
						
						bStart = TRUE
					ENDIF
				ELSE
					IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMusicMid[mc_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] = MUSIC_DEFAULT
						#IF IS_DEBUG_BUILD
						IF bEnableScriptAudioSpammyPrints
							PRINTLN("[PROCESS_MUSIC][DEBUG] 2a - bUseDefault = TRUE")
						ENDIF
						#ENDIF
						
						bUseDefault = TRUE						
					ELIF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMusicMid[mc_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] = MUSIC_DEFAULT_HIGH
						#IF IS_DEBUG_BUILD
						IF bEnableScriptAudioSpammyPrints
							PRINTLN("[PROCESS_MUSIC][DEBUG] 2b - bUseDefaultHigh = TRUE")
						ENDIF
						#ENDIF
						
						bUseDefaultHigh = TRUE
					ELSE
						#IF IS_DEBUG_BUILD
						IF bEnableScriptAudioSpammyPrints
							PRINTLN("[PROCESS_MUSIC][DEBUG] 2c - bMid = TRUE")
						ENDIF
						#ENDIF
						
						bMid = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
		IF POINTLESS_MUSIC_STATE_CHANGE(MUSIC_DEFAULT_HIGH)
			bUseDefaultHigh = TRUE
		ENDIF
			
		IF bUseDefault
		OR bUseDefaultHigh		
			#IF IS_DEBUG_BUILD
			IF bEnableScriptAudioSpammyPrints
				PRINTLN("[PROCESS_MUSIC][DEBUG] bUseDefaultHigh: 					", bUseDefaultHigh, "	SET_MUSIC_STATE:...")
				PRINTLN("[PROCESS_MUSIC][DEBUG] bHasPlayerReceivedBattleEvent:  	", MC_playerBD[iPartToUse].bHasPlayerReceivedBattleEvent)
				PRINTLN("[PROCESS_MUSIC][DEBUG] iWanted: 							", MC_playerBD[iPartToUse].iWanted)
				PRINTLN("[PROCESS_MUSIC][DEBUG] iFakeWanted:						", MC_playerBD[iPartToUse].iFakeWanted)
				PRINTLN("[PROCESS_MUSIC][DEBUG] Gang Chasers:						", GET_TOTAL_NUMBER_OF_GANG_BACKUP_CHASING_ALL_TEAMS())
			ENDIF
			#ENDIF
			IF iRule < FMMC_MAX_RULES
				IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScoreReachedMood[iRule] > 0
					IF MC_serverBD.iScoreOnThisRule[iTeam] >= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScoreReachedMood[iRule]
						PRINTLN("[PROCESS_MUSIC][RCC MISSION] iMusicScoreReachedMood Set to: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScoreReachedMood[iRule], " and iScoreOnThisRule is: ", MC_serverBD.iScoreOnThisRule[iTeam], " Overriding Music Cue to the midpoint: ",  g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicMid[iRule])
						SET_MUSIC_STATE(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicMid[iRule])
						bOverride = TRUE
					ENDIF
				ENDIF				
			ENDIF
			
			IF NOT bOverride
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdMusicTimer) > 15000
					IF bPlayerToUseOK
						tempped = PlayerPedToUse
						IF NOT IS_PED_INJURED(tempped)
							IF IS_PED_IN_ANY_VEHICLE(tempped)
								IF IS_PED_IN_ANY_PLANE(tempped)
								OR IS_PED_IN_ANY_HELI(tempped)
									IF bUseDefaultHigh
										SET_MUSIC_STATE(MUSIC_ACTION)
									ELSE
										SET_MUSIC_STATE(MUSIC_AIRBORNE)
									ENDIF
								ELSE
									IF  MC_playerBD[iPartToUse].bHasPlayerReceivedBattleEvent
									OR 	MC_playerBD[iPartToUse].iWanted > 0
									OR  MC_playerBD[iPartToUse].iFakeWanted > 0
									OR GET_TOTAL_NUMBER_OF_GANG_BACKUP_CHASING_ALL_TEAMS() > 0
										IF 	MC_playerBD[iPartToUse].iWanted > 3 
										OR  MC_playerBD[iPartToUse].iFakeWanted > 3 
										OR GET_TOTAL_NUMBER_OF_GANG_BACKUP_CHASING_ALL_TEAMS() > 0
										OR bUseDefaultHigh
											SET_MUSIC_STATE(MUSIC_VEHICLE_CHASE)
										ELSE
											SET_MUSIC_STATE(MUSIC_ACTION)
										ENDIF
									ELSE
										IF bUseDefaultHigh
											SET_MUSIC_STATE(MUSIC_ACTION)
										ELSE
											SET_MUSIC_STATE(MUSIC_SUSPENSE)
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF  MC_playerBD[iPartToUse].bHasPlayerReceivedBattleEvent
								OR 	MC_playerBD[iPartToUse].iWanted > 0
								OR  MC_playerBD[iPartToUse].iFakeWanted > 0
									IF 	MC_playerBD[iPartToUse].iWanted > 3 
									OR  MC_playerBD[iPartToUse].iFakeWanted > 3 
									OR bUseDefaultHigh
										SET_MUSIC_STATE(MUSIC_VEHICLE_CHASE)
									ELSE
										SET_MUSIC_STATE(MUSIC_ACTION)
									ENDIF
								ELSE
									IF bUseDefaultHigh
										SET_MUSIC_STATE(MUSIC_ACTION)
									ELSE
										SET_MUSIC_STATE(MUSIC_SUSPENSE)
									ENDIF
								ENDIF
							ENDIF
							IF Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer)
								IF MC_PlayerBd[iparttouse].iObjCarryCount > 0
									IF bUseDefaultHigh
										SET_MUSIC_STATE(MUSIC_VEHICLE_CHASE)
									ELSE
										SET_MUSIC_STATE(MUSIC_ACTION)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					Bool bReinit = FALSE
					
					IF MC_PlayerBD[iPartToUse].iMusicState = MUSIC_ACTION
						IF  MC_playerBD[iPartToUse].bHasPlayerReceivedBattleEvent
						OR 	MC_playerBD[iPartToUse].iWanted > 0
						OR  MC_playerBD[iPartToUse].iFakeWanted > 0
						OR GET_TOTAL_NUMBER_OF_GANG_BACKUP_CHASING_ALL_TEAMS() > 0
							IF 	MC_playerBD[iPartToUse].iWanted > 3
							OR  MC_playerBD[iPartToUse].iFakeWanted > 3
							OR GET_TOTAL_NUMBER_OF_GANG_BACKUP_CHASING_ALL_TEAMS() > 0
							OR bUseDefaultHigh
								#IF IS_DEBUG_BUILD
								IF bEnableScriptAudioSpammyPrints
									PRINTLN("[PROCESS_MUSIC][DEBUG] Setting from MUSIC_ACTION to MUSIC_VEHICLE_CHASE")
								ENDIF
								#ENDIF
								SET_MUSIC_STATE(MUSIC_VEHICLE_CHASE)
							ELSE
								bReinit = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					IF MC_PlayerBD[iPartToUse].iMusicState = MUSIC_VEHICLE_CHASE
						IF  MC_playerBD[iPartToUse].bHasPlayerReceivedBattleEvent
						OR 	MC_playerBD[iPartToUse].iWanted > 0
						OR  MC_playerBD[iPartToUse].iFakeWanted > 0
						OR GET_TOTAL_NUMBER_OF_GANG_BACKUP_CHASING_ALL_TEAMS() > 0
							IF 	MC_playerBD[iPartToUse].iWanted > 3 
							OR  MC_playerBD[iPartToUse].iFakeWanted > 3 
							OR GET_TOTAL_NUMBER_OF_GANG_BACKUP_CHASING_ALL_TEAMS() > 0
							OR bUseDefaultHigh
								bReinit = TRUE
							ELSE
								#IF IS_DEBUG_BUILD
								IF bEnableScriptAudioSpammyPrints
									PRINTLN("[PROCESS_MUSIC][DEBUG] Setting from MUSIC_VEHICLE_CHASE to MUSIC_ACTION")
								ENDIF
								#ENDIF	
								SET_MUSIC_STATE(MUSIC_ACTION)
							ENDIF
						ENDIF
					ENDIF
					
					IF bReinit
						#IF IS_DEBUG_BUILD
						IF bEnableScriptAudioSpammyPrints
							PRINTLN("[PROCESS_MUSIC][DEBUG] Reinitiatizing Timer")
						ENDIF
						#ENDIF						
						REINIT_NET_TIMER(tdMusicTimer)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF mc_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES 
				IF NOT bOverrideWithAggro
					IF bStart
						#IF IS_DEBUG_BUILD
						IF bEnableScriptAudioSpammyPrints
							PRINTLN("[PROCESS_MUSIC][DEBUG] Else 1")
							PRINTLN("[PROCESS_MUSIC][DEBUG] Calling SET_MUSIC_STATE passing in GET_ROLE_MUSIC_MOOD: ", GET_ROLE_MUSIC_MOOD())
						ENDIF
						#ENDIF
						
						SET_MUSIC_STATE(GET_ROLE_MUSIC_MOOD())
					ELIF bMid
						#IF IS_DEBUG_BUILD
						IF bEnableScriptAudioSpammyPrints
							PRINTLN("[PROCESS_MUSIC][DEBUG] Else 2")
							PRINTLN("[PROCESS_MUSIC][DEBUG] Calling SET_MUSIC_STATE passing in iMusicMid set rule")
						ENDIF
						#ENDIF
						
						SET_MUSIC_STATE(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMusicMid[mc_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]])
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		IF Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)
			IF MC_serverBD.iNumActiveTeams <=2
			
				INT iTeamToCheck = 0
				FOR iTeamToCheck = 0 TO FMMC_MAX_TEAMS - 1
					IF WAS_TEAM_EVER_ACTIVE( iTeamToCheck )
						//PRINTLN("[RCC MISSION] MUSIC - iTeamToCheck = ", iTeamToCheck, " MC_serverBD.iNumberOfPlayingPlayers[ iTeamToCheck ] = ", MC_serverBD.iNumberOfPlayingPlayers[ iTeamToCheck ])
						IF MC_serverBD.iNumberOfPlayingPlayers[ iTeamToCheck ] <= 1
							SET_BIT( iLocalBoolCheck6, LBOOL6_TRIGGER_LTS_END_MUSIC )
							SET_MUSIC_STATE( MUSIC_LTS_ENDING )
							PRINTLN("[PROCESS_MUSIC] [RCC MISSION] MUSIC - LBOOL6_TRIGGER_LTS_END_MUSIC SET")
							PRINTLN("[PROCESS_MUSIC] [RCC MISSION] MUSIC - SET_MUSIC_STATE( MUSIC_LTS_ENDING )")
						ENDIF
					ENDIF
				ENDFOR

			ENDIF
		ELIF (Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer) AND g_FMMC_STRUCT.iMissionEndType != ciMISSION_SCORING_TYPE_FIRST_FINISH)
		OR IS_BIT_SET( g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciUSE_COUNTDOWN )
			IF mc_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
				IF HAS_NET_TIMER_STARTED( MC_serverBD_3.tdObjectiveLimitTimer[ MC_playerBD[ iPartToUse ].iteam ] )
				OR HAS_NET_TIMER_STARTED( MC_serverBD_3.tdMultiObjectiveLimitTimer[MC_playerBD[iPartToUse].iteam] )
					IF HAS_NET_TIMER_STARTED( MC_serverBD_3.tdObjectiveLimitTimer[ MC_playerBD[ iPartToUse ].iteam ] )
				  	  ms = ((GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iObjectiveTimeLimitRule[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]]))  - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdObjectiveLimitTimer[MC_playerBD[iPartToUse].iteam])- MC_serverBD_3.iTimerPenalty[MC_playerBD[iPartToUse].iteam])
					ELSE
						ms = MC_serverBD_3.iMultiObjectiveTimeLimit[MC_playerBD[iPartToUse].iteam] - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[MC_playerBD[iPartToUse].iteam])
					ENDIF
					
					IF ms < (iCountdownBleepTime * 1000)
					AND iCountdownBleepTime > 1
						PLAY_SOUND_FRONTEND(-1,"5_Second_Timer","DLC_HEISTS_GENERAL_FRONTEND_SOUNDS", FALSE)
						PRINTLN("[PROCESS_MUSIC] [RCC MISSION] PROCESS_MUSIC Play 5_Second_Timer iCountdownBleepTime: ", iCountdownBleepTime)
						iCountdownBleepTime--
					ENDIF
					
					// If we're using specific ones don't call to change the music_mid_countdown.
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciUSE_FREEMODE_30S_COUNTDOWN)
					AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciENABLE_VALENTINES_COUNTDOWN_MUSIC)
						IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
							//MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]]
							IF ms < 30000
								
								IF IS_RULE_FINAL_RULE( MC_serverBD_4.iCurrentHighestPriority[mc_playerBD[iPartToUse].iteam] )
									SET_MUSIC_STATE(MUSIC_CTF_ENDING)
									
									SET_BIT(ilocalboolcheck,LBOOL_TRIGGER_CTF_END_MUSIC)
									SET_BIT(iLocalBoolCheck19, LBOOL19_MUSIC_COUNTDOWN_INITIATED)
									SET_CTF_MUSIC_GLOBAL()
									
									PRINTLN( "[PROCESS_MUSIC] [JJT][MUSIC] Triggering End Rule mission countdown score on rule: ", MC_serverBD_4.iCurrentHighestPriority[mc_playerBD[iPartToUse].iteam] )
								ELSE
									iMidCountdownRuleNum = MC_serverBD_4.iCurrentHighestPriority[mc_playerBD[iPartToUse].iteam]
									iMidCountdownLastScore = MC_PlayerBD[iPartToUse].iMusicState
									SET_MUSIC_STATE(MUSIC_MID_COUNTDOWN)
									SET_BIT(iLocalBoolCheck19, LBOOL19_MUSIC_COUNTDOWN_INITIATED)
									PRINTLN( "[PROCESS_MUSIC] [JJT][MUSIC] Triggering mid mission countdown score on rule: ", iMidCountdownRuleNum )
								ENDIF
								
								REINIT_NET_TIMER( tdMusicTimer )

							ELIF ms < 32000
								IF IS_RULE_FINAL_RULE( MC_serverBD_4.iCurrentHighestPriority[mc_playerBD[iPartToUse].iteam] )
									IF NOT IS_BIT_SET(ilocalboolcheck,LBOOL_TRIGGER_CTF_END_MUSIC)
										TRIGGER_MUSIC_EVENT("MP_PRE_COUNTDOWN_RADIO")
										PRINTLN("[PROCESS_MUSIC] [RCC MISSION] TRIGGER_MUSIC_EVENT(\"MP_PRE_COUNTDOWN_RADIO\")")
										SET_BIT(ilocalboolcheck,LBOOL_TRIGGER_CTF_END_MUSIC)
										SET_BIT(iLocalBoolCheck19, LBOOL19_MUSIC_COUNTDOWN_INITIATED)
									ENDIF
								ENDIF
								
								REINIT_NET_TIMER(tdMusicTimer)
							ELIF ms < 35000
								IF IS_RULE_FINAL_RULE( MC_serverBD_4.iCurrentHighestPriority[mc_playerBD[iPartToUse].iteam] )
									IF NOT IS_BIT_SET(ilocalboolcheck,LBOOL_TRIGGER_CTF_PRE_END_MUSIC)
										SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE)
										TRIGGER_MUSIC_EVENT("PRE_MP_DM_COUNTDOWN_30_SEC")
										PRINTLN("[PROCESS_MUSIC] [RCC MISSION] TRIGGER_MUSIC_EVENT(\"MP_PRE_COUNTDOWN_RADIO\")")
										SET_BIT(ilocalboolcheck,LBOOL_TRIGGER_CTF_PRE_END_MUSIC)
										SET_BIT(iLocalBoolCheck19, LBOOL19_MUSIC_COUNTDOWN_INITIATED)
									ENDIF
								ENDIF
								
								REINIT_NET_TIMER(tdMusicTimer)
							ENDIF
						ELSE
							IF ms < 30000 
							
								IF IS_RULE_FINAL_RULE( MC_serverBD_4.iCurrentHighestPriority[mc_playerBD[iPartToUse].iteam] )
									iMidCountdownLastScore = MC_PlayerBD[iPartToUse].iMusicState
									SET_MUSIC_STATE(MUSIC_CTF_ENDING)
									SET_BIT(ilocalboolcheck,LBOOL_TRIGGER_CTF_END_MUSIC)
									SET_BIT(iLocalBoolCheck19, LBOOL19_MUSIC_COUNTDOWN_INITIATED)
									PRINTLN( "[PROCESS_MUSIC] [JJT][MUSIC] Triggering End Rule mission countdown score on rule: ", MC_serverBD_4.iCurrentHighestPriority[mc_playerBD[iPartToUse].iteam] )
								ELSE
									iMidCountdownRuleNum = MC_serverBD_4.iCurrentHighestPriority[mc_playerBD[iPartToUse].iteam]
									SET_MUSIC_STATE(MUSIC_MID_COUNTDOWN)
									SET_BIT(iLocalBoolCheck19, LBOOL19_MUSIC_COUNTDOWN_INITIATED)
									PRINTLN( "[PROCESS_MUSIC] [JJT][MUSIC] Triggering mid mission countdown score on rule: ", iMidCountdownRuleNum )
								ENDIF
								
								
								REINIT_NET_TIMER(tdMusicTimer)
							ELIF ms < 35000
								IF IS_RULE_FINAL_RULE( MC_serverBD_4.iCurrentHighestPriority[mc_playerBD[iPartToUse].iteam] )
									IF NOT IS_BIT_SET(ilocalboolcheck,LBOOL_TRIGGER_CTF_PRE_END_MUSIC)

										SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE)
										TRIGGER_MUSIC_EVENT("PRE_MP_DM_COUNTDOWN_30_SEC")
										PRINTLN("[PROCESS_MUSIC] [RCC MISSION] TRIGGER_MUSIC_EVENT(\"PRE_MP_DM_COUNTDOWN_30_SEC\")")
										SET_BIT(ilocalboolcheck,LBOOL_TRIGGER_CTF_PRE_END_MUSIC)
										SET_BIT(iLocalBoolCheck19, LBOOL19_MUSIC_COUNTDOWN_INITIATED)
									ENDIF
								ENDIF
								
								REINIT_NET_TIMER(tdMusicTimer)
							ENDIF
						ENDIF
					ELSE
						PRINTLN("[PROCESS_MUSIC] [RCC MISSION] ms < 30000 CountDown music should start but we are already playing creator set versions.")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF MC_PlayerBD[iPartToUse].iMusicState = MUSIC_MID_COUNTDOWN
			IF MC_serverBD_4.iCurrentHighestPriority[mc_playerBD[iPartToUse].iteam] > iMidCountdownRuleNum
				PRINTLN( "[PROCESS_MUSIC] [JJT][MUSIC] Changing the music state back to ", iMidCountdownLastScore )
				
				CLEAR_BIT(iLocalBoolCheck3,LBOOL3_MUSIC_PLAYING)
				SET_MUSIC_STATE( iMidCountdownLastScore )
			ENDIF
			
		ENDIF
	ENDIF
	
	IF iRule < FMMC_MAX_RULES
		IF bOverrideWithAggro
			PRINTLN("[PROCESS_MUSIC][RCC MISSION] iMusicScoreAggroMood Set to: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScoreAggroMood[iRule], " Overriding the Music now!")
			SET_MUSIC_STATE(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScoreAggroMood[iRule])
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck,LBOOL_MUSIC_CHANGED)	
	AND NOT (IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC) // Countdown musics have an inbuilt stop event.
	AND NOT IS_BIT_SET(iLocalBoolCheck15, LBOOL15_HAS_STARTED_VAL_COUNTDOWN_MUSIC)
	AND NOT IS_BIT_SET(iLocalBoolCheck20, LBOOL20_HAS_STARTED_IE_COUNTDOWN_MUSIC))
	AND NOT CONTENT_IS_USING_ARENA()
		BOOL bNotChanged
			
		SWITCH MC_PlayerBD[iPartToUse].iMusicState

			CASE MUSIC_SUSPENSE
			CASE MUSIC_VEHICLE_CHASE
			CASE MUSIC_AIRBORNE
			CASE MUSIC_ACTION
			CASE MUSIC_DRIVING     
			CASE MUSIC_MED_INTENSITY 
			CASE MUSIC_SUSPENSE_LOW  
			CASE MUSIC_SUSPENSE_HIGH 
				IF AUDIO_IS_SCRIPTED_MUSIC_PLAYING()
				AND NOT IS_BIT_SET(iLocalBoolCheck3,LBOOL3_MUSIC_PLAYING)
					IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
						SCRIPT_TRIGGER_MUSIC_EVENT_STOP()
						PRINTLN("[PROCESS_MUSIC] [RCC MISSION] MUSIC - TRIGGER_MUSIC_EVENT(MP_MC_STOP) - A")
					ENDIF					
					bNotChanged = TRUE					
				ELSE
					IF NOT IS_BIT_SET(iLocalBoolCheck3,LBOOL3_MUSIC_PLAYING)
						PRINTLN( "[PROCESS_MUSIC] [JJT][MUSIC] No music playing, setting music to ", g_FMMC_STRUCT.iAudioScore )
						SET_MISSION_MUSIC(g_FMMC_STRUCT.iAudioScore, IS_BIT_SET(iLocalBoolCheck13, LBOOL13_REINITTED_MUSIC_FOR_ENTERING_SPECTATE))
						SET_BIT(iLocalBoolCheck3,LBOOL3_MUSIC_PLAYING)
					ENDIF
					IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
						IF NOT IS_BIT_SET(iLocalBoolCheck19, LBOOL19_MUSIC_COUNTDOWN_INITIATED)
						AND NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC)
							START_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
							TRIGGER_MUSIC_EVENT(GET_MOOD_NAME_FOR_SONG(g_FMMC_STRUCT.iAudioScore, MC_PlayerBD[iPartToUse].iMusicState))
							PRINTLN("[PROCESS_MUSIC] TRIGGER_MUSIC_EVENT(", GET_MOOD_NAME_FOR_SONG(g_FMMC_STRUCT.iAudioScore, MC_PlayerBD[iPartToUse].iMusicState), ")")
							SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE MUSIC_SILENT
				IF AUDIO_IS_SCRIPTED_MUSIC_PLAYING()
				AND NOT IS_BIT_SET(iLocalBoolCheck3,LBOOL3_MUSIC_PLAYING)
					SCRIPT_TRIGGER_MUSIC_EVENT_STOP()
					bNotChanged = TRUE
					PRINTLN("[PROCESS_MUSIC] [RCC MISSION] MUSIC - TRIGGER_MUSIC_EVENT(MP_MC_STOP) - B")
					CPRINTLN(DEBUG_MISSION, "[PROCESS_MUSIC] [RCC MISSION] MUSIC - TRIGGER_MUSIC_EVENT(MP_MC_STOP)")
				ELSE
					IF NOT IS_BIT_SET(iLocalBoolCheck3,LBOOL3_MUSIC_PLAYING)
						SET_MISSION_MUSIC(g_FMMC_STRUCT.iAudioScore)
						SET_BIT(iLocalBoolCheck3,LBOOL3_MUSIC_PLAYING)
						CPRINTLN(DEBUG_MISSION, "[PROCESS_MUSIC] [RCC MISSION] MUSIC - SET_MISSION_MUSIC(", g_FMMC_STRUCT.iAudioScore, ")")
					ENDIF
					STOP_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
					CPRINTLN(DEBUG_MISSION, "[PROCESS_MUSIC] [RCC MISSION] MUSIC - STOP_AUDIO_SCENE(MP_POSITIONED_RADIO_MUTE_SCENE)")
					TRIGGER_MUSIC_EVENT(GET_MOOD_NAME_FOR_SONG(g_FMMC_STRUCT.iAudioScore,MC_PlayerBD[iPartToUse].iMusicState))
					PRINTLN("[PROCESS_MUSIC] TRIGGER_MUSIC_EVENT(", GET_MOOD_NAME_FOR_SONG(g_FMMC_STRUCT.iAudioScore,MC_PlayerBD[iPartToUse].iMusicState), ")")
					CPRINTLN(DEBUG_MISSION, "[PROCESS_MUSIC] [RCC MISSION] MUSIC - TRIGGER_MUSIC_EVENT(", GET_MOOD_NAME_FOR_SONG(g_FMMC_STRUCT.iAudioScore,MC_PlayerBD[iPartToUse].iMusicState), ")")
					SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE)
					CPRINTLN(DEBUG_MISSION, "[PROCESS_MUSIC] [RCC MISSION] MUSIC - SET_AUDIO_FLAG(AllowScoreAndRadio, TRUE)")
					IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
						SET_BIT(iTutorialMissionCutBitset, biTut_NeedToSetRadioStation)
						PRINTLN("[PROCESS_MUSIC] [dsw] [MAINATIN_LAMAR_FOR_TUTORIAL_CUTSCENE] Hit MUSIC_SILENT")
					ENDIF
				ENDIF
			BREAK
			
			CASE MUSIC_STOP
				STOP_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
				SCRIPT_TRIGGER_MUSIC_EVENT_STOP()
				PRINTLN("[PROCESS_MUSIC] TRIGGER_MUSIC_EVENT(\"MP_MC_STOP\")")
				SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE)
				PRINTLN("[PROCESS_MUSIC] [RCC MISSION] MUSIC - TRIGGER_MUSIC_EVENT(MP_MC_STOP) - C")
			BREAK
			CASE MUSIC_FAIL
				STOP_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
				IF NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC)
					SCRIPT_TRIGGER_MUSIC_EVENT_FAIL()
				ENDIF
				PRINTLN("[PROCESS_MUSIC] [RCC MISSION] TRIGGER_MUSIC_EVENT(\"MP_MC_FAIL\") Pm1")
				SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE)
			BREAK
			
			CASE MUSIC_LTS_ENDING
				START_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
				TRIGGER_MUSIC_EVENT("MP_LTS")
				PRINTLN("[PROCESS_MUSIC] [PROCESS_MUSIC] [RCC MISSION] TRIGGER_MUSIC_EVENT(\"MP_LTS\")")
				SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
			BREAK
			
			CASE MUSIC_MID_COUNTDOWN			
			CASE MUSIC_CTF_ENDING
				
				START_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
				TRIGGER_MUSIC_EVENT("MP_DM_COUNTDOWN_30_SEC")
				PRINTLN("[PROCESS_MUSIC] [RCC MISSION] TRIGGER_MUSIC_EVENT(\"MP_DM_COUNTDOWN_30_SEC\")")
				SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
				
			BREAK
			
		/*	
			CASE MUSIC_ALT_COUNTDOWN
				START_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
				TRIGGER_MUSIC_EVENT("FM_COUNTDOWN_30S")
				PRINTLN("[RCC MISSION] TRIGGER_MUSIC_EVENT(\"FM_COUNTDOWN_30S\")")
				SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
			BREAK
		*/	
			CASE MUSIC_COCK_SONG
				IF AUDIO_IS_SCRIPTED_MUSIC_PLAYING()
					SCRIPT_TRIGGER_MUSIC_EVENT_STOP()
					bNotChanged = TRUE
					PRINTLN("[PROCESS_MUSIC] [RCC MISSION] MUSIC - TRIGGER_MUSIC_EVENT(MP_MC_STOP) - D")
					PRINTLN("[PROCESS_MUSIC] [RCC MISSION] TRIGGER_MUSIC_EVENT(\"MP_MC_STOP\")")
				ELSE
					CLEAR_BIT(iLocalBoolCheck3,LBOOL3_MUSIC_PLAYING)
					START_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
					TRIGGER_MUSIC_EVENT("MP_MC_START_COCK_SONG_1")
					PRINTLN("[PROCESS_MUSIC] TRIGGER_MUSIC_EVENT(\"MP_MC_START_COCK_SONG_1\")")
					TRIGGER_MUSIC_EVENT("MP_MC_GENERAL_1")
					PRINTLN("[PROCESS_MUSIC] TRIGGER_MUSIC_EVENT(\"MP_DM_COUNMP_MC_GENERAL_1TDOWN_30_SEC\")")
					SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
				ENDIF
			BREAK
			
			CASE MUSIC_WAVERY
				IF AUDIO_IS_SCRIPTED_MUSIC_PLAYING()
					SCRIPT_TRIGGER_MUSIC_EVENT_STOP()
					bNotChanged = TRUE
					PRINTLN("[PROCESS_MUSIC] [RCC MISSION] MUSIC - TRIGGER_MUSIC_EVENT(MP_MC_STOP) - E")
				ELSE
					CLEAR_BIT(iLocalBoolCheck3,LBOOL3_MUSIC_PLAYING)
					START_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
					TRIGGER_MUSIC_EVENT("MP_MC_START_WAVERY_1")
					PRINTLN("[PROCESS_MUSIC] TRIGGER_MUSIC_EVENT(\"MP_MC_START_WAVERY_1\")")
					TRIGGER_MUSIC_EVENT("MP_MC_GENERAL_1")
					PRINTLN("[PROCESS_MUSIC] TRIGGER_MUSIC_EVENT(\"MP_MC_GENERAL_1\")")
					SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
				ENDIF
			BREAK

 			CASE MUSIC_SEA_RACE	
				IF AUDIO_IS_SCRIPTED_MUSIC_PLAYING()
					SCRIPT_TRIGGER_MUSIC_EVENT_STOP()
					PRINTLN("[PROCESS_MUSIC] TRIGGER_MUSIC_EVENT(\"MP_MC_STOP\")")
					bNotChanged = TRUE
					PRINTLN("[PROCESS_MUSIC] [RCC MISSION] MUSIC - TRIGGER_MUSIC_EVENT(MP_MC_STOP) - F")
				ELSE
					CLEAR_BIT(iLocalBoolCheck3,LBOOL3_MUSIC_PLAYING)
					START_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
					PRINTLN("[PROCESS_MUSIC] TRIGGER_MUSIC_EVENT(\"MP_MC_START_TOUGHT_SEA_RACE_1\")")
					TRIGGER_MUSIC_EVENT("MP_MC_START_TOUGHT_SEA_RACE_1")
					TRIGGER_MUSIC_EVENT("MP_MC_GENERAL_1")
					PRINTLN("[PROCESS_MUSIC] TRIGGER_MUSIC_EVENT(\"MP_MC_GENERAL_1\")")
					SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
				ENDIF
			BREAK
			
			CASE MUSIC_MOOD_NONE
				IF AUDIO_IS_SCRIPTED_MUSIC_PLAYING()
					PRINTLN("[PROCESS_MUSIC] [RCC MISSION] PROCESS_MUSIC - No music mood, not doing anything")
					//PRINTLN("[PROCESS_MUSIC] TRIGGER_MUSIC_EVENT(\"MP_MC_STOP\")")
					bNotChanged = FALSE
					//PRINTLN("[PROCESS_MUSIC] [RCC MISSION] MUSIC - TRIGGER_MUSIC_EVENT(MP_MC_STOP) - E")
				ELSE
					CLEAR_BIT(iLocalBoolCheck3,LBOOL3_MUSIC_PLAYING)
				ENDIF
			BREAK
			
		ENDSWITCH
		
		IF NOT bNotChanged
			PRINTLN("[PROCESS_MUSIC] NEW MUSIC SETTING APPLIED")
			SET_BIT(iLocalBoolCheck,LBOOL_MUSIC_CHANGED) 
		ENDIF
		
	ENDIF

#IF IS_NEXTGEN_BUILD
	// Trigger Danger Zone music event for Humane Labs EMP V2
	IF IS_BIT_SET(iLocalBoolCheck9, LBOOL9_PREPARE_DANGER_ZONE)
		IF PREPARE_MUSIC_EVENT("MP_MC_DANGERZONE")
			PRINTLN("[PROCESS_MUSIC] [RCC MISSION][MJM] TRIGGER_MUSIC_EVENT(MP_MC_DANGERZONE)")
			IF TRIGGER_MUSIC_EVENT("MP_MC_DANGERZONE")
				PRINTLN("[PROCESS_MUSIC] Music Event successfully prepared and has been triggered. Dangerzone should now start playing.")
			ELSE
				PRINTLN("[PROCESS_MUSIC] Music Event successfully prepared but has *** NOT *** been triggered.")
			ENDIF
			
			PRINTLN("[PROCESS_MUSIC] [RCC MISSION][MJM] START_AUDIO_SCENE(DLC_HEISTS_BIOLAB_EMP_DANGERZONE_SCENE)")
			START_AUDIO_SCENE("DLC_HEISTS_BIOLAB_EMP_DANGERZONE_SCENE")
			
			PRINTLN("[PROCESS_MUSIC] [RCC MISSION][MJM] CLEAR_BIT(iLocalBoolCheck9, LBOOL9_PREPARE_DANGER_ZONE)")
			CLEAR_BIT(iLocalBoolCheck9, LBOOL9_PREPARE_DANGER_ZONE)
			
			PRINTLN("[PROCESS_MUSIC] [RCC MISSION][MJM] SET_BIT(iLocalBoolCheck9, LBOOL9_DANGER_ZONE_TRIGGERED)")
			SET_BIT(iLocalBoolCheck9, LBOOL9_DANGER_ZONE_TRIGGERED)
			
			PRINTLN("[PROCESS_MUSIC] [RCC MISSION][MJM] SET_AUDIO_FLAG(AllowForceRadioAfterRetune, FALSE)")
			SET_AUDIO_FLAG("AllowForceRadioAfterRetune", FALSE)
			
			PRINTLN("[PROCESS_MUSIC] Event triggered, bit cleared and audio flag set FALSE")
		ELSE
			PRINTLN("[PROCESS_MUSIC] Music Event HAS NOT successfully prepared.")
		ENDIF
		
	ENDIF
#ENDIF

	//If we aren't showing the timer, don't play the countdown timer bleeps
	IF (NOT IS_SPECTATOR_HUD_HIDDEN() OR CONTENT_IS_USING_ARENA())
	AND NOT IS_SPECTATOR_SHOWING_NEWS_HUD() 
	AND (MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES) //To stop an array overrun on the next line
	AND ((NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitset[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE_HIDE_HUD))
		  OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitset[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE_OVERRIDE_HIDEHUD_SHOW_MULTIRULE_TIMER)
		  OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciUSE_CIRCLE_HUD_TIMER))
		//Countdown sounds:	
		PROCESS_COUNTDOWN_TIMER_SOUNDS()
	ENDIF
	
	IF MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetFive[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE5_MUSIC_CAPTURE_BAR_CUES)
			INT iCapProgress = MC_ServerBD.iCaptureBarPercentage[MC_playerBD[iPartToUse].iteam]
			VECTOR vTempPos = GET_ENTITY_COORDS(LocalPlayerPed)
			IF VDIST(vTempPos, vDZPlayerSpawnLoc) < 100
			AND iDropZoneMusicProgress = 2
				iDropZoneMusicProgress = 0
			ENDIF
			SWITCH iDropZoneMusicProgress
				CASE 0
					IF iDropZoneMusicEvent != ciDROPZONE_HELI
						TRIGGER_MUSIC_EVENT("DROPZONE_HELI")
						iDropZoneMusicEvent = ciDROPZONE_HELI
					ENDIF
					IF IS_PED_IN_PARACHUTE_FREE_FALL(LocalPlayerPed)
						iDropZoneMusicProgress = 1
					ENDIF
				BREAK
				CASE 1
					IF iDropZoneMusicEvent != ciDROPZONE_JUMP
						TRIGGER_MUSIC_EVENT("DROPZONE_JUMP")
						iDropZoneMusicEvent = ciDROPZONE_JUMP
					ENDIF
					IF NOT IS_ENTITY_IN_AIR(LocalPlayerPed)
						iDropZoneMusicProgress = 2
					ENDIF
				BREAK
				CASE 2
					IF iCapProgress < 60
						IF iDropZoneMusicEvent != ciDROPZONE_LAND
							TRIGGER_MUSIC_EVENT("DROPZONE_LAND")
							iDropZoneMusicEvent = ciDROPZONE_LAND
						ENDIF
					ELIF iCapProgress >= 60 AND iCapProgress < 75
						IF iDropZoneMusicEvent != ciDROPZONE_ACTION
							TRIGGER_MUSIC_EVENT("DROPZONE_ACTION")
							iDropZoneMusicEvent = ciDROPZONE_ACTION
						ENDIF
					ELIF iCapProgress >= 75
						IF iDropZoneMusicEvent != ciDROPZONE_ACTION_HIGH
							TRIGGER_MUSIC_EVENT("DROPZONE_ACTION_HIGH")
							iDropZoneMusicEvent = ciDROPZONE_ACTION_HIGH
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF

ENDPROC



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: DIALOGUE TRIGGERS !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************

PROC MANAGE_OBJECTIVE_HELP_TEXT_REPRINT()

	// While a timer of 4 seconds is running, check for whether the player dies. This bitset will be set when a print_help is called in TRIGGER_DIALOGUE
	IF IS_BIT_SET(iLocalBoolCheck18, LBOOL18_CHECK_FOR_DEATH_OBJ_TEXT_REPRINT)
		PRINTLN("[LM][OBJ_TEXT_REPRINT] - MANAGE_OBJECTIVE_HELP_TEXT_REPRINT: Timer ")
		IF HAS_NET_TIMER_STARTED(tdObjHelpDeathCheckTimer)
			PRINTLN("[LM][OBJ_TEXT_REPRINT] - MANAGE_OBJECTIVE_HELP_TEXT_REPRINT: Timer Started - Checking to make sure it has not expired.")
			IF NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(tdObjHelpDeathCheckTimer, ci_OBJ_HELP_DEATH_CHECK_TIME)
				PRINTLN("[LM][OBJ_TEXT_REPRINT] - Checking to make sure the player is dead...")
				IF IS_PED_INJURED(localplayerped)
					PRINTLN("[LM][OBJ_TEXT_REPRINT] - Player died during the OBJ text print. So we must reprint it when they are alive.")
					RESET_NET_TIMER(tdObjHelpDeathCheckTimer)
					CLEAR_BIT(iLocalBoolCheck18, LBOOL18_CHECK_FOR_DEATH_OBJ_TEXT_REPRINT)
					SET_BIT(iLocalBoolCheck18, LBOOL18_DIED_AND_REQUIRES_OBJ_TEXT_REPRINT)
				ENDIF
			ENDIF
		ELIF NOT HAS_NET_TIMER_STARTED(tdObjHelpDeathCheckTimer)
			PRINTLN("[LM][OBJ_TEXT_REPRINT] - We tried to print the text, start a timer to check for death.")
			RESET_NET_TIMER(tdObjHelpDeathCheckTimer)
			START_NET_TIMER(tdObjHelpDeathCheckTimer)
		ENDIF
	ENDIF
		
	// We died during 4 (ci_OBJ_HELP_DEATH_CHECK_TIME) seconds of the OBJ help showing. Therefore the player may have missed it, so when they're back alive we're reprinting that bad boy.
	IF IS_BIT_SET(iLocalBoolCheck18, LBOOL18_DIED_AND_REQUIRES_OBJ_TEXT_REPRINT)
		PRINTLN("[LM][OBJ_TEXT_REPRINT] - Checking to make sure the player isn't dead...")
		IF NOT IS_PED_INJURED(localplayerped)
			PRINTLN("[LM][OBJ_TEXT_REPRINT] - Ped is no longer dead.")
			IF IS_SCREEN_FADED_IN()
			AND NOT IS_HUD_HIDDEN()
				IF NOT IS_STRING_NULL_OR_EMPTY(sLabelObjHelpReprint)
					PRINTLN("[LM][OBJ_TEXT_REPRINT] - Reprinting the cached help text.")
					PRINT_HELP(sLabelObjHelpReprint)
				ENDIF
				
				sLabelObjHelpReprint = ""
				CLEAR_BIT(iLocalBoolCheck18, LBOOL18_DIED_AND_REQUIRES_OBJ_TEXT_REPRINT)
			ELSE 
				PRINTLN("[LM][OBJ_TEXT_REPRINT] - Screen/HUD not ready...")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC STRING GET_CORRECT_FILTERS_HELP_STRING()
	IF IS_PC_VERSION()
	AND IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		PRINTLN("[KH] GET_CORRECT_FILTERS_HELP_STRING - PC Version, using keyboard, printing PC filters help")
		RETURN "FLTR_PC_HELP"
	ELSE
		PRINTLN("[KH] GET_CORRECT_FILTERS_HELP_STRING - Printing normal filters help")
		RETURN "FILTERS_HELP"
	ENDIF
ENDFUNC

FUNC STRING GET_M_OR_F_ASSISTANT_SUFFIX_FOR_TEXT_LABEL_DIALOGUE()
	IF SVM_FLOW_IS_CURRENT_MISSION_SVM_MISSION()		
		IF g_FMMC_STRUCT.iContactCharEnum = Get_Hash_Of_enumCharacterList_Contact(CHAR_CEOASSIST)			
			IF IS_OFFICE_PA_MALE(TRUE)
				PRINTLN("[LM][GET_M_OR_F_ASSISTANT_SUFFIX_FOR_TEXT_LABEL_DIALOGUE] - Appending: M")
				RETURN "M"
			ELSE
				PRINTLN("[LM][GET_M_OR_F_ASSISTANT_SUFFIX_FOR_TEXT_LABEL_DIALOGUE] - Appending: F")
				RETURN "F"
			ENDIF
		ENDIF
	ENDIF
	RETURN ""
ENDFUNC

FUNC TEXT_LABEL_23 PROCESS_APPENDING_DIALOGUE_TRIGGER_SUFFIXES(STRING sRoot, INT iDialogueTrigger = -1)
	
	TEXT_LABEL_23 tl23Root = sRoot
	
	STRING sAssistantSuffix = GET_M_OR_F_ASSISTANT_SUFFIX_FOR_TEXT_LABEL_DIALOGUE()
	IF NOT IS_STRING_NULL_OR_EMPTY(sAssistantSuffix)
		tl23Root += sAssistantSuffix
		PRINTLN("[Dialogue] - PROCESS_APPENDING_DIALOGUE_TRIGGER_SUFFIXES - Applying assistant suffix")
		RETURN tl23Root
	ENDIF
	
	IF iDialogueTrigger = -1
		RETURN tl23Root
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].tl3AppendOnAggro)
		IF HAS_ANY_TEAM_TRIGGERED_AGGRO()
			tl23Root += g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].tl3AppendOnAggro
			PRINTLN("[Dialogue] - PROCESS_APPENDING_DIALOGUE_TRIGGER_SUFFIXES - Applying aggro suffix")
			RETURN tl23Root
		ENDIF
	ENDIF
	
	RETURN tl23Root
	
ENDFUNC

FUNC BOOL CREATE_DIALOGUE(INT iDialogueTrigger, structPedsForConversation& speechPedStructToUse, STRING sBlock, STRING sRoot, enumConversationPriority eConvPriority)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iDialogueBitset3, iBS_Dialogue3_UsePhoneCall)
		RETURN CREATE_CONVERSATION(speechPedStructToUse, sBlock, sRoot, eConvPriority)
	ELSE
		IF GET_INCOMING_CONTACT_CHARACTER_FROM_CREATOR(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iCallCharacter) != MAX_NRM_CHARACTERS_PLUS_DUMMY
			RETURN CHAR_CALL_PLAYER_CELLPHONE_FORCE_ANSWER(speechPedStructToUse, GET_INCOMING_CONTACT_CHARACTER_FROM_CREATOR(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iCallCharacter), sBlock, sRoot, eConvPriority)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL TRIGGER_DIALOGUE()

	INT iPedVoice
	TEXT_LABEL_23 sLabel
	
	IF IS_SCREEN_FADING_OUT()
	OR IS_SCREEN_FADED_OUT()
		PRINTLN("[RCC MISSION] TRIGGER DIALOGUE - Blocking dialogue trigger ", iDialogueProgress," Because IS_SCREEN_FADING_OUT OR IS_SCREEN_FADED_OUT (these commands suppress/clea subtitles)")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[ iDialogueProgress ].iDialogueBitset2, iBS_Dialogue2EnableEarlyEnd)
		IF IS_BIT_SET(iLocalBoolCheck20, LBOOL20_SKIP_HELP_TEXT)
			PRINTLN("[RCC MISSION] TRIGGER DIALOGUE - Blocking dialogue trigger ", iDialogueProgress," LBOOL20_SKIP_HELP_TEXT")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[ iDialogueProgress ].iDialogueBitset2, iBS_Dialogue2DontPlayWhenAloneInVeh)
		IF IS_PED_IN_ANY_VEHICLE(PlayerPedToUse)
			IF GET_NUM_PEDS_IN_VEHICLE(GET_VEHICLE_PED_IS_IN(PlayerPedToUse)) < 2
				PRINTLN("[RCC MISSION] TRIGGER DIALOGUE - Blocking dialogue trigger ", iDialogueProgress," from activating as only 1 person in vehicle")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset3, iBS_Dialogue3_PlayHoverModeSFX)
		IF NOT IS_BIT_SET(iDialogueProgressSFX1Played[iDialogueProgress/32], iDialogueProgress%32)
			PRINTLN("[RCC MISSION] TRIGGER DIALOGUE - dialogue trigger ", iDialogueProgress," Plays a sound (Flight_Unlock). Returning TRUE")
			PLAY_SOUND_FRONTEND(-1, "Hover_Unlock", "DLC_XM17_IAA_Deluxos_Sounds")
			SET_BIT(iDialogueProgressSFX1Played[iDialogueProgress/32], iDialogueProgress%32)
		ENDIF
		//RETURN TRUE
	ENDIF	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset3, iBS_Dialogue3_PlayFlightModeSFX)
		IF NOT IS_BIT_SET(iDialogueProgressSFX2Played[iDialogueProgress/32], iDialogueProgress%32)
			PRINTLN("[RCC MISSION] TRIGGER DIALOGUE - dialogue trigger ", iDialogueProgress," Plays a sound (Hover_Unlock). Returning TRUE")
			PLAY_SOUND_FRONTEND(-1, "Flight_Unlock", "DLC_XM17_IAA_Deluxos_Sounds")	
			SET_BIT(iDialogueProgressSFX2Played[iDialogueProgress/32], iDialogueProgress%32)
		ENDIF
		//RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset,iBS_DialogueHelpText)
	AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		PRINTLN("[RCC MISSION] TRIGGER DIALOGUE - iDialogueProgress: ", iDialogueProgress," iBS_DialogueHelpText")
		
		#IF NOT IS_NEXTGEN_BUILD
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset2,iBS_Dialogue2HelpTextNGOnly)
		#ENDIF
			IF IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[ iDialogueProgress ].iDialogueBitset2, iBS_Dialogue2AimFireDriveByOnly )
				PRINTLN("[RCC MISSION] TRIGGER_DIALOGUE - iBS_Dialogue2AimFireDriveByOnly is set")
				IF GET_IS_USING_ALTERNATE_DRIVEBY()
					PRINTLN("[RCC MISSION] TRIGGER_DIALOGUE - player is using alternative control scheme")
					sLabel = g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].tlRoot
					PRINTLN("[RCC MISSION] TRIGGER_DIALOGUE - DISPLAYING HELP TEXT:", sLabel)
					PRINT_HELP(sLabel)
					
				ENDIF
			ENDIF
			IF IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[ iDialogueProgress ].iDialogueBitset2, iBS_Dialogue2FireDriveByOnly)
				PRINTLN("[RCC MISSION] TRIGGER_DIALOGUE - iBS_Dialogue2FireDriveByOnly is set")
				IF NOT GET_IS_USING_ALTERNATE_DRIVEBY()
					PRINTLN("[RCC MISSION] TRIGGER_DIALOGUE - player is not using alternative control scheme")
					sLabel = g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].tlRoot
					PRINTLN("[RCC MISSION] TRIGGER_DIALOGUE - DISPLAYING HELP TEXT:", sLabel)
					PRINT_HELP(sLabel)
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[ iDialogueProgress ].iDialogueBitset2, iBS_Dialogue2AimFireDriveByOnly )
			AND NOT IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[ iDialogueProgress ].iDialogueBitset2, iBS_Dialogue2FireDriveByOnly)
				sLabel = g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].tlRoot
				
				IF ARE_STRINGS_EQUAL(sLabel, "FILTERS_HELP")
					sLabel = GET_CORRECT_FILTERS_HELP_STRING()
				ENDIF
				
				IF IS_ARENA_WARS_JOB()
				AND IS_HELP_MESSAGE_BEING_DISPLAYED()
					RETURN FALSE
				ENDIF
				
				IF bLocalPlayerOK
					PRINTLN("[RCC MISSION] TRIGGER_DIALOGUE - NOT iBS_Dialogue2AimFireDriveByOnly, NOT iBS_Dialogue2FireDriveByOnly - DISPLAYING HELP TEXT: ", sLabel)
					sLabelObjHelpReprint = sLabel
					SET_BIT(iLocalBoolCheck18, LBOOL18_CHECK_FOR_DEATH_OBJ_TEXT_REPRINT)
					PRINTLN("[LM][OBJ_TEXT_REPRINT] TRIGGER_DIALOGUE - Start death check timers and cache the help text for use later.")
				
					PRINT_HELP(sLabel)
				ELSE					
					sLabelObjHelpReprint = sLabel
					SET_BIT(iLocalBoolCheck18, LBOOL18_CHECK_FOR_DEATH_OBJ_TEXT_REPRINT)
					PRINTLN("[LM][OBJ_TEXT_REPRINT] TRIGGER_DIALOGUE - Start death check timers and cache the help text for use later.")					
				ENDIF
			ENDIF
		#IF NOT IS_NEXTGEN_BUILD
		ENDIF
		#ENDIF
	
		RETURN TRUE
	ENDIF
	
	PRINTLN("[RCC MISSION][MJM] TRIGGER_DIALOGUE - ROOT IS ",g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].tlRoot," Minigame: ",IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset,iBS_DialogueTriggerMinigame))
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[ iDialogueProgress ].iDialogueBitset2, iBS_Dialogue2EnableEarlyEnd)
		sTLToSkip =  sLabel
	ELSE
		sTLToSkip =  ""
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset, iBS_DialogueEnableQuickMask)
		PRINTLN("[RCC MISSION][MJM] TRIGGER_DIALOGUE - IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset, iBS_DialogueEnableQuickMask) - QUICK EQUIP MASK - LBS_MASK_QUICK_EQUIP_AVAILABLE - SET")
		SET_BIT(iLocalQuickMaskBitSet, LBS_MASK_QUICK_EQUIP_AVAILABLE)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset2, iBS_Dialogue2FinalLap)
		PRINTLN("[RCC MISSION][MJM] FINAL LAP EVAL : LAPS COMPLETED - ", MC_ServerBD_4.iTeamLapsCompleted[MC_playerBD[iPartToUse].iteam] + 1," OF ", g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMissionLapLimit)
		PRINTLN("[RCC MISSION][MJM] FINAL LAP EVAL")
		IF MC_ServerBD_4.iTeamLapsCompleted[MC_playerBD[iPartToUse].iteam] + 1 = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMissionLapLimit
			sLabel = g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].tlRoot
			PRINTLN("[RCC MISSION] TRIGGER_DIALOGUE LAST LAP HELP- DISPLAYING HELP TEXT:", sLabel)
			PRINT_HELP(sLabel)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset2, iBS_Dialogue2RemoveMaskPrompt)
		PRINTLN("[RCC MISSION][MJM] TRIGGER_DIALOGUE - IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset2, iBS_Dialogue2RemoveMaskPrompt) - QUICK EQUIP MASK - LBS_MASK_QUICK_UNEQUIP_AVAILABLE - SET")
		SET_BIT(iLocalQuickMaskBitSet, LBS_MASK_QUICK_UNEQUIP_AVAILABLE)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset2,iBS_Dialogue2TurnOffVehicleLights)
		NETWORK_INDEX netVeh = MC_serverBD_1.sFMMC_SBD.niVehicle[ g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iVehicleTrigger ]
		IF NETWORK_DOES_NETWORK_ID_EXIST(netVeh)
			VEHICLE_INDEX lightVeh = NET_TO_VEH(netVeh)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(lightVeh)
				IF IS_VEHICLE_OK(lightVeh)
					SET_VEHICLE_LIGHTS(lightVeh,FORCE_VEHICLE_LIGHTS_OFF)
					SET_VEHICLE_INTERIORLIGHT(lightVeh,FALSE)
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset,iBS_DialogueTriggerMinigame)
		IF MC_playerBD[iLocalPart].iHackMGDialogue = -1
			PRINTLN("[RCC MISSION][MJM] TRIGGER_DIALOGUE - IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset,iBS_DialogueTriggerMinigame)")
			
			BROADCAST_HEIST_BEGIN_HACK_MINIGAME(ALL_PLAYERS_ON_SCRIPT(TRUE), iDialogueProgress)
			MC_playerBD[iLocalPart].iHackMGDialogue = iDialogueProgress
		ENDIF
	ENDIF
	
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset2, iBS_Dialogue2ChangeTOD)
		INT iTeam = MC_playerBD[iPartToUse].iteam
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		IF iRule < FMMC_MAX_RULES
			SET_BIT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_ALLOW_TOD_CHANGE)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset2, iBS_Dialogue2CallAirstrike)
		IF NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_DIALOGUE_TRIGGERED)
			SET_BIT(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_DIALOGUE_TRIGGERED)
			vDialogueAirstrikeCoords = g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].vPosition
			PRINTLN("[RCC MISSION][JT][DIALOGUE] Setting LBOOL24_AIRSTRIKE_DIALOGUE_TRIGGERED and vDialogueAirstrikeCoords: ", vDialogueAirstrikeCoords)
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset2, iBS_Dialogue2CallAirstrikeOrbitalCannon)
				PRINTLN("[RCC MISSION][JT][DIALOGUE] Using orbital Cannon instead.")
				SET_BIT(iLocalBoolCheck25, LBOOL25_ORBITAL_CANNON_DIALOGUE_AIRSTRIKE)
			ELSE
				PRINTLN("[RCC MISSION][JT][DIALOGUE] Using regular airstrike.")	
				CLEAR_BIT(iLocalBoolCheck25, LBOOL25_ORBITAL_CANNON_DIALOGUE_AIRSTRIKE)
			ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	
	IF NOT IS_CONVERSATION_STATUS_FREE()
		TEXT_LABEL_23 tlConvLbl = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
		
		IF NOT ARE_STRINGS_EQUAL(tlConvLbl,"NULL")
			INT iDLoop
			INT iDialogue = -1
			FOR iDLoop = 0 TO g_FMMC_STRUCT.iNumberOfDialogueTriggers - 1
				TEXT_LABEL_23 tlDLoop
				IF IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sDialogueTriggers[iDLoop].tlRoot)
					tlDLoop = GET_DIALOGUE_LABEL_FROM_ID(g_FMMC_STRUCT.sDialogueTriggers[iDLoop].iFileID, g_FMMC_STRUCT.sDialogueTriggers[iDLoop].iDialogueID)
				ELSE
					tlDLoop = g_FMMC_STRUCT.sDialogueTriggers[iDLoop].tlRoot
					tlDLoop = PROCESS_APPENDING_DIALOGUE_TRIGGER_SUFFIXES(tlDLoop, iDialogueProgress)
				ENDIF
				IF ARE_STRINGS_EQUAL(tlConvLbl,tlDLoop)
					PRINTLN("[RCC MISSION] TRIGGER_DIALOGUE - dialogue ", iDLoop," is still playing")
					iDialogue = iDLoop
					iDLoop = FMMC_MAX_DIALOGUES_LEGACY
				ENDIF
			ENDFOR
			
			IF iDialogue != -1 //If the dialogue playing is due to the mission creator
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset,iBS_DialogueInterrupts)
				AND NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset,iBS_DialogueInstantInterrupt)
					IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPriority > g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPriority
						//It's okay to overwrite this currently playing dialogue with the new one - continue on to below
						IF NOT IS_BIT_SET(iDialogueKilledBS[iDialogue/32],iDialogue%32)
							KILL_FACE_TO_FACE_CONVERSATION()
							SET_BIT(iDialogueKilledBS[iDialogue/32],iDialogue%32)
						ENDIF
						PRINTLN("[RCC MISSION] TRIGGER_DIALOGUE FALSE - waiting for line of lower priority dialogue to finish")
						RETURN FALSE
					ELSE
						PRINTLN("[RCC MISSION] TRIGGER_DIALOGUE FALSE - current dialogue has priority over/equal to new dialogue")
						RETURN FALSE
					ENDIF
				ELSE
					IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset,iBS_DialogueInstantInterrupt)
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						SET_BIT(iLocalBoolCheck9,LBOOL9_DO_INSTANT_DIALOGUE_LOOP)
						PRINTLN("[RCC MISSION] TRIGGER_DIALOGUE - new dialogue ",iDialogueProgress," interrupting old dialogue IMMEDIATELY")
					ELSE
						KILL_FACE_TO_FACE_CONVERSATION()
						PRINTLN("[RCC MISSION] TRIGGER_DIALOGUE - new dialogue ",iDialogueProgress," interrupting old dialogue")
					ENDIF
				ENDIF
			ELSE
				//special case for Series A Finale Trevor conversation
				IF iCurrentHeistMissionIndex = HBCA_BS_SERIES_A_FINALE
					PRINTLN("[RCC MISSION] TRIGGER_DIALOGUE - current dialogue during HBCA_BS_SERIES_A_FINALE not an FMMC dialogue. Returning FALSE.")
					RETURN FALSE
				ENDIF
				PRINTLN("[RCC MISSION] TRIGGER_DIALOGUE - current dialogue not an FMMC dialogue")
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION] TRIGGER_DIALOGUE - current dialogue has no label")
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iFileID = DIALOGUE_FILE_ID_EMPTY
	OR ARE_STRINGS_EQUAL(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].tlBlock,"EMPTY")
		PRINTLN("[RCC MISSION][MJM] TRIGGER_DIALOGUE fileID is empty, returning without playing dialogue")
		RETURN TRUE
	ENDIF
	
	TEXT_LABEL_23 sBlock = g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].tlBlock
	IF IS_STRING_NULL_OR_EMPTY(sBlock)
	OR IS_STRING_BLANK_SPACES(sBlock, GET_LENGTH_OF_LITERAL_STRING(sBlock))
		sBlock = GET_DIALOGUE_BLOCK_FROM_ID(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iFileID)
	ENDIF
	PRINTLN("[RCC MISSION][MJM] TRIGGER_DIALOGUE - sBlock = ", sBlock, " with length ", GET_LENGTH_OF_LITERAL_STRING(sBlock))
	
	TEXT_LABEL_23 sRoot = g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].tlRoot
	sRoot = PROCESS_APPENDING_DIALOGUE_TRIGGER_SUFFIXES(sRoot, iDialogueProgress)
	
	IF IS_STRING_NULL_OR_EMPTY(sRoot)
	OR IS_STRING_BLANK_SPACES(sRoot, GET_LENGTH_OF_LITERAL_STRING(sRoot))
		sRoot = GET_DIALOGUE_LABEL_FROM_ID(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iFileID, g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueID)
		sRoot = PROCESS_APPENDING_DIALOGUE_TRIGGER_SUFFIXES(sRoot, iDialogueProgress)
	ENDIF
	PRINTLN("[RCC MISSION][MJM] TRIGGER_DIALOGUE - sRoot = ", sRoot, " with length ", GET_LENGTH_OF_LITERAL_STRING(sRoot))
	
	IF NOT IS_BIT_SET(iLocalBoolCheck3, LBOOL3_ADDED_DIALOGUE_PED)
			
		REQUEST_ADDITIONAL_TEXT_FOR_DLC(sBlock, DLC_MISSION_DIALOGUE_TEXT_SLOT)

		PRINTLN("[RCC MISSION] [DLG] TRIGGER_DIALOGUE - REQUEST_ADDITIONAL_TEXT_FOR_DLC(", sBlock, ", DLC_MISSION_DIALOGUE_TEXT_SLOT)")
	
		IF HAS_THIS_ADDITIONAL_TEXT_LOADED(sBlock, DLC_MISSION_DIALOGUE_TEXT_SLOT) 
			PRINTLN("[RCC MISSION] [DLG] TRIGGER_DIALOGUE - HAS_THIS_ADDITIONAL_TEXT_LOADED ", sBlock, " successful")
			
			//TEXT_LABEL_23 ThisSpeakListenLabel = sRoot
   			//ThisSpeakListenLabel += "SL"
			INT iLines = 60//ROUND(TO_FLOAT(GET_LENGTH_OF_LITERAL_STRING(ThisSpeakListenLabel))/3)
			PRINTLN("[RCC MISSION] [DLG] TRIGGER_DIALOGUE - iLines = ", iLines)
			INT iLastSpeaker 				= -1
			INT iSpeakerList[FMMC_MAX_SPEAKERS]
			INT i, iList
			
			FOR iList = 0 TO FMMC_MAX_SPEAKERS-1
				iSpeakerList[iList] = -1
			ENDFOR
			REPEAT iLines i
			
				iCurrentSpeaker = GET_SPEAKER_INT_FROM_ROOT(sRoot, FMMC_MAX_SPEAKERS, i)
				PRINTLN("[RCC MISSION] TRIGGER_DIALOGUE - iCurrentSpeaker = ", iCurrentSpeaker, " Line: ", i, " iLastSpeaker: ", iLastSpeaker)
				IF iCurrentSpeaker = 9999
					i = iLines
					PRINTLN("[RCC MISSION] [DLG] TRIGGER_DIALOGUE - EXIT LOOP - GONE PAST MAX LINES")
				ENDIF
				IF iCurrentSpeaker >= constMaxNum_Conversers
					PRINTLN("[RCC MISSION] [DLG] TRIGGER_DIALOGUE - iCurrentSpeaker >= constMaxNum_Conversers setting to 0 ", iCurrentSpeaker)
					iCurrentSpeaker = 0
				ENDIF
				
				IF iCurrentSpeaker != iLastSpeaker
				AND iCurrentSpeaker > 0
					IF iLastSpeaker = -1
						iPedVoice = g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPedVoice
						
						IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset3, iBS_Dialogue3_UseAlternatingSpeakers)
							IF iSpeakerList[iCurrentSpeaker] = -1
								iSpeakerList[iCurrentSpeaker] = iPedVoice
								PRINTLN("[RCC MISSION] [DLG] TRIGGER_DIALOGUE - Setting iSpeakerList[",iCurrentSpeaker,"] to ", iSpeakerList[iCurrentSpeaker])
							ENDIF
						ENDIF
					ELSE
						IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset3, iBS_Dialogue3_UseAlternatingSpeakers)
							IF iSpeakerList[iLastSpeaker] = g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPedVoice
								iPedVoice = g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iSecondPedVoice
								PRINTLN("[RCC MISSION] [DLG] TRIGGER_DIALOGUE - getting iPedVoice from Second Ped Voice")
							ELSE
								iPedVoice = g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPedVoice
								PRINTLN("[RCC MISSION] [DLG] TRIGGER_DIALOGUE - getting iPedVoice from Primary Ped Voice")
							ENDIF
							IF iSpeakerList[iCurrentSpeaker] = -1
								iSpeakerList[iCurrentSpeaker] = iPedVoice
								PRINTLN("[RCC MISSION] [DLG] TRIGGER_DIALOGUE - Setting iSpeakerList[",iCurrentSpeaker,"] to ", iSpeakerList[iCurrentSpeaker])
							ENDIF
						ELSE
							iPedVoice = g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iSecondPedVoice
						ENDIF
					ENDIF
									
					PRINTLN("[RCC MISSION] TRIGGER_DIALOGUE - g_FMMC_STRUCT.sDialogueTriggers[",iDialogueProgress,"].iPedVoice =  ", g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPedVoice)
					PRINTLN("[RCC MISSION] TRIGGER_DIALOGUE - g_FMMC_STRUCT.sDialogueTriggers[",iDialogueProgress,"].iSecondPedVoice = ", g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iSecondPedVoice)
					PRINTLN("[RCC MISSION] TRIGGER_DIALOGUE - Voice for this line iPedVoice = ", iPedVoice) 
					iLastSpeaker = iCurrentSpeaker
					
					TEXT_LABEL_23 sSpeaker = g_FMMC_STRUCT.sSpeakers[iCurrentSpeaker]
					
					IF IS_STRING_NULL_OR_EMPTY(sSpeaker)
						sSpeaker = GET_PED_VOICE_ID(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iFileID, sRoot)
					ENDIF
					
					PED_INDEX SpeakerPed = NULL
					
					PRINTLN("[RCC MISSION] [DLG] TRIGGER_DIALOGUE - iPedVoice = ", iPedVoice," iDialogueProgress = ",iDialogueProgress, " sSpeaker: ", sSpeaker)
					IF iPedVoice != -1
						IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iPedVoice])
							SpeakerPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPedVoice])
							PRINTLN("[RCC MISSION] [DLG] TRIGGER_DIALOGUE - adding speaker ped since iPedVoice = ", iPedVoice)
							
							IF ARE_STRINGS_EQUAL(sRoot,"NAFIN_IG1")
							OR ARE_STRINGS_EQUAL(sRoot,"NAFIN_IG2")
								PRINTLN("[RCC MISSION] [DLG] TRIGGER_DIALOGUE - Playing Trevor vignette.")
								IF NOT IS_PED_IN_COVER(SpeakerPed)
									PRINTLN("[RCC MISSION] [DLG] TRIGGER_DIALOGUE - returning false as NAFIN_IG2 and Trevor is not in cover")
									RETURN FALSE
								ENDIF
							ENDIF
							
							//Start the ped using their phone if we have control!
							IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset,iBS_DialogueUsePhone)
								PRINTLN("[RCC MISSION] TRIGGER_DIALOGUE - IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset,iBS_DialogueUsePhone) ")
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iPedVoice])
									SET_PED_CONFIG_FLAG(SpeakerPed,PCF_PhoneDisableTextingAnimations,FALSE)
									SET_PED_CONFIG_FLAG(SpeakerPed,PCF_PhoneDisableTalkingAnimations,FALSE)
									
									TASK_PERFORM_AMBIENT_ANIMATION(SpeakerPed,iPedVoice,ciPED_IDLE_ANIM__PHONE)
									PRINTLN("[RCC MISSION] TRIGGER_DIALOGUE - Calling TASK_PERFORM_AMBIENT_ANIMATION w params SpeakerPed,iPedVoice,ciPED_IDLE_ANIM__PHONE")
								ENDIF
								SET_BIT(iLocalBoolCheck6, LBOOL6_DIALOGUE_ANIM_NEEDED)
								//SET_BIT(iLocalBoolCheck9, LBOOL9_STARTING_PHONE_ANIM)
							ELIF g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPedAnimation != ciPED_IDLE_ANIM__NONE
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iPedVoice])
									TASK_PERFORM_AMBIENT_ANIMATION(SpeakerPed, iPedVoice, g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPedAnimation)
								ENDIF
								SET_BIT(iLocalBoolCheck6, LBOOL6_DIALOGUE_ANIM_NEEDED)
							ENDIF
						
							
						ENDIF
					ENDIF
					
					IF iPedVoice =-1
					OR NOT IS_PED_INJURED(SpeakerPed)
						ADD_PED_FOR_DIALOGUE(speechPedStruct, iCurrentSpeaker, SpeakerPed, sSpeaker)
						PRINTLN("[RCC MISSION] [DLG] TRIGGER_DIALOGUE - ADD_PED_FOR_DIALOGUE(speechPedStruct, ", iCurrentSpeaker, ", PED_INDEX, ", sSpeaker, " ) ")
						SET_BIT(iLocalBoolCheck3, LBOOL3_ADDED_DIALOGUE_PED)
						//CLEAR_BIT(iLocalBoolCheck9, LBOOL9_STARTING_PHONE_ANIM)
						PRINTLN("[RCC MISSION] [DLG] TRIGGER_DIALOGUE - sBlock = ", sBlock)
						PRINTLN("[RCC MISSION] [DLG] TRIGGER_DIALOGUE - sRoot = ", sRoot)
						PRINTLN("[RCC MISSION] [DLG] TRIGGER_DIALOGUE - iDialogueProgress = ", iDialogueProgress)
						PRINTLN("[RCC MISSION] [DLG] TRIGGER_DIALOGUE - iFileID = ", g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iFileID)
						PRINTLN("[RCC MISSION] [DLG] TRIGGER_DIALOGUE - iDialogueID = ", g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueID)
						PRINTLN("[RCC MISSION] [DLG] TRIGGER_DIALOGUE - tlBlock = ", g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].tlBlock)
						PRINTLN("[RCC MISSION] [DLG] TRIGGER_DIALOGUE - tlRoot = ", g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].tlRoot)
						PRINTLN("[RCC MISSION] [DLG] TRIGGER_DIALOGUE - GET--_SPEAKER_INT_FROM_ROOT = ", iCurrentSpeaker)
						PRINTLN("[RCC MISSION] [DLG] TRIGGER_DIALOGUE - GET_PED_VOICE_ID = ", sSpeaker)
					ENDIF
				ENDIF							
			ENDREPEAT
		ENDIF
	ELSE
		PRINTLN("[RCC MISSION] [DLG] TRIGGER_DIALOGUE - iCurrentSpeaker = ",  iCurrentSpeaker)
		PRINTLN("[RCC MISSION] [DLG] TRIGGER_DIALOGUE - GET_SPEAKER_INT_FROM_ROOT(sRoot, FMMC_MAX_SPEAKERS) = ",  GET_SPEAKER_INT_FROM_ROOT(sRoot, FMMC_MAX_SPEAKERS))
		IF iCurrentSpeaker != GET_SPEAKER_INT_FROM_ROOT(sRoot, FMMC_MAX_SPEAKERS)
			PRINTLN("[RCC MISSION] [DLG] TRIGGER_DIALOGUE - Cleared  LBOOL3_ADDED_DIALOGUE_PED")
			CLEAR_BIT(iLocalBoolCheck3, LBOOL3_ADDED_DIALOGUE_PED)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck3, LBOOL3_ADDED_DIALOGUE_PED)
		
		enumConversationPriority ConvPriority
		
		SWITCH g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPriority
		CASE 0
			ConvPriority = CONV_PRIORITY_VERY_LOW
			BREAK
		CASE 1
			ConvPriority = CONV_PRIORITY_LOW
			BREAK
		CASE 2
			ConvPriority = CONV_PRIORITY_MEDIUM
			BREAK
		ENDSWITCH
		
		IF CREATE_DIALOGUE(iDialogueProgress, speechPedStruct, sBlock, sRoot, ConvPriority)
			PRINTLN("[RCC MISSION] TRIGGER_DIALOGUE TRUE - created new dialogue ", iDialogueProgress)
			
			/*IF (ARE_STRINGS_EQUAL(sRoot,"MPBSF_TUNNEL"))
				PRINT_HELP("HEIST_HELP_16") // rebreather help*/
			/*IF (ARE_STRINGS_EQUAL(sRoot,"MPBSF_NIGHT")) 
				PRINT_HELP("HEIST_HELP_9") //night vision help*/
			IF (ARE_STRINGS_EQUAL(sRoot,"PBST_TWOT")) OR (ARE_STRINGS_EQUAL(sRoot,"PBU_LEAVE"))
				PRINT_HELP("HEIST_HELP_20") //team blip colour help
			ELIF (ARE_STRINGS_EQUAL(sRoot,"MPBSF_AJUMP"))
				PRINT_HELP("PARACHUTE_HELP") //parachute help
			ELIF (ARE_STRINGS_EQUAL(sRoot,"MPBSE_CARE2"))
			OR   (ARE_STRINGS_EQUAL(sRoot,"MPBSE_JETS"))
				PRINT_HELP("FM_VULK_HLP2") //flight mode help
			ELIF (ARE_STRINGS_EQUAL(sRoot,"MPBSE_AIR"))
				PRINTLN("[RCC MISSION][MJM] TRIGGER_DIALOGUE - (ARE_STRINGS_EQUAL(sRoot,MPBSE_AIR))")
				IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_HEISTS_BIOLAB_STEAL_EMP_FLYOVER_SCENE")
					PRINTLN("[RCC MISSION][MJM] TRIGGER_DIALOGUE - START_AUDIO_SCENE(DLC_HEISTS_BIOLAB_STEAL_EMP_FLYOVER_SCENE")
					START_AUDIO_SCENE("DLC_HEISTS_BIOLAB_STEAL_EMP_FLYOVER_SCENE")	//play jet noise
				ENDIF
			ELIF (ARE_STRINGS_EQUAL(sRoot,"MPBSE_ALL"))
				PRINTLN("[RCC MISSION][MJM] TRIGGER_DIALOGUE - (ARE_STRINGS_EQUAL(sRoot,MPBSE_ALL))")
				IF NOT IS_BIT_SET(iLocalBoolCheck9, LBOOL9_DANGER_ZONE_TRIGGERED)
				AND NOT IS_BIT_SET(iLocalBoolCheck9, LBOOL9_PREPARE_DANGER_ZONE)
				
					PRINTLN("[RCC MISSION][MJM] TRIGGER_DIALOGUE - SET_AUDIO_FLAG(AllowForceRadioAfterRetune, TRUE)")
					SET_AUDIO_FLAG("AllowForceRadioAfterRetune", TRUE)
				
					PRINTLN("[RCC MISSION][MJM] TRIGGER_DIALOGUE - SET_BIT(iLocalBoolCheck9, LBOOL9_PREPARE_DANGER_ZONE)")
					SET_BIT(iLocalBoolCheck9, LBOOL9_PREPARE_DANGER_ZONE)
					
				ENDIF
			ENDIF
			
			//If we have a target ped to look at
			IF ARE_STRINGS_EQUAL(sRoot,"TUSCO_CHAT")
				INT iSpeechPed
				INT iSpeaker1 = -1
				INT iSpeaker2 = -1
				REPEAT constMaxNum_Conversers iSpeechPed
					IF speechPedStruct.PedInfo[iSpeechPed].ActiveInConversation
						IF iSpeaker1 = -1
							iSpeaker1 = iSpeechPed
						ELIF iSpeaker2 = -1
							iSpeaker2 = iSpeechPed
						ELSE
							PRINTLN("[RCC MISSION][MJM] TRIGGER_DIALOGUE - More than two peds trying to look at each other, chaos imminent")
						ENDIF
					ENDIF
				ENDREPEAT
				
				IF iSpeaker1 > -1 AND iSpeaker2 > -1 AND iSpeaker1 != iSpeaker2
					PRINTLN("[RCC MISSION][MJM] TRIGGER_DIALOGUE - iSpeaker1 > -1 AND iSpeaker2 > -1")
					PED_INDEX lookAtPed1, lookAtPed2
					lookAtPed1 = speechPedStruct.PedInfo[iSpeaker1].Index
					lookAtPed2 = speechPedStruct.PedInfo[iSpeaker2].Index
					IF NOT IS_PED_INJURED(lookAtPed1) AND NOT IS_PED_INJURED(lookAtPed2)
						
						INT iPed1 = IS_ENTITY_A_MISSION_CREATOR_ENTITY(lookAtPed1)
						INT iPed2 = IS_ENTITY_A_MISSION_CREATOR_ENTITY(lookAtPed2)
						
						TASK_LOOK_AT_ENTITY(lookAtPed1,lookAtPed2,-1,SLF_WHILE_NOT_IN_FOV,SLF_LOOKAT_VERY_HIGH)
						TASK_LOOK_AT_ENTITY(lookAtPed2,lookAtPed1,-1,SLF_WHILE_NOT_IN_FOV,SLF_LOOKAT_VERY_HIGH)
						SET_BIT(iPedLookAtBS[GET_LONG_BITSET_INDEX(iPed1)], GET_LONG_BITSET_BIT(iPed1))
						SET_BIT(iPedLookAtBS[GET_LONG_BITSET_INDEX(iPed2)], GET_LONG_BITSET_BIT(iPed2))
						PRINTLN("[RCC MISSION][MJM] TRIGGER_DIALOGUE - Peds ",iPed1," and ",iPed2," looking at each other")
					ENDIF
				ENDIF
			ENDIF
			
			PRINTLN("[RCC MISSION] [DLG] TRIGGER_DIALOGUE - Cleared LBOOL3_ADDED_DIALOGUE_PED")
			CLEAR_BIT(iLocalBoolCheck3, LBOOL3_ADDED_DIALOGUE_PED)
			
			IF NOT MC_playerBD[iLocalPart].bPlayedFirstDialogue
				IF g_FMMC_STRUCT.iTripSkipHelpDialogue = -1
				OR g_FMMC_STRUCT.iTripSkipHelpDialogue = iDialogueProgress
					MC_playerBD[iLocalPart].bPlayedFirstDialogue = TRUE
					PRINTLN("[RCC MISSION][MJM] TRIGGER_DIALOGUE - MC_playerBD[iLocalPart].bPlayedFirstDialogue = TRUE")
				ENDIF
			ENDIF
			
			//If clearing help text check for specific help text being set in tlDialogueTriggerAdditionalHelpText, if not then clear all existing.
			IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset5, iBS_Dialogue5_ClearExistingHelpText)
			AND IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].tlDialogueTriggerAdditionalHelpText)
			AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
					CLEAR_HELP()
					PRINTLN("[RCC MISSION] TRIGGER_DIALOGUE - iBS_Dialogue5_ClearExistingHelpText is set and help message was being displayed - clearing existing help text.")
				ENDIF
			ENDIF
			
			IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].tlDialogueTriggerAdditionalHelpText)
			AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
				sLabel = g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].tlDialogueTriggerAdditionalHelpText
				
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset5, iBS_Dialogue5_ClearExistingHelpText)
					IF bLocalPlayerOK
						PRINTLN("[RCC MISSION] TRIGGER_DIALOGUE - DISPLAYING HELP TEXT:", sLabel)
						sLabelObjHelpReprint = sLabel
						SET_BIT(iLocalBoolCheck18, LBOOL18_CHECK_FOR_DEATH_OBJ_TEXT_REPRINT)
						PRINTLN("[LM][OBJ_TEXT_REPRINT][ALSO SHOW HELPTEXT] TRIGGER_DIALOGUE - Start death check timers and cache the help text for use later.(1)")
						PRINT_HELP(sLabel)
					ELSE					
						sLabelObjHelpReprint = sLabel
						SET_BIT(iLocalBoolCheck18, LBOOL18_CHECK_FOR_DEATH_OBJ_TEXT_REPRINT)
						PRINTLN("[LM][OBJ_TEXT_REPRINT][ALSO SHOW HELPTEXT] TRIGGER_DIALOGUE - Start death check timers and cache the help text for use later.(2)")					
					ENDIF
				ELSE //If this specific message is currently displayed then clear it.
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sLabel)
						CLEAR_HELP()
						PRINTLN("[RCC MISSION] TRIGGER_DIALOGUE - iBS_Dialogue5_ClearExistingHelpText is set and specific help message was being displayed - clearing existing help text.")
					ENDIF
				ENDIF
			ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL CHECK_DIALOGUE_TIMER()
	PRINTLN("[DIALOGUE TRIGGER] CHECK_DIALOGUE_TIMER TRIGGER_DIALOGUE iDialogueProgress: ", iDialogueProgress, " Time Elapsed: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdDialogueTimer[iDialogueProgress]), " Time Delay: ", (g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iTimeDelay*1000))
	// Fix for bug 1849042. We don;t want this dialogue triggering while in the apartment. 
	IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
	OR Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
		IF IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_HEIST_ANY_PARTICIPANT_INSIDE_ANY_MP_PROPERTY)
		AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet2,SBBOOL2_HEIST_ANY_PARTICIPANT_OUTSIDE_ANY_MP_PROPERTY)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iTimeDelay != 0
		IF NOT HAS_NET_TIMER_STARTED(tdDialogueTimer[iDialogueProgress])
			REINIT_NET_TIMER(tdDialogueTimer[iDialogueProgress])
			SET_BIT(iLocalDialoguePlayingBS[iDialogueProgress/32],iDialogueProgress%32)
		ELSE
			IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdDialogueTimer[iDialogueProgress]) > (g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iTimeDelay*1000)
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	RETURN FALSE
	
ENDFUNC

FUNC BOOL HANDLE_BEAMHACK_DIALOGUE(INT iDialogue)
	IF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_STARTED_BEAM_HACK)
		IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_PacketHit)
		AND iBeamHack_PrevPacketBS != sBeamHackGameplayData.iPacketBS
			iBeamHack_PrevPacketBS = sBeamHackGameplayData.iPacketBS
			PRINTLN("HANDLE_BEAMHACK_DIALOGUE - Hit a packet")
			RETURN TRUE
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_FirewallHit)
		AND iBeamHack_PrevLivesRemaining != sBeamHackGameplayData.iLives
			iBeamHack_PrevLivesRemaining = sBeamHackGameplayData.iLives
			PRINTLN("HANDLE_BEAMHACK_DIALOGUE - Lost a life")
			RETURN TRUE
		ENDIF
	ELSE
		iBeamHack_PrevPacketBS = 0
		iBeamHack_PrevLivesRemaining = ciBH_MAX_LIVES
		RETURN FALSE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HANDLE_FINGERPRINT_CLONE_DIALOGUE(INT iDialogue)

	IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_STARTED_FINGERPRINT_CLONE)
		IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_FingerprintClonePatternDone)
		AND iFingerprintClone_PrevPatternDone != sFingerprintCloneGameplay.iCurrentFingerprintIndex
			iFingerprintClone_PrevPatternDone = sFingerprintCloneGameplay.iCurrentFingerprintIndex
			PRINTLN("HANDLE_FINGERPRINT_CLONE_DIALOGUE - Passed a pattern.")
			RETURN TRUE
		ENDIF
			
		IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_FingerprintClonePatternFail)
		AND iFingerprintClone_PrevLivesLost != (sFingerprintCloneGameplay.sBaseStruct.iMaxLives - sFingerprintCloneGameplay.sBaseStruct.iCurrentLives)
			iFingerprintClone_PrevLivesLost = (sFingerprintCloneGameplay.sBaseStruct.iMaxLives - sFingerprintCloneGameplay.sBaseStruct.iCurrentLives)
			PRINTLN("HANDLE_FINGERPRINT_CLONE_DIALOGUE - Failed a pattern")
			RETURN TRUE
		ENDIF
	ELSE
		iFingerprintClone_PrevPatternDone = 0
		iFingerprintClone_PrevLivesLost = 0
		RETURN FALSE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HANDLE_ORDER_UNLOCK_DIALOGUE(INT iDialogue)

	IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_STARTED_ORDER_UNLOCK)
		IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_OrderUnlockPatternDone)
		AND iOrderUnlock_PrevPatternDone != sOrderUnlockGameplay.iCurrentPatternIndex
			iOrderUnlock_PrevPatternDone = sOrderUnlockGameplay.iCurrentPatternIndex
			PRINTLN("HANDLE_ORDER_UNLOCK_DIALOGUE - Passed a pattern.")
			RETURN TRUE
		ENDIF
			
		IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_OrderUnlockPatternFail)
		AND iOrderUnlock_PrevLivesLost != (sOrderUnlockGameplay.sBaseStruct.iMaxLives - sOrderUnlockGameplay.sBaseStruct.iCurrentLives)
			iOrderUnlock_PrevLivesLost = (sOrderUnlockGameplay.sBaseStruct.iMaxLives - sOrderUnlockGameplay.sBaseStruct.iCurrentLives)
			PRINTLN("HANDLE_ORDER_UNLOCK_DIALOGUE - Failed a pattern")
			RETURN TRUE
		ENDIF
	ELSE
		iOrderUnlock_PrevPatternDone = 0
		iOrderUnlock_PrevLivesLost = 0
		RETURN FALSE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HANDLE_CARGOBOB_VEHICLE_DROPOFF_DIALOGUE(INT iDialogue)

	PED_INDEX tempped = PlayerPedToUse
	BOOL bReturn = FALSE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_MustBeCarryingObjVehicle)			
		IF iObjectiveVehicleAttachedToCargobobBS > 0	
			IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].fRadius != 0.0
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(tempped, g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].vPosition) <= g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].fRadius
					PRINTLN("[ML][HANDLE_CARGOBOB_VEHICLE_DROPOFF_DIALOGUE] Within trigger radius. Distance is ", GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(tempped, g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].vPosition))
					IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_CARGOBOB_CARRYING_OBJ_VEHICLE_TRIGGERED)
						SET_BIT(iLocalBoolCheck31, LBOOL31_CARGOBOB_CARRYING_OBJ_VEHICLE_TRIGGERED)
						PRINTLN("[ML][HANDLE_CARGOBOB_VEHICLE_DROPOFF_DIALOGUE] Set LBOOL31_CARGOBOB_CARRYING_OBJ_VEHICLE_TRIGGERED")
						RETURN TRUE
					ENDIF
				ELSE
					PRINTLN("[ML][HANDLE_CARGOBOB_VEHICLE_DROPOFF_DIALOGUE] Not within trigger radius")
					IF IS_BIT_SET(iLocalBoolCheck31, LBOOL31_CARGOBOB_CARRYING_OBJ_VEHICLE_TRIGGERED)
						CLEAR_BIT(iLocalBoolCheck31, LBOOL31_CARGOBOB_CARRYING_OBJ_VEHICLE_TRIGGERED)
						PRINTLN("[ML][HANDLE_CARGOBOB_VEHICLE_DROPOFF_DIALOGUE] Reset LBOOL31_CARGOBOB_CARRYING_OBJ_VEHICLE_TRIGGERED")
					ENDIF		
				ENDIF
			ENDIF
		ELSE	// Not carrying the object
			PRINTLN("[ML][HANDLE_CARGOBOB_VEHICLE_DROPOFF_DIALOGUE] Not carrying a vehicle")
			IF IS_BIT_SET(iLocalBoolCheck31, LBOOL31_CARGOBOB_CARRYING_OBJ_VEHICLE_TRIGGERED)
				CLEAR_BIT(iLocalBoolCheck31, LBOOL31_CARGOBOB_CARRYING_OBJ_VEHICLE_TRIGGERED)
				PRINTLN("[ML][HANDLE_CARGOBOB_VEHICLE_DROPOFF_DIALOGUE] Reset LBOOL31_CARGOBOB_CARRYING_OBJ_VEHICLE_TRIGGERED")
			ENDIF	
		ENDIF
				
	ENDIF
	RETURN bReturn
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_BE_REPLAYABLE(INT iDialogue)
	
	IF HANDLE_BEAMHACK_DIALOGUE(iDialogue)
		PRINTLN("SHOULD_DIALOGUE_BE_REPLAYABLE returning TRUE due to HANDLE_BEAMHACK_DIALOGUE")
		RETURN TRUE
	ENDIF
	
	IF HANDLE_FINGERPRINT_CLONE_DIALOGUE(iDialogue)
		PRINTLN("SHOULD_DIALOGUE_BE_REPLAYABLE returning TRUE due to HANDLE_FINGERPRINT_CLONE_DIALOGUE")
		RETURN TRUE
	ENDIF
	
	IF HANDLE_ORDER_UNLOCK_DIALOGUE(iDialogue)
		PRINTLN("SHOULD_DIALOGUE_BE_REPLAYABLE returning TRUE due to HANDLE_ORDER_UNLOCK_DIALOGUE")
		RETURN TRUE
	ENDIF
	
	IF HANDLE_CARGOBOB_VEHICLE_DROPOFF_DIALOGUE(iDialogue)
		PRINTLN("SHOULD_DIALOGUE_BE_REPLAYABLE returning TRUE due to HANDLE_CARGOBOB_VEHICLE_DROPOFF_DIALOGUE")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_DIALOGUE_TRIGGERS_PLAYING_EVERY_FRAME()
	
	// Clear Some Data that is set by every frame processing below.
	CLEAR_BIT(iLocalBoolCheck32, LBOOL32_BLOCKING_OBJECTIVE_FROM_DIALOGUE_TRIGGER)
			
	// Optimization. No need to run this loop if no options are set up to require it.
	IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_NO_NEED_FOR_DIALOGUE_EVERY_FRAME_PROCESSING)
		EXIT
	ENDIF
	
	// Process Creator Options.
	INT iLoopBSRequired
	INT iTrigger
	FOR iTrigger = 0 TO g_FMMC_STRUCT.iNumberOfDialogueTriggers - 1
		IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iTrigger].iDialogueBitset3, iBS_Dialogue3_BlockObjectiveWhilePlayingDialogue)
			SET_BIT(iLoopBSRequired, 0)
			IF IS_BIT_SET(iLocalDialoguePlayingBS[iTrigger/32],iTrigger%32)
				IF NOT IS_CONVERSATION_STATUS_FREE()
				OR IS_SCRIPTED_CONVERSATION_ONGOING()
					SET_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
					SET_BIT(iLocalBoolCheck32, LBOOL32_BLOCKING_OBJECTIVE_FROM_DIALOGUE_TRIGGER)
					Clear_Any_Objective_Text()
					PRINTLN("[RCC MISSION][DIALOGUE] - iDialogue: ", iTrigger, " has option set to block objective while playing - Dialogue ongoing...")
				ELSE
					CLEAR_BIT(iLocalDialoguePlayingBS[iTrigger/32],iTrigger%32)
					PRINTLN("[RCC MISSION][DIALOGUE] - iDialogue: ", iTrigger, " has option set to block objective while playing - Dialogue Finished. Clearing 'Playing' bit.")
					CLEAR_BIT(iLoopBSRequired, 0)
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	// Shut down the loop.
	IF iLoopBSRequired != 0
		SET_BIT(iLocalBoolCheck32, LBOOL32_NO_NEED_FOR_DIALOGUE_EVERY_FRAME_PROCESSING)
	ENDIF
ENDPROC

PROC SET_DIALOGUE_AS_PLAYED_FOR_CONTINUITY(INT iDialogue)

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_DialogueTracking)
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iContinuityId = -1
		EXIT
	ENDIF
	
	IF SHOULD_DIALOGUE_BE_REPLAYABLE(iDialogue)
		EXIT
	ENDIF
	
	SET_LONG_BIT(sLEGACYMissionLocalContinuityVars.iDialogueTrackingBitset, g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iContinuityId)
	PRINTLN("[CONTINUITY] - SET_DIALOGUE_AS_PLAYED_FOR_CONTINUITY - Setting dialogue trigger ", iDialogue, " with continuity ID ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iContinuityId, " as played")
	
ENDPROC

PROC MANAGE_DIALOGUE()
	IF CHECK_DIALOGUE_TIMER()
		IF NOT IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset,iBS_DialogueNotHeardByTeam0 + MC_playerBD[iPartToUse].iteam)							
			IF TRIGGER_DIALOGUE()
				iDialogueProgressLastPlayed = iDialogueProgress
				RESET_NET_TIMER(tdDialogueTimer[iDialogueProgress])
				SET_BIT(MC_playerBD[iPartToUse].iDialoguePlayedBS[iDialogueProgress/32],iDialogueProgress%32)
				SET_BIT(iLocalDialoguePlayingBS[iDialogueProgress/32],iDialogueProgress%32)
				PRINTLN("[RCC MISSION][DIALOGUE] - Setting MC_playerBD[iPartToUse].iDialoguePlayedBS[",iDialogueProgress/32,"],",iDialogueProgress%32, " Call 1")
				SET_BIT(iLocalDialoguePlayedBS[iDialogueProgress/32],iDialogueProgress%32)				
				SET_DIALOGUE_AS_PLAYED_FOR_CONTINUITY(iDialogueProgress)
				
				IF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_EXPEDITE_DIALOGUE_TRIGGERS_ENABLED)
					IF IS_BIT_SET(iLocalDialoguePending[iDialogueProgress/32],iDialogueProgress%32)
						PRINTLN("[RCC MISSION] TRIGGER_DIALOGUE Dialogue iLocalDialoguePending was set for iDialogueProgress: ", iDialogueProgress, " It has now played, we can clear it.")
						CLEAR_BIT(iLocalDialoguePending[iDialogueProgress/32],iDialogueProgress%32)
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset4, iBS_Dialogue4_BroadcastToAllPlayers)
				AND MC_serverBD_4.iPlayThisAudioTriggerASAP_SERVER != iDialogueProgress
					PRINTLN("[RCC MISSION] TRIGGER_DIALOGUE Broadcasting to other players.")
					PLAY_DIALOGUE_TRIGGER_ASAP(iDialogueProgress)
				ENDIF
			ELSE
				IF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_EXPEDITE_DIALOGUE_TRIGGERS_ENABLED)
					IF NOT IS_BIT_SET(iLocalDialoguePending[iDialogueProgress/32],iDialogueProgress%32)
						PRINTLN("[RCC MISSION] TRIGGER_DIALOGUE Dialogue iLocalDialoguePending set for iDialogueProgress: ", iDialogueProgress)
						SET_BIT(iLocalDialoguePending[iDialogueProgress/32],iDialogueProgress%32)
					ENDIF
				ENDIF
			ENDIF
		ELSE			
			SET_BIT(MC_playerBD[iPartToUse].iDialoguePlayedBS[iDialogueProgress/32],iDialogueProgress%32)
			PRINTLN("[RCC MISSION][DIALOGUE] - Setting MC_playerBD[iPartToUse].iDialoguePlayedBS[",iDialogueProgress/32,"],",iDialogueProgress%32, " Call 2")
			SET_BIT(iLocalDialoguePlayingBS[iDialogueProgress/32],iDialogueProgress%32)
			SET_BIT(iLocalDialoguePlayedBS[iDialogueProgress/32],iDialogueProgress%32)
			SET_DIALOGUE_AS_PLAYED_FOR_CONTINUITY(iDialogueProgress)
			//This dialogue trigger does not play for this team, skip playing it!
			PRINTLN("[RCC MISSION] TRIGGER_DIALOGUE Dialogue checks were processed, but dialogue doesn't play for this team or not loaded for this team - skipping to next dialogue")
		ENDIF
	ENDIF
ENDPROC

//Returns true if this dialogue doesn't care about wanted, or the right wanted change has occurred
FUNC BOOL HAS_A_WANTED_CHANGE_OCCURRED_THAT_TRIGGERS_THIS_DIALOGUE(INT iDialogue)
	
	BOOL bShouldTrigger
	
	IF (NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset,iBS_DialogueTriggerOnWantedGain) )
	AND (NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset,iBS_DialogueTriggerOnWantedLoss) )
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2, iBS_Dialogue2SeenByCops)
			IF HAS_NET_TIMER_STARTED(MC_serverBD.tdCopSpottedFailTimer)
				bShouldTrigger = TRUE
			ENDIF
		ELSE
			bShouldTrigger = TRUE
		ENDIF
		
	ELSE
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset,iBS_DialogueWantedOnlyForCarrier)
			PRINTLN("[RCC MISSION][MJM] HAS_A_WANTED_CHANGE_OCCURRED_THAT_TRIGGERS_THIS_DIALOGUE - NOT iBS_DialogueWantedOnlyForCarrier")
			IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset,iBS_DialogueTriggerOnWantedGain)
				PRINTLN("[RCC MISSION][MJM] HAS_A_WANTED_CHANGE_OCCURRED_THAT_TRIGGERS_THIS_DIALOGUE - iBS_DialogueTriggerOnWantedGain")
				
				IF NOT IS_BIT_SET(iLocalBoolCheck29, LBOOL29_SET_BLIP_IN_BLOCK_WANTED_CONE_RESPONSE_VEHICLE) //Don't trigger if in an active swap vehicle - url:bugstar:6131153
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_DontRetainWantedStatus)
						IF IS_BIT_SET(iLocalBoolCheck4,LBOOL4_T0GOTWANTED + g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTeam)
							bShouldTrigger = TRUE					
							PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_A_WANTED_CHANGE_OCCURRED_THAT_TRIGGERS_THIS_DIALOGUE - Trigger conditions met. GAINED WANTED TEAM dteam: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTeam)
						ENDIF
					ELSE
						IF GET_PLAYER_WANTED_LEVEL(LocalPlayer) > 0
							bShouldTrigger = TRUE
							PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_A_WANTED_CHANGE_OCCURRED_THAT_TRIGGERS_THIS_DIALOGUE - Triggering due to iBS_DialogueWantedOnlyForCarrier")
						ENDIF
					ENDIF
				ENDIF
			ELSE //iBS_DialogueTriggerOnWantedLoss is set
				PRINTLN("[RCC MISSION][MJM] HAS_A_WANTED_CHANGE_OCCURRED_THAT_TRIGGERS_THIS_DIALOGUE - iBS_DialogueTriggerOnWantedLoss")
				IF IS_BIT_SET(iLocalBoolCheck4,LBOOL4_T0LOSTWANTED + g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTeam)
					bShouldTrigger = TRUE					
					PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_A_WANTED_CHANGE_OCCURRED_THAT_TRIGGERS_THIS_DIALOGUE - Trigger conditions met. LOST WANTED TEAM dteam: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTeam)
				ENDIF
			ENDIF
		ELSE //we don't care about the whole team, just the carrier
			PRINTLN("[RCC MISSION][MJM] HAS_A_WANTED_CHANGE_OCCURRED_THAT_TRIGGERS_THIS_DIALOGUE - iBS_DialogueWantedOnlyForCarrier")
			IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset,iBS_DialogueTriggerOnWantedGain)
				PRINTLN("[RCC MISSION][MJM] HAS_A_WANTED_CHANGE_OCCURRED_THAT_TRIGGERS_THIS_DIALOGUE - iBS_DialogueTriggerOnWantedGain")
				
				IF NOT IS_BIT_SET(iLocalBoolCheck29, LBOOL29_SET_BLIP_IN_BLOCK_WANTED_CONE_RESPONSE_VEHICLE) //Don't trigger if in an active swap vehicle - url:bugstar:6131153
					IF IS_BIT_SET(iTeamCarrierWantedBS, ciTCWBS_T0GOT + g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTeam)
						bShouldTrigger = TRUE
						PRINTLN("[RCC MISSION][MJM] HAS_A_WANTED_CHANGE_OCCURRED_THAT_TRIGGERS_THIS_DIALOGUE - GAINED WANTED CARRIER ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTeam ," - bShouldTrigger = TRUE")
						PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_A_WANTED_CHANGE_OCCURRED_THAT_TRIGGERS_THIS_DIALOGUE - Trigger conditions met. GAINED WANTED CARRIER dteam: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTeam)
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION][MJM] HAS_A_WANTED_CHANGE_OCCURRED_THAT_TRIGGERS_THIS_DIALOGUE - iBS_DialogueTriggerOnWantedLoss")
				IF IS_BIT_SET(iTeamCarrierWantedBS, ciTCWBS_T0LOST + g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTeam)
					bShouldTrigger = TRUE					
					PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_A_WANTED_CHANGE_OCCURRED_THAT_TRIGGERS_THIS_DIALOGUE - Trigger conditions met. LOST WANTED CARRIER dteam: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTeam)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
	
ENDFUNC

FUNC BOOL HAS_PED_LEFT_OR_ENTERED_VEHICLE(INT iDialogue)

	BOOL bShouldTrigger
	
	IF (NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset,iBS_DialoguePedInsideVehicle) )
	AND (NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset,iBS_DialoguePedOutsideVehicle) )
		bShouldTrigger = TRUE
	ELSE
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPedVoice != -1 
			PRINTLN("[RCC MISSION][MJM] HAS_PED_LEFT_OR_ENTERED_VEHICLE() - iPedVoice != -1")
			NETWORK_INDEX netPed = MC_serverBD_1.sFMMC_SBD.niPed[ g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPedVoice ]
			IF NETWORK_DOES_NETWORK_ID_EXIST(netPed)
				PRINTLN("[RCC MISSION][MJM] HAS_PED_LEFT_OR_ENTERED_VEHICLE() - NETWORK_DOES_NETWORK_ID_EXIST(netPed)")
				IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset,iBS_DialoguePedInsideVehicle)
					PRINTLN("[RCC MISSION][MJM] HAS_PED_LEFT_OR_ENTERED_VEHICLE() - CHECK INSIDE")
					IF IS_PED_IN_ANY_VEHICLE(NET_TO_PED(netPed))
						bShouldTrigger = TRUE
						PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_PED_LEFT_OR_ENTERED_VEHICLE - Trigger conditions met. IS_PED_IN_ANY_VEHICLE")
					ENDIF
				ELIF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset,iBS_DialoguePedOutsideVehicle)
					PRINTLN("[RCC MISSION][MJM] HAS_PED_LEFT_OR_ENTERED_VEHICLE() - CHECK OUTSIDE")
					IF NOT IS_PED_IN_ANY_VEHICLE(NET_TO_PED(netPed))
						bShouldTrigger = TRUE
						PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_PED_LEFT_OR_ENTERED_VEHICLE - Trigger conditions met. NOT IS_PED_IN_ANY_VEHICLE")
					ENDIF
				ENDIF
			ENDIF
		ELSE
			bShouldTrigger = TRUE
			PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_PED_LEFT_OR_ENTERED_VEHICLE - Trigger conditions met. iPedVoice = -1")
		ENDIF
	ENDIF

	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL HAS_PLAYER_LEFT_OR_ENTERED_VEHICLE(INT iDialogue)

	BOOL bShouldTrigger
	
	IF (NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset,iBS_DialoguePlayerInsideVehicle) )
	AND (NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset,iBS_DialoguePlayerOutsideVehicle) )
		bShouldTrigger = TRUE
	ELSE
		IF bPlayerToUseOK
			PED_INDEX tempPed = PlayerPedToUse
			IF NOT IS_PED_INJURED(tempPed)
				IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset,iBS_DialoguePlayerInsideVehicle)
					BOOL bPlayerInVeh = IS_PED_IN_ANY_VEHICLE(tempPed, TRUE)
					IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2, iBS_Dialogue2RearSeats)
						VEHICLE_SEAT vs_Seat = GET_SEAT_PED_IS_IN(tempPed,TRUE)
						
						// Fixed Assert
						BOOL bInChernobog
						IF bPlayerInVeh
							// Fixes another assert...
							IF IS_PED_IN_ANY_VEHICLE(tempPed)
								IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(tempPed)) = CHERNOBOG
									bInChernobog = TRUE
								ENDIF
							ELIF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_ENTERING(tempPed)) = CHERNOBOG
								bInChernobog = TRUE
							ENDIF							
						ENDIF

						IF (bPlayerInVeh 
							AND (vs_Seat = VS_BACK_LEFT 
							OR vs_Seat = VS_BACK_RIGHT 
							OR vs_Seat = VS_EXTRA_LEFT_1
							OR (bInChernobog AND vs_Seat = VS_FRONT_RIGHT)))
						OR (NOT bPlayerInVeh
							AND IS_PLAYER_IN_CREATOR_AIRCRAFT(localPlayer))	
							bShouldTrigger = TRUE							
							PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_PLAYER_LEFT_OR_ENTERED_VEHICLE - Trigger conditions met. IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset2, iBS_Dialogue2RearSeats)")
						ENDIF
					ELIF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2, iBS_Dialogue2DriversSeat)
						IF bPlayerInVeh
						AND GET_SEAT_PED_IS_IN(PLAYER_PED_ID(),TRUE) = VS_DRIVER 
							bShouldTrigger = TRUE
							PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_PLAYER_LEFT_OR_ENTERED_VEHICLE - Trigger conditions met. IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset2, iBS_Dialogue2DriversSeat)")
						ENDIF
					ELSE
						PRINTLN("[RCC MISSION][MJM] HAS_PLAYER_LEFT_OR_ENTERED_VEHICLE() - CHECK INSIDE")
						IF IS_PED_IN_ANY_VEHICLE(tempPed)
							bShouldTrigger = TRUE							
							PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_PLAYER_LEFT_OR_ENTERED_VEHICLE - Trigger conditions met. IS_PED_IN_ANY_VEHICLE()")
						ENDIF
					ENDIF
				ELIF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset,iBS_DialoguePlayerOutsideVehicle)
					PRINTLN("[RCC MISSION][MJM] HAS_PLAYER_LEFT_OR_ENTERED_VEHICLE() - CHECK OUTSIDE")
					IF NOT IS_PED_IN_ANY_VEHICLE(tempPed)
						bShouldTrigger = TRUE
						PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_PLAYER_LEFT_OR_ENTERED_VEHICLE - Trigger conditions met. HAS_PLAYER_LEFT_OR_ENTERED_VEHICLE() - NOT IS_PED_IN_ANY_VEHICLE")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL IS_PLAYER_NEAR_STARTING_APARTMENT(INT iDialogue)
	BOOL bShouldTrigger
	VECTOR vLoc
	
	IF (NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset,iBS_DialogueTriggerNearApartment) )
		bShouldTrigger = TRUE
	ELSE
		INT iPropertyIndex = -1
		
		IF IS_FAKE_MULTIPLAYER_MODE_SET()
			iPropertyIndex = 1
		ELSE
			iPropertyIndex = MC_ServerBD.iHeistPresistantPropertyID
		ENDIF
		
		IF iPropertyIndex >= 0
		AND iPropertyIndex < ( MAX_MP_PROPERTIES + 1 )
			PRINTLN("[RCC MISSION][MJM] IS_PLAYER_NEAR_STARTING_APARTMENT() - iPropertyIndex is in range")
			vLoc = mpProperties[iPropertyIndex].vBlipLocation[0]
			IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].fRadius != 0.0
				PRINTLN("[RCC MISSION][MJM] IS_PLAYER_NEAR_STARTING_APARTMENT() - g_FMMC_STRUCT.sDialogueTriggers[iDialogue].fRadius != 0.0")
				IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTeam = MC_playerBD[iPartToUse].iteam
					PRINTLN("[RCC MISSION][MJM] IS_PLAYER_NEAR_STARTING_APARTMENT() - g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTeam = MC_playerBD[iPartToUse].iteam")
					PED_INDEX tempped = PlayerPedToUse
					IF NOT IS_PED_INJURED(tempped)
						PRINTLN("[RCC MISSION][MJM] IS_PLAYER_NEAR_STARTING_APARTMENT() - NOT IS_PED_INJURED(tempped)")
						IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(tempped,vLoc) <= g_FMMC_STRUCT.sDialogueTriggers[iDialogue].fRadius
							bShouldTrigger = TRUE
							PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - IS_PLAYER_NEAR_STARTING_APARTMENT - Trigger conditions met.")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION][MJM] IS_PLAYER_NEAR_STARTING_APARTMENT() - iPropertyIndex is out of range")
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL HAS_VEHICLE_BEEN_DAMAGED_PAST_THRESHOLD(INT iDialogue)

	BOOL bShouldTrigger
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVehicleTrigger = -1
	OR g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVehicleDamage = -1
		bShouldTrigger = TRUE
	ELSE
		INT iTriggerVehicle = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVehicleTrigger
		
		NETWORK_INDEX netVeh = MC_serverBD_1.sFMMC_SBD.niVehicle[iTriggerVehicle]
		VEHICLE_INDEX viVeh
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(netVeh)
			PRINTLN("[RCC MISSION][MJM] HAS_VEHICLE_BEEN_DAMAGED_PAST_THRESHOLD - NETWORK_DOES_NETWORK_ID_EXIST(netVeh)")	 
			
			viVeh = NET_TO_VEH(netVeh)
			FLOAT fPercentage = GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(viVeh, iTriggerVehicle, MC_serverBD.iTotalNumStartingPlayers)
					
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_VehDamageAboveOrBelow)
				//Above
				PRINTLN("[ML] iBS_Dialogue3_VehDamageAboveOrBelow is NOT SET (So trigger when taken too much damage) Percentage to use: ", (100 - fPercentage), " fPercentage: ", fPercentage, " g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVehicleDamage: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVehicleDamage)
				IF (100 - fPercentage) >= g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVehicleDamage
				OR IS_ANY_VEHICLE_DOOR_DAMAGED(viVeh)
					bShouldTrigger = TRUE
					PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_VEHICLE_BEEN_DAMAGED_PAST_THRESHOLD - Trigger conditions met (above threshold). bShouldTrigger = TRUE")
				ENDIF	
				
				//If we're checking for damage over 100% check if the vehicle is dead or no longer driveable - url:bugstar:6155554
				IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVehicleDamage = 100
					IF DOES_ENTITY_EXIST(viVeh)
						IF IS_ENTITY_DEAD(viVeh)
						OR NOT IS_VEHICLE_DRIVEABLE(viVeh)
							bShouldTrigger = TRUE
							PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_VEHICLE_BEEN_DAMAGED_PAST_THRESHOLD - Trigger conditions met (above threshold). bShouldTrigger = TRUE")
						ELSE //If we're checking for over 100% damage, do not trigger for just the doors being damaged. url:bugstar:6208238
							IF bShouldTrigger
							AND (100 - fPercentage) < g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVehicleDamage
								bShouldTrigger = FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			ELSE
				//Below
				PRINTLN("[ML] iBS_Dialogue3_VehDamageAboveOrBelow is SET (So trigger when not yet taken too much damage)")
				PRINTLN("[ML] Percentage to use: ", (100 - fPercentage), " fPercentage: ", fPercentage, " g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVehicleDamage: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVehicleDamage)
				IF (100 - fPercentage) < g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVehicleDamage
				AND NOT IS_ANY_VEHICLE_DOOR_DAMAGED(viVeh)
					bShouldTrigger = TRUE
					PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_VEHICLE_BEEN_DAMAGED_PAST_THRESHOLD - Trigger conditions met (below threshold).  bShouldTrigger = TRUE")
				ENDIF
			ENDIF
			
		ENDIF

	ENDIF

	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL HAS_PED_BEEN_DAMAGED_PAST_THRESHOLD(INT iDialogue)

	BOOL bShouldTrigger
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPedTrigger = -1
	OR g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPedDamage = -1
		bShouldTrigger = TRUE
	ELSE
		INT iTriggerPed = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPedTrigger
		
		NETWORK_INDEX netPed = MC_serverBD_1.sFMMC_SBD.niPed[iTriggerPed]
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(netPed)
			
			PED_INDEX piPed = NET_TO_PED(netPed)
			
			PRINTLN("[RCC MISSION][ML] HAS_PED_BEEN_DAMAGED_PAST_THRESHOLD - NETWORK_DOES_NETWORK_ID_EXIST(netPed)")	 
			
			FLOAT fPercentage = (TO_FLOAT(GET_ENTITY_HEALTH(piPed)) / TO_FLOAT(GET_ENTITY_MAX_HEALTH(piPed)) * 100.0)
			
			IF (100 - fPercentage) >= g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPedDamage
				bShouldTrigger = TRUE
				PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_PED_BEEN_DAMAGED_PAST_THRESHOLD - Trigger conditions met. DAMAGED - bShouldTrigger = TRUE")				
			ENDIF
		ENDIF

	ENDIF

	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_REPAIR_DIALOGUE_BE_TRIGGERED(INT iDialogue)

	BOOL bShouldTrigger = TRUE

	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_DamagedVehicleNowRepaired)
	
		IF CURRENT_OBJECTIVE_REPAIR_CAR()
			SET_BIT(iLocalBoolCheck31, LBOOL31_IS_CURRENT_OBJECTIVE_REPAIR_CAR)
			PRINTLN("[ML][SHOULD_VEHICLE_REPAIR_DIALOGUE_BE_TRIGGERED] Current objective is repair car, setting LBOOL31_IS_CURRENT_OBJECTIVE_REPAIR_CAR")
			bShouldTrigger = FALSE
		ELSE
			IF IS_BIT_SET(iLocalBoolCheck31, LBOOL31_IS_CURRENT_OBJECTIVE_REPAIR_CAR)
				IF NOT IS_PLAYER_IN_SHOP_OF_TYPE(SHOP_TYPE_CARMOD)
					CLEAR_BIT(iLocalBoolCheck31, LBOOL31_IS_CURRENT_OBJECTIVE_REPAIR_CAR)
					PRINTLN("[ML][SHOULD_VEHICLE_REPAIR_DIALOGUE_BE_TRIGGERED] Just left repair car objective. Clearing LBOOL31_IS_CURRENT_OBJECTIVE_REPAIR_CAR. Returning SHOULD_VEHICLE_REPAIR_DIALOGUE_BE_TRIGGERED = TRUE")
					bShouldTrigger = TRUE
				ENDIF
			ELSE
				bShouldTrigger = FALSE
			ENDIF
		ENDIF	
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL SHOULD_ENTERED_SWITCH_VEHICLE_UNSEEN_DIALOGUE_BE_TRIGGERED(INT iDialogue)

	BOOL bShouldTrigger = TRUE

	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_EnteredSwitchVehUnseen)
		IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_SET_BLIP_IN_BLOCK_WANTED_CONE_RESPONSE_VEHICLE)
			PRINTLN("[SHOULD_ENTERED_SWITCH_VEHICLE_UNSEEN_DIALOGUE_BE_TRIGGERED] Returning TRUE")
			bShouldTrigger = TRUE
		ELSE	
			bShouldTrigger = FALSE
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL SHOULD_ENTERED_SWITCH_VEHICLE_SEEN_DIALOGUE_BE_TRIGGERED(INT iDialogue)

	BOOL bShouldTrigger = TRUE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_EnteredSwitchVehSeen)
		bShouldTrigger = FALSE	
	
		VEHICLE_INDEX vehTemp
		INT iVeh
		FOR iVeh = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfPeds
			IF iVeh < FMMC_MAX_VEHICLES-1
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBlockWantedConeResponse = ci_VEHICLE_BLOCK_WANTED_CONE_RESPONSE_On
				OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBlockWantedConeResponse = ci_VEHICLE_BLOCK_WANTED_CONE_RESPONSE_UntilIllegal
					IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
						vehTemp = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
			
						IF IS_PED_IN_VEHICLE(LocalPlayerPed, vehTemp, TRUE)
						AND IS_BIT_SET(MC_serverBD_1.iVehicleBlockedWantedConeResponseSeenIllegalBS, iVeh)
							PRINTLN("[SHOULD_ENTERED_SWITCH_VEHICLE_SEEN_DIALOGUE_BE_TRIGGERED] Returning TRUE")
							bShouldTrigger = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL SHOULD_NEAR_SWITCH_VEHICLE_DIALOGUE_BE_TRIGGERED(INT iDialogue)

	BOOL bShouldTrigger = TRUE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_NearSwitchVeh)
		bShouldTrigger = FALSE	
	
		VEHICLE_INDEX vehTemp
		INT iVeh
		FOR iVeh = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfPeds
			IF iVeh < FMMC_MAX_VEHICLES-1
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBlockWantedConeResponse = ci_VEHICLE_BLOCK_WANTED_CONE_RESPONSE_On
				OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBlockWantedConeResponse = ci_VEHICLE_BLOCK_WANTED_CONE_RESPONSE_UntilIllegal
					IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
						vehTemp = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
			
						IF IS_ENTITY_IN_RANGE_ENTITY(LocalPlayerPed, vehTemp, 75.0, FALSE)
							PRINTLN("[SHOULD_NEAR_SWITCH_VEHICLE_DIALOGUE_BE_TRIGGERED] Returning TRUE")
							bShouldTrigger = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL SHOULD_ENTERED_GETAWAY_VEHICLE_DIALOGUE_BE_TRIGGERED(INT iDialogue)

	BOOL bShouldTrigger = TRUE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_EnteredGetawayVeh)
		bShouldTrigger = FALSE	
	
		VEHICLE_INDEX vehTemp
		INT iVeh
		FOR iVeh = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfPeds
			IF iVeh < FMMC_MAX_VEHICLES-1
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehGetawayVehicleSlot > -1
					IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
						vehTemp = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
			
						IF IS_PED_IN_VEHICLE(LocalPlayerPed, vehTemp, TRUE)
							PRINTLN("[SHOULD_ENTERED_GETAWAY_VEHICLE_DIALOGUE_BE_TRIGGERED] Returning TRUE")
							bShouldTrigger = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL HAS_PHONE_HACKING_MINIGAME_BEEN_COMPLETED(INT iDialogue)

	BOOL bShouldTrigger
	
	IF (NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset,iBS_DialoguePlayOnMinigamePass) )
	AND (NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset,iBS_DialoguePlayOnMinigameFail ) )
		bShouldTrigger = TRUE
	ELSE
		IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset,iBS_DialoguePlayOnMinigamePass)
			IF IS_BIT_SET(MC_serverBD_1.iServerHackMGBitset,SBBOOL_HACK_DONE)
				PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_PHONE_HACKING_MINIGAME_BEEN_COMPLETED - Trigger conditions met. Done")
				bShouldTrigger = TRUE
			ENDIF
		ELSE //Trigger on minigame fail
			IF IS_BIT_SET(MC_serverBD_1.iServerHackMGBitset,SBBOOL_HACK_FAILED)
				PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_PHONE_HACKING_MINIGAME_BEEN_COMPLETED - Trigger conditions met. Fail")
				bShouldTrigger = TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL HAVE_PEDS_BEEN_KILLED_DURING_CROWD_CONTROL(INT iDialogue)

	BOOL bShouldTrigger
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iCrowdControlKills <= 0
		bShouldTrigger = TRUE
	ELSE
		INT iTotalKills = GET_CROWD_CONTROL_NUMBER_OF_DEAD_PEDS( sCCLocalPedDecorCopy, MC_serverBD_3.sCCServerData )
		INT iMaxKills = GET_CROWD_CONTROL_PED_DEAD_LIMIT( sCCLocalData.eHeistType )
		INT iActualKills = IMIN(iTotalKills,iMaxKills) //Don't want to count any kills past what would fail the minigame
		
		IF iActualKills = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iCrowdControlKills
			PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAVE_PEDS_BEEN_KILLED_DURING_CROWD_CONTROL - Trigger conditions met.")
			bShouldTrigger = TRUE
		ENDIF
	ENDIF

	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL IS_PLAYER_WEARING_MASK_TO_TRIGGER_DIALOGUE(INT iDialogue)

	BOOL bShouldTrigger
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2,iBS_Dialogue2PlayOnlyWhenInMask)
		bShouldTrigger = TRUE
	ELSE
		IF IS_MP_HEIST_MASK_EQUIPPED(LocalPlayerPed, INT_TO_ENUM(MP_OUTFIT_MASK_ENUM,GET_LOCAL_DEFAULT_MASK(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTeam)))
			PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - IS_PLAYER_WEARING_MASK_TO_TRIGGER_DIALOGUE - Trigger conditions met.")
			bShouldTrigger = TRUE
		ENDIF
	ENDIF

	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL IS_ENOUGH_GANG_CHASE_UNITS_DEAD_TO_TRIGGER_DIALOGUE(INT iDialogue)

	BOOL bShouldTrigger
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iGangChasePedsToKill = -1
		bShouldTrigger = TRUE
	ELSE
		IF MC_serverBD_4.iGangChasePedsKilledThisRule >= g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iGangChasePedsToKill
			PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - IS_ENOUGH_GANG_CHASE_UNITS_DEAD_TO_TRIGGER_DIALOGUE - Trigger conditions met.")
			bShouldTrigger = TRUE
		ENDIF
	ENDIF

	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL IS_PLAYER_FLYING_TOO_HIGH_TO_TRIGGER_DIALOGUE(INT iDialogue)
	BOOL bShouldTrigger

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_FlyTooHigh)
		bShouldTrigger = TRUE
	ELIF IS_BIT_SET(iLocalBoolCheck27, LBOOL27_FLYING_TOO_HIGH)
		PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - IS_PLAYER_FLYING_TOO_HIGH_TO_TRIGGER_DIALOGUE - Trigger conditions met.")
		bShouldTrigger = TRUE
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL IS_PLAYER_SPAWNING_GANG_CHASE_DUE_TO_FLYING_TOO_HIGH(INT iDialogue)
	BOOL bShouldTrigger

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_AltitudeGCTriggered)
		bShouldTrigger = TRUE
	ELIF IS_BIT_SET(MC_serverBD.iServerBitSet6, SBBOOL6_ALLOW_GANGCHASE_AFTER_SPEED_FAIL)
		PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - IS_PLAYER_SPAWNING_GANG_CHASE_DUE_TO_FLYING_TOO_HIGH - Trigger conditions met.")
		bShouldTrigger = TRUE
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL IS_PLAYER_VEH_UNDERWATER_TO_TRIGGER_DIALOGUE(INT iDialogue)
	BOOL bShouldTrigger

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_SubVehicleUnderwater)
		bShouldTrigger = TRUE
	ELIF IS_LOCAL_PED_DRIVING_VEHICLE_UNDERWATER(IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_SubVehicleMode)) //url:bugstar:4187127
		PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - IS_PLAYER_VEH_UNDERWATER_TO_TRIGGER_DIALOGUE - Trigger conditions met.")
		bShouldTrigger = TRUE
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL IS_PLAYER_VEH_BACK_OUT_OF_WATER_TO_TRIGGER_DIALOGUE(INT iDialogue)
	BOOL bShouldTrigger

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_SubVehicleOutofWater)
		bShouldTrigger = TRUE
	ELSE
		IF IS_LOCAL_PED_DRIVING_VEHICLE_UNDERWATER()
			SET_BIT(iLocalBoolCheck26, LBOOL26_BEEN_UNDERWATER)
		ELSE
			IF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_BEEN_UNDERWATER)
				PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - IS_PLAYER_VEH_UNDERWATER_TO_TRIGGER_DIALOGUE - Trigger conditions met.")
				bShouldTrigger = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC	

FUNC BOOL HAS_PLAYER_ACTIVATED_THERMAL_VISION(INT iDialogue)
	BOOL bShouldTrigger

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_ThermalVisionActivated)
		bShouldTrigger = TRUE
	ELSE
		IF GET_PLAYER_VISUAL_AID_MODE() = VISUALAID_THERMAL
			PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_PLAYER_ACTIVATED_THERMAL_VISION - Trigger conditions met.")
			bShouldTrigger = TRUE
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC	

FUNC BOOL HAS_PLAYER_HIT_FIREWALL_TO_TRIGGER_DIALOGUE(INT iDialogue)
	BOOL bShouldTrigger

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_FirewallHit)
		bShouldTrigger = TRUE
	ELSE
		IF MC_playerBD[iPartToUse].iObjHacking != -1
		AND IS_BIT_SET(iLocalBoolCheck26, LBOOL26_STARTED_BEAM_HACK)
		AND IS_BIT_SET(sBeamhack[MC_playerBD[iPartToUse].iObjHacking].iBS, ciGOHACKBS_FIREWALL_HIT)
			PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_PLAYER_HIT_FIREWALL_TO_TRIGGER_DIALOGUE - Trigger conditions met.")
			bShouldTrigger = TRUE
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC	

FUNC BOOL HAS_PLAYER_HIT_PACKET_TO_TRIGGER_DIALOGUE(INT iDialogue)
	BOOL bShouldTrigger

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_PacketHit)
		bShouldTrigger = TRUE
	ELSE
		IF MC_playerBD[iPartToUse].iObjHacking != -1
		AND IS_BIT_SET(iLocalBoolCheck26, LBOOL26_STARTED_BEAM_HACK)
		AND IS_BIT_SET(sBeamhack[MC_playerBD[iPartToUse].iObjHacking].iBS, ciGOHACKBS_PACKET_HIT)
			PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_PLAYER_HIT_PACKET_TO_TRIGGER_DIALOGUE - Trigger conditions met.")
			bShouldTrigger = TRUE
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC	

FUNC BOOL HAS_PLAYER_FAILED_HACKING_PATTERN_TO_TRIGGER_DIALOGUE(INT iDialogue)
	BOOL bShouldTrigger = TRUE

	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_FingerprintClonePatternFail)
		bShouldTrigger = FALSE
		
		IF MC_playerBD[iPartToUse].iObjHacking != -1
		AND IS_BIT_SET(iLocalBoolCheck32, LBOOL32_STARTED_FINGERPRINT_CLONE)
		AND IS_BIT_SET(sFingerprintClone[MC_playerBD[iPartToUse].iObjHacking].iBS, ciHGBS_LOST_LIFE)
			PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_PLAYER_FAILED_HACKING_PATTERN_TO_TRIGGER_DIALOGUE - Trigger conditions met for Fingerprint Clone.")
			bShouldTrigger = TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_OrderUnlockPatternFail)
		bShouldTrigger = FALSE
	
		IF MC_playerBD[iPartToUse].iObjHacking != -1
		AND IS_BIT_SET(iLocalBoolCheck32, LBOOL32_STARTED_ORDER_UNLOCK)
		AND IS_BIT_SET(sOrderUnlock[MC_playerBD[iPartToUse].iObjHacking].iBS, ciHGBS_LOST_LIFE)
			PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_PLAYER_FAILED_HACKING_PATTERN_TO_TRIGGER_DIALOGUE - Trigger conditions met for Order Unlock.")
			bShouldTrigger = TRUE
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC	

FUNC BOOL HAS_PLAYER_PASSED_HACKING_PATTERN_TO_TRIGGER_DIALOGUE(INT iDialogue)
	BOOL bShouldTrigger = TRUE

	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_FingerprintClonePatternDone)
		bShouldTrigger = FALSE
		
		IF MC_playerBD[iPartToUse].iObjHacking != -1
		AND IS_BIT_SET(iLocalBoolCheck32, LBOOL32_STARTED_FINGERPRINT_CLONE)
		AND IS_BIT_SET(sFingerprintClone[MC_playerBD[iPartToUse].iObjHacking].iBS, ciHGBS_PASSED_SECTION)
			PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_PLAYER_PASSED_HACKING_PATTERN_TO_TRIGGER_DIALOGUE - Trigger conditions met for Fingerprint Clone.")
			bShouldTrigger = TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_OrderUnlockPatternDone)
		bShouldTrigger = FALSE
	
		IF MC_playerBD[iPartToUse].iObjHacking != -1
		AND IS_BIT_SET(iLocalBoolCheck32, LBOOL32_STARTED_ORDER_UNLOCK)
		AND IS_BIT_SET(sOrderUnlock[MC_playerBD[iPartToUse].iObjHacking].iBS, ciHGBS_PASSED_SECTION)
			PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_PLAYER_PASSED_HACKING_PATTERN_TO_TRIGGER_DIALOGUE - Trigger conditions met for Order Unlock.")
			bShouldTrigger = TRUE
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC	

FUNC BOOL HAS_NOT_REACHED_MIDPOINT_YET_TRIGGER_DIALOGUE(INT iDialogue)
	BOOL bShouldTrigger

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_NotOnMidPointYet)
		bShouldTrigger = TRUE
	ELSE
		IF MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iPartToUse].iteam] > -1 AND MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iPartToUse].iteam] < FMMC_MAX_RULES
			IF NOT HAS_NET_TIMER_STARTED(tdDialogueTimer[iDialogueProgress])
			OR ((GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdDialogueTimer[iDialogueProgress]) > (g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iTimeDelay*1000)) AND NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_PlayerBD[iPartToUse].iteam], MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iPartToUse].iteam]))
				PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_NOT_REACHED_MIDPOINT_YET_TRIGGER_DIALOGUE - Trigger conditions met.")
				bShouldTrigger = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL HAS_PLAYERS_NOT_IN_SEPARATE_OBJECTIVE_VEHICLES_TRIGGER_DIALOGUE(INT iDialogue)
	BOOL bShouldTrigger

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_NotInSameUniqueVeh)
		bShouldTrigger = TRUE
	ELSE
		IF MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iPartToUse].iteam] > -1 AND MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iPartToUse].iteam] < FMMC_MAX_RULES
			IF MC_serverBD.iTeamUniqueVehHeldNum[MC_PlayerBD[iPartToUse].iteam] > 1
				PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_PLAYERS_IN_SEPARATE_OBJECTIVE_VEHICLES_TRIGGER_DIALOGUE - Trigger conditions met.")
				bShouldTrigger = TRUE
			ENDIF
		ENDIF		
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC	

FUNC BOOL NOT_BLOCKED_DUE_TO_BEING_ANY_KIND_OF_SPECTATOR(INT iDialogue)
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_NotForAnyKindOfSpectators)
	AND (IS_LOCAL_PLAYER_ANY_SPECTATOR()
	OR IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE())
		PRINTLN("[Dialogue] Not showing dialogue ", iDialogue, " because I'm a spectator and iBS_Dialogue3_NotForSpectators is set")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL NOT_BLOCKED_DUE_TO_BEING_SPECTATOR(INT iDialogue)
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_NotForSpectators)
	AND IS_LOCAL_PLAYER_ANY_SPECTATOR()
	AND NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
		PRINTLN("[Dialogue] Not showing dialogue ", iDialogue, " because I'm a spectator and iBS_Dialogue3_NotForSpectators is set")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL NOT_BLOCKED_DUE_TO_NOT_CARRYING_OBJ_VEH_WITH_CARGOBOB(INT iDialogue)
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_MustBeCarryingObjVehicle)
		IF iObjectiveVehicleAttachedToCargobobBS > 0
			PRINTLN("[Dialogue] Allowing dialogue ", iDialogue, " because I AM carrying an objective vehicle with my cargobob")
			RETURN TRUE
		ELSE
			PRINTLN("[Dialogue] Not showing dialogue ", iDialogue, " because I'm not carrying an objective vehicle with my cargobob")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_SPECIFIC_VEHICLE_OPTION_INSIDE_VEHICLE_VALID(INT iDialogue)
	BOOL bReturn = TRUE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_VehSpec_MustBeInsideVehType)
		bReturn = FALSE
		MODEL_NAMES vehModel = INT_TO_ENUM(MODEL_NAMES, g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVehicleModel)
		PRINTLN("[Dialogue] IS_SPECIFIC_VEHICLE_OPTION_INSIDE_VEHICLE_VALID ", iDialogue, " iVehicleModel: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVehicleModel, " Model: ", GET_MODEL_NAME_FOR_DEBUG(vehModel))
		
		IF IS_MODEL_A_VEHICLE(vehModel)
			IF NOT IS_PED_INJURED(PlayerPedToUse)
				VEHICLE_INDEX vehIndex
				IF IS_PED_IN_ANY_VEHICLE(PlayerPedToUse)
					vehIndex = GET_VEHICLE_PED_IS_IN(PlayerPedToUse)
					IF GET_ENTITY_MODEL(vehIndex) = vehModel
						bReturn = TRUE
						PRINTLN("[Dialogue] IS_SPECIFIC_VEHICLE_OPTION_INSIDE_VEHICLE_VALID ", iDialogue, " Returning TRUE. Player is inside Vehicle Model: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(vehIndex)))
					ENDIF
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[Dialogue] IS_SPECIFIC_VEHICLE_OPTION_INSIDE_VEHICLE_VALID ", iDialogue, " Vehicle Model not valid. Allowing this trigger to return true.")
		ENDIF
	ENDIF
	
	RETURN bReturn	
ENDFUNC

FUNC BOOL IS_SPECIFIC_VEHICLE_OPTION_OWNS_VEHICLE_VALID(INT iDialogue)

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_VehSpec_MustOwnVehType)
		RETURN TRUE
	ENDIF
		
	IF IS_BIT_SET(iDialogueVehicleOwnedBS[iDialogueProgress/32],iDialogue%32)
		PRINTLN("[DialogueTrigger] IS_SPECIFIC_VEHICLE_OPTION_OWNS_VEHICLE_VALID ", iDialogue, " Returning TRUE. Vehicle ownership checked earlier")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(iDialogueVehicleNotOwnedBS[iDialogueProgress/32],iDialogue%32)
		RETURN FALSE
	ENDIF
		
	MODEL_NAMES vehModel = INT_TO_ENUM(MODEL_NAMES, g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVehicleModel)
	IF IS_MODEL_A_VEHICLE(vehModel)
		IF DOES_PLAYER_OWN_THIS_PEGASUS_VEHICLE(vehModel)
			PRINTLN("[DialogueTrigger] IS_SPECIFIC_VEHICLE_OPTION_OWNS_VEHICLE_VALID ", iDialogue, " Returning TRUE. Player owns this Pegasus vehicle model.")
			SET_BIT(iDialogueVehicleOwnedBS[iDialogueProgress/32],iDialogue%32)	
			RETURN TRUE
		ENDIF
		IF DOES_PLAYER_OWN_ANY_SAVED_VEHICLES_OF_MODEL(vehModel)
			PRINTLN("[DialogueTrigger] IS_SPECIFIC_VEHICLE_OPTION_OWNS_VEHICLE_VALID ", iDialogue, " Returning TRUE. Player owns this vehicle model.")
			SET_BIT(iDialogueVehicleOwnedBS[iDialogueProgress/32],iDialogue%32)	
			RETURN TRUE
		ENDIF	
		
		SET_BIT(iDialogueVehicleNotOwnedBS[iDialogueProgress/32],iDialogue%32)	
		PRINTLN("[DialogueTrigger] IS_SPECIFIC_VEHICLE_OPTION_OWNS_VEHICLE_VALID ", iDialogue, " Returning FALSE. Player does not own this vehicle model.")
	ELSE
		PRINTLN("[RCC MISSION][DialogueTrigger] IS_SPECIFIC_VEHICLE_OPTION_OWNS_VEHICLE_VALID = Not Valid Vehicle Model not valid")
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL HAS_THE_LAST_VEHICLE_FOR_THIS_RULE_BEEN_HACKED()
	INT iTeam = MC_playerBD[iPartToUse].iTeam
	BOOL bValid = FALSE
	
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet6, SBBOOL6_COUNTED_VEH_HACK_TARGETS)
		PRINTLN("[SecuroHack] HAS_PLAYER_HACKED_VEHICLE_TO_TRIGGER_DIALOGUE - HAS_THE_LAST_VEHICLE_FOR_THIS_RULE_BEEN_HACKED - Returning false due to SBBOOL6_COUNTED_VEH_HACK_TARGETS not being set")
		RETURN FALSE
	ELSE
		PRINTLN("[SecuroHack] HAS_THE_LAST_VEHICLE_FOR_THIS_RULE_BEEN_HACKED - SBBOOL6_COUNTED_VEH_HACK_TARGETS is set so we're gonna check the other things now ")
	ENDIF
	
	bValid = (MC_serverBD_4.iHackingTargetsRemaining = 0 AND NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam))
	
	PRINTLN("[SecuroHack] HAS_THE_LAST_VEHICLE_FOR_THIS_RULE_BEEN_HACKED - bValid: ", bValid, " / MC_serverBD_4.iHackingTargetsRemaining: ", MC_serverBD_4.iHackingTargetsRemaining)
	
	IF bValid
		PRINTLN("[SecuroHack] HAS_THE_LAST_VEHICLE_FOR_THIS_RULE_BEEN_HACKED - Playing dialogue due to other player performing hack on last hack veh!")
	ENDIF
	
	RETURN bValid
ENDFUNC

FUNC BOOL HAS_ANY_PLAYER_HACKED_THIS_VEHICLE(INT iVeh)
	RETURN IS_BIT_SET(MC_serverBD_4.iVehHackedReadyForDeathBS, iVeh)
ENDFUNC

FUNC BOOL HAS_PLAYER_HACKED_VEHICLE_TO_TRIGGER_DIALOGUE(INT iDialogue)
	BOOL bShouldTrigger

	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVehicleHacked <= 0
		bShouldTrigger = TRUE
	ELSE
		INT i
		REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles i
			IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVehicleHacked, i)
				IF (IS_BIT_SET(MC_PlayerBD[iPartToUse].iVehDestroyBitset, i) AND NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_VehHackedByAnyPlayer))
				OR (HAS_ANY_PLAYER_HACKED_THIS_VEHICLE(i) AND HAS_THE_LAST_VEHICLE_FOR_THIS_RULE_BEEN_HACKED() AND IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_VehHackedByAnyPlayer))
					PRINTLN("[RCC MISSION][SecuroHack] ", iDialogue, " IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_PLAYER_HACKED_VEHICLE_TO_TRIGGER_DIALOGUE - Trigger conditions met.")
					
					IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_VehHackedByAnyPlayer)
						PRINTLN("[RCC MISSION][SecuroHack] ", iDialogue, " IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - The final vehicle has been hacked by someone! Veh ", i)
					ENDIF
					
					SET_BIT(iPlayedDialogueForVehHackBS, i)
					SET_BIT(iLocalBoolCheck3, LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
					PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - HAS_PLAYER_HACKED_VEHICLE_TO_TRIGGER_DIALOGUE")
					bShouldTrigger = TRUE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	
	RETURN bShouldTrigger
ENDFUNC	

FUNC BOOL HAS_PLAYER_STARTED_HOTWIRE_TRIGGER_DIALOGUE(INT iDialogue)
	BOOL bShouldTrigger

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_HotwireStart)
		bShouldTrigger = TRUE
	ELSE
		IF MC_playerBD[iPartToUse].iObjHacking != -1
		AND IS_BIT_SET(iLocalBoolCheck26, LBOOL26_STARTED_HOTWIRE_HACK)
			PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_PLAYER_STARTED_HOTWIRE_TRIGGER_DIALOGUE - Trigger conditions met.")
			bShouldTrigger = TRUE
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC	

FUNC BOOL HAS_PLAYER_STARTED_BEAMHACK_TRIGGER_DIALOGUE(INT iDialogue)
	BOOL bShouldTrigger

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_BeamHackStart)
		bShouldTrigger = TRUE
	ELSE
		IF MC_playerBD[iPartToUse].iObjHacking != -1
		AND IS_BIT_SET(iLocalBoolCheck26, LBOOL26_STARTED_BEAM_HACK)
			PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_PLAYER_STARTED_BEAMHACK_TRIGGER_DIALOGUE - Trigger conditions met.")
			bShouldTrigger = TRUE
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC	

FUNC BOOL HAS_PLAYER_STARTED_FINGERPRINT_CLONE_TRIGGER_DIALOGUE(INT iDialogue)
	BOOL bShouldTrigger

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_StartedFingerprintClone)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_FingerprintCloneFirstTime)
		bShouldTrigger = TRUE
	ELSE
		IF MC_playerBD[iPartToUse].iObjHacking != -1
		AND IS_BIT_SET(iLocalBoolCheck32, LBOOL32_STARTED_FINGERPRINT_CLONE)
			IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_FingerprintCloneFirstTime)
				IF IS_BIT_SET(sFingerprintClone[MC_playerBD[iPartToUse].iObjHacking].iBS, ciHGBS_FIRST_PLAYED)
					PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_PLAYER_STARTED_FINGERPRINT_CLONE_TRIGGER_DIALOGUE FIRST TIME - Trigger conditions met.")
					bShouldTrigger = TRUE
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_PLAYER_STARTED_FINGERPRINT_CLONE_TRIGGER_DIALOGUE - Trigger conditions met.")
				bShouldTrigger = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL HAS_PLAYER_STARTED_ORDER_UNLOCK_TRIGGER_DIALOGUE(INT iDialogue)
	BOOL bShouldTrigger

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_StartedOrderUnlock)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_OrderUnlockFirstTime)
		bShouldTrigger = TRUE
	ELSE
		IF MC_playerBD[iPartToUse].iObjHacking != -1
		AND IS_BIT_SET(iLocalBoolCheck32, LBOOL32_STARTED_ORDER_UNLOCK)
			IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_OrderUnlockFirstTime)
				IF IS_BIT_SET(sOrderUnlock[MC_playerBD[iPartToUse].iObjHacking].iBS, ciHGBS_FIRST_PLAYED)
					PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_PLAYER_STARTED_ORDER_UNLOCK_TRIGGER_DIALOGUE FIRST TIME - Trigger conditions met.")
					bShouldTrigger = TRUE
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_PLAYER_STARTED_ORDER_UNLOCK_TRIGGER_DIALOGUE - Trigger conditions met.")
				bShouldTrigger = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC	

FUNC BOOL IS_PLAYER_IN_VEHICLE_OF_PED_VOICE(INT iDialogue)
	BOOL bShouldTrigger = TRUE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_VehSpec_InSameVehAsPedVoice)
		INT iPedVoice = -1
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPedVoice > -1
			iPedVoice = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPedVoice
		ENDIF
		IF iPedVoice > -1		
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iPedVoice])
			AND g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSecondPedVoice > -1
				iPedVoice = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSecondPedVoice
			ENDIF
		ELSE
			iPedVoice = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSecondPedVoice
		ENDIF
		IF iPedVoice > -1
			bShouldTrigger = FALSE
			IF NOT IS_PED_INJURED(PlayerPedToUse)
			AND IS_PED_IN_ANY_VEHICLE(PlayerPedToUse)
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iPedVoice])
					PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - IS_PLAYER_IN_VEHICLE_OF_PED_VOICE - Checking iPed: ", iPedVoice, " Is alive and in a vehicle.")
					
					PED_INDEX pedIndex = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPedVoice])
					IF NOT IS_PED_INJURED(pedIndex)
					AND IS_PED_IN_ANY_VEHICLE(pedIndex)
						VEHICLE_INDEX vehPedIsIn = GET_VEHICLE_PED_IS_IN(pedIndex)
						VEHICLE_INDEX vehPlayerIsIn = GET_VEHICLE_PED_IS_IN(PlayerPedToUse)
						
						IF vehPedIsIn = VehPlayerIsIn
							PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - IS_PLAYER_IN_VEHICLE_OF_PED_VOICE - bShouldTrigger = TRUE")
							bShouldTrigger = TRUE
						ELSE
							PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - IS_PLAYER_IN_VEHICLE_OF_PED_VOICE - Player is not in same vehicle as ped for dialogue.")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger	
ENDFUNC

FUNC BOOL HAS_VEHICLE_BOMB_BAY_DOORS_CLOSED(INT iDialogue)
	BOOL bShouldTrigger

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset3, iBS_Dialogue3_BombBayDoorsClosed)
		bShouldTrigger = TRUE
	ELSE
		IF IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset, iBS_DialogueRequireObjectivVehicle )
		AND g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle >= 0
		AND NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle])
		AND NOT GET_ARE_BOMB_BAY_DOORS_OPEN(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle]))
			PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_VEHICLE_BOMB_BAY_DOORS_CLOSED - Trigger conditions met.")
			bShouldTrigger = TRUE
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC	

FUNC BOOL HAS_PED_DIED_TO_TRIGGER_DIALOGUE(INT iDialogue)
	BOOL bShouldTrigger

	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPedKilled = -1
		bShouldTrigger = TRUE
	ELSE
		IF IS_BIT_SET(MC_serverBD_4.iPedKilledBS[GET_LONG_BITSET_INDEX(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPedKilled)], GET_LONG_BITSET_BIT(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPedKilled))
			PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_PED_DIED_TO_TRIGGER_DIALOGUE - Trigger conditions met.")
			PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - dead ped: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPedKilled)			
			bShouldTrigger = TRUE
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC	

FUNC BOOL HAS_PLAYER_GOT_CLOSE_ENOUGH_TO_WATER_TO_TRIGGER_DIALOGUE(INT iDialogue)
	BOOL bShouldTrigger

	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iWaterDistance = 0
		bShouldTrigger = TRUE
	ELSE
		FLOAT fWaterHeight
		VECTOR vFwd = GET_ENTITY_FORWARD_VECTOR(PlayerPedToUse)
		VECTOR vPlayerPos = GET_ENTITY_COORDS(PlayerPedToUse)
		
		//Check Forwards
		VECTOR vPoint = <<vFwd.x * 100, vFwd.y * 100, vFwd.z>>
		vPoint = vPlayerPos + vPoint
		vPoint.z = vPlayerPos.z + 10
		IF GET_WATER_HEIGHT(vPoint, fWaterHeight)
			PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - water found infront of player, range: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iWaterDistance)			
			bShouldTrigger = TRUE
		ENDIF
		IF NOT bShouldTrigger
			vPoint = <<vFwd.y * 100, -vFwd.x * 100, vFwd.z>>
			vPoint = vPlayerPos + vPoint
			vPoint.z = vPlayerPos.z + 10
			IF GET_WATER_HEIGHT(vPoint, fWaterHeight)
				PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - water found to the right of player, range: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iWaterDistance)			
				bShouldTrigger = TRUE
			ENDIF
			IF NOT bShouldTrigger
				vPoint = <<-vFwd.y * 100, vFwd.x * 100, vFwd.z>>
				vPoint = vPlayerPos + vPoint
				vPoint.z = vPlayerPos.z + 10
				IF GET_WATER_HEIGHT(vPoint, fWaterHeight)
					PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - water found to the left of player, range: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iWaterDistance)			
					bShouldTrigger = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL HAVE_ENOUGH_FIRES_BEEN_EXTINGUISHED(INT iDialogue)
	BOOL bShouldTrigger
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iFiresExtinguished = -1
		bShouldTrigger = TRUE
	ELSE
		INT iTeamToCheck = MC_playerBD[iPartToUse].iTeam
		
		IF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_RIOTVAN)
			iTeamToCheck = 0 //Only team 1 puts out fires in this mission
			PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM (", iDialogue, ") - HAVE_ENOUGH_FIRES_BEEN_EXTINGUISHED - Setting teamtocheck to 0 due to riotvan")
		ENDIF
		
		IF MC_serverBD_4.iNumberOfFiresExtinguished[iTeamToCheck] >= g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iFiresExtinguished
			PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM (", iDialogue, ") - HAVE_ENOUGH_FIRES_BEEN_EXTINGUISHED - Trigger conditions met.")
			bShouldTrigger = TRUE
		ELSE
			PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM (", iDialogue, ") - HAVE_ENOUGH_FIRES_BEEN_EXTINGUISHED - Team ", iTeamToCheck, " has only put out ", MC_serverBD_4.iNumberOfFiresExtinguished[iTeamToCheck], " fires out of the required ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iFiresExtinguished)
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL HAS_MULTIRULE_TIMER_REACHED_TRIGGER_TIME(INT iDialogue)
	BOOL bShouldTrigger
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iMultiruleTimeToReach = -1
		bShouldTrigger = TRUE
	ELSE
		IF HAS_NET_TIMER_STARTED(MC_serverBD_3.tdMultiObjectiveLimitTimer[MC_playerBD[iPartToUse].iteam])
			IF GET_TIME_REMAINING_ON_MULTIRULE_TIMER() <= g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iMultiruleTimeToReach
				PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_MULTIRULE_TIMER_REACHED_TRIGGER_TIME - Trigger conditions met. iMultiruleTimeToReach: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iMultiruleTimeToReach)
				bShouldTrigger = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

PROC MAINTAIN_PRISON_AMBIENCE_STATE()

	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciTURN_OFF_PRISON_YARD_AMB)
		SET_AMBIENT_ZONE_STATE("AZ_COUNTRYSIDE_PRISON_01_ANNOUNCER_ALARM",FALSE,TRUE)
		SET_AMBIENT_ZONE_STATE("AZ_COUNTRYSIDE_PRISON_01_ANNOUNCER_WARNING",FALSE,TRUE)
		SET_AMBIENT_ZONE_STATE("AZ_COUNTRYSIDE_PRISON_01_ANNOUNCER_GENERAL",FALSE,TRUE)
		//PRINTLN("TURNING OFF PRISON AMBIENCE")
	ENDIF

ENDPROC

FUNC BOOL HAS_ANY_PREREQUISITE_DIALOGUE_PLAYED(INT iDialogue)

	BOOL bShouldTrigger
	INT iRequiredDialogue = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPrerequisite
	
	IF iRequiredDialogue = -1
		bShouldTrigger = TRUE
	ELSE		
		IF IS_BIT_SET(MC_playerBD[iPartToUse].iDialoguePlayedBS[iRequiredDialogue/32],iRequiredDialogue%32)
			PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_ANY_PREREQUISITE_DIALOGUE_PLAYED - Trigger conditions met for trigger ",iDialogue,". Prereq = ", iRequiredDialogue)
			bShouldTrigger = TRUE
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_OnlyStartDelayAfterPreqreqFinished)
			IF IS_BIT_SET(iLocalDialoguePlayingBS[iRequiredDialogue/32],iRequiredDialogue%32)
				PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - HAS_ANY_PREREQUISITE_DIALOGUE_PLAYED - Trigger conditions for trigger ",iDialogue,". not yet met because Prereq = ", iRequiredDialogue, " is still playing iBS_Dialogue4_OnlyStartDelayAfterPreqreqFinished and is set.")	
				bShouldTrigger = FALSE
			ENDIF
		ENDIF
	ENDIF

	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL IS_PLAYER_IN_FIRST_PERSON(INT iDialogue)

	BOOL bShouldTrigger
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2,iBS_Dialogue2PlayOnlyInFirstPerson)
		bShouldTrigger = TRUE
	ELSE
		IF GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON
			PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - IS_PLAYER_IN_FIRST_PERSON - Trigger conditions met.")
			bShouldTrigger = TRUE
		ENDIF
	ENDIF

	RETURN bShouldTrigger

ENDFUNC

FUNC BOOL IS_DIALOGUE_TRIGGERED_SD(INT iDialogue)

	BOOL bShouldTrigger
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2, iBS_Dialogue2SuddenDeath)
		bShouldTrigger = TRUE
	ELSE
		IF IS_BIT_SET( MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH )
			PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - IS_DIALOGUE_TRIGGERED_SD - Trigger conditions met.")
			bShouldTrigger = TRUE
		ENDIF
	ENDIF

	RETURN bShouldTrigger

ENDFUNC

FUNC BOOL CAN_VEHICLE_DIAGLOUE_TRIGGER_ON_ANY_RULE(INT iDialogue)
	IF IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset, iBS_DialogueRequireObjectivVehicle )
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle >= 0
		OR g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle2 >= 0
		OR g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle3 >= 0
		OR g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle4 >= 0
		OR g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle5 >= 0
			IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2, iBS_DialogueRequireObjectivVehicleAnyRule)
				PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - CAN_VEHICLE_DIAGLOUE_TRIGGER_ON_ANY_RULE - TRUE")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE	
ENDFUNC

//url:bugstar:2488101 - Can we have the option in creator to set dialogue that plays only when inside of a specific vehicle?
FUNC BOOL IS_PLAYER_IN_VEHICLE_FOR_DIALOGUE(INT iDialogue) 
	//PRINTLN("[JS] - IS_PLAYER_IN_VEHICLE_FOR_DIALOGUE - Called")
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle >= 0
	OR g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle2 >= 0
	OR g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle3 >= 0
	OR g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle4 >= 0
	OR g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle5 >= 0
		PRINTLN("[RCC MISSION] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - IS_PLAYER_IN_VEHICLE_FOR_DIALOGUE - Using specific vehicle rule. iSpecificVehicle: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle,
			"iSpecificVehicle2: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle2,
			"iSpecificVehicle3: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle3,
			"iSpecificVehicle4: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle4,
			"iSpecificVehicle5: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle5)
			
		IF IS_NET_PARTICIPANT_INT_ID_VALID(iPartToUse)
			IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle >= 0
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle])
					IF IS_PED_IN_THIS_VEHICLE(PlayerPedToUse, NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle]))
					AND NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2, iBS_Dialogue2EnteringAnyLockedVehicle)
						PRINTLN("[RCC MISSION] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - IS_PLAYER_IN_VEHICLE_FOR_DIALOGUE - Player in the vehicle 1a")
						RETURN TRUE
					ELIF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2, iBS_Dialogue2EnteringAnyLockedVehicle)
					AND (GET_VEHICLE_PED_IS_ENTERING(PlayerPedToUse) = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle]) OR HAS_NET_TIMER_STARTED(tdDialogueTimer[iDialogueProgress]))
						PRINTLN("[RCC MISSION] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - IS_PLAYER_IN_VEHICLE_FOR_DIALOGUE - Player in the vehicle 1b")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
			IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle2 >= 0
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle2])
					IF IS_PED_IN_THIS_VEHICLE(PlayerPedToUse, NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle2]))
					AND NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2, iBS_Dialogue2EnteringAnyLockedVehicle)
						PRINTLN("[RCC MISSION] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - IS_PLAYER_IN_VEHICLE_FOR_DIALOGUE - Player in the vehicle 2a")
						RETURN TRUE
					ELIF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2, iBS_Dialogue2EnteringAnyLockedVehicle)
					AND (GET_VEHICLE_PED_IS_ENTERING(PlayerPedToUse) = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle2]) OR HAS_NET_TIMER_STARTED(tdDialogueTimer[iDialogueProgress]))
						PRINTLN("[RCC MISSION] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - IS_PLAYER_IN_VEHICLE_FOR_DIALOGUE - Player in the vehicle 2b")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
			IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle3 >= 0
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle3])
					IF IS_PED_IN_THIS_VEHICLE(PlayerPedToUse, NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle3]))
					AND NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2, iBS_Dialogue2EnteringAnyLockedVehicle)
						PRINTLN("[RCC MISSION] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - IS_PLAYER_IN_VEHICLE_FOR_DIALOGUE - Player in the vehicle 3a")
						RETURN TRUE
					ELIF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2, iBS_Dialogue2EnteringAnyLockedVehicle)
					AND (GET_VEHICLE_PED_IS_ENTERING(PlayerPedToUse) = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle3]) OR HAS_NET_TIMER_STARTED(tdDialogueTimer[iDialogueProgress]))
						PRINTLN("[RCC MISSION] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - IS_PLAYER_IN_VEHICLE_FOR_DIALOGUE - Player in the vehicle 3b")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
			IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle4 >= 0
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle4])
					IF IS_PED_IN_THIS_VEHICLE(PlayerPedToUse, NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle4]))
					AND NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2, iBS_Dialogue2EnteringAnyLockedVehicle)
						PRINTLN("[RCC MISSION] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - IS_PLAYER_IN_VEHICLE_FOR_DIALOGUE - Player in the vehicle 4a")
						RETURN TRUE
					ELIF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2, iBS_Dialogue2EnteringAnyLockedVehicle)
					AND (GET_VEHICLE_PED_IS_ENTERING(PlayerPedToUse) = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle4]) OR HAS_NET_TIMER_STARTED(tdDialogueTimer[iDialogueProgress]))
						PRINTLN("[RCC MISSION] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - IS_PLAYER_IN_VEHICLE_FOR_DIALOGUE - Player in the vehicle 4b")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
			IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle5 >= 0
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle5])
					IF IS_PED_IN_THIS_VEHICLE(PlayerPedToUse, NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle5]))
					AND NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2, iBS_Dialogue2EnteringAnyLockedVehicle)
						PRINTLN("[RCC MISSION] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - IS_PLAYER_IN_VEHICLE_FOR_DIALOGUE - Player in the vehicle 5a")
						RETURN TRUE
					ELIF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2, iBS_Dialogue2EnteringAnyLockedVehicle)
					AND (GET_VEHICLE_PED_IS_ENTERING(PlayerPedToUse) = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpecificVehicle5]) OR HAS_NET_TIMER_STARTED(tdDialogueTimer[iDialogueProgress]))
						PRINTLN("[RCC MISSION] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - IS_PLAYER_IN_VEHICLE_FOR_DIALOGUE - Player in the vehicle 5b")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		//PRINTLN("[JS] - IS_PLAYER_IN_VEHICLE_FOR_DIALOGUE - Checking if player is in any objetive vehicle")
		IF MC_playerBD[iPartToUse].iVehCarryCount > 0 
		OR IS_BIT_SET( MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER )
			PRINTLN("[RCC MISSION] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - IS_PLAYER_IN_VEHICLE_FOR_DIALOGUE - Player is in a objective vehicle 6")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_BLOCKING_IF_WANTED(INT iDialogue)
	IF IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2,iBS_Dialogue2BlockIfWanted )
		IF GET_PLAYER_WANTED_LEVEL( LocalPlayer ) > 0
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_ENTERING_ANY_VEHICLE_FOR_DIALOGUE(INT iDialogue)
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2, iBS_Dialogue2EnteringAnyVehicle)
		IF NOT IS_PED_INJURED(PlayerPedToUse)			
			IF (IS_PED_TRYING_TO_ENTER_A_LOCKED_VEHICLE(PlayerPedToUse) OR HAS_NET_TIMER_STARTED(tdDialogueTimer[iDialogueProgress]))
			AND IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2, iBS_Dialogue2EnteringAnyLockedVehicle)
				PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - IS_PLAYER_ENTERING_ANY_VEHICLE_FOR_DIALOGUE - Trigger conditions met.")
				PRINTLN("[LM] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - IS_PED_IN_RANGE_FOR_DIALOGUE_TRIGGER - We are trying to enter any locked vehicle, playing the dialogue text.")
				RETURN TRUE
			ELIF (IS_PED_IN_ANY_VEHICLE(PlayerPedToUse) OR HAS_NET_TIMER_STARTED(tdDialogueTimer[iDialogueProgress]))
			AND NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2, iBS_Dialogue2EnteringAnyLockedVehicle)
				PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - IS_PLAYER_ENTERING_ANY_VEHICLE_FOR_DIALOGUE - Trigger conditions met.")
				RETURN TRUE
			ENDIF
		ENDIF
		
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC BOOL IS_PED_IN_RANGE_FOR_DIALOGUE_TRIGGER(INT iDialogue)
	INT iPed = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPedInRangeTrigger[0]	
	IF iPed > -1	
		PRINTLN("[LM] - IS_PED_IN_RANGE_FOR_DIALOGUE_TRIGGER - iDialogue: ", iDialogue, " is connected to a Ped")
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
			PED_INDEX pedTemp = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
			IF NOT IS_PED_INJURED(pedTemp)
				IF VDIST2(GET_ENTITY_COORDS(pedTemp), g_FMMC_STRUCT.sDialogueTriggers[iDialogue].vPosition) <= POW(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].fRadius, 2)
					PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - IS_PED_IN_RANGE_FOR_DIALOGUE_TRIGGER - Trigger conditions met.")
					PRINTLN("[LM] - IS_PED_IN_RANGE_FOR_DIALOGUE_TRIGGER - iPed: ", iPed, " is now close enough to the Dialogue Trigger.")
					RETURN TRUE
				ELSE
					PRINTLN("[LM] - IS_PED_IN_RANGE_FOR_DIALOGUE_TRIGGER - iPed: ", iPed, " is not close enough to the Dialogue Trigger.")
				ENDIF
			ENDIF
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_PLAYER_IN_ZONE(INT iDialogue)
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iZoneIndex > -1
	AND IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_OnZoneTriggering)
		IF bLocalPlayerPedOk
			IF IS_LOCATION_IN_FMMC_ZONE(GET_ENTITY_COORDS(LocalPlayerPed), g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iZoneIndex)
				PRINTLN("[NA] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_PLAYER_IN_ZONE - LocalPlayerPed is now in the zone for the Dialogue Trigger.")
				RETURN TRUE
			ELSE
				PRINTLN("[NA] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_PLAYER_IN_ZONE - LocalPlayerPed is not in the zone for the Dialogue Trigger.")
			ENDIF
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_EMP_DIALOGUE_BE_TRIGGERED(INT iDialogue)
	
	BOOL bShouldTrigger = TRUE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_TriggerWhenEMPUsed)
		IF IS_PHONE_EMP_CURRENTLY_ACTIVE()
			PRINTLN("[ML] - IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - SHOULD_EMP_DIALOGUE_BE_TRIGGERED = TRUE")
			bShouldTrigger = TRUE
		ELSE
			bShouldTrigger = FALSE
		ENDIF	
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL SHOULD_METAL_DETECTOR_DIALOGUE_BE_TRIGGERED(INT iDialogue)

	BOOL bShouldTrigger = TRUE
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_TriggerWhenSettingOffMetalDetector)
		IF iMetalDetectorZoneHasBeenAlertedBS > 0
			PRINTLN("IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - SHOULD_METAL_DETECTOR_DIALOGUE_BE_TRIGGERED = TRUE")
			bShouldTrigger = TRUE
		ELSE
			PRINTLN("IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - SHOULD_METAL_DETECTOR_DIALOGUE_BE_TRIGGERED = FALSE")
			bShouldTrigger = FALSE
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC


FUNC BOOL IS_DIALOGUE_USING_DISGUISE_CHECK(INT iDialogue)
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_TriggerBasedOnDisguise)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL IS_DIALOGUE_ABLE_TO_TRIGGER_FOR_PLAYER_DISGUISE(INT iDialogue)
	
	//If we are not using costume checks, immediately pass this bool check.
	IF NOT IS_DIALOGUE_USING_DISGUISE_CHECK(iDialogue)	
		RETURN TRUE
	ENDIF
	
	//TODO: Temp outfit names - update when correct ones are available.
	 
	//CASINO_HEIST_OUTFIT_IN__BUGSTAR 
	IF IS_LOCAL_PLAYER_WEARING_A_BUGSTAR_OUTFIT()
	AND IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_TriggerBasedOnDisguise_Bugstar)
		PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - IS_DIALOGUE_ABLE_TO_TRIGGER_FOR_PLAYER_DISGUISE() - Wearing bugstar disguise")			
		RETURN TRUE
	ENDIF
	
	// CASINO_HEIST_OUTFIT_IN__MECHANIC       
	IF IS_LOCAL_PLAYER_WEARING_A_MAINTENANCE_OUTFIT()
	AND IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_TriggerBasedOnDisguise_Mechanic)
		PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - IS_DIALOGUE_ABLE_TO_TRIGGER_FOR_PLAYER_DISGUISE() - Wearing  mechanic disguise")			
		RETURN TRUE
	ENDIF
	
	//  CASINO_HEIST_OUTFIT_IN__GRUPPE_SECHS
	IF IS_LOCAL_PLAYER_WEARING_A_GRUPPE_SECHS_OUTFIT()
	AND IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_TriggerBasedOnDisguise_GruppeSechs)
		PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - IS_DIALOGUE_ABLE_TO_TRIGGER_FOR_PLAYER_DISGUISE() - Wearing Gruppe Sechs disguise")			
		RETURN TRUE
	ENDIF
	
	//CASINO_HEIST_OUTFIT_IN__CELEBRITY
	IF IS_LOCAL_PLAYER_WEARING_A_CELEB_OUTFIT()
	AND IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_TriggerBasedOnDisguise_Brucie)
		PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - IS_DIALOGUE_ABLE_TO_TRIGGER_FOR_PLAYER_DISGUISE() - Wearing Brucie disguise")		
		RETURN TRUE
	ENDIF
	
	// CASINO_HEIST_OUTFIT_OUT__FIREMAN
	IF IS_LOCAL_PLAYER_WEARING_A_FIREMAN_OUTFIT()
	AND IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_TriggerBasedOnDisguise_Fireman)
		PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - IS_DIALOGUE_ABLE_TO_TRIGGER_FOR_PLAYER_DISGUISE() - Wearing Fireman disguise")		
		RETURN TRUE
	ENDIF
	
	// CASINO_HEIST_OUTFIT_OUT__HIGH_ROLLER
	IF IS_LOCAL_PLAYER_WEARING_A_HIGH_ROLLER_OUTFIT()
	AND IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_TriggerBasedOnDisguise_HighRoller)
		PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - IS_DIALOGUE_ABLE_TO_TRIGGER_FOR_PLAYER_DISGUISE() - Wearing High Roller disguise")		
		RETURN TRUE
	ENDIF
	
	//CASINO_HEIST_OUTFIT_OUT__SWAT
	IF IS_LOCAL_PLAYER_WEARING_A_NOOSE_OUTFIT()
	AND IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_TriggerBasedOnDisguise_Noose)
		PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - IS_DIALOGUE_ABLE_TO_TRIGGER_FOR_PLAYER_COSTUME() - Wearing Noose disguise")		
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PLAYERS_AGGROED_TRIGGER_DIALOGUE(INT iDialogue)
	
	BOOL bShouldTrigger = TRUE

	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_RequiresPlayersToHaveTriggeredAggroed)
		PRINTLN("[LM]IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM  - SHOULD_PLAYERS_AGGROED_TRIGGER_DIALOGUE - Checking if players do have aggro in order to trigger dialogue.")
		IF IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_0+g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTeam)
			PRINTLN("[LM] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - SHOULD_PLAYERS_AGGROED_TRIGGER_DIALOGUE = TRUE (2)")
			bShouldTrigger = TRUE
		ELSE
			bShouldTrigger = FALSE
		ENDIF
	ELIF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_RequiresPlayersToNotHaveTriggeredAggroed)
		PRINTLN("[LM] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM- SHOULD_PLAYERS_AGGROED_TRIGGER_DIALOGUE - Checking if players do not have aggro in order to trigger dialogue.")		
		IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_0+g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTeam)
			PRINTLN("[LM] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - SHOULD_PLAYERS_AGGROED_TRIGGER_DIALOGUE = TRUE (2)")
			bShouldTrigger = TRUE
		ELSE
			bShouldTrigger = FALSE
		ENDIF	
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_PLAYER_USING_ELEVATOR(INT iDialogue)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_RequiresPlayerUsedElevator)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_USED_ELEVATOR)
		PRINTLN("[LM] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - SHOULD_DIALOGUE_TRIGGER_DUE_TO_PLAYER_USING_ELEVATOR - Used elevator")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_COP_DECOY_NEAR_END(INT iDialogue)
	
	BOOL bShouldTrigger = TRUE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_TriggerCopDecoyNearEnd)
		bShouldTrigger = FALSE 
		IF IS_BIT_SET(iCopDecoyBS, ci_COP_DECOY_NEAR_END_DIA_TRIGGER)
			PRINTLN("[NA] SHOULD_DIALOGUE_TRIGGER_DUE_TO_COP_DECOY_NEAR_END Returning True.")
			bShouldTrigger = TRUE
		ENDIF
	ENDIF
		
	RETURN bShouldTrigger

ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_COP_DECOY_ENDED(INT iDialogue)
	
	BOOL bShouldTrigger = TRUE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_TriggerCopDecoyEnded)
		bShouldTrigger = FALSE 
		IF IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_COP_DECOY_EXPIRED)
			PRINTLN("[NA] SHOULD_DIALOGUE_TRIGGER_DUE_TO_COP_DECOY_ENDED Returning True.")
			bShouldTrigger = TRUE
		ENDIF
	ENDIF
		
	RETURN bShouldTrigger

ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_COP_DECOY_FLED(INT iDialogue)
	
	BOOL bShouldTrigger = TRUE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_TriggerCopDecoyFled)
		bShouldTrigger = FALSE 
		IF IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_COP_DECOY_FLEEING)
			PRINTLN("[NA] SHOULD_DIALOGUE_TRIGGER_DUE_TO_COP_DECOY_FLED Returning True.")
			bShouldTrigger = TRUE
		ENDIF
	ENDIF
		
	RETURN bShouldTrigger

ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_ENTITY_EXISTING(INT iDialogue)
	
	BOOL bShouldTrigger = TRUE
	BOOL bCheckDoesNotExist = IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_EntityDoesNotExist)
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iEntityExistsIndex != -1		
		SWITCH	g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iEntityExistsType
			CASE ciENTITY_EXISTS_DT_PEDS
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iEntityExistsIndex])
					IF NOT bCheckDoesNotExist
						bShouldTrigger = FALSE
					ENDIF
				ELIF bCheckDoesNotExist
					bShouldTrigger = FALSE
				ENDIF
			BREAK
			CASE ciENTITY_EXISTS_DT_VEHICLES
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iEntityExistsIndex])
					IF NOT bCheckDoesNotExist
						bShouldTrigger = FALSE
					ENDIF
				ELIF bCheckDoesNotExist
					bShouldTrigger = FALSE
				ENDIF
			BREAK
			CASE ciENTITY_EXISTS_DT_OBJECTS
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iEntityExistsIndex])
					IF NOT bCheckDoesNotExist
						bShouldTrigger = FALSE
					ENDIF
				ELIF bCheckDoesNotExist
					bShouldTrigger = FALSE
				ENDIF
			BREAK			
			CASE ciENTITY_EXISTS_DT_PROPS
				IF NOT DOES_ENTITY_EXIST(oiProps[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iEntityExistsIndex])	
					IF NOT bCheckDoesNotExist
						bShouldTrigger = FALSE
					ENDIF
				ELIF bCheckDoesNotExist
					bShouldTrigger = FALSE
				ENDIF
			BREAK
			CASE ciENTITY_EXISTS_DT_WEAPONS
				IF NOT DOES_PICKUP_EXIST(pipickup[g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iEntityExistsIndex])
					IF NOT bCheckDoesNotExist
						bShouldTrigger = FALSE
					ENDIF
				ELIF bCheckDoesNotExist
					bShouldTrigger = FALSE
				ENDIF
			BREAK	
		ENDSWITCH
	ENDIF

	RETURN bShouldTrigger

ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_BEING_GANG_LEADER(INT iDialogue)
	
	BOOL bShouldTrigger = TRUE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_GangBossOnly)
		IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
			// All good!
			PRINTLN("IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - We are the gang boss!")
		ELSE
			bShouldTrigger = FALSE
			PRINTLN("IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - We're not the gang boss!")
		ENDIF
	ENDIF
		
	RETURN bShouldTrigger

ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_CASH_GRAB_TOTAL_TAKE_LIMITS(INT iDialogue)
	
	BOOL bShouldTrigger = TRUE
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iCashGrabLowerLimit > 0
		IF MC_ServerBD.iCashGrabTotalTake < g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iCashGrabLowerLimit
			PRINTLN("[NA] SHOULD_DIALOGUE_TRIGGER_DUE_TO_CASH_GRAB_TOTAL_TAKE_LIMITS - Total cash grab is less than the lower limit. Return FALSE")
			bShouldTrigger = FALSE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iCashGrabUpperLimit > 0
		IF MC_ServerBD.iCashGrabTotalTake > g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iCashGrabUpperLimit
			PRINTLN("[NA] SHOULD_DIALOGUE_TRIGGER_DUE_TO_CASH_GRAB_TOTAL_TAKE_LIMITS - Total cash grab is more than the upper limit. Return FALSE")
			bShouldTrigger = FALSE
		ENDIF
	ENDIF
		
	RETURN bShouldTrigger

ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_VAULT_DRILL_BREAK_LOCK(INT iDialogue)
	
	BOOL bShouldTrigger = TRUE
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDrillBreakLock > -1
	AND g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDrillBreakLockObjIndex > -1
		bShouldTrigger = FALSE
		
		IF MC_playerBD[iLocalPart].iObjHacking = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDrillBreakLockObjIndex
			IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDrillBreakLock > 0
				IF sVaultDrillData.iCurrentObstruction = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDrillBreakLock
					PRINTLN("[NA] SHOULD_DIALOGUE_TRIGGER_DUE_TO_VAULT_DRILL_BREAK_LOCK - Return TRUE - Player broke lock: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDrillBreakLock)
					bShouldTrigger = TRUE
				ENDIF
			ELSE //Check for final lock/obstruction.
				IF sVaultDrillData.iCurrentObstruction = sVaultDrillData.iObstructions
					PRINTLN("[NA] SHOULD_DIALOGUE_TRIGGER_DUE_TO_VAULT_DRILL_BREAK_LOCK - Return TRUE - Player broke the final lock which was lock: ", sVaultDrillData.iObstructions)
					bShouldTrigger = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	RETURN bShouldTrigger

ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_VAULT_DRILL_OVERHEAT(INT iDialogue)
	
	BOOL bShouldTrigger = TRUE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_TriggerDrillOverheated)
		bShouldTrigger = FALSE

		IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_DID_PLAYER_MESS_UP)
			PRINTLN("[NA] SHOULD_DIALOGUE_TRIGGER_DUE_TO_VAULT_DRILL_OVERHEAT - Return TRUE - Player overheated drill.")
			bShouldTrigger = TRUE
		ENDIF
	ENDIF
		
	RETURN bShouldTrigger

ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_THERMAL_CHARGE_CONDITIONS(INT iDialogue)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_ThermiteEmpty)
		
		INT iTeam = MC_playerBD[iPartToUse].iteam
		INT iChargesRemaining
	
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset4, ciBS4_TEAM_PER_PLAYER_THERMAL_CHARGES)
			iChargesRemaining = MC_playerBD_1[iPartToUse].iThermitePerPlayerRemaining
		ELSE
			iChargesRemaining = MC_serverBD_1.iTeamThermalCharges[iTeam]
		ENDIF
		
		IF iChargesRemaining = 0
			PRINTLN("[NA] SHOULD_DIALOGUE_TRIGGER_DUE_TO_THERMAL_CHARGE_CONDITIONS - Return TRUE - Player has no more thermal charges.")
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_ThermiteRemaining)
		
		INT iTeam = MC_playerBD[iPartToUse].iteam
		INT iChargesRemaining
	
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset4, ciBS4_TEAM_PER_PLAYER_THERMAL_CHARGES)
			iChargesRemaining = MC_playerBD_1[iPartToUse].iThermitePerPlayerRemaining
		ELSE
			iChargesRemaining = MC_serverBD_1.iTeamThermalCharges[iTeam]
		ENDIF
		
		IF iChargesRemaining > 0
			PRINTLN("[NA] SHOULD_DIALOGUE_TRIGGER_DUE_TO_THERMAL_CHARGE_CONDITIONS - Return TRUE - Player has thermal charges remaining.")
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_OBJECT_CONTINUITY(INT iDialogue)
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iLinkedObjectContinuityId = -1
		//Not set
		RETURN TRUE
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_ObjectTracking)
		PRINTLN("[JS] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_OBJECT_CONTINUITY - Dialogue ", iDialogue, " can never trigger - object continuity set but tracking off!")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_LONG_BIT_SET(MC_serverBD_1.sLEGACYMissionContinuityVars.iObjectTrackingBitset, g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iLinkedObjectContinuityId)
		PRINTLN("[JS] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_OBJECT_CONTINUITY - Dialogue ", iDialogue, " waiting for linked continuity ID to pass")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC
FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_LOCATION_CONTINUITY(INT iDialogue)
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iLinkedLocationContinuityId = -1
		//Not set
		RETURN TRUE
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_LocationTracking)
		PRINTLN("[JS] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_LOCATION_CONTINUITY - Dialogue ", iDialogue, " can never trigger - location continuity set but tracking off!")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(MC_serverBD_1.sLEGACYMissionContinuityVars.iLocationTrackingBitset, g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iLinkedLocationContinuityId)
		PRINTLN("[JS] - SHOULD_DIALOGUE_TRIGGER_DUE_TO_LOCATION_CONTINUITY - Dialogue ", iDialogue, " waiting for linked continuity ID to pass")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_CONTINUITY(INT iDialogue)
	
	IF NOT SHOULD_DIALOGUE_TRIGGER_DUE_TO_OBJECT_CONTINUITY(iDialogue)
		RETURN FALSE
	ENDIF
	
	IF NOT SHOULD_DIALOGUE_TRIGGER_DUE_TO_LOCATION_CONTINUITY(iDialogue)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL SHOULD_PED_VOICE_INDEX_HAS_LOS_WITH_PLAYER_TRIGGER_DIALOGUE(INT iDialogue)
	
	BOOL bShouldTrigger = TRUE

	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_RequiresLOSWithAPlayerPed)
		PRINTLN("[LM] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - SHOULD_PED_VOICE_INDEX_HAS_LOS_WITH_PLAYER_TRIGGER_DIALOGUE - Checking if players do have aggro in order to trigger dialogue.")
		
		PED_INDEX pedToCheck
		INT iPedVoice = -1
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPedVoice > -1
			iPedVoice = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPedVoice
		ENDIF
		
		IF iPedVoice > -1		
			IF NOT IS_PED_INJURED(PlayerPedToUse)
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iPedVoice])					
					pedToCheck = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPedVoice])
					
					IF NOT IS_PED_INJURED(pedToCheck)
						PRINTLN("[LM] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM SHOULD_PED_VOICE_INDEX_HAS_LOS_WITH_PLAYER_TRIGGER_DIALOGUE - iPed: ", iPedVoice, " Is alive")	
						
						IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(pedToCheck, PlayerPedToUse)
						AND VDIST2(GET_ENTITY_COORDS(pedToCheck), GET_ENTITY_COORDS(PlayerPedToUse)) < POW(2.0, g_FMMC_STRUCT.sDialogueTriggers[iDialogue].fRadius)
							PRINTLN("[LM] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - SHOULD_PED_VOICE_INDEX_HAS_LOS_WITH_PLAYER_TRIGGER_DIALOGUE = TRUE")
							bShouldTrigger = TRUE
						ELSE
							PRINTLN("[LM] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - SHOULD_PED_VOICE_INDEX_HAS_LOS_WITH_PLAYER_TRIGGER_DIALOGUE = FALSE")
							bShouldTrigger = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_WAIT_FOR_MORE_CCTVS_TO_BE_DESTROYED(INT iDialogue)
	
	BOOL bShouldTrigger = TRUE
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iCCTVsDestroyed > 0
		IF MC_serverBD_2.iCamObjDestroyed >= g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iCCTVsDestroyed
			// Destroyed enough cameras!
			bShouldTrigger = TRUE
			PRINTLN("[RCC MISSION][CCTV_Dialogue] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - SHOULD_DIALOGUE_TRIGGER_WAIT_FOR_MORE_CCTVS_TO_BE_DESTROYED - Enough CCTVs have been destroyed!")
		ELSE
			bShouldTrigger = FALSE
			PRINTLN("[RCC MISSION][CCTV_Dialogue] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - SHOULD_DIALOGUE_TRIGGER_WAIT_FOR_MORE_CCTVS_TO_BE_DESTROYED - Players have only destroyed ", MC_serverBD_2.iCamObjDestroyed, " / ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iCCTVsDestroyed)
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_WAIT_FOR_A_CCTV_TO_BE_TASERED(INT iDialogue)
	BOOL bShouldTrigger = TRUE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_RequireCCTVHitByStunGun)
		IF iCCTVCamsCurrentlyTasered > 0
			bShouldTrigger = TRUE
			PRINTLN("[RCC MISSION][CCTV_Dialogue] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - SHOULD_DIALOGUE_TRIGGER_WAIT_FOR_A_CCTV_TO_BE_TASERED - A CCTV has been tasered!")
		ELSE
			bShouldTrigger = FALSE
			PRINTLN("[RCC MISSION][CCTV_Dialogue] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - SHOULD_DIALOGUE_TRIGGER_WAIT_FOR_A_CCTV_TO_BE_TASERED - Waiting for a CCTV to be tasered")
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_WAIT_FOR_A_PLAYER_TO_ALERT_A_CCTV(INT iDialogue)
	BOOL bShouldTrigger = TRUE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_RequirePlayerToBeSpottedByCCTV)
		IF IS_BIT_SET(MC_ServerBD.iServerBitSet6, SBBOOL6_PLAYER_HAS_BEEN_SPOTTED_CCTV)
			bShouldTrigger = TRUE
			PRINTLN("[RCC MISSION][CCTV_Dialogue] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - SHOULD_DIALOGUE_TRIGGER_WAIT_FOR_A_PLAYER_TO_ALERT_A_CCTV - A CCTV has caught a player!")
		ELSE
			bShouldTrigger = FALSE
			PRINTLN("[RCC MISSION][CCTV_Dialogue] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - SHOULD_DIALOGUE_TRIGGER_WAIT_FOR_A_PLAYER_TO_ALERT_A_CCTV - Waiting for a CCTV to catch a player")
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_WAIT_FOR_A_FUSEBOX_TO_BE_TASERED(INT iDialogue)
	BOOL bShouldTrigger = TRUE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_RequireFuseboxHitByStunGun)
		IF iFuseBoxesCurrentlyTasered > 0
			bShouldTrigger = TRUE
			PRINTLN("[RCC MISSION][CCTV_Dialogue] SHOULD_DIALOGUE_TRIGGER_WAIT_FOR_A_FUSEBOX_TO_BE_TASERED - A Fusebox has been tasered!")
		ELSE
			bShouldTrigger = FALSE
			PRINTLN("[RCC MISSION][CCTV_Dialogue] SHOULD_DIALOGUE_TRIGGER_WAIT_FOR_A_FUSEBOX_TO_BE_TASERED - Waiting for a Fusebox to be tasered")
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_DUE_TO_ZONE_CONDITION(INT iDialogue)
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iZoneIndex > -1
		
		INT iZone = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iZoneIndex
		
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iZoneTimer_TimeRemaining > 0
			IF IS_ZONE_TIMER_RUNNING(iZone)
				IF GET_ZONE_TIMER_TIME_REMAINING(iZone, TRUE) <= g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iZoneTimer_TimeRemaining
				AND GET_ZONE_TIMER_TIME_REMAINING(iZone, TRUE) > g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iZoneTimer_TimeRemaining - 5
					// In the time window!
				ELSE
					PRINTLN("SHOULD_DIALOGUE_TRIGGER_DUE_TO_ZONE_CONDITION - iDialogue: ", iDialogue, " / iZone: ", iZone, " || Not in window for iZoneTimer_TimeRemaining || GET_ZONE_TIMER_TIME_REMAINING(iZone, TRUE): ", GET_ZONE_TIMER_TIME_REMAINING(iZone, TRUE))
					RETURN FALSE
				ENDIF
			ELSE
				RETURN FALSE
			ENDIF
			
		ELIF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iZoneTimer_TimeRemaining = 0
			IF NOT HAS_ZONE_TIMER_COMPLETED(iZone)
				PRINTLN("SHOULD_DIALOGUE_TRIGGER_DUE_TO_ZONE_CONDITION - iDialogue: ", iDialogue, " / iZone: ", iZone, " || Zone Timer hasn't completed")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_PLAY_FOR_KILL_COUNT(INT iDialogue)
	BOOL bShouldTrigger = TRUE	
	
	INT iTeam = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTeam
	
	IF iTeam > -1
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		IF iRule < FMMC_MAX_RULES
			
			IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPedsKillRequired > 0
			AND MC_serverBD_4.iCurrentHighestPriority[iTeam] >= g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iRule
			
				IF NOT IS_BIT_SET(iDialogueKillsThisRuleSet, iRule)
					iDialogueKillsThisRule[iRule] = MC_ServerBD.iTeamKills[iTeam]
					SET_BIT(iDialogueKillsThisRuleSet, iRule)
				ENDIF
				
				INT iKillsRequired = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iPedsKillRequired
				INT iKills = (MC_ServerBD.iTeamKills[iTeam] - iDialogueKillsThisRule[iRule])
				
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset, iBS_DialogueTriggerLater)
					IF iRule != g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iRule
						bShouldTrigger = FALSE
						PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - SHOULD_DIALOGUE_TRIGGER_PLAY_FOR_KILL_COUNT - iTeam:  ", iTeam, " iBS_DialogueTriggerLater NOT set and is not on the required rule.")
					ENDIF
				ENDIF
				
				IF iKillsRequired > 0
					IF iKills < iKillsRequired
						bShouldTrigger = FALSE
						PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - SHOULD_DIALOGUE_TRIGGER_PLAY_FOR_KILL_COUNT - iTeam:  ", iTeam, " has ", iKills, " Kills, out of a required: ", iKillsRequired)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_PLAY_FOR_KILL_COUNT_CIVILLIANS(INT iDialogue)
	BOOL bShouldTrigger = TRUE	
	INT iTeam = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iTeam	
	IF iTeam > -1
		INT iKillsRequired = g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iCivillianKillRequired
		INT iKills = MC_ServerBD.iTeamCivillianKills[iTeam]
		IF iKillsRequired > -1
			IF iKills < iKillsRequired
				PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - SHOULD_DIALOGUE_TRIGGER_PLAY_FOR_KILL_COUNT_CIVILLIANS - iTeam:  ", iTeam, " has ", iKills, " Kills, out of a required: ", iKillsRequired)
				bShouldTrigger = FALSE
			ENDIF
		ENDIF
	ENDIF
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_REQUIRE_PHONE_EMP_TO_BE_AVAILABLE(INT iDialogue)
	BOOL bShouldTrigger = TRUE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_RequireEMPToBeAvailable)
		IF IS_PHONE_EMP_AVAILABLE(FALSE)
		AND NOT HAS_PHONE_EMP_BEEN_USED()
			bShouldTrigger = TRUE
			PRINTLN("[RCC MISSION][PhoneEMP_Dialogue] SHOULD_DIALOGUE_TRIGGER_REQUIRE_PHONE_EMP_TO_BE_AVAILABLE - Available!")
		ELSE
			bShouldTrigger = FALSE
			PRINTLN("[RCC MISSION][PhoneEMP_Dialogue] SHOULD_DIALOGUE_TRIGGER_REQUIRE_PHONE_EMP_TO_BE_AVAILABLE - Either not available or already used")
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_REQUIRE_PHONE_EMP_NOT_FIRED(INT iDialogue)
	BOOL bShouldTrigger = TRUE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_RequirePhoneEMPNotUsed)
		IF HAS_PHONE_EMP_BEEN_USED()
			bShouldTrigger = FALSE
			PRINTLN("[RCC MISSION][PhoneEMP_Dialogue] SHOULD_DIALOGUE_TRIGGER_REQUIRE_PHONE_EMP_NOT_FIRED - Phone EMP has been used! Returning FALSE")
		ELSE
			bShouldTrigger = TRUE
			PRINTLN("[RCC MISSION][PhoneEMP_Dialogue] SHOULD_DIALOGUE_TRIGGER_REQUIRE_PHONE_EMP_NOT_FIRED - Not used the Phone EMP yet")
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_REQUIRE_DOOR_UNLOCKED_WITH_KEYCARD(INT iDialogue)
	BOOL bShouldTrigger = TRUE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_RequireKeycardUsage)
		IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_UNLOCKED_DOOR_WITH_SWIPECARD)
			bShouldTrigger = TRUE
			PRINTLN("[RCC MISSION][InteractWith_Dialogue] SHOULD_DIALOGUE_TRIGGER_REQUIRE_DOOR_UNLOCKED_WITH_KEYCARD - LBOOL32_UNLOCKED_DOOR_WITH_SWIPECARD is set!")
		ELSE
			bShouldTrigger = FALSE
			PRINTLN("[RCC MISSION][InteractWith_Dialogue] SHOULD_DIALOGUE_TRIGGER_REQUIRE_DOOR_UNLOCKED_WITH_KEYCARD - Not swiped a door yet")
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL SHOULD_TRIGGER_ON_PED_TASED(INT iDialogue)
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_RequirePedTased)
		//Waiting for trigger
		IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_A_PED_HAS_BEEN_STUNNED)
			RETURN FALSE
		ENDIF
		
		//Blocking
		IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_BlockIfPedHasBeenTased)
		AND IS_BIT_SET(iDialogueBlockedConditionBS, ciDT_Blocked_PedHasBeenTased)
			RETURN FALSE
		ENDIF
		
		IF NOT IS_BIT_SET(iDialogueBlockedConditionBS, ciDT_Blocked_PedHasBeenTased)
			SET_BIT(iDialogueBlockedConditionBS, ciDT_Blocked_PedHasBeenTased)
			PRINTLN("[DialogueTrigger] SHOULD_TRIGGER_ON_PED_TASED - Setting ciDT_Blocked_PedHasBeenTased")
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC
FUNC BOOL SHOULD_TRIGGER_ON_PED_TRANQUILIZED(INT iDialogue)
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_RequirePedTranquilized)
		
		//Waiting for trigger
		IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_A_PED_HAS_BEEN_TRANQUILIZED)
			RETURN FALSE
		ENDIF
		
		//Blocking
		IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset4, iBS_Dialogue4_BlockIfPedHasBeenTranquilized)
		AND IS_BIT_SET(iDialogueBlockedConditionBS, ciDT_Blocked_PedHasBeenTranquilized)
			RETURN FALSE
		ENDIF
		
		IF NOT IS_BIT_SET(iDialogueBlockedConditionBS, ciDT_Blocked_PedHasBeenTranquilized)
			SET_BIT(iDialogueBlockedConditionBS, ciDT_Blocked_PedHasBeenTranquilized)
			PRINTLN("[DialogueTrigger] SHOULD_TRIGGER_ON_PED_TRANQUILIZED - Setting ciDT_Blocked_PedHasBeenTranquilized")
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DIALOGUE_TRIGGER_WORK_USING_DIALOGUE_TRIGGER_BLOCKERS(INT iDialogue)
	BOOL bShouldTrigger = TRUE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset5, iBS_Dialogue5_BlockWithTriggerBlockerList)
		IF IS_BIT_SET(iLocalDialogueBlockedBS[iDialogue/32], iDialogue%32)
			bShouldTrigger = FALSE
			PRINTLN("[DialogueTrigger] SHOULD_DIALOGUE_TRIGGER_WORK_USING_DIALOGUE_TRIGGER_BLOCKERS - iDialogue: ", iDialogue, " Has been blocked - bShouldTrigger = FALSE")
		ELSE
			PRINTLN("[DialogueTrigger] SHOULD_DIALOGUE_TRIGGER_WORK_USING_DIALOGUE_TRIGGER_BLOCKERS - iDialogue: ", iDialogue, " Has not been blocked yet.")
		ENDIF
	ENDIF
	
	RETURN bShouldTrigger
ENDFUNC

FUNC BOOL IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM(INT iTeam)

	BOOL bReturn = FALSE

	IF NOT IS_BIT_SET( MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_FINISHED)	
	AND NOT IS_BIT_SET( MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_START_SPECTATOR)
		IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
		AND NOT HAS_TEAM_FAILED(MC_playerBD[iPartToUse].iteam)
			INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
			INT iTriggerRule = g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iRule
			BOOL bIgnoreRule = FALSE
			
			PRINTLN("[DialogueTrigger] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM Starting Checks... iDialogueProgress: ", iDialogueProgress, " iRule: ", iRule, " iTriggerRule: ", iTriggerRule)
			
			IF iTriggerRule = -1
				RETURN FALSE
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset3, iBS_Dialogue3_IgnoreRule)
				PRINTLN("IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - iBS_Dialogue3_IgnoreRule is set, ignoring rule checks")
				bIgnoreRule = TRUE
			ENDIF
			
			BOOL bTriggerThisDialogueLater = IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset,iBS_DialogueTriggerLater)
			BOOL bTriggerPlayerCountReqMet = TRUE

			INT iTotalPlayers = MC_ServerBD.iNumberOfPlayingPlayers[0] + MC_ServerBD.iNumberOfPlayingPlayers[1] + MC_ServerBD.iNumberOfPlayingPlayers[2] + MC_ServerBD.iNumberOfPlayingPlayers[3]
			IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPlayerCountRequirement > 0
				IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPlayerCountRequirement != iTotalPlayers
					bTriggerPlayerCountReqMet = FALSE
				ENDIF
			ENDIF
			
			IF MC_serverBD_4.iPlayThisAudioTriggerASAP_SERVER = iDialogueProgress		
				bReturn = TRUE
				PRINTLN("[RCC MISSION][DialogueTrigger] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - Marking dialogue trigger as valid because of iPlayThisAudioTriggerASAP_SERVER")
					
				IF bIsLocalPlayerHost
					MC_serverBD_4.iPlayThisAudioTriggerASAP_SERVER = -2
					PRINTLN("[DialogueTrigger] Resetting iPlayThisAudioTriggerASAP_SERVER")
				ENDIF
				
				RETURN TRUE
			ENDIF
			
			IF bTriggerPlayerCountReqMet 
				IF ( NOT bTriggerThisDialogueLater AND iRule = iTriggerRule )
				OR ( bTriggerThisDialogueLater AND iRule >= iTriggerRule )
				OR ( CAN_VEHICLE_DIAGLOUE_TRIGGER_ON_ANY_RULE(iDialogueProgress))
					
					PRINTLN("[DialogueTrigger] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM bTriggerThisDialogueLater: ", bTriggerThisDialogueLater)
					
					IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iTeamRuleLimit[MC_playerBD[iPartToUse].iteam] = -1
					OR MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] <= g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iTeamRuleLimit[MC_playerBD[iPartToUse].iteam]			
					
						// [FIX_2020_CONTROLLER] We should break this up a bit so we're not calling so many functions. This is a lot to process.
						//SHELVED - CL 23948032
						IF (NOT IS_CUTSCENE_PLAYING())
						AND (NOT IS_TRIP_SKIP_IN_PROGRESS(MC_playerBD[NATIVE_TO_INT(LocalPlayer)].TSPlayerData))
						//Individual dialogue checks:
						AND NOT IS_BLOCKING_IF_WANTED(iDialogueProgress)
						AND HAS_ANY_PREREQUISITE_DIALOGUE_PLAYED(iDialogueProgress)
						AND HAS_A_WANTED_CHANGE_OCCURRED_THAT_TRIGGERS_THIS_DIALOGUE(iDialogueProgress) // Checks for triggering on losing/gaining wanted
						AND HAS_PLAYER_LEFT_OR_ENTERED_VEHICLE(iDialogueProgress)
						AND HAS_PED_LEFT_OR_ENTERED_VEHICLE(iDialogueProgress)
						AND IS_PLAYER_NEAR_STARTING_APARTMENT(iDialogueProgress)
						AND HAS_VEHICLE_BEEN_DAMAGED_PAST_THRESHOLD(iDialogueProgress)
						AND HAS_PHONE_HACKING_MINIGAME_BEEN_COMPLETED(iDialogueProgress)
						AND HAVE_PEDS_BEEN_KILLED_DURING_CROWD_CONTROL(iDialogueProgress)
						AND IS_PLAYER_WEARING_MASK_TO_TRIGGER_DIALOGUE(iDialogueProgress)
						AND IS_PLAYER_IN_FIRST_PERSON(iDialogueProgress)
						AND IS_PLAYER_ENTERING_ANY_VEHICLE_FOR_DIALOGUE(iDialogueProgress)						
						AND IS_PED_IN_RANGE_FOR_DIALOGUE_TRIGGER(iDialogueProgress)
						AND IS_DIALOGUE_TRIGGERED_SD(iDialogueProgress)
						AND IS_ENOUGH_GANG_CHASE_UNITS_DEAD_TO_TRIGGER_DIALOGUE(iDialogueProgress)
						AND IS_PLAYER_VEH_UNDERWATER_TO_TRIGGER_DIALOGUE(iDialogueProgress)
						AND HAS_PLAYER_HIT_FIREWALL_TO_TRIGGER_DIALOGUE(iDialogueProgress)
						AND HAS_PLAYER_HIT_PACKET_TO_TRIGGER_DIALOGUE(iDialogueProgress)
						AND HAS_PLAYER_HACKED_VEHICLE_TO_TRIGGER_DIALOGUE(iDialogueProgress)
						AND HAS_PLAYER_STARTED_HOTWIRE_TRIGGER_DIALOGUE(iDialogueProgress)
						AND HAS_PED_DIED_TO_TRIGGER_DIALOGUE(iDialogueProgress)
						AND IS_PLAYER_VEH_BACK_OUT_OF_WATER_TO_TRIGGER_DIALOGUE(iDialogueProgress)
						AND HAS_PLAYER_ACTIVATED_THERMAL_VISION(iDialogueProgress)
						AND HAS_PLAYER_GOT_CLOSE_ENOUGH_TO_WATER_TO_TRIGGER_DIALOGUE(iDialogueProgress)
						AND HAVE_ENOUGH_FIRES_BEEN_EXTINGUISHED(iDialogueProgress)
						AND HAS_PLAYER_STARTED_BEAMHACK_TRIGGER_DIALOGUE(iDialogueProgress)
						AND HAS_PLAYER_STARTED_FINGERPRINT_CLONE_TRIGGER_DIALOGUE(iDialogueProgress)
						AND HAS_PLAYER_STARTED_ORDER_UNLOCK_TRIGGER_DIALOGUE(iDialogueProgress)
						AND HAS_PLAYER_PASSED_HACKING_PATTERN_TO_TRIGGER_DIALOGUE(iDialogueProgress)
						AND HAS_PLAYER_FAILED_HACKING_PATTERN_TO_TRIGGER_DIALOGUE(iDialogueProgress)
						AND HAS_VEHICLE_BOMB_BAY_DOORS_CLOSED(iDialogueProgress)
						AND HAS_MULTIRULE_TIMER_REACHED_TRIGGER_TIME(iDialogueProgress)
						AND IS_PLAYER_FLYING_TOO_HIGH_TO_TRIGGER_DIALOGUE(iDialogueProgress)
						AND IS_PLAYER_SPAWNING_GANG_CHASE_DUE_TO_FLYING_TOO_HIGH(iDialogueProgress)
						AND HAS_NOT_REACHED_MIDPOINT_YET_TRIGGER_DIALOGUE(iDialogueProgress)
						AND HAS_PLAYERS_NOT_IN_SEPARATE_OBJECTIVE_VEHICLES_TRIGGER_DIALOGUE(iDialogueProgress)
						AND NOT_BLOCKED_DUE_TO_BEING_SPECTATOR(iDialogueProgress)
						AND NOT_BLOCKED_DUE_TO_BEING_ANY_KIND_OF_SPECTATOR(iDialogueProgress)
						AND NOT_BLOCKED_DUE_TO_NOT_CARRYING_OBJ_VEH_WITH_CARGOBOB(iDialogueProgress)
						AND IS_SPECIFIC_VEHICLE_OPTION_INSIDE_VEHICLE_VALID(iDialogueProgress)
						AND IS_SPECIFIC_VEHICLE_OPTION_OWNS_VEHICLE_VALID(iDialogueProgress)
						AND IS_PLAYER_IN_VEHICLE_OF_PED_VOICE(iDialogueProgress)
						AND HAS_PED_BEEN_DAMAGED_PAST_THRESHOLD(iDialogueProgress)	
						AND SHOULD_VEHICLE_REPAIR_DIALOGUE_BE_TRIGGERED(iDialogueProgress)
						AND SHOULD_ENTERED_SWITCH_VEHICLE_UNSEEN_DIALOGUE_BE_TRIGGERED(iDialogueProgress)
						AND SHOULD_ENTERED_SWITCH_VEHICLE_SEEN_DIALOGUE_BE_TRIGGERED(iDialogueProgress)
						AND SHOULD_NEAR_SWITCH_VEHICLE_DIALOGUE_BE_TRIGGERED(iDialogueProgress)
						AND SHOULD_ENTERED_GETAWAY_VEHICLE_DIALOGUE_BE_TRIGGERED(iDialogueProgress)
						AND SHOULD_EMP_DIALOGUE_BE_TRIGGERED(iDialogueProgress)
						AND SHOULD_METAL_DETECTOR_DIALOGUE_BE_TRIGGERED(iDialogueProgress)
						AND IS_DIALOGUE_ABLE_TO_TRIGGER_FOR_PLAYER_DISGUISE(iDialogueProgress)	
						AND SHOULD_PLAYERS_AGGROED_TRIGGER_DIALOGUE(iDialogueProgress)
						AND SHOULD_PED_VOICE_INDEX_HAS_LOS_WITH_PLAYER_TRIGGER_DIALOGUE(iDialogueProgress)
						AND SHOULD_DIALOGUE_TRIGGER_WAIT_FOR_MORE_CCTVS_TO_BE_DESTROYED(iDialogueProgress)
						AND SHOULD_DIALOGUE_TRIGGER_WAIT_FOR_A_CCTV_TO_BE_TASERED(iDialogueProgress)
						AND SHOULD_DIALOGUE_TRIGGER_WAIT_FOR_A_PLAYER_TO_ALERT_A_CCTV(iDialogueProgress)
						AND SHOULD_DIALOGUE_TRIGGER_WAIT_FOR_A_FUSEBOX_TO_BE_TASERED(iDialogueProgress)
						AND SHOULD_DIALOGUE_TRIGGER_PLAY_FOR_KILL_COUNT(iDialogueProgress)
						AND SHOULD_DIALOGUE_TRIGGER_PLAY_FOR_KILL_COUNT_CIVILLIANS(iDialogueProgress)
						AND SHOULD_DIALOGUE_TRIGGER_REQUIRE_PHONE_EMP_TO_BE_AVAILABLE(iDialogueProgress)
						AND SHOULD_DIALOGUE_TRIGGER_REQUIRE_DOOR_UNLOCKED_WITH_KEYCARD(iDialogueProgress)					
						AND SHOULD_TRIGGER_ON_PED_TASED(iDialogueProgress)
						AND SHOULD_TRIGGER_ON_PED_TRANQUILIZED(iDialogueProgress)
						AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_PLAYER_USING_ELEVATOR(iDialogueProgress)
						AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_COP_DECOY_NEAR_END(iDialogueProgress)
						AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_COP_DECOY_ENDED(iDialogueProgress)
						AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_COP_DECOY_FLED(iDialogueProgress)
						AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_ENTITY_EXISTING(iDialogueProgress)	
						AND SHOULD_DIALOGUE_TRIGGER_REQUIRE_PHONE_EMP_NOT_FIRED(iDialogueProgress)
						AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_BEING_GANG_LEADER(iDialogueProgress)
						AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_CASH_GRAB_TOTAL_TAKE_LIMITS(iDialogueProgress)
						AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_ZONE_CONDITION(iDialogueProgress)
						AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_VAULT_DRILL_BREAK_LOCK(iDialogueProgress)
						AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_VAULT_DRILL_OVERHEAT(iDialogueProgress)
						AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_CONTINUITY(iDialogueProgress)
						AND SHOULD_DIALOGUE_TRIGGER_WORK_USING_DIALOGUE_TRIGGER_BLOCKERS(iDialogueProgress)
						AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_PLAYER_IN_ZONE(iDialogueProgress)
						AND SHOULD_DIALOGUE_TRIGGER_DUE_TO_THERMAL_CHARGE_CONDITIONS(iDialogueProgress)
							//PRINTLN("[RCC MISSION][MJM] iDialogueProgress = ",iDialogueProgress," iBS_DialogueRequireObjectivVehicle = ",IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset, iBS_DialogueRequireObjectivVehicle), " vehCarryCount = ",MC_playerBD[iPartToUse].iVehCarryCount," with carrier = ",IS_BIT_SET( MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER )
							// IF we want dialogue to wait until we've collected a vehicle or are with the driver
							IF IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset, iBS_DialogueRequireObjectivVehicle )
							AND IS_PLAYER_IN_VEHICLE_FOR_DIALOGUE(iDialogueProgress)
							OR NOT IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset, iBS_DialogueRequireObjectivVehicle )
								
								INT iCurrentPoints = (MC_serverBD.iTeamScore[iTeam] - iOldDialoguePoints[iTeam])
								
								IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset3, iBS_Dialogue3_UseActualScore)
									iCurrentPoints = MC_serverBD.iTeamScore[iTeam]
								ENDIF
								
								// If we have a radius check
								IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].fRadius != 0.0
									IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iTeam = MC_playerBD[iPartToUse].iteam
									OR g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iTeam = ciDIALOGUE_TRIGGER_TEAM_ANY
										PED_INDEX tempped = PlayerPedToUse
										IF NOT IS_PED_INJURED( tempped )
											IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset,iBS_DialoguePedRange)
												IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPedVoice != -1 
													NETWORK_INDEX netPed = MC_serverBD_1.sFMMC_SBD.niPed[g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPedVoice]
													IF NOT IS_NET_PED_INJURED(netPed)
														
														BOOL bOutsideRadiusSetting = IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset3, iBS_Dialogue3_FireWhenPlayerOutsideRadius)
														BOOL bPlayerWithinRadius = (GET_DISTANCE_BETWEEN_ENTITIES(tempped,NET_TO_ENT(netPed)) <= g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].fRadius)
														
														PRINTLN("[RCC MISSION][MJM] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - distance to ped ",g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPedVoice," = ",GET_DISTANCE_BETWEEN_ENTITIES(tempped,NET_TO_ENT(netPed)),", fRadius ",g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].fRadius,", bOutsideRadiusSetting ",bOutsideRadiusSetting)
														
														IF ((NOT bOutsideRadiusSetting) AND bPlayerWithinRadius)
														OR (bOutsideRadiusSetting AND (NOT bPlayerWithinRadius))
															MC_playerBD[iPartToUse].iNearDialogueTrigger = iDialogueProgress
															PRINTLN("[RCC MISSION][MJM] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - PED MC_playerBD[iPartToUse].iNearDialogueTrigger = iDialogueProgress = ",iDialogueProgress," iPartToUse = ",iPartToUse)
														ENDIF
													ENDIF
												ENDIF
											ELSE
												#IF IS_DEBUG_BUILD
													VECTOR vTempPed = GET_ENTITY_COORDS(tempPed)
													PRINTLN("[RCC MISSION][MJM] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - Dialogue - PlayerPedToUse = ",NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX_FROM_PED(PlayerPedToUse)),", player coord ",vTempPed,", dialogue coord ",g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].vPosition)
												#ENDIF
												
												BOOL bOutsideRadiusSetting = IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset3, iBS_Dialogue3_FireWhenPlayerOutsideRadius)
												BOOL bPlayerWithinRadius = (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(tempped,g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].vPosition) <= g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].fRadius)
													
												PRINTLN("[RCC MISSION][MJM] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - distance to coord ",GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(tempped,g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].vPosition),", fRadius ",g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].fRadius,", bOutsideRadiusSetting ",bOutsideRadiusSetting, " bPlayerWithinRadius: ", bPlayerWithinRadius)
												
												IF ((NOT bOutsideRadiusSetting) AND bPlayerWithinRadius)
												OR (bOutsideRadiusSetting AND (NOT bPlayerWithinRadius))
													MC_playerBD[iPartToUse].iNearDialogueTrigger = iDialogueProgress
													PRINTLN("[RCC MISSION][MJM] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - MC_playerBD[iPartToUse].iNearDialogueTrigger = iDialogueProgress = ",iDialogueProgress," iPartToUse = ",iPartToUse)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									// Trigger on midpoint
									IF IS_BIT_SET(g_FMMC_STRUCT.biDialogueTriggerAtMidPoint[iDialogueProgress/32], iDialogueProgress%32)
										IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam],g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iRule)//MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iTeam])
											IF IS_BIT_SET(MC_serverBD_4.iDialogueRangeBitset[iDialogueProgress/32], iDialogueProgress%32)
											AND (NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset, iBS_DialogueLocalPlayerInRange) 
											OR MC_playerBD[iPartToUse].iNearDialogueTrigger = iDialogueProgress)
												IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPoint > 0
													IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam],g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iRule)//MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iTeam])
													AND iCurrentPoints >= g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPoint
														bReturn = true
														PRINTLN("[RCC MISSION][MJM] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - Dialogue points midpoint range check bReturn = true iDialogueProgress = ",iDialogueProgress)
													ENDIF
												ELSE
													bReturn = true
													PRINTLN("[RCC MISSION][MJM] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - Dialogue midpoint range check bReturn = true iDialogueProgress = ",iDialogueProgress)
												ENDIF
											ENDIF
										ENDIF
									ELSE
										IF IS_BIT_SET(MC_serverBD_4.iDialogueRangeBitset[iDialogueProgress/32], iDialogueProgress%32)
										AND (NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset, iBS_DialogueLocalPlayerInRange) 
										OR MC_playerBD[iPartToUse].iNearDialogueTrigger = iDialogueProgress)
											IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPoint > 0
												IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam],g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iRule)//MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iTeam])
												AND iCurrentPoints >= g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPoint
													bReturn = true
													PRINTLN("[RCC MISSION][MJM] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - Dialogue points range check bReturn = true iDialogueProgress = ",iDialogueProgress)
												ENDIF
											ELSE
												bReturn = true
												PRINTLN("[RCC MISSION][MJM] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - Dialogue range check bReturn = true iDialogueProgress = ",iDialogueProgress)
											ENDIF
										ENDIF
									ENDIF
								ELSE // no range check
									IF IS_BIT_SET(g_FMMC_STRUCT.biDialogueTriggerAtMidPoint[iDialogueProgress/32], iDialogueProgress%32)
										IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam],g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iRule)
											IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPoint > 0
												//PRINTLN("[RCC MISSION][MJM] iCurrentPoints (midpoint) = ",iCurrentPoints," g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPoint = ",g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPoint)
												IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam],g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iRule)
												AND iCurrentPoints >= g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPoint
													PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - Points Target Met.")
													bReturn = true
												ENDIF
											ELSE
												PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - Points = 0")
												bReturn = true
											ENDIF
										ENDIF
									ELIF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset2, iBS_Dialogue2TriggerOnRule)
										IF iTriggerRule = iRule
										AND NOT bIgnoreRule
											PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - iBS_Dialogue2TriggerOnRule")
											bReturn = TRUE
										ENDIF
									ELIF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset2, iBS_Dialogue2FinalLap)
										PRINTLN("[RCC MISSION][MJM] FINAL LAP EVAL")
										IF MC_ServerBD_4.iTeamLapsCompleted[iteam] +1 = g_FMMC_STRUCT.sFMMCEndConditions[iteam].iMissionLapLimit
											PRINTLN("[RCC MISSION][MJM] FINAL LAP TRIGGERED")
											bReturn = TRUE
										ENDIF
									ELIF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset2, iBS_Dialogue2CheckMinimumSpeed)
										IF fVehSpeedForAudioTrigger <= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinSpeed[iRule]
											PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - Speed check passing")
											PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - Current Speed: ", fVehSpeedForAudioTrigger, " Minimum Speed: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinSpeed[iRule])
											bReturn = TRUE
										ENDIF
									ELIF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset2, iBS_Dialogue2InBackOfMOC)
										IF IS_PLAYER_IN_CREATOR_TRAILER(PLAYER_ID())
											PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - In back of MOC passing")
											bReturn = TRUE
										ENDIF
									ELSE
										IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPoint > 0
											//PRINTLN("[RCC MISSION][MJM] iCurrentPoints = ",iCurrentPoints," g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPoint = ",g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPoint)
											IF (IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam],g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iRule)
											OR NOT IS_BIT_SET(g_FMMC_STRUCT.biDialogueTriggerAtMidPoint[iDialogueProgress/32], iDialogueProgress%32))
											AND iCurrentPoints >= g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPoint
												PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - Points Target Met and On Mid Point.")
												bReturn = true
											ELSE
												IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam],g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iRule)
													PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - We're not at the mid point")
												ENDIF
											ENDIF
										ELSE
											IF NOT bIgnoreRule
												PRINTLN("[RCC MISSION] IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - NOT bIgnoreRule")
												bReturn = true
											ELSE
												PRINTLN("IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM - bIgnoreRule is set, so we are ignoring the fact that iCurrentPoints >= g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPoint")
											ENDIF
										ENDIF
									ENDIF
								ENDIF // range check								
							ENDIF			
						ENDIF 	
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bReturn

ENDFUNC

///PURPOSE: This function checks all of the different situations in which dialgue can happen and returns TRUE when valid
///    Some of these situations are:
///    	* Within a range
///     * When a certain score has been reached
///     * On a rule midpoint
FUNC BOOL IS_DIALOGUE_TRIGGER_VALID()
	
	#IF IS_DEBUG_BUILD
	IF FMMC_IS_LONG_BIT_SET(iLocalDialogueForcePlay, iDialogueProgress)		
		RETURN TRUE
	ENDIF	
	#ENDIF
	
	//Check if true for any team
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iTeam = ciDIALOGUE_TRIGGER_TEAM_ANY
		INT iLocalTeam
		
		REPEAT FMMC_MAX_TEAMS iLocalTeam
			IF IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM(iLocalTeam)
				RETURN TRUE
			ENDIF
		ENDREPEAT
		
		RETURN FALSE
	ELSE //Check only the specified team
		RETURN IS_DIALOGUE_TRIGGER_VALID_FOR_TEAM(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iTeam)
	ENDIF
	
ENDFUNC

FUNC BOOL DOES_DIALOGUE_HAVE_VALID_SPAWN_GROUP_TRIGGER(INT iDialogue)
	
	BOOL bValid = TRUE
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpawnGroup > 0
		
		IF g_FMMC_STRUCT.iRandomEntitySpawn_MaxNumber > 0
		AND g_FMMC_STRUCT.iRandomEntitySpawn_RandomNumbersToUse != 0
			
			INT iSpawnGroupsWeCareAbout, iCorrectSpawnGroups
			
			INT i
			
			FOR i = 0 TO (g_FMMC_STRUCT.iRandomEntitySpawn_MaxNumber - 1)
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iSpawnGroup, i)
					//We have a valid spawn group to check:
					iSpawnGroupsWeCareAbout++
					
					IF IS_BIT_SET(MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS, i)
						//This spawn group check passes:
						iCorrectSpawnGroups++
					ENDIF
				ENDIF
				
			ENDFOR
			
			IF iSpawnGroupsWeCareAbout > 0
				bValid = FALSE
				
				IF iCorrectSpawnGroups > 0
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iDialogueBitset2, iBS_Dialogue2WithSpawnGroup_AND)
						//This is an OR: any of the selected groups will trigger this dialogue
						bValid = TRUE
					ELSE
						//This is an AND: all of the selected groups have to have spawned to trigger this dialogue
						IF iCorrectSpawnGroups >= iSpawnGroupsWeCareAbout
							bValid = TRUE
						
						#IF IS_DEBUG_BUILD
						ELSE
							PRINTLN("[RCC MISSION] DOES_DIALOGUE_HAVE_VALID_SPAWN_GROUP_TRIGGER - FALSE for dialogue ",iDialogue," as this is an AND check, and some of the spawn groups were missing")
						#ENDIF
						
						ENDIF
					ENDIF
				
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[RCC MISSION] DOES_DIALOGUE_HAVE_VALID_SPAWN_GROUP_TRIGGER - FALSE for dialogue ",iDialogue," as none of the spawn groups were selected")
				#ENDIF
				
				ENDIF
			ENDIF
			
		ENDIF
		
	ENDIF
	
	RETURN bValid
	
ENDFUNC

///PURPOSE: This function returns true if this line of dialogue is meant to be skipped
FUNC BOOL SHOULD_CURRENT_DIALOGUE_LINE_BE_SKIPPED()
	
	#IF IS_DEBUG_BUILD
	IF FMMC_IS_LONG_BIT_SET(iLocalDialogueForcePlay, iDialogueProgress)		
		RETURN FALSE
	ENDIF	
	#ENDIF
	
	BOOL bReturn = FALSE
	
	IF  IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset, iBS_DialogueSkipOnQuickRestart )
	AND IS_THIS_A_QUICK_RESTART_JOB()
		PRINTLN("[RCC MISSION][SHOULD_CURRENT_DIALOGUE_LINE_BE_SKIPPED] Skipping dialogue ",iDialogueProgress,". Reason: Quick Restart." )
		bReturn = TRUE
	ENDIF
	
	IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iSkipOnRestart > 0
		IF IS_THIS_A_QUICK_RESTART_JOB()
			IF g_TransitionSessionNonResetVars.iTimesRestarted >= g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iSkipOnRestart
				PRINTLN("[RCC MISSION][SHOULD_CURRENT_DIALOGUE_LINE_BE_SKIPPED] Skipping dialogue ",iDialogueProgress,". Reason: Restarted ", g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iSkipOnRestart ," or more times." )
				bReturn = TRUE
			ENDIF
		ELSE
			IF g_TransitionSessionNonResetVars.iTimesRestarted > 0
				g_TransitionSessionNonResetVars.iTimesRestarted = 0
				PRINTLN("[RCC MISSION][SHOULD_CURRENT_DIALOGUE_LINE_BE_SKIPPED] Not a quick restart, reseting g_TransitionSessionNonResetVars.iTimesRestarted")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset2, iBS_Dialogue2SkipOnTripSkip )
	AND MC_playerBD[NATIVE_TO_INT(LocalPlayer)].TSPlayerData.iCost > 0
		PRINTLN("[RCC MISSION][SHOULD_CURRENT_DIALOGUE_LINE_BE_SKIPPED] Skipping dialogue ",iDialogueProgress,". Reason: Trip Skip." )
		bReturn = TRUE
	ENDIF
	
	//If this should be skipped if we're going to the first check point
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset2, iBS_Dialogue2SkipOnQuickRestart1)
	AND HAS_MISSION_START_CHECKPOINT_BEEN_SET_OR_PASSED(1)
		PRINTLN("[RCC MISSION][SHOULD_CURRENT_DIALOGUE_LINE_BE_SKIPPED] Skipping dialogue ",iDialogueProgress,". Reason: First Checkpoint." )
		bReturn = TRUE
	ENDIF
	
	//If this should be skipped if we're going to the second check point
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset2, iBS_Dialogue2SkipOnQuickRestart2)
	AND HAS_MISSION_START_CHECKPOINT_BEEN_SET_OR_PASSED(2)
		PRINTLN("[RCC MISSION][SHOULD_CURRENT_DIALOGUE_LINE_BE_SKIPPED] Skipping dialogue ",iDialogueProgress,". Reason: Second Checkpoint." )
		bReturn = TRUE
	ENDIF
	
	//If this should be skipped if we're going to the second check point
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset2, iBS_Dialogue2SkipOnQuickRestart3)
	AND HAS_MISSION_START_CHECKPOINT_BEEN_SET_OR_PASSED(3)
		PRINTLN("[RCC MISSION][SHOULD_CURRENT_DIALOGUE_LINE_BE_SKIPPED] Skipping dialogue ",iDialogueProgress,". Reason: Third Checkpoint." )
		bReturn = TRUE
	ENDIF
	
	//If this should be skipped if we're going to the second check point
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset2, iBS_Dialogue2SkipOnQuickRestart4)
	AND HAS_MISSION_START_CHECKPOINT_BEEN_SET_OR_PASSED(4)
		PRINTLN("[RCC MISSION][SHOULD_CURRENT_DIALOGUE_LINE_BE_SKIPPED] Skipping dialogue ",iDialogueProgress,". Reason: Fourth Checkpoint." )
		bReturn = TRUE
	ENDIF
	
	IF NOT DOES_DIALOGUE_HAVE_VALID_SPAWN_GROUP_TRIGGER(iDialogueProgress)
		PRINTLN("[RCC MISSION][SHOULD_CURRENT_DIALOGUE_LINE_BE_SKIPPED] Skipping dialogue ",iDialogueProgress,". Reason: Spawn Group logic didn't pass." )
		bReturn = TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset2, iBS_Dialogue2OnlyOnFirstRound)
	AND NOT IS_THIS_THE_FIRST_ROUND_OR_NOT_A_ROUNDS_MISSION()
		PRINTLN("[RCC MISSION][SHOULD_CURRENT_DIALOGUE_LINE_BE_SKIPPED] Skipping dialogue ",iDialogueProgress,". Reason: iBS_Dialogue2OnlyOnFirstRound." )
		bReturn = TRUE
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(MC_serverBD.tdCopSpottedFailTimer)
	AND (NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset2, iBS_Dialogue2SeenByCops))
		//If the tdCopSpottedFailTimer has begun, then the mission is about to fail - the only dialogue we should play is dialogue that triggers due to this fail type (ie. dialogue with iBS_Dialogue2SeenByCops set)
		PRINTLN("[RCC MISSION][SHOULD_CURRENT_DIALOGUE_LINE_BE_SKIPPED] Skipping dialogue ",iDialogueProgress,". Reason: tdCopSpottedFailTimer + iBS_Dialogue2SeenByCops." )
		bReturn = TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_SUB_TEAM_ROLES)
	AND (g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPlayForRoleBS != 0)
	AND (MC_playerBD[iPartToUse].iCoronaRole != -1)
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iPlayForRoleBS, MC_playerBD[iPartToUse].iCoronaRole)
			PRINTLN("[RCC MISSION][SHOULD_CURRENT_DIALOGUE_LINE_BE_SKIPPED] Skipping dialogue ",iDialogueProgress,". Reason: iCoronaRole Related" )
			bReturn = TRUE
		ENDIF
	ENDIF
	
	RETURN bReturn
	
ENDFUNC

//TRUE if dialogue should be heard by your team, or if you should process the checks for other teams
FUNC BOOL SHOULD_TEAM_PROCESS_DIALOGUE_CHECKS()
	
	#IF IS_DEBUG_BUILD
	IF FMMC_IS_LONG_BIT_SET(iLocalDialogueForcePlay, iDialogueProgress)		
		RETURN TRUE
	ENDIF	
	#ENDIF
	
	RETURN NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset,iBS_DialogueNotHeardByTeam0 + MC_playerBD[iPartToUse].iteam)
			OR g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iTeam = MC_playerBD[iPartToUse].iteam
			OR g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iTeam = ciDIALOGUE_TRIGGER_TEAM_ANY
ENDFUNC

FUNC BOOL SHOULD_PROCESS_DIALOGUE_TRIGGER_ADVERSARY_MODE_CHECKS()
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
	
		BOOL bSpectatedTargetInjured = FALSE
		
		IF iPartToUse > -1
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartToUse))				
				IF IS_PED_INJURED(GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartToUse))))
					bSpectatedTargetInjured = TRUE
				ENDIF
			ENDIF
		ENDIF
	
		IF IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_TEAM_0_SHOULD_RESPAWN_BACK_AT_START_NOW + MC_PlayerBD[iLocalPart].iteam)
		OR IS_PED_INJURED(localPlayerPed)
		OR NOT GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()
		OR IS_LOCAL_PLAYER_ANY_SPECTATOR()
		OR bSpectatedTargetInjured
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_EnableGameMastersMode)
		IF NOT IS_BIT_SET(g_iSpecialSpectatorBS2, g_ciSpecial_Spectator_BS2_Print_Help_For_Game_Masters_Finished)
		AND IS_BIT_SET(g_iSpecialSpectatorBS2, g_ciSpecial_Spectator_BS2_Print_Help_For_Game_Masters)
			PRINTLN("[LM][SPEC_SPEC][DIALOGUE][HELP_TEXT] - Returning False. NOT g_ciSpecial_Spectator_BS2_Print_Help_For_Game_Masters_Finished")
			RETURN TRUE
		ENDIF
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Enable_Turn_Rotor_Tag_Team)	
		IF NOT IS_BIT_SET(g_iSpecialSpectatorBS2, g_ciSpecial_Spectator_BS2_Print_Help_For_Tag_Team_Finished)
		AND IS_BIT_SET(g_iSpecialSpectatorBS2, g_ciSpecial_Spectator_BS2_Print_Help_For_Tag_Team)
			PRINTLN("[LM][SPEC_SPEC][DIALOGUE][HELP_TEXT] - Returning False. NOT g_ciSpecial_Spectator_BS2_Print_Help_For_Tag_Team_Finished")
			RETURN TRUE
		ENDIF
	ENDIF	
	
	IF IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
		IF NOT IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Ready_For_Help_Text)
			PRINTLN("[LM][SPEC_SPEC][DIALOGUE][HELP_TEXT] - Returning False. g_ciSpecial_Spectator_BS_Ready_For_Help_Text not set")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_PLAY_DIALOGUE_ASAP()

	INT iNumOfPlayersChecked = 0
	INT iNumOfInactivePlayASAPs = 0
	
	IF bIsLocalPlayerHost
		INT iPartLoop = 0
		
		FOR	iPartLoop = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartLoop))
			AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_ANY_SPECTATOR)
				IF MC_playerBD[iPartLoop].iPlayThisAudioTriggerASAP_CLIENT > -1
				AND MC_playerBD[iPartLoop].iPlayThisAudioTriggerASAP_CLIENT != MC_serverBD_4.iPlayThisAudioTriggerASAP_SERVER
					IF MC_serverBD_4.iPlayThisAudioTriggerASAP_SERVER > -2
						PRINTLN("[DialogueTrigger] Setting MC_serverBD_4.iPlayThisAudioTriggerASAP_SERVER to ", MC_playerBD[iPartLoop].iPlayThisAudioTriggerASAP_CLIENT, " thanks to participant ", iPartLoop)
						MC_serverBD_4.iPlayThisAudioTriggerASAP_SERVER = MC_playerBD[iPartLoop].iPlayThisAudioTriggerASAP_CLIENT
					ENDIF
				ELSE
					iNumOfInactivePlayASAPs++
				ENDIF
				
				iNumOfPlayersChecked++
			ENDIF
		ENDFOR
	ENDIF
	
	IF MC_serverBD_4.iPlayThisAudioTriggerASAP_SERVER = -2
	
		IF MC_playerBD[iLocalPart].iPlayThisAudioTriggerASAP_CLIENT > -1
			MC_playerBD[iLocalPart].iPlayThisAudioTriggerASAP_CLIENT = -1
			PRINTLN("[DialogueTrigger] Resetting iPlayThisAudioTriggerASAP_CLIENT")
		ENDIF
		
		PRINTLN(iNumOfInactivePlayASAPs, " / ", iNumOfPlayersChecked)
		
		IF iNumOfInactivePlayASAPs >= iNumOfPlayersChecked
		AND bIsLocalPlayerHost
			MC_serverBD_4.iPlayThisAudioTriggerASAP_SERVER = -1
			PRINTLN("[DialogueTrigger] Host resetting iPlayThisAudioTriggerASAP_SERVER to -1 now because everyone is clear")
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_DIALOGUE_TRIGGER_BLOCKING()
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset5, iBS_Dialogue5_BlockWithTriggerBlockerList)
	OR IS_BIT_SET(iLocalDialogueBlockedBS[iDialogueProgress/32], iDialogueProgress%32)
		PRINTLN("[RCC MISSION][DIALOGUE] - iDialogueProgress: ", iDialogueProgress, " has already been blocked. Exitting process.")
		EXIT
	ENDIF
	
	INT i = 0
	FOR i = 0 TO g_FMMC_STRUCT.iNumberOfDialogueTriggers-1		
		IF IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iBlockingTriggers, i)
		AND (IS_BIT_SET(iLocalDialoguePlayedBS[i/32],i%32) OR IS_BIT_SET(iLocalDialoguePlayingBS[i/32],i%32))
		AND g_FMMC_STRUCT.sDialogueTriggers[i].iRule != -1 //B*6196255
			IF NOT IS_BIT_SET(iLocalDialogueBlockedBS[iDialogueProgress/32], iDialogueProgress%32)
				PRINTLN("[RCC MISSION][DIALOGUE] - iDialogueProgress: ", iDialogueProgress, " is now being blocked... due to Dialogue Trigger: ", i)
				SET_BIT(iLocalDialogueBlockedBS[iDialogueProgress/32], iDialogueProgress%32)
				BREAKLOOP
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC PROCESS_PREVIOUS_DIALOGUE()
	If iDialogueProgressLastPlayed > -1
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgressLastPlayed].iDialogueBitset3, iBS_Dialogue3_BlockObjectiveWhilePlayingDialogue)
			IF IS_BIT_SET(iLocalDialoguePlayingBS[iDialogueProgressLastPlayed/32],iDialogueProgressLastPlayed%32)
				IF IS_SCRIPTED_CONVERSATION_ONGOING()
					PRINTLN("[RCC MISSION][DIALOGUE] - iDialogueProgressLastPlayed: ", iDialogueProgressLastPlayed, " Dialogue ongoing...")
				ELSE
					CLEAR_BIT(iLocalDialoguePlayingBS[iDialogueProgressLastPlayed/32],iDialogueProgressLastPlayed%32)
					PRINTLN("[RCC MISSION][DIALOGUE] - iDialogueProgressLastPlayed: ", iDialogueProgressLastPlayed, " Dialogue Finished. Clearing 'Playing' bit.")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

SCRIPT_TIMER RestartConvTimer
CONST_INT iRestartConvTime	2000

PROC PROCESS_CURRENT_DIALOGUE()
	
	// Do not remove these helpful debug prints for when Dialogue/Help Text is not playing. Just comment them out.
	// DEBUG_AUDIO_DIALOGUE_TRIGGERS_READOUT(iDialogueProgress) 
		
	IF NOT IS_BIT_SET(iLocalDialoguePlayedBS[iDialogueProgress/32],iDialogueProgress%32)
	OR SHOULD_DIALOGUE_BE_REPLAYABLE(iDialogueProgress)
	
		IF IS_VECTOR_ZERO(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].vPosition)
			SET_BIT(MC_playerBD[iPartToUse].iDialoguePlayedBS[iDialogueProgress/32],iDialogueProgress%32)
			PRINTLN("[RCC MISSION][DIALOGUE] - Setting MC_playerBD[iPartToUse].iDialoguePlayedBS[",iDialogueProgress/32,"],",iDialogueProgress%32, " Call 3")
			SET_BIT(iLocalDialoguePlayedBS[iDialogueProgress/32],iDialogueProgress%32)
			SET_DIALOGUE_AS_PLAYED_FOR_CONTINUITY(iDialogueProgress)
			//Accidentally loaded a trigger at zero, skip playing it!
			PRINTLN("[RCC MISSION] Dialogue ",iDialogueProgress," was loaded with a position of <<0,0,0>> - skipping to next dialogue")
		ELSE
			IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset2,iBS_Dialogue2ResetOnLaps)
				IF IS_BIT_SET(iLocalBoolCheck13, LBOOL13_MY_TEAM_HAS_A_NEW_LAP)
					IF HAS_NET_TIMER_STARTED(tdDialogueTimer[iDialogueProgress])
						PRINTLN("[RCC MISSION] reset the dialogue timer for ", iDialogueProgress)
						RESET_NET_TIMER(tdDialogueTimer[iDialogueProgress])
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iDialogueBitset3, iBS_Dialogue3_UsePhoneCall)
				IF GET_INCOMING_CONTACT_CHARACTER_FROM_CREATOR(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iCallCharacter) != MAX_NRM_CHARACTERS_PLUS_DUMMY
				AND GET_INCOMING_CONTACT_CHARACTER_FROM_CREATOR(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iCallCharacter) != CHAR_BLOCKED
					IF NOT IS_CONTACT_IN_PHONEBOOK(GET_INCOMING_CONTACT_CHARACTER_FROM_CREATOR(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iCallCharacter), MULTIPLAYER_BOOK)
						IF NOT IS_BIT_SET(iCharacterAddedToPhoneBook, g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iCallCharacter)
							ADD_CONTACT_TO_PHONEBOOK(GET_INCOMING_CONTACT_CHARACTER_FROM_CREATOR(g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iCallCharacter), MULTIPLAYER_BOOK, FALSE)
							SET_BIT(iCharacterAddedToPhoneBook, g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iCallCharacter)
							PRINTLN("[RCC MISSION] PROCESS_CURRENT_DIALOGUE - Adding temporary contact to phone book for dialogue trigger ", iDialogueProgress)
						ELSE
							PRINTLN("[RCC MISSION] PROCESS_CURRENT_DIALOGUE - Contact not in phone book, but bit has been set.")
							CLEAR_BIT(iCharacterAddedToPhoneBook, g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iCallCharacter)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			
			//Check if dialogue should be heard by this team:
			IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iRule != -1
			AND g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iTeam != ciDIALOGUE_TRIGGER_TEAM_OFF
			AND SHOULD_TEAM_PROCESS_DIALOGUE_CHECKS()
			AND NOT SHOULD_CURRENT_DIALOGUE_LINE_BE_SKIPPED()
			AND SHOULD_PROCESS_DIALOGUE_TRIGGER_ADVERSARY_MODE_CHECKS()
				IF IS_DIALOGUE_TRIGGER_VALID()
					MANAGE_DIALOGUE()
				ENDIF
			ELSE
				SET_BIT(MC_playerBD[iPartToUse].iDialoguePlayedBS[iDialogueProgress/32],iDialogueProgress%32)
				PRINTLN("[RCC MISSION][DIALOGUE] - Setting MC_playerBD[iPartToUse].iDialoguePlayedBS[",iDialogueProgress/32,"],",iDialogueProgress%32, " Call 4")
				SET_BIT(iLocalDialoguePlayedBS[iDialogueProgress/32],iDialogueProgress%32)
				SET_DIALOGUE_AS_PLAYED_FOR_CONTINUITY(iDialogueProgress)
				//This dialogue trigger does not play for this time, skip playing it!
				PRINTLN("[RCC MISSION] Dialogue doesn't play for this team or not loaded for this team - skipping to next dialogue")
			ENDIF
			
			PROCESS_PLAY_DIALOGUE_ASAP()
		ENDIF
	ELSE
		IF iCurrentHeistMissionIndex = HBCA_BS_TUTORIAL_SCOPE_OUT
			IF NOT IS_BIT_SET(iDialogueInterruptedBS[iDialogueProgress/32],iDialogueProgress%32)
				IF bLocalPlayerPedOk
					IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
						IF IS_PED_SHOOTING(LocalPlayerPed)
							TEXT_LABEL_23 tlConvLbl = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
						
							IF NOT ARE_STRINGS_EQUAL(tlConvLbl,"NULL")	
								TEXT_LABEL_23 sRoot = g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].tlRoot 								
								
								IF ARE_STRINGS_EQUAL(tlConvLbl,sRoot)
									PRINTLN("[RCC MISSION] Attempting to interrupt dialogue due to player action (shooting)")
									
									IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[0])
										
										//PRINTLN("[RCC MISSION] [DLG] INTERRUPT_CONVERSATION(NULL, CONV_INTERRUPT_QUIT_IT, ",sSpeaker,")", )
										SET_AUDIO_FLAG("ForceConversationInterrupt",TRUE)
										INTERRUPT_CONVERSATION_AND_PAUSE(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[0]), "CONV_INTERRUPT_QUIT_IT", "LESTER")
										SET_BIT(iDialogueInterruptedBS[iDialogueProgress/32],iDialogueProgress%32)
										SET_BIT(iLocalBoolCheck8,LBOOL8_CONVERSATION_INTERRUPTED)
										RESET_NET_TIMER(RestartConvTimer)
										PRINTLN("[RCC MISSION] Interrupted dialogue due to player action (shooting)")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF		
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + g_FMMC_STRUCT.sDialogueTriggers[iDialogueProgress].iTeam)
		PRINTLN("[RCC MISSION][JR] Resetting MC_serverBD_4.iGangChasePedsKilledThisRule to 0.")
		MC_serverBD_4.iGangChasePedsKilledThisRule = 0
	ENDIF
	
	IF NOT SHOULD_PROCESS_DIALOGUE_TRIGGER_ADVERSARY_MODE_CHECKS()
		IF IS_HELP_MESSAGE_BEING_DISPLAYED()			
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("WS_OS_HELP")
				PRINTLN("[RCC MISSION] Dialogue Help Message is being displayed, even though the Adversary Mode Specific checks are saying don't play. Clear_help_text.")
				CLEAR_HELP(TRUE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// [FIX_2020_CONTROLLER] - Add a Log Tag for anything related to dialogue triggers
//TODO(Tom): Remove iLocalDialoguePriority, we can probably just use the DOES_TEAM_HAVE_NEW_PRIORITY check
PROC PROCESS_DIALOGUE_TRIGGERS()
	// If we have any dialogue to play, loop through all triggers to see if any are valid and play it
	IF g_FMMC_STRUCT.iNumberOfDialogueTriggers > 0		
		
		IF NOT IS_BIT_SET(iLocalBoolCheck8,LBOOL8_CONVERSATION_INTERRUPTED)
		OR HAS_NET_TIMER_EXPIRED(RestartConvTimer,iRestartConvTime)
		OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_LOST_DAMNED(g_FMMC_Struct.iAdversaryModeType)
			IF IS_BIT_SET(iLocalBoolCheck8,LBOOL8_CONVERSATION_INTERRUPTED)
				SET_AUDIO_FLAG("ForceConversationInterrupt",FALSE)
				RESTART_SCRIPTED_CONVERSATION() 
				CLEAR_BIT(iLocalBoolCheck8,LBOOL8_CONVERSATION_INTERRUPTED)
				PRINTLN("[RCC MISSION] PROCESS_DIALOGUE_TRIGGERS - RESTART_SCRIPTED_CONVERSATION after interruption (shooting)")
			ENDIF
			
			IF HAS_NET_TIMER_STARTED(MC_serverBD.tdCopSpottedFailTimer)
			AND NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_DONE_DIALOGUE_LOOP_AFTER_COPS_SPOTTED)
				PRINTLN("[RCC MISSION] PROCESS_DIALOGUE_TRIGGERS - Performing a dialogue loop as we've been spotted by the cops!")
				SET_BIT(iLocalBoolCheck9,LBOOL9_DO_INSTANT_DIALOGUE_LOOP)
				SET_BIT(iLocalBoolCheck13, LBOOL13_DONE_DIALOGUE_LOOP_AFTER_COPS_SPOTTED)
			ENDIF
			
			INT iRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
			
			//If new rule, do one frame loop through all dialogue triggers once
			IF iRule < FMMC_MAX_RULES
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetThree[iRule], ciBS_RULE3_LOOP_ALL_DIALOGUE_TRIGGERS)
				AND iLocalDialoguePriority != iRule
					
					PRINTLN("[RCC MISSION] PROCESS_DIALOGUE_TRIGGERS - Dialogue rule change ",iLocalDialoguePriority," to ",iRule," - Looping through all dialogue triggers")
					
					REPEAT g_FMMC_STRUCT.iNumberOfDialogueTriggers iDialogueProgress						
						PROCESS_CURRENT_DIALOGUE()
						PROCESS_PREVIOUS_DIALOGUE()
					ENDREPEAT
					
					iLocalDialoguePriority = iRule
					iDialogueProgress = 0
					
				//Added to prevent big gaps in dialogue after interrupting
				ELIF IS_BIT_SET(iLocalBoolCheck9,LBOOL9_DO_INSTANT_DIALOGUE_LOOP)
					
					PRINTLN("[RCC MISSION] PROCESS_DIALOGUE_TRIGGERS - LBOOL9_DO_INSTANT_DIALOGUE_LOOP is set - Looping through all dialogue triggers")
					
					REPEAT g_FMMC_STRUCT.iNumberOfDialogueTriggers iDialogueProgress
						PROCESS_CURRENT_DIALOGUE()
						PROCESS_PREVIOUS_DIALOGUE()
					ENDREPEAT
					
					iDialogueProgress = 0
					CLEAR_BIT(iLocalBoolCheck9,LBOOL9_DO_INSTANT_DIALOGUE_LOOP)
					
				ELSE //Otherwise stagger
					
					IF iDialogueProgress < g_FMMC_STRUCT.iNumberOfDialogueTriggers
						PROCESS_CURRENT_DIALOGUE()
						PROCESS_PREVIOUS_DIALOGUE()
						PROCESS_DIALOGUE_TRIGGER_BLOCKING()
					ELSE
						//Loop back around
						iDialogueProgress = -1
					ENDIF
					
					//Move onto next dialogue for next frame
					iDialogueProgress++
					
					// [LM] Check every frame dialogues that have a delay started OR that have called TRIGGER_DIALOGUE() and is therefore pending.
					// This should expedite the processing of Dialogue Triggers and eliminate 5-7 second iterator related delays we are experiencing.
					// The delays are a result of having 10-15 fps and having to iterate through 60 triggers. 1 second creator assigned delays can become up to 7.
					// We don't care to revalidate as the Dialogue was valid when we triggered it (or started the delay).					
					IF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_EXPEDITE_DIALOGUE_TRIGGERS_ENABLED)
						INT iDiag
						iDialogueProgressCached = iDialogueProgress
						REPEAT g_FMMC_STRUCT.iNumberOfDialogueTriggers iDiag
							IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDiag].iDialogueBitset3, iBS_Dialogue3_ExpediteAndDontRevalidate)
							OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_EXPEDITE_AND_DONT_REVALIDATE_DIAG_TRIGGERS)						
								IF NOT IS_BIT_SET(iLocalDialoguePlayedBS[iDiag/32],iDiag%32)
									IF (g_FMMC_STRUCT.sDialogueTriggers[iDiag].iTimeDelay > 0 AND HAS_NET_TIMER_STARTED(tdDialogueTimer[iDiag]))
									OR IS_BIT_SET(iLocalDialoguePending[iDiag/32],iDiag%32)
										iDialogueProgress = iDiag
										
										BOOL bCheckTimer 
										IF HAS_NET_TIMER_STARTED(tdDialogueTimer[iDiag])
											PRINTLN("[LM][DIALOGUE_TRIGGER][EXPEDITED] - iDialogueProgress: ", iDialogueProgress, " Timer has started.")
											bCheckTimer = CHECK_DIALOGUE_TIMER()
										ENDIF
										
										IF IS_BIT_SET(iLocalDialoguePending[iDiag/32],iDiag%32)
										OR bCheckTimer
											PRINTLN("[LM][DIALOGUE_TRIGGER][EXPEDITED] - Expedited Dialogue Trigger Processing - iDialogueProgress: ", iDialogueProgress, " iLocalDialoguePending: ", IS_BIT_SET(iLocalDialoguePending[iDiag/32],iDiag%32), " bCheckTimer: ", bCheckTimer)
											MANAGE_DIALOGUE()
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDREPEAT
						iDialogueProgress = iDialogueProgressCached
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		MANAGE_OBJECTIVE_HELP_TEXT_REPRINT()
	ENDIF
	
ENDPROC

PROC PLAY_POWERPLAY_SOUND(INT iPowerupType, INT iSenderTeam, PLAYER_INDEX piSenderPlayer)

	STRING sSoundSet, sSceneSoundSet
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[RCC MISSION] PLAY_POWERPLAY_SOUND - ",  
				iPPBullsharkSoundID, ", ",
				iPPBeastmodeSoundID, ", ",
				iPPThermalSoundID, ", ",
				iPPSwapSoundID, ", ",
				iPPFilterSoundID, ", ",
				iPPBulletTimeSoundID, ", ",
				iPPHideBlipsSoundID )
	#ENDIF

	SWITCH iPowerupType
		CASE ciCUSTOM_PICKUP_TYPE__BEAST_MODE
			IF iSenderTeam = MC_playerBD[iPartToUse].iteam
				IF piSenderPlayer = LocalPlayer
					sSoundSet = "POWER_PLAY_Beast_Mode_Player_Sounds"
					sSceneSoundSet = "GTAO_Power_Play_Beast_Mode_Player_Scene"
				ELSE
					sSoundSet = "POWER_PLAY_Beast_Mode_Team_Sounds"
					sSceneSoundSet = "GTAO_Power_Play_Beast_Mode_Team_Scene"
				ENDIF
			ELSE
				sSoundSet = "POWER_PLAY_Beast_Mode_Enemy_Sounds"
				sSceneSoundSet = "GTAO_Power_Play_Beast_Mode_Enemy_Scene"
			ENDIF
			
			sPPBeastmodeSoundSet = sSoundSet
			sPPBeastmodeSceneSoundSet = sSceneSoundSet
			
			IF iPPBeastmodeSoundID = -1
				iPPBeastmodeSoundID = GET_SOUND_ID()
				REINIT_NET_TIMER(stPPBeastmodeTimer)
				PLAY_SOUND_FRONTEND(iPPBeastmodeSoundID, "Loop", sSoundSet, FALSE)
				PRINTLN("[RCC MISSION] PLAY_POWERPLAY_SOUND - Playing Loop:", sSoundSet)
			ENDIF
		BREAK
		CASE ciCUSTOM_PICKUP_TYPE__BULLSHARK_TEST	
			IF iSenderTeam = MC_playerBD[iPartToUse].iteam
				IF piSenderPlayer = LocalPlayer
					sSoundSet = "POWER_PLAY_Bullshark_Player_Sounds"
					sSceneSoundSet = "GTAO_Power_Play_Bullshark_Player_Scene"
				ELSE
					sSoundSet = "POWER_PLAY_Bullshark_Team_Sounds"
					sSceneSoundSet = "GTAO_Power_Play_Bullshark_Team_Scene"
				ENDIF
			ELSE
				sSoundSet = "POWER_PLAY_Bullshark_Enemy_Sounds"
				sSceneSoundSet = "GTAO_Power_Play_Bullshark_Enemy_Scene"
			ENDIF
			
			sPPBullsharkSoundSet = sSoundSet
			sPPBullsharkSceneSoundSet = sSceneSoundSet
			
			IF iPPBullsharkSoundID = -1
				iPPBullsharkSoundID = GET_SOUND_ID()
				REINIT_NET_TIMER(stPPBullsharkTimer)
				PLAY_SOUND_FRONTEND(iPPBullsharkSoundID, "Loop", sSoundSet, FALSE)
				PRINTLN("[RCC MISSION] PLAY_POWERPLAY_SOUND - Playing Loop:", sSoundSet)
			ENDIF
		BREAK
		CASE ciCUSTOM_PICKUP_TYPE__FILTER	
			IF iSenderTeam = MC_playerBD[iPartToUse].iteam
				IF piSenderPlayer = LocalPlayer
					sSoundSet = "POWER_PLAY_Filter_Player_Sounds"
					sSceneSoundSet = "GTAO_Power_Play_Filter_Player_Scene"
				ELSE
					sSoundSet = "POWER_PLAY_Filter_Team_Sounds"
					sSceneSoundSet = "GTAO_Power_Play_Filter_Team_Scene"
				ENDIF
			ELSE
				sSoundSet = "POWER_PLAY_Filter_Enemy_Sounds"
				sSceneSoundSet = "GTAO_Power_Play_Filter_Enemy_Scene"
			ENDIF
			
			sPPFilterSoundSet = sSoundSet
			sPPFilterSceneSoundSet = sSceneSoundSet
			
			IF iPPFilterSoundID = -1
				iPPFilterSoundID = GET_SOUND_ID()
				REINIT_NET_TIMER(stPPFilterTimer)
				PLAY_SOUND_FRONTEND(iPPFilterSoundID, "Loop", sSoundSet, FALSE)
				PRINTLN("[RCC MISSION] PLAY_POWERPLAY_SOUND - Playing Loop:", sSoundSet)
			ENDIF
		BREAK	
		CASE ciCUSTOM_PICKUP_TYPE__SWAP	
			IF iSenderTeam = MC_playerBD[iPartToUse].iteam
				IF piSenderPlayer = LocalPlayer
					sSoundSet = "POWER_PLAY_Inverse_Player_Sounds"
					sSceneSoundSet = "GTAO_Power_Play_Inverse_Player_Scene"
				ELSE
					sSoundSet = "POWER_PLAY_Inverse_Team_Sounds"
					sSceneSoundSet = "GTAO_Power_Play_Inverse_Team_Scene"
				ENDIF
			ELSE
				sSoundSet = "POWER_PLAY_Inverse_Enemy_Sounds"
				sSceneSoundSet = "GTAO_Power_Play_Inverse_Enemy_Scene"
			ENDIF
			
			sPPSwapSoundSet = sSoundSet
			sPPSwapSceneSoundSet = sSceneSoundSet
			
			IF iPPSwapSoundID = -1
				iPPSwapSoundID = GET_SOUND_ID()
				REINIT_NET_TIMER(stPPSwapTimer)
				PLAY_SOUND_FRONTEND(iPPSwapSoundID, "Loop", sSoundSet, FALSE)
				PRINTLN("[RCC MISSION] PLAY_POWERPLAY_SOUND - Playing Loop:", sSoundSet)
			ENDIF
		BREAK	
		CASE ciCUSTOM_PICKUP_TYPE__BULLET_TIME	
			IF iSenderTeam = MC_playerBD[iPartToUse].iteam
				IF piSenderPlayer = LocalPlayer
					sSoundSet = "POWER_PLAY_BT_Player_Sounds"
					sSceneSoundSet = "GTAO_Power_Play_BT_Player_Scene"
				ELSE
					sSoundSet = "POWER_PLAY_BT_Team_Sounds"
					sSceneSoundSet = "GTAO_Power_Play_BT_Team_Scene"
				ENDIF
			ELSE
				sSoundSet = "POWER_PLAY_BT_Enemy_Sounds"
				sSceneSoundSet = "GTAO_Power_Play_BT_Enemy_Scene"
			ENDIF
			
			sPPBulletTimeSoundSet = sSoundSet
			sPPBulletTimeSceneSoundSet = sSceneSoundSet
			
			IF iPPBulletTimeSoundID = -1
				iPPBulletTimeSoundID = GET_SOUND_ID()
				REINIT_NET_TIMER(stPPBulletTimeTimer)
				PLAY_SOUND_FRONTEND(iPPBulletTimeSoundID, "Loop", sSoundSet, FALSE)
				PRINTLN("[RCC MISSION] PLAY_POWERPLAY_SOUND - Playing Loop:", sSoundSet)
			ENDIF
		BREAK
		CASE ciCUSTOM_PICKUP_TYPE__HIDE_BLIPS	
			IF iSenderTeam = MC_playerBD[iPartToUse].iteam
				IF piSenderPlayer = LocalPlayer
					sSoundSet = "POWER_PLAY_Hide_Blips_Player_Sounds"
					sSceneSoundSet = "GTAO_Power_Play_Hide_Blips_Player_Scene"
				ELSE
					sSoundSet = "POWER_PLAY_Hide_Blips_Team_Sounds"
					sSceneSoundSet = "GTAO_Power_Play_Hide_Blips_Team_Scene"
				ENDIF
			ELSE
				sSoundSet = "POWER_PLAY_Hide_Blips_Enemy_Sounds"
				sSceneSoundSet = "GTAO_Power_Play_Hide_Blips_Enemy_Scene"
			ENDIF
			
			sPPHideBlipsSoundSet = sSoundSet
			sPPHideBlipsSceneSoundSet = sSceneSoundSet
			
			IF iPPHideBlipsSoundID = -1
				iPPHideBlipsSoundID = GET_SOUND_ID()
				REINIT_NET_TIMER(stPPHideBlipTimer)
				PLAY_SOUND_FRONTEND(iPPHideBlipsSoundID, "Loop", sSoundSet, FALSE)
				PRINTLN("[RCC MISSION] PLAY_POWERPLAY_SOUND - Playing Loop:", sSoundSet)
			ENDIF
		BREAK
		CASE ciCUSTOM_PICKUP_TYPE__THERMAL_VISION	
			IF iSenderTeam = MC_playerBD[iPartToUse].iteam
				IF piSenderPlayer = LocalPlayer
					sSoundSet = "POWER_PLAY_Thermal_Player_Sounds"
					sSceneSoundSet = "GTAO_Power_Play_Thermal_Player_Scene"
				ELSE
					sSoundSet = "POWER_PLAY_Thermal_Team_Sounds"
					sSceneSoundSet = "GTAO_Power_Play_Thermal_Team_Scene"
				ENDIF
			ELSE
				sSoundSet = "POWER_PLAY_Thermal_Enemy_Sounds"
				sSceneSoundSet = "GTAO_Power_Play_Thermal_Enemy_Scene"
			ENDIF
			
			sPPThermalSoundSet = sSoundSet
			sPPThermalSceneSoundSet = sSceneSoundSet
			
			IF iPPThermalSoundID = -1
				iPPThermalSoundID = GET_SOUND_ID()
				REINIT_NET_TIMER(stPPThermalTimer)
				PLAY_SOUND_FRONTEND(iPPThermalSoundID, "Loop", sSoundSet, FALSE)
				PRINTLN("[RCC MISSION] PLAY_POWERPLAY_SOUND - Playing Loop:", sSoundSet)
			ENDIF
		BREAK	
	ENDSWITCH	
	
	PRINTLN("[RCC MISSION] PLAY_POWERPLAY_SOUND - Playing Start:",sSoundSet)
	PLAY_SOUND_FRONTEND(-1, "Start", sSoundSet, FALSE)
	
	PRINTLN("[RCC MISSION] PLAY_POWERPLAY_SOUND - Playing Scene:",sSceneSoundSet)
	START_AUDIO_SCENE(sSceneSoundSet)
ENDPROC

PROC PROCESS_POWERPLAY_LOOP_SOUNDS()

	//######### BEASTMODE LOOPING SOUND
	IF iPPBeastmodeSoundID != -1
	AND HAS_NET_TIMER_STARTED(stPPBeastmodeTimer)
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stPPBeastmodeTimer) > g_FMMC_STRUCT.iRATCBeastTimer*1000
			PRINTLN("[RCC MISSION] PROCESS_POWERPLAY_LOOP_SOUNDS - Ending Beastmode.")
			RESET_NET_TIMER(stPPBeastmodeTimer)
			STOP_SOUND(iPPBeastmodeSoundID)
			STOP_AUDIO_SCENE(sPPBeastmodeSceneSoundSet)
			SCRIPT_RELEASE_SOUND_ID(iPPBeastmodeSoundID)
						
			IF NOT IS_STRING_NULL_OR_EMPTY(sPPBeastmodeSoundSet)
				PLAY_SOUND_FRONTEND(-1, "Stop", sPPBeastmodeSoundSet, FALSE)
			ENDIF
			
			iPPBeastmodeSoundID = -1
		ELSE
			FLOAT fCompletionPercentage = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stPPBeastmodeTimer)) / TO_FLOAT(g_FMMC_STRUCT.iRATCBeastTimer*1000)
			fCompletionPercentage = CLAMP(fCompletionPercentage, 0.0, 1.0)
			SET_VARIABLE_ON_SOUND(iPPBeastmodeSoundID, "Control", fCompletionPercentage)
		ENDIF
	ENDIF

	//######### BULLSHARK LOOPING SOUND
	IF iPPBullsharkSoundID != -1
	AND HAS_NET_TIMER_STARTED(stPPBullsharkTimer)
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stPPBullsharkTimer) > g_FMMC_STRUCT.iRATCBullsharkTimer*1000
			PRINTLN("[RCC MISSION] PROCESS_POWERPLAY_LOOP_SOUNDS - Ending Bullshark.")
			RESET_NET_TIMER(stPPBullsharkTimer)
			STOP_SOUND(iPPBullsharkSoundID)
			STOP_AUDIO_SCENE(sPPBullsharkSceneSoundSet)
			SCRIPT_RELEASE_SOUND_ID(iPPBullsharkSoundID)
			
			IF NOT IS_STRING_NULL_OR_EMPTY(sPPBullsharkSoundSet)
				PLAY_SOUND_FRONTEND(-1, "Stop", sPPBullsharkSoundSet, FALSE)
			ENDIF
			
			iPPBullsharkSoundID = -1
		ELSE
			FLOAT fCompletionPercentage = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stPPBullsharkTimer)) / TO_FLOAT(g_FMMC_STRUCT.iRATCBullsharkTimer*1000)
			fCompletionPercentage = CLAMP(fCompletionPercentage, 0.0, 1.0)
			SET_VARIABLE_ON_SOUND(iPPBullsharkSoundID, "Control", fCompletionPercentage)
		ENDIF
	ENDIF
	
	//######### FILTER LOOPING SOUND
	IF iPPFilterSoundID != -1
	AND HAS_NET_TIMER_STARTED(stPPFilterTimer)
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stPPFilterTimer) > g_FMMC_STRUCT.iRATCFilterTimer*1000
			PRINTLN("[RCC MISSION] PROCESS_POWERPLAY_LOOP_SOUNDS - Ending Filter.")
			RESET_NET_TIMER(stPPFilterTimer)
			STOP_SOUND(iPPFilterSoundID)
			STOP_AUDIO_SCENE(sPPFilterSceneSoundSet)
			SCRIPT_RELEASE_SOUND_ID(iPPFilterSoundID)
			
			IF NOT IS_STRING_NULL_OR_EMPTY(sPPFilterSoundSet)
				PLAY_SOUND_FRONTEND(-1, "Stop", sPPFilterSoundSet, FALSE)
			ENDIF
			
			iPPFilterSoundID = -1
		ELSE
			FLOAT fCompletionPercentage = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stPPFilterTimer)) / TO_FLOAT(g_FMMC_STRUCT.iRATCFilterTimer*1000)
			fCompletionPercentage = CLAMP(fCompletionPercentage, 0.0, 1.0)
			SET_VARIABLE_ON_SOUND(iPPFilterSoundID, "Control", fCompletionPercentage)
		ENDIF
	ENDIF
	
	//######### SWAP LOOPING SOUND
	IF iPPSwapSoundID != -1
	AND HAS_NET_TIMER_STARTED(stPPSwapTimer)
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stPPSwapTimer) > g_FMMC_STRUCT.iRATCSwapTimer*1000
			PRINTLN("[RCC MISSION] PROCESS_POWERPLAY_LOOP_SOUNDS - Ending Swap.")
			RESET_NET_TIMER(stPPSwapTimer)
			STOP_SOUND(iPPSwapSoundID)
			STOP_AUDIO_SCENE(sPPSwapSceneSoundSet)
			SCRIPT_RELEASE_SOUND_ID(iPPSwapSoundID)
			
			IF NOT IS_STRING_NULL_OR_EMPTY(sPPSwapSoundSet)
				PLAY_SOUND_FRONTEND(-1, "Stop", sPPSwapSoundSet, FALSE)
			ENDIF
			
			iPPSwapSoundID = -1
		ELSE
			FLOAT fCompletionPercentage = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stPPSwapTimer)) / TO_FLOAT(g_FMMC_STRUCT.iRATCSwapTimer*1000)
			fCompletionPercentage = CLAMP(fCompletionPercentage, 0.0, 1.0)
			SET_VARIABLE_ON_SOUND(iPPSwapSoundID, "Control", fCompletionPercentage)
		ENDIF
	ENDIF
	
	//######### BULLETTIME LOOPING SOUND
	IF iPPBulletTimeSoundID != -1
	AND HAS_NET_TIMER_STARTED(stPPBulletTimeTimer)
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stPPBulletTimeTimer) > g_FMMC_STRUCT.iRATCBulletTimeTimer*1000
			PRINTLN("[RCC MISSION] PROCESS_POWERPLAY_LOOP_SOUNDS - Ending BulletTime.")
			RESET_NET_TIMER(stPPBulletTimeTimer)
			STOP_SOUND(iPPBulletTimeSoundID)
			STOP_AUDIO_SCENE(sPPBulletTimeSceneSoundSet)
			SCRIPT_RELEASE_SOUND_ID(iPPBulletTimeSoundID)
			
			IF NOT IS_STRING_NULL_OR_EMPTY(sPPBulletTimeSoundSet)
				PLAY_SOUND_FRONTEND(-1, "Stop", sPPBulletTimeSoundSet, FALSE)
			ENDIF
			
			iPPBulletTimeSoundID = -1
		ELSE
			FLOAT fCompletionPercentage = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stPPBulletTimeTimer)) / TO_FLOAT(g_FMMC_STRUCT.iRATCBulletTimeTimer*1000)
			fCompletionPercentage = CLAMP(fCompletionPercentage, 0.0, 1.0)
			SET_VARIABLE_ON_SOUND(iPPBulletTimeSoundID, "Control", fCompletionPercentage)
		ENDIF
	ENDIF
	
	//######### HIDE BLIPS LOOPING SOUND
	IF iPPHideBlipsSoundID != -1
	AND HAS_NET_TIMER_STARTED(stPPHideBlipTimer)
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stPPHideBlipTimer) > g_FMMC_STRUCT.iRATCHideBlipsTimer*1000
			PRINTLN("[RCC MISSION] PROCESS_POWERPLAY_LOOP_SOUNDS - Ending HideBlips.")
			RESET_NET_TIMER(stPPHideBlipTimer)
			STOP_SOUND(iPPHideBlipsSoundID)
			STOP_AUDIO_SCENE(sPPHideBlipsSceneSoundSet)
			SCRIPT_RELEASE_SOUND_ID(iPPHideBlipsSoundID)
			
			IF NOT IS_STRING_NULL_OR_EMPTY(sPPHideBlipsSoundSet)
				PLAY_SOUND_FRONTEND(-1, "Stop", sPPHideBlipsSoundSet, FALSE)
			ENDIF
			
			iPPHideBlipsSoundID = -1
		ELSE
			FLOAT fCompletionPercentage = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stPPHideBlipTimer)) / TO_FLOAT(g_FMMC_STRUCT.iRATCHideBlipsTimer*1000)
			fCompletionPercentage = CLAMP(fCompletionPercentage, 0.0, 1.0)
			SET_VARIABLE_ON_SOUND(iPPHideBlipsSoundID, "Control", fCompletionPercentage)
		ENDIF
	ENDIF
	
	//######### THERMAL LOOPING SOUND
	IF iPPThermalSoundID != -1
	AND HAS_NET_TIMER_STARTED(stPPThermalTimer)
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stPPThermalTimer) > g_FMMC_STRUCT.iRATCThermalTimer*1000
			PRINTLN("[RCC MISSION] PROCESS_POWERPLAY_LOOP_SOUNDS - Ending Thermal.")
			RESET_NET_TIMER(stPPThermalTimer)
			STOP_SOUND(iPPThermalSoundID)
			STOP_AUDIO_SCENE(sPPThermalSceneSoundSet)
			SCRIPT_RELEASE_SOUND_ID(iPPThermalSoundID)
			
			IF NOT IS_STRING_NULL_OR_EMPTY(sPPThermalSoundSet)
				PLAY_SOUND_FRONTEND(-1, "Stop", sPPThermalSoundSet, FALSE)
			ENDIF
			
			iPPThermalSoundID = -1
		ELSE
			FLOAT fCompletionPercentage = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stPPThermalTimer)) / TO_FLOAT(g_FMMC_STRUCT.iRATCThermalTimer*1000)
			fCompletionPercentage = CLAMP(fCompletionPercentage, 0.0, 1.0)
			SET_VARIABLE_ON_SOUND(iPPThermalSoundID, "Control", fCompletionPercentage)
		ENDIF
	ENDIF

ENDPROC

PROC PICK_POWERPLAY_PRE_INTRO_ANNOUNCER(BOOL bEmergencyPick = FALSE)
	PRINTLN("[PP-ANNOUNCER] PICK_POWERPLAY_PRE_INTRO_ANNOUNCER - Called. bEmergencyPick = ", bEmergencyPick)
	
	TEXT_LABEL_15 tl15
	
	INT iRandomPreIntro = GET_RANDOM_INT_IN_RANGE(1, 11)
	PRINTLN("[PP-ANNOUNCER] PICK_POWERPLAY_PRE_INTRO_ANNOUNCER - iRandomPreIntro (1) = ", iRandomPreIntro)

	tl15 = "EXPOW_NEUT"
	tl15 += iRandomPreIntro
	
	PRINTLN("[PP-ANNOUNCER] PICK_POWERPLAY_PRE_INTRO_ANNOUNCER - Setting tl15PPPreIntroRoot as ", tl15)
	tl15PPPreIntroRoot = tl15

	IF NOT bEmergencyPick
		PRINTLN("[PP-ANNOUNCER] PICK_POWERPLAY_PRE_INTRO_ANNOUNCER - broadcasting to other players via BROADCAST_PP_PRE_INTRO_CHOSEN_ROOT")
		BROADCAST_PP_PRE_INTRO_CHOSEN_ROOT(GET_INT_FOR_PP_ANNOUNCER_ROOT(tl15PPPreIntroRoot))
	ENDIF

	SET_BIT(iLocalBoolCheck17, LBOOL17_HAS_PICKED_PP_PRE_INTRO_ANNOUNCER)
ENDPROC

PROC PICK_POWERPLAY_SUDDENDEATH_ANNOUNCER(BOOL bEmergencyPick = FALSE)
	PRINTLN("[PP-ANNOUNCER] PICK_POWERPLAY_SUDDENDEATH_ANNOUNCER - Called. bEmergencyPick = ", bEmergencyPick)
	
	TEXT_LABEL_15 tl15
	
	INT iRandomSDRoot = GET_RANDOM_INT_IN_RANGE(1, 5)
	PRINTLN("[PP-ANNOUNCER] PICK_POWERPLAY_SUDDENDEATH_ANNOUNCER - iRandomSDRoot (1) = ", iRandomSDRoot)

	tl15 = "EXPOW_NSUD"
	tl15 += iRandomSDRoot
	
	PRINTLN("[PP-ANNOUNCER] PICK_POWERPLAY_SUDDENDEATH_ANNOUNCER - Setting tl15PPSuddendeathRoot as ", tl15)
	tl15PPSuddendeathRoot = tl15
	
	IF NOT bEmergencyPick
		PRINTLN("[PP-ANNOUNCER] PICK_POWERPLAY_SUDDENDEATH_ANNOUNCER - broadcasting to other players via BROADCAST_PP_PRE_INTRO_CHOSEN_ROOT")
		BROADCAST_PP_SUDDDEATH_CHOSEN_ROOT(GET_INT_FOR_PP_ANNOUNCER_ROOT(tl15PPSuddendeathRoot))
	ENDIF

	SET_BIT(iLocalBoolCheck17, LBOOL17_HAS_PICKED_PP_SUDDDEATH_ANNOUNCER)
ENDPROC

PROC PICK_POWERPLAY_WINNER_ANNOUNCER(BOOL bEmergencyPick = FALSE)
	PRINTLN("[PP-ANNOUNCER] PICK_POWERPLAY_WINNER_ANNOUNCER - Called. bEmergencyPick = ", bEmergencyPick)
	
	TEXT_LABEL_15 tl15
	BOOL bFoundWinner
	INT i
	
	INT iRandomWinnerRoot = GET_RANDOM_INT_IN_RANGE(1, 4)
	INT iRandomWinnerPick = GET_RANDOM_INT_IN_RANGE(0, 1001)
	PRINTLN("[PP-ANNOUNCER] PICK_POWERPLAY_WINNER_ANNOUNCER - iRandomWinnerRoot (1) = ", iRandomWinnerRoot)
	PRINTLN("[PP-ANNOUNCER] PICK_POWERPLAY_WINNER_ANNOUNCER - iRandomWinnerPick (1) = ", iRandomWinnerPick)
	
	FOR i = 0 TO FMMC_MAX_TEAMS-1
		IF NOT bFoundWinner
		AND HAS_TEAM_PASSED_MISSION(i)
			bFoundWinner = TRUE
			
			IF iRandomWinnerPick <= 800 // 80% change of team specific
				TEXT_LABEL_15 tl15TeamName = GET_ALTERNATIVE_TEAM_NAME(MC_serverBD_3.iVersusOutfitStyleSetup, INT_TO_ENUM(VERSUS_OUTFIT_STYLE, MC_serverBD_3.iVersusOutfitStyleChoice), i)
				
				PRINTLN("[RCC MISSION][PP-ANNOUNCER]  PICK_POWERPLAY_WINNER_ANNOUNCER tl15TeamName = ", tl15TeamName, ", iRandomWinnerPick = ", iRandomWinnerPick)
				IF ARE_STRINGS_EQUAL(tl15TeamName, "TEAM_TOXIC_P")
					PRINTLN("[RCC MISSION][PP-ANNOUNCER]  PICK_POWERPLAY_WINNER_ANNOUNCER Toxic pests win! Team ", i, ", this team = ", MC_playerBD[iPartToUse].iteam)
					tl15 = "EXPOW_TOXIC"
				ELIF ARE_STRINGS_EQUAL(tl15TeamName, "TEAM_KILLER_B")
					PRINTLN("[RCC MISSION][PP-ANNOUNCER]  PICK_POWERPLAY_WINNER_ANNOUNCER Killer Bugs win! Team ", i, ", this team = ", MC_playerBD[iPartToUse].iteam)
					tl15 = "EXPOW_KILL"
				ELIF ARE_STRINGS_EQUAL(tl15TeamName, "TEAM_SHADES")
					PRINTLN("[RCC MISSION][PP-ANNOUNCER]  PICK_POWERPLAY_WINNER_ANNOUNCER Shades win! Team ", i, ", this team = ", MC_playerBD[iPartToUse].iteam)
					tl15 = "EXPOW_SHADE"
				ELIF ARE_STRINGS_EQUAL(tl15TeamName, "TEAM_RAINBOWS")
					PRINTLN("[RCC MISSION][PP-ANNOUNCER]  PICK_POWERPLAY_WINNER_ANNOUNCER Rainbows win! Team ", i, ", this team = ", MC_playerBD[iPartToUse].iteam)
					tl15 = "EXPOW_RAIN"
				ELIF ARE_STRINGS_EQUAL(tl15TeamName, "TEAM_ANIMALS")
					PRINTLN("[RCC MISSION][PP-ANNOUNCER]  PICK_POWERPLAY_WINNER_ANNOUNCER Animals win! Team ", i, ", this team = ", MC_playerBD[iPartToUse].iteam)
					tl15 = "EXPOW_ANIM"
				ELIF ARE_STRINGS_EQUAL(tl15TeamName, "TEAM_MUNCHIES")
					PRINTLN("[RCC MISSION][PP-ANNOUNCER]  PICK_POWERPLAY_WINNER_ANNOUNCER Munchies win! Team ", i, ", this team = ", MC_playerBD[iPartToUse].iteam)
					tl15 = "EXPOW_MUNCH"
				ELIF ARE_STRINGS_EQUAL(tl15TeamName, "TEAM_BOARS")
					PRINTLN("[RCC MISSION][PP-ANNOUNCER]  PICK_POWERPLAY_WINNER_ANNOUNCER Boars win! Team ", i, ", this team = ", MC_playerBD[iPartToUse].iteam)
					tl15 = "EXPOW_NBOAR"
				ELIF ARE_STRINGS_EQUAL(tl15TeamName, "TEAM_COCKS")
					PRINTLN("[RCC MISSION][PP-ANNOUNCER]  PICK_POWERPLAY_WINNER_ANNOUNCER Cocks win! Team ", i, ", this team = ", MC_playerBD[iPartToUse].iteam)
					tl15 = "EXPOW_NCOCK"
				ELSE
					#IF IS_DEBUG_BUILD
						SCRIPT_ASSERT("failed to find team! Using fallback")
						PRINTLN("[RCC MISSION][PP-ANNOUNCER]  PICK_POWERPLAY_WINNER_ANNOUNCER failed to find team! Using fallback")
					#ENDIF
					
					tl15 = "EXPOW_GENW"
				ENDIF
				
				tl15 += iRandomWinnerRoot
				
			ELSE
				iRandomWinnerRoot = GET_RANDOM_INT_IN_RANGE(1, 6)
				PRINTLN("[PP-ANNOUNCER] PICK_POWERPLAY_WINNER_ANNOUNCER - iRandomWinnerRoot (2) = ", iRandomWinnerRoot)
				PRINTLN("[RCC MISSION][PP-ANNOUNCER]  PICK_POWERPLAY_WINNER_ANNOUNCER picking general announcer. iRandomWinnerPick = ", iRandomWinnerPick)
				tl15 = "EXPOW_GENW"
				tl15 += iRandomWinnerRoot
			ENDIF
		ENDIF
	ENDFOR
	
	IF NOT bFoundWinner
		iRandomWinnerRoot = GET_RANDOM_INT_IN_RANGE(1, 6)
		PRINTLN("[PP-ANNOUNCER] PICK_POWERPLAY_WINNER_ANNOUNCER - iRandomWinnerRoot (3) = ", iRandomWinnerRoot)
		PRINTLN("[RCC MISSION][PP-ANNOUNCER]  PICK_POWERPLAY_WINNER_ANNOUNCER It's a tie!")
		tl15 = "EXPOW_NTIE"
		tl15 += iRandomWinnerRoot
	ENDIF
	
	PRINTLN("[PP-ANNOUNCER] PICK_POWERPLAY_WINNER_ANNOUNCER - Setting tl15PPWinnerRoot as ", tl15)
	tl15PPWinnerRoot = tl15
	
	IF NOT bEmergencyPick
		PRINTLN("[PP-ANNOUNCER] PICK_POWERPLAY_WINNER_ANNOUNCER - broadcasting to other players via BROADCAST_PP_WINNER_CHOSEN_ROOT")
		BROADCAST_PP_WINNER_CHOSEN_ROOT(GET_INT_FOR_PP_ANNOUNCER_ROOT(tl15PPWinnerRoot))
	ENDIF

	SET_BIT(iLocalBoolCheck17, LBOOL17_HAS_PICKED_PP_WINNER_ANNOUNCER)
ENDPROC

PROC PICK_POWERPLAY_EXIT_ANNOUNCER(BOOL bEmergencyPick = FALSE)
	PRINTLN("[PP-ANNOUNCER] PICK_POWERPLAY_EXIT_ANNOUNCER - Called. bEmergencyPick = ", bEmergencyPick)
	
	TEXT_LABEL_15 tl15
	
	INT iRandomPreIntro = GET_RANDOM_INT_IN_RANGE(1, 14)
	PRINTLN("[PP-ANNOUNCER] PICK_POWERPLAY_EXIT_ANNOUNCER - iRandomPreIntro (1) = ", iRandomPreIntro)
	
	IF iRandomPreIntro < 10
		tl15 = "EXPOW_NEXIT"
		tl15 += iRandomPreIntro
	ELSE
		tl15 = "EXPOW_NEXI"
		tl15 += iRandomPreIntro
	ENDIF
	
	PRINTLN("[PP-ANNOUNCER] PICK_POWERPLAY_EXIT_ANNOUNCER - Setting tl15PPExitRoot as ", tl15)
	tl15PPExitRoot = tl15
	
	IF NOT bEmergencyPick
		PRINTLN("[PP-ANNOUNCER] PICK_POWERPLAY_EXIT_ANNOUNCER - broadcasting to other players via BROADCAST_PP_EXIT_CHOSEN_ROOT")
		BROADCAST_PP_EXIT_CHOSEN_ROOT(GET_INT_FOR_PP_ANNOUNCER_ROOT(tl15PPExitRoot))
	ENDIF

	SET_BIT(iLocalBoolCheck17, LBOOL17_HAS_PICKED_PP_EXIT_ANNOUNCER)
ENDPROC

PROC PICK_POWERPLAY_INTRO_ANNOUNCER(BOOL bEmergencyPick = FALSE)
	PRINTLN("[PP-ANNOUNCER] PICK_POWERPLAY_INTRO_ANNOUNCER - Called. bEmergencyPick = ", bEmergencyPick)
	
	TEXT_LABEL_15 tl15
	
	INT iRandomChance = GET_RANDOM_INT_IN_RANGE(0, 1001)
	PRINTLN("[PP-ANNOUNCER] PICK_POWERPLAY_INTRO_ANNOUNCER - iRandomChance (1) = ", iRandomChance)
	
	//Play a generic intro announcer
	IF iRandomChance < 200 //20%
		PRINTLN("[PP-ANNOUNCER] PICK_POWERPLAY_INTRO_ANNOUNCER - In generic statement.")
		iRandomChance = GET_RANDOM_INT_IN_RANGE(1, 6)
		PRINTLN("[PP-ANNOUNCER] PICK_POWERPLAY_INTRO_ANNOUNCER - iRandomChance (2) = ", iRandomChance)
	
		tl15 = "EXPOW_STARG"
		tl15 += iRandomChance
		
	//play a team specific announcer
	ELSE
		PRINTLN("[PP-ANNOUNCER] PICK_POWERPLAY_INTRO_ANNOUNCER - In specific statement.")
		BOOL bToxicActive, bKillerActive, bShadesActive, bRainbowsActive, bAnimalsActive, bMunchiesActive, bBoarsActive, bCocksActive
		
		INT i
		FOR i = 0 TO FMMC_MAX_TEAMS-1
			TEXT_LABEL_15 tl15TeamName = GET_ALTERNATIVE_TEAM_NAME(MC_serverBD_3.iVersusOutfitStyleSetup, INT_TO_ENUM(VERSUS_OUTFIT_STYLE, MC_serverBD_3.iVersusOutfitStyleChoice), i)
			PRINTLN("[RCC MISSION][PP-ANNOUNCER] PICK_POWERPLAY_INTRO_ANNOUNCER tl15TeamName = ", tl15TeamName)
			
			IF ARE_STRINGS_EQUAL(tl15TeamName, "TEAM_TOXIC_P")
				PRINTLN("[RCC MISSION][PP-ANNOUNCER]  PICK_POWERPLAY_INTRO_ANNOUNCER team ", i, " are the toxic pests")
				bToxicActive = TRUE
			ELIF ARE_STRINGS_EQUAL(tl15TeamName, "TEAM_KILLER_B")
				PRINTLN("[RCC MISSION][PP-ANNOUNCER]  PICK_POWERPLAY_INTRO_ANNOUNCER team ", i, " are the killer bugs")
				bKillerActive = TRUE
			ELIF ARE_STRINGS_EQUAL(tl15TeamName, "TEAM_SHADES")
				PRINTLN("[RCC MISSION][PP-ANNOUNCER]  PICK_POWERPLAY_INTRO_ANNOUNCER team ", i, " are the shades")
				bShadesActive = TRUE
			ELIF ARE_STRINGS_EQUAL(tl15TeamName, "TEAM_RAINBOWS")
				PRINTLN("[RCC MISSION][PP-ANNOUNCER]  PICK_POWERPLAY_INTRO_ANNOUNCER team ", i, " are the rainbows")
				bRainbowsActive = TRUE
			ELIF ARE_STRINGS_EQUAL(tl15TeamName, "TEAM_ANIMALS")
				PRINTLN("[RCC MISSION][PP-ANNOUNCER]  PICK_POWERPLAY_INTRO_ANNOUNCER team ", i, " are the animals")
				bAnimalsActive = TRUE
			ELIF ARE_STRINGS_EQUAL(tl15TeamName, "TEAM_MUNCHIES")
				PRINTLN("[RCC MISSION][PP-ANNOUNCER]  PICK_POWERPLAY_INTRO_ANNOUNCER team ", i, " are the munchies")
				bMunchiesActive = TRUE
			ELIF ARE_STRINGS_EQUAL(tl15TeamName, "TEAM_BOARS")
				PRINTLN("[RCC MISSION][PP-ANNOUNCER]  PICK_POWERPLAY_INTRO_ANNOUNCER team ", i, " are the boars")
				bBoarsActive = TRUE
			ELIF ARE_STRINGS_EQUAL(tl15TeamName, "TEAM_COCKS")
				PRINTLN("[RCC MISSION][PP-ANNOUNCER]  PICK_POWERPLAY_INTRO_ANNOUNCER team ", i, " are the cocks")
				bCocksActive = TRUE
			ENDIF
		ENDFOR
		
		PRINTLN("[PP-ANNOUNCER] PICK_POWERPLAY_INTRO_ANNOUNCER - bToxicActive = ", bToxicActive, ", bKillerActive = ", bKillerActive, ", bShadesActive = ", bShadesActive, ", bRainbowsActive = ", bRainbowsActive)
		PRINTLN("[PP-ANNOUNCER] PICK_POWERPLAY_INTRO_ANNOUNCER - bAnimalsActive = ", bAnimalsActive, ", bMunchiesActive = ", bMunchiesActive, ", bBoarsActive = ", bBoarsActive, ", bCocksActive = ", bCocksActive)
		
		IF bBoarsActive AND bCocksActive
			IF GET_RANDOM_INT_IN_RANGE(1, 101) < 50
				tl15 = "EXPOW_NCVB"
			ELSE
				tl15 = "EXPOW_NBVC"
			ENDIF
		ELIF bToxicActive AND bKillerActive
			IF GET_RANDOM_INT_IN_RANGE(1, 101) < 50
				tl15 = "EXPOW_TVK"
			ELSE
				tl15 = "EXPOW_KVT"
			ENDIF
		ELIF bShadesActive AND bRainbowsActive
			IF GET_RANDOM_INT_IN_RANGE(1, 101) < 50
				tl15 = "EXPOW_RVS"
			ELSE
				tl15 = "EXPOW_SVR"
			ENDIF
		ELIF bAnimalsActive AND bMunchiesActive
			IF GET_RANDOM_INT_IN_RANGE(1, 101) < 50
				tl15 = "EXPOW_MVA"
			ELSE
				tl15 = "EXPOW_AVM"
			ENDIF
		ELSE
			PRINTLN("[PP-ANNOUNCER] PICK_POWERPLAY_INTRO_ANNOUNCER - In specific statement fallback.")
			iRandomChance = GET_RANDOM_INT_IN_RANGE(1, 6)
			PRINTLN("[PP-ANNOUNCER] PICK_POWERPLAY_INTRO_ANNOUNCER - iRandomChance (3) = ", iRandomChance)
		
			tl15 = "EXPOW_STARG"
			tl15 += iRandomChance
		ENDIF
	ENDIF
	
	PRINTLN("[PP-ANNOUNCER] PICK_POWERPLAY_INTRO_ANNOUNCER - Setting tl15PPTeamIntroRoot as ", tl15)
	tl15PPTeamIntroRoot = tl15
	
	IF NOT bEmergencyPick
		PRINTLN("[PP-ANNOUNCER] PICK_POWERPLAY_INTRO_ANNOUNCER - broadcasting to other players via BROADCAST_PP_INTRO_CHOSEN_TEAM_ROOT")
		BROADCAST_PP_INTRO_CHOSEN_TEAM_ROOT(GET_INT_FOR_PP_ANNOUNCER_ROOT(tl15PPTeamIntroRoot))
	ENDIF
		
	SET_BIT(iLocalBoolCheck17, LBOOL17_HAS_PICKED_PP_INTRO_ANNOUNCER)
ENDPROC

PROC PLAY_POWERPLAY_INTRO_ANNOUNCER()
	PRINTLN("[PP-ANNOUNCER] PLAY_POWERPLAY_INTRO_ANNOUNCER - Called")
	
	IF IS_STRING_NULL_OR_EMPTY(tl15PPTeamIntroRoot)
	OR ARE_STRINGS_EQUAL(tl15PPTeamIntroRoot, "INVALID_ROOT")
		#IF IS_DEBUG_BUILD
			IF ARE_STRINGS_EQUAL(tl15PPTeamIntroRoot, "INVALID_ROOT")
				PRINTLN("[PP-ANNOUNCER] PLAY_POWERPLAY_INTRO_ANNOUNCER - tl15PPTeamIntroRoot = INVALID_ROOT :(")
			ENDIF
		#ENDIF
		PRINTLN("[PP-ANNOUNCER] PLAY_POWERPLAY_INTRO_ANNOUNCER - tl15PPTeamIntroRoot empty or null: ", tl15PPTeamIntroRoot, ". Picking my own as a fallback.")
		PICK_POWERPLAY_INTRO_ANNOUNCER(TRUE)
	ENDIF
	
	PRINTLN("[PP-ANNOUNCER] PLAY_POWERPLAY_INTRO_ANNOUNCER - Calling CREATE_CONVERSATION with tl15PPTeamIntroRoot = ", tl15PPTeamIntroRoot)
	
	ADD_PED_FOR_DIALOGUE(powerplayAnnouncer, 2, NULL, "EXEC1_POWERANN")
	CREATE_CONVERSATION(powerplayAnnouncer, "EXPOWAU", tl15PPTeamIntroRoot, CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
	SET_BIT(iLocalBoolCheck17, LBOOL17_HAS_PLAYED_PP_INTRO_ANNOUNCER)
ENDPROC

PROC PLAY_POWERPLAY_PRE_INTRO_ANNOUNCER()
	PRINTLN("PLAY_POWERPLAY_PRE_INTRO_ANNOUNCER - Called")

	IF IS_STRING_NULL_OR_EMPTY(tl15PPPreIntroRoot)
	OR ARE_STRINGS_EQUAL(tl15PPPreIntroRoot, "INVALID_ROOT")
		#IF IS_DEBUG_BUILD
			IF ARE_STRINGS_EQUAL(tl15PPPreIntroRoot, "INVALID_ROOT")
				PRINTLN("PLAY_POWERPLAY_PRE_INTRO_ANNOUNCER - tl15PPPreIntroRoot = INVALID_ROOT :(")
			ENDIF
		#ENDIF
		PRINTLN("[PP-ANNOUNCER] PLAY_POWERPLAY_PRE_INTRO_ANNOUNCER - tl15PPPreIntroRoot empty or null: ", tl15PPPreIntroRoot, ". Picking my own as a fallback.")
		PICK_POWERPLAY_PRE_INTRO_ANNOUNCER(TRUE)
	ENDIF
	
	PRINTLN("[PP-ANNOUNCER] PLAY_POWERPLAY_PRE_INTRO_ANNOUNCER - Calling CREATE_CONVERSATION with tl15PPPreIntroRoot = ", tl15PPPreIntroRoot)
	
	ADD_PED_FOR_DIALOGUE(powerplayAnnouncer, 2, NULL, "EXEC1_POWERANN")
	CREATE_CONVERSATION(powerplayAnnouncer, "EXPOWAU", tl15PPPreIntroRoot, CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
	SET_BIT(iLocalBoolCheck17, LBOOL17_HAS_PLAYED_PP_PRE_INTRO_ANNOUNCER)
ENDPROC

PROC PLAY_POWERPLAY_WINNER_ANNOUNCER()
	IF IS_STRING_NULL_OR_EMPTY(tl15PPWinnerRoot)
	OR ARE_STRINGS_EQUAL(tl15PPWinnerRoot, "INVALID_ROOT")
		#IF IS_DEBUG_BUILD
			IF ARE_STRINGS_EQUAL(tl15PPWinnerRoot, "INVALID_ROOT")
				PRINTLN("[PP-ANNOUNCER] PLAY_POWERPLAY_WINNER_ANNOUNCER - tl15PPWinnerRoot = INVALID_ROOT :(")
			ENDIF
		#ENDIF
		IF iPPSuddenDeathWaitForEvent < ciPP_SUDDDEATH_WAIT_FOR_HOST_DELAY
			iPPSuddenDeathWaitForEvent++
		ELSE
			PICK_POWERPLAY_WINNER_ANNOUNCER(TRUE)
			PRINTLN("[RCC MISSION][PP-ANNOUNCER]  PLAY_POWERPLAY_WINNER_ANNOUNCER Picked own announcer. Host event took too long!")
		ENDIF
	ELSE
		PRINTLN("[RCC MISSION][PP-ANNOUNCER]  PLAY_POWERPLAY_WINNER_ANNOUNCER Playing winner announcer: ", tl15PPWinnerRoot)
		ADD_PED_FOR_DIALOGUE(powerplayAnnouncer, 2, NULL, "EXEC1_POWERANN")
		CREATE_CONVERSATION(powerplayAnnouncer, "EXPOWAU", tl15PPWinnerRoot, CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
		
		SET_BIT(iLocalBoolCheck17, LBOOL17_HAS_PLAYED_PP_WINNER_ANNOUNCER)
	ENDIF
ENDPROC

PROC PLAY_POWERPLAY_EXIT_ANNOUNCER()
	PRINTLN("[PP-ANNOUNCER] PLAY_POWERPLAY_EXIT_ANNOUNCER - Called")

	IF IS_STRING_NULL_OR_EMPTY(tl15PPExitRoot)
	OR ARE_STRINGS_EQUAL(tl15PPExitRoot, "INVALID_ROOT")
		#IF IS_DEBUG_BUILD
			IF ARE_STRINGS_EQUAL(tl15PPExitRoot, "INVALID_ROOT")
				PRINTLN("[PP-ANNOUNCER] PLAY_POWERPLAY_EXIT_ANNOUNCER - tl15PPExitRoot = INVALID_ROOT :(")
			ENDIF
		#ENDIF
		PRINTLN("[PP-ANNOUNCER] PLAY_POWERPLAY_EXIT_ANNOUNCER - tl15PPExitRoot empty or null: ", tl15PPExitRoot, ". Picking my own as a fallback.")
		PICK_POWERPLAY_EXIT_ANNOUNCER(TRUE)
	ENDIF
	
	PRINTLN("[PP-ANNOUNCER] PLAY_POWERPLAY_EXIT_ANNOUNCER - Calling CREATE_CONVERSATION with tl15PPExitRoot = ", tl15PPExitRoot)
	
	ADD_PED_FOR_DIALOGUE(powerplayAnnouncer, 2, NULL, "EXEC1_POWERANN")
	CREATE_CONVERSATION(powerplayAnnouncer, "EXPOWAU", tl15PPExitRoot, CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
	SET_BIT(iLocalBoolCheck17, LBOOL17_HAS_PLAYED_PP_EXIT_ANNOUNCER)
ENDPROC


PROC PLAY_RAGE_PP_ANNOUNCER(INT iPowerupType, INT iSenderTeam, PLAYER_INDEX piSenderPlayer, BOOL bPlayingCachedSound = FALSE)
	
	IF g_bMissionOver
		PRINTLN("[PP-ANNOUNCER] Call to PLAY_RAGE_PP_ANNOUNCER was blocked because g_bMissionOver = TRUE")
		EXIT
	ENDIF
	
	PRINTLN("### [PP-ANNOUNCER] PLAY_RAGE_PP_ANNOUNCER - iPowerupType = ", iPowerupType, ", iSenderTeam = ", iSenderTeam,
			", myTeam = ", MC_playerBD[iPartToUse].iteam)
	
	IF iPowerupType != ciCUSTOM_PICKUP_TYPE__SUDDEN_DEATH
		//if this is not a cached call and we have already cached something
		IF (NOT bPlayingCachedSound AND sCachedAnnouncement.iPowerupType != -1)
		OR IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			#IF IS_DEBUG_BUILD
				PRINTLN("[PP-ANNOUNCER] PLAY_RAGE_PP_ANNOUNCER - announcer is either already playing or cached. ", 
						bPlayingCachedSound, ", ", sCachedAnnouncement.iPowerupType, ", ", IS_ANY_CONVERSATION_ONGOING_OR_QUEUED())
			#ENDIF
			EXIT
		ENDIF
	
		IF sCachedAnnouncement.iPowerupType = -1
			PRINTLN("[PP-ANNOUNCER] PLAY_RAGE_PP_ANNOUNCER - caching iPowerupType ", iPowerupType)
			sCachedAnnouncement.iPowerupType	= iPowerupType
			sCachedAnnouncement.iSenderTeam		= iSenderTeam
			sCachedAnnouncement.piSenderPlayer	= piSenderPlayer
			sCachedAnnouncement.iFrameTimeAdded = GET_FRAME_COUNT()
			
			REINIT_NET_TIMER(stPowerupAnnouncerDelayTimer)
			EXIT
		ENDIF
	ENDIF	
	
	ADD_PED_FOR_DIALOGUE(powerplayAnnouncer, 2, NULL, "EXEC1_POWERANN")
	
	IF iPowerupType = ciCUSTOM_PICKUP_TYPE__SUDDEN_DEATH
		PRINTLN("[PP-ANNOUNCER] PLAY_RAGE_PP_ANNOUNCER - Playing sudden death announcer.")
		
		IF IS_STRING_NULL_OR_EMPTY(tl15PPSuddendeathRoot)
			PRINTLN("[PP-ANNOUNCER] PLAY_POWERPLAY_INTRO_ANNOUNCER - tl15PPSuddendeathRoot empty or null: ", tl15PPSuddendeathRoot, ". Picking my own as a fallback.")
			PICK_POWERPLAY_SUDDENDEATH_ANNOUNCER(TRUE)
		ENDIF
		
		PRINTLN("[PP-ANNOUNCER] PLAY_RAGE_PP_ANNOUNCER - sudden death announcer is ", tl15PPSuddendeathRoot)
		CREATE_CONVERSATION(powerplayAnnouncer, "EXPOWAU", tl15PPSuddendeathRoot, CONV_PRIORITY_VERY_HIGH)
		EXIT
	ENDIF
	//TEXT_LABEL_15 tl15TeamName = GET_ALTERNATIVE_TEAM_NAME(MC_serverBD_3.iVersusOutfitStyleSetup, INT_TO_ENUM(VERSUS_OUTFIT_STYLE, MC_serverBD_3.iVersusOutfitStyleChoice), i)
	
	TEXT_LABEL_15 sRootToPlay
	BOOL bToxicSender, bKillerSender, bShadesSender, bRainbowsSender, bAnimalsSender, bMunchiesSender, bBoarsSender, bCocksSender
	TEXT_LABEL_15 tl15TeamName = GET_ALTERNATIVE_TEAM_NAME(MC_serverBD_3.iVersusOutfitStyleSetup, INT_TO_ENUM(VERSUS_OUTFIT_STYLE, MC_serverBD_3.iVersusOutfitStyleChoice), iSenderTeam)
	TEXT_LABEL_15 tl15MyTeamname = GET_ALTERNATIVE_TEAM_NAME(MC_serverBD_3.iVersusOutfitStyleSetup, INT_TO_ENUM(VERSUS_OUTFIT_STYLE, MC_serverBD_3.iVersusOutfitStyleChoice), MC_playerBD[iPartToUse].iteam)
	PRINTLN("[PP-ANNOUNCER] PLAY_RAGE_PP_ANNOUNCER tl15TeamName = ", tl15TeamName)
	PRINTLN("[PP-ANNOUNCER] PLAY_RAGE_PP_ANNOUNCER tl15MyTeamname = ", tl15MyTeamname)
	
	IF ARE_STRINGS_EQUAL(tl15TeamName, "TEAM_TOXIC_P")
		PRINTLN("[PP-ANNOUNCER]  PLAY_RAGE_PP_ANNOUNCER Sender team ", iSenderTeam, " are the toxic pests")
		bToxicSender = TRUE
	ELIF ARE_STRINGS_EQUAL(tl15TeamName, "TEAM_KILLER_B")
		PRINTLN("[PP-ANNOUNCER]  PLAY_RAGE_PP_ANNOUNCER Sender team ", iSenderTeam, " are the killer bugs")
		bKillerSender = TRUE
	ELIF ARE_STRINGS_EQUAL(tl15TeamName, "TEAM_SHADES")
		PRINTLN("[PP-ANNOUNCER]  PLAY_RAGE_PP_ANNOUNCER Sender team ", iSenderTeam, " are the shades")
		bShadesSender = TRUE
	ELIF ARE_STRINGS_EQUAL(tl15TeamName, "TEAM_RAINBOWS")
		PRINTLN("[RCC MISSION][PP-ANNOUNCER]  PLAY_RAGE_PP_ANNOUNCER Sender team ", iSenderTeam, " are the rainbows")
		bRainbowsSender = TRUE
	ELIF ARE_STRINGS_EQUAL(tl15TeamName, "TEAM_ANIMALS")
		PRINTLN("[PP-ANNOUNCER]  PLAY_RAGE_PP_ANNOUNCER Sender team ", iSenderTeam, " are the animals")
		bAnimalsSender = TRUE
	ELIF ARE_STRINGS_EQUAL(tl15TeamName, "TEAM_MUNCHIES")
		PRINTLN("[PP-ANNOUNCER]  PLAY_RAGE_PP_ANNOUNCER Sender team ", iSenderTeam, " are the munchies")
		bMunchiesSender = TRUE
	ELIF ARE_STRINGS_EQUAL(tl15TeamName, "TEAM_BOARS")
		PRINTLN("[PP-ANNOUNCER]  PLAY_RAGE_PP_ANNOUNCER Sender team ", iSenderTeam, " are the boars")
		bBoarsSender = TRUE
	ELIF ARE_STRINGS_EQUAL(tl15TeamName, "TEAM_COCKS")
		PRINTLN("[PP-ANNOUNCER]  PLAY_RAGE_PP_ANNOUNCER Sender team ", iSenderTeam, " are the cocks")
		bCocksSender = TRUE
	ENDIF
	
	PRINTLN("[PP-ANNOUNCER] PLAY_RAGE_PP_ANNOUNCER - bToxicSender = ", bToxicSender, ", bKillerSender = ", bKillerSender, ", bShadesSender = ", bShadesSender, ", bRainbowsSender = ", bRainbowsSender)
	PRINTLN("[PP-ANNOUNCER] PLAY_RAGE_PP_ANNOUNCER - bAnimalsSender = ", bAnimalsSender, ", bMunchiesSender = ", bMunchiesSender, ", bBoarsSender = ", bBoarsSender, ", bCocksSender = ", bCocksSender)
		
	sRootToPlay = "EXPOW_"

	SWITCH iPowerupType
		CASE ciCUSTOM_PICKUP_TYPE__BULLSHARK_TEST	sRootToPlay += "BS"		BREAK	
		CASE ciCUSTOM_PICKUP_TYPE__BEAST_MODE		sRootToPlay += "UB" 	BREAK
		CASE ciCUSTOM_PICKUP_TYPE__SWAP  			sRootToPlay += "RE" 	BREAK //AIM REVERSAL
		CASE ciCUSTOM_PICKUP_TYPE__FILTER 			sRootToPlay += "ST" 	BREAK //WEED FILTER
		CASE ciCUSTOM_PICKUP_TYPE__BULLET_TIME 		sRootToPlay += "BT" 	BREAK
		CASE ciCUSTOM_PICKUP_TYPE__HIDE_BLIPS 		sRootToPlay += "BL" 	BREAK
	ENDSWITCH
	
	IF iPowerupType = ciCUSTOM_PICKUP_TYPE__BULLET_TIME //we play the you are in the zone no matter which teams picks it up
		sRootToPlay += "YOUN"
	ELSE
		IF ARE_STRINGS_EQUAL(tl15MyTeamname, tl15TeamName)//if we sent the event
		
			//we want to say they got doped, or their controls are swapped if we collected the powerup
			IF iPowerupType = ciCUSTOM_PICKUP_TYPE__FILTER
			OR iPowerupType = ciCUSTOM_PICKUP_TYPE__SWAP
				TEXT_LABEL_15 tl15OtherTeam
				IF MC_playerBD[iPartToUse].iteam = 1
					tl15OtherTeam = GET_ALTERNATIVE_TEAM_NAME(MC_serverBD_3.iVersusOutfitStyleSetup, INT_TO_ENUM(VERSUS_OUTFIT_STYLE, MC_serverBD_3.iVersusOutfitStyleChoice), 0)
				ELIF MC_playerBD[iPartToUse].iteam = 0
					tl15OtherTeam = GET_ALTERNATIVE_TEAM_NAME(MC_serverBD_3.iVersusOutfitStyleSetup, INT_TO_ENUM(VERSUS_OUTFIT_STYLE, MC_serverBD_3.iVersusOutfitStyleChoice), 1)
				ENDIF
				
				PRINTLN("[PP-ANNOUNCER] PLAY_RAGE_PP_ANNOUNCER tl15OtherTeam = ", tl15OtherTeam)
				
				bToxicSender = FALSE
				bKillerSender = FALSE
				bShadesSender = FALSE
				bRainbowsSender = FALSE
				bAnimalsSender = FALSE
				bMunchiesSender = FALSE
				bBoarsSender = FALSE
				bCocksSender = FALSE
				
				IF ARE_STRINGS_EQUAL(tl15OtherTeam, "TEAM_TOXIC_P")
					PRINTLN("[PP-ANNOUNCER]  PLAY_RAGE_PP_ANNOUNCER tl15OtherTeam ", iSenderTeam, " are the toxic pests")
					bToxicSender = TRUE
				ELIF ARE_STRINGS_EQUAL(tl15OtherTeam, "TEAM_KILLER_B")
					PRINTLN("[PP-ANNOUNCER] (2) PLAY_RAGE_PP_ANNOUNCER tl15OtherTeam ", iSenderTeam, " are the killer bugs")
					bKillerSender = TRUE
				ELIF ARE_STRINGS_EQUAL(tl15OtherTeam, "TEAM_SHADES")
					PRINTLN("[PP-ANNOUNCER] (2) PLAY_RAGE_PP_ANNOUNCER tl15OtherTeam ", iSenderTeam, " are the shades")
					bShadesSender = TRUE
				ELIF ARE_STRINGS_EQUAL(tl15OtherTeam, "TEAM_RAINBOWS")
					PRINTLN("[PP-ANNOUNCER] (2) PLAY_RAGE_PP_ANNOUNCER tl15OtherTeam ", iSenderTeam, " are the rainbows")
					bRainbowsSender = TRUE
				ELIF ARE_STRINGS_EQUAL(tl15OtherTeam, "TEAM_ANIMALS")
					PRINTLN("[PP-ANNOUNCER] (2) PLAY_RAGE_PP_ANNOUNCER tl15OtherTeam ", iSenderTeam, " are the animals")
					bAnimalsSender = TRUE
				ELIF ARE_STRINGS_EQUAL(tl15OtherTeam, "TEAM_MUNCHIES")
					PRINTLN("[PP-ANNOUNCER] (2) PLAY_RAGE_PP_ANNOUNCER tl15OtherTeam ", iSenderTeam, " are the munchies")
					bMunchiesSender = TRUE
				ELIF ARE_STRINGS_EQUAL(tl15OtherTeam, "TEAM_BOARS")
					PRINTLN("[PP-ANNOUNCER] (2) PLAY_RAGE_PP_ANNOUNCER tl15OtherTeam ", iSenderTeam, " are the boars")
					bBoarsSender = TRUE
				ELIF ARE_STRINGS_EQUAL(tl15OtherTeam, "TEAM_COCKS")
					PRINTLN("[PP-ANNOUNCER] (2) PLAY_RAGE_PP_ANNOUNCER tl15OtherTeam ", iSenderTeam, " are the cocks")
					bCocksSender = TRUE
				ENDIF
				
				PRINTLN("[PP-ANNOUNCER] PLAY_RAGE_PP_ANNOUNCER - (2) bToxicSender = ", bToxicSender, ", bKillerSender = ", bKillerSender, ", bShadesSender = ", bShadesSender, ", bRainbowsSender = ", bRainbowsSender)
				PRINTLN("[PP-ANNOUNCER] PLAY_RAGE_PP_ANNOUNCER - (2) bAnimalsSender = ", bAnimalsSender, ", bMunchiesSender = ", bMunchiesSender, ", bBoarsSender = ", bBoarsSender, ", bCocksSender = ", bCocksSender)
				
				IF 		bToxicSender		sRootToPlay += "TOX"
				ELIF 	bKillerSender		sRootToPlay += "KILL"
				ELIF 	bShadesSender		sRootToPlay += "SHD"
				ELIF 	bRainbowsSender		sRootToPlay += "RAIN"
				ELIF 	bAnimalsSender		sRootToPlay += "ANIM"
				ELIF 	bMunchiesSender		sRootToPlay += "MNC"
				ELIF 	bBoarsSender		sRootToPlay += PICK_STRING(iPowerupType = ciCUSTOM_PICKUP_TYPE__BULLET_TIME, "BOAN", "BOAR")
				ELIF 	bCocksSender		sRootToPlay += PICK_STRING(iPowerupType = ciCUSTOM_PICKUP_TYPE__BULLET_TIME, "COCN", "COCK")
				ENDIF
			ELSE
				PRINTLN("[PP-ANNOUNCER] (2) PLAY_RAGE_PP_ANNOUNCER our team sent, got YOUN")
				sRootToPlay += "YOUN"
			ENDIF
		ELIF (iPowerupType = ciCUSTOM_PICKUP_TYPE__FILTER   //or our team got doped
			  OR iPowerupType = ciCUSTOM_PICKUP_TYPE__SWAP) //our team got reversed
			sRootToPlay += "YOUN"
		ELSE
			//IF GET_RANDOM_INT_IN_RANGE(0, 1001) < 100 //10% chance for they generic string
			//	sRootToPlay += "THEY"
			//ELSE
				IF 		bToxicSender		sRootToPlay += "TOX"
				ELIF 	bKillerSender		sRootToPlay += "KILL"
				ELIF 	bShadesSender		sRootToPlay += "SHD"
				ELIF 	bRainbowsSender		sRootToPlay += "RAIN"
				ELIF 	bAnimalsSender		sRootToPlay += "ANIM"
				ELIF 	bMunchiesSender		sRootToPlay += "MNC"
				ELIF 	bBoarsSender		sRootToPlay += PICK_STRING(iPowerupType = ciCUSTOM_PICKUP_TYPE__BULLET_TIME, "BOAN", "BOAR")
				ELIF 	bCocksSender		sRootToPlay += PICK_STRING(iPowerupType = ciCUSTOM_PICKUP_TYPE__BULLET_TIME, "COCN", "COCK")
				ENDIF
			//ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		PRINTLN("[RCC MISSION][PP-ANNOUNCER] PLAY_RAGE_PP_ANNOUNCER - Playing sRootToPlay = ", sRootToPlay)
		CREATE_CONVERSATION(powerplayAnnouncer, "EXPOWAU", sRootToPlay, CONV_PRIORITY_VERY_HIGH)
	ELSE
		PRINTLN("[RCC MISSION][PP-ANNOUNCER] PLAY_RAGE_PP_ANNOUNCER - got through caching and conversation IS playing :( sRootToPlay = ", sRootToPlay)
	ENDIF
ENDPROC

PROC PROCESS_CACHED_PP_ANNOUNCER_CALLS()
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
		IF sCachedAnnouncement.iPowerupType != -1
			IF HAS_NET_TIMER_STARTED(stPowerupAnnouncerDelayTimer)
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stPowerupAnnouncerDelayTimer) > 500
					PLAY_RAGE_PP_ANNOUNCER(sCachedAnnouncement.iPowerupType, 
										   sCachedAnnouncement.iSenderTeam, 
										   sCachedAnnouncement.piSenderPlayer, TRUE)
					
					sCachedAnnouncement.iPowerupType = -1
					sCachedAnnouncement.iSenderTeam = -1
					sCachedAnnouncement.piSenderPlayer = NULL	
				ENDIF
			ELSE
				START_NET_TIMER(stPowerupAnnouncerDelayTimer)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//VEHICULAR VENDETTA SOUNDS

PROC PLAY_VEHICLE_VENDETTA_SOUND(INT iPowerupType, INT iSenderTeam, PLAYER_INDEX piSenderPlayer)

	STRING sSoundSet, sSceneSoundSet
	
	PARTICIPANT_INDEX piTemp
	
	SWITCH iPowerupType
		//done
		CASE ciVEH_WEP_ROCKETS	
			IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARS(g_FMMC_STRUCT.iAdversaryModeType)
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					IF iSenderTeam = MC_playerBD[iPartToUse].iteam
						IF piSenderPlayer = LocalPlayer
							sSoundSet = "DLC_SR_TR_Rocket_Player_Sounds"
							sSceneSoundSet = ""
						ELSE
							sSoundSet = "DLC_SR_TR_Rocket_Player_Sounds"
							sSceneSoundSet = ""
						ENDIF
					ELSE
						sSoundSet = "DLC_SR_TR_Rocket_Enemy_Sounds"
						sSceneSoundSet = ""
					ENDIF
				ELSE
					IF iSenderTeam = MC_playerBD[iPartToUse].iteam
						IF piSenderPlayer = LocalPlayer
							sSoundSet = "DLC_IE_VV_Rocket_Player_Sounds"
							sSceneSoundSet = ""
						ELSE
							sSoundSet = "DLC_IE_VV_Rocket_Team_Sounds"
							sSceneSoundSet = ""
						ENDIF
					ELSE
						sSoundSet = "DLC_IE_VV_Rocket_Enemy_Sounds"
						sSceneSoundSet = ""
					ENDIF
				ENDIF
				
				sVVRocketsSoundSet = sSoundSet
				sVVRocketsSceneSoundSet = sSceneSoundSet

				IF iVVRocketsSoundID = -1
					iVVRocketsSoundID  = GET_SOUND_ID()
					PLAY_SOUND_FRONTEND(iVVRocketsSoundID , "Loop", sVVRocketsSoundSet, FALSE)
					PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND - Playing Loop:", sSoundSet)
				ENDIF
			ENDIF
		BREAK
		
		//done
		CASE ciVEH_WEP_GHOST	
			IF iSenderTeam = MC_playerBD[iPartToUse].iteam
				IF piSenderPlayer = LocalPlayer
					sSoundSet = "DLC_IE_VV_Ghost_Player_Sounds"
					sSceneSoundSet = "DLC_IE_VV_Ghost_Scene"
				ELSE
					sSoundSet = "DLC_IE_VV_Ghost_Team_Sounds"
					sSceneSoundSet = ""
				ENDIF
			ELSE
				sSoundSet = "DLC_IE_VV_Ghost_Enemy_Sounds"
				sSceneSoundSet = ""
			ENDIF
			
			IF iSenderTeam = 0
				sVVGhostSoundSet = sSoundSet
				sVVGhostSceneSoundSet = sSceneSoundSet
			ELSE
				sVVGhostSoundSet1 = sSoundSet
				sVVGhostSceneSoundSet = sSceneSoundSet
			ENDIF
			
			IF iSenderTeam = 0
				IF iVVGhostSoundID = -1
					iVVGhostSoundID = GET_SOUND_ID()
					PLAY_SOUND_FRONTEND(iVVGhostSoundID, "Loop", sVVGhostSoundSet, FALSE)
					START_NET_TIMER(tdVehAudGhost0)
					PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND Team 0 - Playing Loop:", sSoundSet)
				ENDIF
			ELSE
				IF iVVGhostSoundID1 = -1
					iVVGhostSoundID1 = GET_SOUND_ID()
					PLAY_SOUND_FRONTEND(iVVGhostSoundID1, "Loop", sVVGhostSoundSet1, FALSE)
					START_NET_TIMER(tdVehAudGhost1)
					PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND Team 1 - Playing Loop:", sSoundSet)
				ENDIF
			ENDIF
		BREAK	
		
		//done
		CASE ciVEH_WEP_BEAST	
			IF iSenderTeam = MC_playerBD[iPartToUse].iteam
				IF piSenderPlayer = LocalPlayer
					sSoundSet = "DLC_IE_VV_Beast_Player_Sounds"
					sSceneSoundSet = "DLC_IE_VV_Beast_Scene"
				ELSE
					sSoundSet = "DLC_IE_VV_Beast_Team_Sounds"
					sSceneSoundSet = ""
				ENDIF
			ELSE
				sSoundSet = "DLC_IE_VV_Beast_Enemy_Sounds"
				sSceneSoundSet = ""
			ENDIF
			
			IF iSenderTeam = 0
				sVVBeastmodeSoundSet = sSoundSet
				sVVBeastmodeSceneSoundSet = sSceneSoundSet
			ELSE
				sVVBeastmodeSoundSet1 = sSoundSet
				sVVBeastmodeSceneSoundSet = sSceneSoundSet
			ENDIF
			
			IF iSenderTeam = 0
				IF iVVBeastmodeSoundID  = -1
					iVVBeastmodeSoundID  = GET_SOUND_ID()
					PLAY_SOUND_FRONTEND(iVVBeastmodeSoundID, "Loop", sVVBeastmodeSoundSet, FALSE)
					START_NET_TIMER(tdVehAudBeast0)
					PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND Team 0 - Playing Loop:", sSoundSet)
				ENDIF
			ELSE
				IF iVVBeastmodeSoundID1  = -1
					iVVBeastmodeSoundID1  = GET_SOUND_ID()
					PLAY_SOUND_FRONTEND(iVVBeastmodeSoundID1, "Loop", sVVBeastmodeSoundSet1, FALSE)
					START_NET_TIMER(tdVehAudBeast1)
					PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND Team 1 - Playing Loop:", sSoundSet)
				ENDIF
			ENDIF
		BREAK
		
		//done
		CASE ciVEH_WEP_FORCE_ACCELERATE
			IF iSenderTeam = MC_playerBD[iPartToUse].iteam
				IF piSenderPlayer = LocalPlayer
					sSoundSet = "DLC_IE_VV_Jammed_Player_Sounds"
					sSceneSoundSet = ""
				ELSE
					sSoundSet = "DLC_IE_VV_Jammed_Team_Sounds"
					sSceneSoundSet = ""
				ENDIF
			ELSE
				sSoundSet = "DLC_IE_VV_Jammed_Enemy_Sounds"
				sSceneSoundSet = "DLC_IE_VV_Jammed_Scene"
			ENDIF
			
			IF iSenderTeam = 0
				sVVForcedAccSoundSet = sSoundSet
				sVVForcedAccSceneSoundSet = sSceneSoundSet
			ELSE
				sVVForcedAccSoundSet1 = sSoundSet
				sVVForcedAccSceneSoundSet = sSceneSoundSet
			ENDIF
			
			IF iSenderTeam = 0
				IF iVVForcedAccSoundID= -1
					iVVForcedAccSoundID = GET_SOUND_ID()
					PLAY_SOUND_FRONTEND(iVVForcedAccSoundID, "Loop", sVVForcedAccSoundSet, FALSE)
					START_NET_TIMER(tdVehAudAccelerate0)
					PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND Team 0 - Playing Loop:", sSoundSet)
				ENDIF
			ELSE
				IF iVVForcedAccSoundID1 = -1
					iVVForcedAccSoundID1 = GET_SOUND_ID()
					PLAY_SOUND_FRONTEND(iVVForcedAccSoundID1, "Loop", sVVForcedAccSoundSet1, FALSE)
					START_NET_TIMER(tdVehAudAccelerate1)
					PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND Team 1 - Playing Loop:", sSoundSet)
				ENDIF
			ENDIF
		BREAK
		
		//done
		CASE ciVEH_WEP_FLIPPED_CONTROLS	
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
				IF iSenderTeam = MC_playerBD[iPartToUse].iteam
					IF piSenderPlayer = LocalPlayer
						sSoundSet = "DLC_SR_TR_Flipped_Player_Sounds"
						sSceneSoundSet = ""
					ELSE
						sSoundSet = "DLC_SR_TR_Flipped_Player_Sounds"
						sSceneSoundSet = ""
					ENDIF
				ELSE
					sSoundSet = "DLC_SR_TR_Flipped_Enemy_Sounds"
					sSceneSoundSet = ""
				ENDIF
			ELSE
				IF iSenderTeam = MC_playerBD[iPartToUse].iteam
					IF piSenderPlayer = LocalPlayer
						sSoundSet = "DLC_IE_VV_Flipped_Player_Sounds"
						sSceneSoundSet = ""
					ELSE
						sSoundSet = "DLC_IE_VV_Flipped_Team_Sounds"
						sSceneSoundSet = ""
					ENDIF
				ELSE
					sSoundSet = "DLC_IE_VV_Flipped_Enemy_Sounds"
					sSceneSoundSet = ""
				ENDIF
			ENDIF
			
			IF iSenderTeam = 0
				sVVFlippedSoundSet = sSoundSet
				sVVFlippedSceneSoundSet = sSceneSoundSet
				iVVFlippedPartSender = NATIVE_TO_INT(piSenderPlayer)
			ELIF iSenderTeam = 1
				sVVFlippedSoundSet1 = sSoundSet
				sVVFlippedSceneSoundSet = sSceneSoundSet
				iVVFlippedPartSender1 = NATIVE_TO_INT(piSenderPlayer)
			ELIF iSenderTeam = 2
				sVVFlippedSoundSet2 = sSoundSet
				sVVFlippedSceneSoundSet = sSceneSoundSet
				iVVFlippedPartSender2 = NATIVE_TO_INT(piSenderPlayer)
			ELIF iSenderTeam = 3
				sVVFlippedSoundSet3 = sSoundSet
				sVVFlippedSceneSoundSet = sSceneSoundSet
				iVVFlippedPartSender3 = NATIVE_TO_INT(piSenderPlayer)
			ENDIF
						
			IF iSenderTeam = 0
				IF iVVFlippedSoundID = -1
					iVVFlippedSoundID = GET_SOUND_ID()
					PLAY_SOUND_FRONTEND(iVVFlippedSoundID, "Loop", sVVFlippedSoundSet, FALSE)
					START_NET_TIMER(tdVehAudFlipped0)
					PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND - Team 0 - Playing Loop:", sSoundSet)
				ENDIF
			ELIF iSenderTeam = 1
				IF iVVFlippedSoundID1 = -1
					iVVFlippedSoundID1 = GET_SOUND_ID()
					PLAY_SOUND_FRONTEND(iVVFlippedSoundID1, "Loop", sVVFlippedSoundSet1, FALSE)
					START_NET_TIMER(tdVehAudFlipped1)
					PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND -Team 1 - Playing Loop:", sSoundSet)
				ENDIF
			ELIF iSenderTeam = 2
				IF iVVFlippedSoundID2 = -1
					iVVFlippedSoundID2 = GET_SOUND_ID()
					PLAY_SOUND_FRONTEND(iVVFlippedSoundID2, "Loop", sVVFlippedSoundSet2, FALSE)
					START_NET_TIMER(tdVehAudFlipped2)
					PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND -Team 2 - Playing Loop:", sSoundSet)
				ENDIF
			ELIF iSenderTeam = 3
				IF iVVFlippedSoundID3 = -1
					iVVFlippedSoundID3 = GET_SOUND_ID()
					PLAY_SOUND_FRONTEND(iVVFlippedSoundID3, "Loop", sVVFlippedSoundSet3, FALSE)
					START_NET_TIMER(tdVehAudFlipped3)
					PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND -Team 3 - Playing Loop:", sSoundSet)
				ENDIF			
			ENDIF
		BREAK
		
		//done
		CASE ciVEH_WEP_ZONED	
			IF iSenderTeam = MC_playerBD[iPartToUse].iteam
				IF piSenderPlayer = LocalPlayer
					sSoundSet = "DLC_IE_VV_Zoned_Player_Sounds"
					sSceneSoundSet = "DLC_IE_VV_Zoned_Scene"
				ELSE
					sSoundSet = "DLC_IE_VV_Zoned_Team_Sounds"
					sSceneSoundSet = "DLC_IE_VV_Zoned_Scene"
				ENDIF
			ELSE
				sSoundSet = "DLC_IE_VV_Zoned_Enemy_Sounds"
				sSceneSoundSet = "DLC_IE_VV_Zoned_Scene"
			ENDIF
			
			sVVZonedSoundSet = sSoundSet
			sVVZonedSceneSoundSet = sSceneSoundSet
			
			IF iVVZonedSoundID = -1
				iVVZonedSoundID = GET_SOUND_ID()
				PLAY_SOUND_FRONTEND(iVVZonedSoundID, "Loop", sVVZonedSoundSet, FALSE)
				START_NET_TIMER(tdVehAudZoned)
				PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND Zoned - Playing Loop:", sSoundSet)
			ENDIF
			
		BREAK
		
		//done
		CASE ciVEH_WEP_DETONATE	
			IF iSenderTeam = MC_playerBD[iPartToUse].iteam
				IF piSenderPlayer = LocalPlayer
					sSoundSet = "DLC_IE_VV_Detonator_Player_Sounds"
					sSceneSoundSet = ""
				ELSE
					sSoundSet = "DLC_IE_VV_Detonator_Team_Sounds"
					sSceneSoundSet = ""
				ENDIF
			ELSE
				sSoundSet = "DLC_IE_VV_Detonator_Enemy_Sounds"
				sSceneSoundSet = ""
			ENDIF
			
			//SET_BIT(bsPlayersWithDetonateActive, iPartToUse)
			
			sVVDetonateSoundSet = sSoundSet
			sVVDetonateSceneSoundSet = sSceneSoundSet
			
			IF iVVDetonateSoundID = -1
				iVVDetonateSoundID = GET_SOUND_ID()
				//PLAY_SOUND_FRONTEND(iVVDetonateSoundID, "Loop", sVVDetonateSoundSet, FALSE)
				PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND - Playing Loop:", sSoundSet)
			ENDIF
		BREAK
		
		//done
		CASE ciVEH_WEP_BOMB
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
				IF iSenderTeam = MC_playerBD[iPartToUse].iteam
					IF piSenderPlayer = LocalPlayer
						sSoundSet = "DLC_SR_TR_Bomb_Player_Sounds"
						sSceneSoundSet = ""
					ELSE
						sSoundSet = "DLC_SR_TR_Bomb_Player_Sounds"
						sSceneSoundSet = ""
					ENDIF
				ELSE
					sSoundSet = "DLC_SR_TR_Bomb_Enemy_Sounds"
					sSceneSoundSet = ""
				ENDIF
			ELSE
				IF iSenderTeam = MC_playerBD[iPartToUse].iteam
					IF piSenderPlayer = LocalPlayer
						sSoundSet = "DLC_IE_VV_Bomb_Player_Sounds"
						sSceneSoundSet = ""
					ELSE
						sSoundSet = "DLC_IE_VV_Bomb_Team_Sounds"
						sSceneSoundSet = ""
					ENDIF
				ELSE
					sSoundSet = "DLC_IE_VV_Bomb_Enemy_Sounds"
					sSceneSoundSet = ""
				ENDIF
			ENDIF
			
			piTemp  = NETWORK_GET_PARTICIPANT_INDEX (piSenderPlayer)

			SET_BIT(bsPlayersWithBombActive, NATIVE_TO_INT(piTemp))
			
			sVVBombSoundSet = sSoundSet
			sVVBombSceneSoundSet = sSceneSoundSet
			
			IF iVVBombSoundID  = -1
				iVVBombSoundID  = GET_SOUND_ID()
				PLAY_SOUND_FRONTEND(iVVBombSoundID, "Loop", sVVBombSoundSet, FALSE)
				PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND - Playing Loop:", sSoundSet)
			ENDIF
		BREAK

		
		//done
		CASE ciVEH_WEP_PRON			
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
				IF iSenderTeam = MC_playerBD[iPartToUse].iteam
					IF piSenderPlayer = LocalPlayer
						sSoundSet = "DLC_SR_TR_Deadline_Player_Sounds"
						sSceneSoundSet = ""
					ELSE
						sSoundSet = "DLC_SR_TR_Deadline_Player_Sounds"
						sSceneSoundSet = ""
					ENDIF
				ELSE
					sSoundSet = "DLC_SR_TR_Deadline_Enemy_Sounds"
					sSceneSoundSet = ""
				ENDIF
			ELSE			
				IF iSenderTeam = MC_playerBD[iPartToUse].iteam
					IF piSenderPlayer = LocalPlayer
						sSoundSet = "DLC_IE_VV_Deadline_Player_Sounds"
						sSceneSoundSet = "DLC_IE_VV_Deadline_Scene"
					ELSE
						sSoundSet = "DLC_IE_VV_Deadline_Team_Sounds"
						sSceneSoundSet = ""
					ENDIF
				ELSE
					sSoundSet = "DLC_IE_VV_Deadline_Enemy_Sounds"
					sSceneSoundSet = ""
				ENDIF
			ENDIF
			
			IF iSenderTeam = 0
				sVVPronSoundSet = sSoundSet
				sVVPronSceneSoundSet = sSceneSoundSet
				iVVPronPartSender = NATIVE_TO_INT(piSenderPlayer)
			ELIF iSenderTeam = 1
				sVVPronSoundSet1 = sSoundSet
				sVVPronSceneSoundSet = sSceneSoundSet
				iVVPronPartSender1 = NATIVE_TO_INT(piSenderPlayer)
			ELIF iSenderTeam = 2
				sVVPronSoundSet2 = sSoundSet
				sVVPronSceneSoundSet = sSceneSoundSet
				iVVPronPartSender2 = NATIVE_TO_INT(piSenderPlayer)
			ELIF iSenderTeam = 3
				sVVPronSoundSet3 = sSoundSet
				sVVPronSceneSoundSet = sSceneSoundSet
				iVVPronPartSender3 = NATIVE_TO_INT(piSenderPlayer)
			ENDIF
			
			IF iSenderTeam = 0
				IF iVVPronSoundID  = -1
					iVVPronSoundID = GET_SOUND_ID()
					PLAY_SOUND_FRONTEND(iVVPronSoundID, "Loop", sVVPronSoundSet, FALSE)
					START_NET_TIMER(tdVehAudPron0)
					PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND - Playing Loop:", sSoundSet)
				ENDIF
			ELIF iSenderTeam = 1
				IF iVVPronSoundID1  = -1
					iVVPronSoundID1 = GET_SOUND_ID()
					PLAY_SOUND_FRONTEND(iVVPronSoundID1, "Loop", sVVPronSoundSet1, FALSE)
					START_NET_TIMER(tdVehAudPron1)
					PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND - Playing Loop:", sSoundSet)
				ENDIF
			ELIF iSenderTeam = 2
				IF iVVPronSoundID2  = -1
					iVVPronSoundID2 = GET_SOUND_ID()
					PLAY_SOUND_FRONTEND(iVVPronSoundID2, "Loop", sVVPronSoundSet2, FALSE)
					START_NET_TIMER(tdVehAudPron2)
					PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND - Playing Loop:", sSoundSet)
				ENDIF
			ELIF iSenderTeam = 3
				IF iVVPronSoundID3  = -1
					iVVPronSoundID3 = GET_SOUND_ID()
					PLAY_SOUND_FRONTEND(iVVPronSoundID3, "Loop", sVVPronSoundSet3, FALSE)
					START_NET_TIMER(tdVehAudPron3)
					PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND - Playing Loop:", sSoundSet)
				ENDIF
			ENDIF
		BREAK
		
		//done sound not added
		CASE ciVEH_WEP_REPAIR
			IF iSenderTeam = MC_playerBD[iPartToUse].iteam
				IF piSenderPlayer = LocalPlayer
					sSoundSet = "DLC_IE_VV_Repair_Player_Sounds"
					sSceneSoundSet = ""
				ELSE
					sSoundSet = "DLC_IE_VV_Repair_Team_Sounds"
					sSceneSoundSet = ""
				ENDIF
			ELSE
				sSoundSet = "DLC_IE_VV_Repair_Enemy_Sounds"
				sSceneSoundSet = ""
			ENDIF
			
			sVVRepairSoundSet = sSoundSet
			sVVRepairSceneSoundSet = sSceneSoundSet
			
			IF iVVRepairSoundID = -1
				//iVVRepairSoundID  = GET_SOUND_ID()
				//PLAY_SOUND_FRONTEND(iVVRepairSoundID , "Loop", sSoundSet, FALSE)
				PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND - Playing Loop:", sSoundSet)
			ENDIF
		BREAK
		
		//done - no longer used
		CASE ciVEH_WEP_SPEED_BOOST
			IF iSenderTeam = MC_playerBD[iPartToUse].iteam
				IF piSenderPlayer = LocalPlayer
					sSoundSet = "DLC_IE_VV_Boost_Player_Sounds"
					sSceneSoundSet = "DLC_IE_VV_Boost_Scene"
				ELSE
					sSoundSet = "DLC_IE_VV_Boost_Team_Sounds"
					sSceneSoundSet = ""
				ENDIF
			ELSE
				sSoundSet = "DLC_IE_VV_Boost_Enemy_Sounds"
				sSceneSoundSet = ""
			ENDIF
			
			sVVSpeedBoostSoundSet = sSoundSet
			sVVSpeedBoostSceneSoundSet = sSceneSoundSet
			
			IF iVVSpeedBoostID = -1
				iVVSpeedBoostID = GET_SOUND_ID()
				PLAY_SOUND_FRONTEND(iVVSpeedBoostID , "Loop", sSoundSet, FALSE)
				PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND - Playing Loop:", sSoundSet)
			ENDIF
		BREAK
		
		
		
		//done
		CASE ciVEH_WEP_RUINER_SPECIAL_VEH
			IF iSenderTeam = MC_playerBD[iPartToUse].iteam
				IF piSenderPlayer = LocalPlayer
					sSoundSet = "DLC_SR_TR_Ruiner2000_Player_Sounds"
					sSceneSoundSet = ""
				ELSE
					sSoundSet = "DLC_SR_TR_Ruiner2000_Player_Sounds"
					sSceneSoundSet = ""
				ENDIF
			ELSE
				sSoundSet = "DLC_SR_TR_Ruiner2000_Enemy_Sounds"
				sSceneSoundSet = ""
			ENDIF
			
			IF iSenderTeam = 0
				sVVRuinerSoundSet = sSoundSet
				iVVRuinerPartSender = NATIVE_TO_INT(piSenderPlayer)
				//sVVRuinerSceneSoundSet = sSceneSoundSet
			ELIF iSenderTeam = 1
				sVVRuinerSoundSet1 = sSoundSet
				iVVRuinerPartSender1 = NATIVE_TO_INT(piSenderPlayer)
				//sVVRuinerSceneSoundSet = sSceneSoundSet
			ELIF iSenderTeam = 2
				sVVRuinerSoundSet2 = sSoundSet
				iVVRuinerPartSender2 = NATIVE_TO_INT(piSenderPlayer)
				//sVVRuinerSceneSoundSet = sSceneSoundSet
			ELIF iSenderTeam = 3
				sVVRuinerSoundSet3 = sSoundSet
				iVVRuinerPartSender3 = NATIVE_TO_INT(piSenderPlayer)
				//sVVRuinerSceneSoundSet = sSceneSoundSet
			ENDIF
			
			IF iSenderTeam = 0
				//IF iVVRuinerSoundID  = -1
					//iVVRuinerSoundID = GET_SOUND_ID()
					//PLAY_SOUND_FRONTEND(iVVRuinerSoundID, "Loop", sVVRuinerSoundSet, FALSE)
					START_NET_TIMER(tdVehAudRuiner0)
					//PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND - Playing Loop:", sSoundSet)
				//ENDIF
			ELIF iSenderTeam = 1
				//IF iVVRuinerSoundID1  = -1
					//iVVRuinerSoundID1 = GET_SOUND_ID()
					//PLAY_SOUND_FRONTEND(iVVRuinerSoundID1, "Loop", sVVRuinerSoundSet1, FALSE)
					START_NET_TIMER(tdVehAudRuiner1)
					//PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND - Playing Loop:", sSoundSet)
				//ENDIF
			ELIF iSenderTeam = 2
				//IF iVVRuinerSoundID2  = -1
					//iVVRuinerSoundID2 = GET_SOUND_ID()
					//PLAY_SOUND_FRONTEND(iVVRuinerSoundID2, "Loop", sVVRuinerSoundSet2, FALSE)
					START_NET_TIMER(tdVehAudRuiner2)
					//PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND - Playing Loop:", sSoundSet)
				//ENDIF
			ELIF iSenderTeam = 3
				//IF iVVRuinerSoundID3  = -1
					//iVVRuinerSoundID3 = GET_SOUND_ID()
					//PLAY_SOUND_FRONTEND(iVVRuinerSoundID3, "Loop", sVVRuinerSoundSet3, FALSE)
					START_NET_TIMER(tdVehAudRuiner3)
					//PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND - Playing Loop:", sSoundSet)
				//ENDIF
			ENDIF
		BREAK
		
		//done
		CASE ciVEH_WEP_RAMP_SPECIAL_VEH
			IF iSenderTeam = MC_playerBD[iPartToUse].iteam
				IF piSenderPlayer = LocalPlayer
					sSoundSet = "DLC_SR_TR_Ramp_Buggy_Player_Sounds"
					sSceneSoundSet = ""
				ELSE
					sSoundSet = "DLC_SR_TR_Ramp_Buggy_Player_Sounds"
					sSceneSoundSet = ""
				ENDIF
			ELSE
				sSoundSet = "DLC_SR_TR_Ramp_Buggy_Enemy_Sounds"
				sSceneSoundSet = ""
			ENDIF
			
			IF iSenderTeam = 0
				sVVRampSoundSet = sSoundSet
				iVVRampPartSender = NATIVE_TO_INT(piSenderPlayer)
				//sVVRampSceneSoundSet = sSceneSoundSet
			ELIF iSenderTeam = 1
				sVVRampSoundSet1 = sSoundSet
				iVVRampPartSender1 = NATIVE_TO_INT(piSenderPlayer)
				//sVVRampSceneSoundSet = sSceneSoundSet
			ELIF iSenderTeam = 2
				sVVRampSoundSet2 = sSoundSet
				iVVRampPartSender2 = NATIVE_TO_INT(piSenderPlayer)
				//sVVRampSceneSoundSet = sSceneSoundSet
			ELIF iSenderTeam = 3
				sVVRampSoundSet3 = sSoundSet
				iVVRampPartSender3 = NATIVE_TO_INT(piSenderPlayer)
				//sVVRampSceneSoundSet = sSceneSoundSet
			ENDIF
						
			IF iSenderTeam = 0
				//IF iVVRampSoundID  = -1
					//iVVRampSoundID = GET_SOUND_ID()
					//PLAY_SOUND_FRONTEND(iVVRampSoundID, "Loop", sVVRampSoundSet, FALSE)
					START_NET_TIMER(tdVehAudRamp0)
					//PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND - Playing Loop:", sSoundSet)
				//ENDIF
			ELIF iSenderTeam = 1
				//IF iVVRampSoundID1  = -1
					//iVVRampSoundID1 = GET_SOUND_ID()
					//PLAY_SOUND_FRONTEND(iVVRampSoundID1, "Loop", sVVRampSoundSet1, FALSE)
					START_NET_TIMER(tdVehAudRamp1)
					//PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND - Playing Loop:", sSoundSet)
				//ENDIF
			ELIF iSenderTeam = 2
				//IF iVVRampSoundID2  = -1
					//iVVRampSoundID2 = GET_SOUND_ID()
					//PLAY_SOUND_FRONTEND(iVVRampSoundID2, "Loop", sVVRampSoundSet2, FALSE)
					START_NET_TIMER(tdVehAudRamp2)
					//PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND - Playing Loop:", sSoundSet)
				//ENDIF
			ELIF iSenderTeam = 3
				//IF iVVRampSoundID3  = -1
					//iVVRampSoundID3 = GET_SOUND_ID()
					//PLAY_SOUND_FRONTEND(iVVRampSoundID3, "Loop", sVVRampSoundSet3, FALSE)
					START_NET_TIMER(tdVehAudRamp3)
					//PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND - Playing Loop:", sSoundSet)
				//ENDIF
			ENDIF
		BREAK
		CASE ciVEH_WEP_BOMB_LENGTH
			IF iSenderTeam = MC_playerBD[iPartToUse].iteam
				IF piSenderPlayer = LocalPlayer
					sSoundSet = ""
				ELSE
					sSoundSet = ""
				ENDIF
			ELSE
				sSoundSet = ""
			ENDIF
			
			IF iDTBBombLength = -1
				iDTBBombLength = GET_SOUND_ID()
				PLAY_SOUND_FRONTEND(iDTBBombLength , "", sSoundSet, FALSE)
				PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND - Playing Loop:", sSoundSet)
			ENDIF
		BREAK
		CASE ciVEH_WEP_BOMB_MAX
			IF iSenderTeam = MC_playerBD[iPartToUse].iteam
				IF piSenderPlayer = LocalPlayer
					sSoundSet = ""
				ELSE
					sSoundSet = ""
				ENDIF
			ELSE
				sSoundSet = ""
			ENDIF
			
			IF iDTBBombMax = -1
				iDTBBombMax = GET_SOUND_ID()
				PLAY_SOUND_FRONTEND(iDTBBombMax , "", sSoundSet, FALSE)
				PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND - Playing Loop:", sSoundSet)
			ENDIF
		BREAK
		CASE ciVEH_WEP_EXTRA_LIFE
			IF iSenderTeam = MC_playerBD[iPartToUse].iteam
				IF piSenderPlayer = LocalPlayer
					sSoundSet = ""
				ELSE
					sSoundSet = ""
				ENDIF
			ELSE
				sSoundSet = ""
			ENDIF
			
			IF iDTBExtraLife = -1
				iDTBExtraLife = GET_SOUND_ID()
				PLAY_SOUND_FRONTEND(iDTBExtraLife , "", sSoundSet, FALSE)
				PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND - Playing Loop:", sSoundSet)
			ENDIF
		BREAK
	ENDSWITCH	
	
		#IF IS_DEBUG_BUILD
		PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND debug 1 - ",  
				iVVRocketsSoundID , ", ",
				iVVGhostSoundID, ", ",
				iVVBeastmodeSoundID, ", ",
				iVVForcedAccSoundID, ", ",
				iVVFlippedSoundID, ", ",
				iVVZonedSoundID)
		PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND debug 2 - ",  
				iVVDetonateSoundID, ", ",
				iVVBombSoundID , ", ",
				iVVPronSoundID, ", ",
				iVVRepairSoundID)
		#ENDIF

	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType) AND (iPowerupType = ciVEH_WEP_FLIPPED_CONTROLS OR iPowerupType = ciVeh_WEP_PRON OR iPowerupType = ciVEH_WEP_RUINER_SPECIAL_VEH OR iPowerupType = ciVEH_WEP_RAMP_SPECIAL_VEH)
	OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
		IF NOT IS_STRING_NULL_OR_EMPTY(sSoundSet)
			PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND - Playing Start:",sSoundSet)
			IF piSenderPlayer = LocalPlayer
			OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
				PLAY_SOUND_FRONTEND(-1, "Activate", sSoundSet, FALSE)
			ELSE
				PED_INDEX pedSender = GET_PLAYER_PED(piSenderPlayer)
				IF NOT IS_PED_INJURED(pedSender)
					PLAY_SOUND_FROM_ENTITY(-1, "Activate", pedSender, sSoundSet, FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	IF NOT IS_STRING_NULL_OR_EMPTY(sSceneSoundSet)
		PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND - sSceneSoundSet", sSceneSoundSet)
		IF NOT IS_AUDIO_SCENE_ACTIVE(sSceneSoundSet)
			PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND - starting audio scene : ", sSceneSoundSet)
			START_AUDIO_SCENE(sSceneSoundSet)
		ENDIF
	ENDIF
ENDPROC


PROC CLEANUP_VV_SOUNDS()

	//######### GHOST LOOPING SOUND
	
	//need IDs for team 0 and 1
	
	//Team 0
	IF MC_PlayerBD[iPartToUse].iteam = 0
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVGhostSoundSet)
			IF ARE_STRINGS_EQUAL(sVVGhostSoundSet,"DLC_IE_VV_Ghost_Player_Sounds")
				IF iVVGhostSoundID != -1	
					PRINTLN("[RCC MISSION] CLEANUP_VV_SOUNDS - iVVGhostSoundID ! = -1")
					IF HAS_NET_TIMER_STARTED(tdVehAudGhost0)
						PRINTLN("[RCC MISSION] CLEANUP_VV_SOUNDS - tdVehGhost0 - timer started")
						PRINTLN("[RCC MISSION] CLEANUP_VV_SOUNDS - Ending Ghost 0.")
						STOP_SOUND(iVVGhostSoundID)
						STOP_AUDIO_SCENE(sVVGhostSceneSoundSet)
						SCRIPT_RELEASE_SOUND_ID(iVVGhostSoundID)
									
						IF NOT IS_STRING_NULL_OR_EMPTY(sVVGhostSoundSet)
							PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVGhostSoundSet, FALSE)
						ENDIF
						
						//unref
						IF NOT IS_STRING_NULL_OR_EMPTY(sVVGhostSoundSet)
						
						ENDIF
						
						RESET_NET_TIMER(tdVehAudGhost0)
						iVVGhostSoundID = -1
					ELSE
						//triggered effects while power up is active
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Team 1
	IF MC_PlayerBD[iPartToUse].iteam = 1
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVGhostSoundSet1)
			IF ARE_STRINGS_EQUAL(sVVGhostSoundSet1,"DLC_IE_VV_Ghost_Player_Sounds")
				IF iVVGhostSoundID1 != -1
					PRINTLN("[RCC MISSION] CLEANUP_VV_SOUNDS - iVVGhostSoundID1 ! = -1")
					IF HAS_NET_TIMER_STARTED(tdVehAudGhost1)
						PRINTLN("[RCC MISSION] CLEANUP_VV_SOUNDS - tdVehGhost1 - timer started")
						PRINTLN("[RCC MISSION] CLEANUP_VV_SOUNDS - Ending Ghost 1.")
						STOP_SOUND(iVVGhostSoundID1)
						STOP_AUDIO_SCENE(sVVGhostSceneSoundSet)
						SCRIPT_RELEASE_SOUND_ID(iVVGhostSoundID1)
									
						IF NOT IS_STRING_NULL_OR_EMPTY(sVVGhostSoundSet1)
							PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVGhostSoundSet1, FALSE)
						ENDIF
						
						//unref
						IF NOT IS_STRING_NULL_OR_EMPTY(sVVGhostSoundSet1)
						
						ENDIF
						
						RESET_NET_TIMER(tdVehAudGhost1)
						iVVGhostSoundID1 = -1
					ELSE
						//triggered effects while power up is active
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//######### BEAST LOOPING SOUND
	//team 0
	IF MC_PlayerBD[iPartToUse].iteam = 0
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVBeastmodeSoundSet)
			IF ARE_STRINGS_EQUAL(sVVBeastmodeSoundSet,"DLC_IE_VV_Beast_Player_Sounds")
				IF iVVBeastmodeSoundID != -1
					PRINTLN("[RCC MISSION] CLEANUP_VV_SOUNDS - iVVBeastmodeSoundID ! = -1")
					IF HAS_NET_TIMER_STARTED(tdVehAudBeast0)
						PRINTLN("[RCC MISSION] CLEANUP_VV_SOUNDS - tdVehBeast0 - timer started")
						PRINTLN("[RCC MISSION]CLEANUP_VV_SOUNDS - Ending Beast 0.")
						STOP_SOUND(iVVBeastmodeSoundID)
						STOP_AUDIO_SCENE(sVVBeastmodeSceneSoundSet)
						SCRIPT_RELEASE_SOUND_ID(iVVBeastmodeSoundID)
									
						IF NOT IS_STRING_NULL_OR_EMPTY(sVVBeastmodeSoundSet)
							PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVBeastmodeSoundSet, FALSE)
						ENDIF
						
						RESET_NET_TIMER(tdVehAudBeast0)
						iVVBeastmodeSoundID = -1
					ELSE
						//triggered effects while power up is active
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Team 1
	IF MC_PlayerBD[iPartToUse].iteam = 1
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVBeastmodeSoundSet1)
			IF ARE_STRINGS_EQUAL(sVVBeastmodeSoundSet1,"DLC_IE_VV_Beast_Player_Sounds")
				IF iVVBeastmodeSoundID1 != -1
					PRINTLN("[RCC MISSION] CLEANUP_VV_SOUNDS - iVVBeastmodeSoundID1 ! = -1")
					IF HAS_NET_TIMER_STARTED(tdVehAudBeast1)
						PRINTLN("[RCC MISSION] CLEANUP_VV_SOUNDS - tdVehBeast1 - timer started")

						PRINTLN("[RCC MISSION] CLEANUP_VV_SOUNDS - Ending Beast 1.")
						STOP_SOUND(iVVBeastmodeSoundID1)
						STOP_AUDIO_SCENE(sVVBeastmodeSceneSoundSet)
						SCRIPT_RELEASE_SOUND_ID(iVVBeastmodeSoundID1)
									
						IF NOT IS_STRING_NULL_OR_EMPTY(sVVBeastmodeSoundSet1)
							PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVBeastmodeSoundSet1, FALSE)
						ENDIF
						
						RESET_NET_TIMER(tdVehAudBeast1)
						iVVBeastmodeSoundID1 = -1
					ELSE
						//triggered effects while power up is active
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//######### FORCED ACCEL LOOPING SOUND
	//team 0
	IF MC_PlayerBD[iPartToUse].iteam = 0
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVForcedAccSoundSet)
			IF ARE_STRINGS_EQUAL(sVVForcedAccSoundSet,"DLC_IE_VV_Jammed_Player_Sounds")
				IF iVVForcedAccSoundID != -1
					PRINTLN("[RCC MISSION] CLEANUP_VV_SOUNDS - iVVForcedAccSoundID ! = -1")
					IF HAS_NET_TIMER_STARTED(tdVehAudAccelerate0)
						PRINTLN("[RCC MISSION]  CLEANUP_VV_SOUNDSS - tdVehAccelerate0 - timer started")
						PRINTLN("[RCC MISSION]  CLEANUP_VV_SOUNDS - Ending Forced Accel 0.")
						STOP_SOUND(iVVForcedAccSoundID)
						STOP_AUDIO_SCENE(sVVForcedAccSceneSoundSet)
						SCRIPT_RELEASE_SOUND_ID(iVVForcedAccSoundID)
									
						IF NOT IS_STRING_NULL_OR_EMPTY(sVVForcedAccSoundSet)
							PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVForcedAccSoundSet, FALSE)
						ENDIF
						
						RESET_NET_TIMER(tdVehAudAccelerate0)
						iVVForcedAccSoundID = -1
					ELSE
						//triggered effects while power up is active
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Team 1
	IF MC_PlayerBD[iPartToUse].iteam = 1
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVForcedAccSoundSet1)
			IF ARE_STRINGS_EQUAL(sVVForcedAccSoundSet1,"DLC_IE_VV_Jammed_Player_Sounds")
				IF iVVForcedAccSoundID1 != -1
					PRINTLN("[RCC MISSION]  CLEANUP_VV_SOUNDS - iVVForcedAccSoundID1 ! = -1")
					IF HAS_NET_TIMER_STARTED(tdVehAudAccelerate1)
						PRINTLN("[RCC MISSION]  CLEANUP_VV_SOUNDS - tdVehAccelerate1 - timer started")
						PRINTLN("[RCC MISSION]  CLEANUP_VV_SOUNDS - Ending Forced Accel 1.")
						STOP_SOUND(iVVForcedAccSoundID1)
						STOP_AUDIO_SCENE(sVVForcedAccSceneSoundSet)
						SCRIPT_RELEASE_SOUND_ID(iVVForcedAccSoundID1)
									
						IF NOT IS_STRING_NULL_OR_EMPTY(sVVForcedAccSoundSet1)
							PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVForcedAccSoundSet1, FALSE)
						ENDIF
						
						RESET_NET_TIMER(tdVehAudAccelerate1)
						iVVForcedAccSoundID1 = -1

					ELSE
						//triggered effects while power up is active
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//######### FLIPPED LOOPING SOUND
	//Team 0
	IF MC_PlayerBD[iPartToUse].iteam = 0
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVFlippedSoundSet)
			IF iVVFlippedSoundID != -1
				PRINTLN("[RCC MISSION]  CLEANUP_VV_SOUNDS - iVVFlippedSoundID ! = -1")
				IF HAS_NET_TIMER_STARTED(tdVehAudFlipped0)
					PRINTLN("[RCC MISSION] CLEANUP_VV_SOUNDS - tdVehFlipped0 - timer started")
					PRINTLN("[RCC MISSION] CLEANUP_VV_SOUNDS - Ending Flipped 0.")
					STOP_SOUND(iVVFlippedSoundID)
					STOP_AUDIO_SCENE(sVVFlippedSceneSoundSet)
					SCRIPT_RELEASE_SOUND_ID(iVVFlippedSoundID)
					
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVFlippedSoundSet)
						
						PLAYER_INDEX piSenderPlayer = INT_TO_PLAYERINDEX(iVVFlippedPartSender)
						
						IF piSenderPlayer = LocalPlayer
						OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
							PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVFlippedSoundSet, FALSE)
						ELSE
							PED_INDEX pedSender = GET_PLAYER_PED(piSenderPlayer)
							IF NOT IS_PED_INJURED(pedSender)
								PLAY_SOUND_FROM_ENTITY(-1, "Deactivate", pedSender, sVVFlippedSoundSet, FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					RESET_NET_TIMER(tdVehAudFlipped0)
					iVVFlippedSoundID= -1
				ELSE
					//triggered effects while power up is active
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Team 1
	IF MC_PlayerBD[iPartToUse].iteam = 1
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVFlippedSoundSet1)
			IF iVVFlippedSoundID1 != -1
				PRINTLN("[RCC MISSION]  CLEANUP_VV_SOUNDS - iVVFlippedSoundID1 ! = -1")
				IF HAS_NET_TIMER_STARTED(tdVehAudFlipped1)
					PRINTLN("[RCC MISSION]  CLEANUP_VV_SOUNDS - tdVehFlipped1 - timer started")
					PRINTLN("[RCC MISSION]  CLEANUP_VV_SOUNDS - Ending Flipped 1.")
					STOP_SOUND(iVVFlippedSoundID1)
					STOP_AUDIO_SCENE(sVVFlippedSceneSoundSet)
					SCRIPT_RELEASE_SOUND_ID(iVVFlippedSoundID1)
					
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVFlippedSoundSet1)
					
						PLAYER_INDEX piSenderPlayer = INT_TO_PLAYERINDEX(iVVFlippedPartSender1)
					
						IF piSenderPlayer = LocalPlayer
						OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
							PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVFlippedSoundSet1, FALSE)
						ELSE
							PED_INDEX pedSender = GET_PLAYER_PED(piSenderPlayer)
							IF NOT IS_PED_INJURED(pedSender)
								PLAY_SOUND_FROM_ENTITY(-1, "Deactivate", pedSender, sVVFlippedSoundSet1, FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					RESET_NET_TIMER(tdVehAudFlipped1)
					iVVFlippedSoundID1 = -1
				ELSE
					//triggered effects while power up is active
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	
	//Team 2
	IF MC_PlayerBD[iPartToUse].iteam = 2
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVFlippedSoundSet2)
			IF iVVFlippedSoundID2 != -2
				PRINTLN("[RCC MISSION]  CLEANUP_VV_SOUNDS - iVVFlippedSoundID2 ! = -1")
				IF HAS_NET_TIMER_STARTED(tdVehAudFlipped2)
					PRINTLN("[RCC MISSION]  CLEANUP_VV_SOUNDS - tdVehFlipped2 - timer started")
					PRINTLN("[RCC MISSION]  CLEANUP_VV_SOUNDS - Ending Flipped 2.")
					STOP_SOUND(iVVFlippedSoundID2)
					STOP_AUDIO_SCENE(sVVFlippedSceneSoundSet)
					SCRIPT_RELEASE_SOUND_ID(iVVFlippedSoundID2)
					
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVFlippedSoundSet2)
						
						PLAYER_INDEX piSenderPlayer = INT_TO_PLAYERINDEX(iVVFlippedPartSender2)
						
						IF piSenderPlayer = LocalPlayer
						OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
							PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVFlippedSoundSet2, FALSE)
						ELSE
							PED_INDEX pedSender = GET_PLAYER_PED(piSenderPlayer)
							IF NOT IS_PED_INJURED(pedSender)
								PLAY_SOUND_FROM_ENTITY(-1, "Deactivate", pedSender, sVVFlippedSoundSet2, FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					RESET_NET_TIMER(tdVehAudFlipped2)
					iVVFlippedSoundID2 = -1
				ELSE
					//triggered effects while power up is active
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Team 3
	IF MC_PlayerBD[iPartToUse].iteam = 3
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVFlippedSoundSet3)
			IF iVVFlippedSoundID3 != -1
				PRINTLN("[RCC MISSION]  CLEANUP_VV_SOUNDS - iVVFlippedSoundID3 ! = -1")
				IF HAS_NET_TIMER_STARTED(tdVehAudFlipped3)
					PRINTLN("[RCC MISSION]  CLEANUP_VV_SOUNDS - tdVehFlipped3 - timer started")
					PRINTLN("[RCC MISSION]  CLEANUP_VV_SOUNDS - Ending Flipped 3.")
					STOP_SOUND(iVVFlippedSoundID3)
					STOP_AUDIO_SCENE(sVVFlippedSceneSoundSet)
					SCRIPT_RELEASE_SOUND_ID(iVVFlippedSoundID3)
					
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVFlippedSoundSet3)
					
						PLAYER_INDEX piSenderPlayer = INT_TO_PLAYERINDEX(iVVFlippedPartSender3)
						
						IF piSenderPlayer = LocalPlayer
						OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
							PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVFlippedSoundSet3, FALSE)
						ELSE
							PED_INDEX pedSender = GET_PLAYER_PED(piSenderPlayer)
							IF NOT IS_PED_INJURED(pedSender)
								PLAY_SOUND_FROM_ENTITY(-1, "Deactivate", pedSender, sVVFlippedSoundSet3, FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					RESET_NET_TIMER(tdVehAudFlipped3)
					iVVFlippedSoundID3 = -1
				ELSE
					//triggered effects while power up is active
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	
	//######### ZONED LOOPING SOUND
	IF NOT IS_STRING_NULL_OR_EMPTY(sVVZonedSoundSet)
		IF iVVZonedSoundID != -1
			PRINTLN("[RCC MISSION]  CLEANUP_VV_SOUNDS - iVVZonedSoundID ! = -1")
			IF HAS_NET_TIMER_STARTED(tdVehAudZoned)
				PRINTLN("[RCC MISSION]  CLEANUP_VV_SOUNDS - tdVehZoned")
				PRINTLN("[RCC MISSION]  CLEANUP_VV_SOUNDS - Ending Zoned.")
				STOP_SOUND(iVVZonedSoundID)
				STOP_AUDIO_SCENE(sVVZonedSceneSoundSet)
				SCRIPT_RELEASE_SOUND_ID(iVVZonedSoundID)
							
				IF NOT IS_STRING_NULL_OR_EMPTY(sVVZonedSoundSet)
					PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVZonedSoundSet, FALSE)
				ENDIF
				
				RESET_NET_TIMER(tdVehAudZoned)
				iVVZonedSoundID = -1
			ELSE
				//triggered effects while power up is active
			ENDIF
		ENDIF
	ENDIF

	//######### PRON LOOPING SOUND
	//Team 0
	
	IF MC_PlayerBD[iPartToUse].iteam = 0
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVPronSoundSet)
			IF iVVPronSoundID != -1
				PRINTLN("[RCC MISSION]  CLEANUP_VV_SOUNDS - iVVPronSoundID! = -1")
				IF HAS_NET_TIMER_STARTED(tdVehAudPron0)
					PRINTLN("[RCC MISSION]  CLEANUP_VV_SOUNDS - tdVehPron0")

					PRINTLN("[RCC MISSION]  CLEANUP_VV_SOUNDS - ending Pron 0.")
					STOP_SOUND(iVVPronSoundID)
					STOP_AUDIO_SCENE(sVVPronSceneSoundSet)
					SCRIPT_RELEASE_SOUND_ID(iVVPronSoundID)
								
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVPronSoundSet)
						
						PLAYER_INDEX piSenderPlayer = INT_TO_PLAYERINDEX(iVVPronPartSender)
						
						IF piSenderPlayer = LocalPlayer
						OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
							PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVPronSoundSet, FALSE)
						ELSE
							PED_INDEX pedSender = GET_PLAYER_PED(piSenderPlayer)
							IF NOT IS_PED_INJURED(pedSender)
								PLAY_SOUND_FROM_ENTITY(-1, "Deactivate", pedSender, sVVPronSoundSet, FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					RESET_NET_TIMER(tdVehAudPron0)
					iVVPronSoundID = -1
				ELSE
					//triggered effects while power up is active
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Team 1
	IF MC_PlayerBD[iPartToUse].iteam = 1
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVPronSoundSet1)	
			IF iVVPronSoundID1 != -1
				PRINTLN("[RCC MISSION]  CLEANUP_VV_SOUNDS - iVVPronSoundID1 ! = -1")
				IF HAS_NET_TIMER_STARTED(tdVehAudPron1)
					PRINTLN("[RCC MISSION]  CLEANUP_VV_SOUNDS - tdVehPron1")
					PRINTLN("[RCC MISSION]  CLEANUP_VV_SOUNDS - ending Pron 1.")
					STOP_SOUND(iVVPronSoundID1)
					STOP_AUDIO_SCENE(sVVPronSceneSoundSet)
					SCRIPT_RELEASE_SOUND_ID(iVVPronSoundID1)
								
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVPronSoundSet1)
						
						PLAYER_INDEX piSenderPlayer = INT_TO_PLAYERINDEX(iVVPronPartSender1)
						
						IF piSenderPlayer = LocalPlayer
						OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
							PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVPronSoundSet1, FALSE)
						ELSE
							PED_INDEX pedSender = GET_PLAYER_PED(piSenderPlayer)
							IF NOT IS_PED_INJURED(pedSender)
								PLAY_SOUND_FROM_ENTITY(-1, "Deactivate", pedSender, sVVPronSoundSet1, FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					RESET_NET_TIMER(tdVehAudPron1)
					iVVPronSoundID1 = -1
				ELSE
					//triggered effects while power up is active
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Team 2
	IF MC_PlayerBD[iPartToUse].iteam = 2
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVPronSoundSet2)	
			IF iVVPronSoundID2 != -1
				PRINTLN("[RCC MISSION]  CLEANUP_VV_SOUNDS - iVVPronSoundID2 ! = -1")
				IF HAS_NET_TIMER_STARTED(tdVehAudPron2)
					PRINTLN("[RCC MISSION]  CLEANUP_VV_SOUNDS - tdVehPron2")
					PRINTLN("[RCC MISSION]  CLEANUP_VV_SOUNDS - ending Pron 2.")
					STOP_SOUND(iVVPronSoundID2)
					STOP_AUDIO_SCENE(sVVPronSceneSoundSet)
					SCRIPT_RELEASE_SOUND_ID(iVVPronSoundID2)
								
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVPronSoundSet2)
						
						PLAYER_INDEX piSenderPlayer = INT_TO_PLAYERINDEX(iVVPronPartSender2)
												
						IF piSenderPlayer = LocalPlayer
						OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
							PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVPronSoundSet2, FALSE)
						ELSE
							PED_INDEX pedSender = GET_PLAYER_PED(piSenderPlayer)
							IF NOT IS_PED_INJURED(pedSender)
								PLAY_SOUND_FROM_ENTITY(-1, "Deactivate", pedSender, sVVPronSoundSet2, FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					RESET_NET_TIMER(tdVehAudPron2)
					iVVPronSoundID2 = -1
				ELSE
					//triggered effects while power up is active
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Team 3
	IF MC_PlayerBD[iPartToUse].iteam = 3
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVPronSoundSet3)	
			IF iVVPronSoundID3 != -1
				PRINTLN("[RCC MISSION]  CLEANUP_VV_SOUNDS - iVVPronSoundID3 ! = -1")
				IF HAS_NET_TIMER_STARTED(tdVehAudPron3)
					PRINTLN("[RCC MISSION]  CLEANUP_VV_SOUNDS - tdVehPron3")
					PRINTLN("[RCC MISSION]  CLEANUP_VV_SOUNDS - ending Pron 3.")
					STOP_SOUND(iVVPronSoundID3)
					STOP_AUDIO_SCENE(sVVPronSceneSoundSet)
					SCRIPT_RELEASE_SOUND_ID(iVVPronSoundID3)
								
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVPronSoundSet3)
					
						PLAYER_INDEX piSenderPlayer = INT_TO_PLAYERINDEX(iVVPronPartSender3)
						
						IF piSenderPlayer = LocalPlayer
						OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
							PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVPronSoundSet3, FALSE)
						ELSE
							PED_INDEX pedSender = GET_PLAYER_PED(piSenderPlayer)
							IF NOT IS_PED_INJURED(pedSender)
								PLAY_SOUND_FROM_ENTITY(-1, "Deactivate", pedSender, sVVPronSoundSet3, FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					RESET_NET_TIMER(tdVehAudPron3)
					iVVPronSoundID3 = -1
				ELSE
					//triggered effects while power up is active
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	// ########### Special Vehicle Buggy #############
	//Team 0
	IF MC_PlayerBD[iPartToUse].iteam = 0
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVRampSoundSet)
			//IF iVVRampSoundID != -1
				//PRINTLN("[RCC MISSION]  CLEANUP_VV_SOUNDS - iVVRampSoundID ! = -1")
				IF HAS_NET_TIMER_STARTED(tdVehAudRamp0)
					PRINTLN("[RCC MISSION] CLEANUP_VV_SOUNDS - tdVehRamp0 - timer started")
					PRINTLN("[RCC MISSION] CLEANUP_VV_SOUNDS - Ending Ramp 0.")
					//STOP_SOUND(iVVRampSoundID)
					//STOP_AUDIO_SCENE(sVVRampSceneSoundSet)
					//SCRIPT_RELEASE_SOUND_ID(iVVRampSoundID)
								
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVRampSoundSet)
						
						PLAYER_INDEX piSenderPlayer = INT_TO_PLAYERINDEX(iVVRampPartSender)
						
						IF piSenderPlayer = LocalPlayer
						OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
							PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVRampSoundSet, FALSE)
						ELSE
							PED_INDEX pedSender = GET_PLAYER_PED(piSenderPlayer)
							IF NOT IS_PED_INJURED(pedSender)
								PLAY_SOUND_FROM_ENTITY(-1, "Deactivate", pedSender, sVVRampSoundSet, FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					RESET_NET_TIMER(tdVehAudRamp0)
					//iVVRampSoundID = -1
				ELSE
					//triggered effects while power up is active
				ENDIF
			//ENDIF
		ENDIF
	ENDIF
	
	//Team 1
	IF MC_PlayerBD[iPartToUse].iteam = 1
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVRampSoundSet1)
			//IF iVVRampSoundID1 != -1
				PRINTLN("[RCC MISSION]  CLEANUP_VV_SOUNDS - iVVRampSoundID1 ! = -1")
				IF HAS_NET_TIMER_STARTED(tdVehAudRamp1)
					PRINTLN("[RCC MISSION] CLEANUP_VV_SOUNDS - tdVehRamp1 - timer started")
					PRINTLN("[RCC MISSION] CLEANUP_VV_SOUNDS - Ending Ramp 1.")
					//STOP_SOUND(iVVRampSoundID1)
					//STOP_AUDIO_SCENE(sVVRampSceneSoundSet1)
					//SCRIPT_RELEASE_SOUND_ID(iVVRampSoundID1)
								
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVRampSoundSet1)
						
						PLAYER_INDEX piSenderPlayer = INT_TO_PLAYERINDEX(iVVRampPartSender1)
						
						IF piSenderPlayer = LocalPlayer
						OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
							PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVRampSoundSet1, FALSE)
						ELSE
							PED_INDEX pedSender = GET_PLAYER_PED(piSenderPlayer)
							IF NOT IS_PED_INJURED(pedSender)
								PLAY_SOUND_FROM_ENTITY(-1, "Deactivate", pedSender, sVVRampSoundSet1, FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					RESET_NET_TIMER(tdVehAudRamp1)
					//iVVRampSoundID1 = -1
				ELSE
					//triggered effects while power up is active
				ENDIF
			//ENDIF
		ENDIF
	ENDIF
	
	//Team 2
	IF MC_PlayerBD[iPartToUse].iteam = 2
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVRampSoundSet2)
			//IF iVVRampSoundID2 != -1
				PRINTLN("[RCC MISSION]  CLEANUP_VV_SOUNDS - iVVRampSoundID2 ! = -1")
				IF HAS_NET_TIMER_STARTED(tdVehAudRamp2)
					PRINTLN("[RCC MISSION] CLEANUP_VV_SOUNDS - tdVehRamp2 - timer started")
					PRINTLN("[RCC MISSION] CLEANUP_VV_SOUNDS - Ending Ramp 2.")
					//STOP_SOUND(iVVRampSoundID2)
					//STOP_AUDIO_SCENE(sVVRampSceneSoundSet2)
					//SCRIPT_RELEASE_SOUND_ID(iVVRampSoundID2)
								
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVRampSoundSet2)
						
						PLAYER_INDEX piSenderPlayer = INT_TO_PLAYERINDEX(iVVRampPartSender2)
						
						IF piSenderPlayer = LocalPlayer
						OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
							PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVRampSoundSet2, FALSE)
						ELSE
							PED_INDEX pedSender = GET_PLAYER_PED(piSenderPlayer)
							IF NOT IS_PED_INJURED(pedSender)
								PLAY_SOUND_FROM_ENTITY(-1, "Deactivate", pedSender, sVVRampSoundSet2, FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					RESET_NET_TIMER(tdVehAudRamp2)
					//iVVRampSoundID2 = -1
				ELSE
					//triggered effects while power up is active
				ENDIF
			//ENDIF
		ENDIF
	ENDIF
	
	//Team 3
	IF MC_PlayerBD[iPartToUse].iteam = 3
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVRampSoundSet3)
			//IF iVVRampSoundID3 != -1
				PRINTLN("[RCC MISSION]  CLEANUP_VV_SOUNDS - iVVRampSoundID3 ! = -1")
				IF HAS_NET_TIMER_STARTED(tdVehAudRamp3)
					PRINTLN("[RCC MISSION] CLEANUP_VV_SOUNDS - tdVehRamp3 - timer started")
					PRINTLN("[RCC MISSION] CLEANUP_VV_SOUNDS - Ending Ramp 3.")
					//STOP_SOUND(iVVRampSoundID3)
					//STOP_AUDIO_SCENE(sVVRampSceneSoundSet3)
					//SCRIPT_RELEASE_SOUND_ID(iVVRampSoundID3)
								
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVRampSoundSet3)
						
						PLAYER_INDEX piSenderPlayer = INT_TO_PLAYERINDEX(iVVRampPartSender3)
						
						IF piSenderPlayer = LocalPlayer
						OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
							PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVRampSoundSet3, FALSE)
						ELSE
							PED_INDEX pedSender = GET_PLAYER_PED(piSenderPlayer)
							IF NOT IS_PED_INJURED(pedSender)
								PLAY_SOUND_FROM_ENTITY(-1, "Deactivate", pedSender, sVVRampSoundSet3, FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					RESET_NET_TIMER(tdVehAudRamp3)
					//iVVRampSoundID3 = -1
				ELSE
					//triggered effects while power up is active
				ENDIF
			//ENDIF
		ENDIF
	ENDIF
	
	// ########### Special Vehicle RUINER #############
	//Team 0
	IF MC_PlayerBD[iPartToUse].iteam = 0
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVRuinerSoundSet)
			//IF iVVRuinerSoundID != -1
				PRINTLN("[RCC MISSION]  CLEANUP_VV_SOUNDS - iVVRuinerSoundID ! = -1")
				IF HAS_NET_TIMER_STARTED(tdVehAudRuiner0)
					PRINTLN("[RCC MISSION] CLEANUP_VV_SOUNDS - tdVehRuiner0 - timer started")
					PRINTLN("[RCC MISSION] CLEANUP_VV_SOUNDS - Ending Ruiner 0.")
					//STOP_SOUND(iVVRuinerSoundID)
					//STOP_AUDIO_SCENE(sVVRuinerSceneSoundSet)
					//SCRIPT_RELEASE_SOUND_ID(iVVRuinerSoundID)
								
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVRuinerSoundSet)
						
						PLAYER_INDEX piSenderPlayer = INT_TO_PLAYERINDEX(iVVRuinerPartSender)
					
						IF piSenderPlayer = LocalPlayer
						OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
							PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVRuinerSoundSet, FALSE)
						ELSE
							PED_INDEX pedSender = GET_PLAYER_PED(piSenderPlayer)
							IF NOT IS_PED_INJURED(pedSender)
								PLAY_SOUND_FROM_ENTITY(-1, "Deactivate", pedSender, sVVRuinerSoundSet, FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					RESET_NET_TIMER(tdVehAudRuiner0)
					//iVVRuinerSoundID = -1
				ELSE
					//triggered effects while power up is active
				ENDIF
			//ENDIF
		ENDIF
	ENDIF
	
	//Team 1
	IF MC_PlayerBD[iPartToUse].iteam = 1
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVRuinerSoundSet1)
			//IF iVVRuinerSoundID1 != -1
				PRINTLN("[RCC MISSION]  CLEANUP_VV_SOUNDS - iVVRuinerSoundID1 ! = -1")
				IF HAS_NET_TIMER_STARTED(tdVehAudRuiner1)
					PRINTLN("[RCC MISSION] CLEANUP_VV_SOUNDS - tdVehRuiner1 - timer started")
					PRINTLN("[RCC MISSION] CLEANUP_VV_SOUNDS - Ending Ruiner 1.")
					//STOP_SOUND(iVVRuinerSoundID1)
					//STOP_AUDIO_SCENE(sVVRuinerSceneSoundSet1)
					//SCRIPT_RELEASE_SOUND_ID(iVVRuinerSoundID1)
								
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVRuinerSoundSet1)
					
						PLAYER_INDEX piSenderPlayer = INT_TO_PLAYERINDEX(iVVRuinerPartSender1)
						
						IF piSenderPlayer = LocalPlayer
						OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
							PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVRuinerSoundSet1, FALSE)
						ELSE
							PED_INDEX pedSender = GET_PLAYER_PED(piSenderPlayer)
							IF NOT IS_PED_INJURED(pedSender)
								PLAY_SOUND_FROM_ENTITY(-1, "Deactivate", pedSender, sVVRuinerSoundSet1, FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					RESET_NET_TIMER(tdVehAudRuiner1)
					//iVVRuinerSoundID1 = -1
				ELSE
					//triggered effects while power up is active
				ENDIF
			//ENDIF
		ENDIF
	ENDIF
	
	//Team 2
	IF MC_PlayerBD[iPartToUse].iteam = 2
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVRuinerSoundSet2)
			//IF iVVRuinerSoundID2 != -1
				PRINTLN("[RCC MISSION]  CLEANUP_VV_SOUNDS - iVVRuinerSoundID2 ! = -1")
				IF HAS_NET_TIMER_STARTED(tdVehAudRuiner2)
					PRINTLN("[RCC MISSION] CLEANUP_VV_SOUNDS - tdVehRuiner2 - timer started")
					PRINTLN("[RCC MISSION] CLEANUP_VV_SOUNDS - Ending Ruiner 2.")
					//STOP_SOUND(iVVRuinerSoundID2)
					//STOP_AUDIO_SCENE(sVVRuinerSceneSoundSet2)
					//SCRIPT_RELEASE_SOUND_ID(iVVRuinerSoundID2)
								
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVRuinerSoundSet2)
						
						PLAYER_INDEX piSenderPlayer = INT_TO_PLAYERINDEX(iVVRuinerPartSender2)
						
						IF piSenderPlayer = LocalPlayer
						OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
							PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVRuinerSoundSet2, FALSE)
						ELSE
							PED_INDEX pedSender = GET_PLAYER_PED(piSenderPlayer)
							IF NOT IS_PED_INJURED(pedSender)
								PLAY_SOUND_FROM_ENTITY(-1, "Deactivate", pedSender, sVVRuinerSoundSet2, FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					RESET_NET_TIMER(tdVehAudRuiner2)
					//iVVRuinerSoundID2 = -1
				ELSE
					//triggered effects while power up is active
				ENDIF
			//ENDIF
		ENDIF
	ENDIF
	
	//Team 3
	IF MC_PlayerBD[iPartToUse].iteam = 3
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVRuinerSoundSet3)
			//IF iVVRuinerSoundID3 != -1
				PRINTLN("[RCC MISSION]  CLEANUP_VV_SOUNDS - iVVRuinerSoundID3 ! = -1")
				IF HAS_NET_TIMER_STARTED(tdVehAudRuiner3)
					PRINTLN("[RCC MISSION] CLEANUP_VV_SOUNDS - tdVehRuiner3 - timer started")
					PRINTLN("[RCC MISSION] CLEANUP_VV_SOUNDS - Ending Ruiner 3.")
					//STOP_SOUND(iVVRuinerSoundID3)
					//STOP_AUDIO_SCENE(sVVRuinerSceneSoundSet3)
					//SCRIPT_RELEASE_SOUND_ID(iVVRuinerSoundID3)
								
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVRuinerSoundSet3)
						
						PLAYER_INDEX piSenderPlayer = INT_TO_PLAYERINDEX(iVVRuinerPartSender3)
						
						IF piSenderPlayer = LocalPlayer
						OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
							PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVRuinerSoundSet3, FALSE)
						ELSE
							PED_INDEX pedSender = GET_PLAYER_PED(piSenderPlayer)
							IF NOT IS_PED_INJURED(pedSender)
								PLAY_SOUND_FROM_ENTITY(-1, "Deactivate", pedSender, sVVRuinerSoundSet3, FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					RESET_NET_TIMER(tdVehAudRuiner3)
					//iVVRuinerSoundID3 = -1
				ELSE
					//triggered effects while power up is active
				ENDIF
			//ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS()

	//######### ROCKETS LOOPING SOUND
	//Local player
	IF iVVRocketsSoundID != -1
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVRocketsSoundSet)
			IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iVehicleWeaponEffectActiveBS, ciVEH_WEP_ROCKETS)
				PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - Ending Rockets.")
				STOP_SOUND(iVVRocketsSoundID)
				STOP_AUDIO_SCENE(sVVRocketsSceneSoundSet)
				SCRIPT_RELEASE_SOUND_ID(iVVRocketsSoundID)
							
				IF NOT IS_STRING_NULL_OR_EMPTY(sVVRocketsSoundSet)
					PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVRocketsSoundSet, FALSE)
				ENDIF
				
				iVVRocketsSoundID = -1
			ELSE
				//triggered effects while power up is active
			ENDIF
		ENDIF
	ENDIF

	//######### GHOST LOOPING SOUND
	
	//need IDs for team 0 and 1
	
	//Team 0
	IF iVVGhostSoundID != -1	
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVGhostSoundSet)
			PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - iVVGhostSoundID ! = -1")
			IF HAS_NET_TIMER_STARTED(tdVehAudGhost0)
				PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - tdVehGhost0 - timer started")
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdVehAudGhost0) > g_FMMC_STRUCT.iVehicleWeaponGhostDuration * 1000
					PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - Ending Ghost 0.")
					STOP_SOUND(iVVGhostSoundID)
					STOP_AUDIO_SCENE(sVVGhostSceneSoundSet)
					SCRIPT_RELEASE_SOUND_ID(iVVGhostSoundID)
								
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVGhostSoundSet)
						PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVGhostSoundSet, FALSE)
					ENDIF
					
					//unref
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVGhostSoundSet)
					
					ENDIF
					
					RESET_NET_TIMER(tdVehAudGhost0)
					iVVGhostSoundID = -1
				ENDIF
			ELSE
				//triggered effects while power up is active
			ENDIF
		ENDIF
	ENDIF
	
	//Team 1
	IF iVVGhostSoundID1 != -1
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVGhostSoundSet1)
			PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - iVVGhostSoundID1 ! = -1")
			IF HAS_NET_TIMER_STARTED(tdVehAudGhost1)
				PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - tdVehGhost1 - timer started")
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdVehAudGhost1) > g_FMMC_STRUCT.iVehicleWeaponGhostDuration * 1000
					PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - Ending Ghost 1.")
					STOP_SOUND(iVVGhostSoundID1)
					STOP_AUDIO_SCENE(sVVGhostSceneSoundSet)
					SCRIPT_RELEASE_SOUND_ID(iVVGhostSoundID1)
								
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVGhostSoundSet1)
						PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVGhostSoundSet1, FALSE)
					ENDIF
					
					//unref
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVGhostSoundSet1)
					
					ENDIF
					
					RESET_NET_TIMER(tdVehAudGhost1)
					iVVGhostSoundID1 = -1
				ENDIF
			ELSE
				//triggered effects while power up is active
			ENDIF
		ENDIF
	ENDIF
	
	//######### BEAST LOOPING SOUND
	//team 0
	IF iVVBeastmodeSoundID != -1
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVBeastmodeSceneSoundSet)
			PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - iVVBeastmodeSoundID ! = -1")
			IF HAS_NET_TIMER_STARTED(tdVehAudBeast0)
				PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - tdVehBeast0 - timer started")
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdVehAudBeast0) > g_FMMC_STRUCT.iVehicleWeaponBeastDuration * 1000
					PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - Ending Beast 0.")
					STOP_SOUND(iVVBeastmodeSoundID)
					STOP_AUDIO_SCENE(sVVBeastmodeSceneSoundSet)
					SCRIPT_RELEASE_SOUND_ID(iVVBeastmodeSoundID)
								
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVBeastmodeSoundSet)
						PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVBeastmodeSoundSet, FALSE)
					ENDIF
					
					RESET_NET_TIMER(tdVehAudBeast0)
					iVVBeastmodeSoundID = -1
				ENDIF
			ELSE
				//triggered effects while power up is active
			ENDIF
		ENDIF
	ENDIF
	
	//Team 1
	IF iVVBeastmodeSoundID1 != -1
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVBeastmodeSoundSet1)
			PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - iVVBeastmodeSoundID1 ! = -1")
			IF HAS_NET_TIMER_STARTED(tdVehAudBeast1)
				PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - tdVehBeast1 - timer started")
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdVehAudBeast1) > g_FMMC_STRUCT.iVehicleWeaponBeastDuration * 1000
					PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - Ending Beast 1.")
					STOP_SOUND(iVVBeastmodeSoundID1)
					STOP_AUDIO_SCENE(sVVBeastmodeSoundSet1)
					SCRIPT_RELEASE_SOUND_ID(iVVBeastmodeSoundID1)
								
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVBeastmodeSoundSet1)
						PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVBeastmodeSoundSet1, FALSE)
					ENDIF
					
					RESET_NET_TIMER(tdVehAudBeast1)
					iVVBeastmodeSoundID1 = -1
				ENDIF
			ELSE
				//triggered effects while power up is active
			ENDIF
		ENDIF
	ENDIF
	
	//######### FORCED ACCEL LOOPING SOUND
	//team 0
	IF iVVForcedAccSoundID != -1
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVForcedAccSceneSoundSet)
			PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - iVVForcedAccSoundID ! = -1")
			IF HAS_NET_TIMER_STARTED(tdVehAudAccelerate0)
				PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - tdVehAccelerate0 - timer started")
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdVehAudAccelerate0) > g_FMMC_STRUCT.iVehicleWeaponJammedDuration * 1000
					PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - Ending Forced Accel 0.")
					STOP_SOUND(iVVForcedAccSoundID)
					STOP_AUDIO_SCENE(sVVForcedAccSceneSoundSet)
					SCRIPT_RELEASE_SOUND_ID(iVVForcedAccSoundID)
								
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVForcedAccSoundSet)
						PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVForcedAccSoundSet, FALSE)
					ENDIF
					
					RESET_NET_TIMER(tdVehAudAccelerate0)
					iVVForcedAccSoundID = -1
				ENDIF
			ELSE
				//triggered effects while power up is active
			ENDIF
		ENDIF
	ENDIF
	
	//Team 1
	IF iVVForcedAccSoundID1 != -1
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVForcedAccSoundSet1)
			PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - iVVForcedAccSoundID1 ! = -1")
			IF HAS_NET_TIMER_STARTED(tdVehAudAccelerate1)
				PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - tdVehAccelerate1 - timer started")
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdVehAudAccelerate1) > g_FMMC_STRUCT.iVehicleWeaponJammedDuration * 1000
					PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - Ending Forced Accel 1.")
					STOP_SOUND(iVVForcedAccSoundID1)
					STOP_AUDIO_SCENE(sVVForcedAccSoundSet1)
					SCRIPT_RELEASE_SOUND_ID(iVVForcedAccSoundID1)
								
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVForcedAccSoundSet1)
						PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVForcedAccSoundSet1, FALSE)
					ENDIF
					
					RESET_NET_TIMER(tdVehAudAccelerate1)
					iVVForcedAccSoundID1 = -1
				ENDIF
			ELSE
				//triggered effects while power up is active
			ENDIF
		ENDIF
	ENDIF
	
	//######### FLIPPED LOOPING SOUND
	//Team 0
	IF iVVFlippedSoundID != -1
		//IF NOT IS_STRING_NULL_OR_EMPTY(sVVFlippedSceneSoundSet)
			PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - iVVFlippedSoundID ! = -1")
			IF HAS_NET_TIMER_STARTED(tdVehAudFlipped0)
				PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - tdVehFlipped0 - timer started")
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdVehAudFlipped0) > g_FMMC_STRUCT.iVehicleWeaponFlippedDuration * 1000
					PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - Ending Flipped 0.")
					STOP_SOUND(iVVFlippedSoundID)
					STOP_AUDIO_SCENE(sVVFlippedSceneSoundSet)
					SCRIPT_RELEASE_SOUND_ID(iVVFlippedSoundID)
								
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVFlippedSoundSet)
						
						PLAYER_INDEX piSenderPlayer = INT_TO_PLAYERINDEX(iVVFlippedPartSender)
						
						IF piSenderPlayer = LocalPlayer
						OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
							PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVFlippedSoundSet, FALSE)
						ELSE
							PED_INDEX pedSender = GET_PLAYER_PED(piSenderPlayer)
							IF NOT IS_PED_INJURED(pedSender)
								PLAY_SOUND_FROM_ENTITY(-1, "Deactivate", pedSender, sVVFlippedSoundSet, FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					RESET_NET_TIMER(tdVehAudFlipped0)
					iVVFlippedSoundID= -1
				ENDIF
			ELSE
				//triggered effects while power up is active
			ENDIF
		//ENDIF
	ENDIF
	
	//Team 1
	IF iVVFlippedSoundID1 != -1
		//IF NOT IS_STRING_NULL_OR_EMPTY(sVVFlippedSoundSet1)
			PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - iVVFlippedSoundID1 ! = -1")
			IF HAS_NET_TIMER_STARTED(tdVehAudFlipped1)
				PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - tdVehFlipped1 - timer started")
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdVehAudFlipped1) > g_FMMC_STRUCT.iVehicleWeaponFlippedDuration * 1000
					PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - Ending Flipped 1.")
					STOP_SOUND(iVVFlippedSoundID1)
					STOP_AUDIO_SCENE(sVVFlippedSoundSet1)
					SCRIPT_RELEASE_SOUND_ID(iVVFlippedSoundID1)
								
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVFlippedSoundSet1)
						
						PLAYER_INDEX piSenderPlayer = INT_TO_PLAYERINDEX(iVVFlippedPartSender1)
						
						IF piSenderPlayer = LocalPlayer
						OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
							PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVFlippedSoundSet1, FALSE)
						ELSE
							PED_INDEX pedSender = GET_PLAYER_PED(piSenderPlayer)
							IF NOT IS_PED_INJURED(pedSender)
								PLAY_SOUND_FROM_ENTITY(-1, "Deactivate", pedSender, sVVFlippedSoundSet1, FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					RESET_NET_TIMER(tdVehAudFlipped1)
					iVVFlippedSoundID1 = -1
				ENDIF
			ELSE
				//triggered effects while power up is active
			ENDIF
		//ENDIF
	ENDIF
		
	//Team 2
	IF iVVFlippedSoundID2 != -1
		//IF NOT IS_STRING_NULL_OR_EMPTY(sVVFlippedSoundSet2)
			PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - iVVFlippedSoundID2 ! = -1")
			IF HAS_NET_TIMER_STARTED(tdVehAudFlipped2)
				PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - tdVehFlipped2 - timer started")
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdVehAudFlipped2) > g_FMMC_STRUCT.iVehicleWeaponFlippedDuration * 1000
					PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - Ending Flipped 2.")
					STOP_SOUND(iVVFlippedSoundID2)
					STOP_AUDIO_SCENE(sVVFlippedSoundSet2)
					SCRIPT_RELEASE_SOUND_ID(iVVFlippedSoundID2)
								
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVFlippedSoundSet2)
						
						PLAYER_INDEX piSenderPlayer = INT_TO_PLAYERINDEX(iVVFlippedPartSender2)
						
						IF piSenderPlayer = LocalPlayer
						OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
							PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVFlippedSoundSet2, FALSE)
						ELSE
							PED_INDEX pedSender = GET_PLAYER_PED(piSenderPlayer)
							IF NOT IS_PED_INJURED(pedSender)
								PLAY_SOUND_FROM_ENTITY(-1, "Deactivate", pedSender, sVVFlippedSoundSet2, FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					RESET_NET_TIMER(tdVehAudFlipped2)
					iVVFlippedSoundID2 = -1
				ENDIF
			ELSE
				//triggered effects while power up is active
			ENDIF
		//ENDIF
	ENDIF
	
	//Team 3
	IF iVVFlippedSoundID3 != -1
		//IF NOT IS_STRING_NULL_OR_EMPTY(sVVFlippedSoundSet3)
			PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - iVVFlippedSoundID3 ! = -1")
			IF HAS_NET_TIMER_STARTED(tdVehAudFlipped3)
				PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - tdVehFlipped3 - timer started")
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdVehAudFlipped3) > g_FMMC_STRUCT.iVehicleWeaponFlippedDuration * 1000
					PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - Ending Flipped 3.")
					STOP_SOUND(iVVFlippedSoundID3)
					STOP_AUDIO_SCENE(sVVFlippedSoundSet3)
					SCRIPT_RELEASE_SOUND_ID(iVVFlippedSoundID3)
					
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVFlippedSoundSet3)
						
						PLAYER_INDEX piSenderPlayer = INT_TO_PLAYERINDEX(iVVFlippedPartSender3)
						
						IF piSenderPlayer = LocalPlayer
						OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
							PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVFlippedSoundSet3, FALSE)
						ELSE
							PED_INDEX pedSender = GET_PLAYER_PED(piSenderPlayer)
							IF NOT IS_PED_INJURED(pedSender)
								PLAY_SOUND_FROM_ENTITY(-1, "Deactivate", pedSender, sVVFlippedSoundSet3, FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					RESET_NET_TIMER(tdVehAudFlipped3)
					iVVFlippedSoundID3 = -1
				ENDIF
			ELSE
				//triggered effects while power up is active
			ENDIF
		//ENDIF
	ENDIF
	
	//######### ZONED LOOPING SOUND
	IF iVVZonedSoundID != -1
		PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - iVVZonedSoundID ! = -1")
		IF HAS_NET_TIMER_STARTED(tdVehAudZoned)
			PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - tdVehZoned")
			IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdVehAudZoned) > g_FMMC_STRUCT.iVehicleWeaponZonedDuration * 1000
				PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - Ending Zoned.")
				STOP_SOUND(iVVZonedSoundID)
				STOP_AUDIO_SCENE(sVVZonedSceneSoundSet)
				SCRIPT_RELEASE_SOUND_ID(iVVZonedSoundID)
							
				IF NOT IS_STRING_NULL_OR_EMPTY(sVVZonedSoundSet)
					PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVZonedSoundSet, FALSE)
				ENDIF
				
				RESET_NET_TIMER(tdVehAudZoned)
				iVVZonedSoundID = -1
			ENDIF
		ELSE
			//triggered effects while power up is active
		ENDIF
	ENDIF
	
	//######### DETONATE LOOPING SOUND
//	IF iVVDetonateSoundID != -1
//		//Local player
//		IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iVehicleWeaponEffectActiveBS, ciVEH_WEP_DETONATE)
//			PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - ending Detonate.")
//			STOP_SOUND(iVVDetonateSoundID)
//			STOP_AUDIO_SCENE(sVVDetonateSceneSoundSet)
//			SCRIPT_RELEASE_SOUND_ID(iVVDetonateSoundID)
//						
//			IF NOT IS_STRING_NULL_OR_EMPTY(sVVDetonateSoundSet)
//				PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVDetonateSoundSet, FALSE)
//			ENDIF
//			
//			iVVDetonateSoundID = -1
//		ELSE
//			//triggered effects while power up is active
//		ENDIF
//	ENDIF

	INT iPart
	
	//######### BOMB LOOPING SOUND
	IF iVVBombSoundID != -1
		FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
			IF IS_BIT_SET(bsPlayersWithBombActive, iPart)
				IF MC_playerBD[iPart].iQuantityOfBombs <= 0
					PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - Clearing bomb id")
					STOP_SOUND(iVVBombSoundID)
					STOP_AUDIO_SCENE(sVVBombSceneSoundSet)
					SCRIPT_RELEASE_SOUND_ID(iVVBombSoundID)
					
					CLEAR_BIT(bsPlayersWithBombActive, iPart)
								
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVBombSoundSet)
						PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVBombSoundSet, FALSE)
					ENDIF
					
					iVVBombSoundID = -1
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	//######### PRON LOOPING SOUND
	//Team 0
	IF iVVPronSoundID != -1
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVPronSoundSet)
			PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - iVVPronSoundID! = -1")
			IF HAS_NET_TIMER_STARTED(tdVehAudPron0)
				PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - tdVehPron0")
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdVehAudPron0) > g_FMMC_STRUCT.iVehicleWeaponPronDuration * 1000
					PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - ending Pron 0.")
					STOP_SOUND(iVVPronSoundID)
					STOP_AUDIO_SCENE(sVVPronSceneSoundSet)
					SCRIPT_RELEASE_SOUND_ID(iVVPronSoundID)
								
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVPronSoundSet)
						
						PLAYER_INDEX piSenderPlayer = INT_TO_PLAYERINDEX(iVVPronPartSender)
						
						IF piSenderPlayer = LocalPlayer
						OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
							PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVPronSoundSet, FALSE)
						ELSE
							PED_INDEX pedSender = GET_PLAYER_PED(piSenderPlayer)
							IF NOT IS_PED_INJURED(pedSender)
								PLAY_SOUND_FROM_ENTITY(-1, "Deactivate", pedSender, sVVPronSoundSet, FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					RESET_NET_TIMER(tdVehAudPron0)
					iVVPronSoundID = -1
				ENDIF
			ELSE
				//triggered effects while power up is active
			ENDIF
		ENDIF
	ENDIF
	
	//Team 1
	IF iVVPronSoundID1 != -1
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVPronSoundSet1)
			PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - iVVPronSoundID1 ! = -1")
			IF HAS_NET_TIMER_STARTED(tdVehAudPron1)
				PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - tdVehPron1")
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdVehAudPron1) > g_FMMC_STRUCT.iVehicleWeaponPronDuration * 1000
					PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - ending Pron 1.")
					STOP_SOUND(iVVPronSoundID1)
					STOP_AUDIO_SCENE(sVVPronSceneSoundSet)
					SCRIPT_RELEASE_SOUND_ID(iVVPronSoundID1)
								
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVPronSoundSet1)
						
						PLAYER_INDEX piSenderPlayer = INT_TO_PLAYERINDEX(iVVPronPartSender1)
						
						IF piSenderPlayer = LocalPlayer
						OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
							PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVPronSoundSet1, FALSE)
						ELSE
							PED_INDEX pedSender = GET_PLAYER_PED(piSenderPlayer)
							IF NOT IS_PED_INJURED(pedSender)
								PLAY_SOUND_FROM_ENTITY(-1, "Deactivate", pedSender, sVVPronSoundSet1, FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					RESET_NET_TIMER(tdVehAudPron1)
					iVVPronSoundID1 = -1
				ENDIF
			ELSE
				//triggered effects while power up is active
			ENDIF
		ENDIF
	ENDIF
	
	//Team 2
	IF iVVPronSoundID2 != -1
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVPronSoundSet2)
			PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - iVVPronSoundID2 ! = -1")
			IF HAS_NET_TIMER_STARTED(tdVehAudPron2)
				PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - tdVehPron2")
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdVehAudPron2) > g_FMMC_STRUCT.iVehicleWeaponPronDuration * 1000
					PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - ending Pron 2.")
					STOP_SOUND(iVVPronSoundID2)
					STOP_AUDIO_SCENE(sVVPronSceneSoundSet)
					SCRIPT_RELEASE_SOUND_ID(iVVPronSoundID2)
								
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVPronSoundSet2)
						
						PLAYER_INDEX piSenderPlayer = INT_TO_PLAYERINDEX(iVVPronPartSender2)
						
						IF piSenderPlayer = LocalPlayer
						OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
							PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVPronSoundSet2, FALSE)
						ELSE
							PED_INDEX pedSender = GET_PLAYER_PED(piSenderPlayer)
							IF NOT IS_PED_INJURED(pedSender)
								PLAY_SOUND_FROM_ENTITY(-1, "Deactivate", pedSender, sVVPronSoundSet2, FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					RESET_NET_TIMER(tdVehAudPron2)
					iVVPronSoundID2 = -1
				ENDIF
			ELSE
				//triggered effects while power up is active
			ENDIF
		ENDIF
	ENDIF
	
	//Team 3
	IF iVVPronSoundID3 != -1
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVPronSoundSet3)
			PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - iVVPronSoundID3 ! = -1")
			IF HAS_NET_TIMER_STARTED(tdVehAudPron3)
				PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - tdVehPron3")
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdVehAudPron3) > g_FMMC_STRUCT.iVehicleWeaponPronDuration * 1000
					PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - ending Pron 3.")
					STOP_SOUND(iVVPronSoundID3)
					STOP_AUDIO_SCENE(sVVPronSceneSoundSet)
					SCRIPT_RELEASE_SOUND_ID(iVVPronSoundID3)
								
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVPronSoundSet3)
						
						PLAYER_INDEX piSenderPlayer = INT_TO_PLAYERINDEX(iVVPronPartSender3)
						
						IF piSenderPlayer = LocalPlayer
						OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
							PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVPronSoundSet3, FALSE)
						ELSE
							PED_INDEX pedSender = GET_PLAYER_PED(piSenderPlayer)
							IF NOT IS_PED_INJURED(pedSender)
								PLAY_SOUND_FROM_ENTITY(-1, "Deactivate", pedSender, sVVPronSoundSet3, FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					RESET_NET_TIMER(tdVehAudPron3)
					iVVPronSoundID3 = -1
				ENDIF
			ELSE
				//triggered effects while power up is active
			ENDIF
		ENDIF
	ENDIF
	
	
	//######### REPAIR LOOPING SOUND
	IF iVVRepairSoundID != -1
		IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iVehicleWeaponEffectActiveBS, ciVEH_WEP_REPAIR)
			PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - ending Repair.")
			STOP_SOUND(iVVRepairSoundID)
			STOP_AUDIO_SCENE(sVVRepairSceneSoundSet)
			SCRIPT_RELEASE_SOUND_ID(iVVRepairSoundID)
						
			IF NOT IS_STRING_NULL_OR_EMPTY(sVVRepairSoundSet)
				PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVRepairSoundSet, FALSE)
			ENDIF
			
			iVVRepairSoundID = -1
		ELSE
			//triggered effects while power up is active
		ENDIF
	ENDIF
	
	//######### BOOST LOOPING SOUND
	IF iVVSpeedBoostID != -1
		IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iVehicleWeaponEffectActiveBS, ciVEH_WEP_SPEED_BOOST)
			PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - Ending Speed Boost.")
			STOP_SOUND(iVVSpeedBoostID)
			STOP_AUDIO_SCENE(sVVSpeedBoostSceneSoundSet)
			SCRIPT_RELEASE_SOUND_ID(iVVSpeedBoostID)
						
			IF NOT IS_STRING_NULL_OR_EMPTY(sVVSpeedBoostSoundSet)
				PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVSpeedBoostSoundSet, FALSE)
			ENDIF
			
			iVVSpeedBoostID = -1
		ELSE
			//triggered effects while power up is active
		ENDIF
	ENDIF
	
	//######### RAMP BUGGY LOOPING SOUND
	//Team 0
	//IF iVVRampSoundID != -1
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVRampSoundSet)
			//PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - iVVRampSoundID! = -1")
			IF HAS_NET_TIMER_STARTED(tdVehAudRamp0)
				PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - tdVehRamp0")
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdVehAudRamp0) > g_FMMC_STRUCT.iVehicleWeaponRampDuration * 1000
					PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - ending Ramp 0.")
					//STOP_SOUND(iVVRampSoundID)
					//STOP_AUDIO_SCENE(sVVRampSceneSoundSet)
					//SCRIPT_RELEASE_SOUND_ID(iVVRampSoundID)
								
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVRampSoundSet)
						
						PLAYER_INDEX piSenderPlayer = INT_TO_PLAYERINDEX(iVVRampPartSender)
						
						IF piSenderPlayer = LocalPlayer
						OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
							PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVRampSoundSet, FALSE)
						ELSE
							PED_INDEX pedSender = GET_PLAYER_PED(piSenderPlayer)
							IF NOT IS_PED_INJURED(pedSender)
								PLAY_SOUND_FROM_ENTITY(-1, "Deactivate", pedSender, sVVRampSoundSet, FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					RESET_NET_TIMER(tdVehAudRamp0)
					//iVVRampSoundID = -1
				ENDIF
			ELSE
				//triggered effects while power up is active
			ENDIF
		ENDIF
	//ENDIF
	
	//Team 1
	//IF iVVRampSoundID1 != -1
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVRampSoundSet1)
			//PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - iVVRampSoundID1! = -1")
			IF HAS_NET_TIMER_STARTED(tdVehAudRamp1)
				PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - tdVehRamp1")
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdVehAudRamp1) > g_FMMC_STRUCT.iVehicleWeaponRampDuration * 1000
					PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - ending Ramp 1.")
					//STOP_SOUND(iVVRampSoundID1)
					//STOP_AUDIO_SCENE(sVVRampSceneSoundSet1)
					//SCRIPT_RELEASE_SOUND_ID(iVVRampSoundID1)
		
					PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND - iVVRampPartSender3:", iVVRampPartSender)
					PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND - iVVRampPartSender3:", iVVRampPartSender1)
					PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND - iVVRampPartSender3:", iVVRampPartSender2)
					PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND - iVVRampPartSender3:", iVVRampPartSender3)
					
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVRampSoundSet1)
						
						PLAYER_INDEX piSenderPlayer = INT_TO_PLAYERINDEX(iVVRampPartSender1)
						
						IF piSenderPlayer = LocalPlayer
						OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
							PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVRampSoundSet1, FALSE)
						ELSE
							PED_INDEX pedSender = GET_PLAYER_PED(piSenderPlayer)
							IF NOT IS_PED_INJURED(pedSender)
								PLAY_SOUND_FROM_ENTITY(-1, "Deactivate", pedSender, sVVRampSoundSet1, FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					RESET_NET_TIMER(tdVehAudRamp1)
					//iVVRampSoundID1 = -1
				ENDIF
			ELSE
				//triggered effects while power up is active
			ENDIF
		ENDIF
	//ENDIF
	
	//Team 2
	//IF iVVRampSoundID2 != -1
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVRampSoundSet2)
			//PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - iVVRampSoundID2! = -1")
			IF HAS_NET_TIMER_STARTED(tdVehAudRamp2)
				PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - tdVehRamp2")
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdVehAudRamp2) > g_FMMC_STRUCT.iVehicleWeaponRampDuration * 1000
					PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - ending Ramp 2.")
					//STOP_SOUND(iVVRampSoundID2)
					//STOP_AUDIO_SCENE(sVVRampSceneSoundSet2)
					//SCRIPT_RELEASE_SOUND_ID(iVVRampSoundID2)
								
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVRampSoundSet2)
						
						PLAYER_INDEX piSenderPlayer = INT_TO_PLAYERINDEX(iVVRampPartSender2)
						
						IF piSenderPlayer = LocalPlayer
						OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
							PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVRampSoundSet2, FALSE)
						ELSE
							PED_INDEX pedSender = GET_PLAYER_PED(piSenderPlayer)
							IF NOT IS_PED_INJURED(pedSender)
								PLAY_SOUND_FROM_ENTITY(-1, "Deactivate", pedSender, sVVRampSoundSet2, FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					RESET_NET_TIMER(tdVehAudRamp2)
					//iVVRampSoundID2 = -1
				ENDIF
			ELSE
				//triggered effects while power up is active
			ENDIF
		ENDIF
	//ENDIF
	
	//Team 3
	//IF iVVRampSoundID3 != -1
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVRampSoundSet3)
			//PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - iVVRampSoundID3! = -1")
			IF HAS_NET_TIMER_STARTED(tdVehAudRamp3)
				PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - tdVehRamp3")
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdVehAudRamp3) > g_FMMC_STRUCT.iVehicleWeaponRampDuration * 1000
					PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - ending Ramp 3.")
					//STOP_SOUND(iVVRampSoundID3)
					//STOP_AUDIO_SCENE(sVVRampSceneSoundSet3)
					//SCRIPT_RELEASE_SOUND_ID(iVVRampSoundID3)
								
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVRampSoundSet3)
						
						PLAYER_INDEX piSenderPlayer = INT_TO_PLAYERINDEX(iVVRampPartSender3)
						
						IF piSenderPlayer = LocalPlayer
						OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
							PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVRampSoundSet3, FALSE)
						ELSE
							PED_INDEX pedSender = GET_PLAYER_PED(piSenderPlayer)
							IF NOT IS_PED_INJURED(pedSender)
								PLAY_SOUND_FROM_ENTITY(-1, "Deactivate", pedSender, sVVRampSoundSet3, FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					RESET_NET_TIMER(tdVehAudRamp3)
					//iVVRampSoundID3 = -1
				ENDIF
			ELSE
				//triggered effects while power up is active
			ENDIF
		ENDIF
	//ENDIF
	
	// ######### Ruiner Special Vehicle LOOPING SOUND #############
	//Team 0
	//IF iVVRuinerSoundID != -1
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVRuinerSoundSet)
			//PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - iVVRuinerSoundID! = -1")
			IF HAS_NET_TIMER_STARTED(tdVehAudRuiner0)
				PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - tdVehRuiner0")
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdVehAudRuiner0) > g_FMMC_STRUCT.iVehicleWeaponRuinerDuration * 1000
					PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - ending Ruiner 0.")
					//STOP_SOUND(iVVRuinerSoundID)
					//STOP_AUDIO_SCENE(sVVRuinerSceneSoundSet)
					//SCRIPT_RELEASE_SOUND_ID(iVVRuinerSoundID)
								
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVRuinerSoundSet)
						
						PLAYER_INDEX piSenderPlayer = INT_TO_PLAYERINDEX(iVVRuinerPartSender)
						
						IF piSenderPlayer = LocalPlayer
						OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
							PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVRuinerSoundSet, FALSE)
						ELSE
							PED_INDEX pedSender = GET_PLAYER_PED(piSenderPlayer)
							IF NOT IS_PED_INJURED(pedSender)
								PLAY_SOUND_FROM_ENTITY(-1, "Deactivate", pedSender, sVVRuinerSoundSet, FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					RESET_NET_TIMER(tdVehAudRuiner0)
					//iVVRuinerSoundID = -1
				ENDIF
			ELSE
				//triggered effects while power up is active
			ENDIF
		ENDIF
	//ENDIF
		
	//Team 1
	//IF iVVRuinerSoundID1 != -1
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVRuinerSoundSet1)
			//PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - iVVRuinerSoundID1! = -1")
			IF HAS_NET_TIMER_STARTED(tdVehAudRuiner1)
				PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - tdVehRuiner1")
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdVehAudRuiner1) > g_FMMC_STRUCT.iVehicleWeaponRuinerDuration * 1000
					PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - ending Ruiner 1.")
					//STOP_SOUND(iVVRuinerSoundID1)
					//STOP_AUDIO_SCENE(sVVRuinerSceneSoundSet1)
					//SCRIPT_RELEASE_SOUND_ID(iVVRuinerSoundID1)
								
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVRuinerSoundSet1)
						
						PLAYER_INDEX piSenderPlayer = INT_TO_PLAYERINDEX(iVVRuinerPartSender1)
						
						IF piSenderPlayer = LocalPlayer
						OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
							PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVRuinerSoundSet1, FALSE)
						ELSE
							PED_INDEX pedSender = GET_PLAYER_PED(piSenderPlayer)
							IF NOT IS_PED_INJURED(pedSender)
								PLAY_SOUND_FROM_ENTITY(-1, "Deactivate", pedSender, sVVRuinerSoundSet1, FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					RESET_NET_TIMER(tdVehAudRuiner1)
					//iVVRuinerSoundID1 = -1
				ENDIF
			ELSE
				//triggered effects while power up is active
			ENDIF
		ENDIF
	//ENDIF
	
	//Team 2
	//IF iVVRuinerSoundID2 != -1
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVRuinerSoundSet2)
			//PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - iVVRuinerSoundID2! = -1")
			IF HAS_NET_TIMER_STARTED(tdVehAudRuiner2)
				PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - tdVehRuiner2")
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdVehAudRuiner2) > g_FMMC_STRUCT.iVehicleWeaponRuinerDuration * 1000
					PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - ending Ruiner 2.")
					//STOP_SOUND(iVVRuinerSoundID2)
					//STOP_AUDIO_SCENE(sVVRuinerSceneSoundSet2)
					//SCRIPT_RELEASE_SOUND_ID(iVVRuinerSoundID2)
								
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVRuinerSoundSet2)
						
						PLAYER_INDEX piSenderPlayer = INT_TO_PLAYERINDEX(iVVRuinerPartSender2)
						
						IF piSenderPlayer = LocalPlayer
						OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
							PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVRuinerSoundSet2, FALSE)
						ELSE
							PED_INDEX pedSender = GET_PLAYER_PED(piSenderPlayer)
							IF NOT IS_PED_INJURED(pedSender)
								PLAY_SOUND_FROM_ENTITY(-1, "Deactivate", pedSender, sVVRuinerSoundSet2, FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					RESET_NET_TIMER(tdVehAudRuiner2)
					//iVVRuinerSoundID2 = -1
				ENDIF
			ELSE
				//triggered effects while power up is active
			ENDIF
		ENDIF
	//ENDIF
	
	//Team 3
	//IF iVVRuinerSoundID3 != -1
		IF NOT IS_STRING_NULL_OR_EMPTY(sVVRuinerSoundSet3)
			//PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - iVVRuinerSoundID3! = -1")
			IF HAS_NET_TIMER_STARTED(tdVehAudRuiner3)
				PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - tdVehRuiner3")
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdVehAudRuiner3) > g_FMMC_STRUCT.iVehicleWeaponRuinerDuration * 1000
					PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS - ending Ruiner 3.")
					//STOP_SOUND(iVVRuinerSoundID3)
					//STOP_AUDIO_SCENE(sVVRuinerSceneSoundSet3)
					//SCRIPT_RELEASE_SOUND_ID(iVVRuinerSoundID3)
								
					IF NOT IS_STRING_NULL_OR_EMPTY(sVVRuinerSoundSet3)
						
						PLAYER_INDEX piSenderPlayer = INT_TO_PLAYERINDEX(iVVRuinerPartSender3)
						
						IF piSenderPlayer = LocalPlayer
						OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
							PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVRuinerSoundSet3, FALSE)
						ELSE
							PED_INDEX pedSender = GET_PLAYER_PED(piSenderPlayer)
							IF NOT IS_PED_INJURED(pedSender)
								PLAY_SOUND_FROM_ENTITY(-1, "Deactivate", pedSender, sVVRuinerSoundSet3, FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					RESET_NET_TIMER(tdVehAudRuiner3)
					//iVVRuinerSoundID3 = -1
				ENDIF
			ELSE
				//triggered effects while power up is active
			ENDIF
		ENDIF
	//ENDIF
ENDPROC

PROC PLAY_RUGBY_SOUND(INT iSoundToPlay, PLAYER_INDEX piSenderPlayer)

	PRINTLN("[JJT] PLAY_RUGBY_SOUND - iSoundToPlay = ", iSoundToPlay)
	
	SWITCH iSoundToPlay
		CASE ciRUGBY_SOUND_COLLECTED_BALL
			PLAY_SOUND_FROM_COORD(-1, "Collect", GET_ENTITY_COORDS(GET_PLAYER_PED(piSenderPlayer), FALSE), "DLC_Low2_Ibi_Sounds")
		BREAK
		CASE ciRUGBY_SOUND_DROPPED_BALL	
			PLAY_SOUND_FROM_COORD(-1, "Drop", GET_ENTITY_COORDS(GET_PLAYER_PED(piSenderPlayer), FALSE), "DLC_Low2_Ibi_Sounds")
		BREAK
		CASE ciRUGBY_SOUND_SCORED			
			PLAY_SOUND_FRONTEND(-1, "Score", "DLC_Low2_Ibi_Sounds", FALSE)
		BREAK
	ENDSWITCH
ENDPROC

PROC PLAY_IN_AND_OUT_SOUND(INT iActionType, INT iTeam, PLAYER_INDEX piSenderPlayer)
	PRINTLN("[JJT] PLAY_IN_AND_OUT_SOUND - iActionType = ", iActionType, "iSenderTeam = ", iTeam,
			", myTeam = ", MC_playerBD[iPartToUse].iteam)

	SWITCH iActionType
		CASE ciIN_AND_OUT_SOUND_PICKUP	
			IF iTeam = MC_playerBD[iPartToUse].iteam
				IF piSenderPlayer = LocalPlayer
					PLAY_SOUND_FRONTEND(-1, "Player_Pick_Up", "In_And_Out_Attacker_Sounds", FALSE)
				ELSE
					PLAY_SOUND_FRONTEND(-1, "Friend_Pick_Up", "In_And_Out_Attacker_Sounds", FALSE)
				ENDIF
			ELSE
				PLAY_SOUND_FRONTEND(-1, "Enemy_Pick_Up", "In_And_Out_Defender_Sounds", FALSE)
			ENDIF
		BREAK
		CASE ciIN_AND_OUT_SOUND_DROP	
			IF iTeam = MC_playerBD[iPartToUse].iteam
				PLAY_SOUND_FRONTEND(-1, "Dropped", "In_And_Out_Attacker_Sounds", FALSE)
			ELSE
				PLAY_SOUND_FRONTEND(-1, "Dropped", "In_And_Out_Defender_Sounds", FALSE)
			ENDIF
		BREAK
		CASE ciIN_AND_OUT_SOUND_DELIVERED	
			IF iTeam = MC_playerBD[iPartToUse].iteam
				PLAY_SOUND_FRONTEND(-1, "Deliver", "In_And_Out_Attacker_Sounds", FALSE)
			ELSE
				PLAY_SOUND_FRONTEND(-1, "Enemy_Deliver", "In_And_Out_Defender_Sounds", FALSE)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC STOP_ALL_POWER_PLAY_PICKUP_AUDIO()

	#IF IS_DEBUG_BUILD
		PRINTLN("STOP_ALL_POWER_PLAY_PICKUP_AUDIO called")
		DEBUG_PRINTCALLSTACK()
	#ENDIF

	//POWER PLAY SOUNDS/SCENES
	IF iPPBullsharkSoundID != -1 OR iPPBeastmodeSoundID != -1 OR iPPThermalSoundID != -1
	OR iPPSwapSoundID != -1 	 OR iPPFilterSoundID != -1	  OR iPPBulletTimeSoundID != -1 OR iPPHideBlipsSoundID != -1
		IF IS_SOUND_ID_VALID(iPPBullsharkSoundID) AND NOT HAS_SOUND_FINISHED(iPPBullsharkSoundID)
			STOP_SOUND(iPPBullsharkSoundID)
		ENDIF 
		
		IF IS_SOUND_ID_VALID(iPPBeastmodeSoundID) AND NOT HAS_SOUND_FINISHED(iPPBeastmodeSoundID)
			STOP_SOUND(iPPBeastmodeSoundID)
		ENDIF
		
		IF IS_SOUND_ID_VALID(iPPThermalSoundID) AND NOT HAS_SOUND_FINISHED(iPPThermalSoundID)
			STOP_SOUND(iPPThermalSoundID)
		ENDIF
		
		IF IS_SOUND_ID_VALID(iPPSwapSoundID) AND NOT HAS_SOUND_FINISHED(iPPSwapSoundID)
			STOP_SOUND(iPPSwapSoundID)
		ENDIF
		
		IF IS_SOUND_ID_VALID(iPPFilterSoundID) AND NOT HAS_SOUND_FINISHED(iPPFilterSoundID)
			STOP_SOUND(iPPFilterSoundID)
		ENDIF
		
		IF IS_SOUND_ID_VALID(iPPBulletTimeSoundID) AND NOT HAS_SOUND_FINISHED(iPPBulletTimeSoundID)
			STOP_SOUND(iPPBulletTimeSoundID)
		ENDIF
		
		IF IS_SOUND_ID_VALID(iPPHideBlipsSoundID) AND NOT HAS_SOUND_FINISHED(iPPHideBlipsSoundID)
			STOP_SOUND(iPPHideBlipsSoundID)
		ENDIF
		
		IF NOT IS_STRING_EMPTY(sPPBullsharkSceneSoundSet)
			IF IS_AUDIO_SCENE_ACTIVE(sPPBullsharkSceneSoundSet)
				STOP_AUDIO_SCENE(sPPBullsharkSceneSoundSet) 
			ENDIF
		ENDIF	
		
		IF NOT IS_STRING_EMPTY(sPPBeastmodeSceneSoundSet)
			IF IS_AUDIO_SCENE_ACTIVE(sPPBeastmodeSceneSoundSet)
				STOP_AUDIO_SCENE(sPPBeastmodeSceneSoundSet)
			ENDIF
		ENDIF	
		
		IF NOT IS_STRING_EMPTY(sPPFilterSceneSoundSet)
			IF IS_AUDIO_SCENE_ACTIVE(sPPFilterSceneSoundSet)
				STOP_AUDIO_SCENE(sPPFilterSceneSoundSet)
			ENDIF
		ENDIF
		
		IF NOT IS_STRING_EMPTY(sPPThermalSceneSoundSet)
			IF IS_AUDIO_SCENE_ACTIVE(sPPThermalSceneSoundSet)
				STOP_AUDIO_SCENE(sPPThermalSceneSoundSet)
			ENDIF
		ENDIF	
		
		IF NOT IS_STRING_EMPTY(sPPSwapSceneSoundSet)
			IF IS_AUDIO_SCENE_ACTIVE(sPPSwapSceneSoundSet)
				STOP_AUDIO_SCENE(sPPSwapSceneSoundSet)
			ENDIF
		ENDIF	
		
		IF NOT IS_STRING_EMPTY(sPPBulletTimeSceneSoundSet)
			IF IS_AUDIO_SCENE_ACTIVE(sPPBulletTimeSceneSoundSet)
				STOP_AUDIO_SCENE(sPPBulletTimeSceneSoundSet)
			ENDIF
		ENDIF
		
		IF NOT IS_STRING_EMPTY(sPPHideBlipsSceneSoundSet)
			IF IS_AUDIO_SCENE_ACTIVE(sPPHideBlipsSceneSoundSet)
				STOP_AUDIO_SCENE(sPPHideBlipsSceneSoundSet)
			ENDIF
		ENDIF
		
	ENDIF
	
	RESET_NET_TIMER(stPPBullsharkTimer)
	RESET_NET_TIMER(stPPBeastmodeTimer)
	RESET_NET_TIMER(stPPThermalTimer)
	RESET_NET_TIMER(stPPSwapTimer)
	RESET_NET_TIMER(stPPFilterTimer)
	RESET_NET_TIMER(stPPBulletTimeTimer)
	RESET_NET_TIMER(stPPHideBlipTimer)
	
	
	SCRIPT_RELEASE_SOUND_ID(iPPBullsharkSoundID)
	SCRIPT_RELEASE_SOUND_ID(iPPBeastmodeSoundID)
	SCRIPT_RELEASE_SOUND_ID(iPPThermalSoundID)
	SCRIPT_RELEASE_SOUND_ID(iPPSwapSoundID)
	SCRIPT_RELEASE_SOUND_ID(iPPFilterSoundID)
	SCRIPT_RELEASE_SOUND_ID(iPPBulletTimeSoundID)
	SCRIPT_RELEASE_SOUND_ID(iPPHideBlipsSoundID)

ENDPROC

PROC STOP_AUDIO_SCENE_FOR_ARENA_VIP_TRANSITION(BOOL bForce = FALSE)
	IF (g_bIntroJobShardPlayed AND IS_SKYSWOOP_AT_GROUND())
	OR bForce
		IF IS_AUDIO_SCENE_ACTIVE("DLC_AW_Arena_Lobby_Veh_Select_To_VIP_Transition_Scene")
			STOP_AUDIO_SCENE("DLC_AW_Arena_Lobby_Veh_Select_To_VIP_Transition_Scene")
		ENDIF
		IF IS_AUDIO_SCENE_ACTIVE("DLC_AW_Arena_Lobby_Veh_Select_To_Arena_Transition_Scene")
			STOP_AUDIO_SCENE("DLC_AW_Arena_Lobby_Veh_Select_To_Arena_Transition_Scene")
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_DEFUNCT_BASE_BODY_SCANNER()
	
	IF NOT HAS_NET_TIMER_STARTED(tdMissionBodyScannerTimer)
		// Check if the local player is inside the body scanner. This check 
		// is quicker than staggering through all players in the facility.
						
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), sMissionBodyScannerData.vMin, sMissionBodyScannerData.vMax, sMissionBodyScannerData.fWidth)
			START_NET_TIMER(tdMissionBodyScannerTimer)	
						
			PRINTLN("[RCC MISSION] starting tdMissionBodyScannerTimer")

			PLAY_SOUND_FROM_COORD(-1, "scanner_alarm_os", sMissionBodyScannerData.vAudioSource, "dlc_xm_iaa_player_facility_sounds", TRUE, 100, FALSE)
			
			BROADCAST_EVENT_FMMC_TRIGGERED_BODY_SCANNER()
		ELSE
			PRINTLN("[LM][RCC MISSION] - [Audio] - [Facility Property] - temp - not in area")
		ENDIF
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(tdMissionBodyScannerTimer)
		IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdMissionBodyScannerTimer, 3500)
			RESET_NET_TIMER(tdMissionBodyScannerTimer)
		ENDIF
	ENDIF
ENDPROC

PROC CLEAN_UP_SERVER_FARM_ALARM_SOUND()
	IF iSoundIDServerFarmAlarm > -1
		PRINTLN("[LM][CLEAN_UP_SERVER_FARM_ALARM_SOUND] - Cleaning up Server Farm Alarm Sound - id: ", iSoundIDServerFarmAlarm)
		IF NOT HAS_SOUND_FINISHED(iSoundIDServerFarmAlarm)
			STOP_SOUND(iSoundIDServerFarmAlarm)
		ENDIF
		SCRIPT_RELEASE_SOUND_ID(iSoundIDServerFarmAlarm)
		iSoundIDServerFarmAlarm = -1
	ENDIF
ENDPROC

PROC CLEAN_UP_SUBMARINE_ALARM_SOUND()
	IF iSoundIDSubmarineAlarm > -1
		PRINTLN("[LM][CLEAN_UP_SUBMARINE_ALARM_SOUND] - Cleaning up Submarine Alarm Sound - id: ", iSoundIDSubmarineAlarm)
		IF NOT HAS_SOUND_FINISHED(iSoundIDSubmarineAlarm)
			STOP_SOUND(iSoundIDSubmarineAlarm)
		ENDIF
		SCRIPT_RELEASE_SOUND_ID(iSoundIDSubmarineAlarm)
		iSoundIDSubmarineAlarm = -1
	ENDIF
ENDPROC

PROC PROCESS_EVERY_FRAME_INTERIOR_AUDIO(INTERIOR_INSTANCE_INDEX interior, int interiorNameHash, int interiorGroupId)

	// MOUNTAIN BASE
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_ENTRANCE)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_TUNNEL)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_BASE)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_LOOP)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_01)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_02)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_03)
		
		IF NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_INTERIOR_AUDIO_MOUNTAINBASE)

			IF interior != NULL
			AND interiorGroupId = 47
				PRINTLN("[LM][RCC MISSION] - [Audio] - [MountainBase] - Player is inside the Mountain Base Interior. Creating Ambient Audio Scene.")	
				SET_BIT(iLocalBoolCheck26, LBOOL26_INTERIOR_AUDIO_MOUNTAINBASE)
				SET_AMBIENT_ZONE_LIST_STATE("dlc_xm_int_base_zones", true, false)
				START_AUDIO_SCENE("dlc_xm_mountain_base_scene")
			ENDIF
			
		ELSE
			
			IF interior = NULL
			OR interiorGroupId != 47
				PRINTLN("[LM][RCC MISSION] - [Audio] - [MountainBase] - Player is NOT inside the Mountain Base Interior. Clearing Ambient Audio Scene.")
				CLEAR_BIT(iLocalBoolCheck26, LBOOL26_INTERIOR_AUDIO_MOUNTAINBASE)
				SET_AMBIENT_ZONE_LIST_STATE("dlc_xm_int_base_zones", false, false)
				STOP_AUDIO_SCENE("dlc_xm_mountain_base_scene")
			ENDIF
			
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_IAA)
		MAINTAIN_DEFUNCT_BASE_BODY_SCANNER()
	ENDIF
	
	// FACILITY PROPERTY
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_INTERIOR_PLAYER_FACILITY_AUDIO_MIXER)
		IF NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_INTERIOR_AUDIO_FACILITY_PROPERTY)
		
			IF interior != NULL				
			AND interiorNameHash = HASH("xm_x17dlc_int_02") //AND playerInterior = GET_INTERIOR_AT_COORDS(<<344.9904, 4842.0000, -59.9995>>)
				PRINTLN("[LM][RCC MISSION] - [Audio] - [Facility Property] - Player is inside the Facility Proper. Creating Audio Mixer.")	
				SET_BIT(iLocalBoolCheck26, LBOOL26_INTERIOR_AUDIO_FACILITY_PROPERTY)
				START_AUDIO_SCENE("dlc_xm_facility_property_scene")
			ENDIF
			
		ELSE
		
			IF interior = NULL
			OR interiorNameHash != HASH("xm_x17dlc_int_02") // OR playerInterior != GET_INTERIOR_AT_COORDS(<<344.9904, 4842.0000, -59.9995>>)
				PRINTLN("[LM][RCC MISSION] - [Audio] - [Facility Property] - Player is NOT inside the Facility Proper. Clearing Audio Mixer.")
				CLEAR_BIT(iLocalBoolCheck26, LBOOL26_INTERIOR_AUDIO_FACILITY_PROPERTY)
				STOP_AUDIO_SCENE("dlc_xm_facility_property_scene")
			ENDIF
			
		ENDIF
	ENDIF
	
	// SUBMARINE
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SUB)
		
		IF NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_INTERIOR_AUDIO_SUBMARINE)
		
			IF interior != NULL
			AND interiorNameHash = HASH("xm_x17dlc_int_sub") // GET_INTERIOR_AT_COORDS(<<512.7328, 4881.1152, -63.5867>>)
				PRINTLN("[RCC MISSION] - [Audio] - [Submarine] - Player is in sub; ENABLE sub related audio.")
				SET_AMBIENT_ZONE_LIST_STATE("azl_xm_int_sub_zones", TRUE, FALSE)
				SET_STATIC_EMITTER_ENABLED("se_xm_x17dlc_int_sub_stream", TRUE)
				START_AUDIO_SCENE("dlc_xm_submarine_finale_sub_interior_scene")
				SET_BIT(iLocalBoolCheck26, LBOOL26_INTERIOR_AUDIO_SUBMARINE)
			ENDIF
		
		ELSE
		
			IF interior = NULL
			OR interiorNameHash != HASH("xm_x17dlc_int_sub") // GET_INTERIOR_AT_COORDS(<<512.7328, 4881.1152, -63.5867>>)
				PRINTLN("[RCC MISSION] - [Audio] - [Submarine] - Player is NOT in the sub; DISABLE sub related audio.")
				SET_AMBIENT_ZONE_LIST_STATE("azl_xm_int_sub_zones", FALSE, FALSE)
				SET_STATIC_EMITTER_ENABLED("se_xm_x17dlc_int_sub_stream", FALSE)
				STOP_AUDIO_SCENE("dlc_xm_submarine_finale_sub_interior_scene")
				CLEAR_BIT(iLocalBoolCheck26, LBOOL26_INTERIOR_AUDIO_SUBMARINE)
			ENDIF
		
		ENDIF

	ENDIF
	
	// Rule Enabled Sounds
	INT iTeam = MC_PlayerBD[iPartToUse].iteam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	IF iRule < FMMC_MAX_RULES
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[iRule], ciBS_RULE11_ONLY_PLAY_ALARM_ON_AGGRO_THIS_RULE)	
		OR IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_0 + iTeam)
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[iRule], ciBS_RULE11_PLAY_SERVER_FARM_ALARM)
				IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_PLAYING_SERVER_FARM_ALARM)
					IF interiorNameHash = HASH("xm_x17dlc_int_facility2")
						iSoundIDServerFarmAlarm = GET_SOUND_ID()
						PLAY_SOUND_FROM_COORD(iSoundIDServerFarmAlarm, "alarm_loop", (<<2170.4, 2921.5, -75.9>>), "dlc_xm_farm_sounds")
						SET_BIT(iLocalBoolCheck27, LBOOL27_PLAYING_SERVER_FARM_ALARM)
					ENDIF
				ELIF IS_BIT_SET(iLocalBoolCheck27, LBOOL27_PLAYING_SERVER_FARM_ALARM)
					IF interiorNameHash != HASH("xm_x17dlc_int_facility2")
						CLEAN_UP_SERVER_FARM_ALARM_SOUND()
						CLEAR_BIT(iLocalBoolCheck27, LBOOL27_PLAYING_SERVER_FARM_ALARM)
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(iLocalBoolCheck27, LBOOL27_PLAYING_SERVER_FARM_ALARM)
					CLEAN_UP_SERVER_FARM_ALARM_SOUND()
					CLEAR_BIT(iLocalBoolCheck27, LBOOL27_PLAYING_SERVER_FARM_ALARM)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[iRule], ciBS_RULE11_PLAY_SUBMARINE_ALARM)
				IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_PLAYING_SUBMARINE_ALARM)
					IF interiorNameHash = HASH("xm_x17dlc_int_sub")
					AND NOT IS_BIT_SET(iLocalBoolCheck28, LBOOL28_SUPPRESS_SUBMARINE_ALARM_FOR_CUTSCENE)
						iSoundIDSubmarineAlarm = GET_SOUND_ID()
						PLAY_SOUND_FROM_COORD(iSoundIDSubmarineAlarm, "alarm_loop", (<<514.3, 4838.5, -61.7>>), "dlc_xm_submarine_sounds")
						SET_BIT(iLocalBoolCheck27, LBOOL27_PLAYING_SUBMARINE_ALARM)
					ENDIF
				ELIF IS_BIT_SET(iLocalBoolCheck27, LBOOL27_PLAYING_SUBMARINE_ALARM)
					IF interiorNameHash != HASH("xm_x17dlc_int_sub")
					OR IS_BIT_SET(iLocalBoolCheck28, LBOOL28_SUPPRESS_SUBMARINE_ALARM_FOR_CUTSCENE)
						CLEAN_UP_SUBMARINE_ALARM_SOUND()
						CLEAR_BIT(iLocalBoolCheck27, LBOOL27_PLAYING_SUBMARINE_ALARM)
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(iLocalBoolCheck27, LBOOL27_PLAYING_SUBMARINE_ALARM)
					CLEAN_UP_SUBMARINE_ALARM_SOUND()
					CLEAR_BIT(iLocalBoolCheck27, LBOOL27_PLAYING_SUBMARINE_ALARM)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC PROCESS_CASINO_BRAWL_AUDIO()
	
	IF NOT IS_ENTITY_ALIVE(PlayerPedToUse)
		EXIT
	ENDIF
	
	VECTOR vPlayerPos = GET_ENTITY_COORDS(PlayerPedToUse)
	VECTOR vBrawlStartPos = <<1114.0, 231.7, -49.3>>
	
//	#IF IS_DEBUG_BUILD
//	IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_PLAYED_BRAWL_START_ONESHOT)
//		DRAW_DEBUG_SPHERE(vBrawlStartPos, cfCasinoBrawl_DistanceBuffer, 255, 0, 0, 100)
//	ELIF iCasinoBrawl_LoopingBrawlSndID > -1
//		DRAW_DEBUG_SPHERE(vBrawlStartPos, cfCasinoBrawl_DistanceBuffer, 0, 255, 0, 120)
//	ELSE
//		DRAW_DEBUG_SPHERE(vBrawlStartPos, cfCasinoBrawl_DistanceBuffer, 255, 255, 255, 40)
//	ENDIF
//	#ENDIF
	
	IF VDIST2(vPlayerPos, vBrawlStartPos) < cfCasinoBrawl_DistanceBuffer * cfCasinoBrawl_DistanceBuffer
		IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_PLAYED_BRAWL_START_ONESHOT)
			PLAY_SOUND_FROM_COORD(-1, "Brawl_Start_Oneshot", <<1108.0, 213.0, -48.0>>, "dlc_vw_hk_sounds")
			PRINTLN("[BRAWLAUDIO] PROCESS_CASINO_BRAWL_AUDIO - Playing brawl start oneshot!")
			SET_BIT(iLocalBoolCheck31, LBOOL31_PLAYED_BRAWL_START_ONESHOT)
			
			iCasinoBrawl_LoopingBrawlSndID = GET_SOUND_ID()
			
			IF iCasinoBrawl_LoopingBrawlSndID > -1
				PRINTLN("[BRAWLAUDIO] PROCESS_CASINO_BRAWL_AUDIO - Playing brawl looping sound!")
				PLAY_SOUND_FROM_COORD(iCasinoBrawl_LoopingBrawlSndID, "Brawl_Walla_Loop", <<1108.0, 213.0, -48.0>>, "dlc_vw_hk_sounds")
			ENDIF
		ENDIF
	ENDIF
	
	IF iCasinoBrawl_LoopingBrawlSndID > -1
		IF IS_CUTSCENE_PLAYING()
			PRINTLN("[BRAWLAUDIO] PROCESS_CASINO_BRAWL_AUDIO - Stopping the brawl loop now!")
			
			STOP_SOUND(iCasinoBrawl_LoopingBrawlSndID)
			RELEASE_SOUND_ID(iCasinoBrawl_LoopingBrawlSndID)
			iCasinoBrawl_LoopingBrawlSndID = -1
		ENDIF
	ENDIF
	
	IF IS_VALID_INTERIOR(GET_INTERIOR_FROM_ENTITY(PlayerPedToUse))
		IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_PLAYED_BRAWL_START_ONESHOT) 
			IF NOT IS_AUDIO_SCENE_ACTIVE("dlc_vw_casino_brawl_casino_interior_scene")
				START_AUDIO_SCENE("dlc_vw_casino_brawl_casino_interior_scene")
				PRINTLN("[BRAWLAUDIO] PROCESS_CASINO_BRAWL_AUDIO - Starting dlc_vw_casino_brawl_casino_interior_scene")
			ENDIF
		ELSE
			IF IS_AUDIO_SCENE_ACTIVE("dlc_vw_casino_brawl_casino_interior_scene")
				STOP_AUDIO_SCENE("dlc_vw_casino_brawl_casino_interior_scene")
				PRINTLN("[BRAWLAUDIO] PROCESS_CASINO_BRAWL_AUDIO - STOPPING dlc_vw_casino_brawl_casino_interior_scene")
			ENDIF
			
			IF NOT IS_AUDIO_SCENE_ACTIVE("dlc_vw_casino_brawl_fight_scene")
				START_AUDIO_SCENE("dlc_vw_casino_brawl_fight_scene")
				PRINTLN("[BRAWLAUDIO] PROCESS_CASINO_BRAWL_AUDIO - Starting dlc_vw_casino_brawl_fight_scene")
			ENDIF
		ENDIF
	ELSE
		IF IS_AUDIO_SCENE_ACTIVE("dlc_vw_casino_brawl_casino_interior_scene")
			STOP_AUDIO_SCENE("dlc_vw_casino_brawl_casino_interior_scene")
			PRINTLN("[BRAWLAUDIO] PROCESS_CASINO_BRAWL_AUDIO - STOPPING dlc_vw_casino_brawl_casino_interior_scene")
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("dlc_vw_casino_brawl_fight_scene")
			STOP_AUDIO_SCENE("dlc_vw_casino_brawl_fight_scene")
			PRINTLN("[BRAWLAUDIO] PROCESS_CASINO_BRAWL_AUDIO - STOPPING dlc_vw_casino_brawl_fight_scene")
		ENDIF
	ENDIF
	
	INT iTeam = MC_PlayerBD[iLocalPart].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF MC_serverBD_4.iCurrentHighestPriority[iteam] < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_ENABLE_CASINO_AMBIENT_PED_SHOCKED_DIALOGUE)
			IF VDIST2(vPlayerPos, vBrawlStartPos) < cfCasinoBrawl_DistanceBuffer * cfCasinoBrawl_DistanceBuffer
				PRINTLN("[BRAWLAUDIO][ML] PROCESS_CASINO_BRAWL_AUDIO - Within distance of the fight to play onlooker audio")
				IF g_bCasinoShockedAmbientPedDialogue = FALSE	
					PRINTLN("[BRAWLAUDIO][ML] PROCESS_CASINO_BRAWL_AUDIO - Setting g_bCasinoShockedAmbientPedDialogue to TRUE as option is enabled on this rule ", iRule)
					g_bCasinoShockedAmbientPedDialogue = TRUE
				ENDIF
			ELSE
				IF g_bCasinoShockedAmbientPedDialogue = TRUE
					PRINTLN("[BRAWLAUDIO][ML] PROCESS_CASINO_BRAWL_AUDIO - Too far away from the fight, setting g_bCasinoShockedAmbientPedDialogue to FALSE")
					g_bCasinoShockedAmbientPedDialogue = FALSE
				ENDIF
			ENDIF
		ELSE
			IF g_bCasinoShockedAmbientPedDialogue = TRUE
				PRINTLN("[BRAWLAUDIO] PROCESS_CASINO_BRAWL_AUDIO - Setting g_bCasinoShockedAmbientPedDialogue to FALSE as option is not enabled on this rule ", iRule)
				g_bCasinoShockedAmbientPedDialogue = FALSE	
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_CASINO_INTERIOR_SOUND_SCENE()
	
	INTERIOR_INSTANCE_INDEX iCurrentInteriorIndex = GET_INTERIOR_FROM_ENTITY(LocalPlayerPed)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_AmbientCasinoFloorZone)
	AND NOT IS_PHONE_EMP_CURRENTLY_ACTIVE()
		IF NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_TURNED_ON_CASINO_FLOOR_AMBIENT_ZONE)
			PRINTLN("PROCESS_CASINO_INTERIOR_SOUND_SCENE | Turning on ambient zone")
			SET_AMBIENT_ZONE_STATE("IZ_ch_dlc_casino_heist_rm_GamingFloor_01", TRUE, TRUE)
			SET_AMBIENT_ZONE_STATE("IZ_ch_dlc_casino_heist_rm_GamingFloor_02", TRUE, TRUE)
			SET_BIT(iLocalBoolCheck33, LBOOL33_TURNED_ON_CASINO_FLOOR_AMBIENT_ZONE)
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck33, LBOOL33_TURNED_ON_CASINO_FLOOR_AMBIENT_ZONE)
			PRINTLN("PROCESS_CASINO_INTERIOR_SOUND_SCENE | Temporarily disabling ambient zone")
			SET_AMBIENT_ZONE_STATE("IZ_ch_dlc_casino_heist_rm_GamingFloor_01", FALSE, TRUE)
			SET_AMBIENT_ZONE_STATE("IZ_ch_dlc_casino_heist_rm_GamingFloor_02", FALSE, TRUE)
			CLEAR_BIT(iLocalBoolCheck33, LBOOL33_TURNED_ON_CASINO_FLOOR_AMBIENT_ZONE)
		ENDIF
	ENDIF
	
	IF IS_VALID_INTERIOR(iCurrentInteriorIndex)
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_MAIN)
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_MAIN)
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE( structInteriorData.vPos, structInteriorData.sInteriorName )
			IF IS_VALID_INTERIOR(iInteriorIndex)
				IF iInteriorIndex = iCurrentInteriorIndex
					IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_H3_Casino_Interior_Scene")
						AUDIO_SCENE_TOGGLE("DLC_H3_Casino_Interior_Scene", TRUE)
						EXIT
					ELSE
						EXIT
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_TUNNEL)
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_TUNNEL)
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE( structInteriorData.vPos, structInteriorData.sInteriorName )
			IF IS_VALID_INTERIOR(iInteriorIndex)
				IF iInteriorIndex = iCurrentInteriorIndex
					IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_H3_Casino_Interior_Scene")
						AUDIO_SCENE_TOGGLE("DLC_H3_Casino_Interior_Scene", TRUE)
						EXIT
					ELSE
						EXIT
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_BACK_AREA)
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_BACK_AREA)
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE( structInteriorData.vPos, structInteriorData.sInteriorName )
			IF IS_VALID_INTERIOR(iInteriorIndex)
				IF iInteriorIndex = iCurrentInteriorIndex
					IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_H3_Casino_Interior_Scene")
						AUDIO_SCENE_TOGGLE("DLC_H3_Casino_Interior_Scene", TRUE)
						EXIT
					ELSE
						EXIT
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_HOTEL_FLOOR)
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_HOTEL_FLOOR)
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE( structInteriorData.vPos, structInteriorData.sInteriorName )
			IF IS_VALID_INTERIOR(iInteriorIndex)
				IF iInteriorIndex = iCurrentInteriorIndex
					IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_H3_Casino_Interior_Scene")
						AUDIO_SCENE_TOGGLE("DLC_H3_Casino_Interior_Scene", TRUE)
						EXIT
					ELSE
						EXIT
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_LOADING_BAY)
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_LOADING_BAY)
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE( structInteriorData.vPos, structInteriorData.sInteriorName )
			IF IS_VALID_INTERIOR(iInteriorIndex)
				IF iInteriorIndex = iCurrentInteriorIndex
					IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_H3_Casino_Interior_Scene")
						AUDIO_SCENE_TOGGLE("DLC_H3_Casino_Interior_Scene", TRUE)
						EXIT
					ELSE
						EXIT
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_VAULT)
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_VAULT)
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE( structInteriorData.vPos, structInteriorData.sInteriorName )
			IF IS_VALID_INTERIOR(iInteriorIndex)
				IF iInteriorIndex = iCurrentInteriorIndex
					IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_H3_Casino_Interior_Scene")
						AUDIO_SCENE_TOGGLE("DLC_H3_Casino_Interior_Scene", TRUE)
						EXIT
					ELSE
						EXIT
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_UTILITY_LIFT)
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_UTILITY_LIFT)
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE( structInteriorData.vPos, structInteriorData.sInteriorName )
			IF IS_VALID_INTERIOR(iInteriorIndex)
				IF iInteriorIndex = iCurrentInteriorIndex
					IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_H3_Casino_Interior_Scene")
						AUDIO_SCENE_TOGGLE("DLC_H3_Casino_Interior_Scene", TRUE)
						EXIT
					ELSE
						EXIT
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_LIFT_SHAFT)
			structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_LIFT_SHAFT)
			iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE( structInteriorData.vPos, structInteriorData.sInteriorName )
			IF IS_VALID_INTERIOR(iInteriorIndex)
				IF iInteriorIndex = iCurrentInteriorIndex
					IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_H3_Casino_Interior_Scene")
						AUDIO_SCENE_TOGGLE("DLC_H3_Casino_Interior_Scene", TRUE)
						EXIT
					ELSE
						EXIT
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("DLC_H3_Casino_Interior_Scene")
		AUDIO_SCENE_TOGGLE("DLC_H3_Casino_Interior_Scene", FALSE)
	ENDIF
ENDPROC


