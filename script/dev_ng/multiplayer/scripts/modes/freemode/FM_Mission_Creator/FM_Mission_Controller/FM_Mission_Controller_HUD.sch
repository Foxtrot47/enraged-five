
USING "FM_Mission_Controller_USING.sch"
USING "FM_Mission_Controller_Utility.sch"
USING "FM_Mission_Controller_Entities.sch"
USING "FM_Mission_Controller_ObjectiveCompletion.sch"
USING "FM_Mission_Controller_SpecialCase.sch"
USING "FM_Mission_Controller_Rules.sch"
USING "FM_Mission_Controller_Doors.sch"
USING "FM_Mission_Controller_Cutscene.sch"
USING "FM_Mission_Controller_Debug.sch"
USING "FM_Mission_Controller_Locations.sch"
USING "FM_Mission_Controller_Vehicles.sch"
USING "FM_Mission_Controller_Peds_Brain.sch"
USING "FM_Mission_Controller_Peds_Body.sch"
USING "FM_Mission_Controller_Peds.sch"
USING "FM_Mission_Controller_GangChase.sch"
USING "FM_Mission_Controller_Minigame.sch"
USING "FM_Mission_Controller_Objects.sch"
USING "FM_Mission_Controller_Props.sch"
USING "FM_Mission_Controller_Audio.sch"



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: GENERAL HUD FUNCTIONS !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: OUT OF BOUNDS TIMER LIST !
//
//************************************************************************************************************************************************************

PROC DISPLAY_OUT_OF_BOUNDS_TIMER_LIST()
	PLAYER_STATE_LIST_DATA playerStateListData
	
	PLAYER_STATE_LIST_INIT(playerStateListData)
	
	INT iParticipant
	
	FOR iParticipant = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		PARTICIPANT_INDEX participantIndex = INT_TO_PARTICIPANTINDEX(iParticipant)
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(participantIndex)
			PLAYER_INDEX playerIndex = NETWORK_GET_PLAYER_INDEX(participantIndex)
			
			INT iTeam = GET_PLAYER_TEAM(playerIndex)
			
			INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
			
			IF iTeam != -1		
			AND iRule < FMMC_MAX_RULES
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitsetFive[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE5_DRAW_OUT_OF_BOUNDS_TIMER_LIST_T1 + iTeam)
					IF NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_ANY_SPECTATOR)
					AND NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_HEIST_SPECTATOR)
						IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fRadius > 0
						OR g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fWidth > 0
							INT iOutOfBoundsTimerSet
							
							IF HAS_NET_TIMER_STARTED(MC_playerBD[iParticipant].tdBoundstimer)
								INT iTimeLimit
								
								iTimeLimit = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iBoundsPriTimer[MC_serverBD_4.iCurrentHighestPriority[iTeam]]
								
								iOutOfBoundsTimerSet = (iTimeLimit - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_playerBD[iParticipant].tdBoundstimer)) / 1000
							ELSE
								iOutOfBoundsTimerSet = -1
							ENDIF
							
							PLAYER_STATE_LIST_ADD_PLAYER(playerStateListData, iParticipant, iOutOfBoundsTimerSet)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	IF PLAYER_STATE_LIST_SORT(playerStateListData)
		INT iMaxMissionTime = GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iObjectiveTimeLimitRule[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]])
		INT iMissionTimePassed = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdObjectiveLimitTimer[MC_playerBD[iPartToUse].iTeam])
		
		INT iTimeRemaining = iMaxMissionTime - iMissionTimePassed
		
		HUD_COLOURS hcTimer = HUD_COLOUR_WHITE
		
		IF iTimeRemaining < 0
			INT iMyTeam = MC_playerBD[iPartToUse].iTeam
			INT iMyRule = MC_serverBD_4.iCurrentHighestPriority[iMyTeam]
			
			iTimeRemaining = ((GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iMyTeam].iObjectiveTimeLimitRule[iMyRule]) + GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iMyTeam].iObjectiveSuddenDeathTimeLimit[iMyRule])) - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdObjectiveLimitTimer[MC_playerBD[iPartToUse].iteam])- MC_serverBD_3.iTimerPenalty[MC_playerBD[iPartToUse].iteam])
		ENDIF
		
		IF iTimeRemaining < (30 * 1000)
			hcTimer = HUD_COLOUR_RED
		ENDIF
		
		INT iTimer = -1
		
		IF GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iObjectiveTimeLimitRule[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]]) > 0
			iTimer = CLAMP_INT( iTimeRemaining, 0, HIGHEST_INT )
		ENDIF
		
		PLAYER_STATE_LIST_DISPLAY(playerStateListData, iTimer, hcTimer, TRUE)
	ENDIF
ENDPROC

PROC PROCESS_OUT_OF_BOUNDS_TIMER_LIST()
	INT iTeam = MC_playerBD[iPartToUse].iTeam
	
	IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_FINISHED)
	AND IS_TEAM_ACTIVE(iTeam)
	AND MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
	AND (NOT g_bMissionEnding OR IS_LOCAL_PLAYER_ANY_SPECTATOR())
		IF GET_BITS_IN_RANGE(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFive[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE5_DRAW_OUT_OF_BOUNDS_TIMER_LIST_T1, ciBS_RULE5_DRAW_OUT_OF_BOUNDS_TIMER_LIST_T4) > 0
			DISPLAY_OUT_OF_BOUNDS_TIMER_LIST()
		ENDIF 
	ENDIF
ENDPROC

PROC INITIALISE_TIME_BAR_DISPLAY_STRUCT(sTimeBarDisplay& timeBarDisplay[])
	INT i
	
	REPEAT COUNT_OF(timeBarDisplay) i
		timeBarDisplay[i].iTimeBarCurrent = -1
		timeBarDisplay[i].playerIndex = INVALID_PLAYER_INDEX()
	ENDREPEAT
ENDPROC

FUNC BOOL TRADING_PLACES_HELP_CHECK()
	IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
	AND NOT IS_SCREEN_FADING_IN()
	AND NOT IS_SCREEN_FADING_OUT()
	AND IS_NET_PLAYER_OK(LocalPlayer) 
	AND NOT g_bMissionEnding 
	AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_BLOCK_MAP_AREA_TEXT_WHEN_USING_TIME_BARS()
	INT iTeam
	INT iNumPlayingPlayers
	FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
		iNumPlayingPlayers += MC_serverBD.iNumberOfPlayingPlayers[iTeam]
	ENDFOR
	
	IF iNumPlayingPlayers > ciMAX_PLAYER_BARS
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_TRADING_PLACES_TIME_BAR()
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam] < FMMC_MAX_RULES
		IF TRADING_PLACES_HELP_CHECK() AND NOT IS_PLAYER_RESPAWNING(PLAYER_ID())
			IF NOT IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesHelpLosingTeam)
				IF IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesHelpLosingTeamTrigger)
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciTRADING_PLACES_TIME_BAR_ENABLED) AND g_FMMC_STRUCT.iTradingPlacesTimeBarDuration != -1
						IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
							PRINT_HELP("GIG_HELP_8")
						ELSE
							PRINT_HELP("JUGGS_HELP_8")
						ENDIF
					ELSE
						IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
							PRINT_HELP("GIG_HELP_4")
						ELSE
							PRINT_HELP("JUGGS_HELP_4")
						ENDIF
					ENDIF
					
					SET_BIT(iTradingPlacesBitSet, ciTradingPlacesHelpLosingTeam)
				ENDIF
			ENDIF
		ENDIF
		
		IF TRADING_PLACES_HELP_CHECK() AND NOT IS_PLAYER_RESPAWNING(PLAYER_ID())
			IF NOT IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesHelpWinningTeam)
				IF IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesHelpWinningTeamTrigger)
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciTRADING_PLACES_TIME_BAR_ENABLED) AND g_FMMC_STRUCT.iTradingPlacesTimeBarDuration != -1
						IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
							PRINT_HELP("GIG_HELP_7")
						ELSE
							PRINT_HELP("JUGGS_HELP_7")
						ENDIF
					ELSE
						IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
							PRINT_HELP("GIG_HELP_3")
						ELSE
							PRINT_HELP("JUGGS_HELP_3")
						ENDIF
					ENDIF
					
					SET_BIT(iTradingPlacesBitSet, ciTradingPlacesHelpWinningTeam)
				ENDIF
			ENDIF
		ENDIF
		
		IF TRADING_PLACES_HELP_CHECK() AND NOT IS_PLAYER_RESPAWNING(PLAYER_ID())
			IF IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesHelpSuicideTrigger)
				IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
					IF MC_playerBD[iLocalPart].iTeam = 1 //Winners
						PRINT_HELP("GIG_HELP_5")
					ENDIF
				ELSE
					PRINT_HELP("JUGGS_HELP_5")
				ENDIF
				
				CLEAR_BIT(iTradingPlacesBitSet, ciTradingPlacesHelpSuicideTrigger)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciTRADING_PLACES_TIME_BAR_ENABLED) AND g_FMMC_STRUCT.iTradingPlacesTimeBarDuration != -1
			IF NOT g_bMissionEnding
				IF MC_playerBD[iLocalPart].iTeam != -1
					IF iTradingPlacesTimeBarLastTeam != MC_playerBD[iLocalPart].iTeam
						IF IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesTimeBarSet)
							IF MC_playerBD[iLocalPart].iTeam = 0 //Losers
								MC_playerBD[iLocalPart].iTradingPlacesTimeBarCurrent = CLAMP_INT(MC_playerBD[iLocalPart].iTradingPlacesTimeBarCurrent + GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MC_playerBD[iLocalPart].iTradingPlacesTimeBarTimer.Timer), 0, (g_FMMC_STRUCT.iTradingPlacesTimeBarDuration * 1000))
							ELSE
								MC_playerBD[iLocalPart].iTradingPlacesTimeBarCurrent = CLAMP_INT(MC_playerBD[iLocalPart].iTradingPlacesTimeBarCurrent - GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MC_playerBD[iLocalPart].iTradingPlacesTimeBarTimer.Timer), 0, (g_FMMC_STRUCT.iTradingPlacesTimeBarDuration * 1000))
							ENDIF
						ELSE
							MC_playerBD[iLocalPart].iTradingPlacesTimeBarCurrent = 0
						ENDIF
						
						SET_BIT(iTradingPlacesBitSet, ciTradingPlacesTimeBarSet)
						
						
						IF NOT HAS_NET_TIMER_STARTED(MC_playerBD[iLocalPart].iTradingPlacesTimeBarTimer)
							START_NET_TIMER(MC_playerBD[iLocalPart].iTradingPlacesTimeBarTimer)
						ELSE
							REINIT_NET_TIMER(MC_playerBD[iLocalPart].iTradingPlacesTimeBarTimer)
						ENDIF
						
						iTradingPlacesTimeBarLastTeam = MC_playerBD[iLocalPart].iTeam
						
						IF NOT IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesFirstTeamSwapHelp)
							SET_BIT(iTradingPlacesBitSet, ciTradingPlacesFirstTeamSwapHelp)
						ELSE
							IF MC_playerBD[iLocalPart].iTeam = 1	//Winning Team Help
								IF NOT IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesHelpWinningTeam)
									SET_BIT(iTradingPlacesBitSet, ciTradingPlacesHelpWinningTeamTrigger)
								ENDIF
								
								IF NOT IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesHelpLosingTeam)
									CLEAR_BIT(iTradingPlacesBitSet, ciTradingPlacesHelpLosingTeamTrigger)
								ENDIF
							ENDIF
							
							IF MC_playerBD[iLocalPart].iTeam = 0	//Losing Team Help
								IF NOT IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesHelpLosingTeam)
									SET_BIT(iTradingPlacesBitSet, ciTradingPlacesHelpLosingTeamTrigger)
								ENDIF
								
								IF NOT IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesHelpWinningTeam)
									CLEAR_BIT(iTradingPlacesBitSet, ciTradingPlacesHelpWinningTeamTrigger)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT bLocalPlayerPedOk
						IF HAS_NET_TIMER_STARTED(MC_playerBD[iLocalPart].iTradingPlacesTimeBarTimer)
							RESET_NET_TIMER(MC_playerBD[iLocalPart].iTradingPlacesTimeBarTimer)
						ENDIF
					ENDIF
					
					//Start Help
					IF TRADING_PLACES_HELP_CHECK()
						IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TRADING_PLACES_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
						AND NOT IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesHelpRemixAbilities)
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitset2, ciBS2_JUGGERNAUTS)
								PRINT_HELP("BEAST_HELP_04")
								PRINTLN("[RCC MISSION] Trading Places Remix Juggernaut control help shown")
							ELSE
								PRINT_HELP("TPBJ_HELP_1")
								PRINTLN("[RCC MISSION] Trading Places Remix Beast control help shown")
							ENDIF
							PRINTLN("[RCC MISSION] Trading Places help text part 1 shown")
							SET_BIT(iTradingPlacesBitSet, ciTradingPlacesHelpRemixAbilities)
						ELIF NOT IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesHelpStartPart1)
							IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
								PRINT_HELP("GIG_HELP_6")
							ELSE
								PRINT_HELP("JUGGS_HELP_6")
							ENDIF
							PRINTLN("[RCC MISSION] Trading Places help text part 2 shown")
							SET_BIT(iTradingPlacesBitSet, ciTradingPlacesHelpStartPart1)
						ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TRADING_PLACES_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
						AND NOT IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesHelpRemixKnockBack)
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitset2, ciBS2_JUGGERNAUTS)
								PRINT_HELP("TPBJ_HELP_2")
							ENDIF
							PRINTLN("[RCC MISSION] Trading Places help text part 3 shown")
							SET_BIT(iTradingPlacesBitSet, ciTradingPlacesHelpRemixKnockBack)
						ELIF NOT IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesHelpStartPart2)
							IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
								PRINT_HELP("GIG_HELP_2")
							ELSE
								PRINT_HELP("JUGGS_HELP_2")
							ENDIF
							PRINTLN("[RCC MISSION] Trading Places help text part 4 shown")
							SET_BIT(iTradingPlacesBitSet, ciTradingPlacesHelpStartPart2)
						ELIF NOT IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesHelpStartPart3)
							IF (MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1]) = 2	//2 Players
								IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
									PRINT_HELP("GIG_HELP_10")
								ELSE
									PRINT_HELP("JUGGS_HELP_10")
								ENDIF
							ELSE
								IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
									PRINT_HELP("JUGGS_HELP_9")
								ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TRADING_PLACES_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
									PRINT_HELP("GIG_HELP_9b")
								ELSE
									PRINT_HELP("GIG_HELP_9")
								ENDIF
							ENDIF
							PRINTLN("[RCC MISSION] Trading Places help text part 5 shown")
							SET_BIT(iTradingPlacesBitSet, ciTradingPlacesHelpStartPart3)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciCLOBBER_PLAYER_ON_TEAM_SWAP)
				IF NOT g_bMissionEnding
					IF MC_playerBD[iLocalPart].iTeam != -1
						IF iTradingPlacesTimeBarLastTeam != MC_playerBD[iLocalPart].iTeam
							iTradingPlacesTimeBarLastTeam = MC_playerBD[iLocalPart].iTeam
							
							IF NOT IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesFirstTeamSwapHelp)
								SET_BIT(iTradingPlacesBitSet, ciTradingPlacesFirstTeamSwapHelp)
							ELSE
								IF MC_playerBD[iLocalPart].iTeam = 1	//Winning Team Help
									IF NOT IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesHelpWinningTeam)
										SET_BIT(iTradingPlacesBitSet, ciTradingPlacesHelpWinningTeamTrigger)
									ENDIF
									
									IF NOT IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesHelpLosingTeam)
										CLEAR_BIT(iTradingPlacesBitSet, ciTradingPlacesHelpLosingTeamTrigger)
									ENDIF
								ENDIF
								
								IF MC_playerBD[iLocalPart].iTeam = 0	//Losing Team Help
									IF NOT IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesHelpLosingTeam)
										SET_BIT(iTradingPlacesBitSet, ciTradingPlacesHelpLosingTeamTrigger)
									ENDIF
									
									IF NOT IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesHelpWinningTeam)
										CLEAR_BIT(iTradingPlacesBitSet, ciTradingPlacesHelpWinningTeamTrigger)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						//Start Help
						IF TRADING_PLACES_HELP_CHECK()
							IF NOT IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesHelpStartPart1)
								IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
									PRINT_HELP("GIG_HELP_1")
								ELSE
									PRINT_HELP("JUGGS_HELP_1")
								ENDIF
								PRINTLN("[RCC MISSION] START HELP Trading Places help text part 1 shown")
								SET_BIT(iTradingPlacesBitSet, ciTradingPlacesHelpStartPart1)
							ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TRADING_PLACES_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
							AND NOT IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesHelpRemixKnockBack)
								IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitset2, ciBS2_JUGGERNAUTS)
									PRINT_HELP("TPBJ_HELP_2")
								ENDIF
								PRINTLN("[RCC MISSION] START HELP Trading Places help text part 2 shown")
								SET_BIT(iTradingPlacesBitSet, ciTradingPlacesHelpRemixKnockBack)
							ELIF NOT IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesHelpStartPart2)
								IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
									PRINT_HELP("GIG_HELP_2")
								ELSE
									PRINT_HELP("JUGGS_HELP_2")
								ENDIF
								PRINTLN("[RCC MISSION] START HELP Trading Places help text part 3 shown")
								SET_BIT(iTradingPlacesBitSet, ciTradingPlacesHelpStartPart2)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_TRADING_PLACES_TIME_BAR_HUD(BOOL bFinalSort = FALSE)
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciTRADING_PLACES_TIME_BAR_ENABLED) AND g_FMMC_STRUCT.iTradingPlacesTimeBarDuration != -1 AND IS_BIT_SET(MC_serverbd.iServerBitSet3, SBBOOL3_TRADING_PLACES_TIMER_BAR_HUD_MODE)
	
		//Blocks the map area text when there are more than 6 players in the mode and the player is out of bounds
		IF SHOULD_BLOCK_MAP_AREA_TEXT_WHEN_USING_TIME_BARS()
			PRINTLN("[KH] PROCESS_TRADING_PLACES_TIME_BAR_HUD - Blocking map area text as bounds timer is showing")
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
		ENDIF
		
		INT iLocalTeam = MC_playerBD[iPartToUse].iTeam
		
		IF iLocalTeam >= 0 AND iLocalTeam < FMMC_MAX_TEAMS
			sTimeBarDisplay sTimeBarData[MAX_NUM_MC_PLAYERS]
			
			INITIALISE_TIME_BAR_DISPLAY_STRUCT(sTimeBarData)
			
			INT iNumberOfActivePlayers
			
			INT iNumberOfWinningPlayers
			
			INT iParticipant
			
			//Gather Player Data
			FOR iParticipant = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
				PARTICIPANT_INDEX participantIndex = INT_TO_PARTICIPANTINDEX(iParticipant)
				
				IF NETWORK_IS_PARTICIPANT_ACTIVE(participantIndex) AND NOT IS_PARTICIPANT_A_SPECTATOR(iParticipant)
					PLAYER_INDEX playerIndex = NETWORK_GET_PLAYER_INDEX(participantIndex)
					
					INT iTeam = MC_playerBD[iParticipant].iTeam
					
					IF iTeam >= 0 AND iTeam < FMMC_MAX_TEAMS
						IF iTeam = 0 //Losers
							sTimeBarData[iNumberOfActivePlayers].iTimeBarCurrent = CLAMP_INT(MC_playerBD[iParticipant].iTradingPlacesTimeBarCurrent - GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MC_playerBD[iParticipant].iTradingPlacesTimeBarTimer.Timer), 0, (g_FMMC_STRUCT.iTradingPlacesTimeBarDuration * 1000))
						ELSE
							sTimeBarData[iNumberOfActivePlayers].iTimeBarCurrent = CLAMP_INT(MC_playerBD[iParticipant].iTradingPlacesTimeBarCurrent + GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MC_playerBD[iParticipant].iTradingPlacesTimeBarTimer.Timer), 0, (g_FMMC_STRUCT.iTradingPlacesTimeBarDuration * 1000))
						ENDIF
						
						sTimeBarData[iNumberOfActivePlayers].playerIndex = playerIndex
						
						iNumberOfActivePlayers++
						
						IF iTeam = 1
							iNumberOfWinningPlayers++
						ENDIF
						
						IF sTimeBarData[iParticipant].playerIndex != INVALID_PLAYER_INDEX()
							PRINTLN("[PROCESS_TRADING_PLACES_TIME_BAR_HUD] - iTeam: ", MC_playerBD[iParticipant].iTeam, " iPart: ", iParticipant, " iTimeBarCurrent: ", sTimeBarData[iNumberOfActivePlayers].iTimeBarCurrent, " Player Name: ", GET_PLAYER_NAME(sTimeBarData[iParticipant].playerIndex))
						ENDIF
						
						PRINTLN("[PROCESS_TRADING_PLACES_TIME_BAR_HUD] - iNumberOfActivePlayers: ", iNumberOfActivePlayers, " iNumberOfWinningPlayers: ", iNumberOfWinningPlayers)
					ENDIF
				ENDIF
			ENDFOR
			
			//Sort...
			IF iNumberOfActivePlayers > 0
				INT iTotalPlayerHUD = 8	//Full Display
				
				IF iNumberOfActivePlayersLast != iNumberOfActivePlayers
					iWinningPlayerHUD = CLAMP_INT(iNumberOfWinningPlayers, 1, 5)
					
					iNumberOfActivePlayersLast = iNumberOfActivePlayers
				ENDIF
				
				CLEAR_BIT(iTradingPlacesBitSet, ciTradingPlacesWinningPlayerFound)
				
				CONTROL_ACTION caDisplayToggle = INPUT_CONTEXT
				
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TRADING_PLACES_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
					caDisplayToggle = INPUT_ENTER
				ENDIF
				
				IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
					IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, caDisplayToggle) OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, caDisplayToggle))
					AND NOT IS_PAUSE_MENU_ACTIVE()
						IF IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesDisplayToggle)
							CLEAR_BIT(iTradingPlacesBitSet, ciTradingPlacesDisplayToggle)
						ELSE
							SET_BIT(iTradingPlacesBitSet, ciTradingPlacesDisplayToggle)
						ENDIF
					ENDIF
				ENDIF
				
				INT i, ii
				
				INITIALISE_TIME_BAR_DISPLAY_STRUCT(sTimeBarSorted)
				
				BOOL bTradingPlacesPlayerToUseFound
				
				FOR i = 0 TO (iTotalPlayerHUD - 1)
					INT iHighest = -1
					
					FOR ii = 0 TO (iNumberOfActivePlayers - 1)
						IF sTimeBarData[ii].iTimeBarCurrent != -1
							IF sTimeBarData[ii].iTimeBarCurrent > sTimeBarSorted[i].iTimeBarCurrent
							OR (sTimeBarData[ii].iTimeBarCurrent >= sTimeBarSorted[i].iTimeBarCurrent AND sTimeBarData[ii].playerIndex = PlayerToUse)
								sTimeBarSorted[i].iTimeBarCurrent = sTimeBarData[ii].iTimeBarCurrent
								sTimeBarSorted[i].playerIndex = sTimeBarData[ii].playerIndex
								
								iHighest = ii
								
								IF sTimeBarData[ii].playerIndex != INVALID_PLAYER_INDEX()
									PRINTLN("[PROCESS_TRADING_PLACES_TIME_BAR_HUD] - sorted - ii: ", ii, " iTimeBarCurrent: ", sTimeBarData[ii].iTimeBarCurrent, " PlayerName: ", GET_PLAYER_NAME(sTimeBarData[ii].playerIndex))
								ELSE
									PRINTLN("[PROCESS_TRADING_PLACES_TIME_BAR_HUD] - sorted - ii: ", ii, " PlayerName: Invalid.")
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
					
					IF iHighest != -1
						IF sTimeBarData[iHighest].playerIndex = PlayerToUse 
							bTradingPlacesPlayerToUseFound = TRUE
							
							IF i < iWinningPlayerHUD	//Player is Winning
								SET_BIT(iTradingPlacesBitSet, ciTradingPlacesWinningPlayerFound)
							ENDIF
						ENDIF
						
						sTimeBarData[iHighest].iTimeBarCurrent = -1
						sTimeBarData[iHighest].playerIndex = INVALID_PLAYER_INDEX()
					ENDIF
				ENDFOR
				
				//Place the player in the final slot if they're not found...
				IF NOT bTradingPlacesPlayerToUseFound
					IF iLocalTeam = 0 //Losers
						sTimeBarSorted[(iTotalPlayerHUD - 1)].iTimeBarCurrent = CLAMP_INT(MC_playerBD[iPartToUse].iTradingPlacesTimeBarCurrent - GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MC_playerBD[iPartToUse].iTradingPlacesTimeBarTimer.Timer), 0, (g_FMMC_STRUCT.iTradingPlacesTimeBarDuration * 1000))
					ELSE
						sTimeBarSorted[(iTotalPlayerHUD - 1)].iTimeBarCurrent = CLAMP_INT(MC_playerBD[iPartToUse].iTradingPlacesTimeBarCurrent + GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MC_playerBD[iPartToUse].iTradingPlacesTimeBarTimer.Timer), 0, (g_FMMC_STRUCT.iTradingPlacesTimeBarDuration * 1000))
					ENDIF
					sTimeBarSorted[(iTotalPlayerHUD - 1)].playerIndex = PlayerToUse
				ENDIF
				
				sTimeBarDisplay sTimeBarCondensed[2]
				
				INITIALISE_TIME_BAR_DISPLAY_STRUCT(sTimeBarCondensed)
				
				//Condensed View (Lowest Winner then Your Name, or Your Name then Highest Loser)
				IF IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesDisplayToggle)
					INT iTimeBarCurrent
					
					IF iLocalTeam = 0 //Losers
						iTimeBarCurrent = CLAMP_INT(MC_playerBD[iPartToUse].iTradingPlacesTimeBarCurrent - GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MC_playerBD[iPartToUse].iTradingPlacesTimeBarTimer.Timer), 0, (g_FMMC_STRUCT.iTradingPlacesTimeBarDuration * 1000))
					ELSE
						iTimeBarCurrent = CLAMP_INT(MC_playerBD[iPartToUse].iTradingPlacesTimeBarCurrent + GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MC_playerBD[iPartToUse].iTradingPlacesTimeBarTimer.Timer), 0, (g_FMMC_STRUCT.iTradingPlacesTimeBarDuration * 1000))
					ENDIF
					
					IF IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesWinningPlayerFound)	//Your Name then Highest Loser
						sTimeBarCondensed[0].iTimeBarCurrent = iTimeBarCurrent
						sTimeBarCondensed[0].playerIndex = PlayerToUse
						
						sTimeBarCondensed[1].iTimeBarCurrent = sTimeBarSorted[iWinningPlayerHUD].iTimeBarCurrent
						sTimeBarCondensed[1].playerIndex = sTimeBarSorted[iWinningPlayerHUD].playerIndex
					ELSE	//Lowest Winner then Your Name
						sTimeBarCondensed[0].iTimeBarCurrent = sTimeBarSorted[iWinningPlayerHUD - 1].iTimeBarCurrent
						sTimeBarCondensed[0].playerIndex = sTimeBarSorted[iWinningPlayerHUD - 1].playerIndex
						
						sTimeBarCondensed[1].iTimeBarCurrent = iTimeBarCurrent
						sTimeBarCondensed[1].playerIndex = PlayerToUse
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesDisplayToggle)
					iTotalPlayerHUD = 2
				ENDIF
				
				//Display
				IF NOT bFinalSort
					FOR i = 0 TO (iTotalPlayerHUD - 1)
						HUD_COLOURS hudColour = HUD_COLOUR_WHITE
						HUD_COLOURS titleColour = HUD_COLOUR_WHITE
						HUDFLASHING hudFlash = HUDFLASHING_NONE
						INT iColourFlash = 0
						STRING sPlayerName
						
						PLAYER_INDEX playerIndexCurrent = INVALID_PLAYER_INDEX()
						
						IF IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesDisplayToggle)
							IF sTimeBarCondensed[i].playerIndex != INVALID_PLAYER_INDEX()
								playerIndexCurrent = sTimeBarCondensed[i].playerIndex
							ENDIF
						ELSE
							IF sTimeBarSorted[i].playerIndex != INVALID_PLAYER_INDEX()
								playerIndexCurrent = sTimeBarSorted[i].playerIndex
							ENDIF
						ENDIF
						
						INT iTeam = -1
						
						IF playerIndexCurrent != INVALID_PLAYER_INDEX()
							iTeam = GET_PLAYER_TEAM(playerIndexCurrent)
							
							IF iTeam >= 0 AND iTeam < FMMC_MAX_TEAMS
								IF g_FMMC_STRUCT.iTeamColourOverride[iLocalTeam] != -1
								AND g_FMMC_STRUCT.iTeamColourOverride[iLocalTeam] <= FMMC_MAX_TEAMS
									hudColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam, playerIndexCurrent)
									
									titleColour = hudColour
								ELIF iTeam = 1
									hudColour = HUD_COLOUR_GOLD
								ENDIF
							ENDIF
							
							IF playerIndexCurrent = PlayerToUse
								IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
									sPlayerName = GET_PLAYER_NAME(PlayerToUse)
								ELSE
									sPlayerName = GET_FILENAME_FOR_AUDIO_CONVERSATION("EXEC_TRD_YS")
								ENDIF
								
								IF g_FMMC_STRUCT.iTeamColourOverride[iLocalTeam] = -1
									titleColour = HUD_COLOUR_FRIENDLY		//HUD_COLOUR_BLUE
								ENDIF
							ELSE
								sPlayerName = GET_PLAYER_NAME(playerIndexCurrent)
							ENDIF
							
							IF IS_NET_PLAYER_OK(PlayerToUse)
								IF playerIndexCurrent = PlayerToUse
									IF IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesWinningPlayerFoundLast) != IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesWinningPlayerFound)
										IF IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesWinningPlayerFound)
											SET_BIT(iTradingPlacesBitSet, ciTradingPlacesWinningPlayerFoundLast)
										ELSE
											CLEAR_BIT(iTradingPlacesBitSet, ciTradingPlacesWinningPlayerFoundLast)
										ENDIF
									ELSE
										hudFlash = HUDFLASHING_FLASHWHITE
										iColourFlash = 5000
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						BOOL bDividerLine = FALSE
						
						IF IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesDisplayToggle)
							IF i = 0	//First Player... (There are only two when condensed)
								bDividerLine = TRUE
							ENDIF
						ELSE
							IF i = iWinningPlayerHUD - 1	//Lowest Sorted Winning Player
								bDividerLine = TRUE
							ENDIF
						ENDIF
						
						IF IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesDisplayToggle)
							IF sTimeBarCondensed[i].iTimeBarCurrent != -1
								DRAW_GENERIC_METER(sTimeBarCondensed[i].iTimeBarCurrent, g_FMMC_STRUCT.iTradingPlacesTimeBarDuration * 1000, sPlayerName, hudColour, iColourFlash, HUDORDER_NINETHBOTTOM - INT_TO_ENUM(HUDORDER, i), -1, -1, TRUE, TRUE, hudFlash, iColourFlash, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, titleColour, bDividerLine)
							ENDIF
						ELSE
							IF sTimeBarSorted[i].iTimeBarCurrent != -1
								DRAW_GENERIC_METER(sTimeBarSorted[i].iTimeBarCurrent, g_FMMC_STRUCT.iTradingPlacesTimeBarDuration * 1000, sPlayerName, hudColour, iColourFlash, HUDORDER_NINETHBOTTOM - INT_TO_ENUM(HUDORDER, i), -1, -1, TRUE, TRUE, hudFlash, iColourFlash, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, titleColour, bDividerLine)
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: TOUR DE FORCE !
//
//************************************************************************************************************************************************************

PROC DISPLAY_TOUR_DE_FORCE_HUD()
	#IF IS_DEBUG_BUILD
		PRINTLN("DISPLAY_TOUR_DE_FORCE_HUD - - - - - - - - - -")	
	#ENDIF
ENDPROC

PROC PROCESS_TOUR_DE_FORCE_HUD()
	INT iTeam = MC_playerBD[iPartToUse].iTeam
	
	IF NOT IS_BIT_SET( MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_FINISHED )
	AND IS_TEAM_ACTIVE( iTeam )
	AND MC_serverBD_4.iCurrentHighestPriority[ iTeam ] < FMMC_MAX_RULES
	AND (NOT g_bMissionEnding OR IS_LOCAL_PLAYER_ANY_SPECTATOR() )
		
		DISPLAY_TOUR_DE_FORCE_HUD()
	ENDIF
ENDPROC


//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: VEHICLE VENDETTA !
//
//************************************************************************************************************************************************************

PROC DISPLAY_VEHICLE_HEALTH_HUD(VEHICLE_INDEX vehOverride = NULL, BOOL bForceGreenWhenNotMax = FALSE)
	
	INT iVehicleHealth
	INT iMaxHealth = 100
	INT iMinHealthRed = 25
	INT	iVeh = -1
	//check if in a vehicle 
	IF NOT IS_PED_INJURED(PlayerPedToUse)
	AND NOT IS_BIT_SET(iLocalBoolResetCheck1, LBOOL_RESET1_SHOWING_VEH_HEALTH_BARS)
	AND NOT IS_ON_SCREEN_WHEEL_SPIN_MINIGAME_ACTIVE()
		IF IS_PED_IN_ANY_VEHICLE(PlayerPedToUse)
		OR DOES_ENTITY_EXIST(vehOverride)
		
			INT iTeam = MC_playerBD[iPartToUse].iteam
			VEHICLE_INDEX vehIndex 
			
			IF NOT DOES_ENTITY_EXIST(vehOverride)
				vehIndex = GET_VEHICLE_PED_IS_IN(PlayerPedToUse,FALSE)
				IF IS_ENTITY_ALIVE(vehIndex)
					iVeh = IS_ENTITY_A_MISSION_CREATOR_ENTITY(vehIndex)
					IF iVeh > -1
						iVehicleHealth = FLOOR(GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(vehIndex, iVeh, MC_serverBD.iTotalNumStartingPlayers,IS_VEHICLE_A_TRAILER(vehIndex)))
					ELSE
						iVehicleHealth = ROUND(GET_VEHICLE_BODY_HEALTH(vehIndex))
						iMaxHealth = ROUND(GET_FMMC_PLAYER_VEHICLE_BODY_HEALTH_FOR_TEAM(iTeam))
						PRINTLN("(1) [DISPLAY_VEHICLE_HEALTH_HUD] iVehicleHealth: ",iVehicleHealth," iMaxHealth: ",iMaxHealth)	
					ENDIF
				ENDIF
				
				IF GET_ENTITY_MODEL(vehIndex) = RCBANDITO		
					PRINTLN("[DISPLAY_VEHICLE_HEALTH_HUD] Inside Bandito, exitting... ")	
					EXIT
				ENDIF
			ELSE
				vehIndex = vehOverride
				IF IS_ENTITY_ALIVE(vehIndex)
					iVehicleHealth = ROUND(GET_VEHICLE_BODY_HEALTH(vehIndex))
					iMaxHealth = ROUND(GET_FMMC_PLAYER_VEHICLE_BODY_HEALTH_FOR_TEAM(iTeam))
					PRINTLN("(2) [DISPLAY_VEHICLE_HEALTH_HUD] iVehicleHealth: ",iVehicleHealth," iMaxHealth: ",iMaxHealth)	
				ENDIF
			ENDIF
					
			IF DOES_ENTITY_EXIST(vehIndex)
			AND IS_ENTITY_ALIVE(vehIndex)
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA(g_FMMC_STRUCT.iAdversaryModeType)
					iVehicleHealth = GET_ENTITY_HEALTH(vehIndex)
					iMaxHealth = 1000
					iMinHealthRed = 250
				ENDIF
				
				HUD_COLOURS hudColour = HUD_COLOUR_WHITE
				
				IF iVehicleHealth < iMinHealthRed
					hudColour = HUD_COLOUR_RED
				ENDIF
				
				IF bForceGreenWhenNotMax
					FLOAT fTotal = GET_FMMC_PLAYER_VEHICLE_BODY_HEALTH_FOR_TEAM(iTeam)
					FLOAT fCappedTotal = (fTotal * (TO_FLOAT(iRegenCapFromBounds)/100))
					FLOAT fVehicleCurrentBodyHP = GET_VEHICLE_BODY_HEALTH(vehIndex)
					IF fVehicleCurrentBodyHP < fCappedTotal
						hudColour = HUD_COLOUR_GREEN
					ENDIF
				ENDIF
				HUDORDER eHUDOrder = HUDORDER_FIFTHBOTTOM
				IF CONTENT_IS_USING_ARENA()
					eHUDOrder = HUDORDER_EIGHTHBOTTOM
				ENDIF
				DRAW_GENERIC_METER(iVehicleHealth, iMaxHealth, "VEH_HEALTH", hudColour, -1, eHUDOrder, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, FALSE)
				#IF IS_DEBUG_BUILD
					PRINTLN("(3) [DISPLAY_VEHICLE_HEALTH_HUD] iVehicleHealth: ",iVehicleHealth," iMaxHealth: ",iMaxHealth)						
				#ENDIF
				
				SET_BIT(iLocalBoolResetCheck1, LBOOL_RESET1_SHOWING_VEH_HEALTH_BARS)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_CACHED_SCORE_HUD()

	INT iRule	
	TEXT_LABEL_15 tl15	
	INT iTeam = 0	
	
	FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
		iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
		IF iRule < FMMC_MAX_RULES
			IF IS_TEAM_ACTIVE(iTeam)
				HUD_COLOURS hudColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam, PlayerToUse)
				
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciBOMBUSHKA_GIVE_POINT_TO_WINNER)
					tl15 = "MC_HGHSCR_"
					tl15 += iTeam
					
					DRAW_GENERIC_BIG_NUMBER(MC_ServerBD.iCachedBestRuleScore[iTeam], tl15, -1, hudColour, INT_TO_ENUM(HUDORDER, ENUM_TO_INT(HUDORDER_THIRDBOTTOM) - iTeam), DEFAULT, DEFAULT, hudColour)
				ELSE
					tl15 = "MC_HGHPNT_"
					tl15 += iTeam
					
					DRAW_GENERIC_BIG_NUMBER(MC_ServerBD.iCachedRuleScore[iTeam], tl15, -1, hudColour, INT_TO_ENUM(HUDORDER, ENUM_TO_INT(HUDORDER_THIRDBOTTOM) - iTeam), DEFAULT, DEFAULT, hudColour)
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC PROCESS_VEHICLE_HEALTH_HUD()
	INT iTeam = MC_playerBD[iPartToUse].iTeam
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_VEHICLE_VEHICLE_HEALTH_HUD)
		IF NOT IS_BIT_SET( MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_FINISHED )
		AND IS_TEAM_ACTIVE( iTeam )
		AND MC_serverBD_4.iCurrentHighestPriority[ iTeam ] < FMMC_MAX_RULES
		AND NOT g_bMissionEnding
		AND NOT (IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE() AND DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(LocalPlayer) AND IS_PLAYER_SPECTATOR_ONLY(LocalPlayer))
		AND NOT IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Using_RC_CAR)
			DISPLAY_VEHICLE_HEALTH_HUD()
		ENDIF
	ENDIF
ENDPROC

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: CROSS THE LINE !
//
//************************************************************************************************************************************************************


PROC DISPLAY_CROSS_THE_LINE_HUD()
	#IF IS_DEBUG_BUILD	IF bDebugCrossTheLineHUDPrints	PRINTLN("DISPLAY_CROSS_THE_LINE_HUD - - - - - - - - - -")	ENDIF	#ENDIF
	INT iMaxMissionTime = GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iObjectiveTimeLimitRule[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]])
	INT iMissionTimePassed = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdObjectiveLimitTimer[MC_playerBD[iPartToUse].iTeam])
	
	INT iTimeRemaining = iMaxMissionTime - iMissionTimePassed
	
	HUD_COLOURS hcTimer = HUD_COLOUR_WHITE
	
	IF iTimeRemaining < 0
		INT iMyTeam = MC_playerBD[iPartToUse].iTeam
		INT iMyRule = MC_serverBD_4.iCurrentHighestPriority[iMyTeam]
		
		iTimeRemaining = ((GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iMyTeam].iObjectiveTimeLimitRule[iMyRule]) + GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iMyTeam].iObjectiveSuddenDeathTimeLimit[iMyRule])) - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdObjectiveLimitTimer[MC_playerBD[iPartToUse].iTeam])- MC_serverBD_3.iTimerPenalty[iMyTeam])
	ENDIF
	
	IF iTimeRemaining < (30 * 1000)
		hcTimer = HUD_COLOUR_RED
	ENDIF
	
	INT iTimer = -1
	
	IF GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iObjectiveTimeLimitRule[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]]) > 0
		iTimer = CLAMP_INT( iTimeRemaining, 0, HIGHEST_INT )
	ENDIF
	
	IF NOT MPGlobalsHud.b_HideCrossTheLineUI OR IS_LOCAL_PLAYER_ANY_SPECTATOR()
		IF PLAYER_STATE_LIST_SORT(playerStateListDataCrossTheLine)
			PLAYER_STATE_LIST_DISPLAY(playerStateListDataCrossTheLine, iTimer, hcTimer)
		ENDIF
		
		#IF IS_DEBUG_BUILD	IF bDebugCrossTheLineHUDPrints
		INT i
		
		FOR i = 0 TO COUNT_OF(playerStateListDataCrossTheLine.uiSlot) - 1
			IF NOT IS_STRING_NULL_OR_EMPTY(playerStateListDataCrossTheLine.uiSlot[i].sName)
				PRINTLN("DISPLAY_CROSS_THE_LINE_HUD - uiSlot[", i, "].sName = ", playerStateListDataCrossTheLine.uiSlot[i].sName, "   uiSlot[", i, "].iValue = ", playerStateListDataCrossTheLine.uiSlot[i].iValue)
			ELSE
				PRINTLN("DISPLAY_CROSS_THE_LINE_HUD - uiSlot[", i, "].sName = NULL / EMPTY")
			ENDIF
		ENDFOR
		ENDIF	#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD	IF bDebugCrossTheLineHUDPrints	PRINTLN("DISPLAY_CROSS_THE_LINE_HUD - MPGlobalsHud.b_HideCrossTheLineUI")	ENDIF	#ENDIF
		PLAYER_STATE_LIST_DISPLAY(playerStateListDataCrossTheLine, iTimer, hcTimer)
	ENDIF
ENDPROC

PROC TOGGLE_CROSS_THE_LINE_HUD()
	IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_ENTER) AND NOT IS_PAUSE_MENU_ACTIVE()
			IF MPGlobalsHud.b_HideCrossTheLineUI = FALSE
				MPGlobalsHud.b_HideCrossTheLineUI = TRUE
			ELSE
				MPGlobalsHud.b_HideCrossTheLineUI = FALSE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_CROSS_THE_LINE_HUD()
	IF IS_BIT_SET(iLocalBoolCheck13, LBOOL13_DRAW_DISTANCE_TO_CROSS_LINE)
		INT iTeam = MC_playerBD[iPartToUse].iTeam
		
		IF NOT IS_BIT_SET( MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_FINISHED )
		AND IS_TEAM_ACTIVE( iTeam )
		AND MC_serverBD_4.iCurrentHighestPriority[ iTeam ] < FMMC_MAX_RULES
		AND (NOT g_bMissionEnding OR IS_LOCAL_PLAYER_ANY_SPECTATOR() )
			TOGGLE_CROSS_THE_LINE_HUD()
			
			DISPLAY_CROSS_THE_LINE_HUD()
		ENDIF
	ENDIF
ENDPROC

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: PACKAGES HUD !
//
//************************************************************************************************************************************************************

FUNC BOOL SHOULD_DRAW_PACKAGE_HUD()
	IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iPartToUse].iteam] > 0
	OR IS_BIT_SET(iLocalBoolCheck,LBOOL_HAD_MORE_THAN_1_PACKAGE)
		IF MC_serverBD_4.iObjMissionLogic[MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER 
		OR MC_serverBD_4.iObjMissionLogic[MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD
			IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
				IF NOT IS_SPECTATOR_HUD_HIDDEN()
				AND NOT IS_SPECTATOR_SHOWING_NEWS_HUD()
				AND NOT g_bEndOfMissionCleanUpHUD
					IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_OBJECTIVE_BLOCKER)
					AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitset[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE_HIDE_HUD)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC DRAW_PACKAGE_HUD()

	INT imaxpackages
	INT ipackage
	BOOL bMyTeam[FMMC_MAX_NUM_OBJECTS]
	HUD_COLOURS CarryColour[FMMC_MAX_NUM_OBJECTS]
	HUD_COLOURS TextColour1,TextColour2
	
	INT iMyTeam = MC_playerBD[iPartToUse].iteam
	
	IF bPlayerToUseOK
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[ iMyTeam ]
		
		IF NOT SHOULD_USE_CUSTOM_STRING_FOR_HUD(iMyTeam,iRule)
			IF iHUDPriority != MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] 
				iObjectiveMaxPickup =0
				CLEAR_BIT(iLocalBoolCheck,LBOOL_HAD_MORE_THAN_1_PACKAGE)
				iHUDPriority = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] 
				FOR ipackage = 0 TO (FMMC_MAX_NUM_OBJECTS-1)
					DeliverColour[ipackage] = HUD_COLOUR_BLACK
				ENDFOR
				iPackageHUDBitset = 0
				ipackageSlot = 0
			ENDIF
			
			IF iObjectiveMaxPickup < MC_serverBD.iNumObjHighestPriority[MC_playerBD[iPartToUse].iteam]
				iObjectiveMaxPickup = MC_serverBD.iNumObjHighestPriority[MC_playerBD[iPartToUse].iteam]
				IF iObjectiveMaxPickup > 1
				//OR IS_BIT_SET(MC_serverBD.iAnyColObjRespawnAtPriority[MC_playerBD[iPartToUse].iteam],MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
					SET_BIT(iLocalBoolCheck,LBOOL_HAD_MORE_THAN_1_PACKAGE)
					PRINTLN("setting more than 1 package bitset")
				ENDIF
				IF iObjectiveMaxPickup > 8
					iObjectiveMaxPickup = 8
				ENDIF
			ENDIF
			
			IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
				IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMaxDeliveryEntitiesCanCarry[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]]  > 0
					IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iPartToUse].iteam] <= g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMaxDeliveryEntitiesCanCarry[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] 
						imaxpackages = MC_serverBD.iNumObjHighestPriority[MC_playerBD[iPartToUse].iteam]
					ELSE
						imaxpackages = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMaxDeliveryEntitiesCanCarry[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] 
					ENDIF	
				ELSE
					imaxpackages = MC_serverBD.iNumObjHighestPriority[MC_playerBD[iPartToUse].iteam]
				ENDIF
				
				IF imaxpackages > 8
					imaxpackages = 8
				ENDIF
				
				IF imaxpackages > 1
				OR IS_BIT_SET(iLocalBoolCheck,LBOOL_HAD_MORE_THAN_1_PACKAGE)
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetSix[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE6_BLOCK_PACKAGE_HUD)
						IF NOT IS_BIT_SET(MC_serverBD.iAnyColObjRespawnAtPriority[MC_playerBD[iPartToUse].iteam],MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
						AND MC_serverBD_4.iObjMissionLogic[MC_playerBD[iPartToUse].iteam] != FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD	
							
							FOR ipackage = 0 TO (imaxpackages-1)
								IF ipackage < MC_PlayerBd[iparttouse].iObjCarryCount
									CarryColour[ipackage] = HUD_COLOUR_BLUEDARK
								ELSE
									CarryColour[ipackage] = HUD_COLOUR_BLACK
								ENDIF
							ENDFOR
							
							FOR ipackage = 0 TO (iObjectiveMaxPickup-1)
								IF NOT IS_BIT_SET(iPackageHUDBitset, ipackage)
									IF DeliverColour[ipackageSlot] = HUD_COLOUR_BLACK
										IF ipackageSlot <= (iObjectiveMaxPickup-1)
											IF MC_serverBD.iObjDelTeam[ipackage] !=-1
												IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[ipackage].iPriority[MC_playerBD[iPartToUse].iteam] = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
													IF MC_playerBD[iPartToUse].iteam = MC_serverBD.iObjDelTeam[ipackage]
														DeliverColour[ipackageSlot] = HUD_COLOUR_BLUEDARK
														bMyTeam[ipackageSlot] = TRUE
													ELSE
														DeliverColour[ipackageSlot] = GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_serverBD.iObjDelTeam[ipackage], PlayerToUse)
													ENDIF
													
													ipackageSlot++
													
													SET_BIT(iPackageHUDBitset, ipackage)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDFOR
							
							TextColour1 = HUD_COLOUR_PURE_WHITE
							TextColour2 = HUD_COLOUR_BLUEDARK
							
							IF imaxpackages > 1
								DRAW_TWO_PACKAGES_EIGHT_HUD(imaxpackages,"MC_OBJCAR",iObjectiveMaxPickup,"MC_OBJDEL",CarryColour[0],CarryColour[1],CarryColour[2],CarryColour[3],CarryColour[4],CarryColour[5],CarryColour[6],CarryColour[7],
								DeliverColour[0],DeliverColour[1],DeliverColour[2],DeliverColour[3],DeliverColour[4],DeliverColour[5],DeliverColour[6],DeliverColour[7],-1,TextColour1,TextColour2 #IF USE_TU_CHANGES,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE, bMyTeam[0],bMyTeam[1],bMyTeam[2],bMyTeam[3],bMyTeam[4],bMyTeam[5],bMyTeam[6],bMyTeam[7] #ENDIF)
							ELSE
								DRAW_ONE_PACKAGES_EIGHT_HUD(iObjectiveMaxPickup,"MC_OBJDEL", FALSE,DeliverColour[0],DeliverColour[1],DeliverColour[2],DeliverColour[3],DeliverColour[4],DeliverColour[5],DeliverColour[6],DeliverColour[7],-1,TextColour1 #IF USE_TU_CHANGES,bMyTeam[0],bMyTeam[1],bMyTeam[2],bMyTeam[3],bMyTeam[4],bMyTeam[5],bMyTeam[6],bMyTeam[7] #ENDIF)
							ENDIF
						ELSE
							IF imaxpackages > 1
								FOR ipackage = 0 TO (imaxpackages-1)
									IF ipackage < MC_PlayerBd[iparttouse].iObjCarryCount
										CarryColour[ipackage] = HUD_COLOUR_BLUEDARK
									ELSE
										CarryColour[ipackage] = HUD_COLOUR_BLACK
									ENDIF
								ENDFOR
								
								TextColour1 = HUD_COLOUR_PURE_WHITE
								
								DRAW_ONE_PACKAGES_EIGHT_HUD(imaxpackages,"MC_OBJCAR", FALSE,CarryColour[0],CarryColour[1],CarryColour[2],CarryColour[3],CarryColour[4],CarryColour[5],CarryColour[6],CarryColour[7],-1,TextColour1)
							ENDIF
						ENDIF
					ELSE
						IF g_FMMC_STRUCT.iShowPackageHudForTeam > 0
							//For in and out - to deal with Jippin scoring issues - hardcoded o
							INT iNumofPackages = 8 - MC_serverBD.iNumObjHighestPriority[MC_playerBD[iPartToUse].iteam]
							DRAW_GENERIC_BIG_DOUBLE_NUMBER(iNumofPackages, 8 , "OVHEAD_PACKA")
						ELSE
							//default 
							IF iGetDeliverMax = -1
								iGetDeliverMax = MC_serverBD.iNumObjHighestPriority[MC_playerBD[iPartToUse].iteam]
							ENDIF
							INT iNumofPackages = iGetDeliverMax - MC_serverBD.iNumObjHighestPriority[MC_playerBD[iPartToUse].iteam]
							DRAW_GENERIC_BIG_DOUBLE_NUMBER(iNumofPackages, iGetDeliverMax , "OVHEAD_PACKA")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC


//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: HINT CAM AND QUICK GPS (in the interaction menu) !
//
//************************************************************************************************************************************************************

PROC CLEAN_QUICK_GPS(BOOL bPrimaryGPS = TRUE, BOOL bSecondaryGPS = FALSE)
	
	IF bPrimaryGPS
		IF NOT IS_VECTOR_ZERO(MPGlobalsAmbience.vQuickGPS)
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[CLEAN_QUICK_GPS] - Clearing Way Point.")
			SET_WAYPOINT_OFF()
		ENDIF
	
		MPGlobalsAmbience.vQuickGPS = <<0,0,0>>
	ENDIF
	
	IF bSecondaryGPS
		IF NOT IS_VECTOR_ZERO(MPGlobalsAmbience.vQuickGPS2)
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[CLEAN_QUICK_GPS] - Clearing Way Point.")
			SET_WAYPOINT_OFF()
		ENDIF
		
		MPGlobalsAmbience.vQuickGPS2 = <<0,0,0>>
		MPGlobalsAmbience.tlQuickGPS_Offrule = ""
	ENDIF
	
	MPGlobalsAmbience.bIsGPSDropOff = FALSE
	MPGlobalsAmbience.iQuickGPSTeam = -1
	MPGlobalsAmbience.iQuickGPSPriority = -1	
ENDPROC

PROC CLEAN_HINT_CAM()
	iNearestTargetType = ci_TARGET_NONE
	iNearestTarget = -1
ENDPROC

PROC CLEAN_QUICK_GPS_HINT_CAM()
	CLEAN_QUICK_GPS()
	KILL_CHASE_HINT_CAM(sHintCam)
	CLEAN_HINT_CAM()
	iLastFramesNearestTarget = -1
	iLastFramesTargetType = ci_TARGET_NONE
ENDPROC

FUNC BOOL SPECIAL_HINT_CAM_BEING_USED()
	
	IF IS_BIT_SET(iLocalBoolCheck30, LBOOL30_HAS_BEEN_SET_INTO_COVER_START_POSITION)
	AND (NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_HAS_CORRECTED_COVER_START_CAMERA_HEADING) OR NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_HAS_CORRECTED_COVER_START_PLAYER_HEADING))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: The global is used in: x:\gta5\script\dev_ng_Live\singleplayer\include\public\chase_hint_cam.sch
PROC SHOULD_HINT_CAM_BE_BLOCKED()
	g_bBlockHintCamUsageInMPMission = FALSE
	IF bLocalPlayerPedOk
	AND IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, TRUE)
		VEHICLE_INDEX iVehicle = GET_VEHICLE_PED_IS_IN(LocalPlayerPed, TRUE)
		VEHICLE_SEAT sVehicle = GET_SEAT_PED_IS_IN(LocalPlayerPed, TRUE)
		
		IF IS_VEHICLE_DRIVEABLE(iVehicle)
			IF (GET_ENTITY_MODEL(iVehicle) = APC AND sVehicle != VS_DRIVER)
			OR (GET_ENTITY_MODEL(iVehicle) = AKULA AND sVehicle != VS_DRIVER)
			OR (GET_ENTITY_MODEL(iVehicle) = RIOT2 AND sVehicle = VS_FRONT_RIGHT AND HAS_VEHICLE_GOT_MOD(iVehicle, MOD_ROOF) AND GET_VEHICLE_MOD(iVehicle, MOD_ROOF) != -1)
			OR GET_SCRIPT_TASK_STATUS(LocalPlayerPed, SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT) = PERFORMING_TASK
			OR GET_PED_RESET_FLAG(LocalPlayerPed, PRF_IsSeatShuffling)
			OR IS_TURRET_SEAT(iVehicle, sVehicle) // Could use this check to ensure the sVehicle variable is valid first; (ENUM_TO_INT(sVehicle) < (GET_VEHICLE_MODEL_NUMBER_OF_SEATS(GET_ENTITY_MODEL(viVehicle))-1)
				g_bBlockHintCamUsageInMPMission = TRUE
				PRINTLN("SHOULD_HINT_CAM_BE_BLOCKED - g_bBlockHintCamUsageInMPMission - Set to TRUE")
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_IHELP_HNT")
					CLEAR_HELP()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF g_bBombCamActive
	OR g_bPlayerViewingTurretHud 
		IF NOT HAS_NET_TIMER_STARTED(stHintCamTemporarilyDisableTimer)
			PRINTLN("SHOULD_HINT_CAM_BE_BLOCKED | Starting stHintCamTemporarilyDisableTimer due to g_bBombCamActive")
		ENDIF
		REINIT_NET_TIMER(stHintCamTemporarilyDisableTimer)
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(stHintCamTemporarilyDisableTimer)
		IF HAS_NET_TIMER_EXPIRED(stHintCamTemporarilyDisableTimer, ciHintCamTemporarilyDisableTimer_Length)
			RESET_NET_TIMER(stHintCamTemporarilyDisableTimer)
			PRINTLN("SHOULD_HINT_CAM_BE_BLOCKED | Resetting stHintCamTemporarilyDisableTimer")
		ELSE
			g_bBlockHintCamUsageInMPMission = TRUE
			PRINTLN("SHOULD_HINT_CAM_BE_BLOCKED | Blocking due to stHintCamTemporarilyDisableTimer")
		ENDIF
	ENDIF
ENDPROC

PROC HINT_CAM_AND_QUICK_GPS_PROCESSING()
	
	IF NOT IS_SKYSWOOP_AT_GROUND()
		EXIT
	ENDIF
	
	IF SPECIAL_HINT_CAM_BEING_USED()
		EXIT
	ENDIF
	
	INT iTeam = MC_PlayerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iTeam < FMMC_MAX_TEAMS
	AND iTeam >= 0
	AND iRule < FMMC_MAX_RULES
	AND iRule >= 0
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_BLOCK_GPS)
			//Clear the GPS vector
			CLEAN_QUICK_GPS()
		ENDIF
		
		SHOULD_HINT_CAM_BE_BLOCKED()
	
		// Next line removed as replacing this option with individual options for Quick GPS and Hint Cam
		//IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitSetFive[ iRule ], ciBS_RULE5_TOGGLE_QUICK_GPS_AND_HINTCAM)
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciBIRDS_EYE_ENABLED)
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciSCRIPTED_RACE_CAMS_ENABLED)
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_TURF_WAR_TOP_DOWN_CAMERA)
			FLOAT fFarCutoffRange = 400.0
			ENTITY_INDEX tempEnt
			MODEL_NAMES entMod
			
			// don't do gps hint cam during custcene focus hint cam
			IF( NOT IS_BIT_SET( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_START_FOCUS_INTRO )
			OR ( IS_BIT_SET( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_START_FOCUS_INTRO ) 
			AND IS_BIT_SET( iLocalBoolCheck6, LBOOL6_SCRIPTED_CUT_DONE_FOCUS_INTRO ) ) )
				IF iLastFramesNearestTarget = -1
					iLastFramesNearestTarget = iNearestTarget 
				ENDIF
				
				IF iLastFramesTargetType = ci_TARGET_NONE
					iLastFramesTargetType = iNearestTargetType
				ENDIF
				
				BOOL bShouldClear				
				IF IS_BIT_SET(iLocalBoolCheck3, LBOOL3_DISABLE_GPSANDHINTCAM)
					PRINTLN("Exit and Clean GPS (1)")
					bShouldClear = TRUE
				ENDIF				
				IF IS_BIT_SET(iLocalBoolCheck5, LBOOL5_OBJCOMP_KILLCHASEHINTCAM_1)
					PRINTLN("Exit and Clean GPS (2)")
					bShouldClear = TRUE
				ENDIF
				IF IS_BIT_SET(iLocalBoolCheck5, LBOOL5_OBJCOMP_KILLCHASEHINTCAM_2)
					PRINTLN("Exit and Clean GPS (3)")
					bShouldClear = TRUE
				ENDIF
				IF iSpectatorTarget !=-1
					PRINTLN("Exit and Clean GPS (4)")
					bShouldClear = TRUE
				ENDIF
				IF iNearestTargetType = ci_TARGET_NONE
				AND iLastFramesTargetType != iNearestTargetType
					PRINTLN("Exit and Clean GPS (5)")
					bShouldClear = TRUE
				ENDIF
				IF iNearestTarget = -1
				AND iLastFramesNearestTarget != iNearestTarget
					PRINTLN("Exit and Clean GPS (6)")
					bShouldClear = TRUE
				ENDIF				
				IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
				AND iRule > 0
					PRINTLN("Exit and Clean GPS (7)")
					bShouldClear = TRUE
				ENDIF
				IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_MIDPOINT_THIS_FRAME + iTeam)
				AND iRule > 0
					PRINTLN("Exit and Clean GPS (8)")
					bShouldClear = TRUE
				ENDIF
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VENETIAN_JOB(g_FMMC_STRUCT.iAdversaryModeType)
					PRINTLN("Exit and Clean GPS (9)")
					bShouldClear = TRUE
				ENDIF
				
				IF bShouldClear
					PRINTLN("Calling CLEAN_QUICK_GPS_HINT_CAM (1)")
					CLEAN_QUICK_GPS_HINT_CAM()
					EXIT
				ENDIF

				IF iNearestTarget != iLastFramesNearestTarget
				OR iNearestTargetType != iLastFramesTargetType
					IF NOT IS_THIS_BOMB_FOOTBALL()
						PRINTLN("Calling CLEAN_QUICK_GPS_HINT_CAM (2)")
						KILL_CHASE_HINT_CAM(sHintCam)
					ENDIF
					iLastFramesNearestTarget = iNearestTarget 
					iLastFramesTargetType = iNearestTargetType 
					EXIT 
				ENDIF
				
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_ARENA_CTF(g_FMMC_STRUCT.iAdversaryModeType)
				AND iNearestTargetType = ci_TARGET_OBJECT
					VECTOR vTempp 
					tempEnt = NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niObject[iNearestTarget])
					vTempp = GET_ENTITY_COORDS(tempEnt)
					IF MC_playerBD[iLocalPart].iObjCarryCount = 0
					AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed,vTempp) <= 15.0
						PRINTLN("Calling CLEAN_QUICK_GPS_HINT_CAM (3)")
						KILL_CHASE_HINT_CAM(sHintCam, DEFAULT, TRUE) 
						EXIT
					ENDIF
				ENDIF
				
				VECTOR vQuickGPS
				BOOL bCleanGPS = TRUE
				
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetThirteen[ iRule ], ciBS_RULE13_TOGGLE_QUICKGPS)
				
					SWITCH iNearestTargetType
					
						CASE ci_TARGET_DROP_OFF
							IF DOES_BLIP_EXIST( DeliveryBlip )
								vQuickGPS = GET_DROP_OFF_CENTER(TRUE)
								IF NOT IS_VECTOR_ZERO(vQuickGPS  )
									MPGlobalsAmbience.vQuickGPS = vQuickGPS
									MPGlobalsAmbience.bIsGPSDropOff = TRUE
									bCleanGPS = FALSE
								ENDIF
							ELIF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_DUMMY_BLIP_OVERRIDE)
								vQuickGPS = GET_DUMMY_OVERRIDE_CENTER(iDummyBlipCurrentOverride) 
								IF NOT IS_VECTOR_ZERO(vQuickGPS )
									MPGlobalsAmbience.vQuickGPS = vQuickGPS
									MPGlobalsAmbience.bIsGPSDropOff = TRUE
									bCleanGPS = FALSE
								ENDIF
							ENDIF
						BREAK
					
						CASE ci_TARGET_LOCATION
							vQuickGPS  =  GET_LOCATION_VECTOR(iNearestTarget)
							IF NOT IS_VECTOR_ZERO(vQuickGPS)
								MPGlobalsAmbience.vQuickGPS = vQuickGPS
								bCleanGPS = FALSE
							ENDIF
						BREAK
						
						CASE ci_TARGET_PED
							IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iNearestTarget])
								tempEnt = NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niPed[iNearestTarget])
								vQuickGPS  =  GET_ENTITY_COORDS(tempEnt)
								IF NOT IS_VECTOR_ZERO(vQuickGPS)
									MPGlobalsAmbience.vQuickGPS = vQuickGPS
									bCleanGPS = FALSE
								ENDIF
							ENDIF
						BREAK
						
						CASE ci_TARGET_VEHICLE
							IF IS_NET_VEHICLE_DRIVEABLE(MC_serverBD_1.sFMMC_SBD.niVehicle[iNearestTarget])
								tempEnt = NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicle[iNearestTarget])
								vQuickGPS  =  GET_ENTITY_COORDS(tempEnt)
								IF NOT IS_VECTOR_ZERO(vQuickGPS)
									MPGlobalsAmbience.vQuickGPS = vQuickGPS
									bCleanGPS = FALSE
								ENDIF
							ENDIF
						BREAK
						
						CASE ci_TARGET_OBJECT
							IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iNearestTarget])
								tempEnt = NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niObject[iNearestTarget])
								IF NOT IS_ENTITY_DEAD(tempEnt)
									vQuickGPS  =  GET_ENTITY_COORDS(tempEnt)
									IF  NOT IS_VECTOR_ZERO(vQuickGPS)
										MPGlobalsAmbience.vQuickGPS = vQuickGPS
										bCleanGPS = FALSE
									ENDIF
								ENDIF
							ENDIF
						BREAK
						
					ENDSWITCH
					
				ENDIF
					
					
				
				IF bCleanGPS
					CLEAN_QUICK_GPS()
				ELSE
					MPGlobalsAmbience.iQuickGPSTeam = iTeam
					MPGlobalsAmbience.iQuickGPSPriority = iRule
				ENDIF
				
				BOOL bTargetIsDropOffVehicle = FALSE
				INT iVeh
				NETWORK_INDEX niVeh
				VEHICLE_INDEX tempVeh
				
				//- Is the drop off a vehicle?
				IF iNearestTargetType = ci_TARGET_DROP_OFF
					IF g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffType[ iRule ] = ciFMMC_DROP_OFF_TYPE_VEHICLE
						iVeh = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOff_Vehicle[ iRule ] 
						IF iVeh >= 0
							niVeh = MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]
							IF IS_NET_VEHICLE_DRIVEABLE(niVeh)
								bTargetIsDropOffVehicle = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
					
				IF bLocalPlayerPedOk
					IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
						IF IS_PED_IN_ANY_HELI(LocalPlayerPed)
						OR IS_PED_IN_ANY_PLANE(LocalPlayerPed)
							fFarCutoffRange = 800.0
						ENDIF
						IF iNearestTargetType = ci_TARGET_VEHICLE
							IF IS_NET_VEHICLE_DRIVEABLE(MC_serverBD_1.sFMMC_SBD.niVehicle[iNearestTarget])
								tempEnt = NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicle[iNearestTarget])
								entMod = GET_ENTITY_MODEL(tempEnt)
								IF IS_THIS_MODEL_A_HELI(entMod)
								OR IS_THIS_MODEL_A_PLANE(entMod)
									fFarCutoffRange = 800.0
								ENDIF
								tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iNearestTarget])
								IF IS_PED_IN_VEHICLE(LocalPlayerPed, tempVeh)
									PRINTLN("Calling CLEAN_QUICK_GPS_HINT_CAM (4)")
									KILL_CHASE_HINT_CAM(sHintCam) 
									EXIT
								ENDIF
							ENDIF
						ELIF bTargetIsDropOffVehicle
							tempEnt = NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
							entMod = GET_ENTITY_MODEL(tempEnt)
							IF IS_THIS_MODEL_A_HELI(entMod)
							OR IS_THIS_MODEL_A_PLANE(entMod)
								fFarCutoffRange = 800.0
							ENDIF
						ENDIF
					ELSE
						//KILL_CHASE_HINT_CAM(sHintCam)  This is actually ridiculous. Commenting out.
						//EXIT
					ENDIF
				ELSE
					PRINTLN("Calling CLEAN_QUICK_GPS_HINT_CAM (5)")
					KILL_CHASE_HINT_CAM(sHintCam) 
					EXIT
				ENDIF
				
				IF fNearestTargetDist > fFarCutoffRange
				AND fNearestTargetDistTemp < 9999999.0
					PRINTLN("Calling CLEAN_QUICK_GPS_HINT_CAM (6)")
					KILL_CHASE_HINT_CAM(sHintCam) 
					EXIT
				ENDIF
				
				IF MPGlobalsAmbience.bDisableMissionHintCam
				OR g_heligunCamActive
				OR g_bBombCamActive
					PRINTLN("Calling CLEAN_QUICK_GPS_HINT_CAM (7)")
					KILL_CHASE_HINT_CAM(sHintCam) 
					EXIT
				ENDIF
				
				IF IS_PHONE_ONSCREEN()
					PRINTLN("Calling CLEAN_QUICK_GPS_HINT_CAM (8)")
					KILL_CHASE_HINT_CAM(sHintCam) 
					EXIT
				ENDIF
				
				IF IS_CUSTOM_MENU_ON_SCREEN()
					PRINTLN("Calling CLEAN_QUICK_GPS_HINT_CAM (9)")
					KILL_CHASE_HINT_CAM(sHintCam) 
					EXIT
				ENDIF
				
				IF IS_PAUSE_MENU_ACTIVE()
					PRINTLN("Calling CLEAN_QUICK_GPS_HINT_CAM (10)")
					KILL_CHASE_HINT_CAM(sHintCam) 
					EXIT
				ENDIF
				
				IF IS_CINEMATIC_CAM_RENDERING()
					PRINTLN("Calling CLEAN_QUICK_GPS_HINT_CAM (11)")
					KILL_CHASE_HINT_CAM(sHintCam) 
					EXIT
				ENDIF

				SWITCH iNearestTargetType
				
					CASE ci_TARGET_DROP_OFF
						IF bTargetIsDropOffVehicle
							tempEnt = NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetThirteen[ iRule ], ciBS_RULE13_TOGGLE_HINTCAM)
								CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE(sHintCam,tempEnt)
							ENDIF
						ELSE
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetThirteen[ iRule ], ciBS_RULE13_TOGGLE_HINTCAM)
								IF DOES_BLIP_EXIST( DeliveryBlip )
									vQuickGPS = GET_DROP_OFF_CENTER(TRUE)
								ELIF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_DUMMY_BLIP_OVERRIDE)
									vQuickGPS = GET_DUMMY_OVERRIDE_CENTER(iDummyBlipCurrentOverride) 
								ENDIF
							ENDIF
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetThirteen[ iRule ], ciBS_RULE13_TOGGLE_HINTCAM)
								IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed,vQuickGPS) > 30.0
									CONTROL_COORD_CHASE_HINT_CAM_IN_VEHICLE( sHintCam, vQuickGPS, "", HINTTYPE_NO_FOV)
								ELSE
									PRINTLN("Calling CLEAN_QUICK_GPS_HINT_CAM (12)")
									KILL_CHASE_HINT_CAM(sHintCam) 
								ENDIF
							ENDIF
						ENDIF						
					BREAK	
				
					CASE ci_TARGET_LOCATION
						IF  MC_playerBD[ iPartToUse ].iCurrentLoc != iNearestTarget
						AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD( LocalPlayerPed, GET_LOCATION_VECTOR( iNearestTarget ) ) > 30.0
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetThirteen[ iRule ], ciBS_RULE13_TOGGLE_HINTCAM)
								CONTROL_COORD_CHASE_HINT_CAM_IN_VEHICLE( sHintCam, GET_LOCATION_VECTOR( iNearestTarget ), "", HINTTYPE_NO_FOV )
							ENDIF
						ELSE
							PRINTLN("Calling CLEAN_QUICK_GPS_HINT_CAM (13)")
							KILL_CHASE_HINT_CAM(sHintCam) 
						ENDIF
					BREAK
					
					CASE ci_TARGET_PED
					
						IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iNearestTarget])
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetThirteen[ iRule ], ciBS_RULE13_TOGGLE_HINTCAM)
								tempEnt = NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niPed[iNearestTarget])
								CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE(sHintCam,tempEnt)
							ENDIF
						ELSE
							PRINTLN("Calling CLEAN_QUICK_GPS_HINT_CAM (14)")
							KILL_CHASE_HINT_CAM(sHintCam)
						ENDIF
						
					BREAK
					
					CASE ci_TARGET_VEHICLE
					
						IF IS_NET_VEHICLE_DRIVEABLE(MC_serverBD_1.sFMMC_SBD.niVehicle[iNearestTarget])
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetThirteen[ iRule ], ciBS_RULE13_TOGGLE_HINTCAM)
								tempEnt = NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicle[iNearestTarget])
		
								IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetThirteen[ iRule ], ciBS_RULE13_HALF_HINT_CAM_FOCUS_RANGE)						
									entMod = GET_ENTITY_MODEL(tempEnt)
									IF IS_THIS_MODEL_A_HELI(entMod)
									OR IS_THIS_MODEL_A_PLANE(entMod)
										IF VDIST_2D(GET_ENTITY_COORDS(LocalPlayerPed), GET_ENTITY_COORDS(tempEnt)) < 400.0
											CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE(sHintCam,tempEnt)
										ENDIF
									ELSE
										IF VDIST_2D(GET_ENTITY_COORDS(LocalPlayerPed), GET_ENTITY_COORDS(tempEnt)) < 200.0
											CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE(sHintCam,tempEnt)
										ENDIF
									ENDIF
								ELSE
									CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE(sHintCam,tempEnt)
								ENDIF
			
							ENDIF
						ELSE
							PRINTLN("Calling CLEAN_QUICK_GPS_HINT_CAM (15)")
							KILL_CHASE_HINT_CAM(sHintCam) 
						ENDIF
						
					BREAK
					
					CASE ci_TARGET_OBJECT
					
						IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iNearestTarget])
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetThirteen[ iRule ], ciBS_RULE13_TOGGLE_HINTCAM)
								tempEnt = NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niObject[iNearestTarget])
								IF NOT IS_ENTITY_DEAD(tempEnt)
									CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE(sHintCam,tempEnt)
								ELSE
									PRINTLN("Calling CLEAN_QUICK_GPS_HINT_CAM (16)")
									KILL_CHASE_HINT_CAM(sHintCam) 
								ENDIF
							ENDIF
						ELSE
							PRINTLN("Calling CLEAN_QUICK_GPS_HINT_CAM (17)")
							KILL_CHASE_HINT_CAM(sHintCam) 
						ENDIF
						
					BREAK
				
				ENDSWITCH
			ENDIF
		ELSE
			PRINTLN("Calling CLEAN_QUICK_GPS_HINT_CAM (18)")
			CLEAN_QUICK_GPS_HINT_CAM()
			PRINTLN("[QUICK_GPS][HINT_CAM] Hint Cam and Quick GPS disabled on this rule.")
		ENDIF
	ELSE
		PRINTLN("Calling CLEAN_QUICK_GPS_HINT_CAM (19)")
		CLEAN_QUICK_GPS_HINT_CAM()
		PRINTLN("[QUICK_GPS][HINT_CAM] Hint Cam and Quick GPS invalid rule.")
	ENDIF
ENDPROC

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: FILTERS PROCESSING
//
//************************************************************************************************************************************************************

PROC SWITCH_ON_FILTER(INT iFilter)
	
	SWITCH iFilter
		DEFAULT
		CASE 0
			IF NOT IS_BIT_SET(iFilterBS, iFilter)
				IF GET_TIMECYCLE_MODIFIER_INDEX() != -1
					SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(1.0)
					iFilterBS = 0
					SET_BIT(iFilterBS, iFilter)
					PRINTLN("[KH][FILTERS] SWITCH_ON_FILTER - filters de-activated")
					PRINTLN("[KH][FILTERS] SWITCH_ON_FILTER - Transitioning out of timecycle modifier")
				ENDIF
			ENDIF
		BREAK
		
		//TIMECYCLE FILTERS
		CASE 1		//Bluish
			IF NOT IS_BIT_SET(iFilterBS, iFilter)
				CLEAR_TIMECYCLE_MODIFIER()
				SET_TRANSITION_TIMECYCLE_MODIFIER("spectator1", 1.0)
				iFilterBS = 0
				SET_BIT(iFilterBS, iFilter)
				PRINTLN("[KH][FILTERS] SWITCH_ON_FILTER - spectator1 filter activated")
			ENDIF
		BREAK
		CASE 2		//Old TV
			IF NOT IS_BIT_SET(iFilterBS, iFilter)
				CLEAR_TIMECYCLE_MODIFIER()
				SET_TRANSITION_TIMECYCLE_MODIFIER("spectator2", 1.0)
				iFilterBS = 0
				SET_BIT(iFilterBS, iFilter)
				PRINTLN("[KH][FILTERS] SWITCH_ON_FILTER - spectator2 filter activated")
			ENDIF
		BREAK
		CASE 3		//high contrast Sepia
			IF NOT IS_BIT_SET(iFilterBS, iFilter)
				CLEAR_TIMECYCLE_MODIFIER()
				SET_TRANSITION_TIMECYCLE_MODIFIER("spectator3", 1.0)
				iFilterBS = 0
				SET_BIT(iFilterBS, iFilter)
				PRINTLN("[KH][FILTERS] SWITCH_ON_FILTER - spectator3 filter activated")
			ENDIF
		BREAK
		CASE 4		//high contrast B&W
			IF NOT IS_BIT_SET(iFilterBS, iFilter)
				CLEAR_TIMECYCLE_MODIFIER()
				SET_TRANSITION_TIMECYCLE_MODIFIER("spectator4", 1.0)
				iFilterBS = 0
				SET_BIT(iFilterBS, iFilter)
				PRINTLN("[KH][FILTERS] SWITCH_ON_FILTER - spectator4 filter activated")
			ENDIF
		BREAK
		CASE 5		//trippin' balls
			IF NOT IS_BIT_SET(iFilterBS, iFilter)
				CLEAR_TIMECYCLE_MODIFIER()
				SET_TRANSITION_TIMECYCLE_MODIFIER("spectator5", 1.0)
				iFilterBS = 0
				SET_BIT(iFilterBS, iFilter)
				PRINTLN("[KH][FILTERS] SWITCH_ON_FILTER - spectator5 filter activated")
			ENDIF
		BREAK
		CASE 6		//low saturation
			IF NOT IS_BIT_SET(iFilterBS, iFilter)
				CLEAR_TIMECYCLE_MODIFIER()
				SET_TRANSITION_TIMECYCLE_MODIFIER("spectator6", 1.0)
				iFilterBS = 0
				SET_BIT(iFilterBS, iFilter)
				PRINTLN("[KH][FILTERS] SWITCH_ON_FILTER - spectator6 filter activated")
			ENDIF
		BREAK
		CASE 7		//high exposure bluish purple
			IF NOT IS_BIT_SET(iFilterBS, iFilter)
				CLEAR_TIMECYCLE_MODIFIER()
				SET_TRANSITION_TIMECYCLE_MODIFIER("spectator7", 1.0)
				iFilterBS = 0
				SET_BIT(iFilterBS, iFilter)
				PRINTLN("[KH][FILTERS] SWITCH_ON_FILTER - spectator7 filter activated")
			ENDIF
		BREAK
		CASE 8		//noire
			IF NOT IS_BIT_SET(iFilterBS, iFilter)
				CLEAR_TIMECYCLE_MODIFIER()
				SET_TRANSITION_TIMECYCLE_MODIFIER("spectator8", 1.0)
				iFilterBS = 0
				SET_BIT(iFilterBS, iFilter)
				PRINTLN("[KH][FILTERS] SWITCH_ON_FILTER - spectator8 filter activated")
			ENDIF
		BREAK
		CASE 9		//super vibrant
			IF NOT IS_BIT_SET(iFilterBS, iFilter)
				CLEAR_TIMECYCLE_MODIFIER()
				SET_TRANSITION_TIMECYCLE_MODIFIER("spectator9", 1.0)
				iFilterBS = 0
				SET_BIT(iFilterBS, iFilter)
				PRINTLN("[KH][FILTERS] SWITCH_ON_FILTER - spectator9 filter activated")
			ENDIF
		BREAK
		CASE 10		//night vision green
			IF NOT IS_BIT_SET(iFilterBS, iFilter)
				CLEAR_TIMECYCLE_MODIFIER()
				SET_TRANSITION_TIMECYCLE_MODIFIER("spectator10", 1.0)
				iFilterBS = 0
				SET_BIT(iFilterBS, iFilter)
				PRINTLN("[KH][FILTERS] SWITCH_ON_FILTER - spectator10 filter activated")
			ENDIF
		BREAK	
		
		//POSTFX FILTERS
		CASE 11
			IF NOT IS_BIT_SET(iFilterBS, iFilter)
				iFilterBS = 0
				SET_BIT(iFilterBS, iFilter)
			ENDIF
			IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
				IF NOT ANIMPOSTFX_IS_RUNNING("InchPickup")
					ANIMPOSTFX_PLAY("InchPickup", 0, TRUE)
					PRINTLN("[KH][FILTERS] SWITCH_ON_FILTER - InchPickup filter activated")
				ENDIF
			ENDIF
		BREAK	
		CASE 12
			IF NOT IS_BIT_SET(iFilterBS, iFilter)
				iFilterBS = 0
				SET_BIT(iFilterBS, iFilter)
			ENDIF
			IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
				IF NOT ANIMPOSTFX_IS_RUNNING("PPOrange")
					ANIMPOSTFX_PLAY("PPOrange", 0, TRUE)
					PRINTLN("[KH][FILTERS] SWITCH_ON_FILTER - PPOrange filter activated")
				ENDIF
			ENDIF
		BREAK	
		CASE 13
			IF NOT IS_BIT_SET(iFilterBS, iFilter)
				iFilterBS = 0
				SET_BIT(iFilterBS, iFilter)
			ENDIF
			IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
				IF NOT ANIMPOSTFX_IS_RUNNING("PPPurple")
					ANIMPOSTFX_PLAY("PPPurple", 0, TRUE)
					PRINTLN("[KH][FILTERS] SWITCH_ON_FILTER - PPPurple filter activated")
				ENDIF
			ENDIF
		BREAK	
		CASE 14
			IF NOT IS_BIT_SET(iFilterBS, iFilter)
				iFilterBS = 0
				SET_BIT(iFilterBS, iFilter)
			ENDIF
			IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
				IF NOT ANIMPOSTFX_IS_RUNNING("PPGreen")
					ANIMPOSTFX_PLAY("PPGreen", 0, TRUE)
					PRINTLN("[KH][FILTERS] SWITCH_ON_FILTER - PPGreen filter activated")
				ENDIF
			ENDIF
		BREAK	
		CASE 15
			IF NOT IS_BIT_SET(iFilterBS, iFilter)
				iFilterBS = 0
				SET_BIT(iFilterBS, iFilter)
			ENDIF
			IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
				IF NOT ANIMPOSTFX_IS_RUNNING("PPPink")
					ANIMPOSTFX_PLAY("PPPink", 0, TRUE)
					PRINTLN("[KH][FILTERS] SWITCH_ON_FILTER - PPPink filter activated")
				ENDIF
			ENDIF
		BREAK
		CASE 16
			IF NOT IS_BIT_SET(iFilterBS, iFilter)
				iFilterBS = 0
				SET_BIT(iFilterBS, iFilter)
			ENDIF
			IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
				IF NOT ANIMPOSTFX_IS_RUNNING("DeadlineNeon")
					ANIMPOSTFX_PLAY("DeadlineNeon", 0, TRUE)
					PRINTLN("[KH][FILTERS] SWITCH_ON_FILTER - DeadlineNeon filter activated")
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	IF iFilter > -1
		bFilterIsOn = TRUE
	ELSE
		bFilterIsOn = FALSE
	ENDIF

ENDPROC

PROC TURN_OFF_NON_ACTIVE_FILTERS(INT iFilter)

	IF ANIMPOSTFX_IS_RUNNING("InchPickup")
	AND iFilter <> 11
		ANIMPOSTFX_STOP("InchPickup")
		ANIMPOSTFX_PLAY("InchPickupOut", 0, FALSE)
	
	ELIF ANIMPOSTFX_IS_RUNNING("PPOrange")
	AND iFilter <> 12
		ANIMPOSTFX_STOP("PPOrange")
		ANIMPOSTFX_PLAY("PPOrangeOut", 0, FALSE)
	
	ELIF ANIMPOSTFX_IS_RUNNING("PPPurple")
	AND iFilter <> 13
		ANIMPOSTFX_STOP("PPPurple")
		ANIMPOSTFX_PLAY("PPPurpleOut", 0, FALSE)
	
	ELIF ANIMPOSTFX_IS_RUNNING("PPGreen")
	AND iFilter <> 14
		ANIMPOSTFX_STOP("PPGreen")
		ANIMPOSTFX_PLAY("PPGreenOut", 0, FALSE)
	
	ELIF ANIMPOSTFX_IS_RUNNING("PPPink")
	AND iFilter <> 15
		ANIMPOSTFX_STOP("PPPink")
		ANIMPOSTFX_PLAY("PPPinkOut", 0, FALSE)
		
	ELIF ANIMPOSTFX_IS_RUNNING("DeadlineNeon")
	AND iFilter <> 16
		ANIMPOSTFX_STOP("DeadlineNeon")
		
	ELIF GET_TIMECYCLE_MODIFIER_INDEX() != -1
	AND iFilter < 1
	AND iFilter > 10
		SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(1.0)
	
	ENDIF

ENDPROC

PROC TURN_OFF_ACTIVE_FILTERS()
	IF ANIMPOSTFX_IS_RUNNING("InchPickup")
		ANIMPOSTFX_STOP("InchPickup")
	ELIF ANIMPOSTFX_IS_RUNNING("PPOrange")
		ANIMPOSTFX_STOP("PPOrange")
	ELIF ANIMPOSTFX_IS_RUNNING("PPPurple")
		ANIMPOSTFX_STOP("PPPurple")
	ELIF ANIMPOSTFX_IS_RUNNING("PPGreen")
		ANIMPOSTFX_STOP("PPGreen")
	ELIF ANIMPOSTFX_IS_RUNNING("PPPink")
		ANIMPOSTFX_STOP("PPPink")
	ELIF ANIMPOSTFX_IS_RUNNING("DeadlineNeon")
		ANIMPOSTFX_STOP("DeadlineNeon")
	ELIF GET_TIMECYCLE_MODIFIER_INDEX() != -1
		SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(1.0)
	ENDIF	
ENDPROC

/// PURPOSE:
///    Checks if there is any filter playing - please add more filters to this as needed
FUNC BOOL IS_ANY_FILTER_ACTIVE()
	
	IF ANIMPOSTFX_IS_RUNNING("InchPickup")
	OR ANIMPOSTFX_IS_RUNNING("PPOrange")
	OR ANIMPOSTFX_IS_RUNNING("PPPurple")
	OR ANIMPOSTFX_IS_RUNNING("PPGreen")
	OR ANIMPOSTFX_IS_RUNNING("PPPink")
	OR ANIMPOSTFX_IS_RUNNING("DeadlineNeon")
	OR GET_TIMECYCLE_MODIFIER_INDEX() != -1
		//PRINTLN("[KH][FILTERS] IS_ANY_FILTER_ACTIVE - TRUE")
		RETURN TRUE
	ENDIF
	
	//PRINTLN("[KH][FILTERS] IS_ANY_FILTER_ACTIVE - FALSE")
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_FILTER_A_TIMECYCLE_MODIFIER(INT iFilterSelection)
	//Include 0 because it takes time to transition out of the timecycle modifier
	IF iFilterSelection >= 0
	AND iFilterSelection <= 10
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_NEXT_VALID_FILTER(INT iCurrentFilter)
	INT iTeam = MC_playerBD[iPartToUse].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	INT iCacheFilter = iCurrentFilter
	
	IF iRule < FMMC_MAX_RULES
		INT iFilterLoop
		
		FOR iFilterLoop = 0 TO (ciMAX_FILTERS - 1)
			IF iCurrentFilter != iCacheFilter
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iFilterBitset[ iRule ], iCurrentFilter)
					RETURN iCurrentFilter
				ENDIF
			ENDIF
			
			IF iCurrentFilter < (ciMAX_FILTERS - 1)
				iCurrentFilter++
			ELSE
				iCurrentFilter = 0
			ENDIF
		ENDFOR
	ENDIF
	
	RETURN 0
ENDFUNC

PROC PROCESS_FILTERS()
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	IF GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
		IF iRule < FMMC_MAX_RULES
			IF bPlayerToUseOK
				INT iFilterHealth = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLowHealthFilter[iRule]
			
				IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule],  ciBS_RULE6_FILTERS_PLAYER_SELECTION)
				OR IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule],  ciBS_RULE6_FILTERS_FORCE_FILTER)
				OR iFilterHealth > 0
				OR HAS_NET_TIMER_STARTED(tdTeamSwapPassiveModeTimer)
				OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciPRON_USE_NEON_FILTER) AND IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARZONE(g_FMMC_STRUCT.iAdversaryModeType) // Setting these didn't seem to fix the issue... ciBS_RULE6_FILTERS_FORCE_FILTER or ciBS_RULE6_FILTERS_FORCE_FILTER ((Might not need the adversary mode check..))
				
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciPRON_USE_NEON_FILTER)
						IF NOT IS_ANY_FILTER_ACTIVE()
						AND bTimecycleTransitionComplete
							iActiveFilter = 16
						ENDIF
					ENDIF
						
					IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetSix[ iRule ],  ciBS_RULE6_FILTERS_PLAYER_SELECTION)
						
						IF iCachedFilter != 0
							iActiveFilter = iCachedFilter
						ENDIF
						
						IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
						OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT))
						AND NOT IS_PAUSE_MENU_ACTIVE()
						AND NOT IS_PHONE_ONSCREEN()
						AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
						AND NOT IS_CUSTOM_MENU_ON_SCREEN()
							iActiveFilter = GET_NEXT_VALID_FILTER(iActiveFilter)

							IF IS_FILTER_A_TIMECYCLE_MODIFIER(iActiveFilter)
								bTimecycleTransitionComplete = FALSE
								PRINTLN("[KH][FILTERS][NEW] TIMECYCLE SELECTED ", iActiveFilter)
							ENDIF
								
							iCachedFilter = iActiveFilter
						ENDIF
					ENDIF
					
					IF !bTimecycleTransitionComplete
						IF GET_TIMECYCLE_MODIFIER_INDEX() != -1
							bTimecycleTransitionComplete = TRUE
						ENDIF
					ENDIF
					
					IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule],  ciBS_RULE6_FILTERS_FORCE_FILTER)
						iActiveFilter = 0
						iActiveFilter = GET_NEXT_VALID_FILTER(iActiveFilter)
					ENDIF
					
					IF iFilterHealth > 0
						iActiveFilter = 0
						IF  (GET_ENTITY_HEALTH(LocalPlayerPed) - 100) < ((GET_ENTITY_MAX_HEALTH(LocalPlayerPed) - 100) / iFilterHealth)
							PRINTLN("[LH][HEALTHFILTERS] Player Health is below threshold! Player Health: ", GET_ENTITY_HEALTH(LocalPlayerPed)," Max Health: ", GET_ENTITY_MAX_HEALTH(LocalPlayerPed)," iLowHealthFilter: ",iFilterHealth)
							iActiveFilter = GET_NEXT_VALID_FILTER(iActiveFilter)
						ENDIF
					ENDIF
					
					IF HAS_NET_TIMER_STARTED(tdTeamSwapPassiveModeTimer)
					AND g_MultiplayerSettings.g_bTempPassiveModeSetting
						IF NOT (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdTeamSwapPassiveModeTimer) >= 5000)
							iActiveFilter = 11
							PRINTLN("[LH][HEALTHFILTERS] Passive mode filter")
						ENDIF
					ENDIF

					IF IS_ANY_FILTER_ACTIVE()
						IF NOT IS_BIT_SET(iLocalBoolCheck18, LBOOL18_IS_SPEED_BOOST_FILTER_DISABLED)
							SET_SPEED_BOOST_EFFECT_DISABLED(TRUE)
							PRINTLN("[KH][FILTERS] PROCESS_FILTERS - Setting LBOOL18_IS_SPEED_BOOST_FILTER_DISABLED")
							SET_BIT(iLocalBoolCheck18, LBOOL18_IS_SPEED_BOOST_FILTER_DISABLED)
						ENDIF
						
						//Reduce the bloom on pickups if the neon filter is on
						IF ANIMPOSTFX_IS_RUNNING("DeadlineNeon")
							SET_LIGHT_OVERRIDE_MAX_INTENSITY_SCALE(fBloomScale)
						ENDIF
					ELSE
						IF IS_BIT_SET(iLocalBoolCheck18, LBOOL18_IS_SPEED_BOOST_FILTER_DISABLED)
							SET_SPEED_BOOST_EFFECT_DISABLED(FALSE)
							PRINTLN("[KH][FILTERS] PROCESS_FILTERS - Clearing LBOOL18_IS_SPEED_BOOST_FILTER_DISABLED")
							CLEAR_BIT(iLocalBoolCheck18, LBOOL18_IS_SPEED_BOOST_FILTER_DISABLED)
						ENDIF
					ENDIF
					
					//Remove all filters if either of these power ups are active so they can use their own filters
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciOPTION_VEHICLE_WEAPON_ENABLED)
						IF IS_BIT_SET(MC_playerBD[iPartToUse].iVehicleWeaponEffectActiveBS, ciVEH_WEP_SPEED_BOOST)
						OR IS_BIT_SET(MC_playerBD[iPartToUse].iVehicleWeaponEffectActiveBS, ciVEH_WEP_ZONED)
							iActiveFilter = 0
						ENDIF
					ENDIF

					TURN_OFF_NON_ACTIVE_FILTERS(iActiveFilter)
					
					SWITCH_ON_FILTER(iActiveFilter)
				ELSE
					//Turn off any filters which have been on
					IF bFilterIsOn
						IF IS_ANY_FILTER_ACTIVE()
							iActiveFilter = 0
							iFilterBS = 0
							IF GET_TIMECYCLE_MODIFIER_INDEX() != -1
								SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(1.0)
							ENDIF
							TURN_OFF_NON_ACTIVE_FILTERS(iActiveFilter)
							bFilterIsOn = FALSE
							PRINTLN("[KH][FILTERS] PROCESS_FILTERS - Clearing filters")
						ELSE
							iActiveFilter = 0
							iFilterBS = 0
							bFilterIsOn = FALSE
							PRINTLN("[KH][FILTERS] PROCESS_FILTERS - No filters are active, clearing bitsets")
						ENDIF
						
					ENDIF
				ENDIF
			ELSE
				//Turn off any filters which have been on
				IF bFilterIsOn
					IF IS_ANY_FILTER_ACTIVE()
						iActiveFilter = 0
						iFilterBS = 0
						IF GET_TIMECYCLE_MODIFIER_INDEX() != -1
							SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(1.0)
						ENDIF
						TURN_OFF_NON_ACTIVE_FILTERS(iActiveFilter)
						bFilterIsOn = FALSE
						PRINTLN("[KH][FILTERS] PROCESS_FILTERS - Clearing filters as bPlayerToUseOK = FALSE")
					ELSE
						iActiveFilter = 0
						iFilterBS = 0
						bFilterIsOn = FALSE
						PRINTLN("[KH][FILTERS] PROCESS_FILTERS - No filters are active, not disabling")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC


//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: IPL MAP AND RADAR STUFF !
//
//************************************************************************************************************************************************************



/// PURPOSE: When playing The Humane Labs Raid finale mission, this function will change the map to
///    the interior inside the water tunnel while the player is in the water tunnel
proc PROCESS_BIOLAB_TUNNEL() 
	
	if is_entity_in_angled_area(player_ped_id(), <<3831.922, 3672.879, -25.393>>, <<3838.767, 3668.936, -20.793>>, 2.500)
	OR IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), <<3532.465576,3712.562988,21.712595>>, <<3516.776855,3715.232178,19.766590>>, 19.000000)
		render_underwater_tunnel_on_radar = false 
	endif 

	if is_entity_in_angled_area(player_ped_id(), <<3831.749, 3667.262, -24.591>>, <<3834.689, 3665.554, -21.091>>, 6.400)
	OR IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), <<3532.465576,3712.562988,15.712595>>, <<3516.776855,3715.232178,17.766590>>, 19.000000)
		render_underwater_tunnel_on_radar = true 
	endif 

	if render_underwater_tunnel_on_radar 
		SET_RADAR_AS_INTERIOR_THIS_FRAME(-1594035265, 3650, 3700, 0) 
	endif 

endproc


/// PURPOSE: When playing in the Silo interior, this function will change the map to
///    the map of the correct floor based on the player's Z position
PROC PROCESS_SILO_MINIMAP(VECTOR vPos,INT iInteriorHash) 
	
	VECTOR vPlayerPos = GET_ENTITY_COORDS(LocalPlayerPed)
	FLOAT fPlayerZ = vPlayerPos.z
	
	INT iFloor = -2
	
	IF fPlayerZ <= -156
		iFloor = 0
	ELIF fPlayerZ <= -152
		iFloor = 1
	ELIF fPlayerZ <= -148
		iFloor = 2
	ELSE
		iFloor = 3
	ENDIF
	
	PRINTLN("[SiloMinimap] Processing Silo Minimap, player Z is ", fPlayerZ, " hash is ", iInteriorHash, " pos is ", vPos, " floor is ", iFloor)
	//DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(LocalPlayerPed), TO_FLOAT(iFloor) * 2, 255, 255, 255, 100)
	
	BOOL bShowInterior = TRUE
	IF IS_PAUSE_MENU_ACTIVE()
		IF NOT IS_PAUSEMAP_IN_INTERIOR_MODE()
			bShowInterior = FALSE
		ENDIF
	ELIF g_PlayerBlipsData.bBigMapIsActive
		bShowInterior = FALSE
	ENDIF
	
	IF bShowInterior
		SET_RADAR_AS_INTERIOR_THIS_FRAME(iInteriorHash, vPos.x, vPos.y, 0, iFloor)
		HIDE_MINIMAP_EXTERIOR_MAP_THIS_FRAME()
	ELSE
		HIDE_MINIMAP_INTERIOR_MAP_THIS_FRAME()
	ENDIF
ENDPROC

PROC PROCESS_FOUNDRY_MINIMAP()

	//INT iInteriorHash = 87116549
	//INT iFloor = 0
	//VECTOR vInteriorPos = << 1087.14, -1986.68, 31.2089 >>
	//FLOAT fRotation = 0.959931
	
//	INTERIOR_INSTANCE_INDEX intTemp
//	intTemp = GET_INTERIOR_FROM_ENTITY(LocalPlayerPed)
//	
//	GET_INTERIOR_LOCATION_AND_NAMEHASH(intTemp, vInteriorPos, iInteriorHash)
//	fRotation = GET_INTERIOR_HEADING(intTemp)
//
//	SET_RADAR_AS_INTERIOR_THIS_FRAME(iInteriorHash, vInteriorPos.x, vInteriorPos.y, ROUND(fRotation), iFloor)
//	SET_RADAR_ZOOM(100)
//	HIDE_MINIMAP_EXTERIOR_MAP_THIS_FRAME()
//	
//	PRINTLN("[Minimap] Processing foundry Minimap / ", iInteriorHash, " / ", vInteriorPos, " / ", fRotation, "  fgfgf")
	
	SET_RADAR_AS_EXTERIOR_THIS_FRAME()
	SET_RADAR_ZOOM_PRECISE(50)
	
	PRINTLN("[TMS] Processing foundry minimap")
ENDPROC

/// PURPOSE: When playing IAA - Morgue, this function will change the map to
///    the map of the correct floor based on the player's Z position
PROC PROCESS_MORGUE_MINIMAP() 
	
	VECTOR vPlayerPos = GET_ENTITY_COORDS(LocalPlayerPed)
	FLOAT fPlayerZ = vPlayerPos.z
	
	INT iFloor = -2
	
	//INTERIOR_INSTANCE_INDEX intTemp
	//intTemp = GET_INTERIOR_FROM_ENTITY(LocalPlayerPed)
	
	INT iInteriorHash = -1409433418
	
//	IF GET_ROOM_KEY_FROM_ENTITY(PlayerPedToUse) = 0
//		EXIT
//	ENDIF
	
	PRINTLN("[TMS] Processing Morgue Minimap, player Z is ", fPlayerZ, " hash is ", iInteriorHash)
	
	IF fPlayerZ <= 26
		iFloor = -2
		PRINTLN("[TMS] Processing Morgue Minimap - Floor -2")
		//DRAW_DEBUG_SPHERE(vPlayerPos, 0.4, 55, 0, 0, 155)
	ELIF fPlayerZ <= 30
		iFloor = -1
		//SET_RADAR_AS_INTERIOR_THIS_FRAME(iInteriorHash, 249.719, -1368.729, -40, -1)
		PRINTLN("[TMS] Processing Morgue Minimap - Floor -1")
		//DRAW_DEBUG_SPHERE(vPlayerPos, 0.6, 5, 155, 0, 155)
	ELIF fPlayerZ <= 32
		iFloor = 0
		//SET_RADAR_AS_INTERIOR_THIS_FRAME(iInteriorHash, 249.719, -1368.729, -40, -1)
		PRINTLN("[TMS] Processing Morgue Minimap - Floor 0")
		//DRAW_DEBUG_SPHERE(vPlayerPos, 0.6, 155, 155, 0, 195)
	ELIF fPlayerZ > 38
		iFloor = 1
		//SET_RADAR_AS_INTERIOR_THIS_FRAME(iInteriorHash, 249.719, -1368.729, -40, 1)
		PRINTLN("[TMS] Processing Morgue Minimap - Floor 1")
		//DRAW_DEBUG_SPHERE(vPlayerPos, 1, 255, 0, 0, 155)
	ENDIF
	
	SET_RADAR_AS_INTERIOR_THIS_FRAME(iInteriorHash, 249.719, -1368.729, -40, iFloor)
ENDPROC

/// PURPOSE: When playing a mission using the Carrier IPL, this function will show the map and vary the zoom and floor of the map depending
///    on the player's location and exterior/interior-state when the player is in and around the Aircraft Carrier
PROC PROCESS_IPL_CARRIER_MAP(VECTOR vPlayer)
	
	VECTOR vCarrierCentre = <<3014.45, -4661.85, 18.85>>
	
	FLOAT fDistance2 = VDIST2(<<vPlayer.x, vPlayer.y, 0>>, <<vCarrierCentre.x, vCarrierCentre.y, 0>>)
	
	IF NOT IS_BIT_SET(iLocalBoolCheck5,LBOOL5_SHOW_CARRIER_IPL_MAP)
		
		IF IS_BIT_SET(iLocalBoolCheck11, LBOOL11_BIG_MAP_WAS_ZOOMED_IN_FOR_CARRIER)
			SET_RADAR_ZOOM_PRECISE(0)
			CLEAR_BIT(iLocalBoolCheck11, LBOOL11_BIG_MAP_WAS_ZOOMED_IN_FOR_CARRIER)
		ENDIF
		
		//Distance check to initialise:
		IF fDistance2 < (1500 * 1500)
			PRINTLN("[RCC MISSION] PROCESS_IPL_MAP_DISPLAY, IPL_CARRIER - Within 1500m of Carrier, initialising map")
			SET_BIT(iLocalBoolCheck5,LBOOL5_SHOW_CARRIER_IPL_MAP)
			SET_BIT(iLocalBoolCheck11, LBOOL11_BIG_MAP_DO_ZOOM_IN_FOR_CARRIER)
		ENDIF
	ELSE
		INT iFloor = -2
		BOOL bInternal = FALSE
		
		IF IS_PED_IN_ANY_PLANE(LocalPlayerPed)
			VEHICLE_INDEX pedPlane = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
			IF DOES_ENTITY_EXIST(pedPlane)
				IF IS_ENTITY_IN_AIR(pedPlane)
				AND NOT IS_VEHICLE_ON_ALL_WHEELS(pedPlane)
					IF IS_BIT_SET(iLocalBoolCheck11, LBOOL11_BIG_MAP_DO_ZOOM_IN_FOR_CARRIER)
						CLEAR_BIT(iLocalBoolCheck11, LBOOL11_BIG_MAP_DO_ZOOM_IN_FOR_CARRIER)
					ENDIF
					iZoomDelayGameTime = GET_GAME_TIMER()
				ELSE
					IF iZoomDelayGameTime > 0
						IF GET_GAME_TIMER() - iZoomDelayGameTime >= 2000
							SET_BIT(iLocalBoolCheck11, LBOOL11_BIG_MAP_DO_ZOOM_IN_FOR_CARRIER)
						ENDIF
					ENDIF	
				ENDIF
			ENDIF
		ELSE
			SET_BIT(iLocalBoolCheck11, LBOOL11_BIG_MAP_DO_ZOOM_IN_FOR_CARRIER)
		ENDIF
		
		IF (fDistance2 <= (300 * 300))
			
			IF (GET_ROOM_KEY_FROM_ENTITY(PlayerPedToUse) != 0)
				
				bInternal = TRUE
				
				IF vPlayer.z <= 3.0
					iFloor = -1
				ELIF vPlayer.z <= 7.3
					iFloor = 0
				ELIF vPlayer.z <= 12.6
					iFloor = 1
				ELIF vPlayer.z <= 15.724
					iFloor = 2
				ELIF vPlayer.z <= 18.756
					iFloor = 3
				ELIF vPlayer.z <= 21.73
					iFloor = 4
				ELIF vPlayer.z <= 24.633
					iFloor = 5
				ELIF vPlayer.z <= 27.64
					iFloor = 6
				ELIF vPlayer.z <= 32.756
					iFloor = 7
				ELIF vPlayer.z <= 52.938
					iFloor = 8
				ELSE
					iFloor = 2
				ENDIF
				
				IF IS_AUDIO_SCENE_ACTIVE("DLC_HEISTS_BIOLAB_STEAL_EMP_FLYOVER_SCENE")
					PRINTLN("[MJM MISSION] STOP_AUDIO_SCENE(DLC_HEISTS_BIOLAB_STEAL_EMP_FLYOVER_SCENE")
					STOP_AUDIO_SCENE("DLC_HEISTS_BIOLAB_STEAL_EMP_FLYOVER_SCENE")
				ENDIF
			ELSE					
				IF vPlayer.z <= 3.0
					IF IS_POINT_IN_ANGLED_AREA(vPlayer, <<3103.206543, -4805.419434, 5>>, <<3080.537109, -4811.319336, -2>>, 29.5) //Back entrance, dock
					OR IS_POINT_IN_ANGLED_AREA(vPlayer, <<3098.5, -4791.1, 6.6>>, <<3100.8, -4800.0, 0.0>>, 5.5) //Back entrance, stairs
						bInternal = TRUE
						iFloor = -1
					ELIF IS_POINT_IN_ANGLED_AREA(vPlayer, <<3109, -4825, 5>>,<<3082, -4832, -10>>, 130, TRUE) //Back entrance
						iFloor = -1
					ELIF IS_POINT_IN_ANGLED_AREA(vPlayer, <<3098.3, -4748.5, 2.55>>, <<3105.6, -4775.6, 10.3>>, 9.25) //Hole opposite rear platform
						bInternal = TRUE
						iFloor = 0
					ELSE
						iFloor = 2
					ENDIF
				ELIF vPlayer.z <= 11
					IF IS_POINT_IN_ANGLED_AREA(vPlayer, <<3072.1, -4825.9, 3.0>>, <<3107.2, -4810.5, 9.9>>, 28.7) //Back entrance, upper floor
					OR IS_POINT_IN_ANGLED_AREA(vPlayer, <<3082.2, -4659.9, 3.5>>, <<3075.1, -4632.6, 10.3>>, 22.25) //Front side platform
					OR IS_POINT_IN_ANGLED_AREA(vPlayer, <<3063.7, -4809.4, 3.5>>, <<3056.3, -4781.9, 10.3>>, 22.25) //Rear side platform
					OR IS_POINT_IN_ANGLED_AREA(vPlayer, <<3098.3, -4748.5, 2.55>>, <<3105.6, -4775.6, 10.3>>, 9.25) //Hole opposite rear platform
						bInternal = TRUE
						iFloor = 0
					ELIF ( IS_POINT_IN_ANGLED_AREA(vPlayer, <<3103.206543, -4805.419434, 5>>, <<3080.537109, -4811.319336, -2>>, 29.5) //Back entrance, dock
						   OR IS_POINT_IN_ANGLED_AREA(vPlayer, <<3098.5, -4791.1, 6.6>>, <<3100.8, -4800.0, 0.0>>, 5.5) ) //Back entrance, stairs
						bInternal = TRUE
						iFloor = -1
					ELSE
						iFloor = 2
					ENDIF
				ELSE
					iFloor = 2
				ENDIF
			ENDIF
			
			IF g_PlayerBlipsData.bBigMapIsActive
				SET_BIT(iLocalBoolCheck11, LBOOL11_BIG_MAP_WAS_ZOOMED_IN_FOR_CARRIER)
				IF IS_BIT_SET(iLocalBoolCheck11, LBOOL11_BIG_MAP_DO_ZOOM_IN_FOR_CARRIER)
					IF bInternal
						SET_RADAR_ZOOM_PRECISE(55.0)
					ELSE
						SET_RADAR_ZOOM_PRECISE(92.0)
					ENDIF
				ELSE
					SET_RADAR_ZOOM_PRECISE(0.0)
				ENDIF
			ELSE
				IF IS_BIT_SET(iLocalBoolCheck11, LBOOL11_BIG_MAP_WAS_ZOOMED_IN_FOR_CARRIER)
					SET_RADAR_ZOOM_PRECISE(0)
					CLEAR_BIT(iLocalBoolCheck11, LBOOL11_BIG_MAP_WAS_ZOOMED_IN_FOR_CARRIER)
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(iLocalBoolCheck11, LBOOL11_CARRIER_LIGHTS_PRELOADED)
				PRESET_INTERIOR_AMBIENT_CACHE("int_carrier_hanger")
				SET_BIT(iLocalBoolCheck11, LBOOL11_CARRIER_LIGHTS_PRELOADED)
			ENDIF
			
		ELSE					
			IF IS_BIT_SET(iLocalBoolCheck11, LBOOL11_BIG_MAP_WAS_ZOOMED_IN_FOR_CARRIER)
				SET_RADAR_ZOOM_PRECISE(0.0)
				CLEAR_BIT(iLocalBoolCheck11, LBOOL11_BIG_MAP_WAS_ZOOMED_IN_FOR_CARRIER)
			ENDIF
			
			IF IS_BIT_SET(iLocalBoolCheck11, LBOOL11_CARRIER_LIGHTS_PRELOADED)
				CLEAR_BIT(iLocalBoolCheck11, LBOOL11_CARRIER_LIGHTS_PRELOADED)
			ENDIF
			
			IF fDistance2 <= (2000 * 2000)
				iFloor = 2
			ELSE
				//Out of range, we could get in other interiors now - so clear this stuff from the map
				PRINTLN("[RCC MISSION] PROCESS_IPL_MAP_DISPLAY, IPL_CARRIER - Over 2000m from Carrier, clearing map")
				iFloor = -2
				CLEAR_BIT(iLocalBoolCheck5,LBOOL5_SHOW_CARRIER_IPL_MAP)
			ENDIF
		ENDIF
		
		IF iFloor >= -1
			SET_RADAR_AS_INTERIOR_THIS_FRAME(1596791599,3050.0,-4650.0,DEFAULT,iFloor)
			IF NOT bInternal
				SET_RADAR_AS_EXTERIOR_THIS_FRAME()
			ENDIF
		ENDIF				
	ENDIF
	
ENDPROC

//BOOL bDrawLineMarker
//
//PROC PROCESS_MARKER_LINES_BLIPS_FOR_ENEMY_TEAMS()
//	
//	IF NOT bDrawLineMarker
//		START_GPS_RACE_TRACK(HUD_COLOUR_YELLOW)
//		ADD_POINT_TO_GPS_RACE_TRACK(<<-1783.0216, 2518.0305, 2.7588>>)
//		ADD_POINT_TO_GPS_RACE_TRACK(<<-1959.9935, 2720.5234, 2.9044>>)
//		SET_RACE_TRACK_RENDER(TRUE)
//		bDrawLineMarker = TRUE
//		PRINTLN("[RCC MISSION] PROCESS_MARKER_LINES_BLIPS() - In here ")
//	ENDIF
//ENDPROC

/// PURPOSE: When playing a mission using the Yacht Chumash or Yacht Near Pier IPL, this function will show the map and vary the floor of the map depending
///    on the player's location and exterior/interior-state when the player is in and around the Yacht
PROC PROCESS_IPL_YACHT_NEAR_PIER_MAP(VECTOR vPlayer)
	
	VECTOR vYachtCentre = <<-2066.023, -1024.236, 7.95>>
	
	FLOAT fDistance2 = VDIST2(<<vPlayer.x, vPlayer.y, 0>>, <<vYachtCentre.x, vYachtCentre.y, 0>>)

	IF fDistance2 < (70 * 70)
		SET_RADAR_ZOOM_PRECISE(40)
	ELIF fDistance2 > (80 * 80)
		SET_RADAR_ZOOM_PRECISE(0)
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck6,LBOOL6_SHOW_YACHT2_IPL_MAP)
		//Distance check to initialise:
		IF fDistance2 < (300 * 300)
			PRINTLN("[RCC MISSION] PROCESS_IPL_MAP_DISPLAY, IPL_YACHT2 - Within 300m of Yacht, initialising map")
			SET_BIT(iLocalBoolCheck6,LBOOL6_SHOW_YACHT2_IPL_MAP)
		ENDIF
	ELSE
		INT iFloor = -2
		BOOL bInternal
		
		IF (fDistance2 <= (75 * 75))
			IF IS_POINT_IN_ANGLED_AREA(vPlayer, <<-2130.3, -1003.2, 19.7>>, <<-2016.7, -1040.4, -0.5>>, 17.0)
				
				bInternal = TRUE
				
				IF vPlayer.z <= 4.00
					iFloor = 0
				ELIF vPlayer.z <= 6.7
					iFloor = 1
				ELIF vPlayer.z <= 9.5
					iFloor = 2
				ELIF vPlayer.z <= 13.1
					iFloor = 3
				ELIF vPlayer.z <= 26.3
					iFloor = 4
				ELSE
					iFloor = 2
				ENDIF
			ELSE
				iFloor = 2
			ENDIF
		ELSE
			IF fDistance2 <= (320 * 320)
				iFloor = 2
			ELSE
				//Out of range, we could get in other interiors now - so clear this stuff from the map
				PRINTLN("[RCC MISSION] PROCESS_IPL_MAP_DISPLAY, IPL_YACHT2 - Over 320m from Yacht, clearing map")
				iFloor = -2
				CLEAR_BIT( iLocalBoolCheck6, LBOOL6_SHOW_YACHT2_IPL_MAP )
			ENDIF
		ENDIF
		
		IF iFloor >= 0
			SET_RADAR_AS_INTERIOR_THIS_FRAME(1906615853,-2066.023, -1024.236,DEFAULT,iFloor)
			IF NOT bInternal
				SET_RADAR_AS_EXTERIOR_THIS_FRAME()
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

PROC SET_ALL_ENITY_BLIP_DISPLAY(INTERIOR_INSTANCE_INDEX	idxInterior, BLIP_DISPLAY bDisplay, BOOL bCheckAnyInterior = FALSE, BOOL bProcessLocates = FALSE, BOOL bProcessAIBlips = FALSE, BOOL bProcessGetAndDeliver = FALSE)
	ENTITY_INDEX entIndex
	INT i
	PRINTLN("[SET_ALL_ENITY_BLIP_DISPLAY] - Looping Through Peds")
	FOR i = 0 TO FMMC_MAX_PEDS-1
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[i])
			IF DOES_BLIP_EXIST(biPedBlip[i])
				entIndex = NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niPed[i])			
				IF DOES_ENTITY_EXIST(entIndex)
				AND (GET_INTERIOR_FROM_ENTITY(entIndex) = idxInterior 
				OR (bCheckAnyInterior AND IS_VALID_INTERIOR(GET_INTERIOR_FROM_ENTITY(entIndex))))
					SET_BLIP_DISPLAY(biPedBlip[i], bDisplay)
				ENDIF
			ENDIF
		
			IF bProcessAIBlips
				IF bDisplay = DISPLAY_NOTHING
					CLEANUP_AI_PED_BLIP(biHostilePedBlip[i])
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	PRINTLN("[SET_ALL_ENITY_BLIP_DISPLAY] - Looping Through Vehicles with bDisplay: ", bDisplay)
	FOR i = 0 TO FMMC_MAX_VEHICLES-1
		IF DOES_BLIP_EXIST(biVehBlip[i])
		AND NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
			IF NOT bCheckAnyInterior
				entIndex = NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicle[i])	
				IF DOES_ENTITY_EXIST(entIndex)
				AND (GET_INTERIOR_FROM_ENTITY(entIndex) = idxInterior 
				OR (bCheckAnyInterior AND IS_VALID_INTERIOR(GET_INTERIOR_FROM_ENTITY(entIndex))))
					SET_BLIP_DISPLAY(biVehBlip[i], bDisplay)
				ENDIF
			ELSE
				SET_BLIP_DISPLAY(biVehBlip[i], bDisplay)
			ENDIF
		ENDIF
	ENDFOR
	PRINTLN("[SET_ALL_ENITY_BLIP_DISPLAY] - Looping Through Objects")
	FOR i = 0 TO FMMC_MAX_NUM_OBJECTS-1
		IF DOES_BLIP_EXIST(biObjBlip[i])
		AND NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[i])
			IF NOT bCheckAnyInterior
				entIndex = NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niObject[i])			
				IF DOES_ENTITY_EXIST(entIndex)
				AND (GET_INTERIOR_FROM_ENTITY(entIndex) = idxInterior 
				OR (bCheckAnyInterior AND IS_VALID_INTERIOR(GET_INTERIOR_FROM_ENTITY(entIndex))))
					SET_BLIP_DISPLAY(biObjBlip[i], bDisplay)
				ENDIF
			ELSE
				SET_BLIP_DISPLAY(biObjBlip[i], bDisplay)
			ENDIF
		ENDIF
	ENDFOR
	PRINTLN("[SET_ALL_ENITY_BLIP_DISPLAY] - Looping Through Pickups")
	FOR i = 0 TO FMMC_MAX_WEAPONS-1
		IF DOES_BLIP_EXIST(biPickup[i])
		AND DOES_PICKUP_EXIST(pipickup[i])
			OBJECT_INDEX puObject = GET_PICKUP_OBJECT(pipickup[i])
			IF DOES_PICKUP_OBJECT_EXIST(pipickup[i])
			AND DOES_ENTITY_EXIST(puObject)
			AND (GET_INTERIOR_FROM_ENTITY(puObject) = idxInterior 
				OR (bCheckAnyInterior AND IS_VALID_INTERIOR(GET_INTERIOR_FROM_ENTITY(puObject))))
				SET_BLIP_DISPLAY(biPickup[i], bDisplay)
			ENDIF
		ENDIF
	ENDFOR
	IF bProcessLocates
		PRINTLN("[SET_ALL_ENITY_BLIP_DISPLAY] - Looping Through Locates")
		FOR i = 0 TO FMMC_MAX_GO_TO_LOCATIONS-1
			IF DOES_BLIP_EXIST(LocBlip[i])
				SET_BLIP_DISPLAY(LocBlip[i], bDisplay)
			ENDIF
		ENDFOR
	ENDIF
	IF bProcessGetAndDeliver
		IF DOES_BLIP_EXIST(DeliveryBlip)
			SET_BLIP_DISPLAY(DeliveryBlip, bDisplay)
		ENDIF
	ENDIF
	
	IF bDisplay = DISPLAY_NOTHING
		SET_COP_BLIP_SPRITE(RADAR_TRACE_INVALID, 0.0)
	ELSE
		SET_COP_BLIP_SPRITE_AS_STANDARD()
	ENDIF
ENDPROC

PROC PROCESS_IMPORT_WAREHOUSE()

	PRINTLN("PROCESS_IMPORT_WAREHOUSE - calling function...")
	
	INT iInteriorHash = HASH("imp_impexp_intwaremed")
	INT iFloor = 0
	VECTOR vInteriorPos = <<974.9203, -3000.0647, -40.6470>>
	FLOAT fRotation = -0
	
	BOOL bShowInterior = TRUE
	BOOL bStaggerChange = (GET_FRAME_COUNT() % 5) = 0
	
	IF IS_PAUSE_MENU_ACTIVE()
		IF NOT IS_PAUSEMAP_IN_INTERIOR_MODE()
			bShowInterior = FALSE
		ENDIF
	ELIF g_PlayerBlipsData.bBigMapIsActive
		bShowInterior = FALSE
	ENDIF
	
	IF bShowInterior
		SET_RADAR_AS_INTERIOR_THIS_FRAME(iInteriorHash, vInteriorPos.x, vInteriorPos.y, ROUND(fRotation), iFloor)
		SET_RADAR_ZOOM(100)
		HIDE_MINIMAP_EXTERIOR_MAP_THIS_FRAME()
		IF bStaggerChange
			SET_ALL_ENITY_BLIP_DISPLAY(GET_INTERIOR_AT_COORDS(vInteriorPos),DISPLAY_BLIP)
			g_PlayerBlipsData.bHideAllPlayerBlips = FALSE
		ENDIF
	ELSE
		HIDE_PLAYER_ARROW_THIS_FRAME()
		HIDE_MINIMAP_INTERIOR_MAP_THIS_FRAME()
		IF bStaggerChange
			SET_ALL_ENITY_BLIP_DISPLAY(GET_INTERIOR_AT_COORDS(vInteriorPos),DISPLAY_NOTHING)
			g_PlayerBlipsData.bHideAllPlayerBlips = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_INTERIOR_MINIMAP(INT iInteriorHash, VECTOR vInteriorPos, FLOAT fXPosOffet = 0.0, FLOAT fYPosOffset = 0.0)

	IF GET_INTERIOR_FROM_ENTITY(PlayerPedToUse) != GET_INTERIOR_AT_COORDS(vInteriorPos)
		EXIT
	ENDIF
	
	INT iFloor = 0
	FLOAT fRotation = -0
	
	BOOL bShowInterior = TRUE
	BOOL bStaggerChange = (GET_FRAME_COUNT() % 5) = 0
	
	IF IS_PAUSE_MENU_ACTIVE()
		IF NOT IS_PAUSEMAP_IN_INTERIOR_MODE()
			bShowInterior = FALSE
		ENDIF
	ELIF g_PlayerBlipsData.bBigMapIsActive
		bShowInterior = FALSE
	ENDIF
	FLOAT fInteriorX = vInteriorPos.x + fXPosOffet
	FLOAT fInteriorY = vInteriorPos.y + fYPosOffset
	
	#IF IS_DEBUG_BUILD
	IF fMinimapXOffset != 0.0
		fInteriorX += fMinimapXOffset
	ENDIF
	IF fMinimapYOffset != 0.0
		fInteriorY += fMinimapYOffset
	ENDIF
	#ENDIF
	IF bShowInterior
		SET_RADAR_AS_INTERIOR_THIS_FRAME(iInteriorHash, fInteriorX, fInteriorY, ROUND(fRotation), iFloor)
		SET_RADAR_ZOOM(60)
		HIDE_MINIMAP_EXTERIOR_MAP_THIS_FRAME()
		IF bStaggerChange
			SET_ALL_ENITY_BLIP_DISPLAY(GET_INTERIOR_AT_COORDS(vInteriorPos),DISPLAY_BLIP)
			g_PlayerBlipsData.bHideAllPlayerBlips = FALSE
		ENDIF
	ELSE
		HIDE_PLAYER_ARROW_THIS_FRAME()
		HIDE_MINIMAP_INTERIOR_MAP_THIS_FRAME()
		IF bStaggerChange
			SET_ALL_ENITY_BLIP_DISPLAY(GET_INTERIOR_AT_COORDS(vInteriorPos),DISPLAY_NOTHING)
			g_PlayerBlipsData.bHideAllPlayerBlips = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_INTERIOR_DISPLACEMENT(INT iInteriorHash, VECTOR vInteriorPos, VECTOR vDisplacePos)
	
	IF iCommonDisplacementInterior != -1
	AND iCommonDisplacementInterior != iInteriorHash
		EXIT
	ENDIF
	
	IF GET_INTERIOR_FROM_ENTITY(LocalPlayerPed) != NULL
	AND GET_INTERIOR_FROM_ENTITY(LocalPlayerPed) = GET_INTERIOR_AT_COORDS(vInteriorPos)
		IF NOT bIsDisplacedInteriorSet
			bIsDisplacedInteriorSet = TRUE
			iCommonDisplacementInterior = iInteriorHash
			SET_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA(NATIVE_TO_INT(GET_INTERIOR_AT_COORDS(vInteriorPos)), vDisplacePos)
			PRINTLN("PROCESS_INTERIOR_DISPLACEMENT - Setting displacement")
		ENDIF
	ELSE
		IF bIsDisplacedInteriorSet
			bIsDisplacedInteriorSet = FALSE
			iCommonDisplacementInterior = -1
			CLEAR_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA()
			PRINTLN("PROCESS_INTERIOR_DISPLACEMENT - Clearing displacement")
		ENDIF		
	ENDIF
ENDPROC

PROC PROCESS_CASINO_BLIPS()
	
	IF IS_PED_INJURED(localPlayerPed)
		EXIT
	ENDIF
	
	INTERIOR_INSTANCE_INDEX iInteriorIndexCasinoMain, iInteriorIndexCasinoCarpark, iInteriorIndexCasinoApartment, iInteriorIndexCasinoGarage, iInteriorIndexCasinoPlayer
	INTERIOR_DATA_STRUCT structInteriorData	
	BOOL bInsideCasino = TRUE
	
	structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO)
	iInteriorIndexCasinoMain = GET_INTERIOR_AT_COORDS_WITH_TYPE( structInteriorData.vPos, structInteriorData.sInteriorName )
	structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_CAR_PARK)
	iInteriorIndexCasinoCarpark = GET_INTERIOR_AT_COORDS_WITH_TYPE( structInteriorData.vPos, structInteriorData.sInteriorName )
	structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_APARTMENT)
	iInteriorIndexCasinoApartment = GET_INTERIOR_AT_COORDS_WITH_TYPE( structInteriorData.vPos, structInteriorData.sInteriorName )
	structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_GARAGE)
	iInteriorIndexCasinoGarage = GET_INTERIOR_AT_COORDS_WITH_TYPE( structInteriorData.vPos, structInteriorData.sInteriorName )
	
	iInteriorIndexCasinoPlayer = GET_INTERIOR_FROM_ENTITY(localPlayerPed)
	
	#IF IS_DEBUG_BUILD
	IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD4)
		TEXT_LABEL_63 tlDebugText
		tlDebugText = "P: "
		tlDebugText += NATIVE_TO_INT(iInteriorIndexCasinoPlayer)
		DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.5, 0.25, 0.5>>)
		
		tlDebugText = "iInteriorIndexCasinoMain: "
		tlDebugText += NATIVE_TO_INT(iInteriorIndexCasinoMain)
		DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.5, 0.4, 0.5>>)
		
		tlDebugText = "iInteriorIndexCasinoCarpark: "
		tlDebugText += NATIVE_TO_INT(iInteriorIndexCasinoCarpark)
		DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.5, 0.45, 0.5>>)
		
		tlDebugText = "iInteriorIndexCasinoApartment: "
		tlDebugText += NATIVE_TO_INT(iInteriorIndexCasinoApartment)
		DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.5, 0.5, 0.5>>)
		
		tlDebugText = "iInteriorIndexCasinoGarage: "
		tlDebugText += NATIVE_TO_INT(iInteriorIndexCasinoGarage)
		DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.5, 0.55, 0.5>>)
	ENDIF
	#ENDIF
	
	IF (iInteriorIndexCasinoPlayer != iInteriorIndexCasinoMain
	AND iInteriorIndexCasinoPlayer != iInteriorIndexCasinoCarpark
	AND iInteriorIndexCasinoPlayer != iInteriorIndexCasinoApartment
	AND iInteriorIndexCasinoPlayer != iInteriorIndexCasinoGarage)
	OR iInteriorIndexCasinoPlayer = NULL
		PRINTLN("[PROCESS_CASINO_BLIPS] - Not inside a casino interior.")
		bInsideCasino = FALSE
	ELSE
		PRINTLN("[PROCESS_CASINO_BLIPS] - inside a casino interior.")
	ENDIF
		
	BOOL bShowInterior = bInsideCasino
	
	IF IS_PAUSE_MENU_ACTIVE()
		IF NOT IS_PAUSEMAP_IN_INTERIOR_MODE()
			bShowInterior = FALSE
		ENDIF
	ELIF g_PlayerBlipsData.bBigMapIsActive
		bShowInterior = FALSE
	ENDIF
	
	IF bShowInterior
	OR NOT bInsideCasino
		
		IF IS_BIT_SET(iLocalBoolCheck31, LBOOL31_CASINO_MISSION_CONTROLLER_BLIPS_HIDDEN)
			SET_ALL_ENITY_BLIP_DISPLAY(iInteriorIndexCasinoPlayer, DISPLAY_BLIP)
			g_PlayerBlipsData.bHideAllPlayerBlips = FALSE
			CLEAR_BIT(iLocalBoolCheck31, LBOOL31_CASINO_MISSION_CONTROLLER_BLIPS_HIDDEN)
			PRINTLN("[PROCESS_CASINO_BLIPS] - Showing")
		ENDIF
	ELSE
		HIDE_MINIMAP_INTERIOR_MAP_THIS_FRAME()
		
		IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_CASINO_MISSION_CONTROLLER_BLIPS_HIDDEN)
			SET_ALL_ENITY_BLIP_DISPLAY(iInteriorIndexCasinoPlayer, DISPLAY_NOTHING)
			g_PlayerBlipsData.bHideAllPlayerBlips = TRUE
			SET_BIT(iLocalBoolCheck31, LBOOL31_CASINO_MISSION_CONTROLLER_BLIPS_HIDDEN)
			PRINTLN("[PROCESS_CASINO_BLIPS] - Hiding")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: When playing Casino Heist, this function will change the map to
///    the map of the correct floor based on the player's Z position
PROC PROCESS_CASINO_HEIST_MINIMAP() 
	
	IF IS_PED_INJURED(LocalPlayerPed)
		EXIT
	ENDIF
	
	INTERIOR_INSTANCE_INDEX iInteriorIndexCasinoPlayer
	INTERIOR_DATA_STRUCT structInteriorData	
	
	IF iInteriorIndexCasinoMainCH = NULL
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_MAIN)
		iInteriorIndexCasinoMainCH = GET_INTERIOR_AT_COORDS_WITH_TYPE( structInteriorData.vPos, structInteriorData.sInteriorName )
		PRINTLN("[PROCESS_CASINO_HEIST_MINIMAP] - Grabbing interior data for iInteriorIndexCasinoMainCH.")
	ENDIF
	
	IF iInteriorIndexCasinoTunnel = NULL
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_TUNNEL)
		iInteriorIndexCasinoTunnel = GET_INTERIOR_AT_COORDS_WITH_TYPE( structInteriorData.vPos, structInteriorData.sInteriorName )
		PRINTLN("[PROCESS_CASINO_HEIST_MINIMAP] - Grabbing interior data for iInteriorIndexCasinoTunnel.")
	ENDIF
	
	IF iInteriorIndexCasinoBackArea = NULL
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_BACK_AREA)
		iInteriorIndexCasinoBackArea = GET_INTERIOR_AT_COORDS_WITH_TYPE( structInteriorData.vPos, structInteriorData.sInteriorName )
		PRINTLN("[PROCESS_CASINO_HEIST_MINIMAP] - Grabbing interior data for iInteriorIndexCasinoBackArea.")
	ENDIF
	
	IF iInteriorIndexCasinoHotelFloor = NULL
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_HOTEL_FLOOR)
		iInteriorIndexCasinoHotelFloor = GET_INTERIOR_AT_COORDS_WITH_TYPE( structInteriorData.vPos, structInteriorData.sInteriorName )
		PRINTLN("[PROCESS_CASINO_HEIST_MINIMAP] - Grabbing interior data for iInteriorIndexCasinoHotelFloor.")
	ENDIF
	
	IF iInteriorIndexCasinoLoadingBay = NULL
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_LOADING_BAY)
		iInteriorIndexCasinoLoadingBay = GET_INTERIOR_AT_COORDS_WITH_TYPE( structInteriorData.vPos, structInteriorData.sInteriorName )
		PRINTLN("[PROCESS_CASINO_HEIST_MINIMAP] - Grabbing interior data for iInteriorIndexCasinoLoadingBay.")
	ENDIF
	
	IF iInteriorIndexCasinoVault = NULL
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_VAULT)
		iInteriorIndexCasinoVault = GET_INTERIOR_AT_COORDS_WITH_TYPE( structInteriorData.vPos, structInteriorData.sInteriorName )
		PRINTLN("[PROCESS_CASINO_HEIST_MINIMAP] - Grabbing interior data for iInteriorIndexCasinoVault.")
	ENDIF
	
	IF iInteriorIndexCasinoUtilityLift = NULL
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_UTILITY_LIFT)
		iInteriorIndexCasinoUtilityLift = GET_INTERIOR_AT_COORDS_WITH_TYPE( structInteriorData.vPos, structInteriorData.sInteriorName )
		PRINTLN("[PROCESS_CASINO_HEIST_MINIMAP] - Grabbing interior data for iInteriorIndexCasinoUtilityLift.")
	ENDIF
	
	IF iInteriorIndexCasinoUtilityShaft = NULL
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_CASINO_LIFT_SHAFT)
		iInteriorIndexCasinoUtilityShaft = GET_INTERIOR_AT_COORDS_WITH_TYPE( structInteriorData.vPos, structInteriorData.sInteriorName )
		PRINTLN("[PROCESS_CASINO_HEIST_MINIMAP] - Grabbing interior data for iInteriorIndexCasinoUtilityShaft.")
	ENDIF
	
	IF IS_LOCAL_PLAYER_USING_DRONE()
		iInteriorIndexCasinoPlayer = GET_INTERIOR_FROM_ENTITY(g_sDroneGlobals.DroneProp)
	ELSE
		iInteriorIndexCasinoPlayer = GET_INTERIOR_FROM_ENTITY(localPlayerPed)
	ENDIF
	
	IF iInteriorIndexCasinoPlayer = NULL
	OR (iInteriorIndexCasinoPlayer != iInteriorIndexCasinoMainCH
	AND iInteriorIndexCasinoPlayer != iInteriorIndexCasinoTunnel
	AND iInteriorIndexCasinoPlayer != iInteriorIndexCasinoBackArea
	AND iInteriorIndexCasinoPlayer != iInteriorIndexCasinoHotelFloor
	AND iInteriorIndexCasinoPlayer != iInteriorIndexCasinoLoadingBay
	AND iInteriorIndexCasinoPlayer != iInteriorIndexCasinoVault
	AND iInteriorIndexCasinoPlayer != iInteriorIndexCasinoUtilityLift
	AND iInteriorIndexCasinoPlayer != iInteriorIndexCasinoUtilityShaft)
		PRINTLN("[PROCESS_CASINO_HEIST_MINIMAP] - Not inside a Casino Heist interior.")
		
		IF bIsDisplacedInteriorSet
			bIsDisplacedInteriorSet = FALSE
			CLEAR_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA()
		ENDIF
		
		IF IS_BIT_SET(iLocalBoolCheck33, LBOOL33_ZOOMED_IN_FOR_CASINO_HEIST)
			SET_RADAR_ZOOM_PRECISE(0)
			CLEAR_BIT(iLocalBoolCheck33, LBOOL33_ZOOMED_IN_FOR_CASINO_HEIST)
		ENDIF
		
		EXIT
	ENDIF
	
	IF NOT bIsDisplacedInteriorSet
	AND g_PlayerBlipsData.DisplacedInteriorThreadID = INT_TO_NATIVE(THREADID, -1)
		VECTOR vCasinoDisplacedCoord = << 965.82, 42.25, 82.0 >>
		bIsDisplacedInteriorSet = TRUE
		SET_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA(NATIVE_TO_INT(iInteriorIndexCasinoMainCH), vCasinoDisplacedCoord)
		SET_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA(NATIVE_TO_INT(iInteriorIndexCasinoTunnel), vCasinoDisplacedCoord)
		SET_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA(NATIVE_TO_INT(iInteriorIndexCasinoBackArea), vCasinoDisplacedCoord)
		SET_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA(NATIVE_TO_INT(iInteriorIndexCasinoHotelFloor), vCasinoDisplacedCoord)
		SET_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA(NATIVE_TO_INT(iInteriorIndexCasinoLoadingBay), vCasinoDisplacedCoord)
		SET_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA(NATIVE_TO_INT(iInteriorIndexCasinoVault), vCasinoDisplacedCoord)
		SET_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA(NATIVE_TO_INT(iInteriorIndexCasinoUtilityLift), vCasinoDisplacedCoord)
		SET_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA(NATIVE_TO_INT(iInteriorIndexCasinoUtilityShaft), vCasinoDisplacedCoord)
	ENDIF
	
	PRINTLN("[PROCESS_CASINO_HEIST_MINIMAP] - inside a Casino Heist interior.")
	
	INT iInteriorHash = HASH("ch_dlc_casino_FakeHeistComposite") //3616464270
	INT iLevel = 0
	VECTOR vPlayerPos 
	
	IF IS_LOCAL_PLAYER_USING_DRONE()
		vPlayerPos = GET_ENTITY_COORDS(g_sDroneGlobals.DroneProp)
		PRINTLN("[PROCESS_CASINO_HEIST_MINIMAP] DRONE ACTIVE")
	ELSE
		vPlayerPos = GET_ENTITY_COORDS(LocalPlayerPed)
	ENDIF
	
	FLOAT fPlayerZ = vPlayerPos.z
	
	PRINTLN("[PROCESS_CASINO_HEIST_MINIMAP] Processing Casino Heist Minimap, player Z is ", fPlayerZ, " hash is ", iInteriorHash)
		
	iLevel = GET_CASINO_INTERIOR_FLOOR(fPlayerZ)
	
	PRINTLN("[PROCESS_CASINO_HEIST_MINIMAP] iLevel is ", iLevel)
	
	IF iLevel > 0
	AND iInteriorIndexCasinoPlayer != iInteriorIndexCasinoUtilityShaft
		PRINTLN("[PROCESS_CASINO_HEIST_MINIMAP] Calling SET_RADAR_AS_INTERIOR_THIS_FRAME")
		SET_RADAR_AS_INTERIOR_THIS_FRAME(iInteriorHash, 2504.386, -257.22, 0, iLevel)
	ENDIF

	BOOL bShowInterior = TRUE
		
	IF IS_PAUSE_MENU_ACTIVE()
		IF NOT IS_PAUSEMAP_IN_INTERIOR_MODE()
			bShowInterior = FALSE
		ENDIF
	ELIF g_PlayerBlipsData.bBigMapIsActive
		bShowInterior = FALSE
	ENDIF
	
	IF bShowInterior
		IF NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_ZOOMED_IN_FOR_CASINO_HEIST)
			IF iInteriorIndexCasinoPlayer = iInteriorIndexCasinoLoadingBay
				SET_RADAR_ZOOM_PRECISE(0.1)
				SET_BIT(iLocalBoolCheck33, LBOOL33_ZOOMED_IN_FOR_CASINO_HEIST)
			ENDIF	
		ELIF iInteriorIndexCasinoPlayer != iInteriorIndexCasinoLoadingBay
			SET_RADAR_ZOOM_PRECISE(0)
			CLEAR_BIT(iLocalBoolCheck33, LBOOL33_ZOOMED_IN_FOR_CASINO_HEIST)
		ELSE
			SET_RADAR_ZOOM_PRECISE(0.1) //Call everyframe
		ENDIF
		
		HIDE_MINIMAP_EXTERIOR_MAP_THIS_FRAME()
		
		IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_CASINO_HEIST_MISSION_CONTROLLER_BLIPS_HIDDEN)
			SET_ALL_ENITY_BLIP_DISPLAY(iInteriorIndexCasinoPlayer, DISPLAY_BLIP, TRUE, TRUE, TRUE, TRUE)
			g_PlayerBlipsData.bHideAllPlayerBlips = FALSE
			CLEAR_BIT(iLocalBoolCheck32, LBOOL32_CASINO_HEIST_MISSION_CONTROLLER_BLIPS_HIDDEN)
			PRINTLN("[PROCESS_CASINO_HEIST_MINIMAP] - Showing Blips")
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck33, LBOOL33_ZOOMED_IN_FOR_CASINO_HEIST)
			SET_RADAR_ZOOM_PRECISE(0)
			CLEAR_BIT(iLocalBoolCheck33, LBOOL33_ZOOMED_IN_FOR_CASINO_HEIST)
		ENDIF
	
		HIDE_MINIMAP_INTERIOR_MAP_THIS_FRAME()
		HIDE_PLAYER_ARROW_THIS_FRAME()
		SET_ALL_ENITY_BLIP_DISPLAY(iInteriorIndexCasinoPlayer, DISPLAY_NOTHING, TRUE, TRUE, TRUE, TRUE)
		
		IF NOT IS_BIT_SET(iLocalBoolCheck32, LBOOL32_CASINO_HEIST_MISSION_CONTROLLER_BLIPS_HIDDEN)
			g_PlayerBlipsData.bHideAllPlayerBlips = TRUE
			SET_BIT(iLocalBoolCheck32, LBOOL32_CASINO_HEIST_MISSION_CONTROLLER_BLIPS_HIDDEN)
			PRINTLN("[PROCESS_CASINO_HEIST_MINIMAP] - Hiding Blips")
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_CASINO_ROOF_MINIMAP()
	
	BOOL bZoom = TRUE
		
	IF IS_PAUSE_MENU_ACTIVE()
		IF NOT IS_PAUSEMAP_IN_INTERIOR_MODE()
			bZoom = FALSE
		ENDIF
	ELIF g_PlayerBlipsData.bBigMapIsActive
		bZoom = FALSE
	ENDIF
	
	IF bZoom
		IF NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_ZOOMED_IN_FOR_CASINO_ROOF)
			IF IS_PLAYER_ON_CASINO_ROOF()
				SET_RADAR_ZOOM_PRECISE(0.1)
				SET_BIT(iLocalBoolCheck33, LBOOL33_ZOOMED_IN_FOR_CASINO_ROOF)
			ENDIF
		ELIF NOT IS_PLAYER_ON_CASINO_ROOF()
			SET_RADAR_ZOOM_PRECISE(0)
			CLEAR_BIT(iLocalBoolCheck33, LBOOL33_ZOOMED_IN_FOR_CASINO_ROOF)
		ELSE
			SET_RADAR_ZOOM_PRECISE(0.1) //Call everyframe
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck33, LBOOL33_ZOOMED_IN_FOR_CASINO_HEIST)
			SET_RADAR_ZOOM_PRECISE(0)
			CLEAR_BIT(iLocalBoolCheck33, LBOOL33_ZOOMED_IN_FOR_CASINO_HEIST)
		ENDIF
	ENDIF

ENDPROC

PROC PROCESS_WAREHOUSE_UNDRGRND_FACILITY()

	PRINTLN("PROCESS_WAREHOUSE_UNDRGRND_FACILITY - calling function...")
	
	INT iInteriorHash = HASH("imp_impexp_int_02")
	INT iFloor = 0
	VECTOR vInteriorPos = <<969.5376, -3000.4111, -48.6470>>
	FLOAT fRotation = 90
	
	BOOL bShowInterior = TRUE
	BOOL bStaggerChange = (GET_FRAME_COUNT() % 5) = 0
	
	IF IS_PAUSE_MENU_ACTIVE()
		IF NOT IS_PAUSEMAP_IN_INTERIOR_MODE()
			bShowInterior = FALSE
		ENDIF
	ELIF g_PlayerBlipsData.bBigMapIsActive
		bShowInterior = FALSE
	ENDIF
	
	IF bShowInterior
		SET_RADAR_AS_INTERIOR_THIS_FRAME(iInteriorHash, vInteriorPos.x, vInteriorPos.y, ROUND(fRotation), iFloor)
		SET_RADAR_ZOOM(100)
		HIDE_MINIMAP_EXTERIOR_MAP_THIS_FRAME()
		IF bStaggerChange
			SET_ALL_ENITY_BLIP_DISPLAY(GET_INTERIOR_AT_COORDS(vInteriorPos),DISPLAY_BLIP)
			HIDE_ALL_PLAYER_BLIPS(FALSE)
		ENDIF
	ELSE
		HIDE_PLAYER_ARROW_THIS_FRAME()
		HIDE_MINIMAP_INTERIOR_MAP_THIS_FRAME()
		IF bStaggerChange
			SET_ALL_ENITY_BLIP_DISPLAY(GET_INTERIOR_AT_COORDS(vInteriorPos),DISPLAY_NOTHING)
			HIDE_ALL_PLAYER_BLIPS(TRUE)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SUBMARINE_MINIMAP()
	VECTOR vPlayerPos = GET_ENTITY_COORDS(LocalPlayerPed)
	FLOAT fPlayerZ = vPlayerPos.z

	INT iInteriorHash = 1912197552
	INT iFloor = 2
	
	PRINTLN("[JR] Processing Submarine Minimap, player Z is ", fPlayerZ, " hash is ", iInteriorHash)
	
	IF fPlayerZ >= -63
		iFloor = 2
		PRINTLN("[JR] Processing Submarine Minimap - Floor 2")
	ELIF fPlayerZ >= -66.5
		iFloor = 1
		PRINTLN("[JR] Processing Submarine Minimap - Floor 1")
	ELIF fPlayerZ >= -69.5
		iFloor = 0
		PRINTLN("[JR] Processing Submarine Minimap - Floor 0")
	ENDIF
	
	BOOL bShowInterior = TRUE
	IF IS_PAUSE_MENU_ACTIVE()
		IF NOT IS_PAUSEMAP_IN_INTERIOR_MODE()
			bShowInterior = FALSE
		ENDIF
	ELIF g_PlayerBlipsData.bBigMapIsActive
		bShowInterior = FALSE
	ENDIF
	IF bShowInterior
		SET_RADAR_AS_INTERIOR_THIS_FRAME(iInteriorHash, 512.769, 4851.98, -0, iFloor)
		HIDE_MINIMAP_EXTERIOR_MAP_THIS_FRAME()
	ELSE
		HIDE_MINIMAP_INTERIOR_MAP_THIS_FRAME()
	ENDIF
ENDPROC

PROC PROCESS_SERVER_FARM_MINIMAP()

	VECTOR vInteriorPos = << 2168.09, 2920.89, -85.8005 >>
	VECTOR vExteriorPos = << 2483.0, -404.2, -85.8005 >>
	INT interiorNameHash = 1979383629  //HASH("xm_x17dlc_int_facility2")
	 
	BOOL bShowInterior = TRUE
	BOOL bStaggerChange = (GET_FRAME_COUNT() % 5) = 0
	
	IF IS_PAUSE_MENU_ACTIVE()
		IF NOT IS_PAUSEMAP_IN_INTERIOR_MODE()
			bShowInterior = FALSE
		ENDIF
	ELIF g_PlayerBlipsData.bBigMapIsActive
		bShowInterior = FALSE
	ENDIF
	
	IF bShowInterior
		SET_RADAR_AS_INTERIOR_THIS_FRAME(interiorNameHash,vInteriorPos.x,vInteriorPos.y, 0, 0)	
		HIDE_MINIMAP_EXTERIOR_MAP_THIS_FRAME()
		//PRINTLN("[Minimap] Processing Server Farm Minimap / ", interiorNameHash, " / ", vInteriorPos)
		IF bStaggerChange
			SET_ALL_ENITY_BLIP_DISPLAY(GET_INTERIOR_AT_COORDS(vInteriorPos),DISPLAY_BLIP)
		ENDIF
	ELSE
		SET_FAKE_PAUSEMAP_PLAYER_POSITION_THIS_FRAME(vExteriorPos.x,vExteriorPos.y)
		HIDE_MINIMAP_INTERIOR_MAP_THIS_FRAME()
		IF bStaggerChange
			SET_ALL_ENITY_BLIP_DISPLAY(GET_INTERIOR_AT_COORDS(vInteriorPos),DISPLAY_NOTHING)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_HANGAR_MINIMAP()

	INT iInteriorHash = 638945734
	INT iFloor = 0
	VECTOR vInteriorPos = << -1266.8, -3014.84, -50 >>
	FLOAT fRotation = -0
	
	BOOL bShowInterior = TRUE
	BOOL bStaggerChange = (GET_FRAME_COUNT() % 5) = 0
	
	IF IS_PAUSE_MENU_ACTIVE()
		IF NOT IS_PAUSEMAP_IN_INTERIOR_MODE()
			bShowInterior = FALSE
		ENDIF
	ELIF g_PlayerBlipsData.bBigMapIsActive
		bShowInterior = FALSE
	ENDIF
	
	IF bShowInterior
		SET_RADAR_AS_INTERIOR_THIS_FRAME(iInteriorHash, vInteriorPos.x, vInteriorPos.y, ROUND(fRotation), iFloor)
		SET_RADAR_ZOOM(100)
		HIDE_MINIMAP_EXTERIOR_MAP_THIS_FRAME()
		IF bStaggerChange
			SET_ALL_ENITY_BLIP_DISPLAY(GET_INTERIOR_AT_COORDS(vInteriorPos),DISPLAY_BLIP)
		ENDIF
	ELSE
		HIDE_MINIMAP_INTERIOR_MAP_THIS_FRAME()
		IF bStaggerChange
			SET_ALL_ENITY_BLIP_DISPLAY(GET_INTERIOR_AT_COORDS(vInteriorPos),DISPLAY_NOTHING)
		ENDIF
	ENDIF
	
	PRINTLN("[Minimap] Processing Hangar Minimap / ", iInteriorHash, " / ", vInteriorPos, " / ", fRotation, "  (Hiding exterior map + setting radar zoom to 100)")
ENDPROC

PROC PROCESS_DISPLACEMENT_ARENA_INTERIOR_MC()
	VECTOR vInteriorPos = <<2800.0, -3800.0, 100.0>>
	INT i
	BOOL bShowInterior = TRUE
	BOOL bStaggerChange = (GET_FRAME_COUNT() % 5) = 0
	
	IF IS_PAUSE_MENU_ACTIVE()
		IF NOT IS_PAUSEMAP_IN_INTERIOR_MODE()
			bShowInterior = FALSE
		ENDIF
	ELIF g_PlayerBlipsData.bBigMapIsActive
		bShowInterior = FALSE
	ENDIF
	
	IF bShowInterior
		SET_RADAR_AS_INTERIOR_THIS_FRAME(HASH("xs_x18_int_01"),vInteriorPos.x,vInteriorPos.y, 0, GET_ARENA_ENTITY_SET_LEVEL_NUMBER(g_FMMC_STRUCT.sArenaInfo.iArena_Theme, g_FMMC_STRUCT.sArenaInfo.iArena_Variation))	
		HIDE_MINIMAP_EXTERIOR_MAP_THIS_FRAME()
		IF bStaggerChange
			SET_ALL_ENITY_BLIP_DISPLAY(GET_INTERIOR_AT_COORDS(vInteriorPos),DISPLAY_BLIP)
			
			REPEAT FMMC_MAX_NUM_DYNOPROPS i
				IF NOT DOES_BLIP_EXIST(biTrapCamBlip[i])
					RELOOP
				ENDIF
				SET_BLIP_DISPLAY(biTrapCamBlip[i], DISPLAY_BLIP)
			ENDREPEAT
			
			IF DOES_BLIP_EXIST(biBlipTrapCam)
				SET_BLIP_DISPLAY(biBlipTrapCam, DISPLAY_BLIP)
			ENDIF
			
			IF DOES_BLIP_EXIST(DeliveryBlip)
				SET_BLIP_DISPLAY(DeliveryBlip, DISPLAY_BLIP)
			ENDIF
			
		ENDIF
	ELSE
		SET_FAKE_PAUSEMAP_PLAYER_POSITION_THIS_FRAME(ARENA_X, ARENA_Y)
		HIDE_MINIMAP_INTERIOR_MAP_THIS_FRAME()
		IF bStaggerChange
			SET_ALL_ENITY_BLIP_DISPLAY(GET_INTERIOR_AT_COORDS(vInteriorPos),DISPLAY_NOTHING)
			
			REPEAT FMMC_MAX_NUM_DYNOPROPS i
				IF NOT DOES_BLIP_EXIST(biTrapCamBlip[i])
					RELOOP
				ENDIF
				SET_BLIP_DISPLAY(biTrapCamBlip[i], DISPLAY_NOTHING)
			ENDREPEAT
			
			IF DOES_BLIP_EXIST(biBlipTrapCam)
				SET_BLIP_DISPLAY(biBlipTrapCam, DISPLAY_NOTHING)
			ENDIF
			
			IF DOES_BLIP_EXIST(DeliveryBlip)
				SET_BLIP_DISPLAY(DeliveryBlip, DISPLAY_NOTHING)
			ENDIF
			
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_IPL_MAP_DISPLAY()
		
	//IPL Maps:	
	IF g_FMMC_STRUCT.iIPLOptions > 0
	AND NOT IS_PED_INJURED(PlayerPedToUse)

		VECTOR vPlayer = GET_ENTITY_COORDS(PlayerPedToUse)
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions,IPL_CARRIER)
			PROCESS_IPL_CARRIER_MAP(vPlayer)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions,IPL_YACHT_CHUMASH)
		OR IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions,IPL_YACHT_NEAR_PIER)
		OR IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions,IPL_YACHT_PALETO)
			PROCESS_IPL_YACHT_NEAR_PIER_MAP(vPlayer)
		ENDIF
				
	ENDIF
	
	IF iCurrentHeistMissionIndex = HBCA_BS_HUMANE_LABS_FINALE
		PROCESS_BIOLAB_TUNNEL()
	ENDIF
	
	//Left this here in case we need to do anything with the Morgue interior in the future
//	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_CORONER)
//	AND GET_INTERIOR_FROM_ENTITY(LocalPlayerPed) = GET_INTERIOR_AT_COORDS(<< 234.4, -1355.6, 40.5 >>) // If in the morgue

//	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_HANGAR)
		IF GET_INTERIOR_FROM_ENTITY(LocalPlayerPed) = GET_INTERIOR_AT_COORDS(vHangarInteriorCoords) // If in the hangar 
			PROCESS_HANGAR_MINIMAP()
			IF NOT bIsDisplacedInteriorSet
				bIsDisplacedInteriorSet = TRUE	
				SET_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA(638945734, <<-1174.49,-3457.17,-50>>)
			ENDIF
		ELSE
			IF bIsDisplacedInteriorSet
				bIsDisplacedInteriorSet = FALSE
				CLEAR_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA()
			ENDIF
		ENDIF
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SERVER_FARM)
		IF GET_INTERIOR_FROM_ENTITY(LocalPlayerPed) = GET_INTERIOR_AT_COORDS(<< 2168.09, 2920.89, -85.8005 >>) // If in the Server Farm
			IF NOT bIsDisplacedInteriorSet
				bIsDisplacedInteriorSet = TRUE
				SET_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA(1979383629,<< 2483.0, -404.2, -85.8005 >>)
			ENDIF
			PROCESS_SERVER_FARM_MINIMAP()
		ELSE 
			IF bIsDisplacedInteriorSet
				bIsDisplacedInteriorSet = FALSE
				CLEAR_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA()
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_ARENA)
		IF GET_INTERIOR_FROM_ENTITY(LocalPlayerPed) = g_ArenaInterior
		OR GET_INTERIOR_FROM_ENTITY(LocalPlayerPed) = iArenaInterior_VIPLoungeIndex
		
			IF NOT bIsDisplacedInteriorSet
				bIsDisplacedInteriorSet = TRUE
				SET_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA(NATIVE_TO_INT(g_ArenaInterior),<<ARENA_X, ARENA_Y, ARENA_Z>>)
			ENDIF
			PROCESS_DISPLACEMENT_ARENA_INTERIOR_MC()
		ELSE
			IF bIsDisplacedInteriorSet
				bIsDisplacedInteriorSet = FALSE
				CLEAR_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA()
			ENDIF
		ENDIF
	ENDIF
	
	
	IF (IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_01)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_02)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_03)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_TUNNEL)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_LOOP)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_ENTRANCE)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_BASE))
		VECTOR vPos
		INT interiorNameHash = -1
		INTERIOR_INSTANCE_INDEX playerInterior = GET_INTERIOR_FROM_ENTITY(LocalPlayerPed)
		//Uncomment this if you want to check the position 
		//GET_INTERIOR_LOCATION_AND_NAMEHASH(playerInterior, vPos, interiorNameHash)
		//PRINTLN("[SiloMinimap] hash is ", interiorNameHash, " pos is ", vPos)
		
		IF IS_VALID_INTERIOR(playerInterior)
		AND (playerInterior = GET_INTERIOR_AT_COORDS(<< 244.252, 6163.91, -161.423 >>)// Silo Lab  -635400705 
		 OR playerInterior = GET_INTERIOR_AT_COORDS(<< 446.172, 5922.13, -157.216 >>) // Silo -222705970
		 OR playerInterior = GET_INTERIOR_AT_COORDS(<< 252.062, 5972.12, -159.102 >>) // Silo 1704012289
		 OR playerInterior = GET_INTERIOR_AT_COORDS( << 550.948, 5939.26, -157.216 >>)) // Silo 1914093948
			GET_INTERIOR_LOCATION_AND_NAMEHASH(playerInterior, vPos, interiorNameHash)
			PROCESS_SILO_MINIMAP(vPos,interiorNameHash)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_FOUNDRY)
	AND GET_INTERIOR_FROM_ENTITY(LocalPlayerPed) = GET_INTERIOR_AT_COORDS(<< 1087.14, -1986.68, 31.2089 >>) // If in the foundry
		PROCESS_FOUNDRY_MINIMAP()
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_FOUNDRY)
		SET_RADAR_ZOOM_PRECISE(0)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SUB)
	AND GET_INTERIOR_FROM_ENTITY(LocalPlayerPed) = GET_INTERIOR_AT_COORDS(<< 512.769, 4851.98, -62.9025 >>) //If in the submarine
		PROCESS_SUBMARINE_MINIMAP()
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_IMPORT_WAREHOUSE)
	AND GET_INTERIOR_FROM_ENTITY(LocalPlayerPed) = GET_INTERIOR_AT_COORDS(<<974.9203, -3000.0647, -40.6470>>) //If in the docks import warehouse
		PROCESS_IMPORT_WAREHOUSE()
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_WAREHOUSE_UNDRGRND_FACILITY)
	AND GET_INTERIOR_FROM_ENTITY(LocalPlayerPed) = GET_INTERIOR_AT_COORDS(<<969.5376, -3000.4111, -48.6470>>) //If in the docks import warehouse
		PROCESS_WAREHOUSE_UNDRGRND_FACILITY()
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_WEED_FARM)
		PROCESS_INTERIOR_MINIMAP(HASH("bkr_biker_dlc_int_ware02"), <<1049.6, -3196.6, -38.5>>, 10, -5)
		PROCESS_INTERIOR_DISPLACEMENT(HASH("bkr_biker_dlc_int_ware02"), <<1049.6, -3196.6, -38.5>>, <<996.97, -2546.14, -38.5>>)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_COCAINE_FACTORY)
		PROCESS_INTERIOR_MINIMAP(HASH("bkr_biker_dlc_int_ware03"), <<1093.6, -3196.6, -38.5>>)
		PROCESS_INTERIOR_DISPLACEMENT(HASH("bkr_biker_dlc_int_ware03"), <<1093.6, -3196.6, -38.5>>, <<-1211.71, -1070.31, -38.5>>)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_METH_FACTORY)
		PROCESS_INTERIOR_MINIMAP(HASH("bkr_biker_dlc_int_ware01"), <<1009.5, -3196.6, -38.5>>)
		PROCESS_INTERIOR_DISPLACEMENT(HASH("bkr_biker_dlc_int_ware01"), <<1009.5, -3196.6, -38.5>>, <<359.85, 357.91, -38.5>>)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SPECIAL_CARGO_WAREHOUSE_L)
		PROCESS_INTERIOR_MINIMAP(HASH("ex_int_warehouse_l_dlc"), <<1010.0083, -3100.0000, -39.9999>>)
		PROCESS_INTERIOR_DISPLACEMENT(HASH("ex_int_warehouse_l_dlc"), <<1010.0083, -3100.0000, -39.9999>>, <<1186.61, -1394.78, -39.9>>)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_CAR_PARK)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_APARTMENT)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_GARAGE)
		PROCESS_CASINO_BLIPS()
	ENDIF
	
	IF (CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE() OR IS_FAKE_MULTIPLAYER_MODE_SET())
		PROCESS_CASINO_ROOF_MINIMAP()
	ENDIF
	
	IF (IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_MAIN)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_TUNNEL)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_BACK_AREA)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_HOTEL_FLOOR)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_LOADING_BAY)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_VAULT)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_UTILITY_LIFT)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_LIFT_SHAFT))
		PROCESS_CASINO_HEIST_MINIMAP()
	ENDIF
ENDPROC

/// PURPOSE: This function will find and return the first unused array position in the iWaterCalmingQuad array of water calming quads.
///    If a value of -1 is returned, there are no free entries in the array and we have reached the maximum number of water calming quads.
///    This value is currently set at 8 which is a script limit, but the code limit on the number of quads is also 8.
FUNC INT GET_FREE_WATER_CALMING_QUAD()
	
	INT iQuadNum = -1
	
	INT iQuad
	
	FOR iQuad = 0 TO (FMMC_MAX_WATER_CALMING_QUADS - 1)
		IF iWaterCalmingQuad[iQuad] = -1
			iQuadNum = iQuad
			iQuad = FMMC_MAX_WATER_CALMING_QUADS
		ENDIF
	ENDFOR
	
	RETURN iQuadNum
	
ENDFUNC

///PURPOSE: Adds areas where the water is calmed, so the waves don't clip through IPL things on water (like the yacht / carrier).
///    Creator-placed water calming quad zones are handled elsewhere (in the function CREATE_RESTRICTION_ZONES() in the 
///    FM_Mission_Controller_Zones header).
PROC PROCESS_WATER_CALMING_QUADS_FOR_IPLS()
	
	INT iQuad = -1
	
	IF g_FMMC_STRUCT.iIPLOptions > 0
		
		/*IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions,IPL_CARRIER)
		AND NOT IS_BIT_SET(iLocalBoolCheck7, LBOOL7_WATER_CALMING_QUAD_CARRIER_CREATED)
			
			iQuad = GET_FREE_WATER_CALMING_QUAD()
			
			IF iQuad > -1
			AND iQuad < FMMC_MAX_WATER_CALMING_QUADS
			AND iWaterCalmingQuad[iQuad] = -1
				iWaterCalmingQuad[iQuad] = ADD_EXTRA_CALMING_QUAD(3000,-4837,3120,-4508,0.02)
				PRINTLN("[RCC MISSION] PROCESS_WATER_CALMING_QUADS - Adding calming quad to Carrier, calming quad index ",iQuad)
			ELSE
				CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION] PROCESS_WATER_CALMING_QUADS, Carrier - Trying to create too many water dampening zones! There is a maximum of 8.")
				PRINTLN("[RCC MISSION] PROCESS_WATER_CALMING_QUADS, Carrier - Trying to create too many water dampening zones! There is a maximum of 8.")
			ENDIF
			
			SET_BIT(iLocalBoolCheck7, LBOOL7_WATER_CALMING_QUAD_CARRIER_CREATED)
		ENDIF*/
		
		IF ( IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions,IPL_YACHT_CHUMASH)
			 OR IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions,IPL_YACHT_NEAR_PIER) 
			 OR IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions,IPL_YACHT_PALETO))
		AND NOT IS_BIT_SET(iLocalBoolCheck7, LBOOL7_WATER_CALMING_QUAD_YACHT_CREATED)
			
			iQuad = GET_FREE_WATER_CALMING_QUAD()
			
			IF iQuad > -1
			AND iQuad < FMMC_MAX_WATER_CALMING_QUADS
			AND iWaterCalmingQuad[iQuad] = -1
				iWaterCalmingQuad[iQuad] = ADD_EXTRA_CALMING_QUAD(-2125,-1049,-2013,1002,0.02)
				PRINTLN("[RCC MISSION] PROCESS_WATER_CALMING_QUADS - Adding calming quad to Yacht, calming quad index ",iQuad)
			ELSE
				CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION] PROCESS_WATER_CALMING_QUADS, Yacht - Trying to create too many water dampening zones! There is a maximum of 8.")
				PRINTLN("[RCC MISSION] PROCESS_WATER_CALMING_QUADS, Yacht - Trying to create too many water dampening zones! There is a maximum of 8.")
			ENDIF
			
			SET_BIT(iLocalBoolCheck7, LBOOL7_WATER_CALMING_QUAD_YACHT_CREATED)
		ENDIF
		
	ENDIF
	
ENDPROC

PROC PROCESS_MAP_AND_RADAR()
	
	PROCESS_IPL_MAP_DISPLAY()
	
	PROCESS_WATER_CALMING_QUADS_FOR_IPLS()
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
	AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitset[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE_HIDE_RADAR)
		
		IF NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_HIDE_RADAR)
			DISPLAY_RADAR(FALSE)
			SET_BIT(iLocalBoolCheck6, LBOOL6_HIDE_RADAR)
		ENDIF
		
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_HIDE_RADAR)
			DISPLAY_RADAR(TRUE)
			CLEAR_BIT(iLocalBoolCheck6, LBOOL6_HIDE_RADAR)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck28, LBBOOL28_HIDE_RADAR_THIS_FRAME)
		IF iRadarHiddenFrame >= GET_FRAME_COUNT()
			DISPLAY_RADAR(FALSE)
		ELSE
			CLEAR_BIT(iLocalBoolCheck28, LBBOOL28_HIDE_RADAR_THIS_FRAME)
			DISPLAY_RADAR(TRUE)
		ENDIF
	ENDIF
	
ENDPROC

PROC CONTROL_RADAR_ZOOM(BOOL bInstant = FALSE)

FLOAT fZoomLevel

	IF NOT bInstant
		IF fOldFurthestTargetDist > fFurthestTargetDist
			IF (fOldFurthestTargetDist - fFurthestTargetDist) > 5
				fOldFurthestTargetDist = (fOldFurthestTargetDist - cfRADAR_ZOOM_SPEED)
			ENDIF
		ELSE
			IF (fFurthestTargetDist - fOldFurthestTargetDist) > 5
				fOldFurthestTargetDist = (fOldFurthestTargetDist + cfRADAR_ZOOM_SPEED)
			ENDIF
		ENDIF
		fZoomLevel = (fOldFurthestTargetDist/cfRADAR_ZOOM_ADJUST)
		
		// Cap the zoom level
		IF (fZoomLevel >= cfRADAR_ZOOM_CAP)
			fZoomLevel = cfRADAR_ZOOM_CAP
		ELIF (fZoomLevel < cfRADAR_ZOOM_CAP_MIN)
			fZoomLevel = cfRADAR_ZOOM_CAP_MIN
		ENDIF

		// Allow debug tweak of radar zoom 
		// Script/ FreeMode/Les Widgets/Radar Zoom /Radar Zoom 
		#IF IS_DEBUG_BUILD
		IF (f_i_RadarZoom > -1.0)
			SET_RADAR_ZOOM_TO_DISTANCE(f_i_RadarZoom)
		ELSE
		#ENDIF
		//PRINTLN("SET_RADAR_ZOOM_TO_DISTANCE : ",fZoomLevel) 
		IF DO_RADAR_ZOOM()
			SET_RADAR_ZOOM_TO_DISTANCE(fZoomLevel)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
	ELSE
		fOldFurthestTargetDist = fFurthestTargetDist
		fFurthestTargetDistTemp = fFurthestTargetDist
		fZoomLevel = (fOldFurthestTargetDist/cfRADAR_ZOOM_ADJUST)
		//PRINTLN("SET_RADAR_ZOOM_TO_DISTANCE : ",fZoomLevel)
		IF DO_RADAR_ZOOM()
			SET_RADAR_ZOOM_TO_DISTANCE(fZoomLevel)
		ENDIF
	ENDIF

ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: DUMMY BLIPS !
//
//************************************************************************************************************************************************************

FUNC BOOL IS_DUMMY_BLIP_WITHIN_SPECIFIED_RULES()

	// If there is a 'Blip From' rule set
	IF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].sDummyBlipBlipData.sTeamBlip[0].iBlipOverrideStartRule = -2 // [ML] -2 = active on any rule	
		RETURN TRUE
	ELIF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].sDummyBlipBlipData.sTeamBlip[0].iBlipOverrideStartRule <= MC_ServerBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].iTeam]
	AND g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].sDummyBlipBlipData.sTeamBlip[0].iBlipOverrideStartRule != -1
		IF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].sDummyBlipBlipData.sTeamBlip[0].iBlipOverrideEndRule >= MC_ServerBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].iTeam]			
		OR g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].sDummyBlipBlipData.sTeamBlip[0].iBlipOverrideEndRule = -1
			RETURN TRUE
		ENDIF
	ENDIF

	// If there is a specified single rule set
	IF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].iRule = -2
		RETURN TRUE
	ELIF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].iRule = MC_ServerBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].iTeam]
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC BOOL SHOULD_DISPLAY_DUMMY_BLIP_FOR_TEAM(INT iDummyBlip,INT iteam)

	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iBitSet, ciDUMMY_BLIP_HIDE_IN_GUN_TURRET)
		IF g_iInteriorTurretSeat > -1
			SET_BIT(iLocalBoolCheck27, LBOOL27_HAVE_HIDDEN_DUMMY_BLIPS)
			RETURN FALSE
		ENDIF
	ENDIF
	
	SWITCH iteam
	
		CASE 0
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iBitSet,ciDUMMY_BLIP_SHOW_FOR_TEAM1)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 1
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iBitSet,ciDUMMY_BLIP_SHOW_FOR_TEAM2)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 2
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iBitSet,ciDUMMY_BLIP_SHOW_FOR_TEAM3)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 3
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iBitSet,ciDUMMY_BLIP_SHOW_FOR_TEAM4)
				RETURN TRUE
			ENDIF
		BREAK
	
	ENDSWITCH

RETURN FALSE

ENDFUNC

FUNC VECTOR GET_DUMMY_BLIP_LOCATION(INT iDummyBlip, INT& iVehicle, INT &iPed)
	
	VECTOR vBlip = <<0,0,0>>
	iVehicle = -1
	iPed = -1
	
	IF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iType = ciFMMC_DROP_OFF_TYPE_CYLINDER
	OR g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iType = ciFMMC_DROP_OFF_TYPE_AREA
    	vBlip = g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].vPos
	ELIF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iType = ciFMMC_DROP_OFF_TYPE_PROPERTY
		vBlip = GET_PROPERTY_DROP_OFF_COORDS( eApartmentBuzzerEntrance )
	ELIF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iType = ciFMMC_DROP_OFF_TYPE_LEADER_GARAGE
		vBlip = GET_PROPERTY_DROP_OFF_COORDS( eGarageExterior )
	ELIF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iType = ciFMMC_DROP_OFF_TYPE_PED
		IF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iEntityToUse > -1
			NETWORK_INDEX niPed = MC_serverBD_1.sFMMC_SBD.niPed[ g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iEntityToUse ]
			IF NETWORK_DOES_NETWORK_ID_EXIST(niPed)
			AND IS_ENTITY_ALIVE(NET_TO_PED(niPed))
				iPed = g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iEntityToUse
			ENDIF
		ENDIF
	ELSE //It's a vehicle:
		IF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iEntityToUse > -1
			NETWORK_INDEX niVeh = MC_serverBD_1.sFMMC_SBD.niVehicle[ g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iEntityToUse ]
			IF NETWORK_DOES_NETWORK_ID_EXIST(niVeh)
			AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(niVeh))
				iVehicle = g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iEntityToUse
			ENDIF
		ENDIF
	ENDIF
	
	RETURN vBlip
	
ENDFUNC

PROC PROCESS_EVERY_FRAME_DUMMY_BLIPS()
	
	BOOL bDummyBlipSet = FALSE
	INT iDummyBlip 
	BOOL bShouldBlipBeDisplayed = TRUE
	
	FOR iDummyBlip = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfDummyBlips - 1
		IF DOES_BLIP_EXIST(DummyBlip[iDummyBlip])
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iBitSet, ciDUMMY_BLIP_OVERRIDE_QUICK_GPS)
				SET_BIT(iLocalBoolCheck26, LBOOL26_DUMMY_BLIP_OVERRIDE)
				iDummyBlipCurrentOverride = iDummyBlip
				bDummyBlipSet = TRUE
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].iBitset, ciDUMMY_BLIP_DISPLAY_ONLY_WHEN_OOB)		
				PRINTLN("[ML] ciDUMMY_BLIP_DISPLAY_ONLY_WHEN_OOB is SET")
						
				IF HAS_NET_TIMER_STARTED(MC_playerBD[iPartToUse].tdBoundstimer)
					CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] index ", iDummyBlipProgress, " Player is OUT of PRIMARY bounds. Showing Dummy Blip.")
					bShouldBlipBeDisplayed = TRUE
				ELIF HAS_NET_TIMER_STARTED(MC_playerBD[iPartToUse].tdBounds2timer)
					CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] index ", iDummyBlipProgress, " Player is OUT of SECONDARY bounds. Showing Dummy Blip.")
					bShouldBlipBeDisplayed = TRUE
				ELSE
					CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] index ", iDummyBlipProgress, " Player is IN PRIMARY or SECONDARY bounds. Hiding Dummy Blip.")
					bShouldBlipBeDisplayed = FALSE
				ENDIF										
			ENDIF
			
			VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
			VECTOR vDummyBlipPos = GET_BLIP_COORDS(DummyBlip[iDummyBlip])
			FLOAT fHeightDifference = ABSF(vDummyBlipPos.z - vPlayerPos.z)
			
			PRINTLN("[ML][PROCESS_EVERY_FRAME_DUMMY_BLIPS] Blip: ", iDummyBlip, "   Distance between player and blip is : ", VDIST_2D(vDummyBlipPos, vPlayerPos), "    Height distance between player and blip is: ", fHeightDifference)
			
			// Both 'show blip in range' and 'show blip in height range' are set
			IF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].sDummyBlipBlipData.fBlipRange != 0.0
			AND g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].sDummyBlipBlipData.fBlipHeightDifference != 0.0
				PRINTLN("[ML][PROCESS_EVERY_FRAME_DUMMY_BLIPS] Blip: ", iDummyBlip, " - Both show range and show height are set")
				IF VDIST_2D(vDummyBlipPos, vPlayerPos) < POW(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].sDummyBlipBlipData.fBlipRange, 2)
					IF fHeightDifference < g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].sDummyBlipBlipData.fBlipHeightDifference
						bShouldBlipBeDisplayed = TRUE
					ELSE
						bShouldBlipBeDisplayed = FALSE
					ENDIF
				ELSE
					bShouldBlipBeDisplayed = FALSE
				ENDIF
				
			// Just 'show blip in range' is set
			ELIF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].sDummyBlipBlipData.fBlipRange != 0.0
				PRINTLN("[ML][PROCESS_EVERY_FRAME_DUMMY_BLIPS] Blip: ", iDummyBlip, " - Just show range is set")
				IF VDIST_2D(vDummyBlipPos, vPlayerPos) < POW(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].sDummyBlipBlipData.fBlipRange, 2)
					bShouldBlipBeDisplayed = TRUE
				ELSE
					bShouldBlipBeDisplayed = FALSE
				ENDIF
			
			// Just 'show blip in height range' is set
			ELIF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].sDummyBlipBlipData.fBlipHeightDifference != 0.0
				PRINTLN("[ML][PROCESS_EVERY_FRAME_DUMMY_BLIPS] Blip: ", iDummyBlip, " - Just show height is set")
				IF fHeightDifference < g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].sDummyBlipBlipData.fBlipHeightDifference
					bShouldBlipBeDisplayed = TRUE
				ELSE
					bShouldBlipBeDisplayed = FALSE
				ENDIF
			
			ELSE
				// Neither 'show blip in range' or 'show blip in height range' are set
				PRINTLN("[ML][PROCESS_EVERY_FRAME_DUMMY_BLIPS] Blip: ", iDummyBlip, " - Neither show range or height are set")
			ENDIF
			
			// 'Hide blip in range' is set
			IF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].fHideBlipRange != 0.0
				PRINTLN("[ML][PROCESS_EVERY_FRAME_DUMMY_BLIPS] Blip: ", iDummyBlip, " - Hide in range is set")
				IF VDIST_2D(vDummyBlipPos, vPlayerPos) < POW(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].fHideBlipRange, 2)
					PRINTLN("[ML] fHideBlipRange = ", g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].fHideBlipRange, "   ", VDIST2(vDummyBlipPos, vPlayerPos), " < ", POW(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].fHideBlipRange, 2))		
					bShouldBlipBeDisplayed = FALSE
				ENDIF		
			ENDIF
			
			IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_CASINO_HEIST_MISSION_CONTROLLER_BLIPS_HIDDEN)
				PRINTLN("[NA][PROCESS_EVERY_FRAME_DUMMY_BLIPS] Blip: ", iDummyBlip, " - LBOOL32_CASINO_HEIST_MISSION_CONTROLLER_BLIPS_HIDDEN is set.")
				bShouldBlipBeDisplayed = FALSE
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iBitSet, ciDUMMY_BLIP_HIDE_BLIP_AFTER_OUTFIT_SWAP)
				PRINTLN("[ML][PROCESS_EVERY_FRAME_DUMMY_BLIPS] ciDUMMY_BLIP_HIDE_BLIP_AFTER_OUTFIT_SWAP is SET for blip: ", iDummyBlip)
				IF IS_BIT_SET(iLocalBoolCheck31, LBOOL31_OUTFIT_CHANGED_THROUGH_INTERACT)
					PRINTLN("[ML][PROCESS_EVERY_FRAME_DUMMY_BLIPS] LBOOL31_OUTFIT_CHANGED_THROUGH_INTERACT is SET, so hide dummy blip: ", iDummyBlip)	
					bShouldBlipBeDisplayed = FALSE
				ENDIF
			ENDIF
			
			IF bShouldBlipBeDisplayed = TRUE
				PRINTLN("[ML] Blip ", iDummyBlip, " should be displayed")
				SET_BLIP_DISPLAY(DummyBlip[iDummyBlip], DISPLAY_BLIP)
			ELSE
				PRINTLN("[ML] Blip ", iDummyBlip, " should not be displayed")
				SET_BLIP_DISPLAY(DummyBlip[iDummyBlip], DISPLAY_NOTHING)
			ENDIF	
			
		ENDIF
	ENDFOR
	
	IF NOT bDummyBlipSet
	AND IS_BIT_SET(iLocalBoolCheck26, LBOOL26_DUMMY_BLIP_OVERRIDE)
		iDummyBlipCurrentOverride = -1
		CLEAR_BIT(iLocalBoolCheck26, LBOOL26_DUMMY_BLIP_OVERRIDE)
	ENDIF	
ENDPROC

PROC MANAGE_DUMMY_BLIPS()
	
	BOOL bCheck, bBlip, bBlipSpecialConditions = TRUE
	VECTOR vBlip
	
	INT iVehicle = -1
	INT iPed = -1
	
	PROCESS_EVERY_FRAME_DUMMY_BLIPS()
	
	//Team loop:
	bCheck = DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME(TRUE)
	
	IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_NEED_TO_CHECK_IN_VEH_FOR_DUMMY_BLIPS)
	AND !bCheck
	AND NOT IS_PED_INJURED(PlayerPedToUse)
		
		BOOL bInVeh = IS_PED_IN_ANY_VEHICLE(PlayerPedToUse)
		
		IF IS_BIT_SET(iLocalBoolCheck11, LBOOL11_WAS_IN_VEH_FOR_DUMMY_BLIPS)
			IF NOT bInVeh
				CLEAR_BIT(iLocalBoolCheck11, LBOOL11_WAS_IN_VEH_FOR_DUMMY_BLIPS)
				bCheck = TRUE
			ENDIF
		ELSE
			IF bInVeh
				SET_BIT(iLocalBoolCheck11, LBOOL11_WAS_IN_VEH_FOR_DUMMY_BLIPS)
				bCheck = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF g_iInteriorTurretSeat > -1
	OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_PLAYER_IS_USING_SCRIPTED_GUN_CAM)
	OR IS_BIT_SET(iLocalBoolCheck27, LBOOL27_HAVE_HIDDEN_DUMMY_BLIPS)
		bCheck = TRUE
	ENDIF
	
	IF bCheck
		iDummyBlipToReach = iDummyBlipProgress
		CLEAR_BIT(iLocalBoolCheck11, LBOOL11_NEED_TO_CHECK_IN_VEH_FOR_DUMMY_BLIPS_TEMP)
	ENDIF
	
	IF iDummyBlipToReach != -1
		
		iDummyBlipProgress++
		
		IF iDummyBlipProgress >= g_FMMC_STRUCT_ENTITIES.iNumberOfDummyBlips
			iDummyBlipProgress = 0
		ENDIF
		
		IF NOT DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(iDummyBlipProgress, MC_serverBD_4.rsgSpawnSeed, eSGET_Dummyblip)
			CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][MANAGE_DUMMY_BLIPS] iDummyBlipProgress ", iDummyBlipProgress, " Does not have a suitable spawn group - exiting")
			EXIT
		ELSE
			CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][MANAGE_DUMMY_BLIPS] iDummyBlipProgress ", iDummyBlipProgress, " Has a valid spawn group")
		ENDIF
		
		vBlip = GET_DUMMY_BLIP_LOCATION(iDummyBlipProgress, iVehicle, iPed)
		
		IF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].iEntityType > -1
		AND g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].iEntityIndex > -1		
			SET_BIT(iLocalBoolCheck30, LBOOL30_DUMMY_BLIPS_NEED_CONSTANTLY_UPDATING)
			CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] index ", iDummyBlipProgress, " Checking Entity Type: ", g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].iEntityType," With Entity Index: ", g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].iEntityIndex)			
			BOOL bExists
			SWITCH g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].iEntityType
				CASE ciENTITY_TYPE_PED
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].iEntityIndex])
						PED_INDEX pedIndex
						pedIndex = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].iEntityIndex])
						IF NOT IS_PED_INJURED(pedIndex)
							CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] index ", iDummyBlipProgress, " Ped Exists...")
							bExists = TRUE
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] index ", iDummyBlipProgress, " Ped Does not Exist...")
					ENDIF
				BREAK
				CASE ciENTITY_TYPE_VEHICLE
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].iEntityIndex])
						VEHICLE_INDEX vehIndex
						vehIndex = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].iEntityIndex])
						IF IS_VEHICLE_DRIVEABLE(vehIndex)
							CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] index ", iDummyBlipProgress, " Vehicle Exists...")
							bExists = TRUE
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] index ", iDummyBlipProgress, " Vehicle Does not Exist...")
					ENDIF
				BREAK
				CASE ciENTITY_TYPE_OBJECT	
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niObject[g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].iEntityIndex])
						CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] index ", iDummyBlipProgress, " Object Exists...")
						bExists = TRUE
					ELSE
						CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] index ", iDummyBlipProgress, " Object Does not Exist...")
					ENDIF
				BREAK
				CASE ciENTITY_TYPE_GOTO
					/* [ML] Commented out for now, but if we need to do much more to dummy blips we'll want to create a proper init routine
					CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] index ", iDummyBlipProgress, " iLcoate: ", g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].iEntityIndex, " iRuleType: ", MC_serverBD_4.iGotoLocationDataRule[g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].iEntityIndex][MC_playerBD[iLocalPart].iteam])
					
					IF MC_serverBD_4.iGotoLocationDataRule[g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].iEntityIndex][MC_playerBD[iLocalPart].iteam] = FMMC_OBJECTIVE_LOGIC_GO_TO					
						IF MC_serverBD_4.iGotoLocationDataPriority[g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].iEntityIndex][MC_playerBD[iLocalPart].iteam] < FMMC_MAX_RULES
							CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] index ", iDummyBlipProgress, " Locate Exists...")
							bExists = TRUE
						ELSE
							CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] index ", iDummyBlipProgress, " Locate Does not Exist...")						
						ENDIF
					ENDIF*/
				BREAK
				CASE ciENTITY_TYPE_KILL_PLAYER
					
				BREAK
				CASE ciENTITY_TYPE_LAST_PLAYER
				
				BREAK
				CASE ciENTITY_TYPE_ZONES
					
				BREAK
			ENDSWITCH
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].iBitset, ciDUMMY_BLIP_HIDE_WHEN_ENTITY_LINK_EXISTS)
			AND bExists
				CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] index ", iDummyBlipProgress, " ciDUMMY_BLIP_HIDE_WHEN_ENTITY_LINK_EXISTS Set and entity exists. Hiding Blip.")
				bBlipSpecialConditions = FALSE
			ENDIF
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].iBitset, ciDUMMY_BLIP_HIDE_WHEN_ENTITY_LINK_NOT_EXISTS)				
			AND NOT bExists
				CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] index ", iDummyBlipProgress, " ciDUMMY_BLIP_HIDE_WHEN_ENTITY_LINK_NOT_EXISTS Set and entity does not exist exists. Hiding Blip.")
				bBlipSpecialConditions = FALSE
			ENDIF
		ENDIF
		IF NOT IS_PED_INJURED(localPlayerPed)
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].iBitset, ciDUMMY_BLIP_HIDE_WHEN_IN_INTERIOR)			
				IF GET_INTERIOR_FROM_ENTITY(localPlayerPed) != NULL
					CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] index ", iDummyBlipProgress, " ciDUMMY_BLIP_HIDE_WHEN_IN_INTERIOR Set and Player is in an interior. Hiding Blip.")
					bBlipSpecialConditions = FALSE
				ENDIF
			ENDIF
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].iBitset, ciDUMMY_BLIP_HIDE_WHEN_NOT_IN_INTERIOR)
				IF GET_INTERIOR_FROM_ENTITY(localPlayerPed) = NULL
					CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] index ", iDummyBlipProgress, " ciDUMMY_BLIP_HIDE_WHEN_NOT_IN_INTERIOR Set and Player is NOT in an interior. Hiding Blip.")
					bBlipSpecialConditions = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		IF ((NOT IS_VECTOR_ZERO(vBlip)) OR (iPed > -1) OR (iVehicle > -1))
		AND g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].iTeam != -1
			IF IS_DUMMY_BLIP_WITHIN_SPECIFIED_RULES()
				IF SHOULD_DISPLAY_DUMMY_BLIP_FOR_TEAM(iDummyBlipProgress, MC_playerBD[iPartToUse].iteam)
				AND bBlipSpecialConditions
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].iBitSet, ciDUMMY_BLIP_MIDPOINT)
						IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].iTeam], g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].iRule)
							
							bBlip = TRUE
							
							IF NOT DOES_BLIP_EXIST(DummyBlip[iDummyBlipProgress])
								IF iVehicle > -1
									CDEBUG2LN(DEBUG_MC_BLIP, "[RCC MISSION] Removing any existing blips on vehicle ", iVehicle, " ahead of dummy blip creation.")
									REMOVE_ANY_EXISTING_BLIP_FROM_ENTITY(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVehicle]))
									
									CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] Adding midpoint dummy blip at index ",iDummyBlipProgress, " for vehicle ", iVehicle, ".")
									DummyBlip[iDummyBlipProgress] = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVehicle]))
								ELIF iPed > -1
									PRINTLN("[MMacK][DummyBlip] Adding ped dummy blip 1")
									CLEANUP_AI_PED_BLIP(biHostilePedBlip[iPed])
									REMOVE_ANY_EXISTING_BLIP_FROM_ENTITY(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[ iPed ]))
									
									DummyBlip[iDummyBlipProgress] = ADD_BLIP_FOR_ENTITY(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[ iPed ]))
								ELSE
								 	CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] Adding midpoint dummy blip at index ", iDummyBlipProgress, " for coord ", vBlip, ".")
									DummyBlip[iDummyBlipProgress] = ADD_BLIP_FOR_COORD(vBlip)
								ENDIF
								
								IF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].sDummyBlipBlipData.iBlipSpriteOverride != 0
									CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] index ", iDummyBlipProgress, " Setting custom sprite as i: ", g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].sDummyBlipBlipData.iBlipSpriteOverride)
									SET_BLIP_SPRITE(DummyBlip[iDummyBlipProgress], GET_DUMMY_BLIP_SPRITE_FROM_SELECTION(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].sDummyBlipBlipData.iBlipSpriteOverride))
								ENDIF
								
								SET_BLIP_COLOUR(DummyBlip[iDummyBlipProgress], GET_BLIP_COLOUR_FROM_CREATOR(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].sDummyBlipBlipData.iBlipColour))
								SET_BLIP_SCALE(DummyBlip[iDummyBlipProgress], GET_BLIP_SIZE_FROM_CREATOR(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].iBlipSize))
								
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].iBitSet, ciDUMMY_BLIP_HIDE_BLIP_HEIGHT_INDICATOR)
									CDEBUG1LN(DEBUG_MC_BLIP, "[ML] ciDUMMY_BLIP_HIDE_BLIP_HEIGHT_INDICATOR is NOT SET for blip: ", iDummyBlipProgress)
									SHOW_HEIGHT_ON_BLIP(DummyBlip[iDummyBlipProgress], FALSE)
								ENDIF
								
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].iBitSet, ciDUMMY_BLIP_GPS_ROUTE) // creator option to enable routes
									SET_BLIP_ROUTE(DummyBlip[iDummyBlipProgress], TRUE)
									SET_BLIP_ROUTE_COLOUR(DummyBlip[iDummyBlipProgress], GET_BLIP_COLOUR_FROM_CREATOR(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].sDummyBlipBlipData.iBlipColour))
								ENDIF
								
								IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].tlDBName)
									BEGIN_TEXT_COMMAND_SET_BLIP_NAME("MCUSTBLIP")
										ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].tlDBName)
									END_TEXT_COMMAND_SET_BLIP_NAME(DummyBlip[iDummyBlipProgress])
								ENDIF	
								
								SET_BLIP_PRIORITY(DummyBlip[iDummyBlipProgress], BLIPPRIORITY_HIGHEST)
							ENDIF
						ENDIF
					ELSE
						bBlip = TRUE
						
						IF NOT DOES_BLIP_EXIST(DummyBlip[iDummyBlipProgress])
							IF iVehicle > -1
								CDEBUG2LN(DEBUG_MC_BLIP, "[RCC MISSION] Removing any existing blips on vehicle ", iVehicle, " ahead of dummy blip creation.")
								REMOVE_ANY_EXISTING_BLIP_FROM_ENTITY(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVehicle]))
								
								CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] Adding non-midpoint dummy blip at index ", iDummyBlipProgress, " for vehicle ", iVehicle, ".")
								DummyBlip[iDummyBlipProgress] = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVehicle]))
							ELIF iPed > -1
								PRINTLN("[MMacK][DummyBlip] Adding ped dummy blip 2")
								CLEANUP_AI_PED_BLIP(biHostilePedBlip[iPed])
								REMOVE_ANY_EXISTING_BLIP_FROM_ENTITY(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[ iPed ]))
								
								DummyBlip[iDummyBlipProgress] = ADD_BLIP_FOR_ENTITY(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[ iPed ]))
							ELSE
								CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] Adding non-midpoint dummy blip at index ", iDummyBlipProgress, " for coord ", vBlip, ".")
								DummyBlip[iDummyBlipProgress] = ADD_BLIP_FOR_COORD(vBlip)
							ENDIF
							
							IF g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].sDummyBlipBlipData.iBlipSpriteOverride != 0
								CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] index ", iDummyBlipProgress, " Setting custom sprite as i: ", g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].sDummyBlipBlipData.iBlipSpriteOverride)
								SET_BLIP_SPRITE(DummyBlip[iDummyBlipProgress], GET_DUMMY_BLIP_SPRITE_FROM_SELECTION(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].sDummyBlipBlipData.iBlipSpriteOverride))
							ENDIF
							
							SET_BLIP_COLOUR(DummyBlip[iDummyBlipProgress], GET_BLIP_COLOUR_FROM_CREATOR(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].sDummyBlipBlipData.iBlipColour))
							SET_BLIP_SCALE(DummyBlip[iDummyBlipProgress], GET_BLIP_SIZE_FROM_CREATOR(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].iBlipSize))
							
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].iBitSet, ciDUMMY_BLIP_HIDE_BLIP_HEIGHT_INDICATOR)
								CDEBUG1LN(DEBUG_MC_BLIP, "[ML] ciDUMMY_BLIP_HIDE_BLIP_HEIGHT_INDICATOR is NOT SET for blip: ", iDummyBlipProgress)
								SHOW_HEIGHT_ON_BLIP(DummyBlip[iDummyBlipProgress], FALSE)
							ENDIF
							
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].iBitSet, ciDUMMY_BLIP_GPS_ROUTE) // creator option to enable routes
								SET_BLIP_ROUTE(DummyBlip[iDummyBlipProgress], TRUE)
								SET_BLIP_ROUTE_COLOUR(DummyBlip[iDummyBlipProgress], GET_BLIP_COLOUR_FROM_CREATOR(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].sDummyBlipBlipData.iBlipColour))
							ENDIF
							
							IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].tlDBName)
								BEGIN_TEXT_COMMAND_SET_BLIP_NAME("MCUSTBLIP")
									ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].tlDBName)
								END_TEXT_COMMAND_SET_BLIP_NAME(DummyBlip[iDummyBlipProgress])
							ENDIF	
							
							SET_BLIP_PRIORITY(DummyBlip[iDummyBlipProgress], BLIPPRIORITY_HIGHEST)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		IF NOT bBlip
			IF DOES_BLIP_EXIST(DummyBlip[iDummyBlipProgress])
				CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] Removing dummy blip at index ", iDummyBlipProgress, ".")
				REMOVE_BLIP(DummyBlip[iDummyBlipProgress])
			ENDIF
		ELSE
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlipProgress].iBitSet, ciDUMMY_BLIP_HIDE_IN_VEHICLE) 
				
				IF (NOT IS_PED_INJURED(LocalPlayerPed))
				AND IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					IF DOES_BLIP_EXIST(DummyBlip[iDummyBlipProgress])
						CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] Removing dummy blip at index ", iDummyBlipProgress, ".")
						REMOVE_BLIP(DummyBlip[iDummyBlipProgress])
					ENDIF
				ENDIF
				
				SET_BIT(iLocalBoolCheck11, LBOOL11_NEED_TO_CHECK_IN_VEH_FOR_DUMMY_BLIPS_TEMP)
			ENDIF
		ENDIF
		
		CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] iDummyBlipProgress: ", iDummyBlipProgress, " iDummyBlipToReach: ", iDummyBlipToReach)
		
		IF iDummyBlipProgress = iDummyBlipToReach
			IF NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_DUMMY_BLIPS_NEED_CONSTANTLY_UPDATING)
				iDummyBlipToReach = -1 // We're done with the loop
			ENDIF
			
			IF IS_BIT_SET(iLocalBoolCheck11, LBOOL11_NEED_TO_CHECK_IN_VEH_FOR_DUMMY_BLIPS_TEMP)
				SET_BIT(iLocalBoolCheck10, LBOOL10_NEED_TO_CHECK_IN_VEH_FOR_DUMMY_BLIPS)
			ELSE
				CLEAR_BIT(iLocalBoolCheck10, LBOOL10_NEED_TO_CHECK_IN_VEH_FOR_DUMMY_BLIPS)
			ENDIF
		ENDIF
		
	ENDIF

ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: DROP-OFF BLIPPING !
//
//************************************************************************************************************************************************************



FUNC BOOL HAS_CHANGED_DELIVERY_BLIP_SPRITE()

	BLIP_SPRITE CorrectBlipSprite
	// should we change the sprite?
	CorrectBlipSprite = GET_CORRECT_PED_BLIP_SPRITE_FOR_VEHICLE_MODEL(GET_ENTITY_MODEL(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iDeliveryBlip_veh])))	
	IF NOT (CorrectBlipSprite = GET_STANDARD_BLIP_ENUM_ID())
	AND NOT (GET_BLIP_SPRITE(DeliveryBlip) = CorrectBlipSprite)
	
		CDEBUG3LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] HAS_CHANGED_DELIVERY_BLIP_SPRITE -  changing to sprite blip ", GET_BLIP_SPRITE_DEBUG_STRING(CorrectBlipSprite), " this iDeliveryBlip_veh ", iDeliveryBlip_veh, ".")

		// if we change the sprite we need to re-apply the settings.
		SET_BLIP_SPRITE(DeliveryBlip, CorrectBlipSprite)
		SET_CUSTOM_BLIP_NAME_FROM_MODEL(DeliveryBlip, GET_ENTITY_MODEL(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iDeliveryBlip_veh])))
		RETURN(TRUE)
	ENDIF
	
	RETURN(FALSE)

ENDFUNC

///PURPOSE: This function removes the blip for get & deliver
PROC REMOVE_DELIVERY_BLIP()
	IF DOES_BLIP_EXIST(DeliveryBlip)
		REMOVE_BLIP(DeliveryBlip)
		//CLEAR_BIT(iLocalBoolCheck3,LBOOL3_ROUTE_SET) //The only check for this is commented out, so it probably isn't needed any more - removing to make room in the bitset
		CLEAR_BIT( iLocalBoolCheck5, LBOOL5_GPS_DELIVER_ROUTE_SET )
		IF IS_BIT_SET( iLocalBoolCheck9, LBOOL9_GPS_MULTI_ROUTE_ACTIVE ) 
			CLEAR_GPS_MULTI_ROUTE()
			SET_GPS_MULTI_ROUTE_RENDER(FALSE)
			CLEAR_BIT( iLocalBoolCheck9, LBOOL9_GPS_MULTI_ROUTE_ACTIVE )
		ENDIF
		
		// url:bugstar:2087082
		IF IS_BIT_SET( iLocalBoolCheck9, LBOOL9_SET_IGNORE_NO_GPS_FLAG )
			SET_IGNORE_NO_GPS_FLAG(FALSE)
			PRINTLN("[RCC MISSION] REMOVE_DELIVERY_BLIP called SET_IGNORE_NO_GPS_FLAG FALSE")
			CLEAR_BIT( iLocalBoolCheck9, LBOOL9_SET_IGNORE_NO_GPS_FLAG )
		ENDIF
	ENDIF
	iDeliveryBlip_veh = -1
ENDPROC

FUNC INT GET_PLAYER_CARRY_COUNT()

INT icarrycount

	IF MC_playerBD[iPartToUse].iPedCarryCount > 0
		icarrycount = MC_playerBD[iPartToUse].iPedCarryCount
	ENDIF
	IF MC_playerBD[iPartToUse].iObjCarryCount > 0
		icarrycount = MC_playerBD[iPartToUse].iObjCarryCount
	ENDIF
	IF MC_playerBD[iPartToUse].iVehCarryCount > 0
		icarrycount = MC_playerBD[iPartToUse].iVehCarryCount
	ENDIF

	RETURN icarrycount
	
ENDFUNC

///PURPOSE: This function adds a blip in for a get & deliver
///    bBlipChange will refresh the blip (remove and re-add on the next call)
PROC ADD_DELIVERY_BLIP( BOOL bBlipChange = FALSE )

	iDeliveryBlip_veh = -1

	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitSetSix[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE6_HIDE_DROPOFF_BLIP)
			IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_OBJECTIVE_BLOCKER)
				IF NOT DOES_BLIP_EXIST(DeliveryBlip)
						
					VECTOR vDropoff = GET_DROP_OFF_CENTER(TRUE)
					
					IF NOT IS_VECTOR_ZERO(vDropoff)
						IF (g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iDropOffType[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] != ciFMMC_DROP_OFF_TYPE_VEHICLE)
							IF NOT DOES_BLIP_EXIST(DeliveryBlip)
								DeliveryBlip = ADD_BLIP_FOR_COORD(vDropoff)
							ENDIF
							PRINTLN("[RCC MISSION] Adding delivery blip at coords: ",vDropoff)
							SET_BLIP_PRIORITY(DeliveryBlip,BLIPPRIORITY_OVER_CENTRE_BLIP)
							iDelblipPriority = mc_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
							
							IF Is_Player_Currently_On_MP_CTF_Mission(PlayerToUse)
								SET_BLIP_FLASHES(DeliveryBlip,TRUE)
								PRINTLN("[RCC MISSION] setting drop off blip alpha to 0 for CTF")
								SET_BLIP_ALPHA(DeliveryBlip,0)
							ELSE
								IF GET_PLAYER_CARRY_COUNT() > 0
								OR (IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER) OR (MC_playerBD[iPartToUse].iVehNear != -1))
									IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iDropOffGPSBitSet, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
									AND NOT USING_HEIST_SPECTATE()
										PRINTLN("[MMacK][GPSRoute] 1")
										SET_BLIP_ROUTE(DeliveryBlip,TRUE)
										PRINTLN("[RCC MISSION] setting player drop off route")
									ENDIF
									//SET_BIT(iLocalBoolCheck3,LBOOL3_ROUTE_SET) //The only check for this is commented out, so it probably isn't needed any more - removing to make room in the bitset
								ELSE
									PRINTLN("[RCC MISSION] setting drop off blip alpha")
									SET_BLIP_ALPHA(DeliveryBlip,150)
								ENDIF
							ENDIF
							
							// url:bugstar:2087082
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetThree[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE3_IGNORE_GPS_BLOCKING)
								SET_IGNORE_NO_GPS_FLAG(TRUE)
								PRINTLN("[RCC MISSION] ADD_DELIVERY_BLIP called SET_IGNORE_NO_GPS_FLAG TRUE ", vDropoff)
								SET_BIT(iLocalBoolCheck9, LBOOL9_SET_IGNORE_NO_GPS_FLAG)
							ENDIF
							
							// url:bugstar:2068322 & url:bugstar:2080423
							IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iDropOffType[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] = ciFMMC_DROP_OFF_TYPE_LEADER_GARAGE
							AND NOT USING_HEIST_SPECTATE()
								INT iPropertyIndex = -1
								IF IS_FAKE_MULTIPLAYER_MODE_SET()
									iPropertyIndex = 1
								ELSE
									iPropertyIndex = MC_ServerBD.iHeistPresistantPropertyID
								ENDIF
								
								IF iPropertyIndex != -1
									IF mpProperties[iPropertyIndex].iBuildingID = MP_PROPERTY_BUILDING_2
									OR mpProperties[iPropertyIndex].iBuildingID = MP_PROPERTY_BUILDING_4
										PRINTLN("[RCC MISSION] - ADD_DELIVERY_BLIP - Setting up GPS Multi Route for garage")
										START_GPS_MULTI_ROUTE(HUD_COLOUR_OBJECTIVE_ROUTE, TRUE)
										SET_BLIP_ROUTE(DeliveryBlip,FALSE)
										IF mpProperties[iPropertyIndex].iBuildingID = MP_PROPERTY_BUILDING_2
											ADD_POINT_TO_GPS_MULTI_ROUTE(<<-251.3913, -1006.3646, 28.0086>>)
										ELSE
											ADD_POINT_TO_GPS_MULTI_ROUTE(<<-914.8209, -486.2174, 35.5672>>)
										ENDIF
										
										SET_BIT( iLocalBoolCheck9, LBOOL9_GPS_MULTI_ROUTE_ACTIVE )
										SET_GPS_MULTI_ROUTE_RENDER(TRUE)
									ENDIF
								ENDIF
							ENDIF
							
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitSetSix[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE6_DROP_OFF_HOUSE_BLIP)
								SET_BLIP_SPRITE(DeliveryBlip,RADAR_TRACE_CAPTURE_THE_FLAG_BASE)
								SET_BLIP_COLOUR(DeliveryBlip,BLIP_COLOUR_YELLOW)
								SET_BLIP_NAME_FROM_TEXT_FILE(DeliveryBlip,"BLIP_0")
							ENDIF
							
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitSetTen[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE10_USE_TEAM_COLOUR_ON_DELIVERY_BLIP)
								SET_BLIP_COLOUR(DeliveryBlip, GET_INT_FROM_HUD_COLOUR(GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_playerBD[iPartToUse].iteam, PlayerToUse)))	
							ENDIF
							
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetThirteen[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE13_DISABLE_DELIVERY_BLIP_HEIGHT_INDICATOR)
								PRINTLN("[MJL] About to set show height on blip to FALSE")
								SHOW_HEIGHT_ON_BLIP(DeliveryBlip, FALSE)
							ELSE
								PRINTLN("[MJL] About to set show height on blip to TRUE")
								SHOW_HEIGHT_ON_BLIP(DeliveryBlip, TRUE)
							ENDIF
							
						ELSE //We're dropping off at a vehicle
							
							INT iVeh = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iDropOff_Vehicle[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]]
							
							IF iVeh > -1
							AND (NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]))
							AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]))
							
								IF NOT IS_BIT_SET(iVehBlipCreatedThisFrameBitset, iVeh)
									CDEBUG2LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH]  Removing any existing blips on vehicle ", iVeh, " ahead of delivery blip creation.")
									REMOVE_ANY_EXISTING_BLIP_FROM_ENTITY(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]))
									
									IF DOES_BLIP_EXIST(biVehBlip[iVeh])
										CDEBUG2LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH]  Veh: ", iVeh, " still has a blip, removing it")
										REMOVE_BLIP(biVehBlip[iVeh])
									ENDIF
									
									//CDEBUG2LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH]  Removing any AI blips on vehicle ", iVeh, " ahead of delivery blip creation.")
									//CLEANUP_AI_VEHICLE_BLIPS_FOR_VEHICLE(iVeh)
									IF NOT DOES_BLIP_EXIST(DeliveryBlip)
										DeliveryBlip = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]))
									ENDIF
									iDeliveryBlip_veh = iVeh
									
									IF HAS_CHANGED_DELIVERY_BLIP_SPRITE()
										CDEBUG3LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] changed delivery blip to a sprite ", iVeh, " this frame (", GET_FRAME_COUNT(), ").")
									ENDIF
									
									//Blocks another kind of vehicle blip attempting to be made on this vehicle this frame.
									CDEBUG3LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Flagging to block any more new blips being created on veh ", iVeh, " this frame (", GET_FRAME_COUNT(), ").")
									SET_BIT(iVehBlipCreatedThisFrameBitset, iVeh)
									
									SET_BLIP_COLOUR_FROM_HUD_COLOUR(DeliveryBlip,HUD_COLOUR_BLUEDARK)
									SET_BLIP_PRIORITY(DeliveryBlip,BLIPPRIORITY_OVER_CENTRE_BLIP)
									SET_BIT( iLocalBoolCheck5, LBOOL5_GPS_DELIVER_ROUTE_SET )	// url:bugstar:2195395
									iDelblipPriority = mc_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
								ELSE
									CDEBUG3LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Blocking vehicle delivery blip creation this frame as another blip has just been made on the vehicle.")
								ENDIF
								
							ENDIF
							
						ENDIF
					ENDIF
				ELSE // Else the blip exists
					IF iDelblipPriority != mc_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
					//OR ( (NOT IS_BIT_SET(iLocalBoolCheck3,LBOOL3_ROUTE_SET))
					//	 AND ( ( IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER) OR (MC_playerBD[iPartToUse].iVehNear != -1) ) ) )
					//OR (iOldCarryCount = 0 AND GET_PLAYER_CARRY_COUNT() > 0)
					OR bBlipChange
					OR MPGlobalsAmbience.R2Pdata.bOnRaceToPoint != bonImpromptuRace
						PRINTLN("[RCC MISSION] removing drop off blip")
						REMOVE_DELIVERY_BLIP()
						//iOldCarryCount = GET_PLAYER_CARRY_COUNT()
						iDelblipPriority = mc_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
						bonImpromptuRace = MPGlobalsAmbience.R2Pdata.bOnRaceToPoint 
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF iDelblipPriority != mc_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
			OR bBlipChange
				PRINTLN("[RCC MISSION] Current blip hidden, removing previous")
				REMOVE_DELIVERY_BLIP()
				iDelblipPriority = mc_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
			ENDIF
		ENDIF
	ENDIF

ENDPROC

///PURPOSE: This function is used to clear GPS when a vehicle get & deliver rule progresses
///    to "help your team-mates deliver the vehicle"
FUNC BOOL SHOULD_CLEAR_DELIVERY_BLIP_ON_SECONDARY_OBJECTIVE_TEXT()
	BOOL bReturn = FALSE
	
	INT iTeam = MC_playerBD[ iPartToUse ].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[ iTeam ]

	IF GET_MC_CLIENT_MISSION_STAGE( iPartToUse ) = CLIENT_MISSION_STAGE_COLLECT_VEH
		IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitset[ iRule ], ciBS_RULE_HIDE_GPS_ON_SECONDARY )
		AND MC_serverBD.iNumVehHighestPriorityHeld[ iTeam ] >= MC_serverBD.iNumVehHighestPriority[ iTeam ]				
		AND ( (g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffType[ iRule ] != ciFMMC_DROP_OFF_TYPE_VEHICLE )
			 OR ( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOff_Vehicle[ iRule ] = -1 ) )
			bReturn = TRUE
		ENDIF
	ENDIF
	
	RETURN bReturn
ENDFUNC

PROC UPDATE_DELIVERY_BLIP_SPRITE()

	IF (iDeliveryBlip_veh > -1)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iDeliveryBlip_veh])
			IF DOES_ENTITY_EXIST(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iDeliveryBlip_veh]))		
			
				// should we change the sprite?
				IF HAS_CHANGED_DELIVERY_BLIP_SPRITE()
					// if we change the sprite we need to re-apply the settings, as basically it's a new blip that gets created.

					//Blocks another kind of vehicle blip attempting to be made on this vehicle this frame.
					CDEBUG3LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] UPDATE_DELIVERY_BLIP_SPRITE -  Flagging to block any more new blips being created on veh ", iDeliveryBlip_veh, " this frame (", GET_FRAME_COUNT(), ").")
					SET_BIT(iVehBlipCreatedThisFrameBitset, iDeliveryBlip_veh)
					
					SET_BLIP_COLOUR_FROM_HUD_COLOUR(DeliveryBlip,HUD_COLOUR_BLUEDARK)
					SET_BLIP_PRIORITY(DeliveryBlip,BLIPPRIORITY_OVER_CENTRE_BLIP)
				ENDIF

				// Should this blip be rotated?
				IF SHOULD_BLIP_BE_MANUALLY_ROTATED(DeliveryBlip)
					SET_BLIP_ROTATION(DeliveryBlip, ROUND(GET_ENTITY_HEADING_FROM_EULERS(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iDeliveryBlip_veh]))))
				ENDIF
			
			ELSE
				iDeliveryBlip_veh = -1		
			ENDIF
		ELSE
			iDeliveryBlip_veh = -1
		ENDIF
	ENDIF
	
ENDPROC

///PURPOSE: This function will turn GPS on and off for the delivery blip
///    depending on the "GPS Clear Within" option. This option defaults to "always on" 
///    which will force the blip to always have a GPS route every frame.
///    
///    NB:	If iDistanceToCheck = -1, there's no GPS
///    		IF iDistanceToCheck = 0, there's always GPS
///    		IF iDistanceToCheck > 0, that's the distance in meters at which the GPS is cleared
PROC MANAGE_DELIVERY_BLIP()

	INT iCurrentTeam = MC_playerBD[ iPartToUse ].iteam
	INT iCurrentRule = mc_serverBD_4.iCurrentHighestPriority[ iCurrentTeam ]
	
	IF iCurrentRule < FMMC_MAX_RULES
		INT iDistanceToCheck 	= g_FMMC_STRUCT.sFMMCEndConditions[ iCurrentTeam ].iGPSDrop[ iCurrentRule ]
		
		// url:bugstar:2068322 & url:bugstar:2080423
		IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iDropOffType[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] = ciFMMC_DROP_OFF_TYPE_LEADER_GARAGE
		AND NOT USING_HEIST_SPECTATE()
		AND iDistanceToCheck <= 0
			INT iPropertyIndex = -1
			IF IS_FAKE_MULTIPLAYER_MODE_SET()
				iPropertyIndex = 1
			ELSE
				iPropertyIndex = MC_ServerBD.iHeistPresistantPropertyID
			ENDIF
			
			IF iPropertyIndex != -1
				IF mpProperties[iPropertyIndex].iBuildingID = MP_PROPERTY_BUILDING_2
					iDistanceToCheck = 35
				ELIF mpProperties[iPropertyIndex].iBuildingID = MP_PROPERTY_BUILDING_4
					iDistanceToCheck = 45
				ENDIF
			ENDIF
		ENDIF

		IF DOES_BLIP_EXIST( DeliveryBlip )
			IF iDistanceTocheck = -1 		// -1 means "always turn GPS off"
			OR SHOULD_CLEAR_DELIVERY_BLIP_ON_SECONDARY_OBJECTIVE_TEXT()
			//OR iLocalGPSZonesCurrentlyOccupied > 0 // If this bit is >0 it means that one or more GPS-free zones are occupied
				println("[MJM] iDistanceTocheck = -1 ")
				CLEAR_BIT( iLocalBoolCheck5, LBOOL5_GPS_DELIVER_ROUTE_SET )	// Make sure this is clear so it doesn't effect any other deliver rules
				SET_BLIP_ROUTE( DeliveryBlip, FALSE )
		
			ELIF iDistanceTocheck = 0		// 0 means "always leave the GPS on". This is the default of this setting
			AND NOT IS_BIT_SET( iLocalBoolCheck5, LBOOL5_GPS_DELIVER_ROUTE_SET ) 
			AND NOT USING_HEIST_SPECTATE()	
				println("[MJM] iDistanceTocheck = 0 ")
				PRINTLN("[MMacK][GPSRoute] 2")
				SET_BLIP_ROUTE( DeliveryBlip, TRUE )
				SET_BIT( iLocalBoolCheck5, LBOOL5_GPS_DELIVER_ROUTE_SET )
			
			ELIF iDistanceToCheck > 0		// Otherwise the value is the min dist between the team and dest where the GPS should turn off
				println("[MJM] iDistanceToCheck > 0 ")
				PED_INDEX tempped = PlayerPedToUse
				
				FLOAT fDistancecToDestination = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD( tempped, GET_DROP_OFF_CENTER_FOR_TEAM( FALSE, iCurrentTeam, iPartToUse ) )
				
				IF FLOOR( fDistancecToDestination ) < iDistanceToCheck
					println("[MJM] FLOOR( fDistancecToDestination ) < iDistanceToCheck ")
					CLEAR_BIT( iLocalBoolCheck5, LBOOL5_GPS_DELIVER_ROUTE_SET )
					SET_BLIP_ROUTE( DeliveryBlip, FALSE )
					
					// url:bugstar:2068322 & url:bugstar:2080423
					IF IS_BIT_SET( iLocalBoolCheck9, LBOOL9_GPS_MULTI_ROUTE_ACTIVE )
						CLEAR_GPS_MULTI_ROUTE()
						SET_GPS_MULTI_ROUTE_RENDER(FALSE)
						PRINTLN("[RCC MISSION] - MANAGE_DELIVERY_BLIP - Clearing GPS Multi Route for garage")
						CLEAR_BIT( iLocalBoolCheck9, LBOOL9_GPS_MULTI_ROUTE_ACTIVE )
					ENDIF
					
				ELIF NOT IS_BIT_SET( iLocalBoolCheck5, LBOOL5_GPS_DELIVER_ROUTE_SET ) // Ensure setting the rotue only gets called once
				AND NOT USING_HEIST_SPECTATE()
					println("[MJM] NOT IS_BIT_SET( iLocalBoolCheck5, LBOOL5_GPS_DELIVER_ROUTE_SET )")
					PRINTLN("[MMacK][GPSRoute] 3")
					SET_BLIP_ROUTE( DeliveryBlip, TRUE )
					SET_BIT( iLocalBoolCheck5, LBOOL5_GPS_DELIVER_ROUTE_SET )
					
					// url:bugstar:2068322 & url:bugstar:2080423
					IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iDropOffType[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] = ciFMMC_DROP_OFF_TYPE_LEADER_GARAGE
					AND NOT IS_BIT_SET( iLocalBoolCheck9, LBOOL9_GPS_MULTI_ROUTE_ACTIVE )
						INT iPropertyIndex = -1
						IF IS_FAKE_MULTIPLAYER_MODE_SET()
							iPropertyIndex = 1
						ELSE
							iPropertyIndex = MC_ServerBD.iHeistPresistantPropertyID
						ENDIF
						
						IF iPropertyIndex != -1
							IF mpProperties[iPropertyIndex].iBuildingID = MP_PROPERTY_BUILDING_2
							OR mpProperties[iPropertyIndex].iBuildingID = MP_PROPERTY_BUILDING_4
								PRINTLN("[RCC MISSION] - MANAGE_DELIVERY_BLIP - Setting up GPS Multi Route for garage")
								START_GPS_MULTI_ROUTE(HUD_COLOUR_OBJECTIVE_ROUTE, TRUE)
								SET_BLIP_ROUTE(DeliveryBlip,FALSE)
								IF mpProperties[iPropertyIndex].iBuildingID = MP_PROPERTY_BUILDING_2
									ADD_POINT_TO_GPS_MULTI_ROUTE(<<-251.3913, -1006.3646, 28.0086>>)
								ELSE
									ADD_POINT_TO_GPS_MULTI_ROUTE(<<-914.8209, -486.2174, 35.5672>>)
								ENDIF
								
								SET_BIT( iLocalBoolCheck9, LBOOL9_GPS_MULTI_ROUTE_ACTIVE )
								SET_GPS_MULTI_ROUTE_RENDER(TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF // End of the GPS route type check 
			
			// if the delivery blip is on a vehicle check if it's got the correct sprite and correct heading etc.			
			UPDATE_DELIVERY_BLIP_SPRITE()
			
		ENDIF // End of the check to see if the blip exists
	ENDIF // End of the check to make sure we only check the rules that exist - array overrun otherwise
ENDPROC

PROC CLEANUP_OBJECTIVE_BLIPS()
	
	INT i
	
	INT iBlipColour
	BOOL bCustomBlip
	FOR i = 0 TO (FMMC_MAX_PEDS-1)
		IF DOES_BLIP_EXIST(biPedBlip[i])
			IF NOT SHOULD_PED_HAVE_BLIP(i,iBlipColour,bCustomBlip)
				REMOVE_BLIP(biPedBlip[i])
				PRINTLN("[RCC MISSION] CLEANUP_OBJECTIVE_BLIPS removing ped: ",i)
			ENDIF
		ENDIF
	ENDFOR
	
	FOR i = 0 TO (FMMC_MAX_VEHICLES-1)
		IF DOES_BLIP_EXIST(biVehBlip[i])
		AND NOT SHOULD_VEHICLE_HAVE_CUSTOM_BLIP(i)
			REMOVE_VEHICLE_BLIP(i)
			PRINTLN("[RCC MISSION] CLEANUP_OBJECTIVE_BLIPS removing veh: ",i) 
		ENDIF
	ENDFOR
	
	FOR i = 0 TO (FMMC_MAX_NUM_OBJECTS-1)
		REMOVE_OBJECT_BLIP(i)
	ENDFOR
	
	FOR i = 0 TO (FMMC_MAX_NUM_DYNOPROPS-1)
		REMOVE_DYNOPROP_BLIP(i)
	ENDFOR
	CLEAR_FAKE_CONE_ARRAY()
	iCCTVConeBitSet = 0
	
	FOR i = 0 TO (FMMC_MAX_GO_TO_LOCATIONS -1)
		IF DOES_BLIP_EXIST(LocBlip[i])
			REMOVE_BLIP(LocBlip[i])
		ENDIF
	ENDFOR
	
	IF DOES_BLIP_EXIST(DeliveryBlip)
		REMOVE_DELIVERY_BLIP()
	ENDIF

ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: TEXT MESSAGES !
//
//************************************************************************************************************************************************************



FUNC BOOL SHOULD_SEND_TEXT()

	IF IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_HEIST_ANY_PARTICIPANT_INSIDE_ANY_MP_PROPERTY)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC


FUNC BOOL SHOULD_TEXT_MESSAGE_TRIGGER_FROM_DIALOGUE_TRIGGER(INT iSMS)
	BOOL bDTsendsSMS = FALSE
	IF g_FMMC_STRUCT.sSMS[iSMS].iTriggerFromDT != -1
		IF IS_LONG_BIT_SET(iLocalDialoguePlayingBS, g_FMMC_STRUCT.sSMS[iSMS].iTriggerFromDT)
			PRINTLN("[ML][SHOULD_TEXT_MESSAGE_TRIGGER_FROM_DIALOGUE_TRIGGER] Setting should dialogue trigger send SMS to TRUE")
			bDTsendsSMS = TRUE
		ENDIF
	ENDIF
	RETURN bDTsendsSMS
ENDFUNC

//TODO(Tom): improve this function - make it so the timer and the iLocalTextMessagePriority work with more than one text (NG)
PROC PROCESS_TEXT_MESSAGES()
	
	enumCharacterList tempContact
	INT i,inumstring
	BOOL bSendText

	IF SHOULD_SEND_TEXT()
		IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_FINISHED)	
		AND NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_START_SPECTATOR)
			FOR i = 0 TO (FMMC_MAX_NUM_SMS-1)
				IF NOT IS_BIT_SET(iMyLocalSentTextMsgBitset, i)
					IF g_FMMC_STRUCT.sSMS[i].iTeam	= MC_playerBD[iPartToUse].iteam
					OR g_FMMC_STRUCT.sSMS[i].iSendToTeams = FMMC_SMS_SEND_TO_ALL
						IF g_FMMC_STRUCT.sSMS[i].iTeam > -1
							IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT.sSMS[i].iTeam]  < FMMC_MAX_RULES
								IF (MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT.sSMS[i].iTeam] = g_FMMC_STRUCT.sSMS[i].iRule
								AND (g_FMMC_STRUCT.sSMS[i].iPointsRequired = -1 OR (MC_serverBD.iTeamScore[g_FMMC_STRUCT.sSMS[i].iTeam] >= g_FMMC_STRUCT.sSMS[i].iPointsRequired)))	
								OR SHOULD_TEXT_MESSAGE_TRIGGER_FROM_DIALOGUE_TRIGGER(i)
									PRINTLN("[RCC MISSION] [sc_txtMsg] PROCESS_TEXT_MESSAGES - iPointsRequired: ", g_FMMC_STRUCT.sSMS[i].iPointsRequired, " iTeamScore: ", MC_serverBD.iTeamScore[g_FMMC_STRUCT.sSMS[i].iTeam])
									IF g_FMMC_STRUCT.sSMS[i].iTiming = ciFMMC_SMS_MID_RULE
										IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[g_FMMC_STRUCT.sSMS[i].iTeam],MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT.sSMS[i].iTeam])
											IF g_FMMC_STRUCT.sSMS[i].iDelaySeconds = 0
												PRINTLN("[RCC MISSION] [sc_txtMsg] PROCESS_TEXT_MESSAGES - bSendText 1")
												bSendText = TRUE
											ELSE
												IF NOT HAS_NET_TIMER_STARTED(tdTextDelay[i])
													REINIT_NET_TIMER(tdTextDelay[i])
												ELSE
													IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdTextDelay[i]) > (1000*g_FMMC_STRUCT.sSMS[i].iDelaySeconds)
														PRINTLN("[RCC MISSION] [sc_txtMsg] PROCESS_TEXT_MESSAGES - bSendText 2")
														bSendText = TRUE
													ENDIF
												ENDIF
											ENDIF	
										ENDIF
									ELSE
										IF g_FMMC_STRUCT.sSMS[i].iDelaySeconds = 0
											PRINTLN("[RCC MISSION] [sc_txtMsg] PROCESS_TEXT_MESSAGES - bSendText 3")
											bSendText = TRUE
										ELSE
											IF NOT HAS_NET_TIMER_STARTED(tdTextDelay[i])
												REINIT_NET_TIMER(tdTextDelay[i])
											ELSE
												IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ResetSMSTimerAtStartOfEachRule)
													IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iPartToUse].iteam)
														RESET_NET_TIMER(tdTextDelay[i])
														RELOOP
														PRINTLN("[RCC MISSION] [sc_txtMsg] PROCESS_TEXT_MESSAGES - Resetting Text timer due to rule progression")
													ENDIF
												ENDIF
												IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdTextDelay[i]) > (1000*g_FMMC_STRUCT.sSMS[i].iDelaySeconds)
													PRINTLN("[RCC MISSION] [sc_txtMsg] PROCESS_TEXT_MESSAGES - bSendText 4")
													bSendText = TRUE
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									
									IF bSendText
									
										PRINTLN("[RCC MISSION] [sc_txtMsg] PROCESS_TEXT_MESSAGES - MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT.sSMS[i].iTeam] = g_FMMC_STRUCT.sSMS[i].iRule,   MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT.sSMS[i].iTeam] = ", MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT.sSMS[i].iTeam], "  g_FMMC_STRUCT.sSMS[i].iRule = ",  g_FMMC_STRUCT.sSMS[i].iRule, " g_FMMC_STRUCT.sSMS[i].iTeam = ", g_FMMC_STRUCT.sSMS[i].iTeam, " i =" , i )
										
										INT iTempBS = 0
										SET_BIT(iTempBS, MP_COMMS_MODIFIER_IGNORE_RECENT_INVITE)
										IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciSMS_CONTACT_UNKNOWN)
											tempContact = CHAR_ARTHUR
										ELIF g_FMMC_STRUCT.sSMS[i].iContactCharacter != -1
											tempContact = GET_INCOMING_CONTACT_CHARACTER_FROM_CREATOR(g_FMMC_STRUCT.sSMS[i].iContactCharacter)
										ELSE
											tempContact = Get_enumCharacterList_From_ContactAsInt(g_FMMC_STRUCT.iContactCharEnum)
										ENDIF
										
										IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sSMS[i].tl63Message[0])
											inumstring++
										ENDIF
										IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sSMS[i].tl63Message[1])
											inumstring++
										ENDIF
										//IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sSMS[i].tl63Message[2])
										//	inumstring++
										//ENDIF
										
										PRINTLN("[RCC MISSION] [sc_txtMsg] PROCESS_TEXT_MESSAGES - Sending text... inumstring: ", inumstring, " iTempBS: ", iTempBS)
										
										IF inumstring = 1
											IF Request_MP_Comms_Txtmsg_With_Components(tempContact,"MIS_CUST_TXT2", " ", TRUE,NO_INT_SUBSTRING_COMPONENT_VALUE,g_FMMC_STRUCT.sSMS[i].tl63Message[0], DEFAULT, DEFAULT, iTempBS)
												SET_BIT(iMyLocalSentTextMsgBitset, i)
												PRINTLN("[RCC MISSION] [sc_txtMsg] PROCESS_TEXT_MESSAGES - Sending text... inumstring = 1 Text message number ", i )
												RESET_NET_TIMER(tdTextDelay[i])
												bSendText = FALSE
											ENDIF
										ELIF inumstring = 2
											IF Request_MP_Comms_Txtmsg_With_Components(tempContact,"MIS_CUST_TXT3", " ", TRUE,NO_INT_SUBSTRING_COMPONENT_VALUE,g_FMMC_STRUCT.sSMS[i].tl63Message[0],g_FMMC_STRUCT.sSMS[i].tl63Message[1], DEFAULT, iTempBS)
												SET_BIT(iMyLocalSentTextMsgBitset, i)
												PRINTLN("[RCC MISSION] [sc_txtMsg] PROCESS_TEXT_MESSAGES - Sending text... inumstring = 2 Text message number", i )
												RESET_NET_TIMER(tdTextDelay[i])
												bSendText = FALSE
											ENDIF
										//ELIF inumstring = 3
										//	IF Request_MP_Comms_Txtmsg_With_Components(tempContact,"MIS_CUST_TXT3",g_FMMC_STRUCT.sSMS[i].tl63Message[0],TRUE,NO_INT_SUBSTRING_COMPONENT_VALUE, g_FMMC_STRUCT.sSMS[i].tl63Message[1],g_FMMC_STRUCT.sSMS[i].tl63Message[2])
										//		iLocalTextMessagePriority = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] 
										//	ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
	ENDIF
ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: RACE-STYLE COUNTDOWN UI !
//
//************************************************************************************************************************************************************



CONST_INT ciSYNC_RACE_COUNTDOWN_TIMER 3500

PROC REQUEST_COUNTDOWN_UI(MISSION_INTRO_COUNTDOWN_UI &uiToRequest)
	uiToRequest.uiCountdown = REQUEST_SCALEFORM_MOVIE("COUNTDOWN")
ENDPROC

PROC RELEASE_COUNTDOWN_UI(MISSION_INTRO_COUNTDOWN_UI &uiToRelease)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iTeamBitset2, ciBS2_STOP_GO_HUD)
	AND HAS_SCALEFORM_MOVIE_LOADED(uiToRelease.uiCountdown)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(uiToRelease.uiCountdown)
		PRINTLN("RELEASE_COUNTDOWN_UI")
	ENDIF
	//RELEASE_SCRIPT_AUDIO_BANK() COMMENTED OUT BECAUSE OF: url:bugstar:3171853
	//RELEASE_NAMED_SCRIPT_AUDIO_BANK("HUD_321_GO")
	PRINTLN("RELEASE_NAMED_SCRIPT_AUDIO_BANK - HUD_321_GO Called from fm_mission_controller_hud.sch 1")
ENDPROC

PROC CLEAN_COUNTDOWN(MISSION_INTRO_COUNTDOWN_UI &uiToUpdate)
	uiToUpdate.iBitFlags = 0
	CANCEL_TIMER(uiToUpdate.CountdownTimer)
ENDPROC

FUNC BOOL IS_COUNTDOWN_OK_TO_PROCEED() 
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_GAMES_MASTERS(g_FMMC_STRUCT.iAdversaryModeType)
		IF IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_ALL_PLAYERS_RENDERPHASES_PAUSED)
		AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_FINISHED_COUNTDOWN)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_FINISHED_COUNTDOWN)

		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciRACE_STYLE_INTRO)		
			PRINTLN("[MMacK][321][IS_COUNTDOWN_OK_TO_PROCEED] ciRACE_STYLE_INTRO SET")
			IF bIsLocalPlayerHost
			AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_STARTED_COUNTDOWN)

				INT iPlayer = 0
				FOR iPlayer = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
					IF INT_TO_NATIVE(PLAYER_INDEX, iPlayer) != INVALID_PLAYER_INDEX()
					AND NETWORK_IS_PLAYER_ACTIVE(INT_TO_NATIVE(PLAYER_INDEX, iPlayer))
					AND NOT IS_PLAYER_USING_ACTIVE_ROAMING_SPECTATOR_MODE(INT_TO_PLAYERINDEX(iPlayer))
						IF MC_playerBD[iLocalPart].iGameState < GAME_STATE_RUNNING
							PRINTLN("[MMacK][321][IS_COUNTDOWN_OK_TO_PROCEED] - Waiting for iPlayer: ", iPlayer, " | ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iPlayer)))
							RETURN FALSE
						ENDIF
					ENDIF
				ENDFOR
			
				SCRIPT_TIMER sTimerTemp
			
				IF NOT HAS_NET_TIMER_STARTED(sTimerTemp)
					START_NET_TIMER(sTimerTemp) 
				ENDIF
				
				REINIT_NET_TIMER(MC_serverBD.timeServerCountdown)
					
				MC_serverBD.timeServerCountdown = GET_NET_TIMER_OFFSET(sTimerTemp, ciSYNC_RACE_COUNTDOWN_TIMER)
				
				SET_BIT(MC_serverBD.iServerBitSet2, SBBOOL2_STARTED_COUNTDOWN)
				
				PRINTLN("[MMacK][321][IS_COUNTDOWN_OK_TO_PROCEED] Setting SBBOOL2_STARTED_COUNTDOWN")
			ENDIF
			
			IF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_STARTED_COUNTDOWN)
				
				SCRIPT_TIMER sTimerTemp2
				
				IF NOT HAS_NET_TIMER_STARTED(sTimerTemp2)
					START_NET_TIMER(sTimerTemp2) 
					PRINTLN("[MMacK][321][IS_COUNTDOWN_OK_TO_PROCEED], START_NET_TIMER")
				ENDIF
				
				// Reinit multi rule timer...
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_EnableGameMastersMode)	
					IF bIsLocalPlayerHost					
					AND IS_BIT_SET(MC_serverBD.iServerBitSet7, SBBOOL7_MULTIRULE_TIMER_REINIT)
						IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.timeServerCountdown) > -500
							PRINTLN("[LM][321][IS_COUNTDOWN_OK_TO_PROCEED] - ciOptionsBS22_EnableGameMastersMode Resetting Multi Rule Timers for Game Masters Mode.")
							RESET_NET_TIMER(MC_serverBD_3.tdMultiObjectiveLimitTimer[0])
							RESET_NET_TIMER(MC_serverBD_3.tdMultiObjectiveLimitTimer[1])
							START_NET_TIMER(MC_serverBD_3.tdMultiObjectiveLimitTimer[0])
							START_NET_TIMER(MC_serverBD_3.tdMultiObjectiveLimitTimer[1])
							CLEAR_BIT(MC_serverBD.iServerBitSet7, SBBOOL7_MULTIRULE_TIMER_REINIT)
						ELSE
							PRINTLN("[LM][321][IS_COUNTDOWN_OK_TO_PROCEED] - ciOptionsBS22_EnableGameMastersMode Waiting for 1 second on countdown.")
						ENDIF
					ENDIF
					IF IS_BIT_SET(MC_serverBD.iServerBitSet7, SBBOOL7_MULTIRULE_TIMER_REINIT)
						PRINTLN("[LM][321][IS_COUNTDOWN_OK_TO_PROCEED] - Clearing Sounds.")
						CLEAR_BIT(iLocalBoolCheck30, LBOOL30_PLAYED_30s_COUNTDOWN_FOR_ARENA_WARS)
						CLEAR_BIT(iLocalBoolCheck30, LBOOL30_PLAYED_ARENA_WARS_AUDIO_TRACK)
						CLEAR_BIT(iLocalBoolCheck3, LBOOL3_MUSIC_PLAYING)
						CLEAR_BIT(iLocalBoolCheck14, LBOOL14_APT_STARTED_PRE_COUNTDOWN)
						CLEAR_BIT(iLocalBoolCheck5, LBOOL5_STARTED_COUNTDOWN_TIMER_SOUNDS)
						CLEAR_BIT(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC)
						CLEAR_BIT(iLocalBoolCheck14, LBOOL14_COUNTDOWN_EARLY_MISSION_END)	
					ENDIF
					
					IF CONTENT_IS_USING_ARENA()
						IF NOT IS_PED_INJURED(localPlayerPed)	
						AND NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
						AND NOT IS_VECTOR_ZERO(vArenaSpawnVec)
							IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
								VEHICLE_INDEX viTemp = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
								VECTOR vTemp = GET_ENTITY_COORDS(viTemp)
								FLOAT fTempHeight
								GET_GROUND_Z_FOR_3D_COORD(vTemp, fTempHeight)
								
								PRINTLN("[321][PROCESS_ARENA_SPAWN_FIX_2][IS_COUNTDOWN_OK_TO_PROCEED] - Difference: ", (fTempHeight - vTemp.z), " Our Height: ", vTemp.z, " GroundZHeight: ", fTempHeight, " Sorted Vector Height: ", vArenaSpawnVec.z)
								
								IF (fTempHeight - vTemp.z) <= 0.35
								AND (fTempHeight > vTemp.z)
									SET_ENTITY_COORDS(viTemp, vArenaSpawnVec)
									PRINTLN("[321][PROCESS_ARENA_SPAWN_FIX_2][IS_COUNTDOWN_OK_TO_PROCEED] - Setting Veh oto saved COORD: ", vArenaSpawnVec)
									
									IF SET_VEHICLE_ON_GROUND_PROPERLY(viTemp, 5)
										PRINTLN("[321][PROCESS_ARENA_SPAWN_FIX_2][IS_COUNTDOWN_OK_TO_PROCEED] - Setting Veh on ground now that we KNOW everything is loaded dammit. (Success)")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
								
				IF IS_NET_TIMER_MORE_THAN(sTimerTemp2, MC_serverBD.timeServerCountdown)

					PRINTLN("[MMacK][321][IS_COUNTDOWN_OK_TO_PROCEED], COUNT_DOWN_HAS_EXPIRED")
					
					IF NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_EXCLUDE_FROM_3_2_1_GO_COUNTDOWN)
					AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_Reset_321GO_OnRoundRestart)
					AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_GAMES_MASTERS(g_FMMC_STRUCT.iAdversaryModeType) 
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetTwelve, ciENABLE_TINY_RACERS_CHECKPOINT_LOGIC)
							RELEASE_COUNTDOWN_UI(cduiIntro)
							CLEAN_COUNTDOWN(cduiIntro)
						ELSE
							SET_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TINY_RACERS_321_FINISHED)
						ENDIF
					ENDIF
					
					IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
					AND NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
						NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
					ENDIF
					
					IF bIsLocalPlayerHost
						RESET_NET_TIMER(MC_serverBD.timeServerCountdown)						
						SET_BIT(MC_serverBD.iServerBitSet2, SBBOOL2_FINISHED_COUNTDOWN)
					ENDIF
					PRINTLN("[MMacK][321][IS_COUNTDOWN_OK_TO_PROCEED], RETURNING TRUE AS TIMER EXPIRED ", NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()))
					RETURN TRUE
				ENDIF	
			ENDIF
			
			RETURN FALSE
		ENDIF
	ELSE
		PRINTLN("[MMacK][321][IS_COUNTDOWN_OK_TO_PROCEED], RETURNING TRUE AS SBBOOL2_FINISHED_COUNTDOWN IS SET ", NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()))
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SET_COUNTDOWN_NUMBER(MISSION_INTRO_COUNTDOWN_UI &uiToSet, INT iSecond)
	INT iR, iG, iB, iA
	GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(uiToSet.uiCountdown, "SET_MESSAGE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("NUMBER")
			ADD_TEXT_COMPONENT_INTEGER(ABSI(iSecond))
		END_TEXT_COMMAND_SCALEFORM_STRING()
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iR)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iG)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iB)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SHOW_GO_SPLASH(MISSION_INTRO_COUNTDOWN_UI &uiToUpdate)
	IF NOT IS_BITMASK_AS_ENUM_SET(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_Go)
	AND HAS_SCALEFORM_MOVIE_LOADED(uiToUpdate.uiCountdown)
		PRINTLN("[MMacK][321GO] Played Go UNNATURAL")
		SET_BITMASK_AS_ENUM(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_Go)

		INT iR, iG, iB, iA
		GET_HUD_COLOUR(HUD_COLOUR_GREEN, iR, iG, iB, iA)
		
		BEGIN_SCALEFORM_MOVIE_METHOD(uiToUpdate.uiCountdown, "SET_MESSAGE")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CNTDWN_GO")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iR)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iG)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iB)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		END_SCALEFORM_MOVIE_METHOD()
		
		//Also add the sound under the line:
		//PRINTLN("[MMacK][321GO] Played Go NATURAL")
		//in DISPLAY_INTRO_COUNTDOWN
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciRACE_BOOST_START) 
			IF NOT HAS_NET_TIMER_STARTED(g_FMMC_STRUCT.stBoostTimer)
				PRINTLN("[JS] SHOW_GO_SPLASH - Boost timer")
				START_NET_TIMER(g_FMMC_STRUCT.stBoostTimer)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciENABLE_GO_AIRHORN)
			IF CONTENT_IS_USING_ARENA()
				PLAY_SOUND_FRONTEND(-1, "Start", "DLC_AW_Frontend_Sounds" , FALSE)
			ELSE
				PLAY_SOUND_FRONTEND(-1, "Airhorn", "DLC_TG_Running_Back_Sounds" , FALSE)
			ENDIF
		ENDIF

		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciENABLE_SUMO_SOUNDS)
			PRINTLN("[RCC MISSION] SHOW_GO_SPLASH - Playing sumo Round_Start sound.")
			PLAY_SOUND_FRONTEND(-1, "Round_Start", "DLC_LOW2_Sumo_Soundset", FALSE) //B* 2666917
		ENDIF	
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_SUMO_RUN_SOUNDS)
			PRINTLN("[RCC MISSION] SHOW_GO_SPLASH - Playing sumo Round_Start sound.")
			PLAY_SOUND_FRONTEND(-1, "Round_Start", "DLC_BTL_SM_Remix_Soundset", FALSE) //B* 4737001
		ENDIF	
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_RUNNING_BACK_REMIX_SOUNDS)
			PLAY_SOUND_FRONTEND(-1, "Airhorn", "DLC_BTL_RB_Remix_Sounds" , FALSE)
		ENDIF
		
		PLAY_ARENA_ANNOUNCER_GO_LINE()
			
		IF NOT IS_PED_INJURED(localPlayerPed)
		AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
			NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
		ENDIF
	ENDIF
ENDPROC

PROC DISPLAY_INTRO_COUNTDOWN(MISSION_INTRO_COUNTDOWN_UI &uiToUpdate)
	IF HAS_SCALEFORM_MOVIE_LOADED(uiToUpdate.uiCountdown)
		IF NOT IS_TIMER_STARTED(uiToUpdate.CountdownTimer)
			RESTART_TIMER_NOW(uiToUpdate.CountdownTimer)
		ENDIF
		CLEAR_BIT(iLocalBoolCheck13, LBOOL13_DISPLAYED_INTRO_SHARD)
		
		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
		DRAW_SCALEFORM_MOVIE_FULLSCREEN(uiToUpdate.uiCountdown, 255, 255, 255, 100)
		
		INT iTimerVal = FLOOR(GET_TIMER_IN_SECONDS(uiToUpdate.CountdownTimer))
		INT iSeconds = ABSI(iTimerVal - 3)
		
		IF IS_BITMASK_AS_ENUM_SET(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_1)
			IF uiToUpdate.iFrameCount >= 5
				IF NOT IS_BITMASK_AS_ENUM_SET(uiToUpdate.iBitFlags, CNTDWN_UI_SoundGo)
					IF CONTENT_IS_USING_ARENA()
						PLAY_SOUND_FRONTEND(-1, "Countdown_GO", "DLC_AW_Frontend_Sounds")
						PRINTLN("[MJL] [DISPLAY_INTRO_COUNTDOWN][FM_MISSION_CONTROLLER_HUD] PLAY_SOUND_FRONTEND, Countdown_GO ")
						SET_BITMASK_AS_ENUM(uiToUpdate.iBitFlags, CNTDWN_UI_SoundGo)
						STOP_STREAM()
					ELIF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciENABLE_GO_AIRHORN)
					AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TINY_RACERS_SOUNDS)
						PLAY_SOUND_FRONTEND(-1, "GO", "HUD_MINI_GAME_SOUNDSET", FALSE) 
						SET_BITMASK_AS_ENUM(uiToUpdate.iBitFlags, CNTDWN_UI_SoundGo)
						STOP_STREAM()
					ENDIF
				ENDIF
			ELSE
				uiToUpdate.iFrameCount++
			ENDIF
		ENDIF
		
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARS(g_FMMC_STRUCT.iAdversaryModeType)
			PRINTLN("[LM][TURF WAR] - Hiding Radar from 3_2_1 countdown")
			DISPLAY_RADAR(FALSE)
		ENDIF
		
		PROCESS_DISPLACEMENT_ARENA_INTERIOR_MC()
		
		PRINTLN("[DISPLAY_INTRO_COUNTDOWN] - Attempting to play countdown.")
		
		IF NOT IS_BITMASK_AS_ENUM_SET(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_Go)
			IF (iSeconds = 3) AND NOT IS_BITMASK_AS_ENUM_SET(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_3)
				
				IF g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed > 0
					IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_MONSTER_JAM(g_FMMC_STRUCT.iAdversaryModeType)
						SET_ARENA_ANNOUNCER_BS_MODE_SPECIFIC(g_iAA_PlaySound_MonsterJam_Countdown)
						PRINTLN("[MJL] [DISPLAY_INTRO_COUNTDOWN][FM_MISSION_CONTROLLER_HUD] Playing Monster Jam so playing g_iAA_PlaySound_MonsterJam_Countdown")
					ELSE
						SET_ARENA_ANNOUNCER_BS_GENERAL(g_iAA_PlaySound_Generic_CountdownTeams)
						PRINTLN("[MJL] [DISPLAY_INTRO_COUNTDOWN][FM_MISSION_CONTROLLER_HUD] Playing Monster Jam so NOT playing 'Countdown_GO'")
					ENDIF
				ENDIF
				
				SET_BITMASK_AS_ENUM(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_3)
				IF CONTENT_IS_USING_ARENA()
					PLAY_SOUND_FRONTEND(-1, "Countdown_3", "DLC_AW_Frontend_Sounds")
					PRINTLN("[MJL] [DISPLAY_INTRO_COUNTDOWN][FM_MISSION_CONTROLLER_HUD] PLAY_SOUND_FRONTEND, Countdown_3 ")
				ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TINY_RACERS_SOUNDS)
					PLAY_SOUND_FRONTEND(-1,"Countdown_3", "DLC_SR_TR_General_Sounds")
				ELSE
					PLAY_SOUND_FRONTEND(-1, "3_2_1", "HUD_MINI_GAME_SOUNDSET", FALSE)
				ENDIF
				SET_COUNTDOWN_NUMBER(uiToUpdate, iSeconds)
				IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
					NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE)
				ENDIF
			ELIF (iSeconds = 2) AND NOT IS_BITMASK_AS_ENUM_SET(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_2)
				SET_BITMASK_AS_ENUM(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_2)
				IF CONTENT_IS_USING_ARENA()
					PLAY_SOUND_FRONTEND(-1, "Countdown_2", "DLC_AW_Frontend_Sounds")
					PRINTLN("[MJL] [DISPLAY_INTRO_COUNTDOWN][FM_MISSION_CONTROLLER_HUD] PLAY_SOUND_FRONTEND, Countdown_2 ")
				ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TINY_RACERS_SOUNDS)
					PLAY_SOUND_FRONTEND(-1,"Countdown_2", "DLC_SR_TR_General_Sounds")
				ELSE
					PLAY_SOUND_FRONTEND(-1, "3_2_1", "HUD_MINI_GAME_SOUNDSET", FALSE)
				ENDIF				
				SET_COUNTDOWN_NUMBER(uiToUpdate, iSeconds)
				IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
					NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE)
				ENDIF
			ELIF (iSeconds = 1) AND NOT IS_BITMASK_AS_ENUM_SET(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_1)
				SET_BITMASK_AS_ENUM(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_1)
				IF CONTENT_IS_USING_ARENA()
					PLAY_SOUND_FRONTEND(-1, "Countdown_1", "DLC_AW_Frontend_Sounds")
					PRINTLN("[MJL] [DISPLAY_INTRO_COUNTDOWN][FM_MISSION_CONTROLLER_HUD] PLAY_SOUND_FRONTEND, Countdown_1 ")
				ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TINY_RACERS_SOUNDS)
					PLAY_SOUND_FRONTEND(-1,"Countdown_1", "DLC_SR_TR_General_Sounds")
				ELSE
					PLAY_SOUND_FRONTEND(-1, "3_2_1", "HUD_MINI_GAME_SOUNDSET", FALSE)
				ENDIF
				SET_COUNTDOWN_NUMBER(uiToUpdate, iSeconds)
				IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
					NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE)
				ENDIF
			ELIF (iSeconds = 0) AND NOT IS_BITMASK_AS_ENUM_SET(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_Go)
				SET_BITMASK_AS_ENUM(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_Go)
				PRINTLN("[MMacK][321GO] Played Go NATURAL")
				INT iR, iG, iB, iA
				GET_HUD_COLOUR(HUD_COLOUR_GREEN, iR, iG, iB, iA)
				
				BEGIN_SCALEFORM_MOVIE_METHOD(uiToUpdate.uiCountdown, "SET_MESSAGE")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CNTDWN_GO")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iR)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iG)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iB)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
				END_SCALEFORM_MOVIE_METHOD()
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciRACE_BOOST_START) 
					IF NOT HAS_NET_TIMER_STARTED(g_FMMC_STRUCT.stBoostTimer)
						PRINTLN("[JS] DISPLAY_INTRO_COUNTDOWN - Boost timer")
						START_NET_TIMER(g_FMMC_STRUCT.stBoostTimer)
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TINY_RACERS_SOUNDS)
					PLAY_SOUND_FRONTEND(-1,"Countdown_GO", "DLC_SR_TR_General_Sounds")
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciENABLE_GO_AIRHORN)
					IF CONTENT_IS_USING_ARENA()
						PLAY_SOUND_FRONTEND(-1, "Start", "DLC_AW_Frontend_Sounds" , FALSE)
					ELSE
						PLAY_SOUND_FRONTEND(-1, "Airhorn", "DLC_TG_Running_Back_Sounds" , FALSE)
					ENDIF
				ENDIF
				
				PLAY_ARENA_ANNOUNCER_GO_LINE()
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciENABLE_SUMO_SOUNDS)
					PRINTLN("[RCC MISSION] DISPLAY_INTRO_COUNTDOWN - Playing sumo Round_Start sound.")
					PLAY_SOUND_FRONTEND(-1, "Round_Start", "DLC_LOW2_Sumo_Soundset", FALSE) //B* 2666917
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_SUMO_RUN_SOUNDS)
					PRINTLN("[RCC MISSION] DISPLAY_INTRO_COUNTDOWN - Playing sumo Round_Start sound.")
					PLAY_SOUND_FRONTEND(-1, "Round_Start", "DLC_BTL_SM_Remix_Soundset", FALSE) //B* 4737001
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_RUNNING_BACK_REMIX_SOUNDS)
					PLAY_SOUND_FRONTEND(-1, "Airhorn", "DLC_BTL_RB_Remix_Sounds" , FALSE)
				ENDIF
			
				IF g_bDisableVehicleMines
					DISABLE_VEHICLE_MINES(FALSE)
					PRINTLN("[LM][DISPLAY_INTRO_COUNTDOWN] - LIMIT RACE CONROLS - Re-Enabling Vehicle Mines.")
				ENDIF
				
				IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
					NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		PRINTLN("[PROCESS_MISSION_COUNTDOWN_TIMER] LIMIT RACE CONROLS - Scaleform movie not loaded.")
	ENDIF
ENDPROC

FUNC TEXT_LABEL_63 PROCESS_OBJECTIVE_TEXT_COLOUR_CHANGE(TEXT_LABEL_63 tl63ObjectiveText)

	IF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_CONTROL_AREA
	AND IS_BIT_SET(iLocalBoolCheck14, LBOOL14_VFX_CAPTURE_OWNER)
		IF NOT IS_STRING_NULL_OR_EMPTY(tl63ObjectiveText)
	        TEXT_LABEL_63 tl63LastChar, sTemp
			BOOL bColourChar = FALSE
			INT i = 0
			FOR i = 0 TO GET_LENGTH_OF_LITERAL_STRING(tl63ObjectiveText)-1
				IF i > 0
					IF ARE_STRINGS_EQUAL(tl63LastChar, "~")
						bColourChar = TRUE
					ENDIF
				ENDIF
				IF bColourChar
					IF g_iTheWinningTeam = -1
						//Show Yellow = Tied
						tl63LastChar = "y"
					ELIF g_iTheWinningTeam = 0
						//Team 0 - Orange
						tl63LastChar = "o"
					ELIF g_iTheWinningTeam = 1
						//Team 1 - Green
						tl63LastChar = "g"
					ELIF g_iTheWinningTeam = 2
						//Team 2 - Pink
						tl63LastChar = "q"
					ELIF g_iTheWinningTeam = 3
						//Team 3 - Purple
						tl63LastChar = "p"
					ENDIF
					sTemp += tl63LastChar
					sTemp += GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tl63ObjectiveText, i+1, GET_LENGTH_OF_LITERAL_STRING(tl63ObjectiveText))
	       			RETURN sTemp   
				ELSE
		        	tl63LastChar = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tl63ObjectiveText, i, i+1)
				ENDIF
				
		        sTemp += tl63LastChar
			ENDFOR
	        RETURN sTemp     
	    ENDIF
	ENDIF
	RETURN tl63ObjectiveText
ENDFUNC


//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: OBJECTIVE TEXT !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: OBJECTIVE TEXT PRINTING FUNCTIONS !
//
//************************************************************************************************************************************************************



FUNC BOOL PRINT_CUSTOM_OBJECTIVE(TEXT_LABEL_63 tl63CustomGodText)
	IF NOT IS_BIT_SET(iLocalBoolCheck,LBOOL_BLOCK_OBJECTIVE)
	AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_POWER_PLAY(g_FMMC_STRUCT.iAdversaryModeType)
		tl63CustomGodText = PROCESS_OBJECTIVE_TEXT_COLOUR_CHANGE(tl63CustomGodText)
		IF NOT IS_STRING_NULL_OR_EMPTY(tl63CustomGodText)
			IF NOT Has_This_MP_Objective_Text_User_Created_Been_Received(tl63CustomGodText)
				PRINTLN("[RCC MISSION] PRINT_CUSTOM_OBJECTIVE -inserted text is: ",tl63CustomGodText)
				PRINTLN("[RCC MISSION] PRINT_CUSTOM_OBJECTIVE -iclientstage = ",GET_MC_CLIENT_MISSION_STAGE(iPartToUse))
				Print_Objective_Text_User_Created(tl63CustomGodText)
				DEBUG_PRINTCALLSTACK()
			ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL PRINT_NORMAL_OBJECTIVE(STRING sobjective)


	IF NOT IS_BIT_SET(iLocalBoolCheck,LBOOL_BLOCK_OBJECTIVE)
		IF NOT IS_STRING_NULL_OR_EMPTY(sobjective)
			IF NOT Has_This_MP_Objective_Text_Been_Received(sobjective)
				PRINTLN("PRINT_NORMAL_OBJECTIVE : ",sobjective)
				Print_Objective_Text(sobjective)
			ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL PRINT_COLOURED_OBJECTIVE(STRING sobjective,HUD_COLOURS HudColour)

INT R,G,B,A

	IF NOT IS_BIT_SET(iLocalBoolCheck,LBOOL_BLOCK_OBJECTIVE)
		IF NOT IS_STRING_NULL_OR_EMPTY(sobjective)
			IF NOT Has_This_MP_Objective_Text_Been_Received(sobjective)
				GET_HUD_COLOUR(HudColour,R,G,B,A)
				SET_SCRIPT_VARIABLE_HUD_COLOUR(R,G,B,A)
				PRINTLN("[RCC MISSION] MY TEAM: ",MC_playerBD[iPartToUse].iteam)
				PRINTLN("[RCC MISSION] PRINT_COLOURED_OBJECTIVE HUD COLOUR: ",sobjective,ENUM_TO_INT(HudColour))

				Print_Objective_Text(sobjective)
			ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL PRINT_COLOURED_TEAM_OBJECTIVE_WITH_PLAYER(STRING sobjective, INT iteam, PLAYER_INDEX piPlayer )

	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRSHOOTOUT(g_FMMC_STRUCT.iAdversaryModeType)
	AND (piPlayer = INVALID_PLAYER_INDEX()
	OR IS_STRING_NULL_OR_EMPTY(GET_PLAYER_NAME(piPlayer))
	OR (NETWORK_IS_PLAYER_A_PARTICIPANT(piPlayer) AND NOT NETWORK_IS_PARTICIPANT_ACTIVE(NETWORK_GET_PARTICIPANT_INDEX(piPlayer)))) //3920559 - checking this player is a participant before checking its participant stuff
		PRINTLN("[RCC MISSION] PRINT_COLOURED_TEAM_OBJECTIVE_WITH_PLAYER: Doing nothing because no active player for team ",iTeam)
		RETURN FALSE
	ENDIF
	
	HUD_COLOURS teamcolour

	INT R,G,B,A

	IF NOT IS_BIT_SET(iLocalBoolCheck,LBOOL_BLOCK_OBJECTIVE)
		IF NOT IS_STRING_NULL_OR_EMPTY(sobjective)
			IF NOT Has_This_MP_Objective_Text_With_User_Created_String_Been_Received(sobjective,g_sMission_TeamName[iteam])
				
				teamcolour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iteam, PlayerToUse)
				
				PRINTLN("[RCC MISSION] MY TEAM: ",MC_playerBD[iPartToUse].iteam)
				PRINTLN("[RCC MISSION] OTHER TEAM: ",iteam)
				PRINTLN("[RCC MISSION] PRINT_COLOURED_TEAM_OBJECTIVE WITH HUD COLOUR: ", sobjective, " ", ENUM_TO_INT(teamcolour))

				GET_HUD_COLOUR(teamcolour,R,G,B,A)
				SET_SCRIPT_VARIABLE_HUD_COLOUR(R,G,B,A)

				TEXT_LABEL_63 LocalTitle
				
				LocalTitle = GET_PLAYER_NAME(piPlayer)
				
				PRINTLN("[RCC MISSION] PLAYER NAME: ", LocalTitle)
				
				Print_Objective_Text_With_User_Created_String( sobjective, LocalTitle )
			ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL PRINT_COLOURED_TEAM_OBJECTIVE_WITH_TWO_PLAYERS(STRING sobjective, INT iteamOne, PLAYER_INDEX piPlayerOne, INT iteamTwo, PLAYER_INDEX piPlayerTwo)

	HUD_COLOURS teamcolour

	INT R,G,B,A

	IF NOT IS_BIT_SET(iLocalBoolCheck,LBOOL_BLOCK_OBJECTIVE)
		IF NOT IS_STRING_NULL_OR_EMPTY(sobjective)
			IF NOT Has_This_MP_Objective_Text_With_Two_User_Created_Strings_Been_Received(sobjective, g_sMission_TeamName[iteamOne], g_sMission_TeamName[iteamTwo])
				
				teamcolour = GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_PlayerBD[iPartToUse].iteam, piPlayerOne)
				
				PRINTLN("[RCC MISSION] MY TEAM: ", iteamOne)
				PRINTLN("[RCC MISSION] OTHER TEAM: ", iteamTwo)
				PRINTLN("[RCC MISSION] PRINT_COLOURED_TEAM_OBJECTIVE WITH HUD COLOUR: ", sobjective, " ", ENUM_TO_INT(teamcolour))

				GET_HUD_COLOUR(teamcolour,R,G,B,A)
				SET_SCRIPT_VARIABLE_HUD_COLOUR(R,G,B,A)

				TEXT_LABEL_63 LocalTitleOne, LocalTitleTwo
				
				LocalTitleOne = GET_PLAYER_NAME(piPlayerOne)
				
				teamcolour = GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_PlayerBD[iPartToUse].iteam, piPlayerTwo)
				GET_HUD_COLOUR(teamcolour,R,G,B,A)
				SET_SECOND_SCRIPT_VARIABLE_HUD_COLOUR(R,G,B,A)
				PRINTLN("[RCC MISSION] PRINT_COLOURED_TEAM_OBJECTIVE WITH HUD COLOUR (TWO): ", sobjective, " ", ENUM_TO_INT(teamcolour))				
				
				LocalTitleTwo = GET_PLAYER_NAME(piPlayerTwo)
				
				PRINTLN("[RCC MISSION] PLAYER NAME: ", LocalTitleOne)
				PRINTLN("[RCC MISSION] PLAYER NAME TWO: ", LocalTitleTwo)
				
				Print_Objective_Text_With_Two_User_Created_Strings( sobjective, LocalTitleOne, LocalTitleTwo)
			ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL PRINT_COLOURED_TEAM_OBJECTIVE( STRING sobjective, INT iteam )

	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_TEAM_TURNS)
		RETURN PRINT_COLOURED_TEAM_OBJECTIVE_WITH_PLAYER(sobjective, iteam, MC_serverBD.piTurnsActivePlayer[iteam])
	ENDIF
	HUD_COLOURS teamcolour

	INT R,G,B,A

	IF NOT IS_BIT_SET(iLocalBoolCheck,LBOOL_BLOCK_OBJECTIVE)
		IF NOT IS_STRING_NULL_OR_EMPTY(sobjective)
			IF NOT Has_This_MP_Objective_Text_With_User_Created_String_Been_Received(sobjective,g_sMission_TeamName[iteam])
				
				teamcolour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iteam, PlayerToUse)
				
				PRINTLN("[RCC MISSION] MY TEAM: ",MC_playerBD[iPartToUse].iteam)
				PRINTLN("[RCC MISSION] OTHER TEAM: ",iteam)
				PRINTLN("[RCC MISSION] PRINT_COLOURED_TEAM_OBJECTIVE WITH HUD COLOUR: ",sobjective,ENUM_TO_INT(teamcolour))

				GET_HUD_COLOUR(teamcolour,R,G,B,A)
				SET_SCRIPT_VARIABLE_HUD_COLOUR(R,G,B,A)

				TEXT_LABEL_31 LocalTitle
				
				IF IS_BIT_SET( iCelebrationTeamNameIsNotLiteral, iTeam )
					LocalTitle = TEXT_LABEL_TO_STRING( g_sMission_TeamName[ iTeam ] )
				ELSE
					LocalTitle = g_sMission_TeamName[iteam]
				ENDIF
				
				Print_Objective_Text_With_User_Created_String( sobjective, LocalTitle )
			ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC



FUNC TEXT_LABEL_15 GET_TEXT_LABEL_FOR_TOUR_DE_FORCE_OBJECTIVE(INT iTeam)
	
	TEXT_LABEL_15 tlObjLabel
	HUD_COLOURS eteamcolour
	
	eteamcolour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam, PlayerToUse)

	SWITCH eTeamColour
		
		CASE HUD_COLOUR_ORANGE
			IF ARE_ALL_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE(iTeam)
				tlObjLabel = "TDF_OBJO"
			ELSE
				tlObjLabel = "TDF_OBJOW"
			ENDIF
		BREAK
		
		CASE HUD_COLOUR_GREEN
			IF ARE_ALL_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE(iTeam)
				tlObjLabel = "TDF_OBJG"
			ELSE
				tlObjLabel = "TDF_OBJGW"
			ENDIF
		BREAK
		
		CASE HUD_COLOUR_PINK
			IF ARE_ALL_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE(iTeam)
				tlObjLabel = "TDF_OBJP"
			ELSE
				tlObjLabel = "TDF_OBJPW"
			ENDIF
		BREAK
		
		CASE HUD_COLOUR_PURPLE
			IF ARE_ALL_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE(iTeam)
				tlObjLabel = "TDF_OBJPU"
			ELSE
				tlObjLabel = "TDF_OBJPUW"
			ENDIF
		BREAK

	ENDSWITCH
	
	PRINTLN("[RCC MISSION][PP TEXT]GET_TEXT_LABEL_FOR_POWER_PLAY_OBJECTIVE - ", tlObjLabel)
	
	RETURN tlObjLabel
	

ENDFUNC

FUNC BOOL PRINT_TOUR_DE_FORCE_OBJECTIVE()

	STRING sobjective
	TEXT_LABEL_15 tlTemp
	INT iTeam =  MC_playerBD[iPartToUse].iTeam

	IF NOT IS_BIT_SET(iLocalBoolCheck,LBOOL_BLOCK_OBJECTIVE)
		PRINTLN("[RCC MISSION] PRINT_TOUR_DE_FORCE_OBJECTIVE")
		
		tlTemp = GET_TEXT_LABEL_FOR_TOUR_DE_FORCE_OBJECTIVE(iTeam)
		
		sobjective = TEXT_LABEL_INTO_STRING(tlTemp)
		
		IF NOT IS_STRING_NULL_OR_EMPTY(sobjective)
			IF NOT Has_This_MP_Objective_Text_Been_Received(sobjective)
				PRINTLN("[RCC MISSION] PRINT_TOUR_DE_FORCE_OBJECTIVE")
				Print_Objective_Text(sobjective)
			ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC TEXT_LABEL_63 GET_TEXT_LABEL_FOR_POINTLESS(TEXT_LABEL_63 tlPassed)
	TEXT_LABEL_63 tlToReturn
	
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
		IF DOES_ANY_TEAM_HAVE_MAX_POINTS()
			INT i, iTeamToShow
			FOR i = 0 TO FMMC_MAX_TEAMS - 1
				IF MC_serverBD.iTeamScore[i] >= g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionScoreLimit
					iTeamToShow = i
				ENDIF
			ENDFOR
			IF MC_serverBD.iTeamScore[MC_playerBD[iPartToUse].iteam] >= g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionScoreLimit
				tlToReturn = "PL_WIN_TEAM"
			ELSE
				tlToReturn = "PL_STOP_TEAM_"
				tlToReturn += iTeamToShow
			ENDIF
		ELSE
			tlToReturn = PROCESS_OBJECTIVE_TEXT_COLOUR_CHANGE(tlPassed)
		ENDIF
	ELSE
		tlToReturn = "SUDDTH_PTL"
	ENDIF
	RETURN tlToReturn
ENDFUNC

FUNC BOOL IS_PL_TEXT_LABEL(TEXT_LABEL_63 tlPassed)
	IF ARE_STRINGS_EQUAL(tlPassed, "PL_WIN_TEAM")
	OR ARE_STRINGS_EQUAL(tlPassed, "PL_STOP_TEAM_0")
	OR ARE_STRINGS_EQUAL(tlPassed, "PL_STOP_TEAM_1")
	OR ARE_STRINGS_EQUAL(tlPassed, "PL_STOP_TEAM_2")
	OR ARE_STRINGS_EQUAL(tlPassed, "PL_STOP_TEAM_3")
	OR ARE_STRINGS_EQUAL(tlPassed, "SUDDTH_PTL")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRINT_CUSTOM_POINTLESS_OBJECTIVE(TEXT_LABEL_63 tlPassed)
	STRING sobjective
	
	
	IF NOT IS_BIT_SET(iLocalBoolCheck,LBOOL_BLOCK_OBJECTIVE)
		
		sobjective = TEXT_LABEL_INTO_STRING(tlPassed)
		//sobjective = GET_FILENAME_FOR_AUDIO_CONVERSATION(sobjective)
		
		IF NOT IS_STRING_NULL_OR_EMPTY(sobjective)
			IF NOT Has_This_MP_Objective_Text_Been_Received(sobjective)
				IF IS_PL_TEXT_LABEL(tlPassed)
					Print_Objective_Text(sobjective)
				ELSE
					Print_Objective_Text_User_Created(sobjective)
				ENDIF
				PRINTLN("[JT HUD] - Showing text: ", sobjective)
			ENDIF
			PRINTLN("[JT HUD] - IS_STRING_NULL_OR_EMPTY not empty")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
	
ENDFUNC

FUNC TEXT_LABEL_15 GET_TEXT_LABEL_FOR_POWER_PLAY_OBJECTIVE(INT iStyleSetup, VERSUS_OUTFIT_STYLE eVersusStyle, INT iTeam)
	INT iOpponentTeam
	
	IF iTeam = 0
		iOpponentTeam = 1
	ElSE
		iOpponentTeam = 0
	ENDIF
	
	MP_OUTFIT_ENUM eLocalOutfit = GET_MP_VERSUS_OUTFIT_DEFAULT(iStyleSetup, eVersusStyle, iOpponentTeam)
	
	TEXT_LABEL_15 tlTeamName = ""

	SWITCH eLocalOutfit
		
		CASE OUTFIT_VERSUS_THEMED_EXEC_PURPLE_BUGS_0
		CASE OUTFIT_VERSUS_THEMED_EXEC_PURPLE_BUGS_1
		CASE OUTFIT_VERSUS_THEMED_EXEC_PURPLE_BUGS_2
		CASE OUTFIT_VERSUS_THEMED_EXEC_PURPLE_BUGS_3
		CASE OUTFIT_VERSUS_THEMED_EXEC_PURPLE_BUGS_4
		CASE OUTFIT_VERSUS_THEMED_EXEC_PURPLE_BUGS_5
			tlTeamName = "PP_OBJ_TO_TP"
		BREAK
			
		CASE OUTFIT_VERSUS_THEMED_EXEC_ORANGE_BUGS_0
		CASE OUTFIT_VERSUS_THEMED_EXEC_ORANGE_BUGS_1
		CASE OUTFIT_VERSUS_THEMED_EXEC_ORANGE_BUGS_2
		CASE OUTFIT_VERSUS_THEMED_EXEC_ORANGE_BUGS_3
		CASE OUTFIT_VERSUS_THEMED_EXEC_ORANGE_BUGS_4
		CASE OUTFIT_VERSUS_THEMED_EXEC_ORANGE_BUGS_5
			tlTeamName = "PP_OBJ_TO_KB"
		BREAK
		
		CASE OUTFIT_VERSUS_THEMED_EXEC_MONO_0
		CASE OUTFIT_VERSUS_THEMED_EXEC_MONO_1
		CASE OUTFIT_VERSUS_THEMED_EXEC_MONO_2
		CASE OUTFIT_VERSUS_THEMED_EXEC_MONO_3
		CASE OUTFIT_VERSUS_THEMED_EXEC_MONO_4
		CASE OUTFIT_VERSUS_THEMED_EXEC_MONO_5
			tlTeamName = "PP_OBJ_TO_S" 
		BREAK
		
		CASE OUTFIT_VERSUS_THEMED_EXEC_RAINBOW_0
		CASE OUTFIT_VERSUS_THEMED_EXEC_RAINBOW_1
		CASE OUTFIT_VERSUS_THEMED_EXEC_RAINBOW_2
		CASE OUTFIT_VERSUS_THEMED_EXEC_RAINBOW_3
		CASE OUTFIT_VERSUS_THEMED_EXEC_RAINBOW_4
		CASE OUTFIT_VERSUS_THEMED_EXEC_RAINBOW_5
			tlTeamName = "PP_OBJ_TO_R"
		BREAK
		
		CASE OUTFIT_VERSUS_THEMED_EXEC_ANIMALS_0
		CASE OUTFIT_VERSUS_THEMED_EXEC_ANIMALS_1
		CASE OUTFIT_VERSUS_THEMED_EXEC_ANIMALS_2
		CASE OUTFIT_VERSUS_THEMED_EXEC_ANIMALS_3
		CASE OUTFIT_VERSUS_THEMED_EXEC_ANIMALS_4
		CASE OUTFIT_VERSUS_THEMED_EXEC_ANIMALS_5
			tlTeamName = "PP_OBJ_TO_A"
		BREAK
		
		CASE OUTFIT_VERSUS_THEMED_EXEC_COOKIES_0
		CASE OUTFIT_VERSUS_THEMED_EXEC_COOKIES_1
		CASE OUTFIT_VERSUS_THEMED_EXEC_COOKIES_2
		CASE OUTFIT_VERSUS_THEMED_EXEC_COOKIES_3
		CASE OUTFIT_VERSUS_THEMED_EXEC_COOKIES_4
		CASE OUTFIT_VERSUS_THEMED_EXEC_COOKIES_5
			tlTeamName = "PP_OBJ_TO_M"
		BREAK
		
		CASE OUTFIT_VERSUS_HIDDEN_TEAM_SPORT_PURPLE_0
			tlTeamName = "PP_OBJ_TO_B" 
		BREAK
			
		CASE OUTFIT_VERSUS_HIDDEN_TEAM_SPORT_ORANGE_0
			tlTeamName = "PP_OBJ_TO_C"
		BREAK
	
	ENDSWITCH
	
	PRINTLN("[RCC MISSION][PP TEXT]GET_TEXT_LABEL_FOR_POWER_PLAY_OBJECTIVE - ", tlTeamName)
	
	RETURN tlTeamName
	

ENDFUNC

FUNC BOOL PRINT_HARD_TARGET_OBJECTIVE()

	INT iEnemyTeam = PICK_INT(MC_playerBD[iPartToUse].iteam = 0, 1, 0)
	
	IF AM_I_A_HARD_TARGET()
		IF MC_ServerBD.iHardTargetParticipant[iEnemyTeam] > -1
		AND MC_playerBD[iPartToUse].iteam != MC_PlayerBD[MC_ServerBD.iHardTargetParticipant[iEnemyTeam]].iteam // Prevents issues when players leave and we are re-assigning hard targets.
			PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(MC_ServerBD.iHardTargetParticipant[iEnemyTeam])
			IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				RETURN PRINT_COLOURED_TEAM_OBJECTIVE_WITH_PLAYER("HARD_O_HT", iEnemyTeam, NETWORK_GET_PLAYER_INDEX(tempPart))
			ENDIF
		ENDIF
	ELSE
		IF MC_ServerBD.iHardTargetParticipant[iEnemyTeam] > -1
		AND MC_ServerBD.iHardTargetParticipant[MC_PlayerBD[iPartToUse].iteam] > -1
		AND MC_ServerBD.iHardTargetParticipant[iEnemyTeam] != iPartToUse
		AND MC_ServerBD.iHardTargetParticipant[MC_PlayerBD[iPartToUse].iteam] != iPartToUse
		AND MC_ServerBD.iHardTargetParticipant[MC_PlayerBD[iPartToUse].iteam] != MC_ServerBD.iHardTargetParticipant[iEnemyTeam]
			PARTICIPANT_INDEX tempPart1 = INT_TO_PARTICIPANTINDEX(MC_ServerBD.iHardTargetParticipant[iEnemyTeam])
			PARTICIPANT_INDEX tempPart2 = INT_TO_PARTICIPANTINDEX(MC_ServerBD.iHardTargetParticipant[MC_PlayerBD[iPartToUse].iteam])
			IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart1)
			AND NETWORK_IS_PARTICIPANT_ACTIVE(tempPart2)
				RETURN PRINT_COLOURED_TEAM_OBJECTIVE_WITH_TWO_PLAYERS("HARD_O_NHT", iEnemyTeam, NETWORK_GET_PLAYER_INDEX(tempPart1), MC_PlayerBD[iPartToUse].iteam, NETWORK_GET_PLAYER_INDEX(tempPart2))
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL PRINT_POWER_PLAY_TEAM_OBJECTIVE()

	HUD_COLOURS teamcolour
	STRING sobjective
	TEXT_LABEL_15 tlTemp
	INT iThisTeam
	
	INT R,G,B,A
	
	//Team 0 - orange
	//Team 1 - purple

	IF NOT IS_BIT_SET(iLocalBoolCheck,LBOOL_BLOCK_OBJECTIVE)
		PRINTLN("[RCC MISSION][PP TEXT] PRINT_POWER_PLAY_TEAM_OBJECTIVE 1")
		IF MC_playerBD[iPartToUse].iTeam = 0
			iThisTeam = 1
		ElSE
			iThisTeam = 0
		ENDIF
		
		tlTemp = GET_TEXT_LABEL_FOR_POWER_PLAY_OBJECTIVE(MC_serverBD_3.iVersusOutfitStyleSetup, INT_TO_ENUM(VERSUS_OUTFIT_STYLE, MC_serverBD_3.iVersusOutfitStyleChoice), MC_playerBD[iPartToUse].iTeam)
		
		sobjective = TEXT_LABEL_INTO_STRING(tlTemp)
		//sobjective = GET_FILENAME_FOR_AUDIO_CONVERSATION(sobjective)
		
		IF NOT IS_STRING_NULL_OR_EMPTY(sobjective)
			IF NOT Has_This_MP_Objective_Text_Been_Received(sobjective)
				PRINTLN("[RCC MISSION][PP TEXT] PRINT_POWER_PLAY_TEAM_OBJECTIVE 2")
				
				teamcolour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iThisTeam, PlayerToUse)
				GET_HUD_COLOUR(teamcolour,R,G,B,A)
				SET_SCRIPT_VARIABLE_HUD_COLOUR(R,G,B,A)
				
				PRINTLN("[RCC MISSION][PP TEXT] MY TEAM: ",MC_playerBD[iPartToUse].iteam)
				PRINTLN("[RCC MISSION][PP TEXT] OTHER TEAM: ",iThisTeam)
				
				Print_Objective_Text(sobjective)
			ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL PRINT_CONDEMNED_TEAM_OBJECTIVE()

	IF MC_serverBD_3.iCurrentCondemnedTeam = -1
		RETURN TRUE
	ENDIF
	
	STRING sobjective
	TEXT_LABEL_15 tlTemp
	
	INT R,G,B,A
	
	IF (MC_playerBD[iPartToUse].iteam = MC_serverBD_3.iCurrentCondemnedTeam)
		tlTemp = "COND_OBJ_YOU"
	ELSE
		tlTemp = "COND_OBJ_OTH"
	ENDIF
	sobjective = TEXT_LABEL_INTO_STRING(tlTemp)
	//sobjective = GET_FILENAME_FOR_AUDIO_CONVERSATION(sobjective)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sobjective)
		IF NOT Has_This_MP_Objective_Text_Been_Received(sobjective)
			PRINTLN("[RCC MISSION][PP TEXT] PRINT_CONDEMNED_TEAM_OBJECTIVE")
			
			GET_HUD_COLOUR(HUD_COLOUR_RED,R,G,B,A)
			SET_SCRIPT_VARIABLE_HUD_COLOUR(R,G,B,A)
			Print_Objective_Text(sobjective)
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL PRINT_HOSTILE_TAKEOVER_OBJECTIVE()
	STRING sObjective
	
	sObjective = TEXT_LABEL_INTO_STRING("FMMC_HT_MCA")
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sobjective)
		IF NOT Has_This_MP_Objective_Text_Been_Received(sobjective)
			Print_Objective_Text(sobjective)
		ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL PRINT_CUSTOM_COLOURED_OBJECTIVE(TEXT_LABEL_63 tl63CustomGodText,HUD_COLOURS HudColour)


INT R,G,B,A

	IF NOT IS_BIT_SET(iLocalBoolCheck,LBOOL_BLOCK_OBJECTIVE)
		IF NOT IS_STRING_NULL_OR_EMPTY(tl63CustomGodText)
			IF NOT Has_This_MP_Objective_Text_User_Created_Been_Received(tl63CustomGodText)
				GET_HUD_COLOUR(HudColour,R,G,B,A)
				SET_SCRIPT_VARIABLE_HUD_COLOUR(R,G,B,A)
				
				PRINTLN("[RCC MISSION] MY TEAM: ",MC_playerBD[iPartToUse].iteam)
				PRINTLN("[RCC MISSION] PRINT_CUSTOM_COLOURED_OBJECTIVE WITH HUD COLOUR: ",tl63CustomGodText,ENUM_TO_INT(HudColour))

				Print_Objective_Text_User_Created(tl63CustomGodText)
			ENDIF
			RETURN TRUE
		ENDIF
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL PRINT_CUSTOM_COLOURED_TEAM_OBJECTIVE(TEXT_LABEL_63 tl63CustomGodText,INT iteam)

HUD_COLOURS teamcolour

INT R,G,B,A

	IF NOT IS_BIT_SET(iLocalBoolCheck,LBOOL_BLOCK_OBJECTIVE)
		IF NOT IS_STRING_NULL_OR_EMPTY(tl63CustomGodText)
			IF NOT Has_This_MP_Objective_Text_User_Created_Been_Received(tl63CustomGodText)
				
				teamcolour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iteam, PlayerToUse)
				
				PRINTLN("[RCC MISSION] MY TEAM: ",MC_playerBD[iPartToUse].iteam)
				PRINTLN("[RCC MISSION] OTHER TEAM: ",iteam)
				PRINTLN("[RCC MISSION] PRINT_CUSTOM_COLOURED_TEAM_OBJECTIVE WITH HUD COLOUR: ",tl63CustomGodText,ENUM_TO_INT(teamcolour))
				GET_HUD_COLOUR(teamcolour,R,G,B,A)
				SET_SCRIPT_VARIABLE_HUD_COLOUR(R,G,B,A)

				Print_Objective_Text_User_Created(tl63CustomGodText)

			ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL PRINT_OBJECTIVE_WITH_CUSTOM_STRING(STRING sMainText,STRING sInsertedText)
	
	IF NOT IS_BIT_SET(iLocalBoolCheck, LBOOL_BLOCK_OBJECTIVE) 
		IF NOT IS_STRING_NULL_OR_EMPTY(sMainText)
			IF NOT Has_This_MP_Objective_Text_With_User_Created_String_Been_Received(sMainText,sInsertedText)
				
				PRINTLN("[RCC MISSION] PRINT_OBJECTIVE_WITH_CUSTOM_STRING -inserted text is: ",sInsertedText)
				
				Print_Objective_Text_With_User_Created_String(sMainText,sInsertedText)
				
			ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

//This function is currently unused, but could potentially be useful when adding new objectives
/*FUNC BOOL PRINT_NORMAL_OBJECTIVE_WITH_NUMBER(STRING sobjective, INT inumber)

	IF NOT IS_BIT_SET(iLocalBoolCheck,LBOOL_BLOCK_OBJECTIVE)
		IF NOT IS_STRING_NULL_OR_EMPTY(sobjective)
			IF NOT Has_This_MP_Objective_Text_With_Number_Been_Received(sobjective,inumber)
				Print_Objective_Text_With_Number(sobjective,inumber)
			ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC*/



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: OBJECTIVE TEXT PROCESSING FUNCTIONS !
//
//************************************************************************************************************************************************************



//FUNC BOOL IS_ANY_FRIENDLY_CARRYING(INT iCarryState)
//	//loop all players
//	INT i
//	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
//		IF iPartToUse != i //dont want to check ourselves
//		AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet,PBBOOL_ANY_SPECTATOR) //lets ignore spectating too
//			IF DOES_TEAM_LIKE_TEAM(MC_playerBD[iPartToUse].iteam, MC_playerBD[i].iteam)
//				IF GET_MC_CLIENT_MISSION_STAGE(i) = iCarryState
//					PRINTLN("[RCC MISSION][MMacK][FriendlyCarry] Friend Is Carrying")
//					RETURN TRUE
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDFOR
//
//	RETURN FALSE
//ENDFUNC

FUNC BOOL ARE_ANY_VALID_PLAYERS_DRIVING_DELIVERY_VEHICLE()
	
	INT iPart
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		PARTICIPANT_INDEX piPart = INT_TO_PARTICIPANTINDEX(iPart)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(piPart)
			IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)
			AND MC_PlayerBD[iPart].iTeam = MC_PlayerBD[iPartToUse].iTeam
			AND iPartToUse != iPart
				IF MC_playerBD[iPart].iVehNear > -1
					PRINTLN("[RCC MISSION][ModShopProg] - iPart: ", iPart, " MC_playerBD[iPart].iVehNear: ", MC_playerBD[iPart].iVehNear)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CURRENT_OBJECTIVE_WAITING_FOR_DIALOGUE_TO_FINISH()
	INT iTeam = MC_playerBD[iPartToUse].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	BOOL bWaitingOnDialogue
	TEXT_LABEL_23 tlSpeaker = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
	IF NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
	AND NOT ARE_STRINGS_EQUAL(tlSpeaker,"NULL")
		PRINTLN("[MMacK][DiagWait] Subtitles are off, and someone is speaking")	
		RETURN FALSE
	ENDIF
	
	IF iRule < FMMC_MAX_RULES
		INT iDiagToWaitFor = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDialogueToWaitFor[iRule]
		IF iDiagToWaitFor > -1
		AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sDialogueTriggers[iDiagToWaitFor].vPosition)
			IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iDialoguePlayedBS[iDiagToWaitFor/32],iDiagToWaitFor%32)
			AND NOT IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[iDiagToWaitFor].iDialogueBitset,iBS_DialogueNotHeardByTeam0 + iTeam)
				PRINTLN("[MMacK][DiagWait] Not yet played dialogue ",iDiagToWaitFor,", so cant show the objective")	
				bWaitingOnDialogue = TRUE
				
				IF Is_MP_Objective_Text_On_Display()
					Clear_Any_Objective_Text()
				ENDIF
			ELSE
				
				TEXT_LABEL_23 tlConvLbl = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
				
				IF NOT ARE_STRINGS_EQUAL(tlConvLbl,"NULL")
					bWaitingOnDialogue = TRUE
					IF NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
						IF Is_MP_Objective_Text_On_Display()
							Clear_Any_Objective_Text()
						ENDIF
					ELSE
						Clear_Any_Objective_Text_From_This_Script()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		INT iDiagToWaitForMidPoint = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDialogueToWaitForMidPoint[iRule]
		IF iDiagToWaitForMidPoint > -1
		AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sDialogueTriggers[iDiagToWaitForMidPoint].vPosition)
			IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], iRule)
				IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iDialoguePlayedBS[GET_LONG_BITSET_INDEX(iDiagToWaitForMidPoint)], GET_LONG_BITSET_BIT(iDiagToWaitForMidPoint))
				AND NOT IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[iDiagToWaitForMidPoint].iDialogueBitset,iBS_DialogueNotHeardByTeam0 + iTeam)
					PRINTLN("[MMacK][DiagWait] Not yet played dialogue ",iDiagToWaitForMidPoint,", so cant show the objective MIDPOINT")	
					bWaitingOnDialogue = TRUE
					
					IF Is_MP_Objective_Text_On_Display()
						Clear_Any_Objective_Text()
					ENDIF
				ELSE
					
					TEXT_LABEL_23 tlConvLbl = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
					
					IF NOT ARE_STRINGS_EQUAL(tlConvLbl,"NULL")
						bWaitingOnDialogue = TRUE
						IF NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
							IF Is_MP_Objective_Text_On_Display()
								Clear_Any_Objective_Text()
							ENDIF
						ELSE
							Clear_Any_Objective_Text_From_This_Script()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bWaitingOnDialogue
ENDFUNC

FUNC BOOL CURRENT_OBJECTIVE_LOSE_COPS()

	INT iClientStage
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	BOOL bCheckLose = FALSE // If we should be checking for losing the cops
	
	BOOL bLoseCops = FALSE // If we need to hold up the objective and lose the cops
	
	IF iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLoseWantedBitset, iRule)
			
			iClientStage = GET_MC_CLIENT_MISSION_STAGE(iPartToUse)
			
			IF (iClientStage != CLIENT_MISSION_STAGE_COLLECT_OBJ
			AND iClientStage != CLIENT_MISSION_STAGE_COLLECT_VEH
			AND iClientStage != CLIENT_MISSION_STAGE_COLLECT_PED)
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedAtMidBitset, iRule)
					IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], iRule)
						bCheckLose = TRUE
					ENDIF
				ELSE
					bCheckLose = TRUE
				ENDIF
			ELSE
				IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iRule], ciBS_RULE3_OVERRIDE_COLLECTION_TEXT)
					bCheckLose = TRUE
				ELSE // Check if we're holding anyone else up by having a wanted level:
					IF (MC_playerBD[iPartToUse].iWanted > 0)
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iRule], ciBS_RULE_WANTED_TEAM_OBJ)
						OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iRule], ciBS_RULE_WANTED_COOP_TEAMS_OBJ)
							
							SWITCH iClientStage
								CASE CLIENT_MISSION_STAGE_COLLECT_OBJ
									IF MC_serverBD.iNumObjHighestPriorityHeld[iTeam] >= MC_serverBD.iNumObjHighestPriority[iTeam]
										bCheckLose = TRUE
									ENDIF
								BREAK
								CASE CLIENT_MISSION_STAGE_COLLECT_VEH
									IF MC_serverBD.iNumVehHighestPriorityHeld[iTeam] >= MC_serverBD.iNumVehHighestPriority[iTeam]
										bCheckLose = TRUE
									ENDIF
								BREAK
								CASE CLIENT_MISSION_STAGE_COLLECT_PED
									IF MC_serverBD.iNumPedHighestPriorityHeld[iTeam] >= MC_serverBD.iNumPedHighestPriority[iTeam]
										bCheckLose = TRUE
									ENDIF
								BREAK
							ENDSWITCH
							
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF bCheckLose
				
				VEHICLE_INDEX viVehicle
				IF iClientStage = CLIENT_MISSION_STAGE_COLLECT_VEH
					IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
						viVehicle = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
					ENDIF
				ENDIF
				
				IF MC_playerBD[iPartToUse].iWanted > 0
					bLoseCops = TRUE
				ENDIF
				
				IF NOT bLoseCops //no need to do this if we have a wanted ourselves
					IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iRule], ciBS_RULE_WANTED_TEAM_OBJ)
						INT iPart
						INT iChecked
						
						FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
							PARTICIPANT_INDEX piPart = INT_TO_PARTICIPANTINDEX(iPart)
							IF MC_playerBD[iPart].iteam = iTeam
							AND ( (NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR))
								OR IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_HEIST_SPECTATOR) )
							AND NETWORK_IS_PARTICIPANT_ACTIVE(piPart)
								iChecked++
								
								PED_INDEX piPed = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(piPart))
								IF iPart != iPartToUse // Don't need to check local player again
								AND MC_playerBD[iPart].iWanted > 0
									bLoseCops = TRUE
									IF IS_ENTITY_ALIVE(viVehicle)
										IF IS_PED_IN_VEHICLE(piPed, viVehicle)
											bLoseCops = FALSE
										ENDIF
									ENDIF
									BREAKLOOP
								ENDIF
								
								IF iChecked >= MC_serverBD.iNumberOfPlayingPlayers[iTeam]
									BREAKLOOP
								ENDIF
							ENDIF
						ENDFOR
					ELIF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iRule], ciBS_RULE_WANTED_COOP_TEAMS_OBJ)
						INT iTeamLoop
						
						FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
							IF DOES_TEAM_LIKE_TEAM(iTeam, iTeamLoop)
								IF MC_serverBD.iNumOfWanted[iTeamLoop] > 0
									bLoseCops = TRUE
									iTeamLoop = FMMC_MAX_TEAMS // Break out
								ENDIF
							ENDIF
						ENDFOR
					ENDIF
				ENDIF
				
			ENDIF
			
		ENDIF
	ENDIF
	
	RETURN bLoseCops

ENDFUNC

SCRIPT_TIMER stDelayShard

FUNC BOOL SHOULD_SHOW_SHARD_FOR_F_VS_J()
	IF NOT HAS_NET_TIMER_STARTED(stDelayShard)
		START_NET_TIMER(stDelayShard)
	ELSE
		IF HAS_NET_TIMER_EXPIRED(stDelayShard,1500)
			RESET_NET_TIMER(stDelayShard)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC



PROC UPDATE_STORED_TEAM_SCORE_FOR_SHARD()
	
	iStoreLastTeam0Score = GET_TEAM_SCORE_FOR_DISPLAY(0)
	PRINTLN("[RCC MISSION][INCH SHARD] UPDATE_STORED_TEAM_SCORE_FOR_SHARD - Team Score updated - iStoreLastTeam0Score taem 0: ", iStoreLastTeam0Score)

	iStoreLastTeam1Score = GET_TEAM_SCORE_FOR_DISPLAY(1)
	PRINTLN("[RCC MISSION][INCH SHARD] UPDATE_STORED_TEAM_SCORE_FOR_SHARD - Team Score updated - iStoreLastTeam1Score team 1: ", iStoreLastTeam1Score)

ENDPROC

PROC ROLLING_SHARD_SCORE_UPDATE()
	IF MC_serverBD.iTeamScore[0] != iStoreLastTeam0Score OR MC_serverBD.iTeamScore[1] != iStoreLastTeam1Score
		PRINTLN("ROLLING_SHARD_SCORE_UPDATE - Cached score didn't match current score. Updating.")
		UPDATE_STORED_TEAM_SCORE_FOR_SHARD()
	ENDIF
ENDPROC
FUNC STRING GET_TW_SCALEFORM_STATE_AS_STRING(TurfWarHUDState eState)
	SWITCH eState
		CASE TW_REQUEST_ASSETS	RETURN "TW_REQUEST_ASSETS"
		CASE TW_INIT			RETURN "TW_INIT"
		CASE TW_RUNNING			RETURN "TW_RUNNING"
	ENDSWITCH
	RETURN "State invalid, maybe GET_TW_SCALEFORM_STATE_AS_STRING needs updated"
ENDFUNC

PROC SET_TW_SCALEFORM_STATE(TurfWarHUDState eState)
	PRINTLN("[LM][TWHUD] - GET_TW_SCALEFORM_STATE_AS_STRING - State changed from ",
			GET_TW_SCALEFORM_STATE_AS_STRING(eTWHUDState), " TO ",
			GET_TW_SCALEFORM_STATE_AS_STRING(eState))
	eTWHUDState = eState
ENDPROC

PROC MANAGE_TURF_WAR_UI(FLOAT fTimeProgressed, FLOAT fTimeMax, INT iTeamHighest)
	INT iTeam = MC_playerBD[iPartToUse].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		
	IF (IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARS(g_FMMC_STRUCT.iAdversaryModeType) OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARZONE(g_FMMC_STRUCT.iAdversaryModeType))
	AND g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionScoreLimit - g_FMMC_STRUCT.iInitialPoints[0] > 0
		INT iTargetScore = g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionScoreLimit - g_FMMC_STRUCT.iInitialPoints[0]
		DRAW_GENERIC_BIG_NUMBER(iTargetScore, "MC_OSCRLIMY", -1, HUD_COLOUR_WHITE,HUDORDER_SEVENTHBOTTOM)
	ENDIF

	SWITCH eTWHUDState

		CASE TW_REQUEST_ASSETS
			IF HAS_SCALEFORM_MOVIE_LOADED(SF_TWH_MovieIndex)
				SET_TW_SCALEFORM_STATE(TW_INIT)
			ELSE
				PRINTLN("[LM][TWHUD] - MANAGE_TURF_WAR_UI - Requesting Scaleform Movie")
				SF_TWH_MovieIndex = REQUEST_SCALEFORM_MOVIE("POWER_PLAY_TURF")
			ENDIF
		BREAK
		
		CASE TW_INIT
			PRINTLN("[LM][TWHUD] - Setting bubbles equal to: g_FMMC_STRUCT.iNumberOfTeams: ", g_FMMC_STRUCT.iNumberOfTeams)
			
			BEGIN_SCALEFORM_MOVIE_METHOD (SF_TWH_MovieIndex, "SET_NUMBER_OF_TEAMS")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_FMMC_STRUCT.iNumberOfTeams)
			END_SCALEFORM_MOVIE_METHOD()
			
			BEGIN_SCALEFORM_MOVIE_METHOD (SF_TWH_MovieIndex, "SETUP_TEAM_COLOURS")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(GET_HUD_COLOUR_FOR_FMMC_TEAM(0,PLAYER_ID()))) // Hud colour for team one
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(GET_HUD_COLOUR_FOR_FMMC_TEAM(1,PLAYER_ID()))) // Hud colour for team two
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(GET_HUD_COLOUR_FOR_FMMC_TEAM(2,PLAYER_ID()))) // Hud colour for team three
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(GET_HUD_COLOUR_FOR_FMMC_TEAM(3,PLAYER_ID()))) // Hud colour for team four
			END_SCALEFORM_MOVIE_METHOD()
			
			SET_TW_SCALEFORM_STATE(TW_RUNNING)
		BREAK
		
		CASE TW_RUNNING
			IF iRule < FMMC_MAX_RULES
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_GIVE_POINTS_TURF_WAR_RULE_CHANGE)
				OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_TURF_WAR_CUSTOM_SCORING)
				AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciENABLE_TURF_WAR_INCREMENT_SCORE_LOGIC))
					BEGIN_SCALEFORM_MOVIE_METHOD (SF_TWH_MovieIndex, "SET_TEAM_SCORES")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(MC_ServerBD.iTeamScore[0] - g_FMMC_STRUCT.iInitialPoints[0]) // The score for Team 1
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(MC_ServerBD.iTeamScore[1] - g_FMMC_STRUCT.iInitialPoints[1]) // The score for Team 2
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(MC_ServerBD.iTeamScore[2] - g_FMMC_STRUCT.iInitialPoints[2]) // The score for Team 3
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(MC_ServerBD.iTeamScore[3] - g_FMMC_STRUCT.iInitialPoints[3]) // The score for Team 4
					END_SCALEFORM_MOVIE_METHOD()
				ENDIF
			ENDIF
	
			IF NOT HAS_NET_TIMER_STARTED(tdPropResetClearBubble)
			OR HAS_NET_TIMER_EXPIRED_READ_ONLY(tdPropResetClearBubble, ci_Prop_Reset_Clear_Bubble)
				BEGIN_SCALEFORM_MOVIE_METHOD (SF_TWH_MovieIndex, "SET_ICON_TIMERS")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(MC_ServerBD_4.iPropsAmountOwnedByTeam[0])/TO_FLOAT(iTurfWarTotalClaimableProps)) // The score for Team 1
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(MC_ServerBD_4.iPropsAmountOwnedByTeam[1])/TO_FLOAT(iTurfWarTotalClaimableProps)) // The score for Team 2
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(MC_ServerBD_4.iPropsAmountOwnedByTeam[2])/TO_FLOAT(iTurfWarTotalClaimableProps)) // The score for Team 3
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(MC_ServerBD_4.iPropsAmountOwnedByTeam[3])/TO_FLOAT(iTurfWarTotalClaimableProps)) // The score for Team 4
					
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fTimeProgressed / fTimeMax)
					
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(iTeamHighest))
				END_SCALEFORM_MOVIE_METHOD()
			ELSE
				BEGIN_SCALEFORM_MOVIE_METHOD (SF_TWH_MovieIndex, "SET_ICON_TIMERS")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.0) // The score for Team 1
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.0) // The score for Team 2
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.0) // The score for Team 3
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.0) // The score for Team 4

					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.0) // Timer
				END_SCALEFORM_MOVIE_METHOD()
			ENDIF
			
			IF IS_BIT_SET(iLocalBoolCheck20, LBOOL20_TURFWAR_ICON_SHOULD_PULSE_0)
				BEGIN_SCALEFORM_MOVIE_METHOD (SF_TWH_MovieIndex, "PULSE_ICON")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
				END_SCALEFORM_MOVIE_METHOD()
				CLEAR_BIT(iLocalBoolCheck20, LBOOL20_TURFWAR_ICON_SHOULD_PULSE_0)
			ENDIF
			IF IS_BIT_SET(iLocalBoolCheck20, LBOOL20_TURFWAR_ICON_SHOULD_PULSE_1)
				BEGIN_SCALEFORM_MOVIE_METHOD (SF_TWH_MovieIndex, "PULSE_ICON")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
				END_SCALEFORM_MOVIE_METHOD()
				CLEAR_BIT(iLocalBoolCheck20, LBOOL20_TURFWAR_ICON_SHOULD_PULSE_1)
			ENDIF
			IF IS_BIT_SET(iLocalBoolCheck20, LBOOL20_TURFWAR_ICON_SHOULD_PULSE_2)
				BEGIN_SCALEFORM_MOVIE_METHOD (SF_TWH_MovieIndex, "PULSE_ICON")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
				END_SCALEFORM_MOVIE_METHOD()
				CLEAR_BIT(iLocalBoolCheck20, LBOOL20_TURFWAR_ICON_SHOULD_PULSE_2)
			ENDIF
			IF IS_BIT_SET(iLocalBoolCheck20, LBOOL20_TURFWAR_ICON_SHOULD_PULSE_3)
				BEGIN_SCALEFORM_MOVIE_METHOD (SF_TWH_MovieIndex, "PULSE_ICON")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3)
				END_SCALEFORM_MOVIE_METHOD()
				CLEAR_BIT(iLocalBoolCheck20, LBOOL20_TURFWAR_ICON_SHOULD_PULSE_3)
			ENDIF
		BREAK
		
	ENDSWITCH
			
	bTurfWarScaleFormActive = FALSE
	
	IF eTWHUDState > TW_REQUEST_ASSETS
		IF NOT IS_HUD_COMPONENT_ACTIVE(NEW_HUD_RADIO_STATIONS)
		AND NOT IS_BIT_SET(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_READY_FOR_LEADERBOARD) //IS_IT_OK_TO_DRAW_LEADERBOARD
			IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appinternet")) < 1 )
			OR NOT IS_BROWSER_OPEN()
				DRAW_SCALEFORM_MOVIE_FULLSCREEN(SF_TWH_MovieIndex,255,255,255,255)
				bTurfWarScaleFormActive = TRUE
				IF bTurfWarScaleFormActive
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_RUINER_AMMO_UI()
	IF IS_BIT_SET(iLocalBoolCheck22, LBOOL22_USING_CUSTOM_VEHICLE_AMMO)
		INT iTeam = MC_PlayerBD[iLocalPart].iteam
		INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
			
		IF iTeam < FMMC_MAX_TEAMS
		AND iRule < FMMC_MAX_RULES
			IF NOT IS_PED_INJURED(localPlayerPed)
			AND IS_PED_IN_ANY_VEHICLE(localPlayerPed)
				
				VEHICLE_INDEX playerVeh = GET_VEHICLE_PED_IS_IN(localPlayerPed)
				WEAPON_TYPE wtCurrentRuinerWeapon
				
				IF GET_CURRENT_PED_VEHICLE_WEAPON(localPlayerPed, wtCurrentRuinerWeapon)
					IF wtCurrentRuinerWeapon = WEAPONTYPE_DLC_VEHICLE_RUINER_BULLET
						IF iRuinerMGAmmo > -1
							DRAW_GENERIC_BIG_DOUBLE_NUMBER(GET_VEHICLE_WEAPON_RESTRICTED_AMMO(playerVeh, 0), iRuinerMGAmmoMax, "AMMO_RUIN_MG")
						ENDIF
					ELIF wtCurrentRuinerWeapon = WEAPONTYPE_DLC_VEHICLE_RUINER_ROCKET
					AND IS_PLAYER_VEHICLE_WEAPON_TOGGLED_TO_NON_HOMING(PLAYER_ID())
						IF iRuinerRocketsAmmo > -1
							DRAW_GENERIC_BIG_DOUBLE_NUMBER(GET_VEHICLE_WEAPON_RESTRICTED_AMMO(playerVeh, 1), iRuinerRocketsAmmoMax, "AMMO_RUIN_RO")
						ENDIF
					ELIF wtCurrentRuinerWeapon = WEAPONTYPE_DLC_VEHICLE_RUINER_ROCKET
					AND NOT IS_PLAYER_VEHICLE_WEAPON_TOGGLED_TO_NON_HOMING(PLAYER_ID())
						IF iRuinerRocketsHomingAmmo > -1
							DRAW_GENERIC_BIG_DOUBLE_NUMBER(GET_VEHICLE_WEAPON_RESTRICTED_AMMO(playerVeh, 1), iRuinerRocketsHomingAmmoMax, "AMMO_RUIN_HR")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_TURFWARS_BARS()

	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciENABLE_TURF_WAR_PROP_CLAIM_METERS)
		INT iTeam	
		FOR iTeam = 0 TO (FMMC_MAX_TEAMS-1)
			IF iTeam < g_FMMC_STRUCT.iNumberOfTeams
				INT iRule = MC_serverBD_4.iLastHighestPriority[iTeam]
				IF iRule < FMMC_MAX_RULES
					IF NOT HAS_TEAM_FINISHED(iTeam)
					AND NOT HAS_TEAM_FAILED(iTeam)
						INT iScore = MC_ServerBD_4.iPropsAmountOwnedByTeam[iTeam]
						TEXT_LABEL_15 tl15 = g_sMission_TeamName[iteam]
						
						PLAYER_INDEX playerid = INT_TO_PLAYERINDEX(iPartToUse)
						
						DRAW_GENERIC_METER(iScore, iTurfWarTotalClaimableProps, tl15, GET_PLAYER_HUD_COLOUR(playerid, iTeam), DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT,DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

PROC DISPLAY_COUNTERMEASURE_COOLDOWN()

	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND NOT IS_PED_DEAD_OR_DYING(PlayerPedToUse)
	AND NOT IS_PLAYER_RESPAWNING(LocalPlayer)
	
		BOOL bUsingChaff = GET_VEHICLE_MOD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), MOD_BUMPER_F) =  0
		INT iCooldownLength = 2500
		SCRIPT_TIMER stCooldownTimer
		
		IF bUsingChaff
			stCooldownTimer = GET_CHAFF_COOLDOWN_TIMER()
			iCooldownLength = g_sMPTunables.iSMUGGLER_CHAFF_COOLDOWN + g_sMPTunables.iSMUGGLER_CHAFF_DURATION
							
			IF g_bOverrideChaffCooldown
				iCooldownLength = g_sMPTunables.iSMUGGLER_CHAFF_DURATION + g_iChaffCooldown
			ENDIF
			
			PRINTLN("[HUD] Using chaff, Cooldown length is ", iCooldownLength)
		ELSE
			stCooldownTimer = GET_FLARE_COOLDOWN_TIMER()
			iCooldownLength = 2500
		
			IF g_bOverrideFlareCooldown
				iCooldownLength = g_iFlareCooldown
			ENDIF
			
			PRINTLN("[HUD] Using flares, Cooldown length is ", iCooldownLength)
		ENDIF

		IF HAS_NET_TIMER_STARTED(stCooldownTimer)
			INT iTimeSinceLastFired = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stCooldownTimer)
			INT iMaxCooldown = iCooldownLength
			TEXT_LABEL_15 tl15 = "CM_COOLDOWN_UI"
			
			INT iNum = (iMaxCooldown - iTimeSinceLastFired)
			PRINTLN("[HUD] Countermeasure Cooldown meter: ", iNum, " / ", iMaxCooldown)
			
			DRAW_GENERIC_METER(iMaxCooldown - iNum, iMaxCooldown, tl15, HUD_COLOUR_ORANGE)
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_GUNSMITH_BARS()
	INT iTeam	
	FOR iTeam = 0 TO (FMMC_MAX_TEAMS-1)
		IF iTeam < g_FMMC_STRUCT.iNumberOfTeams
			INT iRule = MC_serverBD_4.iLastHighestPriority[iTeam]
			IF iRule < FMMC_MAX_RULES
				IF NOT HAS_TEAM_FINISHED(iTeam)
				AND NOT HAS_TEAM_FAILED(iTeam)
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_ENABLE_GUN_ROULETTE_UI_METERS )
						
						IF iScoreUICached[iTeam] != MC_serverBD.iScoreOnThisRule[iTeam]
						OR MC_serverBD.iScoreOnThisRule[iTeam] = 0
							iScoreUICached[iTeam]						= MC_serverBD.iScoreOnThisRule[iTeam]
							
							iRequiredScoreUICached[iTeam]				= ROUND(GET_FMMC_OBJECTIVE_SCORE_LIMIT_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTargetScore[iRule], iTeam)*GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING(iTeam, iRule))
						ENDIF
						
						IF iActualWeaponIndexUICached[iTeam] != ENUM_TO_INT(GET_WEAPON_FOR_GUNSMITH(iRule, iTeam))
						AND iScoreUICached[iTeam] = MC_serverBD.iScoreOnThisRule[iTeam]
						OR MC_serverBD.iScoreOnThisRule[iTeam] = 0
							iActualWeaponIndexUICached[iTeam]			= ENUM_TO_INT(GET_WEAPON_FOR_GUNSMITH(iRule, iTeam))
						ENDIF
						
						WEAPON_TYPE eWeaponType							= INT_TO_ENUM(WEAPON_TYPE, iActualWeaponIndexUICached[iTeam])
						
						TEXT_LABEL_63 tl63 = ""
						tl63 += g_sMission_TeamName[iTeam]
						tl63 += " "
						tl63 += (iRule+1)
						tl63 += "/"
						tl63 += g_FMMC_STRUCT.sFMMCEndConditions[iteam].iNumberOfTeamRules
																	
						PLAYER_INDEX playerid = INT_TO_PLAYERINDEX(iPartToUse)
						
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_YOUR_TEAM_AT_TOP_OF_HUD_SPRITE_METERS )
						AND iTeam = MC_PlayerBD[iPartToUse].iteam
							DRAW_GENERIC_WEAPON_SPRITE_METER(iScoreUICached[iTeam], iRequiredScoreUICached[iTeam], tl63, eWeaponType, GET_PLAYER_HUD_COLOUR(playerid, iTeam), 
								DEFAULT, HUDORDER_EIGHTHBOTTOM, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE, DEFAULT, GET_PLAYER_HUD_COLOUR(playerid, iTeam))
						ELSE
							DRAW_GENERIC_WEAPON_SPRITE_METER(iScoreUICached[iTeam], iRequiredScoreUICached[iTeam], tl63, eWeaponType, GET_PLAYER_HUD_COLOUR(playerid, iTeam), 
								DEFAULT, HUDORDER_SEVENTHBOTTOM, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE, DEFAULT, GET_PLAYER_HUD_COLOUR(playerid, iTeam))
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRQUOTA(g_FMMC_STRUCT.iAdversaryModeType)
		FOR iTeam = 0 TO (FMMC_MAX_TEAMS-1)
			IF iTeam < g_FMMC_STRUCT.iNumberOfTeams
				INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
				
				IF iRule < FMMC_MAX_RULES
					IF NOT HAS_TEAM_FINISHED(iTeam)
					AND NOT HAS_TEAM_FAILED(iTeam)
						//IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_ENABLE_GUN_ROULETTE_UI_METERS )
							
							IF iScoreUICached[iTeam] != MC_serverBD.iScoreOnThisRule[iTeam]
							OR MC_serverBD.iScoreOnThisRule[iTeam] = 0
								iScoreUICached[iTeam]						= MC_serverBD.iScoreOnThisRule[iTeam]
								
								iRequiredScoreUICached[iTeam]				= ROUND(GET_FMMC_OBJECTIVE_SCORE_LIMIT_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTargetScore[iRule], iTeam)*GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING(iTeam, iRule))
							ENDIF
							
							MODEL_NAMES modelName = MC_serverBD_4.sVehicleSwaps[iTeam][iRule].mnVehicleModelSwap
							TEXT_LABEL_63 tl63 = ""
							tl63 += g_sMission_TeamName[iTeam]
							tl63 += " "
							tl63 += (iRule+1)
							tl63 += "/"
							tl63 += g_FMMC_STRUCT.sFMMCEndConditions[iteam].iNumberOfTeamRules
																		
							PLAYER_INDEX playerid = INT_TO_PLAYERINDEX(iPartToUse)
								
							// plays the sound on the exact moment the score Changes
							IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_SHOULD_WAIT_TO_PLAY_AQ_POINT_GAINED)
							//AND MC_ServerBD.iScoreOnThisRule[MC_PlayerBD[iPartToUse].iteam] != 0
							//AND MC_ServerBD.iScoreOnThisRule[MC_PlayerBD[iPartToUse].iteam] != iScoreUICached[MC_PlayerBD[iPartToUse].iteam]
								PLAY_SOUND_FRONTEND(-1, "Gain_Point" ,"dlc_xm_aqo_sounds", false)
								CLEAR_BIT(iLocalBoolCheck25, LBOOL25_SHOULD_WAIT_TO_PLAY_AQ_POINT_GAINED)
								PRINTLN("[AQSND] Playing Gain_Point now!")
							ENDIF
							
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_YOUR_TEAM_AT_TOP_OF_HUD_SPRITE_METERS )
							AND iTeam = MC_PlayerBD[iPartToUse].iteam
								DRAW_GENERIC_MODEL_SPRITE_METER(iScoreUICached[iTeam], iRequiredScoreUICached[iTeam], tl63, modelName, GET_PLAYER_HUD_COLOUR(playerid, iTeam), 
									DEFAULT, HUDORDER_FOURTHBOTTOM, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE, DEFAULT, GET_PLAYER_HUD_COLOUR(playerid, iTeam))
							ELSE
								DRAW_GENERIC_MODEL_SPRITE_METER(iScoreUICached[iTeam], iRequiredScoreUICached[iTeam], tl63, modelName, GET_PLAYER_HUD_COLOUR(playerid, iTeam), 
									DEFAULT, HUDORDER_THIRDBOTTOM, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE, DEFAULT, GET_PLAYER_HUD_COLOUR(playerid, iTeam))
							ENDIF
						//ENDIF
					ELSE
						PRINTLN("[AirQuotaHud] Team ", iTeam, " has either finished or failed")
					ENDIF
				ELSE
					PRINTLN("[][AirQuotaHud] iRule is too high: ", iRule)
				ENDIF
			ELSE
				PRINTLN("[][AirQuotaHud] iTeam is higher than the amount of teams / iTeam: ", iTeam, " / Num of Teams: ", g_FMMC_STRUCT.iNumberOfTeams)
			ENDIF
		ENDFOR
		
		HANDLE_AIRQUOTA_RADIO()
	ENDIF
ENDPROC

PROC CREATE_CUSTOM_STRAPLINE_FOR_LOST_AND_DAMNED(INT iStraplineValue)
	STRING sLiteralString
	INT iPointsDifference
	INT iTeam
	FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
		IF IS_TEAM_ACTIVE(iTeam)
			IF iTeam != MC_playerBD[iPartToUse].iteam
				IF MC_serverBD.iTeamScore[iTeam] > MC_serverBD.iTeamScore[MC_playerBD[iPartToUse].iteam]
					iPointsDifference = MC_serverBD.iTeamScore[iTeam] - MC_serverBD.iTeamScore[MC_playerBD[iPartToUse].iteam]
					sLiteralString = GET_FILENAME_FOR_AUDIO_CONVERSATION("SHD_TAG_BEHIND")
				ELIF MC_serverBD.iTeamScore[iTeam] < MC_serverBD.iTeamScore[MC_playerBD[iPartToUse].iteam]
					iPointsDifference = MC_serverBD.iTeamScore[MC_playerBD[iPartToUse].iteam] - MC_serverBD.iTeamScore[iTeam]
					sLiteralString = GET_FILENAME_FOR_AUDIO_CONVERSATION("SHD_TAG_AHEAD")
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	STRING sStringToReturn = GET_FMMC_SHARD_STRAPLINE_FROM_CREATOR_INT(iStraplineValue)
	
	//if the teams are tied
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] = 0
		IF MC_playerBD[iPartToUse].iteam = 0
		AND iStraplineValue != 24
			sStringToReturn = "SHD_TAG_DSTART"
		ELIF MC_playerBD[iPartToUse].iteam = 1
		AND iStraplineValue != 23
			sStringToReturn = "SHD_TAG_ASTART"
		ENDIF
	ELIF iPointsDifference = 0
	AND IS_STRING_NULL_OR_EMPTY(sLiteralString)
		IF MC_playerBD[iPartToUse].iteam = 0
		AND iStraplineValue != 24
			sStringToReturn = "SHD_TAG_DTIED"
		ELIF MC_playerBD[iPartToUse].iteam = 1
		AND iStraplineValue != 23
			sStringToReturn = "SHD_TAG_ATIED"
		ENDIF
	//if there is a team 1 point ahead
	ELIF iPointsDifference = 1
		IF MC_playerBD[iPartToUse].iteam = 0
		AND iStraplineValue != 24
			sStringToReturn = "SHD_TAG_DSTRONG1"
		ELIF MC_playerBD[iPartToUse].iteam = 1
		AND iStraplineValue != 23
			sStringToReturn = "SHD_TAG_ASTRONG1"
		ENDIF
	ENDIF
	
	SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_AND_INT(BIG_MESSAGE_GENERIC_TEXT, iPointsDifference, sStringToReturn, sLiteralString, GET_FMMC_SHARD_STRING_FROM_CREATOR_INT(iStraplineValue))

ENDPROC

PROC PROCES_SHARD_TEXT_PLAYED(INT iTeam)
	SET_BIT(iLocalBoolCheck18, LBOOL18_RULE_CHANGE_SHARD_PLAYED_TEAM_0 + iTeam)
	SET_BIT(iLocalBoolCheck18, LBOOL18_RULE_CHANGE_SHARD_FIRST_RULE)
ENDPROC

FUNC INT GET_HASH_FOR_AIR_QUOTA_VEHICLE_ICON(INT iTeam, INT iRule)	
	RETURN ENUM_TO_INT(MC_serverBD_4.sVehicleSwaps[iTeam][iRule].mnVehicleModelSwap)
ENDFUNC

PROC PROCESS_ARENA_CTF_ANNOUNCER_DELIVERY()
	IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_ARENA_CTF(g_FMMC_STRUCT.iAdversaryModeType)
		EXIT
	ENDIF
	
	INT iTeam0Score = GET_TEAM_SCORE_FOR_DISPLAY(0)
	INT iTeam1Score = GET_TEAM_SCORE_FOR_DISPLAY(1)
	INT iLosingTeam = -1
	
	IF iTeam0Score > iTeam1Score
		iLosingTeam = 1
	ELIF iTeam1Score > iTeam0Score
		iLosingTeam = 0
	ENDIF
	PRINTLN("PROCESS_ARENA_CTF_ANNOUNCER_DELIVERY - SCORES - Team 0: ",iTeam0Score," Team 1: ",iTeam1Score," Current losing team ", iLosingTeam)
	
	IF (iTeam0Score = 3 AND iTeam1Score = 0)
	OR (iTeam0Score = 0 AND iTeam1Score = 3)
		SET_ARENA_ANNOUNCER_BS_MODE_SPECIFIC(g_iAA_PlaySound_CTF_3to0)
	ELIF iLosingTeam != -1
	AND iCTFLastLosingTeam != iLosingTeam
	AND IS_BIT_SET(iLocalBoolCheck30, LBOOL30_A_FLAG_HAS_BEEN_DELIVERED)
		iCTFLastLosingTeam = iLosingTeam
		PRINTLN("PROCESS_ARENA_CTF_ANNOUNCER_DELIVERY - Setting iCTFLastLosingTeam to ", iCTFLastLosingTeam)
		SET_ARENA_ANNOUNCER_BS_MODE_SPECIFIC(g_iAA_PlaySound_CTF_TeamComeback)
	ELIF ABSI(iTeam0Score - iTeam1Score) >= 3
		SET_ARENA_ANNOUNCER_BS_MODE_SPECIFIC(g_iAA_PlaySound_CTF_LosingBadly)
	ELSE
		SET_ARENA_ANNOUNCER_BS_MODE_SPECIFIC(g_iAA_PlaySound_CTF_FlagDelivered)
		SET_BIT(iLocalBoolCheck30, LBOOL30_A_FLAG_HAS_BEEN_DELIVERED)
		PLAY_ARENA_CROWD_CHEER_AUDIO()
		PRINTLN("[CROWD CHEER] Flag War - Dropped a flag off to score")
	ENDIF
	
ENDPROC

PROC PROCESS_SHARD_TEXT()
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
			
	IF NOT IS_BIT_SET(iLocalBoolCheck18, LBOOL18_RULE_CHANGE_SHARD_PLAYED_TEAM_0 + MC_playerBD[iPartToUse].iteam)
	OR HAS_NET_TIMER_STARTED(stDelayShard)
	OR NOT IS_BIT_SET(iLocalBoolCheck18, LBOOL18_RULE_CHANGE_SHARD_FIRST_RULE)
		IF iRule < FMMC_MAX_RULES
		AND (NOT IS_PED_INJURED(localPlayerPed) OR IS_LOCAL_PLAYER_ANY_SPECTATOR())
			PROCES_SHARD_TEXT_PLAYED(iTeam)
			INT shardText = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iShardText[iRule]
			sLastShardText = GET_FMMC_SHARD_STRING_FROM_CREATOR_INT(shardText)
			
			IF shardText > 0
			AND (g_FMMC_STRUCT.sFMMCEndConditions[iteam].iMissionLapLimit = 0 OR MC_ServerBD_4.iTeamLapsCompleted[iteam] != g_FMMC_STRUCT.sFMMCEndConditions[iteam].iMissionLapLimit)			
				IF shardText = 1					
					IF SHOULD_SHOW_SHARD_FOR_F_VS_J()
						PRINTLN("[RCC MISSION] PROCESS_SHARD_TEXT - Displaying shard ",shardText," for team ",iTeam," / rule ",iRule)
						STRING sSoundSet = "DLC_HALLOWEEN_FVJ_Sounds"
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_SLASHERS_AUDIO)
							sSoundSet = "dlc_xm_sls_Sounds"
						ENDIF
						PLAY_SOUND_FRONTEND(-1, "Swap_Sides", sSoundSet, FALSE)
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, GET_FMMC_SHARD_STRING_FROM_CREATOR_INT(shardText))
					ENDIF
				ELIF shardText = 2 //HUNT DOWN
					PRINTLN("[RCC MISSION] PROCESS_SHARD_TEXT - Displaying HUNT DOWN shard ",shardText," for team ",iTeam," / rule ",iRule)
					PLAY_SOUND_FRONTEND(-1, "Swap_Sides", "DLC_HALLOWEEN_FVJ_Sounds", FALSE)
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, GET_FMMC_SHARD_STRING_FROM_CREATOR_INT(shardText))
				ELIF shardText = 3 //MINIMUM SPEED INCREASED!
					PRINTLN("[RCC MISSION] PROCESS_SHARD_TEXT - Displaying MINIMUM SPEED INCREASED! shard ",shardText," for team ",iTeam," / rule ",iRule)
					
					IF MC_ServerBD_4.iTeamLapsCompleted[iTeam] > 0 AND MC_ServerBD_4.iTeamLapsCompleted[iTeam] != (g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMissionLapLimit-1)
						PRINTLN("[RCC MISSION] PROCESS_SHARD_TEXT LAPS OK")
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, GET_FMMC_SHARD_STRING_FROM_CREATOR_INT(shardText))
					ENDIF
				//custom shards for different levels
				ELIF (shardText > ciSTART_CUSTOM_LEVEL_SHARDS_WITH_STRAPLINES
				AND shardText < ciEND_CUSTOM_LEVEL_SHARDS_WITH_STRAPLINES)
				OR (shardText > ciSTART_CUSTOM_TLAD_SHARD_WITH_STRAPLINE
				AND shardText < ciEND_CUSTOM_TLAD_SHARD_WITH_STRAPLINE)
					IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_LOST_DAMNED(g_FMMC_STRUCT.iAdversaryModeType)
						IF NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
						AND GET_TIME_REMAINING_ON_MULTIRULE_TIMER() > 2000
							CREATE_CUSTOM_STRAPLINE_FOR_LOST_AND_DAMNED(shardText)
							PRINTLN("[JT SHARD] MR remaining: ", GET_TIME_REMAINING_ON_MULTIRULE_TIMER())
						ENDIF
					ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TOUR_DE_FORCE(g_FMMC_STRUCT.iAdversaryModeType)
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, GET_FMMC_SHARD_STRING_FROM_CREATOR_INT(shardText), GET_FMMC_SHARD_STRAPLINE_FROM_CREATOR_INT(shardText))
					ELIF shardText = ciVEHICLE_SWAP_SHARD_TEXT
						IF MC_serverBD_4.sVehicleSwaps[iTeam][iRule].mnVehicleModelSwap != DUMMY_MODEL_FOR_SCRIPT
						AND NOT g_bMissionEnding
						AND NOT g_bMissionOver
							IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRQUOTA(g_FMMC_STRUCT.iAdversaryModeType)
								CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_WEAPON_IMAGE)
								SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_WEAPON_IMAGE, GET_HASH_FOR_AIR_QUOTA_VEHICLE_ICON(iTeam, iRule), GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(MC_serverBD_4.sVehicleSwaps[iTeam][iRule].mnVehicleModelSwap), "SHD_NVEH", GET_PLAYER_HUD_COLOUR(LocalPlayer, MC_PlayerBD[iLocalPart].iTeam))
								PRINTLN("[RCC MISSION][TMS] Playing Air Quota's swap vehicle shard")
								
								SET_BIT(iLocalBoolCheck18, LBOOL18_RULE_CHANGE_SHARD_PLAYED_TEAM_0)
								SET_BIT(iLocalBoolCheck18, LBOOL18_RULE_CHANGE_SHARD_PLAYED_TEAM_1)
								SET_BIT(iLocalBoolCheck18, LBOOL18_RULE_CHANGE_SHARD_PLAYED_TEAM_2)
								SET_BIT(iLocalBoolCheck18, LBOOL18_RULE_CHANGE_SHARD_PLAYED_TEAM_3)
								PRINTLN("[RCC MISSION][TMS] Setting LBOOL18_RULE_CHANGE_SHARD_PLAYED_TEAM_ for all teams")
							ELSE
								SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_GENERIC_TEXT, "STRING", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].mnVehicleModelSwap[MC_serverBD_4.iCurrentHighestPriority[iTeam]]), DEFAULT, DEFAULT, "SHD_NVEH")
							ENDIF
						ENDIF
					ELSE
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, GET_FMMC_SHARD_STRING_FROM_CREATOR_INT(shardText), GET_FMMC_SHARD_STRAPLINE_FROM_CREATOR_INT(shardText))
					ENDIF
				ELIF shardText >= ciEND_CUSTOM_LEVEL_SHARDS_WITH_STRAPLINES
				AND shardText <= ciEND_CUSTOM_LEVEL_SHARD_TEXT
				OR shardText = ci10_CUSTOM_LEVEL_SHARD_TEXT
					IF NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
					AND NOT HAS_TEAM_FINISHED(iTeam)
					AND NOT HAS_TEAM_FAILED(iTeam)
					AND NOT HAS_MULTI_RULE_TIMER_EXPIRED(iTeam)
						IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_GUNSMITH(g_FMMC_STRUCT.iAdversaryModeType)
							CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_WEAPON_IMAGE)
							SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_WEAPON_IMAGE, ENUM_TO_INT(GET_WEAPON_FOR_GUNSMITH(iRule, iTeam)), GET_WEAPON_NAME(GET_WEAPON_FOR_GUNSMITH(iRule, iTeam)), "SHD_NWEAP", GET_PLAYER_HUD_COLOUR(LocalPlayer, MC_PlayerBD[iLocalPart].iTeam))
							PRINTLN("[RCC MISSION] Playing shard Gunsmith Shard")
						ELSE
							SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT,  GET_FMMC_SHARD_STRING_FROM_CREATOR_INT(shardText))
							PRINTLN("[RCC MISSION] Playing shard Level Up/Progression shard")
						ENDIF
					ENDIF
				ELSE
					RESET_NET_TIMER(stDelayShard) //This is a bypass for the new rule check, so shouldn't run unless we are doing the above option.
					PRINTLN("[RCC MISSION] PROCESS_SHARD_TEXT - Displaying shard ",shardText," for team ",iTeam," / rule ",iRule)
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, GET_FMMC_SHARD_STRING_FROM_CREATOR_INT(shardText))
				ENDIF
			ENDIF
		
			IF NOT IS_BIT_SET(iLocalBoolCheck19, LBOOL19_NEED_TO_SHOW_LAP_SHARD)
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iRule].iLocBS2, ciLoc_BS2_ShowNewLapShard)
					IF g_FMMC_STRUCT.sFMMCEndConditions[iteam].iMissionLapLimit > 0 
						IF MC_ServerBD_4.iTeamLapsCompleted[iteam] != g_FMMC_STRUCT.sFMMCEndConditions[iteam].iMissionLapLimit
							//Display Laps
							SET_BIT(iLocalBoolCheck19, LBOOL19_NEED_TO_SHOW_LAP_SHARD)
							iCachedLapCount = MC_ServerBD_4.iTeamLapsCompleted[iteam]
							PRINTLN("[RCC MISSION] PROCESS_SHARD_TEXT - Need to show shard when score updates - cached score is: ", iCachedLapCount)
						ELSE
							PRINTLN("[RCC MISSION] PROCESS_SHARD_TEXT - NOT Displaying laps shard - MC_ServerBD_4.iTeamLapsCompleted[iteam]: ", MC_ServerBD_4.iTeamLapsCompleted[iteam], "g_FMMC_STRUCT.sFMMCEndConditions[iteam].iMissionLapLimit: ", g_FMMC_STRUCT.sFMMCEndConditions[iteam].iMissionLapLimit )
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck19, LBOOL19_NEED_TO_SHOW_LAP_SHARD)
		IF iCachedLapCount != MC_ServerBD_4.iTeamLapsCompleted[iteam]
			IF MC_ServerBD_4.iTeamLapsCompleted[iteam] < g_FMMC_STRUCT.sFMMCEndConditions[iteam].iMissionLapLimit
				PRINTLN("[RCC MISSION] PROCESS_SHARD_TEXT - Show shard lap shard ")
				IF CONTENT_IS_USING_ARENA()
					PLAY_SOUND_FRONTEND(-1, "Checkpoint_Lap", "DLC_AW_Frontend_Sounds")
				ELSE
					PLAY_SOUND_FRONTEND(-1, "Checkpoint_Lap", "DLC_Stunt_Race_Frontend_Sounds")
				ENDIF
				SETUP_NEW_BIG_MESSAGE_WITH_2_INTS(BIG_MESSAGE_LAP, MC_ServerBD_4.iTeamLapsCompleted[iteam] +1  , g_FMMC_STRUCT.sFMMCEndConditions[iteam].iMissionLapLimit)
				CLEAR_BIT(iLocalBoolCheck19, LBOOL19_NEED_TO_SHOW_LAP_SHARD)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciSHOW_LAST_DELIVERER_SHARD)
	
		IF piLastDeliverer != INVALID_PLAYER_INDEX()
		AND IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
			PRINTLN("[KH] PROCESS_SHARD_TEXT - STARTING TIMER")
			IF NOT HAS_NET_TIMER_STARTED(tdRugby_TryShardTimer)
				REINIT_NET_TIMER(tdRugby_TryShardTimer)
			ENDIF
			
			IF piLastDeliverer = PLAYER_ID()
				IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_YOU_ARE_THE_LAST_DELIVERER)
					SET_BIT(iLocalBoolCheck16, LBOOL16_YOU_ARE_THE_LAST_DELIVERER)
				ENDIF
			ENDIF
			
			piLastDeliverer = INVALID_PLAYER_INDEX()
		ENDIF
		
		IF NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
		AND IS_BIT_SET(iLocalBoolCheck29, LBOOL29_POST_SHARD_SCORE_UPDATE)
			ROLLING_SHARD_SCORE_UPDATE()
			CLEAR_BIT(iLocalBoolCheck29, LBOOL29_POST_SHARD_SCORE_UPDATE)
		ENDIF

		IF piLastDeliverer != INVALID_PLAYER_INDEX()
		AND (MC_serverBD.iTeamScore[0] != (iStoreLastTeam0Score) OR MC_serverBD.iTeamScore[1] != (iStoreLastTeam1Score))
			
			PRINTLN("[RCC MISSION][INCH SHARD] PROCESS_SHARD_TEXT Score difference.")
			
			IF NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
			OR bShownScoreShardInSuddenDeath = FALSE
			
				PRINTLN("[RCC MISSION][INCH SHARD] Not in SD.")
				
				IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
											
					PRINTLN("[RCC MISSION][INCH SHARD] < FMMC_MAX_RULES")
						
					INT iPart = -1
					IF NETWORK_IS_PLAYER_A_PARTICIPANT(piLastDeliverer)
						iPart = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(piLastDeliverer))
						PRINTLN("[RCC MISSION][INCH SHARD] iPart = ", iPart)
					ELSE
						PRINTLN("[RCC MISSION][INCH SHARD] Player ", iPart, " is not a participant!")
					ENDIF
					
					IF iPart != -1
						
						HUD_COLOURS hudColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_playerBD[iPart].iTeam, PlayerToUse)
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciSET_LINES_AS_RUGBY_LINES)
							IF NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
								PRINTLN("[RCC MISSION][INCH SHARD] PROCESS_SHARD_TEXT - iTeam0Score : ",GET_TEAM_SCORE_FOR_DISPLAY(0) )
								PRINTLN("[RCC MISSION][INCH SHARD] PROCESS_SHARD_TEXT - iTeam1Score : ", GET_TEAM_SCORE_FOR_DISPLAY(1) )
								TEXT_LABEL_15 tlTagLine = "SHA_DELP"
								IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_ARENA_CTF(g_FMMC_STRUCT.iAdversaryModeType)
									tlTagLine = "ACTF_DEL"
									tlTagLine+=MC_playerBD[iPart].iTeam
									SET_UP_SCRIPT_VARIABLE_HUD_COLOUR_FROM_HUD_COLOUR(GET_COLOUR_OF_OPPOSITION_TEAM(MC_playerBD[iPart].iTeam, PlayerToUse))
								ENDIF
								PROCESS_ARENA_CTF_ANNOUNCER_DELIVERY()
								
								SETUP_NEW_BIG_MESSAGE_WITH_2_INTS_IN_TITLE(BIG_MESSAGE_2_INTS_IN_TITLE ,"SHA_SCORE",GET_TEAM_SCORE_FOR_DISPLAY(0) - g_FMMC_STRUCT.iInitialPoints[0] ,GET_TEAM_SCORE_FOR_DISPLAY(1) - g_FMMC_STRUCT.iInitialPoints[1] , piLastDeliverer, tlTagLine,GET_HUD_COLOUR_FOR_FMMC_TEAM(0, PlayerToUse) ,GET_HUD_COLOUR_FOR_FMMC_TEAM(1, PlayerToUse), hudColour)
								IF ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
									bShownScoreShardInSuddenDeath = TRUE
								ENDIF
								SET_BIT(iLocalBoolCheck29, LBOOL29_POST_SHARD_SCORE_UPDATE)
							ENDIF
						ENDIF
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("[JJT] iPart != -1")
						#ENDIF
					ENDIF
				
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_DIVE_SCORE_ANIM)
					OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule], ciBS_RULE6_RESPAWN_ON_RULE_END)
						
						IF iPart != -1
							IF MC_playerBD[iPart].iTeam = 0
								//ORANGE FILTER
								IF NOT ANIMPOSTFX_IS_RUNNING("InchOrange")
									PRINTLN("[JJT] PROCESS_PLAYER_RESPAWN_BACK_AT_START - Playing InchOrange FX: ", 
											iTeam, ", ", MC_playerBD[iPart].iTeam)
									ANIMPOSTFX_PLAY("InchOrange", 0, TRUE)
									SET_BIT(iLocalBoolCheck16, LBOOL16_DIVE_SCORE_SCREEN_FX_ACTIVE)
								ENDIF
							ELIF MC_playerBD[iPart].iTeam = 1
								//PURPLE FILTER
								IF NOT ANIMPOSTFX_IS_RUNNING("InchPurple")
									PRINTLN("[JJT] PROCESS_PLAYER_RESPAWN_BACK_AT_START - Playing InchPurple FX: ", 
											iTeam, ", ", MC_playerBD[iPart].iTeam)
									ANIMPOSTFX_PLAY("InchPurple", 0, TRUE)
									SET_BIT(iLocalBoolCheck16, LBOOL16_DIVE_SCORE_SCREEN_FX_ACTIVE)
								ENDIF
							ELSE
								PRINTLN("[JJT] MC_playerBD[iPart].iTeam = ", MC_playerBD[iPart].iTeam, "?????!!")
							ENDIF	
						#IF IS_DEBUG_BUILD
						ELSE
							PRINTLN("[JJT] iPart != -1")
							#ENDIF
						ENDIF	
						
						PRINTLN("[JJT] tdRugby_TryShardTimer(1)")
						REINIT_NET_TIMER(tdRugby_TryShardTimer)
					ENDIF
				
					piLastDeliverer = INVALID_PLAYER_INDEX()
					UPDATE_STORED_TEAM_SCORE_FOR_SHARD()
						
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_EnableGrabbedFlagShard)
		IF MC_playerBD[iPartToUse].iObjCarryCount != 0
			IF NOT IS_BIT_SET(iLocalBoolCheck29, LBOOL29_SENT_CAPTURED_FLAG_SHARD)
			AND NOT HAS_NET_TIMER_STARTED(tdFlagCaptureShardTimer)
			AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
				BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_ModeSpecific, g_iAA_PlaySound_CTF_FlagStolenFromBase)
				BROADCAST_FMMC_CAPTURED_FLAG_SHARD(MC_playerBD[iLocalPart].iteam)
				g_sArena_Telemetry_data.m_flagsStolen++
				PRINTLN("[ARENA][TEL] - Flags stolen ", g_sArena_Telemetry_data.m_flagsStolen)
				SET_BIT(iLocalBoolCheck29, LBOOL29_SENT_CAPTURED_FLAG_SHARD)
			ENDIF
		ELSE
			IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_SENT_CAPTURED_FLAG_SHARD)
				IF NOT HAS_NET_TIMER_STARTED(tdFlagCaptureShardTimer)
					REINIT_NET_TIMER(tdFlagCaptureShardTimer)
					CLEAR_BIT(iLocalBoolCheck29, LBOOL29_SENT_CAPTURED_FLAG_SHARD)
				ENDIF
			ENDIF
			
			IF HAS_NET_TIMER_STARTED(tdFlagCaptureShardTimer)
			AND HAS_NET_TIMER_EXPIRED(tdFlagCaptureShardTimer, 3000)
				RESET_NET_TIMER(tdFlagCaptureShardTimer)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL AM_I_IN_A_BOUNCING_PLANE_OR_HELI()
	IF IS_PED_IN_ANY_HELI(LocalPlayerPed)
		VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		
		IF IS_ENTITY_IN_AIR(tempVeh)
			CLEAR_BIT(MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_DROPPING_OFF)
			CLEAR_BIT(iLocalBoolCheck2, LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
			MC_playerBD[iPartToUse].iVehDeliveryId = -1
			RETURN TRUE
		ENDIF
	ENDIF	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_RULE_REQUIRE_A_SPECIFIC_VEHICLE()
	
	IF MC_serverBD.iPriorityLocation[MC_playerBD[iPartToUse].iteam] != -1
	AND g_FMMC_STRUCT_ENTITIES.sGotoLocationData[MC_serverBD.iPriorityLocation[MC_playerBD[iPartToUse].iteam]].mnVehicleNeeded[MC_playerBD[iPartToUse].iteam] != DUMMY_MODEL_FOR_SCRIPT
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_PLAYER_READY_TO_DELIVER_SPECIFIC_VEHICLE(INT iTeam, PED_INDEX piPlayer)
	
	IF MC_serverBD.iPriorityLocation[iTeam] != -1
		IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[MC_serverBD.iPriorityLocation[iTeam]].mnVehicleNeeded[iTeam] != DUMMY_MODEL_FOR_SCRIPT
			IF IS_PED_IN_ANY_VEHICLE(piPlayer)
				IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_USING(piPlayer)) = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[MC_serverBD.iPriorityLocation[iTeam]].mnVehicleNeeded[iTeam]
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			RETURN TRUE //we don't have a vehicle set, so this check is not needed. 
		ENDIF
	ELSE
		RETURN TRUE 
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_READY_TO_DELIVER_ANY_SPECIFIC_VEHICLE(INT iTeam, PED_INDEX piPlayer)
	INT iLoc
	
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[iTeam] = 0
		RETURN IS_PLAYER_READY_TO_DELIVER_SPECIFIC_VEHICLE(iTeam, piPlayer)
	ENDIF
	
	FOR iLoc = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[iTeam]-1
		IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam] = MC_serverBD_4.iCurrentHighestPriority[iTeam]
			IF MC_serverBD_4.iGotoLocationDataRule[iLoc][iTeam] = FMMC_OBJECTIVE_LOGIC_GO_TO
				IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].mnVehicleNeeded[iTeam] != DUMMY_MODEL_FOR_SCRIPT
					IF IS_PED_IN_ANY_VEHICLE(piPlayer)
						IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_USING(piPlayer)) = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].mnVehicleNeeded[iTeam]
							//Returning TRUE because we're in the right vehicle
							RETURN TRUE
						ENDIF
					ENDIF
				ELSE
					//Returning TRUE because there's no vehicle set for this rule
					RETURN TRUE
				ENDIF
			ELSE
				//Returning TRUE because this isn't FMMC_OBJECTIVE_LOGIC_GO_TO
				RETURN TRUE 
			ENDIF
		ELSE
			RELOOP
		ENDIF
	ENDFOR

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_TEAMMATE_READY_TO_DELIVER_SPECIFIC_VEHICLE(INT iTeam)

	IF MC_serverBD.iPriorityLocation[iTeam] != -1
		PLAYER_INDEX tempPlayer
		PARTICIPANT_INDEX tempPart
		INT iparticipant

		IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[MC_serverBD.iPriorityLocation[iTeam]].mnVehicleNeeded[iTeam] != DUMMY_MODEL_FOR_SCRIPT
			REPEAT GET_MAXIMUM_PLAYERS_ON_MISSION() iparticipant
				tempPart = INT_TO_PARTICIPANTINDEX(iparticipant)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
					tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
					IF tempPlayer != LocalPlayer
						IF IS_NET_PLAYER_OK(tempPlayer)
							IF MC_playerBD[iparticipant].iteam = iTeam
								PED_INDEX playerPed = GET_PLAYER_PED(tempPlayer)
								
								IF IS_PED_IN_ANY_VEHICLE(playerPed)
								AND (NOT IS_REMOTE_PLAYER_IN_NON_CLONED_VEHICLE(tempPlayer)) // Need to make sure we have this vehicle cloned on our machine, or the checks below won't work
									
									VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_USING(playerPed)
									
									IF DOES_ENTITY_EXIST(tempVeh) // url:bugstar:2214433
									AND GET_ENTITY_MODEL(tempVeh) = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[MC_serverBD.iPriorityLocation[iTeam]].mnVehicleNeeded[iTeam]
										RETURN TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_OBJ_TEXT_DELAY_INDEX(INT iBitSetGroup, INT iIndex)
	SWITCH iBitSetGroup
		CASE OBJ_TEXT_DELAY_BS_GROUP_0		//Equivalent of iLocalBoolCheck
			SWITCH iIndex
				CASE	LBOOL3_UPDATE_VEHICLE_OBJECTIVE		RETURN 0
				CASE	LBOOL3_UPDATE_PED_OBJECTIVE			RETURN 1
				CASE	LBOOL3_UPDATE_OBJECT_OBJECTIVE		RETURN 2
				CASE	LBOOL28_UPDATE_LOSE_COPS_OBJECTIVE	RETURN 4
				CASE	LBOOL29_UPDATE_LEAVE_LOC			RETURN 5
			ENDSWITCH
		BREAK
		CASE OBJ_TEXT_DELAY_BS_GROUP_1		//Equivalent of MC_playerBD[iPartToUse].iClientBitSet
			SWITCH iIndex
				CASE	PBBOOL_WEARING_MASK					RETURN 3
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("[RCC MISSION][UOBJ] - OBJECTIVE_TEXT - GET_OBJ_TEXT_DELAY_INDEX - INVALID iBitSetGroup PASSED IN - USING 30")
	RETURN 30
ENDFUNC

//PURPOSE: 
PROC CHECK_FOR_OBJECTIVE_TEXT_UPDATE_CLEAR(INT iBitSetGroup, INT iIndex, INT iDelay = OBJECTIVE_TEXT_DELAY)
	INT iCOTDindex = GET_OBJ_TEXT_DELAY_INDEX(iBitSetGroup, iIndex)

	IF IS_BIT_SET(iLocalBoolCheck30, LBOOL30_UPDATE_OBJECTIVE_TEXT_WITH_VERY_LONG_DELAY)
		PRINTLN("[RCC MISSION][UOBJ] OBJECTIVE_TEXT - CHECK_FOR_OBJECTIVE_TEXT_UPDATE_CLEAR - Using very long delay for entity update.")
		iDelay = OBJECTIVE_TEXT_DELAY_VERY_LONG
	ENDIF
		
	IF IS_BIT_SET(iLocalBoolCheckObjTextDelay, iCOTDindex)
		IF HAS_NET_TIMER_EXPIRED(stObjTextDelayTimerGroup[iCOTDindex], iDelay)
			RESET_NET_TIMER(stObjTextDelayTimerGroup[iCOTDindex])
			CLEAR_BIT(iLocalBoolCheckObjTextDelay, iCOTDindex)
			PRINTLN("[RCC MISSION][UOBJ] OBJECTIVE_TEXT - CHECK_FOR_OBJECTIVE_TEXT_UPDATE_CLEAR - iLocalBoolCheckObjTextDelay ", iCOTDindex, " - CLEARED")
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Adds a delay before returning that it is actually ready.
FUNC BOOL IS_OBJECTIVE_TEXT_UPDATE_READY(INT iBitSetGroup, INT iBitSet, INT iIndex, BOOL bSkipFlagSetCheck = FALSE, INT iDelay = OBJECTIVE_TEXT_DELAY, STRING sDeleteCheck = NULL)
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		RETURN FALSE
	ENDIF
		
	INT iCOTDindex = GET_OBJ_TEXT_DELAY_INDEX(iBitSetGroup, iIndex)
	
	IF IS_BIT_SET(iLocalBoolCheck30, LBOOL30_UPDATE_OBJECTIVE_TEXT_WITH_VERY_LONG_DELAY)
		PRINTLN("[RCC MISSION][UOBJ] OBJECTIVE_TEXT - IS_OBJECTIVE_TEXT_UPDATE_READY - Using very long delay for entity update.")
		iDelay = OBJECTIVE_TEXT_DELAY_VERY_LONG
	ENDIF
		
	//Delay Checks
	IF IS_BIT_SET(iLocalBoolCheckObjTextDelay, iCOTDindex)
		IF HAS_NET_TIMER_STARTED(stObjTextDelayTimerGroup[iCOTDindex])
			IF HAS_NET_TIMER_EXPIRED(stObjTextDelayTimerGroup[iCOTDindex], iDelay)
				PRINTLN("[RCC MISSION][UOBJ] OBJECTIVE_TEXT - IS_OBJECTIVE_TEXT_UPDATE_READY - RETURN TRUE - iCOTDindex = ", iCOTDindex)
				CLEAR_BIT(iLocalBoolCheck30, LBOOL30_UPDATE_OBJECTIVE_TEXT_WITH_VERY_LONG_DELAY)
				RETURN TRUE
			ENDIF
		ELSE
			CLEAR_BIT(iLocalBoolCheckObjTextDelay, iCOTDindex)
			PRINTLN("[RCC MISSION][UOBJ] OBJECTIVE_TEXT - IS_OBJECTIVE_TEXT_UPDATE_READY - Bit set but timer not running ", iCOTDindex, " - CLEARED")
		ENDIF
	ELSE
		IF IS_BIT_SET(iBitSet, iIndex)
		OR bSkipFlagSetCheck = TRUE
			IF NOT IS_STRING_NULL_OR_EMPTY(sDeleteCheck) 						//sDeleteCheck = The string we want to display.
				IF NOT Has_This_MP_Objective_Text_Been_Received(sDeleteCheck) 	//Make sure we don't delete the one we want to show if it is already shown.
					Delete_MP_Objective_Text()
					PRINTLN("[RCC MISSION][UOBJ] OBJECTIVE_TEXT - IS_OBJECTIVE_TEXT_UPDATE_READY - Delete_MP_Objective_Text - iCOTDindex = ", iCOTDindex)
				ENDIF
			ENDIF
			REINIT_NET_TIMER(stObjTextDelayTimerGroup[iCOTDindex])
			iLocalBoolCheckObjTextDelay = 0	//Clear all so we don't keep doing old checks
			SET_BIT(iLocalBoolCheckObjTextDelay, iCOTDindex)
			PRINTLN("[RCC MISSION][UOBJ] OBJECTIVE_TEXT - IS_OBJECTIVE_TEXT_UPDATE_READY - iLocalBoolCheckObjTextDelay ", iCOTDindex, " - SET")
		ELSE
			IF HAS_NET_TIMER_STARTED(stObjTextDelayTimerGroup[iCOTDindex])
				RESET_NET_TIMER(stObjTextDelayTimerGroup[iCOTDindex])
				PRINTLN("[RCC MISSION][UOBJ] OBJECTIVE_TEXT - IS_OBJECTIVE_TEXT_UPDATE_READY - stObjTextDelayTimerGroup[", iCOTDindex, "] - RESET")
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC


//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: HUD HIDING !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************



FUNC BOOL IS_THIS_THE_LAST_VEHICLE_DELIVERY( INT iTeam, INT iRule )

	IF iRule >= MC_serverBD.iMaxObjectives[ iTeam ]
	AND MC_ServerBD.iNumVehHighestPriority[ iTeam ] = 1
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL AM_I_DELIVERING_THE_LAST_VEHICLE_ON_A_GET_AND_DELIVER( INT iTeam, INT iRule )
	
	IF  MC_playerBD[iPartToUse].iVehNear != -1 
	AND NOT AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH()
	AND IS_THIS_THE_LAST_VEHICLE_DELIVERY( iTeam, iRule ) 
	AND ( 	IS_BIT_SET(MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_IN_DROP_OFF) OR IS_BIT_SET(MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_DROPPING_OFF) )
	AND ( GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_COLLECT_VEH OR GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_DELIVER_VEH )
	AND ( NOT AM_I_IN_A_BOUNCING_PLANE_OR_HELI() )
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
	
ENDFUNC

PROC DISABLE_HUD(BOOL bPreventPhoneHangup = FALSE)
	
		#IF IS_DEBUG_BUILD
			DEBUG_PRINTCALLSTACK()
		#ENDIF
		
		IF GET_REASON_HUD_IS_HIDDEN() != ciHUDHIDDEN_MANUALLY_HIDDEN_HUD //Don't disable the D-pad down menu if the hud is just manually hidden
			DISABLE_DPADDOWN_THIS_FRAME()
		ENDIF
		
		IF NOT bPreventPhoneHangup
		AND GET_REASON_HUD_IS_HIDDEN() != ciHUDHIDDEN_MANUALLY_HIDDEN_HUD //Don't disable the phone if the hud is just manually hidden
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
		ENDIF
		
		DISABLE_SELECTOR_THIS_FRAME() 
		
		IF GET_REASON_HUD_IS_HIDDEN() != ciHUDHIDDEN_MANUALLY_HIDDEN_HUD //Don't disable the main menu if the hud is just manually hidden
			DISABLE_FRONTEND_THIS_FRAME()
		ENDIF
		
		CLEAR_INVENTORY_BOX_CONTENT()
		DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_ALL_OVERHEADS)
		
		IF GET_REASON_HUD_IS_HIDDEN() != ciHUDHIDDEN_MANUALLY_HIDDEN_HUD //Don't hide all hud if the hud is only manually hidden
			HIDE_HUD_AND_RADAR_THIS_FRAME()
		ELSE
			HIDE_RADAR_THIS_FRAME()
		ENDIF
		
		DISABLE_ALL_MP_HUD_THIS_FRAME(GET_REASON_HUD_IS_HIDDEN() != ciHUDHIDDEN_MANUALLY_HIDDEN_HUD) //Don't disable the D-pad down menu if the hud is just manually hidden
		THEFEED_HIDE_THIS_FRAME()
		THEFEED_FLUSH_QUEUE()
		THEFEED_FORCE_RENDER_OFF()
		
	//	IF Has_Any_MP_Objective_Text_Been_Received()
	//		Unpause_Objective_Text()
	//		Clear_Any_Objective_Text()
	//	ENDIF


//	IF NOT Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)
//	AND NOT Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer)
//	AND NOT Is_Player_Currently_On_MP_Versus_Mission(LocalPlayer)
//		DISABLE_DPADDOWN_THIS_FRAME()
//		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
//		DISABLE_SELECTOR_THIS_FRAME() 
//		CLEAR_INVENTORY_BOX_CONTENT()
//		THEFEED_HIDE_THIS_FRAME()
//		
//		//b*2190728
//		IF IS_CUTSCENE_PLAYING()
//			DISABLE_FRONTEND_THIS_FRAME()
//		ENDIF
//		
//		IF NOT IS_THIS_SPECTATOR_CAM_RUNNING(g_BossSpecData.specCamData)
//		OR NOT IS_PLAYER_SCTV(LocalPlayer)
//			IF AM_I_ON_A_HEIST()
//				DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_ALL_OVERHEADS)
//				HIDE_HUD_AND_RADAR_THIS_FRAME()
//			ENDIF
//		ENDIF
//	ENDIF
	
ENDPROC

PROC MANAGE_CAM_PHONE_LEGEND()
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iCurrentRule = MC_serverBD_4.iCurrentHighestPriority[ iTeam ]
	
	IF (iCurrentRule < FMMC_MAX_RULES)
		IF iTeam != -1
			IF iTeam < FMMC_MAX_TEAMS
				IF g_FMMC_STRUCT.bRemoveUnwantedCamPhoneOptions = FALSE
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iCurrentRule], ciBS_RULE4_STREAMLINE_CAM_HELP)
						g_FMMC_STRUCT.bRemoveUnwantedCamPhoneOptions = TRUE
					ENDIF
				ELSE
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iCurrentRule], ciBS_RULE4_STREAMLINE_CAM_HELP)
						g_FMMC_STRUCT.bRemoveUnwantedCamPhoneOptions = FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL CHECK_IF_HUD_SHOULD_BE_HIDDEN_THIS_FRAME()
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iCurrentRule = MC_serverBD_4.iCurrentHighestPriority[ iTeam ]
	
	BOOL bShouldPrint
	
	iHUDHiddenReason = ciHUDHIDDEN_NOT_HIDDEN
	
	#IF IS_DEBUG_BUILD
		bShouldPrint = bHUDHidingReasons
	#ENDIF
	
	IF IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING()
	OR (g_bEndOfMissionCleanUpHUD
    AND (GET_STRAND_MISSION_TRANSITION_TYPE() = FMMC_BRANCHING_MISSION_TRANSITION_TYPE_AIRLOCK
	OR ((MC_serverBD.iNextMission > -1 AND MC_serverBD.iNextMission < FMMC_MAX_STRAND_MISSIONS)
	AND g_FMMC_STRUCT.eBranchingTransitionType[MC_serverBD.iNextMission] = FMMC_BRANCHING_MISSION_TRANSITION_TYPE_AIRLOCK))
    AND IS_STRAND_MISSION_READY_TO_START_DOWNLOAD()) //Works earlier
		IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - RETURN FALSE EARLY - AIRLOCK") ENDIF
		RETURN FALSE
	ENDIF
	
	IF MC_playerBD_1[iPartToUse].iCurrentElevator != -1
	AND (MC_playerBD_1[iPartToUse].eCurrentElevatorState = FMMC_ELEVATOR_CLIENT_STATE_IN_POSITION
	OR MC_playerBD_1[iPartToUse].eCurrentElevatorState = FMMC_ELEVATOR_CLIENT_STATE_WARPED
	OR MC_playerBD_1[iPartToUse].eCurrentElevatorState = FMMC_ELEVATOR_CLIENT_STATE_CAMERA_SHOT)
		IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - Inside an elevator") ENDIF
		iHUDHiddenReason = ciHUDHIDDEN_ELEVATOR
		RETURN TRUE
	ENDIF

	IF IS_ON_SCREEN_WHEEL_SPIN_MINIGAME_ACTIVE()
	AND CONTENT_IS_USING_ARENA()
		PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME TRUE - IS_ON_SCREEN_WHEEL_SPIN_MINIGAME_ACTIVE")
	ENDIF
	
	// 2526100: Hide Remaining Players if spectator is hiding HUD
	IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
	AND NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
		IF IS_SPECTATOR_HUD_HIDDEN()
			iHUDHiddenReason = ciHUDHIDDEN_SPECTATORHUDHIDDEN
			IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - (1)") ENDIF
			RETURN TRUE
		ENDIF
	ELSE
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_DIVE_SCORE_ANIM)
			IF IS_BIT_SET(iLocalBoolCheck16, LBOOL16_DIVE_SCORE_SCREEN_FX_ACTIVE)
				iHUDHiddenReason = ciHUDHIDDEN_DIVE_SCORE
				IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - (2)") ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
			
	IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet3, PBBOOL3_RESTART_RENDERPHASE_PAUSED)
	OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TINY_RACERS_SCREEN_PAUSED)
		IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - (3)") ENDIF
		iHUDHiddenReason = ciHUDHIDDEN_RENDERPHASE_PAUSED
		RETURN TRUE
	ENDIF
	
	IF MC_serverBD_3.iBombFB_OfficialRuleIndex > -1
		IF MC_serverBD_4.iCurrentHighestPriority[0] != MC_serverBD_3.iBombFB_OfficialRuleIndex
			iHUDHiddenReason = ciHUDHIDDEN_BOMBFB_TRANSITIONING
			IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - (4)") ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF GET_PLAYER_RESPAWN_BACK_AT_START_STATE(iPartToUse) != PRBSS_PlayingNormally
	AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
	AND NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
		IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - (5)") ENDIF
		RETURN TRUE
	ENDIF
	
	IF iCurrentRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[iCurrentRule], ciBS_RULE8_HIDE_HUD_DURING_RESTART)
			IF IS_PLAYER_IN_RESPAWN_BACK_AT_START_LOGIC(iLocalPart)
				iHUDHiddenReason = ciHUDHIDDEN_RESPAWN_BACK_AT_START_LOGIC
				IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - (6)") ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF (iCurrentRule >= FMMC_MAX_RULES)
	AND NOT g_bPropertyDropOff 
	AND NOT g_bGarageDropOff
		IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
			IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_EARLY_END_SPECTATOR)
				IF NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
					IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - (endofmission - 1)") ENDIF
					g_bEndOfMissionCleanUpHUD = TRUE
				ENDIF
			ENDIF
		ELSE
			IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - (endofmission - 2)") ENDIF
			g_bEndOfMissionCleanUpHUD = TRUE
		ENDIF
	ENDIF
	
	IF (iCurrentRule < FMMC_MAX_RULES)
		IF g_bEndOfMissionCleanUpHUD = TRUE
			g_bEndOfMissionCleanUpHUD = FALSE
		ENDIF
	ENDIF
	
	IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
		IF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_MANUALLY_HIDDEN_HUD)
			IF (NOT bUsingBirdsEyeCam AND IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType))
				//Don't worry about LBOOL28_MANUALLY_HIDDEN_HUD
			ELSE
				IF GET_FRAME_COUNT() % 10 = 0
					PRINTLN("SHOULD_HIDE_THE_HUD_THIS_FRAME - Hiding due to LBOOL28_MANUALLY_HIDDEN_HUD")
				ENDIF
				IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - (7)") ENDIF
				iHUDHiddenReason = ciHUDHIDDEN_MANUALLY_HIDDEN_HUD
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF MC_playerBD[iPartToUse].iObjHacking != -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[MC_playerBD[iPartToUse].iObjHacking].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__DOWNLOAD
			iHUDHiddenReason = ciHUDHIDDEN_INTERACT_WITH_DOWNLOAD
			IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - (8)") ENDIF
			RETURN TRUE
		ENDIF
		IF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_STARTED_BEAM_HACK)
			iHUDHiddenReason = ciHUDHIDDEN_BEAMHACK
			IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - (9)") ENDIF
			RETURN TRUE
		ENDIF
		IF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_STARTED_HOTWIRE_HACK)
			iHUDHiddenReason = ciHUDHIDDEN_HOTWIRE
			IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - (10)") ENDIF
			RETURN TRUE
		ENDIF
		
		
		IF sVaultDrillData.iCurrentState = VAULT_DRILL_MINIGAME_STATE_WAIT_FOR_INTRO
		OR sVaultDrillData.iCurrentState = VAULT_DRILL_MINIGAME_STATE_WAIT_FOR_OUTRO
			RETURN TRUE
		ENDIF
	
	
	ENDIF

	// General checks for everyone
	IF NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
		BOOL bHideTheHud
		
		IF g_bEndOfMissionCleanUpHUD
			IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - (11)") ENDIF
			bHideTheHud = TRUE
		ENDIF
		
		IF HAS_TEAM_FINISHED( iTeam ) //url:bugstar:2233889
			IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - (12)") ENDIF
			bHideTheHud = TRUE
		ENDIF
				
		IF IS_BIT_SET( MC_playerBD[ iLocalPart ].iClientBitSet, PBBOOL_FINISHED )
			IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - (13)") ENDIF
			bHideTheHud = TRUE
		ENDIF		
		
		IF IS_CUTSCENE_PLAYING()
			IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - (14)") ENDIF
			bHideTheHud = TRUE
		ENDIF		
		
		IF AM_I_DELIVERING_THE_LAST_VEHICLE_ON_A_GET_AND_DELIVER( iTeam, iCurrentRule )
			IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - (15)") ENDIF
			bHideTheHud = TRUE
		ENDIF		
		
		IF NOT IS_BIT_SET( MC_serverBD.iServerBitSet, SBBOOL_EVERYONE_RUNNING )
		AND NOT HAS_FIRST_STRAND_MISSION_BEEN_PASSED_NO_RESET()
			IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - (16)") ENDIF
			bHideTheHud = TRUE
		ENDIF		
		
		IF IS_BIT_SET( iLocalBoolCheck13, LBOOL13_DRAW_DISTANCE_TO_CROSS_LINE ) AND NOT IS_BIT_SET( iLocalBoolCheck13, LBOOL13_FIRST_STAGGERED_LOOP_DONE_CLIENTSIDE )	
			IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - (17)") ENDIF
			bHideTheHud = TRUE
		ENDIF
		
		IF bHideTheHud
			IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
				IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_EARLY_END_SPECTATOR)
				OR NOT IS_A_SPECTATOR_CAM_RUNNING()
					iHUDHiddenReason = ciHUDHIDDEN_GENERAL_CHECKS_1
					IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - (18)") ENDIF	
					RETURN TRUE
				ENDIF
			ELSE
				iHUDHiddenReason = ciHUDHIDDEN_GENERAL_CHECKS_2
				IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - (19)") ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	iHUDHiddenReason = ciHUDHIDDEN_NOT_HIDDEN
	IF bShouldPrint PRINTLN("[RCC MISSION] [MMacK] SHOULD_HIDE_THE_HUD_THIS_FRAME - RETURN FALSE") ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_HIDE_THE_HUD_THIS_FRAME()
	
	IF IS_BIT_SET(iLocalBoolResetCheck1, LBOOL_RESET1_HUD_SHOULD_BE_HIDDEN_THIS_FRAME_CHECKED)
		RETURN IS_BIT_SET(iLocalBoolResetCheck1, LBOOL_RESET1_HUD_SHOULD_BE_HIDDEN_THIS_FRAME)
	ENDIF
	
	SET_BIT(iLocalBoolResetCheck1, LBOOL_RESET1_HUD_SHOULD_BE_HIDDEN_THIS_FRAME_CHECKED)
	
	IF CHECK_IF_HUD_SHOULD_BE_HIDDEN_THIS_FRAME()
		SET_BIT(iLocalBoolResetCheck1, LBOOL_RESET1_HUD_SHOULD_BE_HIDDEN_THIS_FRAME)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_GPS_DISTANCE_METER_BAR(INT iBar, INT iRule)
	INT iPed = iGPSTrackingMeterPedIndex[iBar]
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
		INT iHudOrder = iBar
		
		TEXT_LABEL_15 tl15 = "DISTMET_HUD"
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sPedBlipStruct.iBlipSpriteOverride != 0
			tl15 = "DISTMET_HUD_"
			tl15 += g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sPedBlipStruct.iBlipSpriteOverride
			
			iHudOrder = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sPedBlipStruct.iBlipSpriteOverride
		ENDIF
		
		INT fDistMax = ROUND(VDIST(vGPSTrackingMeterStart[iBar], vGPSTrackingMeterEnd[iBar]))
		INT fCurrentProgress = ROUND(VDIST(GET_ENTITY_COORDS(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPed])), vGPSTrackingMeterEnd[iBar]))
	
		IF fCurrentProgress <= iGPSTrackingMeterDistCache[iBar]
			iGPSTrackingMeterDistCache[iBar] = fCurrentProgress
		ENDIF
		
		FLOAT fPercent = 0
		fPercent = (TO_FLOAT(iGPSTrackingMeterDistCache[iBar]) / fDistMax) * 100.0
		INT iTimeToFlash = -1
		IF fPercent < g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iHUDFlashPercentage[iRule]
			iTimeToFlash = 999999
		ENDIF
		
		IF NOT IS_BIT_SET(MC_serverBD_4.iPedClearFailOnGoTo[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			IF NOT HAS_NET_TIMER_STARTED(timerDistanceBarDelay)
				REINIT_NET_TIMER(timerDistanceBarDelay)
			ELIF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(timerDistanceBarDelay) > ciTimerDistanceBarDelay
				DRAW_GENERIC_METER(iGPSTrackingMeterDistCache[iBar], fDistMax, tl15, HUD_COLOUR_RED, iTimeToFlash, HUDORDER_NINETHBOTTOM - INT_TO_ENUM(HUDORDER, iHudOrder))
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL CAN_MANUALLY_TOGGLE_HUD()
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_MANUAL_HUD_TOGGLE()
	IF CAN_MANUALLY_TOGGLE_HUD()
		CONTROL_ACTION caToggleButton = INPUT_CONTEXT
		
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
			caToggleButton = INPUT_VEH_EXIT
		ENDIF
		
		IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, caToggleButton)
		OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, caToggleButton)
			IF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_MANUALLY_HIDDEN_HUD)
				CLEAR_BIT(iLocalBoolCheck28, LBOOL28_MANUALLY_HIDDEN_HUD)
			ELSE
				SET_BIT(iLocalBoolCheck28, LBOOL28_MANUALLY_HIDDEN_HUD)
			ENDIF
			
			PRINTLN("PROCESS_MANUAL_HUD_TOGGLE - Swapping LBOOL28_MANUALLY_HIDDEN_HUD")
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_THE_CURRENT_HUD()


	IF SHOULD_HIDE_THE_HUD_THIS_FRAME()
	AND NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
		IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
			PRINTLN("[RCC MISSION] [MMacK][EOM] END OF MISSION HUD CLEARUP IN PROGRESS - spectator")
			HIDE_SPECTATOR_BUTTONS(TRUE)
			HIDE_SPECTATOR_HUD(TRUE)
		ENDIF
		
		DISABLE_HUD()
		CPRINTLN( DEBUG_OWAIN, "[RCC MISSION] Clearing HUD this frame" )
		
	ELSE
		IF IS_LOCAL_PLAYER_ANY_SPECTATOR() AND NOT IS_CUTSCENE_PLAYING() 
			HIDE_SPECTATOR_BUTTONS(FALSE)
			HIDE_SPECTATOR_HUD(FALSE)
		ENDIF
	ENDIF
	
	PROCESS_MANUAL_HUD_TOGGLE()
	
	IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
	AND NOT IS_BIT_SET(iLocalBoolCheck21, LBOOL21_SPECTATOR_RESPAWN_WAIT_IN_SPECTATE)
		IF IS_PLAYER_DEAD(LocalPlayer)
			CLEAR_INVENTORY_BOX_CONTENT()
			DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_ALL_OVERHEADS)
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Creates the custom wasted string or makes sure the custom wasted string is shown correctly
/// PARAMS:
///    iTeam - Takes the victims team to check if they have any custom wasted shards on
PROC PROCESS_CUSTOM_WASTED_SHARD(INT iVictimTeam, INT iVictim, INT iKiller)
	
	//Clear all big messages before calling a new wasted shard
	CLEAR_ALL_BIG_MESSAGES()
	IF iKiller >= 0
		STRING sTeamName = TEXT_LABEL_TO_STRING(g_sMission_TeamName[MC_playerBD[iKiller].iteam])
		HUD_COLOURS hcKillerTeamColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_playerBD[iKiller].iteam, INT_TO_PLAYERINDEX(iKiller))

		TEXT_LABEL_15 tlTempTitle
		IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tlCustomWastedTitle[MC_playerBD[iVictim].iteam])
			tlTempTitle = g_FMMC_STRUCT.tlCustomWastedTitle[MC_playerBD[iVictim].iteam]
		ELSE
			tlTempTitle = "RESPAWN_W_MP"
		ENDIF
		SWITCH g_FMMC_STRUCT.iCustomWastedShardType[iVictimTeam] 
			CASE ciCUSTOM_WASTED_SHARD_NONE
				SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_CUSTOM_FAILED, "",  g_FMMC_STRUCT.tlCustomWastedStrapline[iVictimTeam], g_FMMC_STRUCT.tlCustomWastedTitle[iVictimTeam], -1, -1, -1, "")
			BREAK
			CASE ciCUSTOM_WASTED_SHARD_BE_MY_VALENTINE
				IF iKiller != PARTICIPANT_ID_TO_INT()
				AND iKiller != -3
					PRINTLN("[KH] -----> CUSTOM WASTED SHARD | TEAM MATE WAS KILLED BY THE ENEMY TEAM:", iKiller)
					SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_CUSTOM_FAILED, g_FMMC_STRUCT.tlCustomWastedStrapline[iVictimTeam], sTeamName, hcKillerTeamColour, DEFAULT, tlTempTitle)
				ELSE
					PRINTLN("[KH] -----> CUSTOM WASTED SHARD | TEAM MATE COMMITED SUICIDE/DIDN'T GET KILLED BY THE OTHER TEAM:", iKiller)
					SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_CUSTOM_FAILED, "", "", tlTempTitle, -1, -1, -1, "")
				ENDIF
			BREAK
			CASE ciCUSTOM_WASTED_SHARD_SUMO
				PRINTLN("[KH] -----> CUSTOM WASTED SHARD | SUMO PRESET | KNOCKED OUT", iKiller)
				SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_CUSTOM_FAILED, "", "", g_FMMC_STRUCT.tlCustomWastedTitle[iVictimTeam], -1, -1, -1, "")
			BREAK
			CASE ciCUSTOM_WASTED_HEALTH_DEPLETED
				PRINTLN("[KH] -----> CUSTOM WASTED SHARD | HEALTH 0 PRESET | You ran out of health")
				SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_CUSTOM_FAILED, g_FMMC_STRUCT.tlCustomWastedStrapline[iVictimTeam], "", DEFAULT, DEFAULT, "RESPAWN_W_MP")
			BREAK
		ENDSWITCH
	//If there was no killer - if the player was blown up by an explosion, killed my ambient ped etc.
	ELSE
		SWITCH g_FMMC_STRUCT.iCustomWastedShardType[iVictimTeam]
		CASE ciCUSTOM_WASTED_SHARD_SUMO
			PRINTLN("[KH] -----> CUSTOM WASTED SHARD | SUMO PRESET | KNOCKED OUT", iKiller)
			SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_CUSTOM_FAILED, "", "", g_FMMC_STRUCT.tlCustomWastedTitle[iVictimTeam], -1, -1, -1, "")
			BREAK
		CASE ciCUSTOM_WASTED_HEALTH_DEPLETED
			PRINTLN("[KH] -----> CUSTOM WASTED SHARD | HEALTH 0 PRESET | You ran out of health")
			SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_CUSTOM_FAILED, g_FMMC_STRUCT.tlCustomWastedStrapline[iVictimTeam], "", DEFAULT, DEFAULT, "RESPAWN_W_MP")
			BREAK
		DEFAULT
			PRINTLN("[KH] -----> CUSTOM WASTED SHARD | TEAM MATE KILLED BY WORLD/GENERIC WASTED TEXT:", iKiller)
			SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_CUSTOM_FAILED, "", "", "RESPAWN_W_MP", -1, -1, -1, "")
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC TEAMS_TO_BLIP(INT iLocalPlayerTeam, INT iPartTeam)
	IF iLocalPlayerTeam != iPartTeam
		IF iPartTeam = 0
			IF NOT IS_BIT_SET( g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM0_TARGET )
				SET_BIT( g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM0_TARGET )
			ENDIF
		ELIF iPartTeam = 1
			IF NOT IS_BIT_SET( g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM1_TARGET )
				SET_BIT( g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM1_TARGET )
			ENDIF
		ELIF iPartTeam = 2
			IF NOT IS_BIT_SET( g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM2_TARGET )
				SET_BIT( g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM2_TARGET )
			ENDIF
		ELIF iPartTeam = 3
			IF NOT IS_BIT_SET( g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM3_TARGET )
				SET_BIT( g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM3_TARGET )
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_PLAYER_CURRENT_SPEED()
	IF iSpectatorTarget = -1
		INT iScratchTeam = MC_playerBD[iLocalPart].iteam
		IF iScratchTeam < FMMC_MAX_TEAMS
		AND iScratchTeam != -1
		
			INT iScratchRule = MC_serverBD_4.iCurrentHighestPriority[iScratchTeam]
			
			IF iScratchRule < FMMC_MAX_RULES
			
				INT iThresholdSpeed = g_FMMC_STRUCT.sFMMCEndConditions[iScratchTeam].iBlipOnSpeedThreshold[MC_serverBD_4.iCurrentHighestPriority[iScratchTeam]]
	
				//check that the player is in a car before trying to blip them
				IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					IF GET_ENTITY_SPEED(LocalPlayerPed) > iThresholdSpeed
						MC_playerBD[iLocalPart].bIsOverSpeedThreshold = TRUE
					ELSE
						MC_playerBD[iLocalPart].bIsOverSpeedThreshold = FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Blip of Speed creator setting - blips the player when they go above a certain speed in a vehicle
PROC PROCESS_BLIP_ON_VEHICLE_SPEED()
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
	
	IF iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetSix[iRule], ciBS_RULE6_BLIP_ON_SPEED_ENABLED)
			//PED_INDEX pedToCheck
			//PLAYER_INDEX playerToCheck

			INT i = 0	
			FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
				INT iScratchTeam = MC_playerBD[i].iteam
				IF iScratchTeam < FMMC_MAX_TEAMS
				AND iScratchTeam != -1
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[i].iteam].iRuleBitsetSix[iRule], ciBS_RULE6_BLIP_ON_SPEED_ENABLED)
						//make sure you're not blipping yourself
						//IF i != iPartToUse
						INT iScratchRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[i].iteam]
							
						IF iScratchRule < FMMC_MAX_RULES
							INT iBlipTime = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[i].iteam].iBlipOnSpeedTime[iScratchRule]
							INT iThresholdSpeed = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[i].iteam].iBlipOnSpeedThreshold[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[i].iteam]]
							
							//if either value is 0, ignore that team as they want to see the blips but don't want to show them to other teams
							IF iThresholdSpeed != 0 OR iBlipTime != 0
							INT iPartToCheck = NATIVE_TO_INT(INT_TO_PARTICIPANTINDEX(i))
								//check that the player is in a car before trying to blip them
								IF MC_playerBD[iPartToCheck].bIsOverSpeedThreshold
									//if you're checking yourself and you're over the threshold, flash your arrow blip
									IF iPartToCheck = iPartToUse
										FLASH_MY_PLAYER_ARROW_RED(TRUE, -1, HIGHEST_INT, 0)
									ENDIF
									TEAMS_TO_BLIP(MC_playerBD[iPartToUse].iteam, MC_playerBD[i].iteam)
										
									IF NOT IS_BIT_SET(iBlippingPlayerBS, iPartToCheck)
										SET_BIT(iBlippingPlayerBS, iPartToCheck)
									ELSE
										IF HAS_NET_TIMER_STARTED(biBlipOnSpeedTimer[i])
											RESET_NET_TIMER(biBlipOnSpeedTimer[i])
										ENDIF
									ENDIF
								ELSE
									IF IS_BIT_SET(iBlippingPlayerBS, iPartToCheck)
										//if they're not over the speed and they've been blipped, start the timer to remove the blip
										IF NOT HAS_NET_TIMER_STARTED(biBlipOnSpeedTimer[i])
											PRINTLN("[KH][BOS] PROCESS_BLIP_ON_VEHICLE_SPEED - PART: ", iPartToUse, " is under the specified speed, starting timer to remove blip")
											START_NET_TIMER(biBlipOnSpeedTimer[i])
										ENDIF
										INT iTimeElapsed = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(biBlipOnSpeedTimer[i])
											
										IF iTimeElapsed > (iBlipTime * 1000)
											PRINTLN("[KH][BOS] PROCESS_BLIP_ON_VEHICLE_SPEED - PART: ", iPartToUse, " has been under the speed specified for too long, removing blip and resetting timer")
											RESET_NET_TIMER(biBlipOnSpeedTimer[i])
											CLEAR_BIT(iBlippingPlayerBS, iPartToCheck)
											IF iPartToCheck = iPartToUse
												FLASH_MY_PLAYER_ARROW_RED(FALSE)
											ENDIF
										ELSE
											TEAMS_TO_BLIP(MC_playerBD[iPartToUse].iteam, MC_playerBD[i].iteam)
										ENDIF	
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
	ENDIF
ENDPROC

PROC DRAW_VIP_MARKER(VECTOR vDrawPosition)

	DestinationBlip = ADD_BLIP_FOR_COORD(vDrawPosition)
	SET_BLIP_SPRITE(DestinationBlip, GET_WAYPOINT_BLIP_ENUM_ID())
	SET_BLIP_SCALE(DestinationBlip, GB_BLIP_SIZE)
	SET_BLIP_PRIORITY(DestinationBlip, BLIPPRIORITY_HIGHEST)
	SET_BLIP_NAME_FROM_TEXT_FILE(DestinationBlip, "VIP_BLIP")
	SET_BLIP_COLOUR(DestinationBlip, BLIP_COLOUR_PURPLE)
	SET_BLIP_ROUTE(DestinationBlip, TRUE)
	PRINTLN( " [RCC MISSION] - DRAW_VIP_MARKER - BLIP ADDED -  vDrawPosition ", vDrawPosition)

ENDPROC

FUNC BOOL HOLD_UP_VEHHACK_OBJECTIVE_TEXT(INT iObjTxtPartToUse)
	INT i
	
	FOR i = 0 TO FMMC_MAX_VEHICLES - 1
		IF IS_BIT_SET(MC_PlayerBD[iObjTxtPartToUse].iVehDestroyBitset, i)
		AND NOT IS_BIT_SET(iPlayedDialogueForVehHackBS, i)
			PRINTLN("[SecuroHack][ObjectiveText][GDTEXT] - HACK VEH - Blocking objective text due to veh ", i)
			PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - HOLD_UP_VEHHACK_OBJECTIVE_TEXT")
			SET_BIT(iLocalBoolCheck3, LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

VECTOR vLastStoredWaypoint
BOOL bSentDrawMarkerRequest
VECTOR vNULLMARKER = <<9999,9999,9999>>


PROC MANAGE_VIP_MARKER_DRAWING()
	VECTOR vDrawPosition = MC_serverBD_3.vVIPMarker
	
	//Check if it needs updating
	IF DOES_BLIP_EXIST(DestinationBlip)
		IF NOT ARE_VECTORS_EQUAL(MC_serverBD_3.vVIPMarker, GET_BLIP_COORDS(DestinationBlip))
			REMOVE_BLIP(DestinationBlip)
			PRINTLN( " [RCC MISSION] - MANAGE_VIP_MARKER_DRAWING - NEEDS UPDATING  ")
		ENDIF
		
		IF ARE_VECTORS_EQUAL(MC_serverBD_3.vVIPMarker, vNULLMARKER)
			REMOVE_BLIP(DestinationBlip)
			PRINTLN( " [RCC MISSION] - MANAGE_VIP_MARKER_DRAWING - VIP REMOVED MARKER  ")
		ENDIF
		
		IF NOT IS_PED_INJURED(LocalPlayerPed)
			IF IS_ENTITY_AT_COORD(LocalPlayerPed,vDrawPosition,DEFAULT_LOCATES_SIZE_VECTOR())
				REMOVE_BLIP(DestinationBlip)
				PRINTLN( " [RCC MISSION] - MANAGE_VIP_MARKER_DRAWING - REMOVING BLIP PLAYER AT COORD ")
			ENDIF
		ENDIF
		
	ENDIF
	
	//Add Blip and Checkpoint
	IF NOT DOES_BLIP_EXIST(DestinationBlip)
		IF NOT ARE_VECTORS_EQUAL(vDrawPosition, <<0, 0, 0>>)
		AND NOT ARE_VECTORS_EQUAL(vDrawPosition, <<0, 0, -2000>>)
		AND NOT ARE_VECTORS_EQUAL(vDrawPosition, vNULLMARKER)
			
			IF NOT IS_PED_INJURED(LocalPlayerPed)
				IF NOT IS_ENTITY_AT_COORD(LocalPlayerPed,vDrawPosition,DEFAULT_LOCATES_SIZE_VECTOR())
					DRAW_VIP_MARKER(vDrawPosition)
				ENDIF
			ELSE
				DRAW_VIP_MARKER(vDrawPosition)
			ENDIF
			
		ENDIF
	ENDIF
ENDPROC


//For use with Entourage so the VIP can share their blip
PROC BROADCAST_VIP_MARKER_FROM_MENU()
	VECTOR vWaypoint
	IF IS_WAYPOINT_ACTIVE()
		vWaypoint = GET_BLIP_INFO_ID_COORD(GET_FIRST_BLIP_INFO_ID(GET_WAYPOINT_BLIP_ENUM_ID()))
		PRINTLN("[RCC MISSION] GET_VIP_MARKER_FROM_MENU  Waypoint active = ", vWaypoint)
	ELSE
		IF bSentDrawMarkerRequest
			BROADCAST_VIP_MARKER_POSITION(vNULLMARKER)
			vLastStoredWaypoint = vNULLMARKER
			bSentDrawMarkerRequest = FALSE
		ENDIF
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(vWaypoint)
		//only update if the position has changed
		IF NOT ARE_VECTORS_EQUAL(vWaypoint,vLastStoredWaypoint)
			vWaypoint.z = GET_APPROX_HEIGHT_FOR_AREA(vWaypoint.x-5, vWaypoint.y-5, vWaypoint.x+5, vWaypoint.y+5)
			BROADCAST_VIP_MARKER_POSITION(vWaypoint)
			vLastStoredWaypoint = vWaypoint
			PRINTLN("[RCC MISSION] GET_VIP_MARKER_FROM_MENU  vLastStoredWaypoint = ", vLastStoredWaypoint)
			bSentDrawMarkerRequest = TRUE
		ENDIF
	ENDIF
	
ENDPROC

//for use only with entourage - expand for further functionality
PROC SHARE_MARKER_WITH_SPECIFIED_TEAM()
	
	//iAssassinTeam = 0 
	INT iBodyguardTeam = 1
	INT iVIPTeam  = 2
	
	
	//Only do this if the creator setting is on 
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iBodyguardTeam].iTeamBitset, ciBS_TEAM_MARKER_VISIBILITY_T2)

		//Get VIP team
		//Get Bodyguard Team
		//Listen for when he sets a marker - all the time
		//Broadcast data to all players - if it changes
		//Add blip for appropriate teams - if it changes

		//If player is the VIP 
		IF MC_playerBD[iPartToUse].iteam = iVIPTeam
			BROADCAST_VIP_MARKER_FROM_MENU()
		ENDIF
		
		//If player is on the bodyguard team
		IF MC_playerBD[iPartToUse].iteam = iBodyguardTeam
			MANAGE_VIP_MARKER_DRAWING()
		ENDIF
	ENDIF

ENDPROC

PROC PROCESS_ALWAYS_SHOW_ONE_ENEMY_BLIP()

	INT iTeam = MC_PlayerBD[iLocalPart].iteam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_ALWAYS_SHOW_AT_LEAST_1_ENEMY_BLIP)
		EXIT
	ENDIF
	
	IF IS_PED_INJURED(LocalPlayerPed)
		EXIT
	ENDIF
		
	IF iPedBlipStaggeredLoop > -1
	AND iPedBlipStaggeredLoop < MC_serverBD.iNumPedCreated
		
		PRINTLN("[LM][PROCESS_ALWAYS_SHOW_ONE_ENEMY_BLIP] - iPedBlipStaggeredLoop: ", iPedBlipStaggeredLoop, " iCustBlipFrom: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedBlipStaggeredLoop].sPedBlipStruct.sTeamBlip[iTeam].iBlipOverrideStartRule)
		PRINTLN("[LM][PROCESS_ALWAYS_SHOW_ONE_ENEMY_BLIP] - iPedBlipStaggeredLoop: ", iPedBlipStaggeredLoop, " iCustBlipTo: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedBlipStaggeredLoop].sPedBlipStruct.sTeamBlip[iTeam].iBlipOverrideEndRule)
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedBlipStaggeredLoop].sPedBlipStruct.sTeamBlip[iTeam].iBlipOverrideStartRule > -1
			IF ((iRule >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedBlipStaggeredLoop].sPedBlipStruct.sTeamBlip[iTeam].iBlipOverrideStartRule)
			AND (iRule <= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedBlipStaggeredLoop].sPedBlipStruct.sTeamBlip[iTeam].iBlipOverrideEndRule OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedBlipStaggeredLoop].sPedBlipStruct.sTeamBlip[iTeam].iBlipOverrideEndRule = -1))
				PED_INDEX tempPed
				NETWORK_INDEX niEntity
				niEntity = MC_serverBD_1.sFMMC_SBD.niPed[iPedBlipStaggeredLoop]	
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niEntity)
					tempPed = NET_TO_PED(niEntity)
					
					IF NOT IS_PED_INJURED(tempPed)
						
						FLOAT fDist
						fDist = VDIST2(vPlayerPosCached, GET_ENTITY_COORDS(tempPed))
						IF fDist < fPedAlwaysShowOneBlipDistCache
							fPedAlwaysShowOneBlipDistCache = fDist
							iPedAlwaysShowOneBlipCache = iPedBlipStaggeredLoop
							PRINTLN("[LM][PROCESS_ALWAYS_SHOW_ONE_ENEMY_BLIP] - iPedBlipStaggeredLoop: ", iPedBlipStaggeredLoop, " fDist: ", fDist)
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		iPedBlipStaggeredLoop++
		
	ELSE
		
		PRINTLN("[LM][PROCESS_ALWAYS_SHOW_ONE_ENEMY_BLIP] - DONE STAGGERED - iPedAlwaysShowOneBlipCache: ", iPedAlwaysShowOneBlipCache, " fPedAlwaysShowOneBlipDistCache: ", fPedAlwaysShowOneBlipDistCache)
	
		iPedAlwaysShowOneBlipChosen = iPedAlwaysShowOneBlipCache		
		vPlayerPosCached = GET_ENTITY_COORDS(localPlayerPed)
		iPedBlipStaggeredLoop = 0
		fPedAlwaysShowOneBlipDistCache = 9999999
		iPedAlwaysShowOneBlipCache = -1
	ENDIF
	
	IF iPedAlwaysShowOneBlipChosen > -1
		PRINTLN("[LM][PROCESS_ALWAYS_SHOW_ONE_ENEMY_BLIP] - Blip - iPedAlwaysShowOneBlipChosen: ", iPedAlwaysShowOneBlipChosen)		
		SET_BIT(sMissionPedsLocalVars[iPedAlwaysShowOneBlipChosen].iPedBS, ci_MissionPedBS_ForcedBlip)
	ENDIF
		
ENDPROC

PROC PROCESS_CARNAGE_BAR_SERVER()
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_CarnageBarFilled_PassRule_CarnageFull)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_CarnageBarFilled_PassRule_HeliFootageFull)
		EXIT
	ENDIF
	
	IF NOT bIsLocalPlayerHost
		EXIT
	ENDIF
	
	INT iTeam = MC_PlayerBD[iLocalPart].iteam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule >= FMMC_MAX_RULES
		EXIT
	ENDIF
		
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_IS_CARNAGE_BAR_ACTIVE)
		EXIT
	ENDIF
			
	BOOL bAddSpeedPoints
	INT i, iHeliPlayers
	FLOAT fSpeedPointsPercent
	PARTICIPANT_INDEX piPlayerPart
	PLAYER_INDEX piPlayerCar
	PED_INDEX PedPlayerCar
	VEHICLE_INDEX vehPlayerCar	
	
	INT iPlayerCount = 0
	INT iMaxPlayers = GET_NUMBER_OF_PLAYING_PLAYERS()
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		PARTICIPANT_INDEX piPart = INT_TO_PARTICIPANTINDEX(i)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(piPart)			
			IF IS_BIT_SET(MC_playerBD_1[i].iCarnageBarBS, ci_CARNAGEBAR_BS_IS_USING_HELI_CAM)
			AND IS_BIT_SET(MC_ServerBD_1.iPartLookingAtCar, i)
				iHeliPlayers++
			ELIF IS_BIT_SET(MC_playerBD_1[i].iCarnageBarBS, ci_CARNAGEBAR_BS_IS_DRIVER_OF_CAR)
				piPlayerPart = INT_TO_PARTICIPANTINDEX(i)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(piPlayerPart)
					piPlayerCar = NETWORK_GET_PLAYER_INDEX(piPlayerPart)
					pedPlayerCar = GET_PLAYER_PED(piPlayerCar)
					IF NOT IS_PED_INJURED(pedPlayerCar)
						IF IS_PED_IN_ANY_VEHICLE(pedPlayerCar)
							vehPlayerCar = GET_VEHICLE_PED_IS_IN(pedPlayerCar)
							FLOAT fSpeedPercent = GET_VEHICLE_CURRENT_SPEED_PERCENTAGE(vehPlayerCar, (!IS_FAKE_MULTIPLAYER_MODE_SET()))
							IF (fSpeedPercent*100) > g_FMMC_STRUCT.sCarnageBar.fCarSpeedThreshold
								bAddSpeedPoints = TRUE
								fSpeedPointsPercent += (fSpeedPercent-(g_FMMC_STRUCT.sCarnageBar.fCarSpeedThreshold/100)) / (1.0-(g_FMMC_STRUCT.sCarnageBar.fCarSpeedThreshold/100))
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		iPlayerCount++
		IF iPlayerCount >= iMaxPlayers
			BREAKLOOP
		ENDIF
	ENDFOR
	
	IF NOT HAS_NET_TIMER_STARTED(tdCarnageBar_FillIncrementTimer)
		START_NET_TIMER(tdCarnageBar_FillIncrementTimer)
	ENDIF
	
	IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdCarnageBar_FillIncrementTimer, 2000)
		REINIT_NET_TIMER(tdCarnageBar_FillIncrementTimer)

		FLOAT fSpeedPoints
		IF bAddSpeedPoints			
			fSpeedPoints = (g_FMMC_STRUCT.sCarnageBar.fCarSpeedFillRate * fSpeedPointsPercent)
		ENDIF
		FLOAT fPedPoints = (g_FMMC_STRUCT.sCarnageBar.fCarPedKillFillRate * MC_ServerBD_1.fCarnageBar_PedKilledForFill)
		FLOAT fVehPoints = (g_FMMC_STRUCT.sCarnageBar.fCarVehKillFillRate * MC_ServerBD_1.fCarnageBar_VehDestroyedForFill)
		FLOAT fStuntPoints = (g_FMMC_STRUCT.sCarnageBar.fCarStuntPerformFillRate * MC_ServerBD_1.fCarnageBar_StuntPerformedForFill)
		MC_ServerBD_1.fCarnageBar_BarFilledCarTarget += -g_FMMC_STRUCT.sCarnageBar.fHeliDrainRate + (fSpeedPoints + fPedPoints + fVehPoints + fStuntPoints)						
		MC_ServerBD_1.fCarnageBar_PedKilledForFill = 0
		MC_ServerBD_1.fCarnageBar_VehDestroyedForFill = 0
		MC_ServerBD_1.fCarnageBar_StuntPerformedForFill = 0
		
		IF MC_ServerBD_1.fCarnageBar_BarFilledCarTarget < 0
			MC_ServerBD_1.fCarnageBar_BarFilledCarTarget = 0
		ELIF MC_ServerBD_1.fCarnageBar_BarFilledCarTarget > 100
			MC_ServerBD_1.fCarnageBar_BarFilledCarTarget = 100
		ENDIF
		
		PRINTLN("[LM][CARNAGE_BAR] - (Server) New Target Car Bar Fill: ", MC_ServerBD_1.fCarnageBar_BarFilledCarTarget, " fSpeedPoints: ", fSpeedPoints, " fPedPoints: ", fPedPoints, " fVehPoints: ", fVehPoints, " fStuntPoints: ", fStuntPoints, " fHeliDrainRate: ", g_FMMC_STRUCT.sCarnageBar.fHeliDrainRate)
		
		FLOAT fHeliPoints = 0.0
		IF MC_ServerBD_1.fCarnageBar_BarFilledCarTarget >= g_FMMC_STRUCT.sCarnageBar.fHeliTargetPositiveThreshold				
		AND iHeliPlayers > 0
			fHeliPoints = g_FMMC_STRUCT.sCarnageBar.fHeliFillRate * iHeliPlayers
		ENDIF
		MC_ServerBD_1.fCarnageBar_BarFilledHeliTarget += -g_FMMC_STRUCT.sCarnageBar.fHeliDrainRate + fHeliPoints
		
		IF MC_ServerBD_1.fCarnageBar_BarFilledHeliTarget < 0
			MC_ServerBD_1.fCarnageBar_BarFilledHeliTarget = 0
		ELIF MC_ServerBD_1.fCarnageBar_BarFilledHeliTarget >= 100
			MC_ServerBD_1.fCarnageBar_BarFilledHeliTarget = 100		
		ENDIF
		
		PRINTLN("[LM][CARNAGE_BAR] - (Server) New Target Heli Bar Fill: ", MC_ServerBD_1.fCarnageBar_BarFilledHeliTarget, " fHeliPoints: ", fHeliPoints, " fHeliDrainRate: ", g_FMMC_STRUCT.sCarnageBar.fHeliDrainRate, " iHeliPlayers: ", iHeliPlayers)
				
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_CarnageBarFilled_PassRule_CarnageFull)
		AND MC_ServerBD_1.fCarnageBar_BarFilledCarTarget >= 100
			IF NOT HAS_NET_TIMER_STARTED(tdCarnageBar_MoveOnRuleTimer)
				START_NET_TIMER(tdCarnageBar_MoveOnRuleTimer)
				PRINTLN("[LM][CARNAGE_BAR] - (Server) Starting New Rule Timer now... (Carnage Bar)")
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_CarnageBarFilled_PassRule_HeliFootageFull)
		AND MC_ServerBD_1.fCarnageBar_BarFilledHeliTarget >= 100
			IF NOT HAS_NET_TIMER_STARTED(tdCarnageBar_MoveOnRuleTimer)
				START_NET_TIMER(tdCarnageBar_MoveOnRuleTimer)
				PRINTLN("[LM][CARNAGE_BAR] - (Server) Starting New Rule Timer now... (Heli Footage)")
			ENDIF
		ENDIF
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(tdCarnageBar_MoveOnRuleTimer)
	AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdCarnageBar_MoveOnRuleTimer, 3000)
		PRINTLN("[LM][CARNAGE_BAR] - (Server) Timer Expired Moving rule on....")
		
		RESET_NET_TIMER(tdCarnageBar_MoveOnRuleTimer)
		
		MC_ServerBD_1.fCarnageBar_BarFilledCarTarget = 0
		MC_ServerBD_1.fCarnageBar_BarFilledHeliTarget = 0
		
		MC_ServerBD_1.fCarnageBar_PedKilledForFill = 0
		MC_ServerBD_1.fCarnageBar_VehDestroyedForFill = 0
		MC_ServerBD_1.fCarnageBar_StuntPerformedForFill = 0

		FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
			SET_PROGRESS_OBJECTIVE_FOR_TEAM(iTeam, TRUE)
		ENDFOR
	ENDIF
ENDPROC

PROC PROCESS_CARNAGE_BAR_CLIENT()
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_CarnageBarFilled_PassRule_CarnageFull)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_CarnageBarFilled_PassRule_HeliFootageFull)
		EXIT
	ENDIF
	
	INT iTeam = MC_PlayerBD[iLocalPart].iteam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule >= FMMC_MAX_RULES
		EXIT
	ENDIF
		
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_IS_CARNAGE_BAR_ACTIVE)
		EXIT
	ENDIF

	BOOL bLookingAtCar

	IF NOT IS_PED_INJURED(localPlayerPed)
	AND IS_PED_IN_ANY_VEHICLE(localPlayerPed)
		VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(localPlayerPed)		
		MODEL_NAMES mnVeh = GET_ENTITY_MODEL(vehPlayer)
		BOOL bDriver = GET_PED_IN_VEHICLE_SEAT(vehPlayer, VS_DRIVER) = localPlayerPed
				
		IF bDriver
			IF NOT IS_THIS_MODEL_A_HELI(mnVeh)
			AND NOT IS_THIS_MODEL_A_PLANE(mnVeh)				
				BOOL bValidVehicle = ARE_WE_IN_A_VALID_CARNAGE_BAR_VEHICLE()
				IF NOT IS_BIT_SET(MC_playerBD_1[iLocalPart].iCarnageBarBS, ci_CARNAGEBAR_BS_IS_DRIVER_OF_CAR)
				AND bValidVehicle
					SET_BIT(MC_playerBD_1[iLocalPart].iCarnageBarBS, ci_CARNAGEBAR_BS_IS_DRIVER_OF_CAR)
					PRINTLN("[LM][CARNAGE_BAR] - (Client) Setting ci_CARNAGEBAR_BS_IS_DRIVER_OF_CAR")
				ENDIF
				
				IF IS_ENTITY_IN_AIR(vehPlayer)
					IF NOT HAS_NET_TIMER_STARTED(tdCarnageBar_BigAirTimer)
						START_NET_TIMER(tdCarnageBar_BigAirTimer)
					ENDIF					
					IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdCarnageBar_BigAirTimer, 3500)
						PRINTLN("[LM][CARNAGE_BAR] - (Client) Performed Stunt: Airtime 3.5 seconds.")
						RESET_NET_TIMER(tdCarnageBar_BigAirTimer)
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedContentEventType_CarnageBarPerformedStuntAir)
					ENDIF
				ELSE
					IF HAS_NET_TIMER_STARTED(tdCarnageBar_BigAirTimer)
						RESET_NET_TIMER(tdCarnageBar_BigAirTimer)
					ENDIF
				ENDIF
			ELIF IS_THIS_MODEL_A_HELI(mnVeh)
				IF NOT IS_BIT_SET(MC_playerBD_1[iLocalPart].iCarnageBarBS, ci_CARNAGEBAR_BS_IS_DRIVER_OF_HELI)
					SET_BIT(MC_playerBD_1[iLocalPart].iCarnageBarBS, ci_CARNAGEBAR_BS_IS_DRIVER_OF_HELI)
					PRINTLN("[LM][CARNAGE_BAR] - (Client) Setting ci_CARNAGEBAR_BS_IS_DRIVER_OF_HELI")
				ENDIF
			ELSE
				IF IS_BIT_SET(MC_playerBD_1[iLocalPart].iCarnageBarBS, ci_CARNAGEBAR_BS_IS_DRIVER_OF_HELI)
					CLEAR_BIT(MC_playerBD_1[iLocalPart].iCarnageBarBS, ci_CARNAGEBAR_BS_IS_DRIVER_OF_HELI)
					PRINTLN("[LM][CARNAGE_BAR] - (Client) Clearing ci_CARNAGEBAR_BS_IS_DRIVER_OF_HELI")
				ENDIF
				IF IS_BIT_SET(MC_playerBD_1[iLocalPart].iCarnageBarBS, ci_CARNAGEBAR_BS_IS_DRIVER_OF_CAR)
					CLEAR_BIT(MC_playerBD_1[iLocalPart].iCarnageBarBS, ci_CARNAGEBAR_BS_IS_DRIVER_OF_CAR)
					PRINTLN("[LM][CARNAGE_BAR] - (Client) Clearing ci_CARNAGEBAR_BS_IS_DRIVER_OF_CAR")
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(MC_playerBD_1[iLocalPart].iCarnageBarBS, ci_CARNAGEBAR_BS_IS_DRIVER_OF_CAR)
			CLEAR_BIT(MC_playerBD_1[iLocalPart].iCarnageBarBS, ci_CARNAGEBAR_BS_IS_DRIVER_OF_CAR)
			PRINTLN("[LM][CARNAGE_BAR] - (Client) Clearing ci_CARNAGEBAR_BS_IS_DRIVER_OF_CAR")
		ENDIF
	ENDIF
	
	IF IS_LOCAL_PLAYER_USING_TURRET_CAM()
		IF NOT IS_BIT_SET(MC_playerBD_1[iLocalPart].iCarnageBarBS, ci_CARNAGEBAR_BS_IS_USING_HELI_CAM)
			SET_BIT(MC_playerBD_1[iLocalPart].iCarnageBarBS, ci_CARNAGEBAR_BS_IS_USING_HELI_CAM)
			PRINTLN("[LM][CARNAGE_BAR] - (Client) Setting ci_CARNAGEBAR_BS_IS_USING_HELI_CAM")
		ENDIF
		
		PARTICIPANT_INDEX partPlayerCarTarget
		PLAYER_INDEX piPlayerCarTarget
		PED_INDEX pedPlayerCarTarget
		VEHICLE_INDEX vehPlayerCarTarget
		
		INT i = 0
		INT iPlayerCount = 0
		INT iMaxPlayers = GET_NUMBER_OF_PLAYING_PLAYERS()
		FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
			partPlayerCarTarget = INT_TO_PARTICIPANTINDEX(i)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(partPlayerCarTarget)
				IF IS_BIT_SET(MC_playerBD_1[i].iCarnageBarBS, ci_CARNAGEBAR_BS_IS_DRIVER_OF_CAR)
							
					PRINTLN("[LM][CARNAGE_BAR] - (Client) iPlayerCarTarget: ", i)
		
					piPlayerCarTarget = NETWORK_GET_PLAYER_INDEX(partPlayerCarTarget)
					pedPlayerCarTarget = GET_PLAYER_PED(piPlayerCarTarget)
					
					IF NOT IS_PED_INJURED(pedPlayerCarTarget)
						IF IS_PED_IN_ANY_VEHICLE(pedPlayerCarTarget)
							vehPlayerCarTarget = GET_VEHICLE_PED_IS_IN(pedPlayerCarTarget)
							
							IF IS_VEHICLE_DRIVEABLE(vehPlayerCarTarget)
								REQUEST_STREAMED_TEXTURE_DICT("helicopterhud")
								IF HAS_STREAMED_TEXTURE_DICT_LOADED("helicopterhud")
																		
									FLOAT fBoxDrawSizeX = 0.025
									FLOAT fBoxDrawSizeY = 0.03
									FLOAT xVehPos = 0
									FLOAT yVehPos = 0
									INT r = 0
									INT g = 0
									INT b = 0
									INT a = 0
									
									SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)	
									SET_DRAW_ORIGIN(GET_ENTITY_COORDS(vehPlayerCarTarget), FALSE)	
									GET_SCREEN_COORD_FROM_WORLD_COORD(GET_ENTITY_COORDS(vehPlayerCarTarget), xVehPos, yVehPos)
									GET_HUD_COLOUR(HUD_COLOUR_GREEN, r, g, b, a)									
									CLEAR_DRAW_ORIGIN()
									
									DRAW_SPRITE("helicopterhud", "hud_corner", 0.4, 0.4, fBoxDrawSizeX, fBoxDrawSizeY, 0, r, g, b, a, TRUE)
									DRAW_SPRITE("helicopterhud", "hud_corner", 0.6, 0.4, fBoxDrawSizeX, fBoxDrawSizeY, 90, r, g, b, a, TRUE)
									DRAW_SPRITE("helicopterhud", "hud_corner", 0.4, 0.6, fBoxDrawSizeX, fBoxDrawSizeY, 270, r, g, b, a, TRUE)
									DRAW_SPRITE("helicopterhud", "hud_corner", 0.6, 0.6, fBoxDrawSizeX, fBoxDrawSizeY, 180, r, g, b, a, TRUE)
									
									PRINTLN("[LM][CARNAGE_BAR] - (Client) Drawing Boxes - xVehPos: ", xVehPos, " yVehPos: ", yVehPos)
																		
									IF xVehPos > 0.4 AND xVehPos < 0.6
									AND yVehPos > 0.4 AND yVehPos < 0.6
										DRAW_BOX_AROUND_TARGET_VEHICLE(vehPlayerCarTarget, DEFAULT, HUD_COLOUR_GREEN)
										PRINTLN("[LM][CARNAGE_BAR] - (Client) bLookingAtCar = TRUE")
										bLookingAtCar = TRUE
									ELSE
										DRAW_BOX_AROUND_TARGET_VEHICLE(vehPlayerCarTarget, DEFAULT, HUD_COLOUR_RED)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				iPlayerCount++
				IF iPlayerCount >= iMaxPlayers
					BREAKLOOP
				ENDIF
			ENDIF
		ENDFOR
	ELSE
		IF IS_BIT_SET(MC_playerBD_1[iLocalPart].iCarnageBarBS, ci_CARNAGEBAR_BS_IS_USING_HELI_CAM)
			CLEAR_BIT(MC_playerBD_1[iLocalPart].iCarnageBarBS, ci_CARNAGEBAR_BS_IS_USING_HELI_CAM)			
			PRINTLN("[LM][CARNAGE_BAR] - (Client) Clearing ci_CARNAGEBAR_BS_IS_USING_HELI_CAM")
		ENDIF
	ENDIF
	
	IF bLookingAtCar
		RESET_NET_TIMER(tdCarnageBar_LookingAtCarResetTimer)
		IF NOT IS_BIT_SET(MC_playerBD_1[iLocalPart].iCarnageBarBS, ci_CARNAGEBAR_BS_LOOKING_AT_CAR)
			BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedContentEventType_HeliCamLookingAtCar)
			SET_BIT(MC_playerBD_1[iLocalPart].iCarnageBarBS, ci_CARNAGEBAR_BS_LOOKING_AT_CAR)
			PRINTLN("[LM][CARNAGE_BAR] - (Client) Resetting Timer: tdCarnageBar_LookingAtCarResetTimer")
			PRINTLN("[LM][CARNAGE_BAR] - (Client) Setting ci_CARNAGEBAR_BS_LOOKING_AT_CAR")
		ENDIF
	ELSE
		IF NOT HAS_NET_TIMER_STARTED(tdCarnageBar_LookingAtCarResetTimer)
			START_NET_TIMER(tdCarnageBar_LookingAtCarResetTimer)
			PRINTLN("[LM][CARNAGE_BAR] - (Client) Starting Timer: tdCarnageBar_LookingAtCarResetTimer")
		ENDIF		
		IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdCarnageBar_LookingAtCarResetTimer, 400)
			IF IS_BIT_SET(MC_playerBD_1[iLocalPart].iCarnageBarBS, ci_CARNAGEBAR_BS_LOOKING_AT_CAR)
				BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedContentEventType_HeliCamStoppedLookingAtCar)
				CLEAR_BIT(MC_playerBD_1[iLocalPart].iCarnageBarBS, ci_CARNAGEBAR_BS_LOOKING_AT_CAR)
				PRINTLN("[LM][CARNAGE_BAR] - (Client) Setting ci_CARNAGEBAR_BS_LOOKING_AT_CAR")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_CARNAGE_BAR_HUD()
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_CarnageBarFilled_PassRule_CarnageFull)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_CarnageBarFilled_PassRule_HeliFootageFull)
		EXIT
	ENDIF	
	
	INT iTeam = MC_PlayerBD[iLocalPart].iteam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule >= FMMC_MAX_RULES
		EXIT
	ENDIF
		
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_IS_CARNAGE_BAR_ACTIVE)
		EXIT
	ENDIF
	
	IF MC_ServerBD_1.fCarnageBar_BarFilledHeliTarget > -1
	AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_CarnageBar_UseHeliCam)
		INTERP_FLOAT_TO_TARGET_AT_SPEED(fCarnageBar_BarFilledHeliHUD, MC_ServerBD_1.fCarnageBar_BarFilledHeliTarget, 0.3)		
		INT iCarnageBar_BarFilledHeliHUD = ROUND(fCarnageBar_BarFilledHeliHUD)
		
		TEXT_LABEL_63 tl63 = "CRNBAR_HELI"
		DRAW_GENERIC_METER(iCarnageBar_BarFilledHeliHUD, 100, tl63, HUD_COLOUR_WHITE, DEFAULT, HUDORDER_THIRDBOTTOM, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 
			DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, iCarnageBar_BarFilledHeliHUD)
	ENDIF
	
	IF MC_ServerBD_1.fCarnageBar_BarFilledCarTarget > -1
		INTERP_FLOAT_TO_TARGET_AT_SPEED(fCarnageBar_BarFilledCarHUD, MC_ServerBD_1.fCarnageBar_BarFilledCarTarget, 0.3)		
				
		INT iCarnageBar_BarFilledCarHUD = ROUND(fCarnageBar_BarFilledCarHUD)		
		INT iConst = ROUND(g_FMMC_Struct.sCarnageBar.fHeliTargetPositiveThreshold)
		PERCENTAGE_METER_LINE PercentageLine = GET_PERCENTAGE_METER_LINE_FROM_PERCENTAGE_INT(iConst)
		IF iConst = 100
			PercentageLine = PERCENTAGE_METER_LINE_NONE
		ENDIF
		
		TEXT_LABEL_63 tl63 = "CRNBAR_CAR"
		DRAW_GENERIC_METER(iCarnageBar_BarFilledCarHUD, 100, tl63, HUD_COLOUR_WHITE, DEFAULT, HUDORDER_THIRDBOTTOM, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT,
			DEFAULT, PercentageLine, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, iCarnageBar_BarFilledCarHUD)
	ENDIF
ENDPROC

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: PROCESS OBJECTIVE TEXT PROCEDURE !
//
//************************************************************************************************************************************************************

FUNC TEXT_LABEL_63 GET_CUSTOM_OBJECTIVE_TEXT_LABEL(INT iTeam, INT iRule, TEXT_LABEL_63 tl63CustomObjText)
	
	TEXT_LABEL_63 tl63ObjectiveText
	
	tl63ObjectiveText = tl63CustomObjText
	
	INT iR, iG, iB,iA
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[iRule], ciBS_RULE8_OVERRIDE_FRIENDLY_WITH_GANG)
		GET_HUD_COLOUR(GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam,PlayerToUse),iR, iG, iB,iA)
		SET_SCRIPT_VARIABLE_HUD_COLOUR(iR, iG, iB,iA)
		PRINTLN("[RCC MISSION] PRE change: ", tl63ObjectiveText)
		REPLACE_STRING_IN_STRING(tl63ObjectiveText,"~f~", "~v~")
		PRINTLN("[RCC MISSION] POST change: ", tl63ObjectiveText, " ~v~ colour: ",iR,", ", iG,", ", iB,", ",iA)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFive[iRule], ciBS_RULE5_OBJECTIVE_TEXT_USES_RSG_OVERRIDE)
	AND g_FMMC_STRUCT.iRandomEntitySpawn_MaxNumber > 0
	AND g_FMMC_STRUCT.iRandomEntitySpawn_RandomNumbersToUse != 0
		
		INT i
		
		FOR i = 0 TO (g_FMMC_STRUCT.iRandomEntitySpawn_MaxNumber - 1)
			IF IS_BIT_SET(MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS, i)
				IF i < ciMAX_NUM_OBJECTIVETEXT_RSG_OVERRIDES
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl63ObjectiveText_SpawnGroupOverride[i])
						tl63ObjectiveText = g_FMMC_STRUCT.tl63ObjectiveText_SpawnGroupOverride[i]
						i = g_FMMC_STRUCT.iRandomEntitySpawn_MaxNumber // Break out!
					ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION] GET_CUSTOM_OBJECTIVE_TEXT_LABEL - Trying to check objective text for a spawn group > ciMAX_NUM_OBJECTIVETEXT_RSG_OVERRIDES!")
					PRINTLN("[RCC MISSION] GET_CUSTOM_OBJECTIVE_TEXT_LABEL - Trying to check objective text for a spawn group > ciMAX_NUM_OBJECTIVETEXT_RSG_OVERRIDES! i = ",i)
				#ENDIF
				ENDIF
			ENDIF
		ENDFOR
		
	ENDIF
	
	
	
	RETURN tl63ObjectiveText
	
ENDFUNC

FUNC BOOL DRAW_OBJECTIVE_TEXT_FOR_WVM_TRAILERLARGE(INT iTeam, INT iRule, TEXT_LABEL_63 tl63CustomGodText1, TEXT_LABEL_63 tl63CustomGodText2)
	VEHICLE_INDEX viTemp
	IF iTeam < FMMC_MAX_TEAMS
	AND iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_USE_ALTERNATIVE_GOTO_VEH_TEXT)//use alternative text on this rule
			PRINTLN("[RCC MISSION] DRAW_OBJECTIVE_TEXT_FOR_WVM_TRAILERLARGE - Bit is set")
			PRINTLN("[RCC MISSION] DRAW_OBJECTIVE_TEXT_FOR_WVM_TRAILERLARGE - tl63CustomGodText1: ", tl63CustomGodText1,"tl63CustomGodText2", tl63CustomGodText2)
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				viTemp = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
				IF IS_ENTITY_ALIVE(viTemp)
					IF GET_ENTITY_MODEL(viTemp) = HAULER2
						IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
							PRINTLN("[RCC MISSION] DRAW_OBJECTIVE_TEXT_FOR_WVM_TRAILERLARGE - In the Hauler, show tl63CustomGodText1")
							RETURN TRUE
						ELSE
							RETURN FALSE
						ENDIF
					ELSE
						IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText2)
							PRINTLN("[RCC MISSION] DRAW_OBJECTIVE_TEXT_FOR_WVM_TRAILERLARGE - In a vehicle but not the right vehicle, show tl63CustomGodText2")
							RETURN TRUE
						ELSE
							RETURN FALSE
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText2)
					PRINTLN("[RCC MISSION] DRAW_OBJECTIVE_TEXT_FOR_WVM_TRAILERLARGE - Not in a vehicle, show tl63CustomGodText2")
					RETURN TRUE
				ELSE
					RETURN FALSE
				ENDIF
			ENDIF		
		ELSE
			IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
				PRINTLN("[RCC MISSION] DRAW_OBJECTIVE_TEXT_FOR_WVM_TRAILERLARGE - Option not set, showing tl63CustomGodText1")
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PROCESS_ENTITY_DESTROYED_OBJECTS_HUD(INT iTeam, INT iRule)
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectsDestroyedRuleHudAmount[iRule] > 0
		
		IF bIsLocalPlayerHost
		AND MC_ServerBD_4.iObjectsRequiredForSpecialHUD[iTeam] !=  g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectsDestroyedRuleHudAmount[iRule]
			PRINTLN("PROCESS_ENTITY_DESTROYED_OBJECTS_HUD - Updating iObjectsRequiredForSpecialHUD to ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectsDestroyedRuleHudAmount[iRule])
			MC_ServerBD_4.iObjectsRequiredForSpecialHUD[iTeam] = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectsDestroyedRuleHudAmount[iRule]
		ENDIF
		
		INT iMaxAmount = MC_ServerBD_4.iObjectsRequiredForSpecialHUD[iTeam]
		INT iAmount = iMaxAmount - MC_ServerBD_4.iObjectsDestroyedForSpecialHUD[iTeam]
		
		DRAW_GENERIC_BIG_NUMBER(iAmount, "MC_GTRGS_HUD")
	ENDIF
ENDPROC

PROC PROCESS_ENTITY_DESTROYED_HUD(INT iTeam, INT iRule)
	PROCESS_ENTITY_DESTROYED_OBJECTS_HUD(iTeam, iRule)
ENDPROC

FUNC BOOL SHOULD_TAG_TEAM_USE_ALT_TEXT()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Enable_Turn_Rotor_Tag_Team)
	AND NOT (IS_PLAYER_SPECTATOR_ONLY(LocalPlayer) OR DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(LocalPlayer))
	AND IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES)
	AND IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_TAG_TEAM_USE_SECONDARY_TEXT()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Enable_Turn_Rotor_Tag_Team)
	AND NOT (IS_PLAYER_SPECTATOR_ONLY(LocalPlayer) OR DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(LocalPlayer))
	AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES)
	AND IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_SHOW_GAIN_WANTED_LEVEL_OBJECTIVE_TEXT()
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
	
	IF iRule < FMMC_MAX_RULES
		PRINTLN("[ML][SHOULD_SHOW_GAIN_WANTED_LEVEL_OBJECTIVE_TEXT] Number of stars = ", GET_PLAYER_WANTED_LEVEL(PLAYER_ID()), "   Creator set wanted requirement = ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedLevelReq[iRule])
		IF g_FMMC_STRUCT.sFMMCEndConditions[iteam].iWantedLevelReq[iRule] > -2
		AND IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
		AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) <= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedLevelReq[iRule]		// [ML] Creator set wanted level 0 = 1 star requried
			PRINTLN("[MJL][SHOULD_SHOW_GAIN_WANTED_LEVEL_OBJECTIVE_TEXT] Returning TRUE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	PRINTLN("[MJL][SHOULD_SHOW_GAIN_WANTED_LEVEL_OBJECTIVE_TEXT] Returning FALSE")
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_GAIN_WANTED_LEVEL()
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
	IF iRule < FMMC_MAX_RULES
		PRINTLN("[MJL][GET_GAIN_WANTED_LEVEL] Returning ", g_FMMC_STRUCT.sFMMCEndConditions[iteam].iWantedLevelReq[iRule], " star wanted requirement")
		RETURN (g_FMMC_STRUCT.sFMMCEndConditions[iteam].iWantedLevelReq[iRule]) + 1
	ENDIF
	
	RETURN 0
ENDFUNC
// [FIX_2020_CONTROLLER] - Shared Script Print logging Tag... 
// [FIX_2020_CONTROLLER] - We really need to write a new function that supports 5+ objective texts for each rule type and well-docment how each one of thee trigger.
// There are so many hacks in here to get objective text to play, and really we should just have clear events that trigger each one for each rule. This is a mess, see down.
// manages the god text for the mission objectives.
PROC PROCESS_OBJECTIVE_TEXT()
	TEXT_LABEL_63 tl63CustomGodText
	TEXT_LABEL_63 tl63CustomGodText1
	TEXT_LABEL_63 tl63CustomGodText2
	BOOL bobjectivedisp
	BOOL bcustomobjective 
	BOOL bDispHelpObjective
	BOOL bNoSecondObjective
	BOOL bPlural
	INT iTeam, i
	BOOL bFoundAiBlip
	BOOL bBlockObjectiveTextThisFrame = FALSE
	TEXT_LABEL_63 tl63RoamingSpectatorOverride
	BOOL bUsingRoamingSpectatorOverride 
	INT iobj
	BOOL bPrintSecondary = FALSE
	INT iNumCokeTrolleyObjs = 0
	INT iNumObjsBeingHacked = 0

	INT iAvengerDriver = -1
	BOOL bAvengerHasDriver = FALSE

	INT iObjTxtPartToUse = iPartToUse
	PLAYER_INDEX ObjTxtPlayerToUse = PlayerToUse

	IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
	AND iSpectatorTarget != -1
		iObjTxtPartToUse = iSpectatorTarget
		ObjTxtPlayerToUse = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iObjTxtPartToUse))	
	ENDIF
	
	IF (IS_LOCAL_PLAYER_ANY_SPECTATOR() AND GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_SETUP)
		PRINTLN("PROCESS_OBJECTIVE_TEXT - Setting bBlockObjectiveTextThisFrame to TRUE || Blocking objective text because we're a spectator the player we're watching hasn't started playing yet")
		bBlockObjectiveTextThisFrame = TRUE
	ENDIF
	
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]	
	IF iRule < FMMC_MAX_RULES
		IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl63_RoamingSpectatorObjectiveTextOverride[MC_playerBD[iObjTxtPartToUse].iteam])
		AND NOT ARE_STRINGS_EQUAL(g_FMMC_STRUCT.tl63_RoamingSpectatorObjectiveTextOverride[MC_playerBD[iObjTxtPartToUse].iteam], "0")
		AND NOT ARE_STRINGS_EQUAL(g_FMMC_STRUCT.tl63_RoamingSpectatorObjectiveTextOverride[MC_playerBD[iObjTxtPartToUse].iteam], " ")
			tl63RoamingSpectatorOverride = g_FMMC_STRUCT.tl63_RoamingSpectatorObjectiveTextOverride[MC_playerBD[iObjTxtPartToUse].iteam]
		ENDIF
		
		IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iObjTxtPartToUse].iteam].tl63RoamSpecObjText[iRule])
		AND NOT ARE_STRINGS_EQUAL(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iObjTxtPartToUse].iteam].tl63RoamSpecObjText[iRule], "0")
		AND NOT ARE_STRINGS_EQUAL(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iObjTxtPartToUse].iteam].tl63RoamSpecObjText[iRule], " ")
			tl63RoamingSpectatorOverride = g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iObjTxtPartToUse].iteam].tl63RoamSpecObjText[iRule]
		ENDIF
		
		bUsingRoamingSpectatorOverride = IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
		
		// Block it for special cases 
		IF SHOULD_TAG_TEAM_USE_SECONDARY_TEXT()
			bUsingRoamingSpectatorOverride = FALSE
		ENDIF
	ENDIF

	IF (NOT HAS_TEAM_FINISHED( MC_playerBD[ iObjTxtPartToUse ].iteam ) OR bUsingRoamingSpectatorOverride)
	AND (NOT IS_BIT_SET( MC_playerBD[ iObjTxtPartToUse ].iClientBitSet, PBBOOL_FINISHED ) OR bUsingRoamingSpectatorOverride)
	AND (NOT IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet2, PBBOOL2_PLAYER_OUT_BOUNDS_FAIL) OR bUsingRoamingSpectatorOverride)
	AND NOT IS_CUTSCENE_PLAYING()
	AND NOT SHOULD_HIDE_THE_HUD_THIS_FRAME()
	AND (NOT (IS_LOCAL_PLAYER_ANY_SPECTATOR() AND GET_SPECTATOR_CURRENT_FOCUS_PED() != GET_SPECTATOR_DESIRED_FOCUS_PED()) OR IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE())
	AND NOT (bBlockObjectiveTextThisFrame)
	AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_REQUEST_TAG_OUT)
	AND (NOT g_bCelebrationScreenIsActive OR (IS_LOCAL_PLAYER_ANY_SPECTATOR() AND NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()))
	AND (NOT g_bMissionEnding OR (IS_LOCAL_PLAYER_ANY_SPECTATOR() AND NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()))
	AND NOT IS_ON_SCREEN_WHEEL_SPIN_MINIGAME_ACTIVE()
		
		IF (GET_FRAME_COUNT() % 30) = 0
			PRINTLN("PROCESS_OBJECTIVE_TEXT - We are okay to do Objective Text Stuff...")
		ENDIF
		
		IF MC_serverBD.iTotalNumPart > 1
			IF NOT IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
			AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
				IF NOT HAS_FIRST_STRAND_MISSION_BEEN_PASSED() 
				AND NOT HAS_FIRST_STRAND_MISSION_BEEN_PASSED_NO_RESET()// 1975112
					IF NOT IS_BIT_SET(iLocalBoolCheck,LBOOL_DPAD_HELP_DISP)
						IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_EVERYONE_RUNNING)
							IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
								IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
									
									IF Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer)
										// "~s~Press ~INPUT_FRONTEND_DOWN~ to view the Capture Scores."
										PRINT_HELP("MC_SCORE_HC")
									ELSE
										IF IS_BIT_SET( g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciHWN_PRIORTISE_HELPTEXT) 
										AND NOT IS_BIT_SET( iLocalBoolCheck13, LBOOL13_PRIORTISED_FVJ_HELPTEXT )
											IF MC_PlayerBD[iPartToUse].iteam = 0
												// "Press ~INPUT_CONTEXT~ when aiming to turn the shotgun's flashlight on and off."
												PRINT_HELP("Lowrider_Help_21")
											ELSE
												// "Turn the flashlight on by aiming with ~INPUT_AIM~."
												IF NOT IS_CELLPHONE_CAMERA_IN_USE()
													PRINTLN("Printing Flashlight Help (2)")
													PRINT_HELP("Lowrider_Help_25")
												ENDIF
											ENDIF
											
											SET_BIT( iLocalBoolCheck13, LBOOL13_PRIORTISED_FVJ_HELPTEXT )
										ELIF IS_BIT_SET( g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciPRIORTISE_THANKSGIVING_HELPTEXT) 
										AND NOT IS_BIT_SET( iLocalBoolCheck13, LBOOL13_PRIORTISED_THANKSGIVING_HELPTEXT )
										
											// "Keep moving or you will become visible on the Radar to other players. When visible, your Radar arrow will turn red."
											PRINT_HELP("Lowrider_Help_28")
											SET_BIT( iLocalBoolCheck13, LBOOL13_PRIORTISED_THANKSGIVING_HELPTEXT )
										ELSE
											IF IS_BIT_SET(MC_serverBD.iOptionsMenuBitSet,ciNO_POINTS_HUD)
												// "~s~Press ~INPUT_FRONTEND_DOWN~ to view the players on the current Job."
												IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciSKIP_LEADERBOARD_HELP_TEXT)
													PRINT_HELP("MC_SCORE_H1")
												ENDIF
											ELSE
												// "~s~Press ~INPUT_FRONTEND_DOWN~ to view players on the job."
												IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciSKIP_PLAYERS_ON_JOB_HELP)
													PRINT_HELP("MC_SCORE_H")
												ENDIF
											ENDIF
											
											SET_BIT(iLocalBoolCheck,LBOOL_DPAD_HELP_DISP)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_CELLPHONE_CAMERA_IN_USE()
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("Lowrider_Help_25")
				CLEAR_HELP(TRUE)
			ENDIF
		ENDIF
		
		//2195360
		IF IS_CIRCUIT_HACKING_MINIGAME_RUNNING(hackingMinigameData)
			EXIT
		ENDIF
		
		// Force Target Score bottom Right HUD.
		IF NOT SHOULD_HIDE_THE_HUD_THIS_FRAME()
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_FORCED_TARGET_SCORE_HUD)
			AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH)
			AND IS_NET_PLAYER_OK(PlayerToUse)
				INT iTargetScore = g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionScoreLimit - g_FMMC_STRUCT.iInitialPoints[0]
				DRAW_GENERIC_BIG_NUMBER(iTargetScore, "MC_OSCRLIMY", -1, HUD_COLOUR_WHITE,HUDORDER_SEVENTHBOTTOM)
			ENDIF
		ENDIF
		
		IF iRule < FMMC_MAX_RULES
			PROCESS_ENTITY_DESTROYED_HUD(MC_playerBD[iObjTxtPartToUse].iteam, iRule)
		ENDIF
		
		MANAGE_DELIVERY_BLIP()		
		
		PROCESS_SHARD_TEXT()
		
		PROCESS_CARNAGE_BAR_HUD()
	
		MAINTAIN_GUNSMITH_BARS()
	
		MAINTAIN_TURFWARS_BARS()		
	
		MAINTAIN_RUINER_AMMO_UI()
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_COUNTERMEASURE_UI)
			DISPLAY_COUNTERMEASURE_COOLDOWN()
		ENDIF
		
		IF iRule < FMMC_MAX_RULES
			IF NOT IS_PED_INJURED(localPlayerPed)
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetEight[iRule], ciBS_RULE8_GPS_TRACKING_DISTANCE_METER)
					IF NOT SHOULD_HIDE_THE_HUD_THIS_FRAME()
						INT iBars = 0
						FOR iBars = 0 TO ci_TOTAL_DISTANCE_CHECK_BARS-1
							IF iGPSTrackingMeterPedIndex[iBars] > -1
								IF DOES_PED_REQUIRE_DISTANCE_TRACKING(iGPSTrackingMeterPedIndex[iBars], iRule)
									IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iGPSTrackingMeterPedIndex[iBars]])
										IF NOT IS_PED_INJURED(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iGPSTrackingMeterPedIndex[iBars]]))
											MAINTAIN_GPS_DISTANCE_METER_BAR(iBars, iRule)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
			IF iLastSpectatedTarget != iSpectatorTarget
			OR iLastPartToUse != iObjTxtPartToUse
				PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - iLastSpectatedTarget != iSpectatorTarget")			
				SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
				PRINTLN("[JS][UOBJ - PED] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - iLastSpectatedTarget != iSpectatorTarget")			
				SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_PED_OBJECTIVE)
				PRINTLN("[JS][UOBJ - OBJECT] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - iLastSpectatedTarget != iSpectatorTarget")			
				SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_OBJECT_OBJECTIVE)
				iLastSpectatedTarget = iSpectatorTarget
				iLastPartToUse = iObjTxtPartToUse
			ENDIF
		ENDIF
		
		IF bUsingRoamingSpectatorOverride
			BOOL bUseBlank
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_EnableGameMastersMode)
			AND DID_I_JOIN_MISSION_AS_SPECTATOR()
			AND IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
				bUseBlank = TRUE
			ENDIF	
			IF NOT IS_STRING_NULL_OR_EMPTY(tl63RoamingSpectatorOverride)
			OR bUseBlank
				IF (bUseBlank AND IS_STRING_NULL_OR_EMPTY(tl63RoamingSpectatorOverride))
				OR NOT Is_This_The_Current_Objective_Text(tl63RoamingSpectatorOverride)
					Clear_Any_Objective_Text()
					PRINTLN("[LM][SPEC_SPEC] - tl63RoamingSpectatorOverride: ", tl63RoamingSpectatorOverride)
					IF NOT bUseBlank
						PRINT_CUSTOM_OBJECTIVE(tl63RoamingSpectatorOverride)
					ENDIF
					bobjectivedisp = TRUE
					bcustomobjective = TRUE
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_STRING_NULL_OR_EMPTY(tl63RoamingSpectatorOverride)
				IF Is_This_The_Current_Objective_Text(tl63RoamingSpectatorOverride)
					Clear_Any_Objective_Text()
					PRINTLN("[LM][UOBJ - VEHICLE][SPEC_SPEC] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - Clearing old Roam Spec Override")			
					SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
					PRINTLN("[LM][UOBJ - PED][SPEC_SPEC] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - Clearing old Roam Spec Override")			
					SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_PED_OBJECTIVE)
					PRINTLN("[LM][UOBJ - OBJECT][SPEC_SPEC] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - Clearing old Roam Spec Override")			
					SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_OBJECT_OBJECTIVE)
				ENDIF
			ENDIF
		ENDIF
		
		IF MC_playerBD_1[iLocalPart].iVehicleNeededToRepair != iOldMissionVehicleNeededToRepair
			PRINTLN("[RCC MISSION][ModShopProg] - iVehicleNeededToRepair changed to: ", MC_playerBD_1[iLocalPart].iVehicleNeededToRepair, " Update Objective Text")
			iOldMissionVehicleNeededToRepair = MC_playerBD_1[iLocalPart].iVehicleNeededToRepair
			
			PRINTLN("[LM][UOBJ - VEHICLE][SPEC_SPEC] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - Clearing old Roam Spec Override")			
			SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
			PRINTLN("[LM][UOBJ - PED][SPEC_SPEC] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - Clearing old Roam Spec Override")			
			SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_PED_OBJECTIVE)
			PRINTLN("[LM][UOBJ - OBJECT][SPEC_SPEC] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - Clearing old Roam Spec Override")			
			SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_OBJECT_OBJECTIVE)
		ENDIF
		
		IF iRule < FMMC_MAX_RULES
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_TEAM_VEHICLE_DROP_OFF_OBJ_TEXT)
				IF ARE_ANY_VALID_PLAYERS_DRIVING_DELIVERY_VEHICLE()
					IF NOT IS_BIT_SET(iLocalboolCheck31, LBOOL31_ARE_ANY_TEAM_PLAYERS_IN_VALID_DELIVERY_VEH)
						PRINTLN("[LM][UOBJ - OBJECT][SPEC_SPEC] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - Setting LBOOL31_ARE_ANY_TEAM_PLAYERS_IN_VALID_DELIVERY_VEH")
						SET_BIT(iLocalboolCheck31, LBOOL31_ARE_ANY_TEAM_PLAYERS_IN_VALID_DELIVERY_VEH)
						
						PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - LBOOL31_ARE_ANY_TEAM_PLAYERS_IN_VALID_DELIVERY_VEH")			
						SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
						PRINTLN("[JS][UOBJ - PED] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - LBOOL31_ARE_ANY_TEAM_PLAYERS_IN_VALID_DELIVERY_VEH")			
						SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_PED_OBJECTIVE)
						PRINTLN("[JS][UOBJ - OBJECT] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - LBOOL31_ARE_ANY_TEAM_PLAYERS_IN_VALID_DELIVERY_VEH")			
						SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_OBJECT_OBJECTIVE)
					ENDIF
				ELSE
					IF IS_BIT_SET(iLocalboolCheck31, LBOOL31_ARE_ANY_TEAM_PLAYERS_IN_VALID_DELIVERY_VEH) 
						PRINTLN("[LM][UOBJ - OBJECT][SPEC_SPEC] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - Clearing LBOOL31_ARE_ANY_TEAM_PLAYERS_IN_VALID_DELIVERY_VEH")
						CLEAR_BIT(iLocalboolCheck31, LBOOL31_ARE_ANY_TEAM_PLAYERS_IN_VALID_DELIVERY_VEH) 
						
						PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - LBOOL31_ARE_ANY_TEAM_PLAYERS_IN_VALID_DELIVERY_VEH")			
						SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
						PRINTLN("[JS][UOBJ - PED] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - LBOOL31_ARE_ANY_TEAM_PLAYERS_IN_VALID_DELIVERY_VEH")			
						SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_PED_OBJECTIVE)
						PRINTLN("[JS][UOBJ - OBJECT] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - LBOOL31_ARE_ANY_TEAM_PLAYERS_IN_VALID_DELIVERY_VEH")			
						SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_OBJECT_OBJECTIVE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF (NOT HAS_NET_TIMER_STARTED( MC_playerBD[ iObjTxtPartToUse ].tdBoundstimer)
		AND NOT HAS_NET_TIMER_STARTED( MC_playerBD[ iObjTxtPartToUse ].tdBounds2timer))		
		AND NOT bUsingRoamingSpectatorOverride
			
			IF (GET_FRAME_COUNT() % 30) = 0
				PRINTLN("PROCESS_OBJECTIVE_TEXT - Checking the current objective conditions...")
			ENDIF
			
			IF MC_playerBD[iObjTxtPartToUse].iWanted != iPrevWantedForHUD
				iPrevWantedForHUD = MC_playerBD[iObjTxtPartToUse].iWanted
				PRINTLN("[JS][UOBJ - COPS] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - iPrevWantedForHUD changed")			
				SET_BIT(iLocalBoolCheck28, LBOOL28_UPDATE_LOSE_COPS_OBJECTIVE)
			ENDIF
			
			IF CURRENT_OBJECTIVE_LOSE_COPS()
				
				IF MC_playerBD[iObjTxtPartToUse].iWanted > 0
					PRINT_NORMAL_OBJECTIVE( "MC_LOSE_COPS" )
				ELSE
					IF IS_OBJECTIVE_TEXT_UPDATE_READY(OBJ_TEXT_DELAY_BS_GROUP_0, iLocalBoolCheck28, LBOOL28_UPDATE_LOSE_COPS_OBJECTIVE, DEFAULT, OBJECTIVE_TEXT_DELAY_LONG)
						// Else if the whole team/all friendly players have to lose the wanted level
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iRuleBitset[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]], ciBS_RULE_WANTED_TEAM_OBJ )
						OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iRuleBitset[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]], ciBS_RULE_WANTED_COOP_TEAMS_OBJ )
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]], ciBS_RULE8_OVERRIDE_FRIENDLY_WITH_GANG)
								INT iR, iG, iB, iA
								GET_HUD_COLOUR(GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam,PLAYER_ID()),iR, iG, iB,iA)
								SET_SCRIPT_VARIABLE_HUD_COLOUR(iR, iG, iB,iA)
								PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT - Using gang colour lose police text. Colour: ", iR,", ", iG, ", ", iB)
								PRINT_NORMAL_OBJECTIVE("MC_HLOSE_G_COP")
							ELSE
								PRINT_NORMAL_OBJECTIVE("MC_HLOSE_COP")
							ENDIF
						ENDIF
					ENDIF
				ENDIF

				IF iSpectatorTarget = -1
					IF NOT IS_BIT_SET( MC_playerBD[ iPartToUse ].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER )
						CLEAR_BIT( g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM0_TARGET )
						CLEAR_BIT( g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM1_TARGET )
						CLEAR_BIT( g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM2_TARGET )
						CLEAR_BIT( g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM3_TARGET )
						PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT, PBBOOL_OBJECTIVE_BLOCKER")
						SET_BIT( MC_playerBD[ iPartToUse ].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER )
					ENDIF
				ENDIF
				
				INT iStage = GET_MC_CLIENT_MISSION_STAGE(iObjTxtPartToUse)
				BOOL bCleanUpBlips = TRUE
				IF (iStage = CLIENT_MISSION_STAGE_COLLECT_OBJ
				OR iStage = CLIENT_MISSION_STAGE_COLLECT_VEH
				OR iStage = CLIENT_MISSION_STAGE_COLLECT_PED)
					IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iRuleBitsetThree[MC_serverBD_4.iCurrentHighestPriority[ MC_playerBD[ iObjTxtPartToUse ].iteam ]], ciBS_RULE3_OVERRIDE_COLLECTION_TEXT)
						bCleanUpBlips = FALSE
					ENDIF
				ENDIF
				
				IF bCleanUpBlips
					CLEANUP_OBJECTIVE_BLIPS()
				ENDIF
				
				PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - CURRENT_OBJECTIVE_LOSE_COPS")			
				SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
				PRINTLN("[JS][UOBJ - PED] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - CURRENT_OBJECTIVE_LOSE_COPS")			
				SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_PED_OBJECTIVE)
				PRINTLN("[JS][UOBJ - OBJECT] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - CURRENT_OBJECTIVE_LOSE_COPS")			
				SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_OBJECT_OBJECTIVE)
				SET_BIT(iLocalBoolCheck3,LBOOL3_DISABLE_GPSANDHINTCAM)
				
			ELIF CURRENT_OBJECTIVE_WAITING_FOR_DIALOGUE_TO_FINISH()
				
				PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - CURRENT_OBJECTIVE_WAITING_FOR_DIALOGUE_TO_FINISH")			
				SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
				PRINTLN("[JS][UOBJ - PED] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - CURRENT_OBJECTIVE_WAITING_FOR_DIALOGUE_TO_FINISH")			
				SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_PED_OBJECTIVE)
				PRINTLN("[JS][UOBJ - OBJECT] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - CURRENT_OBJECTIVE_WAITING_FOR_DIALOGUE_TO_FINISH")			
				SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_OBJECT_OBJECTIVE)
				SET_BIT(iLocalBoolCheck3,LBOOL3_DISABLE_GPSANDHINTCAM)
			
			ELIF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_SHOULD_USE_CUSTOM_HELP_WAIT_TEXT_TAXI)
								
				PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT, LBOOL32_SHOULD_USE_CUSTOM_HELP_WAIT_TEXT_TAXI")
				TEXT_LABEL_63 tlWaitText
				tlWaitText = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].tl63WaitMessage[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]]
				tlWaitText = GET_CUSTOM_OBJECTIVE_TEXT_LABEL(MC_playerBD[iObjTxtPartToUse].iteam, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam], tlWaitText) 
				
				IF NOT IS_STRING_NULL_OR_EMPTY(tlWaitText)
					PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT, LBOOL32_SHOULD_USE_CUSTOM_HELP_WAIT_TEXT_TAXI - We should be printing wait message.")
					IF NOT Is_This_The_Current_Objective_Text(tlWaitText)
						PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT, LBOOL32_SHOULD_USE_CUSTOM_HELP_WAIT_TEXT_TAXI - printing wait message.")
						Clear_Any_Objective_Text()
						PRINT_CUSTOM_OBJECTIVE(tlWaitText)
					ENDIF
					bobjectivedisp = TRUE
					bcustomobjective = TRUE
				ENDIF
				
			ELIF CURRENT_OBJECTIVE_REPAIR_CAR()
				PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT, CURRENT_OBJECTIVE_REPAIR_CAR iObjTxtPartToUse: ",iObjTxtPartToUse," team: ", MC_playerBD[iObjTxtPartToUse].iteam)
				BOOL bCanDisplayRepairObjective = TRUE
				
				IF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_DELIVER_VEH
				AND NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[iObjTxtPartToUse].iteam], iRule)
					bCanDisplayRepairObjective = FALSE
					PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT, CURRENT_OBJECTIVE_REPAIR_CAR || bCanDisplayRepairObjective = FALSE because we're not properly at the midpoint yet")
				ENDIF
				
				IF bCanDisplayRepairObjective
					// This option utilizes wait text separately. We have default repair text so we can use that instead of the customwait one.
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.iVehRuleWaitForTeamsBS[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]][MC_playerBD[iObjTxtPartToUse].iTeam], ciFMMC_VehRuleTeamBS_ALL_TEAM_GOTO)
						TEXT_LABEL_63 tlWaitText
						tlWaitText = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].tl63WaitMessage[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]]
						IF NOT IS_STRING_NULL_OR_EMPTY(tlWaitText)					
							PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT, CURRENT_OBJECTIVE_REPAIR_CAR - We should be printing wait message.")
							IF NOT Is_This_The_Current_Objective_Text(tlWaitText)							
								PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT, CURRENT_OBJECTIVE_REPAIR_CAR - printing wait message.")
								Clear_Any_Objective_Text()
								PRINT_CUSTOM_OBJECTIVE(tlWaitText)	
							ENDIF
							bobjectivedisp = TRUE
							bcustomobjective = TRUE
						ENDIF
					ELSE
						PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT, CURRENT_OBJECTIVE_REPAIR_CAR - Blocking Cusatom Wait Text - Using it for ciFMMC_VehRuleTeamBS_ALL_TEAM_GOTO.")
					ENDIF
					
					IF NOT bcustomobjective
						IF (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetThirteen[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE13_BLOCK_PROGRESSION_BF_MODSHOP_RESPRAY)
						OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetThirteen[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE13_BLOCK_PROGRESSION_BF_MODSHOP_LICENSE_CHNG))
						AND IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
							TEXT_LABEL_63 tl63_ModShop = "OBJ_MODSHP_BLK"
							PRINT_NORMAL_OBJECTIVE(tl63_ModShop)
						ELIF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].tl23ObjSingular[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]])
							PRINT_OBJECTIVE_WITH_CUSTOM_STRING("OBJ_REP_C",g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].tl23ObjSingular[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]])
						ELSE
							IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[i].tl63Objective2[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]])
								PRINTLN("[JS][UOBJ - VEHICLE] - PROCESS_OBJECTIVE_TEXT - CURRENT_OBJECTIVE_REPAIR_CAR - Printing Custom Text 2.")
								TEXT_LABEL_63 tl63 = GET_CUSTOM_OBJECTIVE_TEXT_LABEL(MC_playerBD[iObjTxtPartToUse].iTeam, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam], g_FMMC_STRUCT.sFMMCEndConditions[i].tl63Objective2[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]])
								PRINT_CUSTOM_OBJECTIVE(tl63)
								bobjectivedisp = TRUE
								bcustomobjective = TRUE				
							ELSE
								PRINT_NORMAL_OBJECTIVE("OBJ_REPCR")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_ForceGpsToNearestModShop)
					IF NOT IS_PLAYER_IN_ANY_SHOP()
						BLIP_INDEX tempBlip = GET_CLOSEST_BLIP_INFO_ID(RADAR_TRACE_CAR_MOD_SHOP)
						IF DOES_BLIP_EXIST(tempBlip)
							IF biModshopBlip != tempBlip
							PRINTLN("[ML] changing nearest mod shop blip handle")
								CLEANUP_MODSHOP_BLIP_SETTINGS()
								biModshopBlip = tempBlip
							ENDIF						
						
							IF NOT DOES_BLIP_HAVE_GPS_ROUTE(biModshopBlip)
								SET_BLIP_ROUTE(biModshopBlip, TRUE)
								SET_BLIP_ROUTE_COLOUR(biModshopBlip, BLIP_COLOUR_YELLOW)
								SET_BLIP_COLOUR(biModshopBlip, BLIP_COLOUR_YELLOW)								
								SET_BLIP_AS_SHORT_RANGE(biModshopBlip, FALSE)
							ENDIF
						ELSE // Blip doesn't exist
							CLEANUP_MODSHOP_BLIP_SETTINGS()
						ENDIF
					ELSE // Player is not in a shop					
						CLEANUP_MODSHOP_BLIP_SETTINGS()
					ENDIF
				ENDIF
				
				CLEANUP_OBJECTIVE_BLIPS()
				
				IF iSpectatorTarget = -1
					IF NOT IS_BIT_SET( MC_playerBD[ iPartToUse ].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER )
						CLEAR_BIT( g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM0_TARGET )
						CLEAR_BIT( g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM1_TARGET )
						CLEAR_BIT( g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM2_TARGET )
						CLEAR_BIT( g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM3_TARGET )
						PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT, PBBOOL_OBJECTIVE_BLOCKER")
						SET_BIT( MC_playerBD[ iPartToUse ].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER )
					ENDIF
				ENDIF
						
				CLEAR_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)		
				CLEAR_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_PED_OBJECTIVE)	
				CLEAR_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_OBJECT_OBJECTIVE)
				SET_BIT(iLocalBoolCheck3,LBOOL3_DISABLE_GPSANDHINTCAM)
				
			ELIF SHOULD_SHOW_GAIN_WANTED_LEVEL_OBJECTIVE_TEXT()
				
				INT iGainWantedLevelRequirement = GET_GAIN_WANTED_LEVEL()
				PRINTLN("[MJL] Wanted level = ", iGainWantedLevelRequirement)
				TEXT_LABEL_15 tl15 = "MC_GAIN_WANT_"
				tl15 += iGainWantedLevelRequirement
				PRINT_NORMAL_OBJECTIVE(tl15)		
			
			ELIF CURRENT_OBJECTIVE_LOSE_GANG_BACKUP()
				STRING sLoseBackupObjText = GET_LOSE_GANG_BACKUP_OBJECTIVE_TEXT()
				PRINT_NORMAL_OBJECTIVE( sLoseBackupObjText)
				IF iSpectatorTarget = -1
					IF NOT IS_BIT_SET( MC_playerBD[ iPartToUse ].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER )
						CLEAR_BIT( g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM0_TARGET )
						CLEAR_BIT( g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM1_TARGET )
						CLEAR_BIT( g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM2_TARGET )
						CLEAR_BIT( g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM3_TARGET )
						PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT, PBBOOL_OBJECTIVE_BLOCKER - CURRENT_OBJECTIVE_LOSE_GANG_BACKUP")
						SET_BIT( MC_playerBD[ iPartToUse ].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER )
					ENDIF
				ENDIF
				
				CLEANUP_OBJECTIVE_BLIPS()
				
				PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - CURRENT_OBJECTIVE_LOSE_GANG_BACKUP")			
				SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
				PRINTLN("[JS][UOBJ - PED] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - CURRENT_OBJECTIVE_LOSE_GANG_BACKUP")			
				SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_PED_OBJECTIVE)
				PRINTLN("[JS][UOBJ - OBJECT] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - CURRENT_OBJECTIVE_LOSE_GANG_BACKUP")			
				SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_OBJECT_OBJECTIVE)
				SET_BIT(iLocalBoolCheck3,LBOOL3_DISABLE_GPSANDHINTCAM)
			
			ELIF (IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_POWER_PLAY) AND IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH))
			AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_RESURRECTION(g_FMMC_STRUCT.iAdversaryModeType)
				PRINT_NORMAL_OBJECTIVE("MC_K_FKWW")
				
				PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - SBBOOL3_SUDDEN_DEATH_POWER_PLAY")			
				SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
				PRINTLN("[JS][UOBJ - PED] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - SBBOOL3_SUDDEN_DEATH_POWER_PLAY")			
				SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_PED_OBJECTIVE)
				PRINTLN("[JS][UOBJ - OBJECT] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - SBBOOL3_SUDDEN_DEATH_POWER_PLAY")			
				SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_OBJECT_OBJECTIVE)
				SET_BIT(iLocalBoolCheck3,LBOOL3_DISABLE_GPSANDHINTCAM)
				
			ELIF (IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_SUDDEN_DEATH_KILLALLENEMIES) AND IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH))

				PRINT_NORMAL_OBJECTIVE("MC_K_ALLTNC") //no colour variant - B* 2605701
				
				IF ANIMPOSTFX_IS_RUNNING("CrossLine")
					PRINTLN("[JS] - CrossLine - Turning off red filter FX")
					ANIMPOSTFX_STOP("CrossLine")
					ANIMPOSTFX_PLAY("CrossLineOut", 0, FALSE)
				ENDIF
				
				//CLEANUP_OBJECTIVE_BLIPS()
				
				PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - SBBOOL2_SUDDEN_DEATH_KILLALLENEMIES")			
				SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
				PRINTLN("[JS][UOBJ - PED] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - SBBOOL2_SUDDEN_DEATH_KILLALLENEMIES")			
				SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_PED_OBJECTIVE)
				PRINTLN("[JS][UOBJ - OBJECT] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - SBBOOL2_SUDDEN_DEATH_KILLALLENEMIES")			
				SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_OBJECT_OBJECTIVE)
				SET_BIT(iLocalBoolCheck3,LBOOL3_DISABLE_GPSANDHINTCAM)			
				
			ELIF (IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN) AND IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH))
			
				//IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
					//get and stay inside the area text
					
					HUD_COLOURS colourToUse = HUD_COLOUR_YELLOW
					IF iRule < FMMC_MAX_RULES
						IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].hudColouring != HUD_COLOUR_YELLOW
							colourToUse = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].hudColouring
						ENDIF
					ENDIF
					
					IF IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iSuddenDeathBS, ciPBDBOOL_SSDS_IS_INSIDE_SPHERE)
						IF MC_playerBD[iObjTxtPartToUse].eSumoSuddenDeathStage = eSSDS_INSIDE_SPHERE
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciSUDDEN_DEATH_THAT_ENDS_WHEN_ONLY_ONE_TEAM_ALIVE)
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciPRON_MODE_ENABLED)
									PRINT_NORMAL_OBJECTIVE("MC_K_ALLTNC")
								ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_WARFARE(g_FMMC_STRUCT.iAdversaryModeType)								
								OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRSHOOTOUT(g_FMMC_STRUCT.iAdversaryModeType)
								OR USING_SHOWDOWN_POINTS()
									PRINT_NORMAL_OBJECTIVE("VW_KILL_ALL")
								ELIF colourToUse != HUD_COLOUR_YELLOW
									PRINT_COLOURED_OBJECTIVE("SUMOR_FTO", colourToUse)
								ELSE
									PRINT_NORMAL_OBJECTIVE("SUMO_FTO")
								ENDIF
							ELSE
								PRINT_NORMAL_OBJECTIVE("SUMO_SIA") 
							ENDIF
						ENDIF
					ELSE
						IF MC_playerBD[iObjTxtPartToUse].eSumoSuddenDeathStage = eSSDS_TIMER_GET_INSIDE_SPHERE
						AND GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
							IF colourToUse != HUD_COLOUR_YELLOW
								PRINT_COLOURED_OBJECTIVE("SUMOR_GIA", colourToUse)
							ELSE
								PRINT_NORMAL_OBJECTIVE("SUMO_GIA")
							ENDIF
						ENDIF
					ENDIF

					PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN")				
					SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
					PRINTLN("[JS][UOBJ - PED] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN")				
					SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_PED_OBJECTIVE)
					PRINTLN("[JS][UOBJ - OBJECT] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN")				
					SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_OBJECT_OBJECTIVE)
					SET_BIT(iLocalBoolCheck3,LBOOL3_DISABLE_GPSANDHINTCAM) 
				//ENDIF
			ELIF (IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN) AND IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH))
				
				PRINT_NORMAL_OBJECTIVE("MC_K_ALLTNC")
				
				PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - SBBOOL3_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN")			
				SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
				PRINTLN("[JS][UOBJ - PED] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - SBBOOL3_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN")			
				SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_PED_OBJECTIVE)
				PRINTLN("[JS][UOBJ - OBJECT] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - SBBOOL3_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN")			
				SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_OBJECT_OBJECTIVE)
				SET_BIT(iLocalBoolCheck3,LBOOL3_DISABLE_GPSANDHINTCAM) 
				
			ELIF (IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_BE_MY_VALENTINES) AND IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH))

				PRINT_NORMAL_OBJECTIVE("MC_K_ALLTNC") //no colour variant - B* 2605701
				
				//CLEANUP_OBJECTIVE_BLIPS()
				PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - SBBOOL3_SUDDEN_DEATH_BE_MY_VALENTINES")			
				SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
				PRINTLN("[JS][UOBJ - PED] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - SBBOOL3_SUDDEN_DEATH_BE_MY_VALENTINES")			
				SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_PED_OBJECTIVE)
				PRINTLN("[JS][UOBJ - OBJECT] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - SBBOOL3_SUDDEN_DEATH_BE_MY_VALENTINES")			
				SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_OBJECT_OBJECTIVE)
				SET_BIT(iLocalBoolCheck3,LBOOL3_DISABLE_GPSANDHINTCAM)
			ELIF (IS_BIT_SET(MC_serverBD.iServerBitSet4, SBBOOL4_SUDDEN_DEATH_JUGGERNAUT)
			OR (IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_POWER_PLAY) 
			AND (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciTRANSFORM_TO_JUGGERNAUT_ON_SUDDEN_DEATH)
			OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciTRANSFORM_TO_BEAST_ON_SUDDEN_DEATH)))
			OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciHARD_TARGET_ENABLE_MODE))
			AND IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
			
				PRINT_NORMAL_OBJECTIVE("MC_K_NKW")
				
				//CLEANUP_OBJECTIVE_BLIPS()
				PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - SBBOOL4_SUDDEN_DEATH_JUGGERNAUT")			
				SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
				PRINTLN("[JS][UOBJ - PED] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - SBBOOL4_SUDDEN_DEATH_JUGGERNAUT")			
				SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_PED_OBJECTIVE)
				PRINTLN("[JS][UOBJ - OBJECT] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - SBBOOL4_SUDDEN_DEATH_JUGGERNAUT")			
				SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_OBJECT_OBJECTIVE)
				SET_BIT(iLocalBoolCheck3,LBOOL3_DISABLE_GPSANDHINTCAM)
			ELIF (IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_TARGET) AND IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH))

				PRINT_NORMAL_OBJECTIVE("MC_K_SUDRGT") //no colour variant - B* 2605701
				
				PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - SBBOOL3_SUDDEN_DEATH_TARGET")			
				SET_BIT(iLocalBoolCheck3, LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
				PRINTLN("[JS][UOBJ - PED] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - SBBOOL3_SUDDEN_DEATH_TARGET")			
				SET_BIT(iLocalBoolCheck3, LBOOL3_UPDATE_PED_OBJECTIVE)
				PRINTLN("[JS][UOBJ - OBJECT] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - SBBOOL3_SUDDEN_DEATH_TARGET")			
				SET_BIT(iLocalBoolCheck3, LBOOL3_UPDATE_OBJECT_OBJECTIVE)
				SET_BIT(iLocalBoolCheck3, LBOOL3_DISABLE_GPSANDHINTCAM)
			ELIF (IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_LOST_AND_DAMNED) AND IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH))
				
				IF MC_playerBD[iObjTxtPartToUse].iteam = 0
					PRINT_NORMAL_OBJECTIVE("MC_TLAD_SDD") //no colour variant - B* 2605701
				ELIF MC_playerBD[iObjTxtPartToUse].iteam = 1
					PRINT_NORMAL_OBJECTIVE("MC_TLAD_SDA")
				ENDIF
				
				PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - SBBOOL3_SUDDEN_DEATH_LOST_AND_DAMNED")			
				SET_BIT(iLocalBoolCheck3, LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
				PRINTLN("[JS][UOBJ - PED] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - SBBOOL3_SUDDEN_DEATH_LOST_AND_DAMNED")			
				SET_BIT(iLocalBoolCheck3, LBOOL3_UPDATE_PED_OBJECTIVE)
				PRINTLN("[JS][UOBJ - OBJECT] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - SBBOOL3_SUDDEN_DEATH_LOST_AND_DAMNED")			
				SET_BIT(iLocalBoolCheck3, LBOOL3_UPDATE_OBJECT_OBJECTIVE)
				SET_BIT(iLocalBoolCheck3, LBOOL3_DISABLE_GPSANDHINTCAM)
			ELIF (IS_BIT_SET(MC_serverBD.iServerBitSet6, SBBOOL6_SUDDEN_DEATH_STOCKPILE) AND IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH))
				
				IF ARE_ANY_PORTABLE_PICKUPS_CURRENTLY_HELD()
				OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciNEXT_DELIVERY_WINS_SD_ONLY)
					PRINT_NORMAL_OBJECTIVE("STKP_SD_1")
				ELIF
					PRINT_NORMAL_OBJECTIVE("SUDDTH_KILL")
				ENDIF
				
				PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - SBBOOL6_SUDDEN_DEATH_STOCKPILE")			
				SET_BIT(iLocalBoolCheck3, LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
				PRINTLN("[JS][UOBJ - PED] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - SBBOOL6_SUDDEN_DEATH_STOCKPILE")			
				SET_BIT(iLocalBoolCheck3, LBOOL3_UPDATE_PED_OBJECTIVE)
				PRINTLN("[JS][UOBJ - OBJECT] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - SBBOOL6_SUDDEN_DEATH_STOCKPILE")			
				SET_BIT(iLocalBoolCheck3, LBOOL3_UPDATE_OBJECT_OBJECTIVE)
				SET_BIT(iLocalBoolCheck3, LBOOL3_DISABLE_GPSANDHINTCAM)
				
				IF iRule < FMMC_MAX_RULES
					FOR iteam = 0  TO (FMMC_MAX_TEAMS-1)
						IF GET_PLAYER_CARRY_COUNT() > 0
							ADD_DELIVERY_BLIP()
						ELSE
							REMOVE_DELIVERY_BLIP()
						ENDIF
					ENDFOR
				ENDIF
			ELIF (IS_BIT_SET(MC_serverBD.iServerBitSet7, SBBOOL7_IN_BMBFB_SUDDEN_DEATH))
				PRINT_NORMAL_OBJECTIVE("FMMC_BMB_SD_OB")
				
			ELIF HAS_NET_TIMER_STARTED(MC_serverBD.tdCopSpottedFailTimer)
				
				IF iSpectatorTarget = -1
					IF NOT IS_BIT_SET( MC_playerBD[ iPartToUse ].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER )
						CLEAR_BIT( g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM0_TARGET )
						CLEAR_BIT( g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM1_TARGET )
						CLEAR_BIT( g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM2_TARGET )
						CLEAR_BIT( g_FMMC_STRUCT.iTargetTeamBlipBitset, TEAM3_TARGET )
						PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT, PBBOOL_OBJECTIVE_BLOCKER - tdCopSpottedFailTimer has started")
						SET_BIT( MC_playerBD[ iPartToUse ].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER )
					ENDIF
				ENDIF
				
				CLEANUP_OBJECTIVE_BLIPS()
				
				PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - tdCopSpottedFailTimer")			
				SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
				PRINTLN("[JS][UOBJ - PED] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - tdCopSpottedFailTimer")			
				SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_PED_OBJECTIVE)
				PRINTLN("[JS][UOBJ - OBJECT] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - tdCopSpottedFailTimer")			
				SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_OBJECT_OBJECTIVE)
				SET_BIT(iLocalBoolCheck3,LBOOL3_DISABLE_GPSANDHINTCAM)
				
				IF Has_Any_MP_Objective_Text_Been_Received()
					Clear_Any_Objective_Text()
				ENDIF
				
				PRINTLN("[RCC MISSION] [MB] Clear_Any_Objective_Text() 5 - tdCopSpottedFailTimer is running")
				
			ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(g_FMMC_STRUCT.iAdversaryModeType)
			AND NOT IS_BIT_SET(iObjectiveTextDelayBitset, ciObjectiveTextDelayDawnRaidBit)
			AND MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam] > 0
			AND MC_serverBD_4.iObjMissionLogic[MC_playerBD[iObjTxtPartToUse].iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
				IF NOT HAS_NET_TIMER_STARTED(objectiveTextDelay)
					START_NET_TIMER(objectiveTextDelay)
					
					PRINTLN("[PROCESS_OBJECTIVE_TEXT] Dawn Raid objective text delay ... Timer started!")
				ELSE
					IF Has_Any_MP_Objective_Text_Been_Received()
						Clear_Any_Objective_Text()
					ENDIF
					
					IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(objectiveTextDelay) > ciObjectiveTextDelayDuration
						SET_BIT(iObjectiveTextDelayBitset, ciObjectiveTextDelayDawnRaidBit)
						
						PRINTLN("[PROCESS_OBJECTIVE_TEXT] Dawn Raid objective text delay ... Timer ended!")
					ENDIF
				ENDIF
			ELIF NOT IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet,PBBOOL_OBJECTIVE_BLOCKER)
				
				CLEAR_BIT(iLocalBoolCheck3,LBOOL3_DISABLE_GPSANDHINTCAM)
				
				INT iclientstage = GET_MC_CLIENT_MISSION_STAGE(iObjTxtPartToUse)
				
				IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iObjTxtPartToUse].iteam)
					PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - New Priority")				
					SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
					PRINTLN("[JS][UOBJ - PED] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - New Priority")				
					SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_PED_OBJECTIVE)
					PRINTLN("[JS][UOBJ - OBJECT] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - New Priority")				
					SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_OBJECT_OBJECTIVE)
//					IF NOT bIsLocalPlayerHost
						bDelayedClientCheck = TRUE
//					ENDIF
				ELSE
					//do clientlogic check a frame late, host will always have this correct but remotes do this earlier
					IF bDelayedClientCheck OR bHasTripSkipJustFinished
						PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - bDelayedClientCheck OR bHasTripSkipJustFinished")					
						SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
						PRINTLN("[JS][UOBJ - PED] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - bDelayedClientCheck OR bHasTripSkipJustFinished")					
						SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_PED_OBJECTIVE)
						PRINTLN("[JS][UOBJ - OBJECT] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - bDelayedClientCheck OR bHasTripSkipJustFinished")					
						SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_OBJECT_OBJECTIVE)
						bDelayedClientCheck = FALSE
						bHasTripSkipJustFinished = FALSE
					ENDIF
				ENDIF
				
				#IF IS_DEBUG_BUILD
				IF IS_BIT_SET(iLocalDebugBS, LDEBUGBS_TEAM_CHANGED_WAIT_FOR_OBJTXT)
					IF Is_MP_Objective_Text_On_Display()
						CLEAR_BIT(iLocalDebugBS, LDEBUGBS_TEAM_CHANGED_WAIT_FOR_OBJTXT)
					ELSE
						PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - LDEBUGBS_TEAM_CHANGED_WAIT_FOR_OBJTXT")					
						SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
						PRINTLN("[JS][UOBJ - PED] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - LDEBUGBS_TEAM_CHANGED_WAIT_FOR_OBJTXT")					
						SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_PED_OBJECTIVE)
						PRINTLN("[JS][UOBJ - OBJECT] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - LDEBUGBS_TEAM_CHANGED_WAIT_FOR_OBJTXT")					
						SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_OBJECT_OBJECTIVE)
					ENDIF
				ENDIF
				#ENDIF
				
				IF MC_playerBD[iObjTxtPartToUse].iObjectiveTypeCompleted = -1
					
					IF inumfreevehicles != MC_serverBD.iNumVehHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam]
						PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - inumfreevehicles changed")					
						SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
						inumfreevehicles = MC_serverBD.iNumVehHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam]
					ENDIF
					
					IF inumfreeseats != GET_NUMBER_OF_FREE_SEATS_IN_HIGH_PRIORITY_VEHICLES_FOR_TEAM(MC_playerBD[iObjTxtPartToUse].iteam)
						PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - inumfreeseats changed")					
						SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
						inumfreeseats = GET_NUMBER_OF_FREE_SEATS_IN_HIGH_PRIORITY_VEHICLES_FOR_TEAM(MC_playerBD[iObjTxtPartToUse].iteam)
					ENDIF
					
					BOOL bBlipChange = FALSE
					
					IF iOldClientStageObjText != iclientstage						
						IF iOldClientStageObjText = CLIENT_MISSION_STAGE_DELIVER_VEH
						AND iclientstage = CLIENT_MISSION_STAGE_COLLECT_VEH
							//If the car we're leaving isn't all team:
							BOOL bCheckOnRule // To let us use GET_VEHICLE_GOTO_TEAMS here
							IF (iVehJustIn != -1)
							AND (GET_VEHICLE_GOTO_TEAMS(MC_playerBD[iObjTxtPartToUse].iteam,iVehJustIn,bCheckOnRule) = 0)
								PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - new iVehJustIn")							
								SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
								bBlipChange = TRUE
							ENDIF
							iVehJustIn = -1
							iOldClientStageObjText = iclientstage
						ELSE
							IF iOldClientStageObjText = CLIENT_MISSION_STAGE_COLLECT_VEH
							AND iclientstage = CLIENT_MISSION_STAGE_DELIVER_VEH
								iVehJustIn = MC_playerBD[iObjTxtPartToUse].iVehNear
								bBlipChange = TRUE
							ELSE
								iVehJustIn = -1
							ENDIF
							iOldClientStageObjText = iclientstage
						ENDIF
					ENDIF
					
					IF iclientstage = CLIENT_MISSION_STAGE_DELIVER_VEH
					OR iclientstage = CLIENT_MISSION_STAGE_COLLECT_VEH
					OR iclientstage = CLIENT_MISSION_STAGE_GOTO_VEH
						IF IS_BIT_SET(iLocalBoolCheck24, LBOOL24_IN_CREATOR_TRAILER) != IS_BIT_SET(iLocalBoolCheck27, LBOOL27_IN_CREATOR_TRAILER_OLD_VALUE)
							IF IS_BIT_SET(iLocalBoolCheck24, LBOOL24_IN_CREATOR_TRAILER)
								SET_BIT(iLocalBoolCheck27, LBOOL27_IN_CREATOR_TRAILER_OLD_VALUE)
								PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - Entered Veh Interior")			
								SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
							ELSE
								CLEAR_BIT(iLocalBoolCheck27, LBOOL27_IN_CREATOR_TRAILER_OLD_VALUE)
								PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - Left Veh Interior")
								SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
							ENDIF
						ENDIF
					ENDIF
					
					IF iclientstage = CLIENT_MISSION_STAGE_CONTROL_AREA
						IF MC_playerBD[iObjTxtPartToUse].iCurrentLoc != iPrevCurrentLoc
							iPrevCurrentLoc = MC_playerBD[iPartToUse].iCurrentLoc
							IF NOT IS_BIT_SET(iLocalBoolCheck2,LBOOL2_REQUEST_UPDATE)
								CLEAR_BIT(iLocalBoolCheck2,LBOOL2_WHOLE_UPDATE_DONE)
								SET_BIT(iLocalBoolCheck2,LBOOL2_REQUEST_UPDATE)
								PRINTLN("[JS] requesting update as iCurrentLoc changed")
							ENDIF
						ENDIF
					ENDIF
					
					IF iclientstage = CLIENT_MISSION_STAGE_CONTROL_VEH
					AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING)
						IF iOldVehDestroyBS != MC_PlayerBD[iObjTxtPartToUse].iVehDestroyBitset
							PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - iVehDestroyBitset changed")			
							SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
							iOldVehDestroyBS = MC_PlayerBD[iObjTxtPartToUse].iVehDestroyBitset
						ENDIF
						IF iOldHackTargetsRemaining != MC_serverBD_4.iHackingTargetsRemaining 
							PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - iHackingTargetsRemaining changed")			
							SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
							iOldHackTargetsRemaining = MC_serverBD_4.iHackingTargetsRemaining 
						ENDIF
						IF iOldVehFollowing != MC_playerBD[iObjTxtPartToUse].iVehFollowing
							PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - iVehFollowing changed")			
							SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
							iOldVehFollowing = MC_playerBD[iObjTxtPartToUse].iVehFollowing
						ENDIF
						IF iOldHackingVehInRange != iHackingVehInRangeBitSet
							PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - iHackingVehInRangeBitSet changed")			
							SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
							iOldHackingVehInRange = iHackingVehInRangeBitSet
						ENDIF
					ENDIF
					IF iOldVeh != MC_playerBD[iObjTxtPartToUse].iVehNear
						#IF IS_DEBUG_BUILD
						IF bDebugPrintCustomText
							PRINTLN("[JS] iOldVeh = ", iOldVeh)
							PRINTLN("[JS] MC_playerBD[iObjTxtPartToUse].iVehNear = ", MC_playerBD[iObjTxtPartToUse].iVehNear)
						ENDIF
						#ENDIF
						PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - New VehNear")							
						SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
						iOldVeh = MC_playerBD[iObjTxtPartToUse].iVehNear
					ENDIF
					IF inumfreePeds != MC_serverBD.iNumPedHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam]
						PRINTLN("[JS][UOBJ - PED] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - inumfreePeds changed")					
						SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_PED_OBJECTIVE)
						inumfreePeds = MC_serverBD.iNumPedHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam]
					ENDIF
					
					IF inumfreeObjects != MC_serverBD.iNumObjHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam]
						PRINTLN("[JS][UOBJ - OBJECT] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - inumfreeObjects changed")					
						SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_OBJECT_OBJECTIVE)
						inumfreeObjects = MC_serverBD.iNumObjHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam]
					ENDIF
					
					IF IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER)
						IF NOT IS_BIT_SET(ilocalboolcheck,LBOOL_WITH_CARRIER_FLAG)
							PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - PBBOOL_WITH_CARRIER")						
							SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
							PRINTLN("[JS][UOBJ - PED] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - PBBOOL_WITH_CARRIER")						
							SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_PED_OBJECTIVE)
							PRINTLN("[JS][UOBJ - OBJECT] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - PBBOOL_WITH_CARRIER")						
							SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_OBJECT_OBJECTIVE)
							SET_BIT(ilocalboolcheck,LBOOL_WITH_CARRIER_FLAG)
						ENDIF
					ELSE
						IF IS_BIT_SET(ilocalboolcheck,LBOOL_WITH_CARRIER_FLAG)
							PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - LBOOL_WITH_CARRIER_FLAG")							
							SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
							PRINTLN("[JS][UOBJ - PED] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - LBOOL_WITH_CARRIER_FLAG")						
							SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_PED_OBJECTIVE)
							PRINTLN("[JS][UOBJ - OBJECT] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - LBOOL_WITH_CARRIER_FLAG")						
							SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_OBJECT_OBJECTIVE)
							CLEAR_BIT(ilocalboolcheck,LBOOL_WITH_CARRIER_FLAG)
						ENDIF
					ENDIF
					
					IF IS_BIT_SET(MC_playerBD[ iObjTxtPartToUse ].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
					OR IS_BIT_SET(MC_playerBD[ iObjTxtPartToUse ].iClientBitSet2, PBBOOL2_DROPPING_OFF)
					OR iDeliveryVehForcingOut != -1
						IF NOT IS_BIT_SET(ilocalboolcheck5,LBOOL5_WAS_IN_DROP_OFF)
							PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - LBOOL5_WAS_IN_DROP_OFF")						
							SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
							PRINTLN("[JS][UOBJ - PED] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - LBOOL5_WAS_IN_DROP_OFF")						
							SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_PED_OBJECTIVE)
							PRINTLN("[JS][UOBJ - OBJECT] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - LBOOL5_WAS_IN_DROP_OFF")						
							SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_OBJECT_OBJECTIVE)
							SET_BIT(ilocalboolcheck5,LBOOL5_WAS_IN_DROP_OFF)
						ENDIF
					ELSE
						IF IS_BIT_SET(ilocalboolcheck5,LBOOL5_WAS_IN_DROP_OFF)
							PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - LBOOL5_WAS_IN_DROP_OFF 2")						
							SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
							PRINTLN("[JS][UOBJ - PED] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - LBOOL5_WAS_IN_DROP_OFF 2")						
							SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_PED_OBJECTIVE)
							PRINTLN("[JS][UOBJ - OBJECT] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - LBOOL5_WAS_IN_DROP_OFF 2")						
							SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_OBJECT_OBJECTIVE)
							CLEAR_BIT(ilocalboolcheck5,LBOOL5_WAS_IN_DROP_OFF)
						ENDIF
					ENDIF
					
					IF NOT Is_MP_Objective_Text_On_Display()
						PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - NOT Is_MP_Objective_Text_On_Display")						
						SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
						PRINTLN("[JS][UOBJ - PED] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - NOT Is_MP_Objective_Text_On_Display")						
						SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_PED_OBJECTIVE)
						PRINTLN("[JS][UOBJ - OBJECT] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - NOT Is_MP_Objective_Text_On_Display")						
						SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_OBJECT_OBJECTIVE)
					ENDIF
					
					//IF iSpectatorTarget = -1
						CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM0_TARGET)
						CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM1_TARGET)
						CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM2_TARGET)
						CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM3_TARGET)
						
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciUSE_MP_BLIPPING_FOR_KILL_PLAYER_RULE)
							IF iclientstage = CLIENT_MISSION_STAGE_KILL_PLAYERS
							OR iclientstage = CLIENT_MISSION_STAGE_ARREST_ALL
								IF MC_playerBD[iObjTxtPartToUse].iteam != 0
								AND NOT DOES_TEAM_LIKE_TEAM(MC_playerBD[iObjTxtPartToUse].iteam,0)
									SET_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM0_TARGET)
								ENDIF
								IF MC_playerBD[iObjTxtPartToUse].iteam != 1
								AND NOT DOES_TEAM_LIKE_TEAM(MC_playerBD[iObjTxtPartToUse].iteam,1)
									SET_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM1_TARGET)
								ENDIF
								IF MC_playerBD[iObjTxtPartToUse].iteam != 2
								AND NOT DOES_TEAM_LIKE_TEAM(MC_playerBD[iObjTxtPartToUse].iteam,2)
									SET_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM2_TARGET)
								ENDIF
								IF MC_playerBD[iObjTxtPartToUse].iteam != 3
								AND NOT DOES_TEAM_LIKE_TEAM(MC_playerBD[iObjTxtPartToUse].iteam,3)
									SET_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM3_TARGET)
								ENDIF
							ELIF iclientstage = CLIENT_MISSION_STAGE_KILL_TEAM0
							OR iclientstage = CLIENT_MISSION_STAGE_ARREST_TEAM0
								SET_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM0_TARGET)
							ELIF iclientstage = CLIENT_MISSION_STAGE_KILL_TEAM1
							OR iclientstage = CLIENT_MISSION_STAGE_ARREST_TEAM1
								SET_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM1_TARGET)
							ELIF iclientstage = CLIENT_MISSION_STAGE_KILL_TEAM2
							OR iclientstage = CLIENT_MISSION_STAGE_ARREST_TEAM2
								SET_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM2_TARGET)
							ELIF iclientstage = CLIENT_MISSION_STAGE_KILL_TEAM3
							OR iclientstage = CLIENT_MISSION_STAGE_ARREST_TEAM3
								SET_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM3_TARGET)
							ENDIF
						ENDIF
					//ENDIF
					
					
					IF iRule < FMMC_MAX_RULES
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iRuleBitsetSix[iRule], ciBS_RULE6_BLIP_ON_SPEED_ENABLED)
							PROCESS_PLAYER_CURRENT_SPEED()
							PROCESS_BLIP_ON_VEHICLE_SPEED()
						ENDIF
					ENDIF
					
					
					// this is where the server will update its data based upon the player broadcast data MC_playerBD[iPartToUse].iObjectiveTypeCompleted
					
					IF Is_Player_Currently_On_MP_CTF_Mission(PlayerToUse)
						IF DOES_BLIP_EXIST(HomeBlip[MC_playerBD[iPartToUse].iteam])
							IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER)
							OR iclientstage = CLIENT_MISSION_STAGE_DELIVER_OBJ
							OR iclientstage = CLIENT_MISSION_STAGE_DELIVER_VEH
							OR iclientstage = CLIENT_MISSION_STAGE_DELIVER_PED
								IF NOT IS_BLIP_FLASHING(HomeBlip[MC_playerBD[iPartToUse].iteam])
									
									SET_BLIP_FLASHES(HomeBlip[MC_playerBD[iPartToUse].iteam],TRUE)
									IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
										IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iDropOffGPSBitSet, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
											SET_BLIP_ROUTE(HomeBlip[MC_playerBD[iPartToUse].iteam],TRUE)
											SET_BLIP_ROUTE_COLOUR(HomeBlip[MC_playerBD[iPartToUse].iteam], GET_BLIP_COLOUR_FROM_HUD_COLOUR(GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_playerBD[iPartToUse].iteam, PlayerToUse)))
										ENDIF
									ENDIF	
								ENDIF
							ELSE
								IF IS_BLIP_FLASHING(HomeBlip[MC_playerBD[iPartToUse].iteam])
									SET_BLIP_FLASHES(HomeBlip[MC_playerBD[iPartToUse].iteam],FALSE)
									SET_BLIP_ROUTE(HomeBlip[MC_playerBD[iPartToUse].iteam],FALSE)
								ENDIF
							ENDIF
						ENDIF
						FOR iteam = 0  TO (FMMC_MAX_TEAMS-1)
							IF NOT DOES_BLIP_EXIST(HomeBlip[iteam])
								IF NOT IS_VECTOR_ZERO(GET_DROP_OFF_CENTER_FOR_TEAM(FALSE, iteam, iPartToUse))
									HomeBlip[iteam] = ADD_BLIP_FOR_COORD(GET_DROP_OFF_CENTER_FOR_TEAM(FALSE, iteam, iPartToUse))
									SET_BLIP_SPRITE(HomeBlip[iteam],RADAR_TRACE_CAPTURE_THE_FLAG_BASE)
									SET_BLIP_COLOUR(HomeBlip[iteam],GET_BLIP_COLOUR_FROM_HUD_COLOUR(GET_HUD_COLOUR_FOR_FMMC_TEAM(iteam, PlayerToUse)))
									//SET_BLIP_TEAM_SECONDARY_COLOUR(HomeBlip[iteam],TRUE,iteam)
									SET_BLIP_NAME_FROM_TEXT_FILE(HomeBlip[iteam],"BASEBLIP")
									IF iteam != MC_playerBD[iPartToUse].iteam
										SET_BLIP_PRIORITY(HomeBlip[iteam],BLIPPRIORITY_HIGH_HIGHEST)
									ELSE
										SET_BLIP_PRIORITY(HomeBlip[iteam],BLIP_PRIORITY_HIGHEST_SPECIAL_LOW)
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
					ENDIF

					IF iRule < FMMC_MAX_RULES
						FOR iteam = 0  TO (FMMC_MAX_TEAMS-1)
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iRuleBitsetFour[iRule] ,ciBS_RULE4_ALWAYS_BLIP_DELIVERY_POINT)
								ADD_DELIVERY_BLIP(bBlipChange)
							ENDIF
						ENDFOR
					ENDIF
					
					INT iMyTeam = MC_playerBD[iObjTxtPartToUse].iteam
					
					IF iclientstage != CLIENT_MISSION_STAGE_DELIVER_OBJ
					AND iclientstage != CLIENT_MISSION_STAGE_DELIVER_VEH
					AND iclientstage != CLIENT_MISSION_STAGE_DELIVER_PED
					AND MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] < FMMC_MAX_RULES
											
						IF iclientstage = CLIENT_MISSION_STAGE_COLLECT_PED
							IF MC_serverBD.iNumPedHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] < MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] 
								REMOVE_DELIVERY_BLIP()
							ELSE
								IF IS_BIT_SET(iLocalBoolCheck3,LBOOL3_UPDATE_PED_OBJECTIVE)
								OR NOT DOES_BLIP_EXIST(DeliveryBlip)
									ADD_DELIVERY_BLIP()
								ENDIF
							ENDIF
						ELIF iclientstage = CLIENT_MISSION_STAGE_COLLECT_VEH
							BOOL bAvengerDeliveryBlip
							INT iAvengerDriverBlip
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen,ciENABLE_AVENGER_INTERIOR_ENTRY)
								bAvengerDeliveryBlip = IS_ANY_PLAYER_IN_AVENGER_PILOT_SEAT(iAvengerDriverBlip)
							ENDIF

							IF (MC_serverBD.iNumVehHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] < MC_serverBD.iNumvehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]
							OR AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH()
							OR (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]], ciBS_RULE9_WAIT_FOR_REMAINING_IN_MOC_TO_PROGRESS) AND NOT ARE_REMAINING_PLAYERS_IN_VEHICLE_INTERIOR()))
							AND NOT ((MC_serverBD.iNumVehHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] >= MC_serverBD.iNumvehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]) AND IS_BIT_SET(iLocalBoolCheck24, LBOOL24_IN_CREATOR_TRAILER) AND bAvengerDeliveryBlip)
								REMOVE_DELIVERY_BLIP()
							ELSE
								IF IS_BIT_SET(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
								OR NOT DOES_BLIP_EXIST(DeliveryBlip)
									ADD_DELIVERY_BLIP(bBlipChange)
								ENDIF
							ENDIF
							
						ELIF iclientstage = CLIENT_MISSION_STAGE_COLLECT_OBJ
							
						
							IF MC_serverBD.iNumObjHighestPriorityHeld[ iMyTeam ] < MC_serverBD.iNumobjHighestPriority[ iMyTeam ] // There are still objects left to pick up that aren't picked up
							AND MC_playerBD[iPartToUse].iObjCarryCount = 0 AND NOT IS_BIT_SET( MC_playerBD[iObjTxtPartToUse].iClientBitSet, PBBOOL_WITH_CARRIER )
								REMOVE_DELIVERY_BLIP()
							ELSE
								IF IS_BIT_SET( iLocalBoolCheck3, LBOOL3_UPDATE_OBJECT_OBJECTIVE )
								OR NOT DOES_BLIP_EXIST( DeliveryBlip )
									ADD_DELIVERY_BLIP()
								ENDIF
							ENDIF
						ELSE
							IF iMyTeam < FMMC_MAX_TEAMS
								IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iMyTeam].iRuleBitsetFour[iRule] ,ciBS_RULE4_ALWAYS_BLIP_DELIVERY_POINT)
									REMOVE_DELIVERY_BLIP()
								ENDIF
							ENDIF
						ENDIF
					
					ELSE
						ADD_DELIVERY_BLIP(bBlipChange)
					ENDIF
	
					IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] < FMMC_MAX_RULES
						tl63CustomGodText = GET_CUSTOM_OBJECTIVE_TEXT_LABEL(MC_playerBD[iObjTxtPartToUse].iteam, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam], g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].tl63Objective[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]])
						tl63CustomGodText1 = GET_CUSTOM_OBJECTIVE_TEXT_LABEL(MC_playerBD[iObjTxtPartToUse].iteam, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam], g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].tl63Objective1[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]])
						tl63CustomGodText2 = GET_CUSTOM_OBJECTIVE_TEXT_LABEL(MC_playerBD[iObjTxtPartToUse].iteam, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam],g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].tl63Objective2[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]]) 
					ENDIF
					
					#IF IS_DEBUG_BUILD
					IF bDebugPrintCustomText
					PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT - iclientstage = ", iclientstage)
					PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT - iPartToUse = ", iObjTxtPartToUse)
					PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT - MC_playerBD[iPartToUse].iteam = ", MC_playerBD[iObjTxtPartToUse].iteam)
					PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT - MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] = ", MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam])
					PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT - tl63CustomGodText = '", tl63CustomGodText, "' tl63CustomGodText1 = '", tl63CustomGodText1, "' tl63CustomGodText2 = '", tl63CustomGodText2, "'")	
					ENDIF
					#ENDIF
					
					//IF Is_MP_Objective_Text_On_Display()
					//AND g_sObjectiveTextMP.otStringInString1 != 
						//PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT - otStringInString1 = ", g_sObjectiveTextMP.otStringInString1)						
					//ENDIF
					
					BOOL bLastAliveText					
					BOOL bSkipObjectiveTextUpdate = FALSE
					
					IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] < FMMC_MAX_RULES
						
						IF SHOULD_TAG_TEAM_USE_SECONDARY_TEXT()						
							IF NOT IS_STRING_NULL_OR_EMPTY(tl63CustomGodText1)
							AND NOT bobjectivedisp
							AND NOT bcustomobjective
								PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT - Using Secondary Text for Tag Team")	
								PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
								bobjectivedisp = TRUE
								bcustomobjective = TRUE
								bSkipObjectiveTextUpdate = TRUE
							ENDIF	
						ENDIF				
							
						IF IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH) AND IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_SUDDEN_DEATH_TAG_TEAM)
						OR (HAS_NET_TIMER_STARTED(MC_serverBD_3.tdTagOutCooldown[MC_playerBD[iObjTxtPartToUse].iteam]) AND NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(MC_serverBD_3.tdTagOutCooldown[MC_playerBD[iObjTxtPartToUse].iteam], (g_FMMC_STRUCT.iTagTeamCooldownTime*1000)))
						OR SHOULD_TAG_TEAM_USE_ALT_TEXT()
							IF NOT IS_STRING_NULL_OR_EMPTY(tl63CustomGodText2)
							AND NOT bobjectivedisp
							AND NOT bcustomobjective
								PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText2)
								PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT - Sudden Death - or Alt Text")	
								bobjectivedisp = TRUE
								bcustomobjective = TRUE
								bSkipObjectiveTextUpdate = TRUE
							ENDIF			
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iRuleBitsetTwelve[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]], ciBS_RULE12_LAST_PLAYER_STANDING_ALT_OBJ_TEXT)
						AND IS_LOCAL_PLAYER_LAST_ALIVE_ON_TEAM(MC_playerBD[iObjTxtPartToUse].iteam)
							PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT - Last alive and ciBS_RULE12_LAST_PLAYER_STANDING_ALT_OBJ_TEXT is set. We want to show Alt Text.")	
							bLastAliveText = TRUE
						ENDIF
						IF bLastAliveText
						AND NOT IS_STRING_NULL_OR_EMPTY(tl63CustomGodText2)	
						AND NOT bobjectivedisp
						AND NOT bcustomobjective
							PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText2)
							PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT - Overridden as in last alive")
							bobjectivedisp = TRUE
							bcustomobjective = TRUE
							bSkipObjectiveTextUpdate = TRUE
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]], ciBS_RULE9_USE_ALTERNATIVE_TEXT_IN_MOC)
						AND NOT IS_STRING_NULL_OR_EMPTY(tl63CustomGodText2)	
							IF IS_PLAYER_IN_CREATOR_TRAILER(ObjTxtPlayerToUse)
							OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen,ciENABLE_AVENGER_INTERIOR_ENTRY)
							AND IS_PLAYER_IN_CREATOR_AIRCRAFT(ObjTxtPlayerToUse)
							AND NOT IS_TEAMMATE_READY_TO_DELIVER_SPECIFIC_VEHICLE(MC_playerBD[iObjTxtPartToUse].iteam)
							AND IS_BIT_SET(iLocalBoolCheck2,LBOOL2_WHOLE_UPDATE_DONE))
								IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText2)
									#IF IS_DEBUG_BUILD
									IF bDebugPrintCustomText
										PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT - Overridden as in MOC")	
									ENDIF
									#ENDIF
									bobjectivedisp = TRUE
									bcustomobjective = TRUE
									bSkipObjectiveTextUpdate = TRUE
								ENDIF
							ENDIF
						ENDIF
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iRuleBitsetEleven[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]], ciBS_RULE11_SECONDARY_TEXT_IN_ORBITAL_CANNON)
						AND NOT IS_STRING_NULL_OR_EMPTY(tl63CustomGodText1)	
							IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(ObjTxtPlayerToUse)].iMissionModeBit, ciMISSION_USING_ORBITAL_CANNON)
								IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
									#IF IS_DEBUG_BUILD
									IF bDebugPrintCustomText
										PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT - Overridden as in Orbital Canon")	
									ENDIF
									#ENDIF
									bobjectivedisp = TRUE
									bcustomobjective = TRUE
									bSkipObjectiveTextUpdate = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF MC_serverBD.iCurrentPlayerRule[MC_playerBD[iObjTxtPartToUse].iteam] > -1
					AND MC_serverBD.iCurrentPlayerRule[MC_playerBD[iObjTxtPartToUse].iteam] < FMMC_MAX_RULES
					AND MC_serverBD_4.iPlayerRulePriority[ MC_serverBD.iCurrentPlayerRule[MC_playerBD[iObjTxtPartToUse].iteam]][MC_playerBD[iObjTxtPartToUse].iteam] = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]
					AND MC_serverBD_4.iPlayerRule[MC_serverBD.iCurrentPlayerRule[MC_playerBD[iObjTxtPartToUse].iteam]][MC_playerBD[iObjTxtPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_SCRIPTED_CUTSCENE
					AND MC_serverBD.iPlayerRuleLimit[MC_serverBD.iCurrentPlayerRule[MC_playerBD[iObjTxtPartToUse].iteam]][MC_playerBD[iObjTxtPartToUse].iteam] >= 0
					AND iclientstage != CLIENT_MISSION_STAGE_SCRIPTED_CUTSCENE	
						#IF IS_DEBUG_BUILD
						IF bDebugPrintCustomText
							PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT - Skipping Objective Text - Cutscene Rule, not on correct client stage though")	
						ENDIF
						#ENDIF
						IF Is_MP_Objective_Text_On_Display()
							Clear_Any_Objective_Text()
						ENDIF
						bobjectivedisp = TRUE
						bcustomobjective = TRUE
						bSkipObjectiveTextUpdate = TRUE
					ENDIF
										
					IF IS_BIT_SET(iLocalBoolCheck24, LBOOL24_BLOCK_OBJECTIVE_TEXT_UNTIL_RULE_CHANGE)
						#IF IS_DEBUG_BUILD
						IF bDebugPrintCustomText
							PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT - Skipping Objective Text - LBOOL24_BLOCK_OBJECTIVE_TEXT_UNTIL_RULE_CHANGE")	
						ENDIF
						#ENDIF
						IF Is_MP_Objective_Text_On_Display()
							Clear_Any_Objective_Text()
						ENDIF
						bobjectivedisp = TRUE
						bcustomobjective = TRUE
					ENDIF
					
					IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] < FMMC_MAX_RULES
					AND (NOT IS_STRING_NULL_OR_EMPTY( tl63CustomGodText )
					OR NOT IS_STRING_NULL_OR_EMPTY( tl63CustomGodText1 ) 
					OR NOT IS_STRING_NULL_OR_EMPTY( tl63CustomGodText2 ) )
					AND NOT g_FMMC_STRUCT.bPlayerLangNotEqualCreated
					AND NOT bSkipObjectiveTextUpdate
					AND NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_BLOCK_OBJECTIVE_TEXT_UNTIL_RULE_CHANGE)
					AND NOT bobjectivedisp
					AND NOT bcustomobjective
						SWITCH iclientstage
							
							CASE CLIENT_MISSION_STAGE_CONTROL_AREA
							CASE CLIENT_MISSION_STAGE_PHOTO_LOC
								BOOL bPreventCurrentObjectiveChange
								
								IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
									IF DOES_TEAM_CONTROL_ALL_CAPTURE_LOCATIONS(MC_playerBD[iObjTxtPartToUse].iteam)
										IF PRINT_HOSTILE_TAKEOVER_OBJECTIVE()
											bobjectivedisp=TRUE
											bcustomobjective = TRUE
											bPreventCurrentObjectiveChange = TRUE
										ENDIF
									ENDIF
								ENDIF
								
								IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
								OR (IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType) AND NOT bPreventCurrentObjectiveChange)
									IF MC_playerBD[iPartToUse].iCurrentLoc != -1
										IF PRINT_CUSTOM_OBJECTIVE(PROCESS_OBJECTIVE_TEXT_COLOUR_CHANGE(tl63CustomGodText1))
											bobjectivedisp=TRUE
											bcustomobjective = TRUE
										ELSE
											bNoSecondObjective = TRUE
										ENDIF
									ELSE
										IF MC_serverBD.iNumLocHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] <= 1
										AND NOT IS_BIT_SET(iLocalBoolCheck2,LBOOL2_WHOLE_UPDATE_DONE)
										AND iclientstage = CLIENT_MISSION_STAGE_CONTROL_AREA
										AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
											bobjectivedisp = TRUE
											bcustomobjective = TRUE
											PRINTLN("[JS] CONTROL_AREA - waiting for update")
										ENDIF
									ENDIF
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_GOTO_LOC
								
								IF MC_PlayerBD[iPartToUse].iteam < FMMC_MAX_TEAMS
									IF MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iPartToUse].iteam] < FMMC_MAX_RULES
										IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iPartToUse].iteam].iRuleBitsetSix[MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iPartToUse].iteam]], ciBS_RULE6_RESPAWN_ON_RULE_END)
										AND (IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam) OR IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_TEAM_0_SHOULD_RESPAWN_BACK_AT_START_NOW + iTeam) OR NOT GET_TOGGLE_PAUSED_RENDERPHASES_STATUS())
											IF Is_MP_Objective_Text_On_Display()
												Clear_Any_Objective_Text()
												PRINTLN("[LM][RCC MISSION][PROCESS_OBJECTIVE_TEXT] - Clearing Objective Text as we are respawning back at the start: SBBOOL3_TEAM_0_SHOULD_RESPAWN_BACK_AT_START_NOW + ciBS_RULE6_RESPAWN_ON_RULE_END")
											ENDIF
											bobjectivedisp = FALSE
										ELSE
											IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TOUR_DE_FORCE(g_FMMC_STRUCT.iAdversaryModeType)
												IF IS_BIT_SET(MC_serverbd.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
													PRINT_TOUR_DE_FORCE_OBJECTIVE()
													bobjectivedisp = TRUE
													bcustomobjective = TRUE
												ENDIF
											ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciOVERTIME_WAIT_YOUR_TURN)
											AND GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()
											AND NOT IS_BIT_SET(iLocalBoolCheck22, LBOOL22_OVERTIME_DELAYED_SCORE_SHARD)
												IF iOTPartToMove > -1
												AND iOTPartToMove != iLocalPart
													PARTICIPANT_INDEX tempPart
													tempPart = INT_TO_PARTICIPANTINDEX(iOTPartToMove)
													IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
														IF PRINT_COLOURED_TEAM_OBJECTIVE_WITH_PLAYER("OT_OBJ_WAIT", MC_PlayerBD[iOTPartToMove].iteam, NETWORK_GET_PLAYER_INDEX(tempPart))
															bobjectivedisp=TRUE
															bcustomobjective = TRUE
														ENDIF
													ENDIF
												ELSE
													bcustomobjective = TRUE
												ENDIF
												
											ELIF IS_THIS_CURRENTLY_OVERTIME_RUMBLE()
												IF GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()
												AND NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_OVERTIME_MODE_SWITCH_OBJ_BLOCK)
													IF IS_PROP_IN_LIST_OF_LOCATE_LINKED_PROPS(MC_PlayerBD[iPartToUse].iCurrentPropHit[0], MC_PlayerBD[iPartToUse].iCurrentPropHit[1], MC_PlayerBD[iPartToUse].iCurrentPropHit[2])
													AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdOvertimeStationaryTimer, ciOVERTIME_STATIONARY_TIME)
													AND HAS_NET_TIMER_STARTED(tdOvertimeStationaryTimer)
													AND NOT IS_LOCAL_PLAYER_LAST_ALIVE()
														IF PRINT_NORMAL_OBJECTIVE("OT_OBJ_WAIT_R1")
															bobjectivedisp = TRUE
															bcustomobjective = TRUE
														ENDIF
													ELSE
														IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciUSE_TARGET_SHARD_SINGULAR_TEXT)
															IF PRINT_NORMAL_OBJECTIVE("OT_OBJ_LAND_R1")
																bobjectivedisp = TRUE
																bcustomobjective = TRUE
															ENDIF
														ELSE
															IF PRINT_NORMAL_OBJECTIVE("OT_OBJ_LAND_R2")
																bobjectivedisp = TRUE
																bcustomobjective = TRUE
															ENDIF
														ENDIF
													ENDIF
												ELSE
													IF Is_MP_Objective_Text_On_Display()
														Clear_Any_Objective_Text()
														PRINTLN("[LM][RCC MISSION][PROCESS_OBJECTIVE_TEXT] - Clearing Objective Text as we are Finishing the Round.")
													ENDIF
													bobjectivedisp = FALSE
												ENDIF
											ELSE
											
												IF iLastRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]

													IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_SUB_TEAM_ROLES)
														IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
															PRINTLN("[MMacK][RoleHUD] 1")
															IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRolesBitset[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]][MC_playerBD[iLocalPart].iCoronaRole], ciBS_ROLE_Cant_Complete_This_Rule)
																PRINTLN("[MMacK][RoleHUD] 2")
																IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRolesBitset[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]][MC_playerBD[iLocalPart].iCoronaRole], ciBS_ROLE_SET_AS_GHOST)
																	IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
																		bobjectivedisp=TRUE
																		bcustomobjective = TRUE
																	ELSE
																		bNoSecondObjective = TRUE
																	ENDIF
																ELSE
																	IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText)
																		bobjectivedisp=TRUE
																		bcustomobjective = TRUE
																	ELSE
																		bNoSecondObjective = TRUE
																	ENDIF
																ENDIF
															ELSE
																PRINTLN("[MMacK][RoleHUD] 3")
																IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
																	bobjectivedisp=TRUE
																	bcustomobjective = TRUE
																ELSE
																	bNoSecondObjective = TRUE
																ENDIF
															ENDIF	
														ELSE
															bobjectivedisp = TRUE
															bcustomobjective = TRUE
														ENDIF
													ELIF NOT IS_PLAYER_READY_TO_DELIVER_ANY_SPECIFIC_VEHICLE(MC_playerBD[iObjTxtPartToUse].iteam, GET_PLAYER_PED(ObjTxtPlayerToUse)) 
														IF IS_TEAMMATE_READY_TO_DELIVER_SPECIFIC_VEHICLE(MC_playerBD[iObjTxtPartToUse].iteam)
														AND IS_BIT_SET(iLocalBoolCheck2,LBOOL2_WHOLE_UPDATE_DONE)															
															IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText2)
																bobjectivedisp=TRUE
																bcustomobjective = TRUE
															ELSE
																bNoSecondObjective = TRUE
															ENDIF
														ELSE
															IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
																bobjectivedisp=TRUE
																bcustomobjective = TRUE
															ELSE
																bNoSecondObjective = TRUE
															ENDIF
														ENDIF
													ELIF MC_playerBD[iObjTxtPartToUse].iCurrentLoc != -1
														IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[MC_playerBD[iObjTxtPartToUse].iCurrentLoc].iWholeTeamAtLocation[MC_playerBD[iObjTxtPartToUse].iteam] != ciGOTO_LOCATION_INDIVIDUAL
															IF IS_BIT_SET(iLocalBoolCheck2,LBOOL2_WHOLE_UPDATE_DONE)
															OR IS_THIS_ROCKSTAR_MISSION_SVM_DUNE4(g_FMMC_STRUCT.iRootContentIDHash)
																IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[MC_playerBD[iObjTxtPartToUse].iCurrentLoc].iWholeTeamAtLocation[MC_playerBD[iObjTxtPartToUse].iteam] != ciGOTO_LOCATION_WHOLE_TEAM
																OR MC_serverBD.iNumberOfPlayingPlayers[MC_playerBD[iObjTxtPartToUse].iteam] > 1																	
																	IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[MC_playerBD[iObjTxtPartToUse].iCurrentLoc].fSecondaryRadius = 0
																	OR AM_I_IN_SECONDARY_LOCATE_IF_I_NEED_TO_BE(MC_playerBD[iObjTxtPartToUse].iCurrentLoc)
																		IF DOES_RULE_REQUIRE_A_SPECIFIC_VEHICLE()
																			IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText2)
																				bobjectivedisp=TRUE
																				bcustomobjective = TRUE
																			ELSE
																				bNoSecondObjective = TRUE
																			ENDIF
																		ELSE
																			IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
																				bobjectivedisp=TRUE
																				bcustomobjective = TRUE
																			ELSE
																				bNoSecondObjective = TRUE
																			ENDIF
																		ENDIF
																	ENDIF
																ENDIF
															ELSE
																bcustomobjective = TRUE
																
																IF NOT IS_BIT_SET(iLocalBoolCheck2,LBOOL2_WHOLE_UPDATE_DONE)
																	bobjectivedisp = TRUE
																	PRINTLN("[RCC MISSION][PROCESS_OBJECTIVE_TEXT] - Waiting for LBOOL2_WHOLE_UPDATE_DONE, setting bobjectivedisp to prevent text being drawn below")
																ENDIF
															ENDIF
														ENDIF
													ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iPartToUse].iteam].iRuleBitsetFourteen[MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iPartToUse].iteam]], ciBS_RULE14_OVERRIDE_TEXT_ON_OUTFIT_SWAP)
													AND IS_BIT_SET(iLocalBoolCheck31, LBOOL31_OUTFIT_CHANGED_THROUGH_INTERACT)
													AND PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText2)	
														bobjectivedisp = TRUE
														bcustomobjective = TRUE
														PRINTLN("[RCC MISSION][PROCESS_OBJECTIVE_TEXT] - Primary text overidden as we have changed outfits")
													ENDIF
												ELSE
													bobjectivedisp = TRUE
													bcustomobjective = TRUE
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_LEAVE_LOC
							
								IF iLastRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam] //bit of a hack to sync this
									IF NOT IS_BIT_SET(iLocalBoolCheck6,LBOOL6_IN_MY_PRIORITY_LEAVE_LOC) //I'm out of the location!
										IF IS_BIT_SET(iLocalBoolCheck2,LBOOL2_WHOLE_UPDATE_DONE)
										AND IS_BIT_SET(MC_serverBD.iServerBitSet2,SBBOOL2_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC + MC_playerBD[iObjTxtPartToUse].iteam)
										AND MC_serverBD.iNumberOfPlayingPlayers[MC_playerBD[iObjTxtPartToUse].iteam] > 1
											#IF IS_DEBUG_BUILD
												IF bWdLocDebug
													PRINTLN("[LeaveLoc] CASE CLIENT_MISSION_STAGE_LEAVE_LOC debug... SBBOOL2_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC + MC_playerBD[iObjTxtPartToUse].iteam is set! Gonna try and show tl63CustomGodText1: ", tl63CustomGodText1)
												ENDIF
											#ENDIF

											IF IS_OBJECTIVE_TEXT_UPDATE_READY(OBJ_TEXT_DELAY_BS_GROUP_0, iLocalBoolCheck29, LBOOL29_UPDATE_LEAVE_LOC, TRUE, OBJECTIVE_TEXT_DELAY_LONG)
												IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
													bobjectivedisp = TRUE
													bcustomobjective = TRUE
												ELSE
													bNoSecondObjective = TRUE
												ENDIF
											ELSE
												bobjectivedisp = TRUE
												bcustomobjective = TRUE
											ENDIF
										ELSE
											#IF IS_DEBUG_BUILD
												IF bWdLocDebug
													PRINTLN("[MMacK][LeaveLoc] CASE CLIENT_MISSION_STAGE_LEAVE_LOC debug... LBOOL2_WHOLE_UPDATE_DONE not set! Will fallthru")
												ENDIF
												
												IF NOT IS_BIT_SET(iLocalBoolCheck2,LBOOL2_WHOLE_UPDATE_DONE)
													PRINTLN("[MMacK][LeaveLoc] LBOOL2_WHOLE_UPDATE_DONE")
												ENDIF
												
												IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet2,SBBOOL2_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC + MC_playerBD[iObjTxtPartToUse].iteam)
													PRINTLN("[MMacK][LeaveLoc] SBBOOL2_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC")
												ENDIF
											#ENDIF

											IF NOT IS_PLAYER_IN_VEHICLE_WITH_ALL_OF_TEAM(MC_ServerBD.iNumberOfPlayingPlayers[ MC_playerBD[iObjTxtPartToUse].iteam ])
												IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText2)
													bobjectivedisp = TRUE
													bcustomobjective = TRUE
												ELSE
													bNoSecondObjective = TRUE
												ENDIF
											ELSE
												bobjectivedisp = TRUE
												bcustomobjective = TRUE
											ENDIF
										ENDIF
									ELSE
										#IF IS_DEBUG_BUILD
											IF bWdLocDebug
												PRINTLN("[MMacK][LeaveLoc] CASE CLIENT_MISSION_STAGE_LEAVE_LOC debug...")
												IF IS_BIT_SET(iLocalBoolCheck6,LBOOL6_IN_MY_PRIORITY_LEAVE_LOC) //
													PRINTLN("[MMacK][LeaveLoc] CASE CLIENT_MISSION_STAGE_LEAVE_LOC debug... LBOOL6_IN_MY_PRIORITY_LEAVE_LOC is set Will fallthru")
												ENDIF
												
												IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet2,SBBOOL2_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC + MC_playerBD[iObjTxtPartToUse].iteam)
													PRINTLN("[MMacK][LeaveLoc] CASE CLIENT_MISSION_STAGE_LEAVE_LOC debug... SBBOOL2_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC not set Will fallthru MC_playerBD[iPartToUse].iteam = ",  MC_playerBD[iPartToUse].iteam)
												ENDIF
											ENDIF
										#ENDIF
									ENDIF
								ELSE
									IF Is_MP_Objective_Text_On_Display()
										Clear_Any_Objective_Text()
									ENDIF
									
									PRINTLN("[MMacK][LeaveLoc] CLEAR!")
									
									bobjectivedisp = TRUE
									bcustomobjective = TRUE
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_CHARM_PED
							
								IF MC_playerBD[iObjTxtPartToUse].iPedCharming != -1
									IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
										bobjectivedisp=TRUE
										bcustomobjective = TRUE
									ELSE
										bNoSecondObjective = TRUE
									ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_CROWD_CONTROL
							
								IF MC_playerBD[iObjTxtPartToUse].iPedCrowdControlBits[0] != 0
								OR MC_playerBD[iObjTxtPartToUse].iPedCrowdControlBits[1] != 0
									IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
										bobjectivedisp=TRUE
										bcustomobjective = TRUE
									ELSE
										bNoSecondObjective = TRUE
									ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_PHOTO_PED
							
								IF MC_playerBD[iObjTxtPartToUse].iPedNear != -1
									IF NOT IS_BIT_SET(MC_serverBD.iDeadPedPhotoBitset[MC_playerBD[iObjTxtPartToUse].iteam][GET_LONG_BITSET_INDEX(MC_playerBD[iObjTxtPartToUse].iPedNear)], GET_LONG_BITSET_BIT(MC_playerBD[iObjTxtPartToUse].iPedNear))
									OR	(IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[MC_playerBD[iObjTxtPartToUse].iPedNear]) AND IS_BIT_SET(MC_serverBD.iDeadPedPhotoBitset[MC_playerBD[iObjTxtPartToUse].iteam][GET_LONG_BITSET_INDEX(MC_playerBD[iObjTxtPartToUse].iPedNear)], GET_LONG_BITSET_BIT(MC_playerBD[iObjTxtPartToUse].iPedNear)))
										IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
											bobjectivedisp=TRUE
											bcustomobjective = TRUE
										ELSE
											bNoSecondObjective = TRUE
										ENDIF
									ENDIF
								ELSE
									IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iPhotoCanBeDead,MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam])
										IF MC_serverBD.iNumDeadPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] >= 1
											IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText2)
												bobjectivedisp=TRUE
												bcustomobjective = TRUE
											ELSE
												bDispHelpObjective=TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_PHOTO_VEH
								IF MC_playerBD[iPartToUse].iVehNear != -1
									IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
										bobjectivedisp = TRUE
										bcustomobjective = TRUE
									ELSE
										bNoSecondObjective = TRUE
									ENDIF
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_PHOTO_OBJ
							CASE CLIENT_MISSION_STAGE_HACK_SYNC_LOCK
							CASE CLIENT_MISSION_STAGE_HACK_OPEN_CASE
							CASE CLIENT_MISSION_STAGE_OPEN_CONTAINER
							CASE CLIENT_MISSION_STAGE_CASH_GRAB
								
								FOR iobj = 0 TO FMMC_MAX_NUM_OBJECTS - 1
									IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
										IF DOES_ENTITY_EXIST(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj]))
											IF GET_ENTITY_MODEL(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj])) = INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_impexp_coke_trolly"))
												IF MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] < FMMC_MAX_RULES
													IF MC_serverBD_4.iObjPriority[iobj][MC_playerBD[iObjTxtPartToUse].iTeam] = MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]
														bPrintSecondary = TRUE
														iNumCokeTrolleyObjs++
														PRINTLN("[KH][OBJ] Adding trolley on this rule")
														IF MC_serverBD.iObjHackPart[iobj] != -1
														AND MC_serverBD.iObjHackPart[iobj] != iObjTxtPartToUse
															iNumObjsBeingHacked++
															PRINTLN("[KH][OBJ] Adding object being hacked")
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDFOR
								
								BOOL bKeepPrintingSecondary
								IF bPrintSecondaryCokeGrabText
								AND iNumCokeTrolleyObjs = 0
									bKeepPrintingSecondary = TRUE
								ENDIF
								
								IF bPrintSecondary
									IF iNumObjsBeingHacked >= iNumCokeTrolleyObjs
									OR g_TransitionSessionNonResetVars.iLocalPlayerCashGrabbed >= iMaxCashLocalPlayerCanCarry
									OR bKeepPrintingSecondary
										IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
											bcustomobjective = TRUE
											bobjectivedisp=TRUE
											bPrintSecondaryCokeGrabText = TRUE
										ELSE
											bNoSecondObjective = TRUE
										ENDIF
									ELSE
										IF MC_playerBD[iObjTxtPartToUse].iObjNear != -1
											IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText)
												bcustomobjective = TRUE
												bobjectivedisp=TRUE
												bPrintSecondaryCokeGrabText = FALSE
											ELSE
												bNoSecondObjective = TRUE
											ENDIF
										ENDIF
									ENDIF
								ELSE
									IF MC_playerBD[iObjTxtPartToUse].iObjNear != -1
										IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
											bobjectivedisp=TRUE
											bcustomobjective = TRUE
										ELSE
											bNoSecondObjective = TRUE
										ENDIF
									ENDIF
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_CONTROL_PED
							CASE CLIENT_MISSION_STAGE_COLLECT_PED
							CASE CLIENT_MISSION_STAGE_DELIVER_PED
								IF iLastRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam] //bit of a hack to sync this
									iTeam = MC_playerBD[iObjTxtPartToUse].iteam
									IF IS_OBJECTIVE_TEXT_UPDATE_READY(OBJ_TEXT_DELAY_BS_GROUP_0, iLocalBoolCheck3, LBOOL3_UPDATE_PED_OBJECTIVE)
									
										#IF IS_DEBUG_BUILD
										IF bDebugPrintCustomText
											PRINTLN("[JS] [GDTEXT] - PED - IS_OBJECTIVE_TEXT_UPDATE_READY TRUE")
										ENDIF
										#ENDIF
										
										IF IS_BIT_SET(iLocalBoolCheck30, LBOOL30_SHOW_CUSTOM_WAIT_TEXT)
											TEXT_LABEL_63 tlWaitText
											tlWaitText = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].tl63WaitMessage[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]]
											IF PRINT_CUSTOM_OBJECTIVE(tlWaitText)
												bobjectivedisp = TRUE
												bcustomobjective = TRUE
											ENDIF
										ELIF MC_playerBD[iObjTxtPartToUse].iPedCarryCount > 0
										OR IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER)
										OR IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet,PBBOOL_IN_GROUP_PED_VEH)
											#IF IS_DEBUG_BUILD
											IF bDebugPrintCustomText
												IF MC_playerBD[iObjTxtPartToUse].iPedCarryCount > 0
													PRINTLN("[JS] [GDTEXT] - PED - iPedCarryCount > 0")
												ENDIF
												IF IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER)
													PRINTLN("[JS] [GDTEXT] - PED - PBBOOL_WITH_CARRIER")
												ENDIF
												IF IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet,PBBOOL_IN_GROUP_PED_VEH)
													PRINTLN("[JS] [GDTEXT] - PED - PBBOOL_IN_GROUP_PED_VEH")
												ENDIF
											ENDIF
											#ENDIF
											IF (MC_serverBD.iRequiredDeliveries[iTeam] <= 1)
											OR (MC_serverBD.iNumPedHighestPriorityHeld[iTeam] >= MC_serverBD.iRequiredDeliveries[iTeam])
												#IF IS_DEBUG_BUILD
												IF bDebugPrintCustomText
													IF MC_serverBD.iRequiredDeliveries[iTeam] <= 1
														PRINTLN("[JS] [GDTEXT] - PED - MC_serverBD.iRequiredDeliveries[iTeam] <= 1")
													ENDIF
													IF MC_serverBD.iNumPedHighestPriorityHeld[iTeam] >= MC_serverBD.iRequiredDeliveries[iTeam]
														PRINTLN("[JS] [GDTEXT] - PED - MC_serverBD.iNumPedHighestPriorityHeld[iTeam] >= MC_serverBD.iRequiredDeliveries[iTeam]")
													ENDIF
												ENDIF
												#ENDIF
												IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
													bobjectivedisp=TRUE
													bcustomobjective = TRUE
												ELSE
													bNoSecondObjective = TRUE
												ENDIF
											ENDIF
										ELIF MC_serverBD.iNumPedHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] >= MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]
										//OR IS_ANY_FRIENDLY_CARRYING(CLIENT_MISSION_STAGE_DELIVER_PED)
											#IF IS_DEBUG_BUILD
											IF bDebugPrintCustomText
												PRINTLN("[JS] [GDTEXT] - PED - MC_serverBD.iNumPedHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] >= MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]")
											ENDIF
											#ENDIF
											IF MC_serverBD.iTotalPlayingCoopPlayers[MC_playerBD[iObjTxtPartToUse].iteam] > 1
											AND NOT IS_THIS_ROCKSTAR_MISSION_SVM_VOLTIC2(g_FMMC_STRUCT.iRootContentIDHash)
												#IF IS_DEBUG_BUILD
												IF bDebugPrintCustomText
													PRINTLN("[JS] [GDTEXT] - PED - MC_serverBD.iTotalPlayingCoopPlayers[MC_playerBD[iObjTxtPartToUse].iteam] > 1")
												ENDIF
												#ENDIF
												IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText2)
													bobjectivedisp=TRUE
													bcustomobjective = TRUE
												ELSE
													bDispHelpObjective=TRUE
												ENDIF
											ENDIF
										ENDIF
									ELSE
										#IF IS_DEBUG_BUILD
										IF bDebugPrintCustomText
											PRINTLN("[JS] [GDTEXT] - PED - IS_OBJECTIVE_TEXT_UPDATE_READY FALSE")
										ENDIF
										#ENDIF
										/*IF NOT IS_BIT_SET(iLocalBoolCheck2,LBOOL2_WHOLE_UPDATE_DONE)
											IF Is_MP_Objective_Text_On_Display()
												Clear_Any_Objective_Text()
											ENDIF
										ENDIF*/
										bobjectivedisp=TRUE
										bcustomobjective = TRUE
									ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_GOTO_VEH
							
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen,ciENABLE_AVENGER_INTERIOR_ENTRY)
								AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]], ciBS_RULE9_WAIT_FOR_REMAINING_IN_MOC_TO_PROGRESS)
									bAvengerHasDriver = IS_ANY_PLAYER_IN_AVENGER_PILOT_SEAT(iAvengerDriver)
								ENDIF
								
								IF bAvengerHasDriver != IS_BIT_SET(iLocalBoolCheck28, LBOOL28_PREV_AVENGER_PILOT_STATUS)
									IF bAvengerHasDriver
										SET_BIT(iLocalBoolCheck28, LBOOL28_PREV_AVENGER_PILOT_STATUS)
									ELSE
										CLEAR_BIT(iLocalBoolCheck28, LBOOL28_PREV_AVENGER_PILOT_STATUS)
									ENDIF
									PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - bAvengerHasDriver")			
									SET_BIT(iLocalBoolCheck3, LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
								ENDIF
								
								IF iLastRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]
									IF IS_OBJECTIVE_TEXT_UPDATE_READY(OBJ_TEXT_DELAY_BS_GROUP_0, iLocalBoolCheck3, LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
									OR IS_THIS_ROCKSTAR_MISSION_WVM_OPPRESSOR_2(g_FMMC_STRUCT.iRootContentIDHash)
								
										IF MC_playerBD[iObjTxtPartToUse].iVehNear != -1
										OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen,ciENABLE_AVENGER_INTERIOR_ENTRY)
										AND bAvengerHasDriver
										AND MC_playerBD[iAvengerDriver].iVehNear != -1
										AND (NOT (MC_serverBD.iNumVehHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iTeam] >= MC_serverBD.iNumvehHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]))
										AND IS_BIT_SET(iLocalBoolCheck24, LBOOL24_IN_CREATOR_TRAILER))
											BOOL bCheckOnRule // To let us use GET_VEHICLE_GOTO_TEAMS here	
											INT iVehNearPart
											IF MC_playerBD[iObjTxtPartToUse].iVehNear = -1
												iVehNearPart = iAvengerDriver
											ELSE
												iVehNearPart = iObjTxtPartToUse
											ENDIF
											IF (GET_VEHICLE_GOTO_TEAMS(MC_playerBD[iObjTxtPartToUse].iteam,MC_playerBD[iVehNearPart].iVehNear,bCheckOnRule) > 0)
												PRINTLN("[MMacK][FleecaFinaleGoto] GoToRule Custom text")
												IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
													bobjectivedisp = TRUE
													bcustomobjective = TRUE
													#IF IS_DEBUG_BUILD
													IF bDebugPrintCustomText
														PRINTLN("[JS] [GDTEXT] - GOTO VEH - Printing tl63CustomGodText1")
													ENDIF
													#ENDIF
												ELSE
													#IF IS_DEBUG_BUILD
													IF bDebugPrintCustomText
														PRINTLN("[JS] [GDTEXT] - GOTO VEH - Failed to print tl63CustomGodText1")
													ENDIF
													#ENDIF
													bNoSecondObjective = TRUE
												ENDIF
											#IF IS_DEBUG_BUILD
											ELSE
												IF bDebugPrintCustomText
													PRINTLN("[JS] [GDTEXT] - GOTO VEH - GET_VEHICLE_GOTO_TEAMS = 0")
												ENDIF
											#ENDIF
											ENDIF
										#IF IS_DEBUG_BUILD
										ELSE
											IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].tl63WrongWayMessage[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]])
												IF NOT bAvengerHasDriver
												AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen,ciENABLE_AVENGER_INTERIOR_ENTRY)
												AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]], ciBS_RULE9_WAIT_FOR_REMAINING_IN_MOC_TO_PROGRESS)
												AND IS_BIT_SET(iLocalBoolCheck24, LBOOL24_IN_CREATOR_TRAILER)
													#IF IS_DEBUG_BUILD
														IF bDebugPrintCustomText
															PRINTLN("[JS] [GDTEXT] - Cover Blown/Avenger Wrong way text GOTO")
														ENDIF
													#ENDIF
													IF PRINT_CUSTOM_OBJECTIVE(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].tl63WrongWayMessage[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]])
														bobjectivedisp = TRUE
														bcustomobjective = TRUE
													ENDIF
												ENDIF
											ENDIF
											IF bDebugPrintCustomText
												PRINTLN("[JS] [GDTEXT] - GOTO VEH - MC_playerBD[iObjTxtPartToUse].iVehNear = -1")
											ENDIF
										#ENDIF
										ENDIF
										
									ELIF MC_playerBD[iObjTxtPartToUse].iteam < FMMC_MAX_TEAMS
									AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iRuleBitsetNine[iRule], ciBS_RULE9_USE_ALTERNATIVE_GOTO_VEH_TEXT)
										IF DRAW_OBJECTIVE_TEXT_FOR_WVM_TRAILERLARGE(MC_playerBD[iObjTxtPartToUse].iteam, iRule, tl63CustomGodText, tl63CustomGodText1)
											bobjectivedisp = TRUE
											bcustomobjective = TRUE
											#IF IS_DEBUG_BUILD
											IF bDebugPrintCustomText
												PRINTLN("[JS] [GDTEXT] - GOTO VEH - DRAW_OBJECTIVE_TEXT_FOR_WVM_TRAILERLARGE")
											ENDIF
											#ENDIF
										ELSE
											#IF IS_DEBUG_BUILD
											IF bDebugPrintCustomText
												PRINTLN("[JS] [GDTEXT] - GOTO VEH - ciBS_RULE9_USE_ALTERNATIVE_GOTO_VEH_TEXT")
											ENDIF
											#ENDIF
											bNoSecondObjective = TRUE
										ENDIF
									ELSE
										#IF IS_DEBUG_BUILD
										IF bDebugPrintCustomText
											PRINTLN("[JS] [GDTEXT] - GOTO VEH - Not ready, not ciBS_RULE9_USE_ALTERNATIVE_GOTO_VEH_TEXT")
										ENDIF
										#ENDIF
										bobjectivedisp = TRUE
										bcustomobjective = TRUE
									ENDIF
								ELSE
									#IF IS_DEBUG_BUILD
									IF bDebugPrintCustomText
										PRINTLN("[JS] [GDTEXT] - GOTO VEH - iLastRule Else")
									ENDIF
									#ENDIF
									bobjectivedisp = TRUE
									bcustomobjective = TRUE
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_CONTROL_VEH
								
								IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam] < FMMC_MAX_RULES
								AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].iRuleBitsetEleven[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]], ciBS_RULE11_SECUROHACK_REQUIRES_KILL)
									IF IS_OBJECTIVE_TEXT_UPDATE_READY(OBJ_TEXT_DELAY_BS_GROUP_0, iLocalBoolCheck3, LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
										IF MC_serverBD_4.iHackingTargetsRemaining > 0
											IF MC_PlayerBD[iObjTxtPartToUse].iVehDestroyBitset > 0
												IF NOT HOLD_UP_VEHHACK_OBJECTIVE_TEXT(iObjTxtPartToUse)
													#IF IS_DEBUG_BUILD
													IF bDebugPrintCustomText
														PRINTLN("[JS] [GDTEXT] - HACK VEH - Kill (Alternate) 1")
													ENDIF
													#ENDIF
													IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText2)
														bobjectivedisp = TRUE
														bcustomobjective = TRUE
													ELSE
														bNoSecondObjective = TRUE
													ENDIF
												ELSE
													#IF IS_DEBUG_BUILD
													IF bDebugPrintCustomText
														PRINTLN("[GDTEXT] - HACK VEH - Clearing objective text due to dialogue not being done!")
													ENDIF
													#ENDIF
													
													IF Is_MP_Objective_Text_On_Display()
														Clear_Any_Objective_Text()
													ENDIF
														
													bobjectivedisp = TRUE
													bcustomobjective = TRUE
												ENDIF
											ELSE
												IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING) 
												AND MC_playerBD[iObjTxtPartToUse].iVehFollowing != -1
												AND IS_BIT_SET(iHackingVehInRangeBitSet, MC_playerBD[iObjTxtPartToUse].iVehFollowing)
													#IF IS_DEBUG_BUILD
														IF bDebugPrintCustomText
															PRINTLN("[JS] [GDTEXT] - HACK VEH - Complete the hack (secondary) iHackingVehInRangeBitSet = ", iHackingVehInRangeBitSet, " MC_playerBD[iObjTxtPartToUse].iVehFollowing: ", MC_playerBD[iObjTxtPartToUse].iVehFollowing)
														ENDIF
													#ENDIF
													IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
														bobjectivedisp = TRUE
														bcustomobjective = TRUE
													ELSE
														bNoSecondObjective = TRUE
													ENDIF
												ELSE
													#IF IS_DEBUG_BUILD
													IF bDebugPrintCustomText
														PRINTLN("[JS] [GDTEXT] - HACK VEH - Hack a target (Primary)")
													ENDIF
													#ENDIF
													IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText)
														bobjectivedisp = TRUE
														bcustomobjective = TRUE
													ELSE
														bNoSecondObjective = TRUE
													ENDIF
												ENDIF
											ENDIF
										ELSE
											#IF IS_DEBUG_BUILD
											IF bDebugPrintCustomText
												PRINTLN("[JS] [GDTEXT] - HACK VEH - Kill (Alternate) 2")
											ENDIF
											#ENDIF
											IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText2)
												bobjectivedisp = TRUE
												bcustomobjective = TRUE
											ELSE
												bNoSecondObjective = TRUE
											ENDIF
										ENDIF
									ELSE
										#IF IS_DEBUG_BUILD
										IF bDebugPrintCustomText
											PRINTLN("[JS] [GDTEXT] - HACK VEH  - Not ready")
										ENDIF
										#ENDIF
										bobjectivedisp = TRUE
										bcustomobjective = TRUE
									ENDIF
								ELSE
									// has vehicle objective colour been overriden
									IF iVehCapTeam != -1  
										IF IS_OBJECTIVE_TEXT_UPDATE_READY(OBJ_TEXT_DELAY_BS_GROUP_0, iLocalBoolCheck3, LBOOL3_UPDATE_VEHICLE_OBJECTIVE)	//IS_BIT_SET(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
										OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING)
										OR IS_LOCAL_PLAYER_ANY_SPECTATOR()
											IF (MC_playerBD[iObjTxtPartToUse].iVehNear != -1)
											OR IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER)
											OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING) 
											AND MC_playerBD[iObjTxtPartToUse].iVehFollowing != -1
											AND IS_BIT_SET(iHackingVehInRangeBitSet, MC_playerBD[iObjTxtPartToUse].iVehFollowing))
												IF PRINT_CUSTOM_COLOURED_TEAM_OBJECTIVE(tl63CustomGodText1,iVehCapTeam)
													bobjectivedisp=TRUE
													bcustomobjective = TRUE
												ELSE
													bNoSecondObjective = TRUE
												ENDIF
											ELIF MC_serverBD.iNumVehHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] >= MC_serverBD.iNumvehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]
											OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING) 
											AND IS_BIT_SET(iLocalBoolCheck23, LBOOL23_SOMEONE_IS_HACKING_A_VEHICLE))
												IF MC_serverBD.iTotalPlayingCoopPlayers[MC_playerBD[iObjTxtPartToUse].iteam] > 1
													IF PRINT_CUSTOM_COLOURED_TEAM_OBJECTIVE(tl63CustomGodText2,iVehCapTeam)
														bobjectivedisp=TRUE
														bcustomobjective = TRUE
													ELSE
														bDispHelpObjective=TRUE
													ENDIF
												ENDIF
											ENDIF
										ELSE
											bobjectivedisp = TRUE
											bcustomobjective = TRUE
										ENDIF
									ELSE
										IF IS_OBJECTIVE_TEXT_UPDATE_READY(OBJ_TEXT_DELAY_BS_GROUP_0, iLocalBoolCheck3, LBOOL3_UPDATE_VEHICLE_OBJECTIVE)	//IS_BIT_SET(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
										OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING)
										OR IS_LOCAL_PLAYER_ANY_SPECTATOR()
											IF (MC_playerBD[iObjTxtPartToUse].iVehNear != -1)
											OR IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER)
											OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING) 
											AND MC_playerBD[iObjTxtPartToUse].iVehFollowing != -1
											AND IS_BIT_SET(iHackingVehInRangeBitSet, MC_playerBD[iObjTxtPartToUse].iVehFollowing))
												IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
													bobjectivedisp=TRUE
													bcustomobjective = TRUE
												ELSE
													bNoSecondObjective = TRUE
												ENDIF
											ELIF MC_serverBD.iNumVehHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] >= MC_serverBD.iNumvehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]
											OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING) 
											AND IS_BIT_SET(iLocalBoolCheck23, LBOOL23_SOMEONE_IS_HACKING_A_VEHICLE))
												IF MC_serverBD.iTotalPlayingCoopPlayers[MC_playerBD[iObjTxtPartToUse].iteam] > 1
													IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText2)
														bobjectivedisp=TRUE
														bcustomobjective = TRUE
													ELSE
														bDispHelpObjective=TRUE
													ENDIF
												ENDIF
											ENDIF
										ELSE
											bobjectivedisp = TRUE
											bcustomobjective = TRUE
										ENDIF
									ENDIF
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_COLLECT_VEH
							CASE CLIENT_MISSION_STAGE_DELIVER_VEH
							
								iTeam = MC_playerBD[iObjTxtPartToUse].iteam
								BOOL bSkipUpdateDelay
								bSkipUpdateDelay = FALSE
								
								#IF IS_DEBUG_BUILD
								IF bDebugPrintCustomText
									PRINTLN("[JS] [GDTEXT] - On a Get and Deliver")
								ENDIF
								#ENDIF
								
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen,ciENABLE_AVENGER_INTERIOR_ENTRY)
								AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]], ciBS_RULE9_WAIT_FOR_REMAINING_IN_MOC_TO_PROGRESS)
									bAvengerHasDriver = IS_ANY_PLAYER_IN_AVENGER_PILOT_SEAT(iAvengerDriver)
								ENDIF
								
								IF bAvengerHasDriver != IS_BIT_SET(iLocalBoolCheck28, LBOOL28_PREV_AVENGER_PILOT_STATUS)
									IF bAvengerHasDriver
										SET_BIT(iLocalBoolCheck28, LBOOL28_PREV_AVENGER_PILOT_STATUS)
									ELSE
										CLEAR_BIT(iLocalBoolCheck28, LBOOL28_PREV_AVENGER_PILOT_STATUS)
									ENDIF
									PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - bAvengerHasDriver: GET AND DELIVER!")			
									SET_BIT(iLocalBoolCheck3, LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
								ENDIF
								
								IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUNNING_BACK_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
									IF IS_PED_IN_ANY_VEHICLE(PlayerPedToUse)						
										IF NOT (GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PlayerPedToUse)) = PANTO) 					//If we're not the runner...
										AND NOT Has_This_MP_Objective_Text_User_Created_Been_Received(tl63CustomGodText2) 			//And we aren't seeing the "help your runner" text...
											PRINTLN("[JS][GDTEXT][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - Setting LBOOL3_UPDATE_VEHICLE_OBJECTIVE because we're not seeing the correct one right now")
											SET_BIT(iLocalBoolCheck3, LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
											
											Clear_Any_Objective_Text()
											bSkipUpdateDelay = TRUE
										ENDIF
										
										IF (GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PlayerPedToUse)) = PANTO) 
										AND Has_This_MP_Objective_Text_User_Created_Been_Received(tl63CustomGodText)
											EXIT
										ENDIF
									ELSE
										Clear_Any_Objective_Text()
										EXIT
									ENDIF
								ENDIF
								
								IF iLastRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]								
									IF IS_OBJECTIVE_TEXT_UPDATE_READY(OBJ_TEXT_DELAY_BS_GROUP_0, iLocalBoolCheck3, LBOOL3_UPDATE_VEHICLE_OBJECTIVE)	//IF IS_BIT_SET(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
									OR bSkipUpdateDelay									
										IF ((MC_playerBD[iObjTxtPartToUse].iVehNear != -1) OR IS_BIT_SET(iLocalboolCheck31, LBOOL31_ARE_ANY_TEAM_PLAYERS_IN_VALID_DELIVERY_VEH))
										OR ((NOT (MC_serverBD.iNumVehHighestPriorityHeld[iTeam] >= MC_serverBD.iNumvehHighestPriority[iTeam]))
										AND IS_BIT_SET(iLocalBoolCheck24, LBOOL24_IN_CREATOR_TRAILER)
										AND bAvengerHasDriver)
											#IF IS_DEBUG_BUILD
											IF bDebugPrintCustomText
												PRINTLN("[JS] [GDTEXT] - iVehNear is valid: ", MC_playerBD[iObjTxtPartToUse].iVehNear)
											ENDIF
											#ENDIF
											IF NOT AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH(iAvengerDriver) 
												#IF IS_DEBUG_BUILD
												IF bDebugPrintCustomText
													PRINTLN("[JS] [GDTEXT] - NOT AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH")
												ENDIF
												#ENDIF
												IF (( NOT ( IS_BIT_SET(MC_playerBD[ iObjTxtPartToUse ].iClientBitSet2, PBBOOL2_IN_DROP_OFF) OR IS_BIT_SET(MC_playerBD[ iObjTxtPartToUse ].iClientBitSet2, PBBOOL2_DROPPING_OFF) OR (iDeliveryVehForcingOut != -1) ) )
												OR AM_I_IN_A_BOUNCING_PLANE_OR_HELI())
													#IF IS_DEBUG_BUILD
													IF bDebugPrintCustomText
														PRINTLN("[JS] [GDTEXT] - Not in the drop off")
													ENDIF
													#ENDIF
													IF (MC_serverBD.iRequiredDeliveries[iTeam] <= 1)
													OR (MC_serverBD.iNumVehHighestPriorityHeld[iTeam] >= MC_serverBD.iRequiredDeliveries[iTeam])
														#IF IS_DEBUG_BUILD
														IF bDebugPrintCustomText
															PRINTLN("[JS] [GDTEXT] - Attempting to print tl63CustomGodText1")
														ENDIF
														#ENDIF
														IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
															bobjectivedisp=TRUE
															bcustomobjective = TRUE
														ELSE
															IF NOT IS_THIS_ROCKSTAR_MISSION_WVM_DUNE3(g_FMMC_STRUCT.iRootContentIDHash)
																bNoSecondObjective = TRUE
															ENDIF															
														ENDIF
													ENDIF
												ELSE
													#IF IS_DEBUG_BUILD
													IF bDebugPrintCustomText
														PRINTLN("[JS] [GDTEXT] - In the drop off")
													ENDIF
													#ENDIF
													IF (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iRuleBitsetThree[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]], ciBS_RULE3_ALL_IN_SEPARATE_VEHICLES) 
													AND NOT IS_BIT_SET(MC_serverBD.iAllPlayersInSeparateVehiclesBS[MC_playerBD[iObjTxtPartToUse].iteam], MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]))
													OR (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]], ciBS_RULE9_PROGRESS_GAD_AT_SAME_TIME)
													AND NOT IS_BIT_SET(MC_ServerBD.iAllPlayersInUniqueVehiclesBS[MC_playerBD[iObjTxtPartToUse].iteam], MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]))
													OR (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].iRuleBitsetTen[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]], ciBS_RULE10_PROGRESS_RULE_ALL_DRIVER_ACCEL)
													AND NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet2, PBBOOL2_DROPPING_OFF))
													AND NOT IS_BIT_SET(MC_PlayerBD[iPartToUse].iClientBitSet4, PBBOOL4_SPAWNED_IN_A_PLACED_VEHICLE)
														#IF IS_DEBUG_BUILD
														IF bDebugPrintCustomText
															PRINTLN("[JS] [GDTEXT] - Separate vehicles but not everyone is in the drop off, attempting to print tl63CustomGodText2")
														ENDIF
														#ENDIF
														IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText2)
															bobjectivedisp = TRUE
															bcustomobjective = TRUE
														ELSE
															bNoSecondObjective = TRUE
														ENDIF
													ELSE
														#IF IS_DEBUG_BUILD
														IF bDebugPrintCustomText
															PRINTLN("[JS] [GDTEXT] - Not Separate vehicles or separate vehicles and everyone is here (rule is about to progress), clearing text if shown")
														ENDIF
														#ENDIF
														IF Is_MP_Objective_Text_On_Display()
															Clear_Any_Objective_Text()
														ENDIF
														
														bobjectivedisp = TRUE
														bcustomobjective = TRUE
													ENDIF
												ENDIF
											ELSE
												IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen,ciENABLE_AVENGER_INTERIOR_ENTRY)
												AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]], ciBS_RULE9_WAIT_FOR_REMAINING_IN_MOC_TO_PROGRESS)
												AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iRuleBitsetTen[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]], ciBS_RULE10_PROGRESS_RULE_ALL_DRIVER_ACCEL)	
													#IF IS_DEBUG_BUILD
													IF bDebugPrintCustomText
														PRINTLN("[JS] [GDTEXT] - Avenger waiting for players - tl63CustomGodText1")
													ENDIF
													#ENDIF
													IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
														bobjectivedisp=TRUE
														bcustomobjective = TRUE
													ENDIF
												ELSE
													#IF IS_DEBUG_BUILD
													IF bDebugPrintCustomText
														PRINTLN("[JS] [GDTEXT] - AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH = TRUE")
													ENDIF
													#ENDIF
													bNoSecondObjective = TRUE
												ENDIF
											ENDIF
											
										// [FIX_2020_CONTROLLER] - Oh my lord.
										ELIF MC_serverBD.iNumVehHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] >= MC_serverBD.iNumvehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]
										AND ((IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]], ciBS_RULE9_WAIT_FOR_REMAINING_IN_MOC_TO_PROGRESS)
										AND (ARE_REMAINING_PLAYERS_IN_VEHICLE_INTERIOR(IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen,ciENABLE_AVENGER_INTERIOR_ENTRY)) OR IS_BIT_SET(iLocalBoolCheck24, LBOOL24_IN_CREATOR_TRAILER)))
										OR NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]], ciBS_RULE9_WAIT_FOR_REMAINING_IN_MOC_TO_PROGRESS))
										OR (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]], ciBS_RULE9_PROGRESS_GAD_AT_SAME_TIME)
										AND iUniqueVehiclesHeld[MC_playerBD[iObjTxtPartToUse].iteam] >= GET_FMMC_OBJECTIVE_SCORE_LIMIT_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iTargetScore[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]], MC_serverBD.iNumberOfPlayingPlayers[MC_playerBD[iPartToUse].iteam]) / GET_FMMC_POINTS_FOR_TEAM(MC_playerBD[iObjTxtPartToUse].iteam, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]))	
										OR (NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]], ciBS_RULE9_PROGRESS_GAD_AT_SAME_TIME)
										AND IS_THIS_ROCKSTAR_MISSION_WVM_TRAILERSMALL(g_FMMC_STRUCT.iRootContentIDHash)
										AND iUniqueVehiclesHeld[MC_playerBD[iObjTxtPartToUse].iteam] >= IMAX(FLOOR(MC_ServerBD.iNumberOfPlayingPlayers[MC_playerBD[iObjTxtPartToUse].iteam]/ 2.0), 1))
										AND NOT IS_BIT_SET(MC_PlayerBD[iPartToUse].iClientBitSet4, PBBOOL4_SPAWNED_IN_A_PLACED_VEHICLE)
											#IF IS_DEBUG_BUILD
											IF bDebugPrintCustomText
												PRINTLN("[JS] [GDTEXT] - iNumVehHighestPriorityHeld (",MC_serverBD.iNumVehHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam],") >= iNumvehHighestPriority(",MC_serverBD.iNumvehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam],")")
											ENDIF
											#ENDIF
											IF MC_serverBD.iTotalPlayingCoopPlayers[MC_playerBD[iObjTxtPartToUse].iteam] > 1
												#IF IS_DEBUG_BUILD
												IF bDebugPrintCustomText
													PRINTLN("[JS] [GDTEXT] - iTotalPlayingCoopPlayers > 1")
												ENDIF
												#ENDIF
												IF iDeliveryVehForcingOut = -1
													#IF IS_DEBUG_BUILD
													IF bDebugPrintCustomText
														PRINTLN("[JS] [GDTEXT] - Attempting to print tl63CustomGodText2")
													ENDIF
													#ENDIF
													IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText2)
														bobjectivedisp=TRUE
														bcustomobjective = TRUE
													ELSE
														#IF IS_DEBUG_BUILD
														IF bDebugPrintCustomText
															PRINTLN("[JS] [GDTEXT] - Failed to print tl63CustomGodText2")
														ENDIF
														#ENDIF
														IF IS_THIS_ROCKSTAR_MISSION_SVM_DUNE4(g_FMMC_STRUCT.iRootContentIDHash)
															bobjectivedisp=TRUE
															bcustomobjective = TRUE
														ELSE
															IF NOT IS_THIS_ROCKSTAR_MISSION_WVM_DUNE3(g_FMMC_STRUCT.iRootContentIDHash)
																bDispHelpObjective=TRUE
															ENDIF
														ENDIF
													ENDIF
												ELSE
													#IF IS_DEBUG_BUILD
													IF bDebugPrintCustomText
														PRINTLN("[JS] [GDTEXT] - iDeliveryVehForcingOut != -1 Clearing text if shown")
													ENDIF
													#ENDIF
													IF Is_MP_Objective_Text_On_Display()
														Clear_Any_Objective_Text()
													ENDIF
													
													bobjectivedisp = TRUE
													bcustomobjective = TRUE
												ENDIF
											ENDIF
										ELSE
											#IF IS_DEBUG_BUILD
											IF bDebugPrintCustomText
												PRINTLN("[JS] [GDTEXT] - iVehNear is invalid")
											ENDIF
											#ENDIF
											IF Is_MP_Objective_Text_On_Display()
											AND iclientstage = CLIENT_MISSION_STAGE_DELIVER_VEH//b* 2216433
												#IF IS_DEBUG_BUILD
												IF bDebugPrintCustomText
													PRINTLN("[JS] [GDTEXT] - Text is already shown and we are on the deliver stage")
												ENDIF
												#ENDIF
												bobjectivedisp = TRUE
												bcustomobjective = TRUE
											ENDIF
											
											IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].tl63WrongWayMessage[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]])
												IF (IS_THIS_ROCKSTAR_MISSION_WVM_TRAILERLARGE(g_FMMC_STRUCT.iRootContentIDHash)
												AND iclientstage = CLIENT_MISSION_STAGE_COLLECT_VEH)
												OR (NOT bAvengerHasDriver
												AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen,ciENABLE_AVENGER_INTERIOR_ENTRY)
												AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]], ciBS_RULE9_WAIT_FOR_REMAINING_IN_MOC_TO_PROGRESS)
												AND IS_BIT_SET(iLocalBoolCheck24, LBOOL24_IN_CREATOR_TRAILER))
													#IF IS_DEBUG_BUILD
														IF bDebugPrintCustomText
															PRINTLN("[JS] [GDTEXT] - Cover Blown/Avenger Wrong way text")
														ENDIF
													#ENDIF
													IF PRINT_CUSTOM_OBJECTIVE(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].tl63WrongWayMessage[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]])
														bobjectivedisp = TRUE
														bcustomobjective = TRUE
													ENDIF
												ENDIF
											ENDIF
											
											IF IS_THIS_ROCKSTAR_MISSION_WVM_HALFTRACK(g_FMMC_STRUCT.iRootContentIDHash)
											OR GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION() //Change to just morgue if this causes issues
												IF IS_PED_IN_ANY_VEHICLE(PlayerPedToUse)
													VEHICLE_INDEX viTemp
													viTemp = GET_VEHICLE_PED_IS_IN(PlayerPedToUse)
													IF NOT IS_VEHICLE_DRIVEABLE(viTemp)
														INT iVehID
														iVehID = IS_VEH_A_MISSION_CREATOR_VEH(viTemp)
														IF iVehID > -1
															IF MC_serverBD_4.iVehPriority[iVehID][MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
																IF Is_MP_Objective_Text_On_Display()
																	Clear_Any_Objective_Text()
																ENDIF
																#IF IS_DEBUG_BUILD
																IF bDebugPrintCustomText
																	PRINTLN("[JS] [GDTEXT] - Clearing obj text as it probably isn't valid... (HALF TRACK)")
																ENDIF
																#ENDIF
																bobjectivedisp = TRUE
																bcustomobjective = TRUE
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
											
											PRINTLN("[MMacK][ObjText] 1")
											IF Is_Player_Currently_On_MP_Contact_Mission(LocalPlayer)
											OR Is_Player_Currently_On_MP_Coop_Mission(LocalPlayer)
												PRINTLN("[MMacK][ObjText] 2")
												IF IS_BIT_SET(MC_playerBD[ iObjTxtPartToUse ].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
												OR IS_BIT_SET(MC_playerBD[ iObjTxtPartToUse ].iClientBitSet2, PBBOOL2_DROPPING_OFF)
												OR iDeliveryVehForcingOut != -1
													PRINTLN("[MMacK][ObjText] 3")
													bobjectivedisp = TRUE
													bcustomobjective = TRUE
												ENDIF
											ENDIF
										ENDIF
									ELSE
										#IF IS_DEBUG_BUILD
										IF bDebugPrintCustomText
											PRINTLN("[JS] [GDTEXT] - update not ready ")
										ENDIF
										#ENDIF
										bobjectivedisp = TRUE
										bcustomobjective = TRUE
									ENDIF
								ELSE
									#IF IS_DEBUG_BUILD
									IF bDebugPrintCustomText
										PRINTLN("[JS] [GDTEXT] - Clearing obj text as it probably isn't valid...")
									ENDIF
									#ENDIF
									IF Is_MP_Objective_Text_On_Display()
										Clear_Any_Objective_Text()
									ENDIF
									bobjectivedisp = TRUE
									bcustomobjective = TRUE
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_CONTROL_OBJ
							CASE CLIENT_MISSION_STAGE_COLLECT_OBJ
							CASE CLIENT_MISSION_STAGE_DELIVER_OBJ
								
								iTeam = MC_playerBD[iObjTxtPartToUse].iteam
								
								#IF IS_DEBUG_BUILD
								IF bDebugPrintCustomText
									PRINTLN("[PROCESS_OBJECTIVE_TEXT] iClientstage = ", iClientstage)
									PRINTLN("[PROCESS_OBJECTIVE_TEXT] iObjTxtPartToUse = ", iObjTxtPartToUse)
									PRINTLN("[PROCESS_OBJECTIVE_TEXT] iTeam = ", iTeam)
									PRINTLN("[PROCESS_OBJECTIVE_TEXT] iLastRule = ", iLastRule)
									PRINTLN("[PROCESS_OBJECTIVE_TEXT] MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam] = ", MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam])
								ENDIF
								#ENDIF
								
								IF iLastRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]
									IF MC_playerBD[iObjTxtPartToUse].iObjCarryCount > 0
									OR IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet, PBBOOL_WITH_CARRIER)
										IF (NOT (IS_BIT_SET(MC_playerBD[ iObjTxtPartToUse ].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
										OR IS_BIT_SET(MC_playerBD[ iObjTxtPartToUse ].iClientBitSet2, PBBOOL2_DROPPING_OFF)))
											IF (MC_serverBD.iRequiredDeliveries[iTeam] <= 1)
											OR (MC_serverBD.iNumObjHighestPriorityHeld[iTeam] >= MC_serverBD.iRequiredDeliveries[iTeam])
												IF MC_serverBD.iNumObjHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] > 0
												OR MC_playerBD[iObjTxtPartToUse].iObjCarryCount > 0
													IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
														bobjectivedisp = TRUE
														bcustomobjective = TRUE
													ELSE
														bNoSecondObjective = TRUE
													ENDIF
												ENDIF
											ENDIF
										ELSE
											IF Is_MP_Objective_Text_On_Display()
												Clear_Any_Objective_Text()
											ENDIF
											bobjectivedisp = TRUE
											bcustomobjective = TRUE
										ENDIF
									ELIF MC_serverBD.iNumObjHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] >= MC_serverBD.iNumobjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]
										IF IS_OBJECTIVE_TEXT_UPDATE_READY(OBJ_TEXT_DELAY_BS_GROUP_0, iLocalBoolCheck3, LBOOL3_UPDATE_OBJECT_OBJECTIVE)
											IF MC_serverBD.iTotalPlayingCoopPlayers[MC_playerBD[iObjTxtPartToUse].iteam] > 1
												IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText2)
													bobjectivedisp = TRUE
													bcustomobjective = TRUE
												ELSE
													bDispHelpObjective = TRUE
												ENDIF
											ENDIF
										ELSE
											bobjectivedisp = TRUE
											bcustomobjective = TRUE
										ENDIF
									ENDIF
								ENDIF
								
							BREAK
							
									
							CASE CLIENT_MISSION_STAGE_HACK_COMP
							CASE CLIENT_MISSION_STAGE_HACK_SAFE
							CASE CLIENT_MISSION_STAGE_BLOW_DOOR
							CASE CLIENT_MISSION_STAGE_HACK_DRILL
							
								IF iLastRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam] 
									IF MC_playerBD[iObjTxtPartToUse].iObjHacking != -1
									OR IS_BIT_SET(iLocalBoolCheck12, LBOOL12_SERVER_STILL_THINKS_IM_HACKING)
										IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
											bobjectivedisp=TRUE
											bcustomobjective = TRUE
										ELSE
											bNoSecondObjective = TRUE
										ENDIF
									ELSE
										IF MC_serverBD.iNumberOfPlayingPlayers[MC_playerBD[iObjTxtPartToUse].iteam] > 1
											
											IF ((MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1)
												AND (MC_serverBD.iNumObjHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] >= MC_serverBD.iNumobjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]))
											OR ((MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] = 1)
												AND IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_TEAM_0_IS_HACKING + MC_playerBD[iObjTxtPartToUse].iTeam))
												
												IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText2)
													bobjectivedisp=TRUE
													bcustomobjective = TRUE
												ELSE
													bDispHelpObjective=TRUE
												ENDIF
											ENDIF
											
										ENDIF
									ENDIF
								ELSE
									bobjectivedisp=TRUE
									bcustomobjective = TRUE
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_INTERACT_WITH
								IF IS_OBJECTIVE_TEXT_UPDATE_READY(OBJ_TEXT_DELAY_BS_GROUP_0, iLocalBoolCheck3, LBOOL3_UPDATE_OBJECT_OBJECTIVE)
									SWITCH sIWInfo.iInteractWith_ObjectiveTextToUse
										CASE ciObjectiveText_Primary
											IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText)
												bcustomobjective = TRUE
											ENDIF
										BREAK
										
										CASE ciObjectiveText_Secondary
											IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
												bcustomobjective = TRUE
												bobjectivedisp = TRUE
											ENDIF
										BREAK
										
										CASE ciObjectiveText_AltSecondary
											IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText2)
												bcustomobjective = TRUE
												bobjectivedisp = TRUE
											ENDIF
										BREAK
									ENDSWITCH
								ELSE
									bobjectivedisp = TRUE
									bcustomobjective = TRUE
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_HACK_FINGERPRINT_KEYPAD
								
								PRINTLN("[FingerprintKeypadHack] MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]: ", MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam])
								PRINTLN("[FingerprintKeypadHack] MC_serverBD.iNumObjHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam]: ", MC_serverBD.iNumObjHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam])
								
								IF MC_playerBD[iLocalPart].iObjHacking = -1
									IF ((MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1)
									AND (MC_serverBD.iNumObjHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] >= MC_serverBD.iNumobjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]))
									OR ((MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] = 1)
									AND IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_TEAM_0_IS_HACKING + MC_playerBD[iObjTxtPartToUse].iTeam))
									AND fkhaFingerprintKeypadHackAnimState != FKHA_STATE_LOOP_PRE_EXIT
											
										IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
											bcustomobjective = TRUE
											bobjectivedisp = TRUE
											PRINTLN("[FingerprintKeypadHack] Printing 'watch your team do the hack' objective text")
										ELSE
											PRINTLN("[FingerprintKeypadHack] PRINT_CUSTOM_OBJECTIVE returned FALSE")
										ENDIF
									ELSE
										IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText)
											bcustomobjective = TRUE
										ENDIF
									ENDIF
								ELSE
									PRINTLN("[FingerprintKeypadHack] Not doing PRINT_CUSTOM_OBJECTIVE because my iObjHacking isn't -1")
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_HACKING
								
								PRINTLN("[HackObjText] MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]: ", MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam])
								PRINTLN("[HackObjText] MC_serverBD.iNumObjHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam]: ", MC_serverBD.iNumObjHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam])
								
								IF MC_playerBD[iLocalPart].iObjHacking = -1
									IF ((MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1)
									AND (MC_serverBD.iNumObjHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] >= MC_serverBD.iNumobjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]))
									OR ((MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] = 1)
									AND IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_TEAM_0_IS_HACKING + MC_playerBD[iObjTxtPartToUse].iTeam))
											
										IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
											bcustomobjective = TRUE
											bobjectivedisp = TRUE
											PRINTLN("[TMS] Printing 'watch your team do the hack' objective text")
										ELSE
											PRINTLN("[TMS] PRINT_CUSTOM_OBJECTIVE returned FALSE")
										ENDIF
									ELSE
										IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText)
											bcustomobjective = TRUE
										ENDIF
									ENDIF
								ELSE
									PRINTLN("[TMS] Not doing PRINT_CUSTOM_OBJECTIVE because my iObjHacking isn't -1")
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_KILL_PLAYERS
								PRINTLN("CLIENT_MISSION_STAGE_KILL_PLAYERS - here 1")
								IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_POWER_PLAY(g_FMMC_STRUCT.iAdversaryModeType)
									PRINTLN("CLIENT_MISSION_STAGE_KILL_PLAYERS - Should be printing power play")
									IF PRINT_POWER_PLAY_TEAM_OBJECTIVE()
										bobjectivedisp = TRUE
										bcustomobjective = TRUE
									ENDIF
								ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_HARD_TARGET(g_FMMC_STRUCT.iAdversaryModeType)
									PRINTLN("[TMS] Overriding objective text because this is Hard Target.")
									
									//Stops other stuff from playing.
									IF NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_FIRST_HARD_TARGET_SHARD_PLAYED)
										Clear_Any_Objective_Text()
										EXIT
									ENDIF
									
									IF PRINT_HARD_TARGET_OBJECTIVE()
										bobjectivedisp = TRUE
										bcustomobjective = TRUE
									ELSE
										IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH)
											// Shouldn't show anything if we don't have a hard target.
											Clear_Any_Objective_Text()
											EXIT
										ENDIF
									ENDIF
								ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciCONDEMNED_MODE_ENABLED)
									IF PRINT_CONDEMNED_TEAM_OBJECTIVE()
										bobjectivedisp=TRUE
										bcustomobjective = TRUE
									ENDIF
								ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_POINTLESS(g_FMMC_STRUCT.iAdversaryModeType)
									IF PRINT_CUSTOM_POINTLESS_OBJECTIVE(GET_TEXT_LABEL_FOR_POINTLESS(tl63CustomGodText))
										bobjectivedisp=TRUE
										bcustomobjective = TRUE
									ENDIF
								ELSE
									IF NOT Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)
										IF PRINT_CUSTOM_COLOURED_OBJECTIVE(tl63CustomGodText,HUD_COLOUR_RED)
											bobjectivedisp=TRUE
											bcustomobjective = TRUE
										ENDIF
									ELSE
										bobjectivedisp=TRUE
										bcustomobjective = TRUE
									ENDIF
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_KILL_TEAM0
								IF NOT Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)
									IF PRINT_CUSTOM_COLOURED_TEAM_OBJECTIVE(tl63CustomGodText,0)
										bobjectivedisp=TRUE
										bcustomobjective = TRUE
									ENDIF
								ELSE
									IF Is_MP_Objective_Text_On_Display()
										Clear_Any_Objective_Text()
									ENDIF
									bobjectivedisp=TRUE
									bcustomobjective = TRUE
								ENDIF
							BREAK
							CASE CLIENT_MISSION_STAGE_KILL_TEAM1
								IF NOT Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)
									IF PRINT_CUSTOM_COLOURED_TEAM_OBJECTIVE(tl63CustomGodText,1)
										bobjectivedisp=TRUE
										bcustomobjective = TRUE
									ENDIF
								ELSE
									IF Is_MP_Objective_Text_On_Display()
										Clear_Any_Objective_Text()
									ENDIF
									bobjectivedisp=TRUE
									bcustomobjective = TRUE
								ENDIF
							BREAK
							CASE CLIENT_MISSION_STAGE_KILL_TEAM2
								IF NOT Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)
									IF PRINT_CUSTOM_COLOURED_TEAM_OBJECTIVE(tl63CustomGodText,2)
										bobjectivedisp=TRUE
										bcustomobjective = TRUE
									ENDIF
								ELSE
									IF Is_MP_Objective_Text_On_Display()
										Clear_Any_Objective_Text()
									ENDIF
									bobjectivedisp=TRUE
									bcustomobjective = TRUE								
								ENDIF
							BREAK

							CASE CLIENT_MISSION_STAGE_KILL_TEAM3
								IF NOT Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)
									IF PRINT_CUSTOM_COLOURED_TEAM_OBJECTIVE(tl63CustomGodText,3)
										bobjectivedisp=TRUE
										bcustomobjective = TRUE
									ENDIF
								ELSE
									IF Is_MP_Objective_Text_On_Display()
										Clear_Any_Objective_Text()
									ENDIF
									bobjectivedisp=TRUE
									bcustomobjective = TRUE
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_ARREST_ALL
								IF PRINT_CUSTOM_COLOURED_OBJECTIVE(tl63CustomGodText,HUD_COLOUR_RED)
									bobjectivedisp=TRUE
									bcustomobjective = TRUE
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_ARREST_TEAM0
								IF PRINT_CUSTOM_COLOURED_TEAM_OBJECTIVE(tl63CustomGodText,0)
									bobjectivedisp=TRUE
									bcustomobjective = TRUE
								ENDIF
							BREAK
							CASE CLIENT_MISSION_STAGE_ARREST_TEAM1
								IF PRINT_CUSTOM_COLOURED_TEAM_OBJECTIVE(tl63CustomGodText,1)
									bobjectivedisp=TRUE
									bcustomobjective = TRUE
								ENDIF
							BREAK
							CASE CLIENT_MISSION_STAGE_ARREST_TEAM2
								IF PRINT_CUSTOM_COLOURED_TEAM_OBJECTIVE(tl63CustomGodText,2)
									bobjectivedisp=TRUE
									bcustomobjective = TRUE
								ENDIF
							BREAK
							CASE CLIENT_MISSION_STAGE_ARREST_TEAM3
								IF PRINT_CUSTOM_COLOURED_TEAM_OBJECTIVE(tl63CustomGodText,3)
									bobjectivedisp=TRUE
									bcustomobjective = TRUE
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_GOTO_ANY_PLAYER
								IF PRINT_CUSTOM_COLOURED_OBJECTIVE(tl63CustomGodText,HUD_COLOUR_RED)
									bobjectivedisp = TRUE
									bcustomobjective = TRUE
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_GOTO_TEAM0
							CASE CLIENT_MISSION_STAGE_GOTO_TEAM1
							CASE CLIENT_MISSION_STAGE_GOTO_TEAM2
							CASE CLIENT_MISSION_STAGE_GOTO_TEAM3
								IF PRINT_CUSTOM_COLOURED_TEAM_OBJECTIVE(tl63CustomGodText, (iclientstage - CLIENT_MISSION_STAGE_GOTO_TEAM0)) // iclientstage - CLIENT_MISSION_STAGE_GOTO_TEAM0 = the team we're supposed to be going to!
									bobjectivedisp=TRUE
									bcustomobjective = TRUE
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_KILL_OBJ
								IF GANGOPS_FLOW_GET_CURRENT_GANGOPS_MISSION() != ciGANGOPS_FLOW_MISSION_SPYPLANE //Could be done for all missions when we have QA time... url:bugstar:4202254
								OR IS_OBJECTIVE_TEXT_UPDATE_READY(OBJ_TEXT_DELAY_BS_GROUP_0, iLocalBoolCheck3, LBOOL3_UPDATE_OBJECT_OBJECTIVE)
									IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText)
										bobjectivedisp=TRUE
										bcustomobjective = TRUE
										PRINTLN("[MJM] TEXT IS HERE 1")
									ENDIF
								
									PRINTLN("[MJM] TEXT IS HERE 2")
								ELSE
									PRINTLN("[JS][GDTEXT] - Volotol waiting for update.")
									bobjectivedisp = TRUE
									bcustomobjective = TRUE
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_GET_MASK
								IF NOT IS_PLAYER_SCTV(PLAYER_ID())
									IF IS_OBJECTIVE_TEXT_UPDATE_READY(OBJ_TEXT_DELAY_BS_GROUP_1, MC_playerBD[iObjTxtPartToUse].iClientBitSet, PBBOOL_WEARING_MASK, FALSE, OBJECTIVE_TEXT_DELAY_LONG)	//IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_WEARING_MASK)
										IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
											bobjectivedisp=TRUE
											bcustomobjective = TRUE
										ELSE
											bNoSecondObjective = TRUE
										ENDIF
									ENDIF
								ELSE
									bobjectivedisp=TRUE
									bcustomobjective = TRUE
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_ENTER_SCRIPTED_TURRET
								
								IF g_iInteriorTurretSeat > -1
									IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText1)
										bobjectivedisp=TRUE
										bcustomobjective = TRUE
									ELSE
										bNoSecondObjective = TRUE
									ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_PHONE_BULLSHARK
							CASE CLIENT_MISSION_STAGE_PHONE_AIRSTRIKE
								
								//Custom phone rule objective text goes here
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_PROTECT_OBJ
								IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iPartToUse].iteam].iRuleBitsetSix[MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iPartToUse].iteam]], ciBS_RULE6_RESPAWN_ON_RULE_END)
								AND (IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam) OR IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_TEAM_0_SHOULD_RESPAWN_BACK_AT_START_NOW + iTeam) OR NOT GET_TOGGLE_PAUSED_RENDERPHASES_STATUS())
									IF Is_MP_Objective_Text_On_Display()
										Clear_Any_Objective_Text()
										PRINTLN("[LM][RCC MISSION][PROCESS_OBJECTIVE_TEXT] - Clearing Objective Text as we are respawning back at the start: SBBOOL3_TEAM_0_SHOULD_RESPAWN_BACK_AT_START_NOW + ciBS_RULE6_RESPAWN_ON_RULE_END")
									ENDIF
									bobjectivedisp = FALSE
								ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciOVERTIME_WAIT_YOUR_TURN)
								AND GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()
								AND NOT IS_BIT_SET(iLocalBoolCheck22, LBOOL22_OVERTIME_DELAYED_SCORE_SHARD)
									IF iOTPartToMove > -1
									AND iOTPartToMove != iLocalPart
										PARTICIPANT_INDEX tempPart
										tempPart = INT_TO_PARTICIPANTINDEX(iOTPartToMove)
										IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
											IF PRINT_COLOURED_TEAM_OBJECTIVE_WITH_PLAYER("OT_OBJ_WAIT", MC_PlayerBD[iOTPartToMove].iteam, NETWORK_GET_PLAYER_INDEX(tempPart))
												bobjectivedisp=TRUE
												bcustomobjective = TRUE
											ENDIF
										ENDIF
									ELSE
										bcustomobjective = TRUE
									ENDIF
								ELIF IS_THIS_CURRENTLY_OVERTIME_RUMBLE()
									Clear_Any_Objective_Text()
								ELIF PRINT_CUSTOM_OBJECTIVE( tl63CustomGodText )
									bobjectivedisp = TRUE
									bcustomobjective = TRUE
								ENDIF 
							BREAK
							
							CASE CLIENT_MISSION_STAGE_SCRIPTED_CUTSCENE
								
								bNoSecondObjective = TRUE
								
								IF IS_BIT_SET( iLocalBoolCheck8, LBOOL8_skipped_to_mid_mission_cutscene_stage_5 )
								OR IS_BIT_SET( MC_playerBD[iObjTxtPartToUse].iClientBitSet2, PBBOOL2_FINISHED_CUTSCENE ) // B* 4043147
									IF PRINT_CUSTOM_OBJECTIVE( tl63CustomGodText )
										bobjectivedisp = TRUE
										bcustomobjective = TRUE
									ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_RESULTS
								#IF IS_DEBUG_BUILD
								IF bDebugPrintCustomText
									PRINTLN("[JS] [GDTEXT] - CLIENT_MISSION_STAGE_RESULTS - CUSTOM")
								ENDIF
								#ENDIF
								Clear_Any_Objective_Text()
								bobjectivedisp = TRUE
								bcustomobjective = TRUE
							BREAK
							
						ENDSWITCH
						
						
						IF Is_Player_Currently_On_MP_Versus_Mission(LocalPlayer)
							IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
								IF GET_MC_CLIENT_MISSION_STAGE(iObjTxtPartToUse) = GET_MC_CLIENT_MISSION_STAGE(iLocalPart)
									IF NOT bobjectivedisp // Don't want to call display objective text twice in the same frame, or nothing displays!
										IF PRINT_CUSTOM_COLOURED_OBJECTIVE(tl63CustomGodText,HUD_COLOUR_RED)
											bobjectivedisp=TRUE
											bcustomobjective = TRUE
										ENDIF
									ENDIF
								ELSE
									bobjectivedisp=TRUE
									bcustomobjective = TRUE
									PRINTLN("[MMacK][Black] GET_MC_CLIENT_MISSION_STAGE != SCTV & Player, drawing nothing")
								ENDIF
							ENDIF
						ENDIF
						
						#IF IS_DEBUG_BUILD
						IF bDebugPrintCustomText
							PRINTLN("[PROCESS_OBJECTIVE_TEXT] bobjectivedisp = ", bobjectivedisp)
							PRINTLN("[PROCESS_OBJECTIVE_TEXT] bDispHelpObjective = ", bobjectivedisp)
							PRINTLN("[PROCESS_OBJECTIVE_TEXT] bNoSecondObjective = ", bNoSecondObjective)
							IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].tl63WaitMessage[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]])
								PRINTLN("[PROCESS_OBJECTIVE_TEXT] tl63WaitMessage = ", g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].tl63WaitMessage[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]])
							ELSE
								PRINTLN("[PROCESS_OBJECTIVE_TEXT] tl63WaitMessage = EMPTY")
							ENDIF
						ENDIF
						#ENDIF
						
						IF iLastRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam] //add's a frame delay on rule changes
							IF NOT bobjectivedisp
							AND NOT bDispHelpObjective
							AND NOT bNoSecondObjective
								IF PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText)
									#IF IS_DEBUG_BUILD
									IF bDebugPrintCustomText
										PRINTLN("[PROCESS_OBJECTIVE_TEXT] PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText) = ", tl63CustomGodText)
									ENDIF
									#ENDIF
									bcustomobjective = TRUE
								ENDIF
							ENDIF
						ELSE
							bcustomobjective = TRUE
						ENDIF
						
					ENDIF
					IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam] < FMMC_MAX_RULES
						IF NOT (iclientstage = CLIENT_MISSION_STAGE_COLLECT_VEH
						AND AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH()
						AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].tl63WaitMessage[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]]))
						OR iclientstage = CLIENT_MISSION_STAGE_PROTECT_OBJ
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]], ciBS_RULE9_DISABLE_DEFAULT_OBJECTIVE_TEXT)
							AND WVM_FLOW_IS_CURRENT_MISSION_WVM_MISSION() //This was a hacky fix for some WVM missions
								bcustomobjective = TRUE
							ENDIF
						ENDIF
					ENDIF
					//PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT - before custom objective check = ", iPartToUse)
					IF (NOT bcustomobjective)
					AND iLastRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]
						//PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_TEXT - after custom objective check iclientstage= ", iclientstage)
						
						SWITCH iclientstage
							
							CASE CLIENT_MISSION_STAGE_GOTO_LOC
								
								IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH)
									
									IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TOUR_DE_FORCE(g_FMMC_STRUCT.iAdversaryModeType)
										IF IS_BIT_SET(MC_serverbd.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
											PRINT_TOUR_DE_FORCE_OBJECTIVE()
										ENDIF
									ELSE
										
										IF NOT IS_PED_IN_REQUIRED_LOCATION_VEHICLE(GET_PLAYER_PED(ObjTxtPlayerToUse), MC_playerBD[iObjTxtPartToUse].iteam, MC_playerBD[iObjTxtPartToUse].iCurrentLoc)
											PRINT_NORMAL_OBJECTIVE("MC_WRG_VEH")
										ELSE
											IF MC_playerBD[iObjTxtPartToUse].iCurrentLoc = -1
												IF IS_BIT_SET(iLocalBoolCheck2,LBOOL2_WHOLE_UPDATE_DONE)
												AND NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iObjTxtPartToUse].iteam)
													IF MC_serverBD.iNumLocHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
														PRINT_NORMAL_OBJECTIVE("MC_GOTO_LOCS")
													ELSE
														PRINT_NORMAL_OBJECTIVE("MC_GOTO_LOC")
													ENDIF
												ENDIF
											ELSE
												IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[MC_playerBD[iObjTxtPartToUse].iCurrentLoc].iWholeTeamAtLocation[MC_playerBD[iObjTxtPartToUse].iteam] != ciGOTO_LOCATION_INDIVIDUAL
													IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[MC_playerBD[iObjTxtPartToUse].iCurrentLoc].iWholeTeamAtLocation[MC_playerBD[iObjTxtPartToUse].iteam] != ciGOTO_LOCATION_WHOLE_TEAM
													OR MC_serverBD.iNumberOfPlayingPlayers[MC_playerBD[iObjTxtPartToUse].iteam] > 1
														IF IS_OBJECTIVE_TEXT_UPDATE_READY(OBJ_TEXT_DELAY_BS_GROUP_0, iLocalBoolCheck3, LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
															IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[MC_playerBD[iObjTxtPartToUse].iCurrentLoc].fSecondaryRadius = 0
															OR AM_I_IN_SECONDARY_LOCATE_IF_I_NEED_TO_BE(MC_playerBD[iObjTxtPartToUse].iCurrentLoc)
																PRINT_NORMAL_OBJECTIVE("MC_WAIT_T")
															ELSE
																IF MC_serverBD.iNumLocHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
																	PRINT_NORMAL_OBJECTIVE("MC_GOTO_LOCS")
																ELSE
																	PRINT_NORMAL_OBJECTIVE("MC_GOTO_LOC")
																ENDIF
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_GOTO_PED
							
								IF NOT ( Is_Player_Currently_On_MP_Heist(LocalPlayer) OR Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer) )
									IF MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
										PRINT_NORMAL_OBJECTIVE("MC_GOTO_PEDS")
									ELSE
										PRINT_NORMAL_OBJECTIVE("MC_GOTO_PED")
									ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_CHARM_PED
							
								IF MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
									IF MC_playerBD[iPartToUse].iPedCharming !=-1
										PRINT_NORMAL_OBJECTIVE("MC_CHARMPED")
									ELSE
										PRINT_NORMAL_OBJECTIVE("MC_GOTO_PEDS")
									ENDIF
								ELSE
									IF MC_playerBD[iPartToUse].iPedCharming !=-1
										PRINT_NORMAL_OBJECTIVE("MC_CHARMPED")
									ELSE
										PRINT_NORMAL_OBJECTIVE("MC_GOTO_PED")
									ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_CROWD_CONTROL
							
								IF MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
									IF MC_playerBD[iObjTxtPartToUse].iPedCrowdControlBits[0] != 0
									OR MC_playerBD[iObjTxtPartToUse].iPedCrowdControlBits[1] != 0
										PRINT_NORMAL_OBJECTIVE("MC_CRDCNTS")
									ELSE
										PRINT_NORMAL_OBJECTIVE("MC_GOTO_CRDS")
									ENDIF
								ELIF (MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] = 1)
									IF MC_playerBD[iObjTxtPartToUse].iPedCrowdControlBits[0] != 0
									OR MC_playerBD[iObjTxtPartToUse].iPedCrowdControlBits[1] != 0
										PRINT_NORMAL_OBJECTIVE("MC_CRDCNTP")
									ELSE
										PRINT_NORMAL_OBJECTIVE("MC_GOTO_CRDP")
									ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_LEAVE_LOC
								
								IF IS_BIT_SET(iLocalBoolCheck6,LBOOL6_IN_MY_PRIORITY_LEAVE_LOC)
									PRINT_NORMAL_OBJECTIVE("MC_LVELOC")
								ELSE
									IF IS_BIT_SET(MC_serverBD.iServerBitSet2,SBBOOL2_TEAM_0_NEEDS_TO_WAIT_FOR_LEAVE_LOC + MC_playerBD[iObjTxtPartToUse].iteam)
										IF IS_BIT_SET(iLocalBoolCheck2,LBOOL2_WHOLE_UPDATE_DONE)
											PRINT_NORMAL_OBJECTIVE("MC_WAIT_TL")
										ENDIF
									ENDIF
								ENDIF
							
							BREAK
							
							CASE CLIENT_MISSION_STAGE_GOTO_VEH
								
								IF IS_OBJECTIVE_TEXT_UPDATE_READY(OBJ_TEXT_DELAY_BS_GROUP_0, iLocalBoolCheck3, LBOOL3_UPDATE_VEHICLE_OBJECTIVE)	//Potentially remove if causing issues on more missions
								OR IS_THIS_ROCKSTAR_MISSION_WVM_OPPRESSOR_2(g_FMMC_STRUCT.iRootContentIDHash)
									#IF IS_DEBUG_BUILD
									IF bDebugPrintCustomText
										PRINTLN("[JS] [GDTEXT] - GOTO VEH DEFAULT - Ready to update")
									ENDIF
									#ENDIF
									IF MC_playerBD[iObjTxtPartToUse].iVehNear != -1
										#IF IS_DEBUG_BUILD
										IF bDebugPrintCustomText
											PRINTLN("[JS] [GDTEXT] - GOTO VEH DEFAULT - iVehNear OK")
										ENDIF
										#ENDIF
										BOOL bCheckOnRule // To let us use GET_VEHICLE_GOTO_TEAMS here
										IF (GET_VEHICLE_GOTO_TEAMS(MC_playerBD[iObjTxtPartToUse].iteam,MC_playerBD[iObjTxtPartToUse].iVehNear,bCheckOnRule) > 0)
											#IF IS_DEBUG_BUILD
											IF bDebugPrintCustomText
												PRINTLN("[JS] [GDTEXT] - GOTO VEH DEFAULT - GET_VEHICLE_GOTO_TEAMS OK")
											ENDIF
											#ENDIF
											PRINT_NORMAL_OBJECTIVE("MC_VEH_WT")
										ENDIF
									ELSE
										IF MC_serverBD.iNumVehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
											#IF IS_DEBUG_BUILD
											IF bDebugPrintCustomText
												PRINTLN("[JS] [GDTEXT] - GOTO VEH DEFAULT - PLURAL")
											ENDIF
											#ENDIF
											PRINT_NORMAL_OBJECTIVE("MC_GOTO_VEHS")
										ELSE
											#IF IS_DEBUG_BUILD
											IF bDebugPrintCustomText
												PRINTLN("[JS] [GDTEXT] - GOTO VEH DEFAULT - SINGULAR")
											ENDIF
											#ENDIF
											PRINT_NORMAL_OBJECTIVE("MC_GOTO_VEH")
										ENDIF
									ENDIF
								#IF IS_DEBUG_BUILD
								ELSE
									IF bDebugPrintCustomText
										PRINTLN("[JS] [GDTEXT] - GOTO VEH DEFAULT - NOT ready to update")
									ENDIF
								#ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_GOTO_OBJ
							
								IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
									PRINT_NORMAL_OBJECTIVE("MC_GOTO_OBJS")
								ELSE
									PRINT_NORMAL_OBJECTIVE("MC_GOTO_OBJ")
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_CONTROL_AREA
							
								IF MC_serverBD.iNumLocHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
									IF MC_playerBD[iObjTxtPartToUse].iCurrentLoc =-1
										PRINT_NORMAL_OBJECTIVE("MC_GO_AREAS")
									ELSE
										PRINT_NORMAL_OBJECTIVE("MC_CON_AREAS")
									ENDIF
								ELSE
									IF MC_playerBD[iPartToUse].iCurrentLoc =-1
										PRINT_NORMAL_OBJECTIVE("MC_GO_AREA")
									ELSE
										PRINT_NORMAL_OBJECTIVE("MC_CON_AREA")
									ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_CONTROL_PED
							
								IF MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
									IF (MC_playerBD[iObjTxtPartToUse].iPedCarryCount = 0
									AND NOT IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER)
									AND NOT IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet,PBBOOL_IN_GROUP_PED_VEH))
										IF IS_OBJECTIVE_TEXT_UPDATE_READY(OBJ_TEXT_DELAY_BS_GROUP_0, iLocalBoolCheck3, LBOOL3_UPDATE_PED_OBJECTIVE)	//IS_BIT_SET(iLocalBoolCheck3,LBOOL3_UPDATE_PED_OBJECTIVE)
											IF MC_serverBD.iNumPedHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] < MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] 
												PRINT_NORMAL_OBJECTIVE("MC_CON_PEDS")
											ELSE					
												PRINT_NORMAL_OBJECTIVE("MC_HCON_PEDS")	
											ENDIF
										ENDIF
									ELSE
										IF MC_playerBD[iObjTxtPartToUse].iPedCarryCount = 1
											PRINT_NORMAL_OBJECTIVE("MC_MCON_PED")	
										ELSE
											PRINT_NORMAL_OBJECTIVE("MC_MCON_PEDS")	
										ENDIF
									ENDIF
								ELIF  MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] = 1
									IF (MC_playerBD[iObjTxtPartToUse].iPedCarryCount = 0
									AND NOT IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER)
									AND NOT IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet,PBBOOL_IN_GROUP_PED_VEH))
										IF IS_OBJECTIVE_TEXT_UPDATE_READY(OBJ_TEXT_DELAY_BS_GROUP_0, iLocalBoolCheck3, LBOOL3_UPDATE_PED_OBJECTIVE)	//IS_BIT_SET(iLocalBoolCheck3,LBOOL3_UPDATE_PED_OBJECTIVE)
											IF MC_serverBD.iNumPedHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] < MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] 
												PRINT_NORMAL_OBJECTIVE("MC_CON_PED")	
											ELSE
												PRINT_NORMAL_OBJECTIVE("MC_HCON_PED")	
											ENDIF
										ENDIF
									ELSE
										PRINT_NORMAL_OBJECTIVE("MC_MCON_PED")	
									ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_CONTROL_VEH
								
								
								IF IS_OBJECTIVE_TEXT_UPDATE_READY(OBJ_TEXT_DELAY_BS_GROUP_0, iLocalBoolCheck3, LBOOL3_UPDATE_VEHICLE_OBJECTIVE)	//IS_BIT_SET(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
								OR IS_LOCAL_PLAYER_ANY_SPECTATOR()
									IF MC_serverBD.iNumVehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
										IF MC_playerBD[iObjTxtPartToUse].iVehNear = -1
										AND NOT IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER)
											IF MC_serverBD.iNumVehHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] < MC_serverBD.iNumVehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]
												//Capture the vehicles
												PRINT_NORMAL_OBJECTIVE("MC_CON_VEHS")
											ELSE
												//Help your team
												PRINT_NORMAL_OBJECTIVE("MC_HCON_VEHS")
											ENDIF
										ELSE
											//Maintain control
											PRINT_NORMAL_OBJECTIVE("MC_MCON_VEHS")
										ENDIF
									ELIF MC_serverBD.iNumVehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] = 1
										IF MC_playerBD[iPartToUse].iVehNear = -1
										AND NOT IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER)
											IF MC_serverBD.iNumVehHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] < MC_serverBD.iNumVehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]
												//Capture the vehicle
												PRINT_NORMAL_OBJECTIVE("MC_CON_VEH")
											ELSE
												//Help your team
												PRINT_NORMAL_OBJECTIVE("MC_HCON_VEH")
											ENDIF
										ELSE
											//Maintain control
											PRINT_NORMAL_OBJECTIVE("MC_MCON_VEH")
										ENDIF
									ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_CONTROL_OBJ
							
								IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
									IF MC_playerBD[iObjTxtPartToUse].iObjCarryCount = 0
									AND NOT IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER)
										IF IS_OBJECTIVE_TEXT_UPDATE_READY(OBJ_TEXT_DELAY_BS_GROUP_0, iLocalBoolCheck3, LBOOL3_UPDATE_OBJECT_OBJECTIVE)	//IS_BIT_SET(iLocalBoolCheck3,LBOOL3_UPDATE_OBJECT_OBJECTIVE)
											IF MC_serverBD.iNumObjHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] < MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]
												PRINT_NORMAL_OBJECTIVE("MC_CON_OBJS")
											ELSE
												PRINT_NORMAL_OBJECTIVE("MC_HCON_OBJS")
											ENDIF
										ENDIF
									ELSE
										IF MC_playerBD[iObjTxtPartToUse].iObjCarryCount = 1
											PRINT_NORMAL_OBJECTIVE("MC_MCON_OBJ")
										ELSE
											PRINT_NORMAL_OBJECTIVE("MC_MCON_OBJS")
										ENDIF
									ENDIF
								ELIF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] = 1
									IF MC_playerBD[iObjTxtPartToUse].iObjCarryCount = 0
									AND NOT IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER)
										IF IS_OBJECTIVE_TEXT_UPDATE_READY(OBJ_TEXT_DELAY_BS_GROUP_0, iLocalBoolCheck3, LBOOL3_UPDATE_OBJECT_OBJECTIVE)	//IS_BIT_SET(iLocalBoolCheck3,LBOOL3_UPDATE_OBJECT_OBJECTIVE)
											IF MC_serverBD.iNumObjHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] < MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]
												PRINT_NORMAL_OBJECTIVE("MC_CON_OBJ")
											ELSE
												PRINT_NORMAL_OBJECTIVE("MC_HCON_OBJ")
											ENDIF
										ENDIF
									ELSE
										PRINT_NORMAL_OBJECTIVE("MC_MCON_OBJ")
									ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_KILL_PED								
								IF MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
									FOR i = 0 TO FMMC_MAX_PEDS-1
										IF DOES_BLIP_EXIST(biPedBlip[i]) //only show kill text if something to kill											
											PRINT_NORMAL_OBJECTIVE("MC_K_PEDS")
											i = FMMC_MAX_PEDS
											bFoundAiBlip = TRUE
										ENDIF
									ENDFOR
									
									IF NOT bFoundAiBlip
										IF Is_MP_Objective_Text_On_Display()
											Clear_Any_Objective_Text()
										ENDIF
									ENDIF
								ELSE
									IF MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 0
										//IF IS_OBJECTIVE_TEXT_UPDATE_READY(OBJ_TEXT_DELAY_BS_GROUP_0, iLocalBoolCheck3, LBOOL3_UPDATE_OBJECT_OBJECTIVE)
											PRINT_NORMAL_OBJECTIVE("MC_K_PED")
										//ENDIF
									ENDIF
								ENDIF
								
							BREAK
															
							CASE CLIENT_MISSION_STAGE_KILL_VEH
							
								IF MC_serverBD.iNumVehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
									PRINT_NORMAL_OBJECTIVE("MC_K_VEHS")
								ELSE
									PRINT_NORMAL_OBJECTIVE("MC_K_VEH")
								ENDIF
								
							BREAK 
							
							CASE CLIENT_MISSION_STAGE_KILL_OBJ
								IF iLastRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam] //bit of a hack to sync this
									IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
										PRINT_NORMAL_OBJECTIVE("MC_K_OBJS")
									ELSE
										PRINT_NORMAL_OBJECTIVE("MC_K_OBJ")
									ENDIF
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_PROTECT_PED
							
								IF MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
									PRINT_NORMAL_OBJECTIVE("MC_P_PEDS")
								ELSE
									PRINT_NORMAL_OBJECTIVE("MC_P_PED")
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_PROTECT_VEH
							
								IF MC_serverBD.iNumVehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
									PRINT_NORMAL_OBJECTIVE("MC_P_VEHS")
								ELSE
									PRINT_NORMAL_OBJECTIVE("MC_P_VEH")
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_PROTECT_OBJ
								IF (IS_THIS_ROCKSTAR_MISSION_WVM_HALFTRACK(g_FMMC_STRUCT.iRootContentIDHash)
								AND IS_BIT_SET(iLocalBoolCheck2,LBOOL2_WHOLE_UPDATE_DONE))
								OR NOT IS_THIS_ROCKSTAR_MISSION_WVM_HALFTRACK(g_FMMC_STRUCT.iRootContentIDHash)
									BOOL bProtectHold
									IF Is_Player_Currently_On_MP_Heist(LocalPlayer) OR Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
									OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_SUMO(g_FMMC_STRUCT.iRootContentIdHash)
										IF IS_LOCAL_PLAYER_ANY_SPECTATOR() //stops SCTV displaying hold rules
											IF NOT IS_STRING_NULL_OR_EMPTY( tl63CustomGodText )
												IF PRINT_CUSTOM_OBJECTIVE( tl63CustomGodText )
													bProtectHold = TRUE
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									IF iLastRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam] AND NOT bProtectHold
										IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
											PRINT_NORMAL_OBJECTIVE("MC_P_OBJS")
										ELSE
											PRINT_NORMAL_OBJECTIVE("MC_P_OBJ")
										ENDIF
									ENDIF
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_COLLECT_PED
								
								IF MC_serverBD.iNumPedHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] < MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]
									IF MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
										PRINT_NORMAL_OBJECTIVE("MC_C_PEDS")
									ELSE
										PRINT_NORMAL_OBJECTIVE("MC_C_PED")
									ENDIF
								ELSE
									IF MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 0
										IF IS_OBJECTIVE_TEXT_UPDATE_READY(OBJ_TEXT_DELAY_BS_GROUP_0, iLocalBoolCheck3, LBOOL3_UPDATE_PED_OBJECTIVE)	//IS_BIT_SET(iLocalBoolCheck3,LBOOL3_UPDATE_PED_OBJECTIVE)
										AND MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] < FMMC_MAX_RULES
											//Differentiate between dropping off at a locate or at a vehicle:
											IF (g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iDropOffType[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]] != ciFMMC_DROP_OFF_TYPE_VEHICLE)
											OR (g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iDropOff_Vehicle[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]] = -1)
												
												IF MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
													PRINT_NORMAL_OBJECTIVE("MC_HD_PEDS")
												ELSE
													PRINT_NORMAL_OBJECTIVE("MC_HD_PED")
												ENDIF
												
											ELSE
												
												IF MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
													PRINT_NORMAL_OBJECTIVE("MC_HD_PEDS_V")
												ELSE
													PRINT_NORMAL_OBJECTIVE("MC_HD_PED_V")
												ENDIF
												
											ENDIF
											
										ENDIF
									ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_DELIVER_PED
							
								IF iPartEscorting != -1
									IF IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER) AND MC_playerBD[iPartEscorting].iPedCarryCount > 1
										bPlural = TRUE
									ENDIF
								ENDIF
								IF MC_playerBD[iObjTxtPartToUse].iPedCarryCount > 1
									bPlural = TRUE
								ENDIF
								
								IF NOT (IS_BIT_SET(MC_playerBD[ iObjTxtPartToUse ].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
										OR IS_BIT_SET(MC_playerBD[ iObjTxtPartToUse ].iClientBitSet2, PBBOOL2_DROPPING_OFF))
								AND MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] < FMMC_MAX_RULES
									IF IS_OBJECTIVE_TEXT_UPDATE_READY(OBJ_TEXT_DELAY_BS_GROUP_0, iLocalBoolCheck3, LBOOL3_UPDATE_PED_OBJECTIVE)	//IS_BIT_SET(iLocalBoolCheck3,LBOOL3_UPDATE_PED_OBJECTIVE)
										//Differentiate between dropping off at a locate or at a vehicle:
										IF (g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iDropOffType[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]] != ciFMMC_DROP_OFF_TYPE_VEHICLE)
										OR (g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iDropOff_Vehicle[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]] = -1)
											
											IF bPlural
												PRINT_NORMAL_OBJECTIVE("MC_D_PEDS")
											ELSE
												PRINT_NORMAL_OBJECTIVE("MC_D_PED")
											ENDIF
											
										ELSE
										
											IF bPlural
												PRINT_NORMAL_OBJECTIVE("MC_D_PEDS_V")
											ELSE
												PRINT_NORMAL_OBJECTIVE("MC_D_PED_V")
											ENDIF
											
										ENDIF
									ENDIF
								ELSE
									IF Is_MP_Objective_Text_On_Display()
										Clear_Any_Objective_Text()
									ENDIF
								ENDIF

							BREAK

							
							CASE CLIENT_MISSION_STAGE_COLLECT_VEH

								IF IS_OBJECTIVE_TEXT_UPDATE_READY(OBJ_TEXT_DELAY_BS_GROUP_0, iLocalBoolCheck3, LBOOL3_UPDATE_VEHICLE_OBJECTIVE)	//IS_BIT_SET(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
									IF AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH(iAvengerDriver)
										IF (IS_THIS_ROCKSTAR_MISSION_WVM_APC(g_FMMC_STRUCT.iRootContentIDHash)
										AND IS_BIT_SET(iLocalBoolCheck2,LBOOL2_WHOLE_UPDATE_DONE))
										OR NOT IS_THIS_ROCKSTAR_MISSION_WVM_APC(g_FMMC_STRUCT.iRootContentIDHash)
											IF (((IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].iRuleBitsetThree[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]], ciBS_RULE3_ALL_IN_SEPARATE_VEHICLES) 
											OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]], ciBS_RULE9_PROGRESS_GAD_AT_SAME_TIME))
											AND IS_BIT_SET(iLocalBoolCheck2,LBOOL2_WHOLE_UPDATE_DONE))
											OR NOT (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].iRuleBitsetThree[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]], ciBS_RULE3_ALL_IN_SEPARATE_VEHICLES) OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]], ciBS_RULE9_PROGRESS_GAD_AT_SAME_TIME)))
												IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].tl63WaitMessage[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]])
													TEXT_LABEL_63 tlWaitText
													tlWaitText = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].tl63WaitMessage[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]]
													
													IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[iRule], ciBS_RULE8_OVERRIDE_FRIENDLY_WITH_GANG)
														INT iR, iG, iB, iA
														GET_HUD_COLOUR(GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam,PLAYER_ID()),iR, iG, iB,iA)
														SET_SCRIPT_VARIABLE_HUD_COLOUR(iR, iG, iB,iA)
														PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH - PRE change: ", tlWaitText)
														REPLACE_STRING_IN_STRING(tlWaitText,"~f~", "~v~")
														PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH - POST change: ", tlWaitText, " ~v~ colour: ",iR,", ", iG,", ", iB,", ",iA)
													ENDIF
													
													PRINT_CUSTOM_OBJECTIVE(tlWaitText)
												ELSE
													IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].tl23ObjSingular[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]])
													AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].iRuleBitsetFour[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]], ciBS_RULE4_ALT_TEAM_TEXT)
														IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iRuleBitsetTwo[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]], ciBS_RULE2_BOARD_VEHICLE) 
															PRINT_OBJECTIVE_WITH_CUSTOM_STRING("MC_WTCLBRD",g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].tl23ObjSingular[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]])
														ELSE
															PRINT_OBJECTIVE_WITH_CUSTOM_STRING("MC_WTCLCUST",g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].tl23ObjSingular[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]])
														ENDIF
													ELSE
														IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iTeam].iRuleBitsetFour[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]], ciBS_RULE4_ALT_TEAM_TEXT)
															PRINT_NORMAL_OBJECTIVE("MC_WTCLPRT")
														ELSE
															PRINT_NORMAL_OBJECTIVE("MC_WTCL")
														ENDIF
													ENDIF
												ENDIF
												
												IF DOES_BLIP_EXIST(DeliveryBlip)
													REMOVE_DELIVERY_BLIP()
												ENDIF
											ENDIF
										ENDIF
										
									ELSE

										IF MC_serverBD.iNumVehHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] < MC_serverBD.iNumVehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]
											IF MC_serverBD.iNumVehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
												PRINT_NORMAL_OBJECTIVE("MC_C_VEHS")
											ELSE
												//Collect the vehicle
												PRINT_NORMAL_OBJECTIVE("MC_C_VEH")
											ENDIF
										ELSE
											IF MC_serverBD.iNumVehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 0
											
												IF Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer)
													IF MC_serverBD.iNumVehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
														PRINT_NORMAL_OBJECTIVE("CTF_HD_VEHS")
													ELSE
														PRINT_NORMAL_OBJECTIVE("CTF_HD_VEH")
													ENDIF
												ELSE
													IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] < FMMC_MAX_RULES
													AND ((IS_THIS_ROCKSTAR_MISSION_SVM_BOXVILLE5(g_FMMC_STRUCT.iRootContentIDHash) AND IS_BIT_SET(iLocalBoolCheck2,LBOOL2_WHOLE_UPDATE_DONE))
													OR NOT IS_THIS_ROCKSTAR_MISSION_SVM_BOXVILLE5(g_FMMC_STRUCT.iRootContentIDHash))
														//Differentiate between dropping off at a locate or at a vehicle:
														IF (g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iDropOffType[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]] != ciFMMC_DROP_OFF_TYPE_VEHICLE)
														OR (g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iDropOff_Vehicle[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]] = -1)
															
															IF MC_serverBD.iNumVehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
																//Help your team deliver the vehicles
																PRINT_NORMAL_OBJECTIVE("MC_HD_VEHS")
															ELSE
																//Help your team deliver the vehicle
																IF NOT (IS_THIS_ROCKSTAR_MISSION_WVM_HALFTRACK(g_FMMC_STRUCT.iRootContentIDHash) AND NOT IS_BIT_SET(iLocalBoolCheck2, LBOOL2_WHOLE_UPDATE_DONE))
																	PRINT_NORMAL_OBJECTIVE("MC_HD_VEH")
																ENDIF
															ENDIF
															
														ELSE
															
															IF MC_serverBD.iNumVehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
																PRINT_NORMAL_OBJECTIVE("MC_HD_VEHS_V")
															ELSE
																PRINT_NORMAL_OBJECTIVE("MC_HD_VEH_V")
															ENDIF
															
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_DELIVER_VEH
								IF IS_OBJECTIVE_TEXT_UPDATE_READY(OBJ_TEXT_DELAY_BS_GROUP_0, iLocalBoolCheck3, LBOOL3_UPDATE_VEHICLE_OBJECTIVE)	//IS_BIT_SET(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
									IF NOT ( IS_BIT_SET(MC_playerBD[ iObjTxtPartToUse ].iClientBitSet2, PBBOOL2_IN_DROP_OFF) OR IS_BIT_SET(MC_playerBD[ iObjTxtPartToUse ].iClientBitSet2, PBBOOL2_DROPPING_OFF) OR (iDeliveryVehForcingOut != -1) )
										IF Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer)
											IF bPlural
												PRINT_NORMAL_OBJECTIVE("CTF_D_VEHS") //This text doesn't exist, but the player can't have more than one car
											ELSE
												PRINT_NORMAL_OBJECTIVE("CTF_D_VEH")
											ENDIF
										ELSE
											IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] < FMMC_MAX_RULES
												//Differentiate between dropping off at a locate or at a vehicle:
												IF (g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iDropOffType[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]] != ciFMMC_DROP_OFF_TYPE_VEHICLE)
												OR (g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iDropOff_Vehicle[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]] = -1)
													
													IF bPlural
														PRINT_NORMAL_OBJECTIVE("MC_D_VEHS") //This text doesn't exist, but the player can't have more than one car
													ELSE
														//Deliver the vehicle
														PRINT_NORMAL_OBJECTIVE("MC_D_VEH")
													ENDIF
													
												ELSE
													
													IF bPlural
														PRINT_NORMAL_OBJECTIVE("MC_D_VEHS_V")
													ELSE
														PRINT_NORMAL_OBJECTIVE("MC_D_VEH_V")
													ENDIF
													
												ENDIF
											ENDIF								
										ENDIF
									ELSE
										IF Is_MP_Objective_Text_On_Display()
											Clear_Any_Objective_Text()
										ENDIF
									ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_COLLECT_OBJ
							
								IF MC_serverBD.iNumObjHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] < MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] 
									IF IS_BIT_SET(iLocalBoolCheck3,LBOOL3_CTF_OBJECT_IS_BAG)
										IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
											PRINT_NORMAL_OBJECTIVE("CTF_CBAGS")
										ELSE
											PRINT_NORMAL_OBJECTIVE("CTF_CBAG")
										ENDIF
									ELIF IS_BIT_SET(iLocalBoolCheck3,LBOOL3_CTF_OBJECT_IS_CASE)
										IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
											PRINT_NORMAL_OBJECTIVE("CTF_CCASES")
										ELSE
											PRINT_NORMAL_OBJECTIVE("CTF_CCASE")
										ENDIF
									ELIF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_CTF_OBJECT_IS_DRUGS)
										IF bPlural
											PRINT_NORMAL_OBJECTIVE("CTF_CDRGS")
										ELSE
											PRINT_NORMAL_OBJECTIVE("CTF_CDRG")
										ENDIF
									ELIF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_CTF_OBJECT_IS_LAPTOP)
										IF bPlural
											PRINT_NORMAL_OBJECTIVE("CTF_CLAPS")
										ELSE
											PRINT_NORMAL_OBJECTIVE("CTF_CLAP")
										ENDIF
									ELSE
										IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
											PRINT_NORMAL_OBJECTIVE("MC_C_OBJS")
										ELSE
											PRINT_NORMAL_OBJECTIVE("MC_C_OBJ")
										ENDIF
									ENDIF
								ELSE
									IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 0
										IF IS_OBJECTIVE_TEXT_UPDATE_READY(OBJ_TEXT_DELAY_BS_GROUP_0, iLocalBoolCheck3, LBOOL3_UPDATE_OBJECT_OBJECTIVE)	//IS_BIT_SET(iLocalBoolCheck3,LBOOL3_UPDATE_OBJECT_OBJECTIVE)
										AND MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] < FMMC_MAX_RULES
											//Differentiate between dropping off at a locate or at a vehicle:
											IF (g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iDropOffType[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]] != ciFMMC_DROP_OFF_TYPE_VEHICLE)
											OR (g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iDropOff_Vehicle[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]] = -1)
												
												IF IS_BIT_SET(iLocalBoolCheck3,LBOOL3_CTF_OBJECT_IS_BAG)
													IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
														PRINT_NORMAL_OBJECTIVE("CTF_HBAGS")
													ELSE
														PRINT_NORMAL_OBJECTIVE("CTF_HBAG")
													ENDIF
												ELIF IS_BIT_SET(iLocalBoolCheck3,LBOOL3_CTF_OBJECT_IS_CASE)
													IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
														PRINT_NORMAL_OBJECTIVE("CTF_HCASES")
													ELSE
														PRINT_NORMAL_OBJECTIVE("CTF_HCASE")
													ENDIF
												ELIF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_CTF_OBJECT_IS_DRUGS)
													IF bPlural
														PRINT_NORMAL_OBJECTIVE("CTF_HDRGS")
													ELSE
														PRINT_NORMAL_OBJECTIVE("CTF_HDRG")
													ENDIF
												ELIF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_CTF_OBJECT_IS_LAPTOP)
													IF bPlural
														PRINT_NORMAL_OBJECTIVE("CTF_HLAPS")
													ELSE
														PRINT_NORMAL_OBJECTIVE("CTF_HLAP")
													ENDIF
												ELSE
													IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
														PRINT_NORMAL_OBJECTIVE("MC_HD_OBJS")
													ELSE
														PRINT_NORMAL_OBJECTIVE("MC_HD_OBJ")
													ENDIF
												ENDIF
												
											ELSE
												
												IF IS_BIT_SET(iLocalBoolCheck3,LBOOL3_CTF_OBJECT_IS_BAG)
													IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
														PRINT_NORMAL_OBJECTIVE("CTF_HBAGS_V")
													ELSE
														PRINT_NORMAL_OBJECTIVE("CTF_HBAG_V")
													ENDIF
												ELIF IS_BIT_SET(iLocalBoolCheck3,LBOOL3_CTF_OBJECT_IS_CASE)
													IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
														PRINT_NORMAL_OBJECTIVE("CTF_HCASES_V")
													ELSE
														PRINT_NORMAL_OBJECTIVE("CTF_HCASE_V")
													ENDIF
												ELIF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_CTF_OBJECT_IS_DRUGS)
													IF bPlural
														PRINT_NORMAL_OBJECTIVE("CTF_HDRGS_V")
													ELSE
														PRINT_NORMAL_OBJECTIVE("CTF_HDRG_V")
													ENDIF
												ELIF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_CTF_OBJECT_IS_LAPTOP)
													IF bPlural
														PRINT_NORMAL_OBJECTIVE("CTF_HLAPS_V")
													ELSE
														PRINT_NORMAL_OBJECTIVE("CTF_HLAP_V")
													ENDIF
												ELSE
													IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
														PRINT_NORMAL_OBJECTIVE("MC_HD_OBJS_V")
													ELSE
														PRINT_NORMAL_OBJECTIVE("MC_HD_OBJ_V")
													ENDIF
												ENDIF
												
											ENDIF
											
										ENDIF
									ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_DELIVER_OBJ
							
								IF iPartEscorting !=-1
									IF IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER) AND MC_playerBD[iPartEscorting].iObjCarryCount > 1
										bPlural = TRUE
									ENDIF
								ENDIF
								IF MC_playerBD[iObjTxtPartToUse].iObjCarryCount > 1
									bPlural = TRUE
								ENDIF
							
								IF NOT (IS_BIT_SET(MC_playerBD[ iObjTxtPartToUse ].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
										OR IS_BIT_SET(MC_playerBD[ iObjTxtPartToUse ].iClientBitSet2, PBBOOL2_DROPPING_OFF))
								AND MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] < FMMC_MAX_RULES
									//Differentiate between dropping off at a locate or at a vehicle:
									IF (g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iDropOffType[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]] != ciFMMC_DROP_OFF_TYPE_VEHICLE)
									OR (g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iDropOff_Vehicle[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]] = -1)
										
										IF IS_BIT_SET(iLocalBoolCheck3,LBOOL3_CTF_OBJECT_IS_BAG)
											IF bPlural
												PRINT_NORMAL_OBJECTIVE("CTF_DBAGS")
											ELSE
												PRINT_NORMAL_OBJECTIVE("CTF_DBAG")
											ENDIF
										ELIF IS_BIT_SET(iLocalBoolCheck3,LBOOL3_CTF_OBJECT_IS_CASE)
											IF bPlural
												PRINT_NORMAL_OBJECTIVE("CTF_DCASES")
											ELSE
												PRINT_NORMAL_OBJECTIVE("CTF_DCASE")
											ENDIF
										ELIF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_CTF_OBJECT_IS_DRUGS)
											IF bPlural
												PRINT_NORMAL_OBJECTIVE("CTF_DDRGS")
											ELSE
												PRINT_NORMAL_OBJECTIVE("CTF_DDRG")
											ENDIF
										ELIF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_CTF_OBJECT_IS_LAPTOP)
											IF bPlural
												PRINT_NORMAL_OBJECTIVE("CTF_DLAPS")
											ELSE
												PRINT_NORMAL_OBJECTIVE("CTF_DLAP")
											ENDIF
										ELSE
											IF bPlural
												PRINT_NORMAL_OBJECTIVE("MC_D_OBJS")
											ELSE
												PRINT_NORMAL_OBJECTIVE("MC_D_OBJ")
											ENDIF
										ENDIF
										
									ELSE
										
										IF IS_BIT_SET(iLocalBoolCheck3,LBOOL3_CTF_OBJECT_IS_BAG)
											IF bPlural
												PRINT_NORMAL_OBJECTIVE("CTF_DBAGS_V")
											ELSE
												PRINT_NORMAL_OBJECTIVE("CTF_DBAG_V")
											ENDIF
										ELIF IS_BIT_SET(iLocalBoolCheck3,LBOOL3_CTF_OBJECT_IS_CASE)
											IF bPlural
												PRINT_NORMAL_OBJECTIVE("CTF_DCASES_V")
											ELSE
												PRINT_NORMAL_OBJECTIVE("CTF_DCASE_V")
											ENDIF
										ELIF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_CTF_OBJECT_IS_DRUGS)
											IF bPlural
												PRINT_NORMAL_OBJECTIVE("CTF_DDRGS_V")
											ELSE
												PRINT_NORMAL_OBJECTIVE("CTF_DDRG_V")
											ENDIF
										ELIF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_CTF_OBJECT_IS_LAPTOP)
											IF bPlural
												PRINT_NORMAL_OBJECTIVE("CTF_DLAPS_V")
											ELSE
												PRINT_NORMAL_OBJECTIVE("CTF_DLAP_V")
											ENDIF
										ELSE
											IF bPlural
												PRINT_NORMAL_OBJECTIVE("MC_D_OBJS_V")
											ELSE
												PRINT_NORMAL_OBJECTIVE("MC_D_OBJ_V")
											ENDIF
										ENDIF
										
									ENDIF
										
								ELSE
									IF Is_MP_Objective_Text_On_Display()
										Clear_Any_Objective_Text()
									ENDIF		
								ENDIF

							BREAK
							
							CASE CLIENT_MISSION_STAGE_KILL_PLAYERS
								PRINTLN("CLIENT_MISSION_STAGE_KILL_PLAYERS - here 2")
								IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_POWER_PLAY(g_FMMC_STRUCT.iAdversaryModeType)
									PRINTLN("CLIENT_MISSION_STAGE_KILL_PLAYERS - Should be printing power play")
									PRINT_POWER_PLAY_TEAM_OBJECTIVE()
								ELSE
									IF NOT Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)
										PRINT_COLOURED_OBJECTIVE("MC_K_ALLT",HUD_COLOUR_RED)
									ELSE
										IF Is_MP_Objective_Text_On_Display()
											Clear_Any_Objective_Text()
										ENDIF
									ENDIF
								ENDIF

							BREAK
							
							CASE CLIENT_MISSION_STAGE_KILL_TEAM0
								
								IF NOT Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)
									IF NOT IS_BIT_SET(iLocalBoolCheck2,LBOOL2_MY_TEAM_NAME_IS_CREW)
										PRINT_COLOURED_TEAM_OBJECTIVE("MC_K_T1",0)
									ELSE
										PRINT_COLOURED_TEAM_OBJECTIVE("MC_K_C1",0)
									ENDIF
								ELSE
									IF Is_MP_Objective_Text_On_Display()
										Clear_Any_Objective_Text()
									ENDIF
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_KILL_TEAM1
							
								IF NOT Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)
									IF NOT IS_BIT_SET(iLocalBoolCheck2,LBOOL2_MY_TEAM_NAME_IS_CREW)
										PRINT_COLOURED_TEAM_OBJECTIVE("MC_K_T1",1)
									ELSE
										PRINT_COLOURED_TEAM_OBJECTIVE("MC_K_C1",1)
									ENDIF
								ELSE
									IF Is_MP_Objective_Text_On_Display()
										Clear_Any_Objective_Text()
									ENDIF
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_KILL_TEAM2
							
								IF NOT Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)
									IF NOT IS_BIT_SET(iLocalBoolCheck2,LBOOL2_MY_TEAM_NAME_IS_CREW)
										PRINT_COLOURED_TEAM_OBJECTIVE("MC_K_T1",2)
									ELSE
										PRINT_COLOURED_TEAM_OBJECTIVE("MC_K_C1",2)
									ENDIF
								ELSE
									IF Is_MP_Objective_Text_On_Display()
										Clear_Any_Objective_Text()
									ENDIF
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_KILL_TEAM3
							
								IF NOT Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)
									IF NOT IS_BIT_SET(iLocalBoolCheck2,LBOOL2_MY_TEAM_NAME_IS_CREW)
										PRINT_COLOURED_TEAM_OBJECTIVE("MC_K_T1",3)
									ELSE
										PRINT_COLOURED_TEAM_OBJECTIVE("MC_K_C1",3)
									ENDIF
								ELSE
									IF Is_MP_Objective_Text_On_Display()
										Clear_Any_Objective_Text()
									ENDIF
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_ARREST_ALL
								PRINT_COLOURED_OBJECTIVE("MC_A_ALLT",HUD_COLOUR_RED)
							BREAK
							
							CASE CLIENT_MISSION_STAGE_ARREST_TEAM0
								IF NOT IS_BIT_SET(iLocalBoolCheck2,LBOOL2_MY_TEAM_NAME_IS_CREW)
									PRINT_COLOURED_TEAM_OBJECTIVE("MC_A_T1",0)
								ELSE
									PRINT_COLOURED_TEAM_OBJECTIVE("MC_A_C1",0)
								ENDIF
							BREAK
							CASE CLIENT_MISSION_STAGE_ARREST_TEAM1
								IF NOT IS_BIT_SET(iLocalBoolCheck2,LBOOL2_MY_TEAM_NAME_IS_CREW)
									PRINT_COLOURED_TEAM_OBJECTIVE("MC_A_T1",1)
								ELSE
									PRINT_COLOURED_TEAM_OBJECTIVE("MC_A_C1",1)
								ENDIF
							BREAK
							CASE CLIENT_MISSION_STAGE_ARREST_TEAM2
								IF NOT IS_BIT_SET(iLocalBoolCheck2,LBOOL2_MY_TEAM_NAME_IS_CREW)
									PRINT_COLOURED_TEAM_OBJECTIVE("MC_A_T1",2)
								ELSE
									PRINT_COLOURED_TEAM_OBJECTIVE("MC_A_C1",2)
								ENDIF
							BREAK
							CASE CLIENT_MISSION_STAGE_ARREST_TEAM3
								IF NOT IS_BIT_SET(iLocalBoolCheck2,LBOOL2_MY_TEAM_NAME_IS_CREW)
									PRINT_COLOURED_TEAM_OBJECTIVE("MC_A_T1",3)
								ELSE
									PRINT_COLOURED_TEAM_OBJECTIVE("MC_A_C1",3)
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_GOTO_ANY_PLAYER
								PRINT_COLOURED_OBJECTIVE("MC_GO_ALLT",HUD_COLOUR_RED)
							BREAK
							
							CASE CLIENT_MISSION_STAGE_GOTO_TEAM0
							CASE CLIENT_MISSION_STAGE_GOTO_TEAM1
							CASE CLIENT_MISSION_STAGE_GOTO_TEAM2
							CASE CLIENT_MISSION_STAGE_GOTO_TEAM3
								PRINT_COLOURED_TEAM_OBJECTIVE("MC_GO_TNUM", iclientstage - CLIENT_MISSION_STAGE_GOTO_TEAM0) // iclientstage - CLIENT_MISSION_STAGE_GOTO_TEAM0 = the team we're supposed to be going to!
							BREAK
							
							CASE CLIENT_MISSION_STAGE_GET_MASK
							
								IF NOT IS_PLAYER_SCTV(PLAYER_ID())
									IF NOT IS_BIT_SET(MC_playerBD[iObjTxtPartToUse].iClientBitSet,PBBOOL_WEARING_MASK)
										IF IS_BIT_SET(iLocalBoolCheck4,LBOOL4_OWN_MASK)
											PRINT_NORMAL_OBJECTIVE("MC_GET_MASK")
										ELSE
											IF NOT IS_BIT_SET(iLocalBoolCheck4,LBOOL4_IN_MASK_SHOP)
												PRINT_NORMAL_OBJECTIVE("MC_GO_MASK")
											ELSE
												PRINT_NORMAL_OBJECTIVE("MC_BUY_MASK")
											ENDIF
										ENDIF
									ELSE
										IF IS_OBJECTIVE_TEXT_UPDATE_READY(OBJ_TEXT_DELAY_BS_GROUP_1, MC_playerBD[iObjTxtPartToUse].iClientBitSet, PBBOOL_WEARING_MASK, TRUE, OBJECTIVE_TEXT_DELAY_LONG, "MC_WAIT_MASK")
											PRINT_NORMAL_OBJECTIVE("MC_WAIT_MASK")
										ENDIF
									ENDIF
								ELSE
									PRINT_NORMAL_OBJECTIVE("MC_WAIT_MASK")
								ENDIF
									
							BREAK
							
							CASE CLIENT_MISSION_STAGE_PHOTO_LOC
							
								IF MC_playerBD[iObjTxtPartToUse].iCurrentLoc = -1
									IF MC_serverBD.iNumLocHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
										PRINT_NORMAL_OBJECTIVE("MC_GOTO_LOCS")
									ELSE
										PRINT_NORMAL_OBJECTIVE("MC_GOTO_LOC")
									ENDIF
								ELSE
									PRINT_NORMAL_OBJECTIVE("MC_PH_LOC")
								ENDIF
								
							BREAK
							CASE CLIENT_MISSION_STAGE_PHOTO_PED
							
								IF MC_playerBD[iObjTxtPartToUse].iPedNear = -1
									IF (MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] < FMMC_MAX_RULES) //Just to check when we're calling this after having completed the objective, but before cleaning up
									AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iObjTxtPartToUse].iteam].iPhotoCanBeDead,MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam])
										IF MC_serverBD.iNumDeadPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] >= 1
											IF MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
												PRINT_NORMAL_OBJECTIVE("MC_GO_DPEDS")
											ELSE
												PRINT_NORMAL_OBJECTIVE("MC_GO_DPED")
											ENDIF
										ELSE								
											IF MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
												PRINT_NORMAL_OBJECTIVE("MC_K_PEDS")
											ELSE
												PRINT_NORMAL_OBJECTIVE("MC_K_PED")
											ENDIF
										ENDIF
									ELSE
										IF MC_serverBD.iNumPedHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
											PRINT_NORMAL_OBJECTIVE("MC_GOTO_PEDS")
										ELSE
											PRINT_NORMAL_OBJECTIVE("MC_GOTO_PED")
										ENDIF
									ENDIF
								ELSE
									IF NOT IS_BIT_SET(MC_serverBD.iDeadPedPhotoBitset[MC_playerBD[iObjTxtPartToUse].iteam][GET_LONG_BITSET_INDEX(MC_playerBD[iObjTxtPartToUse].iPedNear)], GET_LONG_BITSET_BIT(MC_playerBD[iObjTxtPartToUse].iPedNear))
										PRINT_NORMAL_OBJECTIVE("MC_PH_PED")
									ELSE
										IF IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[MC_playerBD[iObjTxtPartToUse].iPedNear]) 
											PRINT_NORMAL_OBJECTIVE("MC_PHD_PED")
										ELSE
											PRINT_NORMAL_OBJECTIVE("MC_K_PED")
										ENDIF
									ENDIF
										
								ENDIF
								
							BREAK
							CASE CLIENT_MISSION_STAGE_PHOTO_VEH
							
								IF MC_playerBD[iObjTxtPartToUse].iVehNear = -1
									IF MC_serverBD.iNumVehHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
										PRINT_NORMAL_OBJECTIVE("MC_GO_VEHSP")
									ELSE
										PRINT_NORMAL_OBJECTIVE("MC_GO_VEHP")
									ENDIF
								ELSE
									PRINT_NORMAL_OBJECTIVE("MC_PH_VEH")
								ENDIF

								
							BREAK
							CASE CLIENT_MISSION_STAGE_PHOTO_OBJ
								
								IF MC_playerBD[iObjTxtPartToUse].iObjNear = -1
									IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
										PRINT_NORMAL_OBJECTIVE("MC_GOTO_OBJS")
									ELSE
										PRINT_NORMAL_OBJECTIVE("MC_GOTO_OBJ")
									ENDIF
								ELSE
									PRINT_NORMAL_OBJECTIVE("MC_PH_OBJ")
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_HACK_COMP
								
								IF MC_playerBD[iObjTxtPartToUse].iObjHacking = -1
								AND NOT IS_BIT_SET(iLocalBoolCheck12, LBOOL12_SERVER_STILL_THINKS_IM_HACKING)
									
									IF ((MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1)
										AND (MC_serverBD.iNumObjHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] >= MC_serverBD.iNumobjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]))
									OR ((MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] = 1)
										AND IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_TEAM_0_IS_HACKING + MC_playerBD[iObjTxtPartToUse].iTeam))
										
										IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
											PRINT_NORMAL_OBJECTIVE("MC_GOTO_COMPS")
										ELSE
											PRINT_NORMAL_OBJECTIVE("MC_GOTO_COMP")
										ENDIF
										
									ELSE
										PRINT_NORMAL_OBJECTIVE("MC_HHACK_COMP")
									ENDIF
								ELSE
									IF MC_playerBD[iObjTxtPartToUse].iObjHacking > -1
										PRINT_NORMAL_OBJECTIVE("MC_HACK_COMP")
									ELSE
										PRINTLN("[TMS] Not doing PRINT_NORMAL_OBJECTIVE because iObjHacking is -1")
									ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_INTERACT_WITH
								PRINT_NORMAL_OBJECTIVE("MC_INTRCT")
							BREAK
							
							CASE CLIENT_MISSION_STAGE_HACKING
							
							IF ((MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1)
									AND (MC_serverBD.iNumObjHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] >= MC_serverBD.iNumobjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]))
								OR ((MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] = 1)
									AND IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_TEAM_0_IS_HACKING + MC_playerBD[iObjTxtPartToUse].iTeam))
									
									PRINT_NORMAL_OBJECTIVE("MC_TMHCK_PROT")
									PRINTLN("[TMS] Printing 'watch your team do the hack' objective text (DEFAULT)")
							ELSE
								PRINT_NORMAL_OBJECTIVE("MC_HCKING")
							ENDIF
										
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_HACK_FINGERPRINT_KEYPAD
							
								IF ((MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1)
								AND (MC_serverBD.iNumObjHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] >= MC_serverBD.iNumobjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]))
								OR ((MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] = 1)
								AND IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_TEAM_0_IS_HACKING + MC_playerBD[iObjTxtPartToUse].iTeam))
								AND fkhaFingerprintKeypadHackAnimState != FKHA_STATE_LOOP_PRE_EXIT		
									PRINT_NORMAL_OBJECTIVE("MC_TMHCK_PROT")
									PRINTLN("[FingerprintKeypadHack] Printing 'watch your team do the hack' objective text (DEFAULT)")
								ELSE
									PRINT_NORMAL_OBJECTIVE("MC_HCKING")
								ENDIF
										
							BREAK
							
							CASE CLIENT_MISSION_STAGE_HACK_SAFE
								
								IF MC_playerBD[iPartToUse].iObjHacking = -1
									IF MC_serverBD.iNumObjHighestPriorityHeld[MC_playerBD[iObjTxtPartToUse].iteam] < MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]
										IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
											PRINT_NORMAL_OBJECTIVE("MC_GOTO_SAFES")
										ELSE
											PRINT_NORMAL_OBJECTIVE("MC_GOTO_SAFE")
										ENDIF
									ELSE
										PRINT_NORMAL_OBJECTIVE("MC_HHACK_SAFE")
									ENDIF
								ELSE
									PRINT_NORMAL_OBJECTIVE("MC_HACK_SAFE")
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_HACK_SYNC_LOCK
								
								IF MC_playerBD[iPartToUse].iObjHacking = -1
								OR (MC_playerBD[iPartToUse].iObjHacking > -1 AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[MC_playerBD[iPartToUse].iObjHacking].iLegacyMiniGameBitSet, cibsLEGACY_FMMC_MINI_GAME_TYPE_HUMANE_LABS_SYNC))
									// All being used
									IF iBusySyncLockMinigamesBS >= MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam]
										PRINT_NORMAL_OBJECTIVE("MC_SYNCLOCKF")
									ELSE
										IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
											PRINT_NORMAL_OBJECTIVE("MC_SYNCLOCKS")
										ELSE
											PRINT_NORMAL_OBJECTIVE("MC_SYNCLOCK")
										ENDIF
									ENDIF
								ELSE
									PRINT_NORMAL_OBJECTIVE("MC_TSYNCLOCK")
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_BLOW_DOOR
								
								IF iLastRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]
									IF MC_playerBD[iPartToUse].iObjHacking = -1
										IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
											PRINT_NORMAL_OBJECTIVE("MC_BLOW_DOORS")
										ELSE
											PRINT_NORMAL_OBJECTIVE("MC_BLOW_DOOR")
										ENDIF
									ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_HACK_OPEN_CASE
								
								IF MC_playerBD[iObjTxtPartToUse].iObjNear = -1
									IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
										PRINT_NORMAL_OBJECTIVE("MC_GOTOCASES")
									ELSE
										PRINT_NORMAL_OBJECTIVE("MC_GOTOCASE")
									ENDIF
								ELSE
									PRINT_NORMAL_OBJECTIVE("MC_OPENCASE")
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_OPEN_CONTAINER
								
								IF MC_playerBD[iObjTxtPartToUse].iObjNear = -1
									IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
										PRINT_NORMAL_OBJECTIVE("MC_GOTOCONTS")
									ELSE
										PRINT_NORMAL_OBJECTIVE("MC_GOTOCONT")
									ENDIF
								ELSE
									PRINT_NORMAL_OBJECTIVE("MC_OPENCONT")
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_CASH_GRAB
								
								IF MC_playerBD[iObjTxtPartToUse].iObjNear = -1
									IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
										PRINT_NORMAL_OBJECTIVE("MC_GOCASHS")
									ELSE
										PRINT_NORMAL_OBJECTIVE("MC_GOCASH")
									ENDIF
								ELSE
									PRINT_NORMAL_OBJECTIVE("MC_GETCASH")
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_HACK_DRILL
							
								IF MC_playerBD[iObjTxtPartToUse].iObjHacking = -1
									IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
										PRINT_NORMAL_OBJECTIVE("MC_GO_DRILLS")
									ELSE
										PRINT_NORMAL_OBJECTIVE("MC_GO_DRILL")
									ENDIF
								ELSE
									PRINT_NORMAL_OBJECTIVE("MC_DRILL")
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_ENTER_SCRIPTED_TURRET
								
								IF g_iInteriorTurretSeat > -1
									IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
										PRINT_NORMAL_OBJECTIVE("MC_WT_TURRS")
									ELSE
										PRINT_NORMAL_OBJECTIVE("MC_WT_TURR")
									ENDIF
								ELSE
									IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iObjTxtPartToUse].iteam] > 1
										PRINT_NORMAL_OBJECTIVE("MC_EN_TURRS")
									ELSE
										PRINT_NORMAL_OBJECTIVE("MC_EN_TURR")
									ENDIF
								ENDIF
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_PHONE_BULLSHARK
							CASE CLIENT_MISSION_STAGE_PHONE_AIRSTRIKE
								
								//Default phone rule objective text goes here
								
							BREAK
							
							CASE CLIENT_MISSION_STAGE_SCRIPTED_CUTSCENE
								
								IF IS_BIT_SET( iLocalBoolCheck8, LBOOL8_skipped_to_mid_mission_cutscene_stage_5 )
									PRINT_NORMAL_OBJECTIVE( "MC_SKIP_TXT" )
								ENDIF
							BREAK
							
							CASE CLIENT_MISSION_STAGE_RESULTS
								#IF IS_DEBUG_BUILD
								IF bDebugPrintCustomText
									PRINTLN("[JS] [GDTEXT] - CLIENT_MISSION_STAGE_RESULTS - NORMAL")
								ENDIF
								#ENDIF
								Clear_Any_Objective_Text()
							BREAK
							
						ENDSWITCH
					ENDIF
					
					CLEAR_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
					CLEAR_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_PED_OBJECTIVE)
					CLEAR_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_OBJECT_OBJECTIVE)
					
					CHECK_FOR_OBJECTIVE_TEXT_UPDATE_CLEAR(OBJ_TEXT_DELAY_BS_GROUP_0, LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
					CHECK_FOR_OBJECTIVE_TEXT_UPDATE_CLEAR(OBJ_TEXT_DELAY_BS_GROUP_0, LBOOL3_UPDATE_PED_OBJECTIVE)
					CHECK_FOR_OBJECTIVE_TEXT_UPDATE_CLEAR(OBJ_TEXT_DELAY_BS_GROUP_0, LBOOL3_UPDATE_OBJECT_OBJECTIVE)
					CHECK_FOR_OBJECTIVE_TEXT_UPDATE_CLEAR(OBJ_TEXT_DELAY_BS_GROUP_1, PBBOOL_WEARING_MASK, OBJECTIVE_TEXT_DELAY_LONG)
					CHECK_FOR_OBJECTIVE_TEXT_UPDATE_CLEAR(OBJ_TEXT_DELAY_BS_GROUP_0, LBOOL28_UPDATE_LOSE_COPS_OBJECTIVE)
					CHECK_FOR_OBJECTIVE_TEXT_UPDATE_CLEAR(OBJ_TEXT_DELAY_BS_GROUP_0, LBOOL29_UPDATE_LEAVE_LOC,OBJECTIVE_TEXT_DELAY_LONG)
				ELSE
					IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iObjTxtPartToUse].iteam)
						IF Has_Any_MP_Objective_Text_Been_Received()
							Clear_Any_Objective_Text()
						ENDIF
						IF IS_THIS_ROCKSTAR_MISSION_WVM_HALFTRACK(g_FMMC_STRUCT.iRootContentIDHash)
						OR IS_THIS_ROCKSTAR_MISSION_WVM_APC(g_FMMC_STRUCT.iRootContentIDHash)
						OR IS_THIS_ROCKSTAR_MISSION_WVM_OPPRESSOR_2(g_FMMC_STRUCT.iRootContentIDHash)
							IF NOT IS_BIT_SET(iLocalBoolCheck2,LBOOL2_REQUEST_UPDATE)
								CLEAR_BIT(iLocalBoolCheck2,LBOOL2_WHOLE_UPDATE_DONE)
								SET_BIT(iLocalBoolCheck2,LBOOL2_REQUEST_UPDATE)
								PRINTLN("[JS] requesting update as rule changed (APC/Halftrack mission)")
							ENDIF
						ENDIF
					ENDIF
					PRINTLN("[RCC MISSION] [MB] Clear_Any_Objective_Text() 2")
				ENDIF
			ELSE
				//Objective blocker is set:
				PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - Objective blocker")			
				SET_BIT(iLocalBoolCheck3, LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
				PRINTLN("[JS][UOBJ - PED] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - Objective blocker")			
				SET_BIT(iLocalBoolCheck3, LBOOL3_UPDATE_PED_OBJECTIVE)
				PRINTLN("[JS][UOBJ - OBJECT] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - Objective blocker")			
				SET_BIT(iLocalBoolCheck3, LBOOL3_UPDATE_OBJECT_OBJECTIVE)
				IF Has_Any_MP_Objective_Text_Been_Received()
					Clear_Any_Objective_Text()
				ENDIF
				PRINTLN("[RCC MISSION] [MB] Clear_Any_Objective_Text() 3")
			ENDIF
		ELSE
			//I'm on the wrong side of the mission bounds:
			SET_BIT(iLocalBoolCheck3, LBOOL3_DISABLE_GPSANDHINTCAM)
			PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - wrong side of the mission bounds")		
			SET_BIT(iLocalBoolCheck3, LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
			PRINTLN("[JS][UOBJ - PED] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - wrong side of the mission bounds")		
			SET_BIT(iLocalBoolCheck3, LBOOL3_UPDATE_PED_OBJECTIVE)
			PRINTLN("[JS][UOBJ - OBJECT] - Requested Objective Update - PROCESS_OBJECTIVE_TEXT - wrong side of the mission bounds")		
			SET_BIT(iLocalBoolCheck3, LBOOL3_UPDATE_OBJECT_OBJECTIVE)
		ENDIF
	ELIF IS_BIT_SET(iLocalBoolCheck16, LBOOL16_DIVE_SCORE_SCREEN_FX_ACTIVE)
	OR (IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUGBY(g_FMMC_STRUCT.iRootContentIDHash) AND GET_PLAYER_RESPAWN_BACK_AT_START_STATE(iLocalPart) >= PRBSS_INIT)
		PROCESS_SHARD_TEXT()
	ELSE // IF we've finished OR PBBOOL_FINISHED is set OR PBBOOL2_PLAYER_OUT_BOUNDS_FAIL is true OR a cutscene is playing
	// So here would be all the instances where we want to clear objective text
		IF iSpectatorTarget = -1
			CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_MISSION_BOUNDS)
			CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_MISSION_BOUNDS2)
		ENDIF
		//IF NOT IS_BIT_SET( iLocalBoolCheck, LBOOL_BLOCK_OBJECTIVE )
			IF Has_Any_MP_Objective_Text_Been_Received()
				Clear_Any_Objective_Text()
			ENDIF
			PRINTLN("[RCC MISSION] [MB] Clear_Any_Objective_Text() 4")

		//ENDIF
	
		//==========================
		
		CLEANUP_OBJECTIVE_BLIPS()
	ENDIF
	
	IF MC_playerBD[iPartToUse].iteam > -1
		IF GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
		OR IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		OR HAS_TEAM_FINISHED(MC_playerBD[iPartToUse].iteam)
		OR HAS_TEAM_FAILED(MC_playerBD[iPartToUse].iteam)
		OR IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_FINISHED )
		OR SHOULD_HIDE_THE_HUD_THIS_FRAME()
			IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GENERIC_TEXT)
			OR IS_BIG_MESSAGE_ALREADY_REQUESTED(BIG_MESSAGE_GENERIC_TEXT)
				TEXT_LABEL_15 tl15 = GET_BIG_MESSAGE_TEXT_BEING_DRAWN()
				STRING sStringToCheck
				sStringToCheck = TEXT_LABEL_TO_STRING(tl15)
				
				PRINTLN("[RCC MISSION][shard] sLastShardText: ", sLastShardText)
				PRINTLN("[RCC MISSION][shard] sStringToCheck: ", sStringToCheck)
				IF NOT IS_STRING_NULL_OR_EMPTY(sLastShardText)
				AND NOT IS_STRING_NULL_OR_EMPTY(sStringToCheck)
					IF ARE_STRINGS_EQUAL(sLastShardText, sStringToCheck)
						CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_GENERIC_TEXT)
						PRINTLN("[RCC MISSION][shard] Clearing Shard Text as the mission is over. sLastShardText: ", sLastShardText)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
			
	IF IS_CELLPHONE_CAMERA_IN_USE()
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("Lowrider_Help_25")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MC_SCORE_H1")				
			CLEAR_HELP(TRUE)				
		ENDIF
	ENDIF
	
	iLastRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iObjTxtPartToUse].iTeam]
ENDPROC

FUNC BOOL IS_HEALTH_BAR_ON_FOR_TEAM(INT iTeam)
	BOOL bVisible = FALSE
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
		SWITCH iTeam
			CASE 0
				bVisible = (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iTeam1HealthBar, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]) OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iTeamBitset3, ciBS3_SHOW_TEAM_HEALTH_BARS_TEAM_0+iTeam))
			BREAK
			CASE 1
				bVisible = (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iTeam2HealthBar, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]) OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iTeamBitset3, ciBS3_SHOW_TEAM_HEALTH_BARS_TEAM_0+iTeam))
			BREAK
			CASE 2
				bVisible = (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iTeam3HealthBar, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]) OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iTeamBitset3, ciBS3_SHOW_TEAM_HEALTH_BARS_TEAM_0+iTeam))
			BREAK
			CASE 3
				bVisible = (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iTeam4HealthBar, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]) OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iTeamBitset3, ciBS3_SHOW_TEAM_HEALTH_BARS_TEAM_0+iTeam))
			BREAK
		ENDSWITCH
	ENDIF
	RETURN bVisible
ENDFUNC

PROC PROCESS_PLAYER_HEALTH_BAR_DISPLAY_FOR_RULE()
	INT iTeam
	INT iCount
	INT iCountOfPlayingPlayers = MC_ServerBD.iNumberOfPlayingPlayers[0] + MC_ServerBD.iNumberOfPlayingPlayers[1] + MC_ServerBD.iNumberOfPlayingPlayers[2] + MC_ServerBD.iNumberOfPlayingPlayers[3]
	
	IF NOT SHOULD_HIDE_THE_HUD_THIS_FRAME()		
		BOOL bShowBar
		INT iPart
		FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
			iTeam = MC_playerBD[iPart].iTeam
			bShowBar = TRUE
			
			IF IS_HEALTH_BAR_ON_FOR_TEAM(iTeam)				
				PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iPart)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
					PED_INDEX PedToShow = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(tempPart))
					VEHICLE_INDEX vehToShow
					STRING sTeamTextLabel
					sTeamTextLabel = GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(tempPart))
					
					INT HealthToShow = 0
					INT HealthMaxToShow
					
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_ALL_PLAYERS_HEALTH_BARS_ON_TEAM_FOR_VEHICLE)
					AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iTeamBitset3, ciBS3_SHOW_TEAM_HEALTH_BARS_VEHICLE_TEAM_0+iTeam)
						IF IS_ENTITY_ALIVE(PedToShow)
							HealthToShow = GET_ENTITY_HEALTH(PedToShow) - 100
						ENDIF
						
						HealthMaxToShow = GET_ENTITY_MAX_HEALTH(PedToShow) - 100
					ELSE
						IF IS_PED_IN_ANY_VEHICLE(PedToShow)
							vehToShow = GET_VEHICLE_PED_IS_IN(PedToShow)
							
							IF GET_PED_IN_VEHICLE_SEAT(vehToShow, VS_DRIVER) != PedToShow
								bShowBar = FALSE
							ENDIF
							
							IF IS_ENTITY_ALIVE(vehToShow)
								HealthToShow = ROUND(GET_VEHICLE_HEALTH_PERCENTAGE(vehToShow))
							ENDIF
							
							HealthMaxToShow = 100
						ELSE
							IF IS_ENTITY_ALIVE(PedToShow)
								HealthToShow = GET_ENTITY_HEALTH(PedToShow) - 100
							ENDIF
							
							HealthMaxToShow = GET_ENTITY_MAX_HEALTH(PedToShow) - 100
						ENDIF
					ENDIF
					
					IF bShowBar
						DRAW_GENERIC_METER(HealthToShow, HealthMaxToShow, sTeamTextLabel, GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam, PlayerToUse), -1, INT_TO_ENUM(HUDORDER, (-(iTeam+1))+8), DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
					ENDIF
					
					iCount++
				ENDIF
			ENDIF
			
			IF iCount >= iCountOfPlayingPlayers
				BREAKLOOP
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

PROC PROCESS_PLAYER_HEALTH_BAR_DISPLAY()
	IF NOT SHOULD_HIDE_THE_HUD_THIS_FRAME()
	AND NOT IS_ENTITY_DEAD(PlayerPedToUse)
	AND NOT (IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
	AND IS_BIT_SET(MC_serverBD.iServerBitSet4, SBBOOL4_SUDDEN_DEATH_JUGGERNAUT))
		INT iTeam
		FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
			IF IS_HEALTH_BAR_ON_FOR_TEAM(iTeam)
				IF MC_ServerBD_4.iCurrentHighestPriority[ iTeam ] < FMMC_MAX_RULES
					IF NOT IS_STRING_NULL_OR_EMPTY(g_sMission_TeamName[iteam])
						IF iPartForTeamHealthBar[iTeam] = -1
							INT iPart
							FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
								IF MC_playerBD[iPart].iTeam = iTeam
									iPartForTeamHealthBar[iTeam] = iPart
									BREAKLOOP
								ENDIF
							ENDFOR
						ELSE
						ENDIF
						PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iPartForTeamHealthBar[iTeam])
						IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
							PED_INDEX PedToShow = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(tempPart))
							IF IS_ENTITY_ALIVE(PedToShow)
								STRING sTeamTextLabel
								HUDORDER eHudOrder = HUDORDER_DONTCARE
								IF iTeam = 0 OR iTeam = 2
									sTeamTextLabel = "OJUGG_HEALTH"
									eHudOrder = HUDORDER_THIRDBOTTOM
								ELIF iTeam = 1 OR iTeam = 3
									sTeamTextLabel = "PJUGG_HEALTH"
									eHudOrder = HUDORDER_FOURTHBOTTOM
								ENDIF
								IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_ENTOURAGE(g_FMMC_STRUCT.iAdversaryModeType)
									sTeamTextLabel = "TARGET_HEALTH"
								ENDIF
								
								DRAW_GENERIC_METER(GET_ENTITY_HEALTH(PedToShow) - 100, GET_ENTITY_MAX_HEALTH(PedToShow) - 100, sTeamTextLabel, GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam, PlayerToUse), -1, eHudOrder)
							ENDIF
						ELSE
							iPartForTeamHealthBar[iTeam] = -1
						ENDIF
					ENDIF
				ENDIF
			ENDIF	
		ENDFOR
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC DRAW_SHOWDOWN_POINTS_DEBUG_FOR_PLAYER(INT iTeam, INT iPart)
	FLOAT x = vShowdownDEBUG_StartPos.x + (vShowdownDEBUG_Offsets.x * iTeam)
	FLOAT y = vShowdownDEBUG_StartPos.y + (vShowdownDEBUG_Offsets.y * iPart)
	
	INT iRed = 255, iGreen = 255, iBlue = 255, iAlpha = 255
	
	TEXT_LABEL_23 tlTeam = "Team: "
	tlTeam += iTeam
	DRAW_DEBUG_TEXT_2D(tlTeam, <<x, 0.120, 0>>, iRed, iGreen, iBlue, iAlpha)
	
	PLAYER_INDEX piThisPlayer = INT_TO_PLAYERINDEX(GET_PLAYER_INDEX_NUMBER_FROM_PARTICIPANT_INDEX_NUMBER(iPart))
	
	TEXT_LABEL_63 fPoints = ""
	fPoints += GET_PLAYER_NAME(piThisPlayer)
	fPoints += ": "
	fPoints += FLOAT_TO_STRING(MC_playerBD_1[iPart].fShowdownPoints)
	fPoints += " / "
	
	fPoints += FLOAT_TO_STRING((MC_playerBD_1[iPart].fShowdownPoints / (g_FMMC_STRUCT.fShowdown_MaximumPoints * TO_FLOAT(MC_serverBD.iNumStartingPlayers[iTeam]))) * 100, DEFAULT, FORMATFLOATTOSTRING_PERCENTAGE)
	
	//DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(LocalPlayerPed), 2.5, 255, 255, 255, 150)
	DRAW_DEBUG_TEXT_2D(fPoints, <<x, y, 0>>, iRed, iGreen, iBlue, iAlpha)
ENDPROC

PROC DRAW_SHOWDOWN_DEBUG()

	IF HAS_NET_TIMER_STARTED(stShowdown_FirstFifteenSecondsTimer)
		IF HAS_NET_TIMER_EXPIRED(stShowdown_FirstFifteenSecondsTimer, ciShowdown_TimeBeforeDrain)
			TEXT_LABEL_63 tlDrain = "Points Drain: approx. "
			FLOAT fDrainAmount = GET_SHOWDOWN_DRAIN_AMOUNT()
			tlDrain += FLOAT_TO_STRING(fDrainAmount)
			tlDrain += " per second"
			
			DRAW_DEBUG_TEXT_2D(tlDrain, <<0.75, 0.75, 0>>, 255, 255, 255, 255)
			
			IF HAS_NET_TIMER_STARTED(stShowdown_IdleTimer)
				IF HAS_NET_TIMER_EXPIRED(stShowdown_IdleTimer, ciShowdown_IdleTimeAllowed)
					DRAW_DEBUG_TEXT_2D("Taking extra drain damage due to being idle for too long!", <<0.7, 0.77, 0>>, 255, 0, 0, 255)
				ELSE
					DRAW_DEBUG_TEXT_2D("Currently Idle!", <<0.75, 0.77, 0>>, 255, 255, 255, 255)
				ENDIF
			ENDIF
			
			IF bShowdown_DisableIdleDrain
				DRAW_DEBUG_TEXT_2D("Idle drain currently disabled for debug!", <<0.75, 0.82, 0>>, 255, 255, 255, 255)
			ENDIF
			
			IF bShowdown_DisableDrain
				DRAW_DEBUG_TEXT_2D("Drain damage currently disabled for debug!", <<0.75, 0.85, 0>>, 0, 255, 0, 255)
			ENDIF
		ELSE
			DRAW_DEBUG_TEXT_2D("Idle drain currently disabled because 15 seconds haven't passed!", <<0.75, 0.82, 0>>, 255, 255, 255, 255)
		ENDIF
	ENDIF
	
ENDPROC
#ENDIF

FUNC FLOAT GET_SHOWDOWN_TEAM_CURRENT_POINTS(INT iTeam)
	INT i
	FLOAT fCurPoints = 0
	
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			IF NOT IS_PARTICIPANT_A_SPECTATOR(i)
				IF MC_playerBD[i].iteam = iTeam
					fCurPoints += ROUND(MC_playerBD_1[i].fShowdownPoints)
					
					#IF IS_DEBUG_BUILD
						IF bShowdown_TeamDebugActive
							DRAW_SHOWDOWN_POINTS_DEBUG_FOR_PLAYER(iTeam, i)
						ENDIF
					#ENDIF
					
					INT iPlayerNum = GET_PLAYER_INDEX_NUMBER_FROM_PARTICIPANT_INDEX_NUMBER(i)
					
					IF IS_MP_GAMER_TAG_ACTIVE(iPlayerNum)
						SET_MP_GAMER_TAGS_SHOULD_USE_POINTS_HEALTH(iPlayerNum, TRUE)
						SET_MP_GAMER_TAGS_POINT_HEALTH(iPlayerNum, ROUND(MC_playerBD_1[i].fShowdownPoints), ROUND(g_FMMC_STRUCT.fShowdown_MaximumPoints))
						
						IF MC_playerBD_1[i].fShowdownPoints > 0.0
							SET_MP_GAMER_TAG_VISIBILITY(iPlayerNum, MP_TAG_HEALTH_BAR, TRUE, TRUE)
						ELSE
							SET_MP_GAMER_TAG_VISIBILITY(iPlayerNum, MP_TAG_HEALTH_BAR, FALSE, TRUE)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				fCurPoints += 0
			ENDIF
		ENDIF
	ENDFOR
	
	PRINTLN("[SHOWDOWNDEBUG] Points currently held by team ", iTeam, ": ", fCurPoints)
	RETURN fCurPoints
ENDFUNC

PROC SET_SHOWDOWN_HUD_STATE(SHOWDOWN_HUD_STATE eNewState)
	eShowdownHUDState = eNewState
	PRINTLN("[SHOWDOWNHUD] Setting HUD state to ", eNewState)
ENDPROC

PROC DRAW_SHOWDOWN_TEAM_CIRCLE(INT iTeam)
	INT j
	FLOAT fPercentage
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "SET_ICON_METER")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTeam)
	
	//This loop is here because in order to reach team 4 (for example) you need to add 3 dummy param floats to skip over teams 1-3 and then the fourth float added will actually apply to team 4's bubble
	FOR j = 0 TO MC_serverBD.iNumberOfTeams - 1
		IF j = iTeam
		AND IS_TEAM_ACTIVE(iTeam)
		AND NOT HAS_TEAM_FAILED(iTeam)			
			fPercentage = GET_SHOWDOWN_TEAM_CURRENT_POINTS(iTeam) / (g_FMMC_STRUCT.fShowdown_MaximumPoints * TO_FLOAT(MC_serverBD.iNumStartingPlayers[iTeam]))
		    SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fPercentage)
			PRINTLN("[SHOWDOWNHUD] Setting percentage for team ", iTeam, " to ", fPercentage)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.0)
			PRINTLN("[SHOWDOWNHUD] Setting percentage for team ", j, " to 0.0 to skip over them because they're either not the right team or they're inactive")
		ENDIF
	ENDFOR
	
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC PROCESS_RULE_ATTEMPTS_DISPLAY()
	IF NOT SHOULD_HIDE_THE_HUD_THIS_FRAME()
		INT iTeam = MC_playerBD[iPartToUse].iteam
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamRuleAttempts > 1
			INT iAttemptsLeft = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamRuleAttempts - MC_serverBD.iNumTeamRuleAttempts[iTeam]
			BOOL bLastAttempt = (MC_serverBD.iNumTeamRuleAttempts[iTeam] = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamRuleAttempts - 1)
			HUD_COLOURS eHudColour = HUD_COLOUR_WHITE
			IF bLastAttempt
				eHudColour = HUD_COLOUR_RED
				DRAW_GENERIC_BIG_NUMBER(iAttemptsLeft - 1,"MC_TATTEMP",30000,eHudColour,HUDORDER_DONTCARE,FALSE,"",eHudColour, FALSE, HUDFLASHING_FLASHWHITE, 1)
			ELSE
				DRAW_GENERIC_BIG_NUMBER(iAttemptsLeft - 1,"MC_TATTEMP",-1,eHudColour,HUDORDER_DONTCARE,FALSE,"",eHudColour)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DRAW_HARD_TARGET_TIMER()
	INT iTeam = MC_PlayerBD[iLocalPart].iteam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	
	IF MC_ServerBD_4.iHardTargetMiniRound > 0
		INT iTotalTimeRemaining = GET_REMAINING_TIME_ON_RULE(iTeam, iRule)
		INT iTimeRemaining = (g_FMMC_STRUCT.iHRDTARPickTargetTimer * MC_ServerBD_4.iHardTargetMiniRound) - (GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveTimeLimitRule[iRule]) - GET_REMAINING_TIME_ON_RULE(iTeam, iRule))		
		
		TEXT_LABEL_15 tl15 = "HRD_TRG_NXT"
		HUD_COLOURS hudCol = HUD_COLOUR_WHITE
		
		IF iTimeRemaining < 10000
			hudCol = HUD_COLOUR_RED
		ENDIF
		
		IF iTimeRemaining <= 10000
		AND iTimeRemaining > 0
			IF iHardTargetCountdownTimerSoundID = -1
				PRINTLN("[PROCESS_HARD_TARGET_MODE][LM] - Starting 10s Timer for Hard Target Change.")
				iHardTargetCountdownTimerSoundID = GET_SOUND_ID()
				PLAY_SOUND_FRONTEND(iHardTargetCountdownTimerSoundID, "10s", "MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
			ENDIF
		ELSE
			IF IS_SOUND_ID_VALID(iHardTargetCountdownTimerSoundID)
				PRINTLN("[PROCESS_HARD_TARGET_MODE][LM] - Clearing 10s Timer for Hard Target Change.")
				iHardTargetCountdownTimerSoundID = -1
				STOP_SOUND(iHardTargetCountdownTimerSoundID)
			ENDIF
		ENDIF
		
		IF iTimeRemaining <= 0
			iTimeRemaining = 0
		ENDIF
		
		IF iTotalTimeRemaining > iTimeRemaining
			DRAW_GENERIC_TIMER(iTimeRemaining, tl15, DEFAULT, TIMER_STYLE_DONTUSEMILLISECONDS, -1,  DEFAULT, HUDORDER_SIXTHBOTTOM, DEFAULT, hudCol,  DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_WHITE)
		ENDIF
	ENDIF
ENDPROC

PROC BOTTOM_RIGHT_UI_PLAYER_STATE_TIMER_TEAMS(HUD_COLOURS &TeamHUDColours[FMMC_MAX_TEAMS],
											  PLAYER_INDEX Player1ID = NULL, INT Player1Status = -3, INT Player1Team = 0,
											  PLAYER_INDEX Player2ID = NULL, INT Player2Status = -3, INT Player2Team = 0,
											  PLAYER_INDEX Player3ID = NULL, INT Player3Status = -3, INT Player3Team = 0,
											  PLAYER_INDEX Player4ID = NULL, INT Player4Status = -3, INT Player4Team = 0,
											  PLAYER_INDEX Player5ID = NULL, INT Player5Status = -3, INT Player5Team = 0,
											  PLAYER_INDEX Player6ID = NULL, INT Player6Status = -3, INT Player6Team = 0,
											  PLAYER_INDEX Player7ID = NULL, INT Player7Status = -3, INT Player7Team = 0,
											  PLAYER_INDEX Player8ID = NULL, INT Player8Status = -3, INT Player8Team = 0,
											  INT Event_Timer_Display = 0,			
											  HUD_COLOURS EventTimerColour = HUD_COLOUR_WHITE,
											  STRING ModeName = NULL,
										   	  BOOL bIsSpectating = FALSE)
																
	IF bIsSpectating
	AND IS_SPECTATOR_SHOWING_NEWS_HUD()
		EXIT
	ENDIF
	
	INT iHudOrder = ENUM_TO_INT(HUDORDER_BOTTOM)
	
	#IF IS_DEBUG_BUILD
	IF bDebugRezHUDPrints
		PRINTLN("[JS] [REZQ] - BOTTOM_RIGHT_UI_PLAYER_STATE_TIMER_TEAMS - Initial HUD Order - ", iHUDOrder)
	ENDIF
	#ENDIF
	
	HIDE_BOTTOM_RIGHT_CODE_UI()
	
	STRING LocalModelName = "HUD_COUNTDOWN"
	IF NOT IS_STRING_EMPTY_HUD(ModeName)
		LocalModelName = ModeName
	ENDIF
	IF Event_Timer_Display != -1
		DRAW_GENERIC_TIMER(Event_Timer_Display, LocalModelName, DEFAULT, DEFAULT, DEFAULT, DEFAULT, INT_TO_ENUM(HUDORDER, iHudOrder), DEFAULT, EventTimerColour, DEFAULT, DEFAULT, DEFAULT, EventTimerColour)
		iHudOrder++
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bDebugRezHUDPrints
		PRINTLN("[JS] [REZQ] - BOTTOM_RIGHT_UI_PLAYER_STATE_TIMER_TEAMS - After Timer HUD Order - ", iHUDOrder)
	ENDIF
	#ENDIF
	
	//ENEMY TEAM
	INT iEnemyTeam = 0
	IF MC_playerBD[iPartToUse].iteam = iEnemyTeam
		iEnemyTeam = 1
	ENDIF

	SWITCH Player4Status 
		CASE -3 //NONE
		BREAK
		CASE -2 //DEAD
			DRAW_GENERIC_SCORE(Player4Status, GET_PLAYER_NAME(Player4ID), DEFAULT, DEFAULT,  INT_TO_ENUM(HUDORDER, iHudOrder), TRUE, "", DEFAULT, DEFAULT, DEFAULT, DEFAULT, TeamHUDColours[Player4Team], DEFAULT, DEFAULT, DEFAULT, ACTIVITY_POWERUP_PED_HEADSHOT_DEAD, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, Player4ID)
			iHudOrder++
		BREAK
		CASE -1 //ALIVE
			DRAW_GENERIC_SCORE(Player4Status, GET_PLAYER_NAME(Player4ID), DEFAULT, DEFAULT,  INT_TO_ENUM(HUDORDER, iHudOrder), TRUE, "", DEFAULT, DEFAULT, DEFAULT, DEFAULT, TeamHUDColours[Player4Team], DEFAULT, DEFAULT, DEFAULT, ACTIVITY_POWERUP_PED_HEADSHOT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, Player4ID)
			iHudOrder++
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	IF bDebugRezHUDPrints
		PRINTLN("[JS] [REZQ] - BOTTOM_RIGHT_UI_PLAYER_STATE_TIMER_TEAMS - After Player 4 Order - ", iHUDOrder)
	ENDIF
	#ENDIF
	
	SWITCH Player3Status 
		CASE -3 //NONE
		BREAK
		CASE -2 //DEAD
			DRAW_GENERIC_SCORE(Player3Status, GET_PLAYER_NAME(Player3ID), DEFAULT, DEFAULT,  INT_TO_ENUM(HUDORDER, iHudOrder), TRUE, "", DEFAULT, DEFAULT, DEFAULT, DEFAULT, TeamHUDColours[Player3Team], DEFAULT, DEFAULT, DEFAULT, ACTIVITY_POWERUP_PED_HEADSHOT_DEAD, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, Player3ID)
			iHudOrder++
		BREAK
		CASE -1 //ALIVE
			DRAW_GENERIC_SCORE(Player3Status, GET_PLAYER_NAME(Player3ID), DEFAULT, DEFAULT,  INT_TO_ENUM(HUDORDER, iHudOrder), TRUE, "", DEFAULT, DEFAULT, DEFAULT, DEFAULT, TeamHUDColours[Player3Team], DEFAULT, DEFAULT, DEFAULT, ACTIVITY_POWERUP_PED_HEADSHOT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, Player3ID)
			iHudOrder++
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	IF bDebugRezHUDPrints
		PRINTLN("[JS] [REZQ] - BOTTOM_RIGHT_UI_PLAYER_STATE_TIMER_TEAMS - After Player 3 Order - ", iHUDOrder)
	ENDIF
	#ENDIF
	
	SWITCH Player2Status 
		CASE -3 //NONE
		BREAK
		CASE -2 //DEAD
			DRAW_GENERIC_SCORE(Player2Status, GET_PLAYER_NAME(Player2ID), DEFAULT, DEFAULT,  INT_TO_ENUM(HUDORDER, iHudOrder), TRUE, "", DEFAULT, DEFAULT, DEFAULT, DEFAULT, TeamHUDColours[Player2Team], DEFAULT, DEFAULT, DEFAULT, ACTIVITY_POWERUP_PED_HEADSHOT_DEAD, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, Player2ID)
			iHudOrder++
		BREAK
		CASE -1 //ALIVE
			DRAW_GENERIC_SCORE(Player2Status, GET_PLAYER_NAME(Player2ID), DEFAULT, DEFAULT,  INT_TO_ENUM(HUDORDER, iHudOrder), TRUE, "", DEFAULT, DEFAULT, DEFAULT, DEFAULT, TeamHUDColours[Player2Team], DEFAULT, DEFAULT, DEFAULT, ACTIVITY_POWERUP_PED_HEADSHOT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, Player2ID)
			iHudOrder++
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	IF bDebugRezHUDPrints
		PRINTLN("[JS] [REZQ] - BOTTOM_RIGHT_UI_PLAYER_STATE_TIMER_TEAMS - After Player 2 Order - ", iHUDOrder)
	ENDIF
	#ENDIF
	
	SWITCH Player1Status 
		CASE -3 //NONE
		BREAK
		CASE -2 //DEAD
			DRAW_GENERIC_SCORE(Player1Status, GET_PLAYER_NAME(Player1ID), 999999, DEFAULT,  INT_TO_ENUM(HUDORDER, iHudOrder), TRUE, "", DEFAULT, DEFAULT, HUDFLASHING_NONE, 500, TeamHUDColours[Player1Team], DEFAULT, DEFAULT, DEFAULT, ACTIVITY_POWERUP_PED_HEADSHOT_DEAD, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, Player1ID, TRUE)
			iHudOrder++
		BREAK
		CASE -1 //ALIVE
			DRAW_GENERIC_SCORE(Player1Status, GET_PLAYER_NAME(Player1ID), DEFAULT, DEFAULT,  INT_TO_ENUM(HUDORDER, iHudOrder), TRUE, "", DEFAULT, DEFAULT, DEFAULT, DEFAULT, TeamHUDColours[Player1Team], DEFAULT, DEFAULT, DEFAULT, ACTIVITY_POWERUP_PED_HEADSHOT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, Player1ID)
			iHudOrder++
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	IF bDebugRezHUDPrints
		PRINTLN("[JS] [REZQ] - BOTTOM_RIGHT_UI_PLAYER_STATE_TIMER_TEAMS - After Player 1 Order - ", iHUDOrder)
	ENDIF
	#ENDIF
	
	HUD_COLOURS eTextColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iEnemyTeam, PlayerToUse)				
	BOOL bLiteral = FALSE
	TEXT_LABEL_63 tl63LocalTitle = "DZ_E_TEAM"
	HUDORDER eHudOrder = INT_TO_ENUM(HUDORDER, iHudOrder)
	INT iScore = MC_serverBD.iTeamScore[iEnemyTeam] - g_FMMC_STRUCT.iInitialPoints[iEnemyTeam]
	INT iTarget = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMissionTargetScore
	DRAW_GENERIC_BIG_DOUBLE_NUMBER(iScore, iTarget, tl63LocalTitle, DEFAULT, DEFAULT, eHudOrder, bLiteral, DEFAULT, DEFAULT, DEFAULT, DEFAULT, eTextColour)
	iHudOrder++
	
	#IF IS_DEBUG_BUILD
	IF bDebugRezHUDPrints
		PRINTLN("[JS] [REZQ] - BOTTOM_RIGHT_UI_PLAYER_STATE_TIMER_TEAMS - After Enemy Score Order - ", iHUDOrder)
	ENDIF
	#ENDIF

	//LOCAL TEAM	
	SWITCH Player8Status 
		CASE -3 //NONE
		BREAK
		CASE -2 //DEAD
			DRAW_GENERIC_SCORE(Player8Status, GET_PLAYER_NAME(Player8ID), DEFAULT, DEFAULT,  INT_TO_ENUM(HUDORDER, iHudOrder), TRUE, "", DEFAULT, DEFAULT, DEFAULT, DEFAULT, TeamHUDColours[Player8Team], DEFAULT, DEFAULT, DEFAULT, ACTIVITY_POWERUP_PED_HEADSHOT_DEAD, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, Player8ID)
			iHudOrder++
		BREAK
		CASE -1 //ALIVE
			DRAW_GENERIC_SCORE(Player8Status, GET_PLAYER_NAME(Player8ID), DEFAULT, DEFAULT,  INT_TO_ENUM(HUDORDER, iHudOrder), TRUE, "", DEFAULT, DEFAULT, DEFAULT, DEFAULT, TeamHUDColours[Player8Team], DEFAULT, DEFAULT, DEFAULT, ACTIVITY_POWERUP_PED_HEADSHOT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, Player8ID)
			iHudOrder++
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	IF bDebugRezHUDPrints
		PRINTLN("[JS] [REZQ] - BOTTOM_RIGHT_UI_PLAYER_STATE_TIMER_TEAMS - After Player 8 Order - ", iHUDOrder)
	ENDIF
	#ENDIF
	
	SWITCH Player7Status 
		CASE -3 //NONE
		BREAK
		CASE -2 //DEAD
			DRAW_GENERIC_SCORE(Player7Status, GET_PLAYER_NAME(Player7ID), DEFAULT, DEFAULT,  INT_TO_ENUM(HUDORDER, iHudOrder), TRUE, "", DEFAULT, DEFAULT, DEFAULT, DEFAULT, TeamHUDColours[Player7Team], DEFAULT, DEFAULT, DEFAULT, ACTIVITY_POWERUP_PED_HEADSHOT_DEAD, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, Player7ID)
			iHudOrder++
		BREAK
		CASE -1 //ALIVE
			DRAW_GENERIC_SCORE(Player7Status, GET_PLAYER_NAME(Player7ID), DEFAULT, DEFAULT,  INT_TO_ENUM(HUDORDER, iHudOrder), TRUE, "", DEFAULT, DEFAULT, DEFAULT, DEFAULT, TeamHUDColours[Player7Team], DEFAULT, DEFAULT, DEFAULT, ACTIVITY_POWERUP_PED_HEADSHOT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, Player7ID)
			iHudOrder++
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	IF bDebugRezHUDPrints
		PRINTLN("[JS] [REZQ] - BOTTOM_RIGHT_UI_PLAYER_STATE_TIMER_TEAMS - After Player 7 Order - ", iHUDOrder)
	ENDIF
	#ENDIF
	
	SWITCH Player6Status 
		CASE -3 //NONE
		BREAK
		CASE -2 //DEAD
			DRAW_GENERIC_SCORE(Player6Status, GET_PLAYER_NAME(Player6ID), DEFAULT, DEFAULT,  INT_TO_ENUM(HUDORDER, iHudOrder), TRUE, "", DEFAULT, DEFAULT, DEFAULT, DEFAULT, TeamHUDColours[Player6Team], DEFAULT, DEFAULT, DEFAULT, ACTIVITY_POWERUP_PED_HEADSHOT_DEAD, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, Player6ID)
			iHudOrder++
		BREAK
		CASE -1 //ALIVE
			DRAW_GENERIC_SCORE(Player6Status, GET_PLAYER_NAME(Player6ID), DEFAULT, DEFAULT,  INT_TO_ENUM(HUDORDER, iHudOrder), TRUE, "", DEFAULT, DEFAULT, DEFAULT, DEFAULT, TeamHUDColours[Player6Team], DEFAULT, DEFAULT, DEFAULT, ACTIVITY_POWERUP_PED_HEADSHOT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, Player6ID)
			iHudOrder++
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	IF bDebugRezHUDPrints
		PRINTLN("[JS] [REZQ] - BOTTOM_RIGHT_UI_PLAYER_STATE_TIMER_TEAMS - After Player 6 Order - ", iHUDOrder)
	ENDIF
	#ENDIF
	
	SWITCH Player5Status 
		CASE -3 //NONE
		BREAK
		CASE -2 //DEAD
			DRAW_GENERIC_SCORE(Player5Status, GET_PLAYER_NAME(Player5ID), 999999, DEFAULT,  INT_TO_ENUM(HUDORDER, iHudOrder), TRUE, "", DEFAULT, DEFAULT, HUDFLASHING_NONE, 500, TeamHUDColours[Player5Team], DEFAULT, DEFAULT, DEFAULT, ACTIVITY_POWERUP_PED_HEADSHOT_DEAD, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, Player5ID, TRUE)
			iHudOrder++
		BREAK
		CASE -1 //ALIVE
			DRAW_GENERIC_SCORE(Player5Status, GET_PLAYER_NAME(Player5ID), DEFAULT, DEFAULT,  INT_TO_ENUM(HUDORDER, iHudOrder), TRUE, "", DEFAULT, DEFAULT, DEFAULT, DEFAULT, TeamHUDColours[Player5Team], DEFAULT, DEFAULT, DEFAULT, ACTIVITY_POWERUP_PED_HEADSHOT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, Player5ID)
			iHudOrder++
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	IF bDebugRezHUDPrints
		PRINTLN("[JS] [REZQ] - BOTTOM_RIGHT_UI_PLAYER_STATE_TIMER_TEAMS - After Player 5 Order - ", iHUDOrder)
	ENDIF
	#ENDIF
	
	eTextColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_playerBD[iPartToUse].iteam, PlayerToUse)				
	bLiteral = FALSE
	tl63LocalTitle = "DZ_Y_TEAM"
	eHudOrder = INT_TO_ENUM(HUDORDER, iHudOrder)
	iScore = MC_serverBD.iTeamScore[MC_playerBD[iPartToUse].iteam] - g_FMMC_STRUCT.iInitialPoints[MC_playerBD[iPartToUse].iteam]
	iTarget = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMissionTargetScore
	DRAW_GENERIC_BIG_DOUBLE_NUMBER(iScore, iTarget, tl63LocalTitle, DEFAULT, DEFAULT, eHudOrder, bLiteral, DEFAULT, DEFAULT, DEFAULT, DEFAULT, eTextColour)
	iHudOrder++
	
	#IF IS_DEBUG_BUILD
	IF bDebugRezHUDPrints
		PRINTLN("[JS] [REZQ] - BOTTOM_RIGHT_UI_PLAYER_STATE_TIMER_TEAMS - After Team Score Order - ", iHUDOrder)
	ENDIF
	#ENDIF

ENDPROC

FUNC ACTIVITY_POWERUP GET_ICON_FOR_RESURRECTION_PLAYER(INT iStatus)
	ACTIVITY_POWERUP eActivityPowerup = ACTIVITY_POWERUP_NONE
	SWITCH iStatus 
		CASE -2 //DEAD
			eActivityPowerup = ACTIVITY_POWERUP_PED_HEADSHOT_DEAD
		BREAK
		CASE -1 //ALIVE
			eActivityPowerup = ACTIVITY_POWERUP_PED_HEADSHOT
		BREAK
	ENDSWITCH
	RETURN eActivityPowerup
ENDFUNC

PROC BOTTOM_RIGHT_UI_PLAYER_STATE_TIMER_TEAMS_CONDENSED(HUD_COLOURS &TeamHUDColours[FMMC_MAX_TEAMS],
											  PLAYER_INDEX Player1ID = NULL, INT Player1Status = -3,
											  PLAYER_INDEX Player2ID = NULL, INT Player2Status = -3,
											  PLAYER_INDEX Player3ID = NULL, INT Player3Status = -3,
											  PLAYER_INDEX Player4ID = NULL, INT Player4Status = -3,
											  PLAYER_INDEX Player5ID = NULL, INT Player5Status = -3,
											  PLAYER_INDEX Player6ID = NULL, INT Player6Status = -3,
											  PLAYER_INDEX Player7ID = NULL, INT Player7Status = -3,
											  PLAYER_INDEX Player8ID = NULL, INT Player8Status = -3,
											  INT Event_Timer_Display = 0,			
											  HUD_COLOURS EventTimerColour = HUD_COLOUR_WHITE,
											  STRING ModeName = NULL,
										   	  BOOL bIsSpectating = FALSE)
																
	IF bIsSpectating
	AND IS_SPECTATOR_SHOWING_NEWS_HUD()
		EXIT
	ENDIF
	
	HIDE_BOTTOM_RIGHT_CODE_UI()
	
	INT iHudOrder = ENUM_TO_INT(HUDORDER_BOTTOM)

	STRING LocalModelName = "HUD_COUNTDOWN"
	IF NOT IS_STRING_EMPTY_HUD(ModeName)
		LocalModelName = ModeName
	ENDIF
	IF Event_Timer_Display != -1
		DRAW_GENERIC_TIMER(Event_Timer_Display, LocalModelName, DEFAULT, DEFAULT, DEFAULT, DEFAULT, INT_TO_ENUM(HUDORDER, iHudOrder), DEFAULT, EventTimerColour, DEFAULT, DEFAULT, DEFAULT, EventTimerColour)
		iHudOrder++
	ENDIF
								
	//ENEMY TEAM
	INT iEnemyTeam = 0
	IF MC_playerBD[iPartToUse].iteam = iEnemyTeam
		iEnemyTeam = 1
	ENDIF
	
	DRAW_GENERIC_FOUR_ICON_BAR(TeamHUDColours[iEnemyTeam], 
							PICK_PLAYER_ID(Player1ID != INVALID_PLAYER_INDEX(), Player1ID, NULL),
							PICK_PLAYER_ID(Player2ID != INVALID_PLAYER_INDEX(), Player2ID, NULL),
							PICK_PLAYER_ID(Player3ID != INVALID_PLAYER_INDEX(), Player3ID, NULL),
							PICK_PLAYER_ID(Player4ID != INVALID_PLAYER_INDEX(), Player4ID, NULL),
							GET_ICON_FOR_RESURRECTION_PLAYER(Player1Status),
							GET_ICON_FOR_RESURRECTION_PLAYER(Player2Status),
							GET_ICON_FOR_RESURRECTION_PLAYER(Player3Status),
							GET_ICON_FOR_RESURRECTION_PLAYER(Player4Status),
							INT_TO_ENUM(HUDORDER, iHudOrder), (GET_ICON_FOR_RESURRECTION_PLAYER(Player1Status) = ACTIVITY_POWERUP_PED_HEADSHOT_DEAD), DEFAULT, DEFAULT, DEFAULT, 9999999)
	iHudOrder += 2 //LEave a space for the OOB timer
	
	HUD_COLOURS eTextColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iEnemyTeam, PlayerToUse)				
	BOOL bLiteral = FALSE
	TEXT_LABEL_63 tl63LocalTitle = "DZ_E_TEAM"
	HUDORDER eHudOrder = INT_TO_ENUM(HUDORDER, iHudOrder)
	INT iScore = MC_serverBD.iTeamScore[iEnemyTeam] - g_FMMC_STRUCT.iInitialPoints[iEnemyTeam]
	INT iTarget = g_FMMC_STRUCT.sFMMCEndConditions[iEnemyTeam].iMissionTargetScore
	DRAW_GENERIC_BIG_DOUBLE_NUMBER(iScore, iTarget, tl63LocalTitle, DEFAULT, DEFAULT, eHudOrder, bLiteral, DEFAULT, DEFAULT, DEFAULT, DEFAULT, eTextColour)
	iHudOrder++

	//LOCAL TEAM	
	DRAW_GENERIC_FOUR_ICON_BAR(TeamHUDColours[MC_playerBD[iPartToUse].iteam], 
								PICK_PLAYER_ID(Player5ID != INVALID_PLAYER_INDEX(), Player5ID, NULL),
								PICK_PLAYER_ID(Player6ID != INVALID_PLAYER_INDEX(), Player6ID, NULL),
								PICK_PLAYER_ID(Player7ID != INVALID_PLAYER_INDEX(), Player7ID, NULL),
								PICK_PLAYER_ID(Player8ID != INVALID_PLAYER_INDEX(), Player8ID, NULL),
								GET_ICON_FOR_RESURRECTION_PLAYER(Player5Status),
								GET_ICON_FOR_RESURRECTION_PLAYER(Player6Status),
								GET_ICON_FOR_RESURRECTION_PLAYER(Player7Status),
								GET_ICON_FOR_RESURRECTION_PLAYER(Player8Status),
								INT_TO_ENUM(HUDORDER, iHudOrder), (GET_ICON_FOR_RESURRECTION_PLAYER(Player5Status) = ACTIVITY_POWERUP_PED_HEADSHOT_DEAD), DEFAULT, DEFAULT, DEFAULT, 9999999)
	iHudOrder++
	
	eTextColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_playerBD[iPartToUse].iteam, PlayerToUse)				
	bLiteral = FALSE
	eHudOrder = INT_TO_ENUM(HUDORDER, iHudOrder)
	tl63LocalTitle = "DZ_Y_TEAM"
	iScore = MC_serverBD.iTeamScore[MC_playerBD[iPartToUse].iteam] - g_FMMC_STRUCT.iInitialPoints[MC_playerBD[iPartToUse].iteam]
	iTarget = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMissionTargetScore
	DRAW_GENERIC_BIG_DOUBLE_NUMBER(iScore, iTarget, tl63LocalTitle, DEFAULT, DEFAULT, eHudOrder, bLiteral, DEFAULT, DEFAULT, DEFAULT, DEFAULT, eTextColour)
	iHudOrder++
	
ENDPROC

PROC PLAYER_STATE_LIST_RESURRECTION_DISPLAY_EXTENDED(PLAYER_STATE_LIST_RESURRECTION_DATA &playerStateListData, INT iEventTimerDisplay, HUD_COLOURS EventTimerColour)
	BOTTOM_RIGHT_UI_PLAYER_STATE_TIMER_TEAMS(playerStateListData.hcTeam,
									   		 playerStateListData.uiSlot[0].piID, playerStateListData.uiSlot[0].iValue, playerStateListData.iTeam[0],
									  		 playerStateListData.uiSlot[1].piID, playerStateListData.uiSlot[1].iValue, playerStateListData.iTeam[1],
									  		 playerStateListData.uiSlot[2].piID, playerStateListData.uiSlot[2].iValue, playerStateListData.iTeam[2],
									   		 playerStateListData.uiSlot[3].piID, playerStateListData.uiSlot[3].iValue, playerStateListData.iTeam[3],
										   	 playerStateListData.uiSlot[4].piID, playerStateListData.uiSlot[4].iValue, playerStateListData.iTeam[4],
										   	 playerStateListData.uiSlot[5].piID, playerStateListData.uiSlot[5].iValue, playerStateListData.iTeam[5],
										     playerStateListData.uiSlot[6].piID, playerStateListData.uiSlot[6].iValue, playerStateListData.iTeam[6],
										  	 playerStateListData.uiSlot[7].piID, playerStateListData.uiSlot[7].iValue, playerStateListData.iTeam[7],
										   	 iEventTimerDisplay,
										   	 EventTimerColour,
										   	 "MC_TIMEOL", 
										   	 IS_LOCAL_PLAYER_ANY_SPECTATOR())
ENDPROC

PROC PLAYER_STATE_LIST_RESURRECTION_DISPLAY_CONDENSED(PLAYER_STATE_LIST_RESURRECTION_DATA &playerStateListData, INT iEventTimerDisplay, HUD_COLOURS EventTimerColour)
	BOTTOM_RIGHT_UI_PLAYER_STATE_TIMER_TEAMS_CONDENSED(playerStateListData.hcTeam,
									   		 playerStateListData.uiSlot[0].piID, playerStateListData.uiSlot[0].iValue,
									  		 playerStateListData.uiSlot[1].piID, playerStateListData.uiSlot[1].iValue,
									  		 playerStateListData.uiSlot[2].piID, playerStateListData.uiSlot[2].iValue,
									   		 playerStateListData.uiSlot[3].piID, playerStateListData.uiSlot[3].iValue,
										   	 playerStateListData.uiSlot[4].piID, playerStateListData.uiSlot[4].iValue,
										   	 playerStateListData.uiSlot[5].piID, playerStateListData.uiSlot[5].iValue,
										     playerStateListData.uiSlot[6].piID, playerStateListData.uiSlot[6].iValue,
										  	 playerStateListData.uiSlot[7].piID, playerStateListData.uiSlot[7].iValue,
										   	 iEventTimerDisplay,
										   	 EventTimerColour,
										   	 "MC_TIMEOL", 
										   	 IS_LOCAL_PLAYER_ANY_SPECTATOR())
ENDPROC

PROC SORT_PLAYER_LIST_FOR_RESURRECTION(PLAYER_STATE_LIST_RESURRECTION_DATA &playerStateListDataResurrection)
	INT iAlreadyInsertedBS
	INT iNumPlayersPerTeam = 8 / MC_serverBD.iNumberOfTeams
	INT iTeam, i
	INT iCurrentSlot[FMMC_MAX_TEAMS]
	INT iMaxPlayersToShow[FMMC_MAX_TEAMS]
	INT iTeamStartPos[FMMC_MAX_TEAMS]

	FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
		iMaxPlayersToShow[iTeam] = iNumPlayersPerTeam//PICK_INT(iNumPlayersPerTeam > MC_serverBD.iNumberOfPlayingPlayers[iTeam], MC_serverBD.iNumberOfPlayingPlayers[iTeam], iNumPlayersPerTeam)
	ENDFOR

	SWITCH(MC_playerBD[iPartToUse].iTeam)
		CASE 0
			iTeamStartPos[0] = iMaxPlayersToShow[1] + iMaxPlayersToShow[2] + iMaxPlayersToShow[3]
			iTeamStartPos[1] = iMaxPlayersToShow[2] + iMaxPlayersToShow[3]
			iTeamStartPos[2] = iMaxPlayersToShow[3]
			iTeamStartPos[3] = 0
		BREAK
		CASE 1
			iTeamStartPos[1] = iMaxPlayersToShow[0] + iMaxPlayersToShow[2]  + iMaxPlayersToShow[3]
			iTeamStartPos[0] = iMaxPlayersToShow[2] + iMaxPlayersToShow[3]
			iTeamStartPos[2] = iMaxPlayersToShow[2]
			iTeamStartPos[3] = 0
		BREAK
		CASE 2
			iTeamStartPos[2] = iMaxPlayersToShow[0] + iMaxPlayersToShow[1] + iMaxPlayersToShow[3]
			iTeamStartPos[0] = iMaxPlayersToShow[1] + iMaxPlayersToShow[3]
			iTeamStartPos[1] = iMaxPlayersToShow[1]
			iTeamStartPos[3] = 0
		BREAK
		CASE 3
			iTeamStartPos[3] = iMaxPlayersToShow[0] + iMaxPlayersToShow[1] + iMaxPlayersToShow[2]
			iTeamStartPos[0] = iMaxPlayersToShow[1] + iMaxPlayersToShow[2]
			iTeamStartPos[1] = iMaxPlayersToShow[2]
			iTeamStartPos[2] = 0
		BREAK
	ENDSWITCH
	
	FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
		#IF IS_DEBUG_BUILD
		IF bDebugRezHUDPrints
			PRINTLN("[JS] [REZQ] - SORT_PLAYER_LIST_FOR_RESURRECTION TEAM = ", iTeam)
		ENDIF
		#ENDIF
		iCurrentSlot[iTeam] = iTeamStartPos[iTeam]
		IF DOES_TEAM_LIKE_TEAM(iTeam, MC_playerBD[iPartToUse].iteam)
			playerStateListDataResurrection.hcTeam[iTeam] = HUD_COLOUR_FRIENDLY
		ELSE
			playerStateListDataResurrection.hcTeam[iTeam] = HUD_COLOUR_RED
		ENDIF
		FOR i = 0 TO iNumPlayersPerTeam - 1
			#IF IS_DEBUG_BUILD
			IF bDebugRezHUDPrints
				PRINTLN("[JS] [REZQ] - SORT_PLAYER_LIST_FOR_RESURRECTION REZQ LOOP i = ", i)
				PRINTLN("[JS] [REZQ] - SORT_PLAYER_LIST_FOR_RESURRECTION REZQ LOOP iRespawnQueue = ", MC_serverBD_1.iRespawnQueue[iTeam][i])
			ENDIF
			#ENDIF
			
			IF MC_serverBD_1.iRespawnQueue[iTeam][i] != -1
				PARTICIPANT_INDEX piTemp = INT_TO_PARTICIPANTINDEX(MC_serverBD_1.iRespawnQueue[iTeam][i])
				IF NETWORK_IS_PARTICIPANT_ACTIVE(piTemp)
					#IF IS_DEBUG_BUILD
					IF bDebugRezHUDPrints
						PRINTLN("[JS] [REZQ] - SORT_PLAYER_LIST_FOR_RESURRECTION REZQ LOOP PART IS ACTIVE")
					ENDIF
					#ENDIF
					PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(piTemp)
					IF NOT IS_NET_PLAYER_OK(tempPlayer)
						playerStateListDataResurrection.uiSlot[iCurrentSlot[iTeam]].iValue = -2
						playerStateListDataResurrection.uiSlot[iCurrentSlot[iTeam]].piID = tempPlayer
						playerStateListDataResurrection.iTeam[iCurrentSlot[iTeam]] = iTeam
						#IF IS_DEBUG_BUILD
						IF bDebugRezHUDPrints
							PRINTLN("[JS] [REZQ] - SORT_PLAYER_LIST_FOR_RESURRECTION - Adding player ",MC_serverBD_1.iRespawnQueue[iTeam][i]," team: ", iTeam, "  at position: ", iCurrentSlot[iTeam])
						ENDIF
						#ENDIF
						SET_BIT(iAlreadyInsertedBS, MC_serverBD_1.iRespawnQueue[iTeam][i])
						iCurrentSlot[iTeam]++
					ENDIF
				ENDIF
			ELSE
				BREAKLOOP
			ENDIF
			IF iCurrentSlot[iTeam] >= iTeamStartPos[iTeam] + iMaxPlayersToShow[iTeam]
				#IF IS_DEBUG_BUILD
				IF bDebugRezHUDPrints
					PRINTLN("[JS] [REZQ] - SORT_PLAYER_LIST_FOR_RESURRECTION REZQ LOOP BREAKLOOP iCurrentSlot = ", iCurrentSlot[iTeam] ," >= ", (iMaxPlayersToShow[iTeam] * iTeam) + iMaxPlayersToShow[iTeam])
				ENDIF
				#ENDIF
				BREAKLOOP
			ENDIF
		ENDFOR
	ENDFOR
	
	INT iPlayersChecked
	INT iPlayersToCheck = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		#IF IS_DEBUG_BUILD
		IF bDebugRezHUDPrints
			PRINTLN("[JS] [REZQ] - SORT_PLAYER_LIST_FOR_RESURRECTION PLAYER LOOP i = ", i)
		ENDIF
		#ENDIF
		IF ((NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_ANY_SPECTATOR)) OR IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
		AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet,PBBOOL_PLAYER_FAIL)
		AND IS_BIT_SET(MC_playerBD[i].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
		AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_FINISHED)
			#IF IS_DEBUG_BUILD
			IF bDebugRezHUDPrints
				PRINTLN("[JS] [REZQ] - SORT_PLAYER_LIST_FOR_RESURRECTION PLAYER LOOP part not failed/spectator")
			ENDIF
			#ENDIF
			PARTICIPANT_INDEX partTemp = INT_TO_PARTICIPANTINDEX(i)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(partTemp)
				#IF IS_DEBUG_BUILD
				IF bDebugRezHUDPrints
					PRINTLN("[JS] [REZQ] - SORT_PLAYER_LIST_FOR_RESURRECTION PLAYER LOOP part active")
				ENDIF
				#ENDIF
				iPlayersChecked++
				IF iCurrentSlot[MC_playerBD[i].iTeam] < iTeamStartPos[MC_playerBD[i].iTeam] + iMaxPlayersToShow[MC_playerBD[i].iTeam]
					#IF IS_DEBUG_BUILD
					IF bDebugRezHUDPrints
						PRINTLN("[JS] [REZQ] - SORT_PLAYER_LIST_FOR_RESURRECTION PLAYER LOOP iCurrentSlot[",MC_playerBD[i].iTeam,"] = ", iCurrentSlot[MC_playerBD[i].iTeam])
					ENDIF
					#ENDIF
					PLAYER_INDEX playerTemp = NETWORK_GET_PLAYER_INDEX(partTemp)
					IF IS_NET_PLAYER_OK(playerTemp)
					OR NOT IS_BIT_SET(iAlreadyInsertedBS, i)
						playerStateListDataResurrection.uiSlot[iCurrentSlot[MC_playerBD[i].iTeam]].iValue = -1
						playerStateListDataResurrection.uiSlot[iCurrentSlot[MC_playerBD[i].iTeam]].piID = playerTemp
						playerStateListDataResurrection.iTeam[iCurrentSlot[MC_playerBD[i].iTeam]] = MC_playerBD[i].iTeam
						#IF IS_DEBUG_BUILD
						IF bDebugRezHUDPrints
							PRINTLN("[JS] [REZQ] - SORT_PLAYER_LIST_FOR_RESURRECTION - Adding alive player ",i," team: ", MC_playerBD[i].iTeam, " at position: ", iCurrentSlot[MC_playerBD[i].iTeam])
						ENDIF
						#ENDIF
						iCurrentSlot[MC_playerBD[i].iTeam]++
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF iPlayersChecked >= iPlayersToCheck
			BREAKLOOP
		ENDIF
	ENDFOR
ENDPROC

PROC DISPLAY_RESURRECTION_HUD(INT iTeam, INT iRule)
	
	INT iTimeRemaining
	HUD_COLOURS eTimerHudColour = HUD_COLOUR_WHITE
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
		IF HAS_NET_TIMER_STARTED(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeam])					
		AND (MC_serverBD_3.iMultiObjectiveTimeLimit[iTeam] != 0)
			iTimeRemaining = MC_serverBD_3.iMultiObjectiveTimeLimit[iTeam] - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeam])
		ELSE
			iTimeRemaining = GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveTimeLimitRule[iRule]) - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdObjectiveLimitTimer[iTeam])
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciMULTIRULE_TIMER_SUDDEN_DEATH)
			iTimeRemaining = ((GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveTimeLimitRule[iRule]) + GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveSuddenDeathTimeLimit[iRule])) - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdObjectiveLimitTimer[iTeam]) - MC_serverBD_3.iTimerPenalty[iTeam])
		ELSE
			iTimeRemaining = MC_serverBD_3.iMultiObjectiveTimeLimit[iTeam] + GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveSuddenDeathTimeLimit[iRule]) - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeam])
		ENDIF
	ENDIF
	IF iTimeRemaining < 30000
		eTimerHudColour = HUD_COLOUR_RED
		IF iTimeRemaining <= 0
			//Stop timer going negative before sudden death
			iTimeRemaining = 0
		ENDIF
	ENDIF
	
	PLAYER_STATE_LIST_RESURRECTION_DATA playerStateListDataResurrection
	SORT_PLAYER_LIST_FOR_RESURRECTION(playerStateListDataResurrection)
	IF IS_BIT_SET(iLocalBoolCheck21, LBOOL21_RESURRECTION_HUD_SHOW_ENEMY_TEAM) OR IS_LOCAL_PLAYER_ANY_SPECTATOR()
		PLAYER_STATE_LIST_RESURRECTION_DISPLAY_EXTENDED(playerStateListDataResurrection, iTimeRemaining, eTimerHudColour)
	ELSE
		PLAYER_STATE_LIST_RESURRECTION_DISPLAY_CONDENSED(playerStateListDataResurrection, iTimeRemaining, eTimerHudColour)
	ENDIF

ENDPROC

PROC TOGGLE_RESURRECTION_HUD()
	IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_ENTER)
		IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_ENTER) AND NOT IS_PAUSE_MENU_ACTIVE()
			IF NOT IS_BIT_SET(iLocalBoolCheck21, LBOOL21_RESURRECTION_HUD_SHOW_ENEMY_TEAM)
				SET_BIT(iLocalBoolCheck21, LBOOL21_RESURRECTION_HUD_SHOW_ENEMY_TEAM)
			ELSE
				CLEAR_BIT(iLocalBoolCheck21, LBOOL21_RESURRECTION_HUD_SHOW_ENEMY_TEAM)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_RESURRECTION_HUD()
	
	INT iTeam = MC_playerBD[iPartToUse].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF MC_playerBD[iPartToUse].iTeam != -1
	AND NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_FINISHED)
	AND IS_TEAM_ACTIVE(iTeam)
	AND iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(MC_serverbd.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE8_RESPAWN_PLAYERS_ON_KILL)
			AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE8_SHOW_SCORES_TO_SPECTATORS)
				IF NOT SHOULD_HIDE_THE_HUD_THIS_FRAME()
					TOGGLE_RESURRECTION_HUD()
					DISPLAY_RESURRECTION_HUD(iTeam, iRule)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC ACTIVITY_POWERUP GET_SCORE_HEADSHOT_FOR_PLAYER(BOOL bPartToMove, INT iScore)
	IF bPartToMove
		//RETURN ACTIVITY_POWERUP_PED_HEADSHOT
	ENDIF
	
	SWITCH(iScore)
		CASE -1
			RETURN ACTIVITY_POWERUP_PED_HEADSHOT
		CASE 0
			RETURN ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_ZERO_TINT
		CASE 1
			RETURN ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_ONE_TINT
		CASE 2
			RETURN ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_TWO_TINT
		CASE 3
			RETURN ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_THREE_TINT
		CASE 4
			RETURN ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_FOUR_TINT
		CASE 5
			RETURN ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_FIVE_TINT
	ENDSWITCH
	
	RETURN ACTIVITY_POWERUP_NONE
ENDFUNC

FUNC ACTIVITY_POWERUP GET_RUMBLE_HEADSHOT_FOR_PLAYER(BOOL bDead, INT iScore, INT iPart)
	IF bDead
		RETURN ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_ZERO_TINT
	ENDIF
	
	BOOL bShowZero
	
	SWITCH(iScore)
		CASE -1
		CASE 0
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciOVERTIME_ONLY_SHOW_ZERO_POINTS_WHEN_STOPPED)
				bShowZero = TRUE
			ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciOVERTIME_ONLY_SHOW_ZERO_POINTS_WHEN_STOPPED)				
				IF NOT IS_PED_INJURED(localPlayerPed)
					IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
						VEHICLE_INDEX vehPlayer
						vehPlayer = GET_VEHICLE_PED_IS_IN(localPlayerPed)
						IF HAS_VEHICLE_STOPPED_VELOCITY_ANGULAR(vehPlayer)
						AND HAS_VEHICLE_STOPPED_VELOCITY(vehPlayer)
							bShowZero = TRUE
						ENDIF
					ENDIF
				ELSE
					bShowZero = TRUE
				ENDIF
			ENDIF
			
			IF bShowZero			
				IF MC_PlayerBD[iPart].iCurrentOTZoneBD > -1
				AND (MC_PlayerBD[iPart].iCurrentPropHit[0] > -1 OR MC_PlayerBD[iPart].iCurrentPropHit[1] > -1 OR MC_PlayerBD[iPart].iCurrentPropHit[2] > -1)
					RETURN ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_ZERO_TINT
				ENDIF
			ENDIF
			
			RETURN ACTIVITY_POWERUP_PED_HEADSHOT
		CASE 1
			RETURN ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_ONE_TINT
		CASE 2
			RETURN ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_TWO_TINT
		CASE 3
			RETURN ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_THREE_TINT
		CASE 4
			RETURN ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_FOUR_TINT
		CASE 5
			RETURN ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_FIVE_TINT
	ENDSWITCH
	
	RETURN ACTIVITY_POWERUP_NONE
ENDFUNC

FUNC ACTIVITY_POWERUP GET_TICK_HEADSHOT_FOR_PLAYER(BOOL bPartToMove, INT iScore)
	IF bPartToMove
		//RETURN ACTIVITY_POWERUP_PED_HEADSHOT
	ENDIF
	
	SWITCH(iScore)
		CASE -1
			RETURN ACTIVITY_POWERUP_PED_HEADSHOT
		CASE 0
			RETURN ACTIVITY_POWERUP_PED_HEADSHOT_DEAD
		CASE 1
			RETURN ACTIVITY_POWERUP_PED_HEADSHOT_ALIVE
	ENDSWITCH	
	
	RETURN ACTIVITY_POWERUP_NONE
ENDFUNC

PROC DRAW_PENALTY_SCORE_HUD_FOR_TEAM(INT iTeam, HUDORDER eHudOrder, BOOL bTicks)
	PLAYER_INDEX pi[MAX_PENALTY_PLAYERS]
	ACTIVITY_POWERUP ap[MAX_PENALTY_PLAYERS]
	HUD_COLOURS eHudColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam, PlayerToUse)	
	INT iTeamScore = MC_serverBD.iTeamScore[iTeam] - g_FMMC_STRUCT.iInitialPoints[iTeam]
	INT iMaxScore = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMissionTargetScore
	TEXT_LABEL_15 stString = ""
//	IF iMaxScore != FMMC_TARGET_SCORE_OFF //Causing url:bugstar:3402332
//		stString = "TIMER_DASHES"
//	ENDIF

	INT iMaxTurns = MAX_PENALTY_PLAYERS
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
		iMaxTurns = PICK_INT(MC_serverBD.iNumberOfPlayingPlayers[0] > MC_serverBD.iNumberOfPlayingPlayers[1], MC_serverBD.iNumberOfPlayingPlayers[0], MC_serverBD.iNumberOfPlayingPlayers[1])
	ENDIF
		
	INT i
	INT iHUDPos = iMaxTurns - 1
	
	FOR i = 0 TO MAX_PENALTY_PLAYERS - 1
		pi[i] = INVALID_PLAYER_INDEX()
	ENDFOR
	PLAYER_INDEX piToHighlight = INVALID_PLAYER_INDEX()
	
	IF iOTPartToMove > -1
		PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iOTPartToMove)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			piToHighlight = NETWORK_GET_PLAYER_INDEX(tempPart)	
		ENDIF
	ENDIF
	
	//INT iRounds = g_FMMC_STRUCT.iOvertimeRounds
	INT iCurrentRound = MC_serverBD_1.iCurrentRound + 1	
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_OVERTIME_SUDDEN_DEATH_ROUNDS)
		INT iTarget = g_FMMC_STRUCT.iOvertimeRounds
	
		IF g_FMMC_STRUCT.iOvertimeRoundScale > 0
			iTarget = iTarget * (g_FMMC_STRUCT.iOvertimeRoundScale * GET_LARGEST_STARTING_TEAM())
		ENDIF
		
		IF g_FMMC_STRUCT.iOvertimeSDRounds != ciOVERTIME_ROUNDS_UNLIMITED
			iCurrentRound = iCurrentRound - iTarget
			//iRounds = g_FMMC_STRUCT.iOvertimeSDRounds + 1	
		ENDIF
	ENDIF
	
	PLAYER_INDEX piToHighlightBox = PlayerToUse
	IF NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(PLAYER_ID())
	AND NOT IS_PLAYER_SCTV(PLAYER_ID())
		piToHighlightBox = LocalPlayer
	ENDIF
		
	FOR i = 0 TO iMaxTurns - 1
		INT iRealPart = OVERTIME_GET_PARTICIPANT_NUMBER_FROM_TEAM_PARTICIPANT_NUMBER(iTeam, i)
		IF iRealPart > -1
			PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iRealPart)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
				pi[iHUDPos] = tempPlayer
				BOOL bPartToMove = FALSE
				IF iTeam = MC_serverBD_1.iTeamToMove
				AND i = MC_serverBD_1.iPartToMove[MC_serverBD_1.iTeamToMove]
					bPartToMove = TRUE
				ENDIF
				
				INT iScore = -1
				
				iScore = MC_serverBD.iTeamTurnScore[iTeam][i]
				
				IF iCurrentRound > (i+1)
				AND iScore < 0
					iScore = 0
				ENDIF
				
				IF ((i+1) > iCurrentRound AND iScore > -1)
				OR (iTeam != MC_serverBD_1.iTeamToMove AND (i+1) = iCurrentRound AND iScore > -1 AND iTeam = 1) // Rare Special case issue fix. url:bugstar:3683806
					iScore = -1
				ENDIF
				
				IF bTicks
					ap[iHUDPos] = GET_TICK_HEADSHOT_FOR_PLAYER(bPartToMove, iScore)
				ELSE
					ap[iHUDPos] = GET_SCORE_HEADSHOT_FOR_PLAYER(bPartToMove, iScore)
				ENDIF
								
				iHUDPos--
				#IF IS_DEBUG_BUILD
				IF (GET_FRAME_COUNT() % 50) = 0
					PRINTLN("[JS] [TURNS] - DRAW_PENALTY_SCORE_HUD_FOR_TEAM - bTicks = ", bTicks,
					" iTeam = ", iTeam, " iPart = ", iRealPart, " bPartToMove = ", bPartToMove, 
					" Score = ", iScore)
				ENDIF
				#ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
		iCurrentRound = (MAX_PENALTY_PLAYERS - g_FMMC_STRUCT.iOvertimeSDRounds) + iCurrentRound
	ENDIF
	
	DRAW_GENERIC_FIVE_ICON_SCORE_BAR(iTeamScore, 0.0, stString, FALSE, iMaxScore, FALSE, eHudColour, pi[0], pi[1], pi[2], pi[3], pi[4], ap[0], ap[1], ap[2], ap[3], ap[4], eHudOrder, piToHighlightBox, TRUE,
									eHudColour, eHudColour, eHudColour, eHudColour, eHudColour, -1, FALSE, default, piToHighlight, TRUE, DEFAULT, iCurrentRound)
ENDPROC

PROC DRAW_PENALTY_RUMBLE_HUD_FOR_TEAM(INT iTeam, HUDORDER eHudOrder)
	PLAYER_INDEX pi[MAX_PENALTY_PLAYERS]
	ACTIVITY_POWERUP ap[MAX_PENALTY_PLAYERS]
	HUD_COLOURS eHudColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam, PlayerToUse)	
	INT iTeamScore = MC_serverBD.iTeamScore[iTeam] - g_FMMC_STRUCT.iInitialPoints[iTeam]
	INT iMaxScore = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMissionTargetScore
	TEXT_LABEL_15 stString = ""
	INT i
	FOR i = 0 TO MAX_PENALTY_PLAYERS - 1
		pi[i] = INVALID_PLAYER_INDEX()
	ENDFOR

	FOR i = 0 TO MAX_PENALTY_PLAYERS - 1
		INT iRealPart = GET_PARTICIPANT_NUMBER_FROM_TEAM_PARTICIPANT_NUMBER_MC(iTeam, i)
		IF iRealPart > -1
			PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iRealPart)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
				pi[i] = tempPlayer
				INT iScore = MC_PlayerBD[iRealPart].iMyTurnScore[0]
				ap[i] = GET_RUMBLE_HEADSHOT_FOR_PLAYER(NOT IS_NET_PLAYER_OK(tempPlayer), iScore, iRealPart)
								
				#IF IS_DEBUG_BUILD
				IF (GET_FRAME_COUNT() % 50) = 0
					PRINTLN("[JS] [TURNS] - DRAW_PENALTY_RUMBLE_HUD_FOR_TEAM - iTeam = ",
					iTeam, " iPart = ", iRealPart, " Score = ", iScore)
				ENDIF
				#ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	DRAW_GENERIC_FIVE_ICON_SCORE_BAR(iTeamScore, 0.0, stString, FALSE, iMaxScore, FALSE, eHudColour, pi[0], pi[1], pi[2], pi[3], pi[4], ap[0], ap[1], ap[2], ap[3], ap[4], eHudOrder, PlayerToUse, TRUE,
									eHudColour, eHudColour, eHudColour, eHudColour, eHudColour, -1, FALSE)
ENDPROC

FUNC STRING GET_PPG_SCALEFORM_STATE_AS_STRING(PPGenericState eState)
	SWITCH eState
		CASE PPG_REQUEST_ASSETS	RETURN "PPG_REQUEST_ASSETS"
		CASE PPG_INIT			RETURN "PPG_INIT"
		CASE PPG_RUNNING		RETURN "PPG_RUNNING"
	ENDSWITCH
	RETURN "State invalid, maybe GET_PPG_SCALEFORM_STATE_AS_STRING needs updated"
ENDFUNC

PROC SET_PPG_SCALEFORM_STATE(PPGenericState eState)
	PRINTLN("[JS][PPGHUD] - SET_PPG_SCALEFORM_STATE - State changed from ",
			GET_PPG_SCALEFORM_STATE_AS_STRING(ePPGenericState), " TO ",
			GET_PPG_SCALEFORM_STATE_AS_STRING(eState))
	ePPGenericState = eState
ENDPROC

PROC PROCESS_POWER_MAD_HUD()
	INT i
	SWITCH ePPGenericState
		CASE PPG_REQUEST_ASSETS
			IF HAS_SCALEFORM_MOVIE_LOADED(sfiPPGenericMovie)
				SET_PPG_SCALEFORM_STATE(PPG_INIT)
			ELSE
				PRINTLN("[JS][PMHUD] - PROCESS_POWER_MAD_HUD - Requesting Scaleform Movie")
				sfiPPGenericMovie = REQUEST_SCALEFORM_MOVIE("POWER_PLAY_GENERIC")
			ENDIF
		BREAK
		
		CASE PPG_INIT
			FOR i = 0 TO MC_serverBD.iNumberOfTeams - 1
				BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "ADD_TEAM")
					INT iHudColour
					iHudColour = ENUM_TO_INT(GET_HUD_COLOUR_FOR_FMMC_TEAM(i, PlayerToUse))
					PRINTLN("[JS][PMHUD] - PROCESS_POWER_MAD_HUD - Adding team: ", i, " hud colour: ", iHudColour)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iHudColour)
				END_SCALEFORM_MOVIE_METHOD()
			ENDFOR
			
			FOR i = 0 TO MC_serverBD.iNumberOfTeams - 1
				BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "ADD_ICON")
					PRINTLN("[JS][PMHUD] - PROCESS_POWER_MAD_HUD - Adding icon for team: ", i, " icon: ", ciPPGICON_SCORE_RING)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ciPPGICON_SCORE_RING)
				END_SCALEFORM_MOVIE_METHOD()
			ENDFOR
					
			SET_PPG_SCALEFORM_STATE(PPG_RUNNING)
		BREAK
		
		CASE PPG_RUNNING
			FOR i = 0 TO MC_serverBD.iNumberOfTeams - 1
				BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "SET_ICON_SCORE")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(i)	//Icon Index
					INT iScoreToShow
					iScoreToShow = GET_TEAM_SCORE_FOR_DISPLAY(i) - g_FMMC_STRUCT.iInitialPoints[i]
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iScoreToShow) //Score to show
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(i) //Team index for colour
				END_SCALEFORM_MOVIE_METHOD()
				
				BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "SET_ICON_METER")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(i) //Icon Index
					INT j
					FOR j = 0 TO MC_serverBD.iNumberOfTeams - 1
						IF i = j
						AND IS_TEAM_ACTIVE(i)
						AND NOT HAS_TEAM_FAILED(i)
							FLOAT fCaptureProgress
							IF MC_serverBD_4.iCurrentHighestPriority[i] < FMMC_MAX_RULES
							AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[i]], ciBS_RULE9_USE_GRANULAR_CAPTURE)
								fCaptureProgress = TO_FLOAT(MC_serverBD.iGranularCurrentPoints[i]) / TO_FLOAT(GET_FMMC_GRANULAR_CAPTURE_TARGET(MC_serverBD_4.iCurrentHighestPriority[i], i))
							ELSE
								fCaptureProgress = fPowerMadCaptureProgress[i]
							ENDIF
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fCaptureProgress)
						ELSE
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.0)
						ENDIF
					ENDFOR
				END_SCALEFORM_MOVIE_METHOD()
			ENDFOR
		BREAK	
	ENDSWITCH
	
	IF ePPGenericState > PPG_REQUEST_ASSETS
	AND NOT SHOULD_HIDE_CIRCLE_SCORE_HUD_TIMER()
		IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH( "appinternet" ) ) < 1 )
		OR NOT IS_BROWSER_OPEN()
			DRAW_SCALEFORM_MOVIE_FULLSCREEN(sfiPPGenericMovie, 255, 255, 255, 255)
		ENDIF
	ENDIF
ENDPROC

PROC DISPLAY_VEHICLE_RESPAWN_POOL()
	IF NOT SHOULD_HIDE_THE_HUD_THIS_FRAME()
		INT iTeam = MC_playerBD[iPartToUse].iTeam
		INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
		IF iRule < FMMC_MAX_RULES
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_SHOW_VEHICLE_POOL_HUD)
				DRAW_GENERIC_BIG_NUMBER(MC_ServerBD.iVehicleRespawnPool, "VEH_RSP_R", DEFAULT, HUD_COLOUR_WHITE, HUDORDER_FIFTHBOTTOM)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_CIRCLE_SCORE_HUD()
	INT i
	SWITCH ePPGenericState
		CASE PPG_REQUEST_ASSETS
			IF HAS_SCALEFORM_MOVIE_LOADED(sfiPPGenericMovie)
				SET_PPG_SCALEFORM_STATE(PPG_INIT)
			ELSE
				PRINTLN("[JT][HTHUD] - PROCESS_CIRCLE_SCORE_HUD - Requesting Scaleform Movie")
				sfiPPGenericMovie = REQUEST_SCALEFORM_MOVIE("POWER_PLAY_GENERIC")
			ENDIF
		BREAK
		
		CASE PPG_INIT
			FOR i = 0 TO MC_serverBD.iNumberOfTeams - 1
				BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "ADD_TEAM")
					INT iHudColour
					iHudColour = ENUM_TO_INT(GET_HUD_COLOUR_FOR_FMMC_TEAM(i, PlayerToUse))
					PRINTLN("[JT][HTHUD] - PROCESS_CIRCLE_SCORE_HUD - Adding team: ", i, " hud colour: ", iHudColour, " PlayerToUse: ", GET_PLAYER_NAME(PlayerToUse), " PlayerTeam: ", GET_PLAYER_TEAM(PlayerToUse) )
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iHudColour)
				END_SCALEFORM_MOVIE_METHOD()
			ENDFOR
			
			FOR i = 0 TO MC_serverBD.iNumberOfTeams - 1
				BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "ADD_ICON")
					PRINTLN("[JT][HTHUD] - PROCESS_CIRCLE_SCORE_HUD - Adding icon for team: ", i, " icon: ", ciPPGICON_SCORE_RING)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ciPPGICON_SCORE_RING)
				END_SCALEFORM_MOVIE_METHOD()
			ENDFOR
					
			SET_PPG_SCALEFORM_STATE(PPG_RUNNING)
		BREAK
		
		CASE PPG_RUNNING
			FOR i = 0 TO MC_serverBD.iNumberOfTeams - 1
				BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "SET_ICON_SCORE")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(i)	//Icon Index
					
					IF IS_TEAM_ACTIVE(i)
						INT iScoreToShow
						iScoreToShow = GET_TEAM_SCORE_FOR_DISPLAY(i) - g_FMMC_STRUCT.iInitialPoints[i]
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iScoreToShow) //Score to show
					ELSE
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
					ENDIF
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(i) //Team index for colour
				END_SCALEFORM_MOVIE_METHOD()
				
				BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "SET_ICON_METER")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(i) //Icon Index
					INT j
					FOR j = 0 TO MC_serverBD.iNumberOfTeams - 1
						IF i = j
						AND IS_TEAM_ACTIVE(i)
						AND NOT HAS_TEAM_FAILED(i)
						
							FLOAT fScorePercentage
							INT iCurrentScore 
							iCurrentScore = GET_TEAM_SCORE_FOR_DISPLAY(i) - g_FMMC_STRUCT.iInitialPoints[i]
							INT iTargetScore
							INT iRule 
							iRule = MC_serverBD_4.iCurrentHighestPriority[i]
							
							IF iRule < FMMC_MAX_RULES
								IF g_FMMC_STRUCT.sFMMCEndConditions[i].iMissionTargetScore > 0
									iTargetScore = g_FMMC_STRUCT.sFMMCEndConditions[i].iMissionTargetScore
								ELSE
									iTargetScore = GET_FMMC_OBJECTIVE_SCORE_LIMIT_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[i].iTargetScore[0], MC_ServerBD.iNumberOfPlayingPlayers[i])
								ENDIF
								fScorePercentage =  TO_FLOAT(iCurrentScore) / TO_FLOAT(iTargetScore)
								
							ENDIF
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fScorePercentage)
						ELSE
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.0)
						ENDIF
					ENDFOR
				END_SCALEFORM_MOVIE_METHOD()
				
				IF NOT IS_RP_BAR_ACTIVE_IN_ARENA_BOX()
					DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_RANKBAR) 
				ENDIF
			ENDFOR
		BREAK	
	ENDSWITCH
	
	IF ePPGenericState > PPG_REQUEST_ASSETS
	AND NOT SHOULD_HIDE_CIRCLE_SCORE_HUD_TIMER()
		IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH( "appinternet" ) ) < 1 )
		OR NOT IS_BROWSER_OPEN()
			DRAW_SCALEFORM_MOVIE_FULLSCREEN(sfiPPGenericMovie, 255, 255, 255, 255)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_FINAL_COUNTDOWN_ACTIVE(INT iTeam, INT iRule, INT iTimeRemaining)
	
	IF iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule], ciBS_RULE6_COUNTDOWN_TIMER_SOUND_THIRTYSECONDS) AND iTimeRemaining <= 30000
		OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iRule], ciBS_RULE2_COUNTDOWN_TIMER_SOUND_TENSECOND) AND iTimeRemaining <= 10000
		OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iRule], ciBS_RULE2_COUNTDOWN_TIMER_SOUND_FIVESECOND) AND iTimeRemaining <= 5000
		OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iRule], ciBS_RULE2_COUNTDOWN_TIMER_SOUND_ONESHOT) AND iTimeRemaining <= 1000
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PROCESS_CIRCLE_TIMER()

	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]	
	IF iRule >= FMMC_MAX_RULES
		EXIT
	ENDIF

	BOOL bCountdownActive
	fRadialPulsTimePassed = 0.0
	fRadialPulsTimeMax = 0.0
	bShowRadialPuse = FALSE
	
	//Timer
	IF MC_serverBD_3.iMultiObjectiveTimeLimit[iTeam] != 0
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciCIRCLE_TIMER_IGNORE_MULTIRULE)
		IF HAS_NET_TIMER_STARTED(MC_serverBD_3.tdMultiObjectiveLimitTimer[MC_playerBD[iPartToUse].iteam])
			fRadialPulsTimePassed = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeam]))
			fRadialPulsTimeMax = TO_FLOAT(MC_serverBD_3.iMultiObjectiveTimeLimit[iTeam])
			bCountdownActive = IS_FINAL_COUNTDOWN_ACTIVE(iTeam, iRule, GET_TIME_REMAINING_ON_MULTIRULE_TIMER())
		ELSE
			fRadialPulsTimePassed = 0.0
			fRadialPulsTimeMax = 0.0
		ENDIF
	ELSE
		IF HAS_NET_TIMER_STARTED(MC_serverBD_3.tdObjectiveLimitTimer[iTeam]) //Rule timer 
			fRadialPulsTimePassed = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdObjectiveLimitTimer[iTeam]))
			fRadialPulsTimeMax = TO_FLOAT(GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveTimeLimitRule[iRule]))
			bCountdownActive = IS_FINAL_COUNTDOWN_ACTIVE( iTeam, iRule, GET_REMAINING_TIME_ON_RULE(iTeam, iRule))
		ELSE
			fRadialPulsTimePassed = 0.0
			fRadialPulsTimeMax = 0.0
		ENDIF
	ENDIF

	IF IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
	AND (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciSUDDEN_DEATH_THAT_ENDS_WHEN_ONLY_ONE_TEAM_ALIVE)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_SUDDEN_DEATH_KILLALLENEMIES))
		IF HAS_NET_TIMER_STARTED(MC_ServerBD.tdSuddenDeath_ShrinkingBoundsHUDTimer[iTeam])
			fRadialPulsTimePassed = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_ServerBD.tdSuddenDeath_ShrinkingBoundsHUDTimer[iTeam]))
			fRadialPulsTimeMax = TO_FLOAT(g_FMMC_STRUCT.iSphereShrinkTime)
			bCountdownActive = IS_FINAL_COUNTDOWN_ACTIVE(iTeam, iRule, GET_REMAINING_TIME_ON_SHRINKING_BOUNDS(iTeam))
			PRINTLN("[PROCESS_CIRCLE_TIMER] Switching to Sudden Death Timer (Shrinking Bounds) --- fTimePassed: ", fRadialPulsTimePassed, " fTimeMax: ", fRadialPulsTimeMax)
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[iteam].iObjectiveSuddenDeathTimeLimit[iRule] > 0
			fRadialPulsTimePassed = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[iteam]) - MC_serverBD_3.iMultiObjectiveTimeLimit[iTeam])
			fRadialPulsTimeMax = TO_FLOAT(GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iObjectiveSuddenDeathTimeLimit[iRule]))
			INT iTimeRemaining 
			iTimeRemaining = ROUND(fRadialPulsTimeMax - fRadialPulsTimePassed)
			bCountdownActive = IS_FINAL_COUNTDOWN_ACTIVE(iTeam, iRule, iTimeRemaining)
			PRINTLN("[PROCESS_CIRCLE_TIMER] Switching to Sudden Death Timer (multirule) --- fTimePassed: ", fRadialPulsTimePassed, " fTimeMax: ", fRadialPulsTimeMax)
		ENDIF
	ENDIF
	
	IF fRadialPulsTimePassed >= fRadialPulsTimeMax
		fRadialPulsTimePassed = fRadialPulsTimeMax
	ENDIF
	
	PRINTLN("[PROCESS_CIRCLE_TIMER] Timer --- fTimePassed: ", fRadialPulsTimePassed, " fTimeMax: ", fRadialPulsTimeMax)
	
	IF IS_BIT_SET(iLocalBoolCheck24, LBOOL24_PULSED_CIRCLE_HUD_TIMER)
		IF NOT HAS_NET_TIMER_STARTED(tdCircleHUDPulseTimer)
			START_NET_TIMER(tdCircleHUDPulseTimer)
			PRINTLN("[PROCESS_CIRCLE_TIMER] Start pulse timer")
		ENDIF
		IF HAS_NET_TIMER_STARTED(tdCircleHUDPulseTimer)
			IF HAS_NET_TIMER_EXPIRED(tdCircleHUDPulseTimer, ci_CIRCLE_HUD_PULSE_TIME)
				RESET_NET_TIMER(tdCircleHUDPulseTimer)
				CLEAR_BIT(iLocalBoolCheck24, LBOOL24_PULSED_CIRCLE_HUD_TIMER)
				PRINTLN("[PROCESS_CIRCLE_TIMER] Reset pulse timer")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iPartToUse].iteam)
		iRadialPulseCounter = 0
		PRINTLN("[PROCESS_CIRCLE_TIMER] Reset Pulse counter new rule")
	ENDIF
			
	IF fRadialPulsTimePassed < fRadialPulsTimeMax
	AND fRadialPulsTimePassed > 0
	AND (HAS_NET_TIMER_STARTED(MC_serverBD_3.tdObjectiveLimitTimer[iTeam]) OR HAS_NET_TIMER_STARTED(MC_serverBD_3.tdMultiObjectiveLimitTimer[iteam]))
	AND NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iPartToUse].iteam)
		IF bCountDownActive
		AND NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_PULSED_CIRCLE_HUD_TIMER)
		AND iRadialPulseCounter < 5
			SET_BIT(iLocalBoolCheck24, LBOOL24_PULSED_CIRCLE_HUD_TIMER)
			iRadialPulseCounter++
			bShowRadialPuse = TRUE
			PRINTLN("[PROCESS_CIRCLE_TIMER] Show/Play pulse HUD/SOUND: iRadialPulseCounter: ", iRadialPulseCounter)
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_CIRCLE_TIMER_SOUND()

	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]	
	IF iRule >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	IF bShowRadialPuse
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_VEHICULAR_WARFARE_SOUNDS)					
			PLAY_SOUND_FRONTEND(-1, "Bounds_Timer_Pulse", "DLC_SM_VEHWA_Player_Sounds", FALSE)
			PRINTLN("[PROCESS_CIRCLE_TIMER][PROCESS_CIRCLE_TIMER_SOUND] Play  TIMER_RADIAL_Pulse from ciENABLE_VEHICULAR_WARFARE_SOUNDS")
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_ENABLE_TRAP_DOOR_SOUNDS)
		AND NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
			PLAY_SOUND_FRONTEND(-1, "TIMER_RADIAL_Pulse", "DLC_AS_TRP_Sounds", FALSE)
			PRINTLN("[PROCESS_CIRCLE_TIMER][PROCESS_CIRCLE_TIMER_SOUND] Play  TIMER_RADIAL_Pulse from ciOptionsBS18_ENABLE_TRAP_DOOR_SOUNDS")
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_SUMO_RUN_SOUNDS)
		AND NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
			PLAY_SOUND_FRONTEND(-1, "TIMER_RADIAL_Pulse", "DLC_BTL_SM_Remix_Soundset", FALSE)
			PRINTLN("[PROCESS_CIRCLE_TIMER][PROCESS_CIRCLE_TIMER_SOUND] Play  TIMER_RADIAL_Pulse from ciOptionsBS19_ENABLE_SUMO_RUN_SOUNDS")
		ENDIF
	ENDIF
	//RESET
	BOOL bSuddenDeath = ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
	IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iPartToUse].iteam)
	OR bSuddenDeath
		IF NOT bSuddenDeath 
		OR NOT IS_BIT_SET(iLocalBoolCheck28, LBOOL28_LAST_TIMER_RESET_FOR_SD)
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_ENABLE_TRAP_DOOR_SOUNDS)
				PLAY_SOUND_FRONTEND(-1, "TIMER_RADIAL_Reset", "DLC_AS_TRP_Sounds", FALSE)
			ELSE
				PLAY_SOUND_FRONTEND(-1, "TIMER_RADIAL_Reset", "DLC_BTL_SM_Remix_Soundset", FALSE)
			ENDIF
			PRINTLN("[PROCESS_CIRCLE_TIMER][PROCESS_CIRCLE_TIMER_SOUND] PLAY_SOUND_FRONTEND TIMER_RADIAL_Reset")
			IF bSuddenDeath
				SET_BIT(iLocalBoolCheck28, LBOOL28_LAST_TIMER_RESET_FOR_SD)
				PRINTLN("[PROCESS_CIRCLE_TIMER][PROCESS_CIRCLE_TIMER_SOUND] PULSE CHECK: Setting LBOOL28_LAST_TIMER_RESET_FOR_SD")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Shows a single circle icon to be used as a timer. This will prioritise the mutlirule timer over the rule timer.
PROC PROCESS_CIRCLE_TIMER_HUD()

	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]	
	
	IF iRule >= FMMC_MAX_RULES
		EXIT
	ENDIF

	SWITCH ePPGenericState
		CASE PPG_REQUEST_ASSETS
			IF HAS_SCALEFORM_MOVIE_LOADED(sfiPPGenericMovie)
				SET_PPG_SCALEFORM_STATE(PPG_INIT)
			ELSE
				PRINTLN("[JT][HTHUD] - PROCESS_CIRCLE_TIMER_HUD - Requesting Scaleform Movie")
				sfiPPGenericMovie = REQUEST_SCALEFORM_MOVIE("POWER_PLAY_GENERIC")
			ENDIF
		BREAK
		
		CASE PPG_INIT
			
			BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "ADD_TEAM")			
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_WHITE))
			END_SCALEFORM_MOVIE_METHOD()
			BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "ADD_TEAM")			
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_RED))
			END_SCALEFORM_MOVIE_METHOD()
			BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "ADD_ICON")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ciPPGICON_BLANK)
			END_SCALEFORM_MOVIE_METHOD()
			
			SET_PPG_SCALEFORM_STATE(PPG_RUNNING)
		BREAK
		
		CASE PPG_RUNNING
						
			BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "SET_ICON_TIMER")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) //Icon index
				
				IF fRadialPulsTimePassed <= ((fRadialPulsTimeMax/4)*3)
				AND fRadialPulsTimePassed >= 0
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fRadialPulsTimePassed / fRadialPulsTimeMax) //Value
				ELSE
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0) //Value
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fRadialPulsTimePassed / fRadialPulsTimeMax) //Value
				ENDIF
			END_SCALEFORM_MOVIE_METHOD()
			
			
			IF bShowRadialPuse
				BEGIN_SCALEFORM_MOVIE_METHOD (sfiPPGenericMovie, "PULSE_ICON")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
				END_SCALEFORM_MOVIE_METHOD()
				PRINTLN("[PROCESS_CIRCLE_TIMER][PROCESS_CIRCLE_TIMER_HUD] Showed Pulse Icon")
			ENDIF
		BREAK	
	ENDSWITCH
	
	IF ePPGenericState > PPG_REQUEST_ASSETS
	AND NOT SHOULD_HIDE_CIRCLE_SCORE_HUD_TIMER()
		IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH( "appinternet" ) ) < 1 )
		OR NOT IS_BROWSER_OPEN()
			DRAW_SCALEFORM_MOVIE_FULLSCREEN(sfiPPGenericMovie, 255, 255, 255, 255)
		ENDIF
	ENDIF
ENDPROC

FUNC FLOAT GET_SCORE_PERCENTAGE_FOR_HUD(INT iTeam)
	IF iTeam < FMMC_MAX_TEAMS
		FLOAT fScorePercentage
		INT iCurrentScore 
		iCurrentScore = GET_TEAM_SCORE_FOR_DISPLAY(iTeam) - g_FMMC_STRUCT.iInitialPoints[iTeam]
		INT iTargetScore
		INT iRule 
		iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		
		IF iRule < FMMC_MAX_RULES
			IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMissionTargetScore > 0
				iTargetScore = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMissionTargetScore
			ELSE
				iTargetScore = GET_FMMC_OBJECTIVE_SCORE_LIMIT_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTargetScore[0], MC_ServerBD.iNumberOfPlayingPlayers[iTeam])
			ENDIF
			
			fScorePercentage =  TO_FLOAT(iCurrentScore) / TO_FLOAT(iTargetScore)
			
		ENDIF
		
		RETURN fScorePercentage
	ENDIF
	RETURN 0.0
ENDFUNC

PROC CIRCLE_HUD_SET_UP_METERED_TEAM_SCORE(INT iIconIndex, INT iTeam)
	
	IF IS_TEAM_ACTIVE(iTeam)
	AND NOT HAS_TEAM_FAILED(iTeam)
		PRINTLN("[JT][CIRCLE HUD] Calling CIRCLE_HUD_SET_UP_METERED_TEAM_SCORE for team: ", iTeam)
		BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "SET_ICON_SCORE")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iIconIndex)	//Icon Index
			INT iScoreToShow
			iScoreToShow = GET_TEAM_SCORE_FOR_DISPLAY(iTeam) - g_FMMC_STRUCT.iInitialPoints[iTeam]
			
			// This is required due to a Score reset/Cache system which sets the teamscore back to 0. iPoints given to pass would make it appear as -1 here.
			IF iScoreToShow <= -1
			AND MC_ServerBD.iPointsGivenToPass[iTeam] > 0
				iScoreToShow = 0
			ENDIF
			
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iScoreToShow) //Score to show
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iIconIndex) //Team index for colour
		END_SCALEFORM_MOVIE_METHOD()
		
		BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "SET_ICON_METER")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iIconIndex) //Icon Index
			INT j
			FOR j = 0 TO MC_serverBD.iNumberOfTeams
				INT iTeamToUse = j
				IF iTeamToUse > CEIL(TO_FLOAT(MC_serverBD.iNumberOfTeams)/2)
					iTeamToUse -= 1
				ENDIF
				
				IF iTeam = iTeamToUse
				AND IS_TEAM_ACTIVE(iTeam)
				AND NOT HAS_TEAM_FAILED(iTeam)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(GET_SCORE_PERCENTAGE_FOR_HUD(iTeam))
				ELSE
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.0)
				ENDIF
			ENDFOR
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

PROC CIRCLE_HUD_SET_UP_TIMER_ICON(INT iIconIndex, INT iTeam)
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	FLOAT fTimePassed, fTimeMax
	
	IF iRule < FMMC_MAX_RULES
		IF MC_serverBD_3.iMultiObjectiveTimeLimit[iTeam] != 0
		AND NOT (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciCIRCLE_TIMER_IGNORE_MULTIRULE) AND HAS_NET_TIMER_STARTED(MC_serverBD_3.tdObjectiveLimitTimer[iTeam]))
			IF HAS_NET_TIMER_STARTED(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeam])
				fTimePassed = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeam]))
				fTimeMax = TO_FLOAT(MC_serverBD_3.iMultiObjectiveTimeLimit[iTeam])
			ELSE
				fTimePassed = 0.0
				fTimeMax = 0.0
			ENDIF
		ELSE
			IF HAS_NET_TIMER_STARTED(MC_serverBD_3.tdObjectiveLimitTimer[iTeam]) //Rule timer 
				fTimePassed = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdObjectiveLimitTimer[iTeam]))
				fTimeMax = TO_FLOAT(GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveTimeLimitRule[iRule]))
			ELSE
				fTimePassed = 0.0
				fTimeMax = 0.0
			ENDIF
		ENDIF
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "SET_ICON_TIMER")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iIconIndex) //Icon index
		INT iIndex
		FOR iIndex = 0 TO MC_serverBD.iNumberOfTeams
			IF iIndex = CEIL(TO_FLOAT(MC_serverBD.iNumberOfTeams)/2)
				IF fTimePassed / fTimeMax < 0.85
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fTimePassed / fTimeMax) //Value
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
				ELSE
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fTimePassed / fTimeMax) //Value
				ENDIF
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.0)
			ENDIF
		ENDFOR

	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC CIRCLE_HUD_ADD_TEAM_COLOUR_AND_ICON(INT iTeam)
	
	IF IS_TEAM_ACTIVE(iTeam)
	AND NOT HAS_TEAM_FAILED(iTeam)
		PRINTLN("[JT][CIRCLE HUD] Calling CIRCLE_HUD_ADD_TEAM_COLOUR_AND_ICON for team: ", iTeam)
		BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "ADD_TEAM")
			INT iHudColour
			iHudColour = ENUM_TO_INT(GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam, PlayerToUse))
			
			PRINTLN("[JT][HTHUD] - PROCESS_CIRCLE_SCORE_HUD - Adding team: ", iTeam, " hud colour: ", iHudColour)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iHudColour)
		END_SCALEFORM_MOVIE_METHOD()
		
		BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "ADD_ICON")
			PRINTLN("[JT][HTHUD] - PROCESS_CIRCLE_SCORE_HUD - Adding icon for team: ", iTeam, " icon: ", ciPPGICON_SCORE_RING)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ciPPGICON_SCORE_RING)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC
	
PROC PROCESS_CIRCLE_SCORE_HUD_WITH_TIMER()
	
	SWITCH ePPGenericState
		CASE PPG_REQUEST_ASSETS
			IF HAS_SCALEFORM_MOVIE_LOADED(sfiPPGenericMovie)
				SET_PPG_SCALEFORM_STATE(PPG_INIT)
			ELSE
				PRINTLN("[JT][HTHUD] - PROCESS_CIRCLE_SCORE_HUD_WITH_TIMER - Requesting Scaleform Movie")
				sfiPPGenericMovie = REQUEST_SCALEFORM_MOVIE("POWER_PLAY_GENERIC")
			ENDIF
		BREAK
		
		CASE PPG_INIT
			
			IF MC_serverBD.iNumberOfTeams <= 2
				PRINTLN("[JT][HTHUD] - PROCESS_CIRCLE_SCORE_HUD_WITH_TIMER - <= 2 teams: ", MC_serverBD.iNumberOfTeams)
				//Team 0
				CIRCLE_HUD_ADD_TEAM_COLOUR_AND_ICON(0)
				//Timer
				BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "ADD_TEAM")			
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_WHITE))
				END_SCALEFORM_MOVIE_METHOD()
				BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "ADD_ICON")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ciPPGICON_BLANK)
				END_SCALEFORM_MOVIE_METHOD()
				//Team 1
				CIRCLE_HUD_ADD_TEAM_COLOUR_AND_ICON(1)
				
				//Adding Red Timer "team"
				BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "ADD_TEAM")			
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_RED))
				END_SCALEFORM_MOVIE_METHOD()
			ELSE
				PRINTLN("[JT][HTHUD] - PROCESS_CIRCLE_SCORE_HUD_WITH_TIMER - More Than 2 teams: ", MC_serverBD.iNumberOfTeams)
				//Team 0
				CIRCLE_HUD_ADD_TEAM_COLOUR_AND_ICON(0)
				//Team 1
				CIRCLE_HUD_ADD_TEAM_COLOUR_AND_ICON(1)
				//Timer
				BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "ADD_TEAM")			
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_WHITE))
				END_SCALEFORM_MOVIE_METHOD()
				BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "ADD_ICON")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ciPPGICON_BLANK)
				END_SCALEFORM_MOVIE_METHOD()
				//Team 2
				CIRCLE_HUD_ADD_TEAM_COLOUR_AND_ICON(2)
				//Team 3
				CIRCLE_HUD_ADD_TEAM_COLOUR_AND_ICON(3)
				
			ENDIF
					
			SET_PPG_SCALEFORM_STATE(PPG_RUNNING)
		BREAK
		
		CASE PPG_RUNNING
			INT iIconIndex, iTeamToUse

			FOR iIconIndex = 0 TO MC_serverBD.iNumberOfTeams
				iTeamToUse = iIconIndex
				IF iTeamToUse > CEIL(TO_FLOAT(MC_serverBD.iNumberOfTeams)/2)
					iTeamToUse -= 1
				ENDIF
				
				IF iIconIndex = CEIL(TO_FLOAT(MC_serverBD.iNumberOfTeams)/2)
					CIRCLE_HUD_SET_UP_TIMER_ICON(iIconIndex, MC_playerBD[iPartToUse].iteam)
				ELSE
					IF iTeamToUse < FMMC_MAX_TEAMS
						CIRCLE_HUD_SET_UP_METERED_TEAM_SCORE(iIconIndex,iTeamToUse)
					ENDIF
				ENDIF
			ENDFOR
			
		BREAK	
	ENDSWITCH
	
	IF ePPGenericState > PPG_REQUEST_ASSETS
	AND NOT SHOULD_HIDE_CIRCLE_SCORE_HUD_TIMER()
		IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH( "appinternet" ) ) < 1 )
		OR NOT IS_BROWSER_OPEN()
			DRAW_SCALEFORM_MOVIE_FULLSCREEN(sfiPPGenericMovie, 255, 255, 255, 255)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_TONY_HAWKS_HUD()
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_StuntRunMode)
		EXIT
	ENDIF
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]	
	IF iRule >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	fRadialPulsTimePassed = 0.0
	fRadialPulsTimeMax = 0.0
	bShowRadialPuse = FALSE
	
	//Timer
	IF HAS_NET_TIMER_STARTED(MC_playerBD_1[iLocalPart].tdComboTimer)
		fRadialPulsTimePassed = ciTHPSTimer - TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_playerBD_1[iLocalPart].tdComboTimer))
		fRadialPulsTimeMax = TO_FLOAT(ciTHPSTimer)
	ELSE
		fRadialPulsTimePassed = 0.0
		fRadialPulsTimeMax = 0.0
	ENDIF
	
	IF bStuntLanded
		bShowRadialPuse = TRUE
		bStuntLanded = FALSE
	ENDIF

	SWITCH ePPGenericState
		CASE PPG_REQUEST_ASSETS
			IF HAS_SCALEFORM_MOVIE_LOADED(sfiPPGenericMovie)
				SET_PPG_SCALEFORM_STATE(PPG_INIT)
			ELSE
				PRINTLN("[JT][HTHUD] - PROCESS_CIRCLE_TIMER_HUD - Requesting Scaleform Movie")
				sfiPPGenericMovie = REQUEST_SCALEFORM_MOVIE("POWER_PLAY_GENERIC")
			ENDIF
		BREAK
		
		CASE PPG_INIT
			
			BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "ADD_TEAM")			
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_YELLOW))
			END_SCALEFORM_MOVIE_METHOD()
			BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "ADD_TEAM")			
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_RED))
			END_SCALEFORM_MOVIE_METHOD()
			BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "ADD_ICON")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ciPPGICON_BLANK)
			END_SCALEFORM_MOVIE_METHOD()
			
			SET_PPG_SCALEFORM_STATE(PPG_RUNNING)
		BREAK
		
		CASE PPG_RUNNING
						
			BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "SET_ICON_TIMER")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) //Icon index
				
				IF fRadialPulsTimePassed >= ((fRadialPulsTimeMax/4))
				AND fRadialPulsTimePassed >= 0
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fRadialPulsTimePassed / fRadialPulsTimeMax) //Value
				ELSE
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0) //Value
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fRadialPulsTimePassed / fRadialPulsTimeMax) //Value
				ENDIF
			END_SCALEFORM_MOVIE_METHOD()
			
			
			IF bShowRadialPuse
				BEGIN_SCALEFORM_MOVIE_METHOD (sfiPPGenericMovie, "PULSE_ICON")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
				END_SCALEFORM_MOVIE_METHOD()
				PRINTLN("[PROCESS_CIRCLE_TIMER][PROCESS_CIRCLE_TIMER_HUD] Showed Pulse Icon")
			ENDIF
		BREAK	
	ENDSWITCH
	
	IF ePPGenericState > PPG_REQUEST_ASSETS
	AND NOT SHOULD_HIDE_CIRCLE_SCORE_HUD_TIMER()
		IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH( "appinternet" ) ) < 1 )
		OR NOT IS_BROWSER_OPEN()
			DRAW_SCALEFORM_MOVIE_FULLSCREEN(sfiPPGenericMovie, 255, 255, 255, 255)
		ENDIF
	ENDIF
	
	DRAW_GENERIC_BIG_NUMBER(MC_PlayerBD[iLocalPart].iPlayerScore, "LBD_SCO")
	DRAW_GENERIC_BIG_NUMBER(MC_playerBD_1[iLocalPart].iStuntComboMultiplier, "MULTIPLIER")
ENDPROC

FUNC ACTIVITY_POWERUP GET_TEAM_TURN_HEADSHOT_FOR_PLAYER(BOOL bTurnTaken, BOOL bWon, BOOL bCurrentTurn)
	IF bTurnTaken
		IF bWon
			RETURN ACTIVITY_POWERUP_PED_HEADSHOT_ALIVE
		ENDIF
		RETURN ACTIVITY_POWERUP_PED_HEADSHOT_DEAD
	ENDIF
	IF bCurrentTurn
		RETURN ACTIVITY_POWERUP_PED_HEADSHOT
	ELSE
		RETURN ACTIVITY_POWERUP_PED_HEADSHOT_FADED
	ENDIF
ENDFUNC

PROC DRAW_TEAM_TURN_HUD()
	PLAYER_INDEX pi[MAX_AIRSHOOTOUT_TEAMS][MAX_PENALTY_PLAYERS]
	ACTIVITY_POWERUP ap[MAX_AIRSHOOTOUT_TEAMS][MAX_PENALTY_PLAYERS]
	INT iTeamScore[MAX_AIRSHOOTOUT_TEAMS]
	PLAYER_INDEX piToHighlight[MAX_AIRSHOOTOUT_TEAMS]
	PLAYER_INDEX piToHighlightBox = LocalPlayer
	IF ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
		EXIT
	ENDIF
	
	INT iRealPartToUse = iPartToUse
	IF USING_HEIST_SPECTATE()
		PED_INDEX tempPed = GET_SPECTATOR_SELECTED_PED()
		IF IS_PED_A_PLAYER(tempPed)
			PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(tempPed)
			IF NETWORK_IS_PLAYER_A_PARTICIPANT(tempPlayer)
				PARTICIPANT_INDEX tempPart = NETWORK_GET_PARTICIPANT_INDEX(tempPlayer)
				iRealPartToUse = NATIVE_TO_INT(tempPart)
			ENDIF
		ENDIF
	ENDIF
	
	INT iTeam
	FOR iTeam = 0 TO MAX_AIRSHOOTOUT_TEAMS - 1
		BOOL bPulse = FALSE
		HUDORDER eHudOrder = HUDORDER_SEVENTHBOTTOM
		IF NOT DOES_TEAM_LIKE_TEAM(MC_playerBD[iRealPartToUse].iteam, iTeam)
			eHudOrder = HUDORDER_SIXTHBOTTOM
			bPulse = FALSE
		ENDIF
		HUD_COLOURS eHudColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam, PlayerToUse)	
		iTeamScore[iTeam] = MC_serverBD.iTeamScore[iTeam] - g_FMMC_STRUCT.iInitialPoints[iTeam]
		piToHighlight[iTeam] = MC_serverBD.piTurnsActivePlayer[iTeam]
		INT iHUDPos = MAX_PENALTY_PLAYERS - 1
		INT i
		FOR i = 0 TO MAX_PENALTY_PLAYERS - 1
			INT iPart = GET_THIS_TURN_PART_ON_TEAM(iTeam, i)
			pi[iTeam][iHUDPos] = INVALID_PLAYER_INDEX()
			IF iPart > -1
				PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iPart)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
					PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
					pi[iTeam][iHUDPos] = tempPlayer
					BOOL bTurnTaken = (i < MC_serverBD_1.iCurrentRound)
					BOOL bCurrentTurn = (MC_serverBD_1.iCurrentRound = i)
					BOOL bWon = IS_BIT_SET(MC_serverBD_3.iTeamTurnsRoundWonBS[iTeam], i)
					ap[iTeam][iHUDPos] = GET_TEAM_TURN_HEADSHOT_FOR_PLAYER(bTurnTaken, bWon, bCurrentTurn)
					PRINTLN("DRAW_TEAM_TURN_HUD - Team ", iTeam, ", turn ", i, " part ", iPart, " bTurnTaken: ", bTurnTaken, " bCurrentTurn: ", bCurrentTurn, " bWon: ", bWon)
					iHUDPos--
				ENDIF
			ENDIF
		ENDFOR
		DRAW_GENERIC_FIVE_ICON_SCORE_BAR(iTeamScore[iTeam], 0.0, "", FALSE, 0, FALSE, eHudColour, pi[iTeam][0], pi[iTeam][1], pi[iTeam][2], pi[iTeam][3], pi[iTeam][4],
										ap[iTeam][0], ap[iTeam][1], ap[iTeam][2], ap[iTeam][3], ap[iTeam][4], eHudOrder, piToHighlightBox, TRUE,
										eHudColour, eHudColour, eHudColour, eHudColour, eHudColour, -1, FALSE, DEFAULT, piToHighlight[iTeam], bPulse, DEFAULT, MC_serverBD_1.iCurrentRound)
	ENDFOR
ENDPROC

PROC PROCESS_HIDE_SPECIFIC_HELP_TEXT_IN_PHONE()
	IF IS_CELLPHONE_CAMERA_IN_USE()
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("OPPT_HELP_1")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("OPPT_HELP_2")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("OPPT_HELP_3")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("OPPT_HELP_4")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("WVM_OPP_2")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("WVM_OPP_5")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MC_SCORE_H1")
			HIDE_HELP_TEXT_THIS_FRAME()
		ENDIF
	ENDIF
ENDPROC



PROC PROCESS_INCREMENTAL_RULE_FAIL_TIMER()
	
	INT iTeam, iTeamLoop, iRule	
	iTeam = MC_PlayerBD[iPartToUse].iteam	
	iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]	
	
	IF bIsLocalPlayerHost		
		FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS-1
			IF IS_TEAM_ACTIVE(iTeamLoop)
				iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeamLoop]
				
				// Migration...
				IF iModifiedTimeElapsed[iTeamLoop] < MC_ServerBD_4.iRuleTimerFailElapsedTime[iTeamLoop]
					iModifiedTimeElapsed[iTeamLoop] = MC_ServerBD_4.iRuleTimerFailElapsedTime[iTeamLoop]
				ENDIF
				
				#IF IS_DEBUG_BUILD
				IF (GET_FRAME_COUNT() % 10) = 0
					PRINTLN("[LM][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - Before_________________________________________________ for iTeam: ", iTeamLoop)
				ENDIF
				#ENDIF
				
				IF iRule < FMMC_MAX_RULES
					IF iFTTimeElapsedLastFrame > 0
					AND NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeamLoop)
					AND g_FMMC_STRUCT.sFMMCEndConditions[iTeamLoop].sRuleIncTimerFail[iRule].iStartTime > 0
						IF NOT HAS_NET_TIMER_STARTED(MC_ServerBD_4.tdIncrementalRuleFailTimer[iTeamLoop])
							START_NET_TIMER(MC_ServerBD_4.tdIncrementalRuleFailTimer[iTeamLoop])
						ENDIF
						INT iTimeMax = g_FMMC_STRUCT.sFMMCEndConditions[iTeamLoop].sRuleIncTimerFail[iRule].iStartTime
						INT iTimeElapsed = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_ServerBD_4.tdIncrementalRuleFailTimer[iTeamLoop])
						INT iTimeLastFrame = GET_GAME_TIMER() - iFTTimeElapsedLastFrame
						INT iTimeLastFrameMod
						FLOAT fMult = (TO_FLOAT(iCurrentPlacedPedsAlive) / TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeamLoop].sRuleIncTimerFail[iRule].iPlacedPedAmountThresh))
						FLOAT fTimeProgressModifier = 1.0
						
						#IF IS_DEBUG_BUILD
						IF (GET_FRAME_COUNT() % 10) = 0
							PRINTLN("[LM][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - iTimeElapsed: ", iTimeElapsed)
							PRINTLN("[LM][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - iCurrentPlacedPedsAlive: ", iCurrentPlacedPedsAlive)
							PRINTLN("[LM][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - iTimeLastFrame: ", iTimeLastFrame)			
							PRINTLN("[LM][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - fMult: ", fMult)
							PRINTLN("[LM][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - Thresh: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeamLoop].sRuleIncTimerFail[iRule].iPlacedPedAmountThresh)
							PRINTLN("[LM][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - Rate to * by Mult: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeamLoop].sRuleIncTimerFail[iRule].fPlacedPedAmountRateInc)
						ENDIF
						#ENDIF
						
						IF fMult > 1
						AND g_FMMC_STRUCT.sFMMCEndConditions[iTeamLoop].sRuleIncTimerFail[iRule].fPlacedPedAmountRateInc <> 0
							fTimeProgressModifier += g_FMMC_STRUCT.sFMMCEndConditions[iTeamLoop].sRuleIncTimerFail[iRule].fPlacedPedAmountRateInc * fMult
							
							IF fTimeProgressModifier < 0.0
								fTimeProgressModifier = 0.05
								PRINTLN("[LM][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - fTimeProgressModifier is: ", fTimeProgressModifier, " Capping at 0.05")							
								ASSERTLN("[LM] - fTimeProgerssModifier is too low, capping to 0.05. Content, please adjust the Rule Fail Timer settings.")
							ENDIF
							
							iTimeLastFrameMod = ROUND((TO_FLOAT(iTimeLastFrame) * fTimeProgressModifier))
																
							iModifiedTimeElapsed[iTeamLoop] += (iTimeLastFrameMod-iTimeLastFrame)
						ENDIF
																	
						IF (GET_FRAME_COUNT() % 30) = 0
							PRINTLN("[LM][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - ADDING TO SERVER BD NOW ")
							MC_ServerBD_4.iRuleTimerFailElapsedTime[iTeamLoop] = iModifiedTimeElapsed[iTeamLoop]
						ENDIF
							
						iTimeElapsed += iModifiedTimeElapsed[iTeamLoop]
						
						#IF IS_DEBUG_BUILD	
						IF (GET_FRAME_COUNT() % 10) = 0
							PRINTLN("[LM][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - After_________________________________________________")
							PRINTLN("[LM][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - iTimeLastFrameMod: ", iTimeLastFrameMod)
							PRINTLN("[LM][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - iModifiedTimeElapsed: ", iModifiedTimeElapsed[iTeamLoop])
							PRINTLN("[LM][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - fTimeProgressModifier: ", fTimeProgressModifier)
							PRINTLN("[LM][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - iTimeElapsed: ", iTimeElapsed)			
						ENDIF
						#ENDIF
						
						IF iTimeElapsed > iTimeMax
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeamLoop].sRuleIncTimerFail[iRule].iIncTimerBS, ciBS_INC_TIMER_SHOULD_PASS_INSTEAD)
								SET_BIT(MC_serverBD.iServerBitSet,SSBOOL_TEAM0_FINISHED + iTeamLoop)
								PRINTLN("[LM][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - Passing Mission...")
							ELSE							
								SET_TEAM_FAILED(iTeamLoop)
								PRINTLN("[LM][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - Failing Mission...")
							ENDIF
							
							iModifiedTimeElapsed[iTeamLoop] = 0
							RESET_NET_TIMER(MC_ServerBD_4.tdIncrementalRuleFailTimer[iTeamLoop])
							MC_ServerBD_4.iRuleTimerFailElapsedTime[iTeamLoop] = 0
						ENDIF
					ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeamLoop].sRuleIncTimerFail[iRule].iStartTime <= 0
						IF HAS_NET_TIMER_STARTED(MC_ServerBD_4.tdIncrementalRuleFailTimer[iTeamLoop])
							PRINTLN("[LM][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - Cleaning up... 1")
							RESET_NET_TIMER(MC_ServerBD_4.tdIncrementalRuleFailTimer[iTeamLoop])
							MC_ServerBD_4.iRuleTimerFailElapsedTime[iTeamLoop] = 0
						ENDIF					
						IF iModifiedTimeElapsed[iTeamLoop] != 0
							iModifiedTimeElapsed[iTeamLoop] = 0
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	IF iRule < FMMC_MAX_RULES
	AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sRuleIncTimerFail[iRule].iStartTime > 0
	AND MC_ServerBD_4.iRuleTimerFailElapsedTime[iTeam] != 0
	AND HAS_NET_TIMER_STARTED(MC_ServerBD_4.tdIncrementalRuleFailTimer[iTeam])	
	AND NOT SHOULD_HIDE_THE_HUD_THIS_FRAME()
		INT iTimeMax = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sRuleIncTimerFail[iRule].iStartTime
		INT iTimeElapsed = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_ServerBD_4.tdIncrementalRuleFailTimer[iTeam]) + MC_ServerBD_4.iRuleTimerFailElapsedTime[iTeam]
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sRuleIncTimerFail[iRule].iIncTimerBS, ciBS_INC_TIMER_ENABLE_CHOPPY_MODE)
			iFTCurrentTimerProgress[iTeam] = ROUND(iFTCurrentTimerProgress[iTeam] +@ (iTimeElapsed - iFTCurrentTimerProgress[iTeam]))
		ELSE
			IF NOT HAS_NET_TIMER_STARTED(tdUpdateProgressBarChoppy)
				START_NET_TIMER(tdUpdateProgressBarChoppy)
			ENDIf
			
			IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdUpdateProgressBarChoppy, 3000)
				iFTCurrentTimerProgress[iTeam] = iTimeElapsed
				RESET_NET_TIMER(tdUpdateProgressBarChoppy)
				PRINTLN("[LM][PROCESS_INCREMENTAL_RULE_FAIL_TIMER] - Choppy update time. iTimeElapsed: ", iTimeElapsed)		
			ENDIF
		ENDIF
		
		IF HAS_TEAM_FAILED(iTeam)
		OR HAS_TEAM_FINISHED(MC_playerBD[iPartToUse].iteam)
		OR IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_FINISHED )
		OR GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
		OR IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
			iFTCurrentTimerProgress[iTeam] = iTimeMax
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sRuleIncTimerFail[iRule].iIncTimerBS, ciBS_INC_TIMER_DEPLETE_INSTEAD_OF_FILL)
			DRAW_GENERIC_METER((iTimeMax-iFTCurrentTimerProgress[iTeam]), iTimeMax, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63ObjectiveEntityHUDMessage[iRule])
		ELSE
			DRAW_GENERIC_METER(iFTCurrentTimerProgress[iTeam], iTimeMax, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63ObjectiveEntityHUDMessage[iRule])
		ENDIF
	ENDIF
	
	// Store the game timer so we can calculate how much time has passed from one frame to the other.
	iFTTimeElapsedLastFrame = GET_GAME_TIMER()
ENDPROC

FUNC BOOL CAN_CHANGE_SHOWDOWN_POINTS_BAR_COLOUR()
	IF HAS_NET_TIMER_STARTED(stShowdown_BarSwitchTimer)
		IF HAS_NET_TIMER_EXPIRED(stShowdown_BarSwitchTimer, 750)
			RESET_NET_TIMER(stShowdown_BarSwitchTimer)
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DRAW_SHOWDOWN_MY_POINTS_BAR(FLOAT &fCurrentPoints)
	INT iFlashTime = -1
	FLOAT fBuffer = 0.45
	
	IF CAN_CHANGE_SHOWDOWN_POINTS_BAR_COLOUR()
		IF fShowdown_DisplayPoints < fCurrentPoints - fBuffer
			hcShowdown_BarColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_playerBD[iPartToUse].iteam, PlayerToUse)
			REINIT_NET_TIMER(stShowdown_BarSwitchTimer)
		ELSE	
			IF fShowdown_DisplayPoints > fCurrentPoints + fBuffer
			AND MC_playerBD_1[iPartToUse].iShowdown_LastDamageType != SHOWDOWNDMG_DRAIN
				hcShowdown_BarColour = HUD_COLOUR_RED
				REINIT_NET_TIMER(stShowdown_BarSwitchTimer)
				iFlashTime = SCRIPT_MAX_INT32
			ELSE
				hcShowdown_BarColour = HUD_COLOUR_WHITE
				REINIT_NET_TIMER(stShowdown_BarSwitchTimer)
			ENDIF
		ENDIF
	ENDIF
	
	fShowdown_DisplayPoints = LERP_FLOAT(fShowdown_DisplayPoints, fCurrentPoints, 2.5 * GET_FRAME_TIME())
	DRAW_GENERIC_METER(ROUND(fShowdown_DisplayPoints), ROUND(g_FMMC_STRUCT.fShowdown_MaximumPoints), "MP_SR_SCORE", hcShowdown_BarColour, iFlashTime, HUDORDER_TOP)
ENDPROC

PROC PROCESS_SHOWDOWN_HUD()
	
	INT i
	
	SWITCH eShowdownHUDState
		CASE SHOWDOWN_HUD_OFF
			PRINTLN("[SHOWDOWNHUD] Starting up the Showdown HUD!")
			SET_SHOWDOWN_HUD_STATE(SHOWDOWN_HUD_LOADING)
		BREAK
		
		CASE SHOWDOWN_HUD_LOADING
			IF HAS_SCALEFORM_MOVIE_LOADED(sfiPPGenericMovie)
				SET_SHOWDOWN_HUD_STATE(SHOWDOWN_HUD_SETUP)
			ELSE
				PRINTLN("[SHOWDOWNHUD] Requesting scaleform!")
				sfiPPGenericMovie = REQUEST_SCALEFORM_MOVIE("POWER_PLAY_GENERIC")
			ENDIF
		BREAK
		
		CASE SHOWDOWN_HUD_SETUP
			FOR i = 0 TO MC_serverBD.iNumberOfTeams - 1
				BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "ADD_TEAM")
					INT iHudColour
					iHudColour = ENUM_TO_INT(GET_HUD_COLOUR_FOR_FMMC_TEAM(i, PlayerToUse))
					PRINTLN("[JT][HTHUD][SHOWDOWNHUD] - PROCESS_SHOWDOWN_HUD - Adding team: ", i, " hud colour: ", iHudColour, " PlayerToUse: ", GET_PLAYER_NAME(PlayerToUse), " PlayerTeam: ", GET_PLAYER_TEAM(PlayerToUse) )
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iHudColour)
				END_SCALEFORM_MOVIE_METHOD()
			ENDFOR
			
			FOR i = 0 TO MC_serverBD.iNumberOfTeams - 1
				BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "ADD_ICON")
					PRINTLN("[JT][HTHUD][SHOWDOWNHUD] - PROCESS_SHOWDOWN_HUD - Adding icon for team: ", i, " icon: ", ciPPGICON_BLANK)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ciPPGICON_BLANK)
				END_SCALEFORM_MOVIE_METHOD()
			ENDFOR
			
			SET_SHOWDOWN_HUD_STATE(SHOWDOWN_HUD_RUNNING)
		BREAK
		
		CASE SHOWDOWN_HUD_RUNNING
			IF MC_playerBD[iLocalPart].iGameState = GAME_STATE_RUNNING
			AND GET_MC_SERVER_GAME_STATE() = GAME_STATE_RUNNING
				IF NOT SHOULD_HIDE_THE_HUD_THIS_FRAME()
				AND NOT g_b_OnLeaderboard
			
					IF NOT IS_RP_BAR_ACTIVE_IN_ARENA_BOX()
						DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_RANKBAR) 
					ENDIF
				
					FOR i=0 TO (MC_serverBD.iNumberOfTeams - 1)
						PRINTLN("[SHOWDOWNHUD] Processing team ", i)
						DRAW_SHOWDOWN_TEAM_CIRCLE(i)
					ENDFOR
				
					DRAW_SCALEFORM_MOVIE_FULLSCREEN(sfiPPGenericMovie, 255, 255, 255, 255)
					
					IF HAS_NET_TIMER_EXPIRED(stShowdown_IdleTimer, ciShowdown_IdleTimeAllowed)
					AND HAS_NET_TIMER_EXPIRED(stShowdown_FirstFifteenSecondsTimer, ciShowdown_TimeBeforeDrain)
					AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
					AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SHWDWN_NEP")
					AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SHWDWN_MRSP")
						DISPLAY_HELP_TEXT_THIS_FRAME("SHWDWN_ITD", TRUE)
					ENDIF
					
					//Personal healthbar stuff
					IF MC_playerBD[iLocalPart].iGameState = GAME_STATE_RUNNING
					AND GET_MC_SERVER_GAME_STATE() = GAME_STATE_RUNNING
					AND bPlayerToUseOK
					AND NOT IS_PLAYER_DEAD(PlayerToUse)
						DRAW_SHOWDOWN_MY_POINTS_BAR(MC_playerBD_1[iPartToUse].fShowdownPoints)
					ENDIF
					
					SET_MAX_HEALTH_HUD_DISPLAY(100 + ROUND(g_FMMC_STRUCT.fShowdown_MaximumPoints))
					SET_HEALTH_HUD_DISPLAY_VALUES(100 + ROUND(MC_playerBD_1[iPartToUse].fShowdownPoints), 0, FALSE)
				ELSE
					PRINTLN("[SHOWDOWNHUD] Not drawing Showdown HUD due to SHOULD_HIDE_THE_HUD_THIS_FRAME or g_b_OnLeaderboard")
				ENDIF
			ELSE
				SET_SHOWDOWN_HUD_STATE(SHOWDOWN_HUD_CLEANUP)
			ENDIF
		BREAK
		
		CASE SHOWDOWN_HUD_CLEANUP
			IF HAS_SCALEFORM_MOVIE_LOADED(sfiPPGenericMovie)
				SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfiPPGenericMovie)
			ENDIF
		BREAK
	
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	IF bShowdown_TeamDebugActive
		DRAW_SHOWDOWN_DEBUG()
	ELSE
		IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
			bShowdown_TeamDebugActive = TRUE
		ENDIF
	ENDIF
	#ENDIF
ENDPROC

PROC HANDLE_PREGAME_BLIPPING()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_PROCESS_VEH_BLIPS_IN_PREGAME)
		//Blip vehicles in pre-game
		INT iVehForBlip, iPColour, iSColourTeam
		FOR iVehForBlip = 0 TO (FMMC_MAX_VEHICLES - 1)
		
			VEHICLE_INDEX tempVeh = NULL
			
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVehForBlip])
			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iVehForBlip])
				tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVehForBlip])
			ENDIF
			
			IF DOES_ENTITY_EXIST(tempVeh)
				HANDLE_VEHICLE_BLIP_CREATION(iVehForBlip, tempVeh, iPColour, iSColourTeam)
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

PROC HANDLE_SUPPRESSING_WASTED_SHARD()

	//Running Back Remix
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUNNING_BACK_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
		VEHICLE_INDEX viPlayerVeh = GET_VEHICLE_PED_IS_USING(LocalPlayerPed)
		
		IF DOES_ENTITY_EXIST(viPlayerVeh)
			IF IS_ENTITY_A_MISSION_CREATOR_ENTITY(viPlayerVeh) >= 0
				IF NOT MPGlobals.g_bSuppressWastedBM
					SET_BIG_MESSAGE_SUPPRESS_WASTED(TRUE)
					PRINTLN("[RunningBackRM] Player has been put into the runner vehicle! Suppressing wasted shard.")
				ENDIF
			ELSE
				IF MPGlobals.g_bSuppressWastedBM
					SET_BIG_MESSAGE_SUPPRESS_WASTED(FALSE)
					PRINTLN("[RunningBackRM] Player has been put into a standard respawn vehicle. Un-suppressing wasted shard.")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_RUNNING_BACK_SHARDS()

	
	IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_BLOCK_RUNNING_BACK_SHARD)
	OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUNNING_BACK_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
		EXIT
	ENDIF
	
	IF GET_PLAYER_RESPAWN_BACK_AT_START_STATE(iLocalPart) = PRBSS_PlayingNormally
		IF iCurrentRunnerPart = -1
			INT iPlayers = 0		
			FOR iPlayers = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
				IF IS_BIT_SET(MC_PlayerBD[iPlayers].iClientBitSet4, PBBOOL4_SPAWNED_IN_A_PLACED_VEHICLE)
					PARTICIPANT_INDEX piPlayer = INT_TO_PARTICIPANTINDEX(iPlayers)
					IF NETWORK_IS_PARTICIPANT_ACTIVE(piPlayer)					
						PLAYER_INDEX pPlayer = NETWORK_GET_PLAYER_INDEX(piPlayer)
						iCurrentRunnerPart = iPlayers
						iCurrentRunnerPlayer = NATIVE_TO_INT(pPlayer)
						tlCurrentRunnerPlayerName = GET_PLAYER_NAME(pPlayer)
						PRINTLN("[LM][PROCESS_RUNNING_BACK_SHARDS] - iPlayer: ", iPlayers, " has PBBOOL4_SPAWNED_IN_A_PLACED_VEHICLE set. They are the current runner. iCurrentRunnerPlayer: ", iCurrentRunnerPlayer, " tlCurrentRunnerPlayerName: ", tlCurrentRunnerPlayerName)
						BREAKLOOP
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
	ENDIF
	
	TEXT_LABEL_15 tl15 = ""
	TEXT_LABEL_15 tl15b = ""
	IF (iCurrentRunnerPart > -1 AND iCurrentRunnerPlayer > -1)
	AND (NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iCurrentRunnerPart)) OR IS_BIT_SET(g_iMissionPlayerLeftBS, iCurrentRunnerPlayer))
		PRINTLN("[LM][PROCESS_RUNNING_BACK_SHARDS] - bRunnerLeft - iCurrentRunnerPart: ", iCurrentRunnerPart, " iCurrentRunnerPlayer: ", iCurrentRunnerPlayer)
		PRINTLN("[LM][PROCESS_RUNNING_BACK_SHARDS] - bRunnerLeft - NETWORK_IS_PARTICIPANT_ACTIVE: ", NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iCurrentRunnerPart)), " g_iMissionPlayerLeftBS: ", IS_BIT_SET(g_iMissionPlayerLeftBS, iCurrentRunnerPlayer))		
		tl15 = "RNBK_PLEFT"
		tl15b = "RNBK_PLEFT_P"
	ELIF IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_SHOULD_PLAY_RUNNING_BACK_REMIX_SCORED)
		PRINTLN("[LM][PROCESS_RUNNING_BACK_SHARDS] - SBBOOL7_SHOULD_PLAY_RUNNING_BACK_REMIX_SCORED - iCurrentRunnerPart: ", iCurrentRunnerPart)
		tl15 = "RNBK_SCORE"
		tl15b = "RNBK_SCORE_P"
	ELIF IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_SHOULD_PLAY_RUNNING_BACK_REMIX_DESTROYED)
		PRINTLN("[LM][PROCESS_RUNNING_BACK_SHARDS] - SBBOOL7_SHOULD_PLAY_RUNNING_BACK_REMIX_DESTROYED - iCurrentRunnerPart: ", iCurrentRunnerPart)
		tl15 = "RNBK_DESTR"
		tl15b = "RNBK_DESTR_P"
	ELIF IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_SHOULD_PLAY_RUNNING_BACK_REMIX_BLOCKED)
		PRINTLN("[LM][PROCESS_RUNNING_BACK_SHARDS] - SBBOOL7_SHOULD_PLAY_RUNNING_BACK_REMIX_BLOCKED - iCurrentRunnerPart: ", iCurrentRunnerPart)
		tl15 = "RNBK_BLOCK"
		tl15b = "RNBK_BLOCK_P"
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(tl15)
	AND iCurrentRunnerPart > -1
	AND GET_PLAYER_RESPAWN_BACK_AT_START_STATE(iLocalPart) != PRBSS_PlayingNormally
	AND GET_PLAYER_RESPAWN_BACK_AT_START_STATE(iLocalPart) != PRBSS_Init		
		PRINTLN("[LM][PROCESS_RUNNING_BACK_SHARDS] - iCurrentRunnerPart ", iCurrentRunnerPart, " PlayerName: ", tlCurrentRunnerPlayerName)		
		SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_AND_COLOUR(BIG_MESSAGE_GENERIC_TEXT, tl15b, tlCurrentRunnerPlayerName, tl15, DEFAULT, DEFAULT, DEFAULT, GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_playerBD[iCurrentRunnerPart].iteam, PlayerToUse), TRUE)
		iCurrentRunnerPart = -1
		iCurrentRunnerPlayer = -1
		tlCurrentRunnerPlayerName = ""
		IF IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
			SET_BIT(iLocalBoolCheck29, LBOOL29_BLOCK_RUNNING_BACK_SHARD)
		ENDIF
	ENDIF	
ENDPROC

PROC PROCESS_ALL_TEAM_MULTIRULE_TIMER_HUD()
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_SHOW_ALL_TEAM_MULTIRULE)
	OR g_bMissionEnding
		EXIT
	ENDIF

	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iTeamLoop
	TEXT_LABEL_63 tlTeamName
	
	FOR iTeamLoop = FMMC_MAX_TEAMS-1 TO 0 STEP -1
		tlTeamName = g_sMission_TeamName[iTeamLoop]
		IF HAS_NET_TIMER_STARTED(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeamLoop])
			IF iTeamLoop != iTeam
				IF IS_TEAM_ACTIVE(iTeamLoop)		
					DRAW_GENERIC_TIMER(GET_TIME_REMAINING_ON_MULTIRULE_TIMER(iTeamLoop), tlTeamName, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_DONTCARE, DEFAULT, GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeamLoop, PlayerToUse), DEFAULT, DEFAULT, DEFAULT,GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeamLoop, PlayerToUse), TRUE)
				ENDIF
			ELSE
				DRAW_GENERIC_TIMER(GET_TIME_REMAINING_ON_MULTIRULE_TIMER(iTeam), tlTeamName, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_TOP, DEFAULT, GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam, PlayerToUse), DEFAULT, DEFAULT, DEFAULT,GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeamLoop, PlayerToUse), TRUE)
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

FUNC BOOL REDO_NEWS_FILTER()
	TEXT_LABEL_15 tl15Line0, tl15Line1
	GET_NEWS_HUD_TEXT_LABELS(tl15Line0, tl15Line1)
	sfFilter = REQUEST_SCALEFORM_MOVIE("BREAKING_NEWS")
	IF HAS_SCALEFORM_MOVIE_LOADED(sfFilter)
		DISPLAY_RADAR(FALSE)						
		
		BEGIN_SCALEFORM_MOVIE_METHOD(sfFilter, "SET_TEXT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("SPC_TXT_DFLT")
		END_SCALEFORM_MOVIE_METHOD()
		
		BEGIN_SCALEFORM_MOVIE_METHOD(sfFilter, "CLEAR_SCROLL_TEXT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		END_SCALEFORM_MOVIE_METHOD()
		
		BEGIN_SCALEFORM_MOVIE_METHOD(sfFilter, "SET_SCROLL_TEXT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_LABEL_WITH_PLAYER_NAME("MPTV_TICK0", GET_PLAYER_NAME(PLAYER_ID()))
		END_SCALEFORM_MOVIE_METHOD()
		
		GAMER_HANDLE gamerPlayer
		NETWORK_CLAN_DESC crewPlayer
		BEGIN_SCALEFORM_MOVIE_METHOD(sfFilter, "SET_SCROLL_TEXT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(1)
			gamerPlayer = GET_GAMER_HANDLE_PLAYER(NETWORK_GET_PLAYER_INDEX_FROM_PED(PLAYER_PED_ID()))
			IF IS_PLAYER_IN_ACTIVE_CLAN(gamerPlayer)
				crewPlayer = GET_PLAYER_CREW(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_CURRENT_FOCUS_PED()))
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_LABEL_WITH_TL63("MPTV_TICK1", GET_CREW_CLAN_NAME(gamerPlayer, crewPlayer))
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPTV_TICK2")
			ENDIF
		END_SCALEFORM_MOVIE_METHOD()
		
		BEGIN_SCALEFORM_MOVIE_METHOD(sfFilter, "SET_SCROLL_TEXT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(2)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tl15Line0)
		END_SCALEFORM_MOVIE_METHOD()
		BEGIN_SCALEFORM_MOVIE_METHOD(sfFilter, "SET_SCROLL_TEXT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(3)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tl15Line1)
		END_SCALEFORM_MOVIE_METHOD()
			
		DISPLAY_RADAR(FALSE)
		
		fNewsFilterScrollEntry = 0
		
		BEGIN_SCALEFORM_MOVIE_METHOD(sfFilter, "DISPLAY_SCROLL_TEXT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fNewsFilterScrollEntry)
		END_SCALEFORM_MOVIE_METHOD()
		
		timeNewsFilterNextUpdate = GET_NETWORK_TIME()
		
		PRINTLN("[LM] PROCESS_CAMERA_FILTER - REDO_NEWS_FILTER")
		
		screenRT = GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID()
		
		CLEAR_REDO_NEWS_HUD_DUE_TO_EVENT()		
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC MANAGE_CAMERA_FILTER_RENDERING()
	SWITCH eCurrentFilter
		CASE SPEC_HUD_FILTER_NEWS
		CASE SPEC_HUD_FILTER_GTAOTV
			IF NOT IS_SPECTATOR_RUNNING_CUTSCENE()
			AND NOT g_bCelebrationScreenIsActive
			AND NOT IS_CUTSCENE_ACTIVE()
				IF iFilterStage = 99
					IF HAS_SCALEFORM_MOVIE_LOADED(sfFilter)
						SET_TEXT_RENDER_ID (screenRT)
						DRAW_SCALEFORM_MOVIE_FULLSCREEN(sfFilter, 255,255,255,0)
						IF NOT IS_RADAR_HIDDEN()
							DISPLAY_RADAR(FALSE)
						ENDIF
						THEFEED_HIDE_THIS_FRAME()
						HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC
 
 // copied from SCTV system.
PROC PROCESS_CAMERA_FILTER()
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_EnableWeazelNewsCamFilterForHeli)
		EXIT
	ENDIF
	
	IF IS_PED_INJURED(LocalPlayerPed)
		g_bUsingTurretHeliCam = FALSE
		PRINTLN("[LM] PROCESS_CAMERA_FILTER - Player is dead - g_bUsingTurretHeliCam = FALSE")
	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		g_bUsingTurretHeliCam = FALSE
		PRINTLN("[LM] PROCESS_CAMERA_FILTER - Player is not in a vehicle - g_bUsingTurretHeliCam = FALSE")
	ENDIF
		
	IF NOT IS_LOCAL_PLAYER_USING_TURRET_CAM()
	AND iFilterStage = -1
		EXIT
	ENDIF
	
	PRINTLN("[LM] PROCESS_CAMERA_FILTER - Processing iFilterStage: ", iFilterStage)
	
	IF IS_BIT_SET(iFilterBitSet, ci_MCFILTER_BIT_REFRESH_FILTER)
		iFilterStage = 0
		PRINTLN("[LM] PROCESS_CAMERA_FILTER - eDesiredFilter = ", ENUM_TO_INT(iFilterStage))
		CLEAR_BIT(iFilterBitSet, ci_MCFILTER_BIT_REFRESH_FILTER)
		PRINTLN("[LM] PROCESS_CAMERA_FILTER - ci_MCFILTER_BIT_REFRESH_FILTER CLEARED")
	ENDIF
	
	//option
	eDesiredFilter = SPEC_HUD_FILTER_NEWS
	
	SWITCH iFilterStage
		CASE -1
			IF IS_LOCAL_PLAYER_USING_TURRET_CAM()
				iFilterStage++
			ENDIF
		BREAK
		
		CASE 0	//noise on
			SET_TIMECYCLE_MODIFIER("CAMERA_secuirity_FUZZ")
			PLAY_SOUND_FRONTEND(iSoundChangeFilter, "Change_Cam", "MP_CCTV_SOUNDSET")
			iFilterChangeTime = GET_GAME_TIMER()
			iFilterStage++
			PRINTLN("[LM] PROCESS_CAMERA_FILTER - noise on, iFilterStage++")
		BREAK
		CASE 1	//cleanup current
			BOOL bFinished
			SWITCH eCurrentFilter
				CASE SPEC_HUD_FILTER_SPEC_1
				CASE SPEC_HUD_FILTER_SPEC_2
				CASE SPEC_HUD_FILTER_SPEC_3
				CASE SPEC_HUD_FILTER_SPEC_4
				CASE SPEC_HUD_FILTER_SPEC_5
				CASE SPEC_HUD_FILTER_SPEC_6
				CASE SPEC_HUD_FILTER_SPEC_7
				CASE SPEC_HUD_FILTER_SPEC_8
				CASE SPEC_HUD_FILTER_SPEC_9
				CASE SPEC_HUD_FILTER_SPEC_10
					bFinished = TRUE
				BREAK
				CASE SPEC_HUD_FILTER_NEWS
					DISPLAY_RADAR(TRUE)
					SET_SCTV_TICKER_SCORE_ON()
					SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfFilter)
					bFinished = TRUE
				BREAK
				CASE SPEC_HUD_FILTER_GTAOTV
					IF NETWORK_IS_ACTIVITY_SESSION()
						SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfFilter)
						DISPLAY_RADAR(TRUE)						
						SET_SCTV_TICKER_UI_OFF()
						bFinished = TRUE
					ENDIF
				BREAK
			ENDSWITCH
			IF bFinished
			AND IS_LOCAL_PLAYER_USING_TURRET_CAM()
				PRINTLN("[LM] PROCESS_CAMERA_FILTER - cleanup current, iFilterStage++")
				iFilterStage++
			ELIF NOT IS_LOCAL_PLAYER_USING_TURRET_CAM()
				PRINTLN("[LM] PROCESS_CAMERA_FILTER - cleanup current, iFilterStage = -1")
				CLEAR_TIMECYCLE_MODIFIER()
				iFilterStage = -1
			ENDIF
		BREAK
		CASE 2	//load in desired
			SWITCH eDesiredFilter				
				CASE SPEC_HUD_FILTER_SPEC_1
				CASE SPEC_HUD_FILTER_SPEC_2
				CASE SPEC_HUD_FILTER_SPEC_3
				CASE SPEC_HUD_FILTER_SPEC_4
				CASE SPEC_HUD_FILTER_SPEC_5
				CASE SPEC_HUD_FILTER_SPEC_6
				CASE SPEC_HUD_FILTER_SPEC_7
				CASE SPEC_HUD_FILTER_SPEC_8
				CASE SPEC_HUD_FILTER_SPEC_9
				CASE SPEC_HUD_FILTER_SPEC_10
					bFinished = TRUE
				BREAK
				CASE SPEC_HUD_FILTER_NEWS
					IF REDO_NEWS_FILTER()
						bFinished = TRUE
					ENDIF
				BREAK
				CASE SPEC_HUD_FILTER_GTAOTV
					IF NETWORK_IS_ACTIVITY_SESSION()
						DISPLAY_RADAR(FALSE)
						bFinished = TRUE
					ENDIF
				BREAK
			ENDSWITCH
			IF bFinished				
				PRINTLN("[LM] PROCESS_CAMERA_FILTER - load in desired, specData.specHUDData.iFilterStage++")
				iFilterStage++
			ENDIF
		BREAK
		CASE 3 //wait for timer
			IF GET_GAME_TIMER() - iFilterChangeTime > SPEC_HUD_FILTER_CHANGE_TIME
				PRINTLN("[LM] PROCESS_CAMERA_FILTER - wait for timer, specData.specHUDData.iFilterStage++")
				iFilterStage++
			ENDIF
		BREAK
		CASE 4	//noise off
			SWITCH eDesiredFilter
				CASE SPEC_HUD_FILTER_SPEC_1
					SET_TIMECYCLE_MODIFIER("spectator1")
				BREAK
				CASE SPEC_HUD_FILTER_SPEC_2
					SET_TIMECYCLE_MODIFIER("spectator2")
				BREAK
				CASE SPEC_HUD_FILTER_SPEC_3
					SET_TIMECYCLE_MODIFIER("spectator3")
				BREAK
				CASE SPEC_HUD_FILTER_SPEC_4
					SET_TIMECYCLE_MODIFIER("spectator4")
				BREAK
				CASE SPEC_HUD_FILTER_SPEC_5
					SET_TIMECYCLE_MODIFIER("spectator5")
				BREAK
				CASE SPEC_HUD_FILTER_SPEC_6
					SET_TIMECYCLE_MODIFIER("spectator6")
				BREAK
				CASE SPEC_HUD_FILTER_SPEC_7
					SET_TIMECYCLE_MODIFIER("spectator7")
				BREAK
				CASE SPEC_HUD_FILTER_SPEC_8
					SET_TIMECYCLE_MODIFIER("spectator8")
				BREAK
				CASE SPEC_HUD_FILTER_SPEC_9
					SET_TIMECYCLE_MODIFIER("spectator9")
				BREAK
				CASE SPEC_HUD_FILTER_SPEC_10
					SET_TIMECYCLE_MODIFIER("spectator10")
				BREAK
				
				CASE SPEC_HUD_FILTER_NEWS
				CASE SPEC_HUD_FILTER_GTAOTV
					SET_TIMECYCLE_MODIFIER("spectator1")
				BREAK
			ENDSWITCH
			
			// If we are no longer using a filter
			
			IF iSoundChangeFilter <> -1
				STOP_SOUND(iSoundChangeFilter)
				RELEASE_SOUND_ID(iSoundChangeFilter)
				iSoundChangeFilter = -1
			ENDIF
			PRINTLN("[LM] PROCESS_CAMERA_FILTER - noise off, specData.specHUDData.iFilterStage++")
			iFilterStage++
		BREAK
		CASE 5	//set current to desired
			eCurrentFilter = eDesiredFilter			
			PRINTLN("[LM] PROCESS_CAMERA_FILTER - set current to desired, specData.specHUDData.iFilterStage = 99")
			iFilterStage = 99
		BREAK
		CASE 99	//Run the filter
			
			MANAGE_CAMERA_FILTER_RENDERING()
			
			IF eCurrentFilter = SPEC_HUD_FILTER_NEWS
											
				IF IS_BIT_SET(iFilterBitSet, ci_MCFILTER_BIT_REFRESH_NEWS_FILTER)
					REDO_NEWS_FILTER()
				ENDIF
				
				IF NETWORK_IS_GAME_IN_PROGRESS()					
					DRAW_SCALEFORM_MOVIE_FULLSCREEN(sfFilter, 255, 255, 255, 255)
					
					IF GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), timeNewsFilterNextUpdate) >= SPEC_HUD_NEWS_TICKER_TIME_UPDATE
						DISPLAY_RADAR(FALSE)
						
						fNewsFilterScrollEntry++
						
						IF fNewsFilterScrollEntry >= SPEC_HUD_NUMBER_OF_NEWS_TICKER
							fNewsFilterScrollEntry = 0
						ENDIF
						
						BEGIN_SCALEFORM_MOVIE_METHOD(sfFilter, "DISPLAY_SCROLL_TEXT")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fNewsFilterScrollEntry)
						END_SCALEFORM_MOVIE_METHOD()
						
						timeNewsFilterNextUpdate = GET_NETWORK_TIME()
						
						PRINTLN("[LM] PROCESS_CAMERA_FILTER - News Update... fNewsFilterScrollEntry: ", fNewsFilterScrollEntry)
					ELSE
						PRINTLN("[LM] PROCESS_CAMERA_FILTER - News Update... Time until next update: ", 5000-GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), timeNewsFilterNextUpdate))
					ENDIF
				ENDIF
			ENDIF
			IF eCurrentFilter = SPEC_HUD_FILTER_GTAOTV
			
			ENDIF
			IF NOT IS_LOCAL_PLAYER_USING_TURRET_CAM()
				iFilterStage = 1
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

PROC SET_AGGRO_HELP_TEXT_DATA(INT iSpookReason, INT iPartFail, INT iPed)
	IF !bIsLocalPlayerHost
		EXIT
	ENDIF
	DEBUG_PRINTCALLSTACK()
	INT iTeam = MC_playerBD[iLocalPart].iTeam
	INT iCurrentRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]
	PRINTLN("SET_AGGRO_HELP_TEXT_DATA Current rule = ", iCurrentRule, " || iSpookReason = ", iSpookReason, " iPartFail = ", iPartFail)
	
	IF iCurrentRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iCurrentRule], ciBS_RULE13_RULE_SHOW_PLAYER_SPOTTED_HELPTEXT)
		AND NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_AGGRO_HELPTEXT_PLAYED)
			IF MC_serverBD_2.iAggroText_PartCausingSpook = -1
			AND MC_serverBD_2.iAggroText_PedSpooked = -1
			AND MC_serverBD_2.iAggroText_SpookReason = -1
				MC_serverBD_2.iAggroText_PartCausingSpook = iPartFail
				MC_serverBD_2.iAggroText_PedSpooked = iPed
				MC_serverBD_2.iAggroText_SpookReason = iSpookReason
				PRINTLN("SET_AGGRO_HELP_TEXT_DATA - MC_serverBD_2.iAggroText_PartCausingSpook: ", MC_serverBD_2.iAggroText_PartCausingSpook)
				PRINTLN("SET_AGGRO_HELP_TEXT_DATA - MC_serverBD_2.iAggroText_PedSpooked: ", MC_serverBD_2.iAggroText_PedSpooked)
				PRINTLN("SET_AGGRO_HELP_TEXT_DATA - MC_serverBD_2.iAggroText_SpookReason: ", MC_serverBD_2.iAggroText_SpookReason)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CLEAR_AGGRO_HELP_TEXT_DATA()
	IF !bIsLocalPlayerHost
		EXIT
	ENDIF
	MC_serverBD_2.iAggroText_PartCausingSpook = -1
	MC_serverBD_2.iAggroText_PedSpooked = -1
	MC_serverBD_2.iAggroText_SpookReason = -1
	DEBUG_PRINTCALLSTACK()
	PRINTLN("SET_AGGRO_HELP_TEXT_DATA - Clearing ped aggro text data")
ENDPROC

FUNC BOOL SHOULD_SHOW_AGGRO_HELP_TEXT()
	IF MC_serverBD_2.iAggroText_PartCausingSpook != -1
	AND MC_serverBD_2.iAggroText_PedSpooked != -1
	AND MC_serverBD_2.iAggroText_SpookReason != -1
		IF IS_BIT_SET(MC_serverBD.iPedAggroedBitset[GET_LONG_BITSET_INDEX(MC_serverBD_2.iAggroText_PedSpooked)], GET_LONG_BITSET_BIT(MC_serverBD_2.iAggroText_PedSpooked))
		AND NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[MC_serverBD_2.iAggroText_PedSpooked])
		AND IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_0 + MC_playerBD[iLocalPart].iTeam)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SHOW_PED_AGGRO_HELP_TEXT()
	DEBUG_PRINTCALLSTACK()
	INT iTeam = MC_playerBD[iLocalPart].iTeam
	INT iCurrentRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]
	
	IF iCurrentRule > -1 AND iCurrentRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iCurrentRule], ciBS_RULE13_RULE_SHOW_PLAYER_SPOTTED_HELPTEXT)
			
			PRINTLN("[ML] SHOW_PED_AGGRO_HELP_TEXT Current rule = ", iCurrentRule, " || MC_serverBD_2.iAggroText_SpookReason = ", MC_serverBD_2.iAggroText_SpookReason, " MC_serverBD_2.iAggroText_PartCausingSpook = ", MC_serverBD_2.iAggroText_PartCausingSpook, " MC_serverBD_2.iAggroText_PedSpooked: ", MC_serverBD_2.iAggroText_PedSpooked)
			
			IF SHOULD_SHOW_AGGRO_HELP_TEXT()
			
				IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_AGGRO_HELPTEXT_PLAYED)
					IF MC_serverBD_2.iAggroText_SpookReason = ciSPOOK_SEEN_BODY
						PRINTLN("[ML][PED_BODY_SPOTTED_HELP_TEXT] SHOW_PED_AGGRO_HELP_TEXT - Ped spotted corpse")	
						PRINT_HELP("SPOTEDPEDB", 5000)
						SET_BIT(iLocalBoolCheck31, LBOOL31_AGGRO_HELPTEXT_PLAYED)
						CLEAR_AGGRO_HELP_TEXT_DATA()
					ENDIF
					
					IF MC_serverBD_2.iAggroText_SpookReason = ciSPOOK_NONE
					OR MC_serverBD_2.iAggroText_SpookReason = ciSPOOK_PLAYER_HEARD
						IF MC_serverBD_2.iAggroText_PartCausingSpook != -1
							PARTICIPANT_INDEX partTriggerer = INT_TO_PARTICIPANTINDEX(MC_serverBD_2.iAggroText_PartCausingSpook)
							PLAYER_INDEX piTriggerer = NETWORK_GET_PLAYER_INDEX(partTriggerer)	
							PRINT_HELP_WITH_PLAYER_NAME("SPOTEDPEDC", GET_PLAYER_NAME(piTriggerer), HUD_COLOUR_WHITE, 5000)
							SET_BIT(iLocalBoolCheck31, LBOOL31_AGGRO_HELPTEXT_PLAYED)
							CLEAR_AGGRO_HELP_TEXT_DATA()
							PRINTLN("[ML][PED_BODY_SPOTTED_HELP_TEXT] SHOW_PED_AGGRO_HELP_TEXT  Player seen or heard")
						ELSE
							PRINTLN("[ML][PED_BODY_SPOTTED_HELP_TEXT] SHOW_PED_AGGRO_HELP_TEXT  iPartCausingFail is -1")
						ENDIF
					ENDIF
				ELSE
					PRINTLN("[ML][PED_BODY_SPOTTED_HELP_TEXT] SHOW_PED_AGGRO_HELP_TEXT  LBOOL31_AGGRO_HELPTEXT_PLAYED already set")
				ENDIF
			ELSE
				IF bIsLocalPlayerHost
				AND MC_serverBD_2.iAggroText_PedSpooked != -1
					IF IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[MC_serverBD_2.iAggroText_PedSpooked])
						CLEAR_AGGRO_HELP_TEXT_DATA()
					ENDIF
				ENDIF
			ENDIF
		ELSE
			CLEAR_BIT(iLocalBoolCheck31, LBOOL31_AGGRO_HELPTEXT_PLAYED)
			CLEAR_AGGRO_HELP_TEXT_DATA()
			PRINTLN("[ML][PED_BODY_SPOTTED_HELP_TEXT] SHOW_PED_AGGRO_HELP_TEXT  ciBS_RULE13_RULE_SHOW_PLAYER_SPOTTED_HELPTEXT isn't set on this rule || ", iCurrentRule)
		ENDIF
	
	ENDIF
ENDPROC

PROC UPDATE_THERMITE_REMAINING_HUD(BOOL bShouldHideHUD)
	
	IF bShouldHideHUD
		EXIT
	ENDIF
	
	IF g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__STEALTH
	OR g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
		IF IS_BIT_SET(MC_serverBD_1.sLEGACYMissionContinuityVars.iGenericTrackingBitset, ciLEGACY_ContinuityGenericTrackingBS_DroppedToDirect)
			//Don't display the thermite hud if we've dropped down to the direct branch.
			EXIT
		ENDIF
	ENDIF
	
	INT iTeam = MC_playerBD[iLocalPart].iTeam
	INT iCurrentRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]
	
	IF iCurrentRule < FMMC_MAX_RULES
		IF NOT ( g_bMissionEnding OR g_bMissionOver )
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iCurrentRule], ciBS_RULE13_SHOW_THERMITE_REMAINING_HUD)
			
			INT iRemainingCharges
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset4, ciBS4_TEAM_PER_PLAYER_THERMAL_CHARGES)
				iRemainingCharges = MC_playerBD_1[iPartToUse].iThermitePerPlayerRemaining
			ELSE
				iRemainingCharges = MC_serverBD_1.iTeamThermalCharges[iTeam]
			ENDIF
			
			SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
			
			IF iRemainingCharges < 0
				DRAW_GENERIC_SCORE(99, "FMMC_THERM_CHGS", DEFAULT, HUD_COLOUR_WHITE, HUDORDER_TOP) //Infinite
			ELIF iRemainingCharges = 0
				DRAW_GENERIC_SCORE(iRemainingCharges, "FMMC_THERM_CHGS", DEFAULT, HUD_COLOUR_RED, HUDORDER_TOP) //Red 0
			ELSE	
				DRAW_GENERIC_SCORE(iRemainingCharges, "FMMC_THERM_CHGS", DEFAULT, HUD_COLOUR_WHITE, HUDORDER_TOP) //White
			ENDIF
			SET_BIT(g_TransitionSessionNonResetVars.iAirlockHUDBitset, ciFMMCAirlockHUDBS_ThermiteShown)
			IF IS_BIT_SET(g_TransitionSessionNonResetVars.iAirlockHUDBitset, ciFMMCAirlockHUDBS_ShowThermite)
				g_TransitionSessionNonResetVars.iPlayerThermalChargesForHUD = iRemainingCharges
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC PROCESS_LIMITED_STUN_GUN()
	
	IF g_FMMC_STRUCT.iStunGunCharges = -1
	AND NOT IS_BIT_SET(g_TransitionSessionNonResetVars.iFMMCMissionBS, iFMMCMissionBS__LIMITED_STUN_GUN_INITIALISED)
		EXIT
	ENDIF
	
	IF NOT HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_STUNGUN)
	OR g_bMissionEnding
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_LIMITED_STUN_GUN_FULLY_INITIALISED)
		WEAPON_TYPE wtCurrentWeapon
		IF GET_CURRENT_PED_WEAPON(LocalPlayerPed, wtCurrentWeapon)
			IF NOT IS_WEAPONTYPE_A_STUN_GUN(wtCurrentWeapon)
				EXIT
			ENDIF
			
			// Holding stun gun
			INT iAmmo
			IF wtCurrentWeapon = WEAPONTYPE_STUNGUN
				iAmmo = GET_PED_AMMO_BY_TYPE(LocalPlayerPed, AMMOTYPE_STUNGUN)
			ENDIF
			
			DRAW_GENERIC_SCORE(iAmmo, "FMMC_STUN_AMMO", DEFAULT, DEFAULT, HUDORDER_TOP)
		ENDIF
	
	ELIF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_SET_STUN_GUN_AS_LIMITED)
		IF NOT HAS_FIRST_STRAND_MISSION_BEEN_PASSED()
			PRINTLN("[StunGun] Setting stun gun ammo to ", g_FMMC_STRUCT.iStunGunCharges)
			SET_PED_AMMO(LocalPlayerPed, WEAPONTYPE_STUNGUN, g_FMMC_STRUCT.iStunGunCharges)
		ENDIF
		
		SET_BIT(iLocalBoolCheck32, LBOOL32_LIMITED_STUN_GUN_FULLY_INITIALISED)
		
	ELSE
		PRINTLN("[StunGun] Initialising limited stun gun and setting iFMMCMissionBS__LIMITED_STUN_GUN_INITIALISED!")
		SET_PED_STUN_GUN_FINITE_AMMO(LocalPlayerPed, TRUE)
		
		SET_BIT(iLocalBoolCheck32, LBOOL32_SET_STUN_GUN_AS_LIMITED)
		SET_BIT(g_TransitionSessionNonResetVars.iFMMCMissionBS, iFMMCMissionBS__LIMITED_STUN_GUN_INITIALISED)
	ENDIF
ENDPROC
