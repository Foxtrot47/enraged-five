
USING "FM_Mission_Controller_USING.sch"
USING "FM_Mission_Controller_Utility.sch"
USING "FM_Mission_Controller_Entities.sch"
USING "FM_Mission_Controller_ObjectiveCompletion.sch"
USING "FM_Mission_Controller_SpecialCase.sch"
USING "FM_Mission_Controller_Rules.sch"
USING "FM_Mission_Controller_Doors.sch"
USING "FM_Mission_Controller_Cutscene.sch"
USING "FM_Mission_Controller_Debug.sch"
USING "FM_Mission_Controller_Locations.sch"
USING "FM_Mission_Controller_Vehicles.sch"
USING "FM_Mission_Controller_Peds_Brain.sch"
USING "FM_Mission_Controller_Peds_Body.sch"
USING "FM_Mission_Controller_Peds.sch"
USING "FM_Mission_Controller_GangChase.sch"
USING "FM_Mission_Controller_Minigame.sch"
USING "FM_Mission_Controller_Objects.sch"
USING "FM_Mission_Controller_Props.sch"
USING "FM_Mission_Controller_Audio.sch"
USING "FM_Mission_Controller_HUD.sch"
USING "FM_Mission_Controller_Players.sch"
USING "FM_Mission_Controller_Zones.sch"



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: MISSION BOUNDS !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************

FUNC VECTOR GET_FMMC_LAST_USED_VEHICLE_LOCATION()
	VECTOR vReturnLoc
	IF iLastUsedPlacedVeh > -1
		IF iLastUsedPlacedVeh < FMMC_MAX_VEHICLES
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iLastUsedPlacedVeh])
				PRINTLN("[RCC MISSION] GET_FMMC_LAST_USED_VEHICLE_LOCATION - Veh ", iLastUsedPlacedVeh)
				VEHICLE_INDEX viTemp = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iLastUsedPlacedVeh])
				vReturnLoc = GET_ENTITY_COORDS(viTemp, FALSE)
				IF NOT IS_ENTITY_DEAD(viTemp)
					IF GET_ENTITY_MODEL(viTemp) = TAMPA3 //This can probably be applied to all vehicles when there is more QA time
						VECTOR vMin, vMax
						GET_MODEL_DIMENSIONS(TAMPA3, vMin, vMax)
						vMin = vMin * 1.5
						vReturnLoc = vReturnLoc + (vMin.y * GET_ENTITY_FORWARD_VECTOR(viTemp))
						#IF IS_DEBUG_BUILD
						DRAW_DEBUG_SPHERE(vReturnLoc, 2,0,255,0,200)
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN vReturnLoc
ENDFUNC

FUNC VECTOR GET_FMMC_WINNING_RUNNER_LOCATION()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciUSE_MORE_ACCURATE_RACE_POSITION)
		INT iWinningPart = GET_PARTICIPANT_NUMBER_FROM_TEAM_PARTICIPANT_NUMBER(MC_serverBD.iWinningTeam, 0)
		IF iWinningPart != -1
			PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iWinningPart)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
				IF IS_NET_PLAYER_OK(tempPlayer)
					RETURN GET_PLAYER_COORDS(tempPlayer)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	VECTOR vWinningRunner
	
	INT iTeam
	
	INT iHighestLap = -1
	
	INT iHighestRule = -1
	INT iSameRuleBS
	INT iSameRuleCount
	
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		
		//Laps:
		IF MC_ServerBD_4.iTeamLapsCompleted[iTeam] > iHighestLap
			iHighestLap = MC_ServerBD_4.iTeamLapsCompleted[iTeam]
			iHighestRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
			iSameRuleCount = 1
			iSameRuleBS = 0
			SET_BIT(iSameRuleBS, iTeam)
			//PRINTLN("team ",iteam," has the highest lap - ",iHighestLap,"; rule ",iHighestRule)
		ELIF MC_ServerBD_4.iTeamLapsCompleted[iTeam] = iHighestLap
			
			//Laps are equal, compare checkpoint progress:
			IF MC_serverBD_4.iCurrentHighestPriority[iTeam] > iHighestRule
				iHighestRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
				iSameRuleCount = 1
				iSameRuleBS = 0
				SET_BIT(iSameRuleBS, iTeam)
				//PRINTLN("team ",iteam," has the highest rule - ",iHighestRule)
			ELIF MC_serverBD_4.iCurrentHighestPriority[iTeam] = iHighestRule
				iSameRuleCount++
				SET_BIT(iSameRuleBS, iTeam)
				//PRINTLN("team ",iteam," has the same rule as the highest - ",iHighestRule)
			//ELSE
				//PRINTLN("team ",iteam," has a lower rule than the highest - ",MC_serverBD_4.iCurrentHighestPriority[iTeam]," < ",iHighestRule)
			ENDIF
			
		ENDIF
		
	ENDFOR
	
	//PRINTLN("First loop done - iSameRuleCount ",iSameRuleCount,", iSameRuleBS ",iSameRuleBS)
	
	IF iSameRuleBS > 0 // If we've found anyone at all:
		
		PARTICIPANT_INDEX tempPart
		PLAYER_INDEX tempPlayer
		VECTOR vTemp
		VECTOR vObjective
		FLOAT fClosestDist2 = 999999999 //Furthest distance apart on map is O(10,000)
		FLOAT fTempDist2
		
		FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
			
			vObjective = <<0, 0, 0>>
			
			IF IS_BIT_SET(iSameRuleBS, iTeam)
				
				#IF IS_DEBUG_BUILD
				IF bDebugBoundsPrints
					PRINTLN("l2: checking team ",iteam, "iNumLocHighestPriority = ",MC_serverBD.iNumLocHighestPriority[iTeam], " iSameRuleCount: ", iSameRuleCount)
				ENDIF #ENDIF
				
				IF iSameRuleCount > 1
					IF MC_serverBD.iNumLocHighestPriority[iTeam] > 0
						INT iLoop
						
						FOR iLoop = 0 TO (MC_serverBD.iNumLocCreated - 1)
							IF MC_serverBD_4.iGotoLocationDataPriority[iLoop][iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam]
								vObjective = GET_LOCATION_VECTOR(iLoop)
								#IF IS_DEBUG_BUILD
								IF bDebugBoundsPrints
									PRINTLN("found location vector at ",vObjective)
								ENDIF #ENDIF
								iLoop = MC_serverBD.iNumLocCreated // Break out, we've found the location we should be after!
							ENDIF
						ENDFOR
					ENDIF
					
					IF IS_VECTOR_ZERO(vObjective)
						vObjective = GET_ENTITY_COORDS(LocalPlayerPed, FALSE)
						#IF IS_DEBUG_BUILD
						IF bDebugBoundsPrints
							PRINTLN("no location found, use my own coords as vObjective - ",vObjective)
						ENDIF #ENDIF
					ENDIF
				ENDIF
				
				IF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_SUB_TEAM_ROLES) AND iRolePlayers[iTeam][0] != -1) // Hack to get the right player working for bug 2491596, replace this with a role that can complete the rule normally
					
					//PRINTLN("checking role 0 for this team - part ",iRolePlayers[iTeam][0])
					
					tempPart = INT_TO_PARTICIPANTINDEX(iRolePlayers[iTeam][0])
					
					IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
						//PRINTLN("part active")
						tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
						
						IF IS_NET_PLAYER_OK(tempPlayer)
							vTemp = GET_PLAYER_COORDS(tempPlayer)
							
							#IF IS_DEBUG_BUILD
							IF bDebugBoundsPrints
								PRINTLN("part okay at ",vTemp," - iSameRuleCount = ",iSameRuleCount)
							ENDIF #ENDIF
							
							IF iSameRuleCount > 1
								fTempDist2 = VDIST2(vTemp, vObjective)
								#IF IS_DEBUG_BUILD
								IF bDebugBoundsPrints
									PRINTLN("fTempDist2 = ",fTempDist2,", fClosestDist2 = ",fClosestDist2)
								ENDIF #ENDIF
								IF fTempDist2 < fClosestDist2
									fClosestDist2 = fTempDist2
									vWinningRunner = vTemp
									#IF IS_DEBUG_BUILD
									IF bDebugBoundsPrints
										PRINTLN("new closest dist! vWinningRunner = this loc, ",vTemp)
									ENDIF #ENDIF
								ENDIF
							ELSE
								vWinningRunner = vTemp // Only do this, as this is the only player we need to check
								#IF IS_DEBUG_BUILD
								IF bDebugBoundsPrints
									PRINTLN("this is the only team we need to check - vWinningRunner = this loc, ",vTemp)
								ENDIF #ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ELSE
					#IF IS_DEBUG_BUILD
					IF bDebugBoundsPrints
						PRINTLN("this team doesn't have roles set up, get the closest player")
					ENDIF #ENDIF
					
					tempPlayer = INT_TO_PLAYERINDEX(GET_NEAREST_PLAYER_TO_ENTITY_ON_TEAM(PlayerPedToUse, iTeam))
					
					IF tempPlayer != INVALID_PLAYER_INDEX() AND IS_NET_PLAYER_OK(tempPlayer)
						
						vTemp = GET_PLAYER_COORDS(tempPlayer)
						
						#IF IS_DEBUG_BUILD
						IF bDebugBoundsPrints
							PRINTLN("player is okay, location ",vTemp," - iSameRuleCount ",iSameRuleCount)
						ENDIF #ENDIF
						
						IF iSameRuleCount > 1
							fTempDist2 = VDIST2(vTemp, vObjective)
							
							#IF IS_DEBUG_BUILD
							IF bDebugBoundsPrints
								PRINTLN("fTempDist2 = ",fTempDist2,", fClosestDist2 = ",fClosestDist2)
							ENDIF #ENDIF
							
							IF fTempDist2 < fClosestDist2
								fClosestDist2 = fTempDist2
								vWinningRunner = vTemp
								#IF IS_DEBUG_BUILD
								IF bDebugBoundsPrints
									PRINTLN("new closest dist! vWinningRunner = this loc, ",vTemp)
								ENDIF #ENDIF
							ENDIF
						ELSE
							vWinningRunner = vTemp // Only do this, as this is the only player we need to check
							#IF IS_DEBUG_BUILD
							IF bDebugBoundsPrints
								PRINTLN("this is the only team we need to check - vWinningRunner = this loc, ",vTemp)
							ENDIF #ENDIF
							
						ENDIF
					ENDIF
				ENDIF
				
				CLEAR_BIT(iSameRuleBS, iTeam)
				#IF IS_DEBUG_BUILD
				IF bDebugBoundsPrints
					PRINTLN("clearing bit for team ",iTeam,"in iSameRuleBS - ",iSameRuleBS)
				ENDIF #ENDIF
				IF iSameRuleBS = 0
					iTeam = MC_serverBD.iNumberOfTeams // Break out, we've checked everyone we need to!
				ENDIF
			ENDIF
			
		ENDFOR
		
	ENDIF
	
	
	// Backups:
	IF IS_VECTOR_ZERO(vWinningRunner)
		//PRINTLN("after loop two: vWinningRunner is still zero! get nearest player on my team ",MC_playerBD[iPartToUse].iteam)
		PLAYER_INDEX tempPlayer = INT_TO_PLAYERINDEX(GET_NEAREST_PLAYER_TO_ENTITY_ON_TEAM(PlayerPedToUse, MC_playerBD[iPartToUse].iteam))
		
		IF IS_NET_PLAYER_OK(tempPlayer)
			vWinningRunner = GET_PLAYER_COORDS(tempPlayer)
			#IF IS_DEBUG_BUILD
			IF bDebugBoundsPrints
				PRINTLN("use player on my team at coords ",vWinningRunner)
			ENDIF #ENDIF
		ENDIF
		
		IF IS_VECTOR_ZERO(vWinningRunner)
			vWinningRunner = GET_ENTITY_COORDS(PlayerPedToUse, FALSE)
			#IF IS_DEBUG_BUILD
			IF bDebugBoundsPrints
				PRINTLN("STILL ZERO! just use my own coords, who cares: ",vWinningRunner)
			ENDIF #ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bDebugBoundsPrints
		PRINTLN("[RCC MISSION] GET_FMMC_WINNING_RUNNER_LOCATION - vWinningRunner = ",vWinningRunner)
	ENDIF #ENDIF
	
	RETURN vWinningRunner
	
ENDFUNC

FUNC VECTOR GET_FMMC_AREA_BOUNDS_POS(INT iTeam, INT iRule, BOOL bBounds2 = FALSE)
	
	VECTOR vBoundsPos
	INT iEntityType
	INT iEntityID
	BOOL bUsePositionAhead
	PLAYER_INDEX tempPlayer
	
	IF NOT bBounds2
		vBoundsPos = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos
		iEntityType = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iEntityType
		iEntityID = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iEntityID
		
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
			CLEAR_BIT(iLocalBoolCheck13, LBOOL13_RESPAWNBOUNDS1_CHOSEN_IN_FRONT)
			CLEAR_BIT(iLocalBoolCheck13, LBOOL13_RESPAWNBOUNDS1_CHOSEN_NOT_IN_FRONT)
		ENDIF
		
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iPercentageLikelihoodToSpawnInFrontOfPlyr > 0
			IF (NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_RESPAWNBOUNDS1_CHOSEN_IN_FRONT))
			AND (NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_RESPAWNBOUNDS1_CHOSEN_NOT_IN_FRONT))
				INT iRandom = GET_RANDOM_INT_IN_RANGE(1 * 100, 101 * 100)
				iRandom -= 100
				
				IF iRandom <= (g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iPercentageLikelihoodToSpawnInFrontOfPlyr * 100)
					PRINTLN("[RCC MISSION][spawnahead] GET_FMMC_AREA_BOUNDS_POS 1 - Chosen in front, team ",iTeam,", rule ",irule)
					SET_BIT(iLocalBoolCheck13, LBOOL13_RESPAWNBOUNDS1_CHOSEN_IN_FRONT)
				ELSE
					PRINTLN("[RCC MISSION][spawnahead] GET_FMMC_AREA_BOUNDS_POS 1 - Chosen NOT in front, team ",iTeam,", rule ",irule)
					SET_BIT(iLocalBoolCheck13, LBOOL13_RESPAWNBOUNDS1_CHOSEN_NOT_IN_FRONT)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(iLocalBoolCheck13, LBOOL13_RESPAWNBOUNDS1_CHOSEN_IN_FRONT)
				bUsePositionAhead = TRUE
			ENDIF
		ENDIF
	ELSE
		vBoundsPos = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].vPos
		iEntityType = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].iEntityType
		iEntityID = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].iEntityID
		
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
			CLEAR_BIT(iLocalBoolCheck13, LBOOL13_RESPAWNBOUNDS2_CHOSEN_IN_FRONT)
			CLEAR_BIT(iLocalBoolCheck13, LBOOL13_RESPAWNBOUNDS2_CHOSEN_NOT_IN_FRONT)
		ENDIF
		
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].iPercentageLikelihoodToSpawnInFrontOfPlyr > 0
			IF (NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_RESPAWNBOUNDS2_CHOSEN_IN_FRONT))
			AND (NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_RESPAWNBOUNDS2_CHOSEN_NOT_IN_FRONT))
				INT iRandom = GET_RANDOM_INT_IN_RANGE(1 * 100, 101 * 100)
				iRandom -= 100
				
				IF iRandom <= (g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].iPercentageLikelihoodToSpawnInFrontOfPlyr * 100)
					PRINTLN("[RCC MISSION][spawnahead] GET_FMMC_AREA_BOUNDS_POS 2 - Chosen in front, team ",iTeam,", rule ",irule)
					SET_BIT(iLocalBoolCheck13, LBOOL13_RESPAWNBOUNDS2_CHOSEN_IN_FRONT)
				ELSE
					PRINTLN("[RCC MISSION][spawnahead] GET_FMMC_AREA_BOUNDS_POS 2 - Chosen NOT in front, team ",iTeam,", rule ",irule)
					SET_BIT(iLocalBoolCheck13, LBOOL13_RESPAWNBOUNDS2_CHOSEN_NOT_IN_FRONT)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(iLocalBoolCheck13, LBOOL13_RESPAWNBOUNDS2_CHOSEN_IN_FRONT)
				bUsePositionAhead = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF iEntityType != ciRULE_TYPE_NONE
		
		IF iEntityType = ciRULE_TYPE_PLAYER //This is 'Team' in the creator
			IF iEntityID = -2
				RETURN GET_FMMC_WINNING_RUNNER_LOCATION()
			ENDIF
		ENDIF
		
		IF iEntityType = ciRULE_TYPE_VEHICLE
		AND iEntityID = -2
			VECTOR vLastVehLocation = GET_FMMC_LAST_USED_VEHICLE_LOCATION()
			IF NOT IS_VECTOR_ZERO(vLastVehLocation)
				RETURN vLastVehLocation
			ENDIF
		ENDIF
		
		IF iEntityID > -1
			PRINTLN("[RCC MISSION][spawnahead] Adding Entity Bounds") 
			IF bUsePositionAhead
			AND iEntityType = ciRULE_TYPE_PLAYER
			AND iEntityID < MC_serverBD.iNumberOfTeams
			AND (MC_serverBD.iNumberOfPlayingPlayers[iEntityID] > 0)
				
				INT iPlayer = GET_NEAREST_PLAYER_TO_ENTITY_ON_TEAM(LocalPlayerPed, iEntityID)
				tempPlayer = INT_TO_PLAYERINDEX(iPlayer)
				
				IF IS_NET_PLAYER_OK(tempPlayer)
					
					PARTICIPANT_INDEX tempPart = NETWORK_GET_PARTICIPANT_INDEX(tempPlayer)
					
					IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
						
						INT iPart = NATIVE_TO_INT(tempPart)
						
						IF iPart >= 0 AND iPart < MAX_NUM_MC_PLAYERS
						AND ( (NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_ANY_SPECTATOR)) OR IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_HEIST_SPECTATOR) )
						AND NOT IS_VECTOR_ZERO(MC_playerBD[iPart].vPositionAhead)
							
							RETURN MC_playerBD[iPart].vPositionAhead
							
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			VECTOR vTemp = GET_FMMC_ENTITY_LOCATION(GET_ENTITY_COORDS(LocalPlayerPed, FALSE), iEntityType, iEntityID, LocalPlayerPed)
			
			PRINTLN("[RCC MISSION] GET_FMMC_AREA_BOUNDS_POS - vTemp: ", vTemp)
			
			IF NOT IS_VECTOR_ZERO(vTemp)
				vBoundsPos = vTemp
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[RCC MISSION] GET_FMMC_AREA_BOUNDS_POS - GET_FMMC_ENTITY_LOCATION returning zero vector for entity type ",iEntityType,", entity ID ",iEntityID)
			#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	PRINTLN("[RCC MISSION] GET_FMMC_AREA_BOUNDS_POS - Returning vBoundsPos: ", vBoundsPos)
	
	RETURN vBoundsPos
	
ENDFUNC

FUNC BOOL CAN_BOUNDS_TIMER_RESET_YET(BOOL bSecondary)
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Enable_Turn_Rotor_Tag_Team)
		IF NOT bSecondary
			IF NOT HAS_NET_TIMER_STARTED(td_BoundsTimerResetDelay1)
				START_NET_TIMER(td_BoundsTimerResetDelay1)
			ENDIF
			IF (HAS_NET_TIMER_STARTED(td_BoundsTimerResetDelay1)
			AND NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(td_BoundsTimerResetDelay1, 6000))
				RETURN FALSE
			ENDIF
		ELSE
			IF NOT HAS_NET_TIMER_STARTED(td_BoundsTimerResetDelay2)
				START_NET_TIMER(td_BoundsTimerResetDelay2)
			ENDIF
			IF (HAS_NET_TIMER_STARTED(td_BoundsTimerResetDelay2)
			AND NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(td_BoundsTimerResetDelay2, 6000))
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

VECTOR vBoundsAhead_LastPos

PROC PROCESS_BOUNDS_AHEAD_POSITION()
	
	IF iSpectatorTarget != -1
		EXIT
	ENDIF
	
	IF DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME()
		
		BOOL bNeedToStoreAheadPos
		INT iTeam, iRule, iEntityType, iEntityID
		
		FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
			
			iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
			
			IF iRule < FMMC_MAX_RULES
				
				//Mission bounds 1:
				iEntityType = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iEntityType
				iEntityID = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iEntityID
				
				IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iPercentageLikelihoodToSpawnInFrontOfPlyr > 0
				AND iEntityType = ciRULE_TYPE_PLAYER
				AND iEntityID = MC_playerBD[iLocalPart].iTeam
					bNeedToStoreAheadPos = TRUE
					iTeam = MC_serverBD.iNumberOfTeams // Break out!
				ENDIF
				
				IF NOT bNeedToStoreAheadPos
					//Mission bounds 2:
					iEntityType = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].iEntityType
					iEntityID = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].iEntityID
					
					IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].iPercentageLikelihoodToSpawnInFrontOfPlyr > 0
					AND iEntityType = ciRULE_TYPE_PLAYER
					AND iEntityID = MC_playerBD[iLocalPart].iTeam
						bNeedToStoreAheadPos = TRUE
						iTeam = MC_serverBD.iNumberOfTeams // Break out!
					ENDIF
				ENDIF
				
			ENDIF
			
		ENDFOR
		
		IF bNeedToStoreAheadPos
			
			#IF IS_DEBUG_BUILD
			IF NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_NEED_TO_CALCULATE_POSITION_AHEAD)
				PRINTLN("[RCC MISSION][spawnahead] PROCESS_BOUNDS_AHEAD_POSITION - Setting LBOOL6_NEED_TO_CALCULATE_POSITION_AHEAD!")
			ENDIF
			#ENDIF
			
			SET_BIT(iLocalBoolCheck6, LBOOL6_NEED_TO_CALCULATE_POSITION_AHEAD)
		ELSE
			#IF IS_DEBUG_BUILD
			IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_NEED_TO_CALCULATE_POSITION_AHEAD)
				PRINTLN("[RCC MISSION][spawnahead] PROCESS_BOUNDS_AHEAD_POSITION - Clearing LBOOL6_NEED_TO_CALCULATE_POSITION_AHEAD!")
			ENDIF
			#ENDIF
			
			CLEAR_BIT(iLocalBoolCheck6, LBOOL6_NEED_TO_CALCULATE_POSITION_AHEAD)
			MC_playerBD[iLocalPart].vPositionAhead = <<0, 0, 0>>
		ENDIF
		
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_NEED_TO_CALCULATE_POSITION_AHEAD)
		
		IF IS_VECTOR_ZERO(MC_playerBD[iLocalPart].vPositionAhead)
		OR NOT ARE_VECTORS_ALMOST_EQUAL(vBoundsAhead_LastPos, GET_ENTITY_COORDS(LocalPlayerPed), 20)
			
			IF GET_GPS_BLIP_ROUTE_FOUND()
			AND IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				
				FLOAT fDistanceAlongRoute = 500
				FLOAT fSpeed = ABSF(GET_ENTITY_SPEED(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)))
				
				fDistanceAlongRoute = CLAMP(fSpeed * 15, 500, 700)
				
				VECTOR vReturn
				
				IF GET_POS_ALONG_GPS_TYPE_ROUTE(vReturn, TRUE, fDistanceAlongRoute, GPS_SLOT_RADAR_BLIP)
					PRINTLN("[RCC MISSION][spawnahead] PROCESS_BOUNDS_AHEAD_POSITION - Setting new vPositionAhead = ",vReturn)
					MC_playerBD[iLocalPart].vPositionAhead = vReturn
					vBoundsAhead_LastPos = GET_ENTITY_COORDS(LocalPlayerPed)
				ENDIF
			ELSE
				MC_playerBD[iLocalPart].vPositionAhead = <<0, 0, 0>>
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

FUNC INT GET_MISSION_BOUNDS_TEAMS_BITSET(INT iTeam, INT iRule, BOOL bSecondaryBounds = FALSE)
	
	INT iTeamBS
	INT iTeamLoop
	
	LEGACY_BOUNDS_STRUCT structBounds
	
	IF bSecondaryBounds
		structBounds = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule]
	ELSE
		structBounds = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule]
	ENDIF
	
	FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF IS_BIT_SET(structBounds.iBoundsBS, ciBounds_OnlyOneMemberOfTeam0Needed + iTeamLoop)
			SET_BIT(iTeamBS, iTeamLoop)
		ENDIF
	ENDFOR
	
	RETURN iTeamBS
	
ENDFUNC

FUNC BOOL IS_ANYONE_ON_NEEDED_TEAM_IN_BOUNDS(INT iTeam, INT iRule, BOOL bSecondaryBounds = FALSE)
	
	BOOL bSomeoneInBounds = FALSE
	
	INT iTeamBS = GET_MISSION_BOUNDS_TEAMS_BITSET(iTeam, iRule, bSecondaryBounds)
	
	IF iTeamBS > 0
		
		INT iLoop
		INT iPartsToCheck, iPartsChecked
		
		FOR iLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
			iPartsToCheck += MC_serverBD.iNumberOfPlayingPlayers[iLoop]
		ENDFOR
		
		PARTICIPANT_INDEX tempPart
		
		FOR iLoop = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
			
			IF ((NOT IS_BIT_SET(MC_playerBD[iLoop].iClientBitSet, PBBOOL_ANY_SPECTATOR))
				 OR IS_BIT_SET(MC_playerBD[iLoop].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
			AND (NOT IS_BIT_SET(MC_playerBD[iLoop].iClientBitSet,PBBOOL_PLAYER_FAIL))
			AND (NOT IS_BIT_SET(MC_playerBD[iLoop].iClientBitSet, PBBOOL_FINISHED))
			AND IS_BIT_SET(MC_playerBD[iLoop].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
			AND IS_BIT_SET(iTeamBS, MC_playerBD[iLoop].iteam)
				
				tempPart = INT_TO_PARTICIPANTINDEX(iLoop)
				
				IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
					
					iPartsChecked++
					
					IF ((NOT bSecondaryBounds) AND IS_BIT_SET(MC_playerBD[iLoop].iClientBitSet2, PBBOOL2_IN_MISSION_BOUNDS))
					OR (bSecondaryBounds AND IS_BIT_SET(MC_playerBD[iLoop].iClientBitSet2, PBBOOL2_IN_MISSION_BOUNDS2))
						bSomeoneInBounds = TRUE
						iLoop = MAX_NUM_MC_PLAYERS // Break out!
					ENDIF
				ENDIF
				
			ENDIF
			
			IF iPartsChecked >= iPartsToCheck
				iLoop = MAX_NUM_MC_PLAYERS // Break out, we've checked everyone we need to
			ENDIF
		ENDFOR
		
	ENDIF
	
	RETURN bSomeoneInBounds
	
ENDFUNC


PROC HANDLE_BOUNDS_BLIP_COLOUR(LEGACY_BOUNDS_STRUCT& boundsInfo, BLIP_INDEX& blipIndex)
	IF DOES_BLIP_EXIST(blipIndex)
	AND boundsInfo.hudColouring != HUD_COLOUR_YELLOW
		INT iColour = GET_BLIP_COLOUR_FROM_HUD_COLOUR(boundsInfo.hudColouring)
		SET_BLIP_COLOUR(blipIndex,iColour)
		PRINTLN("[HANDLE_BOUNDS_BLIP_COLOUR] Setting blip to use a different colour: ",iColour)
	ENDIF
ENDPROC

PROC HANDLE_BOUNDS_BLIP_HEIGHT_THRESHOLD(LEGACY_BOUNDS_STRUCT& boundsInfo, BLIP_INDEX& blipIndex)

	IF DOES_BLIP_EXIST(blipIndex)
		IF (boundsInfo.iBoundsBlipHeightThreshold = BLIP_HEIGHT_THRESHOLD_DEFAULT)
			PRINTLN("[HANDLE_BOUNDS_BLIP_HEIGHT_THRESHOLD] Setting bounds_blip to use default height threshold")
			SET_BLIP_EXTENDED_HEIGHT_THRESHOLD(blipIndex, FALSE)
			SET_BLIP_SHORT_HEIGHT_THRESHOLD(blipIndex, FALSE)
		ELIF (boundsInfo.iBoundsBlipHeightThreshold = BLIP_HEIGHT_THRESHOLD_EXTENDED)
			PRINTLN("[HANDLE_BOUNDS_BLIP_HEIGHT_THRESHOLD] Setting bounds_blip to use extended height threshold")
			SET_BLIP_SHORT_HEIGHT_THRESHOLD(blipIndex, FALSE)
			SET_BLIP_EXTENDED_HEIGHT_THRESHOLD(blipIndex, TRUE)
		ELIF (boundsInfo.iBoundsBlipHeightThreshold = BLIP_HEIGHT_THRESHOLD_SHORT)
			PRINTLN("[HANDLE_BOUNDS_BLIP_HEIGHT_THRESHOLD] Setting bounds_blip to use short height threshold")
			SET_BLIP_EXTENDED_HEIGHT_THRESHOLD(blipIndex, FALSE)
			SET_BLIP_SHORT_HEIGHT_THRESHOLD(blipIndex, TRUE)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL ADD_FMMC_BOUNDS_BLIP(INT iTeam, INT iRule, BOOL bSecondary = FALSE)
	
	INT iColour
	BOOL bBlipped, bEnemy
	
	LEGACY_BOUNDS_STRUCT boundsStruct
	
	IF NOT bSecondary
		boundsStruct = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule]
	ELSE
		boundsStruct = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule]
	ENDIF
	
	IF boundsStruct.iEntityID > -1
		
		INT iEntityID = boundsStruct.iEntityID
		
		SWITCH boundsStruct.iEntityType
			
			CASE ciRULE_TYPE_PED
				IF iEntityID < FMMC_MAX_PEDS
				AND NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iEntityID])
					bBlipped = TRUE
					
					bEnemy = (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEntityID].iTeam[iteam] != ciPED_RELATION_SHIP_LIKE)
					
					IF bEnemy
						iColour = BLIP_COLOUR_RED
					ELSE
						iColour = BLIP_COLOUR_BLUE
					ENDIF
					
					CREATE_PED_BOUNDS_BLIP_WITH_COLOUR(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iEntityID]), iEntityID, iColour, bSecondary)
				ENDIF
			BREAK
			
			CASE ciRULE_TYPE_VEHICLE
				IF iEntityID < FMMC_MAX_VEHICLES
				AND NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iEntityID])
					bBlipped = TRUE
					
					VEHICLE_INDEX tempVeh
					tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iEntityID])
					
					PED_INDEX driverPed
					driverPed = GET_PED_IN_VEHICLE_SEAT(tempVeh)
					
					iColour = BLIP_COLOUR_BLUEDARK
					
					IF NOT IS_PED_INJURED(driverPed)
						IF IS_PED_A_PLAYER(driverPed)
							IF NETWORK_IS_PLAYER_A_PARTICIPANT(NETWORK_GET_PLAYER_INDEX_FROM_PED(driverPed))
							AND NETWORK_IS_PARTICIPANT_ACTIVE(NETWORK_GET_PARTICIPANT_INDEX(NETWORK_GET_PLAYER_INDEX_FROM_PED(driverPed)))
								INT iPart
								iPart = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(NETWORK_GET_PLAYER_INDEX_FROM_PED(driverPed)))
								
								IF NOT DOES_TEAM_LIKE_TEAM(iTeam, MC_playerBD[iPart].iteam)
									iColour = BLIP_COLOUR_RED
								ELSE
									iColour = BLIP_COLOUR_BLUE
								ENDIF
							ENDIF
						ELSE
							INT iDriver
							iDriver = IS_ENTITY_A_MISSION_CREATOR_ENTITY(driverPed)
							
							IF iDriver > -1
								IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iDriver].iTeam[iteam] != ciPED_RELATION_SHIP_LIKE
									iColour = BLIP_COLOUR_RED
								ELSE
									iColour = BLIP_COLOUR_BLUE
								ENDIF
							ELIF (IS_ENTITY_A_CHASE_PED(driverPed) > -1)
								iColour = BLIP_COLOUR_RED
							ENDIF
						ENDIF
					ENDIF
					
					CREATE_BLIP_FOR_MP_VEHICLE_BOUNDS(iEntityID, tempVeh, iColour, bSecondary)
				ENDIF
			BREAK
			
			CASE ciRULE_TYPE_OBJECT
				IF iEntityID < FMMC_MAX_NUM_OBJECTS
				AND NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iEntityID])
					bBlipped = TRUE
					CREATE_OBJECT_BOUNDS_BLIP_WITH_PARAMS(NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niObject[iEntityID]), iEntityID, BLIP_COLOUR_GREEN, bSecondary)
				ENDIF
			BREAK
			
			//CASE ciRULE_TYPE_GOTO
				//IF iEntityID < FMMC_MAX_RULES
					//vEntityLoc = GET_LOCATION_VECTOR(iEntityID)
				//ENDIF
			//BREAK
			
			CASE ciRULE_TYPE_PLAYER
				//Get the player closest to the original spawn position:
				IF iEntityID < MC_serverBD.iNumberOfTeams
				AND (MC_serverBD.iNumberOfPlayingPlayers[iEntityID] > 0)
					
					INT iPartLoop
					INT iClosestPart
					iClosestPart = -1
					INT iTeamChecked
					FLOAT fClosestDist2
					fClosestDist2 = 999999999 //Furthest distance apart on map is O(10,000)
					FLOAT fDistance2
					PLAYER_INDEX tempPlayer
					
					FOR iPartLoop = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartLoop))
						AND (MC_playerBD[iPartLoop].iteam = iEntityID)
							iTeamChecked++
							
							tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartLoop))
							
							IF IS_NET_PLAYER_OK(tempPlayer)
							AND NOT IS_PLAYER_SPECTATOR_ONLY(tempPlayer)
								fDistance2 = VDIST2(GET_PLAYER_COORDS(tempPlayer),GET_ENTITY_COORDS(LocalPlayerPed, FALSE))
								
								IF fDistance2 < fClosestDist2
									fClosestDist2 = fDistance2
									iClosestPart = iPartLoop
								ENDIF
							ENDIF
						ENDIF
						
						IF iTeamChecked >= MC_serverBD.iNumberOfPlayingPlayers[iEntityID]
							iPartLoop = MAX_NUM_MC_PLAYERS // Break out!
						ENDIF
					ENDFOR
					
					IF iClosestPart != -1
						bBlipped = TRUE
						bEnemy = DOES_TEAM_LIKE_TEAM(iTeam, iEntityID)
						
						IF NOT bSecondary
							bounds_blip = CREATE_BLIP_FOR_PED(GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iClosestPart))), bEnemy)
						ELSE
							sec_bounds_blip = CREATE_BLIP_FOR_PED(GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iClosestPart))), bEnemy)
						ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		
		IF bSecondary
			HANDLE_BOUNDS_BLIP_HEIGHT_THRESHOLD(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule], sec_bounds_blip)
			HANDLE_BOUNDS_BLIP_COLOUR(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule], sec_bounds_blip)
		ELSE
			HANDLE_BOUNDS_BLIP_HEIGHT_THRESHOLD(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule], bounds_blip)
			HANDLE_BOUNDS_BLIP_COLOUR(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule], bounds_blip)
		ENDIF
	ENDIF
	
	RETURN bBlipped
	
ENDFUNC

FUNC TEXT_LABEL_63 GET_FMMC_BOUNDS_ZONE_NAME_TEXT_LABEL(INT iTeam, INT iRule, BOOL bSecondary = FALSE)
	
	TEXT_LABEL_63 tlZoneName
	LEGACY_BOUNDS_STRUCT boundsStruct
	
	IF NOT bSecondary
		boundsStruct = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule]
	ELSE
		boundsStruct = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule]
	ENDIF
	
	IF boundsStruct.iEntityID > -1
		
		INT iEntityID = boundsStruct.iEntityID
		
		SWITCH boundsStruct.iEntityType
			
			CASE ciRULE_TYPE_PED
				IF iEntityID < FMMC_MAX_PEDS
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEntityID].iCustomPedName != -1
					AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEntityID].iCustomPedName])
						tlZoneName = "~b~"
						tlZoneName += g_FMMC_STRUCT_ENTITIES.tlCustomPedName[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEntityID].iCustomPedName]
					ELSE
						IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEntityID].iTeam[iTeam] != ciPED_RELATION_SHIP_LIKE
							tlZoneName = GET_FILENAME_FOR_AUDIO_CONVERSATION("MC_BNDRPD")
						ELSE
							tlZoneName = GET_FILENAME_FOR_AUDIO_CONVERSATION("MC_BNDBPD")
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE ciRULE_TYPE_VEHICLE
				IF iEntityID < FMMC_MAX_VEHICLES
					IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntityID].iCustomVehName != -1
					AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomVehName[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntityID].iCustomVehName])
						tlZoneName = "~HUD_COLOUR_BLUEDARK~"
						tlZoneName += g_FMMC_STRUCT_ENTITIES.tlCustomVehName[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntityID].iCustomVehName]
					ELIF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iEntityID])
						
						VEHICLE_INDEX tempVeh
						tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iEntityID])
						
						PED_INDEX driverPed
						driverPed = GET_PED_IN_VEHICLE_SEAT(tempVeh)
						
						BOOL bEnemy
						
						IF NOT IS_PED_INJURED(driverPed)
							IF IS_PED_A_PLAYER(driverPed)
								IF NETWORK_IS_PLAYER_A_PARTICIPANT(NETWORK_GET_PLAYER_INDEX_FROM_PED(driverPed))
								AND NETWORK_IS_PARTICIPANT_ACTIVE(NETWORK_GET_PARTICIPANT_INDEX(NETWORK_GET_PLAYER_INDEX_FROM_PED(driverPed)))
									INT iPart
									iPart = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(NETWORK_GET_PLAYER_INDEX_FROM_PED(driverPed)))
									
									IF NOT DOES_TEAM_LIKE_TEAM(iTeam, MC_playerBD[iPart].iteam)
										bEnemy = TRUE
									ENDIF
								ENDIF
							ELSE
								INT iDriver
								iDriver = IS_ENTITY_A_MISSION_CREATOR_ENTITY(driverPed)
								
								IF iDriver > -1
									IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iDriver].iTeam[iteam] != ciPED_RELATION_SHIP_LIKE
										bEnemy = TRUE
									ENDIF
								ELIF (IS_ENTITY_A_CHASE_PED(driverPed) > -1)
									bEnemy = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						IF bEnemy
							tlZoneName = GET_FILENAME_FOR_AUDIO_CONVERSATION("MC_BNDRVH")
						ELSE
							tlZoneName = GET_FILENAME_FOR_AUDIO_CONVERSATION("MC_BNDBVH")
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE ciRULE_TYPE_OBJECT
				IF iEntityID < FMMC_MAX_NUM_OBJECTS
					IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntityID].iCustomObjName != -1
					AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomObjName[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntityID].iCustomObjName])
						tlZoneName = "~g~"
						tlZoneName += g_FMMC_STRUCT_ENTITIES.tlCustomObjName[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntityID].iCustomObjName]
					ELSE
						tlZoneName = GET_FILENAME_FOR_AUDIO_CONVERSATION("MC_BNDOBJ")
					ENDIF
				ENDIF
			BREAK
			
			CASE ciRULE_TYPE_PLAYER
				IF iEntityID < MC_serverBD.iNumberOfTeams
				AND (MC_serverBD.iNumberOfPlayingPlayers[iEntityID] > 0)
					INT iPartLoop
					INT iClosestPart
					iClosestPart = -1
					INT iTeamChecked
					FLOAT fClosestDist2
					fClosestDist2 = 999999999 //Furthest distance apart on map is O(10,000)
					FLOAT fDistance2
					PLAYER_INDEX tempPlayer
					
					FOR iPartLoop = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartLoop))
						AND (MC_playerBD[iPartLoop].iteam = iEntityID)
						
							iTeamChecked++
							
							tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartLoop))
							
							IF IS_NET_PLAYER_OK(tempPlayer)
							AND NOT IS_PLAYER_SPECTATOR_ONLY(tempPlayer)
								fDistance2 = VDIST2(GET_PLAYER_COORDS(tempPlayer),GET_ENTITY_COORDS(LocalPlayerPed, FALSE))
								
								IF fDistance2 < fClosestDist2
									fClosestDist2 = fDistance2
									iClosestPart = iPartLoop
								ENDIF
							ENDIF
						ENDIF
						
						IF iTeamChecked >= MC_serverBD.iNumberOfPlayingPlayers[iEntityID]
							iPartLoop = MAX_NUM_MC_PLAYERS // Break out!
						ENDIF
					ENDFOR
					
					IF iClosestPart != -1
						IF DOES_TEAM_LIKE_TEAM(iTeam, iEntityID)
							tlZoneName = "~b~"
						ELSE
							tlZoneName = "~r~"
						ENDIF
						
						tlZoneName += MC_serverBD_2.tParticipantNames[iClosestPart]
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN tlZoneName
	
ENDFUNC

PROC PROCESS_LAST_DAMAGER_TIMER()
	IF HAS_NET_TIMER_STARTED(tdLastDamagerTimer)
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdLastDamagerTimer) > 7500
			PRINTLN("[KH] PROCESS_LAST_DAMAGER_TIMER - Resetting last damager")
			iPartLastDamager = -1
			RESET_NET_TIMER(tdLastDamagerTimer)
		ELSE
			IF IS_PED_INJURED(piPlayerPedLastDamager)
				PRINTLN("[KH] PROCESS_LAST_DAMAGER_TIMER - Player that damaged the local player is dead, resetting last damager")
				iPartLastDamager = -1
				RESET_NET_TIMER(tdLastDamagerTimer)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC INT ATTRIBUTE_KILL_TO_ENEMY_PLAYER()
	INT iPlayerToReturn = -1

	IF iPartLastDamager != -1
	AND NOT IS_PARTICIPANT_A_SPECTATOR(iPartLastDamager)
		PRINTLN("[KH] ATTRIBUTE_KILL_TO_ENEMY_PLAYER - iPartLastDamager: ", iPartLastDamager)
		iPlayerToReturn = iPartLastDamager
	ELSE
		PRINTLN("[KH] ATTRIBUTE_KILL_TO_ENEMY_PLAYER - No damager, giving kill to closest player from other team")
		iPlayerToReturn = NATIVE_TO_INT(FIND_CLOSEST_PLAYER_INDEX_OF_TEAM(MC_playerBD[iPartToUse].iteam, TRUE))
	ENDIF
	
	IF iPlayerToReturn = -1
		PRINTLN("[KH] ATTRIBUTE_KILL_TO_ENEMY_PLAYER - None found, returning local player")
		iPlayerToReturn = NATIVE_TO_INT(LocalPlayer)
	ENDIF
	
	RETURN iPlayerToReturn
ENDFUNC

PROC EXPLODE_LOCAL_PLAYER_DUE_TO_BOUNDS_FAIL(INT iTeam, INT iRule, BOOL bReadyForSpectate = FALSE, BOOL bSecondary = FALSE, BOOL bEither = FALSE, BOOL bForceExplodeOnRuleCheck = FALSE)
	
	IF SHOULD_RULE_EXPLODE_ON_FAIL(iTeam, iRule, bSecondary, bEither)
	OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[iRule], ciBS_RULE11_EXPLODE_ON_RULE_END_BOUNDS_FAIL)
	OR bForceExplodeOnRuleCheck	
		IF NOT IS_PED_INJURED(LocalPlayerPed)
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			AND NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
				
				VEHICLE_INDEX viVehiclePlayerIsIn = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
				
				IF bReadyForSpectate
					SET_BIT(iLocalBoolCheck14, LBOOL14_OUT_OF_BOUNDS_DEATHFAIL)
					SET_GAME_STATE_ON_DEATH(AFTERLIFE_SET_SPECTATORCAM_AFTER_KILLSTRIP)
				ENDIF
				
				//[KH] If player is in a vehicle and it is set to be explosion proof, remove this before exploding
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciVEHICLE_EXPLOSION_PROOF)
				OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_VEHICLE_WEAPON_BULLETPROOF_PLAYER)
					SET_ENTITY_PROOFS(viVehiclePlayerIsIn, FALSE, FALSE, FALSE, FALSE, FALSE)
				ENDIF
				
				IF NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)				
					SET_ENTITY_INVINCIBLE(LocalPlayerPed, FALSE)
				ENDIF
				
				SET_ENTITY_INVINCIBLE(viVehiclePlayerIsIn, FALSE)
				
				//IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA(g_FMMC_STRUCT.iAdversaryModeType)
					SET_ENTITY_PROOFS(LocalPlayerPed, FALSE, FALSE, FALSE, FALSE, FALSE)
				//ENDIF
				
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA(g_FMMC_STRUCT.iAdversaryModeType)
				AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciVEHICLE_WEAPON_DEATHMATCH_TOGGLE)
				AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciPRON_DEATHMATCH_TOGGLE_CORONA)
					SET_KILL_STRIP_OVER_RIDE_PLAYER_ID(NETWORK_GET_PLAYER_INDEX(INT_TO_NATIVE(PARTICIPANT_INDEX, ATTRIBUTE_KILL_TO_ENEMY_PLAYER())))
					IF DOES_ENTITY_EXIST(viVehiclePlayerIsIn)
						NETWORK_EXPLODE_VEHICLE(viVehiclePlayerIsIn, DEFAULT, DEFAULT, ATTRIBUTE_KILL_TO_ENEMY_PLAYER())
						//Reset the last damager on this player
						iPartLastDamager = -1
					ENDIF
				ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_SUMO_RUN(g_FMMC_STRUCT.iAdversaryModeType)
					IF DOES_ENTITY_EXIST(viVehiclePlayerIsIn)
						NETWORK_EXPLODE_VEHICLE(viVehiclePlayerIsIn)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(viVehiclePlayerIsIn)
					ENDIF
				ELSE
					IF DOES_ENTITY_EXIST(viVehiclePlayerIsIn)
						NETWORK_EXPLODE_VEHICLE(viVehiclePlayerIsIn)
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Enable_Turn_Rotor_Tag_Team)
							RESET_NET_TIMER(vehicleCleanUpTimer)
							START_NET_TIMER(vehicleCleanUpTimer)
							SET_BIT(iLocalBoolCheck16, LBOOL16_VEHICLE_MARKED_FOR_CLEAN_UP)
							IF NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(viVehiclePlayerIsIn)
								NETWORK_INDEX niVeh = VEH_TO_NET(viVehiclePlayerIsIn)
								START_NET_TIMER(vehicleCleanUpTimer)
								netVehicleToCleanUp = niVeh
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				
				PRINTLN("[RCC MISSION] EXPLODE_LOCAL_PLAYER_DUE_TO_BOUNDS_FAIL - calling NETWORK_EXPLODE_VEHICLE on player vehicle due to bounds fail")
			ELSE
				IF NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_OOB_ON_FOOT_EXPLODE_DONE)
					VECTOR vPlayer = GET_ENTITY_COORDS(LocalPlayerPed)
					
					FLOAT fGroundZ
					IF NOT IS_ENTITY_IN_AIR(LocalPlayerPed)
					AND GET_GROUND_Z_FOR_3D_COORD(vPlayer, fGroundZ)
						vPlayer.Z = fGroundZ
					ELSE
						VECTOR vGround = GET_PED_BONE_COORDS(LocalPlayerPed, BONETAG_R_FOOT, <<0, 0, -0.1>>)
						
						IF NOT IS_VECTOR_ZERO(vGround)
							vPlayer = vGround
						ENDIF
					ENDIF
					
					IF NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
						SET_ENTITY_INVINCIBLE(LocalPlayerPed, FALSE)
					ENDIF
					
					SET_ENTITY_PROOFS(LocalPlayerPed, FALSE, FALSE, FALSE, FALSE, FALSE)
					IF NOT IS_PED_SWIMMING(LocalPlayerPed)
						ADD_EXPLOSION(vPlayer, EXP_TAG_GRENADELAUNCHER, 1.0)
					
						SET_PED_TO_RAGDOLL(LocalPlayerPed, 5000, 10000, TASK_RELAX)
					
						VECTOR vVelocity = GET_ENTITY_VELOCITY(LocalPlayerPed)
						SET_ENTITY_VELOCITY(LocalPlayerPed, vVelocity + <<0, 0, 15>>)
					ELSE
						SET_ENTITY_HEALTH(LocalPlayerPed, 0)
					ENDIF
					
					PRINTLN("[RCC MISSION] EXPLODE_LOCAL_PLAYER_DUE_TO_BOUNDS_FAIL - calling ADD_EXPLOSION at player coords ",vPlayer)
					
					SET_BIT(iLocalBoolCheck26, LBOOL26_OOB_ON_FOOT_EXPLODE_DONE)
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciNEGATIVE_SUICIDE_SCORE)
			BROADCAST_FMMC_PLAYER_COMMITED_SUICIDE(iTeam)
			
			//Negative score
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciNEGATIVE_SUICIDE_SCORE)
			AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRQUOTA(g_FMMC_STRUCT.iAdversaryModeType)
			AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_GUNSMITH(g_FMMC_STRUCT.iAdversaryModeType)
				IF MC_playerBD[iPartToUse].iNumPlayerKills > 0
					MC_playerBD[iPartToUse].iNumPlayerKills--
					PRINTLN("[JS] - EXPLODE_LOCAL_PLAYER_DUE_TO_BOUNDS_FAIL SUICIDE POINTS - decrementing local player's kills")
				ENDIF
			
				IF MC_serverBD.iNumPlayerRuleHighestPriority[MC_playerBD[iPartToUse].iteam] > 0 
					IF MC_serverBD_4.iPlayerRuleMissionLogic[MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL_PLAYERS // this high priority kill player objective
						IF MC_playerBD[iPartToUse].iPlayerScore > 0
							PRINTLN("[TMS] - EXPLODE_LOCAL_PLAYER_DUE_TO_BOUNDS_FAIL SUICIDE POINTS - decrementing local player's score (KILL_PLAYERS)")
							INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(-1)
						ENDIF
					ELIF MC_serverBD_4.iPlayerRuleMissionLogic[MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL_TEAM0	
					OR MC_serverBD_4.iPlayerRuleMissionLogic[MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL_TEAM1	
					OR MC_serverBD_4.iPlayerRuleMissionLogic[MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL_TEAM2
					OR MC_serverBD_4.iPlayerRuleMissionLogic[MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL_TEAM3
						IF MC_playerBD[iPartToUse].iPlayerScore > 0
						AND MC_Playerbd[iPartToUse].iKillScore > 0
							PRINTLN("[JS] - EXPLODE_LOCAL_PLAYER_DUE_TO_BOUNDS_FAIL SUICIDE POINTS - decrementing local player's score (KILL_TEAM)")
							INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(-1,TRUE)
						ENDIF
					ENDIF	
				ELSE
					IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iPointsPerKill > 0
						IF MC_playerBD[iPartToUse].iPlayerScore >= g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iPointsPerKill
							PRINTLN("[JS] - EXPLODE_LOCAL_PLAYER_DUE_TO_BOUNDS_FAIL SUICIDE POINTS - iPoints per kill = ", g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iPointsPerKill)
							INCREMENT_LOCAL_PLAYER_SCORE_BY((g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iPointsPerKill * -1),TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF

		ENDIF
		
	ENDIF
	
ENDPROC

FUNC BOOL IS_MISSION_BOUND_VALID_FOR_USE(INT iTeam, INT iRule, INT iBounds)
		
	IF (iBounds = 0 AND IS_BIT_SET(iLocalBoolCheck29, LBOOL29_DELAY_BLOCK_MISSION_BOUNDS_1))
	OR (iBounds = 1 AND IS_BIT_SET(iLocalBoolCheck29, LBOOL29_DELAY_BLOCK_MISSION_BOUNDS_2))
		PRINTLN("[LM][PROCESS_MISSION_BOUNDS_DELAYS] - IS_MISSION_BOUND_VALID_FOR_USE Returning False.")
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(iBounds, MC_serverBD_4.rsgSpawnSeed, eSGET_Bounds, iTeam, iRule)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL SHOULD_BOUNDS_SPAWN_POSITION_UPDATE(INT iTeam, INT iRule, VECTOR vBoundsPos)
	
	BOOL bUpdate = FALSE
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iEntityType > 0
	AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iEntityID != -1
	AND IS_MISSION_BOUND_VALID_FOR_USE(iTeam, iRule, 0)
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].sBoundsStruct[iRule].iBoundsBS, ciBounds_LEGACY_DelayedSpawnAreaPosition)
			
			IF NOT IS_VECTOR_ZERO(vBoundsSpawnPos_Temp)
				FLOAT fDiff2 = VDIST2(vBoundsPos, vBoundsSpawnPos_Temp)
				
				IF fDiff2 > (g_FMMC_STRUCT.sFMMCEndConditions[iteam].sBoundsStruct[iRule].fRadius * g_FMMC_STRUCT.sFMMCEndConditions[iteam].sBoundsStruct[iRule].fRadius)
					PRINTLN("[RCC MISSION][bounds] SHOULD_BOUNDS_SPAWN_POSITION_UPDATE - Returning TRUE as spawn position has moved enough: fDiff2 = ",fDiff2," > fRadius2 = ",(g_FMMC_STRUCT.sFMMCEndConditions[iteam].sBoundsStruct[iRule].fRadius * g_FMMC_STRUCT.sFMMCEndConditions[iteam].sBoundsStruct[iRule].fRadius))
					bUpdate = TRUE
				ENDIF
				
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdBoundsSpawnDelayTimer) >= 10000
					PRINTLN("[RCC MISSION][bounds] SHOULD_BOUNDS_SPAWN_POSITION_UPDATE - Returning TRUE as timer has passed 10 seconds")
					bUpdate = TRUE
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION][bounds] SHOULD_BOUNDS_SPAWN_POSITION_UPDATE - Returning TRUE as vBoundsSpawnPos_Temp is zero")
				bUpdate = TRUE
			ENDIF
			
		ELSE
			bUpdate = TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].iEntityType > 0
	AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].iEntityID != -1
	AND IS_MISSION_BOUND_VALID_FOR_USE(iTeam, iRule, 1)
		PRINTLN("[RCC MISSION][bounds] SHOULD_BOUNDS_SPAWN_POSITION_UPDATE - We have a valid Entity to follow.")
		IF NOT IS_VECTOR_ZERO(vBoundsPos)
		AND NOT IS_VECTOR_ZERO(vBoundsSpawnPos_TempSecondary)
		AND NOT IS_VECTOR_ZERO(vBoundsSpawnPos_TempSecondary2)
			PRINTLN("[RCC MISSION][bounds] SHOULD_BOUNDS_SPAWN_POSITION_UPDATE - We have a valid Position to compare.")
			
			PRINTLN("[RCC MISSION][bounds] SHOULD_BOUNDS_SPAWN_POSITION_UPDATE - vBoundsSpawnPos_TempSecondary: ", vBoundsSpawnPos_TempSecondary)
			PRINTLN("[RCC MISSION][bounds] SHOULD_BOUNDS_SPAWN_POSITION_UPDATE - vBoundsSpawnPos_TempSecondary2: ", vBoundsSpawnPos_TempSecondary2)
			
			FLOAT fDiff2 = VDIST(vBoundsSpawnPos_TempSecondary, vBoundsSpawnPos_TempSecondary2)
			
			PRINTLN("[RCC MISSION][bounds] SHOULD_BOUNDS_SPAWN_POSITION_UPDATE - fDiff2: ", fDiff2)
			PRINTLN("[RCC MISSION][bounds] SHOULD_BOUNDS_SPAWN_POSITION_UPDATE - g_FMMC_STRUCT.sFMMCEndConditions[",iteam,"].sBoundsStruct2[",iRule,"].fRadius: ", g_FMMC_STRUCT.sFMMCEndConditions[iteam].sBoundsStruct2[iRule].fRadius)
			
			IF fDiff2 >= g_FMMC_STRUCT.sFMMCEndConditions[iteam].sBoundsStruct2[iRule].fRadius
				bUpdate = TRUE
				PRINTLN("[RCC MISSION][bounds] SHOULD_BOUNDS_SPAWN_POSITION_UPDATE - Setting true: ")
			ENDIF
		ENDIF
	ELSE
		PRINTLN("REPOS DEBUG - current team: ",iteam," Type: ",g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].iEntityType," ID: ",g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].iEntityID," Valid: ", IS_MISSION_BOUND_VALID_FOR_USE(iTeam, iRule, 1))
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_TEAM_SWAP_UPDATE_SPAWN_LOCATION)
		CLEAR_BIT(iLocalBoolCheck29, LBOOL29_TEAM_SWAP_UPDATE_SPAWN_LOCATION)
		bUpdate = TRUE
		PRINTLN("[RCC MISSION][bounds] SHOULD_BOUNDS_SPAWN_POSITION_UPDATE - Setting TRUE due to LBOOL29_TEAM_SWAP_UPDATE_SPAWN_LOCATION")
	ENDIF
	
	PRINTLN("[RCC MISSION][bounds] SHOULD_BOUNDS_SPAWN_POSITION_UPDATE - bUpdate: ", bUpdate)
	
	RETURN bUpdate
	
ENDFUNC

//[KH][SSDS] Sumo sudden death blip drawing and updating
PROC RENDER_SUDDEN_DEATH_AREA_BOUNDS_ON_MINIMAP(INT iTeam, INT iRule)
	//Set up the blip for this first, make sure old blip isn't used
	IF NOT IS_BIT_SET(iLocalBoolCheck15, LBOOL15_IS_SUDDEN_DEATH_BLIP_SETUP)
		IF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN)
			IF vSphereSpawnPosition.x != 0 AND vSphereSpawnPosition.y != 0 AND vSphereSpawnPosition.z != 0
				IF DOES_BLIP_EXIST(SuddenDeathAreaRadiusBlip)
					REMOVE_BLIP(SuddenDeathAreaRadiusBlip)
				ENDIF
				
				PRINTLN("[RCC MISSION] RENDER_SUDDEN_DEATH_AREA_BOUNDS_ON_MINIMAP : vSphereSpawnPosition", vSphereSpawnPosition)
				PRINTLN("[RCC MISSION] RENDER_SUDDEN_DEATH_AREA_BOUNDS_ON_MINIMAP : fCurrentSphereRadius", g_FMMC_STRUCT.fSphereStartRadius)
				
				SuddenDeathAreaRadiusBlip = ADD_BLIP_FOR_RADIUS( vSphereSpawnPosition, g_FMMC_STRUCT.fSphereStartRadius)
				SET_BLIP_SCALE(SuddenDeathAreaRadiusBlip, g_FMMC_STRUCT.fSphereStartRadius)
				SET_BLIP_COLOUR(SuddenDeathAreaRadiusBlip, BLIP_COLOUR_YELLOW)
				SET_BLIP_ALPHA(SuddenDeathAreaRadiusBlip, 120)
				HANDLE_BOUNDS_BLIP_COLOUR(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule], SuddenDeathAreaRadiusBlip)
				SET_BIT(iLocalBoolCheck15, LBOOL15_IS_SUDDEN_DEATH_BLIP_SETUP) 
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(SuddenDeathAreaRadiusBlip)
				REMOVE_BLIP(SuddenDeathAreaRadiusBlip)
			ENDIF
		
			PRINTLN("[RCC MISSION] RENDER_SUDDEN_DEATH_AREA_BOUNDS_ON_MINIMAP : vSphereSpawnPoint", g_FMMC_STRUCT.vSphereSpawnPoint)
			PRINTLN("[RCC MISSION] RENDER_SUDDEN_DEATH_AREA_BOUNDS_ON_MINIMAP : fCurrentSphereRadius", g_FMMC_STRUCT.fSphereStartRadius)
			
			SuddenDeathAreaRadiusBlip = ADD_BLIP_FOR_RADIUS( g_FMMC_STRUCT.vSphereSpawnPoint, g_FMMC_STRUCT.fSphereStartRadius)
			SET_BLIP_SCALE(SuddenDeathAreaRadiusBlip, g_FMMC_STRUCT.fSphereStartRadius)
			SET_BLIP_COLOUR(SuddenDeathAreaRadiusBlip, BLIP_COLOUR_YELLOW)
			SET_BLIP_ALPHA(SuddenDeathAreaRadiusBlip, 120)
			HANDLE_BOUNDS_BLIP_COLOUR(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule], SuddenDeathAreaRadiusBlip)
			SET_BIT(iLocalBoolCheck15, LBOOL15_IS_SUDDEN_DEATH_BLIP_SETUP) 
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN)
		IF DOES_BLIP_EXIST(SuddenDeathAreaRadiusBlip)
			IF fCurrentSphereRadius != 0
			AND fCurrentSphereRadius != fSphereEndRadius
				SET_BLIP_SCALE(SuddenDeathAreaRadiusBlip, fCurrentSphereRadius)
			ENDIF
		ENDIF
		
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_SUMO_RUN(g_FMMC_STRUCT.iAdversaryModeType)
			IF DOES_BLIP_EXIST(bounds_blip)
				PRINTLN("[BLIP][RH] Removing blip 1")
				REMOVE_BLIP(bounds_blip)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

INT iLocalRenderedBoundsRule

PROC RENDER_BOUNDS_AREA_ON_MINIMAP(INT iTeam, INT iRule, VECTOR vBoundsPos, BOOL primaryBounds)
	BLIP_INDEX MissionBounds
	BOOL showBoundsOnMinimap
	LEGACY_BOUNDS_STRUCT sBoundsStruct
	BOOL onlyShowBoundsOutline
	BOOL bKeepBlipOnEdge
	IF primaryBounds
		MissionBounds = biMissionBounds
		showBoundsOnMinimap = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iRule], ciBS_RULE4_SHOW_BOUNDS_ON_MINIMAP)
		sBoundsStruct = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct[ iRule ]
		onlyShowBoundsOutline = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_ONLY_SHOW_BOUNDS_OUTLINE)
		bKeepBlipOnEdge = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[iRule], ciBS_RULE11_LONG_RANGE_BLIP)
	ELSE
		MissionBounds = biMissionBounds2
		showBoundsOnMinimap = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[iRule], ciBS_RULE8_SHOW_SECONDARY_BOUNDS_ON_MINIMAP)
		sBoundsStruct = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct2[ iRule ]
		onlyShowBoundsOutline = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[iRule], ciBS_RULE8_ONLY_SHOW_SECONDARY_BOUNDS_OUTLINE)
		bKeepBlipOnEdge = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[iRule], ciBS_RULE11_LONG_RANGE_BLIP_SECONDARY)
	ENDIF
	BOOL bDisableBoundsAreaForRole
	
	IF (primaryBounds AND IS_BIT_SET(iLocalBoolCheck29, LBOOL29_DELAY_BLOCK_MISSION_BOUNDS_1))
	OR (NOT primaryBounds AND IS_BIT_SET(iLocalBoolCheck29, LBOOL29_DELAY_BLOCK_MISSION_BOUNDS_2))
		PRINTLN("[LM][PROCESS_MISSION_BOUNDS_DELAYS] - Preventing RENDER_BOUNDS_AREA_ON_MINIMAP functionality. EXIT.")
		EXIT
	ENDIF
		
	IF iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_SUB_TEAM_ROLES)
		AND MC_playerBD[iPartToUse].iCoronaRole != -1
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRolesBitset[iRule][MC_playerBD[iPartToUse].iCoronaRole], ciBS_ROLE_Cant_Complete_This_Rule)
				bDisableBoundsAreaForRole = TRUE //b* 2536711
			ENDIF
		ENDIF
		
		IF ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
		AND (IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN)
		OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN))
			RENDER_SUDDEN_DEATH_AREA_BOUNDS_ON_MINIMAP(iTeam, iRule)
			
			showBoundsOnMinimap = FALSE
		ENDIF

		IF showBoundsOnMinimap
		AND NOT bDisableBoundsAreaForRole
			IF DOES_BLIP_EXIST(MissionBounds)
				// url:bugstar:2821929 - This is a fix to switch the alpha and edge/fill method of drawing the blip area/radius.
				// We check the current alpha so we're not spamming SET natives.	
				IF NOT IS_VECTOR_ZERO(sBoundsStruct.vPos1)
				AND NOT IS_VECTOR_ZERO(sBoundsStruct.vPos2)	
					IF onlyShowBoundsOutline
					AND NOT IS_BIT_SET(iLocalBoolCheck17, LBOOL17_IS_PLAY_OUT_OF_MAIN_BOUNDS) // Is OUT the Main bounds.
						IF GET_BLIP_ALPHA(MissionBounds) != 255
							PRINTLN("[LM][RENDER_BOUNDS_AREA_ON_MINIMAP] : Calling - SET_AREA_BLIP_EDGE TRUE, 255 Alpha")
							//SET_AREA_BLIP_EDGE(MissionBounds,TRUE)
							SET_BLIP_ALPHA(MissionBounds,255)
						ENDIF						
					ELSE
						IF GET_BLIP_ALPHA(MissionBounds) != sBoundsStruct.iMinimapAlpha
							//SET_AREA_BLIP_EDGE(MissionBounds,FALSE)
							SET_BLIP_ALPHA(MissionBounds,sBoundsStruct.iMinimapAlpha)
						ENDIF
					ENDIF
						
						
					IF bKeepBlipOnEdge
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_BOUNDS_DISABLE_RADAR_EDGE_BLIP)
							SET_BLIP_AS_SHORT_RANGE(MissionBounds, TRUE)
						ENDIF
					ENDIF

				ELSE
					IF onlyShowBoundsOutline
					AND NOT IS_BIT_SET(iLocalBoolCheck17, LBOOL17_IS_PLAY_OUT_OF_MAIN_BOUNDS) // Is OUT the Main bounds.
						IF GET_BLIP_ALPHA(MissionBounds) != 255
							PRINTLN("[LM][RENDER_BOUNDS_AREA_ON_MINIMAP] : Calling - SET_AREA_BLIP_Radius TRUE, 255 Alpha")
							//SET_RADIUS_BLIP_EDGE(MissionBounds,TRUE)
							SET_BLIP_ALPHA(MissionBounds,255)
						ENDIF
					ELSE
						IF GET_BLIP_ALPHA(MissionBounds) != sBoundsStruct.iMinimapAlpha
							PRINTLN("[LM][RENDER_BOUNDS_AREA_ON_MINIMAP] : Calling - SET_AREA_BLIP_Radius TRUE, iMinimapAlpha Alpha:", sBoundsStruct.iMinimapAlpha)
							//SET_RADIUS_BLIP_EDGE(MissionBounds,FALSE)
							SET_BLIP_ALPHA(MissionBounds,sBoundsStruct.iMinimapAlpha)
						ENDIF
					ENDIF					
				ENDIF
				
				IF bKeepBlipOnEdge
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_BOUNDS_DISABLE_RADAR_EDGE_BLIP)
						SET_BLIP_AS_SHORT_RANGE(MissionBounds, TRUE)
					ENDIF
				ENDIF
				
				IF CONTENT_IS_USING_ARENA()
					SET_BLIP_DISPLAY(MissionBounds, DISPLAY_RADAR_ONLY)
				ENDIF
				
				//update the blip if we're following the centre of a vehicle
				IF (sBoundsStruct.iEntityID > -1
				AND sBoundsStruct.iEntityType = ciRULE_TYPE_VEHICLE)
					SET_BLIP_COORDS(MissionBounds, vBoundsPos)
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_ONLY_SHOW_MINIMAP_BOUNDS_ON_FIRST)
					 INT iTemp = GET_PARTICIPANT_NUMBER_FROM_TEAM_PARTICIPANT_NUMBER(MC_serverBD.iWinningTeam, 0)
					 IF iTemp != -1
					 	IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iTemp))
							PED_INDEX tempPed = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iTemp)))
							SET_BLIP_COORDS(MissionBounds, GET_ENTITY_COORDS(tempPed))
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_ONLY_SHOW_MINIMAP_BOUNDS_ON_FIRST)
					SET_BLIP_SCALE(MissionBounds, TINY_RACERS_GET_CURRENT_BOUNDS_SIZE(iTeam, iRule))
				ENDIF
				
				IF iRule != iLocalRenderedBoundsRule
					IF DOES_BLIP_EXIST(MissionBounds) 
						REMOVE_BLIP(MissionBounds)
						PRINTLN("[RCC MISSION] RENDER_BOUNDS_AREA_ON_MINIMAP - rule has progressed and has a bounds on it but isn't the same rule")
					ENDIF
				ENDIF
				
			ENDIF		
			
			IF NOT DOES_BLIP_EXIST(MissionBounds)
				IF NOT IS_VECTOR_ZERO(sBoundsStruct.vPos1)
				AND NOT IS_VECTOR_ZERO(sBoundsStruct.vPos2)
					PRINTLN("[RCC MISSION] RENDER_BOUNDS_AREA_ON_MINIMAP : vPos1", sBoundsStruct.vPos1)
					PRINTLN("[RCC MISSION] RENDER_BOUNDS_AREA_ON_MINIMAP : vPos2", sBoundsStruct.vPos2)
					PRINTLN("[RCC MISSION] RENDER_BOUNDS_AREA_ON_MINIMAP : fWidth", sBoundsStruct.fWidth)				
					PRINTLN("[BLIP][RH] Creating blip 2")
					MissionBounds = ADD_BLIP_FOR_AREA_FROM_EDGES( sBoundsStruct.vPos1, 
															 sBoundsStruct.vPos2, 																												
															 sBoundsStruct.fWidth )
					SET_BLIP_COLOUR(MissionBounds,BLIP_COLOUR_YELLOW)
					
					HANDLE_BOUNDS_BLIP_COLOUR(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule], MissionBounds)
									
					// Init.
					IF onlyShowBoundsOutline
					AND NOT IS_BIT_SET(iLocalBoolCheck17, LBOOL17_IS_PLAY_OUT_OF_MAIN_BOUNDS) // Is OUT the Main bounds.
						PRINTLN("[LM][RENDER_BOUNDS_AREA_ON_MINIMAP] : Calling - SET_AREA_BLIP_EDGE TRUE, Alhpa 255 (INIT)")
						//SET_AREA_BLIP_EDGE(MissionBounds,TRUE)
						SET_BLIP_ALPHA(MissionBounds,255)
					ELSE
						PRINTLN("[LM][RENDER_BOUNDS_AREA_ON_MINIMAP] : Calling - SET_AREA_BLIP_EDGE FALSE, Alhpa iMinimapAlpha (INIT)")
						//SET_AREA_BLIP_EDGE(MissionBounds,FALSE)
						SET_BLIP_ALPHA(MissionBounds,sBoundsStruct.iMinimapAlpha)
					ENDIF
					iLocalRenderedBoundsRule = iRule
					PRINTLN("[RCC MISSION] RENDER_BOUNDS_AREA_ON_MINIMAP - Storing current bounds rule value iLocalRenderedBoundsRule: " ,iLocalRenderedBoundsRule)
					
					IF bKeepBlipOnEdge
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_BOUNDS_DISABLE_RADAR_EDGE_BLIP)
							PRINTLN("[LM][RENDER_BOUNDS_AREA_ON_MINIMAP] : Calling - SET_BLIP_AS_SHORT_RANGE MissionBounds (1)")
							SET_BLIP_AS_SHORT_RANGE(MissionBounds, TRUE)
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_VECTOR_ZERO(sBoundsStruct.vPos)
						PRINTLN("[RCC MISSION] RENDER_BOUNDS_RADIUS_ON_MINIMAP : vPos", sBoundsStruct.vPos)
						PRINTLN("[RCC MISSION] RENDER_BOUNDS_RADIUS_ON_MINIMAP : vPos1", sBoundsStruct.vPos1)
						PRINTLN("[RCC MISSION] RENDER_BOUNDS_RADIUS_ON_MINIMAP : fWidth", sBoundsStruct.fWidth)
						PRINTLN("[RCC MISSION] RENDER_BOUNDS_RADIUS_ON_MINIMAP : fRadius", sBoundsStruct.fRadius)
						MissionBounds = ADD_BLIP_FOR_RADIUS( vBoundsPos, sBoundsStruct.fRadius )
						SET_BLIP_SCALE(MissionBounds,sBoundsStruct.fRadius)
						SET_BLIP_COLOUR(MissionBounds,BLIP_COLOUR_YELLOW)
						PRINTLN("[BLIP][RH] Creating blip 3")
						// Init.
						IF onlyShowBoundsOutline
						AND NOT IS_BIT_SET(iLocalBoolCheck17, LBOOL17_IS_PLAY_OUT_OF_MAIN_BOUNDS) // Is OUT the Main bounds.
							PRINTLN("[LM][RENDER_BOUNDS_AREA_ON_MINIMAP] : Calling - SET_RADIUS_BLIP_EDGE, Alpha 255 (INIT)")
							//SET_RADIUS_BLIP_EDGE(MissionBounds,TRUE)
							SET_BLIP_ALPHA(MissionBounds,255)
						ELSE							
							PRINTLN("[LM][RENDER_BOUNDS_AREA_ON_MINIMAP] : Calling - Radius, Alpha iMinimapAlpha (INIT)")
							SET_BLIP_ALPHA(MissionBounds,sBoundsStruct.iMinimapAlpha)
						ENDIF
						
						HANDLE_BOUNDS_BLIP_COLOUR(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule], MissionBounds)
						
						IF bKeepBlipOnEdge
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_BOUNDS_DISABLE_RADAR_EDGE_BLIP)
								PRINTLN("[LM][RENDER_BOUNDS_AREA_ON_MINIMAP] : Calling - SET_BLIP_AS_SHORT_RANGE MissionBounds (2)")
								SET_BLIP_AS_SHORT_RANGE(MissionBounds, TRUE)
							ENDIF
						ENDIF
						
						iLocalRenderedBoundsRule = iRule
					ELSE
						PRINTLN("[RCC MISSION] RENDER_BOUNDS_AREA_ON_MINIMAP : iRule: ", iRule , "is ZERO" )
					ENDIF
				ENDIF			
			ENDIF
		
		ELSE
			IF DOES_BLIP_EXIST(MissionBounds) 
				REMOVE_BLIP(MissionBounds)
				PRINTLN("[RCC MISSION] RENDER_BOUNDS_AREA_ON_MINIMAP - removing blip")
			ENDIF
		ENDIF
		
		
	ELSE
		IF DOES_BLIP_EXIST(MissionBounds) 
			REMOVE_BLIP(MissionBounds)
			PRINTLN("[RCC MISSION] RENDER_BOUNDS_AREA_ON_MINIMAP - removing blip")
		ENDIF
	ENDIF
	
	IF primaryBounds
		biMissionBounds = MissionBounds
	ELSE
		biMissionBounds2 = MissionBounds
	ENDIF
ENDPROC

PROC RENDER_BOUNDS_IN_MISSION(INT iTeam, INT iRule, VECTOR& vBoundsPos1, VECTOR& vBoundsPos2)
		INT iR, iG, iB, iA
		FLOAT fA = 0.5
		
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
			VECTOR vPlayerPos = GET_ENTITY_COORDS(LocalPlayerPed, TRUE)
			IF IS_BIT_SET(iLocalBoolCheck3, LBOOL3_IN_BOUNDS1)
			OR NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
			OR VDIST2(vPlayerPos, vBoundsPos1) < POW(TINY_RACERS_GET_CURRENT_BOUNDS_SIZE(iTeam, iRule),2)
			OR TINY_RACERS_GET_CURRENT_BOUNDS_SIZE(iTeam, iRule) < 1
			OR TINY_RACERS_GET_ROUND_STATE(iLocalPart) != TR_Racing
			OR NOT IS_BIT_SET(iLocalBoolCheck22, LBOOL22_TINY_RACERS_OK_TO_PROCESS_BOUNDS)
				EXIT
			ENDIF
		ENDIF
		
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_SUMO_RUN(g_FMMC_STRUCT.iAdversaryModeType)
			IF IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
				IF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN)
					IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
						EXIT
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iEntityID > -1
		AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iEntityType = ciRULE_TYPE_VEHICLE
			vBoundsPos1 = GET_FMMC_ENTITY_LOCATION(GET_ENTITY_COORDS(LocalPlayerPed, FALSE), g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iEntityType, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iEntityID, LocalPlayerPed)
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iEntityID = -2 
		AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iEntityType = ciRULE_TYPE_PLAYER //This is 'Team' in the creator
			vBoundsPos1 = GET_FMMC_AREA_BOUNDS_POS(iTeam, iRule)
		ENDIF
		
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].iEntityID > -1
		AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].iEntityType = ciRULE_TYPE_VEHICLE
			vBoundsPos2 = GET_FMMC_ENTITY_LOCATION(GET_ENTITY_COORDS(LocalPlayerPed, FALSE), g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].iEntityType, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].iEntityID, LocalPlayerPed)
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].iEntityID = -2 
		AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].iEntityType = ciRULE_TYPE_PLAYER //This is 'Team' in the creator			
			vBoundsPos2 = GET_FMMC_AREA_BOUNDS_POS(iTeam, iRule, TRUE)
		ENDIF
				
		IF (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[iRule], ciBS_RULE8_ONLY_SHOW_BOUNDS_WHEN_OOB) AND NOT IS_BIT_SET(iLocalBoolCheck3, LBOOL3_IN_BOUNDS1))
		OR NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[iRule], ciBS_RULE8_ONLY_SHOW_BOUNDS_WHEN_OOB)
			FLOAT fBoundsRadius
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciTINY_RACERS_SHRINK_BOUNDS)
				fBoundsRadius = TINY_RACERS_GET_CURRENT_BOUNDS_SIZE(iTeam, iRule)
				IF fBoundsRadius < 1
				AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fRadius != 0
					fBoundsRadius = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fRadius
				ENDIF
			ELSE
				fBoundsRadius = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fRadius
			ENDIF
			
			//Mission bounds
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iBoundsBS, ciBounds_LEGACY_ShowBoundsInGame)
				//Gets colour to render sphere in
				GET_HUD_COLOUR(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].hudColouring, iR, iG, iB, iA)
				IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iType = 1
				
					DRAW_ANGLED_AREA_FROM_FACES(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos1, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos2, 
					g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fWidth, iR, iG, iB, 128)

				ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iType = 0
					IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
						DRAW_MARKER_SPHERE(vBoundsPos1, fBoundsRadius, iR, iG, iB, fA)
					ENDIF
					
				ENDIF
				
			ENDIF
			
			//Secondary mission bounds
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].iBoundsBS, ciBounds_LEGACY_ShowBoundsInGame)
				//Gets colour to render sphere in
				GET_HUD_COLOUR(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].hudColouring, iR, iG, iB, iA)
				IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].iType = 1
				
					DRAW_ANGLED_AREA_FROM_FACES(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].vPos1, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].vPos2, 
					g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].fWidth, iR, iG, iB, 128)
					
				ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].iType = 0
					IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
						DRAW_MARKER_SPHERE(vBoundsPos2, fBoundsRadius, iR, iG, iB, fA)
					ENDIF
				ENDIF
				
			ENDIF
			
		ENDIF

ENDPROC

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: HANDLE BOUNDS TIMERS AND TEXT (as well as blips and, most importantly, failing the player) !
//
//************************************************************************************************************************************************************

FUNC INT GET_BOUNDS_TIME_LIMIT(INT iTeam, INT iRule, BOOL bIsPrimary = TRUE)
	INT iTimeLimit 
	IF bIsPrimary
		iTimeLimit= g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iBoundsPriTimer[iRule]
	ELSE
		iTimeLimit= g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iBoundsSecTimer[iRule]
	ENDIF
	
	PRINTLN("GET_BOUNDS_TIME_LIMIT iTimeLimit: ", iTimeLimit, " bisprimary ", bIsPrimary)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciDISABLE_BOUNDS_TIMER_OVERRIDE_FOR_SUDDEN_DEATH)
		IF ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
			IF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_SUDDEN_DEATH_KILLALLENEMIES) 
				iTimeLimit = 30000
			ELIF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL3_SUDDEN_DEATH_BE_MY_VALENTINES) 
				iTimeLimit = 60000
			ENDIF
		ENDIF
	ENDIF

	RETURN iTimeLimit
	
ENDFUNC

PROC PROCESS_BOUNDS_TAG_OUT(INT iTeam, INT iRule, BOOL bValidBounds1, BOOL bValidBounds2, BOOL bOnWrongSideOfBounds1, BOOL bOnWrongSideOfBounds2, BOOL bInBounds1, BOOL bInBounds2)
	PRINTLN("[LM][PROCESS_BOUNDS_TAG_OUT] Bool checks - bValidBounds1: ", bValidBounds1, " bValidBounds2: ", bValidBounds2, " bOnWrongSideOfBounds1: ", bOnWrongSideOfBounds1, " bOnWrongSideOfBounds2: ", bOnWrongSideOfBounds2, " bInBounds1: ", bInBounds1, " bInBounds2: ", bInBounds2)
	
	BOOL bShouldTagOut
	BOOL bIsSuddenDeath = (IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH) AND IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_SUDDEN_DEATH_TAG_TEAM))
	
	IF bOnWrongSideOfBounds1
	AND bOnWrongSideOfBounds2
		PRINTLN("[LM][PROCESS_BOUNDS_TAG_OUT][TAG_TEAM_TURNS] We are not inside a bounds, or in a Leave Area.")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[iRule], ciBS_RULE12_TAG_OUT_ON_BOUNDS_ENTRY_1)
		IF bValidBounds1
		AND bInBounds1
		//AND bInPlayBounds1		
		//AND NOT bOnWrongSideOfBounds1
			bShouldTagOut = TRUE
		ENDIF
	ENDIF	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[iRule], ciBS_RULE12_TAG_OUT_ON_BOUNDS_ENTRY_2)
		IF bValidBounds2
		AND bInBounds2
		//AND bInPlayBounds2		
		//AND NOT bOnWrongSideOfBounds2
			bShouldTagOut = TRUE
		ENDIF
	ENDIF

	IF bShouldTagOut
	AND NOT bIsSuddenDeath
		IF HAS_NET_TIMER_STARTED(MC_serverBD_3.tdTagOutCooldown[iTeam])
		AND HAS_NET_TIMER_EXPIRED_READ_ONLY(MC_serverBD_3.tdTagOutCooldown[iTeam], (g_FMMC_STRUCT.iTagTeamCooldownTime*1000))
		AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_REQUEST_TAG_OUT)
		AND iLocalPart = MC_serverBD_3.iServerTagTeamPartTurn[iTeam]
		AND MC_serverBD.iNumberOfPlayingPlayers[iTeam] > 1
		AND NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
		AND IS_SCREEN_FADED_IN() 
		AND IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		AND g_vehRCCar != GET_VEHICLE_PED_IS_IN(localPlayerPed)
			PRINTLN("[LM][PROCESS_BOUNDS_TAG_OUT][TAG_TEAM_TURNS] Setting - PBBOOL4_REQUEST_TAG_OUT")
			SET_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_REQUEST_TAG_OUT)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_BOUNDS_VEHICLE_REGEN(INT iTeam, INT iRule, BOOL bValidBounds1, BOOL bValidBounds2, BOOL bOnWrongSideOfBounds1, BOOL bOnWrongSideOfBounds2, BOOL bInBounds1, BOOL bInBounds2)
	PRINTLN("[LM][PROCESS_BOUNDS_VEHICLE_REGEN] Bool checks - bValidBounds1: ", bValidBounds1, " bValidBounds2: ", bValidBounds2, " bOnWrongSideOfBounds1: ", bOnWrongSideOfBounds1, " bOnWrongSideOfBounds2: ", bOnWrongSideOfBounds2, " bInBounds1: ", bInBounds1, " bInBounds2: ", bInBounds2)
	
	IF bOnWrongSideOfBounds1
	AND bOnWrongSideOfBounds2
		PRINTLN("[LM][PROCESS_BOUNDS_VEHICLE_REGEN] We are not inside a bounds, or in a Leave Area.")
	ENDIF
	
	BOOL bShouldRegen	
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iVehicleRegenAndCap > 0
		IF bValidBounds1
		AND bInBounds1
			iRegenCapFromBounds = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iVehicleRegenAndCap
			bShouldRegen = TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].iVehicleRegenAndCap > 0
		IF bValidBounds2
		AND bInBounds2
			iRegenCapFromBounds = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].iVehicleRegenAndCap
			bShouldRegen = TRUE
		ENDIF
	ENDIF
	
	IF bShouldRegen
	AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TAG_TEAM(g_FMMC_STRUCT.iAdversaryModeType) // special request..
		PROCESS_VEH_HEALTH_REGEN_FOR_BOUNDS()
	ENDIF
ENDPROC

FUNC BOOL SHOULD_PLAY_OUT_OF_BOUNDS_SOUND(BOOL b1Valid, 			BOOL b2Valid, 
										  BOOL b1InsideLeaveArea, 	BOOL b2InsideLeaveArea)
	
	INT iBoundsPartToUse = iPartToUse
	PLAYER_INDEX BoundsPlayerToUse = PlayerToUse
	PED_INDEX BoundsPlayerPedToUse = PlayerPedToUse
	IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
	AND iSpectatorTarget != -1
		iBoundsPartToUse = iSpectatorTarget
		BoundsPlayerToUse = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iBoundsPartToUse))
		BoundsPlayerPedToUse = GET_PLAYER_PED(BoundsPlayerToUse)
	ENDIF
	
	INT iTeam = MC_playerBD[ iBoundsPartToUse ].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[ iTeam ]
	
	BOOL bCountBounds1Valid, bCountBounds2Valid
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARZONE(g_FMMC_STRUCT.iAdversaryModeType)
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
		bCountBounds1Valid = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLeaveAreaBitset, iRule) AND b1InsideLeaveArea OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPlayAreaBitset, iRule) AND NOT b1InsideLeaveArea
		bCountBounds2Valid = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLeaveArea2Bitset, iRule) AND b2InsideLeaveArea OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPlayArea2Bitset, iRule) AND NOT b2InsideLeaveArea
	ELSE
		//Fix for url:bugstar:3533430, changes to the above will cause issues the two game modes, this is what the above was before hand. 
		//Above needs both a play and leave area to work with the ternary bools below 
		bCountBounds1Valid = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLeaveAreaBitset, iRule)
		bCountBounds2Valid = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLeaveArea2Bitset, iRule)
	ENDIF
	
	IF IS_PED_INJURED(BoundsPlayerPedToUse)
		RETURN FALSE
	ENDIF
	
	//TRUE if both bounds are valid
	IF b1Valid AND b2Valid
		//Only count the proper value of the bool if the bounds is valid for leaving
		IF TERNARY_BOOL( bCountBounds1Valid, b1InsideLeaveArea, TRUE )
		AND TERNARY_BOOL( bCountBounds2Valid, b2InsideLeaveArea, TRUE )			 
				RETURN TRUE
			ENDIF
	
	//FALSE if neither bound is valid
	ELIF NOT b1Valid AND NOT b2Valid
		RETURN FALSE
	
	//TRUE if only bounds 1 is valid and inside/outside of it
	ELIF b1Valid AND NOT b2Valid
		IF b1InsideLeaveArea
			RETURN TRUE
		ENDIF
	
	//TRUE if only bounds 2 is valid and inside/outside of it
	ELIF NOT b1Valid AND b2Valid
		IF b2InsideLeaveArea
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PLAY_EXPLODE_ON_BOUNDS_FAIL_TIMER(INT iTeam, INT iRule, BOOl bSecondary=FALSE, BOOL bEither=FALSE)
	IF iSpectatorTarget = -1
	AND SHOULD_RULE_EXPLODE_ON_FAIL(iTeam, iRule, bSecondary, bEither)
	AND IS_SOUND_ID_VALID(iBoundsExplodeSound)
	AND HAS_SOUND_FINISHED(iBoundsExplodeSound)
		
		//url:bugstar:5028269 url:bugstar:5001312 url:bugstar:5018095
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_SUMO_RUN(g_FMMC_STRUCT.iAdversaryModeType)
			PRINTLN("[PROCESS_MISSION_BOUNDS][RCC MISSION] PLAY_EXPLODE_ON_BOUNDS_FAIL_TIMER - Can't play Explosion_Timer because we are in Sumo Run")
			EXIT
		ENDIF
		
		BOOL bNetworked = TRUE
		BOOL bShouldPlay = TRUE
		
		ENTITY_INDEX entSource
		
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			entSource = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
			
			IF GET_VEHICLE_NUMBER_OF_PASSENGERS(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)) > 0			
				bNetworked = FALSE
			ENDIF
		ELSE
			entSource = LocalPlayerPed
		ENDIF
		
		STRING sSoundSet = "DLC_Lowrider_Relay_Race_Sounds"
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
			sSoundSet = "DLC_SR_TR_General_Sounds"
		ENDIF
				
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
			bNetworked = FALSE
			
			INT ms = ( GET_BOUNDS_TIME_LIMIT(iTeam,iRule) - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_playerBD[iPartToUse].tdBoundstimer ) )
			
			PRINTLN("[PROCESS_MISSION_BOUNDS][RCC MISSION] PROCESS_MISSION_BOUNDS - ciBS_RULE4_EXPLODE_ON_BOUNDS_FAIL - ms: ", ms)
			
			IF ms > 15000
				bShouldPlay = FALSE									
				PRINTLN("[PROCESS_MISSION_BOUNDS][RCC MISSION] PROCESS_MISSION_BOUNDS - ciBS_RULE4_EXPLODE_ON_BOUNDS_FAIL - should not play the sound yet...")
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Enable_Turn_Rotor_Tag_Team)
			bNetworked = FALSE
		ENDIF
				
		IF bShouldPlay
			PLAY_SOUND_FROM_ENTITY(iBoundsExplodeSound, "Explosion_Timer", entSource, sSoundSet, bNetworked, 20)
			SET_VARIABLE_ON_SOUND(iBoundsExplodeSound, "Time", (GET_BOUNDS_TIME_LIMIT(iTeam, iRule) / 1000.0))
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_OVERTIME_AND_NOT_VALID_FOR_OO_BOUNDS()
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
		IF IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_TEAM_0_SHOULD_RESPAWN_BACK_AT_START_NOW + iLocalPart)
		OR IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_PlayerBD[iLocalPart].iTeam)
		OR (iOTPartToMove != iLocalPart AND IS_THIS_CURRENTLY_OVERTIME_TURNS()) // If it's -1 then that means it's no ones turn, and this will return true, as designed.
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_END_OF_RULE_BOUNDS_CHECK(INT iTeam, INT iRule, BOOL bSecondary=FALSE, BOOL bEither=FALSE)
	//Getting the current timer
	SCRIPT_TIMER stTimer = MC_serverBD_3.tdObjectiveLimitTimer[iTeam]
	IF IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH)
		//multirule timer
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciMULTIRULE_TIMER_SUDDEN_DEATH)
		OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_SUMO_RUN(g_FMMC_STRUCT.iAdversaryModeType)
			stTimer = MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeam]
			PRINTLN("[PROCESS_END_OF_RULE_BOUNDS_CHECK] Using multi limit timer")
		ENDIF
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(stTimer)
		INT iLimitRule = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveTimeLimitRule[iRule]
		INT iRemainingTime
		BOOL bMultiRule = FALSE
		IF IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH)
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciMULTIRULE_TIMER_SUDDEN_DEATH)
			OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_SUMO_RUN(g_FMMC_STRUCT.iAdversaryModeType)
				iLimitRule = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveSuddenDeathTimeLimit[iRule]
				bMultiRule = TRUE
			ENDIF
			//In sudden Death there is no problem of checking this every frame.
			iRemainingTime = GET_REMAINING_TIME_ON_RULE_SUDDENDEATH(iTeam,iRule,bMultiRule)
		ELSE
			iRemainingTime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stTimer)
		ENDIF
		INT iTimeFromSelection = GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(iLimitRule)
		
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
			CLEAR_BIT(iLocalBoolCheck25, LBOOL25_SUMO_RUN_TIMER_DELAY)
			PRINTLN("[PROCESS_END_OF_RULE_BOUNDS_CHECK] CLEAR_BIT LBOOL25_SUMO_RUN_TIMER_DELAY")
		ENDIF
		
		//This is to add a delay to the check, as previosuly MC_serverBD_3.tdObjectiveLimitTimer was becoming out of sync with the current rule and not resetting.
		IF (iRemainingTime < iTimeFromSelection/2) 
		AND NOT IS_BIT_SET(iLocalBoolCheck25, LBOOL25_SUMO_RUN_TIMER_DELAY)	
			SET_BIT(iLocalBoolCheck25, LBOOL25_SUMO_RUN_TIMER_DELAY)	
			PRINTLN("[PROCESS_END_OF_RULE_BOUNDS_CHECK] SET_BIT LBOOL25_SUMO_RUN_TIMER_DELAY")
		ENDIF 
		PRINTLN("[PROCESS_END_OF_RULE_BOUNDS_CHECK] iRemainingTime ", iRemainingTime, " iTimeFromSelection: ",iTimeFromSelection)
		IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_SUMO_RUN_TIMER_DELAY)
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[iRule], ciBS_RULE11_EXPLODE_ON_RULE_END_BOUNDS_FAIL) 
			IF (iRemainingTime >= iTimeFromSelection)
			AND NOT(IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH) AND IS_BIT_SET(iLocalBoolCheck28,LBBOOL28_HAS_DONE_SUDDEN_DEATH_BOUNDS_CHECK))
				IF iSpectatorTarget = -1
				AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
				AND NOT IS_OVERTIME_AND_NOT_VALID_FOR_OO_BOUNDS()
					IF NOT IS_PED_INJURED(LocalPlayerPed)
					AND NOT IS_PLAYER_RESPAWNING(PLAYER_ID())
						PRINTLN("[PROCESS_END_OF_RULE_BOUNDS_CHECK][RH] Exploding on rule end due to rule: ", iRule)
						EXPLODE_LOCAL_PLAYER_DUE_TO_BOUNDS_FAIL(iTeam, iRule, DEFAULT, bSecondary, bEither)
					ENDIF
				ENDIF
			ENDIF
		ENDIF 
	ENDIF
ENDPROC 

TEXT_LABEL_63 tlZoneName =""

PROC HANDLE_BOUNDS_TIMERS_AND_TEXT(BOOL bValidBounds1, BOOL bInBounds1, BOOL bValidBounds2, BOOL bInBounds2, BOOL bProcessMapBounds, BOOL bInMapBounds)
	
	BOOL bOnWrongSideOfBounds1, bOnWrongSideOfBounds2, bInPlayBounds1, bInPlayBounds2, bTimerDisplayed
	INT iGodTextForBounds = 0 //Can be 1 or 2

	INT iBoundsPartToUse = iPartToUse
	PLAYER_INDEX BoundsPlayerToUse = PlayerToUse
	IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
	AND iSpectatorTarget != -1
		iBoundsPartToUse = iSpectatorTarget
		BoundsPlayerToUse = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iBoundsPartToUse))
	ENDIF
	
	INT iTeam = MC_playerBD[ iBoundsPartToUse ].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[ iTeam ]

	VECTOR vBoundsCenter
	HUD_COLOURS eHudColour
	
	// If we are a spectator but we do not have a target yet, then EXIT
	// Fixes Late joining players who choose to spectate: url:bugstar:2954131
	IF IS_LOCAL_PLAYER_ANY_SPECTATOR() AND iSpectatorTarget = -1
		PRINTLN("[PROCESS_MISSION_BOUNDS][LM][SHARD CHECK] - we are a spectator with no iSpectatorTarget.. EXIT")
		EXIT
	ENDIF	
	
	TEXT_LABEL_63 tlTempTextLabel // This text label is to allow text for entity-centered mission bounds to stick around in memory while this function runs
	
	BOOL bProcessCreatorBounds
	
	//4001751 // For the purposes of Air Quota we shouldn't worry about the map bounds
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRQUOTA(g_FMMC_STRUCT.iAdversaryModeType)
		bProcessMapBounds = FALSE
		PRINTLN("[PROCESS_MISSION_BOUNDS] Not processing map bounds because this is Air Quota")
	ENDIF
		
	IF bProcessMapBounds
		
		//These checks will reset the relevant timers on the start of a new rule. These are being done just incase the mode manages to carry over timers from a differnet rule 
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_SUMO_RUN(g_FMMC_STRUCT.iAdversaryModeType)
			IF HAS_NET_TIMER_STARTED(MC_playerBD[iLocalPart].tdBoundstimer) AND IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
				RESET_NET_TIMER(MC_playerBD[iLocalPart].tdBoundstimer)
				PRINTLN("[HANDLE_BOUNDS_TIMERS_AND_TEXT] - RESET_NET_TIMER tdBoundstimer")
			ENDIF 
			IF HAS_NET_TIMER_STARTED(MC_playerBD[iLocalPart].tdBounds2timer) AND IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
				RESET_NET_TIMER(MC_playerBD[iLocalPart].tdBounds2timer)
				PRINTLN("[HANDLE_BOUNDS_TIMERS_AND_TEXT] - RESET_NET_TIMER tdBounds2timer")
			ENDIF 
			IF HAS_NET_TIMER_STARTED(MC_playerBD[iLocalPart].tdMapBoundsTimer) AND IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
				RESET_NET_TIMER(MC_playerBD[iLocalPart].tdMapBoundsTimer)
				PRINTLN("[HANDLE_BOUNDS_TIMERS_AND_TEXT] - RESET_NET_TIMER tdMapBoundsTimer")
			ENDIF 
		ENDIF
	
		IF NOT bInMapBounds
			
			IF DOES_BLIP_EXIST(bounds_blip)
				PRINTLN("[BLIP][RH] Removing blip 2")
				REMOVE_BLIP(bounds_blip)
				tlZoneName = ""
			ENDIF
			IF DOES_BLIP_EXIST(sec_bounds_blip)
				REMOVE_BLIP(sec_bounds_blip)
				tlZoneName = ""
			ENDIF
			IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_LEAVE_AREA)
				CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_LEAVE_AREA)
			ENDIF
			IF NOT HAS_NET_TIMER_STARTED(MC_playerBD[iBoundsPartToUse].tdMapBoundsTimer)
				IF iSpectatorTarget = -1
					START_NET_TIMER(MC_playerBD[iBoundsPartToUse].tdMapBoundsTimer)
				ENDIF
			ELSE
				BOOL bFailForMap = FALSE
				IF IS_THIS_ROCKSTAR_MISSION_WVM_APC(g_FMMC_STRUCT.iRootContentIDHash)
					IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
						VEHICLE_INDEX viTemp = GET_VEHICLE_PED_IS_IN(LocalPlayerPed, TRUE)
						IF GET_ENTITY_MODEL(viTemp) = APC
							PRINTLN("[JS] HANDLE_BOUNDS_TIMERS_AND_TEXT - APC taken out of bounds, failing")
							bFailForMap = TRUE
						ENDIF
					ENDIF
				ENDIF
			
				IF (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_playerBD[iBoundsPartToUse].tdMapBoundsTimer) > OUT_OF_MAP_BOUNDS_TIMER_LIMIT)
				OR bFailForMap
					IF NOT IS_BIT_SET(MC_playerBD[iBoundsPartToUse].iClientBitSet,PBBOOL_PLAYER_FAIL)
						IF iSpectatorTarget = -1
						AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
						AND NOT IS_OVERTIME_AND_NOT_VALID_FOR_OO_BOUNDS()
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_DONT_FAIL_MISSION_MAP_BOUNDS)
							OR bFailForMap
								CPRINTLN(DEBUG_MISSION,"[RCC MISSION] OUT OF MAP BOUNDS - mission fail")
								SET_BIT(MC_playerBD[iPartToUse].iClientBitSet2, PBBOOL2_PLAYER_OUT_BOUNDS_FAIL)								
								SET_LOCAL_PLAYER_FAILED(1)
								MC_playerBD[iPartToUse].iReasonForPlayerFail = PLAYER_FAIL_REASON_BOUNDS
							ENDIF
							
							IF SHOULD_RULE_EXPLODE_ON_FAIL(iTeam, iRule)//			NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iRule], ciBS_RULE4_EXPLODE_ON_BOUNDS_FAIL)
								IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_DONT_FAIL_MISSION_MAP_BOUNDS)
									SET_ENTITY_HEALTH(LocalPlayerPed, 0)
								ENDIF
							ENDIF
							
							EXPLODE_LOCAL_PLAYER_DUE_TO_BOUNDS_FAIL(iTeam, iRule, NOT SHOULD_RULE_EXPLODE_ON_FAIL(iTeam, iRule))
						ENDIF
					ENDIF
					IF DOES_BLIP_EXIST(map_bounds_blip)
						REMOVE_BLIP(map_bounds_blip)
					ENDIF
				ELSE
					IF iSpectatorTarget = -1
						IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_OBJECTIVE_BLOCKER)
							CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM0_TARGET)
							CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM1_TARGET)
							CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM2_TARGET)
							CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM3_TARGET)
							CLEANUP_OBJECTIVE_BLIPS()
							CPRINTLN(DEBUG_MISSION,"[PROCESS_MISSION_BOUNDS][RCC MISSION] PROCESS_MISSION_BOUNDS, PBBOOL_OBJECTIVE_BLOCKER - out of map bounds")
							SET_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_OBJECTIVE_BLOCKER)
						ENDIF
					ENDIF
					IF NOT DOES_BLIP_EXIST(map_bounds_blip)
						VECTOR vBlipCoord = << 748.9, 2595.5, 74.3 >>
						BOOL bUseBoundsAsBlipCoord = NOT ARE_VECTORS_ALMOST_EQUAL(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos, <<0,0,0>>)
						
						IF bUseBoundsAsBlipCoord
							vBlipCoord = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos
							PRINTLN("[TMS] Placing 'Return To Los Santos' blip in the middle of the bounds")
						ENDIF
						
						map_bounds_blip = ADD_BLIP_FOR_COORD(vBlipCoord)
					ENDIF
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset2, ciBS2_STOP_GO_HUD)
						IF NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_OO_BOUNDS)
							SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_OO_BOUNDS, "RETURN_LS", tlZoneName, DEFAULT, 999999)
						ENDIF
					ENDIF
					Clear_Any_Objective_Text_From_This_Script()
					IF IS_NET_PLAYER_OK(BoundsPlayerToUse)
						IF NOT IS_SPECTATOR_HUD_HIDDEN()
						AND NOT IS_SPECTATOR_SHOWING_NEWS_HUD()
							// Play area timer
							INT ms = (OUT_OF_MAP_BOUNDS_TIMER_LIMIT - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_playerBD[iBoundsPartToUse].tdMapBoundsTimer))
							IF ms >= 0
								eHudColour = HUD_COLOUR_RED
								IF NOT bTimerDisplayed
									DRAW_GENERIC_TIMER(ms, "LEAVE_LS",0,TIMER_STYLE_DONTUSEMILLISECONDS,-1,PODIUMPOS_NONE,HUDORDER_TOP,FALSE,eHudColour,HUDFLASHING_NONE,0,FALSE,eHudColour)
								ENDIF
								bTimerDisplayed = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(map_bounds_blip)
				REMOVE_BLIP(map_bounds_blip)
			ENDIF
			IF iSpectatorTarget = -1
				IF HAS_NET_TIMER_STARTED(MC_playerBD[iPartToUse].tdMapBoundsTimer)
					RESET_NET_TIMER(MC_playerBD[iPartToUse].tdMapBoundsTimer)
					
					IF NOT HAS_NET_TIMER_STARTED(MC_playerBD[iPartToUse].tdMapBoundsTimer)
						IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
							CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
							PRINTLN("[RCC MISSION] PROCESS_PRE_MAIN_CHECKS - (23) Clearing - PBBOOL_OBJECTIVE_BLOCKER")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			bProcessCreatorBounds = TRUE // If we are not outside of the map bounds, process the creator set checks.
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(map_bounds_blip)
			REMOVE_BLIP(map_bounds_blip)
		ENDIF
		IF iSpectatorTarget = -1
			IF HAS_NET_TIMER_STARTED(MC_playerBD[iPartToUse].tdMapBoundsTimer)
				RESET_NET_TIMER(MC_playerBD[iPartToUse].tdMapBoundsTimer)
				
				IF NOT HAS_NET_TIMER_STARTED(MC_playerBD[iPartToUse].tdMapBoundsTimer)
					IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
						CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
						PRINTLN("[RCC MISSION] PROCESS_PRE_MAIN_CHECKS - (22) Clearing - PBBOOL_OBJECTIVE_BLOCKER")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		bProcessCreatorBounds = TRUE // If we are not processing the map bounds check, process the creator set bounds.
	ENDIF
	
	IF bProcessCreatorBounds
		IF bValidBounds1		
			IF bInBounds1
				PRINTLN("[PROCESS_MISSION_BOUNDS][LM] bInBounds1 = TRUE")
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLeaveAreaBitset, iRule)
					bOnWrongSideOfBounds1 = TRUE
					PRINTLN("[PROCESS_MISSION_BOUNDS][LM] bOnWrongSideOfBounds1 = TRUE (1).")
				ENDIF
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPlayAreaBitset, iRule)
					PRINTLN("[PROCESS_MISSION_BOUNDS][LM] bInPlayBounds1 = TRUE (1).")					
					bInPlayBounds1 = TRUE
					
					IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iMaxPlayersAllowedInBounds > 0
						IF NOT IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_INSIDE_A_PLAY_AREA_BOUND_1)
							SET_BIT(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_INSIDE_A_PLAY_AREA_BOUND_1)
							PRINTLN("[PROCESS_MISSION_BOUNDS][LM][BoundsLimiter] PBBOOL4_INSIDE_A_PLAY_AREA_BOUND_1 SETTING")
						ENDIF
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[PROCESS_MISSION_BOUNDS][LM] bOnWrongSideOfBounds1. bInBounds1 = FALSE")
				IF IS_BIT_SET(MC_playerBD[iBoundsPartToUse].iBoundsBitSet,iRule)
				OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iRule], ciBS_RULE2_PLAY_AREA_TRIGGER_ON_START)
					PRINTLN("[PROCESS_MISSION_BOUNDS][LM] bOnWrongSideOfBounds1. Checking iPlayAreaBitset, iRule")
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPlayAreaBitset, iRule)
						bOnWrongSideOfBounds1 = TRUE
						PRINTLN("[PROCESS_MISSION_BOUNDS][LM] bOnWrongSideOfBounds1 = TRUE (2).")
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_INSIDE_A_PLAY_AREA_BOUND_1)
					PRINTLN("[PROCESS_MISSION_BOUNDS][LM][BoundsLimiter] PBBOOL4_INSIDE_A_PLAY_AREA_BOUND_1 CLEARING")
					CLEAR_BIT(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_INSIDE_A_PLAY_AREA_BOUND_1)
				ENDIF
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(bounds_blip)
				PRINTLN("[BLIP][RH] Removing blip 3")
				REMOVE_BLIP(bounds_blip)
				tlZoneName = ""
			ENDIF
			
			IF HAS_NET_TIMER_STARTED(MC_playerBD[iPartToUse].tdBoundstimer)
				IF CAN_BOUNDS_TIMER_RESET_YET(FALSE)
					RESET_NET_TIMER(td_BoundsTimerResetDelay1)
					IF iSpectatorTarget = -1
						RESET_NET_TIMER(MC_playerBD[iPartToUse].tdBoundstimer)
						PRINTLN("[PROCESS_MISSION_BOUNDS][JS] HANDLE_BOUNDS_TIMERS_AND_TEXT RESET_NET_TIMER tdBoundstimer")
					ENDIF
					IF NOT HAS_NET_TIMER_STARTED(MC_playerBD[iPartToUse].tdBounds2timer)
					OR NOT bValidBounds2
						PRINTLN("[PROCESS_MISSION_BOUNDS][JS] HANDLE_BOUNDS_TIMERS_AND_TEXT - Bounds no longer valid, clearing blip and timer")
						STOP_SOUND(iBoundsExplodeSound)
						IF iSpectatorTarget = -1
							IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
								CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
								PRINTLN("[RCC MISSION] PROCESS_PRE_MAIN_CHECKS - (21) Clearing - PBBOOL_OBJECTIVE_BLOCKER")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF bValidBounds2
			IF bInBounds2
				PRINTLN("[PROCESS_MISSION_BOUNDS][LM] bInBounds2 = TRUE")
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLeaveArea2Bitset, iRule)
					bOnWrongSideOfBounds2 = TRUE
					PRINTLN("[PROCESS_MISSION_BOUNDS][LM] bOnWrongSideOfBounds2 = TRUE (1).")
				ENDIF
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPlayArea2Bitset, iRule)
					PRINTLN("[PROCESS_MISSION_BOUNDS][LM] bInPlayBounds2 = TRUE (1).")
					bInPlayBounds2 = TRUE
										
					IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].iMaxPlayersAllowedInBounds > 0
						IF NOT IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_INSIDE_A_PLAY_AREA_BOUND_2)
							SET_BIT(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_INSIDE_A_PLAY_AREA_BOUND_2)
							PRINTLN("[PROCESS_MISSION_BOUNDS][LM][BoundsLimiter] PBBOOL4_INSIDE_A_PLAY_AREA_BOUND_2 SETTING")
						ENDIF
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[PROCESS_MISSION_BOUNDS][LM] bOnWrongSideOfBounds2. bInBounds2 = FALSE")
				IF IS_BIT_SET(MC_playerBD[iBoundsPartToUse].iBounds2BitSet,iRule)
				OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iRule], ciBS_RULE3_PLAY_AREA_2_TRIGGER_ON_START)
					PRINTLN("[PROCESS_MISSION_BOUNDS][LM] bOnWrongSideOfBounds2. checking iPlayArea2Bitset, iRule")
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPlayArea2Bitset, iRule)
						bOnWrongSideOfBounds2 = TRUE
						PRINTLN("[PROCESS_MISSION_BOUNDS][LM] bOnWrongSideOfBounds2 = TRUE (2).")
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_INSIDE_A_PLAY_AREA_BOUND_2)
					PRINTLN("[PROCESS_MISSION_BOUNDS][LM][BoundsLimiter] PBBOOL4_INSIDE_A_PLAY_AREA_BOUND_2 CLEARING")
					CLEAR_BIT(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_INSIDE_A_PLAY_AREA_BOUND_2)
				ENDIF
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(sec_bounds_blip)
				REMOVE_BLIP(sec_bounds_blip)
				tlZoneName = ""
			ENDIF
			
			IF HAS_NET_TIMER_STARTED(MC_playerBD[iPartToUse].tdBounds2timer)
				IF CAN_BOUNDS_TIMER_RESET_YET(TRUE)
					RESET_NET_TIMER(td_BoundsTimerResetDelay2)
					IF iSpectatorTarget = -1
						RESET_NET_TIMER(MC_playerBD[iPartToUse].tdBounds2timer)
					ENDIF
					IF NOT HAS_NET_TIMER_STARTED(MC_playerBD[iPartToUse].tdBoundstimer)
					OR NOT bValidBounds1
						PRINTLN("[PROCESS_MISSION_BOUNDS][JS] HANDLE_BOUNDS_TIMERS_AND_TEXT - Bounds 2 no longer valid, clearing blip and timer")
						STOP_SOUND(iBoundsExplodeSound)
						IF iSpectatorTarget = -1
							IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
								CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
								PRINTLN("[RCC MISSION] PROCESS_PRE_MAIN_CHECKS - (20) Clearing - PBBOOL_OBJECTIVE_BLOCKER")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
		BOOL bConsiderOOB
		IF MC_serverBD_3.iNumberOfPlayersInBounds1[iTeam] > 0
		AND (IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_INSIDE_A_PLAY_AREA_BOUND_1) AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iMaxPlayersAllowedInBounds > 0)
		AND NOT IS_BIT_SET(MC_serverBD_3.iPlayerBoundAllowedBitset1, iLocalPart)
			bConsiderOOB = TRUE
		ENDIF
		IF MC_serverBD_3.iNumberOfPlayersInBounds2[iTeam] > 0
		AND (IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_INSIDE_A_PLAY_AREA_BOUND_2) AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].iMaxPlayersAllowedInBounds > 0)
		AND NOT IS_BIT_SET(MC_serverBD_3.iPlayerBoundAllowedBitset2, iLocalPart)
			bConsiderOOB = TRUE
		ENDIF
		IF bConsiderOOB
		AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)
		AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_HEIST_SPECTATOR)
		AND NOT IS_PED_INJURED(localPlayerPed)
			IF NOT HAS_NET_TIMER_STARTED(tdTooManyPlayersInBoundsTimer)
				PRINTLN("[LM][PROCESS_MISSION_BOUNDS][BoundsLimiter] - Too many players in bounds, and we are not flagged as allowed!!")
				START_NET_TIMER(tdTooManyPlayersInBoundsTimer)		
			ENDIF
			
			IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdTooManyPlayersInBoundsTimer, ci_START_WARNING_FOR_TOO_MANY_PLAYERS_INBOUNDS)
				PRINTLN("[LM][PROCESS_MISSION_BOUNDS][BoundsLimiter] - Too many players in bounds, start the shard (", ci_START_WARNING_FOR_TOO_MANY_PLAYERS_INBOUNDS, "sec passed)")				
				IF NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GENERIC_MIDSIZED)
					SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_GENERIC_MIDSIZED, "", "", DEFAULT, 999999, "MC_LVE_OBJC")
				ENDIF
				
				INT ms = (ci_EXPLODE_FOR_TOO_MANY_PLAYERS_INBOUNDS - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdTooManyPlayersInBoundsTimer))
				DRAW_GENERIC_TIMER(ms, "MC_LVE_OBJC", 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1, PODIUMPOS_NONE, HUDORDER_TOP, FALSE, eHudColour, HUDFLASHING_FLASHRED, 0, FALSE, eHudColour)
					
				IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdTooManyPlayersInBoundsTimer, ci_EXPLODE_FOR_TOO_MANY_PLAYERS_INBOUNDS)
					PRINTLN("[LM][PROCESS_MISSION_BOUNDS][BoundsLimiter] - Too many players in bounds, Explode (", ci_EXPLODE_FOR_TOO_MANY_PLAYERS_INBOUNDS, " sec passed)")
					EXPLODE_LOCAL_PLAYER_DUE_TO_BOUNDS_FAIL(iTeam, iRule, FALSE, TRUE, TRUE, TRUE)
					RESET_NET_TIMER(tdTooManyPlayersInBoundsTimer)
												
					IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GENERIC_MIDSIZED)
						CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_GENERIC_MIDSIZED)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF HAS_NET_TIMER_STARTED(tdTooManyPlayersInBoundsTimer)
				PRINTLN("[LM][PROCESS_MISSION_BOUNDS][BoundsLimiter] - tdTooManyPlayersInBoundsTimer Reset Net Timer")
				RESET_NET_TIMER(tdTooManyPlayersInBoundsTimer)
				IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GENERIC_MIDSIZED)
					CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_GENERIC_MIDSIZED)
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
			IF NOT bOnWrongSideOfBounds1 AND bOnWrongSideOfBounds2
				IF iSpectatorTarget = -1
				AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
				AND NOT IS_OVERTIME_AND_NOT_VALID_FOR_OO_BOUNDS()
					IF NOT IS_PED_INJURED(LocalPlayerPed)
					AND NOT IS_PLAYER_RESPAWNING(PLAYER_ID())
						EXPLODE_LOCAL_PLAYER_DUE_TO_BOUNDS_FAIL(iTeam, iRule, TRUE, TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF ((bInPlayBounds1 OR bInPlayBounds2) AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciSHOW_BOUNDS_TIMER_IN_SECONDARY_BOUNDS))
		OR ((bInPlayBounds1 AND NOT bInPlayBounds2) AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciSHOW_BOUNDS_TIMER_IN_SECONDARY_BOUNDS))
			// url:bugstar:4725552
			PRINTLN("[PROCESS_MISSION_BOUNDS][LM] Setting bOnWrongSideOfBounds1 to false and bOnWrongSideOfBounds2 to false. MAY WANT TO ENABLE ciSHOW_BOUNDS_TIMER_IN_SECONDARY_BOUNDS")
			bOnWrongSideOfBounds1 = FALSE
			bOnWrongSideOfBounds2 = FALSE
			tlZoneName = ""
			iGodTextForBounds = 0
			
			IF IS_SOUND_ID_VALID(iBoundsTimerSound)
			AND NOT HAS_SOUND_FINISHED(iBoundsTimerSound)
				STOP_SOUND(iBoundsTimerSound)
				PRINTLN("[PROCESS_MISSION_BOUNDS][JJT] In bounds, stopping bounds timer sound.")
			ENDIF
			
			IF IS_SOUND_ID_VALID(iBoundsExplodeSound)
			AND NOT HAS_SOUND_FINISHED(iBoundsExplodeSound)
				STOP_SOUND(iBoundsExplodeSound)
				PRINTLN("[PROCESS_MISSION_BOUNDS][JJT] In bounds, stopping bounds timer sound.")
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN)
		OR (IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN) AND ARE_WE_CURRENTLY_IN_SUDDEN_DEATH() AND NOT IS_BIT_SET(iLocalBoolCheck28,LBBOOL28_HAS_DONE_SUDDEN_DEATH_BOUNDS_CHECK))
		OR (IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN) AND NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH())
		OR (ARE_WE_CURRENTLY_IN_SUDDEN_DEATH() AND IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN) AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_BOUNDS_ONE_SUDDEN_DEATH))
			IF bOnWrongSideOfBounds1
				PROCESS_END_OF_RULE_BOUNDS_CHECK(iTeam, iRule)
				IF NOT HAS_NET_TIMER_STARTED(MC_playerBD[iBoundsPartToUse].tdBoundstimer) AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
					IF iSpectatorTarget = -1
						START_NET_TIMER(MC_playerBD[iLocalPart].tdBoundstimer)
						CPRINTLN(DEBUG_MISSION,"[PROCESS_MISSION_BOUNDS][RCC MISSION] PLAYER OUTSIDE BOUNDS STARTING TIMER FOR OBJECTIVE: ",iRule)
					ENDIF
					
					IF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_OOB_ON_FOOT_EXPLODE_DONE)
						CLEAR_BIT(iLocalBoolCheck26, LBOOL26_OOB_ON_FOOT_EXPLODE_DONE)
					ENDIF
				ELSE
					// Play area timer
				
					RESET_NET_TIMER(td_BoundsTimerResetDelay1)
					
					IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_playerBD[iBoundsPartToUse].tdBoundstimer) > GET_BOUNDS_TIME_LIMIT(iTeam,iRule) AND (GET_BOUNDS_TIME_LIMIT(iTeam,iRule) > -1)
					
						CLEAR_ENTITY_LAST_DAMAGE_ENTITY( PLAYER_PED_ID() )
						
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iRule], ciBS_RULE4_KILL_PLAYER_ON_BOUNDS_FAIL)
							PRINTLN("[PROCESS_MISSION_BOUNDS][LM] Timer Expired and bOnWrongSideOfBounds1 = TRUE - ciBS_RULE4_KILL_PLAYER_ON_BOUNDS_FAIL Not Set")
							IF NOT IS_BIT_SET(MC_playerBD[iBoundsPartToUse].iClientBitSet,PBBOOL_PLAYER_FAIL)
								IF iSpectatorTarget = -1
								AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
									IF bInBounds1
										CPRINTLN(DEBUG_MISSION,"[PROCESS_MISSION_BOUNDS][RCC MISSION] INSIDE BOUNDS 1 - mission fail")
										SET_BIT(MC_playerBD[iPartToUse].iClientBitSet2, PBBOOL2_PLAYER_LEAVE_AREA_FAIL)
									ELSE
										CPRINTLN(DEBUG_MISSION,"[PROCESS_MISSION_BOUNDS][RCC MISSION] OUT OF BOUNDS 1 - mission fail")							
										SET_BIT(MC_playerBD[iPartToUse].iClientBitSet2, PBBOOL2_PLAYER_OUT_BOUNDS_FAIL)
									ENDIF
									
									//[KH] - Displays out of bounds shard if option selected in creator
									IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciENABLE_SUMO_TICKERS)
										IF NOT IS_BIT_SET(iLocalBoolCheck15, LBOOL15_IS_OUT_OF_BOUNDS_SHARD_DISPLAYED)
											PRINTLN("[PROCESS_MISSION_BOUNDS][KH] | CUSTOM SHARD ON OUT OF BOUNDS | BOUNDS 1 | SHOW OUT OF BOUNDS SHARD")
											BROADCAST_FMMC_OBJECTIVE_PLAYER_KNOCKED_OUT(iTeam)
											SET_BIT(iLocalBoolCheck15, LBOOL15_IS_OUT_OF_BOUNDS_SHARD_DISPLAYED)
										ENDIF
									ELSE
										PRINTLN("[PROCESS_MISSION_BOUNDS][AW] | CUSTOM SHARD ON OUT OF BOUNDS | BOUNDS 1 | ciENABLE_SUMO_TICKERS - off")
									ENDIF
									
									SET_LOCAL_PLAYER_FAILED(2)
									MC_playerBD[iPartToUse].iReasonForPlayerFail = PLAYER_FAIL_REASON_BOUNDS
									EXPLODE_LOCAL_PLAYER_DUE_TO_BOUNDS_FAIL(iTeam, iRule, TRUE)
								ENDIF
								IF DOES_BLIP_EXIST(bounds_blip)
									PRINTLN("[BLIP][RH] Removing blip 4")
									REMOVE_BLIP(bounds_blip)
								ENDIF
								IF DOES_BLIP_EXIST(sec_bounds_blip)
									REMOVE_BLIP(sec_bounds_blip)
								ENDIF
							ENDIF
						ELSE						
							PRINTLN("[PROCESS_MISSION_BOUNDS][LM] Timer Expired and bOnWrongSideOfBounds1 = TRUE - ciBS_RULE4_KILL_PLAYER_ON_BOUNDS_FAIL Set")
							IF iSpectatorTarget = -1
							AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
							AND NOT IS_OVERTIME_AND_NOT_VALID_FOR_OO_BOUNDS()
								IF NOT IS_PED_INJURED(LocalPlayerPed)
								AND NOT IS_PLAYER_RESPAWNING(PLAYER_ID())
									IF SHOULD_RULE_EXPLODE_ON_FAIL(iTeam, iRule) //IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iRule], ciBS_RULE4_EXPLODE_ON_BOUNDS_FAIL)
									 
										PRINTLN("[PROCESS_MISSION_BOUNDS][RH] Exploding due to rule number: ", iRule)
										IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_SUMO_RUN(g_FMMC_STRUCT.iAdversaryModeType)   
											IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH)
												EXPLODE_LOCAL_PLAYER_DUE_TO_BOUNDS_FAIL(iTeam, iRule)
											ENDIF
										ELSE
											EXPLODE_LOCAL_PLAYER_DUE_TO_BOUNDS_FAIL(iTeam, iRule)
										ENDIF
									ELSE
										IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
											IF g_FMMC_STRUCT.iShowPackageHudForTeam != 0
											OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciNEGATIVE_SUICIDE_SCORE)
												//Negative score
												IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciNEGATIVE_SUICIDE_SCORE)
												AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciNEGATIVE_SUICIDE_FALL_IS_SUICIDE)
													IF MC_playerBD[iPartToUse].iNumPlayerKills > 0
														MC_playerBD[iPartToUse].iNumPlayerKills--
														PRINTLN("[PROCESS_MISSION_BOUNDS][JS] - SUICIDE POINTS - decrementing local player's kills")
													ENDIF
												
													IF MC_serverBD.iNumPlayerRuleHighestPriority[MC_playerBD[iPartToUse].iteam] > 0 
														PRINTLN("[PROCESS_MISSION_BOUNDS][RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - more than 0 highest ") 
														IF MC_serverBD_4.iPlayerRuleMissionLogic[MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL_PLAYERS // this high priority kill player objective
															IF MC_playerBD[iPartToUse].iPlayerScore > 0
																INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(-1)
															ENDIF
														ELIF MC_serverBD_4.iPlayerRuleMissionLogic[MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL_TEAM0	
														OR MC_serverBD_4.iPlayerRuleMissionLogic[MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL_TEAM1	
														OR MC_serverBD_4.iPlayerRuleMissionLogic[MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL_TEAM2
														OR MC_serverBD_4.iPlayerRuleMissionLogic[MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL_TEAM3
															IF MC_playerBD[iPartToUse].iPlayerScore > 0
															AND MC_Playerbd[iPartToUse].iKillScore > 0
																PRINTLN("[PROCESS_MISSION_BOUNDS][JS] - SUICIDE POINTS - decrementing local player's score")
																INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(-1,TRUE)
															ENDIF
														ENDIF	
													ELSE
														IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iPointsPerKill > 0
															IF MC_playerBD[iPartToUse].iPlayerScore >= g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iPointsPerKill
																PRINTLN("[PROCESS_MISSION_BOUNDS][JS] - SUICIDE POINTS - iPoints per kill = ", g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iPointsPerKill)
																INCREMENT_LOCAL_PLAYER_SCORE_BY((g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iPointsPerKill * -1),TRUE)
															ENDIF
														ENDIF
													ENDIF
												ENDIF

											ENDIF
											
											IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VENETIAN_JOB(g_FMMC_STRUCT.iAdversaryModeType)
												IF g_FMMC_STRUCT.fManualRespawnTimeLose[MC_playerBD[iLocalPart].iteam] != 0
												AND IS_THERE_ENOUGH_TIME_LEFT_TO_DECREMENT_REMAINING_TIME(iRule, iTeam)
													BROADCAST_FMMC_DECREMENT_REMAINING_TIME(MC_playerBD[iLocalPart].iteam, ROUND(g_FMMC_STRUCT.fManualRespawnTimeLose[MC_playerBD[iLocalPart].iteam] * 1000))
												ENDIF
											ENDIF
											
											BROADCAST_FMMC_PLAYER_COMMITED_SUICIDE(iTeam)
											SET_ENTITY_HEALTH(LocalPlayerPed, 0)
										ENDIF
									ENDIF
								ENDIF
								
								//[KH] - Displays out of bounds shard if option selected in creator
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciENABLE_SUMO_TICKERS)
									IF NOT IS_BIT_SET(iLocalBoolCheck15, LBOOL15_IS_OUT_OF_BOUNDS_SHARD_DISPLAYED)
										PRINTLN("[PROCESS_MISSION_BOUNDS][KH] | CUSTOM SHARD ON OUT OF BOUNDS | BOUNDS 1 | SHOW OUT OF BOUNDS SHARD")
										BROADCAST_FMMC_OBJECTIVE_PLAYER_KNOCKED_OUT(iTeam)
										SET_BIT(iLocalBoolCheck15, LBOOL15_IS_OUT_OF_BOUNDS_SHARD_DISPLAYED)
									ENDIF
								ELSE
									PRINTLN("[PROCESS_MISSION_BOUNDS][AW] | CUSTOM SHARD ON OUT OF BOUNDS | BOUNDS 1 | ciENABLE_SUMO_TICKERS - off")
								ENDIF
							ENDIF
						ENDIF
					ELSE
						//Blocks the map area text when there are more than 6 players in the mode and the player is out of bounds
						IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_GRASS_GREENER(g_FMMC_STRUCT.iAdversaryModeType)
						OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
							IF SHOULD_BLOCK_MAP_AREA_TEXT_WHEN_USING_TIME_BARS()
								PRINTLN("[PROCESS_MISSION_BOUNDS][KH] HANDLE_BOUNDS_TIMERS_AND_TEXT - Blocking map area text as bounds timer is showing")
								HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
								HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
								HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
							ENDIF
						ENDIF
					
						IF iSpectatorTarget = -1
							IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_OBJECTIVE_BLOCKER)
								CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM0_TARGET)
								CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM1_TARGET)
								CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM2_TARGET)
								CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM3_TARGET)
								CLEANUP_OBJECTIVE_BLIPS()
								CPRINTLN(DEBUG_MISSION,"[PROCESS_MISSION_BOUNDS][RCC MISSION] PROCESS_MISSION_BOUNDS 1, PBBOOL_OBJECTIVE_BLOCKER")
								SET_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_OBJECTIVE_BLOCKER)
							ENDIF
						ENDIF

						// Create the blip that gets drawn in the middle of the bounds if we leave them
						IF NOT DOES_BLIP_EXIST( bounds_blip )
						AND NOT IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitset[ iRule ], ciBS_RULE_HIDE_BLIP_ON_BOUNDS_FAIL )
						AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
						AND NOT (IS_THIS_ROCKSTAR_MISSION_NEW_VS_SUMO_RUN(g_FMMC_STRUCT.iAdversaryModeType) 
								AND IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN) 
								AND IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH))
							
							IF g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct[ iRule ].fRadius > 0 // Spherical bounds
								IF Is_Player_Currently_On_MP_LTS_Mission( LocalPlayer ) 
									//bounds_blip = ADD_BLIP_FOR_RADIUS( g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fRadius )
									//SET_BLIP_COLOUR( bounds_blip, BLIP_COLOUR_RED )
									//SET_BLIP_ALPHA( bounds_blip, 150 )
								ELSE
									VECTOR vBoundsPos = GET_FMMC_AREA_BOUNDS_POS(iTeam, iRule)
									PRINTLN("[PROCESS_MISSION_BOUNDS][RCC MISSION][bounds] Get Bounds Pos A")
									
									IF ARE_VECTORS_EQUAL(vBoundsPos, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos)
									OR (g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iEntityType = ciRULE_TYPE_GOTO)
										bounds_blip = ADD_BLIP_FOR_COORD( vBoundsPos )
										PRINTLN("[BLIP][RH] Creating blip 4")
									ELSE
										IF NOT ADD_FMMC_BOUNDS_BLIP(iTeam, iRule)
											bounds_blip = ADD_BLIP_FOR_COORD(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos)
											PRINTLN("[BLIP][RH] Creating blip 5")
										ENDIF
									ENDIF
									
									HANDLE_BOUNDS_BLIP_HEIGHT_THRESHOLD(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule], bounds_blip)
									HANDLE_BOUNDS_BLIP_COLOUR(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule], bounds_blip)
									IF DOES_BLIP_EXIST(bounds_blip)
										SET_BLIP_PRIORITY(bounds_blip, BLIPPRIORITY_HIGH_HIGHEST)
									ENDIF
								ENDIF
							ELSE
								vBoundsCenter = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct[ iRule ].vPos1 
									+ g_FMMC_STRUCT.sFMMCEndConditions[ iteam ].sBoundsStruct[ iRule ].vPos2
								vBoundsCenter = vBoundsCenter * << 0.5, 0.5, 0.5 >>
								GET_GROUND_Z_FOR_3D_COORD( vBoundsCenter,vBoundsCenter.z)
								bounds_blip = ADD_BLIP_FOR_COORD(vBoundsCenter)
								PRINTLN("[BLIP][RH] Creating blip 6")
								SET_BLIP_PRIORITY(bounds_blip, BLIPPRIORITY_HIGH_HIGHEST)
								IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[iRule], ciBS_RULE11_LONG_RANGE_BLIP)
									SET_BLIP_AS_SHORT_RANGE(bounds_blip,TRUE)
								ENDIF
								
								HANDLE_BOUNDS_BLIP_HEIGHT_THRESHOLD(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule], bounds_blip)
								HANDLE_BOUNDS_BLIP_COLOUR(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule], bounds_blip)
							ENDIF
						ELIF NOT DOES_BLIP_EXIST( bounds_blip )
						AND NOT IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iRule], ciBS_RULE_HIDE_BLIP_ON_BOUNDS_FAIL )
						AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetTwelve[iRule], ciBS_RULE12_BOUNDS_SHOW_SPECTATOR_BLIP_PRIMARY)
						AND NOT (IS_THIS_ROCKSTAR_MISSION_NEW_VS_SUMO_RUN(g_FMMC_STRUCT.iAdversaryModeType) 
								AND IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN) 
								AND IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH))
								
							IF g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct[ iRule ].fRadius > 0
								VECTOR vBoundsPos = GET_FMMC_AREA_BOUNDS_POS(iTeam, iRule)
								IF ARE_VECTORS_EQUAL(vBoundsPos, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos)
								OR (g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iEntityType = ciRULE_TYPE_GOTO)
									bounds_blip = ADD_BLIP_FOR_COORD( vBoundsPos )
									PRINTLN("[BLIP][RH] Creating blip 7")
								ELSE
									PRINTLN("[BLIP][RH] ARE_VECTORS_EQUAL(vBoundsPos, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos) ", BOOL_TO_INT(ARE_VECTORS_EQUAL(vBoundsPos, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos)))
									PRINTLN("[BLIP][RH] g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iEntityType = ciRULE_TYPE_GOTO ", BOOL_TO_INT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iEntityType = ciRULE_TYPE_GOTO))
									
									BOOL bShouldBlip = ADD_FMMC_BOUNDS_BLIP(iTeam, iRule)
									
									PRINTLN("[BLIP][RH] bShouldBlip ", bShouldBlip)
									IF NOT bShouldBlip
										bounds_blip = ADD_BLIP_FOR_COORD(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos)
										PRINTLN("[BLIP][RH] Creating blip 8")
									ENDIF
								ENDIF
								HANDLE_BOUNDS_BLIP_HEIGHT_THRESHOLD(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule], bounds_blip)
								HANDLE_BOUNDS_BLIP_COLOUR(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule], bounds_blip)
								SET_BLIP_PRIORITY(bounds_blip, BLIPPRIORITY_HIGH_HIGHEST)
							ELSE
								PRINTLN("[BLIP][RH] g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct[ iRule ].fRadius > 0 ", BOOL_TO_INT(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct[ iRule ].fRadius > 0))
								vBoundsCenter = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct[ iRule ].vPos1 
									+ g_FMMC_STRUCT.sFMMCEndConditions[ iteam ].sBoundsStruct[ iRule ].vPos2
								vBoundsCenter = vBoundsCenter * << 0.5, 0.5, 0.5 >>
								GET_GROUND_Z_FOR_3D_COORD( vBoundsCenter,vBoundsCenter.z)
								bounds_blip = ADD_BLIP_FOR_COORD(vBoundsCenter)
								PRINTLN("[BLIP][RH] Creating blip 9")
								SET_BLIP_PRIORITY(bounds_blip, BLIPPRIORITY_HIGH_HIGHEST)
								IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[iRule], ciBS_RULE11_LONG_RANGE_BLIP)
									SET_BLIP_AS_SHORT_RANGE(bounds_blip,TRUE)
								ENDIF
								
								HANDLE_BOUNDS_BLIP_HEIGHT_THRESHOLD(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule], bounds_blip)
								HANDLE_BOUNDS_BLIP_COLOUR(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule], bounds_blip)
							ENDIF
						ENDIF
						
						IF IS_NET_PLAYER_OK(BoundsPlayerToUse)
							PLAY_EXPLODE_ON_BOUNDS_FAIL_TIMER(iTeam, iRule)
							
							IF NOT IS_SPECTATOR_HUD_HIDDEN()
							AND NOT IS_SPECTATOR_SHOWING_NEWS_HUD()
							AND NOT SHOULD_HIDE_THE_HUD_THIS_FRAME()
							AND (NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iRule], ciBS_RULE_HIDE_HUD)	OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iRule], ciBS_RULE2_FORCE_BOUNDS_TIMER))
								// Play area timer
								INT ms = ( GET_BOUNDS_TIME_LIMIT(iTeam,iRule) - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_playerBD[iBoundsPartToUse].tdBoundstimer ) )
								IF ms >= 0
									IF ms < 30000
										eHudColour = HUD_COLOUR_RED
									ELSE
										eHudColour = HUD_COLOUR_WHITE
									ENDIF
									TEXT_LABEL_15 sBoundTimer
									IF AM_I_ON_A_HEIST()
										sBoundTimer = "MC_HBF"
									ELIF Is_Player_Currently_On_MP_LTS_Mission( LocalPlayer ) 
										sBoundTimer = "MC_LTSBF"
									ELIF Is_Player_Currently_On_MP_CTF_Mission( LocalPlayer ) 
										sBoundTimer = "MC_CTFBF"
									ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iRule], ciBS_RULE4_KILL_PLAYER_ON_BOUNDS_FAIL)
										sBoundTimer = "BM_OO_BOUNDS"
									ELSE
										sBoundTimer = "MC_MBF"
									ENDIF
									IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_OUT_OF_RANGE_SHARD)
										sBoundTimer = "BM_OO_RANGE"
									ENDIF
									IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_OT_RAMP_SHARD)
										sBoundTimer = "OT_JUMP_TIMER"
									ENDIF									
									HUDORDER eHudOrder = HUDORDER_TOP
									IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[iRule], ciBS_RULE8_RESPAWN_PLAYERS_ON_KILL)
										eHudOrder = HUDORDER_SECONDBOTTOM
									ENDIF
									RESET_NET_TIMER(td_BoundsTimerResetDelay1)
									IF NOT bTimerDisplayed
										IF GET_BITS_IN_RANGE(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFive[iRule], ciBS_RULE5_DRAW_OUT_OF_BOUNDS_TIMER_LIST_T1, ciBS_RULE5_DRAW_OUT_OF_BOUNDS_TIMER_LIST_T4) = 0
											DRAW_GENERIC_TIMER(ms, sBoundTimer,0,TIMER_STYLE_DONTUSEMILLISECONDS,-1,PODIUMPOS_NONE,eHudOrder,FALSE,eHudColour,HUDFLASHING_NONE,0,FALSE,eHudColour)
										ENDIF
									ENDIF
									bTimerDisplayed = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						INT iZoneHash
						INT iZoneHash2
						
						IF g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct[ iRule ].fRadius > 0
							vBoundsCenter = GET_FMMC_AREA_BOUNDS_POS(iTeam, iRule)
							
							IF ARE_VECTORS_EQUAL(vBoundsCenter, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos)
							OR (g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iEntityType = ciRULE_TYPE_GOTO)
								IF IS_STRING_NULL_OR_EMPTY(tlZoneName)
									GET_STREET_NAME_AT_COORD( vBoundsCenter, iZoneHash, iZoneHash2 )
									
									IF iZoneHash != 0
										tlZoneName = GET_STREET_NAME_FROM_HASH_KEY( iZoneHash )
									ENDIF
								ENDIF
							ELSE
								tlTempTextLabel = GET_FMMC_BOUNDS_ZONE_NAME_TEXT_LABEL(iTeam, iRule)
								
								tlZoneName = TEXT_LABEL_TO_STRING(tlTempTextLabel)
								
								IF IS_STRING_NULL_OR_EMPTY(tlZoneName) // Backup
									GET_STREET_NAME_AT_COORD( vBoundsCenter, iZoneHash, iZoneHash2 )
									
									IF iZoneHash != 0
										tlZoneName = GET_STREET_NAME_FROM_HASH_KEY( iZoneHash )
									ENDIF
								ENDIF
							ENDIF
						ELSE //Angled area mission bounds:
							vBoundsCenter = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct[ iRule ].vPos1 
								+ g_FMMC_STRUCT.sFMMCEndConditions[ iteam ].sBoundsStruct[ iRule ].vPos2
							vBoundsCenter = vBoundsCenter * << 0.5, 0.5, 0.5 >>
							IF IS_STRING_NULL_OR_EMPTY(tlZoneName)
								GET_STREET_NAME_AT_COORD( vBoundsCenter, iZoneHash, iZoneHash2 )
								IF iZoneHash != 0
									tlZoneName = GET_STREET_NAME_FROM_HASH_KEY( iZoneHash )
								ENDIF
							ENDIF
						ENDIF
						
						
						iGodTextForBounds = 1
						
					ENDIF
				ENDIF
						
				IF iSpectatorTarget = -1
				AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
				AND NOT IS_OVERTIME_AND_NOT_VALID_FOR_OO_BOUNDS()
					IF IS_BIT_SET(iLocalBoolCheck23, LBOOL23_OVERTIME_ENTERED_BOUNDS)
						IF bLocalPlayerPedOK
							PRINTLN("[RCC MISSION] Exploding due to iLocalBoolCheck23, LBOOL23_OVERTIME_ENTERED_BOUNDS")
							EXPLODE_LOCAL_PLAYER_DUE_TO_BOUNDS_FAIL(iTeam, iRule, TRUE)
							CLEAR_BIT(iLocalBoolCheck23, LBOOL23_OVERTIME_ENTERED_BOUNDS)
						ENDIF
					ENDIF
				ENDIF
				
			ELIF bValidBounds1 // Right side of bounds 1
				IF DOES_BLIP_EXIST(bounds_blip)
					PRINTLN("[BLIP][RH] Removing blip 5")
					REMOVE_BLIP(bounds_blip)
					tlZoneName = ""
				ENDIF
				
				//[KH] - Clears the big message if the player reenters the bounds
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciMISSION_CUSTOM_SHARD_ON_OUT_OF_BOUNDS)
					IF IS_BIT_SET(iLocalBoolCheck15, LBOOL15_IS_OUT_OF_BOUNDS_SHARD_DISPLAYED)
						CLEAR_ALL_BIG_MESSAGES()
						PRINTLN("[PROCESS_MISSION_BOUNDS][KH] | CUSTOM SHARD ON OUT OF BOUNDS | BOUNDS 1 | CLEAR_ALL_BIG_MESSAGES()")
						CLEAR_BIT(iLocalBoolCheck15, LBOOL15_IS_OUT_OF_BOUNDS_SHARD_DISPLAYED)
					ENDIF
				ENDIF
				
				CLEAR_BIT(iLocalBoolCheck28, LBOOL28_BOUNDS_SHARD_SHOWN)

				IF HAS_NET_TIMER_STARTED(MC_playerBD[iPartToUse].tdBoundstimer)
					IF CAN_BOUNDS_TIMER_RESET_YET(FALSE)
						RESET_NET_TIMER(td_BoundsTimerResetDelay1)
						IF iSpectatorTarget = -1
							RESET_NET_TIMER(MC_playerBD[iPartToUse].tdBoundstimer)
							PRINTLN("[PROCESS_MISSION_BOUNDS] RESET_NET_TIMER tdBoundstimer")
						ENDIF
						IF NOT HAS_NET_TIMER_STARTED(MC_playerBD[iPartToUse].tdBounds2timer)
						OR NOT bValidBounds2
							STOP_SOUND(iBoundsExplodeSound)
							IF iSpectatorTarget = -1
								IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
									CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
									PRINTLN("[RCC MISSION] PROCESS_PRE_MAIN_CHECKS - (19) Clearing - PBBOOL_OBJECTIVE_BLOCKER")
								ENDIF
							ENDIF
							PRINTLN("[PROCESS_MISSION_BOUNDS][KH] | STOP_SOUND | BOUNDS 1")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		
		IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN)
		OR (IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN) AND NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH())
		OR (IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN) AND ARE_WE_CURRENTLY_IN_SUDDEN_DEATH() AND NOT IS_BIT_SET(iLocalBoolCheck28,LBBOOL28_HAS_DONE_SUDDEN_DEATH_BOUNDS_CHECK))
		OR (ARE_WE_CURRENTLY_IN_SUDDEN_DEATH() AND IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN) AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_BOUNDS_TWO_SUDDEN_DEATH))
		
			IF bOnWrongSideOfBounds2
				IF NOT HAS_NET_TIMER_STARTED(MC_playerBD[iBoundsPartToUse].tdBounds2timer)
					IF iSpectatorTarget = -1
						START_NET_TIMER(MC_playerBD[iPartToUse].tdBounds2timer)
						CPRINTLN(DEBUG_MISSION,"[PROCESS_MISSION_BOUNDS][RCC MISSION] PLAYER OUTSIDE BOUNDS 2 STARTING TIMER FOR OBJECTIVE: ",iRule)
					ENDIF
				ELSE
					// Play area timer
					IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_playerBD[iBoundsPartToUse].tdBounds2timer) > GET_BOUNDS_TIME_LIMIT(iTeam,iRule,FALSE)
						CLEAR_ENTITY_LAST_DAMAGE_ENTITY( PLAYER_PED_ID() )
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iRule], ciBS_RULE4_KILL_PLAYER_ON_BOUNDS_FAIL)
							
							PRINTLN("[PROCESS_MISSION_BOUNDS][LM] Timer Expired and bOnWrongSideOfBounds2 = TRUE - ciBS_RULE4_KILL_PLAYER_ON_BOUNDS_FAIL Not Set")
							
							IF NOT IS_BIT_SET(MC_playerBD[iBoundsPartToUse].iClientBitSet,PBBOOL_PLAYER_FAIL)
								IF iSpectatorTarget = -1
								AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
								AND NOT IS_OVERTIME_AND_NOT_VALID_FOR_OO_BOUNDS()
									IF bInBounds1
										CPRINTLN(DEBUG_MISSION,"[PROCESS_MISSION_BOUNDS][RCC MISSION] INSIDE BOUNDS 2 - mission fail")
										SET_BIT(MC_playerBD[iPartToUse].iClientBitSet2, PBBOOL2_PLAYER_LEAVE_AREA_FAIL)
									ELSE
										CPRINTLN(DEBUG_MISSION,"[PROCESS_MISSION_BOUNDS][RCC MISSION] OUT OF BOUNDS 2 - mission fail")							
										SET_BIT(MC_playerBD[iPartToUse].iClientBitSet2, PBBOOL2_PLAYER_OUT_BOUNDS_FAIL)
										
										//[KH] - Displays out of bounds shard if option selected in creator
										IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciMISSION_CUSTOM_SHARD_ON_OUT_OF_BOUNDS)
											IF NOT IS_BIT_SET(iLocalBoolCheck15, LBOOL15_IS_OUT_OF_BOUNDS_SHARD_DISPLAYED)
												PRINTLN("[PROCESS_MISSION_BOUNDS][KH] | CUSTOM SHARD ON OUT OF BOUNDS | BOUNDS 2 | SHOW OUT OF BOUNDS SHARD")
												//SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_GENERIC_TEXT, "STRING", g_FMMC_STRUCT.tlCustomShardStrapline, DEFAULT, DEFAULT, "FMMC_CSO_SYKO")
												SET_BIT(iLocalBoolCheck15, LBOOL15_IS_OUT_OF_BOUNDS_SHARD_DISPLAYED)
											ENDIF
										ENDIF
									ENDIF
									SET_LOCAL_PLAYER_FAILED(3)
									MC_playerBD[iPartToUse].iReasonForPlayerFail = PLAYER_FAIL_REASON_BOUNDS
									
									IF g_FMMC_STRUCT.iShowPackageHudForTeam != 0
									
									ENDIF
								
									RESET_NET_TIMER(td_BoundsTimerResetDelay2)
									
									EXPLODE_LOCAL_PLAYER_DUE_TO_BOUNDS_FAIL(iTeam, iRule, TRUE, TRUE)
								ENDIF
								IF DOES_BLIP_EXIST(bounds_blip)
									PRINTLN("[BLIP][RH] Removing blip 6")
									REMOVE_BLIP(bounds_blip)
								ENDIF
								IF DOES_BLIP_EXIST(sec_bounds_blip)
									REMOVE_BLIP(sec_bounds_blip)
								ENDIF
							ENDIF
						ELSE
							PRINTLN("[PROCESS_MISSION_BOUNDS][LM] Timer Expired and bOnWrongSideOfBounds2 = TRUE - ciBS_RULE4_KILL_PLAYER_ON_BOUNDS_FAIL Set")
							
							IF iSpectatorTarget = -1
							AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
							AND NOT IS_OVERTIME_AND_NOT_VALID_FOR_OO_BOUNDS()
								IF NOT IS_PED_INJURED(LocalPlayerPed)
								AND NOT IS_PLAYER_RESPAWNING(PLAYER_ID())
									IF SHOULD_RULE_EXPLODE_ON_FAIL(iTeam, iRule, TRUE) //IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iRule], ciBS_RULE4_EXPLODE_ON_BOUNDS_FAIL)
										EXPLODE_LOCAL_PLAYER_DUE_TO_BOUNDS_FAIL(iTeam, iRule, DEFAULT, TRUE)
									ELSE
										IF NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
											//IF g_FMMC_STRUCT.iShowPackageHudForTeam != 0
												BROADCAST_FMMC_PLAYER_COMMITED_SUICIDE(iTeam)
											//ENDIF
											SET_ENTITY_HEALTH(LocalPlayerPed, 0)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						//Blocks the map area text when there are more than 6 players in the mode and the player is out of bounds
						IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_GRASS_GREENER(g_FMMC_STRUCT.iAdversaryModeType)
						OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
							IF SHOULD_BLOCK_MAP_AREA_TEXT_WHEN_USING_TIME_BARS()
								PRINTLN("[PROCESS_MISSION_BOUNDS][KH] HANDLE_BOUNDS_TIMERS_AND_TEXT - Blocking map area text as bounds timer is showing")
								HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
								HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
								HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
							ENDIF
						ENDIF
					
						IF iSpectatorTarget = -1
							IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_OBJECTIVE_BLOCKER)
								CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM0_TARGET)
								CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM1_TARGET)
								CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM2_TARGET)
								CLEAR_BIT(g_FMMC_STRUCT.iTargetTeamBlipBitset,TEAM3_TARGET)
								CLEANUP_OBJECTIVE_BLIPS()
								CPRINTLN(DEBUG_MISSION,"[PROCESS_MISSION_BOUNDS][RCC MISSION] PROCESS_MISSION_BOUNDS 2, PBBOOL_OBJECTIVE_BLOCKER")
								SET_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_OBJECTIVE_BLOCKER)
							ENDIF
						ENDIF
						
						// Create the blip that gets drawn in the middle of the bounds if we leave them
						IF NOT DOES_BLIP_EXIST( sec_bounds_blip )
						AND NOT IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetThree[ iRule ], ciBS_RULE3_HIDE_BLIP_ON_BOUNDS_2_FAIL )
							IF g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct2[ iRule ].fRadius > 0 // Spherical bounds
								
								VECTOR vBoundsPos = GET_FMMC_AREA_BOUNDS_POS(iTeam, iRule, TRUE)
								PRINTLN("[PROCESS_MISSION_BOUNDS][RCC MISSION][bounds] Get Bounds Pos B")
								
								IF ARE_VECTORS_EQUAL(vBoundsPos, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].vPos)
								OR (g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].iEntityType = ciRULE_TYPE_GOTO)
									sec_bounds_blip = ADD_BLIP_FOR_COORD( vBoundsPos )
									PRINTLN("[BLIP][RH] Creating blip 10")
								ELSE
									IF NOT ADD_FMMC_BOUNDS_BLIP(iTeam, iRule, TRUE)
										sec_bounds_blip = ADD_BLIP_FOR_COORD(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].vPos)
										PRINTLN("[BLIP][RH] Creating blip 11")
									ENDIF
								ENDIF
								
								HANDLE_BOUNDS_BLIP_HEIGHT_THRESHOLD(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule], sec_bounds_blip)
								HANDLE_BOUNDS_BLIP_COLOUR(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule], sec_bounds_blip)
							ELSE
								vBoundsCenter = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct2[ iRule ].vPos1 
									+ g_FMMC_STRUCT.sFMMCEndConditions[ iteam ].sBoundsStruct2[ iRule ].vPos2
								vBoundsCenter = vBoundsCenter * << 0.5, 0.5, 0.5 >>
								GET_GROUND_Z_FOR_3D_COORD( vBoundsCenter,vBoundsCenter.z)
								sec_bounds_blip = ADD_BLIP_FOR_COORD(vBoundsCenter)
								PRINTLN("[BLIP][RH] Creating blip 12")
								IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[iRule], ciBS_RULE11_LONG_RANGE_BLIP_SECONDARY)
									SET_BLIP_AS_SHORT_RANGE(sec_bounds_blip, TRUE)
								ENDIF
								
								HANDLE_BOUNDS_BLIP_HEIGHT_THRESHOLD(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule], sec_bounds_blip)
								HANDLE_BOUNDS_BLIP_COLOUR(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule], sec_bounds_blip)
							ENDIF
						ENDIF
						
						IF IS_NET_PLAYER_OK(BoundsPlayerToUse)
							IF iSpectatorTarget = -1
							AND IS_SOUND_ID_VALID(iBoundsExplodeSound)
							AND HAS_SOUND_FINISHED(iBoundsExplodeSound)
							AND (SHOULD_RULE_EXPLODE_ON_FAIL(iTeam, iRule, TRUE) /*IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iRule], ciBS_RULE4_EXPLODE_ON_BOUNDS_FAIL)*/ OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iRule], ciBS_RULE2_FORCE_BOUNDS_TIMER))
								ENTITY_INDEX entSource
								STRING sSoundSet = "GTAO_FM_Events_Soundset"
								IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
									sSoundSet = "DLC_SR_TR_General_Sounds"
								ENDIF
								IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
									entSource = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
								ELSE
									entSource = LocalPlayerPed
								ENDIF
								
								BOOL bNetworked = TRUE
								BOOL bShouldPlay = TRUE
								
								IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
									bNetworked = FALSE
									
									INT ms = ( GET_BOUNDS_TIME_LIMIT(iTeam,iRule,FALSE) - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_playerBD[iBoundsPartToUse].tdBounds2timer ) )
									IF ms > 15000
										bShouldPlay = FALSE									
										PRINTLN("[PROCESS_MISSION_BOUNDS][RCC MISSION] PROCESS_MISSION_BOUNDS - ciBS_RULE4_EXPLODE_ON_BOUNDS_FAIL - ms: ", ms, " should not play the sound yet...")
									ENDIF
								ENDIF
								
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Enable_Turn_Rotor_Tag_Team)
									bNetworked = FALSE
								ENDIF
								
								RESET_NET_TIMER(td_BoundsTimerResetDelay2)
								
								IF bShouldPlay
									PLAY_SOUND_FROM_ENTITY(iBoundsExplodeSound,"Explosion_Countdown", entSource, sSoundSet, bNetworked, 20) //PLAY_SOUND_FROM_ENTITY(iBoundsExplodeSound, "Explosion_Timer", entSource, "DLC_Lowrider_Relay_Race_Sounds", TRUE, 20)
									SET_VARIABLE_ON_SOUND(iBoundsExplodeSound, "Time", (GET_BOUNDS_TIME_LIMIT(iTeam,iRule,FALSE) / 1000.0))
									PRINTLN("[PROCESS_MISSION_BOUNDS][RCC MISSION] PROCESS_MISSION_BOUNDS - ciBS_RULE4_EXPLODE_ON_BOUNDS_FAIL")
								ENDIF							
							ENDIF
							
							IF NOT IS_SPECTATOR_HUD_HIDDEN()
							AND NOT IS_SPECTATOR_SHOWING_NEWS_HUD()
							AND NOT SHOULD_HIDE_THE_HUD_THIS_FRAME()
							AND (NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iRule], ciBS_RULE_HIDE_HUD)	OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iRule], ciBS_RULE2_FORCE_BOUNDS_TIMER))
								// Play area timer
								INT ms = ( GET_BOUNDS_TIME_LIMIT(iTeam,iRule,FALSE) - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_playerBD[iBoundsPartToUse].tdBounds2timer ) )
								IF ms >= 0
									RESET_NET_TIMER(td_BoundsTimerResetDelay2)
									IF ms < 30000
										eHudColour = HUD_COLOUR_RED
									ELSE
										eHudColour = HUD_COLOUR_WHITE
									ENDIF
									TEXT_LABEL_15 sBoundTimer
									IF AM_I_ON_A_HEIST()
										sBoundTimer = "MC_HBF"
									ELIF Is_Player_Currently_On_MP_LTS_Mission( LocalPlayer ) 
										sBoundTimer = "MC_LTSBF"
									ELIF Is_Player_Currently_On_MP_CTF_Mission( LocalPlayer ) 
										sBoundTimer = "MC_CTFBF"
									ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iRule], ciBS_RULE4_KILL_PLAYER_ON_BOUNDS_FAIL)
										sBoundTimer = "BM_OO_BOUNDS"
									ELSE
										sBoundTimer = "MC_MBF"
									ENDIF
									IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_OUT_OF_RANGE_SHARD)
										sBoundTimer = "BM_OO_RANGE"
									ENDIF
									IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_OT_RAMP_SHARD)
										sBoundTimer = "OT_JUMP_TIMER"
									ENDIF
									IF NOT bTimerDisplayed
										HUDORDER eHudOrder = HUDORDER_TOP
										IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[iRule], ciBS_RULE8_RESPAWN_PLAYERS_ON_KILL)
											eHudOrder = HUDORDER_SECONDBOTTOM
										ENDIF
										DRAW_GENERIC_TIMER(ms, sBoundTimer,0,TIMER_STYLE_DONTUSEMILLISECONDS,-1,PODIUMPOS_NONE,eHudOrder,FALSE,eHudColour,HUDFLASHING_NONE,0,FALSE,eHudColour)
									ENDIF
									bTimerDisplayed = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						IF iGodTextForBounds = 0 // If primary mission bounds haven't set this up already:
							INT iZoneHash
							INT iZoneHash2
							
							IF g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct2[ iRule ].fRadius > 0
								
								vBoundsCenter = GET_FMMC_AREA_BOUNDS_POS(iTeam, iRule, TRUE)
								
								IF ARE_VECTORS_EQUAL(vBoundsCenter, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].vPos)
								OR (g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].iEntityType = ciRULE_TYPE_GOTO)
									IF IS_STRING_NULL_OR_EMPTY(tlZoneName)
										GET_STREET_NAME_AT_COORD( vBoundsCenter, iZoneHash, iZoneHash2 )
										IF iZoneHash != 0
											tlZoneName = GET_STREET_NAME_FROM_HASH_KEY( iZoneHash )
										ENDIF
									ENDIF
								ELSE
									tlTempTextLabel = GET_FMMC_BOUNDS_ZONE_NAME_TEXT_LABEL(iTeam, iRule, TRUE)
									
									tlZoneName = TEXT_LABEL_TO_STRING(tlTempTextLabel)
									
									IF IS_STRING_NULL_OR_EMPTY(tlZoneName) // Backup
										GET_STREET_NAME_AT_COORD( vBoundsCenter, iZoneHash, iZoneHash2 )
										IF iZoneHash != 0
											tlZoneName = GET_STREET_NAME_FROM_HASH_KEY( iZoneHash )
										ENDIF
									ENDIF
								ENDIF
							ELSE
								vBoundsCenter = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct2[ iRule ].vPos1 
									+ g_FMMC_STRUCT.sFMMCEndConditions[ iteam ].sBoundsStruct2[ iRule ].vPos2
								vBoundsCenter = vBoundsCenter * << 0.5, 0.5, 0.5 >>
								IF IS_STRING_NULL_OR_EMPTY(tlZoneName)
									GET_STREET_NAME_AT_COORD( vBoundsCenter, iZoneHash, iZoneHash2 )
									IF iZoneHash != 0
										tlZoneName = GET_STREET_NAME_FROM_HASH_KEY( iZoneHash )
									ENDIF
								ENDIF
							ENDIF
							
							iGodTextForBounds = 2
						ENDIF
						
					ENDIF
				ENDIF
				
			ELIF bValidBounds2 // Right side of bounds 2
				IF DOES_BLIP_EXIST(sec_bounds_blip)
					REMOVE_BLIP(sec_bounds_blip)
					tlZoneName = ""
				ENDIF
				
				//[KH] - Clears the big message if the player reenters the bounds
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciMISSION_CUSTOM_SHARD_ON_OUT_OF_BOUNDS)
					IF IS_BIT_SET(iLocalBoolCheck15, LBOOL15_IS_OUT_OF_BOUNDS_SHARD_DISPLAYED)
						CLEAR_ALL_BIG_MESSAGES()
						PRINTLN("[PROCESS_MISSION_BOUNDS][KH] | CUSTOM SHARD ON OUT OF BOUNDS | BOUNDS 2 | CLEAR_ALL_BIG_MESSAGES()")
						CLEAR_BIT(iLocalBoolCheck15, LBOOL15_IS_OUT_OF_BOUNDS_SHARD_DISPLAYED)
						CLEAR_BIT(iLocalBoolCheck28, LBOOL28_BOUNDS_SHARD_SHOWN)
					ENDIF
				ENDIF
				
				IF HAS_NET_TIMER_STARTED(MC_playerBD[iPartToUse].tdBounds2timer)
					IF CAN_BOUNDS_TIMER_RESET_YET(TRUE)
						RESET_NET_TIMER(td_BoundsTimerResetDelay2)
						IF iSpectatorTarget = -1
							RESET_NET_TIMER(MC_playerBD[iPartToUse].tdBounds2timer)
						ENDIF
						IF NOT HAS_NET_TIMER_STARTED(MC_playerBD[iPartToUse].tdBoundstimer) 
						OR NOT bValidBounds1
							STOP_SOUND(iBoundsExplodeSound)
							IF iSpectatorTarget = -1
								IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
									CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
									PRINTLN("[RCC MISSION] PROCESS_PRE_MAIN_CHECKS - (18) Clearing - PBBOOL_OBJECTIVE_BLOCKER")
								ENDIF
							ENDIF					
							PRINTLN("[PROCESS_MISSION_BOUNDS][KH] | STOP_SOUND | BOUNDS 2")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[iRule], ciBS_RULE11_EXPLODE_ON_RULE_END_BOUNDS_FAIL) 
			IF (IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH) AND NOT IS_BIT_SET(iLocalBoolCheck28,LBBOOL28_HAS_DONE_SUDDEN_DEATH_BOUNDS_CHECK))
				SET_BIT(iLocalBoolCheck28,LBBOOL28_HAS_DONE_SUDDEN_DEATH_BOUNDS_CHECK)
			ENDIF
		ENDIF
		
		IF iGodTextForBounds > 0
			// Allowing custom godtext to be displayed when you leave the mission area
			TEXT_LABEL_63 sCustomAreaLeaveText = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].tl63BoundsFailMessage[ iRule ]
			
			IF  ((iGodTextForBounds = 1) AND bInBounds1) 
			OR ((iGodTextForBounds = 2) AND bInBounds2)
				// Check to see whether we have custom return text, and if so, use a custom string.
				IF NOT IS_STRING_NULL_OR_EMPTY( sCustomAreaLeaveText )
					tlZoneName = TEXT_LABEL_TO_STRING( sCustomAreaLeaveText )
					//PRINT_OBJECTIVE_WITH_CUSTOM_STRING( "MC_RTN_OBJ_C", sZoneName )
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset2, ciBS2_STOP_GO_HUD)
						IF !IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_LEAVE_AREA)
							Clear_Any_Objective_Text_From_This_Script()
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[iRule], ciBS_RULE8_OVERRIDE_FRIENDLY_WITH_GANG)
								INT iR, iG, iB, iA
								GET_HUD_COLOUR(GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam,BoundsPlayerToUse),iR, iG, iB,iA)
								SET_SCRIPT_VARIABLE_HUD_COLOUR(iR, iG, iB,iA)
								PRINTLN("[PROCESS_MISSION_BOUNDS][RCC MISSION] HANDLE_BOUNDS_TIMERS_AND_TEXT - PRE change: ", tlZoneName)
								REPLACE_STRING_IN_STRING(tlZoneName,"~f~", "~v~")
								PRINTLN("[PROCESS_MISSION_BOUNDS][RCC MISSION] HANDLE_BOUNDS_TIMERS_AND_TEXT - POST change: ", tlZoneName, " ~v~ colour: ",iR,", ", iG,", ", iB,", ",iA)
							ENDIF
							SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_LEAVE_AREA, "MC_LTS_OBJ_C", tlZoneName, DEFAULT, 999999)
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset2, ciBS2_STOP_GO_HUD)
						IF !IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_LEAVE_AREA)
							Clear_Any_Objective_Text_From_This_Script()
							SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_LEAVE_AREA, DEFAULT, DEFAULT, DEFAULT, 999999)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				// Check to see whether we have custom return text, and if so, use a custom string.
				IF NOT IS_STRING_NULL_OR_EMPTY( sCustomAreaLeaveText )
					tlZoneName = TEXT_LABEL_TO_STRING( sCustomAreaLeaveText )
					IF Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)
						IF g_bVSMission
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset2, ciBS2_STOP_GO_HUD)
								IF !IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_OO_BOUNDS)
									SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_OO_BOUNDS, "MC_LTS_OBJ_C", tlZoneName, DEFAULT, 999999)
								ENDIF
							ENDIF
							Clear_Any_Objective_Text_From_This_Script()
						ELSE
							PRINT_OBJECTIVE_WITH_CUSTOM_STRING( "MC_LTS_OBJ_C", tlZoneName ) //no full stop added
						ENDIF
					ELSE
						IF g_bVSMission
							BOOL bClearObj = TRUE
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset2, ciBS2_STOP_GO_HUD)
							
								IF !IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_OO_BOUNDS)
								AND NOT (IS_BIT_SET(iLocalBoolCheck28, LBOOL28_BOUNDS_SHARD_SHOWN) AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciOPTIONSBS17_ONESHOT_BOUNDS_SHARD))
									
									IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_POWER_PLAY(g_FMMC_STRUCT.iAdversaryModeType)
										IF bPowerPlayScaleFormActive
											IF iManagePowerPlayUI > PPH_CHECK_IF_ASSETS_READY 
												SET_POWER_PLAY_SHARD_VISIBLE(FALSE)
											ENDIF
										ENDIF
									ENDIF
										
									IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_SUMO_RUN(g_FMMC_STRUCT.iAdversaryModeType) 
									OR (IS_THIS_ROCKSTAR_MISSION_NEW_VS_SUMO_RUN(g_FMMC_STRUCT.iAdversaryModeType) 
										AND (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stSumoMessageDelay) > ciSumoMessageDelay) 
										AND HAS_NET_TIMER_STARTED(stSumoMessageDelay))
										RESET_NET_TIMER(stSumoMessageDelay)

										INT iTime = 999999
									
										IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciOPTIONSBS17_ONESHOT_BOUNDS_SHARD)
											iTime = 5000
										ENDIF
										
										IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[iRule], ciBS_RULE12_USE_SUMO_RUN_BOUND_SHARD)
											SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_OO_BOUNDS, "FMMC_SR_STL", tlZoneName, DEFAULT, iTime, "FMMC_SR_ST")
										ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[iRule], ciBS_RULE12_USE_TAG_TEAM_BOUNDS_SHARDS)
											IF (IS_LOCAL_PLAYER_LAST_ALIVE_ON_TEAM(iTeam)
											OR ((g_FMMC_STRUCT.iTagTeamCooldownTime*1000)-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdTagOutCooldown[iTeam])) > 0)
											OR IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH)
												SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_OO_BOUNDS, "ARENA_TGTE_0", "ARENA_TGTE_2", DEFAULT, iTime)
											ELSE
												TEXT_LABEL_15 tl15 = "ARENA_TGTE_1"
												tl15 += iTeam
												SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_OO_BOUNDS, "ARENA_TGTE_0", tl15, DEFAULT, iTime)												
											ENDIF
										ELSE
										 	SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_OO_BOUNDS, "MC_RTN_OBJ_C", tlZoneName, DEFAULT, iTime)
										ENDIF
										SET_BIT(iLocalBoolCheck28, LBOOL28_BOUNDS_SHARD_SHOWN)
											
									ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_SUMO_RUN(g_FMMC_STRUCT.iAdversaryModeType)
										IF NOT HAS_NET_TIMER_STARTED(stSumoMessageDelay) AND NOT IS_BIT_SET(iLocalBoolCheck28, LBOOL28_BOUNDS_SHARD_SHOWN)
											START_NET_TIMER(stSumoMessageDelay)
										ENDIF
									ENDIF
								ENDIF	
								
								IF !IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
								AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciOPTIONSBS17_ONESHOT_BOUNDS_SHARD)
								AND NOT (ARE_WE_CURRENTLY_IN_SUDDEN_DEATH() AND IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN))
									IF NOT HAS_NET_TIMER_STARTED(stSumoMessageDelay)
										RESET_NET_TIMER(stSumoMessageDelay)
									ENDIF
									PRINT_OBJECTIVE_WITH_CUSTOM_STRING( "MC_RTN_OBJ_C", tlZoneName)
									bClearObj = FALSE
								ENDIF
							ENDIF
							
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciOVERRIDE_BOUNDS_OBJECTIVE_TEXT_BLOCK)
								IF bClearObj
									Clear_Any_Objective_Text_From_This_Script()
								ENDIF
							ELSE
								TEXT_LABEL_63 tl63CustomGodText
								IF iRule < FMMC_MAX_RULES
									tl63CustomGodText = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63Objective[iRule]
								ENDIF
								
								IF NOT IS_STRING_NULL_OR_EMPTY( tl63CustomGodText )
									PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText)
								ELSE
									Clear_Any_Objective_Text_From_This_Script()
								ENDIF
							ENDIF
							
						ELSE
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iRule], ciBS_RULE2_WRONG_WAY_SHARD)
								IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset2, ciBS2_STOP_GO_HUD)
									IF NOT IS_STRING_NULL_OR_EMPTY( g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63WrongWayMessage[iRule] )
										IF !IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_OO_BOUNDS)
											SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_OO_BOUNDS, "MC_RTN_OBJ_C",g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63WrongWayMessage[iRule],DEFAULT, 999999)
										ENDIF
										Clear_Any_Objective_Text_From_This_Script()
									ELSE
										IF !IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_OO_BOUNDS)
											SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_OO_BOUNDS, "MC_RTN_OBJ_C",tlZoneName,DEFAULT, 999999)
										ENDIF
										Clear_Any_Objective_Text_From_This_Script()
									ENDIF
								ENDIF
							ELSE
								IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciOVERRIDE_BOUNDS_OBJECTIVE_TEXT_BLOCK)
									PRINT_OBJECTIVE_WITH_CUSTOM_STRING( "MC_RTN_OBJ_C", tlZoneName )
								ELSE
									TEXT_LABEL_63 tl63CustomGodText
									IF iRule < FMMC_MAX_RULES
										tl63CustomGodText = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63Objective[iRule]
									ENDIF
									
									IF NOT IS_STRING_NULL_OR_EMPTY( tl63CustomGodText )
										PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText)
									ELSE
										PRINT_OBJECTIVE_WITH_CUSTOM_STRING( "MC_RTN_OBJ_C", tlZoneName )
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_STRING_NULL_OR_EMPTY(tlZoneName)
						IF g_bVSMission
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset2, ciBS2_STOP_GO_HUD)
								IF !IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_OO_BOUNDS)
									IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciSHOW_LARGER_OUT_OF_BOUNDS_SHARD)
										SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_CUSTOM, "MC_RTN_OBJ", tlZoneName, DEFAULT, 999999,"FMMC_OOBSM" )
									ELSE
										SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_OO_BOUNDS, "MC_RTN_OBJ", tlZoneName, DEFAULT, 999999)
									ENDIF
								ENDIF
							ENDIF
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciOVERRIDE_BOUNDS_OBJECTIVE_TEXT_BLOCK)
								Clear_Any_Objective_Text_From_This_Script()
							ELSE
								TEXT_LABEL_63 tl63CustomGodText
								IF iRule < FMMC_MAX_RULES
									tl63CustomGodText = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63Objective[iRule]
								ENDIF
								
								IF NOT IS_STRING_NULL_OR_EMPTY( tl63CustomGodText )
									PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText)
								ELSE
									Clear_Any_Objective_Text_From_This_Script()
								ENDIF
							ENDIF
						ELSE
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iRule], ciBS_RULE2_WRONG_WAY_SHARD)
								IF NOT IS_STRING_NULL_OR_EMPTY( g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63WrongWayMessage[iRule] )
									IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset2, ciBS2_STOP_GO_HUD)
										IF !IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_OO_BOUNDS)
											SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_OO_BOUNDS, "MC_RTN_OBJ_C",g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63WrongWayMessage[iRule], DEFAULT, 999999)
										ENDIF
									ENDIF
									Clear_Any_Objective_Text_From_This_Script()
								ELSE
									IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset2, ciBS2_STOP_GO_HUD)
										IF !IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_OO_BOUNDS)
											SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_OO_BOUNDS, "MC_RTN_OBJ_C",tlZoneName,DEFAULT, 999999)
										ENDIF
									ENDIF
									Clear_Any_Objective_Text_From_This_Script()
								ENDIF
							ELSE
								IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciOVERRIDE_BOUNDS_OBJECTIVE_TEXT_BLOCK)
									PRINT_OBJECTIVE_WITH_CUSTOM_STRING( "MC_RTN_OBJ", tlZoneName )
								ELSE
									TEXT_LABEL_63 tl63CustomGodText
									IF iRule < FMMC_MAX_RULES
										tl63CustomGodText = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63Objective[iRule]
									ENDIF
									
									IF NOT IS_STRING_NULL_OR_EMPTY( tl63CustomGodText )
										PRINT_CUSTOM_OBJECTIVE(tl63CustomGodText)
									ELSE
										PRINT_OBJECTIVE_WITH_CUSTOM_STRING( "MC_RTN_OBJ", tlZoneName )
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
				PRINTLN("[BLIP][RH] spectator god text is 0")
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciSHOW_LARGER_OUT_OF_BOUNDS_SHARD)
				CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_CUSTOM)
			ENDIF
			IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_OO_BOUNDS)
				CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_OO_BOUNDS)
			ENDIF
			IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_LEAVE_AREA)
				CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_LEAVE_AREA)
			ENDIF
			
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_POWER_PLAY(g_FMMC_STRUCT.iAdversaryModeType)
				IF iManagePowerPlayUI > PPH_CHECK_IF_ASSETS_READY 
					IF bPowerPlayScaleFormActive
					AND NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
						SET_POWER_PLAY_SHARD_VISIBLE(TRUE)
					ELSE
						IF IS_POWER_PLAY_SHARD_VISIBLE()
							SET_POWER_PLAY_SHARD_VISIBLE(FALSE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		PROCESS_BOUNDS_VEHICLE_REGEN(iTeam, iRule, bValidBounds1, bValidBounds2, bOnWrongSideOfBounds1, bOnWrongSideOfBounds2, bInBounds1, bInBounds2) // bInPlayBounds1 bInPlayBounds2
		PROCESS_BOUNDS_TAG_OUT(iTeam, iRule, bValidBounds1, bValidBounds2, bOnWrongSideOfBounds1, bOnWrongSideOfBounds2, bInBounds1, bInBounds2) // bInPlayBounds1 bInPlayBounds2
		
		IF SHOULD_PLAY_OUT_OF_BOUNDS_SOUND(bValidBounds1, bValidBounds2, bOnWrongSideOfBounds1, bOnWrongSideOfBounds2)
			
			IF eHudColour = HUD_COLOUR_RED
			AND NOT SHOULD_RULE_EXPLODE_ON_FAIL(iTeam, iRule, bOnWrongSideOfBounds2)//IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iRule], ciBS_RULE4_EXPLODE_ON_BOUNDS_FAIL) // If we're not playing the bomb sound (we don't want them both playing)
				INT ms
					
				IF bValidBounds1 AND NOT bValidBounds2
				OR (bValidBounds1 AND bValidBounds2)
					ms = ( GET_BOUNDS_TIME_LIMIT(iTeam,iRule) - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_playerBD[iPartToUse].tdBoundstimer ) )
					RESET_NET_TIMER(td_BoundsTimerResetDelay1)
					PRINTLN("[PROCESS_MISSION_BOUNDS][LM] (1) SHOULD_PLAY_OUT_OF_BOUNDS_SOUND - ms: ", ms)
				ELIF bValidBounds2 AND NOT bValidBounds1
					ms = ( GET_BOUNDS_TIME_LIMIT(iTeam,iRule,FALSE) - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_playerBD[iPartToUse].tdBounds2timer ) )
					RESET_NET_TIMER(td_BoundsTimerResetDelay2)
					PRINTLN("[PROCESS_MISSION_BOUNDS][LM] (2) SHOULD_PLAY_OUT_OF_BOUNDS_SOUND - ms: ", ms)					
				ELSE
					ms = ( GET_BOUNDS_TIME_LIMIT(iTeam,iRule,FALSE) - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_playerBD[iPartToUse].tdBounds2timer ) )
					RESET_NET_TIMER(td_BoundsTimerResetDelay2)
					PRINTLN("[PROCESS_MISSION_BOUNDS][LM] (3) SHOULD_PLAY_OUT_OF_BOUNDS_SOUND - ms: ", ms)					
				ENDIF
				
				FLOAT fSeconds = ms/1000.0
				
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(g_FMMC_STRUCT.iAdversaryModeType)
				//OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
					ms = GET_BOUNDS_TIME_LIMIT(iTeam,iRule,FALSE)
					fSeconds = ms/1000.0
				ENDIF
					
				IF IS_SOUND_ID_VALID(iBoundsTimerSound)
				AND HAS_SOUND_FINISHED(iBoundsTimerSound)
				AND HAS_SOUND_FINISHED(iBoundsExplodeSound) OR NOT IS_SOUND_ID_VALID(iBoundsExplodeSound)
					PLAY_SOUND_FRONTEND(iBoundsTimerSound, "Out_of_Bounds", "MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
					PRINTLN("[PROCESS_MISSION_BOUNDS][LM] SHOULD_PLAY_OUT_OF_BOUNDS_SOUND - fSeconds: ", fSeconds)
					PRINTLN("[PROCESS_MISSION_BOUNDS][LM] SHOULD_PLAY_OUT_OF_BOUNDS_SOUND Returning TRUE and the sound hasn't started. Playing sound.")
					SET_VARIABLE_ON_SOUND(iBoundsTimerSound, "Time", fSeconds)
				ENDIF
			ENDIF
		ELSE
			IF IS_SOUND_ID_VALID(iBoundsTimerSound)
			AND NOT HAS_SOUND_FINISHED(iBoundsTimerSound)
				STOP_SOUND(iBoundsTimerSound)
				PRINTLN("[PROCESS_MISSION_BOUNDS][LM] SHOULD_PLAY_OUT_OF_BOUNDS_SOUND Returning FALSE and the sound hasn't finished. Force it to stop.")
			ENDIF
			
			IF SHOULD_RULE_EXPLODE_ON_FAIL(iTeam, iRule, bOnWrongSideOfBounds2) AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciSHOW_BOUNDS_TIMER_IN_SECONDARY_BOUNDS) // If we're not playing the bomb sound (we don't want them both playing)
				// url:bugstar:3403565
				// Timer perhaps has being cleared elsewhere and therefore never getting into the above.
				IF IS_SOUND_ID_VALID(iBoundsExplodeSound)
				AND NOT HAS_SOUND_FINISHED(iBoundsExplodeSound)		
					STOP_SOUND(iBoundsExplodeSound)
					PRINTLN("[PROCESS_MISSION_BOUNDS] - iBoundsExplodeSound Being stopped.")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC



PROC PROCESS_BOUNDS_CENTRE_LINE_RESPAWN()
	INT iTeam = MC_playerBD[ iPartToUse ].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[ iTeam ]
	
	IF iRule < FMMC_MAX_RULES
	AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule], ciBS_RULE6_BOUNDS_CENTRE_LINE_RESPAWN)	//CENTRE LINE RESPAWN - Always update respawn area to the get & deliver objects offset (Bug 2677618)
		IF HAS_TEAM_FINISHED(iTeam)
		OR IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_FINISHED)
		OR IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet2, PBBOOL2_PLAYER_OUT_BOUNDS_FAIL)
		OR IS_CUTSCENE_PLAYING()
		OR ( iRule < FMMC_MAX_RULES AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFive[iRule], ciBS_RULE5_ACTIVATE_BOUNDS_SUDDEN_DEATH) AND NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH())
			EXIT
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_SUB_TEAM_ROLES)
			IF iRule < FMMC_MAX_RULES
			AND MC_playerBD[iLocalPart].iCoronaRole != -1
			AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRolesBitset[iRule][MC_playerBD[iPartToUse].iCoronaRole], ciBS_ROLE_IGNORE_BOUNDS)
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iRolesBitset[iRule][MC_playerBD[iPartToUse].iCoronaRole], ciBS_ROLE_USE_SECONDARY_SPAWN_BOUNDS)			
					EXIT
				ENDIF
			ENDIF	
		ENDIF
		
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iEntityType > ciRULE_TYPE_NONE
		AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iEntityID > -1
		AND IS_VECTOR_ZERO(GET_FMMC_ENTITY_LOCATION(GET_ENTITY_COORDS(LocalPlayerPed, FALSE), g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iEntityType, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iEntityID, LocalPlayerPed))
		 	EXIT
		ENDIF
		
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fWidth > 0
			IF NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
			AND NOT IS_PLAYER_IN_RESPAWN_BACK_AT_START_LOGIC(iPartToUse)
				IF GET_MC_CLIENT_MISSION_STAGE(iLocalPart) = CLIENT_MISSION_STAGE_COLLECT_OBJ
				OR GET_MC_CLIENT_MISSION_STAGE(iLocalPart) = CLIENT_MISSION_STAGE_DELIVER_OBJ
					PRINTLN("[BOUNDS] Using bounds centre line respawn...")
					
					INT iObject
					OBJECT_INDEX objIndex
					
					FOR iObject = 0 TO (MC_ServerBD.iNumObjCreated - 1)
						IF MC_ServerBD_4.iObjPriority[iObject][iTeam] = iRule
						AND MC_serverBD_4.iObjRule[iObject][iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
							IF NETWORK_DOES_NETWORK_ID_EXIST(MC_ServerBD_1.sFMMC_SBD.niObject[iObject])
								objIndex = NET_TO_OBJ(MC_ServerBD_1.sFMMC_SBD.niObject[iObject])
								
								BREAKLOOP
							ENDIF
						ENDIF
					ENDFOR
					
					IF DOES_ENTITY_EXIST(objIndex)
						VECTOR vPoint1 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos1
						VECTOR vPoint2 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos2
						
						vPoint1.Z = vPoint1.Z + ((vPoint2.Z - vPoint1.Z) / 2.0)
						vPoint2.Z = vPoint1.Z
						
						
						FLOAT fOffset = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fCentreLineOffset
						FLOAT fRadius = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fCentreLineRadius
						
						VECTOR vCentrePoint = vPoint1 + ((vPoint2 - vPoint1) / 2.0)
						
						VECTOR vClosestPoint = GET_CLOSEST_POINT_ON_LINE(GET_ENTITY_COORDS(objIndex, FALSE), vPoint1, vPoint2)
						
						VECTOR vNormalised = NORMALISE_VECTOR(vPoint2 - vPoint1)
						
						VECTOR vOffset = vNormalised * fOffset
						
						FLOAT fDist
						
						IF GET_DISTANCE_BETWEEN_COORDS(vCentrePoint + vOffset, vPoint1) < GET_DISTANCE_BETWEEN_COORDS(vCentrePoint + vOffset, vPoint2)
							PRINTLN("[BOUNDS] CENTRE LINE RESPAWN ... vPoint1 is own side")
							fDist = GET_DISTANCE_BETWEEN_COORDS(vClosestPoint, vPoint1)
						ELSE
							PRINTLN("[BOUNDS] CENTRE LINE RESPAWN ... vPoint2 is own side")
							fDist = GET_DISTANCE_BETWEEN_COORDS(vClosestPoint, vPoint2)
						ENDIF
						
						IF fOffset > (fDist - fRadius)
							PRINTLN("[BOUNDS] CENTRE LINE RESPAWN ... fOffset > (fDist - fRadius)")
							fOffset = 0 - (fDist - fRadius)
						ELIF fOffset < 0 - (fDist - fRadius)
							PRINTLN("[BOUNDS] CENTRE LINE RESPAWN ... fOffset < 0 - (fDist - fRadius)")
							fOffset = (fDist - fRadius)
						ENDIF
						
						vOffset = vNormalised * fOffset
						
						VECTOR vGround = vClosestPoint
						
						GET_GROUND_Z_FOR_3D_COORD(vClosestPoint, vGround.Z, TRUE)
						
						PRINTLN("[BOUNDS] CENTRE LINE RESPAWN ... vPoint1 = ", vPoint1)
						PRINTLN("[BOUNDS] CENTRE LINE RESPAWN ... vPoint2 = ", vPoint2)
						PRINTLN("[BOUNDS] CENTRE LINE RESPAWN ... fOffset = ", fOffset)
						PRINTLN("[BOUNDS] CENTRE LINE RESPAWN ... fRadius = ", fRadius)
						PRINTLN("[BOUNDS] CENTRE LINE RESPAWN ... vClosestPoint = ", vClosestPoint)
						PRINTLN("[BOUNDS] CENTRE LINE RESPAWN ... fDist = ", fDist)
						PRINTLN("[BOUNDS] CENTRE LINE RESPAWN ... vNormalised = ", vNormalised)
						PRINTLN("[BOUNDS] CENTRE LINE RESPAWN ... vOffset = ", vOffset)
						PRINTLN("[BOUNDS] CENTRE LINE RESPAWN ... vGround = ", vGround)										
						
						SET_MISSION_SPAWN_SPHERE(vGround + vOffset, fRadius, DEFAULT, FALSE)
						
						SET_PLAYER_WILL_SPAWN_FACING_COORDS(vGround, TRUE, FALSE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_PROCESS_ANY_BOUNDS_THIS_FRAME()
	BOOL bProcessBounds = TRUE
	
	IF ( manualRespawnState > eManualRespawnState_OKAY_TO_SPAWN )
		PRINTLN("[JS] SHOULD_PROCESS_ANY_BOUNDS_THIS_FRAME - False, We are manually respawning")
		bProcessBounds = FALSE
	ENDIF
	
	IF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_TINY_RACERS_CHECKPOINT_LOGIC)
	AND NOT (MC_playerBD[iLocalPart].ePlayerTinyRacersRoundState = TR_Racing OR MC_playerBD[iLocalPart].ePlayerTinyRacersRoundState = TR_321))
		PRINTLN("[JS] SHOULD_PROCESS_ANY_BOUNDS_THIS_FRAME - False, Tiny Racers and not racing")
		bProcessBounds = FALSE
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_TEAM_0_SHOULD_RESPAWN_BACK_AT_START_NOW + MC_playerBD[iLocalPart].iTeam)
		PRINTLN("[JS] SHOULD_PROCESS_ANY_BOUNDS_THIS_FRAME - False, in the restart logic")
		bProcessBounds = FALSE
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		PRINTLN("[JS] SHOULD_PROCESS_ANY_BOUNDS_THIS_FRAME - False, SBBOOL_MISSION_OVER")
		bProcessBounds = FALSE
	ENDIF
	
	IF g_bMissionOver
		PRINTLN("[JS] SHOULD_PROCESS_ANY_BOUNDS_THIS_FRAME - False, g_bMissionOver")
		bProcessBounds = FALSE
	ENDIF
	
	RETURN bProcessBounds
ENDFUNC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: PROCESS MISSION BOUNDS (this checks whether we're in/out of any bounds) !
//
//************************************************************************************************************************************************************


PROC PROCESS_SERVER_COUNT_PLAYERS_IN_BOUNDS()

	IF NOT IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_USING_LIMITED_PLAYERS_IN_BOUNDS)
	OR NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		EXIT
	ENDIF
	
	INT iRule
	INT iTeam
	INT iPart
	INT iPlayersCount1[FMMC_MAX_TEAMS]
	INT iPlayersCount2[FMMC_MAX_TEAMS]
	INT iPlayersAllowedCount1[FMMC_MAX_TEAMS]
	INT iPlayersAllowedCount2[FMMC_MAX_TEAMS]
	PLAYER_INDEX piPlayer
	PED_INDEX pedIndex
	PARTICIPANT_INDEX partIndex 
		
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		partIndex = INT_TO_PARTICIPANTINDEX(iPart)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(partIndex)
			IF IS_BIT_SET(MC_serverBD_3.iPlayerBoundAllowedBitset1, iPart)
				IF MC_PlayerBD[iPart].iTeam > -1
					iPlayersAllowedCount1[MC_PlayerBD[iPart].iTeam]++
				ENDIF
			ENDIF
			IF IS_BIT_SET(MC_serverBD_3.iPlayerBoundAllowedBitset2, iPart)
				IF MC_PlayerBD[iPart].iTeam > -1
					iPlayersAllowedCount2[MC_PlayerBD[iPart].iTeam]++
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
		
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		iTeam = MC_PlayerBD[iPart].iTeam
		iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
		
		IF iRule < FMMC_MAX_RULES
			IF NOT IS_BIT_SET(MC_PlayerBD[iPart].iClientBitSet4, PBBOOL4_INSIDE_A_PLAY_AREA_BOUND_1)
				IF IS_BIT_SET(MC_serverBD_3.iPlayerBoundAllowedBitset1, iPart)
					PRINTLN("[LM][PROCESS_SERVER_COUNT_PLAYERS_IN_BOUNDS][BoundsLimiter] - iPart: ", iPart, " on Team: ", iTeam, " ||CLEARING|| Flagged as allowed in bounds 1.")
					CLEAR_BIT(MC_serverBD_3.iPlayerBoundAllowedBitset1, iPart)
				ENDIF
			ELSE
				PRINTLN("[LM][PROCESS_SERVER_COUNT_PLAYERS_IN_BOUNDS][BoundsLimiter] - iPart: ", iPart, " on Team: ", iTeam, " Is currently inside Play Area Bounds 1. Are they allowed: ", IS_BIT_SET(MC_serverBD_3.iPlayerBoundAllowedBitset1, iPart))
			ENDIF
			
			IF NOT IS_BIT_SET(MC_PlayerBD[iPart].iClientBitSet4, PBBOOL4_INSIDE_A_PLAY_AREA_BOUND_2)
				IF IS_BIT_SET(MC_serverBD_3.iPlayerBoundAllowedBitset2, iPart)
					PRINTLN("[LM][PROCESS_SERVER_COUNT_PLAYERS_IN_BOUNDS][BoundsLimiter] - iPart: ", iPart, " on Team: ", iTeam, " ||CLEARING|| Flagged as allowed in bounds 2.")
					CLEAR_BIT(MC_serverBD_3.iPlayerBoundAllowedBitset2, iPart)
				ENDIF
			ELSE
				PRINTLN("[LM][PROCESS_SERVER_COUNT_PLAYERS_IN_BOUNDS][BoundsLimiter] - iPart: ", iPart, " on Team: ", iTeam, " Is currently inside Play Area Bounds 2. Are they allowed: ", IS_BIT_SET(MC_serverBD_3.iPlayerBoundAllowedBitset2, iPart))
			ENDIF
			
			IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)
			AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_HEIST_SPECTATOR)
				partIndex = INT_TO_PARTICIPANTINDEX(iPart)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(partIndex)			
					piPlayer = NETWORK_GET_PLAYER_INDEX(partIndex)		
					
					IF iRule < FMMC_MAX_RULES
						IF NOT IS_PLAYER_RESPAWNING(piPlayer)
							pedIndex = GET_PLAYER_PED(piPlayer)
							
							IF NOT IS_PED_INJURED(pedIndex)
								
								// BOUNDS 1
								IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iMaxPlayersAllowedInBounds > 0
								AND IS_BIT_SET(MC_PlayerBD[iPart].iClientBitSet4, PBBOOL4_INSIDE_A_PLAY_AREA_BOUND_1)
									iPlayersCount1[iTeam]++
									
									IF NOT IS_BIT_SET(MC_serverBD_3.iPlayerBoundAllowedBitset1, iPart)
									AND iPlayersCount1[iTeam] <= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iMaxPlayersAllowedInBounds
									AND iPlayersAllowedCount1[iTeam] < g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iMaxPlayersAllowedInBounds
										PRINTLN("[LM][PROCESS_SERVER_COUNT_PLAYERS_IN_BOUNDS][BoundsLimiter] - iPart: ", iPart, " on Team: ", iTeam, " ||SETTING|| Flagged as allowed in bounds 1 (Name: ", GET_PLAYER_NAME(piPlayer), ")")
										SET_BIT(MC_serverBD_3.iPlayerBoundAllowedBitset1, iPart)
									ENDIF
								ENDIF
								
								// BOUNDS 2
								IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].iMaxPlayersAllowedInBounds > 0
								AND IS_BIT_SET(MC_PlayerBD[iPart].iClientBitSet4, PBBOOL4_INSIDE_A_PLAY_AREA_BOUND_2)
									iPlayersCount2[iTeam]++
								
									IF NOT IS_BIT_SET(MC_serverBD_3.iPlayerBoundAllowedBitset2, iPart)
									AND iPlayersCount2[iTeam] <= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].iMaxPlayersAllowedInBounds
									AND iPlayersAllowedCount2[iTeam] < g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].iMaxPlayersAllowedInBounds
										PRINTLN("[LM][PROCESS_SERVER_COUNT_PLAYERS_IN_BOUNDS][BoundsLimiter] - iPart: ", iPart, " on Team: ", iTeam, " ||SETTING|| Flagged as allowed in bounds 2 (Name: ", GET_PLAYER_NAME(piPlayer), ")")
										SET_BIT(MC_serverBD_3.iPlayerBoundAllowedBitset2, iPart)
									ENDIF
								ENDIF
							ENDIF					
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
		IF MC_serverBD_3.iNumberOfPlayersInBounds1[iTeam] != iPlayersCount1[iTeam]
			PRINTLN("[LM][PROCESS_SERVER_COUNT_PLAYERS_IN_BOUNDS][BoundsLimiter] - iTeam: ", iTeam, " Number of Players in this Bounds1 : ", MC_serverBD_3.iNumberOfPlayersInBounds1[iTeam])
			MC_serverBD_3.iNumberOfPlayersInBounds1[iTeam] = iPlayersCount1[iTeam]
		ENDIF
		IF MC_serverBD_3.iNumberOfPlayersInBounds2[iTeam] != iPlayersCount2[iTeam]
			PRINTLN("[LM][PROCESS_SERVER_COUNT_PLAYERS_IN_BOUNDS][BoundsLimiter] - iTeam: ", iTeam, " Number of Players in this Bounds2 : ", MC_serverBD_3.iNumberOfPlayersInBounds2[iTeam])
			MC_serverBD_3.iNumberOfPlayersInBounds2[iTeam] = iPlayersCount2[iTeam]
		ENDIF
	ENDFOR
ENDPROC

PROC REMOVE_ALL_BOUNDS_BLIPS()
	IF DOES_BLIP_EXIST(bounds_blip)
		PRINTLN("[LM][SPECBLIP] Removing Primary Blip.")
		REMOVE_BLIP(bounds_blip)
	ENDIF
	IF DOES_BLIP_EXIST(sec_bounds_blip)
		PRINTLN("[LM][SPECBLIP] Removing Secondary Blip.")
		REMOVE_BLIP(sec_bounds_blip)
	ENDIF
	IF DOES_BLIP_EXIST(biMissionBounds)
		PRINTLN("[LM][SPECBLIP] Removing biMissionBounds Blip.")
		REMOVE_BLIP(biMissionBounds)
	ENDIF
	IF DOES_BLIP_EXIST(biMissionBounds2)
		PRINTLN("[LM][SPECBLIP] Removing biMissionBounds2 Blip.")
		REMOVE_BLIP(biMissionBounds2)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_REMOVE_ALL_BOUNDS_BLIPS(INT iTeam)
	IF (IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH) AND IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_SUDDEN_DEATH_TAG_TEAM))
	OR (MC_serverBD.iNumberOfPlayingPlayers[iTeam] <= 1 AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Enable_Turn_Rotor_Tag_Team))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_MISSION_BOUNDS_DELAYS(INT iTeam, INT iRule)

	IF iRule >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	//Reset
	IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
		IF HAS_NET_TIMER_STARTED(tdBoundsTimerDelay1)
			PRINTLN("[LM][PROCESS_MISSION_BOUNDS_DELAYS] - clearing LBOOL29_DELAY_BLOCK_MISSION_BOUNDS_1 and resetting timer.")
			RESET_NET_TIMER(tdBoundsTimerDelay1)
			CLEAR_BIT(iLocalBoolCheck29, LBOOL29_DELAY_BLOCK_MISSION_BOUNDS_1)
		ENDIF
		IF HAS_NET_TIMER_STARTED(tdBoundsTimerDelay2)
			PRINTLN("[LM][PROCESS_MISSION_BOUNDS_DELAYS] - clearing LBOOL29_DELAY_BLOCK_MISSION_BOUNDS_2 and resetting timer.")
			RESET_NET_TIMER(tdBoundsTimerDelay2)
			CLEAR_BIT(iLocalBoolCheck29, LBOOL29_DELAY_BLOCK_MISSION_BOUNDS_2)
		ENDIF
	ENDIF
	
	// Primary
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iBoundActivationDelaySeconds > 0
		IF NOT HAS_NET_TIMER_STARTED(tdBoundsTimerDelay1)
			START_NET_TIMER(tdBoundsTimerDelay1)
			SET_BIT(iLocalBoolCheck29, LBOOL29_DELAY_BLOCK_MISSION_BOUNDS_1)
			PRINTLN("[LM][PROCESS_MISSION_BOUNDS_DELAYS] - setting LBOOL29_DELAY_BLOCK_MISSION_BOUNDS_1 and starting timer.")
		ENDIF
		IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdBoundsTimerDelay1, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iBoundActivationDelaySeconds*1000)
			IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_DELAY_BLOCK_MISSION_BOUNDS_1)
				PRINTLN("[LM][PROCESS_MISSION_BOUNDS_DELAYS] - clearing LBOOL29_DELAY_BLOCK_MISSION_BOUNDS_1 Timer has expired.")
				CLEAR_BIT(iLocalBoolCheck29, LBOOL29_DELAY_BLOCK_MISSION_BOUNDS_1)
			ENDIF
		ENDIF
	ELSE
		IF HAS_NET_TIMER_STARTED(tdBoundsTimerDelay1)
			RESET_NET_TIMER(tdBoundsTimerDelay1)
		ENDIF
	ENDIF
	
	// Secondary
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].iBoundActivationDelaySeconds > 0
		IF NOT HAS_NET_TIMER_STARTED(tdBoundsTimerDelay2)
			PRINTLN("[LM][PROCESS_MISSION_BOUNDS_DELAYS] - setting LBOOL29_DELAY_BLOCK_MISSION_BOUNDS_2 and starting timer.")
			START_NET_TIMER(tdBoundsTimerDelay2)
			SET_BIT(iLocalBoolCheck29, LBOOL29_DELAY_BLOCK_MISSION_BOUNDS_2)
		ENDIF
		IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdBoundsTimerDelay2, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].iBoundActivationDelaySeconds*1000)
			IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_DELAY_BLOCK_MISSION_BOUNDS_2)
				PRINTLN("[LM][PROCESS_MISSION_BOUNDS_DELAYS] - clearing LBOOL29_DELAY_BLOCK_MISSION_BOUNDS_2 Timer has expired.")
				CLEAR_BIT(iLocalBoolCheck29, LBOOL29_DELAY_BLOCK_MISSION_BOUNDS_2)
			ENDIF
		ENDIF
	ELSE
		IF HAS_NET_TIMER_STARTED(tdBoundsTimerDelay2)
			RESET_NET_TIMER(tdBoundsTimerDelay2)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_MISSION_BOUNDS() 
	INT iBoundsPartToUse = iPartToUse
	PLAYER_INDEX BoundsPlayerToUse = PlayerToUse
	PED_INDEX BoundsPlayerPedToUse = PlayerPedToUse
	IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
	AND iSpectatorTarget != -1
		iBoundsPartToUse = iSpectatorTarget
		BoundsPlayerToUse = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iBoundsPartToUse))
		BoundsPlayerPedToUse = GET_PLAYER_PED(BoundsPlayerToUse)
	ENDIF
	
	INT iTeam = MC_playerBD[ iBoundsPartToUse ].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[ iTeam ]
	
	IF HAS_TEAM_FINISHED(iTeam)
	OR IS_BIT_SET(MC_playerBD[iBoundsPartToUse].iClientBitSet, PBBOOL_FINISHED )
	OR IS_BIT_SET(MC_playerBD[iBoundsPartToUse].iClientBitSet2, PBBOOL2_PLAYER_OUT_BOUNDS_FAIL)
	OR IS_CUTSCENE_PLAYING()
	OR ( NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
	AND ((IS_PLAYER_RESPAWNING(BoundsPlayerToUse) AND NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE))
	OR manualRespawnState != eManualRespawnState_OKAY_TO_SPAWN AND manualRespawnState != eManualRespawnState_WAITING_AFTER_RESPAWN
	OR IS_PED_DEAD_OR_DYING(BoundsPlayerPedToUse)
	OR ( iRule < FMMC_MAX_RULES AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFive[iRule], ciBS_RULE5_ACTIVATE_BOUNDS_SUDDEN_DEATH) AND NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()))
	AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TRADING_PLACES_REMIX(g_FMMC_STRUCT.iAdversaryModeType))
	OR IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
		IF IS_SOUND_ID_VALID(iBoundsTimerSound)
		AND NOT HAS_SOUND_FINISHED(iBoundsTimerSound)
			STOP_SOUND(iBoundsTimerSound)
			PRINTLN("[PROCESS_MISSION_BOUNDS][EXIT] - iBoundsTimerSound Being stopped.")
		ENDIF
		
		IF IS_SOUND_ID_VALID(iBoundsExplodeSound)
		AND NOT HAS_SOUND_FINISHED(iBoundsExplodeSound)		
			STOP_SOUND(iBoundsExplodeSound)
			PRINTLN("[PROCESS_MISSION_BOUNDS][EXIT] - iBoundsExplodeSoundBeing stopped.")
		ENDIF
		
		IF IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
			REMOVE_ALL_BOUNDS_BLIPS()		 
		ENDIF
		
		PRINTLN("[PROCESS_MISSION_BOUNDS] EXIT...")
		EXIT
	ENDIF

	PROCESS_MISSION_BOUNDS_DELAYS(iTeam, iRule)
		
	//Special case for dawn raid as we still need to render the bounds on the minimap but don't want to actually process the bounds
	IF (iRule < FMMC_MAX_RULES AND IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN) AND ARE_WE_CURRENTLY_IN_SUDDEN_DEATH())
		VECTOR vBoundsCentreSD1 = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct[ iRule ].vPos
		VECTOR vBoundsCentreSD2 = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct2[ iRule ].vPos
		RENDER_BOUNDS_AREA_ON_MINIMAP(iTeam,iRule,vBoundsCentreSD1, TRUE)
		RENDER_BOUNDS_AREA_ON_MINIMAP(iTeam,iRule,vBoundsCentreSD2, TRUE)
		EXIT
	ENDIF
	
	IF iRule < FMMC_MAX_RULES
		BOOL bBlockBoundsBlip = SHOULD_REMOVE_ALL_BOUNDS_BLIPS(iTeam)
		VECTOR vBoundsCentre1 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos
		VECTOR vBoundsCentre2 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].vPos
		
		IF NOT bBlockBoundsBlip
			RENDER_BOUNDS_IN_MISSION(iTeam,iRule,vBoundsCentre1,vBoundsCentre2)
			RENDER_BOUNDS_AREA_ON_MINIMAP(iTeam,iRule,vBoundsCentre1, TRUE)
			RENDER_BOUNDS_AREA_ON_MINIMAP(iTeam,iRule,vBoundsCentre2, FALSE)
		ELSE
			REMOVE_ALL_BOUNDS_BLIPS()
		ENDIF 
		
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].MNIgnoreLeaveAreaVeh != DUMMY_MODEL_FOR_SCRIPT
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)) = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].MNIgnoreLeaveAreaVeh
					PRINTLN("[RCC MISSION][PROCESS_MISSION_BOUNDS][EXIT] - Local player is in vehicle exempt from leave areas. Primary Bounds")
					EXIT
				ENDIF
			ENDIF
		ENDIF
		
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].MNIgnoreLeaveAreaVeh != DUMMY_MODEL_FOR_SCRIPT
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			AND GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)) = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].MNIgnoreLeaveAreaVeh
				PRINTLN("[RCC MISSION][PROCESS_MISSION_BOUNDS][EXIT] - Local player is in vehicle exempt from leave areas. Secondary Bounds")
				EXIT
			ENDIF
		ENDIF
	ENDIF
	
	PROCESS_BOUNDS_AHEAD_POSITION()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_SUB_TEAM_ROLES)
		IF iRule < FMMC_MAX_RULES
		AND MC_playerBD[iLocalPart].iCoronaRole != -1
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRolesBitset[iRule][MC_playerBD[iBoundsPartToUse].iCoronaRole], ciBS_ROLE_IGNORE_BOUNDS)
			SET_BIT(MC_playerBD[iBoundsPartToUse].iClientBitSet2, PBBOOL2_IN_MISSION_BOUNDS)
			SET_BIT(MC_playerBD[iBoundsPartToUse].iClientBitSet2, PBBOOL2_IN_MISSION_BOUNDS2)
			
			IF CAN_BOUNDS_TIMER_RESET_YET(TRUE)
			OR CAN_BOUNDS_TIMER_RESET_YET(FALSE)
				RESET_NET_TIMER(td_BoundsTimerResetDelay1)
				RESET_NET_TIMER(td_BoundsTimerResetDelay2)
				RESET_NET_TIMER(MC_playerBD[iBoundsPartToUse].tdBoundstimer)
				RESET_NET_TIMER(MC_playerBD[iBoundsPartToUse].tdBounds2timer)
			ENDIF
					
			PRINTLN("[PROCESS_MISSION_BOUNDS] ciUSE_SUB_TEAM_ROLES RESET_NET_TIMER tdBoundstimer")
			PRINTLN("[PROCESS_MISSION_BOUNDS] ciUSE_SUB_TEAM_ROLES RESET_NET_TIMER tdBounds2timer")
			IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_OO_BOUNDS)
				CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_OO_BOUNDS)
			ENDIF
			IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_CUSTOM)
				CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_CUSTOM)
			ENDIF
			IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_LEAVE_AREA)
				CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_LEAVE_AREA)
			ENDIF
			
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iRolesBitset[iRule][MC_playerBD[iBoundsPartToUse].iCoronaRole], ciBS_ROLE_USE_SECONDARY_SPAWN_BOUNDS)			
				EXIT
			ENDIF
		ENDIF	
	ENDIF
	
	// Might want to do something where we check the driver part as our iBoundsParttoUse. This could be problematic if we don't have a driver though...
	// or we can do vPlayerPos = GET_ENTITY_COORD on the vehicle.
	IF IS_PLAYER_IN_CREATOR_AIRCRAFT(LocalPlayer)
		SET_BIT(MC_playerBD[iBoundsPartToUse].iClientBitSet2, PBBOOL2_IN_MISSION_BOUNDS)
		SET_BIT(MC_playerBD[iBoundsPartToUse].iClientBitSet2, PBBOOL2_IN_MISSION_BOUNDS2)
		
		IF CAN_BOUNDS_TIMER_RESET_YET(TRUE)
		OR CAN_BOUNDS_TIMER_RESET_YET(FALSE)
			RESET_NET_TIMER(td_BoundsTimerResetDelay1)
			RESET_NET_TIMER(td_BoundsTimerResetDelay2)
			RESET_NET_TIMER(MC_playerBD[iBoundsPartToUse].tdBoundstimer)
			RESET_NET_TIMER(MC_playerBD[iBoundsPartToUse].tdBounds2timer)
		ENDIF
		
		IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_OO_BOUNDS)
			CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_OO_BOUNDS)
		ENDIF
		IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_CUSTOM)
			CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_CUSTOM)
		ENDIF
		IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_LEAVE_AREA)
			CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_LEAVE_AREA)
		ENDIF
		
		IF DOES_BLIP_EXIST(bounds_blip)
			PRINTLN("[BLIP][RH] Removing blip 7")
			REMOVE_BLIP(bounds_blip)
		ENDIF
					
		IF DOES_BLIP_EXIST(sec_bounds_blip)
			REMOVE_BLIP(sec_bounds_blip)
		ENDIF
		
		IF DOES_BLIP_EXIST(map_bounds_blip)
			REMOVE_BLIP(map_bounds_blip)
		ENDIF
					
		PRINTLN("[RCC MISSION][PROCESS_MISSION_BOUNDS][EXIT] - INSIDE AVENGER INTERIOR.")
		EXIT
	ENDIF	

	IF IS_THIS_CURRENTLY_OVERTIME_TURNS()
		IF iLocalPart = iOTPartToMove
		AND iBoundsPartToUse != iLocalPart
			PRINTLN("[PROCESS_MISSION_BOUNDS][EXIT] - iOTPartToMove is me but iBoundsPartToUse is not. Exiting")
			EXIT
		ENDIF
	ENDIF
	
	
	PED_INDEX PedToUse
	BOOL bValidBounds1, bValidBounds2
	BOOL bProcessMapBounds = TRUE
	BOOL bSphereBound, bAreaBound
	BOOL bSphereBound2, bAreaBound2
	BOOL bSetSpawn
	BOOL bPrimary = TRUE
	VECTOR vPlayerPos, vBoundsPos
	
	IF iPartToUse = iLocalPart
		PedToUse = localPlayerPed
	ELSE
		IF iPartToUse > -1
			PARTICIPANT_INDEX pinPlayer = INT_TO_PARTICIPANTINDEX(iPartToUse)
			
			IF NETWORK_IS_PARTICIPANT_ACTIVE(pinPlayer)
				PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(pinPlayer)
				PedToUse = GET_PLAYER_PED(piPlayer)
			ENDIF
		ENDIF
	ENDIF
	
	//PRINTLN("[RCC MISSION][MMacK][Bounds] Current Rule is ", iRule)
	
	// Clear this bit every frame
	CLEAR_BIT( iLocalBoolCheck3, LBOOL3_IN_BOUNDS1 )
	CLEAR_BIT( iLocalBoolCheck3, LBOOL3_IN_BOUNDS2 )
	
	IF iSpectatorTarget = -1
		CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_MISSION_BOUNDS)
		CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_MISSION_BOUNDS2)
	ENDIF
	IF iRule < FMMC_MAX_RULES
		
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[iRule], ciBS_RULE11_EXPLODE_ON_RULE_END_BOUNDS_FAIL) 
				IF DOES_BLIP_EXIST(bounds_blip)
					PRINTLN("[BLIP][RH] Removing blip 8")
					REMOVE_BLIP(bounds_blip)
				ENDIF
			ENDIF
		ENDIF
	
		IF bProcessMapBounds
			
			vPlayerPos = GET_ENTITY_COORDS(PedToUse, FALSE)
			
			IF IS_VECTOR_OUT_OF_MAP_BOUNDS(vPlayerPos)
				CLEAR_BIT(iLocalBoolCheck10, LBOOL10_IN_MAP_BOUNDS)
			ELSE
				SET_BIT(iLocalBoolCheck10, LBOOL10_IN_MAP_BOUNDS)
			ENDIF
			
		ELSE
			
			SET_BIT(iLocalBoolCheck10, LBOOL10_IN_MAP_BOUNDS)
		
		ENDIF
		
		IF IS_MISSION_BOUND_VALID_FOR_USE(iTeam, iRule, 0)
			IF g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct[ iRule ].fRadius > 0
				
				bValidBounds1 = TRUE
				bSphereBound = TRUE
				
				vBoundsPos = GET_FMMC_AREA_BOUNDS_POS(iTeam, iRule)
				PRINTLN("[PROCESS_MISSION_BOUNDS][RCC MISSION][bounds] Get Bounds Pos C: ",vBoundsPos)
				
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					TINY_RACERS_SET_FRONT_OF_CAR_POS(vPlayerPos)
				ELSE
					vPlayerPos = GET_ENTITY_COORDS(PedToUse, FALSE)
				ENDIF
				
				FLOAT fBoundsRadius
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciTINY_RACERS_SHRINK_BOUNDS)
					fBoundsRadius = TINY_RACERS_GET_CURRENT_BOUNDS_SIZE(iTeam, iRule)
				ELSE
					fBoundsRadius = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fRadius
				ENDIF
				
				VECTOR vBoundsSphere = vBoundsPos
				
				// Ignore Z pos
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iBoundsBS, ciBounds_Ignore_Z_Checks_For_Spheres)
					vBoundsSphere = GET_FMMC_AREA_BOUNDS_POS(iTeam, iRule)
					
					IF vPlayerPos.z < MAP_BOUNDS_POS_Z
						vPlayerPos.z = vBoundsSphere.z
					ELSE
						PRINTLN("[PROCESS_MISSION_BOUNDS][RCC MISSION][bounds] Not ignoring player's Z because they are too high (out of map bounds)")
					ENDIF
				ENDIF
				//DRAW_DEBUG_SPHERE(vBoundsSphere, fBoundsRadius, 0, 200, 100, 150)
				IF GET_DISTANCE_BETWEEN_COORDS(vPlayerPos, vBoundsSphere, TRUE) < fBoundsRadius
					SET_BIT(iLocalBoolCheck3,LBOOL3_IN_BOUNDS1)
					IF iSpectatorTarget = -1
					AND bLocalPlayerOK
						SET_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_MISSION_BOUNDS)
					ENDIF
				//	PRINTLN("[RCC MISSION] [dsw] [bounds] PROCESS_MISSION_BOUNDS Set LBOOL3_IN_BOUNDS1 - 1 ")
				ELSE
				
					IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
						IF TINY_RACERS_CHECK_REAR_OF_CAR(vBoundsPos, fBoundsRadius)
							SET_BIT(iLocalBoolCheck3,LBOOL3_IN_BOUNDS1)
							IF iSpectatorTarget = -1
							AND bLocalPlayerOK
								SET_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_MISSION_BOUNDS)
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_ANYONE_ON_NEEDED_TEAM_IN_BOUNDS(iTeam, iRule)
						SET_BIT(iLocalBoolCheck3,LBOOL3_IN_BOUNDS1)
					ENDIF
				ENDIF
				
				//Resets vPlayerPos so that it does not affect later checks
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iBoundsBS, ciBounds_Ignore_Z_Checks_For_Spheres)
					vPlayerPos = GET_ENTITY_COORDS(LocalPlayerPed, FALSE)
				ENDIF
			ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fWidth > 0
			AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos1)
			AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos2)	
			
				bValidBounds1 = TRUE
				bAreaBound = TRUE
				
				//DRAW_DEBUG_ANGLED_AREA(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos1, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos2, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fWidth,0, 100, 200, 150)
				
				vPlayerPos = GET_ENTITY_COORDS(PedToUse, FALSE)
				
				IF IS_POINT_IN_ANGLED_AREA(vPlayerPos, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos1, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos2, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fWidth)
					SET_BIT( iLocalBoolCheck3,LBOOL3_IN_BOUNDS1 )
					IF iSpectatorTarget = -1
					AND bLocalPlayerOK
						SET_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_MISSION_BOUNDS)
					ENDIF
				//	PRINTLN("[RCC MISSION] [dsw] [bounds] PROCESS_MISSION_BOUNDS Set LBOOL3_IN_BOUNDS1 - 2 ")
				ELSE
					IF IS_ANYONE_ON_NEEDED_TEAM_IN_BOUNDS(iTeam, iRule)
						SET_BIT(iLocalBoolCheck3,LBOOL3_IN_BOUNDS1)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].mnOnlyAffectVeh != DUMMY_MODEL_FOR_SCRIPT
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)) != g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].mnOnlyAffectVeh
					PRINTLN("[RCC MISSION][PROCESS_MISSION_BOUNDS] - Local player is not in a vehicle which the bounds care about.")
					bValidBounds1 = FALSE
				ENDIF
			ENDIF
		ENDIF
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].mnOnlyAffectVeh != DUMMY_MODEL_FOR_SCRIPT
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)) != g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].mnOnlyAffectVeh
					PRINTLN("[RCC MISSION][PROCESS_MISSION_BOUNDS] - Local player is not in a vehicle which the secondary bounds care about.")
					bValidBounds2 = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_MISSION_BOUND_VALID_FOR_USE(iTeam, iRule, 1)
			IF g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct2[ iRule ].fRadius > 0
			AND NOT IS_THIS_A_CAPTURE()
			AND NOT IS_THIS_A_LTS()
				
				bValidBounds2 = TRUE
				bSphereBound2 = TRUE
				
				//bSphereBound = TRUE - these are used for mission bounds, which only run off sBoundsStruct (not sBoundsStruct2)
				
				//DRAW_DEBUG_SPHERE(vBoundsPos2, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].fRadius, 200, 0, 100, 150)
				
				IF IS_VECTOR_ZERO(vPlayerPos)
					vPlayerPos = GET_ENTITY_COORDS(LocalPlayerPed, FALSE)
				ENDIF
				
				PRINTLN("[PROCESS_MISSION_BOUNDS][RCC MISSION][bounds] Get Bounds Pos D")
				VECTOR vSecondaryBoundsSphere = GET_FMMC_AREA_BOUNDS_POS(iTeam, iRule, TRUE)
				
				// Ignore Z pos
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].iBoundsBS, ciBounds_Ignore_Z_Checks_For_Spheres)
					vPlayerPos.z = vSecondaryBoundsSphere.z
				ENDIF
				
				IF GET_DISTANCE_BETWEEN_COORDS(vPlayerPos, vSecondaryBoundsSphere, TRUE) < g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].fRadius	
					SET_BIT(iLocalBoolCheck3,LBOOL3_IN_BOUNDS2)
					IF iSpectatorTarget = -1
					AND bLocalPlayerOK
						SET_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_MISSION_BOUNDS2)
					ENDIF
					//PRINTLN("[RCC MISSION] [dsw] [bounds] PROCESS_MISSION_BOUNDS Set LBOOL3_IN_BOUNDS2 - 1 ")
				ELSE
					IF IS_ANYONE_ON_NEEDED_TEAM_IN_BOUNDS(iTeam, iRule, TRUE)
						SET_BIT(iLocalBoolCheck3,LBOOL3_IN_BOUNDS2)
					ENDIF
				ENDIF
				
			ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].fWidth > 0
			AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].vPos1)
			AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].vPos2)
			AND NOT IS_THIS_A_CAPTURE()
			AND NOT IS_THIS_A_LTS()
				
				bValidBounds2 = TRUE
				bAreaBound2 = TRUE
				//bAreaBound = TRUE - these are used for mission bounds, which only run off sBoundsStruct (not sBoundsStruct2)
				
				//DRAW_DEBUG_ANGLED_AREA(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].vPos1, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].vPos2, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].fWidth, 100, 0, 200, 150)
				
				IF IS_VECTOR_ZERO(vPlayerPos)
					vPlayerPos = GET_ENTITY_COORDS(LocalPlayerPed, FALSE)
				ENDIF
				
				IF IS_POINT_IN_ANGLED_AREA(vPlayerPos, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].vPos1, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].vPos2, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].fWidth)
					SET_BIT(iLocalBoolCheck3,LBOOL3_IN_BOUNDS2)
					IF iSpectatorTarget = -1
					AND bLocalPlayerOK
						SET_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_MISSION_BOUNDS2)
					ENDIF
					//PRINTLN("[RCC MISSION] [dsw] [bounds] PROCESS_MISSION_BOUNDS Set LBOOL3_IN_BOUNDS2 - 2 ")
				ELSE
					IF IS_ANYONE_ON_NEEDED_TEAM_IN_BOUNDS(iTeam, iRule, TRUE)
						SET_BIT(iLocalBoolCheck3,LBOOL3_IN_BOUNDS2)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iEntityType > ciRULE_TYPE_NONE
		AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iEntityID > -1
		AND IS_VECTOR_ZERO(GET_FMMC_ENTITY_LOCATION(GET_ENTITY_COORDS(LocalPlayerPed, FALSE), g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iEntityType, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iEntityID, LocalPlayerPed))
		 	PRINTLN("No more bounds processing, trying to set bounds on entity which isnt valid")
			IF iSpectatorTarget = -1
				SET_BIT(MC_playerBD[iBoundsPartToUse].iClientBitSet2, PBBOOL2_IN_MISSION_BOUNDS)
				SET_BIT(MC_playerBD[iBoundsPartToUse].iClientBitSet2, PBBOOL2_IN_MISSION_BOUNDS2)

				IF CAN_BOUNDS_TIMER_RESET_YET(TRUE)
				OR CAN_BOUNDS_TIMER_RESET_YET(FALSE)
					RESET_NET_TIMER(td_BoundsTimerResetDelay1)
					RESET_NET_TIMER(td_BoundsTimerResetDelay2)
					RESET_NET_TIMER(MC_playerBD[iBoundsPartToUse].tdBoundstimer)
					RESET_NET_TIMER(MC_playerBD[iBoundsPartToUse].tdBounds2timer)
				ENDIF
			ENDIF
			SET_BIT( iLocalBoolCheck3, LBOOL3_IN_BOUNDS1 )
			SET_BIT( iLocalBoolCheck3, LBOOL3_IN_BOUNDS2 )
			EXIT
		ENDIF
		
		IF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_SUB_TEAM_ROLES)
		AND iRule < FMMC_MAX_RULES
		AND MC_playerBD[iLocalPart].iCoronaRole != -1
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRolesBitset[iRule][MC_playerBD[iLocalPart].iCoronaRole], ciBS_ROLE_IGNORE_BOUNDS)
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iRolesBitset[iRule][MC_playerBD[iLocalPart].iCoronaRole], ciBS_ROLE_USE_SECONDARY_SPAWN_BOUNDS))			
			PRINTLN("[PROCESS_MISSION_BOUNDS] set player in bounds, we are wanting to use secondary for spawning purposes only")		
			IF iSpectatorTarget = -1
				SET_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_MISSION_BOUNDS)
				SET_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_MISSION_BOUNDS2)
				IF CAN_BOUNDS_TIMER_RESET_YET(TRUE)
				OR CAN_BOUNDS_TIMER_RESET_YET(FALSE)
					RESET_NET_TIMER(td_BoundsTimerResetDelay1)
					RESET_NET_TIMER(td_BoundsTimerResetDelay2)
					RESET_NET_TIMER(MC_playerBD[iLocalPart].tdBoundstimer)
					RESET_NET_TIMER(MC_playerBD[iLocalPart].tdBounds2timer)
				ENDIF
			ENDIF
			SET_BIT( iLocalBoolCheck3, LBOOL3_IN_BOUNDS1 )
			SET_BIT( iLocalBoolCheck3, LBOOL3_IN_BOUNDS2 )
		ENDIF
	
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
			IF NOT IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_TEAM_0_SHOULD_RESPAWN_BACK_AT_START_NOW + iTeam)
			AND NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
				IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_MISSION_BOUNDS)
					IF NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_OVERTIME_ENTERED_BOUNDS)
						PRINTLN("[RCC MISSION] - Setting LBOOL23_OVERTIME_ENTERED_BOUNDS")
						SET_BIT(iLocalBoolCheck23, LBOOL23_OVERTIME_ENTERED_BOUNDS)
					ENDIF					
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION] - We are transitioning from mini-round in Overtime, so we are suppressing the special 're-enter bounds explode stuff'")
			ENDIF		
		ENDIF
		
		IF bValidBounds1
		OR bValidBounds2	
			
			IF iOldSpawnPriority != iRule
				IF NOT IS_PLAYER_RESPAWNING(LocalPlayer) 
					IF IS_BIT_SET(iLocalBoolCheck, LBOOL_SPAWN_AREA_SET)
						IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
							CLEAR_SPAWN_AREA(TRUE, FALSE, SHOULD_USE_CUSTOM_SPAWN_POINTS())
							vBoundsSpawnPos_Temp = <<0, 0, 0>>
							RESET_NET_TIMER(tdBoundsSpawnDelayTimer)
							INT iZone
							FOR iZone = 0 TO FMMC_MAX_NUM_ZONES-1
								IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iType = ciFMMC_ZONE_TYPE__NO_PLAYER_SPAWNING
									IF IS_BIT_SET(iLocalZoneBitset, iZone)
										CLEAR_BIT(iLocalZoneBitset, iZone)
									ENDIF
								ENDIF
							ENDFOR
						ENDIF
						CLEAR_BIT(iLocalBoolCheck, LBOOL_SPAWN_AREA_SET)
					ENDIF
					iOldSpawnPriority = iRule
				ENDIF
			ENDIF
			
			IF iSpectatorTarget = -1
			AND ((IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iSpawnAreaBitset, iRule) AND IS_MISSION_BOUND_VALID_FOR_USE(iTeam, iRule, 0))
				OR (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iSecSpawnAreaBitset, iRule) AND IS_MISSION_BOUND_VALID_FOR_USE(iTeam, iRule, 1)))
			AND NOT IS_BIT_SET(iLocalBoolCheck5, LBOOL5_IS_MANUALLY_RESPAWNING_NOW)	// Don't set mission respawn bounds to the specified one if we're already respawning elsewhere
			AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule], ciBS_RULE6_BOUNDS_CENTRE_LINE_RESPAWN)	//CENTRE LINE RESPAWN - Always update respawn area to the get & deliver objects offset (Bug 2677618)				
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iSpawnWhileInArea, iRule) AND IS_MISSION_BOUND_VALID_FOR_USE(iTeam, iRule, 0)
					IF IS_BIT_SET(iLocalBoolCheck3, LBOOL3_IN_BOUNDS1)
						bSetSpawn = TRUE
					ENDIF
				ELIF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iSecSpawnWhileInArea, iRule ) AND IS_MISSION_BOUND_VALID_FOR_USE(iTeam, iRule, 1)
					IF IS_BIT_SET(iLocalBoolCheck3, LBOOL3_IN_BOUNDS2)
						bSetSpawn = TRUE
					ENDIF
				ELSE
					IF iRule < FMMC_MAX_RULES
					AND IS_THIS_ROCKSTAR_MISSION_SVM_DUNE4(g_FMMC_STRUCT.iRootContentIDHash)
					AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iRule], ciBS_RULE2_PLAY_AREA_TRIGGER_ON_START)
						IF NOT IS_BIT_SET( iLocalBoolCheck3, LBOOL3_IN_BOUNDS1 )
							PRINTLN("[PROCESS_MISSION_BOUNDS][KH][DUNE4] - Should still spawn outside the bounds")
							bSetSpawn = FALSE
						ELSE
							PRINTLN("[PROCESS_MISSION_BOUNDS][KH][DUNE4] - Inside the bounds, now we can spawn in it")
							bSetSpawn = TRUE
						ENDIF
					ELSE
						bSetSpawn = TRUE
					ENDIF
				ENDIF
				
				IF bSetSpawn
					
					IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iSecSpawnAreaBitset, iRule )
					AND IS_MISSION_BOUND_VALID_FOR_USE(iTeam, iRule, 1)
						PRINTLN("[PROCESS_MISSION_BOUNDS][LM][SPAWNBUG] - vBoundsSpawnPos_TempSecondary entered")
						IF g_FMMC_STRUCT.sFMMCEndConditions[iteam].sBoundsStruct2[iRule].iEntityType != ciRULE_TYPE_NONE
						AND g_FMMC_STRUCT.sFMMCEndConditions[iteam].sBoundsStruct2[iRule].iEntityID > -1
							IF NOT IS_VECTOR_ZERO(GET_FMMC_AREA_BOUNDS_POS(iTeam, iRule, TRUE))
								vBoundsSpawnPos_TempSecondary = GET_FMMC_AREA_BOUNDS_POS(iTeam, iRule, TRUE)
								PRINTLN("[PROCESS_MISSION_BOUNDS][LM][SPAWNBUG] - vBoundsSpawnPos_TempSecondary assigned: ", vBoundsSpawnPos_TempSecondary)
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_BIT_SET(iLocalBoolCheck, LBOOL_SPAWN_AREA_SET)
					OR SHOULD_BOUNDS_SPAWN_POSITION_UPDATE(iTeam, iRule, vBoundsPos)
						
						IF NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
						AND NOT IS_PLAYER_IN_RESPAWN_BACK_AT_START_LOGIC(iBoundsPartToUse)
							
							VECTOR vBoundsPos1 = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct[iRule].vPos1
							VECTOR vBoundsPos2 = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct[iRule].vPos2
							VECTOR vHeading	   = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct[iRule].vHeadingVec
							FLOAT  fSpawnRad   = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct[iRule].fRadius 
							FLOAT  fWidth	   = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fWidth
							
							BOOL bAllowTeamSwap = TRUE
							
							IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TRADING_PLACES_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
							AND iTeam = 1
								bAllowTeamSwap = FALSE
							ENDIF
							
							IF g_FMMC_STRUCT.sFMMCEndConditions[iteam].iSwapToTeamOnDeath[iRule] != -1
							AND bAllowTeamSwap
								iTeam = g_FMMC_STRUCT.sFMMCEndConditions[iteam].iSwapToTeamOnDeath[iRule]
								PRINTLN("[PROCESS_MISSION_BOUNDS][MMacK][BoundsTeamSwap] Swapped to team : ", iTeam)
							ENDIF
							
							IF IS_MISSION_BOUND_VALID_FOR_USE(iTeam, iRule, 1)
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_SUB_TEAM_ROLES)
									IF MC_playerBD[iBoundsPartToUse].iCoronaRole != -1
									AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iRolesBitset[iRule][MC_playerBD[iBoundsPartToUse].iCoronaRole], ciBS_ROLE_USE_SECONDARY_SPAWN_BOUNDS)
										PRINTLN("[PROCESS_MISSION_BOUNDS][MMacK][BoundsTeam] Using Secondary for team ", iteam, " on rule ", iRule, " with role ", MC_playerBD[iLocalPart].iCoronaRole)
										IF bSphereBound2	
											vBoundsPos = g_FMMC_STRUCT.sFMMCEndConditions[iteam].sBoundsStruct2[iRule].vPos
											fSpawnRad = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].fRadius										
											bPrimary = FALSE
										ELIF bAreaBound2
											vBoundsPos1 = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct2[iRule].vPos1
											vBoundsPos2 = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct2[iRule].vPos2
											fWidth = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].fWidth
											bPrimary = FALSE
										ELSE
											PRINTLN("[PROCESS_MISSION_BOUNDS][MMacK][BoundsTeam] NO SECONDARY BOUNDS SET")
										ENDIF
										vHeading = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct2[iRule].vHeadingVec
									ELSE
										IF MC_playerBD[iBoundsPartToUse].iCoronaRole != -1
											PRINTLN("[PROCESS_MISSION_BOUNDS][MMacK][BoundsTeam] Using Primary for team ", iteam, " on rule ", iRule, " with role ", MC_playerBD[iBoundsPartToUse].iCoronaRole)
										ENDIF
									ENDIF
								ELIF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iSecSpawnAreaBitset, iRule )
									IF bSphereBound2	
										vBoundsPos = g_FMMC_STRUCT.sFMMCEndConditions[iteam].sBoundsStruct2[iRule].vPos
										fSpawnRad = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].fRadius
										bPrimary = FALSE
									ELIF bAreaBound2
										vBoundsPos1 = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct2[iRule].vPos1
										vBoundsPos2 = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct2[iRule].vPos2
										fWidth = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].fWidth
										bPrimary = FALSE
									ELSE
										PRINTLN("[PROCESS_MISSION_BOUNDS][MMacK][BoundsTeam] NO SECONDARY BOUNDS SET")
									ENDIF
									vHeading = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct2[iRule].vHeadingVec
								ENDIF
							ELSE
								PRINTLN("[PROCESS_MISSION_BOUNDS][MMacK][BoundsTeam] Secondary bounds not valid for use")
							ENDIF
							
							IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iSecSpawnAreaBitset, iRule )
							AND IS_MISSION_BOUND_VALID_FOR_USE(iTeam, iRule, 1)
								IF g_FMMC_STRUCT.sFMMCEndConditions[iteam].sBoundsStruct2[iRule].iEntityType != ciRULE_TYPE_NONE
								AND g_FMMC_STRUCT.sFMMCEndConditions[iteam].sBoundsStruct2[iRule].iEntityID > -1
									IF NOT IS_VECTOR_ZERO(GET_FMMC_AREA_BOUNDS_POS(iTeam, iRule, TRUE))
										vBoundsPos = GET_FMMC_AREA_BOUNDS_POS(iTeam, iRule, TRUE)
										PRINTLN("[PROCESS_MISSION_BOUNDS][RCC MISSION][bounds] Get Bounds Pos E")
									ENDIF
								ENDIF
							ENDIF
							
							IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TRADING_PLACES_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
							OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].sBoundsStruct[iRule].iBoundsBS, ciBounds_KeepEntitySpawnInBounds)
								IF NOT IS_VECTOR_ZERO(vBoundsPos)
									IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iType = 1
										IF NOT IS_POINT_IN_ANGLED_AREA(vBoundsPos, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos1, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos2, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fWidth)
											PRINTLN("[PROCESS_MISSION_BOUNDS][RCC MISSION][bounds] - New spawn position is not within the angled area bounds. vBoundsPos: ", vBoundsPos)
											MovePointInsideAngledArea(vBoundsPos, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos1, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos2, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fWidth, 10)
											PRINTLN("[PROCESS_MISSION_BOUNDS][RCC MISSION][bounds] - Moving spawn position into the play area ANGLED AREA. New location: ", vBoundsPos)
										ENDIF
									ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iType = 0
										IF GET_DISTANCE_BETWEEN_COORDS(g_FMMC_STRUCT.sFMMCEndConditions[iteam].sBoundsStruct[iRule].vPos , vBoundsPos, TRUE) > g_FMMC_STRUCT.sFMMCEndConditions[iteam].sBoundsStruct[iRule].fRadius
											PRINTLN("[PROCESS_MISSION_BOUNDS][RCC MISSION][bounds] - New spawn position is not within the sphere bounds. vBoundsPos: ", vBoundsPos)
											MovePointInsideSphere(vBoundsPos, g_FMMC_STRUCT.sFMMCEndConditions[iteam].sBoundsStruct[iRule].vPos, g_FMMC_STRUCT.sFMMCEndConditions[iteam].sBoundsStruct[iRule].fRadius, 10)
											PRINTLN("[PROCESS_MISSION_BOUNDS][RCC MISSION][bounds] - Moving spawn position into the play area SPHERE. New location: ", vBoundsPos)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							IF (bSphereBound AND bPrimary) OR (bSphereBound2 AND NOT bPrimary)
								IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].sBoundsStruct[iRule].iBoundsBS, ciBounds_LEGACY_DelayedSpawnAreaPosition)
								OR IS_VECTOR_ZERO(vBoundsSpawnPos_Temp)
									PRINTLN("[PROCESS_MISSION_BOUNDS][RCC MISSION][bounds] Using custom spawn area - sphere, vBoundsPos = ",vBoundsPos,", fRadius = ",fSpawnRad)
									IF fSpawnRad > 0
										SET_MISSION_SPAWN_SPHERE(vBoundsPos, fSpawnRad, DEFAULT, IS_THIS_ROCKSTAR_MISSION_WVM_TAMPA(g_FMMC_STRUCT.iRootContentIDHash) OR (IS_THIS_ROCKSTAR_MISSION_WVM_OPPRESSOR_1(g_FMMC_STRUCT.iRootContentIDHash) AND (GET_INTERIOR_FROM_ENTITY(LocalPlayerPed) != NULL OR iRule = 8)))
										SET_BIT( iLocalBoolCheck, LBOOL_SPAWN_AREA_SET )
										vBoundsSpawnPos_Temp = vBoundsPos
										vBoundsSpawnPos_TempSecondary2 = vBoundsPos
									ELSE
										PRINTLN("[PROCESS_MISSION_BOUNDS][RCC MISSION][bounds] invalid spawn radius")
									ENDIF
								ELSE
									PRINTLN("[PROCESS_MISSION_BOUNDS][RCC MISSION][bounds] Using custom spawn area - sphere, vBoundsSpawnPos_Temp = ",vBoundsSpawnPos_Temp,", fRadius = ",fSpawnRad)
									SET_MISSION_SPAWN_SPHERE(vBoundsSpawnPos_Temp, fSpawnRad, DEFAULT, FALSE)
									SET_BIT( iLocalBoolCheck, LBOOL_SPAWN_AREA_SET )
									
									vBoundsSpawnPos_Temp = vBoundsPos
									vBoundsSpawnPos_TempSecondary2 = vBoundsPos
									REINIT_NET_TIMER(tdBoundsSpawnDelayTimer)
								ENDIF
								
								IF (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].sBoundsStruct[iRule].iBoundsBS, ciBounds_LEGACY_LookTowardsEntity) AND bPrimary)
								OR (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].sBoundsStruct2[iRule].iBoundsBS, ciBounds_LEGACY_LookTowardsEntity) AND NOT bPrimary)
									PRINTLN("[PROCESS_MISSION_BOUNDS][RCC MISSION] [dsw] [bounds] using Custom spawn area - sphere, ciBounds_LEGACY_LookTowardsEntity is set Heading = ", vBoundsSpawnPos_Temp)
									SET_PLAYER_WILL_SPAWN_FACING_COORDS(vBoundsSpawnPos_Temp, TRUE, FALSE)
								ELIF NOT IS_VECTOR_ZERO(vHeading)
									PRINTLN("[PROCESS_MISSION_BOUNDS][RCC MISSION] [dsw] [bounds] using Custom spawn area - sphere, vHeadingVec = ", vHeading)
									SET_PLAYER_WILL_SPAWN_FACING_COORDS(vHeading, TRUE, FALSE)
								ENDIF
								
								CREATE_RESTRICTION_ZONES(iTeam)
							ELIF (bAreaBound AND bPrimary) OR (bAreaBound2 AND NOT bPrimary)
								IF NOT IS_VECTOR_ZERO(vBoundsPos1)
								AND NOT IS_VECTOR_ZERO(vBoundsPos2)
									IF NOT IS_VECTOR_ZERO(GET_FMMC_AREA_BOUNDS_POS(iTeam, iRule, NOT bPrimary))
										vBoundsPos = GET_FMMC_AREA_BOUNDS_POS(iTeam, iRule, NOT bPrimary)
										PRINTLN("[PROCESS_MISSION_BOUNDS][RCC MISSION][bounds] Get Bounds Pos E")
										VECTOR vTranslation = vBoundsPos - ((vBoundsPos1 + vBoundsPos2) * 0.5)
										vBoundsPos1 = vBoundsPos1 + vTranslation
										vBoundsPos2 = vBoundsPos2 + vTranslation
									ENDIF
									PRINTLN("[PROCESS_MISSION_BOUNDS][RCC MISSION][bounds] using Cutsom spawn area - angled area, v1 = ",vBoundsPos1,", v2 = ",vBoundsPos2,", fWidth = ",fWidth)
									SET_MISSION_SPAWN_ANGLED_AREA(vBoundsPos1,vBoundsPos2,fWidth)
									vBoundsSpawnPos_Temp = (vBoundsPos1 + vBoundsPos2) * 0.5
									SET_BIT( iLocalBoolCheck, LBOOL_SPAWN_AREA_SET )
									
									IF (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].sBoundsStruct[iRule].iBoundsBS, ciBounds_LEGACY_LookTowardsEntity) AND bPrimary)
									OR (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].sBoundsStruct2[iRule].iBoundsBS, ciBounds_LEGACY_LookTowardsEntity) AND NOT bPrimary)
										PRINTLN("[PROCESS_MISSION_BOUNDS][RCC MISSION] [dsw] [bounds] using Custom spawn area - angled area, ciBounds_LEGACY_LookTowardsEntity is set Heading = ", vBoundsSpawnPos_Temp)
										SET_PLAYER_WILL_SPAWN_FACING_COORDS(GET_BOUNDS_TARGET_TO_FACE(), TRUE, FALSE)
									ELIF NOT IS_VECTOR_ZERO(vHeading)
										PRINTLN("[PROCESS_MISSION_BOUNDS][RCC MISSION] [dsw] [bounds] using Custom spawn area - angled area, vHeadingVec = ", vHeading)
										SET_PLAYER_WILL_SPAWN_FACING_COORDS(vHeading, TRUE, FALSE)
									ENDIF
									
									CREATE_RESTRICTION_ZONES(iTeam)
								ELSE
									SCRIPT_ASSERT("[PROCESS_MISSION_BOUNDS] SPAWN AREA IS NOT VALID - VECTORS <<0,0,0>> - ADD BUG FOR PAUL D")
								ENDIF
							ENDIF									
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF iSpectatorTarget = -1
				IF IS_BIT_SET( iLocalBoolCheck, LBOOL_SPAWN_AREA_SET )
					IF NOT IS_PLAYER_RESPAWNING( LocalPlayer ) 
						IF NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
							CLEAR_SPAWN_AREA(TRUE, FALSE, SHOULD_USE_CUSTOM_SPAWN_POINTS())
							vBoundsSpawnPos_Temp = <<0, 0, 0>>
							RESET_NET_TIMER(tdBoundsSpawnDelayTimer)
						ENDIF
						CLEAR_BIT( iLocalBoolCheck, LBOOL_SPAWN_AREA_SET )
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF bValidBounds1
			IF IS_BIT_SET( iLocalBoolCheck3, LBOOL3_IN_BOUNDS1 )
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPlayAreaBitset, iRule) AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciSHOW_BOUNDS_TIMER_IN_SECONDARY_BOUNDS)
					IF iSpectatorTarget = -1
						SET_BIT(MC_playerBD[iPartToUse].iBoundsBitSet,iRule)
						CLEAR_BIT(iLocalBoolCheck17, LBOOL17_IS_PLAY_OUT_OF_MAIN_BOUNDS)	
						IF HAS_NET_TIMER_STARTED(MC_playerBD[iPartToUse].tdBoundstimer)
							IF CAN_BOUNDS_TIMER_RESET_YET(FALSE)
								RESET_NET_TIMER(td_BoundsTimerResetDelay1)
								RESET_NET_TIMER(MC_playerBD[iPartToUse].tdBoundstimer)
								
								PRINTLN("[PROCESS_MISSION_BOUNDS] LBOOL3_IN_BOUNDS1 RESET_NET_TIMER tdBoundstimer")
								IF NOT HAS_NET_TIMER_STARTED(MC_playerBD[iPartToUse].tdBounds2timer)
								OR NOT bValidBounds2
									STOP_SOUND(iBoundsExplodeSound)
									IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
										CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
										PRINTLN("[RCC MISSION] PROCESS_PRE_MAIN_CHECKS - (17) Clearing - PBBOOL_OBJECTIVE_BLOCKER")
									ENDIF
									PRINTLN("[PROCESS_MISSION_BOUNDS][LM] STOP_SOUND (4)")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF DOES_BLIP_EXIST(bounds_blip)
						PRINTLN("[BLIP][RH] Removing blip 9")
						REMOVE_BLIP(bounds_blip)
					ENDIF
					
					IF Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer) //LTS has no other obj text so needs to clear this
						Delete_MP_Objective_Text()
					ENDIF
				ENDIF
			ELSE // Else we're not in the first bounds
				SET_BIT(iLocalBoolCheck17, LBOOL17_IS_PLAY_OUT_OF_MAIN_BOUNDS)
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSpawnWhileInArea, iRule)
					IF IS_BIT_SET(iLocalBoolCheck, LBOOL_SPAWN_AREA_SET)
						IF NOT IS_PLAYER_RESPAWNING(LocalPlayer) 
							IF NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
								CLEAR_SPAWN_AREA(TRUE, FALSE, SHOULD_USE_CUSTOM_SPAWN_POINTS())
								vBoundsSpawnPos_Temp = <<0, 0, 0>>
								RESET_NET_TIMER(tdBoundsSpawnDelayTimer)
							ENDIF
							PRINTLN("[PROCESS_MISSION_BOUNDS][RCC MISSION] PLAYER OUTSIDE spawn area so clearing it FOR OBJECTIVE: ",iRule)
							CLEAR_BIT(iLocalBoolCheck, LBOOL_SPAWN_AREA_SET)
						ENDIF
					ENDIF 
				ENDIF
				
				IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_INSIDE_A_PLAY_AREA_BOUND_1)
					PRINTLN("[PROCESS_MISSION_BOUNDS][LM][BoundsLimiter] PBBOOL4_INSIDE_A_PLAY_AREA_BOUND_1 CLEARING - NOT IN PROCESS")
					CLEAR_BIT(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_INSIDE_A_PLAY_AREA_BOUND_1)
				ENDIF
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST( bounds_blip )
				PRINTLN("[BLIP][RH] Removing blip 10")
				REMOVE_BLIP( bounds_blip )
			ENDIF
			
			IF iSpectatorTarget = -1
				IF HAS_NET_TIMER_STARTED(MC_playerBD[iPartToUse].tdBoundstimer)
					IF CAN_BOUNDS_TIMER_RESET_YET(FALSE)
						RESET_NET_TIMER(td_BoundsTimerResetDelay1)
						RESET_NET_TIMER(MC_playerBD[iPartToUse].tdBoundstimer)
						
						PRINTLN("[PROCESS_MISSION_BOUNDS] NOT ValidBound RESET_NET_TIMER tdBoundstimer")
						IF NOT HAS_NET_TIMER_STARTED(MC_playerBD[iPartToUse].tdBounds2timer)
						OR NOT bValidBounds2
							STOP_SOUND(iBoundsExplodeSound)
							IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
								CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
								PRINTLN("[RCC MISSION] PROCESS_PRE_MAIN_CHECKS - (16) Clearing - PBBOOL_OBJECTIVE_BLOCKER")
							ENDIF
							PRINTLN("[PROCESS_MISSION_BOUNDS][LM] STOP_SOUND (1)")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_INSIDE_A_PLAY_AREA_BOUND_1)
				PRINTLN("[PROCESS_MISSION_BOUNDS][LM][BoundsLimiter] PBBOOL4_INSIDE_A_PLAY_AREA_BOUND_1 CLEARING - NOT VALID")
				CLEAR_BIT(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_INSIDE_A_PLAY_AREA_BOUND_1)
			ENDIF
		ENDIF
		
		IF bValidBounds2
			IF IS_BIT_SET( iLocalBoolCheck3, LBOOL3_IN_BOUNDS2 )
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPlayArea2Bitset, iRule)
					IF iSpectatorTarget = -1
						SET_BIT(MC_playerBD[iPartToUse].iBounds2BitSet,iRule)					
						IF HAS_NET_TIMER_STARTED(MC_playerBD[iPartToUse].tdBounds2timer)
							IF CAN_BOUNDS_TIMER_RESET_YET(TRUE)
								RESET_NET_TIMER(td_BoundsTimerResetDelay2)
								RESET_NET_TIMER(MC_playerBD[iPartToUse].tdBounds2timer)
								
								IF NOT HAS_NET_TIMER_STARTED(MC_playerBD[iPartToUse].tdBoundstimer)
								OR NOT bValidBounds1
									STOP_SOUND(iBoundsExplodeSound)
									IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
										CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
										PRINTLN("[RCC MISSION] PROCESS_PRE_MAIN_CHECKS - (15) Clearing - PBBOOL_OBJECTIVE_BLOCKER")
									ENDIF
									PRINTLN("[PROCESS_MISSION_BOUNDS][LM] STOP_SOUND (2)")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF DOES_BLIP_EXIST(sec_bounds_blip)
						REMOVE_BLIP(sec_bounds_blip)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(sec_bounds_blip)
				REMOVE_BLIP(sec_bounds_blip)
			ENDIF
			IF iSpectatorTarget = -1
 				IF HAS_NET_TIMER_STARTED(MC_playerBD[iPartToUse].tdBounds2timer)					
					IF CAN_BOUNDS_TIMER_RESET_YET(TRUE)
						RESET_NET_TIMER(td_BoundsTimerResetDelay2)
						RESET_NET_TIMER(MC_playerBD[iPartToUse].tdBounds2timer)
					
						IF NOT HAS_NET_TIMER_STARTED(MC_playerBD[iPartToUse].tdBoundstimer)
						OR NOT bValidBounds1
							STOP_SOUND(iBoundsExplodeSound)
							IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
								CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
								PRINTLN("[RCC MISSION] PROCESS_PRE_MAIN_CHECKS - (14) Clearing - PBBOOL_OBJECTIVE_BLOCKER")
							ENDIF
							PRINTLN("[PROCESS_MISSION_BOUNDS][LM] STOP_SOUND (3)")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_INSIDE_A_PLAY_AREA_BOUND_2)
				PRINTLN("[PROCESS_MISSION_BOUNDS][LM][BoundsLimiter] PBBOOL4_INSIDE_A_PLAY_AREA_BOUND_2 CLEARING - NOT VALID")
				CLEAR_BIT(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_INSIDE_A_PLAY_AREA_BOUND_2)
			ENDIF
		ENDIF
		
		PRINTLN("[PROCESS_MISSION_BOUNDS][LM] bValidBounds1: ", bValidBounds1, " bValidBounds2: ", bValidBounds2, " bProcessMapBounds: ", bProcessMapBounds)
		PRINTLN("[PROCESS_MISSION_BOUNDS][LM] iCoronaRole: ", MC_playerBD[iPartToUse].iCoronaRole, " iTeam: ", iTeam, " iRule: ", iRule)
		
		IF (bValidBounds1 OR bValidBounds2 OR bProcessMapBounds)
			IF SHOULD_PROCESS_ANY_BOUNDS_THIS_FRAME() 
				HANDLE_BOUNDS_TIMERS_AND_TEXT(bValidBounds1, IS_BIT_SET(iLocalBoolCheck3, LBOOL3_IN_BOUNDS1), bValidBounds2, IS_BIT_SET(iLocalBoolCheck3, LBOOL3_IN_BOUNDS2), bProcessMapBounds, IS_BIT_SET(iLocalBoolCheck10, LBOOL10_IN_MAP_BOUNDS))
			ENDIF
		ELSE // No bounds active on this rule
			IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_OO_BOUNDS)
				CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_OO_BOUNDS)
			ENDIF
			IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_CUSTOM)
				CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_CUSTOM)
			ENDIF
			IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_LEAVE_AREA)
				CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_LEAVE_AREA)
			ENDIF
		ENDIF
		
		IF ( IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_SUB_TEAM_ROLES) AND MC_playerBD[iPartToUse].iCoronaRole != -1 AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRolesBitset[iRule][MC_playerBD[iPartToUse].iCoronaRole], ciBS_ROLE_Cant_Complete_This_Rule) )
			PRINTLN("[PROCESS_MISSION_BOUNDS][JS] Clearing big message  url:bugstar:2519098")
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRolesBitset[iRule][MC_playerBD[iPartToUse].iCoronaRole], ciBS_ROLE_LAST_RULE_FOR_TEAMMATE_OVERRIDE)
				IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_OO_BOUNDS)
					CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_OO_BOUNDS)
				ENDIF
				IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_CUSTOM)
					CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_CUSTOM)
				ENDIF
				IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_LEAVE_AREA)
					CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_LEAVE_AREA)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC
