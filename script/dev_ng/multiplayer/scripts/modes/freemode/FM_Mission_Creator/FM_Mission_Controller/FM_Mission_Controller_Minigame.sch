USING "FM_Mission_Controller_USING.sch"
USING "FM_Mission_Controller_Utility.sch"
USING "FM_Mission_Controller_Entities.sch"
USING "FM_Mission_Controller_ObjectiveCompletion.sch"
USING "FM_Mission_Controller_SpecialCase.sch"
USING "FM_Mission_Controller_Rules.sch"
USING "FM_Mission_Controller_Doors.sch"
USING "FM_Mission_Controller_Cutscene.sch"
USING "FM_Mission_Controller_Debug.sch"
USING "FM_Mission_Controller_Locations.sch"
USING "FM_Mission_Controller_Vehicles.sch"
USING "FM_Mission_Controller_Peds_Brain.sch"
USING "FM_Mission_Controller_Peds_Body.sch"
USING "FM_Mission_Controller_Peds.sch"
USING "FM_Mission_Controller_GangChase.sch"

SCRIPT_TIMER stMGTimer0
SCRIPT_TIMER stMGTimer1
INT iMGAttempts0
INT iMGAttempts1



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: GENERAL MINIGAME FUNCTIONS !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************

PROC DISPLAY_HELP_TEXT_THIS_FRAME_FOR_MINIGAME(STRING sText, BOOL bCurvedWindow)
	DISPLAY_HELP_TEXT_THIS_FRAME(sText, bCurvedWindow)
	SET_BIT(iLocalBoolCheck32, LBOOL32_MINIGAME_HELP_TEXT_SHOWING_THIS_FRAME)
ENDPROC


FUNC BOOL MANAGE_THIS_MINIGAME_TIMER( INT iStartTime, INT iDelay )
	
	IF GET_GAME_TIMER() - iStartTime > iDelay
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL HAS_MINIGAME_TIMER_BEEN_INITIALISED(INT iStoreSlot = 0)

	IF iStoreSlot = 0
		IF HAS_NET_TIMER_STARTED(stMGTimer0) 
			RETURN TRUE
		ENDIF
	ELIF iStoreSlot = 1
		IF HAS_NET_TIMER_STARTED(stMGTimer1) 
			RETURN TRUE
		ENDIF
	ELSE 
		SCRIPT_ASSERT("[RCC MISSION] - Invalid telemetry store slot - should be either 0 or 1 if you require more slots see Adam W")
	ENDIF
	
	RETURN FALSE

ENDFUNC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: MINIGAME TELEMETRY !
//
//************************************************************************************************************************************************************



PROC START_MINIGAME_TIMER_FOR_TELEMETRY(INT iStoreSlot = 0)

	IF iStoreSlot = 0
		REINIT_NET_TIMER(stMGTimer0) 
	ELIF iStoreSlot = 1
		REINIT_NET_TIMER(stMGTimer1) 
	ELSE 
		SCRIPT_ASSERT("[RCC MISSION] - Invalid telemetry store slot - should be either 0 or 1 if you require more slots see Adam W")
	ENDIF

ENDPROC

PROC STORE_MINIGAME_TIMER_FOR_TELEMETRY(INT iStoreSlot = 0)

	IF iStoreSlot = 0
		g_sJobHeistInfo.m_minigameTimeTaken0  = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stMGTimer0)
	ELIF iStoreSlot = 1
		g_sJobHeistInfo.m_minigameTimeTaken1  = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stMGTimer1)
	ELSE 
		SCRIPT_ASSERT("[RCC MISSION] - Invalid telemetry store slot - should be either 0 or 1 if you require more slots see Adam W")
	ENDIF

ENDPROC

PROC RESET_MINIGAME_TIMER_FOR_TELEMETRY(INT iStoreSlot = 0)

	IF iStoreSlot = 0
		RESET_NET_TIMER(stMGTimer0) 
	ELIF iStoreSlot = 1
		RESET_NET_TIMER(stMGTimer1) 
	ELSE 
		SCRIPT_ASSERT("[RCC MISSION] - Invalid telemetry store slot - should be either 0 or 1 if you require more slots see Adam W")
	ENDIF

ENDPROC

PROC RESET_MINIGAME_ATTEMPTS_FOR_TELEMETRY(INT iStoreSlot = 0)

	IF iStoreSlot = 0
		iMGAttempts0 = 0
	ELIF iStoreSlot = 1
		iMGAttempts1 = 0
	ELSE 
		SCRIPT_ASSERT("[RCC MISSION] - Invalid telemetry store slot - should be either 0 or 1 if you require more slots see Adam W")
	ENDIF

ENDPROC

//Call once to increase attempts
PROC INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY(INT iStoreSlot = 0)

	IF iStoreSlot = 0
		iMGAttempts0 ++ 
	ELIF iStoreSlot = 1
		iMGAttempts1 ++ 
	ELSE 
		SCRIPT_ASSERT("[RCC MISSION] - Invalid telemetry store slot - should be either 0 or 1 if you require more slots see Adam W")
	ENDIF

ENDPROC

PROC STORE_MINIGAME_ATTEMPTS_FOR_TELEMETRY(INT iStoreSlot = 0)

	IF iStoreSlot = 0
		g_sJobHeistInfo.m_minigameNumberOfTimes0   = iMGAttempts0
	ELIF iStoreSlot = 1
		g_sJobHeistInfo.m_minigameNumberOfTimes1   = iMGAttempts1
	ELSE 
		SCRIPT_ASSERT("[RCC MISSION] - Invalid telemetry store slot - should be either 0 or 1 if you require more slots see Adam W")
	ENDIF

ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: MINIGAME CLEANUP !
//
//************************************************************************************************************************************************************



PROC DELETE_LAPTOP_OBJECT()
	OBJECT_INDEX tempObj
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_playerBD[iPartToUse].netMiniGameObj2)
		tempObj = NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2)
		DELETE_OBJECT(tempObj)
		MC_RESERVE_NETWORK_MISSION_OBJECTS(-1)
	ENDIF
ENDPROC

PROC DELETE_FAKE_PHONE_OBJECT()
	OBJECT_INDEX tempObj
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_playerBD[iPartToUse].netMiniGameObj2)
		tempObj = NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2)
		DELETE_OBJECT(tempObj)
		MC_RESERVE_NETWORK_MISSION_OBJECTS(-1)
	Else
		tempObj = GET_CLOSEST_OBJECT_OF_TYPE(GET_ENTITY_COORDS(LocalPlayerPed), 10, Prop_Phone_ING_03, false, false, false)
		IF DOES_ENTITY_EXIST(tempObj)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
				NETWORK_INDEX niPhone
				niPhone = OBJ_TO_NET(tempObj)
				DELETE_NET_ID(niPhone)
				MC_RESERVE_NETWORK_MISSION_OBJECTS(-1)
			ELSE
				NETWORK_REQUEST_CONTROL_OF_ENTITY(tempObj)
			endif
		endif
	endif
ENDPROC

PROC DELETE_HACKING_KEYCARD_OBJECT()
	OBJECT_INDEX tempObj
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_playerBD[iPartToUse].netMiniGameObj)
		tempObj = NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj)
		DELETE_OBJECT(tempObj)
		MC_RESERVE_NETWORK_MISSION_OBJECTS(-1)
	ENDIF
ENDPROC

PROC DELETE_KEYCARD_OBJECT()
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_playerBD[iPartToUse].netMiniGameObj)
		
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_playerBD[iPartToUse].netMiniGameObj)
			PRINTLN("[RCC MISSION] DELETE_KEYCARD_OBJECT: deleted keycard object.")
			DELETE_NET_ID(MC_playerBD[iPartToUse].netMiniGameObj)
		ELSE
			PRINTLN("[RCC MISSION] DELETE_KEYCARD_OBJECT: set keycard as no longer needed.")
			CLEANUP_NET_ID(MC_playerBD[iPartToUse].netMiniGameObj)
		ENDIF
		
		MC_RESERVE_NETWORK_MISSION_OBJECTS(-1)
	ENDIF
ENDPROC

PROC CLEAN_UP_MINIGAME_OBJECTS()
	DELETE_FAKE_PHONE_OBJECT()
	DELETE_KEYCARD_OBJECT()
	DELETE_LAPTOP_OBJECT()
ENDPROC

PROC SET_HACK_FAIL_BIT_SET(INT iCall)

	UNUSED_PARAMETER(iCall)
	
	IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iHackFailBitset, MC_playerBD[iLocalPart].iObjHacking)
		PRINTLN("SET_HACK_FAIL_BIT_SET - Call ", iCall, " - Setting MC_playerBD[iLocalPart].iHackFailBitset bit ", MC_playerBD[iLocalPart].iObjHacking)
		DEBUG_PRINTCALLSTACK()
		
		SET_BIT(MC_playerBD[iLocalPart].iHackFailBitset, MC_playerBD[iLocalPart].iObjHacking)
	ENDIF
ENDPROC

PROC RESET_OBJ_HACKING_INT()
	MC_playerBD[iLocalPart].iObjHacking = -1
	PRINTLN("RESET_OBJ_HACKING - Resetting MC_playerBD[iLocalPart].iObjHacking back to -1")
	DEBUG_PRINTCALLSTACK()
ENDPROC

PROC RESET_ALL_HACKING_MINIGAMES()
	
	RESET_SAFE_CRACK(SafeCrackData, !IS_BIT_SET( MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_CC_FAILCUT_FINISH ))
	CLEAR_SAFE_CRACK_SEQUENCES(SafeCrackData)
	REMOVE_ANIM_DICT("mp_common_miss")
	RESET_NET_TIMER(tdHackTimer)
	ENABLE_INTERACTION_MENU()
	IF NOT g_b_OnLeaderboard
		CLEANUP_MENU_ASSETS()
	ENDIF
	FORCE_QUIT_PASS_HACKING_MINIGAME(sHackingData, !IS_BIT_SET( MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_CC_FAILCUT_FINISH ))
	IF bLocalPlayerPedOk
		//2284319
		IF g_FMMC_STRUCT.iRootContentIDHash = g_sMPTUNABLES.iroot_id_HASH_The_Flecca_Job
			IF NOT IS_BIT_SET( MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_CC_FAILCUT_FINISH )
				NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
				CPRINTLN(DEBUG_MISSION, "RESET_ALL_HACKING_MINIGAMES - NET_SET_PLAYER_CONTROL(LocalPlayer,TRUE)")
			ENDIF
		ELSE
			NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
			PRINTLN("[RCC MISSION] RESET_ALL_HACKING_MINIGAMES() - Returning player control.")
		ENDIF
		CLEAR_PED_TASKS(LocalPlayerPed)
	ENDIF
	PRINTLN("[RCC MISSION] SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)")
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
	CLEAR_BIT(MC_playerBD[iLocalPart].iHackFailBitset,MC_playerBD[iLocalPart].iObjHacking)
	PRINTLN("[RCC MISSION]  resetting iObjHacking, part: ",iPartToUse," for object: ",MC_playerBD[iLocalPart].iObjHacking)
	IF MC_playerBD[iLocalPart].iObjHacking != -1
		CLEAN_UP_BEAM_HACK_MINIGAME(sBeamHack[MC_playerBD[iLocalPart].iObjHacking])
		CLEAN_UP_HOTWIRE_MINIGAME(sHotwire[MC_playerBD[iLocalPart].iObjHacking])
		CLEAN_UP_FINGERPRINT_CLONE(sFingerprintCloneGameplay, sFingerprintClone[MC_playerBD[iLocalPart].iObjHacking], DEFAULT, HASH("heist"), g_HeistTelemetryData.iHeistPosixTime)
		CLEAN_UP_ORDER_UNLOCK(sOrderUnlockGameplay, sOrderUnlock[MC_playerBD[iLocalPart].iObjHacking], DEFAULT, HASH("heist"), g_HeistTelemetryData.iHeistPosixTime)
	ENDIF
	RESET_OBJ_HACKING_INT()
ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: HEIST BAGS FOR MINIGAMES !
//
//************************************************************************************************************************************************************

/// PURPOSE: Gets the tunable multiplier used for double money events
FUNC FLOAT GET_PACIFIC_STANDARD_TUNABLE_CASH_MULTIPLIER()

	FLOAT fMultiplier = 1.0
	FLOAT fCashAmountMod = 1.0

	fCashAmountMod = TO_FLOAT(g_sMPTunables.iHeist_Pacific_Standard_Job_Cash_Reward) / 1000000
	
	IF MC_serverBD.iNumHeistPlays > 1
		fMultiplier = g_sMPTunables.fEarnings_Heists_Finale_replay_cash_reward
		PRINTLN( "[JJT MISSION][MINIGAME] Heist tunable multiplier set to (replay): ", fMultiplier )
	ELSE
		fMultiplier = g_sMPTunables.fEarnings_Heists_Finale_first_play_cash_reward
		PRINTLN( "[JJT MISSION][MINIGAME] Heist tunable multiplier set to (first_play): ", fMultiplier )
	ENDIF

	IF fCashAmountMod < 1.0
		PRINTLN("fCashAmountMod WAS UNDER 1!")
		fCashAmountMod = 1.0
	ENDIF
	
	PRINTLN("fCashAmountMod = ", fCashAmountMod)
	PRINTLN("g_sMPTunables.iHeist_Pacific_Standard_Job_Cash_Reward = ", g_sMPTunables.iHeist_Pacific_Standard_Job_Cash_Reward)
	PRINTLN("Before: fMultiplier = ", fMultiplier)
	
	fMultiplier *= fCashAmountMod
	PRINTLN("After: fMultiplier = ", fMultiplier)
	
	RETURN fMultiplier

ENDFUNC

FUNC BOOL CREATE_HACK_BAG_OBJECTS(BOOL bSetInvisible = FALSE)
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_playerBD[iPartToUse].netMiniGameObj3)
		REQUEST_MODEL(MC_playerBD_1[iLocalPart].mnHeistGearBag)
		IF HAS_MODEL_LOADED(MC_playerBD_1[iLocalPart].mnHeistGearBag)
			IF NOT IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_BAG)
				MC_RESERVE_NETWORK_MISSION_OBJECTS(1)
				SET_BIT(iLocalBoolCheck12,LBOOL12_OBJ_RES_BAG)
			ENDIF
			IF IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_BAG)
				IF CAN_REGISTER_MISSION_OBJECTS(1)
					IF CREATE_NET_OBJ(MC_playerBD[iPartToUse].netMiniGameObj3, MC_playerBD_1[iLocalPart].mnHeistGearBag,GET_ENTITY_COORDS(LocalPlayerPed),bIsLocalPlayerHost)
						SET_ENTITY_COLLISION(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj3), FALSE)
						SET_ENTITY_INVINCIBLE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj3), TRUE)
						SET_ENTITY_VISIBLE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj3), NOT bSetInvisible)
						SET_MODEL_AS_NO_LONGER_NEEDED(MC_playerBD_1[iLocalPart].mnHeistGearBag)
						
						CDEBUG1LN(DEBUG_MISSION, "[AW_MISSION] CREATE_BAG_OBJECTS: successfully created bag object.")
						CLEAR_BIT(iLocalBoolCheck12,LBOOL12_OBJ_RES_BAG)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC DELETE_HACK_BAG_OBJECT()
	OBJECT_INDEX tempObj
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_playerBD[iPartToUse].netMiniGameObj3)
		tempObj = NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj3)
		
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_playerBD[iPartToUse].netMiniGameObj3)
			CDEBUG1LN(DEBUG_MISSION, "[AW_MISSION] DELETE_BAG_OBJECT: deleted bag object.")
		
			DELETE_OBJECT(tempObj)
		ELSE
			CDEBUG1LN(DEBUG_MISSION, "[AW_MISSION] DELETE_BAG_OBJECT: set bag as no longer needed.")
		
			CLEANUP_NET_ID(MC_playerBD[iPartToUse].netMiniGameObj3)
		ENDIF
		
		MC_RESERVE_NETWORK_MISSION_OBJECTS(-1)
	ENDIF
ENDPROC

FUNC BOOL CREATE_BAG_OBJECTS(BOOL bSetInvisible = FALSE, BOOL bSetNoAlpha = FALSE)
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[0])
		REQUEST_MODEL(MC_playerBD_1[iLocalPart].mnHeistGearBag)
		IF HAS_MODEL_LOADED(MC_playerBD_1[iLocalPart].mnHeistGearBag)
			IF NOT IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_BAG)
				MC_RESERVE_NETWORK_MISSION_OBJECTS(1)
				SET_BIT(iLocalBoolCheck12,LBOOL12_OBJ_RES_BAG)
			ENDIF
			IF IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_BAG)
				IF CAN_REGISTER_MISSION_OBJECTS(1)
					IF CREATE_NET_OBJ(niMinigameObjects[0], MC_playerBD_1[iLocalPart].mnHeistGearBag,GET_ENTITY_COORDS(LocalPlayerPed), FALSE)
						SET_ENTITY_COLLISION(NET_TO_OBJ(niMinigameObjects[0]), FALSE)
						SET_ENTITY_INVINCIBLE(NET_TO_OBJ(niMinigameObjects[0]), TRUE)
						SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[0]), NOT bSetInvisible)
						SET_MODEL_AS_NO_LONGER_NEEDED(MC_playerBD_1[iLocalPart].mnHeistGearBag)
						SET_NETWORK_ID_CAN_MIGRATE(niMinigameObjects[0], FALSE)
						
						IF bSetNoAlpha
							SET_ENTITY_ALPHA(NET_TO_OBJ(niMinigameObjects[0]), 0, FALSE)
						ENDIF
						
						PRINTLN("[AW_MISSION] - [RCC MISSION] CREATE_BAG_OBJECTS: successfully created bag object.")
						CLEAR_BIT(iLocalBoolCheck12,LBOOL12_OBJ_RES_BAG)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL CREATE_BAG_OBJECTS_AT_POSITION(VECTOR vPosition, BOOL bSetInvisible = FALSE, BOOL bSetNoAlpha = FALSE)

	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[0])
		REQUEST_MODEL(MC_playerBD_1[iLocalPart].mnHeistGearBag)
		IF HAS_MODEL_LOADED(MC_playerBD_1[iLocalPart].mnHeistGearBag)
			IF NOT IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_BAG)
				MC_RESERVE_NETWORK_MISSION_OBJECTS(1)
				SET_BIT(iLocalBoolCheck12,LBOOL12_OBJ_RES_BAG)
			ENDIF
			IF IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_BAG)
				IF CAN_REGISTER_MISSION_OBJECTS(1)
					PRINTLN("[RCC MISSION] CREATE_BAG_OBJECTS_AT_POSITION - GET_NUM_RESERVED_MISSION_OBJECTS: ", GET_NUM_RESERVED_MISSION_OBJECTS(FALSE))
					IF CREATE_NET_OBJ(niMinigameObjects[0], MC_playerBD_1[iLocalPart].mnHeistGearBag,vPosition, FALSE)
						SET_ENTITY_COLLISION(NET_TO_OBJ(niMinigameObjects[0]), FALSE)
						SET_ENTITY_INVINCIBLE(NET_TO_OBJ(niMinigameObjects[0]), TRUE)
						SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[0]), NOT bSetInvisible)
						SET_MODEL_AS_NO_LONGER_NEEDED(MC_playerBD_1[iLocalPart].mnHeistGearBag)
						SET_NETWORK_ID_CAN_MIGRATE(niMinigameObjects[0], FALSE)
						SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(niMinigameObjects[0], TRUE)
						
						IF bSetNoAlpha
							SET_ENTITY_ALPHA(NET_TO_OBJ(niMinigameObjects[0]), 0, FALSE)
						ENDIF

						PRINTLN("[RCC MISSION] CREATE_BAG_OBJECTS_AT_POSITION: successfully created bag object at position ", vPosition)
						CLEAR_BIT(iLocalBoolCheck12,LBOOL12_OBJ_RES_BAG)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC DELETE_BAG_OBJECT()
	OBJECT_INDEX tempObj
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[0])
		tempObj = NET_TO_OBJ(niMinigameObjects[0])
		
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(niMinigameObjects[0])
			PRINTLN("[AW_MISSION] - [RCC MISSION] DELETE_BAG_OBJECT: deleted bag object.")
		
			DELETE_OBJECT(tempObj)
		ELSE
			PRINTLN("[AW_MISSION] - [RCC MISSION] DELETE_BAG_OBJECT: set bag as no longer needed.")
		
			CLEANUP_NET_ID(niMinigameObjects[0])
		ENDIF
		
		MC_RESERVE_NETWORK_MISSION_OBJECTS(-1)
	ENDIF
ENDPROC



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: DRILLING MINIGAME !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************



FUNC BOOL CREATE_DRILL_OBJECT(BOOL bSetInvisible = FALSE)
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[0])
		REQUEST_MODEL(hei_prop_heist_drill)
		IF HAS_MODEL_LOADED(hei_prop_heist_drill)
			IF NOT IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_DRILL)
				MC_RESERVE_NETWORK_MISSION_OBJECTS(1)
				SET_BIT(iLocalBoolCheck12,LBOOL12_OBJ_RES_DRILL)
			ENDIF
			IF IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_DRILL)
				IF CAN_REGISTER_MISSION_OBJECTS(1)
					IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[0])
						IF CREATE_NET_OBJ(niMinigameObjects[0],hei_prop_heist_drill,GET_ENTITY_COORDS(LocalPlayerPed), bIsLocalPlayerHost)
							FREEZE_ENTITY_POSITION(NET_TO_OBJ(niMinigameObjects[0]), TRUE)
							SET_ENTITY_COLLISION(NET_TO_OBJ(niMinigameObjects[0]), FALSE)
							ATTACH_ENTITY_TO_ENTITY(NET_TO_OBJ(niMinigameObjects[0]),LocalPlayerPed,GET_PED_BONE_INDEX(LocalPlayerPed,BONETAG_PH_R_HAND),<<0,0,0>>,<<0,0,0>>)
							SET_ENTITY_INVINCIBLE(NET_TO_OBJ(niMinigameObjects[0]), TRUE)
							SET_NETWORK_ID_CAN_MIGRATE(niMinigameObjects[0], FALSE)
							SET_MODEL_AS_NO_LONGER_NEEDED(hei_prop_heist_drill)
							
							SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[0]), NOT bSetInvisible)
							
							CLEAR_BIT(iLocalBoolCheck12,LBOOL12_OBJ_RES_DRILL)
							RETURN TRUE
						ENDIF
					ELSE
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC DELETE_DRILL_OBJECT()
	OBJECT_INDEX tempObj
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[0])
		tempObj = NET_TO_OBJ(niMinigameObjects[0])
		
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(niMinigameObjects[0])
			PRINTLN("[DRILLING] - [RCC MISSION] DELETE_DRILL_OBJECT: deleted object.")
			DELETE_OBJECT(tempObj)
		ELSE
			PRINTLN("[DRILLING] - [RCC MISSION] DELETE_DRILL_OBJECT: set object as no longer needed.")
			CLEANUP_NET_ID(niMinigameObjects[0])
		ENDIF
		
		MC_RESERVE_NETWORK_MISSION_OBJECTS(-1)
	ENDIF
ENDPROC

FUNC BOOL CREATE_DRILL_BAG_OBJECT(BOOL bSetInvisible = FALSE)

	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[1])
		REQUEST_MODEL(MC_playerBD_1[iLocalPart].mnHeistGearBag)
		IF HAS_MODEL_LOADED(MC_playerBD_1[iLocalPart].mnHeistGearBag)
			IF NOT IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_DRILL_BAG)
				MC_RESERVE_NETWORK_MISSION_OBJECTS(1)
				SET_BIT(iLocalBoolCheck12,LBOOL12_OBJ_RES_DRILL_BAG)
			ENDIF
			IF IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_DRILL_BAG)
				IF CAN_REGISTER_MISSION_OBJECTS(1)
					IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[1])
						IF CREATE_NET_OBJ(niMinigameObjects[1],MC_playerBD_1[iLocalPart].mnHeistGearBag,GET_ENTITY_COORDS(LocalPlayerPed), bIsLocalPlayerHost)
							FREEZE_ENTITY_POSITION(NET_TO_OBJ(niMinigameObjects[1]), TRUE)
							SET_ENTITY_COLLISION(NET_TO_OBJ(niMinigameObjects[1]), FALSE)
							SET_ENTITY_INVINCIBLE(NET_TO_OBJ(niMinigameObjects[1]), TRUE)
							SET_NETWORK_ID_CAN_MIGRATE(niMinigameObjects[1], FALSE)
							SET_MODEL_AS_NO_LONGER_NEEDED(MC_playerBD_1[iLocalPart].mnHeistGearBag)
							
							SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[1]), NOT bSetInvisible)
							CLEAR_BIT(iLocalBoolCheck12,LBOOL12_OBJ_RES_DRILL_BAG)
							
							RETURN TRUE
						ENDIF
					ELSE
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC DELETE_DRILL_BAG_OBJECT()
	OBJECT_INDEX tempObj
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[1])
		tempObj = NET_TO_OBJ(niMinigameObjects[1])
		
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(niMinigameObjects[1])
			PRINTLN("[DRILLING] - [RCC MISSION] DELETE_DRILL_BAG_OBJECT: deleted object.")
			DELETE_OBJECT(tempObj)
		ELSE
			PRINTLN("[DRILLING] - [RCC MISSION] DELETE_DRILL_BAG_OBJECT: set object as no longer needed.")
			CLEANUP_NET_ID(niMinigameObjects[1])
		ENDIF
		
		MC_RESERVE_NETWORK_MISSION_OBJECTS(-1)
	ENDIF
ENDPROC

FUNC BOOL CREATE_DRILL_BOX_OBJECT(VECTOR vPos, VECTOR vRot)
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[2])
		REQUEST_MODEL(HEI_PROP_HEIST_DEPOSIT_BOX)
		IF HAS_MODEL_LOADED(HEI_PROP_HEIST_DEPOSIT_BOX)
			IF NOT IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_DRILL_BOX)
				MC_RESERVE_NETWORK_MISSION_OBJECTS(1)
				SET_BIT(iLocalBoolCheck12,LBOOL12_OBJ_RES_DRILL_BOX)
			ENDIF
			
			IF IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_DRILL_BOX)
				IF CAN_REGISTER_MISSION_OBJECTS(1)
					
					IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[2])
						IF CREATE_NET_OBJ(niMinigameObjects[2],HEI_PROP_HEIST_DEPOSIT_BOX, vPos, bIsLocalPlayerHost)
							SET_ENTITY_COORDS_NO_OFFSET(NET_TO_OBJ(niMinigameObjects[2]), vPos)
							SET_ENTITY_ROTATION(NET_TO_OBJ(niMinigameObjects[2]), vRot)
							FREEZE_ENTITY_POSITION(NET_TO_OBJ(niMinigameObjects[2]), TRUE)
							SET_ENTITY_COLLISION(NET_TO_OBJ(niMinigameObjects[2]), FALSE)
							SET_ENTITY_INVINCIBLE(NET_TO_OBJ(niMinigameObjects[2]), TRUE)
							SET_NETWORK_ID_CAN_MIGRATE(niMinigameObjects[2], FALSE)
							SET_MODEL_AS_NO_LONGER_NEEDED(HEI_PROP_HEIST_DEPOSIT_BOX)
							
							PRINTLN("[DRILLING] - [RCC MISSION] [CREATE_DRILL_BOX_OBJECT] - Object created at ", vPos, " ", vRot)
							
							INTERIOR_INSTANCE_INDEX interiorCurrent
							INT iCurrentRoom
							
							interiorCurrent = GET_INTERIOR_FROM_ENTITY(LocalPlayerPed)
							iCurrentRoom = GET_ROOM_KEY_FROM_ENTITY(LocalPlayerPed)
							
							IF interiorCurrent != NULL
							AND iCurrentRoom != 0
								PRINTLN("[DRILLING] - [RCC MISSION] [CREATE_DRILL_BOX_OBJECT] - Forcing room for object.")
							
								FORCE_ROOM_FOR_ENTITY(NET_TO_OBJ(niMinigameObjects[2]), interiorCurrent, iCurrentRoom)
							ENDIF
							
							CLEAR_BIT(iLocalBoolCheck12,LBOOL12_OBJ_RES_DRILL_BOX)
							
							RETURN TRUE
						ENDIF
					ELSE
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC DELETE_DRILL_BOX_OBJECT()
	OBJECT_INDEX tempObj
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[2])
		tempObj = NET_TO_OBJ(niMinigameObjects[2])
		
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(niMinigameObjects[2])
			PRINTLN("[DRILLING] - [RCC MISSION] DELETE_DRILL_BOX_OBJECT: deleted object.")
			DELETE_OBJECT(tempObj)
		ELSE
			PRINTLN("[DRILLING] - [RCC MISSION] DELETE_DRILL_BOX_OBJECT: set object as no longer needed.")
			CLEANUP_NET_ID(niMinigameObjects[2])
		ENDIF
		
		MC_RESERVE_NETWORK_MISSION_OBJECTS(-1)
	ENDIF
ENDPROC

///Creates a small hole object that's placed over the keyhole of the deposit box, so it looks like the drill has gone through it.
FUNC BOOL CREATE_DRILL_HOLE_OBJECT(VECTOR vPos, VECTOR vRot)
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[3])
		REQUEST_MODEL(HEI_PROP_HEI_DRILL_HOLE)
		IF HAS_MODEL_LOADED(HEI_PROP_HEI_DRILL_HOLE)
			IF NOT IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_DRILL_HOLE)
				MC_RESERVE_NETWORK_MISSION_OBJECTS(1)
				SET_BIT(iLocalBoolCheck12,LBOOL12_OBJ_RES_DRILL_HOLE)
			ENDIF
			IF IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_DRILL_HOLE)
				IF CAN_REGISTER_MISSION_OBJECTS(1)
					IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[3])
						IF CREATE_NET_OBJ(niMinigameObjects[3],HEI_PROP_HEI_DRILL_HOLE, vPos, bIsLocalPlayerHost)
							SET_ENTITY_COORDS_NO_OFFSET(NET_TO_OBJ(niMinigameObjects[3]), vPos)
							SET_ENTITY_ROTATION(NET_TO_OBJ(niMinigameObjects[3]), vRot)
							FREEZE_ENTITY_POSITION(NET_TO_OBJ(niMinigameObjects[3]), TRUE)
							SET_ENTITY_COLLISION(NET_TO_OBJ(niMinigameObjects[3]), FALSE)
							SET_ENTITY_INVINCIBLE(NET_TO_OBJ(niMinigameObjects[3]), TRUE)
							SET_NETWORK_ID_CAN_MIGRATE(niMinigameObjects[3], FALSE)
							SET_MODEL_AS_NO_LONGER_NEEDED(HEI_PROP_HEI_DRILL_HOLE)
							
							PRINTLN("[DRILLING] - [RCC MISSION] [CREATE_DRILL_HOLE_OBJECT] - Object created at ", vPos, " ", vRot)
							
							INTERIOR_INSTANCE_INDEX interiorCurrent
							INT iCurrentRoom
							
							interiorCurrent = GET_INTERIOR_FROM_ENTITY(LocalPlayerPed)
							iCurrentRoom = GET_ROOM_KEY_FROM_ENTITY(LocalPlayerPed)
							
							IF interiorCurrent != NULL
							AND iCurrentRoom != 0
								PRINTLN("[DRILLING] - [RCC MISSION] [CREATE_DRILL_HOLE_OBJECT] - Forcing room for object.")
							
								FORCE_ROOM_FOR_ENTITY(NET_TO_OBJ(niMinigameObjects[3]), interiorCurrent, iCurrentRoom)
							ENDIF
							
							CLEAR_BIT(iLocalBoolCheck12,LBOOL12_OBJ_RES_DRILL_HOLE)
							
							RETURN TRUE
						ENDIF
					ELSE
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC DELETE_DRILL_HOLE_OBJECT()
	OBJECT_INDEX tempObj
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[3])
		tempObj = NET_TO_OBJ(niMinigameObjects[3])
		
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(niMinigameObjects[3])
			PRINTLN("[DRILLING] [RCC MISSION] DELETE_DRILL_HOLE_OBJECT: deleted object.")
			DELETE_OBJECT(tempObj)
		ELSE
			PRINTLN("[DRILLING] [RCC MISSION] DELETE_DRILL_HOLE_OBJECT: set object as no longer needed.")
			CLEANUP_NET_ID(niMinigameObjects[3])
		ENDIF
		
		MC_RESERVE_NETWORK_MISSION_OBJECTS(-1)
	ENDIF
ENDPROC

FUNC BOOL HAVE_DRILL_WALL_AND_DOOR_MODELS_LOADED(BOOL bMakeDoor, BOOL bMakeWall)
	BOOL bReturn = TRUE
	IF bMakeWall 
	AND NOT	HAS_MODEL_LOADED(HEI_PROP_HEIST_SAFEDEPOSIT)
		bReturn = FALSE
	ENDIF
	IF bMakeDoor
	AND NOT HAS_MODEL_LOADED(HEI_PROP_HEIST_SAFEDEPDOOR)
		bReturn = FALSE
	ENDIF
	RETURN bReturn
	
ENDFUNC
/// PURPOSE:
///    Creates the wall for the drilling minigame.
FUNC BOOL CREATE_DRILL_WALL_AND_DOOR_OBJECT(VECTOR vPos, VECTOR vRot, VECTOR vPosDoor, VECTOR vRotDoor, INT iMinigameObj)
	BOOL bMakeDoor, bMakeWall
	INT iNumObjects
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_2.niDrillMinigameWall)		
		REQUEST_MODEL(HEI_PROP_HEIST_SAFEDEPOSIT)
		bMakeWall = TRUE
		iNumObjects++
	ENDIF
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_2.niDrillMinigameDoor)
		REQUEST_MODEL(HEI_PROP_HEIST_SAFEDEPDOOR)
		bMakeDoor = TRUE
		iNumObjects++
	ENDIF
	
	IF bMakeWall = FALSE
	AND bMakeDoor = FALSE
		RETURN TRUE
	ENDIF
	
	IF HAVE_DRILL_WALL_AND_DOOR_MODELS_LOADED(bMakeWall, bMakeDoor)
		IF NOT IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_DRILL_WALL_AND_DOOR )
			MC_RESERVE_NETWORK_MISSION_OBJECTS(iNumObjects)
			SET_BIT(iLocalBoolCheck12,LBOOL12_OBJ_RES_DRILL_WALL_AND_DOOR)
		ENDIF
		IF IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_DRILL_WALL_AND_DOOR )
			IF CAN_REGISTER_MISSION_OBJECTS(iNumObjects)
				IF bMakeWall
					IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_2.niDrillMinigameWall)
						IF CREATE_NET_OBJ(MC_serverBD_2.niDrillMinigameWall, HEI_PROP_HEIST_SAFEDEPOSIT, vPos, bIsLocalPlayerHost)
							SET_ENTITY_COORDS_NO_OFFSET(NET_TO_OBJ(MC_serverBD_2.niDrillMinigameWall), vPos)
							SET_ENTITY_ROTATION(NET_TO_OBJ(MC_serverBD_2.niDrillMinigameWall), vRot)
							FREEZE_ENTITY_POSITION(NET_TO_OBJ(MC_serverBD_2.niDrillMinigameWall), TRUE)
							SET_ENTITY_COLLISION(NET_TO_OBJ(MC_serverBD_2.niDrillMinigameWall), TRUE)
							SET_ENTITY_INVINCIBLE(NET_TO_OBJ(MC_serverBD_2.niDrillMinigameWall), TRUE)
							SET_MODEL_AS_NO_LONGER_NEEDED(HEI_PROP_HEIST_SAFEDEPOSIT)
							
							PRINTLN("[DRILLING] [RCC MISSION] [CREATE_DRILL_WALL_OBJECT] - Object created at ", vPos, " ", vRot)
							
							//2036823 - the wall is not appearing in the interior, for now hard code it in. If we want the drilling minigame to work elsewhere this will need to be redone.
							//INTERIOR_INSTANCE_INDEX interiorCurrent = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-2953.6, 484.5, 16.0>>, "hei_generic_bank_dlc")
							//FORCE_ROOM_FOR_ENTITY(NET_TO_OBJ(MC_serverBD_2.niDrillMinigameWall), interiorCurrent, HASH("bankvault"))
							
							//New version: doesn't require the interior to be streamed beforehand.
							INTERIOR_INSTANCE_INDEX interiorCurrent = GET_INTERIOR_AT_COORDS_WITH_TYPEHASH(<<-2953.6, 484.5, 16.0>>, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iMinigameObj].iInterior)
							CDEBUG1LN(DEBUG_MISSION, "[DRILLING] [RCC MISSION] [CREATE_DRILL_WALL_OBJECT] Retaining wall in interior, instance index = ", NATIVE_TO_INT(interiorCurrent))
							RETAIN_ENTITY_IN_INTERIOR(NET_TO_OBJ(MC_serverBD_2.niDrillMinigameWall), interiorCurrent)
						ENDIF
					ENDIF
				ENDIF
				
				IF bMakeDoor
					IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_2.niDrillMinigameDoor)
						IF CREATE_NET_OBJ(MC_serverBD_2.niDrillMinigameDoor, HEI_PROP_HEIST_SAFEDEPDOOR, vPosDoor, bIsLocalPlayerHost)
							SET_ENTITY_COORDS_NO_OFFSET(NET_TO_OBJ(MC_serverBD_2.niDrillMinigameDoor), vPosDoor)
							SET_ENTITY_ROTATION(NET_TO_OBJ(MC_serverBD_2.niDrillMinigameDoor), vRotDoor)
							FREEZE_ENTITY_POSITION(NET_TO_OBJ(MC_serverBD_2.niDrillMinigameDoor), TRUE)
							SET_ENTITY_COLLISION(NET_TO_OBJ(MC_serverBD_2.niDrillMinigameDoor), TRUE)
							SET_ENTITY_INVINCIBLE(NET_TO_OBJ(MC_serverBD_2.niDrillMinigameDoor), TRUE)
							SET_MODEL_AS_NO_LONGER_NEEDED(HEI_PROP_HEIST_SAFEDEPDOOR)
							
							PRINTLN("[DRILLING] [RCC MISSION] [CREATE_DRILL_DOOR_OBJECT] - Object created at ", vPosDoor, " ", vRotDoor)
							
							INTERIOR_INSTANCE_INDEX interiorCurrent = GET_INTERIOR_AT_COORDS_WITH_TYPEHASH(<<-2953.6, 484.5, 16.0>>, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iMinigameObj].iInterior)
							CDEBUG1LN(DEBUG_MISSION, "[DRILLING] [RCC MISSION] [CREATE_DRILL_WALL_OBJECT] Retaining door in interior, instance index = ", NATIVE_TO_INT(interiorCurrent))
							RETAIN_ENTITY_IN_INTERIOR(NET_TO_OBJ(MC_serverBD_2.niDrillMinigameDoor), interiorCurrent)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_2.niDrillMinigameWall)
	AND NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_2.niDrillMinigameDoor)
		CLEAR_BIT(iLocalBoolCheck12,LBOOL12_OBJ_RES_DRILL_WALL_AND_DOOR)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC DELETE_DRILL_WALL_OBJECT()
	OBJECT_INDEX tempObj
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_2.niDrillMinigameWall)
		tempObj = NET_TO_OBJ(MC_serverBD_2.niDrillMinigameWall)
		
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_2.niDrillMinigameWall)
			PRINTLN("[DRILLING] - [RCC MISSION] DELETE_DRILL_WALL_OBJECT: deleted object.")
			DELETE_OBJECT(tempObj)
		ELSE
			PRINTLN("[DRILLING] - [RCC MISSION] DELETE_DRILL_WALL_OBJECT: set object as no longer needed.")
			CLEANUP_NET_ID(MC_serverBD_2.niDrillMinigameWall)
		ENDIF
		
		MC_RESERVE_NETWORK_MISSION_OBJECTS(-1)
	ENDIF
ENDPROC

PROC DELETE_DRILL_DOOR_OBJECT()	
	OBJECT_INDEX tempObj
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_2.niDrillMinigameDoor)
		tempObj = NET_TO_OBJ(MC_serverBD_2.niDrillMinigameDoor)
		
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_2.niDrillMinigameDoor)
			PRINTLN("[DRILLING] - [RCC MISSION] DELETE_DRILL_DOOR_OBJECT: deleted object.")
			DELETE_OBJECT(tempObj)
		ELSE
			PRINTLN("[DRILLING] - [RCC MISSION] DELETE_DRILL_DOOR_OBJECT: set object as no longer needed.")
			CLEANUP_NET_ID(MC_serverBD_2.niDrillMinigameDoor)
		ENDIF
		
		MC_RESERVE_NETWORK_MISSION_OBJECTS(-1)
	ENDIF
ENDPROC

//Variables here moved to FM_Mission_Controller_USING header - search for vPlayAnimAdvancedPlayback

	///Creates the wall that's used for the drilling minigame: the wall is created at an offset to the given minigame object.
	///This has to be created in advance as the player can reach the drilling location before the rule has started.
	FUNC BOOL CREATE_DRILL_MINIGAME_PROPS(INT iObj)
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_2.niDrillMinigameWall)
		OR NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_2.niDrillMinigameDoor)
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iObj])
				OBJECT_INDEX objMinigame = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iObj])
			
				IF DOES_ENTITY_EXIST(objMinigame)
					VECTOR vDepositWallOffsetFromObj = <<0.33, -0.06, -0.01>>
					VECTOR vDepositDoorOffsetFromObj = <<-0.176, -0.245, -0.02>>
				
					//2036823 - The minigame object position seems to change when launching from the planning board. Currently the reason is unknown but reset it here and log that it changed.
					IF VDIST(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vPos, <<-2952.17, 484.632, 15.935>>) < 5.0
						VECTOR vObjPos = GET_ENTITY_COORDS(objMinigame)
						VECTOR vObjRot = GET_ENTITY_ROTATION(objMinigame)
						
						IF NOT ARE_VECTORS_ALMOST_EQUAL(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vPos, <<-2952.17, 484.632, 15.935>>, 0.01)
						OR NOT ARE_VECTORS_ALMOST_EQUAL(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vRot, <<0.0, 0.0, -92.458>>, 0.01)
							PRINTLN("[RCC MISSION] [DRILLING] Minigame object creator data has moved! ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vPos, " ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vRot)
							
							g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vPos = <<-2952.17, 484.632, 15.935>>
							g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vRot = <<0.0, 0.0, -92.458>>
							SET_ENTITY_COORDS(objMinigame, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vPos)
							SET_ENTITY_ROTATION(objMinigame, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vRot)
						ENDIF
						
						IF NOT ARE_VECTORS_ALMOST_EQUAL(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vPos, vObjPos, 0.01)
						OR NOT ARE_VECTORS_ALMOST_EQUAL(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vRot, vObjRot, 0.01)
							PRINTLN("[RCC MISSION] [DRILLING] Minigame object has moved from ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vPos, " ", 
									  g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vRot, " to ", vObjPos, " ", vObjRot)
							
							SET_ENTITY_COORDS(objMinigame, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vPos)
							SET_ENTITY_ROTATION(objMinigame, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vRot)
						ENDIF
					ENDIF
					
					CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] [DRILLING] Creating drill wall props, minigame obj pos = ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vPos)
					
					CREATE_DRILL_WALL_AND_DOOR_OBJECT(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objMinigame, vDepositWallOffsetFromObj), GET_ENTITY_ROTATION(objMinigame), 
													  GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objMinigame, vDepositDoorOffsetFromObj), GET_ENTITY_ROTATION(objMinigame), iobj)
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION] [DRILLING] Minigame object does not exist.")
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
		
		RETURN FALSE
	ENDFUNC

	///PURPOSE: Handles updates to the position and shaking of the camera while drilling.
	PROC UPDATE_DRILLING_CAMERA()
		CONST_FLOAT DRILL_CAM_ROT_SPEED 		45.0
		CONST_FLOAT DRILL_CAM_HEIGHT_SPEED 		1.0
		CONST_FLOAT DRILL_CAM_ROT_MIN			-30.0
		CONST_FLOAT DRILL_CAM_ROT_MAX			40.0
		CONST_FLOAT DRILL_CAM_HEIGHT_MIN		-0.1
		CONST_FLOAT DRILL_CAM_HEIGHT_MAX		0.6

		IF DOES_CAM_EXIST(sDrillData.cam)
			SET_USE_HI_DOF()
		
			//Apply shake based on drill power.
			sDrillData.fCurrentCamShake += ((0.1 + (sDrillData.fCurrentDrillPower * 0.9)) - sDrillData.fCurrentCamShake) * 0.15
			SET_CAM_SHAKE_AMPLITUDE(sDrillData.cam, sDrillData.fCurrentCamShake) 
			
			//Allow the player to rotate the camera around the player.
			VECTOR vPosDiff = sDrillData.vCamStartPos - sDrillData.vCamPointPos
			FLOAT fDesiredCamSpeedZ = -GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X) * DRILL_CAM_ROT_SPEED
			
			sDrillData.fDesiredCamRotOffset = sDrillData.fDesiredCamRotOffset +@ fDesiredCamSpeedZ
			
			IF sDrillData.fDesiredCamRotOffset > DRILL_CAM_ROT_MAX
				sDrillData.fDesiredCamRotOffset = DRILL_CAM_ROT_MAX
			ELIF sDrillData.fDesiredCamRotOffset < DRILL_CAM_ROT_MIN
				sDrillData.fDesiredCamRotOffset = DRILL_CAM_ROT_MIN
			ENDIF
			
			sDrillData.fCamRotOffset = COSINE_INTERP_FLOAT(sDrillData.fCamRotOffset, sDrillData.fDesiredCamRotOffset, 0.5)
			
			ROTATE_VECTOR_FMMC(vPosDiff, <<0.0, 0.0, sDrillData.fCamRotOffset>>)
			
			//Allow the player to move the camera up/down.
			FLOAT fDesiredCamSpeedHeight = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y) * DRILL_CAM_HEIGHT_SPEED
			
			IF IS_LOOK_INVERTED()
				fDesiredCamSpeedHeight = -fDesiredCamSpeedHeight
			ENDIF
			
			sDrillData.fDesiredCamHeightOffset = sDrillData.fDesiredCamHeightOffset +@ fDesiredCamSpeedHeight
			
			IF sDrillData.fDesiredCamHeightOffset > DRILL_CAM_HEIGHT_MAX
				sDrillData.fDesiredCamHeightOffset = DRILL_CAM_HEIGHT_MAX
			ELIF sDrillData.fDesiredCamHeightOffset < DRILL_CAM_HEIGHT_MIN
				sDrillData.fDesiredCamHeightOffset = DRILL_CAM_HEIGHT_MIN
			ENDIF
			
			sDrillData.fCamHeightOffset = COSINE_INTERP_FLOAT(sDrillData.fCamHeightOffset, sDrillData.fDesiredCamHeightOffset, 0.5)
			vPosDiff = vPosDiff + <<0.0, 0.0, sDrillData.fCamHeightOffset>>
			
			SET_CAM_COORD(sDrillData.cam, sDrillData.vCamPointPos + vPosDiff)
			
			//Set the minimap to replicate the cam rotation.
			VECTOR vCamRot = GET_CAM_ROT(sDrillData.cam)
			
			IF vCamRot.z < 0.0
				vCamRot.z += 360.0
			ENDIF
			
			LOCK_MINIMAP_ANGLE(ROUND(vCamRot.z))
			
			//SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			//DRAW_DEBUG_SPHERE(sDrillData.vCamPointPos, 0.05)
			
			//Debug widget for controlling DOF on the cam.
			#IF IS_DEBUG_BUILD
				IF fDebugDrillCamNearDofStart > 0.0
				AND fDebugDrillCamNearDofEnd > 0.0
				AND fDebugDrillCamFarDofStart > 0.0
				AND fDebugDrillCamFarDofEnd > 0.0
					SET_USE_HI_DOF()
					
					SET_CAM_DOF_PLANES(sDrillData.cam, fDebugDrillCamNearDofStart, fDebugDrillCamNearDofEnd, fDebugDrillCamFarDofStart, fDebugDrillCamFarDofEnd)
				ENDIF
			#ENDIF
		ENDIF
	ENDPROC

	/// PURPOSE:
	///    Handles playback of the drill bit spinning anim.
	/// PARAMS:
	///    fCurrentSpeed - The desired anim speed (0.0 - 1.0).
	PROC UPDATE_DRILL_BIT_ANIM(FLOAT fCurrentSpeed)
		IF NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[0])
			IF NOT IS_ENTITY_PLAYING_ANIM(NET_TO_OBJ(niMinigameObjects[0]), "anim@heists@fleeca_bank@drilling", "drill_straight_idle_drill_bit")
				PLAY_ENTITY_ANIM(NET_TO_OBJ(niMinigameObjects[0]), "drill_straight_idle_drill_bit", "anim@heists@fleeca_bank@drilling",
								 INSTANT_BLEND_IN, TRUE, FALSE, DEFAULT, DEFAULT)	
			ELSE
				SET_ENTITY_ANIM_SPEED(NET_TO_OBJ(niMinigameObjects[0]), "anim@heists@fleeca_bank@drilling", "drill_straight_idle_drill_bit", fCurrentSpeed)
			ENDIF
		ENDIF
	ENDPROC

	FUNC BOOL SHOULD_USE_ARMOUR_BAG_ANIMS()
		IF GET_PED_DRAWABLE_VARIATION(LocalPlayerPed, PED_COMP_SPECIAL2) = 0
			#IF IS_DEBUG_BUILD
				IF iDebugDrillOutfitCheckOverride > -1
					sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, iDebugDrillOutfitCheckOverride)
				ENDIF
			#ENDIF
		
			//Some thicker outfits will require the armoured version of the bag anims to play.
			HEIST_OUTFIT_SYTLE eStyle = GET_STYLE_FROM_OUTFIT(ENUM_TO_INT(sApplyOutfitData.eOutfit))
			
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] [SHOULD_USE_ARMOUR_BAG_ANIMS] eOutfit = ", ENUM_TO_INT(sApplyOutfitData.eOutfit), " eStyle = ", ENUM_TO_INT(eStyle))
			
			SWITCH eStyle
				//Every outfit in these styles should use the armoured anims.
				CASE OUTFIT_STYLE_HEAVY_COMBAT
				CASE OUTFIT_STYLE_CASUAL_STEALTH
				CASE OUTFIT_STYLE_CASUAL_PILOT
				CASE OUTFIT_STYLE_SMART_SUITS
				CASE OUTFIT_STYLE_SLOPPY_SUITS
				CASE OUTFIT_STYLE_SHARP_SUITS
				CASE OUTFIT_STYLE_MINIMAL_SUITS
				CASE OUTFIT_STYLE_TUXEDOS
				CASE OUTFIT_STYLE_REFUSE_COLLECTOR
				CASE OUTFIT_STYLE_NIGHT_BIKER
				CASE OUTFIT_STYLE_PRISON_OFFICER
				CASE OUTFIT_STYLE_COVERALLS
					CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] [SHOULD_USE_ARMOUR_BAG_ANIMS] outfit style needs armoured anims.")
				
					RETURN TRUE
				BREAK
				
				//Every outfit in these styles should use the unarmoured anims.
				CASE OUTFIT_STYLE_POLICE
				CASE OUTFIT_STYLE_PRISONER
				CASE OUTFIT_STYLE_TOURIST
				CASE OUTFIT_STYLE_HAZCHEM
					CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] [SHOULD_USE_ARMOUR_BAG_ANIMS] outfit style needs unarmoured anims.")
				
					RETURN FALSE
				BREAK
			
				//These styles have a mixture of outfits, some need the armoured anims and some don't.
				CASE OUTFIT_STYLE_STREET
				CASE OUTFIT_STYLE_CASUAL
				CASE OUTFIT_STYLE_DRIVER
				CASE OUTFIT_STYLE_STREET_COMBAT
				CASE OUTFIT_STYLE_TACTICAL_STEALTH
				CASE OUTFIT_STYLE_LIGHT_COMBAT
				CASE OUTFIT_STYLE_STEALTH_PILOT
				CASE OUTFIT_STYLE_FIGHTER_PILOT
					SWITCH sApplyOutfitData.eOutfit
						CASE OUTFIT_HEIST_STREET_2
						CASE OUTFIT_HEIST_CASUAL_1
						CASE OUTFIT_HEIST_STREET_COMBAT_0
						CASE OUTFIT_HEIST_STREET_COMBAT_2
						CASE OUTFIT_HEIST_LIGHT_COMBAT_2
						CASE OUTFIT_HEIST_TACTICAL_STEALTH_0
						CASE OUTFIT_HEIST_TACTICAL_STEALTH_2
						CASE OUTFIT_HEIST_DRIVER_0
						CASE OUTFIT_HEIST_DRIVER_1
						CASE OUTFIT_HEIST_DRIVER_2
						CASE OUTFIT_HEIST_FLIGHT_SUIT_0
						CASE OUTFIT_HEIST_FLIGHT_SUIT_1
						CASE OUTFIT_HEIST_FLIGHT_SUIT_3
							CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] [SHOULD_USE_ARMOUR_BAG_ANIMS] Specific outfit needs armoured anims.")
						
							RETURN TRUE
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		ELSE
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] [SHOULD_USE_ARMOUR_BAG_ANIMS] Player is wearing armour, use armoured anims.")
		
			//If armour is on then always play the armoured bag anims.
			RETURN TRUE
		ENDIF
		
		RETURN FALSE
		
	ENDFUNC
	
	FUNC BOOL SHOULD_USE_SUIT_BAG_ANIMS()

		#IF IS_DEBUG_BUILD
			IF iDebugDrillOutfitCheckOverride > -1
				sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, iDebugDrillOutfitCheckOverride)
			ENDIF
		#ENDIF

		//Some outfits will require the suit version of the bag anims to play.
		HEIST_OUTFIT_SYTLE eStyle = GET_STYLE_FROM_OUTFIT(ENUM_TO_INT(sApplyOutfitData.eOutfit))
		
		CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] [SHOULD_USE_SUIT_BAG_ANIMS] eOutfit = ", ENUM_TO_INT(sApplyOutfitData.eOutfit), " eStyle = ", ENUM_TO_INT(eStyle))
		
		SWITCH eStyle
			//Every outfit in these styles should use the suit anims.
			CASE OUTFIT_STYLE_SMART_SUITS
			CASE OUTFIT_STYLE_SLOPPY_SUITS
			CASE OUTFIT_STYLE_SHARP_SUITS
			CASE OUTFIT_STYLE_MINIMAL_SUITS
			CASE OUTFIT_STYLE_TUXEDOS
				CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] [SHOULD_USE_SUIT_BAG_ANIMS] outfit style needs armoured anims.")
			
				RETURN TRUE
			BREAK
		ENDSWITCH
		
		RETURN FALSE
		
	ENDFUNC

	/// PURPOSE: Manage drill minigame
	///    
	///    
	CONST_INT DRILL_STATE_INITIALISE			0
	CONST_INT DRILL_STATE_WAITING_FOR_ASSETS	1
	CONST_INT DRILL_STATE_STARTING_INTRO		2
	CONST_INT DRILL_STATE_REMOVE_BAG			3
	CONST_INT DRILL_STATE_WAITING_FOR_INTRO		4
	CONST_INT DRILL_STATE_DRILLING				5
	CONST_INT DRILL_STATE_WAITING_FOR_OUTRO		6
	CONST_INT DRILL_STATE_DO_BAG_SWAP			7
	CONST_INT DRILL_STATE_CLEANUP				8
	CONST_INT DRILL_STATE_MESSED_UP				99
	CONST_INT DRILL_STATE_BACK_OUT				100
	CONST_INT DRILL_STATE_FAILSAFE				101
	
	VECTOR vSceneNodeOffsetFromObj = <<-0.175, -0.24, -0.02>> 

	PROC PROCESS_DRILL_MINI_GAME(OBJECT_INDEX tempObj,INT iobj)
		#IF IS_DEBUG_BUILD
			INT iPrevHackProgress = iHackProgress
		#ENDIF
		
		VECTOR vDrillPTFXOffsetFromObj = <<0.074, -0.235, -0.02>>
		VECTOR vDrillPTFXRotation = <<90.0, 0.0, 0.0>>
		VECTOR vDrillHoleOffsetFromDoor = <<0.249, 0.0117, 0.0025>>
	
		IF MC_playerBD[iPartToUse].iObjNear = -1
			IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed,tempObj) <= 3.0
				SET_OBJ_NEAR(iObj)
			ENDIF
		ENDIF		
		
		/*IF MC_playerBD[iPartToUse].iObjHacked = -1
		AND iHackProgress < DRILL_STATE_WAITING_FOR_OUTRO
			CREATE_DRILL_MINIGAME_PROPS(iobj)
		ENDIF*/
		
		//Have the driller take control of the objects that were created at the start of the mission.
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
			IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
				NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
				CDEBUG1LN(DEBUG_MISSION, "[DRILLING] Driller is requesting control of creator object.")
			ENDIF
		ENDIF
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_2.niDrillMinigameWall)
			IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_2.niDrillMinigameWall)
				NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD_2.niDrillMinigameWall)
				CDEBUG1LN(DEBUG_MISSION, "[DRILLING] Driller is requesting control of deposit box wall.")
			ENDIF
		ENDIF
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_2.niDrillMinigameDoor)
			IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_2.niDrillMinigameDoor)
				NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD_2.niDrillMinigameDoor)
				CDEBUG1LN(DEBUG_MISSION, "[DRILLING] Driller is requesting control of deposit box door.")
			ENDIF
		ENDIF
	
		IF MC_playerBD[iPartToUse].iObjHacked = -1
			//2094394 - The drill wall is created at the start of the mission and seems to move slightly at some point before the 
			//minigame starts. If this happens we need to put it back in the correct spot.
			IF DOES_ENTITY_EXIST(tempObj) 
			AND iHackProgress < DRILL_STATE_DRILLING
				//First check that the minigame object matches the original creator position.
				IF (DOES_ENTITY_EXIST(tempObj) AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niObject[iobj]))
					VECTOR vObjPos = GET_ENTITY_COORDS(tempObj)
					VECTOR vObjRot = GET_ENTITY_ROTATION(tempObj)
					
					IF NOT ARE_VECTORS_ALMOST_EQUAL(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vPos, vObjPos, 0.01)
					OR NOT ARE_VECTORS_ALMOST_EQUAL(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vRot, vObjRot, 0.01)
						PRINTLN("[DRILLING] Creator object needs to be moved!")
						PRINTLN("[DRILLING] Creator position/rotation = ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vPos, " ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vRot)
						PRINTLN("[DRILLING] Current position/rotation = ", vObjPos, " ", vObjRot)
						
						SET_ENTITY_COORDS(tempObj, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vPos)
						SET_ENTITY_ROTATION(tempObj, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vRot)
					ENDIF
				ENDIF
				
				//Next check if the wall is in the correct place relative to the minigame object.
				IF (NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_2.niDrillMinigameWall) AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_2.niDrillMinigameWall))
					VECTOR vDepositWallOffsetFromObj = <<0.33, -0.06, -0.01>>
					VECTOR vWallPos = GET_ENTITY_COORDS(NET_TO_OBJ(MC_serverBD_2.niDrillMinigameWall))
					VECTOR vWallRot = GET_ENTITY_ROTATION(NET_TO_OBJ(MC_serverBD_2.niDrillMinigameWall))
					VECTOR vDesiredWallPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vPos, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vRot.z, vDepositWallOffsetFromObj)
					
					IF ABSF(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vRot.z - vWallRot.z) > 0.1
					OR NOT ARE_VECTORS_ALMOST_EQUAL(vDesiredWallPos, vWallPos, 0.005) //2239244 - The wall position needs to be accurate to less than a centimetre, otherwise it may leave the interior.
						PRINTLN("[DRILLING] Wall object needs to be moved!")
						PRINTLN("[DRILLING] Desired position/rotation = ", vDesiredWallPos, " ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vRot)
						PRINTLN("[DRILLING] Current position/rotation = ", vWallPos, " ", vWallRot)
						
						SET_ENTITY_COORDS_NO_OFFSET(NET_TO_OBJ(MC_serverBD_2.niDrillMinigameWall), vDesiredWallPos)
						SET_ENTITY_ROTATION(NET_TO_OBJ(MC_serverBD_2.niDrillMinigameWall), g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vRot)
						SET_NETWORK_ID_CAN_MIGRATE(MC_serverBD_2.niDrillMinigameWall, FALSE) //Once driller has control we want them to keep it.
						
						//2237023 - Add the object back into the interior in case it was removed when it moved out of position.
						INTERIOR_INSTANCE_INDEX interiorCurrent = GET_INTERIOR_AT_COORDS_WITH_TYPEHASH(<<-2953.6, 484.5, 16.0>>, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iInterior)
						CDEBUG1LN(DEBUG_MISSION, "[DRILLING] Retaining wall in interior, instance index = ", NATIVE_TO_INT(interiorCurrent))
						RETAIN_ENTITY_IN_INTERIOR(NET_TO_OBJ(MC_serverBD_2.niDrillMinigameWall), interiorCurrent)
					ENDIF
				ENDIF
				
				//Finally check that the door is in the correct place relative to the minigame object.
				IF (NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_2.niDrillMinigameDoor) AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_2.niDrillMinigameDoor))
					VECTOR vDepositDoorOffsetFromObj = <<-0.176, -0.245, -0.02>>
					VECTOR vDoorPos = GET_ENTITY_COORDS(NET_TO_OBJ(MC_serverBD_2.niDrillMinigameDoor))
					VECTOR vDoorRot = GET_ENTITY_ROTATION(NET_TO_OBJ(MC_serverBD_2.niDrillMinigameDoor))
					VECTOR vDesiredDoorPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vPos, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vRot.z, vDepositDoorOffsetFromObj)
					
					IF ABSF(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vRot.z - vDoorRot.z) > 0.1
					OR NOT ARE_VECTORS_ALMOST_EQUAL(vDesiredDoorPos, vDoorPos, 0.005)
						PRINTLN("[DRILLING] Door object needs to be moved!")
						PRINTLN("[DRILLING] Desired position/rotation = ", vDesiredDoorPos, " ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vRot)
						PRINTLN("[DRILLING] Current position/rotation = ", vDoorPos, " ", vDoorRot)
						
						SET_ENTITY_COORDS_NO_OFFSET(NET_TO_OBJ(MC_serverBD_2.niDrillMinigameDoor), vDesiredDoorPos)
						SET_ENTITY_ROTATION(NET_TO_OBJ(MC_serverBD_2.niDrillMinigameDoor), g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vRot)
						SET_NETWORK_ID_CAN_MIGRATE(MC_serverBD_2.niDrillMinigameDoor, FALSE)
						
						//2237023 - Add the object back into the interior in case it was removed when it moved out of position.
						INTERIOR_INSTANCE_INDEX interiorCurrent = GET_INTERIOR_AT_COORDS_WITH_TYPEHASH(<<-2953.6, 484.5, 16.0>>, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iInterior)
						CDEBUG1LN(DEBUG_MISSION, "[DRILLING] Retaining door in interior, instance index = ", NATIVE_TO_INT(interiorCurrent))
						RETAIN_ENTITY_IN_INTERIOR(NET_TO_OBJ(MC_serverBD_2.niDrillMinigameDoor), interiorCurrent)
					ENDIF
				ENDIF
			ENDIF
		
			IF MC_serverBD.iObjHackPart[iobj] = -1
				IF MC_playerBD[iPartToUse].iObjHacking = -1
					//Check if the player has triggered the minigame: do distance and button press check.
					IF bPlayerToUseOK
					AND NOT g_bMissionEnding
						REQUEST_DRILL_MINIGAME_ASSETS(sDrillData)
						
						//2152186 - Broadcast to spectators/remote players that the player is now on the drilling minigame and the assets need to be requested.
						IF NOT IS_BIT_SET(sDrillData.iBitset, DRILL_BITSET_HAS_BROADCAST_ASSET_REQUEST)
							BROADCAST_SCRIPT_EVENT_FMMC_DRILL_ASSET(ciEVENT_DRILL_ASSET_REQUEST)
							SET_BIT(sDrillData.iBitset, DRILL_BITSET_HAS_BROADCAST_ASSET_REQUEST)
						ENDIF
					
						IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed,tempObj) < 4.0
							VECTOR vDrillAAOffset = <<0,-1.8,0>>
							VECTOR vDrillGotoOffset = <<0,-0.75,0>>
							VECTOR vAACoordOb 
							VECTOR vAACoordPro
							vAACoordOb = GET_ENTITY_COORDS(tempObj)
							vAACoordOb.z = vAACoordOb.z + 2.0
							
							vAACoordPro = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj, vDrillAAOffset)
							vAACoordPro.z = vAACoordPro.z - 1.5

							vGoToPoint = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj, vDrillGotoOffset)
							
							DRAW_DEBUG_CROSS(vGoToPoint,0.5,0,0,255,255)
							
							IF IS_ENTITY_IN_ANGLED_AREA( LocalPlayerPed, vAACoordOb, vAACoordPro, 5.000000)
								IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
									IF NOT IS_PHONE_ONSCREEN()
										IF NOT IS_CUTSCENE_PLAYING()
										AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitset2, PBBOOL2_PLAYING_MOCAP_CUTSCENE)
										AND NOT g_bCelebrationScreenIsActive
										AND IS_GAMEPLAY_CAM_RENDERING()
										AND GET_SKYFREEZE_STAGE() != SKYFREEZE_FROZEN
											DISPLAY_HELP_TEXT_THIS_FRAME_FOR_MINIGAME("MC_DRILL_1",TRUE)
											
											IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
												MC_playerBD[iPartToUse].iObjHacking = iobj
												CLEAR_BIT(MC_playerBD[iPartToUse].iHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
												CLEAR_BIT(sDrillData.iBitset, DRILL_BITSET_HAS_BROADCAST_ASSET_REQUEST)
												NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_ALLOW_PLAYER_DAMAGE | NSPC_PREVENT_EVERYBODY_BACKOFF | NSPC_LEAVE_CAMERA_CONTROL_ON)
												
												RESET_NET_TIMER(tdHackTimer)
												iHackLimitTimer = 0
												
												IF NOT HAS_MINIGAME_TIMER_BEEN_INITIALISED(1)
													PRINTLN("[RCC MISSION][TELEMETRY] HACKING (DRILLING) - INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY - SLOT 1")
													PRINTLN("[RCC MISSION][TELEMETRY] HACKING (DRILLING) - START MINIGAME TIMER - SLOT 1")							
													INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY(1)
													START_MINIGAME_TIMER_FOR_TELEMETRY(1)
												ELSE
													PRINTLN("[RCC MISSION][TELEMETRY] HACKING (DRILLING) VALUES NOT INITIALISED - TIMER ALREADY STARTED")
												ENDIF
												
												iHackProgress = DRILL_STATE_INITIALISE
												PRINTLN("[RCC MISSION] [DRILLING] local player has started drilling, part: ",iPartToUse)
											ENDIF
										ELSE
											IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MC_DRILL_1")
												CLEAR_HELP()
											ENDIF
										ENDIF
									ELSE
										IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MC_DRILL_1")
											CLEAR_HELP()
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					//If we've failed but haven't triggered the minigame yet then quit.
					IF IS_BIT_SET(MC_playerBD[iPartToUse].iHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
						TASK_CLEAR_LOOK_AT(LocalPlayerPed)
						CLEAR_BIT(MC_playerBD[iPartToUse].iHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
						RESET_OBJ_HACKING_INT()
						IF NOT IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
							RELEASE_SCRIPT_AUDIO_BANK()
							PRINTLN("RELEASE_SCRIPT_AUDIO_BANK - Called from fm_mission_controller_minigame.sch 1")
						ENDIF
						FORCE_QUIT_FAIL_DRILL_MINIGAME_KEEP_DATA(sDrillData)
						CLEAR_PED_TASKS(LocalPlayerPed)
						NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
						SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed, TRUE)
						ENABLE_INTERACTION_MENU()
						Unpause_Objective_Text()
						PRINTLN("[RCC MISSION] [DRILLING] local player has failed drilling, part: ",iPartToUse)
						iHackProgress++
						iHackLimitTimer = 0
						iHackProgress = DRILL_STATE_INITIALISE
					ENDIF
				ENDIF
			ELSE
				IF MC_playerBD[iPartToUse].iObjHacking != -1
					IF MC_serverBD.iObjHackPart[iobj] = iPartToUse
						//2159869 - If the mission fails while this player is drilling then force a cleanup.
						IF (NOT bPlayerToUseOK) OR g_bMissionEnding
							IF iHackProgress != DRILL_STATE_CLEANUP
							AND iHackProgress != DRILL_STATE_DO_BAG_SWAP
							AND iHackProgress != DRILL_STATE_FAILSAFE
								//2216351 - Make sure all cleanup is done in a single frame if a fail triggers, as the drill logic
								//may not be run again due to the broadcast flags getting wiped on fail.
								
								//NOTE: there's a bag pop due to it streaming in a few frames after the fail, 
								//as long as the bag is given back on a retry we can avoid giving it here and stop the pop.
								//APPLY_DUFFEL_BAG_HEIST_GEAR(LocalPlayerPed, iLocalPart)
							
								IF bLocalPlayerPedOk
									CLEAR_PED_TASKS_IMMEDIATELY(LocalPlayerPed)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed)
								ENDIF
							
								PRINTLN("[RCC MISSION] [DRILLING] Mission failed, forcing cleanup.")
								iHackProgress = DRILL_STATE_CLEANUP
							ENDIF
						ENDIF
					
						IF iHackProgress > DRILL_STATE_INITIALISE
							DRAW_DEBUG_CROSS(vGoToPoint,0.5,0,0,255,255)
							
							#IF IS_DEBUG_BUILD
								IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
									PRINTLN("[RCC MISSION] [DRILLING] Player J-skipped drilling minigame.")
								
									APPLY_DUFFEL_BAG_HEIST_GEAR(LocalPlayerPed, iLocalPart)
									SET_BIT(sDrillData.iBitset, DRILL_BITSET_IS_MINIGAME_COMPLETE)
									iHackProgress = DRILL_STATE_DO_BAG_SWAP
								ENDIF
							#ENDIF
						ENDIF

						//Block the player from spinning the camera while in the drilling minigame.
						IF iHackProgress >= DRILL_STATE_STARTING_INTRO
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
							
							//2074382 - Expand the player's capsule to stop other players walking into the drill.
							IF IS_TASK_MOVE_NETWORK_ACTIVE(LocalPlayerPed)
								//SET_PED_DESIRED_HEADING(LocalPlayerPed, GET_ENTITY_HEADING(LocalPlayerPed))
								SET_PED_CAPSULE(LocalPlayerPed, 0.54)
							ENDIF
						ENDIF
						
						DISABLE_CELLPHONE_THIS_FRAME_ONLY()

						SWITCH iHackProgress
							CASE DRILL_STATE_INITIALISE //Begin requesting assets, lock the player into the game and fetch the coord the player needs to go to.
								IF DOES_ENTITY_EXIST(tempObj)
									NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_ALLOW_PLAYER_DAMAGE | NSPC_PREVENT_EVERYBODY_BACKOFF | NSPC_LEAVE_CAMERA_CONTROL_ON)
									REINIT_NET_TIMER(tdHackTimer)
									REQUEST_DRILL_MINIGAME_ASSETS(sDrillData)
									INITIALISE_MINIGAME_VALUES(sDrillData)
									START_AUDIO_SCENE("DLC_HEIST_MINIGAME_FLEECA_DRILLING_SCENE")
									iHackProgress = DRILL_STATE_WAITING_FOR_ASSETS
									
									IF NOT HAS_NET_TIMER_STARTED(stMGTimer0)
										REINIT_NET_TIMER(stMGTimer0)
									ENDIF

									IF sDrillData.iDrillSoundID = -1
										sDrillData.iDrillSoundID = GET_SOUND_ID()
									ENDIF
									
									//Output the wall and door positions in case something has gone wrong.
									#IF IS_DEBUG_BUILD
										VECTOR vDebugWallPos, vDebugDoorPos
									
										IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_2.niDrillMinigameWall)
											vDebugWallPos = GET_ENTITY_COORDS(NET_TO_OBJ(MC_serverBD_2.niDrillMinigameWall))
										ENDIF
										
										IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_2.niDrillMinigameDoor)
											vDebugDoorPos = GET_ENTITY_COORDS(NET_TO_OBJ(MC_serverBD_2.niDrillMinigameDoor))
										ENDIF
										
										PRINTLN("[DRILLING] Minigame has started, wall pos = ", vDebugWallPos, ", door pos =  ", vDebugDoorPos)
									#ENDIF
								ENDIF
							BREAK
							
							CASE DRILL_STATE_WAITING_FOR_ASSETS //Wait for the assets to load, once ready then task the player into the right spot.
								FLOAT fGroundZTemp
							
								REQUEST_DRILL_MINIGAME_ASSETS(sDrillData)
							
								IF HAVE_DRILL_ASSETS_LOADED(sDrillData)									
									//Task the player to walk to where they will be when the anim triggers.
									IF bLocalPlayerPedOk
									AND NOT IS_PED_PERFORMING_MELEE_ACTION(LocalPlayerPed)
									AND NOT IS_PED_RAGDOLL(LocalPlayerPed)
									AND NOT IS_PED_RUNNING(LocalPlayerPed)
									AND NOT IS_PED_JUMPING(LocalPlayerPed)
										vPlayAnimAdvancedPlayback = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj, vSceneNodeOffsetFromObj)
										fObjRot = GET_ENTITY_HEADING(tempObj)
										vGoToPoint = GET_ANIM_INITIAL_OFFSET_POSITION("anim@heists@fleeca_bank@drilling", "intro", vPlayAnimAdvancedPlayback, <<0.0, 0.0, fObjRot>>)
										GET_GROUND_Z_FOR_3D_COORD(vGoToPoint + <<0.0, 0.0, 0.5>>, fGroundZTemp)
										vGoToPoint.z = fGroundZTemp
									
										NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_ALLOW_PLAYER_DAMAGE | NSPC_PREVENT_EVERYBODY_BACKOFF | NSPC_LEAVE_CAMERA_CONTROL_ON)
										CLEAR_PED_TASKS(LocalPlayerPed)
										GET_CURRENT_PED_WEAPON(LocalPlayerPed, eWeaponBeforeMinigame)
										
										IF VDIST(GET_ENTITY_COORDS(LocalPlayerPed), vGoToPoint + <<0.0, 0.0, 1.0>>) >= 0.9
										
											SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED, TRUE)
										
											SEQUENCE_INDEX seq				
											OPEN_SEQUENCE_TASK(seq)
												PRINTLN("[AW_MISSION] - [RCC MISSION] Distance between player and goto", GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(LocalPlayerPed), vGoToPoint, FALSE))
												TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vGoToPoint, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, 0.1, ENAV_STOP_EXACTLY, fObjRot)
												PRINTLN("[AW_MISSION] - [RCC MISSION] Too far away using goto ")
											CLOSE_SEQUENCE_TASK(seq)
											TASK_PERFORM_SEQUENCE(LocalPlayerPed, seq)
											CLEAR_SEQUENCE_TASK(seq)
											
											CLEAR_BIT(sDrillData.iBitset, DRILL_BITSET_SKIP_WALK_TO_POSITION)
										ELSE
											SET_BIT(sDrillData.iBitset, DRILL_BITSET_SKIP_WALK_TO_POSITION)
										ENDIF
									
										PRINTLN("[DRILLING] - [RCC MISSION] vGoTo: ", vGoToPoint)
										PRINTLN("[DRILLING] - [RCC MISSION] Heading: ", fObjRot)
		
										REINIT_NET_TIMER(tdHackTimer)
										iSafetyTimer = GET_GAME_TIMER()
										iHackProgress = DRILL_STATE_STARTING_INTRO
									ENDIF
								ELSE
									PRINTLN("[RCC MISSION] [DRILLING] WAITING FOR HAVE_DRILL_ASSETS_LOADED")
								ENDIF
							BREAK
							
							CASE DRILL_STATE_STARTING_INTRO //Begin the drilling intro.
								CREATE_DRILL_BAG_OBJECT(TRUE)
								CREATE_DRILL_OBJECT(TRUE)
							
								VECTOR vInitialRot
								FLOAT fDistFromAnimStart, fHeadingDiffFromAnimStart
								vInitialRot = GET_ANIM_INITIAL_OFFSET_ROTATION("anim@heists@fleeca_bank@drilling", "intro", vPlayAnimAdvancedPlayback, <<0.0, 0.0, fObjRot>>)
								fDistFromAnimStart = VDIST(GET_ENTITY_COORDS(LocalPlayerPed, FALSE), vGoToPoint + <<0.0, 0.0, 1.0>>)
								fHeadingDiffFromAnimStart = ABSF(GET_ENTITY_HEADING(LocalPlayerPed) - vInitialRot.z)
								
								IF fHeadingDiffFromAnimStart > 180.0
									fHeadingDiffFromAnimStart -= 360.0
								ENDIF
							
								//sDrillData.iDebugNumPrintsThisFrame = 0
								//DRAW_FLOAT_TO_DRILL_DEBUG(sDrillData, fDistFromAnimStart, "fDistFromAnimStart")
								//DRAW_FLOAT_TO_DRILL_DEBUG(sDrillData, fHeadingDiffFromAnimStart, "fHeadingDiffFromAnimStart")
							
								IF GET_SCRIPT_TASK_STATUS(LocalPlayerPed,SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
								OR IS_BIT_SET(sDrillData.iBitset, DRILL_BITSET_SKIP_WALK_TO_POSITION) //2092239 - Just start the anims if the player is near the box.
								OR (fDistFromAnimStart < 0.2 AND ABSF(fHeadingDiffFromAnimStart) < 10.0)
								OR MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer,6000)
									IF CREATE_DRILL_OBJECT(TRUE)
									AND CREATE_DRILL_BAG_OBJECT(TRUE)		
										TASK_MOVE_NETWORK_ADVANCED_BY_NAME(LocalPlayerPed, "minigame_drilling", vPlayAnimAdvancedPlayback, <<0.0, 0.0, fObjRot>>, DEFAULT,
																		   0.5, DEFAULT, "anim@heists@fleeca_bank@drilling", MOVE_USE_KINEMATIC_PHYSICS)
										//This makes sure the move network phase is in sync with the bag: bForceZeroTimestep needs to be TRUE otherwise the move network ends up ahead.
										FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed, DEFAULT, TRUE) 
										//2119705 - Freezing the player stops issues with physics affecting the move network and causing the player to not line up with the door.
										FREEZE_ENTITY_POSITION(LocalPlayerPed, TRUE)
										
										FREEZE_ENTITY_POSITION(NET_TO_OBJ(niMinigameObjects[1]), FALSE)
										sDrillData.iSyncScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vPlayAnimAdvancedPlayback, <<0.0, 0.0, fObjRot>>, DEFAULT, TRUE)
										IF SHOULD_USE_ARMOUR_BAG_ANIMS()
											NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(niMinigameObjects[1]), sDrillData.iSyncScene,
																		  			"anim@heists@fleeca_bank@drilling", "bag_intro", INSTANT_BLEND_IN, WALK_BLEND_OUT)
										ELSE
											NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(niMinigameObjects[1]), sDrillData.iSyncScene,
																		  			"anim@heists@fleeca_bank@drilling", "bag_intro_no_armour", INSTANT_BLEND_IN, WALK_BLEND_OUT)	
										ENDIF										
										NETWORK_ADD_SYNCHRONISED_SCENE_CAMERA(sDrillData.iSyncScene, "anim@heists@fleeca_bank@drilling", "intro_cam")
										NETWORK_FORCE_LOCAL_USE_OF_SYNCED_SCENE_CAMERA(sDrillData.iSyncScene)
										NETWORK_START_SYNCHRONISED_SCENE(sDrillData.iSyncScene)
										
										SET_PED_PROOFS_FOR_MINIGAME(LocalPlayerPed)
										Pause_Objective_Text()
										
										iSafetyTimer = GET_GAME_TIMER()
										DISABLE_INTERACTION_MENU() 
										iHackProgress = DRILL_STATE_REMOVE_BAG
									ENDIF
								ENDIF
							BREAK
							
							CASE DRILL_STATE_REMOVE_BAG //The bag component needs to be removed before we make the bag object visible, otherwise we get two bags in a frame.
								IF NOT (IS_SCRIPTED_CONVERSATION_ONGOING() AND IS_SUBTITLE_PREFERENCE_SWITCHED_ON() AND IS_MESSAGE_BEING_DISPLAYED())
									HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)
								ENDIF
								HIDE_HUD_AND_RADAR_THIS_FRAME()
								THEFEED_HIDE_THIS_FRAME() 
							
								IF IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sDrillData.iSyncScene))
								OR (NOT IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sDrillData.iSyncScene)) AND MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer, 500))
									//2080153 - Clear area of bombs when starting the minigame.
									CLEAR_AREA_OF_PROJECTILES(GET_ENTITY_COORDS(LocalPlayerPed), 3.0, TRUE)
									
									//Hide player's weapon.
									//If we skipped the walk then we need to remove the weapon when the scene starts to prevent pops (normally it's removed during the walk).
									IF IS_BIT_SET(sDrillData.iBitset, DRILL_BITSET_SKIP_WALK_TO_POSITION)
										SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED, TRUE)
									ENDIF
									SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed, FALSE, FALSE)
								
									//2239244 - Failsafe in case the wall object isn't in the interior: force it again just as the minigame starts.
									INTERIOR_INSTANCE_INDEX interiorPlayer, interiorObject
									INT iPlayerRoom, iObjectRoom
									
									interiorPlayer = GET_INTERIOR_FROM_ENTITY(LocalPlayerPed)
									iPlayerRoom = GET_ROOM_KEY_FROM_ENTITY(LocalPlayerPed)
									
									IF interiorPlayer != NULL
									AND iPlayerRoom != 0
										IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_2.niDrillMinigameWall)
											interiorObject = GET_INTERIOR_FROM_ENTITY(NET_TO_OBJ(MC_serverBD_2.niDrillMinigameWall))
											iObjectRoom = GET_ROOM_KEY_FROM_ENTITY(NET_TO_OBJ(MC_serverBD_2.niDrillMinigameWall))
										ENDIF
									
										IF interiorPlayer != interiorObject
										OR iPlayerRoom != iObjectRoom
											CDEBUG1LN(DEBUG_MISSION, "[DRILLING] Wall object is not in interior, forcing room.")
											CDEBUG1LN(DEBUG_MISSION, "[DRILLING] interiorPlayer = ", NATIVE_TO_INT(interiorPlayer))
											CDEBUG1LN(DEBUG_MISSION, "[DRILLING] interiorObject = ", NATIVE_TO_INT(interiorObject))
											CDEBUG1LN(DEBUG_MISSION, "[DRILLING] iPlayerRoom = ", iPlayerRoom)
											CDEBUG1LN(DEBUG_MISSION, "[DRILLING] iObjectRoom = ", iObjectRoom)
											
											FORCE_ROOM_FOR_ENTITY(NET_TO_OBJ(MC_serverBD_2.niDrillMinigameWall), interiorPlayer, iPlayerRoom)
										ENDIF
									ENDIF
								
									REMOVE_PLAYER_BAG()
									iHackProgress = DRILL_STATE_WAITING_FOR_INTRO
								ENDIF
							BREAK
							
							CASE DRILL_STATE_WAITING_FOR_INTRO //Wait for the intro anim to end, then start drilling.
								HIDE_HUD_AND_RADAR_THIS_FRAME()
								THEFEED_HIDE_THIS_FRAME() 

								//Now is a good time to create the drill hole object: hide it under the ground for later.
								CREATE_DRILL_HOLE_OBJECT(GET_ENTITY_COORDS(LocalPlayerPed) + <<0.0, 0.0, -5.0>>, <<0.0, 0.0, 0.0>>)

								//Handle making the drill visible: needs to be synced with the player pulling the drill out the bag.
								IF IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sDrillData.iSyncScene))
									//Do the bag swap once the scene is playing and the component bag is gone.
									IF NOT IS_ENTITY_VISIBLE_TO_SCRIPT(NET_TO_OBJ(niMinigameObjects[1]))
									AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(LocalPlayerPed) 
										SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[1]), TRUE)
									ENDIF
									
									IF GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sDrillData.iSyncScene)) >= 0.36
										SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[0]), TRUE)
									ENDIF
								ENDIF
							
								IF (IS_TASK_MOVE_NETWORK_ACTIVE(LocalPlayerPed) AND GET_TASK_MOVE_NETWORK_EVENT(LocalPlayerPed, "IntroFinished"))
								OR MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer, 12000)
									IF IS_TASK_MOVE_NETWORK_ACTIVE(LocalPlayerPed)
									AND IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(LocalPlayerPed) 
										IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(LocalPlayerPed, "Cutting")
											//Set the props to be visible again just in case something happened with the anim.
											IF NOT IS_ENTITY_VISIBLE_TO_SCRIPT(NET_TO_OBJ(niMinigameObjects[1]))
												REMOVE_PLAYER_BAG()
												SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[1]), TRUE)
											ENDIF
											
											SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[0]), TRUE) 
											
											NETWORK_STOP_SYNCHRONISED_SCENE(sDrillData.iSyncScene)											
											sDrillData.iSyncScene = -1
											
											//The anim offset won't work with the bag attached, instead we need to calculate the anim offset and attach it at that offset manually.
											//Anim offsets from Malcolm: position = x -0.071451 y 0.197707 z -0.052787 		rotation = x 12.861300 y 14.866415 z -8.128334 
											/*VECTOR vPlayerRootPos, vPlayerRootRot, vBagPos, vBagRot, vBagPosOffset, vBagRotOffset
											vPlayerRootPos = GET_ENTITY_COORDS(LocalPlayerPed)
											vPlayerRootRot = GET_ENTITY_ROTATION(LocalPlayerPed)
											vBagPos = GET_ANIM_INITIAL_OFFSET_POSITION("anim@heists@fleeca_bank@drilling", "bag_drill_loop", vPlayerRootPos, vPlayerRootRot)
											vBagRot = GET_ANIM_INITIAL_OFFSET_ROTATION("anim@heists@fleeca_bank@drilling", "bag_drill_loop", vPlayerRootPos, vPlayerRootRot)
											vBagPosOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(LocalPlayerPed, vBagPos)
											vBagRotOffset = vBagRot - vPlayerRootRot
											
											PRINTLN("[AW_MISSION] - [RCC MISSION] Attaching drill bag, vPlayerRootPos = ", vPlayerRootPos)
											PRINTLN("[AW_MISSION] - [RCC MISSION] Attaching drill bag, vPlayerRootRot = ", vPlayerRootRot)
											PRINTLN("[AW_MISSION] - [RCC MISSION] Attaching drill bag, vBagPos = ", vBagPos)
											PRINTLN("[AW_MISSION] - [RCC MISSION] Attaching drill bag, vBagRot = ", vBagRot)
											PRINTLN("[AW_MISSION] - [RCC MISSION] Attaching drill bag, vBagPosOffset = ", vBagPosOffset)
											PRINTLN("[AW_MISSION] - [RCC MISSION] Attaching drill bag, vBagRotOffset = ", vBagRotOffset)*/
											
											ATTACH_ENTITY_TO_ENTITY(NET_TO_OBJ(niMinigameObjects[1]), LocalPlayerPed, GET_PED_BONE_INDEX(LocalPlayerPed, BONETAG_PH_L_HAND), 
																	<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 
																	DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
											IF SHOULD_USE_ARMOUR_BAG_ANIMS()
												PLAY_ENTITY_ANIM(NET_TO_OBJ(niMinigameObjects[1]), "bag_drill_straight_idle", "anim@heists@fleeca_bank@drilling",
																 INSTANT_BLEND_IN, TRUE, FALSE, DEFAULT, DEFAULT)
											ELSE
												PLAY_ENTITY_ANIM(NET_TO_OBJ(niMinigameObjects[1]), "bag_straight_idle_no_armour", "anim@heists@fleeca_bank@drilling",
																 INSTANT_BLEND_IN, TRUE, FALSE, DEFAULT, DEFAULT)
											ENDIF
											FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(NET_TO_OBJ(niMinigameObjects[1]))									

											sDrillData.cam = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
											sDrillData.fCamRotOffset = 0.0
											sDrillData.fDesiredCamRotOffset = 0.0
											sDrillData.fCamHeightOffset = 0.0
											sDrillData.fDesiredCamHeightOffset = 0.0
											//Old position: changed for 2133226
											//sDrillData.vCamStartPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(LocalPlayerPed, <<0.4396, -0.5454, 0.6720>>)
											//sDrillData.vCamPointPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(LocalPlayerPed, <<0.2494, 0.2582, 0.3885>>)
											sDrillData.vCamStartPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(LocalPlayerPed, <<0.9193, -0.5807, 0.0869>>)
											sDrillData.vCamPointPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(LocalPlayerPed, <<0.1376, 0.4819, 0.4162>>)
											sDrillData.fCurrentCamShake = 0.1
											SET_CAM_COORD(sDrillData.cam, sDrillData.vCamStartPos)
											POINT_CAM_AT_COORD(sDrillData.cam, sDrillData.vCamPointPos)
											SET_CAM_FOV(sDrillData.cam, 39.9877)
											SHAKE_CAM(sDrillData.cam, "HAND_SHAKE", sDrillData.fCurrentCamShake)
											SET_USE_HI_DOF()
											SET_CAM_DOF_PLANES(sDrillData.cam, 0.0, 0.0, 1.645, 2.070)
											RENDER_SCRIPT_CAMS(TRUE, FALSE)											
											Unpause_Objective_Text()
									
											//Call the update procedure a frame early to allow the HUD to draw in time with the camera cut.
											RUN_DRILL_MINIGAME(sDrillData)
											DISPLAY_HELP_TEXT_THIS_FRAME_FOR_MINIGAME("MC_DRILL_3",TRUE)
											iHackProgress = DRILL_STATE_DRILLING
										ENDIF
									ENDIF
								ENDIF
								
								//If we haven't switched to the gameplay camera yet then hide the objective text.
								IF iHackProgress != DRILL_STATE_DRILLING
								AND NOT (IS_SCRIPTED_CONVERSATION_ONGOING() AND IS_SUBTITLE_PREFERENCE_SWITCHED_ON() AND IS_MESSAGE_BEING_DISPLAYED())
									HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)
								ENDIF
								
								//If the player loses their move network task then reset.
								IF NOT IS_TASK_MOVE_NETWORK_ACTIVE(LocalPlayerPed)
								AND MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer, 500)
									DELETE_DRILL_OBJECT()
									iSafetyTimer = GET_GAME_TIMER()
									iHackProgress = DRILL_STATE_FAILSAFE
								ENDIF
							BREAK
							
							CASE DRILL_STATE_DRILLING //Run the actual minigame.
								RUN_DRILL_MINIGAME(sDrillData)
								UPDATE_DRILLING_CAMERA()
								
								sDrillData.fDrillBitAnimSpeed = sDrillData.fDrillBitAnimSpeed + ((sDrillData.fCurrentDrillPower - sDrillData.fDrillBitAnimSpeed) * 0.8)
								UPDATE_DRILL_BIT_ANIM(sDrillData.fDrillBitAnimSpeed)
								
								//Set the weights for the drilling move network. Network games are limited to 2 signals per frame so we need to stagger them.
								IF IS_TASK_MOVE_NETWORK_ACTIVE(LocalPlayerPed)
									//SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(LocalPlayerPed, "x_axis", 1.0 - ((sDrillData.fCurrentLeanPos * 0.5) + 0.5))
								
									SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(LocalPlayerPed, "z_axis", sDrillData.fCurrentForwardLeanPos + sDrillData.fForwardLeanExtraAmountForPins)
									SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(LocalPlayerPed, "drill_force", 1.0 - sDrillData.fCurrentDrillAnimPower)
								ENDIF
								
								//2077048 - Create the drill box under the floor in advance so it has time to load for remote players.
								IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[2])
									CREATE_DRILL_BOX_OBJECT(GET_ENTITY_COORDS(tempObj) - <<0.0, 0.0, 3.0>>, GET_ENTITY_ROTATION(tempObj))
								ENDIF
								
								//Attach the drill hole object to the door once the player has drilled a little bit into the lock.
								IF NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[3])
								AND NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_2.niDrillMinigameDoor)
									OBJECT_INDEX objDrillHole
									objDrillHole = NET_TO_OBJ(niMinigameObjects[3])
								
									IF NOT IS_ENTITY_ATTACHED(objDrillHole)
									AND sDrillData.fCurrentProgress > DRILL_START_POINT
										FREEZE_ENTITY_POSITION(objDrillHole, FALSE)
										ATTACH_ENTITY_TO_ENTITY(objDrillHole, NET_TO_OBJ(MC_serverBD_2.niDrillMinigameDoor), 0, vDrillHoleOffsetFromDoor, <<0.0, 0.0, 0.0>>)
										CDEBUG1LN(DEBUG_MISSION, "[DRILLING] Attaching drill hole object to door.")
									ENDIF
								ENDIF
								
								//If we win then play an outro, if we screw up then play a fail anim.
								IF HAS_PLAYER_BEAT_DRILL_MINIGAME(sDrillData)
									IF IS_TASK_MOVE_NETWORK_ACTIVE(LocalPlayerPed)
									AND IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(LocalPlayerPed) 
										//IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(LocalPlayerPed, "Success")	
											//This makes sure the move network phase is in sync with the bag: bForceZeroTimestep needs to be TRUE otherwise the move network ends up ahead.
											//NOTE: commented out for 2197790 (see below).
											//FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed, DEFAULT, TRUE) 
										
											//The box is being treated as outside the interior, so force the right interior room here.
											INTERIOR_INSTANCE_INDEX interiorCurrent
											INT iCurrentRoom
											
											interiorCurrent = GET_INTERIOR_FROM_ENTITY(LocalPlayerPed)
											iCurrentRoom = GET_ROOM_KEY_FROM_ENTITY(LocalPlayerPed)
										
											IF interiorCurrent != NULL
											AND iCurrentRoom != 0
												FORCE_ROOM_FOR_ENTITY(NET_TO_OBJ(niMinigameObjects[2]), interiorCurrent, iCurrentRoom)
											ENDIF
										
											//Bag has to be detached before starting the scene.
											DETACH_ENTITY(NET_TO_OBJ(niMinigameObjects[1]))
										
											//Setup the outro scene.
											sDrillData.iSyncScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vPlayAnimAdvancedPlayback, <<0.0, 0.0, fObjRot>>, DEFAULT, TRUE)
											//2197790 - The move network and synced scene don't always sync on remote machines, so the player's outro anim is now just played as part of the synced scene
											//(instead of using the move network 'Success' transition).
											NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(LocalPlayerPed, sDrillData.iSyncScene, "anim@heists@fleeca_bank@drilling", "outro", 
																				  INSTANT_BLEND_IN, NORMAL_BLEND_OUT,
																				  SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_HIDE_WEAPON | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON | SYNCED_SCENE_ABORT_ON_DEATH, 
										  										  RBF_PLAYER_IMPACT)
											IF SHOULD_USE_ARMOUR_BAG_ANIMS()
												NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(niMinigameObjects[1]), sDrillData.iSyncScene,
																			  			"anim@heists@fleeca_bank@drilling", "bag_outro", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
											ELSE
												NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(niMinigameObjects[1]), sDrillData.iSyncScene,
																			  			"anim@heists@fleeca_bank@drilling", "bag_outro_no_armour", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
											ENDIF
											NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(niMinigameObjects[2]), sDrillData.iSyncScene,
																		  			"anim@heists@fleeca_bank@drilling", "safety_boxoutro", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
											NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(MC_serverBD_2.niDrillMinigameDoor), sDrillData.iSyncScene,
																		  			"anim@heists@fleeca_bank@drilling", "outro_door", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
											NETWORK_ADD_SYNCHRONISED_SCENE_CAMERA(sDrillData.iSyncScene, "anim@heists@fleeca_bank@drilling", "outro_cam")
											NETWORK_FORCE_LOCAL_USE_OF_SYNCED_SCENE_CAMERA(sDrillData.iSyncScene)										
											NETWORK_START_SYNCHRONISED_SCENE(sDrillData.iSyncScene)
										
											IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_2.niDrillMinigameDoor)
												USE_PARTICLE_FX_ASSET("FM_Mission_Controler")
												START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_drill_out", NET_TO_OBJ(MC_serverBD_2.niDrillMinigameDoor), <<0.25, 0.0, 0.0>>, vDrillPTFXRotation)
												CDEBUG1LN(DEBUG_MISSION, "[DRILLING] Drill door exists, playing particle effect attached to door.")
											ELSE
												USE_PARTICLE_FX_ASSET("FM_Mission_Controler")
												START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_drill_out", tempObj, vDrillPTFXOffsetFromObj, vDrillPTFXRotation)
												CDEBUG1LN(DEBUG_MISSION, "[DRILLING] Drill door does not exist, playing particle effect at coord.")
											ENDIF
										
											Pause_Objective_Text()
											sDrillData.fDrillBitAnimSpeed = 1.0
											iSafetyTimer = GET_GAME_TIMER()
											iHackProgress = DRILL_STATE_WAITING_FOR_OUTRO
										//ENDIF
									ENDIF
								ELSE
									//Work out which transition to play and play it.
									IF HAS_PLAYER_MESSED_UP_DRILLING(sDrillData)
										IF IS_TASK_MOVE_NETWORK_ACTIVE(LocalPlayerPed)
										AND IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(LocalPlayerPed) 	
											//NOTE: leaning is currenly disabled, the left fail anim looks better than the right one so just use that every time.
											/*BOOL bTransitionReady
											
											IF DID_PLAYER_MESS_UP_DRILLING_ON_LEFT_SIDE(sDrillData)
												bTransitionReady = REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(LocalPlayerPed, "LeftFailure")
											ELSE
												bTransitionReady = REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(LocalPlayerPed, "RightFailure")
											ENDIF*/
										
											IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(LocalPlayerPed, "LeftFailure")
												SET_EXPECTED_CLONE_NEXT_TASK_MOVE_NETWORK_STATE(LocalPlayerPed, "Cutting")
												USE_PARTICLE_FX_ASSET("FM_Mission_Controler")
												START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_drill_out", tempObj, vDrillPTFXOffsetFromObj, vDrillPTFXRotation)
											
												PLAY_SOUND_FROM_ENTITY(-1, "Drill_Jam", LocalPlayerPed, "DLC_HEIST_FLEECA_SOUNDSET", TRUE, 20)
											
												iSafetyTimer = GET_GAME_TIMER()
												INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY(1)
												PRINTLN("[RCC MISSION][TELEMETRY] HACKING (DRILLING) - INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY - SLOT 1")
												
												iHackProgress = DRILL_STATE_MESSED_UP
												sDrillData.fDrillBitAnimSpeed = 1.0
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								//2077289 - This feature has been removed, however keep it for debugging purposes only.
								//Allow the player to back out if they haven't messed up or passed yet.
								#IF IS_DEBUG_BUILD
									IF iHackProgress = DRILL_STATE_DRILLING
									AND sDrillData.bDisplayDebug
										IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
											IF IS_TASK_MOVE_NETWORK_ACTIVE(LocalPlayerPed)
											AND IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(LocalPlayerPed) 
												IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(LocalPlayerPed, "Quit")	
													//This makes sure move network phase is in sync with the bag: bForceZeroTimestep needs to be TRUE otherwise the move network ends up ahead.
													FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed, DEFAULT, TRUE) 
												
													//Play the exit scene on the bag.
													DETACH_ENTITY(NET_TO_OBJ(niMinigameObjects[1]))
													sDrillData.iSyncScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vPlayAnimAdvancedPlayback, <<0.0, 0.0, fObjRot>>, DEFAULT, TRUE)
													IF SHOULD_USE_ARMOUR_BAG_ANIMS()
														NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(niMinigameObjects[1]), sDrillData.iSyncScene,
																					  			"anim@heists@fleeca_bank@drilling", "bag_exit", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
													ELSE
														NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(niMinigameObjects[1]), sDrillData.iSyncScene,
																					  			"anim@heists@fleeca_bank@drilling", "bag_exit_no_armour", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
													ENDIF										
													NETWORK_ADD_SYNCHRONISED_SCENE_CAMERA(sDrillData.iSyncScene, "anim@heists@fleeca_bank@drilling", "exit_cam")
													NETWORK_FORCE_LOCAL_USE_OF_SYNCED_SCENE_CAMERA(sDrillData.iSyncScene)
													NETWORK_START_SYNCHRONISED_SCENE(sDrillData.iSyncScene)
												
													Pause_Objective_Text()
													iSafetyTimer = GET_GAME_TIMER()
													iHackProgress = DRILL_STATE_BACK_OUT
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								#ENDIF
								
								//If the player loses their move network task then reset.
								IF NOT IS_TASK_MOVE_NETWORK_ACTIVE(LocalPlayerPed)
									DELETE_DRILL_OBJECT()
									iSafetyTimer = GET_GAME_TIMER()
									iHackProgress = DRILL_STATE_FAILSAFE
								ENDIF
								
								//Update the drill audio: the drill is silent when no power is applied, if power is applied the sound depends on whether the drill is in the sweet spot or not.
								IF sDrillData.iDrillSoundID != -1
									IF sDrillData.fCurrentDrillPower = 0.0
									OR iHackProgress = DRILL_STATE_MESSED_UP
										IF NOT HAS_SOUND_FINISHED(sDrillData.iDrillSoundID)
											STOP_SOUND(sDrillData.iDrillSoundID)
										ENDIF
									ELSE
										IF HAS_SOUND_FINISHED(sDrillData.iDrillSoundID)
											// Changed range to 0 , see 2228844
											PLAY_SOUND_FROM_ENTITY(sDrillData.iDrillSoundID, "Drill", LocalPlayerPed, "DLC_HEIST_FLEECA_SOUNDSET", TRUE, 0)
										ENDIF
									
										IF sDrillData.fCurrentDrillPosY < sDrillData.fCurrentSweetSpotMin
											SET_VARIABLE_ON_SOUND(sDrillData.iDrillSoundID, "DrillState", 0.0)
										ELIF sDrillData.fCurrentDrillPosY <= sDrillData.fCurrentSweetSpotMin
											SET_VARIABLE_ON_SOUND(sDrillData.iDrillSoundID, "DrillState", 0.5)
										ELSE
											SET_VARIABLE_ON_SOUND(sDrillData.iDrillSoundID, "DrillState", 1.0)
										ENDIF
									ENDIF 
								ENDIF
								
								//Update drill particle effects (currently placeholder).
								//First effect: debris that appears when successfully drilling
								IF DrillptfxIndex = NULL
									USE_PARTICLE_FX_ASSET("FM_Mission_Controler")
									DrillptfxIndex = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY("scr_drill_debris", tempObj, vDrillPTFXOffsetFromObj, vDrillPTFXRotation)
									SET_PARTICLE_FX_LOOPED_EVOLUTION(DrillptfxIndex, "power", 0.0)
								ELSE
									IF sDrillData.fCurrentDrillPosY >= sDrillData.fCurrentSweetSpotMin
									AND sDrillData.fCurrentDrillPosY <= sDrillData.fCurrentSweetSpotMax
										SET_PARTICLE_FX_LOOPED_EVOLUTION(DrillptfxIndex, "power", sDrillData.fCurrentDrillPower)
									ELSE
										SET_PARTICLE_FX_LOOPED_EVOLUTION(DrillptfxIndex, "power", 0.0)
									ENDIF
								ENDIF
								
								//Second effect: plays when the drill is overheating.
								IF OverheatptfxIndex = NULL
									USE_PARTICLE_FX_ASSET("FM_Mission_Controler")
									OverheatptfxIndex = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY("scr_drill_overheat", tempObj, vDrillPTFXOffsetFromObj, vDrillPTFXRotation)
									SET_PARTICLE_FX_LOOPED_EVOLUTION(OverheatptfxIndex, "heat", 0.0)
								ELSE
									IF sDrillData.fCurrentDrillPosY > sDrillData.fCurrentSweetSpotMax
									AND sDrillData.fCurrentDrillPower > 0.0
										SET_PARTICLE_FX_LOOPED_EVOLUTION(OverheatptfxIndex, "heat", sDrillData.fCurrentHeatLevel)
									ELSE
										SET_PARTICLE_FX_LOOPED_EVOLUTION(OverheatptfxIndex, "heat", 0.0)
									ENDIF
								ENDIF
								
								//Help text: display every frame except if we've just triggered a state change or if something went wrong that would affect the drilling minigame.
								IF iHackProgress = DRILL_STATE_DRILLING
								AND IS_SAFE_TO_PLAY_DRILL_MINIGAME()
								AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitset2, PBBOOL2_PLAYING_MOCAP_CUTSCENE)
									DISPLAY_HELP_TEXT_THIS_FRAME_FOR_MINIGAME("MC_DRILL_3",TRUE)
								ENDIF
							BREAK
							
							CASE DRILL_STATE_WAITING_FOR_OUTRO //Wait for the outro to finish and clean up.
								HIDE_HUD_AND_RADAR_THIS_FRAME()
								THEFEED_HIDE_THIS_FRAME() 
								IF NOT (IS_SCRIPTED_CONVERSATION_ONGOING() AND IS_SUBTITLE_PREFERENCE_SWITCHED_ON() AND IS_MESSAGE_BEING_DISPLAYED())
									HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)
								ENDIF
							
								IF DOES_CAM_EXIST(sDrillData.cam)
									DESTROY_CAM(sDrillData.cam)
									RENDER_SCRIPT_CAMS(FALSE, FALSE)
								ENDIF
								
								IF DrillptfxIndex!= NULL
									STOP_PARTICLE_FX_LOOPED(DrillptfxIndex)
									DrillptfxIndex = NULL
								ENDIF
								
								IF OverheatptfxIndex != NULL
									STOP_PARTICLE_FX_LOOPED(OverheatptfxIndex)
									OverheatptfxIndex = NULL
								ENDIF
								
								IF sDrillData.iDrillSoundID != -1
									IF NOT HAS_SOUND_FINISHED(sDrillData.iDrillSoundID)
										STOP_SOUND(sDrillData.iDrillSoundID)
									ENDIF
								ENDIF
								
								//Drill bit speed is set to 1.0 at the start of this sequence, gradually bring it down to zero.
								sDrillData.fDrillBitAnimSpeed = sDrillData.fDrillBitAnimSpeed * 0.75
								UPDATE_DRILL_BIT_ANIM(sDrillData.fDrillBitAnimSpeed)
								
								//Handle deleting the drill and the safety deposit box: both need to be synched with when they go inside the bag.
								IF IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sDrillData.iSyncScene))
									IF GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sDrillData.iSyncScene)) >= 0.38
										DELETE_DRILL_OBJECT()
									ENDIF
									
									//NOTE: Can't delete the box as it breaks the synchronized scene, make it invisible instead and delete later.
									IF GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sDrillData.iSyncScene)) >= 0.68
										SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[2]), FALSE)
									ENDIF
								ENDIF
			
								PRELOAD_DUFFEL_BAG_HEIST_GEAR(LocalPlayerPed, iLocalPart)
			
								IF (IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sDrillData.iSyncScene))
								AND GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sDrillData.iSyncScene)) >= 0.9)
								OR MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer, 12000)
									APPLY_DUFFEL_BAG_HEIST_GEAR(LocalPlayerPed, iLocalPart)
									
									UNLOCK_MINIMAP_ANGLE()
									SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
									SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
									
									iSafetyTimer = GET_GAME_TIMER()
									iHackProgress = DRILL_STATE_DO_BAG_SWAP
								ENDIF
							BREAK
							
							CASE DRILL_STATE_DO_BAG_SWAP //Safety stage to make sure the bag swap is done correctly (currently the bag component doesn't appear straight away even when preloaded).
								IF NOT (IS_SCRIPTED_CONVERSATION_ONGOING() AND IS_SUBTITLE_PREFERENCE_SWITCHED_ON() AND IS_MESSAGE_BEING_DISPLAYED())
									HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)
								ENDIF
							
								IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(LocalPlayerPed)
								OR MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer, 2000)
									IF IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sDrillData.iSyncScene))
										NETWORK_STOP_SYNCHRONISED_SCENE(sDrillData.iSyncScene)
									ENDIF
									sDrillData.iSyncScene = -1
								
									DELETE_DRILL_OBJECT()
									DELETE_DRILL_BOX_OBJECT()
									DELETE_DRILL_BAG_OBJECT()
									
									FREEZE_ENTITY_POSITION(LocalPlayerPed, FALSE)
									
									//IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
										CLEAR_PED_TASKS_IMMEDIATELY(LocalPlayerPed)
										FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed)
									//ELSE
									//	CLEAR_PED_TASKS(LocalPlayerPed)
									//ENDIF
									
									//This should only get hit if the mission failed while drilling, during normal flow the camera has already been destroyed.
									IF DOES_CAM_EXIST(sDrillData.cam)
										DESTROY_CAM(sDrillData.cam)
										RENDER_SCRIPT_CAMS(FALSE, FALSE)
									ENDIF
								
									IF IS_BIT_SET(sDrillData.iBitset, DRILL_BITSET_IS_MINIGAME_COMPLETE)
										PRINTLN("[DRILLING] - [RCC MISSION] DRILL_STATE_DO_BAG_SWAP - Tasking player to turn towards exit.")
										TASK_TURN_PED_TO_FACE_COORD(LocalPlayerPed, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(LocalPlayerPed, <<4.0, 0.0, 0.0>>))
									ENDIF
								
									iHackProgress = DRILL_STATE_CLEANUP
								ELSE
									HIDE_HUD_AND_RADAR_THIS_FRAME()
									THEFEED_HIDE_THIS_FRAME() 
								ENDIF
							BREAK

							CASE DRILL_STATE_CLEANUP //Final cleanup: can enter this state either by passing or backing out of the game. If we backed out we need to be able to retrigger.
								IF NOT (IS_SCRIPTED_CONVERSATION_ONGOING() AND IS_SUBTITLE_PREFERENCE_SWITCHED_ON() AND IS_MESSAGE_BEING_DISPLAYED())
									HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)
								ENDIF
								
								IF sDrillData.iSyncScene != -1
									IF IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sDrillData.iSyncScene))
										NETWORK_STOP_SYNCHRONISED_SCENE(sDrillData.iSyncScene)
									ENDIF
									sDrillData.iSyncScene = -1
								ENDIF
							
								RESET_OBJ_HACKING_INT()
							
								IF IS_BIT_SET(sDrillData.iBitset, DRILL_BITSET_IS_MINIGAME_COMPLETE)
									MC_playerBD[iPartToUse].iObjHacked = iobj
									STORE_MINIGAME_ATTEMPTS_FOR_TELEMETRY(1)
									STORE_MINIGAME_TIMER_FOR_TELEMETRY(1)
									PRINTLN("[RCC MISSION][TELEMETRY] HACKING (DRILL) - STORE_MINIGAME_ATTEMPTS_FOR_TELEMETRY - SLOT 1")
									PRINTLN("[RCC MISSION][TELEMETRY] HACKING (DRILL) - STORE_MINIGAME_TIMER_FOR_TELEMETRY( - SLOT 1")
									
									MC_PlayerBD[iPartToUse].iHackTime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stMGTimer0)
									MC_playerBD[iPartToUse].iNumHacks++
									PRINTLN("[RCC MISSION] inumHacks HEIST TELEMETRY DRILLING MINI GAME  -", MC_playerBD[iPartToUse].iNumHacks)
									CPRINTLN(DEBUG_MISSION, "DRILL_BITSET_IS_MINIGAME_COMPLETE - TRUE - FORCE_QUIT_PASS_DRILL_MINIGAME_KEEP_DATA")
									FORCE_QUIT_PASS_DRILL_MINIGAME_KEEP_DATA(sDrillData, TRUE, FALSE, !IS_BIT_SET( MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_CC_FAILCUT_FINISH ) AND !g_bMissionEnding)
								ELSE
									MC_serverBD.iObjHackPart[iobj] = -1
									CPRINTLN(DEBUG_MISSION, "DRILL_BITSET_IS_MINIGAME_COMPLETE - FALSE - FORCE_QUIT_PASS_DRILL_MINIGAME_KEEP_DATA")
									FORCE_QUIT_PASS_DRILL_MINIGAME_KEEP_DATA(sDrillData, FALSE, FALSE, !IS_BIT_SET( MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_CC_FAILCUT_FINISH ) AND !g_bMissionEnding)
								ENDIF
								
								//Normally the minigame objects will have been deleted before this point, but do it again just in case.
								IF DrillptfxIndex!= NULL
									STOP_PARTICLE_FX_LOOPED(DrillptfxIndex)
									DrillptfxIndex = NULL
								ENDIF
								
								IF OverheatptfxIndex != NULL
									STOP_PARTICLE_FX_LOOPED(OverheatptfxIndex)
									OverheatptfxIndex = NULL
								ENDIF
								
								DELETE_DRILL_BOX_OBJECT()
								DELETE_DRILL_OBJECT()
								DELETE_DRILL_BAG_OBJECT()
								
								RESET_NET_TIMER(tdHackTimer)
								ENABLE_INTERACTION_MENU()
								BROADCAST_SCRIPT_EVENT_FMMC_DRILL_ASSET(ciEVENT_DRILL_ASSET_CLEANUP)
								Unpause_Objective_Text()
								
								STOP_AUDIO_SCENE("DLC_HEIST_MINIGAME_FLEECA_DRILLING_SCENE")
								
								IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_2.niDrillMinigameDoor)
									FREEZE_ENTITY_POSITION(NET_TO_OBJ(MC_serverBD_2.niDrillMinigameDoor), TRUE)
								ENDIF
								
								//This should only get hit if the mission failed while drilling, during normal flow the camera has already been destroyed.
								IF DOES_CAM_EXIST(sDrillData.cam)
									DESTROY_CAM(sDrillData.cam)
									RENDER_SCRIPT_CAMS(FALSE, FALSE)
								ENDIF
								
								UNLOCK_MINIMAP_ANGLE()
								
								IF bLocalPlayerPedOk
									FREEZE_ENTITY_POSITION(LocalPlayerPed, FALSE)
									SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed,TRUE,FALSE)
									IF NOT IS_BIT_SET( MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_CC_FAILCUT_FINISH )
									AND NOT g_bMissionEnding
										NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
									ENDIF
									RELEASE_PED_PRELOAD_VARIATION_DATA(LocalPlayerPed)
									RUN_LOCAL_PLAYER_PROOF_SETTING(TRUE)
									
									IF eWeaponBeforeMinigame != WEAPONTYPE_UNARMED
									AND HAS_PED_GOT_WEAPON(LocalPlayerPed, eWeaponBeforeMinigame)
										SET_CURRENT_PED_WEAPON(LocalPlayerPed, eWeaponBeforeMinigame)
									ENDIF
								ENDIF
								
								IF sDrillData.iDrillSoundID != -1
									IF NOT HAS_SOUND_FINISHED(sDrillData.iDrillSoundID)
										STOP_SOUND(sDrillData.iDrillSoundID)
									ENDIF
									
									RELEASE_SOUND_ID(sDrillData.iDrillSoundID)
									sDrillData.iDrillSoundID = -1
								ENDIF
								
								SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sDrillData.sfDrillHud)
								
								IF NOT IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
									PRINTLN("RELEASE_SCRIPT_AUDIO_BANK - Called from fm_mission_controller_minigame.sch 2")
									RELEASE_SCRIPT_AUDIO_BANK()
								ENDIF

								REMOVE_PTFX_ASSET()
								iHackLimitTimer = 0
								iHackProgress = DRILL_STATE_INITIALISE
							BREAK
							
							CASE DRILL_STATE_MESSED_UP //Fail case: once the fail anim has finished then reset and begin the game again.
								RUN_DRILL_MINIGAME(sDrillData)
								UPDATE_DRILLING_CAMERA()
								DISPLAY_HELP_TEXT_THIS_FRAME_FOR_MINIGAME("MC_DRILL_4",TRUE)
								
								IF DrillptfxIndex!= NULL
									STOP_PARTICLE_FX_LOOPED(DrillptfxIndex)
									DrillptfxIndex = NULL
								ENDIF
								
								IF OverheatptfxIndex != NULL
									STOP_PARTICLE_FX_LOOPED(OverheatptfxIndex)
									OverheatptfxIndex = NULL
								ENDIF
								
								//Drill bit speed is set to 1.0 at the start of this sequence, gradually bring it down to zero.
								sDrillData.fDrillBitAnimSpeed = sDrillData.fDrillBitAnimSpeed * 0.85
								UPDATE_DRILL_BIT_ANIM(sDrillData.fDrillBitAnimSpeed)
								
								IF IS_TASK_MOVE_NETWORK_ACTIVE(LocalPlayerPed)
								OR MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer,6000)
									//Leaning is currently disabled, we always trigger the left fail now so only check for that event.
									/*BOOL bEventFired
									
									IF DID_PLAYER_MESS_UP_DRILLING_ON_LEFT_SIDE(sDrillData)
										bEventFired = GET_TASK_MOVE_NETWORK_EVENT(LocalPlayerPed, "LeftFailFinish")
									ELSE
										bEventFired = GET_TASK_MOVE_NETWORK_EVENT(LocalPlayerPed, "RightFailFinish")
									ENDIF*/
								
									IF GET_TASK_MOVE_NETWORK_EVENT(LocalPlayerPed, "LeftFailFinish")
										IF IS_TASK_MOVE_NETWORK_ACTIVE(LocalPlayerPed)
										AND IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(LocalPlayerPed) 
											IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(LocalPlayerPed, "Cutting")
												sDrillData.iCurrentState = DRILL_MINIGAME_STATE_INITIALISE
												iHackProgress = DRILL_STATE_DRILLING
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							BREAK
							
							CASE DRILL_STATE_BACK_OUT //Player chose to back out of the drill minigame: wait for the back out anim to finish then clean up.
								HIDE_HUD_AND_RADAR_THIS_FRAME()
								THEFEED_HIDE_THIS_FRAME() 
								IF NOT (IS_SCRIPTED_CONVERSATION_ONGOING() AND IS_SUBTITLE_PREFERENCE_SWITCHED_ON() AND IS_MESSAGE_BEING_DISPLAYED())
									HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)
								ENDIF
							
								IF DOES_CAM_EXIST(sDrillData.cam)
									DESTROY_CAM(sDrillData.cam)
									RENDER_SCRIPT_CAMS(FALSE, FALSE)
								ENDIF
							
								IF sDrillData.iDrillSoundID != -1
									IF NOT HAS_SOUND_FINISHED(sDrillData.iDrillSoundID)
										STOP_SOUND(sDrillData.iDrillSoundID)
									ENDIF
								ENDIF

								//Fix for 2236982, after submission.
								IF DrillptfxIndex!= NULL
									STOP_PARTICLE_FX_LOOPED(DrillptfxIndex)
									DrillptfxIndex = NULL
								ENDIF
								
								IF OverheatptfxIndex != NULL
									STOP_PARTICLE_FX_LOOPED(OverheatptfxIndex)
									OverheatptfxIndex = NULL
								ENDIF
							
								//Handle deleting the drill: needs to be synched with when they go inside the bag.
								IF IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sDrillData.iSyncScene))
									IF GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sDrillData.iSyncScene)) >= 0.32
										DELETE_DRILL_OBJECT()
									ENDIF
								ENDIF
			
								PRELOAD_DUFFEL_BAG_HEIST_GEAR(LocalPlayerPed, iLocalPart)
			
								IF (IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sDrillData.iSyncScene))
								AND GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sDrillData.iSyncScene)) >= 0.98)
								OR MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer, 8000)
									APPLY_DUFFEL_BAG_HEIST_GEAR(LocalPlayerPed, iLocalPart)
									SET_PLAYER_BACKED_OUT_OF_DRILLING(sDrillData)
									
									UNLOCK_MINIMAP_ANGLE()
									SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
									
									iSafetyTimer = GET_GAME_TIMER()
									iHackProgress = DRILL_STATE_DO_BAG_SWAP
								ENDIF
							BREAK
							
							CASE DRILL_STATE_FAILSAFE //Enter this case if something goes wrong (i.e. the player is knocked out of their move network tasks, or the mission failed while drilling).
								HIDE_HUD_AND_RADAR_THIS_FRAME()
								THEFEED_HIDE_THIS_FRAME() 
							
								APPLY_DUFFEL_BAG_HEIST_GEAR(LocalPlayerPed, iLocalPart)
								iSafetyTimer = GET_GAME_TIMER()
								iHackProgress = DRILL_STATE_DO_BAG_SWAP
							BREAK
						ENDSWITCH
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF iPrevHackProgress != iHackProgress
				PRINTLN("[DRILLING] - [RCC MISSION] iHackProgress changed from ", iPrevHackProgress, " to ", iHackProgress)
			ENDIF
			
			IF IS_KEYBOARD_KEY_PRESSED(KEY_SPACE)
			OR IS_KEYBOARD_KEY_PRESSED(KEY_RBRACKET)
				VECTOR vDrillWallPos, vDrillDoorPos, vDrillWallRot, vDrillDoorRot
			
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_2.niDrillMinigameWall)
					vDrillWallPos = GET_ENTITY_COORDS(NET_TO_OBJ(MC_serverBD_2.niDrillMinigameWall))
					vDrillWallRot = GET_ENTITY_ROTATION(NET_TO_OBJ(MC_serverBD_2.niDrillMinigameWall))
				ENDIF
				
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_2.niDrillMinigameDoor)
					vDrillDoorPos = GET_ENTITY_COORDS(NET_TO_OBJ(MC_serverBD_2.niDrillMinigameDoor))
					vDrillDoorRot = GET_ENTITY_ROTATION(NET_TO_OBJ(MC_serverBD_2.niDrillMinigameDoor))
				ENDIF
			
				CDEBUG1LN(DEBUG_MISSION, "[DRILLING] Dumping drilling state.")
				CDEBUG1LN(DEBUG_MISSION, "[DRILLING] vDrillWallPos = ", vDrillWallPos)
				CDEBUG1LN(DEBUG_MISSION, "[DRILLING] vDrillWallRot = ", vDrillWallRot)
				CDEBUG1LN(DEBUG_MISSION, "[DRILLING] vDrillDoorPos = ", vDrillDoorPos)
				CDEBUG1LN(DEBUG_MISSION, "[DRILLING] vDrillDoorRot = ", vDrillDoorRot)
				CDEBUG1LN(DEBUG_MISSION, "[DRILLING] iHackProgress = ", iHackProgress)
			ENDIF
		#ENDIF
	ENDPROC




//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: VAULT DRILLING MINIGAME !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************


PROC CORRECT_VAULT_DRILL_OBJECT_IF_INCORRECTLY_PLACED(OBJECT_INDEX tempObj, INT iobj)

	IF DOES_ENTITY_EXIST(tempObj) 
		AND iHackProgress < VAULT_DRILL_STATE_DRILLING
		//First check that the minigame object matches the original creator position.
		IF (DOES_ENTITY_EXIST(tempObj) AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niObject[iobj]))
			VECTOR vObjPos = GET_ENTITY_COORDS(tempObj)
			VECTOR vObjRot = GET_ENTITY_ROTATION(tempObj)
			
			IF NOT ARE_VECTORS_ALMOST_EQUAL(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vPos, vObjPos, 0.01)
			OR NOT ARE_VECTORS_ALMOST_EQUAL(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vRot, vObjRot, 0.01)
				SET_ENTITY_COORDS(tempObj, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vPos)
				SET_ENTITY_ROTATION(tempObj, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vRot)
			ENDIF
		ENDIF

	ENDIF
	
ENDPROC


PROC FORCE_VAULT_DRILL_MINIGAME_TO_RESET_IF_FAILED_BEFORE_TRIGGER()
	//If we've failed but haven't triggered the minigame yet then quit.

	IF IS_BIT_SET(MC_playerBD[iPartToUse].iHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
		TASK_CLEAR_LOOK_AT(LocalPlayerPed)
		CLEAR_BIT(MC_playerBD[iPartToUse].iHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
		RESET_OBJ_HACKING_INT()
		
		IF NOT IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
			RELEASE_SCRIPT_AUDIO_BANK()
			PRINTLN("RELEASE_SCRIPT_AUDIO_BANK - Called from fm_mission_controller_minigame.sch 1")
		ENDIF

		FORCE_QUIT_FAIL_VAULT_DRILL_MINIGAME_KEEP_DATA(sVaultDrillData)
		CLEAR_PED_TASKS(LocalPlayerPed)
		NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
		SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed, TRUE)
		ENABLE_INTERACTION_MENU()
		Unpause_Objective_Text()
		PRINTLN("[RCC MISSION] [VAULT_DRILLING] local player has failed drilling, part: ",iPartToUse)

		iHackLimitTimer = 0
		iHackProgress = VAULT_DRILL_STATE_INITIALISE
	ENDIF				
ENDPROC



PROC HANDLE_VAULT_DRILL_TRIGGER_AND_PROMPT(OBJECT_INDEX tempObj, INT iobj)
	
	IF iHackProgress >  VAULT_DRILL_STATE_BEGIN
		EXIT
	ENDIF

	IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed,tempObj) < 3.0
		VECTOR vDrillAAOffset = <<0,-1.8,0>>
		VECTOR vDrillGotoOffset = <<0,-0.75,0>>
		VECTOR vAACoordOb 
		VECTOR vAACoordPro
		vAACoordOb = GET_ENTITY_COORDS(tempObj)
		vAACoordOb.z = vAACoordOb.z + 2.0
		
		vAACoordPro = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj, vDrillAAOffset)
		vAACoordPro.z = vAACoordPro.z - 1.5

		vGoToPoint = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj, vDrillGotoOffset)
		
		IF IS_ENTITY_IN_ANGLED_AREA( LocalPlayerPed, vAACoordOb, vAACoordPro, 1.2)
			IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				IF NOT IS_PHONE_ONSCREEN()
					IF NOT IS_CUTSCENE_PLAYING()
					AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitset2, PBBOOL2_PLAYING_MOCAP_CUTSCENE)
					AND NOT g_bCelebrationScreenIsActive
					AND IS_GAMEPLAY_CAM_RENDERING()
					AND GET_SKYFREEZE_STAGE() != SKYFREEZE_FROZEN
						DISPLAY_HELP_TEXT_THIS_FRAME_FOR_MINIGAME("MC_DRILL_1",TRUE)
						
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
							MC_playerBD[iPartToUse].iObjHacking = iobj
							CLEAR_BIT(MC_playerBD[iPartToUse].iHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
							CLEAR_BIT(sVaultDrillData.iBitset, DRILL_BITSET_HAS_BROADCAST_ASSET_REQUEST)
														
							RESET_NET_TIMER(tdHackTimer)
							iHackLimitTimer = 0
							
							IF NOT HAS_MINIGAME_TIMER_BEEN_INITIALISED(1)
								PRINTLN("[RCC MISSION][TELEMETRY] HACKING (DRILLING) - INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY - SLOT 1")
								PRINTLN("[RCC MISSION][TELEMETRY] HACKING (DRILLING) - START MINIGAME TIMER - SLOT 1")							
								INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY(1)
								START_MINIGAME_TIMER_FOR_TELEMETRY(1)
							ELSE
								PRINTLN("[RCC MISSION][TELEMETRY] HACKING (DRILLING) VALUES NOT INITIALISED - TIMER ALREADY STARTED")
							ENDIF
							
							iHackProgress =  VAULT_DRILL_STATE_BEGIN
							PRINTLN("[RCC MISSION] [DRILLING] local player has started drilling, part: ",iPartToUse , " on object: ", iobj)
						ENDIF
					ELSE
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MC_DRILL_1")
							CLEAR_HELP()
						ENDIF
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MC_DRILL_1")
						CLEAR_HELP()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF


ENDPROC




PROC VAULT_DRILL_DEBUG_DISPLAY_SERVER_STATES(INT iobj)
	UNUSED_PARAMETER(iobj)
	#IF IS_DEBUG_BUILD 
		
		TEXT_LABEL_63 State = ""
		
		
		State = "MC_playerBD[iPartToUse].iObjHacked:  " 
		State += CONVERT_INT_TO_STRING(MC_playerBD[iPartToUse].iObjHacked) 
		
		SET_TEXT_SCALE(0.4,0.4)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.05,0.15, "STRING", State)		
		
		State = " MC_serverBD.iObjHackPart["
		State += CONVERT_INT_TO_STRING( iobj)
		State += "] :  " 
		State += CONVERT_INT_TO_STRING( MC_serverBD.iObjHackPart[iobj] )  		
		SET_TEXT_SCALE(0.4,0.4)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.05,0.25, "STRING", State)	
		
		State = "  MC_playerBD[iPartToUse].iObjHacking :  " 
		State += CONVERT_INT_TO_STRING(  MC_playerBD[iPartToUse].iObjHacking)  		
		SET_TEXT_SCALE(0.4,0.4)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.05,0.35, "STRING", State)	
		

	#ENDIF
ENDPROC



PROC CLEANUP_VAULT_DRILL_MINI_GAME(OBJECT_INDEX tempObj, INT iobj, INT iLocalParti)
	UNUSED_PARAMETER(tempObj)
	IF NOT (IS_SCRIPTED_CONVERSATION_ONGOING() AND IS_SUBTITLE_PREFERENCE_SWITCHED_ON() AND IS_MESSAGE_BEING_DISPLAYED())
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)
	ENDIF
	
	RESET_OBJ_HACKING_INT()

	IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_IS_MINIGAME_COMPLETE)
		MC_playerBD[iLocalParti].iObjHacked = iobj
		STORE_MINIGAME_ATTEMPTS_FOR_TELEMETRY(1)
		STORE_MINIGAME_TIMER_FOR_TELEMETRY(1)
		PRINTLN("[RCC MISSION][TELEMETRY] HACKING (VAULT_DRILL) - STORE_MINIGAME_ATTEMPTS_FOR_TELEMETRY - SLOT 1")
		PRINTLN("[RCC MISSION][TELEMETRY] HACKING (VAULT_DRILL) - STORE_MINIGAME_TIMER_FOR_TELEMETRY( - SLOT 1")
		
		MC_PlayerBD[iLocalParti].iHackTime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stMGTimer0)
		MC_playerBD[iLocalParti].iNumHacks++
		PRINTLN("[RCC MISSION] inumHacks HEIST TELEMETRY DRILLING MINI GAME  -", MC_playerBD[iLocalParti].iNumHacks)
		CPRINTLN(DEBUG_MISSION, "VAULT_DRILL_BITSET_IS_MINIGAME_COMPLETE - TRUE - FORCE_QUIT_PASS_VAULT_DRILL_MINIGAME_KEEP_DATA")
		FORCE_QUIT_PASS_VAULT_DRILL_MINIGAME_KEEP_DATA(sVaultDrillData, iLocalParti, TRUE, FALSE, !IS_BIT_SET( MC_playerBD[iLocalParti].iClientBitSet, PBBOOL_CC_FAILCUT_FINISH ) AND !g_bMissionEnding)
	ELSE
		
		BROADCAST_SCRIPT_EVENT_FMMC_VAULT_DRILL_PROGRESS(iobj, FLOOR(sVaultDrillData.fCurrentProgress * 100), sVaultDrillData.iCurrentObstruction  )
		
		
		CPRINTLN(DEBUG_MISSION, "DRILL_BITSET_IS_MINIGAME_COMPLETE - FALSE - FORCE_QUIT_PASS_DRILL_MI NIGAME_KEEP_DATA")
		FORCE_QUIT_PASS_VAULT_DRILL_MINIGAME_KEEP_DATA(sVaultDrillData, iLocalParti, FALSE, FALSE, !IS_BIT_SET( MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_CC_FAILCUT_FINISH ) AND !g_bMissionEnding)
	ENDIF
	
	RESET_NET_TIMER(tdHackTimer)		
	Unpause_Objective_Text()
	ENABLE_INTERACTION_MENU()
	
	IF bLocalPlayerPedOk
		FREEZE_ENTITY_POSITION(LocalPlayerPed, FALSE)
		SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed,TRUE,FALSE)
		IF NOT IS_BIT_SET( MC_playerBD[iLocalParti].iClientBitSet, PBBOOL_CC_FAILCUT_FINISH )
		AND NOT g_bMissionEnding
			NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
		ENDIF
		RELEASE_PED_PRELOAD_VARIATION_DATA(LocalPlayerPed)
		RUN_LOCAL_PLAYER_PROOF_SETTING(TRUE)
		
		IF eWeaponBeforeMinigame != WEAPONTYPE_UNARMED
		AND HAS_PED_GOT_WEAPON(LocalPlayerPed, eWeaponBeforeMinigame)
			SET_CURRENT_PED_WEAPON(LocalPlayerPed, eWeaponBeforeMinigame)
		ENDIF
	ENDIF
	BROADCAST_SCRIPT_EVENT_FMMC_VAULT_DRILL_ASSET(ciEVENT_VAULT_DRILL_ASSET_CLEANUP)
	iHackLimitTimer = 0
	RESET_NET_TIMER(tdHackTimer)		
	MC_playerBD[iLocalPart].iObjHacking = -1		
ENDPROC



PROC VAULT_DRILL_SETUP_DIFFICULTY(INT iobj)
	BOOL bSkip = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetFour, cibsOBJ4_VaultDoor_SkipOutro)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetFour, cibsOBJ4_VaultDoor_AllowLaser)
		IF g_FMMC_STRUCT.iDifficulity = DIFF_HARD
		
			SETUP_DRILL_TYPE(sVaultDrillData, TRUE, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iNumberOfObstructions_Hard, 
				g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].fDifficultyOfObstructionsLaser_Hard, bSkip)	
			PRINTLN("[RCC MISSION] [VAULT_DRILLING] VAULT_DRILL_STATE_BEGIN: SETUP FOR LASER - HARD")
		ELSE
			SETUP_DRILL_TYPE(sVaultDrillData, TRUE, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iNumberOfObstructions, 
				g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].fDifficultyOfObstructionsLaser, bSkip)	
			PRINTLN("[RCC MISSION] [VAULT_DRILLING] VAULT_DRILL_STATE_BEGIN: SETUP FOR LASER - NORMAL")
		ENDIF
	ELSE
		IF g_FMMC_STRUCT.iDifficulity = DIFF_HARD
			SETUP_DRILL_TYPE(sVaultDrillData, FALSE, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iNumberOfObstructions_Hard, 
				g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].fDifficultyOfObstructions_Hard, bSkip)	
			PRINTLN("[RCC MISSION] [VAULT_DRILLING] VAULT_DRILL_STATE_BEGIN: SETUP FOR DRILL - HARD")
		ELSE							
			SETUP_DRILL_TYPE(sVaultDrillData, FALSE, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iNumberOfObstructions, 
				g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].fDifficultyOfObstructions, bSkip)	
			PRINTLN("[RCC MISSION] [VAULT_DRILLING] VAULT_DRILL_STATE_BEGIN: SETUP FOR DRILL - NORMAL")
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_VAULT_DRILL_MINI_GAME(OBJECT_INDEX tempObj, INT iobj)
	
	IF MC_playerBD[iPartToUse].iObjHacked != -1
		EXIT
	ENDIF
	
	STRING sVaultDrillingAnimDict 		=	VAULT_DRILL__GET_ANIM_DICTIONARY_DRILL()
	STRING sVaultDrillingAnimDict_Laser = 	VAULT_DRILL__GET_ANIM_DICTIONARY_LASER()	
	
	//VAULT_DRILL_DEBUG_DISPLAY_SERVER_STATES(iobj)
	IF MC_playerBD[iPartToUse].iObjNear = -1
		IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed,tempObj) <= 3.0
			SET_OBJ_NEAR(iObj)
		ENDIF
	ENDIF		
	IF iHackProgress >= VAULT_DRILL_STATE_BEGIN
	//	VAULT_DRILL_DEBUG_DISPLAY_VARIABLES(sVaultDrillData)
	ENDIF
	
	IF MC_playerBD[iLocalPart].iObjHacking > -1
	AND MC_playerBD[iLocalPart].iObjHacking != iObj
		// We're already interacting with a different object - ignore this one
		EXIT
	ENDIF
	
	//Have the driller take control of the objects that were created at the start of the mission.
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
		IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
			NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
			CDEBUG1LN(DEBUG_MISSION, "[VAULT DRILL] Driller is requesting control of creator object.")
		ENDIF
	ENDIF


	IF MC_serverBD.iObjHackPart[iobj] = -1
		CORRECT_VAULT_DRILL_OBJECT_IF_INCORRECTLY_PLACED(tempObj, iobj)
		IF MC_playerBD[iPartToUse].iObjHacking = -1
			//Check if the player has triggered the minigame: do distance and button press check.
			IF bPlayerToUseOK
			AND NOT g_bMissionEnding
				SETUP_DRILL_TYPE(sVaultDrillData, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetFour, cibsOBJ4_VaultDoor_AllowLaser), g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iNumberOfObstructions, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].fDifficultyOfObstructions, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetFour, cibsOBJ4_VaultDoor_SkipOutro))
				REQUEST_VAULT_DRILL_MINIGAME_ASSETS(sVaultDrillData, MC_playerBD_1[iLocalPart].mnHeistGearBag)
				
				//2152186 - Broadcast to spectators/remote players that the player is now on the drilling minigame and the assets need to be requested.
				IF NOT IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_HAS_BROADCAST_ASSET_REQUEST)
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetFour, cibsOBJ4_VaultDoor_AllowLaser)
						BROADCAST_SCRIPT_EVENT_FMMC_VAULT_DRILL_ASSET(ciEVENT_VAULT_DRILL_ASSET_REQUEST_LASER)
					ELSE
						BROADCAST_SCRIPT_EVENT_FMMC_VAULT_DRILL_ASSET(ciEVENT_VAULT_DRILL_ASSET_REQUEST)
					ENDIF
				
					SET_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_HAS_BROADCAST_ASSET_REQUEST)
				ENDIF
			
				HANDLE_VAULT_DRILL_TRIGGER_AND_PROMPT(tempObj, iobj)
			ENDIF
		ELSE
			FORCE_VAULT_DRILL_MINIGAME_TO_RESET_IF_FAILED_BEFORE_TRIGGER()
		ENDIF
	ELSE
		IF MC_playerBD[iPartToUse].iObjHacking != -1
			IF MC_serverBD.iObjHackPart[iobj] = iPartToUse
				//2159869 - If the mission fails while this player is drilling then force a cleanup.
				IF (NOT bPlayerToUseOK) OR g_bMissionEnding
					IF iHackProgress != VAULT_DRILL_STATE_CLEANUP
					AND iHackProgress != VAULT_DRILL_STATE_DO_BAG_SWAP
					
						
						//NOTE: there's a bag pop due to it streaming in a few frames after the fail, 
						//as long as the bag is given back on a retry we can avoid giving it here and stop the pop.
						//APPLY_DUFFEL_BAG_HEIST_GEAR(LocalPlayerPed, iLocalPart)
					
						IF bLocalPlayerPedOk
							CLEAR_PED_TASKS_IMMEDIATELY(LocalPlayerPed)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed)
						ENDIF
					
						PRINTLN("[VAULT DRILL] Mission failed, forcing cleanup.")
						iHackProgress = VAULT_DRILL_STATE_CLEANUP
					ENDIF
				
					PRINTLN("[VAULT DRILL] Mission failed, forcing cleanup.")
					iHackProgress = VAULT_DRILL_STATE_CLEANUP
				ENDIF
			
			
				IF iHackProgress >= DRILL_STATE_STARTING_INTRO
					//2074382 - Expand the player's capsule to stop other players walking into the drill.
					SET_PED_CAPSULE(LocalPlayerPed, 1.0)
				ENDIF
				
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				
				///
				///    	 Main Minigame loop
				///    	 
				
				SWITCH iHackProgress
					CASE VAULT_DRILL_STATE_BEGIN //Begin requesting assets, lock the player into the game and fetch the coord the player needs to go to.
						PRINTLN("[RCC MISSION] [VAULT_DRILLING] VAULT_DRILL_STATE_BEGIN.")
					
					
						IF DOES_ENTITY_EXIST(tempObj)
							NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_ALLOW_PLAYER_DAMAGE | NSPC_PREVENT_EVERYBODY_BACKOFF | NSPC_LEAVE_CAMERA_CONTROL_ON)
							REINIT_NET_TIMER(tdHackTimer)
							
							VAULT_DRILL_SETUP_DIFFICULTY(iobj)
							
							REQUEST_VAULT_DRILL_MINIGAME_ASSETS(sVaultDrillData, MC_playerBD_1[iLocalPart].mnHeistGearBag)
							
							CLEAR_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_ALREADY_STARTED)
							
							INT  i
							
							FOR i = 0 TO (FMMC_MAX_VAULT_DRILL_GAMES -1)
								IF MC_serverBD.iObjVaultDrillIDs[i] = iobj
									IF MC_serverBD.iObjVaultDrillProgress[i] > 0
										SET_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_ALREADY_STARTED)
										
										sVaultDrillData.iCurrentObstruction = 	MC_serverBD.iObjVaultDrillDiscs[i] 		
										sVaultDrillData.fCurrentProgress 	= 	TO_FLOAT(MC_serverBD.iObjVaultDrillProgress[i]) /100.0
										CPRINTLN(DEBUG_MISSION, "[RCC MISSION] [VAULT_DRILLING] SETTING UP VALUES: MC_serverBD.iObjVaultDrillIDs[i]", MC_serverBD.iObjVaultDrillIDs[i], " i: ", i, "MC_serverBD.iObjVaultDrillDiscs[i]:  ", MC_serverBD.iObjVaultDrillDiscs[i], 
											" sVaultDrillData.fCurrentProgress: ", sVaultDrillData.fCurrentProgress, "  ", MC_serverBD.iObjVaultDrillProgress[i])
											
									ENDIF
									BREAKLOOP
								ENDIF
							ENDFOR
							
							CLEAR_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_DID_PLAYER_MESS_UP)
							
							IF NOT HAS_NET_TIMER_STARTED(stMGTimer0)
								REINIT_NET_TIMER(stMGTimer0)
							ENDIF

							IF sVaultDrillData.iDrillSoundID = -1
								sVaultDrillData.iDrillSoundID = GET_SOUND_ID()
							ENDIF
							
							NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_ALLOW_PLAYER_DAMAGE | NSPC_PREVENT_EVERYBODY_BACKOFF | NSPC_LEAVE_CAMERA_CONTROL_ON)
							START_AUDIO_SCENE("DLC_HEIST_MINIGAME_FLEECA_DRILLING_SCENE")
							iHackProgress = VAULT_DRILL_STATE_WAITING_FOR_ASSETS
							PRINTLN(DEBUG_MISSION,"[VAULT DRILL] VAULT_DRILL_STATE_WAITING_FOR_ASSETS.")
						ENDIF
					BREAK
					
					CASE VAULT_DRILL_STATE_WAITING_FOR_ASSETS //Wait for the assets to load, once ready then task the player into the right spot.
						FLOAT fGroundZTemp
						
						VAULT_DRILL_SETUP_DIFFICULTY(iobj)
												
						REQUEST_VAULT_DRILL_MINIGAME_ASSETS(sVaultDrillData, MC_playerBD_1[iLocalPart].mnHeistGearBag)
						
						IF HAVE_VAULT_DRILL_ASSETS_LOADED(sVaultDrillData, MC_playerBD_1[iLocalPart].mnHeistGearBag)									
							//Task the player to walk to where they will be when the anim triggers.
							IF bLocalPlayerPedOk
							AND NOT IS_PED_PERFORMING_MELEE_ACTION(LocalPlayerPed)
							AND NOT IS_PED_RAGDOLL(LocalPlayerPed)
							AND NOT IS_PED_RUNNING(LocalPlayerPed)
							AND NOT IS_PED_JUMPING(LocalPlayerPed)

								fObjRot = GET_ENTITY_HEADING(tempObj)
								
								IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_USE_LASER)
									vPlayAnimAdvancedPlayback 	= GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj, VAULT_DRILL__GET_OFFSET_FOR_MINIGAME_ROOT_LASER())
									vGoToPoint					= GET_ANIM_INITIAL_OFFSET_POSITION(sVaultDrillingAnimDict_Laser, "intro", vPlayAnimAdvancedPlayback, <<0.0, 0.0, fObjRot>>)
								ELSE
									vPlayAnimAdvancedPlayback 	= GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj, VAULT_DRILL__GET_OFFSET_FOR_MINIGAME_ROOT())
									vGoToPoint 					= GET_ANIM_INITIAL_OFFSET_POSITION(sVaultDrillingAnimDict, "intro", vPlayAnimAdvancedPlayback, <<0.0, 0.0, fObjRot>>)
								ENDIF
								
								GET_GROUND_Z_FOR_3D_COORD(vGoToPoint + <<0.0, 0.0, 5.0>>, fGroundZTemp)
								vGoToPoint.z = fGroundZTemp
							
								NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_ALLOW_PLAYER_DAMAGE | NSPC_PREVENT_EVERYBODY_BACKOFF | NSPC_LEAVE_CAMERA_CONTROL_ON)
								CLEAR_PED_TASKS(LocalPlayerPed)
								GET_CURRENT_PED_WEAPON(LocalPlayerPed, eWeaponBeforeMinigame)
								
								PRINTLN(DEBUG_MISSION,"[VAULT DRILL] - [RCC MISSION] vGoTo: ", vGoToPoint)
								PRINTLN(DEBUG_MISSION,"[VAULT DRILL] - [RCC MISSION] Heading: ", fObjRot)

								REINIT_NET_TIMER(tdHackTimer)
								iSafetyTimer 		= GET_GAME_TIMER()
								
								IF VDIST(GET_ENTITY_COORDS(LocalPlayerPed), vGoToPoint + <<0.0, 0.0, 1.0>>) >= 0.9
									
									SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED, TRUE)
								
									SEQUENCE_INDEX seq				
									OPEN_SEQUENCE_TASK(seq)
										PRINTLN("[AW_MISSION] - [RCC MISSION] Distance between player and goto", GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(LocalPlayerPed), vGoToPoint, FALSE))
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vGoToPoint, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, 0.1, ENAV_STOP_EXACTLY, fObjRot)
										PRINTLN("[AW_MISSION] - [RCC MISSION] Too far away using goto ")
									CLOSE_SEQUENCE_TASK(seq)
									TASK_PERFORM_SEQUENCE(LocalPlayerPed, seq)
									CLEAR_SEQUENCE_TASK(seq)
									
									CLEAR_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_SKIP_WALK_TO_POSITION)
								ELSE
									SET_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_SKIP_WALK_TO_POSITION)
								ENDIF
								
								iHackProgress 		= VAULT_DRILL_STATE_STARTING_INTRO
								PRINTLN(DEBUG_MISSION,"[VAULT DRILL] - iHackProgress 		= VAULT_DRILL_STATE_STARTING_INTRO")
								
							ENDIF
					
						ENDIF
					BREAK
					
					CASE VAULT_DRILL_STATE_STARTING_INTRO //Begin the drilling intro.
												
						VECTOR vInitialRot
						FLOAT fDistFromAnimStart, fHeadingDiffFromAnimStart
						
						
						IF IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_USE_LASER)
							vInitialRot = GET_ANIM_INITIAL_OFFSET_ROTATION(sVaultDrillingAnimDict_Laser, "intro", vPlayAnimAdvancedPlayback, <<0.0, 0.0, fObjRot>>)
						ELSE
							vInitialRot = GET_ANIM_INITIAL_OFFSET_ROTATION(sVaultDrillingAnimDict, "intro", vPlayAnimAdvancedPlayback, <<0.0, 0.0, fObjRot>>)
						ENDIF
						
						fDistFromAnimStart = VDIST2(GET_ENTITY_COORDS(LocalPlayerPed, FALSE), vGoToPoint + <<0.0, 0.0, 1.0>>)
						fHeadingDiffFromAnimStart = ABSF(GET_ENTITY_HEADING(LocalPlayerPed) - vInitialRot.z)
						
						IF fHeadingDiffFromAnimStart > 180.0
							fHeadingDiffFromAnimStart -= 360.0
						ENDIF
						
						IF GET_SCRIPT_TASK_STATUS(LocalPlayerPed,SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
						OR IS_BIT_SET(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_SKIP_WALK_TO_POSITION) 
						OR (fDistFromAnimStart < 0.04 AND ABSF(fHeadingDiffFromAnimStart) < 10.0)
						OR MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer,6000)
							
							INT iVaults, iTeam
						
							iTeam	=	MC_playerBD[iPartToUse].iteam
							iVaults =  MC_serverBD.iNumObjHighestPriority[iTeam]

							//Created the props by here.
							SET_PED_PROOFS_FOR_MINIGAME(LocalPlayerPed)
							Pause_Objective_Text()
							
							iSafetyTimer = GET_GAME_TIMER()
							DISABLE_INTERACTION_MENU() 
							
							RUN_VAULT_DRILL_MINIGAME(sVaultDrillData, niMinigameObjects, iSafetyTimer, tempObj, iLocalPart,  iVaults, iObj, DEFAULT, MC_playerBD_1[iLocalPart].mnHeistGearBag, MC_playerBD_1[iLocalPart].eHeistGearBagType )
							
							iHackProgress = VAULT_DRILL_STATE_DRILLING
							PRINTLN(DEBUG_MISSION,"[VAULT_DRILLING] - iHackProgress 		= VAULT_DRILL_STATE_DRILLING")
						ENDIF
					BREAK
					
					CASE VAULT_DRILL_STATE_DRILLING //Run the actual minigame.
													
						INT iVaults, iTeam
						
						iTeam	=	MC_playerBD[iPartToUse].iteam
						iVaults =  MC_serverBD.iNumObjHighestPriority[iTeam]
						
						RUN_VAULT_DRILL_MINIGAME(sVaultDrillData, niMinigameObjects, iSafetyTimer, tempObj, iLocalPart,  iVaults, iObj, DEFAULT, MC_playerBD_1[iLocalPart].mnHeistGearBag, MC_playerBD_1[iLocalPart].eHeistGearBagType)
						
						IF IS_BIT_SET(sVaultDrillData.iBitset , VAULT_DRILL_BITSET_MESS_UP_TRIGGERED_FOR_TELEMETRY)
							iSafetyTimer = GET_GAME_TIMER()
							INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY(1)
							CPRINTLN(DEBUG_MISSION,"[VAULT DRILL] HACKING (DRILLING) - INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY - SLOT 1")
							CLEAR_BIT(sVaultDrillData.iBitset , VAULT_DRILL_BITSET_MESS_UP_TRIGGERED_FOR_TELEMETRY)
						ENDIF
						
						IF IS_BIT_SET(sVaultDrillData.iBitset , VAULT_DRILL_BITSET_IS_MINIGAME_COMPLETE)
						OR IS_BIT_SET(sVaultDrillData.iBitset , VAULT_DRILL_BITSET_IS_BACK_OUT_COMPLETE)
							CPRINTLN(DEBUG_MISSION,"[VAULT DRILL] Cleanup start for: ", iPartToUse )
							iHackProgress =  VAULT_DRILL_STATE_CLEANUP
						ENDIF
					BREAK
					
				

					CASE VAULT_DRILL_STATE_CLEANUP //Final cleanup: can enter this state either by passing or backing out of the game. If we backed out we need to be able to retrigger.
						CLEANUP_VAULT_DRILL_MINI_GAME(tempObj, iobj, iLocalPart)
						MC_serverBD.iObjHackPart[iobj] = -1
						CLEAR_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_IS_BACK_OUT_COMPLETE)
						CLEAR_BIT(sVaultDrillData.iBitset, VAULT_DRILL_BITSET_ALREADY_STARTED)
						
						CPRINTLN(DEBUG_MISSION,"[VAULT DRILL] Cleanup finished for: ", iPartToUse )
						iHackProgress =  VAULT_DRILL_STATE_INITIALISE
					BREAK
					
				
				ENDSWITCH
			ELSE
				CPRINTLN(DEBUG_MISSION, "[VAULT DRILL] SECOND PLAYER ATTEMPTING : kicking out: ", iPartToUse )
				CPRINTLN(DEBUG_MISSION, "[VAULT DRILL] SECOND PLAYER ATTEMPTING : current player on minigame: ", MC_serverBD.iObjHackPart[iobj] )
				CPRINTLN(DEBUG_MISSION, "[VAULT DRILL] SECOND PLAYER ATTEMPTING : Object this player is attempting: ",	MC_playerBD[iPartToUse].iObjHacking )
				
				FORCE_VAULT_DRILL_MINIGAME_TO_RESET_IF_FAILED_BEFORE_TRIGGER()
				VAULT_DRILL_GAME_ENDING_SKIP(sVaultDrillData, niMinigameObjects, iSafetyTimer, localPlayerPed, iLocalPart)
				CLEAR_BIT(MC_playerBD[iPartToUse].iHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
				FORCE_QUIT_PASS_VAULT_DRILL_MINIGAME_KEEP_DATA(sVaultDrillData, iLocalPart, FALSE, TRUE, TRUE, TRUE)
				RESET_OBJ_HACKING_INT()
				
				//Kick player out as someone is already playing the minigame
				FREEZE_ENTITY_POSITION(LocalPlayerPed, FALSE)
				SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed,TRUE,FALSE)
				
				iHackProgress =  VAULT_DRILL_STATE_INITIALISE
				
				IF eWeaponBeforeMinigame != WEAPONTYPE_UNARMED
				AND HAS_PED_GOT_WEAPON(LocalPlayerPed, eWeaponBeforeMinigame)
					SET_CURRENT_PED_WEAPON(LocalPlayerPed, eWeaponBeforeMinigame)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC




//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: BLOW DOORS (+ Thermite + Casco Container) MINIGAME !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: THERMITE BLOW DOORS (used during The Pacific Standard Job heist finale) !
//
//************************************************************************************************************************************************************



PROC REQUEST_THERMITE_ASSETS(BOOL bIsKeypadObject = FALSE)
	
	//2315026
	IF IS_PLAYSTATION_PLATFORM()
	OR IS_XBOX_PLATFORM()
	OR IS_PC_VERSION()
		PRINTLN("[THERMITE] - [RCC MISSION] REQUEST_THERMITE_ASSETS() PARTICLES - Using patched PTFX")
		sPTFXThermalAsset = "pat_heist"
		REQUEST_NAMED_PTFX_ASSET("scr_ornate_heist")
	ENDIF
	
	IF bIsKeypadObject
		sPTFXThermalAsset = "scr_ch_finale"
		PRINTLN("[THERMITE] - [RCC MISSION] REQUEST_THERMITE_ASSETS() PARTICLES - Using keypad PTFX")
	ENDIF
	
	REQUEST_MODEL(MC_playerBD_1[iLocalPart].mnHeistGearBag)
	REQUEST_MODEL(HEI_PROP_HEIST_THERMITE)
	REQUEST_MODEL(HEI_PROP_HEIST_THERMITE_FLASH)
	REQUEST_NAMED_PTFX_ASSET(sPTFXThermalAsset)
		
	IF bIsKeypadObject
		REQUEST_ANIM_DICT("anim_heist@hs3f@ig13_thermal_charge@thermal_charge@male@")
	ENDIF
	
	REQUEST_ANIM_DICT("anim@heists@ornate_bank@thermal_charge")

	IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
		IF bIsKeypadObject
			REQUEST_ANIM_DICT("anim_heist@hs3f@ig13_thermal_charge@thermal_charge@female@")
		ENDIF
		
		REQUEST_ANIM_DICT("anim@heists@ornate_bank@thermal_charge_heels")
	ENDIF
ENDPROC

FUNC BOOL HAVE_THERMITE_ASSETS_LOADED(BOOL bIsKeypadObject = FALSE)
	BOOL bHeelAnimsReady = TRUE
	BOOL bKeypadAnimsReady = TRUE
	
	IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
		bHeelAnimsReady = HAS_ANIM_DICT_LOADED("anim@heists@ornate_bank@thermal_charge_heels")
		IF bIsKeypadObject
			bKeypadAnimsReady = HAS_ANIM_DICT_LOADED("anim_heist@hs3f@ig13_thermal_charge@thermal_charge@female@")
		ENDIF
	ELSE
		IF bIsKeypadObject
			bKeypadAnimsReady = HAS_ANIM_DICT_LOADED("anim_heist@hs3f@ig13_thermal_charge@thermal_charge@male@")
		ENDIF
	ENDIF
	
	IF IS_PLAYSTATION_PLATFORM()
	OR IS_XBOX_PLATFORM()
	OR IS_PC_VERSION()
		IF HAS_MODEL_LOADED(MC_playerBD_1[iLocalPart].mnHeistGearBag)
		AND HAS_MODEL_LOADED(HEI_PROP_HEIST_THERMITE)
		AND HAS_MODEL_LOADED(HEI_PROP_HEIST_THERMITE_FLASH)
		AND HAS_NAMED_PTFX_ASSET_LOADED(sPTFXThermalAsset)
		AND HAS_NAMED_PTFX_ASSET_LOADED("scr_ornate_heist")
		AND HAS_ANIM_DICT_LOADED("anim@heists@ornate_bank@thermal_charge")
		AND bHeelAnimsReady
		AND bKeypadAnimsReady
			RETURN TRUE
		ENDIF
	ELSE
	
		IF HAS_MODEL_LOADED(MC_playerBD_1[iLocalPart].mnHeistGearBag)
		AND HAS_MODEL_LOADED(HEI_PROP_HEIST_THERMITE)
		AND HAS_MODEL_LOADED(HEI_PROP_HEIST_THERMITE_FLASH)
		AND HAS_NAMED_PTFX_ASSET_LOADED(sPTFXThermalAsset)
		AND HAS_ANIM_DICT_LOADED("anim@heists@ornate_bank@thermal_charge")
		AND bHeelAnimsReady
		AND bKeypadAnimsReady
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC REMOVE_THERMITE_ASSETS(BOOL bIsKeypadObject = FALSE)
	
	IF IS_PLAYSTATION_PLATFORM()
	OR IS_XBOX_PLATFORM()
	OR IS_PC_VERSION()
		PRINTLN("[THERMITE] - [RCC MISSION] REMOVE_THERMITE_ASSETS() PARTICLES - Using patched PTFX")
		REMOVE_NAMED_PTFX_ASSET("scr_ornate_heist")
	ENDIF
	
	SET_MODEL_AS_NO_LONGER_NEEDED(MC_playerBD_1[iLocalPart].mnHeistGearBag)
	SET_MODEL_AS_NO_LONGER_NEEDED(HEI_PROP_HEIST_THERMITE)
	SET_MODEL_AS_NO_LONGER_NEEDED(HEI_PROP_HEIST_THERMITE_FLASH)
	REMOVE_NAMED_PTFX_ASSET(sPTFXThermalAsset)
	
	IF bIsKeypadObject
		REMOVE_ANIM_DICT("anim_heist@hs3f@ig13_thermal_charge@thermal_charge@male@")
		REMOVE_ANIM_DICT("anim_heist@hs3f@ig13_thermal_charge@thermal_charge@female@")
	ENDIF
	
	REMOVE_ANIM_DICT("anim@heists@ornate_bank@thermal_charge")
	REMOVE_ANIM_DICT("anim@heists@ornate_bank@thermal_charge_heels")
ENDPROC

PROC FORCE_OBJECT_ROOM_TO_MATCH_PLAYER(OBJECT_INDEX obj)
	IF DOES_ENTITY_EXIST(obj)
		INTERIOR_INSTANCE_INDEX interiorCurrent
		INT iCurrentRoom
		
		interiorCurrent = GET_INTERIOR_FROM_ENTITY(LocalPlayerPed)
		iCurrentRoom = GET_ROOM_KEY_FROM_ENTITY(LocalPlayerPed)
		
		IF interiorCurrent != NULL
		AND iCurrentRoom != 0
		AND IS_INTERIOR_READY(interiorCurrent)
			PRINTLN("[AW_MISSION] - [RCC MISSION] FORCE_OBJECT_ROOM_TO_MATCH_PLAYER: forcing room for object.")
			
			FORCE_ROOM_FOR_ENTITY(obj, interiorCurrent, iCurrentRoom)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL CREATE_THERMAL_CHARGE_OBJECT(BOOL bSetInvisible = FALSE)
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[1])
		REQUEST_MODEL(HEI_PROP_HEIST_THERMITE)
		
		IF HAS_MODEL_LOADED(HEI_PROP_HEIST_THERMITE)
			IF NOT IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_THERMITE_CHARGE )
				MC_RESERVE_NETWORK_MISSION_OBJECTS(1)
				SET_BIT(iLocalBoolCheck12,LBOOL12_OBJ_RES_THERMITE_CHARGE)
			ENDIF
			
			IF IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_THERMITE_CHARGE )
				IF CAN_REGISTER_MISSION_OBJECTS(1)
					IF CREATE_NET_OBJ(niMinigameObjects[1],HEI_PROP_HEIST_THERMITE,GET_ENTITY_COORDS(LocalPlayerPed), FALSE)
						SET_ENTITY_COLLISION(NET_TO_OBJ(niMinigameObjects[1]), FALSE)
						SET_ENTITY_INVINCIBLE(NET_TO_OBJ(niMinigameObjects[1]), TRUE)
						SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[1]), NOT bSetInvisible)
						SET_MODEL_AS_NO_LONGER_NEEDED(HEI_PROP_HEIST_THERMITE)
						SET_NETWORK_ID_CAN_MIGRATE(niMinigameObjects[1], FALSE)
						
						PRINTLN("[THERMITE] - [RCC MISSION] CREATE_THERMAL_CHARGE_OBJECT: successfully created thermite object.")
						
						//Make sure the object is being treated as part of the interior.
						FORCE_OBJECT_ROOM_TO_MATCH_PLAYER(NET_TO_OBJ(niMinigameObjects[1]))
						CLEAR_BIT(iLocalBoolCheck12,LBOOL12_OBJ_RES_THERMITE_CHARGE)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC DELETE_THERMAL_CHARGE_OBJECT()
	OBJECT_INDEX tempObj
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[1])
		tempObj = NET_TO_OBJ(niMinigameObjects[1])
		
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(niMinigameObjects[1])
			PRINTLN("[THERMITE] - [RCC MISSION] DELETE_THERMAL_CHARGE_OBJECT: deleted thermite object.")
		
			DELETE_OBJECT(tempObj)
		ELSE
			PRINTLN("[THERMITE] - [RCC MISSION] DELETE_THERMAL_CHARGE_OBJECT: set thermite as no longer needed.")
		
			CLEANUP_NET_ID(niMinigameObjects[1])
		ENDIF
		
		MC_RESERVE_NETWORK_MISSION_OBJECTS(-1)
	ENDIF
ENDPROC

FUNC BOOL CREATE_FLASHING_THERMAL_CHARGE_OBJECT(VECTOR vPos, BOOL bSetInvisible = FALSE)
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[2])
		REQUEST_MODEL(HEI_PROP_HEIST_THERMITE_FLASH)
		
		IF HAS_MODEL_LOADED(HEI_PROP_HEIST_THERMITE_FLASH)
			
			IF NOT IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_THERMITE_CHARGE_FLASH )
				MC_RESERVE_NETWORK_MISSION_OBJECTS(1)
				SET_BIT(iLocalBoolCheck12,LBOOL12_OBJ_RES_THERMITE_CHARGE_FLASH)
			ENDIF
			IF IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_THERMITE_CHARGE_FLASH )
				IF CAN_REGISTER_MISSION_OBJECTS(1)
					IF CREATE_NET_OBJ(niMinigameObjects[2],HEI_PROP_HEIST_THERMITE_FLASH, vPos, FALSE)
						SET_ENTITY_COLLISION(NET_TO_OBJ(niMinigameObjects[2]), FALSE)
						SET_ENTITY_INVINCIBLE(NET_TO_OBJ(niMinigameObjects[2]), TRUE)
						SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[2]), NOT bSetInvisible)
						SET_MODEL_AS_NO_LONGER_NEEDED(HEI_PROP_HEIST_THERMITE_FLASH)
						SET_NETWORK_ID_CAN_MIGRATE(niMinigameObjects[2], FALSE)
						
						PRINTLN("[THERMITE] - [RCC MISSION] CREATE_FLASHING_THERMAL_CHARGE_OBJECT: successfully created object.")
						
						//Make sure the object is being treated as part of the interior.
						FORCE_OBJECT_ROOM_TO_MATCH_PLAYER(NET_TO_OBJ(niMinigameObjects[2]))
						CLEAR_BIT(iLocalBoolCheck12,LBOOL12_OBJ_RES_THERMITE_CHARGE_FLASH)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC DELETE_FLASHING_THERMAL_CHARGE_OBJECT()
	OBJECT_INDEX tempObj
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[2])
		tempObj = NET_TO_OBJ(niMinigameObjects[2])
		
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(niMinigameObjects[2])
			PRINTLN("[THERMITE] - [RCC MISSION] DELETE_FLASHING_THERMAL_CHARGE_OBJECT: deleted object.")
		
			DELETE_OBJECT(tempObj)
		ELSE
			PRINTLN("[THERMITE] - [RCC MISSION] DELETE_FLASHING_THERMAL_CHARGE_OBJECT: set as no longer needed.")
		
			CLEANUP_NET_ID(niMinigameObjects[2])
		ENDIF
		
		MC_RESERVE_NETWORK_MISSION_OBJECTS(-1)
	ENDIF
ENDPROC

///Removes the thermite object and replaces it with a thermite explosion effect.
PROC ACTIVATE_THERMITE_EFFECT(OBJECT_INDEX objMinigame, INT iMinigameObjID, VECTOR vThermitePos, VECTOR vThermiteRot, VECTOR vSparkEffectOffset, VECTOR vDripEffectOffset, BOOL bIsKeypadObject = FALSE)
	IF DOES_ENTITY_EXIST(objMinigame)
		DETACH_ENTITY(NET_TO_OBJ(niMinigameObjects[1]), FALSE)
		FREEZE_ENTITY_POSITION(NET_TO_OBJ(niMinigameObjects[1]), TRUE)
		SET_ENTITY_COORDS_NO_OFFSET(NET_TO_OBJ(niMinigameObjects[1]), vThermitePos)
		SET_ENTITY_ROTATION(NET_TO_OBJ(niMinigameObjects[1]), vThermiteRot)
		SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[1]), FALSE)
		SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[2]), FALSE)
		PRINTLN("[THERMITE] - [RCC MISSION] [ACTIVATE_THERMITE_EFFECT] Placed thermite object at: ", vThermitePos, " ", vThermiteRot)
		
		//2092725 - planting sticky bombs on the doors causes issues when the door is swapped later.
		CLEAR_AREA_OF_PROJECTILES(GET_ENTITY_COORDS(objMinigame), 1.5, TRUE)
				
		//Make sure the effect height is consistent with the animation, as the creator object may be placed at different heights.
		VECTOR vMinigameObjPos = GET_ENTITY_COORDS(objMinigame)
		vSparkEffectOffset.z = vThermitePos.z - vMinigameObjPos.z
		vDripEffectOffset.z = vThermitePos.z - vMinigameObjPos.z
		
		//The effect appears to have a slightly higher offset on current gen.
		#IF NOT IS_NEXTGEN_BUILD
			vSparkEffectOffset = vSparkEffectOffset + <<0.0, 0.0, -0.08>>
			vDripEffectOffset = vDripEffectOffset + <<0.0, 0.0, -0.08>>
		#ENDIF
		
		IF bIsKeypadObject //Process locally for keypad objects. //url:bugstar:6058740 	
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iMinigameObjID].iObjectBitSetFour, cibsOBJ4_SpookNearbyWhenUsed)
				SPOOK_PEDS_AT_COORD_IN_RADIUS(vMinigameObjPos, 7.5, ciSpookCoordID_THERMITE_PLACED, localPlayerPed)
			ENDIF
			BROADCAST_FMMC_THERMITE_EFFECT_EVENT(GET_ORDERED_PARTICIPANT_NUMBER(iLocalPart), iMinigameObjID, OBJ_TO_NET(objMinigame), vDripEffectOffset, vSparkEffectOffset, TRUE, FALSE, FALSE, FALSE, FALSE)
			EXIT
		ENDIF
		
		//Begin the thermite explosion: use the same offset as the scene to make sure it lines up with the thermite.
		//NOTE: We could attach the effect to the thermite itself but we might end up deleting the thermite object early.
		IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxThermite[THERMITE_DRIP_EFFECT_INDEX])
			OBJECT_INDEX objTempDoor
			
			//The drip/smoke effect needs to be attached to the door as it will persist after the door has been opened.
			//NOTE: This is currently disabled as this doesn't work on remote machines (probably because the door being attached to is local to each player).
			/*IF NOT DOES_ENTITY_EXIST(objTempDoor)
				INT iCurrentRule = MC_serverBD_4.iObjPriority[iMinigameObjID][MC_playerBD[iPartToUse].iteam]
				INT iCurrentDoor = iHackDoorID[iCurrentRule]
				MODEL_NAMES modelCurrentDoor = GET_FMMC_DOOR_MODEL(iCurrentDoor)
			
				IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(vMinigameObjPos, 2.0, modelCurrentDoor) 
					//NOTE: we just want a temp reference to the door so we can play an effect on it. Need to check that this works across all players.
					//IF CAN_REGISTER_MISSION_OBJECTS(1)
					//	RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS() + 1)
						objTempDoor = GET_CLOSEST_OBJECT_OF_TYPE(vMinigameObjPos, 2.0, modelCurrentDoor, FALSE, FALSE, FALSE)
				
						PRINTLN("[RCC MISSION] ACTIVATE_THERMITE_EFFECT Grabbed thermite door for placing effect. Rule = ", iCurrentRule)
					//ENDIF 
				ENDIF 
			ENDIF*/
			
			//2174654 - Store data about the door that the thermite was planted on: this needs to be used after the thermite rule has ended to check when to stop any ongoing PTFX.
			iLastThermiteDoorRule = MC_serverBD_4.iObjPriority[iMinigameObjID][MC_playerBD[iPartToUse].iteam]
			iLastThermiteDoorID = iHackDoorID[iLastThermiteDoorRule]
			CDEBUG1LN(DEBUG_MISSION, "[THERMITE] [RCC MISSION] [ACTIVATE_THERMITE_EFFECT] Storing door info: iLastThermiteDoorRule = ", iLastThermiteDoorRule, " iLastThermiteDoorID = ", iLastThermiteDoorID)
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iMinigameObjID].iObjectBitSetFour, cibsOBJ4_SpookNearbyWhenUsed)
				SPOOK_PEDS_AT_COORD_IN_RADIUS(vMinigameObjPos, 7.5, ciSpookCoordID_THERMITE_PLACED, localPlayerPed)
			ENDIF
			
			IF DOES_ENTITY_EXIST(objTempDoor)
				VECTOR vOffsetFromDoor = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(objTempDoor, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objMinigame, vDripEffectOffset))
				USE_PARTICLE_FX_ASSET("scr_ornate_heist")
				ptfxThermite[THERMITE_DRIP_EFFECT_INDEX] = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY("scr_heist_ornate_metal_drip", objTempDoor, vOffsetFromDoor, <<0.0, 0.0, 0.0>>)
				
				CDEBUG1LN(DEBUG_MISSION, "[THERMITE] [RCC MISSION] ACTIVATE_THERMITE_EFFECT Playing thermite drip effect on door.")
			ELSE
				USE_PARTICLE_FX_ASSET("scr_ornate_heist")
				ptfxThermite[THERMITE_DRIP_EFFECT_INDEX] = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY("scr_heist_ornate_metal_drip", objMinigame, vDripEffectOffset, <<0.0, 0.0, 0.0>>)
				
				CDEBUG1LN(DEBUG_MISSION, "[THERMITE] [RCC MISSION] ACTIVATE_THERMITE_EFFECT Playing thermite drip effect on minigame object.")
			ENDIF
		ENDIF
		
		IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxThermite[THERMITE_SPARK_EFFECT_INDEX])
			USE_PARTICLE_FX_ASSET(sPTFXThermalAsset)
			
			IF IS_PLAYSTATION_PLATFORM()
			OR IS_XBOX_PLATFORM()
			OR IS_PC_VERSION()
				PRINTLN("[THERMITE] - [RCC MISSION] PARTICLES - Using patched PTFX")
				ptfxThermite[THERMITE_SPARK_EFFECT_INDEX] = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY("scr_heist_ornate_thermal_burn_patch", objMinigame, vSparkEffectOffset, <<0.0, 0.0, 0.0>>)
			ELSE
				PRINTLN("[THERMITE] - [RCC MISSION] PARTICLES - Using original PTFX")
				ptfxThermite[THERMITE_SPARK_EFFECT_INDEX] = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY("scr_heist_ornate_thermal_burn", objMinigame, vSparkEffectOffset, <<0.0, 0.0, 0.0>>)
			ENDIF
		ENDIF
		
		//Flag indicates that the thermite has started burning: if the player dies while this flag is set then we don't force them to plant the charge again after respawning.
		SET_BIT(iThermiteBitset, THERMITE_BITSET_IS_THERMITE_BURNING)
		iThermiteSmokeEffectTimer = 0
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles creating and updating the local (not networked) versions of the thermite PTFX.
PROC UPDATE_LOCAL_THERMITE_EFFECTS()
	
	INT i
	REPEAT ciMAX_LOCAL_THERMITE_PTFX i
	
		IF IS_BIT_SET(iThermiteLocalBitset[i], THERMITE_LOCAL_BITSET_CREATE_LOCAL_THERMITE_PTFX) 
			
			sPTFXThermalAsset = "scr_ch_finale" //Currently only used on Casino Heist Finale. Make sure we're using this asset if player hasn't placed one yet.
			REQUEST_NAMED_PTFX_ASSET(sPTFXThermalAsset)
			REQUEST_NAMED_PTFX_ASSET("scr_ornate_heist")
			
			IF HAS_NAMED_PTFX_ASSET_LOADED(sPTFXThermalAsset)
			AND HAS_NAMED_PTFX_ASSET_LOADED("scr_ornate_heist")
				IF DOES_ENTITY_EXIST(oiLocalThermiteTarget[i])
					IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxThermiteLocalDrip[i])
						USE_PARTICLE_FX_ASSET("scr_ornate_heist")
						ptfxThermiteLocalDrip[i] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_heist_ornate_metal_drip", oiLocalThermiteTarget[i], vThermiteLocalDripOffset[i], <<0.0, 0.0, 0.0>>)
									
						CDEBUG1LN(DEBUG_MISSION, "[THERMITE] UPDATE_LOCAL_THERMITE_EFFECTS Playing thermite drip effect on minigame object locally. Placed by Ordered Participant: ", i)
					ENDIF
						
					IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxThermiteLocalSpark[i])
						vThermiteLocalSparkOffset[i] = << 0.0, 0.0, 0.0>>
						USE_PARTICLE_FX_ASSET(sPTFXThermalAsset)
						ptfxThermiteLocalSpark[i] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ch_finale_thermal_burn", oiLocalThermiteTarget[i], vThermiteLocalSparkOffset[i], <<0.0, 0.0, 0.0>>)
									
						CDEBUG1LN(DEBUG_MISSION, "[THERMITE] UPDATE_LOCAL_THERMITE_EFFECTS Playing thermite drip effect on minigame object locally. Placed by Ordered Participant: ", i)
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxThermiteLocalDrip[i])
			AND DOES_PARTICLE_FX_LOOPED_EXIST(ptfxThermiteLocalSpark[i])
				CLEAR_BIT(iThermiteLocalBitset[i], THERMITE_LOCAL_BITSET_CREATE_LOCAL_THERMITE_PTFX)	
				SET_BIT(iThermiteLocalBitset[i], THERMITE_LOCAL_BITSET_IS_THERMITE_BURNING)
				iThermiteLocalDripTimer[i] = 0
			ENDIF
		ELSE
			//Also update spark die off effect here if running locally.
			IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxThermiteLocalSpark[i])
				IF IS_BIT_SET(iThermiteLocalBitset[i], THERMITE_LOCAL_BITSET_START_LOCAL_SPARK_DIE_OFF_EVO)
					IF GET_GAME_TIMER() - iThermiteLocalSparkTimer[i] < (THERMAL_CHARGE_DURATION - 10000) //2 seconds
						FLOAT fEvoRatio
						fEvoRatio = TO_FLOAT(GET_GAME_TIMER() - iThermiteLocalSparkTimer[i] - (THERMAL_CHARGE_DURATION - 10000)) / 2000.0
													
						IF fEvoRatio > 1.0
							fEvoRatio = 1.0
						ENDIF
						
						SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxThermiteLocalSpark[i], "DieOff", fEvoRatio)
					
					ELSE
					
						IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxThermiteLocalSpark[i])
							STOP_PARTICLE_FX_LOOPED(ptfxThermiteLocalSpark[i])
						ENDIF
						
						iThermiteLocalSparkTimer[i] = 0
						CLEAR_BIT(iThermiteLocalBitset[i], THERMITE_LOCAL_BITSET_START_LOCAL_SPARK_DIE_OFF_EVO)
						CLEAR_BIT(iThermiteLocalBitset[i], THERMITE_LOCAL_BITSET_IS_ANY_THERMITE_PROCESSING)
						
						IF IS_BIT_SET(iThermiteLocalBitset[i], THERMITE_LOCAL_BITSET_IS_THERMITE_BURNING)
							CLEAR_BIT(iThermiteLocalBitset[i], THERMITE_LOCAL_BITSET_IS_THERMITE_BURNING)
						ENDIF
					ENDIF
				ELSE //Failsafe to end the local ptfx after time if still running.
					IF iThermiteLocalSparkTimer[i] != 0
					AND GET_GAME_TIMER() - iThermiteLocalSparkTimer[i] > (THERMAL_CHARGE_DURATION + 1000)
						iThermiteLocalSparkTimer[i] = GET_GAME_TIMER()
						SET_BIT(iThermiteLocalBitset[i], THERMITE_LOCAL_BITSET_START_LOCAL_SPARK_DIE_OFF_EVO)
						CDEBUG1LN(DEBUG_MISSION, "[THERMITE] UPDATE_LOCAL_THERMITE_EFFECTS Setting THERMITE_LOCAL_BITSET_START_LOCAL_SPARK_DIE_OFF_EVO from failsafe timer. Placed by Ordered Participant: ", i)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Update drip die off effect here if running locally.
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxThermiteLocalDrip[i])
			IF iThermiteLocalDripTimer[i] = 0
				IF NOT IS_BIT_SET(iThermiteLocalBitset[i], THERMITE_LOCAL_BITSET_START_LOCAL_SPARK_DIE_OFF_EVO) //Don't modify the effect if the thermite is currently actively burning (i.e. the door hasn't unlocked yet).
					iThermiteLocalDripTimer[i] = GET_GAME_TIMER()
					
					CDEBUG1LN(DEBUG_MISSION, "[THERMITE] UPDATE_LOCAL_THERMITE_EFFECTS Starting local drip effect timer. Placed by Ordered Participant: ", i)
				ENDIF
			ELSE
				//Have the effect die down over time: once the evo reaches 1.0 we no longer need it.
				FLOAT fEvoRatio = TO_FLOAT(GET_GAME_TIMER() - iThermiteLocalDripTimer[i]) / 4000
				
				IF fEvoRatio > 1.0
					fEvoRatio = 1.0
				ENDIF
				
				SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxThermiteLocalDrip[i], "DIE_OFF", fEvoRatio)
				
				IF fEvoRatio = 1.0
					STOP_PARTICLE_FX_LOOPED(ptfxThermiteLocalDrip[i])
					
					CDEBUG1LN(DEBUG_MISSION, "[THERMITE] UPDATE_LOCAL_THERMITE_EFFECTS Local Drip effect has been stopped. fEvoRatio = ", fEvoRatio, " Placed by Ordered Participant: ", i)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC

///2174654 - Handles the dying off of the thermite drip effect: this effect is triggered within the thermite rule but carries on after the rule has finished, so needs to be cleaned up separately.
PROC UPDATE_THERMITE_DRIP_EFFECT()
	CONST_INT THERMITE_PTFX_DIE_OFF_DURATION 	4000	
	
	//This is for the old networked only version of the thermite ptfx. Local implementation is in UPDATE_LOCAL_THERMITE_EFFECTS().
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxThermite[THERMITE_DRIP_EFFECT_INDEX])
		IF iThermiteSmokeEffectTimer = 0
			IF NOT IS_BIT_SET(iThermiteBitset, THERMITE_BITSET_IS_THERMITE_BURNING) //Don't modify the effect if the thermite is currently actively burning (i.e. the door hasn't unlocked yet).
				iThermiteSmokeEffectTimer = GET_GAME_TIMER()
				
				CDEBUG1LN(DEBUG_MISSION, "[THERMITE] [RCC MISSION] [UPDATE_THERMITE_DRIP_EFFECT] Starting drip effect timer.")
			ENDIF
		ELSE
			//Grab the open ratio of the door: if any player opens the door then stop the effect immediately otherwise it'll look like it's floating mid-air.
			FLOAT fDoorOpenRatio = 0.0
			INT iLastThermiteDoorHash = -1
			GET_NEW_HACK_DOOR_HASH(iLastThermiteDoorRule, GET_FMMC_DOOR_MODEL(iLastThermiteDoorID, IS_BIT_SET(iLocalHackDoorThermiteBitset, iLastThermiteDoorRule)), GET_FMMC_DOOR_COORDS(iLastThermiteDoorID), iLastThermiteDoorHash)
		
			IF iLastThermiteDoorHash != -1
				fDoorOpenRatio = DOOR_SYSTEM_GET_OPEN_RATIO(iLastThermiteDoorHash)
			ENDIF
		
			//Have the effect die down over time: once the evo reaches 1.0 we no longer need it.
			FLOAT fEvoRatio = TO_FLOAT(GET_GAME_TIMER() - iThermiteSmokeEffectTimer) / THERMITE_PTFX_DIE_OFF_DURATION
			
			IF fEvoRatio > 1.0
				fEvoRatio = 1.0
			ENDIF
			
			//If the door was opened then force the evo to zero so the drips die off quickly.
			IF ABSF(fDoorOpenRatio) > 0.02
				fEvoRatio = 0.0
			ENDIF
			
			SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxThermite[THERMITE_DRIP_EFFECT_INDEX], "DIE_OFF", fEvoRatio)
			
			IF fEvoRatio = 1.0
			OR ABSF(fDoorOpenRatio) > 0.02
				STOP_PARTICLE_FX_LOOPED(ptfxThermite[THERMITE_DRIP_EFFECT_INDEX])
				
				CDEBUG1LN(DEBUG_MISSION, "[THERMITE] [RCC MISSION] [UPDATE_THERMITE_DRIP_EFFECT] Drip effect has been stopped. fEvoRatio = ", fEvoRatio, " fDoorOpenRatio = ", fDoorOpenRatio)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_OBJECT_INTERACTION_HELP_TEXT_BE_BLOCKED()
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CH_EMP_HELP")
		CDEBUG1LN(DEBUG_MISSION, "SHOULD_OBJECT_INTERACTION_HELP_TEXT_BE_BLOCKED() Return TRUE, blocked due to CH_EMP_HELP help message being displayed.")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_REQUIRED_DIST_TO_INTERACT_WITH_KEYPAD(PED_INDEX pedToCheck, OBJECT_INDEX tempObj)

	VECTOR vKeypadInteractPoint = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj, << 0.0, -0.9, 0.0 >>)
	FLOAT fKeypadInteractZ = vKeypadInteractPoint.z
	IF GET_GROUND_Z_FOR_3D_COORD((vKeypadInteractPoint + << 0.0, 0.0, 0.5>>), fKeypadInteractZ)
		vKeypadInteractPoint.z = fKeypadInteractZ + 1.0 //If we got a valid ground pos + 1.0 for waist height.
	ELSE
		fKeypadInteractZ = vKeypadInteractPoint.z //If this fails use the object Z value.
	ENDIF

	IF VDIST2(GET_ENTITY_COORDS(pedToCheck, FALSE), vKeypadInteractPoint) <= (0.9 * 0.9)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if another player is potentially occupying a given position.
/// PARAMS:
///    vPos - The position to check.
///    fRadius - If the player is within this radius around the position then they are considered occupying it.
FUNC BOOL IS_ANOTHER_PLAYER_BLOCKING_THERMITE_LOCATION(INT iObj, OBJECT_INDEX obj, FLOAT fRadius, BOOL bIsKeypadObject = FALSE, BOOL bIsPlayerInKeypadRange = FALSE)
	VECTOR vObjPos = GET_ENTITY_COORDS(obj)
	VECTOR vObjOffset1 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(obj, <<-0.3, 0.0, 0.0>>) + <<0.0, 0.0, 2.0>>
	VECTOR vObjOffset2 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(obj, <<-0.3, -1.25, 0.0>>) + <<0.0, 0.0, -1.5>>
	FLOAT fLocalPlayerSquaredDistFromObj = VDIST2(GET_ENTITY_COORDS(LocalPlayerPed), vObjPos)
	
	IF bIsKeypadObject
		
		IF bIsPlayerInKeypadRange
			IF NOT IS_BIT_SET(iThermiteKeypadInRangeBS, iObj)
				SET_BIT(iThermiteKeypadInRangeBS, iObj)
			ENDIF
		ELSE
			IF IS_BIT_SET(iThermiteKeypadInRangeBS, iObj)
				CLEAR_BIT(iThermiteKeypadInRangeBS, iObj)
			ENDIF
		ENDIF
	ENDIF
	
	INT iPartsChecked = 0
	INT iPartLoop
	FOR iPartLoop = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		
		IF iPartLoop = iLocalPart
			RELOOP
		ENDIF
		
		PARTICIPANT_INDEX currentParticipant = INT_TO_PARTICIPANTINDEX(iPartLoop)
		
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(currentParticipant)	
			RELOOP
		ENDIF
		
		IF IS_PARTICIPANT_A_SPECTATOR(iPartLoop)
			RELOOP
		ENDIF
		
		PLAYER_INDEX currentPlayer = NETWORK_GET_PLAYER_INDEX(currentParticipant)
	
		IF IS_NET_PLAYER_OK(currentPlayer)
			PED_INDEX partPed = GET_PLAYER_PED(currentPlayer)
			VECTOR vPedPos = GET_ENTITY_COORDS(partPed)
			FLOAT fSquaredDistFromPos = VDIST2(vPedPos, vObjPos)
			
			IF NOT bIsKeypadObject
				IF fSquaredDistFromPos < (fRadius * fRadius)
				AND fSquaredDistFromPos < fLocalPlayerSquaredDistFromObj
				AND IS_ENTITY_IN_ANGLED_AREA(partPed, vObjOffset1, vObjOffset2, 0.75)
					PRINTLN("[THERMITE] [RCC MISSION] [IS_ANOTHER_PLAYER_BLOCKING_POSITION] Player ", iPartLoop, " is blocking position ", vObjPos, " squared distance = ", fSquaredDistFromPos)
					
					RETURN TRUE
				ENDIF
			ELSE
				IF (bIsPlayerInKeypadRange OR IS_BIT_SET(MC_playerBD_1[iLocalPart].iKeypadThermiteInRangeBS, iObj))
				AND (IS_PLAYER_REQUIRED_DIST_TO_INTERACT_WITH_KEYPAD(partPed, obj) OR IS_BIT_SET(MC_playerBD_1[iPartLoop].iKeypadThermiteInRangeBS, iObj))
					PRINTLN("[THERMITE] [RCC MISSION] [IS_ANOTHER_PLAYER_BLOCKING_POSITION] Player ", iPartLoop, " is blocking position ", vObjPos, " squared distance = ", fSquaredDistFromPos)
					
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
			
		iPartsChecked ++
	
		IF iPartsChecked >= MC_serverBD.iTotalNumPart - 1
			BREAKLOOP
		ENDIF
			
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_SAFE_TO_START_THERMITE()
	IF NOT IS_PED_PERFORMING_MELEE_ACTION(LocalPlayerPed)
	AND NOT IS_PED_RAGDOLL(LocalPlayerPed)
	AND NOT IS_PLAYER_CLIMBING(LocalPlayer)
	AND NOT IS_PED_FALLING(LocalPlayerPed)
	AND NOT IS_ENTITY_IN_AIR(LocalPlayerPed)
	AND NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, TRUE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_CLOSE_ENOUGH_TO_THERMITE_TRIGGER(STRING strThermiteAnimDict, STRING strThermiteClip)
	VECTOR vInitialRot = GET_ANIM_INITIAL_OFFSET_ROTATION(strThermiteAnimDict, strThermiteClip, vThermitePlaybackPos, <<0.0, 0.0, fObjRot>>)
	FLOAT fDistFromAnimStart = VDIST(GET_ENTITY_COORDS(LocalPlayerPed, FALSE), vThermiteGotoPoint + <<0.0, 0.0, 1.0>>)
	FLOAT fDistFromAnimRoot = VDIST(GET_ENTITY_COORDS(LocalPlayerPed, FALSE), vThermitePlaybackPos)
	FLOAT fDistBetweenGotoAndAnimRoot = VDIST(vThermitePlaybackPos, vThermiteGotoPoint + <<0.0, 0.0, 1.0>>)
	FLOAT fHeadingDiffFromAnimStart = ABSF(GET_ENTITY_HEADING(LocalPlayerPed) - vInitialRot.z)
	
	IF fHeadingDiffFromAnimStart > 180.0
		fHeadingDiffFromAnimStart -= 360.0
	ENDIF
	
	CDEBUG1LN(DEBUG_MISSION, "[THERMITE] - [RCC MISSION] IS_PLAYER_CLOSE_ENOUGH_TO_THERMITE_TRIGGER fDistFromAnimStart = ", fDistFromAnimStart, 
			  " fHeadingDiffFromAnimStart = ", fHeadingDiffFromAnimStart, 
			  " fDistBetweenGotoAndAnimRoot = ", fDistBetweenGotoAndAnimRoot,
			  " fDistFromAnimRoot = ", fDistFromAnimRoot)
	
	//2200153 - Updated the 'close enough' check so if the player would have to walk backwards to the anim trigger point we just start the anim instead.
	IF fDistFromAnimStart < 0.3
	OR (fDistFromAnimRoot < fDistBetweenGotoAndAnimRoot AND fDistFromAnimStart < 0.7)
		IF ABSF(fHeadingDiffFromAnimStart) < 120.0
			CDEBUG1LN(DEBUG_MISSION, "[THERMITE] - [RCC MISSION] Player close enough to thermite anim start.")
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC UPDATE_THERMITE_COVER_EYES_ANIM(OBJECT_INDEX objMinigame)
	IF iHackProgress >= THERMITE_STATE_DO_BAG_SWAP
	AND HAS_ANIM_DICT_LOADED("anim@heists@ornate_bank@thermal_charge")
		BOOL bForcedAnimUpdateThisFrame = FALSE
	
		IF IS_BIT_SET(iThermiteBitset, THERMITE_BITSET_IS_COVER_EYES_ANIM_PLAYING)
			VECTOR vPlayerPos = GET_ENTITY_COORDS(LocalPlayerPed)
			VECTOR vDoorPos = GET_ENTITY_COORDS(objMinigame)
			FLOAT fDistFromObject = VDIST(GET_ENTITY_COORDS(LocalPlayerPed), GET_ENTITY_COORDS(objMinigame))
			FLOAT fHeadingToObject = GET_HEADING_FROM_VECTOR_2D(vDoorPos.x - vPlayerPos.x, vDoorPos.y - vPlayerPos.y)
			FLOAT fPlayerHeading = GET_ENTITY_HEADING(LocalPlayerPed)
			FLOAT fHeadingDiff = ABSF(fHeadingToObject - fPlayerHeading)
	
			IF fHeadingDiff > 180.0
				fHeadingDiff = ABSF(fHeadingDiff - 360.0)
			ENDIF
			
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_RELOAD)
			
			IF fDistFromObject > 2.5
			OR (fHeadingDiff > 135.0 AND fDistFromObject > 0.8)
			OR (fHeadingDiff > 165.0 AND fDistFromObject > 0.4)
			OR IS_CUTSCENE_PLAYING() //fix for B*2170948
			OR SHOULD_PLAYER_BREAK_OUT_OF_SECONDARY_ANIM(LocalPlayer, TRUE)
			OR (iHackProgress = THERMITE_STATE_WAIT_FOR_EXPLOSION AND GET_GAME_TIMER() - iSafetyTimer >= THERMAL_CHARGE_DURATION - 500)
			OR iHackProgress = THERMITE_STATE_FAIL_CLEANUP
				IF NOT IS_BIT_SET(iThermiteBitset, THERMITE_BITSET_FORCED_ANIM_UPDATE_LAST_FRAME)
					CDEBUG1LN(DEBUG_MISSION, "[THERMITE] [RCC MISSION] [PROCESS_BLOW_OPEN_DOOR_WITH_THERMAL_CHARGE] Stopping cover eyes anim: fDistFromObject = ", fDistFromObject,
							 " fHeadingToObject = ", fHeadingToObject, " fPlayerHeading = ", fPlayerHeading, " timer = ", GET_GAME_TIMER() - iSafetyTimer)
				
					ANIM_DATA sAnimDataNone
					ANIM_DATA sAnimDataBlend
					INT iAnimFlags
					sAnimDataBlend.type = APT_SINGLE_ANIM
					sAnimDataBlend.anim0 = "cover_eyes_exit"
					sAnimDataBlend.dictionary0 = "anim@heists@ornate_bank@thermal_charge"
					sAnimDataBlend.phase0 = 0.0
					sAnimDataBlend.rate0 = 1.0
					sAnimDataBlend.filter = GET_HASH_KEY("BONEMASK_HEAD_NECK_AND_L_ARM")
					iAnimFlags = 0
					iAnimFlags += ENUM_TO_INT(AF_SECONDARY)
					iAnimFlags += ENUM_TO_INT(AF_UPPERBODY)
					sAnimDataBlend.flags = INT_TO_ENUM(ANIMATION_FLAGS, iAnimFlags)
					TASK_SCRIPTED_ANIMATION(LocalPlayerPed, sAnimDataBlend, sAnimDataNone, sAnimDataNone, DEFAULT, DEFAULT)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed)
					CLEAR_BIT(iThermiteBitset, THERMITE_BITSET_IS_COVER_EYES_ANIM_PLAYING)
					CLEAR_BIT(iThermiteBitset, THERMITE_BITSET_IS_COVER_EYES_INTRO_PLAYING)
					iTimeStoppedCoverEyesAnim = GET_GAME_TIMER()
					bForcedAnimUpdateThisFrame = TRUE
				ENDIF
			ELSE
				IF IS_BIT_SET(iThermiteBitset, THERMITE_BITSET_IS_COVER_EYES_INTRO_PLAYING)
					BOOL bAnimPlaying = IS_ENTITY_PLAYING_ANIM(LocalPlayerPed, "anim@heists@ornate_bank@thermal_charge", "cover_eyes_intro")
					IF (bAnimPlaying AND GET_ENTITY_ANIM_CURRENT_TIME(LocalPlayerPed, "anim@heists@ornate_bank@thermal_charge", "cover_eyes_intro") > 0.98)
					OR NOT bAnimPlaying
						ANIM_DATA sAnimDataNone
						ANIM_DATA sAnimDataBlend
						INT iAnimFlags
						sAnimDataBlend.type = APT_SINGLE_ANIM
						sAnimDataBlend.anim0 = "cover_eyes_loop"
						sAnimDataBlend.dictionary0 = "anim@heists@ornate_bank@thermal_charge"
						sAnimDataBlend.phase0 = 0.0
						sAnimDataBlend.rate0 = 1.0
						sAnimDataBlend.filter = GET_HASH_KEY("BONEMASK_HEAD_NECK_AND_L_ARM")
						iAnimFlags = 0
						iAnimFlags += ENUM_TO_INT(AF_SECONDARY)
						iAnimFlags += ENUM_TO_INT(AF_LOOPING)
						iAnimFlags += ENUM_TO_INT(AF_UPPERBODY)
						sAnimDataBlend.flags = INT_TO_ENUM(ANIMATION_FLAGS, iAnimFlags)
						TASK_SCRIPTED_ANIMATION(LocalPlayerPed, sAnimDataBlend, sAnimDataNone, sAnimDataNone, DEFAULT, DEFAULT)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed)
						CLEAR_BIT(iThermiteBitset, THERMITE_BITSET_IS_COVER_EYES_INTRO_PLAYING)
						bForcedAnimUpdateThisFrame = TRUE
						
						CDEBUG1LN(DEBUG_MISSION, "[THERMITE] [PROCESS_BLOW_OPEN_DOOR_WITH_THERMAL_CHARGE] Blending from intro to loop.")
					ENDIF
				ENDIF
			ENDIF
		ELSE
			//Give the player's weapon back as soon as the cover eyes anim has ended.
			IF iTimeStoppedCoverEyesAnim != 0
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_RELOAD)
			
				IF GET_GAME_TIMER() - iTimeStoppedCoverEyesAnim >= 700
					IF eWeaponBeforeMinigame != WEAPONTYPE_UNARMED
					AND HAS_PED_GOT_WEAPON(LocalPlayerPed, eWeaponBeforeMinigame)
						WEAPON_TYPE eCurrentWeapon
						GET_CURRENT_PED_WEAPON(LocalPlayerPed, eCurrentWeapon)
					
						CDEBUG1LN(DEBUG_MISSION, "[THERMITE] [RCC MISSION] [PROCESS_BLOW_OPEN_DOOR_WITH_THERMAL_CHARGE] Giving weapon back to player. ")
					
						//Only switch weapons if the player is unarmed, if they switched weapons before the anim ended then let them keep that weapon.
						IF eCurrentWeapon = WEAPONTYPE_UNARMED
							SET_CURRENT_PED_WEAPON(LocalPlayerPed, eWeaponBeforeMinigame)
						ENDIF
						
						eWeaponBeforeMinigame = WEAPONTYPE_UNARMED
					ENDIF
					
					iTimeWeaponReturnedAfterThermite = GET_GAME_TIMER()
					iTimeStoppedCoverEyesAnim = 0
				ENDIF
			ELSE
				//2239913 - Have the player's arm go back up if they turn around back towards the thermite.
				IF (iHackProgress = THERMITE_STATE_WAIT_FOR_EXPLOSION OR iHackProgress = THERMITE_STATE_DO_DOOR_SWAP)
				AND (GET_GAME_TIMER() - iSafetyTimer < THERMAL_CHARGE_DURATION - 500)
				AND (GET_GAME_TIMER() - iTimeWeaponReturnedAfterThermite > 800)
				AND NOT IS_CUTSCENE_PLAYING()
				AND NOT SHOULD_PLAYER_BREAK_OUT_OF_SECONDARY_ANIM(LocalPlayer, TRUE)
				AND NOT IS_PLAYER_FREE_AIMING(LocalPlayer)
				AND NOT IS_PED_RELOADING(LocalPlayerPed)
					VECTOR vPlayerPos = GET_ENTITY_COORDS(LocalPlayerPed)
					VECTOR vDoorPos = GET_ENTITY_COORDS(objMinigame)
					FLOAT fDistFromObject = VDIST(GET_ENTITY_COORDS(LocalPlayerPed), GET_ENTITY_COORDS(objMinigame))
					FLOAT fHeadingToObject = GET_HEADING_FROM_VECTOR_2D(vDoorPos.x - vPlayerPos.x, vDoorPos.y - vPlayerPos.y)
					FLOAT fPlayerHeading = GET_ENTITY_HEADING(LocalPlayerPed)
					FLOAT fHeadingDiff = ABSF(fHeadingToObject - fPlayerHeading)
			
					IF fHeadingDiff > 180.0
						fHeadingDiff = ABSF(fHeadingDiff - 360.0)
					ENDIF
					
					IF fDistFromObject <= 2.0
					AND fHeadingDiff < 90.0
						ANIM_DATA sAnimDataNone
						ANIM_DATA sAnimDataBlend
						INT iAnimFlags
						sAnimDataBlend.type = APT_SINGLE_ANIM
						sAnimDataBlend.anim0 = "cover_eyes_intro"
						sAnimDataBlend.dictionary0 = "anim@heists@ornate_bank@thermal_charge"
						sAnimDataBlend.phase0 = 0.0
						sAnimDataBlend.rate0 = 1.0
						sAnimDataBlend.filter = GET_HASH_KEY("BONEMASK_HEAD_NECK_AND_L_ARM")
						iAnimFlags = 0
						iAnimFlags += ENUM_TO_INT(AF_SECONDARY)
						iAnimFlags += ENUM_TO_INT(AF_HOLD_LAST_FRAME)
						iAnimFlags += ENUM_TO_INT(AF_UPPERBODY)
						sAnimDataBlend.flags = INT_TO_ENUM(ANIMATION_FLAGS, iAnimFlags)
						TASK_SCRIPTED_ANIMATION(LocalPlayerPed, sAnimDataBlend, sAnimDataNone, sAnimDataNone, DEFAULT, DEFAULT)
						//FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed)
						iTimeStoppedCoverEyesAnim = 0
						SET_BIT(iThermiteBitset, THERMITE_BITSET_IS_COVER_EYES_ANIM_PLAYING)
						SET_BIT(iThermiteBitset, THERMITE_BITSET_IS_COVER_EYES_INTRO_PLAYING)
						
						CDEBUG1LN(DEBUG_MISSION, "[THERMITE] [PROCESS_BLOW_OPEN_DOOR_WITH_THERMAL_CHARGE] Restarting cover eyes anim: fDistFromObject = ", fDistFromObject,
						 		  " fHeadingToObject = ", fHeadingToObject, " fPlayerHeading = ", fPlayerHeading)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//2242031 - Keep track of whether we called FORCE_PED_AI_AND_ANIMATION_UPDATE, in case the next frame a different state
		//tries to call it again.
		IF bForcedAnimUpdateThisFrame
			SET_BIT(iThermiteBitset, THERMITE_BITSET_FORCED_ANIM_UPDATE_LAST_FRAME)
		ELSE
			IF IS_BIT_SET(iThermiteBitset, THERMITE_BITSET_FORCED_ANIM_UPDATE_LAST_FRAME)
				CLEAR_BIT(iThermiteBitset, THERMITE_BITSET_FORCED_ANIM_UPDATE_LAST_FRAME)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles the creation of the main thermite synched scene: this is in a second function as there are 
///    multiple moments during the thermite minigame where it could be safe to start the anim.
PROC START_THERMITE_SYNCHRONIZED_SCENE(STRING strAnimDict, OBJECT_INDEX objMinigame, BOOL bIsKeypadObject = FALSE)
	
	STRING sAnimClip = "thermal_charge"
	IF bIsKeypadObject
		IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
			sAnimClip = "thermal_charge_female_female"
		ELSE
			sAnimClip = "thermal_charge_male_male"
		ENDIF
	ENDIF
	
	STOP_SYNC_SCENE(MC_playerBD[iPartToUse].iSynchSceneID)
		
	MC_playerBD[iPartToUse].iSynchSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(vThermitePlaybackPos, <<0,0, GET_ENTITY_HEADING(objMinigame)>>, EULER_YXZ, TRUE)
	NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(LocalPlayerPed, MC_playerBD[iPartToUse].iSynchSceneID, strAnimDict, sAnimClip, 
										  1.25, WALK_BLEND_OUT, 
										  SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_HIDE_WEAPON | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON | SYNCED_SCENE_ABORT_ON_DEATH, 
										  RBF_PLAYER_IMPACT, 1.25)
	
	//2185876 - Play different bag anims depending on what outfit the player is wearing.
	IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
		IF bIsKeypadObject
			NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(niMinigameObjects[0]), MC_playerBD[iPartToUse].iSynchSceneID, 
												 strAnimDict, "thermal_charge_female_p_m_bag_var22_arm_s", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
												 
			NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(niMinigameObjects[1]), MC_playerBD[iPartToUse].iSynchSceneID, 
											 strAnimDict, "thermal_charge_female_hei_prop_heist_thermite", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
												 
		ELSE
			IF SHOULD_USE_ARMOUR_BAG_ANIMS()
				NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(niMinigameObjects[0]), MC_playerBD[iPartToUse].iSynchSceneID, 
												 strAnimDict, "bag_thermal_charge", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
			ELSE
				NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(niMinigameObjects[0]), MC_playerBD[iPartToUse].iSynchSceneID, 
												 strAnimDict, "bag_thermal_charge_no_armour", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
			ENDIF
		ENDIF
	ELSE
		IF bIsKeypadObject
			NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(niMinigameObjects[0]), MC_playerBD[iPartToUse].iSynchSceneID, 
											 strAnimDict, "thermal_charge_male_p_m_bag_var22_arm_s", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
											 
			NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(niMinigameObjects[1]), MC_playerBD[iPartToUse].iSynchSceneID, 
											 strAnimDict, "thermal_charge_male_hei_prop_heist_thermite", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
											 
		ELIF SHOULD_USE_SUIT_BAG_ANIMS()
			NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(niMinigameObjects[0]), MC_playerBD[iPartToUse].iSynchSceneID, 
											 strAnimDict, "bag_thermal_charge_suit", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
		ELIF SHOULD_USE_ARMOUR_BAG_ANIMS()
			NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(niMinigameObjects[0]), MC_playerBD[iPartToUse].iSynchSceneID, 
											 strAnimDict, "bag_thermal_charge", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
		ELSE
			NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(niMinigameObjects[0]), MC_playerBD[iPartToUse].iSynchSceneID, 
											 strAnimDict, "bag_thermal_charge_no_armour", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
		ENDIF
	ENDIF
	
	IF NOT bIsKeypadObject
		ATTACH_ENTITY_TO_ENTITY(NET_TO_OBJ(niMinigameObjects[1]), LocalPlayerPed, GET_PED_BONE_INDEX(LocalPlayerPed, BONETAG_PH_R_HAND), 
								<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
	ENDIF
	
	NETWORK_START_SYNCHRONISED_SCENE(MC_playerBD[iPartToUse].iSynchSceneID)
	
	FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(NET_TO_OBJ(niMinigameObjects[0]))
	iThermiteBagStage = THERMITE_BAG_STAGE_PLAYERS_BAG_VISIBLE //This kicks off the checks for making the animated bag visible at the right time.
	iThermiteBagReturnStage = THERMITE_BAG_RETURN_STAGE_WAITING_FOR_ANIM //This kicks off the checks for returning the component bag near the end of the anim.
	
	CDEBUG1LN(DEBUG_MISSION, "[THERMITE] - [RCC MISSION] Starting thermite scene, vThermitePlaybackPos = ", vThermitePlaybackPos)
ENDPROC

/// PURPOSE:
///    Handles the swap from the animated bag back to the player's component bag that occurs near the end of the thermite anim.
///    This has been separated from the main thermite state logic for bug 2215313.
PROC UPDATE_THERMITE_BAG_RETURN()
	IF iThermiteBagReturnStage = THERMITE_BAG_RETURN_STAGE_WAITING_FOR_ANIM
		PRELOAD_DUFFEL_BAG_HEIST_GEAR(LocalPlayerPed, iLocalPart)
		
		//Give back the bag component, this may take a few frames so wait until the next stage before removing the bag object.
		IF (IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID))
		AND GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID)) >= 0.71)
		OR (NOT IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID)) AND MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer, 1000))
		OR IS_CUTSCENE_PLAYING() //If the fail cutscene kicks in just reset the bag immediately.
			APPLY_DUFFEL_BAG_HEIST_GEAR(LocalPlayerPed, iLocalPart)
			RELEASE_PRELOADED_MP_HEIST_OUTFIT(LocalPlayerPed)
			
			CDEBUG1LN(DEBUG_MISSION, "[THERMITE] UPDATE_THERMITE_BAG_RETURN - bringing back component bag.")
			iThermiteBagFadeTimer = GET_GAME_TIMER()
			iThermiteBagReturnStage = THERMITE_BAG_RETURN_STAGE_WAITING_FOR_COMPONENT_BAG	
		ENDIF
	ELIF iThermiteBagReturnStage = THERMITE_BAG_RETURN_STAGE_WAITING_FOR_COMPONENT_BAG
		IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(LocalPlayerPed)
			INT iTimeDiff = GET_GAME_TIMER() - iThermiteBagFadeTimer
			
			IF iTimeDiff > 350
				iTimeDiff = 350
			ENDIF
			
			INT iBagAlpha = ROUND(255.0 - ((TO_FLOAT(iTimeDiff) / 350.0) * 255.0))
		
			IF DOES_ENTITY_EXIST(NET_TO_OBJ(niMinigameObjects[0]))
				SET_ENTITY_ALPHA(NET_TO_OBJ(niMinigameObjects[0]), iBagAlpha, FALSE)
			ENDIF
		
			IF iTimeDiff >= 350
			OR IS_CUTSCENE_PLAYING()
				CDEBUG1LN(DEBUG_MISSION, "[THERMITE] UPDATE_THERMITE_BAG_RETURN - hiding animated bag.")
				SET_ENTITY_ALPHA(NET_TO_OBJ(niMinigameObjects[0]), 0, FALSE)
			
				//No longer deleting the bag in normal playthrough as this can cause synching issues with the bag on remote machines.
				//If the fail cutscene kicked in then delete the bag immediately.
				IF IS_CUTSCENE_PLAYING()
					CDEBUG1LN(DEBUG_MISSION, "[THERMITE] UPDATE_THERMITE_BAG_RETURN - mocap triggered, deleting bag object.")
					DELETE_BAG_OBJECT()	
				ENDIF

				iThermiteBagReturnStage = THERMITE_BAG_RETURN_STAGE_SWAP_COMPLETE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
	PROC UPDATE_THERMITE_DEBUG(OBJECT_INDEX objMinigame)
		IF bDebugIsThermiteDebuggingEnabled
			//Aallow movement of the minigame object.
			VECTOR vObjPos = GET_ENTITY_COORDS(objMinigame)
			BOOL bChangedPos = FALSE
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD8)
				vObjPos.y += 0.01
				bChangedPos = TRUE
			ENDIF
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD2)
				vObjPos.y -= 0.01
				bChangedPos = TRUE
			ENDIF
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD4)
				vObjPos.x -= 0.01
				bChangedPos = TRUE
			ENDIF
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD6)
				vObjPos.x += 0.01
				bChangedPos = TRUE
			ENDIF
			
			IF bChangedPos
				SET_ENTITY_COORDS_NO_OFFSET(objMinigame, vObjPos)
				PRINTLN("New thermite pos: ", vObjPos)
			ENDIF
			
			//Force debug a crowd control fail (useful for testing what happens to the thermite sequence when the fail cutscene triggers).
			//IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD5)
			//	sCCLocalData.eFailReason = CCFAIL_PEDKILLED
			//	SET_BIT_ENUM( MC_serverBD_3.sCCServerData.iBitSet, CCDATA_Failed)		
			//	BROADCAST_FMMC_CC_FAIL(ENUM_TO_INT(sCCLocalData.eFailReason), sCCLocalData.iFailCausedByPed, sCCLocalData.timeFailDelay)
			//ENDIF
			
			DRAW_DEBUG_CROSS(vObjPos, 0.5, 255, 0, 0, 255)
		ENDIF
	ENDPROC
#ENDIF

/// PURPOSE: Manage blowing up doors with thermite.
PROC PROCESS_BLOW_OPEN_DOOR_WITH_THERMAL_CHARGE(OBJECT_INDEX tempObj, INT iobj, BOOL bIsKeypadObject = FALSE)
	//If the player is wearing heels we need to use a different set of anims for the main scene.
	
	TEXT_LABEL_63 strAnimDict
	TEXT_LABEL_63 strAnimClip = "thermal_charge"
	
	IF bIsKeypadObject
		strAnimClip = "thermal_charge_male_male"
		strAnimDict = "anim_heist@hs3f@ig13_thermal_charge@thermal_charge@male@"
	ELSE
		strAnimDict = "anim@heists@ornate_bank@thermal_charge"
	ENDIF
	
	IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
		IF bIsKeypadObject
			strAnimClip = "thermal_charge_female_female"
			strAnimDict = "anim_heist@hs3f@ig13_thermal_charge@thermal_charge@female@"
		ELSE
			strAnimDict = "anim@heists@ornate_bank@thermal_charge_heels"
		ENDIF
	ENDIF
	
	VECTOR vAACoordOb 
	VECTOR vAACoordPro
	VECTOR vThermalChargeAAOffset = <<0.0, -2.8, 0.0>>
	vAACoordOb = GET_ENTITY_COORDS(tempObj)
	vAACoordOb.z = vAACoordOb.z + 2.0
	
	vAACoordPro = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj,vThermalChargeAAOffset)
	vAACoordPro.z = vAACoordPro.z - 1.5
	
	vGoToPoint = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj,vGoToPointOffset)

	IF MC_playerBD[iPartToUse].iObjNear = -1
		IF IS_ENTITY_IN_ANGLED_AREA( LocalPlayerPed, vAACoordOb, vAACoordPro, 3.000000, TRUE)
			SET_OBJ_NEAR(iObj)
		ENDIF
	ENDIF
	
	VECTOR vInitialRot
	FLOAT fGroundZTemp
	INT iPrevHackProgress = iHackProgress

	#IF IS_DEBUG_BUILD
		UPDATE_THERMITE_DEBUG(tempObj)
	#ENDIF

	//2200153 - The bag synched scene might start early, therefore we need to perform the timing checks for making the bag visible outside of the main stage checks.
	IF iThermiteBagStage = THERMITE_BAG_STAGE_PLAYERS_BAG_VISIBLE
		IF (IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID)) AND GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID)) >= 0.05)
		OR (NOT IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID)) AND MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer, 500))
			REMOVE_PLAYER_BAG()
			iThermiteBagStage = THERMITE_BAG_STAGE_OBJECT_BAG_INVISIBLE
		ENDIF
	ELIF iThermiteBagStage = THERMITE_BAG_STAGE_OBJECT_BAG_INVISIBLE
		//Do the bag swap once the scene is playing and the component bag is gone.
		IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(LocalPlayerPed)	
			SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[0]), TRUE)
			SET_ENTITY_ALPHA(NET_TO_OBJ(niMinigameObjects[0]), 255, FALSE)
			iThermiteBagStage = THERMITE_BAG_STAGE_OBJECT_BAG_VISIBLE
		ENDIF
	ENDIF
	
	INT iLocalOrderedPart = GET_ORDERED_PARTICIPANT_NUMBER(iLocalPart)

	IF MC_playerBD[iPartToUse].iObjHacked = -1
		IF MC_serverBD.iObjHackPart[iobj] = -1
			IF MC_playerBD[iPartToUse].iObjHacking = -1
				IF bPlayerToUseOK
					REQUEST_THERMITE_ASSETS(bIsKeypadObject)
				
					//2191877 - Special case: if the player already planted thermite but died before it finished then on the restart jump straight to the relevant section.
					IF ((DOES_PARTICLE_FX_LOOPED_EXIST(ptfxThermite[THERMITE_SPARK_EFFECT_INDEX]) AND IS_BIT_SET(iThermiteBitset, THERMITE_BITSET_IS_THERMITE_BURNING))
					OR (DOES_PARTICLE_FX_LOOPED_EXIST(ptfxThermiteLocalSpark[iLocalOrderedPart]) AND IS_BIT_SET(iThermiteLocalBitset[iLocalOrderedPart], THERMITE_LOCAL_BITSET_IS_THERMITE_BURNING)))
					AND HAVE_THERMITE_ASSETS_LOADED(bIsKeypadObject)
						MC_playerBD[iPartToUse].iObjHacking = iobj
						CLEAR_BIT(MC_playerBD[iPartToUse].iHackFailBitset, MC_playerBD[iPartToUse].iObjHacking)
						RESET_NET_TIMER(tdHackTimer)
						iHackLimitTimer = 0
						iHackProgress = THERMITE_STATE_DO_DOOR_SWAP
						CLEAR_BIT(iThermiteBitset, THERMITE_BITSET_IS_COVER_EYES_ANIM_PLAYING)
						CLEAR_BIT(iThermiteBitset, THERMITE_BITSET_HAS_DOOR_SOUND_TRIGGERED)
						PRINTLN("[THERMITE] [RCC MISSION] PROCESS_BLOW_OPEN_DOOR_WITH_THERMAL_CHARGE - Demolitions player died while waiting for thermite to finish, jumping to stage ", iHackProgress)
					ENDIF
				
					IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed,tempObj) < 6.0
						
						DRAW_DEBUG_CROSS(vGoToPoint,0.5,0,0,255,255)
						
						IF IS_ENTITY_IN_ANGLED_AREA( LocalPlayerPed, vAACoordOb, vAACoordPro, 3.000000)
							IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
								BOOL bCanInteractWithKeypad = TRUE
			
								IF bIsKeypadObject
									bCanInteractWithKeypad = IS_PLAYER_REQUIRED_DIST_TO_INTERACT_WITH_KEYPAD(LocalPlayerPed, tempObj)
								ENDIF
								
								BOOL bCrewBlockingThermite = IS_ANOTHER_PLAYER_BLOCKING_THERMITE_LOCATION(iobj, tempObj, 1.25, bIsKeypadObject, bCanInteractWithKeypad)
									
								IF NOT IS_PHONE_ONSCREEN()
								AND NOT IS_CUTSCENE_PLAYING()
								AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
								AND NOT IS_BROWSER_OPEN()
								AND bCanInteractWithKeypad
								AND NOT bCrewBlockingThermite
									IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
									AND IS_SAFE_TO_START_THERMITE()
										//Initialise these variables before doing anything else
										eWeaponBeforeMinigame = WEAPONTYPE_UNARMED
										iThermiteBitset = 0
										iThermiteBagStage = THERMITE_BAG_STAGE_ANIM_NOT_STARTED
										iThermiteBagReturnStage = THERMITE_BAG_RETURN_STAGE_ANIM_NOT_STARTED
										GET_CURRENT_PED_WEAPON(LocalPlayerPed, eWeaponBeforeMinigame)
									
										//fix for B*2174653 - task the ped to walk to the anim start point on the button press
										IF HAVE_THERMITE_ASSETS_LOADED(bIsKeypadObject)
											SEQUENCE_INDEX LocalSequenceIndex
										
											CLEAR_PED_TASKS(LocalPlayerPed)
											NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_ALLOW_PLAYER_DAMAGE|NSPC_PREVENT_EVERYBODY_BACKOFF|NSPC_LEAVE_CAMERA_CONTROL_ON|NSPC_ALLOW_PAD_SHAKE|NSPC_REENABLE_CONTROL_ON_DEATH)
											SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED, TRUE)
											
											fObjRot = GET_ENTITY_HEADING(tempObj)
											
											IF bIsKeypadObject
												vThermiteSceneOffsetFromObj = <<-0.02, 0.00, -0.08>>
											ELSE
												vThermiteSceneOffsetFromObj = <<-0.02, -0.05, -0.08>> //Keep in sync with FM_Mission_Controller_USING.sch
											ENDIF
											
											vThermitePlaybackPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj, vThermiteSceneOffsetFromObj)
											vThermiteGotoPoint = GET_ANIM_INITIAL_OFFSET_POSITION(strAnimDict, strAnimClip, vThermitePlaybackPos, <<0.0, 0.0, fObjRot>>)
											vInitialRot = GET_ANIM_INITIAL_OFFSET_ROTATION(strAnimDict, strAnimClip, vThermitePlaybackPos, <<0.0, 0.0, fObjRot>>)
											GET_GROUND_Z_FOR_3D_COORD(vThermiteGotoPoint + <<0.0, 0.0, 0.5>>, fGroundZTemp)
											vThermiteGotoPoint.z = fGroundZTemp
											
											IF bIsKeypadObject
												//Adjust Z of scene node to match ground pos, in case there are slight differences in where the creator objects were placed.
												vThermitePlaybackPos.z = vThermiteGotoPoint.z + 1.37
											ELSE
												//Adjust Z of scene node to match ground pos, in case there are slight differences in where the creator objects were placed.
												vThermitePlaybackPos.z = vThermiteGotoPoint.z + 1.08
											ENDIF
											
											IF IS_PLAYER_CLOSE_ENOUGH_TO_THERMITE_TRIGGER(strAnimDict, strAnimClip)
												CDEBUG1LN(DEBUG_MISSION, "[THERMITE] - [RCC MISSION] Attempting to play thermite scene early.")
											
												CREATE_BAG_OBJECTS_AT_POSITION(vThermitePlaybackPos + <<0.0, 0.0, -5.0>>, FALSE, TRUE)
												CREATE_THERMAL_CHARGE_OBJECT(TRUE)
												CREATE_FLASHING_THERMAL_CHARGE_OBJECT(GET_ENTITY_COORDS(PLAYER_PED_ID()), TRUE)
												
												IF NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[0])
												AND NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[1])
												AND NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[2])
													CDEBUG1LN(DEBUG_MISSION, "[THERMITE] - [RCC MISSION] Objects successfully created, playing thermite scene early.")
												
													START_THERMITE_SYNCHRONIZED_SCENE(strAnimDict, tempObj, bIsKeypadObject)
													SET_BIT(iThermiteBitset, THERMITE_BITSET_TRIGGERED_ANIMS_EARLY)
												ENDIF
											ELSE
												PRINTLN("[THERMITE] - [RCC MISSION] Distance between player and goto ", VDIST(GET_ENTITY_COORDS(LocalPlayerPed), vGoToPoint))
											
												OPEN_SEQUENCE_TASK(LocalSequenceIndex)
													TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vThermiteGotoPoint + <<0.0, 0.0, 0.1>>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, 0.2, 
																			      ENAV_STOP_EXACTLY | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED, vInitialRot.z)
												CLOSE_SEQUENCE_TASK(LocalSequenceIndex)
												TASK_PERFORM_SEQUENCE(LocalPlayerPed, LocalSequenceIndex)
												CLEAR_SEQUENCE_TASK(LocalSequenceIndex)
												
												SET_BIT(iThermiteBitset, THERMITE_BITSET_WALK_TASK_GIVEN_EARLY)
											ENDIF
										ENDIF

										PRINTLN("[THERMITE] [RCC MISSION] local player has started blow door with thermal charge, part: ", iPartToUse)
										
										IF bIsKeypadObject
											//Trigger processing event.
											BROADCAST_FMMC_THERMITE_EFFECT_EVENT(iLocalOrderedPart, iobj, OBJ_TO_NET(tempObj), <<0,0,0>>, <<0,0,0>>, FALSE, FALSE, TRUE, FALSE, FALSE)
											SET_BIT(iThermiteKeypadIsProcessing, iObj)
										ENDIF
										
										MC_playerBD[iPartToUse].iObjHacking = iobj
										CLEAR_BIT(MC_playerBD[iPartToUse].iHackFailBitset, MC_playerBD[iPartToUse].iObjHacking)
										RESET_NET_TIMER(tdHackTimer)
										iHackLimitTimer = 0
										iHackProgress = THERMITE_STATE_INITIALISE
									ELSE
										IF bIsKeypadObject
											IF NOT SHOULD_OBJECT_INTERACTION_HELP_TEXT_BE_BLOCKED()
												DISPLAY_HELP_TEXT_THIS_FRAME("MC_THERCH_1",TRUE)
											ENDIF
										ELSE
											DISPLAY_HELP_TEXT_THIS_FRAME("MC_THERCH_1",TRUE)
										ENDIF
									ENDIF
								ELSE
									IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MC_THERCH_1")
										CLEAR_HELP()
									ENDIF
									
									//Display help text if another player is blocking the thermite location.
									IF bCrewBlockingThermite
										IF bIsKeypadObject
											DISPLAY_HELP_TEXT_THIS_FRAME_FOR_MINIGAME("MC_THRBLK_KP", TRUE)
										ELSE
											DISPLAY_HELP_TEXT_THIS_FRAME_FOR_MINIGAME("MC_THERBLCK", TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			//Main flow.
			IF MC_playerBD[iPartToUse].iObjHacking != -1
				IF MC_serverBD.iObjHackPart[iobj] = iPartToUse
					SEQUENCE_INDEX ThisSequenceIndex

					#IF IS_DEBUG_BUILD
						IF bDebugIsThermiteDebuggingEnabled
						AND iHackProgress > 0
						AND iHackProgress <= THERMITE_STATE_START_ANIM 
							DRAW_DEBUG_CROSS(vGoToPoint,0.5,0,0,255,255)
						ENDIF
					#ENDIF
					
					IF iHackProgress < THERMITE_STATE_WAIT_FOR_EXPLOSION
						DISABLE_CELLPHONE_THIS_FRAME_ONLY()
					ENDIF
					
					IF bIsKeypadObject
						IF iHackProgress < THERMITE_STATE_DO_DOOR_SWAP
						AND NOT bPlayerToUseOK
							iHackProgress = THERMITE_STATE_FAIL_CLEANUP
							PRINTLN("[THERMITE] - [RCC MISSION] Player placing thermite no longer ok to use! Bailing to THERMITE_STATE_FAIL_CLEANUP.")
						ENDIF
					ENDIF

					//2090004 - Handle breaking the player out of the cover eyes anims and giving their weapon back.
					UPDATE_THERMITE_COVER_EYES_ANIM(tempObj)
					UPDATE_THERMITE_BAG_RETURN()
					
					BOOL bCanInteractWithKeypad = TRUE

					SWITCH iHackProgress
						CASE THERMITE_STATE_INITIALISE
							IF DOES_ENTITY_EXIST(tempObj)
								NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_ALLOW_PLAYER_DAMAGE | NSPC_PREVENT_EVERYBODY_BACKOFF | NSPC_LEAVE_CAMERA_CONTROL_ON | NSPC_REENABLE_CONTROL_ON_DEATH)
								REINIT_NET_TIMER(tdHackTimer)
								
								REQUEST_THERMITE_ASSETS(bIsKeypadObject)
								
								//Reset the thermite PTFX IDs: if something went wrong with a previous thermite door we want to guarantee we at least have clean IDs for the current door.
								IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxThermite[THERMITE_DRIP_EFFECT_INDEX])
									STOP_PARTICLE_FX_LOOPED(ptfxThermite[THERMITE_DRIP_EFFECT_INDEX])
								ENDIF
							
								IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxThermite[THERMITE_SPARK_EFFECT_INDEX])
									STOP_PARTICLE_FX_LOOPED(ptfxThermite[THERMITE_SPARK_EFFECT_INDEX])
								ENDIF
								
								IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxThermiteLocalDrip[iLocalOrderedPart])
									STOP_PARTICLE_FX_LOOPED(ptfxThermiteLocalDrip[iLocalOrderedPart])
								ENDIF
								
								IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxThermiteLocalSpark[iLocalOrderedPart])
									STOP_PARTICLE_FX_LOOPED(ptfxThermiteLocalSpark[iLocalOrderedPart])
								ENDIF
								
								ptfxThermite[THERMITE_DRIP_EFFECT_INDEX] = NULL
								ptfxThermite[THERMITE_SPARK_EFFECT_INDEX] = NULL
								
								ptfxThermiteLocalDrip[iLocalOrderedPart] = NULL
								ptfxThermiteLocalSpark[iLocalOrderedPart] = NULL
								
								iTimeStoppedCoverEyesAnim = 0
								iHackProgress = THERMITE_STATE_WAITING_FOR_ASSETS
							ENDIF
						BREAK
						
						CASE THERMITE_STATE_WAITING_FOR_ASSETS
							IF HAVE_THERMITE_ASSETS_LOADED(bIsKeypadObject)			
								IF bLocalPlayerPedOk
									IF NOT IS_BIT_SET(iThermiteBitset, THERMITE_BITSET_WALK_TASK_GIVEN_EARLY)
									AND NOT IS_BIT_SET(iThermiteBitset, THERMITE_BITSET_TRIGGERED_ANIMS_EARLY)
										CLEAR_PED_TASKS(LocalPlayerPed)	
									ENDIF
									
									SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED, TRUE)
									NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_ALLOW_PLAYER_DAMAGE|NSPC_PREVENT_EVERYBODY_BACKOFF|NSPC_LEAVE_CAMERA_CONTROL_ON|NSPC_ALLOW_PAD_SHAKE|NSPC_REENABLE_CONTROL_ON_DEATH)
									
									fObjRot = GET_ENTITY_HEADING(tempObj)
									
									IF bIsKeypadObject
										vThermiteSceneOffsetFromObj = <<-0.02, 0.00, -0.08>>
									ELSE
										vThermiteSceneOffsetFromObj = <<-0.02, -0.05, -0.08>> //Keep in sync with FM_Mission_Controller_USING.sch
									ENDIF
									
									vThermitePlaybackPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj, vThermiteSceneOffsetFromObj)
									vThermiteGotoPoint = GET_ANIM_INITIAL_OFFSET_POSITION(strAnimDict, strAnimClip, vThermitePlaybackPos, <<0.0, 0.0, fObjRot>>)
									vInitialRot = GET_ANIM_INITIAL_OFFSET_ROTATION(strAnimDict, strAnimClip, vThermitePlaybackPos, <<0.0, 0.0, fObjRot>>)
									GET_GROUND_Z_FOR_3D_COORD(vThermiteGotoPoint + <<0.0, 0.0, 0.5>>, fGroundZTemp)
									vThermiteGotoPoint.z = fGroundZTemp
									
									IF bIsKeypadObject
										//Adjust Z of scene node to match ground pos, in case there are slight differences in where the creator objects were placed.
										vThermitePlaybackPos.z = vThermiteGotoPoint.z + 1.37
									ELSE
										//Adjust Z of scene node to match ground pos, in case there are slight differences in where the creator objects were placed.
										vThermitePlaybackPos.z = vThermiteGotoPoint.z + 1.08
									ENDIF
									
									IF NOT IS_BIT_SET(iThermiteBitset, THERMITE_BITSET_WALK_TASK_GIVEN_EARLY)
									AND NOT IS_BIT_SET(iThermiteBitset, THERMITE_BITSET_TRIGGERED_ANIMS_EARLY)
										PRINTLN("[THERMITE] - [RCC MISSION] Distance between player and goto ", VDIST(GET_ENTITY_COORDS(LocalPlayerPed), vGoToPoint))
									
										OPEN_SEQUENCE_TASK(ThisSequenceIndex)
											TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vThermiteGotoPoint + <<0.0, 0.0, 0.1>>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, 0.2, 
																	      ENAV_STOP_EXACTLY | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED, vInitialRot.z)
										CLOSE_SEQUENCE_TASK(ThisSequenceIndex)
										TASK_PERFORM_SEQUENCE(LocalPlayerPed, ThisSequenceIndex)
										CLEAR_SEQUENCE_TASK(ThisSequenceIndex)
									ENDIF
									
									iSafetyTimer = GET_GAME_TIMER()
									iHackProgress = THERMITE_STATE_START_ANIM
									
									CLEAR_BIT(iThermiteBitset, THERMITE_BITSET_WALK_TASK_GIVEN_EARLY)
								ENDIF
							ELSE
								PRINTLN("[RCC MISSION] [THERMITE] WAITING FOR HAVE_THERMITE_ASSETS_LOADED")
							ENDIF
						BREAK
						
						CASE THERMITE_STATE_START_ANIM
							//2040428 - The bag needs to be created in advance to prevent popping when starting the animation.
							//Also create all the other objects in advance to prevent any delays playing the anim.
							CREATE_BAG_OBJECTS_AT_POSITION(vThermitePlaybackPos + <<0.0, 0.0, -5.0>>, FALSE, TRUE)
							CREATE_THERMAL_CHARGE_OBJECT(TRUE)
							CREATE_FLASHING_THERMAL_CHARGE_OBJECT(GET_ENTITY_COORDS(PLAYER_PED_ID()), TRUE)
						
							//Allow progress once the player is close enough to the synched scene start point (or if something went wrong).
							IF GET_SCRIPT_TASK_STATUS(LocalPlayerPed, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
							OR IS_BIT_SET(iThermiteBitset, THERMITE_BITSET_TRIGGERED_ANIMS_EARLY)
							OR IS_PLAYER_CLOSE_ENOUGH_TO_THERMITE_TRIGGER(strAnimDict, strAnimClip)
							OR MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer, 6000)
								IF HAS_ANIM_DICT_LOADED(strAnimDict)
									IF NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[0])
									AND NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[1])
									AND NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[2])
										IF DOES_ENTITY_HAVE_DRAWABLE(NET_TO_OBJ(niMinigameObjects[0]))
										AND DOES_ENTITY_HAVE_DRAWABLE(NET_TO_OBJ(niMinigameObjects[1]))
											
											IF bIsKeypadObject
												bCanInteractWithKeypad = IS_PLAYER_REQUIRED_DIST_TO_INTERACT_WITH_KEYPAD(LocalPlayerPed, tempObj)
											ENDIF	
											
											IF NOT IS_ANOTHER_PLAYER_BLOCKING_THERMITE_LOCATION(iobj, tempObj, 1.25, bIsKeypadObject, bCanInteractWithKeypad)
											OR IS_BIT_SET(iThermiteBitset, THERMITE_BITSET_TRIGGERED_ANIMS_EARLY)
												
												IF bIsKeypadObject 
												AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iTeamBitset4, ciBS4_TEAM_PER_PLAYER_THERMAL_CHARGES)
												AND MC_serverBD_1.iTeamThermalCharges[MC_playerBD[iPartToUse].iteam] = 0 //Check another player hasn't used the last thermite before this point.
													PRINTLN("[THERMITE] [RCC MISSION] Another player used the final thermal charge, cancelling tasks.")
													iHackProgress = THERMITE_STATE_FAIL_CLEANUP
													BREAK
												ENDIF
												
												IF NOT IS_BIT_SET(iThermiteBitset, THERMITE_BITSET_TRIGGERED_ANIMS_EARLY)
													START_THERMITE_SYNCHRONIZED_SCENE(strAnimDict, tempObj, bIsKeypadObject)
												ENDIF

												SET_PED_PROOFS_FOR_MINIGAME(LocalPlayerPed)
												
												vThermitePosWhenPlaced = <<0.0, 0.0, 0.0>>
												vThermiteRotWhenPlaced = <<0.0, 0.0, 0.0>>
												
												IF bIsKeypadObject
													//Deduct thermite total amount here if required. 
													IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iTeamBitset4, ciBS4_TEAM_PER_PLAYER_THERMAL_CHARGES)
														IF MC_playerBD_1[iPartToUse].iThermitePerPlayerRemaining = 1
															PRINTLN("[THERMITE] [RCC MISSION] Player is using their final remaining charge. Setting iThermiteKeypadWasLastPlayerCharge for ", iObj)
															SET_BIT(iThermiteKeypadWasLastPlayerCharge, iObj)	
														ENDIF
														
														IF MC_playerBD_1[iPartToUse].iThermitePerPlayerRemaining > 0
															MC_playerBD_1[iPartToUse].iThermitePerPlayerRemaining = (MC_playerBD_1[iPartToUse].iThermitePerPlayerRemaining - 1)
															PRINTLN("[THERMITE] [RCC MISSION] Player is using limited thermal charges and has just used one. ", MC_playerBD_1[iPartToUse].iThermitePerPlayerRemaining, " charges remain.")
														ENDIF
													ELSE
														BROADCAST_FMMC_THERMITE_EFFECT_EVENT(iLocalOrderedPart, iobj, OBJ_TO_NET(tempObj), <<0,0,0>>, <<0,0,0>>, FALSE, FALSE, FALSE, FALSE, FALSE, TRUE)
													ENDIF
												ENDIF
												
												CLEAR_BIT(iThermiteBitset, THERMITE_BITSET_TRIGGERED_ANIMS_EARLY)
												iSafetyTimer = GET_GAME_TIMER()
												iHackProgress = THERMITE_STATE_REMOVE_BAG
											ELSE
												PRINTLN("[THERMITE] [RCC MISSION] Another player blocking thermite pos, cancelling tasks.")
												
												iHackProgress = THERMITE_STATE_FAIL_CLEANUP
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						BREAK
						
						//The bag component needs to be removed at a specific point in the anim to get the smoothest transition. It also has to be removed before we make the bag object visible.
						//2200153 - the player bag removal is now done outside the main states due to the anim potentially starting early. This case is still here but shouldn't do anything.
						CASE THERMITE_STATE_REMOVE_BAG 
							IF (IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID)) AND GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID)) >= 0.07)
							OR (NOT IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID)) AND MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer, 500))
								//REMOVE_PLAYER_BAG()
								iHackProgress = THERMITE_STATE_WAIT_FOR_ANIM
							ENDIF
						BREAK
						
						CASE THERMITE_STATE_WAIT_FOR_ANIM
							INT iLocalSynchSceneID
							iLocalSynchSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID)
						
							IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSynchSceneID)
								//Set the thermite object visible when the player takes it out of the bag.
								IF NOT IS_BIT_SET(iThermiteBitset, THERMITE_BITSET_SET_THERMITE_OBJECT_VISIBLE)
									IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSynchSceneID) > 0.24
									OR (bIsKeypadObject AND HAS_ANIM_EVENT_FIRED(NET_TO_OBJ(niMinigameObjects[1]), HASH("Create")))
										SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[1]), TRUE)
										
										//2080062: we have to make the flashing thermite object visible before it's used as setting an entity visible fades in for remote players.
										//As it's not ready to be used yet we need to warp the object under the map, it can't be created under the map to begin with as that stops 
										//the light from rendering.
										SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[2]), TRUE)
										SET_ENTITY_COORDS_NO_OFFSET(NET_TO_OBJ(niMinigameObjects[2]), GET_ENTITY_COORDS(LocalPlayerPed) + <<0.0, 0.0, -5.0>>)
										
										PRINTLN("[THERMITE] - [RCC MISSION] [PROCESS_BLOW_OPEN_DOOR_WITH_THERMAL_CHARGE] Made thermite object visible.")
										SET_BIT(iThermiteBitset, THERMITE_BITSET_SET_THERMITE_OBJECT_VISIBLE)
									ENDIF
								ENDIF
								
								//Switch the standard thermite object with the flashing one once it's on the wall.
								IF NOT IS_BIT_SET(iThermiteBitset, THERMITE_BITSET_SET_THERMITE_OBJECT_TO_FLASH)
									IF HAS_ANIM_EVENT_FIRED(LocalPlayerPed, HASH("armed")) 
									OR GET_SYNCHRONIZED_SCENE_PHASE(iLocalSynchSceneID) > 0.54
										SET_ENTITY_COORDS_NO_OFFSET(NET_TO_OBJ(niMinigameObjects[2]), GET_ENTITY_COORDS(NET_TO_OBJ(niMinigameObjects[1])))
										SET_ENTITY_ROTATION(NET_TO_OBJ(niMinigameObjects[2]), GET_ENTITY_ROTATION(NET_TO_OBJ(niMinigameObjects[1])))
										FREEZE_ENTITY_POSITION(NET_TO_OBJ(niMinigameObjects[2]), TRUE)
										FORCE_OBJECT_ROOM_TO_MATCH_PLAYER(NET_TO_OBJ(niMinigameObjects[2]))
										SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[1]), FALSE)
										
										PRINTLN("[THERMITE] - [RCC MISSION] [PROCESS_BLOW_OPEN_DOOR_WITH_THERMAL_CHARGE] switched thermite object with flashing version.")
										SET_BIT(iThermiteBitset, THERMITE_BITSET_SET_THERMITE_OBJECT_TO_FLASH)
									ENDIF
								ENDIF
																
								//Store the correct position of the thermite object so we can freeze it there later.
								IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSynchSceneID) >= 0.45
								AND vThermitePosWhenPlaced.x = 0.0
								AND vThermitePosWhenPlaced.y = 0.0
								AND vThermitePosWhenPlaced.z = 0.0
									vThermitePosWhenPlaced = GET_ENTITY_COORDS(NET_TO_OBJ(niMinigameObjects[1]))
									vThermiteRotWhenPlaced = GET_ENTITY_ROTATION(NET_TO_OBJ(niMinigameObjects[1]))
									PRINTLN("[THERMITE] - [RCC MISSION] [PROCESS_BLOW_OPEN_DOOR_WITH_THERMAL_CHARGE] 1 - vThermitePosWhenPlaced: ", vThermitePosWhenPlaced, " vThermiteRotWhenPlaced: ", vThermiteRotWhenPlaced)
								ENDIF
							ENDIF
							
							//2234235 - Check if the crowd control fail cutscene has triggered during the thermite anim.
							//If so we need to deal with cleanup as the cutscene interferes with the normal flow.
							IF NOT IS_BIT_SET(iThermiteBitset, THERMITE_BITSET_MOCAP_TRIGGERED_DURING_ANIM)
								IF IS_CUTSCENE_PLAYING()
									CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] [THERMITE] - Mocap cutscene triggered during plant thermite anim.")
									SET_BIT(iThermiteBitset, THERMITE_BITSET_MOCAP_TRIGGERED_DURING_ANIM)
								ENDIF
							ENDIF
							
							//Debug: force the scene to terminate to test how it progresses.
//								#IF IS_DEBUG_BUILD
//									IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD5)
//										IF IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID))
//											NETWORK_STOP_SYNCHRONISED_SCENE(MC_playerBD[iPartToUse].iSynchSceneID)
//											MC_playerBD[iPartToUse].iSynchSceneID = -1
//										ENDIF
//									ENDIF
//								#ENDIF
							
							//Place the thermite in the correct position and make invisible (the particle effect will be played over the top).
							//This is synched with the player lifting their arm up.
							IF IS_ENTITY_VISIBLE_TO_SCRIPT(NET_TO_OBJ(niMinigameObjects[2]))
							AND IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSynchSceneID)
							AND GET_SYNCHRONIZED_SCENE_PHASE(iLocalSynchSceneID) >= 0.833
								ACTIVATE_THERMITE_EFFECT(tempObj, iobj, vThermitePosWhenPlaced, vThermiteRotWhenPlaced, vThermiteSparkEffectOffset, vThermiteDripEffectOffset, bIsKeypadObject)
							ENDIF

							IF (IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSynchSceneID)
							AND GET_SYNCHRONIZED_SCENE_PHASE(iLocalSynchSceneID) >= 0.9)
							OR (NOT IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSynchSceneID) AND MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer, 1000))
								//If the scene stopped early (e.g. a fail cutscene kicked in) then make sure everything is reset.
								IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSynchSceneID)						
									IF vThermitePosWhenPlaced.x = 0.0
									AND vThermitePosWhenPlaced.y = 0.0
									AND vThermitePosWhenPlaced.z = 0.0
										vThermitePosWhenPlaced = vThermitePlaybackPos
										vThermiteRotWhenPlaced = GET_ENTITY_ROTATION(tempObj)
										PRINTLN("[THERMITE] - [RCC MISSION] [PROCESS_BLOW_OPEN_DOOR_WITH_THERMAL_CHARGE] 2 - vThermitePosWhenPlaced: ", vThermitePosWhenPlaced, " vThermiteRotWhenPlaced: ", vThermiteRotWhenPlaced)
									ENDIF
									
									SET_ENTITY_COORDS_NO_OFFSET(NET_TO_OBJ(niMinigameObjects[2]), vThermitePosWhenPlaced)
									SET_ENTITY_ROTATION(NET_TO_OBJ(niMinigameObjects[2]), vThermiteRotWhenPlaced)
									SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[2]), TRUE)
									SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[1]), FALSE)
									
									PRINTLN("[THERMITE] - [RCC MISSION] PROCESS_BLOW_OPEN_DOOR_WITH_THERMAL_CHARGE: Thermite scene was terminated early.")
								ENDIF
								
								IF bIsKeypadObject //url:bugstar:6084762
									SET_BIT(iThermiteBitset, THERMITE_BITSET_FORCED_ANIM_UPDATE_LAST_FRAME)
								ENDIF
								
								iSafetyTimer = GET_GAME_TIMER()
								iHackProgress = THERMITE_STATE_DO_BAG_SWAP
							ENDIF
						BREAK
						
						//NOTE: the bag swap has been separated from the main flow as it now happens before the anim ends
						//see (UPDATE_THERMITE_BAG_RETURN), this stage can potentially be removed.
						CASE THERMITE_STATE_DO_BAG_SWAP //Attempts to synch the bag component being added and the object being removed.
							IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(LocalPlayerPed)
							OR MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer, 2000)								
								IF IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID))
									NETWORK_STOP_SYNCHRONISED_SCENE(MC_playerBD[iPartToUse].iSynchSceneID)
									MC_playerBD[iPartToUse].iSynchSceneID = -1
								ENDIF
							
								IF NOT IS_CUTSCENE_PLAYING() //fix for B*2242031
								AND NOT IS_BIT_SET(iThermiteBitset, THERMITE_BITSET_MOCAP_TRIGGERED_DURING_ANIM)
									ANIM_DATA sAnimDataNone
									ANIM_DATA sAnimDataBlend
									INT iAnimFlags
									sAnimDataBlend.type = APT_SINGLE_ANIM
									sAnimDataBlend.anim0 = "cover_eyes_loop"
									sAnimDataBlend.dictionary0 = "anim@heists@ornate_bank@thermal_charge"
									sAnimDataBlend.phase0 = 0.0
									sAnimDataBlend.rate0 = 1.0
									sAnimDataBlend.filter = GET_HASH_KEY("BONEMASK_HEAD_NECK_AND_L_ARM")
									iAnimFlags = 0
									iAnimFlags += ENUM_TO_INT(AF_SECONDARY)
									iAnimFlags += ENUM_TO_INT(AF_LOOPING)
									iAnimFlags += ENUM_TO_INT(AF_UPPERBODY)
									sAnimDataBlend.flags = INT_TO_ENUM(ANIMATION_FLAGS, iAnimFlags)
									TASK_SCRIPTED_ANIMATION(LocalPlayerPed, sAnimDataBlend, sAnimDataNone, sAnimDataNone, DEFAULT, DEFAULT)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed)
									iTimeStoppedCoverEyesAnim = 0
									SET_BIT(iThermiteBitset, THERMITE_BITSET_IS_COVER_EYES_ANIM_PLAYING)
								ENDIF
							
								RUN_LOCAL_PLAYER_PROOF_SETTING(TRUE)
							
								//Place the thermite in the correct position and make invisible (the particle effect will be played over the top).
								//NOTE: This gets run in the previous stage, so it should only run here if something went wrong with the synched scene.
								IF IS_ENTITY_VISIBLE_TO_SCRIPT(NET_TO_OBJ(niMinigameObjects[2]))
									ACTIVATE_THERMITE_EFFECT(tempObj, iobj, vThermitePosWhenPlaced, vThermiteRotWhenPlaced, vThermiteSparkEffectOffset, vThermiteDripEffectOffset, bIsKeypadObject)
								ENDIF
								
								iSafetyTimer = GET_GAME_TIMER()
								iHackProgress = THERMITE_STATE_DO_DOOR_SWAP
								
								//Debug widget: force a fail cleanup here. Use this to allow testing the thermite anims on the same door repeatedly.
								#IF IS_DEBUG_BUILD
									IF bDebugEnableThermiteReplay
										iHackProgress = THERMITE_STATE_FAIL_CLEANUP
									ENDIF
								#ENDIF
							ENDIF
						BREAK
						
						CASE THERMITE_STATE_DO_DOOR_SWAP
							//Allow player control once the blend out is complete.
							IF bLocalPlayerPedOk
							AND NOT IS_PLAYER_CONTROL_ON(LocalPlayer)
								IF GET_GAME_TIMER() - iSafetyTimer > 500
									NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
									
									//SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed,TRUE,FALSE)
									
									//IF eWeaponBeforeMinigame != WEAPONTYPE_UNARMED
									//AND HAS_PED_GOT_WEAPON(LocalPlayerPed, eWeaponBeforeMinigame)
									//	SET_CURRENT_PED_WEAPON(LocalPlayerPed, eWeaponBeforeMinigame)
									//ENDIF
								ENDIF
							ENDIF
						
							IF GET_GAME_TIMER() - iSafetyTimer > 6000
							OR IS_CUTSCENE_PLAYING() //fix for B*2170948
							OR IS_BIT_SET(iThermiteBitset, THERMITE_BITSET_MOCAP_TRIGGERED_DURING_ANIM)
								
								IF NOT bIsKeypadObject
									PRINTLN("[THERMITE] - [RCC MISSION] PROCESS_BLOW_OPEN_DOOR_WITH_THERMAL_CHARGE: broadcasting damage data, rule = ", MC_serverBD_4.iObjPriority[iobj][MC_playerBD[iPartToUse].iteam])	
									BROADCAST_SCRIPT_EVENT_SWAP_DOOR_FOR_DAMAGED_VERSION(MC_serverBD_4.iObjPriority[iobj][MC_playerBD[iPartToUse].iteam])
									//SET_BIT(MC_serverBD.iHackDoorThermiteDamageBitset, MC_serverBD_4.iObjPriority[iobj][MC_playerBD[iPartToUse].iteam])
								ENDIF
								
								iHackProgress = THERMITE_STATE_WAIT_FOR_EXPLOSION
							ENDIF
						BREAK
						
						CASE THERMITE_STATE_WAIT_FOR_EXPLOSION
							//Control the particle effect evo.
							IF GET_GAME_TIMER() - iSafetyTimer > (THERMAL_CHARGE_DURATION - 2000)
							AND (DOES_PARTICLE_FX_LOOPED_EXIST(ptfxThermite[THERMITE_SPARK_EFFECT_INDEX]) OR DOES_PARTICLE_FX_LOOPED_EXIST(ptfxThermiteLocalSpark[iLocalOrderedPart]))
								IF NOT bIsKeypadObject
									FLOAT fEvoRatio
									fEvoRatio = TO_FLOAT(GET_GAME_TIMER() - iSafetyTimer - (THERMAL_CHARGE_DURATION - 2000)) / 2000.0
									
									IF fEvoRatio > 1.0
										fEvoRatio = 1.0
									ENDIF
									
									SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxThermite[THERMITE_SPARK_EFFECT_INDEX], "DieOff", fEvoRatio)
								ELSE
									IF NOT IS_BIT_SET(iThermiteLocalBitset[iLocalOrderedPart], THERMITE_LOCAL_BITSET_START_LOCAL_SPARK_DIE_OFF_EVO)
										BROADCAST_FMMC_THERMITE_EFFECT_EVENT(iLocalOrderedPart, iObj, OBJ_TO_NET(tempObj), <<0,0,0>>, <<0,0,0>>, FALSE, TRUE, FALSE, FALSE, FALSE)
									ENDIF
								ENDIF
							ENDIF
							
							//2174662 - Play a sound effect of the door breaking.
							IF NOT IS_BIT_SET(iThermiteBitset, THERMITE_BITSET_HAS_DOOR_SOUND_TRIGGERED)
								IF GET_GAME_TIMER() - iSafetyTimer > (THERMAL_CHARGE_DURATION - 2000)
									IF bIsKeypadObject
										PLAY_SOUND_FROM_COORD(-1, "keypad_break", GET_ENTITY_COORDS(tempObj), "dlc_ch_heist_thermal_charge_sounds", TRUE, 30)
									ELSE
										PLAY_SOUND_FROM_COORD(-1, "Gate_Lock_Break", GET_ENTITY_COORDS(tempObj), "DLC_HEISTS_ORNATE_BANK_FINALE_SOUNDS", TRUE, 30)
									ENDIF
									
									IF bIsKeypadObject
										BROADCAST_FMMC_THERMITE_EFFECT_EVENT(iLocalOrderedPart, iObj, OBJ_TO_NET(tempObj), <<0,0,0>>, <<0,0,0>>, FALSE, TRUE, FALSE, TRUE, FALSE)
										
										//Update/Unlock any linked doors when passing.
										IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLinkedDoor1 > -1
											BROADCAST_FMMC_UPDATE_DOOR_OFF_RULE_EVENT(TRUE, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLinkedDoor1, ciUPDATE_DOOR_OFF_RULE_METHOD__THERMITE)
										ENDIF

										IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLinkedDoor2 > -1
											BROADCAST_FMMC_UPDATE_DOOR_OFF_RULE_EVENT(TRUE, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLinkedDoor2, ciUPDATE_DOOR_OFF_RULE_METHOD__THERMITE)
										ENDIF
										
										IF NOT IS_BIT_SET(sLEGACYMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciLEGACY_ContinuityGenericTrackingBS_ThermiteUsed)
											PRINTLN("[JS][CONTINUITY] - THERMITE - Setting thermite as used")
											SET_BIT(sLEGACYMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciLEGACY_ContinuityGenericTrackingBS_ThermiteUsed)
										ENDIF
									ENDIF
									
									SET_BIT(iThermiteBitset, THERMITE_BITSET_HAS_DOOR_SOUND_TRIGGERED)
								ENDIF
							ENDIF
							
							//The anim has to be timed with the spark PTFX stopping, so stop them here and then wait a bit longer before progressing to cleanup so the anim can finish.
							IF GET_GAME_TIMER() - iSafetyTimer > THERMAL_CHARGE_DURATION									
								IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxThermite[THERMITE_SPARK_EFFECT_INDEX])
									STOP_PARTICLE_FX_LOOPED(ptfxThermite[THERMITE_SPARK_EFFECT_INDEX])
								ENDIF
								
								IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxThermiteLocalSpark[iLocalOrderedPart])
									STOP_PARTICLE_FX_LOOPED(ptfxThermiteLocalSpark[iLocalOrderedPart])
								ENDIF
							ENDIF

							//Progress in the following cases:
							// - The 'cover eyes' anim has stopped.
							// - A mocap cutscene triggered while waiting for the door to open.
							IF MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer, THERMAL_CHARGE_DURATION + 500)
							OR IS_CUTSCENE_PLAYING() //fix for B*2170948
							OR IS_BIT_SET(iThermiteBitset, THERMITE_BITSET_MOCAP_TRIGGERED_DURING_ANIM)
							OR (GET_GAME_TIMER() - iSafetyTimer > THERMAL_CHARGE_DURATION AND iTimeStoppedCoverEyesAnim = 0 AND NOT IS_BIT_SET(iThermiteBitset, THERMITE_BITSET_IS_COVER_EYES_ANIM_PLAYING))
								iHackProgress = THERMITE_STATE_CLEANUP
							ENDIF
						BREAK

						CASE THERMITE_STATE_CLEANUP
						CASE THERMITE_STATE_FAIL_CLEANUP
							//The drip/smoke effect now continues after this rule ends: it'll be stopped after a certain amount of time or if the player plants another thermal charge.
							//IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxThermite[THERMITE_DRIP_EFFECT_INDEX])
							//	STOP_PARTICLE_FX_LOOPED(ptfxThermite[THERMITE_DRIP_EFFECT_INDEX])
							//ENDIF
							
							IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxThermite[THERMITE_SPARK_EFFECT_INDEX])
								STOP_PARTICLE_FX_LOOPED(ptfxThermite[THERMITE_SPARK_EFFECT_INDEX])
							ENDIF
							
							IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxThermiteLocalSpark[iLocalOrderedPart])
								STOP_PARTICLE_FX_LOOPED(ptfxThermiteLocalSpark[iLocalOrderedPart])
							ENDIF
							
							CLEAR_BIT(iThermiteBitset, THERMITE_BITSET_IS_THERMITE_BURNING)
							CLEAR_BIT(iThermiteLocalBitset[iLocalOrderedPart], THERMITE_LOCAL_BITSET_IS_THERMITE_BURNING)
							CLEAR_BIT(iThermiteBitset, THERMITE_BITSET_TRIGGERED_ANIMS_EARLY)
							CLEAR_BIT(iThermiteBitset, THERMITE_BITSET_WALK_TASK_GIVEN_EARLY)
							
							//If something failed with the thermite bag swap then force the player's bag back on here.
							IF iThermiteBagReturnStage != THERMITE_BAG_RETURN_STAGE_SWAP_COMPLETE
								CDEBUG1LN(DEBUG_MISSION, "[THERMITE] Bag return failed, forcing player's bag back on.")
							ENDIF
							
							DELETE_BAG_OBJECT()
							DELETE_THERMAL_CHARGE_OBJECT()
							DELETE_FLASHING_THERMAL_CHARGE_OBJECT()
							
							REMOVE_THERMITE_ASSETS(bIsKeypadObject)
							ENABLE_INTERACTION_MENU()
							
							IF NOT IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
								RELEASE_SCRIPT_AUDIO_BANK()
								PRINTLN("RELEASE_SCRIPT_AUDIO_BANK - Called from fm_mission_controller_minigame.sch 3")
							ENDIF

							IF bLocalPlayerPedOk
								SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed,TRUE,FALSE)
								NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
								RUN_LOCAL_PLAYER_PROOF_SETTING(TRUE)
								
								IF eWeaponBeforeMinigame != WEAPONTYPE_UNARMED
								AND HAS_PED_GOT_WEAPON(LocalPlayerPed, eWeaponBeforeMinigame)
									SET_CURRENT_PED_WEAPON(LocalPlayerPed, eWeaponBeforeMinigame)
								ENDIF
							ENDIF
							
							//If we passed the minigame then make sure we progress, otherwise the player needs to repeat the minigame.
							IF iHackProgress != THERMITE_STATE_FAIL_CLEANUP
								MC_playerBD[iPartToUse].iObjHacked = iobj
								INCREMENT_OFF_RULE_MINIGAME_SCORE_FROM_OBJECT(ciOffRuleScore_Low, iObj)
							ENDIF
							
							IF bIsKeypadObject
								IF iHackProgress != THERMITE_STATE_FAIL_CLEANUP
									//Set the minigame as completed off rule if needed.
									IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_AllowMinigameOffRule)
										BROADCAST_FMMC_UPDATE_MINIGAME_OFF_RULE_EVENT(TRUE, iObj)
									ENDIF
								ELSE
									BROADCAST_FMMC_THERMITE_EFFECT_EVENT(iLocalOrderedPart, iObj, OBJ_TO_NET(tempObj), <<0,0,0>>, <<0,0,0>>, FALSE, FALSE, FALSE, FALSE, TRUE)
									PRELOAD_DUFFEL_BAG_HEIST_GEAR(LocalPlayerPed, iLocalPart)
									APPLY_DUFFEL_BAG_HEIST_GEAR(LocalPlayerPed, iLocalPart)
									RELEASE_PRELOADED_MP_HEIST_OUTFIT(LocalPlayerPed)
								ENDIF
								
								iThermiteLocalSparkTimer[iLocalOrderedPart] = 0
								iThermiteLocalDripTimer[iLocalOrderedPart] = 0
								CLEAR_BIT(iThermiteLocalBitset[iLocalOrderedPart], THERMITE_LOCAL_BITSET_START_LOCAL_SPARK_DIE_OFF_EVO)
								CLEAR_BIT(iThermiteKeypadIsProcessing, iObj)
							ENDIF
							
							RESET_NET_TIMER(tdHackTimer)
							RESET_OBJ_HACKING_INT()
							iHackLimitTimer = 0
							iThermiteSmokeEffectTimer = 0
							iThermiteBagStage = THERMITE_BAG_STAGE_ANIM_NOT_STARTED
							iHackProgress = THERMITE_STATE_INITIALISE
						BREAK
					ENDSWITCH
				ENDIF
			ELSE
				IF bIsKeypadObject
					IF MC_serverBD.iObjHackPart[iobj] != -1
					AND MC_serverBD.iObjHackPart[iobj] != iLocalPart
				
						//Block using cover near the keypads for hacking if a hack is ongoing by another player.
						IF IS_PLAYER_REQUIRED_DIST_TO_INTERACT_WITH_KEYPAD(LocalPlayerPed, tempObj)
							DISABLE_CONTROL_ACTION(FRONTEND_CONTROL,INPUT_COVER)
							IF IS_PED_IN_COVER(LocalPlayerPed)
								CLEAR_PED_TASKS(LocalPlayerPed)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF iPrevHackProgress != iHackProgress
		PRINTLN("[THERMITE] - [RCC MISSION] PROCESS_BLOW_OPEN_DOOR_WITH_THERMAL_CHARGE: iHackProgress changed from ", iPrevHackProgress, " to ", iHackProgress)
	ENDIF
ENDPROC

///PURPOSE: This function controls hiding/showing a remote player's Heist Bag during the Thermite minigame
PROC PROCESS_REMOTE_PLAYER_THERMITE_OBJECTS(INT iPart, PED_INDEX ped, INT iLocalSyncScene)

	INT iOrderedPart = GET_ORDERED_PARTICIPANT_NUMBER(iPart)
	PRINTLN("PROCESS_REMOTE_PLAYER_THERMITE_OBJECTS - iPart: ", iPart, " iOrderedPart: ", iOrderedPart)
	IF iOrderedPart = -1
		EXIT
	ENDIF
	
	//PRELOAD_DUFFEL_BAG_HEIST_GEAR(ped, iPart)
	
	//First two bits are the intro: keep the bag object hidden until the correct phase.
	//Second two bits are the outro: the bag object visible until the correct phase.
	IF NOT IS_BIT_SET(iThermiteLocalBitset[iOrderedPart], THERMITE_LOCAL_BITSET_REMOTE_BAG_COMPONENT_REMOVED)
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSyncScene)
		OR GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_SYNCHRONIZED_SCENE) = PERFORMING_TASK
			//Make sure the bag starts hidden: this needs to happen before iLocalSyncScene is valid otherwise
			//you get a couple of frames where the bag is already playing the synched scene and is visible.
			OBJECT_INDEX bagObj = GET_CLOSEST_OBJECT_OF_TYPE(GET_ENTITY_COORDS(ped), 10.0, MC_playerBD_1[iPart].mnHeistGearBag, FALSE, FALSE, FALSE)
			
			IF DOES_ENTITY_EXIST(bagObj)
				CDEBUG1LN(DEBUG_MISSION, "[THERMITE] - remote bag object found, setting alpha to 0.")
				SET_ENTITY_ALPHA(bagObj, 0, FALSE)
			ENDIF
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSyncScene)
				IF IS_ENTITY_PLAYING_ANIM(ped, "anim@heists@ornate_bank@thermal_charge", "thermal_charge")
				OR IS_ENTITY_PLAYING_ANIM(ped, "anim@heists@ornate_bank@thermal_charge_heels", "thermal_charge")
				OR IS_ENTITY_PLAYING_ANIM(ped, "anim_heist@hs3f@ig13_thermal_charge@thermal_charge@male@", "thermal_charge_male_male")
				OR IS_ENTITY_PLAYING_ANIM(ped, "anim_heist@hs3f@ig13_thermal_charge@thermal_charge@female@", "thermal_charge_female_female")
					IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSyncScene) >= 0.05
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAND, 0, 0) 	//hide the bag component variation
						SET_BIT(iThermiteLocalBitset[iOrderedPart], THERMITE_LOCAL_BITSET_REMOTE_BAG_COMPONENT_REMOVED)
						CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] [THERMITE] - Removing bag component.")
					ELSE
						APPLY_DUFFEL_BAG_HEIST_GEAR(ped, iPart) //Make sure the bag component remains.
					ENDIF
				ENDIF
			ENDIF
		ELSE
			//Anims aren't running, reset the flags in case we need to check the bag again later.
			CLEAR_BIT(iThermiteLocalBitset[iOrderedPart], THERMITE_LOCAL_BITSET_REMOTE_BAG_COMPONENT_REMOVED)
			CLEAR_BIT(iThermiteLocalBitset[iOrderedPart], THERMITE_LOCAL_BITSET_REMOTE_BAG_OBJECT_REVEALED)
			CLEAR_BIT(iThermiteLocalBitset[iOrderedPart], THERMITE_LOCAL_BITSET_REMOTE_BAG_OBJECT_HIDDEN)
			CLEAR_BIT(iThermiteLocalBitset[iOrderedPart], THERMITE_LOCAL_BITSET_REMOTE_BAG_COMPONENT_RESTORED)
		ENDIF
	ELIF NOT IS_BIT_SET(iThermiteLocalBitset[iOrderedPart], THERMITE_LOCAL_BITSET_REMOTE_BAG_OBJECT_REVEALED)
		IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(ped)
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] [THERMITE] - Revealing bag object.")
			OBJECT_INDEX bagObj = GET_CLOSEST_OBJECT_OF_TYPE(GET_ENTITY_COORDS(ped), 0.5, MC_playerBD_1[iPart].mnHeistGearBag, FALSE, FALSE, FALSE)
			
			IF DOES_ENTITY_EXIST(bagObj)
				CDEBUG1LN(DEBUG_MISSION, "[THERMITE] - remote bag object found, setting alpha to 255.")
				SET_ENTITY_ALPHA(bagObj, 255, FALSE)
			ENDIF
			
			SET_BIT(iThermiteLocalBitset[iOrderedPart], THERMITE_LOCAL_BITSET_REMOTE_BAG_OBJECT_REVEALED)
		ENDIF
	ELIF NOT IS_BIT_SET(iThermiteLocalBitset[iOrderedPart], THERMITE_LOCAL_BITSET_REMOTE_BAG_COMPONENT_RESTORED)
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSyncScene)
		OR GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_SYNCHRONIZED_SCENE) = PERFORMING_TASK	
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSyncScene)
			AND GET_SYNCHRONIZED_SCENE_PHASE(iLocalSyncScene) >= 0.71
				iThermiteRemoteBagFadeTimer[iOrderedPart] = GET_GAME_TIMER()
				APPLY_DUFFEL_BAG_HEIST_GEAR(ped, iPart) //Restore the bag component variation
				SET_BIT(iThermiteLocalBitset[iOrderedPart], THERMITE_LOCAL_BITSET_REMOTE_BAG_COMPONENT_RESTORED)
				CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] [THERMITE] - Restoring bag component.")
			ELSE
				SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAND, 0, 0) //Make sure the bag remains hidden.
			ENDIF
		ELSE
			//Something has gone wrong: reset the bag state immediately.
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] [THERMITE] - Synchronized scene terminated mid-swap, resetting bag.")
			
			iThermiteRemoteBagFadeTimer[iOrderedPart] = 0 //Skip the fade.
			APPLY_DUFFEL_BAG_HEIST_GEAR(ped, iPart) //Restore the bag component variation
			SET_BIT(iThermiteLocalBitset[iOrderedPart], THERMITE_LOCAL_BITSET_REMOTE_BAG_COMPONENT_RESTORED)
		ENDIF
	ELIF NOT IS_BIT_SET(iThermiteLocalBitset[iOrderedPart], THERMITE_LOCAL_BITSET_REMOTE_BAG_OBJECT_HIDDEN)
		IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(ped)
			OBJECT_INDEX bagObj = GET_CLOSEST_OBJECT_OF_TYPE(GET_ENTITY_COORDS(ped), 0.5, MC_playerBD_1[iPart].mnHeistGearBag, FALSE, FALSE, FALSE)
			
			INT iTimeDiff = GET_GAME_TIMER() - iThermiteRemoteBagFadeTimer[iOrderedPart]

			IF iTimeDiff > 250
				iTimeDiff = 250
			ENDIF
			
			INT iBagAlpha = ROUND(255.0 - ((TO_FLOAT(iTimeDiff) / 500.0) * 255.0))
		
			IF DOES_ENTITY_EXIST(bagObj)
				CDEBUG1LN(DEBUG_MISSION, "[THERMITE] - remote bag object found, setting alpha to ", iBagAlpha)
				SET_ENTITY_ALPHA(bagObj, iBagAlpha, FALSE)
			ENDIF
			
			IF iTimeDiff >= 250
				IF DOES_ENTITY_EXIST(bagObj)
					SET_ENTITY_ALPHA(bagObj, 0, FALSE)
				ENDIF
				
				SET_BIT(iThermiteLocalBitset[iOrderedPart], THERMITE_LOCAL_BITSET_REMOTE_BAG_OBJECT_HIDDEN)
				CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] [THERMITE] - Hiding bag object.")
			ENDIF
		ENDIF
	ELSE
		//Once the scene terminates reset all the flags in case we need to run this again later.
		IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSyncScene)
		AND GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_SYNCHRONIZED_SCENE) != PERFORMING_TASK
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] [THERMITE] - Scene has ended, resetting flags.")
		
			CLEAR_BIT(iThermiteLocalBitset[iOrderedPart], THERMITE_LOCAL_BITSET_REMOTE_BAG_COMPONENT_REMOVED)
			CLEAR_BIT(iThermiteLocalBitset[iOrderedPart], THERMITE_LOCAL_BITSET_REMOTE_BAG_OBJECT_REVEALED)
			CLEAR_BIT(iThermiteLocalBitset[iOrderedPart], THERMITE_LOCAL_BITSET_REMOTE_BAG_OBJECT_HIDDEN)
			CLEAR_BIT(iThermiteLocalBitset[iOrderedPart], THERMITE_LOCAL_BITSET_REMOTE_BAG_COMPONENT_RESTORED)
		ENDIF
	ENDIF
	
ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: CASCO CONTAINER (used during The Prison Break Station heist prep) !
//
//************************************************************************************************************************************************************



/// PURPOSE: returns true if the casco lock has been shot off. 
///    		 When the lock has been shot off within process_blow_open_door() a player broadcast data bit is set. 
///    		 The lock however could have been shot off by another machine. 
///    		 Their player broadcast data will be updated. So our local machine can cycle through all the 
///    		 participants player broadcast data and if the bit is set then the funciton will return true. 
func bool has_casco_lock_been_shot_off()
	
	int i

	if is_bit_set(mc_playerbd[ilocalpart].iclientbitset2, pbbool2_casco_lock_shot_off)
		return true
	endif
	
	for i = 0 to GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		if is_bit_set(mc_playerbd[i].iclientbitset2, pbbool2_casco_lock_shot_off)
			set_bit(mc_playerbd[ilocalpart].iclientbitset2, pbbool2_casco_lock_shot_off)
			return true
		endif
	endfor 

	return false

endfunc

func bool allow_release_of_lock_shot_off_audio_bank()

	int i

	if is_bit_set(mc_playerbd[ilocalpart].iclientbitset2, PBBOOL2_RELEASE_LOCK_SHOT_OFF_AUDIO_BANK)
		return true
	endif 
	
	for i = 0 to GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		if is_bit_set(mc_playerbd[i].iclientbitset2, PBBOOL2_RELEASE_LOCK_SHOT_OFF_AUDIO_BANK)
			set_bit(mc_playerbd[ilocalpart].iclientbitset2, PBBOOL2_RELEASE_LOCK_SHOT_OFF_AUDIO_BANK)
			return true
		endif
	endfor 
	
	return false

endfunc 

func bool is_the_casco_lock_clear_of_the_container_doors()

	int i

	if is_bit_set(mc_playerbd[ilocalpart].iclientbitset2, pbbool2_casco_lock_clear_of_container_doors)
		return true
	endif
	
	for i = 0 to GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		if is_bit_set(mc_playerbd[i].iclientbitset2, pbbool2_casco_lock_clear_of_container_doors)
			set_bit(mc_playerbd[ilocalpart].iclientbitset2, pbbool2_casco_lock_clear_of_container_doors)
			return true
		endif
	endfor 
	
	return false

endfunc

#if is_debug_build
proc casco_lock_pfx_widget()

	INT lock 	= 2
	
	NETWORK_INDEX niLock = MC_serverBD_1.niMinigameCrateDoors[lock]

	if network_does_entity_exist_with_network_id(nilock)
	
		if network_has_control_of_network_id(nilock)

			if widget_activate_ptfx
			
				if HAS_NAMED_PTFX_ASSET_LOADED("scr_prison_break_heist_station")
				
					USE_PARTICLE_FX_ASSET("scr_prison_break_heist_station")
				
					//widget_ptfx = START_PARTICLE_FX_LOOPED_on_entity("scr_brk_metal_lock", nilock, widget_ptfx_pos, widget_ptfx_rot)
					widget_ptfx = start_networked_particle_fx_looped_on_entity("scr_brk_metal_lock", net_to_ent(nilock), widget_ptfx_pos, widget_ptfx_rot) 
				
					widget_activate_ptfx = false
					
				endif 
			
			else 
			
				if does_particle_fx_looped_exist(widget_ptfx)
					SET_PARTICLE_FX_LOOPED_OFFSETS(widget_ptfx, widget_ptfx_pos, widget_ptfx_rot)
				endif 
			
			endif
		
		else 
		
			NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(niLock)

		endif 
		
	endif 

endproc
#endif 

proc break_off_container_lock_system()

	INT right 	= 1
	INT lock 	= 2
	
	float lock_force_multiplayer = 2.0
	float fheightbelow		
	
	vector lock_target_pos = <<871.0291, -2882.0300, 18.0179>>
	vector lock_force_vec
	vector vright
	vector vlock 

	NETWORK_INDEX niRightDoor = MC_serverBD_1.niMinigameCrateDoors[right]
	NETWORK_INDEX niLock = MC_serverBD_1.niMinigameCrateDoors[lock]

	if network_does_entity_exist_with_network_id(nilock)
	
		if network_has_control_of_network_id(nilock)
		
			PRINTLN("[RCC MISSION] break_off_container_lock_system_status = ", break_off_container_lock_system_status)

			switch break_off_container_lock_system_status 
			
				case 0
				
					set_network_id_can_migrate(nilock, false)
					
					if request_script_audio_bank("dlc_mpheist/heist_container_lock_shoot")
						play_sound_from_entity(-1, "container_door", net_to_ent(nilock), "dlc_prison_break_heist_sounds", true)			
					endif
					
					detach_entity(net_to_ent(nilock))
					
					activate_physics(net_to_ent(nilock))
					set_entity_dynamic(net_to_ent(nilock), true)
					
					
					lock_force_vec = lock_target_pos - get_entity_coords(net_to_ent(nilock))
					
					normalise_vector(lock_force_vec)
					
					lock_force_vec.x *= lock_force_multiplayer
					lock_force_vec.y *= lock_force_multiplayer
					lock_force_vec.z *= lock_force_multiplayer

					//apply_force_to_entity(net_to_ent(nilock), apply_type_impulse, lock_force_vec, <<0.0, 0.0, 0.0>>, 0, false, true, true)
					set_entity_velocity(net_to_ent(nilock), lock_force_vec)
					
					if HAS_NAMED_PTFX_ASSET_LOADED("scr_prison_break_heist_station")
						USE_PARTICLE_FX_ASSET("scr_prison_break_heist_station")
						casco_lock_ptfx = start_networked_particle_fx_looped_on_entity("scr_brk_metal_lock", net_to_ent(nilock), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>) 
					endif 

					break_off_container_lock_system_status++
						
				break 
				
				case 1
					
					lock_force_vec = lock_target_pos - get_entity_coords(net_to_ent(nilock))
					
					normalise_vector(lock_force_vec)
					
					lock_force_vec.x *= lock_force_multiplayer
					lock_force_vec.y *= lock_force_multiplayer
					lock_force_vec.z *= lock_force_multiplayer

					//apply_force_to_entity(net_to_ent(nilock), apply_type_impulse, lock_force_vec, <<0.0, 0.0, 0.0>>, 0, false, true, true)
					set_entity_velocity(net_to_ent(nilock), lock_force_vec)

					set_bit(mc_playerbd[ilocalpart].iclientbitset2, pbbool2_casco_lock_clear_of_container_doors)
					
					PRINTLN("[RCC MISSION] apply force to lock")
					
					break_off_container_lock_system_status++
				
				break 
				
				case 2
				
					vright = get_entity_coords(net_to_ent(nirightdoor))
					
					fheightbelow = vright.z -0.7
					
					vlock = get_entity_coords(net_to_ent(nilock))
					
					if (vlock.z <= fheightbelow)

						if does_particle_fx_looped_exist(casco_lock_ptfx)
							stop_particle_fx_looped(casco_lock_ptfx)
						endif 
						
						cleanup_net_id(nilock)
						
						set_bit(mc_playerbd[ilocalpart].iclientbitset2, PBBOOL2_RELEASE_LOCK_SHOT_OFF_AUDIO_BANK)
						
						break_off_container_lock_system_status++
					
					endif
					
				break 
			
				case 3
			
				break 
				
			endswitch 
			
		else 
		
			PRINTLN("[RCC MISSION] network_has_control_of_network_id LOCK FALSE")
		
		endif 
		
	else 
	
		if not is_bit_set(iLocalBoolCheck10, LBOOL10_RELEASE_LOCK_SHOT_OFF_AUDIO_BANK)
			if allow_release_of_lock_shot_off_audio_bank()
				
				release_named_script_audio_bank("dlc_mpheist/heist_container_lock_shoot")
				
				set_bit(iLocalBoolCheck10, LBOOL10_RELEASE_LOCK_SHOT_OFF_AUDIO_BANK)
				
				PRINTLN("[RCC MISSION] break_off_container_lock_system - LBOOL10_RELEASE_LOCK_SHOT_OFF_AUDIO_BANK")
			endif 
		endif 
//	
//		PRINTLN("[RCC MISSION] network_does_entity_exist_with_network_id FALSE")
		
	endif   

endproc



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: GENERAL BLOW DOORS FUNCTIONS !
//
//************************************************************************************************************************************************************



FUNC BOOL I_AM_FIRST_BLOW_DOOR_PLAYER(INT iobj)
	
	INT iParticipant
	
	REPEAT GET_MAXIMUM_PLAYERS_ON_MISSION() iParticipant
		IF MC_serverBD_4.iObjRule[iobj][MC_playerBD[iParticipant].iteam] = FMMC_OBJECTIVE_LOGIC_MINIGAME
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet, cibsLEGACY_FMMC_MINI_GAME_TYPE_BLOW_DOOR)
				IF iParticipant = iLocalPart
					RETURN TRUE
				ELSE
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE

ENDFUNC

FUNC BOOL SHOULD_EXPLOSION_IN_AREA_OPEN_DOOR(VECTOR vOne,VECTOR vTwo)

	IF IS_EXPLOSION_ACTIVE_IN_AREA(EXP_TAG_DONTCARE, vOne, vTwo)
		RETURN TRUE
	ENDIF
	IF IS_EXPLOSION_ACTIVE_IN_AREA(EXP_TAG_STICKYBOMB, vOne, vTwo)
		RETURN TRUE
	ENDIF
	IF IS_EXPLOSION_ACTIVE_IN_AREA(EXP_TAG_GRENADE, vOne, vTwo)
		RETURN TRUE
	ENDIF
	IF IS_EXPLOSION_ACTIVE_IN_AREA(EXP_TAG_GRENADELAUNCHER, vOne, vTwo)
		RETURN TRUE
	ENDIF
	IF IS_EXPLOSION_ACTIVE_IN_AREA(EXP_TAG_ROCKET, vOne, vTwo)
		RETURN TRUE
	ENDIF	
	IF IS_EXPLOSION_ACTIVE_IN_AREA(EXP_TAG_TANKSHELL, vOne, vTwo)
		RETURN TRUE
	ENDIF
	IF IS_EXPLOSION_ACTIVE_IN_AREA(EXP_TAG_PROXMINE, vOne, vTwo)
		RETURN TRUE
	ENDIF
	IF IS_EXPLOSION_ACTIVE_IN_AREA(EXP_TAG_EXPLOSIVEAMMO | EXP_TAG_EXPLOSIVEAMMO_SHOTGUN, vOne, vTwo)
		RETURN TRUE
	ENDIF	
	
	RETURN FALSE

ENDFUNC

FUNC BOOL DID_PED_CAUSE_AN_EXPLOSION_IN_ANGLED_AREA(PED_INDEX tempPed, VECTOR vOne, VECTOR vTwo, FLOAT fArea)

	ENTITY_INDEX expOwner
	
	expOwner = GET_OWNER_OF_EXPLOSION_IN_ANGLED_AREA( EXP_TAG_STICKYBOMB, vOne, vTwo, farea )
	
	IF DOES_ENTITY_EXIST(expOwner)
	AND IS_ENTITY_A_PED(expOwner)
	AND (GET_PED_INDEX_FROM_ENTITY_INDEX(expOwner) = tempPed)
		RETURN TRUE
	ENDIF
	
	expOwner = GET_OWNER_OF_EXPLOSION_IN_ANGLED_AREA( EXP_TAG_GRENADE, vOne, vTwo, farea )
	
	IF DOES_ENTITY_EXIST(expOwner)
	AND IS_ENTITY_A_PED(expOwner)
	AND (GET_PED_INDEX_FROM_ENTITY_INDEX(expOwner) = tempPed)
		RETURN TRUE
	ENDIF
	
	expOwner = GET_OWNER_OF_EXPLOSION_IN_ANGLED_AREA( EXP_TAG_GRENADELAUNCHER, vOne, vTwo, farea )
	
	IF DOES_ENTITY_EXIST(expOwner)
	AND IS_ENTITY_A_PED(expOwner)
	AND (GET_PED_INDEX_FROM_ENTITY_INDEX(expOwner) = tempPed)
		RETURN TRUE
	ENDIF
	
	expOwner = GET_OWNER_OF_EXPLOSION_IN_ANGLED_AREA( EXP_TAG_ROCKET, vOne, vTwo, farea )
	
	IF DOES_ENTITY_EXIST(expOwner)
	AND IS_ENTITY_A_PED(expOwner)
	AND (GET_PED_INDEX_FROM_ENTITY_INDEX(expOwner) = tempPed)
		RETURN TRUE
	ENDIF
	
	expOwner = GET_OWNER_OF_EXPLOSION_IN_ANGLED_AREA( EXP_TAG_TANKSHELL, vOne, vTwo, farea )
	
	IF DOES_ENTITY_EXIST(expOwner)
	AND IS_ENTITY_A_PED(expOwner)
	AND (GET_PED_INDEX_FROM_ENTITY_INDEX(expOwner) = tempPed)
		RETURN TRUE
	ENDIF
	
	expOwner = GET_OWNER_OF_EXPLOSION_IN_ANGLED_AREA( EXP_TAG_PROXMINE, vOne, vTwo, farea )
	
	IF DOES_ENTITY_EXIST(expOwner)
	AND IS_ENTITY_A_PED(expOwner)
	AND (GET_PED_INDEX_FROM_ENTITY_INDEX(expOwner) = tempPed)
		RETURN TRUE
	ENDIF
	
	expOwner = GET_OWNER_OF_EXPLOSION_IN_ANGLED_AREA( EXP_TAG_EXPLOSIVEAMMO_SHOTGUN, vOne, vTwo, farea )
	
	IF DOES_ENTITY_EXIST(expOwner)
	AND IS_ENTITY_A_PED(expOwner)
	AND (GET_PED_INDEX_FROM_ENTITY_INDEX(expOwner) = tempPed)
		RETURN TRUE
	ENDIF
	
	expOwner = GET_OWNER_OF_EXPLOSION_IN_ANGLED_AREA( EXP_TAG_EXPLOSIVEAMMO, vOne, vTwo, farea )
	
	IF DOES_ENTITY_EXIST(expOwner)
	AND IS_ENTITY_A_PED(expOwner)
	AND (GET_PED_INDEX_FROM_ENTITY_INDEX(expOwner) = tempPed)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

//remove old thermal charge logic

/// PURPOSE: This appears to be what checks to see whether the player has blown up/opened a door in a heist
///   		 This detects when the casco container lock has been blown off
PROC PROCESS_BLOW_OPEN_DOOR( OBJECT_INDEX tempObj, INT iobj )

	FLOAT farea = 5.0
	
	VECTOR vEntityCoords = GET_ENTITY_COORDS( tempObj )
	
	VECTOR vOne = vEntityCoords - <<farea,farea,farea>>
	VECTOR vTwo = vEntityCoords + <<farea,farea,farea>>

	IF MC_playerBD[iLocalPart].iObjHacked = -1
		
		IF GET_ENTITY_MODEL( tempObj ) = PROP_CONTAINER_LD
		AND NETWORK_DOES_NETWORK_ID_EXIST( MC_serverBD_1.niMinigameCrateDoors[2] )
		
			if not is_ped_injured(player_ped_id()) //bug 2198693 only allow the lock to be shot off if player is alive.  
		
				//request audio for shooting the lock off in advance. Audio trigered via break_off_container_lock_system()
				if not is_bit_set(iLocalBoolCheck10, LBOOL10_container_lock_shot_audio_loaded)
					if request_script_audio_bank("dlc_mpheist/heist_container_lock_shoot")
						set_bit(iLocalBoolCheck10, LBOOL10_container_lock_shot_audio_loaded)
					endif 
				endif 
						
				PRINTLN("[RCC MISSION] process blow open door test 1:")
				
				OBJECT_INDEX oiLock = NET_TO_OBJ(MC_serverBD_1.niMinigameCrateDoors[2])
				
				GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(oiLock),vOne,vTwo)
				
				vOne = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oiLock, vOne)//tempObj, << -1.25, -6.075, 0 >> )
				vTwo = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oiLock, vTwo)//tempObj, << 1.25, -6.075, 2.5 >> )
								
				vector explosion_offset_bl = get_offset_from_entity_in_world_coords(oiLock, <<-1.5, -1.5, -1.5>>) 
				vector explosion_offset_tr = get_offset_from_entity_in_world_coords(oiLock, <<1.5, 1.5, 1.5>>) 
				
				is_entity_in_area(player_ped_id(), explosion_offset_bl, explosion_offset_tr)
				
				fArea = 0.4
				
				is_entity_in_angled_area(player_ped_id(), vOne, vTwo, fArea) //debug to see the area box render
								
				IF ((IS_BULLET_IN_ANGLED_AREA( vOne, vTwo, fArea, TRUE ) OR IS_EXPLOSION_ACTIVE_IN_AREA(exp_tag_dontcare, explosion_offset_bl, explosion_offset_tr) OR IS_EXPLOSION_IN_AREA(EXP_TAG_EXPLOSIVEAMMO | EXP_TAG_EXPLOSIVEAMMO_SHOTGUN, explosion_offset_bl, explosion_offset_tr))
				AND (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD( LocalPlayerPed, vOne ) < 90))
					
					MC_playerBD[ iLocalPart ].iObjHacked = iobj
					
					set_entity_proofs(player_ped_id(), true, true, true, true, true)//proof the player to avoid death via an explosion rpg which can mess up the cutscene logic. Bug 2198693
					
					set_bit(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_CASCO_LOCK_SHOT_OFF) 
					
					PRINTLN("[RCC MISSION] LK OPEN HACK DOOR CONDITIONS MET: ")
		
				ENDIF
				
			endif 
			
		ELSE
		
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[ iobj ].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_USE_THERMAL_CHARGE)
				if (IS_EXPLOSION_ACTIVE_IN_AREA(EXP_TAG_DONTCARE, vOne, vTwo)
				OR IS_EXPLOSION_IN_AREA(EXP_TAG_EXPLOSIVEAMMO | EXP_TAG_EXPLOSIVEAMMO_SHOTGUN, vOne, vTwo))			
					
					PRINTLN("[RCC MISSION]  blow past exp in area: ",iobj, " ipart: ",iPartToUse)
					IF DOES_ENTITY_EXIST(GET_OWNER_OF_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE, vOne, vTwo, farea ) )
						PRINTLN("[RCC MISSION]  blow past exp in has owner: ",iobj, " ipart: ",iPartToUse)
						IF IS_ENTITY_A_PED(GET_OWNER_OF_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE, vOne, vTwo, farea ) )
							PRINTLN("[RCC MISSION]  blow past ped check ",iobj, " ipart: ",iPartToUse)
							IF DID_PED_CAUSE_AN_EXPLOSION_IN_ANGLED_AREA(LocalPlayerPed, vOne, vTwo, fArea)
								PRINTLN( "[RCC MISSION]  local player has registered explosion near obj: ",iobj, " ipart: ",iLocalPart )
								MC_playerBD[ iLocalPart ].iObjHacked = iobj
							ELSE
								IF SHOULD_EXPLOSION_IN_AREA_OPEN_DOOR(vOne, vTwo)
									PRINTLN( "[RCC MISSION]  local player has registered explosion near obj even though was not him: ",iobj, " ipart: ",iLocalPart )
									IF I_AM_FIRST_BLOW_DOOR_PLAYER(iobj)
										PRINTLN( "[RCC MISSION]  local player has registered explosion near obj even though was not him: ",iobj, " ipart: ",iLocalPart )
										MC_playerBD[ iLocalPart ].iObjHacked = iobj
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
					PRINTLN( "[RCC MISSION]  PROCESS_BLOW_OPEN_DOOR_WITH_THERMAL_CHARGE ")
					PROCESS_BLOW_OPEN_DOOR_WITH_THERMAL_CHARGE(tempObj,iobj)
			ENDIF
		ENDIF

	ENDIF
	
ENDPROC



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: CASH GRAB MINIGAME !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************




FUNC BOOL CREATE_MONEY_OBJECT()
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[1])
		REQUEST_MODEL(HEI_PROP_HEIST_CASH_PILE)
		IF HAS_MODEL_LOADED(HEI_PROP_HEIST_CASH_PILE)
			IF NOT IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_MONEY)
				MC_RESERVE_NETWORK_MISSION_OBJECTS(1)
				SET_BIT(iLocalBoolCheck12,LBOOL12_OBJ_RES_MONEY)
			ENDIF
			IF IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_MONEY)
				IF CAN_REGISTER_MISSION_OBJECTS(1)
					PRINTLN("[RCC MISSION] CREATE_MONEY_OBJECT - GET_NUM_RESERVED_MISSION_OBJECTS: ", GET_NUM_RESERVED_MISSION_OBJECTS(FALSE))
					IF CREATE_NET_OBJ(niMinigameObjects[1], HEI_PROP_HEIST_CASH_PILE, GET_ENTITY_COORDS(LocalPlayerPed), bIsLocalPlayerHost)
						FREEZE_ENTITY_POSITION(NET_TO_OBJ(niMinigameObjects[1]), TRUE)
						SET_ENTITY_COLLISION(NET_TO_OBJ(niMinigameObjects[1]), FALSE)
						ATTACH_ENTITY_TO_ENTITY(NET_TO_OBJ(niMinigameObjects[1]),LocalPlayerPed,GET_PED_BONE_INDEX(LocalPlayerPed,BONETAG_PH_L_HAND),<<0,0,0>>,<<0,0,0>>)
						SET_ENTITY_INVINCIBLE(NET_TO_OBJ(niMinigameObjects[1]), TRUE)
						SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[1]), FALSE)
						SET_MODEL_AS_NO_LONGER_NEEDED(HEI_PROP_HEIST_CASH_PILE)
						
						PRINTLN("[RCC MISSION] CREATE_MONEY_OBJECT: successfully created money.")
						CLEAR_BIT(iLocalBoolCheck12,LBOOL12_OBJ_RES_MONEY)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL CREATE_COKE_OBJECT()
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[1])
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_impexp_coke_pile")))
		IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_impexp_coke_pile")))
			IF NOT IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_MONEY)
				MC_RESERVE_NETWORK_MISSION_OBJECTS(1)
				SET_BIT(iLocalBoolCheck12,LBOOL12_OBJ_RES_MONEY)
			ENDIF
			IF IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_MONEY)
				IF CAN_REGISTER_MISSION_OBJECTS(1)
					PRINTLN("[RCC MISSION] CREATE_COKE_OBJECT - GET_NUM_RESERVED_MISSION_OBJECTS: ", GET_NUM_RESERVED_MISSION_OBJECTS(FALSE))
					IF CREATE_NET_OBJ(niMinigameObjects[1], INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_impexp_coke_pile")), GET_ENTITY_COORDS(LocalPlayerPed), bIsLocalPlayerHost)
						FREEZE_ENTITY_POSITION(NET_TO_OBJ(niMinigameObjects[1]), TRUE)
						SET_ENTITY_COLLISION(NET_TO_OBJ(niMinigameObjects[1]), FALSE)
						ATTACH_ENTITY_TO_ENTITY(NET_TO_OBJ(niMinigameObjects[1]),LocalPlayerPed,GET_PED_BONE_INDEX(LocalPlayerPed,BONETAG_PH_L_HAND),<<0,0,0>>,<<0,0,0>>)
						SET_ENTITY_INVINCIBLE(NET_TO_OBJ(niMinigameObjects[1]), TRUE)
						SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[1]), FALSE)
						SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_impexp_coke_pile")))
						
						PRINTLN("[RCC MISSION] CREATE_COKE_OBJECT: successfully created money.")
						CLEAR_BIT(iLocalBoolCheck12,LBOOL12_OBJ_RES_MONEY)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL CREATE_DIAMOND_OBJECT()
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[1])
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Vault_Dimaondbox_01a")))
		IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Vault_Dimaondbox_01a")))
			IF NOT IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_MONEY)
				MC_RESERVE_NETWORK_MISSION_OBJECTS(1)
				SET_BIT(iLocalBoolCheck12,LBOOL12_OBJ_RES_MONEY)
			ENDIF
			IF IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_MONEY)
				IF CAN_REGISTER_MISSION_OBJECTS(1)
					PRINTLN("[RCC MISSION] CREATE_COKE_OBJECT - GET_NUM_RESERVED_MISSION_OBJECTS: ", GET_NUM_RESERVED_MISSION_OBJECTS(FALSE))
					IF CREATE_NET_OBJ(niMinigameObjects[1], INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Vault_Dimaondbox_01a")), GET_ENTITY_COORDS(LocalPlayerPed), bIsLocalPlayerHost)
						FREEZE_ENTITY_POSITION(NET_TO_OBJ(niMinigameObjects[1]), TRUE)
						SET_ENTITY_COLLISION(NET_TO_OBJ(niMinigameObjects[1]), FALSE)
						ATTACH_ENTITY_TO_ENTITY(NET_TO_OBJ(niMinigameObjects[1]),LocalPlayerPed,GET_PED_BONE_INDEX(LocalPlayerPed,BONETAG_PH_L_HAND),<<0,0,0>>,<<0,0,0>>)
						SET_ENTITY_INVINCIBLE(NET_TO_OBJ(niMinigameObjects[1]), TRUE)
						SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[1]), FALSE)
						SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Vault_Dimaondbox_01a")))
						
						PRINTLN("[RCC MISSION] CREATE_DIAMOND_OBJECT: successfully created Diamond Box.")
						CLEAR_BIT(iLocalBoolCheck12,LBOOL12_OBJ_RES_MONEY)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL CREATE_GOLD_OBJECT()
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[1])
		REQUEST_MODEL(CH_PROP_GOLD_BAR_01A)
		IF HAS_MODEL_LOADED(CH_PROP_GOLD_BAR_01A)
			IF NOT IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_MONEY)
				MC_RESERVE_NETWORK_MISSION_OBJECTS(1)
				SET_BIT(iLocalBoolCheck12,LBOOL12_OBJ_RES_MONEY)
			ENDIF
			IF IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_MONEY)
				IF CAN_REGISTER_MISSION_OBJECTS(1)
					PRINTLN("[RCC MISSION] CREATE_COKE_OBJECT - GET_NUM_RESERVED_MISSION_OBJECTS: ", GET_NUM_RESERVED_MISSION_OBJECTS(FALSE))
					IF CREATE_NET_OBJ(niMinigameObjects[1], CH_PROP_GOLD_BAR_01A, GET_ENTITY_COORDS(LocalPlayerPed), bIsLocalPlayerHost)
						FREEZE_ENTITY_POSITION(NET_TO_OBJ(niMinigameObjects[1]), TRUE)
						SET_ENTITY_COLLISION(NET_TO_OBJ(niMinigameObjects[1]), FALSE)
						ATTACH_ENTITY_TO_ENTITY(NET_TO_OBJ(niMinigameObjects[1]),LocalPlayerPed,GET_PED_BONE_INDEX(LocalPlayerPed,BONETAG_PH_L_HAND),<<0,0,0>>,<<0,0,0>>)
						SET_ENTITY_INVINCIBLE(NET_TO_OBJ(niMinigameObjects[1]), TRUE)
						SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[1]), FALSE)
						SET_MODEL_AS_NO_LONGER_NEEDED(CH_PROP_GOLD_BAR_01A)
						
						PRINTLN("[RCC MISSION] CREATE_GOLD_OBJECT: successfully created Gold Bar.")
						CLEAR_BIT(iLocalBoolCheck12,LBOOL12_OBJ_RES_MONEY)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC
PROC DELETE_MONEY_OBJECT()
	OBJECT_INDEX tempObj
	IF NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[1])
		tempObj = NET_TO_OBJ(niMinigameObjects[1])
		DELETE_OBJECT(tempObj)
		MC_RESERVE_NETWORK_MISSION_OBJECTS(-1)
	ENDIF
ENDPROC

PROC UPDATE_CASH_GRAB_SCRIPTED_CAMERA(S_CASH_GRAB_DATA &data, OBJECT_INDEX tempObj)

	CONST_FLOAT CAM_ROT_SPEED 			20.0
	CONST_FLOAT CAM_ROT_MIN				-9.0
	CONST_FLOAT CAM_ROT_MAX				9.0
	CONST_FLOAT CAM_HEIGHT_SPEED 		1.0
	CONST_FLOAT CAM_HEIGHT_MIN			-0.1
	CONST_FLOAT CAM_HEIGHT_MAX			0.5
	
	IF DOES_ENTITY_EXIST(tempObj)

		SWITCH data.eState
		
			CASE HBGS_INTRO_ANIM
			
				IF NOT DOES_CAM_EXIST(data.cameraIndex)
				
					//check if ped is playing intro anim first before starting camera animation
					IF IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID))
					//OR IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sCashGrabData.iTrolleySceneID))
					
						IF GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID)) > 0.050
						//IF IS_ENTITY_PLAYING_ANIM(LocalPlayerPed, GET_HEIST_BAG_MINIGAME_ANIM_DICT(tempObj, IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS)), "intro")
					
							data.cameraIndex 	= CREATE_CAMERA(CAMTYPE_SCRIPTED, FALSE)

							SET_CAM_COORD(data.cameraIndex, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj, vCashGrabCamAttachOffsetEnd))
							POINT_CAM_AT_ENTITY(data.cameraIndex, tempObj, vCashGrabCamPointOffsetEnd)
							SET_CAM_FOV(data.cameraIndex, 55.6419)
							
							SET_CAM_ACTIVE(data.cameraIndex, TRUE)
							SET_CAM_PARAMS(data.cameraIndex, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj, vCashGrabCamAttachOffsetStart), vCashGrabCamRotationStart, 55.6419)
							SET_CAM_PARAMS(data.cameraIndex, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj, vCashGrabCamAttachOffsetEnd), vCashGrabCamRotationEnd, 55.6419, 2000,
										   GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)
							
							SHAKE_CAM(data.cameraIndex, "HAND_SHAKE", 0.1)
							
							data.vCamStartPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj, vCashGrabCamAttachOffsetEnd)
							data.vCamPointPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj, vCashGrabCamPointOffsetEnd)
							
							data.fCamRotOffset				= 0.0
							data.fDesiredCamRotOffset 		= 0.0
							data.fCamHeightOffset 			= 0.0
							data.fDesiredCamHeightOffset 	= 0.0
							
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_ShowHUDDuringCashGrab)
								DISPLAY_HUD(FALSE)
								DISPLAY_RADAR(FALSE)
							ENDIF
							
							RENDER_SCRIPT_CAMS(TRUE, FALSE)
							
						ENDIF
					ENDIF

				ENDIF
				
			BREAK
			
			CASE HBGS_IDLE_ANIM
			CASE HBGS_GRAB_ANIM
			CASE HBGS_EXIT_ANIM
			
				IF DOES_CAM_EXIST(data.cameraIndex)
				
					VECTOR 	vPosDiff
					FLOAT 	fDesiredCamSpeedZ
					FLOAT 	fDesiredCamSpeedHeight
				
					vPosDiff 					= data.vCamStartPos - data.vCamPointPos
					fDesiredCamSpeedZ 			= GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X) * CAM_ROT_SPEED
					data.fDesiredCamRotOffset 	= data.fDesiredCamRotOffset +@ fDesiredCamSpeedZ
					
					#IF IS_DEBUG_BUILD
						DRAW_DEBUG_SPHERE(data.vCamStartPos, 0.025, 255, 0, 0)
						DRAW_DEBUG_SPHERE(data.vCamPointPos, 0.025, 0, 255, 0)
					#ENDIF
					
					IF data.fDesiredCamRotOffset > CAM_ROT_MAX
					      data.fDesiredCamRotOffset = CAM_ROT_MAX
					ELIF data.fDesiredCamRotOffset < CAM_ROT_MIN
					      data.fDesiredCamRotOffset = CAM_ROT_MIN
					ENDIF

					data.fCamRotOffset = COSINE_INTERP_FLOAT(data.fCamRotOffset, data.fDesiredCamRotOffset, 0.5)
					
					ROTATE_VECTOR_FMMC(vPosDiff, <<0.0, 0.0, data.fCamRotOffset>>)
					
					//Allow the player to move the camera up/down.
					fDesiredCamSpeedHeight = -GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) * CAM_HEIGHT_SPEED
					
					data.fDesiredCamHeightOffset = data.fDesiredCamHeightOffset +@ fDesiredCamSpeedHeight
					
					IF data.fDesiredCamHeightOffset > CAM_HEIGHT_MAX
						data.fDesiredCamHeightOffset = CAM_HEIGHT_MAX
					ELIF data.fDesiredCamHeightOffset < CAM_HEIGHT_MIN
						data.fDesiredCamHeightOffset = CAM_HEIGHT_MIN
					ENDIF
					
					data.fCamHeightOffset = COSINE_INTERP_FLOAT(data.fCamHeightOffset, data.fDesiredCamHeightOffset, 0.5)
					
					vPosDiff = vPosDiff + <<0.0, 0.0, data.fCamHeightOffset>>

					SET_CAM_COORD(data.cameraIndex, data.vCamPointPos + vPosDiff)
					
				ENDIF

			BREAK
		
		ENDSWITCH
		
	ENDIF

ENDPROC

FUNC BOOL IS_PLAYER_CLOSE_ENOUGH_TO_START_CASH_GRAB_INTRO(OBJECT_INDEX tempObj, STRING AnimDict)

	VECTOR vObjectPosition 			= GET_ENTITY_COORDS(tempObj)
	VECTOR vObjectRotation 			= GET_ENTITY_ROTATION(tempObj)
	VECTOR vPlayerPosition			= GET_ENTITY_COORDS(LocalPlayerPed, FALSE)
	VECTOR vInitialOffsetRotation 	= GET_ANIM_INITIAL_OFFSET_ROTATION(AnimDict, "intro", vObjectPosition, vObjectRotation)
	VECTOR vInitialOffsetPosition 	= GET_ANIM_INITIAL_OFFSET_POSITION(AnimDict, "intro", vObjectPosition, vObjectRotation)

	FLOAT fDistFromAnimPosition		= VDIST(vPlayerPosition, vInitialOffsetPosition)
	FLOAT fHeadingDiffFromAnimStart = ABSF(GET_ENTITY_HEADING(LocalPlayerPed) - vInitialOffsetRotation.z)

	IF fHeadingDiffFromAnimStart > 180.0
		fHeadingDiffFromAnimStart -= 360.0
	ENDIF

	//DRAW_DEBUG_LINE(vPlayerPosition, vInitialOffsetPosition)
	//DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(fDistFromAnimPosition), << 0.1, 0.2, 0.0 >>)
	//DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(ABSF(fHeadingDiffFromAnimStart)), << 0.1, 0.3, 0.0 >>)

	IF (fDistFromAnimPosition < 0.315)
		IF ABSF(fHeadingDiffFromAnimStart) < 120.0
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_CASH_GRAB_FAIL_CLEANUP(INT iObj, BOOL bForceCleanup = FALSE)
	IF MC_playerBD[iLocalPart].iObjHacking != -1
	AND MC_playerBD[iLocalPart].iObjHacking = iObj
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iHackFailBitset, MC_playerBD[iPartToUse].iObjHacking)
		OR bForceCleanup
							
			DELETE_BAG_OBJECT()
			DELETE_MONEY_OBJECT()
			
			IF DOES_CAM_EXIST(sCashGrabData.cameraIndex)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			ENDIF
			CLEANUP_CASH_GRAB_SCRIPTED_CAMERA(sCashGrabData)
			STOP_AUDIO_SCENE("DLC_HEIST_MINIGAME_PAC_CASH_GRAB_SCENE")
			
			IF NOT IS_PED_INJURED(LocalPlayerPed)
				CLEAR_PED_TASKS(LocalPlayerPed)
				TASK_CLEAR_LOOK_AT(LocalPlayerPed)
			ENDIF
			
			NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)

			SET_CURRENT_PED_WEAPON(LocalPlayerPed, eWeaponBeforeMinigame, TRUE)
			APPLY_DUFFEL_BAG_HEIST_GEAR(LocalPlayerPed, iLocalPart)

			REINIT_NET_TIMER(tdHackTimer)

			ENABLE_INTERACTION_MENU()
			RESET_CASH_GRAB_MINIGAME_DATA(sCashGrabData)
			FORCE_QUIT_FAIL_CASH_GRAB_MINIGAME_KEEP_DATA(sCashGrabData)
			
			CLEAR_BIT(MC_playerBD[iLocalPart].iHackFailBitset, MC_playerBD[iLocalPart].iObjHacking)
			
			RESET_OBJ_HACKING_INT()
			
			IF NOT bForceCleanup
				PRINTLN("[CashGrab] PROCESS_CASH_GRAB_FAIL_CLEANUP - Local player has failed cash grab minigame due to being smacked, part: ", iLocalPart, ". Running fail cleanup 3.")
			ELSE
				PRINTLN("[CashGrab] PROCESS_CASH_GRAB_FAIL_CLEANUP - Forced cleanup, likely a player death.")
			ENDIF

			iHackProgress 	= 0
			iHackLimitTimer = 0
			
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_CASH_GRAB_RUN_ON_THIS_OBJECT()
	
	IF sIWInfo.eInteractWith_CurrentState != IW_STATE_IDLE
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_CASH_GRAB(OBJECT_INDEX tempObj, INT iobj)
	
	IF NOT SHOULD_CASH_GRAB_RUN_ON_THIS_OBJECT()
		EXIT
	ENDIF
	
	IF MC_playerBD[iPartToUse].iObjHacked = -1
		IF MC_serverBD.iObjHackPart[iobj] = -1
			IF MC_playerBD[iPartToUse].iObjHacking = -1
					
				//run synched scene on the prop right from the start?
				
				//broadcast event to all players that cash grab has started
				//this in  turn should allow all players to draw the cash grab hud
				IF g_TransitionSessionNonResetVars.bDisplayCashGrabTake = FALSE
				AND NOT IS_COKE_TROLLEY(GET_ENTITY_MODEL(tempObj))
					BROADCAST_SCRIPT_EVENT_GRAB_CASH_DISPLAY_TAKE()
					g_TransitionSessionNonResetVars.bDisplayCashGrabTake = TRUE
				ENDIF
				
				IF DOES_ENTITY_EXIST(tempObj)
					IF IS_COKE_TROLLEY(GET_ENTITY_MODEL(tempObj))
						bShouldProcessCokeGrabHud = TRUE
					ENDIF
				ENDIF
				
				//request minigame assets as early as possible
				IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
					SET_BIT(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS)
				ELSE
					CLEAR_BIT(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS)
				ENDIF
		
				IF bPlayerToUseOK
					IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
						
						IF 	NOT IS_BROWSER_OPEN()
						AND NOT IS_PHONE_ONSCREEN()
						AND NOT IS_CUTSCENE_PLAYING()
						AND IS_PLAYER_CONTROL_ON(PLAYER_ID())

							VECTOR vCashGrabAngledArea1 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj, vCashGrabAngledAreaOffset1)
							VECTOR vCashGrabAngledArea2 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj, vCashGrabAngledAreaOffset2)
							
							BOOL bShouldBlockCokeGrab = FALSE
							
							IF DOES_ENTITY_EXIST(tempObj)
								IF IS_COKE_TROLLEY(GET_ENTITY_MODEL(tempObj))
									INT iSearchFlags = VEHICLE_SEARCH_FLAG_RETURN_LAW_ENFORCER_VEHICLES + VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES + VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES + VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_GROUP_MEMBERS + VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_PLAYER + VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_DEAD_OR_DYING_PED + VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_SCRIPTED_TASK + VEHICLE_SEARCH_FLAG_ALLOW_LAW_ENFORCER_VEHICLES_WITH_WANTED_LEVEL
									VECTOR vCentre = vCashGrabAngledArea1 + ((vCashGrabAngledArea2 - vCashGrabAngledArea1) / 2.0)
									VEHICLE_INDEX viClosestVeh = GET_CLOSEST_VEHICLE(vCentre, 1.5, DUMMY_MODEL_FOR_SCRIPT, iSearchFlags)
									
									IF viClosestVeh != NULL
										IF DOES_VEHICLE_OVERLAP_ANGLED_AREA(viClosestVeh, vCashGrabAngledArea1, vCashGrabAngledArea2, fCashGrabAngledAreaWidth)
											bShouldBlockCokeGrab = TRUE
											PRINTLN("[RCC MISSION][CashGrab] - Blocking Coke Grab as there is a vehicle in the way")
										ENDIF
									ELSE
										PRINTLN("[RCC MISSION][CashGrab] - No vehicle found from search")
									ENDIF
								ENDIF
								
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_AllowMinigameOffRule)
								AND IS_BIT_SET(MC_serverBD_1.iOffRuleMinigameCompleted, iObj)
									PRINTLN("[RCC MISSION][CashGrab] - Off rule minigame ", iObj," has already been completed.")
									bShouldBlockCokeGrab = TRUE
								ENDIF
								
								IF sIWInfo.iInteractWith_HelptextGiverIndex != -1
								OR sIWInfo.iInteractWith_LastFrameHelptextGiverIndex != -1
									PRINTLN("[RCC MISSION][CashGrab] - An interact with is taking priority.")
									bShouldBlockCokeGrab = TRUE
								ENDIF
								
								IF SHOULD_OBJECT_MINIGAME_BE_DISABLED_BY_POISON_GAS(iObj)
									PRINTLN("[RCC MISSION][CashGrab] - SHOULD_OBJECT_MINIGAME_BE_DISABLED_BY_POISON_GAS - blocking usage.")
									bShouldBlockCokeGrab = TRUE
								ENDIF
							ENDIF
							
							IF IS_ENTITY_IN_ANGLED_AREA(LocalPlayerPed, vCashGrabAngledArea1, vCashGrabAngledArea2, fCashGrabAngledAreaWidth, DEFAULT, DEFAULT, TM_ON_FOOT)
							AND !bShouldBlockCokeGrab
								
								SET_OBJ_NEAR(iObj)
								
								REQUEST_HEIST_BAG_MINIGAME_ASSETS(tempObj, IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS), MC_playerBD_1[iLocalPart].mnHeistGearBag)
								
								IF DOES_ENTITY_EXIST(tempObj)
									IF IS_THIS_ROCKSTAR_MISSION_SVM_PHANTOM2(g_FMMC_STRUCT.iRootContentIDHash)
									AND g_TransitionSessionNonResetVars.iLocalPlayerCashGrabbed >= iMaxCashLocalPlayerCanCarry
										IF NOT IS_BIT_SET(iLocalBoolCheck20, LBOOL20_SHOWN_BAG_FULL_HELP)
											SET_BIT(iLocalBoolCheck20, LBOOL20_SHOWN_BAG_FULL_HELP)
											PRINT_HELP("MC_GRAB_FULL", DEFAULT_HELP_TEXT_TIME)
										ENDIF
									ELSE
										IF IS_COKE_TROLLEY(GET_ENTITY_MODEL(tempObj))
											DISPLAY_HELP_TEXT_THIS_FRAME_FOR_MINIGAME("MC_GRAB_6",TRUE)
										ELIF IS_DIAMOND_TROLLEY(GET_ENTITY_MODEL(tempObj))
											DISPLAY_HELP_TEXT_THIS_FRAME_FOR_MINIGAME("MC_GRAB_7",TRUE)
										ELIF IS_GOLD_TROLLEY(GET_ENTITY_MODEL(tempObj))
											DISPLAY_HELP_TEXT_THIS_FRAME_FOR_MINIGAME("START_GRAB_2",TRUE)
										ELSE
											DISPLAY_HELP_TEXT_THIS_FRAME_FOR_MINIGAME("MC_GRAB_1",TRUE)
										ENDIF
									ENDIF
								ENDIF
								
								//call only for debug reasons here
								//IS_PLAYER_CLOSE_ENOUGH_TO_START_CASH_GRAB_INTRO(tempObj, GET_HEIST_BAG_MINIGAME_ANIM_DICT(tempObj, IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS)))
								BOOL bAllowCashGrabStart = FALSE
								IF IS_THIS_ROCKSTAR_MISSION_SVM_PHANTOM2(g_FMMC_STRUCT.iRootContentIDHash)
									IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
									AND g_TransitionSessionNonResetVars.iLocalPlayerCashGrabbed < iMaxCashLocalPlayerCanCarry)
										bAllowCashGrabStart = TRUE
									ENDIF
								ELSE
									IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
										bAllowCashGrabStart = TRUE
									ENDIF
								ENDIF

								IF bAllowCashGrabStart
									
									NETWORK_REQUEST_CONTROL_OF_ENTITY(tempObj)
									
									IF NOT HAS_MINIGAME_TIMER_BEEN_INITIALISED(1)
										PRINTLN("[RCC MISSION][TELEMETRY][CashGrab] - INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY - SLOT 1")
										PRINTLN("[RCC MISSION][TELEMETRY][CashGrab] - START MINIGAME TIMER - SLOT 1")							
										INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY(1)
										START_MINIGAME_TIMER_FOR_TELEMETRY(1)
									ENDIF
								
									DISABLE_CELLPHONE_THIS_FRAME_ONLY()
									
									START_AUDIO_SCENE("DLC_HEIST_MINIGAME_PAC_CASH_GRAB_SCENE")
									
									MC_playerBD[iPartToUse].iObjHacking = iobj
									
									CLEAR_BIT(MC_playerBD[iPartToUse].iHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)

									//get mission difficulty
									SWITCH g_FMMC_STRUCT.iDifficulity
										CASE DIFF_EASY 		sCashGrabData.fMissionDifficultyModifier = g_sMPTunables.fheist_difficulty_easy 	BREAK
										CASE DIFF_NORMAL 	sCashGrabData.fMissionDifficultyModifier = g_sMPTunables.fheist_difficulty_normal 	BREAK
										CASE DIFF_HARD 		sCashGrabData.fMissionDifficultyModifier = g_sMPTunables.fheist_difficulty_hard 	BREAK
										DEFAULT				sCashGrabData.fMissionDifficultyModifier = g_sMPTunables.fheist_difficulty_normal	BREAK
									ENDSWITCH

									//get minigame difficulty
									IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet, cibsLEGACY_FMMC_MINI_GAME_DIFF_EASY)			
										sCashGrabData.eDifficulty = GD_EASY
									ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet, cibsLEGACY_FMMC_MINI_GAME_DIFF_HARD)
										sCashGrabData.eDifficulty = GD_HARD
									ELSE
										sCashGrabData.eDifficulty = GD_MEDIUM
									ENDIF
									
									//Get how much cash we've already grabbed: this determines where we start the grab anims.
									INT iTotalCashGrabbed, iBaseCashGrabbed, iBonusCashGrabbed, iBaseCashPiles
																														
									IF IS_THIS_ROCKSTAR_MISSION_SVM_PHANTOM2(g_FMMC_STRUCT.iRootContentIDHash)
										sCashGrabData.iBonusCashPiles = 0
										sCashGrabData.iBaseCashPileValue = 10000
										sCashGrabData.iBonusCashPileValue = 10000
										sCashGrabData.fMissionDifficultyModifier = 1.0
									ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_UseDailyCashGrabAmount)
										INT iCashPerStack = MC_ServerBD_4.iDailyCashVaultStackValue
										sCashGrabData.iBonusCashPiles		= 0
										sCashGrabData.iBaseCashPileValue 	= iCashPerStack
										sCashGrabData.iBonusCashPileValue	= iCashPerStack
										sCashGrabData.fMissionDifficultyModifier = 1.0
									ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCashAmount != 0
										sCashGrabData.iBonusCashPiles		= 0
										sCashGrabData.iBaseCashPileValue 	= g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCashAmount
										sCashGrabData.iBonusCashPileValue	= g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCashAmount
										sCashGrabData.fMissionDifficultyModifier = 1.0
									ELSE
										sCashGrabData.iBonusCashPiles		= g_sMPTunables.Bonus_Stack_Number_Per_Trolley
										sCashGrabData.iBaseCashPileValue 	= FLOOR( TO_FLOAT(g_sMPTunables.Default_Stack_Cash_Value) * sCashGrabData.fMissionDifficultyModifier * 
																						GET_PACIFIC_STANDARD_TUNABLE_CASH_MULTIPLIER() )
										sCashGrabData.iBonusCashPileValue	= FLOOR( TO_FLOAT(g_sMPTunables.Bonus_Stack_Cash_Value) * sCashGrabData.fMissionDifficultyModifier *
																						GET_PACIFIC_STANDARD_TUNABLE_CASH_MULTIPLIER() )
									ENDIF

									iTotalCashGrabbed					= MC_ServerBD.iCashGrabTotal[iObj]
									iBonusCashGrabbed					= MC_ServerBD.iCashGrabBonusPilesGrabbed[iObj] * sCashGrabData.iBonusCashPileValue
									iBaseCashGrabbed					= iTotalCashGrabbed - iBonusCashGrabbed
									iBaseCashPiles						= iBaseCashGrabbed / sCashGrabData.iBaseCashPileValue
									
									sCashGrabData.iBonusCashPilesGrabbed= MC_ServerBD.iCashGrabBonusPilesGrabbed[iObj]
									sCashGrabData.iTotalCashPiles 		= MC_ServerBD.iCashGrabBonusPilesGrabbed[iObj] + iBaseCashPiles
									
									IF IS_GOLD_TROLLEY(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn)
										sCashGrabData.iTotalCashPiles += 23
									ELIF IS_LOW_CASH_TROLLEY(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn)
									AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_UseDailyCashGrabAmount)
									AND MC_serverBD_1.sLEGACYMissionContinuityVars.iDailyVaultCashGrabbed != 0
										sCashGrabData.iTotalCashPiles = MC_serverBD_1.sLEGACYMissionContinuityVars.iDailyVaultCashGrabbed
									ENDIF
									
									#IF IS_DEBUG_BUILD
									IF bSingleItemCashGrab
										sCashGrabData.iTotalCashPiles = 44
									ENDIF
									#ENDIF

									
									PRINTLN("[CashGrab] PROCESS_CASH_GRAB:
											  sCashGrabData.fMissionDifficultyModifier = ", sCashGrabData.fMissionDifficultyModifier,
											" sCashGrabData.iBaseCashPileValue = ", sCashGrabData.iBaseCashPileValue,
											" sCashGrabData.iBonusCashPileValue = ", sCashGrabData.iBonusCashPileValue,
											" sCashGrabData.iTotalCashPiles = ", sCashGrabData.iTotalCashPiles,
											" sCashGrabData.iBonusCashPiles = ", sCashGrabData.iBonusCashPiles,
											" sCashGrabData.iBonusCashPilesGrabbed = ", sCashGrabData.iBonusCashPilesGrabbed,
											" MC_ServerBD.iCashGrabBonusPilesGrabbed[iObj] = ", MC_ServerBD.iCashGrabBonusPilesGrabbed[iObj],
											" MC_ServerBD.iCashGrabTotal[iObj] = ", MC_ServerBD.iCashGrabTotal[iObj])
									PRINTLN("[CashGrab] PROCESS_CASH_GRAB - Model: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(tempObj)))
									//Reset any other cash grab variables.
									CLEAR_BIT(sCashGrabData.iBitSet, CASH_GRAB_BS_BAG_REMOVED)
									CLEAR_BIT(sCashGrabData.iBitSet, CASH_GRAB_BS_CASH_BAGGED)
									CLEAR_BIT(sCashGrabData.iBitSet, CASH_GRAB_BS_ANIM_EVENTS_STARTED)
									CLEAR_BIT(sCashGrabData.iBitSet, CASH_GRAB_BS_SKIP_WALK_AND_DO_INTRO)
									CLEAR_BIT(sCashGrabData.iBitSet, CASH_GRAB_BS_BAG_PROP_LOCALLY_VISIBLE)
									
									IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
										SET_BIT(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS)
									ELSE
										CLEAR_BIT(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS)
									ENDIF
									
									REQUEST_HEIST_BAG_MINIGAME_ASSETS(tempObj, IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS), MC_playerBD_1[iLocalPart].mnHeistGearBag)
									
									//fix for B*2174653 - task the ped to walk to the anim start point on the button press
									//*actually commenting this early walk task and blocking control as it caues issues with players at the same time, B*2220815
									IF HAVE_HEIST_BAG_MINIGAME_ASSETS_LOADED(tempObj, IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS), MC_playerBD_1[iLocalPart].mnHeistGearBag)
										
										IF NOT IS_PED_INJURED(LocalPlayerPed)
											CLEAR_PED_TASKS(LocalPlayerPed)	
										ENDIF
										//NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_ALLOW_PLAYER_DAMAGE|NSPC_PREVENT_EVERYBODY_BACKOFF|NSPC_LEAVE_CAMERA_CONTROL_ON|NSPC_ALLOW_PAD_SHAKE|NSPC_REENABLE_CONTROL_ON_DEATH)
										
										IF GET_CURRENT_PED_WEAPON(LocalPlayerPed, eWeaponBeforeMinigame, FALSE)
											SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED, TRUE)
										ENDIF
										
										VECTOR 	vStartPosition, vStartRotation
											
										//get position and rotation of where the player should navigate to start the animations
										vStartPosition 	= GET_ANIM_INITIAL_OFFSET_POSITION(GET_HEIST_BAG_MINIGAME_ANIM_DICT(tempObj, IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS)), "intro", GET_ENTITY_COORDS(tempObj), GET_ENTITY_ROTATION(tempObj))
										vStartRotation 	= GET_ANIM_INITIAL_OFFSET_ROTATION(GET_HEIST_BAG_MINIGAME_ANIM_DICT(tempObj, IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS)), "intro", GET_ENTITY_COORDS(tempObj), GET_ENTITY_ROTATION(tempObj))
									
										IF NOT IS_PLAYER_CLOSE_ENOUGH_TO_START_CASH_GRAB_INTRO(tempObj, GET_HEIST_BAG_MINIGAME_ANIM_DICT(tempObj, IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS)))

											SEQUENCE_INDEX SequenceIndex				
											CLEAR_SEQUENCE_TASK(SequenceIndex)
											OPEN_SEQUENCE_TASK(SequenceIndex)									
												TASK_GO_STRAIGHT_TO_COORD(NULL, vStartPosition, PEDMOVE_WALK,
																		  DEFAULT_TIME_BEFORE_WARP, vStartRotation.Z, 0.05)
											CLOSE_SEQUENCE_TASK(SequenceIndex)
											TASK_PERFORM_SEQUENCE(LocalPlayerPed, SequenceIndex)
											CLEAR_SEQUENCE_TASK(SequenceIndex)
											
											TASK_LOOK_AT_ENTITY(LocalPlayerPed, tempObj, DEFAULT_TIME_BEFORE_WARP, SLF_WHILE_NOT_IN_FOV)
											
											SET_BIT(sCashGrabData.iBitSet, CASH_GRAB_BS_WALK_TASK_GIVEN_EARLY)
										ELSE
										
											SEQUENCE_INDEX SequenceIndex				
											CLEAR_SEQUENCE_TASK(SequenceIndex)
											OPEN_SEQUENCE_TASK(SequenceIndex)									
												//TASK_PED_SLIDE_TO_COORD(NULL, vStartPosition, vStartRotation.Z)
												TASK_ACHIEVE_HEADING(NULL, vStartRotation.Z)
											CLOSE_SEQUENCE_TASK(SequenceIndex)
											TASK_PERFORM_SEQUENCE(LocalPlayerPed, SequenceIndex)
											CLEAR_SEQUENCE_TASK(SequenceIndex)
										
											SET_BIT(sCashGrabData.iBitSet, CASH_GRAB_BS_SKIP_WALK_AND_DO_INTRO)
										ENDIF
									ENDIF
									
									IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetFour, cibsOBJ4_DisableMinigameCamera)
									OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetFour, cibsOBJ4_UseAlternateCashGrabCamera)
										IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
											SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT, CAM_VIEW_MODE_THIRD_PERSON_NEAR)
										ENDIF
										DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_NEXT_CAMERA)
									ENDIF
									//*/
									//end fix for B*2174653		 
									
									iHackProgress	= 0
									iHackLimitTimer = 0
								ENDIF
							ELSE
								IF IS_BIT_SET(iLocalBoolCheck20, LBOOL20_SHOWN_BAG_FULL_HELP)
									CLEAR_BIT(iLocalBoolCheck20, LBOOL20_SHOWN_BAG_FULL_HELP)
								ENDIF
								RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_STASH_SWAG")
							ENDIF
						ELSE
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MC_GRAB_1")
							OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MC_GRAB_2")
							OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MC_GRAB_6")
							OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MC_GRAB_7")
								CLEAR_HELP()
							ENDIF
							IF IS_BIT_SET(iLocalBoolCheck20, LBOOL20_SHOWN_BAG_FULL_HELP)
								CLEAR_BIT(iLocalBoolCheck20, LBOOL20_SHOWN_BAG_FULL_HELP)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
			
				IF IS_BIT_SET(MC_playerBD[iPartToUse].iHackFailBitset, MC_playerBD[iPartToUse].iObjHacking)
				AND MC_playerBD[iPartToUse].iObjHacking = iObj
					
					SET_OBJ_NEAR(iObj)
					
					IF NOT g_bMissionEnding
					
						DELETE_BAG_OBJECT()
						DELETE_MONEY_OBJECT()
						
						IF DOES_CAM_EXIST(sCashGrabData.cameraIndex)
							SET_GAMEPLAY_CAM_RELATIVE_PITCH()
							SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						ENDIF
						CLEANUP_CASH_GRAB_SCRIPTED_CAMERA(sCashGrabData)
						STOP_AUDIO_SCENE("DLC_HEIST_MINIGAME_PAC_CASH_GRAB_SCENE")
						
						IF bPlayerToUseOK
							CLEAR_PED_TASKS(LocalPlayerPed)
							TASK_CLEAR_LOOK_AT(LocalPlayerPed)
						ENDIF
						
						NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
						SET_CURRENT_PED_WEAPON(LocalPlayerPed, eWeaponBeforeMinigame, TRUE)
						APPLY_DUFFEL_BAG_HEIST_GEAR(LocalPlayerPed, iLocalPart)
			
						REINIT_NET_TIMER(tdHackTimer)

						ENABLE_INTERACTION_MENU()
						RESET_CASH_GRAB_MINIGAME_DATA(sCashGrabData)
						FORCE_QUIT_FAIL_CASH_GRAB_MINIGAME_KEEP_DATA(sCashGrabData)
						
						CLEAR_BIT(MC_playerBD[iPartToUse].iHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
						
						RESET_OBJ_HACKING_INT()
						
						PRINTLN("[RUN_CASH_GRAB_MINIGAME] Local player has failed cash grab minigame, part: ", iPartToUse, ". Running fail cleanup 1.")
					ENDIF

					iHackProgress 	= 0
					iHackLimitTimer = 0
					
				ENDIF
				
			ENDIF

		ELSE
		
			//fix leg ik issues when walking to trolley and hitting the bottom of it
			SWITCH iHackProgress
				CASE 0
					SET_PED_CAN_LEG_IK(LocalPlayerPed, FALSE)
				BREAK
			ENDSWITCH
			
			IF MC_serverBD.iObjHackPart[iobj] != iPartToUse		//check if the local player is not the hacking participant of this object
			
				IF MC_playerBD[iPartToUse].iObjHacking = iobj	//check if the local player is trying to hack this object
					IF NOT IS_PED_INJURED(LocalPlayerPed)
						CLEAR_PED_TASKS(LocalPlayerPed)
					ENDIF
					RESET_OBJ_HACKING_INT()	//reset the object number local player tried to hack
					PRINTLN("[RUN_CASH_GRAB_MINIGAME] Setting MC_playerBD[",iPartToUse,"].iObjHacking for object = ",iobj, " to ", MC_playerBD[iPartToUse].iObjHacking, " as another participant is hacking this object.")
				ENDIF
				
			ELSE												
			
				IF MC_playerBD[iPartToUse].iObjHacking != -1 //check if the local player is the hacking participant of this object
				
					//check for cash grab minigame being interrupted, fix for B*2118175
					IF (NOT bPlayerToUseOK) OR g_bMissionEnding
						IF IS_BIT_SET(MC_playerBD[iPartToUse].iHackFailBitset, MC_playerBD[iPartToUse].iObjHacking)
						
							DELETE_BAG_OBJECT()
							DELETE_MONEY_OBJECT()
							
							IF DOES_CAM_EXIST(sCashGrabData.cameraIndex)
								SET_GAMEPLAY_CAM_RELATIVE_PITCH()
								SET_GAMEPLAY_CAM_RELATIVE_HEADING()
							ENDIF
							CLEANUP_CASH_GRAB_SCRIPTED_CAMERA(sCashGrabData)
							STOP_AUDIO_SCENE("DLC_HEIST_MINIGAME_PAC_CASH_GRAB_SCENE")
							
							IF bPlayerToUseOK
								CLEAR_PED_TASKS(LocalPlayerPed)
								TASK_CLEAR_LOOK_AT(LocalPlayerPed)
							ENDIF
							
							NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)

							SET_CURRENT_PED_WEAPON(LocalPlayerPed, eWeaponBeforeMinigame, TRUE)
							APPLY_DUFFEL_BAG_HEIST_GEAR(LocalPlayerPed, iLocalPart)
				
							REINIT_NET_TIMER(tdHackTimer)

							ENABLE_INTERACTION_MENU()
							RESET_CASH_GRAB_MINIGAME_DATA(sCashGrabData)
							FORCE_QUIT_FAIL_CASH_GRAB_MINIGAME_KEEP_DATA(sCashGrabData)
							
							CLEAR_BIT(MC_playerBD[iPartToUse].iHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
							
							RESET_OBJ_HACKING_INT()
							
							PRINTLN("[RUN_CASH_GRAB_MINIGAME] Local player has failed cash grab minigame due to death, part: ", iPartToUse, ". Running fail cleanup 2.")

							iHackProgress 	= 0
							iHackLimitTimer = 0
							
						ENDIF
					ENDIF
				
					IF MC_serverBD.iObjHackPart[iobj] = iPartToUse
						SET_OBJ_NEAR(iObj)
						
						IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetFour, cibsOBJ4_DisableMinigameCamera)
						AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetFour, cibsOBJ4_UseAlternateCashGrabCamera)
							UPDATE_CASH_GRAB_SCRIPTED_CAMERA(sCashGrabData, tempObj)
							
							DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_LR)
							DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_UD)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
						ELSE
							IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
								SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT, CAM_VIEW_MODE_THIRD_PERSON_NEAR)
							ENDIF
							DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_NEXT_CAMERA)
						ENDIF
						
						IF SHOULD_OBJECT_MINIGAME_BE_DISABLED_BY_POISON_GAS(iObj)
							//Disable input
							IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
								DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
							ELSE
								DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
							ENDIF
							IF sCashGrabData.eState = HBGS_IDLE_ANIM
								sCashGrabData.eState = HBGS_EXIT_ANIM
							ENDIF
						ENDIF
										
						SWITCH iHackProgress
						
							CASE 0	//request animation and sound assets

								SET_PED_CAN_LEG_IK(LocalPlayerPed, FALSE)
								REQUEST_HEIST_BAG_MINIGAME_ASSETS(tempObj, IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS), MC_playerBD_1[iLocalPart].mnHeistGearBag)
								
								IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
									NETWORK_REQUEST_CONTROL_OF_ENTITY(tempObj)
								ENDIF

								IF HAVE_HEIST_BAG_MINIGAME_ASSETS_LOADED(tempObj, IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS), MC_playerBD_1[iLocalPart].mnHeistGearBag)
								
									REMOVE_MENU_HELP_KEYS()
									DISABLE_INTERACTION_MENU()

									IF 	NOT IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_WALK_TASK_GIVEN_EARLY)
									AND NOT IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_SKIP_WALK_AND_DO_INTRO)
										IF NOT IS_PED_INJURED(LocalPlayerPed)
											CLEAR_PED_TASKS(LocalPlayerPed)
										ENDIF
										
										IF GET_CURRENT_PED_WEAPON(LocalPlayerPed, eWeaponBeforeMinigame, FALSE)
											SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED, TRUE)
										ENDIF
									ENDIF
									
									NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_ALLOW_PLAYER_DAMAGE|NSPC_PREVENT_EVERYBODY_BACKOFF|NSPC_LEAVE_CAMERA_CONTROL_ON|NSPC_ALLOW_PAD_SHAKE|NSPC_REENABLE_CONTROL_ON_DEATH)

									//get the phase at which to resume grab animation
									sCashGrabData.fGrabPhase 	= GET_ANIMATION_RESUMPTION_PHASE_FROM_INT(sCashGrabData.iTotalCashPiles)

									IF NOT IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_SKIP_WALK_AND_DO_INTRO)

										VECTOR 	vStartPosition, vStartRotation
										
										//get position and rotation of where the player should navigate to start the animations
										vStartPosition 				= GET_ANIM_INITIAL_OFFSET_POSITION(GET_HEIST_BAG_MINIGAME_ANIM_DICT(tempObj, IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS)), "intro", GET_ENTITY_COORDS(tempObj), GET_ENTITY_ROTATION(tempObj))
										vStartRotation 				= GET_ANIM_INITIAL_OFFSET_ROTATION(GET_HEIST_BAG_MINIGAME_ANIM_DICT(tempObj, IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS)), "intro", GET_ENTITY_COORDS(tempObj), GET_ENTITY_ROTATION(tempObj))

										IF NOT IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_WALK_TASK_GIVEN_EARLY)
											SEQUENCE_INDEX SequenceIndex				
											CLEAR_SEQUENCE_TASK(SequenceIndex)
											OPEN_SEQUENCE_TASK(SequenceIndex)									
												TASK_GO_STRAIGHT_TO_COORD(NULL, vStartPosition, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, vStartRotation.Z, 0.05)
											CLOSE_SEQUENCE_TASK(SequenceIndex)
											TASK_PERFORM_SEQUENCE(LocalPlayerPed, SequenceIndex)
											CLEAR_SEQUENCE_TASK(SequenceIndex)
											
											TASK_LOOK_AT_ENTITY(LocalPlayerPed, tempObj, DEFAULT_TIME_BEFORE_WARP, SLF_WHILE_NOT_IN_FOV)
										ENDIF
									ENDIF
									
									REINIT_NET_TIMER(tdHackTimer)
									
									sCashGrabData.eState = HBGS_APPROACH
									
									CLEAR_BIT(sCashGrabData.iBitSet, CASH_GRAB_BS_WALK_TASK_GIVEN_EARLY)
									
									IF NOT HAS_NET_TIMER_STARTED(stMGTimer0)
										REINIT_NET_TIMER(stMGTimer0)
									ENDIF
								
									iHackProgress++

								ENDIF

							BREAK
							
							CASE 1	//handle cash grab animations

								DISABLE_DPADDOWN_THIS_FRAME()
								DISABLE_SELECTOR_THIS_FRAME()
								DISABLE_CELLPHONE_THIS_FRAME_ONLY()
								
								IF bLocalPlayerPedOk
									SET_PED_RESET_FLAG(LocalPlayerPed, PRF_CannotBeTargetedByAI, FALSE)
								ENDIF
								
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetFour, cibsOBJ4_UseAlternateCashGrabCamera)
									IF NOT IS_ENTITY_PLAYING_ANIM(LocalPlayerPed, GET_HEIST_BAG_MINIGAME_ANIM_DICT(tempObj, IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS)), "exit")
										SET_TABLE_GAMES_CAMERA_THIS_UPDATE(TABLE_GAMES_CAMERA_TYPE_BLACK_JACK)
									ENDIF
								ENDIF
								
								//handle help
								SWITCH sCashGrabData.eState
									CASE HBGS_INTRO_ANIM
									CASE HBGS_IDLE_ANIM
									CASE HBGS_GRAB_ANIM
										IF DOES_CAM_EXIST(sCashGrabData.cameraIndex)
										OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetFour, cibsOBJ4_DisableMinigameCamera)
										OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetFour, cibsOBJ4_UseAlternateCashGrabCamera)
											IF DOES_ENTITY_EXIST(tempObj)
												IF IS_COKE_TROLLEY(GET_ENTITY_MODEL(tempObj))
													IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
														DISPLAY_HELP_TEXT_THIS_FRAME_FOR_MINIGAME("MC_GRAB_5_MK",TRUE)
													ELSE
														DISPLAY_HELP_TEXT_THIS_FRAME_FOR_MINIGAME("MC_GRAB_5",TRUE)
													ENDIF
												ELIF IS_DIAMOND_TROLLEY(GET_ENTITY_MODEL(tempObj))
													IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
														DISPLAY_HELP_TEXT_THIS_FRAME_FOR_MINIGAME("MC_GRAB_7_MK",TRUE)
													ELSE
														DISPLAY_HELP_TEXT_THIS_FRAME_FOR_MINIGAME("MC_GRAB_7_TP",TRUE)
													ENDIF
												ELIF IS_GOLD_TROLLEY(GET_ENTITY_MODEL(tempObj))
													IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
														DISPLAY_HELP_TEXT_THIS_FRAME_FOR_MINIGAME("MC_GRAB_4b",TRUE)
													ELSE
														DISPLAY_HELP_TEXT_THIS_FRAME_FOR_MINIGAME("MC_GRAB_4",TRUE)
													ENDIF
												ELSE
													IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
														DISPLAY_HELP_TEXT_THIS_FRAME_FOR_MINIGAME("MC_GRAB_3_MK",TRUE)
													ELSE
														DISPLAY_HELP_TEXT_THIS_FRAME_FOR_MINIGAME("MC_GRAB_3",TRUE)
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									BREAK
								ENDSWITCH
								
								//update animation states
								SWITCH sCashGrabData.eState
								
									CASE HBGS_APPROACH
									
										SET_PED_CAN_LEG_IK(LocalPlayerPed, FALSE)
										CREATE_BAG_OBJECTS_AT_POSITION(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj, << 0.0, 0.0, -2.0 >>), TRUE)
									
										IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
											NETWORK_REQUEST_CONTROL_OF_ENTITY(tempObj)
										ENDIF
									
										IF bPlayerToUseOK
											IF GET_SCRIPT_TASK_STATUS(LocalPlayerPed,SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
											OR HAS_NET_TIMER_EXPIRED(tdHackTimer, DEFAULT_TIME_BEFORE_WARP / 2)
											OR IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_WALK_TASK_GIVEN_EARLY)
											OR IS_PLAYER_CLOSE_ENOUGH_TO_START_CASH_GRAB_INTRO(tempObj, GET_HEIST_BAG_MINIGAME_ANIM_DICT(tempObj, IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS)))
												IF NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
												
													REINIT_NET_TIMER(tdHackTimer)
													SET_PED_PROOFS_FOR_MINIGAME(LocalPlayerPed)
												
													sCashGrabData.eState = HBGS_INTRO_ANIM
												ENDIF
											ENDIF
										ENDIF
									
									BREAK
								
									CASE HBGS_INTRO_ANIM
									
										IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID))
										OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sCashGrabData.iTrolleySceneID))
										OR NOT IS_ENTITY_PLAYING_ANIM(LocalPlayerPed, GET_HEIST_BAG_MINIGAME_ANIM_DICT(tempObj, IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS)), "intro")
											
											IF bPlayerToUseOK
												//create bag prop under the map/trolley initially
												IF CREATE_BAG_OBJECTS_AT_POSITION(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj, << 0.0, 0.0, -2.0 >>))
													
													IF IS_COKE_TROLLEY(GET_ENTITY_MODEL(tempObj))
														CREATE_COKE_OBJECT()
													ELIF IS_DIAMOND_TROLLEY(GET_ENTITY_MODEL(tempObj))
														CREATE_DIAMOND_OBJECT()
													ELIF IS_GOLD_TROLLEY(GET_ENTITY_MODEL(tempObj))
														CREATE_GOLD_OBJECT()
													ELSE
														CREATE_MONEY_OBJECT()			//create hidden cash pile prop in player's hand
													ENDIF
													
													IF NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
														REMOVE_DECALS_FROM_OBJECT(tempObj)
													ELSE
														NETWORK_REQUEST_CONTROL_OF_ENTITY(tempObj)
													ENDIF
													
													//create player synchronised scene
													MC_playerBD[iPartToUse].iSynchSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_ENTITY_COORDS(tempObj),
																													 GET_ENTITY_ROTATION(tempObj),
																													 EULER_YXZ, TRUE, FALSE)
													
													//play bag animation							  
													IF DOES_ENTITY_EXIST(NET_TO_OBJ(niMinigameObjects[0]))
													
														STRING sBagAnimDictName, sBagAnimName
														
														sBagAnimDictName = GET_HEIST_BAG_MINIGAME_ANIM_DICT(tempObj, IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS))
													
														IF IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS)
															IF SHOULD_USE_SUIT_BAG_ANIMS()
																sBagAnimName = "bag_intro_suit"
															ELSE
																IF SHOULD_USE_ARMOUR_BAG_ANIMS()
																	sBagAnimName = "bag_intro"
																ELSE
																	sBagAnimName = "bag_inro_no_armour"
																ENDIF
															ENDIF
														ELSE
															IF SHOULD_USE_SUIT_BAG_ANIMS()
																sBagAnimName = PICK_STRING(IS_PLAYER_FEMALE(), "bag_intro_suit_female",  "bag_intro_suit")
															ELSE
																IF SHOULD_USE_ARMOUR_BAG_ANIMS()
																	sBagAnimName = PICK_STRING(IS_PLAYER_FEMALE(), "bag_intro_female",  "bag_intro")
																ELSE
																	sBagAnimName = PICK_STRING(IS_PLAYER_FEMALE(), "bag_inro_no_armour_female", "bag_inro_no_armour")
																ENDIF
															ENDIF
														ENDIF
														
														PRINTLN("[RUN_CASH_GRAB_MINIGAME] Playing bag HBGS_INTRO_ANIM synched scene animation ", sBagAnimDictName, "/", sBagAnimName, ".")
														NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(niMinigameObjects[0]), MC_playerBD[iPartToUse].iSynchSceneID,
																								 sBagAnimDictName, sBagAnimName, INSTANT_BLEND_IN, INSTANT_BLEND_OUT)

														FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(NET_TO_OBJ(niMinigameObjects[0]))
													ENDIF
													
													//task player
													TASK_CLEAR_LOOK_AT(LocalPlayerPed)
													NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(LocalPlayerPed, MC_playerBD[iPartToUse].iSynchSceneID,
																						  GET_HEIST_BAG_MINIGAME_ANIM_DICT(tempObj, IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS)), "intro",
																						  WALK_BLEND_IN, NORMAL_BLEND_OUT,
																						  SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS
																						  | SYNCED_SCENE_ON_ABORT_STOP_SCENE, RBF_PLAYER_IMPACT, WALK_BLEND_IN)
													FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed, DEFAULT, TRUE)
																				
													//start synchronised scene
													NETWORK_START_SYNCHRONISED_SCENE(MC_playerBD[iPartToUse].iSynchSceneID)
													
													
													//handle trolley

													//create trolley synchronised scene
													PRINTLN("[RUN_CASH_GRAB_MINIGAME]  starting idle animation at phase: ", sCashGrabData.fGrabPhase)
													sCashGrabData.iTrolleySceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_ENTITY_COORDS(tempObj),
																													  GET_ENTITY_ROTATION(tempObj),
																													  EULER_YXZ, TRUE, FALSE, DEFAULT,
																													  sCashGrabData.fGrabPhase, 0.0)	
																	
													//play trolley animation
													IF DOES_ENTITY_EXIST(tempObj)
														NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(tempObj, sCashGrabData.iTrolleySceneID,
																								 GET_HEIST_BAG_MINIGAME_ANIM_DICT(tempObj, IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS)) ,"cart_cash_dissapear",
																								 INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
														FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(tempObj)
													ENDIF
												
													//start trolley synchronised scene
													NETWORK_START_SYNCHRONISED_SCENE(sCashGrabData.iTrolleySceneID)
													
												ENDIF
											ENDIF
						
										ELSE	//when intro scene is running
										
											IF IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID))
										
												//swap the variation bag with bag object once the bag animation is playing
												IF DOES_ENTITY_EXIST(NET_TO_OBJ(niMinigameObjects[0]))
													//IF IS_ENTITY_PLAYING_ANIM(NET_TO_OBJ(niMinigameObjects[0]), GET_HEIST_BAG_MINIGAME_ANIM_DICT(tempObj, sCashGrabData.bUseHighHeelsAnims), "bag_intro")
													//OR IS_ENTITY_PLAYING_ANIM(NET_TO_OBJ(niMinigameObjects[0]), GET_HEIST_BAG_MINIGAME_ANIM_DICT(tempObj, sCashGrabData.bUseHighHeelsAnims), "bag_intro_no_armour")
														IF GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID)) > 0.340
															//remove player bag
															IF NOT IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_BAG_REMOVED)
																REMOVE_PLAYER_BAG()
																SET_BIT(sCashGrabData.iBitSet, CASH_GRAB_BS_BAG_REMOVED)															
															ELSE
																//make the prop bag appear
																IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(LocalPlayerPed)  
																	IF NOT IS_ENTITY_VISIBLE_TO_SCRIPT(NET_TO_OBJ(niMinigameObjects[0]))
																		//make the bag prop visible once the anim is playing and streaming requests are done
																		SET_BIT(sCashGrabData.iBitSet, CASH_GRAB_BS_BAG_PROP_LOCALLY_VISIBLE)
																		SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[0]), TRUE)
																	ENDIF
																ENDIF
															ENDIF
														ENDIF
													//ENDIF
												ENDIF

												//move into idle state at the end of intro synched scene
												IF GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID)) >= 0.99
												
													sCashGrabData.eState = HBGS_IDLE_ANIM
											
												ENDIF
												
											ENDIF
										
										ENDIF
									
									BREAK
									
									CASE HBGS_IDLE_ANIM

										IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID))
										OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sCashGrabData.iTrolleySceneID))
										OR NOT IS_ENTITY_PLAYING_ANIM(LocalPlayerPed, GET_HEIST_BAG_MINIGAME_ANIM_DICT(tempObj, IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS)), "grab_idle")

											//handle trolley

											//create trolley synchronised scene
											PRINTLN("[RUN_CASH_GRAB_MINIGAME]  starting idle animation at phase: ", sCashGrabData.fGrabPhase)
											sCashGrabData.iTrolleySceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_ENTITY_COORDS(tempObj),
																											  GET_ENTITY_ROTATION(tempObj),
																											  EULER_YXZ, TRUE, FALSE, DEFAULT,
																											  sCashGrabData.fGrabPhase, 0.0)
											PRINTLN("[RUN_CASH_GRAB_MINIGAME] HBGS_IDLE_ANIM Starting scene sCashGrabData.iTrolleySceneID with id ", sCashGrabData.iTrolleySceneID, " and rate 0.0")
											PRINTLN("[RUN_CASH_GRAB_MINIGAME] HBGS_IDLE_ANIM Local scene id from network id for sCashGrabData.iTrolleySceneID is ", NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sCashGrabData.iTrolleySceneID))
															
											//play trolley animation
											IF DOES_ENTITY_EXIST(tempObj)
												NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(tempObj, sCashGrabData.iTrolleySceneID,
																						 GET_HEIST_BAG_MINIGAME_ANIM_DICT(tempObj, IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS)) ,"cart_cash_dissapear",
																						 INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
												FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(tempObj)
											ENDIF
										
											//start trolley synchronised scene
											NETWORK_START_SYNCHRONISED_SCENE(sCashGrabData.iTrolleySceneID)

											//handle player and bag
											
											//create player synchronised scene
											MC_playerBD[iPartToUse].iSynchSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_ENTITY_COORDS(tempObj), GET_ENTITY_ROTATION(tempObj), EULER_YXZ, FALSE, TRUE)
											
											//play bag animation							  
											IF DOES_ENTITY_EXIST(NET_TO_OBJ(niMinigameObjects[0]))
											
												STRING sBagAnimDictName, sBagAnimName
															
												sBagAnimDictName = GET_HEIST_BAG_MINIGAME_ANIM_DICT(tempObj, IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS))
											
												IF IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS)
													IF SHOULD_USE_SUIT_BAG_ANIMS()
														sBagAnimName = "bag_grab_idle_suit"
													ELSE
														IF SHOULD_USE_ARMOUR_BAG_ANIMS()
															sBagAnimName = "bag_grab_idle"
														ELSE
															sBagAnimName = "bag_grab_idle_no_armour"
														ENDIF
													ENDIF
												ELSE
													IF SHOULD_USE_SUIT_BAG_ANIMS()
														sBagAnimName = PICK_STRING(IS_PLAYER_FEMALE(), "bag_grab_idle_suit_female",  "bag_grab_idle_suit")
													ELSE
														IF SHOULD_USE_ARMOUR_BAG_ANIMS()
															sBagAnimName = PICK_STRING(IS_PLAYER_FEMALE(), "bag_grab_idle_female",  "bag_grab_idle")
														ELSE
															sBagAnimName = PICK_STRING(IS_PLAYER_FEMALE(), "bag_grab_idle_no_armour_female", "bag_grab_idle_no_armour")
														ENDIF
													ENDIF
												ENDIF
												
												PRINTLN("[RUN_CASH_GRAB_MINIGAME] Playing bag HBGS_IDLE_ANIM synched scene animation ", sBagAnimDictName, "/", sBagAnimName, ".")
												NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(niMinigameObjects[0]), MC_playerBD[iPartToUse].iSynchSceneID,
																						 sBagAnimDictName, sBagAnimName, INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
																							 
												FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(NET_TO_OBJ(niMinigameObjects[0]))
											ENDIF
											
											//task player 
											NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(LocalPlayerPed, MC_playerBD[iPartToUse].iSynchSceneID,
																				  GET_HEIST_BAG_MINIGAME_ANIM_DICT(tempObj, IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS)), "grab_idle",
																				  REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT,
																				  SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS
																				  | SYNCED_SCENE_ON_ABORT_STOP_SCENE, RBF_PLAYER_IMPACT)
											FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed, DEFAULT, TRUE)
																		
											//start player synchronised scene
											NETWORK_START_SYNCHRONISED_SCENE(MC_playerBD[iPartToUse].iSynchSceneID)

										ELSE	//when idle scene is running
										
											//print trolley scene information
											IF IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sCashGrabData.iTrolleySceneID))
												PRINTLN("[RUN_CASH_GRAB_MINIGAME] HBGS_IDLE_ANIM: Trolley scene information 
														  sCashGrabData.iTrolleySceneID = ", sCashGrabData.iTrolleySceneID,
														" local id from sCashGrabData.iTrolleySceneID = ", NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sCashGrabData.iTrolleySceneID),
														" scene phase = ", GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sCashGrabData.iTrolleySceneID)),
														" scene rate = ", GET_SYNCHRONIZED_SCENE_RATE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sCashGrabData.iTrolleySceneID)))
											ENDIF
										
											//check for player input
											IF IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID))
												IF NOT IS_PAUSE_MENU_ACTIVE()
													IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
														IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
														OR (IS_THIS_ROCKSTAR_MISSION_SVM_PHANTOM2(g_FMMC_STRUCT.iRootContentIDHash) AND g_TransitionSessionNonResetVars.iLocalPlayerCashGrabbed >= iMaxCashLocalPlayerCanCarry)
														
															sCashGrabData.eState = HBGS_EXIT_ANIM
													
														ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
														
															sCashGrabData.eState = HBGS_GRAB_ANIM
														
														ENDIF
													ELSE
														IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
														OR (IS_THIS_ROCKSTAR_MISSION_SVM_PHANTOM2(g_FMMC_STRUCT.iRootContentIDHash) AND g_TransitionSessionNonResetVars.iLocalPlayerCashGrabbed >= iMaxCashLocalPlayerCanCarry)
														
															sCashGrabData.eState = HBGS_EXIT_ANIM
													
														ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
														
															sCashGrabData.eState = HBGS_GRAB_ANIM
														
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										
										ENDIF
										
									BREAK
																
									CASE HBGS_GRAB_ANIM
										
										IF NOT IS_ENTITY_PLAYING_ANIM(LocalPlayerPed, GET_HEIST_BAG_MINIGAME_ANIM_DICT(tempObj, IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS)), "grab")
											
											//create trolley synchronised scene
											sCashGrabData.iTrolleySceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_ENTITY_COORDS(tempObj),
																											  GET_ENTITY_ROTATION(tempObj),
																											  EULER_YXZ, TRUE, FALSE, DEFAULT,
																											  sCashGrabData.fGrabPhase, 1.0)

											//play bag animation							  
											IF DOES_ENTITY_EXIST(NET_TO_OBJ(niMinigameObjects[0]))
											
												STRING sBagAnimDictName, sBagAnimName
															
												sBagAnimDictName = GET_HEIST_BAG_MINIGAME_ANIM_DICT(tempObj, IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS))
											
												IF IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS)
													IF SHOULD_USE_SUIT_BAG_ANIMS()
														sBagAnimName = "bag_grab_suit"
													ELSE
														IF SHOULD_USE_ARMOUR_BAG_ANIMS()
															sBagAnimName = "bag_grab"
														ELSE
															sBagAnimName = "bag_grab_no_armour"
														ENDIF
													ENDIF
												ELSE
													IF SHOULD_USE_SUIT_BAG_ANIMS()
														sBagAnimName = PICK_STRING(IS_PLAYER_FEMALE(), "bag_grab_suit_female",  "bag_grab_suit")
													ELSE
														IF SHOULD_USE_ARMOUR_BAG_ANIMS()
															sBagAnimName = PICK_STRING(IS_PLAYER_FEMALE(), "bag_grab_female",  "bag_grab")
														ELSE
															sBagAnimName = PICK_STRING(IS_PLAYER_FEMALE(), "bag_grab_no_armour_female", "bag_grab_no_armour")
														ENDIF
													ENDIF
												ENDIF
												
												PRINTLN("[RUN_CASH_GRAB_MINIGAME] Playing bag HBGS_GRAB_ANIM synched scene animation ", sBagAnimDictName, "/", sBagAnimName, ".")
												NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(niMinigameObjects[0]), sCashGrabData.iTrolleySceneID,
																						 sBagAnimDictName, sBagAnimName, INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
												
												FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(NET_TO_OBJ(niMinigameObjects[0]))
											ENDIF
											
											//task player 
											NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(LocalPlayerPed, sCashGrabData.iTrolleySceneID,
																				  GET_HEIST_BAG_MINIGAME_ANIM_DICT(tempObj, IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS)), "grab",
																				  SLOW_BLEND_IN, SLOW_BLEND_OUT,
																				  SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS
																				  | SYNCED_SCENE_ON_ABORT_STOP_SCENE, RBF_PLAYER_IMPACT)

											//play trolley animation
											IF DOES_ENTITY_EXIST(tempObj)
												NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(tempObj, sCashGrabData.iTrolleySceneID,
																						 GET_HEIST_BAG_MINIGAME_ANIM_DICT(tempObj, IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS)), "cart_cash_dissapear",
																						 INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
												FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(tempObj)
											ENDIF
																		
											//start trolley synchronised scene
											NETWORK_START_SYNCHRONISED_SCENE(sCashGrabData.iTrolleySceneID)

											RESET_CASH_GRAB_SPEED(sCashGrabData)
											
											CLEAR_BIT(sCashGrabData.iBitSet, CASH_GRAB_BS_GRAB_ANIM_STOPPED)
										
										ELSE	//when grab scene is running
											
											FLOAT fMaxSpeed 
											fMaxSpeed = 1.5
											IF IS_GOLD_TROLLEY(GET_ENTITY_MODEL(tempObj))
												fMaxSpeed = 1.2
											ENDIF
											
											UPDATE_CASH_GRAB_SPEED(sCashGrabData, 0.75, fMaxSpeed)
											
											//hide and unhide the cash pile prop in player's hand based on grab anim events
											IF IS_ENTITY_PLAYING_ANIM(LocalPlayerPed, GET_HEIST_BAG_MINIGAME_ANIM_DICT(tempObj, IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS)), "grab")
												IF HAS_ANIM_EVENT_FIRED(LocalPlayerPed, GET_HASH_KEY("CASH_APPEAR"))
													PRINTLN(GET_THIS_SCRIPT_NAME(), ": Anim event CASH_APPEAR fired at phase ", GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sCashGrabData.iTrolleySceneID)), ".")
													IF NOT IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_ANIM_EVENTS_STARTED)
														IF DOES_ENTITY_EXIST(NET_TO_OBJ(niMinigameObjects[1])) AND NOT IS_ENTITY_DEAD(NET_TO_OBJ(niMinigameObjects[1]))
															SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[1]), TRUE)
															SET_BIT(sCashGrabData.iBitSet, CASH_GRAB_BS_ANIM_EVENTS_STARTED)
															PRINTLN(GET_THIS_SCRIPT_NAME(), ": Received anim event CASH_APPEAR at phase ", GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sCashGrabData.iTrolleySceneID)), ".")
														ENDIF
													ENDIF
												ENDIF
												IF HAS_ANIM_EVENT_FIRED(LocalPlayerPed, GET_HASH_KEY("RELEASE_CASH_DESTROY"))
													PRINTLN(GET_THIS_SCRIPT_NAME(), ": Anim event RELEASE_CASH_DESTROY fired at phase ", GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sCashGrabData.iTrolleySceneID)), ".")
													IF IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_ANIM_EVENTS_STARTED)
														IF DOES_ENTITY_EXIST(NET_TO_OBJ(niMinigameObjects[1])) AND NOT IS_ENTITY_DEAD(NET_TO_OBJ(niMinigameObjects[1]))
															SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[1]), FALSE)
															SET_BIT(sCashGrabData.iBitSet, CASH_GRAB_BS_CASH_BAGGED)
															CLEAR_BIT(sCashGrabData.iBitSet, CASH_GRAB_BS_ANIM_EVENTS_STARTED)
															PRINTLN(GET_THIS_SCRIPT_NAME(), ": Received anim event RELEASE_CASH_DESTROY at phase ", GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sCashGrabData.iTrolleySceneID)), ".")
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										
											//set the phase and rate of the synched scene
											IF IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sCashGrabData.iTrolleySceneID))
											
												//keep setting the scene rate based on speed calculated from player input
												SET_SYNCHRONIZED_SCENE_RATE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sCashGrabData.iTrolleySceneID), sCashGrabData.fGrabSpeed)
												
												//update this value as we need it for the progress bar
												sCashGrabData.fGrabPhase = GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sCashGrabData.iTrolleySceneID))
											ENDIF
											
											//check for minigame completion
											//and for grabbing interruption
											PRINTLN("[KH] PROCESS_CASH_GRAB - Starting cash bagging")
											IF NOT IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_MINIGAME_COMPLETED)
												
												//check for player input
												IF NOT IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_GRAB_ANIM_STOPPED)
													IF NOT IS_PAUSE_MENU_ACTIVE()
														IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
															IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
															OR (IS_THIS_ROCKSTAR_MISSION_SVM_PHANTOM2(g_FMMC_STRUCT.iRootContentIDHash) AND g_TransitionSessionNonResetVars.iLocalPlayerCashGrabbed >= iMaxCashLocalPlayerCanCarry)
																SET_BIT(sCashGrabData.iBitSet, CASH_GRAB_BS_GRAB_ANIM_STOPPED)
																
															ENDIF
														ELSE
															IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
															OR (IS_THIS_ROCKSTAR_MISSION_SVM_PHANTOM2(g_FMMC_STRUCT.iRootContentIDHash) AND g_TransitionSessionNonResetVars.iLocalPlayerCashGrabbed >= iMaxCashLocalPlayerCanCarry)
																SET_BIT(sCashGrabData.iBitSet, CASH_GRAB_BS_GRAB_ANIM_STOPPED)
																
															ENDIF
														ENDIF
													ENDIF
												ENDIF
												
												IF IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_CASH_BAGGED)
												
													IF ( sCashGrabData.fGrabSpeed < 1.0 )
													OR IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_GRAB_ANIM_STOPPED)
													
														sCashGrabData.fGrabPhase = GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sCashGrabData.iTrolleySceneID))
													
														sCashGrabData.eState = HBGS_IDLE_ANIM	//go to idle state

													ENDIF
													
													//increment the number of piles grabbed
													//use this to work out how much total cas we've grabbed
													sCashGrabData.iTotalCashPiles++
													
													IF IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS, ciFMMC_USE_SCORE_OFF_RULE_MINIGAMES)
														IF IS_LOW_CASH_TROLLEY(GET_ENTITY_MODEL(tempObj))
															iLowCashStacksGrabbed++
															IF iLowCashStacksGrabbed = 15
																INCREMENT_OFF_RULE_MINIGAME_SCORE_FROM_OBJECT(ciOffRuleScore_Low, iObj)
																iLowCashStacksGrabbed = 0
															ENDIF
														ELSE
															iCashStacksGrabbed++
															IF iCashStacksGrabbed = 9
																INCREMENT_OFF_RULE_MINIGAME_SCORE_FROM_OBJECT(ciOffRuleScore_Low, iObj)
																iCashStacksGrabbed = 0
															ENDIF
														ENDIF
													ENDIF
													
													IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_SlowPlayerDown)
													AND iGrabbedStolenGoods < ciSTOLEN_GOODS_END
														iGrabbedStolenGoods++
														IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_HEAVY_STOLEN_GOODS_CASINO_HEIST)
														AND IS_GOLD_TROLLEY(GET_ENTITY_MODEL(tempObj))
															
															SET_BIT(iLocalBoolCheck31, LBOOL31_HEAVY_STOLEN_GOODS_CASINO_HEIST)
															PRINTLN("[CashGrab] PROCESS_CASH_GRAB - Setting stolen goods as heavy")
														ENDIF
														PRINTLN("[CashGrab] PROCESS_CASH_GRAB - iGrabbedStolenGoods: ", iGrabbedStolenGoods)
													ENDIF
													
													//check for bonus value cash piles, their counter is incremented when host processes script event
													//check for local number of bonus piles grabbed as network lag can make the bonus piles count be out of sync, see B*2097960						
													IF 	( MC_ServerBD.iCashGrabBonusPilesGrabbed[iObj] < sCashGrabData.iBonusCashPiles )
													AND ( sCashGrabData.iBonusCashPilesGrabbed < sCashGrabData.iBonusCashPiles )
														PRINTLN("[CashGrab] PROCESS_CASH_GRAB - Adding bonus cash pile")
														//check if this would be the last cash pile collected
														BOOL bIsFinalCashPile
														INT iCashType
														
														iCashType = FMMC_CASH_ORNATE_BANK
														IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCashAmount != 0
															iCashType = g_FMMC_STRUCT.iCashReward
														ENDIF
														IF (MC_ServerBD.iCashGrabTotalTake + sCashGrabData.iBonusCashPileValue) = GET_HEIST_MODIFIED_TAKE_FOR_CASH_GRAB(iCashType)
															bIsFinalCashPile = TRUE
														ENDIF
														IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_AllowMinigameOffRule)
															IF GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sCashGrabData.iTrolleySceneID)) >= 1.0
																PRINTLN("[CashGrab] PROCESS_CASH_GRAB - bIsFinalCashPile = TRUE - Bonus cash for off rule minigame")
																bIsFinalCashPile = TRUE
															ELSE
																IF bIsFinalCashPile
																	bIsFinalCashPile = FALSE
																ENDIF
															ENDIF
														ENDIF
													
														//broadcast event about bonus cash pile grabbed
														BROADCAST_SCRIPT_EVENT_GRAB_CASH(iObj, sCashGrabData.iBonusCashPileValue, sCashGrabData.iTotalCashPiles, TRUE, bIsFinalCashPile, IS_LOW_CASH_TROLLEY(GET_ENTITY_MODEL(tempObj)))
														
														//increment the local number of bonus piles grabbed
														//in case there is server lag and bonus cash piles grabbed are not in sync between players
														sCashGrabData.iBonusCashPilesGrabbed++
														
														IF SVM_FLOW_IS_CURRENT_MISSION_SVM_MISSION()
															//Add to your local cash so we can track how much the local player has
															g_TransitionSessionNonResetVars.iLocalPlayerCashGrabbed += sCashGrabData.iBaseCashPileValue
															PRINTLN("[CashGrab] PROCESS_CASH_GRAB - g_TransitionSessionNonResetVars.iLocalPlayerCashGrabbed: ", g_TransitionSessionNonResetVars.iLocalPlayerCashGrabbed)
														ENDIF
													ELSE
														PRINTLN("[CashGrab] PROCESS_CASH_GRAB - Adding cash pile")
														//check if this would be the last cash pile collected
														BOOL bIsFinalCashPile
														INT iMaxCashTake
														INT iCashType
														
														iCashType = FMMC_CASH_ORNATE_BANK
														IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCashAmount != 0
															iCashType = g_FMMC_STRUCT.iCashReward
														ENDIF
														
														iMaxCashTake = GET_HEIST_MODIFIED_TAKE_FOR_CASH_GRAB(iCashType)
														
														PRINTLN("[CashGrab] PROCESS_CASH_GRAB - sCashGrabData.iBaseCashPileValue: ", sCashGrabData.iBaseCashPileValue)
														PRINTLN("[CashGrab] PROCESS_CASH_GRAB - MC_ServerBD.iCashGrabTotalTake: ", MC_ServerBD.iCashGrabTotalTake)
														PRINTLN("[CashGrab] PROCESS_CASH_GRAB - iMaxCashTake: ", iMaxCashTake)
														
														IF (MC_ServerBD.iCashGrabTotalTake + sCashGrabData.iBaseCashPileValue) >= iMaxCashTake
															bIsFinalCashPile = TRUE
															PRINTLN("[CashGrab] PROCESS_CASH_GRAB - Final cash pile")
															IF SVM_FLOW_IS_CURRENT_MISSION_SVM_MISSION()
																sCashGrabData.fGrabPhase 			= 0.99
																SET_BIT(sCashGrabData.iBitSet, CASH_GRAB_BS_MINIGAME_COMPLETED)
																sCashGrabData.eState = HBGS_EXIT_ANIM	//go to exit state
															ENDIF
														ENDIF
														
														IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_AllowMinigameOffRule)
															IF GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sCashGrabData.iTrolleySceneID)) >= 1.0
																bIsFinalCashPile = TRUE
																PRINTLN("[CashGrab] PROCESS_CASH_GRAB - bIsFinalCashPile = TRUE - Regular cash for off rule minigame")
															ELSE
																IF bIsFinalCashPile
																	bIsFinalCashPile = FALSE
																ENDIF
															ENDIF
														ENDIF
													
														//broadcast event about default cash pile grabbed
														BROADCAST_SCRIPT_EVENT_GRAB_CASH(iObj, sCashGrabData.iBaseCashPileValue, sCashGrabData.iTotalCashPiles, FALSE, bIsFinalCashPile, IS_LOW_CASH_TROLLEY(GET_ENTITY_MODEL(tempObj)))
														
														IF SVM_FLOW_IS_CURRENT_MISSION_SVM_MISSION()
															//Add to your local cash so we can track how much the local player has
															g_TransitionSessionNonResetVars.iLocalPlayerCashGrabbed += sCashGrabData.iBaseCashPileValue
															PRINTLN("[CashGrab] PROCESS_CASH_GRAB - g_TransitionSessionNonResetVars.iLocalPlayerCashGrabbed: ", g_TransitionSessionNonResetVars.iLocalPlayerCashGrabbed)
														ENDIF
																 
													ENDIF
													
													IF IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sCashGrabData.iTrolleySceneID))
														IF GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sCashGrabData.iTrolleySceneID)) >= 1.0
														
															sCashGrabData.fGrabPhase 			= 0.99
															SET_BIT(sCashGrabData.iBitSet, CASH_GRAB_BS_MINIGAME_COMPLETED)
															
															sCashGrabData.eState = HBGS_EXIT_ANIM	//go to exit state
														ELSE
															PRINTLN("[CashGrab] PROCESS_CASH_GRAB - GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sCashGrabData.iTrolleySceneID)) < 1.0")
														ENDIF
													ELSE
														PRINTLN("[CashGrab] PROCESS_CASH_GRAB - IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sCashGrabData.iTrolleySceneID)) = FALSE")
													ENDIF
													
													PRINTLN("[CashGrab] PROCESS_CASH_GRAB - CLEAR_BIT(sCashGrabData.iBitSet, CASH_GRAB_BS_CASH_BAGGED)")
													CLEAR_BIT(sCashGrabData.iBitSet, CASH_GRAB_BS_CASH_BAGGED)
												ENDIF
												
											ENDIF
											
											PRINTLN("[CashGrab] PROCESS_CASH_GRAB - Finished cash bagging")
										ENDIF
									
									BREAK
									
									CASE HBGS_EXIT_ANIM
									
										IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID))
										OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sCashGrabData.iTrolleySceneID))
										OR NOT IS_ENTITY_PLAYING_ANIM(LocalPlayerPed, GET_HEIST_BAG_MINIGAME_ANIM_DICT(tempObj, IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS)), "exit")
											
											//handle trolley
											
											//create trolley synchronised scene
											sCashGrabData.iTrolleySceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_ENTITY_COORDS(tempObj),
																											  GET_ENTITY_ROTATION(tempObj),
																											  EULER_YXZ, TRUE, FALSE, DEFAULT,
																											  sCashGrabData.fGrabPhase, 0.0)
											PRINTLN("[CashGrab] HBGS_EXIT_ANIM Starting scene sCashGrabData.iTrolleySceneID with id ", sCashGrabData.iTrolleySceneID, " and rate 0.0")
											PRINTLN("[CashGrab] HBGS_EXIT_ANIM Local scene id from network id for sCashGrabData.iTrolleySceneID is ", NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sCashGrabData.iTrolleySceneID))
																		
											IF DOES_ENTITY_EXIST(tempObj)
												NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(tempObj, sCashGrabData.iTrolleySceneID,
																						 GET_HEIST_BAG_MINIGAME_ANIM_DICT(tempObj, IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS)), "cart_cash_dissapear",
																						 INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
												FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(tempObj)
											ENDIF
																		
											//start trolley synchronised scene
											NETWORK_START_SYNCHRONISED_SCENE(sCashGrabData.iTrolleySceneID)
											
											//handle player and bag
											
											//create player synchronised scene
											MC_playerBD[iPartToUse].iSynchSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_ENTITY_COORDS(tempObj), GET_ENTITY_ROTATION(tempObj), EULER_YXZ, TRUE, FALSE, DEFAULT, DEFAULT, 1.25)
											
											//play bag animation							  
											IF DOES_ENTITY_EXIST(NET_TO_OBJ(niMinigameObjects[0]))
											
												STRING sBagAnimDictName, sBagAnimName
															
												sBagAnimDictName = GET_HEIST_BAG_MINIGAME_ANIM_DICT(tempObj, IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS))
											
												IF IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS)
													IF SHOULD_USE_SUIT_BAG_ANIMS()
														sBagAnimName = "bag_exit_suit"
													ELSE
														IF SHOULD_USE_ARMOUR_BAG_ANIMS()
															sBagAnimName = "bag_exit"
														ELSE
															sBagAnimName = "bag_exit_no_armour"
														ENDIF
													ENDIF
												ELSE
													IF SHOULD_USE_SUIT_BAG_ANIMS()
														sBagAnimName = PICK_STRING(IS_PLAYER_FEMALE(), "bag_exit_suit_female",  "bag_exit_suit")
													ELSE
														IF SHOULD_USE_ARMOUR_BAG_ANIMS()
															sBagAnimName = PICK_STRING(IS_PLAYER_FEMALE(), "bag_exit_female",  "bag_exit")
														ELSE
															sBagAnimName = PICK_STRING(IS_PLAYER_FEMALE(), "bag_exit_no_armour_female", "bag_exit_no_armour")
														ENDIF
													ENDIF
												ENDIF
												
												PRINTLN("[CashGrab] Playing bag HBGS_EXIT_ANIM synched scene animation ", sBagAnimDictName, "/", sBagAnimName, ".")
												NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(niMinigameObjects[0]), MC_playerBD[iPartToUse].iSynchSceneID,
																						 sBagAnimDictName, sBagAnimName, INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
																						
												FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(NET_TO_OBJ(niMinigameObjects[0]))
											ENDIF
											
											//task player 
											NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(LocalPlayerPed, MC_playerBD[iPartToUse].iSynchSceneID,
																				  GET_HEIST_BAG_MINIGAME_ANIM_DICT(tempObj, IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS)), "exit",
																				  SLOW_BLEND_IN, WALK_BLEND_OUT,
																				  SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS
																				  | SYNCED_SCENE_ON_ABORT_STOP_SCENE, RBF_PLAYER_IMPACT)
											FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed, DEFAULT, TRUE)
																		
											//start player synchronised scene
											NETWORK_START_SYNCHRONISED_SCENE(MC_playerBD[iPartToUse].iSynchSceneID)
											
											//delete the cash pile prop in player's hand
											DELETE_MONEY_OBJECT()
											
										ELSE
											
											//print trolley scene information
											IF IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sCashGrabData.iTrolleySceneID))
												PRINTLN("[CashGrab] HBGS_EXIT_ANIM: Trolley scene information 
														  sCashGrabData.iTrolleySceneID = ", sCashGrabData.iTrolleySceneID,
														" local id from sCashGrabData.iTrolleySceneID = ", NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sCashGrabData.iTrolleySceneID),
														" scene phase = ", GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sCashGrabData.iTrolleySceneID)),
														" scene rate = ", GET_SYNCHRONIZED_SCENE_RATE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sCashGrabData.iTrolleySceneID)))
											ENDIF
										
											
											IF IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sCashGrabData.iTrolleySceneID))
												IF GET_SYNCHRONIZED_SCENE_RATE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sCashGrabData.iTrolleySceneID)) != 0.0
													SET_SYNCHRONIZED_SCENE_RATE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sCashGrabData.iTrolleySceneID), 0.0)
													PRINTLN("[CashGrab] HBGS_EXIT_ANIM Setting scene rate sCashGrabData.iTrolleySceneID with id ", sCashGrabData.iTrolleySceneID, " to rate 0.0")
													PRINTLN("[CashGrab] HBGS_EXIT_ANIM Local scene id from network id for sCashGrabData.iTrolleySceneID is ", NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sCashGrabData.iTrolleySceneID))
												ENDIF
											ENDIF

											IF IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID))
												IF IS_ENTITY_PLAYING_ANIM(LocalPlayerPed, GET_HEIST_BAG_MINIGAME_ANIM_DICT(tempObj, IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS)), "exit")
												
													IF GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID)) >= 0.70
														
														//set the bag variation back on the player
														APPLY_DUFFEL_BAG_HEIST_GEAR(LocalPlayerPed, iLocalPart)
														RELEASE_PRELOADED_MP_HEIST_OUTFIT(LocalPlayerPed)
														
														REINIT_NET_TIMER(tdHackTimer)
																								
														iHackProgress++
														
													ENDIF
												ENDIF
											ENDIF
																			
										ENDIF

									BREAK
								
								ENDSWITCH
							
								#IF IS_DEBUG_BUILD
									//display state of the minigame
									TEXT_LABEL_63 tl63Trolly
									tl63Trolly = "State: "
									tl63Trolly += GET_HEIST_BAG_GRAB_STATE(sCashGrabData)
									DRAW_DEBUG_TEXT_ABOVE_ENTITY(tempObj, tl63Trolly, 0.70)
									tl63Trolly = "Diff: "
									tl63Trolly += GET_HEIST_BAG_GRAB_DIFFICULTY(sCashGrabData)
									DRAW_DEBUG_TEXT_ABOVE_ENTITY(tempObj, tl63Trolly, 0.675)
									tl63Trolly = "Diff mod: "
									tl63Trolly += GET_STRING_FROM_FLOAT(sCashGrabData.fMissionDifficultyModifier)
									DRAW_DEBUG_TEXT_ABOVE_ENTITY(tempObj, tl63Trolly, 0.65)
									//display anim phase and rate
									tl63Trolly = "Anim Phase: "
									tl63Trolly += GET_STRING_FROM_FLOAT(sCashGrabData.fGrabPhase)
									DRAW_DEBUG_TEXT_ABOVE_ENTITY(tempObj, tl63Trolly, 0.625)
									tl63Trolly = "Anim Rate: "
									tl63Trolly += GET_STRING_FROM_FLOAT(sCashGrabData.fGrabSpeed)
									DRAW_DEBUG_TEXT_ABOVE_ENTITY(tempObj, tl63Trolly, 0.60)
									//display total cash piles, bonus cash piles and bonus cash piles grabbed, and total money grabbed
									tl63Trolly = "Total Cash: "
									tl63Trolly += GET_STRING_FROM_INT(MC_ServerBD.iCashGrabTotal[iobj])
									DRAW_DEBUG_TEXT_ABOVE_ENTITY(tempObj, tl63Trolly, 0.575)
									tl63Trolly = "Bonus grabbed: "
									tl63Trolly += GET_STRING_FROM_INT(MC_ServerBD.iCashGrabBonusPilesGrabbed[iobj])
									DRAW_DEBUG_TEXT_ABOVE_ENTITY(tempObj, tl63Trolly, 0.55)
									tl63Trolly = "Bonus piles: "
									tl63Trolly += GET_STRING_FROM_INT(sCashGrabData.iBonusCashPiles)
									DRAW_DEBUG_TEXT_ABOVE_ENTITY(tempObj, tl63Trolly, 0.525)
									tl63Trolly = "Total Piles Grabbed: "
									tl63Trolly += GET_STRING_FROM_INT(sCashGrabData.iTotalCashPiles)
									DRAW_DEBUG_TEXT_ABOVE_ENTITY(tempObj, tl63Trolly, 0.50)
									//display base and bonus cash value
									tl63Trolly = "Bonus Pile Value: "
									tl63Trolly += GET_STRING_FROM_INT(sCashGrabData.iBonusCashPileValue)
									DRAW_DEBUG_TEXT_ABOVE_ENTITY(tempObj, tl63Trolly, 0.475)
									tl63Trolly = "Pile Value: "
									tl63Trolly += GET_STRING_FROM_INT(sCashGrabData.iBaseCashPileValue)
									DRAW_DEBUG_TEXT_ABOVE_ENTITY(tempObj, tl63Trolly, 0.45)
								#ENDIF
								
								IF SVM_FLOW_IS_CURRENT_MISSION_SVM_MISSION()
									IF DOES_ENTITY_EXIST(NET_TO_OBJ(niMinigameObjects[0]))
										IF IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_BAG_PROP_LOCALLY_VISIBLE)
											IF NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[0])
												PRINTLN("[CashGrab] - Setting bag visible")

												SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[0]), TRUE)
											ELSE
												PRINTLN("[CashGrab] - Requesting control of bag")
												NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(niMinigameObjects[0])
											ENDIF
										ELSE
											PRINTLN("[CashGrab] - CASH_GRAB_BS_BAG_PROP_LOCALLY_VISIBLE not set, not setting locally visible")
										ENDIF
									ELSE
										PRINTLN("[CashGrab] - Net id in Case 2 doesn't exist not setting locally visible")
									ENDIF
								ELSE
									IF DOES_ENTITY_EXIST(NET_TO_OBJ(niMinigameObjects[0]))
										IF IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_BAG_PROP_LOCALLY_VISIBLE)
											PRINTLN("[CashGrab] - Setting bag visible locally Case 1")
											SET_ENTITY_LOCALLY_VISIBLE(NET_TO_OBJ(niMinigameObjects[0]))
											IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
												IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(niMinigameObjects[0])
													SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[0]), TRUE)
													PRINTLN("[CashGrab] - Setting bag visible")
												ENDIF
											ENDIF
										ENDIF
									ELSE
										PRINTLN("[CashGrab] - Net id in Case 1 doesn't exist not setting locally visible")
									ENDIF
								ENDIF
								
							BREAK

							CASE 2	//cleanup
								
								IF SVM_FLOW_IS_CURRENT_MISSION_SVM_MISSION()
									IF DOES_ENTITY_EXIST(NET_TO_OBJ(niMinigameObjects[0]))
										IF IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_BAG_PROP_LOCALLY_VISIBLE)
											IF NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[0])
												PRINTLN("[CashGrab] - Setting bag visible")

												SET_ENTITY_VISIBLE(NET_TO_OBJ(niMinigameObjects[0]), TRUE)
											ELSE
												PRINTLN("[CashGrab] - Requesting control of bag")
												NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(niMinigameObjects[0])
											ENDIF
										ELSE
											PRINTLN("[CashGrab] - CASH_GRAB_BS_BAG_PROP_LOCALLY_VISIBLE not set, not setting locally visible")
										ENDIF
									ELSE
										PRINTLN("[CashGrab] - Net id in Case 2 doesn't exist not setting locally visible")
									ENDIF
								ELSE
									IF DOES_ENTITY_EXIST(NET_TO_OBJ(niMinigameObjects[0]))
										IF IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_BAG_PROP_LOCALLY_VISIBLE)
											PRINTLN("[CashGrab] - Setting bag visible locally")

											SET_ENTITY_LOCALLY_VISIBLE(NET_TO_OBJ(niMinigameObjects[0]))
										ELSE
											PRINTLN("[CashGrab] - CASH_GRAB_BS_BAG_PROP_LOCALLY_VISIBLE not set, not setting locally visible")
										ENDIF
									ELSE
										PRINTLN("[CashGrab] - Net id in Case 2 doesn't exist not setting locally visible")
									ENDIF
								ENDIF
							
								//wait until streaming requests have completed
								//then delete the bag prop, clear tasks and exit the grab minigame
								IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(LocalPlayerPed)
								OR HAS_NET_TIMER_EXPIRED(tdHackTimer, 2000)	
							
									//for B*2200165, don't delete the bag prop but make it invisible locally instead
									//remote players watching a player grabbing cash will also set the bag locally invisble on their machines
									//this is needed to fix the bag pop triggered by bag deletion and variation setting, which don't synch perfectly over network
									//DELETE_BAG_OBJECT()
									IF DOES_ENTITY_EXIST(NET_TO_OBJ(niMinigameObjects[0]))
										PRINTLN("[CashGrab] - Setting bag invisible locally Case 2")
										SET_ENTITY_LOCALLY_INVISIBLE(NET_TO_OBJ(niMinigameObjects[0]))
									ELSE
										PRINTLN("[CashGrab] - Net id in Case 2 doesn't exist not setting locally invisible")
									ENDIF
									DELETE_MONEY_OBJECT()
									CLEANUP_HEIST_BAG_MINIGAME_ASSETS(tempObj, IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_USE_HIGH_HEELS_ANIMS), MC_playerBD_1[iLocalPart].mnHeistGearBag)
									
									IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetFour, cibsOBJ4_DisableMinigameCamera)
									AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetFour, cibsOBJ4_UseAlternateCashGrabCamera)
										SET_GAMEPLAY_CAM_RELATIVE_PITCH()
										SET_GAMEPLAY_CAM_RELATIVE_HEADING()
									ENDIF
									CLEANUP_CASH_GRAB_SCRIPTED_CAMERA(sCashGrabData)
									STOP_AUDIO_SCENE("DLC_HEIST_MINIGAME_PAC_CASH_GRAB_SCENE")
																		
									REINIT_NET_TIMER(tdHackTimer)
													
									iHackProgress++
									
								ENDIF

							BREAK
							
							CASE 3	//actual cleanup, delayed for B*2200165, this gives time remote players to set the bag prop invisble locally for themselves
								
								PRINTLN("[CashGrab] - Case 3")
								IF NETWORK_DOES_NETWORK_ID_EXIST(niMinigameObjects[0])
									PRINTLN("[KH] 3143565 (PRINTS) - Setting bag invisible locally")
									SET_ENTITY_LOCALLY_INVISIBLE(NET_TO_OBJ(niMinigameObjects[0]))
								ELSE
									PRINTLN("[CashGrab] - Net id doesn't exist not setting locally invisible")
								ENDIF
							
								IF HAS_NET_TIMER_EXPIRED(tdHackTimer, 500)
								
									DELETE_BAG_OBJECT()
									IF NOT IS_PED_INJURED(LocalPlayerPed)
										CLEAR_PED_TASKS(LocalPlayerPed)
										TASK_CLEAR_LOOK_AT(LocalPlayerPed)
									ENDIF
									NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
									RUN_LOCAL_PLAYER_PROOF_SETTING(TRUE)
									APPLY_DUFFEL_BAG_HEIST_GEAR(LocalPlayerPed, iLocalPart)
									SET_CURRENT_PED_WEAPON(LocalPlayerPed, eWeaponBeforeMinigame, TRUE)
							
									MC_PlayerBD[iPartToUse].iHackTime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stMGTimer0)
									MC_playerBD[iPartToUse].iNumHacks++
									PRINTLN("[CashGrab] inumHacks HEIST TELEMETRY CASH GRAB MINIGAME  -", MC_playerBD[iPartToUse].iNumHacks)
									
									ENABLE_INTERACTION_MENU()
							
									IF IS_BIT_SET(sCashGrabData.iBitSet, CASH_GRAB_BS_MINIGAME_COMPLETED)
										MC_playerBD[iPartToUse].iObjHacked = iobj //only call this on completion of clearing the trolly
									ENDIF
									
									RESET_OBJ_HACKING_INT()
									
									STORE_MINIGAME_TIMER_FOR_TELEMETRY(1)
									STORE_MINIGAME_ATTEMPTS_FOR_TELEMETRY(1)
									PRINTLN("[RCC MISSION][TELEMETRY][CashGrab] - STORE_MINIGAME_TIMER_FOR_TELEMETRY - SLOT 1")
									PRINTLN("[RCC MISSION][TELEMETRY][CashGrab] - STORE_MINIGAME_TIMER_FOR_TELEMETRY - SLOT 1")							
									
									FORCE_QUIT_PASS_CASH_GRAB_MINIGAME_KEEP_DATA(sCashGrabData)
			
									iHackProgress 	= 0
									iHackLimitTimer = 0

								ENDIF
							
							BREAK
							
						ENDSWITCH
						
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF SVM_FLOW_IS_CURRENT_MISSION_SVM_MISSION()
		PROCESS_CASH_GRAB_FAIL_CLEANUP(iObj)
	ENDIF

ENDPROC

INT iNoteTimer = 0

///PURPOSE: This function is what checks to see if the player has been hit by a bullet/explosion
///    and plays the correct vfx
PROC DROP_MONEY_IF_HIT_EVERY( INT iFrequencyMS, BOOL bCokeGrab = FALSE )
	
	STRING sPTFXAsset = "scr_ornate_heist"
	STRING sPTFXName  = "scr_heist_ornate_banknotes"
	INT iCash = FMMC_CASH_ORNATE_BANK
	BOOL bUseArtworkColor = FALSE
	
	IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
		iCash = FMMC_CASH_CASINO_HEIST

		//Casino Heist Cash Target uses scr_ornate_heist/scr_heist_ornate_banknotes.
		IF g_sCasinoHeistMissionConfigData.eTarget = CASINO_HEIST_TARGET_TYPE__DIAMONDS
			sPTFXAsset = "scr_ch_finale"
			sPTFXName  = "scr_ch_finale_hit_diamond"
		ELIF g_sCasinoHeistMissionConfigData.eTarget = CASINO_HEIST_TARGET_TYPE__GOLD
			sPTFXAsset = "scr_ch_finale"
			sPTFXName  = "scr_ch_finale_hit_gold"
		ELIF g_sCasinoHeistMissionConfigData.eTarget = CASINO_HEIST_TARGET_TYPE__ART
			sPTFXAsset = "scr_ch_finale"
			sPTFXName  = "scr_ch_finale_hit_art"
			bUseArtworkColor = TRUE
		ENDIF
	ENDIF
	
	IF g_TransitionSessionNonResetVars.bInvolvedInCashGrab
		IF (MC_ServerBD.iCashGrabTotalTake > GET_HEIST_MINIMUM_TAKE_FOR_CASH_DROP(iCash))
		OR (SVM_FLOW_IS_CURRENT_MISSION_SVM_MISSION() AND MC_ServerBD.iCashGrabTotalTake > 250000)

			BOOL bDropMoney

			//set this flag to true as it is involved in cash rewards and celebration screen
			//these procedures might need changing
			bHasBeatCashGrab = TRUE
			
			IF bCokeGrab
				PRINTLN("[KH] DROP_MONEY_IF_HIT_EVERY - Loading coke grab drop fx")
				sPTFXAsset = "scr_ie_svm_phantom2"
				sPTFXName = "scr_ie_bul_coc_bag"
				REQUEST_NAMED_PTFX_ASSET(sPTFXAsset)
			ElSE
				REQUEST_NAMED_PTFX_ASSET(sPTFXAsset)
			ENDIF
			
			IF bLocalPlayerPedOk
				IF NOT IS_PED_ON_ANY_BIKE(LocalPlayerPed)
					//set the offsets based on player model and high heels
					SWITCH GET_ENTITY_MODEL(LocalPlayerPed)
						CASE MP_M_FREEMODE_01
							vCashGrabDropOffset1 		= << -0.180, -0.190, -0.150 >>
							vCashGrabDropOffset2 		= << -0.130, -0.190, 0.0000 >>
							vCashGrabDropOffset3 		= << -0.090, -0.170, 0.1600 >>
						BREAK
						CASE MP_F_FREEMODE_01
							IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
								vCashGrabDropOffset1 	= << -0.270, -0.130, -0.190 >>
								vCashGrabDropOffset2 	= << -0.220, -0.160, -0.010 >>
								vCashGrabDropOffset3 	= << -0.190, -0.160, 0.1100 >>
							ELSE
								vCashGrabDropOffset1 	= << -0.190, -0.130, -0.190 >>
								vCashGrabDropOffset2 	= << -0.130, -0.160, -0.010 >>
								vCashGrabDropOffset3 	= << -0.100, -0.160, 0.1100 >>
							ENDIF
						BREAK
					ENDSWITCH
				ELSE
					//set the offset if player is on a motorbike
					vCashGrabDropOffset1 				= << -0.270, -0.130, -0.190 >>
					vCashGrabDropOffset2 				= << -0.220, -0.160, -0.010 >>
					vCashGrabDropOffset3 				= << -0.190, -0.160, 0.1100 >>
				ENDIF
			ENDIF

			IF GET_GAME_TIMER() - iNoteTimer > iFrequencyMs
								
				//check for player damage
				IF NOT bDropMoney
					IF IS_ENTITY_ON_FIRE(LocalPlayerPed)
					OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(LocalPlayerPed)
					OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_OBJECT(LocalPlayerPed)
					OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(LocalPlayerPed)
					
						CLEAR_ENTITY_LAST_DAMAGE_ENTITY(LocalPlayerPed)
					
						IF NOT IS_PED_PLAYING_ANY_CASH_GRAB_ANIM(LocalPlayerPed)
							IF HAS_NAMED_PTFX_ASSET_LOADED(sPTFXAsset)
								USE_PARTICLE_FX_ASSET(sPTFXAsset)
								IF bCokeGrab
									PRINTLN("[KH] DROP_MONEY_IF_HIT_EVERY - Playing coke grab drop fx")
								ENDIF
								
								IF bUseArtworkColor
									INT iTint = GET_RANDOM_INT_IN_RANGE(0, 9) //0-3 will just use the default tint.
									IF iTint > 7
										SET_PARTICLE_FX_NON_LOOPED_COLOUR(0.41, 0.25, 0.13)   //Brown
									ELIF iTint > 6
										SET_PARTICLE_FX_NON_LOOPED_COLOUR(0.18, 0.31, 0.17)   //Green
									ELIF iTint > 5
										SET_PARTICLE_FX_NON_LOOPED_COLOUR(0.45, 0.39, 0.18)   //Yellow
									ELIF iTint > 4
										SET_PARTICLE_FX_NON_LOOPED_COLOUR(0.33, 0.53, 0.6) 	  //Blue
									ELIF iTint > 3
										SET_PARTICLE_FX_NON_LOOPED_COLOUR(0.48, 0.7, 0.0)     //Red
									ENDIF
								ENDIF
								
								START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_ENTITY(sPTFXName, LocalPlayerPed,
																				 PICK_VECTOR(IS_PED_ON_ANY_BIKE(LocalPlayerPed),
																				 << 0.00, -0.10, 0.20>>, << -0.10, -0.30, 0.0>>),
																				 << 0.0, 0.0, 0.0 >>)
								
								BROADCAST_SCRIPT_EVENT_GRAB_CASH_CASH_DROPPED(0.5, FALSE)
								PRINTLN( "[JJT MISSION][HEIST] Dropping money, scale of 0.5" )
								iNoteTimer = GET_GAME_TIMER()
								bDropMoney = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT bDropMoney
					//check bullets and explosions around player bag
					IF IS_BULLET_IN_AREA(GET_PED_BONE_COORDS(LocalPlayerPed, BONETAG_PELVIS, vCashGrabDropOffset1 ), fCashGrabDropRadius, FALSE )
					OR IS_BULLET_IN_AREA(GET_PED_BONE_COORDS(LocalPlayerPed, BONETAG_PELVIS, vCashGrabDropOffset2 ), fCashGrabDropRadius, FALSE )
					OR IS_BULLET_IN_AREA(GET_PED_BONE_COORDS(LocalPlayerPed, BONETAG_PELVIS, vCashGrabDropOffset3 ), fCashGrabDropRadius, FALSE )
					OR IS_EXPLOSION_IN_SPHERE( EXP_TAG_GRENADE, GET_ENTITY_COORDS( LocalPlayerPed ), 5.0 )
					OR IS_EXPLOSION_IN_SPHERE( EXP_TAG_GRENADELAUNCHER, GET_ENTITY_COORDS( LocalPlayerPed ), 5.0 )
					OR IS_EXPLOSION_IN_SPHERE( EXP_TAG_STICKYBOMB, GET_ENTITY_COORDS( LocalPlayerPed ), 5.0 )
					OR IS_EXPLOSION_IN_SPHERE( EXP_TAG_MOLOTOV, GET_ENTITY_COORDS( LocalPlayerPed ), 5.0 )
					OR IS_EXPLOSION_IN_SPHERE( EXP_TAG_ROCKET, GET_ENTITY_COORDS( LocalPlayerPed ), 5.0 )
					OR IS_EXPLOSION_IN_SPHERE( EXP_TAG_TANKSHELL, GET_ENTITY_COORDS( LocalPlayerPed ), 5.0 )
					OR IS_EXPLOSION_IN_SPHERE( EXP_TAG_CAR, GET_ENTITY_COORDS( LocalPlayerPed ), 5.0 )
					OR IS_EXPLOSION_IN_SPHERE( EXP_TAG_BIKE, GET_ENTITY_COORDS( LocalPlayerPed ), 5.0 )
					OR IS_EXPLOSION_IN_SPHERE( EXP_TAG_TRUCK, GET_ENTITY_COORDS( LocalPlayerPed ), 5.0 )
					OR IS_EXPLOSION_IN_SPHERE( EXP_TAG_PLANE, GET_ENTITY_COORDS( LocalPlayerPed ), 5.0 )
					OR IS_EXPLOSION_IN_SPHERE( EXP_TAG_PLANE_ROCKET, GET_ENTITY_COORDS( LocalPlayerPed ), 5.0 )
					OR IS_EXPLOSION_IN_SPHERE( EXP_TAG_VEHICLE_BULLET, GET_ENTITY_COORDS( LocalPlayerPed ), 5.0 )
					OR GET_NUMBER_OF_FIRES_IN_RANGE( GET_ENTITY_COORDS( LocalPlayerPed ), 0.75 ) > 0
					
						CLEAR_ENTITY_LAST_DAMAGE_ENTITY(LocalPlayerPed)
					
						IF NOT IS_PED_PLAYING_ANY_CASH_GRAB_ANIM(LocalPlayerPed)
							IF HAS_NAMED_PTFX_ASSET_LOADED(sPTFXAsset)
								USE_PARTICLE_FX_ASSET(sPTFXAsset)
								IF bCokeGrab
									PRINTLN("[KH] DROP_MONEY_IF_HIT_EVERY - Playing coke grab drop fx")
								ENDIF
								
								IF bUseArtworkColor
									INT iTint = GET_RANDOM_INT_IN_RANGE(0, 9) //0-3 will just use the default tint.
									IF iTint > 7
										SET_PARTICLE_FX_NON_LOOPED_COLOUR(0.41, 0.25, 0.13)   //Brown
									ELIF iTint > 6
										SET_PARTICLE_FX_NON_LOOPED_COLOUR(0.18, 0.31, 0.17)   //Green
									ELIF iTint > 5
										SET_PARTICLE_FX_NON_LOOPED_COLOUR(0.45, 0.39, 0.18)   //Yellow
									ELIF iTint > 4
										SET_PARTICLE_FX_NON_LOOPED_COLOUR(0.33, 0.53, 0.6) 	  //Blue
									ELIF iTint > 3
										SET_PARTICLE_FX_NON_LOOPED_COLOUR(0.48, 0.7, 0.0)     //Red
									ENDIF
								ENDIF
								
								START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_ENTITY(sPTFXName, LocalPlayerPed,
																				 PICK_VECTOR(IS_PED_ON_ANY_BIKE(LocalPlayerPed),
																				 << 0.00, -0.10, 0.20>>, << -0.10, -0.30, 0.0>>),
																				 << 0.0, 0.0, 0.0 >>)
								
								BROADCAST_SCRIPT_EVENT_GRAB_CASH_CASH_DROPPED(1.0, TRUE)
								PRINTLN( "[JJT MISSION][HEIST] Dropping money, scale of 1.0" )
								iNoteTimer = GET_GAME_TIMER()
								bDropMoney = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			PRINTLN( "[JJT MISSION][HEIST] Not enough cash to check for drop. ", MC_ServerBD.iCashGrabTotalTake, "/", GET_HEIST_MINIMUM_TAKE_FOR_CASH_DROP(iCash) )
		ENDIF
	ENDIF
ENDPROC

PROC DRAW_CASH_GRAB_TAKE_HUD()

	IF NOT ( g_bMissionEnding OR g_bMissionOver )
		IF g_TransitionSessionNonResetVars.bDisplayCashGrabTake
			SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
			
			IF ( bDrawCashGrabTakeRed )
				DRAW_GENERIC_SCORE(MC_ServerBD.iCashGrabTotalTake, "MONEY_HELD", 1000, HUD_COLOUR_RED, HUDORDER_DONTCARE, FALSE, "HUD_CASH")
				IF ( iDrawCashGrabTakeRedTime = 0 )
					iDrawCashGrabTakeRedTime = GET_GAME_TIMER()
				ELSE
					IF GET_GAME_TIMER() - iDrawCashGrabTakeRedTime > 500
						bDrawCashGrabTakeRed = FALSE
					ENDIF
				ENDIF
			ELSE
				DRAW_GENERIC_SCORE(MC_ServerBD.iCashGrabTotalTake, "MONEY_HELD", 1000, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, "HUD_CASH")
			ENDIF
			SET_BIT(g_TransitionSessionNonResetVars.iAirlockHUDBitset, ciFMMCAirlockHUDBS_TakeShown)
			IF IS_BIT_SET(g_TransitionSessionNonResetVars.iAirlockHUDBitset, ciFMMCAirlockHUDBS_ShowTake)
				g_TransitionSessionNonResetVars.iTotalCashGrabTakeForHUD = MC_ServerBD.iCashGrabTotalTake
			ENDIF
		ELSE
			//Lukasz: for spectating players, set the flag if not set once some money is collected, see B*2133931
			IF IS_PLAYER_SPECTATING(LocalPlayer)
				IF MC_ServerBD.iCashGrabTotalTake > 0
					g_TransitionSessionNonResetVars.bDisplayCashGrabTake = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DRAW_COKE_GRAB_HUD()
	
	INT iClientStage = GET_MC_CLIENT_MISSION_STAGE(iPartToUse)
	IF iClientStage = CLIENT_MISSION_STAGE_CASH_GRAB
	OR iClientStage = CLIENT_MISSION_STAGE_GOTO_OBJ
		g_TransitionSessionNonResetVars.bDrawCokeGrabHud = TRUE
	ENDIF
	
	INT iNumTrolleys = 4
	IF IS_THIS_ROCKSTAR_MISSION_SVM_PHANTOM2(g_FMMC_STRUCT.iRootContentIDHash)
		INT iNumParts = NETWORK_GET_NUM_PARTICIPANTS()
		IF iNumParts = 3
			iNumTrolleys = 3
		ENDIF
		iMaxCashLocalPlayerCanCarry = (iNumTrolleys * 450000) / NETWORK_GET_NUM_PARTICIPANTS()
		PRINTLN("[KH] DRAW_COKE_GRAB_HUD - iMaxCashLocalPlayerCanCarry: ", iMaxCashLocalPlayerCanCarry)
	ENDIF
	
	IF NOT g_bMissionEnding
	AND g_TransitionSessionNonResetVars.bDrawCokeGrabHud
		DRAW_GENERIC_METER(MC_ServerBD.iCashGrabTotalTake, (450000 * iNumTrolleys), "DRUG_GRAB", DEFAULT, DEFAULT, HUDORDER_TOP, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT)
		DRAW_GENERIC_METER(g_TransitionSessionNonResetVars.iLocalPlayerCashGrabbed, iMaxCashLocalPlayerCanCarry, "DRUG_GRAB_B", DEFAULT, DEFAULT, HUDORDER_TOP, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT)
	ENDIF
	
ENDPROC

///PURPOSE: This function handles all of the functionality to do with dropping money
PROC MANAGE_CASH_GRAB_TAKE_AND_DROP(BOOL bHidingHud = FALSE)
	
	IF NOT IS_CUTSCENE_PLAYING()

		IF MC_ServerBD.iCashGrabTotalTake > 0
			DROP_MONEY_IF_HIT_EVERY( 600, bShouldProcessCokeGrabHud )
		ENDIF
		
		//display grabbed and dropped money on the hud
		IF IS_THIS_ROCKSTAR_MISSION_SVM_PHANTOM2(g_FMMC_STRUCT.iRootContentIDHash)
			IF bShouldProcessCokeGrabHud
				DRAW_COKE_GRAB_HUD()
			ELSE
				//IF IS_PLAYER_SPECTATING(LocalPlayer)
					bShouldProcessCokeGrabHud = TRUE
				//ENDIF
			ENDIF
		ELSE
			IF NOT bHidingHud
				DRAW_CASH_GRAB_TAKE_HUD()
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC


CONST_INT CASHGRAB_BITSET_REMOTE_BAG_COMPONENT_REMOVED 	1
CONST_INT CASHGRAB_BITSET_REMOTE_BAG_OBJECT_REVEALED	2	
CONST_INT CASHGRAB_BITSET_REMOTE_BAG_OBJECT_HIDDEN		3
CONST_INT CASHGRAB_BITSET_REMOTE_BAG_COMPONENT_RESTORED	4
CONST_INT CASHGRAB_BITSET_REMOTE_CASH_PILE_VISIBLE		5
CONST_INT CASHGRAB_BITSET_REMOTE_CASH_PILE_INVISIBLE	6

INT iCashGrabBitset[MAX_NUM_MC_PLAYERS]
INT iCashGrabTimer[MAX_NUM_MC_PLAYERS]

///PURPOSE: B*2200165, run a check on all players on active cash grab rule if they are playing exit anim
///		if they are handle the prop bag and variation bag hiding and unhiding locally for yourself
///		by altering the remote player component hand variation and prop bag visibility locally
PROC PROCESS_REMOTE_PLAYER_CASH_GRAB_OBJECTS(PED_INDEX ped, INT iPart, INT iLocalSyncScene)
	
	//set the local visiblity of the cash pile prop in remote player's hand
	IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSyncScene)
		
		//set the visibility flags based on the animation events of the grab anim
		IF IS_ENTITY_PLAYING_ANIM(ped, "anim@heists@ornate_bank@grab_cash", "grab")
		OR IS_ENTITY_PLAYING_ANIM(ped, "anim@heists@ornate_bank@grab_cash_heels", "grab")
			IF HAS_ANIM_EVENT_FIRED(ped, GET_HASH_KEY("CASH_APPEAR"))
				SET_BIT(iCashGrabBitset[iPart], CASHGRAB_BITSET_REMOTE_CASH_PILE_VISIBLE)
				CLEAR_BIT(iCashGrabBitset[iPart], CASHGRAB_BITSET_REMOTE_CASH_PILE_INVISIBLE)
			ENDIF
			IF HAS_ANIM_EVENT_FIRED(ped, GET_HASH_KEY("RELEASE_CASH_DESTROY"))
				CLEAR_BIT(iCashGrabBitset[iPart], CASHGRAB_BITSET_REMOTE_CASH_PILE_VISIBLE)
				SET_BIT(iCashGrabBitset[iPart], CASHGRAB_BITSET_REMOTE_CASH_PILE_INVISIBLE)
			ENDIF	
		ELSE
			CLEAR_BIT(iCashGrabBitset[iPart], CASHGRAB_BITSET_REMOTE_CASH_PILE_VISIBLE)
			SET_BIT(iCashGrabBitset[iPart], CASHGRAB_BITSET_REMOTE_CASH_PILE_INVISIBLE)
		ENDIF
		
		
		BOOL bIsCashObject = TRUE
		INT iObj = MC_playerBD[iPart].iObjHacking
		PRINTLN("PROCESS_REMOTE_PLAYER_CASH_GRAB_OBJECTS - iObj: ", iObj)
		IF iObj > -1
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iObj])
				IF DOES_ENTITY_EXIST(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iObj]))
					IF IS_COKE_TROLLEY(GET_ENTITY_MODEL(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iObj])))
					OR IS_DIAMOND_TROLLEY(GET_ENTITY_MODEL(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iObj])))
					OR IS_GOLD_TROLLEY(GET_ENTITY_MODEL(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iObj])))
						bIsCashObject = FALSE
					ENDIF
				ENDIF
			ENDIF
			
			//grab the cash pile object and set the local visibility
			OBJECT_INDEX cashObj
			IF !bIsCashObject
				MODEL_NAMES mnGrabModel
				IF IS_DIAMOND_TROLLEY(GET_ENTITY_MODEL(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iObj])))
					mnGrabModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Vault_Dimaondbox_01a"))
				ELIF IS_GOLD_TROLLEY(GET_ENTITY_MODEL(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iObj])))
					mnGrabModel = CH_PROP_GOLD_BAR_01A
				ELSE
					mnGrabModel = INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_impexp_coke_pile"))
				ENDIF
				cashObj	= GET_CLOSEST_OBJECT_OF_TYPE(GET_ENTITY_COORDS(ped), 0.5, mnGrabModel, FALSE, FALSE, FALSE)
			ELSE
				cashObj	= GET_CLOSEST_OBJECT_OF_TYPE(GET_ENTITY_COORDS(ped), 0.5, HEI_PROP_HEIST_CASH_PILE, FALSE, FALSE, FALSE)
			ENDIF
		
		
			IF DOES_ENTITY_EXIST(cashObj)
				IF IS_BIT_SET(iCashGrabBitset[iPart], CASHGRAB_BITSET_REMOTE_CASH_PILE_VISIBLE)
					SET_ENTITY_LOCALLY_VISIBLE(cashObj)
				ELIF IS_BIT_SET(iCashGrabBitset[iPart], CASHGRAB_BITSET_REMOTE_CASH_PILE_INVISIBLE)
					SET_ENTITY_LOCALLY_INVISIBLE(cashObj)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//set the local visiblity of the bag during the intro animation
	IF NOT IS_BIT_SET(iCashGrabBitSet[iPart], CASHGRAB_BITSET_REMOTE_BAG_COMPONENT_REMOVED)
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSyncScene)
		OR GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_SYNCHRONIZED_SCENE) = PERFORMING_TASK
			OBJECT_INDEX bagObj = GET_CLOSEST_OBJECT_OF_TYPE(GET_ENTITY_COORDS(ped), 1.0, MC_playerBD_1[iPart].mnHeistGearBag, FALSE, FALSE, FALSE)
			
			IF DOES_ENTITY_EXIST(bagObj)
				SET_ENTITY_ALPHA(bagObj, 0, FALSE)
			ENDIF
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSyncScene)
				IF IS_ENTITY_PLAYING_ANIM(ped, "anim@heists@ornate_bank@grab_cash", "intro")
				OR IS_ENTITY_PLAYING_ANIM(ped, "anim@heists@ornate_bank@grab_cash_heels", "intro")
					IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSyncScene) >= 0.325
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAND, 0, 0) 	//hide the bag component variation
						SET_BIT(iCashGrabBitSet[iPart], CASHGRAB_BITSET_REMOTE_BAG_COMPONENT_REMOVED)
					ELSE
						APPLY_DUFFEL_BAG_HEIST_GEAR(ped, iPart) //Make sure the bag component remains.
					ENDIF
				ENDIF
			ENDIF
			
		ELSE
			CLEAR_BIT(iCashGrabBitSet[iPart], CASHGRAB_BITSET_REMOTE_BAG_COMPONENT_REMOVED)
			CLEAR_BIT(iCashGrabBitSet[iPart], CASHGRAB_BITSET_REMOTE_BAG_OBJECT_REVEALED)
		ENDIF
	ELIF NOT IS_BIT_SET(iCashGrabBitSet[iPart], CASHGRAB_BITSET_REMOTE_BAG_OBJECT_REVEALED)
		IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(ped)
			OBJECT_INDEX bagObj = GET_CLOSEST_OBJECT_OF_TYPE(GET_ENTITY_COORDS(ped), 0.5, MC_playerBD_1[iPart].mnHeistGearBag, FALSE, FALSE, FALSE)
			
			IF DOES_ENTITY_EXIST(bagObj)
				SET_ENTITY_ALPHA(bagObj, 255, FALSE)
				SET_ENTITY_LOCALLY_VISIBLE(bagObj)
			ENDIF
			
			SET_BIT(iCashGrabBitSet[iPart], CASHGRAB_BITSET_REMOTE_BAG_OBJECT_REVEALED)
		ENDIF
	ELSE
		IF 	NOT IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSyncScene)
		AND GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_SYNCHRONIZED_SCENE) != PERFORMING_TASK
		AND NOT IS_ENTITY_PLAYING_ANIM(ped, "anim@heists@ornate_bank@grab_cash", "intro")
		AND NOT IS_ENTITY_PLAYING_ANIM(ped, "anim@heists@ornate_bank@grab_cash", "grab_idle")
		AND NOT IS_ENTITY_PLAYING_ANIM(ped, "anim@heists@ornate_bank@grab_cash", "grab")
		AND NOT IS_ENTITY_PLAYING_ANIM(ped, "anim@heists@ornate_bank@grab_cash", "exit")
		AND NOT IS_ENTITY_PLAYING_ANIM(ped, "anim@heists@ornate_bank@grab_cash_heels", "intro")
		AND NOT IS_ENTITY_PLAYING_ANIM(ped, "anim@heists@ornate_bank@grab_cash_heels", "grab_idle")
		AND NOT IS_ENTITY_PLAYING_ANIM(ped, "anim@heists@ornate_bank@grab_cash_heels", "grab")
		AND NOT IS_ENTITY_PLAYING_ANIM(ped, "anim@heists@ornate_bank@grab_cash_heels", "exit")
			CLEAR_BIT(iCashGrabBitSet[iPart], CASHGRAB_BITSET_REMOTE_BAG_COMPONENT_REMOVED)
			CLEAR_BIT(iCashGrabBitSet[iPart], CASHGRAB_BITSET_REMOTE_BAG_OBJECT_REVEALED)
			CLEAR_BIT(iCashGrabBitset[iPart], CASHGRAB_BITSET_REMOTE_BAG_COMPONENT_RESTORED)
			CLEAR_BIT(iCashGrabBitset[iPart], CASHGRAB_BITSET_REMOTE_BAG_OBJECT_HIDDEN)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iCashGrabBitSet[iPart], CASHGRAB_BITSET_REMOTE_BAG_OBJECT_REVEALED)
		OBJECT_INDEX bagObj = GET_CLOSEST_OBJECT_OF_TYPE(GET_ENTITY_COORDS(ped), 0.5, MC_playerBD_1[iPart].mnHeistGearBag, FALSE, FALSE, FALSE)
		IF DOES_ENTITY_EXIST(bagObj)
			SET_ENTITY_LOCALLY_VISIBLE(bagObj)
		ENDIF
	ENDIF

	//set the local visiblity of the bag during the outro animation
	IF NOT IS_BIT_SET(iCashGrabBitset[iPart], CASHGRAB_BITSET_REMOTE_BAG_COMPONENT_RESTORED)
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSyncScene)
			//IF NOT IS_BIT_SET(iCashGrabBitset, CASHGRAB_BITSET_REMOTE_BAG_OBJECT_HIDDEN) 
				IF IS_ENTITY_PLAYING_ANIM(ped, "anim@heists@ornate_bank@grab_cash", "exit")
				OR IS_ENTITY_PLAYING_ANIM(ped, "anim@heists@ornate_bank@grab_cash_heels", "exit")
					IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSyncScene) >= 0.510//0.395 //0.6
						iCashGrabTimer[iPart] = GET_GAME_TIMER()
						APPLY_DUFFEL_BAG_HEIST_GEAR(ped, iPart)		 	//set the variation bag
						SET_BIT(iCashGrabBitset[iPart], CASHGRAB_BITSET_REMOTE_BAG_COMPONENT_RESTORED)
					ELSE
						SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAND, 0, 0) 	//hide the variation bag
					ENDIF
				ENDIF
			//ENDIF
		//ELSE
		//	CLEAR_BIT(iCashGrabBitset, CASHGRAB_BITSET_REMOTE_BAG_COMPONENT_RESTORED)
		//	CLEAR_BIT(iCashGrabBitset, CASHGRAB_BITSET_REMOTE_BAG_OBJECT_HIDDEN)
		ENDIF
	ELSE
		IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(ped)
		
			//grab remote player's bag prop object and hide it locally until it gets removed by the remote player
			OBJECT_INDEX bagObj = GET_CLOSEST_OBJECT_OF_TYPE(GET_ENTITY_COORDS(ped), 0.5, MC_playerBD_1[iPart].mnHeistGearBag, FALSE, FALSE, FALSE)

			INT iTimeDiff = GET_GAME_TIMER() - iCashGrabTimer[iPart]
			
			IF iTimeDiff > 500
				iTimeDiff = 500
			ENDIF
			
			INT iBagAlpha = ROUND(255.0 - ((TO_FLOAT(iTimeDiff) / 500.0) * 255.0))
			
			IF DOES_ENTITY_EXIST(bagObj)
				IF iTimeDiff >= 500
					SET_ENTITY_ALPHA(bagObj, 0, FALSE)
				ELSE
					SET_ENTITY_ALPHA(bagObj, iBagAlpha, FALSE)
				ENDIF
			ELSE
				CLEAR_BIT(iCashGrabBitset[iPart], CASHGRAB_BITSET_REMOTE_BAG_COMPONENT_RESTORED)
				//CLEAR_BIT(iCashGrabBitset, CASHGRAB_BITSET_REMOTE_BAG_OBJECT_HIDDEN)
			ENDIF
		ELSE
			iCashGrabTimer[iPart] = GET_GAME_TIMER()
		ENDIF
	ENDIF
	
ENDPROC



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: HACKING MINIGAME !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: HACKING SCREEN RENDERING !
//
//************************************************************************************************************************************************************



TWEAK_FLOAT		f_Hacking_Render_Target_x		0.101
TWEAK_FLOAT		f_Hacking_Render_Target_y		0.178
TWEAK_FLOAT		f_Hacking_Render_Target_width	0.216
TWEAK_FLOAT		f_Hacking_Render_Target_height	0.347

SCALEFORM_INDEX sfLaptop

INT rt_laptop_monitor
INT rt_default

//PURPOSE: Wipes the render target back to its plain black colour
PROC WIPE_RENDERTARGETS(BOOL bFullScreen)
	IF bFullScreen
		DRAW_RECT(0.5,0.5,1.0,1.0,0,0,0,255)
	ELSE	
		SET_TEXT_RENDER_ID(rt_laptop_monitor)
		DRAW_RECT(f_Hacking_Render_Target_x, f_Hacking_Render_Target_y, f_Hacking_Render_Target_width, f_Hacking_Render_Target_height, 0, 0, 0, 255)  
		SET_TEXT_RENDER_ID(rt_default)
	ENDIF
ENDPROC

//Requests laptop scaleform assets
PROC REQUEST_LAPTOP_SCALEFORM_ASSETS()
	sfLaptop = REQUEST_SCALEFORM_MOVIE("MPH_BruteForce_Laptop")
	PRINTLN("RC MISSION EQUEST_LAPTOP_SCALEFORM_ASSETS() scale form assets requested")
ENDPROC

//checks if laptop scaleform assets have loaded
FUNC BOOL HAVE_LAPTOP_SCALEFORM_ASSETS_LOADED()
	IF HAS_SCALEFORM_MOVIE_LOADED(sfLaptop)
			PRINTLN("RC MISSION HAVE_LAPTOP_SCALEFORM_ASSETS_LOADED() laptop scaleform assets loaded")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//rt_Tvscreen

// Setup the render target for the screen
PROC SETUP_LAPTOP_RENDERTARGET()
	IF NOT IS_NAMED_RENDERTARGET_REGISTERED("tvscreen")
		REGISTER_NAMED_RENDERTARGET("tvscreen")
	ENDIF
	IF NOT IS_NAMED_RENDERTARGET_LINKED(hei_Prop_Hst_Laptop)
		LINK_NAMED_RENDERTARGET(hei_Prop_Hst_Laptop)
	ENDIF
	rt_laptop_monitor		= GET_NAMED_RENDERTARGET_RENDER_ID("tvscreen")
	rt_default			= GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID()
ENDPROC

// Setup the render target for the screen
FUNC BOOL IS_LAPTOP_RENDERTARGET_SETUP()
	IF IS_NAMED_RENDERTARGET_REGISTERED("tvscreen")
	AND IS_NAMED_RENDERTARGET_LINKED(hei_Prop_Hst_Laptop)
		rt_laptop_monitor = GET_NAMED_RENDERTARGET_RENDER_ID("tvscreen")
		rt_default		  = GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: HACKING UI !
//
//************************************************************************************************************************************************************



//PURPOSE: Maintains the laptop screen
PROC MANAGE_LAPTOP_SCALEFORM(BOOL bWipeToBlack = FALSE)
	
	//TWEAK VALUES
	#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD4)
			f_Hacking_Render_Target_x =  f_Hacking_Render_Target_x - 0.001
			PRINTLN("RC MISSION f_Hacking_Render_Target_x =",f_Hacking_Render_Target_x)
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD6)
			f_Hacking_Render_Target_x =  f_Hacking_Render_Target_x + 0.001
			PRINTLN("RC MISSION f_Hacking_Render_Target_x =",f_Hacking_Render_Target_x)
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD7)
			f_Hacking_Render_Target_y =  f_Hacking_Render_Target_y - 0.001
			PRINTLN("RC MISSION f_Hacking_Render_Target_y =",f_Hacking_Render_Target_y)
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD9)
			f_Hacking_Render_Target_y =  f_Hacking_Render_Target_y + 0.001
			PRINTLN("RC MISSION f_Hacking_Render_Target_y =",f_Hacking_Render_Target_y)
		ENDIF
		
		//width - height
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_MINUS)
			f_Hacking_Render_Target_width =  f_Hacking_Render_Target_width - 0.001
			PRINTLN("RC MISSION f_Hacking_Render_Target_width =",f_Hacking_Render_Target_width)
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_PLUS)
			f_Hacking_Render_Target_width =  f_Hacking_Render_Target_width + 0.001
			PRINTLN("RC MISSION f_Hacking_Render_Target_width =",f_Hacking_Render_Target_width)
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD0)
			f_Hacking_Render_Target_height =  f_Hacking_Render_Target_height - 0.001
			PRINTLN("RC MISSION f_Hacking_Render_Target_heighth =",f_Hacking_Render_Target_height)
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_DELETE)
			f_Hacking_Render_Target_height =  f_Hacking_Render_Target_height + 0.001
			PRINTLN("RC MISSION f_Hacking_Render_Target_height =",f_Hacking_Render_Target_height)
		ENDIF
	#ENDIF


	IF HAVE_LAPTOP_SCALEFORM_ASSETS_LOADED()
		IF bWipeToBlack
			WIPE_RENDERTARGETS(FALSE)
		ELSE
			PRINTLN("RC MISSION MANAGE_LAPTOP_SCALEFORM - RENDERING")
			SET_TEXT_RENDER_ID(rt_laptop_monitor)
			DRAW_SCALEFORM_MOVIE(sfLaptop, f_Hacking_Render_Target_x, f_Hacking_Render_Target_y, f_Hacking_Render_Target_width, f_Hacking_Render_Target_height, 255, 255, 255, 255)
			SET_TEXT_RENDER_ID(rt_default)
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_HACKING_LAPTOP_SCALEFORM()
	IF MC_serverBD_1.iPlayerHacking != -1
		IF NOT IS_BIT_SET(MC_playerBD[MC_serverBD_1.iPlayerHacking].iClientBitSet2, PBBOOL2_HACKING_MINIGAME_COMPLETED)
		
			//IF iLocalPart != MC_serverBD_1.iPlayerForHackingMG							
				REQUEST_LAPTOP_SCALEFORM_ASSETS()
				IF HAVE_LAPTOP_SCALEFORM_ASSETS_LOADED()
					IF IS_LAPTOP_RENDERTARGET_SETUP()
						MANAGE_LAPTOP_SCALEFORM()
					ELSE
						PRINTLN("RC MISSION PROCESS_HACKING_LAPTOP_SCALEFORM() -  SETTING UP SCALEFORM ASSETS")
						SETUP_LAPTOP_RENDERTARGET()
					ENDIF
				ELSE
					PRINTLN("RC MISSION PROCESS_HACKING_LAPTOP_SCALEFORM() - WAITING ON SCALEFORM ASSETS")
				ENDIF
			//ENDIF
		ELSE
			PRINTLN("RC MISSION PROCESS_HACKING_LAPTOP_SCALEFORM() - PBBOOL2_HACKING_MINIGAME_COMPLETED set")
		ENDIF
	ELSE
		//PRINTLN("RC MISSION PROCESS_HACKING_LAPTOP_SCALEFORM() - MC_serverBD_1.iPlayerHacking != -1")
	ENDIF
	
ENDPROC

PROC FORCE_HACKING_HELP_BUTTONS(BOOL bThisAltHackingGame = FALSE)
	
	CLEAR_MENU_DATA()
	SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT)

	SET_MENU_ITEM_TOGGLEABLE(FALSE)
	
	LOAD_MENU_ASSETS()
	
	REMOVE_MENU_HELP_KEYS()
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		ADD_MENU_HELP_KEY_INPUT( INPUT_CURSOR_ACCEPT,		"HACK_SEL")	//select
	ELSE
		ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_ACCEPT,		"HACK_SEL")	//select
	ENDIF
	ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_CANCEL,		"HACK_BAC")	//back
	IF NOT bThisAltHackingGame
		ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_DPAD_LR,	"HACK_MOV")	//move mouse
	ENDIF
	
	PRINTLN("FORCE_HACKING_HELP_BUTTONS() - adding menu" )

ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: HACKING ASSETS & CREATION !
//
//************************************************************************************************************************************************************



FUNC BOOL SHOULD_USE_NEW_ANIMS()
	//fix for bug 2279153
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_USE_SWIPE_KEYCARD_ANIMS(OBJECT_INDEX objThisObj)
	IF GET_ENTITY_MODEL(objThisObj) = hei_prop_hei_bio_panel
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_USE_HACK_USING_LAPTOP_ANIMS(OBJECT_INDEX objThisObj)
	IF GET_ENTITY_MODEL(objThisObj) = hei_prop_hei_securitypanel 
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_LAPTOP_FOR_HACKING_ANIMS(BOOL bCreateVisible = TRUE)
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_playerBD[iPartToUse].netMiniGameObj2)
		REQUEST_MODEL(hei_Prop_Hst_Laptop)
		IF HAS_MODEL_LOADED(hei_Prop_Hst_Laptop)
			IF NOT IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_LAPTOP)
				MC_RESERVE_NETWORK_MISSION_OBJECTS(1)
				SET_BIT(iLocalBoolCheck12,LBOOL12_OBJ_RES_LAPTOP)
			ENDIF
			IF IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_LAPTOP)
				IF CAN_REGISTER_MISSION_OBJECTS(1)
					IF CREATE_NET_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2,hei_Prop_Hst_Laptop,GET_ENTITY_COORDS(LocalPlayerPed), bIsLocalPlayerHost)
						FREEZE_ENTITY_POSITION(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2), TRUE)
						SET_ENTITY_COLLISION(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2), FALSE)
						SET_ENTITY_INVINCIBLE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2), TRUE)
						SET_ENTITY_VISIBLE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2), bCreateVisible)
						SET_MODEL_AS_NO_LONGER_NEEDED(hei_Prop_Hst_Laptop)
						CLEAR_BIT(iLocalBoolCheck12, LBOOL12_OBJ_RES_LAPTOP)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL CREATE_FAKE_PHONE_FOR_HACKING_ANIMS(BOOL bCreateVisible = TRUE, MODEL_NAMES mnModel = Prop_Phone_ING_03, BOOL bAttach = TRUE)
	
	//Default model - Prop_Phone_ING_03
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_playerBD[iPartToUse].netMiniGameObj2)
		REQUEST_MODEL(mnModel)
		IF HAS_MODEL_LOADED(mnModel)
			IF NOT IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_MOBILE_PHONE)
				MC_RESERVE_NETWORK_MISSION_OBJECTS(1)
				SET_BIT(iLocalBoolCheck12,LBOOL12_OBJ_RES_MOBILE_PHONE)
			ENDIF
			IF IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_MOBILE_PHONE)
				IF CAN_REGISTER_MISSION_OBJECTS(1)
					IF CREATE_NET_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2, mnModel, GET_ENTITY_COORDS(LocalPlayerPed), bIsLocalPlayerHost)
						FREEZE_ENTITY_POSITION(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2), TRUE)
						SET_ENTITY_COLLISION(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2), FALSE)
						IF bAttach
							ATTACH_ENTITY_TO_ENTITY(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2),LocalPlayerPed,GET_PED_BONE_INDEX(LocalPlayerPed,BONETAG_PH_R_HAND),<<0,0,0>>,<<0,0,0>>)
						ENDIF
						SET_ENTITY_INVINCIBLE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2), TRUE)
						SET_ENTITY_VISIBLE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2), bCreateVisible)
						SET_MODEL_AS_NO_LONGER_NEEDED(mnModel)
						CLEAR_BIT(iLocalBoolCheck12, LBOOL12_OBJ_RES_MOBILE_PHONE)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL CREATE_HACKING_KEYCARD_OBJECT(BOOL bCreateVisible = TRUE, BOOL bAttach = TRUE, MODEL_NAMES mnModel = HEI_PROP_HEIST_CARD_HACK_02)
	
	//Default model - Prop_Heist_Card_Hack_02
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_playerBD[iPartToUse].netMiniGameObj)
		REQUEST_MODEL(mnModel)
		IF HAS_MODEL_LOADED(mnModel)
			IF NOT IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_KEYCARD)
				MC_RESERVE_NETWORK_MISSION_OBJECTS(1)
				SET_BIT(iLocalBoolCheck12,LBOOL12_OBJ_RES_KEYCARD)
			ENDIF
			IF IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_KEYCARD)
				IF CAN_REGISTER_MISSION_OBJECTS(1)
					IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_playerBD[iPartToUse].netMiniGameObj)
						IF CREATE_NET_OBJ(MC_playerBD[iPartToUse].netMiniGameObj, mnModel, GET_ENTITY_COORDS(LocalPlayerPed), bIsLocalPlayerHost)
							FREEZE_ENTITY_POSITION(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj), TRUE)
							SET_ENTITY_COLLISION(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj), FALSE)
							IF bAttach
								ATTACH_ENTITY_TO_ENTITY(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj),LocalPlayerPed,GET_PED_BONE_INDEX(LocalPlayerPed,BONETAG_PH_L_HAND),<<0,0,0>>,<<0,0,0>>)
							ENDIF
							SET_ENTITY_INVINCIBLE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj), TRUE)
							SET_MODEL_AS_NO_LONGER_NEEDED(mnModel)
							SET_ENTITY_VISIBLE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj), bCreateVisible)
							CLEAR_BIT(iLocalBoolCheck12,LBOOL12_OBJ_RES_KEYCARD)
							RETURN TRUE
						ENDIF
					ELSE
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC DESKTOP_WALLPAPER GET_HACKING_BACKGROUND(INT ibackground)

	SWITCH ibackground
		CASE ciFMMC_HACK_SCREEN_GENERIC
			RETURN WALL_GENERIC
		BREAK
		CASE  ciFMMC_HACK_SCREEN_FIB
			RETURN WALL_FBI
		BREAK
		CASE ciFMMC_HACK_SCREEN_BANK
			RETURN WALL_BANK		
		BREAK
		CASE ciFMMC_HACK_SCREEN_HUMANE
			RETURN WALL_HUMANE		
		BREAK
		CASE ciFMMC_HACK_SCREEN_GOV
			RETURN WALL_GOVERMENT		
		BREAK
		CASE ciFMMC_HACK_SCREEN_MERRYW
			RETURN WALL_MERRYWEATHER		
		BREAK
	ENDSWITCH
	
	RETURN WALL_GENERIC

ENDFUNC

FUNC STRING GET_DOOR_SOUND_FROM_MODEL_NAME(MODEL_NAMES model)

	SWITCH model
		
		CASE HEI_V_ILEV_BK_GATE2_PRIS
			RETURN "Metal_Bar_Door"
		BREAK
		CASE v_ilev_bk_vaultdoor
			RETURN "vault"
		BREAK
		CASE HEI_V_ILEV_BK_GATE_PRIS
			RETURN "Metal_Bar_Door"
		BREAK
		CASE prop_biolab_g_door
			RETURN  "Garage"
		BREAK
		CASE prop_gd_ch2_08
			RETURN "Garage"
		BREAK
		CASE v_ilev_ss_door01
			RETURN "Standard_Door"
		BREAK
		CASE prop_lrggate_02_ld
			RETURN "Metal_Gate_Large"
		BREAK
		CASE prop_facgate_01
			RETURN "ChainLink" 
		BREAK
		CASE prop_gate_airport_01
			RETURN "ChainLink"
		BREAK
		CASE v_ilev_gb_vauldr
			RETURN "vault"
		BREAK
		
	ENDSWITCH
	
	RETURN ""
	
ENDFUNC

FUNC STRING GET_BAG_ENTER_ANIM_NAME()
	
	STRING sThisString
	
	IF SHOULD_USE_ARMOUR_BAG_ANIMS()
		sThisString = "Hack_Enter_bag"
	ELSE
		IF SHOULD_USE_SUIT_BAG_ANIMS() 
			sThisString = "Hack_Enter_SUIT_bag"
		ELSE
			sThisString = "Hack_Enter_NO_ARMOUR_bag"
		ENDIF
	ENDIF

	PRINTLN("[AW_MISSION]GET_HACK_ENTER_ANIM_NAME -  ",sThisString)
	
	RETURN sThisString
	
ENDFUNC

FUNC STRING GET_BAG_LOOP_ANIM_NAME()
	
	STRING sThisString
	
	IF SHOULD_USE_ARMOUR_BAG_ANIMS()
		sThisString = "Hack_Loop_bag"
	ELSE
		IF SHOULD_USE_SUIT_BAG_ANIMS() 
			sThisString = "Hack_Loop_SUIT_bag"
		ELSE
			sThisString = "Hack_Loop_NO_ARMOUR_bag"
		ENDIF
	ENDIF

	PRINTLN("[AW_MISSION]GET_HACK_LOOP_ANIM_NAME -  ",sThisString)
	
	RETURN sThisString
	
ENDFUNC

FUNC STRING GET_BAG_EXIT_ANIM_NAME()
	
	STRING sThisString
	
	IF SHOULD_USE_ARMOUR_BAG_ANIMS()
		sThisString = "Hack_Exit_bag"
	ELSE
		IF SHOULD_USE_SUIT_BAG_ANIMS() 
			sThisString = "Hack_Exit_SUIT_bag"
		ELSE
			sThisString = "Hack_Exit_NO_ARMOUR_bag"
		ENDIF
	ENDIF

	PRINTLN("[AW_MISSION]GET_HACK_EXIT_ANIM_NAME -  ",sThisString)
	
	RETURN sThisString
	
ENDFUNC

FUNC STRING GET_LAPTOP_HACK_DICT()
	STRING sThisString
	IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
		sThisString = "anim@heists@ornate_bank@hack_heels"
	ELSE	
		sThisString = "anim@heists@ornate_bank@hack"
	ENDIF
	
	RETURN sThisString
	
ENDFUNC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: GENERAL HACKING FUNCTIONS !
//
//************************************************************************************************************************************************************



PROC CLEAN_UP_NEW_HACKING_GAME_SOUNDS()
	
	PRINTLN("[HACKING] CLEAN_UP_NEW_HACKING_GAME_SOUNDS()")
	IF IS_BIT_SET(iNewMiniGameSoundBitSet,0)
		IF iNewMiniGameSoundID[0] != -1
			IF NOT HAS_SOUND_FINISHED(iNewMiniGameSoundID[0])
				STOP_SOUND(iNewMiniGameSoundID[0])
				RELEASE_SOUND_ID(iNewMiniGameSoundID[0])
				PRINTLN("[HACKING] STOPPING SOUND iNewMiniGameSoundID[0]")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iNewMiniGameSoundBitSet,1)
		IF iNewMiniGameSoundID[1] != -1
			IF NOT HAS_SOUND_FINISHED(iNewMiniGameSoundID[1])
				STOP_SOUND(iNewMiniGameSoundID[1])
				RELEASE_SOUND_ID(iNewMiniGameSoundID[1])
				PRINTLN("[HACKING] STOPPING SOUND iNewMiniGameSoundID[1]")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iNewMiniGameSoundBitSet,2)
		IF iNewMiniGameSoundID[2] != -1
			IF NOT HAS_SOUND_FINISHED(iNewMiniGameSoundID[2])
				STOP_SOUND(iNewMiniGameSoundID[2])
				RELEASE_SOUND_ID(iNewMiniGameSoundID[2])
				PRINTLN("[HACKING] STOPPING SOUND iNewMiniGameSoundID[2]")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iNewMiniGameSoundBitSet,3)
		IF iNewMiniGameSoundID[3] != -1
			IF NOT HAS_SOUND_FINISHED(iNewMiniGameSoundID[3])
				STOP_SOUND(iNewMiniGameSoundID[3])
				RELEASE_SOUND_ID(iNewMiniGameSoundID[3])
				PRINTLN("[HACKING] STOPPING SOUND iNewMiniGameSoundID[3]")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iNewMiniGameSoundBitSet,4)
		IF iNewMiniGameSoundID[4] != -1
			IF NOT HAS_SOUND_FINISHED(iNewMiniGameSoundID[4])
				STOP_SOUND(iNewMiniGameSoundID[4])
				RELEASE_SOUND_ID(iNewMiniGameSoundID[4])
				PRINTLN("[HACKING] STOPPING SOUND iNewMiniGameSoundID[4]")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iNewMiniGameSoundBitSet,5)
		IF iNewMiniGameSoundID[5] != -1
			IF NOT HAS_SOUND_FINISHED(iNewMiniGameSoundID[5])
				STOP_SOUND(iNewMiniGameSoundID[5])
				RELEASE_SOUND_ID(iNewMiniGameSoundID[5])
				PRINTLN("[HACKING] STOPPING SOUND iNewMiniGameSoundID[5]")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iNewMiniGameSoundBitSet,6)
		IF iNewMiniGameSoundID[6] != -1
			IF NOT HAS_SOUND_FINISHED(iNewMiniGameSoundID[6])
				STOP_SOUND(iNewMiniGameSoundID[6])
				RELEASE_SOUND_ID(iNewMiniGameSoundID[6])
				PRINTLN("[HACKING] STOPPING SOUND iNewMiniGameSoundID[6]")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iNewMiniGameSoundBitSet,7)
		IF iNewMiniGameSoundID[7] != -1
			IF NOT HAS_SOUND_FINISHED(iNewMiniGameSoundID[7])
				STOP_SOUND(iNewMiniGameSoundID[7])
				RELEASE_SOUND_ID(iNewMiniGameSoundID[7])
				PRINTLN("[HACKING] STOPPING SOUND iNewMiniGameSoundID[7]")
			ENDIF
		ENDIF
	ENDIF

	
ENDPROC

PROC RUN_HACKING_CLEANUP(BOOL bBlockAudioRelease = FALSE)
	SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed,TRUE,FALSE)
	IF MC_playerBD[iPartToUse].iObjHacking !=-1
		IF IS_BIT_SET(MC_playerBD[iPartToUse].iHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
			CLEAR_BIT(MC_playerBD[iPartToUse].iHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
		ENDIF
		IF IS_BIT_SET(MC_playerBD[iPartToUse].iBeginHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
			CLEAR_BIT(MC_playerBD[iPartToUse].iBeginHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
		ENDIF
	ENDIF
	
	RESET_OBJ_HACKING_INT()
	
	IF NOT bBlockAudioRelease
		IF NOT IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
			RELEASE_SCRIPT_AUDIO_BANK()
			PRINTLN("RELEASE_SCRIPT_AUDIO_BANK - Called from fm_mission_controller_minigame.sch 4")
		ENDIF
	ELSE
		PRINTLN("RUN_HACKING_CLEANUP was called with bBlockAudioRelease = true")
	ENDIF
	CLEANUP_MENU_ASSETS()
	FORCE_QUIT_FAIL_HACKING_MINIGAME(sHackingData)
	RESET_HACKING_DATA(sHackingData, bBlockAudioRelease)
	IF NOT IS_PED_INJURED(LocalPlayerPed)
		CLEAR_PED_TASKS(LocalPlayerPed)
		TASK_CLEAR_LOOK_AT(LocalPlayerPed)
	ENDIF
	NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
	ENABLE_INTERACTION_MENU()
	CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Force fail and clean up hacking minigame due to player death.  ",iPartToUse)
	iHackLimitTimer = 0
	iHackProgress = 0
ENDPROC

/// PURPOSE:
///    Checks if any mission vehicle is potentially occupying a given position.
/// PARAMS:
///    vPos - The position to check.
///    fRadius - If the vehicle is within this radius around the position then they are considered occupying it.
FUNC BOOL IS_THE_MINIGAME_OBJECT_BLOCKED_BY_VEH(OBJECT_INDEX obj, FLOAT fRadius)
	VECTOR vObjPos = GET_ENTITY_COORDS(obj)
	VECTOR vObjOffset1 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(obj, <<-0.3, 0.0, 0.0>>) + <<0.0, 0.0, 2.0>>
	VECTOR vObjOffset2 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(obj, <<-0.3, -1.25, 0.0>>) + <<0.0, 0.0, -1.5>>
	
	INT iVeh
						
	FOR iVeh = 0 TO (MC_serverBD.iNumVehCreated - 1)
		IF(NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]))
			VEHICLE_INDEX tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
			VECTOR vVehPos = GET_ENTITY_COORDS(tempVeh)
			FLOAT fDistFromPos = VDIST(vVehPos, vObjPos)
		
			IF fDistFromPos < fRadius
			AND fDistFromPos < VDIST(GET_ENTITY_COORDS(LocalPlayerPed), vObjPos)
			AND IS_ENTITY_IN_ANGLED_AREA(tempVeh, vObjOffset1, vObjOffset2, 0.75)
				PRINTLN("[HACKING] [RCC MISSION] [IS_THE_MINIGAME_OBJECT_BLOCKED_BY_VEH] Vehicle ", iveh, " is blocking position ", vObjPos, " distance = ", fDistFromPos)
					
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_OBJECT_A_LAPTOP(OBJECT_INDEX objThisObj)
	IF GET_ENTITY_MODEL(objThisObj) = prop_laptop_01a
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Variables here moved to FM_Mission_Controller_USING header - search for b_cibsLEGACY_FMMC_MINI_GAME_IGNORE_CONNECT

PROC PROCESS_HACKING_MINI_GAME(OBJECT_INDEX tempObj,INT iobj, BOOL bAltHackingGame = FALSE)
	
	#IF IS_DEBUG_BUILD
		IF iHackProgress > 0
			IF SHOULD_USE_HACK_USING_LAPTOP_ANIMS(tempObj)
				IF SHOULD_USE_ARMOUR_BAG_ANIMS()
					PRINTLN("[RCC MISSION][HACK ANIMS] PLAYER WEARING ARMOUR")
				ENDIF
				
				IF SHOULD_USE_SUIT_BAG_ANIMS() 
					PRINTLN("[RCC MISSION][HACK ANIMS] PLAYER WEARING SUIT")
				ENDIF	
				
				IF NOT SHOULD_USE_ARMOUR_BAG_ANIMS()
				AND NOT SHOULD_USE_SUIT_BAG_ANIMS() 
					PRINTLN("[RCC MISSION][HACK ANIMS] PLAYER WEARING NORMAL")
				ENDIF
				
				IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
					PRINTLN("[RCC MISSION][HACK ANIMS] PLAYER WEARING HEELS")
				ENDIF
			ENDIF
		ENDIF
	#ENDIF
	
//For the hacking mini game. 
INT istartLives,iMaxLives,iSpeed,iDecay,iScreenX, iScreenY,iLocalHackLimitTimer
BOOL bJustBrute,bJustConnect
BOOL bDownAndOut = TRUE

VECTOR vObjRot

VECTOR vTemp1 
VECTOR vTemp2

//VECTOR vTempPosition
//VECTOR vTempRotation

	IF g_FMMC_STRUCT.iRootContentIDHash = g_sMPTUNABLES.iroot_id_HASH_The_Pacific_Standard_Job
	//OR bFakeOnMission
		PRINTLN("[RCC MISSION] On the Pacific Standard Job")
	ENDIF
	
	//Check to see if bits are being correctly set in release
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_DIFF_EASY)
		b_cibsLEGACY_FMMC_MINI_GAME_DIFF_EASY = TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_DIFF_HARD)
		b_cibsLEGACY_FMMC_MINI_GAME_DIFF_HARD = TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_IGNORE_BRUTE)
		b_cibsLEGACY_FMMC_MINI_GAME_IGNORE_BRUTE = TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_IGNORE_CONNECT)
		b_cibsLEGACY_FMMC_MINI_GAME_IGNORE_CONNECT = TRUE
	ENDIF
	
	IF MC_playerBD[iPartToUse].iObjNear = -1
		IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed,tempObj) <= 3.0
			SET_OBJ_NEAR(iObj)
			//PRINTLN("[RCC MISSION]  PROCESS_SYNC_LOCK_MINI_GAME: MC_playerBD[iPartToUse].iObjNear ",MC_playerBD[iPartToUse].iObjNear)
		ENDIF
	ELSE
		//PRINTLN("[RCC MISSION]  PROCESS_SYNC_LOCK_MINI_GAME: iObjNear has been assigned ")
	ENDIF

	IF b_cibsLEGACY_FMMC_MINI_GAME_IGNORE_BRUTE
	AND b_cibsLEGACY_FMMC_MINI_GAME_IGNORE_CONNECT
	AND b_cibsLEGACY_FMMC_MINI_GAME_DIFF_EASY
	AND b_cibsLEGACY_FMMC_MINI_GAME_DIFF_HARD
	
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(vHackOffset)
		
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF NOT bSetUpHackWidgets
			//CREATE_MISSION_CONTROLER_WIDGETS()
			bSetUpHackWidgets = TRUE
		ENDIF
	#ENDIF
	
	IF iHackProgress > 0
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
		ENDIF
	ENDIF
	
	//prop_laptop_01a
	
	IF MC_playerBD[iPartToUse].iObjHacked = -1
		IF MC_serverBD.iObjHackPart[iobj] = -1
			IF MC_playerBD[iPartToUse].iObjHacking = -1
				IF bPlayerToUseOK
					IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed,tempObj) < 2.0
						IF SHOULD_USE_SWIPE_KEYCARD_ANIMS(tempObj)
							//update
							IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iRequestHackAnimBitset,iobj)
								REQUEST_ANIM_DICT("anim@heists@humane_labs@emp@hack_door")
								SET_BIT(MC_playerBD[iPartToUse].iRequestHackAnimBitset,iobj)
							ENDIF
							vGoToPointOffset = vHackLaptopGotoOffset
						ELSE
							IF IS_OBJECT_A_LAPTOP(tempObj)
								vGoToPointOffset = vHackLaptopGotoOffset
							ELSE
								vGoToPointOffset = vHackKeypadGotoOffset
							ENDIF
						ENDIF
						
						VECTOR vAACoordOb 
						VECTOR vAACoordPro
						vAACoordOb = GET_ENTITY_COORDS(tempObj)
						vAACoordOb.z = vAACoordOb.z + fThisTweakValue1
						
						vAACoordPro = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj,vHackAAOffset)
						vAACoordPro.z = vAACoordPro.z - fThisTweakValue2
						
						//DRAW_DEBUG_ANGLED_AREA(vAACoordOb, vAACoordPro,1.000000,255,0,0,100)
						
						//vGoToPoint = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj,vGoToPointOffset)
						
						DRAW_DEBUG_CROSS(vGoToPoint,0.5,0,0,255,255)
						
						IF IS_ENTITY_IN_ANGLED_AREA( LocalPlayerPed, vAACoordOb, vAACoordPro, 1.200000)
							IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
								IF NOT IS_PHONE_ONSCREEN()
									BOOL IsMiniGameBlocked
						
									IsMiniGameBlocked = IS_THE_MINIGAME_OBJECT_BLOCKED_BY_VEH(tempObj,1.0)
									
									IF NOT IsMiniGameBlocked
										DISPLAY_HELP_TEXT_THIS_FRAME_FOR_MINIGAME("MC_HACK_1",TRUE)
									ENDIF
									IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
										IF NOT IsMiniGameBlocked
											MC_playerBD[iPartToUse].iObjHacking = iobj
											CLEAR_BIT(MC_playerBD[iPartToUse].iHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
											CLEAR_BIT(MC_playerBD[iPartToUse].iBeginHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
											RESET_NET_TIMER(tdHackTimer)
											iHackLimitTimer = 0
											iHackProgress = 0
											
											IF SHOULD_USE_SWIPE_KEYCARD_ANIMS(tempObj)
												IF HAS_ANIM_DICT_LOADED("anim@heists@humane_labs@emp@hack_door")
													FLOAT fGroundZTemp
													SET_PED_USING_ACTION_MODE(LocalPlayerPed,FALSE)
													fObjRot = GET_ENTITY_HEADING(tempObj)
													vGoToPoint = GET_ANIM_INITIAL_OFFSET_POSITION("anim@heists@humane_labs@emp@hack_door", "hack_intro", GET_ENTITY_COORDS(tempObj), <<0.0, 0.0, fObjRot+ 100>>)
													GET_GROUND_Z_FOR_3D_COORD(vGoToPoint + <<0.0, 0.0, 0.5>>, fGroundZTemp)
													vGoToPoint.z = fGroundZTemp
													
													SEQUENCE_INDEX ThisSequenceIndex				
													CLEAR_SEQUENCE_TASK(ThisSequenceIndex)
													OPEN_SEQUENCE_TASK(ThisSequenceIndex)
														TASK_GO_STRAIGHT_TO_COORD(NULL, vGoToPoint, PEDMOVE_WALK,  DEFAULT_TIME_BEFORE_WARP , fObjRot)
														TASK_LOOK_AT_ENTITY(NULL,tempObj,5000)
													CLOSE_SEQUENCE_TASK(ThisSequenceIndex)
													TASK_PERFORM_SEQUENCE(LocalPlayerPed, ThisSequenceIndex)
													CLEAR_SEQUENCE_TASK(ThisSequenceIndex)
													PRINTLN("[RCC MISSION] Dictionary loaded in time - assigning go to")
												ENDIF
											ENDIF
											
											PRINTLN("[RCC MISSION] local player has started hacking, part: ",iPartToUse)
										ENDIF
									ENDIF
								ELSE
									IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MC_HACK_1")
										CLEAR_HELP()
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF SHOULD_USE_SWIPE_KEYCARD_ANIMS(tempObj)
							IF IS_BIT_SET(MC_playerBD[iPartToUse].iRequestHackAnimBitset,iobj)
								IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed,tempObj) > 3.0
									REMOVE_ANIM_DICT("anim@heists@humane_labs@emp@hack_door")
									CLEAR_BIT(MC_playerBD[iPartToUse].iRequestHackAnimBitset,iobj)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(MC_playerBD[iPartToUse].iHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
					IF bAltHackingGame = TRUE
						PLAY_SOUND_FRONTEND(-1,"Hack_Failed" ,"DLC_HEIST_BIOLAB_PREP_HACKING_SOUNDS", FALSE)
					ENDIF
					IF IS_AUDIO_SCENE_ACTIVE("DLC_HEIST_PACIFIC_BANK_HACK_PASSWORD_SCENE")
						STOP_AUDIO_SCENE("DLC_HEIST_PACIFIC_BANK_HACK_PASSWORD_SCENE" )
					ENDIF
					TASK_CLEAR_LOOK_AT(LocalPlayerPed)
					//SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed,TRUE,FALSE)
					CLEAR_BIT(MC_playerBD[iPartToUse].iHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
					CLEAR_BIT(MC_playerBD[iPartToUse].iBeginHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
					RESET_OBJ_HACKING_INT()

					IF NOT IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
						RELEASE_SCRIPT_AUDIO_BANK()
						PRINTLN("RELEASE_SCRIPT_AUDIO_BANK - Called from fm_mission_controller_minigame.sch 5")
					ENDIF

					CLEANUP_MENU_ASSETS()
					FORCE_QUIT_FAIL_HACKING_MINIGAME(sHackingData)
					RESET_HACKING_DATA(sHackingData)
					CLEAR_PED_TASKS(LocalPlayerPed)
					NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
					ENABLE_INTERACTION_MENU()
					PRINTLN("[RCC MISSION]  local player has failed hacking, part: ",iPartToUse)
					
					iHackProgress++
					iHackLimitTimer = 0
					iHackProgress = 0
				ENDIF
			ENDIF
		ELSE
			
			IF MC_playerBD[iPartToUse].iObjHacking != -1
			
				//check for hacking minigame being interrupted, fix for B*2118175
				IF (NOT bPlayerToUseOK) OR g_bMissionEnding
					IF IS_BIT_SET(MC_playerBD[iPartToUse].iHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
						IF bAltHackingGame = TRUE
							PLAY_SOUND_FRONTEND(-1,"Hack_Failed" ,"DLC_HEIST_BIOLAB_PREP_HACKING_SOUNDS", FALSE)
						ENDIF
						
						IF SHOULD_USE_HACK_USING_LAPTOP_ANIMS(tempObj)
							DELETE_LAPTOP_OBJECT()
							DELETE_HACK_BAG_OBJECT()
							DELETE_HACKING_KEYCARD_OBJECT()
							APPLY_DUFFEL_BAG_HEIST_GEAR(LocalPlayerPed, iLocalPart)
						ENDIF
						
						CLEAN_UP_MINIGAME_OBJECTS()
						
						IF IS_AUDIO_SCENE_ACTIVE("DLC_HEIST_PACIFIC_BANK_HACK_PASSWORD_SCENE")
							STOP_AUDIO_SCENE("DLC_HEIST_PACIFIC_BANK_HACK_PASSWORD_SCENE" )
						ENDIF
						
						IF bPlayerToUseOK
							CLEAR_PED_TASKS(LocalPlayerPed)
							TASK_CLEAR_LOOK_AT(LocalPlayerPed)
						ENDIF				
						
						CLEAR_BIT(MC_playerBD[iPartToUse].iHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
						CLEAR_BIT(MC_playerBD[iPartToUse].iBeginHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
						RESET_OBJ_HACKING_INT()

						IF NOT IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
							RELEASE_SCRIPT_AUDIO_BANK()
							PRINTLN("RELEASE_SCRIPT_AUDIO_BANK - Called from fm_mission_controller_minigame.sch 6")
						ENDIF

						CLEANUP_MENU_ASSETS()
						FORCE_QUIT_FAIL_HACKING_MINIGAME(sHackingData)
						RESET_HACKING_DATA(sHackingData)
						
						NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
						ENABLE_INTERACTION_MENU()
						PRINTLN("[RCC MISSION] Local player has failed hacking minigame due to death, part: ", iPartToUse, ". Running fail cleanup.")
						iHackProgress++
						iHackLimitTimer = 0
						iHackProgress = 0
					ENDIF
				ENDIF
				
				IF MC_serverBD.iObjHackPart[iobj] = iPartToUse
				
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_DIFF_EASY)			
						istartLives =5
						iMaxLives = 5
						iSpeed = 25
						iDecay = 12
					ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_DIFF_HARD)
						istartLives =3
						iMaxLives = 3
						iSpeed = 90
						iDecay = 1
					ELSE
						istartLives =4
						iMaxLives = 4
						iSpeed = 50
						iDecay = 7
					ENDIF
					
					IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
						iWallPaper = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iHackScreen[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]]
						PRINTLN("[RCC MISSION] [AW_HACKING] iWallPaper: ", iWallPaper)
					ENDIF
				
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet, cibsLEGACY_FMMC_MINI_GAME_IGNORE_CONNECT)
						bJustBrute = TRUE
						bJustConnect = FALSE
						bDownAndOut = FALSE
						b_cibsLEGACY_FMMC_MINI_GAME_IGNORE_CONNECT = TRUE 
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet, cibsLEGACY_FMMC_MINI_GAME_IGNORE_BRUTE)
						bJustConnect = TRUE
						bJustBrute = FALSE
						bDownAndOut = FALSE
						b_cibsLEGACY_FMMC_MINI_GAME_IGNORE_BRUTE = TRUE
					ENDIF
						
					FLOAT fGroundZTemp
					
					IF iHackProgress > 0
						DRAW_DEBUG_CROSS(vGoToPoint,0.5,0,0,255,255)
					ENDIF

					SWITCH iHackProgress
						CASE 0
							IF DOES_ENTITY_EXIST(tempObj)
							
								IF bAltHackingGame //3988432
									//Unlock columns
									sColumnData[0].bRowLocked = FALSE
									sColumnData[1].bRowLocked = FALSE
									sColumnData[2].bRowLocked = FALSE
									sColumnData[3].bRowLocked = FALSE
									sColumnData[4].bRowLocked = FALSE
									sColumnData[5].bRowLocked = FALSE
									sColumnData[6].bRowLocked = FALSE
									sColumnData[7].bRowLocked = FALSE
									CLEAR_BIT(sHackingData.bsHacking, BS_NEW_HACKING_GAME_SOLVED)
									PRINTLN("[TMS] Resetting progress for 'lockpicking' minigame")
								ENDIF
								
								IF SHOULD_USE_SWIPE_KEYCARD_ANIMS(tempObj)
									vGoToPointOffset = vHackKeycardGotoOffset
								ELSE
									IF IS_OBJECT_A_LAPTOP(tempObj)
										vGoToPointOffset = vHackLaptopGotoOffset
									ELSE
										vGoToPointOffset = vHackKeypadGotoOffset
									ENDIF
								ENDIF

								vGoToPoint = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj,vGoToPointOffset)
								
								GET_GROUND_Z_FOR_3D_COORD(vGoToPoint,fGroundZTemp)
								vGoToPoint.z = fGroundZTemp 
								
								IF IS_PLAYSTATION_PLATFORM()
								OR IS_XBOX_PLATFORM()
									sKeyPadAnim = "mp_heists@keypad@"
								ENDIF
								
								IF SHOULD_USE_HACK_USING_LAPTOP_ANIMS(tempObj)
									//2006221
									sLaptopHackDict = GET_LAPTOP_HACK_DICT()
									REQUEST_MODEL(hei_Prop_Hst_Laptop)
									REQUEST_ANIM_DICT(sLaptopHackDict)
								ELSE
									IF SHOULD_USE_SWIPE_KEYCARD_ANIMS(tempObj)
										//1959134
										REQUEST_MODEL(HEI_PROP_HEI_ID_BIO)
										REQUEST_MODEL(PROP_PHONE_ING_03)
										REQUEST_ANIM_DICT("anim@heists@humane_labs@emp@hack_door")
									ELSE
										IF IS_OBJECT_A_LAPTOP(tempObj)
											REQUEST_ANIM_DICT("missheist_jewel@hacking")
										ELSE
											IF SHOULD_USE_NEW_ANIMS()
												REQUEST_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_USE_KEYPAD")
												REQUEST_ANIM_DICT(sKeyPadAnim)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								
								PRINTLN("[RCC MISSION] REQUEST_HACKING_MINI_GAME: GET_ENTITY_ROTATION: ", vobjRot)
								
								//sHackingData.b_wallpaper_initialised = FALSE
								CLEAR_BIT(sHackingData.bsHacking,BS_WALLPAPER_INITIALISED)
								IF MC_serverBD_1.iPlayerHacking > -1 AND MC_serverBD_1.iPlayerHacking < MAX_NUM_MC_PLAYERS
								CLEAR_BIT(MC_playerBD[ MC_serverBD_1.iPlayerHacking ].iClientBitSet2, PBBOOL2_HACKING_MINIGAME_COMPLETED)
								ENDIF
								MC_serverBD_1.iPlayerHacking = -1
								REQUEST_HACKING_MINI_GAME()
								CLEANUP_MENU_ASSETS()
								PRINTLN("[RCC MISSION] REQUEST_HACKING_MINI_GAME")
								IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
									NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_ALLOW_PLAYER_DAMAGE|NSPC_PREVENT_EVERYBODY_BACKOFF|NSPC_LEAVE_CAMERA_CONTROL_ON|NSPC_ALLOW_PAD_SHAKE|NSPC_REENABLE_CONTROL_ON_DEATH)
								ELSE
									NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_PREVENT_EVERYBODY_BACKOFF|NSPC_LEAVE_CAMERA_CONTROL_ON|NSPC_ALLOW_PAD_SHAKE|NSPC_REENABLE_CONTROL_ON_DEATH)
								ENDIF

								REINIT_NET_TIMER(tdHackTimer)
								PRINTLN("[RCC MISSION] SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)")
								//SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
								SET_PED_PROOFS_FOR_MINIGAME(LocalPlayerPed) 
								PRINTLN("[RCC MISSION] hacking details: iStartLives = ", istartLives,",iMaxLives = ",iMaxLives,",iSpeed = ",iSpeed ,",iDecay = ",iDecay)
								iHackProgress++
							ENDIF
						BREAK
						
						CASE 1
						
							
							VECTOR vPlayerCord
						
							IF HAVE_HACKING_ASSETS_LOADED()
							AND LOAD_MENU_ASSETS()	

								IF bAltHackingGame
									sHackingData.bTestHacking = TRUE
								ENDIF
								
								IF bLocalPlayerPedOk
									CLEAR_PED_TASKS(LocalPlayerPed)
									
									//SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed,FALSE,FALSE)
									vobjRot = GET_ENTITY_ROTATION(tempObj)
									NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_ALLOW_PLAYER_DAMAGE|NSPC_PREVENT_EVERYBODY_BACKOFF|NSPC_LEAVE_CAMERA_CONTROL_ON|NSPC_ALLOW_PAD_SHAKE|NSPC_REENABLE_CONTROL_ON_DEATH)
									IF GET_SCRIPT_TASK_STATUS(LocalPlayerPed,SCRIPT_TASK_PERFORM_SEQUENCE)<> PERFORMING_TASK
										SEQUENCE_INDEX ThisSequenceIndex				
										CLEAR_SEQUENCE_TASK(ThisSequenceIndex)
										OPEN_SEQUENCE_TASK(ThisSequenceIndex)
											vPlayerCord = GET_ENTITY_COORDS(LocalPlayerPed)
											vPlayerCord.z = vGoToPoint.z
											
											IF SHOULD_USE_HACK_USING_LAPTOP_ANIMS(tempObj)
												SET_PED_USING_ACTION_MODE(LocalPlayerPed,FALSE)
												vPlayAnimAdvancedPlayback = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj,<<0,0.07,0>>)
												fObjRot = GET_ENTITY_HEADING(tempObj)
												vGoToPoint = GET_ANIM_INITIAL_OFFSET_POSITION(sLaptopHackDict, "hack_enter", vPlayAnimAdvancedPlayback, <<0.0, 0.0, fObjRot>>)
												GET_GROUND_Z_FOR_3D_COORD(vGoToPoint + <<0.0, 0.0, 0.5>>, fGroundZTemp)
												vGoToPoint.z = fGroundZTemp
											ELIF SHOULD_USE_SWIPE_KEYCARD_ANIMS(tempObj)
												SET_PED_USING_ACTION_MODE(LocalPlayerPed,FALSE)
												fObjRot = GET_ENTITY_HEADING(tempObj)
												vGoToPoint = GET_ANIM_INITIAL_OFFSET_POSITION("anim@heists@humane_labs@emp@hack_door", "hack_intro", GET_ENTITY_COORDS(tempObj), <<0.0, 0.0, fObjRot+ 100>>)
												GET_GROUND_Z_FOR_3D_COORD(vGoToPoint + <<0.0, 0.0, 0.5>>, fGroundZTemp)
												vGoToPoint.z = fGroundZTemp
											ELIF IS_OBJECT_A_LAPTOP(tempObj)
												fObjRot = GET_ENTITY_HEADING(tempObj)
												vGoToPoint = GET_ANIM_INITIAL_OFFSET_POSITION("missheist_jewel@hacking", "hack_intro", GET_ENTITY_COORDS(tempObj), <<0.0, 0.0, fObjRot>>)
												GET_GROUND_Z_FOR_3D_COORD(vGoToPoint + <<0.0, 0.0, 0.5>>, fGroundZTemp)
												vGoToPoint.z = fGroundZTemp
											ELSE
												fObjRot = GET_ENTITY_HEADING(tempObj)
												vGoToPoint = GET_ANIM_INITIAL_OFFSET_POSITION(sKeyPadAnim, "enter", GET_ENTITY_COORDS(tempObj), <<0.0, 0.0, fObjRot>>)
												GET_GROUND_Z_FOR_3D_COORD(vGoToPoint + <<0.0, 0.0, 0.5>>, fGroundZTemp)
												vGoToPoint.z = fGroundZTemp
											ENDIF
											
											PRINTLN("[AW_MISSION] Distance between player and goto",GET_DISTANCE_BETWEEN_COORDS(vPlayerCord,vGoToPoint, FALSE))
											
											IF GET_DISTANCE_BETWEEN_COORDS(vPlayerCord,vGoToPoint, FALSE) > fUseGoToDistance
												//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vGoToPoint, PEDMOVE_WALK,  DEFAULT_TIME_BEFORE_WARP , 0.1, ENAV_STOP_EXACTLY, fObjRot)
												TASK_GO_STRAIGHT_TO_COORD(NULL, vGoToPoint, PEDMOVE_WALK,  DEFAULT_TIME_BEFORE_WARP , fObjRot)
												PRINTLN("[AW_MISSION] Too far away using goto ")
											ENDIF
											TASK_LOOK_AT_ENTITY(NULL,tempObj,5000)
										CLOSE_SEQUENCE_TASK(ThisSequenceIndex)
										TASK_PERFORM_SEQUENCE(LocalPlayerPed, ThisSequenceIndex)
										CLEAR_SEQUENCE_TASK(ThisSequenceIndex)
									ELSE
										PRINTLN("[AW_MISSION] Already performing a task sequence ")
									ENDIF
									
									vTemp1 = GET_ENTITY_COORDS(LocalPlayerPed) 
									vTemp2 = GET_ENTITY_COORDS(tempObj) 
									fObjRot = vobjRot.z
									
									IF IS_OBJECT_A_LAPTOP(tempObj)
										vHackLaptopScenePlayback.z = vTemp1.z - vTemp2.z
									ELSE
										vHackKeypadScenePlayback.z = vTemp1.z - vTemp2.z
									ENDIF
									
									PRINTLN("[AW_MISSION] iThisHackAnimProgress = 2 ")
									IF IS_OBJECT_A_LAPTOP(tempObj)
										PRINTLN("[AW_MISSION] vScenePlayback: ")PRINTLN(vHackLaptopScenePlayback)
									ELSE
										PRINTLN("[AW_MISSION] vScenePlayback: ")PRINTLN(vHackKeypadScenePlayback)
									ENDIF
									PRINTLN("[AW_MISSION] vGoTo: ")PRINTLN(vGoToPoint)
									PRINTLN("[AW_MISSION]Heading: ")PRINTLN(vobjRot.z)
									REQUEST_MISSION_AUDIO_BANK("Vault_Door")
									REINIT_NET_TIMER(tdHackTimer)
									iHackProgress++
								ENDIF

							ELSE
								PRINTLN("[RCC MISSION]  WAITING FOR HAVE_HACKING_ASSETS_LOADED")
							ENDIF
							
						BREAK

						
						CASE 2
							
							IF SHOULD_USE_HACK_USING_LAPTOP_ANIMS(tempObj)
								//2006221
								REQUEST_ANIM_DICT(sLaptopHackDict)
								IF HAS_ANIM_DICT_LOADED(sLaptopHackDict)
									iHackProgress++	
								ENDIF
							ELSE
								IF SHOULD_USE_SWIPE_KEYCARD_ANIMS(tempObj)
									//1959134
									REQUEST_ANIM_DICT("anim@heists@humane_labs@emp@hack_door")
									IF HAS_ANIM_DICT_LOADED("anim@heists@humane_labs@emp@hack_door")
										iHackProgress++	
									ENDIF
								ELSE
									IF IS_OBJECT_A_LAPTOP(tempObj)
										IF HAS_ANIM_DICT_LOADED("missheist_jewel@hacking")
											PRINTLN("[AW_MISSION] Laptop iHackProgress = 3 ")
											iHackProgress++
										ENDIF
									ELSE
										IF SHOULD_USE_NEW_ANIMS()
											IF HAS_ANIM_DICT_LOADED(sKeyPadAnim)
												PRINTLN("[AW_MISSION] Keypad iHackProgress = 3 ")
												vPlayAnimAdvancedPlayback = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj,vHackKeypadScenePlayback)
												PRINTLN("[AW_MISSION] vPlayAnimAdvancedPlayback: ")PRINTLN(vPlayAnimAdvancedPlayback)
												iHackProgress++
											ENDIF
										ELSE
											iHackProgress++
										ENDIF
									ENDIF
								ENDIF
							ENDIF
	
						BREAK
						
						CASE 3

							FLOAT fDistFromAnimStart, fHeadingDiffFromAnimStart
							VECTOR vInitialRot
							IF SHOULD_USE_HACK_USING_LAPTOP_ANIMS(tempObj)
								vInitialRot = GET_ANIM_INITIAL_OFFSET_POSITION(sLaptopHackDict, "hack_enter", vPlayAnimAdvancedPlayback, <<0.0, 0.0, fObjRot>>)
							ELIF SHOULD_USE_SWIPE_KEYCARD_ANIMS(tempObj)
								vInitialRot = GET_ANIM_INITIAL_OFFSET_POSITION("anim@heists@humane_labs@emp@hack_door", "hack_intro", GET_ENTITY_COORDS(tempObj), <<0.0, 0.0, fObjRot+ 100>>)
							ELIF IS_OBJECT_A_LAPTOP(tempObj)
								vInitialRot = GET_ANIM_INITIAL_OFFSET_POSITION("missheist_jewel@hacking", "hack_intro", GET_ENTITY_COORDS(tempObj), <<0.0, 0.0, fObjRot>>)
							ELSE
								vInitialRot = GET_ANIM_INITIAL_OFFSET_POSITION(sKeyPadAnim, "enter", GET_ENTITY_COORDS(tempObj), <<0.0, 0.0, fObjRot>>)
							ENDIF
						
							fDistFromAnimStart = VDIST(GET_ENTITY_COORDS(LocalPlayerPed, FALSE), vGoToPoint + <<0.0, 0.0, 1.0>>)
							fHeadingDiffFromAnimStart = ABSF(GET_ENTITY_HEADING(LocalPlayerPed) - vInitialRot.z)
							
							IF fHeadingDiffFromAnimStart > 180.0
								fHeadingDiffFromAnimStart -= 360.0
							ENDIF
							
							IF GET_SCRIPT_TASK_STATUS(LocalPlayerPed,SCRIPT_TASK_PERFORM_SEQUENCE)<> PERFORMING_TASK
							OR (fDistFromAnimStart < 0.2 AND ABSF(fHeadingDiffFromAnimStart) < 10.0)
							OR HAS_NET_TIMER_EXPIRED(tdHackTimer, 6000)
								TASK_LOOK_AT_ENTITY(LocalPlayerPed,tempObj,-1)
								
								ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_ACCEPT,		"HACK_SEL")	//select
								ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_CANCEL,		"HACK_BAC")	//back
								ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_DPAD_LR,	"HACK_MOV")	//move mouse
								
								IF SHOULD_USE_HACK_USING_LAPTOP_ANIMS(tempObj)
										IF CREATE_HACK_BAG_OBJECTS(TRUE)
											SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed,FALSE)
											vPlayAnimAdvancedPlayback = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj,<<0,0.07,0>>)
											PRINTLN("[AW_MISSION] iHackProgress = 4 ")
											iHackProgress++
										ENDIF
								ELSE
									IF SHOULD_USE_SWIPE_KEYCARD_ANIMS(tempObj)
											SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed,FALSE)
											//IF CREATE_HACKING_KEYCARD_OBJECT(FALSE)
											IF CREATE_FAKE_PHONE_FOR_HACKING_ANIMS(FALSE)
												vPlayAnimAdvancedPlayback = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj,vHackKeycardScenePlayback)
												PRINTLN("[AW_MISSION] iHackProgress = 4 ")
												iHackProgress++
											ENDIF
									ELSE
										IF NOT SHOULD_USE_NEW_ANIMS()
											IF NOT IS_OBJECT_A_LAPTOP(tempObj)
												IF bLocalPlayerPedOk
													TASK_START_SCENARIO_IN_PLACE(LocalPlayerPed,"WORLD_HUMAN_STAND_MOBILE",0,TRUE)
												ENDIF
											ENDIF
											PRINTLN("[AW_MISSION] iHackProgress = 5 ")
											iHackProgress = 5
										ELSE
											IF NOT IS_OBJECT_A_LAPTOP(tempObj)
												//vTempPosition = GET_ENTITY_COORDS(LocalPlayerPed)
												//vTempRotation = GET_ENTITY_ROTATION(LocalPlayerPed)
												PRINTLN("[AW_MISSION] iHackProgress = 4 ")
												iHackProgress++
											ELSE
												PRINTLN("[AW_MISSION]  Laptop iHackProgress = 5 ")
												iHackProgress = 5
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						BREAK
						
						CASE 4
							
							//IF SLIDE_TO_PLAYER_TO_COORD(vTempPosition,vPlayAnimAdvancedPlayback,vTempRotation,<<0,0,fObjRot>>,0.01)
								PRINTLN("[AW_MISSION] iHackProgress = 5 ")
								iHackProgress++
							//ENDIF
						
						BREAK
						
						CASE 5
							IF SHOULD_USE_HACK_USING_LAPTOP_ANIMS(tempObj)
								IF HAS_ANIM_DICT_LOADED("anim@heists@ornate_bank@hack")
								OR MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer,6000)
										IF CREATE_LAPTOP_FOR_HACKING_ANIMS(FALSE)
										AND CREATE_HACKING_KEYCARD_OBJECT(FALSE,FALSE)
										
											
											//The box is being treated as outside the interior, so force the right interior room here.
											INTERIOR_INSTANCE_INDEX interiorCurrent
											INT iCurrentRoom
											
											interiorCurrent = GET_INTERIOR_FROM_ENTITY(LocalPlayerPed)
											iCurrentRoom = GET_ROOM_KEY_FROM_ENTITY(LocalPlayerPed)
										
											IF interiorCurrent != NULL
											AND iCurrentRoom != 0
												FORCE_ROOM_FOR_ENTITY(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj), interiorCurrent, iCurrentRoom)
												FORCE_ROOM_FOR_ENTITY(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2), interiorCurrent, iCurrentRoom)
												FORCE_ROOM_FOR_ENTITY(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj3), interiorCurrent, iCurrentRoom)
											ENDIF
											
											//vPlayAnimAdvancedPlayback = GET_ENTITY_COORDS(tempObj)
											
											vPlayAnimAdvancedPlayback = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj,<<0,0.07,0>>)
											
											sBagAnim = GET_BAG_ENTER_ANIM_NAME()
											
											MC_playerBD[iPartToUse].iSynchSceneID  = NETWORK_CREATE_SYNCHRONISED_SCENE(vPlayAnimAdvancedPlayback, <<0,0, GET_ENTITY_HEADING(tempObj)>>, DEFAULT, TRUE)
											//MC_playerBD[iPartToUse].iSynchSceneID  = NETWORK_CREATE_SYNCHRONISED_SCENE(vPlayAnimAdvancedPlayback, <<0,0,0>>, DEFAULT, TRUE)
											NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(LocalPlayerPed, MC_playerBD[iPartToUse].iSynchSceneID, sLaptopHackDict, "hack_enter", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_HIDE_WEAPON | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON | SYNCED_SCENE_ABORT_ON_DEATH , RBF_PLAYER_IMPACT)
											NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj), MC_playerBD[iPartToUse].iSynchSceneID,
																		  			sLaptopHackDict, "hack_enter_card", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
											NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2), MC_playerBD[iPartToUse].iSynchSceneID,
																		  			sLaptopHackDict, "hack_enter_laptop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
											NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj3), MC_playerBD[iPartToUse].iSynchSceneID,
																		  			sLaptopHackDict, sBagAnim , NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
											NETWORK_START_SYNCHRONISED_SCENE(MC_playerBD[iPartToUse].iSynchSceneID )

											//SET_PED_USING_ACTION_MODE(LocalPlayerPed,FALSE)
											
											FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj3))

											iSafetyTimer = GET_GAME_TIMER()
											iHackProgress++
										ENDIF
								ENDIF
							ELSE
								IF SHOULD_USE_SWIPE_KEYCARD_ANIMS(tempObj)
									//1959134
									REQUEST_ANIM_DICT("anim@heists@humane_labs@emp@hack_door")
									IF HAS_ANIM_DICT_LOADED("anim@heists@humane_labs@emp@hack_door")
									OR MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer,6000)
										g_FMMC_STRUCT.bDisablePhoneInstructions = TRUE
										//vPlayAnimAdvancedPlayback
										vTemp1 = GET_ENTITY_ROTATION(tempObj)
										fObjRot = vTemp1.z 
										fObjRot = fObjRot + 100
										
										//TASK_PLAY_ANIM_ADVANCED(LocalPlayerPed,"anim@heists@humane_labs@emp@hack_door", "hack_intro",GET_ENTITY_COORDS(tempObj),<<0,0,fObjRot>>,SLOW_BLEND_IN,NORMAL_BLEND_OUT,-1, AF_EXTRACT_INITIAL_OFFSET| AF_HIDE_WEAPON | AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME )
										
										MC_playerBD[iPartToUse].iSynchSceneID  = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_ENTITY_COORDS(tempObj),<<0,0,fObjRot>>, DEFAULT, TRUE)
										NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(LocalPlayerPed, MC_playerBD[iPartToUse].iSynchSceneID, "anim@heists@humane_labs@emp@hack_door", "hack_intro", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_HIDE_WEAPON | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON | SYNCED_SCENE_ABORT_ON_DEATH , RBF_PLAYER_IMPACT, WALK_BLEND_IN)
										NETWORK_START_SYNCHRONISED_SCENE(MC_playerBD[iPartToUse].iSynchSceneID )
										
										PRINTLN("[AW_MISSION] iHackProgress = 6")
										iSafetyTimer = GET_GAME_TIMER()
										
										HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
										
										iHackProgress++
									ENDIF
								ELSE
									IF IS_OBJECT_A_LAPTOP(tempObj)
										REQUEST_ANIM_DICT("missheist_jewel@hacking")
										IF HAS_ANIM_DICT_LOADED("missheist_jewel@hacking")
											MC_playerBD[iPartToUse].iSynchSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(vHackLaptopScenePlayback, <<0,0,0>>, EULER_YXZ, TRUE)	
											NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(LocalPlayerPed, MC_playerBD[iPartToUse].iSynchSceneID, "missheist_jewel@hacking", "hack_intro", SLOW_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_HIDE_WEAPON| SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_ABORT_ON_DEATH, RBF_PLAYER_IMPACT)
											NETWORK_ATTACH_SYNCHRONISED_SCENE_TO_ENTITY(MC_playerBD[iPartToUse].iSynchSceneID,tempObj,-1)
											NETWORK_START_SYNCHRONISED_SCENE(MC_playerBD[iPartToUse].iSynchSceneID)
											PRINTLN("[AW_MISSION] Laptop iHackProgress = 6 ")
											iHackProgress++
										ENDIF
									ELSE
										IF fObjRot = 0.0
										
										ENDIF
										IF SHOULD_USE_NEW_ANIMS()
											REQUEST_ANIM_DICT(sKeyPadAnim )
											IF HAS_ANIM_DICT_LOADED(sKeyPadAnim )
												TASK_PLAY_ANIM_ADVANCED(LocalPlayerPed,sKeyPadAnim, "enter",vPlayAnimAdvancedPlayback,<<0,0,fObjRot>>,SLOW_BLEND_IN,NORMAL_BLEND_OUT,-1, AF_HIDE_WEAPON | AF_NOT_INTERRUPTABLE | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME | AF_USE_KINEMATIC_PHYSICS )
												PRINTLN("[AW_MISSION] iHackProgress = 6 ")
												iHackProgress++
											ENDIF
										ELSE
											PRINTLN("[AW_MISSION] iHackProgress = 6 ")
											PRINTLN("[AW_MISSION] No HP ")
											iHackProgress++
										ENDIF
										
									ENDIF
								ENDIF
							ENDIF
						
						BREAK
						
						CASE 6
							IF SHOULD_USE_HACK_USING_LAPTOP_ANIMS(tempObj)
								IF IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID))
								OR NOT (IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID))
								AND MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer,500))
									 PRINTLN("[AW_MISSION] Removing Component bag ")
									 PRINTLN("[AW_MISSION] iHackProgress = 7 ")
									 REMOVE_PLAYER_BAG()
									 iHackProgress++
								ENDIF
							ELSE
								iHackProgress++
							ENDIF
						BREAK
						
						CASE 7
							IF SHOULD_USE_HACK_USING_LAPTOP_ANIMS(tempObj)
								IF HAS_ANIM_DICT_LOADED(sLaptopHackDict)
									
										
										IF IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID))
										OR MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer,8000)
										
											//card
											IF GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID)) > 0.227
												IF NOT IS_ENTITY_VISIBLE_TO_SCRIPT(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj))
													SET_ENTITY_VISIBLE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj), TRUE)
												ENDIF
											ENDIF
											
											//laptop
											IF GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID)) > 0.714
												IF NOT IS_ENTITY_VISIBLE_TO_SCRIPT(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2))
													SET_ENTITY_VISIBLE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2), TRUE)
												ENDIF
											ENDIF
										
											IF DOES_ENTITY_EXIST(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj3))
												IF NOT IS_ENTITY_VISIBLE_TO_SCRIPT(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj3))
												AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(LocalPlayerPed)  
													SET_ENTITY_VISIBLE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj3), TRUE)
													PRINTLN("[AW_MISSION] Setting minigame bag object visible. ")
												ENDIF
											ENDIF
											
											IF GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID)) > 0.932
											OR MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer,8000)
												iSafetyTimer = GET_GAME_TIMER()
												PRINTLN("[AW_MISSION] iHackProgress = 8 ")
												iHackProgress++
											ENDIF
										ENDIF
								ENDIF
							ELSE
								IF SHOULD_USE_SWIPE_KEYCARD_ANIMS(tempObj)
									//1959134
									//PHONE
//									IF IS_ENTITY_PLAYING_ANIM(LocalPlayerPed,"anim@heists@humane_labs@emp@hack_door", "hack_intro")
//										IF GET_ENTITY_ANIM_CURRENT_TIME(LocalPlayerPed,"anim@heists@humane_labs@emp@hack_door", "hack_intro") > 0.15
//											SET_ENTITY_VISIBLE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2), TRUE)
//										ENDIF
//									ENDIF
									
									IF IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID))
										IF GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID)) > 0.15
											SET_ENTITY_VISIBLE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2), TRUE)
										ENDIF
									ENDIF
									
									//IF IS_ENTITY_PLAYING_ANIM(LocalPlayerPed,"anim@heists@humane_labs@emp@hack_door", "hack_intro")
									IF IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID))
									OR MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer,6000)
										//IF GET_ENTITY_ANIM_CURRENT_TIME(LocalPlayerPed,"anim@heists@humane_labs@emp@hack_door", "hack_intro") > 0.5
										IF GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID)) > 0.5
											IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH( "cellphone_flashhand" ) ) = 0
												IF IS_PED_DRUNK(PLAYER_PED_ID())
													QUIT_DRUNK_CAMERA_IMMEDIATELY()
													Force_All_Drunk_Peds_To_Become_Sober_And_Quit_Camera()
													Make_Ped_Sober(PLAYER_PED_ID())
												ENDIF
												SET_BIT (BitSet_CellphoneTU, g_BSTU_REMOVE_SNAPMATIC_GRID)
												LAUNCH_CELLPHONE_APPLICATION(AppCamera, FALSE, TRUE, FALSE)
												
												PRINTLN("[AW_MISSION] iHackProgress = 8 ")
												iSafetyTimer = GET_GAME_TIMER()
												iHackProgress++
											ENDIF
										ENDIF
									ELSE
										PRINTLN("[AW_MISSION] Failing hack at stage 6, no keycard anim running")
										SET_HACK_FAIL_BIT_SET(16)
									ENDIF
								ELSE
									IF IS_OBJECT_A_LAPTOP(tempObj)
										IF IS_PAST_SYNC_SCENE_PHASE(MC_playerBD[iPartToUse].iSynchSceneID,0.99)
											MC_playerBD[iPartToUse].iSynchSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(vHackLaptopScenePlayback, <<0,0,0>>, EULER_YXZ, FALSE,TRUE)	
											NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(LocalPlayerPed, MC_playerBD[iPartToUse].iSynchSceneID, "missheist_jewel@hacking", "hack_loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_HIDE_WEAPON | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON | SYNCED_SCENE_ABORT_ON_DEATH, RBF_PLAYER_IMPACT)
											NETWORK_ATTACH_SYNCHRONISED_SCENE_TO_ENTITY(MC_playerBD[iPartToUse].iSynchSceneID,tempObj,-1)
											NETWORK_START_SYNCHRONISED_SCENE(MC_playerBD[iPartToUse].iSynchSceneID)
											iSafetyTimer = GET_GAME_TIMER()
											
											IF MC_serverBD_1.iPlayerHacking = -1
												MC_serverBD_1.iPlayerHacking = iLocalPart
											ENDIF
											
											iHackProgress++
											
											PRINTLN("[AW_MISSION] iHackProgress = 8 ")
										ELSE
											iThisLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID)
											IF iThisLocalSceneID = -1
											OR (NOT IS_SYNCHRONIZED_SCENE_RUNNING(iThisLocalSceneID))
												PRINTLN("[AW_MISSION] Failing hack at stage 6, no synch scene running")
												SET_HACK_FAIL_BIT_SET(14)
											ENDIF
										ENDIF
									ELSE
										IF SHOULD_USE_NEW_ANIMS()
											IF IS_ENTITY_PLAYING_ANIM(LocalPlayerPed,sKeyPadAnim, "enter")
												IF GET_ENTITY_ANIM_CURRENT_TIME(LocalPlayerPed,sKeyPadAnim, "enter") > 0.99
													TASK_PLAY_ANIM(LocalPlayerPed,sKeyPadAnim, "idle_a",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_HIDE_WEAPON | AF_NOT_INTERRUPTABLE | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)
													iSafetyTimer = GET_GAME_TIMER()
													iHackProgress++
													PRINTLN("[AW_MISSION] iHackProgress = 8 ")
												ENDIF
											ELSE
												PRINTLN("[AW_MISSION] Failing hack at stage 7, no keypad enter anim running")
												SET_HACK_FAIL_BIT_SET(13)
											ENDIF
										ELSE
											PRINTLN("[AW_MISSION] iHackProgress = 8 ")
											PRINTLN("[AW_MISSION] No HP ")
											iSafetyTimer = GET_GAME_TIMER()
											iHackProgress++
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						BREAK
						
						CASE 8
						
							IF MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer,6000)
								iHackProgress++
								PRINTLN("[RCC MISSION] PROCESS HACKING MINIGAME - SAFETY TIMER HIT, DONE")
								
								IF SHOULD_USE_SWIPE_KEYCARD_ANIMS(tempObj)
									iSafetyTimer = GET_GAME_TIMER()
								ENDIF
								
								EXIT
							ENDIF
							
							IF SHOULD_USE_HACK_USING_LAPTOP_ANIMS(tempObj)
								iHackProgress++
							ELSE
								IF SHOULD_USE_SWIPE_KEYCARD_ANIMS(tempObj)
									// 2200440 - Possible fix
									IF IS_PC_VERSION()
										IF (MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer, 1000)
										AND (NOT IS_SCREEN_FADING_OUT()
										AND NOT IS_SCREEN_FADED_OUT()))
											DO_SCREEN_FADE_OUT(400)
											CPRINTLN(DEBUG_COLIN, "[RCC MISSION] PROCESS HACKING MINIGAME -  - PC Version Calling Screen Fade Out")
										ENDIF
									ENDIF
									
									//1959134
									VECTOR g_Test3dPhonePosVec
									GET_MOBILE_PHONE_POSITION(g_Test3dPhonePosVec)
									
								   	PRINTLN( "[RCC MISSION] PROCESS HACKING MINIGAME - phone position: ", g_Test3dPhonePosVec, " g_Phone_Active_but_Hidden: ", g_Phone_Active_but_Hidden )
									
									IF g_Test3dPhonePosVec.x = 1.5 AND g_Test3dPhonePosVec.z = -17.0
									OR (IS_PC_VERSION() AND IS_SCREEN_FADED_OUT())
										IF (NOT IS_SCREEN_FADING_OUT()
										AND NOT IS_SCREEN_FADED_OUT())
											PRINTLN("[RCC MISSION] PROCESS HACKING MINIGAME - PHONE HAS FILLED SCREEN, DONE")
										ENDIF
										
										HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
										iSafetyTimer = GET_GAME_TIMER()
										CLEAR_MENU_DATA()
										REMOVE_MENU_HELP_KEYS()
										
										IF IS_PC_VERSION()
											DO_SCREEN_FADE_IN(0)
											CPRINTLN(DEBUG_COLIN, "Circuit Hacking: DO_CIRCUIT_MINIGAME_LAUNCH_PHONE_INTRO - PC Version Calling Screen Fade In")
										ENDIF
										PRINTLN("[RCC MISSION][TELEMETRY]HACKING (DATA CRACK) - INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY - SLOT 0")
										INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY(0)
										iHackProgress++
									ENDIF
									
									IF IS_ENTITY_PLAYING_ANIM(LocalPlayerPed,"anim@heists@humane_labs@emp@hack_door", "hack_intro")
									
									ELSE
										PRINTLN("[RCC MISSION] PROCESS HACKING MINIGAME - NOT PLAYING ANIM, DONE")
										iSafetyTimer = GET_GAME_TIMER()
										iHackProgress++
									ENDIF
									
								ELSE
									IF IS_OBJECT_A_LAPTOP(tempObj)
										IF IS_PAST_SYNC_SCENE_PHASE(MC_playerBD[iPartToUse].iSynchSceneID,0.4)
											PRINTLN("[AW_MISSION] Laptop iHackProgress = 9 ")
											iHackProgress++
										ELSE
											iThisLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID)
											IF iThisLocalSceneID = -1
											OR (NOT IS_SYNCHRONIZED_SCENE_RUNNING(iThisLocalSceneID))
												PRINTLN("[AW_MISSION] Failing hack at stage 8, no synch scene running")
												SET_HACK_FAIL_BIT_SET(12)
											ENDIF
										ENDIF
									ELSE
										IF SHOULD_USE_NEW_ANIMS()
											IF IS_ENTITY_PLAYING_ANIM(LocalPlayerPed,sKeyPadAnim, "idle_a")
												IF GET_ENTITY_ANIM_CURRENT_TIME(LocalPlayerPed,sKeyPadAnim, "idle_a") > 0.4
													//IF IS_PAST_SYNC_SCENE_PHASE(MC_playerBD[iPartToUse].iSynchSceneID,0.4)
														PRINTLN("[AW_MISSION] iHackProgress = 9 ")
														iHackProgress++
													//ENDIF
												ENDIF
											ELSE
												PRINTLN("[AW_MISSION] Failing hack at stage 8, no keypad idle anim running")
												SET_HACK_FAIL_BIT_SET(11)
											ENDIF
										ELSE
											iHackProgress++
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
						BREAK
						
						CASE 9
							IF SHOULD_USE_SWIPE_KEYCARD_ANIMS(tempObj)
								IF bLocalPlayerPedOk
									REMOVE_MENU_HELP_KEYS()
									PRINTLN("[RCC MISSION] HAVE_HACKING_ASSETS_LOADED TRUE, MOVING TO HACK STAGE")
									//iWallPaper
									INITIALISE_HACKING_WALLPAPER_AND_ICONS(sHackingData, GET_HACKING_BACKGROUND(iWallPaper))

									SET_BIT(sHackingData.bsHacking,BS_IS_HACKING)
									RUN_HACKING_MINIGAME_WITH_PARAMETERS(sHackingData, istartLives, iMaxLives, iSpeed, iDecay, 8, 0,TRUE, bJustConnect, bJustBrute, TRUE, TRUE, FALSE, bDownAndOut)
									//NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_CLEAR_TASKS  | NSPC_CAN_BE_TARGETTED | NSPC_ALLOW_PLAYER_DAMAGE)
									NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_ALLOW_PLAYER_DAMAGE|NSPC_PREVENT_EVERYBODY_BACKOFF|NSPC_REENABLE_CONTROL_ON_DEATH)
									DISABLE_INTERACTION_MENU() 
									
									IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
										ADD_MENU_HELP_KEY_INPUT( INPUT_CURSOR_ACCEPT,		"HACK_SEL")	//select
									ELSE
										ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_ACCEPT,		"HACK_SEL")	//select
									ENDIF
									ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_CANCEL,		"HACK_BAC")	//back
									IF NOT bAltHackingGame
										ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_SCRIPT_LSTICK_ALL,	"HACK_MOV")	//move mouse
									ENDIF
									
									//GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].g_bIsHacking = TRUE
									SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bIsHacking)
										START_AUDIO_SCENE("DLC_HEIST_PACIFIC_BANK_HACK_PASSWORD_SCENE" )
									
										IF bAltHackingGame
											IF NOT HAS_MINIGAME_TIMER_BEEN_INITIALISED(0)
												PRINTLN("[RCC MISSION][TELEMETRY] HACKING (DATA CRACK) - INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY - SLOT 0")
												PRINTLN("[RCC MISSION][TELEMETRY] HACKING (DATA CRACK) - START MINIGAME TIMER - SLOT 0")
												INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY(0)
												START_MINIGAME_TIMER_FOR_TELEMETRY(0)
											ENDIF
										ENDIF
										IF bJustBrute
											IF NOT HAS_MINIGAME_TIMER_BEEN_INITIALISED(0)
												PRINTLN("[RCC MISSION][TELEMETRY] HACKING (BRUTEFORCE) - INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY - SLOT 0")
												PRINTLN("[RCC MISSION][TELEMETRY] HACKING (BRUTEFORCE) - START MINIGAME TIMER - SLOT 0")
												INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY(0)
												START_MINIGAME_TIMER_FOR_TELEMETRY(0)
											ENDIF	
										ENDIF
									
									REINIT_NET_TIMER(tdHackTimer)
									
									IF IS_PED_DRUNK(PLAYER_PED_ID())
										QUIT_DRUNK_CAMERA_IMMEDIATELY()
									ENDIF

									iHackProgress++
								ENDIF
							ELSE
								IF bLocalPlayerPedOk
									REMOVE_MENU_HELP_KEYS()
									PRINTLN("[RCC MISSION] HAVE_HACKING_ASSETS_LOADED TRUE, MOVING TO HACK STAGE")
									//iWallPaper
									INITIALISE_HACKING_WALLPAPER_AND_ICONS(sHackingData, GET_HACKING_BACKGROUND(iWallPaper))

									SET_BIT(sHackingData.bsHacking,BS_IS_HACKING)
									RUN_HACKING_MINIGAME_WITH_PARAMETERS(sHackingData, istartLives, iMaxLives, iSpeed, iDecay, 8, 0,TRUE, bJustConnect, bJustBrute, TRUE, TRUE, FALSE, bDownAndOut)
									//NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_CLEAR_TASKS  | NSPC_CAN_BE_TARGETTED | NSPC_ALLOW_PLAYER_DAMAGE)
									NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_ALLOW_PLAYER_DAMAGE|NSPC_PREVENT_EVERYBODY_BACKOFF|NSPC_REENABLE_CONTROL_ON_DEATH)
									DISABLE_INTERACTION_MENU() 
									IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
										ADD_MENU_HELP_KEY_INPUT( INPUT_CURSOR_ACCEPT,		"HACK_SEL")	//select
									ELSE
										ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_ACCEPT,		"HACK_SEL")	//select
									ENDIF
									ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_CANCEL,		"HACK_BAC")	//back
									IF NOT bAltHackingGame
										ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_SCRIPT_LSTICK_ALL,	"HACK_MOV")	//move mouse
									ENDIF
									//GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].g_bIsHacking = TRUE
									SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bIsHacking)
										IF bAltHackingGame
											IF NOT HAS_MINIGAME_TIMER_BEEN_INITIALISED(0)
												PRINTLN("[RCC MISSION][TELEMETRY] HACKING (DATA CRACK) - INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY - SLOT 0")
												PRINTLN("[RCC MISSION][TELEMETRY] HACKING (DATA CRACK) - START MINIGAME TIMER - SLOT 0")
												INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY(0)
												START_MINIGAME_TIMER_FOR_TELEMETRY(0)
											ENDIF
										ELSE
											IF bJustBrute
												IF NOT HAS_MINIGAME_TIMER_BEEN_INITIALISED(0)
													PRINTLN("[RCC MISSION][TELEMETRY] HACKING (BRUTEFORCE) - INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY - SLOT 0")
													PRINTLN("[RCC MISSION][TELEMETRY] HACKING (BRUTEFORCE) - START MINIGAME TIMER - SLOT 0")
													INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY(0)
													START_MINIGAME_TIMER_FOR_TELEMETRY(0)
												ENDIF	
											ENDIF
										ENDIF
									
										START_AUDIO_SCENE("DLC_HEIST_PACIFIC_BANK_HACK_PASSWORD_SCENE" )
									
									
									
									
									REINIT_NET_TIMER(tdHackTimer)

									iHackProgress++
								ENDIF
							ENDIF
						
						BREAK
						
						CASE 10
							
							IF SHOULD_USE_SWIPE_KEYCARD_ANIMS(tempObj)
								FORCE_HACKING_HELP_BUTTONS(bAltHackingGame)
							ENDIF
							
							IF SHOULD_USE_SWIPE_KEYCARD_ANIMS(tempObj)
								IF NOT IS_ENTITY_PLAYING_ANIM(LocalPlayerPed,"anim@heists@humane_labs@emp@hack_door", "hack_loop")
									IF IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID))
										IF GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID)) > 0.99
											TASK_PLAY_ANIM(LocalPlayerPed,"anim@heists@humane_labs@emp@hack_door", "hack_loop",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1, AF_EXTRACT_INITIAL_OFFSET | AF_HIDE_WEAPON | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS  | AF_LOOPING)
											PRINTLN("[AW_MISSION] playing hack loop ")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							//Start loop when necessary
							IF SHOULD_USE_HACK_USING_LAPTOP_ANIMS(tempObj)
								IF NOT IS_ENTITY_PLAYING_ANIM(LocalPlayerPed,sLaptopHackDict, "hack_loop")
									IF IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID))
										IF GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID)) > 0.99
											
											sBagAnim = GET_BAG_LOOP_ANIM_NAME()
											
											//vPlayAnimAdvancedPlayback = GET_ENTITY_COORDS(tempObj)
											vPlayAnimAdvancedPlayback = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj,<<0,0.07,0>>)
											//0.08	
											MC_playerBD[iPartToUse].iSynchSceneID  = NETWORK_CREATE_SYNCHRONISED_SCENE(vPlayAnimAdvancedPlayback, <<0,0, GET_ENTITY_HEADING(tempObj)>>, DEFAULT, FALSE,TRUE)
											NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(LocalPlayerPed, MC_playerBD[iPartToUse].iSynchSceneID, sLaptopHackDict, "hack_loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_HIDE_WEAPON | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON | SYNCED_SCENE_ABORT_ON_DEATH , RBF_PLAYER_IMPACT)
											NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj), MC_playerBD[iPartToUse].iSynchSceneID,
																		  			sLaptopHackDict, "hack_loop_card", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
											NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2), MC_playerBD[iPartToUse].iSynchSceneID,
																		  			sLaptopHackDict, "hack_loop_laptop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
											NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj3), MC_playerBD[iPartToUse].iSynchSceneID,
																		  			sLaptopHackDict,sBagAnim , NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
											NETWORK_START_SYNCHRONISED_SCENE(MC_playerBD[iPartToUse].iSynchSceneID )
											
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							
							IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
							
								
								
								IF NOT HAS_PLAYER_FAILED_HACKING_MP(sHackingData)
									
									//Distance check
							
									IF GET_DISTANCE_BETWEEN_ENTITIES(tempObj,PLAYER_PED_ID()) > 5
										IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iBeginHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
											SET_BIT(MC_playerBD[iPartToUse].iBeginHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
											PRINTLN("[RCC MISSION]  setting iBeginHackFailBitset- quit, part: ",iPartToUse," for object: ",MC_playerBD[iPartToUse].iObjHacking)
											CLEAR_HELP()
											iSafetyTimer = GET_GAME_TIMER()
											IF IS_AUDIO_SCENE_ACTIVE("DLC_HEIST_PACIFIC_BANK_HACK_PASSWORD_SCENE")
												STOP_AUDIO_SCENE("DLC_HEIST_PACIFIC_BANK_HACK_PASSWORD_SCENE" )
											ENDIF
											
											iHackProgress++
										ENDIF
									ENDIF

									GET_SCREEN_RESOLUTION(iScreenX, iScreenY)
									DRAW_MENU_HELP_SCALEFORM(iScreenX)
									
									IF WarningPopup = TRUE
										PRINTLN("[AW MISSION] POP UP ACTIVE, BLOCKING CANCEL")
									ENDIF
									
									
									IF iThisHacking = 2
									OR iThisHacking = 8
										IF NOT IS_PAUSE_MENU_ACTIVE()
											IF WarningPopup = FALSE
												IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
													IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iBeginHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
														SET_BIT(MC_playerBD[iPartToUse].iBeginHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
														PRINTLN("[RCC MISSION] setting iBeginHackFailBitset- quit, part: ",iPartToUse," for object: ",MC_playerBD[iPartToUse].iObjHacking)
														PRINTLN("[RCC MISSION] PLAYER BACKED OUT OF HACKING")
														CLEAN_UP_NEW_HACKING_GAME_SOUNDS()
														IF IS_AUDIO_SCENE_ACTIVE("DLC_HEIST_PACIFIC_BANK_HACK_PASSWORD_SCENE")
															STOP_AUDIO_SCENE("DLC_HEIST_PACIFIC_BANK_HACK_PASSWORD_SCENE" )
														ENDIF
														iHackProgress++
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									
									IF bLocalPlayerPedOk
										SET_PED_RESET_FLAG(LocalPlayerPed, PRF_CannotBeTargetedByAI, FALSE)
									ENDIF
									
									DISABLE_DPADDOWN_THIS_FRAME()
									DISABLE_CELLPHONE_THIS_FRAME_ONLY()
									DISABLE_SELECTOR_THIS_FRAME() 
									DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_ALL_OVERHEADS)
									
									//IF sHackingData.b_hack_connect_game_solved
									IF IS_BIT_SET(sHackingData.bsHacking,BS_IS_HACKING)
										IF NOT IS_BIT_SET(iLocalBoolCheck2, LBOOL2_HACKING_TICKER_SENT)
											BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_PART_HACK_OBJ)
											PRINTLN("[RCC MISSION] printing part hack ticker")
											SET_BIT(ilocalboolcheck2, LBOOL2_HACKING_TICKER_SENT)
										ENDIF
									ENDIF
									
									IF iThisHacking = 5
									OR iThisHacking = 6
										IF iHackLimitTimer = 0
											IF HAS_NET_TIMER_STARTED(MC_serverBD_3.tdObjectiveLimitTimer[MC_playerBD[iPartToUse].iteam])
									      		IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
													IF HAS_NET_TIMER_STARTED(MC_serverBD_3.tdObjectiveLimitTimer[MC_playerBD[iPartToUse].iteam])
									      				 iHackLimitTimer = ((GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iObjectiveTimeLimitRule[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]]))  - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdObjectiveLimitTimer[MC_playerBD[iPartToUse].iteam])- MC_serverBD_3.iTimerPenalty[MC_playerBD[iPartToUse].iteam])
														 PRINTLN("[RCC MISSION] STORE HACKING TIME ")
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ELSE
										iHackLimitTimer = 0
									ENDIF
									IF iHackLimitTimer = 0
										iLocalHackLimitTimer = 60000
									ELSE
										iLocalHackLimitTimer = iHackLimitTimer
									ENDIF
									
									//process hack connect
									RUN_HACKING_MINIGAME_WITH_PARAMETERS(sHackingData, istartLives, iMaxLives, iSpeed, iDecay, 8, 0, TRUE, bJustConnect, bJustBrute, TRUE, TRUE, FALSE, bDownAndOut,iLocalHackLimitTimer)
									
										IF bJustBrute
											IF IS_BIT_SET(sHackingData.bsHacking,BS_BRUTE_FORCE_INCREASE_FAIL_TELEMETRY)
												PRINTLN("[RCC MISSION][TELEMETRY] HACKING (BRUTEFORCE) - INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY - SLOT 0")
												INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY(0)
												CLEAR_BIT(sHackingData.bsHacking,BS_BRUTE_FORCE_INCREASE_FAIL_TELEMETRY)
											ENDIF
										ENDIF
										IF bAltHackingGame
											IF sThisDCHacking.iIncrementFailCount = 1
												PRINTLN("[RCC MISSION][TELEMETRY] HACKING (DATA CRACK) - INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY - SLOT 0")
												INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY(0)
												sThisDCHacking.iIncrementFailCount = -1
											ENDIF
										ENDIF
									
									IF bAltHackingGame
										IF HAS_PLAYER_BEAT_NEW_HACKING(sHackingData)
												PRINTLN("[RCC MISSION][TELEMETRY] HACKING (DATA CRACK) - STORE_MINIGAME_TIMER_FOR_TELEMETRY - SLOT 0")
												PRINTLN("[RCC MISSION][TELEMETRY] HACKING (DATA CRACK) - STORE_MINIGAME_ATTEMPTS_FOR_TELEMETRY - SLOT 0")
												STORE_MINIGAME_TIMER_FOR_TELEMETRY(0)
												STORE_MINIGAME_ATTEMPTS_FOR_TELEMETRY(0)
											IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("H1_HNG")
												CLEAR_HELP()
											ENDIF
											iSafetyTimer = GET_GAME_TIMER()
											iHackProgress++
										ENDIF
									ELSE
										IF bJustConnect
											IF HAS_PLAYER_BEAT_HACK_CONNECT(sHackingData)
												CLEAR_HELP()
												iSafetyTimer = GET_GAME_TIMER()
												iHackProgress++
											ENDIF
										ELIF bJustBrute
											IF HAS_PLAYER_BEAT_BRUTEFORCE(sHackingData)
												CLEAR_HELP()
													PRINTLN("[RCC MISSION][TELEMETRY] HACKING (BRUTEFORCE) - STORE_MINIGAME_TIMER_FOR_TELEMETRY - SLOT 0")
													PRINTLN("[RCC MISSION][TELEMETRY] HACKING (BRUTEFORCE) - STORE_MINIGAME_ATTEMPTS_FOR_TELEMETRY - SLOT 0")
													STORE_MINIGAME_TIMER_FOR_TELEMETRY(0)
													STORE_MINIGAME_ATTEMPTS_FOR_TELEMETRY(0)
												
												iSafetyTimer = GET_GAME_TIMER()
												iHackProgress++
											ENDIF
										ELSE
											IF HAS_DOWNLOAD_WINDOW_BEEN_OPENED(sHackingData)
												CLEAR_HELP()
												iSafetyTimer = GET_GAME_TIMER()
												iHackProgress++
											ENDIF
										ENDIF
									ENDIF
								ELSE
									IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iBeginHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
										SET_BIT(MC_playerBD[iPartToUse].iBeginHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
										PRINTLN("[RCC MISSION]  setting iBeginHackFailBitset- quit, part: ",iPartToUse," for object: ",MC_playerBD[iPartToUse].iObjHacking)
										CLEAR_HELP()
										iSafetyTimer = GET_GAME_TIMER()
										IF IS_AUDIO_SCENE_ACTIVE("DLC_HEIST_PACIFIC_BANK_HACK_PASSWORD_SCENE")
											STOP_AUDIO_SCENE("DLC_HEIST_PACIFIC_BANK_HACK_PASSWORD_SCENE" )
										ENDIF
										iHackProgress++
									ENDIF
								ENDIF
							ENDIF
							
						BREAK
						
						CASE 11
							
							IF IS_AUDIO_SCENE_ACTIVE("DLC_HEIST_PACIFIC_BANK_HACK_PASSWORD_SCENE")
								STOP_AUDIO_SCENE("DLC_HEIST_PACIFIC_BANK_HACK_PASSWORD_SCENE" )
							ENDIF
							
							IF SHOULD_USE_HACK_USING_LAPTOP_ANIMS(tempObj)
									IF HAS_ANIM_DICT_LOADED(sLaptopHackDict)
										//vPlayAnimAdvancedPlayback = GET_ENTITY_COORDS(tempObj)
										IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<252.731796,227.221893,100.683220>>, <<253.558563,229.504852,102.683220>>, 1.500000)
										AND NOT IS_BIT_SET(MC_playerBD[iPartToUse].iBeginHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
											vPlayAnimAdvancedPlayback = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj,<<0,0.07,0>>)
											
											sBagAnim = GET_BAG_EXIT_ANIM_NAME()
											
											MC_playerBD[iPartToUse].iSynchSceneID  = NETWORK_CREATE_SYNCHRONISED_SCENE(vPlayAnimAdvancedPlayback, <<0,0, GET_ENTITY_HEADING(tempObj)>>, DEFAULT, FALSE)
											NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(LocalPlayerPed, MC_playerBD[iPartToUse].iSynchSceneID, sLaptopHackDict, "hack_exit", NORMAL_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_HIDE_WEAPON | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON | SYNCED_SCENE_ABORT_ON_DEATH , RBF_PLAYER_IMPACT)
											NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj), MC_playerBD[iPartToUse].iSynchSceneID,
																		  			sLaptopHackDict, "hack_exit_card", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
											NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2), MC_playerBD[iPartToUse].iSynchSceneID,
																		  			sLaptopHackDict, "hack_exit_laptop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
											NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj3), MC_playerBD[iPartToUse].iSynchSceneID,
																		  			sLaptopHackDict, sBagAnim , NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
											NETWORK_START_SYNCHRONISED_SCENE(MC_playerBD[iPartToUse].iSynchSceneID )
											
											SET_GAMEPLAY_CAM_RELATIVE_PITCH()
											SET_GAMEPLAY_CAM_RELATIVE_HEADING()
											NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_ALLOW_PLAYER_DAMAGE|NSPC_PREVENT_EVERYBODY_BACKOFF|NSPC_LEAVE_CAMERA_CONTROL_ON|NSPC_ALLOW_PAD_SHAKE|NSPC_REENABLE_CONTROL_ON_DEATH)
											
											
											iSafetyTimer = GET_GAME_TIMER()
											iHackProgress++
										ELSE
											IF IS_BIT_SET(MC_playerBD[iPartToUse].iBeginHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
												PRINTLN("[RCC MISSION] - MC_playerBD[iPartToUse].iBeginHackFailBitset,MC_playerBD[iPartToUse].iObjHacking")
											ENDIF
											
											vPlayAnimAdvancedPlayback = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj,<<0,0.07,0>>)
											
											sBagAnim = GET_BAG_EXIT_ANIM_NAME()
													
											MC_playerBD[iPartToUse].iSynchSceneID  = NETWORK_CREATE_SYNCHRONISED_SCENE(vPlayAnimAdvancedPlayback, <<0,0, GET_ENTITY_HEADING(tempObj)>>, DEFAULT, TRUE)
											NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(LocalPlayerPed, MC_playerBD[iPartToUse].iSynchSceneID, sLaptopHackDict, "hack_exit", NORMAL_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_HIDE_WEAPON | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON | SYNCED_SCENE_ABORT_ON_DEATH , RBF_PLAYER_IMPACT)
											NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj), MC_playerBD[iPartToUse].iSynchSceneID,
																		  			sLaptopHackDict, "hack_exit_card", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
											NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2), MC_playerBD[iPartToUse].iSynchSceneID,
																		  			sLaptopHackDict, "hack_exit_laptop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
											NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj3), MC_playerBD[iPartToUse].iSynchSceneID,
																		  			sLaptopHackDict, sBagAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
											NETWORK_START_SYNCHRONISED_SCENE(MC_playerBD[iPartToUse].iSynchSceneID )
											
											SET_GAMEPLAY_CAM_RELATIVE_PITCH()
											SET_GAMEPLAY_CAM_RELATIVE_HEADING()
											NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_ALLOW_PLAYER_DAMAGE|NSPC_PREVENT_EVERYBODY_BACKOFF|NSPC_LEAVE_CAMERA_CONTROL_ON|NSPC_ALLOW_PAD_SHAKE|NSPC_REENABLE_CONTROL_ON_DEATH)
											
											iSafetyTimer = GET_GAME_TIMER()
											iHackProgress++
										ENDIF
									ENDIF
							ELSE
								IF SHOULD_USE_SWIPE_KEYCARD_ANIMS(tempObj)
									//1959134
									TASK_PLAY_ANIM(LocalPlayerPed,"anim@heists@humane_labs@emp@hack_door", "hack_outro",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1, AF_EXTRACT_INITIAL_OFFSET | AF_HIDE_WEAPON |  AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE | AF_ABORT_ON_WEAPON_DAMAGE)
									iTimeOutTimer = GET_GAME_TIMER()
									PLAY_SOUND_FROM_ENTITY(-1,"Hack_Success" ,PLAYER_PED_ID(),"DLC_HEIST_BIOLAB_PREP_HACKING_SOUNDS",TRUE,20)
									iHackProgress++
									PRINTLN("[AW_MISSION] Playing hack audio ")
									PRINTLN("[AW_MISSION] iHackProgress = 12 ")
								ELSE
									IF IS_OBJECT_A_LAPTOP(tempObj)
										IF bLocalPlayerPedOk
											MC_playerBD[iPartToUse].iSynchSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(vHackLaptopScenePlayback, <<0,0,0>>, EULER_YXZ, FALSE)	
											NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(LocalPlayerPed, MC_playerBD[iPartToUse].iSynchSceneID, "missheist_jewel@hacking", "hack_outro", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_HIDE_WEAPON| SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_ABORT_ON_DEATH | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON, RBF_PLAYER_IMPACT)
											NETWORK_ATTACH_SYNCHRONISED_SCENE_TO_ENTITY(MC_playerBD[iPartToUse].iSynchSceneID,tempObj,-1)
											NETWORK_START_SYNCHRONISED_SCENE(MC_playerBD[iPartToUse].iSynchSceneID)
											PRINTLN("[AW_MISSION] iHackProgress = 12")
											PRINTLN("[AW_MISSION] Playing exit animation")
											iTimeOutTimer = GET_GAME_TIMER()
											iHackProgress++
										ENDIF
									ELSE
										IF SHOULD_USE_NEW_ANIMS()
											IF bLocalPlayerPedOk
												TASK_PLAY_ANIM(LocalPlayerPed,sKeyPadAnim, "exit",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_HIDE_WEAPON | AF_NOT_INTERRUPTABLE | AF_HOLD_LAST_FRAME)
												PRINTLN("[AW_MISSION] iHackProgress = 12")
												iTimeOutTimer = GET_GAME_TIMER()
												iHackProgress++
											ENDIF
										ELSE
											IF bLocalPlayerPedOk
												CLEAR_PED_TASKS(LocalPlayerPed)
											ENDIF
											iHackProgress++
										ENDIF

									ENDIF
								ENDIF
							ENDIF
						BREAK
						
						CASE 12
							IF SHOULD_USE_HACK_USING_LAPTOP_ANIMS(tempObj)
								
								IF IS_AUDIO_SCENE_ACTIVE("DLC_HEIST_PACIFIC_BANK_HACK_PASSWORD_SCENE")
									STOP_AUDIO_SCENE("DLC_HEIST_PACIFIC_BANK_HACK_PASSWORD_SCENE" )
								ENDIF
								
								IF HAS_ANIM_DICT_LOADED(sLaptopHackDict)
									IF IS_ENTITY_PLAYING_ANIM(LocalPlayerPed,sLaptopHackDict, "hack_exit")
									OR MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer,6000)
										IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(LocalPlayerPed)
											//hide objects as they get put in the bag.
											IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<252.731796,227.221893,100.683220>>, <<253.558563,229.504852,102.683220>>, 1.500000)
											AND NOT IS_BIT_SET(MC_playerBD[iPartToUse].iBeginHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
												IF IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID))
												OR MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer,8000)
													IF GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID)) > 0.6
													OR MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer,8000)
															IF g_FMMC_STRUCT.iRootContentIDHash = g_sMPTUNABLES.iroot_id_HASH_The_Pacific_Standard_Job
															OR g_FMMC_STRUCT.iRootContentIDHash = g_sMPTUNABLES.iroot_id_HASH_The_Pacific_Standard_Job2
																SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed,FALSE,FALSE)
															ENDIF
															g_FMMC_STRUCT.bRememberToCleanUpHacking = TRUE
															PRINTLN("[AW_MISSION] Timer expired iHackProgress = 13")
															iHackProgress = 13
													ENDIF
												ENDIF
											ELSE
												IF IS_BIT_SET(MC_playerBD[iPartToUse].iBeginHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
													PRINTLN("[AW_MISSION] case 12 - MC_playerBD[iPartToUse].iBeginHackFailBitset,MC_playerBD[iPartToUse].iObjHacking")
												ENDIF
												IF IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID))
												OR MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer,8000)
													IF GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID)) > 0.915
													OR MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer,8000)
															
															DELETE_HACKING_KEYCARD_OBJECT()
															DELETE_LAPTOP_OBJECT()
															APPLY_DUFFEL_BAG_HEIST_GEAR(LocalPlayerPed, iLocalPart)
															RELEASE_PRELOADED_MP_HEIST_OUTFIT(LocalPlayerPed)
															RUN_LOCAL_PLAYER_PROOF_SETTING(TRUE)
															
															PRINTLN("[AW_MISSION] Timer expired iHackProgress = 99")
															CLEAR_PED_TASKS(LocalPlayerPed)
															iHackProgress = 99
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
							
							
								IF SHOULD_USE_SWIPE_KEYCARD_ANIMS(tempObj)
									IF MANAGE_HACKING_TIMER(iTimeOutTimer,8000)
										CLEAR_PED_TASKS(LocalPlayerPed)
										PRINTLN("[AW_MISSION] Timer expired iHackProgress = 13")
										SET_ENTITY_VISIBLE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2), FALSE)
										DELETE_FAKE_PHONE_OBJECT()
										CLEAN_UP_MINIGAME_OBJECTS()
										RUN_LOCAL_PLAYER_PROOF_SETTING(TRUE)
										SET_PED_USING_ACTION_MODE(LocalPlayerPed,TRUE)
										iTimeOutTimer = GET_GAME_TIMER()
										iHackProgress++
									ENDIF
									IF IS_ENTITY_PLAYING_ANIM(LocalPlayerPed,"anim@heists@humane_labs@emp@hack_door", "hack_outro")
										IF IS_ENTITY_VISIBLE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2))
											IF GET_ENTITY_ANIM_CURRENT_TIME(LocalPlayerPed,"anim@heists@humane_labs@emp@hack_door", "hack_outro") > 0.599
												PRINTLN("[AW_MISSION] Hiding phone")
												SET_ENTITY_VISIBLE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2), FALSE)
											ENDIF
										ENDIF
										IF GET_ENTITY_ANIM_CURRENT_TIME(LocalPlayerPed,"anim@heists@humane_labs@emp@hack_door", "hack_outro") > 0.65 //0.99
											//CLEAR_PED_TASKS(LocalPlayerPed)
											PRINTLN("[AW_MISSION] Breaking out of anim early")
											PRINTLN("[AW_MISSION] iHackProgress = 13")
											SET_ENTITY_VISIBLE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2), FALSE)
											DELETE_FAKE_PHONE_OBJECT()
											CLEAN_UP_MINIGAME_OBJECTS()
											RUN_LOCAL_PLAYER_PROOF_SETTING(TRUE)
											SET_PED_USING_ACTION_MODE(LocalPlayerPed,TRUE)
											iTimeOutTimer = GET_GAME_TIMER()
											NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
											iHackProgress++
										ENDIF
									ENDIF
								ELSE
									IF IS_OBJECT_A_LAPTOP(tempObj)
										IF IS_PAST_SYNC_SCENE_PHASE(MC_playerBD[iPartToUse].iSynchSceneID,0.95)
										OR NOT IS_SYNC_SCENE_ACTIVE(MC_playerBD[iPartToUse].iSynchSceneID)
											IF NOT IS_SYNC_SCENE_ACTIVE(MC_playerBD[iPartToUse].iSynchSceneID)
												PRINTLN("[AW_MISSION] SS not active")
											ENDIF
											CLEAR_PED_TASKS(LocalPlayerPed)
											PRINTLN("[AW_MISSION] Coming out of exit anim")
											PRINTLN("[AW_MISSION] iHackProgress = 13")	
											RUN_LOCAL_PLAYER_PROOF_SETTING(TRUE)
											iHackProgress++
										ENDIF

										IF MANAGE_HACKING_TIMER(iTimeOutTimer,7000)
											CLEAR_PED_TASKS(LocalPlayerPed)
											PRINTLN("[AW_MISSION] Coming out of exit anim - time expired?")
											PRINTLN("[AW_MISSION] Coming out of exit anim")
											PRINTLN("[AW_MISSION] iHackProgress = 13")
											RUN_LOCAL_PLAYER_PROOF_SETTING(TRUE)
											iHackProgress++
										ENDIF
									ELSE
										IF SHOULD_USE_NEW_ANIMS()
											IF MANAGE_HACKING_TIMER(iTimeOutTimer,4000)
												PRINTLN("[AW_MISSION] TImer expired iHackProgress = 12")
												CLEAR_PED_TASKS(LocalPlayerPed)
												iHackProgress++
											ELSE
												IF IS_ENTITY_PLAYING_ANIM(LocalPlayerPed,sKeyPadAnim, "exit")
													IF GET_ENTITY_ANIM_CURRENT_TIME(LocalPlayerPed,sKeyPadAnim, "exit") > 0.99
														CLEAR_PED_TASKS(LocalPlayerPed)
														PRINTLN("[AW_MISSION] iHackProgress = 13")
														RUN_LOCAL_PLAYER_PROOF_SETTING(TRUE)
														iHackProgress++
													ENDIF
												ENDIF
											ENDIF
										ELSE
											CLEAR_PED_TASKS(LocalPlayerPed)
											PRINTLN("[AW_MISSION] iHackProgress = 12")
											RUN_LOCAL_PLAYER_PROOF_SETTING(TRUE)
											iHackProgress++
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						BREAK
						
						CASE 99
							IF SHOULD_USE_SWIPE_KEYCARD_ANIMS(tempObj)
								IF MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer,1000)
									PRINTLN("[AW_MISSION] iHackProgress = 13")
									iHackProgress++
								ENDIF
							ELSE
								IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(LocalPlayerPed)
									PRINTLN("[AW_MISSION] iHackProgress = 13")
									DELETE_HACK_BAG_OBJECT()
									iHackProgress = 13
								ENDIF
							ENDIF
						BREAK
						
						CASE 13
						
							IF IS_BIT_SET(MC_playerBD[iPartToUse].iBeginHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
								IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
									SET_HACK_FAIL_BIT_SET(10)
									PRINTLN("[RCC MISSION]  setting hack fail bitset - quit, part: ",iPartToUse," for object: ",MC_playerBD[iPartToUse].iObjHacking)
								ENDIF
							ELSE
								
								//GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].g_bIsHacking = FALSE
								CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bIsHacking)
								
								IF SHOULD_USE_HACK_USING_LAPTOP_ANIMS(tempObj)
									IF g_FMMC_STRUCT.iRootContentIDHash != g_sMPTUNABLES.iroot_id_HASH_The_Pacific_Standard_Job 
									//OR NOT bFakeOnMission
										SET_PED_USING_ACTION_MODE(LocalPlayerPed,TRUE)
										REMOVE_ANIM_DICT("anim@heists@ornate_bank@hack")
										REMOVE_ANIM_DICT("anim@heists@ornate_bank@hack_heels")
									ENDIF
								ELIF SHOULD_USE_SWIPE_KEYCARD_ANIMS(tempObj)
									//1959134
									//SET_PED_USING_ACTION_MODE(LocalPlayerPed,TRUE)
									REMOVE_ANIM_DICT("anim@heists@humane_labs@emp@hack_door")
								ELSE
									IF IS_OBJECT_A_LAPTOP(tempObj)
										REMOVE_ANIM_DICT("missheist_jewel@hacking")
									ELSE
										IF SHOULD_USE_NEW_ANIMS()
											REMOVE_ANIM_DICT(sKeyPadAnim)
										ENDIF
									ENDIF
								ENDIF
								
								IF bLocalPlayerPedOk
									IF g_FMMC_STRUCT.iRootContentIDHash != g_sMPTUNABLES.iroot_id_HASH_The_Pacific_Standard_Job 
									//OR NOT bFakeOnMission
										SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed,TRUE,FALSE)
										NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
									ENDIF
								ENDIF
								
								MC_playerBD[iPartToUse].iHackTime = MC_playerBD[iPartToUse].iHackTime + GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdHackTimer)
								MC_playerBD[iPartToUse].iNumHacks++
								PRINTLN("[RCC MISSION][TELEMETRY] inumHacks HEIST TELEMETRY HACKING MINIGAME  (COMPUTER) -", MC_playerBD[iPartToUse].iNumHacks)
								
								RESET_NET_TIMER(tdHackTimer)
								ENABLE_INTERACTION_MENU()
								CLEANUP_MENU_ASSETS()
								
								PRINTLN("[RCC MISSION] Player passed hacking minigame.")

								IF g_FMMC_STRUCT.iRootContentIDHash != g_sMPTUNABLES.iroot_id_HASH_The_Pacific_Standard_Job
								//OR NOT bFakeOnMission
									FORCE_QUIT_PASS_HACKING_MINIGAME(sHackingData)
								ELSE
									FORCE_QUIT_PASS_HACKING_MINIGAME(sHackingData,TRUE,FALSE)
								ENDIF
								
								CLEAR_BIT(MC_playerBD[iPartToUse].iRequestHackAnimBitset,MC_playerBD[iPartToUse].iObjHacking)
								CLEAR_BIT(MC_playerBD[iPartToUse].iBeginHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
								MC_playerBD[iPartToUse].iObjHacked = iobj
								RESET_OBJ_HACKING_INT()
								
								RUN_LOCAL_PLAYER_PROOF_SETTING(TRUE)
	//							CLEAR_PED_TASKS(LocalPlayerPed)
								PRINTLN("[RCC MISSION] SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)")
								SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
								TASK_CLEAR_LOOK_AT(LocalPlayerPed)
								CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_REMOVE_SNAPMATIC_GRID)
								g_FMMC_STRUCT.bDisablePhoneInstructions = FALSE
								
									STOP_AUDIO_SCENE("DLC_HEIST_PACIFIC_BANK_HACK_PASSWORD_SCENE" )
																
								// What is happening here
								PLAY_SOUND_FROM_COORD( -1, 
														GET_DOOR_SOUND_FROM_MODEL_NAME(GET_FMMC_DOOR_MODEL(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iLinkedDoor[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]])),
														GET_FMMC_DOOR_COORDS(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iLinkedDoor[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]]),
														"HACKING_DOOR_UNLOCK_SOUNDS",
														TRUE,
														30,
														DEFAULT )

								IF NOT IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
									RELEASE_SCRIPT_AUDIO_BANK()
									PRINTLN("RELEASE_SCRIPT_AUDIO_BANK - Called from fm_mission_controller_minigame.sch 7")
								ENDIF

								iHackLimitTimer = 0
								iHackProgress = 0
							ENDIF
						BREAK
						
					ENDSWITCH
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: SAFE CRACKING MINIGAME !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************



PROC SETUP_SAFE_DATA(OBJECT_INDEX tempObj)

	SafeCrackData.vSafeCoord 				= (GET_ENTITY_COORDS(tempObj)-<<0, 0, 0.04>>) 
	SafeCrackData.fSafeHeading				= GET_ENTITY_HEADING(tempObj)  
	SafeCrackData.vSafeCoord 				= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(SafeCrackData.vSafeCoord, SafeCrackData.fSafeHeading, <<0.40, -0.46, -0.47>>)
	
	//serverBD.SafeData.vSafeDoor				= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(SafeCrackData.vSafeCoord, SafeCrackData.fSafeHeading, <<0, 0, 0.1>>)
	//serverBD.SafeData.fSafeDoorHeading		= SafeCrackData.fSafeHeading
	
	SafeCrackData.vPlayerSafeCrackCoord 	= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(SafeCrackData.vSafeCoord, SafeCrackData.fSafeHeading, <<-0.55, -0.55, 0>>)
	SafeCrackData.fPlayerSafeCrackHeading 	= SafeCrackData.fSafeHeading	
	PRINTLN("[RCC MISSION]   ---->  MISSION CONTROLLER - SafeCrackData.vPlayerSafeCrackCoord ",SafeCrackData.vPlayerSafeCrackCoord)
	
	SafeCrackData.vFloatingHelpMin			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(SafeCrackData.vSafeCoord, SafeCrackData.fSafeHeading, <<-5, -5, -1.0>>)	
	SafeCrackData.vFloatingHelpMax			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(SafeCrackData.vSafeCoord, SafeCrackData.fSafeHeading, <<5, 5, 2.0>>)
	SafeCrackData.fFloatingHelpWidth		= 8.5
	PRINTLN("[RCC MISSION]   ---->  MISSION CONTROLLER  - SafeCrackData.vFloatingHelpMin ",SafeCrackData.vFloatingHelpMin) 
	PRINTLN("[RCC MISSION]   ---->  MISSION CONTROLLER  - SafeCrackData.vFloatingHelpMax ",SafeCrackData.vFloatingHelpMax) 
	
	SafeCrackData.vCameraPos				= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(SafeCrackData.vSafeCoord, SafeCrackData.fSafeHeading, <<1.4, -1.2, 0.75+0.5>>)
	SafeCrackData.fCameraFOV				= 60.0
	
	SafeCrackData.vSafeDoorProp				= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(SafeCrackData.vSafeCoord, SafeCrackData.fSafeHeading, <<0.32, -0.34, -0.68>>)
	
ENDPROC

PROC EXIT_AND_RESET_SAFE_CRACKING_MINI_GAME()
	CPRINTLN(DEBUG_MISSION, "EXIT_AND_RESET_SAFE_CRACKING_MINI_GAME")
	
	RESET_OBJ_HACKING_INT()
	RESET_SAFE_CRACK(SafeCrackData, TRUE)
	CLEAR_SAFE_CRACK_SEQUENCES(SafeCrackData)
	CLEAR_BIT(iLocalBoolCheck2, LBOOL2_IN_RANGE_FOR_SAFE_CRACK)
	
	IF IS_ENTITY_ATTACHED(LocalPlayerPed)
		DETACH_ENTITY(LocalPlayerPed, FALSE, FALSE)
	ENDIF
ENDPROC

FUNC BOOL NEED_TO_CREATE_SAFE_CRACK_SEQUENCES()
	
	IF SafeCrackData.seqGetIntoPos = NULL
	OR SafeCrackData.seqIdleAnim = NULL
	OR SafeCrackData.seqLockSucceed = NULL
	OR SafeCrackData.seqLockFail = NULL
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_SAFE_CRACKING_MINI_GAME(OBJECT_INDEX tempObj,INT iobj)
	
	IF MC_playerBD[iPartToUse].iObjHacking = iObj
	AND NEED_TO_CREATE_SAFE_CRACK_SEQUENCES()
		CREATE_SAFE_CRACK_SEQUENCES(SafeCrackData, tempObj)
		EXIT
	ENDIF
	
	IF MC_playerBD[iPartToUse].iObjHacked = -1
		IF MC_serverBD.iObjHackPart[iobj] = -1
			IF MC_playerBD[iPartToUse].iObjHacking = -1
			
				VECTOR vAACoordOb 
				VECTOR vAACoordPro
				vAACoordOb = GET_ENTITY_COORDS(tempObj)
				vAACoordOb.z = vAACoordOb.z + fThisTweakValue1
				
				vAACoordPro = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj, vHackAAOffset * 0.45)
				vAACoordPro.z = vAACoordPro.z - fThisTweakValue2
				
				//DRAW_DEBUG_ANGLED_AREA(vAACoordOb, vAACoordPro, 1.000000, 255, 0, 0, 100)
				BOOL bInAngledArea = IS_ENTITY_IN_ANGLED_AREA(LocalPlayerPed, vAACoordOb, vAACoordPro, 1.200000)
			
				IF NOT IS_BIT_SET(iLocalBoolCheck2,LBOOL2_IN_RANGE_FOR_SAFE_CRACK)
					CPRINTLN(DEBUG_MISSION, "Calling LBOOL2_IN_RANGE_FOR_SAFE_CRACK ot set... ")
					
					IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed,tempObj) < 3.0
					AND NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
						IF bInAngledArea
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SC_START")
								CLEAR_BIT(SafeCrackData.iBitSet, SC_START_HELP_DISPLAYED)
							ENDIF							
							
							IF NOT IS_BIT_SET(iLocalBoolCheck5, LBOOL5_SAFE_CRACK_DATA_INITIALISED)
								IF CREATE_SAFE_CRACK_SEQUENCES(SafeCrackData, tempObj)
									SETUP_SAFE_DATA(tempObj)
									PRINTLN("SETUP_SAFE_DATA called")
									
									SET_BIT(iLocalBoolCheck5, LBOOL5_SAFE_CRACK_DATA_INITIALISED)
								ENDIF
							ELSE
								SET_BIT(iLocalBoolCheck2, LBOOL2_IN_RANGE_FOR_SAFE_CRACK)
							ENDIF
						ELSE
							CLEAR_BIT(SafeCrackData.iBitSet, SC_START_HELP_DISPLAYED)
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SC_START")
								CLEAR_HELP()
							ENDIF
						ENDIF
					ENDIF
				ELSE				
					IF bInAngledArea
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SC_START")
							CLEAR_BIT(SafeCrackData.iBitSet, SC_START_HELP_DISPLAYED)
						ENDIF
						
						CPRINTLN(DEBUG_MISSION, "Calling PROCESS_PLAYER_SAFE_CRACKING - (1)")
						PROCESS_PLAYER_SAFE_CRACKING(SafeCrackData, tempObj, MC_playerBD[iPartToUse].iSafeSyncSceneID[iobj])
						IF IS_PLAYER_SAFE_CRACKING(SafeCrackData)
							PRINTLN("IS_PLAYER_SAFE_CRACKING TRUE")
							MC_playerBD[iPartToUse].iObjHacking = iobj
						ENDIF
					ELSE
						CLEAR_BIT(iLocalBoolCheck2, LBOOL2_IN_RANGE_FOR_SAFE_CRACK)						
						CLEAR_BIT(SafeCrackData.iBitSet, SC_START_HELP_DISPLAYED)
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SC_START")
							CLEAR_HELP()
						ENDIF
					ENDIF
				ENDIF
			ELSE
				CPRINTLN(DEBUG_MISSION, "Calling PROCESS_PLAYER_SAFE_CRACKING - (2)")
				PROCESS_PLAYER_SAFE_CRACKING(SafeCrackData, tempObj, MC_playerBD[iPartToUse].iSafeSyncSceneID[iobj])
				IF IS_BIT_SET(MC_playerBD[iPartToUse].iHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
					RESET_SAFE_CRACK(SafeCrackData, TRUE)
					CPRINTLN(DEBUG_MISSION, "PROCESS_SAFE_CRACKING_MINI_GAME - RESET_SAFE_CRACK(SafeCrackData, TRUE) MC_serverBD.iObjHackPart[iobj] = -1")
					CLEAR_SAFE_CRACK_SEQUENCES(SafeCrackData)
					CLEAR_BIT(iLocalBoolCheck2, LBOOL2_IN_RANGE_FOR_SAFE_CRACK)
					CLEAR_BIT(MC_playerBD[iPartToUse].iHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
					RESET_OBJ_HACKING_INT()
				ENDIF
			ENDIF
		ELSE
			IF MC_playerBD[iPartToUse].iObjHacking != -1
				IF MC_serverBD.iObjHackPart[iobj] = iPartToUse	
					IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iHackFailBitset, MC_playerBD[iPartToUse].iObjHacking)
						IF g_bMissionEnding
							SET_HACK_FAIL_BIT_SET(9)
							PRINTLN("[RCC MISSION]  setting safe fail bitset - mission finished: ",iPartToUse," for object: ",MC_playerBD[iPartToUse].iObjHacking)
						ENDIF
						IF NOT bPlayerToUseOK
							SET_HACK_FAIL_BIT_SET(8)
							PRINTLN("[RCC MISSION]  setting safe fail bitset - dead, part: ",iPartToUse," for object: ",MC_playerBD[iPartToUse].iObjHacking)
						ELSE
							IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed,tempObj) >= 3.0
								SET_HACK_FAIL_BIT_SET(7)
								PRINTLN("[RCC MISSION]  setting safe fail bitset - too far, part: ",iPartToUse," for object: ",MC_playerBD[iPartToUse].iObjHacking)
							ENDIF
						ENDIF
						CPRINTLN(DEBUG_MISSION, "Calling PROCESS_PLAYER_SAFE_CRACKING - (3)")
						
						PROCESS_PLAYER_SAFE_CRACKING(SafeCrackData, tempObj, MC_playerBD[iPartToUse].iSafeSyncSceneID[iobj])
						
						IF HAS_PLAYER_CRACKED_SAFE(SafeCrackData)
							MC_playerBD[iPartToUse].iObjHacked = iobj
							EXIT_AND_RESET_SAFE_CRACKING_MINI_GAME()
						ENDIF
					ENDIF
				ELSE
					// We aren't actually the one who's hacking this, it's somebody else. We need to bail
					SET_HACK_FAIL_BIT_SET(7)
					
					IF MC_playerBD[iPartToUse].iSafeSyncSceneID[iobj] > -1
						MC_playerBD[iPartToUse].iSafeSyncSceneID[iobj] = -1
					ENDIF
					 
					MC_playerBD[iPartToUse].iObjHacking = -1
					
					CLEAR_HELP()
					CLEAR_PED_TASKS(LocalPlayerPed)
					CLEAR_PED_SECONDARY_TASK(LocalPlayerPed)
					NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE, DEFAULT, TRUE)
					EXIT_AND_RESET_SAFE_CRACKING_MINI_GAME()
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC CLIENT_MANAGE_SAFE_CRACK_ANIM(INT iobj,OBJECT_INDEX tempObj)

	IF iSpectatorTarget = -1
		IF MC_playerBD[iPartToUse].iSafeSyncSceneID[iobj] = -1
			IF MC_serverBD.iObjHackPart[iobj] != -1
				IF MC_playerBD[MC_serverBD.iObjHackPart[iobj]].iSafeSyncSceneID[iobj] != -1 
					MC_playerBD[iPartToUse].iSafeSyncSceneID[iobj] = MC_playerBD[MC_serverBD.iObjHackPart[iobj]].iSafeSyncSceneID[iobj]
					PRINTLN("[RCC MISSION] CLIENT_MANAGE_SAFE_CRACK_ANIM - iSafeSyncSceneID[", iobj, "] = ", MC_playerBD[iPartToUse].iSafeSyncSceneID[iobj])
				ENDIF
			ENDIF
		ELSE
			MAINTAIN_PASSED_SAFE_CRACK_ANIM(tempObj, MC_playerBD[iPartToUse].iSafeSyncSceneID[iobj])
			
		ENDIF
	ENDIF

ENDPROC



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: OPEN CASE MINIGAME (used in the canned Booty Call mission) !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************




PROC PROCESS_OPEN_CASE(OBJECT_INDEX tempObj, INT iobj)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn != HEI_P_ATTACHE_CASE_SHUT_S
		ASSERTLN("[RCC MISSION] PROCESS_OPEN_CASE - Trying to run card clone minigame on an object which isn't the attache case")
	ENDIF
	
	IF MC_playerBD[iPartToUse].iObjHacked = -1
		
		//load animation / model of things
		IF MC_playerBD[iPartToUse].iObjNear = -1
			IF VDIST2(GET_PLAYER_COORDS(LocalPlayer),GET_ENTITY_COORDS(tempObj)) <= 25.0
				REQUEST_ANIM_DICT("anim@heists@ornate_bank@clone_card")
				REQUEST_MODEL(HEI_PROP_HEI_ID_BIO)
				REQUEST_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_CARD_SCANNER",TRUE)
				REQUEST_MODEL(PROP_PHONE_ING)
			ENDIF
			IF VDIST2(GET_PLAYER_COORDS(LocalPlayer),GET_ENTITY_COORDS(tempObj)) <= 4.0
				IF HAS_ANIM_DICT_LOADED("anim@heists@ornate_bank@clone_card")
				AND HAS_MODEL_LOADED(HEI_PROP_HEI_ID_BIO)
				AND HAS_MODEL_LOADED(PROP_PHONE_ING)
					IF REQUEST_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_CARD_SCANNER",TRUE)
						SET_OBJ_NEAR(iObj)
						
					ELIF iAudioBankFails >= 20
						PRINTLN("[RCC MISSION] PROCESS_OPEN_CASE - Audio bank DLC_MPHEIST/HEIST_CARD_SCANNER hasn't loaded after ", iAudioBankFails," attempts - giving up and moving on...")
						SET_OBJ_NEAR(iObj)
					ELSE
						iAudioBankFails++
						PRINTLN("[RCC MISSION] PROCESS_OPEN_CASE - Audio bank DLC_MPHEIST/HEIST_CARD_SCANNER hasn't loaded after ", iAudioBankFails," attempts!")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
		IF MC_serverBD.iObjHackPart[iobj] = -1
			IF MC_playerBD[iPartToUse].iObjHacking = -1
				IF bPlayerToUseOK
					IF MC_playerBD[iPartToUse].iObjNear = iObj
						IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
							IF NOT IS_PHONE_ONSCREEN()
								DISPLAY_HELP_TEXT_THIS_FRAME_FOR_MINIGAME("HUSECASE",TRUE)
								IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
									MC_playerBD[iPartToUse].iObjHacking = iobj
									CLEAR_BIT(MC_playerBD[iPartToUse].iHackFailBitset,iobj)
									CLEAR_HELP()				
									PRINTLN("[RCC MISSION] PROCESS_OPEN_CASE - Local player has started opening case: ",iobj, ", ipart: ",iPartToUse)
									iHackProgress = 0
								ENDIF
							ELSE
								IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HUSECASE")
									CLEAR_HELP()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(MC_playerBD[iPartToUse].iHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
					CLEAR_BIT(MC_playerBD[iPartToUse].iHackFailBitset,MC_playerBD[iPartToUse].iObjHacking)
					RESET_OBJ_HACKING_INT()
					CLEAR_PED_TASKS(LocalPlayerPed)
					NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
					ENABLE_INTERACTION_MENU()
					PRINTLN("[RCC MISSION] PROCESS_OPEN_CASE - Local player has failed to open case: ",iobj, ", part: ",iPartToUse)
					iHackProgress = 0
				ENDIF
			ENDIF
		ELSE
			//object is in use by someone, is it me?
			IF MC_playerBD[iPartToUse].iObjHacking != -1
				IF MC_serverBD.iObjHackPart[iobj] = iPartToUse
					//use the object!
					SWITCH iHackProgress
						CASE 0
							CLEANUP_MENU_ASSETS() //?
							DISABLE_INTERACTION_MENU()
							NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_ALLOW_PLAYER_DAMAGE|NSPC_PREVENT_EVERYBODY_BACKOFF|NSPC_LEAVE_CAMERA_CONTROL_ON|NSPC_ALLOW_PAD_SHAKE|NSPC_REENABLE_CONTROL_ON_DEATH)
							
							vGoToPoint = GET_ENTITY_COORDS(tempObj)
							vGoToPoint = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj,vHackOffset)
							
							FLOAT fGroundZTemp
							GET_GROUND_Z_FOR_3D_COORD(vGoToPoint,fGroundZTemp)
							vGoToPoint.z = fGroundZTemp 
							
							IF bLocalPlayerPedOk
								CLEAR_PED_TASKS(LocalPlayerPed)
								
								OPEN_SEQUENCE_TASK(temp_sequence)
									IF VDIST2(GET_PLAYER_COORDS(LocalPlayer),GET_ENTITY_COORDS(tempObj)) > 0.25 //0.5^2
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vGoToPoint, PEDMOVE_WALK)
									ENDIF
									TASK_SWAP_WEAPON(NULL,FALSE)
									TASK_TURN_PED_TO_FACE_COORD(NULL,GET_ENTITY_COORDS(tempObj))
									//TASK_PLAY_ANIM(NULL,"anim@heists@ornate_bank@clone_card","clone_action")
								CLOSE_SEQUENCE_TASK(temp_sequence)
								
								TASK_PERFORM_SEQUENCE(LocalPlayerPed,temp_sequence)	
								CLEAR_SEQUENCE_TASK(temp_sequence)
								
							ENDIF
							
							PRINTLN("[RCC MISSION] PROCESS_OPEN_CASE - iHackProgress 0 >> 1, player start walking up to case")
							iHackProgress++
						BREAK
						
						CASE 1
							IF NOT IS_PED_PERFORMING_TASK(LocalPlayerPed, SCRIPT_TASK_PERFORM_SEQUENCE)
								
								IF CAN_REGISTER_MISSION_OBJECTS(2)
									
									MC_RESERVE_NETWORK_MISSION_OBJECTS(2)
									
									MC_playerBD[iPartToUse].iSynchSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_ENTITY_COORDS(tempObj), GET_ENTITY_ROTATION(tempObj), EULER_YXZ)
								
									NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(LocalPlayerPed, MC_playerBD[iPartToUse].iSynchSceneID, "anim@heists@ornate_bank@clone_card","clone_action", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE, RBF_PLAYER_IMPACT)
									//NETWORK_ATTACH_SYNCHRONISED_SCENE_TO_ENTITY(MC_playerBD[iPartToUse].iSynchSceneID,tempObj,-1)
									
									CREATE_NET_OBJ(MC_playerBD[iPartToUse].netMiniGameObj,HEI_PROP_HEI_ID_BIO,GET_ENTITY_COORDS(tempObj),bIsLocalPlayerHost)
									
									//Set card as invisible as it spawns in front of the case for a second there
									SET_ENTITY_VISIBLE(NET_TO_ENT(MC_playerBD[iPartToUse].netMiniGameObj),FALSE)
									
									CREATE_NET_OBJ(MC_playerBD[iPartToUse].netMiniGameObj3,PROP_PHONE_ING,GET_PLAYER_COORDS(LocalPlayer),bIsLocalPlayerHost)
									
									//Stick card in anim
									NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_ENT(MC_playerBD[iPartToUse].netMiniGameObj),MC_playerBD[iPartToUse].iSynchSceneID, "anim@heists@ornate_bank@clone_card","card_clone_action", SLOW_BLEND_IN, SLOW_BLEND_OUT)
									
									NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(tempObj,MC_playerBD[iPartToUse].iSynchSceneID, "anim@heists@ornate_bank@clone_card","case_clone_action", SLOW_BLEND_IN, SLOW_BLEND_OUT)
									
									//Attach phone to hand:
									ATTACH_ENTITY_TO_ENTITY(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj3),LocalPlayerPed,GET_PED_BONE_INDEX(LocalPlayerPed,BONETAG_PH_L_HAND),<<0,0,0>>,<<0,0,0>>)
									SET_ENTITY_INVINCIBLE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj3), TRUE)
									SET_ENTITY_VISIBLE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj3),FALSE)
									
									NETWORK_START_SYNCHRONISED_SCENE(MC_playerBD[iPartToUse].iSynchSceneID)
									
									//To prevent camera jump:
									ciWarpCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE) 
									SET_CAM_ACTIVE(ciWarpCam, TRUE)
									SET_CAM_PARAMS(ciWarpCam, GET_FINAL_RENDERED_CAM_COORD(), GET_FINAL_RENDERED_CAM_ROT(), GET_FINAL_RENDERED_CAM_FOV())
									RENDER_SCRIPT_CAMS(TRUE, FALSE)
									
									PRINTLN("[RCC MISSION] PROCESS_OPEN_CASE - iHackProgress 1 >> 2, beginning synch scene")
									iHackProgress++
								ENDIF
							ENDIF
						BREAK
						
						CASE 2
							
							IF GET_SCRIPT_TASK_STATUS(LocalPlayerPed,SCRIPT_TASK_SYNCHRONIZED_SCENE) = PERFORMING_TASK
								
								//Smooth transition from old camera to new camera angle for player beginning animation
								IF DOES_CAM_EXIST(ciWarpCam)
									IF IS_CAM_ACTIVE(ciWarpCam)
										SET_CAM_ACTIVE(ciWarpCam, FALSE)
									ENDIF
									DESTROY_CAM(ciWarpCam)
									STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP(TRUE)
								ENDIF
								
								IF HAS_ANIM_EVENT_FIRED(LocalPlayerPed,GET_HASH_KEY("PHONE_APPEAR"))
									SET_ENTITY_VISIBLE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj3),TRUE)
									SET_ENTITY_VISIBLE(NET_TO_ENT(MC_playerBD[iPartToUse].netMiniGameObj),TRUE)
									PRINTLN("[RCC MISSION] PROCESS_OPEN_CASE - iHackProgress 2 >> 3, phone appearing")
									iHackProgress++
								ENDIF
							ENDIF
						
						BREAK
						
						CASE 3
						
							IF GET_SCRIPT_TASK_STATUS(LocalPlayerPed,SCRIPT_TASK_SYNCHRONIZED_SCENE) = PERFORMING_TASK
								IF HAS_ANIM_EVENT_FIRED(LocalPlayerPed,GET_HASH_KEY("PHONE_DISAPPEAR"))
									SET_ENTITY_VISIBLE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj3),FALSE)
									PRINTLN("[RCC MISSION] PROCESS_OPEN_CASE - iHackProgress 2 >> 3, phone disappearing")
									iHackProgress++
								ENDIF
							ENDIF
						BREAK
						
						CASE 4
							IF bLocalPlayerPedOk
								
								IF NOT IS_PED_PERFORMING_TASK(LocalPlayerPed, SCRIPT_TASK_SYNCHRONIZED_SCENE)
									
									DELETE_NET_ID(MC_playerBD[iPartToUse].netMiniGameObj)
									DELETE_NET_ID(MC_playerBD[iPartToUse].netMiniGameObj3)
									
									SET_MODEL_AS_NO_LONGER_NEEDED(HEI_PROP_HEI_ID_BIO)
									SET_MODEL_AS_NO_LONGER_NEEDED(PROP_PHONE_ING)
									
									//Won't need this line once the new model is used (the old model, P_ATTACHE_CASE_01_S, is an open case)
									//PLAY_ENTITY_ANIM(tempObj,"case_clone_action","anim@heists@ornate_bank@clone_card", NORMAL_BLEND_IN, FALSE,TRUE,FALSE,0.999)
									
									REMOVE_ANIM_DICT("anim@heists@ornate_bank@clone_card")
									
									RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_CARD_SCANNER")
									iAudioBankFails = 0
									
									MC_RESERVE_NETWORK_MISSION_OBJECTS(-2)
									
									ENABLE_INTERACTION_MENU()
									CLEANUP_MENU_ASSETS() //?
									MC_playerBD[iPartToUse].iObjHacked = iobj
									RESET_OBJ_HACKING_INT()
									NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
									
									PRINTLN("[RCC MISSION] PROCESS_OPEN_CASE - iHackProgress 4 >> 0, local player has finished opening case: ", iobj)
									iHackProgress = 0
								ENDIF
								
							ENDIF
						BREAK
						
					ENDSWITCH
					
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: SYNC LOCK MINIGAME !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************



CONST_INT bs_LockSyncActivated 0
CONST_INT bs_ShouldPlayFail 1
CONST_INT bs_ShouldPlayPass 2

INT iHumaneLockSyncBit

PROC RESET_HUMANE_SYNC_LOCK(INT iObj, BOOL bFullCleanup = TRUE)

	PRINTLN("[HumaneSyncLock][RCC MISSION][AW_MISSION] RESET_HUMANE_SYNC_LOCK | iObj: ", iObj, " / bFullCleanup: ", bFullCleanup)
	DEBUG_PRINTCALLSTACK()
	
	IF HAS_THIS_ADDITIONAL_TEXT_LOADED("MC_PLAY", MISSION_TEXT_SLOT)
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HPSLOCK")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HQPSLOCK")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("H1_HSL")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("H2_HSL")
			PRINTLN("[HumaneSyncLock][RCC MISSION] - RESET_HUMANE_SYNC_LOCK - HELP TEXT CLEARED")
			CLEAR_HELP(TRUE)
		ENDIF
		iSyncLockCount[MC_playerBD[iPartToUse].iteam] = 0
		CLEAR_BIT(iLocalBoolCheck3, LBOOL3_ORIGINAL_SYNC_LOCK_PLAYER)
		CLEAR_BIT(iLocalBoolCheck3, LBOOL3_USING_SYNC_LOCK)
		PRINTLN("[HumaneSyncLock][RCC MISSION] - RESET_HUMANE_SYNC_LOCK")
		RESET_NET_TIMER(tdSyncTimer[MC_playerBD[iPartToUse].iteam])
	ENDIF
	
	IF bFullCleanup
		IF NOT IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
			STOP_AUDIO_SCENE("DLC_HEIST_MINIGAME_BIOLAB_KEYCARD_SCENE") 
		ENDIF
		
		SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed, TRUE, FALSE)
		
		IF MC_playerBD[iPartToUse].iObjHacking = iObj 
			MC_playerBD[iPartToUse].iObjHacking = -1
			PRINTLN("[HumaneSyncLock][RCC MISSION][AW_MISSION] RESET_HUMANE_SYNC_LOCK | Resetting iObjHacking")
		ENDIF
	ENDIF
ENDPROC

PROC CLEAN_UP_SYNC_LOCK_OBJECTS(BOOL bForceCleanUp, BOOL bCutsceneCleanUp = FALSE)

	CLEAR_BIT(iHumaneLockSyncBit,bs_ShouldPlayFail)
	CLEAR_BIT(iHumaneLockSyncBit,bs_ShouldPlayPass)
	CLEAR_BIT(iHumaneLockSyncBit,bs_LockSyncActivated)
	
	IF NOT bCutsceneCleanUp
	AND NOT IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
		SET_PED_USING_ACTION_MODE(LocalPlayerPed, TRUE)
	ENDIF

	IF NOT bForceCleanUp
		IF IS_PAST_SYNC_SCENE_PHASE(MC_playerBD[iPartToUse].iSynchSceneID,0.99)
			IF NOT bCutsceneCleanUp
				SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed, TRUE)
			ENDIF
			DELETE_KEYCARD_OBJECT()
			PRINTLN("[HumaneSyncLock][AW_MISSION] - [RCC MISSION] CLEAN_UP_SYNC_LOCK_OBJECTS ")
		ENDIF
	ELSE
		IF NOT bCutsceneCleanUp
			SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed, TRUE)
		ENDIF
		PRINTLN("[HumaneSyncLock][AW_MISSION] - [RCC MISSION] Forcing cleanup of Sync Lock objects. ")
		DELETE_KEYCARD_OBJECT()
	ENDIF

ENDPROC

FUNC SYNC_LOCK_TYPE GET_OBJ_SYNC_LOCK_TYPE(INT iObj)

	IF IS_THIS_MODEL_A_CASINO_FINGERPRINT_SCANNER(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn)
		RETURN SYNC_LOCK_TYPE__CASINO_KEYPAD
	ENDIF

	RETURN SYNC_LOCK_TYPE__ORIGINAL
ENDFUNC

FUNC MODEL_NAMES GET_MODEL_NAME_FOR_HUMANE_SYNC_LOCK_CARD(INT iObj)
	IF GET_OBJ_SYNC_LOCK_TYPE(iObj) = SYNC_LOCK_TYPE__ORIGINAL
		RETURN HEI_PROP_HEI_ID_BIO
		
	ELIF GET_OBJ_SYNC_LOCK_TYPE(iObj) = SYNC_LOCK_TYPE__CASINO_KEYPAD
		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Swipe_Card_01d"))
		
	ENDIF
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC BOOL CREATE_KEYCARD_OBJECT(INT iObj, BOOL bLeft = FALSE,BOOL bCreateVisible = TRUE)
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_playerBD[iPartToUse].netMiniGameObj)
		REQUEST_MODEL(GET_MODEL_NAME_FOR_HUMANE_SYNC_LOCK_CARD(iObj))
		IF HAS_MODEL_LOADED(GET_MODEL_NAME_FOR_HUMANE_SYNC_LOCK_CARD(iObj))
			IF NOT IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_KEYCARD)
				MC_RESERVE_NETWORK_MISSION_OBJECTS(1)
				SET_BIT(iLocalBoolCheck12,LBOOL12_OBJ_RES_KEYCARD)
			ENDIF
			IF IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_KEYCARD)
				IF CAN_REGISTER_MISSION_OBJECTS(1)
					IF CREATE_NET_OBJ(MC_playerBD[iPartToUse].netMiniGameObj, GET_MODEL_NAME_FOR_HUMANE_SYNC_LOCK_CARD(iObj), GET_ENTITY_COORDS(LocalPlayerPed), bIsLocalPlayerHost)
						SET_ENTITY_COLLISION(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj), FALSE)
						IF bLeft
							ATTACH_ENTITY_TO_ENTITY(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj),LocalPlayerPed,GET_PED_BONE_INDEX(LocalPlayerPed,BONETAG_PH_L_HAND),<<0,0,0>>,<<0,0,0>>)
						ELSE
							ATTACH_ENTITY_TO_ENTITY(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj),LocalPlayerPed,GET_PED_BONE_INDEX(LocalPlayerPed,BONETAG_PH_R_HAND),<<0,0,0>>,<<0,0,0>>)
						ENDIF
						
						IF GET_OBJ_SYNC_LOCK_TYPE(iObj) = SYNC_LOCK_TYPE__CASINO_KEYPAD
							SET_ENTITY_VISIBLE(NET_TO_OBJ(MC_playerBD[iLocalPart].netMiniGameObj), FALSE)
						ENDIF
						
						SET_ENTITY_INVINCIBLE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj), TRUE)
						SET_MODEL_AS_NO_LONGER_NEEDED(GET_MODEL_NAME_FOR_HUMANE_SYNC_LOCK_CARD(iObj))
						SET_ENTITY_VISIBLE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj),bCreateVisible)
						CLEAR_BIT(iLocalBoolCheck12, LBOOL12_OBJ_RES_KEYCARD)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: SYNC LOCK ANIMATIONS !
//
//************************************************************************************************************************************************************


//Lock Synch anims
//"anim@heists@keycard@"
//"idle_a"
//"enter"
//"exit"

FUNC STRING GET_INTRO_ANIM_NAME(BOOL bLeftSide, INT iTimes,BOOL bReturnKeyCard = FALSE)
	
	STRING sThisString
	
	IF bLeftSide
		IF iTimes > 1
			IF bReturnKeyCard
				sThisString = "ped_a_intro_b_keycard"
			ELSE
				sThisString = "ped_a_intro_b"
			ENDIF
		ELSE
			IF bReturnKeyCard
				sThisString = "ped_a_intro_b_keycard"
				//sThisString = "ped_a_intro_a_keycard"
			ELSE
				sThisString = "ped_a_intro_b"
				//sThisString = "ped_a_intro_a"
			ENDIF
		ENDIF
	ELSE
		IF iTimes > 1
			IF bReturnKeyCard
				sThisString = "ped_b_intro_b_keycard"
			ELSE
				sThisString = "ped_b_intro_b"
			ENDIF
		ELSE
			IF bReturnKeyCard
				sThisString = "ped_b_intro_b_keycard"
				//sThisString = "ped_b_intro_a_keycard"
			ELSE
				sThisString = "ped_b_intro_b"
				//sThisString = "ped_b_intro_a"
			ENDIF
		ENDIF
	ENDIF
	
	PRINTLN("[AW_MISSION]GET_INTRO_ANIM_NAME bLeftSide-  ",bLeftSide)
	PRINTLN("[AW_MISSION]GET_INTRO_ANIM_NAME iTimes-  ",iTimes)
	PRINTLN("[AW_MISSION]GET_INTRO_ANIM_NAME picked -  ",sThisString)
	RETURN sThisString
	
ENDFUNC

FUNC STRING GET_EXIT_ANIM_NAME(BOOL bLeftSide,BOOL bReturnKeyCard = FALSE)
	
	STRING sThisString
	
	IF bLeftSide
		IF bReturnKeyCard
			sThisString = "ped_a_exit_keycard"
		ELSE
			sThisString = "ped_a_exit"
		ENDIF
	ELSE
		IF bReturnKeyCard
			sThisString = "ped_b_exit_keycard"
		ELSE
			sThisString = "ped_b_exit"
		ENDIF
	ENDIF
	
	PRINTLN("[AW_MISSION]GET_EXIT_ANIM_NAME bLeftSide-  ",bLeftSide)
	PRINTLN("[AW_MISSION] GET_EXIT_ANIM_NAME picked -  ",sThisString)
	RETURN sThisString
	
ENDFUNC

FUNC STRING GET_ENTER_ANIM_NAME(BOOL bLeftSide,BOOL bReturnKeyCard = FALSE)
	
	STRING sThisString
	
	IF bLeftSide
		IF bReturnKeyCard
			sThisString = "ped_a_enter_keycard"
		ELSE
			sThisString = "ped_a_enter"
		ENDIF
	ELSE
		IF bReturnKeyCard
			sThisString = "ped_b_enter_keycard"
		ELSE
			sThisString = "ped_b_enter"
		ENDIF
	ENDIF
	
	PRINTLN("[AW_MISSION]GET_ENTER_ANIM_NAME bLeftSide-  ",bLeftSide)
	PRINTLN("[AW_MISSION] GET_ENTER_ANIM_NAME picked -  ",sThisString)
	RETURN sThisString
	
ENDFUNC

FUNC STRING GET_ENTER_LOOP_ANIM_NAME(BOOL bLeftSide,BOOL bReturnKeyCard = FALSE)
	
	STRING sThisString
	
	IF bLeftSide
		IF bReturnKeyCard
			sThisString = "ped_a_enter_loop_keycard"
		ELSE
			sThisString = "ped_a_enter_loop"
		ENDIF
	ELSE
		IF bReturnKeyCard
			sThisString = "ped_b_enter_loop_keycard"
		ELSE
			sThisString = "ped_b_enter_loop"
		ENDIF
	ENDIF
	
	PRINTLN("[AW_MISSION]GET_ENTER_LOOP_ANIM_NAME bLeftSide-  ",bLeftSide)
	PRINTLN("[AW_MISSION]GET_ENTER_LOOP_ANIM_NAME picked -  ",sThisString)
	RETURN sThisString
	
ENDFUNC

FUNC STRING GET_LOOP_ANIM_NAME(BOOL bLeftSide,BOOL bReturnKeyCard = FALSE)
	
	STRING sThisString
	
	IF bLeftSide
		IF bReturnKeyCard
			sThisString = "ped_a_loop_keycard"
		ELSE
			sThisString = "ped_a_loop"
		ENDIF
	ELSE
		IF bReturnKeyCard
			sThisString = "ped_b_loop_keycard"
		ELSE
			sThisString = "ped_b_loop"
		ENDIF
	ENDIF
	
	PRINTLN("[AW_MISSION]GET_LOOP_ANIM_NAME bLeftSide-  ",bLeftSide)
	PRINTLN("[AW_MISSION] GET_LOOP_ANIM_NAME picked -  ",sThisString)
	RETURN sThisString
	
ENDFUNC

FUNC STRING GET_FAIL_ANIM_NAME(BOOL bLeftSide, INT iTimes,BOOL bReturnKeyCard = FALSE)
	
	STRING sThisString
	
	IF bLeftSide
		IF iTimes = 0
			IF bReturnKeyCard
				sThisString = "ped_a_fail_c_keycard"
			ELSE
				sThisString = "ped_a_fail_c"
			ENDIF
		ELIF iTimes = 1
			IF bReturnKeyCard
				sThisString = "ped_a_fail_d_keycard"
			ELSE
				sThisString = "ped_a_fail_d"
			ENDIF
		ELIF iTimes > 1
			IF iTimes % 2 = 0
				IF bReturnKeyCard
					sThisString = "ped_a_fail_c_keycard"
				ELSE
					sThisString = "ped_a_fail_c"
				ENDIF
			ELSE
				IF bReturnKeyCard
					sThisString = "ped_a_fail_d_keycard"
				ELSE
					sThisString = "ped_a_fail_d"
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF iTimes = 0
			IF bReturnKeyCard
				sThisString = "ped_b_fail_a_keycard"
			ELSE
				sThisString = "ped_b_fail_a"
			ENDIF
		ELIF iTimes = 1
			IF bReturnKeyCard
				sThisString = "ped_b_fail_b_keycard"
			ELSE
				sThisString = "ped_b_fail_b"
			ENDIF
		ELIF iTimes > 1
			IF iTimes / 2 = 0
				IF bReturnKeyCard
					sThisString = "ped_b_fail_a_keycard"
				ELSE
					sThisString = "ped_b_fail_a"
				ENDIF
			ELSE
				IF bReturnKeyCard
					sThisString = "ped_b_fail_b_keycard"
				ELSE
					sThisString = "ped_b_fail_b"
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	PRINTLN("[AW_MISSION]GET_FAIL_ANIM_NAME bLeftSide-  ",bLeftSide)
	PRINTLN("[AW_MISSION]GET_FAIL_ANIM_NAME iTimes-  ",iTimes)
	PRINTLN("[AW_MISSION] GET_FAIL_ANIM_NAME picked -  ",sThisString)
	RETURN sThisString

ENDFUNC

FUNC STRING GET_PASS_ANIM_NAME(BOOL bLeftSide,BOOL bReturnKeyCard = FALSE)
	
	STRING sThisString
	
	IF bLeftSide
		IF bReturnKeyCard
			sThisString = "ped_a_pass_keycard"
		ELSE
			sThisString = "ped_a_pass"
		ENDIF
	ELSE
		IF bReturnKeyCard
			sThisString = "ped_b_pass_keycard"
		ELSE
			sThisString = "ped_b_pass"
		ENDIF
	ENDIF
	
	PRINTLN("[AW_MISSION]GET_PASS_ANIM_NAME bLeftSide-  ",bLeftSide)
	PRINTLN("[AW_MISSION]GET_PASS_ANIM_NAME picked -  ",sThisString)
	RETURN sThisString

ENDFUNC

FUNC STRING GET_DICTIONARY_NAME_FOR_HUMANE_LABS(INT iObj)
	STRING sThisString
	
	IF GET_OBJ_SYNC_LOCK_TYPE(iObj) = SYNC_LOCK_TYPE__ORIGINAL
		IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
			sThisString = "anim@heists@humane_labs@finale@keycards_heels"
		ELSE
			sThisString = "anim@heists@humane_labs@finale@keycards"
		ENDIF
		
	ELIF GET_OBJ_SYNC_LOCK_TYPE(iObj) = SYNC_LOCK_TYPE__CASINO_KEYPAD
		sThisString = "ANIM_HEIST@HS3F@IG3_CARDSWIPE_INSYNC@MALE@"
		
	ENDIF
	
	RETURN sThisString
ENDFUNC

PROC CLEANUP_HUMANE_LOCK_SYNC_MINIGAME(INT iThisObj)
	
	PRINTLN("[HumaneSyncLock] CLEANUP_HUMANE_LOCK_SYNC_MINIGAME!")
	DEBUG_PRINTCALLSTACK()
	
	PRINTLN("[HumaneSyncLock] [RCC MISSION][AW_MISSION]  local player has backed out of sync locked obj: ",iThisObj, " ipart: ",iPartToUse)
	PRINTLN("[HumaneSyncLock] [AW_MISSION] iThisLockSyncProgress = -1 ")
	RESET_HUMANE_SYNC_LOCK(iThisObj)
	SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed,TRUE,FALSE)
	NET_SET_PLAYER_CONTROL(LocalPlayer,TRUE)

	IF NOT IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
		RELEASE_SCRIPT_AUDIO_BANK()
		PRINTLN("[HumaneSyncLock] RELEASE_SCRIPT_AUDIO_BANK - Called from fm_mission_controller_minigame.sch 11")
	ENDIF

	CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_LOCK_SYNC_STARTED)
	CLEAR_BIT(iHumaneLockSyncBit,bs_LockSyncActivated)
	CLEAN_UP_SYNC_LOCK_OBJECTS(TRUE)
	SET_SYNC_LOCK_PROGRESS(-1)
	CLEAR_PED_TASKS(LocalPlayerPed)
	RUN_LOCAL_PLAYER_PROOF_SETTING(TRUE) 
	CLEAR_HELP()
ENDPROC

//Variables here moved to FM_Mission_Controller_USING header - search for ci_HUMANE_SYNC_LOCK_TIME

PROC MANAGE_PLAYER_HUMANE_LOCK_SYNC_ANIMS(OBJECT_INDEX tempObj, INT iThisObj)
	
	//PRINTLN("[HumaneSyncLock] vHumaneSyncPlayback.z : " , vHumaneSyncPlayback.z)
	//PRINTLN("[HumaneSyncLock] fZAmount : " , fZAmount)
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_UP)
		fZAmount = fZAmount + 0.001
	ENDIF
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_DOWN)
		fZAmount = fZAmount - 0.001
	ENDIF
	
	
	VECTOR vGoTo
	
	VECTOR vPlayerCord
	
	//Allows the player to backout
	IF MC_playerBD[iPartToUse].iSyncAnimProgress = 5
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
		OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
		OR (MC_serverBD.iObjHackPart[iThisObj] > -1	AND MC_serverBD.iObjHackPart[iThisObj] != iLocalPart)
			sHSLExit = GET_EXIT_ANIM_NAME(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iThisObj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_RIGHT_LEFT_POSITIONING ))
			sKeyCardAnim = GET_EXIT_ANIM_NAME(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iThisObj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_RIGHT_LEFT_POSITIONING ),TRUE)
			TASK_PLAY_ANIM(LocalPlayerPed,sKeyCardDict,sHSLExit ,NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_EXTRACT_INITIAL_OFFSET | AF_HIDE_WEAPON | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_ABORT_ON_WEAPON_DAMAGE| AF_HOLD_LAST_FRAME, 0 , FALSE)
			PLAY_ENTITY_ANIM(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj),sKeyCardAnim, 
													 sKeyCardDict,
													 INSTANT_BLEND_IN, FALSE, TRUE, FALSE, 0.0)
													 
			FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed)
			PRINTLN("[HumaneSyncLock] [RCC MISSION][AW_MISSION]  local player has backed out of sync locked obj: ",iThisObj, " ipart: ",iPartToUse)
			PRINTLN("[HumaneSyncLock] [AW_MISSION] iThisLockSyncProgress = 99 ")
			SET_SYNC_LOCK_PROGRESS(99)
			CLEAR_HELP()
		ENDIF
	ENDIF
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_playerBD[iLocalPart].netMiniGameObj)
		PROCESS_ANIMATED_PROP_TAGS(NET_TO_OBJ(MC_playerBD[iLocalPart].netMiniGameObj))
	ENDIF
	
	SWITCH MC_playerBD[iPartToUse].iSyncAnimProgress
		
		CASE 0
			sKeyCardDict = GET_DICTIONARY_NAME_FOR_HUMANE_LABS(iThisObj)
			IF NOT IS_STRING_NULL_OR_EMPTY(sKeyCardDict)
				REQUEST_ANIM_DICT(sKeyCardDict)
				IF HAS_ANIM_DICT_LOADED(sKeyCardDict)
					IF bLocalPlayerPedOk
						IF IS_BIT_SET(iHumaneLockSyncBit,bs_LockSyncActivated)
							HANG_UP_AND_PUT_AWAY_PHONE()
							NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_PREVENT_EVERYBODY_BACKOFF | NSPC_LEAVE_CAMERA_CONTROL_ON | NSPC_ALLOW_PAD_SHAKE | NSPC_REENABLE_CONTROL_ON_DEATH | NSPC_ALLOW_PLAYER_DAMAGE)
							SEQUENCE_INDEX ThisSequenceIndex				
							CLEAR_SEQUENCE_TASK(ThisSequenceIndex)
							OPEN_SEQUENCE_TASK(ThisSequenceIndex)
								sHSLEnter = GET_ENTER_ANIM_NAME(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iThisObj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_RIGHT_LEFT_POSITIONING ))
								fObjRot = GET_ENTITY_HEADING(tempObj)
								vGoTo = GET_ANIM_INITIAL_OFFSET_POSITION(sKeyCardDict, sHSLEnter, GET_ENTITY_COORDS(tempObj), <<0.0, 0.0, fObjRot>>)
								vGoToPoint = vGoTo
								
								vPlayerCord = GET_ENTITY_COORDS(LocalPlayerPed)
								vPlayerCord.z = vGoToPoint.z
								PRINTLN("[HumaneSyncLock] [AW_MISSION] Distance between player and goto",GET_DISTANCE_BETWEEN_COORDS(vPlayerCord,vGoToPoint, FALSE))
								
								IF GET_DISTANCE_BETWEEN_COORDS(vPlayerCord, vGoTo, FALSE) > 0.1
									IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
										TASK_GO_STRAIGHT_TO_COORD(NULL, vGoTo, PEDMOVE_RUN, 4000, fObjRot, 0.1)
									ELSE
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vGoTo, PEDMOVE_RUN, 4000, 0.1, ENAV_STOP_EXACTLY, fObjRot)
									ENDIF
								ENDIF
								
								TASK_LOOK_AT_ENTITY(NULL,tempObj,2000, SLF_FAST_TURN_RATE)
							CLOSE_SEQUENCE_TASK(ThisSequenceIndex)
							TASK_PERFORM_SEQUENCE(LocalPlayerPed, ThisSequenceIndex)
							CLEAR_SEQUENCE_TASK(ThisSequenceIndex)

							PRINTLN("[HumaneSyncLock] [AW_MISSION] MANAGE_PLAYER_LOCK_SYNC_ANIMS obj: ",iThisObj, " ipart: ",iPartToUse)
							PRINTLN("[HumaneSyncLock] [AW_MISSION] iThisLockSyncProgress = 1 ")
							PRINTLN("[HumaneSyncLock] [AW_MISSION] vGoTo: ")PRINTLN(vGoTo)
							
							REQUEST_MODEL(GET_MODEL_NAME_FOR_HUMANE_SYNC_LOCK_CARD(iThisObj))
							iSafetyTimer = GET_GAME_TIMER()
							
							IF GET_OBJ_SYNC_LOCK_TYPE(iThisObj) = SYNC_LOCK_TYPE__ORIGINAL
								SET_PED_PROOFS_FOR_MINIGAME(LocalPlayerPed) 
							ENDIF
							
							INCREMENT_SYNC_LOCK_PROGRESS()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK

		CASE 1
		
			IF (MC_serverBD.iObjHackPart[iThisObj] > -1
			AND MC_serverBD.iObjHackPart[iThisObj] != iLocalPart)
				PRINTLN("[HumaneSyncLock] Cleaning up sync lock minigame because the server says someone else is hacking it")
				CLEANUP_HUMANE_LOCK_SYNC_MINIGAME(iThisObj)
			ENDIF
			
			IF GET_SCRIPT_TASK_STATUS(LocalPlayerPed,SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
			OR MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer, 6000)
				SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed,FALSE)
				HANG_UP_AND_PUT_AWAY_PHONE()
				REQUEST_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_USE_KEYPAD")
				REQUEST_ANIM_DICT(sKeyCardDict)
				PRINTLN("[HumaneSyncLock] [AW_MISSION] iThisLockSyncProgress = 2 ")
				//SET_PLAYER_CONTROL(LocalPlayer,FALSE)
				NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_PREVENT_EVERYBODY_BACKOFF|NSPC_LEAVE_CAMERA_CONTROL_ON|NSPC_ALLOW_PAD_SHAKE|NSPC_REENABLE_CONTROL_ON_DEATH|NSPC_ALLOW_PLAYER_DAMAGE)
				GET_ENTITY_COORDS(tempObj)
				
				vHumaneSyncPlayback = GET_ENTITY_COORDS(tempObj)
				
				IF NOT IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
					//vHumaneSyncPlayback.z = vHumaneSyncPlayback.z + fZAmount
					vHumaneSyncPlayback.z = 28.436001
				ENDIF
				
				PRINTLN("[HumaneSyncLock] vHumaneSyncPlayback.z : " , vHumaneSyncPlayback.z)
				
				INCREMENT_SYNC_LOCK_PROGRESS()
			ENDIF
		BREAK
		
		//Play enter anim
		CASE 2
			IF CREATE_KEYCARD_OBJECT(iThisObj, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iThisObj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_RIGHT_LEFT_POSITIONING))	
				sHSLEnter = GET_ENTER_ANIM_NAME(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iThisObj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_RIGHT_LEFT_POSITIONING ))
				sKeyCardAnim = GET_ENTER_ANIM_NAME(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iThisObj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_RIGHT_LEFT_POSITIONING ),TRUE)
				
				TASK_PLAY_ANIM_ADVANCED(LocalPlayerPed,sKeyCardDict, sHSLEnter ,vHumaneSyncPlayback, GET_ENTITY_ROTATION(tempObj), SLOW_BLEND_IN, NORMAL_BLEND_OUT,-1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_HOLD_LAST_FRAME | AF_USE_MOVER_EXTRACTION | AF_HIDE_WEAPON )
				
				PLAY_ENTITY_ANIM(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj),sKeyCardAnim,
														 sKeyCardDict, 
														 NORMAL_BLEND_IN, FALSE, TRUE, FALSE, 0.0)
				PRINTLN("[HumaneSyncLock] [AW_MISSION] iThisLockSyncProgress = 3 ")
				iSafetyTimer = GET_GAME_TIMER()
				
				CLEAR_BIT(iHumaneLockSyncBit,bs_ShouldPlayPass)
				CLEAR_BIT(iHumaneLockSyncBit,bs_ShouldPlayFail)
				
				FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed)
				INCREMENT_SYNC_LOCK_PROGRESS()
			ENDIF
		
		BREAK
		
		//Check enter anim is complete 
		CASE 3
			IF IS_ENTITY_PLAYING_ANIM(LocalPlayerPed,sKeyCardDict, sHSLEnter)
				IF GET_ENTITY_ANIM_CURRENT_TIME(LocalPlayerPed,sKeyCardDict, sHSLEnter) > 0.99
					sHSLEnterLoop = GET_ENTER_LOOP_ANIM_NAME(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iThisObj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_RIGHT_LEFT_POSITIONING ))
					sKeyCardAnim = GET_ENTER_LOOP_ANIM_NAME(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iThisObj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_RIGHT_LEFT_POSITIONING ),TRUE)
					TASK_PLAY_ANIM(LocalPlayerPed,sKeyCardDict, sHSLEnterLoop,NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1, AF_EXTRACT_INITIAL_OFFSET | AF_HIDE_WEAPON | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_ABORT_ON_WEAPON_DAMAGE | AF_LOOPING, 0 , FALSE) //AIK_DISABLE_LEG_IK
					//AF_EXTRACT_INITIAL_OFFSET |
					PLAY_ENTITY_ANIM(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj),sKeyCardAnim,
															 sKeyCardDict, 
															 INSTANT_BLEND_IN, TRUE, FALSE, FALSE, 0.0)
					
					iSafetyTimer = GET_GAME_TIMER()
					
					CLEAR_BIT(iHumaneLockSyncBit,bs_ShouldPlayPass)
					CLEAR_BIT(iHumaneLockSyncBit,bs_ShouldPlayFail)
					
					FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed)
					PRINTLN("[HumaneSyncLock] [AW_MISSION] iThisLockSyncProgress = 5 ")
					SET_SYNC_LOCK_PROGRESS(5)
				ENDIF
			ELSE
				IF MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer, 10000)
					SET_SYNC_LOCK_PROGRESS(2)
				ENDIF
			ENDIF
		BREAK
		
		//Play the enter loop when failed
		CASE 4
			sHSLEnterLoop = GET_ENTER_LOOP_ANIM_NAME(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iThisObj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_RIGHT_LEFT_POSITIONING ))
			sKeyCardAnim = GET_ENTER_LOOP_ANIM_NAME(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iThisObj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_RIGHT_LEFT_POSITIONING ),TRUE)
			TASK_PLAY_ANIM(LocalPlayerPed,sKeyCardDict, sHSLEnterLoop,NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1, AF_EXTRACT_INITIAL_OFFSET | AF_HIDE_WEAPON | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_ABORT_ON_WEAPON_DAMAGE | AF_LOOPING, 0 , FALSE) //AIK_DISABLE_LEG_IK
			//AF_EXTRACT_INITIAL_OFFSET |
			PLAY_ENTITY_ANIM(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj),sKeyCardAnim,
													sKeyCardDict, 
													 INSTANT_BLEND_IN, TRUE, FALSE, FALSE, 0.0)
			
			iSafetyTimer = GET_GAME_TIMER()
			
			CLEAR_BIT(iHumaneLockSyncBit,bs_ShouldPlayPass)
			CLEAR_BIT(iHumaneLockSyncBit,bs_ShouldPlayFail)
			
			FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed)
			PRINTLN("[HumaneSyncLock] [AW_MISSION] iThisLockSyncProgress = 5 ")
			INCREMENT_SYNC_LOCK_PROGRESS()
		BREAK
		
		CASE 5
			//vPlayAnimAdvancedPlayback
			//<<0,0,fObjRot>>
			
			REQUEST_ANIM_DICT(sKeyCardDict)
			IF HAS_ANIM_DICT_LOADED(sKeyCardDict)
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
				OR (IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL))

					sHSLIntro = GET_INTRO_ANIM_NAME(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iThisObj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_RIGHT_LEFT_POSITIONING ), iStoreAmountOfFails)
					sKeyCardAnim = GET_INTRO_ANIM_NAME(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iThisObj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_RIGHT_LEFT_POSITIONING ), iStoreAmountOfFails,TRUE)
					TASK_PLAY_ANIM(LocalPlayerPed,sKeyCardDict, sHSLIntro,NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_EXTRACT_INITIAL_OFFSET | AF_HIDE_WEAPON | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_ABORT_ON_WEAPON_DAMAGE | AF_HOLD_LAST_FRAME, 0 , FALSE)
					PLAY_ENTITY_ANIM(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj),sKeyCardAnim,
															 sKeyCardDict, 
															 INSTANT_BLEND_IN, FALSE, TRUE, FALSE, 0.0)
					
					iSafetyTimer = GET_GAME_TIMER()
					
					CLEAR_BIT(iHumaneLockSyncBit,bs_ShouldPlayPass)
					CLEAR_BIT(iHumaneLockSyncBit,bs_ShouldPlayFail)
					
					PRINTLN("[HumaneSyncLock] [AW_MISSION] iThisLockSyncProgress = 6 ")
					INCREMENT_SYNC_LOCK_PROGRESS()
				ELSE
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("H2_HSL")
						DISPLAY_HELP_TEXT_THIS_FRAME_FOR_MINIGAME("H2_HSL",TRUE)
					ENDIF
				ENDIF
			ENDIF

		BREAK
		
			
		
		CASE 6
		
			IF IS_ENTITY_PLAYING_ANIM(LocalPlayerPed,sKeyCardDict, GET_INTRO_ANIM_NAME(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iThisObj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_RIGHT_LEFT_POSITIONING) , iStoreAmountOfFails))
				IF GET_ENTITY_ANIM_CURRENT_TIME(LocalPlayerPed,sKeyCardDict, sHSLIntro) > 0.99
					sHSLLoop = GET_LOOP_ANIM_NAME(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iThisObj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_RIGHT_LEFT_POSITIONING ))
					sKeyCardAnim = GET_LOOP_ANIM_NAME(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iThisObj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_RIGHT_LEFT_POSITIONING ),TRUE)
					TASK_PLAY_ANIM(LocalPlayerPed,sKeyCardDict, sHSLLoop,NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_EXTRACT_INITIAL_OFFSET |AF_HIDE_WEAPON | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_ABORT_ON_WEAPON_DAMAGE | AF_HOLD_LAST_FRAME, 0 , FALSE)

					PLAY_ENTITY_ANIM(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj),sKeyCardAnim,
																 sKeyCardDict, 
																 INSTANT_BLEND_IN, FALSE, TRUE, FALSE, 0.0)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed)
					
					IF MC_ServerBD.iNumObjHighestPriority[MC_playerBD[iPartToUse].iteam] > 1
						IF NOT HAS_NET_TIMER_STARTED(tdSyncTimer[MC_playerBD[iPartToUse].iteam])
							REINIT_NET_TIMER(tdSyncTimer[MC_playerBD[iPartToUse].iteam])
							SET_BIT(iLocalBoolCheck3, LBOOL3_ORIGINAL_SYNC_LOCK_PLAYER)
							PRINTLN("[HumaneSyncLock] [RCC MISSION][AW_MISSION] Setting LBOOL3_ORIGINAL_SYNC_LOCK_PLAYER")
						ENDIF
						
						BROADCAST_FMMC_START_SYNC_LOCK_HACK_TIMER(MC_playerBD[iPartToUse].iteam)
						BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_START_SYNC_LOCK,0,MC_playerBD[iPartToUse].iteam,-1,LocalPlayer,ci_TARGET_OBJECT,iThisObj)
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("H2_HSL")
							CLEAR_HELP()
						ENDIF
						PRINTLN("[HumaneSyncLock] [RCC MISSION][AW_MISSION]  local player has started hacking 1: ",iThisObj, " ipart: ",iPartToUse)
					
						SET_BIT(iLocalBoolCheck3,LBOOL3_USING_SYNC_LOCK)
						SET_BIT(iLocalBoolCheck4, LBOOL4_I_HAVE_PUSHED_SYNC_LOCK)
						SET_BIT(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_LOCK_SYNC_STARTED)
						PRINTLN("[HumaneSyncLock] [AW_MISSION] here -1")
					ELSE
						//do we have instance of only one card reader?				
					ENDIF
					
					
					iSafetyTimer = GET_GAME_TIMER()
					PRINTLN("[HumaneSyncLock] [AW_MISSION] iThisLockSyncProgress = 7 ")
					INCREMENT_SYNC_LOCK_PROGRESS()
				ENDIF
			ELSE
				PRINTLN("[HumaneSyncLock] [AW_MISSION] not playing animation breaking out")
				SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed,TRUE,FALSE)
				NET_SET_PLAYER_CONTROL(LocalPlayer,TRUE)
				CLEAN_UP_SYNC_LOCK_OBJECTS(TRUE)
				RESET_HUMANE_SYNC_LOCK(iThisObj)	
				SET_SYNC_LOCK_PROGRESS(-1)
			ENDIF 

		BREAK
		
		CASE 7
			
			IF IS_BIT_SET(iHumaneLockSyncBit,bs_ShouldPlayPass)
				sHSLPass = GET_PASS_ANIM_NAME(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iThisObj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_RIGHT_LEFT_POSITIONING ))
				sKeyCardAnim = GET_PASS_ANIM_NAME(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iThisObj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_RIGHT_LEFT_POSITIONING ),TRUE)
				TASK_PLAY_ANIM(LocalPlayerPed,sKeyCardDict, sHSLPass,NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_EXTRACT_INITIAL_OFFSET |AF_HIDE_WEAPON | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS |AF_ABORT_ON_WEAPON_DAMAGE| AF_HOLD_LAST_FRAME, 0 , FALSE, AIK_DISABLE_LEG_IK)
				PLAY_ENTITY_ANIM(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj),sKeyCardAnim,
														  sKeyCardDict,
														 INSTANT_BLEND_IN, FALSE, TRUE, FALSE, 0.0)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed)
				PRINTLN("[HumaneSyncLock] [AW_MISSION] should play pass.")
				PRINTLN("[HumaneSyncLock] [AW_MISSION] iThisLockSyncProgress = 8 ")
				INCREMENT_SYNC_LOCK_PROGRESS()
			ENDIF
			
			//Pass automatically if there is only one swipe card
			IF MC_ServerBD.iNumObjHighestPriority[MC_playerBD[iPartToUse].iteam] > 1
			
			ELSE
				PRINTLN("[HumaneSyncLock] [AW_MISSION]  local player has sync locked obj: ",iThisObj, " ipart: ",iPartToUse)
				PRINTLN("[HumaneSyncLock] [AW_MISSION] here -2")
				MC_playerBD[iPartToUse].iObjHacked = iThisObj
				RESET_HUMANE_SYNC_LOCK(iThisObj)
			ENDIF
			
			IF MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer, 8000)
			OR GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdSyncTimer[MC_playerBD[iPartToUse].iteam]) > ci_HUMANE_SYNC_LOCK_TIME * 4
				SET_BIT(iHumaneLockSyncBit, bs_ShouldPlayFail)
				PRINTLN("[HumaneSyncLock] [AW_MISSION] Fail safe timer exceeded, playing fail. ")
			ENDIF
			
			IF IS_ENTITY_PLAYING_ANIM(LocalPlayerPed,sKeyCardDict, sHSLLoop)
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdSyncTimer[MC_playerBD[iPartToUse].iteam]) > ci_HUMANE_SYNC_LOCK_TIME
					IF IS_BIT_SET(iHumaneLockSyncBit,bs_ShouldPlayFail)
						sHSLFail = GET_FAIL_ANIM_NAME(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iThisObj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_RIGHT_LEFT_POSITIONING ),iStoreAmountOfFails)
						sKeyCardAnim = GET_FAIL_ANIM_NAME(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iThisObj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_RIGHT_LEFT_POSITIONING ),iStoreAmountOfFails,TRUE)
						TASK_PLAY_ANIM(LocalPlayerPed,sKeyCardDict,sHSLFail ,NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_EXTRACT_INITIAL_OFFSET |AF_HIDE_WEAPON | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_ABORT_ON_WEAPON_DAMAGE| AF_HOLD_LAST_FRAME, 0 , FALSE, AIK_DISABLE_LEG_IK)
						PLAY_ENTITY_ANIM(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj),sKeyCardAnim, 
																 sKeyCardDict,
																 INSTANT_BLEND_IN, FALSE, TRUE, FALSE, 0.0)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed)
						PRINTLN("[HumaneSyncLock] [AW_MISSION] should play fail.")
					
						PRINTLN("[HumaneSyncLock] [AW_MISSION] iThisLockSyncProgress = 8 ")
						iSafetyTimer = GET_GAME_TIMER()
						INCREMENT_SYNC_LOCK_PROGRESS()

						//MC_playerBD[iPartToUse].bLockSyncStarted = FALSE
					ENDIF
				ENDIF			
			ELSE
				PRINTLN("[HumaneSyncLock] [AW_MISSION] not playing animation breaking out")
				SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed,TRUE,FALSE)
				NET_SET_PLAYER_CONTROL(LocalPlayer,TRUE)
				CLEAN_UP_SYNC_LOCK_OBJECTS(TRUE)
				RESET_HUMANE_SYNC_LOCK(iThisObj)	
				SET_SYNC_LOCK_PROGRESS(-1)
			ENDIF
		BREAK
		
		//Wait until pass or fail anim is done
		CASE 8
			
			IF HAS_THIS_ADDITIONAL_TEXT_LOADED("MC_PLAY", MISSION_TEXT_SLOT)
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HPSLOCK")
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HQPSLOCK")
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("H1_HSL")
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("H2_HSL")
					CLEAR_HELP(TRUE)
				ENDIF
			ENDIF
			
			IF MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer,6000)
				PRINTLN("[HumaneSyncLock] [AW_MISSION] Saftey Timer Expired ")
				PRINTLN("[HumaneSyncLock] [AW_MISSION] iThisLockSyncProgress = 6 ")
				SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed,TRUE,FALSE)
				NET_SET_PLAYER_CONTROL(LocalPlayer,TRUE)

				IF NOT IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
					RELEASE_SCRIPT_AUDIO_BANK()
					PRINTLN("[HumaneSyncLock] RELEASE_SCRIPT_AUDIO_BANK - Called from fm_mission_controller_minigame.sch 8")
				ENDIF

				SET_SYNC_LOCK_PROGRESS(-1)
			ENDIF
	
			IF IS_BIT_SET(iHumaneLockSyncBit,bs_ShouldPlayFail)
				IF IS_ENTITY_PLAYING_ANIM(LocalPlayerPed,sKeyCardDict, sHSLFail)
					IF GET_ENTITY_ANIM_CURRENT_TIME(LocalPlayerPed,sKeyCardDict, sHSLFail) > 0.99
						iStoreAmountOfFails ++
						RESET_HUMANE_SYNC_LOCK(iThisObj, FALSE)
						PRINTLN("[HumaneSyncLock] [AW_MISSION] iThisLockSyncProgress = 4 ")
						//new
						CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_LOCK_SYNC_STARTED)
						SET_SYNC_LOCK_PROGRESS(4)
					ENDIF
				ELSE
					iStoreAmountOfFails ++
					RESET_HUMANE_SYNC_LOCK(iThisObj, FALSE)
					PRINTLN("[HumaneSyncLock] [AW_MISSION] - Fail anim isn't playing")
					PRINTLN("[HumaneSyncLock] [AW_MISSION] iThisLockSyncProgress = 4 ")
					//new
					CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_LOCK_SYNC_STARTED)
					SET_SYNC_LOCK_PROGRESS(4)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(iHumaneLockSyncBit,bs_ShouldPlayPass)
				IF IS_ENTITY_PLAYING_ANIM(LocalPlayerPed,sKeyCardDict, sHSLPass)
					IF GET_ENTITY_ANIM_CURRENT_TIME(LocalPlayerPed,sKeyCardDict, sHSLPass) > 0.75
					OR HAS_ANIM_BREAKOUT_EVENT_FIRED()
						PRINTLN("[HumaneSyncLock] [RCC MISSION][AW_MISSION]  local player has sync locked obj: ",iThisObj, " ipart: ",iPartToUse)
						MC_playerBD[iPartToUse].iObjHacked = iThisObj
						START_AUDIO_SCENE("DLC_HEIST_BIOLAB_HEIST_KEYCARD_HACK_SCENE")
						CLEAN_UP_SYNC_LOCK_OBJECTS(TRUE)
						RESET_HUMANE_SYNC_LOCK(iThisObj)
						SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed,TRUE,FALSE)
						CLEAR_PED_TASKS(LocalPlayerPed)
						NET_SET_PLAYER_CONTROL(LocalPlayer,TRUE)

						IF NOT IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
							RELEASE_SCRIPT_AUDIO_BANK()
							PRINTLN("[HumaneSyncLock] RELEASE_SCRIPT_AUDIO_BANK - Called from fm_mission_controller_minigame.sch 9")	
						ENDIF

						PRINTLN("[HumaneSyncLock] [AW_MISSION] iThisLockSyncProgress = -1 ")
						//new
						CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_LOCK_SYNC_STARTED)
						RUN_LOCAL_PLAYER_PROOF_SETTING(TRUE)
						SET_SYNC_LOCK_PROGRESS(-1)
					ENDIF
				ELSE
					PRINTLN("[HumaneSyncLock] [RCC MISSION][AW_MISSION]  local player has sync locked obj: ",iThisObj, " ipart: ",iPartToUse)
					MC_playerBD[iPartToUse].iObjHacked = iThisObj
					START_AUDIO_SCENE("DLC_HEIST_BIOLAB_HEIST_KEYCARD_HACK_SCENE")
					CLEAN_UP_SYNC_LOCK_OBJECTS(TRUE)
					RESET_HUMANE_SYNC_LOCK(iThisObj)
					SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed,TRUE,FALSE)
					NET_SET_PLAYER_CONTROL(LocalPlayer,TRUE)
					CLEAR_PED_TASKS(LocalPlayerPed)

					IF NOT IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
						RELEASE_SCRIPT_AUDIO_BANK()
						PRINTLN("[HumaneSyncLock] RELEASE_SCRIPT_AUDIO_BANK - Called from fm_mission_controller_minigame.sch 10")
					ENDIF

					PRINTLN("[HumaneSyncLock] [AW_MISSION] - Pass anim isn't playing")
					PRINTLN("[HumaneSyncLock] [AW_MISSION] iThisLockSyncProgress = -1 ")
					//new
					CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_LOCK_SYNC_STARTED)
					SET_SYNC_LOCK_PROGRESS(-1)
				ENDIF
			ENDIF
		
		BREAK
		
		//Exit case
		CASE 99
			IF IS_ENTITY_PLAYING_ANIM(LocalPlayerPed,sKeyCardDict, sHSLExit)
				IF GET_ENTITY_ANIM_CURRENT_TIME(LocalPlayerPed,sKeyCardDict, sHSLExit) > 0.99
				OR HAS_ANIM_BREAKOUT_EVENT_FIRED()
					CLEANUP_HUMANE_LOCK_SYNC_MINIGAME(iThisObj)
				ENDIF
			ENDIF
		BREAK

	
		
	ENDSWITCH

ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: GENERIC SYNC LOCK FUNCTIONS !
//
//************************************************************************************************************************************************************



PROC RESET_SYNC_LOCK()
	
	IF HAS_THIS_ADDITIONAL_TEXT_LOADED("MC_PLAY", MISSION_TEXT_SLOT)
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HPSLOCK")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HQPSLOCK")
			PRINTLN("[RCC MISSION][AW_MISSION] - RESET_SYNC_LOCK - HELP TEXT CLEARED")
			CLEAR_HELP(TRUE)
		ENDIF
	ENDIF
	
	iSyncLockCount[MC_playerBD[iPartToUse].iteam] = 0
	CLEAR_BIT(iLocalBoolCheck3,LBOOL3_ORIGINAL_SYNC_LOCK_PLAYER)
	CLEAR_BIT(iLocalBoolCheck3,LBOOL3_USING_SYNC_LOCK)
	PRINTLN("[RCC MISSION][AW_MISSION] - RESET_SYNC_LOCK")
	RESET_NET_TIMER(tdSyncTimer[MC_playerBD[iPartToUse].iteam])

ENDPROC

FUNC INT RETURN_OTHER_PLAYER_PART_ID()
	
	INT ipart
	
	FOR ipart= 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1 
		IF iPartToUse !=ipart 
			IF MC_playerBD[iPartToUse].iteam = MC_playerBD[ipart].iteam 
				RETURN ipart
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN -1 
	
ENDFUNC

/// PURPOSE:
///    Checks if another player is potentially occupying a given position.
/// PARAMS:
///    vPos - The position to check.
///    fRadius - If the player is within this radius around the position then they are considered occupying it.
FUNC BOOL IS_ANOTHER_PLAYER_BLOCKING_THE_MINIGAME_OBJECT(OBJECT_INDEX obj, FLOAT fRadius)
	VECTOR vObjPos = GET_ENTITY_COORDS(obj)
	VECTOR vObjOffset1 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(obj, <<-0.3, 0.0, 0.0>>) + <<0.0, 0.0, 2.0>>
	VECTOR vObjOffset2 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(obj, <<-0.3, -1.25, 0.0>>) + <<0.0, 0.0, -1.5>>
	
	#IF IS_DEBUG_BUILD
		IF bHumaneSyncLockDebug
			DRAW_DEBUG_ANGLED_AREA(vObjOffset1, vObjOffset2, 1.0,255,0,0,100)
		ENDIF
	#ENDIF

	INT iPartLoop
	FOR iPartLoop = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		PARTICIPANT_INDEX currentParticipant = INT_TO_PARTICIPANTINDEX(iPartLoop)
	
		IF NETWORK_IS_PARTICIPANT_ACTIVE(currentParticipant)
			PLAYER_INDEX currentPlayer = NETWORK_GET_PLAYER_INDEX(currentParticipant)
		
			IF IS_NET_PLAYER_OK(NETWORK_GET_PLAYER_INDEX(currentParticipant))
			AND currentPlayer != LocalPlayer
				PED_INDEX partPed = GET_PLAYER_PED(currentPlayer)
				VECTOR vPedPos = GET_ENTITY_COORDS(partPed)
				FLOAT fDistFromPos = VDIST(vPedPos, vObjPos)
				
				IF fDistFromPos < fRadius
				AND fDistFromPos < VDIST(GET_ENTITY_COORDS(LocalPlayerPed), vObjPos)
				AND IS_ENTITY_IN_ANGLED_AREA(partPed, vObjOffset1, vObjOffset2, 0.75)
					PRINTLN("[RCC MISSION][MINIGAME][IS_ANOTHER_PLAYER_BLOCKING_POSITION] Player ", iPartLoop, " is blocking position ", vObjPos, " distance = ", fDistFromPos)
					
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_HUMANE_SYNC_LOCK_MINI_GAME(OBJECT_INDEX tempObj,INT iObj)
	
	//PRINTLN("[HumaneSyncLock] [RCC MISSION] PROCESS_HUMANE_SYNC_LOCK_MINI_GAME: Obj ",iobj, " MC_playerBD[iPartToUse].iObjHacked: ",MC_playerBD[iPartToUse].iObjHacked)

	//just included for the debug info here so commenting out.
	//IS_ANOTHER_PLAYER_BLOCKING_THE_MINIGAME_OBJECT(tempObj,1.5)
	
	IF MC_serverBD.iObjHackPart[iObj] > -1
		IF NOT IS_BIT_SET(iBusySyncLockMinigamesBS, iObj)
			SET_BIT(iBusySyncLockMinigamesBS, iObj)
			PRINTLN("[HumaneSyncLock] PROCESS_HUMANE_SYNC_LOCK_MINI_GAME | Setting ", iObj, " in iBusySyncLockMinigamesBS")
		ENDIF
	ELSE
		IF IS_BIT_SET(iBusySyncLockMinigamesBS, iObj)
			CLEAR_BIT(iBusySyncLockMinigamesBS, iObj)
			PRINTLN("[HumaneSyncLock] PROCESS_HUMANE_SYNC_LOCK_MINI_GAME | Clearing ", iObj, " in iBusySyncLockMinigamesBS")
		ENDIF
	ENDIF
	
	IF MC_playerBD[iPartToUse].iObjHacked = -1
		
		IF MC_playerBD[iPartToUse].iObjNear = -1
			IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed,tempObj) <= 5.0
				IF GET_OBJ_SYNC_LOCK_TYPE(iObj) = SYNC_LOCK_TYPE__ORIGINAL
					REQUEST_ANIM_DICT("anim@heists@humane_labs@finale@keycards")
					REQUEST_MODEL(GET_MODEL_NAME_FOR_HUMANE_SYNC_LOCK_CARD(iObj))
					
				ELIF GET_OBJ_SYNC_LOCK_TYPE(iObj) = SYNC_LOCK_TYPE__CASINO_KEYPAD
					REQUEST_ANIM_DICT("anim@heists@humane_labs@finale@keycards")
					REQUEST_MODEL(GET_MODEL_NAME_FOR_HUMANE_SYNC_LOCK_CARD(iObj))
					
				ENDIF
			ENDIF
			
			IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed,tempObj) <= 3.0
			AND (MC_serverBD.iObjHackPart[iObj] = -1 OR MC_serverBD.iObjHackPart[iObj] = iLocalPart)
				SET_OBJ_NEAR(iObj)
				//PRINTLN("[HumaneSyncLock] [RCC MISSION] PROCESS_HUMANE_SYNC_LOCK_MINI_GAME: MC_playerBD[iPartToUse].iObjNear ",MC_playerBD[iPartToUse].iObjNear)
			ELSE
				IF MC_playerBD[iPartToUse].iObjHacking = iObj 
					MC_playerBD[iPartToUse].iObjHacking = -1
					PRINTLN("[HumaneSyncLock] [RCC MISSION][AW_MISSION] (1) Clearing iObjHacking - we're not close enough any more")
				ENDIF
			ENDIF
		ELSE
			//new print not in logs
			//PRINTLN("[HumaneSyncLock] [RCC MISSION] PROCESS_HUMANE_SYNC_LOCK_MINI_GAME: iObjNear has been assigned | MC_playerBD[iPartToUse].iObjNear is ", MC_playerBD[iPartToUse].iObjNear)
		ENDIF
		
		VECTOR vAACoordOb 
		VECTOR vAACoordPro
		vAACoordOb = GET_ENTITY_COORDS(tempObj)
		vAACoordOb.z = vAACoordOb.z + (cfHumaneSyncLock_AreaHeight * 0.5)
		vAACoordPro = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj, <<0, -cfHumaneSyncLock_AreaLength, 0>>)
		vAACoordPro.z = vAACoordPro.z - (cfHumaneSyncLock_AreaHeight * 0.5)	
		
		//new print not in logs
		#IF IS_DEBUG_BUILD
			IF bHumaneSyncLockDebug
				TEXT_LABEL_63 tlSLD = ""
				IF IS_BIT_SET(iLocalBoolCheck3, LBOOL3_USING_SYNC_LOCK)	
					tlSLD += "USL"
				ENDIF
				tlSLD += " | on: "
				tlSLD += MC_playerBD[iPartToUse].iObjNear
				
				tlSLD += " | state: "
				tlSLD += MC_playerBD[iPartToUse].iSyncAnimProgress
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(tempObj, tlSLD, -0.1)
			ENDIF
			
			//VECTOR coords = GET_ENTITY_COORDS(tempObj)
			//PRINTLN("[HumaneSyncLock] [RCC MISSION][SYNC LOCK] GET_ENTITY_COORDS for sync lock object: ", coords)
		#ENDIF
		
		//IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_LOCK_SYNC_STARTED)
		IF NOT IS_BIT_SET(iHumaneLockSyncBit, bs_LockSyncActivated)
			IF MC_playerBD[iPartToUse].iObjNear = iObj
				IF NOT IS_PHONE_ONSCREEN()
					IF IS_ENTITY_IN_ANGLED_AREA( LocalPlayerPed, vAACoordOb, vAACoordPro, cfHumaneSyncLock_AreaWidth)
					
						BOOL IsMiniGameBlocked
						
						IF IS_ANOTHER_PLAYER_BLOCKING_THE_MINIGAME_OBJECT(tempObj, 1.5)
							IsMiniGameBlocked = TRUE
							PRINTLN("[HumaneSyncLock_SPAM][RCC MISSION][AW_MISSION] sync lock obj ", iObj, " blocked by IS_ANOTHER_PLAYER_BLOCKING_THE_MINIGAME_OBJECT")
						ENDIF
						
						IF MC_serverBD.iObjHackPart[iObj] > -1
						AND MC_serverBD.iObjHackPart[iObj] != iLocalPart
							IsMiniGameBlocked = TRUE
							PRINTLN("[HumaneSyncLock_SPAM][RCC MISSION][AW_MISSION] sync lock obj ", iObj, " blocked by MC_serverBD.iObjHackPart")
						ENDIF
						
						IF NOT IsMiniGameBlocked
							DISPLAY_HELP_TEXT_THIS_FRAME_FOR_MINIGAME("H1_HSL", TRUE)
						ENDIF
						
						IF MC_playerBD[iPartToUse].iObjHacking != iObj 
							MC_playerBD[iPartToUse].iObjHacking = iObj
							PRINTLN("[HumaneSyncLock] [RCC MISSION][AW_MISSION] Setting iObjHacking to ", iObj, " because we're getting prompted to use it")
						ENDIF
						
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
						AND NOT IsMiniGameBlocked 
							
							IF NOT HAS_MINIGAME_TIMER_BEEN_INITIALISED(0)
								PRINTLN("[HumaneSyncLock] [RCC MISSION] HACKING (SYNC LOCK) - INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY - SLOT 0")
								INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY(0)
								PRINTLN("[HumaneSyncLock] [RCC MISSION] HACKING (SYNC LOCK) - START MINIGAME TIMER - SLOT 0")							
								START_MINIGAME_TIMER_FOR_TELEMETRY(0)
							ENDIF
							
							PRINTLN("[HumaneSyncLock] [RCC MISSION][AW_MISSION]  local player has started to sync locked obj: ", iobj, " ipart: ",iPartToUse)
							SET_SYNC_LOCK_PROGRESS(0)
							SET_BIT(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_SYNC_LOCK_FORCE_CLEANUP)
							SET_BIT(iHumaneLockSyncBit,bs_LockSyncActivated)
							SET_PED_USING_ACTION_MODE(LocalPlayerPed, FALSE)
							MC_playerBD[iPartToUse].iSyncLockSuccessBitSet = ci_HUMANE_SYNC_LOCK__WORKING
							
							IF NOT IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
								START_AUDIO_SCENE("DLC_HEIST_MINIGAME_BIOLAB_KEYCARD_SCENE")
							ENDIF
							
						ENDIF
					ELSE
						
						PRINTLN("[HumaneSyncLock_SPAM][RCC MISSION][AW_MISSION] I'm not in the angled area for sync lock obj ", iObj)
						
						IF MC_playerBD[iPartToUse].iObjHacking = iObj 
							MC_playerBD[iPartToUse].iObjHacking = -1
							PRINTLN("[HumaneSyncLock] [RCC MISSION][AW_MISSION] (2) Clearing iObjHacking - we're not close enough any more")
						ENDIF
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("H1_HSL")
						CLEAR_HELP()
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF MC_playerBD[iPartToUse].iObjNear = iObj
				IF IS_NET_PLAYER_OK(LocalPlayer)
					
					IF MC_playerBD[iPartToUse].iObjHacking = -1
						MC_playerBD[iPartToUse].iObjHacking = iObj
						PRINTLN("[HumaneSyncLock] Setting iObjHacking to ", iObj, " because we're hacking it and it was -1")
					ENDIF
					
					DISABLE_FRONTEND_THIS_FRAME()
					PRINTLN("[HumaneSyncLock_SPAM] [AW_MISSION] MANAGE_PLAYER_LOCK_SYNC_ANIMS obj: ",iobj, " ipart: ",iPartToUse)
					MANAGE_PLAYER_HUMANE_LOCK_SYNC_ANIMS(tempObj,iobj)
				ELSE
					CLEAN_UP_SYNC_LOCK_OBJECTS(TRUE)
					RESET_HUMANE_SYNC_LOCK(iobj)
					iHumaneLockSyncBit = 0
					SET_SYNC_LOCK_PROGRESS(-1)
				ENDIF
			ENDIF
		ENDIF
		
		INT iPlayerToCheck
				
		IF IS_BIT_SET(iLocalBoolCheck3, LBOOL3_ORIGINAL_SYNC_LOCK_PLAYER)
			iPlayerToCheck = iPartToUse
		ELSE
			IF RETURN_OTHER_PLAYER_PART_ID() != -1
				iPlayerToCheck = RETURN_OTHER_PLAYER_PART_ID()
			ENDIF
		ENDIF

		IF HAS_NET_TIMER_STARTED(tdSyncTimer[MC_playerBD[iPartToUse].iteam])
			IF IS_BIT_SET(iLocalBoolCheck3, LBOOL3_USING_SYNC_LOCK)	
				IF iSyncLockCount[MC_playerBD[iPartToUse].iteam] >=MC_ServerBD.iNumObjHighestPriority[MC_playerBD[iPartToUse].iteam] 
					IF IS_BIT_SET(iLocalBoolCheck3, LBOOL3_ORIGINAL_SYNC_LOCK_PLAYER)
						IF NOT IS_BIT_SET(iHumaneLockSyncBit, bs_ShouldPlayPass)
							IF NOT HAS_NET_TIMER_STARTED(timerHumLabDoors) //Delay for doors to open 2203784
								START_NET_TIMER(timerHumLabDoors)
							ELSE
								IF HAS_NET_TIMER_EXPIRED(timerHumLabDoors, iHumLabDoorDelay)
									
									PRINTLN("[HumaneSyncLock] [RCC MISSION] HACKING (SYNC LOCK) - STORE_MINIGAME_ATTEMPTS_FOR_TELEMETRY (IN PASS LOGIC) - SLOT 0")
									PRINTLN("[HumaneSyncLock] [RCC MISSION] HACKING (SYNC LOCK) - STORE_MINIGAME_ATTEMPTS_FOR_TELEMETRY (IN PASS LOGIC) - SLOT 0")
									STORE_MINIGAME_TIMER_FOR_TELEMETRY(0)
									STORE_MINIGAME_ATTEMPTS_FOR_TELEMETRY(0)
									
									RESET_NET_TIMER(timerHumLabDoors)
								
									//Play pass anim
									SET_BIT(iHumaneLockSyncBit,bs_ShouldPlayPass)
									CLEAR_BIT(iHumaneLockSyncBit,bs_ShouldPlayFail)
									
									MC_playerBD[iPartToUse].iSyncLockSuccessBitSet = ci_HUMANE_SYNC_LOCK__SUCCESS
									
									PRINTLN("[HumaneSyncLock] [AW_MISSION] Lock sync success, playing and waiting on pass anims to finish. ")
									//SET_SCRIPT_UPDATE_DOOR_AUDIO(-1,TRUE)
									
									INT hack_door_rule
									INT hack_door_hash
									
									hack_door_rule = mc_serverbd_4.iobjpriority[iobj][mc_playerbd[iparttouse].iteam] //obtains the rule for the object / door
									
									IF hack_door_rule < FMMC_MAX_RULES
										
										MODEL_NAMES mnDoor = GET_FMMC_DOOR_MODEL(ihackdoorid[hack_door_rule], IS_BIT_SET(ilocalhackdoorthermitebitset, hack_door_rule))
										
										IF get_new_hack_door_hash(hack_door_rule, mnDoor, get_fmmc_door_coords(ihackdoorid[hack_door_rule]), hack_door_hash)
										AND mnDoor != DUMMY_MODEL_FOR_SCRIPT
											SET_SCRIPT_UPDATE_DOOR_AUDIO(hack_door_hash, true)
											PRINTLN("[HumaneSyncLock] [lk_mission] set_script_update_door_audio test 0")
										ELSE 
											PRINTLN("[HumaneSyncLock] [lk_mission] set_script_update_door_audio test 1")
										ENDIF 
									ELSE 
										PRINTLN("[HumaneSyncLock] [lk_mission] set_script_update_door_audio test 2")
									ENDIF
									
									PLAY_SOUND_FROM_ENTITY(-1,"Keycard_Success",tempObj,"DLC_HEISTS_BIOLAB_FINALE_SOUNDS",TRUE,10)
									
									IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("H2_HSL")
										CLEAR_HELP(TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						PRINTLN("[HumaneSyncLock] [AW_MISSION] Not original player?")
						
						IF RETURN_OTHER_PLAYER_PART_ID() = -1
						AND HAS_NET_TIMER_EXPIRED(tdSyncTimer[MC_playerBD[iPartToUse].iteam], 5000)
							SET_BIT(iLocalBoolCheck3, LBOOL3_ORIGINAL_SYNC_LOCK_PLAYER)
							
							MC_playerBD[iPartToUse].iSyncLockSuccessBitSet = ci_HUMANE_SYNC_LOCK__FAILED
							CLEAR_BIT(iHumaneLockSyncBit,bs_ShouldPlayPass)
							PRINTLN("[HumaneSyncLock] [AW_MISSION] Something's gone wrong and there's no original player - setting self as original player & setting us as failed")
						ENDIF
						//SET_BIT(iHumaneLockSyncBit,bs_ShouldPlayPass)
					ENDIF
				ELSE
					PRINTLN("[HumaneSyncLock_SPAM] [AW_MISSION] Else condition")

				ENDIF
				
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdSyncTimer[MC_playerBD[iPartToUse].iteam]) > ci_HUMANE_SYNC_LOCK_TIME
				
					IF IS_BIT_SET(iLocalBoolCheck3, LBOOL3_ORIGINAL_SYNC_LOCK_PLAYER)
					AND MC_playerBD[iPartToUse].iSyncLockSuccessBitSet != ci_HUMANE_SYNC_LOCK__FAILED
						
						IF NOT IS_BIT_SET(iHumaneLockSyncBit, bs_ShouldPlayPass)
							// If we haven't passed by now then we've failed
							MC_playerBD[iPartToUse].iSyncLockSuccessBitSet = ci_HUMANE_SYNC_LOCK__FAILED
							PRINTLN("[HumaneSyncLock] [RCC MISSION] HACKING (SYNC LOCK) - Setting iSyncLockSuccessBitSet to ci_HUMANE_SYNC_LOCK__FAILED")
						ENDIF
					ENDIF
					
					IF NOT IS_BIT_SET(iHumaneLockSyncBit,bs_ShouldPlayFail)
						IF NOT IS_BIT_SET(iHumaneLockSyncBit,bs_ShouldPlayPass)
							IF MC_playerBD[iPlayerToCheck].iSyncLockSuccessBitSet = ci_HUMANE_SYNC_LOCK__FAILED
								//Play fail anim
								SET_BIT(iHumaneLockSyncBit,bs_ShouldPlayFail)
								
								PRINTLN("[HumaneSyncLock] [RCC MISSION] HACKING (SYNC LOCK) - INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY - SLOT 0")
								INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY(0)
								
								//iSyncLockCount[MC_playerBD[iPartToUse].iteam] = 0
								RESET_HUMANE_SYNC_LOCK(iObj, FALSE)
								
								PRINTLN("[HumaneSyncLock] [AW_MISSION]  sync lock time expired for ipart: ",iPartToUse)
								PRINTLN("[HumaneSyncLock] [AW_MISSION] Lock sync failure, playing and waiting on fail anims to finish. ")
								PLAY_SOUND_FROM_ENTITY(-1,"Keycard_Fail",tempObj,"DLC_HEISTS_BIOLAB_FINALE_SOUNDS",TRUE,10)
								
							ELIF MC_playerBD[iPlayerToCheck].iSyncLockSuccessBitSet = ci_HUMANE_SYNC_LOCK__SUCCESS
								PRINTLN("[HumaneSyncLock] [RCC MISSION] HACKING (SYNC LOCK) - STORE_MINIGAME_ATTEMPTS_FOR_TELEMETRY (IN NOT FAIL LOGIC) - SLOT 0")
								PRINTLN("[HumaneSyncLock] [RCC MISSION] HACKING (SYNC LOCK) - STORE_MINIGAME_ATTEMPTS_FOR_TELEMETRY (IN NOT FAIL LOGIC) - SLOT 0")
								STORE_MINIGAME_TIMER_FOR_TELEMETRY(0)
								STORE_MINIGAME_ATTEMPTS_FOR_TELEMETRY(0)
								SET_BIT(iHumaneLockSyncBit,bs_ShouldPlayPass)
								CLEAR_BIT(iHumaneLockSyncBit,bs_ShouldPlayFail)
								
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
			
			IF MC_playerBD[iPartToUse].iObjNear = iObj
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdSyncTimer[MC_playerBD[iPartToUse].iteam]) <= ci_HUMANE_SYNC_LOCK_TIME
					DRAW_GENERIC_METER((ci_HUMANE_SYNC_LOCK_TIME-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdSyncTimer[MC_playerBD[iPartToUse].iteam])), ci_HUMANE_SYNC_LOCK_TIME,"LOCK_TIME")
				ENDIF
			ENDIF
		ENDIF
	ELSE
		PRINTLN("[HumaneSyncLock] [RCC MISSION] PROCESS_HUMANE_SYNC_LOCK_MINI_GAME: Not currently processing this minigame due to MC_playerBD[", iPartToUse, "].iObjHacked being ", MC_playerBD[iPartToUse].iObjHacked)
	ENDIF

ENDPROC

PROC MANAGE_PLAYER_LOCK_SYNC_ANIMS(OBJECT_INDEX tempObj, INT iThisObj)
	
	VECTOR vGoTo
	
	VECTOR vTemp1
	VECTOR vTemp2
	VECTOR vobjRot
	
	VECTOR vPlayerCord
	
	//VECTOR vTempPosition
	//VECTOR vTempRotation
	
//	VECTOR vThisOffset = <<0,-0.75,0>>
//	VECTOR vScenePlayback = <<0,-0.75,0>>
	
	// player  Z = 105.54
	// object  Z = 106.23
	
	SWITCH MC_playerBD[iPartToUse].iSyncAnimProgress
		
		CASE 0
			IF bLocalPlayerPedOk
				NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_PREVENT_EVERYBODY_BACKOFF|NSPC_LEAVE_CAMERA_CONTROL_ON|NSPC_ALLOW_PAD_SHAKE|NSPC_REENABLE_CONTROL_ON_DEATH)
				SEQUENCE_INDEX ThisSequenceIndex				
				CLEAR_SEQUENCE_TASK(ThisSequenceIndex)
				OPEN_SEQUENCE_TASK(ThisSequenceIndex)
					vGoTo = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj,vThisOffset)
					vobjRot = GET_ENTITY_ROTATION(tempObj)
					GET_GROUND_Z_FOR_3D_COORD(vGoTo,vGoTo.z)
					vPlayerCord = GET_ENTITY_COORDS(LocalPlayerPed)
					vPlayerCord.z = vGoToPoint.z
					PRINTLN("[AW_MISSION] - [RCC MISSION] Distance between player and goto ",GET_DISTANCE_BETWEEN_COORDS(vPlayerCord,vGoToPoint, FALSE))
					IF GET_DISTANCE_BETWEEN_COORDS(vPlayerCord,vGoTo, FALSE) > fUseGoToDistance
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vGoTo, PEDMOVE_WALK,  DEFAULT_TIME_BEFORE_WARP , DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT,vobjRot.z)
					ENDIF
					TASK_LOOK_AT_ENTITY(NULL, tempObj, 2000)
				CLOSE_SEQUENCE_TASK(ThisSequenceIndex)
				TASK_PERFORM_SEQUENCE(LocalPlayerPed, ThisSequenceIndex)
				CLEAR_SEQUENCE_TASK(ThisSequenceIndex)
				
				vTemp1 = GET_ENTITY_COORDS(LocalPlayerPed) 
				vTemp2 = GET_ENTITY_COORDS(tempObj) 
				vScenePlayback.z = vTemp1.z - vTemp2.z
				
				fObjRot = vobjRot.z
				
				PRINTLN("[AW_MISSION] - [RCC MISSION] iThisLockSyncProgress = 1 ")
				PRINTLN("[AW_MISSION] - [RCC MISSION] vScenePlayback: ")PRINTLN(vScenePlayback)
				PRINTLN("[AW_MISSION] - [RCC MISSION] vGoTo: ")PRINTLN(vGoTo)
				REQUEST_MODEL(GET_MODEL_NAME_FOR_HUMANE_SYNC_LOCK_CARD(iThisObj))
				iSafetyTimer = GET_GAME_TIMER()
				INCREMENT_SYNC_LOCK_PROGRESS()
			ENDIF
		BREAK

		CASE 1
			IF GET_SCRIPT_TASK_STATUS(LocalPlayerPed,SCRIPT_TASK_PERFORM_SEQUENCE)<> PERFORMING_TASK
			OR MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer,6000)
				SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed,FALSE)
				IF CREATE_KEYCARD_OBJECT(iThisObj)
					vPlayAnimAdvancedPlayback = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj,vScenePlayback)
					//vTempPosition = GET_ENTITY_COORDS(LocalPlayerPed)
					//vTempRotation = GET_ENTITY_ROTATION(LocalPlayerPed)
					REQUEST_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_USE_KEYPAD")
					REQUEST_ANIM_DICT("anim@heists@keycard@")
					PRINTLN("[AW_MISSION] - [RCC MISSION] iThisLockSyncProgress = 2 ")
					iSafetyTimer = GET_GAME_TIMER()
					INCREMENT_SYNC_LOCK_PROGRESS()
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			REQUEST_ANIM_DICT("anim@heists@keycard@")
			IF HAS_ANIM_DICT_LOADED("anim@heists@keycard@")
			OR MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer,6000)
				TASK_PLAY_ANIM_ADVANCED(LocalPlayerPed,"anim@heists@keycard@", "enter",vPlayAnimAdvancedPlayback,<<0,0,fObjRot>>,SLOW_BLEND_IN,NORMAL_BLEND_OUT,-1, AF_HIDE_WEAPON | AF_NOT_INTERRUPTABLE | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME )
				PRINTLN("[AW_MISSION] - [RCC MISSION] iThisLockSyncProgress = 3 ")
				iSafetyTimer = GET_GAME_TIMER()
				INCREMENT_SYNC_LOCK_PROGRESS()
			ENDIF
		BREAK
		
		CASE 3
			IF IS_ENTITY_PLAYING_ANIM(LocalPlayerPed,"anim@heists@keycard@", "enter")
				IF GET_ENTITY_ANIM_CURRENT_TIME(LocalPlayerPed,"anim@heists@keycard@", "enter") > 0.99
				OR MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer,6000)
					TASK_PLAY_ANIM(LocalPlayerPed,"anim@heists@keycard@", "idle_a",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_HIDE_WEAPON | AF_NOT_INTERRUPTABLE | AF_ABORT_ON_WEAPON_DAMAGE | AF_HOLD_LAST_FRAME)
					PRINTLN("[AW_MISSION] - [RCC MISSION] iThisLockSyncProgress = 4 ")
					
					IF MC_ServerBD.iNumObjHighestPriority[MC_playerBD[iPartToUse].iteam] > 1
						IF NOT HAS_NET_TIMER_STARTED(tdSyncTimer[MC_playerBD[iPartToUse].iteam])
							REINIT_NET_TIMER(tdSyncTimer[MC_playerBD[iPartToUse].iteam])
							SET_BIT(iLocalBoolCheck3,LBOOL3_ORIGINAL_SYNC_LOCK_PLAYER)
						ENDIF
						
						BROADCAST_FMMC_START_SYNC_LOCK_HACK_TIMER(MC_playerBD[iPartToUse].iteam)
						BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_START_SYNC_LOCK,0,MC_playerBD[iPartToUse].iteam,-1,NULL,ci_TARGET_OBJECT,iThisObj)
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HPSLOCK")
							CLEAR_HELP()
						ENDIF
						PRINTLN("[RCC MISSION]  local player has started hacking 1: ",iThisObj, " ipart: ",iPartToUse)
					
						SET_BIT(iLocalBoolCheck3,LBOOL3_USING_SYNC_LOCK)
						SET_BIT(iLocalBoolCheck4, LBOOL4_I_HAVE_PUSHED_SYNC_LOCK)
						PRINTLN("[AW MISSION] here -1")
					ELSE
										
					ENDIF
					
					iSafetyTimer = GET_GAME_TIMER()
					INCREMENT_SYNC_LOCK_PROGRESS()
				ENDIF
			ELSE
				PRINTLN("[AW MISSION] not playing animation breaking out")
				SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed, TRUE, FALSE)
				NET_SET_PLAYER_CONTROL(LocalPlayer,TRUE)
				CLEAN_UP_SYNC_LOCK_OBJECTS(TRUE)
				RESET_SYNC_LOCK()	
				SET_SYNC_LOCK_PROGRESS(-1)
			ENDIF 

		BREAK
		
		CASE 4
			IF IS_ENTITY_PLAYING_ANIM(LocalPlayerPed,"anim@heists@keycard@", "idle_a")
				IF GET_ENTITY_ANIM_CURRENT_TIME(LocalPlayerPed,"anim@heists@keycard@", "idle_a") > 0.45
				OR MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer,6000)
					TASK_PLAY_ANIM(LocalPlayerPed,"anim@heists@keycard@", "exit",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_HIDE_WEAPON | AF_NOT_INTERRUPTABLE | AF_ABORT_ON_WEAPON_DAMAGE)
					SET_ANIM_RATE(LocalPlayerPed,GET_RANDOM_FLOAT_IN_RANGE(0.6,1.2))
					PRINTLN("[AW_MISSION] - [RCC MISSION] iThisLockSyncProgress = 5 ")
					
					//new
					
					CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_LOCK_SYNC_STARTED)
					
					IF MC_ServerBD.iNumObjHighestPriority[MC_playerBD[iPartToUse].iteam] > 1
					
					ELSE
						PRINTLN("[RCC MISSION]  local player has sync locked obj: ",iThisObj, " ipart: ",iPartToUse)
						PRINTLN("[AW MISSION] here -2")
						MC_playerBD[iPartToUse].iObjHacked = iThisObj
						
						RESET_SYNC_LOCK()	
					ENDIF
					
					
					CLEAN_UP_SYNC_LOCK_OBJECTS(TRUE)
					SET_SYNC_LOCK_PROGRESS(0)
					MC_playerBD[iPartToUse].iObjNear =-1
					SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed,TRUE,FALSE)
					NET_SET_PLAYER_CONTROL(LocalPlayer,TRUE)
					
					IF NOT IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
						RELEASE_SCRIPT_AUDIO_BANK()
						PRINTLN("RELEASE_SCRIPT_AUDIO_BANK - Called from fm_mission_controller_minigame.sch 12")
					ENDIF

					iSafetyTimer = GET_GAME_TIMER()
					
					//MC_playerBD[iPartToUse].bLockSyncStarted = FALSE
							
				ENDIF
			ELSE
				PRINTLN("[AW MISSION] not playing animation breaking out")
				SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed,TRUE,FALSE)
				NET_SET_PLAYER_CONTROL(LocalPlayer,TRUE)
				CLEAN_UP_SYNC_LOCK_OBJECTS(TRUE)
				RESET_SYNC_LOCK()	
				SET_SYNC_LOCK_PROGRESS(-1)
			ENDIF
		BREAK
	
		
	ENDSWITCH

ENDPROC


CONST_INT ci_SYNC_LOCK_TIME 4500

PROC PROCESS_SYNC_LOCK_MINI_GAME(OBJECT_INDEX tempObj,INT iobj)
	
	PRINTLN("[RCC MISSION]  PROCESS_SYNC_LOCK_MINI_GAME: Obj ",iobj, " MC_playerBD[iPartToUse].iObjHacked: ",MC_playerBD[iPartToUse].iObjHacked)
	
	IF MC_playerBD[iPartToUse].iObjHacked = -1
		
		IF MC_playerBD[iPartToUse].iObjNear = -1
			IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed,tempObj) <= 5.0
				REQUEST_ANIM_DICT("anim@heists@keycard@")
				REQUEST_MODEL(GET_MODEL_NAME_FOR_HUMANE_SYNC_LOCK_CARD(iObj))
			ENDIF
			IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed,tempObj) <= 3.0
				SET_OBJ_NEAR(iObj)
				PRINTLN("[RCC MISSION]  PROCESS_SYNC_LOCK_MINI_GAME: MC_playerBD[iPartToUse].iObjNear ",MC_playerBD[iPartToUse].iObjNear)
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION]  PROCESS_SYNC_LOCK_MINI_GAME: iObjNear has been assigned ")
		ENDIF
		
		VECTOR vAACoordOb 
		VECTOR vAACoordPro
		vAACoordOb = GET_ENTITY_COORDS(tempObj)
		vAACoordOb.z = vAACoordOb.z + fThisTweakValue1
		
		vAACoordPro = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj,vHackAAOffset)
		vAACoordPro.z = vAACoordPro.z - fThisTweakValue2		
		
		//IF NOT MC_playerBD[iPartToUse].bLockSyncStarted 
		IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_LOCK_SYNC_STARTED)
			IF MC_playerBD[iPartToUse].iObjNear = iObj
				IF IS_ENTITY_IN_ANGLED_AREA( LocalPlayerPed, vAACoordOb, vAACoordPro, 1.200000)
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
						PRINTLN("[RCC MISSION]  local player has started to sync locked obj: ",iobj, " ipart: ",iPartToUse)
						SET_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_LOCK_SYNC_STARTED)
						//MC_playerBD[iPartToUse].bLockSyncStarted = TRUE
					ENDIF
				ENDIF
			ENDIF
			IF MC_playerBD[iPartToUse].iObjHacked != iobj
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HPSLOCK")
					PRINT_HELP_FOREVER("HPSLOCK")
				ENDIF
			ENDIF
		ELSE
			IF MC_playerBD[iPartToUse].iObjNear = iObj
				PRINTLN("[RCC MISSION] MANAGE_PLAYER_LOCK_SYNC_ANIMS obj: ",iobj, " ipart: ",iPartToUse)
				MANAGE_PLAYER_LOCK_SYNC_ANIMS(tempObj,iobj)
			ENDIF
		ENDIF

		IF HAS_NET_TIMER_STARTED(tdSyncTimer[MC_playerBD[iPartToUse].iteam])
			IF IS_BIT_SET(iLocalBoolCheck3,LBOOL3_USING_SYNC_LOCK)	
				IF iSyncLockCount[MC_playerBD[iPartToUse].iteam] >=MC_ServerBD.iNumObjHighestPriority[MC_playerBD[iPartToUse].iteam] 
					IF IS_BIT_SET(iLocalBoolCheck3,LBOOL3_ORIGINAL_SYNC_LOCK_PLAYER)
						PRINTLN("[RCC MISSION]  local player has sync locked obj: ",iobj, " ipart: ",iPartToUse)
						MC_playerBD[iPartToUse].iObjHacked = iobj
						CLEAN_UP_SYNC_LOCK_OBJECTS(TRUE)
						PLAY_SOUND_FROM_ENTITY(-1,"Keycard_Success",tempObj,"DLC_HEISTS_BIOLAB_FINALE_SOUNDS",TRUE,10)
						RESET_SYNC_LOCK()
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HPSLOCK")
							CLEAR_HELP(TRUE)
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HWSLOCK")
						PRINT_HELP_FOREVER("HWSLOCK")
					ENDIF
				ENDIF
			ENDIF
			
			IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdSyncTimer[MC_playerBD[iPartToUse].iteam]) > ci_SYNC_LOCK_TIME
				PRINTLN("[RCC MISSION]  sync lock time expired for ipart: ",iPartToUse)
				PLAY_SOUND_FROM_ENTITY(-1,"Keycard_Fail",tempObj,"DLC_HEISTS_BIOLAB_FINALE_SOUNDS",TRUE,10)
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HPSLOCK")
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HQPSLOCK")
					CLEAR_HELP(TRUE)
				ENDIF
				RESET_SYNC_LOCK()
			ENDIF
			IF MC_playerBD[iPartToUse].iObjNear = iObj
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdSyncTimer[MC_playerBD[iPartToUse].iteam]) <= ci_SYNC_LOCK_TIME
					DRAW_GENERIC_METER((ci_SYNC_LOCK_TIME-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdSyncTimer[MC_playerBD[iPartToUse].iteam])), ci_SYNC_LOCK_TIME,"LOCK_TIME")
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: PHONE HACKING / SNAKE MINIGAME !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************



FUNC BOOL SHOULD_DEBUG_LAUNCH_PHONE_HACK_MINIGAME()

	#IF IS_DEBUG_BUILD
		
		IF bLocalPlayerPedOk
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
					PRINTLN("[RCC MISSION] SHOULD_DEBUG_LAUNCH_PHONE_HACK_MINIGAME - I have pressed P, part ",iLocalPart)
					bDebugCircuitHack = TRUE
				ENDIF
				IF bDebugCircuitHack
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		
	#ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL SHOULD_FORCE_PASS_HACK_MINIGAME()
	INT iTeam = MC_playerBD[iPartToUse].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule < FMMC_MAX_RULES
		RETURN g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iProgressMinigame[iRule] = 1
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_FORCE_FAIL_HACK_MINIGAME()
	INT iTeam = MC_playerBD[iPartToUse].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule < FMMC_MAX_RULES
		RETURN g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iProgressMinigame[iRule] = 2
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC TASK_FAKE_PHONE_HACKING()
	VEHICLE_INDEX e_TempVehicle = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
	
	SWITCH e_FakePhoneState
		CASE FPPS_INIT
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			AND NOT DOES_ENTITY_EXIST(e_DefaultPlayerPhone)
				CPRINTLN(DEBUG_COLIN, "[RCC MISSION] TASK_FAKE_PHONE_HACKING - Check 1 - Getting LocalPlayerPed Vehicle")
				
				VECTOR v_TempVector
				v_TempVector = GET_PED_BONE_COORDS(GET_PED_IN_VEHICLE_SEAT(e_TempVehicle, VS_FRONT_RIGHT), BONETAG_R_HAND, <<0, 0, 0>>)
				
				CPRINTLN(DEBUG_COLIN, "[RCC MISSION] TASK_FAKE_PHONE_HACKING - Check 2 - Get Front Right Seat Ped's Right Hand Bonetag Pos")
				
				// Grab the default phone hacker ped uses/looks at during minigame
				e_DefaultPlayerPhone = GET_CLOSEST_OBJECT_OF_TYPE(v_TempVector, 0.1, PROP_PLAYER_PHONE_01, FALSE, FALSE, FALSE)
				
				CPRINTLN(DEBUG_COLIN, "[RCC MISSION] TASK_FAKE_PHONE_HACKING - Check 3 - Get Nearest Phone Object")
				
				PRINTLN("[RCC MISSION] TASK_FAKE_PHONE_HACKING - Local Player: ",  GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(LocalPlayerPed)))
				PRINTLN("[RCC MISSION] TASK_FAKE_PHONE_HACKING - Vehicle Local Player ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(LocalPlayerPed)), " is in: ", GET_MODEL_NAME_STRING((GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)))))
				PRINTLN("[RCC MISSION] TASK_FAKE_PHONE_HACKING - Player Ped in Front Right Seat is: ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(e_TempVehicle, VS_FRONT_RIGHT))))
				
				
				PRINTLN("[RCC MISSION] TASK_FAKE_PHONE_HACKING - Player in Front Right Seat, Right Hand Bonetag Coords: ", "<<", v_TempVector.x, ",", v_TempVector.y, ",", v_TempVector.z, ">>")
				
				IF e_DefaultPlayerPhone <> NULL
					PRINTLN("[RCC MISSION] TASK_FAKE_PHONE_HACKING - Check 4a - Found & Grabbed Object.")
				ELSE
					PRINTLN("[RCC MISSION] TASK_FAKE_PHONE_HACKING - Check 4b - Coundn't Find Object.")
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(e_DefaultPlayerPhone)
				CPRINTLN(DEBUG_COLIN, "[RCC MISSION] TASK_FAKE_PHONE_HACKING - Check 5 - Phone object exists")
				
				// Check default pohone object is invisible
				IF IS_ENTITY_VISIBLE(e_DefaultPlayerPhone)
					SET_ENTITY_VISIBLE(e_DefaultPlayerPhone, FALSE)
					CPRINTLN(DEBUG_COLIN, "[RCC MISSION] TASK_FAKE_PHONE_HACKING - Check 6 - Set Object Invisible 1.")
				ENDIF
				
				PED_INDEX e_HackingPed
				e_HackingPed = GET_PED_IN_VEHICLE_SEAT(e_TempVehicle, VS_FRONT_RIGHT)
				
				REQUEST_MODEL(Prop_Phone_ING_03)
				
				IF HAS_MODEL_LOADED(Prop_Phone_ING_03)
					// Create Fake Phone we can render sprite to
					e_FakePlayerPhone = CREATE_OBJECT(Prop_Phone_ING_03, GET_PED_BONE_COORDS(GET_PED_IN_VEHICLE_SEAT(e_TempVehicle, VS_FRONT_RIGHT), BONETAG_R_HAND, <<0, 0, 0>>), FALSE, FALSE)
					SET_ENTITY_ROTATION(e_FakePlayerPhone, GET_ENTITY_ROTATION(e_DefaultPlayerPhone))
					FREEZE_ENTITY_POSITION(e_FakePlayerPhone, TRUE)
					SET_ENTITY_COLLISION(e_FakePlayerPhone, FALSE)
					ATTACH_ENTITY_TO_ENTITY(e_FakePlayerPhone, e_HackingPed, GET_PED_BONE_INDEX(e_HackingPed, BONETAG_PH_R_HAND), <<0, 0, 0>>, <<0, 0, 0>>, TRUE)
					SET_ENTITY_INVINCIBLE(e_FakePlayerPhone, TRUE)
					SET_ENTITY_VISIBLE(e_FakePlayerPhone, TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(Prop_Phone_ING_03)
					CPRINTLN(DEBUG_COLIN, "TASK_FAKE_PHONE_HACKING - Check 7 - Fake Phone Created")
					
					REQUEST_STREAMED_TEXTURE_DICT("MPCircuitHack2")
					CPRINTLN(DEBUG_COLIN, "TASK_FAKE_PHONE_HACKING - Check 8 - Requesting Streamed Texture Dictionary - MPCircuitHack2")
					
					e_FakePhoneState = FPPS_UPDATE
					CPRINTLN(DEBUG_COLIN, "TASK_FAKE_PHONE_HACKING - Check 9 - Moving to Update State")
				ENDIF
			ENDIF
		BREAK
		
		
		CASE FPPS_UPDATE
			// PHONE GRABBING RANGE DEBUG SPHERE
			//#IF IS_DEBUG_BUILD
			//	IF DOES_ENTITY_EXIST(GET_PED_IN_VEHICLE_SEAT(e_TempVehicle, VS_FRONT_RIGHT))
			//		DRAW_DEBUG_SPHERE(GET_PED_BONE_COORDS(GET_PED_IN_VEHICLE_SEAT(e_TempVehicle, VS_FRONT_RIGHT), BONETAG_PH_R_HAND, <<0,0,0>>), 0.1, 20, 190, 20, 120)
			//	ENDIF
			//#ENDIF
			
			IF DOES_ENTITY_EXIST(e_DefaultPlayerPhone)
				IF IS_ENTITY_VISIBLE(e_DefaultPlayerPhone)
					SET_ENTITY_VISIBLE(e_DefaultPlayerPhone, FALSE)
					CPRINTLN(DEBUG_COLIN, "[RCC MISSION] TASK_FAKE_PHONE_HACKING - Check 10 - Set Object Invisible 2.")
				ENDIF
				
			ELSE
				// REGRAB THE PHONE
				
				CPRINTLN(DEBUG_COLIN, "[RCC MISSION] TASK_FAKE_PHONE_HACKING - Check 11 - Phone no longer exists in update state.")
				
				VECTOR v_TempVector
				
				IF DOES_ENTITY_EXIST(GET_PED_IN_VEHICLE_SEAT(e_TempVehicle, VS_FRONT_RIGHT))
				AND IS_NET_PLAYER_OK(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(e_TempVehicle, VS_FRONT_RIGHT)))
					v_TempVector = GET_PED_BONE_COORDS(GET_PED_IN_VEHICLE_SEAT(e_TempVehicle, VS_FRONT_RIGHT), BONETAG_PH_R_HAND, <<0, 0, 0>>)
				ENDIF
				
				CPRINTLN(DEBUG_COLIN, "[RCC MISSION] TASK_FAKE_PHONE_HACKING - Check 12 - Attempt phone object regrab")
				
				IF NOT IS_VECTOR_ZERO(v_TempVector)
					e_DefaultPlayerPhone = GET_CLOSEST_OBJECT_OF_TYPE(v_TempVector, 0.1, PROP_PLAYER_PHONE_01, FALSE, FALSE, FALSE)
				ENDIF
				
				IF e_DefaultPlayerPhone <> NULL
					PRINTLN("[RCC MISSION] TASK_FAKE_PHONE_HACKING - Check 13a - Found & Grabbed Object.")
				ELSE
					PRINTLN("[RCC MISSION] TASK_FAKE_PHONE_HACKING - Check 13b - Coundn't Find Object.")
				ENDIF
			ENDIF
			
			
			// RENDER TO FAKE PHONE RENDER TARGET
			IF DOES_ENTITY_EXIST(e_FakePlayerPhone)
				CPRINTLN(DEBUG_COLIN, "TASK_FAKE_PHONE_HACKING - Render to Fake Phone")
				
				IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPCircuitHack2")
					INT iCellphoneTarget
					iCellphoneTarget = -1
					INT iDefaultTarget
					iDefaultTarget = GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID()

					GET_MOBILE_PHONE_RENDER_ID(iCellphoneTarget)

					IF iCellphoneTarget != -1
						//CELL_HORIZONTAL_MODE_TOGGLE(TRUE)
						
						SET_TEXT_RENDER_ID(iCellphoneTarget)
						
						DRAW_SPRITE("MPCircuitHack2", "CBLevel2", 0.5, 0.5, 1, 1, 90.0, 255, 255, 255, 255)
						
						SET_TEXT_RENDER_ID(iDefaultTarget)
					ENDIF
					
				ELSE
					REQUEST_STREAMED_TEXTURE_DICT("MPCircuitHack2")
					CPRINTLN(DEBUG_COLIN, "TASK_FAKE_PHONE_HACKING - Requesting Streamed Texture Dictionary again")
				ENDIF
				
				
				IF DOES_ENTITY_EXIST(GET_PED_IN_VEHICLE_SEAT(e_TempVehicle, VS_FRONT_RIGHT))
				AND NOT IS_NET_PLAYER_OK(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(e_TempVehicle, VS_FRONT_RIGHT)))
					DELETE_OBJECT(e_FakePlayerPhone)
					PRINTLN("[RCC MISSION] TASK_FAKE_PHONE_HACKING - Local Player: ",  GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(LocalPlayerPed)), " - deleted phone object 2")
					SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPCircuitHack2")
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC PROCESS_SERVER_CIRCUIT_HACK_MG_INIT(INT iDialogueCausingHack)
	
	IF iDialogueCausingHack >= 0
	AND iDialogueCausingHack < FMMC_MAX_DIALOGUES_LEGACY
	AND NOT IS_BIT_SET(MC_serverBD_1.iHackDialogueCompletedBS[GET_LONG_BITSET_INDEX(iDialogueCausingHack)], GET_LONG_BITSET_BIT(iDialogueCausingHack))
		
		SET_BIT(MC_serverBD_1.iServerHackMGBitset, SBBOOL_HACK_FIND_PLAYER)
		PRINTLN("[RCC MISSION] [MJM] PROCESS_SERVER_CIRCUIT_HACK_MG_INIT - SET_BIT(MC_serverBD_1.iServerHackMGBitset, SBBOOL_HACK_FIND_PLAYER)")
		
		INT iTeam = g_FMMC_STRUCT.sDialogueTriggers[iDialogueCausingHack].iTeam
		INT iRule = g_FMMC_STRUCT.sDialogueTriggers[iDialogueCausingHack].iRule
		
		IF iRule > -1
		AND iRule < FMMC_MAX_RULES
			IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueCausingHack].iDialogueBitset, iBS_DialogueBlockProgress)
				//SET_BIT(MC_serverBD_1.iServerHackMGBitset, SBBOOL_HACK_HOLD_UP_PROGRESSION)
				IF NOT IS_BIT_SET(MC_serverBD_1.iObjectiveProgressionHeldUpBitset[iTeam], iRule)
					PRINTLN("[RCC MISSION] PROCESS_SERVER_CIRCUIT_HACK_MG_INIT - iObjectiveProgressionHeldUpBitset / objective progression to be held up for team ",iTeam," and rule ",iRule," by circuit hack minigame")
					SET_BIT(MC_serverBD_1.iObjectiveProgressionHeldUpBitset[iTeam], iRule)
				ENDIF
			//ELSE
				//CLEAR_BIT(MC_serverBD_1.iServerHackMGBitset, SBBOOL_HACK_HOLD_UP_PROGRESSION)
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueCausingHack].iDialogueBitset, iBS_DialogueForceProgress)
				SET_BIT(MC_serverBD_1.iServerHackMGBitset, SBBOOL_HACK_PASSES_RULE)
			ELSE
				CLEAR_BIT(MC_serverBD_1.iServerHackMGBitset, SBBOOL_HACK_PASSES_RULE)
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueCausingHack].iDialogueBitset, iBS_DialogueFailOnMinigame)
				SET_BIT(MC_serverBD_1.iServerHackMGBitset, SBBOOL_HACK_FAIL_ON_FAIL)
			ELSE 
				CLEAR_BIT(MC_serverBD_1.iServerHackMGBitset, SBBOOL_HACK_FAIL_ON_FAIL)
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueCausingHack].iDialogueBitset, iBS_DialogueUseHardMinigame)
				SET_BIT(MC_serverBD_1.iServerHackMGBitset, SBBOOL_HACK_DO_HARD)
			ELSE
				CLEAR_BIT(MC_serverBD_1.iServerHackMGBitset, SBBOOL_HACK_DO_HARD)
			ENDIF
			
			MC_serverBD_1.iVehicleForHackingMG = g_FMMC_STRUCT.sDialogueTriggers[iDialogueCausingHack].iVehMinigameIndex
			
			SET_BIT(MC_serverBD_1.iHackDialogueCompletedBS[GET_LONG_BITSET_INDEX(iDialogueCausingHack)], GET_LONG_BITSET_BIT(iDialogueCausingHack))
			
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_SERVER_CIRCUIT_HACK_MG_END(INT iTeam, BOOL bPass)
	
	IF bPass
		SET_BIT(MC_serverBD_1.iServerHackMGBitset,SBBOOL_HACK_DONE)
		
		//Force progress
		IF IS_BIT_SET(MC_serverBD_1.iServerHackMGBitset, SBBOOL_HACK_PASSES_RULE)
			SET_TEAM_OBJECTIVE_OUTCOME(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], TRUE)
			INVALIDATE_UP_TO_AND_INCLUDING_OBJECTIVE(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], TRUE)
			RECALCULATE_OBJECTIVE_LOGIC(iTeam)
			PRINTLN("[RCC MISSION] Rule: ",MC_serverBD_4.iCurrentHighestPriority[iTeam]," FOR TEAM: ",iteam," Progressed BECAUSE HACK MINIGAME PASSED: ")
		ENDIF
	ELSE
	
		SET_BIT(MC_serverBD_1.iServerHackMGBitset,SBBOOL_HACK_FAILED)
	
		//Fail mission if creator option set
		IF IS_BIT_SET(MC_serverBD_1.iServerHackMGBitset,SBBOOL_HACK_FAIL_ON_FAIL)
			MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_HACK_OBJ
			SET_TEAM_FAILED(iTeam,mFail_HACK_OBJ)
			CLEAR_BIT(MC_serverBD_1.iServerHackMGBitset,SBBOOL_HACK_FAIL_ON_FAIL)
			PRINTLN("[RCC MISSION] MISSION FAILED FOR TEAM:",iteam," BECAUSE HACK MINIGAME FAILED: ")
		ENDIF
	ENDIF
	
	PRINTLN("[MJM] PROCESS_SERVER_CIRCUIT_HACK_MG_END - MC_serverBD_1.iPlayerForHackingMG = ", MC_serverBD_1.iPlayerForHackingMG)
	
	MC_serverBD_1.iPlayerForHackingMG = -1
	
ENDPROC

PROC MAINTAIN_PHONE_HACK_MINIGAME()
	
	IF MC_serverBD_1.iPlayerForHackingMG != -1
	OR SHOULD_DEBUG_LAUNCH_PHONE_HACK_MINIGAME()
		IF IS_PED_SITTING_IN_ANY_VEHICLE(LocalPlayerPed) // url:bugstar:2228360
			IF MC_serverBD_1.iPlayerForHackingMG = iLocalPart
			OR SHOULD_DEBUG_LAUNCH_PHONE_HACK_MINIGAME()
				IF NOT IS_BIT_SET(iLocalBoolCheck7, LBOOL7_HACKING_MINIGAME_COMPLETE)
				
					INT iTeam = MC_playerBD[iPartToUse].iteam
					INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
					
					IF IS_SAFE_TO_RUN_FAKE_PHONE_TASK(hackingMinigameData)
						IF NOT IS_PED_RUNNING_MOBILE_PHONE_TASK(LocalPlayerPed)
							TASK_USE_MOBILE_PHONE(LocalPlayerPed, TRUE, Mode_ToText)
							PRINTLN("[RCC MISSION] MAINTAIN_PHONE_HACK_MINIGAME - PHONE TASK GIVEN - TASK_USE_MOBILE_PHONE")
						ELSE
							//SET_PED_CAN_PLAY_GESTURE_ANIMS(LocalPlayerPed, FALSE)
							TASK_LOOK_AT_COORD(LocalPlayerPed, GET_PED_BONE_COORDS(LocalPlayerPed, BONETAG_PH_R_HAND, <<0,0,0>>), -1, SLF_DEFAULT)
						ENDIF
					ELSE
						PRINTLN("[RCC MISSION] NOT IS_SAFE_TO_RUN_FAKE_PHONE_TASK")
					ENDIF

					IF NOT SHOULD_DEBUG_LAUNCH_PHONE_HACK_MINIGAME() // FIX FOR DEBUG LAUNCH TESTING
						IF NOT IS_BIT_SET(MC_playerBD[MC_serverBD_1.iPlayerForHackingMG].iClientBitSet2, PBBOOL2_USE_FAKE_PHONE)
							SET_BIT(MC_playerBD[MC_serverBD_1.iPlayerForHackingMG].iClientBitSet2, PBBOOL2_USE_FAKE_PHONE)
							PRINTLN("[RCC MISSION] MAINTAIN_PHONE_HACK_MINIGAME - SET USE FAKE PHONE")
						ENDIF
					ENDIF
					
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
					
					IF NOT HAS_MINIGAME_TIMER_BEEN_INITIALISED(0)
						PRINTLN("[RCC MISSION] HACKING (CIRCUIT BOARD) - INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY - SLOT 0")
						PRINTLN("[RCC MISSION] HACKING (CIRCUIT BOARD) - START MINIGAME TIMER - SLOT 0")							
						INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY(0)
						START_MINIGAME_TIMER_FOR_TELEMETRY(0)
					ENDIF
					
					IF NOT IS_BIT_SET(hackingMinigameData.i_CircuitBitSet, BIT_SET_CIRCUIT_START_TIMER)
						REINIT_NET_TIMER(stMGTimer0)
						SET_BIT(hackingMinigameData.i_CircuitBitSet, BIT_SET_CIRCUIT_START_TIMER)
					ENDIF
					
					DISABLE_DPADDOWN_THIS_FRAME()
					
					IF NOT GET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_PreventAutoShuffleToDriversSeat)
						SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_PreventAutoShuffleToDriversSeat, TRUE)
					ENDIF
					
					BOOL bHard
					IF SHOULD_DEBUG_LAUNCH_PHONE_HACK_MINIGAME()
						bHard = TRUE
					ELSE
						IF IS_BIT_SET(MC_serverBD_1.iServerHackMGBitset, SBBOOL_HACK_DO_HARD)
							bHard = TRUE
						ENDIF
					ENDIF
					
					PRINTLN("[RCC MISSION] MC_PlayerBD[iPartToUse].inumHacks test 0 = ", MC_PlayerBD[iPartToUse].inumHacks)

					RUN_CIRCUIT_HACKING_MINIGAME(hackingMinigameData,bHard)
					
					IF IS_BIT_SET(hackingMinigameData.i_CircuitBitSet, BIT_SET_CIRCUIT_LEVEL_FAILED)
						INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY(0)
						PRINTLN("[RCC MISSION] HACKING (CIRCUIT BOARD) - INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY - SLOT 0")
					ENDIF
					
					IF HAS_PLAYER_BEAT_CIRCUIT_HACKING(hackingMinigameData)
					OR SHOULD_FORCE_PASS_HACK_MINIGAME()
						BROADCAST_HEIST_END_HACK_MINIGAME(ALL_PLAYERS_ON_SCRIPT(TRUE), TRUE, MC_playerBD[iPartToUse].iTeam)
						CLEANUP_AND_QUIT_CIRCUIT_HACKING_MINIGAME(hackingMinigameData, IS_BIT_SET(MC_serverBD_1.iServerHackMGBitset, SBBOOL_HACK_DO_HARD))
						
						SET_BIT(iLocalBoolCheck7,LBOOL7_HACKING_MINIGAME_COMPLETE)
						
						IF IS_BIT_SET(MC_serverBD_1.iServerHackMGBitset, SBBOOL_HACK_DO_HARD)
							SET_BIT(MC_playerBD[iPartToUse].iClientBitSet2, PBBOOL2_HACKING_MINIGAME_COMPLETED)
						ENDIF
						
						IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
							CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
							PRINTLN("[RCC MISSION] PROCESS_PRE_MAIN_CHECKS - (2) Clearing - PBBOOL_OBJECTIVE_BLOCKER")
						ENDIF
						
						SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_PreventAutoShuffleToDriversSeat, FALSE)
						
						SET_BIT(MC_playerBD[iPartToUse].iClientBitSet2, PBBOOL2_HACKING_MINIGAME_COMPLETED)
						
						PRINTLN("[RCC MISSION] MAINTAIN_PHONE_HACK_MINIGAME - AWARD GIVEN - HAS_PLAYER_BEAT_CIRCUIT_HACKING - TRUE")
						MC_playerBD[iPartToUse].iHackMGDialogue = -1
						
						increment_medal_objective_completed(iTeam, iRule)
						INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE()
						
						STORE_MINIGAME_TIMER_FOR_TELEMETRY(0)
						STORE_MINIGAME_ATTEMPTS_FOR_TELEMETRY(0)
						PRINTLN("[RCC MISSION] HACKING (CIRCUIT BOARD) - STORE_MINIGAME_ATTEMPTS_FOR_TELEMETRY - SLOT 0")
						PRINTLN("[RCC MISSION] HACKING (CIRCUIT BOARD) - STORE_MINIGAME_TIMER_FOR_TELEMETRY( - SLOT 0")
						
						MC_PlayerBD[iPartToUse].iHackTime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stMGTimer0)
						MC_PlayerBD[iPartToUse].inumHacks++
						PRINTLN("[RCC MISSION] inumHacks HEIST TELEMETRY CIRCUIT BOARD MINIGAME  -", MC_playerBD[iPartToUse].iNumHacks)
						
						#IF IS_DEBUG_BUILD
							bDebugCircuitHack = FALSE
						#ENDIF
						
					ELIF HAS_PLAYER_FAILED_CIRCUIT_HACKING(hackingMinigameData)
					OR SHOULD_FORCE_FAIL_HACK_MINIGAME()
						BROADCAST_HEIST_END_HACK_MINIGAME(ALL_PLAYERS_ON_SCRIPT(TRUE), FALSE, MC_playerBD[iPartToUse].iTeam)
						CLEANUP_AND_QUIT_CIRCUIT_HACKING_MINIGAME(hackingMinigameData, IS_BIT_SET(MC_serverBD_1.iServerHackMGBitset, SBBOOL_HACK_DO_HARD))
						
						SET_BIT(iLocalBoolCheck7, LBOOL7_HACKING_MINIGAME_COMPLETE)
						
						IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
							CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
							PRINTLN("[RCC MISSION] PROCESS_PRE_MAIN_CHECKS - (1) Clearing - PBBOOL_OBJECTIVE_BLOCKER")
						ENDIF
						
						SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_PreventAutoShuffleToDriversSeat,FALSE)
						
						SET_BIT(MC_playerBD[iPartToUse].iClientBitSet2, PBBOOL2_HACKING_MINIGAME_FAILED)
						MC_playerBD[iPartToUse].iHackMGDialogue = -1
						PRINTLN("[RCC MISSION] MAINTAIN_PHONE_HACK_MINIGAME - AWARD GIVEN - HAS_PLAYER_BEAT_CIRCUIT_HACKING - FALSE")
					
						INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY(0)
						PRINTLN("[RCC MISSION] HACKING (CIRCUIT BOARD) - INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY - SLOT 0")
						
						#IF IS_DEBUG_BUILD
							bDebugCircuitHack = FALSE
						#ENDIF
						
					ELSE
						//SET_BIT(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
						
						CLEAR_ANY_OBJECTIVE_TEXT()
						
						PRINTLN("[RCC MISSION] [MB] Clear_Any_Objective_Text() due to Hacking Minigame")
					ENDIF
				ENDIF
				
			ELSE
				IF NOT IS_BIT_SET(MC_playerBD[ MC_serverBD_1.iPlayerForHackingMG ].iClientBitSet2, PBBOOL2_HACKING_MINIGAME_COMPLETED)
					
					IF NOT IS_PHONE_ONSCREEN()
						IF IS_BIT_SET(MC_playerBD[ MC_serverBD_1.iPlayerForHackingMG ].iClientBitSet2, PBBOOL2_USE_FAKE_PHONE)
							PRINTLN("[RCC MISSION] MAINTAIN_PHONE_HACK_MINIGAME - Local Player: ",  GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(LocalPlayerPed)), ", calling TASK_FAKE_PHONE_HACKING")
							TASK_FAKE_PHONE_HACKING()
						ENDIF
					ENDIF
				
				ELSE
					PRINTLN("[RCC MISSION] MAINTAIN_PHONE_HACK_MINIGAME - Local Player: ",  GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(LocalPlayerPed)), ", minigame over - need to clean up phone object")
					
					IF DOES_ENTITY_EXIST(e_FakePlayerPhone)
						DELETE_OBJECT(e_FakePlayerPhone)
						PRINTLN("[RCC MISSION] MAINTAIN_PHONE_HACK_MINIGAME - Local Player: ",  GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(LocalPlayerPed)), " - deleted phone object 1")
						SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPCircuitHack2")
						e_FakePhoneState = FPPS_INIT
					ENDIF
				ENDIF
			ENDIF
			
		ELSE
			IF IS_CIRCUIT_HACKING_MINIGAME_RUNNING(hackingMinigameData) AND NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_ALLOW_LESTER_SNAKE_MINIGAME)
				CLEANUP_AND_QUIT_CIRCUIT_HACKING_MINIGAME(hackingMinigameData, IS_BIT_SET(MC_serverBD_1.iServerHackMGBitset, SBBOOL_HACK_DO_HARD))
				
				IF bLocalPlayerPedOk
					SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_PreventAutoShuffleToDriversSeat, FALSE)
				ENDIF
				PRINTLN("[RCC MISSION] MAINTAIN_PHONE_HACK_MINIGAME - Player not in vehicle but mg running, cleaning up")
			ENDIF
		ENDIF
		
	ELSE
		IF DOES_ENTITY_EXIST(e_FakePlayerPhone)
			DELETE_OBJECT(e_FakePlayerPhone)
			PRINTLN("[RCC MISSION] MAINTAIN_PHONE_HACK_MINIGAME - Local Player: ",  GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(LocalPlayerPed)), " - deleted phone object 3")
			SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPCircuitHack2")
			e_FakePhoneState = FPPS_INIT
		ENDIF
					
		IF IS_BIT_SET(iLocalBoolCheck7, LBOOL7_HACKING_MINIGAME_COMPLETE)
			CLEAR_BIT(iLocalBoolCheck7, LBOOL7_HACKING_MINIGAME_COMPLETE)
			PRINTLN("[RCC MISSION] MAINTAIN_PHONE_HACK_MINIGAME - CLEAR_BIT(iLocalBoolCheck7, LBOOL7_HACKING_MINIGAME_COMPLETE)")
		ENDIF
	ENDIF
ENDPROC



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: TRACKIFY (+ Rally Arrows) !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************



CONST_INT ciRALLY_HELP_TIMER 4500

PROC DISPLAY_ARROWS_HELP()
	//b*2189142
	//CLEAR_HELP()
	
	IF IS_BIT_SET(iLocalBoolCheck,LBOOL_DPAD_HELP_DISP)
		IF NOT HAS_NET_TIMER_EXPIRED(stRallyHelpTimer, ciRALLY_HELP_TIMER)
			IF IS_CELLPHONE_TRACKIFY_IN_USE()
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TRACKIFY_OPE")
					PRINT_HELP("TRACKIFY_OPE", ciRALLY_HELP_TIMER)
				ENDIF
			ELSE
				
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TRACKIFY_PHO")
					PRINT_HELP("TRACKIFY_PHO", ciRALLY_HELP_TIMER)
					PRINTLN("[RCC MISSION] DISPLAY_ARROWS_HELP - PRINTING TRACKIFY_PHO HELP!")
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT HAS_NET_TIMER_STARTED(stRallyHelpTimer)
			START_NET_TIMER(stRallyHelpTimer)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_I_GIVE_DIRECTIONS()
	PRINTLN("[MMacK] [Arrows] SHOULD_I_GIVE_DIRECTIONS")
	//disabled the vehicle check as per 2139836
	//IF MC_playerBD[iPartToUse].iVehNear != -1 //are we in the vehicle we need? if not, bail
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		PRINTLN("[MMacK] [Arrows] MC_playerBD[iPartToUse].iVehNear > -1")
		VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed, FALSE)
		
		IF GET_PED_IN_VEHICLE_SEAT(tempVeh, VS_FRONT_RIGHT) = LocalPlayerPed //are we the front right? if not, bail. 
			PRINTLN("[MMacK] [Arrows] VS_FRONT_RIGHT")
			IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE)
				IF IS_CELLPHONE_TRACKIFY_IN_USE()
					IF IS_ANY_OTHER_PLAYER_USING_VEHICLE(tempVeh)
						PRINTLN("[MMacK] [Arrows] YES!")
						RETURN TRUE
					ENDIF
				ELSE
					PRINTLN("[MMacK] [Arrows] TRACKIFY ISNT OPEN!")
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciHIDE_DRIVING_ARROW_HELP)
						PRINTLN("ciHIDE_DRIVING_ARROW_HELP is not set?? - here 1")
						DISPLAY_ARROWS_HELP()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	PRINTLN("[MMacK] [Arrows] NO!")
	RETURN FALSE
ENDFUNC

PROC SET_ARROW_BITS(INT iArrowBit, BOOL bBroadcast, BOOL bButtonHeld = FALSE)
	RESET_NET_TIMER(stRallyArrowTimer)
	
	CLEAR_BIT(iArrowBitset, ciRALLY_ARROW_RIGHT)
	CLEAR_BIT(iArrowBitset, ciRALLY_ARROW_LEFT)
	CLEAR_BIT(iArrowBitset, ciRALLY_ARROW_UP)
	CLEAR_BIT(iArrowBitset, ciRALLY_ARROW_DOWN)
	
	SET_BIT(iArrowBitset, iArrowBit)

//	DO_ARROW_SOUNDS(iArrowBit)

	IF bBroadcast
		BROADCAST_RALLY_ARROW_DIRECTION(iArrowBit, bButtonHeld)
	ENDIF
ENDPROC

PROC SEND_DRIVER_ARROW_DIRECTIONS()	
	PRINTLN("[MMacK] [Arrows] SEND_DRIVER_ARROW_DIRECTIONS")
	IF NOT IS_PAUSE_MENU_ACTIVE()
		IF SHOULD_I_GIVE_DIRECTIONS() 
		
			//Turn of using radio for co-driver
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_RADIO_WHEEL)
			
			IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciHIDE_DRIVING_ARROW_HELP)
						PRINTLN("ciHIDE_DRIVING_ARROW_HELP is not set?? - here 1")
						DISPLAY_ARROWS_HELP()
					ENDIF
				ENDIF
			ENDIF
		
			// Pressed
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_LEFT)
			OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_LEFT)
				SET_ARROW_BITS(ciRALLY_ARROW_LEFT, TRUE)
				DO_ARROW_SOUNDS(ciRALLY_ARROW_LEFT)
			ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
			OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
				SET_ARROW_BITS(ciRALLY_ARROW_RIGHT, TRUE)
				DO_ARROW_SOUNDS(ciRALLY_ARROW_RIGHT)
			ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_DOWN)
			OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_DOWN)
				SET_ARROW_BITS(ciRALLY_ARROW_DOWN, TRUE)
				DO_ARROW_SOUNDS(ciRALLY_ARROW_DOWN)
			ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_UP)
			OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_UP)
				SET_ARROW_BITS(ciRALLY_ARROW_UP, TRUE)
				DO_ARROW_SOUNDS(ciRALLY_ARROW_UP)
			ENDIF
			
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_ARROW_EVENT(INT iEventID)
	SCRIPT_EVENT_DATA_RALLY_ARROWS Event
	INT iSenderTeam, iLocalTeam
	IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE)
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
			iSenderTeam = GET_PLAYER_TEAM(Event.Details.FromPlayerIndex)
			iLocalTeam = GET_PLAYER_TEAM(PLAYER_ID())
			IF iSenderTeam != iLocalTeam
			AND AM_I_DRIVING_THE_NAVIGATOR(Event.Details.FromPlayerIndex)
		
				#IF IS_DEBUG_BUILD
				STRING sSender = GET_PLAYER_NAME(Event.Details.FromPlayerIndex)
				PRINTLN("[CS_ARR] PROCESS_RALLY_ARROW_EVENT, sSender = ", sSender, " direction  = ", GET_ARROW_DIRECTION_NAME(event.iArrowDirection))
				#ENDIF
				
				SWITCH event.iArrowDirection
					CASE ciRALLY_ARROW_LEFT 							
						SET_ARROW_BITS(ciRALLY_ARROW_LEFT, FALSE)
						DO_ARROW_SOUNDS(ciRALLY_ARROW_LEFT)
						PRINTLN("[CS_ARR] PROCESS_RALLY_ARROW_EVENT, ciRALLY_ARROW_LEFT ")
					BREAK
					
					CASE ciRALLY_ARROW_RIGHT
						SET_ARROW_BITS(ciRALLY_ARROW_RIGHT, FALSE)
						DO_ARROW_SOUNDS(ciRALLY_ARROW_RIGHT)
						PRINTLN("[CS_ARR] PROCESS_RALLY_ARROW_EVENT,  ciRALLY_ARROW_RIGHT ")
					BREAK
					
					CASE ciRALLY_ARROW_UP
						SET_ARROW_BITS(ciRALLY_ARROW_UP, FALSE)
						DO_ARROW_SOUNDS(ciRALLY_ARROW_UP)
						PRINTLN("[CS_ARR] PROCESS_RALLY_ARROW_EVENT,  ciRALLY_ARROW_UP ")
					BREAK
					
					CASE ciRALLY_ARROW_DOWN
						SET_ARROW_BITS(ciRALLY_ARROW_DOWN, FALSE)
						DO_ARROW_SOUNDS(ciRALLY_ARROW_DOWN)
						PRINTLN("[CS_ARR] PROCESS_RALLY_ARROW_EVENT,  ciRALLY_ARROW_DOWN ")
					BREAK
				ENDSWITCH
			ELSE
				PRINTLN("[CS_ARR] ERROR, PROCESS_RALLY_ARROW_EVENT, TEAMS iSenderTeam = ", iSenderTeam, " iLocalTeam = ", iLocalTeam)
			ENDIF
		ELSE
			PRINTLN("[CS_ARR] ERROR, PROCESS_RALLY_ARROW_EVENT,  EVENT DATA ")
		ENDIF
	ELSE
		PRINTLN("[CS_ARR] ERROR, PROCESS_RALLY_ARROW_EVENT,  OK CHECK ")
	ENDIF
ENDPROC

CONST_INT ciRALLY_ARROW_TIMEOUT 2000

PROC DRAW_ARROW_FOR_DURATION(INT iDirection)
	IF NOT HAS_NET_TIMER_STARTED(stRallyArrowTimer)
		PRINTLN("[CS_ARR] START_NET_TIMER ")
		START_NET_TIMER(stRallyArrowTimer)
	ELSE
		IF HAS_NET_TIMER_EXPIRED(stRallyArrowTimer, ciRALLY_ARROW_TIMEOUT)
			RESET_NET_TIMER(stRallyArrowTimer)
			CLEAR_BIT(iArrowBitset, iDirection)
			PRINTLN("[CS_ARR] CLEAR_BIT ")
		ELSE
			PRINTLN("[CS_ARR] DRAW_NEW_RALLY_ARROW ")
			DRAW_NEW_RALLY_ARROW(siRallyArrow, iDirection)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_ARROW_DRAWING()
	REQUEST_ARROW_MOVIE(siRallyArrow)
	IF IS_BIT_SET(iArrowBitset, ciRALLY_ARROW_LEFT)
		DRAW_ARROW_FOR_DURATION(ciRALLY_ARROW_LEFT)
	ENDIF
	IF IS_BIT_SET(iArrowBitset, ciRALLY_ARROW_RIGHT)
		DRAW_ARROW_FOR_DURATION(ciRALLY_ARROW_RIGHT)
	ENDIF
	IF IS_BIT_SET(iArrowBitset, ciRALLY_ARROW_UP)
		DRAW_ARROW_FOR_DURATION(ciRALLY_ARROW_UP)
	ENDIF
	IF IS_BIT_SET(iArrowBitset, ciRALLY_ARROW_DOWN)
		DRAW_ARROW_FOR_DURATION(ciRALLY_ARROW_DOWN)
	ENDIF
ENDPROC

PROC MAINTAIN_DRIVING_ARROWS()
	IF MC_playerBD[iPartToUse].iteam != -1
		IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
		AND MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] > -1
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetTwo[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE2_RALLY_MODE_ARROWS)
				PRINTLN("[MMacK] [Arrows] MAINTAIN_DRIVING_ARROWS")
				SEND_DRIVER_ARROW_DIRECTIONS()
				MAINTAIN_ARROW_DRAWING()
			ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetThree[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE3_RALLY_MODE_ARROWS_DRIVER)
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TRACKIFY_DRI")
				AND NOT bHelpTextArrows
					CLEAR_HELP()
					PRINT_HELP("TRACKIFY_DRI", DEFAULT_HELP_TEXT_TIME)
					bHelpTextArrows = TRUE
				ENDIF
				
				MAINTAIN_ARROW_DRAWING()
			ELSE
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TRACKIFY_OPE")
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TRACKIFY_PHO")
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitset[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE_ENABLETRACKIFY)
						//CLEAR_HELP()
						PRINTLN("[RCC MISSION] MAINTAIN_DRIVING_ARROWS - HELP BEING CLEARED!")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC ENABLE_TRANSMITTER_LIGHT_ON_OBJECT(OBJECT_INDEX tempObj, BOOL bDisable = FALSE)
	
	IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(trans_lights_ptfx)
		IF NOT bDisable
			REQUEST_NAMED_PTFX_ASSET("scr_adversary")
			REQUEST_PTFX_ASSET()
			IF HAS_NAMED_PTFX_ASSET_LOADED("scr_adversary")
				USE_PARTICLE_FX_ASSET("scr_adversary")
				trans_lights_ptfx = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_emp_prop_light", tempObj, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
				PRINTLN("[ENABLE_TRANSMITTER_LIGHT_ON_OBJECT] START_PARTICLE_FX_LOOPED_ON_ENTITY")
			ENDIF
		ENDIF
	ELSE
		//green
		IF NOT bDisable
			SET_PARTICLE_FX_LOOPED_COLOUR(trans_lights_ptfx, 0.35, 0.72, 0.11)
		ELSE
			STOP_PARTICLE_FX_LOOPED(trans_lights_ptfx)
		ENDIF
	ENDIF

ENDPROC

//Checks if another player is potentially occupying a given position.
FUNC BOOL IS_ANOTHER_PLAYER_BLOCKING_CONTAINER_LOCATION(OBJECT_INDEX obj, FLOAT fRadius)
	VECTOR vObjPos = GET_ENTITY_COORDS(obj)
	VECTOR vObjOffset1 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(obj, <<-0.3, 0.0, 0.0>>) + <<0.0, 0.0, 2.0>>
	VECTOR vObjOffset2 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(obj, <<-0.3, -1.25, 0.0>>) + <<0.0, 0.0, -1.5>>

	INT iPartLoop
	FOR iPartLoop = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		PARTICIPANT_INDEX currentParticipant = INT_TO_PARTICIPANTINDEX(iPartLoop)
	
		IF NETWORK_IS_PARTICIPANT_ACTIVE(currentParticipant)
			PLAYER_INDEX currentPlayer = NETWORK_GET_PLAYER_INDEX(currentParticipant)
		
			IF IS_NET_PLAYER_OK(NETWORK_GET_PLAYER_INDEX(currentParticipant))
			AND currentPlayer != LocalPlayer
				PED_INDEX partPed = GET_PLAYER_PED(currentPlayer)
				VECTOR vPedPos = GET_ENTITY_COORDS(partPed)
				FLOAT fDistFromPos = VDIST(vPedPos, vObjPos)
				
				IF fDistFromPos < fRadius
				AND fDistFromPos < VDIST(GET_ENTITY_COORDS(LocalPlayerPed), vObjPos)
				AND IS_ENTITY_IN_ANGLED_AREA(partPed, vObjOffset1, vObjOffset2, 0.75)
					PRINTLN("[TRACKIFY] [RCC MISSION] [IS_ANOTHER_PLAYER_BLOCKING_POSITION] Player ", iPartLoop, " is blocking position ", vObjPos, " distance = ", fDistFromPos)
					
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

//Request all assets need for the container minigame
PROC REQUEST_ASSETS_FOR_CONTAINER_MINIGAME()
	REQUEST_ANIM_DICT("anim@mp_mission@dr_objective")
	REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_adv_hdsec")))
	SET_BIT(iLocalBoolCheck17, LBOOL17_DR_REQUESTED_ANIM)
ENDPROC

//Checks if assets have loaded
FUNC BOOL HAVE_CONTAINER_MINIGAME_ASSETS_LOADED()
	IF IS_BIT_SET(iLocalBoolCheck17, LBOOL17_DR_REQUESTED_ANIM)
	AND HAS_ANIM_DICT_LOADED("anim@mp_mission@dr_objective")
	AND HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_adv_hdsec")))
		PRINTLN("[OPEN] - [RCC MISSION] HAVE_CONTAINER_MINIGAME_ASSETS_LOADED - loaded")
		RETURN TRUE
	ELSE
		REQUEST_ASSETS_FOR_CONTAINER_MINIGAME()
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Clean up assets
PROC CLEAN_UP_ASSETS_FOR_CONTAINER_MINIGAME()
	REMOVE_ANIM_DICT("anim@mp_mission@dr_objective")
	CLEAR_BIT(iLocalBoolCheck17,LBOOL17_DR_REQUESTED_ANIM)
	CLEAR_BIT(iLocalBoolCheck20,LBOOL20_OBJ_CRE_CONTAINER_PICK_UP)
ENDPROC

//Do all the necessary stuff to get the player ready for minigame
PROC GET_PLAYER_READY_FOR_CONTAINER_MINIGAME()
	
	IF bLocalPlayerPedOk
		eWeaponBeforeMinigame = WEAPONTYPE_UNARMED
		GET_CURRENT_PED_WEAPON(LocalPlayerPed, eWeaponBeforeMinigame)
		SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed,TRUE,FALSE)
		CLEAR_PED_TASKS(LocalPlayerPed)
		NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_ALLOW_PLAYER_DAMAGE|NSPC_LEAVE_CAMERA_CONTROL_ON|NSPC_ALLOW_PAD_SHAKE|NSPC_REENABLE_CONTROL_ON_DEATH | NSPC_CAN_BE_TARGETTED)
		SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED, TRUE)
	ENDIF
ENDPROC

//Clean up player from minigame
PROC CLEAN_UP_PLAYER_FROM_MINIGAME()
	CLEAR_PED_TASKS(LocalPlayerPed)
	NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
	
	IF eWeaponBeforeMinigame != WEAPONTYPE_UNARMED
	AND HAS_PED_GOT_WEAPON(LocalPlayerPed, eWeaponBeforeMinigame)
		SET_CURRENT_PED_WEAPON(LocalPlayerPed, eWeaponBeforeMinigame)
	ENDIF
	
ENDPROC

//Checks if the player is close enough to trigger minigame
FUNC BOOL IS_PLAYER_CLOSE_ENOUGH_TO_CONTAINER_TRIGGER(STRING strContainerAnimDict)

	VECTOR vInitialRot = GET_ANIM_INITIAL_OFFSET_ROTATION(strContainerAnimDict, "player_success", vContainerPlaybackPos, <<0.0, 0.0, fObjRot>>)
	FLOAT fDistFromAnimStart = VDIST(GET_ENTITY_COORDS(LocalPlayerPed, FALSE), vContainerGotoPoint + <<0.0, 0.0, 1.0>>)
	FLOAT fDistFromAnimRoot = VDIST(GET_ENTITY_COORDS(LocalPlayerPed, FALSE), vContainerPlaybackPos)
	FLOAT fDistBetweenGotoAndAnimRoot = VDIST(vContainerPlaybackPos, vContainerGotoPoint + <<0.0, 0.0, 1.0>>)
	FLOAT fHeadingDiffFromAnimStart = ABSF(GET_ANGULAR_DIFFERENCE(GET_ENTITY_HEADING(LocalPlayerPed), vInitialRot.Z))
	
	IF fHeadingDiffFromAnimStart > 180.0
		fHeadingDiffFromAnimStart -= 360.0
	ENDIF
	
	CDEBUG1LN(DEBUG_MISSION, "[TRACKIFY CRATE] - [RCC MISSION] IS_PLAYER_CLOSE_ENOUGH_TO_CONTAINER_TRIGGER fDistFromAnimStart = ", fDistFromAnimStart, 
			  " fHeadingDiffFromAnimStart = ", fHeadingDiffFromAnimStart, 
			  " fDistBetweenGotoAndAnimRoot = ", fDistBetweenGotoAndAnimRoot,
			  " fDistFromAnimRoot = ", fDistFromAnimRoot)
	
	// check so if the player would have to walk backwards to the anim trigger point we just start the anim instead.
	IF fDistFromAnimStart < 0.3
	OR (fDistFromAnimRoot < fDistBetweenGotoAndAnimRoot AND fDistFromAnimStart < 0.7)
		IF ABSF(fHeadingDiffFromAnimStart) < 90.0
			CDEBUG1LN(DEBUG_MISSION, "[TRACKIFY] - [RCC MISSION] Player close enough to container anim start.")
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


//objDawnPickUp

/// PURPOSE:
///    Handles the investigate container anims and pass logic.
PROC START_INVESTIGATE_CONTAINER_SYNCHRONIZED_SCENE(STRING strAnimDict, OBJECT_INDEX objMinigame, BOOL bSuccess, ENTITY_INDEX entity)

	MC_playerBD[iPartToUse].iSynchSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(vContainerPlaybackPos, <<0,0, GET_ENTITY_HEADING(objMinigame)>>, EULER_YXZ, TRUE)
	//Player anim stuff
	IF bSuccess
		NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(LocalPlayerPed, MC_playerBD[iPartToUse].iSynchSceneID, strAnimDict, "player_success", 
											  1.25, WALK_BLEND_OUT, 
											  SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_HIDE_WEAPON | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON | SYNCED_SCENE_ABORT_ON_DEATH, 
											  RBF_PLAYER_IMPACT, 1.25)
	ELSE
		NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(LocalPlayerPed, MC_playerBD[iPartToUse].iSynchSceneID, strAnimDict, "player_fail", 
										  1.25, WALK_BLEND_OUT, 
										  SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_HIDE_WEAPON | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON | SYNCED_SCENE_ABORT_ON_DEATH, 
										  RBF_PLAYER_IMPACT, 1.25)
	ENDIF
	
	//Object anim stuff - objMinigame is the container
	//Need this to hold the last frame of the sync scene? model swap?
	IF bSuccess
		NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(objMinigame, MC_playerBD[iPartToUse].iSynchSceneID, 
												 strAnimDict, "case_success", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
	ELSE
		NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(objMinigame, MC_playerBD[iPartToUse].iSynchSceneID, 
												 strAnimDict, "case_fail", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
	ENDIF
	
	//Case object only included in scene if in container
	IF bSuccess

		NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(entity, MC_playerBD[iPartToUse].iSynchSceneID, 
												 strAnimDict, "device_success", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
	ENDIF
	

	NETWORK_START_SYNCHRONISED_SCENE(MC_playerBD[iPartToUse].iSynchSceneID)
	
	//SET_SYNCHRONIZED_SCENE_RATE(MC_playerBD[iPartToUse].iSynchSceneID,2.0)
	
	FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(objMinigame)
	
	IF bSuccess
		FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(entity)
	ENDIF

	CDEBUG1LN(DEBUG_MISSION, "[OPEN][TRACKIFY] - [RCC MISSION] Starting container investigation scene, vContainerPlaybackPos = ", vContainerPlaybackPos)
ENDPROC

PROC PROCESS_SERVER_INVESTIGATE_CONTAINER(INT iThisObj)
	IF bIsLocalPlayerHost
		IF iThisObj != -1
			IF iTrackifyTargets[iThisObj] > -1
				IF NOT (IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_OBJ_0_FOUND + iThisObj)
				AND IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_SHOULD_SHOW_COLLECTABLE))
					IF MC_serverBD.iObjHackPart[iTrackifyTargets[iThisObj]] != -1
					OR bServerSafetyTimer[iThisObj]
						IF NOT bServerSafetyTimer[iThisObj]
							iServerSafetyTimer[iThisObj] = GET_GAME_TIMER()
							
							bServerSafetyTimer[iThisObj] = TRUE
						ENDIF
						
						IF MANAGE_THIS_MINIGAME_TIMER(iServerSafetyTimer[iThisObj], 20000)	//Time Out
							PRINTLN("[PROCESS_SERVER_INVESTIGATE_CONTAINER] - Investigation Timed Out for iObj - ", iThisObj)
							
							CLEAR_BIT(MC_serverBD.iServerObjectBeingInvestigatedBitSet, iThisObj)
							
							IF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_OBJ_TARGET_BEING_INVESTIGATED)
								CLEAR_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_OBJ_TARGET_BEING_INVESTIGATED)
							ENDIF
							
							IF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_SHOULD_SHOW_COLLECTABLE)
								CLEAR_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_SHOULD_SHOW_COLLECTABLE)
								PRINTLN("[PROCESS_SERVER_INVESTIGATE_CONTAINER] CLEAR_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_SHOULD_SHOW_COLLECTABLE)")
							ENDIF
							
							MC_serverBD.iObjHackPart[iTrackifyTargets[iThisObj]] = -1
							
							PRINTLN("[PROCESS_SERVER_INVESTIGATE_CONTAINER] - Investigation Timed Out MC_serverBD.iObjHackPart[", iTrackifyTargets[iThisObj], "] = -1")
							
							bServerSafetyTimer[iThisObj] = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_CLIENT_INVESTIGATE_CONTAINER(OBJECT_INDEX tempObj, INT iThisObj)
	
	//VECTOR vGoTo
	FLOAT fContainerObjRot 
	FLOAT fGroundZTemp
	
	STRING strAnimDict = "anim@mp_mission@dr_objective"
	
	SEQUENCE_INDEX LocalSequenceIndex
	
	IF bIsCaseInContainer
		IF IS_BIT_SET(MC_serverBD.iServerBitSet3,SBBOOL3_MINIGAME_SHOULD_SHOW_COLLECTABLE)
			IF DOES_ENTITY_EXIST(objDawnPickUp)
				SET_ENTITY_LOCALLY_VISIBLE(objDawnPickUp)
				//SET_ENTITY_VISIBLE(objDawnPickUp,TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	BOOL bReadyToProceed
	
	IF iThisObj != -1
		IF iTrackifyTargets[iThisObj] > -1
			//Cancel investigation if you stray too far or someone blocks the container
			IF IS_BIT_SET(iBSContainerBeingInvestigated, iThisObj)
				VECTOR vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
				VECTOR vObjectCoords = GET_ENTITY_COORDS(objTrackifyTarget[iThisObj])
				
				IF GET_DISTANCE_BETWEEN_COORDS(vObjectCoords, vPlayerCoords) > 3.0
				OR ABSF(vObjectCoords.Z - vPlayerCoords.Z) > 1.2
					BROADCAST_CONTAINER_MINIGAME_DATA_CHANGE(iThisObj, iLocalPart, bIsCaseInContainer, FALSE, TRUE)
					
					CLEAR_BIT(iBSContainerBeingInvestigated, iThisObj)
					
					EXIT
				ENDIF
			ENDIF
			
			IF MC_serverBD.iObjHackPart[iTrackifyTargets[iThisObj]] = iLocalPart
			AND bLocalPlayerPedOK
				bReadyToProceed = TRUE
			ELSE
				IF NOT bLocalPlayerPedOK
					IF IS_BIT_SET(iBSContainerBeingInvestigated, iThisObj)
						BROADCAST_CONTAINER_MINIGAME_DATA_CHANGE(iThisObj, iLocalPart, bIsCaseInContainer, FALSE, TRUE)
						CLEAR_BIT(iBSContainerBeingInvestigated, iThisObj)
					ENDIF
				ENDIF
				bReadyToProceed = FALSE
			ENDIF

			SWITCH iInvestigateContainerState
			
				CASE ciHICInit
					
					IF iThisObj != -1 
						IF IS_BIT_SET(iBSContainerBeingInvestigated,iThisObj)
							IF IS_BIT_SET(MC_serverBD.iServerObjectBeingInvestigatedBitSet, iThisObj)
								IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet3,SBBOOL3_MINIGAME_OBJ_0_FOUND + iThisObj)
									IF IS_BIT_SET(iBSContainerBeingInvestigated,iThisObj)
										PRINTLN("[TRACKIFY] - [RCC MISSION] HANDLE_INVESTIGATE_CONTAINER iInvestigateContainerState: ",iInvestigateContainerState, " FOR THIS OBJECT: ", iThisObj) 
										PRINTLN("[TRACKIFY] - [RCC MISSION] HANDLE_INVESTIGATE_CONTAINER - MC_serverBD.iObjHackPart[",iTrackifyTargets[iThisObj],"] = ", MC_serverBD.iObjHackPart[iTrackifyTargets[iThisObj]])
										PRINTLN("[TRACKIFY] - [RCC MISSION] HANDLE_INVESTIGATE_CONTAINER - iPartToUse, " ,iPartToUse)
										IF bReadyToProceed
											//Turn off player control etc.
											//Request all assets
											GET_PLAYER_READY_FOR_CONTAINER_MINIGAME()
											REQUEST_ASSETS_FOR_CONTAINER_MINIGAME()
											
											//Request control of container and case object so I can animate them in the scene
											IF bIsCaseInContainer
												NETWORK_REQUEST_CONTROL_OF_ENTITY(objDawnPickUp)
											ENDIF
											NETWORK_REQUEST_CONTROL_OF_ENTITY(tempObj)
											
											//Set local safety timer
											iSafetyTimer = GET_GAME_TIMER()
											iInvestigateContainerState = ciHICHaveAssetsLoaded
										ELSE
											PRINTLN("[TRACKIFY] - [RCC MISSION] HANDLE_INVESTIGATE_CONTAINER Waiting on bReadyToProceed ")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE ciHICHaveAssetsLoaded
					//check anims have loaded
					IF HAVE_CONTAINER_MINIGAME_ASSETS_LOADED()
					OR MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer, 3000)
					
						//Calculate scene position and goto position
						fContainerObjRot  = GET_ENTITY_HEADING(tempObj)
						vContainerPlaybackPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj, vContainerSceneOffsetFromObj)
						vContainerGotoPoint = GET_ANIM_INITIAL_OFFSET_POSITION(strAnimDict, "player_success", vContainerPlaybackPos, <<0.0, 0.0, fContainerObjRot>>)
						vContainerInitialRot = GET_ANIM_INITIAL_OFFSET_ROTATION(strAnimDict, "player_success", vContainerPlaybackPos, <<0.0, 0.0, fContainerObjRot>>)
						GET_GROUND_Z_FOR_3D_COORD(vContainerGotoPoint + <<0.0, 0.0, 0.5>>, fGroundZTemp)
						vContainerGotoPoint.z = fGroundZTemp
						
						PRINTLN("[TRACKIFY] - [RCC MISSION] Distance between player and goto ", VDIST(GET_ENTITY_COORDS(LocalPlayerPed), vContainerGotoPoint))
						
						IF IS_PLAYER_CLOSE_ENOUGH_TO_CONTAINER_TRIGGER(strAnimDict)
							//Don't bother tasking if we already close enough
					
							//Set local safety timer
							iSafetyTimer = GET_GAME_TIMER()
							iInvestigateContainerState = ciHICIsPlayerPlace
						ELSE
							OPEN_SEQUENCE_TASK(LocalSequenceIndex)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vContainerGotoPoint + <<0.0, 0.0, 0.1>>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, 0.2, 
														      ENAV_STOP_EXACTLY | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED, vContainerInitialRot.z)
							CLOSE_SEQUENCE_TASK(LocalSequenceIndex)
							TASK_PERFORM_SEQUENCE(LocalPlayerPed, LocalSequenceIndex)
							CLEAR_SEQUENCE_TASK(LocalSequenceIndex)
							
							//Set local safety timer
							iSafetyTimer = GET_GAME_TIMER()
							iInvestigateContainerState = ciHICIsPlayerPlace
						ENDIF
					ENDIF
				BREAK
				
				CASE ciHICIsPlayerPlace
					IF IS_PLAYER_CLOSE_ENOUGH_TO_CONTAINER_TRIGGER(strAnimDict) 
						IF bIsCaseInContainer
							IF NETWORK_HAS_CONTROL_OF_ENTITY(tempObj) 
							AND NETWORK_HAS_CONTROL_OF_ENTITY(objDawnPickUp) 
								//Play anims
								START_INVESTIGATE_CONTAINER_SYNCHRONIZED_SCENE(strAnimDict,tempObj, bIsCaseInContainer, objDawnPickUp)
								PRINTLN("[TRACKIFY] - [RCC MISSION] HANDLE_INVESTIGATE_CONTAINER - To ciHICHasAnimFinished 1")
								iInvestigateContainerState = ciHICHasAnimFinished
							ELSE
								PRINTLN("[TRACKIFY] - [RCC MISSION] HANDLE_INVESTIGATE_CONTAINER - trying to get control of entities")
								NETWORK_REQUEST_CONTROL_OF_ENTITY(tempObj)
								NETWORK_REQUEST_CONTROL_OF_ENTITY(objDawnPickUp)
							ENDIF
						ELSE
							IF NETWORK_HAS_CONTROL_OF_ENTITY(tempObj) 
								//Play anims
								START_INVESTIGATE_CONTAINER_SYNCHRONIZED_SCENE(strAnimDict,tempObj, bIsCaseInContainer, objDawnPickUp)
								PRINTLN("[TRACKIFY] - [RCC MISSION] HANDLE_INVESTIGATE_CONTAINER - To ciHICHasAnimFinished 2")
								iInvestigateContainerState = ciHICHasAnimFinished
							ELSE
								PRINTLN("[TRACKIFY] - [RCC MISSION] HANDLE_INVESTIGATE_CONTAINER - trying to get control of entities")
								NETWORK_REQUEST_CONTROL_OF_ENTITY(tempObj)
							ENDIF
						ENDIF
					ENDIF
					
					//Drop through if we can't get control, can't nav there
					IF MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer, 3000)
						START_INVESTIGATE_CONTAINER_SYNCHRONIZED_SCENE(strAnimDict,tempObj, bIsCaseInContainer, objDawnPickUp)
						PRINTLN("[TRACKIFY] - [RCC MISSION] HANDLE_INVESTIGATE_CONTAINER - couldn't get control or failed to nav to entity")
						iInvestigateContainerState = ciHICHasAnimFinished
						PRINTLN("[TRACKIFY] - [RCC MISSION] HANDLE_INVESTIGATE_CONTAINER - To ciHICHasAnimFinished 3")
					ENDIF
					
					IF iInvestigateContainerState = ciHICHasAnimFinished	//Reset timer in case it takes longer than 1 second to reach the container
						iSafetyTimer = GET_GAME_TIMER()
					ENDIF
				BREAK
				
				CASE ciHICHasAnimFinished
					//Send an event to tell everyone they should be showing collectable locally
					IF bIsCaseInContainer
						IF (IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID))
						AND GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID)) >= 0.2)
							IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet3,SBBOOL3_MINIGAME_SHOULD_SHOW_COLLECTABLE)
								PRINTLN("[TRACKIFY] - [RCC MISSION] HANDLE_INVESTIGATE_CONTAINER - ciHICHasAnimFinished - Sending event")
								BROADCAST_CONTAINER_MINIGAME_DATA_CHANGE(iThisObj,iPartToUse,bIsCaseInContainer,FALSE,FALSE,TRUE)
							ENDIF
						ENDIF
					ENDIF
					
					//Make the pick up object visible if it's in this container
					IF bIsCaseInContainer
						PRINTLN("[TRACKIFY] - [RCC MISSION] HANDLE_INVESTIGATE_CONTAINER - ciHICHasAnimFinished - bIsCaseInContainer")
						SET_ENTITY_LOCALLY_VISIBLE(objDawnPickUp)
						
						IF NETWORK_HAS_CONTROL_OF_ENTITY(objDawnPickUp)
							SET_ENTITY_ALPHA(objDawnPickUp, 255, FALSE)
						ELSE
							NETWORK_REQUEST_CONTROL_OF_ENTITY(objDawnPickUp)
						ENDIF
						
						IF (IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID))
						AND GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID)) >= 0.7)
							PRINTLN("[TRACKIFY] - [RCC MISSION] HANDLE_INVESTIGATE_CONTAINER - ciHICHasAnimFinished - Set team pickup")
							SET_TEAM_PICKUP_OBJECT(objDawnPickUp, MC_playerBD[iPartToUse].iTeam, TRUE)
							IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(objDawnPickUp, LocalPlayerPed)
								PRINTLN("[TRACKIFY] - [RCC MISSION] HANDLE_INVESTIGATE_CONTAINER - ciHICHasAnimFinished - attaching")
								ATTACH_PORTABLE_PICKUP_TO_PED(objDawnPickUp, LocalPlayerPed)
							ENDIF
						ENDIF
					ENDIF
					
					//Check if finished
					IF (IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID))
					AND GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID)) >= 1.0)
					OR (NOT IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID)) AND MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer, 1500))
						CLEAR_BIT(iBSContainerBeingInvestigated, iThisObj)
						PRINTLN("[TRACKIFY] - [RCC MISSION] HANDLE_INVESTIGATE_CONTAINER - ciHICHasAnimFinished - To Cleanup")
						iInvestigateContainerState = ciHICCleanUp
					ENDIF
				BREAK
				
				CASE ciHICCleanUp
					//Set found player found flag and print help message etc.
					
					BOOL bReady
					bReady = TRUE
					
					IF bIsCaseInContainer
						
						IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(objDawnPickUp)
							PRINTLN("[TRACKIFY] - [RCC MISSION] HANDLE_INVESTIGATE_CONTAINER - Requesting control of objDawnPickUp as we're done with the minigame and it's time to attach it!")
							NETWORK_REQUEST_CONTROL_OF_ENTITY(objDawnPickUp)
							bReady = FALSE
						ELSE
							IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(g_FMMC_STRUCT.iAdversaryModeType)
								PRINT_HELP("DR_CNIC_H2",DEFAULT_HELP_TEXT_TIME)
							ENDIF
							//Found it - Set this to flag so we can request control of the entity externally and for all players
							BROADCAST_CONTAINER_MINIGAME_DATA_CHANGE(iThisObj,iPartToUse,bIsCaseInContainer,TRUE,FALSE,TRUE) 

							INT iObj, iDawnPickup
							iDawnPickup = -1
							
							FOR iObj = 0 TO (MC_serverBD.iNumObjCreated - 1)
								IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iObj])
								AND NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iObj]) = objDawnPickUp
									iDawnPickup = iObj
									BREAKLOOP
								ENDIF
							ENDFOR
							
							SET_ENTITY_VISIBLE(objDawnPickUp, TRUE)
					
							INT iTeamLoop
							
							FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
								IF iDawnPickup = -1
								OR ((MC_serverBD_4.iObjPriority[iDawnPickup][iTeamLoop] <= MC_serverBD_4.iCurrentHighestPriority[iTeamLoop]) AND DOES_OBJECT_RULE_TYPE_REQUIRE_PICKUP(MC_serverBD_4.iObjRule[iDawnPickup][iTeamLoop]))
									SET_TEAM_PICKUP_OBJECT(objDawnPickUp, iTeamLoop, TRUE)
									PRINTLN("[TRACKIFY] - [RCC MISSION] HANDLE_INVESTIGATE_CONTAINER allowing object to be collected for team ",iTeamLoop)
								ENDIF
							ENDFOR
							
							IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(objDawnPickUp, LocalPlayerPed)
								IF NOT IS_PED_INJURED(LocalPlayerPed)
									PRINTLN("[TRACKIFY] - [RCC MISSION] HANDLE_INVESTIGATE_CONTAINER - Attaching objDawnPickUp to local player as we're done with the minigame")
									ATTACH_PORTABLE_PICKUP_TO_PED(objDawnPickUp, LocalPlayerPed)
								ENDIF
							#IF IS_DEBUG_BUILD
							ELSE
								PRINTLN("[TRACKIFY] - [RCC MISSION] HANDLE_INVESTIGATE_CONTAINER - We're done with the minigame, but the object is already attached to the player???")
							#ENDIF
							ENDIF
							
							//REMOVE_TRACKIFY_MULTIPLE_TARGET(iThisObj)
							
							SET_BIT(iTrackifyTargetProcessed, iThisObj)
						ENDIF
					ELSE
						//didn't find it
						IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(g_FMMC_STRUCT.iAdversaryModeType)
							PRINT_HELP("DR_CNIC",DEFAULT_HELP_TEXT_TIME)
						ENDIF
						PRINTLN("[TRACKIFY] - [RCC MISSION] HANDLE_INVESTIGATE_CONTAINER -  bIsCaseInContainer FALSE")
						IF NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
							PRINTLN("[TRACKIFY] - [RCC MISSION] HANDLE_INVESTIGATE_CONTAINER -  Switching lights on case off")
							SET_ENTITY_LIGHTS(tempObj, TRUE)
						ENDIF
						//Broadcast found state (even if nothing is found...you found nothing.
						BROADCAST_CONTAINER_MINIGAME_DATA_CHANGE(iThisObj,iPartToUse,bIsCaseInContainer,TRUE) 
						REMOVE_TRACKIFY_MULTIPLE_TARGET(iThisObj)
						//iTrackifyTargets[iThisObj] = -2	
						SET_BIT(iTrackifyTargetProcessed, iThisObj)
					ENDIF
					
					IF bReady
						CLEAN_UP_PLAYER_FROM_MINIGAME()
						CLEAN_UP_ASSETS_FOR_CONTAINER_MINIGAME()
						iInvestigateContainerState = ciHICInit
					ENDIF
				BREAK
				
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_CONTAINER_MINIGAME_EXTERNAL_LOGIC()
	//Once the trackify Obj is found, make it visible and turn on lights
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
		IF DOES_ENTITY_EXIST(objDawnPickUp)
		AND DOES_ENTITY_HAVE_DRAWABLE(objDawnPickUp)
			IF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_TARGET_FOUND)
				SET_ENTITY_LOCALLY_VISIBLE(objDawnPickUp)
				
				IF IS_SOUND_ID_VALID(iSoundIDDawnRaidPickup)
				AND HAS_SOUND_FINISHED(iSoundIDDawnRaidPickup)
					PRINTLN("[HANDLE_CONTAINER_MINIGAME_EXTERNAL_LOGIC] PLAY_SOUND_FROM_ENTITY(iSoundIDDawnRaidPickup, \"Transmitter_Beeps\", objDawnPickUp, \"DLC_GR_DR_Player_Sounds\")")
					PLAY_SOUND_FROM_ENTITY(iSoundIDDawnRaidPickup, "Transmitter_Beeps", objDawnPickUp, "DLC_GR_DR_Player_Sounds")
				ENDIF
				
				IF NOT IS_ENTITY_ATTACHED(objDawnPickUp)
					CLEAR_BIT(iLocalBoolCheck17, LBOOL17_PREVENT_PICKUP_OF_PICKUP)
					
					ENABLE_TRANSMITTER_LIGHT_ON_OBJECT(objDawnPickUp)
				ELSE
					ENABLE_TRANSMITTER_LIGHT_ON_OBJECT(objDawnPickUp, TRUE)
				ENDIF
			ELSE
				IF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_SHOULD_SHOW_COLLECTABLE)
					SET_ENTITY_LOCALLY_VISIBLE(objDawnPickUp)
				ELSE
					//SET_ENTITY_LOCALLY_INVISIBLE(objDawnPickUp)
				ENDIF
				
				IF IS_SOUND_ID_VALID(iSoundIDDawnRaidPickup)
				AND NOT HAS_SOUND_FINISHED(iSoundIDDawnRaidPickup)
					PRINTLN("[HANDLE_CONTAINER_MINIGAME_EXTERNAL_LOGIC] STOP_SOUND(iSoundIDDawnRaidPickup")
					STOP_SOUND(iSoundIDDawnRaidPickup)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		ENABLE_TRANSMITTER_LIGHT_ON_OBJECT(objDawnPickUp, TRUE)
		
		SET_ENTITY_LOCALLY_INVISIBLE(objDawnPickUp)
		
		IF IS_SOUND_ID_VALID(iSoundIDDawnRaidPickup)
		AND NOT HAS_SOUND_FINISHED(iSoundIDDawnRaidPickup)
			PRINTLN("[HANDLE_CONTAINER_MINIGAME_EXTERNAL_LOGIC] STOP_SOUND(iSoundIDDawnRaidPickup")
			STOP_SOUND(iSoundIDDawnRaidPickup)
		ENDIF
	ENDIF
ENDPROC

PROC DISABLE_FILTER_SWITCHES_WHILE_AIMING()

	IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM)
		SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_BLOCK_TOGGLING_NIGHTVISION_FILTER)
	ELSE
		CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_BLOCK_TOGGLING_NIGHTVISION_FILTER)
	ENDIF
	
ENDPROC

FUNC BOOL CREATE_CONTAINER_PICKUP_OBJECT(VECTOR vCreate , BOOL bCreateVisible = TRUE )
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_playerBD[iPartToUse].netMiniGameObj)
		PRINTLN("[OPEN] - [RCC MISSION] CREATE_CONTAINER_PICKUP_OBJECT - minigame id doesn't exist ")
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_adv_hdsec")))
		IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_adv_hdsec")))
			PRINTLN("[OPEN] - [RCC MISSION] CREATE_CONTAINER_PICKUP_OBJECT - model loaded ")
			IF NOT IS_BIT_SET(iLocalBoolCheck20,LBOOL20_OBJ_RES_CONTAINER_PICK_UP)
				PRINTLN("[OPEN] - [RCC MISSION] CREATE_CONTAINER_PICKUP_OBJECT - reserving model ")
				MC_RESERVE_NETWORK_MISSION_OBJECTS(1)
				SET_BIT(iLocalBoolCheck20,LBOOL20_OBJ_RES_CONTAINER_PICK_UP)
			ELSE
				IF NOT IS_BIT_SET(iLocalBoolCheck20,LBOOL20_OBJ_CRE_CONTAINER_PICK_UP)
					IF CAN_REGISTER_MISSION_OBJECTS(1)
						IF CREATE_NET_OBJ(MC_playerBD[iPartToUse].netMiniGameObj,INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_adv_hdsec")),vCreate, bIsLocalPlayerHost)
							SET_ENTITY_COLLISION(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj), FALSE)
							SET_ENTITY_INVINCIBLE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj), TRUE)
							SET_ENTITY_VISIBLE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj),bCreateVisible)
							//FREEZE_ENTITY_POSITION(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj),TRUE)
							
							IF NOT bCreateVisible
								SET_ENTITY_ALPHA(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj),0,FALSE)
							ENDIF
							SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_adv_hdsec")))
							CLEAR_BIT(iLocalBoolCheck20, LBOOL20_OBJ_CRE_CONTAINER_PICK_UP)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC DELETE_CONTAINER_PICKUP_OBJECT()
	OBJECT_INDEX tempObj
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_playerBD[iPartToUse].netMiniGameObj)
		tempObj = NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj)
		DELETE_OBJECT(tempObj)
		MC_RESERVE_NETWORK_MISSION_OBJECTS(-1)
		CLEAR_BIT(iLocalBoolCheck20,LBOOL20_OBJ_RES_CONTAINER_PICK_UP)
	ENDIF
ENDPROC

PROC PROCESS_INVESTIGATE_CONTAINER_MINI_GAME(OBJECT_INDEX tempObj,INT iobj)

	FLOAT fContainerObjRot
	FLOAT fGroundZTemp
	SEQUENCE_INDEX LocalSequenceIndex
	VECTOR vPickupSpawnLoc
	
	STRING strAnimDict = "anim@mp_mission@dr_objective"

	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet , cibsLEGACY_FMMC_MINI_GAME_TYPE_OPEN_CONTAINER)
		IF MC_playerBD[iPartToUse].iObjHacked = -1
			IF MC_serverBD.iObjHackPart[iobj] = -1
				IF MC_playerBD[iPartToUse].iObjHacking = -1
					//If close enough to the target or the client bit is set
					
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(tempObj) ,GET_ENTITY_COORDS(PLAYER_PED_ID()) ) < 8.0
						REQUEST_ASSETS_FOR_CONTAINER_MINIGAME()
						PRINTLN("[OPEN] - [RCC MISSION] PROCESS_INVESTIGATE_CONTAINER_MINI_GAME - requesting assets")
					ENDIF
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(tempObj),GET_ENTITY_COORDS(PLAYER_PED_ID()) ) < 2.0
						IF IS_NET_PLAYER_OK(PLAYER_ID())
							PRINTLN("[OPEN] - [RCC MISSION] PROCESS_INVESTIGATE_CONTAINER_MINI_GAME - player ok")
							//If input is pressed
							IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
							OR IS_BIT_SET(iLocalBoolCheck19,LBOOL19_TRIGGERED_CONTAINER_INVESTIGATION)
							
								PRINTLN("[OPEN] - [RCC MISSION] PROCESS_INVESTIGATE_CONTAINER_MINI_GAME - pressed")
								IF NOT IS_ANOTHER_PLAYER_BLOCKING_CONTAINER_LOCATION(tempObj,2.0)
									PRINTLN("[OPEN] - [RCC MISSION] PROCESS_INVESTIGATE_CONTAINER_MINI_GAME - not blocked")
									GET_PLAYER_READY_FOR_CONTAINER_MINIGAME()
									REQUEST_ASSETS_FOR_CONTAINER_MINIGAME()

									IF NOT IS_BIT_SET(iLocalBoolCheck19,LBOOL19_TRIGGERED_CONTAINER_INVESTIGATION)
										//Set local safety timer
										iSafetyTimer = GET_GAME_TIMER()
										SET_BIT(iLocalBoolCheck19,LBOOL19_TRIGGERED_CONTAINER_INVESTIGATION)
									ENDIF

									HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
									IF HAVE_CONTAINER_MINIGAME_ASSETS_LOADED()
									OR MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer, 7000)
										PRINTLN("[OPEN] - [RCC MISSION] PROCESS_INVESTIGATE_CONTAINER_MINI_GAME - triggering minigame")
										//Calculate scene position and goto position
										fContainerObjRot  = GET_ENTITY_HEADING(tempObj)
										vContainerPlaybackPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj, vContainerSceneOffsetFromObj)
										vContainerGotoPoint = GET_ANIM_INITIAL_OFFSET_POSITION(strAnimDict, "player_success", vContainerPlaybackPos, <<0.0, 0.0, fContainerObjRot>>)
										vContainerInitialRot = GET_ANIM_INITIAL_OFFSET_ROTATION(strAnimDict, "player_success", vContainerPlaybackPos, <<0.0, 0.0, fContainerObjRot>>)
										GET_GROUND_Z_FOR_3D_COORD(vContainerGotoPoint + <<0.0, 0.0, 0.5>>, fGroundZTemp)
										vContainerGotoPoint.z = fGroundZTemp
										
										SET_OBJ_NEAR(iObj)
										
										IF IS_PLAYER_CLOSE_ENOUGH_TO_CONTAINER_TRIGGER(strAnimDict)
											//Don't bother tasking if we already close enough
											
											//Set local safety timer
											iSafetyTimer = GET_GAME_TIMER()
											iInvestigateContainerState = ciHICIsPlayerPlace
											MC_playerBD[iPartToUse].iObjHacking = iobj
											PRINTLN("[OPEN] - [RCC MISSION] PROCESS_INVESTIGATE_CONTAINER_MINI_GAME -assigning MC_playerBD[iPartToUse].iObjHacking:",MC_playerBD[iPartToUse].iObjHacking)
											PRINTLN("[OPEN] - [RCC MISSION] PROCESS_INVESTIGATE_CONTAINER_MINI_GAME - close enough!!")
											CLEAR_BIT(iLocalBoolCheck19,LBOOL19_TRIGGERED_CONTAINER_INVESTIGATION)
										ELSE
											OPEN_SEQUENCE_TASK(LocalSequenceIndex)
												TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vContainerGotoPoint + <<0.0, 0.0, 0.1>>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, 0.2, 
																		      ENAV_STOP_EXACTLY | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED, vContainerInitialRot.z)
											CLOSE_SEQUENCE_TASK(LocalSequenceIndex)
											TASK_PERFORM_SEQUENCE(LocalPlayerPed, LocalSequenceIndex)
											CLEAR_SEQUENCE_TASK(LocalSequenceIndex)
											
											//Set local safety timer
											iSafetyTimer = GET_GAME_TIMER()
											iInvestigateContainerState = ciHICIsPlayerPlace
											MC_playerBD[iPartToUse].iObjHacking = iobj
											PRINTLN("[OPEN] - [RCC MISSION] PROCESS_INVESTIGATE_CONTAINER_MINI_GAME - task player")
											PRINTLN("[OPEN] - [RCC MISSION] PROCESS_INVESTIGATE_CONTAINER_MINI_GAME -assigning MC_playerBD[iPartToUse].iObjHacking:",MC_playerBD[iPartToUse].iObjHacking)
											CLEAR_BIT(iLocalBoolCheck19,LBOOL19_TRIGGERED_CONTAINER_INVESTIGATION)
										ENDIF
									ENDIF

								ENDIF
							ELSE
								//only show for local player close enough to use
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(tempObj) ,GET_ENTITY_COORDS(PLAYER_PED_ID()) ) < 2.0
									//Don't show help text if someone is blocking the container
									IF NOT IS_ANOTHER_PLAYER_BLOCKING_CONTAINER_LOCATION(tempObj,1.0)
										IF NOT IS_BROWSER_OPEN()
											DISPLAY_HELP_TEXT_THIS_FRAME_FOR_MINIGAME("DR_CNIC_H1",TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					// bail stuff here / reset etc.
				ENDIF
			ELSE
				PRINTLN("[OPEN] - [RCC MISSION] PROCESS_INVESTIGATE_CONTAINER_MINI_GAME MC_playerBD[iPartToUse].iObjHacking: ",MC_playerBD[iPartToUse].iObjHacking)
				IF MC_serverBD.iObjHackPart[iobj] = iPartToUse
				//AND MC_playerBD[iPartToUse].iObjHacking = iobj
					IF MC_playerBD[iPartToUse].iObjHacking != -1
						PRINTLN("[OPEN] - [RCC MISSION] PROCESS_INVESTIGATE_CONTAINER_MINI_GAME - iObjHacking != -1")
						PRINTLN("[OPEN] - [RCC MISSION] PROCESS_INVESTIGATE_CONTAINER_MINI_GAME MC_serverBD.iObjHackPart[iobj]: ",MC_serverBD.iObjHackPart[iobj])
						IF MC_serverBD.iObjHackPart[iobj] = iPartToUse
							PRINTLN("[OPEN] - [RCC MISSION] PROCESS_INVESTIGATE_CONTAINER_MINI_GAME - iObjHackPart[iobj] = iPartToUse")
							
							PRINTLN("[OPEN] - [RCC MISSION] PROCESS_INVESTIGATE_CONTAINER_MINI_GAME - iInvestigateContainerState",iInvestigateContainerState)
							
							SWITCH iInvestigateContainerState
								CASE ciHICIsPlayerPlace
									
									PRINTLN("[OPEN] - [RCC MISSION] PROCESS_INVESTIGATE_CONTAINER_MINI_GAME - created pick up")
									vPickupSpawnLoc = GET_ENTITY_COORDS(tempObj) 
									vPickupSpawnLoc.z = vPickupSpawnLoc.z - 2
									IF CREATE_CONTAINER_PICKUP_OBJECT(vPickupSpawnLoc)
										IF IS_PLAYER_CLOSE_ENOUGH_TO_CONTAINER_TRIGGER(strAnimDict) 
											IF NETWORK_HAS_CONTROL_OF_ENTITY(tempObj) 
											AND NETWORK_HAS_CONTROL_OF_ENTITY(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj)) 
												iSafetyTimer = GET_GAME_TIMER()
												//Play anims
												
												START_INVESTIGATE_CONTAINER_SYNCHRONIZED_SCENE(strAnimDict,tempObj, TRUE, NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj))
												iInvestigateContainerState = ciHICHasAnimFinished
											ELSE
												PRINTLN("[OPEN]- [RCC MISSION] HANDLE_INVESTIGATE_CONTAINER - trying to get control of entities")
												NETWORK_REQUEST_CONTROL_OF_ENTITY(tempObj)
												NETWORK_REQUEST_CONTROL_OF_ENTITY(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj))
											ENDIF
										ENDIF
									ENDIF
									
									//Drop through if we can't get control, can't nav there
									IF MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer, 3000)
										START_INVESTIGATE_CONTAINER_SYNCHRONIZED_SCENE(strAnimDict,tempObj, TRUE, NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj))
										PRINTLN("[OPEN] - [RCC MISSION] HANDLE_INVESTIGATE_CONTAINER - couldn't get control or failed to nav to entity")
										iSafetyTimer = GET_GAME_TIMER()
										iInvestigateContainerState = ciHICHasAnimFinished
									ENDIF
			
								BREAK
								
								CASE ciHICHasAnimFinished
									IF (IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID))
									AND GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID)) < 0.6)
										IF DOES_ENTITY_EXIST(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj))
											SET_ENTITY_VISIBLE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj),TRUE)
											SET_ENTITY_LOCALLY_VISIBLE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj))
											SET_ENTITY_ALPHA(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj),255,FALSE)
											PRINTLN("[OPEN] - [RCC MISSION] TRYING TO MAKE VISIBLE???")
											IF NETWORK_HAS_CONTROL_OF_ENTITY(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj))
												SET_ENTITY_ALPHA(tempObj,255,FALSE)
											ELSE
												NETWORK_REQUEST_CONTROL_OF_ENTITY(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj))
											ENDIF
										ENDIF
									ENDIF
									
									IF (IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID))
									AND GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID)) >=  0.6962)
										SET_ENTITY_VISIBLE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj),FALSE)
										SET_ENTITY_ALPHA(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj),255,FALSE)
										SET_ENTITY_LOCALLY_INVISIBLE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj))
										IF NETWORK_HAS_CONTROL_OF_ENTITY(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj))
											SET_ENTITY_ALPHA(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj),0,FALSE)
										ENDIF
									ENDIF
									
									//Check if finished
									IF (IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID))
									AND GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID)) >= 1.0)
									OR (NOT IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iPartToUse].iSynchSceneID)) AND MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer, 1000))
										PRINTLN("[OPEN] - [RCC MISSION] PROCESS_INVESTIGATE_CONTAINER_MINI_GAME - anim finished")
										CLEAN_UP_PLAYER_FROM_MINIGAME()
										iInvestigateContainerState = ciHICCleanUp
									ENDIF
									
									//Drop through if we can't get control, can't nav there
									IF MANAGE_THIS_MINIGAME_TIMER(iSafetyTimer, 7000)
										CLEAN_UP_PLAYER_FROM_MINIGAME()
										iInvestigateContainerState = ciHICCleanUp
									ENDIF
								BREAK
								
								CASE ciHICCleanUp
									PRINTLN("[OPEN] - [RCC MISSION] PROCESS_INVESTIGATE_CONTAINER_MINI_GAME - cleaning up - setting object as hacked")
									DELETE_CONTAINER_PICKUP_OBJECT()
									CLEAN_UP_PLAYER_FROM_MINIGAME()
									CLEAN_UP_ASSETS_FOR_CONTAINER_MINIGAME()
									NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
									MC_playerBD[iPartToUse].iObjHacked = iobj
									RESET_OBJ_HACKING_INT()
									
								BREAK

							ENDSWITCH
						ENDIF
					ENDIF
				ELSE
					IF MC_playerBD[iPartToUse].iObjHacking = iobj
						RESET_OBJ_HACKING_INT()
						CLEAN_UP_PLAYER_FROM_MINIGAME()
						NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
					ENDIF
				ENDIF

			ENDIF
		ENDIF
	ENDIF


ENDPROC


PROC PROCESS_DAWN_RAID_EVERY_FRAME_CHECKS()

	#IF IS_DEBUG_BUILD
		IF bDawnRaidWarpToObject
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F5)
				IF DOES_ENTITY_EXIST(objDawnPickUp)
					SAFE_WARP_PLAYER_TO_COORDINATE(GET_ENTITY_COORDS(objDawnPickUp))
				ENDIF
			ENDIF
		ENDIF
	#ENDIF
	
	//Set HD texture on the cargobob is this mode
	INT i
	REPEAT (FMMC_MAX_TEAMS*2) i
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.niDZSpawnVehicle[i])
			VEHICLE_INDEX viVeh = NET_TO_VEH(MC_serverBD_1.niDZSpawnVehicle[i])
			IF NOT IS_ENTITY_DEAD(viVeh)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.niDZSpawnVehicle[i])
					IF GET_ENTITY_MODEL(viVeh) = CARGOBOB4
						SET_FORCE_HD_VEHICLE(viVeh, TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	//Ghost the players in the cargobob so they can't block the door
	i = 0
	REPEAT (FMMC_MAX_TEAMS*2) i
		IF i = iSpawnCargobobGhostedID
		OR iSpawnCargobobGhostedID = -1
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.niDZSpawnVehicle[i])
				VEHICLE_INDEX viVeh = NET_TO_VEH(MC_serverBD_1.niDZSpawnVehicle[i])
				IF NOT IS_ENTITY_DEAD(viVeh)
					VECTOR vHeliGhostCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viVeh, <<0,-6.25,-1.0>>)
					IF GET_DISTANCE_BETWEEN_COORDS(vHeliGhostCoords, GET_ENTITY_COORDS(LocalPlayerPed)) < 1.75
						IF HAS_NET_TIMER_STARTED(tdCargobobDoorBlockTimer)
							IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdCargobobDoorBlockTimer) > ciCARGOBOB_DOOR_BLOCK_TIME
								IF NOT IS_ENTITY_A_GHOST(LocalPlayerPed)
									PRINTLN("[KH] PROCESS_DAWN_RAID_EVERY_FRAME_CHECKS - PLAYER GHOSTED")
									SET_LOCAL_PLAYER_AS_GHOST(TRUE, FALSE)
									RESET_ENTITY_ALPHA(LocalPlayerPed)
									iSpawnCargobobGhostedID = i
									RESET_NET_TIMER(tdCargobobDoorBlockTimer)
									BREAKLOOP
								ENDIF
							ENDIF
						ELSE
							START_NET_TIMER(tdCargobobDoorBlockTimer)
						ENDIF
					ELSE
						IF i = iSpawnCargobobGhostedID
							IF HAS_NET_TIMER_STARTED(tdCargobobDoorBlockTimer)
								RESET_NET_TIMER(tdCargobobDoorBlockTimer)
							ENDIF
						ENDIF
						IF IS_ENTITY_A_GHOST(LocalPlayerPed)
							PRINTLN("[KH] PROCESS_DAWN_RAID_EVERY_FRAME_CHECKS - PLAYER GHOST RESET")
							SET_LOCAL_PLAYER_AS_GHOST(FALSE, FALSE)
							RESET_ENTITY_ALPHA(LocalPlayerPed)
							iSpawnCargobobGhostedID = -1
						ENDIF
					ENDIF	
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC MANAGE_TRACKIFY_LOGIC(BOOL bEnableTrackify = TRUE)

	VEHICLE_INDEX tempVeh
	OBJECT_INDEX  tempObj
	INT iThisVehLoop
	INT iThisObjLoop
	INT iTargetLoop
	INT iMiniGameLoop
	INT iTrackifyTargetsArrayLoop
	
	VECTOR vPlayerCoords
	VECTOR vObjectCoords
	
	BOOL bDisableNightVision = FALSE
	
	INT iFindCaseLoop
	OBJECT_INDEX  tempCaseObj
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(g_FMMC_STRUCT.iAdversaryModeType)
		DISABLE_FILTER_SWITCHES_WHILE_AIMING()
	ENDIF
	
	//Prevent Trackify use while the player is skydiving and set play unarmed
	IF iTrackifyProgress > 0
		IF GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_SKYDIVING 
		OR GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_DEPLOYING
		OR GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_PARACHUTING
			//If we're in dawn raid we want to let the player still use trackify while falling
			IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(g_FMMC_STRUCT.iAdversaryModeType)
				IF !bTrackifyDisabled
					ENABLE_MULTIPLAYER_TRACKIFY_APP(FALSE)
					bTrackifyDisabled = TRUE
				ENDIF
			ENDIF
		ELSE
			IF bTrackifyDisabled
				HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
				ENABLE_MULTIPLAYER_TRACKIFY_APP(TRUE)
				SET_NUMBER_OF_MULTIPLE_TRACKIFY_TARGETS(MAX_TRACKIFY_TARGETS)
				bTrackifyDisabled = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	//Prevent Trackify during Dawn Raid sudden death
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(g_FMMC_STRUCT.iAdversaryModeType)
		IF IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
			ENABLE_MULTIPLAYER_TRACKIFY_APP(FALSE)
			
			iTrackifyProgress = 100	//Invalid State so it does nothing!
			
		 	IF IS_CELLPHONE_TRACKIFY_IN_USE()
				HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	BOOL bContainerBeingInvestigated = FALSE
	IF iThisContainerNumBeingInvestigated > -1
		IF IS_BIT_SET(iBSContainerBeingInvestigated, iThisContainerNumBeingInvestigated)		
			bContainerBeingInvestigated = TRUE
		ENDIF
	ENDIF	
	
	IF IS_CELLPHONE_TRACKIFY_IN_USE()
	OR bContainerBeingInvestigated
		WEAPON_TYPE wtCurrent = WEAPONTYPE_UNARMED
		
		GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtCurrent)
		
		IF wtCurrent != WEAPONTYPE_INVALID	//No weapon cached...
		AND wtCurrent != WEAPONTYPE_UNARMED
		AND wtTrackifyCache != wtCurrent
			wtTrackifyCache = wtCurrent
			
			PRINTLN("[MANAGE_TRACKIFY_LOGIC] Cached weapon ", GET_WEAPON_NAME(wtTrackifyCache))
		ENDIF
		
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
		
		IF GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_SKYDIVING 
		OR GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_DEPLOYING
		OR GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_PARACHUTING	
		OR GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_LANDING
			IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(g_FMMC_STRUCT.iAdversaryModeType)
				ENABLE_MULTIPLAYER_TRACKIFY_APP(FALSE)
				HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
				iTrackifyProgress = 99
			ENDIF
		ENDIF
	ELSE
		IF wtTrackifyCache != WEAPONTYPE_INVALID
		AND wtTrackifyCache != WEAPONTYPE_UNARMED
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtTrackifyCache)
			
			PRINTLN("[MANAGE_TRACKIFY_LOGIC] Restored weapon ", GET_WEAPON_NAME(wtTrackifyCache))
			
			wtTrackifyCache = WEAPONTYPE_UNARMED
		ENDIF
	ENDIF
	
	//Reset Trackify if the rule has now disabled it.
	IF bEnableTrackify = FALSE
		IF iTrackifyProgress > 0
			iTrackifyProgress = 0
		ENDIF
	ENDIF
	
	//Local process for players investigating container
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(g_FMMC_STRUCT.iAdversaryModeType)
		IF IS_NET_PLAYER_OK(PLAYER_ID()) AND iThisContainerNumBeingInvestigated != -1
			PROCESS_CLIENT_INVESTIGATE_CONTAINER(oiCurrentContainer, iThisContainerNumBeingInvestigated)
		ENDIF
		//g_iDawnRaidPickupCarrierPart = -1
		
		IF DOES_ENTITY_EXIST(objDawnPickUp)
			//IF IS_ENTITY_ATTACHED_TO_ANY_PED(objDawnPickUp)
				INT iPartLoop
				
				REPEAT GET_MAXIMUM_PLAYERS_ON_MISSION() iPartLoop
					PARTICIPANT_INDEX currentParticipant = INT_TO_PARTICIPANTINDEX(iPartLoop)
					
					IF NETWORK_IS_PARTICIPANT_ACTIVE(currentParticipant)
						PLAYER_INDEX currentPlayer = NETWORK_GET_PLAYER_INDEX(currentParticipant)
						
						IF IS_ENTITY_ATTACHED_TO_ENTITY(objDawnPickUp, GET_PLAYER_PED(currentPlayer))
							g_iDawnRaidPickupCarrierPart = iPartLoop
							
							IF g_iDawnRaidPickupCarrierPart != -1
							AND !bDawnRaidContainerFound
								PRINTLN("[MANAGE_TRACKIFY_LOGIC] - Dawn Raid container found, setting bDawnRaidContainerFound")
								bDawnRaidContainerFound = TRUE 
							ENDIF
							
							#IF IS_DEBUG_BUILD
							IF debug_iDawnRaidPickupCarrierPart != g_iDawnRaidPickupCarrierPart
								PRINTLN("g_iDawnRaidPickupCarrierPart = ", g_iDawnRaidPickupCarrierPart)
								
								debug_iDawnRaidPickupCarrierPart = g_iDawnRaidPickupCarrierPart
							ENDIF
							#ENDIF
							
							BREAKLOOP
						ELSE
							IF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_TARGET_FOUND)
								IF g_iDawnRaidPickupCarrierPart = iPartLoop
									PRINTLN("[MANAGE_TRACKIFY_LOGIC] - Container dropped by carrying player, resetting global")
									g_iDawnRaidPickupCarrierPart = -1
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF g_iDawnRaidPickupCarrierPart = iPartLoop
							PRINTLN("[MANAGE_TRACKIFY_LOGIC] - Player disconnected, resetting global")
							g_iDawnRaidPickupCarrierPart = -1
							IF bIsLocalPlayerHost
								iDisconnectedDawnRaidPart = iPartLoop
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
			//ENDIF
		ENDIF
	ENDIF
	
	//Host process for players investigating container (timeouts)
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(g_FMMC_STRUCT.iAdversaryModeType)
		IF GET_BITS_IN_RANGE(MC_serverBD.iServerObjectBeingInvestigatedBitSet, 0, MAX_TRACKIFY_TARGETS - 1) != 0
		
			INT iContainer
			
			FOR iContainer = 0 TO MAX_TRACKIFY_TARGETS - 1
				IF IS_BIT_SET(MC_serverBD.iServerObjectBeingInvestigatedBitSet, iContainer)
					PROCESS_SERVER_INVESTIGATE_CONTAINER(iContainer)
					IF NOT IS_BIT_SET(BitSet_CellphoneTU, g_BSTU_REMOVE_MP_TRACKIFY_TARGET_0 + iContainer)
						PRINTLN("[AW_MISSION] - [RCC MISSION][TRACKIFY] - REMOVING DAWN RAID TARGET ", iContainer)
						REMOVE_TRACKIFY_MULTIPLE_TARGET(iContainer)
						SET_BIT(iTrackifyTargetProcessed, iContainer)
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
	ENDIF

	//Once the mission begins find the case object and make it invisible (alpha = 0) and make it non collectable.
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciDAWN_RAID_ENABLE_MODE)
		IF IS_VECTOR_ZERO(vTargetObj)
			FOR iFindCaseLoop = 0 TO FMMC_MAX_NUM_OBJECTS - 1
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iFindCaseLoop])
					tempCaseObj = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iFindCaseLoop])
					
					IF GET_ENTITY_MODEL(tempCaseObj) = INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_adv_hdsec"))
						IF IS_OBJECT_A_PORTABLE_PICKUP(tempCaseObj)
							objDawnPickUp = tempCaseObj
							vTargetObj = GET_ENTITY_COORDS(tempCaseObj)
							bDawnRaidCratesLightsOn = TRUE
							
							INT iTeam
							
							IF NETWORK_HAS_CONTROL_OF_ENTITY(objDawnPickUp)
								REPEAT MC_serverBD.iNumberOfTeams iTeam
									SET_TEAM_PICKUP_OBJECT(objDawnPickUp, iTeam, FALSE)
									PRINTLN("[AW_MISSION] - [RCC MISSION][TRACKIFY] - SET_TEAM_PICKUP_OBJECT(objDawnPickUp, ", iTeam, ", FALSE)")
								ENDREPEAT
							ENDIF
							
							SET_BIT(iLocalBoolCheck17, LBOOL17_PREVENT_PICKUP_OF_PICKUP)
							
							PRINTLN("[AW_MISSION] - [RCC MISSION][TRACKIFY] - vTargetObj ", vTargetObj)
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
		
		iFindCaseLoop = 0
		IF bDawnRaidContainerFound
		AND bDawnRaidCratesLightsOn
			FOR iFindCaseLoop = 0 TO FMMC_MAX_NUM_OBJECTS - 1
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iFindCaseLoop])
					OBJECT_INDEX crateObj = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iFindCaseLoop])
					IF GET_ENTITY_MODEL(crateObj) = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_adv_case"))
						IF NETWORK_HAS_CONTROL_OF_ENTITY(crateObj)
							SET_ENTITY_LIGHTS(crateObj, TRUE)
							UPDATE_LIGHTS_ON_ENTITY(crateObj)
							PRINTLN("[AW_MISSION] - [RCC MISSION][TRACKIFY] - Case: ", iFindCaseLoop, " lights switched off")
						ELSE
							PRINTLN("[AW_MISSION] - [RCC MISSION][TRACKIFY] - Case: ", iFindCaseLoop, " player doesn't have control, can't switch lights on this one")
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
			PRINTLN("[AW_MISSION] - [RCC MISSION][TRACKIFY] - Switching lights off on the crates as the object has been found!")
			bDawnRaidCratesLightsOn = FALSE
		ENDIF
	ENDIF
	
	SWITCH iTrackifyProgress

		CASE 0
			//Initialise etc.
			IF bEnableTrackify
				PRINTLN("[AW_MISSION] - [RCC MISSION] - Trackify turned on ")
				ENABLE_MULTIPLAYER_TRACKIFY_APP(TRUE)
				
				
				FOR iThisObjLoop = 0 TO FMMC_MAX_NUM_OBJECTS - 1
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iThisObjLoop].iObjectBitSet , cibsOBJ_EnableTrackify)
						IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iThisObjLoop])
							iNumberOfTrackifyTargets++
							PRINTLN("[MJL] OBJ iNumberOfTrackifyTargets = ", iNumberOfTrackifyTargets)
						ENDIF
					ENDIF
				ENDFOR
				FOR iThisVehLoop = 0 TO FMMC_MAX_VEHICLES - 1	
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iThisVehLoop].iVehBitsetTwo , ciFMMC_VEHICLE2_TRACKIFY_TARGET)
						IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iThisVehLoop])
							iNumberOfTrackifyTargets++
							PRINTLN("[MJL] VEH iNumberOfTrackifyTargets = ", iNumberOfTrackifyTargets)
						ENDIF
					ENDIF
				ENDFOR
				
				PRINTLN("[ML] NumberOfTrackifyTargets = ", iNumberOfTrackifyTargets)
				
				SET_NUMBER_OF_MULTIPLE_TRACKIFY_TARGETS(iNumberOfTrackifyTargets)
				
				iAcquiredTargets = 0
				FOR iTrackifyTargetsArrayLoop = 0 TO iNumberOfTrackifyTargets-1
					iTrackifyTargets[iTrackifyTargetsArrayLoop] = -1
				ENDFOR
				
				bTrackObjects = FALSE
				bTrackifyDisabled = FALSE

				iTrackifyProgress ++
			ELSE
				IF IS_BIT_SET(BitSet_CellphoneTU, g_BSTU_ENABLE_MP_TRACKIFY)
					ENABLE_MULTIPLAYER_TRACKIFY_APP(FALSE)
					HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
					PRINTLN("[AW_MISSION] - [RCC MISSION] - Trackify turned off ")
				ENDIF
				
				iTrackifyProgress = 99
			ENDIF
		BREAK
		
		CASE 1

//          For multiple targets
			IF iAcquiredTargets < MAX_TRACKIFY_TARGETS
				FOR iThisVehLoop = 0 TO FMMC_MAX_VEHICLES - 1
					//PRINTLN("[AW_MISSION] - [RCC MISSION] - iAcquiredTargets :",iAcquiredTargets)
					//PRINTLN("[AW_MISSION] - [RCC MISSION] - iThisVehLoop: ", iThisVehLoop)
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iThisVehLoop].iVehBitsetTwo, ciFMMC_VEHICLE2_TRACKIFY_TARGET)
						PRINTLN("[AW_MISSION] - [RCC MISSION] - Adding Vehicle : ",iThisVehLoop," is a trackify target.")
						INT iTeam
						iTeam = MC_playerBD[iLocalPart].iteam
						IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
							IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iThisVehLoop])
							AND IS_THIS_GANG_OPS_AND_BOOL_OR_NOT_GANG_OPS((MC_serverBD_4.iVehPriority[iThisVehLoop][iTeam] = MC_serverBD_4.iCurrentHighestPriority[iTeam]))
								tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iThisVehLoop])
								IF IS_VEHICLE_DRIVEABLE(tempVeh)
									IF iTrackifyTargets[iAcquiredTargets] = -1 AND iAcquiredTargets < iNumberOfTrackifyTargets
										PRINTLN("[AW_MISSION] - [RCC MISSION] - iTrackifyTargets[ ",iAcquiredTargets, "] has been asigned with vehicle: ", iThisVehLoop ) 
										iTrackifyTargets[iAcquiredTargets] = iThisVehLoop
										iAcquiredTargets ++
									ELSE
										BREAKLOOP
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
				
				//For Objects - Dawn Raid
				FOR iThisObjLoop = 0 TO FMMC_MAX_NUM_OBJECTS - 1
					//PRINTLN("[AW_MISSION] - [RCC MISSION] - iAcquiredTargets :",iAcquiredTargets)
					//PRINTLN("[AW_MISSION] - [RCC MISSION] - iThisObjLoop: ", iThisObjLoop)
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iThisObjLoop].iObjectBitSet , cibsOBJ_EnableTrackify)
						PRINTLN("[AW_MISSION] - [RCC MISSION] - Adding Object : ",iThisObjLoop," is a trackify target.")
						IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iThisObjLoop])
							tempObj = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iThisObjLoop])
							IF NOT IS_ENTITY_DEAD(tempObj)
								IF iAcquiredTargets < MAX_TRACKIFY_TARGETS
									IF iTrackifyTargets[iAcquiredTargets] = -1 AND iAcquiredTargets < iNumberOfTrackifyTargets
										PRINTLN("[AW_MISSION] - [RCC MISSION] - iTrackifyTargets[ ",iAcquiredTargets, "] has been asigned with obj: ", iThisObjLoop ) 
										iTrackifyTargets[iAcquiredTargets] = iThisObjLoop
										iAcquiredTargets ++
										//this flag is set
										bTrackObjects = TRUE
									ELSE
										BREAKLOOP
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDFOR

				
			ENDIF
	
			IF iAcquiredTargets > 0
				//For vehicle trackify stuff used in Heists - don't change 
				IF NOT bTrackObjects
					IF iTrackifyTargets[0] >= 0
						IF MC_serverBD_4.iVehPriority[iTrackifyTargets[0]][mc_playerBD[iPartToUse].iteam]  != MC_ServerBD_4.iCurrentHighestPriority[mc_playerBD[iPartToUse].iteam]
							PRINTLN("[AW_MISSION] - [RCC MISSION] [TRACKIFY] - REMOVE_TRACKIFY_MULTIPLE_TARGET ",0 ) 
							REMOVE_TRACKIFY_MULTIPLE_TARGET(0)
							iTrackifyTargets[0] = -2
						ENDIF
					ENDIF
					
					IF iTrackifyTargets[1] >= 0
						IF MC_serverBD_4.iVehPriority[iTrackifyTargets[1]][mc_playerBD[iPartToUse].iteam]  != MC_ServerBD_4.iCurrentHighestPriority[mc_playerBD[iPartToUse].iteam]
							PRINTLN("[AW_MISSION] - [RCC MISSION] [TRACKIFY] - REMOVE_TRACKIFY_MULTIPLE_TARGET ",1 ) 
							REMOVE_TRACKIFY_MULTIPLE_TARGET(1)
							iTrackifyTargets[1] = -2
						ENDIF
					ENDIF

					IF iTrackifyTargets[2] >= 0
						IF MC_serverBD_4.iVehPriority[iTrackifyTargets[2]][mc_playerBD[iPartToUse].iteam]  != MC_ServerBD_4.iCurrentHighestPriority[mc_playerBD[iPartToUse].iteam]
							PRINTLN("[AW_MISSION] - [RCC MISSION] [TRACKIFY] - REMOVE_TRACKIFY_MULTIPLE_TARGET ",2 ) 
							REMOVE_TRACKIFY_MULTIPLE_TARGET(2)
							iTrackifyTargets[2] = -2
						ENDIF
					ENDIF
					
					IF iTrackifyTargets[3] >= 0
						IF MC_serverBD_4.iVehPriority[iTrackifyTargets[3]][mc_playerBD[iPartToUse].iteam]  != MC_ServerBD_4.iCurrentHighestPriority[mc_playerBD[iPartToUse].iteam]
							PRINTLN("[AW_MISSION] - [RCC MISSION] [TRACKIFY] - REMOVE_TRACKIFY_MULTIPLE_TARGET ",3 )
							REMOVE_TRACKIFY_MULTIPLE_TARGET(3)
							iTrackifyTargets[3] = -2
						ENDIF
					ENDIF
				ELSE

					// Dawn Raid Minigame Logic
					#IF IS_DEBUG_BUILD IF bDebugPrintContainer PRINTLN("[TRACKIFY] sc_DebugPrintContainer")	ENDIF	#ENDIF
					
					IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(g_FMMC_STRUCT.iAdversaryModeType)
						#IF IS_DEBUG_BUILD IF bDebugPrintContainer PRINTLN("[TRACKIFY] IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID")	ENDIF	#ENDIF
						IF bLocalPlayerPedOK
							IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
								#IF IS_DEBUG_BUILD IF bDebugPrintContainer PRINTLN("[TRACKIFY] NOT IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)")	ENDIF	#ENDIF
								
								FOR iMiniGameLoop = 0 TO MAX_TRACKIFY_TARGETS -1
									#IF IS_DEBUG_BUILD IF bDebugPrintContainer PRINTLN("[TRACKIFY] iMiniGameLoop = ", iMiniGameLoop)	ENDIF	#ENDIF
									
									IF iTrackifyTargets[iMiniGameLoop] >= 0
									AND NOT IS_BIT_SET(iTrackifyTargetProcessed, iMiniGameLoop)
										#IF IS_DEBUG_BUILD IF bDebugPrintContainer PRINTLN("iTrackifyTargets[iMiniGameLoop] = ", iTrackifyTargets[iMiniGameLoop])	ENDIF	#ENDIF
										
										IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iTrackifyTargets[iMiniGameLoop]])
											objTrackifyTarget[iMiniGameLoop] = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iTrackifyTargets[iMiniGameLoop]])
											
											#IF IS_DEBUG_BUILD IF bDebugPrintContainer PRINTLN("[TRACKIFY] NETWORK_DOES_NETWORK_ID_EXIST")	ENDIF	#ENDIF
											
											//If close enough to the target or the client bit is set
											IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(objTrackifyTarget[iMiniGameLoop]), GET_ENTITY_COORDS(PLAYER_PED_ID()) ) < 2.0
											OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_OBJ_0_FOUND + iMiniGameLoop)
												#IF IS_DEBUG_BUILD IF bDebugPrintContainer PRINTLN("[TRACKIFY] IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_OBJ_0_FOUND + iMiniGameLoop) = ", IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_OBJ_0_FOUND + iMiniGameLoop))	ENDIF	#ENDIF
												
												IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(objTrackifyTarget[iMiniGameLoop]), GET_ENTITY_COORDS(PLAYER_PED_ID()) ) < 2.0
														bDisableNightVision = TRUE
												ENDIF
												
												vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
												vObjectCoords = GET_ENTITY_COORDS(objTrackifyTarget[iMiniGameLoop])
												
												FLOAT fActivationRange
												
												IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iTrackifyTargets[iMiniGameLoop]].fActivationRangeOverride != 0.0
													fActivationRange = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iTrackifyTargets[iMiniGameLoop]].fActivationRangeOverride
												ELSE
													fActivationRange = 2.0
												ENDIF
												
												#IF IS_DEBUG_BUILD 
												IF bDebugPrintContainer
													PRINTLN("[TRACKIFY] IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT) = ", IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT))
													PRINTLN("[TRACKIFY] IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_OBJ_0_FOUND + iMiniGameLoop) = ", IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_OBJ_0_FOUND + iMiniGameLoop))
													PRINTLN("[TRACKIFY] IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_VEH_EXIT) = ", IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_VEH_EXIT))
													PRINTLN("[TRACKIFY] manualRespawnState != eManualRespawnState_OKAY_TO_SPAWN = ", manualRespawnState != eManualRespawnState_OKAY_TO_SPAWN)
													PRINTLN("[TRACKIFY] HAS_NET_TIMER_STARTED(timerManualRespawn) = ", HAS_NET_TIMER_STARTED(timerManualRespawn))
													PRINTLN("[TRACKIFY] GET_DISTANCE_BETWEEN_COORDS(vObjectCoords, vPlayerCoords) = ", GET_DISTANCE_BETWEEN_COORDS(vObjectCoords, vPlayerCoords))
													PRINTLN("[TRACKIFY] ABSF(vObjectCoords.Z - vPlayerCoords.Z) = ", ABSF(vObjectCoords.Z - vPlayerCoords.Z))
													PRINTLN("[TRACKIFY] IS_ANOTHER_PLAYER_BLOCKING_CONTAINER_LOCATION(objTrackifyTarget[iMiniGameLoop], 1.0) = ", IS_ANOTHER_PLAYER_BLOCKING_CONTAINER_LOCATION(objTrackifyTarget[iMiniGameLoop], 1.0))
													PRINTLN("[TRACKIFY] IS_BIT_SET(MC_serverBD.iServerObjectBeingInvestigatedBitSet, iMiniGameLoop) = ", IS_BIT_SET(MC_serverBD.iServerObjectBeingInvestigatedBitSet, iMiniGameLoop))
													PRINTLN("[TRACKIFY] fActivationRange = ", fActivationRange)
												ENDIF
												#ENDIF
												
												//If input is pressed
												IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
												OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_OBJ_0_FOUND + iMiniGameLoop))
												AND NOT (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_VEH_EXIT)	//
												OR manualRespawnState != eManualRespawnState_OKAY_TO_SPAWN		//	Manual Respawn Checks
												OR HAS_NET_TIMER_STARTED(timerManualRespawn))					//
													IF GET_DISTANCE_BETWEEN_COORDS(vObjectCoords, vPlayerCoords) < fActivationRange
													AND ABSF(vObjectCoords.Z - vPlayerCoords.Z) < 1.2
														IF NOT IS_ANOTHER_PLAYER_BLOCKING_CONTAINER_LOCATION(objTrackifyTarget[iMiniGameLoop], 1.0)
															IF NOT IS_BIT_SET(MC_serverBD.iServerObjectBeingInvestigatedBitSet, iMiniGameLoop) 
																HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
																//local player only
																
																IF NETWORK_IS_HOST()
																	SET_BIT(MC_serverBD.iServerObjectBeingInvestigatedBitSet, iMiniGameLoop) 
																ENDIF
																
																PRINTLN("[AW_MISSION] - [RCC MISSION] [TRACKIFY] - Player has begun investigation")
																PRINTLN("[AW_MISSION] - [RCC MISSION] [TRACKIFY] - iThisContainerNumBeingInvestigated :", iThisContainerNumBeingInvestigated, ", distance to case = ", GET_DISTANCE_BETWEEN_COORDS(vTargetObj, GET_ENTITY_COORDS(PLAYER_PED_ID())))
																
																IF GET_DISTANCE_BETWEEN_COORDS(vTargetObj, GET_ENTITY_COORDS(PLAYER_PED_ID())) > 4.0
																	bIsCaseInContainer = FALSE
																	PRINTLN("[AW_MISSION] - [RCC MISSION] [TRACKIFY] - Player has begun investigation - case not here!")
																ELSE
																	bIsCaseInContainer = TRUE
																	PRINTLN("[AW_MISSION] - [RCC MISSION] [TRACKIFY] - Player has begun investigation - case here")
																ENDIF
																
																oiCurrentContainer = objTrackifyTarget[iMiniGameLoop]
																iThisContainerNumBeingInvestigated = iMiniGameLoop
																SET_BIT(iBSContainerBeingInvestigated, iMiniGameLoop)
																
																//Send an event to store the object and player using it and if the transmitor is in it
																BROADCAST_CONTAINER_MINIGAME_DATA_CHANGE(iMiniGameLoop, iPartToUse, bIsCaseInContainer)
																MC_playerBD[iPartToUse].iObjHacking = iTrackifyTargets[iMiniGameLoop]
															ENDIF
														ENDIF
													ENDIF
													
													IF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_OBJ_0_FOUND + iMiniGameLoop)
													AND IS_ENTITY_ATTACHED_TO_ANY_PED(objDawnPickUp)
														//Do this for everyone
														PRINTLN("[AW_MISSION] - [RCC MISSION] [TRACKIFY] - REMOVE_TRACKIFY_MULTIPLE_TARGET ", iMiniGameLoop )
														REMOVE_TRACKIFY_MULTIPLE_TARGET(iMiniGameLoop)
														SET_BIT(iTrackifyTargetProcessed, iMiniGameLoop)
														//iTrackifyTargets[iMiniGameLoop] = -2	//No longer clearing as we need the object
													ENDIF	
												ELSE
													//only show for local player close enough to use
													IF GET_DISTANCE_BETWEEN_COORDS(vObjectCoords, vPlayerCoords) < fActivationRange
													AND ABSF(vObjectCoords.Z - vPlayerCoords.Z) < 1.2
														//Don't show help text if someone is blocking the container
														IF NOT IS_BIT_SET(MC_serverBD.iServerObjectBeingInvestigatedBitSet, iMiniGameLoop)
															IF NOT IS_ANOTHER_PLAYER_BLOCKING_CONTAINER_LOCATION(objTrackifyTarget[iMiniGameLoop], 1.0)
																IF NOT IS_BROWSER_OPEN()
																AND NOT IS_CELLPHONE_CAMERA_IN_USE()
																	DISPLAY_HELP_TEXT_THIS_FRAME_FOR_MINIGAME("DR_CNIC_H1", TRUE)
																ENDIF
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDFOR
							ENDIF
						ENDIF
					ELSE
						IF iTrackifyTargets[0] >= 0
							IF MC_serverBD_4.iObjPriority[iTrackifyTargets[0]][mc_playerBD[iPartToUse].iteam]  != MC_ServerBD_4.iCurrentHighestPriority[mc_playerBD[iPartToUse].iteam]
								PRINTLN("[AW_MISSION] - [RCC MISSION] [TRACKIFY] - REMOVE_TRACKIFY_MULTIPLE_TARGET ",0 ) 
								REMOVE_TRACKIFY_MULTIPLE_TARGET(0)
								iTrackifyTargets[0] = -2
							ENDIF
						ENDIF
						
						IF iTrackifyTargets[1] >= 0
							IF MC_serverBD_4.iObjPriority[iTrackifyTargets[1]][mc_playerBD[iPartToUse].iteam]  != MC_ServerBD_4.iCurrentHighestPriority[mc_playerBD[iPartToUse].iteam]
								PRINTLN("[AW_MISSION] - [RCC MISSION] [TRACKIFY] - REMOVE_TRACKIFY_MULTIPLE_TARGET ",1 ) 
								REMOVE_TRACKIFY_MULTIPLE_TARGET(1)
								iTrackifyTargets[1] = -2
							ENDIF
						ENDIF

						IF iTrackifyTargets[2] >= 0
							IF MC_serverBD_4.iObjPriority[iTrackifyTargets[2]][mc_playerBD[iPartToUse].iteam]  != MC_ServerBD_4.iCurrentHighestPriority[mc_playerBD[iPartToUse].iteam]
								PRINTLN("[AW_MISSION] - [RCC MISSION] [TRACKIFY] - REMOVE_TRACKIFY_MULTIPLE_TARGET ",2 ) 
								REMOVE_TRACKIFY_MULTIPLE_TARGET(2)
								iTrackifyTargets[2] = -2
							ENDIF
						ENDIF
						
						IF iTrackifyTargets[3] >= 0
							IF MC_serverBD_4.iObjPriority[iTrackifyTargets[3]][mc_playerBD[iPartToUse].iteam]  != MC_ServerBD_4.iCurrentHighestPriority[mc_playerBD[iPartToUse].iteam]
								PRINTLN("[AW_MISSION] - [RCC MISSION] [TRACKIFY] - REMOVE_TRACKIFY_MULTIPLE_TARGET ",3 )
								REMOVE_TRACKIFY_MULTIPLE_TARGET(3)
								iTrackifyTargets[3] = -2
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
		
			IF IS_CELLPHONE_TRACKIFY_IN_USE()
				DISABLE_CINEMATIC_VEHICLE_IDLE_MODE_THIS_UPDATE()
				INVALIDATE_CINEMATIC_VEHICLE_IDLE_MODE()
				
				//Dawn Raid
				IF bTrackObjects
					vTrackifyPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
					FOR iTargetLoop = 0 TO MAX_TRACKIFY_TARGETS - 1
						IF iTrackifyTargets[iTargetLoop] >= 0
							IF (vTrackifyPlayerCoords.z - vTrackifyVector[iTargetLoop].z)  <= - 2
								SET_TRACKIFY_MULTIPLE_TARGET_ARROW_TYPE(iTargetLoop, ARROW_UP)
								PRINTLN("[AW_MISSION] - [RCC MISSION] [TRACKIFY] -iTargetLoop = ", iTargetLoop, " HEIGHT vTrackifyPlayerCoords.z =  ",vTrackifyPlayerCoords.z, "vTrackifyVector[iTargetLoop].z = ", vTrackifyVector[iTargetLoop].z, "USING ARROW_UP")
							ELIF (vTrackifyPlayerCoords.z - vTrackifyVector[iTargetLoop].z) >= 2
								SET_TRACKIFY_MULTIPLE_TARGET_ARROW_TYPE(iTargetLoop, ARROW_DOWN)
								PRINTLN("[AW_MISSION] - [RCC MISSION] [TRACKIFY] -iTargetLoop = ", iTargetLoop, " HEIGHT vTrackifyPlayerCoords.z =  ",vTrackifyPlayerCoords.z, "vTrackifyVector[iTargetLoop].z = ", vTrackifyVector[iTargetLoop].z, "USING ARROW_DOWN")
							ELSE
								SET_TRACKIFY_MULTIPLE_TARGET_ARROW_TYPE(iTargetLoop, ARROW_OFF)
								PRINTLN("[AW_MISSION] - [RCC MISSION] [TRACKIFY] -iTargetLoop = ", iTargetLoop, " HEIGHT vTrackifyPlayerCoords.z =  ",vTrackifyPlayerCoords.z, "vTrackifyVector[iTargetLoop].z = ", vTrackifyVector[iTargetLoop].z, "USING ARROW_OFF")
							ENDIF
						ENDIF
					ENDFOR
				ENDIF

				
				IF MANAGE_THIS_MINIGAME_TIMER(iTrackifyUpdateTimer,100)
					FOR iTargetLoop = 0 TO MAX_TRACKIFY_TARGETS - 1
						IF iTrackifyTargets[iTargetLoop] >= 0
						AND NOT IS_BIT_SET(iTrackifyTargetProcessed, iTargetLoop)
							//Dawn Raid
							IF bTrackObjects
								IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iTrackifyTargets[iTargetLoop]])
									IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iTrackifyTargets[iTargetLoop]].iObjectBitSet, cibsOBJ_EnableTrackify)
										objTrackifyTarget[iTargetLoop] = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iTrackifyTargets[iTargetLoop]])
										vTrackifyVector[iTargetLoop] = GET_ENTITY_COORDS(objTrackifyTarget[iTargetLoop])
										IF iTrackifyTargets[iTargetLoop] >= 0
											SET_TRACKIFY_MULTIPLE_TARGET_VECTOR (iTargetLoop, vTrackifyVector[iTargetLoop])
										ENDIF
										PRINTLN("[AW_MISSION] - [RCC MISSION] - Object : ",iTrackifyTargets[iTargetLoop], " is the trackify target. At this vector: ", vTrackifyVector[iTargetLoop] )
										iTrackifyUpdateTimer = GET_GAME_TIMER()
									ENDIF
								ENDIF
							ELSE
								//Vehicles
								IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iTrackifyTargets[iTargetLoop]])
									IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iTrackifyTargets[iTargetLoop]].iVehBitsetTwo, ciFMMC_VEHICLE2_TRACKIFY_TARGET)
										IF MC_serverBD_4.iVehPriority[iTrackifyTargets[iTargetLoop] ][mc_playerBD[iPartToUse].iteam]  = MC_ServerBD_4.iCurrentHighestPriority[mc_playerBD[iPartToUse].iteam]
											vehTrackifyTarget[iTargetLoop] = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iTrackifyTargets[iTargetLoop]])
											IF IS_VEHICLE_DRIVEABLE(vehTrackifyTarget[iTargetLoop])
												vTrackifyVector[iTargetLoop] = GET_ENTITY_COORDS(vehTrackifyTarget[iTargetLoop])
												IF iTrackifyTargets[iTargetLoop] >= 0
													SET_TRACKIFY_MULTIPLE_TARGET_VECTOR (iTargetLoop, vTrackifyVector[iTargetLoop])
												ENDIF
												PRINTLN("[AW_MISSION] - [RCC MISSION] - Vehicle : ",iTrackifyTargets[iTargetLoop], " is the trackify target. At this vector: ", vTrackifyVector[iTargetLoop] )
												
												iTrackifyUpdateTimer = GET_GAME_TIMER()
											ELSE
												PRINTLN("[AW_MISSION] - [RCC MISSION] - Vehicle is not drivable " )  
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
			
			//Debug Stuff
			#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD0)
				PRINTLN("[AW_MISSION] - [RCC MISSION] [TRACKIFY] - REMOVE_TRACKIFY_MULTIPLE_TARGET ",0 ) 
				REMOVE_TRACKIFY_MULTIPLE_TARGET(0)
				iTrackifyTargets[0] = -2
			ENDIF
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD1)
				PRINTLN("[AW_MISSION] - [RCC MISSION] [TRACKIFY] - REMOVE_TRACKIFY_MULTIPLE_TARGET ",1 ) 
				REMOVE_TRACKIFY_MULTIPLE_TARGET(1)
				iTrackifyTargets[1] = -2
			ENDIF
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD2)
				PRINTLN("[AW_MISSION] - [RCC MISSION] [TRACKIFY] - REMOVE_TRACKIFY_MULTIPLE_TARGET ",2 ) 
				REMOVE_TRACKIFY_MULTIPLE_TARGET(2)
				iTrackifyTargets[2] = -2
			ENDIF
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD3)
				PRINTLN("[AW_MISSION] - [RCC MISSION] [TRACKIFY] - REMOVE_TRACKIFY_MULTIPLE_TARGET ",3 ) 
				REMOVE_TRACKIFY_MULTIPLE_TARGET(3)
				iTrackifyTargets[3] = -2
			ENDIF
			#ENDIF
		
		BREAK
		
		CASE 99
			IF IS_CELLPHONE_TRACKIFY_IN_USE()
				IF GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) != PPS_SKYDIVING 
				AND GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) != PPS_DEPLOYING
				AND GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) != PPS_PARACHUTING	
				AND GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) != PPS_LANDING
				AND NOT IS_PED_FALLING(PLAYER_PED_ID())
					IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(g_FMMC_STRUCT.iAdversaryModeType)
						iTrackifyProgress = 0
					ENDIF
				ELSE
					IF IS_PHONE_ONSCREEN()
						HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
					ENDIF
				ENDIF
			ELSE
				iTrackifyProgress = 0
			ENDIF
		BREAK
		
	ENDSWITCH
	
	//Toggle Ability to turn Nightvision on and off
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(g_FMMC_STRUCT.iAdversaryModeType)
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DR_CNIC_H1")
			OR bDisableNightVision
				//PRINTLN("[AW_MISSION][TRACKIFY]  Can't turn on night vision")
				SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_BLOCK_TOGGLING_NIGHTVISION_FILTER)
			ENDIF

			IF NOT bDisableNightVision
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DR_CNIC_H1")
					IF NOT IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
						IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
							//PRINTLN("[AW_MISSION][TRACKIFY]  Can turn on night vision")
							CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_BLOCK_TOGGLING_NIGHTVISION_FILTER)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
	
ENDPROC


PROC PROCESS_ENTER_SCRIPTED_TURRET_MINIGAME_MC(OBJECT_INDEX tempObj, INT iobj)
	
	UNUSED_PARAMETER(tempObj)
	
	IF MC_playerBD[iLocalPart].iObjHacked = -1
	AND bLocalPlayerPedOk
		IF g_iInteriorTurretSeat > -1
			IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_PLAYER_IS_USING_SCRIPTED_GUN_CAM)
				SET_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_PLAYER_IS_USING_SCRIPTED_GUN_CAM)
				PRINTLN("PROCESS_ENTER_GUNCAM_MINIGAME_MC - Setting bit to say that this player is in the scripted gun cam")
			ENDIF
		ELSE
			IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_PLAYER_IS_USING_SCRIPTED_GUN_CAM)
				CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_PLAYER_IS_USING_SCRIPTED_GUN_CAM)
				PRINTLN("PROCESS_ENTER_GUNCAM_MINIGAME_MC - Clearing the bit because this player is not in the scripted gun cam")
			ENDIF
		ENDIF
		
		INT iNumberOfPlayers = (MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]) 
		INT iPart
		INT iNumberOfPlayersInGunCam = 0
		INT iNumberOfPlayersChecked = 0
		
		IF iNumberOfPlayers < 1
			PRINTLN("PROCESS_ENTER_GUNCAM_MINIGAME_MC - Exiting function for now because we don't know how many players are playing")
			EXIT
		ENDIF
		
		FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
			IF ((NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)) OR IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
			AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_PLAYER_FAIL)
			AND IS_BIT_SET(MC_playerBD[iPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
			AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_FINISHED)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
					iNumberOfPlayersChecked++
					IF IS_BIT_SET(MC_playerBD[iPart].iClientBitSet4, PBBOOL4_PLAYER_IS_USING_SCRIPTED_GUN_CAM)
						iNumberOfPlayersInGunCam++
						PRINTLN("PROCESS_ENTER_GUNCAM_MINIGAME_MC - Participant ", iPart, " says they're in the gun turret cam")
						PRINTLN("PROCESS_ENTER_GUNCAM_MINIGAME_MC - ", iNumberOfPlayersInGunCam, " out of ", iNumberOfPlayers, " participants are ready to go")
					ELSE
						PRINTLN("PROCESS_ENTER_GUNCAM_MINIGAME_MC - Participant ", iPart, " says they're NOT in the gun turret cam")
					ENDIF
				ENDIF
				
				IF iNumberOfPlayersChecked >= iNumberOfPlayers
					BREAKLOOP
				ENDIF
			ENDIF
		ENDFOR
		
		IF iNumberOfPlayersInGunCam = iNumberOfPlayers
			//All players are in the gun turret - ready to proceed!
			MC_playerBD[iLocalPart].iObjHacked = iObj
			PRINTLN("PROCESS_ENTER_GUNCAM_MINIGAME_MC - Setting iObjHacked to this obj because all ", iNumberOfPlayersInGunCam, " players are in the turret")
		ENDIF
	ENDIF
ENDPROC

//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: INTERACT-WITH
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************

FUNC OBJECT_INDEX GET_INTERACT_WITH_SPAWNED_PROP(INT iSpawnedProp, INT iPart = -1)

	IF iPart = -1
		iPart = iLocalPart
	ENDIF
	
	IF iSpawnedProp = -1
		RETURN NULL
	ENDIF
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_playerBD_1[iPart].niInteractWith_CurrentSpawnedProps[iSpawnedProp])
		RETURN NET_TO_OBJ(MC_playerBD_1[iPart].niInteractWith_CurrentSpawnedProps[iSpawnedProp])
	ENDIF
	
	RETURN NULL
ENDFUNC

FUNC OBJECT_INDEX GET_INTERACT_WITH_PERSISTENT_PROP(INT iPersistentProp, INT iPart = -1)

	IF iPart = -1
		iPart = iLocalPart
	ENDIF
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_PlayerBD_1[iPart].niInteractWith_PersistentProps[iPersistentProp])
		RETURN NET_TO_OBJ(MC_PlayerBD_1[iPart].niInteractWith_PersistentProps[iPersistentProp])
	ENDIF
	
	RETURN NULL
ENDFUNC

#IF IS_DEBUG_BUILD
FUNC STRING GET_IW_STATE_NAME(INTERACT_WITH_STATE eState)
	SWITCH eState
		CASE IW_STATE_IDLE							RETURN "IDLE"
		CASE IW_STATE_INIT							RETURN "INIT"
		CASE IW_STATE_TURNING						RETURN "TURNING"
		CASE IW_STATE_STARTING_ANIMATION			RETURN "STARTING ANIMATION"
		CASE IW_STATE_ANIMATING						RETURN "ANIMATING"
		CASE IW_STATE_DOWNLOADING					RETURN "DOWNLOADING"
		CASE IW_STATE_CUT_PAINTING					RETURN "CUT PAINTING"
		CASE IW_STATE_PLANT_VAULT_DOOR_EXPLOSIVES	RETURN "PLANT_VAULT_DOOR_EXPLOSIVES"
		CASE IW_STATE_YACHT_CUTSCENE				RETURN "YACHT CUTSCENE"
		CASE IW_STATE_WALKING						RETURN "WALKING"
		CASE IW_STATE_EXIT_ANIMATION				RETURN "EXIT ANIMATION"
		CASE IW_STATE_FINISHED						RETURN "FINISHED"
		CASE IW_STATE_HOLD_INTERACT					RETURN "HOLD INTERACT"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

PROC INTERACT_WITH_DEBUG(OBJECT_INDEX tempObj, INT iObj)
	
	TEXT_LABEL_63 tlDebugText
	UNUSED_PARAMETER(tempObj)
	UNUSED_PARAMETER(iObj)
	
	IF NOT bInteractWithDebug
		EXIT
	ENDIF
	
	IF DOES_ENTITY_EXIST(sIWInfo.eiInteractWith_CachedSceneAlignmentEntities[iObj])
		DRAW_DEBUG_LINE(GET_ENTITY_COORDS(sIWInfo.eiInteractWith_CachedSceneAlignmentEntities[iObj]), GET_ENTITY_COORDS(tempObj), 255, 255, 255)
		tempObj = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(sIWInfo.eiInteractWith_CachedSceneAlignmentEntities[iObj])
	ENDIF
	
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	
	IF sIWInfo.eInteractWith_CurrentState != IW_STATE_IDLE
		DRAW_DEBUG_TEXT_2D("INTERACT WITH", <<0.1, 0.3, 0.5>>)
		
		tlDebugText = "State: "
		tlDebugText += GET_IW_STATE_NAME(sIWInfo.eInteractWith_CurrentState)
		DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.1, 0.335, 0.5>>)
		
		tlDebugText = "iObjHacking: "
		tlDebugText += MC_playerBD[iLocalPart].iObjHacking
		DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.1, 0.35, 0.5>>)
	ENDIF
	
	TEXT_LABEL_63 tlEntityDebug = "Interact With (Obj "
	tlEntityDebug += iObj
	tlEntityDebug += ")"
	DRAW_DEBUG_TEXT_ABOVE_ENTITY(tempObj, tlEntityDebug, 0.1, 255, 255, 255)
	
	INT iSpawnedProp
	FOR iSpawnedProp = 0 TO MC_PlayerBD_1[iLocalPart].iInteractWith_NumSpawnedProps - 1
		ENTITY_INDEX eiSP = GET_INTERACT_WITH_SPAWNED_PROP(iSpawnedProp)
		
		IF DOES_ENTITY_EXIST(eiSP)
			tlEntityDebug = "SP"
			tlEntityDebug += iSpawnedProp
			DRAW_DEBUG_TEXT_ABOVE_ENTITY(eiSP, tlEntityDebug, 0.1, 200, 0, 0, 155)
		ENDIF
	ENDFOR
	
	INT iPersistentProp
	FOR iPersistentProp = 0 TO MC_PlayerBD_1[iLocalPart].iInteractWith_NumPersistentProps - 1
		ENTITY_INDEX eiSP = GET_INTERACT_WITH_PERSISTENT_PROP(iPersistentProp)
		
		IF DOES_ENTITY_EXIST(eiSP)
			tlEntityDebug = "PP"
			tlEntityDebug += iPersistentProp
			DRAW_DEBUG_TEXT_ABOVE_ENTITY(eiSP, tlEntityDebug, 0.1, 0, 0, 255, 155)
		ENDIF
	ENDFOR
	
	IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
		DRAW_DEBUG_LINE(GET_ENTITY_COORDS(tempObj), sIWInfo.vInteractWith_TargetPos, 255, 0, 0)
		DRAW_DEBUG_SPHERE(sIWInfo.vInteractWith_TargetPos, 1.0, 255, 0, 0, 100)
		
		DRAW_DEBUG_LINE(GET_ENTITY_COORDS(LocalPlayerPed), sIWInfo.vInteractWith_TargetPos, 255, 0, 0)
		
		IF IS_SYNC_SCENE_RUNNING(MC_playerBD[iLocalPart].iSynchSceneID)
			FLOAT fPhase = GET_SYNC_SCENE_PHASE(MC_playerBD[iLocalPart].iSynchSceneID)
			
			tlDebugText = "fPhase: "
			tlDebugText += FLOAT_TO_STRING(fPhase)
			DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.1, 0.4, 0.5>>)
		ENDIF
	ENDIF
	
//	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__BOMB_PLANT
//		g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_CASINO_TUNNEL_EXPLOSIVE
//	ENDIF

ENDPROC
#ENDIF

PROC SET_INTERACT_WITH_STATE(INTERACT_WITH_STATE newState)
	IF sIWInfo.eInteractWith_CurrentState != newState
		
		sIWInfo.eInteractWith_CurrentState = newState
		PRINTLN("[InteractWith] SET_INTERACT_WITH_STATE | sIWInfo.eInteractWith_CurrentState is now ", GET_IW_STATE_NAME(sIWInfo.eInteractWith_CurrentState), " and we are interacting with Obj ", MC_playerBD[iLocalPart].iObjHacking)
		DEBUG_PRINTCALLSTACK()
		
		#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
			ASSERTLN("[InteractWith] SET_INTERACT_WITH_STATE | sIWInfo.eInteractWith_CurrentState is now ", GET_IW_STATE_NAME(sIWInfo.eInteractWith_CurrentState), " and we are interacting with Obj ", MC_playerBD[iLocalPart].iObjHacking)
		ENDIF
		#ENDIF
	ENDIF
ENDPROC

PROC DELETE_INTERACT_WITH_SPAWNED_PROP(INT iSpawnedProp)
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_playerBD_1[iLocalPart].niInteractWith_CurrentSpawnedProps[iSpawnedProp])
		DELETE_NET_ID(MC_playerBD_1[iLocalPart].niInteractWith_CurrentSpawnedProps[iSpawnedProp])
		MC_RESERVE_NETWORK_MISSION_OBJECTS(-1)
		PRINTLN("[InteractWith] Deleting the spawned Interact With prop at index ", iSpawnedProp)
		MC_PlayerBD_1[iLocalPart].iInteractWith_NumSpawnedProps--
	ENDIF
ENDPROC

PROC SET_UP_INTERACT_WITH_PERSISTENT_PROP(OBJECT_INDEX eiObj, ENTITY_INDEX eiTemp, INT iObj, INT iSpawnedProp)

	UNUSED_PARAMETER(iSpawnedProp)

	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_RADIO_OVERRIDE
		IF IS_ENTITY_ATTACHED(eiObj)
			DETACH_ENTITY(eiObj, FALSE, TRUE)
			
			VECTOR vOffset = <<-0.025, 0.530, 1.0160>>
			VECTOR vRotation = <<-20.0, -90.0, -90.0>>
			
			ATTACH_ENTITY_TO_ENTITY(eiObj, eiTemp, 0, vOffset, vRotation)
			FREEZE_ENTITY_POSITION(eiObj, TRUE)
			PRINTLN("[InteractWith] Attaching prop to elec box")
		ENDIF
		
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__LEFT
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__RIGHT
		FREEZE_ENTITY_POSITION(eiObj, TRUE)
		
	ENDIF
ENDPROC

PROC TRANSFER_SPAWNED_INTERACT_WITH_PROP_TO_PERSISTENT_PROP(OBJECT_INDEX eiTemp, INT iObj, INT iSpawnedProp)

	IF iObj = -1
		EXIT
	ENDIF
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_playerBD_1[iLocalPart].niInteractWith_CurrentSpawnedProps[iSpawnedProp])
		EXIT
	ENDIF
	
	IF MC_PlayerBD_1[iLocalPart].iInteractWith_NumPersistentProps = ciInteractWith__Max_Persistent_Props
		PRINTLN("[InteractWith] TRANSFER_SPAWNED_INTERACT_WITH_PROP_TO_PERSISTENT_PROP - Persistent props list is full!")
		EXIT
	ENDIF
	
	IF DOES_INTERACT_WITH_ANIM_PRESET_HAVE_PERSISTENT_PROP(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim)
		PRINTLN("[InteractWith] Transferring niInteractWith_CurrentSpawnedProps[", iSpawnedProp, "] into sIWInfo.niInteractWith_PersistentProp[", MC_PlayerBD_1[iLocalPart].iInteractWith_NumPersistentProps, "]")
		
		MC_PlayerBD_1[iLocalPart].niInteractWith_PersistentProps[MC_PlayerBD_1[iLocalPart].iInteractWith_NumPersistentProps] = MC_playerBD_1[iLocalPart].niInteractWith_CurrentSpawnedProps[iSpawnedProp]
		SET_NETWORK_ID_CAN_MIGRATE(MC_PlayerBD_1[iLocalPart].niInteractWith_PersistentProps[MC_PlayerBD_1[iLocalPart].iInteractWith_NumPersistentProps], TRUE)
		MC_playerBD_1[iLocalPart].niInteractWith_CurrentSpawnedProps[iSpawnedProp] = NULL
		MC_PlayerBD_1[iLocalPart].iInteractWith_NumSpawnedProps--
		
		OBJECT_INDEX oiObj = GET_INTERACT_WITH_PERSISTENT_PROP(MC_PlayerBD_1[iLocalPart].iInteractWith_NumPersistentProps)
		SET_UP_INTERACT_WITH_PERSISTENT_PROP(oiObj, eiTemp, iObj, iSpawnedProp)
		
		MC_PlayerBD_1[iLocalPart].iInteractWith_NumPersistentProps++
		PRINTLN("[InteractWith] MC_PlayerBD_1[iLocalPart].iInteractWith_NumPersistentProps is now ", MC_PlayerBD_1[iLocalPart].iInteractWith_NumPersistentProps, " / MC_PlayerBD_1[iLocalPart].iInteractWith_NumSpawnedProps is now ", MC_PlayerBD_1[iLocalPart].iInteractWith_NumSpawnedProps)
	ENDIF
ENDPROC

PROC INTERACT_WITH_TRANSFER_ALL_SPAWNED_PROPS_TO_PERSISTENT_PROPS(OBJECT_INDEX tempObj, INT iObj)
	INT iSpawnedProp
	FOR iSpawnedProp = 0 TO ciInteractWith__Max_Spawned_Props - 1
		TRANSFER_SPAWNED_INTERACT_WITH_PROP_TO_PERSISTENT_PROP(tempObj, iObj, iSpawnedProp)
	ENDFOR
ENDPROC

FUNC BOOL DOES_PAINTING_OBJECT_HAVE_VALID_PAINTING_ID(INT iObj)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex = -1
		RETURN FALSE
	ELSE
		RETURN TRUE
	ENDIF
ENDFUNC

FUNC STRING GET_INTERACT_WITH_CUT_OUT_PAINTING_PLAYER_ANIM_NAME(INT iObj)
	IF DOES_PAINTING_OBJECT_HAVE_VALID_PAINTING_ID(iObj)
		IF iPaintingBeingCutAnimVariation = 0
			SWITCH MC_serverBD_1.iCurrentCutPaintingState[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex]
				CASE ciCutPaintingTopLeft
					PRINTLN("[ML]GET_INTERACT_WITH_CUT_OUT_PAINTING_PLAYER_ANIM_NAME - 1")
					iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex] = ciInteractWith_Painting_EnterTopLeft
					RETURN "VER_01_TOP_LEFT_ENTER"
				BREAK
				CASE ciCutPaintingTopRight
					PRINTLN("[ML]GET_INTERACT_WITH_CUT_OUT_PAINTING_PLAYER_ANIM_NAME - 2")
					iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex] = ciInteractWith_Painting_EnterTopRight
					RETURN "VER_01_TOP_RIGHT_ENTER"
				BREAK
				CASE ciCutPaintingBottomRight
					PRINTLN("[ML]GET_INTERACT_WITH_CUT_OUT_PAINTING_PLAYER_ANIM_NAME - 3")
					iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex] = ciInteractWith_Painting_EnterBottomRight
					RETURN "VER_01_BOTTOM_RIGHT_ENTER"
				BREAK
				CASE ciCutPaintingBottomLeft
					PRINTLN("[ML]GET_INTERACT_WITH_CUT_OUT_PAINTING_PLAYER_ANIM_NAME - 4")
					iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex] = ciInteractWith_Painting_EnterBottomLeft
					RETURN "VER_01_TOP_LEFT_RE-ENTER"
				BREAK
			ENDSWITCH
		ELIF iPaintingBeingCutAnimVariation = 1
			SWITCH MC_serverBD_1.iCurrentCutPaintingState[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex]
				CASE ciCutPaintingTopLeft
					PRINTLN("[ML]GET_INTERACT_WITH_CUT_OUT_PAINTING_PLAYER_ANIM_NAME - 5")
					iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex] = ciInteractWith_Painting_V2EnterTopLeft
					RETURN "VER_02_TOP_LEFT_ENTER"
				BREAK
				CASE ciCutPaintingTopRight
					PRINTLN("[ML]GET_INTERACT_WITH_CUT_OUT_PAINTING_PLAYER_ANIM_NAME - 6")
					iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex] = ciInteractWith_Painting_V2EnterTopRight
					RETURN "VER_02_TOP_RIGHT_ENTER"
				BREAK
				CASE ciCutPaintingBottomRight
					PRINTLN("[ML]GET_INTERACT_WITH_CUT_OUT_PAINTING_PLAYER_ANIM_NAME - 7")
					iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex] = ciInteractWith_Painting_V2EnterBottomRight
					RETURN "VER_02_BOTTOM_RIGHT_ENTER"
				BREAK
				CASE ciCutPaintingBottomLeft
					PRINTLN("[ML]GET_INTERACT_WITH_CUT_OUT_PAINTING_PLAYER_ANIM_NAME - 8")
					iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex] = ciInteractWith_Painting_V2EnterBottomLeft
					RETURN "VER_02_TOP_LEFT_RE-ENTER"
				BREAK
			ENDSWITCH
		ELSE
			PRINTLN("[GET_INTERACT_WITH_CUT_OUT_PAINTING_PLAYER_ANIM_NAME][ML] Illegal anim variation")
		ENDIF	
	ELSE
		PRINTLN("[ML][InteractWith] GET_INTERACT_WITH_CUT_OUT_PAINTING_PLAYER_ANIM_NAME | Obj: ", iObj, " Painting does not have a valid ID (so -1). Enter anim TOP_LEFT_ENTER (Just so there is some animation to continue the logic flow")
		RETURN "TOP_LEFT_ENTER"
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC STRING GET_INTERACT_WITH_PLANT_VAULT_DOOR_EXPLOSIVE_ANIM_NAME(INT iObj, INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE eState, BOOL bGetPlayerAnim = FALSE, BOOL bGetBombAnim = FALSE, INT iBombIndex = -1)
	
	INT iProgress = -1
	UNUSED_PARAMETER(iObj)
	
//	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__LEFT
//		iProgress = MC_serverBD_1.iCurrentVaultDoorPlantedExplosives_LeftSide
//	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__RIGHT
//		iProgress = MC_serverBD_1.iCurrentVaultDoorPlantedExplosives_RightSide
//	ENDIF

	iProgress = sIWInfo.iInteractWith_ObjectsPlacedLocalCounter
	
	SWITCH eState
		CASE INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE__INIT
			IF bGetPlayerAnim
				RETURN "PLAYER_IG8_VAULT_EXPLOSIVE_ENTER"
			ELIF bGetBombAnim
				SWITCH iBombIndex
					CASE 0		RETURN "SEMTEX_A_IG8_VAULT_EXPLOSIVE_ENTER"
					CASE 1		RETURN "SEMTEX_B_IG8_VAULT_EXPLOSIVE_ENTER"
					CASE 2		RETURN "SEMTEX_C_IG8_VAULT_EXPLOSIVE_ENTER"
				ENDSWITCH
			ENDIF
		BREAK
		CASE INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE__IDLE
			IF bGetPlayerAnim
				RETURN "PLAYER_IG8_VAULT_EXPLOSIVE_IDLE"
			ELIF bGetBombAnim
				SWITCH iBombIndex
					CASE 0		RETURN "SEMTEX_A_IG8_VAULT_EXPLOSIVE_IDLE"
					CASE 1		RETURN "SEMTEX_A_IG8_VAULT_EXPLOSIVE_IDLE"
					CASE 2		RETURN "SEMTEX_A_IG8_VAULT_EXPLOSIVE_IDLE"
				ENDSWITCH
			ENDIF
		BREAK
		CASE INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE__PLANTING
			IF bGetPlayerAnim
				SWITCH iProgress
					CASE 0		RETURN "PLAYER_IG8_VAULT_EXPLOSIVE_PLANT_A"
					CASE 1		RETURN "PLAYER_IG8_VAULT_EXPLOSIVE_PLANT_B"
					CASE 2		RETURN "PLAYER_IG8_VAULT_EXPLOSIVE_PLANT_C"
				ENDSWITCH
				
			ELIF bGetBombAnim
				SWITCH iProgress
					CASE 0
						SWITCH iBombIndex
							CASE 0		RETURN "SEMTEX_A_IG8_VAULT_EXPLOSIVE_PLANT_A"
							CASE 1		RETURN "SEMTEX_B_IG8_VAULT_EXPLOSIVE_PLANT_A"
							CASE 2		RETURN "SEMTEX_C_IG8_VAULT_EXPLOSIVE_PLANT_A"
						ENDSWITCH
					BREAK
					
					CASE 1
						SWITCH iBombIndex
							CASE 0		RETURN "SEMTEX_A_IG8_VAULT_EXPLOSIVE_PLANT_B"
							CASE 1		RETURN "SEMTEX_B_IG8_VAULT_EXPLOSIVE_PLANT_B"
							CASE 2		RETURN "SEMTEX_C_IG8_VAULT_EXPLOSIVE_PLANT_B"
						ENDSWITCH
					BREAK
					
					CASE 2
						SWITCH iBombIndex
							CASE 0		RETURN "SEMTEX_A_IG8_VAULT_EXPLOSIVE_PLANT_C"
							CASE 1		RETURN "SEMTEX_B_IG8_VAULT_EXPLOSIVE_PLANT_C"
							CASE 2		RETURN "SEMTEX_C_IG8_VAULT_EXPLOSIVE_PLANT_C"
						ENDSWITCH
					BREAK
				ENDSWITCH
			ENDIF
			
		BREAK
		CASE INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE__EXITING
			IF bGetPlayerAnim
				RETURN "PLAYER_IG8_VAULT_EXPLOSIVE_ENTER"
				
			ELIF bGetBombAnim
				RETURN "SEMTEX_A_IG8_VAULT_EXPLOSIVE_IDLE"
				
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN "INVALID"
ENDFUNC

FUNC STRING GET_INTERACT_WITH_PLAYER_FAILSAFE_ANIM_DICT(INT iObj)
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim
		CASE ciINTERACT_WITH_PRESET__PUSH_BUTTON				RETURN "anim@mp_radio@high_life_apment"
		CASE ciINTERACT_WITH_PRESET__RUMMAGE_AROUND			RETURN "ANIM@HEISTS@PRISON_HEISTSTATION@"
		CASE ciINTERACT_WITH_PRESET__BOMB_PLANT				RETURN  "ANIM@MP_FIREWORKS"
		CASE ciINTERACT_WITH_PRESET__SEARCH_BODY_ON_FLOOR		RETURN  "ANIM@MP_FIREWORKS"
	ENDSWITCH
	
	RETURN "ANIM@HEISTS@PRISON_HEISTSTATION@"
ENDFUNC

FUNC STRING GET_INTERACT_WITH_PLAYER_FAILSAFE_ANIM_NAME(INT iObj)
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim
		CASE ciINTERACT_WITH_PRESET__PUSH_BUTTON				RETURN "action_a_bedroom"
		CASE ciINTERACT_WITH_PRESET__RUMMAGE_AROUND			RETURN "pickup_bus_schedule"
		CASE ciINTERACT_WITH_PRESET__BOMB_PLANT				RETURN "PLACE_FIREWORK_3_BOX"
		CASE ciINTERACT_WITH_PRESET__SEARCH_BODY_ON_FLOOR		RETURN "PLACE_FIREWORK_3_BOX"
	ENDSWITCH
	
	RETURN "pickup_bus_schedule"
ENDFUNC

FUNC STRING GET_INTERACT_WITH_CHANGE_CLOTHES_ANIMATION(INT iObj)
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iClothesChangeOutfit
		CASE ciChangeClothesNOOSE
			IF IS_PED_FEMALE(LocalPlayerPed)
				RETURN "CHANGE_NOOSE_FEMALE"
			ELSE
				RETURN "CHANGE_NOOSE_MALE"
			ENDIF
		BREAK
		CASE ciChangeClothesFireman
			IF IS_PED_FEMALE(LocalPlayerPed)
				RETURN "CHANGE_FIRE_FEMALE"
			ELSE
				RETURN "CHANGE_FIRE_MALE"
			ENDIF
		BREAK
		CASE ciChangeClothesHighroller
			IF IS_PED_FEMALE(LocalPlayerPed)
				RETURN "CHANGE_HIGHROLLER_FEMALE"
			ELSE
				RETURN "CHANGE_HIGHROLLER_MALE"
			ENDIF
		BREAK
	ENDSWITCH
	
	IF IS_PED_FEMALE(LocalPlayerPed)
		RETURN "CHANGE_HIGHROLLER_FEMALE"
	ELSE
		RETURN "CHANGE_HIGHROLLER_MALE"
	ENDIF
ENDFUNC

FUNC STRING GET_INTERACT_WITH_AUDIO_BANK(INT iObj)
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim
		CASE ciINTERACT_WITH_PRESET__USE_KEYCARD					RETURN "DLC_MPHEIST/HEIST_USE_KEYPAD"
		CASE ciINTERACT_WITH_PRESET__CUT_OUT_PAINTING				RETURN "DLC_HEIST3/HEIST_FINALE_STEAL_PAINTINGS"
	ENDSWITCH
	
	RETURN ""
ENDFUNC


FUNC STRING GET_INTERACT_WITH_PLAYER_ANIM_DICT(INT iObj)
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim
		CASE ciINTERACT_WITH_PRESET__PUSH_BUTTON						RETURN "anim@mp_radio@high_life_apment"
		CASE ciINTERACT_WITH_PRESET__RUMMAGE_AROUND					RETURN "ANIM@HEISTS@PRISON_HEISTSTATION@"
		CASE ciINTERACT_WITH_PRESET__BOMB_PLANT						RETURN "ANIM@MP_FIREWORKS"
		CASE ciINTERACT_WITH_PRESET__SEARCH_BODY_ON_TABLE				RETURN "anim@GangOps@Morgue@Table@"
		CASE ciINTERACT_WITH_PRESET__SEARCH_BODY_ON_FLOOR				RETURN "anim@GangOps@Facility@Servers@BodySearch@"
		CASE ciINTERACT_WITH_PRESET__POWER_BOX_CRANK							RETURN "anim@GangOps@Hanger@FUSE_BOX@"
		CASE ciINTERACT_WITH_PRESET__HACK_LAPTOP_USB							RETURN "anim@GangOps@Morgue@Office@Laptop@"
		CASE ciINTERACT_WITH_PRESET__CRATE_IN_VAN						RETURN "anim@GangOps@VAN@DRIVE_GRAB@"
		CASE ciINTERACT_WITH_PRESET__PLANT_RADIO_OVERRIDE				RETURN "weapons@projectile@sticky_bomb"
		CASE ciINTERACT_WITH_PRESET__USE_KEYCARD					RETURN "ANIM_HEIST@HS3F@IG3_CARDSWIPE@male@"
		CASE ciINTERACT_WITH_PRESET__CLOTHES							RETURN "ANIM_HEIST@HS3F@IG12_CHANGE_CLOTHES@"
		CASE ciINTERACT_WITH_PRESET__HOLD_BUTTON						RETURN "ANIM_HEIST@HS3F@IG1_daily_storage@male@"
		CASE ciINTERACT_WITH_PRESET__CUT_OUT_PAINTING							RETURN "ANIM_HEIST@HS3F@IG11_STEAL_PAINTING@MALE@"
		CASE ciINTERACT_WITH_PRESET__PUSH_DAILY_CASH_BUTTON			RETURN "ANIM_HEIST@HS3F@IG6_PUSH_BUTTON@"
		CASE ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__LEFT	RETURN "ANIM_HEIST@HS3F@IG8_VAULT_EXPLOSIVES@LEFT@MALE@"
		CASE ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__RIGHT	RETURN "ANIM_HEIST@HS3F@IG8_VAULT_EXPLOSIVES@RIGHT@MALE@"
		CASE ciINTERACT_WITH_PRESET__PLANT_CASINO_TUNNEL_EXPLOSIVE	RETURN "ANIM_HEIST@HS3F@IG7_PLANT_BOMB@MALE@"
		CASE ciINTERACT_WITH_PRESET__SEARCH_BROKEN_LAPTOP			RETURN "ANIM@SCRIPTED@ULP_MISSIONS@BROKEN_LAPTOP@MALE@"
		CASE ciINTERACT_WITH_PRESET__SEARCH_PAPERWORK				RETURN "ANIM@SCRIPTED@ULP_MISSIONS@PAPERWORK@MALE@"
	ENDSWITCH

	RETURN ""
ENDFUNC

FUNC STRING GET_INTERACT_WITH_PLAYER_ANIM_NAME(INT iObj)
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim
		CASE ciINTERACT_WITH_PRESET__PUSH_BUTTON						RETURN "action_a_bedroom"
		CASE ciINTERACT_WITH_PRESET__RUMMAGE_AROUND					RETURN "pickup_bus_schedule"
		CASE ciINTERACT_WITH_PRESET__BOMB_PLANT						RETURN "PLACE_FIREWORK_3_BOX"
		CASE ciINTERACT_WITH_PRESET__SEARCH_BODY_ON_TABLE				RETURN "Player_Search"
		CASE ciINTERACT_WITH_PRESET__SEARCH_BODY_ON_FLOOR				RETURN "Player_Search"
		CASE ciINTERACT_WITH_PRESET__POWER_BOX_CRANK							RETURN "SWITCH_ON"
		CASE ciINTERACT_WITH_PRESET__HACK_LAPTOP_USB							RETURN "ENTER"
		CASE ciINTERACT_WITH_PRESET__CRATE_IN_VAN						RETURN "grab_drive"
		CASE ciINTERACT_WITH_PRESET__PLANT_RADIO_OVERRIDE				RETURN "plant_vertical"
		CASE ciINTERACT_WITH_PRESET__USE_KEYCARD					RETURN "SUCCESS_VAR01"
		CASE ciINTERACT_WITH_PRESET__CLOTHES							RETURN GET_INTERACT_WITH_CHANGE_CLOTHES_ANIMATION(iObj)
		CASE ciINTERACT_WITH_PRESET__HOLD_BUTTON						RETURN "ENTER"
		CASE ciINTERACT_WITH_PRESET__CUT_OUT_PAINTING							RETURN GET_INTERACT_WITH_CUT_OUT_PAINTING_PLAYER_ANIM_NAME(iObj)
		CASE ciINTERACT_WITH_PRESET__PUSH_DAILY_CASH_BUTTON			RETURN "push_button"
		CASE ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__LEFT		RETURN GET_INTERACT_WITH_PLANT_VAULT_DOOR_EXPLOSIVE_ANIM_NAME(iObj, MC_playerBD_1[iLocalPart].eiInteractWith_PlantExplosivesState, TRUE)
		CASE ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__RIGHT		RETURN GET_INTERACT_WITH_PLANT_VAULT_DOOR_EXPLOSIVE_ANIM_NAME(iObj, MC_playerBD_1[iLocalPart].eiInteractWith_PlantExplosivesState, TRUE)
		CASE ciINTERACT_WITH_PRESET__PLANT_CASINO_TUNNEL_EXPLOSIVE	RETURN "plant_bomb"
		CASE ciINTERACT_WITH_PRESET__SEARCH_BROKEN_LAPTOP			RETURN "ACTION"
		CASE ciINTERACT_WITH_PRESET__SEARCH_PAPERWORK				RETURN "ACTION"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_PLAYER_EXIT_ANIM_NAME_FOR_INTERACT_WITH(INT iObj)
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim
		CASE ciINTERACT_WITH_PRESET__HOLD_BUTTON			RETURN "EXIT"
		CASE ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__LEFT		RETURN "PLAYER_IG8_VAULT_EXPLOSIVE_IDLE"
		CASE ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__RIGHT		RETURN "PLAYER_IG8_VAULT_EXPLOSIVE_IDLE"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_PLAYER_LOOP_ANIM_NAME_FOR_INTERACT_WITH(INT iObj)
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim
		CASE ciINTERACT_WITH_PRESET__HOLD_BUTTON		RETURN "LOOP"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC BOOL DOES_INTERACTION_SYNC_SCENE_BLEND_SEAMLESSLY(INT iObj)
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim
		CASE ciINTERACT_WITH_PRESET__HOLD_BUTTON
		CASE ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__LEFT
		CASE ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__RIGHT
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_INTERACT_WITH_DELAY_CLEANUP_ON_COMPLETION(INT iObj)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_CASINO_TUNNEL_EXPLOSIVE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL SHOULD_INTERACT_WITH_HOLD_LAST_FRAME(INT iobj)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_CASINO_TUNNEL_EXPLOSIVE
		RETURN TRUE
	ENDIF
	
	IF DOES_INTERACTION_SYNC_SCENE_BLEND_SEAMLESSLY(iObj)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC SYNCED_SCENE_PLAYBACK_FLAGS GET_INTERACT_WITH_SYNC_SCENE_FLAGS(INT iObj)
	UNUSED_PARAMETER(iObj)
	SYNCED_SCENE_PLAYBACK_FLAGS sResult = (SYNCED_SCENE_HIDE_WEAPON | SYNCED_SCENE_ABORT_ON_DEATH | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_NET_DISREGARD_ATTACHMENT_CHECKS)
	RETURN sResult
ENDFUNC

FUNC FLOAT GET_INTERACT_WITH_START_PHASE(INT iObj)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__USE_KEYCARD
		RETURN 0.15
	ENDIF

	RETURN 0.0
ENDFUNC

FUNC FLOAT GET_INTERACT_WITH_SKIPPED_TRANSITION_START_PHASE(INT iObj)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__LEFT
		SWITCH sIWInfo.iInteractWith_ObjectsPlacedLocalCounter
			CASE 0		RETURN 0.15
			CASE 1		RETURN 0.125
		ENDSWITCH
		
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__RIGHT
		SWITCH sIWInfo.iInteractWith_ObjectsPlacedLocalCounter
			CASE 0		RETURN 0.15
			CASE 1		RETURN 0.1
		ENDSWITCH
	ENDIF

	RETURN 0.0
ENDFUNC

// ONLY ADD TO THIS IF YOUR ANIMATION NEEDS TO ALIGN TO AN OBJECT OTHER THAN THE MINIGAME OBJCET ITSELF
FUNC MODEL_NAMES GET_MODEL_FOR_INTERACT_WITH_SCENE_ALIGNMENT_ENTITY(INT iobj)

	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim
		CASE ciINTERACT_WITH_PRESET__SEARCH_BODY_ON_TABLE			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("v_med_cor_emblmtable"))
		CASE ciINTERACT_WITH_PRESET__POWER_BOX_CRANK				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_powerbox_01"))
		CASE ciINTERACT_WITH_PRESET__HACK_LAPTOP_USB				RETURN PROP_LAPTOP_01A
		CASE ciINTERACT_WITH_PRESET__CRATE_IN_VAN					RETURN SPEEDO
		CASE ciINTERACT_WITH_PRESET__SEARCH_BODY_ON_FLOOR			RETURN DUMMY_MODEL_FOR_SCRIPT
		CASE ciINTERACT_WITH_PRESET__PLANT_RADIO_OVERRIDE			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_elecbox_01a")) // Radio box
		CASE ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__LEFT	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_vaultdoor01x")) // Casino Vault Door
		CASE ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__RIGHT	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_vaultdoor01x")) // Casino Vault Door
		CASE ciINTERACT_WITH_PRESET__CUT_OUT_PAINTING				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_CH_Sec_Cabinet_02a")) 	 // Casino Vault Painting Security Cabinet
		CASE ciINTERACT_WITH_PRESET__PLANT_CASINO_TUNNEL_EXPLOSIVE	RETURN DUMMY_MODEL_FOR_SCRIPT 	 // Tunnel debris
		CASE ciINTERACT_WITH_PRESET__SEARCH_BROKEN_LAPTOP			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("reh_Prop_REH_laptop_01a"))
		CASE ciINTERACT_WITH_PRESET__SEARCH_PAPERWORK				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("REH_PROP_REH_FOLDER_01B"))
	ENDSWITCH
	
	RETURN g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn
ENDFUNC

FUNC MODEL_NAMES GET_KEYCARD_MODEL_FOR_CASINO_FINGERPRINT_SCANNER(INT iObj)
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectKeypadAccessRequired
		CASE INTERACT_WITH_KEYPAD_CLEARANCE_REQUIRED__INSIDE_MAN_LVL1		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Swipe_Card_01a"))
		CASE INTERACT_WITH_KEYPAD_CLEARANCE_REQUIRED__INSIDE_MAN_LVL2		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Swipe_Card_01b"))
		CASE INTERACT_WITH_KEYPAD_CLEARANCE_REQUIRED__INSIDE_MAN_LVL3		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Swipe_Card_01c"))
		CASE INTERACT_WITH_KEYPAD_CLEARANCE_REQUIRED__INSIDE_MAN_LVL4		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Swipe_Card_01d"))
	ENDSWITCH
	
	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Swipe_Card_01a"))
ENDFUNC

FUNC MODEL_NAMES GET_INTERACT_WITH_PROP_MODEL_TO_SPAWN(INT iObj)

	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim
		CASE ciINTERACT_WITH_PRESET__SEARCH_BODY_ON_FLOOR			RETURN HEI_PROP_HST_USB_DRIVE
		CASE ciINTERACT_WITH_PRESET__HACK_LAPTOP_USB						RETURN HEI_PROP_HST_USB_DRIVE
		CASE ciINTERACT_WITH_PRESET__CRATE_IN_VAN					RETURN PROP_CS_SERVER_DRIVE
		CASE ciINTERACT_WITH_PRESET__USE_KEYCARD				RETURN GET_KEYCARD_MODEL_FOR_CASINO_FINGERPRINT_SCANNER(iObj)
		CASE ciINTERACT_WITH_PRESET__CUT_OUT_PAINTING						RETURN INT_TO_ENUM(MODEL_NAMES, HASH("W_ME_Switchblade"))
		CASE ciINTERACT_WITH_PRESET__PLANT_RADIO_OVERRIDE			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("vw_Prop_VW_EX_PE_01a"))
		CASE ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__LEFT	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Ch_Explosive_01a"))
		CASE ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__RIGHT	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Ch_Explosive_01a"))
		CASE ciINTERACT_WITH_PRESET__PLANT_CASINO_TUNNEL_EXPLOSIVE	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Ch_LD_Bomb_01a"))
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC INT GET_INTERACT_WITH_NUM_PROPS_TO_SPAWN(INT iObj)

	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim
		CASE ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__LEFT			RETURN 2
		CASE ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__RIGHT			RETURN 3
	ENDSWITCH
	
	IF GET_INTERACT_WITH_PROP_MODEL_TO_SPAWN(iObj) != DUMMY_MODEL_FOR_SCRIPT
		RETURN 1
	ELSE
		RETURN 0
	ENDIF
ENDFUNC

FUNC STRING GET_ANIM_NAME_FOR_INTERACT_WITH_SPAWNED_PROP(INT iObj, INT iSpawnedProp, INT iMode = 0)
	PRINTLN("[ML][GET_ANIM_NAME_FOR_INTERACT_WITH_SPAWNED_PROP] iMode = ", iMode)
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim
		CASE ciINTERACT_WITH_PRESET__SEARCH_BODY_ON_TABLE
			RETURN "Device_Search"
		
		CASE ciINTERACT_WITH_PRESET__SEARCH_BODY_ON_FLOOR
			RETURN "Device_Search"
		
		CASE ciINTERACT_WITH_PRESET__CRATE_IN_VAN
			RETURN "drive_grab_drive"
		
		CASE ciINTERACT_WITH_PRESET__HACK_LAPTOP_USB
			SWITCH iMode
				CASE 0 RETURN "ENTER_USB"
				CASE 1 RETURN "IDLE_USB"
				CASE 2 RETURN "EXIT_USB"
			ENDSWITCH
		BREAK
		CASE ciINTERACT_WITH_PRESET__PLANT_RADIO_OVERRIDE
			RETURN ""
			
		CASE ciINTERACT_WITH_PRESET__USE_KEYCARD
			RETURN "SUCCESS_VAR01_CARD"
			
		CASE ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__LEFT
		CASE ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__RIGHT
			SWITCH iSpawnedProp
				CASE 0		RETURN GET_INTERACT_WITH_PLANT_VAULT_DOOR_EXPLOSIVE_ANIM_NAME(iObj, MC_playerBD_1[iLocalPart].eiInteractWith_PlantExplosivesState, FALSE, TRUE, iSpawnedProp)
				CASE 1		RETURN GET_INTERACT_WITH_PLANT_VAULT_DOOR_EXPLOSIVE_ANIM_NAME(iObj, MC_playerBD_1[iLocalPart].eiInteractWith_PlantExplosivesState, FALSE, TRUE, iSpawnedProp)
				CASE 2		RETURN GET_INTERACT_WITH_PLANT_VAULT_DOOR_EXPLOSIVE_ANIM_NAME(iObj, MC_playerBD_1[iLocalPart].eiInteractWith_PlantExplosivesState, FALSE, TRUE, iSpawnedProp)
			ENDSWITCH
		BREAK
		CASE ciINTERACT_WITH_PRESET__CUT_OUT_PAINTING
			SWITCH iMode
				CASE 0		RETURN "ver_01_top_left_enter_w_me_switchblade"
				CASE 1		RETURN "ver_01_top_right_enter_w_me_switchblade"
				CASE 2		RETURN "ver_01_bottom_right_enter_w_me_switchblade"
				CASE 3		RETURN "ver_01_top_left_enter_w_me_switchblade"
				CASE 4		RETURN "ver_01_cutting_top_left_idle_w_me_switchblade"
				CASE 5		RETURN "ver_01_cutting_top_right_idle_w_me_switchblade"
				CASE 6		RETURN "ver_01_cutting_bottom_right_idle_w_me_switchblade"
				CASE 7		RETURN "ver_01_cutting_top_left_idle_w_me_switchblade"
				CASE 8		RETURN "ver_01_cutting_top_left_to_right_w_me_switchblade"
				CASE 9		RETURN "ver_01_cutting_right_top_to_bottom_w_me_switchblade"
				CASE 10		RETURN "ver_01_cutting_bottom_right_to_left_w_me_switchblade"
				CASE 11		RETURN "ver_01_cutting_left_top_to_bottom_w_me_switchblade"
				CASE 12		RETURN "ver_01_top_left_exit_w_me_switchblade"
				CASE 13		RETURN "ver_01_top_right_exit_w_me_switchblade"
				CASE 14		RETURN "ver_01_bottom_right_exit_w_me_switchblade"
				CASE 15		RETURN "ver_01_top_left_exit_w_me_switchblade"
				CASE 16		RETURN "ver_01_with_painting_exit_w_me_switchblade"
				
				CASE 17		RETURN "ver_02_top_left_enter_w_me_switchblade"
				CASE 18		RETURN "ver_02_top_right_enter_w_me_switchblade"
				CASE 19		RETURN "ver_02_bottom_right_enter_w_me_switchblade"
				CASE 20		RETURN "ver_02_top_left_enter_w_me_switchblade"
				CASE 21		RETURN "ver_02_cutting_top_left_idle_w_me_switchblade"
				CASE 22		RETURN "ver_02_cutting_top_right_idle_w_me_switchblade"
				CASE 23		RETURN "ver_02_cutting_bottom_right_idle_w_me_switchblade"
				CASE 24		RETURN "ver_02_cutting_top_left_idle_w_me_switchblade"
				CASE 25		RETURN "ver_02_cutting_top_left_to_right_w_me_switchblade"
				CASE 26		RETURN "ver_02_cutting_right_top_to_bottom_w_me_switchblade"
				CASE 27		RETURN "ver_02_cutting_bottom_right_to_left_w_me_switchblade"
				CASE 28		RETURN "ver_02_cutting_left_top_to_bottom_w_me_switchblade"
				CASE 29		RETURN "ver_02_top_left_exit_w_me_switchblade"
				CASE 30		RETURN "ver_02_top_right_exit_w_me_switchblade"
				CASE 31		RETURN "ver_02_bottom_right_exit_w_me_switchblade"
				CASE 32		RETURN "ver_02_top_left_exit_w_me_switchblade"
				CASE 33		RETURN "ver_02_with_painting_exit_w_me_switchblade"
			ENDSWITCH
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__PLANT_CASINO_TUNNEL_EXPLOSIVE
			RETURN "PLANT_BOMB_PROP"
			
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_ANIM_NAME_FOR_PAINTING_CUT_PAINTING_OBJECT(INT iObj, INT iMode = 0)		// [ML] Get the animation name for the painting object being cut
	PRINTLN("[ML][GET_ANIM_NAME_FOR_PAINTING_CUT_PAINTING_OBJECT] iMode = ", iMode)
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim
		CASE ciINTERACT_WITH_PRESET__CUT_OUT_PAINTING
			SWITCH iMode
				CASE 0		RETURN "ver_01_top_left_enter_ch_prop_vault_painting_01a"
				CASE 1		RETURN "ver_01_top_right_enter_ch_prop_vault_painting_01a"
				CASE 2		RETURN "ver_01_bottom_right_enter_ch_prop_vault_painting_01a"
				CASE 3		RETURN "ver_01_top_left_re-enter_ch_prop_vault_painting_01a"
				CASE 4		RETURN "ver_01_cutting_top_left_idle_ch_prop_vault_painting_01a"
				CASE 5		RETURN "ver_01_cutting_top_right_idle_ch_prop_vault_painting_01a"
				CASE 6		RETURN "ver_01_cutting_bottom_right_idle_ch_prop_vault_painting_01a"
				CASE 7		RETURN "ver_01_cutting_bottom_left_idle_ch_prop_vault_painting_01a"
				CASE 8		RETURN "VER_01_CUTTING_TOP_LEFT_TO_RIGHT_ch_prop_vault_painting_01a"
				CASE 9		RETURN "VER_01_CUTTING_RIGHT_TOP_TO_BOTTOM_ch_prop_vault_painting_01a"
				CASE 10		RETURN "VER_01_CUTTING_BOTTOM_RIGHT_TO_LEFT_ch_prop_vault_painting_01a"
				CASE 11		RETURN "VER_01_CUTTING_LEFT_TOP_TO_BOTTOM_ch_prop_vault_painting_01a"
				CASE 12		RETURN "ver_01_top_left_exit_ch_prop_vault_painting_01a"
				CASE 13		RETURN "ver_01_top_right_exit_ch_prop_vault_painting_01a"
				CASE 14		RETURN "ver_01_bottom_right_exit_ch_prop_vault_painting_01a"
				CASE 15		RETURN "ver_01_bottom_left_exit_ch_prop_vault_painting_01a"
				CASE 16		RETURN "VER_01_WITH_PAINTING_EXIT_ch_prop_vault_painting_01a"
				
				CASE 17		RETURN "ver_02_top_left_enter_ch_prop_vault_painting_01a"
				CASE 18		RETURN "ver_02_top_right_enter_ch_prop_vault_painting_01a"
				CASE 19		RETURN "ver_02_bottom_right_enter_ch_prop_vault_painting_01a"
				CASE 20		RETURN "ver_02_top_left_re-enter_ch_prop_vault_painting_01a"
				CASE 21		RETURN "ver_02_cutting_top_left_idle_ch_prop_vault_painting_01a"
				CASE 22		RETURN "ver_02_cutting_top_right_idle_ch_prop_vault_painting_01a"
				CASE 23		RETURN "ver_02_cutting_bottom_right_idle_ch_prop_vault_painting_01a"
				CASE 24		RETURN "ver_02_cutting_bottom_left_idle_ch_prop_vault_painting_01a"
				CASE 25		RETURN "VER_02_CUTTING_TOP_LEFT_TO_RIGHT_ch_prop_vault_painting_01a"
				CASE 26		RETURN "VER_02_CUTTING_RIGHT_TOP_TO_BOTTOM_ch_prop_vault_painting_01a"
				CASE 27		RETURN "VER_02_CUTTING_BOTTOM_RIGHT_TO_LEFT_ch_prop_vault_painting_01a"
				CASE 28		RETURN "VER_02_CUTTING_LEFT_TOP_TO_BOTTOM_ch_prop_vault_painting_01a"
				CASE 29		RETURN "ver_02_top_left_exit_ch_prop_vault_painting_01a"
				CASE 30		RETURN "ver_02_top_right_exit_ch_prop_vault_painting_01a"
				CASE 31		RETURN "ver_02_bottom_right_exit_ch_prop_vault_painting_01a"
				CASE 32		RETURN "ver_02_bottom_left_exit_ch_prop_vault_painting_01a"
				CASE 33		RETURN "VER_02_WITH_PAINTING_EXIT_ch_prop_vault_painting_01a"
			ENDSWITCH
		BREAK
			
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_ANIM_NAME_FOR_PAINTING_CUT_BAG_OBJECT(INT iObj, INT iMode = 0)		// [ML] Get the animation name for the bag object
	PRINTLN("[ML][GET_ANIM_NAME_FOR_PAINTING_CUT_BAG_OBJECT] iMode = ", iMode)
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim
		CASE ciINTERACT_WITH_PRESET__CUT_OUT_PAINTING
			SWITCH iMode
				CASE 0		RETURN "ver_01_top_left_enter_hei_p_m_bag_var22_arm_s"
				CASE 1		RETURN "ver_01_top_right_enter_hei_p_m_bag_var22_arm_s"
				CASE 2		RETURN "ver_01_bottom_right_enter_hei_p_m_bag_var22_arm_s"
				CASE 3		RETURN "ver_01_top_left_re-enter_hei_p_m_bag_var22_arm_s"
				CASE 4		RETURN "ver_01_cutting_top_left_idle_hei_p_m_bag_var22_arm_s"
				CASE 5		RETURN "ver_01_cutting_top_right_idle_hei_p_m_bag_var22_arm_s"
				CASE 6		RETURN "ver_01_cutting_bottom_right_idle_hei_p_m_bag_var22_arm_s"
				CASE 7		RETURN "ver_01_cutting_top_left_idle_hei_p_m_bag_var22_arm_s"
				CASE 8		RETURN "ver_01_cutting_top_left_to_right_hei_p_m_bag_var22_arm_s"
				CASE 9		RETURN "ver_01_cutting_right_top_to_bottom_hei_p_m_bag_var22_arm_s"
				CASE 10		RETURN "ver_01_cutting_bottom_right_to_left_hei_p_m_bag_var22_arm_s"
				CASE 11		RETURN "ver_01_cutting_left_top_to_bottom_hei_p_m_bag_var22_arm_s"
				CASE 12		RETURN "ver_01_top_left_exit_hei_p_m_bag_var22_arm_s"
				CASE 13		RETURN "ver_01_top_right_exit_hei_p_m_bag_var22_arm_s"
				CASE 14		RETURN "ver_01_bottom_right_exit_hei_p_m_bag_var22_arm_s"
				CASE 15		RETURN "ver_01_top_left_exit_hei_p_m_bag_var22_arm_s"
				CASE 16		RETURN "ver_01_with_painting_exit_hei_p_m_bag_var22_arm_s"
				
				CASE 17		RETURN "ver_02_top_left_enter_hei_p_m_bag_var22_arm_s"
				CASE 18		RETURN "ver_02_top_right_enter_hei_p_m_bag_var22_arm_s"
				CASE 19		RETURN "ver_02_bottom_right_enter_hei_p_m_bag_var22_arm_s"
				CASE 20		RETURN "ver_02_top_left_re-enter_hei_p_m_bag_var22_arm_s"
				CASE 21		RETURN "ver_02_cutting_top_left_idle_hei_p_m_bag_var22_arm_s"
				CASE 22		RETURN "ver_02_cutting_top_right_idle_hei_p_m_bag_var22_arm_s"
				CASE 23		RETURN "ver_02_cutting_bottom_right_idle_hei_p_m_bag_var22_arm_s"
				CASE 24		RETURN "ver_02_cutting_top_left_idle_hei_p_m_bag_var22_arm_s"
				CASE 25		RETURN "ver_02_cutting_top_left_to_right_hei_p_m_bag_var22_arm_s"
				CASE 26		RETURN "ver_02_cutting_right_top_to_bottom_hei_p_m_bag_var22_arm_s"
				CASE 27		RETURN "ver_02_cutting_bottom_right_to_left_hei_p_m_bag_var22_arm_s"
				CASE 28		RETURN "ver_02_cutting_left_top_to_bottom_hei_p_m_bag_var22_arm_s"
				CASE 29		RETURN "ver_02_top_left_exit_hei_p_m_bag_var22_arm_s"
				CASE 30		RETURN "ver_02_top_right_exit_hei_p_m_bag_var22_arm_s"
				CASE 31		RETURN "ver_02_bottom_right_exit_hei_p_m_bag_var22_arm_s"
				CASE 32		RETURN "ver_02_top_left_exit_hei_p_m_bag_var22_arm_s"
				CASE 33		RETURN "ver_02_with_painting_exit_hei_p_m_bag_var22_arm_s"
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC INT GET_CAM_RELATIVE_POSITION_TO_PAINTING()

	FLOAT fCam_Heading = GET_GAMEPLAY_CAM_RELATIVE_HEADING()
	FLOAT fCam_Pitch = GET_GAMEPLAY_CAM_RELATIVE_PITCH()
	PRINTLN("[ML] Cam heading = ", fCam_Heading)
	PRINTLN("[ML] Cam pitch = ", fCam_Pitch)
	
	IF fCam_Heading < -25.0
	AND fCam_Pitch < -25.0
		PRINTLN("[ML][GET_CAM_RELATIVE_POSITION_TO_PAINTING], Cam is above left of target")
		RETURN ciInteractWith_Painting_Enter_TLE_CAM
	ELIF  fCam_Heading > 25.0
	AND fCam_Pitch < -25.0
		PRINTLN("[ML][GET_CAM_RELATIVE_POSITION_TO_PAINTING], Cam is above right of target")
		RETURN ciInteractWith_Painting_Enter_TRE_CAM
	ELIF  fCam_Heading > 25.0
	AND fCam_Pitch > 25.0
		PRINTLN("[ML][GET_CAM_RELATIVE_POSITION_TO_PAINTING], Cam is below right of target")
		RETURN ciInteractWith_Painting_Enter_BRE_CAM	
	ELIF  fCam_Heading < -25.0
	AND fCam_Pitch > 25.0
		PRINTLN("[ML][GET_CAM_RELATIVE_POSITION_TO_PAINTING], Cam is below left of target")
		RETURN ciInteractWith_Painting_Enter_BLE_CAM
	ELSE 
		PRINTLN("[ML][GET_CAM_RELATIVE_POSITION_TO_PAINTING], Cam is tight behind target")
		RETURN ciInteractWith_Painting_Enter_RE_CAM
	ENDIF	
ENDFUNC

FUNC STRING GET_ANIM_NAME_FOR_PAINTING_CUT_CAMERA(INT iMode = 0)		// [ML] Get the animation name for the camera
	
	INT iQuadrant
	
	PRINTLN("[ML][GET_ANIM_NAME_FOR_PAINTING_CUT_CAMERA] iMode = ", iMode)
	
		SWITCH iMode
			CASE 0
				iQuadrant = GET_CAM_RELATIVE_POSITION_TO_PAINTING()
				PRINTLN("[ML][GET_ANIM_NAME_FOR_PAINTING_CUT_CAMERA] iQuadrant = ", iQuadrant)
				SWITCH iQuadrant
					CASE ciInteractWith_Painting_Enter_TLE_CAM	RETURN "VER_01_TOP_LEFT_ENTER_CAM_TLE"
					CASE ciInteractWith_Painting_Enter_TRE_CAM	RETURN "VER_01_TOP_LEFT_ENTER_CAM_TRE"
					CASE ciInteractWith_Painting_Enter_BRE_CAM	RETURN "VER_01_TOP_LEFT_ENTER_CAM_BRE"
					CASE ciInteractWith_Painting_Enter_BLE_CAM	RETURN "VER_01_TOP_LEFT_ENTER_CAM_BLE"
					CASE ciInteractWith_Painting_Enter_RE_CAM	RETURN "VER_01_TOP_LEFT_ENTER_CAM_RE"
				ENDSWITCH
			BREAK
			CASE 1
				iQuadrant = GET_CAM_RELATIVE_POSITION_TO_PAINTING()
				PRINTLN("[ML][GET_ANIM_NAME_FOR_PAINTING_CUT_CAMERA] iQuadrant = ", iQuadrant)
				SWITCH iQuadrant
					CASE ciInteractWith_Painting_Enter_TLE_CAM	RETURN "VER_01_TOP_RIGHT_ENTER_CAM_TLE"
					CASE ciInteractWith_Painting_Enter_TRE_CAM	RETURN "VER_01_TOP_RIGHT_ENTER_CAM_TRE"
					CASE ciInteractWith_Painting_Enter_BRE_CAM	RETURN "VER_01_TOP_RIGHT_ENTER_CAM_BRE"
					CASE ciInteractWith_Painting_Enter_BLE_CAM	RETURN "VER_01_TOP_RIGHT_ENTER_CAM_BLE"
					CASE ciInteractWith_Painting_Enter_RE_CAM	RETURN "VER_01_TOP_RIGHT_ENTER_CAM_RE"
				ENDSWITCH
			BREAK
			CASE 2
				iQuadrant = GET_CAM_RELATIVE_POSITION_TO_PAINTING()
				PRINTLN("[ML][GET_ANIM_NAME_FOR_PAINTING_CUT_CAMERA] iQuadrant = ", iQuadrant)
				SWITCH iQuadrant
					CASE ciInteractWith_Painting_Enter_TLE_CAM	RETURN "VER_01_BOTTOM_RIGHT_ENTER_CAM_TLE"
					CASE ciInteractWith_Painting_Enter_TRE_CAM	RETURN "VER_01_BOTTOM_RIGHT_ENTER_CAM_TRE"
					CASE ciInteractWith_Painting_Enter_BRE_CAM	RETURN "VER_01_BOTTOM_RIGHT_ENTER_CAM_BRE"
					CASE ciInteractWith_Painting_Enter_BLE_CAM	RETURN "VER_01_BOTTOM_RIGHT_ENTER_CAM_BLE"
					CASE ciInteractWith_Painting_Enter_RE_CAM	RETURN "VER_01_BOTTOM_RIGHT_ENTER_CAM_RE"
				ENDSWITCH
			BREAK
			CASE 3
				iQuadrant = GET_CAM_RELATIVE_POSITION_TO_PAINTING()
				PRINTLN("[ML][GET_ANIM_NAME_FOR_PAINTING_CUT_CAMERA] iQuadrant = ", iQuadrant)
				SWITCH iQuadrant
					CASE ciInteractWith_Painting_Enter_TLE_CAM	RETURN "VER_01_TOP_LEFT_ENTER_CAM_TLE"
					CASE ciInteractWith_Painting_Enter_TRE_CAM	RETURN "VER_01_TOP_LEFT_ENTER_CAM_TRE"
					CASE ciInteractWith_Painting_Enter_BRE_CAM	RETURN "VER_01_TOP_LEFT_ENTER_CAM_BRE"
					CASE ciInteractWith_Painting_Enter_BLE_CAM	RETURN "VER_01_TOP_LEFT_ENTER_CAM_BLE"
					CASE ciInteractWith_Painting_Enter_RE_CAM	RETURN "VER_01_TOP_LEFT_ENTER_CAM_RE"
				ENDSWITCH
			BREAK
			CASE 4		RETURN "VER_01_CUTTING_TOP_LEFT_IDLE_CAM"
			CASE 5		RETURN "VER_01_CUTTING_TOP_RIGHT_IDLE_CAM"
			CASE 6		RETURN "VER_01_CUTTING_BOTTOM_RIGHT_IDLE_CAM"
			CASE 7		RETURN "VER_01_CUTTING_TOP_LEFT_IDLE_CAM"
			CASE 8		RETURN "VER_01_CUTTING_TOP_LEFT_TO_RIGHT_CAM"
			CASE 9		RETURN "VER_01_CUTTING_RIGHT_TOP_TO_BOTTOM_CAM"
			CASE 10		RETURN "VER_01_CUTTING_BOTTOM_RIGHT_TO_LEFT_CAM"
			CASE 11		RETURN "VER_01_RE-ENTER_CUTTING_LEFT_TOP_TO_BOTTOM_CAM"		// VER_01_CUTTING_LEFT_TOP_TO_BOTTOM_CAM
			CASE 12		RETURN "VER_01_TOP_LEFT_EXIT_CAM"
			CASE 13		RETURN "VER_01_TOP_RIGHT_EXIT_CAM"
			CASE 14		RETURN "VER_01_BOTTOM_RIGHT_EXIT_CAM"
			CASE 15		RETURN "VER_01_TOP_LEFT_EXIT_CAM"
			CASE 16		RETURN "VER_01_WITH_PAINTING_EXIT_CAM_RE1"
			CASE 17
				iQuadrant = GET_CAM_RELATIVE_POSITION_TO_PAINTING()
				PRINTLN("[ML][GET_ANIM_NAME_FOR_PAINTING_CUT_CAMERA] iQuadrant = ", iQuadrant)
				SWITCH iQuadrant
					CASE ciInteractWith_Painting_Enter_TLE_CAM	RETURN "VER_02_TOP_LEFT_ENTER_CAM_TLE"
					CASE ciInteractWith_Painting_Enter_TRE_CAM	RETURN "VER_02_TOP_LEFT_ENTER_CAM_TRE"
					CASE ciInteractWith_Painting_Enter_BRE_CAM	RETURN "VER_02_TOP_LEFT_ENTER_CAM_BRE"
					CASE ciInteractWith_Painting_Enter_BLE_CAM	RETURN "VER_02_TOP_LEFT_ENTER_CAM_BLE"
					CASE ciInteractWith_Painting_Enter_RE_CAM	RETURN "VER_02_TOP_LEFT_ENTER_CAM_RE"
				ENDSWITCH
			BREAK
			CASE 18
				iQuadrant = GET_CAM_RELATIVE_POSITION_TO_PAINTING()
				PRINTLN("[ML][GET_ANIM_NAME_FOR_PAINTING_CUT_CAMERA] iQuadrant = ", iQuadrant)
				SWITCH iQuadrant
					CASE ciInteractWith_Painting_Enter_TLE_CAM	RETURN "VER_02_TOP_RIGHT_ENTER_CAM_TLE"
					CASE ciInteractWith_Painting_Enter_TRE_CAM	RETURN "VER_02_TOP_RIGHT_ENTER_CAM_TRE"
					CASE ciInteractWith_Painting_Enter_BRE_CAM	RETURN "VER_02_TOP_RIGHT_ENTER_CAM_BRE"
					CASE ciInteractWith_Painting_Enter_BLE_CAM	RETURN "VER_02_TOP_RIGHT_ENTER_CAM_BLE"
					CASE ciInteractWith_Painting_Enter_RE_CAM	RETURN "VER_02_TOP_RIGHT_ENTER_CAM_RE"
				ENDSWITCH
			BREAK
			CASE 19
				iQuadrant = GET_CAM_RELATIVE_POSITION_TO_PAINTING()
				PRINTLN("[ML][GET_ANIM_NAME_FOR_PAINTING_CUT_CAMERA] iQuadrant = ", iQuadrant)
				SWITCH iQuadrant
					CASE ciInteractWith_Painting_Enter_TLE_CAM	RETURN "VER_02_BOTTOM_RIGHT_ENTER_CAM_TLE"
					CASE ciInteractWith_Painting_Enter_TRE_CAM	RETURN "VER_02_BOTTOM_RIGHT_ENTER_CAM_TRE"
					CASE ciInteractWith_Painting_Enter_BRE_CAM	RETURN "VER_02_BOTTOM_RIGHT_ENTER_CAM_BRE"
					CASE ciInteractWith_Painting_Enter_BLE_CAM	RETURN "VER_02_BOTTOM_RIGHT_ENTER_CAM_BLE"
					CASE ciInteractWith_Painting_Enter_RE_CAM	RETURN "VER_02_BOTTOM_RIGHT_ENTER_CAM_RE"
				ENDSWITCH
			BREAK
			CASE 20
				iQuadrant = GET_CAM_RELATIVE_POSITION_TO_PAINTING()
				PRINTLN("[ML][GET_ANIM_NAME_FOR_PAINTING_CUT_CAMERA] iQuadrant = ", iQuadrant)
				SWITCH iQuadrant
					CASE ciInteractWith_Painting_Enter_TLE_CAM	RETURN "VER_02_TOP_LEFT_ENTER_CAM_TLE"
					CASE ciInteractWith_Painting_Enter_TRE_CAM	RETURN "VER_02_TOP_LEFT_ENTER_CAM_TRE"
					CASE ciInteractWith_Painting_Enter_BRE_CAM	RETURN "VER_02_TOP_LEFT_ENTER_CAM_BRE"
					CASE ciInteractWith_Painting_Enter_BLE_CAM	RETURN "VER_02_TOP_LEFT_ENTER_CAM_BLE"
					CASE ciInteractWith_Painting_Enter_RE_CAM	RETURN "VER_02_TOP_LEFT_ENTER_CAM_RE"
				ENDSWITCH
			BREAK
			CASE 21		RETURN "VER_02_CUTTING_TOP_LEFT_IDLE_CAM"
			CASE 22		RETURN "VER_02_CUTTING_TOP_RIGHT_IDLE_CAM"
			CASE 23		RETURN "VER_02_CUTTING_BOTTOM_RIGHT_IDLE_CAM"
			CASE 24		RETURN "VER_02_CUTTING_TOP_LEFT_IDLE_CAM"
			CASE 25		RETURN "VER_02_CUTTING_TOP_LEFT_TO_RIGHT_CAM"
			CASE 26		RETURN "VER_02_CUTTING_RIGHT_TOP_TO_BOTTOM_CAM"
			CASE 27		RETURN "VER_02_CUTTING_BOTTOM_RIGHT_TO_LEFT_CAM"
			CASE 28		RETURN "VER_02_RE-ENTER_CUTTING_LEFT_TOP_TO_BOTTOM_CAM"	// VER_02_CUTTING_LEFT_TOP_TO_BOTTOM_CAM
			CASE 29		RETURN "VER_02_TOP_LEFT_EXIT_CAM"
			CASE 30		RETURN "VER_02_TOP_RIGHT_EXIT_CAM"
			CASE 31		RETURN "VER_02_BOTTOM_RIGHT_EXIT_CAM"
			CASE 32		RETURN "VER_02_TOP_LEFT_EXIT_CAM"
			CASE 33		RETURN "VER_02_WITH_PAINTING_EXIT_CAM"
		ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC BOOL SET_UP_EXTRA_INTERACT_WITH_SYNC_SCENE_ENTITIES(INT iObj, ENTITY_INDEX eiInteractObj, OBJECT_INDEX oiTempObj, INT iMode=0)

	BOOL bReady = TRUE
	
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim
		CASE ciINTERACT_WITH_PRESET__POWER_BOX_CRANK
			NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(eiInteractObj,  MC_playerBD[iLocalPart].iSynchSceneID, "anim@GangOps@Hanger@FUSE_BOX@", "SWITCH_ON_BOX", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__CRATE_IN_VAN
			IF DOES_ENTITY_EXIST(oiTempObj)

				sIWInfo.eiInteractWith_CurrentSceneAlignmentEntity_AttachedEntity = GET_ENTITY_ATTACHED_TO(oiTempObj)
				
				IF NETWORK_HAS_CONTROL_OF_ENTITY(oiTempObj)
				AND NETWORK_HAS_CONTROL_OF_ENTITY(sIWInfo.eiInteractWith_CurrentSceneAlignmentEntity_AttachedEntity)
					PRINTLN("[InteractWith] Adding crate to sync scene!")
					NETWORK_ALLOW_REMOTE_ATTACHMENT_MODIFICATION(oiTempObj, TRUE)
					
					SET_ENTITY_COLLISION(oiTempObj, FALSE)
					//DETACH_ENTITY(oiTempObj)
					
					FREEZE_ENTITY_POSITION(oiTempObj, TRUE)
					SET_ENTITY_DYNAMIC(oiTempObj, FALSE)
					
					FREEZE_ENTITY_POSITION(sIWInfo.eiInteractWith_CurrentSceneAlignmentEntity_AttachedEntity, TRUE)
					SET_ENTITY_DYNAMIC(sIWInfo.eiInteractWith_CurrentSceneAlignmentEntity_AttachedEntity, FALSE)
					
					NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(oiTempObj,  MC_playerBD[iLocalPart].iSynchSceneID, "anim@GangOps@VAN@DRIVE_GRAB@", "crate_grab_drive", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_BLOCK_MOVER_UPDATE)

					bReady = TRUE
					RETURN TRUE
				ELSE
					NETWORK_REQUEST_CONTROL_OF_ENTITY(oiTempObj)
					NETWORK_REQUEST_CONTROL_OF_ENTITY(sIWInfo.eiInteractWith_CurrentSceneAlignmentEntity_AttachedEntity)
					PRINTLN("[InteractWith] Currently requesting control of oiTempObj and sIWInfo.eiInteractWith_CurrentSceneAlignmentEntity_AttachedEntity!")
					RETURN FALSE
				ENDIF
			ELSE
				PRINTLN("[InteractWith] No crate object available!")
			ENDIF
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__SEARCH_BODY_ON_FLOOR
			
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__CUT_OUT_PAINTING
			IF DOES_ENTITY_EXIST(oiTempObj)				
				NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(oiTempObj, MC_playerBD[iLocalPart].iSynchSceneID, "ANIM_HEIST@HS3F@IG11_STEAL_PAINTING@MALE@", GET_ANIM_NAME_FOR_PAINTING_CUT_PAINTING_OBJECT(iObj, iMode), INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
				PRINTLN("[ML] Adding painting object to sync scene")
				
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niMinigameObjects[0])
					IF DOES_ENTITY_EXIST(NET_TO_OBJ(niMinigameObjects[0]))
						PRINTLN("[ML] Adding bag object to sync scene")
						NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(niMinigameObjects[0]), MC_playerBD[iLocalPart].iSynchSceneID, "ANIM_HEIST@HS3F@IG11_STEAL_PAINTING@MALE@", GET_ANIM_NAME_FOR_PAINTING_CUT_BAG_OBJECT(iObj, iMode), INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
					ELSE
						PRINTLN("[ML] NOT adding bag object to sync scene")
					ENDIF
				ENDIF	
				RETURN TRUE
			ENDIF		
		BREAK
	ENDSWITCH
	
	RETURN bReady
ENDFUNC

FUNC TEXT_LABEL_15 GET_INTERACT_WITH_PROMPT(INT iObj)
	TEXT_LABEL_15 tl15 = "MC_INTOBJ_"
	tl15 += g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim
	
	RETURN tl15
ENDFUNC

FUNC FLOAT GET_INTERACTION_DISTANCE_FROM_OBJECT(INT iObj)
	
	//Use the override setting if it's used
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].fActivationRangeOverride > 0
		#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
			DRAW_DEBUG_SPHERE(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].vPos, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].fActivationRangeOverride, 255, 255, 255, 100)
		ENDIF
		#ENDIF
		
		RETURN (g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].fActivationRangeOverride * g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].fActivationRangeOverride)
	ENDIF
	
	//Otherwise use these
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim
		CASE ciINTERACT_WITH_PRESET__PUSH_BUTTON
			RETURN 3.5
		CASE ciINTERACT_WITH_PRESET__RUMMAGE_AROUND
			RETURN 0.5
		CASE ciINTERACT_WITH_PRESET__DOWNLOAD
			RETURN 1.0
		CASE ciINTERACT_WITH_PRESET__HACK_LAPTOP_USB
			RETURN 3.5
		CASE ciINTERACT_WITH_PRESET__CRATE_IN_VAN
			RETURN 4.0
		CASE ciINTERACT_WITH_PRESET__PLANT_RADIO_OVERRIDE
			RETURN 8.0
		CASE ciINTERACT_WITH_PRESET__CLOTHES
			RETURN 8.0
		CASE ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__LEFT
		CASE ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__RIGHT
			RETURN 2.0
	ENDSWITCH
	
	RETURN 2.5
ENDFUNC

FUNC BOOL IS_OBJECT_INTERACT_WITH_A_SYNC_SCENE_ANIM(INT iObj)
	
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim
		CASE ciINTERACT_WITH_PRESET__SEARCH_BODY_ON_TABLE
		CASE ciINTERACT_WITH_PRESET__SEARCH_BODY_ON_FLOOR
		CASE ciINTERACT_WITH_PRESET__POWER_BOX_CRANK
		CASE ciINTERACT_WITH_PRESET__HACK_LAPTOP_USB
		CASE ciINTERACT_WITH_PRESET__CRATE_IN_VAN
		CASE ciINTERACT_WITH_PRESET__CLOTHES
		CASE ciINTERACT_WITH_PRESET__PLANT_RADIO_OVERRIDE
		CASE ciINTERACT_WITH_PRESET__HOLD_BUTTON
		CASE ciINTERACT_WITH_PRESET__CUT_OUT_PAINTING
		CASE ciINTERACT_WITH_PRESET__USE_KEYCARD
		CASE ciINTERACT_WITH_PRESET__PUSH_DAILY_CASH_BUTTON
		CASE ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__LEFT
		CASE ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__RIGHT
		CASE ciINTERACT_WITH_PRESET__PLANT_CASINO_TUNNEL_EXPLOSIVE
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL INIT_INTERACT_WITH_DOWNLOAD_SCREEN()
	sfiDownloadScreen = REQUEST_SCALEFORM_MOVIE("MORGUE_LAPTOP")
	IF HAS_SCALEFORM_MOVIE_LOADED(sfiDownloadScreen)
		SET_BIT(iLocalBoolCheck26, LBOOL26_DOWNLOADING_STARTED)
		CLEAR_BIT(iLocalBoolCheck26, LBOOL26_DOWNLOAD_PAUSE_STARTED)
		
		NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE)
		BEGIN_SCALEFORM_MOVIE_METHOD(sfiDownloadScreen, "SET_PROGRESS_PERCENT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		END_SCALEFORM_MOVIE_METHOD()
		
		fInteractWith_DownloadPercentage = 0.0
		SET_MULTIHEAD_SAFE(TRUE, FALSE, TRUE, TRUE)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PROCESS_INTERACT_WITH_DOWNLOAD_SCREEN(INT iObj, OBJECT_INDEX tempObj, BOOL bUseScaleform = FALSE)

	IF NOT HAS_NET_TIMER_STARTED(sIWInfo.stInteractWith_DownloadStutterTimer[iObj])
	OR HAS_NET_TIMER_EXPIRED(sIWInfo.stInteractWith_DownloadStutterTimer[iObj], ciINTERACT_WITH_DOWNLOAD_PAUSE_TIME)
	
		IF NETWORK_GET_RANDOM_INT_RANGED(0, 100) > ciINTERACT_WITH_DOWNLOAD_STUTTER_AMOUNT
		OR fInteractWith_DownloadPercentage <= ciINTERACT_WITH_DOWNLOAD_START_STUTTERING_AT
			
			IF fInteractWith_DownloadPercentage <= ciINTERACT_WITH_DOWNLOAD_START_STUTTERING_AT
				fInteractWith_DownloadPercentage = fInteractWith_DownloadPercentage +@ ciINTERACT_WITH_DOWNLOAD_SPEED
			ELSE
				fInteractWith_DownloadPercentage = fInteractWith_DownloadPercentage +@ TO_FLOAT(NETWORK_GET_RANDOM_INT_RANGED(0, ciINTERACT_WITH_DOWNLOAD_SPEED))
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_DOWNLOAD_PAUSE_STARTED)
			IF fInteractWith_DownloadPercentage > ciINTERACT_WITH_DOWNLOAD_PAUSE_AT_PERCENT
				fInteractWith_DownloadPercentage = TO_FLOAT(ciINTERACT_WITH_DOWNLOAD_PAUSE_AT_PERCENT)
			ENDIF
			
			IF fInteractWith_DownloadPercentage >= ciINTERACT_WITH_DOWNLOAD_PAUSE_AT_PERCENT
				PRINTLN("Starting stutter timer!")
				START_NET_TIMER(sIWInfo.stInteractWith_DownloadStutterTimer[iObj])
				SET_BIT(iLocalBoolCheck26, LBOOL26_DOWNLOAD_PAUSE_STARTED)
			ENDIF
		ENDIF
	ENDIF
	
	IF bUseScaleform
	
		IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL28_STARTED_DOWNLOAD_SND)
			iInteractWith_DownloadSndID = GET_SOUND_ID()
			PLAY_SOUND_FRONTEND(iInteractWith_DownloadSndID, "laptop_download_loop", "dlc_xm_heists_iaa_morgue_sounds")
		
			iInteractWith_DownloadSndID_Remote = GET_SOUND_ID()
			PLAY_SOUND_FROM_COORD(iInteractWith_DownloadSndID_Remote, "laptop_download_loop", GET_ENTITY_COORDS(tempObj), "dlc_xm_heists_iaa_morgue_sounds")
			
			SET_BIT(iLocalBoolCheck27, LBOOL28_STARTED_DOWNLOAD_SND)
		ENDIF
		
		IF HAS_SCALEFORM_MOVIE_LOADED(sfiDownloadScreen)
			BEGIN_SCALEFORM_MOVIE_METHOD(sfiDownloadScreen, "SET_PROGRESS_PERCENT")
				INT iPercentage = ROUND(fInteractWith_DownloadPercentage)
				
				IF iPercentage >= 100
					iPercentage = 100
					
					IF IS_SOUND_ID_VALID(iInteractWith_DownloadSndID)
						IF NOT HAS_SOUND_FINISHED(iInteractWith_DownloadSndID)
							STOP_SOUND(iInteractWith_DownloadSndID)
							RELEASE_SOUND_ID(iInteractWith_DownloadSndID)
						ENDIF
					ENDIF
					
					IF IS_SOUND_ID_VALID(iInteractWith_DownloadSndID_Remote)
						IF NOT HAS_SOUND_FINISHED(iInteractWith_DownloadSndID_Remote)
							STOP_SOUND(iInteractWith_DownloadSndID_Remote)
							RELEASE_SOUND_ID(iInteractWith_DownloadSndID_Remote)
						ENDIF
					ENDIF
				ENDIF
				
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPercentage)
			END_SCALEFORM_MOVIE_METHOD()
			DRAW_SCALEFORM_MOVIE_FULLSCREEN(sfiDownloadScreen, 255, 255, 255, 255)
			
			DISABLE_INTERACTION_MENU()
			DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD, TRUE)
			DISABLE_DPADDOWN_THIS_FRAME()
		ENDIF
	ELSE
		IF HAS_NET_TIMER_STARTED(sIWInfo.stInteractWith_DownloadTimer[iObj])
			DRAW_GENERIC_METER(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sIWInfo.stInteractWith_DownloadTimer[iObj]), ciInteractWith_MeterFillDuration - 500, "IW_DL")
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_INTERACT_WITH_DOWNLOAD_SCREEN()
	NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
	CLEAR_BIT(iLocalBoolCheck26, LBOOL26_DOWNLOADING_STARTED)
	CLEAR_BIT(iLocalBoolCheck26, LBOOL26_DOWNLOAD_PAUSE_STARTED)
	IF HAS_SCALEFORM_MOVIE_LOADED(sfiDownloadScreen)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfiDownloadScreen)
	ENDIF
	
	SET_MULTIHEAD_SAFE(FALSE, FALSE, TRUE, TRUE)
	fInteractWith_DownloadPercentage = 0.0
	
	IF IS_SOUND_ID_VALID(iInteractWith_DownloadSndID)
		IF NOT HAS_SOUND_FINISHED(iInteractWith_DownloadSndID)
			STOP_SOUND(iInteractWith_DownloadSndID)
			RELEASE_SOUND_ID(iInteractWith_DownloadSndID)
		ENDIF
	ENDIF
	
	IF IS_SOUND_ID_VALID(iInteractWith_DownloadSndID_Remote)
		IF NOT HAS_SOUND_FINISHED(iInteractWith_DownloadSndID_Remote)
			STOP_SOUND(iInteractWith_DownloadSndID_Remote)
			RELEASE_SOUND_ID(iInteractWith_DownloadSndID_Remote)
		ENDIF
	ENDIF
	
	CLEAR_BIT(iLocalBoolCheck27, LBOOL28_STARTED_DOWNLOAD_SND)
	
	ENABLE_INTERACTION_MENU()
ENDPROC

FUNC BOOL IS_INTERACT_WITH_HEADING_DEPENDANT(INT iObj)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__ACTIVATE_EXPLOSIVES
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__ENTER_CASINO_ENTRANCE
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC BOOL IS_INTERACT_WITH_INSTANT(INT iObj)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__DOWNLOAD_INSTANT
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__ACTIVATE_EXPLOSIVES
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__ENTER_CASINO_ENTRANCE
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_INTERACT_WITH_HAVE_NO_ANIM(INT iObj)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__DOWNLOAD
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__DOWNLOAD_BAR_ONLY
		RETURN TRUE
	ENDIF
	
	IF IS_STRING_NULL_OR_EMPTY(GET_INTERACT_WITH_PLAYER_ANIM_DICT(iObj))
		RETURN TRUE
	ENDIF
	
	IF IS_INTERACT_WITH_INSTANT(iObj)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_INTERACT_WITH_STARTING_ANIMATION_MODE(INT iObj)

	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__CUT_OUT_PAINTING
	AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex > -1
		RETURN iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex]
	ENDIF
	
	RETURN 0
	
ENDFUNC

FUNC BOOL SHOULD_INTERACT_WITH_SEQUENCE_QUICKBLEND(INT iObj, OBJECT_INDEX tempObj)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__USE_KEYCARD
		VECTOR vPlayerCoords = GET_ENTITY_COORDS(LocalPlayerPed)
		VECTOR vObjectCoords = GET_ENTITY_COORDS(tempObj)
		FLOAT fMaxDistance = 1.0
		
		vPlayerCoords.z = sIWInfo.vInteractWith_TargetPos.z
		FLOAT fDistToTargetPoint = VDIST2(vPlayerCoords, sIWInfo.vInteractWith_TargetPos)
		
		vPlayerCoords.z = vObjectCoords.z
		FLOAT fDistToObject = VDIST2(vPlayerCoords, vObjectCoords)
		
		PRINTLN("[InteractWith] SHOULD_INTERACT_WITH_SEQUENCE_QUICKBLEND - fDistToTargetPoint: ", fDistToTargetPoint, " / fDistToObject: ", fDistToObject)
		
		IF fDistToTargetPoint > fDistToObject
		AND fDistToTargetPoint < fMaxDistance
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_INTERACT_WITH_REQUIRE_PHONE_ACTIVATION(INT iObj)

	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__ACTIVATE_EXPLOSIVES
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_INTERACT_WITH_ANIMATION_FINISHED(INT iObj, STRING sDict, STRING sAnim)
	
	IF DOES_INTERACT_WITH_HAVE_NO_ANIM(iObj)
		RETURN TRUE
	ENDIF
	
	IF IS_OBJECT_INTERACT_WITH_A_SYNC_SCENE_ANIM(iObj)
		
		IF DOES_INTERACTION_SYNC_SCENE_BLEND_SEAMLESSLY(iObj)
			IF GET_SYNC_SCENE_PHASE(MC_playerBD[iLocalPart].iSynchSceneID) >= 0.95
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_CASINO_TUNNEL_EXPLOSIVE
			IF GET_SYNC_SCENE_PHASE(MC_playerBD[iLocalPart].iSynchSceneID) >= 0.2
				RETURN TRUE
			ENDIF
			
		ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__LEFT
		OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__RIGHT
			FLOAT fPhaseToUse = 1.0
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__LEFT
				fPhaseToUse = 0.85
			ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__RIGHT
				fPhaseToUse = 0.75
			ENDIF
			
			IF GET_SYNC_SCENE_PHASE(MC_playerBD[iLocalPart].iSynchSceneID) >= fPhaseToUse
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF HAS_ANIM_BREAKOUT_EVENT_FIRED()
			PRINTLN("[InteractWith] HAS_INTERACT_WITH_ANIMATION_FINISHED - 'BREAKOUT' event fired on local player")
			SET_BIT(sIWInfo.iInteractWith_Bitset, ciInteractWith_Bitset__BrokeOut)
			RETURN TRUE
		ENDIF
		
		IF NOT IS_SYNC_SCENE_RUNNING(MC_playerBD[iLocalPart].iSynchSceneID)
			RETURN TRUE
		ENDIF
	ELSE
		IF NOT IS_ENTITY_PLAYING_ANIM(LocalPlayerPed, sDict, sAnim)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_INTERACT_WITH_DOWNLOAD_FINISHED(INT iObj)

	//Scaleform way
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__DOWNLOAD
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__HACK_LAPTOP_USB
	
		FLOAT fDelayWhenFull = 15
		IF fInteractWith_DownloadPercentage >= 100 + fDelayWhenFull
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	
	//Timer way
	IF HAS_NET_TIMER_STARTED(sIWInfo.stInteractWith_DownloadTimer[iObj])
		IF HAS_NET_TIMER_EXPIRED(sIWInfo.stInteractWith_DownloadTimer[iObj], ciInteractWith_MeterFillDuration)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEANUP_INTERACT_WITH__EXTRA_BEHAVIOUR()
	
	IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_HAS_CUT_PAINTING_EXITED_EARLY)
		CLEAR_BIT(iLocalBoolCheck32, LBOOL32_HAS_CUT_PAINTING_EXITED_EARLY)
		PRINTLN("[InteractWith] Clearing LBOOL32_HAS_CUT_PAINTING_EXITED_EARLY")
	ENDIF
	
	IF DOES_ENTITY_EXIST(sIWInfo.oiInteractWith_CachedObj)
	AND sIWInfo.iInteractWith_CachedIndex > -1
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[sIWInfo.iInteractWith_CachedIndex].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__CRATE_IN_VAN
		AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedObject[sIWInfo.iInteractWith_CachedIndex].iAttachParent])
			
			VEHICLE_INDEX van = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedObject[sIWInfo.iInteractWith_CachedIndex].iAttachParent])
			FREEZE_ENTITY_POSITION(van, FALSE)
			
			SET_ENTITY_COLLISION(van, TRUE)
			SET_ENTITY_DYNAMIC(van, TRUE)
			
			SET_ENTITY_COLLISION(sIWInfo.oiInteractWith_CachedObj, TRUE)
			SET_ENTITY_DYNAMIC(sIWInfo.oiInteractWith_CachedObj, TRUE)
			
			ATTACH_OBJECT_TO_FLATBED(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[sIWInfo.iInteractWith_CachedIndex]),
								van,
								GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(van, GET_ENTITY_COORDS(sIWInfo.oiInteractWith_CachedObj)),
								//g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vAttachOffset,
								g_FMMC_STRUCT_ENTITIES.sPlacedObject[sIWInfo.iInteractWith_CachedIndex].vAttachOffsetRotation,
								TRUE, FALSE)
								
			PRINTLN("[InteractWith] Attaching interaction object back on!")
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_INTERACT_WITH_ATTACHED_VEHICLE(INT iobj, OBJECT_INDEX tempObj)

	FLOAT fMaximumDistanceForMyVehicle = 4.50	
	
	INT iClosestVehIndexSoFar = -1
	
	IF iInteractObject_AttachedVehicle[iObj] = -1	
	
		IF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE (ciGANGOPS_FLOW_MISSION_RIOTVAN)
			fMaximumDistanceForMyVehicle = 10
			PRINTLN("[InteractWith][ObjVehIndex] Upping the maximum distance allowed because this is the Riot Van mission and we know it'll be okay")
		ENDIF
		
		INT iVehLoop = 0
		FOR iVehLoop = 0 TO (FMMC_MAX_VEHICLES -1)
			IF NOT (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehLoop].mn = DUMMY_MODEL_FOR_SCRIPT)
				IF IS_ENTITY_ALIVE(tempObj)
					FLOAT fDistToVeh = VDIST(GET_ENTITY_COORDS(tempObj), g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehLoop].vPos)
					PRINTLN("[InteractWith][ObjVehIndex] Obj ", iObj, " fDistToVeh = ", fDistToVeh, " for vehicle ", iVehLoop)
					
					IF fDistToVeh < fMaximumDistanceForMyVehicle
						fMaximumDistanceForMyVehicle = fDistToVeh
						iClosestVehIndexSoFar = iVehLoop
						PRINTLN("[InteractWith][ObjVehIndex]", "(Obj ", iobj, ") Found a vehicle I'm close enough to! Obj ", iObj, "'s iClosestVehIndexSoFar is now ", iClosestVehIndexSoFar)
					ELSE
						PRINTLN("[InteractWith][ObjVehIndex]", "(Obj ", iobj, ") Too far from veh ", iVehLoop)
					ENDIF
				ENDIF
			ELSE
				//PRINTLN("[InteractWith][ObjVehIndex]", "(Obj ", iobj, ")", " Vehicle ", iVehLoop, "'s model is DUMMY_MODEL_FOR_SCRIPT; not checking this one")
			ENDIF
		ENDFOR
		
		IF iClosestVehIndexSoFar > -1
			iInteractObject_AttachedVehicle[iObj] = iClosestVehIndexSoFar
			
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iInteractObject_AttachedVehicle[iObj]])
				SET_ENTITY_NO_COLLISION_ENTITY(NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicle[iInteractObject_AttachedVehicle[iObj]]), tempObj, FALSE)
				PRINTLN("[InteractWith][ObjVehIndex] Setting no collision between niVehicle[", iInteractObject_AttachedVehicle[iObj], "] and tempObj")
			ENDIF
			
			PRINTLN("[InteractWith][ObjVehIndex] Closest vehicle has been decided for Obj ", iObj, "! iInteractObject_AttachedVehicle is now ", iInteractObject_AttachedVehicle[iObj])
		ENDIF
		
		IF iInteractObject_AttachedVehicle[iObj] = -1
			iInteractObject_AttachedVehicle[iObj] = -2
			//PRINTLN("[InteractWith][ObjVehIndex] Obj ", iObj, " setting iInteractObject_AttachedVehicle to -2 so that it won't look for a vehicle anymore")
		ENDIF
	ENDIF
ENDPROC

FUNC MP_OUTFIT_ENUM GET_FIREMAN_OUTFIT_FOR_CHANGE_CLOTHES()
	INT iPartInTeam = GET_PARTICIPANT_NUMBER_IN_TEAM()
	SWITCH iPartInTeam
		CASE 0 RETURN OUTFIT_CASINO_HEIST_FIREFIGHTER_4
		CASE 1 RETURN OUTFIT_CASINO_HEIST_FIREFIGHTER_5
		CASE 2 RETURN OUTFIT_CASINO_HEIST_FIREFIGHTER_6
		CASE 3 RETURN OUTFIT_CASINO_HEIST_FIREFIGHTER_7
	ENDSWITCH
	RETURN OUTFIT_CASINO_HEIST_FIREFIGHTER_0
ENDFUNC

FUNC MP_OUTFIT_ENUM GET_NOOSE_OUTFIT_FOR_CHANGE_CLOTHES()
	INT iPartInTeam = GET_PARTICIPANT_NUMBER_IN_TEAM()
	SWITCH iPartInTeam
		CASE 0 RETURN OUTFIT_CASINO_HEIST_NOOSE_0
		CASE 1 RETURN OUTFIT_CASINO_HEIST_NOOSE_1
		CASE 2 RETURN OUTFIT_CASINO_HEIST_NOOSE_2
		CASE 3 RETURN OUTFIT_CASINO_HEIST_NOOSE_3
	ENDSWITCH
	RETURN OUTFIT_CASINO_HEIST_NOOSE_0
ENDFUNC

FUNC MP_OUTFIT_ENUM GET_HIGH_ROLLER_OUTFIT_FOR_CHANGE_CLOTHES()
	INT iPartInTeam = GET_PARTICIPANT_NUMBER_IN_TEAM()
	SWITCH iPartInTeam
		CASE 0 RETURN OUTFIT_CASINO_HEIST_HIGH_ROLLER_0
		CASE 1 RETURN OUTFIT_CASINO_HEIST_HIGH_ROLLER_1
		CASE 2 RETURN OUTFIT_CASINO_HEIST_HIGH_ROLLER_2
		CASE 3 RETURN OUTFIT_CASINO_HEIST_HIGH_ROLLER_3
	ENDSWITCH
	RETURN OUTFIT_CASINO_HEIST_HIGH_ROLLER_0
ENDFUNC

FUNC MP_OUTFIT_ENUM GET_CHANGE_CLOTHES_OUTFIT_FROM_SELECTION(INT iObj)
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iClothesChangeOutfit
		CASE ciChangeClothesNOOSE		RETURN	GET_NOOSE_OUTFIT_FOR_CHANGE_CLOTHES()
		CASE ciChangeClothesFireman		RETURN	GET_FIREMAN_OUTFIT_FOR_CHANGE_CLOTHES()
		CASE ciChangeClothesHighroller	RETURN	GET_HIGH_ROLLER_OUTFIT_FOR_CHANGE_CLOTHES()
	ENDSWITCH
	
	RETURN OUTFIT_MP_FREEMODE
ENDFUNC

FUNC BOOL IS_INTERACT_WITH_OBJECT_DISABLED_BY_ATTACHED_VEHICLE(INT iObj, OBJECT_INDEX tempObj)

	BOOL bObjInFire = FALSE
	
	IF iInteractObject_AttachedVehicle[iObj] > -1
	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iInteractObject_AttachedVehicle[iObj]])
		
		//Checks if my vehicle is "on fire"
		IF MC_serverBD_4.fFireStrength[iInteractObject_AttachedVehicle[iObj]] > 0.0
			bObjInFire = TRUE
		ENDIF
		
		PRINTLN("[ObjVehIndex][CannotInteract] Obj ", iObj, "'s attached vehicle's fire strength is ", MC_serverBD_4.fFireStrength[iInteractObject_AttachedVehicle[iObj]])
		
		VEHICLE_INDEX viVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iInteractObject_AttachedVehicle[iObj]])
		
		IF IS_ENTITY_ALIVE(viVeh)
		
			#IF IS_DEBUG_BUILD		
			DRAW_DEBUG_LINE(GET_ENTITY_COORDS(tempobj), GET_ENTITY_COORDS(viVeh))
			#ENDIF
		
			VECTOR vVehUpVector = GET_ENTITY_UP_VECTOR(viVeh)
			
			IF vVehUpVector.z <= 0.8
				#IF IS_DEBUG_BUILD
				IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
					DRAW_DEBUG_LINE(GET_ENTITY_COORDS(tempobj), GET_ENTITY_COORDS(tempobj) + vVehUpVector, 255, 0, 0, 255)
				ENDIF
				#ENDIF
				
				PRINTLN("[CannotInteract] Not allowing interaction because the attached vehicle is at a funny angle. iObj: ", iobj)
				
				IF VDIST2(GET_ENTITY_COORDS(LocalPlayerPed), GET_ENTITY_COORDS(tempobj)) < 9.5
					DISPLAY_HELP_TEXT_THIS_FRAME_FOR_MINIGAME("MC_INTOBJ_VANG", TRUE)
				ENDIF
				
				RETURN FALSE
			ELSE
				#IF IS_DEBUG_BUILD
				IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
					DRAW_DEBUG_LINE(GET_ENTITY_COORDS(tempobj), GET_ENTITY_COORDS(tempobj) + vVehUpVector, 0, 255, 0, 255)
				ENDIF
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bObjInFire
ENDFUNC

FUNC BOOL PLAYER_CAN_INTERACT_WITH_OBJ(INT iobj, OBJECT_INDEX tempObj, BOOL bMainCheck = FALSE)
	
	VECTOR vMyPos = GET_ENTITY_COORDS(LocalPlayerPed)
	VECTOR vObjPos = GET_ENTITY_COORDS(tempObj)
	FLOAT fZDifference = ABSF(vMyPos.z - vObjPos.z)
	
	IF sIWInfo.bInteractWith_ExtraDebug
		PRINTLN("[ObjVehIndex][CannotInteract] Now checking if interaction should be allowed. iObj: ", iObj)
	ENDIF
	
	IF bMainCheck
		IF NOT DOES_INTERACT_WITH_HAVE_NO_ANIM(iobj)
			IF NOT HAS_ANIM_DICT_LOADED(GET_INTERACT_WITH_PLAYER_ANIM_DICT(iObj))
				PRINTLN("[CannotInteract] Obj ", iObj, " still loading ", sIWInfo.sInteractWith_Dictionary)
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF IS_INTERACT_WITH_HEADING_DEPENDANT(iObj)
			IF NOT IS_PED_CLOSE_TO_HEADING(LocalPlayerPed, sIWInfo.fInteractWith_RequiredHeading, 80)
				PRINTLN("[CannotInteract] Not allowing interaction because the player is facing the wrong way. iObj: ", iobj)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF SHOULD_OBJECT_MINIGAME_BE_DISABLED_BY_POISON_GAS(iObj)
		PRINTLN("[CannotInteract] Not allowing interaction because of poison gas. iObj: ", iobj)
		RETURN FALSE
	ENDIF
	
	IF IS_INTERACT_WITH_OBJECT_DISABLED_BY_ATTACHED_VEHICLE(iObj, tempObj)
		PRINTLN("[CannotInteract] Not allowing interaction because of IS_INTERACT_WITH_OBJECT_DISABLED_BY_ATTACHED_VEHICLE. iObj: ", iobj)
		RETURN FALSE
	ENDIF
	
	IF IS_AIMING_THROUGH_SNIPER_SCOPE(LocalPlayerPed)
	OR (IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM) AND GET_CURRENT_PLAYER_WEAPON_TYPE() != WEAPONTYPE_UNARMED)
		PRINTLN("[CannotInteract] Not allowing interaction because the player is holding the left trigger and has a weapon. iObj: ", iobj)
		RETURN FALSE
	ENDIF
	
	IF IS_ENTITY_ON_FIRE(LocalPlayerPed)
		PRINTLN("[CannotInteract] Not allowing interaction because the player is on fire. iObj: ", iobj)
		RETURN FALSE
	ENDIF
	
	IF IS_PED_JUMPING(LocalPlayerPed)
		PRINTLN("[CannotInteract] Not allowing interaction because the player is jumping. iObj: ", iobj)
		RETURN FALSE
	ENDIF
	
	IF IS_PED_CLIMBING(LocalPlayerPed)
		PRINTLN("[CannotInteract] Not allowing interaction because the player is climbing. iObj: ", iobj)
		RETURN FALSE
	ENDIF
	
	IF IS_PED_PERFORMING_MELEE_ACTION(LocalPlayerPed)
		PRINTLN("[CannotInteract] Not allowing interaction because the player is performing a melee action. iObj: ", iobj)
		RETURN FALSE
	ENDIF
	
	IF IS_BROWSER_OPEN()
		PRINTLN("[CannotInteract] Not allowing interaction because Browser is open. iObj: ", iobj)
		RETURN FALSE
	ENDIF
	
	IF IS_PAUSE_MENU_ACTIVE()
		PRINTLN("[CannotInteract] Not allowing interaction because pause menu is active. iObj: ", iobj)
		RETURN FALSE
	ENDIF
	
	IF IS_INTERACTION_MENU_OPEN()
		PRINTLN("[CannotInteract] Not allowing interaction because interaction menu is active. iObj: ", iobj)
		RETURN FALSE
	ENDIF
	
	IF IS_HUD_COMPONENT_ACTIVE(NEW_HUD_WEAPON_WHEEL)
	OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SELECT_WEAPON)
		PRINTLN("[CannotInteract] Not allowing interaction because Weapon Wheel is showing or the player is holding LB/L1. iObj: ", iobj)
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_USING_DRONE()
	OR IS_PLAYER_INITIALISING_DRONE()
	OR IS_DRONE_CLEANING_UP()
		PRINTLN("[CannotInteract] Not allowing interaction because player is using a drone. iObj: ", iobj)
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__ACTIVATE_EXPLOSIVES
	AND bMainCheck
	
		BOOL bAllPlayersSafe = TRUE
		INT iPart, iNumPlayersChecked
		
		WHILE DO_PLAYER_LOOP(iPart, iNumPlayersChecked)
			IF MC_playerBD_1[iPart].iExplosionDangerZoneInsideBS > 0
				bAllPlayersSafe = FALSE
				PRINTLN("[CannotInteract][InteractWith][VaultDoorExplosives] Participant ", iPart, " is in the danger zone!")
				BREAKLOOP
			ELSE
				PRINTLN("[CannotInteract][InteractWith][VaultDoorExplosives] Participant ", iPart, " is safe")
			ENDIF
			
			iPart++
		ENDWHILE
		
		IF bAllPlayersSafe
			RETURN TRUE
		ELSE
		
			IF MC_playerBD_1[iPartToUse].iExplosionDangerZoneInsideBS = 0
				// I'm not in the zone, but someone else is
				sIWInfo.iInteractWith_ObjectiveTextToUse = ciObjectiveText_AltSecondary
			ELSE
				// I'm personally inside the danger zone
				sIWInfo.iInteractWith_ObjectiveTextToUse = ciObjectiveText_Secondary
			ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF fZDifference > GET_INTERACTION_DISTANCE_FROM_OBJECT(iObj) * 0.75
		PRINTLN("[CannotInteract] Not allowing interaction because the Z difference is ", fZDifference, ". iObj: ", iobj)
		RETURN FALSE
	ENDIF
	
	IF MC_serverBD.iObjHackPart[iobj] != -1
	AND MC_serverBD.iObjHackPart[iobj] != iLocalPart
		PRINTLN("[InteractWith][CannotInteract] Not allowing interaction because someone is already hacking this object. iObj: ", iobj)
		RETURN FALSE
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		PRINTLN("[CannotInteract] Not allowing interaction because the player is in a vehicle. iObj: ", iobj)
		RETURN FALSE
	ENDIF
	
	IF IS_PHONE_ONSCREEN()
		PRINTLN("[CannotInteract] Not allowing interaction because the phone is up. iObj: ", iobj)
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__CLOTHES
		IF IS_BIT_SET(MC_serverBD_1.iOffRuleMinigameCompleted, iObj)
			PRINTLN("[CannotInteract] Not allowing interaction because this outfit has already been used. iObj: ", iobj)
			RETURN FALSE
		ENDIF
		
		IF MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(GET_CHANGE_CLOTHES_OUTFIT_FROM_SELECTION(iObj))
			PRINTLN("[CannotInteract] Not allowing interaction because local player is already wearing the chosen outfit. iObj: ", iobj)
			RETURN FALSE
		ENDIF
		
		IF GET_CHANGE_CLOTHES_OUTFIT_FROM_SELECTION(iObj) = OUTFIT_MP_FREEMODE
			PRINTLN("[CannotInteract] Not allowing interaction because no outfit has been chosen for this Casino Heist run. iObj: ", iobj)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_AllowMinigameOffRule)
	AND IS_BIT_SET(MC_serverBD_1.iOffRuleMinigameCompleted, iObj)
		PRINTLN("[CannotInteract] Off rule minigame ", iObj," has already been completed.")
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLinkedDoor1 != -1
		MODEL_NAMES mnDoorModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_vault_d_door_01a"))
		IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLinkedDoor1].mnDoorModel = mnDoorModel
			IF eCasinoDailyCashState != CASINO_DAILY_CASH_STATE__WAITING
				PRINTLN("[CannotInteract] Timed door 1 opening from obj ", iObj," is already running.")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLinkedDoor2 != -1
		MODEL_NAMES mnDoorModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_vault_d_door_01a"))
		IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLinkedDoor2].mnDoorModel = mnDoorModel
			IF eCasinoDailyCashState != CASINO_DAILY_CASH_STATE__WAITING
				PRINTLN("[CannotInteract] Timed door 2 opening from obj ", iObj," is already running.")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF sIWInfo.bInteractWith_ExtraDebug
		PRINTLN("[CannotInteract][InteractWith] Object CAN be interacted with! Made it to the bottom of PLAYER_CAN_INTERACT_WITH_OBJ on iObj: ", iobj)
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SET_IW_CAM_STATE(INTERACT_WITH_CAMERA_STATE eNewState)
	IF sIWInfo.eInteractWith_CameraState != eNewState
		sIWInfo.eInteractWith_CameraState = eNewState
		PRINTLN("[IWcam] SET_IW_CAM_STATE - Setting sIWInfo.eInteractWith_CameraState to ", sIWInfo.eInteractWith_CameraState)
	ENDIF
ENDPROC

PROC CLEANUP_INTERACT_WITH_CAMERA()
	IF DOES_CAM_EXIST(sIWInfo.camInteractWith_Camera)
		SET_CAM_ACTIVE(sIWInfo.camInteractWith_Camera, FALSE)
		DESTROY_CAM(sIWInfo.camInteractWith_Camera)
		RENDER_SCRIPT_CAMS(FALSE, TRUE, sIWInfo.sInteractWith_CameraVars.iLerpOut)
		PRINTLN("[IWcam] Cleaned up Interact with camera")
		SET_IW_CAM_STATE(IW_CAM_NONE)
	ENDIF
ENDPROC

PROC CLEANUP_INTERACT_WITH(INT iObj)
	
	PRINTLN("[InteractWith] CLEANUP_INTERACT_WITH | Cleaning up Interact With! iObj: ", iobj)
	
	IF iObj < 0
		iObj = 0
		PRINTLN("[InteractWith] CLEANUP_INTERACT_WITH | iObj was -1")
		ASSERTLN("[InteractWith] CLEANUP_INTERACT_WITH | iObj was -1")
	ENDIF
	
	IF IS_BIT_SET(sIWInfo.iInteractWith_Bitset, ciInteractWith_Bitset__BrokeOut)
		CLEAR_PED_TASKS(LocalPlayerPed)
	ENDIF
	
	sIWInfo.iInteractWith_Bitset = 0
	sIWInfo.iInteractWith_AudioBS = 0
	sIWInfo.eiInteractWith_CachedSceneAlignmentEntities[iObj] = NULL
	sIWInfo.eiInteractWith_CurrentSceneAlignmentEntity_AttachedEntity = NULL
	
	IF NOT DOES_INTERACT_WITH_HAVE_NO_ANIM(iObj)
		REMOVE_ANIM_DICT(sIWInfo.sInteractWith_Dictionary)
		REMOVE_ANIM_DICT(sIWInfo.sInteractWith_FailsafeDictionary)
	ELSE
		RESET_NET_TIMER(sIWInfo.stInteractWith_DownloadTimer[iObj])
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sIWInfo.sInteractWith_AudioBankName)
		RELEASE_NAMED_SCRIPT_AUDIO_BANK(sIWInfo.sInteractWith_AudioBankName)
	ENDIF
	
	RESET_NET_TIMER(sIWInfo.tdInteractWith_AnimationTimer)
	RESET_NET_TIMER(sIWInfo.stInteractWith_BailTimer)
	
	INT iSpawnedProp
	FOR iSpawnedProp = 0 TO ciInteractWith__Max_Spawned_Props - 1
		DELETE_INTERACT_WITH_SPAWNED_PROP(iSpawnedProp)
	ENDFOR
	
	IF sIWInfo.eInteractWith_CurrentState != IW_STATE_IDLE
		CLEANUP_INTERACT_WITH__EXTRA_BEHAVIOUR()
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__DOWNLOAD
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__DOWNLOAD_BAR_ONLY
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__HACK_LAPTOP_USB
		CLEANUP_INTERACT_WITH_DOWNLOAD_SCREEN()
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__CUT_OUT_PAINTING
		DELETE_BAG_OBJECT()
		APPLY_DUFFEL_BAG_HEIST_GEAR(localPlayerPed, iLocalPart)
	ENDIF		
	
	MC_playerBD[iLocalPart].iObjHacking = -1
	sIWInfo.oiInteractWith_CachedObj = NULL
	sIWInfo.iInteractWith_CachedIndex = -1
	SET_INTERACT_WITH_STATE(IW_STATE_IDLE)
	
	IF MC_playerBD_1[iLocalPart].iCurrentHoldInteract = iObj
		MC_playerBD_1[iLocalPart].iCurrentHoldInteract = -1
		PRINTLN("[InteractWith] CLEANUP_INTERACT_WITH | Clearing iCurrentHoldInteract")
	ENDIF
	
	CLEANUP_INTERACT_WITH_CAMERA()
ENDPROC

PROC ATTACH_INTERACT_WITH_SPAWNED_PROP_TO_HAND(INT iSpawnedProp)
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_playerBD_1[iLocalPart].niInteractWith_CurrentSpawnedProps[iSpawnedProp])
		INT iHandBone = GET_PED_BONE_INDEX(LocalPlayerPed, BONETAG_R_HAND)
		ENTITY_INDEX eiSpawnedProp = GET_INTERACT_WITH_SPAWNED_PROP(iSpawnedProp)
		
		IF DOES_ENTITY_EXIST(eiSpawnedProp)
		AND NOT IS_ENTITY_ATTACHED(eiSpawnedProp)
			ATTACH_ENTITY_TO_ENTITY(eiSpawnedProp, LocalPlayerPed, iHandBone, (<<0.15, 0.0, 0>>), (<<0,0,0>>))
			PRINTLN("[InteractWith] Attaching prop ", iSpawnedProp, " to hand")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_INTERACT_WITH_PROP_BE_INITIALLY_HIDDEN(INT iObj)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__LEFT
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__RIGHT
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_CASINO_TUNNEL_EXPLOSIVE
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__USE_KEYCARD
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SPAWN_AND_SET_UP_INTERACT_WITH_SPAWNED_PROP(STRING sDict, STRING sAnim, INT iObj, INT iSpawnedProp, BOOL bCreateProps = TRUE)
	
	UNUSED_PARAMETER(sDict) //Not actually unused
	UNUSED_PARAMETER(sAnim) //Not actually unused
	
	IF GET_INTERACT_WITH_PROP_MODEL_TO_SPAWN(iObj) = DUMMY_MODEL_FOR_SCRIPT
		RETURN TRUE
	ENDIF
	
	// Deleting old prop in this slot if it already exists
	IF bCreateProps
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_playerBD_1[iLocalPart].niInteractWith_CurrentSpawnedProps[iSpawnedProp])
			IF NETWORK_HAS_CONTROL_OF_ENTITY(GET_INTERACT_WITH_SPAWNED_PROP(iSpawnedProp))
				DELETE_INTERACT_WITH_SPAWNED_PROP(iSpawnedProp)
			ELSE
				NETWORK_REQUEST_CONTROL_OF_ENTITY(GET_INTERACT_WITH_SPAWNED_PROP(iSpawnedProp))
				PRINTLN("[InteractWith] Requesting control of prop!!")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	REQUEST_MODEL(GET_INTERACT_WITH_PROP_MODEL_TO_SPAWN(iObj))

	IF GET_INTERACT_WITH_PROP_MODEL_TO_SPAWN(iObj) != DUMMY_MODEL_FOR_SCRIPT
	AND HAS_MODEL_LOADED(GET_INTERACT_WITH_PROP_MODEL_TO_SPAWN(iObj))
		
		BOOL bReadyToSetUp = FALSE
		
		IF bCreateProps
			IF CAN_REGISTER_MISSION_OBJECTS(1)
				MC_RESERVE_NETWORK_MISSION_OBJECTS(1)
				
				IF CREATE_NET_OBJ(MC_playerBD_1[iLocalPart].niInteractWith_CurrentSpawnedProps[iSpawnedProp], GET_INTERACT_WITH_PROP_MODEL_TO_SPAWN(iObj), GET_ENTITY_COORDS(LocalPlayerPed), FALSE)
					bReadyToSetUp = TRUE
					MC_PlayerBD_1[iLocalPart].iInteractWith_NumSpawnedProps++
					
				ENDIF
			ENDIF
		ELSE
			bReadyToSetUp = TRUE
			
		ENDIF
		
		IF bReadyToSetUp
			ENTITY_INDEX eiSpawnedProp = GET_INTERACT_WITH_SPAWNED_PROP(iSpawnedProp)
			PRINTLN("[InteractWith] SPAWN_AND_SET_UP_INTERACT_WITH_SPAWNED_PROP | Spawned prop ", iSpawnedProp, " for Interact With has been created!")
			
			IF DOES_ENTITY_EXIST(eiSpawnedProp)
				SET_ENTITY_COMPLETELY_DISABLE_COLLISION(eiSpawnedProp, FALSE)
				SET_NETWORK_ID_CAN_MIGRATE(MC_playerBD_1[iLocalPart].niInteractWith_CurrentSpawnedProps[iSpawnedProp], FALSE)
				
				IF SHOULD_INTERACT_WITH_PROP_BE_INITIALLY_HIDDEN(iObj)
				AND bCreateProps
					SET_ENTITY_VISIBLE(eiSpawnedProp, FALSE)
				ENDIF
			ENDIF
			
			RETURN TRUE
		ELSE
			PRINTLN("[InteractWith] SPAWN_AND_SET_UP_INTERACT_WITH_SPAWNED_PROP | Prop isn't ready to set up!")
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_INTERACT_WITH_SPAWNED_PROP_INTERIOR_RETAIN(OBJECT_INDEX tempObj, OBJECT_INDEX oiSpawnedProp)
	INTERIOR_INSTANCE_INDEX iiInteriorAtSpawnedProp = GET_INTERIOR_FROM_ENTITY(tempObj)
	
	IF IS_VALID_INTERIOR(iiInteriorAtSpawnedProp)
	AND IS_INTERIOR_READY(iiInteriorAtSpawnedProp)
		INT iTargetRoomKey = GET_ROOM_KEY_FROM_ENTITY(tempObj)
		
		IF GET_INTERIOR_FROM_ENTITY(oiSpawnedProp) != iiInteriorAtSpawnedProp
		OR GET_ROOM_KEY_FROM_ENTITY(oiSpawnedProp) != iTargetRoomKey
			RETAIN_ENTITY_IN_INTERIOR(oiSpawnedProp, iiInteriorAtSpawnedProp)
			FORCE_ROOM_FOR_ENTITY(oiSpawnedProp, iiInteriorAtSpawnedProp, iTargetRoomKey)
			PRINTLN("[InteractWith] PROCESS_INTERACT_WITH_SPAWNED_PROP - Forcing spawned prop into interior: ", NATIVE_TO_INT(iiInteriorAtSpawnedProp), " / room: ", iTargetRoomKey)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_INTERACT_WITH_SPAWNED_PROP_TAGS(OBJECT_INDEX oiSpawnedProp)
	PROCESS_ANIMATED_PROP_TAGS(oiSpawnedProp)
ENDPROC

PROC PROCESS_INTERACT_WITH_SPAWNED_PROP(OBJECT_INDEX tempObj, INT iObj, INT iSpawnedProp)
	FLOAT fPhase
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_playerBD_1[iLocalPart].niInteractWith_CurrentSpawnedProps[iSpawnedProp])
		EXIT
	ENDIF
	
	OBJECT_INDEX oiSpawnedProp = GET_INTERACT_WITH_SPAWNED_PROP(iSpawnedProp)
	NETWORK_REQUEST_CONTROL_OF_ENTITY(oiSpawnedProp)
	
	IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(oiSpawnedProp)
		PRINTLN("[InteractWith] PROCESS_INTERACT_WITH_SPAWNED_PROP - I haven't got control of prop ", iSpawnedProp)
		EXIT
	ENDIF
	
	IF IS_SYNC_SCENE_RUNNING(MC_playerBD[iLocalPart].iSynchSceneID)
		fPhase = GET_SYNC_SCENE_PHASE(MC_playerBD[iLocalPart].iSynchSceneID)
	ELSE
		fPhase = 0.0
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__USE_KEYCARD
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__CUT_OUT_PAINTING
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__LEFT
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__RIGHT
		PROCESS_INTERACT_WITH_SPAWNED_PROP_INTERIOR_RETAIN(tempObj, oiSpawnedProp)
	ENDIF
	
	PROCESS_INTERACT_WITH_SPAWNED_PROP_TAGS(oiSpawnedProp)
	
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim
		CASE ciINTERACT_WITH_PRESET__SEARCH_BODY_ON_TABLE
			IF fPhase <= 0.55
				SET_ENTITY_VISIBLE(oiSpawnedProp, FALSE)
			ELSE
				SET_ENTITY_VISIBLE(oiSpawnedProp, TRUE)
			ENDIF
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__PLANT_RADIO_OVERRIDE
			IF fPhase >= 0.41
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_playerBD_1[iLocalPart].niInteractWith_CurrentSpawnedProps[iSpawnedProp])
					TRANSFER_SPAWNED_INTERACT_WITH_PROP_TO_PERSISTENT_PROP(tempObj, iObj, iSpawnedProp)
				ENDIF
			ELSE
				ATTACH_INTERACT_WITH_SPAWNED_PROP_TO_HAND(iSpawnedProp)
			ENDIF
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__USE_KEYCARD
			//ATTACH_INTERACT_WITH_SPAWNED_PROP_TO_HAND(iSpawnedProp)
		BREAK
		
	ENDSWITCH
ENDPROC

FUNC ENTITY_INDEX FIND_INTERACT_WITH_SCENE_ALIGNMENT_ENTITY(OBJECT_INDEX tempObj, INT iObj)
	
	MODEL_NAMES mnObjectModelName = GET_MODEL_FOR_INTERACT_WITH_SCENE_ALIGNMENT_ENTITY(iObj)
	ENTITY_INDEX eiEntity
	
	IF mnObjectModelName = GET_ENTITY_MODEL(tempObj)
		eiEntity = tempObj
		
	ELIF IS_MODEL_A_VEHICLE(mnObjectModelName)
		IF iInteractObject_AttachedVehicle[iObj] > -1
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iInteractObject_AttachedVehicle[iObj]])
			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iInteractObject_AttachedVehicle[iObj]])
				eiEntity = NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicle[iInteractObject_AttachedVehicle[iObj]])
			ENDIF
		ELSE
			PRINTLN("[InteractWith][ObjVehIndex] Looking for nearest ", GET_MODEL_NAME_FOR_DEBUG(mnObjectModelName))
			eiEntity = GET_CLOSEST_VEHICLE(GET_ENTITY_COORDS(tempObj), 255, mnObjectModelName, VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES)
		ENDIF
	ELSE
		eiEntity = GET_CLOSEST_OBJECT_OF_TYPE(GET_ENTITY_COORDS(tempObj), 40, mnObjectModelName, FALSE, FALSE, FALSE)
		
	ENDIF
	
	RETURN eiEntity
ENDFUNC

FUNC VECTOR SET_IW_OFFSET_ON_GROUND(VECTOR vCoords, VECTOR vOffset)

	FLOAT fGroundZ
	FLOAT fUpwardsOffset
	FLOAT fTargetZ
	
	IF GET_GROUND_Z_FOR_3D_COORD(vCoords, fGroundZ)
		fTargetZ = fGroundZ + 1.0
		
		fUpwardsOffset = (fTargetZ - vCoords.z)
		vOffset.z += fUpwardsOffset
		
		PRINTLN("[InteractWith] SET_IW_COORDS_ON_GROUND | vCoords: ", vCoords, " / fUpwardsOffset: ", fUpwardsOffset, " / fTargetZ: ", fTargetZ)
	ENDIF
	
	RETURN vOffset
ENDFUNC

FUNC VECTOR GET_INTERACT_WITH_SYNC_SCENE_ALIGN_OBJ_OFFSET(INT iobj)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_RADIO_OVERRIDE
		RETURN GET_ENTITY_FORWARD_VECTOR(sIWInfo.eiInteractWith_CachedSceneAlignmentEntities[iObj]) + <<0, 0, 1.0>>
	
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__CLOTHES
		FLOAT fForwardSize = -0.7
		VECTOR vAnimPos = SET_IW_OFFSET_ON_GROUND(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].vPos, GET_ENTITY_FORWARD_VECTOR(sIWInfo.eiInteractWith_CachedSceneAlignmentEntities[iObj]) * fForwardSize)
		vAnimPos.z -= 1.0
		RETURN vAnimPos
		
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__LEFT
	
		VECTOR vOffset = <<0,0,0>>
		VECTOR vVaultForward, vVaultRight, vVaultUp, vVaultPos
		GET_ENTITY_MATRIX(sIWInfo.eiInteractWith_CachedSceneAlignmentEntities[iObj], vVaultForward, vVaultRight, vVaultUp, vVaultPos)
		
		vOffset += vVaultForward * 0.0 //vRagPlacementVector.y
		vOffset += vVaultRight * 0.0 //vRagPlacementVector.x
		vOffset += vVaultUp * 0.0 //vRagPlacementVector.z

		RETURN vOffset
		
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__RIGHT
	
		VECTOR vOffset = <<0,0,0>>
		VECTOR vVaultForward, vVaultRight, vVaultUp, vVaultPos
		GET_ENTITY_MATRIX(sIWInfo.eiInteractWith_CachedSceneAlignmentEntities[iObj], vVaultForward, vVaultRight, vVaultUp, vVaultPos)
		
		vOffset += vVaultForward * 0.0
		vOffset += vVaultRight * 0.225
		vOffset += vVaultUp * 0.0


		RETURN vOffset
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PUSH_DAILY_CASH_BUTTON
		VECTOR vOffset = <<0,0,0>>
		VECTOR vForward, vRight, vUp, vPos
		GET_ENTITY_MATRIX(sIWInfo.eiInteractWith_CachedSceneAlignmentEntities[iObj], vForward, vRight, vUp, vPos)
		
		vOffset += vForward * -0.8
		vOffset.z -= 0.30
		
		vOffset -= vRight
		
		RETURN vOffset
		
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__HOLD_BUTTON
		VECTOR vOffset = <<0,0,0>>
		VECTOR vForward, vRight, vUp, vPos
		GET_ENTITY_MATRIX(sIWInfo.eiInteractWith_CachedSceneAlignmentEntities[iObj], vForward, vRight, vUp, vPos)
		
		vOffset += vForward * -0.05
		
		RETURN vOffset
	ENDIF
	
	RETURN <<0, 0, 0>>
ENDFUNC

FUNC VECTOR GET_INTERACT_WITH_SYNC_SCENE_COORDS(OBJECT_INDEX tempObj, INT iobj, STRING sDict, STRING sAnim)
	VECTOR vSceneCoords
	
	UNUSED_PARAMETER(sDict)
	UNUSED_PARAMETER(sAnim)
	UNUSED_PARAMETER(tempObj)
	
	IF GET_MODEL_FOR_INTERACT_WITH_SCENE_ALIGNMENT_ENTITY(iObj) != DUMMY_MODEL_FOR_SCRIPT
	
		IF DOES_ENTITY_EXIST(sIWInfo.eiInteractWith_CachedSceneAlignmentEntities[iObj])
			vSceneCoords = GET_ENTITY_COORDS(sIWInfo.eiInteractWith_CachedSceneAlignmentEntities[iObj], FALSE)
			vSceneCoords += GET_INTERACT_WITH_SYNC_SCENE_ALIGN_OBJ_OFFSET(iObj)
			
			#IF IS_DEBUG_BUILD
			IF sIWInfo.bInteractWith_ExtraDebug
				PRINTLN("[InteractWith] GET_INTERACT_WITH_SYNC_SCENE_COORDS | Obj ", iobj, " found an object with this model: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(sIWInfo.eiInteractWith_CachedSceneAlignmentEntities[iObj])))
			ENDIF
			#ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			PRINTLN("[InteractWith] GET_INTERACT_WITH_SYNC_SCENE_COORDS | Couldn't find object to use for sync scene! Model we were looking for was: ", GET_MODEL_NAME_FOR_DEBUG(GET_MODEL_FOR_INTERACT_WITH_SCENE_ALIGNMENT_ENTITY(iObj)))
			#ENDIF
		ENDIF
	ELSE	
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_CASINO_TUNNEL_EXPLOSIVE
			vSceneCoords = <<2497.708, -312.771, -71.47>>
			
		ELSE
			IF DOES_ENTITY_EXIST(LocalPlayerPed)
			AND NOT IS_PED_INJURED(LocalPlayerPed)
			
				vSceneCoords = GET_ENTITY_COORDS(LocalPlayerPed)
				
				FLOAT fGroundZ = 0
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionPed >= 0
				AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionPed])
					PED_INDEX piAttachedPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionPed])
					IF DOES_ENTITY_EXIST(piAttachedPed)
						vSceneCoords = GET_ENTITY_COORDS(piAttachedPed, FALSE)
					ELSE
						PRINTLN("[InteractWith] GET_INTERACT_WITH_SYNC_SCENE_COORDS | piAttachedPed doesn't exist!")
					ENDIF
				ELSE
					PRINTLN("[InteractWith] GET_INTERACT_WITH_SYNC_SCENE_COORDS | piAttachedPed doesn't exist or the iObjectInteractionPed setting is -1")
				ENDIF
				
				GET_GROUND_Z_FOR_3D_COORD(vSceneCoords, fGroundZ)
				vSceneCoords.z = fGroundZ
				
				PRINTLN("[InteractWith] GET_INTERACT_WITH_SYNC_SCENE_COORDS | ", vSceneCoords, " / ", fGroundZ, " / Dict: ", sDict, " / Anim: ", sAnim)
				
			ENDIF
		ENDIF
	ENDIF
	
	IF sIWInfo.bInteractWith_ExtraDebug
		PRINTLN("[InteractWith] GET_INTERACT_WITH_SYNC_SCENE_COORDS | Returning " , vSceneCoords)
	ENDIF
		
	RETURN vSceneCoords
ENDFUNC

FUNC VECTOR GET_INTERACT_WITH_SYNC_SCENE_ORIENTATION(OBJECT_INDEX tempObj, INT iobj, STRING sDict, STRING sAnim)
	VECTOR vSceneOrientation
	
	UNUSED_PARAMETER(sDict)
	UNUSED_PARAMETER(sAnim)
	
	IF GET_MODEL_FOR_INTERACT_WITH_SCENE_ALIGNMENT_ENTITY(iObj) != DUMMY_MODEL_FOR_SCRIPT
		IF DOES_ENTITY_EXIST(sIWInfo.eiInteractWith_CachedSceneAlignmentEntities[iObj])
			vSceneOrientation = GET_ENTITY_ROTATION(sIWInfo.eiInteractWith_CachedSceneAlignmentEntities[iObj])
			
			#IF IS_DEBUG_BUILD
			IF sIWInfo.bInteractWith_ExtraDebug
				PRINTLN("[InteractWith] GET_INTERACT_WITH_SYNC_SCENE_ORIENTATION | Found an object with this model: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(sIWInfo.eiInteractWith_CachedSceneAlignmentEntities[iObj])))
			ENDIF
			#ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			PRINTLN("[InteractWith] GET_INTERACT_WITH_SYNC_SCENE_ORIENTATION | Couldn't find object to use for sync scene! Model we were looking for was: ", GET_MODEL_NAME_FOR_DEBUG(GET_MODEL_FOR_INTERACT_WITH_SCENE_ALIGNMENT_ENTITY(iObj)))
			#ENDIF
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_RADIO_OVERRIDE
			VECTOR vVec = -GET_ENTITY_FORWARD_VECTOR(tempObj)
			FLOAT fHeading = GET_HEADING_FROM_VECTOR_2D(vVec.x, vVec.y)
			vSceneOrientation = <<0, 0, fHeading>>
			PRINTLN("[InteractWith_Spam] GET_INTERACT_WITH_SYNC_SCENE_ORIENTATION | ciINTERACT_WITH_PRESET__PLANT_RADIO_OVERRIDE | Setting vSceneOrientation to ", vSceneOrientation)
		
		ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__CLOTHES
			VECTOR vVec = GET_ENTITY_FORWARD_VECTOR(tempObj)
			FLOAT fHeading = GET_HEADING_FROM_VECTOR_2D(vVec.x, vVec.y)
			vSceneOrientation = <<0, 0, fHeading>>
			PRINTLN("[InteractWith_Spam] GET_INTERACT_WITH_SYNC_SCENE_ORIENTATION | ciINTERACT_WITH_PRESET__CLOTHES | Setting vSceneOrientation to ", vSceneOrientation)
			
		ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__LEFT
			vSceneOrientation -= <<0, 0, 90>>
			
		ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__RIGHT
			vSceneOrientation -= <<0, 0, 90>>
			
		ENDIF
	ELSE
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_CASINO_TUNNEL_EXPLOSIVE
			vSceneOrientation = <<0.0, 0.0, 0.0>> //<<2497.708, -312.771, -71.47>>
			
		ELSE
			vSceneOrientation = <<0, 0, GET_ENTITY_HEADING(LocalPlayerPed)>>
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionPed >= 0		
				PED_INDEX piAttachedPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionPed])
				vSceneOrientation = GET_ENTITY_ROTATION(piAttachedPed)
				
				vSceneOrientation.x = 0.0
				vSceneOrientation.y = 0.0
				
				IF NOT DOES_ENTITY_EXIST(piAttachedPed)
					PRINTLN("[InteractWith] GET_INTERACT_WITH_SYNC_SCENE_ORIENTATION | piAttachedPed doesn't exist!")
				ELSE
					PRINTLN("[interactWith] GET_INTERACT_WITH_SYNC_SCENE_ORIENTATION | piAttachedPed is ped ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionPed)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF sIWInfo.bInteractWith_ExtraDebug
		PRINTLN("[InteractWith] GET_INTERACT_WITH_SYNC_SCENE_ORIENTATION | Returning: ", vSceneOrientation)
	ENDIF
	
	RETURN vSceneOrientation
ENDFUNC

FUNC BOOL ADD_ALL_ENTITIES_TO_INTERACT_WITH_SYNC_SCENE(INT iObj, OBJECT_INDEX tempObj, STRING sDict, STRING sAnim, INT iMode = 0, FLOAT fBlendInRate = 3.0, FLOAT fBlendOutRate = REALLY_SLOW_BLEND_OUT)
	
	IF SET_UP_EXTRA_INTERACT_WITH_SYNC_SCENE_ENTITIES(iObj, sIWInfo.eiInteractWith_CachedSceneAlignmentEntities[iObj], tempObj, iMode)
		// Add entities to the sync scene
		NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE_WITH_IK(LocalPlayerPed,  MC_playerBD[iLocalPart].iSynchSceneID, sDict, sAnim, fBlendInRate, fBlendOutRate, GET_INTERACT_WITH_SYNC_SCENE_FLAGS(iObj), DEFAULT, fBlendInRate)
		
		INT iSpawnedProp
		FOR iSpawnedProp = 0 TO GET_INTERACT_WITH_NUM_PROPS_TO_SPAWN(iObj) - 1
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_playerBD_1[iLocalPart].niInteractWith_CurrentSpawnedProps[iSpawnedProp])
			
				OBJECT_INDEX oiSpawnedProp = NET_TO_OBJ(MC_playerBD_1[iLocalPart].niInteractWith_CurrentSpawnedProps[iSpawnedProp])
				FLOAT fPropBlendInRate = fBlendInRate
				FLOAT fPropBlendOutRate = fBlendOutRate
				
				IF IS_BIT_SET(sIWInfo.iInteractWith_Bitset, ciInteractWith_Bitset__SkippedTransition)
					fPropBlendInRate = REALLY_SLOW_BLEND_IN
				ENDIF
				
				IF IS_OBJECT_INTERACT_WITH_A_SYNC_SCENE_ANIM(iObj)
				AND NOT IS_STRING_NULL_OR_EMPTY(GET_ANIM_NAME_FOR_INTERACT_WITH_SPAWNED_PROP(iObj, iSpawnedProp, iMode))
					NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(oiSpawnedProp, MC_playerBD[iLocalPart].iSynchSceneID, sDict, GET_ANIM_NAME_FOR_INTERACT_WITH_SPAWNED_PROP(iObj, iSpawnedProp, iMode), fPropBlendInRate, fPropBlendOutRate, GET_INTERACT_WITH_SYNC_SCENE_FLAGS(iObj))
				ENDIF
			ENDIF
		ENDFOR
	ELSE
		PRINTLN("[InteractWith] SET_UP_INTERACT_WITH_SYNC_SCENE | ADD_ALL_ENTITIES_TO_INTERACT_WITH_SYNC_SCENE returned FALSE")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SET_UP_INTERACT_WITH_SYNC_SCENE(OBJECT_INDEX tempObj, INT iObj, STRING sDict, STRING sAnim, INT iMode = 0, BOOL bLooping = FALSE, FLOAT fStartPhase = 0.0, FLOAT fBlendInRate = SLOW_BLEND_IN, BOOL bHoldOnLastFrame = FALSE, FLOAT fBlendOutRate = SLOW_BLEND_OUT, BOOL bCreateProps = TRUE)

	BOOL bReady = TRUE
	VECTOR vSceneCoords = GET_INTERACT_WITH_SYNC_SCENE_COORDS(tempObj, iobj, sDict, sAnim)
	VECTOR vSceneOrientation = GET_INTERACT_WITH_SYNC_SCENE_ORIENTATION(tempObj, iobj, sDict, sAnim)
	
	IF IS_BIT_SET(sIWInfo.iInteractWith_Bitset, ciInteractWith_Bitset__SlowBlendIn)
		fBlendInRate = SLOW_BLEND_IN
	ENDIF
	
	IF IS_BIT_SET(sIWInfo.iInteractWith_Bitset, ciInteractWith_Bitset__SlowBlendOut)
		fBlendOutRate = SLOW_BLEND_OUT
	ENDIF
	
	IF IS_BIT_SET(sIWInfo.iInteractWith_Bitset, ciInteractWith_Bitset__SkippedTransition)
		fStartPhase = GET_INTERACT_WITH_SKIPPED_TRANSITION_START_PHASE(iObj)
	ENDIF
	
	INT iSpawnedProp
	FOR iSpawnedProp = 0 TO GET_INTERACT_WITH_NUM_PROPS_TO_SPAWN(iObj) - 1
		IF GET_INTERACT_WITH_PROP_MODEL_TO_SPAWN(iObj) != DUMMY_MODEL_FOR_SCRIPT
			IF NOT SPAWN_AND_SET_UP_INTERACT_WITH_SPAWNED_PROP(sDict, sAnim, iObj, iSpawnedProp, bCreateProps)
				PRINTLN("[InteractWith] SET_UP_INTERACT_WITH_SYNC_SCENE | Waiting for props...")
				bReady = FALSE
			ENDIF
		ENDIF
	ENDFOR
	
	IF bReady
		//If this sync scene already exists then delete it
		STOP_SYNC_SCENE(MC_playerBD[iLocalPart].iSynchSceneID)
		MC_playerBD[iLocalPart].iSynchSceneID = -1
	
		MC_playerBD[iLocalPart].iSynchSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(vSceneCoords, vSceneOrientation, DEFAULT, bHoldOnLastFrame, bLooping, DEFAULT, fStartPhase)
		PRINTLN("[InteractWith] SET_UP_INTERACT_WITH_SYNC_SCENE | Created sync scene at: ", vSceneCoords, " MC_playerBD[iLocalPart].iSynchSceneID: ", MC_playerBD[iLocalPart].iSynchSceneID)
		
		IF ADD_ALL_ENTITIES_TO_INTERACT_WITH_SYNC_SCENE(iObj, tempObj, sDict, sAnim, iMode, fBlendInRate, fBlendOutRate)
			// All good!
		ELSE
			bReady = FALSE
			PRINTLN("[InteractWith] SET_UP_INTERACT_WITH_SYNC_SCENE | ADD_ALL_ENTITIES_TO_INTERACT_WITH_SYNC_SCENE returned FALSE!")
		ENDIF
	ENDIF
	
	IF bReady
		CLEAR_BIT(sIWInfo.iInteractWith_Bitset, ciInteractWith_Bitset__SlowBlendIn)
		CLEAR_BIT(sIWInfo.iInteractWith_Bitset, ciInteractWith_Bitset__SlowBlendOut)
		CLEAR_BIT(sIWInfo.iInteractWith_Bitset, ciInteractWith_Bitset__SkippedTransition)
		
		PRINTLN("[InteractWith] SET_UP_INTERACT_WITH_SYNC_SCENE | Ready to play!")
	ENDIF
	
	RETURN bReady
ENDFUNC

PROC SET_DOOR_TO_UPDATE_OFF_RULE_FROM_OBJECT(INT iObj, BOOL bSet, INT iMethodUsed)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLinkedDoor1 > -1
		BROADCAST_FMMC_UPDATE_DOOR_OFF_RULE_EVENT(bSet, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLinkedDoor1, iMethodUsed)
	ENDIF

	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLinkedDoor2 > -1
		BROADCAST_FMMC_UPDATE_DOOR_OFF_RULE_EVENT(bSet, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLinkedDoor2, iMethodUsed)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_PASS_COMPLETED_INTERACT_WITH_MINIGAME(INT iObj)
	
	UNUSED_PARAMETER(iObj)
	
	IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_HAS_CUT_PAINTING_EXITED_EARLY)
		// [ML] For Interact With Paintings, don't mark as completed if coming here from a player instigated early exit
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__LEFT
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__RIGHT
		IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_COMPLETED_CASINO_VAULT_DOOR_BOMB_PLANT)
			CLEAR_BIT(iLocalBoolCheck32, LBOOL32_COMPLETED_CASINO_VAULT_DOOR_BOMB_PLANT)
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_INTERACT_WITH_COMPLETED(INT iObj)
	
	IF SHOULD_PASS_COMPLETED_INTERACT_WITH_MINIGAME(iObj)
		MC_playerBD[iLocalPart].iObjHacked = iObj
		INCREMENT_OFF_RULE_MINIGAME_SCORE_FROM_OBJECT(ciOffRuleScore_Low, iObj)
		PRINTLN("[InteractWith] Interaction complete on object ", iObj, ", setting iObjHacked now")
	ENDIF
	
	//Update/Unlock any linked doors when finishing.
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_HoldInteractWith)
	
		//Set the minigame as completed off rule if needed.
		IF SHOULD_PASS_COMPLETED_INTERACT_WITH_MINIGAME(iObj)
		AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetFour, cibsOBJ4_AllowMultipleInteractions)
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_AllowMinigameOffRule)
				BROADCAST_FMMC_UPDATE_MINIGAME_OFF_RULE_EVENT(TRUE, iObj)
			ENDIF
		ENDIF
		
		IF DOES_OBJECT_HAVE_LINKED_DOORS(iObj)
			SET_DOOR_TO_UPDATE_OFF_RULE_FROM_OBJECT(iObj, TRUE, ciUPDATE_DOOR_OFF_RULE_METHOD__KEYCARD)
		ENDIF
		
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__USE_KEYCARD
		IF NOT IS_BIT_SET(sLEGACYMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciLEGACY_ContinuityGenericTrackingBS_KeycardUsed)
			PRINTLN("[JS][CONTINUITY] - PROCESS_INTERACT_WITH_COMPLETED - Setting keycard as used")
			SET_BIT(sLEGACYMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciLEGACY_ContinuityGenericTrackingBS_KeycardUsed)
		ENDIF
	ENDIF
	
	IF NOT SHOULD_INTERACT_WITH_DELAY_CLEANUP_ON_COMPLETION(iObj)
		CLEANUP_INTERACT_WITH(iObj)
	ENDIF
ENDPROC

PROC PROCESS_INTERACT_WITH_SOUNDS(OBJECT_INDEX tempObj, INT iobj)
	
	FLOAT fPhase
	
	IF IS_SYNC_SCENE_RUNNING(MC_playerBD[iLocalPart].iSynchSceneID)
		fPhase = GET_SYNC_SCENE_PHASE(MC_playerBD[iLocalPart].iSynchSceneID)
	ELSE
		fPhase = 0.0
	ENDIF
	
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim
		CASE ciINTERACT_WITH_PRESET__POWER_BOX_CRANK
			
			IF fPhase >= 0.257
			AND NOT IS_BIT_SET(sIWInfo.iInteractWith_AudioBS, 0)
				sIWInfo.iInteractWith_AudioLoopID = GET_SOUND_ID()
				PLAY_SOUND_FROM_COORD(sIWInfo.iInteractWith_AudioLoopID, "generator_on_loop", GET_ENTITY_COORDS(tempObj), "dlc_xm_stealavg_sounds", TRUE, 100)
				SET_BIT(sIWInfo.iInteractWith_AudioBS, 0)
			ENDIF
			
			IF fPhase >= 0.463
			AND NOT IS_BIT_SET(sIWInfo.iInteractWith_AudioBS, 1)
				sIWInfo.iInteractWith_AudioLoopID = GET_SOUND_ID()
				PLAY_SOUND_FROM_COORD(-1, "handle_up", GET_ENTITY_COORDS(tempObj), "dlc_xm_stealavg_sounds", TRUE, 80)
				SET_BIT(sIWInfo.iInteractWith_AudioBS, 1)
			ENDIF
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__CLOTHES
			
			IF HAS_ANIM_EVENT_FIRED(LocalPlayerPed, HASH("CHANGE_CLOTHES"))
			AND NOT IS_BIT_SET(sIWInfo.iInteractWith_AudioBS, 0)
				MP_OUTFITS_APPLY_DATA   sApplyData
				sApplyData.pedID		= LocalPlayerPed
				sApplyData.eApplyStage 	= AOS_SET
				sApplyData.eOutfit		= GET_CHANGE_CLOTHES_OUTFIT_FROM_SELECTION(iObj)
				IF sApplyData.eOutfit != OUTFIT_MP_FREEMODE
					SET_PED_MP_OUTFIT(sApplyData, FALSE, FALSE, FALSE, FALSE)
					SET_BIT(sIWInfo.iInteractWith_AudioBS, 0)
					SET_BIT(iLocalBoolCheck31, LBOOL31_OUTFIT_CHANGED_THROUGH_INTERACT)
					MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(sApplyData.eOutfit)
					CLEAR_PED_BLOOD_DAMAGE(LocalPlayerPed)
					PRINTLN("[InteractWith] PROCESS_INTERACT_WITH_SOUNDS - Outfit changed! Updated outfit int to: ", MC_playerBD[iLocalPart].iOutfit)
					PRINTLN("[InteractWith] PROCESS_INTERACT_WITH_SOUNDS - g_FMMC_STRUCT.iGearDefault[MC_playerBD[iLocalPart].iteam]: ", g_FMMC_STRUCT.iGearDefault[MC_playerBD[iLocalPart].iteam])
					#IF IS_DEBUG_BUILD
					INT iPrintLoop
					FOR iPrintLoop = 0 TO MAX_HEIST_GEAR_BITSETS-1
						PRINTLN("[InteractWith] PROCESS_INTERACT_WITH_SOUNDS - g_FMMC_STRUCT.biGearAvailableBitset[MC_playerBD[iLocalPart].iteam][", iPrintLoop,"]: ", g_FMMC_STRUCT.biGearAvailableBitset[MC_playerBD[iLocalPart].iteam][iPrintLoop])
					ENDFOR
					#ENDIF
					IF g_FMMC_STRUCT.iGearDefault[MC_playerBD[iLocalPart].iteam] != -1
						IF IS_LONG_BIT_SET(g_FMMC_STRUCT.biGearAvailableBitset[MC_playerBD[iLocalPart].iteam], g_FMMC_STRUCT.iGearDefault[MC_playerBD[iLocalPart].iteam])
							sApplyOutfitData.eGear = INT_TO_ENUM(MP_HEIST_GEAR_ENUM, g_FMMC_STRUCT.iGearDefault[MC_playerBD[iLocalPart].iteam])
							SET_BAG_FOR_CASINO_HEIST()
							SET_MP_HEIST_GEAR(LocalPlayerPed, sApplyOutfitData.eGear)
							IF IS_HEIST_GEAR_A_BAG(sApplyOutfitData.eGear)
								SET_CACHED_HEIST_GEAR_BAG()
							ENDIF
							
							//Clear the enemy ped bits to update relationship groups again
							INT iBitLoop
							
							FOR iBitLoop = 0 TO FMMC_MAX_PEDS_BITSET - 1
								iPedSetupDisguiseOverride[iBitLoop] = 0
							ENDFOR
							
							PRINTLN("[InteractWith] PROCESS_INTERACT_WITH_SOUNDS - Reapplying default heist gear")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__USE_KEYCARD
			IF fPhase >= 0.257
			AND NOT IS_BIT_SET(sIWInfo.iInteractWith_AudioBS, 0)
				sIWInfo.iInteractWith_AudioLoopID = GET_SOUND_ID()
				PLAY_SOUND_FROM_COORD(sIWInfo.iInteractWith_AudioLoopID, "generator_on_loop", GET_ENTITY_COORDS(tempObj), "dlc_xm_stealavg_sounds", TRUE, 100) // PLACEHOLDER FOR KEYPAD SOUNDS url:bugstar:5967210
				SET_BIT(sIWInfo.iInteractWith_AudioBS, 0)
			ENDIF
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__HOLD_BUTTON
			IF sIWInfo.eInteractWith_CurrentState = IW_STATE_ANIMATING
				IF fPhase >= 0.85
				AND NOT IS_BIT_SET(sIWInfo.iInteractWith_AudioBS, 0)
					PLAY_SOUND_FROM_ENTITY(-1, "cash_room_door_button", tempObj, "dlc_ch_heist_finale_sounds", TRUE)
					SET_BIT(sIWInfo.iInteractWith_AudioBS, 0)
				ENDIF
			ENDIF
		BREAK
		CASE ciINTERACT_WITH_PRESET__PUSH_DAILY_CASH_BUTTON
			IF fPhase >= 0.257
			AND NOT IS_BIT_SET(sIWInfo.iInteractWith_AudioBS, 0)
				PLAY_SOUND_FROM_ENTITY(-1, "cash_room_door_button", tempObj, "dlc_ch_heist_finale_sounds", TRUE)
				SET_BIT(sIWInfo.iInteractWith_AudioBS, 0)
			ENDIF
		BREAK
		
	ENDSWITCH
	
ENDPROC

FUNC BOOL DOES_INTERACT_WITH_USE_A_CUSTOM_CAMERA(INT iObj)
	#IF IS_DEBUG_BUILD
	IF bDisableIWCam
		RETURN FALSE
	ENDIF
	#ENDIF
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__CLOTHES
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__CUT_OUT_PAINTING
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_INTERACT_WITH_CREATE_CAM_ON_ANIM_TAG(INT iObj)
	#IF IS_DEBUG_BUILD
	IF bDisableIWCam
		RETURN FALSE
	ENDIF
	#ENDIF
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__CLOTHES
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//For non sync scenes that need particular timings
FUNC BOOL SHOULD_IW_USE_INTERACTION_TIMER_FOR_CAMERA(INT iObj)

	UNUSED_PARAMETER(iObj)
	
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_POS_FOR_IW_CAMERA(BOOL bSyncScene, VECTOR vTargetPos, VECTOR vTargetRot, BOOL bUseInteractWithPos = FALSE)
	VECTOR vForward, vRight, vUp, vPos
	
	IF bSyncScene
		vPos = vTargetPos
		vUp = <<0,0,1>>
		vForward = ROTATE_VECTOR_ABOUT_Z(<<0, 1, 0>>, vTargetRot.z)
		vRight = CROSS_PRODUCT(vForward, vUp)
	ELSE
		GET_ENTITY_MATRIX(LocalPlayerPed, vForward, vRight, vUp, vPos)
		IF bUseInteractWithPos	// [ML] Using this if the player moves forward from the interact_with target position. This can knock off the position of subsequent cameras
			PRINTLN("[ML]GET_POS_FOR_IW_CAMERA - bUseInteractWithPos = TRUE, so setting vPos = sIWInfo.vInteractWith_TargetPos")
			vPos = sIWInfo.vInteractWith_TargetPos
		ENDIF
	ENDIF
	
	VECTOR vLocalOffset = sIWInfo.sInteractWith_CameraVars.vOffsetCoords
	VECTOR vResult
	
	#IF IS_DEBUG_BUILD
	IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
		DRAW_DEBUG_LINE(vPos, vPos + vForward, 0, 0, 0)
		DRAW_DEBUG_LINE(vPos, vPos + vRight, 255, 0, 0)
		DRAW_DEBUG_LINE(vPos, vPos + vUp, 0, 255, 0)
	ENDIF
	#ENDIF
	
	vResult = vPos
	vResult += vRight * vLocalOffset.x
	vResult += vForward * vLocalOffset.y
	vResult += vUp * vLocalOffset.z
	
	PRINTLN("GET_POS_FOR_IW_CAMERA || vLocalOffset = ", vLocalOffset)
	
	RETURN vResult
ENDFUNC

FUNC VECTOR GET_ROT_FOR_IW_CAMERA(BOOL bSyncScene, VECTOR vTargetRot)
	VECTOR vReturn = sIWInfo.sInteractWith_CameraVars.vRot
	FLOAT fHeadingToUse
	
	IF bSyncScene
		fHeadingToUse = vTargetRot.z	
	ELSE
		fHeadingToUse = GET_ENTITY_HEADING(localPlayerPed)
	ENDIF
	
	vReturn.z += fHeadingToUse
	RETURN vReturn
ENDFUNC

FUNC BOOL HAS_IW_CAM_BEEN_OVERRIDDEN()
	#IF IS_DEBUG_BUILD
	IF NOT IS_VECTOR_ZERO(vIWCamOffsetOverride)
	OR NOT IS_VECTOR_ZERO(vIWCamRotOverride)
		RETURN TRUE
	ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_CHANGE_CLOTHES_CAMERA_VARS(INTERACT_WITH_CAMERA_VARS& sCamVars, INT iAnimIndex)
	SWITCH iAnimIndex
		CASE ciChangeClothesNOOSE
			sCamVars.vOffsetCoords = <<0.0, 0.0, -1.0>>
			sCamVars.vRot = <<0.0, 0.0, 0.0>>
			sCamVars.sCamAnimName = "CHANGE_NOOSE_CAM"
		BREAK
		CASE ciChangeClothesFireman
			sCamVars.vOffsetCoords = <<0.0, 0.0, -1.0>>
			sCamVars.vRot = <<0.0, 0.0, 0.0>>
			sCamVars.sCamAnimName = "CHANGE_FIRE_CAM"
		BREAK
		CASE ciChangeClothesHighroller
			sCamVars.vOffsetCoords = <<0.0, 0.0, -1.0>>
			sCamVars.vRot = <<0.0, 0.0, 0.0>>
			sCamVars.sCamAnimName = "CHANGE_HIGHROLLER_CAM"
		BREAK
	ENDSWITCH
ENDPROC

PROC SET_CUT_PAINTING_CAMERA_VARS(INTERACT_WITH_CAMERA_VARS& sCamVars, INT iCameraAnimationToUse)
	sCamVars.vOffsetCoords = <<0.0, 0.7, -1.0>>
	sCamVars.vRot = <<0.0, 0.0, 0.0>>
	sCamVars.sCamAnimName = GET_ANIM_NAME_FOR_PAINTING_CUT_CAMERA(iCameraAnimationToUse)
ENDPROC

PROC SET_IW_CAMERA_VARS(INT iObj, INTERACT_WITH_CAMERA_VARS& sCamVars)
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim
		CASE ciINTERACT_WITH_PRESET__CLOTHES
			sCamVars.fFOV = 44.6382
			sCamVars.fPhaseEnd = 0.95
			sCamVars.iLerpIn = 0
			sCamVars.iLerpOut = 0
			sCamVars.bIsAnimated = TRUE
			sCamVars.sCamAnimDict = "ANIM_HEIST@HS3F@IG12_CHANGE_CLOTHES@"
			SET_CHANGE_CLOTHES_CAMERA_VARS(sCamVars, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iClothesChangeOutfit)
		BREAK
		CASE ciINTERACT_WITH_PRESET__CUT_OUT_PAINTING
			sCamVars.fFOV = 28.0
			sCamVars.fPhaseEnd = 1.0
			sCamVars.iLerpIn = 0
			sCamVars.iLerpOut = 0
			sCamVars.bIsAnimated = TRUE
			sCamVars.sCamAnimDict = "ANIM_HEIST@HS3F@IG11_STEAL_PAINTING@MALE@"
			SET_CUT_PAINTING_CAMERA_VARS(sCamVars, iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex])
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	IF NOT IS_VECTOR_ZERO(vIWCamOffsetOverride)
		sCamVars.vOffsetCoords = vIWCamOffsetOverride
	ENDIF
	IF NOT IS_VECTOR_ZERO(vIWCamRotOverride)
		sCamVars.vRot = vIWCamRotOverride
	ENDIF
	#ENDIF
	
	PRINTLN("[IWcam] SET_IW_CAMERA_VARS - Setting up cam for Object ", iObj)
	PRINTLN("[IWcam] SET_IW_CAMERA_VARS - sCamVars.vRot: ", sCamVars.vRot)
	PRINTLN("[IWcam] SET_IW_CAMERA_VARS - sCamVars.vOffsetCoords: ", sCamVars.vOffsetCoords)
	PRINTLN("[IWcam] SET_IW_CAMERA_VARS - sCamVars.fPhaseEnd: ", sCamVars.fPhaseEnd, " sCamVars.fFOV: ", sCamVars.fFOV)
	PRINTLN("[IWcam] SET_IW_CAMERA_VARS - sCamVars.iDuration: ", sCamVars.iDuration)
	PRINTLN("[IWcam] SET_IW_CAMERA_VARS - sCamVars.iLerpIn: ", sCamVars.iLerpIn, " sCamVars.iLerpOut: ", sCamVars.iLerpOut)
ENDPROC

FUNC BOOL SHOULD_IW_CAM_MOVE_FROM_HOLD(INT iObj, FLOAT fPhase)

	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__CUT_OUT_PAINTING
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex = -1
			PRINTLN("[ML] iPaintingIndex = ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex, " for object ", iObj)
			CASSERTLN( DEBUG_CONTROLLER, "SHOULD_IW_CAM_MOVE_FROM_HOLD - Painting index set to OFF on for object: ", iObj)
			RETURN FALSE
		ELSE
			IF sIWInfo.eInteractWith_CurrentState != IW_STATE_EXIT_ANIMATION
				PRINTLN("[ML][SHOULD_IW_CAM_MOVE_FROM_HOLD] Returning False as sIWInfo.eInteractWith_CurrentState != IW_STATE_EXIT_ANIMATION")
				RETURN FALSE
			ENDIF	
		ENDIF
	ENDIF

	IF fPhase > sIWInfo.sInteractWith_CameraVars.fPhaseEnd
		PRINTLN("[IWcam] SHOULD_IW_CAM_MOVE_FROM_HOLD - Phase has passed.")
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SET_IW_CAMERA_SHAKE(INT iObj, CAMERA_INDEX camCamera)	
	INT iInteractWithType = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim
	SWITCH iInteractWithType
		CASE ciINTERACT_WITH_PRESET__CUT_OUT_PAINTING
			SHAKE_CAM(camCamera, "HAND_SHAKE", 0.5)
		BREAK
	ENDSWITCH
ENDPROC

PROC PROCESS_INTERACT_WITH_CAMERAS(INT iObj, VECTOR vTargetPos, VECTOR vTargetRot)
	IF NOT DOES_INTERACT_WITH_USE_A_CUSTOM_CAMERA(iObj)
		EXIT
	ENDIF
	
	IF sIWInfo.eInteractWith_CurrentState < IW_STATE_ANIMATING
		EXIT
	ENDIF
	
	FLOAT fPhase
	IF IS_SYNC_SCENE_RUNNING(MC_playerBD[iLocalPart].iSynchSceneID)
		fPhase = GET_SYNC_SCENE_PHASE(MC_playerBD[iLocalPart].iSynchSceneID)
	ELSE
		fPhase = 0.0
		IF SHOULD_IW_USE_INTERACTION_TIMER_FOR_CAMERA(iObj)
			IF HAS_NET_TIMER_STARTED(sIWInfo.tdInteractWith_AnimationTimer)
			AND sIWInfo.sInteractWith_CameraVars.iDuration != 0
				INT iInteractWithTime
				iInteractWithTime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sIWInfo.tdInteractWith_AnimationTimer)
				fPhase = TO_FLOAT(iInteractWithTime)/sIWInfo.sInteractWith_CameraVars.iDuration
			ENDIF
		ENDIF
	ENDIF
	
	SWITCH sIWInfo.eInteractWith_CameraState
		CASE IW_CAM_NONE
		BREAK
		CASE IW_CAM_CREATE
			
			sIWInfo.sInteractWith_CameraVars.bProcess = TRUE
			
			IF DOES_INTERACT_WITH_CREATE_CAM_ON_ANIM_TAG(iObj)
			AND NOT sIWInfo.sInteractWith_CameraVars.bStartTagHit
				sIWInfo.sInteractWith_CameraVars.bProcess = HAS_ANIM_EVENT_FIRED(LocalPlayerPed, HASH("START_CAM"))
				IF sIWInfo.sInteractWith_CameraVars.bProcess
					PRINTLN("[IWcam] PROCESS_INTERACT_WITH_CAMERAS - DOES_INTERACT_WITH_CREATE_CAM_ON_ANIM_TAG - bProcess: ", BOOL_TO_STRING(sIWInfo.sInteractWith_CameraVars.bProcess))
					sIWInfo.sInteractWith_CameraVars.bStartTagHit = TRUE
				ENDIF
				
			ENDIF
			
			IF sIWInfo.sInteractWith_CameraVars.bProcess
				SET_IW_CAMERA_VARS(iObj, sIWInfo.sInteractWith_CameraVars)
				IF NOT DOES_CAM_EXIST(sIWInfo.camInteractWith_Camera)
					PRINTLN("[IWcam] Final Pos to use: ", VECTOR_TO_STRING(GET_POS_FOR_IW_CAMERA(IS_OBJECT_INTERACT_WITH_A_SYNC_SCENE_ANIM(iObj), vTargetPos, vTargetRot)))
					STRING sCam
					sCam = "DEFAULT_SCRIPTED_CAMERA"
					IF sIWInfo.sInteractWith_CameraVars.bIsAnimated
						sCam = "DEFAULT_ANIMATED_CAMERA"
					ENDIF
					
					IF sIWInfo.sInteractWith_CameraVars.bIsAnimated
						sIWInfo.camInteractWith_Camera = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
						PLAY_CAM_ANIM(sIWInfo.camInteractWith_Camera, sIWInfo.sInteractWith_CameraVars.sCamAnimName, sIWInfo.sInteractWith_CameraVars.sCamAnimDict, GET_POS_FOR_IW_CAMERA(IS_OBJECT_INTERACT_WITH_A_SYNC_SCENE_ANIM(iObj), vTargetPos, vTargetRot), GET_ROT_FOR_IW_CAMERA(IS_OBJECT_INTERACT_WITH_A_SYNC_SCENE_ANIM(iObj), vTargetRot))
					ELSE
						sIWInfo.camInteractWith_Camera = CREATE_CAM_WITH_PARAMS(sCam, GET_POS_FOR_IW_CAMERA(IS_OBJECT_INTERACT_WITH_A_SYNC_SCENE_ANIM(iObj), vTargetPos, vTargetRot), GET_ROT_FOR_IW_CAMERA(IS_OBJECT_INTERACT_WITH_A_SYNC_SCENE_ANIM(iObj), vTargetRot), sIWInfo.sInteractWith_CameraVars.fFOV, TRUE)
					ENDIF
					
					RENDER_SCRIPT_CAMS(TRUE, TRUE, sIWInfo.sInteractWith_CameraVars.iLerpIn)
					PRINTLN("[IWcam] Camera created and rendering")
				ELSE
					SET_IW_CAMERA_SHAKE(iObj, sIWInfo.camInteractWith_Camera)
				
					SET_IW_CAM_STATE(IW_CAM_HOLD) 
				ENDIF
			ENDIF
		BREAK
		CASE IW_CAM_HOLD
			
			#IF IS_DEBUG_BUILD
			IF HAS_IW_CAM_BEEN_OVERRIDDEN()
				SET_IW_CAMERA_VARS(iObj, sIWInfo.sInteractWith_CameraVars)
				IF NOT IS_VECTOR_ZERO(vIWCamOffsetOverride)
					SET_CAM_COORD(sIWInfo.camInteractWith_Camera, GET_POS_FOR_IW_CAMERA(IS_OBJECT_INTERACT_WITH_A_SYNC_SCENE_ANIM(iObj), vTargetPos, vTargetRot))
				ENDIF
				IF NOT IS_VECTOR_ZERO(vIWCamRotOverride)
					SET_CAM_ROT(sIWInfo.camInteractWith_Camera, GET_ROT_FOR_IW_CAMERA(IS_OBJECT_INTERACT_WITH_A_SYNC_SCENE_ANIM(iObj), vTargetRot))
				ENDIF
				PRINTLN("PROCESS_INTERACT_WITH_CAMERAS - vIWCamOffsetOverride: ", vIWCamOffsetOverride, " vIWCamRotOverride: ", vIWCamRotOverride, " HAS_IW_CAM_BEEN_OVERRIDDEN(): ", HAS_IW_CAM_BEEN_OVERRIDDEN())
			ENDIF
			#ENDIF
			
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			//SET_CAM_FOV(sIWInfo.camInteractWith_Camera, 44)
			IF SHOULD_IW_CAM_MOVE_FROM_HOLD(iObj, fPhase)
			AND NOT HAS_IW_CAM_BEEN_OVERRIDDEN()
				SET_IW_CAM_STATE(IW_CAM_CLEAN_UP)
			ENDIF
		BREAK
		CASE IW_CAM_CLEAN_UP
			
			CLEANUP_INTERACT_WITH_CAMERA()
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC BOOL SHOULD_SKIP_INTERACT_WITH_SEQUENCE(OBJECT_INDEX tempObj, INT iobj, VECTOR vExpectedPos, VECTOR vExpectedRot, BOOL bOccupied)
	
	UNUSED_PARAMETER(tempObj)
	UNUSED_PARAMETER(vExpectedPos)
	
	IF IS_INTERACT_WITH_INSTANT(iObj)
		RETURN TRUE
	ENDIF
	
	FLOAT fAllowedRotation = 20
	
	IF ABSF(vExpectedRot.x) > fAllowedRotation
	OR ABSF(vExpectedRot.y) > fAllowedRotation
		PRINTLN("[InteractWith] SHOULD_SKIP_INTERACT_WITH_SEQUENCE returning TRUE for obj ", iobj, " due to vExpectedRot being ", vExpectedRot, " , Allowed rotation : ", fAllowedRotation)
		RETURN TRUE
	ENDIF
	
	IF bOccupied
		PRINTLN("[InteractWith] SHOULD_SKIP_INTERACT_WITH_SEQUENCE returning TRUE for obj ", iobj, " due to IS_POSITION_OCCUPIED ")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL AM_I_CLOSER_TO_INTERACTION_THAN_MY_TARGETPOS_IS(OBJECT_INDEX tempObj, VECTOR vTargetPos)
	FLOAT fDistToObj = 0
	FLOAT fDistFromPosToObj = 0
	
	VECTOR vObjCoords = GET_ENTITY_COORDS(tempObj)
	
	fDistToObj = VDIST2(vObjCoords, GET_ENTITY_COORDS(LocalPlayerPed))
	fDistFromPosToObj = VDIST2(vObjCoords, vTargetPos)
	
	RETURN (fDistToObj < fDistFromPosToObj)
ENDFUNC

FUNC BOOL HAS_INTERACT_WITH_BUTTON_BEEN_PRESSED(INT iObj)

	IF DOES_INTERACT_WITH_REQUIRE_PHONE_ACTIVATION(iObj)
		RETURN IS_CALLING_CONTACT(CHAR_MP_DETONATEPHONE)
	ENDIF

	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_HoldInteractWith)
		RETURN IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
	ELSE
		RETURN IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
	ENDIF
ENDFUNC

FUNC BOOL IS_INTERACT_WITH_BUTTON_HELD(INT iObj)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_HoldInteractWith)
		RETURN IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_PAINTING_CUT_STATE_MACHINE_STATE(INT iObj)
	SWITCH ecpsAnimationState[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex]
		CASE CP_STATE_WAITING_ON_INPUT
			RETURN "CP_STATE_WAITING_ON_INPUT"
		BREAK
		CASE CP_STATE_ANIMATING_CUT
			RETURN "CP_STATE_ANIMATING_CUT"
		BREAK
		CASE CP_STATE_EARLY_EXIT
			RETURN "CP_STATE_EARLY_EXIT"
		BREAK
		CASE CP_STATE_ROLL_UP_PAINTING
			RETURN "CP_STATE_ROLL_UP_PAINTING"
		BREAK
	ENDSWITCH
	RETURN "NULL"
ENDFUNC

PROC SET_CUT_PAINTING_STATE(INT iObj, CUT_PAINTING_STATE newState)

	IF ecpsAnimationState[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex] != newState
		ecpsAnimationState[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex] = newState													
		PRINTLN("[InteractWith] ecpsAnimationState is now ", GET_PAINTING_CUT_STATE_MACHINE_STATE(iObj), " and we are interacting with painting index: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex)	
	ENDIF
	
ENDPROC

FUNC STRING GET_CUT_PAINTING_CUT_PLAYER_ANIM_NAME(INT iObj, INT iLocalPaintingState)
	
	UNUSED_PARAMETER(iObj)
	PRINTLN("[InteractWith] GET_CUT_PAINTING_CUT_PLAYER_ANIM_NAME State is : ", iLocalPaintingState, " for painting index: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex)
	
	IF iPaintingBeingCutAnimVariation = 0 
		SWITCH iLocalPaintingState
			CASE ciCutPaintingTopLeft
				iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex] = ciInteractWith_Painting_CutTopLeftTopRight
				RETURN "VER_01_CUTTING_TOP_LEFT_TO_RIGHT"
			CASE ciCutPaintingTopRight
				iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex] = ciInteractWith_Painting_CutTopRightBottomRight
				RETURN "VER_01_CUTTING_RIGHT_TOP_TO_BOTTOM"
			CASE ciCutPaintingBottomRight
				iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex] = ciInteractWith_Painting_CutBottomRightBottomLeft
				RETURN "VER_01_CUTTING_BOTTOM_RIGHT_TO_LEFT"
			CASE ciCutPaintingBottomLeft
				iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex] = ciInteractWith_Painting_CutBottomLeftTopLeft	
				RETURN "VER_01_CUTTING_LEFT_TOP_TO_BOTTOM"
			CASE ciCutPaintingComplete
				iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex] = ciInteractWith_Painting_ExitRollUp
				RETURN "VER_01_WITH_PAINTING_EXIT"
		ENDSWITCH
	ELIF iPaintingBeingCutAnimVariation = 1
		SWITCH iLocalPaintingState
			CASE ciCutPaintingTopLeft
				iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex] = ciInteractWith_Painting_V2CutTopLeftTopRight
				RETURN "VER_02_CUTTING_TOP_LEFT_TO_RIGHT"
			CASE ciCutPaintingTopRight
				iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex] = ciInteractWith_Painting_V2CutTopRightBottomRight
				RETURN "VER_02_CUTTING_RIGHT_TOP_TO_BOTTOM"
			CASE ciCutPaintingBottomRight
				iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex] = ciInteractWith_Painting_V2CutBottomRightBottomLeft
				RETURN "VER_02_CUTTING_BOTTOM_RIGHT_TO_LEFT"
			CASE ciCutPaintingBottomLeft
				iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex] = ciInteractWith_Painting_V2CutBottomLeftTopLeft
				RETURN "VER_02_CUTTING_LEFT_TOP_TO_BOTTOM"
			CASE ciCutPaintingComplete
				iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex] = ciInteractWith_Painting_V2ExitRollUp
				RETURN "VER_02_WITH_PAINTING_EXIT"
		ENDSWITCH
	ELSE
		PRINTLN("[GET_CUT_PAINTING_CUT_PLAYER_ANIM_NAME][ML] Illegal anim variation")
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC STRING GET_CUT_PAINTING_IDLE_PLAYER_ANIM_NAME(INT iObj, INT iLocalPaintingState)

	UNUSED_PARAMETER(iObj)
	PRINTLN("[InteractWith] GET_CUT_PAINTING_IDLE_PLAYER_ANIM_NAME State is : ", iLocalPaintingState, " for painting index: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex)

	IF iPaintingBeingCutAnimVariation = 0
		SWITCH iLocalPaintingState
			CASE ciCutPaintingTopLeft
				iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex] = ciInteractWith_Painting_IdleTopLeft
				RETURN "VER_01_CUTTING_TOP_LEFT_IDLE"
			CASE ciCutPaintingTopRight
				iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex] = ciInteractWith_Painting_IdleTopRight
				RETURN "VER_01_CUTTING_TOP_RIGHT_IDLE"
			CASE ciCutPaintingBottomRight
				iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex] = ciInteractWith_Painting_IdleBottomRight
				RETURN "VER_01_CUTTING_BOTTOM_RIGHT_IDLE"
			CASE ciCutPaintingBottomLeft
				iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex] = ciInteractWith_Painting_IdleBottomLeft
				RETURN "VER_01_CUTTING_TOP_LEFT_IDLE"
		ENDSWITCH
	ELIF iPaintingBeingCutAnimVariation = 1
		SWITCH iLocalPaintingState
			CASE ciCutPaintingTopLeft
				iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex] = ciInteractWith_Painting_V2IdleTopLeft
				RETURN "VER_02_CUTTING_TOP_LEFT_IDLE"
			CASE ciCutPaintingTopRight
				iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex] = ciInteractWith_Painting_V2IdleTopRight
				RETURN  "VER_02_CUTTING_TOP_RIGHT_IDLE"
			CASE ciCutPaintingBottomRight
				iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex] = ciInteractWith_Painting_V2IdleBottomRight
				RETURN "VER_02_CUTTING_BOTTOM_RIGHT_IDLE"
			CASE ciCutPaintingBottomLeft
				iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex] = ciInteractWith_Painting_V2IdleBottomLeft
				RETURN "VER_02_CUTTING_TOP_LEFT_IDLE"
		ENDSWITCH
	ELSE
		PRINTLN("[GET_CUT_PAINTING_IDLE_PLAYER_ANIM_NAME][ML] Illegal anim variation")
	ENDIF
	RETURN ""
ENDFUNC

FUNC STRING GET_CUT_PAINTING_EARLY_EXIT_PLAYER_ANIM_NAME(INT iObj, INT iLocalPaintingState)

	UNUSED_PARAMETER(iObj)
	PRINTLN("[InteractWith] GET_CUT_PAINTING_EARLY_EXIT_PLAYER_ANIM_NAME State is : ", iLocalPaintingState, " for painting index: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex)
	
	IF iPaintingBeingCutAnimVariation = 0 
		SWITCH iLocalPaintingState
			CASE ciCutPaintingTopLeft
				iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex] = ciInteractWith_Painting_ExitTopLeft
				RETURN "VER_01_TOP_LEFT_EXIT"
			CASE ciCutPaintingTopRight
				iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex] = ciInteractWith_Painting_ExitTopRight
				RETURN "VER_01_TOP_RIGHT_EXIT"
			CASE ciCutPaintingBottomRight
				iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex] = ciInteractWith_Painting_ExitBottomRight
				RETURN "VER_01_BOTTOM_RIGHT_EXIT"
			CASE ciCutPaintingBottomLeft
				iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex] = ciInteractWith_Painting_ExitBottomLeft
				RETURN "VER_01_TOP_LEFT_EXIT"
		ENDSWITCH
	ELIF iPaintingBeingCutAnimVariation = 1 
		SWITCH iLocalPaintingState
			CASE ciCutPaintingTopLeft
				iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex] = ciInteractWith_Painting_V2ExitTopLeft
				RETURN "VER_02_TOP_LEFT_EXIT"
			CASE ciCutPaintingTopRight
				iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex] = ciInteractWith_Painting_V2ExitTopRight
				RETURN "VER_02_TOP_RIGHT_EXIT"
			CASE ciCutPaintingBottomRight
				iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex] = ciInteractWith_Painting_V2ExitBottomRight
				RETURN "VER_02_BOTTOM_RIGHT_EXIT"
			CASE ciCutPaintingBottomLeft
				iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex] = ciInteractWith_Painting_V2ExitBottomLeft
				RETURN "VER_02_TOP_LEFT_EXIT"
		ENDSWITCH
	ELSE
		PRINTLN("[GET_CUT_PAINTING_EARLY_EXIT_PLAYER_ANIM_NAME][ML] Illegal anim variation")
	ENDIF	
	
	RETURN ""
ENDFUNC

FUNC STRING GET_CUT_PAINTING_EXIT_ROLL_UP_ANIM_NAME(INT iObj)

	UNUSED_PARAMETER(iObj)
	
	IF iPaintingBeingCutAnimVariation = 0 
		iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex] = ciInteractWith_Painting_ExitRollUp
		RETURN "ver_01_with_painting_exit"
	ELIF iPaintingBeingCutAnimVariation = 1
		iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex] = ciInteractWith_Painting_V2ExitRollUp
		RETURN "ver_02_with_painting_exit"
	ELSE
		PRINTLN("[GET_CUT_PAINTING_EXIT_ROLL_UP_ANIM_NAME][ML] Illegal anim variation")
	ENDIF	
	
	RETURN ""
ENDFUNC

FUNC STRING GET_CUT_PAINTING_PAINTING_IDLE_ANIM_NAME(INT iObj)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex = -1
		RETURN ""
	ENDIF

	SWITCH MC_serverBD_1.iCurrentCutPaintingState[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex]
		CASE ciCutPaintingTopLeft		RETURN ""
		CASE ciCutPaintingTopRight		RETURN PICK_STRING(iPaintingBeingCutAnimVariation = 0, "VER_01_TOP_RIGHT_EXIT_ch_Prop_vault_painting_01a",			"VER_02_TOP_RIGHT_EXIT_ch_Prop_vault_painting_01a")
		CASE ciCutPaintingBottomRight	RETURN PICK_STRING(iPaintingBeingCutAnimVariation = 0, "VER_01_BOTTOM_RIGHT_EXIT_ch_Prop_vault_painting_01a", 		"VER_02_BOTTOM_RIGHT_ENTER_PAINTING_ch_Prop_vault_painting_01a")
		CASE ciCutPaintingBottomLeft	RETURN PICK_STRING(iPaintingBeingCutAnimVariation = 0, "VER_01_BOTTOM_LEFT_EXIT_ch_Prop_vault_painting_01a", 		"VER_02_TOP_LEFT_RE-ENTER_ch_Prop_vault_painting_01a")
		CASE ciCutPaintingComplete		RETURN PICK_STRING(iPaintingBeingCutAnimVariation = 0, "VER_01_WITH_PAINTING_REMOVED_ch_Prop_vault_painting_01a",	"VER_02_WITH_PAINTING_REMOVED_ch_Prop_vault_painting_01a")
	ENDSWITCH

	RETURN ""
	
ENDFUNC

FUNC STRING GET_CUT_PAINTING_HELPTEXT(INT iObj, INT iLocalPaintingState)

	UNUSED_PARAMETER(iObj)
	PRINTLN("[InteractWith] GET_CUT_PAINTING_HELPTEXT State is : ", iLocalPaintingState, " for painting index: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex)
	
	SWITCH iLocalPaintingState
		CASE ciCutPaintingTopLeft   
			RETURN "MC_INTOBJ_CP_R"
		BREAK
		CASE ciCutPaintingTopRight
			RETURN "MC_INTOBJ_CP_D"
		BREAK
		CASE ciCutPaintingBottomRight
			RETURN "MC_INTOBJ_CP_L"
		BREAK
		CASE ciCutPaintingBottomLeft
			RETURN "MC_INTOBJ_CP_D"
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC BOOL HAS_CUT_PAINTING_REQUIRED_INPUT_BEEN_RECEIVED(INT iLocalPaintingState)

	BOOL bReturnValue = FALSE
	
	//PRINTLN("[ML][InteractWith][HAS_CUT_PAINTING_REQUIRED_INPUT_BEEN_RECEIVED] MC_serverBD_1.iCurrentCutPaintingState = ", MC_serverBD_1.iCurrentCutPaintingState[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex])
	SWITCH iLocalPaintingState
		CASE ciCutPaintingTopLeft   
			IF GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X) > 0.7
				bReturnValue = TRUE
			ENDIF
		BREAK
		CASE ciCutPaintingTopRight
			IF GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) > 0.7
				bReturnValue = TRUE
			ENDIF
		BREAK
		CASE ciCutPaintingBottomRight
			IF GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X) < -0.7
				bReturnValue = TRUE
			ENDIF
		BREAK
		CASE ciCutPaintingBottomLeft
			IF GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) > 0.7
				bReturnValue = TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN bReturnValue
ENDFUNC

PROC CLEANUP_CUT(INT iObj)
	CLEAR_BIT(iLocalBoolCheck32, LBOOL32_HAS_CUT_PAINTING_ANIMATION_STARTED)
			
	IF iPaintingBeingCutLocalState != ciCutPaintingComplete
		PRINTLN("[ML][InteractWith][CP_STATE_CLEAN_UP_CUT] Moving to CP_STATE_WAITING_ON_INPUT as these are not equal iPaintingBeingCutLocalState = ", iPaintingBeingCutLocalState, " ciCutPaintingComplete = ", ciCutPaintingComplete)
		SET_CUT_PAINTING_STATE(iObj, CP_STATE_WAITING_ON_INPUT)
	ELSE
		PRINTLN("[ML][InteractWith][CP_STATE_CLEAN_UP_CUT] Moving to CP_STATE_ROLL_UP_PAINTING as these are equal iPaintingBeingCutLocalState = ", iPaintingBeingCutLocalState, " ciCutPaintingComplete = ", ciCutPaintingComplete)
		SET_CUT_PAINTING_STATE(iObj, CP_STATE_ROLL_UP_PAINTING)
	ENDIF
ENDPROC

PROC PROCESS_CUT_PAINTING(INT iObj, OBJECT_INDEX tempObj)

	STRING sDictionary = GET_INTERACT_WITH_PLAYER_ANIM_DICT(iObj)

	STRING sPaintingCutCurrentIdleAnim
	STRING sPaintingCutCurrentCutAnim
	STRING sPaintingCutCurrentExitAnim
	STRING sPaintingCutCurrentRollUpAnim
	
	DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_LR)
	DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_UD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PHONE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex = -1
		PRINTLN("[ML][InteractWith][PROCESS_CUT_PAINTING] iPaintingIndex = ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex, " for object ", iObj)
		CASSERTLN( DEBUG_CONTROLLER, "PROCESS_INTERACT_WITH_OBJECT_MINIGAME - Painting index set to OFF on for object: ", iObj)
		SET_INTERACT_WITH_STATE(IW_STATE_EXIT_ANIMATION)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck32, LBOOL32_HAS_CUT_PAINTING_GOT_LOCAL_COPY)	
		PRINTLN("[ML][InteractWith][PROCESS_CUT_PAINTING] Getting local copy of MC_serverBD_1.iCurrentCutPaintingState[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex]")
		iPaintingBeingCutLocalState = MC_serverBD_1.iCurrentCutPaintingState[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex]
		SET_BIT(iLocalBoolCheck32, LBOOL32_HAS_CUT_PAINTING_GOT_LOCAL_COPY)
	ENDIF
	
	BOOL bCreateProps = TRUE
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_playerBD_1[iLocalPart].niInteractWith_CurrentSpawnedProps[0]) //Knife
		bCreateProps = FALSE
	ENDIF
	
	REQUEST_HEIST_BAG_MINIGAME_ASSETS(tempObj, FALSE, MC_playerBD_1[iLocalPart].mnHeistGearBag)
	IF HAVE_HEIST_BAG_MINIGAME_ASSETS_LOADED(tempObj, FALSE, MC_playerBD_1[iLocalPart].mnHeistGearBag)
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niMinigameObjects[0])
			IF CREATE_BAG_OBJECTS_AT_POSITION(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj, << 0.0, 0.0, -2.0 >>))
				REMOVE_PLAYER_BAG()
				PRINTLN("[ML][InteractWith][PROCESS_CUT_PAINTING] Created bag object")
			ENDIF
		ENDIF	
	ELSE
		PRINTLN("[ML][InteractWith][PROCESS_CUT_PAINTING] Heist bag minigame assets not loaded")
	ENDIF
	
	SWITCH ecpsAnimationState[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex]
		CASE CP_STATE_WAITING_ON_INPUT
			
			sPaintingCutCurrentIdleAnim = GET_CUT_PAINTING_IDLE_PLAYER_ANIM_NAME(iObj, iPaintingBeingCutLocalState)
			
			IF SHOULD_OBJECT_MINIGAME_BE_DISABLED_BY_POISON_GAS(iObj)
				PRINTLN("[ML][InteractWith][PROCESS_CUT_PAINTING][CP_STATE_WAITING_ON_INPUT] Poison gas build up in progress, so set state to Early Exit")
				SET_CUT_PAINTING_STATE(iObj, CP_STATE_EARLY_EXIT)
				EXIT
			ENDIF
			
			// [ML] deal with player early exit input
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RRIGHT)
				PRINTLN("[ML][InteractWith][PROCESS_CUT_PAINTING][CP_STATE_WAITING_ON_INPUT] Early exit input pressed")
				SET_CUT_PAINTING_STATE(iObj, CP_STATE_EARLY_EXIT)
				EXIT
			ENDIF
			
			IF NOT IS_BIT_SET(iLocalBoolCheck32, LBOOL32_CUT_PAINTING_IS_IDLE_ANIMATION_IN_PROGRESS)
				PRINTLN("[ML][InteractWith][PROCESS_CUT_PAINTING][CP_STATE_WAITING_ON_INPUT] LBOOL32_CUT_PAINTING_IS_IDLE_ANIMATION_IN_PROGRESS is not yet set")
				IF SET_UP_INTERACT_WITH_SYNC_SCENE(tempObj, iObj, sDictionary, sPaintingCutCurrentIdleAnim, iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex], TRUE, DEFAULT, INSTANT_BLEND_IN, DEFAULT, DEFAULT, bCreateProps)
					SET_IW_CAMERA_VARS(iObj, sIWInfo.sInteractWith_CameraVars)
					PLAY_CAM_ANIM(sIWInfo.camInteractWith_Camera, sIWInfo.sInteractWith_CameraVars.sCamAnimName, sIWInfo.sInteractWith_CameraVars.sCamAnimDict, GET_POS_FOR_IW_CAMERA((FALSE), <<0.0, 0.0, -1.0>>, <<0.0, 0.0, 0.0>>, TRUE), GET_ROT_FOR_IW_CAMERA(FALSE, <<0.0, 0.0, 0.0>>), CAF_LOOPING)
					NETWORK_START_SYNCHRONISED_SCENE( MC_playerBD[iLocalPart].iSynchSceneID)
					SET_BIT(iLocalBoolCheck32, LBOOL32_CUT_PAINTING_IS_IDLE_ANIMATION_IN_PROGRESS) 
					PRINTLN("[ML][InteractWith][PROCESS_CUT_PAINTING][CP_STATE_WAITING_ON_INPUT] Playing Idle animation: ", sPaintingCutCurrentIdleAnim)
				ENDIF
				SET_BIT(iLocalBoolCheck32, LBOOL32_CUT_PAINTING_IS_IDLE_ANIMATION_IN_PROGRESS)
			ELSE
				// [ML] check for input			
				DISPLAY_HELP_TEXT_THIS_FRAME_FOR_MINIGAME(GET_CUT_PAINTING_HELPTEXT(iObj, iPaintingBeingCutLocalState), TRUE)
			
				IF HAS_CUT_PAINTING_REQUIRED_INPUT_BEEN_RECEIVED(iPaintingBeingCutLocalState)
					PRINTLN("[ML][InteractWith][PROCESS_CUT_PAINTING][CP_STATE_WAITING_ON_INPUT] Input received")
					SET_CUT_PAINTING_STATE(iObj, CP_STATE_ANIMATING_CUT)
					EXIT
				ENDIF
			ENDIF
		BREAK
		
		CASE CP_STATE_ANIMATING_CUT
			
			IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_CUT_PAINTING_IS_IDLE_ANIMATION_IN_PROGRESS)

				sPaintingCutCurrentCutAnim = GET_CUT_PAINTING_CUT_PLAYER_ANIM_NAME(iObj, iPaintingBeingCutLocalState)
				
				IF SET_UP_INTERACT_WITH_SYNC_SCENE(tempObj, iObj, sDictionary, sPaintingCutCurrentCutAnim, iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex], FALSE, DEFAULT, SLOW_BLEND_IN, DEFAULT, DEFAULT, bCreateProps)
					SET_IW_CAMERA_VARS(iObj, sIWInfo.sInteractWith_CameraVars)
					PLAY_CAM_ANIM(sIWInfo.camInteractWith_Camera, sIWInfo.sInteractWith_CameraVars.sCamAnimName, sIWInfo.sInteractWith_CameraVars.sCamAnimDict, GET_POS_FOR_IW_CAMERA((FALSE), <<0.0, 0.0, -1.0>>, <<0.0, 0.0, 0.0>>, TRUE), GET_ROT_FOR_IW_CAMERA(FALSE, <<0.0, 0.0, 0.0>>))
					NETWORK_START_SYNCHRONISED_SCENE( MC_playerBD[iLocalPart].iSynchSceneID)
					SET_BIT(iLocalBoolCheck32, LBOOL32_CUT_PAINTING_IS_CUT_ANIMATION_IN_PROGRESS) 
					PRINTLN("[ML][InteractWith][PROCESS_CUT_PAINTING][CP_STATE_ANIMATING_CUT] playing Cut animation: ", sPaintingCutCurrentCutAnim)
					SET_BIT(iLocalBoolCheck32, LBOOL32_HAS_CUT_PAINTING_ANIMATION_STARTED)
				ENDIF	
				CLEAR_BIT(iLocalBoolCheck32, LBOOL32_CUT_PAINTING_IS_IDLE_ANIMATION_IN_PROGRESS)
			ELSE
				// we've started and not running, so move to cleanup
				IF NOT IS_SYNC_SCENE_RUNNING(MC_playerBD[iLocalPart].iSynchSceneID)
					PRINTLN("[ML][InteractWith][PROCESS_CUT_PAINTING][CP_STATE_ANIMATING_CUT] Clean up Animating Cut")
					iPaintingBeingCutLocalState++
					PRINTLN("[ML][InteractWith][PROCESS_CUT_PAINTING][CP_STATE_ANIMATING_CUT] iPaintingBeingCutLocalState = ", iPaintingBeingCutLocalState, " after incrementing")
					BROADCAST_CASINO_UPDATED_STEAL_PAINTING_STATE(iPaintingBeingCutLocalState, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex, iObj)
					CLEANUP_CUT(iObj)
					PROCESS_CUT_PAINTING(iObj, tempObj)
				ENDIF
			ENDIF
		BREAK	
		
		CASE CP_STATE_EARLY_EXIT
			PRINTLN("[ML][InteractWith][PROCESS_CUT_PAINTING][CP_STATE_EARLY_EXIT] Entered CP_STATE_EARLY_EXIT")
			
			IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_CUT_PAINTING_IS_IDLE_ANIMATION_IN_PROGRESS)

				sPaintingCutCurrentExitAnim = GET_CUT_PAINTING_EARLY_EXIT_PLAYER_ANIM_NAME(iObj, iPaintingBeingCutLocalState)
				
				IF SET_UP_INTERACT_WITH_SYNC_SCENE(tempObj, iObj, sDictionary, sPaintingCutCurrentExitAnim, iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex], FALSE, DEFAULT, SLOW_BLEND_IN, DEFAULT, DEFAULT, bCreateProps)
					SET_IW_CAMERA_VARS(iObj, sIWInfo.sInteractWith_CameraVars)
					PLAY_CAM_ANIM(sIWInfo.camInteractWith_Camera, sIWInfo.sInteractWith_CameraVars.sCamAnimName, sIWInfo.sInteractWith_CameraVars.sCamAnimDict, GET_POS_FOR_IW_CAMERA((FALSE), <<0.0, 0.0, -1.0>>, <<0.0, 0.0, 0.0>>, TRUE), GET_ROT_FOR_IW_CAMERA(FALSE, <<0.0, 0.0, 0.0>>))
					NETWORK_START_SYNCHRONISED_SCENE( MC_playerBD[iLocalPart].iSynchSceneID)
					PRINTLN("[ML][InteractWith][PROCESS_CUT_PAINTING][CP_STATE_EARLY_EXIT] playing Cut animation: ", sPaintingCutCurrentExitAnim)
					SET_BIT(iLocalBoolCheck32, LBOOL32_HAS_CUT_PAINTING_ANIMATION_STARTED)
											
					IF FIND_ANIM_EVENT_PHASE(sDictionary, sIWInfo.sInteractWith_CameraVars.sCamAnimName, "CamBlendOut", fPaintingCamBlendoutPhaseStart, fPaintingCamBlendoutPhaseEnd)	
						PRINTLN("[ML][InteractWith][PROCESS_CUT_PAINTING][CP_STATE_EARLY_EXIT] - FIND_ANIM_EVENT_PHASE - fPaintingCamBlendoutPhaseStart: ", fPaintingCamBlendoutPhaseStart, " fPaintingCamBlendoutPhaseEnd: ", fPaintingCamBlendoutPhaseEnd, " SUCCESS")
					ELSE
						PRINTLN("[ML][InteractWith][PROCESS_CUT_PAINTING][CP_STATE_EARLY_EXIT] - FIND_ANIM_EVENT_PHASE - fPaintingCamBlendoutPhaseStart: ", fPaintingCamBlendoutPhaseStart, " fPaintingCamBlendoutPhaseEnd: ", fPaintingCamBlendoutPhaseEnd, " FAILED")
					ENDIF														
				ENDIF
	 			CLEAR_BIT(iLocalBoolCheck32, LBOOL32_CUT_PAINTING_IS_IDLE_ANIMATION_IN_PROGRESS)
			
			ELSE
				IF NOT IS_SYNC_SCENE_RUNNING(MC_playerBD[iLocalPart].iSynchSceneID)
					// we've started and not running, so move to cleanup
					PRINTLN("[ML][InteractWith][PROCESS_CUT_PAINTING][CP_STATE_EARLY_EXIT] Early exit, leave state machine")
					CLEAR_BIT(iLocalBoolCheck32, LBOOL32_HAS_CUT_PAINTING_ANIMATION_STARTED)
					CLEAR_BIT(iLocalBoolCheck32, LBOOL32_HAS_CUT_PAINTING_GOT_LOCAL_COPY)
					SET_BIT(iLocalBoolCheck32, LBOOL32_HAS_CUT_PAINTING_EXITED_EARLY)
					SET_CUT_PAINTING_STATE(iObj, CP_STATE_WAITING_ON_INPUT)	// [ML] So that when we return we're in the correct state					
					SET_INTERACT_WITH_STATE(IW_STATE_EXIT_ANIMATION)
					IF NOT IS_STRING_NULL_OR_EMPTY(GET_CUT_PAINTING_PAINTING_IDLE_ANIM_NAME(iObj))
						PLAY_ENTITY_ANIM(tempObj, GET_CUT_PAINTING_PAINTING_IDLE_ANIM_NAME(iObj), sDictionary, INSTANT_BLEND_IN, TRUE, FALSE)
					ENDIF
				ELSE
					PRINTLN("[ML][InteractWith][PROCESS_CUT_PAINTING][CP_STATE_EARLY_EXIT] Sync scene ", MC_playerBD[iLocalPart].iSynchSceneID, " is running")
					PRINTLN("[ML][InteractWith][PROCESS_CUT_PAINTING][CP_STATE_EARLY_EXIT] fPaintingCamBlendoutPhaseStart = ", fPaintingCamBlendoutPhaseStart)
					IF GET_SYNC_SCENE_PHASE(MC_playerBD[iLocalPart].iSynchSceneID) >= fPaintingCamBlendoutPhaseStart
						PRINTLN("[ML][InteractWith][PROCESS_CUT_PAINTING][CP_STATE_EARLY_EXIT] GET_SYNC_SCENE_PHASE(MC_playerBD[iLocalPart].iSynchSceneID) >= fPaintingCamBlendoutPhaseStart = ")
						IF DOES_CAM_EXIST(sIWInfo.camInteractWith_Camera)
							SET_GAMEPLAY_CAM_RELATIVE_PITCH()
							SET_GAMEPLAY_CAM_RELATIVE_HEADING()
							RENDER_SCRIPT_CAMS(FALSE, TRUE, 1750)
							DESTROY_CAM(sIWInfo.camInteractWith_Camera)
						ENDIF
					ENDIF
				ENDIF
			ENDIF		
		BREAK	
		
		CASE CP_STATE_ROLL_UP_PAINTING
		
			IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_CUT_PAINTING_IS_CUT_ANIMATION_IN_PROGRESS)
				PRINTLN("[ML][InteractWith][PROCESS_CUT_PAINTING][CP_STATE_ROLL_UP_PAINTING] Clearing LBOOL32_CUT_PAINTING_IS_CUT_ANIMATION_IN_PROGRESS")
				CLEAR_BIT(iLocalBoolCheck32, LBOOL32_CUT_PAINTING_IS_CUT_ANIMATION_IN_PROGRESS)
			ENDIF
			
			IF NOT IS_SYNC_SCENE_RUNNING(MC_playerBD[iLocalPart].iSynchSceneID)			
				
				IF NOT IS_BIT_SET(iLocalBoolCheck32, LBOOL32_HAS_CUT_PAINTING_ANIMATION_STARTED)
					
					sPaintingCutCurrentRollUpAnim = GET_CUT_PAINTING_EXIT_ROLL_UP_ANIM_NAME(iObj)
					
					IF SET_UP_INTERACT_WITH_SYNC_SCENE(tempObj, iObj, sDictionary, sPaintingCutCurrentRollUpAnim, iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex], FALSE, DEFAULT, INSTANT_BLEND_IN, DEFAULT, DEFAULT, bCreateProps)
						SET_IW_CAMERA_VARS(iObj, sIWInfo.sInteractWith_CameraVars)
						PLAY_CAM_ANIM(sIWInfo.camInteractWith_Camera, sIWInfo.sInteractWith_CameraVars.sCamAnimName, sIWInfo.sInteractWith_CameraVars.sCamAnimDict, GET_POS_FOR_IW_CAMERA((FALSE), <<0.0, 0.0, -1.0>>, <<0.0, 0.0, 0.0>>, TRUE), GET_ROT_FOR_IW_CAMERA(FALSE, <<0.0, 0.0, 0.0>>))
						NETWORK_START_SYNCHRONISED_SCENE( MC_playerBD[iLocalPart].iSynchSceneID)
						PRINTLN("[ML][InteractWith][PROCESS_CUT_PAINTING][CP_STATE_ROLL_UP_PAINTING] playing Roll Up Painting to exit animation: ", sPaintingCutCurrentRollUpAnim)
						SET_BIT(iLocalBoolCheck32, LBOOL32_HAS_CUT_PAINTING_ANIMATION_STARTED)
						
						IF FIND_ANIM_EVENT_PHASE(sDictionary, sIWInfo.sInteractWith_CameraVars.sCamAnimName, "CamBlendOut", fPaintingCamBlendoutPhaseStart, fPaintingCamBlendoutPhaseEnd)	
							PRINTLN("[ML][InteractWith][PROCESS_CUT_PAINTING][CP_STATE_ROLL_UP_PAINTING] - FIND_ANIM_EVENT_PHASE - fPaintingCamBlendoutPhaseStart: ", fPaintingCamBlendoutPhaseStart, " fPaintingCamBlendoutPhaseEnd: ", fPaintingCamBlendoutPhaseEnd, " SUCCESS")
						ELSE
							PRINTLN("[ML][InteractWith][PROCESS_CUT_PAINTING][CP_STATE_ROLL_UP_PAINTING] - FIND_ANIM_EVENT_PHASE - fPaintingCamBlendoutPhaseStart: ", fPaintingCamBlendoutPhaseStart, " fPaintingCamBlendoutPhaseEnd: ", fPaintingCamBlendoutPhaseEnd, " FAILED")
						ENDIF
						
					ENDIF	
				ELSE      
					// we've started and not running, so move to cleanup
					PRINTLN("[ML][InteractWith][PROCESS_CUT_PAINTING][CP_STATE_ROLL_UP_PAINTING] Leave the painting cut state machine, move to IW_STATE_EXIT_ANIMATION")
					CLEAR_BIT(iLocalBoolCheck32, LBOOL32_HAS_CUT_PAINTING_ANIMATION_STARTED)
					CLEAR_BIT(iLocalBoolCheck32, LBOOL32_HAS_CUT_PAINTING_GOT_LOCAL_COPY)
					BROADCAST_SCRIPT_EVENT_PAINTING_STOLEN(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex, iLocalPart)
					g_TransitionSessionNonResetVars.bInvolvedInCashGrab = TRUE //For the cash drop logic
					INCREMENT_OFF_RULE_MINIGAME_SCORE_FROM_OBJECT(ciOffRuleScore_High, iObj)
					IF NOT IS_STRING_NULL_OR_EMPTY(GET_CUT_PAINTING_PAINTING_IDLE_ANIM_NAME(iObj))
						PLAY_ENTITY_ANIM(tempObj, GET_CUT_PAINTING_PAINTING_IDLE_ANIM_NAME(iObj), sDictionary, INSTANT_BLEND_IN, TRUE, FALSE)
					ENDIF
					SET_INTERACT_WITH_STATE(IW_STATE_EXIT_ANIMATION)
				ENDIF
			ELSE
				PRINTLN("[ML][InteractWith][PROCESS_CUT_PAINTING][CP_STATE_ROLL_UP_PAINTING] Sync scene ", MC_playerBD[iLocalPart].iSynchSceneID, " is running")
				PRINTLN("[ML][InteractWith][PROCESS_CUT_PAINTING][CP_STATE_ROLL_UP_PAINTING] fPaintingCamBlendoutPhaseStart = ", fPaintingCamBlendoutPhaseStart)
				IF GET_SYNC_SCENE_PHASE(MC_playerBD[iLocalPart].iSynchSceneID) >= fPaintingCamBlendoutPhaseStart
					PRINTLN("[ML][InteractWith][PROCESS_CUT_PAINTING][CP_STATE_ROLL_UP_PAINTING] GET_SYNC_SCENE_PHASE(MC_playerBD[iLocalPart].iSynchSceneID) >= fPaintingCamBlendoutPhaseStart = ", fPaintingCamBlendoutPhaseStart)
					IF DOES_CAM_EXIST(sIWInfo.camInteractWith_Camera)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH()
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						RENDER_SCRIPT_CAMS(FALSE, TRUE, 3500)
						DESTROY_CAM(sIWInfo.camInteractWith_Camera)
					ENDIF
				ENDIF
			ENDIF		
		BREAK
		
	ENDSWITCH

	#IF IS_DEBUG_BUILD
		//display state of the Painting minigame
		TEXT_LABEL_63 tl63Painting
		tl63Painting = "Painting Index: "
		tl63Painting += g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex
		DRAW_DEBUG_TEXT_ABOVE_ENTITY(tempObj, tl63Painting, 0.70)
		tl63Painting = "Anim Variation: "
		IF iPaintingBeingCutAnimVariation = 0
			tl63Painting += "VER_01"
		ELIF iPaintingBeingCutAnimVariation = 1
			tl63Painting += "VER_02"
		ELSE
			tl63Painting += "ILLEGAL VARIATION"
		ENDIF
		DRAW_DEBUG_TEXT_ABOVE_ENTITY(tempObj, tl63Painting, 0.675)
		tl63Painting = "Painting Corner: "
		tl63Painting += iPaintingBeingCutLocalState
		DRAW_DEBUG_TEXT_ABOVE_ENTITY(tempObj, tl63Painting, 0.65)
		tl63Painting = "State: "
		tl63Painting += GET_PAINTING_CUT_STATE_MACHINE_STATE(iObj)
		DRAW_DEBUG_TEXT_ABOVE_ENTITY(tempObj, tl63Painting, 0.625)
		tl63Painting = "iPaintingKnifeAnimation: "
		tl63Painting += iPaintingKnifeAnimation[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex]
		DRAW_DEBUG_TEXT_ABOVE_ENTITY(tempObj, tl63Painting, 0.60)
		IF DOES_CAM_EXIST(sIWInfo.camInteractWith_Camera)
			tl63Painting = "Camera Phase: "
			tl63Painting += GET_STRING_FROM_FLOAT(GET_CAM_ANIM_CURRENT_PHASE(sIWInfo.camInteractWith_Camera))
			DRAW_DEBUG_TEXT_ABOVE_ENTITY(tempObj, tl63Painting, 0.575)
		ENDIF
	#ENDIF

ENDPROC

FUNC STRING GET_INTERACT_WITH_PLANT_CASINO_VAULT_DOOR_EXPLOSIVES_STATE_NAME(INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE state)
	SWITCH state
		CASE INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE__INIT
			RETURN "INIT"
		CASE INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE__IDLE
			RETURN "IDLE"			
		CASE INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE__PLANTING
			RETURN "PLANTING"			
		CASE INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE__EXITING
			RETURN "EXITING"			
	ENDSWITCH
	
	RETURN "INVALID STATE"
ENDFUNC
PROC SET_INTERACT_WITH_PLANT_CASINO_VAULT_DOOR_EXPLOSIVES_STATE(INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE newState, BOOL bDontChangeAnim = FALSE)
	IF MC_playerBD_1[iLocalPart].eiInteractWith_PlantExplosivesState != newState
		MC_playerBD_1[iLocalPart].eiInteractWith_PlantExplosivesState = newState
		PRINTLN("[InteractWith][PlantExplosives] Setting state to ", GET_INTERACT_WITH_PLANT_CASINO_VAULT_DOOR_EXPLOSIVES_STATE_NAME(newState))
		
		IF bDontChangeAnim
			eiInteractWith_PlantExplosivesState_LOCAL = MC_playerBD_1[iLocalPart].eiInteractWith_PlantExplosivesState
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_SKIP_INTERACT_WITH_PLANT_CASINO_VAULT_DOOR_EXPLOSIVES_ANIM(INT iObj)
	
	FLOAT fPhaseToUse = 1.0
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__LEFT
		fPhaseToUse = 0.79
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__RIGHT
		fPhaseToUse = 0.83
	ENDIF
	
	IF HAS_ANIM_BREAKOUT_EVENT_FIRED()
	OR GET_SYNC_SCENE_PHASE(MC_playerBD[iLocalPart].iSynchSceneID) > fPhaseToUse
		IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_ATTACK2)
		OR sIWInfo.iInteractWith_ObjectsPlacedLocalCounter + 1 >= GET_INTERACT_WITH_NUM_PROPS_TO_SPAWN(iObj)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_INTERACT_WITH_PLANT_CASINO_VAULT_DOOR_EXPLOSIVES(INT iObj, OBJECT_INDEX tempObj)
	
	INT iProgress
	BOOL bFinished = FALSE
	TEXT_LABEL_15 tlHelpLabel = "MC_PLCV_PL"
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__LEFT
		iProgress = MC_serverBD_1.iCurrentVaultDoorPlantedExplosives_LeftSide
		
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__RIGHT
		iProgress = MC_serverBD_1.iCurrentVaultDoorPlantedExplosives_RightSide
		
	ENDIF
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PHONE)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
	
	SWITCH MC_playerBD_1[iLocalPart].eiInteractWith_PlantExplosivesState
		CASE INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE__INIT
			sIWInfo.iInteractWith_ObjectsPlacedLocalCounter = iProgress
			SET_INTERACT_WITH_PLANT_CASINO_VAULT_DOOR_EXPLOSIVES_STATE(INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE__IDLE, TRUE)
		BREAK
		
		CASE INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE__IDLE
		
			IF sIWInfo.iInteractWith_ObjectsPlacedLocalCounter != iProgress
			AND NOT IS_BIT_SET(sIWInfo.iInteractWith_Bitset, ciInteractWith_Bitset__SkippedTransition)
				PRINTLN("[InteractWith][PlantExplosives] MISMATCH! Wait for serverBD... sIWInfo.iInteractWith_ObjectsPlacedLocalCounter is ", sIWInfo.iInteractWith_ObjectsPlacedLocalCounter, " / iProgress is ", iProgress)
				EXIT
			ENDIF
			
			IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_ATTACK2)
			OR IS_BIT_SET(sIWInfo.iInteractWith_Bitset, ciInteractWith_Bitset__SkippedTransition)
				IF iProgress < GET_INTERACT_WITH_NUM_PROPS_TO_SPAWN(iObj)
					SET_INTERACT_WITH_PLANT_CASINO_VAULT_DOOR_EXPLOSIVES_STATE(INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE__PLANTING)
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP()
					ENDIF
				ELSE
					PRINTLN("[InteractWith][PlantExplosives] Already planted all of the bombs")
				ENDIF
			ELSE
				IF iProgress + 1 = GET_INTERACT_WITH_NUM_PROPS_TO_SPAWN(iObj)
					tlHelpLabel = "MC_PLCV_PLF"
				ELSE
					tlHelpLabel += iProgress
				ENDIF
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					DISPLAY_HELP_TEXT_THIS_FRAME_FOR_MINIGAME(tlHelpLabel, TRUE)
				ENDIF
			ENDIF
		BREAK
	
		CASE INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE__PLANTING
			IF eiInteractWith_PlantExplosivesState_LOCAL = MC_playerBD_1[iLocalPart].eiInteractWith_PlantExplosivesState
			AND (NOT IS_SYNC_SCENE_RUNNING(MC_playerBD[iLocalPart].iSynchSceneID)
			OR SHOULD_SKIP_INTERACT_WITH_PLANT_CASINO_VAULT_DOOR_EXPLOSIVES_ANIM(iObj))
				
				TRANSFER_SPAWNED_INTERACT_WITH_PROP_TO_PERSISTENT_PROP(tempObj, iObj, iProgress)
				sIWInfo.iInteractWith_ObjectsPlacedLocalCounter = iProgress + 1
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__LEFT 
					BROADCAST_FMMC_INTERACT_WITH_EVENT(iObj, FALSE, FALSE, TRUE, FALSE)
				ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__RIGHT
					BROADCAST_FMMC_INTERACT_WITH_EVENT(iObj, FALSE, FALSE, FALSE, TRUE)
				ENDIF
				
				IF iProgress + 1 >= GET_INTERACT_WITH_NUM_PROPS_TO_SPAWN(iObj)
					bFinished = TRUE
				ENDIF
				
				IF bFinished
					PRINTLN("[InteractWith][PlantExplosives] Finished planting all explosives!")
					SET_INTERACT_WITH_PLANT_CASINO_VAULT_DOOR_EXPLOSIVES_STATE(INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE__EXITING)
					SET_BIT(iLocalBoolCheck32, LBOOL32_COMPLETED_CASINO_VAULT_DOOR_BOMB_PLANT)
					EXIT
				ELSE
					PRINTLN("[InteractWith][PlantExplosives] Finished planting this one!")
					SET_INTERACT_WITH_PLANT_CASINO_VAULT_DOOR_EXPLOSIVES_STATE(INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE__IDLE)
				ENDIF
				
				IF SHOULD_SKIP_INTERACT_WITH_PLANT_CASINO_VAULT_DOOR_EXPLOSIVES_ANIM(iObj)
					SET_BIT(sIWInfo.iInteractWith_Bitset, ciInteractWith_Bitset__SlowBlendIn)
					SET_BIT(sIWInfo.iInteractWith_Bitset, ciInteractWith_Bitset__SlowBlendOut)
					SET_BIT(sIWInfo.iInteractWith_Bitset, ciInteractWith_Bitset__SkippedTransition)
					PROCESS_INTERACT_WITH_PLANT_CASINO_VAULT_DOOR_EXPLOSIVES(iObj, tempObj)
					EXIT
				ENDIF
			ENDIF
		BREAK
		
		CASE INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE__EXITING
		
			IF HAS_ANIM_BREAKOUT_EVENT_FIRED()
			OR (NOT IS_SYNC_SCENE_RUNNING(MC_playerBD[iLocalPart].iSynchSceneID))
			OR GET_SYNC_SCENE_PHASE(MC_playerBD[iLocalPart].iSynchSceneID) > 0.7
				CLEAR_PED_TASKS(LocalPlayerPed)
				SET_INTERACT_WITH_STATE(IW_STATE_FINISHED)
				SET_INTERACT_WITH_PLANT_CASINO_VAULT_DOOR_EXPLOSIVES_STATE(INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE__INIT)
			ENDIF
			
			EXIT
	ENDSWITCH
	
	IF eiInteractWith_PlantExplosivesState_LOCAL != MC_playerBD_1[iLocalPart].eiInteractWith_PlantExplosivesState
	OR NOT IS_SYNC_SCENE_RUNNING(MC_playerBD[iLocalPart].iSynchSceneID)
	OR IS_BIT_SET(sIWInfo.iInteractWith_Bitset, ciInteractWith_Bitset__SkippedTransition)
	
		sIWInfo.sInteractWith_AnimName = GET_INTERACT_WITH_PLAYER_ANIM_NAME(iObj)
		
		BOOL bLooping = (MC_playerBD_1[iLocalPart].eiInteractWith_PlantExplosivesState = INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE__IDLE)
		BOOL bSkippedTransition = IS_BIT_SET(sIWInfo.iInteractWith_Bitset, ciInteractWith_Bitset__SkippedTransition)
		
		IF SET_UP_INTERACT_WITH_SYNC_SCENE(tempObj, iObj, sIWInfo.sInteractWith_Dictionary, sIWInfo.sInteractWith_AnimName, DEFAULT, bLooping, DEFAULT, INSTANT_BLEND_IN, DEFAULT, INSTANT_BLEND_OUT, FALSE)
		
			IF bSkippedTransition
			AND iProgress + 1 < ciInteractWith__Max_Spawned_Props
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_playerBD_1[iLocalPart].niInteractWith_CurrentSpawnedProps[iProgress + 1])
					OBJECT_INDEX oiNextBomb = NET_TO_OBJ(MC_playerBD_1[iLocalPart].niInteractWith_CurrentSpawnedProps[iProgress + 1])
					PRINTLN("[InteractWith][PlantExplosives] Making the bomb visible now because we'll be skipping its 'Create' tag!")
					SET_ENTITY_VISIBLE(oiNextBomb, TRUE)
				ENDIF
			ENDIF
		
			NETWORK_START_SYNCHRONISED_SCENE(MC_playerBD[iLocalPart].iSynchSceneID)
			eiInteractWith_PlantExplosivesState_LOCAL = MC_playerBD_1[iLocalPart].eiInteractWith_PlantExplosivesState
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bInteractWithDebug
		TEXT_LABEL_63 tlDebugText
		tlDebugText = "BD State: "
		tlDebugText += GET_INTERACT_WITH_PLANT_CASINO_VAULT_DOOR_EXPLOSIVES_STATE_NAME(MC_playerBD_1[iLocalPart].eiInteractWith_PlantExplosivesState)
		DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.25, 0.75, 0.5>>)
		
		tlDebugText = "Lc State: "
		tlDebugText += GET_INTERACT_WITH_PLANT_CASINO_VAULT_DOOR_EXPLOSIVES_STATE_NAME(eiInteractWith_PlantExplosivesState_LOCAL)
		DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.25, 0.775, 0.5>>)
		
		tlDebugText = "Left Side: "
		tlDebugText += MC_serverBD_1.iCurrentVaultDoorPlantedExplosives_LeftSide
		tlDebugText += " | Right Side: "
		tlDebugText += MC_serverBD_1.iCurrentVaultDoorPlantedExplosives_RightSide
		DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.25, 0.8, 0.5>>)
	ENDIF
	#ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_CLOSE_ENOUGH_TO_INTERACT_WITH_OBJECT(OBJECT_INDEX tempObj, INT iObj, VECTOR vTargetPos)
	UNUSED_PARAMETER(tempObj)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__ACTIVATE_EXPLOSIVES
		RETURN TRUE
	ENDIF
	
	RETURN VDIST2(GET_ENTITY_COORDS(LocalPlayerPed), vTargetPos) <= GET_INTERACTION_DISTANCE_FROM_OBJECT(iObj)
ENDFUNC

FUNC FLOAT GET_INTERACT_WITH_DISTANCE_ALLOWED(INT iObj)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__HOLD_BUTTON
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PUSH_DAILY_CASH_BUTTON
		RETURN 0.2955
	ENDIF
	
	IF IS_OBJECT_INTERACT_WITH_A_SYNC_SCENE_ANIM(iObj)
		RETURN 0.15
	ENDIF

	RETURN 0.125
ENDFUNC

PROC INTERACT_WITH_MISC_PRE_STATE_MACHINE_PROCESSING(INT iObj, OBJECT_INDEX tempObj)
	
	IF GET_INTERACT_WITH_PROP_MODEL_TO_SPAWN(iObj) != DUMMY_MODEL_FOR_SCRIPT
		REQUEST_MODEL(GET_INTERACT_WITH_PROP_MODEL_TO_SPAWN(iObj))
	ENDIF
	
	sIWInfo.iInteractWith_ObjectiveTextToUse = ciObjectiveText_Primary
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__SEARCH_BODY_ON_FLOOR
		sIWInfo.bInteractWith_Occupied = IS_POSITION_OCCUPIED(sIWInfo.vInteractWith_TargetPos, cfInteractWith_OccupiedAreaCheckSize, FALSE, TRUE, FALSE, FALSE, FALSE, tempObj)
	
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__USE_KEYCARD
		//Set the minigame as completed if processing off rule and the object is broken/fragmented early.
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_AllowMinigameOffRule)
		AND NOT IS_BIT_SET(MC_serverBD_1.iOffRuleMinigameCompleted, iObj)
			IF DOES_ENTITY_EXIST(tempObj)
			AND IS_OBJECT_BROKEN_AND_VISIBLE(tempObj)
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetFour, cibsOBJ4_UpdateLinkedDoorsOnDestroyed)
					PRINTLN("[InteractWith] Fingerprint Keypad Obj ", iobj, " broken! cibsOBJ4_UpdateLinkedDoorsOnDestroyed is set.")
					//Update/Unlock any linked doors when destroyed.
					SET_DOOR_TO_UPDATE_OFF_RULE_FROM_OBJECT(iObj, TRUE, ciUPDATE_DOOR_OFF_RULE_METHOD__OBJ_DESTROYED)
				ELSE
					PRINTLN("[InteractWith] Fingerprint Keypad Obj ", iobj, " broken! cibsOBJ4_UpdateLinkedDoorsOnDestroyed is NOT set.")
				ENDIF	
			
				BROADCAST_FMMC_UPDATE_MINIGAME_OFF_RULE_EVENT(TRUE, iObj)
			ENDIF
		ENDIF
		
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__CRATE_IN_VAN
	AND iInteractObject_AttachedVehicle[iObj] > -1
	
		IF DOES_BLIP_EXIST(biObjBlip[iobj])
			IF MC_serverBD_4.fFireStrength[iInteractObject_AttachedVehicle[iObj]] > 0.0
				IF GET_BLIP_COLOUR(biObjBlip[iobj]) != BLIP_COLOUR_RED
					PRINTLN("[CannotInteract] Setting iObj: ", iobj, "'s blip colour to red")
					SET_BLIP_COLOUR(biObjBlip[iobj], BLIP_COLOUR_RED)
					SET_BLIP_SCALE(biObjBlip[iobj], FMMC_BLIP_SCALE_OBJECT)
				ENDIF
			ELSE			
				IF GET_BLIP_COLOUR(biObjBlip[iobj]) != BLIP_COLOUR_GREEN
					PRINTLN("[CannotInteract] Setting iObj: ", iobj, "'s blip colour to green")
					SET_BLIP_COLOUR(biObjBlip[iobj], BLIP_COLOUR_GREEN)
					SET_BLIP_SCALE(biObjBlip[iobj], FMMC_BLIP_SCALE_OBJECT)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(sIWInfo.eiInteractWith_CurrentSceneAlignmentEntity_AttachedEntity)
		IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(sIWInfo.eiInteractWith_CurrentSceneAlignmentEntity_AttachedEntity)
			NETWORK_REQUEST_CONTROL_OF_ENTITY(sIWInfo.eiInteractWith_CurrentSceneAlignmentEntity_AttachedEntity)
			PRINTLN("[InteractWith] INTERACT_WITH_MISC_PRE_STATE_MACHINE_PROCESSING | Getting control of eiInteractWith_CurrentSceneAlignmentEntity_AttachedEntity")
		ENDIF
	ENDIF
	
	PROCESS_INTERACT_WITH_ATTACHED_VEHICLE(iObj, tempObj)
	
ENDPROC

PROC HEAD_TO_INTERACT_WITH_POSITION(INT iObj, BOOL bForceTask = FALSE)

	IF NOT bForceTask
		IF IS_PED_PERFORMING_TASK(LocalPlayerPed, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
		OR IS_PED_PERFORMING_TASK(LocalPlayerPed, SCRIPT_TASK_GO_STRAIGHT_TO_COORD)
			EXIT
		ENDIF
	ENDIF
	
	CLEAR_PED_TASKS(LocalPlayerPed)
	
	TASK_GO_STRAIGHT_TO_COORD(LocalPlayerPed, sIWInfo.vInteractWith_TargetPos, PEDMOVEBLENDRATIO_RUN, 6000, sIWInfo.fInteractWith_RequiredHeading, GET_INTERACT_WITH_DISTANCE_ALLOWED(iObj))
	PRINTLN("[InteractWith] HEAD_TO_INTERACT_WITH_POSITION | Tasking player to go straight to coords ", sIWInfo.vInteractWith_TargetPos)
	
ENDPROC

FUNC BOOL INTERACT_WITH_DESTROY_ALL_PERSISTENT_PROPS(BOOL bCreateExplosionEffect = FALSE)
	INT iPersistentProp
	INT iPart, iNumPlayersChecked
	
	WHILE DO_PLAYER_LOOP(iPart, iNumPlayersChecked)
		FOR iPersistentProp = 0 TO MC_PlayerBD_1[iPart].iInteractWith_NumPersistentProps - 1
			OBJECT_INDEX oiPersistentProp = GET_INTERACT_WITH_PERSISTENT_PROP(iPersistentProp, iPart)
			
			IF DOES_ENTITY_EXIST(oiPersistentProp)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(oiPersistentProp)
					IF bCreateExplosionEffect
						ADD_EXPLOSION(GET_ENTITY_COORDS(oiPersistentProp), EXP_TAG_HI_OCTANE, 0.5)
					ENDIF
					
					DELETE_OBJECT(oiPersistentProp)
					PRINTLN("[InteractWith] INTERACT_WITH_DESTROY_ALL_PERSISTENT_PROPS - Exploded prop ", iPersistentProp, " from participant ", iPart, "'s list!")
				ENDIF
			ENDIF
		ENDFOR
		
		iPart++
	ENDWHILE
	
	RETURN TRUE
ENDFUNC

PROC START_INTERACT_WITH(OBJECT_INDEX tempObj, INT iObj)

	PRINTLN("[InteractWith] START_INTERACT_WITH | Starting Interact With sequence on object ", iObj)
	
	IF SHOULD_INTERACT_WITH_SEQUENCE_QUICKBLEND(iObj, tempObj)
		SET_BIT(sIWInfo.iInteractWith_Bitset, ciInteractWith_Bitset__QuickBlend)
		PRINTLN("[InteractWith] START_INTERACT_WITH | Setting ciInteractWith_Bitset__QuickBlend for object ", iobj)
	ENDIF
	
	IF NOT SHOULD_SKIP_INTERACT_WITH_SEQUENCE(tempObj, iObj, sIWInfo.vInteractWith_TargetPos, sIWInfo.vInteractWith_TargetRot, sIWInfo.bInteractWith_Occupied)

		sIWInfo.iInteractWith_StartInteractionImmediately = -1
		
		sIWInfo.eiInteractWith_CurrentSceneAlignmentEntity_AttachedEntity = GET_ENTITY_ATTACHED_TO(tempObj)
		sIWInfo.iInteractWith_CachedIndex = iObj
		sIWInfo.oiInteractWith_CachedObj = tempObj
		
		MC_playerBD[iLocalPart].iObjHacking = iobj
		HEAD_TO_INTERACT_WITH_POSITION(iObj, TRUE)
		
		SET_INTERACT_WITH_STATE(IW_STATE_INIT)
		
		SET_IW_CAM_STATE(IW_CAM_CREATE)
		
	ELSE
		PRINTLN("[InteractWith] START_INTERACT_WITH | Skipping animation due to SHOULD_SKIP_INTERACT_WITH_SEQUENCE for object ", iobj)
		MC_playerBD[iLocalPart].iObjHacked = iobj
		
		IF NOT DOES_INTERACT_WITH_HAVE_NO_ANIM(iObj)
			//Skipping as a failsafe - cover it up with an anim
			TASK_PLAY_ANIM(LocalPlayerPed, sIWInfo.sInteractWith_FailsafeDictionary, sIWInfo.sInteractWith_FailsafeAnimName, NORMAL_BLEND_IN, SLOW_BLEND_OUT, DEFAULT, AF_HIDE_WEAPON, 0, FALSE)
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__CUT_OUT_PAINTING
		PRINTLN("[ML] Going to set random variation")
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex % 2 = 0
			PRINTLN("[ML]  Variation is VER_01")
			iPaintingBeingCutAnimVariation = 0
		ELSE
			PRINTLN("[ML]  Variation is VER_02")
			iPaintingBeingCutAnimVariation = 1
		ENDIF
		
		IF IS_BIT_SET(iObjPaintingForceRightHandBS, iObj)
			PRINTLN("[ML] Obj: ", iObj, " has something to its left setting painting cut anim VER_01")
			iPaintingBeingCutAnimVariation = 0
		ELIF IS_BIT_SET(iObjPaintingForceLeftHandBS, iObj)
			PRINTLN("[ML] Obj: ", iObj, " has something to its right, setting painting cut anim VER_02")
			iPaintingBeingCutAnimVariation = 1
		ENDIF	
		
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_CASINO_TUNNEL_EXPLOSIVE
		BROADCAST_FMMC_INTERACT_WITH_EVENT(iObj, FALSE, FALSE, FALSE, FALSE, iLocalPart)
		SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed, FALSE, FALSE)
		SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED, TRUE)
		SET_BIT(sIWInfo.iInteractWith_Bitset, ciInteractWith_Bitset__HidPlayerWeaponForEndCutscene)
		PRINTLN("[InteractWith] Hiding player weapon and setting ciInteractWith_Bitset__HidPlayerWeaponForEndCutscene")
		
	ENDIF
	
ENDPROC

PROC GET_INTERACT_WITH_INFO_AND_LOAD_ANIMS(OBJECT_INDEX tempObj, INT iObj)
	
	// Cache the alignment entity if we need one
	IF GET_MODEL_FOR_INTERACT_WITH_SCENE_ALIGNMENT_ENTITY(iObj) != DUMMY_MODEL_FOR_SCRIPT
	AND NOT DOES_ENTITY_EXIST(sIWInfo.eiInteractWith_CachedSceneAlignmentEntities[iObj])
		sIWInfo.eiInteractWith_CachedSceneAlignmentEntities[iObj] = FIND_INTERACT_WITH_SCENE_ALIGNMENT_ENTITY(tempObj, iObj)
		
		IF DOES_ENTITY_EXIST(sIWInfo.eiInteractWith_CachedSceneAlignmentEntities[iObj])
			PRINTLN("[InteractWith] Caching alignment entity for object ", iObj)
		ELSE
			PRINTLN("[InteractWith] COULDN'T FIND ALIGNMENT ENTITY FOR OBJ ", iObj)
		ENDIF
	ENDIF
	
	sIWInfo.sInteractWith_Dictionary = GET_INTERACT_WITH_PLAYER_ANIM_DICT(iObj)
	sIWInfo.sInteractWith_AnimName = GET_INTERACT_WITH_PLAYER_ANIM_NAME(iObj)
	sIWInfo.sInteractWith_AudioBankName = GET_INTERACT_WITH_AUDIO_BANK(iObj)
	
	sIWInfo.sInteractWith_FailsafeDictionary = GET_INTERACT_WITH_PLAYER_FAILSAFE_ANIM_DICT(iObj)
	sIWInfo.sInteractWith_FailsafeAnimName = GET_INTERACT_WITH_PLAYER_FAILSAFE_ANIM_NAME(iObj)
	
	sIWInfo.vInteractWith_TargetPos = <<0,0,0>>
	sIWInfo.vInteractWith_TargetRot = <<0,0,0>>
	sIWInfo.fInteractWith_RequiredHeading = GET_HEADING_FROM_ENTITIES_LA(LocalPlayerPed, tempObj)
	
	sIWInfo.fInteractWith_DistanceAllowed = GET_INTERACT_WITH_DISTANCE_ALLOWED(iObj)
	sIWInfo.fInteractWith_StartPhase = GET_INTERACT_WITH_START_PHASE(iObj)
	sIWInfo.bInteractWith_Occupied = FALSE
	
	sIWInfo.tlInteractWith_Object = GET_INTERACT_WITH_PROMPT(iObj)
	sIWInfo.iInteractWith_LastFrameProcessed = GET_FRAME_COUNT()
	
	// For Off-Rule Interact-With object minigames, load the anims only when close enough to be relevant
	BOOL bCheckDistance = MC_serverBD_4.iObjRule[iObj][MC_playerBD[iPartToUse].iteam] != FMMC_OBJECTIVE_LOGIC_MINIGAME AND GET_MODEL_FOR_INTERACT_WITH_SCENE_ALIGNMENT_ENTITY(iObj) != DUMMY_MODEL_FOR_SCRIPT // Certain anims are exempt from this check
		
	IF NOT bCheckDistance
	OR iInteractWithAssetLoadStagger = iObj
		IF NOT bCheckDistance
		OR VDIST2(GET_ENTITY_COORDS(tempObj, FALSE), GET_ENTITY_COORDS(LocalPlayerPed, FALSE)) <= POW(cfInteractWithAssetLoadDistance_Offrule, 2.0)
			IF NOT DOES_INTERACT_WITH_HAVE_NO_ANIM(iobj)
				REQUEST_ANIM_DICT(sIWInfo.sInteractWith_Dictionary)
				REQUEST_ANIM_DICT(sIWInfo.sInteractWith_FailsafeDictionary)
				SET_BIT(iInteractWithAssetLoadStartedBS, iObj)
				PRINTLN("[InteractWith] GET_INTERACT_WITH_INFO_AND_LOAD_ANIMS | Loading ", sIWInfo.sInteractWith_Dictionary)
			ENDIF
		ELSE
			CLEAR_BIT(iInteractWithAssetLoadStartedBS, iObj)
		ENDIF
	ENDIF
	
	//Get target position (if we've started loading the assets)
	IF IS_BIT_SET(iInteractWithAssetLoadStartedBS, iObj)
	OR DOES_INTERACT_WITH_HAVE_NO_ANIM(iObj)
		IF sIWInfo.eInteractWith_CurrentState = IW_STATE_IDLE
		OR IS_LOCAL_PLAYER_INTERACTING_WITH_THIS_OBJECT(iObj) // If you're hacking nothing or hacking THIS in particular
			IF IS_OBJECT_INTERACT_WITH_A_SYNC_SCENE_ANIM(iObj)
			AND HAS_ANIM_DICT_LOADED(sIWInfo.sInteractWith_Dictionary)
			AND HAS_ANIM_DICT_LOADED(sIWInfo.sInteractWith_FailsafeDictionary)
			AND NOT ARE_STRINGS_EQUAL(sIWInfo.sInteractWith_AnimName, "")
				sIWInfo.vInteractWith_TargetPos = GET_ANIM_INITIAL_OFFSET_POSITION(sIWInfo.sInteractWith_Dictionary, sIWInfo.sInteractWith_AnimName, GET_INTERACT_WITH_SYNC_SCENE_COORDS(tempObj, iobj, sIWInfo.sInteractWith_Dictionary, sIWInfo.sInteractWith_AnimName), GET_INTERACT_WITH_SYNC_SCENE_ORIENTATION(tempObj, iobj, sIWInfo.sInteractWith_Dictionary, sIWInfo.sInteractWith_AnimName), sIWInfo.fInteractWith_StartPhase)
				sIWInfo.vInteractWith_TargetRot = GET_ANIM_INITIAL_OFFSET_ROTATION(sIWInfo.sInteractWith_Dictionary, sIWInfo.sInteractWith_AnimName, GET_INTERACT_WITH_SYNC_SCENE_COORDS(tempObj, iobj, sIWInfo.sInteractWith_Dictionary, sIWInfo.sInteractWith_AnimName), GET_INTERACT_WITH_SYNC_SCENE_ORIENTATION(tempObj, iobj, sIWInfo.sInteractWith_Dictionary, sIWInfo.sInteractWith_AnimName), sIWInfo.fInteractWith_StartPhase)
				sIWInfo.fInteractWith_RequiredHeading = sIWInfo.vInteractWith_TargetRot.z
			ELSE
				sIWInfo.vInteractWith_TargetPos = GET_ENTITY_COORDS(tempObj)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_MINIGAME_EXEMPT_FROM_BAIL_TIMER(INT iObj)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__CUT_OUT_PAINTING
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__HOLD_BUTTON
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__LEFT
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__RIGHT
		PRINTLN("[ML][IS_MINIGAME_EXEMPT_FROM_BAIL_TIMER] Minigame is exempt from bail timer")
		RETURN TRUE
	ELSE 
		RETURN FALSE
	ENDIF
ENDFUNC

PROC BAIL_INTERACT_WITH(INT iObj)
	CLEANUP_INTERACT_WITH(iObj)
	PRINTLN("[InteractWith][CannotInteract] BAIL_INTERACT_WITH | Bailing interaction with object ", iObj)
ENDPROC

FUNC BOOL SHOULD_INTERACT_WITH_BAIL(OBJECT_INDEX tempObj, INT iObj, BOOL& bShouldSkipMinigame)

	UNUSED_PARAMETER(tempObj)
	UNUSED_PARAMETER(iObj)
	
	IF MC_playerBD[iLocalPart].iObjHacking = iObj
		IF MC_serverBD.iObjHackPart[iObj] > -1
		AND MC_serverBD.iObjHackPart[iObj] != iLocalPart
			PRINTLN("[InteractWith] SHOULD_INTERACT_WITH_BAIL | Bailing because we're hacking this object but the server says someone else is!")
			RETURN TRUE
		ENDIF
	ENDIF
	
	// Haven't started yet
	IF sIWInfo.eInteractWith_CurrentState = IW_STATE_IDLE
		RETURN FALSE
	ENDIF
	
	IF sIWInfo.eInteractWith_CurrentState = IW_STATE_TURNING
	OR sIWInfo.eInteractWith_CurrentState = IW_STATE_WALKING
		IF VDIST2(GET_ENTITY_COORDS(LocalPlayerPed), sIWInfo.vInteractWith_TargetPos) > (GET_INTERACTION_DISTANCE_FROM_OBJECT(iObj) * 2.5)
			PRINTLN("[InteractWith] SHOULD_INTERACT_WITH_BAIL | Bailing due to distance from target pos!")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF MC_playerBD[iLocalPart].iObjHacking = -1
		PRINTLN("[InteractWith] SHOULD_INTERACT_WITH_BAIL | Bailing due to iObjHacking!")
		RETURN TRUE
	ENDIF
	
	IF IS_PED_CLIMBING(LocalPlayerPed)
	OR IS_PED_JUMPING(LocalPlayerPed)
		PRINTLN("[InteractWith] SHOULD_INTERACT_WITH_BAIL | Bailing because we're climbing/jumping!")
		RETURN TRUE
	ENDIF
	
	IF NOT IS_MINIGAME_EXEMPT_FROM_BAIL_TIMER(iObj)
		IF HAS_NET_TIMER_STARTED(sIWInfo.stInteractWith_BailTimer)
			IF HAS_NET_TIMER_EXPIRED(sIWInfo.stInteractWith_BailTimer, ciInteractWith_BailTime)
				bShouldSkipMinigame = TRUE
				PRINTLN("[InteractWith] SHOULD_INTERACT_WITH_BAIL | Bailing because of the bail timer!")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_INTERACT_WITH_MISC(OBJECT_INDEX tempObj, INT iObj)

	PROCESS_INTERACT_WITH_SOUNDS(tempObj, iObj)
	PROCESS_INTERACT_WITH_CAMERAS(iObj, sIWInfo.vInteractWith_TargetPos, sIWInfo.vInteractWith_TargetRot)
	
	INT iSpawnedProp
	FOR iSpawnedProp = 0 TO ciInteractWith__Max_Spawned_Props - 1
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_playerBD_1[iLocalPart].niInteractWith_CurrentSpawnedProps[iSpawnedProp])
			PROCESS_INTERACT_WITH_SPAWNED_PROP(tempObj, iObj, iSpawnedProp)
		ENDIF
	ENDFOR
	
	IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_WEAPON_HAS_BEEN_HIDDEN_BY_INTERACTION)
		WEAPON_TYPE wtCurWeapon
		GET_CURRENT_PED_WEAPON(LocalPlayerPed, wtCurWeapon)
		IF wtCurWeapon != WEAPONTYPE_UNARMED
			SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED, TRUE)
			sIWInfo.wtInteractWith_StoredWeapon = wtCurWeapon
			SET_BIT(iLocalBoolCheck27, LBOOL27_WEAPON_HAS_BEEN_HIDDEN_BY_INTERACTION)
			PRINTLN("[InteractWith] IW_STATE_WALKING | Player should now be unarmed")
		ENDIF
	ENDIF
	
	// Load audio
	IF NOT IS_STRING_NULL_OR_EMPTY(sIWInfo.sInteractWith_AudioBankName)
		REQUEST_SCRIPT_AUDIO_BANK(sIWInfo.sInteractWith_AudioBankName)
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sIWInfo.tlInteractWith_Object)
		CLEAR_HELP(TRUE)
	ENDIF
	
	IF DOES_ENTITY_EXIST(sIWInfo.eiInteractWith_CurrentSceneAlignmentEntity_AttachedEntity)
		IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(sIWInfo.eiInteractWith_CurrentSceneAlignmentEntity_AttachedEntity)
			NETWORK_REQUEST_CONTROL_OF_ENTITY(sIWInfo.eiInteractWith_CurrentSceneAlignmentEntity_AttachedEntity)
		ELSE
			SET_ENTITY_NO_COLLISION_ENTITY(sIWInfo.eiInteractWith_CurrentSceneAlignmentEntity_AttachedEntity, LocalPlayerPed, FALSE)
			SET_ENTITY_NO_COLLISION_ENTITY(sIWInfo.eiInteractWith_CurrentSceneAlignmentEntity_AttachedEntity, tempObj, FALSE)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL PROCESS_INTERACT_WITH_STARTING_ANIMATION(OBJECT_INDEX tempObj, INT iObj)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__DOWNLOAD
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__DOWNLOAD_BAR_ONLY
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__HACK_LAPTOP_USB
		IF NOT INIT_INTERACT_WITH_DOWNLOAD_SCREEN()
			//Need to wait for download screen
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sIWInfo.sInteractWith_AudioBankName)
		IF NOT REQUEST_SCRIPT_AUDIO_BANK(sIWInfo.sInteractWith_AudioBankName, TRUE)
			PRINTLN("[CannotInteract] Not allowing interaction because we're still loading audio bank: ", sIWInfo.sInteractWith_AudioBankName, ". iObj: ", iobj)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF DOES_INTERACT_WITH_HAVE_NO_ANIM(iobj)
		REINIT_NET_TIMER(sIWInfo.stInteractWith_DownloadTimer[iObj])
		RETURN TRUE
	ENDIF
	
	IF IS_OBJECT_INTERACT_WITH_A_SYNC_SCENE_ANIM(iObj)
		IF IS_SYNC_SCENE_RUNNING(MC_playerBD[iLocalPart].iSynchSceneID)
			SET_INTERACT_WITH_STATE(IW_STATE_ANIMATING)
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetFour, cibsOBJ4_SpookNearbyWhenUsed)
				SPOOK_PEDS_AT_COORD_IN_RADIUS(GET_ENTITY_COORDS(tempObj), 7.5, ciSpookCoordID_OBJECT_INTERACT_WITH, localPlayerPed)
			ENDIF
			
			RETURN TRUE
			
		ELSE
			IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
				NETWORK_REQUEST_CONTROL_OF_ENTITY(tempObj)
				PRINTLN("[InteractWith] PROCESS_INTERACT_WITH_STARTING_ANIMATION | Getting control of tempObj")
				RETURN FALSE
			ENDIF
			
			IF DOES_ENTITY_EXIST(sIWInfo.eiInteractWith_CurrentSceneAlignmentEntity_AttachedEntity)
				IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(sIWInfo.eiInteractWith_CurrentSceneAlignmentEntity_AttachedEntity)
					NETWORK_REQUEST_CONTROL_OF_ENTITY(sIWInfo.eiInteractWith_CurrentSceneAlignmentEntity_AttachedEntity)
					PRINTLN("[InteractWith] PROCESS_INTERACT_WITH_STARTING_ANIMATION | Getting control of eiInteractWith_CurrentSceneAlignmentEntity_AttachedEntity")
					RETURN FALSE
				ENDIF
			ENDIF
			
			INT iMode = GET_INTERACT_WITH_STARTING_ANIMATION_MODE(iObj)
			
			IF SET_UP_INTERACT_WITH_SYNC_SCENE(tempObj, iObj, sIWInfo.sInteractWith_Dictionary, sIWInfo.sInteractWith_AnimName, iMode, FALSE, sIWInfo.fInteractWith_StartPhase, DEFAULT, SHOULD_INTERACT_WITH_HOLD_LAST_FRAME(iObj))
				PRINTLN("[InteractWith] PROCESS_INTERACT_WITH_STARTING_ANIMATION | Calling NETWORK_START_SYNCHRONISED_SCENE")
				NETWORK_START_SYNCHRONISED_SCENE(MC_playerBD[iLocalPart].iSynchSceneID)
			ENDIF
			
		ENDIF
	ELSE
		IF NOT IS_PED_PERFORMING_TASK(LocalPlayerPed, SCRIPT_TASK_PLAY_ANIM)
			PRINTLN("[InteractWith] Trying to play animation")
			TASK_PLAY_ANIM(LocalPlayerPed, sIWInfo.sInteractWith_Dictionary, sIWInfo.sInteractWith_AnimName, NORMAL_BLEND_IN, SLOW_BLEND_OUT, DEFAULT, AF_HIDE_WEAPON)
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetFour, cibsOBJ4_SpookNearbyWhenUsed)
				SPOOK_PEDS_AT_COORD_IN_RADIUS(GET_ENTITY_COORDS(tempObj), 7.5, ciSpookCoordID_OBJECT_INTERACT_WITH, localPlayerPed)
			ENDIF
			REINIT_NET_TIMER(sIWInfo.tdInteractWith_AnimationTimer)
		ELSE
			SET_INTERACT_WITH_STATE(IW_STATE_ANIMATING)
		ENDIF
		
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PROCESS_INTERACT_WITH_WAITING_FOR_INPUT(OBJECT_INDEX tempObj, INT iObj)
	
	// No interaction just yet
	IF sIWInfo.eInteractWith_CurrentState != IW_STATE_IDLE
		PRINTLN("[InteractWith] PROCESS_INTERACT_WITH_WAITING_FOR_INPUT | We're not in idle state! Cleaning up")
		CLEANUP_INTERACT_WITH(iObj)
	ENDIF
	
	// Adding phone contact for phone activation
	IF DOES_INTERACT_WITH_REQUIRE_PHONE_ACTIVATION(iObj)
		IF NOT IS_CONTACT_IN_PHONEBOOK(CHAR_MP_DETONATEPHONE, MULTIPLAYER_BOOK)
			ADD_CONTACT_TO_PHONEBOOK(CHAR_MP_DETONATEPHONE, MULTIPLAYER_BOOK, TRUE)
			MAKE_CONTACT_ENTRY_PRIORITY(CHAR_MP_DETONATEPHONE)
			PRINTLN("[InteractWith] Adding CHAR_MP_DETONATEPHONE!")
		ENDIF
	ENDIF
	
	IF IS_PLAYER_CLOSE_ENOUGH_TO_INTERACT_WITH_OBJECT(tempObj, iObj, sIWInfo.vInteractWith_TargetPos)
	AND PLAYER_CAN_INTERACT_WITH_OBJ(iobj, tempObj, TRUE)
		
		IF HAS_INTERACT_WITH_BUTTON_BEEN_PRESSED(iObj)
		OR sIWInfo.iInteractWith_StartInteractionImmediately = iObj
			RETURN TRUE
			
		ELSE
			IF NOT DOES_INTERACT_WITH_REQUIRE_PHONE_ACTIVATION(iObj)
			AND NOT SHOULD_OBJECT_INTERACTION_HELP_TEXT_BE_BLOCKED()
				sIWInfo.iInteractWith_HelptextGiverIndex = iObj
				DISPLAY_HELP_TEXT_THIS_FRAME_FOR_MINIGAME(sIWInfo.tlInteractWith_Object, TRUE)
				PRINTLN("[InteractWith] Player is being prompted ", sIWInfo.tlInteractWith_Object, " by Obj ", iObj)
			ENDIF
			
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CONTEXT)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__POWER_BOX_CRANK
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)
			ENDIF
			
		ENDIF
	ELSE		
		IF IS_PLAYER_CLOSE_ENOUGH_TO_INTERACT_WITH_OBJECT(tempObj, iObj, sIWInfo.vInteractWith_TargetPos)
			PRINTLN("[CannotInteract] Special condition is stopping interaction with obj ", iobj)
		ELSE
			PRINTLN("[CannotInteract] Player is too far away from iObj ", iobj)
		ENDIF
		
		IF iObj = sIWInfo.iInteractWith_LastFrameHelptextGiverIndex
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sIWInfo.tlInteractWith_Object)
				PRINTLN("[InteractWith] Clearing helptext ", sIWInfo.tlInteractWith_Object, " because we're no longer able to interact with obj ", iObj)
				CLEAR_ALL_HELP_MESSAGES()
				sIWInfo.iInteractWith_HelptextGiverIndex = -1
				sIWInfo.iInteractWith_LastFrameHelptextGiverIndex = -1
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_INTERACT_WITH_DOWNLOADING(OBJECT_INDEX tempObj, INT iObj)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__DOWNLOAD
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__HACK_LAPTOP_USB
		PROCESS_INTERACT_WITH_DOWNLOAD_SCREEN(iObj, tempObj, TRUE)
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__DOWNLOAD_BAR_ONLY
		PROCESS_INTERACT_WITH_DOWNLOAD_SCREEN(iObj, tempObj, FALSE)
	ENDIF
	
	IF HAS_INTERACT_WITH_DOWNLOAD_FINISHED(iObj)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__HACK_LAPTOP_USB
			STOP_SYNC_SCENE(MC_playerBD[iLocalPart].iSynchSceneID)
			//Exit anim with USB stick
			IF SET_UP_INTERACT_WITH_SYNC_SCENE(tempObj, iObj, sIWInfo.sInteractWith_Dictionary, "EXIT", ciInteractWith_LaptopAnim_EXIT, FALSE, DEFAULT, INSTANT_BLEND_IN)
				NETWORK_START_SYNCHRONISED_SCENE( MC_playerBD[iLocalPart].iSynchSceneID)
				PRINTLN("[InteractWith] playing exit for laptop anim now")
				SET_INTERACT_WITH_STATE(IW_STATE_EXIT_ANIMATION)
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__DOWNLOAD
				OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__DOWNLOAD_BAR_ONLY
				OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__HACK_LAPTOP_USB
					CLEANUP_INTERACT_WITH_DOWNLOAD_SCREEN()
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__HACK_LAPTOP_USB
		AND NOT IS_SYNC_SCENE_RUNNING(MC_playerBD[iLocalPart].iSynchSceneID)
			//Idle anim with USB stick
			IF SET_UP_INTERACT_WITH_SYNC_SCENE(tempObj, iObj, sIWInfo.sInteractWith_Dictionary, "IDLE", ciInteractWith_LaptopAnim_IDLE, TRUE, DEFAULT, INSTANT_BLEND_IN)
				NETWORK_START_SYNCHRONISED_SCENE( MC_playerBD[iLocalPart].iSynchSceneID)
				PRINTLN("[InteractWith] playing idle scene for laptop now")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_INTERACT_WITH_ANIMATION_HAS_FINISHED(OBJECT_INDEX tempObj, INT iObj)
	
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__HACK_LAPTOP_USB
		REINIT_NET_TIMER(sIWInfo.stInteractWith_DownloadTimer[iObj])
		STOP_SYNC_SCENE(MC_playerBD[iLocalPart].iSynchSceneID)
		SET_INTERACT_WITH_STATE(IW_STATE_DOWNLOADING)
		
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__MOVE_YACHT
		STOP_SYNC_SCENE(MC_playerBD[iLocalPart].iSynchSceneID)
		SET_INTERACT_WITH_STATE(IW_STATE_YACHT_CUTSCENE)
		
	ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_HoldInteractWith)
		REINIT_NET_TIMER(sIWInfo.stInteractWith_DownloadTimer[iObj])
		SET_INTERACT_WITH_STATE(IW_STATE_HOLD_INTERACT)
		
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__CUT_OUT_PAINTING
		PRINTLN("[InteractWith] Jumping to IW_STATE_CUT_PAINTING", iObj)
		REINIT_NET_TIMER(sIWInfo.stInteractWith_DownloadTimer[iObj])
		STOP_SYNC_SCENE(MC_playerBD[iLocalPart].iSynchSceneID)
		SET_INTERACT_WITH_STATE(IW_STATE_CUT_PAINTING)
		PROCESS_CUT_PAINTING(iObj, tempObj)
		
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__LEFT
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__RIGHT
		PRINTLN("[InteractWith] Jumping to IW_STATE_PLANT_VAULT_DOOR_EXPLOSIVES", iObj)
		REINIT_NET_TIMER(sIWInfo.stInteractWith_DownloadTimer[iObj])
		STOP_SYNC_SCENE(MC_playerBD[iLocalPart].iSynchSceneID)
		SET_INTERACT_WITH_STATE(IW_STATE_PLANT_VAULT_DOOR_EXPLOSIVES)
		
	ELSE
		// Standard animation completion
		INTERACT_WITH_TRANSFER_ALL_SPAWNED_PROPS_TO_PERSISTENT_PROPS(tempObj, iObj)
		
		IF NOT HAS_IW_CAM_BEEN_OVERRIDDEN()
			SET_INTERACT_WITH_STATE(IW_STATE_FINISHED)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_INTERACT_WITH_VALID_TO_PROCESS(OBJECT_INDEX tempObj, INT iObj)
	UNUSED_PARAMETER(tempObj)
	
	IF MC_playerBD[iLocalPart].iObjHacking > -1
	AND MC_playerBD[iLocalPart].iObjHacking != iObj
		// We're already interacting with a different object - ignore this one
		RETURN FALSE
	ENDIF
	
	IF MC_playerBD[iLocalPart].iObjHacked > -1
	OR NOT bLocalPlayerPedOk
		// Not valid for this right now
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SET_HOLD_BUTTON_INTERACT_STATE(HOLD_BUTTON_INTERACT_STATE eNewState)
	PRINTLN("[InteractWith] SET_HOLD_BUTTON_INTERACT_STATE - Setting state from ", sIWInfo.eHoldInteractState, " to ", eNewState)
	sIWInfo.eHoldInteractState = eNewState
ENDPROC

FUNC STRING GET_PLAYER_LOOP_ANIM_NAME_FOR_HOLD_BUTTON_INTERACT_WITH(INT iAnim)
	SWITCH iAnim
		CASE 0		RETURN "REACT_A"
		CASE 1		RETURN "REACT_B"
		CASE 2		RETURN "REACT_D"
		CASE 3		RETURN "REACT_E"
	ENDSWITCH
	
	RETURN "REACT_A"
ENDFUNC

PROC RESET_HOLD_BUTTON_INTERACT_DATA()
	RESET_NET_TIMER(sIWInfo.stHoldInteractReactTimer)
	RESET_NET_TIMER(sIWInfo.stHoldInteractReactDuration)
	sIWInfo.eHoldInteractState = HOLD_BUTTON_STATE_WAITING
	sIWInfo.bDoneLongHoldReact = FALSE
ENDPROC

PROC PROCESS_HOLD_BUTTON_INTERACT_ANIMS(OBJECT_INDEX tempObj, INT iObj)
	TEXT_LABEL_63 tl63 = "State: "
	tl63 += ENUM_TO_INT(sIWInfo.eHoldInteractState)
	DRAW_DEBUG_TEXT_2D(tl63, <<0.1, 0.65, 0.0>>)
	tl63 = "fPhase: "
	tl63 += FLOAT_TO_STRING(GET_SYNC_SCENE_PHASE(MC_playerBD[iLocalPart].iSynchSceneID))
	DRAW_DEBUG_TEXT_2D(tl63, <<0.1, 0.67, 0.0>>)
	SWITCH sIWInfo.eHoldInteractState
		CASE HOLD_BUTTON_STATE_WAITING
			IF NOT HAS_NET_TIMER_STARTED(sIWInfo.stHoldInteractReactTimer)
				REINIT_NET_TIMER(sIWInfo.stHoldInteractReactTimer)
				sIWInfo.iHoldInteractReactTimerLength = GET_RANDOM_INT_IN_RANGE(1000, 5001)
				sIWInfo.iHoldInteractReactAnim = GET_RANDOM_INT_IN_RANGE(0, 4)
				PRINTLN("[InteractWith] PROCESS_HOLD_BUTTON_INTERACT_ANIMS - sIWInfo.iHoldInteractReactTimerLength: ", sIWInfo.iHoldInteractReactTimerLength, " sIWInfo.iHoldInteractReactAnim: ", sIWInfo.iHoldInteractReactAnim)
			ELSE
				IF HAS_NET_TIMER_EXPIRED(sIWInfo.stHoldInteractReactTimer, sIWInfo.iHoldInteractReactTimerLength)
					SET_HOLD_BUTTON_INTERACT_STATE(HOLD_BUTTON_STATE_REACT_START)
					RESET_NET_TIMER(sIWInfo.stHoldInteractReactTimer)
				ENDIF
			ENDIF
			IF NOT HAS_NET_TIMER_STARTED(sIWInfo.stHoldInteractReactDuration)
				REINIT_NET_TIMER(sIWInfo.stHoldInteractReactDuration)
			ENDIF
		BREAK
		CASE HOLD_BUTTON_STATE_REACT_START
			STOP_SYNC_SCENE(MC_playerBD[iLocalPart].iSynchSceneID)
			IF NOT sIWInfo.bDoneLongHoldReact
			AND HAS_NET_TIMER_EXPIRED(sIWInfo.stHoldInteractReactDuration, 20000)
				//Hand ache/swap
				IF SET_UP_INTERACT_WITH_SYNC_SCENE(tempObj, iObj, sIWInfo.sInteractWith_Dictionary, "REACT_C", 0, FALSE, DEFAULT, INSTANT_BLEND_IN)
					NETWORK_START_SYNCHRONISED_SCENE(MC_playerBD[iLocalPart].iSynchSceneID)
					SET_HOLD_BUTTON_INTERACT_STATE(HOLD_BUTTON_STATE_REACT_LOOP)
					sIWInfo.bDoneLongHoldReact = TRUE
				ENDIF
			ELSE
				IF SET_UP_INTERACT_WITH_SYNC_SCENE(tempObj, iObj, sIWInfo.sInteractWith_Dictionary, GET_PLAYER_LOOP_ANIM_NAME_FOR_HOLD_BUTTON_INTERACT_WITH(sIWInfo.iHoldInteractReactAnim), 0, FALSE, DEFAULT, INSTANT_BLEND_IN)
					NETWORK_START_SYNCHRONISED_SCENE(MC_playerBD[iLocalPart].iSynchSceneID)
					SET_HOLD_BUTTON_INTERACT_STATE(HOLD_BUTTON_STATE_REACT_LOOP)
				ENDIF
			ENDIF
		BREAK
		CASE HOLD_BUTTON_STATE_REACT_LOOP
			IF GET_SYNC_SCENE_PHASE(MC_playerBD[iLocalPart].iSynchSceneID) >= 0.99
				SET_HOLD_BUTTON_INTERACT_STATE(HOLD_BUTTON_STATE_RETURN_LOOP)
			ENDIF
		BREAK
		CASE HOLD_BUTTON_STATE_RETURN_LOOP
			STOP_SYNC_SCENE(MC_playerBD[iLocalPart].iSynchSceneID)
			IF SET_UP_INTERACT_WITH_SYNC_SCENE(tempObj, iObj, sIWInfo.sInteractWith_Dictionary, GET_PLAYER_LOOP_ANIM_NAME_FOR_INTERACT_WITH(iObj), 0, TRUE, DEFAULT, INSTANT_BLEND_IN)
				NETWORK_START_SYNCHRONISED_SCENE( MC_playerBD[iLocalPart].iSynchSceneID)
				SET_HOLD_BUTTON_INTERACT_STATE(HOLD_BUTTON_STATE_WAITING)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

//Main Interact-With Function
PROC PROCESS_INTERACT_WITH_OBJECT_MINIGAME(OBJECT_INDEX tempObj, INT iObj)

	IF NOT IS_INTERACT_WITH_VALID_TO_PROCESS(tempObj, iObj)
		EXIT
	ENDIF
	
	GET_INTERACT_WITH_INFO_AND_LOAD_ANIMS(tempObj, iObj)
	INTERACT_WITH_MISC_PRE_STATE_MACHINE_PROCESSING(iObj, tempObj)
	
	IF MC_serverBD.iObjHackPart[iobj] = -1
	AND MC_playerBD[iLocalPart].iObjHacking = -1
		IF PROCESS_INTERACT_WITH_WAITING_FOR_INPUT(tempObj, iObj)
			START_INTERACT_WITH(tempObj, iObj)
		ENDIF
	ELSE
		// Things to perform while I am the one in the middle of an interaction
		IF MC_serverBD.iObjHackPart[iobj] = iLocalPart
		AND MC_playerBD[iLocalPart].iObjHacking = iObj
			PROCESS_INTERACT_WITH_MISC(tempObj, iObj)
		ENDIF
		
		// Check if we should bail
		BOOL bInteractWith_BailSkipMinigame
		IF SHOULD_INTERACT_WITH_BAIL(tempObj, iObj, bInteractWith_BailSkipMinigame)
			IF bInteractWith_BailSkipMinigame
				SET_INTERACT_WITH_STATE(IW_STATE_FINISHED)
			ELSE
				BAIL_INTERACT_WITH(iObj)
			ENDIF
		ENDIF
		
		// INTERACT-WITH STATE MACHINE BELOW
		SWITCH sIWInfo.eInteractWith_CurrentState
			CASE IW_STATE_INIT
				IF MC_serverBD.iObjHackPart[iObj] = iLocalPart
					IF IS_OBJECT_INTERACT_WITH_A_SYNC_SCENE_ANIM(iObj)
						SET_INTERACT_WITH_STATE(IW_STATE_WALKING)
					ELSE
						SET_INTERACT_WITH_STATE(IW_STATE_TURNING)
					ENDIF
				ELSE
					PRINTLN("[InteractWith] Waiting for MC_serverBD.iObjHackPart[", iObj, "] to be me!")
				ENDIF
			BREAK
			
			CASE IW_STATE_WALKING

				FLOAT fDist
				
				VECTOR vPosNoZ
				vPosNoZ = GET_ENTITY_COORDS(LocalPlayerPed)
				sIWInfo.vInteractWith_TargetPos.z = vPosNoZ.z
					
				fDist = VDIST2(GET_ENTITY_COORDS(LocalPlayerPed), sIWInfo.vInteractWith_TargetPos)
				
				IF fDist > POW(GET_INTERACT_WITH_DISTANCE_ALLOWED(iObj), 2.0)
				AND NOT IS_BIT_SET(sIWInfo.iInteractWith_Bitset, ciInteractWith_Bitset__QuickBlend)
					HEAD_TO_INTERACT_WITH_POSITION(iObj)

				ELSE
					SET_INTERACT_WITH_STATE(IW_STATE_STARTING_ANIMATION)
				ENDIF
			BREAK
			
			CASE IW_STATE_TURNING					
				IF NOT IS_PED_PERFORMING_TASK(LocalPlayerPed, SCRIPT_TASK_ACHIEVE_HEADING)
					CLEAR_PED_TASKS(LocalPlayerPed)
					TASK_ACHIEVE_HEADING(LocalPlayerPed, sIWInfo.fInteractWith_RequiredHeading, 1000)
					PRINTLN("[InteractWith] Tasking ped to turn now!")
				ENDIF
				
				IF (IS_OBJECT_INTERACT_WITH_A_SYNC_SCENE_ANIM(iObj) AND IS_PED_CLOSE_TO_HEADING(LocalPlayerPed, sIWInfo.fInteractWith_RequiredHeading, 60))
				OR (NOT IS_OBJECT_INTERACT_WITH_A_SYNC_SCENE_ANIM(iObj) AND IS_PED_CLOSE_TO_HEADING(LocalPlayerPed, sIWInfo.fInteractWith_RequiredHeading, 10))
					CLEAR_PED_TASKS(LocalPlayerPed)
					SET_INTERACT_WITH_STATE(IW_STATE_STARTING_ANIMATION)
				ENDIF
			BREAK
			
			CASE IW_STATE_STARTING_ANIMATION
				
				IF NOT HAS_NET_TIMER_STARTED(sIWInfo.stInteractWith_BailTimer)
					REINIT_NET_TIMER(sIWInfo.stInteractWith_BailTimer)
					PRINTLN("[InteractWith] IW_STATE_STARTING_ANIMATION | Starting bail timer!")
				ENDIF
				
				IF PROCESS_INTERACT_WITH_STARTING_ANIMATION(tempObj, iObj)
					SET_INTERACT_WITH_STATE(IW_STATE_ANIMATING)
				ENDIF
			BREAK
			
			CASE IW_STATE_ANIMATING
			
				IF HAS_INTERACT_WITH_ANIMATION_FINISHED(iObj, sIWInfo.sInteractWith_Dictionary, sIWInfo.sInteractWith_AnimName)
					PROCESS_INTERACT_WITH_ANIMATION_HAS_FINISHED(tempObj, iObj)
				ENDIF
				
				IF sIWInfo.bInteractWith_ExtraDebug
					PRINTLN("[InteractWith] IW_STATE_ANIMATING | Player is currently animating with object ", iObj)
				ENDIF
			BREAK
			
			CASE IW_STATE_YACHT_CUTSCENE
				BROADCAST_FMMC_MOVE_FMMC_YACHT(iLocalPart)
				
				IF IS_BIT_SET(MC_serverBD.iServerBitSet7, SBBOOL7_YACHT_MOVED_TO_DESTINATION)
					PRINTLN("[InteractWith] IW_STATE_YACHT_CUTSCENE - Setting state to IW_STATE_EXIT_ANIMATION")
					SET_INTERACT_WITH_STATE(IW_STATE_EXIT_ANIMATION)
				ENDIF
			BREAK
			
			CASE IW_STATE_DOWNLOADING
				PROCESS_INTERACT_WITH_DOWNLOADING(tempObj, iObj)
			BREAK
			
			CASE IW_STATE_HOLD_INTERACT
				IF NOT IS_INTERACT_WITH_BUTTON_HELD(iObj)
					STOP_SYNC_SCENE(MC_playerBD[iLocalPart].iSynchSceneID)
					IF SET_UP_INTERACT_WITH_SYNC_SCENE(tempObj, iObj, sIWInfo.sInteractWith_Dictionary, GET_PLAYER_EXIT_ANIM_NAME_FOR_INTERACT_WITH(iObj), 0, FALSE)
						NETWORK_START_SYNCHRONISED_SCENE( MC_playerBD[iLocalPart].iSynchSceneID)
						PRINTLN("[InteractWith] IW_STATE_HOLD_INTERACT - Playing exit for sync scene now")
						SET_INTERACT_WITH_STATE(IW_STATE_EXIT_ANIMATION)
						MC_playerBD_1[iLocalPart].iCurrentHoldInteract = -1
						SET_DOOR_TO_UPDATE_OFF_RULE_FROM_OBJECT(iObj, FALSE, ciUPDATE_DOOR_OFF_RULE_METHOD__HOLDING_BUTTON)
						
						RESET_HOLD_BUTTON_INTERACT_DATA()
					ENDIF
				ELSE
					IF MC_playerBD_1[iLocalPart].iCurrentHoldInteract = -1
						STOP_SYNC_SCENE(MC_playerBD[iLocalPart].iSynchSceneID)
						IF SET_UP_INTERACT_WITH_SYNC_SCENE(tempObj, iObj, sIWInfo.sInteractWith_Dictionary, GET_PLAYER_LOOP_ANIM_NAME_FOR_INTERACT_WITH(iObj), 0, TRUE, DEFAULT, INSTANT_BLEND_IN)
							NETWORK_START_SYNCHRONISED_SCENE( MC_playerBD[iLocalPart].iSynchSceneID)
							PRINTLN("[InteractWith] playing idle scene for sync scene now")
							MC_playerBD_1[iLocalPart].iCurrentHoldInteract = iObj
							PRINTLN("[InteractWith] MC_playerBD_1[iLocalPart].iCurrentHoldInteract now ", MC_playerBD_1[iLocalPart].iCurrentHoldInteract)
							SET_DOOR_TO_UPDATE_OFF_RULE_FROM_OBJECT(iObj, TRUE, ciUPDATE_DOOR_OFF_RULE_METHOD__HOLDING_BUTTON)
							
							RESET_HOLD_BUTTON_INTERACT_DATA()
						ENDIF
					ELSE
						IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__HOLD_BUTTON
							PROCESS_HOLD_BUTTON_INTERACT_ANIMS(tempObj, iObj)
						ENDIF
					ENDIF
				ENDIF
			BREAK

			CASE IW_STATE_CUT_PAINTING
				PROCESS_CUT_PAINTING(iObj, tempObj)
			BREAK
			
			CASE IW_STATE_PLANT_VAULT_DOOR_EXPLOSIVES
				PROCESS_INTERACT_WITH_PLANT_CASINO_VAULT_DOOR_EXPLOSIVES(iObj, tempObj)
			BREAK
			
			CASE IW_STATE_EXIT_ANIMATION
				IF NOT IS_SYNC_SCENE_RUNNING(MC_playerBD[iLocalPart].iSynchSceneID)
					PRINTLN("[InteractWith] IW_STATE_EXIT_ANIMATION | Ending extended exit now")
					SET_INTERACT_WITH_STATE(IW_STATE_FINISHED)
				ELSE
					// Waiting for exit animation to finish
				ENDIF
			BREAK
			
			CASE IW_STATE_FINISHED
				PROCESS_INTERACT_WITH_COMPLETED(iobj)
				SET_INTERACT_WITH_STATE(IW_STATE_IDLE)
			BREAK
		ENDSWITCH
	ENDIF
	
	#IF IS_DEBUG_BUILD
	INTERACT_WITH_DEBUG(tempObj, iObj)
	#ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
FUNC STRING DEBUG_GET_HANDHELD_DRILL_MINIGAME_STATE_NAME(HANDHELD_DRILL_MINIGAME_STATE eState)
	SWITCH eState
		CASE HANDHELD_DRILL_MINIGAME_STATE_IDLE					RETURN "HANDHELD_DRILL_MINIGAME_STATE_IDLE"
		CASE HANDHELD_DRILL_MINIGAME_STATE_INIT					RETURN "HANDHELD_DRILL_MINIGAME_STATE_INIT"
		CASE HANDHELD_DRILL_MINIGAME_STATE_WAITING				RETURN "HANDHELD_DRILL_MINIGAME_STATE_WAITING"
		CASE HANDHELD_DRILL_MINIGAME_DRILL_POSITIONING			RETURN "HANDHELD_DRILL_MINIGAME_DRILL_POSITIONING"
		CASE HANDHELD_DRILL_MINIGAME_DRILL_READY_TO_DRILL		RETURN "HANDHELD_DRILL_MINIGAME_DRILL_READY_TO_DRILL"
		CASE HANDHELD_DRILL_MINIGAME_DRILL_FINISHED				RETURN "HANDHELD_DRILL_MINIGAME_DRILL_FINISHED"
		CASE HANDHELD_DRILL_MINIGAME_DRILL_CLEANUP				RETURN "HANDHELD_DRILL_MINIGAME_DRILL_CLEANUP"
	ENDSWITCH
	RETURN "INVALID"
ENDFUNC
#ENDIF

PROC SET_HANDHELD_DRILL_MINIGAME_STATE(HANDHELD_DRILL_MINIGAME_STATE &eOldState, HANDHELD_DRILL_MINIGAME_STATE eNewState)
	PRINTLN("[LM][HandHeldDrill] - SET_HANDHELD_DRILL_MINIGAME_STATE - eOldState: ", DEBUG_GET_HANDHELD_DRILL_MINIGAME_STATE_NAME(eOldState), " eNewState: ", DEBUG_GET_HANDHELD_DRILL_MINIGAME_STATE_NAME(eNewState))
	eOldState = eNewState	
ENDPROC

#IF IS_DEBUG_BUILD
FUNC STRING DEBUG_GET_HANDHELD_DRILL_MINIGAME_DRILLING_STATE_NAME(HANDHELD_DRILL_MINIGAME_DRILLING_STATE eState)
	SWITCH eState
		CASE HANDHELD_DRILL_MINIGAME_DRILLING_STATE_WAITING			RETURN "HANDHELD_DRILL_MINIGAME_DRILLING_STATE_WAITING"
		CASE HANDHELD_DRILL_MINIGAME_DRILLING_STATE_SELECTING		RETURN "HANDHELD_DRILL_MINIGAME_DRILLING_STATE_SELECTING"
		CASE HANDHELD_DRILL_MINIGAME_DRILLING_STATE_DRILLING		RETURN "HANDHELD_DRILL_MINIGAME_DRILLING_STATE_DRILLING"	
		CASE HANDHELD_DRILL_MINIGAME_DRILLING_STATE_FINISHING		RETURN "HANDHELD_DRILL_MINIGAME_DRILLING_STATE_FINISHING"	
		CASE HANDHELD_DRILL_MINIGAME_DRILLING_STATE_EXITTING		RETURN "HANDHELD_DRILL_MINIGAME_DRILLING_STATE_EXITTING"		
		CASE HANDHELD_DRILL_MINIGAME_DRILLING_STATE_CLEANUP			RETURN "HANDHELD_DRILL_MINIGAME_DRILLING_STATE_CLEANUP"	
	ENDSWITCH
	RETURN "INVALID"
ENDFUNC
#ENDIF

PROC SET_HANDHELD_DRILL_MINIGAME_DRILLING_STATE(HANDHELD_DRILL_MINIGAME_DRILLING_STATE &eOldState, HANDHELD_DRILL_MINIGAME_DRILLING_STATE eNewState)
	PRINTLN("[LM][HandHeldDrill] - SET_HANDHELD_DRILL_MINIGAME_DRILLING_STATE - eOldState: ", DEBUG_GET_HANDHELD_DRILL_MINIGAME_DRILLING_STATE_NAME(eOldState), " eNewState: ", DEBUG_GET_HANDHELD_DRILL_MINIGAME_DRILLING_STATE_NAME(eNewState))
	eOldState = eNewState
ENDPROC

FUNC BOOL IS_HANDHELD_DRILL_MINIGAME_CABINET_A_LOCKBOX(INT iIndex, INT iConfiguration)
	#IF FEATURE_CASINO_HEIST
	IF GET_HANDHELD_DRILL_MINIGAME_CABINET_TYPE(iIndex, iConfiguration) = HANDHELD_DRILL_MINIGAME_CABINET_TYPE_SAFETY_DEPOSIT_BOX
	OR (GET_HANDHELD_DRILL_MINIGAME_CABINET_TYPE(iIndex, iConfiguration) = HANDHELD_DRILL_MINIGAME_CABINET_TYPE_PAINTING AND (g_sCasinoHeistMissionConfigData.eTarget != CASINO_HEIST_TARGET_TYPE__ART AND GET_PLAYER_CASINO_HEIST_TARGET(GB_GET_LOCAL_PLAYER_GANG_BOSS()) != CASINO_HEIST_TARGET_TYPE__ART))
		RETURN TRUE
	ENDIF
	#ENDIF
	RETURN FALSE
ENDFUNC
// ################################ HandHeld Drill Minigame Initialization - START ################################
FUNC FLOAT GET_PED_REQUIRED_HEADING_FOR_LOCKBOX_CABINET_IN_HANDHELD_DRILL_MINIGAME_WITH_INDEX(INT iIndex)	
	RETURN (GET_HEADING_FOR_LOCKBOX_CABINET_IN_HANDHELD_DRILL_MINIGAME_WITH_INDEX(iIndex) - 180)
ENDFUNC

FUNC TEXT_LABEL_63 GET_HANDHELD_MINIGAME_ANIM_DICT(INT iPattern, INT iLockbox)
	TEXT_LABEL_63 tlAnimDict_63
	tlAnimDict_63 = "ANIM_HEIST@HS3F@IG10_LOCKBOX_DRILL@PATTERN_0"
	tlAnimDict_63 += iPattern
	tlAnimDict_63 += "@LOCKBOX_0"
	tlAnimDict_63 += iLockbox
	tlAnimDict_63 += "@MALE@"
	RETURN tlAnimDict_63
ENDFUNC

FUNC BOOL REQUEST_HANDHELD_MINIGAME_ANIM_DICT(INT iPattern, INT iLockbox)
	TEXT_LABEL_63 tlAnimDict_63 = GET_HANDHELD_MINIGAME_ANIM_DICT(iPattern, iLockbox)
	REQUEST_ANIM_DICT(tlAnimDict_63)
	IF NOT HAS_ANIM_DICT_LOADED(tlAnimDict_63)
		PRINTLN("[LM][HandHeldDrill] - REQUEST_HANDHELD_DRILL_MINIGAME_ASSETS - Waiting for strAnimDict: ", tlAnimDict_63)
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

PROC CLEANUP_HANDHELD_MINIGAME_ANIM_DICTS()
	TEXT_LABEL_63 tlAnimDict_63
	INT iPattern, iLockbox
	FOR iPattern = 0 TO ciHANDHELD_DRILL_MINIGAME_MAX_PATTERNS-1
		FOR iLockbox = 0 TO ciMAX_NUMBER_OF_HANDHELD_DRILL_LOCKBOXES-1
			tlAnimDict_63 = GET_HANDHELD_MINIGAME_ANIM_DICT(iPattern+1, iLockbox+1)
			PRINTLN("[LM][HandHeldDrill] - CLEANUP_HANDHELD_MINIGAME_ANIM_DICTS - Removing Anim Dict strAnimDict: ", tlAnimDict_63)
			REMOVE_ANIM_DICT(tlAnimDict_63)
		ENDFOR
	ENDFOR
ENDPROC
FUNC MODEL_NAMES GET_CABINET_MODEL_FOR_PAINTING()
	MODEL_NAMES mnCabinet = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_CH_Sec_Cabinet_04a"))
	RETURN mnCabinet
ENDFUNC
FUNC BOOL REQUEST_HANDHELD_DRILL_MINIGAME_ASSETS()
	
	INT iPattern, iLockbox
	FOR iPattern = 0 TO ciHANDHELD_DRILL_MINIGAME_MAX_PATTERNS-1
		FOR iLockbox = 0 TO ciMAX_NUMBER_OF_HANDHELD_DRILL_LOCKBOXES-1
			IF NOT REQUEST_HANDHELD_MINIGAME_ANIM_DICT(iPattern+1, iLockbox+1)
				RETURN FALSE
			ENDIF
		ENDFOR
	ENDFOR
		
	MODEL_NAMES mnDrill = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Vault_Drill_01a"))	
	REQUEST_MODEL(mnDrill)	
	IF NOT HAS_MODEL_LOADED(mnDrill)
		PRINTLN("[LM][HandHeldDrill] - REQUEST_HANDHELD_DRILL_MINIGAME_ASSETS - Waiting for mnDrill.")
		RETURN FALSE
	ENDIF
	
	MODEL_NAMES mnCabinet
	INT i, ii // ADD const
	FOR i = 0 TO ciHANDHELD_DRILL_MINIGAME_MAX_PATTERNS-1
		FOR ii = 0 TO ciHANDHELD_DRILL_MINIGAME_MAX_POSITION-1
			mnCabinet = GET_HANDHELD_DRILL_MINIGAME_CABINET_MODEL_FROM_INT(i, ii)
			REQUEST_MODEL(mnCabinet)
		ENDFOR
	ENDFOR
	FOR i = 0 TO ciHANDHELD_DRILL_MINIGAME_MAX_PATTERNS-1
		FOR ii = 0 TO ciHANDHELD_DRILL_MINIGAME_MAX_POSITION-1
			mnCabinet = GET_HANDHELD_DRILL_MINIGAME_CABINET_MODEL_FROM_INT(i, ii)
			IF NOT HAS_MODEL_LOADED(mnCabinet)
				PRINTLN("[LM][HandHeldDrill] - REQUEST_HANDHELD_DRILL_MINIGAME_ASSETS - Waiting for mnCabinet: ", GET_MODEL_NAME_FOR_DEBUG(mnCabinet))
				RETURN FALSE
			ENDIF	
		ENDFOR
	ENDFOR
	
	REQUEST_MODEL(GET_CABINET_MODEL_FOR_PAINTING())
	IF NOT HAS_MODEL_LOADED(GET_CABINET_MODEL_FOR_PAINTING())
		PRINTLN("[LM][HandHeldDrill] - REQUEST_HANDHELD_DRILL_MINIGAME_ASSETS - Waiting for GET_CABINET_MODEL_FOR_PAINTING.")
		RETURN FALSE
	ENDIF
	
	REQUEST_NAMED_PTFX_ASSET("scr_ch_finale")
	IF NOT HAS_NAMED_PTFX_ASSET_LOADED("scr_ch_finale")
		PRINTLN("[LM][HandHeldDrill] - REQUEST_HANDHELD_DRILL_MINIGAME_ASSETS - Waiting for HAS_NAMED_PTFX_ASSET_LOADED('scr_ch_finale')")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL DOES_CABINET_REQUIRE_FORCE_LEFT_HAND_SIDE(INT iCabinet)
	SWITCH iCabinet
		CASE 5	RETURN TRUE
		CASE 11	RETURN TRUE
		CASE 17	RETURN TRUE
		CASE 23	RETURN TRUE
		CASE 29	RETURN TRUE
		CASE 35	RETURN TRUE
		CASE 41	RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC
FUNC BOOL DOES_CABINET_REQUIRE_FORCE_RIGHT_HAND_SIDE(INT iCabinet)
	SWITCH iCabinet
		CASE 0	RETURN TRUE
		CASE 6	RETURN TRUE
		CASE 12	RETURN TRUE
		CASE 18	RETURN TRUE
		CASE 24	RETURN TRUE
		CASE 30	RETURN TRUE
		CASE 36	RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC
PROC SET_UP_PAINTING_OBJECT_FROM_PAINTING_CABINET_INDEX_CONTROLLER(INT iProp, VECTOR vPropCoord, VECTOR vPropRot, INT iCabinet)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_CH_Sec_Cabinet_02a"))
		INT iObjLoop
		FOR iObjLoop = 0 to FMMC_MAX_NUM_OBJECTS-1
			IF IS_THIS_MODEL_A_CASINO_PAINTING(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjLoop].mn)
			AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjLoop].iPaintingIndex != -1
				IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropPaintingIndex = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjLoop].iPaintingIndex					
					
					OBJECT_INDEX oiIndex = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iObjLoop])
					
					IF DOES_ENTITY_EXIST(oiIndex)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(oiIndex)						
							VECTOR vPaintingOffset
							vPaintingOffset = <<0.0, 0.4, 1.15>>
							g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjLoop].vPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPropCoord, vPropRot.z, vPaintingOffset)
							g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjLoop].vRot = vPropRot
							PRINTLN("SET_UP_PAINTING_OBJECT_FROM_PAINTING_CABINET_INDEX_CONTROLLER - Moving object: ", iObjLoop, "to ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjLoop].vPos.x, ", ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjLoop].vPos.y, ", ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjLoop].vPos.z)
					
							SET_ENTITY_COORDS(oiIndex, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjLoop].vPos)
							SET_ENTITY_HEADING(oiIndex, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjLoop].vRot.z)
						ENDIF
					ENDIF
					
					IF DOES_CABINET_REQUIRE_FORCE_RIGHT_HAND_SIDE(iCabinet)
						SET_BIT(iObjPaintingForceRightHandBS, iObjLoop)
						PRINTLN("SET_UP_PAINTING_OBJECT_FROM_PAINTING_CABINET_INDEX_CONTROLLER - Setting iObjLoop: ", iObjLoop, "to be Right Hand Side Forced for Animations.")
					ELIF DOES_CABINET_REQUIRE_FORCE_LEFT_HAND_SIDE(iCabinet)					
						SET_BIT(iObjPaintingForceLeftHandBS, iObjLoop)
						PRINTLN("SET_UP_PAINTING_OBJECT_FROM_PAINTING_CABINET_INDEX_CONTROLLER - Setting iObjLoop: ", iObjLoop, "to be Right Hand Side Forced for Animations.")
					ENDIF
					
					BREAKLOOP
				ENDIF
			ENDIF	 
		ENDFOR		
	ENDIF
	
ENDPROC
FUNC BOOL CREATE_ASSETS_FOR_HANDHELD_DRILL_MINIGAME(STRUCT_HANDHELD_DRILL_MINI_GAME &sHandHeldDrillPassedIn)

	// [LM] NOTE ********** REVISIT THE ADVANTAGES / DISADVANTAGES OF INTEGRATING THIS INTO REGULAR STARTUP ROUTINES **********
	// Right now it seems cleaner to do it here else all the asset requesting needs to be broken out of the minigame initialization states and into the MC initialization.
	// The majority of this minigame is reusable with the exception of ServerBD being used and a couple of MC globals iNumberOfDynoProps. Also one or two cases of iLocalPart being used and two events. These could easily be made generic or fired externally.
	// If we get those variables passed through by reference this game can be reused in different scripts.
	#IF FEATURE_CASINO_HEIST
	INT iStartingDynoObjectIndex = (g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps - ciMAX_NUMBER_OF_HANDHELD_DRILL_LOCKBOX_CABINETS_DYNAMIC) //((FMMC_MAX_NUM_DYNOPROPS-1)-ciMAX_NUMBER_OF_HANDHELD_DRILL_LOCKBOX_CABINETS_DYNAMIC)
	INT iDynamicIndex
	MODEL_NAMES mnCabinet	
	INT i
	INT iPropPaintingCabinetIndexes[6]
	INT iCurrentPainting = 0
	INT iPattern
	INT iSubPattern
	
	IF g_sCasinoHeistMissionConfigData.eTarget = CASINO_HEIST_TARGET_TYPE__ART
	OR GET_PLAYER_CASINO_HEIST_TARGET(GB_GET_LOCAL_PLAYER_GANG_BOSS()) = CASINO_HEIST_TARGET_TYPE__ART
		PRINTLN("[LM][HandHeldDrill] - CREATE_ASSETS_FOR_HANDHELD_DRILL_MINIGAME - Target Type is Art.")
	ELSE
		PRINTLN("[LM][HandHeldDrill] - CREATE_ASSETS_FOR_HANDHELD_DRILL_MINIGAME - Target Type is NOT Art.")
	ENDIF
	
	// Cache the prop indexes which are flagged as a painting.
	IF g_sCasinoHeistMissionConfigData.eTarget = CASINO_HEIST_TARGET_TYPE__ART
	OR GET_PLAYER_CASINO_HEIST_TARGET(GB_GET_LOCAL_PLAYER_GANG_BOSS()) = CASINO_HEIST_TARGET_TYPE__ART
		FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfProps-1
			IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iPropPaintingIndex > -1
			AND iCurrentPainting < 6
				iPropPaintingCabinetIndexes[iCurrentPainting] = i
				iCurrentPainting++
			ENDIF
		ENDFOR
	ENDIF
	iCurrentPainting = 0
	
	FOR i = 0 TO ciMAX_NUMBER_OF_HANDHELD_DRILL_LOCKBOX_CABINETS_TOTAL-1
		
		IF iDynamicIndex < ciMAX_NUMBER_OF_HANDHELD_DRILL_LOCKBOX_CABINETS_DYNAMIC
			IF DOES_CABINET_REQUIRE_FORCE_LEFT_HAND_SIDE(i)
				iPattern = ciHANDHELD_DRILL_MINIGAME_PATTERN_1
				iSubPattern = ciHANDHELD_DRILL_MINIGAME_POSITION_2
				sHandHeldDrillPassedIn.iLockboxCabinetPattern[iDynamicIndex] = iPattern
				sHandHeldDrillPassedIn.iLockboxCabinetSubPattern[iDynamicIndex] = iSubPattern
			ELSE
				iPattern	= sHandHeldDrillPassedIn.iLockboxCabinetPattern[iDynamicIndex]
				iSubPattern = sHandHeldDrillPassedIn.iLockboxCabinetSubPattern[iDynamicIndex]
			ENDIF
		ENDIF
		
		mnCabinet = DUMMY_MODEL_FOR_SCRIPT
		
		IF IS_HANDHELD_DRILL_MINIGAME_CABINET_A_LOCKBOX(i, sHandHeldDrillPassedIn.iLockboxConfiguration)
			IF DOES_CABINET_REQUIRE_FORCE_LEFT_HAND_SIDE(i)
				mnCabinet = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_CH_Sec_Cabinet_01f"))
				PRINTLN("[LM][HandHeldDrill] - CREATE_ASSETS_FOR_HANDHELD_DRILL_MINIGAME - iCabinet: (",i,"/42) iDynoProp: ", iStartingDynoObjectIndex, " mnCabinet: ", GET_MODEL_NAME_FOR_DEBUG(mnCabinet), " Forcing to be a left hand side lockbox.")
			ELSE
				mnCabinet = GET_HANDHELD_DRILL_MINIGAME_CABINET_MODEL_FROM_INT(iPattern, iSubPattern)
				PRINTLN("[LM][HandHeldDrill] - CREATE_ASSETS_FOR_HANDHELD_DRILL_MINIGAME - iCabinet: (",i,"/42) iDynoProp: ", iStartingDynoObjectIndex, " mnCabinet: ", GET_MODEL_NAME_FOR_DEBUG(mnCabinet), " Drillable Lockbox Cabinet with Pattern: ", sHandHeldDrillPassedIn.iLockboxCabinetPattern[iDynamicIndex], " Subpattern: ", sHandHeldDrillPassedIn.iLockboxCabinetSubPattern[iDynamicIndex])
			ENDIF
		ELSE
			IF (GET_HANDHELD_DRILL_MINIGAME_CABINET_TYPE(i, sHandHeldDrillPassedIn.iLockboxConfiguration) = HANDHELD_DRILL_MINIGAME_CABINET_TYPE_PAINTING
			AND (g_sCasinoHeistMissionConfigData.eTarget = CASINO_HEIST_TARGET_TYPE__ART OR GET_PLAYER_CASINO_HEIST_TARGET(GB_GET_LOCAL_PLAYER_GANG_BOSS()) = CASINO_HEIST_TARGET_TYPE__ART))
				PRINTLN("[LM][HandHeldDrill] - CREATE_ASSETS_FOR_HANDHELD_DRILL_MINIGAME - iCabinet: (",i,"/42) iDynoProp: ", iStartingDynoObjectIndex, " Should have a placed painting down.")
			ELSE 
				PRINTLN("[LM][HandHeldDrill] - CREATE_ASSETS_FOR_HANDHELD_DRILL_MINIGAME - iCabinet: (",i,"/42) iDynoProp: ", iStartingDynoObjectIndex, " Is supposed to be static from the entity set.")
			ENDIF
		ENDIF
		
		IF mnCabinet != DUMMY_MODEL_FOR_SCRIPT
			IF HAS_MODEL_LOADED(mnCabinet)
				IF bIsLocalPlayerHost
					IF CREATE_NET_OBJ(MC_serverBD_1.sFMMC_SBD.niDynoProps[iStartingDynoObjectIndex], mnCabinet, GET_POSITION_FOR_LOCKBOX_CABINET_IN_HANDHELD_DRILL_MINIGAME_WITH_INDEX(i, iPattern, iSubPattern), TRUE, TRUE)
						IF NOT DOES_ENTITY_EXIST(sHandHeldDrillPassedIn.oiLockboxes[iDynamicIndex])
							sHandHeldDrillPassedIn.oiLockboxes[iDynamicIndex] = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niDynoProps[iStartingDynoObjectIndex])
							SET_ENTITY_HEADING(sHandHeldDrillPassedIn.oiLockboxes[iDynamicIndex], GET_HEADING_FOR_LOCKBOX_CABINET_IN_HANDHELD_DRILL_MINIGAME_WITH_INDEX(i)-180)
							PRINTLN("[LM][HandHeldDrill] - CREATE_ASSETS_FOR_HANDHELD_DRILL_MINIGAME - iCabinet: (",i,"/42) CREATING iDynoProp: ", iStartingDynoObjectIndex, " DrillCabient: ", iDynamicIndex)						
							iDynamicIndex++
							iStartingDynoObjectIndex++
						ENDIF
					ELSE
						RETURN FALSE
					ENDIF
				ELSE
					IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niDynoProps[iStartingDynoObjectIndex])
						PRINTLN("[LM][HandHeldDrill] - CREATE_ASSETS_FOR_HANDHELD_DRILL_MINIGAME - iCabinet: (",i,"/42) ASSIGNING iDynoProp: ", iStartingDynoObjectIndex, " DrillCabient: ", iDynamicIndex, " Assigning Cabinet from host data.")
						sHandHeldDrillPassedIn.oiLockboxes[iDynamicIndex] = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niDynoProps[iStartingDynoObjectIndex])
						iDynamicIndex++
						iStartingDynoObjectIndex++
					ELSE
						PRINTLN("[LM][HandHeldDrill] - CREATE_ASSETS_FOR_HANDHELD_DRILL_MINIGAME - iCabinet: (",i,"/42) iDynoProp: ", iStartingDynoObjectIndex, " DrillCabient: ", iDynamicIndex, " Cabinet does not exist, waiting for Host to create it.")
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			// Move Paintings and their Cabinets to the correct locations.				
			IF (GET_HANDHELD_DRILL_MINIGAME_CABINET_TYPE(i, sHandHeldDrillPassedIn.iLockboxConfiguration) = HANDHELD_DRILL_MINIGAME_CABINET_TYPE_PAINTING
			AND (g_sCasinoHeistMissionConfigData.eTarget = CASINO_HEIST_TARGET_TYPE__ART OR GET_PLAYER_CASINO_HEIST_TARGET(GB_GET_LOCAL_PLAYER_GANG_BOSS()) = CASINO_HEIST_TARGET_TYPE__ART))
				INT iActualPropIndex = iPropPaintingCabinetIndexes[iCurrentPainting]
				IF iActualPropIndex > -1					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iActualPropIndex].iPropBitSet2, ciFMMC_PROP2_Reposition_Based_On_Painting_Index)
						IF DOES_ENTITY_EXIST(oiProps[iActualPropIndex])
							PRINTLN("[LM][HandHeldDrill] - CREATE_ASSETS_FOR_HANDHELD_DRILL_MINIGAME - iCabinet: (",i,"/42) iActualPropIndex: ", iActualPropIndex, " iCurrentPainting: ", iCurrentPainting, " Painting exists. Moving it into position.")
							SET_UP_PROP_PAINTING_AS_A_LINKED_CABINET(oiProps[iActualPropIndex], iActualPropIndex, i)
							SET_UP_PAINTING_OBJECT_FROM_PAINTING_CABINET_INDEX_CONTROLLER(iActualPropIndex, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iActualPropIndex].vPos, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iActualPropIndex].vRot, i)
							iCurrentPainting++
						ELSE
							PRINTLN("[LM][HandHeldDrill] - CREATE_ASSETS_FOR_HANDHELD_DRILL_MINIGAME - iCabinet: (",i,"/42) iActualPropIndex: ", iActualPropIndex, " iCurrentPainting: ", iCurrentPainting, " Painting does not exist")
						ENDIF
					ELSE
						PRINTLN("[LM][HandHeldDrill] - CREATE_ASSETS_FOR_HANDHELD_DRILL_MINIGAME - iCabinet: (",i,"/42) iActualPropIndex: ", iActualPropIndex, " iCurrentPainting: ", iCurrentPainting, " Painting not set up to use Reposition.")
					ENDIF
				ENDIF					
			ENDIF
		ENDIF
	ENDFOR
	#ENDIF
	
	RETURN TRUE
	
ENDFUNC
PROC PROCESS_PRE_GAME_HANDHELD_DRILL_MINIGAME()
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_EnableHandheldDrillMinigame)
		EXIT
	ENDIF
	// This is done so that we reserve the correct number of Dyno Props during PROCESS_PRE_GAME
	IF (g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps + ciMAX_NUMBER_OF_HANDHELD_DRILL_LOCKBOX_CABINETS_DYNAMIC) < FMMC_MAX_NUM_DYNOPROPS
		g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps += ciMAX_NUMBER_OF_HANDHELD_DRILL_LOCKBOX_CABINETS_DYNAMIC
		PRINTLN("[LM][HandHeldDrill] - PROCESS_PRE_GAME_HANDHELD_DRILL_MINIGAME - iNumberOfDynoProps = ", g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps)
	ELSE
		ASSERTLN("[LM][HandHeldDrill] - PROCESS_PRE_GAME_HANDHELD_DRILL_MINIGAME - TOO MANY DYNO PROPS HAVE BEEN PLACED!")
		PRINTLN("[LM][HandHeldDrill] - PROCESS_PRE_GAME_HANDHELD_DRILL_MINIGAME - TOO MANY DYNO PROPS HAVE BEEN PLACED! Difference: ", (g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps+ciMAX_NUMBER_OF_HANDHELD_DRILL_LOCKBOX_CABINETS_DYNAMIC)-FMMC_MAX_NUM_DYNOPROPS)
	ENDIF
ENDPROC
// ################################ HandHeld Drill Minigame Initialization - END ################################

// Should move from WAITING state.
FUNC BOOL IS_PED_ABLE_TO_BEGIN_HANDHELD_DRILL_MINIGAME(PED_INDEX pedIndex)
	IF IS_PED_INJURED(pedIndex)
	OR IS_PED_JUMPING(pedIndex)
	OR IS_PED_SHOOTING(pedIndex)
	OR IS_PED_PERFORMING_MELEE_ACTION(pedIndex)
	OR fPoisonGasFillBuildup >= 0.1
		RETURN FALSE
	ENDIF
		
	RETURN TRUE
ENDFUNC
FUNC BOOL HAS_TRIGGERED_TO_BEGIN_HANDHELD_DRILL_MINIGAME()
	IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)		
		PRINTLN("[LM][HandHeldDrill] - HAS_TRIGGERED_TO_BEGIN_HANDHELD_DRILL_MINIGAME - Pressed Input to start minigame.")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL IS_HANDHELD_DRILL_MINIGAME_LEAVE_DRILLING_JUST_PRESSED()
	IF IS_PC_VERSION()
	AND IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
		IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)			
			PRINTLN("[LM][HandHeldDrill] - IS_HANDHELD_DRILL_MINIGAME_LEAVE_DRILLING_JUST_PRESSED - Pressed Input to Leave Drilling minigame.")
			RETURN TRUE
		ENDIF
	ELSE
		IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_RELOAD)			
			PRINTLN("[LM][HandHeldDrill] - IS_HANDHELD_DRILL_MINIGAME_LEAVE_DRILLING_JUST_PRESSED - Pressed Input to Leave Drilling minigame.")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL IS_HANDHELD_DRILL_MINIGAME_DRILL_BUTTON_BEING_HELD()
	IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_ATTACK)
		//PRINTLN("[LM][HandHeldDrill] - IS_HANDHELD_DRILL_MINIGAME_DRILL_BUTTON_BEING_HELD - Holding down drilling button.")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CHECK_HANDHELD_DRILL_MINIGAME_LOCATION_DISTANCE(PED_INDEX pedIndex, INT iLockboxCabinet)
	
	VECTOR vLockBoxPos = GET_POSITION_FOR_LOCKBOX_CABINET_IN_HANDHELD_DRILL_MINIGAME_WITH_INDEX(iLockboxCabinet)
	VECTOR vPos1, vPos2	
	vPos1 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vLockBoxPos, GET_HEADING_FOR_LOCKBOX_CABINET_IN_HANDHELD_DRILL_MINIGAME_WITH_INDEX(iLockboxCabinet), <<0.9, 1.1, 0.0>>)
	vPos2 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vLockBoxPos, GET_HEADING_FOR_LOCKBOX_CABINET_IN_HANDHELD_DRILL_MINIGAME_WITH_INDEX(iLockboxCabinet), <<-0.9, 1.1, 0.0>>)
	vPos2.z += 1.5
	//DRAW_ANGLED_AREA(vPos1, vPos2, 2.0, 200, 200, 0, 250)
	
	IF IS_ENTITY_IN_ANGLED_AREA(pedIndex, vPos1, vPos2, 2.0)
		PRINTLN("[LM][HandHeldDrill] - CHECK_HANDHELD_DRILL_MINIGAME_LOCATION_DISTANCE - iLockboxCabinet: ", iLockboxCabinet, " inside the angled area for activation.")
		RETURN TRUE
	ELSE
		PRINTLN("[LM][HandHeldDrill] - CHECK_HANDHELD_DRILL_MINIGAME_LOCATION_DISTANCE - iLockboxCabinet: ", iLockboxCabinet, " Not inside the angled area for activation.")
	ENDIF
	RETURN FALSE
ENDFUNC
FUNC BOOL CHECK_HANDHELD_DRILL_MINIGAME_HEADING(PED_INDEX pedIndex, INT iLockboxCabinet)	
	
	// Ignoring the actual checks below because there were bugs complaining about having to face the lockbox cabinet.
	RETURN TRUE
	
	FLOAT fPedHead = GET_ENTITY_HEADING(pedIndex)
	FLOAT fPedDesiredHead = GET_PED_REQUIRED_HEADING_FOR_LOCKBOX_CABINET_IN_HANDHELD_DRILL_MINIGAME_WITH_INDEX(iLockboxCabinet)
	
	fPedHead = GET_NORMALISED_HEADING(fPedHead)
	fPedDesiredHead = GET_NORMALISED_HEADING(fPedDesiredHead)	
	
	IF fPedHead > (fPedDesiredHead-25)
	AND fPedHead < (fPedDesiredHead+25)
		PRINTLN("[LM][HandHeldDrill] - CHECK_HANDHELD_DRILL_MINIGAME_HEADING - iLockboxCabinet: ", iLockboxCabinet, " fPedHead: ", fPedHead, " we are in range of Deposit Box Cabinet fPedDesiredHead: ", fPedDesiredHead, "(+- 8)")
		RETURN TRUE
	ELSE
		PRINTLN("[LM][HandHeldDrill] - CHECK_HANDHELD_DRILL_MINIGAME_HEADING - iLockboxCabinet: ", iLockboxCabinet, " fPedHead: ", fPedHead, " we are NOT in range of Deposit Box Cabinet fPedDesiredHead: ", fPedDesiredHead, "(+- 8)")
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCATION_FOR_HANDHELD_DRILL_MINIGAME_VALID(PED_INDEX pedIndex, INT &iLockbox, INT &iLockboxDynamic, INT iConfiguration)
	INt iDynamicIndex = -1
	INT i = 0
	FOR i = 0 TO ciMAX_NUMBER_OF_HANDHELD_DRILL_LOCKBOX_CABINETS_TOTAL-1
		IF IS_HANDHELD_DRILL_MINIGAME_CABINET_A_LOCKBOX(i, iConfiguration)
			iDynamicIndex++
			
			IF NOT IS_BIT_SET(MC_ServerBD_2.iSafetyDepositCabinetOccupiedUsedBS, iDynamicIndex)
			AND CHECK_HANDHELD_DRILL_MINIGAME_LOCATION_DISTANCE(pedIndex, i)
			AND CHECK_HANDHELD_DRILL_MINIGAME_HEADING(pedIndex, i)
				iLockbox = i
				iLockboxDynamic = iDynamicIndex
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	
	iLockbox = -1
	iLockboxDynamic = -1
	
	RETURN FALSE	
ENDFUNC

FUNC BOOL PROCESS_HANDHELD_DRILL_MINIGAME_PED_POSITIONING(STRUCT_HANDHELD_DRILL_MINI_GAME &sHandHeldDrillPassedIn)
	
	INT iPattern	= sHandHeldDrillPassedIn.iLockboxCabinetPattern[sHandHeldDrillPassedIn.iLockboxDynamicCabinetSelected]	
	INT iSubPattern = sHandHeldDrillPassedIn.iLockboxCabinetSubPattern[sHandHeldDrillPassedIn.iLockboxDynamicCabinetSelected]
	
	PED_INDEX pedDrill = sHandHeldDrillPassedIn.pedDrill
	INT iIndex = sHandHeldDrillPassedIn.iLockboxCabinetSelected
	VECTOR vPedDesiredPosition
	FLOAT fPedDesiredHeading	
	vPedDesiredPosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_POSITION_FOR_LOCKBOX_CABINET_IN_HANDHELD_DRILL_MINIGAME_WITH_INDEX(iIndex, iPattern, iSubPattern), GET_HEADING_FOR_LOCKBOX_CABINET_IN_HANDHELD_DRILL_MINIGAME_WITH_INDEX(iIndex), <<0.0, 1.0, 0.0>>)
	fPedDesiredHeading = GET_PED_REQUIRED_HEADING_FOR_LOCKBOX_CABINET_IN_HANDHELD_DRILL_MINIGAME_WITH_INDEX(iIndex)
	
	DISABLE_CONTROLS()
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD, TRUE)	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR, TRUE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD, TRUE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR, TRUE)
	
	IF IS_BIT_SET(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_StartedPositioningTask)
		IF NOT IS_PED_PERFORMING_TASK(pedDrill, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) //fDistance < POW(2, 2.0)
		AND HAS_NET_TIMER_EXPIRED_READ_ONLY(sHandHeldDrillPassedIn.tdMinigamePositionTimer, 500)
			CLEAR_PED_TASKS(pedDrill)
			PRINTLN("[LM][HandHeldDrill] - PROCESS_HANDHELD_DRILL_MINIGAME_PED_POSITIONING - Finished task to walk the cabinet position. vPlayerDesiredPosition: ",
				vPedDesiredPosition, " fPlayerDesiredHeading: ", fPedDesiredHeading) //, " vpedPos: ", vpedPos, " fPedHead: ", fPedHead)
			RETURN TRUE
		ENDIF
	ELSE
		IF NOT IS_PED_PERFORMING_TASK(pedDrill, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
			SET_BIT(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_StartedPositioningTask)
			RESET_NET_TIMER(sHandHeldDrillPassedIn.tdMinigamePositionTimer)
			START_NET_TIMER(sHandHeldDrillPassedIn.tdMinigamePositionTimer)
			CLEAR_PED_TASKS(pedDrill)			
			TASK_FOLLOW_NAV_MESH_TO_COORD(pedDrill, vPedDesiredPosition, PEDMOVEBLENDRATIO_WALK, DEFAULT, DEFAULT, ENAV_STOP_EXACTLY, fPedDesiredHeading)
			PRINTLN("[LM][HandHeldDrill] - PROCESS_HANDHELD_DRILL_MINIGAME_PED_POSITIONING - Tasking to walk to the cabinet position. vPlayerDesiredPosition: ",
				vPedDesiredPosition, " fPlayerDesiredHeading: ", fPedDesiredHeading) //, " vpedPos: ", vpedPos, " fPedHead: ", fPedHead)				
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_HANDHELD_DRILL_MINIGAME_PED_HAVE_A_DRILL()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_GiveAllPlayersHandheldDrill)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_HANDHELD_DRILL_MINIGAME_LOCKBOX_COMPLETE(STRUCT_HANDHELD_DRILL_MINI_GAME &sHandHeldDrillPassedIn, INT iDynamicCabinet, INT iLockbox)
	IF sHandHeldDrillPassedIn.fLockboxCompletionPercent[iDynamicCabinet][iLockbox] >= g_FMMC_STRUCT.fHandheldDrillingMinigameSafetyDepositBoxTime
	OR IS_BIT_SET(MC_ServerBD_2.iSafetyDepositBoxDrilledBS[iLockbox], iDynamicCabinet)
		RETURN TRUE
	ENDIF	
	RETURN FALSE
ENDFUNC
FUNC BOOL IS_THIS_HANDHELD_DRILL_MINIGAME_CABINET_COMPLETE(STRUCT_HANDHELD_DRILL_MINI_GAME &sHandHeldDrillPassedIn, INT iDynamicCabinet)
	INT iBox
	FOR iBox = 0 TO ciMAX_NUMBER_OF_HANDHELD_DRILL_LOCKBOXES-1
		IF NOT IS_THIS_HANDHELD_DRILL_MINIGAME_LOCKBOX_COMPLETE(sHandHeldDrillPassedIn, iDynamicCabinet, iBox)
			RETURN FALSE
		ENDIF
	ENDFOR
	RETURN TRUE
ENDFUNC
FUNC FLOAT GET_CURRENT_HANDHELD_DRILL_MINIGAME_LOCKBOX_COMPLETION(STRUCT_HANDHELD_DRILL_MINI_GAME &sHandHeldDrillPassedIn)
	RETURN sHandHeldDrillPassedIn.fLockboxCompletionPercent[sHandHeldDrillPassedIn.iLockboxDynamicCabinetSelected][sHandHeldDrillPassedIn.iLockboxSelected]
ENDFUNC
PROC INCREMENT_CURRENT_HANDHELD_DRILL_MINIGAME_LOCKBOX_COMPLETION(STRUCT_HANDHELD_DRILL_MINI_GAME &sHandHeldDrillPassedIn)
	sHandHeldDrillPassedIn.fLockboxCompletionPercent[sHandHeldDrillPassedIn.iLockboxDynamicCabinetSelected][sHandHeldDrillPassedIn.iLockboxSelected] += 0.050
ENDPROC
PROC STOP_HANDHELD_DRILL_MINIGAME_PARTICLE_EFFECT_FOR_DRILL(STRUCT_HANDHELD_DRILL_MINI_GAME &sHandHeldDrillPassedIn)
	IF DOES_PARTICLE_FX_LOOPED_EXIST(sHandHeldDrillPassedIn.ptfxDrillSparks)
		PRINTLN("[LM][HandHeldDrill] - STOP_HANDHELD_DRILL_MINIGAME_PARTICLE_EFFECT_FOR_DRILL - Destroying Particle Effect for Drill")
		STOP_PARTICLE_FX_LOOPED(sHandHeldDrillPassedIn.ptfxDrillSparks)
	ENDIF
ENDPROC
PROC CREATE_HANDHELD_DRILL_MINIGAME_PARTICLE_EFFECT_FOR_DRILL(STRUCT_HANDHELD_DRILL_MINI_GAME &sHandHeldDrillPassedIn)
	IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(sHandHeldDrillPassedIn.ptfxDrillSparks)		
		IF HAS_NAMED_PTFX_ASSET_LOADED("scr_ch_finale")	
			USE_PARTICLE_FX_ASSET("scr_ch_finale")	
			INT iBone = GET_ENTITY_BONE_INDEX_BY_NAME(sHandHeldDrillPassedIn.oiDrill, "Tip_01")
			sHandHeldDrillPassedIn.ptfxDrillSparks = START_PARTICLE_FX_LOOPED_ON_ENTITY_BONE("scr_ch_finale_drill_sparks_nodecal", sHandHeldDrillPassedIn.oiDrill, <<0.0, 0.0, 0.0>>, (<<0.0, 90.0, 0.0>>), iBone, 1.0)
		ENDIF
	ELSE
		FLOAT fPercent = (GET_CURRENT_HANDHELD_DRILL_MINIGAME_LOCKBOX_COMPLETION(sHandHeldDrillPassedIn) / g_FMMC_STRUCT.fHandheldDrillingMinigameSafetyDepositBoxTime)
		SET_PARTICLE_FX_LOOPED_EVOLUTION(sHandHeldDrillPassedIn.ptfxDrillSparks, "power", CLAMP(1.0-fPercent, 0.2, 1.0))
	ENDIF
ENDPROC
PROC STOP_HANDHELD_DRILL_MINIGAME_SOUND_EFFECT(STRUCT_HANDHELD_DRILL_MINI_GAME &sHandHeldDrillPassedIn)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_EnableCasinoHeistAudioBank)
		EXIT
	ENDIF	
	IF sHandHeldDrillPassedIn.iDrillSoundID = -1
		EXIT
	ENDIF
	
	STOP_SOUND(sHandHeldDrillPassedIn.iDrillSoundID)
	RELEASE_SOUND_ID(sHandHeldDrillPassedIn.iDrillSoundID)
	sHandHeldDrillPassedIn.iDrillSoundID = -1
ENDPROC
PROC PLAY_HANDHELD_DRILL_MINIGAME_SOUND_EFFECT(STRUCT_HANDHELD_DRILL_MINI_GAME &sHandHeldDrillPassedIn)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_EnableCasinoHeistAudioBank)
		EXIT
	ENDIF	
	IF sHandHeldDrillPassedIn.iDrillSoundID != -1
		EXIT
	ENDIF
	
	VECTOR vSoundPos = GET_ENTITY_COORDS(sHandHeldDrillPassedIn.oiDrill)
	STRING sSoundSet = "dlc_ch_heist_finale_lockbox_drill_sounds"
	STRING sSoundName = "drill"
	
	sHandHeldDrillPassedIn.iDrillSoundID = GET_SOUND_ID()
	
	PLAY_SOUND_FROM_COORD(sHandHeldDrillPassedIn.iDrillSoundID, sSoundName, vSoundPos, sSoundSet, TRUE)	
ENDPROC

PROC STOP_HANDHELD_DRILL_MINIGAME_PARTICLE_EFFECT_FOR_DRILL_REMOTELY(STRUCT_HANDHELD_DRILL_MINI_GAME &sHandHeldDrillPassedIn, INT iRemotePlayer)
	IF DOES_PARTICLE_FX_LOOPED_EXIST(sHandHeldDrillPassedIn.ptfxRemoteDrillSparks[iRemotePlayer])
		PRINTLN("[LM][HandHeldDrill] - STOP_HANDHELD_DRILL_MINIGAME_PARTICLE_EFFECT_FOR_DRILL_REMOTELY - Destroying Particle Effect for Drill")
		STOP_PARTICLE_FX_LOOPED(sHandHeldDrillPassedIn.ptfxRemoteDrillSparks[iRemotePlayer])
	ENDIF
ENDPROC
PROC CREATE_HANDHELD_DRILL_MINIGAME_PARTICLE_EFFECT_FOR_DRILL_REMOTELY(STRUCT_HANDHELD_DRILL_MINI_GAME &sHandHeldDrillPassedIn, INT iRemotePlayer, PED_INDEX pedIndex)
	IF NOT DOES_ENTITY_EXIST(sHandHeldDrillPassedIn.oiRemoteDrills[iRemotePlayer])
		MODEL_NAMES mnDrill = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Vault_Drill_01a"))
		VECTOR vPedCoord = GET_ENTITY_COORDS(pedIndex)
		sHandHeldDrillPassedIn.oiRemoteDrills[iRemotePlayer] = GET_CLOSEST_OBJECT_OF_TYPE(vPedCoord, 2.0, mnDrill, FALSE, FALSE, FALSE)
		PRINTLN("[LM][HandHeldDrill] - CREATE_HANDHELD_DRILL_MINIGAME_PARTICLE_EFFECT_FOR_DRILL_REMOTELY - Assigning Drill")
	ENDIF
	
	IF DOES_ENTITY_EXIST(sHandHeldDrillPassedIn.oiRemoteDrills[iRemotePlayer])
		IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(sHandHeldDrillPassedIn.ptfxRemoteDrillSparks[iRemotePlayer])		
			IF HAS_NAMED_PTFX_ASSET_LOADED("scr_ch_finale")	
				USE_PARTICLE_FX_ASSET("scr_ch_finale")	
				INT iBone = GET_ENTITY_BONE_INDEX_BY_NAME(sHandHeldDrillPassedIn.oiRemoteDrills[iRemotePlayer], "Tip_01")			
				sHandHeldDrillPassedIn.ptfxRemoteDrillSparks[iRemotePlayer] = START_PARTICLE_FX_LOOPED_ON_ENTITY_BONE("scr_ch_finale_drill_sparks", sHandHeldDrillPassedIn.oiRemoteDrills[iRemotePlayer], <<0.0, 0.0, 0.0>>, (<<0.0, 90.0, 0.0>>), iBone, 1.0)
				PRINTLN("[LM][HandHeldDrill] - CREATE_HANDHELD_DRILL_MINIGAME_PARTICLE_EFFECT_FOR_DRILL_REMOTELY - Creating PTFX.")
			ENDIF
		ELSE
			SET_PARTICLE_FX_LOOPED_EVOLUTION(sHandHeldDrillPassedIn.ptfxRemoteDrillSparks[iRemotePlayer], "power", 0.5)
		ENDIF
	ENDIF
ENDPROC

FUNC VECTOR GET_HANDHELD_DRILL_MINIGAME_CAMERA_OFFSET(STRUCT_HANDHELD_DRILL_MINI_GAME &sHandHeldDrillPassedIn)	
	VECTOR vPos = GET_ENTITY_COORDS(sHandHeldDrillPassedIn.pedDrill)
	FLOAT fHead = GET_PED_REQUIRED_HEADING_FOR_LOCKBOX_CABINET_IN_HANDHELD_DRILL_MINIGAME_WITH_INDEX(sHandHeldDrillPassedIn.iLockboxCabinetSelected)
	vPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fHead, (<<1.3, -0.25, 0.18>>))
	
	RETURN vPos
ENDFUNC

PROC PROCESS_HANDHELD_DRILL_MINIGAME_DRILLING_HUD(STRUCT_HANDHELD_DRILL_MINI_GAME &sHandHeldDrillPassedIn)
	IF sHandHeldDrillPassedIn.eDrillingState > HANDHELD_DRILL_MINIGAME_DRILLING_STATE_WAITING
	AND sHandHeldDrillPassedIn.eDrillingState < HANDHELD_DRILL_MINIGAME_DRILLING_STATE_CLEANUP
		IF IS_PC_VERSION()
		AND IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
			DISPLAY_HELP_TEXT_THIS_FRAME_FOR_MINIGAME("CAS_DRIL_HELP3", FALSE)
		ELSE
			DISPLAY_HELP_TEXT_THIS_FRAME_FOR_MINIGAME("CAS_DRIL_HELP2", FALSE)
		ENDIF
		/*INT iPercent
		INT iPercentMax
		iPercentMax = 100
		iPercent = ROUND(100*(GET_CURRENT_HANDHELD_DRILL_MINIGAME_LOCKBOX_COMPLETION(sHandHeldDrillPassedIn) / g_FMMC_STRUCT.fHandheldDrillingMinigameSafetyDepositBoxTime))
		DRAW_GENERIC_METER(iPercent, iPercentMax, "CAS_DRIL_MTR")*/
	ENDIF
ENDPROC

PROC SET_UP_HANDHELD_DRILL_MINIGAME_OBJECTS_FOR_ANIMATIONS(STRUCT_HANDHELD_DRILL_MINI_GAME &sHandHeldDrillPassedIn)
	MODEL_NAMES mnDrill = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Vault_Drill_01a"))
	MODEL_NAMES mnMoneyBag = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Ch_MoneyBag_01a"))
	MODEL_NAMES mnDuffelBag = MC_playerBD_1[iLocalPart].mnHeistGearBag
			
	IF NOT DOES_ENTITY_EXIST(sHandHeldDrillPassedIn.oiCabinet)
		IF DOES_ENTITY_EXIST(sHandHeldDrillPassedIn.oiLockboxes[sHandHeldDrillPassedIn.iLockboxDynamicCabinetSelected])
			PRINTLN("[LM][HandHeldDrill] - SET_UP_HANDHELD_DRILL_MINIGAME_OBJECTS_FOR_ANIMATIONS - Assigning Cabinet Index: ", sHandHeldDrillPassedIn.iLockboxDynamicCabinetSelected)
			sHandHeldDrillPassedIn.oiCabinet = sHandHeldDrillPassedIn.oiLockboxes[sHandHeldDrillPassedIn.iLockboxDynamicCabinetSelected]
		ELSE
			PRINTLN("[LM][HandHeldDrill] - SET_UP_HANDHELD_DRILL_MINIGAME_OBJECTS_FOR_ANIMATIONS - Tried to assign Cabinet Index: ", sHandHeldDrillPassedIn.iLockboxDynamicCabinetSelected, " but Cabinet does not exist")
		ENDIF
	ENDIF
	
	VECTOR vHiddenPosition
	vHiddenPosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(sHandHeldDrillPassedIn.oiCabinet), GET_ENTITY_HEADING(sHandHeldDrillPassedIn.oiCabinet), <<0.0, 1.0, 0.0>>)
	vHiddenPosition.z += 1.0
	
	PRINTLN("[LM][HandHeldDrill] - SET_UP_HANDHELD_DRILL_MINIGAME_OBJECTS_FOR_ANIMATIONS - Creating The Minigame Objects!")
	
	IF NOT DOES_ENTITY_EXIST(sHandHeldDrillPassedIn.oiDrill)
		PRINTLN("[LM][HandHeldDrill] - SET_UP_HANDHELD_DRILL_MINIGAME_OBJECTS_FOR_ANIMATIONS - Creating Drill Object at coord: ", vHiddenPosition)
		sHandHeldDrillPassedIn.oiDrill = CREATE_OBJECT(mnDrill, vHiddenPosition, TRUE, FALSE)
		SET_ENTITY_VISIBLE(sHandHeldDrillPassedIn.oiDrill, FALSE)		
		SET_ENTITY_COLLISION(sHandHeldDrillPassedIn.oiDrill, FALSE)
	ENDIF

	IF NOT DOES_ENTITY_EXIST(sHandHeldDrillPassedIn.oiMoneyBag)	
		PRINTLN("[LM][HandHeldDrill] - SET_UP_HANDHELD_DRILL_MINIGAME_OBJECTS_FOR_ANIMATIONS - Creating money bag at coord: ", vHiddenPosition)
		sHandHeldDrillPassedIn.oiMoneyBag = CREATE_OBJECT(mnMoneyBag, vHiddenPosition, TRUE, FALSE)
		SET_ENTITY_VISIBLE(sHandHeldDrillPassedIn.oiMoneyBag, FALSE)
		SET_ENTITY_COLLISION(sHandHeldDrillPassedIn.oiMoneyBag, FALSE)
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(sHandHeldDrillPassedIn.oiDrillBag)	
		PRINTLN("[LM][HandHeldDrill] - SET_UP_HANDHELD_DRILL_MINIGAME_OBJECTS_FOR_ANIMATIONS - Creating drill bag at coord: ", vHiddenPosition)
		sHandHeldDrillPassedIn.oiDrillBag = CREATE_OBJECT(mnDuffelBag, vHiddenPosition, TRUE, FALSE)
		SET_ENTITY_VISIBLE(sHandHeldDrillPassedIn.oiDrillBag, FALSE)
		SET_ENTITY_COLLISION(sHandHeldDrillPassedIn.oiDrillBag, FALSE)
	ENDIF
ENDPROC

FUNC BOOL IS_HANDHELD_DRILL_MINIGAME_SYNC_SCENE_PLAYING(STRUCT_HANDHELD_DRILL_MINI_GAME &sHandHeldDrillPassedIn, STRING sAnimToPlay)	
	BOOL bIsSceneRunning = IS_SYNC_SCENE_RUNNING(sHandHeldDrillPassedIn.iSyncScene)
	
	IF NOT ARE_STRINGS_EQUAL(sHandHeldDrillPassedIn.tl63_AnimPlaying, sAnimToPlay)
	OR NOT bIsSceneRunning
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_HANDHELD_DRILL_MINIGAME_READY_TO_PLAY_IDLE_ANIMS(STRUCT_HANDHELD_DRILL_MINI_GAME &sHandHeldDrillPassedIn)
	TEXT_LABEL_63 tl63
	
	tl63 = "TRANS_TO_IDLE"
	IF IS_HANDHELD_DRILL_MINIGAME_SYNC_SCENE_PLAYING(sHandHeldDrillPassedIn, tl63)
	AND GET_SYNC_SCENE_PHASE(sHandHeldDrillPassedIn.iSyncScene) <= 0.99
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC BOOL IS_HANDHELD_DRILL_MINIGAME_READY_TO_PLAY_EXIT(STRUCT_HANDHELD_DRILL_MINI_GAME &sHandHeldDrillPassedIn)
	TEXT_LABEL_63 tl63
	
	tl63 = "TRANS_TO_REST"
	IF IS_HANDHELD_DRILL_MINIGAME_SYNC_SCENE_PLAYING(sHandHeldDrillPassedIn, tl63)
	AND GET_SYNC_SCENE_PHASE(sHandHeldDrillPassedIn.iSyncScene) <= 0.99
		RETURN FALSE
	ENDIF
	tl63 = "TRANS_TO_IDLE"
	IF IS_HANDHELD_DRILL_MINIGAME_SYNC_SCENE_PLAYING(sHandHeldDrillPassedIn, tl63)
	AND GET_SYNC_SCENE_PHASE(sHandHeldDrillPassedIn.iSyncScene) <= 0.99
		RETURN FALSE
	ENDIF	
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_HANDHELD_DRILL_MINIGAME_PLAY_REST_EXIT(STRUCT_HANDHELD_DRILL_MINI_GAME &sHandHeldDrillPassedIn)
	TEXT_LABEL_63 tl63 = "REST"
	IF IS_HANDHELD_DRILL_MINIGAME_SYNC_SCENE_PLAYING(sHandHeldDrillPassedIn, tl63)	
		RETURN TRUE
	ENDIF
	tl63 = "REST_EXIT"
	IF IS_HANDHELD_DRILL_MINIGAME_SYNC_SCENE_PLAYING(sHandHeldDrillPassedIn, tl63)	
		RETURN TRUE
	ENDIF
	tl63 = "TRANS_TO_REST"
	IF IS_HANDHELD_DRILL_MINIGAME_SYNC_SCENE_PLAYING(sHandHeldDrillPassedIn, tl63)
	AND GET_SYNC_SCENE_PHASE(sHandHeldDrillPassedIn.iSyncScene) <= 0.99
		RETURN TRUE
	ENDIF
	tl63 = "TRANS_TO_IDLE"
	IF IS_HANDHELD_DRILL_MINIGAME_SYNC_SCENE_PLAYING(sHandHeldDrillPassedIn, tl63)
	AND GET_SYNC_SCENE_PHASE(sHandHeldDrillPassedIn.iSyncScene) <= 0.99
		RETURN FALSE
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL SHOULD_HANDHELD_DRILL_MINIGAME_REST(STRUCT_HANDHELD_DRILL_MINI_GAME &sHandHeldDrillPassedIn)
	IF HAS_NET_TIMER_EXPIRED_READ_ONLY(sHandHeldDrillPassedIn.tdMinigameIdleTimer, ciTIME_FOR_DRILL_ANIM_TO_ENTER_REST)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC
FUNC BOOL IS_HANDHELD_DRILL_MINIGAME_READY_TO_PLAY_ACTION(STRUCT_HANDHELD_DRILL_MINI_GAME &sHandHeldDrillPassedIn)
	IF IS_HANDHELD_DRILL_MINIGAME_DRILL_BUTTON_BEING_HELD()
		TEXT_LABEL_63 tl63 = "IDLE"
		IF IS_HANDHELD_DRILL_MINIGAME_SYNC_SCENE_PLAYING(sHandHeldDrillPassedIn, tl63)
			RETURN TRUE
		ENDIF		
		tl63 = "REWARD"
		IF IS_HANDHELD_DRILL_MINIGAME_SYNC_SCENE_PLAYING(sHandHeldDrillPassedIn, tl63)
			RETURN TRUE
		ENDIF
		tl63 = "NO_REWARD"
		IF IS_HANDHELD_DRILL_MINIGAME_SYNC_SCENE_PLAYING(sHandHeldDrillPassedIn, tl63)
			RETURN TRUE
		ENDIF
		tl63 = "ACTION"
		IF IS_HANDHELD_DRILL_MINIGAME_SYNC_SCENE_PLAYING(sHandHeldDrillPassedIn, tl63)
			RETURN TRUE
		ENDIF
		tl63 = "ENTER"
		IF IS_HANDHELD_DRILL_MINIGAME_SYNC_SCENE_PLAYING(sHandHeldDrillPassedIn, tl63)
			RETURN TRUE
		ENDIF
		tl63 = "TRANS_TO_IDLE"
		IF IS_HANDHELD_DRILL_MINIGAME_SYNC_SCENE_PLAYING(sHandHeldDrillPassedIn, tl63)
		AND GET_SYNC_SCENE_PHASE(sHandHeldDrillPassedIn.iSyncScene) >= 0.99
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL SHOULD_HANDHELD_DRILL_MINIGAME_TRANSITION_TO_RESTING(STRUCT_HANDHELD_DRILL_MINI_GAME &sHandHeldDrillPassedIn)
	IF SHOULD_HANDHELD_DRILL_MINIGAME_REST(sHandHeldDrillPassedIn)
		TEXT_LABEL_63 tl63 = "IDLE"
		IF IS_HANDHELD_DRILL_MINIGAME_SYNC_SCENE_PLAYING(sHandHeldDrillPassedIn, tl63)
			RETURN TRUE
		ENDIF
		tl63 = "TRANS_TO_REST"
		IF IS_HANDHELD_DRILL_MINIGAME_SYNC_SCENE_PLAYING(sHandHeldDrillPassedIn, tl63)		
		AND GET_SYNC_SCENE_PHASE(sHandHeldDrillPassedIn.iSyncScene) <= 0.99
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC START_HANDHELD_DRILL_MINIGAME_SYNC_SCENE(STRUCT_HANDHELD_DRILL_MINI_GAME &sHandHeldDrillPassedIn, TEXT_LABEL_63 tl63_AnimDict, TEXT_LABEL_63 tl63_AnimPed, TEXT_LABEL_63 tl63_AnimDrill, TEXT_LABEL_63 tl63_AnimCab,
												TEXT_LABEL_63 tl63_AnimCam, TEXT_LABEL_63 tl63_AnimMoneyBag, TEXT_LABEL_63 tl63_AnimDuffelBag, BOOL bExitting = FALSE)
		
	VECTOR vCoords = GET_ENTITY_COORDS(sHandHeldDrillPassedIn.oiCabinet)
	VECTOR vRot = GET_ENTITY_ROTATION(sHandHeldDrillPassedIn.oiCabinet)
	
	PRINTLN("[LM][HandHeldDrill] - START_HANDHELD_DRILL_MINIGAME_SYNC_SCENE - Sync Scene is being set up at vCoords: ", vCoords, " vRot: ", vRot, " AnimDict: ", tl63_AnimDict, " AnimPed: ", tl63_AnimPed, " AnimDrill: ", tl63_AnimDrill, " AnimCab: ", tl63_AnimCab, " tl63_AnimCam: ", tl63_AnimCam)
	PRINTLN("------ tl63_AnimMoneyBag: ", tl63_AnimMoneyBag, " tl63_AnimDuffelBag: ", tl63_AnimDuffelBag)
		
	IK_CONTROL_FLAGS ikFlags
	ikFlags = AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK | AIK_DISABLE_HEAD_IK | AIK_DISABLE_TORSO_IK	| AIK_DISABLE_TORSO_REACT_IK
	
	SYNCED_SCENE_PLAYBACK_FLAGS sceneFlags
	sceneFlags = SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT

	sHandHeldDrillPassedIn.iSyncScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCoords, vRot, EULER_XYZ, SHOULD_HOLD_LAST_FRAME_HANDHELD_DRILL_MINIGAME_ANIM(tl63_AnimPed), SHOULD_LOOP_HANDHELD_DRILL_MINIGAME_ANIM(tl63_AnimPed))	
	IF bExitting
		NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(sHandHeldDrillPassedIn.pedDrill, sHandHeldDrillPassedIn.iSyncScene, tl63_AnimDict, tl63_AnimPed, SLOW_BLEND_IN, SLOW_BLEND_OUT, sceneFlags, DEFAULT, DEFAULT, ikFlags)
	ELSE
		NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(sHandHeldDrillPassedIn.pedDrill, sHandHeldDrillPassedIn.iSyncScene, tl63_AnimDict, tl63_AnimPed, SLOW_BLEND_IN, INSTANT_BLEND_OUT, sceneFlags, DEFAULT, DEFAULT, ikFlags)
	ENDIF
	NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(sHandHeldDrillPassedIn.oiDrill, sHandHeldDrillPassedIn.iSyncScene, tl63_AnimDict, tl63_AnimDrill, SLOW_BLEND_IN, SLOW_BLEND_OUT, sceneFlags)
	NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(sHandHeldDrillPassedIn.oiCabinet, sHandHeldDrillPassedIn.iSyncScene, tl63_AnimDict, tl63_AnimCab, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, sceneFlags)	
	NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(sHandHeldDrillPassedIn.oiDrillBag, sHandHeldDrillPassedIn.iSyncScene, tl63_AnimDict, tl63_AnimDuffelBag, SLOW_BLEND_IN, SLOW_BLEND_OUT, sceneFlags)		
	IF ARE_STRINGS_EQUAL(tl63_AnimPed, "REWARD")
		SET_ENTITY_VISIBLE(sHandHeldDrillPassedIn.oiMoneyBag, TRUE)
		NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(sHandHeldDrillPassedIn.oiMoneyBag, sHandHeldDrillPassedIn.iSyncScene, tl63_AnimDict, tl63_AnimMoneyBag, SLOW_BLEND_IN, INSTANT_BLEND_OUT, sceneFlags)	
	ELSE
		SET_ENTITY_VISIBLE(sHandHeldDrillPassedIn.oiMoneyBag, FALSE)
	ENDIF
	
	//NETWORK_ADD_SYNCHRONISED_SCENE_CAMERA(sHandHeldDrillPassedIn.iSyncScene, tl63_AnimDict, tl63_AnimCam)	
	//NETWORK_FORCE_LOCAL_USE_OF_SYNCED_SCENE_CAMERA(sHandHeldDrillPassedIn.iSyncScene)
	
	NETWORK_START_SYNCHRONISED_SCENE(sHandHeldDrillPassedIn.iSyncScene)	
	
	PLAY_CAM_ANIM(sHandHeldDrillPassedIn.camDrillCam, tl63_AnimCam, tl63_AnimDict, vCoords, vRot)
	IF FIND_ANIM_EVENT_PHASE(tl63_AnimDict, tl63_AnimCam, "CamBlendOut", sHandHeldDrillPassedIn.fDrillCamBlendoutPhaseStart, sHandHeldDrillPassedIn.fDrillCamBlendoutPhaseEnd)	
		PRINTLN("[LM][HandHeldDrill] - FIND_ANIM_EVENT_PHASE - fDrillCamBlendoutPhaseStart: ", sHandHeldDrillPassedIn.fDrillCamBlendoutPhaseStart, " fDrillCamBlendoutPhaseEnd: ", sHandHeldDrillPassedIn.fDrillCamBlendoutPhaseEnd, " SUCCESS")
	ELSE
		PRINTLN("[LM][HandHeldDrill] - FIND_ANIM_EVENT_PHASE - fDrillCamBlendoutPhaseStart: ", sHandHeldDrillPassedIn.fDrillCamBlendoutPhaseStart, " fDrillCamBlendoutPhaseEnd: ", sHandHeldDrillPassedIn.fDrillCamBlendoutPhaseEnd, " FAILED")
	ENDIF
	
	sHandHeldDrillPassedIn.tl63_AnimPlaying = tl63_AnimPed
ENDPROC

PROC BUILD_HANDHELD_DRILL_MINIGAME_ANIM_NAMES(STRUCT_HANDHELD_DRILL_MINI_GAME &sHandHeldDrillPassedIn, STRING tl63_AnimToPlay, TEXT_LABEL_63 &tl63_AnimPed, TEXT_LABEL_63 &tl63_AnimDrill,
												TEXT_LABEL_63 &tl63_AnimCab, TEXT_LABEL_63 &tl63_AnimCam, TEXT_LABEL_63 &tl63_AnimMoney, TEXT_LABEL_63 &tl63_AnimDuffel)
	PRINTLN("[LM][HandHeldDrill] - BUILD_HANDHELD_DRILL_MINIGAME_ANIM_NAMES - Anim to build: ", tl63_AnimToPlay)
	INT iPattern = sHandHeldDrillPassedIn.iLockboxCabinetPattern[sHandHeldDrillPassedIn.iLockboxDynamicCabinetSelected]
	TEXT_LABEL_63 tlName_Cam_63 = "_CAM"
	TEXT_LABEL_63 tlName_Drill_63 = "_ch_Prop_Vault_Drill_01a"	
	TEXT_LABEL_15 tl15 = GET_HANDHELD_DRILL_MINIGAME_CABINET_MODEL_NAME_SUFFIX_FROM_INT(iPattern, sHandHeldDrillPassedIn.iLockboxCabinetSubPattern[sHandHeldDrillPassedIn.iLockboxDynamicCabinetSelected])	
	TEXT_LABEL_63 tlName_Cabinet_63 = "_ch_Prop_CH_Sec_Cabinet_01"
	tlName_Cabinet_63 += tl15
	TEXT_LABEL_63 tlName_Money_63 = "_ch_Prop_Ch_MoneyBag_01a"
	TEXT_LABEL_63 tlName_Duffel_63 = "_P_M_bag_var22_Arm_S"
	
	tl63_AnimPed = tl63_AnimToPlay
	tl63_AnimDrill = tl63_AnimToPlay
	tl63_AnimDrill += tlName_Drill_63
	tl63_AnimCab = tl63_AnimToPlay
	tl63_AnimCab += tlName_Cabinet_63
	tl63_AnimCam = tl63_AnimToPlay
	tl63_AnimCam += tlName_Cam_63
	tl63_AnimMoney = tl63_AnimToPlay
	tl63_AnimMoney += tlName_Money_63
	tl63_AnimDuffel = tl63_AnimToPlay
	tl63_AnimDuffel += tlName_Duffel_63
ENDPROC

PROC PROCESS_HANDHELD_DRILL_MINIGAME_HIDE_ANIM_OBJECTS(STRUCT_HANDHELD_DRILL_MINI_GAME &sHandHeldDrillPassedIn)	
	
	BOOL bFinished
	FLOAT fScenePhase, fScenePhaseCamBlendout
	bFinished = HAS_ANIM_EVENT_FIRED(sHandHeldDrillPassedIn.pedDrill, GET_HASH_KEY("BREAKOUT"))
	fScenePhase = GET_SYNC_SCENE_PHASE(sHandHeldDrillPassedIn.iSyncScene)
	fScenePhaseCamBlendout = sHandHeldDrillPassedIn.fDrillCamBlendoutPhaseStart	
	IF fScenePhaseCamBlendout >= 1.0
		fScenePhaseCamBlendout = 0.8
	ENDIF
		
	IF (NOT IS_BIT_SET(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_ShowDuffelBag) AND fScenePhase >= 0.91)
	OR bFinished
		BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_ShowHeistBagForPart)
		PRINTLN("[LM][HandHeldDrill] - PROCESS_HANDHELD_DRILL_MINIGAME_HIDE_ANIM_OBJECTS - Showing Heist Duffel Bag.")
	ENDIF
	
	IF (NOT IS_BIT_SET(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_ShowDuffelBag) AND fScenePhase >= 0.96)
	OR bFinished
		SET_ENTITY_VISIBLE(sHandHeldDrillPassedIn.oiDrillBag, FALSE)
		SET_BIT(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_ShowDuffelBag)
		PRINTLN("[LM][HandHeldDrill] - PROCESS_HANDHELD_DRILL_MINIGAME_HIDE_ANIM_OBJECTS - Hiding Animated Duffel Bag.")
	ENDIF

	IF NOT IS_BIT_SET(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_HideAnimDrill)
	AND HAS_ANIM_EVENT_FIRED(sHandHeldDrillPassedIn.oiDrill, GET_HASH_KEY("DELETE"))
	OR bFinished
		SET_ENTITY_VISIBLE(sHandHeldDrillPassedIn.oiDrill, FALSE)
		SET_BIT(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_HideAnimDrill)		
		PRINTLN("[LM][HandHeldDrill] - PROCESS_HANDHELD_DRILL_MINIGAME_HIDE_ANIM_OBJECTS - Hiding Animated Drill.")
	ENDIF
	
	IF fScenePhase >= fScenePhaseCamBlendout
	OR bFinished
		IF DOES_CAM_EXIST(sHandHeldDrillPassedIn.camDrillCam)
			PRINTLN("[LM][HandHeldDrill] - PROCESS_HANDHELD_DRILL_MINIGAME_HIDE_ANIM_OBJECTS - BLENDING OUT CAMERA.")
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			RENDER_SCRIPT_CAMS(FALSE, TRUE, 1500)
			DESTROY_CAM(sHandHeldDrillPassedIn.camDrillCam)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_HANDHELD_DRILL_MINIGAME_BY_REMOTE_CLIENTS(STRUCT_HANDHELD_DRILL_MINI_GAME &sHandHeldDrillPassedIn)	
	INT iNumberOfPlayers = (MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]) 
	INT iCount
	INT i
	PARTICIPANT_INDEX piPart
	PLAYER_INDEX piPlayer
	PED_INDEX pedPlayer
	
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		piPart = INT_TO_PARTICIPANTINDEX(i)
		
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(piPart)
			RELOOP
		ENDIF
		
		IF IS_PARTICIPANT_A_SPECTATOR(i)
			RELOOP
		ENDIF
		
		piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)		
		pedPlayer = GET_PLAYER_PED(piPlayer)
		
		IF pedPlayer != LocalPlayerPed			
			IF IS_PED_INJURED(pedPlayer)			
			OR NOT IS_BIT_SET(MC_PlayerBD[i].iClientBitSet4, PBBOOL4_IS_USING_THE_HANDHELD_DRILL)
				STOP_HANDHELD_DRILL_MINIGAME_PARTICLE_EFFECT_FOR_DRILL_REMOTELY(sHandheldDrillPassedIn, iCount)
			ELIF IS_BIT_SET(MC_PlayerBD[i].iClientBitSet4, PBBOOL4_IS_USING_THE_HANDHELD_DRILL)
				CREATE_HANDHELD_DRILL_MINIGAME_PARTICLE_EFFECT_FOR_DRILL_REMOTELY(sHandheldDrillPassedIn, iCount, pedPlayer)
			ENDIF
		ENDIF
		
		iCount++
		IF iCount >= iNumberOfPlayers
			BREAKLOOP
		ENDIF
	ENDFOR
ENDPROC

PROC CLEANUP_HANDHELD_DRILL_MINIGAME_REMOTE_FUNCTIONALITY(STRUCT_HANDHELD_DRILL_MINI_GAME &sHandHeldDrillPassedIn)
	INT i
	FOR i = 0 TO ciMAX_HANDHELD_DRILLERS-1
		IF DOES_PARTICLE_FX_LOOPED_EXIST(sHandHeldDrillPassedIn.ptfxRemoteDrillSparks[i])
			STOP_PARTICLE_FX_LOOPED(sHandHeldDrillPassedIn.ptfxRemoteDrillSparks[i])
		ENDIF
		IF DOES_ENTITY_EXIST(sHandHeldDrillPassedIn.oiRemoteDrills[i])
			DELETE_OBJECT(sHandHeldDrillPassedIn.oiRemoteDrills[i])
		ENDIF
	ENDFOR
ENDPROC

FUNC BOOL PROCESS_HANDHELD_DRILL_MINIGAME_DRILLING(STRUCT_HANDHELD_DRILL_MINI_GAME &sHandHeldDrillPassedIn)	
	INT iPattern = sHandHeldDrillPassedIn.iLockboxCabinetPattern[sHandHeldDrillPassedIn.iLockboxDynamicCabinetSelected]	
	INT iLockbox = sHandHeldDrillPassedIn.iLockboxSelected
	
	TEXT_LABEL_63 tlAnimDict_63 = GET_HANDHELD_MINIGAME_ANIM_DICT(iPattern+1, iLockbox+1)
	TEXT_LABEL_63 tlAnimName_63
	TEXT_LABEL_63 tlAnimName_Cam_63	
	TEXT_LABEL_63 tlAnimName_Drill_63		
	TEXT_LABEL_63 tlAnimName_Cabinet_63
	TEXT_LABEL_63 tlAnimName_Money_63
	TEXT_LABEL_63 tlAnimName_Duffel_63
	
	PROCESS_HANDHELD_DRILL_MINIGAME_DRILLING_HUD(sHandHeldDrillPassedIn)	
			
	IF IS_PED_INJURED(sHandHeldDrillPassedIn.pedDrill)
		PRINTLN("[LM][HandHeldDrill] - PROCESS_HANDHELD_DRILL_MINIGAME_DRILLING - We are dead while drilling. Sending to Cleanup.")
		SET_HANDHELD_DRILL_MINIGAME_DRILLING_STATE(sHandHeldDrillPassedIn.eDrillingState, HANDHELD_DRILL_MINIGAME_DRILLING_STATE_CLEANUP)
	ELSE
		// Something else must be enabling it to be visible, this will fix the bug which seems to be have a very rare reproduction rate and no logs when it was found. Calling this every frame is done in various other places of script so should be safe enough.
		SET_PED_CURRENT_WEAPON_VISIBLE(sHandHeldDrillPassedIn.pedDrill, FALSE, FALSE)
	ENDIF
	
	SWITCH sHandHeldDrillPassedIn.eDrillingState
		// Init and Enter
		CASE HANDHELD_DRILL_MINIGAME_DRILLING_STATE_WAITING
			IF NOT IS_BIT_SET(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_InitializedDrillingSettings)
				SET_HANDHELD_DRILL_MINIGAME_DRILLING_STATE(sHandHeldDrillPassedIn.eDrillingState, HANDHELD_DRILL_MINIGAME_DRILLING_STATE_SELECTING)			
				RETURN FALSE
			ENDIF

			SET_BIT(iLocalBoolCheck32, LBOOL32_USING_HANDHELD_DRILL)
			CLEAR_BIT(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_LockboxHasAReward)
			CLEAR_BIT(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_CheckLockboxHasAReward)
			
			IF DOES_CAM_EXIST(sHandHeldDrillPassedIn.camDrillCam)
				BUILD_HANDHELD_DRILL_MINIGAME_ANIM_NAMES(sHandHeldDrillPassedIn, "ENTER", tlAnimName_63, tlAnimName_Drill_63, tlAnimName_Cabinet_63, tlAnimName_Cam_63, tlAnimName_Money_63, tlAnimName_Duffel_63)			
				IF IS_HANDHELD_DRILL_MINIGAME_SYNC_SCENE_PLAYING(sHandHeldDrillPassedIn, tlAnimName_63)						
					IF NOT IS_CAM_ACTIVE(sHandHeldDrillPassedIn.camDrillCam)
						SET_CAM_ACTIVE(sHandHeldDrillPassedIn.camDrillCam, TRUE)
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
					ENDIF
					
					IF NOT IS_BIT_SET(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_HideDuffelBag)
					AND GET_SYNC_SCENE_PHASE(sHandHeldDrillPassedIn.iSyncScene) >= 0.1
						SET_BIT(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_HideDuffelBag)
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_HideHeistBagForPart)						
						SET_ENTITY_VISIBLE(sHandHeldDrillPassedIn.oiDrillBag, TRUE)	
						
						PRINTLN("[LM][HandHeldDrill] - PROCESS_HANDHELD_DRILL_MINIGAME_DRILLING - Showing Animated Duffel Bag.")
					ENDIF
					
					IF NOT IS_BIT_SET(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_ShowAnimDrill)
					AND GET_SYNC_SCENE_PHASE(sHandHeldDrillPassedIn.iSyncScene) >= 0.05
						SET_ENTITY_VISIBLE(sHandHeldDrillPassedIn.oiDrill, TRUE)
						SET_BIT(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_ShowAnimDrill)
						PRINTLN("[LM][HandHeldDrill] - PROCESS_HANDHELD_DRILL_MINIGAME_DRILLING - Showing Animated Drill.")
					ENDIF
					
					IF GET_SYNC_SCENE_PHASE(sHandHeldDrillPassedIn.iSyncScene) >= 0.99
						SET_HANDHELD_DRILL_MINIGAME_DRILLING_STATE(sHandHeldDrillPassedIn.eDrillingState, HANDHELD_DRILL_MINIGAME_DRILLING_STATE_SELECTING)
					ENDIF
				ELSE					
					REINIT_NET_TIMER(sHandHeldDrillPassedIn.tdDrillCameraSwitchTimer)						
					SET_UP_HANDHELD_DRILL_MINIGAME_OBJECTS_FOR_ANIMATIONS(sHandHeldDrillPassedIn)
					SET_PED_CURRENT_WEAPON_VISIBLE(sHandHeldDrillPassedIn.pedDrill, FALSE, FALSE)
					START_HANDHELD_DRILL_MINIGAME_SYNC_SCENE(sHandHeldDrillPassedIn, tlAnimDict_63, tlAnimName_63, tlAnimName_Drill_63, tlAnimName_Cabinet_63, tlAnimName_Cam_63, tlAnimName_Money_63, tlAnimName_Duffel_63)
				ENDIF
			ELSE
				VECTOR vCamOffset
				vCamOffset = GET_HANDHELD_DRILL_MINIGAME_CAMERA_OFFSET(sHandHeldDrillPassedIn)				
				sHandHeldDrillPassedIn.camDrillCam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_ANIMATED, vCamOffset, <<0.0, 0.0, 0.0>>)						
				SHAKE_CAM(sHandHeldDrillPassedIn.camDrillCam, "HAND_SHAKE", 0.35)
				PRINTLN("[LM][HandHeldDrill] - PROCESS_HANDHELD_DRILL_MINIGAME_DRILLING - Creating camera at vCamOffsetStart: ", vCamOffset)
			ENDIF
		BREAK
		
		// Selecting valid new box and anim
		CASE HANDHELD_DRILL_MINIGAME_DRILLING_STATE_SELECTING			
			CLEAR_BIT(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_LockboxHasAReward)
			CLEAR_BIT(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_CheckLockboxHasAReward)
			CLEAR_BIT(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_LockboxGivenReward)
			INT iBox
			FOR iBox = 0 TO ciMAX_NUMBER_OF_HANDHELD_DRILL_LOCKBOXES-1
				IF IS_THIS_HANDHELD_DRILL_MINIGAME_LOCKBOX_COMPLETE(sHandHeldDrillPassedIn, sHandHeldDrillPassedIn.iLockboxDynamicCabinetSelected, iBox)
					IF iBox = ciMAX_NUMBER_OF_HANDHELD_DRILL_LOCKBOXES-1
						SET_BIT(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_LockboxExittingFinished)
						SET_HANDHELD_DRILL_MINIGAME_DRILLING_STATE(sHandHeldDrillPassedIn.eDrillingState, HANDHELD_DRILL_MINIGAME_DRILLING_STATE_EXITTING)
						BREAKLOOP
					ELSE
						RELOOP
					ENDIF
				ELSE					
					sHandHeldDrillPassedIn.iLockboxSelected = iBox
					IF IS_BIT_SET(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_InitializedDrillingSettings)
						REINIT_NET_TIMER(sHandHeldDrillPassedIn.tdMinigameIdleTimer)
						SET_HANDHELD_DRILL_MINIGAME_DRILLING_STATE(sHandHeldDrillPassedIn.eDrillingState, HANDHELD_DRILL_MINIGAME_DRILLING_STATE_DRILLING)			
					ELSE
						SET_BIT(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_InitializedDrillingSettings)
						SET_HANDHELD_DRILL_MINIGAME_DRILLING_STATE(sHandHeldDrillPassedIn.eDrillingState, HANDHELD_DRILL_MINIGAME_DRILLING_STATE_WAITING)						
					ENDIF
					BREAKLOOP
				ENDIF
			ENDFOR
		BREAK
		
		// Drilling and inputs
		CASE HANDHELD_DRILL_MINIGAME_DRILLING_STATE_DRILLING
			IF (IS_HANDHELD_DRILL_MINIGAME_LEAVE_DRILLING_JUST_PRESSED() OR SHOULD_OBJECT_MINIGAME_BE_DISABLED_BY_POISON_GAS())
			AND NOT (IS_HANDHELD_DRILL_MINIGAME_SYNC_SCENE_PLAYING(sHandHeldDrillPassedIn, "TRANS_TO_IDLE") OR IS_HANDHELD_DRILL_MINIGAME_SYNC_SCENE_PLAYING(sHandHeldDrillPassedIn, "TRANS_TO_REST"))
				IF SHOULD_OBJECT_MINIGAME_BE_DISABLED_BY_POISON_GAS()
					PRINT_HELP("CAS_DRIL_HELP5", 4000)
				ENDIF
				SET_HANDHELD_DRILL_MINIGAME_DRILLING_STATE(sHandHeldDrillPassedIn.eDrillingState, HANDHELD_DRILL_MINIGAME_DRILLING_STATE_EXITTING)
				RETURN FALSE
			ENDIF
			
			IF (NOT IS_HANDHELD_DRILL_MINIGAME_DRILL_BUTTON_BEING_HELD() AND NOT IS_THIS_HANDHELD_DRILL_MINIGAME_LOCKBOX_COMPLETE(sHandHeldDrillPassedIn, sHandHeldDrillPassedIn.iLockboxDynamicCabinetSelected, sHandHeldDrillPassedIn.iLockboxSelected))
				IF NOT HAS_NET_TIMER_STARTED(sHandHeldDrillPassedIn.tdMinigameIdleTimer)
					START_NET_TIMER(sHandHeldDrillPassedIn.tdMinigameIdleTimer)
				ENDIF				
				IF IS_HANDHELD_DRILL_MINIGAME_READY_TO_PLAY_IDLE_ANIMS(sHandHeldDrillPassedIn)
					IF SHOULD_HANDHELD_DRILL_MINIGAME_TRANSITION_TO_RESTING(sHandHeldDrillPassedIn)
						PRINTLN("[LM][HandHeldDrill] - PROCESS_HANDHELD_DRILL_MINIGAME_DRILLING - We should be transitioning to RESTING.")
						BUILD_HANDHELD_DRILL_MINIGAME_ANIM_NAMES(sHandHeldDrillPassedIn, "TRANS_TO_REST", tlAnimName_63, tlAnimName_Drill_63, tlAnimName_Cabinet_63, tlAnimName_Cam_63, tlAnimName_Money_63, tlAnimName_Duffel_63)
						IF NOT IS_HANDHELD_DRILL_MINIGAME_SYNC_SCENE_PLAYING(sHandHeldDrillPassedIn, tlAnimName_63)				
							START_HANDHELD_DRILL_MINIGAME_SYNC_SCENE(sHandHeldDrillPassedIn, tlAnimDict_63, tlAnimName_63, tlAnimName_Drill_63, tlAnimName_Cabinet_63, tlAnimName_Cam_63, tlAnimName_Money_63, tlAnimName_Duffel_63)
						ENDIF
					ELIF SHOULD_HANDHELD_DRILL_MINIGAME_REST(sHandHeldDrillPassedIn)
						PRINTLN("[LM][HandHeldDrill] - PROCESS_HANDHELD_DRILL_MINIGAME_DRILLING - We should be RESTING.")
						BUILD_HANDHELD_DRILL_MINIGAME_ANIM_NAMES(sHandHeldDrillPassedIn, "REST", tlAnimName_63, tlAnimName_Drill_63, tlAnimName_Cabinet_63, tlAnimName_Cam_63, tlAnimName_Money_63, tlAnimName_Duffel_63)
						IF NOT IS_HANDHELD_DRILL_MINIGAME_SYNC_SCENE_PLAYING(sHandHeldDrillPassedIn, tlAnimName_63)				
							START_HANDHELD_DRILL_MINIGAME_SYNC_SCENE(sHandHeldDrillPassedIn, tlAnimDict_63, tlAnimName_63, tlAnimName_Drill_63, tlAnimName_Cabinet_63, tlAnimName_Cam_63, tlAnimName_Money_63, tlAnimName_Duffel_63)
						ENDIF
					ELSE
						PRINTLN("[LM][HandHeldDrill] - PROCESS_HANDHELD_DRILL_MINIGAME_DRILLING - We should be IDLING.")
						BUILD_HANDHELD_DRILL_MINIGAME_ANIM_NAMES(sHandHeldDrillPassedIn, "IDLE", tlAnimName_63, tlAnimName_Drill_63, tlAnimName_Cabinet_63, tlAnimName_Cam_63, tlAnimName_Money_63, tlAnimName_Duffel_63)
						IF NOT IS_HANDHELD_DRILL_MINIGAME_SYNC_SCENE_PLAYING(sHandHeldDrillPassedIn, tlAnimName_63)
							START_HANDHELD_DRILL_MINIGAME_SYNC_SCENE(sHandHeldDrillPassedIn, tlAnimDict_63, tlAnimName_63, tlAnimName_Drill_63, tlAnimName_Cabinet_63, tlAnimName_Cam_63, tlAnimName_Money_63, tlAnimName_Duffel_63)
							REINIT_NET_TIMER(sHandHeldDrillPassedIn.tdMinigameIdleTimer)
						ENDIF
					ENDIF
				ENDIF
			ELIF IS_HANDHELD_DRILL_MINIGAME_DRILL_BUTTON_BEING_HELD()
			AND NOT IS_HANDHELD_DRILL_MINIGAME_READY_TO_PLAY_ACTION(sHandHeldDrillPassedIn)
			AND NOT IS_THIS_HANDHELD_DRILL_MINIGAME_LOCKBOX_COMPLETE(sHandHeldDrillPassedIn, sHandHeldDrillPassedIn.iLockboxDynamicCabinetSelected, sHandHeldDrillPassedIn.iLockboxSelected)
				BUILD_HANDHELD_DRILL_MINIGAME_ANIM_NAMES(sHandHeldDrillPassedIn, "TRANS_TO_REST", tlAnimName_63, tlAnimName_Drill_63, tlAnimName_Cabinet_63, tlAnimName_Cam_63, tlAnimName_Money_63, tlAnimName_Duffel_63)
				IF NOT IS_HANDHELD_DRILL_MINIGAME_SYNC_SCENE_PLAYING(sHandHeldDrillPassedIn, tlAnimName_63)
				OR (IS_HANDHELD_DRILL_MINIGAME_SYNC_SCENE_PLAYING(sHandHeldDrillPassedIn, tlAnimName_63) AND GET_SYNC_SCENE_PHASE(sHandHeldDrillPassedIn.iSyncScene) >= 0.99)
					REINIT_NET_TIMER(sHandHeldDrillPassedIn.tdMinigameIdleTimer)
					PRINTLN("[LM][HandHeldDrill] - PROCESS_HANDHELD_DRILL_MINIGAME_DRILLING - We should be transitiing to TRANS_TO_IDLE - TRANS.")
					BUILD_HANDHELD_DRILL_MINIGAME_ANIM_NAMES(sHandHeldDrillPassedIn, "TRANS_TO_IDLE", tlAnimName_63, tlAnimName_Drill_63, tlAnimName_Cabinet_63, tlAnimName_Cam_63, tlAnimName_Money_63, tlAnimName_Duffel_63)
					IF NOT IS_HANDHELD_DRILL_MINIGAME_SYNC_SCENE_PLAYING(sHandHeldDrillPassedIn, tlAnimName_63)
						START_HANDHELD_DRILL_MINIGAME_SYNC_SCENE(sHandHeldDrillPassedIn, tlAnimDict_63, tlAnimName_63, tlAnimName_Drill_63, tlAnimName_Cabinet_63, tlAnimName_Cam_63, tlAnimName_Money_63, tlAnimName_Duffel_63)						
					ENDIF
				ENDIF
			ELIF (IS_HANDHELD_DRILL_MINIGAME_DRILL_BUTTON_BEING_HELD() OR IS_THIS_HANDHELD_DRILL_MINIGAME_LOCKBOX_COMPLETE(sHandHeldDrillPassedIn, sHandHeldDrillPassedIn.iLockboxDynamicCabinetSelected, sHandHeldDrillPassedIn.iLockboxSelected))
			AND IS_HANDHELD_DRILL_MINIGAME_READY_TO_PLAY_ACTION(sHandHeldDrillPassedIn)
			AND (NOT HAS_NET_TIMER_STARTED(sHandHeldDrillPassedIn.tdMinigameDrillToggleDelay) OR HAS_NET_TIMER_EXPIRED_READ_ONLY(sHandHeldDrillPassedIn.tdMinigameDrillToggleDelay, ciTIME_FOR_DRILL_ANIM_TOGGLE_DLEAY))
				PRINTLN("[LM][HandHeldDrill] - PROCESS_HANDHELD_DRILL_MINIGAME_DRILLING - Button held. Let's DRILL!")
				
				REINIT_NET_TIMER(sHandHeldDrillPassedIn.tdMinigameIdleTimer)
				BUILD_HANDHELD_DRILL_MINIGAME_ANIM_NAMES(sHandHeldDrillPassedIn, "ACTION", tlAnimName_63, tlAnimName_Drill_63, tlAnimName_Cabinet_63, tlAnimName_Cam_63, tlAnimName_Money_63, tlAnimName_Duffel_63)				
				IF NOT IS_HANDHELD_DRILL_MINIGAME_SYNC_SCENE_PLAYING(sHandHeldDrillPassedIn, tlAnimName_63)					
					START_HANDHELD_DRILL_MINIGAME_SYNC_SCENE(sHandHeldDrillPassedIn, tlAnimDict_63, tlAnimName_63, tlAnimName_Drill_63, tlAnimName_Cabinet_63, tlAnimName_Cam_63, tlAnimName_Money_63, tlAnimName_Duffel_63)
					RESET_NET_TIMER(sHandHeldDrillPassedIn.tdDrillingTimer)
					START_NET_TIMER(sHandHeldDrillPassedIn.tdDrillingTimer)
				ELSE
					SET_CONTROL_SHAKE(PLAYER_CONTROL, 250, 200)
					CREATE_HANDHELD_DRILL_MINIGAME_PARTICLE_EFFECT_FOR_DRILL(sHandHeldDrillPassedIn)
					PLAY_HANDHELD_DRILL_MINIGAME_SOUND_EFFECT(sHandHeldDrillPassedIn)					

					IF NOT IS_BIT_SET(MC_PlayerBD[ilocalPart].iClientBitSet4, PBBOOL4_IS_USING_THE_HANDHELD_DRILL)
						SET_BIT(MC_PlayerBD[ilocalPart].iClientBitSet4, PBBOOL4_IS_USING_THE_HANDHELD_DRILL)
					ENDIF
					
					IF HAS_NET_TIMER_EXPIRED_READ_ONLY(sHandHeldDrillPassedIn.tdDrillingTimer, 50)
						INCREMENT_CURRENT_HANDHELD_DRILL_MINIGAME_LOCKBOX_COMPLETION(sHandHeldDrillPassedIn)
						RESET_NET_TIMER(sHandHeldDrillPassedIn.tdDrillingTimer)
						START_NET_TIMER(sHandHeldDrillPassedIn.tdDrillingTimer)
						
						IF IS_THIS_HANDHELD_DRILL_MINIGAME_LOCKBOX_COMPLETE(sHandHeldDrillPassedIn, sHandHeldDrillPassedIn.iLockboxDynamicCabinetSelected, sHandHeldDrillPassedIn.iLockboxSelected)
							PRINTLN("[LM][HandHeldDrill] - PROCESS_HANDHELD_DRILL_MINIGAME_DRILLING - TASK_PLAY_ANIM - Lockbox is complete - 100%")
							STOP_HANDHELD_DRILL_MINIGAME_PARTICLE_EFFECT_FOR_DRILL(sHandHeldDrillPassedIn)
							STOP_HANDHELD_DRILL_MINIGAME_SOUND_EFFECT(sHandHeldDrillPassedIn)
							CLEAR_BIT(MC_PlayerBD[ilocalPart].iClientBitSet4, PBBOOL4_IS_USING_THE_HANDHELD_DRILL)
							SET_HANDHELD_DRILL_MINIGAME_DRILLING_STATE(sHandHeldDrillPassedIn.eDrillingState, HANDHELD_DRILL_MINIGAME_DRILLING_STATE_FINISHING)
						ELSE
							PRINTLN("[LM][HandHeldDrill] - PROCESS_HANDHELD_DRILL_MINIGAME_DRILLING - TASK_PLAY_ANIM - Lockbox is now at: ", GET_CURRENT_HANDHELD_DRILL_MINIGAME_LOCKBOX_COMPLETION(sHandHeldDrillPassedIn), "% completion")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_HANDHELD_DRILL_MINIGAME_DRILL_BUTTON_BEING_HELD()
				IF IS_BIT_SET(MC_PlayerBD[ilocalPart].iClientBitSet4, PBBOOL4_IS_USING_THE_HANDHELD_DRILL)
					RESET_NET_TIMER(sHandHeldDrillPassedIn.tdMinigameDrillToggleDelay)
					START_NET_TIMER(sHandHeldDrillPassedIn.tdMinigameDrillToggleDelay)
					CLEAR_BIT(MC_PlayerBD[ilocalPart].iClientBitSet4, PBBOOL4_IS_USING_THE_HANDHELD_DRILL)
				ENDIF
				STOP_HANDHELD_DRILL_MINIGAME_PARTICLE_EFFECT_FOR_DRILL(sHandHeldDrillPassedIn)
				STOP_HANDHELD_DRILL_MINIGAME_SOUND_EFFECT(sHandHeldDrillPassedIn)
			ENDIF
			
		BREAK
		
		// Award and anim
		CASE HANDHELD_DRILL_MINIGAME_DRILLING_STATE_FINISHING
		
			IF NOT IS_BIT_SET(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_CheckLockboxHasAReward)
				SET_BIT(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_CheckLockboxHasAReward)	
				SET_RANDOM_SEED(NATIVE_TO_INT(GET_NETWORK_TIME()))
				IF GET_RANDOM_INT_IN_RANGE(0, 100) < g_sMPTunables.iCH_VAULT_SAFETY_DEPOSIT_BOX_VALUE_LOOT_CHANCE
					SET_BIT(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_LockboxHasAReward)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_LockboxHasAReward)
				BUILD_HANDHELD_DRILL_MINIGAME_ANIM_NAMES(sHandHeldDrillPassedIn, "REWARD", tlAnimName_63, tlAnimName_Drill_63, tlAnimName_Cabinet_63, tlAnimName_Cam_63, tlAnimName_Money_63, tlAnimName_Duffel_63)
			ELSE
				BUILD_HANDHELD_DRILL_MINIGAME_ANIM_NAMES(sHandHeldDrillPassedIn, "NO_REWARD", tlAnimName_63, tlAnimName_Drill_63, tlAnimName_Cabinet_63, tlAnimName_Cam_63, tlAnimName_Money_63, tlAnimName_Duffel_63)
			ENDIF
			IF IS_HANDHELD_DRILL_MINIGAME_SYNC_SCENE_PLAYING(sHandHeldDrillPassedIn, tlAnimName_63)
				IF IS_BIT_SET(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_LockboxHasAReward)
					IF GET_SYNC_SCENE_PHASE(sHandHeldDrillPassedIn.iSyncScene) >= 0.65
						IF NOT IS_BIT_SET(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_LockboxGivenReward)
							SET_BIT(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_LockboxGivenReward)
							BROADCAST_SCRIPT_EVENT_SAFETY_DEPOSIT_BOX_LOOTED(sHandHeldDrillPassedIn.iLockboxSelected, sHandHeldDrillPassedIn.iLockboxDynamicCabinetSelected, iLocalPart)
							g_TransitionSessionNonResetVars.bInvolvedInCashGrab = TRUE //For the cash drop logic
							INCREMENT_OFF_RULE_MINIGAME_SCORE(ciOffRuleScore_Low)
						ENDIF
					ENDIF
				ENDIF
				
				IF iLockbox >= 3
					PROCESS_HANDHELD_DRILL_MINIGAME_HIDE_ANIM_OBJECTS(sHandHeldDrillPassedIn)					
				ENDIF
				
				IF GET_SYNC_SCENE_PHASE(sHandHeldDrillPassedIn.iSyncScene) >= 0.99								
					SET_HANDHELD_DRILL_MINIGAME_DRILLING_STATE(sHandHeldDrillPassedIn.eDrillingState, HANDHELD_DRILL_MINIGAME_DRILLING_STATE_SELECTING)
				ENDIF
			ELSE
				START_HANDHELD_DRILL_MINIGAME_SYNC_SCENE(sHandHeldDrillPassedIn, tlAnimDict_63, tlAnimName_63, tlAnimName_Drill_63, tlAnimName_Cabinet_63, tlAnimName_Cam_63, tlAnimName_Money_63, tlAnimName_Duffel_63, iLockbox >= 3)
			ENDIF
		BREAK
		
		// Exit the minigame
		CASE HANDHELD_DRILL_MINIGAME_DRILLING_STATE_EXITTING			
			IF IS_BIT_SET(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_LockboxExittingFinished)
				SET_HANDHELD_DRILL_MINIGAME_DRILLING_STATE(sHandHeldDrillPassedIn.eDrillingState, HANDHELD_DRILL_MINIGAME_DRILLING_STATE_CLEANUP)
				RETURN FALSE
			ENDIF
			
			IF NOT IS_HANDHELD_DRILL_MINIGAME_READY_TO_PLAY_EXIT(sHandHeldDrillPassedIn)
				PRINTLN("[LM][HandHeldDrill] - PROCESS_HANDHELD_DRILL_MINIGAME_DRILLING - Exitting state is Returning False - Waiting for transition anims to finish.")
				RETURN FALSE
			ENDIF
			
			IF SHOULD_HANDHELD_DRILL_MINIGAME_PLAY_REST_EXIT(sHandHeldDrillPassedIn)
				BUILD_HANDHELD_DRILL_MINIGAME_ANIM_NAMES(sHandHeldDrillPassedIn, "REST_EXIT", tlAnimName_63, tlAnimName_Drill_63, tlAnimName_Cabinet_63, tlAnimName_Cam_63, tlAnimName_Money_63, tlAnimName_Duffel_63)
			ELSE
				BUILD_HANDHELD_DRILL_MINIGAME_ANIM_NAMES(sHandHeldDrillPassedIn, "EXIT", tlAnimName_63, tlAnimName_Drill_63, tlAnimName_Cabinet_63, tlAnimName_Cam_63, tlAnimName_Money_63, tlAnimName_Duffel_63)
			ENDIF
			
			IF IS_HANDHELD_DRILL_MINIGAME_SYNC_SCENE_PLAYING(sHandHeldDrillPassedIn, tlAnimName_63)	
				BOOL bFinished 
				FLOAT fScenePhase 
				bFinished = HAS_ANIM_EVENT_FIRED(sHandHeldDrillPassedIn.pedDrill, GET_HASH_KEY("BREAKOUT"))
				fScenePhase = GET_SYNC_SCENE_PHASE(sHandHeldDrillPassedIn.iSyncScene)
				
				PROCESS_HANDHELD_DRILL_MINIGAME_HIDE_ANIM_OBJECTS(sHandHeldDrillPassedIn)
				
				IF fScenePhase >= 0.99
				OR bFinished
					PRINTLN("[LM][HandHeldDrill] - PROCESS_HANDHELD_DRILL_MINIGAME_DRILLING - bFinished: ", bFinished, " Phase: ", fScenePhase)
					SET_HANDHELD_DRILL_MINIGAME_DRILLING_STATE(sHandHeldDrillPassedIn.eDrillingState, HANDHELD_DRILL_MINIGAME_DRILLING_STATE_CLEANUP)
				ENDIF
			ELSE
				START_HANDHELD_DRILL_MINIGAME_SYNC_SCENE(sHandHeldDrillPassedIn, tlAnimDict_63, tlAnimName_63, tlAnimName_Drill_63, tlAnimName_Cabinet_63, tlAnimName_Cam_63, tlAnimName_Money_63, tlAnimName_Duffel_63, TRUE)
			ENDIF
		BREAK
		
		// Full cleanup
		CASE HANDHELD_DRILL_MINIGAME_DRILLING_STATE_CLEANUP		
			IF DOES_CAM_EXIST(sHandHeldDrillPassedIn.camDrillCam) // just incase.
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				RENDER_SCRIPT_CAMS(FALSE, TRUE, 500)
				DESTROY_CAM(sHandHeldDrillPassedIn.camDrillCam)
			ENDIF
				
			IF sHandHeldDrillPassedIn.iSyncScene > -1				
				NETWORK_STOP_SYNCHRONISED_SCENE(sHandHeldDrillPassedIn.iSyncScene)								
				sHandHeldDrillPassedIn.iSyncScene =- 1
			ENDIF
			
			BUILD_HANDHELD_DRILL_MINIGAME_ANIM_NAMES(sHandHeldDrillPassedIn, "IDLE", sHandHeldDrillPassedIn.tl63_AnimPlaying, tlAnimName_Drill_63, tlAnimName_Cabinet_63, tlAnimName_Cam_63, tlAnimName_Money_63, tlAnimName_Duffel_63)
			PLAY_ENTITY_ANIM(sHandHeldDrillPassedIn.oiCabinet, tlAnimName_Cabinet_63, tlAnimDict_63, 0.0, TRUE, TRUE, FALSE, 0.99)
			
			IF NOT IS_BIT_SET(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_ShowDuffelBag) // Contingency.
				BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_ShowHeistBagForPart)
			ENDIF
			
			STOP_HANDHELD_DRILL_MINIGAME_PARTICLE_EFFECT_FOR_DRILL(sHandHeldDrillPassedIn)
			STOP_HANDHELD_DRILL_MINIGAME_SOUND_EFFECT(sHandHeldDrillPassedIn)
							
			IF DOES_ENTITY_EXIST(sHandHeldDrillPassedIn.oiDrill)
				DELETE_OBJECT(sHandHeldDrillPassedIn.oiDrill)
			ENDIF
			IF DOES_ENTITY_EXIST(sHandHeldDrillPassedIn.oiDrillBag)
				DELETE_OBJECT(sHandHeldDrillPassedIn.oiDrillBag)
			ENDIF
			IF DOES_ENTITY_EXIST(sHandHeldDrillPassedIn.oiMoneyBag)
				DELETE_OBJECT(sHandHeldDrillPassedIn.oiMoneyBag)
			ENDIF
			IF DOES_ENTITY_EXIST(sHandHeldDrillPassedIn.oiCabinet)
				sHandHeldDrillPassedIn.oiCabinet = NULL
			ENDIF
			
			sHandHeldDrillPassedIn.iLockboxSelected = 0
			sHandHeldDrillPassedIn.tl63_AnimPlaying = ""
			RESET_NET_TIMER(sHandHeldDrillPassedIn.tdDrillCameraSwitchTimer)
			CLEAR_BIT(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_LockboxExittingFinished)
			CLEAR_BIT(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_CheckLockboxHasAReward)	
			CLEAR_BIT(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_LockboxHasAReward)
			CLEAR_BIT(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_LockboxGivenReward)
			CLEAR_BIT(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_InitializedDrillingSettings)
			CLEAR_BIT(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_HideDuffelBag)
			CLEAR_BIT(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_ShowDuffelBag)			
			CLEAR_BIT(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_ShowAnimDrill)
			CLEAR_BIT(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_HideAnimDrill)
			CLEAR_BIT(MC_PlayerBD[ilocalPart].iClientBitSet4, PBBOOL4_IS_USING_THE_HANDHELD_DRILL)
			CLEAR_BIT(iLocalBoolCheck32, LBOOL32_USING_HANDHELD_DRILL)
			
			SET_PED_CURRENT_WEAPON_VISIBLE(sHandHeldDrillPassedIn.pedDrill, TRUE, FALSE)
			CLEAR_PED_TASKS(sHandHeldDrillPassedIn.pedDrill)
			FREEZE_ENTITY_POSITION(sHandHeldDrillPassedIn.pedDrill, FALSE)
			SET_HANDHELD_DRILL_MINIGAME_DRILLING_STATE(sHandHeldDrillPassedIn.eDrillingState, HANDHELD_DRILL_MINIGAME_DRILLING_STATE_WAITING)
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

#IF IS_DEBUG_BUILD
PROC PROCESS_HANDHELD_DRILL_MINIGAME_DEBUG(STRUCT_HANDHELD_DRILL_MINI_GAME &sHandHeldDrillPassedIn)
	IF NOT bHandheldDrillMinigameDebug
		EXIT
	ENDIF
	#IF FEATURE_CASINO_HEIST
	INT i = 0
	
	/*PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("Main State: ", DEBUG_GET_HANDHELD_DRILL_MINIGAME_STATE_NAME(sHandHeldDrillPassedIn.eDrillMinigameState), vBlankDbg)
	PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("Drill State: ", DEBUG_GET_HANDHELD_DRILL_MINIGAME_DRILLING_STATE_NAME(sHandHeldDrillPassedIn.eDrillingState), vBlankDbg)
	PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("DepositBoxMiniGameConfiguration: ", "", vBlankDbg, MC_ServerBD_2.iSafetyDepositBoxMiniGameConfiguration)
	PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("DepositBoxMiniGamePatternPreset: ", "", vBlankDbg, MC_ServerBD_2.iSafetyDepositBoxMiniGamePatternPreset)*/
	
	SWITCH sHandHeldDrillPassedIn.eDrillMinigameState
		CASE HANDHELD_DRILL_MINIGAME_STATE_IDLE	
		
		BREAK
		CASE HANDHELD_DRILL_MINIGAME_STATE_INIT
			
		BREAK
		CASE HANDHELD_DRILL_MINIGAME_STATE_WAITING
			VECTOR vDir
			VECTOR vPos
			FLOAT fHead
			VECTOR vDesiredPos
			FLOAT fDesiredHead
			VECTOR vBoxPos
			INT iDynamicIndex
			INT r,g,b
			INT iPattern	
			INT iSubPattern
			FOR i = 0 TO ciMAX_NUMBER_OF_HANDHELD_DRILL_LOCKBOX_CABINETS_TOTAL-1
				IF IS_HANDHELD_DRILL_MINIGAME_CABINET_A_LOCKBOX(i, sHandHeldDrillPassedIn.iLockboxConfiguration)
					iPattern	= sHandHeldDrillPassedIn.iLockboxCabinetPattern[iDynamicIndex]
					iSubPattern = sHandHeldDrillPassedIn.iLockboxCabinetSubPattern[iDynamicIndex]
					fHead = GET_HEADING_FOR_LOCKBOX_CABINET_IN_HANDHELD_DRILL_MINIGAME_WITH_INDEX(i)
					vPos = GET_POSITION_FOR_LOCKBOX_CABINET_IN_HANDHELD_DRILL_MINIGAME_WITH_INDEX(i, iPattern, iSubPattern)
					fDesiredHead = GET_PED_REQUIRED_HEADING_FOR_LOCKBOX_CABINET_IN_HANDHELD_DRILL_MINIGAME_WITH_INDEX(i)
					vDesiredPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_POSITION_FOR_LOCKBOX_CABINET_IN_HANDHELD_DRILL_MINIGAME_WITH_INDEX(i, iPattern, iSubPattern), fDesiredHead, <<0.0, -1.75, 0.0>>)
					
					vDir = NORMALISE_VECTOR(<<-SIN(fHead), COS(fHead), 0.0>>)
					vPos.z += 0.3
					IF IS_THIS_HANDHELD_DRILL_MINIGAME_CABINET_COMPLETE(sHandHeldDrillPassedIn, iDynamicIndex)
						r = 200
						g = 200
						b = 200
					ELSE
						r = 200
						g = 100
						b = 100
					ENDIF
					DRAW_MARKER(MARKER_ARROW, vPos, vDir, <<90.0, 0.0, 0.0>>, <<1, 1, 1>>, r, g, b, 150)
					
					vDir = NORMALISE_VECTOR(<<-SIN(fDesiredHead), COS(fDesiredHead), 0.0>>)				
					vDesiredPos.z += 0.3
					IF IS_THIS_HANDHELD_DRILL_MINIGAME_CABINET_COMPLETE(sHandHeldDrillPassedIn, iDynamicIndex)
						r = 200
						g = 200
						b = 200
					ELSE
						r = 100
						g = 200
						b = 100
					ENDIF
					DRAW_MARKER(MARKER_ARROW, vDesiredPos, vDir, <<90.0, 0.0, 0.0>>, <<1, 1, 1>>, r, g, b, 150)					
				ENDIF
				IF GET_HANDHELD_DRILL_MINIGAME_CABINET_TYPE(i, sHandHeldDrillPassedIn.iLockboxConfiguration) != HANDHELD_DRILL_MINIGAME_CABINET_TYPE_STATIC
					IF GET_HANDHELD_DRILL_MINIGAME_CABINET_TYPE(i, sHandHeldDrillPassedIn.iLockboxConfiguration) = HANDHELD_DRILL_MINIGAME_CABINET_TYPE_PAINTING
					AND (g_sCasinoHeistMissionConfigData.eTarget = CASINO_HEIST_TARGET_TYPE__ART OR GET_PLAYER_CASINO_HEIST_TARGET(GB_GET_LOCAL_PLAYER_GANG_BOSS()) = CASINO_HEIST_TARGET_TYPE__ART)
						r = 200
						g = 50
						b = 50
					ELIF GET_HANDHELD_DRILL_MINIGAME_CABINET_TYPE(i, sHandHeldDrillPassedIn.iLockboxConfiguration) = HANDHELD_DRILL_MINIGAME_CABINET_TYPE_PAINTING
						r = 50
						g = 50
						b = 200
					ELIF IS_HANDHELD_DRILL_MINIGAME_CABINET_A_LOCKBOX(i, sHandHeldDrillPassedIn.iLockboxConfiguration)
						r = 50
						g = 200
						b = 50
					ENDIF
					vBoxPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_POSITION_FOR_LOCKBOX_CABINET_IN_HANDHELD_DRILL_MINIGAME_WITH_INDEX(i), fHead, <<0.0, 1.0, 0.0>>)
					DRAW_MARKER(MARKER_BOXES, vBoxPos, vDir, (<<0.0, 0.0, 0.0>>), (<<2.0, 2.0, 4.0>>), r, g, b, 40)
				ENDIF
				IF GET_HANDHELD_DRILL_MINIGAME_CABINET_TYPE(i, sHandHeldDrillPassedIn.iLockboxConfiguration) != HANDHELD_DRILL_MINIGAME_CABINET_TYPE_STATIC
					iDynamicIndex++
				ENDIF
			ENDFOR
		BREAK
		CASE HANDHELD_DRILL_MINIGAME_DRILL_POSITIONING
		
		BREAK
		CASE HANDHELD_DRILL_MINIGAME_DRILL_READY_TO_DRILL
			
		BREAK
		CASE HANDHELD_DRILL_MINIGAME_DRILL_FINISHED
		
		BREAK
		CASE HANDHELD_DRILL_MINIGAME_DRILL_CLEANUP
			
		BREAK
	ENDSWITCH
	#ENDIF
ENDPROC
#ENDIF

// functionalized initialization.
PROC INITIALIZE_HANDHELD_DRILL_MINIGAME_SERVER_DATA(STRUCT_HANDHELD_DRILL_MINI_GAME &sHandHeldDrillPassedIn)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_EnableHandheldDrillMinigame)
		EXIT
	ENDIF
	
	PRINTLN("[LM][HandHeldDrill] - INITIALIZE_HANDHELD_DRILL_MINIGAME_SERVER_DATA - Initializing Settings...")
	
	INT iSpawnGroup = g_FMMC_STRUCT.iHandheldDrillingMinigameSpawnGroup
	
	// Save the variant entity set used.
	IF MC_ServerBD_2.iSafetyDepositBoxMiniGameConfiguration = -1		// 0 and 2 for non art.
		IF g_sCasinoHeistMissionConfigData.eTarget = CASINO_HEIST_TARGET_TYPE__ART
		OR GET_PLAYER_CASINO_HEIST_TARGET(GB_GET_LOCAL_PLAYER_GANG_BOSS()) = CASINO_HEIST_TARGET_TYPE__ART
			PRINTLN("[LM][HandHeldDrill] - INITIALIZE_HANDHELD_DRILL_MINIGAME_SERVER_DATA - Using Art Presets")
			IF IS_BIT_SET(MC_ServerBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[iSpawnGroup], 0)
				MC_ServerBD_2.iSafetyDepositBoxMiniGameConfiguration = 0
			ELIF IS_BIT_SET(MC_ServerBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[iSpawnGroup], 2)
				MC_ServerBD_2.iSafetyDepositBoxMiniGameConfiguration = 1
			ELIF IS_BIT_SET(MC_ServerBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[iSpawnGroup], 1)
				MC_ServerBD_2.iSafetyDepositBoxMiniGameConfiguration = 2
			ELIF IS_BIT_SET(MC_ServerBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[iSpawnGroup], 3)
				MC_ServerBD_2.iSafetyDepositBoxMiniGameConfiguration = 3
			ELSE
				PRINTLN("[LM][HandHeldDrill] - INITIALIZE_HANDHELD_DRILL_MINIGAME_SERVER_DATA - No sub spawn  group chosen!!!!")
				ASSERTLN("[LM][HandHeldDrill] - INITIALIZE_HANDHELD_DRILL_MINIGAME_SERVER_DATA - No sub spawn  group chosen!!!!")
			ENDIF
		ELSE
			PRINTLN("[LM][HandHeldDrill] - INITIALIZE_HANDHELD_DRILL_MINIGAME_SERVER_DATA - Not using Art Presets")
			IF IS_BIT_SET(MC_ServerBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[iSpawnGroup], 0)
			OR IS_BIT_SET(MC_ServerBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[iSpawnGroup], 2)
				MC_ServerBD_2.iSafetyDepositBoxMiniGameConfiguration = 0
			ELIF IS_BIT_SET(MC_ServerBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[iSpawnGroup], 1)
			OR IS_BIT_SET(MC_ServerBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[iSpawnGroup], 3)
				MC_ServerBD_2.iSafetyDepositBoxMiniGameConfiguration = 2
			ELSE
				PRINTLN("[LM][HandHeldDrill] - INITIALIZE_HANDHELD_DRILL_MINIGAME_SERVER_DATA - No sub spawn  group chosen!!!!")
				ASSERTLN("[LM][HandHeldDrill] - INITIALIZE_HANDHELD_DRILL_MINIGAME_SERVER_DATA - No sub spawn  group chosen!!!!")
			ENDIF
		ENDIF
		
		PRINTLN("[LM][HandHeldDrill] - INITIALIZE_HANDHELD_DRILL_MINIGAME_SERVER_DATA - iEntitySpawnSeed_SubGroupBS: ", MC_ServerBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[0])
	ENDIF
	
	// Save the Patterns that are used for the cabinets
	IF MC_ServerBD_2.iSafetyDepositBoxMiniGamePatternPreset = -1
		SET_RANDOM_SEED(NATIVE_TO_INT(GET_NETWORK_TIME()))
		MC_ServerBD_2.iSafetyDepositBoxMiniGamePatternPreset = GET_RANDOM_INT_IN_RANGE(0, 4)
	ENDIF
	
	PRINTLN("[LM][HandHeldDrill] - INITIALIZE_HANDHELD_DRILL_MINIGAME_SERVER_DATA - iSafetyDepositBoxMiniGameConfiguration: ", MC_ServerBD_2.iSafetyDepositBoxMiniGameConfiguration, " iSafetyDepositBoxMiniGamePatternPreset: ", MC_ServerBD_2.iSafetyDepositBoxMiniGamePatternPreset)
	
	sHandHeldDrillPassedIn.pedDrill = localPlayerPed
ENDPROC
PROC INITIALIZE_HANDHELD_DRILL_MINIGAME(STRUCT_HANDHELD_DRILL_MINI_GAME &sHandHeldDrillPassedIn)
	PRINTLN("[LM][HandHeldDrill] - INITIALIZE_HANDHELD_DRILL_MINIGAME - Initialized Settings...")
	
	g_TransitionSessionNonResetVars.sMissionSafetyDepositBoxMG.iSafetyDepositBoxMinigameVariant = MC_ServerBD_2.iSafetyDepositBoxMiniGameConfiguration
	sHandHeldDrillPassedIn.iLockboxConfiguration = MC_ServerBD_2.iSafetyDepositBoxMiniGameConfiguration
	
	g_TransitionSessionNonResetVars.sMissionSafetyDepositBoxMG.iSafetyDepositBoxMinigamePatternPreset = MC_ServerBD_2.iSafetyDepositBoxMiniGamePatternPreset
	sHandHeldDrillPassedIn.iLockboxPatternPreset = MC_ServerBD_2.iSafetyDepositBoxMiniGamePatternPreset
		
	INT i = 0
	FOR i = 0 TO ciMAX_NUMBER_OF_HANDHELD_DRILL_LOCKBOX_CABINETS_DYNAMIC-1
		sHandHeldDrillPassedIn.iLockboxCabinetPattern[i] = GET_HANDHELD_DRILL_MINIGAME_CABINET_LOCKBOX_PATTERN(i, sHandHeldDrillPassedIn.iLockboxPatternPreset)
		sHandHeldDrillPassedIn.iLockboxCabinetSubPattern[i] = GET_HANDHELD_DRILL_MINIGAME_CABINET_LOCKBOX_POSITION(i, sHandHeldDrillPassedIn.iLockboxPatternPreset)
	ENDFOR
	
	sHandHeldDrillPassedIn.pedDrill = localPlayerPed
ENDPROC
PROC BLOCK_HANDHELD_DRILL_MINIGAME_PAUSE_MENU()
	PRINTLN("[LM][HandHeldDrill] - BLOCK_HANDHELD_DRILL_MINIGAME_PAUSE_MENU - Blocking Pause Menu.")	
	
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD, TRUE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR, TRUE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD, TRUE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR, TRUE)	
ENDPROC
// Every Frame Updates, no matter the state.
PROC UPDATE_HANDHELD_DRILL_MINIGAME(STRUCT_HANDHELD_DRILL_MINI_GAME &sHandHeldDrillPassedIn)
	sHandHeldDrillPassedIn.pedDrill = localPlayerPed
	
	PROCESS_HANDHELD_DRILL_MINIGAME_BY_REMOTE_CLIENTS(sHandHeldDrillPassedIn)
		
	#IF IS_DEBUG_BUILD
	PROCESS_HANDHELD_DRILL_MINIGAME_DEBUG(sHandHeldDrillPassedIn)
	#ENDIF
ENDPROC

PROC PROCESS_HANDHELD_DRILL_MINIGAME(STRUCT_HANDHELD_DRILL_MINI_GAME &sHandHeldDrillPassedIn)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_EnableHandheldDrillMinigame)
		EXIT
	ENDIF
	
	UPDATE_HANDHELD_DRILL_MINIGAME(sHandHeldDrillPassedIn)
	
	SWITCH sHandHeldDrillPassedIn.eDrillMinigameState
		
		CASE HANDHELD_DRILL_MINIGAME_STATE_IDLE			
			IF NOT IS_BIT_SET(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_RequestedAssets)
				SET_HANDHELD_DRILL_MINIGAME_STATE(sHandHeldDrillPassedIn.eDrillMinigameState, HANDHELD_DRILL_MINIGAME_STATE_INIT)
			ENDIF
		BREAK
		
		CASE HANDHELD_DRILL_MINIGAME_STATE_INIT
			IF REQUEST_HANDHELD_DRILL_MINIGAME_ASSETS()
				IF NOT IS_BIT_SET(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_InitializedSettings)
					INITIALIZE_HANDHELD_DRILL_MINIGAME(sHandHeldDrillPassedIn)
					SET_BIT(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_InitializedSettings)
				ENDIF
				IF CREATE_ASSETS_FOR_HANDHELD_DRILL_MINIGAME(sHandHeldDrillPassedIn)
					SET_BIT(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_RequestedAssets)
					SET_HANDHELD_DRILL_MINIGAME_STATE(sHandHeldDrillPassedIn.eDrillMinigameState, HANDHELD_DRILL_MINIGAME_STATE_WAITING)
				ENDIF
			ENDIF
		BREAK
		
		CASE HANDHELD_DRILL_MINIGAME_STATE_WAITING
			IF IS_PED_ABLE_TO_BEGIN_HANDHELD_DRILL_MINIGAME(sHandHeldDrillPassedIn.pedDrill)			
			AND IS_LOCATION_FOR_HANDHELD_DRILL_MINIGAME_VALID(sHandHeldDrillPassedIn.pedDrill, sHandHeldDrillPassedIn.iLockboxCabinetSelected, sHandHeldDrillPassedIn.iLockboxDynamicCabinetSelected, sHandHeldDrillPassedIn.iLockboxConfiguration)
			AND NOT IS_THIS_HANDHELD_DRILL_MINIGAME_CABINET_COMPLETE(sHandHeldDrillPassedIn, sHandHeldDrillPassedIn.iLockboxDynamicCabinetSelected)
				IF DOES_HANDHELD_DRILL_MINIGAME_PED_HAVE_A_DRILL()
					DISPLAY_HELP_TEXT_THIS_FRAME_FOR_MINIGAME("CAS_DRIL_HELP1", FALSE)
					IF HAS_TRIGGERED_TO_BEGIN_HANDHELD_DRILL_MINIGAME()
						BLOCK_HANDHELD_DRILL_MINIGAME_PAUSE_MENU()
						PRINTLN("[LM][HandHeldDrill] - PROCESS_HANDHELD_DRILL_MINIGAME - Actual Lockbox index: ", sHandHeldDrillPassedIn.iLockboxCabinetSelected, "/", ciMAX_NUMBER_OF_HANDHELD_DRILL_LOCKBOX_CABINETS_TOTAL,
							" but we have a dynamic index of ", sHandHeldDrillPassedIn.iLockboxDynamicCabinetSelected, "/", ciMAX_NUMBER_OF_HANDHELD_DRILL_LOCKBOX_CABINETS_DYNAMIC)				
						BROADCAST_SCRIPT_EVENT_OCCUPY_SAFETY_DEPOSIT_BOX_CABINET(sHandHeldDrillPassedIn.iLockboxDynamicCabinetSelected, iLocalPart, TRUE)
						SET_HANDHELD_DRILL_MINIGAME_STATE(sHandHeldDrillPassedIn.eDrillMinigameState, HANDHELD_DRILL_MINIGAME_DRILL_POSITIONING)
					ENDIF
				ELSE
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CAS_DRIL_HELP4")
					AND NOT IS_BIT_SET(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_PlayedMissingDrillHelpText)
						PRINTLN("[LM][HandHeldDrill] - PROCESS_HANDHELD_DRILL_MINIGAME - Printing 'no drill' help text.")
						SET_BIT(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_PlayedMissingDrillHelpText)
						PRINT_HELP("CAS_DRIL_HELP4", 5000)
					ENDIF
				ENDIF
			ELSE				
				IF IS_BIT_SET(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_PlayedMissingDrillHelpText)
					PRINTLN("[LM][HandHeldDrill] - PROCESS_HANDHELD_DRILL_MINIGAME - clearing 'no drill' help text.")
					CLEAR_BIT(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_PlayedMissingDrillHelpText)
				ENDIF				
			ENDIF
		BREAK
		
		CASE HANDHELD_DRILL_MINIGAME_DRILL_POSITIONING
			BLOCK_HANDHELD_DRILL_MINIGAME_PAUSE_MENU()
			IF NOT HAS_NET_TIMER_STARTED(sHandHeldDrillPassedIn.tdMinigameSafetyTimer)
				PRINTLN("[LM][HandHeldDrill] - PROCESS_HANDHELD_DRILL_MINIGAME_PED_POSITIONING - Starting Safety Timer")
				START_NET_TIMER(sHandHeldDrillPassedIn.tdMinigameSafetyTimer)		
			ELIF HAS_NET_TIMER_EXPIRED_READ_ONLY(sHandHeldDrillPassedIn.tdMinigameSafetyTimer, 7500)
				PRINTLN("[LM][HandHeldDrill] - PROCESS_HANDHELD_DRILL_MINIGAME_PED_POSITIONING - Safety Timer expired. We probably got stuck, running cleanup.")
				RESET_NET_TIMER(sHandHeldDrillPassedIn.tdMinigameSafetyTimer)
				SET_HANDHELD_DRILL_MINIGAME_STATE(sHandHeldDrillPassedIn.eDrillMinigameState, HANDHELD_DRILL_MINIGAME_DRILL_CLEANUP)
			ENDIF
			IF PROCESS_HANDHELD_DRILL_MINIGAME_PED_POSITIONING(sHandHeldDrillPassedIn)
				SET_HANDHELD_DRILL_MINIGAME_STATE(sHandHeldDrillPassedIn.eDrillMinigameState, HANDHELD_DRILL_MINIGAME_DRILL_READY_TO_DRILL)
			ENDIF
		BREAK
		
		CASE HANDHELD_DRILL_MINIGAME_DRILL_READY_TO_DRILL
			BLOCK_HANDHELD_DRILL_MINIGAME_PAUSE_MENU()
			IF PROCESS_HANDHELD_DRILL_MINIGAME_DRILLING(sHandHeldDrillPassedIn)
				SET_HANDHELD_DRILL_MINIGAME_STATE(sHandHeldDrillPassedIn.eDrillMinigameState, HANDHELD_DRILL_MINIGAME_DRILL_FINISHED)
			ENDIF
		BREAK
		
		CASE HANDHELD_DRILL_MINIGAME_DRILL_FINISHED
			BLOCK_HANDHELD_DRILL_MINIGAME_PAUSE_MENU()
			SET_HANDHELD_DRILL_MINIGAME_STATE(sHandHeldDrillPassedIn.eDrillMinigameState, HANDHELD_DRILL_MINIGAME_DRILL_CLEANUP)
		BREAK
		
		CASE HANDHELD_DRILL_MINIGAME_DRILL_CLEANUP
			BLOCK_HANDHELD_DRILL_MINIGAME_PAUSE_MENU()
			BROADCAST_SCRIPT_EVENT_OCCUPY_SAFETY_DEPOSIT_BOX_CABINET(sHandHeldDrillPassedIn.iLockboxDynamicCabinetSelected, iLocalPart, FALSE)			
			sHandHeldDrillPassedIn.iLockboxDynamicCabinetSelected = -1
			RESET_NET_TIMER(sHandHeldDrillPassedIn.tdDrillingTimer)
			RESET_NET_TIMER(sHandHeldDrillPassedIn.tdMinigameSafetyTimer)
			RESET_NET_TIMER(sHandHeldDrillPassedIn.tdMinigamePositionTimer)
			CLEAR_BIT(sHandHeldDrillPassedIn.iDrillMinigameBS, ciDrill_Bitset_StartedPositioningTask)
			SET_HANDHELD_DRILL_MINIGAME_STATE(sHandHeldDrillPassedIn.eDrillMinigameState, HANDHELD_DRILL_MINIGAME_STATE_WAITING)
			SET_HANDHELD_DRILL_MINIGAME_DRILLING_STATE(sHandHeldDrillPassedIn.eDrillingState, HANDHELD_DRILL_MINIGAME_DRILLING_STATE_WAITING)
		BREAK
		
	ENDSWITCH
	
ENDPROC



