
USING "FM_Mission_Controller_USING.sch"
USING "FM_Mission_Controller_Utility.sch"
USING "FM_Mission_Controller_Entities.sch"
USING "FM_Mission_Controller_ObjectiveCompletion.sch"
USING "FM_Mission_Controller_SpecialCase.sch"
USING "FM_Mission_Controller_Rules.sch"
USING "FM_Mission_Controller_Doors.sch"
USING "FM_Mission_Controller_Cutscene.sch"
USING "FM_Mission_Controller_Debug.sch"
USING "FM_Mission_Controller_Locations.sch"
USING "FM_Mission_Controller_Vehicles.sch"
USING "FM_Mission_Controller_Peds_Brain.sch"
USING "FM_Mission_Controller_Peds_Body.sch"
USING "FM_Mission_Controller_Peds.sch"



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: GANG CHASE !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************



FUNC BOOL IS_VEHICLE_FMMC_GANG_BACKUP_VEHICLE(VEHICLE_INDEX tempVeh)
	
	BOOL bBackupVeh
	INT iGangLoop
									
	FOR iGangLoop = 0 TO (MAX_BACKUP_VEHICLES - 1)
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.GangBackup[iGangLoop].BackupVeh)
		AND (tempVeh = NET_TO_VEH(MC_serverBD_1.GangBackup[iGangLoop].BackupVeh))
			bBackupVeh = TRUE
			iGangLoop = MAX_BACKUP_VEHICLES //Break out!
		ENDIF
	ENDFOR
	
	RETURN bBackupVeh
	
ENDFUNC

/// PURPOSE:
///    Can set different values for the maximum gang chase at any one time for CG/ NG
FUNC INT GET_GANG_CHASE_INTENSITY_FOR_THIS_GEN(INT iTeam #IF IS_DEBUG_BUILD , BOOL bDoDebug = FALSE #ENDIF)
	INT iIntensity = 3
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iteam]
	
	#IF IS_DEBUG_BUILD
		IF bDoDebug
			PRINTLN("[RCC MISSION] [gang_backup] [GET_GANG_CHASE_INTENSITY_FOR_THIS_GEN] Called, iTeam = ", iTeam, " iRule = ", iRule, " iIntensity = ",iIntensity)
		ENDIF
	#ENDIF
	
	IF IS_PS3_VERSION()
	OR IS_XBOX360_VERSION()
		
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangLGNum[iRule] <> 0
			iIntensity = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangLGNum[iRule]
			
			#IF IS_DEBUG_BUILD
				IF bDoDebug
					PRINTLN("[RCC MISSION] [gang_backup] [GET_GANG_CHASE_INTENSITY_FOR_THIS_GEN] Using last gen value, iTotalNumber = ", iIntensity)
				ENDIF
			#ENDIF
		ENDIF
	ELSE
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangNGNum[iRule] <> 0
			iIntensity = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangNGNum[iRule]
			
			#IF IS_DEBUG_BUILD
				IF bDoDebug
					PRINTLN("[RCC MISSION] [gang_backup] [GET_GANG_CHASE_INTENSITY_FOR_THIS_GEN] Using NG version, iTotalNumber = ", iIntensity)
				ENDIF
			#ENDIF
		ENDIF
	ENDIF
	
	RETURN iIntensity
ENDFUNC

/// PURPOSE:
///    Lose Gang Chase objective text is different depending on the chasing gang 
FUNC STRING GET_LOSE_GANG_BACKUP_OBJECTIVE_TEXT()
	STRING sObj
	
	INT iTeam 		= MC_playerBD[ iPartToUse].iteam
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[MC_serverBD_4.iCurrentHighestPriority[iteam]], ciBS_RULE3_GANG_CHASE_ALT_OBJECTIVE_TEXT)
		SWITCH g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangBackupType[MC_serverBD_4.iCurrentHighestPriority[iteam]]
			CASE ciBACKUP_TYPE_MERRYWEATHER      		sObj = "MC_LSE_MER"			BREAK
			CASE ciBACKUP_TYPE_LOST              		sObj = "MC_LSE_LOST"		BREAK
			CASE ciBACKUP_TYPE_LOST_SLAMVAN2           	sObj = "MC_LSE_LOST"		BREAK
			CASE ciBACKUP_TYPE_VAGOS             		sObj = "MC_LSE_VAG"			BREAK
			CASE ciBACKUP_TYPE_FAMILIES          		sObj = "MC_LSE_FAM"			BREAK
			CASE ciBACKUP_TYPE_PROFESSIONALS     		sObj = "MC_LSE_PROF"		BREAK
			CASE ciBACKUP_TYPE_BALLAS            		sObj = "MC_LSE_BALL"		BREAK
			CASE ciBACKUP_TYPE_SALVA             		sObj = "MC_LSE_SALV"		BREAK
			CASE ciBACKUP_TYPE_ALTRUISTS         		sObj = "MC_LSE_ALTR"		BREAK
			CASE ciBACKUP_TYPE_FIB               		sObj = "MC_LSE_FIB"			BREAK
			CASE ciBACKUP_TYPE_ONEIL_BROS        		sObj = "MC_LSE_ONL"			BREAK
			CASE ciBACKUP_TYPE_KOREANS           		sObj = "MC_LSE_KOR"			BREAK
			CASE ciBACKUP_TYPE_MERRYWEATHER_MESAS		sObj = "MC_LSE_MER"			BREAK
			CASE ciBACKUP_TYPE_KOREANS2          		sObj = "MC_LSE_KOR"			BREAK
			CASE ciBACKUP_TYPE_ARMY						sObj = "MC_LSE_ARMY"		BREAK
			CASE ciBACKUP_TYPE_ARMY2					sObj = "MC_LSE_ARMY"		BREAK
			CASE ciBACKUP_TYPE_LOWRIDER_VAGOS_BLA		sObj = "MC_LSE_VAG"		    BREAK
			CASE ciBACKUP_TYPE_LOWRIDER_VAGOS_EMP		sObj = "MC_LSE_VAG"		    BREAK
			CASE ciBACKUP_TYPE_LOWRIDER_BALLAS		    sObj = "MC_LSE_BALL"		BREAK
			CASE ciBACKUP_TYPE_NIGHTSHARK_BOGDAN_GOON
			CASE ciBACKUP_TYPE_INSURGENT_BOGDAN_GOON
			CASE ciBACKUP_TYPE_INSURGENT2_BOGDAN_GOON
			CASE ciBACKUP_TYPE_AIR_BUZZ_AVON_GOON
			CASE ciBACKUP_TYPE_NIGHTSHARK_AVON_GOON
			CASE ciBACKUP_TYPE_DUBSTA3_AVON_GOON
				sObj = "MC_LSE_BG"
				BREAK
			CASE ciBACKUP_TYPE_KAMACHO_PROFESSIONALS	sObj = "MC_LSE_PROF"		BREAK
			CASE ciBACKUP_TYPE_CONTENDER_PROFESSIONALS	sObj = "MC_LSE_PROF"		BREAK
			CASE ciBACKUP_TYPE_CARACARA_HILLBILLY		sObj = "MC_LSE_HILL"		BREAK
				
			DEFAULT
				NET_SCRIPT_ASSERT("GET_LOSE_GANG_BACKUP_OBJECTIVE_TEXT - failed to find backup type! Please tell Dave W")
			BREAK	
		ENDSWITCH
	ELSE
		SWITCH g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangBackupType[MC_serverBD_4.iCurrentHighestPriority[iteam]]
			CASE ciBACKUP_TYPE_MERRYWEATHER      		sObj = "MC_TKO_MER"			BREAK
			CASE ciBACKUP_TYPE_LOST              		sObj = "MC_TKO_LOST"		BREAK
			CASE ciBACKUP_TYPE_LOST_SLAMVAN2           	sObj = "MC_TKO_LOST"		BREAK
			CASE ciBACKUP_TYPE_VAGOS             		sObj = "MC_TKO_VAG"			BREAK
			CASE ciBACKUP_TYPE_FAMILIES          		sObj = "MC_TKO_FAM"			BREAK
			CASE ciBACKUP_TYPE_PROFESSIONALS     		sObj = "MC_TKO_PROF"		BREAK
			CASE ciBACKUP_TYPE_BALLAS            		sObj = "MC_TKO_BALL"		BREAK
			CASE ciBACKUP_TYPE_SALVA             		sObj = "MC_TKO_SALV"		BREAK
			CASE ciBACKUP_TYPE_ALTRUISTS         		sObj = "MC_TKO_ALTR"		BREAK
			CASE ciBACKUP_TYPE_FIB               		sObj = "MC_TKO_FIB"			BREAK
			CASE ciBACKUP_TYPE_ONEIL_BROS        		sObj = "MC_TKO_ONL"			BREAK
			CASE ciBACKUP_TYPE_KOREANS           		sObj = "MC_TKO_KOR"			BREAK
			CASE ciBACKUP_TYPE_MERRYWEATHER_MESAS		sObj = "MC_TKO_MER"			BREAK
			CASE ciBACKUP_TYPE_KOREANS2          		sObj = "MC_TKO_KOR"			BREAK
			CASE ciBACKUP_TYPE_ARMY						sObj = "MC_TKO_ARMY"		BREAK
			CASE ciBACKUP_TYPE_ARMY2					sObj = "MC_TKO_ARMY"		BREAK
			CASE ciBACKUP_TYPE_LOWRIDER_VAGOS_BLA		sObj = "MC_TKO_VAG"		    BREAK
			CASE ciBACKUP_TYPE_LOWRIDER_VAGOS_EMP		sObj = "MC_TKO_VAG"		    BREAK
			CASE ciBACKUP_TYPE_LOWRIDER_BALLAS		    sObj = "MC_TKO_BALL"		BREAK
			CASE ciBACKUP_TYPE_NIGHTSHARK_BOGDAN_GOON
			CASE ciBACKUP_TYPE_INSURGENT_BOGDAN_GOON
			CASE ciBACKUP_TYPE_INSURGENT2_BOGDAN_GOON
			CASE ciBACKUP_TYPE_AIR_BUZZ_AVON_GOON
			CASE ciBACKUP_TYPE_NIGHTSHARK_AVON_GOON
			CASE ciBACKUP_TYPE_DUBSTA3_AVON_GOON
				sObj = "MC_TKO_BG"		
				BREAK
			CASE ciBACKUP_TYPE_KAMACHO_PROFESSIONALS	sObj = "MC_TKO_PROF"		BREAK
			CASE ciBACKUP_TYPE_CONTENDER_PROFESSIONALS	sObj = "MC_TKO_PROF"		BREAK			
			CASE ciBACKUP_TYPE_CARACARA_HILLBILLY		sObj = "MC_TKO_HILL"		BREAK
			
			DEFAULT
				NET_SCRIPT_ASSERT("GET_LOSE_GANG_BACKUP_OBJECTIVE_TEXT - failed to find backup type! Please tell Dave W")
			BREAK	
		ENDSWITCH
	ENDIF
	RETURN sObj
ENDFUNC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: GANG CHASE TARGET PLAYER / SPAWN LOCATION CALCULATION !
//
//************************************************************************************************************************************************************



FUNC INT GET_TOTAL_NUMBER_OF_GANG_BACKUP_CHASING_ALL_TEAMS()
	INT iTeam
	INT iTotal = 0
	FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
		iTotal += MC_serverBD_1.iNumBackupChasing[iteam]
	ENDFOR
	
	RETURN iTotal
ENDFUNC

FUNC BOOL SHOULD_CHASE_BACKUP_SPAWN_FOR_TEAM(INT iteam)
	#IF IS_DEBUG_BUILD
		IF bWdGangChaseDebug
			PRINTLN("[RCC MISSION] [gang_backup] SHOULD_CHASE_BACKUP_SPAWN_FOR_TEAM (has limit number option) called with iteam = ", iteam)
			DEBUG_PRINTCALLSTACK()
		ENDIF
	#ENDIF
	
	BOOL bShouldSpawn = FALSE
	BOOL bRestrictTotalNumber = FALSE
	INT iIntensity	
	
	IF iOldbackuptimerPriority[iteam] != MC_serverBD_4.iCurrentHighestPriority[iteam]
		RESET_NET_TIMER(tdbackupdelay[iteam])
		iOldbackuptimerPriority[iteam] = MC_serverBD_4.iCurrentHighestPriority[iteam]
	ENDIF
	
	IF IS_TEAM_ACTIVE(iteam)
		IF MC_serverBD_4.iCurrentHighestPriority[iteam] < FMMC_MAX_RULES
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[MC_serverBD_4.iCurrentHighestPriority[iteam]], ciBS_RULE11_GANG_CHASE_TRIGGER_ON_SPEED_FAIL)
				IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet6, SBBOOL6_ALLOW_GANGCHASE_AFTER_SPEED_FAIL)
					#IF IS_DEBUG_BUILD
					IF bWdGangChaseDebug
						PRINTLN("SHOULD_CHASE_BACKUP_SPAWN_FOR_TEAM - Returning FALSE due to SBBOOL6_HOLD_BACK_GANGCHASE")
					ENDIF
					#ENDIF
					RETURN FALSE
				ENDIF
			ENDIF
			
			bRestrictTotalNumber = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iRuleBitsetThree[MC_serverBD_4.iCurrentHighestPriority[iteam]], ciBS_RULE3_LIMIT_GANG_CHASE_PER_VEHICLE)
			iIntensity = GET_GANG_CHASE_INTENSITY_FOR_THIS_GEN(iteam #IF IS_DEBUG_BUILD, bWdGangChaseDebug #ENDIF)
			
			#IF IS_DEBUG_BUILD
				IF bWdGangChaseDebug
					PRINTLN("[RCC MISSION] [gang_backup] SHOULD_CHASE_BACKUP_SPAWN_FOR_TEAM Hit 1 MC_serverBD_1.iNumBackupChasing[iteam] = ", MC_serverBD_1.iNumBackupChasing[iteam], "  g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangBackupNumber[MC_serverBD_4.iCurrentHighestPriority[iteam]] = ", iIntensity, " MC_serverBD_4.iCurrentHighestPriority[iteam] = ",MC_serverBD_4.iCurrentHighestPriority[iteam], " bRestrictTotalNumber = ", TRUE)
				ENDIF
			#ENDIF
			
			IF MC_serverBD_1.iNumBackupChasing[iteam] < iIntensity //g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangBackupNumber[MC_serverBD_4.iCurrentHighestPriority[iteam]] 
				#IF IS_DEBUG_BUILD
					IF bWdGangChaseDebug
						PRINTLN("[RCC MISSION] [gang_backup] SHOULD_CHASE_BACKUP_SPAWN_FOR_TEAM Hit 2")
					ENDIF
				#ENDIF
				
				IF NOT bRestrictTotalNumber
				OR (bRestrictTotalNumber AND GET_TOTAL_NUMBER_OF_GANG_BACKUP_CHASING_ALL_TEAMS() < iIntensity) //g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangBackupNumber[MC_serverBD_4.iCurrentHighestPriority[iteam]] )
					#IF IS_DEBUG_BUILD
						IF bWdGangChaseDebug
							PRINTLN("[RCC MISSION] [gang_backup] SHOULD_CHASE_BACKUP_SPAWN_FOR_TEAM Hit 16")
						ENDIF
					#ENDIF
				
					IF g_FMMC_STRUCT.sFMMCEndConditions[iteam].iGangBackupType[MC_serverBD_4.iCurrentHighestPriority[iteam]] != ciBACKUP_TYPE_NONE  
						#IF IS_DEBUG_BUILD
							IF bWdGangChaseDebug
								PRINTLN("[RCC MISSION] [gang_backup] SHOULD_CHASE_BACKUP_SPAWN_FOR_TEAM Hit 3")
							ENDIF
						#ENDIF
						
						IF IS_BIT_SET(MC_serverBD_1.iChaseAreaBitset[iteam],MC_serverBD_4.iCurrentHighestPriority[iteam])
							
							PRINTLN("[RCC MISSION] [gang_backup] SHOULD_CHASE_BACKUP_SPAWN_FOR_TEAM Hit 4 - MC_serverBD_1.iChaseAreaBitset set for team ",iteam," & rule ",MC_serverBD_4.iCurrentHighestPriority[iteam])
							
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iGangBackupAtMidBitset,MC_serverBD_4.iCurrentHighestPriority[iteam])
								#IF IS_DEBUG_BUILD
									IF bWdGangChaseDebug
										PRINTLN("[RCC MISSION] [gang_backup] SHOULD_CHASE_BACKUP_SPAWN_FOR_TEAM Hit 5")
									ENDIF
								#ENDIF
								IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iteam],MC_serverBD_4.iCurrentHighestPriority[iteam])
									#IF IS_DEBUG_BUILD
										IF bWdGangChaseDebug
											PRINTLN("[RCC MISSION] [gang_backup] SHOULD_CHASE_BACKUP_SPAWN_FOR_TEAM Hit 6")
										ENDIF
									#ENDIF
									IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangBackupDelay[MC_serverBD_4.iCurrentHighestPriority[iteam]] != 0
										#IF IS_DEBUG_BUILD
											IF bWdGangChaseDebug
												PRINTLN("[RCC MISSION] [gang_backup] SHOULD_CHASE_BACKUP_SPAWN_FOR_TEAM Hit 7")
											ENDIF
										#ENDIF
										IF NOT HAS_NET_TIMER_STARTED(tdbackupdelay[iteam])
											#IF IS_DEBUG_BUILD
												IF bWdGangChaseDebug
													PRINTLN("[RCC MISSION] [gang_backup] SHOULD_CHASE_BACKUP_SPAWN_FOR_TEAM Hit 8")
												ENDIF
											#ENDIF
											REINIT_NET_TIMER(tdbackupdelay[iteam])
										ELSE
											IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdbackupdelay[iteam]) > (g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangBackupDelay[MC_serverBD_4.iCurrentHighestPriority[iteam]]*1000)
												#IF IS_DEBUG_BUILD
													IF bWdGangChaseDebug
														PRINTLN("[RCC MISSION] [gang_backup] SHOULD_CHASE_BACKUP_SPAWN_FOR_TEAM Hit 9")
													ENDIF
												#ENDIF
												IF NOT HAS_NET_TIMER_STARTED(timeBetweenTeamBackup[iteam])
												OR (HAS_NET_TIMER_STARTED(timeBetweenTeamBackup[iteam]) AND HAS_NET_TIMER_EXPIRED((timeBetweenTeamBackup[iteam]), 1500))
													bShouldSpawn = TRUE
													PRINTLN("[RCC MISSION] [gang_backup] SHOULD_CHASE_BACKUP_SPAWN_FOR_TEAM Hit 17")
												ELSE
													PRINTLN("[RCC MISSION] [gang_backup] SHOULD_CHASE_BACKUP_SPAWN_FOR_TEAM Hit 18 - waiting for timeBetweenTeamBackup (Midpoint)")
												ENDIF
											ENDIF
										ENDIF
									ELSE
										#IF IS_DEBUG_BUILD
											IF bWdGangChaseDebug
												PRINTLN("[RCC MISSION] [gang_backup] SHOULD_CHASE_BACKUP_SPAWN_FOR_TEAM Hit 10")
											ENDIF
										#ENDIF
										IF NOT HAS_NET_TIMER_STARTED(timeBetweenTeamBackup[iteam])
										OR (HAS_NET_TIMER_STARTED(timeBetweenTeamBackup[iteam]) AND HAS_NET_TIMER_EXPIRED((timeBetweenTeamBackup[iteam]), 1500))
											bShouldSpawn = TRUE
											PRINTLN("[RCC MISSION] [gang_backup] SHOULD_CHASE_BACKUP_SPAWN_FOR_TEAM Hit 25")
										ELSE
											PRINTLN("[RCC MISSION] [gang_backup] SHOULD_CHASE_BACKUP_SPAWN_FOR_TEAM Hit 26 - waiting for timeBetweenTeamBackup - mid point - no initial delay")
										ENDIF
									ENDIF
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
									IF bWdGangChaseDebug
										PRINTLN("[RCC MISSION] [gang_backup] SHOULD_CHASE_BACKUP_SPAWN_FOR_TEAM Hit 11")
									ENDIF
								#ENDIF
								
								IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangBackupDelay[MC_serverBD_4.iCurrentHighestPriority[iteam]] != 0
									#IF IS_DEBUG_BUILD
										IF bWdGangChaseDebug
											PRINTLN("[RCC MISSION] [gang_backup] SHOULD_CHASE_BACKUP_SPAWN_FOR_TEAM Hit 12")
										ENDIF
									#ENDIF
									IF NOT HAS_NET_TIMER_STARTED(tdbackupdelay[iteam])
										#IF IS_DEBUG_BUILD
											IF bWdGangChaseDebug
												PRINTLN("[RCC MISSION] [gang_backup] SHOULD_CHASE_BACKUP_SPAWN_FOR_TEAM Hit 13")
											ENDIF
										#ENDIF
										REINIT_NET_TIMER(tdbackupdelay[iteam])
									ELSE
										IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdbackupdelay[iteam]) > (g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangBackupDelay[MC_serverBD_4.iCurrentHighestPriority[iteam]]*1000)
											#IF IS_DEBUG_BUILD
												IF bWdGangChaseDebug
													PRINTLN("[RCC MISSION] [gang_backup] SHOULD_CHASE_BACKUP_SPAWN_FOR_TEAM Hit 14")
												ENDIF
											#ENDIF
											IF NOT HAS_NET_TIMER_STARTED(timeBetweenTeamBackup[iteam])
											OR (HAS_NET_TIMER_STARTED(timeBetweenTeamBackup[iteam]) AND HAS_NET_TIMER_EXPIRED((timeBetweenTeamBackup[iteam]), 1500))
												bShouldSpawn = TRUE
												PRINTLN("[RCC MISSION] [gang_backup] SHOULD_CHASE_BACKUP_SPAWN_FOR_TEAM Hit 19")
											ELSE
												PRINTLN("[RCC MISSION] [gang_backup] SHOULD_CHASE_BACKUP_SPAWN_FOR_TEAM Hit 20 - waiting for timeBetweenTeamBackup")
											ENDIF
										ENDIF
									ENDIF
								ELSE
									#IF IS_DEBUG_BUILD
										IF bWdGangChaseDebug
											PRINTLN("[RCC MISSION] [gang_backup] SHOULD_CHASE_BACKUP_SPAWN_FOR_TEAM Hit 15")
										ENDIF
									#ENDIF
									IF NOT HAS_NET_TIMER_STARTED(timeBetweenTeamBackup[iteam])
									OR (HAS_NET_TIMER_STARTED(timeBetweenTeamBackup[iteam]) AND HAS_NET_TIMER_EXPIRED((timeBetweenTeamBackup[iteam]), 1500))
										bShouldSpawn = TRUE
										PRINTLN("[RCC MISSION] [gang_backup] BCK_SPN SHOULD_CHASE_BACKUP_SPAWN_FOR_TEAM Hit 23")
									ELSE
										PRINTLN("[RCC MISSION] [gang_backup] BCK_SPN SHOULD_CHASE_BACKUP_SPAWN_FOR_TEAM Hit 24 - waiting for timeBetweenTeamBackup - no mid point - no initial delay")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						IF bWdGangChaseDebug
							INT iTotalChase = GET_TOTAL_NUMBER_OF_GANG_BACKUP_CHASING_ALL_TEAMS()
							PRINTLN("[RCC MISSION] [gang_backup] SHOULD_CHASE_BACKUP_SPAWN_FOR_TEAM Hit 17 won't spawn because iTotalChase = ", iTotalChase, " >= iIntensity  = ", iIntensity)
						ENDIF
					#ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE13_GANGCHASE_REQUIRES_AGGRO)
				IF IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_0 + iTeam)
					PRINTLN("[RCC MISSION] [gang_backup] Aggro has been activated at some point - not blocking gang chase due to ciBS_RULE13_GANGCHASE_REQUIRES_AGGRO!")
				ELSE
					bShouldSpawn = FALSE
					PRINTLN("[RCC MISSION] [gang_backup] Blocking gang chase due to ciBS_RULE13_GANGCHASE_REQUIRES_AGGRO")
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF bWdGangChaseDebug
			PRINTLN("[RCC MISSION] [gang_backup] SHOULD_CHASE_BACKUP_SPAWN_FOR_TEAM returning ", bShouldSpawn)
		ENDIF
	#ENDIF
	
	RETURN bShouldSpawn
	
ENDFUNC

FUNC BOOL IS_ALREADY_CHASE_TARGET(INT ipart)
INT i

	FOR i = 0 TO (MAX_BACKUP_VEHICLES-1)
		IF MC_serverBD_1.GangBackup[i].iTargetPart = ipart
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE

ENDFUNC

FUNC INT GET_NUMBER_OF_CHASE_BACKUP_CHASING_PART(INT iPart)
	INT iCount = 0
	INT i = 0
	
	REPEAT MAX_BACKUP_VEHICLES i
		IF MC_serverBD_1.GangBackup[i].iTargetPart = iPart
			iCount++
		ENDIF
	ENDREPEAT
	
	RETURN iCount
ENDFUNC

FUNC BOOL IS_POTENTIAL_BACKUP_TARGET_NEAR_DROP_OFF(PED_INDEX targetPed, INT iTeam, INT iDropOffDist = 200, INT iPart = -1)
	
	VECTOR vDropOff = GET_DROP_OFF_CENTER_FOR_TEAM(DEFAULT, iTeam, iPart)
	
	IF IS_VECTOR_ZERO(vDropOff)
		RETURN FALSE
	ENDIF
	
	FLOAT fMaxDist = TO_FLOAT(iDropOffDist)
	FLOAT fDist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(targetPed, vDropOff)
	
//	PRINTLN("[RCC MISSION] [gang_backup] IS_POTENTIAL_BACKUP_TARGET_NEAR_DROP_OFF iPart = ",iPart," vDropOff = ", vDropOff, " fDist = ", fDist, " fMaxDist = ", fMaxDist)
	
	IF fDist <= fMaxDist
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC PED_INDEX GET_AVENGER_DUMMY_PED(INT iAvenger)
	
	IF iAvenger = -1
	OR NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iAvenger])
		PRINTLN("[RCC MISSION][gang_backup] GET_AVENGER_DUMMY_PED - iAvenger ",iAvenger," doesn't exist")
		RETURN NULL
	ENDIF
	
	VEHICLE_INDEX vehAvenger = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iAvenger])
	VECTOR vPedCoord = GET_ENTITY_COORDS(vehAvenger) - <<0,0,-3>>
	
	IF g_iHostOfam_mp_armory_aircraft != -1
		
		IF DOES_ENTITY_EXIST(MPGlobals.RemoteFakeAvengerPed[g_iHostOfam_mp_armory_aircraft])
			
			VECTOR vGlobalPed = GET_ENTITY_COORDS(MPGlobals.RemoteFakeAvengerPed[g_iHostOfam_mp_armory_aircraft])
			PRINTLN("[RCC MISSION][gang_backup] GET_AVENGER_DUMMY_PED - Found RemoteFakeAvengerPed of g_iHostOfam_mp_armory_aircraft ",g_iHostOfam_mp_armory_aircraft,", vGlobalPed = ",vGlobalPed)
			
			IF ARE_VECTORS_ALMOST_EQUAL(vGlobalPed, vPedCoord, 5.0)
				
				PRINTLN("[RCC MISSION][gang_backup] GET_AVENGER_DUMMY_PED - RETURN RemoteFakeAvengerPed, they're close enough to vPedCoord ",vPedCoord)
				RETURN MPGlobals.RemoteFakeAvengerPed[g_iHostOfam_mp_armory_aircraft]
				
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[RCC MISSION][gang_backup] GET_AVENGER_DUMMY_PED - Reject RemoteFakeAvengerPed, they're too far from vPedCoord ",vPedCoord)
			#ENDIF
			ENDIF
			
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[RCC MISSION][gang_backup] GET_AVENGER_DUMMY_PED - No RemoteFakeAvengerPed exists for g_iHostOfam_mp_armory_aircraft ",g_iHostOfam_mp_armory_aircraft)
		#ENDIF
		ENDIF
		
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[RCC MISSION][gang_backup] GET_AVENGER_DUMMY_PED - g_iHostOfam_mp_armory_aircraft is -1")
	#ENDIF
	ENDIF
	
	PED_INDEX tempPed = NULL
	
	IF GET_CLOSEST_PED(vPedCoord, 5.00, TRUE, TRUE, tempPed, FALSE, TRUE)
		IF DOES_ENTITY_EXIST(tempPed)
			IF GET_ENTITY_MODEL(tempPed) = GET_MOC_PED_MODEL()
				
				PRINTLN("[RCC MISSION][gang_backup] GET_AVENGER_DUMMY_PED - GET_CLOSEST_PED returned a valid ped")
				RETURN tempPed
				
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[RCC MISSION][gang_backup] GET_AVENGER_DUMMY_PED - GET_CLOSEST_PED returned a ped with the wrong model")
			#ENDIF
			ENDIF
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[RCC MISSION][gang_backup] GET_AVENGER_DUMMY_PED - GET_CLOSEST_PED returned a non-existent ped")
		#ENDIF
		ENDIF
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[RCC MISSION][gang_backup] GET_AVENGER_DUMMY_PED - GET_CLOSEST_PED failed at vPedCoord ",vPedCoord)
	#ENDIF
	ENDIF
	
	RETURN NULL
	
ENDFUNC

FUNC INT GET_PARTICIPANT_OF_TEAM_FOR_BACKUP_TARGET(INT iteam, INT iDropDist)
	
	INT iparticipant
	INT iPartToChase = ciBACKUP_TARGET_NONE
	INT iFewestChasing = 99999
	INT iNumChasing = 0
	PARTICIPANT_INDEX temppart
	PLAYER_INDEX tempplayer
	
	IF NOT SHOULD_CHASE_BACKUP_SPAWN_FOR_TEAM(iteam)
		PRINTLN("[RCC MISSION][gang_backup] GET_PARTICIPANT_OF_TEAM_FOR_BACKUP_TARGET - SHOULD_CHASE_BACKUP_SPAWN_FOR_TEAM is FALSE for iteam = ",iteam)
		RETURN -1
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[RCC MISSION][gang_backup] GET_PARTICIPANT_OF_TEAM_FOR_BACKUP_TARGET - SHOULD_CHASE_BACKUP_SPAWN_FOR_TEAM is true for iteam = ",iteam)
	#ENDIF
	ENDIF
	
	//-- Check a little bit further than the actual flee distance
	iDropDist += 50
	
	INT iPlayersChecked, iPlayersInAvengerHold
	
	REPEAT GET_MAXIMUM_PLAYERS_ON_MISSION() iParticipant
		
		IF MC_playerBD[iParticipant].iteam != iteam
			RELOOP
		ENDIF
		
		IF IS_BIT_SET(MC_playerBD[iparticipant].iClientBitSet,PBBOOL_ANY_SPECTATOR)
		AND NOT IS_BIT_SET(MC_playerBD[iparticipant].iClientBitSet,PBBOOL_HEIST_SPECTATOR)
			RELOOP
		ENDIF
		
		IF IS_BIT_SET(MC_playerBD[iparticipant].iClientBitSet,PBBOOL_PLAYER_FAIL)
			RELOOP
		ENDIF
		
		IF NOT IS_BIT_SET(MC_playerBD[iparticipant].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
			RELOOP
		ENDIF
		
		IF IS_BIT_SET( MC_playerBD[iparticipant].iClientBitSet, PBBOOL_FINISHED )
			RELOOP
		ENDIF
			
		tempPart = INT_TO_PARTICIPANTINDEX(iParticipant)
		
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			RELOOP
		ENDIF
		
		tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
		
		IF NOT IS_NET_PLAYER_OK(tempPlayer)
			RELOOP
		ENDIF
		
		iPlayersChecked++
		
		IF IS_BIT_SET(MC_playerBD[iparticipant].iClientBitSet,PBBOOL_CHASE_TARGET)
			
			IF IS_BIT_SET(MC_playerBD[iparticipant].iClientBitSet2,PBBOOL2_LOSE_GANG_CHASE)
			OR NOT IS_POTENTIAL_BACKUP_TARGET_NEAR_DROP_OFF(GET_PLAYER_PED(tempPlayer), iteam, iDropDist, iparticipant)
				
				IF NOT IS_ALREADY_CHASE_TARGET(iparticipant)
					
					PRINTLN("[RCC MISSION][gang_backup] GET_PARTICIPANT_OF_TEAM_FOR_BACKUP_TARGET - RETURN part ",iparticipant," / ",GET_PLAYER_NAME(tempPlayer),", no gang chase on them yet")
					RETURN iparticipant
					
				ELSE
					iNumChasing = GET_NUMBER_OF_CHASE_BACKUP_CHASING_PART(iParticipant)
					IF iNumChasing <= iFewestChasing
						PRINTLN("[RCC MISSION][gang_backup] GET_PARTICIPANT_OF_TEAM_FOR_BACKUP_TARGET - Found new part with fewest chasing ",iparticipant," / ",GET_PLAYER_NAME(tempPlayer), ", iNumChasing ",iNumChasing)
						iFewestChasing = iNumChasing
						iPartToChase = iparticipant
					ENDIF
				ENDIF
				
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[RCC MISSION][gang_backup] GET_PARTICIPANT_OF_TEAM_FOR_BACKUP_TARGET - Reject part ",iparticipant," / ",GET_PLAYER_NAME(tempplayer),", within drop off distance ",iDropDist)
			#ENDIF
			ENDIF
		ELSE
			IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
			AND IS_PLAYER_IN_CREATOR_AIRCRAFT(tempPlayer)
				iPlayersInAvengerHold++
				PRINTLN("[RCC MISSION][gang_backup] GET_PARTICIPANT_OF_TEAM_FOR_BACKUP_TARGET - Part ",iparticipant," / ",GET_PLAYER_NAME(tempPlayer)," is in avenger hold, iPlayersInAvengerHold = ",iPlayersInAvengerHold)
			ENDIF
		ENDIF
		
		IF iPlayersChecked > MC_serverBD.iNumberOfPlayingPlayers[iteam]
			PRINTLN("[RCC MISSION][gang_backup] GET_PARTICIPANT_OF_TEAM_FOR_BACKUP_TARGET - Checked all players, iNumberOfPlayingPlayers ",MC_serverBD.iNumberOfPlayingPlayers[iteam])
			BREAKLOOP
		ENDIF
		
	ENDREPEAT
	
	IF iPartToChase = -1
		IF iPlayersInAvengerHold > 0
		AND iPlayersInAvengerHold >= iPlayersChecked
			
			IF ((MC_serverBD_1.iBackupTargetAvenger = -1) OR (NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[MC_serverBD_1.iBackupTargetAvenger])))
			AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_AVENGER_INTERIOR_ENTRY)
				INT iVehLoop
				FOR iVehLoop = 0 TO (MC_serverBD.iNumVehCreated - 1)
					IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehLoop].mn = AVENGER
					AND NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVehLoop])
					AND DECOR_EXIST_ON(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVehLoop]), "Creator_Trailer")
						PRINTLN("[RCC MISSION][gang_backup] GET_PARTICIPANT_OF_TEAM_FOR_BACKUP_TARGET - Found iBackupTargetAvenger as veh ",iVehLoop)
						MC_serverBD_1.iBackupTargetAvenger = iVehLoop
						BREAKLOOP
					ENDIF
				ENDFOR
			ENDIF
			
			IF MC_serverBD_1.iBackupTargetAvenger != -1
				PED_INDEX dummyPed = GET_AVENGER_DUMMY_PED(MC_serverBD_1.iBackupTargetAvenger)
				
				IF DOES_ENTITY_EXIST(dummyPed)
					IF ((MC_serverBD_4.iCurrentHighestPriority[iteam] < FMMC_MAX_RULES) AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[MC_serverBD_4.iCurrentHighestPriority[iteam]], ciBS_RULE3_LOSE_ALL_GANG_CHASE_VEHICLES))
					OR NOT IS_POTENTIAL_BACKUP_TARGET_NEAR_DROP_OFF(dummyPed, iTeam, iDropDist)
						
						PRINTLN("[RCC MISSION][gang_backup] GET_PARTICIPANT_OF_TEAM_FOR_BACKUP_TARGET - Everyone is in the avenger hold! Target the dummy ped, RETURN ciBACKUP_TARGET_AVENGER_DUMMY_PED ",ciBACKUP_TARGET_AVENGER_DUMMY_PED)
						RETURN ciBACKUP_TARGET_AVENGER_DUMMY_PED
						
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("[RCC MISSION][gang_backup] GET_PARTICIPANT_OF_TEAM_FOR_BACKUP_TARGET - Everyone is in the avenger hold, but iBackupTargetAvenger ",MC_serverBD_1.iBackupTargetAvenger,"'s dummy ped is near a drop off")
					#ENDIF
					ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[RCC MISSION][gang_backup] GET_PARTICIPANT_OF_TEAM_FOR_BACKUP_TARGET - Everyone is in the avenger hold, but iBackupTargetAvenger ",MC_serverBD_1.iBackupTargetAvenger,"'s dummy ped doesn't exist")
				#ENDIF
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[RCC MISSION][gang_backup] GET_PARTICIPANT_OF_TEAM_FOR_BACKUP_TARGET - Everyone is in the avenger hold, but iBackupTargetAvenger is -1")
			#ENDIF
			ENDIF
		ENDIF
	ELSE
		PRINTLN("[RCC MISSION][gang_backup] GET_PARTICIPANT_OF_TEAM_FOR_BACKUP_TARGET - RETURN part ",iPartToChase,", fewest number chasing them")
	ENDIF
	
	RETURN iPartToChase

ENDFUNC

FUNC BOOL IS_TEAM_BETTER_CHASE_CANDIDATE_THAN_TEAM(INT iteam1,INT iteam2)

	IF HAS_TEAM_FAILED(iteam1) AND NOT HAS_TEAM_FAILED(iteam2)
		RETURN FALSE
	ENDIF
	
	IF MC_serverBD_1.iNumBackupChaseCandidates[iteam1] = 0 AND MC_serverBD_1.iNumBackupChaseCandidates[iteam2] != 0
		RETURN FALSE
	ENDIF
	
	IF MC_serverBD_1.iNumBackupChasing[iteam1] > MC_serverBD_1.iNumBackupChasing[iteam2]
		RETURN FALSE
	ELIF MC_serverBD_1.iNumBackupChasing[iteam1] = MC_serverBD_1.iNumBackupChasing[iteam2]	
		IF MC_serverBD_1.iNumBackupChaseCandidates[iteam1] < MC_serverBD_1.iNumBackupChaseCandidates[iteam2]
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE

ENDFUNC

FUNC INT GET_TEAM_TO_CHASE(BOOL bConsiderteam0,BOOL bConsiderteam1,BOOL bConsiderteam2,BOOL bConsiderteam3)

INT ihigh1,ihigh2

INT iteam = -1

	IF bConsiderteam0 AND bConsiderteam1 AND bConsiderteam2 AND bConsiderteam3
		IF IS_TEAM_BETTER_CHASE_CANDIDATE_THAN_TEAM(1,0)
			ihigh1 = 1
		ELSE
			ihigh1 = 0
		ENDIF
		IF IS_TEAM_BETTER_CHASE_CANDIDATE_THAN_TEAM(3,2)
			ihigh2 = 3
		ELSE
			ihigh2 = 2
		ENDIF
		IF IS_TEAM_BETTER_CHASE_CANDIDATE_THAN_TEAM(ihigh1,ihigh2)
			iteam = ihigh1
		ELSE
			iteam = ihigh2
		ENDIF
	ELIF bConsiderteam0 AND bConsiderteam1 AND bConsiderteam2 AND NOT bConsiderteam3	

		IF IS_TEAM_BETTER_CHASE_CANDIDATE_THAN_TEAM(1,0)
			ihigh1 = 1
		ELSE
			ihigh1 = 0
		ENDIF
		IF IS_TEAM_BETTER_CHASE_CANDIDATE_THAN_TEAM(ihigh1,2)
			iteam =  ihigh1
		ELSE
			iteam =  2
		ENDIF
	ELIF bConsiderteam0 AND bConsiderteam1 AND NOT bConsiderteam2 AND bConsiderteam3	

		IF IS_TEAM_BETTER_CHASE_CANDIDATE_THAN_TEAM(1,0)
			ihigh1 = 1
		ELSE
			ihigh1 = 0
		ENDIF
		IF IS_TEAM_BETTER_CHASE_CANDIDATE_THAN_TEAM(ihigh1,3)
			iteam =  ihigh1
		ELSE
			iteam =  3
		ENDIF
	ELIF bConsiderteam0 AND NOT bConsiderteam1 AND bConsiderteam2 AND bConsiderteam3	

		IF IS_TEAM_BETTER_CHASE_CANDIDATE_THAN_TEAM(2,0)
			ihigh1 = 2
		ELSE
			ihigh1 = 0
		ENDIF
		IF IS_TEAM_BETTER_CHASE_CANDIDATE_THAN_TEAM(ihigh1,3)
			iteam =  ihigh1
		ELSE
			iteam =  3
		ENDIF	
	ELIF NOT bConsiderteam0 AND bConsiderteam1 AND bConsiderteam2 AND bConsiderteam3	

		IF IS_TEAM_BETTER_CHASE_CANDIDATE_THAN_TEAM(2,1)
			ihigh1 = 2
		ELSE
			ihigh1 = 1
		ENDIF
		IF IS_TEAM_BETTER_CHASE_CANDIDATE_THAN_TEAM(ihigh1,3)
			iteam =  ihigh1
		ELSE
			iteam =  3
		ENDIF
	ELIF bConsiderteam0 AND bConsiderteam1 AND NOT bConsiderteam2 AND NOT bConsiderteam3	
		IF IS_TEAM_BETTER_CHASE_CANDIDATE_THAN_TEAM(0,1)
			iteam =  0
		ELSE
			iteam =  1
		ENDIF
	ELIF bConsiderteam0 AND NOT bConsiderteam1 AND bConsiderteam2 AND NOT bConsiderteam3	
		IF IS_TEAM_BETTER_CHASE_CANDIDATE_THAN_TEAM(0,2)
			iteam =  0
		ELSE
			iteam =  2
		ENDIF	
	ELIF NOT bConsiderteam0 AND bConsiderteam1 AND bConsiderteam2 AND NOT bConsiderteam3	
		IF IS_TEAM_BETTER_CHASE_CANDIDATE_THAN_TEAM(1,2)
			iteam =  1
		ELSE
			iteam =  2
		ENDIF	
	ELIF bConsiderteam0 AND NOT bConsiderteam1 AND NOT bConsiderteam2 AND bConsiderteam3	
		IF IS_TEAM_BETTER_CHASE_CANDIDATE_THAN_TEAM(1,3)
			iteam =  1
		ELSE
			iteam =  3
		ENDIF	
	ELIF NOT bConsiderteam0 AND bConsiderteam1 AND NOT bConsiderteam2 AND bConsiderteam3	
		IF IS_TEAM_BETTER_CHASE_CANDIDATE_THAN_TEAM(0,3)
			iteam =  0
		ELSE
			iteam =  3
		ENDIF	
		
	ELIF NOT bConsiderteam0 AND NOT bConsiderteam1 AND bConsiderteam2 AND bConsiderteam3	
		IF IS_TEAM_BETTER_CHASE_CANDIDATE_THAN_TEAM(2,3)
			iteam =  2
		ELSE
			iteam =  3
		ENDIF
	ELIF bConsiderteam0 AND NOT bConsiderteam1 AND NOT bConsiderteam2 AND NOT bConsiderteam3	
		iteam =  0
	ELIF NOT bConsiderteam0 AND bConsiderteam1 AND NOT bConsiderteam2 AND NOT bConsiderteam3	
		iteam =  1
	ELIF NOT bConsiderteam0 AND NOT bConsiderteam1 AND bConsiderteam2 AND NOT bConsiderteam3	
		iteam =  2	
	ELIF NOT bConsiderteam0 AND NOT bConsiderteam1 AND NOT bConsiderteam2 AND bConsiderteam3	
		iteam =  3	
	ENDIF
	
	IF iteam != -1
		PRINTLN("[RCC MISSION][gang_backup] GET_TEAM_TO_CHASE - SELECTING TARGET CHASE TEAM: ",iteam) 
	ENDIF
	
	RETURN iteam
	
ENDFUNC

PROC RESET_BACKUP_SPAWN_VARIABLES(INT iUnit)
	
	MC_PlayerBD[iparttouse].vBackupSpawnCoords = <<0.0,0.0,0.0>>
	vBackupSpawnDirection = <<0.0,0.0,0.0>>
	iBackupForwardSpawnAttempts = 0
	iAirChase_StandardSpawnAttempts = 0
	
	IF iUnit != -1
		CLEAR_BIT(MC_PlayerBD[iparttouse].ibackupSpawnBitset, iUnit)
		CLEAR_BIT(MC_PlayerBD[iPartToUse].ibackupSpawnBitset, PBGANGSPAWN_UNIT_0_TRY_FORWARDS + MC_serverBD_1.iCurrentBackupSpawn)
		CLEAR_BIT(MC_PlayerBD[iPartToUse].ibackupSpawnBitset, PBGANGSPAWN_UNIT_0_SUCCEED_FORWARDS + MC_serverBD_1.iCurrentBackupSpawn)
		CLEAR_BIT(MC_PlayerBD[iPartToUse].ibackupSpawnBitset, PBGANGSPAWN_UNIT_0_AIR_FAILED_HELISPAWN + MC_serverBD_1.iCurrentBackupSpawn)
	ELSE
		MC_PlayerBD[iparttouse].ibackupSpawnBitset = 0
	ENDIF
	
ENDPROC

FUNC BOOL IS_GANG_CHASE_AIR(INT iBackupType)
	
	SWITCH iBackupType
		
		CASE ciBACKUP_TYPE_AIR_BUZZ_MERRYWEATHER
		CASE ciBACKUP_TYPE_AIR_BUZZ_NOOSE
		CASE ciBACKUP_TYPE_AIR_BUZZ_PROFESSIONALS
		CASE ciBACKUP_TYPE_AIR_MAVERICK_NOOSE
		CASE ciBACKUP_TYPE_AIR_MAVERICK_COP
		CASE ciBACKUP_TYPE_AIR_FROGGER_MERRYWEATHER
		CASE ciBACKUP_TYPE_AIR_FROGGER_PROFESSIONALS
		CASE ciBACKUP_TYPE_AIR_FROGGER_LOST
		CASE ciBACKUP_TYPE_AIR_FROGGER_VAGOS
		CASE ciBACKUP_TYPE_AIR_FROGGER_KOREAN
		CASE ciBACKUP_TYPE_AIR_VALKYRIE_MERRYWEATHER
		CASE ciBACKUP_TYPE_AIR_VALKYRIE_PROFESSIONALS
		CASE ciBACKUP_TYPE_AIR_HUNTER_MERRYWEATHER
		CASE ciBACKUP_TYPE_AIR_HUNTER_PROFESSIONALS
		CASE ciBACKUP_TYPE_AIR_SAVAGE_MERRYWEATHER
		CASE ciBACKUP_TYPE_AIR_SAVAGE_PROFESSIONALS
		CASE ciBACKUP_TYPE_AIR_LAZER_MERRYWEATHER
		CASE ciBACKUP_TYPE_AIR_MOLOTOK_MERRYWEATHER
		CASE ciBACKUP_TYPE_AIR_BUZZ_AVON_GOON
			RETURN TRUE
		
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_GANG_CHASE_SEA(INT iBackupType)
	
	UNUSED_PARAMETER(iBackupType)
	// Add sea gang chases into a switch here (like in IS_GANG_CHASE_AIR) if we need to
	
	RETURN FALSE
	
ENDFUNC

FUNC NODE_FLAGS GET_GANG_CHASE_NODE_FLAGS(INT iBackupType)
	
	NODE_FLAGS eNodeFlags = NF_NONE
	
	IF IS_GANG_CHASE_AIR(iBackupType)
	OR IS_GANG_CHASE_SEA(iBackupType)
		eNodeFlags |= NF_INCLUDE_BOAT_NODES
	ENDIF
	
	RETURN eNodeFlags
	
ENDFUNC

FUNC BOOL DOES_NODE_HAVE_SUITABLE_GANG_CHASE_HEADING(VECTOR &vNearNode, FLOAT fNearHeading, FLOAT& fNodeHeading, INT iBackupType, FLOAT fTolerance = 45.0, FLOAT fMaxDistance = 9999.0)
	
	BOOL bSuitable = FALSE
	VECTOR vNodePos
	FLOAT fReturnedHeading
	
	fNodeHeading = 0
	
	VECTOR vResult = <<0, 1, 0>>
	RotateVec(vResult, <<0, 0, fNearHeading>>)
	VECTOR vFacePos = vNearNode + vResult
	NODE_FLAGS eNodeFlags = GET_GANG_CHASE_NODE_FLAGS(iBackupType)
	
	IF GET_NTH_CLOSEST_VEHICLE_NODE_FAVOUR_DIRECTION(vNearNode, vFacePos, 0, vNodePos, fReturnedHeading, eNodeFlags)
		
		PRINTLN("[RCC MISSION][gang_backup] DOES_NODE_HAVE_SUITABLE_GANG_CHASE_HEADING - vMyPosition ",vNearNode,", fMyHeading ",fNearHeading,",vNodePos ",vNodePos,", fReturnedHeading ",fReturnedHeading,", fTolerance ",fTolerance, " fMaxDistance = ", fMaxDistance)
		
		IF (GET_ABSOLUTE_DELTA_HEADING(fNearHeading, fReturnedHeading) <= fTolerance)
			IF GET_DISTANCE_BETWEEN_COORDS(vNodePos, vNearNode) < fMaxDistance
				bSuitable = TRUE
				fNodeHeading = fReturnedHeading
				vNearNode = vNodePos
			ENDIF
		ENDIF
		
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[RCC MISSION][gang_backup] DOES_NODE_HAVE_SUITABLE_GANG_CHASE_HEADING - Unable to find a node with vNearNode ",vNearNode,", vFacePos ",vFacePos," & node flags ",ENUM_TO_INT(eNodeFlags))
	#ENDIF	
	ENDIF
	
	RETURN bSuitable
	
ENDFUNC

FUNC BOOL IS_ANY_GANG_CHASE_VEHICLE_NEAR_COORD(VECTOR vCoord, INT iBackupType)
	
	INT i
	FLOAT fMinDist = 15.0
	
	IF IS_GANG_CHASE_AIR(iBackupType)
		fMinDist = 25.0
	ENDIF
	
	REPEAT MAX_BACKUP_VEHICLES i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.GangBackup[i].BackupVeh)
			IF IS_NET_VEHICLE_DRIVEABLE(MC_serverBD_1.GangBackup[i].BackupVeh)
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(NET_TO_VEH(MC_serverBD_1.GangBackup[i].BackupVeh), vCoord) < fMinDist
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		
	ENDREPEAT	
	RETURN FALSE
ENDFUNC

PROC ADD_ROTATION_FOR_SPAWN_CHECK(VECTOR &vSpawnDirection, INT iBackupType, INT iBackupUnit, BOOL bSpawnForwards = FALSE)
	
	IF NOT IS_GANG_CHASE_AIR(iBackupType)
		// This won't actually work - we need to use a rotation vector like with ROTATE_VECTOR_ABOUT_Z,
		//		but we also don't want to change functionality in any old missions (because it works well enough that nobody has bugged this)
		
		IF NOT bSpawnForwards
			vSpawnDirection = vSpawnDirection + <<0.0,0.0, cfGangBackup_RotationAmountOnFail>>
			IF vSpawnDirection.z > 360.0
				vSpawnDirection.z = (vSpawnDirection.z - 360.0)
			ENDIF
		ELSE
			iBackupForwardSpawnAttempts++
			
			IF iBackupForwardSpawnAttempts < 5
				//0 = 0 degrees in front, 1 = +10, 2 = +20, 3 = -10, 4 = -20
				
				SWITCH iBackupForwardSpawnAttempts
					CASE 1
					CASE 2
						vSpawnDirection = vSpawnDirection + <<0.0,0.0,10.0>>
					BREAK
					
					CASE 3
						vSpawnDirection = vSpawnDirection - <<0.0,0.0,30.0>>
					BREAK
					
					CASE 4
						vSpawnDirection = vSpawnDirection - <<0.0,0.0,10.0>>
					BREAK
				ENDSWITCH
				
				IF vSpawnDirection.z > 360.0
					vSpawnDirection.z = (vSpawnDirection.z - 360.0)
				ENDIF
			ELSE
				iBackupForwardSpawnAttempts = 0
				CLEAR_BIT(MC_PlayerBD[iPartToUse].ibackupSpawnBitset, PBGANGSPAWN_UNIT_0_SUCCEED_FORWARDS + MC_serverBD_1.iCurrentBackupSpawn)
				vSpawnDirection = vSpawnDirection + <<0.0,0.0,20.0>> // Back to 0 degrees in front
			ENDIF
			
		ENDIF
		
	ELSE
		
		IF NOT IS_BIT_SET(MC_PlayerBD[iPartToUse].ibackupSpawnBitset, PBGANGSPAWN_UNIT_0_AIR_FAILED_HELISPAWN + iBackupUnit)
			PRINTLN("[RCC MISSION][gang_backup] ADD_ROTATION_FOR_SPAWN_CHECK - heli spawn failed, setting PBGANGSPAWN_UNIT_",iBackupUnit,"_AIR_FAILED_HELISPAWN")
			SET_BIT(MC_PlayerBD[iPartToUse].ibackupSpawnBitset, PBGANGSPAWN_UNIT_0_AIR_FAILED_HELISPAWN + iBackupUnit)
		ELSE
			// Normal spawning: do rotations!
			
			FLOAT fZRot
			
			IF NOT bSpawnForwards
				fZRot = cfGangBackup_RotationAmountOnFail
				iAirChase_StandardSpawnAttempts++
				
				// If we've gone all the way around!
				IF iAirChase_StandardSpawnAttempts >= (360.0 / cfGangBackup_RotationAmountOnFail)
					iAirChase_StandardSpawnAttempts = 0
					PRINTLN("[RCC MISSION][gang_backup] ADD_ROTATION_FOR_SPAWN_CHECK - gone round a whole circle of normal spawn attempts without any luck, clearing PBGANGSPAWN_UNIT_",iBackupUnit,"_AIR_FAILED_HELISPAWN to try heli spawn again")
					CLEAR_BIT(MC_PlayerBD[iPartToUse].ibackupSpawnBitset, PBGANGSPAWN_UNIT_0_AIR_FAILED_HELISPAWN + iBackupUnit)
				ENDIF
			ELSE
				iBackupForwardSpawnAttempts++
				
				IF iBackupForwardSpawnAttempts < 5
					//0 = 0 degrees in front, 1 = +10, 2 = +20, 3 = -10, 4 = -20
					SWITCH iBackupForwardSpawnAttempts
						CASE 1
						CASE 2
							fZRot = 10.0
						BREAK
						
						CASE 3
							fZRot = -30.0
						BREAK
						
						CASE 4
							fZRot = -10.0
						BREAK
					ENDSWITCH
				ELSE
					iBackupForwardSpawnAttempts = 0
					iAirChase_StandardSpawnAttempts = 0
					CLEAR_BIT(MC_PlayerBD[iPartToUse].ibackupSpawnBitset, PBGANGSPAWN_UNIT_0_SUCCEED_FORWARDS + MC_serverBD_1.iCurrentBackupSpawn)
					fZRot = 20.0 // Back to 0 degrees in front
				ENDIF
			ENDIF
			
			IF fZRot != 0.0
				ROTATE_VECTOR_ABOUT_Z(vSpawnDirection, fZRot)
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC

FUNC VECTOR RANDOMISE_ROTATION_FOR_AIR_GANG_CHASE(VECTOR vRotation)
	
	PRINTLN("[RCC MISSION][gang_backup] RANDOMISE_ROTATION_FOR_AIR_GANG_CHASE - Called with initial rotation ",vRotation)
	
	// Run the randomiser a few times to try and get an actual decently random number
	GET_RANDOM_INT_IN_RANGE(1000, 37000)
	GET_RANDOM_INT_IN_RANGE(1000, 37000)
	GET_RANDOM_INT_IN_RANGE(1000, 37000)
	GET_RANDOM_INT_IN_RANGE(1000, 37000)
	
	INT iRandom = GET_RANDOM_INT_IN_RANGE(1000, 37000) // (1 - 370) * 100
	
	iRandom = FLOOR(TO_FLOAT(iRandom) / 100.0) - 10 // 0 - 360
	
	vRotation.z += iRandom
	
	IF vRotation.z > 180.0
		vRotation.z = (vRotation.z - 360.0)
	ENDIF
	
	VECTOR vDirection = CONVERT_ROTATION_TO_DIRECTION_VECTOR(vRotation)
	PRINTLN("[RCC MISSION][gang_backup] RANDOMISE_ROTATION_FOR_AIR_GANG_CHASE - Applying random rotation of ",iRandom," to get new rotation ",vRotation," => new spawn direction ",vDirection)
	
	RETURN vDirection
	
ENDFUNC

FUNC BOOL FIND_GANG_CHASE_SPAWN_COORDINATES(PED_INDEX tempPed, INT iTeam, INT iRule, INT iBackupType, VECTOR &vtempSpawnCoords, BOOL &bSpawnForwards)
	
	vtempSpawnCoords = <<0, 0, 0>>
	
	IF IS_VECTOR_ZERO(vBackupSpawnDirection)
		VEHICLE_INDEX tempVeh
		IF IS_PED_IN_ANY_VEHICLE(tempPed)
			tempVeh = GET_VEHICLE_PED_IS_IN(tempPed)
		ENDIF
		BOOL bTempSpawnForwards = FALSE
		
		IF (g_FMMC_STRUCT.sFMMCEndConditions[iteam].iGangChaseForwardSpawnChance[iRule] > 0)
		AND (GET_RANDOM_INT_IN_RANGE(0,100) < g_FMMC_STRUCT.sFMMCEndConditions[iteam].iGangChaseForwardSpawnChance[iRule])
			bTempSpawnForwards = TRUE
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(tempVeh)
			vBackupSpawnDirection = GET_ENTITY_FORWARD_VECTOR(tempVeh)
			PRINTLN("[RCC MISSION][gang_backup] FIND_GANG_CHASE_SPAWN_COORDINATES - testing vBackupSpawnDirection in player's vehicle forward vector: ",vBackupSpawnDirection)
			IF bTempSpawnForwards
				IF ABSF(GET_ENTITY_SPEED(tempVeh)) < 5.0
					PRINTLN("[RCC MISSION][gang_backup] FIND_GANG_CHASE_SPAWN_COORDINATES - Clearing bTempSpawnForwards as player not going fast enough  ")
					bTempSpawnForwards = FALSE
				ENDIF
			ELSE
				IF IS_GANG_CHASE_AIR(iBackupType)
					vBackupSpawnDirection = RANDOMISE_ROTATION_FOR_AIR_GANG_CHASE(GET_ENTITY_ROTATION(tempVeh))
				ENDIF
			ENDIF
		ELSE
			bTempSpawnForwards = FALSE
			vBackupSpawnDirection = GET_ENTITY_FORWARD_VECTOR(tempPed)
			PRINTLN("[RCC MISSION][gang_backup] FIND_GANG_CHASE_SPAWN_COORDINATES - testing vBackupSpawnDirection in player ped forward vector: ",vBackupSpawnDirection)
		ENDIF
		
		IF bTempSpawnForwards
			SET_BIT(MC_PlayerBD[iPartToUse].ibackupSpawnBitset, PBGANGSPAWN_UNIT_0_TRY_FORWARDS + MC_serverBD_1.iCurrentBackupSpawn)
			SET_BIT(MC_PlayerBD[iPartToUse].ibackupSpawnBitset, PBGANGSPAWN_UNIT_0_SUCCEED_FORWARDS + MC_serverBD_1.iCurrentBackupSpawn)
			iBackupForwardSpawnAttempts = 0
			bSpawnForwards = TRUE
		ELSE
			CLEAR_BIT(MC_PlayerBD[iPartToUse].ibackupSpawnBitset, PBGANGSPAWN_UNIT_0_TRY_FORWARDS + MC_serverBD_1.iCurrentBackupSpawn)
			CLEAR_BIT(MC_PlayerBD[iPartToUse].ibackupSpawnBitset, PBGANGSPAWN_UNIT_0_SUCCEED_FORWARDS + MC_serverBD_1.iCurrentBackupSpawn)
			bSpawnForwards = FALSE
		ENDIF
		
	ELSE
		bSpawnForwards = IS_BIT_SET(MC_PlayerBD[iPartToUse].ibackupSpawnBitset, PBGANGSPAWN_UNIT_0_SUCCEED_FORWARDS + MC_serverBD_1.iCurrentBackupSpawn)
	ENDIF
	
	FLOAT fspawndist
	
	IF vBackupSpawnDirection.z >= 0.0 AND vBackupSpawnDirection.z < 90.0
	OR vBackupSpawnDirection.z >= 270.0 AND vBackupSpawnDirection.z < 360.0
		fspawndist = 220
	ELSE
		fspawndist = 190 
	ENDIF
	
	IF IS_GANG_CHASE_AIR(iBackupType)
		fspawndist += 150
	ENDIF
	
	VECTOR vPlayerCoords = GET_ENTITY_COORDS(tempPed)
	
	IF NOT FIND_SPAWN_POINT_IN_DIRECTION(vPlayerCoords, vBackupSpawnDirection, fspawndist, vtempSpawnCoords)
		RETURN FALSE
	ELSE
		PRINTLN("[RCC MISSION][gang_backup] FIND_GANG_CHASE_SPAWN_COORDINATES - FIND_SPAWN_POINT_IN_DIRECTION returned TRUE: vPlayerCoords ",vPlayerCoords,", vBackupSpawnDirection ",vBackupSpawnDirection,", fSpawnDist ",fSpawnDist," => ",vTempSpawnCoords)
	ENDIF
	
	VECTOR vOffsetFromPLayer = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(tempPed, vtempSpawnCoords)
	
	IF bSpawnForwards
		
		PRINTLN("[RCC MISSION][gang_backup] FIND_GANG_CHASE_SPAWN_COORDINATES - trying to spawn vehicle ",MC_serverBD_1.iCurrentBackupSpawn," forwards")
		
		FLOAT fNodeHeading
		
		IF NOT DOES_NODE_HAVE_SUITABLE_GANG_CHASE_HEADING(vtempSpawnCoords, GET_ENTITY_HEADING(tempPed), fNodeHeading, iBackupType, DEFAULT, 100.0)
			PRINTLN("[RCC MISSION][gang_backup] FIND_GANG_CHASE_SPAWN_COORDINATES - unsuitable node/heading found at ",vtempSpawnCoords)
			RETURN FALSE
		ELSE
			PRINTLN("[RCC MISSION][gang_backup] FIND_GANG_CHASE_SPAWN_COORDINATES - suitable heading found at ",vtempSpawnCoords," as fNodeHeading ",fNodeHeading,", spawn it forwards, Offset from player = ", vOffsetFromPLayer)
		ENDIF
		
	ELSE
		IF NOT IS_GANG_CHASE_AIR(iBackupType)
			//-- If we're not spawning forwards then try to avoid spawn points that are likely to result in the backup unit
			//-- driving at right angles to the player.
			IF ABSF(vOffsetFromPLayer.x) >= 50.0
				PRINTLN("[RCC MISSION][gang_backup] FIND_GANG_CHASE_SPAWN_COORDINATES - Not spawning forwards, rejecting point ", vtempSpawnCoords, " because offset from player = ", vOffsetFromPLayer)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_GANG_CHASE_AIR(iBackupType) // Try and spawn a helicopter in the air please :)
		FLOAT fHeliHeight = FMAX(GET_ENTITY_HEIGHT_ABOVE_GROUND(tempPed), cfGangBackup_AirSpawnZMinOffset)
		vtempSpawnCoords.z += fHeliHeight
		PRINTLN("[RCC MISSION][gang_backup] FIND_GANG_CHASE_SPAWN_COORDINATES - This is an air gang chase, bump z of vtempSpawnCoords by ",fHeliHeight," to ",vTempSpawnCoords)
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL FIND_BACKUP_SPAWN_COORDS(BOOL bDummyPedTarget = FALSE)
	
	IF MC_serverBD_1.iCurrentBackupSpawn = -1
		RESET_BACKUP_SPAWN_VARIABLES(MC_serverBD_1.iCurrentBackupSpawn)
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD_1.GangBackup[MC_serverBD_1.iCurrentBackupSpawn].iBackupBitset, SBBBOOL_BACKUP_CREATED)
		RESET_BACKUP_SPAWN_VARIABLES(MC_serverBD_1.iCurrentBackupSpawn)
		RETURN FALSE
	ENDIF
	
	IF (NOT bDummyPedTarget)
	AND MC_serverBD_1.GangBackup[MC_serverBD_1.iCurrentBackupSpawn].iTargetPart = ciBACKUP_TARGET_AVENGER_DUMMY_PED
	AND bIsLocalPlayerHost
		// We'll be calling this function elsewhere with bDummyPedTarget as TRUE, don't want to mess with the temp variables here
		RETURN FALSE
	ENDIF
	
	IF ((NOT bDummyPedTarget) AND (MC_serverBD_1.GangBackup[MC_serverBD_1.iCurrentBackupSpawn].iTargetPart != iPartToUse))
	OR (bDummyPedTarget AND (MC_serverBD_1.GangBackup[MC_serverBD_1.iCurrentBackupSpawn].iTargetPart != ciBACKUP_TARGET_AVENGER_DUMMY_PED))
		RESET_BACKUP_SPAWN_VARIABLES(MC_serverBD_1.iCurrentBackupSpawn)
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(MC_PlayerBD[iparttouse].ibackupSpawnBitset, MC_serverBD_1.iCurrentBackupSpawn)
		RETURN TRUE
	ENDIF
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	
	IF bDummyPedTarget
		iTeam = MC_serverBD_1.GangBackup[MC_serverBD_1.iCurrentBackupSpawn].iBackSpawnTeam
	ENDIF
	
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iteam]
	
	IF iRule >= FMMC_MAX_RULES
		RESET_BACKUP_SPAWN_VARIABLES(MC_serverBD_1.iCurrentBackupSpawn)
		RETURN FALSE
	ENDIF
	
	IF NOT bDummyPedTarget
		IF NOT bPlayerToUseOK
			RETURN FALSE
		ENDIF
	ELSE
		// We check that the dummy ped exists before calling this function
	ENDIF
	
	INT iBackupType = g_FMMC_STRUCT.sFMMCEndConditions[iteam].iGangBackupType[iRule]
	PED_INDEX tempPed = PlayerPedToUse
	BOOL bSpawnForwards
	
	IF bDummyPedTarget
		tempPed = GET_AVENGER_DUMMY_PED(MC_serverBD_1.iBackupTargetAvenger)
	ENDIF
	
	// 1. Grab a spawn point:
	
	VECTOR vtempSpawnCoords
	
	IF IS_GANG_CHASE_AIR(iBackupType)
	AND NOT IS_BIT_SET(MC_PlayerBD[iPartToUse].ibackupSpawnBitset, PBGANGSPAWN_UNIT_0_AIR_FAILED_HELISPAWN + MC_serverBD_1.iCurrentBackupSpawn)
		
		vtempSpawnCoords = FIND_SPAWN_COORDINATES_FOR_HELI(tempPed)
		PRINTLN("[RCC MISSION][gang_backup] FIND_BACKUP_SPAWN_COORDS - FIND_SPAWN_COORDINATES_FOR_HELI returned ",vtempSpawnCoords)
		
	ELSE
		
		IF NOT FIND_GANG_CHASE_SPAWN_COORDINATES(tempPed, iTeam, iRule, iBackupType, vtempSpawnCoords, bSpawnForwards)
			ADD_ROTATION_FOR_SPAWN_CHECK(vBackupSpawnDirection, iBackupType, MC_serverBD_1.iCurrentBackupSpawn, bSpawnForwards)
			PRINTLN("[RCC MISSION][gang_backup] FIND_BACKUP_SPAWN_COORDS - failed to find - going to be testing new vBackupSpawnDirection: ",vBackupSpawnDirection)
			RETURN FALSE
		ENDIF
		
	ENDIF
	
	
	// 2. Make sure it's not too close to a drop-off that they need to flee
	
	IF NOT IS_VECTOR_ZERO(GET_DROP_OFF_CENTER_FOR_TEAM(DEFAULT, iTeam, iPartToUse))
		IF NOT CURRENT_OBJECTIVE_LOSE_GANG_BACKUP()
			IF GET_DISTANCE_BETWEEN_COORDS(vtempSpawnCoords,GET_DROP_OFF_CENTER_FOR_TEAM(DEFAULT, iTeam, iPartToUse)) < 300
				PRINTLN("[RCC MISSION][gang_backup] FIND_BACKUP_SPAWN_COORDS - rejecting backup spawn coords as they're too near drop off: ",vtempSpawnCoords)
				vBackupSpawnDirection = <<0.0,0.0,0.0>>
				
				IF IS_GANG_CHASE_AIR(iBackupType)
				AND NOT IS_BIT_SET(MC_PlayerBD[iPartToUse].ibackupSpawnBitset, PBGANGSPAWN_UNIT_0_AIR_FAILED_HELISPAWN + MC_serverBD_1.iCurrentBackupSpawn)
					PRINTLN("[RCC MISSION][gang_backup] FIND_BACKUP_SPAWN_COORDS - heli spawn failed, setting PBGANGSPAWN_UNIT_",MC_serverBD_1.iCurrentBackupSpawn,"_AIR_FAILED_HELISPAWN")
					SET_BIT(MC_PlayerBD[iPartToUse].ibackupSpawnBitset, PBGANGSPAWN_UNIT_0_AIR_FAILED_HELISPAWN + MC_serverBD_1.iCurrentBackupSpawn)
				ENDIF
				
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	
	// 3. Make sure the point is valid to spawn - distance, sight, proximity checks
	
	BOOL bSpawn = TRUE
	
	FLOAT fSpawnDistFromPlayer = GET_DISTANCE_BETWEEN_COORDS(vtempSpawnCoords, GET_ENTITY_COORDS(tempPed))
	FLOAT fTooFar = 290.0
	
	IF IS_GANG_CHASE_AIR(iBackupType)
		fTooFar = 550.0
	ENDIF
	
	IF fSpawnDistFromPlayer > fTooFar
		PRINTLN("[RCC MISSION][gang_backup] FIND_BACKUP_SPAWN_COORDS - rejecting backup spawn coords too far from player ", vtempSpawnCoords, " dist = ", fSpawnDistFromPlayer)
		bSpawn = FALSE
	ELSE
		PRINTLN("[RCC MISSION][gang_backup] FIND_BACKUP_SPAWN_COORDS - backup spawn coords Distance from player ", vtempSpawnCoords, " dist = ", fSpawnDistFromPlayer)
	ENDIF
	
	IF bSpawn
	AND CAN_ANY_PLAYER_SEE_POINT(vtempSpawnCoords, 10.0, TRUE, TRUE, 200.0)
		PRINTLN("[RCC MISSION][gang_backup] FIND_BACKUP_SPAWN_COORDS - rejecting backup spawn coords visible to a player ", vtempSpawnCoords)
		bSpawn = FALSE
	ENDIF
	
	IF bSpawn
	AND IS_ANY_GANG_CHASE_VEHICLE_NEAR_COORD(vtempSpawnCoords, iBackupType)
		PRINTLN("[RCC MISSION][gang_backup] FIND_BACKUP_SPAWN_COORDS - rejecting backup spawn coords too near existing gang chase ", vtempSpawnCoords)
		bSpawn = FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	BOOL bShowAdvancedSpew = g_SpawnData.bShowAdvancedSpew
	g_SpawnData.bShowAdvancedSpew = TRUE // Get IS_POINT_OK_FOR_NET_ENTITY_CREATION to print out reasons for it failing
	#ENDIF
	
	IF bSpawn
	AND NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION(vtempSpawnCoords, DEFAULT, DEFAULT, DEFAULT, DEFAULT, FALSE, FALSE)
		PRINTLN("[RCC MISSION][gang_backup] FIND_BACKUP_SPAWN_COORDS - rejecting backup spawn coords IS_POINT_OK_FOR_NET_ENTITY_CREATION ", vtempSpawnCoords)
		bSpawn = FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	g_SpawnData.bShowAdvancedSpew = bShowAdvancedSpew
	#ENDIF
	
	
	IF bSpawn
		MC_PlayerBD[iparttouse].vBackupSpawnCoords = vtempSpawnCoords + <<0.0, 0.0, 0.5>>
		vBackupSpawnDirection = <<0.0,0.0,0.0>>
		iBackupForwardSpawnAttempts = 0
		iAirChase_StandardSpawnAttempts = 0
		SET_BIT(MC_PlayerBD[iparttouse].ibackupSpawnBitset, MC_serverBD_1.iCurrentBackupSpawn)
		PRINTLN("[RCC MISSION][gang_backup] FIND_BACKUP_SPAWN_COORDS - selecting backup spawn coords: ",MC_PlayerBD[iparttouse].vBackupSpawnCoords," for unit: ",MC_serverBD_1.iCurrentBackupSpawn, " bSpawnForwards = ", bSpawnForwards )
		
		RETURN TRUE
	ENDIF
	
	// bSpawn was FALSE, so rotate the direction we're trying to find a spawn point in
	ADD_ROTATION_FOR_SPAWN_CHECK(vBackupSpawnDirection, iBackupType, MC_serverBD_1.iCurrentBackupSpawn, bSpawnForwards)
	
	RETURN FALSE

ENDFUNC

/// PURPOSE: Checks whether the player being checked has reached a point in the mission where gang chase should start spawning for their team
FUNC BOOL MANAGE_CHASE_BACKUP_AREA_CONDITIONS(PED_INDEX tempPed,INT ipart)
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[ipart].iteam] >= FMMC_MAX_RULES
		RETURN FALSE
	ENDIF
	
	IF IS_PED_INJURED(tempPed)
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD_1.iChaseAreaBitset[MC_PlayerBD[ipart].iteam],MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[ipart].iteam])
		RETURN TRUE
	ENDIF
	
	VECTOR vPos1
	VECTOR vPos2
	FLOAT fWidth
	
	//-- Should gang chase trigger when reaching the midpoint of the rule?
	BOOL bTriggersOnMidpoint = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[ipart].iteam].iGangBackupAtMidBitset,MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[ipart].iteam])
	
	//-- Has part reached the mid-point of the rule? 
	BOOL bReachedMidpoint = IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_PlayerBD[ipart].iteam],MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[ipart].iteam])
	
	#IF IS_DEBUG_BUILD
		PLAYER_INDEX tempPlayer
		VECTOR vPlayerLoc
		IF bWdGangChaseDebug
			PRINTLN("[RCC MISSION] [dsw] [gang_backup] MANAGE_CHASE_BACKUP_AREA_CONDITIONS, ipart ",ipart," - PBBOOL_CHASE_TARGET ",IS_BIT_SET(MC_playerBD[ipart].iClientBitSet,PBBOOL_CHASE_TARGET),", area type ",g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[ipart].iteam].iGangBackupAreaType[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[ipart].iteam]], " bTriggersOnMidpoint = ", bTriggersOnMidpoint, " bReachedMidpoint = ", bReachedMidpoint)
		ENDIF
	#ENDIF
	
	SWITCH g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[ipart].iteam].iGangBackupAreaType[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[ipart].iteam]]
		
		CASE ciGANGCHASE_AREA_OFF
			IF IS_BIT_SET(MC_playerBD[ipart].iClientBitSet,PBBOOL_CHASE_TARGET)
				PRINTLN("[RCC MISSION] [dsw] [gang_backup] MANAGE_CHASE_BACKUP_AREA_CONDITIONS - TRUE ciGANGCHASE_AREA_OFF ipart = ", ipart)
				SET_BIT(MC_serverBD_1.iChaseAreaBitset[MC_PlayerBD[ipart].iteam],MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[ipart].iteam])
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE ciGANGCHASE_INSIDE_AREA
			IF IS_BIT_SET(MC_playerBD[ipart].iClientBitSet,PBBOOL_CHASE_TARGET)
				IF NOT bTriggersOnMidpoint
				OR bTriggersOnMidpoint AND bReachedMidpoint
					vPos1 =	g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[ipart].iteam].vGangTriggerPos1[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[ipart].iteam]]
					vPos2 =	g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[ipart].iteam].vGangTriggerPos2[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[ipart].iteam]]
					fWidth = g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[ipart].iteam].fGangTriggerWidth[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[ipart].iteam]]	
					
					IF IS_ENTITY_IN_ANGLED_AREA(tempPed, vPos1, vPos2, fWidth)
						#IF IS_DEBUG_BUILD
							PRINTLN("[RCC MISSION] [dsw] [gang_backup] MANAGE_CHASE_BACKUP_AREA_CONDITIONS - TRUE ciGANGCHASE_INSIDE_AREA ipart = ", ipart, " vPos1 = ", vPos1, " vPos2 = ", vPos2, " width = ", fWidth, " rule = ", MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[ipart].iteam])
							IF IS_PED_A_PLAYER(tempPed)
								tempPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(tempPed)
								vPlayerLoc = GET_ENTITY_COORDS(tempPed)
								IF NETWORK_IS_PLAYER_ACTIVE(tempPlayer)
									PRINTLN("[RCC MISSION] [dsw] [gang_backup] MANAGE_CHASE_BACKUP_AREA_CONDITIONS - TRUE ciGANGCHASE_INSIDE_AREA player is ", GET_PLAYER_NAME(tempPlayer), " their location is ", vPlayerLoc)
								ENDIF
							ENDIF
						#ENDIF
						
						SET_BIT(MC_serverBD_1.iChaseAreaBitset[MC_PlayerBD[ipart].iteam],MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[ipart].iteam])
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ciGANGCHASE_OUTSIDE_AREA
			IF IS_BIT_SET(MC_playerBD[ipart].iClientBitSet,PBBOOL_CHASE_TARGET)
				IF NOT bTriggersOnMidpoint
				OR bTriggersOnMidpoint AND bReachedMidpoint
					vPos1 = g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[ipart].iteam].vGangTriggerPos1[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[ipart].iteam]]
					vPos2 = g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[ipart].iteam].vGangTriggerPos2[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[ipart].iteam]]
					fWidth = g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[ipart].iteam].fGangTriggerWidth[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[ipart].iteam]]
					
					IF NOT IS_ENTITY_IN_ANGLED_AREA(tempPed, vPos1, vPos2, fWidth)
						#IF IS_DEBUG_BUILD
							PRINTLN("[RCC MISSION] [dsw] [gang_backup] MANAGE_CHASE_BACKUP_AREA_CONDITIONS - TRUE ciGANGCHASE_OUTSIDE_AREA ipart = ", ipart, " vPos1 = ", vPos1, " vPos2 = ", vPos2, " width = ", fWidth)
							IF IS_PED_A_PLAYER(tempPed)
								tempPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(tempPed)
								vPlayerLoc = GET_ENTITY_COORDS(tempPed)
								IF NETWORK_IS_PLAYER_ACTIVE(tempPlayer)
									PRINTLN("[RCC MISSION] [dsw] [gang_backup] MANAGE_CHASE_BACKUP_AREA_CONDITIONS - TRUE ciGANGCHASE_OUTSIDE_AREA player is ", GET_PLAYER_NAME(tempPlayer), " their  location is ", vPlayerLoc)
								ENDIF
							ENDIF
						#ENDIF
						
						SET_BIT(MC_serverBD_1.iChaseAreaBitset[MC_PlayerBD[ipart].iteam],MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[ipart].iteam])
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

FUNC BOOL SHOULD_ALLOW_BACKUP_SPAWN(INT iGangBackupUnit)
	
	INT iTeam = MC_serverBD_1.GangBackup[iGangBackupUnit].iBackSpawnTeam
	INT iRule
	INT iTotalNumber
	
	IF iTeam != -1
		iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		IF iRule < FMMC_MAX_RULES
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[iRule], ciBS_RULE11_GANG_CHASE_TRIGGER_ON_SPEED_FAIL)
				IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet6, SBBOOL6_ALLOW_GANGCHASE_AFTER_SPEED_FAIL)
					#IF IS_DEBUG_BUILD
					IF bWdGangChaseDebug
						PRINTLN("SHOULD_ALLOW_BACKUP_SPAWN - Returning FALSE due to SBBOOL6_HOLD_BACK_GANGCHASE")
					ENDIF
					#ENDIF
					RETURN FALSE
				ENDIF
			ENDIF
			
			// controls intensity
			IF MC_serverBD_1.iNumBackupChasing[iTeam] >= GET_GANG_CHASE_INTENSITY_FOR_THIS_GEN(iTeam) //g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangBackupNumber[iRule]
				#IF IS_DEBUG_BUILD
					IF bWdGangChaseDebug
						PRINTLN("[RCC MISSION] [gang_backup] SHOULD_ALLOW_BACKUP_SPAWN, unit ",iGangBackupUnit," - returning FALSE as iNumBackupChasing (",MC_serverBD_1.iNumBackupChasing[iTeam]," >= max num at once for team ",iTeam," (",GET_GANG_CHASE_INTENSITY_FOR_THIS_GEN(iTeam),")")
					ENDIF
				#ENDIF
				RETURN FALSE
			ENDIF
			// controls total number made
			iTotalNumber = GET_TOTAL_NUMBER_OF_GANG_CHASE_FOR_THIS_GEN(iTeam)
		//	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangMaxBackup[iRule] = -1
			IF iTotalNumber = -1
				IF MC_serverBD_1.iNumBackupSpawned[iTeam][iRule] >= MC_serverBD_1.iVariableNumBackupAllowed[iTeam]
					#IF IS_DEBUG_BUILD
						IF bWdGangChaseDebug
							PRINTLN("[RCC MISSION] [gang_backup] SHOULD_ALLOW_BACKUP_SPAWN, unit ",iGangBackupUnit," - returning FALSE as iNumBackupSpawned (",MC_serverBD_1.iNumBackupSpawned[iTeam][iRule]," >= variable num of backup allowed for team ",iTeam," (",MC_serverBD_1.iVariableNumBackupAllowed[iTeam],")")
						ENDIF
					#ENDIF
					RETURN FALSE
				ENDIF
			ELSE
			//	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangMaxBackup[iRule] > 0
				IF iTotalNumber > 0
					IF MC_serverBD_1.iNumBackupSpawned[iTeam][iRule] >= iTotalNumber //g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangMaxBackup[iRule]
						#IF IS_DEBUG_BUILD
							IF bWdGangChaseDebug
								PRINTLN("[RCC MISSION] [gang_backup] SHOULD_ALLOW_BACKUP_SPAWN, unit ",iGangBackupUnit," - returning FALSE as iNumBackupSpawned (",MC_serverBD_1.iNumBackupSpawned[iTeam][iRule]," >= max backup spawned for team ",iTeam," (",iTotalNumber,")")
							ENDIF
						#ENDIF
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			RETURN FALSE
		ENDIF
	ELSE
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: GANG CHASE COLLISION LOADING !
//
//************************************************************************************************************************************************************



PROC CLEANUP_COLLISION_ON_GANG_CHASE_PED(PED_INDEX tempPed, INT iBackupUnit)
	
	IF DOES_ENTITY_EXIST(tempPed)
		IF NOT IS_ENTITY_DEAD(tempPed)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(tempPed)
				SET_ENTITY_LOAD_COLLISION_FLAG(tempPed, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iBackupPedCollisionBitset, iBackupUnit)
		IF iCollisionStreamingLimit > 0
			iCollisionStreamingLimit--
		ELSE
			PRINTLN("[LOAD_COLLISION_FLAG][gang_backup] CLEANUP_COLLISION_ON_GANG_CHASE_PED - iCollisionStreamingLimit somehow dropping to below 0, somebody messed up!")
			SCRIPT_ASSERT("CLEANUP_COLLISION_ON_GANG_CHASE_PED - iCollisionStreamingLimit somehow dropping to below 0, somebody messed up!")
		ENDIF
		
		CLEAR_BIT(iBackupPedCollisionBitset, iBackupUnit)
		PRINTLN("[LOAD_COLLISION_FLAG][gang_backup] CLEANUP_COLLISION_ON_GANG_CHASE_PED - Clearing iBackupPedCollisionBitset for gang backup unit: ",iBackupUnit, " current requests: ",iCollisionStreamingLimit)
	ENDIF
	
	IF IS_BIT_SET(iBackupPedNavmeshCheckedBS, iBackupUnit)
		IF IS_BIT_SET(iBackupPedNavmeshLoadedBS, iBackupUnit)
			IF IS_BIT_SET(iLocalBoolCheck8, LBOOL8_NAVMESH_REGION_REQUESTED)
				PRINTLN("[LOAD_COLLISION_FLAG][gang_backup] CLEANUP_COLLISION_ON_GANG_CHASE_PED - Calling REMOVE_NAVMESH_REQUIRED_REGIONS for gang backup unit: ",iBackupUnit,".")
				REMOVE_NAVMESH_REQUIRED_REGIONS()
				CLEAR_BIT(iLocalBoolCheck8, LBOOL8_NAVMESH_REGION_REQUESTED)
			ELSE
				PRINTLN("[LOAD_COLLISION_FLAG][gang_backup] CLEANUP_COLLISION_ON_GANG_CHASE_PED - iBackupPedNavmeshLoadedBS set, but no navmesh region requested")
				SCRIPT_ASSERT("CLEANUP_COLLISION_ON_GANG_CHASE_PED - iBackupPedNavmeshLoadedBS set, but no navmesh region requested!")
			ENDIF
			
			CLEAR_BIT(iBackupPedNavmeshLoadedBS, iBackupUnit)
			PRINTLN("[LOAD_COLLISION_FLAG][gang_backup] CLEANUP_COLLISION_ON_GANG_CHASE_PED - Clearing iBackupPedNavmeshLoadedBS for gang backup unit: ",iBackupUnit,".")
		ENDIF
		
		CLEAR_BIT(iBackupPedNavmeshCheckedBS, iBackupUnit)
		PRINTLN("CLEANUP_COLLISION_ON_GANG_CHASE_PED - Clearing iBackupPedNavmeshCheckedBS for gang backup unit: ",iBackupUnit,".")
	ENDIF
	
ENDPROC

PROC LOAD_NAVMESH_ON_GANG_CHASE_PED_IF_POSSIBLE(PED_INDEX tempPed, INT iBackupUnit)
	
	IF NOT IS_BIT_SET(iBackupPedNavmeshCheckedBS, iBackupUnit)
		IF IS_IT_SAFE_TO_LOAD_NAVMESH()
			IF NOT IS_BIT_SET(iLocalBoolCheck8, LBOOL8_NAVMESH_REGION_REQUESTED)
				
				INT			i, j, k
				VECTOR 		vPedCoords
				PED_INDEX 	nearbyPeds[8]
				
				i 			= GET_PED_NEARBY_PEDS(tempPed, nearbyPeds)
				vPedCoords 	= GET_ENTITY_COORDS(tempPed)
				
				PRINTLN("LOAD_NAVMESH_ON_GANG_CHASE_PED_IF_POSSIBLE - Checking navmesh required region for gang backup unit: ",iBackupUnit,".")
				
				IF ( i > 0 )
					PRINTLN("LOAD_NAVMESH_ON_GANG_CHASE_PED_IF_POSSIBLE - Checking navmesh required region for ped as there are ",i," peds around this ped in gang backup unit: ",iBackupUnit,".")
					
					REPEAT COUNT_OF(nearbyPeds) j
						IF DOES_ENTITY_EXIST(nearbyPeds[j]) AND NOT IS_ENTITY_DEAD(nearbyPeds[j])
							IF GET_PED_RELATIONSHIP_GROUP_HASH(tempPed) = GET_PED_RELATIONSHIP_GROUP_HASH(nearbyPeds[j])
								IF VDIST2(vPedCoords, GET_ENTITY_COORDS(nearbyPeds[j])) < 50*50
									k++
									PRINTLN("LOAD_NAVMESH_ON_GANG_CHASE_PED_IF_POSSIBLE - Adding nearby friendly peds to ped in gang backup unit: ",iBackupUnit,", currently friendly neighbours at ", k,".")
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
					
					IF ( k > 5 )
						ADD_NAVMESH_REQUIRED_REGION(vPedCoords.x, vPedCoords.y, 125.0)
						SET_BIT(iBackupPedNavmeshLoadedBS, iBackupUnit)
						SET_BIT(iLocalBoolCheck8, LBOOL8_NAVMESH_REGION_REQUESTED)
						PRINTLN("[LOAD_COLLISION_FLAG][gang_backup] LOAD_NAVMESH_ON_GANG_CHASE_PED_IF_POSSIBLE - Adding navmesh required region 125m around ",vPedCoords," for gang backup unit: ",iBackupUnit,".")
					ENDIF
				ELSE
					PRINTLN("LOAD_NAVMESH_ON_GANG_CHASE_PED_IF_POSSIBLE - Navmesh required region not needed as no other peds around gang backup unit: ",iBackupUnit,".")
				ENDIF
				
				SET_BIT(iBackupPedNavmeshCheckedBS, iBackupUnit)
			ENDIF
		ELSE
			PRINTLN("LOAD_NAVMESH_ON_GANG_CHASE_PED_IF_POSSIBLE - IS_IT_SAFE_TO_LOAD_NAVMESH returning FALSE, waiting to add navmesh required for gang backup unit: ",iBackupUnit,".")
		ENDIF
	ENDIF
	
ENDPROC

PROC LOAD_COLLISION_ON_GANG_CHASE_PED_IF_POSSIBLE(PED_INDEX tempPed, INT iBackupUnit)
	
	// We know the ped is not injured and that we have control if we're calling this
	
	IF NOT IS_BIT_SET(iBackupPedCollisionBitset, iBackupUnit)
		IF IS_ENTITY_WAITING_FOR_WORLD_COLLISION(tempPed)
		OR (IS_PED_IN_ANY_VEHICLE(tempPed) AND IS_ENTITY_WAITING_FOR_WORLD_COLLISION(GET_VEHICLE_PED_IS_IN(tempPed)))
			IF iCollisionStreamingLimit <= ciCOLLISION_STREAMING_MAX
				
				SET_ENTITY_LOAD_COLLISION_FLAG(tempPed, TRUE)
				
				iCollisionStreamingLimit++
				SET_BIT(iBackupPedCollisionBitset, iBackupUnit)
				PRINTLN("[LOAD_COLLISION_FLAG][gang_backup] LOAD_COLLISION_ON_GANG_CHASE_PED_IF_POSSIBLE - Loading collision for gang backup unit: ",iBackupUnit, " current requests: ",iCollisionStreamingLimit)
				
				LOAD_NAVMESH_ON_GANG_CHASE_PED_IF_POSSIBLE(tempPed, iBackupUnit)
				
			ELSE
				PRINTLN("[LOAD_COLLISION_FLAG][gang_backup] LOAD_COLLISION_ON_GANG_CHASE_PED_IF_POSSIBLE - iCollisionStreamingLimit reached! Shelved CL 13920263 has a possible solution. gang backup unit: ",iBackupUnit)
				SCRIPT_ASSERT("[LOAD_COLLISION_FLAG][gang_backup] LOAD_COLLISION_ON_GANG_CHASE_PED_IF_POSSIBLE - iCollisionStreamingLimit reached!")
			ENDIF
		ENDIF
	ELSE
		LOAD_NAVMESH_ON_GANG_CHASE_PED_IF_POSSIBLE(tempPed, iBackupUnit)
	ENDIF
	
ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: GENERIC GANG CHASE PROCESSING !
//
//************************************************************************************************************************************************************



PROC TASK_BACKUP_FLEE_COORDS(INT iGangBackupUnit, VECTOR vcoords,BOOL bBlockNonTempEvents = FALSE)
	INT iped
	PED_INDEX tempPed
	
	FOR iped= 0 TO (MAX_BACKUP_PEDS_PER_VEHICLE-1)
		IF NOT IS_NET_PED_INJURED(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupPed[iped])
			tempPed = NET_TO_PED(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupPed[iped])
			
			IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_SMART_FLEE_POINT)
				IF bBlockNonTempEvents
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,TRUE)
				ELSE
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,FALSE)
				ENDIF
				
				SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_ALWAYS_FIGHT, FALSE)
				SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_ALWAYS_FLEE, TRUE)
				SET_PED_FLEE_ATTRIBUTES(tempPed, FA_DISABLE_COWER,TRUE)
				SET_PED_FLEE_ATTRIBUTES(tempPed,  FA_COWER_INSTEAD_OF_FLEE,FALSE)
				SET_PED_FLEE_ATTRIBUTES(tempPed, FA_DISABLE_FLEE_FROM_INDIRECT_THREATS,FALSE)
				
				TASK_SMART_FLEE_COORD(tempPed,vcoords,5000,-1)
				PRINTLN("[RCC MISSION] BACKUP TASK_FLEE DROPOFF (w CA_ALWAYS_FLEE) FOR gang backup ped ",iped," from unit ",iGangBackupUnit)
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

PROC DELETE_BACKUP_UNIT(INT iGangBackupUnit)
	
	INT iped
	
	FOR iped = 0 TO (MAX_BACKUP_PEDS_PER_VEHICLE-1)
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupPed[iped])
			
			IF iped = 0
				CLEANUP_COLLISION_ON_GANG_CHASE_PED(NET_TO_PED(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupPed[iped]), iGangBackupUnit)
			ENDIF
			
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupPed[iped])
				DELETE_NET_ID(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupPed[iped])
			ELSE
				CLEANUP_NET_ID(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupPed[iped])
			ENDIF
			
		ELIF iped = 0
			CLEANUP_COLLISION_ON_GANG_CHASE_PED(NULL, iGangBackupUnit)
		ENDIF
	ENDFOR
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupVeh)
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupVeh)
			DELETE_NET_ID(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupVeh)
		ELSE
			CLEANUP_NET_ID(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupVeh)
		ENDIF
	ENDIF
	
	NET_PRINT("DELETE_BACKUP_UNIT called") NET_NL()
	
ENDPROC

FUNC BOOL IS_BACKUP_UNIT_DEAD(INT iGangBackupUnit)
	
	INT iped
	INT iMaxpeds = 2
	
	FOR iped = 0 TO (iMaxpeds-1)
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupPed[iped])
			IF NOT IS_NET_PED_INJURED(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupPed[iped])
				RETURN FALSE
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN TRUE
	
ENDFUNC

PROC CLEANUP_BACKUP_UNIT(INT iGangBackupUnit,BOOL bFlee = FALSE)
	
	#IF IS_DEBUG_BUILD
		IF bGangChaseCleanup
			PRINTLN("[RCC MISSION] [gang_backup] [sc_GangChaseCleanup] CLEANUP_BACKUP_UNIT has been called, iGangBackupUnit =  ", iGangBackupUnit, " Callstack...")
			DEBUG_PRINTCALLSTACK()
		ENDIF
	#ENDIF
	
	INT iped
	BOOL bFlee2
	PED_INDEX tempPed
	
	IF IS_BACKUP_UNIT_DEAD(iGangBackupUnit)
		PRINTLN("[RCC MISSION][JR] Incrementing MC_serverBD_4.iGangChasePedsKilledThisRule. MC_serverBD_4.iGangChasePedsKilledThisRule is now: ", MC_serverBD_4.iGangChasePedsKilledThisRule + 1)
		MC_serverBD_4.iGangChasePedsKilledThisRule++
	ENDIF
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupVeh)
		PRINTLN("[RCC MISSION] [gang_backup] cleanup backup vehicle called for backup unit",iGangBackupUnit)
		CLEANUP_NET_ID(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupVeh)
	ENDIF
	
	FOR iped = 0 TO (MAX_BACKUP_PEDS_PER_VEHICLE-1)
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupPed[iped])
			
			tempPed = NET_TO_PED(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupPed[iped])
			
			IF MC_serverBD_1.GangBackup[iGangBackupUnit].iBackSpawnTeam != -1
				IF MC_serverBD_4.iCurrentHighestPriority[MC_serverBD_1.GangBackup[iGangBackupUnit].iBackSpawnTeam] < FMMC_MAX_RULES
					IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[MC_serverBD_1.GangBackup[iGangBackupUnit].iBackSpawnTeam].vDropOff[MC_serverBD_4.iCurrentHighestPriority[MC_serverBD_1.GangBackup[iGangBackupUnit].iBackSpawnTeam]])
						
						INT iTargetPart = MC_serverBD_1.GangBackup[iGangBackupUnit].iTargetPart
						IF iTargetPart < 0
							iTargetPart = -1 // Clamp it to -1 for use with GET_DROP_OFF_CENTER_FOR_TEAM
						ENDIF
						
						IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(tempPed, GET_DROP_OFF_CENTER_FOR_TEAM(DEFAULT, MC_serverBD_1.GangBackup[iGangBackupUnit].iBackSpawnTeam, iTargetPart)) < 200
							bFlee2 = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF bFlee
			OR bFlee2
				IF NOT IS_NET_PED_INJURED(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupPed[iped])
					tempPed = NET_TO_PED(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupPed[iped])
					SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_ALWAYS_FIGHT, FALSE)
					SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_ALWAYS_FLEE, TRUE)
					PRINTLN("[RCC MISSION] [gang_backup] backup unit ",iGangBackupUnit," setting ped CA to flee CA_ALWAYS_FLEE: ",iped)
					IF MC_serverBD_1.GangBackup[iGangBackupUnit].iBackSpawnTeam != -1
						IF MC_serverBD_4.iCurrentHighestPriority[MC_serverBD_1.GangBackup[iGangBackupUnit].iBackSpawnTeam] < FMMC_MAX_RULES
							
							INT iTargetPart = MC_serverBD_1.GangBackup[iGangBackupUnit].iTargetPart
							IF iTargetPart < 0
								iTargetPart = -1 // Clamp it to -1 for use with GET_DROP_OFF_CENTER_FOR_TEAM
							ENDIF
							
							IF NOT IS_VECTOR_ZERO(GET_DROP_OFF_CENTER_FOR_TEAM(DEFAULT, MC_serverBD_1.GangBackup[iGangBackupUnit].iBackSpawnTeam, iTargetPart))
								//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,TRUE)
								TASK_BACKUP_FLEE_COORDS(iGangBackupUnit, GET_DROP_OFF_CENTER_FOR_TEAM(DEFAULT, MC_serverBD_1.GangBackup[iGangBackupUnit].iBackSpawnTeam, iTargetPart))
								PRINTLN("[RCC MISSION] [gang_backup] [TASK_BACKUP_FLEE_COORDS] backup unit ",iGangBackupUnit," setting smart flee on ped: ",iped)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF iped = 0
				CLEANUP_COLLISION_ON_GANG_CHASE_PED(tempPed, iGangBackupUnit)
			ENDIF
			
			PRINTLN("[RCC MISSION] [gang_backup] cleanup backup unit: ",iGangBackupUnit," backup ped ",iped)
			CLEANUP_NET_ID(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupPed[iped])
			
		ELIF iped = 0
			CLEANUP_COLLISION_ON_GANG_CHASE_PED(NULL, iGangBackupUnit)
		ENDIF
	ENDFOR

ENDPROC

PROC RESET_GANG_BACKUP_DATA(INT iGangBackupUnit)
	PRINTLN("[RCC MISSION] RESET_GANG_BACKUP_DATA called iGangBackupUnit = ", iGangBackupUnit)
	MC_serverBD_1.GangBackup[iGangBackupUnit].iGangBackupProgress = 0
	MC_serverBD_1.GangBackup[iGangBackupUnit].iBackSpawnTeam = -1
	MC_serverBD_1.GangBackup[iGangBackupUnit].iTargetPart = ciBACKUP_TARGET_NONE
	MC_serverBD_1.GangBackup[iGangBackupUnit].iBackupBitset = 0
	MC_serverBD_1.GangBackup[iGangBackupUnit].iBackupType = ciBACKUP_TYPE_NONE
	MC_serverBD_1.GangBackup[iGangBackupUnit].iFleeDist = 200
	RESET_NET_TIMER(MC_serverBD_1.GangBackup[iGangBackupUnit].tdCleanupTimer)
	CLEAR_BIT(MC_serverBD_1.GangBackup[iGangBackupUnit].iBackupBitset, SBBBOOL_RESERVATION_COMPLETE)
	
ENDPROC

FUNC BOOL IS_UNIT_CLEANED_UP(INT iGangBackupUnit)

INT iped
INT iMaxpeds = 2

	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupVeh)
		RETURN FALSE
	ENDIF
	FOR iped = 0 TO (iMaxpeds-1)
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupPed[iped])
			RETURN FALSE
		ENDIF
	ENDFOR
	
	RETURN TRUE

ENDFUNC

PROC PROCESS_GANG_BACKUP_BRAIN(INT iGangBackupUnit, PED_INDEX targetPed, INT iTargetTeam, INT iTargetRule, INT iTargetPart = -1)
	
	FLOAT fDist
	fDist = GET_DISTANCE_BETWEEN_ENTITIES(NET_TO_ENT(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupVeh), targetPed)
	
	#IF IS_DEBUG_BUILD
		IF bGangChaseCleanup
			PRINTLN("[RCC MISSION][gang_backup][sc_GangChaseCleanup] PROCESS_GANG_BACKUP_BRAIN - Distance between Backup unit and target = ",fDist," iGangBackupUnit ",iGangBackupUnit)
		ENDIF
	#ENDIF
	
	FLOAT fDist_InstantCleanup, fDist_TimedCleanup
	
	IF NOT IS_GANG_CHASE_AIR(MC_serverBD_1.GangBackup[iGangBackupUnit].iBackupType)
		fDist_InstantCleanup = 300.0
		fDist_TimedCleanup = 250.0
	ELSE
		fDist_InstantCleanup = 750.0
		fDist_TimedCleanup = 500.0
	ENDIF
	
	IF fDist > fDist_InstantCleanup
	OR IS_BACKUP_UNIT_DEAD(iGangBackupUnit)
		#IF IS_DEBUG_BUILD
		IF fDist > fDist_InstantCleanup
			PRINTLN("[RCC MISSION][gang_backup] PROCESS_GANG_BACKUP_BRAIN - Cleaning up backup unit as outside of ",fDist_InstantCleanup,"m - unit ",iGangBackupUnit)
		ELSE
			PRINTLN("[RCC MISSION][gang_backup] PROCESS_GANG_BACKUP_BRAIN - Cleaning up backup unit as IS_BACKUP_UNIT_DEAD returning TRUE - unit ",iGangBackupUnit)
		ENDIF
		#ENDIF
		
		CLEANUP_BACKUP_UNIT(iGangBackupUnit)
		
//	ELIF ( (fDist > 200) // Only do the timer over 200m cleaning up if this is a gang chase with vehicles that spawn forwards
//		  AND ( IS_BIT_SET(MC_serverBD_1.GangBackup[iGangBackupUnit].iBackupBitset, SBBBOOL_BACKUP_SPAWNING_FORWARDS)
//				OR ( (MC_serverBD_4.iCurrentHighestPriority[iteam] < FMMC_MAX_RULES) AND (g_FMMC_STRUCT.sFMMCEndConditions[iteam].iGangChaseForwardSpawnChance[MC_serverBD_4.iCurrentHighestPriority[iteam]] > 0) ) ) )
//								
	ELIF fDist > fDist_TimedCleanup
		IF NOT HAS_NET_TIMER_STARTED(MC_serverBD_1.GangBackup[iGangBackupUnit].tdCleanupTimer)
			PRINTLN("[RCC MISSION][gang_backup] PROCESS_GANG_BACKUP_BRAIN - Starting timer for outside of ",fDist_TimedCleanup,"m - unit ",iGangBackupUnit)
			REINIT_NET_TIMER(MC_serverBD_1.GangBackup[iGangBackupUnit].tdCleanupTimer)
		ELSE
			IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.GangBackup[iGangBackupUnit].tdCleanupTimer) > 10000 //10 seconds
				PRINTLN("[RCC MISSION][gang_backup] PROCESS_GANG_BACKUP_BRAIN - Cleaning up backup unit as outside of ",fDist_TimedCleanup,"m for more than 10s - unit ",iGangBackupUnit)
				CLEANUP_BACKUP_UNIT(iGangBackupUnit)
			ELSE
				#IF IS_DEBUG_BUILD       
					IF bGangChaseCleanup
						INT iTimeCleanup
						iTimeCleanup = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.GangBackup[iGangBackupUnit].tdCleanupTimer)
						PRINTLN("[RCC MISSION][gang_backup][sc_GangChaseCleanup] PROCESS_GANG_BACKUP_BRAIN - Not cleaning up due to timed distance check as time expired = ", iTimeCleanup)
					ENDIF
				#ENDIF
			ENDIF
		ENDIF
		
	ELSE
		IF HAS_NET_TIMER_STARTED(MC_serverBD_1.GangBackup[iGangBackupUnit].tdCleanupTimer)
			PRINTLN("[RCC MISSION][gang_backup] PROCESS_GANG_BACKUP_BRAIN - Resetting timer as back inside 200m - unit ",iGangBackupUnit)
			RESET_NET_TIMER(MC_serverBD_1.GangBackup[iGangBackupUnit].tdCleanupTimer)									
		ENDIF
		
		IF IS_BIT_SET(MC_serverBD_1.GangBackup[iGangBackupUnit].iBackupBitset, SBBBOOL_BACKUP_ON_GOTO_TASK)
		AND fDist < 250.0
			
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupPed[0])
				
				PED_INDEX pedDriver
				pedDriver = NET_TO_PED(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupPed[0])
				
				IF NOT IS_PED_INJURED(pedDriver)
					
					IF NETWORK_HAS_CONTROL_OF_ENTITY(pedDriver)
						PRINTLN("[RCC MISSION][gang_backup] PROCESS_GANG_BACKUP_BRAIN - Tasking driver with combat - unit ",iGangBackupUnit)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDriver, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedDriver, 299)
						
						CLEANUP_COLLISION_ON_GANG_CHASE_PED(pedDriver, iGangBackupUnit)
						CLEAR_BIT(MC_serverBD_1.GangBackup[iGangBackupUnit].iBackupBitset, SBBBOOL_BACKUP_ON_GOTO_TASK)
					ELSE
						PRINTLN("[RCC MISSION][gang_backup] PROCESS_GANG_BACKUP_BRAIN - Requesting control of driver to task with combat - unit ",iGangBackupUnit)
						NETWORK_REQUEST_CONTROL_OF_ENTITY(pedDriver)
					ENDIF
					
				ELSE
					CLEANUP_COLLISION_ON_GANG_CHASE_PED(pedDriver, iGangBackupUnit)
					CLEAR_BIT(MC_serverBD_1.GangBackup[iGangBackupUnit].iBackupBitset, SBBBOOL_BACKUP_ON_GOTO_TASK)
				ENDIF
			ELSE
				CLEANUP_COLLISION_ON_GANG_CHASE_PED(NULL, iGangBackupUnit)
				CLEAR_BIT(MC_serverBD_1.GangBackup[iGangBackupUnit].iBackupBitset, SBBBOOL_BACKUP_ON_GOTO_TASK)
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD       
		IF bGangChaseCleanup
			PRINTLN("[RCC MISSION][gang_backup][sc_GangChaseCleanup] PROCESS_GANG_BACKUP_BRAIN - Doing target near drop off check iGangBackupUnit =  ", iGangBackupUnit)
		ENDIF
	#ENDIF
	
	IF iTargetRule < FMMC_MAX_RULES
		IF NOT IS_VECTOR_ZERO(GET_DROP_OFF_CENTER_FOR_TEAM(DEFAULT, iTargetTeam, iTargetPart))
			#IF IS_DEBUG_BUILD
				IF bGangChaseCleanup
					PRINTLN("[RCC MISSION][gang_backup][sc_GangChaseCleanup] PROCESS_GANG_BACKUP_BRAIN - Doing target near drop off check, drop of center not zero, iGangBackupUnit =  ", iGangBackupUnit)
				ENDIF
			#ENDIF
	
			IF NOT CURRENT_OBJECTIVE_LOSE_GANG_BACKUP()
				
				VECTOR vDropOff = GET_DROP_OFF_CENTER_FOR_TEAM(DEFAULT, iTargetTeam, iTargetPart)
				FLOAT fDistFromDrop = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(targetPed, vDropOff)
				FLOAT fMaxDist = 0.0
				
				// Gang backup will flee when the player they are chasing is close to the drop-off
				// How close they need to be is stored in iGangBackupFleeDist in the global struct
				
				IF MC_serverBD_1.GangBackup[iGangBackupUnit].iFleeDist > 0
					fMaxDist = TO_FLOAT(MC_serverBD_1.GangBackup[iGangBackupUnit].iFleeDist)
				//	PRINTLN("[RCC MISSION] [gang_backup] [TASK_BACKUP_FLEE_COORDS] Found max dist fMaxDist = ", fMaxDist)
				ENDIF
														
				IF fMaxDist <= 0.0
			//		PRINTLN("[RCC MISSION] [gang_backup] SERVER_PROCESS_CHASE_BACKUP_VEH  Failed to find max dist, setting tp default iGangBackupUnit = ",iGangBackupUnit, " iTeamChasing = ", iTeamChasing )
					fMaxDist = 200.0
				ENDIF
								
				PRINTLN("[RCC MISSION][gang_backup][sc_GangChaseCleanup] PROCESS_GANG_BACKUP_BRAIN Global iGangBackupFleeDist.", g_FMMC_STRUCT.sFMMCEndConditions[iTargetTeam].iGangBackupFleeDist[iTargetRule], " iRule: ", iTargetRule)
				
				IF fDistFromDrop < fMaxDist
					#IF IS_DEBUG_BUILD
						IF bGangChaseCleanup
							PRINTLN("[RCC MISSION][gang_backup][sc_GangChaseCleanup] PROCESS_GANG_BACKUP_BRAIN - backup unit told to flee, iGangBackupUnit =  ", iGangBackupUnit, " fDistFromDrop = ", fDistFromDrop, " < fMaxDist = ", fMaxDist )
						ENDIF
					#ENDIF
				
					PRINTLN("[RCC MISSION][gang_backup][TASK_BACKUP_FLEE_COORDS] PROCESS_GANG_BACKUP_BRAIN - giving TASK_BACKUP_FLEE_COORDS - 1 ",iGangBackupUnit, "  fDistFromDrop = ", fDistFromDrop, " fMaxDist = ", fMaxDist)
					TASK_BACKUP_FLEE_COORDS(iGangBackupUnit,vDropOff,TRUE)
				ELSE
					#IF IS_DEBUG_BUILD
						IF bGangChaseCleanup
							PRINTLN("[RCC MISSION][gang_backup][sc_GangChaseCleanup] PROCESS_GANG_BACKUP_BRAIN - backup unit NOT told to flee, iGangBackupUnit =  ", iGangBackupUnit, " fDistFromDrop = ", fDistFromDrop, " >= fMaxDist = ", fMaxDist )
						ENDIF
					#ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					IF bGangChaseCleanup
						PRINTLN("[RCC MISSION][gang_backup][sc_GangChaseCleanup] PROCESS_GANG_BACKUP_BRAIN - Not cleaning up as CURRENT_OBJECTIVE_LOSE_GANG_BACKUP, iGangBackupUnit =  ", iGangBackupUnit)
					ENDIF
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC SERVER_PROCESS_CHASE_BACKUP_VEH(INT iGangBackupUnit)

BOOL bConsiderTeam[FMMC_MAX_TEAMS]
INT iteam, iTargetTeam, iTargetRule
PARTICIPANT_INDEX temppart
PLAYER_INDEX tempPlayer
PED_INDEX tempPed

#IF IS_DEBUG_BUILD
	PLAYER_INDEX playerTemp
	
	IF bGangChaseCleanup
		PRINTLN("[RCC MISSION] [gang_backup] [sc_GangChaseCleanup]")
		PRINTLN("[RCC MISSION] [gang_backup] [sc_GangChaseCleanup] SERVER_PROCESS_CHASE_BACKUP_VEH - running with sc_GangChaseCleanup. Checking iGangBackupUnit = ", iGangBackupUnit, " iGangBackupProgress = ", MC_serverBD_1.GangBackup[iGangBackupUnit].iGangBackupProgress)
	ENDIF
#ENDIF

	
	SWITCH MC_serverBD_1.GangBackup[iGangBackupUnit].iGangBackupProgress
		
		CASE 0
			
			IF NOT HAS_NET_TIMER_STARTED(tdbackuptargettimer[iGangBackupUnit])
				REINIT_NET_TIMER(tdbackuptargettimer[iGangBackupUnit])
			ELSE
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdbackuptargettimer[iGangBackupUnit]) > 3000
					FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
						IF SHOULD_CHASE_BACKUP_SPAWN_FOR_TEAM(iteam)
							bConsiderTeam[iteam] = TRUE
							PRINTLN("[RCC MISSION] [gang_backup] SERVER_PROCESS_CHASE_BACKUP_VEH case 0 Gang chase should spawn for team ",iteam)
						ENDIF
					ENDFOR
					
					MC_serverBD_1.GangBackup[iGangBackupUnit].iBackSpawnTeam = GET_TEAM_TO_CHASE(bConsiderTeam[0],bConsiderTeam[1],bConsiderTeam[2],bConsiderTeam[3])
					
					IF MC_serverBD_1.GangBackup[iGangBackupUnit].iBackSpawnTeam != -1
						 
						INT iTempTeam
						iTempTeam = MC_serverBD_1.GangBackup[iGangBackupUnit].iBackSpawnTeam
						MC_serverBD_1.GangBackup[iGangBackupUnit].iFleeDist = g_FMMC_STRUCT.sFMMCEndConditions[iTempTeam].iGangBackupFleeDist[MC_serverBD_4.iCurrentHighestPriority[iTempTeam]] 
						
						PRINTLN("[RCC MISSION] [gang_backup] SERVER_PROCESS_CHASE_BACKUP_VEH case 0 Global iGangBackupFleeDist.", g_FMMC_STRUCT.sFMMCEndConditions[iTempTeam].iGangBackupFleeDist[MC_serverBD_4.iCurrentHighestPriority[iTempTeam]], " iRule: ", MC_serverBD_4.iCurrentHighestPriority[iTempTeam])
						
						IF MC_serverBD_1.GangBackup[iGangBackupUnit].iFleeDist <> 0
							PRINTLN("[RCC MISSION] [gang_backup] SERVER_PROCESS_CHASE_BACKUP_VEH case 0 iFleeDist chosen for iGangBackupUnit ", iGangBackupUnit, " = ", MC_serverBD_1.GangBackup[iGangBackupUnit].iFleeDist)
						ELSE
							MC_serverBD_1.GangBackup[iGangBackupUnit].iFleeDist = 200
							PRINTLN("[RCC MISSION] [gang_backup] SERVER_PROCESS_CHASE_BACKUP_VEH case 0 iFleeDist chosen for iGangBackupUnit ", iGangBackupUnit, " was zero. Setting to 200")
						ENDIF
						
						MC_serverBD_1.GangBackup[iGangBackupUnit].iTargetPart = GET_PARTICIPANT_OF_TEAM_FOR_BACKUP_TARGET(MC_serverBD_1.GangBackup[iGangBackupUnit].iBackSpawnTeam, MC_serverBD_1.GangBackup[iGangBackupUnit].iFleeDist)
						
						#IF IS_DEBUG_BUILD
							IF MC_serverBD_1.GangBackup[iGangBackupUnit].iTargetPart >= 0
								IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(MC_serverBD_1.GangBackup[iGangBackupUnit].iTargetPart))
									playerTemp = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(MC_serverBD_1.GangBackup[iGangBackupUnit].iTargetPart))
								ENDIF
								
								IF playerTemp <> INVALID_PLAYER_INDEX()
									PRINTLN("[RCC MISSION] [gang_backup] SERVER_PROCESS_CHASE_BACKUP_VEH case 0 iGangBackupUnit = ", iGangBackupUnit, " chasing iTargetPart = ", MC_serverBD_1.GangBackup[iGangBackupUnit].iTargetPart, " which is player ", GET_PLAYER_NAME(playerTemp))
								ELSE
									PRINTLN("[RCC MISSION] [gang_backup] SERVER_PROCESS_CHASE_BACKUP_VEH case 0 failed to find player! ")
								ENDIF
							ELSE
								PRINTLN("[RCC MISSION] [gang_backup] SERVER_PROCESS_CHASE_BACKUP_VEH case 0 iTargetPart = ", MC_serverBD_1.GangBackup[iGangBackupUnit].iTargetPart, " For iGangBackupUnit = ", iGangBackupUnit)
							ENDIF
						#ENDIF
				
						MC_serverBD_1.GangBackup[iGangBackupUnit].iGangBackupProgress++
						PRINTLN("[RCC MISSION] [gang_backup] SERVER_PROCESS_CHASE_BACKUP_VEH moving to stage 1: ",iGangBackupUnit, " iBackSpawnTeam = ", MC_serverBD_1.GangBackup[iGangBackupUnit].iBackSpawnTeam)
						PRINTLN("[RCC MISSION] [gang_backup] SERVER_PROCESS_CHASE_BACKUP_VEH ")
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 1
			
			IF MC_serverBD_1.GangBackup[iGangBackupUnit].iTargetPart = ciBACKUP_TARGET_NONE
				
				MC_serverBD_1.GangBackup[iGangBackupUnit].iTargetPart = GET_PARTICIPANT_OF_TEAM_FOR_BACKUP_TARGET(MC_serverBD_1.GangBackup[iGangBackupUnit].iBackSpawnTeam, MC_serverBD_1.GangBackup[iGangBackupUnit].iFleeDist)
				
				#IF IS_DEBUG_BUILD
					
					IF MC_serverBD_1.GangBackup[iGangBackupUnit].iTargetPart >= 0
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(MC_serverBD_1.GangBackup[iGangBackupUnit].iTargetPart))
							playerTemp = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(MC_serverBD_1.GangBackup[iGangBackupUnit].iTargetPart))
						ENDIF
						
						IF playerTemp <> INVALID_PLAYER_INDEX()
							PRINTLN("[RCC MISSION] [gang_backup] SERVER_PROCESS_CHASE_BACKUP_VEH case 1 iGangBackupUnit = ", iGangBackupUnit, " chasing iTargetPart = ", MC_serverBD_1.GangBackup[iGangBackupUnit].iTargetPart, " which is player ", GET_PLAYER_NAME(playerTemp))
						ELSE
							PRINTLN("[RCC MISSION] [gang_backup] SERVER_PROCESS_CHASE_BACKUP_VEH case 1 failed to find player! ")
						ENDIF
					ELSE
						PRINTLN("[RCC MISSION] [gang_backup] SERVER_PROCESS_CHASE_BACKUP_VEH case 1 iTargetPart = ", MC_serverBD_1.GangBackup[iGangBackupUnit].iTargetPart, " For iGangBackupUnit = ", iGangBackupUnit)
					ENDIF
				#ENDIF
			ENDIF
			
			
			IF MC_serverBD_1.GangBackup[iGangBackupUnit].iTargetPart != ciBACKUP_TARGET_NONE
				MC_serverBD_1.GangBackup[iGangBackupUnit].iGangBackupProgress++
				PRINTLN("[RCC MISSION] [gang_backup] SERVER_PROCESS_CHASE_BACKUP_VEH moving to stage 2: ",iGangBackupUnit)
			ELSE
				REINIT_NET_TIMER(tdbackuptargettimer[iGangBackupUnit])
				MC_serverBD_1.GangBackup[iGangBackupUnit].iBackSpawnTeam = -1
				MC_serverBD_1.GangBackup[iGangBackupUnit].iGangBackupProgress = 0
				PRINTLN("[RCC MISSION] [gang_backup] SERVER_PROCESS_CHASE_BACKUP_VEH moving to stage back to 0 as iTargetPart is -1: ",iGangBackupUnit)
			ENDIF
			
		BREAK
					
							
		CASE 2
		
			IF IS_BIT_SET(MC_serverBD_1.GangBackup[iGangBackupUnit].iBackupBitset, SBBBOOL_BACKUP_CREATED)
			AND NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupVeh)
				MC_serverBD_1.GangBackup[iGangBackupUnit].iGangBackupProgress++
				PRINTLN("[RCC MISSION] [gang_backup] SERVER_PROCESS_CHASE_BACKUP_VEH moving to stage 3: ",iGangBackupUnit)
			ELSE
				IF NOT SHOULD_CHASE_BACKUP_SPAWN_FOR_TEAM(MC_serverBD_1.GangBackup[iGangBackupUnit].iBackSpawnTeam)
					PRINTLN("[RCC MISSION] [gang_backup] SERVER_PROCESS_CHASE_BACKUP_VEH Waiting for spawn but should no longer spawn for team: ",MC_serverBD_1.GangBackup[iGangBackupUnit].iBackSpawnTeam, ". Will reset to stage 0 unit ", iGangBackupUnit)
					RESET_GANG_BACKUP_DATA(iGangBackupUnit)
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 3
			
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupVeh)
				#IF IS_DEBUG_BUILD
					IF bGangChaseCleanup
						PRINTLN("[RCC MISSION] [gang_backup] [sc_GangChaseCleanup] Backup unit exists ", iGangBackupUnit)
					ENDIF
				#ENDIF
				
				iTargetTeam = MC_serverBD_1.GangBackup[iGangBackupUnit].iBackSpawnTeam
				iTargetRule = MC_serverBD_4.iCurrentHighestPriority[iTargetTeam]
				
				IF NOT IS_BIT_SET(MC_serverBD_1.GangBackup[iGangBackupUnit].iBackupBitset, SBBBOOL_BACKUP_DESPAWN_ON_NEW_TYPE)
				OR ((iTargetRule < FMMC_MAX_RULES) AND (MC_serverBD_1.GangBackup[iGangBackupUnit].iBackupType = g_FMMC_STRUCT.sFMMCEndConditions[iTargetTeam].iGangBackupType[iTargetRule]))
					
					IF MC_serverBD_1.GangBackup[iGangBackupUnit].iTargetPart >= 0
						
						#IF IS_DEBUG_BUILD
							IF bGangChaseCleanup
								PRINTLN("[RCC MISSION] [gang_backup] [sc_GangChaseCleanup] Target part ", MC_serverBD_1.GangBackup[iGangBackupUnit].iTargetPart)
							ENDIF
						#ENDIF
						
						temppart = INT_TO_PARTICIPANTINDEX(MC_serverBD_1.GangBackup[iGangBackupUnit].iTargetPart)
						
						IF NETWORK_IS_PARTICIPANT_ACTIVE(temppart)
							
							tempplayer = NETWORK_GET_PLAYER_INDEX(temppart)
							
							IF IS_NET_PLAYER_OK(tempplayer)
								
								#IF IS_DEBUG_BUILD
									IF bGangChaseCleanup
										PRINTLN("[RCC MISSION] [gang_backup] [sc_GangChaseCleanup] Player ok. Player = ", GET_PLAYER_NAME(tempplayer), " iGangBackupUnit = ", iGangBackupUnit)
									ENDIF
								#ENDIF
									
								tempPed = GET_PLAYER_PED(tempplayer)
								
								IF NOT IS_PED_INJURED(tempPed)
									
									PROCESS_GANG_BACKUP_BRAIN(iGangBackupUnit, tempPed, iTargetTeam, iTargetRule, MC_serverBD_1.GangBackup[iGangBackupUnit].iTargetPart)
									
								ENDIF
							ELSE
								CLEANUP_BACKUP_UNIT(iGangBackupUnit)
							ENDIF
						ELSE
							CLEANUP_BACKUP_UNIT(iGangBackupUnit)
						ENDIF
						
					ELIF MC_serverBD_1.GangBackup[iGangBackupUnit].iTargetPart = ciBACKUP_TARGET_AVENGER_DUMMY_PED
						
						IF MC_serverBD_1.iBackupTargetAvenger != -1
							
							tempPed = GET_AVENGER_DUMMY_PED(MC_serverBD_1.iBackupTargetAvenger)
							
							IF DOES_ENTITY_EXIST(tempPed)
								
								PROCESS_GANG_BACKUP_BRAIN(iGangBackupUnit, tempPed, iTargetTeam, iTargetRule)
								
							ELSE
								CLEANUP_BACKUP_UNIT(iGangBackupUnit)
							ENDIF
						ELSE
							CLEANUP_BACKUP_UNIT(iGangBackupUnit)
						ENDIF
						
					ELSE
						CLEANUP_BACKUP_UNIT(iGangBackupUnit, TRUE)
					ENDIF
				ELSE
					CLEANUP_BACKUP_UNIT(iGangBackupUnit,TRUE)
				ENDIF
			ELSE
				IF IS_UNIT_CLEANED_UP(iGangBackupUnit)
					
					IF MC_serverBD_1.GangBackup[iGangBackupUnit].iBackSpawnTeam != -1
						MC_serverBD_1.iNumBackupChasing[MC_serverBD_1.GangBackup[iGangBackupUnit].iBackSpawnTeam]--
					ENDIF
					RESERVE_NETWORK_MISSION_VEHICLES(GET_NUM_RESERVED_MISSION_VEHICLES() - 1)					
					RESERVE_NETWORK_MISSION_PEDS(GET_NUM_RESERVED_MISSION_PEDS() - MC_serverBD_1.GangBackup[iGangBackupUnit].iMaxPeds)
					
					PRINTLN("[RCC MISSION] [gang_backup] SERVER_PROCESS_CHASE_BACKUP_VEH Unit Cleaned up - reset - stage 0: ",iGangBackupUnit, " GET_NUM_RESERVED_MISSION_VEHICLES = ", GET_NUM_RESERVED_MISSION_VEHICLES(), " GET_NUM_RESERVED_MISSION_PEDS = ", GET_NUM_RESERVED_MISSION_PEDS(), " iMaxPeds: ", MC_serverBD_1.GangBackup[iGangBackupUnit].iMaxPeds)
					RESET_GANG_BACKUP_DATA(iGangBackupUnit)
				ENDIF
			ENDIF
			
		BREAK
		
	ENDSWITCH


ENDPROC

PROC CHASE_BACKUP_BLIPS(INT iGangBackupUnit) 
	
	INT iPed
	INT iblip
	PED_INDEX tempPed
	
	#IF IS_DEBUG_BUILD
		IF bWdLoseGangBackupDebug
			IF CURRENT_OBJECTIVE_LOSE_GANG_BACKUP()
				PRINTLN("[RCC MISSION] [CHASE_BACKUP_BLIPS] CURRENT_OBJECTIVE_LOSE_GANG_BACKUP is set ")
			ELSE
				PRINTLN("[RCC MISSION] [CHASE_BACKUP_BLIPS] CURRENT_OBJECTIVE_LOSE_GANG_BACKUP is NOT set ")
			ENDIF
		ENDIF
	#ENDIF
	
	FOR iped = 0 TO (MAX_BACKUP_PEDS_PER_VEHICLE-1)
		
		iblip = (4*iGangBackupUnit+iped)
		
		IF NOT IS_NET_PED_INJURED(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupPed[iped])
		AND MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam] < FMMC_MAX_RULES
			
			tempPed = NET_TO_PED(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupPed[iped])
			
			IF iped = 0
				IF IS_BIT_SET(MC_serverBD_1.GangBackup[iGangBackupUnit].iBackupBitset, SBBBOOL_BACKUP_ON_GOTO_TASK)
				AND NETWORK_HAS_CONTROL_OF_ENTITY(tempPed)
					LOAD_COLLISION_ON_GANG_CHASE_PED_IF_POSSIBLE(tempPed, iGangBackupUnit) // Need collision to do a goto task
				ELSE
					CLEANUP_COLLISION_ON_GANG_CHASE_PED(tempPed, iGangBackupUnit)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(MC_serverBD_1.GangBackup[iGangBackupUnit].iBackupBitset, SBBOOL_BACKUP_USES_COP_BLIPS)
				// Don't need to do anything more, and if we're in here once then we're past ped 0 - the only ped needing to do the collision loading above
				BREAKLOOP
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE9_ONLY_BLIP_IN_INTERIOR)
				IF IS_ENTITY_ALIVE(tempPed)
					IF NOT IS_ENTITY_IN_SAME_INTERIOR_AS_LOCAL_PLAYER(tempPed)
						PRINTLN("[RCC MISSION] [CHASE_BACKUP_BLIPS] Skipping over blipping this ped, as not in same interior ", iPed)
						RELOOP
					ENDIF
				ENDIF
			ENDIF
			
//			#IF IS_DEBUG_BUILD
//				TEXT_LABEL_31 tlDebugName = "GangPed["
//				tlDebugName += iGangBackupUnit
//				tlDebugName += "]["
//				tlDebugName += iped
//				tlDebugName += "]"
//			#ENDIF
			
			#IF IS_DEBUG_BUILD
				IF bGangChaseCleanup
					VECTOR vCoord = GET_ENTITY_COORDS(tempPed)
					PRINTLN("[RCC MISSION] [gang_backup] [sc_GangChaseCleanup] UPDATE_ENEMY_NET_PED_BLIP called for backup unit ", iGangBackupUnit, " Ped ", iped, " Ped coord = ", vCoord," Checking vehicle...")
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupVeh)
						vCoord = GET_ENTITY_COORDS(NET_TO_VEH(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupVeh), FALSE)
						PRINTLN("[RCC MISSION] [gang_backup] [sc_GangChaseCleanup] Backup vehicle exists! Backup unit ", iGangBackupUnit, " Coords ",vCoord)
					ELSE
						PRINTLN("[RCC MISSION] [gang_backup] [sc_GangChaseCleanup] Backup vehicle does not exist! Backup unit ", iGangBackupUnit)
					ENDIF
				ENDIF
			#ENDIF
			
			IF CURRENT_OBJECTIVE_LOSE_GANG_BACKUP()
				//-- Force blips on if player has to lose the gang backup.
				UPDATE_ENEMY_NET_PED_BLIP( 	MC_serverBD_1.GangBackup[iGangBackupUnit].BackupPed[iped], 
											biChasePedBlip[iblip],
											DEFAULT, 				// fNoticableRadius
											DEFAULT, 								// bIsProfessionalAIGang
											TRUE,									// bForceBlipOn
											DEFAULT,								// bShowVisionCone
											DEFAULT									// strBlipName
											)										// strDebugName
			ELSE
				UPDATE_ENEMY_NET_PED_BLIP(	MC_serverBD_1.GangBackup[iGangBackupUnit].BackupPed[iped],
					biChasePedBlip[iblip],
					g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].fGangBlipNoticeRange[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], // fNoticableRadius
					DEFAULT, // bIsProfessionalAIGang
					IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitsetFive[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE5_GANG_CHASE_BLIP_FORCED_ON), // bForceBlipOn
					DEFAULT, // bShowVisionCone
					DEFAULT // strBlipName
					) // strDebugName
			ENDIF
		ELSE
			IF iped = 0
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupPed[iped])
					CLEANUP_COLLISION_ON_GANG_CHASE_PED(NET_TO_PED(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupPed[iped]), iGangBackupUnit)
				ELSE
					CLEANUP_COLLISION_ON_GANG_CHASE_PED(NULL, iGangBackupUnit)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(MC_serverBD_1.GangBackup[iGangBackupUnit].iBackupBitset, SBBOOL_BACKUP_USES_COP_BLIPS)
				// Don't need to do anything more, and if we're in here once then we're past ped 0 - the only ped needing to do the collision loading above
				BREAKLOOP
			ENDIF
			
			#IF IS_DEBUG_BUILD
				IF biChasePedBlip[iblip].PedID != NULL
					CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][AI] Chase ped ", iblip, " AI blip cleaning up due to ped being injured.")
				ENDIF
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupPed[iped])
					PRINTLN("[RCC MISSION] Net ID for ped ", iBlip, " still exists. Definitely injured.")
				ENDIF
			#ENDIF
			
			CLEANUP_AI_PED_BLIP(biChasePedBlip[iblip])
		ENDIF
	ENDFOR
	
ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: GANG CHASE CREATION !
//
//************************************************************************************************************************************************************



PROC SET_UP_FMMC_BACKUP_PED_RC_TASK(PED_INDEX tempPed, INT iBackupType, INT iBackupUnit, PED_INDEX targetPed)
	
	IF (NOT IS_GANG_CHASE_AIR(iBackupType))
	OR (NOT IS_PED_IN_ANY_VEHICLE(tempPed))
	OR (GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(tempPed), VS_DRIVER) != tempPed)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, FALSE)
		TASK_COMBAT_HATED_TARGETS_AROUND_PED(tempPed, 299)
	ELSE
		// We're flying a helicopter! Might be too far away from the player, so let's do the heli flying task
		VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(tempPed)
		//INT iFlightHeight = CEIL(GET_ENTITY_HEIGHT_ABOVE_GROUND(tempVeh))
		HELIMODE eHeliMode = HF_MaintainHeightAboveTerrain
		
		VEHICLE_INDEX targetVeh = NULL
		VECTOR vTargetCoords = GET_ENTITY_COORDS(targetPed)
		
		IF MC_serverBD_1.GangBackup[iBackupUnit].iTargetPart >= 0
			IF IS_PED_IN_ANY_VEHICLE(targetPed)
				targetVeh = GET_VEHICLE_PED_IS_IN(targetPed)
			ENDIF
		ENDIF
		
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, TRUE)
		LOAD_COLLISION_ON_GANG_CHASE_PED_IF_POSSIBLE(tempPed, iBackupUnit)
		
		FLOAT fCruiseSpeed = FMAX(25.0, GET_VEHICLE_ESTIMATED_MAX_SPEED(tempVeh))
		
		PRINTLN("[RCC MISSION] [gang_backup] SET_UP_FMMC_BACKUP_PED_RC_TASK - Tasking ped to go to part ",MC_serverBD_1.GangBackup[iBackupUnit].iTargetPart," at vTargetCoords ",vTargetCoords,", fCruiseSpeed ",fCruiseSpeed)
		
		IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(tempVeh))
			TASK_HELI_MISSION(tempPed, tempVeh, targetVeh, targetPed, vTargetCoords, MISSION_ATTACK, fCruiseSpeed, 5, -1, 0, 10, DEFAULT, eHeliMode)
		ELSE
			TASK_PLANE_MISSION(tempPed, tempVeh, targetVeh, targetPed, vTargetCoords, MISSION_ATTACK, fCruiseSpeed, 5, -1, 0, 10, DEFAULT)
		ENDIF
		
		SET_BIT(MC_serverBD_1.GangBackup[iBackupUnit].iBackupBitset, SBBBOOL_BACKUP_ON_GOTO_TASK)
		
	ENDIF
	
ENDPROC

/// PURPOSE: Sets up gang chase peds with the properties and flags required
PROC SET_UP_FMMC_BACKUP_PED_RC(PED_INDEX piPassed, INT iPedForDecor, INT istartingplayers, WEAPON_TYPE wtWeapon, INT iteam, INT iBackupUnit, PED_INDEX targetPed, BOOL bSpawnForwards = FALSE)
	
	INT irepeat
	INT iAccuracy
	COMBAT_ABILITY_LEVEL eCombatAbility
	FLOAT fstartingplayers 
	fstartingplayers = TO_FLOAT(istartingplayers)
	FLOAT fNumParticipants
	fNumParticipants = TO_FLOAT(g_FMMC_STRUCT.iNumParticipants)
	FLOAT fDifficultyMod
	CONST_FLOAT fLesMultiplier 0.8
	fDifficultyMod = (fstartingplayers/fNumParticipants)
	PRINTLN("[RCC MISSION] [gang_backup] g_FMMC_STRUCT.iNumParticipants = ",g_FMMC_STRUCT.iNumParticipants)
	PRINTLN("[RCC MISSION] [gang_backup] istartingplayers = ",istartingplayers)
	PRINTLN("[RCC MISSION] [gang_backup] fDifficultyMod = ",fDifficultyMod)
	
	IF MC_serverBD.iDifficulty = DIFF_EASY
		fDifficultyMod = fDifficultyMod - 1.0
		PRINTLN("[RCC MISSION] [gang_backup] fDifficultyMod after easy mod = ",fDifficultyMod)
	ELIF MC_serverBD.iDifficulty = DIFF_HARD
		fDifficultyMod = fDifficultyMod + 1.0
		PRINTLN("[RCC MISSION] [gang_backup] fDifficultyMod after hard mod = ",fDifficultyMod)
	ENDIF

	GIVE_DELAYED_WEAPON_TO_PED(piPassed, wtWeapon, 25000, TRUE)
	SET_CURRENT_PED_WEAPON(piPassed,wtWeapon,TRUE)
		
	IF NETWORK_IS_GAME_IN_PROGRESS()
		FOR irepeat = 0 TO (FMMC_MAX_TEAMS-1)
			SET_PED_CAN_BE_TARGETTED_BY_TEAM(piPassed, irepeat, TRUE)
		ENDFOR
	ENDIF
	
	SET_ENTITY_IS_TARGET_PRIORITY(piPassed, TRUE)
	
	SET_PED_RELATIONSHIP_GROUP_HASH(piPassed,rgFM_AiHatePlyrLikeAllAI)
	
	MODEL_NAMES mnPed = GET_ENTITY_MODEL(piPassed)
	
	IF mnPed = mp_m_fibsec_01
		SET_PED_CONFIG_FLAG(piPassed, PCF_DontBlip, FALSE)
		SET_PED_CONFIG_FLAG(piPassed, PCF_DontBlipCop, FALSE)
		SET_PED_AS_COP(piPassed,FALSE)
		SET_PED_TARGET_LOSS_RESPONSE(piPassed,TLR_SEARCH_FOR_TARGET)
		SET_PED_CONFIG_FLAG(piPassed,PCF_CanAttackNonWantedPlayerAsLaw,TRUE)
		SET_PED_CONFIG_FLAG(piPassed,PCF_OnlyUpdateTargetWantedIfSeen,TRUE)
	ELSE
		IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE8_GANG_CHASE_GET_COP_BLIPS)
			SET_PED_CONFIG_FLAG(piPassed, PCF_DontBlip, FALSE)
			SET_PED_CONFIG_FLAG(piPassed, PCF_DontBlipCop, FALSE)
			SET_PED_AS_COP(piPassed,FALSE)
			SET_PED_CONFIG_FLAG(piPassed,PCF_CanAttackNonWantedPlayerAsLaw,TRUE)
		ELSE
			IF IS_MODEL_AMBIENT_MISSION_COP(mnPed)
				SET_PED_CONFIG_FLAG(piPassed,PCF_CanAttackNonWantedPlayerAsLaw,TRUE)
			ENDIF
		ENDIF
		SET_PED_TARGET_LOSS_RESPONSE(piPassed, TLR_NEVER_LOSE_TARGET)
		PRINTLN("[RCC MISSION] [gang_backup] setting TLR_NEVER_LOSE_TARGET on backup ped")
	ENDIF
	
	//SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_ALWAYS_FIGHT, TRUE)
	//SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_CAN_FIGHT_ARMED_PEDS_WHEN_NOT_ARMED, TRUE)
	SET_PED_COMBAT_MOVEMENT(piPassed, CM_WILLADVANCE)
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_CAN_USE_FRUSTRATED_ADVANCE,TRUE)
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_CAN_CHARGE,TRUE)
	
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_DISABLE_ALL_RANDOMS_FLEE, TRUE)
	
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_DISABLE_BLOCK_FROM_PURSUE_DURING_VEHICLE_CHASE,FALSE)
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_DISABLE_CRUISE_IN_FRONT_DURING_BLOCK_DURING_VEHICLE_CHASE,FALSE)

	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_USE_VEHICLE,TRUE)
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_USE_VEHICLE_ATTACK_IF_VEHICLE_HAS_MOUNTED_GUNS,TRUE)
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_DO_DRIVEBYS,TRUE)
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_DISABLE_SEEK_DUE_TO_LINE_OF_SIGHT,FALSE)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE9_GANG_CHASE_PREVENT_COMMANDEERING_VEHICLES)
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_CAN_COMMANDEER_VEHICLES,FALSE)
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_USE_VEHICLE_ATTACK_IF_VEHICLE_HAS_MOUNTED_GUNS,FALSE)
		SET_PED_CONFIG_FLAG(piPassed, PCF_PedsJackingMeDontGetIn, TRUE)
	ENDIF
	
	SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(piPassed,KNOCKOFFVEHICLE_HARD)

	SET_PED_FLEE_ATTRIBUTES(piPassed, FA_USE_VEHICLE,TRUE)
	SET_PED_FLEE_ATTRIBUTES(piPassed, FA_DISABLE_COWER,TRUE)
	SET_PED_FLEE_ATTRIBUTES(piPassed,  FA_COWER_INSTEAD_OF_FLEE,FALSE)
	SET_PED_FLEE_ATTRIBUTES(piPassed, FA_DISABLE_FLEE_FROM_INDIRECT_THREATS,FALSE)	
	
	SET_PED_CONFIG_FLAG(piPassed,PCF_AICanDrivePlayerAsRearPassenger,TRUE)
	
	SET_PED_CONFIG_FLAG(piPassed, PCF_KeepTargetLossResponseOnCleanup, TRUE)
	SET_PED_CONFIG_FLAG(piPassed, PCF_KeepRelationshipGroupAfterCleanUp, TRUE)

	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFourteen[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE14_BLOCK_GANG_CHASE_PEDS_ABILITY_TO_CLIMB_LADDERS)
		PRINTLN("[GANG_CHASE] CREATE_GANG_CHASE_UNIT_VEHICLE - Unit: ", iBackupUnit, " Disabling Ability to climb ladders.")
		SET_PED_CONFIG_FLAG(piPassed, PCF_DisableLadderClimbing, TRUE)
	ENDIF
	
	INT ihealth = 200
	FLOAT ftunablehealth

	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
	
		PRINTLN("[RCC MISSION] [gang_backup] starting ihealth = ",ihealth)
		
		FLOAT fhealth = ihealth + ( 0.5 * fDifficultyMod * ( ihealth - 100 ) )
		
		PRINTLN("[RCC MISSION] [gang_backup] modded fhealth = ", fhealth )
		
		ihealth = ROUND( fhealth )
		
		PRINTLN("[RCC MISSION] [gang_backup] natural modded ihealth = ", ihealth )
		
		ftunablehealth = ihealth * g_sMPTunables.fAiHealthModifier
	
		ihealth = ROUND( ftunablehealth )
	
		PRINTLN( "[RCC MISSION] [gang_backup] tunable modded ihealth = ", ihealth )
		
		#IF IS_DEBUG_BUILD 
			IF lw_fSetMissPedHealth != 1.0
				fhealth = ihealth * lw_fSetMissPedHealth
				ihealth = ROUND(fhealth)
				PRINTLN("[RCC MISSION] [gang_backup] chase ped debug health override starting ihealth = ",ihealth)
				
			ENDIF
		#ENDIF
	
		IF ihealth < 101
			ihealth = 101
		ENDIF

	ENDIF

	SET_ENTITY_HEALTH(piPassed, ihealth)
	
	IF MC_serverBD.iDifficulty = DIFF_EASY
		iAccuracy = 2
		SET_PED_SHOOT_RATE(piPassed, 20)
		
		IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[MC_ServerBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE11_USE_GANG_CHASE_BURST_FIRE_MODE)
			PRINTLN("[RCC MISSION] [gang_backup] Using FIRING_PATTERN_BURST_FIRE for this ped. (1)")
			SET_PED_FIRING_PATTERN(piPassed,FIRING_PATTERN_BURST_FIRE)
		ELSE
			PRINTLN("[RCC MISSION] [gang_backup] Using FIRING_PATTERN_SHORT_BURSTS for this ped. (1)")
			SET_PED_FIRING_PATTERN(piPassed,FIRING_PATTERN_SHORT_BURSTS)
		ENDIF
		
		eCombatAbility = CAL_POOR
	ELIF MC_serverBD.iDifficulty = DIFF_HARD
		iAccuracy = 10
		eCombatAbility = CAL_PROFESSIONAL
	ELSE
		iAccuracy = 5
		eCombatAbility = CAL_AVERAGE
	ENDIF
	
	
	FLOAT fAccuracy = iAccuracy + (0.25*iAccuracy*fDifficultyMod)
	
	fAccuracy = fAccuracy*fLesMultiplier
	
	iAccuracy = ROUND(fAccuracy)
	
	PRINTLN("[RCC MISSION] [gang_backup] normal modded iAccuracy = ",iAccuracy)
	
	#IF IS_DEBUG_BUILD 
		IF lw_fSetMissPedAccuracy != 1.0
			
			faccuracy = iAccuracy * lw_fSetMissPedAccuracy

			iAccuracy = ROUND(faccuracy)
			
			PRINTLN("[RCC MISSION] [gang_backup] chase ped debug accuracy override starting iAccuracy = ",iAccuracy)
			
		ENDIF
	#ENDIF
	
	IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangAccuracy[MC_ServerBD_4.iCurrentHighestPriority[iTeam]] != FMMC_GANG_CHASE_PED_ACCURACY_DEFAULT 
			IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangAccuracy[MC_ServerBD_4.iCurrentHighestPriority[iTeam]] <=  5
				SET_PED_SHOOT_RATE(piPassed,20)
				
				IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
				AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[MC_ServerBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE11_USE_GANG_CHASE_BURST_FIRE_MODE)
					PRINTLN("[RCC MISSION] [gang_backup] Using FIRING_PATTERN_BURST_FIRE for this ped. (2)")
					SET_PED_FIRING_PATTERN(piPassed,FIRING_PATTERN_BURST_FIRE)
				ELSE
					PRINTLN("[RCC MISSION] [gang_backup] Using FIRING_PATTERN_SHORT_BURSTS for this ped. (2)")
					SET_PED_FIRING_PATTERN(piPassed,FIRING_PATTERN_SHORT_BURSTS)
				ENDIF
				
				eCombatAbility = CAL_POOR
				PRINTLN("[RCC MISSION] [gang_backup] chase ped accuracy FMMC_GANG_CHASE_PED_ACCURACY_LOW = ")
			ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangAccuracy[MC_ServerBD_4.iCurrentHighestPriority[iTeam]] <= 10
				eCombatAbility = CAL_AVERAGE
				PRINTLN("[RCC MISSION] [gang_backup] chase ped accuracy FMMC_GANG_CHASE_PED_ACCURACY_AVERAGE = ")
			ELSE
				eCombatAbility = CAL_PROFESSIONAL
				PRINTLN("[RCC MISSION] [gang_backup] chase ped accuracy FMMC_GANG_CHASE_PED_ACCURACY_HIGH = ")
			ENDIF
			iAccuracy = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangAccuracy[MC_ServerBD_4.iCurrentHighestPriority[iTeam]] 
			PRINTLN("[RCC MISSION] [gang_backup] chase ped accuracy override = ",iAccuracy)
		ENDIF
	ENDIF
	
	IF iAccuracy < 0
		iAccuracy = 0
	ENDIF
	IF iAccuracy > 100
		iAccuracy = 100
	ENDIF
	
	IF GET_ENTITY_MODEL(piPassed)= mp_g_m_pros_01
		SET_PED_COMPONENT_VARIATION(piPassed,PED_COMP_SPECIAL2,0,0)
		PRINTLN("[RCC MISSION] [gang_backup] removing prof mask from backup: ")
	ENDIF
	
	SET_PED_COMBAT_ABILITY(piPassed, eCombatAbility)
	SET_PED_ACCURACY(piPassed, iAccuracy)
	
	SET_PED_DIES_WHEN_INJURED(piPassed,TRUE)
	SET_PED_KEEP_TASK(piPassed,TRUE)
	
	SET_PED_TO_INFORM_RESPECTED_FRIENDS(piPassed, 299, 40)
	SET_PED_COMBAT_RANGE(piPassed, CR_FAR)
	SET_PED_SEEING_RANGE(piPassed, 299)
	
	SET_UP_FMMC_BACKUP_PED_RC_TASK(piPassed, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangBackupType[MC_serverBD_4.iCurrentHighestPriority[iTeam]], iBackupUnit, targetPed)
	
	IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
	AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE10_GANG_CHASE_NO_HOMING_WEAPONS)
		IF IS_PED_IN_ANY_VEHICLE(piPassed)
		AND (GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(piPassed), VS_DRIVER) = piPassed)
			IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangBackupType[MC_serverBD_4.iCurrentHighestPriority[iTeam]] = ciBACKUP_TYPE_AIR_BUZZ_MERRYWEATHER
			OR g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangBackupType[MC_serverBD_4.iCurrentHighestPriority[iTeam]] = ciBACKUP_TYPE_AIR_BUZZ_NOOSE
			OR g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangBackupType[MC_serverBD_4.iCurrentHighestPriority[iTeam]] = ciBACKUP_TYPE_AIR_BUZZ_PROFESSIONALS
			OR g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangBackupType[MC_serverBD_4.iCurrentHighestPriority[iTeam]] = ciBACKUP_TYPE_AIR_BUZZ_AVON_GOON
				PRINTLN("[RCC MISSION] [gang_backup] Disabling homing rockets")
				
				DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_VEHICLE_SPACE_ROCKET, GET_VEHICLE_PED_IS_IN(piPassed), piPassed)
				SET_CURRENT_PED_VEHICLE_WEAPON(piPassed, WEAPONTYPE_VEHICLE_PLAYER_BUZZARD)
				SET_PED_CAN_SWITCH_WEAPON(piPassed, FALSE)
				
				SET_PED_FIRING_PATTERN(piPassed, FIRING_PATTERN_BURST_FIRE_HELI)
				SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_FORCE_CHECK_ATTACK_ANGLE_FOR_MOUNTED_GUNS, TRUE)
				
				#IF IS_DEBUG_BUILD
				WEAPON_TYPE eWeap
				IF GET_CURRENT_PED_VEHICLE_WEAPON(piPassed, eWeap)
					PRINTLN("[RCC MISSION] [gang_backup] Weapon is now: ", ENUM_TO_INT(eWeap))
				ENDIF
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
#IF IS_DEBUG_BUILD
	IF IS_PED_IN_ANY_VEHICLE(piPassed)
		VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(piPassed)
		IF IS_ENTITY_ALIVE(tempVeh)
			PRINTLN("[RCC MISSION] [gang_backup] Tasking ped with combat (SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(piPassed), "), (SCRIPT_VEHICLE_",  NETWORK_ENTITY_GET_OBJECT_ID(tempVeh), ")")
		ELSE
			PRINTLN("[RCC MISSION] [gang_backup] Tasking ped with combat (SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(piPassed), ") vehicle is dead")
		ENDIF
	ELSE
		PRINTLN("[RCC MISSION] [gang_backup] Tasking ped with combat (SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(piPassed), ") ped is not in vehicle")
	ENDIF
#ENDIF
	
	IF bSpawnForwards
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_CAN_CRUISE_AND_BLOCK_IN_VEHICLE, TRUE)
		SET_COMBAT_FLOAT(piPassed, CCF_AUTOMOBILE_SPEED_MODIFIER, 2.0)
		SET_DRIVER_AGGRESSIVENESS(piPassed, 1.0)
		PRINTLN("[RCC MISSION] [gang_backup] Set CA_CAN_CRUISE_AND_BLOCK_IN_VEHICLE")
	ENDIF
	
	SET_CREATOR_ID_INT_DECOR_ON_ENTITY(GET_ENTITY_FROM_PED_OR_VEHICLE(piPassed),iPedForDecor, TRUE)
	
ENDPROC

/// PURPOSE: Sets up gang chase vehicles with the properties and flags required
PROC SET_UP_FMMC_BACKUP_VEH_RC(VEHICLE_INDEX viPassed, INT iteam, BOOL bSpawnForwards = FALSE)
	
	println("[RCC MISSION] [MJM] [gang_backup] SET_UP_FMMC_BACKUP_VEH_RC ")
	
	SET_VEHICLE_HAS_STRONG_AXLES(viPassed, TRUE)
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(viPassed, TRUE)
	SET_VEHICLE_ON_GROUND_PROPERLY(viPassed)
	SET_VEHICLE_ALLOW_NO_PASSENGERS_LOCKON(viPassed, TRUE)
	SET_VEHICLE_IS_STOLEN(viPassed, FALSE)
	SET_VEHICLE_MAY_BE_USED_BY_GOTO_POINT_ANY_MEANS(viPassed, TRUE)
	SET_VEHICLE_CAN_BE_USED_BY_FLEEING_PEDS(viPassed, TRUE)
	SET_VEHICLE_ENGINE_ON(viPassed, TRUE,TRUE)
	
	IF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(viPassed))
	OR IS_THIS_MODEL_A_FIGHTER_JET(GET_ENTITY_MODEL(viPassed))
		CONTROL_LANDING_GEAR(viPassed, LGC_RETRACT_INSTANT)
		PRINTLN("[gang_backup] SET_UP_FMMC_BACKUP_VEH_RC - Telling the landing gear of this plane to retract instantly")
	ENDIF
	
	MODEL_NAMES mnVeh = GET_ENTITY_MODEL(viPassed)
	BOOL bAirVehicle = IS_THIS_MODEL_A_HELI(mnVeh) OR IS_THIS_MODEL_A_PLANE(mnVeh)
	
	IF NOT bAirVehicle
		FLOAT fForwardSpeed
		
		IF NOT bSpawnForwards
			fForwardSpeed = 5.0
		ELSE
			fForwardSpeed = 20.0
		ENDIF
		
		PRINTLN("[RCC MISSION][gang_backup] SET_UP_FMMC_BACKUP_VEH_RC - Set vehicle with forward speed ",fForwardSpeed)
		SET_VEHICLE_FORWARD_SPEED(viPassed,fForwardSpeed)
	ENDIF
	
	IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
		
		//*******************   ATTENTION *****************************//
		// Gang backup colour is saved in contiguous ints that mirrior //
		// the menu selection NOT the actual colour value
		
		FMMC_SET_THIS_VEHICLE_COLOURS(viPassed, GET_VEHICLE_COLOUR_FROM_SELECTION( g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangBackupColour[MC_serverBD_4.iCurrentHighestPriority[iTeam]] ), -1)
		
		//*************************************************************//
		
		
		println("[RCC MISSION] [MJM] [gang_backup] Set backup vehicle colours: colour: ",g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangBackupColour[MC_serverBD_4.iCurrentHighestPriority[iTeam]])
		IF g_FMMC_STRUCT.sFMMCEndConditions[iteam].iGangVehHealth[MC_serverBD_4.iCurrentHighestPriority[iteam]] != 100
		AND g_FMMC_STRUCT.sFMMCEndConditions[iteam].iGangVehHealth[MC_serverBD_4.iCurrentHighestPriority[iteam]] > 0
			FLOAT fHealth = g_FMMC_STRUCT.sFMMCEndConditions[iteam].iGangVehHealth[MC_serverBD_4.iCurrentHighestPriority[iteam]]/100.0
			fHealth = fHealth * 1000
			INT ihealth = ROUND(fHealth)
			SET_ENTITY_HEALTH(viPassed, iHealth)
			SET_VEHICLE_ENGINE_HEALTH(viPassed,fHealth)
			SET_VEHICLE_PETROL_TANK_HEALTH(viPassed,fHealth)
			SET_VEHICLE_BODY_HEALTH(viPassed,fHealth)
			IF ihealth <= 400
				SET_FORCE_VEHICLE_ENGINE_DAMAGE_BY_BULLET(viPassed,TRUE)
			ENDIF
		ENDIF	
	ENDIF
	
ENDPROC

FUNC WEAPON_TYPE GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(INT iBackupType)
	//-- From list from Josh, bug 2157386
	WEAPON_TYPE weaponToUse
	INT iRand
	SWITCH iBackupType
		CASE ciBACKUP_TYPE_MERRYWEATHER
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			IF iRand = 0	weaponToUse = WEAPONTYPE_MICROSMG
			ELIF iRand = 1	weaponToUse = WEAPONTYPE_DLC_HEAVYPISTOL
			ENDIF
		BREAK
		
		CASE ciBACKUP_TYPE_LOST
		CASE ciBACKUP_TYPE_LOST_SLAMVAN2
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			IF iRand = 0	weaponToUse = WEAPONTYPE_MICROSMG
			ELIF iRand = 1	weaponToUse = WEAPONTYPE_PISTOL
			ELIF iRand = 2	weaponToUse = WEAPONTYPE_SAWNOFFSHOTGUN
			ENDIF
		BREAK
		
		CASE ciBACKUP_TYPE_VAGOS
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			IF iRand = 0	weaponToUse = WEAPONTYPE_MICROSMG
			ELIF iRand = 1	weaponToUse = WEAPONTYPE_PISTOL
			ENDIF
		BREAK
		
		CASE ciBACKUP_TYPE_FAMILIES
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			IF iRand = 0	weaponToUse = WEAPONTYPE_MICROSMG
			ELIF iRand = 1	weaponToUse = WEAPONTYPE_PISTOL
			ENDIF
		BREAK
		
		CASE ciBACKUP_TYPE_PROFESSIONALS
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			IF iRand = 0	weaponToUse = WEAPONTYPE_MICROSMG
			ELIF iRand = 1	weaponToUse = WEAPONTYPE_COMBATPISTOL
			ENDIF
		BREAK
		
		CASE ciBACKUP_TYPE_BALLAS
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			IF iRand = 0	weaponToUse = WEAPONTYPE_MICROSMG
			ELIF iRand = 1	weaponToUse = WEAPONTYPE_PISTOL
			ENDIF
		BREAK
		
		CASE ciBACKUP_TYPE_AIR_BUZZ_NOOSE
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			IF iRand = 0	weaponToUse = WEAPONTYPE_SMG
			ELIF iRand = 1	weaponToUse = WEAPONTYPE_DLC_SPECIALCARBINE
			ENDIF
		BREAK
		
		CASE ciBACKUP_TYPE_AIR_MAVERICK_COP
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			IF iRand = 0	weaponToUse = WEAPONTYPE_SMG
			ELIF iRand = 1	weaponToUse = WEAPONTYPE_DLC_SPECIALCARBINE
			ENDIF
		BREAK
		
		CASE ciBACKUP_TYPE_SALVA
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			IF iRand = 0	weaponToUse = WEAPONTYPE_MICROSMG
			ELIF iRand = 1	weaponToUse = WEAPONTYPE_DLC_SNSPISTOL
			ELIF iRand = 2	weaponToUse = WEAPONTYPE_PISTOL
			ENDIF
		BREAK
		
		CASE ciBACKUP_TYPE_ALTRUISTS
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			IF iRand = 0	weaponToUse = WEAPONTYPE_DLC_VINTAGEPISTOL
			ELIF iRand = 1	weaponToUse = WEAPONTYPE_PISTOL
			ENDIF
		BREAK
		
		CASE ciBACKUP_TYPE_FIB
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			IF iRand = 0	weaponToUse = WEAPONTYPE_DLC_HEAVYPISTOL
			ELIF iRand = 1	weaponToUse = WEAPONTYPE_PISTOL
			ENDIF
		BREAK
		
		CASE ciBACKUP_TYPE_ONEIL_BROS
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			IF iRand = 0	weaponToUse = WEAPONTYPE_MICROSMG
			ELIF iRand = 1	weaponToUse = WEAPONTYPE_PISTOL
			ELIF iRand = 2	weaponToUse = WEAPONTYPE_DLC_SNSPISTOL
			ENDIF
		BREAK
		
		CASE ciBACKUP_TYPE_KOREANS
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			IF iRand = 0	weaponToUse = WEAPONTYPE_MICROSMG
			ELIF iRand = 1	weaponToUse = WEAPONTYPE_PISTOL
			ENDIF
		BREAK
		
		CASE ciBACKUP_TYPE_KOREANS2
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			IF iRand = 0	weaponToUse = WEAPONTYPE_MICROSMG
			ELIF iRand = 1	weaponToUse = WEAPONTYPE_COMBATPISTOL
			ENDIF
		BREAK
		
		CASE ciBACKUP_TYPE_MERRYWEATHER_MESAS
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			IF iRand = 0	weaponToUse = WEAPONTYPE_MICROSMG
			ELIF iRand = 1	weaponToUse = WEAPONTYPE_DLC_HEAVYPISTOL
			ENDIF
		BREAK
		
		CASE ciBACKUP_TYPE_ARMY
		CASE ciBACKUP_TYPE_ARMY2
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			IF iRand = 0	weaponToUse = WEAPONTYPE_APPISTOL
			ELIF iRand = 1	weaponToUse = WEAPONTYPE_PISTOL
			ENDIF
		BREAK
		
		CASE ciBACKUP_TYPE_LOWRIDER_BALLAS
		CASE ciBACKUP_TYPE_LOWRIDER_VAGOS_BLA
		CASE ciBACKUP_TYPE_LOWRIDER_VAGOS_EMP
			weaponToUse = WEAPONTYPE_MICROSMG
		BREAK
		CASE ciBACKUP_TYPE_NIGHTSHARK_BOGDAN_GOON
		CASE ciBACKUP_TYPE_INSURGENT_BOGDAN_GOON
		CASE ciBACKUP_TYPE_INSURGENT2_BOGDAN_GOON
		CASE ciBACKUP_TYPE_AIR_BUZZ_AVON_GOON
		CASE ciBACKUP_TYPE_NIGHTSHARK_AVON_GOON
		CASE ciBACKUP_TYPE_DUBSTA3_AVON_GOON
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			IF iRand = 0	weaponToUse = WEAPONTYPE_MICROSMG
			ELIF iRand = 1	weaponToUse = WEAPONTYPE_PISTOL
			ENDIF
		BREAK
		
		CASE ciBACKUP_TYPE_KAMACHO_PROFESSIONALS
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			IF iRand = 0	weaponToUse = WEAPONTYPE_MICROSMG
			ELIF iRand = 1	weaponToUse = WEAPONTYPE_COMBATPISTOL
			ENDIF
		BREAK
		CASE ciBACKUP_TYPE_CONTENDER_PROFESSIONALS
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			IF iRand = 0	weaponToUse = WEAPONTYPE_MICROSMG
			ELIF iRand = 1	weaponToUse = WEAPONTYPE_COMBATPISTOL
			ENDIF
		BREAK
		
		CASE ciBACKUP_TYPE_CARACARA_HILLBILLY
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			IF iRand = 0	weaponToUse = WEAPONTYPE_MICROSMG
			ELIF iRand = 1	weaponToUse = WEAPONTYPE_PISTOL
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN weaponToUse
ENDFUNC

PROC GET_GANG_BACKUP_TYPE_DATA(INT backupType, BOOL bRandomWeapons, MODEL_NAMES &mnVehicleBackupModel, MODEL_NAMES &mnPedBackupModel, WEAPON_TYPE &wtWeapon, INT &iMaxpeds, BOOL bSoloBikesOnly)
	
	IF backupType = ciBACKUP_TYPE_LOST
		mnVehicleBackupModel = DAEMON
		mnPedBackupModel = G_M_Y_LOST_01
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_LOST)
		ELSE
			wtWeapon = WEAPONTYPE_PISTOL
		ENDIF
		INT inumpedchance = GET_RANDOM_INT_IN_RANGE(0,10)
		IF inumpedchance <=6 OR bSoloBikesOnly
			iMaxpeds = 1
		ELSE
			iMaxpeds = 2
		ENDIF
	ELIF backupType = ciBACKUP_TYPE_LOST_SLAMVAN2
		mnVehicleBackupModel = SLAMVAN2
		mnPedBackupModel = G_M_Y_LOST_01
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_LOST_SLAMVAN2)
		ELSE
			wtWeapon = WEAPONTYPE_PISTOL
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_VAGOS
		mnVehicleBackupModel = CAVALCADE2
		mnPedBackupModel = G_M_Y_MEXGOON_02
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_VAGOS)
		ELSE
			wtWeapon = WEAPONTYPE_PISTOL
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_FAMILIES
		mnVehicleBackupModel = BALLER
		mnPedBackupModel = G_M_Y_FAMCA_01
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_FAMILIES)
		ELSE
			wtWeapon = WEAPONTYPE_MICROSMG
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_PROFESSIONALS
		mnVehicleBackupModel = GRANGER
		mnPedBackupModel = MP_G_M_PROS_01
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_PROFESSIONALS)
		ELSE
			wtWeapon = WEAPONTYPE_MICROSMG
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_BALLAS
		mnVehicleBackupModel = Baller
		mnPedBackupModel = G_M_Y_BallaOrig_01
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_BALLAS)
		ELSE
			wtWeapon = WEAPONTYPE_MICROSMG
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_SALVA
		mnVehicleBackupModel = Emperor
		mnPedBackupModel = G_M_Y_SALVAGOON_01
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_SALVA)
		ELSE
			wtWeapon = WEAPONTYPE_MICROSMG
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_ALTRUISTS
		mnVehicleBackupModel = bodhi2
		mnPedBackupModel = A_M_O_ACult_02
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_ALTRUISTS)
		ELSE
			wtWeapon = WEAPONTYPE_MICROSMG
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_FIB	
		mnVehicleBackupModel = FBI
		mnPedBackupModel =s_m_m_fibsec_01
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_FIB)
		ELSE
			wtWeapon = WEAPONTYPE_PISTOL
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_ONEIL_BROS	
		mnVehicleBackupModel = Sandking
		mnPedBackupModel = A_M_M_Hillbilly_01 
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_ONEIL_BROS)
		ELSE
			wtWeapon = WEAPONTYPE_PISTOL
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_KOREANS	
		mnVehicleBackupModel = FUGITIVE
		mnPedBackupModel = G_M_Y_Korean_01
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_KOREANS)
		ELSE
			wtWeapon = WEAPONTYPE_PISTOL
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_KOREANS2
		mnVehicleBackupModel = FELTZER2
		mnPedBackupModel = G_M_Y_Korean_01
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_KOREANS2)
		ELSE
			wtWeapon = WEAPONTYPE_MICROSMG
		ENDIF
		iMaxpeds = 2			
	ELIF backupType = ciBACKUP_TYPE_MERRYWEATHER_MESAS
		mnVehicleBackupModel = MESA3
		mnPedBackupModel = S_M_Y_BLACKOPS_01
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_MERRYWEATHER_MESAS)
		ELSE
			wtWeapon = WEAPONTYPE_MICROSMG
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_ARMY
		mnVehicleBackupModel = MESA3
		mnPedBackupModel = S_M_Y_MARINE_03
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_ARMY)
		ELSE
			wtWeapon = WEAPONTYPE_PISTOL
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_ARMY2
		mnVehicleBackupModel = CRUSADER
		mnPedBackupModel = S_M_Y_MARINE_03
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_ARMY)
		ELSE
			wtWeapon = WEAPONTYPE_PISTOL
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_LOWRIDER_VAGOS_BLA
		mnVehicleBackupModel = BLADE //Place holder until lowrider vehicles are available.
		mnPedBackupModel = G_M_Y_MEXGOON_02
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_VAGOS)
		ELSE
			wtWeapon = WEAPONTYPE_PISTOL
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_LOWRIDER_VAGOS_EMP
		mnVehicleBackupModel = EMPEROR //Placeholder until lowrider vehicles are available.
		mnPedBackupModel = G_M_Y_MEXGOON_02
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_VAGOS)
		ELSE
			wtWeapon = WEAPONTYPE_PISTOL
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_LOWRIDER_BALLAS
		mnVehicleBackupModel = BUCCANEER //Placeholder until lowrider vehicles are available.
		mnPedBackupModel = G_M_Y_BallaOrig_01
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_BALLAS)
		ELSE
			wtWeapon = WEAPONTYPE_PISTOL
		ENDIF
		iMaxpeds = 2
	
	ELIF backupType = ciBACKUP_TYPE_MERRYWEATHER
		mnVehicleBackupModel = PATRIOT
		mnPedBackupModel = S_M_Y_BLACKOPS_01
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_MERRYWEATHER)
		ELSE
			wtWeapon = WEAPONTYPE_MICROSMG
		ENDIF
		iMaxpeds = 2
		
	ELIF backupType = ciBACKUP_TYPE_AIR_BUZZ_MERRYWEATHER
		mnVehicleBackupModel = BUZZARD
		mnPedBackupModel = S_M_Y_BLACKOPS_01
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_MERRYWEATHER)
		ELSE
			wtWeapon = WEAPONTYPE_MICROSMG
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_AIR_BUZZ_NOOSE
		mnVehicleBackupModel = BUZZARD2
		mnPedBackupModel = S_M_Y_Swat_01
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_AIR_BUZZ_NOOSE)
		ELSE
			wtWeapon = WEAPONTYPE_MICROSMG
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_AIR_BUZZ_PROFESSIONALS
		mnVehicleBackupModel = BUZZARD
		mnPedBackupModel = MP_G_M_PROS_01
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_PROFESSIONALS)
		ELSE
			wtWeapon = WEAPONTYPE_MICROSMG
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_AIR_MAVERICK_NOOSE
		mnVehicleBackupModel = POLMAV
		mnPedBackupModel = S_M_Y_Swat_01
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_AIR_BUZZ_NOOSE)
		ELSE
			wtWeapon = WEAPONTYPE_MICROSMG
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_AIR_MAVERICK_COP
		mnVehicleBackupModel = POLMAV
		mnPedBackupModel = S_M_Y_COP_01
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_AIR_MAVERICK_COP)
		ELSE
			wtWeapon = WEAPONTYPE_MICROSMG
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_AIR_FROGGER_MERRYWEATHER
		mnVehicleBackupModel = FROGGER
		mnPedBackupModel = S_M_Y_BLACKOPS_01
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_MERRYWEATHER)
		ELSE
			wtWeapon = WEAPONTYPE_MICROSMG
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_AIR_FROGGER_PROFESSIONALS
		mnVehicleBackupModel = FROGGER
		mnPedBackupModel = MP_G_M_PROS_01
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_PROFESSIONALS)
		ELSE
			wtWeapon = WEAPONTYPE_MICROSMG
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_AIR_FROGGER_LOST
		mnVehicleBackupModel = FROGGER
		mnPedBackupModel = G_M_Y_LOST_01
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_LOST)
		ELSE
			wtWeapon = WEAPONTYPE_PISTOL
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_AIR_FROGGER_VAGOS
		mnVehicleBackupModel = FROGGER
		mnPedBackupModel = G_M_Y_MEXGOON_02
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_VAGOS)
		ELSE
			wtWeapon = WEAPONTYPE_PISTOL
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_AIR_FROGGER_KOREAN
		mnVehicleBackupModel = FROGGER
		mnPedBackupModel = G_M_Y_Korean_01
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_KOREANS)
		ELSE
			wtWeapon = WEAPONTYPE_PISTOL
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_AIR_VALKYRIE_MERRYWEATHER
		mnVehicleBackupModel = VALKYRIE2
		mnPedBackupModel = S_M_Y_BLACKOPS_01
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_MERRYWEATHER)
		ELSE
			wtWeapon = WEAPONTYPE_MICROSMG
		ENDIF
		iMaxpeds = 4
	ELIF backupType = ciBACKUP_TYPE_AIR_VALKYRIE_PROFESSIONALS
		mnVehicleBackupModel = VALKYRIE2
		mnPedBackupModel = MP_G_M_PROS_01
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_PROFESSIONALS)
		ELSE
			wtWeapon = WEAPONTYPE_MICROSMG
		ENDIF
		iMaxpeds = 4
	ELIF backupType = ciBACKUP_TYPE_AIR_HUNTER_MERRYWEATHER
		mnVehicleBackupModel = HUNTER
		mnPedBackupModel = S_M_Y_BLACKOPS_01
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_MERRYWEATHER)
		ELSE
			wtWeapon = WEAPONTYPE_MICROSMG
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_AIR_HUNTER_PROFESSIONALS
		mnVehicleBackupModel = HUNTER
		mnPedBackupModel = MP_G_M_PROS_01
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_PROFESSIONALS)
		ELSE
			wtWeapon = WEAPONTYPE_MICROSMG
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_AIR_SAVAGE_MERRYWEATHER
		mnVehicleBackupModel = SAVAGE
		mnPedBackupModel = S_M_Y_BLACKOPS_01
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_MERRYWEATHER)
		ELSE
			wtWeapon = WEAPONTYPE_MICROSMG
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_AIR_SAVAGE_PROFESSIONALS
		mnVehicleBackupModel = SAVAGE
		mnPedBackupModel = MP_G_M_PROS_01
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_PROFESSIONALS)
		ELSE
			wtWeapon = WEAPONTYPE_MICROSMG
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_AIR_LAZER_MERRYWEATHER
		mnVehicleBackupModel = LAZER
		mnPedBackupModel = MP_G_M_PROS_01
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_PROFESSIONALS)
		ELSE
			wtWeapon = WEAPONTYPE_MICROSMG
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_AIR_MOLOTOK_MERRYWEATHER
		mnVehicleBackupModel = MOLOTOK
		mnPedBackupModel = MP_G_M_PROS_01
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_PROFESSIONALS)
		ELSE
			wtWeapon = WEAPONTYPE_MICROSMG
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_NIGHTSHARK_BOGDAN_GOON
		mnVehicleBackupModel = NIGHTSHARK
		mnPedBackupModel = INT_TO_ENUM(MODEL_NAMES, HASH("MP_M_BogdanGoon"))
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_NIGHTSHARK_BOGDAN_GOON)
		ELSE
			wtWeapon = WEAPONTYPE_MICROSMG
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_INSURGENT_BOGDAN_GOON
		mnVehicleBackupModel = INSURGENT
		mnPedBackupModel = INT_TO_ENUM(MODEL_NAMES, HASH("MP_M_BogdanGoon"))
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_INSURGENT_BOGDAN_GOON)
		ELSE
			wtWeapon = WEAPONTYPE_MICROSMG
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_INSURGENT2_BOGDAN_GOON
		mnVehicleBackupModel = INSURGENT2
		mnPedBackupModel = INT_TO_ENUM(MODEL_NAMES, HASH("MP_M_BogdanGoon"))
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_INSURGENT2_BOGDAN_GOON)
		ELSE
			wtWeapon = WEAPONTYPE_MICROSMG
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_AIR_BUZZ_AVON_GOON
		mnVehicleBackupModel = BUZZARD
		mnPedBackupModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_avongoon"))
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_AIR_BUZZ_AVON_GOON)
		ELSE
			wtWeapon = WEAPONTYPE_MICROSMG
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_NIGHTSHARK_AVON_GOON
		mnVehicleBackupModel = NIGHTSHARK
		mnPedBackupModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_avongoon"))
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_NIGHTSHARK_AVON_GOON)
		ELSE
			wtWeapon = WEAPONTYPE_MICROSMG
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_DUBSTA3_AVON_GOON
		mnVehicleBackupModel = DUBSTA3
		mnPedBackupModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_avongoon"))
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_DUBSTA3_AVON_GOON)
		ELSE
			wtWeapon = WEAPONTYPE_MICROSMG
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_KAMACHO_PROFESSIONALS
		mnVehicleBackupModel = KAMACHO
		mnPedBackupModel = MP_G_M_PROS_01
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_PROFESSIONALS)
		ELSE
			wtWeapon = WEAPONTYPE_MICROSMG
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_CONTENDER_PROFESSIONALS
		mnVehicleBackupModel = CONTENDER
		mnPedBackupModel = MP_G_M_PROS_01
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_PROFESSIONALS)
		ELSE
			wtWeapon = WEAPONTYPE_MICROSMG
		ENDIF
		iMaxpeds = 2
	ELIF backupType = ciBACKUP_TYPE_CARACARA_HILLBILLY
		mnVehicleBackupModel = CARACARA2
		mnPedBackupModel = INT_TO_ENUM(MODEL_NAMES, HASH("G_M_M_CasRN_01"))
		IF bRandomWeapons
			wtWeapon = GET_RANDOM_WEAPON_FOR_BACKUP_TYPE(ciBACKUP_TYPE_CARACARA_HILLBILLY)
		ELSE
			wtWeapon = WEAPONTYPE_MICROSMG
		ENDIF
		iMaxpeds = 2
	ENDIF
	// When adding a new backupType, please add it to IS_GANG_CHASE_AIR / IS_GANG_CHASE_SEA if it's an air chase or sea chase :)
	
ENDPROC

FUNC BOOL CREATE_BACKUP_UNIT(INT iGangBackupUnit, INT iteam, VECTOR vPosition, PED_INDEX tempPed, INT iSpawnPart)
	
	INT iped
	INT iMaxpeds
	MODEL_NAMES mnVehicleBackupModel
	MODEL_NAMES mnPedBackupModel
	VEHICLE_INDEX tempVeh
	WEAPON_TYPE wtWeapon
	VEHICLE_SEAT tempVehSeat
	FLOAT fBackupSpawnHeading
	INT iPedDecorInt 
	
	BOOL bTriedForwards = IS_BIT_SET(MC_PlayerBD[iSpawnPart].ibackupSpawnBitset, PBGANGSPAWN_UNIT_0_TRY_FORWARDS + iGangBackupUnit)
	
	IF MC_serverBD_4.iCurrentHighestPriority[iteam] < FMMC_MAX_RULES
		
		BOOL bRandomWeapons = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[MC_serverBD_4.iCurrentHighestPriority[iteam]], ciBS_RULE3_RANDOMISE_GANG_CHASE_WEAPONS)
		BOOL bSoloBikesOnly = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[MC_serverBD_4.iCurrentHighestPriority[iteam]], ciBS_RULE13_GANG_CHASE_SOLO_BIKES_ONLY)
		INT backupType = g_FMMC_STRUCT.sFMMCEndConditions[iteam].iGangBackupType[MC_serverBD_4.iCurrentHighestPriority[iteam]]
		
		GET_GANG_BACKUP_TYPE_DATA(backupType, bRandomWeapons, mnVehicleBackupModel, mnPedBackupModel, wtWeapon, iMaxpeds, bSoloBikesOnly)
		
		IF REQUEST_LOAD_MODEL(mnVehicleBackupModel)
		AND REQUEST_LOAD_MODEL(mnPedBackupModel)
		    IF CAN_RESERVE_NETWORK_VEHICLES_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_VEHICLES() + 1,TRUE,TRUE)
			AND CAN_RESERVE_NETWORK_PEDS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_PEDS() + iMaxpeds,TRUE,TRUE)
				IF NOT IS_BIT_SET(MC_serverBD_1.GangBackup[iGangBackupUnit].iBackupBitset, SBBBOOL_RESERVATION_COMPLETE)
					RESERVE_NETWORK_MISSION_VEHICLES(GET_NUM_RESERVED_MISSION_VEHICLES() + 1)
					RESERVE_NETWORK_MISSION_PEDS(GET_NUM_RESERVED_MISSION_PEDS() + iMaxpeds)
					
					MC_serverBD_1.GangBackup[iGangBackupUnit].iMaxPeds = iMaxPeds
					
					SET_BIT(MC_serverBD_1.GangBackup[iGangBackupUnit].iBackupBitset, SBBBOOL_RESERVATION_COMPLETE)
					PRINTLN("[RCC MISSION] [gang_backup] CREATE_BACKUP_UNIT - spamming reservation iGangBackupUnit = ", iGangBackupUnit, " GET_NUM_RESERVED_MISSION_VEHICLES = ", GET_NUM_RESERVED_MISSION_VEHICLES(), " GET_NUM_RESERVED_MISSION_PEDS = ", GET_NUM_RESERVED_MISSION_PEDS(), " iMaxpeds = ", iMaxpeds)
				ELSE
					IF CAN_REGISTER_MISSION_ENTITIES(iMaxpeds, 1, 0,  0)
						
						BOOL bSpawnForwards = FALSE
						
						IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupVeh)
							
							IF IS_BIT_SET(MC_PlayerBD[iSpawnPart].ibackupSpawnBitset, PBGANGSPAWN_UNIT_0_SUCCEED_FORWARDS + iGangBackupUnit)
								PRINTLN("[RCC MISSION] [gang_backup] BCK_SPN CREATE_BACKUP_UNIT - vehicle number ",iGangBackupUnit,", client says spawn forward")
								IF DOES_NODE_HAVE_SUITABLE_GANG_CHASE_HEADING(vPosition, GET_ENTITY_HEADING(tempPed), fBackupSpawnHeading, backupType, DEFAULT, 100.0)
									PRINTLN("[RCC MISSION] [gang_backup] BCK_SPN CREATE_BACKUP_UNIT - vehicle number ",iGangBackupUnit," spawn forward heading found as ",fBackupSpawnHeading)
									bSpawnForwards = TRUE
									SET_BIT(MC_serverBD_1.GangBackup[iGangBackupUnit].iBackupBitset, SBBBOOL_BACKUP_SPAWNING_FORWARDS)
								ENDIF
							ENDIF
							
							IF NOT bSpawnForwards
								IF bTriedForwards
									PRINTLN("[RCC MISSION] [gang_backup] BCK_SPN CREATE_BACKUP_UNIT - vehicle number ",iGangBackupUnit,", client says we tried to spawn it forwards")
									IF NOT DOES_NODE_HAVE_SUITABLE_GANG_CHASE_HEADING(vPosition, GET_HEADING_BETWEEN_VECTORS_2D(MC_PlayerBD[iSpawnPart].vBackupSpawnCoords,GET_ENTITY_COORDS(tempPed)), fBackupSpawnHeading, backupType, 90, 100.0)
										fBackupSpawnHeading = GET_HEADING_BETWEEN_VECTORS_2D(MC_PlayerBD[iSpawnPart].vBackupSpawnCoords,GET_ENTITY_COORDS(tempPed))
										PRINTLN("[RCC MISSION] [gang_backup] BCK_SPN CREATE_BACKUP_UNIT - no suitable road node heading found for vehicle number ",iGangBackupUnit,", use normal heading ",fBackupSpawnHeading)
									#IF IS_DEBUG_BUILD
									ELSE
										PRINTLN("[RCC MISSION] [gang_backup] BCK_SPN CREATE_BACKUP_UNIT - spawning vehicle number ",iGangBackupUnit," along road node heading found as ",fBackupSpawnHeading)
									#ENDIF
									ENDIF
								ENDIF
								CLEAR_BIT(MC_serverBD_1.GangBackup[iGangBackupUnit].iBackupBitset, SBBBOOL_BACKUP_SPAWNING_FORWARDS)
							ENDIF
							
							IF NOT bTriedForwards
								fBackupSpawnHeading = GET_HEADING_BETWEEN_VECTORS_2D(MC_PlayerBD[iSpawnPart].vBackupSpawnCoords,GET_ENTITY_COORDS(tempPed))
								CLEAR_BIT(MC_serverBD_1.GangBackup[iGangBackupUnit].iBackupBitset, SBBBOOL_BACKUP_SPAWNING_FORWARDS)
								PRINTLN("[RCC MISSION] [gang_backup] BCK_SPN CREATE_BACKUP_UNIT - vehicle number: ", iGangBackupUnit," using backwards heading: ",fBackupSpawnHeading)
							ENDIF
							
							IF CREATE_NET_VEHICLE(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupVeh,mnVehicleBackupModel,vPosition,fBackupSpawnHeading)
								
								tempVeh = NET_TO_VEH(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupVeh)
								
								SET_UP_FMMC_BACKUP_VEH_RC(tempVeh, iteam)
								
								NETWORK_FADE_IN_ENTITY(tempVeh, TRUE, FALSE)
								
								IF backupType = ciBACKUP_TYPE_KOREANS2
									SET_VEHICLE_MOD_KIT(tempVeh, 0)
									TOGGLE_VEHICLE_MOD(tempVeh, MOD_TOGGLE_TURBO, TRUE)
									PRINTLN("[RCC MISSION] [gang_backup] BCK_SPN CREATE_BACKUP_UNIT - vehicle number: ", iGangBackupUnit," adding turbo ")
								ENDIF
								
								IF backupType = ciBACKUP_TYPE_DUBSTA3_AVON_GOON
									PRINTLN("[RCC MISSION] [gang_backup] Set mod variation for ciBACKUP_TYPE_DUBSTA3_AVON_GOON")
									FMMC_SET_VEH_MOD_PRESET(tempVeh, DUBSTA3, 0)
								ELIF backupType = ciBACKUP_TYPE_KAMACHO_PROFESSIONALS
									PRINTLN("[RCC MISSION] [gang_backup] Set mod variation for ciBACKUP_TYPE_KAMACHO_PROFESSIONALS")
									FMMC_SET_VEH_MOD_PRESET(tempVeh, KAMACHO, 0)
								ENDIF
								
								IF IS_GANG_CHASE_AIR(backupType)
									PRINTLN("[RCC MISSION][gang_backup] BCK_SPN CREATE_BACKUP_UNIT - Unit ",iGangBackupUnit," is an air vehicle, set the heli blades at full speed, freeze position and set SBBBOOL_BACKUP_AIR_VEHICLE_IS_FROZEN")
									SET_HELI_BLADES_FULL_SPEED(tempVeh)
									FREEZE_ENTITY_POSITION(tempVeh, TRUE)
									SET_BIT(MC_serverBD_1.GangBackup[iGangBackupUnit].iBackupBitset, SBBBOOL_BACKUP_AIR_VEHICLE_IS_FROZEN)
								ENDIF
								
								MC_serverBD_1.GangBackup[iGangBackupUnit].iBackupType = backupType
								
								IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iRuleBitsetEleven[MC_serverBD_4.iCurrentHighestPriority[iteam]], ciBS_RULE11_GANG_CHASE_DESPAWN_ON_NEW_GANG_CHASE_TYPE)
									PRINTLN("[RCC MISSION] [gang_backup] BCK_SPN CREATE_BACKUP_UNIT - vehicle number: ", iGangBackupUnit," set bit SBBBOOL_BACKUP_DESPAWN_ON_NEW_TYPE")
									SET_BIT(MC_serverBD_1.GangBackup[iGangBackupUnit].iBackupBitset, SBBBOOL_BACKUP_DESPAWN_ON_NEW_TYPE)
								ENDIF
								
								PRINTLN("[RCC MISSION] [gang_backup] BCK_SPN CREATE_BACKUP_UNIT - vehicle number: ", iGangBackupUnit," created at: ", vPosition, " vehicle model = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mnVehicleBackupModel))
							ENDIF
							
						ELSE
							
							tempVeh = NET_TO_VEH(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupVeh)
							
							IF IS_BIT_SET(MC_serverBD_1.GangBackup[iGangBackupUnit].iBackupBitset, SBBBOOL_BACKUP_AIR_VEHICLE_IS_FROZEN)
							AND (NOT IS_ENTITY_DEAD(tempVeh))
							AND IS_VEHICLE_DRIVEABLE(tempVeh)
								IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
									SET_HELI_BLADES_FULL_SPEED(tempVeh)
								ELSE
									PRINTLN("[RCC MISSION] [gang_backup] BCK_SPN CREATE_BACKUP_UNIT - vehicle number: ", iGangBackupUnit," don't have control to keep the blades going, request control")
									NETWORK_REQUEST_CONTROL_OF_ENTITY(tempVeh)
								ENDIF
							ENDIF
							
							IF g_FMMC_STRUCT.sFMMCEndConditions[iteam].iGangChaseForwardSpawnChance[MC_serverBD_4.iCurrentHighestPriority[iteam]] > 0
							AND IS_BIT_SET(MC_serverBD_1.GangBackup[iGangBackupUnit].iBackupBitset, SBBBOOL_BACKUP_SPAWNING_FORWARDS)
								bSpawnForwards = TRUE
							ENDIF
							
							AI_BLIP_STRUCT emptyAIBlipStruct
							
							FOR iped = 0 TO (iMaxpeds-1)
								IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupPed[iped])
									IF IS_VEHICLE_DRIVEABLE(tempVeh)
										IF ARE_ANY_VEHICLE_SEATS_FREE(tempVeh)
											IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupVeh)
												tempVehSeat =GET_FIRST_FREE_VEHICLE_SEAT_RC(tempVeh)
												IF CREATE_NET_PED_IN_VEHICLE(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupPed[iped],MC_serverBD_1.GangBackup[iGangBackupUnit].BackupVeh,PEDTYPE_MISSION,mnPedBackupModel,tempVehSeat)
													
													//Make sure the AI blip handles are blanked for this index.
													COPY_AI_BLIP_STRUCT_TO_STRUCT(emptyAIBlipStruct, biChasePedBlip[iped])
													
													// Set a debug name so it's easy to identify specific peds using AI debug output.
													#IF IS_DEBUG_BUILD
														PED_INDEX tempPedIndex = NET_TO_PED(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupPed[iped])
													
														TEXT_LABEL_31 tl31DebugName = "Gang["
														tl31DebugName += iGangBackupUnit
														tl31DebugName += "]["
														tl31DebugName += iped
														tl31DebugName += "]"
														SET_PED_NAME_DEBUG(tempPedIndex, tl31DebugName)
													#ENDIF
													
													IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE8_GANG_CHASE_GET_COP_BLIPS)
														PRINTLN("[RCC MISSION] [gang_backup] BCK_SPN CREATE_BACKUP_UNIT - Unit uses cop blips")
														SET_BIT(MC_serverBD_1.GangBackup[iGangBackupUnit].iBackupBitset, SBBOOL_BACKUP_USES_COP_BLIPS)
													ENDIF
													
													// 2206938 - LOD distance issue
													IF g_FMMC_STRUCT.sFMMCEndConditions[iteam].iGangBackupType[MC_serverBD_4.iCurrentHighestPriority[iteam]] = ciBACKUP_TYPE_LOST
													AND IS_THIS_MODEL_A_BIKE( GET_ENTITY_MODEL( tempVeh ) )
														SET_PED_LOD_MULTIPLIER( NET_TO_PED( MC_serverBD_1.GangBackup[iGangBackupUnit].BackupPed[iped] ), 2.0 )
														PRINTLN("[RCC MISSION] [gang_backup] BCK_SPN CREATE_BACKUP_UNIT - Ped is on a bike, ped lod multiplier set to 2.0")
													ENDIF
													
													//3954148: prevent gang members from randomly jumping out of Valkyrie helicopters
													IF backupType = ciBACKUP_TYPE_AIR_VALKYRIE_MERRYWEATHER
													OR backupType = ciBACKUP_TYPE_AIR_VALKYRIE_PROFESSIONALS
														PED_INDEX piThisGangMember = NET_TO_PED( MC_serverBD_1.GangBackup[iGangBackupUnit].BackupPed[iped])
														SET_PED_COMBAT_ATTRIBUTES(piThisGangMember, CA_LEAVE_VEHICLES, FALSE)
														PRINTLN("[RCC MISSION] [gang_backup] BCK_SPN CREATE_BACKUP_UNIT - preventing gang ped from jumping out of Valkyrie")
													ENDIF
													
													IF IS_BIT_SET(MC_serverBD_1.GangBackup[iGangBackupUnit].iBackupBitset, SBBBOOL_BACKUP_AIR_VEHICLE_IS_FROZEN)
														PRINTLN("[RCC MISSION][gang_backup] BCK_SPN CREATE_BACKUP_UNIT - Spawned first ped in veh ",iGangBackupUnit," & it's an air vehicle, unfreeze & clear SBBBOOL_BACKUP_AIR_VEHICLE_IS_FROZEN")
														FREEZE_ENTITY_POSITION(tempVeh, FALSE)
														CLEAR_BIT(MC_serverBD_1.GangBackup[iGangBackupUnit].iBackupBitset, SBBBOOL_BACKUP_AIR_VEHICLE_IS_FROZEN)
													ENDIF
													
													MC_serverBD.iNumPedsSpawned++
													iPedDecorInt = (4*iGangBackupUnit+iped)
													SET_UP_FMMC_BACKUP_PED_RC(NET_TO_PED(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupPed[iped]), iPedDecorInt, MC_serverBD.iTotalNumStartingPlayers, wtWeapon, iteam, iGangBackupUnit, tempPed, bSpawnForwards)
													PRINTLN("[RCC MISSION] [gang_backup] BCK_SPN CREATE_BACKUP_UNIT - vehicle number: ", iGangBackupUnit," ped create in veh ")
													
												ENDIF
											ELSE
												PRINTLN("[RCC MISSION] [gang_backup] BCK_SPN CREATE_BACKUP_UNIT - vehicle number: ", iGangBackupUnit," DON'T HAVE CONTROL!! REQUESTING CONTROL OF NET ID...")
												NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupVeh)
											ENDIF
										ELSE
											IF CREATE_NET_PED(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupPed[iped],PEDTYPE_MISSION,mnPedBackupModel,vPosition,GET_ENTITY_HEADING(tempVeh))
												
												//Make sure the AI blip handles are blanked for this index.
												COPY_AI_BLIP_STRUCT_TO_STRUCT(emptyAIBlipStruct, biChasePedBlip[iped])
												
												// Set a debug name so it's easy to identify specific peds using AI debug output.
												#IF IS_DEBUG_BUILD
													PED_INDEX tempPedIndex = NET_TO_PED(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupPed[iped])
												
													TEXT_LABEL_31 tl31DebugName = "Gang["
													tl31DebugName += iGangBackupUnit
													tl31DebugName += "]["
													tl31DebugName += iped
													tl31DebugName += "]"
													SET_PED_NAME_DEBUG(tempPedIndex, tl31DebugName)
												#ENDIF
												
												IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE8_GANG_CHASE_GET_COP_BLIPS)
													PRINTLN("[RCC MISSION] [gang_backup] BCK_SPN CREATE_BACKUP_UNIT - Unit uses cop blips")
													SET_BIT(MC_serverBD_1.GangBackup[iGangBackupUnit].iBackupBitset, SBBOOL_BACKUP_USES_COP_BLIPS)
												ENDIF
												
												MC_serverBD.iNumPedsSpawned++
												iPedDecorInt = (4*iGangBackupUnit+iped)
												SET_UP_FMMC_BACKUP_PED_RC(NET_TO_PED(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupPed[iped]), iPedDecorInt, MC_serverBD.iTotalNumStartingPlayers, wtWeapon, iteam, iGangBackupUnit, tempPed, bSpawnForwards)
												PRINTLN("[RCC MISSION] [gang_backup] BCK_SPN CREATE_BACKUP_UNIT - vehicle number: ", iGangBackupUnit," ped create on foot, seats full ")
											ENDIF
										ENDIF
									ELSE
										IF CREATE_NET_PED(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupPed[iped],PEDTYPE_MISSION,mnPedBackupModel,vPosition,GET_ENTITY_HEADING(tempVeh))
											
											//Make sure the AI blip handles are blanked for this index.
											COPY_AI_BLIP_STRUCT_TO_STRUCT(emptyAIBlipStruct, biChasePedBlip[iped])
											
											// Set a debug name so it's easy to identify specific peds using AI debug output.
											#IF IS_DEBUG_BUILD
												PED_INDEX tempPedIndex = NET_TO_PED(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupPed[iped])
											
												TEXT_LABEL_31 tl31DebugName = "Gang["
												tl31DebugName += iGangBackupUnit
												tl31DebugName += "]["
												tl31DebugName += iped
												tl31DebugName += "]"
												SET_PED_NAME_DEBUG(tempPedIndex, tl31DebugName)
											#ENDIF
											
											IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE8_GANG_CHASE_GET_COP_BLIPS)
												PRINTLN("[RCC MISSION] [gang_backup] BCK_SPN CREATE_BACKUP_UNIT - Unit uses cop blips")
												SET_BIT(MC_serverBD_1.GangBackup[iGangBackupUnit].iBackupBitset, SBBOOL_BACKUP_USES_COP_BLIPS)
											ENDIF
											
											MC_serverBD.iNumPedsSpawned++
											iPedDecorInt = (4*iGangBackupUnit+iped)
											SET_UP_FMMC_BACKUP_PED_RC(NET_TO_PED(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupPed[iped]), iPedDecorInt, MC_serverBD.iTotalNumStartingPlayers, wtWeapon, iteam, iGangBackupUnit, tempPed, bSpawnForwards)
											PRINTLN("[RCC MISSION] [gang_backup] BCK_SPN CREATE_BACKUP_UNIT - vehicle number: ", iGangBackupUnit," ped create on foot, vehicle broken ")
										ENDIF
									ENDIF
								ELSE 
									PRINTLN("[RCC MISSION] [gang_backup] BCK_SPN CREATE_BACKUP_UNIT - vehicle number: ", iGangBackupUnit," ped already exists: ",iped)
								ENDIF
							ENDFOR
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupVeh)
		RETURN FALSE
	ENDIF
	FOR iped = 0 TO (iMaxpeds-1)
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupPed[iped])
			RETURN FALSE
		ENDIF
	ENDFOR
	
	IF IS_BIT_SET(MC_serverBD_1.GangBackup[iGangBackupUnit].iBackupBitset, SBBBOOL_BACKUP_AIR_VEHICLE_IS_FROZEN)
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupVeh)
			CASSERTLN(DEBUG_CONTROLLER, "[RCC MISSION][gang_backup] BCK_SPN CREATE_BACKUP_UNIT - Finished spawning for unit ",iGangBackupUnit," but SBBBOOL_BACKUP_AIR_VEHICLE_IS_FROZEN is still set - unfreeze the entity")
			PRINTLN("[RCC MISSION][gang_backup] BCK_SPN CREATE_BACKUP_UNIT - Finished spawning for unit ",iGangBackupUnit," but SBBBOOL_BACKUP_AIR_VEHICLE_IS_FROZEN is still set - unfreeze the entity")
			FREEZE_ENTITY_POSITION(NET_TO_VEH(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupVeh), FALSE)
			CLEAR_BIT(MC_serverBD_1.GangBackup[iGangBackupUnit].iBackupBitset, SBBBOOL_BACKUP_AIR_VEHICLE_IS_FROZEN)
		ELSE
			PRINTLN("[RCC MISSION] [gang_backup] BCK_SPN CREATE_BACKUP_UNIT - vehicle number: ", iGangBackupUnit," need control to unfreeze after entity spawning, request control")
			NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD_1.GangBackup[iGangBackupUnit].BackupVeh)
			RETURN FALSE
		ENDIF
	ENDIF
	
	SET_MODEL_AS_NO_LONGER_NEEDED(mnVehicleBackupModel)
	SET_MODEL_AS_NO_LONGER_NEEDED(mnPedBackupModel)
	
	PRINTLN("[RCC MISSION] [gang_backup] BCK_SPN CREATE_BACKUP_UNIT - Backup unit: ",iGangBackupUnit," spawned at: ",vPosition," with heading: ",fBackupSpawnHeading)
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL CHASE_BACKUP_SPAWNING(INT iGangBackupUnit)

PED_INDEX tempPed
PARTICIPANT_INDEX temppart
PLAYER_INDEX tempPlayer

	IF NOT SHOULD_ALLOW_BACKUP_SPAWN(iGangBackupUnit)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(MC_serverBD_1.GangBackup[iGangBackupUnit].iBackupBitset, SBBBOOL_BACKUP_CREATED)
	AND (MC_serverBD_1.iCurrentBackupSpawn = -1 OR MC_serverBD_1.iCurrentBackupSpawn = iGangBackupUnit)
		IF MC_serverBD_1.GangBackup[iGangBackupUnit].iTargetPart != ciBACKUP_TARGET_NONE
			
			BOOL bReject
			
			IF SHOULD_CHASE_BACKUP_SPAWN_FOR_TEAM(MC_serverBD_1.GangBackup[iGangBackupUnit].iBackSpawnTeam)
				
				IF MC_serverBD_1.GangBackup[iGangBackupUnit].iTargetPart >= 0
					
					temppart = INT_TO_PARTICIPANTINDEX(MC_serverBD_1.GangBackup[iGangBackupUnit].iTargetPart)
					IF NETWORK_IS_PARTICIPANT_ACTIVE(temppart)
						tempPlayer = NETWORK_GET_PLAYER_INDEX(temppart)
						IF IS_NET_PLAYER_OK(tempPlayer)
							tempPed = GET_PLAYER_PED(tempPlayer)
							MC_serverBD_1.iCurrentBackupSpawn = iGangBackupUnit
							IF NOT IS_VECTOR_ZERO(MC_PlayerBD[MC_serverBD_1.GangBackup[iGangBackupUnit].iTargetPart].vBackupSpawnCoords)
							AND IS_BIT_SET(MC_PlayerBD[MC_serverBD_1.GangBackup[iGangBackupUnit].iTargetPart].ibackupSpawnBitset,iGangBackupUnit)
								IF CREATE_BACKUP_UNIT(iGangBackupUnit,MC_playerBD[MC_serverBD_1.GangBackup[iGangBackupUnit].iTargetPart].iteam,MC_PlayerBD[MC_serverBD_1.GangBackup[iGangBackupUnit].iTargetPart].vBackupSpawnCoords,tempPed,MC_serverBD_1.GangBackup[iGangBackupUnit].iTargetPart)
									SET_BIT(MC_serverBD_1.GangBackup[iGangBackupUnit].iBackupBitset, SBBBOOL_BACKUP_CREATED)
									MC_serverBD_1.iCurrentBackupSpawn = -1
									vBackupSpawnDirection = <<0.0,0.0,0.0>>
									MC_serverBD_1.iNumBackupChasing[MC_playerBD[MC_serverBD_1.GangBackup[iGangBackupUnit].iTargetPart].iteam]++
									REINIT_NET_TIMER(timeBetweenTeamBackup[MC_playerBD[MC_serverBD_1.GangBackup[iGangBackupUnit].iTargetPart].iteam])
									IF MC_serverBD_4.iCurrentHighestPriority[MC_serverBD_1.GangBackup[iGangBackupUnit].iBackSpawnTeam] < FMMC_MAX_RULES
										MC_serverBD_1.iNumBackupSpawned[MC_playerBD[MC_serverBD_1.GangBackup[iGangBackupUnit].iTargetPart].iteam][MC_serverBD_4.iCurrentHighestPriority[MC_serverBD_1.GangBackup[iGangBackupUnit].iBackSpawnTeam]]++
									ENDIF
									RETURN TRUE
								ENDIF
							ELSE
								PRINTLN("[RCC MISSION] [gang_backup] BCK_SPN CHASE_BACKUP_SPAWNING, unit ",iGangBackupUnit," - waiting for spawn coords for backup: ",iGangBackupUnit," from part ",MC_serverBD_1.GangBackup[iGangBackupUnit].iTargetPart)
							ENDIF
						ENDIF
					ENDIF
					
				ELIF MC_serverBD_1.GangBackup[iGangBackupUnit].iTargetPart = ciBACKUP_TARGET_AVENGER_DUMMY_PED
					// This ped spawns on the dummy ped in the avenger!
					IF MC_serverBD_1.iBackupTargetAvenger != -1
					AND NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[MC_serverBD_1.iBackupTargetAvenger])
						PED_INDEX dummyPed = GET_AVENGER_DUMMY_PED(MC_serverBD_1.iBackupTargetAvenger)
						
						IF DOES_ENTITY_EXIST(dummyPed)
							
							MC_serverBD_1.iCurrentBackupSpawn = iGangBackupUnit
							
							IF FIND_BACKUP_SPAWN_COORDS(TRUE)
								IF CREATE_BACKUP_UNIT(iGangBackupUnit, MC_serverBD_1.GangBackup[iGangBackupUnit].iBackSpawnTeam, MC_PlayerBD[iLocalPart].vBackupSpawnCoords, dummyPed, iLocalPart)
									SET_BIT(MC_serverBD_1.GangBackup[iGangBackupUnit].iBackupBitset, SBBBOOL_BACKUP_CREATED)
									MC_serverBD_1.iCurrentBackupSpawn = -1
									vBackupSpawnDirection = <<0.0,0.0,0.0>>
									MC_serverBD_1.iNumBackupChasing[MC_serverBD_1.GangBackup[iGangBackupUnit].iBackSpawnTeam]++
									REINIT_NET_TIMER(timeBetweenTeamBackup[MC_serverBD_1.GangBackup[iGangBackupUnit].iBackSpawnTeam])
									IF MC_serverBD_4.iCurrentHighestPriority[MC_serverBD_1.GangBackup[iGangBackupUnit].iBackSpawnTeam] < FMMC_MAX_RULES
										MC_serverBD_1.iNumBackupSpawned[MC_serverBD_1.GangBackup[iGangBackupUnit].iBackSpawnTeam][MC_serverBD_4.iCurrentHighestPriority[MC_serverBD_1.GangBackup[iGangBackupUnit].iBackSpawnTeam]]++
									ENDIF
									RETURN TRUE
								ENDIF
							ENDIF
							
						ELSE
							bReject = TRUE
						ENDIF
					ELSE
						bReject = TRUE
					ENDIF
				ENDIF
			ELSE
				bReject = TRUE
			ENDIF
			
			IF bReject
				//These backup peds shouldn't spawn any more, reset the data:
				MC_serverBD_1.iCurrentBackupSpawn = -1
				vBackupSpawnDirection = <<0.0,0.0,0.0>>
				REINIT_NET_TIMER(tdbackuptargettimer[iGangBackupUnit])
				MC_serverBD_1.GangBackup[iGangBackupUnit].iBackSpawnTeam = -1
				MC_serverBD_1.GangBackup[iGangBackupUnit].iGangBackupProgress = 0
				
				IF MC_serverBD_1.GangBackup[iGangBackupUnit].iTargetPart = ciBACKUP_TARGET_AVENGER_DUMMY_PED
					RESET_BACKUP_SPAWN_VARIABLES(iGangBackupUnit)
				ENDIF
				
				PRINTLN("[RCC MISSION] [gang_backup] CHASE_BACKUP_SPAWNING, BCK_SPN unit ",iGangBackupUnit," - moving spawn progress back to 0: ",iGangBackupUnit)
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				IF bWdGangChaseDebug
					PRINTLN("[RCC MISSION] [gang_backup] CHASE_BACKUP_SPAWNING, unit ",iGangBackupUnit," - returning FALSE: iTargetPart = -1")
				ENDIF
			#ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			IF bWdGangChaseDebug
				PRINTLN("[RCC MISSION] [gang_backup] CHASE_BACKUP_SPAWNING, unit ",iGangBackupUnit," - returning FALSE: SBBOOL_BACKUP_CREATED = ",IS_BIT_SET(MC_serverBD_1.GangBackup[iGangBackupUnit].iBackupBitset, SBBBOOL_BACKUP_CREATED),", iCurrentBackupSpawn = ",MC_serverBD_1.iCurrentBackupSpawn)
			ENDIF
		#ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC
