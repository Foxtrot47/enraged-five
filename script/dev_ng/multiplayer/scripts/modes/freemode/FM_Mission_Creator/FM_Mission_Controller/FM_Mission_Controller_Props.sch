
USING "FM_Mission_Controller_USING.sch"
USING "FM_Mission_Controller_Utility.sch"
USING "FM_Mission_Controller_Entities.sch"
USING "FM_Mission_Controller_ObjectiveCompletion.sch"
USING "FM_Mission_Controller_SpecialCase.sch"
USING "FM_Mission_Controller_Rules.sch"
USING "FM_Mission_Controller_Doors.sch"
USING "FM_Mission_Controller_Cutscene.sch"
USING "FM_Mission_Controller_Debug.sch"
USING "FM_Mission_Controller_Locations.sch"
USING "FM_Mission_Controller_Vehicles.sch"
USING "FM_Mission_Controller_Peds_Brain.sch"
USING "FM_Mission_Controller_Peds_Body.sch"
USING "FM_Mission_Controller_Peds.sch"
USING "FM_Mission_Controller_GangChase.sch"
USING "FM_Mission_Controller_Minigame.sch"
USING "FM_Mission_Controller_Objects.sch"



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: PROPS !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************



// Note(Owain):
//
// Props are all entirely local, so are made by each machine when required
// and otherwise behave like other entities. They have rules on which they spawn on
// (if not set, then at the start), and rules on which they are cleaned up on
// Some have particular behaviours that are dealt with in PROCESS_PROP_BEHAVIOUR



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: MONKEYS IN CRATES (used in The Humane Labs - Deliver EMP heist prep mission) !
//
//************************************************************************************************************************************************************



func bool is_monkey_antagonised(ped_index monkey)
	
	if does_entity_exist(monkey)
		if not is_ped_injured(monkey)
			
			if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(monkey)) < 22 
				if is_ped_shooting(player_ped_id())
					return true 
				endif 
			endif 
		
			if is_bullet_in_area(get_entity_coords(monkey), 7.0)
			or is_sniper_bullet_in_area(get_offset_from_entity_in_world_coords(monkey, <<-3.5, 3.5, 3.5>>), get_offset_from_entity_in_world_coords(monkey, <<3.5, -3.5, -3.5>>))
				return true		
			endif 
		endif 
		
	endif 
	
	return false 
	
endfunc

func bool has_any_monkey_been_antagonised(ped_index &monkey[], ped_index &returned_antagonised_monkey)

	int i

	for i = 0 to count_of(monkey) - 1
		if is_monkey_antagonised(monkey[i])
			returned_antagonised_monkey = monkey[i]
		
			return true 
		endif 
	endfor 
			
	return false

endfunc

proc monkey_sound_system()

	ped_index antagonised_monkey //cache the potential antagonised monkey
	
	if not HAS_NET_TIMER_STARTED(monkey_time)
		START_NET_TIMER(monkey_time)
		
		PRINTLN("[RCC MISSION] monkey sound set net timer")
	endif 

	if has_sound_finished(monkey_sound)
		if has_any_monkey_been_antagonised(monkey_ped, antagonised_monkey)
		or (HAS_NET_TIMER_STARTED(monkey_time) and (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(monkey_time) > 5000))

			ped_index monkey_array[ciMAX_MONKEYPEDS]
			int monkey_index = 0
			int i = 0

			for i = 0 to count_of(monkey_ped) - 1
				if not is_ped_injured(monkey_ped[i])
					monkey_array[monkey_index] = monkey_ped[i]
					monkey_index++ //total number of monkeys alive
				endif 
			endfor 

			if monkey_index > 0 
				if does_entity_exist(antagonised_monkey)
					monkey_sound_ped = antagonised_monkey //play sound from specific antagonised monkey
				else 
					monkey_sound_ped = monkey_array[get_random_int_in_range(0, monkey_index)] //play sound on random monkey
				endif 

				switch get_random_int_in_range(0, 3)
					case 0
						IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_SLASHERS(g_FMMC_STRUCT.iAdversaryModeType)
							PLAY_SOUND_FROM_ENTITY(monkey_sound, "Screech", monkey_sound_ped, "DLC_HEIST_BIOLAB_MONKEYS_SOUNDS") 
						ENDIF
					break
					
					case 1
						PLAY_SOUND_FROM_ENTITY(monkey_sound, "General_Chatter", monkey_sound_ped, "DLC_HEIST_BIOLAB_MONKEYS_SOUNDS") 
					break 
					
					case 2
						PLAY_SOUND_FROM_ENTITY(monkey_sound, "Cage_Rattle", monkey_sound_ped, "DLC_HEIST_BIOLAB_MONKEYS_SOUNDS") 
					break 

				endswitch 
				
				PRINTLN("[RCC MISSION] monkey sound play sound")
			endif 
			
			reset_net_timer(monkey_time)
		endif 
		
	else 
		//cleanup the sound if the monkey dies
		if does_entity_exist(monkey_sound_ped)
			if is_ped_injured(monkey_sound_ped)
				stop_sound(monkey_sound)
				
				PRINTLN("[RCC MISSION] monkey sound stop sound")
			endif 
		endif  
	endif 

endproc  

proc monkey_anim_system()

	int i = 0
	float anim_start_phase

	for i = 0 to count_of(monkey_ped) - 1
		if not is_ped_injured(monkey_ped[i])
			if not is_ped_ragdoll(monkey_ped[i])
			
				switch monkey_anim_status[i]
					case play_normal_anims 
					
						anim_start_phase = (get_random_int_in_range(0, 3) / 10.0)

						switch i
							case 0
							case 3
							case 6
								task_play_anim(monkey_ped[i], "missfbi5ig_30monkeys", "monkey_a_idle", normal_blend_in, normal_blend_out, -1, af_looping, anim_start_phase) 	
							break 
							
							case 1
							case 4
							case 7
								task_play_anim(monkey_ped[i], "missfbi5ig_30monkeys", "monkey_b_idle", normal_blend_in, normal_blend_out, -1, af_looping, anim_start_phase) 	
							break 
							
							case 2
							case 5
							case 8
								task_play_anim(monkey_ped[i], "missfbi5ig_30monkeys", "monkey_c_idle", normal_blend_in, normal_blend_out, -1, af_looping, anim_start_phase)
							break 
						endswitch 
						
						monkey_anim_status[i] = play_freaked_out_anims
					break 
					
					case play_freaked_out_anims
						if is_monkey_antagonised(monkey_ped[i])
							switch i
								case 0
								case 3
								case 6
									if is_entity_playing_anim(monkey_ped[i], "missfbi5ig_30monkeys", "monkey_a_idle")
										open_sequence_task(monkey_seq)
											task_play_anim(null, "missfbi5ig_30monkeys", "monkey_a_freakout_in", normal_blend_in, normal_blend_out, -1) 
											task_play_anim(null, "missfbi5ig_30monkeys", "monkey_a_freakout_loop", instant_blend_in, normal_blend_out, -1, af_looping) //5000
										close_sequence_task(monkey_seq)
										task_perform_sequence(monkey_ped[i], monkey_seq)
										clear_sequence_task(monkey_seq)
									endif
								break 
								
								case 1
								case 4
								case 7
									if is_entity_playing_anim(monkey_ped[i], "missfbi5ig_30monkeys", "monkey_b_idle")
										open_sequence_task(monkey_seq)
											task_play_anim(null, "missfbi5ig_30monkeys", "monkey_b_freakout_in", normal_blend_in, normal_blend_out, -1) 
											task_play_anim(null, "missfbi5ig_30monkeys", "monkey_b_freakout_loop", instant_blend_in, normal_blend_out, -1, af_looping) 
										close_sequence_task(monkey_seq)
										task_perform_sequence(monkey_ped[i], monkey_seq)
										clear_sequence_task(monkey_seq)
									endif 
								break 
								
								case 2
								case 5
								case 8
									if is_entity_playing_anim(monkey_ped[i], "missfbi5ig_30monkeys", "monkey_c_idle")
										open_sequence_task(monkey_seq)
											task_play_anim(null, "missfbi5ig_30monkeys", "monkey_c_freakout_in", normal_blend_in, normal_blend_out, -1) 
											task_play_anim(null, "missfbi5ig_30monkeys", "monkey_c_freakout_loop", instant_blend_in, normal_blend_out, -1, af_looping) 
										close_sequence_task(monkey_seq)
										task_perform_sequence(monkey_ped[i], monkey_seq)
										clear_sequence_task(monkey_seq)
									endif 
								break 
							endswitch 
							
							monkey_anim_status[i] = blend_back_to_normal_anims
						endif 
					break 
					
					case blend_back_to_normal_anims
						if not is_monkey_antagonised(monkey_ped[i])
						
							switch i 
							
								case 0
								case 3
								case 6
									if is_entity_playing_anim(monkey_ped[i], "missfbi5ig_30monkeys", "monkey_a_freakout_loop")
										if GET_ENTITY_ANIM_CURRENT_TIME(monkey_ped[i], "missfbi5ig_30monkeys", "monkey_a_freakout_loop") > 0.99
											open_sequence_task(monkey_seq)
												task_play_anim(null, "missfbi5ig_30monkeys", "monkey_a_freakout_out", normal_blend_in, normal_blend_out, -1) 
												task_play_anim(null, "missfbi5ig_30monkeys", "monkey_a_idle", normal_blend_in, normal_blend_out, -1, af_looping) 
											close_sequence_task(monkey_seq)
											task_perform_sequence(monkey_ped[i], monkey_seq)
											clear_sequence_task(monkey_seq)
											
											monkey_anim_status[i] = play_freaked_out_anims
										endif
									endif 
								break 
								
								case 1
								case 4
								case 7
									if is_entity_playing_anim(monkey_ped[i], "missfbi5ig_30monkeys", "monkey_b_freakout_loop")
										if GET_ENTITY_ANIM_CURRENT_TIME(monkey_ped[i], "missfbi5ig_30monkeys", "monkey_b_freakout_loop") > 0.99
											open_sequence_task(monkey_seq)
												task_play_anim(null, "missfbi5ig_30monkeys", "monkey_b_freakout_out", normal_blend_in, normal_blend_out, -1) 
												task_play_anim(null, "missfbi5ig_30monkeys", "monkey_b_idle", normal_blend_in, normal_blend_out, -1, af_looping) 
											close_sequence_task(monkey_seq)
											task_perform_sequence(monkey_ped[i], monkey_seq)
											clear_sequence_task(monkey_seq)
											
											monkey_anim_status[i] = play_freaked_out_anims
										endif 
									endif 
								break 
								
								case 2
								case 5
								case 8
									if is_entity_playing_anim(monkey_ped[i], "missfbi5ig_30monkeys", "monkey_c_freakout_loop")
										if GET_ENTITY_ANIM_CURRENT_TIME(monkey_ped[i], "missfbi5ig_30monkeys", "monkey_c_freakout_loop") > 0.99
											open_sequence_task(monkey_seq)
												task_play_anim(null, "missfbi5ig_30monkeys", "monkey_c_freakout_out", normal_blend_in, normal_blend_out, -1) 
												task_play_anim(null, "missfbi5ig_30monkeys", "monkey_c_idle", normal_blend_in, normal_blend_out, -1, af_looping) 
											close_sequence_task(monkey_seq)
											task_perform_sequence(monkey_ped[i], monkey_seq)
											clear_sequence_task(monkey_seq)
											
											monkey_anim_status[i] = play_freaked_out_anims
										endif 
									endif
								break 
							endswitch // monkey index
							
						endif 
					break 
				endswitch // monkey_anim_status[i]
			endif 
		endif 
	endfor 

endproc



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: PROP SPAWNING/CLEANUP !
//
//************************************************************************************************************************************************************

PROC CLEAN_UP_MC_PROP(INT iProp)
	IF isoundid[iprop] <> -1
		PRINTLN("[RCC MISSION] [ALARM] Stopping alarm as prop is getting cleaned up ", iprop)
        STOP_SOUND(isoundid[iprop])
    ENDIF
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].mn = PROP_FLARE_01
		CLEAN_UP_FLARE_SFX_FOR_THIS_PROP(iprop)
	ENDIF
	IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iprop ].iPropBitset, ciFMMC_PROP_IgnoreVisCheckCleanup )
		DELETE_OBJECT( oiProps[ iprop ] )
		PRINTLN("[PRP_BUG] [RCC MISSION] Prop ", iprop, " getting cleaned up early - delete, objective: ", g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iCleanupObjective," team: ",g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iCleanupTeam)
	ELSE
		SET_OBJECT_AS_NO_LONGER_NEEDED( oiProps[ iprop ] )
		PRINTLN("[PRP_BUG] [RCC MISSION] Prop ", iprop, " getting cleaned up early - no longer needed, objective: ", g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iCleanupObjective," team: ",g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iCleanupTeam)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_CLEANUP_PROP( INT iprop, OBJECT_INDEX  tempProp)
	
	IF GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
	AND IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iprop ].iPropBitset, ciFMMC_PROP_CleanupAtMissionEnd )
		RETURN TRUE
	ENDIF
	
	// Round Restarts. If we return to an earlier rule where the prop wouldn't have yet spawned, we need this to clean it up...
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iAssociatedTeam > -1
		PRINTLN("[RCC MISSION][SHOULD_CLEANUP_PROP] - iProp: ", iprop, " Has an associated Rule/Team.")
		IF NOT HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iAssociatedObjective)
			PRINTLN("[RCC MISSION][SHOULD_CLEANUP_PROP] - iProp: ", iprop, " Has not reached those spawn conditions.")
			IF DOES_ENTITY_EXIST(tempProp)
				PRINTLN("[RCC MISSION][SHOULD_CLEANUP_PROP] - iProp: ", iprop, " Still exists.")
				IF ((g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iCleanupObjective != -1) AND (g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iCleanupTeam != -1))					
					PRINTLN("[RCC MISSION][SHOULD_CLEANUP_PROP] - Prop exists, but is not meant to be spawned yet. Cleaning up iProp: ", iProp)
					CLEAN_UP_MC_PROP(iProp)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF IS_BIT_SET( iPropCleanedupBS[ GET_LONG_BITSET_INDEX( iprop ) ], GET_LONG_BITSET_BIT( iprop ) )
		//Prop is already cleaned up
		RETURN TRUE
	ENDIF
			
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iprop ].iCleanupObjective !=-1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iprop ].iCleanupTeam !=-1
			IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iprop ].iCleanupObjective <= MC_serverBD_4.iCurrentHighestPriority[ g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iprop ].iCleanupTeam ]
				
				BOOL bAtMidpoint
				IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iprop ].iPropBitset, ciFMMC_PROP_CleanupAtMidpoint )
					IF IS_BIT_SET( MC_serverBD.iObjectiveMidPointBitset[ g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iprop ].iCleanupTeam ], g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iprop ].iCleanupObjective )
						IF( tempProp = NULL )
							RETURN TRUE
						ENDIF
						
						IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iCleanupRange <= 0
							bAtMidpoint = TRUE
						ELSE
							INT iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY(tempProp)
							PLAYER_INDEX ClosestPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
							PED_INDEX playerPed = GET_PLAYER_PED(ClosestPlayer)
							IF tempProp != NULL
								IF GET_DISTANCE_BETWEEN_ENTITIES(playerPed,tempProp) > g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iCleanupRange
									bAtMidpoint = TRUE
								ENDIF
							ELSE
								IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(playerPed,g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].vPos) > g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iCleanupRange
									bAtMidpoint = TRUE
								ENDIF
							ENDIF
							
						ENDIF
					ENDIF
				ELSE
					IF( tempProp = NULL )
						RETURN TRUE
					ENDIF
				
					//Midpoint doesn't matter
					IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iCleanupRange <= 0
						bAtMidpoint = TRUE
					ELSE
						INT iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY(tempProp)
						PLAYER_INDEX ClosestPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
						PED_INDEX playerPed = GET_PLAYER_PED(ClosestPlayer)
						IF tempProp != NULL
							IF GET_DISTANCE_BETWEEN_ENTITIES(playerPed,tempProp) > g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iCleanupRange
								bAtMidpoint = TRUE
							ENDIF
						ELSE
							IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(playerPed,g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].vPos) > g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iCleanupRange
								bAtMidpoint = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF bAtMidpoint
					CLEAN_UP_MC_PROP(iProp)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL HAS_ANY_TEAM_HIT_PROP_ACTION_ACTIVATION(INT iprop)
	
	BOOL bHit = FALSE
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iAssociatedTeam != -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iAssociatedObjective != -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iAssociatedActionStart, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iAssociatedObjective, TRUE)
			bHit = TRUE
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iPropBitset, ciFMMC_PROP_AssociatedSpawn)
			bHit = TRUE
		ENDIF
	ENDIF
	
	IF NOT bHit // If we already know we've hit it, we don't need to keep checking
	AND g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iSecondAssociatedTeam != -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iSecondAssociatedObjective != -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iSecondActionStart, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iSecondAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iSecondAssociatedObjective, TRUE)
			bHit = TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iThirdAssociatedTeam != -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iThirdAssociatedObjective != -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iThirdActionStart, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iThirdAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iThirdAssociatedObjective, TRUE)
			bHit = TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iFourthAssociatedTeam != -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iFourthAssociatedObjective != -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iFourthActionStart, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iFourthAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iFourthAssociatedObjective, TRUE)
			bHit = TRUE
		ENDIF
	ENDIF
	
	RETURN bHit

ENDFUNC

func bool have_prop_assets_loaded(INT iPropToSpawn)
	
	if g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropToSpawn].mn = HEI_PROP_HEIST_APECRATE 
		request_model(A_C_Rhesus)
		request_anim_dict("missfbi5ig_30Monkeys")
		
		IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_SLASHERS(g_FMMC_STRUCT.iAdversaryModeType)
			request_script_audio_bank("FBI_05_MONKEY_SCREAMS_01")
		ENDIF
		
		request_script_audio_bank("FBI_05_MONKEYS")
	endif 

	IF NOT REQUEST_LOAD_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropToSpawn].mn)
		return false
	endif  
	
	if g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropToSpawn].mn = HEI_PROP_HEIST_APECRATE 
		if not has_model_loaded(A_C_Rhesus)
		or not has_anim_dict_loaded("missfbi5ig_30Monkeys")
		or not request_script_audio_bank("FBI_05_MONKEYS")
			return false 
		endif  
		
		IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_SLASHERS(g_FMMC_STRUCT.iAdversaryModeType)
			IF NOT request_script_audio_bank("FBI_05_MONKEY_SCREAMS_01")
				RETURN FALSE
			ENDIF
		ENDIF
	endif
	
	return true

endfunc

FUNC BOOL IS_MODEL_A_DOOR(MODEL_NAMES mn)
	SWITCH(mn)
		CASE PROP_FNCLINK_03GATE1  
		CASE PROP_FNCLINK_03GATE1_L1  
		CASE PROP_FNCLINK_03GATE1_L2
		CASE PROP_FNCLINK_03GATE2  
		CASE PROP_FNCLINK_03GATE2_L1
		CASE PROP_FNCLINK_03GATE2_L2 
		CASE PROP_FNCLINK_03GATE3   
		CASE PROP_FNCLINK_03GATE3_L1
		CASE PROP_FNCLINK_03GATE4    
		CASE PROP_FNCLINK_03GATE4_L1   
		CASE PROP_FNCLINK_03GATE4_L2 
		CASE PROP_FNCLINK_03GATE5   
		CASE PROP_FNCLINK_03GATE5_L1 
		CASE PROP_FNCLINK_03GATE5_L2
		CASE PROP_LRGGATE_04A
		CASE PROP_LRGGATE_04A_L1
		//Add more doors as they assert...
		//"Cannot create doors in MP, it is currently not supported."
			RETURN TRUE
	ENDSWITCH
	
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_casino_door_01d"))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: This function takes the index of the prop and spawns it given the parameters in the creator
PROC LOAD_PROP( INT iPropToSpawn )

	IF have_prop_assets_loaded(iPropToSpawn)
		isoundid[iPropToSpawn] = -1

		//visibility check? -IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iPropBitset,ciFMMC_PROP_IgnoreVisCheckCleanup)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropToSpawn].mn = XM_PROP_X17_SUB
			PRINTLN("[LOAD_PROP] - This is a submarine, creating a second object for it's collision")
			oiProps[iPropToSpawn] = CREATE_PROP_WITH_SUB_COMPONENT(iPropToSpawn, oiPropsChildren, FALSE)
		ELSE
		
		oiProps[ iPropToSpawn ] = CREATE_OBJECT( 	g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iPropToSpawn ].mn, 
													g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iPropToSpawn ].vPos, 
													FALSE, 			// Not a network object?	
													FALSE,			// This script is not the host object
													IS_MODEL_A_DOOR(g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iPropToSpawn ].mn) )		//Don't force this to be an object unless its a door to stop asserts
		ENDIF
										
		CLEAR_BIT(iPropCleanedupBS[GET_LONG_BITSET_INDEX(iPropToSpawn)], GET_LONG_BITSET_BIT(iPropToSpawn))
		CLEAR_BIT(iPropRespawnNowBS[GET_LONG_BITSET_INDEX(iPropToSpawn)], GET_LONG_BITSET_BIT(iPropToSpawn))
		// Sets up the correct rotation/position for the prop passed, but will ALWAYS make the alarm invisible!
		
		SET_UP_FMMC_PROP_RC( oiProps[ iPropToSpawn ], iPropToSpawn, DEFAULT )
		
		///----

		if g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropToSpawn].mn = HEI_PROP_HEIST_APECRATE 
		AND iMonkeyPedCount < ciMAX_MONKEYPEDS

			vector monkey_pos
			float monkey_heading
			text_label_15 monkey

			monkey_pos = get_offset_from_entity_in_world_coords(oiprops[iPropToSpawn], <<0.0, 0.0, 0.7>>)
			monkey_heading = (get_entity_heading(oiprops[iPropToSpawn]) + 180.00)

			monkey_ped[iMonkeyPedCount] = create_ped(pedtype_mission, a_c_rhesus, monkey_pos, monkey_heading, false, false)

			setup_monkey_attributes(monkey_ped[iMonkeyPedCount])
			monkey = "monkey"
			monkey += iMonkeyPedCount
			set_ped_name_debug(monkey_ped[iMonkeyPedCount], monkey)
			
			monkey_anim_status[iMonkeyPedCount] = play_normal_anims //initialise monkey_anim_system()

			iMonkeyPedCount++
				
		endif      
		
		PRINTLN( "[RCC MISSION] CREATED PROP ", iPropToSpawn )
		
		IF( g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iPropToSpawn ].mn = PROP_FLARE_01 )
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] [PTFX] Requesting flare assets - BACKUP")
			REQUEST_NAMED_PTFX_ASSET( "scr_biolab_heist" )
		ENDIF
		
		IF GET_PROP_LOD_OVERRIDE_DISTANCE( g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iPropToSpawn ].mn ) != -1
			SET_ENTITY_LOD_DIST( oiProps[ iPropToSpawn ],GET_PROP_LOD_OVERRIDE_DISTANCE( g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iPropToSpawn ].mn ) )
			PRINTLN( "[RCC MISSION] SETTING PROP LOD DISTANCE: ", GET_PROP_LOD_OVERRIDE_DISTANCE( g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iPropToSpawn ].mn ) )
		ENDIF
		
		SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropToSpawn].mn)

	ENDIF
	
ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: PROP PROCESSING !
//
//************************************************************************************************************************************************************



/// PURPOSE: Get the correct sound bank ID for the alarm prop passed in, as well as setting up the alarm timer
PROC REQUEST_ALARM_SOUND_BANK_AND_SOUND_ID( INT iAlarmPropIndex )
	STRING sAlarmBank
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iAlarmPropIndex].iPropBitSet2, ciFMMC_PROP2_TriggerAlarmViaCCTV) 
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iAlarmPropIndex].iPropBitSet2, ciFMMC_PROP2_TriggerAlarmViaCCTV) AND IS_BIT_SET(MC_ServerBD.iServerBitSet6, SBBOOL6_PLAYER_HAS_BEEN_SPOTTED_CCTV)
		IF NOT HAS_NET_TIMER_STARTED( AlarmTimer[ iAlarmPropIndex ] )
			START_NET_TIMER( AlarmTimer[ iAlarmPropIndex ] )
		ENDIF
		
		INT iDelayFromAlarmProp = g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iAlarmPropIndex ].iAlarmDelay

		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( AlarmTimer[ iAlarmPropIndex ] ) >= iDelayFromAlarmProp
			// Gettin the correct sound bank string for this alarm
			sAlarmBank = GET_FMMC_ALARM_BANK_FROM_SELECTION( g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iAlarmPropIndex ].iAlarmSound )
			IF NOT IS_STRING_NULL_OR_EMPTY( sAlarmBank )
				IF REQUEST_SCRIPT_AUDIO_BANK( sAlarmBank )
					PRINTLN("[RCC MISSION] [ALARM] ALARM team at objective 1: ",g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iAlarmPropIndex ].iAssociatedObjective )
					isoundid[ iAlarmPropIndex ] = GET_SOUND_ID()
					IF IS_THIS_IS_A_STRAND_MISSION()
					AND NOT HAS_FIRST_STRAND_MISSION_BEEN_PASSED()
						PASS_OVER_CUT_SCENE_ALARM(oiProps[ iAlarmPropIndex ], isoundid[ iAlarmPropIndex ], g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iAlarmPropIndex ].iAlarmSound)
					ENDIF
				ELSE
					PRINTLN( "[RCC MISSION] requesting audio bank" )
				ENDIF
			ELSE
				PRINTLN( "[RCC MISSION] [ALARM] ALARM team at objective: ", g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iAlarmPropIndex ].iAssociatedObjective )
				isoundid[ iAlarmPropIndex ] = GET_SOUND_ID()
				IF IS_THIS_IS_A_STRAND_MISSION()
				AND NOT HAS_FIRST_STRAND_MISSION_BEEN_PASSED()
					PASS_OVER_CUT_SCENE_ALARM(oiProps[ iAlarmPropIndex ], isoundid[ iAlarmPropIndex ], g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iAlarmPropIndex ].iAlarmSound)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_MISSION_FLARE_PTFX()

	INT iPTFX

	FOR iPTFX = 0 TO (ciTOTAL_PTFX-1)
		IF ptfxFlareIndex[iPTFX] != NULL
			IF DOES_PARTICLE_FX_LOOPED_EXIST( ptfxFlareIndex[ iPTFX ] )
				STOP_PARTICLE_FX_LOOPED(ptfxFlareIndex[iPTFX])
			ENDIF
			PRINTLN("[RCC MISSION] cleaning up PTFX for end of mission: ",iPTFX)
			ptfxFlareIndex[iPTFX] = NULL
		ENDIF
	ENDFOR
	
ENDPROC

PROC CLEANUP_FLARE_PROPS_FOR_CUTSCENE()

	INT iprop

	CLEANUP_MISSION_FLARE_PTFX()
	
	FOR iProp = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfProps-1)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iprop ].mn = PROP_FLARE_01
			IF DOES_ENTITY_EXIST( oiProps[ iprop ] )	
				DELETE_OBJECT(oiProps[ iprop ] )
				PRINTLN("[RCC MISSION] cleaning up flare for end of mission cutscene: ",iprop)
			ENDIF
		ENDIF
	ENDFOR

ENDPROC

FUNC INT GET_TEAM_ASSOCIATED_OBJECTIVE(INT iProp, INT iTeam)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAssociatedTeam = iTeam
		RETURN g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAssociatedObjective
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iSecondAssociatedTeam = iTeam
		RETURN g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iSecondAssociatedObjective
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iThirdAssociatedTeam = iTeam
		RETURN g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iThirdAssociatedObjective
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iFourthAssociatedTeam = iTeam
		RETURN g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iFourthAssociatedObjective
	ENDIF
	
	RETURN -1
ENDFUNC

FUNC BOOL IS_LOCAL_SUDDEN_DEATH_ACTIVE()
	IF IS_BIT_SET( MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH )
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC INITALISE_CORONA_DATA(INT iFlare)

	corona_flare_data[iFlare].iFlareState = FLARE_STATE_INIT
	corona_flare_data[iFlare].fNextRed = 1.0
	corona_flare_data[iFlare].FNextGreen = 1.0
	corona_flare_data[iFlare].fNextBlue = 0 
	corona_flare_data[iFlare].fRed = 1.0
	corona_flare_data[iFlare].fGreen = 1.0
	corona_flare_data[iFlare].fBlue = 0
	corona_flare_data[iFlare].iPTFXval = iFlare
	PRINTLN("[RCC MISSION][AW SMOKE]Setting up flare corona |   ",iFlare)
	
ENDPROC

PROC INITALISE_FLARE_DATA(INT iFlare, BOOL bAndCorona = TRUE)

	flare_data[iFlare].iFlareState = FLARE_STATE_INIT
	flare_data[iFlare].iNewState = FLARE_STATE_NEUTRAL
	flare_data[iFlare].fNextRed = 1.0
	flare_data[iFlare].FNextGreen = 1.0
	flare_data[iFlare].fNextBlue = 0 
	flare_data[iFlare].fRed = 1.0
	flare_data[iFlare].fGreen = 1.0
	flare_data[iFlare].fBlue = 0
	flare_data[iFlare].iPTFXval = iFlare
	flare_data[iFlare].iWinningTeam = -2
	PRINTLN("[RCC MISSION][AW SMOKE]Setting up flare |   ",iFlare)

	IF bAndCorona
		INITALISE_CORONA_DATA(iFlare)
	ENDIF
	
ENDPROC

PROC SET_LOCAL_CORONA_NEW_RGB(INT iFlare, FLOAT fR, FLOAT fG, FLOAT fB )
	corona_flare_data[iFlare].fNextRed = fR
	corona_flare_data[iFlare].fNextGreen = fG
	corona_flare_data[iFlare].fNextBlue = fB
	PRINTLN("[RCC MISSION][AW SMOKE]SET_CORONA_NEW_RGB | fR: ",fR," fG:  ",fG," fB: " , fB )
ENDPROC

PROC SET_LOCAL_FLARE_NEW_RGB(INT iFlare, FLOAT fR, FLOAT fG, FLOAT fB, BOOL bAndCorona = TRUE  )
	flare_data[iFlare].fNextRed = fR
	flare_data[iFlare].fNextGreen = fG
	flare_data[iFlare].fNextBlue = fB
	
	PRINTLN("[RCC MISSION][AW SMOKE]SET_FLARE_NEW_RGB | fR: ",fR," fG:  ",fG," fB: " , fB )
	
	IF bAndCorona
		SET_LOCAL_CORONA_NEW_RGB(iFlare,fR,fG,fB)
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_ALARM_PLAY_FOR_TEAM(INT iPropIndex, INT iTeam)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropIndex].iAlarmTeam != -1
		IF iTeam = g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropIndex].iAlarmTeam
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

PROC CREATE_RANDOM_SUBMARINE_EXPLOSION(ENTITY_INDEX eiSubProp, BOOL bPlaySnd = FALSE)

	VECTOR vExplosionPos
	
	VECTOR vForwardsOffset
	VECTOR vSidewaysOffset
	VECTOR vUpOffset
	
	NETWORK_SEED_RANDOM_NUMBER_GENERATOR(1000)
	GET_ENTITY_MATRIX(eiSubProp, vSidewaysOffset, vForwardsOffset, vUpOffset, vExplosionPos) //Offsets are swapped around here because the model seems to be rotated 90
	
	vForwardsOffset = vForwardsOffset * (GET_RANDOM_FLOAT_IN_RANGE(-cf_SubExplosion_Range_Forward, cf_SubExplosion_Range_Forward))
	vSidewaysOffset = vSidewaysOffset * (GET_RANDOM_FLOAT_IN_RANGE(-cf_SubExplosion_Range_Sideways, cf_SubExplosion_Range_Sideways))
	vUpOffset 		= vUpOffset * (GET_RANDOM_FLOAT_IN_RANGE(-cf_SubExplosion_Range_Sideways, cf_SubExplosion_Range_Sideways))
	
	vExplosionPos = GET_ENTITY_COORDS(eiSubProp) + vForwardsOffset + vSidewaysOffset + vUpOffset
	vExplosionPos -= (GET_ENTITY_UP_VECTOR(eiSubProp) * 5.0)

	ADD_EXPLOSION(vExplosionPos, EXP_TAG_MINE_UNDERWATER, 1, bPlaySnd, FALSE, 1.1)
	//ADD_EXPLOSION_WITH_USER_VFX(vExplosionPos, EXP_TAG_MINE_UNDERWATER, HASH("scr_xm_submarine_explosion"), 1, TRUE, FALSE, 1.1)
	
	//Surface effects
	VECTOR vSurfacePos = vExplosionPos
	GET_WATER_HEIGHT(vSurfacePos, vSurfacePos.z)
	
	USE_PARTICLE_FX_ASSET("scr_xm_submarine")
	START_NETWORKED_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_xm_submarine_surface_explosion", vSurfacePos, <<0,0,0>>)
	
	PRINTLN("[TMS][ExplodingSub] Adding explosion at ", vExplosionPos)
ENDPROC

PROC SWAP_OUT_SUB_PROP_FOR_BROKEN_SUB_PROP(ENTITY_INDEX& eiSubProp)
	
	MODEL_NAMES mnBrokenSubModel = INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_sub_damage"))
	REQUEST_MODEL(mnBrokenSubModel)
	
	IF HAS_MODEL_LOADED(mnBrokenSubModel)
		//VECTOR vCoords = GET_ENTITY_COORDS(eiSubProp)
		//VECTOR vRotation = GET_ENTITY_ROTATION(eiSubProp)
		//DELETE_ENTITY(eiSubProp)
		
		//eiSubmarineProp = CREATE_OBJECT(mnBrokenSubModel, vCoords, FALSE, TRUE, TRUE)
		//SET_ENTITY_ROTATION(eiSubmarineProp, vRotation)
		
		CREATE_MODEL_SWAP(GET_ENTITY_COORDS(eiSubProp), 200, GET_ENTITY_MODEL(eiSubProp), mnBrokenSubModel, TRUE)
		
		PRINTLN("[TMS][ExplodingSub] Broken sub model is loaded; SWAP_OUT_SUB_PROP_FOR_BROKEN_SUB_PROP has been called! Swapping sub to use this model: ", mnBrokenSubModel)
	ELSE
		PRINTLN("[ExplodingSub] Requesting broken sub model before model swapping...")
	ENDIF
ENDPROC


PROC PROCESS_SUBMARINE_PROP(ENTITY_INDEX& eiSubProp)
	
	//IF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_SUBMARINE_FINALE)
	IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
	AND DOES_ENTITY_EXIST(eiSubProp)
		INT iTeam = 0
		INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
		MODEL_NAMES mnBrokenSubModel = INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_sub_damage"))
		
		PRINTLN("[ExplodingSub] Rule ", iRule)
		//DRAW_LINE(GET_ENTITY_COORDS(LocalPlayerPed), GET_ENTITY_COORDS(eiSubProp), 255, 255, 255)
		
		IF iRule < FMMC_MAX_RULES
		
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[iRule], ciBS_RULE11_EXPLODE_SUBMARINE)
			AND NOT IS_BIT_SET(iSubExplosionBS, iSE_SequenceStarted)
				SET_BIT(iSubExplosionBS, iSE_SequenceStarted)
				PRINTLN("[ExplodingSub] Sub is starting to explode now!")
				
				IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_OVERRIDDEN_MINE_PTFX)
					SET_BIT(iLocalBoolCheck27, LBOOL27_OVERRIDDEN_MINE_PTFX)
					SET_PARTICLE_FX_OVERRIDE("exp_underwater_mine", "scr_xm_submarine_explosion")
				ENDIF
				
				IF IS_ENTITY_UNDERWATER(LocalPlayerPed)
					START_AUDIO_SCENE("dlc_xm_submarine_destruction_scene")
				ENDIF
			ENDIF			
			
			IF IS_BIT_SET(iSubExplosionBS, iSE_SequenceStarted)
			AND NOT IS_BIT_SET(iSubExplosionBS, iSE_SequenceDone)
			
				REQUEST_MODEL(mnBrokenSubModel)
			
				IF IS_BIT_SET(MC_serverBD.iServerBitSet6, SBBOOL6_SUB_EXPLOSION_STARTED)
				AND NOT IS_BIT_SET(iSubExplosionBS, iSE_PlayedStream)
				AND IS_ENTITY_UNDERWATER(LocalPlayerPed)
					IF LOAD_STREAM("submarine_explosions_stream", "dlc_xm_submarine_sounds")
						PLAY_STREAM_FROM_POSITION(GET_ENTITY_COORDS(eiSubProp))
						SET_BIT(iSubExplosionBS, iSE_PlayedStream)
						PRINTLN("[ExplodingSub] Playing audio stream locally")
					ENDIF
				ENDIF
				
				IF NOT IS_BIT_SET(iSubExplosionBS, iSE_SwappedModel)
				AND IS_BIT_SET(MC_serverBD.iServerBitSet6, SBBOOL6_SUB_EXPLOSION_BIGONE)
					PRINTLN("[TMS][ExplodingSub] SWAP_OUT_SUB_PROP_FOR_BROKEN_SUB_PROP is being called!")
					SWAP_OUT_SUB_PROP_FOR_BROKEN_SUB_PROP(eiSubProp)
					SET_BIT(iSubExplosionBS, iSE_SwappedModel)
					
					USE_PARTICLE_FX_ASSET("scr_xm_submarine")
					VECTOR vSurfaceSplashPos = GET_ENTITY_COORDS(eiSubProp)
					GET_WATER_HEIGHT(vSurfaceSplashPos, vSurfaceSplashPos.z)
					ptfx_SubmarineSplashes = START_PARTICLE_FX_LOOPED_AT_COORD("scr_xm_submarine_surface_splashes", vSurfaceSplashPos, <<0,0,90>>)
				ENDIF
				
				//Explosions
				IF bIsLocalPlayerHost
					IF HAS_NET_TIMER_EXPIRED(stSubExplosion_Preexplosion_Timer, ci_SubExplosion_TimeBeforeExploding)
						
						//Scripted explosions
						IF NOT HAS_NET_TIMER_STARTED(stSubExplosion_Timer)								
							SET_BIT(MC_serverBD.iServerBitSet6, SBBOOL6_SUB_EXPLOSION_STARTED)
							
							START_NET_TIMER(stSubExplosion_Timer)
							PRINTLN("[ExplodingSub] stSubExplosion_Timer starting now!")
							
							iSubExplosionBS = 0
							CREATE_RANDOM_SUBMARINE_EXPLOSION(eiSubProp)
						ELSE
							IF HAS_NET_TIMER_EXPIRED(stSubExplosion_Timer, ci_SubExplosion_Explosion1Time)
							AND NOT IS_BIT_SET(iSubExplosionBS, iSE_Explosion1)
								CREATE_RANDOM_SUBMARINE_EXPLOSION(eiSubProp)
								SET_BIT(iSubExplosionBS, iSE_Explosion1)
								PRINTLN("[ExplodingSub] Triggering Explosion 1")
							ENDIF
							
							IF HAS_NET_TIMER_EXPIRED(stSubExplosion_Timer, ci_SubExplosion_Explosion2Time)
							AND NOT IS_BIT_SET(iSubExplosionBS, iSE_Explosion2)
								CREATE_RANDOM_SUBMARINE_EXPLOSION(eiSubProp)
								SET_BIT(iSubExplosionBS, iSE_Explosion2)
								PRINTLN("[ExplodingSub] Triggering Explosion 2")
							ENDIF
							
							IF HAS_NET_TIMER_EXPIRED(stSubExplosion_Timer, ci_SubExplosion_Explosion3Time)
							AND NOT IS_BIT_SET(iSubExplosionBS, iSE_Explosion3)
								CREATE_RANDOM_SUBMARINE_EXPLOSION(eiSubProp)
								SET_BIT(iSubExplosionBS, iSE_Explosion3)
								PRINTLN("[ExplodingSub] Triggering Explosion 3")
							ENDIF
							
							IF HAS_NET_TIMER_EXPIRED(stSubExplosion_Timer, ci_SubExplosion_Explosion4Time)
							AND NOT IS_BIT_SET(iSubExplosionBS, iSE_Explosion4)
								CREATE_RANDOM_SUBMARINE_EXPLOSION(eiSubProp)
								SET_BIT(iSubExplosionBS, iSE_Explosion4)
								PRINTLN("[ExplodingSub] Triggering Explosion 4")
							ENDIF
							
							IF HAS_NET_TIMER_EXPIRED(stSubExplosion_Timer, ci_SubExplosion_BigExplosionTime)
							AND NOT IS_BIT_SET(iSubExplosionBS, iSE_BigExplosion)
							
								INT iExpCount = 0
								
								FOR iExpCount = 0 TO 20
									CREATE_RANDOM_SUBMARINE_EXPLOSION(eiSubProp)
								ENDFOR
								
								SET_BIT(iSubExplosionBS, iSE_BigExplosion)
								SET_BIT(MC_serverBD.iServerBitSet6, SBBOOL6_SUB_EXPLOSION_BIGONE)
								PRINTLN("[ExplodingSub] Triggering Big Explosion!")
							ENDIF
							
							IF HAS_NET_TIMER_EXPIRED(stSubExplosion_Timer, ci_SubExplosion_RandomExplosionStartTime)
							AND NOT IS_BIT_SET(iSubExplosionBS, iSE_DoRandomExplosions)
								SET_BIT(iSubExplosionBS, iSE_DoRandomExplosions)
								PRINTLN("[ExplodingSub] Triggering iSE_DoRandomExplosions!")
							ENDIF
							
						ENDIF
						
						//Random explosions
						IF IS_BIT_SET(iSubExplosionBS, iSE_DoRandomExplosions)
							IF HAS_NET_TIMER_STARTED(stSubExplosion_RandomExplosions_Timer)
								IF HAS_NET_TIMER_EXPIRED(stSubExplosion_RandomExplosions_Timer, ci_SubExplosion_MaxTime - (GET_RANDOM_INT_IN_RANGE(0, 2000)))
									//Boom
									INT iExplosions = GET_RANDOM_INT_IN_RANGE(0, 2)
									INT i

									FOR i = 0 TO iExplosions
										CREATE_RANDOM_SUBMARINE_EXPLOSION(eiSubProp, TRUE)
									ENDFOR
									
									RESET_NET_TIMER(stSubExplosion_RandomExplosions_Timer)
								ENDIF
								
								PRINTLN("[ExplodingSub] stSubExplosion_RandomExplosions_Timer: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stSubExplosion_RandomExplosions_Timer))
							ELSE
								START_NET_TIMER(stSubExplosion_RandomExplosions_Timer)
								PRINTLN("[ExplodingSub] stSubExplosion_RandomExplosions_Timer starting now!")
							ENDIF
						ENDIF
					ELSE
						IF NOT HAS_NET_TIMER_STARTED(stSubExplosion_Preexplosion_Timer)
							START_NET_TIMER(stSubExplosion_Preexplosion_Timer)
							PRINTLN("[ExplodingSub] Pre-timer starting now!")
						ELSE
							PRINTLN("[ExplodingSub] Pre-timer running now!")
						ENDIF
					ENDIF
				ENDIF
				
				//Moving the prop
				VECTOR vSubNewCoords = GET_ENTITY_COORDS(eiSubProp)
				vSubNewCoords.z = vSubNewCoords.z -@ (fCurSubSpeed * 1.5)
				vSubNewCoords.x = vSubNewCoords.x -@ (fCurSubSpeed * 2)
				
				VECTOR vSubNewRot = GET_ENTITY_ROTATION(eiSubProp)
				vSubNewRot.x = vSubNewRot.x -@ (fCurSubSpeed * 0.85)
				vSubNewRot.y = vSubNewRot.y -@ (fCurSubSpeed * 0.95)
				SET_ENTITY_ROTATION(eiSubProp, vSubNewRot)
				
				PRINTLN("[ExplodingSub] fCurSubSpeed: ", fCurSubSpeed)
				
				//IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
					//DRAW_DEBUG_LINE(GET_ENTITY_COORDS(LocalPlayerPed), vSubNewCoords, 255, 0, 0, 255)
				//ENDIF
				
				FLOAT fLowestZ = -45
				
				IF vSubNewCoords.z > fLowestZ
					SET_ENTITY_COORDS(eiSubProp, vSubNewCoords)
					PRINTLN("[ExplodingSub] Setting submarine coords to ", vSubNewCoords)
					
					IF fCurSubSpeed < 25
						fCurSubSpeed = fCurSubSpeed +@ 0.025
					ENDIF
				ELSE
					vSubNewCoords.z = fLowestZ
					
					
					//DRAW_DEBUG_SPHERE(vSubnewCoords, 5)
					
					IF fCurSubSpeed > 0.1
						PRINTLN("[ExplodingSub] Slowing down now")
						fCurSubSpeed = fCurSubSpeed -@ 2
						
						IF fCurSubSpeed <= 0
							fCurSubSpeed = 0
						ENDIF
						
					ELSE
						fCurSubSpeed = 0
						PRINTLN("[ExplodingSub] Submarine has hit sea floor - done exploding now")
						
						REMOVE_PARTICLE_FX(ptfx_SubmarineSplashes)
						
						PLAY_SOUND_FROM_COORD(-1, "submarine_sunk", vSubNewCoords, "dlc_xm_submarine_sounds")
						STOP_AUDIO_SCENE("dlc_xm_submarine_destruction_scene")
						SET_BIT(iSubExplosionBS, iSE_SequenceDone)
					ENDIF
				ENDIF
			ELSE
				//RESET_NET_TIMER(stSubExplosion_Preexplosion_Timer)
				//RESET_NET_TIMER(stSubExplosion_Timer)
				SET_MODEL_AS_NO_LONGER_NEEDED(mnBrokenSubModel)
				
				IF IS_AUDIO_SCENE_ACTIVE("dlc_xm_submarine_destruction_scene")
					STOP_AUDIO_SCENE("dlc_xm_submarine_destruction_scene")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE: This function maintains any special prop behaviours eg alarms
PROC PROCESS_PROP_BEHAVIOUR( INT iProp )
	
	

	STRING strFlareFX = ""
	STRING strPTFXAsset = ""
	INT iR, iG, iB, iA
	FLOAT fR, fG, fB
	
	INT iTeam = MC_playerBD[ iLocalPart ].iteam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[ iTeam ]
	
	INT iPropAssociatedObjective = GET_TEAM_ASSOCIATED_OBJECTIVE(iProp, iTeam)	//g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iprop ].iAssociatedObjective
	
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iprop ].mn
		
		CASE PROP_LD_ALARM_01
			// If the sound ID is -1, a valid alarm sound hasn't been set yet - if not, this initialises it
			IF isoundid[ iprop ] = -1
				IF HAS_ANY_TEAM_HIT_PROP_ACTION_ACTIVATION(iprop)
				AND SHOULD_ALARM_PLAY_FOR_TEAM(iprop, iTeam)
					REQUEST_ALARM_SOUND_BANK_AND_SOUND_ID(iprop)
				ENDIF
			ENDIF
			
			IF isoundid[ iprop ] != -1 // Make some noise! We have a valid sound ID for this prop now
				MAINTAIN_PROP_LD_ALARM_01_SOUND(oiProps[iprop], isoundid[iprop], g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iAlarmSound,iNonRepeatingAlarmPlayedBS, iSiloAlarmLoopSound)
			ENDIF
			
		BREAK // End of PROP_LD_ALARM_01
		
		CASE PROP_BARRIER_WORK05
			// Has no special behaviour
		BREAK
		
		CASE XM_PROP_X17_SUB
			IF NOT DOES_ENTITY_EXIST(eiSubmarineProp)
				eiSubmarineProp = oiProps[iProp]
			ENDIF
		BREAK
		
		CASE PROP_FLARE_01
		
			IF iPropAssociatedObjective <= iRule OR iPropAssociatedObjective = -1
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iprop ].iFlareVFX = ciFLARE_VFX_SHOW_WINNER
				OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iprop ].iFlareVFX = ciFLARE_VFX_CAPTURE_OWNER
				OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iprop ].iFlareVFX = ciFLARE_VFX_CUSTOM_COLOUR
					PRINTLN( "[RCC MISSION][AW SMOKE] Changing colour flare loading in -scr_lowrider ")
					strPTFXAsset = "scr_lowrider"
				ELSE
					PRINTLN( "[RCC MISSION][AW SMOKE] Standard flare loading in -scr_biolab_heist ")
					strPTFXAsset = "scr_biolab_heist"
				ENDIF
				
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
					strPTFXAsset = "scr_xm_ht"
				ENDIF
								
				REQUEST_NAMED_PTFX_ASSET(strPTFXAsset)
				IF HAS_NAMED_PTFX_ASSET_LOADED(strPTFXAsset)
				AND iNumCreatedFlarePTFX < ciTOTAL_PTFX
				AND NOT IS_BIT_SET( iCreatedFlaresBitset[GET_LONG_BITSET_INDEX(iProp)], GET_LONG_BITSET_BIT(iProp) )
					SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iFlareVFX
					
						CASE ciFLARE_VFX_OUTDOORS
							strFlareFX = "scr_heist_biolab_flare"
						BREAK
						
						CASE ciFLARE_VFX_INTERIOR
							strFlareFX = "scr_heist_biolab_flare"
						BREAK
						
						CASE ciFLARE_VFX_UNDERWATER
							strFlareFX = "scr_heist_biolab_flare_underwater"
						BREAK
						
						CASE ciFLARE_VFX_SHOW_WINNER
							strFlareFX = "scr_lowrider_flare"
						BREAK
						
						CASE ciFLARE_VFX_CAPTURE_OWNER
							strFlareFX = "scr_lowrider_flare"
						BREAK
						
						CASE ciFLARE_VFX_CUSTOM_COLOUR
							strFlareFX = "scr_lowrider_flare"
						BREAK
						
					ENDSWITCH

					IF NOT DOES_PARTICLE_FX_LOOPED_EXIST( ptfxFlareIndex[ iNumCreatedFlarePTFX ] )
					
						IF NOT IS_LOCAL_SUDDEN_DEATH_ACTIVE()
							USE_PARTICLE_FX_ASSET( strPTFXAsset ) // NOTE(Owain): This call is used for START_PARTICLE_FX_ commands ONLY
							
							FLOAT fPFXScale
							
							fPFXScale = 1
							
							IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
								strFlareFX = "scr_xm_ht_package_flare"
								fPFXScale = 2
							ENDIF
							
							ptfxFlareIndex[ iNumCreatedFlarePTFX ] = START_PARTICLE_FX_LOOPED_ON_ENTITY( strFlareFX, oiProps[ iprop ], <<0,0,0.12>>, <<0,0,0>>, fPFXScale )							
							
							IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
								//Sound
								IF AM_I_ON_A_HEIST()
									PLAY_SOUND_FROM_ENTITY(-1, "Flare", oiProps[iProp], "DLC_HEISTS_BIOLAB_FINALE_SOUNDS")
								ELSE
									FlareSoundID[iNumCreatedFlarePTFX] = -1
									FlareSoundID[iNumCreatedFlarePTFX] = GET_SOUND_ID()
									FlareSoundIDPropNum[iNumCreatedFlarePTFX] = iProp
									PLAY_SOUND_FROM_ENTITY(FlareSoundID[iNumCreatedFlarePTFX], "Flare", oiProps[iProp], "DLC_GR_DR_Player_Sounds")
								ENDIF
							ENDIF
							
							IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iprop ].iFlareVFX = ciFLARE_VFX_CUSTOM_COLOUR
								IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iPropColouring != -1
								
									GET_HUD_COLOUR(INT_TO_ENUM(HUD_COLOURS,g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iPropColouring), iR, iG, iB, iA)
									PRINTLN( "[RCC MISSION][AW SMOKE]iNumCreatedFlarePTFX: ",iNumCreatedFlarePTFX)
									PRINTLN( "[RCC MISSION][AW SMOKE] g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iPropColouring: ", g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iPropColouring)
									PRINTLN( "[RCC MISSION][AW SMOKE] iR:",iR)
									PRINTLN( "[RCC MISSION][AW SMOKE] iG:",iG)
									PRINTLN( "[RCC MISSION][AW SMOKE] iB:",iB)
									
									fR = TO_FLOAT(iR)				
									fR  = fR  / 255.0
									
									fG = TO_FLOAT(iG)
									fG  = fG  / 255.0
									
									fB = TO_FLOAT(iB)
									fB  = fB  / 255.0
									
									PRINTLN( "[RCC MISSION][AW SMOKE] fR:",fR)
									PRINTLN( "[RCC MISSION][AW SMOKE] fG:",fG)
									PRINTLN( "[RCC MISSION][AW SMOKE] fB:",fB)
									
									//unref
									IF iA = 0
									
									ENDIF
									
									SET_PARTICLE_FX_LOOPED_COLOUR(ptfxFlareIndex[iNumCreatedFlarePTFX],fR, fG, fB,TRUE)
									
								ENDIF
							ENDIF
							
							FREEZE_ENTITY_POSITION(oiProps[iProp], TRUE)	//Stop Flares being moved causing a de-sync in coordinates (as they're not networked).
							
							iNumCreatedFlarePTFX++
							SET_BIT( iCreatedFlaresBitset[ GET_LONG_BITSET_INDEX( iProp ) ], GET_LONG_BITSET_BIT( iProp ) )
							PRINTLN( "[RCC MISSION][AW SMOKE] Creating flare etc ")
							CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] [PTFX] Created flare #", iNumCreatedFlarePTFX, " with string: ", strFlareFX )
							
							IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iprop ].iFlareVFX = ciFLARE_VFX_CAPTURE_OWNER
								PRINTLN( "[RCC MISSION][AW SMOKE] INITALISE_FLARE_DATA - setting to yellow")
								INITALISE_FLARE_DATA(iNumCreatedDynamicFlarePTFX)
								SET_LOCAL_FLARE_NEW_RGB(iNumCreatedDynamicFlarePTFX, 1.0, 1.0, 0)
								IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iFlareConnectedLocate > -1
									flare_data[iNumCreatedDynamicFlarePTFX].iConnectedLocate = g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iFlareConnectedLocate
								ENDIF
								iNumCreatedDynamicFlarePTFX ++
							ENDIF
							
							
							
							g_iTheWinningTeam = - 1
							iMaxCaptureAmount = -1
							iWinningState = -1			
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF
			
			INT iPTFX
			INT iHighestScore
			
			FOR iPTFX = 0 TO iNumCreatedFlarePTFX -1
				IF DOES_PARTICLE_FX_LOOPED_EXIST( ptfxFlareIndex[ iPTFX ] )
					PRINTLN( "[RCC MISSION][AW SMOKE] Particle FX Looped Exists  ")
					IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iprop ].iFlareVFX = ciFLARE_VFX_CAPTURE_OWNER
					
						
						// Logic for this is now handled in - PROCESS_EVERY_FRAME_CAPTURE_LOGIC()
						
						IF NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_VFX_CAPTURE_OWNER)
							SET_BIT(iLocalBoolCheck14, LBOOL14_VFX_CAPTURE_OWNER)
						ENDIF
					ELIF g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iprop ].iFlareVFX = ciFLARE_VFX_SHOW_WINNER
						PRINTLN( "[RCC MISSION][AW SMOKE] ciFLARE_VFX_SHOW_WINNER ")
						
						
						INT iWinningTeam
						INT i
							
						FOR i = 0 TO MC_ServerBD.iNumberOfTeams -1
							IF MC_serverBD.iTeamScore[i] > iHighestScore
								iHighestScore = MC_serverBD.iTeamScore[i]
								iWinningTeam = i
								PRINTLN( "[RCC MISSION][AW SMOKE] iHighestScore: ", iHighestScore)
								PRINTLN( "[RCC MISSION][AW SMOKE] iWinningTeam: ",  iWinningTeam)
							ENDIF
						ENDFOR
						
						IF iTeam != iWinningTeam 
							IF iHighestScore =  MC_serverBD.iTeamScore[i]
								//Show Yellow = Tied
								PRINTLN( "[RCC MISSION][AW SMOKE] YELLOW TIED -1 new")
								SET_PARTICLE_FX_LOOPED_COLOUR(ptfxFlareIndex[ iPTFX ],255,255,0,TRUE )
							ELSE
								//Show Red = Losing
								PRINTLN( "[RCC MISSION][AW SMOKE] RED LOSING ")
								SET_PARTICLE_FX_LOOPED_COLOUR(ptfxFlareIndex[ iPTFX ],255,0,0,TRUE )
							ENDIF
						ELSE
							IF iHighestScore =  MC_serverBD.iTeamScore[i]
								//Show Yellow = Tied
								PRINTLN( "[RCC MISSION][AW SMOKE] YELLOW TIED -2 new")
								SET_PARTICLE_FX_LOOPED_COLOUR(ptfxFlareIndex[ iPTFX ],255,255,0,TRUE )
							ELSE
								//Show Blue = Winning
								PRINTLN( "[RCC MISSION][AW SMOKE] BLUE WINNING ")
								SET_PARTICLE_FX_LOOPED_COLOUR(ptfxFlareIndex[ iPTFX ],0,0,255,TRUE )
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
			
		BREAK
		
		case HEI_PROP_HEIST_APECRATE
		
		break 
		
		CASE PROP_CONST_FENCE02B
			IF iCurrentHeistMissionIndex = HBCA_BS_PACIFIC_STANDARD_FINALE
			AND DOES_ENTITY_EXIST( oiProps[ iprop ] )
			AND ARE_VECTORS_EQUAL(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].vPos, <<232.311, 215.677, 105.385>>)
			AND NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(oiProps[iprop]), <<231.86, 215.68, 105.39>>, 0.1)
				PRINTLN("[RCC MISSION] PROCESS_PROP_BEHAVIOUR - Moving prop ",iprop," into position to block the bank doors for Pacific Standard Finale part 1")
				SET_ENTITY_COORDS(oiProps[iprop], <<231.86, 215.68, 105.39>>)
				SET_ENTITY_ROTATION(oiProps[iprop], <<0, 0, -64.55>>)
				SET_ENTITY_VISIBLE(oiProps[iprop], FALSE)
			ENDIF
		BREAK	
		CASE PROP_BOOMBOX_01
		CASE PROP_GHETTOBLAST_02
		CASE PROP_TAPEPLAYER_01
		CASE PROP_RADIO_01
			IF iPropAssociatedObjective <= iRule OR iPropAssociatedObjective = -1
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_EmitRadio)
					IF DOES_ENTITY_EXIST(oiProps[iProp])
						FLOAT fDistGameCamToEmitterMin
						FLOAT fDistGameCamToEmitterMax
						FLOAT fDistGameCamToEmitter
						
						fDistGameCamToEmitterMin = 50.0
						fDistGameCamToEmitterMax = 100.0
						fDistGameCamToEmitter = GET_DISTANCE_BETWEEN_COORDS(GET_GAMEPLAY_CAM_COORD(), GET_ENTITY_COORDS(oiProps[iProp]))
						
						IF fDistGameCamToEmitter <= fDistGameCamToEmitterMax
							IF iRadioEmitterPropCheck = -1
								IF NOT IS_AUDIO_SCENE_ACTIVE("MP_Reduce_Score_For_Emitters_Scene")
									START_AUDIO_SCENE("MP_Reduce_Score_For_Emitters_Scene")
									PRINTLN("START_AUDIO_SCENE = MP_Reduce_Score_For_Emitters_Scene")
								ENDIF
								
								//"SE_Script_Placed_Prop_Emitter_PA"  		(bassier)
								//"SE_Script_Placed_Prop_Emitter_Boombox"  	(regular)
		 						//"SE_Script_Placed_Prop_Emitter_Radio"  	(tinny)
								LINK_STATIC_EMITTER_TO_ENTITY("SE_Script_Placed_Prop_Emitter_Boombox", oiProps[iProp])
								
								SET_EMITTER_RADIO_STATION("SE_Script_Placed_Prop_Emitter_Boombox", GET_RADIO_STATION_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iRadioStation))
								PRINTLN("SET_EMITTER_RADIO_STATION = ", GET_RADIO_STATION_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iRadioStation))
								
								SET_STATIC_EMITTER_ENABLED("SE_Script_Placed_Prop_Emitter_Boombox", TRUE)
								
								PRINTLN("iRadioEmitterPropCheck = ", iProp)
								
								iRadioEmitterPropCheck = iProp
							ENDIF
							
							IF iRadioEmitterPropCheck != -1 AND iRadioEmitterPropCheck = iProp
								IF IS_AUDIO_SCENE_ACTIVE("MP_POSITIONED_RADIO_MUTE_SCENE")
									SET_AUDIO_SCENE_VARIABLE("MP_POSITIONED_RADIO_MUTE_SCENE", "apply", (1.0 - (((1.0) / fDistGameCamToEmitterMax) * (fDistGameCamToEmitterMax - CLAMP(fDistGameCamToEmitter - fDistGameCamToEmitterMin, 0.0, fDistGameCamToEmitterMax)))))
								ENDIF
								
								IF IS_AUDIO_SCENE_ACTIVE("MP_Reduce_Score_For_Emitters_Scene")
									SET_AUDIO_SCENE_VARIABLE("MP_Reduce_Score_For_Emitters_Scene", "apply", (((1.0) / fDistGameCamToEmitterMax) * (fDistGameCamToEmitterMax - CLAMP(fDistGameCamToEmitter - fDistGameCamToEmitterMin, 0.0, fDistGameCamToEmitterMax))))
									PRINTLN("SET_AUDIO_SCENE_VARIABLE = ", (((1.0) / fDistGameCamToEmitterMax) * (fDistGameCamToEmitterMax - CLAMP(fDistGameCamToEmitter - fDistGameCamToEmitterMin, 0.0, fDistGameCamToEmitterMax))))
								ENDIF
							ENDIF
						ELSE
							IF iRadioEmitterPropCheck != -1
								IF IS_AUDIO_SCENE_ACTIVE("MP_Reduce_Score_For_Emitters_Scene")
									STOP_AUDIO_SCENE("MP_Reduce_Score_For_Emitters_Scene")
									PRINTLN("STOP_AUDIO_SCENE = MP_Reduce_Score_For_Emitters_Scene (out of range)")
								ENDIF
								
								SET_STATIC_EMITTER_ENABLED("SE_Script_Placed_Prop_Emitter_Boombox", FALSE)
								
								iRadioEmitterPropCheck = -1
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE IND_PROP_FIREWORK_01
			
			PRINTLN("[TMS][FW] Prop ", iProp, " || Looking at a IND_PROP_FIREWORK_01!")
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitSet2, ciFMMC_PROP2_UsePlacedFireworkZone) 
				IF iPropAssociatedObjective <= iRule OR iPropAssociatedObjective = -1
					IF NOT IS_BIT_SET(iCreatedFireworkBitset[GET_LONG_BITSET_INDEX(iProp)], GET_LONG_BITSET_BIT(iProp))
						IF CREATE_FIREWORK_FX(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAlarmDelay, oiProps[iProp], g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAlarmSound)
							SET_BIT(iCreatedFireworkBitset[GET_LONG_BITSET_INDEX(iProp)], GET_LONG_BITSET_BIT(iProp))
						ELSE
							PRINTLN("[TMS][FW] Prop ", iProp, " || CREATE_FIREWORK_FX is returning false!")
						ENDIF
					ELSE
						PRINTLN("[TMS][FW] Prop ", iProp, " || iCreatedFireworkBitset is set for this one")
					ENDIF
				ELSE
					PRINTLN("[TMS][FW] Prop ", iProp, " || This one doesn't have a valid objective: ", iPropAssociatedObjective)
				ENDIF
			ELSE
				PRINTLN("[TMS][FW] Prop ", iProp, " || ciFMMC_PROP2_UsePlacedFireworkZone is set for this one")
			ENDIF
		BREAK
		// Add more special props behaviour in here
		
	ENDSWITCH // End of the switch over the special prop model names
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IBI_02"))
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_SuddenDeathTarget)
			iSuddenDeathTargetProp = iProp
		ENDIF
	ENDIF
	
	//INT_TO_ENUM doesnt work in case statements, moved this bit out
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iprop ].mn = PROP_SHAMAL_CRASH
	OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iprop ].mn = INT_TO_ENUM(MODEL_NAMES, HASH("apa_MP_Apa_Crashed_USAF_01a"))
	OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iprop ].mn = INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_Shamal_crash"))
		IF DOES_ENTITY_EXIST( oiProps[ iprop ] )
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_EmitCrashSound)
				IF HAS_SOUND_FINISHED(iCrashSound)
					vCrashSoundPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oiProps[iprop],<<0.0,6.5,0.0>>)
					PLAY_SOUND_FROM_COORD(iCrashSound, "Crashed_Plane_Ambience", vCrashSoundPos, "DLC_Apartments_Extraction_SoundSet", FALSE, 0,FALSE)
				ELSE
					IF NOT ARE_VECTORS_EQUAL(vCrashSoundPos, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oiProps[iprop],<<0.0,6.5,0.0>>))
						STOP_SOUND(iCrashSound)
						vCrashSoundPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oiProps[iprop],<<0.0,6.5,0.0>>)
						PLAY_SOUND_FROM_COORD(iCrashSound, "Crashed_Plane_Ambience", vCrashSoundPos, "DLC_Apartments_Extraction_SoundSet", FALSE, 0,FALSE)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_EmitCrashSound)
				IF NOT HAS_SOUND_FINISHED(iCrashSound)
					STOP_SOUND(iCrashSound)
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	
ENDPROC

//PURPOSE: Does the distance checks for stinger props and pops the local's player's tyres if they're too close
PROC MAINTAIN_PLACED_STINGER(INT iProp)

	VECTOR vPlayerVehicleCoords
	VECTOR vStingerCoord = g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iProp].vPos
	VEHICLE_INDEX viPlayerVehicle
	
	IF DOES_ENTITY_EXIST(LocalPlayerPed) AND IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		viPlayerVehicle = GET_VEHICLE_PED_IS_USING(LocalPlayerPed)
		
		IF DOES_ENTITY_EXIST(viPlayerVehicle)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(viPlayerVehicle)
				IF IS_VEHICLE_TYRE_BURST(viPlayerVehicle, SC_WHEEL_CAR_FRONT_LEFT) AND IS_VEHICLE_TYRE_BURST(viPlayerVehicle, SC_WHEEL_CAR_FRONT_RIGHT)
					//Forget doing the rest of this if the player's tyres are already popped
					EXIT
				ENDIF

				//Adds a bit of an offset to make it more like the distance is being measured from the front tyres rather than the middle of the car
				VECTOR vRoughFrontTyreOffset = GET_ENTITY_FORWARD_VECTOR(viPlayerVehicle)
				FLOAT fRoughFrontTyreOffsetDistance = 1.8
				
				vRoughFrontTyreOffset.x *= fRoughFrontTyreOffsetDistance
				vRoughFrontTyreOffset.y *= fRoughFrontTyreOffsetDistance
				
				vPlayerVehicleCoords = GET_ENTITY_COORDS(viPlayerVehicle) + vRoughFrontTyreOffset
				//DRAW_DEBUG_SPHERE(vPlayerVehicleCoords, 0.5, 50, 50, 50, 155)
				
				FLOAT fPopDistance = 3.5
				//DRAW_DEBUG_SPHERE(vStingerCoord, SQRT(fPopDistance), 50, 50, 50, 155)
					
				IF VDIST2(vPlayerVehicleCoords, vStingerCoord) <= fPopDistance
					PRINTLN("[TMS] MAINTAIN_PLACED_STINGER - Popped the wheels of the local player")
					
					SET_VEHICLE_TYRES_CAN_BURST(viPlayerVehicle, TRUE)
					
					SET_VEHICLE_TYRE_BURST(viPlayerVehicle, SC_WHEEL_CAR_FRONT_LEFT, TRUE)
					SET_VEHICLE_TYRE_BURST(viPlayerVehicle, SC_WHEEL_CAR_FRONT_RIGHT, TRUE)
				ELSE
					//PRINTLN("[TMS] distance to stinger is ", VDIST2(vPlayerVehicleCoords, vStingerCoord))
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

///purpose: processes only non associated objective g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iprop ].iAssociatedObjective = -1
PROC NON_ASSOCIATED_OBJECTIVE_PROCESS_PROP_BEHAVIOUR(INT iProp)

	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iprop ].mn
		
		case HEI_PROP_HEIST_APECRATE
		
		break 
		
		CASE PROP_CONST_FENCE02B
			IF iCurrentHeistMissionIndex = HBCA_BS_PACIFIC_STANDARD_FINALE
			AND DOES_ENTITY_EXIST( oiProps[ iprop ] )
			AND ARE_VECTORS_EQUAL(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].vPos, <<232.311, 215.677, 105.385>>)
			AND NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(oiProps[iprop]), <<231.86, 215.68, 105.39>>, 0.1)
				PRINTLN("[RCC MISSION] NON_ASSOCIATED_OBJECTIVE_PROCESS_PROP_BEHAVIOUR - Moving prop ",iprop," into position to block the bank doors for Pacific Standard Finale part 1")
				SET_ENTITY_COORDS(oiProps[iprop], <<231.86, 215.68, 105.39>>)
				SET_ENTITY_ROTATION(oiProps[iprop], <<0, 0, -64.55>>)
				SET_ENTITY_VISIBLE(oiProps[iprop], FALSE)
			ENDIF
		BREAK

		// Add more special props behaviour in here
	ENDSWITCH 
ENDPROC

FUNC BOOL IS_FLASHING_STATE_TOGGLE(INT iMiliseconds, INT iIncrementer, INT iTime, SCRIPT_TIMER timerToUse)
	RETURN GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(timerToUse) > (iTime*1000 + (iMiliseconds*(iIncrementer+1)))
ENDFUNC

FUNC STRING GET_PTFX_DISSOLVE_NAME_BASED_ON_PROP(MODEL_NAMES mn)
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_STUNT_TRACK_SHORT"))
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_STUNT_TRACK_STRAIGHT"))
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_TRACK_STRAIGHT_S"))
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_TRACK_STRAIGHT_M"))
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_TRACK_STRAIGHT_LM"))
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_TRACK_STRAIGHT_L"))
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_STUNT_BLOCK_HUGE_03"))
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("ba_Prop_Track_Straight_LM"))
		RETURN "scr_as_trap_zone_rectangle"
	ENDIF
	
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("AS_PROP_AS_STUNT_TARGET"))
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("AS_PROP_AS_STUNT_TARGET_SMALL"))
		RETURN "scr_as_trap_zone_circle"
	ENDIF
	
	RETURN "scr_as_trap_zone_rectangle"
ENDFUNC

FUNC FLOAT GET_PTFX_DISSOLVE_LENGTH_BASED_ON_PROP(MODEL_NAMES mn)
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_STUNT_TRACK_SHORT"))
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_STUNT_TRACK_STRAIGHT"))
		RETURN 0.6
	ENDIF
	
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_TRACK_STRAIGHT_S"))
		RETURN 0.1
	ENDIF
	
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_TRACK_STRAIGHT_M"))
		RETURN 0.31
	ENDIF
	
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("ba_Prop_Track_Straight_LM"))
		RETURN 0.31
	ENDIF
	
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_TRACK_STRAIGHT_LM"))	
		RETURN 0.63
	ENDIF
	
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_TRACK_STRAIGHT_L"))
		RETURN 0.95
	ENDIF
	
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_STUNT_BLOCK_HUGE_03"))
		RETURN 0.5
	ENDIF
	
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("AS_PROP_AS_STUNT_TARGET"))
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("AS_PROP_AS_STUNT_TARGET_SMALL"))
		RETURN 1.0
	ENDIF
	RETURN 0.4
ENDFUNC

FUNC FLOAT GET_PTFX_DISSOLVE_WIDTH_BASED_ON_PROP(MODEL_NAMES mn)
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_STUNT_TRACK_SHORT"))
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_STUNT_TRACK_STRAIGHT"))
		RETURN 0.08
	ENDIF
	
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_TRACK_STRAIGHT_S"))
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_TRACK_STRAIGHT_M"))
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_TRACK_STRAIGHT_LM"))
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_TRACK_STRAIGHT_L"))
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("ba_Prop_Track_Straight_LM"))
		RETURN 0.135
	ENDIF
	
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_STUNT_BLOCK_HUGE_03"))
		RETURN 0.4
	ENDIF
	
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("AS_PROP_AS_STUNT_TARGET"))
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("AS_PROP_AS_STUNT_TARGET_SMALL"))
		RETURN 1.0
	ENDIF
	RETURN 0.5
ENDFUNC

PROC VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP(INT iProp, BOOL bFlash, INT iMiliseconds)
	INT iPropActual = iPropFadeoutEveryFrameIndex[iProp]

	IF DOES_ENTITY_EXIST(oiProps[iPropActual])
		SCRIPT_TIMER timerToUse			
		IF HAS_NET_TIMER_STARTED(MC_serverBD_3.tdMultiObjectiveLimitTimer[MC_playerBD[iPartToUse].iteam])
			timerToUse = MC_serverBD_3.tdMultiObjectiveLimitTimer[MC_playerBD[iPartToUse].iteam]
		ELIF HAS_NET_TIMER_STARTED(MC_serverBD_3.tdObjectiveLimitTimer[MC_playerBD[iPartToUse].iteam])
			timerToUse = MC_serverBD_3.tdObjectiveLimitTimer[MC_playerBD[iPartToUse].iteam]
		ENDIF
		
		PRINTLN("[LM][VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP] - Flashes counter: ", iFlashToggle[iProp])
		IF iFlashToggle[iProp] < 7
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropActual].iPropBitSet2, ciFMMC_PROP2_CleanUpVFX)
				IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_PropFadeoutEveryFrameIndex[iProp])
					STOP_PARTICLE_FX_LOOPED(ptfx_PropFadeoutEveryFrameIndex[iProp], TRUE)
				ENDIF
				MODEL_NAMES mn = GET_ENTITY_MODEL(oiProps[iPropActual])
				IF IS_MODEL_VALID(mn)
				AND NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_PropFadeoutEveryFrameIndex[iProp])				
					VECTOR vDimensionsMin
					VECTOR vDimensionsMax
					FLOAT fHeight
					
					GET_MODEL_DIMENSIONS(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropActual].mn, vDimensionsMin, vDimensionsMax)
					
					fHeight = vDimensionsMax.z
					
					VECTOR vSpawn
					
					vSpawn = GET_ENTITY_COORDS(oiProps[iPropActual])
					vSpawn.z += fHeight
					
					IF HAS_NAMED_PTFX_ASSET_LOADED("scr_as_trap")
						USE_PARTICLE_FX_ASSET("scr_as_trap")
						ptfx_PropFadeoutEveryFrameIndex[iProp] = START_PARTICLE_FX_LOOPED_AT_COORD(GET_PTFX_DISSOLVE_NAME_BASED_ON_PROP(mn), vSpawn, GET_ENTITY_ROTATION(oiProps[iPropActual]), 1.0, DEFAULT, DEFAULT, DEFAULT, TRUE)					
					ELSE
						REQUEST_NAMED_PTFX_ASSET("scr_as_trap")
					ENDIF 
					
					PRINTLN("[LM][VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP] - Creating PTFX for iPropEF: ", iProp)
					SET_BIT(iCleanUpPropEF_BS[iProp], ci_CleanUpPropEF_BS_ColourNotSet)
				ENDIF
				IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_PropFadeoutEveryFrameIndex[iProp])
				AND IS_BIT_SET(iCleanUpPropEF_BS[iProp], ci_CleanUpPropEF_BS_ColourNotSet)
					SET_PARTICLE_FX_LOOPED_COLOUR(ptfx_PropFadeoutEveryFrameIndex[iProp], 0.8, 0.2, 0.2, TRUE)
					SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfx_PropFadeoutEveryFrameIndex[iProp], "ScaleX", GET_PTFX_DISSOLVE_LENGTH_BASED_ON_PROP(mn), TRUE)
					SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfx_PropFadeoutEveryFrameIndex[iProp], "ScaleY", GET_PTFX_DISSOLVE_WIDTH_BASED_ON_PROP(mn), TRUE)
					SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfx_PropFadeoutEveryFrameIndex[iProp], "Intensity", 0.5, TRUE)				
					CLEAR_BIT(iCleanUpPropEF_BS[iProp], ci_CleanUpPropEF_BS_ColourNotSet)
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
			PRINTLN("[LM][VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP] - Flashing not finished for iProp: ", iProp)
			PRINTLN("[LM][VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP] - Flashing not finished for iPropActual: ", iPropActual)
			PRINTLN("[LM][VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP] - Flashing Toggle Time: ", iFlashToggleTime[iProp])
			PRINTLN("[LM][VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP] - prop time: ", g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropActual].iForceCleanupPropAfterTime*1000)
			PRINTLN("[LM][VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP] - prop time + calc: ", g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropActual].iForceCleanupPropAfterTime*1000 + (iMiliseconds*iFlashToggle[iProp]))			
			PRINTLN("[LM][VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP] - Timer to use: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(timerToUse))
			PRINTLN("[LM][VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP] - Mission Length: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdMissionLengthTimer))
			#ENDIF
			
			IF bFlash
				IF IS_FLASHING_STATE_TOGGLE(iMiliseconds, iFlashToggle[iProp], iFlashToggleTime[iProp], timerToUse)
					IF GET_ENTITY_ALPHA(oiProps[iPropActual]) = 150
						SET_ENTITY_ALPHA(oiProps[iPropActual], 255, FALSE)
					ELIF GET_ENTITY_ALPHA(oiProps[iPropActual]) = 255
						SET_ENTITY_ALPHA(oiProps[iPropActual], 150, FALSE)
					ENDIF
					
					IF NOT IS_STRING_EMPTY(sVVDetonateSoundSet)
						PLAY_SOUND_FROM_ENTITY(-1, "Prop_Flash", oiProps[iPropActual], sVVDetonateSoundSet, FALSE)
						PRINTLN("[RCC MISSION] PLAY_VEHICLE_VENDETTA_SOUND - Playing LProp_Flash")
					ENDIF
					
					iFlashToggle[iProp]++
					PRINTLN("[LM][VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP] - iFlashToggle++ ")
					PRINTLN("[LM][VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP] - Alpha is now set to: ", GET_ENTITY_ALPHA(oiProps[iPropActual]))
				ENDIF
			ELSE
				IF IS_FLASHING_STATE_TOGGLE(iMiliseconds, iFlashToggle[iProp], iFlashToggleTime[iProp], timerToUse)
					IF GET_ENTITY_ALPHA(oiProps[iPropActual])-25 >= 0
						SET_ENTITY_ALPHA(oiProps[iPropActual], GET_ENTITY_ALPHA(oiProps[iPropActual])-25, FALSE)
					ENDIF
					iFlashToggle[iProp]++
					
					PRINTLN("[LM][VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP] - iFadeToggle++ ")
					PRINTLN("[LM][VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP] - Alpha is now set to: ", GET_ENTITY_ALPHA(oiProps[iPropActual]))
				ENDIF
			ENDIF
		ENDIF
		
		IF iFlashToggle[iProp] >= 7
		OR (iFlashToggle[iProp] > 1 AND NOT HAS_NET_TIMER_STARTED(timerToUse))
			PRINTLN("[LM][VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP] - Flashing finished for iProp: ", iPropActual)
			
			IF NOT IS_STRING_EMPTY(sVVDetonateSoundSet)
				PLAY_SOUND_FROM_ENTITY(-1, "Prop_Delete" ,oiProps[iPropActual], sVVDetonateSoundSet, FALSE)
			ENDIF
			
			IF iVVDetonateSoundID != -1
				//Local player
				PRINTLN("[RCC MISSION] PROCESS_VEHICLE_VENDETTA_LOOP_SOUNDS -inside of VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP - ending Detonate.")
				STOP_SOUND(iVVDetonateSoundID)
				STOP_AUDIO_SCENE(sVVDetonateSceneSoundSet)
				SCRIPT_RELEASE_SOUND_ID(iVVDetonateSoundID)
							
				IF NOT IS_STRING_NULL_OR_EMPTY(sVVDetonateSoundSet)
					PLAY_SOUND_FRONTEND(-1, "Deactivate", sVVDetonateSoundSet, FALSE)
				ENDIF
				
				iVVDetonateSoundID = -1
			ENDIF
			
			SET_ENTITY_ALPHA(oiProps[iPropActual], 0, FALSE)
			SET_ENTITY_COLLISION(oiProps[iPropActual], FALSE)
			REMOVE_DECALS_FROM_OBJECT(oiProps[iPropActual])
			SET_OBJECT_AS_NO_LONGER_NEEDED(oiProps[iPropActual])
			IF bIsLocalPlayerHost
				SET_BIT(MC_serverBD_3.iPropDestroyedBS[iPropActual / 32], iPropActual % 32)
			ENDIF
			
			iPropFadeoutEveryFrameIndex[iProp] = -1
			iFlashToggle[iProp] = 1
			iFlashToggleTime[iProp] = 0
			CLEAR_BIT(iCleanUpPropEF_BS[iProp], ci_CleanUpPropEF_BS_ColourNotSet)
			IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_PropFadeoutEveryFrameIndex[iProp])
				SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfx_PropFadeoutEveryFrameIndex[iProp], "Intensity", 1.0, TRUE)
				PRINTLN("[LM][VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP] - Timer Expired for iPropEF: ", iProp)
				START_NET_TIMER(tdFlashToggleDissolvePTFX[iProp])
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_PROP_CLEAN_UP_WITH_FADE(INT iProp)
	
	INT iPropActual = iPropFadeoutEveryFrameIndex[iProp]
	
	IF iPropActual > -1
	AND iPropActual < GET_FMMC_MAX_NUM_PROPS()
		IF IS_BIT_SET(iPropCleanedupTriggeredBS[GET_LONG_BITSET_INDEX(iPropActual)], GET_LONG_BITSET_BIT(iPropActual))
			PRINTLN("[LM][PROCESS_PROP_CLEAN_UP_WITH_FADE] - iPropCleanedupTriggeredBS set on iProp: ", iPropActual)
			IF (g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropActual].iForceCleanupPropAfterTime > 0 // 0 means off.
			AND NOT IS_BIT_SET(iPropDetonateTriggeredBS[GET_LONG_BITSET_INDEX(iPropActual)], GET_LONG_BITSET_BIT(iPropActual)))
			OR IS_BIT_SET(iPropDetonateTriggeredBS[GET_LONG_BITSET_INDEX(iPropActual)], GET_LONG_BITSET_BIT(iPropActual))
				IF HAS_NET_TIMER_STARTED(MC_serverBD.tdMissionLengthTimer)
					PRINTLN("[LM][PROCESS_PROP_CLEAN_UP_WITH_FADE] - Calling VV ForceCleanup: ", iPropActual)
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropActual].iPropBitset, ciFMMC_PROP_Cleanup_Flash)
						VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP(iProp, TRUE, ci_FLASH_TIMER_INTERVAL)
						PRINTLN("[LM][PROCESS_PROP_CLEAN_UP_WITH_FADE] - Flash")
					ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropActual].iPropBitset, ciFMMC_PROP_Cleanup_Fade)
						VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP(iProp, FALSE, ci_FLASH_TIMER_INTERVAL)
						PRINTLN("[LM][PROCESS_PROP_CLEAN_UP_WITH_FADE] - Fade")
					ELIF IS_BIT_SET(iPropDetonateTriggeredBS[GET_LONG_BITSET_INDEX(iPropActual)], GET_LONG_BITSET_BIT(iPropActual))
						VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP(iProp, TRUE, ci_FLASH_TIMER_INTERVAL)
						PRINTLN("[LM][PROCESS_PROP_CLEAN_UP_WITH_FADE] - Detonate")
					ELSE
						VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP(iProp, TRUE, ci_FLASH_TIMER_INTERVAL)
						PRINTLN("[LM][PROCESS_PROP_CLEAN_UP_WITH_FADE] - Default")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(tdFlashToggleDissolvePTFX[iProp])
		IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdFlashToggleDissolvePTFX[iProp], ci_FlashToggleDissolveTime)
			PRINTLN("[LM][VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP] - Timer Expired for iPropEF: ", iProp)
			IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_PropFadeoutEveryFrameIndex[iProp])
				STOP_PARTICLE_FX_LOOPED(ptfx_PropFadeoutEveryFrameIndex[iProp], TRUE)
				PRINTLN("[LM][VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP] - Stopping ptfx for iPropEF: ", iProp)
			ENDIF
			RESET_NET_TIMER(tdFlashToggleDissolvePTFX[iProp])
		ENDIF
	ENDIF
ENDPROC

PROC TARGET_PRACTICE_TARGET_PROP_SHOT(INT iProp)
	//Change Prop Appearance
	SET_OBJECT_TINT_INDEX(oiProps[iProp], 1) //Red
ENDPROC

PROC TARGET_PRACTICE_TARGET_PROP_RESET(INT iProp)
	//Reset Prop Appearance
	SET_OBJECT_TINT_INDEX(oiProps[iProp], 9) //Green
ENDPROC

PROC PROCESS_TARGET_PRACTICE_TARGET_PROP_SPIN(INT iProp)
	FLOAT fOriginRotZ = g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].vRot.Z
	
	INT iSpins = CEIL(g_FMMC_STRUCT.iTargetPracticeResetTime / 360.0)
	
	FLOAT fTargetRotZ = g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].vRot.Z * iSpins
	
	FLOAT fAlpha = CLAMP((1.0 / g_FMMC_STRUCT.iTargetPracticeResetTime) * GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stTargetPropShot[iProp]), 0.0, 1.0)
	
	FLOAT fSpin = INTERP_FLOAT(fOriginRotZ, fTargetRotZ, fAlpha, INTERPTYPE_DECEL)
	
	SET_ENTITY_ROTATION(oiProps[iProp], g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].vRot + <<0.0, 0.0, fSpin>>)
ENDPROC

PROC PROCESS_TARGET_PRACTICE(INT iProp)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_spec_target_s_01a"))
	OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_spec_target_m_01a"))
	OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_spec_target_b_01a"))
		//[Client] Keep the prop at full health for damage tracking
		IF GET_ENTITY_HEALTH(oiProps[iProp]) < 1000
			SET_ENTITY_HEALTH(oiProps[iProp], 1000)
		ENDIF
		
		//PROCESS_TARGET_PRACTICE_TARGET_PROP_SPIN(iProp)
		
		//[Client] Send message when a valid target is shot
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(oiProps[iProp], LocalPlayerPed)
			IF MC_ServerBD_4.ePropClaimState[iProp] != ePropClaimingState_Contested
				IF NOT HAS_NET_TIMER_STARTED(stTargetPropShot[iProp])
					BROADCAST_FMMC_TARGET_PROP_SHOT(iProp, MC_PlayerBD[iLocalPart].iTeam, iLocalPart, GET_NETWORK_TIME())
					
					PRINTLN("[PROCESS_PROP_EVERY_FRAME] HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY - Local Player damaged Target Prop = ", iProp)
					
					START_NET_TIMER(stTargetPropShot[iProp])
					
					TARGET_PRACTICE_TARGET_PROP_SHOT(iProp)
				ENDIF
			ENDIF
			
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(oiProps[iProp])
		ENDIF
		
		//[Client] If the server broadcast data shows target is shot, but the targets timer isn't started already, do it now!
		IF NOT HAS_NET_TIMER_STARTED(stTargetPropShot[iProp])
			IF MC_ServerBD_4.ePropClaimState[iProp] = ePropClaimingState_Contested
				START_NET_TIMER(stTargetPropShot[iProp])
				
				TARGET_PRACTICE_TARGET_PROP_SHOT(iProp)
			ENDIF
		ENDIF
		
		//[Client] Target reset time expired
		IF HAS_NET_TIMER_EXPIRED_READ_ONLY(stTargetPropShot[iProp], g_FMMC_STRUCT.iTargetPracticeResetTime)
			//[Server] Reset the target props server broadcast data state
			IF bIsLocalPlayerHost
				IF MC_ServerBD_4.ePropClaimState[iProp] != ePropClaimingState_Unclaimed
					MC_ServerBD_4.ePropClaimState[iProp] = ePropClaimingState_Unclaimed
				ENDIF
			ENDIF
			
			//[Client] Reset the target props local state and timer
			IF MC_ServerBD_4.ePropClaimState[iProp] != ePropClaimingState_Contested
				RESET_NET_TIMER(stTargetPropShot[iProp])
				
				TARGET_PRACTICE_TARGET_PROP_RESET(iProp)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    This is used to grab the index that a prop uses to access it's fake door variables
/// RETURNS:
///    
FUNC BOOL GET_FAKE_DOOR_PROP_VARIABLE_INDEX(INT iPropIndex, INT &iVarIdx)

	//Grab the index that the variables for the door stuff is stored at
	INT i
	REPEAT COUNT_OF(sFakeDoorProp) i
		IF sFakeDoorProp[i].iPropIndex = iPropIndex
			iVarIdx = i
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	//If there is no index set up for thsi one yet then create one
	REPEAT COUNT_OF(sFakeDoorProp) i
		IF sFakeDoorProp[i].iPropIndex = -1
			iVarIdx = i
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
	
ENDFUNC

PROC HANDLE_DROP_THE_BOMB_BOMBABLE_WALLS(INT iProp)
	IF IS_PROP_A_BOMBABLE_BLOCK(iProp)
		
		//Syncing blocks between players
		IF IS_BIT_SET(MC_serverBD_3.iPropDestroyedBS[GET_LONG_BITSET_INDEX(iProp)], GET_LONG_BITSET_BIT(iProp)) //If this block needs to be deleted...
			IF DOES_ENTITY_EXIST(oiProps[iProp])
				USE_PARTICLE_FX_ASSET("scr_ba_bomb")
				START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_ba_bomb_destroy", GET_ENTITY_COORDS(oiProps[iProp]) + <<0.0,0.0,50.0>>, GET_ENTITY_ROTATION(oiProps[iProp]), 1.0)		
				PRINTLN("[RCC MISSION][BombBlocks] Playing the destroy particle effect")
				
				DELETE_OBJECT(oiProps[iProp])
				PRINTLN("[RCC MISSION][BombBlocks] Removing prop ", iProp, " as it has been destroyed by another player")
			ENDIF
		ELSE
			IF NOT DOES_ENTITY_EXIST(oiProps[iProp])
				PRINTLN("[RCC MISSION][BombBlocks] Bombable Prop ", iProp, " doesn't exist! Broadcasting event now")
				BROADCAST_FMMC_PROP_DESTROYED(iProp)
			ELSE
				IF IS_ENTITY_DEAD(oiProps[iProp])
					PRINTLN("[RCC MISSION][BombBlocks] Bombable Prop ", iProp, " is dead! Broadcasting event now")
					BROADCAST_FMMC_PROP_DESTROYED(iProp)
				ELIF HAS_OBJECT_BEEN_BROKEN(oiProps[iProp])
					PRINTLN("[RCC MISSION][BombBlocks] Bombable Prop ", iProp, " is broken! Broadcasting event now")
					BROADCAST_FMMC_PROP_DESTROYED(iProp)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_PROP_FOR_CUTSCENES(INT iProp)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitSet2, ciFMMC_PROP2_HidePropInCutscene)
		IF DOES_ENTITY_EXIST(oiProps[iProp])
			IF IS_BIT_SET(iPropHiddenForCutsceneBS[GET_LONG_BITSET_INDEX(iProp)], GET_LONG_BITSET_BIT(iProp))
				IF NOT g_bInMissionControllerCutscene
					RESET_ENTITY_ALPHA(oiProps[iProp])
					CLEAR_BIT(iPropHiddenForCutsceneBS[GET_LONG_BITSET_INDEX(iProp)], GET_LONG_BITSET_BIT(iProp))
					PRINTLN("[LM][RCC MISSION][PROCESS_PROP_FOR_CUTSCENES] iProp ", iProp, " is flagged to be hidden in cutscenes. We are no longer playing a cutscene, resetting prop alpha.")
				ENDIF
			ELIF NOT IS_BIT_SET(iPropHiddenForCutsceneBS[GET_LONG_BITSET_INDEX(iProp)], GET_LONG_BITSET_BIT(iProp))
				IF g_bInMissionControllerCutscene
					SET_ENTITY_ALPHA(oiProps[iProp], 0, FALSE)
					SET_BIT(iPropHiddenForCutsceneBS[GET_LONG_BITSET_INDEX(iProp)], GET_LONG_BITSET_BIT(iProp))
					PRINTLN("[LM][RCC MISSION][PROCESS_PROP_FOR_CUTSCENES] iProp ", iProp, " is flagged to be hidden in cutscenes. We are in a cutscene, setting prop alpha to 0.")
				ENDIF
			ENDIF
		ELSE
			CLEAR_BIT(iPropHiddenForCutsceneBS[GET_LONG_BITSET_INDEX(iProp)], GET_LONG_BITSET_BIT(iProp))
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_PROP_EVERY_FRAME(INT iProp)

	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iRespawnOnRuleChangeBS > 0
		INT iTeam
		FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
			IF MC_serverBD_4.iCurrentHighestPriority[iTeam] > 0
				IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_RESET_ALL_PROPS_ON_RENDERPHASE_PAUSE)
				AND NOT GET_TOGGLE_PAUSED_RENDERPHASES_STATUS() 
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iRespawnOnRuleChangeBS, iTeam)
						PRINTLN("[LM][PROCESS_PROP_EVERY_FRAME][GET_TOGGLE_PAUSED_RENDERPHASES_STATUS] - Resetting Prop: ", iProp)
						DELETE_OBJECT(oiProps[iProp])
						CLEAR_BIT(iPropCleanedupBS[GET_LONG_BITSET_INDEX(iProp)], GET_LONG_BITSET_BIT(iProp))
						SET_BIT(iPropRespawnNowBS[GET_LONG_BITSET_INDEX(iProp)], GET_LONG_BITSET_BIT(iProp))
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	// If this prop has an associated rule
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iprop ].iAssociatedObjective != -1
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iPropBitset, ciFMMC_PROP_AssociatedSpawn)
	OR IS_BIT_SET(iPropRespawnNowBS[GET_LONG_BITSET_INDEX(iProp)], GET_LONG_BITSET_BIT(iProp))	
		IF NOT DOES_ENTITY_EXIST( oiProps[ iprop ] )
		AND SHOULD_PROP_SPAWN_NOW( iProp )
		AND NOT SHOULD_CLEANUP_PROP( iprop, NULL )
		 	LOAD_PROP( iprop ) 
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_Requires_Alpha_Flash)
		MAINTAIN_SPEED_BOOST_ALPHA_FLASH(oiProps[iProp], iProp)
	ENDIF
	
	PROCESS_PROP_FOR_CUTSCENES(iProp)
	
	//Target Practice
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_TARGET_PRACTICE)
		PROCESS_TARGET_PRACTICE(iProp)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_TAGGING_TARGETS)
	AND DOES_ENTITY_EXIST(oiProps[iProp])
		IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam] < FMMC_MAX_RULES
			IF IS_IT_SAFE_TO_ADD_TAG_BLIP(MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam])
				INT iTagged
				FOR iTagged = 0 TO FMMC_MAX_TAGGED_ENTITIES-1
					IF MC_serverBD_3.iTaggedEntityType[iTagged] = ENUM_TO_INT(GET_ENTITY_TYPE(oiProps[iProp]))
						IF MC_serverBD_3.iTaggedEntityIndex[iTagged] = iProp
							IF IS_ENTITY_ALIVE(oiProps[iProp])
								VECTOR vEntityCoords = GET_ENTITY_COORDS(oiProps[iProp])
								vEntityCoords.z = (vEntityCoords.z + 1.0)
								
								DRAW_TAGGED_MARKER(vEntityCoords, oiProps[iProp], ENUM_TO_INT(GET_ENTITY_TYPE(oiProps[iProp])), iProp)
								
								IF NOT DOES_BLIP_EXIST(biTaggedEntity[iTagged])
								AND NOT DOES_BLIP_EXIST(GET_BLIP_FROM_ENTITY(oiProps[iProp]))
									ADD_TAGGED_BLIP(oiProps[iProp], ENUM_TO_INT(GET_ENTITY_TYPE(oiProps[iProp])), iProp, iTagged)
								ENDIF
							ELSE
								REMOVE_TAGGED_BLIP(3, iProp)
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
		HANDLE_DROP_THE_BOMB_BOMBABLE_WALLS(iProp)
	ENDIF
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Props with Update rotations
	
	// Moving this here so fake door props are rotated every frame -4117383
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].fUpdateZRotation != -1.0
	AND IS_PROP_MODEL_INSIDE_LIBRARY(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn,PROP_LIBRARY_SPECIAL)
	
		INT iVarIdx
		
		IF NOT GET_FAKE_DOOR_PROP_VARIABLE_INDEX(iProp, iVarIdx)
		
			SCRIPT_ASSERT("Failed to allocate an index to fake door prop. Too many fake door props set up in mission.")
			PRINTLN("PROCESS_PROP_EVERY_FRAME - Failed to add fake door prop using prop index ", iProp)
			
		ELSE
		
			IF HAS_ANY_TEAM_HIT_PROP_ACTION_ACTIVATION(iProp)
			AND sFakeDoorProp[iVarIdx].iStartTime != -1
			
				IF sFakeDoorProp[iVarIdx].iStartTime = 0
					sFakeDoorProp[iVarIdx].iStartTime = GET_GAME_TIMER()
					PRINTLN("[PD2] PROP ROTATION ON UPDATE - Starting rotation")
				ENDIF
				
				INT t = GET_GAME_TIMER() - sFakeDoorProp[iVarIdx].iStartTime
				VECTOR vOriginalRotation = g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].vRot
				FLOAT fRatio = CLAMP(TO_FLOAT(t - g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iUpdateRotationDelay)/g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iUpdateRotationTime, 0.0, 1.0)
				FLOAT fDestAngle = g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].fUpdateZRotation
				
				IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitSet2, ciFMMC_PROP2_InvertUpdateRotation)
				AND fDestAngle < vOriginalRotation.Z
					fDestAngle += 360
				ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitSet2, ciFMMC_PROP2_InvertUpdateRotation)
				AND fDestAngle > vOriginalRotation.Z
					fDestAngle -= 360
				ENDIF
				
				PRINTLN("[PD2] PROP ROTATION ON UPDATE - Rotation ratio is ", fRatio, " with a destination angle of ", fDestAngle) 
				
				IF DOES_ENTITY_EXIST(oiProps[iProp])
					FLOAT fCurrentZ = vOriginalRotation.Z + ((fDestAngle - vOriginalRotation.Z) * fRatio)
					SET_ENTITY_ROTATION(oiProps[iProp], <<vOriginalRotation.X, vOriginalRotation.Y, fCurrentZ>>)
				ENDIF
				
				IF fRatio = 1.0
					sFakeDoorProp[iVarIdx].iStartTime = -1
					PRINTLN("[PD2] PROP ROTATION ON UPDATE - Rotation Done")
				ENDIF
				
			ENDIF
			
			//////////////////////////////////////////////////////
			//START OF SILO DOORS SPECIAL CASE
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = XM_PROP_BASE_CABINET_DOOR_01
			AND NOT FMMC_CHECKPOINTS_IS_ANY_RETRY_START_POINT_SET()
			
				SWITCH sFakeDoorProp[iVarIdx].eSoundStage
				
					CASE FAKE_DOOR_SOUND_STAGE_LOAD
					
						IF LOAD_STREAM("Door_Open_Long", "DLC_XM_Silo_Secret_Door_Sounds")
							IF sFakeDoorProp[iVarIdx].iStartTime != 0
							AND GET_GAME_TIMER() - sFakeDoorProp[iVarIdx].iStartTime >= g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iUpdateRotationDelay
								PLAY_STREAM_FRONTEND()
								sFakeDoorProp[iVarIdx].eSoundStage = FAKE_DOOR_SOUND_STAGE_PLAYING
								PRINTLN("[PD2] PROP ROTATION ON UPDATE ", GET_GAME_TIMER() - sFakeDoorProp[iVarIdx].iStartTime, " - Starting Door opening sound ")
							ELSE
								PRINTLN("[PD2] PROP ROTATION ON UPDATE ", GET_GAME_TIMER() - sFakeDoorProp[iVarIdx].iStartTime, " - Waiting for sound to start ")
							ENDIF
						ENDIF
						
					BREAK
					
					CASE FAKE_DOOR_SOUND_STAGE_PLAYING
						IF sFakeDoorProp[iVarIdx].iStartTime = -1
							STOP_STREAM()
							PLAY_SOUND_FRONTEND(-1,"Door_Open_Limit","DLC_XM_Silo_Secret_Door_Sounds")
							sFakeDoorProp[iVarIdx].eSoundStage = FAKE_DOOR_SOUND_STAGE_COMPLETE
						ENDIF
					BREAK
					
				ENDSWITCH
				
			ENDIF
			
			//END OF SILO DOORS SPECIAL CASE
			//////////////////////////////////////////////////////
			
			
			
		ENDIF
		
	ENDIF
	
	// Props with Update rotations
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iForceCleanupPropAfterTime > 0
	OR IS_BIT_SET(iPropDetonateTriggeredBS[GET_LONG_BITSET_INDEX(iProp)], GET_LONG_BITSET_BIT(iProp))
		IF NOT HAS_NET_TIMER_STARTED(tdFlashPropTimer)
			START_NET_TIMER(tdFlashPropTimer)
		ENDIF
	ENDIF
	
	// Force Prop Cleanup and Prop detonation.
	IF NOT IS_BIT_SET(iPropDetonateTriggeredBS[GET_LONG_BITSET_INDEX(iProp)], GET_LONG_BITSET_BIT(iProp))
		IF NOT IS_BIT_SET(iPropCleanedupTriggeredBS[GET_LONG_BITSET_INDEX(iprop)], GET_LONG_BITSET_BIT(iprop))
		AND g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iForceCleanupPropAfterTime > 0 // 0 means off.
			
			SCRIPT_TIMER timerToUse			
			IF HAS_NET_TIMER_STARTED(MC_serverBD_3.tdMultiObjectiveLimitTimer[MC_playerBD[iPartToUse].iteam])
				timerToUse = MC_serverBD_3.tdMultiObjectiveLimitTimer[MC_playerBD[iPartToUse].iteam]
			ELIF HAS_NET_TIMER_STARTED(MC_serverBD_3.tdObjectiveLimitTimer[MC_playerBD[iPartToUse].iteam])
				timerToUse = MC_serverBD_3.tdObjectiveLimitTimer[MC_playerBD[iPartToUse].iteam]
			ENDIF
			
			IF HAS_NET_TIMER_STARTED(timerToUse)
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(timerToUse) > ((g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iForceCleanupPropAfterTime*1000) - 2500)
					PRINTLN("[LM][PROCESS_PROP_EVERY_FRAME] - Beginning the loop to assign this prop to every frame loop. iProp: ", iProp)
					INT iPropEF = 0
					FOR iPropEF = 0 TO FMMC_MAX_NUM_FADEOUT_PROPS_EVERY_FRAME-1
						IF iPropFadeoutEveryFrameIndex[iPropEF] = -1
							iPropFadeoutEveryFrameIndex[iPropEF] = iProp
							iFlashToggle[iPropEF] = 1
							iFlashToggleTime[iPropEF] = (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(timerToUse)/1000)							
							SET_BIT(iPropCleanedupTriggeredBS[GET_LONG_BITSET_INDEX(iprop)], GET_LONG_BITSET_BIT(iprop))	
							IF DOES_ENTITY_EXIST(oiProps[iProp])
								SET_ENTITY_ALPHA(oiProps[iProp], 150, FALSE)
							ENDIF
							PRINTLN("[LM][PROCESS_PROP_EVERY_FRAME] - ciFMMC_PROP_Cleanup_Triggered has been set on iProp: ", iProp)
							BREAKLOOP
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitSet2,  ciFMMC_PROP2_BlipAsSafeProp)
		iSafeBlippedProp = iProp
	ENDIF
	
	IF DOES_ENTITY_EXIST(oiProps[iProp])
		IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iColourChange > -1
			INT iTeam = MC_PlayerBD[iPartToUse].iteam
			INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
			IF iRule > -1 AND iRule < FMMC_MAX_RULES
				INT iColourToUse = -1
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iColourChangeOnRuleBS, iRule)
					iColourToUse = g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iColourChange
				ELSE 
					iColourToUse = g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropColour
				ENDIF
				IF iColourToUse > -1
					BOOL bReady = FALSE
					IF g_FMMC_STRUCT.iPropColourChangeDelay = 0
						bReady = TRUE
					ELSE
						IF NOT HAS_NET_TIMER_STARTED(tdPropColourChangeDelayTimer)
						AND GET_OBJECT_TINT_INDEX(oiProps[iProp]) != iColourToUse
							PRINTLN("[LM][PROCESS_PROP_EVERY_FRAME] - Starting the Prop Colour Change Delay via iProp: ", iProp)
							START_NET_TIMER(tdPropColourChangeDelayTimer)
						ENDIF
						
						IF HAS_NET_TIMER_STARTED(tdPropColourChangeDelayTimer)
						AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdPropColourChangeDelayTimer, (g_FMMC_STRUCT.iPropColourChangeDelay*1000))
							bReady = TRUE
						ENDIF
					ENDIF
					
					IF bReady = TRUE
						IF GET_OBJECT_TINT_INDEX(oiProps[iProp]) != iColourToUse
							PRINTLN("[LM][PROCESS_PROP_EVERY_FRAME] - Applying the Colour change ",iColourToUse," to iProp: ", iProp)
							SET_OBJECT_TINT_INDEX(oiProps[iProp], iColourToUse)
							RESET_NET_TIMER(tdPropColourChangeDelayTimer)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		MAINTAIN_PLACED_PROPS_PARTICLE_EFFECTS(iProp, oiProps[iProp])
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn =  IND_PROP_FIREWORK_01
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitSet2, ciFMMC_PROP2_UsePlacedFireworkZone)
			AND NOT IS_BIT_SET(iCreatedFireworkBitset[GET_LONG_BITSET_INDEX(iProp)], GET_LONG_BITSET_BIT(iProp))
				FLOAT fDist
				fDist = VDIST2(GET_ENTITY_COORDS(LocalPlayerPed), g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].vFireworkTriggerPos)
				
//				TEXT_LABEL_63 tlDebugText
//				tlDebugText = "fDist: "
//				tlDebugText += FLOAT_TO_STRING(fDist)
//				tlDebugText += " / "
//				tlDebugText += FLOAT_TO_STRING(POW(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].fFireworkTriggerSize, 2))
//				DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.5, 0.5, 0.5>>)
				
				IF fDist <= POW(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].fFireworkTriggerSize, 2)
				AND ((MC_playerBD[iLocalPart].iteam = g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iFireworkTeam) OR (g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iFireworkTeam = -1))
					IF CREATE_FIREWORK_FX(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAlarmDelay, oiProps[iProp], g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAlarmSound)
						SET_BIT(iCreatedFireworkBitset[GET_LONG_BITSET_INDEX(iProp)], GET_LONG_BITSET_BIT(iProp))
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_THIS_PROP_CLAIMABLE(INT iProp)
	BOOL bClaimable = TRUE
	IF MC_ServerBD_4.ePropClaimState[iProp] = ePropClaimingState_Disabled
	OR NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_Is_Claimable)
		bClaimable = FALSE
	ENDIF
	RETURN bClaimable
ENDFUNC

PROC CHANGE_PROP_CLAIMING_STATE(ePropClaimingState eNewClaimState, INT iProp, INT iTeam, INT iPart = -1, BOOL bBroadcast = TRUE)
	IF IS_THIS_PROP_CLAIMABLE(iProp)
		PRINTLN("[LM][TURF WAR][CHANGE_PROP_CLAIMING_STATE] - Changing State for iProp: ", iProp, " 	iTeam: ", iTeam)
		
		ePropClaimingState eOldClaimState = MC_ServerBD_4.ePropClaimState[iProp]
		UNUSED_PARAMETER(iTeam)	
		
		MC_ServerBD_4.iPropOwnedByPart[iProp] = iPart
		
		// OLD STATE PRINT
		SWITCH eOldClaimState
			CASE ePropClaimingState_Unclaimed
				PRINTLN("[LM][TURF WAR][CHANGE_PROP_CLAIMING_STATE] - Old State: ePropClaimingState_Unclaimed")
			BREAK		
			CASE ePropClaimingState_Claimed_Team_0
				PRINTLN("[LM][TURF WAR][CHANGE_PROP_CLAIMING_STATE] - Old State: ePropClaimingState_Claimed for Team: ", 0)
			BREAK
			CASE ePropClaimingState_Claimed_Team_1
				PRINTLN("[LM][TURF WAR][CHANGE_PROP_CLAIMING_STATE] - Old State: ePropClaimingState_Claimed for Team: ", 1)
			BREAK
			CASE ePropClaimingState_Claimed_Team_2
				PRINTLN("[LM][TURF WAR][CHANGE_PROP_CLAIMING_STATE] - Old State: ePropClaimingState_Claimed for Team: ", 2)
			BREAK
			CASE ePropClaimingState_Claimed_Team_3
				PRINTLN("[LM][TURF WAR][CHANGE_PROP_CLAIMING_STATE] - Old State: ePropClaimingState_Claimed for Team: ", 3)
			BREAK		
			CASE ePropClaimingState_Contested
				PRINTLN("[LM][TURF WAR][CHANGE_PROP_CLAIMING_STATE] - Old State: ePropClaimingState_Contested")
			BREAK
			CASE ePropClaimingState_Disabled
				PRINTLN("[LM][TURF WAR][CHANGE_PROP_CLAIMING_STATE] - Old State: ePropClaimingState_Disabled")
			BREAK
		ENDSWITCH
		
		// NEW STATE PRINT
		SWITCH eNewClaimState
			CASE ePropClaimingState_Unclaimed
				PRINTLN("[LM][TURF WAR][CHANGE_PROP_CLAIMING_STATE] - New State: ePropClaimingState_Unclaimed")
				MC_ServerBD_4.iPropOwnedByPart[iProp] = -1
			BREAK		
			CASE ePropClaimingState_Claimed_Team_0
				PRINTLN("[LM][TURF WAR][CHANGE_PROP_CLAIMING_STATE] - New State: ePropClaimingState_Claimed for Team: ", 0)
			BREAK
			CASE ePropClaimingState_Claimed_Team_1
				PRINTLN("[LM][TURF WAR][CHANGE_PROP_CLAIMING_STATE] - New State: ePropClaimingState_Claimed for Team: ", 1)
			BREAK
			CASE ePropClaimingState_Claimed_Team_2
				PRINTLN("[LM][TURF WAR][CHANGE_PROP_CLAIMING_STATE] - New State: ePropClaimingState_Claimed for Team: ", 2)
			BREAK
			CASE ePropClaimingState_Claimed_Team_3
				PRINTLN("[LM][TURF WAR][CHANGE_PROP_CLAIMING_STATE] - New State: ePropClaimingState_Claimed for Team: ", 3)
			BREAK		
			CASE  ePropClaimingState_Contested
				PRINTLN("[LM][TURF WAR][CHANGE_PROP_CLAIMING_STATE] - New State: ePropClaimingState_Contested")
				MC_ServerBD_4.iPropOwnedByPart[iProp] = -1
			BREAK
			CASE ePropClaimingState_Disabled
				PRINTLN("[LM][TURF WAR][CHANGE_PROP_CLAIMING_STATE] - New State: ePropClaimingState_Disabled")
				MC_ServerBD_4.iPropOwnedByPart[iProp] = -1
			BREAK
		ENDSWITCH	
		
		MC_ServerBD_4.ePropClaimState[iProp] = eNewClaimState
				
		IF bBroadcast
			SET_BIT(iLocalBoolCheck20, LBOOL20_TURFWAR_ICON_START_PULSE)			
		ENDIF
		
		IF iPart > -1
			MC_ServerBD_4.iPropAmountOwnedByPart[iPart]++
			BROADCAST_FMMC_PROP_CLAIMED_SUCCESSFULLY_BY_PART(iPart)
		ENDIF
	ELSE
		PRINTLN("[LM][TURF WAR][CHANGE_PROP_CLAIMING_STATE] - Should not have got this far, as this prop: ", iProp, "is not claimable (IS_THIS_PROP_CLAIMABLE) - Talk to Luke")
	ENDIF
ENDPROC

PROC RESET_TURF_WAR_UNCONTESTED_CONTESTED_PROPS(INT iProp)
	BOOL bReset = TRUE
	INT iCountPlayers
	INT iPart = 0
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, iPart))
		AND NOT IS_PARTICIPANT_A_SPECTATOR(iPart)
			IF MC_PlayerBD[iPart].iCurrentPropHit[0] = iProp
			OR MC_PlayerBD[iPart].iCurrentPropHit[1] = iProp
				IF iPart != iLocalPart
					PRINTLN("[LM][TURF WAR][RESET_TURF_WAR_UNCONTESTED_CONTESTED_PROPS] - iPart: ", iPart, "  We're currently on iProp: ", iProp)
					bReset = FALSE
					BREAKLOOP
				ENDIF
			ENDIF
			iCountPlayers++
		ENDIF
		IF iCountPlayers > (MC_ServerBD.iNumberOfPlayingPlayers[0] + MC_ServerBD.iNumberOfPlayingPlayers[1] + MC_ServerBD.iNumberOfPlayingPlayers[2] + MC_ServerBD.iNumberOfPlayingPlayers[3])
			PRINTLN("[LM][TURF WAR][RESET_TURF_WAR_UNCONTESTED_CONTESTED_PROPS] - iCountPlayers: ", iPart, "  Total Players: ", 
				(MC_ServerBD.iNumberOfPlayingPlayers[0] + MC_ServerBD.iNumberOfPlayingPlayers[1] + MC_ServerBD.iNumberOfPlayingPlayers[2] + MC_ServerBD.iNumberOfPlayingPlayers[3]))
			PRINTLN("[LM][TURF WAR][RESET_TURF_WAR_UNCONTESTED_CONTESTED_PROPS] - BREAKLOOP")
			BREAKLOOP
		ENDIF
	ENDFOR

	IF bReset		
		PRINTLN("[LM][TURF WAR][RESET_TURF_WAR_UNCONTESTED_CONTESTED_PROPS] - Broadcasting Reset Single prop. iProp: ", iProp)
		BROADCAST_FMMC_RESET_INDIVIDUAL_PROP(iProp)
	ELSE
		PRINTLN("[LM][TURF WAR][RESET_TURF_WAR_UNCONTESTED_CONTESTED_PROPS] - This prop is rightfully contested. iProp: ", iProp)
	ENDIF
ENDPROC

PROC SET_TURF_WAR_PROP_TO_COLOUR_UNCLAIMED(INT iProp)
	IF GET_OBJECT_TINT_INDEX(oiProps[iProp]) != GET_CHANGE_PROP_COLOUR_TURF_WAR(g_FMMC_STRUCT.iTurfWarColourUnclaimed)
		SET_OBJECT_TINT_INDEX(oiProps[iProp], GET_CHANGE_PROP_COLOUR_TURF_WAR(g_FMMC_STRUCT.iTurfWarColourUnclaimed))
		RESET_ENTITY_ALPHA(oiProps[iProp])
		PRINTLN("[LM][TURF WAR][SET_TURF_WAR_PROP_TO_COLOUR_UNCLAIMED] - Colour of iProp: ", iProp, " Should be Unclaimed.")
	ENDIF
ENDPROC

PROC SET_TURF_WAR_PROP_TO_COLOUR_TEAM_0(INT iProp)
	IF GET_OBJECT_TINT_INDEX(oiProps[iProp]) != GET_CHANGE_PROP_COLOUR_TURF_WAR(g_FMMC_STRUCT.iTurfWarColourTeam1)
		SET_OBJECT_TINT_INDEX(oiProps[iProp], GET_CHANGE_PROP_COLOUR_TURF_WAR(g_FMMC_STRUCT.iTurfWarColourTeam1))
		RESET_ENTITY_ALPHA(oiProps[iProp])
		PRINTLN("[LM][TURF WAR][SET_TURF_WAR_PROP_TO_COLOUR_TEAM_0] - Colour of iProp: ", iProp, " Should appear Claimed by Team: ", 0)
	ENDIF
ENDPROC

PROC SET_TURF_WAR_PROP_TO_COLOUR_TEAM_1(INT iProp)
	IF GET_OBJECT_TINT_INDEX(oiProps[iProp]) != GET_CHANGE_PROP_COLOUR_TURF_WAR(g_FMMC_STRUCT.iTurfWarColourTeam2)
		SET_OBJECT_TINT_INDEX(oiProps[iProp], GET_CHANGE_PROP_COLOUR_TURF_WAR(g_FMMC_STRUCT.iTurfWarColourTeam2))
		RESET_ENTITY_ALPHA(oiProps[iProp])
		PRINTLN("[LM][TURF WAR][SET_TURF_WAR_PROP_TO_COLOUR_TEAM_1] - Colour of iProp: ", iProp, " Should appear Claimed by Team: ", 1)
	ENDIF
ENDPROC

PROC SET_TURF_WAR_PROP_TO_COLOUR_TEAM_2(INT iProp)
	IF GET_OBJECT_TINT_INDEX(oiProps[iProp]) != GET_CHANGE_PROP_COLOUR_TURF_WAR(g_FMMC_STRUCT.iTurfWarColourTeam3)
		SET_OBJECT_TINT_INDEX(oiProps[iProp], GET_CHANGE_PROP_COLOUR_TURF_WAR(g_FMMC_STRUCT.iTurfWarColourTeam3))
		RESET_ENTITY_ALPHA(oiProps[iProp])
		PRINTLN("[LM][TURF WAR][SET_TURF_WAR_PROP_TO_COLOUR_TEAM_2] - Colour of iProp: ", iProp, " Should appear Claimed by Team: ", 2)
	ENDIF
ENDPROC

PROC SET_TURF_WAR_PROP_TO_COLOUR_TEAM_3(INT iProp)
	IF GET_OBJECT_TINT_INDEX(oiProps[iProp]) != GET_CHANGE_PROP_COLOUR_TURF_WAR(g_FMMC_STRUCT.iTurfWarColourTeam4)
		SET_OBJECT_TINT_INDEX(oiProps[iProp], GET_CHANGE_PROP_COLOUR_TURF_WAR(g_FMMC_STRUCT.iTurfWarColourTeam4))
		RESET_ENTITY_ALPHA(oiProps[iProp])
		PRINTLN("[LM][TURF WAR][SET_TURF_WAR_PROP_TO_COLOUR_TEAM_3] - Colour of iProp: ", iProp, " Should appear Claimed by Team: ", 3)
	ENDIF
ENDPROC

PROC SET_TURF_WAR_PROP_TO_COLOUR_CONTESTED(INT iProp)
	IF GET_OBJECT_TINT_INDEX(oiProps[iProp]) != GET_CHANGE_PROP_COLOUR_TURF_WAR(g_FMMC_STRUCT.iTurfWarColourContested)
		SET_OBJECT_TINT_INDEX(oiProps[iProp], GET_CHANGE_PROP_COLOUR_TURF_WAR(g_FMMC_STRUCT.iTurfWarColourContested))
		RESET_ENTITY_ALPHA(oiProps[iProp])
		PRINTLN("[LM][TURF WAR][SET_TURF_WAR_PROP_TO_COLOUR_CONTESTED] - Colour of iProp: ", iProp, " Should appear Contested")
	ENDIF	
ENDPROC

PROC SET_TURF_WAR_PROP_TO_COLOUR_DISABLED(INT iProp)
	IF GET_OBJECT_TINT_INDEX(oiProps[iProp]) != GET_CHANGE_PROP_COLOUR_TURF_WAR(g_FMMC_STRUCT.iTurfWarColourDisabled)
		SET_OBJECT_TINT_INDEX(oiProps[iProp], GET_CHANGE_PROP_COLOUR_TURF_WAR(g_FMMC_STRUCT.iTurfWarColourDisabled))
		RESET_ENTITY_ALPHA(oiProps[iProp])
		PRINTLN("[LM][TURF WAR][SET_TURF_WAR_PROP_TO_COLOUR_DISABLED] - Colour of iProp: ", iProp, " Should appear Disabled")
	ENDIF
ENDPROC

PROC HANDLE_PROP_CLAIMING_CLIENT_PREDICT_PVP(INT iProp, INT iNewState)
	IF INT_TO_ENUM(ePropClaimingState, iNewState) = ePropClaimingState_Claimed_Team_0
		SET_TURF_WAR_PROP_TO_COLOUR_TEAM_0(iProp)
	ELIF INT_TO_ENUM(ePropClaimingState, iNewState) = ePropClaimingState_Claimed_Team_1
		SET_TURF_WAR_PROP_TO_COLOUR_TEAM_1(iProp)
	ELIF INT_TO_ENUM(ePropClaimingState, iNewState) = ePropClaimingState_Claimed_Team_2
		SET_TURF_WAR_PROP_TO_COLOUR_TEAM_2(iProp)
	ELIF INT_TO_ENUM(ePropClaimingState, iNewState) = ePropClaimingState_Claimed_Team_3
		SET_TURF_WAR_PROP_TO_COLOUR_TEAM_3(iProp)
	ENDIF
ENDPROC

PROC HANDLE_PROP_CLAIMING_CLIENT(INT iProp)
	IF MC_ServerBD_4.ePropClaimState[iProp] = ePropClaimingState_Contested
		PRINTLN("[LM][HANDLE_PROP_CLAIMING_CLIENT] - Forcing iProp: ", iProp, " to appear Contested Colour.")
		SET_TURF_WAR_PROP_TO_COLOUR_CONTESTED(iProp)
	ENDIF
	
	IF MC_ServerBD_4.ePropClaimState[iProp] != ePropClaimingState_Contested
	AND GET_OBJECT_TINT_INDEX(oiProps[iProp]) = GET_CHANGE_PROP_COLOUR_TURF_WAR(g_FMMC_STRUCT.iTurfWarColourContested)
		PRINTLN("[LM][HANDLE_PROP_CLAIMING_CLIENT] - Forcing Prop to appear it's state based colour as it looked contested when it wasn't.")
		
		SWITCH 	MC_ServerBD_4.ePropClaimState[iProp]
			CASE ePropClaimingState_Unclaimed
				SET_TURF_WAR_PROP_TO_COLOUR_UNCLAIMED(iProp)
			BREAK
			CASE ePropClaimingState_Claimed_Team_0
				SET_TURF_WAR_PROP_TO_COLOUR_TEAM_0(iProp)
			BREAK
			CASE ePropClaimingState_Claimed_Team_1
				SET_TURF_WAR_PROP_TO_COLOUR_TEAM_1(iProp)
			BREAK
			CASE ePropClaimingState_Claimed_Team_2
				SET_TURF_WAR_PROP_TO_COLOUR_TEAM_2(iProp)
			BREAK
			CASE ePropClaimingState_Claimed_Team_3
				SET_TURF_WAR_PROP_TO_COLOUR_TEAM_3(iProp)
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC HANDLE_PROP_MARK_FOR_CORRECTION(INT iProp, ePropClaimingState eNewState)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_Marked_for_correction)
		PRINTLN("[LM][HANDLE_PROP_MARK_FOR_CORRECTION] - Correcting Prop: ", iProp)
		IF bIsLocalPlayerHost
			INT iPart = -1
			INT iTeam = ENUM_TO_INT(eNewState)
			iTeam -= 1
			
			IF iTeam >= 0
			OR iTeam <= 3
				iPart = GET_FIRST_PARTICIPANT_NUMBER_FROM_TEAM_NUMBER(iTeam)
			ENDIF
			
			PRINTLN("[LM][HANDLE_PROP_MARK_FOR_CORRECTION] - We are host so we want to correct the Prop State. Setting as claimed by iPart: ", iPart, " for team: ", iTeam)
			
			CHANGE_PROP_CLAIMING_STATE(eNewState, iProp, -1, iPart)			
		ELSE
			// Send out an event with the Colour of the Prop, and the State we're seeing. If the states are correct, but the Prop colour isn't, then the Host will send out an event for that
			// Participant to process in order to correct the Prop Colour.
			PRINTLN("[LM][HANDLE_PROP_MARK_FOR_CORRECTION] - We are a client so we want to correct the Prop Colour, but only if the States are the same, and the Colour does not match the host.")
			BROADCAST_FMMC_REQUEST_RIGHTFUL_PROP_COLOUR_FROM_HOST(iPartToUse, iProp, ENUM_TO_INT(MC_serverBD_4.ePropClaimState[iProp]), GET_OBJECT_TINT_INDEX(oiProps[iProp]))
		ENDIF
		CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_Marked_for_correction)
	ELSE
		PRINTLN("[LM][HANDLE_PROP_MARK_FOR_CORRECTION] - Marking Prop: ", iProp,"  for Correction.")
		SET_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_Marked_for_correction)
	ENDIF	
ENDPROC

PROC CORRECT_TURF_WAR_PROP_STATE(INT iProp)
	
	// Unclaimed
	IF GET_OBJECT_TINT_INDEX(oiProps[iProp]) = GET_CHANGE_PROP_COLOUR_TURF_WAR(g_FMMC_STRUCT.iTurfWarColourUnclaimed)
	AND MC_ServerBD_4.ePropClaimState[iProp] != ePropClaimingState_Unclaimed
		HANDLE_PROP_MARK_FOR_CORRECTION(iProp, ePropClaimingState_Unclaimed)
	ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_Marked_for_correction)
	AND GET_OBJECT_TINT_INDEX(oiProps[iProp]) = GET_CHANGE_PROP_COLOUR_TURF_WAR(g_FMMC_STRUCT.iTurfWarColourUnclaimed)
	AND MC_ServerBD_4.ePropClaimState[iProp] = ePropClaimingState_Unclaimed
		PRINTLN("[LM][HANDLE_PROP_MARK_FOR_CORRECTION] - Prop: ", iProp," Corrected itself already. Clearing Mark. (Unclaimed Branch)")
		CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_Marked_for_correction)
	ENDIF

	// Team 1
	IF GET_OBJECT_TINT_INDEX(oiProps[iProp]) = GET_CHANGE_PROP_COLOUR_TURF_WAR(g_FMMC_STRUCT.iTurfWarColourTeam1)
	AND MC_ServerBD_4.ePropClaimState[iProp] != ePropClaimingState_Claimed_Team_0
		HANDLE_PROP_MARK_FOR_CORRECTION(iProp, ePropClaimingState_Claimed_Team_0)
	ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_Marked_for_correction)
	AND GET_OBJECT_TINT_INDEX(oiProps[iProp]) = GET_CHANGE_PROP_COLOUR_TURF_WAR(g_FMMC_STRUCT.iTurfWarColourTeam1)
	AND MC_ServerBD_4.ePropClaimState[iProp] = ePropClaimingState_Claimed_Team_0
		PRINTLN("[LM][HANDLE_PROP_MARK_FOR_CORRECTION] - Prop: ", iProp," Corrected itself already. Clearing Mark. (Team 1 Branch)")
		CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_Marked_for_correction)
	ENDIF
	
	// Team 2
	IF GET_OBJECT_TINT_INDEX(oiProps[iProp]) = GET_CHANGE_PROP_COLOUR_TURF_WAR(g_FMMC_STRUCT.iTurfWarColourTeam2)
	AND MC_ServerBD_4.ePropClaimState[iProp] != ePropClaimingState_Claimed_Team_1
		HANDLE_PROP_MARK_FOR_CORRECTION(iProp, ePropClaimingState_Claimed_Team_1)
	ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_Marked_for_correction)
	AND GET_OBJECT_TINT_INDEX(oiProps[iProp]) = GET_CHANGE_PROP_COLOUR_TURF_WAR(g_FMMC_STRUCT.iTurfWarColourTeam2)
	AND MC_ServerBD_4.ePropClaimState[iProp] = ePropClaimingState_Claimed_Team_1
		PRINTLN("[LM][HANDLE_PROP_MARK_FOR_CORRECTION] - Prop: ", iProp," Corrected itself already. Clearing Mark. (Team 2 Branch)")
		CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_Marked_for_correction)
	ENDIF	
	
	// Team 3
	IF GET_OBJECT_TINT_INDEX(oiProps[iProp]) = GET_CHANGE_PROP_COLOUR_TURF_WAR(g_FMMC_STRUCT.iTurfWarColourTeam3)
	AND MC_ServerBD_4.ePropClaimState[iProp] != ePropClaimingState_Claimed_Team_2
		HANDLE_PROP_MARK_FOR_CORRECTION(iProp, ePropClaimingState_Claimed_Team_2)
	ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_Marked_for_correction)
	AND GET_OBJECT_TINT_INDEX(oiProps[iProp]) = GET_CHANGE_PROP_COLOUR_TURF_WAR(g_FMMC_STRUCT.iTurfWarColourTeam3)
	AND MC_ServerBD_4.ePropClaimState[iProp] = ePropClaimingState_Claimed_Team_2
		PRINTLN("[LM][HANDLE_PROP_MARK_FOR_CORRECTION] - Prop: ", iProp," Corrected itself already. Clearing Mark. (Team 3 Branch)")
		CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_Marked_for_correction)
	ENDIF
	
	// Team 4
	IF GET_OBJECT_TINT_INDEX(oiProps[iProp]) = GET_CHANGE_PROP_COLOUR_TURF_WAR(g_FMMC_STRUCT.iTurfWarColourTeam4)
	AND MC_ServerBD_4.ePropClaimState[iProp] != ePropClaimingState_Claimed_Team_3
		HANDLE_PROP_MARK_FOR_CORRECTION(iProp, ePropClaimingState_Claimed_Team_3)
	ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_Marked_for_correction)
	AND GET_OBJECT_TINT_INDEX(oiProps[iProp]) = GET_CHANGE_PROP_COLOUR_TURF_WAR(g_FMMC_STRUCT.iTurfWarColourTeam4)
	AND MC_ServerBD_4.ePropClaimState[iProp] = ePropClaimingState_Claimed_Team_3
		PRINTLN("[LM][HANDLE_PROP_MARK_FOR_CORRECTION] - Prop: ", iProp," Corrected itself already. Clearing Mark. (Team 4 Branch)")
		CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_Marked_for_correction)
	ENDIF
	
	// Contested
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_Marked_for_correction)
	AND GET_OBJECT_TINT_INDEX(oiProps[iProp]) = GET_CHANGE_PROP_COLOUR_TURF_WAR(g_FMMC_STRUCT.iTurfWarColourContested)
		SET_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_Marked_for_correction)
		PRINTLN("[LM][HANDLE_PROP_MARK_FOR_CORRECTION] - Prop: ", iProp," Is Contested. Marking to check next loop round. (Contested Branch)")
	ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_Marked_for_correction)
	AND GET_OBJECT_TINT_INDEX(oiProps[iProp]) = GET_CHANGE_PROP_COLOUR_TURF_WAR(g_FMMC_STRUCT.iTurfWarColourContested)	
		PRINTLN("[LM][HANDLE_PROP_MARK_FOR_CORRECTION] - Prop: ", iProp," Is Marked. We're going to check if this prop should be contested or not via (RESET_TURF_WAR_UNCONTESTED_CONTESTED_PROPS).. (Contested Branch)")
		CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_Marked_for_correction)
		RESET_TURF_WAR_UNCONTESTED_CONTESTED_PROPS(iProp)
	ENDIF
ENDPROC

PROC RESET_THIS_TEAMS_PROPS(INT iTeam)
	INT iNewState = ENUM_TO_INT(ePropClaimingState_Claimed_Team_0)
	iNewState += iTeam
	
	PRINTLN("[LM][RESET_THIS_TEAMS_PROPS] - Resetting the props on this team: ", iTeam)
	INT iProp = 0
	FOR iProp = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfProps-1
		IF MC_ServerBD_4.ePropClaimState[iProp] = INT_TO_ENUM(ePropClaimingState, iNewState)
			BROADCAST_FMMC_RESET_INDIVIDUAL_PROP(iProp)
		ENDIF
	ENDFOR
ENDPROC


//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: MAP PROP MODEL HIDES !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************

// Add Cleanup functions here that you would want to happen early when the Mission has ended.
PROC PROCESS_CLEANUP_PROPS_EARLY()

	// Likely do not want to affect Co-op missions.
	IF g_FMMC_STRUCT.iAdversaryModeType = 0
		EXIT
	ENDIF
	
	// Skycam has just transitioned to be fully in the air.
	IF GET_SKYSWOOP_STAGE() = SKYSWOOP_GOINGUP
		// Flares persisted in the air.
		IF iNumCreatedDynamicFlarePTFX > 0
		OR iNumCreatedFlarePTFX > 0
			PRINTLN("[RCC MISSION][PROCESS_CLEANUP_PROPS_EARLY][LM] Calling early prop cleanups.")
			CLEANUP_DYNAMIC_FLARES()
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: This function creates and maintains the behaviour of special props
PROC PROCESS_PROPS(INT iProp)
	
	// If this prop has an associated rule
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iprop ].iAssociatedObjective != -1
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iprop].iPropBitset, ciFMMC_PROP_AssociatedSpawn)
	OR IS_BIT_SET(iPropRespawnNowBS[GET_LONG_BITSET_INDEX(iProp)], GET_LONG_BITSET_BIT(iProp))
	
		IF NOT DOES_ENTITY_EXIST( oiProps[ iprop ] )
		AND SHOULD_PROP_SPAWN_NOW( iProp )
		AND NOT SHOULD_CLEANUP_PROP( iprop, NULL )
			// move spawning to every frame.
		ELIF DOES_ENTITY_EXIST( oiProps[ iprop ] )		
			// Do any special prop behaviours, but only if the prop exists
			PROCESS_PROP_BEHAVIOUR( iProp )
		ENDIF
	ELSE 
		NON_ASSOCIATED_OBJECTIVE_PROCESS_PROP_BEHAVIOUR(iProp)
	ENDIF
	
	// Creates a staggered loop that processes 3 props at a time. This because previously the correction would take 10 seconds to complete. (150 props / ci_TURF_WAR_STAGGERED_LOOP_COUNT * 30fps)
	// With this method it only takes ~3.3 seconds. * 2
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARS(g_FMMC_STRUCT.iAdversaryModeType)
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARZONE(g_FMMC_STRUCT.iAdversaryModeType)
		IF NOT HAS_NET_TIMER_STARTED(tdTurfWarStaggeredLoopTimerCap)
			START_NET_TIMER(tdTurfWarStaggeredLoopTimerCap)
		ENDIF
		
		IF HAS_NET_TIMER_STARTED(tdTurfWarStaggeredLoopTimerCap)
			IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdTurfWarStaggeredLoopTimerCap, ci_TURF_WAR_STAGGERED_LOOP_TIMER_CAP)
				INT iPropLoop
				INT i = 0
				FOR i = 0 TO ci_TURF_WAR_STAGGERED_LOOP_COUNT-1
					iPropLoop = (ci_TURF_WAR_STAGGERED_LOOP_COUNT*iTurfWarStaggeredLoopIterations)+i
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropLoop].iPropBitset, ciFMMC_PROP_Is_Claimable)
						IF iPropLoop > -1
						AND iPropLoop < g_FMMC_STRUCT_ENTITIES.iNumberOfProps-1
							IF DOES_ENTITY_EXIST(oiProps[iPropLoop])
								CORRECT_TURF_WAR_PROP_STATE(iPropLoop)
							ENDIF
						ENDIF
					ENDIF
					
					PRINTLN("[LM][LoopCheckLM] - iPropLoop: ", iPropLoop, "		iTurfWarStaggeredLoopIterations: ", iTurfWarStaggeredLoopIterations)					
				ENDFOR
				
				iTurfWarStaggeredLoopIterations++					
				IF iTurfWarStaggeredLoopIterations >= ci_TURF_WAR_STAGGERED_LOOP_ITERATIONS
					iTurfWarStaggeredLoopIterations = 0
					RESET_NET_TIMER(tdTurfWarStaggeredLoopTimerCap)
					START_NET_TIMER(tdTurfWarStaggeredLoopTimerCap)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF (NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType) //We handle this elsewhere in Drop the Bomb
	AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_SUMO_RUN(g_FMMC_STRUCT.iAdversaryModeType))
		IF DOES_ENTITY_EXIST(oiProps[iProp])
			IF IS_LOCAL_PLAYER_ANY_SPECTATOR() AND IS_BIT_SET(MC_serverBD_3.iPropDestroyedBS[GET_LONG_BITSET_INDEX(iProp)], GET_LONG_BITSET_BIT(iProp))
				SET_ENTITY_ALPHA(oiProps[iProp], 0, FALSE)
				SET_ENTITY_COLLISION(oiProps[iProp], FALSE)
				REMOVE_DECALS_FROM_OBJECT(oiProps[iProp])
				SET_OBJECT_AS_NO_LONGER_NEEDED(oiProps[iProp])
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(oiProps[iProp])
		VECTOR vPlayerCoords 
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
		ENDIF
		
		MAINTAIN_PLACED_AUDIO_TRIGGER_PROPS(iProp, vPlayerCoords)
	ENDIF
			
	//process monkeys every frame if one is created via g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iprop ].mn = HEI_PROP_HEIST_APECRATE 
	IF iMonkeyPedCount > 0
		MONKEY_SOUND_SYSTEM()
		MONKEY_ANIM_SYSTEM()
	ENDIF 
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropLodDist != -1
		IF DOES_ENTITY_EXIST(oiProps[iProp])
			IF GET_ENTITY_LOD_DIST(oiProps[iProp]) != g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropLodDist
				SET_ENTITY_LOD_DIST(oiProps[iProp], g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropLodDist)
				PRINTLN("PROCESS_PROPS - - - SET_ENTITY_LOD_DIST(oiProps[iProp], g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropLodDist) = ", g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropLodDist)
			ENDIF
		ENDIF
	ENDIF
	
	// Cleanup props
	IF DOES_ENTITY_EXIST( oiProps[ iprop ] )
		IF SHOULD_CLEANUP_PROP( iprop , oiProps[ iprop ] )
			PRINTLN("[PRP_BUG] SHOULD_CLEANUP_PROP, iprop = ", iprop)
			SET_BIT( iPropCleanedupBS[ GET_LONG_BITSET_INDEX( iprop ) ],GET_LONG_BITSET_BIT( iprop ) )
		ENDIF
	ENDIF
	
	IF IS_LOADED_MISSION_LOW_FLOW_MISSION(ciLOW_FLOW_MISSION_PHOTO_CARS)
		PRINTLN("Running Photo Cars Mission - checking for stair prop")
		IF NOT DOES_ENTITY_EXIST(oiStairCollision)
			MODEL_NAMES mn = INT_TO_ENUM(MODEL_NAMES, HASH("lr_prop_rail_col_01"))
			REQUEST_MODEL(mn)
			IF HAS_MODEL_LOADED(mn)
				oiStairCollision = CREATE_OBJECT(mn,<<766.35, -1319.77, 26.21>>,FALSE, FALSE, DEFAULT)
			ENDIF
		ENDIF
	ENDIF
		
ENDPROC



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: DYNOPROPS !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************

FUNC BOOL CLEANUP_DYNOPROP_EARLY(INT iProp, OBJECT_INDEX tempProp)

	IF SHOULD_CLEANUP_DYNOPROP( iProp, tempProp )
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iProp].iDynoPropBitset,ciFMMC_DYNOPROP_IgnoreVisCheckCleanup)
			IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niDynoProps[iProp])
				NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niDynoProps[iProp])
			ELSE
				DELETE_NET_ID(MC_serverBD_1.sFMMC_SBD.niDynoProps[iProp])
				PRINTLN("[RCC MISSION] DynoProp getting cleaned up early - delete, objective: ",g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iProp].iCleanupObjective," team: ",g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iProp].iCleanupTeam)
				RETURN TRUE
			ENDIF
		ELSE
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niDynoProps[iProp])
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niDynoProps[iProp]), FALSE)
			ENDIF
			CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niDynoProps[iProp])
			PRINTLN("[RCC MISSION] DynoProp getting cleaned up early - no longer needed, objective: ",g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iProp].iCleanupObjective," team: ",g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iProp].iCleanupTeam)
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE

ENDFUNC

/// PURPOSE: This function creates and maintains the behaviour of special props
PROC PROCESS_DYNOPROP_BRAIN( INT iDynoprop ) 
	 
	IF bIsLocalPlayerHost
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niDynoProps[iDynoprop])
			IF CLEANUP_DYNOPROP_EARLY( iDynoprop , NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niDynoProps[iDynoprop]) )
				PRINTLN("[PRP_BUG] SHOULD_CLEANUP_DYNOPROP, iDynoprop = ", iDynoprop)
			ENDIF
			IF IS_BIT_SET(MC_serverBD.iDynoPropCleanup_NeedOwnershipBS, iDynoprop)
				IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niDynoProps[iDynoprop])
					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niDynoProps[iDynoprop])
				ELSE
					IF (IS_ANY_TEAM_BEING_WARPED_TO_START_POINT()
					AND IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_ALL_PLAYERS_RENDERPHASES_PAUSED))
					OR NOT IS_ANY_TEAM_BEING_WARPED_TO_START_POINT()
						PRINTLN("[RCC MISSION] DynoProp getting cleaned up")
						DELETE_NET_ID(MC_serverBD_1.sFMMC_SBD.niDynoProps[iDynoprop])
					ENDIF
				ENDIF
				CLEAR_BIT(MC_serverBD.iDynoPropCleanup_NeedOwnershipBS, iDynoprop)
			ENDIF
		ENDIF
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niDynoProps[iDynoprop])
		
			INT iTeam
			
			FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
				INT iRule = MC_serverBD_4.iCurrentHighestPriority[ iTeam ]
				
				IF iRule < FMMC_MAX_RULES
					
					IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niDynoProps[iDynoprop])
						IF SHOULD_DYNOPROP_SPAWN_NOW( iDynoprop ) 
						AND NOT SHOULD_CLEANUP_DYNOPROP( iDynoprop, NULL )
						AND ( g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].mn != DUMMY_MODEL_FOR_SCRIPT )
						AND g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].mn != Prop_Container_LD_PU
						AND g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].iObjReference = -1
							REQUEST_MODEL( g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].mn )
							
							IF HAS_MODEL_LOADED( g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].mn )
								IF CAN_REGISTER_MISSION_OBJECTS(1)
									IF CREATE_NET_OBJ( MC_serverBD_1.sFMMC_SBD.niDynoProps[iDynoprop], 
													   g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].mn, 
													   g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].vPos, 
													   TRUE, 
													   TRUE)
									
										SET_UP_FMMC_DYNOPROP_MP_RC(NET_TO_OBJ( MC_serverBD_1.sFMMC_SBD.niDynoProps[iDynoprop] ), 
																    iDynoprop, 
																    g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].mn, 
																	TRUE)
										#IF IS_DEBUG_BUILD
											PRINTLN( "[PRP_BUG] SHOULD_SPAWN_DYNOPROP, iDynoprop = ", iDynoprop )
										#ENDIF
										IF IS_BIT_SET(MC_serverBD.iDynoPropShouldRespawnNowBS, iDynoprop)
											CLEAR_BIT(MC_serverBD.iDynoPropShouldRespawnNowBS, iDynoprop)
										ENDIF
										SET_MODEL_AS_NO_LONGER_NEEDED( g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].mn )
					
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
	ENDIF
	
ENDPROC

PROC SET_DYNOPROP_BLIP_COLOUR(INT iDynopropIndex, INT iDefaultColour)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynopropIndex].sDynoPropBlipStruct.iBlipColour != BLIP_COLOUR_DEFAULT
		iDefaultColour = GET_BLIP_COLOUR_FROM_CREATOR(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynopropIndex].sDynoPropBlipStruct.iBlipColour)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynopropIndex].sDynoPropElectronicData.sCCTVData.iCCTVBS, ciCCTV_BS_OverrideBlipColour)
		IF DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynopropIndex].sDynoPropElectronicData.sCCTVData)
			iDefaultColour = BLIP_COLOUR_WHITE
		ELSE
			iDefaultColour = BLIP_COLOUR_RED
		ENDIF
	ENDIF
	
	IF DOES_BLIP_EXIST(biDynoPropBlips[iDynopropIndex])
		SET_BLIP_COLOUR(biDynoPropBlips[iDynopropIndex], iDefaultColour)
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_DYNOPROP_BE_BLIPPED(INT iDynoProp, OBJECT_INDEX oiEntity)
	
	IF NOT DOES_ENTITY_EXIST(oiEntity)
		#IF IS_DEBUG_BUILD
		IF bDynoPropSpam
			PRINTLN("SHOULD_DYNOPROP_BE_BLIPPED - iDynoProp: ", iDynoProp, " Doesn't exist")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iDynopropBitset, ciFMMC_DYNOPROP_ShowBlip)
		#IF IS_DEBUG_BUILD
		IF bDynoPropSpam
			PRINTLN("SHOULD_DYNOPROP_BE_BLIPPED - iDynoProp: ", iDynoProp, " Shouldn't be blipped")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_ENTITY_DEAD(oiEntity)
		#IF IS_DEBUG_BUILD
		IF bDynoPropSpam
			PRINTLN("SHOULD_DYNOPROP_BE_BLIPPED - iDynoProp: ", iDynoProp, " Is dead")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_OBJECT_BROKEN_AND_VISIBLE(oiEntity)
		#IF IS_DEBUG_BUILD
		IF bDynoPropSpam
			PRINTLN("SHOULD_DYNOPROP_BE_BLIPPED - iDynoProp: ", iDynoProp, " Is broken")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_OBJECT_BROKEN_AND_VISIBLE(oiEntity, TRUE)
		#IF IS_DEBUG_BUILD
		IF bDynoPropSpam
			PRINTLN("SHOULD_DYNOPROP_BE_BLIPPED - iDynoProp: ", iDynoProp, " Is broken (networked)")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_THIS_MODEL_A_CCTV_CAMERA(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].mn)
		IF IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_0)
		OR IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_1)
		OR IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_2)
		OR IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_3)
			#IF IS_DEBUG_BUILD
			IF bDynoPropSpam
				PRINTLN("SHOULD_DYNOPROP_BE_BLIPPED - iDynoProp: ", iDynoProp, " Is a CCTV camera and a player has already been spotted - removing blip now")
			ENDIF
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].sDynoPropBlipStruct.fBlipRange > 0
		IF DOES_ENTITY_EXIST(oiEntity)
			FLOAT fPlayerObjDist = VDIST(GET_ENTITY_COORDS(LocalPlayerPed), GET_ENTITY_COORDS(oiEntity))
			IF fPlayerObjDist > g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].sDynoPropBlipStruct.fBlipRange
				PRINTLN("SHOULD_DYNOPROP_BE_BLIPPED - Blip range (2) iDynoProp: ", iDynoProp)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].sDynoPropBlipStruct.fBlipHeightDifference > 0.0
		IF DOES_ENTITY_EXIST(oiEntity)
			IF NOT IS_PED_INJURED(LocalPlayerPed)
				VECTOR vPlayerPos = GET_ENTITY_COORDS(LocalPlayerPed)
				VECTOR vObjPos = GET_ENTITY_COORDS(oiEntity)
				
				IF NOT IS_VECTOR_ZERO(vPlayerPos)
				AND NOT IS_VECTOR_ZERO(vObjPos)
					FLOAT fZDifference = ABSF(vPlayerPos.z - vObjPos.z)
					IF (GET_FRAME_COUNT() % 15) = 0
						PRINTLN("SHOULD_DYNOPROP_BE_BLIPPED - Dyno Prop: ",iDynoProp," fZDifference: ", fZDifference, " g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].sDynoPropBlipStruct.fBlipHeightDifference: ", g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].sDynoPropBlipStruct.fBlipHeightDifference)
					ENDIF
					IF fZDifference > g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].sDynoPropBlipStruct.fBlipHeightDifference
						PRINTLN("SHOULD_DYNOPROP_BE_BLIPPED - Blip height difference")
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_CASINO_HEIST_MISSION_CONTROLLER_BLIPS_HIDDEN)
		PRINTLN("SHOULD_DYNOPROP_BE_BLIPPED - Dyno Prop: ",iDynoProp, " - False - LBOOL32_CASINO_HEIST_MISSION_CONTROLLER_BLIPS_HIDDEN is set.")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC REMOVE_DYNOPROP_BLIP(INT iDynoProp)
	IF DOES_BLIP_EXIST(biDynoPropBlips[iDynoProp])
		REMOVE_CCTV_VISION_CONE(biDynoPropBlips[iDynoProp], iDynoProp, FMMC_ELECTRONIC_ENTITY_TYPE__DYNOPROP)
		REMOVE_BLIP(biDynoPropBlips[iDynoProp])
		PRINTLN("[Dynoprops] REMOVE_DYNOPROP_BLIP | Removing blip for dynoprop ", iDynoProp)
	ENDIF
ENDPROC

PROC PROCESS_DYNOPROP_BLIP(INT iDynoProp, OBJECT_INDEX oiEntity)
	
	IF NOT SHOULD_DYNOPROP_BE_BLIPPED(iDynoProp, oiEntity)
		REMOVE_DYNOPROP_BLIP(iDynoProp)
		EXIT
	ENDIF
	
	IF NOT DOES_BLIP_EXIST(biDynoPropBlips[iDynoProp])
		biDynoPropBlips[iDynoProp] = ADD_BLIP_FOR_ENTITY(oiEntity)
		PRINTLN("[Dynoprops] PROCESS_DYNOPROP_BLIP | Adding blip for dynoprop ", iDynoProp)
		SET_BLIP_SPRITE(biDynoPropBlips[iDynoProp], GET_BLIP_SPRITE_FROM_BLIP_SPRITE_OVERRIDE(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].sDynoPropBlipStruct.iBlipSpriteOverride))
		SET_DYNOPROP_BLIP_COLOUR(iDynoProp, BLIP_COLOUR_WHITE)
		SET_BLIP_MARKER_LONG_DISTANCE(biDynoPropBlips[iDynoProp], FALSE)
		SET_BLIP_AS_SHORT_RANGE(biDynoPropBlips[iDynoProp], TRUE)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].sDynoPropBlipStruct.fBlipScale != 1.0
			SET_BLIP_SCALE(biDynoPropBlips[iDynoProp], g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].sDynoPropBlipStruct.fBlipScale)
		ENDIF
		IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].sDynoPropBlipStruct.iBlipPriority != 0
			SET_BLIP_PRIORITY(biDynoPropBlips[iDynoProp], INT_TO_ENUM(BLIP_PRIORITY, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].sDynoPropBlipStruct.iBlipPriority))
			PRINTLN("PROCESS_DYNOPROP_BLIP - Blip priority set to ", g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].sDynoPropBlipStruct.iBlipPriority)
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		IF bDynoPropSpam
			PRINTLN("DYNOPROP MEGA SPAM - iDynoProp: ", iDynoProp, " is blipped right now")
		ENDIF
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE: This function creates and maintains the behaviour of special props
PROC PROCESS_DYNOPROP_BODY(INT iDynoprop, BOOL bProcessTraps)
	
	PROCESS_PARTICLE_FX_OVERRIDE_DYNOPROP(iDynoprop)
	
	NETWORK_INDEX niDynoProp = MC_serverBD_1.sFMMC_SBD.niDynoProps[iDynoprop]
	OBJECT_INDEX objDynoProp = NULL
	
	#IF IS_DEBUG_BUILD
	IF bDynoPropSpam
		PRINTLN("PROCESS_DYNOPROP_BODY - iDynoprop: ", iDynoprop, " being processed")
	ENDIF
	#ENDIF
	
	IF NETWORK_DOES_NETWORK_ID_EXIST( niDynoProp )
		
		objDynoProp = NET_TO_OBJ(niDynoProp)
		
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID( niDynoProp )
			
			MODEL_NAMES mnObject = GET_ENTITY_MODEL( objDynoProp )
			
			INT iPlayerIndex = GET_NEAREST_PLAYER_TO_ENTITY( objDynoProp )			
			PLAYER_INDEX player = INT_TO_PLAYERINDEX( iPlayerIndex )
			PED_INDEX ped = GET_PLAYER_PED( player )
			
			VECTOR vObjectCoords = GET_ENTITY_COORDS( objDynoProp, FALSE )
			
			IF DOES_MODEL_EXPLODE(mnObject)
				IF IS_ENTITY_ALIVE( objDynoProp )
					IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[ iDynoprop ].iDynopropBitset, ciFMMC_DYNOPROP_ExplodeOnTouch )
						IF IS_NET_PLAYER_OK( player )
						AND IS_PED_IN_ANY_VEHICLE( ped )
							IF IS_ENTITY_TOUCHING_ENTITY( objDynoProp, ped )
								// This is temporary as it is not networked 
								SET_ENTITY_INVINCIBLE( objDynoProp, FALSE )
								SET_ENTITY_PROOFS( objDynoProp, FALSE, FALSE, FALSE, FALSE, FALSE )
								ADD_EXPLOSION(vObjectCoords, EXP_TAG_BARREL, 2.0)
							ENDIF		
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].iDynopropBitset, ciFMMC_DYNOPROP_KamakaziInvincibility)
				INT iPlayer = GET_NEAREST_PLAYER_TO_ENTITY_ON_TEAM(objDynoProp, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].iPropKamaTeam)
				PLAYER_INDEX ClosestPlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPlayer))
				PED_INDEX ClosestPlayerPedId = GET_PLAYER_PED(ClosestPlayerId)
				IF VDIST2(GET_ENTITY_COORDS(objDynoProp),GET_ENTITY_COORDS(ClosestPlayerPedId)) < 40
					SET_ENTITY_INVINCIBLE(objDynoProp, FALSE)
					SET_ENTITY_PROOFS( objDynoProp, FALSE, FALSE, FALSE, FALSE, FALSE )
					SET_DISABLE_FRAG_DAMAGE(objDynoProp, FALSE)
					DRAW_DEBUG_LINE(GET_ENTITY_COORDS(objDynoProp),GET_ENTITY_COORDS(ClosestPlayerPedId), 255,0,0)
				ELSE
					SET_ENTITY_INVINCIBLE(objDynoProp, TRUE)
					SET_ENTITY_PROOFS( objDynoProp, TRUE, TRUE, TRUE, TRUE, TRUE )
					SET_DISABLE_FRAG_DAMAGE(objDynoProp, TRUE)
					DRAW_DEBUG_LINE(GET_ENTITY_COORDS(objDynoProp),GET_ENTITY_COORDS(ClosestPlayerPedId))
				ENDIF
			ENDIF
			
			IF IS_ENTITY_ALIVE( objDynoProp )
				IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[ iDynoprop ].iDynopropBitset, ciFMMC_DYNOPROP_DestroyInOneHit )
					IF IS_NET_PLAYER_OK( player )
						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(objDynoProp, ped )
							// This is temporary as it is not networked 
							SET_ENTITY_INVINCIBLE(objDynoProp, FALSE)
							SET_ENTITY_PROOFS(objDynoProp, FALSE, FALSE, FALSE, FALSE, FALSE)
							
							FLOAT fExplosionScaler = 1.0
							
							IF mnObject = INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_groupbarrel_01"))
							OR mnObject = INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_groupbarrel_02"))
							OR mnObject = INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_groupbarrel_03"))
								fExplosionScaler = 2.0
							ENDIF
							
							IF DOES_MODEL_EXPLODE(mnObject)
								ADD_EXPLOSION(vObjectCoords, EXP_TAG_BARREL, 0.5 * fExplosionScaler)
							ENDIF
						ENDIF		
					ENDIF
				ENDIF
			ENDIF
			
			IF bIsLocalPlayerHost
				IF IS_MODEL_SPECIAL_RACES_DESTRUCTIBLE_PROP(mnObject)
					IF CHECK_ALL_PLAYERS_EXCEED_DISTANCE_FROM_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].vPos, ciSPECIAL_RACES_DESTRUCTIBLE_PROP_RESPAWN_DISTANCE)
						IF IS_OBJECT_BROKEN_AND_VISIBLE(objDynoProp)
							DELETE_NET_ID(niDynoProp)
							CLEAR_AREA(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].vPos, 10.0, FALSE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//Stingers
			IF mnObject = p_ld_stinger_s
			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niDynoProp)
				MAINTAIN_PLACED_STINGER(iDynoProp)
			ENDIF
			
			IF IS_THIS_A_CAPTURE()
			OR IS_THIS_A_LTS()
				PROCESS_UGC_SEA_MINES(objDynoProp, iDynoProp, TRUE, iSeaMine_HasExploded)
			ENDIF
		ENDIF
		
		PROCESS_TASERED_ENTITY(sDynoPropTaserVars, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].sDynoPropElectronicData, objDynoProp, iDynoProp, FMMC_ELECTRONIC_ENTITY_TYPE__DYNOPROP)
		
		// CCTV
		IF IS_THIS_MODEL_A_CCTV_CAMERA(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].mn)
			PROCESS_CCTV_MOVING_CAMERAS(objDynoProp, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].sDynoPropElectronicData.sCCTVData, iDynoProp, iDynoPropCachedCamIndexes[iDynoProp], FMMC_ELECTRONIC_ENTITY_TYPE__DYNOPROP)
		ENDIF
	
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bDynoPropSpam
		IF NETWORK_DOES_NETWORK_ID_EXIST(niDynoProp)
			PRINTLN("PROCESS_DYNOPROP_BODY - Has control of ", iDynoprop, "? ", NETWORK_HAS_CONTROL_OF_NETWORK_ID( niDynoProp ))
		ENDIF
	ENDIF
	#ENDIF
	
	PROCESS_DYNOPROP_BLIP(iDynoProp, objDynoProp)
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(niDynoProp)
		objDynoProp = NET_TO_OBJ(niDynoProp)
		
		IF bProcessTraps
			PROCESS_ARENA_TRAPS(objDynoProp, iDynoProp, TRUE, MC_serverBD_2.sTrapInfo_Host, sTrapInfo_Local)
		ENDIF
		SET_BIT(g_iCurrentSpectatorTrapSpawned, iDynoProp)
	ELSE	
		IF IS_BIT_SET(g_iCurrentSpectatorTrapSpawned, iDynoProp)
			CLEANUP_TRAP(iDynoProp, MC_serverBD_2.sTrapInfo_Host, sTrapInfo_Local)
			CLEAR_BIT(g_iCurrentSpectatorTrapSpawned, iDynoProp)
		ENDIF
	ENDIF
	
	PROCESS_REMOVING_TARGETTABLE_FROM_DYNO_PROP(objDynoProp)
	
	PROCESS_TRAP_RESPAWNING(iDynoProp, MC_serverBD_1.sFMMC_SBD.niDynoProps, sTrapInfo_Local, MC_serverBD_2.sTrapInfo_Host)
	
	IF bIsLocalPlayerHost
		
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niDynoProp)
			IF IS_MODEL_SPECIAL_RACES_DESTRUCTIBLE_PROP(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].mn)
				IF CHECK_ALL_PLAYERS_EXCEED_DISTANCE_FROM_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].vPos, ciSPECIAL_RACES_DESTRUCTIBLE_PROP_RESPAWN_DISTANCE)
					REQUEST_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].mn)
					
					IF HAS_MODEL_LOADED( g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].mn )
						IF CREATE_NET_OBJ(MC_serverBD_1.sFMMC_SBD.niDynoProps[iDynoprop], g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].mn, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].vPos, TRUE, TRUE)
							SET_UP_FMMC_DYNOPROP_MP_RC(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niDynoProps[iDynoprop]), iDynoprop, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].mn, TRUE)
							
							SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].mn)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_CASINO_TUNNEL_RUBBLE_PROP()
	
	#IF IS_DEBUG_BUILD
	IF bCasinoTunnelRubbleDebug
		IF DOES_ENTITY_EXIST(oiCasinoTunnelDebris)
			DRAW_DEBUG_TEXT_ABOVE_ENTITY(oiCasinoTunnelDebris, "oiCasinoTunnelDebris", 0.1)
			DRAW_DEBUG_TEXT_2D("oiCasinoTunnelDebris exists", <<0.35, 0.35, 0.35>>)
		ELSE
			DRAW_DEBUG_TEXT_2D("oiCasinoTunnelDebris does not exist", <<0.35, 0.35, 0.35>>)
		ENDIF
	ENDIF
	#ENDIF
	
	VECTOR vRubbleCoords = <<2481.6, -296.3, -70.2>>
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_TUNNEL)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_BACK_AREA)
		EXIT
	ENDIF
	
	IF NOT IS_THIS_CASINO_HEIST_MISSION_A_DIRECT_APPROACH_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
		PRINTLN("[CasinoTunnelRubble] PROCESS_CASINO_TUNNEL_RUBBLE_PROP | This isn't Direct")
		EXIT
	ENDIF
	
	IF VDIST2(GET_ENTITY_COORDS(LocalPlayerPed), vRubbleCoords) > 1400.0
		PRINTLN("[CasinoTunnelRubble] PROCESS_CASINO_TUNNEL_RUBBLE_PROP | Too far from tunnel")
		EXIT
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(oiCasinoTunnelDebris)
		MODEL_NAMES mnRubbleModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_des_heist3_tunnel_end"))
		oiCasinoTunnelDebris = GET_CLOSEST_OBJECT_OF_TYPE(<<2481.6, -296.3, -70.2>>, 50.0, mnRubbleModel, FALSE, FALSE, FALSE)
		PRINTLN("[CasinoTunnelRubble] PROCESS_CASINO_TUNNEL_RUBBLE_PROP | Grabbing oiCasinoTunnelDebris")
	ENDIF
	
	BOOL bHide = IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_CASINO_HIDE_COLLAPSED_TUNNEL) AND NOT IS_CUTSCENE_PLAYING()
	
	IF DOES_ENTITY_EXIST(oiCasinoTunnelDebris)
		IF bHide
			IF IS_ENTITY_VISIBLE(oiCasinoTunnelDebris)
				SET_ENTITY_VISIBLE(oiCasinoTunnelDebris, FALSE)
				SET_ENTITY_COMPLETELY_DISABLE_COLLISION(oiCasinoTunnelDebris, FALSE)
				PRINTLN("[CasinoTunnelRubble] PROCESS_CASINO_TUNNEL_RUBBLE_PROP | Hiding oiCasinoTunnelDebris now!")
			ENDIF
		ELSE
			IF NOT IS_ENTITY_VISIBLE(oiCasinoTunnelDebris)
				SET_ENTITY_VISIBLE(oiCasinoTunnelDebris, TRUE)
				SET_ENTITY_COMPLETELY_DISABLE_COLLISION(oiCasinoTunnelDebris, TRUE)
				SET_OBJECT_AS_NO_LONGER_NEEDED(oiCasinoTunnelDebris)
				PRINTLN("[CasinoTunnelRubble] PROCESS_CASINO_TUNNEL_RUBBLE_PROP | Making oiCasinoTunnelDebris visible now!")
			ENDIF
		ENDIF
	ELSE
		PRINTLN("[CasinoTunnelRubble_SPAM] PROCESS_CASINO_TUNNEL_RUBBLE_PROP | No oiCasinoTunnelDebris was found")
	ENDIF
ENDPROC
