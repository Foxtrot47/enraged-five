
USING "FM_Mission_Controller_USING.sch"
USING "FM_Mission_Controller_Utility.sch"
USING "FM_Mission_Controller_Entities.sch"
USING "FM_Mission_Controller_ObjectiveCompletion.sch"
USING "FM_Mission_Controller_SpecialCase.sch"
USING "FM_Mission_Controller_Rules.sch"
USING "FM_Mission_Controller_Doors.sch"
USING "FM_Mission_Controller_Cutscene.sch"
USING "FM_Mission_Controller_Debug.sch"
USING "FM_Mission_Controller_Locations.sch"
USING "FM_Mission_Controller_Vehicles.sch"
USING "FM_Mission_Controller_Peds_Brain.sch"
USING "FM_Mission_Controller_Peds_Body.sch"
USING "FM_Mission_Controller_Peds.sch"
USING "FM_Mission_Controller_GangChase.sch"
USING "FM_Mission_Controller_Minigame.sch"
USING "FM_Mission_Controller_Objects.sch"
USING "FM_Mission_Controller_Props.sch"
USING "FM_Mission_Controller_Audio.sch"
USING "FM_Mission_Controller_HUD.sch"
USING "FM_Mission_Controller_Players.sch"
USING "FM_Mission_Controller_Zones.sch"
USING "FM_Mission_Controller_Bounds.sch"



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: DAMAGE EVENT PROCESSING !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************



#IF IS_DEBUG_BUILD
PROC PRINT_DAMAGED_EVENT_DATA(STRUCT_ENTITY_DAMAGE_EVENT &sEntityID)

	PRINTLN("[RCC MISSION]  - Mission Controller - event vars - sEntityID.VictimIndex = ",NATIVE_TO_INT(sEntityID.VictimIndex))
	PRINTLN("[RCC MISSION]  - Mission Controller - event vars - sEntityID.DamagerIndex = ",NATIVE_TO_INT(sEntityID.DamagerIndex))
	PRINTLN("[RCC MISSION]  - Mission Controller - event vars - sEntityID.Damage = ",sEntityID.Damage)
	PRINTLN("[RCC MISSION] - Mission Controller - event vars - sEntityID.VictimDestroyed = ")
	IF sEntityID.VictimDestroyed
		PRINTLN("[RCC MISSION] TRUE")
	ELSE
		PRINTLN("[RCC MISSION] FALSE")
	ENDIF
	PRINTLN("[RCC MISSION] - Mission Controller - event vars - sEntityID.WeaponUsed = ",sEntityID.WeaponUsed)
	PRINTLN("[RCC MISSION] - Mission Controller - event vars - sEntityID.VictimSpeed = ",sEntityID.VictimSpeed)
	PRINTLN("[RCC MISSION] - Mission Controller - event vars - sEntityID.DamagerSpeed = ",sEntityID.DamagerSpeed)
	
ENDPROC
#ENDIF

PROC PRIVATE_PROCESS_ENTITY_SUICIDE_EVENT(PLAYER_INDEX VictimPlayerID, INT iVictimPart, INT iKillerPart = -1)
	PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_SUICIDE_EVENT - Victim: ",iVictimPart, " Killer: ", iKillerPart) 
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciCLOBBER_PLAYER_ON_TEAM_SWAP)
	AND LocalPlayer = VictimPlayerID 
	AND iKillerPart = -1
		PRINTLN("[RCC MISSION][MMacK][ServerSwitch] SUICIDE SQUAD")
		IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam] < FMMC_MAX_RULES
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iDontSwitchOnSuicide[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciDYNAMIC_MENU_DONT_SWITCH_ON_SUICIDE)
				PLAYER_INDEX piTempKiller = GET_PLAYER_FROM_LOSING_TEAM_FOR_SWAP()
				
				INT iNewTeam = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iSwapToTeamOnDeath[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]]	
				
				IF iNewTeam != -1
				AND iNewTeam != MC_playerBD[iPartToUse].iteam
				AND VictimPlayerID != piTempKiller
					IF bIsLocalPlayerHost
						PRINTLN("[RCC MISSION][MMacK][ServerSwitch] Processing Inside Damage Event")
						PROCESS_PLAYER_TEAM_SWITCH(iVictimPart, piTempKiller, TRUE)
					ELSE
						BROADCAST_FMMC_PLAYER_TEAM_SWITCH_REQUEST(MC_ServerBD.iSessionScriptEventKey, MC_playerBD[iPartToUse].iteam, iNewTeam, iVictimPart, piTempKiller, TRUE)
						PRINTLN("[RCC MISSION][MMacK][ServerSwitch] Broadcasting Request")
					ENDIF
					
					MC_PlayerBD[iLocalPart].bSwapTeamStarted = TRUE
					REINIT_NET_TIMER(tdTeamSwapFailSafe)
				ELSE
					IF VictimPlayerID = piTempKiller
						PRINTLN("[RCC MISSION][MMacK][ServerSwitch] NO VALID PLAYER TO SWAP WITH")
					ELSE
						PRINTLN("[RCC MISSION][MMacK][ServerSwitch] SWAP IS NOT VALID", iNewTeam)
					ENDIF
				ENDIF
				iPartLastDamager = -1
			ENDIF
		ENDIF
	ENDIF
	PROCESS_CONDEMNED_KILL(iVictimPart, -1)
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRSHOOTOUT(g_FMMC_STRUCT.iAdversaryModeType)
	
		INT iOtherTeam = 0
		IF MC_playerBD[iVictimPart].iteam = 0
			iOtherTeam = 1
		ENDIF
		
		IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam] < FMMC_MAX_RULES
			IF bIsLocalPlayerHost
				
				INT igamestage = MC_serverBD_4.iPlayerRuleMissionLogic[iOtherTeam]
				PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_SUICIDE_EVENT - GET_MC_CLIENT_MISSION_STAGE: ",igamestage) 
				PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_SUICIDE_EVENT - host adding to team kills ") 
				IF igamestage = CLIENT_MISSION_STAGE_KILL_PLAYERS
					MC_serverBD.iTeamPlayerKills[iOtherTeam][MC_playerBD[iVictimpart].iteam]++
				ELIF igamestage = CLIENT_MISSION_STAGE_KILL_TEAM0
					IF MC_playerBD[iVictimpart].iteam = 0
						MC_serverBD.iTeamPlayerKills[iOtherTeam][MC_playerBD[iVictimpart].iteam]++
						PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_SUICIDE_EVENT - MC_serverBD.iTeamPlayerKills of team 0 increased for team: ",iOtherTeam) 
					ENDIF
				ELIF igamestage = CLIENT_MISSION_STAGE_KILL_TEAM1
					IF MC_playerBD[iVictimpart].iteam = 1
						MC_serverBD.iTeamPlayerKills[iOtherTeam][MC_playerBD[iVictimpart].iteam]++
						PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_SUICIDE_EVENT - MC_serverBD.iTeamPlayerKills of team 1 increased for team: ",iOtherTeam)
					ENDIF
				ELIF igamestage = CLIENT_MISSION_STAGE_KILL_TEAM2
					IF MC_playerBD[iVictimpart].iteam = 2
						MC_serverBD.iTeamPlayerKills[iOtherTeam][MC_playerBD[iVictimpart].iteam]++
						PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_SUICIDE_EVENT - MC_serverBD.iTeamPlayerKills of team 2 increased for team: ",iOtherTeam)
					ENDIF
				ELIF igamestage = CLIENT_MISSION_STAGE_KILL_TEAM3
					IF MC_playerBD[iVictimpart].iteam = 3
						MC_serverBD.iTeamPlayerKills[iOtherTeam][MC_playerBD[iVictimpart].iteam]++
						PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_SUICIDE_EVENT - MC_serverBD.iTeamPlayerKills of team 3 increased for team: ",iOtherTeam) 
					ENDIF
				ENDIF
				SET_BIT(MC_serverBD_3.iTeamTurnsRoundWonBS[iOtherTeam], MC_serverBD_1.iCurrentRound)
			ENDIF
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_TEAM_TURNS)
			AND NOT IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_TEAM_0_SHOULD_RESPAWN_BACK_AT_START_NOW + MC_playerBD[iVictimPart].iteam)
			AND NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iVictimPart].iteam)
			AND NOT IS_PLAYER_IN_RESPAWN_BACK_AT_START_LOGIC(iVictimPart)	
				SET_BIT(iLocalTeamTurnsRoundWonBS[iOtherTeam], MC_serverBD_1.iCurrentRound)
				IF MC_serverBD.piTurnsActivePlayer[iOtherTeam] = GET_PLAYER_INDEX()
					PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_SUICIDE_EVENT - Incrementing my own score ") 
					INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(1,TRUE, iVictimpart)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_POINT_TO_INCREMENT_VALID_FOR_RULE_WEAPON(WEAPON_TYPE weapType, INT iTeam, INT iRule)
	
	IF weapType = GET_WEAPON_FOR_GUNSMITH(iRule, iTeam)
	OR weapType = WEAPONTYPE_UNARMED
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC HANDLE_OBJECT_DESTROY_HUD(OBJECT_INDEX Victimobj)
	//Objects destroyed HUD
	IF bIsLocalPlayerHost
		INT iobjID = IS_OBJ_A_MISSION_CREATOR_OBJ(Victimobj)
		
		IF iobjID >= 0
			INT iTeam
			
			PRINTLN("[TargetsRemaining] I am the host and I have seen that obj ", iObjID, " has died!")
			
			FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
				IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
				
					PRINTLN("[TargetsRemaining] The objpriority for obj ", iObjID, " for team ", iTeam, " is ", MC_serverBD_4.iObjPriority[iobjID][iTeam], ", while the priority of that team is ", MC_serverBD_4.iCurrentHighestPriority[iteam])
					
					IF MC_serverBD_4.iObjPriority[iobjID][iTeam] <= FMMC_MAX_RULES
					AND MC_serverBD_4.iObjPriority[iobjID][iTeam] >= MC_serverBD_4.iCurrentHighestPriority[iteam]
						IF NOT IS_BIT_SET(MC_ServerBD_4.iObjectsDestroyedForSpecialHUDBitset[iTeam], iobjID)																
							SET_BIT(MC_ServerBD_4.iObjectsDestroyedForSpecialHUDBitset[iTeam], iobjID)
							PRINTLN("[TargetsRemaining] [RCC MISSION][PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT][iEntityincrement] HANDLE_OBJECT_DESTROY_HUD - iTeam: ", iTeam, " A new object has been destroyed obj: ", iobjID, " Incrementing iObjectsDestroyedForSpecialHUD to: ", (MC_ServerBD_4.iObjectsDestroyedForSpecialHUD[iTeam]+1))
							MC_ServerBD_4.iObjectsDestroyedForSpecialHUD[iTeam]++
						ELSE
							PRINTLN("[TargetsRemaining] [RCC MISSION][PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT][iEntityincrement] HANDLE_OBJECT_DESTROY_HUD - Bit is already set for obj ", iObjID, " for team ", iTeam)
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_DAMAGER_PARTICIPANT_INDEX(ENTITY_INDEX eiDamager)
	PED_INDEX DamagerPed
	PLAYER_INDEX DamagerPlayer
	PARTICIPANT_INDEX DamagerParticipant
	INT iDamagerPartNumber
	
	IF IS_ENTITY_A_PED(eiDamager)
		DamagerPed = GET_PED_INDEX_FROM_ENTITY_INDEX(eiDamager)
		IF IS_PED_A_PLAYER(DamagerPed)
			DamagerPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(DamagerPed)
			IF NETWORK_IS_PLAYER_A_PARTICIPANT(DamagerPlayer)
				DamagerParticipant = NETWORK_GET_PARTICIPANT_INDEX(DamagerPlayer)
				iDamagerPartNumber = NATIVE_TO_INT(DamagerParticipant)
			ENDIF
		ENDIF
	ELSE
		iDamagerPartNumber = -1
	ENDIF
	PRINTLN("[JT FLIP] Damager part = ", iDamagerPartNumber)
	RETURN iDamagerPartNumber
ENDFUNC
FUNC BOOL IS_FLIPPED_ALREADY_IN_ARRAY(VEHICLE_INDEX FlippedVeh)
	INT i
	FOR i = 0 TO DUNE_MAX_FLIPS -1
		IF viFlippedVeh[i] = FlippedVeh
			RETURN TRUE
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC
PROC DUNE_FLIP_MARK_FOR_EXPLOSION(ENTITY_INDEX VictimIndex, VEHICLE_INDEX VictimVehicle, ENTITY_INDEX DamagerIndex)
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			IF IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE),INT_TO_ENUM(MODEL_NAMES, HASH("DUNE4")))
			AND GET_DAMAGER_PARTICIPANT_INDEX(DamagerIndex) = iLocalPart
				IF DOES_ENTITY_EXIST(VictimIndex)
					IF IS_ENTITY_A_VEHICLE(VictimIndex)
						INT i
						FOR i = 0 TO DUNE_MAX_FLIPS -1
							IF (viFlippedVeh[i] != VictimVehicle AND viFlippedVeh[i] = NULL)
							AND NOT HAS_NET_TIMER_STARTED(stFlipTimer[i])
							AND NOT IS_FLIPPED_ALREADY_IN_ARRAY(VictimVehicle)
							AND NOT DOES_VEHICLE_RESIST_DUNE_FLIP(VictimVehicle)	
								viFlippedVeh[i] = VictimVehicle
								NETWORK_REQUEST_CONTROL_OF_ENTITY(viFlippedVeh[i])
								PRINTLN("[JT FLIP] FLIPPED NEW VEHICLE! FLIP COUNT: ", i, " Flipped vehicle: ", NATIVE_TO_INT(viFlippedVeh[i]))
								BREAKLOOP
							ENDIF
						ENDFOR
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT(INT iCount)
	
	PED_INDEX Victimped
	PED_INDEX Killerped
	PLAYER_INDEX victimPlayerID = INVALID_PLAYER_INDEX()
	PLAYER_INDEX KillerPlayerID = INVALID_PLAYER_INDEX()
	PARTICIPANT_INDEX victimPart = INVALID_PARTICIPANT_INDEX()
	PARTICIPANT_INDEX KillerPart = INVALID_PARTICIPANT_INDEX()
	INT iKillerpart = -1
	INT iVictimpart = -1
	INT ipedid = -2
	INT ivehid = -2
	INT iobjid = -2
	INT iteam
	BOOL bnotignore
	STRUCT_ENTITY_DAMAGE_EVENT sEntityID

	// Grab the event data.
	GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sEntityID, SIZE_OF(sEntityID))
		
	// TAKE THIS OUT......
	//PRINT_DAMAGED_EVENT_DATA(sEntityID)
		
	// Process event.
	IF DOES_ENTITY_EXIST(sEntityID.VictimIndex)
		
		IF HAS_NET_TIMER_STARTED(td_MissedShotTakeDamageTimer)
			IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
				IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
					IF GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex) = LocalPlayerPed
						PRINTLN("[LM][iMissedShotPerc] Hit someone, reset timer (1)")
						RESET_NET_TIMER(td_MissedShotTakeDamageTimer)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		// Ensure the damage event captures a melee attack
		DAMAGE_TYPE damageType = GET_WEAPON_DAMAGE_TYPE(INT_TO_ENUM(WEAPON_TYPE, sEntityID.WeaponUsed))
		WEAPON_TYPE wtWeaponUsed = INT_TO_ENUM(WEAPON_TYPE, sEntityID.WeaponUsed)
		WEAPON_GROUP wgWeaponGroupUsed
		
		IF wtWeaponUsed != WEAPONTYPE_INVALID
			wgWeaponGroupUsed = GET_WEAPONTYPE_GROUP(wtWeaponUsed)
		ENDIF
						
		// Check creator option set to anything non-zero
		IF g_FMMC_STRUCT.iPointsStolenOnBikeMelee > 0
		
			// WAS DAMAGED INFLICTED WITH A MELEE WEAPON
			IF sEntityID.IsWithMeleeWeapon
			OR wtWeaponUsed = WEAPONTYPE_UNARMED
			OR wgWeaponGroupUsed = WEAPONGROUP_MELEE
			OR damageType = DAMAGE_TYPE_MELEE
				PRINTLN("[ML] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT : YES, Damage done with a melee weapon.")
				
				// POPULATE VICTIM INFO
				IF IS_ENTITY_A_PED(sEntityID.VictimIndex)
					Victimped = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)
					
					IF IS_PED_A_PLAYER(Victimped)	
						victimPlayerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(Victimped)
						
						IF NETWORK_IS_PLAYER_A_PARTICIPANT(victimPlayerID)
							VictimPart = NETWORK_GET_PARTICIPANT_INDEX(victimPlayerID)
						ENDIF
					ENDIF
				ENDIF
				
				// POPULATE DAMAGER INFO
				IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
					Killerped = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)	
					
					IF IS_PED_A_PLAYER(Killerped)	
						KillerPlayerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(Killerped)
						
						IF NETWORK_IS_PLAYER_A_PARTICIPANT(KillerPlayerID)
							KillerPart = NETWORK_GET_PARTICIPANT_INDEX(KillerPlayerID)
						ENDIF
					ENDIF
				ENDIF
				
				// CHECK IF LOCAL PLAYER IS EITHER DAMAGER OR VICTIM
				IF victimPlayerID = PLAYER_ID()
					PRINTLN("[ML] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT : I am the victim. Broadcast this")
					iVictimpart = NATIVE_TO_INT(VictimPart)
					iteam = MC_PlayerBD[iVictimpart].iteam	
					BROADCAST_FMMC_DECREMENT_REMAINING_TIME(iteam, (g_FMMC_STRUCT.iPointsStolenOnBikeMelee*1000), TRUE, iVictimpart, TRUE)
				ELIF KillerPlayerID = PLAYER_ID()
					PRINTLN("[ML] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT : I am the attacker. Now broadcast this") 
					iKillerpart = NATIVE_TO_INT(KillerPart)
					iteam = MC_PlayerBD[iKillerpart].iteam
					BROADCAST_FMMC_INCREMENT_REMAINING_TIME(iteam, (g_FMMC_STRUCT.iPointsStolenOnBikeMelee*1000), TRUE, iKillerpart, TRUE, NATIVE_TO_INT(victimPlayerID))
				ELSE	
					PRINTLN("[ML] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT : I am NEIHER attacker or victim.")
				ENDIF	
			
			ELSE 
				PRINTLN("[ML] Damage not taken from a melee weapon") 
			ENDIF	
		ENDIF
		
		
		IF sEntityID.VictimDestroyed
		OR (IS_ENTITY_A_VEHICLE(sEntityID.VictimIndex) AND (NOT IS_VEHICLE_DRIVEABLE(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex))))
			
			IF IS_ENTITY_A_PED(sEntityID.VictimIndex)
				
				Victimped = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)
				
				IF IS_PED_A_PLAYER(Victimped)
					
					victimPlayerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(Victimped)
					
					IF NETWORK_IS_PLAYER_A_PARTICIPANT(victimPlayerID)
						VictimPart = NETWORK_GET_PARTICIPANT_INDEX(victimPlayerID)
						IF NETWORK_IS_PARTICIPANT_ACTIVE(VictimPart)
							iVictimpart = NATIVE_TO_INT(VictimPart)
							PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - victim is part: ",iVictimpart) 
						ENDIF
					ENDIF
					
					IF iVictimPart != -1
						IF MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] < FMMC_MAX_RULES
							//Beast
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitSetFive[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]], ciBS_RULE5_ENABLE_BEAST_MODE)
							OR IS_PARTICIPANT_A_BEAST(iLocalPart)
								PED_INDEX piDamager
								IF Victimped != LocalPlayerPed
									IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
										IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
											piDamager = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
											IF IS_PED_A_PLAYER(piDamager)
												IF piDamager = LocalPlayerPed
													PRINTLN("[JS] [BEASTMODE] - Beast killed another player")
													bBeastKilledPlayer = TRUE
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							//Drain
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitSetSix[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]], ciBS_RULE6_ENABLE_HEALTH_DRAIN)
								IF Victimped != LocalPlayerPed
									INT iDamage
									IF NETWORK_GET_ASSISTED_KILL_OF_ENTITY(LocalPlayer, Victimped, iDamage)
										PRINTLN("[JS] [HEALTHDRAIN] - PROCESS_HEALTH_DRAIN_DAMAGE_EVENTS - Assisted Kill")
										fDrainHealPercentage += g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].sDrainStruct[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]].fVehicleHeal
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF bIsLocalPlayerHost
						AND NOT IS_BIT_SET(MC_serverBD.iPartDeathEventBS, iVictimPart)
							PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Part ",iVictimpart," has died, set iPartDeathEventBS")
							SET_BIT(MC_serverBD.iPartDeathEventBS, iVictimPart)
						ENDIF
						
						IF iVictimPart = iLocalPart
						AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iVictimPart].iteam].iTeamBitset2, ciBS2_EXPLODE_ON_DEATH)
							PRINTLN("[JS] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - EXPLODING")
							ADD_EXPLOSION(GET_PLAYER_COORDS(LocalPlayer), EXP_TAG_PETROL_PUMP, DEFAULT, FALSE, DEFAULT, DEFAULT, TRUE)
						ENDIF
						
						IF (IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
						AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_JUGGERNAUT_SOUNDS))
						OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciENABLE_POWER_MAD_SOUNDS)
							IF IS_PARTICIPANT_A_JUGGERNAUT(iVictimPart)
							OR IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAS_JUGGERNAUT_ON_DEATH)
								STRING stSoundSet
								IF iVictimPart = iPartToUse
									IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciENABLE_POWER_MAD_SOUNDS)
										stSoundSet = "DLC_GR_PM_Juggernaut_Player_Sounds"
									ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_TRADING_PLACES_SOUNDS)
										stSoundSet = "DLC_BTL_TP_Remix_Juggernaut_Player_Sounds"
									ELSE
										stSoundSet = "DLC_IE_JN_Player_Sounds"
									ENDIF
								ELIF DOES_TEAM_LIKE_TEAM(MC_playerBD[iVictimPart].iTeam, MC_playerBD[iPartToUse].iTeam)
									IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciENABLE_POWER_MAD_SOUNDS)
										stSoundSet = "DLC_GR_PM_Juggernaut_Team_Sounds"
									ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_TRADING_PLACES_SOUNDS)
										stSoundSet = "DLC_BTL_TP_Remix_Juggernaut_Team_Sounds"
									ELSE
										stSoundSet = "DLC_IE_JN_Team_Sounds"
									ENDIF
								ELSE
									IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciENABLE_POWER_MAD_SOUNDS)
										stSoundSet = "DLC_GR_PM_Juggernaut_Enemy_Sounds"
									ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_TRADING_PLACES_SOUNDS)
										stSoundSet = "DLC_BTL_TP_Remix_Juggernaut_Enemy_Sounds"
									ELSE
										stSoundSet = "DLC_IE_JN_Enemy_Sounds"
									ENDIF
								ENDIF
								PLAY_SOUND_FROM_COORD(-1, "JN_Death", GET_PLAYER_COORDS(victimPlayerID), stSoundSet)	
							ENDIF
						ENDIF
						
						IF bIsLocalPlayerHost
							INT iVictimRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iVictimPart].iTeam]
							IF iVictimRule < FMMC_MAX_RULES
								IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iVictimPart].iTeam].iDeathPenalty[iVictimRule] > 0
									INT iCurrentTimer
									INT iPenalty = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iVictimPart].iTeam].iDeathPenalty[iVictimRule] * 1000
									IF HAS_NET_TIMER_STARTED(MC_serverBD_3.tdObjectiveLimitTimer[MC_playerBD[iVictimPart].iTeam])
										iCurrentTimer = GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iVictimPart].iTeam].iObjectiveTimeLimitRule[iVictimRule]) - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdObjectiveLimitTimer[MC_playerBD[iVictimPart].iTeam]) - MC_serverBD_3.iTimerPenalty[MC_playerBD[iVictimPart].iTeam]
										IF (iCurrentTimer - iPenalty) > (g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iVictimPart].iTeam].iDeathPenaltyThreshold[iVictimRule] * 1000)
											PRINTLN("[JS] - PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Decrementing team ", MC_playerBD[iVictimPart].iTeam,"'s timer due to player death")
											MC_serverBD_3.iTimerPenalty[MC_playerBD[iVictimPart].iTeam] += iPenalty
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						// Creator option to make drowning look like a suicide.
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciNEGATIVE_SUICIDE_DROWNING_IS_SUICIDE)
						OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciNEGATIVE_SUICIDE_FALL_IS_SUICIDE)
						OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciNEGATIVE_SUICIDE_UNKNOWN_SOURCES_IS_SUICIDE)
							PRINTLN("[LM][Drown] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - iVictimPart is: ", 							iVictimPart) 
							PRINTLN("[LM][Drown] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - iKillerPart is part: ", 					iKillerPart) 
							PRINTLN("[LM][Drown] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Killer Entity Exist check: ", 				DOES_ENTITY_EXIST(sEntityID.DamagerIndex))							
							
							PRINTLN("[LM][Drown] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Compare this:___________") 
							PRINTLN("[LM][Drown] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - sEntityID.WeaponUsed: ", 					sEntityID.WeaponUsed) 
							PRINTLN("[LM][Drown] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Against one of these:___________") 
							PRINTLN("[LM][Drown] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - ENUM_TO_INT(WEAPONTYPE_DROWNING): ",		HASH("WEAPON_DROWNING"))
							PRINTLN("[LM][Drown] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - ENUM_TO_INT(WEAPON_FALL): ",				HASH("WEAPON_FALL"))
							PRINTLN("[LM][Drown] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - ENUM_TO_INT(WEAPON_INVALID): ",				HASH("WEAPON_INVALID"))
							PRINTLN("[LM][Drown] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - ENUM_TO_INT(WEAPON_DROWNING_IN_VEHICLE): ",	HASH("WEAPON_DROWNING_IN_VEHICLE"))
							
							BOOL bOverrideDamager = FALSE				
							// If we died by drowning and there is no damager entity, then fudge it to make it appear as a suicide.
							IF iVictimPart = iLocalPart
							AND NOT DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciNEGATIVE_SUICIDE_DROWNING_IS_SUICIDE)
									IF sEntityID.WeaponUsed = HASH("WEAPON_DROWNING")
									OR sEntityID.WeaponUsed = HASH("WEAPON_DROWNING_IN_VEHICLE")
										PRINTLN("[LM][Drown] Overriding damage due to detection of: WEAPON_DROWNING or WEAPON_DROWNING_IN_VEHICLE")
										bOverrideDamager = TRUE
									ENDIF
								ENDIF
								
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciNEGATIVE_SUICIDE_FALL_IS_SUICIDE)
									IF sEntityID.WeaponUsed = HASH("WEAPON_FALL")
										PRINTLN("[LM][Drown] Overriding damage due to detection of: WEAPON_FALL")
										bOverrideDamager = TRUE
									ENDIF
								ENDIF
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciNEGATIVE_SUICIDE_UNKNOWN_SOURCES_IS_SUICIDE)
									IF sEntityID.WeaponUsed = HASH("WEAPON_INVALID")
										PRINTLN("[LM][Drown] Overriding damage due to detection of: WEAPON_INVALID")
										bOverrideDamager = TRUE
									ENDIF
								ENDIF
							ENDIF
							
							IF bOverrideDamager
								PRINTLN("[LM][Drown] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Everything checks out. Forcing the player as the killer ")
								sEntityID.DamagerIndex  = localPlayerPed
							ENDIF
						ENDIF
												
						// Hard Target Audio - url:bugstar:3951761
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_HARD_TARGET_AUDIO)
							IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
							AND DOES_ENTITY_EXIST(sEntityID.VictimIndex)								
								PED_INDEX KillerpedAudio = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
								PED_INDEX VictimpedAudio = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)
								IF IS_PED_A_PLAYER(KillerpedAudio)
								AND IS_PED_A_PLAYER(VictimpedAudio)				
								
									PLAYER_INDEX plKiller = NETWORK_GET_PLAYER_INDEX_FROM_PED(KillerpedAudio)
									PLAYER_INDEX plVictim = NETWORK_GET_PLAYER_INDEX_FROM_PED(VictimpedAudio)
									
									PARTICIPANT_INDEX piKiller = NETWORK_GET_PARTICIPANT_INDEX(plKiller)
									PARTICIPANT_INDEX piVictim = NETWORK_GET_PARTICIPANT_INDEX(plVictim)
									
									IF NETWORK_IS_PARTICIPANT_ACTIVE(piKiller)
									AND NETWORK_IS_PARTICIPANT_ACTIVE(piVictim)
										INT iKillerAudio = NATIVE_TO_INT(piKiller)
										INT iVictimAudio = NATIVE_TO_INT(piVictim)
										
										IF iKillerAudio > -1
										AND iKillerAudio < NUM_NETWORK_PLAYERS
										AND iVictimAudio > -1
										AND iVictimAudio < NUM_NETWORK_PLAYERS
											INT iMyTeam = MC_PlayerBD[iPartToUse].iteam
											INT iKillerTeam = MC_PlayerBD[iKillerAudio].iteam
											INT iVictimTeam = MC_PlayerBD[iVictimAudio].iteam
											
											PRINTLN("[LM][HARD_TARGET][AUDIO] Player was Killed in Hard Target - iMyTeam: ", iMyTeam, " | iKillerTeam: ", iKillerTeam, "
													| iVictimTeam: ", iVictimTeam, " | iKillerAudio: ", iKillerAudio, " | iVictimAudio: ", iVictimAudio)
											
											IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
												IF iMyTeam = iKillerTeam
												AND IS_BIT_SET(MC_PlayerBD[iVictimAudio].iClientBitSet3, PBBOOL3_PLAYER_IS_A_HARD_TARGET)
												AND NOT IS_BIT_SET(MC_PlayerBD[iKillerAudio].iClientBitSet3, PBBOOL3_PLAYER_IS_A_HARD_TARGET)
													//should trigger for the local player only, when the enemy Target is killed by a non-Target team member, earning the player's team 1 point.
													PLAY_SOUND_FRONTEND(-1, "Enemy_Killed_1p" ,"dlc_xm_hata_Sounds", false)
												ELIF iMyTeam = iKillerTeam
												AND IS_BIT_SET(MC_PlayerBD[iVictimAudio].iClientBitSet3, PBBOOL3_PLAYER_IS_A_HARD_TARGET)
												AND IS_BIT_SET(MC_PlayerBD[iKillerAudio].iClientBitSet3, PBBOOL3_PLAYER_IS_A_HARD_TARGET)
													//should trigger for the local player only, when the enemy Target is killed by a Target team member, earning the player's team 3 points.
													PLAY_SOUND_FRONTEND(-1, "Enemy_Killed_3p" ,"dlc_xm_hata_Sounds", FALSE)												 
												ELIF iMyTeam = iVictimTeam
												AND IS_BIT_SET(MC_PlayerBD[iVictimAudio].iClientBitSet3, PBBOOL3_PLAYER_IS_A_HARD_TARGET)
												AND NOT IS_BIT_SET(MC_PlayerBD[iKillerAudio].iClientBitSet3, PBBOOL3_PLAYER_IS_A_HARD_TARGET)
													//should trigger for the local player only, when the team Target is killed by a non-Target enemy, earning the enemy team 1 point.
													PLAY_SOUND_FRONTEND(-1, "Team_Killed_1p" ,"dlc_xm_hata_Sounds", FALSE)
												ELIF iMyTeam = iVictimTeam
												AND IS_BIT_SET(MC_PlayerBD[iVictimAudio].iClientBitSet3, PBBOOL3_PLAYER_IS_A_HARD_TARGET)
												AND IS_BIT_SET(MC_PlayerBD[iKillerAudio].iClientBitSet3, PBBOOL3_PLAYER_IS_A_HARD_TARGET)				
													//should trigger for the local player only, when the team Target is killed by a Target enemy, earning the enemy team 3 points.
													PLAY_SOUND_FRONTEND(-1, "Team_Killed_3p" ,"dlc_xm_hata_Sounds", FALSE)
												ENDIF
											ELSE
												PRINTLN("[LM][HARD_TARGET][AUDIO] Player was Killed in Hard Target (SUDDEN DEATH)")
												IF iMyTeam = iKillerTeam				
													//should trigger for the local player only, when the enemy Target is killed by a Target team member, earning the player's team 3 points.
													PLAY_SOUND_FRONTEND(-1, "Enemy_Killed_3p" ,"dlc_xm_hata_Sounds", false)									 
												ELIF iMyTeam = iVictimTeam
													//should trigger for the local player only, when the team Target is killed by a Target enemy, earning the enemy team 3 points.
													PLAY_SOUND_FRONTEND(-1, "Team_Killed_3p" ,"dlc_xm_hata_Sounds", FALSE)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
							IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
								Killerped = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
								IF IS_PED_A_PLAYER(Killerped)
									KillerPlayerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(Killerped)
									IF NETWORK_IS_PLAYER_A_PARTICIPANT(KillerPlayerID)
										KillerPart = NETWORK_GET_PARTICIPANT_INDEX(KillerPlayerID)
										IF NETWORK_IS_PARTICIPANT_ACTIVE(KillerPart)
											ikillerpart=NATIVE_TO_INT(KillerPart)
											PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - killer is part: ",ikillerpart)
										ENDIF
									ENDIF
									
									INT iMaxHealth 
									INT iHealthToAdd
									
									IF ikillerpart !=-1
										
										IF iKillerPart = iLocalPart
										AND iKillerPart != iVictimPart
											IF MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] < FMMC_MAX_RULES
												IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitSetSix[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]], ciBS_RULE6_ENABLE_HEALTH_DRAIN)
													PRINTLN("[JS] [HEALTHDRAIN] - PROCESS_HEALTH_DRAIN_DAMAGE_EVENTS - Killed another player")
													fDrainHealPercentage += g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].sDrainStruct[ MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] ].fPlayerHeal
												ENDIF
											ENDIF
										ENDIF
										IF iKillerPart != iVictimPart
											IF MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iKillerPart].iteam] < FMMC_MAX_RULES
												FLOAT fCaptureBoost = g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iKillerPart].iteam].fCaptureIncrease[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iKillerPart].iteam]]
												IF fCaptureBoost > 0
													IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iKillerPart].iteam].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iKillerPart].iteam]], ciBS_RULE9_USE_GRANULAR_CAPTURE)
														IF iKillerPart = iLocalPart
															IF MC_PlayerBD[iLocalPart].iGranularObjCapturingBS > 0
																INT iIncrease = ROUND(GET_FMMC_GRANULAR_CAPTURE_TARGET(MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam], MC_PlayerBD[iLocalPart].iteam) * (fCaptureBoost/100))
																MC_PlayerBD[iLocalPart].iGranularCurrentPoints += iIncrease
																MC_PlayerBD[iLocalPart].iGranularTotalPoints += iIncrease
																PRINTLN("[JS] - PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - increasing capture progress by ",fCaptureBoost,"% = ",iIncrease," points")
															ENDIF
														ENDIF
													ELSE
														IF bIsLocalPlayerHost
															INT iObj = -1, iObjLoop
															FOR iObjLoop = 0 TO MC_serverBD.iNumObjCreated - 1
																IF MC_serverBD.iObjCarrier[iObjLoop] = iKillerPart
																	iObj = iObjLoop
																	BREAKLOOP
																ENDIF
															ENDFOR
															IF iObj > -1
																IF MC_serverBD_4.iObjPriority[iObj][MC_PlayerBD[iKillerPart].iteam] < FMMC_MAX_RULES
																	IF MC_serverBD_4.iObjRule[iObj][MC_PlayerBD[iKillerPart].iteam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
																		INT iIncrease = ROUND(MC_serverBD.iObjTakeoverTime[MC_PlayerBD[iKillerPart].iteam][MC_serverBD_4.iObjPriority[iObj][MC_PlayerBD[iKillerPart].iteam]] * (fCaptureBoost/100))
																		PRINTLN("[JS] - PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - increasing capture progress by ",fCaptureBoost,"% = ",iIncrease,"ms")
																		MC_serverBD.iObjectCaptureOffset[MC_PlayerBD[iKillerPart].iteam] += iIncrease
																	ENDIF
																ENDIF
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
										
										IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciHEALTH_BOOST_ON_KILL)
										AND NOT (IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
											AND IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_BE_MY_VALENTINES))	//Don't give players health back in sudden death
											IF MC_playerBD[PARTICIPANT_ID_TO_INT()].iteam = MC_playerBD[ikillerpart].iteam 
												IF NOT IS_PED_INJURED(LocalPlayerPed)
													iMaxHealth = GET_ENTITY_MAX_HEALTH(LocalPlayerPed)
													iHealthToAdd = ROUND(iMaxHealth / 3.0)
													PRINTLN("[RCC MISSION] ciHEALTH_BOOST_ON_KILL - Giving Health to local player")
													SET_ENTITY_HEALTH(LocalPlayerPed, GET_ENTITY_HEALTH(LocalPlayerPed) + iHealthToAdd)
												ENDIF
											ELSE
												PRINTLN("[RCC MISSION] ciHEALTH_BOOST_ON_KILL in on but teams don't match. Local Player Team: " , MC_playerBD[PARTICIPANT_ID_TO_INT()].iteam , "Killer Team: " , MC_playerBD[ikillerpart].iteam)
											ENDIF
										ENDIF
										
										IF IS_PARTICIPANT_A_JUGGERNAUT(iLocalPart)
										AND bLocalPlayerPedOK
											IF iLocalPart = ikillerpart
											AND ikillerpart != iVictimPart
												USE_PARTICLE_FX_ASSET("scr_impexp_jug")
												START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_ie_jug_mask_flame", LocalPlayerPed, vJuggernautVFXFire1Pos, vJuggernautVFXFire1Rot, BONETAG_HEAD)
												USE_PARTICLE_FX_ASSET("scr_impexp_jug")
												START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_ie_jug_mask_flame", LocalPlayerPed, vJuggernautVFXFire2Pos, vJuggernautVFXFire2Rot, BONETAG_HEAD)
												REINIT_NET_TIMER(tdJuggernautVFX)
											ENDIF
										ENDIF
										
										IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciTRADING_PLACES_TIME_BAR_ENABLED) AND g_FMMC_STRUCT.iTradingPlacesTimeBarDuration != -1
											IF iLocalPart = ikillerpart
											AND ikillerpart != iVictimPart
												IF g_FMMC_STRUCT.iTradingPlacesTimeBarAddOnKill > 0
												AND MC_playerBD[iLocalPart].iteam != 0 //Winning team
													PRINTLN("[JS] [TradingPlaces] - Killed a player, adding score")
													PRINTLN("[JS] [TradingPlaces] - iTradingPlacesTimeBarCurrent = ", MC_playerBD[iLocalPart].iTradingPlacesTimeBarCurrent)
													PRINTLN("[JS] [TradingPlaces] - iTradingPlacesTimeBarAddOnKill = ", g_FMMC_STRUCT.iTradingPlacesTimeBarAddOnKill)
													PRINTLN("[JS] [TradingPlaces] - iTradingPlacesTimeBarDuration = ", g_FMMC_STRUCT.iTradingPlacesTimeBarDuration * 1000)
													PRINTLN("[JS] [TradingPlaces] - Adding ", (g_FMMC_STRUCT.iTradingPlacesTimeBarDuration * 1000) * (TO_FLOAT(g_FMMC_STRUCT.iTradingPlacesTimeBarAddOnKill)/100))
													MC_playerBD[iLocalPart].iTradingPlacesTimeBarCurrent += ((g_FMMC_STRUCT.iTradingPlacesTimeBarDuration * 1000) * ROUND(TO_FLOAT(g_FMMC_STRUCT.iTradingPlacesTimeBarAddOnKill)/100))
													
													//Trigger HUD Flash
													IF IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesWinningPlayerFoundLast) = IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesWinningPlayerFound)
														IF IS_BIT_SET(iTradingPlacesBitSet, ciTradingPlacesWinningPlayerFound)
															CLEAR_BIT(iTradingPlacesBitSet, ciTradingPlacesWinningPlayerFoundLast)
														ELSE
															SET_BIT(iTradingPlacesBitSet, ciTradingPlacesWinningPlayerFoundLast)
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
										//custom wasted shard
										IF MC_playerBD[ivictimpart].iteam >= 0
											IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[ivictimpart].iteam].iTeamBitset2, ciBS2_CUSTOM_WASTED_SHARD)
												IF g_FMMC_STRUCT.iCustomWastedShardType[MC_playerBD[ivictimpart].iteam] = ciCUSTOM_WASTED_SHARD_BE_MY_VALENTINE
													IF MC_playerBD[ivictimpart].iteam = MC_playerBD[iLocalPart].iteam
														IF ivictimpart != iLocalPart
															IF ivictimpart != ikillerpart
																PRINTLN("[KH] - TEAM MATE KILLED BY: ", ikillerpart)
																iPartTeammateWasKilledBy = ikillerpart
															ELSE
																//your team mate committed suicide
																PRINTLN("[KH] - TEAM MATE COMMITTED SUICIDE: ", ikillerpart)
																iPartTeammateWasKilledBy = -3
															ENDIF
														ENDIF
													ENDIF
													IF ivictimpart = iLocalPart
														PRINTLN("[KH] - YOU WERE KILLED BY - PLAYING WASTED SHARD: ", ikillerpart) 
														IF iPartTeammateWasKilledBy >= 0
															PRINTLN("[KH] - YOU WERE KILLED BUT YOUR TEAM MATE WAS KILLED BY: ", iPartTeammateWasKilledBy) 
															PROCESS_CUSTOM_WASTED_SHARD(MC_playerBD[ivictimpart].iteam, ivictimpart, iPartTeammateWasKilledBy)
														ELSE	
															iPartTeammateWasKilledBy = ikillerpart
															#IF IS_DEBUG_BUILD 
																IF ivictimpart = ikillerpart
																	PRINTLN("[KH] - YOU COMMITED SUICIDE")
																ENDIF
															#ENDIF
															
															PRINTLN("[KH] - YOU WERE KILLED BY - PLAYING WASTED SHARD: ", ikillerpart) 
															PROCESS_CUSTOM_WASTED_SHARD(MC_playerBD[ivictimpart].iteam, ivictimpart, ikillerpart)
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
										
										//Deadline shard
										IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciPRON_MODE_ENABLED)
										AND ivictimpart = iLocalPart
											CLEAR_ALL_BIG_MESSAGES()
											IF ivictimpart = ikillerpart
												SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_WASTED, "RESPAWN_W_MP", "DM_U_SUIC")
											ELSE
												//STRING sTeamName = TEXT_LABEL_TO_STRING(g_sMission_TeamName[MC_playerBD[ikillerpart].iteam])
												HUD_COLOURS hcKillerTeamColour
												IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA(g_FMMC_STRUCT.iAdversaryModeType)
													IF MC_playerBD[ikillerpart].iteam = 0
														PRINTLN("[KH] (3149342) - Colour Orange")
														hcKillerTeamColour = HUD_COLOUR_ORANGE
													ELSE
														PRINTLN("[KH] (3149342) - Colour Purple")
														hcKillerTeamColour = HUD_COLOUR_PURPLE
													ENDIF
												ELSE
													PRINTLN("[KH] (3149342) - Colour Blue")
													hcKillerTeamColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_playerBD[ikillerpart].iteam, KillerPlayerID)
												ENDIF
												SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_WASTED, KillerPlayerID, DEFAULT, "DM_TK_BOMB12", "RESPAWN_W_MP", hcKillerTeamColour)
											ENDIF
										ENDIF
										
										//Switch teams on death
										IF iVictimPart = iLocalPart
										AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciCLOBBER_PLAYER_ON_TEAM_SWAP)
											IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
												INT iNewTeam = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iSwapToTeamOnDeath[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]]
												
												IF iNewTeam != -1
												AND iNewTeam != MC_playerBD[iLocalPart].iteam
													IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iDontSwitchOnSuicide[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]], ciDYNAMIC_MENU_DONT_SWITCH_ON_SUICIDE)
														IF MC_playerBD[iLocalPart].iteam != MC_playerBD[ikillerpart].iteam 
															IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_SHOULD_SHOW_TEAM_SWITCH_SHARD)
																SET_BIT(iLocalBoolCheck16, LBOOL16_SHOULD_SHOW_TEAM_SWITCH_SHARD)
															ENDIF
															CHANGE_PLAYER_TEAM(iNewTeam, TRUE)
															BROADCAST_FMMC_OBJECTIVE_PLAYER_SWITCHED_TEAMS(MC_playerBD[iLocalPart].iteam, iNewTeam)
															PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Died (iDontSwitchOnSuicide is enabled), swapping to team ", iNewTeam)
														ELSE
															PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Died (suicide), not switching team")
														ENDIF
													ELSE
														IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_SHOULD_SHOW_TEAM_SWITCH_SHARD)
															SET_BIT(iLocalBoolCheck16, LBOOL16_SHOULD_SHOW_TEAM_SWITCH_SHARD)
														ENDIF
														PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Died, swapping to team ", iNewTeam)
														CHANGE_PLAYER_TEAM(iNewTeam, TRUE)
														BROADCAST_FMMC_OBJECTIVE_PLAYER_SWITCHED_TEAMS(MC_playerBD[iLocalPart].iteam, iNewTeam)
													ENDIF
												ENDIF
											ENDIF
										ENDIF
										
										//Pointless
										IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciPOINTLESS_MODE_ENABLED)
											IF bIsLocalPlayerHost
												PTL_SET_PACKAGE_KILLER(iVictimPart, iKillerPart)
											ENDIF
										ENDIF
										
										//Removing PTFX in Air Quota
										IF iVictimPart = iLocalPart
										AND DOES_PARTICLE_FX_LOOPED_EXIST(ptfxAirQuotaLastKillEffect)
											REMOVE_PARTICLE_FX(ptfxAirQuotaLastKillEffect)
											PRINTLN("[AIRQUOTA_PTFX] - Clearing up the effect as the local player dies")
										ENDIF

										IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciCLOBBER_PLAYER_ON_TEAM_SWAP)
											IF LocalPlayer = KillerPlayerID
												IF LocalPlayer != victimPlayerID 
													PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Team swap, not a suicide. Adding kills")
													MC_playerBD[iPartToUse].iNumPlayerKills++
													
													iKilledThisManyFrmTeam[MC_playerBD[iVictimpart].iteam]++
													MC_playerBD[iPartToUse].iPureKills++
														
													IF sEntityID.IsHeadShot
														MC_playerBD[iPartToUse].iNumHeadshots++
													ELSE
														CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_ALL_HEADSHOTS)
													ENDIF
												ENDIF
												PRINTLN("[RCC MISSION][MMacK][ServerSwitch] ITS CLOBBERING TIME")
												
												IF (LocalPlayer != victimPlayerID 
												OR NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iDontSwitchOnSuicide[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciDYNAMIC_MENU_DONT_SWITCH_ON_SUICIDE))
													IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam] < FMMC_MAX_RULES
														INT iNewTeam = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iSwapToTeamOnKill[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]]
														PLAYER_INDEX piTempKiller = LocalPlayer
															
														IF LocalPlayer = VictimPlayerID 
														AND LocalPlayer = KillerPlayerID
														AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iDontSwitchOnSuicide[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciDYNAMIC_MENU_DONT_SWITCH_ON_SUICIDE)
															piTempKiller = GET_PLAYER_FROM_LOSING_TEAM_FOR_SWAP()
															iNewTeam = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iSwapToTeamOnDeath[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]]
														ENDIF
														
														IF iNewTeam != -1
														AND iNewTeam != MC_playerBD[iPartToUse].iteam
														AND VictimPlayerID != piTempKiller
															IF bIsLocalPlayerHost
																PRINTLN("[RCC MISSION][MMacK][ServerSwitch] Processing Inside Damage Event")
																PROCESS_PLAYER_TEAM_SWITCH(iVictimPart, piTempKiller, LocalPlayer = victimPlayerID)
															ELSE
																BROADCAST_FMMC_PLAYER_TEAM_SWITCH_REQUEST(MC_ServerBD.iSessionScriptEventKey, MC_playerBD[iPartToUse].iteam, iNewTeam, iVictimPart, piTempKiller, LocalPlayer = victimPlayerID)
																PRINTLN("[RCC MISSION][MMacK][ServerSwitch] Broadcasting Request")
															ENDIF
															
															MC_PlayerBD[iLocalPart].bSwapTeamStarted = TRUE
															PRINTLN("[MMacK][TeamSwaps] bSwapTeamLock = TRUE 1")
															REINIT_NET_TIMER(tdTeamSwapFailSafe)
														ELSE
															IF VictimPlayerID = piTempKiller
																PRINTLN("[RCC MISSION][MMacK][ServerSwitch] NO VALID PLAYER TO SWAP WITH")
															ELSE
																PRINTLN("[RCC MISSION][MMacK][ServerSwitch] SWAP IS NOT VALID", iNewTeam)
															ENDIF
														ENDIF
														
														IF LocalPlayer = VictimPlayerID 
															iPartLastDamager = -1
														ENDIF
													ENDIF
												ENDIF
											ELIF LocalPlayer = victimPlayerID
												MC_PlayerBD[iPartToUse].bSwapTeamStarted = TRUE
												PRINTLN("[MMacK][TeamSwaps] bSwapTeamLock = TRUE 2")
												REINIT_NET_TIMER(tdTeamSwapFailSafe)
											ENDIF
										ELIF MC_playerBD[iVictimpart].iteam != MC_playerBD[ikillerpart].iteam 
										AND NOT DOES_TEAM_LIKE_TEAM(MC_playerBD[iVictimpart].iteam, MC_playerBD[ikillerpart].iteam )
										AND MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
											IF bIsLocalPlayerHost
												INT igamestage = GET_MC_CLIENT_MISSION_STAGE(ikillerpart)
												PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - GET_MC_CLIENT_MISSION_STAGE: ",igamestage) 
												PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - host adding to team kills ") 
												IF igamestage = CLIENT_MISSION_STAGE_KILL_PLAYERS
													MC_serverBD.iTeamPlayerKills[MC_playerBD[ikillerpart].iteam][MC_playerBD[iVictimpart].iteam]++
												ELIF igamestage = CLIENT_MISSION_STAGE_KILL_TEAM0
													IF MC_playerBD[iVictimpart].iteam = 0
														MC_serverBD.iTeamPlayerKills[MC_playerBD[ikillerpart].iteam][MC_playerBD[iVictimpart].iteam]++
														PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - MC_serverBD.iTeamPlayerKills of team 0 increased for team: ",MC_playerBD[ikillerpart].iteam) 
													ENDIF
												ELIF igamestage = CLIENT_MISSION_STAGE_KILL_TEAM1
													IF MC_playerBD[iVictimpart].iteam = 1
														MC_serverBD.iTeamPlayerKills[MC_playerBD[ikillerpart].iteam][MC_playerBD[iVictimpart].iteam]++
														PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - MC_serverBD.iTeamPlayerKills of team 1 increased for team: ",MC_playerBD[ikillerpart].iteam)
													ENDIF
												ELIF igamestage = CLIENT_MISSION_STAGE_KILL_TEAM2
													IF MC_playerBD[iVictimpart].iteam = 2
														MC_serverBD.iTeamPlayerKills[MC_playerBD[ikillerpart].iteam][MC_playerBD[iVictimpart].iteam]++
														PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - MC_serverBD.iTeamPlayerKills of team 2 increased for team: ",MC_playerBD[ikillerpart].iteam)
													ENDIF
												ELIF igamestage = CLIENT_MISSION_STAGE_KILL_TEAM3
													IF MC_playerBD[iVictimpart].iteam = 3
														MC_serverBD.iTeamPlayerKills[MC_playerBD[ikillerpart].iteam][MC_playerBD[iVictimpart].iteam]++
														PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - MC_serverBD.iTeamPlayerKills of team 3 increased for team: ",MC_playerBD[ikillerpart].iteam) 
													ENDIF
												ENDIF
												IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRSHOOTOUT(g_FMMC_STRUCT.iAdversaryModeType)
													SET_BIT(MC_serverBD_3.iTeamTurnsRoundWonBS[MC_playerBD[ikillerpart].iteam], MC_serverBD_1.iCurrentRound)
												ENDIF
											ENDIF
											IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_TEAM_TURNS)
											AND NOT IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_TEAM_0_SHOULD_RESPAWN_BACK_AT_START_NOW + MC_playerBD[ikillerpart].iteam)
											AND NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[ikillerpart].iteam)
											AND NOT IS_PLAYER_IN_RESPAWN_BACK_AT_START_LOGIC(ikillerpart)	
												SET_BIT(iLocalTeamTurnsRoundWonBS[MC_playerBD[ikillerpart].iteam], MC_serverBD_1.iCurrentRound)
											ENDIF
											IF LocalPlayer = KillerPlayerID
											
												MC_playerBD[iPartToUse].iNumPlayerKills++
												
												IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_RESURRECTION(g_FMMC_STRUCT.iAdversaryModeType)
												AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)	
													IF MC_serverBD_1.iRespawnQueue[MC_playerBD[iPartToUse].iteam][0] != -1
														IF NOT IS_BIT_SET(MC_serverBD_1.iServerRespawnBitset, SRBS_RESPAWN_FROM_SPECTATOR_NOW_TEAM_0 + MC_PlayerBD[iPartToUse].iteam)
															MC_playerBD[iPartToUse].iResurrections ++
															PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Increasing Resurrections to ", MC_playerBD[iPartToUse].iResurrections)
														ELSE
															INT iRezLoop
															FOR iRezLoop = 1 TO FMMC_MAX_REZQ_PARTS - 1
																IF MC_serverBD_1.iRespawnQueue[MC_playerBD[iPartToUse].iteam][iRezLoop] != -1
																AND MC_serverBD_1.iRespawnMultiKillQueue[MC_PlayerBD[iPartToUse].iteam] < iRezLoop
																	MC_playerBD[iPartToUse].iResurrections ++
																	PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Increasing Resurrections to ", MC_playerBD[iPartToUse].iResurrections, " Kill queue = ", MC_serverBD_1.iRespawnMultiKillQueue[MC_PlayerBD[iPartToUse].iteam])
																	BREAKLOOP
																ENDIF
															ENDFOR
														ENDIF
													ENDIF
												ENDIF
												
												// Count how many players from team that local player has killed
												iKilledThisManyFrmTeam[MC_playerBD[iVictimpart].iteam]++
												PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - AWARD, MP_AWARD_KILL_TEAM_YOURSELF_LTS, killed  ", iKilledThisManyFrmTeam[MC_playerBD[iVictimpart].iteam], " from team ", MC_playerBD[iVictimpart].iteam)
												MC_playerBD[iPartToUse].iPureKills++
												
												BOOL bGiveTeamOptionPoints = TRUE
												
												IF NOT (IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
												AND IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_LOST_AND_DAMNED))
													IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
													AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetSeven[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE7_SUBTRACT_POINTS_FROM_TEAM)

														IF IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
															PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Sudden Death, incrementing by 1 ") 
															INCREMENT_LOCAL_PLAYER_SCORE_BY(1,FALSE)
														ENDIF
														PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - skipping incrementing local player score, decrementing other team ")
													ELSE
														IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_TEAM_TURNS)
														OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_TEAM_TURNS)
														AND NOT IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_TEAM_0_SHOULD_RESPAWN_BACK_AT_START_NOW + MC_playerBD[iLocalPart].iteam)
														AND NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iLocalPart].iteam)
														AND NOT IS_PLAYER_IN_RESPAWN_BACK_AT_START_LOGIC(iLocalPart))
															IF MC_serverBD.iNumPlayerRuleHighestPriority[MC_playerBD[iPartToUse].iteam] > 0 
																PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - more than 0 higherst ") 
																IF MC_serverBD_4.iPlayerRuleMissionLogic[MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL_PLAYERS // this high priority kill player objective
																	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_LOST_DAMNED(g_FMMC_STRUCT.iAdversaryModeType) //Check to see if the weapon used to kill to prevent points from being allocated incorrectly
																		IF NOT (sEntityID.WeaponUsed = ENUM_TO_INT(WEAPONTYPE_DLC_BATTLEAXE)
																		OR sEntityID.WeaponUsed = ENUM_TO_INT(WEAPONTYPE_DLC_MACHETE))
																			INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(DEFAULT, DEFAULT, iVictimpart)
																		ENDIF
																	ELSE
																		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciHARD_TARGET_ENABLE_MODE)
																			IF IS_BIT_SET(MC_PlayerBD[iVictimPart].iClientBitSet3, PBBOOL3_PLAYER_IS_A_HARD_TARGET)
																			AND IS_BIT_SET(MC_PlayerBD[iKillerPart].iClientBitSet3, PBBOOL3_PLAYER_IS_A_HARD_TARGET)
																				INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(3, DEFAULT, iVictimpart)
																			ELIF IS_BIT_SET(MC_PlayerBD[iVictimPart].iClientBitSet3, PBBOOL3_PLAYER_IS_A_HARD_TARGET) OR IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
																				INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(1, DEFAULT, iVictimpart)
																			ENDIF
																		ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_GUNSMITH(g_FMMC_STRUCT.iAdversaryModeType)
																			IF IS_POINT_TO_INCREMENT_VALID_FOR_RULE_WEAPON(INT_TO_ENUM(WEAPON_TYPE, sEntityID.WeaponUsed), MC_playerBD[iPartToUse].iteam, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
																				INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(DEFAULT, DEFAULT, iVictimpart)
																			ENDIF
																		ELSE
																			INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(DEFAULT, DEFAULT, iVictimpart)
																		ENDIF
																	ENDIF
																	bGiveTeamOptionPoints = FALSE
																ELIF MC_serverBD_4.iPlayerRuleMissionLogic[MC_playerBD[iPartToUse].iteam] >= FMMC_OBJECTIVE_LOGIC_KILL_TEAM0
																AND MC_serverBD_4.iPlayerRuleMissionLogic[MC_playerBD[iPartToUse].iteam] <= FMMC_OBJECTIVE_LOGIC_KILL_TEAM3
																	
																	IF MC_playerBD[iVictimpart].iteam = (MC_serverBD_4.iPlayerRuleMissionLogic[MC_playerBD[iPartToUse].iteam] - FMMC_OBJECTIVE_LOGIC_KILL_TEAM0)
																		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciHARD_TARGET_ENABLE_MODE)
																			IF IS_BIT_SET(MC_PlayerBD[iVictimPart].iClientBitSet3, PBBOOL3_PLAYER_IS_A_HARD_TARGET)
																			AND IS_BIT_SET(MC_PlayerBD[iKillerPart].iClientBitSet3, PBBOOL3_PLAYER_IS_A_HARD_TARGET)
																				INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(3, DEFAULT, iVictimpart)
																			ELIF IS_BIT_SET(MC_PlayerBD[iVictimPart].iClientBitSet3, PBBOOL3_PLAYER_IS_A_HARD_TARGET) OR IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
																				INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(1, DEFAULT, iVictimpart)
																			ENDIF
																		ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_GUNSMITH(g_FMMC_STRUCT.iAdversaryModeType)
																			IF IS_POINT_TO_INCREMENT_VALID_FOR_RULE_WEAPON(INT_TO_ENUM(WEAPON_TYPE, sEntityID.WeaponUsed), MC_playerBD[iPartToUse].iteam, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
																				INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(1,TRUE, iVictimpart)
																			ENDIF
																		ELSE
																			INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(1,TRUE, iVictimpart)
																		ENDIF																	
																		bGiveTeamOptionPoints = FALSE
																	ELSE
																		IF (IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
																		AND IS_BIT_SET(MC_serverBD.iServerBitSet4, SBBOOL4_SUDDEN_DEATH_JUGGERNAUT))
																		
																			INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(1,TRUE, iVictimpart)
																		ENDIF
																		bGiveTeamOptionPoints = FALSE
																	ENDIF
																ENDIF
															ENDIF
														ENDIF
														
														IF IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
														AND (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciTRANSFORM_TO_JUGGERNAUT_ON_SUDDEN_DEATH)
														OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciTRANSFORM_TO_BEAST_ON_SUDDEN_DEATH))
															PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Sudden Death (transformed), incrementing by 1 ") 
															INCREMENT_LOCAL_PLAYER_SCORE_BY(1,FALSE)
														ENDIF
														
														IF bGiveTeamOptionPoints // If we haven't already handed out points
															IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iPointsPerKill > 0
																IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_GUNSMITH(g_FMMC_STRUCT.iAdversaryModeType)
																	PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Gunsmith, we don't want non-current objective points added. Ignoring.")
																ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciHARD_TARGET_ENABLE_MODE)
																	IF IS_BIT_SET(MC_PlayerBD[iVictimPart].iClientBitSet3, PBBOOL3_PLAYER_IS_A_HARD_TARGET)
																	AND IS_BIT_SET(MC_PlayerBD[iKillerPart].iClientBitSet3, PBBOOL3_PLAYER_IS_A_HARD_TARGET)
																		INCREMENT_LOCAL_PLAYER_SCORE_BY(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iPointsPerKill*3,TRUE)
																	ELIF IS_BIT_SET(MC_PlayerBD[iVictimPart].iClientBitSet3, PBBOOL3_PLAYER_IS_A_HARD_TARGET) OR IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
																		INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(1, DEFAULT, iVictimpart)
																	ENDIF
																ELSE
																	INCREMENT_LOCAL_PLAYER_SCORE_BY(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iPointsPerKill,TRUE)
																ENDIF
															ENDIF
														ENDIF
													ENDIF
												ELSE
													PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - In lost and damned sudden death, no points awarded")
												ENDIF
												
												// Award, Don't let the enemy get the merchandise back to their base. A strategic ambush could turn the match in your favor. Kill package carriers.
												IF Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer)
													IF MC_playerBD[iVictimpart].iObjCarryCount > 0
														PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - AWARD, MP_AWARD_KILL_CARRIER_CAPTURE ")
														INCREMENT_MP_INT_CHARACTER_AWARD(MP_AWARD_KILL_CARRIER_CAPTURE)
													ELSE
														PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - AWARD, MP_AWARD_KILL_CARRIER_CAPTURE  = ", MC_playerBD[iVictimpart].iPedCarryCount)
													ENDIF
												ENDIF
												
												IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam] < FMMC_MAX_RULES
													INT iNewTeam = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iSwapToTeamOnKill[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]]
													
													IF iNewTeam != -1
													AND iNewTeam != MC_playerBD[iPartToUse].iteam
														PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Killed a player, swapping to team ", iNewTeam)
														IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciTEAM_SWITCH_SHARDS)
															PRINTLN("[RCC MISSION] Playing Winning_Team_Shard 2")
															STRING sWinText
															IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
																sWinText= "JUGGS_WIN"
																IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciTRADING_PLACES_TIME_BAR_ENABLED) AND g_FMMC_STRUCT.iTradingPlacesTimeBarDuration != -1
																	sWinText = "JUGGS_WIN_ALT"
																ENDIF
																PLAY_SOUND_FRONTEND(-1, "Become_JN", "DLC_IE_JN_Player_Sounds", FALSE)
																SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_GENERIC_TEXT, "", sWinText, "JUGGS_JUGG")
															ELSE
																sWinText= "TS_WIN"
																IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciTRADING_PLACES_TIME_BAR_ENABLED) AND g_FMMC_STRUCT.iTradingPlacesTimeBarDuration != -1
																	sWinText = "TS_WIN_ALT"
																ENDIF
																IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_TRADING_PLACES_SOUNDS)
																	PLAY_SOUND_FRONTEND(-1, "Become_JN", "DLC_BTL_TP_Remix_Juggernaut_Player_Sounds", FALSE)
																ELSE
																	PLAY_SOUND_FRONTEND(-1, "Winning_Team_Shard", "DLC_Exec_TP_SoundSet", FALSE)
																ENDIF
																SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_GENERIC_TEXT, "", sWinText, "TGIG_WINNER")
															ENDIF
														ENDIF
														CHANGE_PLAYER_TEAM(iNewTeam, TRUE)
														BROADCAST_FMMC_OBJECTIVE_PLAYER_SWITCHED_TEAMS(MC_playerBD[iLocalPart].iteam, iNewTeam)
													ENDIF
												ENDIF
												
												IF sEntityID.IsHeadShot
													MC_playerBD[iPartToUse].iNumHeadshots++
	//												IF MC_playerBD[iPartToUse].iCurrentLoc != -1
	//													PRINT_TICKER_WITH_INT("CONBTIC",5)
	//													MC_playerBD[iPartToUse].iNumControlKills = MC_playerBD[iPartToUse].iNumControlKills + 5
	//													REINIT_NET_TIMER(tdcontolHUDFlash)
	//													PRINTLN("[RCC MISSION] - mission Controller - PROCESS_EVENTS - PLAYER HEADSHOT control kills increased to: ",MC_playerBD[iPartToUse].iNumControlKills)
	//												ENDIF
												ELSE
													CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_ALL_HEADSHOTS)
	//												IF MC_playerBD[iPartToUse].iCurrentLoc != -1
	//													PRINT_TICKER_WITH_INT("CONBTIC",2)
	//													REINIT_NET_TIMER(tdcontolHUDFlash)
	//													MC_playerBD[iPartToUse].iNumControlKills = MC_playerBD[iPartToUse].iNumControlKills + 2
	//													PRINTLN("[RCC MISSION] - mission Controller - PROCESS_EVENTS - PLAYER Normal control kills increased to: ",MC_playerBD[iPartToUse].iNumControlKills)
	//												ENDIF
												ENDIF
												
											ELIF LocalPlayer = VictimPlayerID
												IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[ikillerpart].iteam] < FMMC_MAX_RULES
												AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[ikillerpart].iteam].iRuleBitsetSeven[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[ikillerpart].iteam]], ciBS_RULE7_SUBTRACT_POINTS_FROM_TEAM)
													IF MC_playerBD[iPartToUse].iPlayerScore > 0 AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
														IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[ikillerpart].iteam].iObjectiveScore[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[ikillerpart].iteam]]  > 0
															IF MC_playerBD[iPartToUse].iPlayerScore >= g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[ikillerpart].iteam].iObjectiveScore[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[ikillerpart].iteam]]
																PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - decrementing team point by ", g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[ikillerpart].iteam].iPointsPerKill) 
																INCREMENT_LOCAL_PLAYER_SCORE_BY((g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[ikillerpart].iteam].iObjectiveScore[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[ikillerpart].iteam]] * -1),FALSE)
															ELSE
																PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - score too low decrementing team point by ", MC_playerBD[iPartToUse].iPlayerScore) 
																INCREMENT_LOCAL_PLAYER_SCORE_BY((MC_playerBD[iPartToUse].iPlayerScore * -1),FALSE)
															ENDIF
														ELSE
															PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - points not set, decrementing by 1 ") 
															INCREMENT_LOCAL_PLAYER_SCORE_BY(-1,FALSE)
														ENDIF
													ELSE
														IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
															//Show help
															BROADCAST_FMMC_CANT_DECREMENT_SCORE(iKillerPart)															
														ENDIF															
													ENDIF
												ENDIF
												
												IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciENABLE_LOST_AND_DAMNED_SOUNDS)
													IF MC_playerBD[iPartToUse].iTeam = 0
														PRINTLN("[RCC MISSION][KH] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Playing LAD devil death sound")
														IF NOT IS_ENTITY_ALIVE(PlayerPedToUse)
															PLAY_SOUND_FROM_ENTITY(-1, "Night_Die", PlayerPedToUse, "DLC_Biker_LostAndDamned_Sounds", TRUE, 30)
														ENDIF
													ELIF MC_playerBD[iPartToUse].iTeam = 1
														PRINTLN("[RCC MISSION][KH] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Playing LAD angel death sound")
														IF NOT IS_ENTITY_ALIVE(PlayerPedToUse)
															PLAY_SOUND_FROM_ENTITY(-1, "Day_Die", PlayerPedToUse, "DLC_Biker_LostAndDamned_Sounds", TRUE, 30)
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ELIF MC_playerBD[iVictimpart].iteam = MC_playerBD[ikillerpart].iteam
											IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciNEGATIVE_SUICIDE_SCORE)
											AND ((NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRQUOTA(g_FMMC_STRUCT.iAdversaryModeType)) OR LocalPlayer = KillerPlayerID)
											AND MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES	
												IF bIsLocalPlayerHost
													INT igamestage = GET_MC_CLIENT_MISSION_STAGE(ikillerpart)
													PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - GET_MC_CLIENT_MISSION_STAGE: ",igamestage) 
													PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - host subtracting team kills ") 
													IF igamestage = CLIENT_MISSION_STAGE_KILL_PLAYERS
														IF MC_serverBD.iTeamPlayerKills[MC_playerBD[ikillerpart].iteam][MC_playerBD[iVictimpart].iteam] > 0
															MC_serverBD.iTeamPlayerKills[MC_playerBD[ikillerpart].iteam][MC_playerBD[iVictimpart].iteam]--
															PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - MC_serverBD.iTeamPlayerKills decreased")
														ENDIF
													ELIF igamestage = CLIENT_MISSION_STAGE_KILL_TEAM0
														IF MC_playerBD[iVictimpart].iteam = 0
														AND MC_serverBD.iTeamPlayerKills[MC_playerBD[ikillerpart].iteam][MC_playerBD[iVictimpart].iteam] > 0
															MC_serverBD.iTeamPlayerKills[MC_playerBD[ikillerpart].iteam][MC_playerBD[iVictimpart].iteam]--
															PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - MC_serverBD.iTeamPlayerKills of team 0 decreased for team: ",MC_playerBD[ikillerpart].iteam) 
														ENDIF
													ELIF igamestage = CLIENT_MISSION_STAGE_KILL_TEAM1
														IF MC_playerBD[iVictimpart].iteam = 1
														AND MC_serverBD.iTeamPlayerKills[MC_playerBD[ikillerpart].iteam][MC_playerBD[iVictimpart].iteam] > 0
															MC_serverBD.iTeamPlayerKills[MC_playerBD[ikillerpart].iteam][MC_playerBD[iVictimpart].iteam]--
															PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - MC_serverBD.iTeamPlayerKills of team 1 decreased for team: ",MC_playerBD[ikillerpart].iteam)
														ENDIF
													ELIF igamestage = CLIENT_MISSION_STAGE_KILL_TEAM2
														IF MC_playerBD[iVictimpart].iteam = 2
														AND MC_serverBD.iTeamPlayerKills[MC_playerBD[ikillerpart].iteam][MC_playerBD[iVictimpart].iteam] > 0
															MC_serverBD.iTeamPlayerKills[MC_playerBD[ikillerpart].iteam][MC_playerBD[iVictimpart].iteam]--
															PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - MC_serverBD.iTeamPlayerKills of team 2 decreased for team: ",MC_playerBD[ikillerpart].iteam)
														ENDIF
													ELIF igamestage = CLIENT_MISSION_STAGE_KILL_TEAM3
														IF MC_playerBD[iVictimpart].iteam = 3
														AND MC_serverBD.iTeamPlayerKills[MC_playerBD[ikillerpart].iteam][MC_playerBD[iVictimpart].iteam] > 0
															MC_serverBD.iTeamPlayerKills[MC_playerBD[ikillerpart].iteam][MC_playerBD[iVictimpart].iteam]--
															PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - MC_serverBD.iTeamPlayerKills of team 3 decreased for team: ",MC_playerBD[ikillerpart].iteam) 
														ENDIF
													ENDIF
												ENDIF
												
												IF LocalPlayer = KillerPlayerID
													PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - We are the Killer and Victim. PlayerScore: ", MC_playerBD[iPartToUse].iPlayerScore, " PlayerKills: ", MC_Playerbd[iPartToUse].iKillScore) 
													
	//												IF MC_playerBD[iPartToUse].iNumPlayerKills > 0
	//													MC_playerBD[iPartToUse].iNumPlayerKills--
	//													PRINTLN("[JS] - SUICIDE POINTS - decrementing local player's kills")
	//												ENDIF
													
													IF MC_serverBD.iNumPlayerRuleHighestPriority[MC_playerBD[iPartToUse].iteam] > 0 
														PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - more than 0 highest ") 
														IF MC_serverBD_4.iPlayerRuleMissionLogic[MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL_PLAYERS // this high priority kill player objective
															IF MC_playerBD[iPartToUse].iPlayerScore > 0
																IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciHARD_TARGET_ENABLE_MODE) AND IS_BIT_SET(MC_PlayerBD[iVictimpart].iClientBitSet3, PBBOOL3_PLAYER_IS_A_HARD_TARGET)
																OR NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciHARD_TARGET_ENABLE_MODE)
																	PRINTLN("[JS] - SUICIDE POINTS - decrementing local player's score (1)")
																	INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(-1)
																ENDIF
															ENDIF
														ELIF MC_serverBD_4.iPlayerRuleMissionLogic[MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL_TEAM0	
														OR MC_serverBD_4.iPlayerRuleMissionLogic[MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL_TEAM1	
														OR MC_serverBD_4.iPlayerRuleMissionLogic[MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL_TEAM2
														OR MC_serverBD_4.iPlayerRuleMissionLogic[MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL_TEAM3
															IF MC_playerBD[iPartToUse].iPlayerScore > 0
															AND MC_Playerbd[iPartToUse].iKillScore > 0
																IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciHARD_TARGET_ENABLE_MODE) AND IS_BIT_SET(MC_PlayerBD[iVictimpart].iClientBitSet3, PBBOOL3_PLAYER_IS_A_HARD_TARGET)
																OR NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciHARD_TARGET_ENABLE_MODE)
																	PRINTLN("[JS] - SUICIDE POINTS - decrementing local player's score (2)")
																	INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(-1,TRUE)
																ENDIF
															ENDIF
														ENDIF	
													ELSE
														IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iPointsPerKill > 0
															IF MC_playerBD[iPartToUse].iPlayerScore >= g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iPointsPerKill
																PRINTLN("[JS] - SUICIDE POINTS - iPoints per kill = ", g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iPointsPerKill)
																INCREMENT_LOCAL_PLAYER_SCORE_BY((g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iPointsPerKill * -1),TRUE)
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
											IF iVictimPart = iKillerPart
												PRIVATE_PROCESS_ENTITY_SUICIDE_EVENT(VictimPlayerID, iVictimPart, ikillerpart)
											ENDIF
										ENDIF
									ELSE
										IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
											INT iNewTeam = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iSwapToTeamOnDeath[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]]
											
											IF iNewTeam != -1
											AND iNewTeam != MC_playerBD[iLocalPart].iteam
												IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iDontSwitchOnSuicide[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]], ciDYNAMIC_MENU_DONT_SWITCH_ON_SUICIDE)
													IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_SHOULD_SHOW_TEAM_SWITCH_SHARD)
														SET_BIT(iLocalBoolCheck16, LBOOL16_SHOULD_SHOW_TEAM_SWITCH_SHARD)
													ENDIF
													CHANGE_PLAYER_TEAM(iNewTeam, TRUE)
													BROADCAST_FMMC_OBJECTIVE_PLAYER_SWITCHED_TEAMS(MC_playerBD[iLocalPart].iteam, iNewTeam)
													PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Died, swapping to team ", iNewTeam)
												ELSE
													PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Died (no killer, not suicide), not switching team")
												ENDIF
											ENDIF
										ENDIF

										IF MC_playerBD[ivictimpart].iteam >= 0
											IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[ivictimpart].iteam].iTeamBitset2, ciBS2_CUSTOM_WASTED_SHARD)
												IF ivictimpart = PARTICIPANT_ID_TO_INT()
													PROCESS_CUSTOM_WASTED_SHARD(MC_playerBD[ivictimpart].iteam, ivictimpart, ikillerpart)
												ENDIF
											ENDIF
											
											IF MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iVictimPart].iteam] < FMMC_MAX_RULES
											AND g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iVictimPart].iteam].iTeamEliminationScore[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iVictimPart].iteam]] > 0
												IF NOT IS_BIT_SET(MC_serverBD_1.iServerRespawnBitset, SRBS_RESPAWN_FROM_SPECTATOR_NOW_TEAM_0 + MC_PlayerBD[iVictimPart].iteam)
													IF NOT IS_BIT_SET(MC_serverBD_1.iServerRespawnBitset, SRBS_RESPAWN_CHECK_ALL_DEAD_NOW_TEAM_0 + MC_PlayerBD[iVictimPart].iteam)
														SET_BIT(MC_serverBD_1.iServerRespawnBitset, SRBS_RESPAWN_CHECK_ALL_DEAD_NOW_TEAM_0 + MC_PlayerBD[iVictimPart].iteam)
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									// Condemned
									PROCESS_CONDEMNED_KILL(iVictimPart, iKillerPart)
									
									IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_MONSTER_JAM(g_FMMC_STRUCT.iAdversaryModeType)
										IF MC_PlayerBD[iVictimPart].iteam = 1
										AND MC_PlayerBD[iLocalPart].iTeam != 1
										AND LocalPlayer != VictimPlayerID
											g_sArena_Telemetry_data.m_gladiatorKills++
											PRINTLN("[ARENA][TEL] - Gladiator Kills ", g_sArena_Telemetry_data.m_gladiatorKills)
										ENDIF
									ENDIF
								ENDIF
							ELSE
								PRIVATE_PROCESS_ENTITY_SUICIDE_EVENT(VictimPlayerID, iVictimPart)
							ENDIF
						ELSE
							//did we drown maybe?
							PRIVATE_PROCESS_ENTITY_SUICIDE_EVENT(VictimPlayerID, iVictimPart)
						ENDIF
						
						//Sumo custom wasted shard
						IF MC_playerBD[ivictimpart].iteam >= 0
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[ivictimpart].iteam].iTeamBitset2, ciBS2_CUSTOM_WASTED_SHARD)
								IF g_FMMC_STRUCT.iCustomWastedShardType[MC_playerBD[ivictimpart].iteam] = ciCUSTOM_WASTED_SHARD_SUMO
									IF ivictimpart = PARTICIPANT_ID_TO_INT()
										PROCESS_CUSTOM_WASTED_SHARD(MC_playerBD[ivictimpart].iteam, ivictimpart, ikillerpart)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
					ENDIF
				ELSE

					IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
						IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
							Killerped = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
							IF IS_PED_A_PLAYER(Killerped)
								IF LocalPlayerPed = Killerped
									IF MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] < FMMC_MAX_RULES
										IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitSetSix[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]], ciBS_RULE6_ENABLE_HEALTH_DRAIN)
											PRINTLN("[JS] [HEALTHDRAIN] - PROCESS_HEALTH_DRAIN_DAMAGE_EVENTS - Killed a non-player ped")
											fDrainHealPercentage += g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].sDrainStruct[ MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] ].fPedHeal
										ENDIF
									ENDIF
									
									IF IS_THIS_MISSION_A_CASINO_HEIST_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
									AND IS_LOCAL_PLAYER_USING_DRONE()
									AND sEntityID.WeaponUsed = ENUM_TO_INT(WEAPONTYPE_DLC_TRANQUILIZER)
										PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT (Destroyed) - Incrementing MP_AWARD_ASLEEPONJOB to ", GET_MP_INT_CHARACTER_AWARD(MP_AWARD_ASLEEPONJOB))
										INCREMENT_MP_INT_CHARACTER_AWARD(MP_AWARD_ASLEEPONJOB)
									ENDIF
									
									IF g_iInteriorTurretSeat > -1
										MC_playerBD_1[iLocalPart].iGunCameraKills++
										PRINTLN("[JS] - PROCESS_HEALTH_DRAIN_DAMAGE_EVENTS - Killed Ped with Gun Camera. Total: ", MC_playerBD_1[iLocalPart].iGunCameraKills)
									ENDIF
								
									IF ipedid = -2
										ipedid = IS_ENTITY_A_MISSION_CREATOR_ENTITY(Victimped)
									ENDIF
									
									IF ipedid >= 0
										
										//THIS IS A MISSION CREATOR PLACED PED!
										
										BOOL bGiveRP = TRUE
										
										FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
											IF MC_serverBD_4.iPedRule[ipedid][iteam] = FMMC_OBJECTIVE_LOGIC_CROWD_CONTROL
												bGiveRP = FALSE
											ENDIF
										ENDFOR
										
										
										IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[ipedid].iTeam[MC_playerBD[iPartToUse].iteam] = ciPED_RELATION_SHIP_HATE
										AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[ipedid].iPedBitsetFourteen, ciPED_BSFourteen_PedIsACivilian)
											IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
												IF NOT IS_MODEL_AMBIENT_COP_FOR_RP(g_FMMC_STRUCT_ENTITIES.sPlacedPed[ipedid].mn)
													IF Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer)
														IF MC_playerBD[iPartToUse].iNumPedKills < 20
															GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD,Victimped, "XPT_KAIE",XPTYPE_ACTION,XPCATEGORY_ACTION_KILLS, g_sMPTunables.ixp_tuneable_kill_capture_ped, 1)
														ENDIF
													ELSE
														IF MC_playerBD[iPartToUse].iNumPedKills < g_sMPTunables.iMISSION_AI_KILL_AMOUNT_CAP
														AND bGiveRP
															PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION, 1 ")
															GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD,Victimped, "XPT_KAIE",XPTYPE_ACTION,XPCATEGORY_ACTION_KILLS, g_sMPTunables.iMISSION_AI_KILL_RP, 1)
														ELSE
															PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION, FAIL 1, iNumPedKills = ", MC_playerBD[iPartToUse].iNumPedKills, " CAP = ", g_sMPTunables.iMISSION_AI_KILL_AMOUNT_CAP)
														ENDIF
													ENDIF
												ENDIF
											ENDIF
											
											PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - MC_playerBD[iPartToUse].iNumPedKills test 0 MC_playerBD[iPartToUse].iNumPedKills BEFORE ", MC_playerBD[iPartToUse].iNumPedKills) 
											
											MC_playerBD[iPartToUse].iNumPedKills++
											
											PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - MC_playerBD[iPartToUse].iNumPedKills test 0 - iNumPedKills = ", MC_playerBD[iPartToUse].iNumPedKills)
											
											IF(g_FMMC_STRUCT_ENTITIES.sPlacedPed[ipedid].mn = S_M_Y_PRISONER_01)
												MC_playerBD[iPartToUse].iPrisonersKilled++
											ENDIF
											
										ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[ipedid].iTeam[MC_playerBD[iPartToUse].iteam] = ciPED_RELATION_SHIP_DISLIKE										
											IF WILL_PED_EVER_BE_HOSTILE(ipedid)
											AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[ipedid].iPedBitsetFourteen, ciPED_BSFourteen_PedIsACivilian)
												IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
													IF NOT IS_MODEL_AMBIENT_COP_FOR_RP(g_FMMC_STRUCT_ENTITIES.sPlacedPed[ipedid].mn)
														IF Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer)
															IF MC_playerBD[iPartToUse].iNumPedKills < 20
																GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD,Victimped, "XPT_KAIE",XPTYPE_ACTION,XPCATEGORY_ACTION_KILLS, g_sMPTunables.ixp_tuneable_kill_capture_ped, 1)
															ENDIF
														ELSE
															IF MC_playerBD[iPartToUse].iNumPedKills < g_sMPTunables.iMISSION_AI_KILL_AMOUNT_CAP
															AND bGiveRP	
																PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION, 2 ")
																GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD,Victimped, "XPT_KAIE",XPTYPE_ACTION,XPCATEGORY_ACTION_KILLS, g_sMPTunables.iMISSION_AI_KILL_RP, 1)
															ELSE
																PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION, FAIL 2, iNumPedKills = ", MC_playerBD[iPartToUse].iNumPedKills, " CAP = ", g_sMPTunables.iMISSION_AI_KILL_RP)
															ENDIF
														ENDIF
													ENDIF
												ENDIF
												
												MC_playerBD[iPartToUse].iNumPedKills++
												
												PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - MC_playerBD[iPartToUse].iNumPedKills test 1")
												
												IF(g_FMMC_STRUCT_ENTITIES.sPlacedPed[ipedid].mn = S_M_Y_PRISONER_01)
													MC_playerBD[iPartToUse].iPrisonersKilled++
												ENDIF
												
											ELSE
												PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - MC_playerBD[iPartToUse].iNumPedKills test 4")
												MC_playerBD[iPartToUse].iCivilianKills++
											ENDIF
											
											PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - MC_playerBD[iPartToUse].iNumPedKills test 5")
											
										ENDIF
										
										IF DOES_BLIP_EXIST(biHostilePedBlip[ipedid].BlipID)
											PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Clearing Blip biHostilePedBlip: ", ipedid)
											CLEANUP_AI_PED_BLIP(biHostilePedBlip[ipedid])
										ELIF DOES_BLIP_EXIST(biPedBlip[ipedid])
											PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Clearing Blip biPedBlip: ", ipedid)
											REMOVE_BLIP(biPedBlip[ipedid])
										ENDIF
										
										PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - MC_playerBD[iPartToUse].iNumPedKills = ", MC_playerBD[iPartToUse].iNumPedKills)
										
										PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - ped id: ",ipedid) 
										IF MC_serverBD_4.iPedPriority[ipedid][MC_playerBD[iPartToUse].iteam] = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]

											NET_PRINT("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - ped is current objective: ") NET_PRINT_INT(ipedid) NET_NL()
											IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[ipedid].iTeam[MC_playerBD[iPartToUse].iteam] = ciPED_RELATION_SHIP_DISLIKE
											OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[ipedid].iTeam[MC_playerBD[iPartToUse].iteam] = ciPED_RELATION_SHIP_HATE
												BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_KILL_PED,0,MC_playerBD[iPartToUse].iteam,-1,NULL,ci_TARGET_PED,ipedid)
											ELSE
												BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_KILL_FRIENDLY,0,MC_playerBD[iPartToUse].iteam,-1,NULL,ci_TARGET_PED,ipedid)
											ENDIF
											
											IF MC_serverBD_4.iPedPriority[ipedid][MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
												IF  MC_serverBD_4.iPedRule[ipedid][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL
													BROADCAST_OBJECTIVE_MID_POINT(MC_serverBD_4.iPedPriority[ipedid][MC_playerBD[iPartToUse].iteam],MC_playerBD[iPartToUse].iteam)
													PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - ped is kill objective: ",ipedid) 
													IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_GUNSMITH(g_FMMC_STRUCT.iAdversaryModeType)
														IF IS_POINT_TO_INCREMENT_VALID_FOR_RULE_WEAPON(INT_TO_ENUM(WEAPON_TYPE, sEntityID.WeaponUsed), MC_playerBD[iPartToUse].iteam, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
															INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(DEFAULT, DEFAULT, DEFAULT, ipedid)
														ENDIF
													ELSE
														INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(DEFAULT, DEFAULT, DEFAULT, ipedid)
													ENDIF
												ELSE
													GIVE_TEAM_POINTS_FOR_PED_KILL(ipedid,Victimped,Killerped)												
												ENDIF
											ELSE
												GIVE_TEAM_POINTS_FOR_PED_KILL(ipedid,Victimped,Killerped)
											ENDIF
										ELSE
											GIVE_TEAM_POINTS_FOR_PED_KILL(ipedid,Victimped,Killerped)
											PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - ped is not current objective: ",ipedid)
											IF MC_serverBD_4.iPedPriority[ipedid][MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
												IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[ipedid].iTeam[MC_playerBD[iPartToUse].iteam] = ciPED_RELATION_SHIP_DISLIKE
												OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[ipedid].iTeam[MC_playerBD[iPartToUse].iteam] = ciPED_RELATION_SHIP_HATE
													PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - ped hates youe: ") NET_PRINT_INT(ipedid) NET_NL()
													BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_KILL_PED,0,MC_playerBD[iPartToUse].iteam,-1,NULL,ci_TARGET_PED,ipedid)
												ELSE
													BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_KILL_FRIENDLY,0,MC_playerBD[iPartToUse].iteam,-1,NULL,ci_TARGET_PED,ipedid)
												ENDIF
											ENDIF
										ENDIF
										
										PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - MC_playerBD[iPartToUse].iNumHeadshots test 0")
										IF sEntityID.IsHeadShot
											MC_playerBD[iPartToUse].iNumHeadshots++
											PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - MC_playerBD[iPartToUse].iNumHeadshots test 1", MC_playerBD[iPartToUse].iNumHeadshots)
//											IF MC_playerBD[iPartToUse].iCurrentLoc != -1
//												PRINT_TICKER_WITH_INT("CONBTIC",2)
//												MC_playerBD[iPartToUse].iNumControlKills = MC_playerBD[iPartToUse].iNumControlKills + 2
//												REINIT_NET_TIMER(tdcontolHUDFlash)
//												PRINTLN("[RCC MISSION] - mission Controller - PROCESS_EVENTS - PED HEADSHOT control kills increased to: ",MC_playerBD[iPartToUse].iNumControlKills)
//											ENDIF
										ELSE
											PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - MC_playerBD[iPartToUse].iNumHeadshots test 2", MC_playerBD[iPartToUse].iNumHeadshots)
											CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_ALL_HEADSHOTS)
//											IF MC_playerBD[iPartToUse].iCurrentLoc != -1
//												PRINT_TICKER_WITH_INT("CONBTIC",1)
//												MC_playerBD[iPartToUse].iNumControlKills++
//												REINIT_NET_TIMER(tdcontolHUDFlash)
//												PRINTLN("[RCC MISSION] - mission Controller - PROCESS_EVENTS - PLAYER Normal control kills increased to: ",MC_playerBD[iPartToUse].iNumControlKills) 
//											ENDIF
										ENDIF
										
										IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[ipedid].iPedBitsetTwelve, ciPed_BSTwelve_UseDamageScoreContribution)
											MC_playerBD_1[iPartToUse].iDamageToPedsForMedal += ROUND(sEntityID.Damage)
											PRINTLN("[JS] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - damaged specific ped (dead): ", ipedid, " Total: ", MC_playerBD_1[iPartToUse].iDamageToPedsForMedal)
										ENDIF
									ELSE
										INT iChasePed = IS_ENTITY_A_CHASE_PED(Victimped)
										
										IF iChasePed != -1
											
											//THIS IS A GANG CHASE PED!
											
											PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - killed chase ped: ", iChasePed)
											
											IF NOT IS_MODEL_AMBIENT_COP_FOR_RP(GET_ENTITY_MODEL(Victimped))
												IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
													IF Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer)
														IF MC_playerBD[iPartToUse].iNumPedKills < 20
															GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD,Victimped, "XPT_KAIE",XPTYPE_ACTION,XPCATEGORY_ACTION_KILLS, g_sMPTunables.ixp_tuneable_kill_capture_ped, 1, -1, "", TRUE)
														ENDIF
													ELSE
														IF MC_playerBD[iPartToUse].iNumPedKills < g_sMPTunables.iMISSION_AI_KILL_AMOUNT_CAP
															PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION, 3 ")	
															GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD,Victimped, "XPT_KAIE",XPTYPE_ACTION,XPCATEGORY_ACTION_KILLS, g_sMPTunables.iMISSION_AI_KILL_RP, 1, -1, "", TRUE)
														ELSE
															PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION, FAIL 3, iNumPedKills = ", MC_playerBD[iPartToUse].iNumPedKills, " CAP = ", g_sMPTunables.iMISSION_AI_KILL_AMOUNT_CAP)
														ENDIF
													ENDIF
												ENDIF
											ENDIF
											
											MC_playerBD[iPartToUse].iNumPedKills++
											
										ELSE
											
											//THIS IS AN AMBIENT PED!
											
											IF NOT IS_MODEL_AMBIENT_COP_FOR_RP(GET_ENTITY_MODEL(Victimped))
												MC_playerBD[iPartToUse].iCivilianKills++
												PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT -iCivilianKills increased to ",MC_playerBD[iPartToUse].iCivilianKills)
											ELSE
												PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT -killed ambient cop")
												MC_playerBD[iPartToUse].iAmbientCopsKilled++
											ENDIF
										ENDIF
									ENDIF
								ELSE
									GIVE_TEAM_POINTS_FOR_PED_KILL(ipedid,Victimped,Killerped)
								ENDIF
							ELSE
								GIVE_TEAM_POINTS_FOR_PED_KILL(ipedid,Victimped)
							ENDIF
						ELSE
							GIVE_TEAM_POINTS_FOR_PED_KILL(ipedid,Victimped)
						ENDIF
					ELSE
						GIVE_TEAM_POINTS_FOR_PED_KILL(ipedid,Victimped)
					ENDIF
					
					//Invisible ped flickering effect
					IF ipedid = -2
						ipedid = IS_ENTITY_A_MISSION_CREATOR_ENTITY(Victimped)
					ENDIF
					
					IF ipedid >= 0
						IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxFlickeringInvisiblePedEffects[ipedid])
							REMOVE_PARTICLE_FX(ptfxFlickeringInvisiblePedEffects[ipedid])
							PRINTLN("[FlickeringPeds] - removing particles for ped ", ipedid, " because it's dead")
						ENDIF
					ENDIF
		
					IF bIsLocalPlayerHost
						
						IF ipedid = -2
							ipedid = IS_ENTITY_A_MISSION_CREATOR_ENTITY(Victimped)
						ENDIF
						
						IF ipedid >= 0
						AND NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[ipedid])
							//If we should be tracking damage on this ped:
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[ipedid].iPedBitset , ciPED_BS_TrackDamage)
								IF GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
									MC_serverBD_2.fVIPDamage += ((sEntityID.Damage / GET_ENTITY_MAX_HEALTH(victimPed)) * 100)
									PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT 1 - sEntityID.Damage = ",sEntityID.Damage, " percentage = ",((sEntityID.Damage / GET_ENTITY_MAX_HEALTH(victimPed)) * 100)," MC_serverBD_2.fVIPDamage = ",MC_serverBD_2.fVIPDamage)
								ENDIF
							ENDIF
							
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[ipedid].iPedBitsetFive, ciPED_BSFive_AggroWhenHurtOrKilled)
							AND NOT IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(ipedid)], GET_LONG_BITSET_BIT(ipedid))
							AND NOT IS_BIT_SET( iPedHitByStunGun[GET_LONG_BITSET_INDEX(ipedid)], GET_LONG_BITSET_BIT(ipedid))
								PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Ped ",ipedid," SPOOKED as ped killed had ciPED_BSFive_AggroWhenHurtOrKilled set")
								
								#IF IS_DEBUG_BUILD SET_PED_IS_SPOOKED_DEBUG(ipedid) #ENDIF
								SET_BIT(iPedLocalSpookedBitset[GET_LONG_BITSET_INDEX(ipedid)], GET_LONG_BITSET_BIT(ipedid)) // broadcast
								SET_BIT(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(ipedid)], GET_LONG_BITSET_BIT(ipedid))
								BROADCAST_FMMC_PED_SPOOKED(ipedid)
								
								FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
									IF SHOULD_AGRO_FAIL_FOR_TEAM(ipedid, iTeam)
										SET_BIT(iPedLocalAggroedBitset[GET_LONG_BITSET_INDEX(ipedid)], GET_LONG_BITSET_BIT(ipedid))
										TRIGGER_AGGRO_ON_PED(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[ipedid]),ipedid)
										iTeam = MC_serverBD.iNumberOfTeams // Break out, we found an aggro team!
									ENDIF
								ENDFOR
								
								SET_BIT(iPedLocalReceivedEvent_HurtOrKilled[GET_LONG_BITSET_INDEX(ipedid)], GET_LONG_BITSET_BIT(ipedid))
								BROADCAST_FMMC_PED_SPOOK_HURTORKILLED(ipedid)
							ENDIF
							
							IF sEntityID.WeaponUsed = ENUM_TO_INT(WEAPONTYPE_DLC_TRANQUILIZER)
								IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_PedSpecialTracking)
								AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[ipedid].iContinuityId > -1
								AND g_FMMC_STRUCT.eContinuitySpecialPedType = FMMC_MISSION_CONTINUITY_PED_TRACKING_TYPE_TRANQUILIZED
								AND NOT IS_LONG_BIT_SET(MC_serverBD_1.sLEGACYMissionContinuityVars.iPedSpecialBitset, g_FMMC_STRUCT_ENTITIES.sPlacedPed[ipedid].iContinuityId)
									SET_LONG_BIT(MC_serverBD_1.sLEGACYMissionContinuityVars.iPedSpecialBitset, g_FMMC_STRUCT_ENTITIES.sPlacedPed[ipedid].iContinuityId)
									PRINTLN("[JS][CONTINUITY] - PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - ped ", ipedid, " with continuityID ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[ipedid].iContinuityId, " was tranqed")
								ENDIF
							ELSE	
								IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_PedDeathTracking)
								AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[ipedid].iContinuityId > -1
									SET_LONG_BIT(MC_serverBD_1.sLEGACYMissionContinuityVars.iPedDeathBitset, g_FMMC_STRUCT_ENTITIES.sPlacedPed[ipedid].iContinuityId)
									PRINTLN("[JS][CONTINUITY] - PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - ped ", ipedid, " with continuityID ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[ipedid].iContinuityId, " was killed")
								ENDIF
							ENDIF
							
							FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
								IF NOT HAS_TEAM_FAILED(iTeam)
								AND MC_serverBD.iNumPedHighestPriority[iteam] = 1 // This has recalculate_objective_logic in it, so we don't want to go crazy on massive kill rules
								AND MC_serverBD_4.iPedPriority[ipedid][iteam] <= MC_serverBD_4.iCurrentHighestPriority[iteam]
								AND MC_serverBD_4.iPedRule[ipedid][iteam] = FMMC_OBJECTIVE_LOGIC_KILL
									RUN_TEAM_PED_FAIL_CHECKS(ipedid,iteam,MC_serverBD_4.iPedPriority[ipedid][iteam])
									RECALCULATE_OBJECTIVE_LOGIC(iteam)
								ENDIF
							ENDFOR
							
							IF NOT IS_BIT_SET(MC_serverBD_4.iPedKilledBS[GET_LONG_BITSET_INDEX(ipedid)], GET_LONG_BITSET_BIT(ipedid))
								PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Setting ped killed bit ", ipedid)
								SET_BIT(MC_serverBD_4.iPedKilledBS[GET_LONG_BITSET_INDEX(ipedid)], GET_LONG_BITSET_BIT(ipedid))
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
			ELIF IS_ENTITY_A_VEHICLE(sEntityID.VictimIndex)
				
				PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - event ",iCount," - entity was veh - DESTROYED / UNDRIVEABLE")
				
				VEHICLE_INDEX Victimveh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)
				IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
					PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - DOES_ENTITY_EXIST(sEntityID.DamagerIndex)")
					IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
						Killerped = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
						IF IS_PED_A_PLAYER(Killerped)

							IF LocalPlayerPed = Killerped
								IF MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] < FMMC_MAX_RULES
									IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitSetSix[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]], ciBS_RULE6_ENABLE_HEALTH_DRAIN)
										PRINTLN("[JS] [HEALTHDRAIN] - PROCESS_HEALTH_DRAIN_DAMAGE_EVENTS - Destroyed a vehicle")
										fDrainHealPercentage += g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].sDrainStruct[ MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] ].fVehicleHeal
									ENDIF
								ENDIF
								
								IF ivehID = -2
									ivehID = IS_VEH_A_MISSION_CREATOR_VEH(Victimveh)
								ENDIF
								
								IF ivehID >= 0
									PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - ivehID (",ivehID,") >= 0") 
									IF NOT IS_BIT_SET(MC_serverBD_2.iVehIsDestroyed, ivehID)
								
										//NET_PRINT("[RCC] - mission Controller - PROCESS_EVENTS - veh id: ") NET_PRINT_INT(ivehID) NET_NL()
										IF MC_serverBD_4.iVehPriority[ivehID][MC_playerBD[iPartToUse].iteam] = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
											PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - veh is the current priority") 
											BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_KILL_VEH,0,MC_playerBD[iPartToUse].iteam,-1,NULL,ci_TARGET_VEHICLE,ivehID)
											IF MC_serverBD_4.iVehPriority[ivehID][MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
												//NET_PRINT("logic is not ignore") NET_NL()
												IF MC_serverBD_4.ivehRule[ivehID][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL
													IF NOT IS_BIT_SET(iCountThisVehAsDestroyedBS, ivehID)
														PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - logic is kill") 
														BROADCAST_OBJECTIVE_MID_POINT(MC_serverBD_4.iVehPriority[ivehID][MC_playerBD[iPartToUse].iteam],MC_playerBD[iPartToUse].iteam)
														IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_GUNSMITH(g_FMMC_STRUCT.iAdversaryModeType)
															IF IS_POINT_TO_INCREMENT_VALID_FOR_RULE_WEAPON(INT_TO_ENUM(WEAPON_TYPE, sEntityID.WeaponUsed), MC_playerBD[iPartToUse].iteam, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
																INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE()
															ENDIF
														ELSE
															INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE()
														ENDIF
														SET_BIT(iCountThisVehAsDestroyedBS, ivehID)
													ELSE
														PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - iCountThisVehAsDestroyedBS is set") 
													ENDIF
												ELSE
													PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - GIVE_TEAM_POINTS_FOR_VEH_KILL") 
													GIVE_TEAM_POINTS_FOR_VEH_KILL(ivehID,Victimveh,Killerped)
												ENDIF
											ELSE
												PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - GIVE_TEAM_POINTS_FOR_VEH_KILL") 
												GIVE_TEAM_POINTS_FOR_VEH_KILL(ivehID,Victimveh,Killerped)
											ENDIF
										ELSE
											PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - GIVE_TEAM_POINTS_FOR_VEH_KILL") 
											GIVE_TEAM_POINTS_FOR_VEH_KILL(ivehID,Victimveh,Killerped)
											FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
												IF MC_serverBD_4.iVehPriority[ivehID][iteam] < FMMC_MAX_RULES
													bnotignore = TRUE
												ENDIF
											ENDFOR
											IF bnotignore
												PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - veh not current prior: ",ivehID) 
												BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_KILL_VEH,0,MC_playerBD[iPartToUse].iteam,-1,NULL,ci_TARGET_VEHICLE,ivehID)
											ENDIF
										ENDIF
										
										//If it's a hostile vehicle:
										IF IS_BIT_SET(iEnemyVehBitSet, ivehID)

											MC_playerBD[iPartToUse].iVehDestroyed++
											
											IF (IS_THIS_MODEL_A_HELI(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[ivehID].mn))
												MC_playerBD[iPartToUse].iChoppersDestroyed++
											ENDIF
											
											printstring("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - ivehID >= 0 MC_playerBD[iPartToUse].iVehDestroyed 1 = ")
											printint(MC_playerBD[iPartToUse].iVehDestroyed)
											printnl()
											
										ENDIF
										
										IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[ivehID].iVehBitsetEight, ciFMMC_VEHICLE8_USE_DAMAGE_SCORE_CONTRIBUTION)
											MC_playerBD_1[iPartToUse].iDamageToVehsForMedal += ROUND(sEntityID.Damage)
											PRINTLN("[JS] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - damaged specific veh (dead): ", ivehID, " Total: ", MC_playerBD_1[iPartToUse].iDamageToVehsForMedal)
										ENDIF
										
									ELSE
										PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - IS_BIT_SET(MC_serverBD_2.iVehIsDestroyed, ivehID)") 
									ENDIF
									
									IF NOT IS_BIT_SET(iVehHasBeenDamagedOrHarmedBS, ivehID)
										PRINTLN("[LM] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Setting iVehHasBeenDamagedOrHarmedBS for iVeh: ", ivehID)
										SET_BIT(iVehHasBeenDamagedOrHarmedBS, ivehID)
									ENDIF
									
								ELSE
									PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - ivehID (",ivehID,") >= 0 - FALSE")
									//Gang backup vehicles:
									IF IS_VEHICLE_FMMC_GANG_BACKUP_VEHICLE(Victimveh)
										MC_playerBD[iPartToUse].iVehDestroyed++
									ENDIF
								ENDIF
								
							ELSE
								GIVE_TEAM_POINTS_FOR_VEH_KILL(ivehID,Victimveh,Killerped)
							ENDIF
						ELSE
							GIVE_TEAM_POINTS_FOR_VEH_KILL(ivehID,Victimveh)
						ENDIF
					ELSE
						GIVE_TEAM_POINTS_FOR_VEH_KILL(ivehID,Victimveh)
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - VEHICLE DESTROYED ITSELF")
					GIVE_TEAM_POINTS_FOR_VEH_KILL(ivehID,Victimveh)
				ENDIF
				
				//VEHICLE DESTROYED
				IF bIsLocalPlayerHost
											
					IF ivehID = -2
						ivehID = IS_VEH_A_MISSION_CREATOR_VEH(Victimveh)
					ENDIF
					
					IF ivehID >= 0
					
						IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_VehicleDestroyedTracking)
						AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[ivehID].iContinuityId > -1
							SET_BIT(MC_serverBD_1.sLEGACYMissionContinuityVars.iVehicleDestroyedTrackingBitset, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[ivehID].iContinuityId)
							PRINTLN("[JS][CONTINUITY] - PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - veh ", ivehID, " with continuityID ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[ivehID].iContinuityId, " was killed")
						ENDIF
					
						//If we should be tracking damage on this ped:
						IF (g_FMMC_STRUCT.iHeistCSVehDmgTrackBitSet > 0)
							
							IF IS_BIT_SET(g_FMMC_STRUCT.iHeistCSVehDmgTrackBitSet, ivehID)
								IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
								AND GET_ENTITY_MODEL(VictimVeh) = AVENGER
									MC_serverBD_2.fHeistVehDamage = (100.0 - GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(VictimVeh, ivehID, MC_serverBD.iTotalNumStartingPlayers, FALSE))
									PRINTLN("[JS] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Overriding damage % for Avenger: ", MC_serverBD_2.fHeistVehDamage)
								ELSE
									MC_serverBD_2.fHeistVehDamage += ((sEntityID.Damage / GET_ENTITY_MAX_HEALTH(Victimveh)) * 100)
									PRINTLN("[MJM] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - VEHICLE DESTROYED adding vehicle damage percentage: ", ((sEntityID.Damage / GET_ENTITY_MAX_HEALTH(Victimveh)) * 100))
									
									IF iCurrentHeistMissionIndex = HBCA_BS_TUTORIAL_CAR
									OR iCurrentHeistMissionIndex = HBCA_BS_PRISON_BREAK_STATION
										VEHICLE_VALUE_DATA vehValueData
										IF GET_VEHICLE_VALUE(vehValueData,GET_ENTITY_MODEL(VictimVeh))
											MC_serverBD_2.iHeistVehRepairCost = vehValueData.iInsurancePremium
										ENDIF
									ENDIF
									PRINTLN("[LK MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - VEHICLE DESTROYED adding SPECIFIC vehicle damage: ", sEntityID.Damage)
									PRINTLN("[LK MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - VEHICLE DESTROYED TOTAL vehicle damage: ", MC_serverBD_2.fHeistVehDamage)	
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			ELIF IS_ENTITY_AN_OBJECT(sEntityID.VictimIndex)
				INT iMyTeam = MC_playerBD[iPartToUse].iteam
			
				OBJECT_INDEX Victimobj = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)
				
				HANDLE_OBJECT_DESTROY_HUD(Victimobj)
				
				IF iobjID = -2
					iobjID = IS_OBJ_A_MISSION_CREATOR_OBJ(Victimobj)
				ENDIF
				
				IF bIsLocalPlayerHost
					IF iobjID >= 0
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobjID].iObjectBitSetTwo, cibsOBJ2_DontSpawnIfDestroyed)
							IF NOT IS_BIT_SET(MC_serverBD_4.iDestroyedTurretBitSet, iobjID)
								SET_BIT(MC_serverBD_4.iDestroyedTurretBitSet, iobjID)
								PRINTLN("[RCC MISSION] Setting MC_serverBD_4.iDestroyedTurretBitSet for object: ", iobjID)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
					IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
						Killerped = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
						IF IS_PED_A_PLAYER(Killerped)		
							IF LocalPlayerPed = Killerped
								IF MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] < FMMC_MAX_RULES
									IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitSetSix[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]], ciBS_RULE6_ENABLE_HEALTH_DRAIN)
										PRINTLN("[JS] [HEALTHDRAIN] - PROCESS_HEALTH_DRAIN_DAMAGE_EVENTS - Destroyed an object adding ",g_FMMC_STRUCT.sFMMCEndConditions[ MC_PlayerBD[iLocalPart].iteam ].sDrainStruct[ MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] ].fObjectTimer, " seconds to the current timer (",(iStopDrainTime/1000) ,"seconds)" )
										iStopDrainTime = ROUND(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].sDrainStruct[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]].fObjectTimer * 1000)
									ENDIF
								ENDIF
							
								IF iobjID = -2
									iobjID = IS_OBJ_A_MISSION_CREATOR_OBJ(Victimobj)
								ENDIF
								IF iobjID >= 0
									
									IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
									AND (GANGOPS_FLOW_GET_CURRENT_GANGOPS_MISSION() = ciGANGOPS_FLOW_MISSION_SPYPLANE)
									AND (g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobjID].mn = XM_PROP_SAM_TURRET_01)
										
										// Destroyed a turret in Setup: Air Defences mission, need to give myself points for this - to fix url:bugstar:4254514 - Setup: Air Defenses -  After destroying 6 turrets, we didn't get 6 points to pass us onto the next rule
										PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - obj ",iobjID," is a turret in Setup: Air Defences mission! Give myself points no matter what")
										
										BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_KILL_OBJ, 0, iMyTeam, -1, localPlayer, ci_TARGET_OBJECT, iobjID)
										BROADCAST_OBJECTIVE_MID_POINT(MC_serverBD_4.iCurrentHighestPriority[iMyTeam], iMyTeam)
										INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE()
										
									ELSE
										
										// Normal object damage event checks:
										
										IF MC_serverBD_4.iObjPriority[iobjID][ iMyTeam ] = MC_serverBD_4.iCurrentHighestPriority[ iMyTeam ]
											PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - obj ",iobjID," is my team ",iMyTeam,"'s current objective")
											BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_KILL_OBJ,0, iMyTeam ,-1,localPlayer,ci_TARGET_OBJECT,iobjID)
											IF MC_serverBD_4.iObjPriority[iobjID][ iMyTeam ] < FMMC_MAX_RULES
												IF MC_serverBD_4.iObjRule[iobjID][ iMyTeam ] = FMMC_OBJECTIVE_LOGIC_KILL
													BROADCAST_OBJECTIVE_MID_POINT(MC_serverBD_4.iObjPriority[iobjID][ iMyTeam ], iMyTeam )
													PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - obj ",iobjID," current objective is kill for my team ",iMyTeam)
													//MC_playerBD[iPartToUse].iNumObjKills++
													
													INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE()
												ELSE
													PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - GIVE_TEAM_POINTS_FOR_OBJ_KILL, object not on a kill rule for my team ",iMyTeam," iObjRule = ",MC_serverBD_4.iObjRule[iobjID][ iMyTeam ])
													GIVE_TEAM_POINTS_FOR_OBJ_KILL(iobjID,VictimObj,Killerped)
												ENDIF
											ELSE
												PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - GIVE_TEAM_POINTS_FOR_OBJ_KILL, object priority > FMMC_MAX_RULES for my team ",iMyTeam)
												GIVE_TEAM_POINTS_FOR_OBJ_KILL(iobjID,VictimObj,Killerped)
											ENDIF								
										ELSE
											PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - GIVE_TEAM_POINTS_FOR_OBJ_KILL, object priority ",MC_serverBD_4.iObjPriority[iobjID][ iMyTeam ]," for my team ",iMyTeam," is not current")
											GIVE_TEAM_POINTS_FOR_OBJ_KILL(iobjID,VictimObj,Killerped)
											FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
												IF MC_serverBD_4.iObjPriority[iobjID][iteam] < FMMC_MAX_RULES
													bnotignore = TRUE
												ENDIF
											ENDFOR
											IF bnotignore
												PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - obj is not current objective: ",iobjID)
												BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_KILL_OBJ,0, iMyTeam ,-1,localPlayer,ci_TARGET_OBJECT,iobjID)
											ENDIF
										ENDIF
										
									ENDIF
									
									// Fail if we should, upon killing this object
									INT iTeamLoop = 0
									
									FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS - 1
										IF MC_serverBD_4.iObjPriority[iobjID][ iTeamLoop ] < FMMC_MAX_RULES
											RUN_TEAM_OBJ_FAIL_CHECKS( iobjID, iTeamLoop, MC_serverBD_4.iObjPriority[ iobjID ][ iTeamLoop ] )
										ENDIF
									ENDFOR
								#IF IS_DEBUG_BUILD
								ELSE
									PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - obj is not a mission creator object")
								#ENDIF
								ENDIF
								
								IF IS_THIS_MODEL_A_CCTV_CAMERA(GET_ENTITY_MODEL(VictimObj))
								AND IS_THIS_MISSION_A_CASINO_HEIST_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
									INCREMENT_MP_INT_CHARACTER_AWARD(MP_AWARD_BIGBRO)
									PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Incrementing MP_AWARD_BIGBRO to ", GET_MP_INT_CHARACTER_AWARD(MP_AWARD_BIGBRO))
								ENDIF
							ELSE
								PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - GIVE_TEAM_POINTS_FOR_OBJ_KILL, damager is not the local player")
								GIVE_TEAM_POINTS_FOR_OBJ_KILL(iobjID,VictimObj,Killerped)
							ENDIF
						ELSE
							PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - GIVE_TEAM_POINTS_FOR_OBJ_KILL, damager is not a player")
							GIVE_TEAM_POINTS_FOR_OBJ_KILL(iobjID,VictimObj)
						ENDIF
					ELSE
						PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - GIVE_TEAM_POINTS_FOR_OBJ_KILL, damager is not a ped")
						GIVE_TEAM_POINTS_FOR_OBJ_KILL(iobjID,VictimObj)
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - GIVE_TEAM_POINTS_FOR_OBJ_KILL, damager does not exist")
					GIVE_TEAM_POINTS_FOR_OBJ_KILL(iobjID,VictimObj)
				ENDIF
				
				IF IS_MODEL_A_SEA_MINE(GET_ENTITY_MODEL(Victimobj))
					VECTOR vMinePos = GET_ENTITY_COORDS(Victimobj)
					
					IF NOT IS_EXPLOSION_IN_SPHERE(EXP_TAG_MINE_UNDERWATER, vMinePos, 4)
						ADD_EXPLOSION(vMinePos, EXP_TAG_MINE_UNDERWATER, 8)
						PRINTLN("[TMS] Adding explosion for sea mine that has been attacked")
					ELSE
						PRINTLN("[TMS] NOT adding explosion for sea mine that has been attacked - it seems to already be exploding itself")
					ENDIF
				ENDIF
				
				PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - iobjID = ", iobjID)
				
				IF iobjID > -1
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobjID].iObjectBitSetFour, cibsOBJ4_UpdateLinkedDoorsOnDestroyed)
						//Update/Unlock any linked doors when destroyed.
						SET_DOOR_TO_UPDATE_OFF_RULE_FROM_OBJECT(iobjID, TRUE, ciUPDATE_DOOR_OFF_RULE_METHOD__OBJ_DESTROYED)
						
						//Set the minigame as completed off rule if needed.
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobjID].iObjectBitSetThree, cibsOBJ3_AllowMinigameOffRule)
							BROADCAST_FMMC_UPDATE_MINIGAME_OFF_RULE_EVENT(TRUE, iobjID)
						ENDIF
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Not processing some extra event logic for this destroyed object because iobjID is ", iobjID, " (not a mission creator object)")
				ENDIF
				
				IF iobjID = -1
					//Not an object
					INT iDynoProp = IS_OBJ_A_MISSION_CREATOR_DYNOPROP(Victimobj)
						
					IF iDynoProp > -1
					AND iDynoProp < FMMC_MAX_NUM_DYNOPROPS
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_DynoPropDestroyedTracking)
						AND g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iContinuityId > -1
						AND NOT IS_BIT_SET(MC_serverBD_1.sLEGACYMissionContinuityVars.iDynoPropDestroyedTrackingBitset, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iContinuityId)
							SET_BIT(MC_serverBD_1.sLEGACYMissionContinuityVars.iDynoPropDestroyedTrackingBitset, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iContinuityId)
							PRINTLN("[JS][CONTINUITY] - PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - dynoprop ", iDynoProp, " with continuityID ", g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iContinuityId, " was destroyed")
						ENDIF
						
					ENDIF
				ENDIF
				
			ENDIF 
		ELSE // if not destroyed
			IF IS_ENTITY_A_VEHICLE(sEntityID.VictimIndex)
				
				PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - event ",iCount," - damaged entity was veh - NOT DESTROYED / UNDRIVEABLE")
				VEHICLE_INDEX Victimveh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)
				
				IF GET_PED_IN_VEHICLE_SEAT(Victimveh) = LocalPlayerPed
					IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
						IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
							Killerped = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
							IF IS_PED_A_PLAYER(Killerped)
								MC_Playerbd[iPartToUse].iVehDamage = MC_Playerbd[iPartToUse].iVehDamage + ROUND(sEntityID.Damage)
								PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - adding vehicle damage: ",sEntityID.Damage," making a total of: ",MC_Playerbd[iPartToUse].iVehDamage)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				ivehID = IS_VEH_A_MISSION_CREATOR_VEH(Victimveh)
				IF iVehID > -1 AND iVehID < FMMC_MAX_VEHICLES
					//For the Riot Van fire system
					IF sEntityID.WeaponUsed = ENUM_TO_INT(WEAPONTYPE_WATER_CANNON)
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[ivehID].iVehBitsetSeven, ciFMMC_VEHICLE7_SPAWN_BURNING)
							IF NETWORK_HAS_CONTROL_OF_ENTITY(Victimveh)
								fLocalFireDamageDealt[iVehID] += sEntityID.Damage
								IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iBurningVehFlashBS, iVehID)
									SET_BIT(MC_playerBD[iPartToUse].iBurningVehFlashBS, iVehID)
									PRINTLN("[RiotVanFire] Setting MC_playerBD[iPartToUse].iBurningVehFlashBS for Veh ", iVehID)
								ENDIF
								PRINTLN("[JR][RiotVanFire] sEntityID.Damage was ", sEntityID.Damage, " to equal ", fLocalFireDamageDealt[iVehID])
							ENDIF
						ENDIF
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[ivehID].iVehBitsetEight, ciFMMC_VEHICLE8_USE_DAMAGE_SCORE_CONTRIBUTION)
							IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
								IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)	
									Killerped = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
									IF LocalPlayerPed = Killerped
										MC_playerBD_1[iPartToUse].iDamageToVehsForMedal += ROUND(sEntityID.Damage)
										PRINTLN("[JS] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - damaged specific veh (not dead): ", ivehID, " Total: ", MC_playerBD_1[iPartToUse].iDamageToVehsForMedal)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_BIT_SET(iVehHasBeenDamagedOrHarmedBS, ivehID)
						PRINTLN("[LM] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Setting iVehHasBeenDamagedOrHarmedBS for iVeh: ", ivehID)
						SET_BIT(iVehHasBeenDamagedOrHarmedBS, ivehID)
					ENDIF
				ENDIF
				
				IF USING_SHOWDOWN_POINTS()
				AND MC_playerBD_1[iLocalPart].fShowdownPoints > 0.0
					PED_INDEX piShowDown_VehPed = GET_PED_IN_VEHICLE_SEAT(Victimveh)
					//FLOAT fShowDownPointsDamage = g_FMMC_STRUCT.fShowdown_PointsDamagePerHit
					
					IF DOES_ENTITY_EXIST(piShowDown_VehPed)
					AND IS_PED_A_PLAYER(piShowDown_VehPed)
					
						INT iShowDown_VictimPart = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(NETWORK_GET_PLAYER_INDEX_FROM_PED(piShowDown_VehPed)))
						
						IF iShowDown_VictimPart = iLocalPart
						AND DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
						AND IS_ENTITY_A_PED(sEntityID.DamagerIndex)
							//I've been hit! Broadcast event
							PED_INDEX piShowDown_DamagerPed = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
							
							IF DOES_ENTITY_EXIST(piShowDown_DamagerPed)
							AND IS_PED_A_PLAYER(piShowDown_DamagerPed)
								INT iShowDown_DamagerPart = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(NETWORK_GET_PLAYER_INDEX_FROM_PED(piShowDown_DamagerPed)))
								
								IF iShowDown_DamagerPart > -1
									IF MC_playerBD[iShowDown_DamagerPart].iTeam = MC_playerBD[iLocalPart].iTeam
										//No friendly fire
									ELSE
										iShowdown_LastDamagerPart = iShowDown_DamagerPart
										BROADCAST_FMMC_SHOWDOWN_DAMAGE(iShowDown_VictimPart, iShowDown_DamagerPart, SHOWDOWNDMG_STANDARD)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
									
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_DUNE_EXPLOSIONS)
					IF DOES_ENTITY_EXIST(VictimVeh)
						IF DOES_ENTITY_EXIST(GET_PED_IN_VEHICLE_SEAT(VictimVeh))
							IF NOT IS_PED_A_PLAYER(GET_PED_IN_VEHICLE_SEAT(VictimVeh))
								DUNE_FLIP_MARK_FOR_EXPLOSION(sEntityID.VictimIndex, VictimVeh, sEntityID.DamagerIndex)
							ENDIF
						ELSE
							DUNE_FLIP_MARK_FOR_EXPLOSION(sEntityID.VictimIndex, VictimVeh, sEntityID.DamagerIndex)
						ENDIF
					ENDIF
				ENDIF
				
								
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciVEHICLE_WEAPON_DEATHMATCH_TOGGLE)
				AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciPRON_DEATHMATCH_TOGGLE_CORONA)
					IF GET_PED_IN_VEHICLE_SEAT(Victimveh) = LocalPlayerPed
						IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
							IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
								Killerped = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)								
								PLAYER_INDEX plPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(Killerped)
								
								IF plPlayer != INVALID_PLAYER_INDEX()
									INT iPartDamager = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(plPlayer))
								
									IF iPartDamager > -1
										IF IS_PED_A_PLAYER(Killerped)
										AND MC_playerBD[iPartToUse].iteam != MC_playerBD[iPartDamager].iteam
											iPartLastDamager = iPartDamager
											piPlayerPedLastDamager = Killerped
											PRINTLN("[KH] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - PLAYER IS THE VICTIM, DAMAGER: ", iPartLastDamager)
											
											IF NOT HAS_NET_TIMER_STARTED(tdLastDamagerTimer)
												PRINTLN("[KH] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Starting last damager timer")
												START_NET_TIMER(tdLastDamagerTimer)
											ELSE
												PRINTLN("[KH] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Resetting and starting last damager timer")
												RESET_NET_TIMER(tdLastDamagerTimer)
												START_NET_TIMER(tdLastDamagerTimer)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA(g_FMMC_STRUCT.iAdversaryModeType)
				OR (CONTENT_IS_USING_ARENA() AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciEXPLODE_VEHICLE_ON_ZERO_HEALTH))
					IF GET_PED_IN_VEHICLE_SEAT(Victimveh) = LocalPlayerPed
						IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
							IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
								Killerped = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
								
								IF NETWORK_IS_PLAYER_A_PARTICIPANT(NETWORK_GET_PLAYER_INDEX_FROM_PED(Killerped))
									INT iPartDamager = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(NETWORK_GET_PLAYER_INDEX_FROM_PED(Killerped)))
									IF iPartDamager > -1
										IF IS_PED_A_PLAYER(Killerped)
										AND MC_playerBD[iPartToUse].iteam != MC_playerBD[iPartDamager].iteam
											IF iPartToShootPlayerLast != iPartDamager
												PRINTLN("[KH] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Assigning iPartDamager as last damager: ", iPartDamager)
												iPartToShootPlayerLast = iPartDamager
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(iLocalBoolCheck3, LBOOL3_EXPLOSIVE_ZONE_ACTIVE)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(Victimveh)
					AND IS_VEHICLE_EMPTY(Victimveh,TRUE)
						INT iZone
						
						FOR iZone = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfZones - 1)
							IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iType = ciFMMC_ZONE_TYPE__EXPLOSION_AREA
							AND IS_BIT_SET(iLocalZoneBitset, iZone)
							AND NOT IS_BIT_SET(iLocalZoneRemovedBitset, iZone)
								PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - ciFMMC_ZONE_TYPE__EXPLOSION_AREA  took damage: ",sEntityID.Damage," for vehicle: ",NATIVE_TO_INT(Victimveh), " Zone: ",iZone," zone threshold is: ",g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue)
								IF sEntityID.Damage >= g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue
									#IF IS_DEBUG_BUILD
									VECTOR vtemp = GET_ENTITY_COORDS(Victimveh)
									PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - ciFMMC_ZONE_TYPE__EXPLOSION_AREA  vehicle coords: ",vtemp, " Zone: ",iZone," zone coords 0: ",g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0]," zone coords 1: ",g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1])
									#ENDIF
									
									IF IS_LOCATION_IN_FMMC_ZONE(GET_ENTITY_COORDS(Victimveh), iZone, TRUE, DEFAULT, DEFAULT, fZoneGrowth[iZone])
									OR IS_BIT_SET(iZoneBS_ActivatedRemotely, iZone)
										NETWORK_EXPLODE_VEHICLE(Victimveh)
										PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - ciFMMC_ZONE_TYPE__EXPLOSION_AREA  exploded  for vehicle: ",NATIVE_TO_INT(Victimveh))
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
					ENDIF
				ENDIF
				
				//If we should be tracking damage on this vehicle:
				IF (g_FMMC_STRUCT.iHeistCSVehDmgTrackBitSet > 0)
					IF bIsLocalPlayerHost
						IF ivehID !=-1
							IF IS_BIT_SET(g_FMMC_STRUCT.iHeistCSVehDmgTrackBitSet, ivehID)
								IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
								AND GET_ENTITY_MODEL(VictimVeh) = AVENGER
									MC_serverBD_2.fHeistVehDamage = (100.0 - GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(VictimVeh, ivehID, MC_serverBD.iTotalNumStartingPlayers, FALSE))
									PRINTLN("[JS] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Overriding damage % for Avenger: ", MC_serverBD_2.fHeistVehDamage)
								ELSE
									MC_serverBD_2.fHeistVehDamage += ((sEntityID.Damage / GET_ENTITY_MAX_HEALTH(Victimveh)) * 100)
									PRINTLN("[LK MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - adding SPECIFIC vehicle damage: ", sEntityID.Damage)
									PRINTLN("[LK MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - TOTAL vehicle damage: ", MC_serverBD_2.fHeistVehDamage)
									PRINTLN("[MJM] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - adding vehicle damage percentage: ", ((sEntityID.Damage / GET_ENTITY_MAX_HEALTH(Victimveh)) * 100))
									PRINTLN("[LK MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - MC_serverBD_2.fHeistVehDamage: ", MC_serverBD_2.fHeistVehDamage)
								
									IF iCurrentHeistMissionIndex = HBCA_BS_TUTORIAL_CAR
									OR iCurrentHeistMissionIndex = HBCA_BS_PRISON_BREAK_STATION
										MC_serverBD_2.iHeistVehRepairCost = GET_CARMOD_REPAIR_COST(Victimveh)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
	
			ELIF IS_ENTITY_A_PED(sEntityID.VictimIndex)
				victimPed = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)
				// Track total damage on player. 
				IF IS_PED_A_PLAYER(victimPed)
					IF (LocalPlayerPed = victimPed)
						IF MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] < FMMC_MAX_RULES
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitSetFive[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]], ciBS_RULE5_ENABLE_BEAST_MODE)
							OR IS_PARTICIPANT_A_BEAST(iLocalPart)
								PED_INDEX piDamager
								IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
									IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
										piDamager = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
										IF IS_PED_A_PLAYER(piDamager)
											IF piDamager != LocalPlayerPed
												PRINTLN("[JS] [BEASTMODE] - Beast was damaged while stealthed")
												bBeastBeenDamaged = TRUE
												IF sEntityID.VictimDestroyed
													//Play death sound
													STRING sSoundSet = GET_BEAST_MODE_SOUNDSET()
													PLAY_SOUND_FROM_ENTITY(-1, "Beast_Die", localPlayerPed, sSoundSet, TRUE, 60)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						MC_Playerbd[iLocalPart].iPlayerDamage = MC_Playerbd[iLocalPart].iPlayerDamage + ROUND(sEntityID.Damage)
						PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - adding player damage: ",sEntityID.Damage," making a total of: ",MC_Playerbd[iLocalPart].iPlayerDamage) 
						
						IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
							IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)	
								Killerped = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
								IF IS_PED_A_PLAYER(Killerped) 
									KillerPlayerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(Killerped)
									IF NETWORK_IS_PLAYER_A_PARTICIPANT(KillerPlayerID)
										KillerPart = NETWORK_GET_PARTICIPANT_INDEX(KillerPlayerID)
										IF NETWORK_IS_PARTICIPANT_ACTIVE(KillerPart)
											iKillerPart = NATIVE_TO_INT(KillerPart)
											IF iKillerPart != iLocalPart
											AND iKillerPart != -1
											AND iLocalPart != -1
												IF MC_playerBD[iKillerPart].iTeam != MC_playerBD[iLocalPart].iTeam
													PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT iPartLastDamager = ", iPartLastDamager)
													iPartLastDamager = iKillerPart
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						victimPlayerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(Victimped)
						IF NETWORK_IS_PLAYER_A_PARTICIPANT(victimPlayerID)
							VictimPart = NETWORK_GET_PARTICIPANT_INDEX(victimPlayerID)
							IF NETWORK_IS_PARTICIPANT_ACTIVE(VictimPart)
								iVictimpart = NATIVE_TO_INT(VictimPart)
								IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
									IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)	
										Killerped = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
										IF LocalPlayerPed = Killerped
										AND LocalPlayerPed != victimPed
											IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iVictimpart].iTeam].iTeamBitset2, ciBS2_JUGGERNAUTS)
												MC_Playerbd[iLocalPart].iDamageToJuggernaut += ROUND(sEntityID.Damage)
												PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - adding juggernaut damage: ",ROUND(sEntityID.Damage)," making a total of: ",MC_Playerbd[iLocalPart].iDamageToJuggernaut) 
											ENDIF
											IF MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] < FMMC_MAX_RULES
												IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitSetSix[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]], ciBS_RULE6_ENABLE_HEALTH_DRAIN)
													IF NOT IS_DAMAGE_FROM_SCRIPT(sEntityID.WeaponUsed)
														PRINTLN("[JS] [HEALTHDRAIN] - PROCESS_HEALTH_DRAIN_DAMAGE_EVENTS - Healing the attacker: ", (sEntityID.Damage * g_FMMC_STRUCT.sFMMCEndConditions[ MC_PlayerBD[iLocalPart].iteam ].sDrainStruct[ MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] ].fHealthDmgRestore))
														SET_ENTITY_HEALTH(Killerped, ROUND((sEntityID.Damage * g_FMMC_STRUCT.sFMMCEndConditions[ MC_PlayerBD[iLocalPart].iteam ].sDrainStruct[ MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] ].fHealthDmgRestore)))													
														RESET_NET_TIMER(tdDamageDrainRateTimer)
														START_NET_TIMER(tdDamageDrainRateTimer)												
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					ipedid = IS_ENTITY_A_MISSION_CREATOR_ENTITY(Victimped)
					PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - ipedid = ",ipedid)
					IF ipedid >= 0
						IF bIsLocalPlayerHost
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[ipedid].iPedBitsetFive, ciPED_BSFive_AggroWhenHurtOrKilled)
							AND NOT IS_BIT_SET(iPedLocalReceivedEvent_HurtOrKilled[GET_LONG_BITSET_INDEX(ipedid)], GET_LONG_BITSET_BIT(ipedid))
								PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Ped ",ipedid," SPOOKED as ped hurt had ciPED_BSFive_AggroWhenHurtOrKilled set")
								SET_BIT(iPedLocalReceivedEvent_HurtOrKilled[GET_LONG_BITSET_INDEX(ipedid)], GET_LONG_BITSET_BIT(ipedid))
								BROADCAST_FMMC_PED_SPOOK_HURTORKILLED(ipedid)
							ENDIF
							
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[ipedid].iPedBitset, ciPED_BS_TrackDamage)
								IF GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
									MC_serverBD_2.fVIPDamage += ((sEntityID.Damage / GET_ENTITY_MAX_HEALTH(victimPed)) * 100)
									PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT 2 - sEntityID.Damage = ",sEntityID.Damage, " percentage = ",((sEntityID.Damage / GET_ENTITY_MAX_HEALTH(victimPed)) * 100)," MC_serverBD_2.fVIPDamage = ",MC_serverBD_2.fVIPDamage)
								ENDIF
							ENDIF
							
							IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
								IF sEntityID.WeaponUsed = ENUM_TO_INT(WEAPONTYPE_DLC_TRANQUILIZER)
									IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_A_PED_HAS_BEEN_TRANQUILIZED)
										SET_BIT(MC_serverBD.iServerBitSet8, SBBOOL8_A_PED_HAS_BEEN_TRANQUILIZED)
										PRINTLN("[RCC MISSION][Tranq] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Setting SBBOOL8_A_PED_HAS_BEEN_TRANQUILIZED")
									ENDIF
								ENDIF
								
								IF sEntityID.WeaponUsed = ENUM_TO_INT(WEAPONTYPE_STUNGUN)
									IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_A_PED_HAS_BEEN_STUNNED)
										SET_BIT(MC_serverBD.iServerBitSet8, SBBOOL8_A_PED_HAS_BEEN_STUNNED)
										PRINTLN("[RCC MISSION][StunGun] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Setting SBBOOL8_A_PED_HAS_BEEN_STUNNED")										
										PRINTLN("[RCC MISSION][StunGun] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Setting iPedHitByStunGun on: ", ipedid )										
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
						AND NOT IS_BIT_SET(iPedHitByStunGun[GET_LONG_BITSET_INDEX(ipedid)], GET_LONG_BITSET_BIT(ipedid))
							IF sEntityID.WeaponUsed = ENUM_TO_INT(WEAPONTYPE_STUNGUN)
								IF NOT IS_PED_INJURED(victimPed)
									SET_BIT( iPedHitByStunGun[GET_LONG_BITSET_INDEX(ipedid)], GET_LONG_BITSET_BIT(ipedid))
									IF NETWORK_HAS_CONTROL_OF_ENTITY(victimPed)
										PRINTLN("[RCC MISSION][StunGun] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Setting PCF_DisableInjuredCryForHelpEvents on: ", ipedid )
										SET_PED_CONFIG_FLAG(victimPed, PCF_DisableInjuredCryForHelpEvents, TRUE)
									ELSE
										PRINTLN("[RCC MISSION][StunGun] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Not Owner, preventing PCF_DisableInjuredCryForHelpEvents from being set on: ", ipedid )
									ENDIF
								ENDIF
							ENDIF
						ENDIF

						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[ipedid].iPedBitsetFive, ciPED_BSFive_OneHitKill)
						AND (sEntityID.WeaponUsed = ENUM_TO_INT(WEAPONTYPE_STUNGUN)
						#IF FEATURE_COPS_N_CROOKS
						OR sEntityID.WeaponUsed = ENUM_TO_INT(WEAPONTYPE_DLC_STUNGUNCNC)
						#ENDIF
						OR sEntityID.WeaponUsed = ENUM_TO_INT(WEAPONTYPE_DLC_TRANQUILIZER))
							IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
								IF NETWORK_HAS_CONTROL_OF_ENTITY(victimPed)
									IF NOT IS_PED_DEAD_OR_DYING(victimPed)
										SET_ENTITY_HEALTH(victimPed, ciPED_OneHitKill_Health)
										PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT | Setting ped ", ipedid, " health to ", ciPED_OneHitKill_Health, " aka ciPED_OneHitKill_Health due to ciPED_BSFive_OneHitKill")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[ipedid].iPedBitsetTwelve, ciPed_BSTwelve_UseDamageScoreContribution)
							IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
								IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)	
									Killerped = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
									IF LocalPlayerPed = Killerped
										MC_playerBD_1[iPartToUse].iDamageToPedsForMedal += ROUND(sEntityID.Damage)
										PRINTLN("[JS] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - damaged specific ped (not dead): ", ipedid, " Total: ", MC_playerBD_1[iPartToUse].iDamageToPedsForMedal)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			
			ELIF IS_ENTITY_AN_OBJECT(sEntityID.VictimIndex)
				OBJECT_INDEX oiObj = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)
				INT iObj = IS_OBJ_A_MISSION_CREATOR_OBJ(oiObj)
				
				IF iObj > -1
				AND iObj < FMMC_MAX_NUM_OBJECTS
					IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
						
						IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
							Killerped = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
						ENDIF
						
						IF DOES_ENTITY_EXIST(KillerPed)
							IF IS_THIS_BOMB_FOOTBALL()
								IF LocalPlayerPed = Killerped
									MC_playerBD_1[iLocalPart].iLastBombFootballHitTimestamp[iObj] = NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE())
									PRINTLN("[BMBFB TOUCH SPAM] DAMAGE EVENT - I've damaged ball ", iObj, "! Timestamp: ", MC_playerBD_1[iLocalPart].iLastBombFootballHitTimestamp[iObj])
								ENDIF
							ENDIF
						ENDIF					
					ENDIF
					
					IF sEntityID.WeaponUsed = ENUM_TO_INT(WEAPONTYPE_STUNGUN)
					#IF FEATURE_COPS_N_CROOKS
					OR sEntityID.WeaponUsed = ENUM_TO_INT(WEAPONTYPE_DLC_STUNGUNCNC)
					#ENDIF
						SET_ENTITY_HIT_BY_TASER(iObj, oiObj, FMMC_ELECTRONIC_ENTITY_TYPE__OBJECT)
						PRINTLN("[Taser] DAMAGE EVENT - Object has been tased! iObj: ", iObj)						
					ELSE
						
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetFour, cibsOBJ4_DieInOneHit)
						AND sEntityID.Damage > 5.0
							IF NETWORK_HAS_CONTROL_OF_ENTITY(oiObj)
								SET_ENTITY_HEALTH(sEntityID.VictimIndex, 0, sEntityID.DamagerIndex)
								PRINTLN("[CCTV] DAMAGE EVENT - Setting ", iObj, " health to 0 because cibsOBJ4_DieInOneHit is set and the object was hit!")
							ELSE
								PRINTLN("[CCTV] DAMAGE EVENT - object ", iObj, " has cibsOBJ4_DieInOneHit set but we are not the owner of the object")
							ENDIF
							
							IF IS_THIS_MODEL_A_CCTV_CAMERA(GET_ENTITY_MODEL(sEntityID.VictimIndex))
							AND IS_THIS_MISSION_A_CASINO_HEIST_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
								IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
									IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
										Killerped = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
										IF IS_PED_A_PLAYER(Killerped)		
											IF LocalPlayerPed = Killerped
												INCREMENT_MP_INT_CHARACTER_AWARD(MP_AWARD_BIGBRO)
												PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Incrementing MP_AWARD_BIGBRO to ", GET_MP_INT_CHARACTER_AWARD(MP_AWARD_BIGBRO))
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ELSE
					INT iDynoProp = IS_OBJ_A_MISSION_CREATOR_DYNOPROP(oiObj)
					
					IF iDynoProp > -1
					AND iDynoProp < FMMC_MAX_NUM_DYNOPROPS
						
						IF sEntityID.WeaponUsed = ENUM_TO_INT(WEAPONTYPE_STUNGUN)
						#IF FEATURE_COPS_N_CROOKS
						OR sEntityID.WeaponUsed = ENUM_TO_INT(WEAPONTYPE_DLC_STUNGUNCNC)
						#ENDIF
							SET_ENTITY_HIT_BY_TASER(iDynoProp, oiObj, FMMC_ELECTRONIC_ENTITY_TYPE__DYNOPROP)
							PRINTLN("[Taser] DAMAGE EVENT - Dynoprop has been tased! iDynoProp: ", iDynoProp)
						ELSE
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iDynopropBitset, ciFMMC_DYNOPROP_DestroyInOneHit)
							AND sEntityID.Damage > 5.0
								IF NETWORK_HAS_CONTROL_OF_ENTITY(sEntityID.VictimIndex)
									SET_ENTITY_HEALTH(sEntityID.VictimIndex, 0, sEntityID.DamagerIndex)
									PRINTLN("[CCTV] DAMAGE EVENT - Setting ", iDynoProp, " health to 0 because ciFMMC_DYNOPROP_DestroyInOneHit is set and the dynoprop was hit!")
								ENDIF

								IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
									IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
										Killerped = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
										IF IS_PED_A_PLAYER(Killerped)		
											IF LocalPlayerPed = Killerped
												IF IS_THIS_MODEL_A_CCTV_CAMERA(GET_ENTITY_MODEL(sEntityID.VictimIndex))
												AND IS_THIS_MISSION_A_CASINO_HEIST_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
													INCREMENT_MP_INT_CHARACTER_AWARD(MP_AWARD_BIGBRO)
													PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Incrementing MP_AWARD_BIGBRO to ", GET_MP_INT_CHARACTER_AWARD(MP_AWARD_BIGBRO))
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF IS_OBJECT_BROKEN_AND_VISIBLE(oiObj)
							IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_DynoPropDestroyedTracking)
							AND g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iContinuityId > -1
							AND NOT IS_BIT_SET(MC_serverBD_1.sLEGACYMissionContinuityVars.iDynoPropDestroyedTrackingBitset, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iContinuityId)
								SET_BIT(MC_serverBD_1.sLEGACYMissionContinuityVars.iDynoPropDestroyedTrackingBitset, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iContinuityId)
								PRINTLN("[JS][CONTINUITY] - PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - dynoprop ", iDynoProp, " with continuityID ", g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iContinuityId, " was destroyed")
							ENDIF
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		
		// [MJL] url:bugstar:5458827 - Arena Wars - Arena Announcer - MC - Monster Jam
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_MONSTER_JAM(g_FMMC_STRUCT.iAdversaryModeType)
			
			VEHICLE_INDEX viVehicleDestroyedForCrowdCheer
			
			IF IS_ENTITY_A_PED(sEntityID.VictimIndex)
			AND IS_ENTITY_DEAD(sEntityID.VictimIndex)
			
				Victimped = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)
				
				IF IS_PED_IN_ANY_VEHICLE(Victimped)
				
					viVehicleDestroyedForCrowdCheer = GET_VEHICLE_PED_IS_IN(Victimped)
					
					IF IS_VEHICLE_A_MONSTER(GET_ENTITY_MODEL(viVehicleDestroyedForCrowdCheer))
						SET_ARENA_ANNOUNCER_BS_MODE_SPECIFIC(g_iAA_PlaySound_MonsterJam_GladiatorKilled, TRUE)
						PRINTLN("[MJL] Local: Monster is destroyed, playing: g_iAA_PlaySound_MonsterJam_GladiatorKilled")
					ENDIF
					
					IF IS_ARENA_VEHICLE_MODEL_A_ISSI(GET_ENTITY_MODEL(viVehicleDestroyedForCrowdCheer))
						IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
						AND IS_ENTITY_A_PED(sEntityID.DamagerIndex)
														
							IF NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_FIRST_CONTENDER_DESTROYED)
								SET_ARENA_ANNOUNCER_BS_MODE_SPECIFIC(g_iAA_PlaySound_MonsterJam_FirstBloodContenderNormal, TRUE, default, default, TRUE)
								PRINTLN("[MJL] Local: First contender destroyed, playing: g_iAA_PlaySound_MonsterJam_FirstBloodContenderNormal")
								SET_BIT(iLocalBoolCheck30, LBOOL30_FIRST_CONTENDER_DESTROYED)
							ELIF INT_TO_ENUM(WEAPON_TYPE, sEntityID.WeaponUsed) = WEAPONTYPE_EXPLOSION
								SET_ARENA_ANNOUNCER_BS_MODE_SPECIFIC(g_iAA_PlaySound_MonsterJam_FirstBloodContenderCrushed, TRUE)
								PRINTLN("[MJL] Local: Contender crushed to death, playing: g_iAA_PlaySound_MonsterJam_FirstBloodContenderCrushed")
							ENDIF
						ENDIF			
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
		
	ENDIF
	
ENDPROC



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: OBJECTIVE COMPLETION EVENT PROCESSING (processes the events sent out by HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE) !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************

PROC FAIL_TEAM_FOR_INCORRECT_GOTO_HEADING( INT iTeam )
	MC_serverBD.iReasonForObjEnd[ iTeam ] = OBJ_END_REASON_LOCATE_WRONG_ANGLE
	SET_TEAM_OBJECTIVE_OUTCOME( iTeam, MC_serverBD_4.iCurrentHighestPriority[ iTeam ], FALSE )
	SET_LOCAL_OBJECTIVE_END_MESSAGE( iTeam, MC_serverBD_4.iCurrentHighestPriority[ iTeam ], MC_serverBD.iReasonForObjEnd[ iTeam ], TRUE )
	BROADCAST_FMMC_OBJECTIVE_END_MESSAGE( MC_serverBD.iReasonForObjEnd[ iTeam ], iTeam, mc_serverBD_4.iCurrentHighestPriority[ iTeam ], DID_TEAM_PASS_OBJECTIVE( iTeam, mc_serverBD_4.iCurrentHighestPriority[ iTeam ] ),TRUE )
	SET_TEAM_FAILED(iTeam,mFail_LOCATE_WRONG_ANGLE)
ENDPROC

FUNC BOOL SHOULD_DELIVER_VEH_BE_PROGRESSED_BY_LIKED_TEAM(INT iveh, INT iTeam, INT ipriority, INT iTeam2, INT ipriority2)

	PRINTLN("[RCC MISSION][MJM] - SHOULD_DELIVER_VEH_BE_PROGRESSED_BY_LIKED_TEAM - ipriority = ",ipriority, " ipriority2 = ",ipriority2)
	
	IF ipriority < FMMC_MAX_RULES
	AND ipriority2 < FMMC_MAX_RULES
		PRINTLN("[RCC MISSION][MJM] - SHOULD_DELIVER_VEH_BE_PROGRESSED_BY_LIKED_TEAM - Priority ok ")
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam2].iRuleBitsetThree[ipriority2], ciBS_RULE3_PROGRESS_IN_SAME_PLACE)
			
			IF g_FMMC_STRUCT.sFMMCEndConditions[iteam].iDropOffType[ipriority] = g_FMMC_STRUCT.sFMMCEndConditions[iteam2].iDropOffType[ipriority2]
				
				SWITCH g_FMMC_STRUCT.sFMMCEndConditions[iteam].iDropOffType[ipriority]
					
					CASE ciFMMC_DROP_OFF_TYPE_CYLINDER
					CASE ciFMMC_DROP_OFF_TYPE_SPHERE
						
						VECTOR v1
						v1 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[ ipriority ]
						VECTOR v2
						v2 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam2].vDropOff[ ipriority2 ]
						FLOAT r1
						r1 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffRadius[ ipriority ]
						FLOAT r2
						r2 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam2].fDropOffRadius[ ipriority2 ]
						
						IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].vDropOffOverride)
							v1 = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].vDropOffOverride
							v2 = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].vDropOffOverride
							IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fVehDropOffRadius > 0
								r1 = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fVehDropOffRadius
								r2 = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fVehDropOffRadius
							ENDIF
						ELSE
							IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[iteam].vDropOff2[ipriority])
								IF iveh != iFirstHighPriorityVehicleThisRule[iteam]
									v1 = g_FMMC_STRUCT.sFMMCEndConditions[iteam].vDropOff2[ipriority]
								ENDIF
							ENDIF
							IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[iteam2].vDropOff2[ipriority2])
								IF iveh != iFirstHighPriorityVehicleThisRule[iteam2]
									v2 = g_FMMC_STRUCT.sFMMCEndConditions[iteam2].vDropOff2[ipriority2]
								ENDIF
							ENDIF
						ENDIF
						
						PRINTLN("[RCC MISSION][MJM] - SHOULD_DELIVER_VEH_BE_PROGRESSED_BY_LIKED_TEAM (1) - iTeam: ",iTeam, " ipriority: ",ipriority, " v1: ",v1," r1: ",r1)
						PRINTLN("[RCC MISSION][MJM] - SHOULD_DELIVER_VEH_BE_PROGRESSED_BY_LIKED_TEAM (2) - iTeam2: ",iTeam2, " ipriority2: ",ipriority2, " v2: ",v2," r2: ",r2)
						
						IF NOT ARE_VECTORS_EQUAL(v1, v2)
						OR (r1 != r2)
							RETURN FALSE
						ENDIF
						
					BREAK
					
					CASE ciFMMC_DROP_OFF_TYPE_AREA
						VECTOR v1_1
						v1_1 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[ ipriority ]
						VECTOR v1_2
						v1_2 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff2[ ipriority ]
						FLOAT fWidth1
						fWidth1 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffRadius[ ipriority ]
						
						VECTOR v2_1
						v2_1 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam2].vDropOff[ ipriority2 ]
						VECTOR v2_2
						v2_2 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam2].vDropOff2[ ipriority2 ]
						FLOAT fWidth2
						fWidth2 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam2].fDropOffRadius[ ipriority2 ]
						
						IF (fWidth1 = fWidth2)
						AND ((ARE_VECTORS_EQUAL(v1_1, v2_1) AND ARE_VECTORS_EQUAL(v1_2, v2_2)) OR (ARE_VECTORS_EQUAL(v1_1, v2_2) AND ARE_VECTORS_EQUAL(v1_2, v2_1)))
							RETURN TRUE
						ELSE
							RETURN FALSE
						ENDIF
					BREAK
					
					CASE ciFMMC_DROP_OFF_TYPE_VEHICLE
						IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOff_Vehicle[ipriority] != g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOff_Vehicle[ipriority2]
							RETURN FALSE
						ELSE
							
							BOOL bGetIn1
							bGetIn1 = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iVehDropOff_GetInBS, ipriority)
							BOOL bHookUp1
							bHookUp1 = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iVehDropOff_HookUpBS, ipriority)
							FLOAT fRadius1
							fRadius1 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffRadius[ipriority]
							
							BOOL bGetIn2
							bGetIn2 = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam2].iVehDropOff_GetInBS, ipriority2)
							BOOL bHookUp2
							bHookUp2 = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam2].iVehDropOff_HookUpBS, ipriority2)
							FLOAT fRadius2
							fRadius2 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam2].fDropOffRadius[ipriority2]
							
							IF (bGetIn1 = bGetIn2)
							AND (bGetIn1 OR (bHookUp1 = bHookUp2))
							AND (bGetIn1 OR bHookUp1 OR (fRadius1 = fRadius2))
								RETURN TRUE
							ELSE
								RETURN FALSE
							ENDIF
							
						ENDIF
					BREAK
					
					CASE ciFMMC_DROP_OFF_TYPE_LEADER_GARAGE
					CASE ciFMMC_DROP_OFF_TYPE_PROPERTY
						RETURN TRUE
					BREAK
					
				ENDSWITCH
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_DELIVER_VEH_STAT_TRACKING(INT iEntityId)
	
	IF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_MISSILE_SILO_FINALE)
		IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
			VEHICLE_INDEX tempVeh
			tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iEntityId])
			MODEL_NAMES mnVeh = GET_ENTITY_MODEL(tempVeh)
			
			PRINTLN("[RCC MISSION] PROCESS_DELIVER_VEH_STAT_TRACKING - vehicle is: ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mnVeh))	

			SWITCH mnVeh
				CASE KHANJALI
					SET_BIT(g_sTransitionSessionData.sStrandMissionData.iGangOpsMissionChoiceTrackingBitSet, ENUM_TO_INT(GANG_OPS_MISSION_CHOICE_KHANJALI))
				BREAK
				CASE BARRAGE
					SET_BIT(g_sTransitionSessionData.sStrandMissionData.iGangOpsMissionChoiceTrackingBitSet, ENUM_TO_INT(GANG_OPS_MISSION_CHOICE_BARRAGE))
				BREAK				
			ENDSWITCH			
		ENDIF
	ENDIF	
ENDPROC

PROC PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT(INT iCount)
	

INT iteam,irepeat,iteamrepeat
INT ivehNumber
INT ipriority[FMMC_MAX_TEAMS]
BOOL bwasobjective[FMMC_MAX_TEAMS]
BOOL bobjectivefails[FMMC_MAX_TEAMS]
PARTICIPANT_INDEX tempPart
PLAYER_INDEX tempPlayer

INT iBlipColour = BLIP_COLOUR_RED
BOOL bCustomBlip
	
	SCRIPT_EVENT_DATA_FMMC_OBJECTIVE_COMPLETE ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_OBJECTIVE_COMPLETE
			
			PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iObjectiveID = ",ObjectiveData.iObjectiveID)
			PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iParticipant = ",ObjectiveData.iparticipant) 
			PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iEntityID = ",ObjectiveData.iEntityID)
			PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ",ObjectiveData.iPriority)
			PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iRevalidates = ",ObjectiveData.iRevalidates)

			IF ObjectiveData.iPriority < FMMC_MAX_RULES
			AND ObjectiveData.iParticipant = iLocalPart
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[ObjectiveData.iParticipant].iteam].iRuleBitsetNine[ObjectiveData.iPriority], ciBS_RULE9_FINAL_RULE_RP_BONUS)
					GIVE_LOCAL_PLAYER_FINAL_RP_REWARD_BONUS()
				ENDIF
			ENDIF

			IF NOT bIsLocalPlayerHost
				
				SWITCH ObjectiveData.iObjectiveID
					
					CASE JOB_COMP_ARRIVE_LOC
						
						IF ObjectiveData.iEntityID != -1
						AND MC_playerBD[ObjectiveData.iparticipant].iteam = MC_PlayerBD[ iPartToUse ].iTeam
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFIve, ciBEAST_SFX)
								STRING sSoundSet
								sSoundSet = GET_BEAST_MODE_SOUNDSET()
								PLAY_SOUND_FRONTEND(-1, "Beast_Checkpoint_NPC",sSoundSet, FALSE)
							ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciSOUND_ON_TEAMMATE_CHECKPOINT)
								IF ObjectiveData.iParticipant != iPartToUse
										PLAY_SOUND_FRONTEND(-1, "Checkpoint_Teammate", "GTAO_Shepherd_Sounds", FALSE)
								ENDIF
							ENDIF
							
							IF DOES_BLIP_EXIST(LocBlip[ObjectiveData.iEntityID])
								// url:bugstar:2185410
								IF DOES_BLIP_HAVE_GPS_ROUTE(LocBlip[ObjectiveData.iEntityID])
									SET_IGNORE_NO_GPS_FLAG(FALSE)
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT called SET_IGNORE_NO_GPS_FLAG FALSE (A)")
								ENDIF
							
								CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] Removing blip for location ", ObjectiveData.iEntityID, " as a ARRIVE location job is complete.")
								REMOVE_BLIP(LocBlip[ObjectiveData.iEntityID])
							ENDIF
							
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_ENABLE_VENETIAN_JOB_SOUNDS)
							AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
								BROADCAST_FMMC_LOCATE_ALERT_SOUND()
							ENDIF
													
						ENDIF
						
					BREAK // End of JOB_COMP_ARRIVE_LOC
					
					CASE JOB_COMP_PHOTO_LOC
						IF ObjectiveData.iEntityID != -1
							IF DOES_BLIP_EXIST(LocBlip[ObjectiveData.iEntityID])
								CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] Removing blip for location ", ObjectiveData.iEntityID, " as a PHOTO location job is complete.")
								REMOVE_BLIP(LocBlip[ObjectiveData.iEntityID])
							ENDIF
						ENDIF
						
					BREAK // End of JOB_COMP_PHOTO_LOC
					
					CASE JOB_COMP_ARRIVE_OBJ
					CASE JOB_COMP_PHOTO_OBJ
					CASE JOB_COMP_HACK_OBJ
						IF ObjectiveData.iEntityID != -1
							CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] Removing blip for package ", ObjectiveData.iEntityID, " as an ARRIVE, PHOTO, or HACK package job is complete.")
							REMOVE_OBJECT_BLIP(ObjectiveData.iEntityID)
							
							IF ObjectiveData.iObjectiveID = JOB_COMP_HACK_OBJ
								IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[ObjectiveData.iEntityID].mn = HEI_P_ATTACHE_CASE_SHUT_S
									RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_CARD_SCANNER")
								ENDIF
							ENDIF
							
							IF ObjectiveData.iObjectiveID = JOB_COMP_ARRIVE_OBJ
								IF ObjectiveData.Details.FromPlayerIndex != LocalPlayer
								AND GET_DISTANCE_BETWEEN_PEDS(GET_PLAYER_PED(ObjectiveData.Details.FromPlayerIndex),LocalPlayerPed) <= 30.0
								AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_IN_AND_OUT_SOUNDS)	
								AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_STOCKPILE_SOUNDS)
									PLAY_SOUND_FRONTEND(-1, "Friend_Pick_Up", "HUD_FRONTEND_MP_COLLECTABLE_SOUNDS", FALSE)
								ENDIF
							ENDIF
							
						ENDIF
						
						IF IS_THIS_ROCKSTAR_MISSION_SVM_PHANTOM2(g_FMMC_STRUCT.iRootContentIDHash)
							IF ObjectiveData.iObjectiveID = JOB_COMP_HACK_OBJ
								PRINTLN("[KH][COKEGRAB] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Adding coke grab trolley complete")
								g_TransitionSessionNonResetVars.iNumCokeGrabsCompleted++
								INT iNumTrolleysFirstCheckpoint
								iNumTrolleysFirstCheckpoint = 2
								INT iNumTrolleysSecondCheckpoint
								iNumTrolleysSecondCheckpoint = 1
								IF GET_NUMBER_OF_ACTIVE_PARTICIPANTS() = 3
									PRINTLN("[KH][COKEGRAB] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - 3 players reducing trolleys grabbed")
									iNumTrolleysFirstCheckpoint--
								ENDIF
								IF g_TransitionSessionNonResetVars.iLocalPlayerCheckpoint1CashGrabbed = 0
								AND g_TransitionSessionNonResetVars.iNumCokeGrabsCompleted = iNumTrolleysFirstCheckpoint
									PRINTLN("[KH][COKEGRAB] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Setting local player cash and total at checkpoint 1")
									g_TransitionSessionNonResetVars.iLocalPlayerCheckpoint1CashGrabbed = g_TransitionSessionNonResetVars.iLocalPlayerCashGrabbed
									g_TransitionSessionNonResetVars.iCheckpoint1CashGrabbed = MC_ServerBD.iCashGrabTotalTake
								ELIF g_TransitionSessionNonResetVars.iLocalPlayerCheckpoint2CashGrabbed = 0 
								AND g_TransitionSessionNonResetVars.iNumCokeGrabsCompleted = iNumTrolleysFirstCheckpoint + iNumTrolleysSecondCheckpoint
									PRINTLN("[KH][COKEGRAB] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Setting local player cash and total at checkpoint 2")
									g_TransitionSessionNonResetVars.iLocalPlayerCheckpoint2CashGrabbed = g_TransitionSessionNonResetVars.iLocalPlayerCashGrabbed
									g_TransitionSessionNonResetVars.iCheckpoint2CashGrabbed = MC_ServerBD.iCashGrabTotalTake
								ELIF g_TransitionSessionNonResetVars.iNumCokeGrabsCompleted > 2
									PRINTLN("[KH][COKEGRAB] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Player completed a coke grab - num complete: ", g_TransitionSessionNonResetVars.iNumCokeGrabsCompleted)
								ENDIF
							ENDIF
						ENDIF
					BREAK // End of JOB_COMP_ARRIVE_OBJ, JOB_COMP_PHOTO_OBJ and JOB_COMP_HACK_OBJ
					
					CASE JOB_COMP_ARRIVE_PED
					CASE JOB_COMP_PHOTO_PED
					CASE JOB_COMP_CHARM_PED
					
						IF ObjectiveData.iEntityID != -1
							IF DOES_BLIP_EXIST(biPedBlip[ObjectiveData.iEntityID])
								CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] Removing blip for ped ", ObjectiveData.iEntityID, " as an ARRIVE, PHOTO, or CHARM ped job is complete.")
								REMOVE_BLIP(biPedBlip[ObjectiveData.iEntityID])
							ENDIF
						ENDIF
						
					BREAK // End of JOB_COMP_ARRIVE_PED, JOB_COMP_PHOTO_PED and JOB_COMP_CHARM_PED
					
					CASE JOB_COMP_ARRIVE_VEH
					CASE JOB_COMP_PHOTO_VEH
				
						IF ObjectiveData.iEntityID != -1
//							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[ObjectiveData.iEntityID].iVehBitsetTwo, ciFMMC_VEHICLE2_TRACKIFY_TARGET)
//								CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[ObjectiveData.iEntityID].iVehBitsetTwo, ciFMMC_VEHICLE2_TRACKIFY_TARGET) 
//								PRINTLN("[AW_MISSION] - [RCC MISSION] [TRACKIFY] - iTrackifyTargets no. ",ObjectiveData.iEntityID, "has been flagged as no longer a target. " ) 
//							ENDIF
							IF DOES_BLIP_EXIST(biVehBlip[ObjectiveData.iEntityID])
							AND NOT SHOULD_VEHICLE_HAVE_CUSTOM_BLIP(ObjectiveData.iEntityID)
								CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] Removing blip for vehicle ", ObjectiveData.iEntityID, " as an ARRIVE or PHOTO veh job is complete.")
								REMOVE_VEHICLE_BLIP(ObjectiveData.iEntityID)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - JOB_COMP_ARRIVE_VEH objective complete removing blip for veh: ",ObjectiveData.iEntityID)
							ENDIF
						ENDIF
						
					BREAK // ENd of JOB_COMP_ARRIVE_VEH and JOB_COMP_PHOTO_VEH
					
					CASE JOB_COMP_DELIVER_OBJ
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciSHOW_LAST_DELIVERER_SHARD)
							piLastDeliverer = ObjectiveData.Details.FromPlayerIndex
						ENDIF
						
						FOR irepeat = 0 TO (FMMC_MAX_NUM_OBJECTS-1)
							IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[irepeat])
								IF IS_BIT_SET(ObjectiveData.iEntityID, irepeat)
									CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] Removing blip for package ", irepeat, " as a delivery job is complete.")
									REMOVE_OBJECT_BLIP(irepeat)
								ENDIF
							ENDIF
						ENDFOR

					BREAK

					CASE JOB_COMP_DELIVER_PED
					
						FOR irepeat = 0 TO (FMMC_MAX_PEDS-1)
							IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[irepeat])
								IF NOT SHOULD_PED_HAVE_BLIP( irepeat, iBlipColour, bCustomBlip )
									IF DOES_BLIP_EXIST(biPedBlip[irepeat])
										CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] Removing blip for ped ", irepeat, " as a delivery job is complete.")
										REMOVE_BLIP(biPedBlip[irepeat])
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
						
					BREAK // End of JOB_COMP_DELIVER_PED
					
					CASE JOB_COMP_DELIVER_VEH
						
						IF ObjectiveData.iEntityID != -1
														
							IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[ObjectiveData.iParticipant].iteam].iDropOffStopTimer[ObjectiveData.iPriority] = -1
								IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[ObjectiveData.iEntityID])
									VEHICLE_INDEX tempVeh
									tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[ObjectiveData.iEntityID])
									IF IS_PED_IN_VEHICLE(LocalPlayerPed, tempVeh)
										SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_PreventAutoShuffleToDriversSeat, TRUE)
										
										bStopDurationBlockSeatShuffle = TRUE
									ENDIF
								ENDIF
							ENDIF
							
							IF DOES_BLIP_EXIST(biVehBlip[ObjectiveData.iEntityID])
							AND NOT SHOULD_VEHICLE_HAVE_CUSTOM_BLIP(ObjectiveData.iEntityID)
								CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] Removing blip for vehicle ", ObjectiveData.iEntityID, " as a delivery job is complete.")
								REMOVE_VEHICLE_BLIP(ObjectiveData.iEntityID)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - JOB_COMP_DELIVER_VEH objective complete removing blip for veh: ",ObjectiveData.iEntityID) 
							ENDIF
							
							PROCESS_DELIVER_VEH_STAT_TRACKING(ObjectiveData.iEntityID)						
						ENDIF
						
					BREAK // End of JOB_COMP_DELIVER_VEH
					
				ENDSWITCH
			
			ELSE // Else the person is the Host of this script, the above are not
				
				
				IF NOT IS_BIT_SET(MC_serverBD.iProcessJobCompBitset,ObjectiveData.iparticipant)
				
					SWITCH ObjectiveData.iObjectiveID
						
						// this is where the server will update its data based upon the player broadcast data MC_playerBD[iPartToUse].iObjectiveTypeCompleted
						
						CASE JOB_COMP_ARRIVE_LOC
							
							PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ************************************ ")
							PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - CASE = JOB_COMP_ARRIVE_LOC")
								
							IF ObjectiveData.iEntityID != -1
								
								ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] = MC_serverBD_4.iGotoLocationDataPriority[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam] 
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ", ObjectiveData.iPriority)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iparticipant = ", ObjectiveData.iparticipant)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_playerBD[ObjectiveData.iparticipant].iteam = ", MC_playerBD[ObjectiveData.iparticipant].iteam)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam])
								
								IF ObjectiveData.iPriority = ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]
								AND MC_serverBD_4.iGotoLocationDataRule[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam] = FMMC_OBJECTIVE_LOGIC_GO_TO
								AND ObjectiveData.iRevalidates = MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam]
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]")
									
									IF ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] < FMMC_MAX_RULES
										IF DOES_THIS_LOCATION_HAVE_A_SPECIFIC_ENTRY_HEADING( ObjectiveData.iEntityID )
										AND NOT IS_PARTICIPANT_FACING_CORRECT_DIRECTION_FOR_LOCATION( ObjectiveData.iEntityID, ObjectiveData.iparticipant )
											
											PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - iLoc: ", ObjectiveData.iEntityID, " In here (1)")
											
											IF NOT IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ ObjectiveData.iEntityID ].iLocBS2, ciLoc_BS2_DirectionRestrictPass )
												FAIL_TEAM_FOR_INCORRECT_GOTO_HEADING( iTeam )
											ENDIF
										ELSE // Else this is a valid locate :)
										
											PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - iLoc: ", ObjectiveData.iEntityID, " In here (2)")											
											
											IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
												PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Setting Midpoint iObjectiveMidPointBitset for Team arrive loc ", MC_playerBD[ObjectiveData.iparticipant].iteam, " on Rule ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
												SET_BIT(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
												SET_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
											ENDIF
										
											SET_TEAM_OBJECTIVE_OUTCOME(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],TRUE)
											MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam] = OBJ_END_REASON_ARV_LOC
											
											MC_ServerBD.iLastCompletedGoToLocation = ObjectiveData.iEntityID
											
											PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_LOCATION,ObjectiveData.iEntityID,MC_playerBD[ObjectiveData.iparticipant].iteam,TRUE)
											
											IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_LOCATION,MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
												PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - iLoc: ", ObjectiveData.iEntityID, " In here (3) - IS_THIS_OBJECTIVE_OVER")
												
												SET_LOCAL_OBJECTIVE_END_MESSAGE(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam])
												BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam],MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],DID_TEAM_PASS_OBJECTIVE(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]))
											ENDIF
											
											IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ObjectiveData.iEntityID].iWholeTeamAtLocation[MC_playerBD[ObjectiveData.iparticipant].iteam] != ciGOTO_LOCATION_INDIVIDUAL
												INCREMENT_SERVER_TEAM_SCORE(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],GET_FMMC_POINTS_FOR_TEAM(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]))
											ENDIF
											
											IF MC_playerBD[ObjectiveData.iparticipant].iteam = MC_PlayerBD[ iPartToUse ].iTeam
												IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciSOUND_ON_TEAMMATE_CHECKPOINT)
													IF ObjectiveData.iParticipant != iPartToUse
														PLAY_SOUND_FRONTEND(-1, "Checkpoint_Teammate", "GTAO_Shepherd_Sounds", FALSE)
													ENDIF
												ENDIF
												
												IF DOES_BLIP_EXIST(LocBlip[ObjectiveData.iEntityID])
													// url:bugstar:2185410
													IF DOES_BLIP_HAVE_GPS_ROUTE(LocBlip[ObjectiveData.iEntityID])
														SET_IGNORE_NO_GPS_FLAG(FALSE)
														PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT called SET_IGNORE_NO_GPS_FLAG FALSE (B)")
													ENDIF
												
													REMOVE_BLIP(LocBlip[ObjectiveData.iEntityID])
												ENDIF
												IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_ENABLE_VENETIAN_JOB_SOUNDS)
												AND ObjectiveData.iparticipant = iLocalPart
												AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
													BROADCAST_FMMC_LOCATE_ALERT_SOUND()
												ENDIF
											ENDIF
											
											IF NOT IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_SHOULD_PLAY_RUNNING_BACK_REMIX_SCORED)
											AND GET_PLAYER_RESPAWN_BACK_AT_START_STATE(iLocalPart) = PRBSS_PlayingNormally
												PRINTLN("[LM][PROCESS_RUNNING_BACK_SHARDS] - SBBOOL7_SHOULD_PLAY_RUNNING_BACK_REMIX_SCORED - Setting bit.")
												SET_BIT(MC_ServerBD.iServerBitSet7, SBBOOL7_SHOULD_PLAY_RUNNING_BACK_REMIX_SCORED)
											ENDIF
											
											IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ObjectiveData.iEntityID].iLocBS3, ciLoc_BS3_IncrementTimeRemaining)
												IF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ALL_TEAMS_SHARE_LOCATES)
												AND IS_PED_IN_LOCATION(GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_NATIVE(PARTICIPANT_INDEX, ObjectiveData.iparticipant))), ObjectiveData.iEntityID, iTeam))
												OR NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ALL_TEAMS_SHARE_LOCATES)
													INT iIncrementTime
													iIncrementTime = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ObjectiveData.iEntityID].iIncrementTime * 1000
												
													BROADCAST_FMMC_INCREMENT_REMAINING_TIME(MC_playerBD[ObjectiveData.iparticipant].iteam, iIncrementTime, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ObjectiveData.iEntityID].iLocBS3, ciLoc_BS3_IncrementTimeTeamOnly), ObjectiveData.iparticipant)
												ENDIF
											ENDIF
											
											tempPart = INT_TO_PARTICIPANTINDEX(ObjectiveData.iparticipant)
											
											IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
												tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
												IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ObjectiveData.iEntityID].iWholeTeamAtLocation[MC_playerBD[ObjectiveData.iparticipant].iteam] = ciGOTO_LOCATION_INDIVIDUAL
													INCREMENT_SERVER_REMOTE_PLAYER_SCORE(tempPlayer, GET_FMMC_POINTS_FOR_TEAM(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]),MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
												ENDIF
											ENDIF
											SET_CUTSCENE_PRIMARY_PLAYER(ObjectiveData.iparticipant, MC_playerBD[ObjectiveData.iparticipant].iteam)

											PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_playerBD[iparticipant].iCurrentLoc : ",ObjectiveData.iEntityID)
											PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - server processing JOB_COMP_ARRIVE_LOC from part: ",ObjectiveData.iparticipant)
										ENDIF
									ENDIF
								#IF IS_DEBUG_BUILD	
								ELSE
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - priorities or revalidates don't match up!")
								
								#ENDIF
								
								ENDIF
								
							ENDIF
							
						BREAK // End of JOB_COMP_ARRIVE_LOC
						
						
						CASE JOB_COMP_ARRIVE_PED
						
							IF ObjectiveData.iEntityID != -1
								
								ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] = MC_serverBD_4.iPedPriority[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam]
								
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ************************************ ")
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - CASE = JOB_COMP_ARRIVE_PED")
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ", ObjectiveData.iPriority)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iparticipant = ", ObjectiveData.iparticipant)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_playerBD[ObjectiveData.iparticipant].iteam = ", MC_playerBD[ObjectiveData.iparticipant].iteam)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam])
								
								IF ObjectiveData.iPriority = ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]
								AND MC_serverBD_4.iPedRule[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam] = FMMC_OBJECTIVE_LOGIC_GO_TO
								AND ObjectiveData.iRevalidates = MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam]
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]")
									
									IF ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] < FMMC_MAX_RULES
										
										IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Setting Midpoint iObjectiveMidPointBitset for Team arrive ped ", MC_playerBD[ObjectiveData.iparticipant].iteam, " on Rule ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_BIT(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
										ENDIF
										
										SET_TEAM_OBJECTIVE_OUTCOME(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],TRUE)
										MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam] = OBJ_END_REASON_ARV_PED
										
										PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED,ObjectiveData.iEntityID,MC_playerBD[ObjectiveData.iparticipant].iteam,TRUE)
										
										IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_PED,MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_LOCAL_OBJECTIVE_END_MESSAGE(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam])
											BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam],MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],DID_TEAM_PASS_OBJECTIVE(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]))
										ENDIF
										
										IF NOT SHOULD_PED_HAVE_BLIP( ObjectiveData.iEntityID, iBlipColour, bCustomBlip )
											IF DOES_BLIP_EXIST(biPedBlip[ObjectiveData.iEntityID])
												REMOVE_BLIP(biPedBlip[ObjectiveData.iEntityID])
											ENDIF
										ENDIF

										PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iEntityID: ",ObjectiveData.iEntityID)
										PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - server processing JOB_COMP_ARRIVE_PED from part: ",ObjectiveData.iparticipant)
										
									ENDIF
									
								#IF IS_DEBUG_BUILD	
								ELSE
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - priorities or revalidates don't match up!")
								
								#ENDIF
									
								ENDIF
								
							ENDIF
							
						BREAK // End of JOB_COMP_ARRIVE_PED
						
						CASE JOB_COMP_ARRIVE_VEH
						
							IF ObjectiveData.iEntityID != -1
							
								ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] = MC_serverBD_4.iVehPriority[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam]
								
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ************************************ ")
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - CASE = JOB_COMP_ARRIVE_VEH")
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ", ObjectiveData.iPriority)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iparticipant = ", ObjectiveData.iparticipant)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_playerBD[ObjectiveData.iparticipant].iteam = ", MC_playerBD[ObjectiveData.iparticipant].iteam)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam])
								
								IF ObjectiveData.iPriority = ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]
								AND MC_serverBD_4.ivehRule[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam] = FMMC_OBJECTIVE_LOGIC_GO_TO
								AND ObjectiveData.iRevalidates = MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam]
									
									PRINTLN("[RCC MISSION] - PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT: ObjectiveData.iPriority = ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]")
									
									IF ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] < FMMC_MAX_RULES
										//url:bugstar:3219654
										INCREMENT_SERVER_REMOTE_PLAYER_SCORE(ObjectiveData.Details.FromPlayerIndex, GET_FMMC_POINTS_FOR_TEAM(MC_playerBD[ObjectiveData.iparticipant].iteam, ObjectiveData.iPriority), MC_playerBD[ObjectiveData.iparticipant].iteam, ObjectiveData.iPriority)
										IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Setting Midpoint iObjectiveMidPointBitset arrive veh for Team: ", MC_playerBD[ObjectiveData.iparticipant].iteam, " on Rule ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_BIT(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
										ENDIF
										
										INT iRule
										iRule = MC_serverBD_4.ivehRule[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam]
										
										BOOL bCheckOnRule
										bCheckOnRule = TRUE
										INT iTeamBS
										iTeamBS = GET_VEHICLE_GOTO_TEAMS(MC_playerBD[ObjectiveData.iparticipant].iteam,ObjectiveData.iEntityID,bCheckOnRule)
										
										FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
											ipriority[iteam] = MC_serverBD_4.iVehPriority[ObjectiveData.iEntityID][iTeam]
											IF ipriority[iteam] < FMMC_MAX_RULES
											AND ((NOT bCheckOnRule) OR (ipriority[iteam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam]))
											AND MC_serverBD_4.ivehRule[ObjectiveData.iEntityID][iTeam] = iRule
												IF IS_BIT_SET(iTeamBS,iTeam)
												OR (iTeam = MC_playerBD[ObjectiveData.iparticipant].iteam)
													SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iteam],TRUE)
													MC_serverBD.iReasonForObjEnd[iteam] = OBJ_END_REASON_ARV_VEH
													PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE,ObjectiveData.iEntityID,iTeam,TRUE)
													
													IF (iTeam != MC_playerBD[ObjectiveData.iparticipant].iteam)
													AND NOT bCheckOnRule
														INCREMENT_SERVER_TEAM_SCORE(iteam,ipriority[iTeam],GET_FMMC_POINTS_FOR_TEAM(iteam,ipriority[iTeam]))
													ENDIF
													
												ENDIF
											ENDIF
										ENDFOR
										
										IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_VEHICLE,MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_LOCAL_OBJECTIVE_END_MESSAGE(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam])
											BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam],MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],DID_TEAM_PASS_OBJECTIVE(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]))
										ENDIF
										
										IF DOES_BLIP_EXIST(biVehBlip[ObjectiveData.iEntityID])
										AND NOT SHOULD_VEHICLE_HAVE_CUSTOM_BLIP(ObjectiveData.iEntityID)
											REMOVE_VEHICLE_BLIP(ObjectiveData.iEntityID)
											PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - JOB_COMP_ARRIVE_VEH objective complete removing blip for veh HOST: ",ObjectiveData.iEntityID)
										ENDIF
										
										PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iEntityID: ",ObjectiveData.iEntityID) 
										PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - server processing JOB_COMP_ARRIVE_VEH from part: ",ObjectiveData.iparticipant) 
									
									ENDIF
									
								#IF IS_DEBUG_BUILD	
								ELSE
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - priorities or revalidates don't match up!")
								
								#ENDIF
									
								ENDIF
								
							ENDIF
							
						BREAK // End of JOB_COMP_ARRIVE_VEH
						
						CASE JOB_COMP_ARRIVE_OBJ
						
							IF ObjectiveData.iEntityID != -1
							
								ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] = MC_serverBD_4.iObjPriority[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam] 
								
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ************************************ ")
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - CASE = JOB_COMP_ARRIVE_OBJ")
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ", ObjectiveData.iPriority)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iparticipant = ", ObjectiveData.iparticipant)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iEntityID = ", ObjectiveData.iEntityID)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_playerBD[ObjectiveData.iparticipant].iteam = ", MC_playerBD[ObjectiveData.iparticipant].iteam)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam])
								
								IF ObjectiveData.iPriority = ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]
								AND MC_serverBD_4.iObjRule[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam] = FMMC_OBJECTIVE_LOGIC_GO_TO
								AND ObjectiveData.iRevalidates = MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam]
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]")
									
									IF ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] < FMMC_MAX_RULES
										
										IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Setting Midpoint iObjectiveMidPointBitset for Team arrive obj ", MC_playerBD[ObjectiveData.iparticipant].iteam, " on Rule ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_BIT(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
										ENDIF
										
										IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[ObjectiveData.iEntityID])
										AND SHOULD_OBJ_CLEAN_UP_ON_DELIVERY(ObjectiveData.iEntityID, MC_playerBD[ObjectiveData.iparticipant].iteam)
											PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - JOB_COMP_ARRIVE_OBJ object should clean up!")
											IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niObject[ObjectiveData.iEntityID])
												DELETE_NET_ID(MC_serverBD_1.sFMMC_SBD.niObject[ObjectiveData.iEntityID])
											ELSE
												CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niObject[ObjectiveData.iEntityID])
											ENDIF
										ENDIF
										
										IF ObjectiveData.Details.FromPlayerIndex != LocalPlayer
										AND GET_DISTANCE_BETWEEN_PEDS(GET_PLAYER_PED(ObjectiveData.Details.FromPlayerIndex),LocalPlayerPed) <= 30.0
										AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_IN_AND_OUT_SOUNDS)
										AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_STOCKPILE_SOUNDS)
											PLAY_SOUND_FRONTEND(-1, "Friend_Pick_Up", "HUD_FRONTEND_MP_COLLECTABLE_SOUNDS", FALSE)
										ENDIF
										
										IF MC_serverBD_2.iCurrentObjRespawnLives[ObjectiveData.iEntityID] <= 0
											IF SHOULD_OBJ_CLEAN_UP_ON_DELIVERY(ObjectiveData.iEntityID,MC_playerBD[ObjectiveData.iparticipant].iteam)
												FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
													IF iteam = MC_playerBD[ObjectiveData.iparticipant].iteam
														IF IS_BIT_SET(MC_serverBD.iObjteamFailBitset[ObjectiveData.iEntityID],iteam)
															CLEAR_BIT(MC_serverBD.iObjteamFailBitset[ObjectiveData.iEntityID],iteam)
															CLEAR_BIT(MC_serverBD.iMissionCriticalObj[iteam], ObjectiveData.iEntityID)
															bwasobjective[iTeam] = TRUE
														ENDIF
													ELSE
														IF DOES_TEAM_LIKE_TEAM(iteam,MC_playerBD[ObjectiveData.iparticipant].iteam)
															IF IS_BIT_SET(MC_serverBD.iObjteamFailBitset[ObjectiveData.iEntityID],iteam)
																CLEAR_BIT(MC_serverBD.iObjteamFailBitset[ObjectiveData.iEntityID],iteam)
																CLEAR_BIT(MC_serverBD.iMissionCriticalObj[iteam], ObjectiveData.iEntityID)
																bwasobjective[iTeam] =TRUE
															ENDIF
														ENDIF
													ENDIF
												ENDFOR
											ENDIF
											
											FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
												
												ipriority[iteam] = MC_serverBD_4.iObjPriority[ObjectiveData.iEntityID][iteam]
												
												IF iteam != MC_playerBD[ObjectiveData.iparticipant].iteam
													
													IF SHOULD_OBJ_CLEAN_UP_ON_DELIVERY(ObjectiveData.iEntityID,MC_playerBD[ObjectiveData.iparticipant].iteam)
														IF IS_BIT_SET(MC_serverBD.iObjteamFailBitset[ObjectiveData.iEntityID],iteam)
															IF ipriority[iteam] < FMMC_MAX_RULES
																IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iTeamFailBitset,ipriority[iteam])
																	bobjectivefails[iTeam] = TRUE
																	SET_TEAM_FAILED(iTeam,mFail_ARV_OBJ)
																	MC_serverBD.iEntityCausingFail[iteam] = ObjectiveData.iEntityID
																	PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MISSION FAILED FOR TEAM:",iteam," BECAUSE OBJ GONE TO: ",ObjectiveData.iEntityID)
																ENDIF
															ENDIF
															bwasobjective[iTeam] = TRUE
															CLEAR_BIT(MC_serverBD.iObjteamFailBitset[ObjectiveData.iEntityID],iteam)
														ENDIF
													ENDIF
													
													PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - team ", iTeam, " - obj's primary rule: ",MC_serverBD_4.iObjPriority[ObjectiveData.iEntityID][iTeam])
													PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - team ", iTeam, " - MC_serverBD_4.iCurrentHighestPriority[iTeam]: ",MC_serverBD_4.iCurrentHighestPriority[iTeam])
													PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ", ObjectiveData.iPriority)
													
													IF ( ipriority[iTeam] = MC_serverBD_4.iCurrentHighestPriority[iTeam]
														OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[ObjectiveData.iEntityID].iObjectBitSet, cibsOBJ_InvalidateAllTeams) )
													AND ipriority[iteam] < FMMC_MAX_RULES
														IF DOES_TEAM_LIKE_TEAM(iteam,MC_playerBD[ObjectiveData.iparticipant].iteam)
															IF MC_serverBD_4.iObjRule[ObjectiveData.iEntityID][iteam] = FMMC_OBJECTIVE_LOGIC_PROTECT
																PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Protect Obj rule adding points = ", GET_FMMC_POINTS_FOR_TEAM(iteam,ipriority[iTeam]))
																INCREMENT_SERVER_TEAM_SCORE(iteam,ipriority[iTeam],GET_FMMC_POINTS_FOR_TEAM(iteam,ipriority[iTeam]))
																//BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM_PRO_OBJ,0,iteam,-1,NULL,ci_TARGET_OBJECT,ObjectiveData.iEntityID)
															ENDIF
															SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iteam], TRUE)
															PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,ObjectiveData.iEntityID,iTeam,TRUE)	
														ELSE
															SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iteam], FALSE)
															PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,ObjectiveData.iEntityID,iTeam,FALSE)
														ENDIF
														MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_ARV_OBJ
													ENDIF
												ELSE
													PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ipriority[iteam] team ", iTeam, " - priority: ",ipriority[iteam])
													PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ", ObjectiveData.iPriority)
													
													SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iteam], TRUE)
													PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,ObjectiveData.iEntityID,iTeam,TRUE)	
													MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_ARV_OBJ
												ENDIF
												
											ENDFOR
											
										ENDIF
										
										FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
											IF bwasobjective[iTeam]
												IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_OBJECT,iteam,ipriority[iteam])
													SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam,ipriority[iteam],MC_serverBD.iReasonForObjEnd[iteam],bobjectivefails[iTeam])
													BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iteam],iteam,ipriority[iteam],DID_TEAM_PASS_OBJECTIVE(iteam,ipriority[iteam]),bobjectivefails[iTeam])
												ENDIF
											ENDIF
										ENDFOR
										
										REMOVE_OBJECT_BLIP(ObjectiveData.iEntityID)
										
										SET_CUTSCENE_PRIMARY_PLAYER(ObjectiveData.iparticipant, MC_playerBD[ObjectiveData.iparticipant].iteam)
										
										PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iEntityID: ",ObjectiveData.iEntityID) 
										PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - server processing JOB_COMP_ARRIVE_OBJ from part: ",ObjectiveData.iparticipant)
										
									ENDIF
									
								#IF IS_DEBUG_BUILD	
								ELSE
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - priorities or revalidates don't match up!")
								
								#ENDIF
									
								ENDIF
								
							ENDIF
							
						BREAK // End of JOB_COMP_ARRIVE_OBJ
						
						CASE JOB_COMP_PHOTO_LOC
						
							IF ObjectiveData.iEntityID != -1
								
								ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] = MC_serverBD_4.iGotoLocationDataPriority[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam] 
								
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ************************************ ")
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - CASE = JOB_COMP_PHOTO_LOC")
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ", ObjectiveData.iPriority)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iparticipant = ", ObjectiveData.iparticipant)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_playerBD[ObjectiveData.iparticipant].iteam = ", MC_playerBD[ObjectiveData.iparticipant].iteam)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam])
								
								IF ObjectiveData.iPriority = ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]
								AND MC_serverBD_4.iGotoLocationDataRule[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam] = FMMC_OBJECTIVE_LOGIC_PHOTO
								AND ObjectiveData.iRevalidates = MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam]
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]")
									
									IF ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] < FMMC_MAX_RULES
										
										IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Setting Midpoint iObjectiveMidPointBitset for Team photo loc ", MC_playerBD[ObjectiveData.iparticipant].iteam, " on Rule ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_BIT(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
										ENDIF
										
										SET_TEAM_OBJECTIVE_OUTCOME(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam], TRUE)
										MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam] = OBJ_END_REASON_PHOTO_LOC

										PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_LOCATION,ObjectiveData.iEntityID,MC_playerBD[ObjectiveData.iparticipant].iteam,TRUE)
										
										IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_LOCATION,MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_LOCAL_OBJECTIVE_END_MESSAGE(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam])
											BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam],MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],DID_TEAM_PASS_OBJECTIVE(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]))
										ENDIF
										
										IF DOES_BLIP_EXIST(LocBlip[ObjectiveData.iEntityID])
											REMOVE_BLIP(LocBlip[ObjectiveData.iEntityID])
										ENDIF
										
										IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[ObjectiveData.iparticipant].iteam].iRuleBitset[ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]], ciBS_RULE_INVALIDATE_OTHER_TEAMS)
											FOR iteamrepeat = 0 TO (FMMC_MAX_TEAMS-1)
												IF iteamrepeat != MC_playerBD[ObjectiveData.iparticipant].iteam
													IF MC_serverBD_4.iGotoLocationDataRule[ObjectiveData.iEntityID][iteamrepeat] = FMMC_OBJECTIVE_LOGIC_PHOTO
														PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_LOCATION,ObjectiveData.iEntityID,iteamrepeat,TRUE)
														PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - photo loc iInvalidateTargetForAll call for team: ",iteamrepeat)
													ENDIF
												ENDIF
											ENDFOR
										ENDIF

										PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_playerBD[iparticipant].iLocPhoto : ",ObjectiveData.iEntityID) 
										PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - server processing JOB_COMP_PHOTO_LOC from part: ",ObjectiveData.iparticipant)
									
									ENDIF
									
								#IF IS_DEBUG_BUILD	
								ELSE
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - priorities or revalidates don't match up!")
								
								#ENDIF
									
								ENDIF
								
							ENDIF
							
						BREAK // End of JOB_COMP_PHOTO_LOC
						
						CASE JOB_COMP_PHOTO_PED
						
							IF ObjectiveData.iEntityID != -1
							
								ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] = MC_serverBD_4.iPedPriority[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam]
								
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ************************************ ")
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - CASE = JOB_COMP_PHOTO_PED")
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ", ObjectiveData.iPriority)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iparticipant = ", ObjectiveData.iparticipant)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_playerBD[ObjectiveData.iparticipant].iteam = ", MC_playerBD[ObjectiveData.iparticipant].iteam)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam])
								
								IF ObjectiveData.iPriority = ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]
								AND MC_serverBD_4.iPedRule[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam] = FMMC_OBJECTIVE_LOGIC_PHOTO
								AND ObjectiveData.iRevalidates = MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam]
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]")
									
									IF ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] < FMMC_MAX_RULES
									
										IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Setting Midpoint iObjectiveMidPointBitset for Team photo ped ", MC_playerBD[ObjectiveData.iparticipant].iteam, " on Rule ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_BIT(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
										ENDIF
									
										SET_TEAM_OBJECTIVE_OUTCOME(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],TRUE)
										MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam] = OBJ_END_REASON_PHOTO_PED
										
										PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED,ObjectiveData.iEntityID,MC_playerBD[ObjectiveData.iparticipant].iteam,TRUE)
										
										IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_PED,MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_LOCAL_OBJECTIVE_END_MESSAGE(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam])
											BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam],MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],DID_TEAM_PASS_OBJECTIVE(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]))
										ENDIF
									
										IF DOES_BLIP_EXIST(biPedBlip[ObjectiveData.iEntityID])
											REMOVE_BLIP(biPedBlip[ObjectiveData.iEntityID])
										ENDIF

										CLEAR_BIT(MC_serverBD.iDeadPedPhotoBitset[MC_playerBD[ObjectiveData.iparticipant].iteam][GET_LONG_BITSET_INDEX(ObjectiveData.iEntityID)], GET_LONG_BITSET_BIT(ObjectiveData.iEntityID))
										
										IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[ObjectiveData.iparticipant].iteam].iRuleBitset[ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]], ciBS_RULE_INVALIDATE_OTHER_TEAMS)
											FOR iteamrepeat = 0 TO (FMMC_MAX_TEAMS-1)
												IF iteamrepeat != MC_playerBD[ObjectiveData.iparticipant].iteam
													IF MC_serverBD_4.iPedRule[ObjectiveData.iEntityID][iteamrepeat] = FMMC_OBJECTIVE_LOGIC_PHOTO
														PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED,ObjectiveData.iEntityID,iteamrepeat,TRUE)
														CLEAR_BIT(MC_serverBD.iDeadPedPhotoBitset[iteamrepeat][GET_LONG_BITSET_INDEX(ObjectiveData.iEntityID)], GET_LONG_BITSET_BIT(ObjectiveData.iEntityID))
														PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - photo ped iInvalidateTargetForAll call for team: ",iteamrepeat)
													ENDIF
												ENDIF
											ENDFOR
										ENDIF
										
										PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iEntityID: ",ObjectiveData.iEntityID) 
										PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - server processing JOB_COMP_PHOTO_PED from part: ",ObjectiveData.iparticipant)
									ENDIF
									
								#IF IS_DEBUG_BUILD	
								ELSE
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - priorities or revalidates don't match up!")
								
								#ENDIF
								
								ENDIF
								
							ENDIF
							
						BREAK // End of JOB_COMP_PHOTO_PED
						
						CASE JOB_COMP_CHARM_PED
						
							IF ObjectiveData.iEntityID != -1
							
								ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] = MC_serverBD_4.iPedPriority[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam]
								
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ************************************ ")
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - CASE = JOB_COMP_CHARM_PED")
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ", ObjectiveData.iPriority)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iparticipant = ", ObjectiveData.iparticipant)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_playerBD[ObjectiveData.iparticipant].iteam = ", MC_playerBD[ObjectiveData.iparticipant].iteam)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam])
								
								IF ObjectiveData.iPriority = ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]
								AND MC_serverBD_4.iPedRule[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam] = FMMC_OBJECTIVE_LOGIC_CHARM
								AND ObjectiveData.iRevalidates = MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam]
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]")
									
									IF ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] < FMMC_MAX_RULES
										
										IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Setting Midpoint iObjectiveMidPointBitset for Team charm ped ", MC_playerBD[ObjectiveData.iparticipant].iteam, " on Rule ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_BIT(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
										ENDIF
										
										SET_TEAM_OBJECTIVE_OUTCOME(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],TRUE)
										MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam] = OBJ_END_REASON_CHARM_PED
										
										PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED,ObjectiveData.iEntityID,MC_playerBD[ObjectiveData.iparticipant].iteam,TRUE)
										
										IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_PED,MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_LOCAL_OBJECTIVE_END_MESSAGE(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam])
											BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam],MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],DID_TEAM_PASS_OBJECTIVE(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]))
										ENDIF
									
										IF DOES_BLIP_EXIST(biPedBlip[ObjectiveData.iEntityID])
											REMOVE_BLIP(biPedBlip[ObjectiveData.iEntityID])
										ENDIF
										
										PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iEntityID: ",ObjectiveData.iEntityID) 
										PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - server processing JOB_COMP_CHARM_PED from part: ",ObjectiveData.iparticipant)
									ENDIF
									
								#IF IS_DEBUG_BUILD	
								ELSE
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - priorities or revalidates don't match up!")
								
								#ENDIF
								
								ENDIF
								
							ENDIF
						BREAK // End of JOB_COMP_CHARM_PED
						
						
						CASE JOB_COMP_PHOTO_VEH
						
							IF ObjectiveData.iEntityID != -1

								ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] = MC_serverBD_4.iVehPriority[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam]
								
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ************************************ ")
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - CASE = JOB_COMP_PHOTO_VEH")
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ", ObjectiveData.iPriority)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iparticipant = ", ObjectiveData.iparticipant)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_playerBD[ObjectiveData.iparticipant].iteam = ", MC_playerBD[ObjectiveData.iparticipant].iteam)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam])
								
								IF ObjectiveData.iPriority = ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]
								AND MC_serverBD_4.ivehRule[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam] = FMMC_OBJECTIVE_LOGIC_PHOTO
								AND ObjectiveData.iRevalidates = MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam]
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]")
									
									IF ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] < FMMC_MAX_RULES
										
										IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Setting Midpoint iObjectiveMidPointBitset for Team photo veh ", MC_playerBD[ObjectiveData.iparticipant].iteam, " on Rule ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_BIT(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
										ENDIF
										
										tempPart = INT_TO_PARTICIPANTINDEX(ObjectiveData.iparticipant)
											
										IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
											tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
										
											INCREMENT_SERVER_REMOTE_PLAYER_SCORE(tempPlayer, GET_FMMC_POINTS_FOR_TEAM(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]), MC_playerBD[ObjectiveData.iparticipant].iteam, ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
										ENDIF
										
										SET_TEAM_OBJECTIVE_OUTCOME(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],TRUE)
										MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam] = OBJ_END_REASON_PHOTO_VEH
										
										PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE,ObjectiveData.iEntityID,MC_playerBD[ObjectiveData.iparticipant].iteam,TRUE)
										
										IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_VEHICLE,MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_LOCAL_OBJECTIVE_END_MESSAGE(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam])
											BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam],MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],DID_TEAM_PASS_OBJECTIVE(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]))
										ENDIF
										
	//									IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[ObjectiveData.iEntityID].iVehBitsetTwo, ciFMMC_VEHICLE2_TRACKIFY_TARGET)
	//										CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[ObjectiveData.iEntityID].iVehBitsetTwo, ciFMMC_VEHICLE2_TRACKIFY_TARGET) 
	//										PRINTLN("[AW_MISSION] - [RCC MISSION] [TRACKIFY] - iTrackifyTargets no. ",ObjectiveData.iEntityID, "has been flagged as no longer a target. " ) 
	//									ENDIF
										
										IF DOES_BLIP_EXIST(biVehBlip[ObjectiveData.iEntityID])
										AND NOT SHOULD_VEHICLE_HAVE_CUSTOM_BLIP(ObjectiveData.iEntityID)
											REMOVE_VEHICLE_BLIP(ObjectiveData.iEntityID)
											PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - JOB_COMP_PHOTO_VEH objective complete removing blip for veh HOST: ",ObjectiveData.iEntityID)
										ENDIF
										
										IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[ObjectiveData.iparticipant].iteam].iRuleBitset[ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]], ciBS_RULE_INVALIDATE_OTHER_TEAMS)
											FOR iteamrepeat = 0 TO (FMMC_MAX_TEAMS-1)
												IF iteamrepeat != MC_playerBD[ObjectiveData.iparticipant].iteam
													IF MC_serverBD_4.ivehRule[ObjectiveData.iEntityID][iteamrepeat] = FMMC_OBJECTIVE_LOGIC_PHOTO
														PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE,ObjectiveData.iEntityID,iteamrepeat,TRUE)
														PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - photo veh iInvalidateTargetForAll call for team: ",iteamrepeat)
													ENDIF
												ENDIF
											ENDFOR
										ENDIF

										PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iEntityID: ",ObjectiveData.iEntityID)
										PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - server processing JOB_COMP_PHOTO_VEH from part: ",ObjectiveData.iparticipant)
									ENDIF
									
								#IF IS_DEBUG_BUILD	
								ELSE
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - priorities or revalidates don't match up!")
								
								#ENDIF	
								
								ENDIF
								
							ENDIF
							
						BREAK // End of JOB_COMP_PHOTO_VEH
						
						CASE JOB_COMP_PHOTO_OBJ
						
							IF ObjectiveData.iEntityID != -1
								
								ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] = MC_serverBD_4.iObjPriority[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam] 
								
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ************************************ ")
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - CASE = JOB_COMP_PHOTO_OBJ")
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ", ObjectiveData.iPriority)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iparticipant = ", ObjectiveData.iparticipant)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_playerBD[ObjectiveData.iparticipant].iteam = ", MC_playerBD[ObjectiveData.iparticipant].iteam)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam])
								
								IF ObjectiveData.iPriority = ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]
								AND MC_serverBD_4.iObjRule[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam] = FMMC_OBJECTIVE_LOGIC_PHOTO
								AND ObjectiveData.iRevalidates = MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam]
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]")
									
									IF ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] < FMMC_MAX_RULES
										
										IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Setting Midpoint iObjectiveMidPointBitset for Team photo Obj ", MC_playerBD[ObjectiveData.iparticipant].iteam, " on Rule ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_BIT(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
										ENDIF
										
										SET_TEAM_OBJECTIVE_OUTCOME(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],TRUE)
										MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam] = OBJ_END_REASON_PHOTO_OBJ
										
										PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,ObjectiveData.iEntityID,MC_playerBD[ObjectiveData.iparticipant].iteam,TRUE)
										
										IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_OBJECT,MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_LOCAL_OBJECTIVE_END_MESSAGE(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam])
											BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam],MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],DID_TEAM_PASS_OBJECTIVE(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]))
										ENDIF
										REMOVE_OBJECT_BLIP(ObjectiveData.iEntityID)
										
										IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[ObjectiveData.iparticipant].iteam].iRuleBitset[ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]], ciBS_RULE_INVALIDATE_OTHER_TEAMS)
											FOR iteamrepeat = 0 TO (FMMC_MAX_TEAMS-1)
												IF iteamrepeat != MC_playerBD[ObjectiveData.iparticipant].iteam
													IF MC_serverBD_4.iObjRule[ObjectiveData.iEntityID][iteamrepeat] = FMMC_OBJECTIVE_LOGIC_PHOTO
														PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,ObjectiveData.iEntityID,iteamrepeat,TRUE)
														PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - photo Obj iInvalidateTargetForAll call for team: ",iteamrepeat)
													ENDIF
												ENDIF
											ENDFOR
										ENDIF
										
										PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iEntityID: ",ObjectiveData.iEntityID) 
										PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - server processing JOB_COMP_PHOTO_OBJ from part: ",ObjectiveData.iparticipant)
									ENDIF
									
								#IF IS_DEBUG_BUILD	
								ELSE
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - priorities or revalidates don't match up!")
								
								#ENDIF
									
								ENDIF
								
							ENDIF
							
						BREAK // End of JOB_COMP_PHOTO_OBJ
						
						CASE JOB_COMP_HACK_OBJ
						
							IF ObjectiveData.iEntityID != -1
								
								ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] = MC_serverBD_4.iObjPriority[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam] 
								
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ************************************ ")
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - CASE = JOB_COMP_HACK_OBJ")
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ", ObjectiveData.iPriority)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iparticipant = ", ObjectiveData.iparticipant)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_playerBD[ObjectiveData.iparticipant].iteam = ", MC_playerBD[ObjectiveData.iparticipant].iteam)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam])
								
								IF ObjectiveData.iPriority = ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]
								AND MC_serverBD_4.iObjRule[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam] = FMMC_OBJECTIVE_LOGIC_MINIGAME
								AND ObjectiveData.iRevalidates = MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam]
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]")
									
									IF ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] < FMMC_MAX_RULES
										
										PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ipriority: ",ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]) 
										
										IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Setting Midpoint iObjectiveMidPointBitset for Team hack Obj ", MC_playerBD[ObjectiveData.iparticipant].iteam, " on Rule ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_BIT(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
										ENDIF
										
										SET_TEAM_OBJECTIVE_OUTCOME(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],TRUE)
										MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam] = OBJ_END_REASON_HACK_OBJ
										
										IF MC_serverBD_4.iObjMissionSubLogic[MC_playerBD[ObjectiveData.iparticipant].iteam] = ci_HACK_SUBLOGIC_SYNC_LOCK
											INVALIDATE_UP_TO_AND_INCLUDING_OBJECTIVE(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],FALSE,TRUE)
										ELSE
											//for cash grab minigame loop through all teams and progress objective for those who have hack logic rule set on this object
											IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[ObjectiveData.iEntityID ].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_CASH_GRAB)
											
												PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Looping through teams as the placed object has minigame bit set to cibsLEGACY_FMMC_MINI_GAME_TYPE_CASH_GRAB.") 
											
												FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
													PRINTLN("[RCC MISSION] Processing team ", iteam, ".") 
													IF MC_serverBD_4.iObjRule[ObjectiveData.iEntityID][iteam] = FMMC_OBJECTIVE_LOGIC_MINIGAME
														PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Team ", iteam, " objective rule for object is FMMC_OBJECTIVE_LOGIC_MINIGAME.") 
														PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,ObjectiveData.iEntityID,iteam,TRUE)
													ELSE
														PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Team ", iteam, " objective rule for object is not FMMC_OBJECTIVE_LOGIC_MINIGAME.") 
													ENDIF
												ENDFOR
											ELSE
												PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,ObjectiveData.iEntityID,MC_playerBD[ObjectiveData.iparticipant].iteam,TRUE)
											ENDIF
										ENDIF
										
										IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_OBJECT,MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_LOCAL_OBJECTIVE_END_MESSAGE(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam])
											BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam],MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],DID_TEAM_PASS_OBJECTIVE(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]))
										ENDIF
										IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[ObjectiveData.iparticipant].iteam].iLinkedDoor[ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]] !=-1
						
											SET_BIT(MC_serverBD.iHackDoorOpenBitset,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[ObjectiveData.iEntityID ].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_BLOW_DOOR)
												SET_BIT(MC_serverBD.iHackDoorBlownBitset,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											ENDIF
											PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - HOST SETTING DOOR OPEN BITSET FOR RULE: ",ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
										ELIF IS_THIS_A_HACK_CONTAINER( ObjectiveData.iEntityID )
											SET_BIT( MC_serverBD.iOpenHackContainer, ObjectiveData.iEntityID )
											PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Opening hack container: ", ObjectiveData.iEntityID )
										ENDIF
										
										REMOVE_OBJECT_BLIP(ObjectiveData.iEntityID)
										
										IF ObjectiveData.iObjectiveID = JOB_COMP_HACK_OBJ
											IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[ObjectiveData.iEntityID].mn = HEI_P_ATTACHE_CASE_SHUT_S
												RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_CARD_SCANNER")
											ENDIF
										ENDIF
										
										//Destroy object after minigame
										IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[ObjectiveData.iEntityID].iObjectBitSetTwo, cibsOBJ2_DestroyAfterMinigame)
											FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
												IF iteam = MC_playerBD[ObjectiveData.iparticipant].iteam
													IF IS_BIT_SET(MC_serverBD.iObjteamFailBitset[ObjectiveData.iEntityID],iteam)
														CLEAR_BIT(MC_serverBD.iPedteamFailBitset[ObjectiveData.iEntityID],iteam)
														CLEAR_BIT(MC_serverBD.iMissionCriticalObj[iTeam], ObjectiveData.iEntityID)
														
														PRINTLN("[TMS][DestroyAfterMinigame] Clearing iPedteamFailBitset and iMissionCriticalObj for team ", iteam, " on obj ", ObjectiveData.iEntityID)
													ENDIF
												ELSE
													IF DOES_TEAM_LIKE_TEAM(iteam,MC_playerBD[ObjectiveData.iparticipant].iteam)
														IF IS_BIT_SET(MC_serverBD.iPedteamFailBitset[ObjectiveData.iEntityID],iteam)
															CLEAR_BIT(MC_serverBD.iPedteamFailBitset[ObjectiveData.iEntityID],iteam)
															CLEAR_BIT(MC_serverBD.iMissionCriticalObj[iTeam], ObjectiveData.iEntityID)
															
															PRINTLN("[TMS][DestroyAfterMinigame] Clearing iPedteamFailBitset and iMissionCriticalObj for team ", iteam, " on obj ", ObjectiveData.iEntityID)
														ENDIF
													ENDIF
												ENDIF
											ENDFOR
											
											PRINTLN("[TMS][DestroyAfterMinigame] Setting iObjCleanup_NeedOwnershipBS for minigame object ", ObjectiveData.iEntityID, " because of cibsOBJ2_DestroyAfterMinigame")
											SET_BIT(MC_serverBD.iObjCleanup_NeedOwnershipBS, ObjectiveData.iEntityID)
										ENDIF
										
										IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[ObjectiveData.iEntityID].iObjectBitSetTwo, cibsOBJ2_UnblipAfterMinigame)
											SET_BIT(MC_serverBD_4.iObjectDoNotBlipBS, ObjectiveData.iEntityID)
											PRINTLN("[JS] - PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Setting iObjectDoNotBlipBS for minigame object ", ObjectiveData.iEntityID)
										ENDIF	
										
										IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[ObjectiveData.iEntityID].iObjectBitSetTwo, cibsOBJ2_ClearUpMissionCriticalVeh)
											PRINTLN("[JS] - PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - cibsOBJ2_ClearUpMissionCriticalVeh set, clearing veh ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[ObjectiveData.iEntityID].iAttachParent, " mission critical")
											CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[MC_playerBD[ObjectiveData.iparticipant].iteam], g_FMMC_STRUCT_ENTITIES.sPlacedObject[ObjectiveData.iEntityID].iAttachParent)
										ENDIF	
										
										IF IS_THIS_ROCKSTAR_MISSION_SVM_PHANTOM2(g_FMMC_STRUCT.iRootContentIDHash)
											PRINTLN("[KH][COKEGRAB] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Adding coke grab trolley complete")
											g_TransitionSessionNonResetVars.iNumCokeGrabsCompleted++
											INT iNumTrolleysFirstCheckpoint
											iNumTrolleysFirstCheckpoint = 2
											INT iNumTrolleysSecondCheckpoint
											iNumTrolleysSecondCheckpoint = 1
											IF GET_NUMBER_OF_ACTIVE_PARTICIPANTS() = 3
												PRINTLN("[KH][COKEGRAB] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - 3 players reducing trolleys grabbed")
												iNumTrolleysFirstCheckpoint--
											ENDIF
											IF g_TransitionSessionNonResetVars.iLocalPlayerCheckpoint1CashGrabbed = 0
											AND g_TransitionSessionNonResetVars.iNumCokeGrabsCompleted = iNumTrolleysFirstCheckpoint
												PRINTLN("[KH][COKEGRAB] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Setting local player cash and total at checkpoint 1")
												g_TransitionSessionNonResetVars.iLocalPlayerCheckpoint1CashGrabbed = g_TransitionSessionNonResetVars.iLocalPlayerCashGrabbed
												g_TransitionSessionNonResetVars.iCheckpoint1CashGrabbed = MC_ServerBD.iCashGrabTotalTake
											ELIF g_TransitionSessionNonResetVars.iLocalPlayerCheckpoint2CashGrabbed = 0 
											AND g_TransitionSessionNonResetVars.iNumCokeGrabsCompleted = iNumTrolleysFirstCheckpoint + iNumTrolleysSecondCheckpoint
												PRINTLN("[KH][COKEGRAB] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Setting local player cash and total at checkpoint 2")
												g_TransitionSessionNonResetVars.iLocalPlayerCheckpoint2CashGrabbed = g_TransitionSessionNonResetVars.iLocalPlayerCashGrabbed
												g_TransitionSessionNonResetVars.iCheckpoint2CashGrabbed = MC_ServerBD.iCashGrabTotalTake
											ELIF g_TransitionSessionNonResetVars.iNumCokeGrabsCompleted > 2
												PRINTLN("[KH][COKEGRAB] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Player completed a coke grab - num complete: ", g_TransitionSessionNonResetVars.iNumCokeGrabsCompleted)
											ENDIF
										ENDIF
										
										IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[ObjectiveData.iEntityID ].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_VAULT_DRILLING)
											SET_CUTSCENE_PRIMARY_PLAYER(ObjectiveData.iparticipant, MC_playerBD[ObjectiveData.iparticipant].iteam)
										ENDIF
										
										PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iEntityID: ",ObjectiveData.iEntityID) 
										PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - server processing JOB_COMP_HACK_OBJ from part: ",ObjectiveData.iparticipant)
									ENDIF
									
								#IF IS_DEBUG_BUILD	
								ELSE
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - priorities or revalidates don't match up!")
								
								#ENDIF
								
								ENDIF
								
							ENDIF
							
						BREAK // End of JOB_COMP_HACK_OBJ
						
						
						CASE JOB_COMP_DELIVER_PED
							
							PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam])
							
							IF ObjectiveData.iRevalidates = MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam]
								
								FOR irepeat = 0 TO (FMMC_MAX_PEDS-1)
									IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[irepeat])
										//IF IS_BIT_SET(MC_playerBD[ObjectiveData.iparticipant].iPedFollowingBitset,irepeat)
										IF IS_BIT_SET(ObjectiveData.iBitsetArrayPassed[GET_LONG_BITSET_INDEX(irepeat)], GET_LONG_BITSET_BIT(irepeat))
											
											
											IF ObjectiveData.iPriority = MC_serverBD_4.iPedPriority[irepeat][MC_playerBD[ObjectiveData.iparticipant].iteam]
											AND MC_serverBD_4.iPedRule[irepeat][MC_playerBD[ObjectiveData.iparticipant].iteam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
												
												IF SHOULD_PED_CLEAN_UP_ON_DELIVERY(irepeat,MC_playerBD[ObjectiveData.iparticipant].iteam)
													PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - JOB_COMP_DELIVER_PED, cleaning up ped ",irepeat," on delivery")
													CLEAR_BIT(MC_serverBD.iPedCleanup_NeedOwnershipBS[GET_LONG_BITSET_INDEX(irepeat)], GET_LONG_BITSET_BIT(irepeat))
													CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niPed[irepeat])
												ELSE
													SET_PED_STATE(irepeat,ciTASK_CHOOSE_NEW_TASK)
												ENDIF
												// Only remove this ped's blip if they're no longer needed
												IF NOT SHOULD_PED_HAVE_BLIP( irepeat, iBlipColour, bCustomBlip )
													IF DOES_BLIP_EXIST( biPedBlip[irepeat] )
														PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Removing a blip on delivery ped  ", irepeat )
														REMOVE_BLIP( biPedBlip[irepeat] )
													ENDIF
												ENDIF
												
												IF MC_serverBD_2.iCurrentPedRespawnLives[irepeat] <= 0
													IF SHOULD_PED_CLEAN_UP_ON_DELIVERY(irepeat,MC_playerBD[ObjectiveData.iparticipant].iteam)
														FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
															IF iteam = MC_playerBD[ObjectiveData.iparticipant].iteam
																IF IS_BIT_SET(MC_serverBD.iPedteamFailBitset[irepeat],iteam)
																	CLEAR_BIT(MC_serverBD.iPedteamFailBitset[irepeat],iteam)
																	CLEAR_BIT(MC_serverBD.iMissionCriticalPed[iTeam][GET_LONG_BITSET_INDEX(irepeat)], GET_LONG_BITSET_BIT(irepeat))
																	bwasobjective[iTeam] = TRUE
																ENDIF
															ELSE
																IF DOES_TEAM_LIKE_TEAM(iteam,MC_playerBD[ObjectiveData.iparticipant].iteam)
																	IF IS_BIT_SET(MC_serverBD.iPedteamFailBitset[irepeat],iteam)
																		CLEAR_BIT(MC_serverBD.iPedteamFailBitset[irepeat],iteam)
																		CLEAR_BIT(MC_serverBD.iMissionCriticalPed[iTeam][GET_LONG_BITSET_INDEX(irepeat)], GET_LONG_BITSET_BIT(irepeat))
																		bwasobjective[iTeam] =TRUE
																	ENDIF	
																ENDIF
															ENDIF
														ENDFOR
													ENDIF
													FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
														
														ipriority[iteam] = MC_serverBD_4.iPedPriority[irepeat][iteam]
														
														IF iteam != MC_playerBD[ObjectiveData.iparticipant].iteam
															
															IF SHOULD_PED_CLEAN_UP_ON_DELIVERY(irepeat,MC_playerBD[ObjectiveData.iparticipant].iteam)
																IF IS_BIT_SET(MC_serverBD.iPedteamFailBitset[irepeat],iteam)
																	IF ipriority[iteam] < FMMC_MAX_RULES
																	OR IS_BIT_SET(MC_serverBD.iMissionCriticalPed[iTeam][GET_LONG_BITSET_INDEX(irepeat)], GET_LONG_BITSET_BIT(irepeat))
																		IF ((ipriority[iteam] < FMMC_MAX_RULES) AND (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iTeamFailBitset,ipriority[iteam])))
																		OR IS_BIT_SET(MC_serverBD.iMissionCriticalPed[iTeam][GET_LONG_BITSET_INDEX(irepeat)], GET_LONG_BITSET_BIT(irepeat))
																			bobjectivefails[iTeam] = TRUE																		
																			SET_TEAM_FAILED(iTeam,mfail_PED_DELIVERED)
																			PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MISSION FAILED FOR TEAM:",iteam," BECAUSE PED DELIVERED: ",irepeat)
																			MC_serverBD.iEntityCausingFail[iteam] = irepeat
																		ENDIF
																	ENDIF
																	CLEAR_BIT(MC_serverBD.iPedteamFailBitset[irepeat],iteam)
																	
																	bwasobjective[iTeam] = TRUE
																ENDIF
															ENDIF
															
															PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - team ", iTeam, " - ped's primary rule: ",MC_serverBD_4.iPedPriority[irepeat][iTeam])
															PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - team ", iTeam, " - MC_serverBD_4.iCurrentHighestPriority[iTeam]: ",MC_serverBD_4.iCurrentHighestPriority[iTeam])
															PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ", ObjectiveData.iPriority)
															
															IF ipriority[iteam] = MC_serverBD_4.iCurrentHighestPriority[iTeam]
															AND ipriority[iteam] < FMMC_MAX_RULES
																IF DOES_TEAM_LIKE_TEAM(iteam,MC_playerBD[ObjectiveData.iparticipant].iteam)
																	IF MC_serverBD_4.iPedRule[irepeat][iteam] = FMMC_OBJECTIVE_LOGIC_PROTECT
																		INCREMENT_SERVER_TEAM_SCORE(iteam,ipriority[iTeam],GET_FMMC_POINTS_FOR_TEAM(iteam,ipriority[iTeam]))
																		PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Protect ped rule adding points = ", GET_FMMC_POINTS_FOR_TEAM(iteam,ipriority[iTeam]))
																		//BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM_PRO_PED,0,iteam,-1,NULL,ci_TARGET_PED,irepeat)
																	ENDIF
																	SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iteam],TRUE)
																	PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED,irepeat,iTeam,TRUE)													
																ELSE
																	SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iteam],FALSE)
																	PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED,irepeat,iTeam,FALSE)
																ENDIF
																MC_serverBD.iReasonForObjEnd[iteam] = OBJ_END_REASON_PED_DELIVERED
															ENDIF
														ELSE
															PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ipriority[iteam] team ", iTeam, " - priority: ",ipriority[iteam])
															PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ", ObjectiveData.iPriority)
															
															IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
																PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Setting Midpoint iObjectiveMidPointBitset for Team deliver ped ", MC_playerBD[ObjectiveData.iparticipant].iteam, " on Rule ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
																SET_BIT(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
																SET_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
															ENDIF
															MC_serverBD.iReasonForObjEnd[iteam] = OBJ_END_REASON_PED_DELIVERED
															SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iteam],TRUE)
															PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED,irepeat,iTeam,TRUE)
														ENDIF
													ENDFOR
												ENDIF
												
												SET_BIT(MC_serverBD.iPedDelivered[GET_LONG_BITSET_INDEX(irepeat)], GET_LONG_BITSET_BIT(irepeat))
												
											ENDIF
												
										ENDIF // END of IS_BIT_SET(ObjectiveData.iEntityID,GET_LONG_BITSET_BIT(irepeat))
									ENDIF // END of NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[irepeat])
								ENDFOR
								FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
									IF bwasobjective[iTeam]
										IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_PED,iteam,ipriority[iteam])
											SET_LOCAL_OBJECTIVE_END_MESSAGE(iteam,ipriority[iteam],MC_serverBD.iReasonForObjEnd[iteam],bobjectivefails[iTeam])
											BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iteam],iteam,ipriority[iteam],DID_TEAM_PASS_OBJECTIVE(iteam,ipriority[iteam]),bobjectivefails[iTeam])
										ENDIF
									ENDIF
								ENDFOR
								
								tempPart= INT_TO_PARTICIPANTINDEX(ObjectiveData.iparticipant)
								
								SET_CUTSCENE_PRIMARY_PLAYER(ObjectiveData.iparticipant, MC_playerBD[ObjectiveData.iparticipant].iteam)
								
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - server processing JOB_COMP_DELIVER_PED from part: ",ObjectiveData.iparticipant)
								
							ELSE
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - revalidates don't match up!")
							ENDIF
							
						BREAK // End of JOB_COMP_DELIVER_PED
						
						CASE JOB_COMP_DELIVER_VEH
							
							PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam])
							
							IF ObjectiveData.iRevalidates = MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam]
						
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - DELIVER_VEH FOR TEAM:", MC_playerBD[ObjectiveData.iparticipant].iteam)
								
								IF ObjectiveData.iEntityID != -1
									IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[ObjectiveData.iEntityID])
										
										PROCESS_DELIVER_VEH_STAT_TRACKING(ObjectiveData.iEntityID)						
										
										IF ObjectiveData.iPriority > -1 
										AND ObjectiveData.iPriority < FMMC_MAX_RULES
											IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[ObjectiveData.iParticipant].iteam].iRuleBitsetNine[ObjectiveData.iPriority], ciBS_RULE9_CLEANUP_UNDELIVERED_VEHICLES)
												IF IS_BIT_SET(MC_serverBD.iUndeliveredVehCleanupBS[MC_playerBD[ObjectiveData.iParticipant].iteam], ObjectiveData.iEntityID)
													PRINTLN("[JS] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Veh ",ObjectiveData.iEntityID," was delivered this wont be cleaned up")
													CLEAR_BIT(MC_serverBD.iUndeliveredVehCleanupBS[MC_playerBD[ObjectiveData.iParticipant].iteam], ObjectiveData.iEntityID)
												ENDIF
											ENDIF
										ENDIF
													
										IF ObjectiveData.iPriority = MC_serverBD_4.iVehPriority[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iParticipant].iteam]
										AND MC_serverBD_4.ivehRule[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER	
											//url:bugstar:3219654
											INCREMENT_SERVER_REMOTE_PLAYER_SCORE(ObjectiveData.Details.FromPlayerIndex, GET_FMMC_POINTS_FOR_TEAM(MC_playerBD[ObjectiveData.iparticipant].iteam, ObjectiveData.iPriority), MC_playerBD[ObjectiveData.iparticipant].iteam, ObjectiveData.iPriority)
											
											cutscene_vehicle[ivehNumber] = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[ObjectiveData.iEntityID])

											// #2225760 - Set decorator on vehicle to remember which rule it was delivered on.
											STRING strDecorName
											SWITCH MC_playerBD[ObjectiveData.iParticipant].iteam
												CASE 0	strDecorName = "MC_Team0_VehDeliveredRules"		BREAK
												CASE 1	strDecorName = "MC_Team1_VehDeliveredRules"		BREAK
												CASE 2	strDecorName = "MC_Team2_VehDeliveredRules"		BREAK
												CASE 3	strDecorName = "MC_Team3_VehDeliveredRules"		BREAK
											ENDSWITCH
											IF DECOR_IS_REGISTERED_AS_TYPE(strDecorName, DECOR_TYPE_INT)
												INT iDecorValue
												iDecorValue = 0
												IF DECOR_EXIST_ON(cutscene_vehicle[ivehNumber], strDecorName)
													iDecorValue = DECOR_GET_INT(cutscene_vehicle[ivehNumber], strDecorName)
												ENDIF
												SET_BIT(iDecorValue, ObjectiveData.iPriority)
												DECOR_SET_INT(cutscene_vehicle[ivehNumber], strDecorName, iDecorValue)
												CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Updated decorator [", strDecorName, "] to remember veh ", ivehNumber, " was delivered on rule ", ObjectiveData.iPriority, ".")
											ENDIF

											IF MC_serverBD.cutscene_vehicle_count < total_number_of_cutscene_vehicles
												ivehNumber = MC_serverBD.cutscene_vehicle_count
											ENDIF
											cutscene_vehicle[ivehNumber] = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[ObjectiveData.iEntityID])
											
											BROADCAST_FMMC_DELIVERED_CUTSCENE_VEH(ObjectiveData.iEntityID,ivehNumber)

											IF SHOULD_VEH_CLEAN_UP_ON_DELIVERY(ObjectiveData.iEntityID,MC_playerBD[ObjectiveData.iparticipant].iteam)
												PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - JOB_COMP_DELIVER_VEH, cleaning up vehicle ",ObjectiveData.iEntityID," on delivery")
												CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[ObjectiveData.iEntityID])
											ENDIF
											
											IF NOT IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_SHOULD_PLAY_RUNNING_BACK_REMIX_SCORED)
											AND GET_PLAYER_RESPAWN_BACK_AT_START_STATE(iLocalPart) = PRBSS_PlayingNormally
												PRINTLN("[LM][PROCESS_RUNNING_BACK_SHARDS] - SBBOOL7_SHOULD_PLAY_RUNNING_BACK_REMIX_SCORED - Setting bit.")
												SET_BIT(MC_ServerBD.iServerBitSet7, SBBOOL7_SHOULD_PLAY_RUNNING_BACK_REMIX_SCORED)
											ENDIF
											
											IF GET_VEHICLE_RESPAWNS(ObjectiveData.iEntityID) <=0	
											OR ((ObjectiveData.iPriority > -1 AND ObjectiveData.iPriority < FMMC_MAX_RULES) AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[ObjectiveData.iparticipant].iteam].iRuleBitsetNine[ObjectiveData.iPriority], ciBS_RULE9_PROGRESS_VEHICLES_WITH_RESPAWNS))
												IF SHOULD_VEH_CLEAN_UP_ON_DELIVERY(ObjectiveData.iEntityID,MC_playerBD[ObjectiveData.iparticipant].iteam)
													FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
														IF iteam = MC_playerBD[ObjectiveData.iparticipant].iteam
															IF IS_BIT_SET(MC_serverBD.iVehteamFailBitset[ObjectiveData.iEntityID],iteam)
																CLEAR_BIT(MC_serverBD.iVehteamFailBitset[ObjectiveData.iEntityID],iteam)
																CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iTeam], ObjectiveData.iEntityID)
																bwasobjective[iTeam] =TRUE
															ENDIF
														ELSE
															IF DOES_TEAM_LIKE_TEAM(iteam,MC_playerBD[ObjectiveData.iparticipant].iteam)
																IF IS_BIT_SET(MC_serverBD.iVehteamFailBitset[ObjectiveData.iEntityID],iteam)
																	CLEAR_BIT(MC_serverBD.iVehteamFailBitset[ObjectiveData.iEntityID],iteam)
																	CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iTeam], ObjectiveData.iEntityID)
																	bwasobjective[iTeam] =TRUE
																ENDIF
	//															IF  MC_serverBD_4.ivehRule[ObjectiveData.iEntityID][iteam] = FMMC_OBJECTIVE_LOGIC_PROTECT
	//																INVALIDATE_UP_TO_AND_INCLUDING_OBJECTIVE(iteam,ipriority[iteam],TRUE,TRUE)
	//																SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iteam],TRUE)
	//																PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - HOST SETTING FRIENDLY DELIVERY PROTECT PASS FOR RULE: ",ipriority[iteam]," TEAM ",iteam)
	//
	//																SET_LOCAL_OBJECTIVE_END_MESSAGE(iteam,iPriority[iteam],MC_serverBD.iReasonForObjEnd[iTeam])
	//																BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iteam],iteam,iPriority[iteam],DID_TEAM_PASS_OBJECTIVE(iteam,iPriority[iteam]))
	//																
	//																RECALCULATE_OBJECTIVE_LOGIC(iteam)
	//															ENDIF
															ENDIF
														ENDIF
													ENDFOR
												ENDIF
												FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
												
													ipriority[iteam] = MC_serverBD_4.iVehPriority[ObjectiveData.iEntityID][iteam]
													
													IF iteam != MC_playerBD[ObjectiveData.iparticipant].iteam
														
														IF ipriority[iteam] < FMMC_MAX_RULES
														AND NOT IS_BIT_SET(MC_serverBD_1.iObjectiveProgressionHeldUpBitset[iteam], ipriority[iteam])
														//IF MC_serverBD_1.iPlayerForHackingMG = -1
														//OR NOT ( MC_playerBD[MC_serverBD_1.iPlayerForHackingMG].iteam = iteam
														//		AND IS_BIT_SET(MC_serverBD_1.iServerHackMGBitset, SBBOOL_HACK_HOLD_UP_PROGRESSION)
															
															IF SHOULD_DELIVER_VEH_BE_PROGRESSED_BY_LIKED_TEAM(ObjectiveData.iEntityID,iTeam,ipriority[iteam],MC_playerBD[ObjectiveData.iparticipant].iteam,MC_serverBD_4.iVehPriority[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam])
																
																IF SHOULD_VEH_CLEAN_UP_ON_DELIVERY(ObjectiveData.iEntityID,MC_playerBD[ObjectiveData.iparticipant].iteam)
																	IF IS_BIT_SET(MC_serverBD.iVehteamFailBitset[ObjectiveData.iEntityID],iteam)
																		IF ipriority[iteam] < FMMC_MAX_RULES
																		OR IS_BIT_SET(MC_serverBD.iMissionCriticalVeh[iTeam], ObjectiveData.iEntityID)
																			IF SHOULD_VEH_CAUSE_FAIL(ObjectiveData.iEntityID, iTeam, iPriority[iTeam])
																				bobjectivefails[iTeam] = TRUE																		
																				SET_TEAM_FAILED(iTeam,mFail_VEH_DELIVERED)
																				MC_serverBD.iEntityCausingFail[iteam] = ObjectiveData.iEntityID
																				PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MISSION FAILED FOR TEAM:",iteam," BECAUSE VEH DELIVERED: ",ObjectiveData.iEntityID)
																			ENDIF
																		ENDIF
																		bwasobjective[iTeam] =TRUE
																		CLEAR_BIT(MC_serverBD.iVehteamFailBitset[ObjectiveData.iEntityID],iteam)
																	ENDIF
																ENDIF
																
																PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - team ", iTeam, " - veh's primary rule: ",MC_serverBD_4.iVehPriority[ObjectiveData.iEntityID][iTeam])
																PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - team ", iTeam, " - MC_serverBD_4.iCurrentHighestPriority[iTeam]: ",MC_serverBD_4.iCurrentHighestPriority[iTeam])
																PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ", ObjectiveData.iPriority)
																
																IF ipriority[iTeam] = MC_serverBD_4.iCurrentHighestPriority[iTeam]
																AND ipriority[iteam] < FMMC_MAX_RULES
																	IF DOES_TEAM_LIKE_TEAM(iteam,MC_playerBD[ObjectiveData.iparticipant].iteam)
																		IF MC_serverBD_4.ivehRule[ObjectiveData.iEntityID][iteam] = FMMC_OBJECTIVE_LOGIC_PROTECT
																			INCREMENT_SERVER_TEAM_SCORE(iteam,ipriority[iTeam],GET_FMMC_POINTS_FOR_TEAM(iteam,ipriority[iTeam]))
																			PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Protect veh rule adding points = ", GET_FMMC_POINTS_FOR_TEAM(iteam,ipriority[iTeam]))
																			//BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM_PRO_VEH,0,iteam,-1,NULL,ci_TARGET_VEHICLE,ObjectiveData.iEntityID)
																		ENDIF
																		SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iteam],TRUE)
																		PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE,ObjectiveData.iEntityID,iTeam,TRUE)												
																	ELSE
																		SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iteam],FALSE)
																		PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE,ObjectiveData.iEntityID,iTeam,FALSE)
																	ENDIF
																	MC_serverBD.iReasonForObjEnd[iteam] = OBJ_END_REASON_VEH_DELIVERED
																ENDIF
															ELSE
																PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Team ", iTeam, " DELIVER_VEHICLE not progressed as different coords or radius")
															ENDIF
														ELSE
															PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Team ", iTeam, " progression is held up by hack minigame blocking objective progression")
														ENDIF
													ELSE
													
														PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ipriority[iteam] team ", iTeam, " - priority: ",ipriority[iteam])
														PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ", ObjectiveData.iPriority)
														
														IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
															PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Setting Midpoint iObjectiveMidPointBitset for Team deliver veh ", MC_playerBD[ObjectiveData.iparticipant].iteam, " on Rule ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
															SET_BIT(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
															SET_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
														ENDIF
														
														SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iteam],TRUE)
														PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE,ObjectiveData.iEntityID,iTeam,TRUE)
														MC_serverBD.iReasonForObjEnd[iteam] = OBJ_END_REASON_VEH_DELIVERED
													ENDIF
													
												ENDFOR
											ENDIF
											IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[ObjectiveData.iParticipant].iteam].iDropOffStopTimer[ObjectiveData.iPriority] = -1
												IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[ObjectiveData.iEntityID])
													VEHICLE_INDEX tempVeh
													
													tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[ObjectiveData.iEntityID])
													
													IF IS_PED_IN_VEHICLE(LocalPlayerPed, tempVeh)
														SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_PreventAutoShuffleToDriversSeat, TRUE)
														
														bStopDurationBlockSeatShuffle = TRUE
													ENDIF
												ENDIF
											ENDIF
											
											PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - vehicle delivered on host") 
											
											IF DOES_BLIP_EXIST(biVehBlip[ObjectiveData.iEntityID])
											AND NOT SHOULD_VEHICLE_HAVE_CUSTOM_BLIP(ObjectiveData.iEntityID)
												REMOVE_VEHICLE_BLIP(ObjectiveData.iEntityID)
												PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - JOB_COMP_DELIVER_VEH objective complete removing blip for veh HOST: ",ObjectiveData.iEntityID) 
											ENDIF
											
											IF iDeliveryVehForcingOut = ObjectiveData.iEntityID
												iDeliveryVehForcingOut = -1
												iPartSending_DeliveryVehForcingOut = -1
												RESET_NET_TIMER(tdForceEveryoneOutBackupTimer)
											ENDIF
										ENDIF
									ENDIF
									
									FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
										IF bwasobjective[iTeam]
											IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_VEHICLE,iteam,ipriority[iteam])
												SET_LOCAL_OBJECTIVE_END_MESSAGE(iteam,ipriority[iteam],MC_serverBD.iReasonForObjEnd[iteam],bobjectivefails[iTeam])
												BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iteam],iteam,ipriority[iteam],DID_TEAM_PASS_OBJECTIVE(iteam,ipriority[iteam]),bobjectivefails[iTeam])
											ENDIF
										ENDIF
									ENDFOR
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iEntityID: ",ObjectiveData.iEntityID) 
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - server processing JOB_COMP_DELIVER_VEH from part: ",ObjectiveData.iparticipant)
									
									tempPart= INT_TO_PARTICIPANTINDEX(ObjectiveData.iparticipant)
									
									SET_CUTSCENE_PRIMARY_PLAYER(ObjectiveData.iparticipant, MC_playerBD[ObjectiveData.iparticipant].iteam)
								ENDIF
							ELSE
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - revalidates don't match up!")
							ENDIF
							
						BREAK // End of JOB_COMP_DELIVER_VEH
						
						CASE JOB_COMP_DELIVER_OBJ

							PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam])
							
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciSHOW_LAST_DELIVERER_SHARD)
								piLastDeliverer = ObjectiveData.Details.FromPlayerIndex
							ENDIF	
							
							IF ObjectiveData.iRevalidates = MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam]
								
								FOR irepeat = 0 TO (FMMC_MAX_NUM_OBJECTS-1)
									IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[irepeat])
										//IF MC_serverBD.iObjCarrier[irepeat]  = ObjectiveData.iparticipant
										IF IS_BIT_SET(ObjectiveData.iEntityID,irepeat)
											
											IF ObjectiveData.iPriority = MC_serverBD_4.iObjPriority[irepeat][MC_playerBD[ObjectiveData.iparticipant].iteam]
											AND MC_serverBD_4.iObjRule[irepeat][MC_playerBD[ObjectiveData.iparticipant].iteam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
												IF MC_serverBD_4.iObjPriority[irepeat][MC_playerBD[ObjectiveData.iparticipant].iteam] < FMMC_MAX_RULES
													
													IF NOT IS_BIT_SET(MC_serverBD_1.iObjDeliveredOnceBS, irepeat)
														PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - JOB_COMP_DELIVER_OBJ, setting MC_serverBD_1.iObjDeliveredOnceBS for object ",irepeat)
														SET_BIT(MC_serverBD_1.iObjDeliveredOnceBS, irepeat)
													ENDIF
													
													MC_serverBD.iNumObjDelAtPriority[MC_playerBD[ObjectiveData.iparticipant].iteam][MC_serverBD_4.iObjPriority[irepeat][MC_playerBD[ObjectiveData.iparticipant].iteam]]++
													MC_serverBD.iObjDelTeam[irepeat]=MC_playerBD[ObjectiveData.iparticipant].iteam
													REMOVE_OBJECT_BLIP(irepeat)
													IF SHOULD_OBJ_CLEAN_UP_ON_DELIVERY(irepeat,MC_playerBD[ObjectiveData.iparticipant].iteam)
														PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - JOB_COMP_DELIVER_OBJ, cleaning up object ",irepeat," on delivery")
														CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niObject[irepeat])
													ENDIF
													IF MC_serverBD_2.iCurrentObjRespawnLives[irepeat] <= 0
														IF SHOULD_OBJ_CLEAN_UP_ON_DELIVERY(irepeat,MC_playerBD[ObjectiveData.iparticipant].iteam)
															FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
																IF iteam=MC_playerBD[ObjectiveData.iparticipant].iteam
																	IF IS_BIT_SET(MC_serverBD.iObjteamFailBitset[irepeat],iteam)
																		CLEAR_BIT(MC_serverBD.iObjteamFailBitset[irepeat],iteam)
																		CLEAR_BIT(MC_serverBD.iMissionCriticalObj[iTeam], irepeat)
																		bwasobjective[iTeam] = TRUE
																	ENDIF
																ELSE
																	IF DOES_TEAM_LIKE_TEAM(iteam,MC_playerBD[ObjectiveData.iparticipant].iteam)
																		IF IS_BIT_SET(MC_serverBD.iObjteamFailBitset[irepeat],iteam)
																			CLEAR_BIT(MC_serverBD.iObjteamFailBitset[irepeat],iteam)
																			CLEAR_BIT(MC_serverBD.iMissionCriticalObj[iTeam], irepeat)
																			bwasobjective[iTeam] =TRUE
																		ENDIF	
																	ENDIF
																ENDIF
															ENDFOR
														ENDIF
														FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
															
															ipriority[iteam] = MC_serverBD_4.iObjPriority[irepeat][iteam]
															
															IF iteam!=MC_playerBD[ObjectiveData.iparticipant].iteam
																
																IF SHOULD_OBJ_CLEAN_UP_ON_DELIVERY(irepeat,MC_playerBD[ObjectiveData.iparticipant].iteam)
																	IF IS_BIT_SET(MC_serverBD.iObjteamFailBitset[irepeat],iteam)
																		IF ipriority[iteam] < FMMC_MAX_RULES
																		OR IS_BIT_SET(MC_serverBD.iMissionCriticalObj[iTeam], irepeat)
																			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iTeamFailBitset,ipriority[iteam])
																			OR IS_BIT_SET(MC_serverBD.iMissionCriticalObj[iTeam], irepeat)
																				bobjectivefails[iTeam] = TRUE
																				SET_TEAM_FAILED(iTeam,mFail_OBJ_DELIVERED)
																				MC_serverBD.iEntityCausingFail[iteam] = irepeat
																				PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MISSION FAILED FOR TEAM:",iteam," BECAUSE OBJ DELIVERED: ",irepeat)
																			ENDIF
																		ENDIF
																		bwasobjective[iTeam] = TRUE
																		CLEAR_BIT(MC_serverBD.iObjteamFailBitset[irepeat],iteam)
																	ENDIF
																ENDIF
																
																PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - team ", iTeam, " - obj's primary rule: ",MC_serverBD_4.iObjPriority[irepeat][iTeam])
																PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - team ", iTeam, " - MC_serverBD_4.iCurrentHighestPriority[iTeam]: ",MC_serverBD_4.iCurrentHighestPriority[iTeam])
																PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ", ObjectiveData.iPriority)
																
																IF (ipriority[iTeam] = MC_serverBD_4.iCurrentHighestPriority[iTeam]
																	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[irepeat].iObjectBitSet, cibsOBJ_InvalidateAllTeams))
																AND ipriority[iteam] < FMMC_MAX_RULES
																	IF DOES_TEAM_LIKE_TEAM(iteam,MC_playerBD[ObjectiveData.iparticipant].iteam)
																		IF MC_serverBD_4.iObjRule[irepeat][iteam] = FMMC_OBJECTIVE_LOGIC_PROTECT
																			PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Protect Obj rule adding points = ", GET_FMMC_POINTS_FOR_TEAM(iteam,ipriority[iTeam]))
																			INCREMENT_SERVER_TEAM_SCORE(iteam,ipriority[iTeam],GET_FMMC_POINTS_FOR_TEAM(iteam,ipriority[iTeam]))
																			//BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM_PRO_OBJ,0,iteam,-1,NULL,ci_TARGET_OBJECT,irepeat)
																		ENDIF
																		SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iteam], TRUE)
																		PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,irepeat,iTeam,TRUE)	
																	ELSE
																		SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iteam], FALSE)
																		PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,irepeat,iTeam,FALSE)
																	ENDIF
																	MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_OBJ_DELIVERED
																ENDIF
															ELSE
																PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ipriority[iteam] team ", iTeam, " - priority: ",ipriority[iteam])
																PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ", ObjectiveData.iPriority)
																
																IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
																	PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Setting Midpoint iObjectiveMidPointBitset for Team deliver obj ", MC_playerBD[ObjectiveData.iparticipant].iteam, " on Rule ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
																	SET_BIT(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
																	SET_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
																ENDIF
																
																SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iteam], TRUE)
																PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,irepeat,iTeam,TRUE)	
																MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_OBJ_DELIVERED
															ENDIF
															
														ENDFOR

													ENDIF
													
												ENDIF
												IF ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] < FMMC_MAX_RULES
													IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[ObjectiveData.iparticipant].iteam].iRuleBitSetSix[ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]], ciBS_RULE6_GET_DELIVER_DONT_DROP_ON_DELIVER)
														MC_serverBD.iObjCarrier[irepeat] = -1
													ENDIF
												ENDIF
											
											ENDIF
										ENDIF
									ENDIF
								ENDFOR
								FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
									IF bwasobjective[iTeam]
										IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_OBJECT,iteam,ipriority[iteam])
											SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam,ipriority[iteam],MC_serverBD.iReasonForObjEnd[iteam],bobjectivefails[iTeam])
											BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iteam],iteam,ipriority[iteam],DID_TEAM_PASS_OBJECTIVE(iteam,ipriority[iteam]),bobjectivefails[iTeam])
										ENDIF
									ENDIF
								ENDFOR
								
								SET_CUTSCENE_PRIMARY_PLAYER(ObjectiveData.iparticipant, MC_playerBD[ObjectiveData.iparticipant].iteam)
								
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - server processing JOB_COMP_DELIVER_OBJ from part: ",ObjectiveData.iparticipant) 
								
							ELSE
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - revalidates don't match up!")
							ENDIF
						BREAK // End of JOB_COMP_DELIVER_OBJ
						
					ENDSWITCH
					
					FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
						IF SHOULD_PROGRESS_OBJECTIVE_FOR_TEAM(iteam)
							PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - SHOULD_PROGRESS_OBJECTIVE_FOR_TEAM returned true for team ",iteam)
							
							INT iObjectiveToProgressTo
							BOOL bPreviousObjective = FALSE
							
							IF MC_serverBD.iNextObjective[iteam] >= 0
								IF MC_serverBD.iNextObjective[iteam] < MC_serverBD_4.iCurrentHighestPriority[iteam]
									//We're going back in time!
									bPreviousObjective = TRUE
									iObjectiveToProgressTo = MC_serverBD.iNextObjective[iteam]
								ELIF MC_serverBD.iNextObjective[iteam] > 0
									iObjectiveToProgressTo = (MC_serverBD.iNextObjective[iteam]-1)
								ELSE
									iObjectiveToProgressTo = MC_serverBD_4.iCurrentHighestPriority[iteam]
								ENDIF
							ELSE
								iObjectiveToProgressTo = MC_serverBD_4.iCurrentHighestPriority[iteam]
							ENDIF
							INVALIDATE_UP_TO_AND_INCLUDING_OBJECTIVE(iteam, iObjectiveToProgressTo, TRUE, DEFAULT, bPreviousObjective)
							RECALCULATE_OBJECTIVE_LOGIC(iteam)
							SET_PROGRESS_OBJECTIVE_FOR_TEAM(iteam,FALSE)
						ELSE
							PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - SHOULD_PROGRESS_OBJECTIVE_FOR_TEAM returned false for team ",iteam)
							RECALCULATE_OBJECTIVE_LOGIC(iTeam)
						ENDIF
					ENDFOR
					
					SET_BIT(MC_serverBD.iServerBitSet,SBBOOL_SKIP_TEAM_0_UPDATE)
					SET_BIT(MC_serverBD.iServerBitSet,SBBOOL_SKIP_TEAM_1_UPDATE)
					SET_BIT(MC_serverBD.iServerBitSet,SBBOOL_SKIP_TEAM_2_UPDATE)
					SET_BIT(MC_serverBD.iServerBitSet,SBBOOL_SKIP_TEAM_3_UPDATE)
					
					SET_BIT(MC_serverBD.iProcessJobCompBitset,ObjectiveData.iparticipant)
					PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_serverBD.iProcessJobCompBitset set for part: ",ObjectiveData.iparticipant)
					
				ELSE
					PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - part ",ObjectiveData.iparticipant," already has iProcessJobCompBitset set")
				ENDIF
			ENDIF // End of IF is this person in control of this script
		ENDIF
	ENDIF
				
ENDPROC



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: OTHER NETWORK EVENTS !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************



PROC PROCESS_FMMC_OBJECTIVE_END_MESSAGE_EVENT(int icount)

	SCRIPT_EVENT_DATA_FMMC_OBJECTIVE_END_MESSAGE ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))

		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_OBJECTIVE_END_MESSAGE
			
			PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_END_MESSAGE_EVENT - ObjectiveData.iObjectiveID = ",ObjectiveData.iObjectiveID)
			PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_END_MESSAGE_EVENT - ObjectiveData.iTeam = ",ObjectiveData.iTeam) 
			PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_END_MESSAGE_EVENT - ObjectiveData.iPriority = ",ObjectiveData.iPriority)
			PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_END_MESSAGE_EVENT - ObjectiveData.bpass = ",ObjectiveData.bpass)
			PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_END_MESSAGE_EVENT - ObjectiveData.bFailMission = ",ObjectiveData.bFailMission)
			 
			IF ObjectiveData.iObjectiveID !=-1
				
				IF ObjectiveData.bFailMission
				AND (ireasonObjEnd[ObjectiveData.iTeam] = -1)
					iObjEndPriority[ObjectiveData.iTeam] = ObjectiveData.iPriority
					ireasonObjEnd[ObjectiveData.iTeam] = ObjectiveData.iObjectiveID
					//iObjEndMostRecentTeam = ObjectiveData.iTeam
					
					PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_END_MESSAGE_EVENT - Setting fail reason for team ",ObjectiveData.iTeam,", priority",iObjEndPriority[ObjectiveData.iTeam],", reason ",ireasonObjEnd[ObjectiveData.iTeam])
					
					//This is commented out as LBOOL3_PASSED_OBJECTIVE didn't ever seem to be used (it was only ever set or cleared, never checked), so removed it to make space in the bitset
					//IF (ObjectiveData.bpass)
					//	SET_BIT(iLocalBoolCheck3,LBOOL3_PASSED_OBJECTIVE)
					//ELSE
					//	CLEAR_BIT(iLocalBoolCheck3,LBOOL3_PASSED_OBJECTIVE)
					//ENDIF
				ENDIF
				
				//iObjEndPriorityBackup = ObjectiveData.iPriority
				//ireasonObjEndBackup = ObjectiveData.iObjectiveID
				//iObjEndTeamBackup = ObjectiveData.iTeam
				
			ENDIF
			
		ENDIF
	ENDIF
ENDPROC

//[KH] Process a player being knocked out and display a shard to show who was knocked out
PROC PROCESS_FMMC_PLAYER_KNOCKED_OUT_EVENT(int iCount)
	
	SCRIPT_EVENT_DATA_FMMC_PLAYER_KNOCKED_OUT ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))

		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PLAYER_KNOCKED_OUT

			//[KH] Don't display it if the local player sent it as they will have already done this locally
			IF ObjectiveData.Details.FromPlayerIndex != PLAYER_ID()
				HUD_COLOURS hcPlayerTeamColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(ObjectiveData.iTeam, ObjectiveData.Details.FromPlayerIndex)
				PRINT_TICKER_WITH_PLAYER_NAME_WITH_HUD_COLOUR("SUMO_TICK", ObjectiveData.Details.FromPlayerIndex, hcPlayerTeamColour)
			ENDIF
	
		ENDIF	
	ENDIF
ENDPROC

//[KH] Process a player being knocked out and display a ticker to show which team the player switched to
PROC PROCESS_FMMC_PLAYER_SWITCHED_TEAMS_EVENT(int iCount)
		
	SCRIPT_EVENT_DATA_FMMC_PLAYER_SWITCHED_TEAMS ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))

		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PLAYER_SWITCHED_TEAMS
			PLAYER_INDEX piPlayer
			
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PLAYER_SWITCHED_TEAMS_EVENT - ObjectiveData.Details.FromPlayerIndex = ", GET_PLAYER_NAME(ObjectiveData.Details.FromPlayerIndex))
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PLAYER_SWITCHED_TEAMS_EVENT - From team ",ObjectiveData.iTeam, ", to team ", ObjectiveData.iNewTeam)
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PLAYER_SWITCHED_TEAMS_EVENT - LocalPlayer = ", GET_PLAYER_NAME(LocalPlayer))
								
			// ????? Plug the issue from this end ?????
			// If the data points to the spectator, redirect it to the player we're spectating.
			//IF IS_PLAYER_SPECTATOR_ONLY(ObjectiveData.Details.FromPlayerIndex)
			//	ObjectiveData.Details.FromPlayerIndex = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartToUse))
			//ENDIF			
			
			// Inherit the spectated target's tickers.
			IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
				piPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartToUse))
				PRINTLN("[RCC MISSION][PROCESS_FMMC_PLAYER_SWITCHED_TEAMS_EVENT] - Switched Teams On This Event, I am a Spectator")
			ELSE
				piPlayer = LocalPlayer
				PRINTLN("[RCC MISSION][PROCESS_FMMC_PLAYER_SWITCHED_TEAMS_EVENT] - Switched Teams On This Event")
			ENDIF
			
			IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] > -1
			AND MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
				PROCESS_MY_PLAYER_BLIP_VISIBILITY(MC_playerBD[iPartToUse].iteam, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
			ENDIF
			
			TEXT_LABEL_63 tlNewTeamName = GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN(ObjectiveData.iNewTeam)	
			HUD_COLOURS hcPlayerTeamColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(ObjectiveData.iNewTeam, piPlayer)
			
			IF ObjectiveData.Details.FromPlayerIndex != LocalPlayer
				PRINT_TICKER_WITH_CUSTOM_STRING_AND_PLAYER_NAME_CUSTOM_HUD_COLOUR("TS_TICK", tlNewTeamName, ObjectiveData.Details.FromPlayerIndex, hcPlayerTeamColour)
				REFRESH_ALL_OVERHEAD_DISPLAY()
			ENDIF
			
			IF ObjectiveData.bShowShard
				IF ObjectiveData.Details.FromPlayerIndex = LocalPlayer
					SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_GENERIC_TEXT, "GG_S_LSL", tlNewTeamName, DEFAULT, DEFAULT, "GG_S_ST")
				ELSE
					SETUP_NEW_BIG_MESSAGE_WITH_PLAYER_AND_LITERAL_STRING(BIG_MESSAGE_GENERIC_TEXT, ObjectiveData.Details.FromPlayerIndex, "GG_S_RSL", tlNewTeamName, "GG_S_ST")
				ENDIF
			ENDIF
			
			IF bIsLocalPlayerHost
				SET_BIT(MC_serverBD.iServerBitSet, SBBOOL_SKIP_PARTICIPANT_UPDATE)
			ENDIF	
		ENDIF	
	ENDIF
ENDPROC

PROC PROCESS_FMMC_PLAYER_REQUEST_TEAM_SWITCH_EVENT(INT iCount)
	SCRIPT_EVENT_DATA_FMMC_PLAYER_TEAM_SWITCH_REQUEST ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))

		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PLAYER_TEAM_SWITCH_REQUEST
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciCLOBBER_PLAYER_ON_TEAM_SWAP)
				IF ObjectiveData.iEventSessionKey = MC_ServerBD.iSessionScriptEventKey
					IF bIsLocalPlayerHost
						
						PROCESS_PLAYER_TEAM_SWITCH(ObjectiveData.iVictimPart, ObjectiveData.piKiller, ObjectiveData.bSuicide, ObjectiveData.bNoVictim)
					ENDIF	
				ELSE
					PRINTLN("[JS][TeamSwaps] PROCESS_FMMC_PLAYER_REQUEST_TEAM_SWITCH_EVENT - Session Keys dont match")
				ENDIF
			ELSE
				PRINTLN("[JS][TeamSwaps] PROCESS_FMMC_PLAYER_REQUEST_TEAM_SWITCH_EVENT - Creator setting not on, this shouldn't be called")
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

PROC PROCESS_FMMC_SERVER_AUTHORISED_TEAM_SWAP_EVENT(INT iCount)
	SCRIPT_EVENT_DATA_SERVER_AUTHORISED_TEAM_SWAP ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_SERVER_AUTHORISED_TEAM_SWAP
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciCLOBBER_PLAYER_ON_TEAM_SWAP)
				IF ObjectiveData.iEventSessionKey = MC_ServerBD.iSessionScriptEventKey
					IF ObjectiveData.iKillerPart = iLocalPart
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciTEAM_SWITCH_SHARDS)
							PRINTLN("[RCC MISSION] Playing Winning_Team_Shard 1")
							PRINTLN("[RCC MISSION] Killer iPart: ", ObjectiveData.iKillerPart)
							
							STRING sWinText = "TS_WIN"
							IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
								sWinText= "JUGGS_WIN"
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciTRADING_PLACES_TIME_BAR_ENABLED) AND g_FMMC_STRUCT.iTradingPlacesTimeBarDuration != -1
									sWinText = "JUGGS_WIN_ALT"
								ENDIF
								PLAY_SOUND_FRONTEND(-1, "Become_JN", "DLC_IE_JN_Player_Sounds", FALSE)
								SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_GENERIC_TEXT, "", sWinText, "JUGGS_JUGG")
							ELSE
								sWinText= "TS_WIN"
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciTRADING_PLACES_TIME_BAR_ENABLED) AND g_FMMC_STRUCT.iTradingPlacesTimeBarDuration != -1
									sWinText = "TS_WIN_ALT"
								ENDIF
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_TRADING_PLACES_SOUNDS)
									PLAY_SOUND_FRONTEND(-1, "Become_JN", "DLC_BTL_TP_Remix_Juggernaut_Player_Sounds", FALSE)
								ELSE
									PLAY_SOUND_FRONTEND(-1, "Winning_Team_Shard", "DLC_Exec_TP_SoundSet", FALSE)
								ENDIF
								SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_GENERIC_TEXT, "", sWinText, "TGIG_WINNER")
							ENDIF
						ENDIF
						CHANGE_PLAYER_TEAM(ObjectiveData.iKillerNewTeam, TRUE)
						BROADCAST_FMMC_OBJECTIVE_PLAYER_SWITCHED_TEAMS(MC_playerBD[iLocalPart].iteam, ObjectiveData.iKillerNewTeam)
						
						//Trading Places Suicide Help Text
						IF ObjectiveData.bSuicide
							PRINTLN("[MMacK][TeamSwaps] CHANGE_PLAYER_TEAM - Suicide Swap iLocalPart = ", iLocalPart)
							SET_BIT(iTradingPlacesBitSet, ciTradingPlacesHelpSuicideTrigger)
						ENDIF
					ELIF ObjectiveData.iVictimPart = iLocalPart
						PRINTLN("[RCC MISSION] Victim iPart: ", ObjectiveData.iVictimPart)
						IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_SHOULD_SHOW_TEAM_SWITCH_SHARD)
							SET_BIT(iLocalBoolCheck16, LBOOL16_SHOULD_SHOW_TEAM_SWITCH_SHARD)
						ENDIF
						CHANGE_PLAYER_TEAM(ObjectiveData.iVictimNewTeam, TRUE)
						BROADCAST_FMMC_OBJECTIVE_PLAYER_SWITCHED_TEAMS(MC_playerBD[iLocalPart].iteam, ObjectiveData.iVictimNewTeam)
					ENDIF
								
					IF bIsLocalPlayerHost
						SET_BIT(MC_serverBD.iServerBitSet, SBBOOL_SKIP_PARTICIPANT_UPDATE)
					ENDIF
				ELSE
					PRINTLN("[JS][TeamSwaps] PROCESS_FMMC_SERVER_AUTHORISED_TEAM_SWAP_EVENT - Session Keys dont match")
				ENDIF
			ELSE
				PRINTLN("[JS][TeamSwaps] PROCESS_FMMC_SERVER_AUTHORISED_TEAM_SWAP_EVENT - Creator setting not on, this shouldn't be called")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_FMMC_CLIENT_TEAM_SWAP_COMPLETE(INT iCount)
	SCRIPT_EVENT_DATA_CLIENT_TEAM_SWAP_COMPLETE ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_CLIENT_TEAM_SWAP_COMPLETE
			IF bIsLocalPlayerHost
				CLEAR_BIT(MC_serverBD_4.iTeamSwapLockFlag, ObjectiveData.iPartToClear)
				MC_serverBD.iHostRefreshDpadValue++
				PRINTLN("[MMacK][TeamSwaps] iTeamSwapLockFlag = CLEAR 3", ObjectiveData.iPartToClear)
			ENDIF
		ENDIF
	ENDIF	
ENDPROC

//[AW] Process a player commiting suicide because of being out of bounds
PROC PROCESS_FMMC_PLAYER_COMMITED_SUICIDE_EVENT(int iCount)
	
	SCRIPT_EVENT_DATA_FMMC_PLAYER_COMMITED_SUICIDE ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))

		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PLAYER_COMMITED_SUICIDE

			//[KH] Don't display it if the local player sent it as they will have already done this locally
			IF ObjectiveData.Details.FromPlayerIndex != LocalPlayer
				PRINTLN("[PROCESS_FMMC_PLAYER_COMMITED_SUICIDE_EVENT][RH] localPlayer is: ", iLocalPart, " partToUse is ", iPartToUse)
				HUD_COLOURS hcPlayerTeamColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(ObjectiveData.iTeam, PlayerToUse)
				PRINT_TICKER_WITH_PLAYER_NAME_WITH_HUD_COLOUR("OOBSUI_TICK", ObjectiveData.Details.FromPlayerIndex, hcPlayerTeamColour)
			ENDIF
			
			INT iDeadPart = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(ObjectiveData.Details.FromPlayerIndex))
			PROCESS_CONDEMNED_KILL(iDeadPart, -1)
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_TEAM_TURNS)
				PRIVATE_PROCESS_ENTITY_SUICIDE_EVENT(ObjectiveData.Details.FromPlayerIndex, iDeadPart, -1)
			ENDIF
			IF bIsLocalPlayerHost
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciNEGATIVE_SUICIDE_SCORE)
					INT igamestage = GET_MC_CLIENT_MISSION_STAGE(iDeadPart)
					PRINTLN("[RCC MISSION] PROCESS_FMMC_PLAYER_COMMITED_SUICIDE_EVENT - GET_MC_CLIENT_MISSION_STAGE: ",igamestage) 
					PRINTLN("[RCC MISSION] PROCESS_FMMC_PLAYER_COMMITED_SUICIDE_EVENT - host subtracting team kills ") 
					IF igamestage = CLIENT_MISSION_STAGE_KILL_PLAYERS
						IF MC_serverBD.iTeamPlayerKills[ObjectiveData.iTeam][ObjectiveData.iTeam] > 0
							MC_serverBD.iTeamPlayerKills[ObjectiveData.iTeam][ObjectiveData.iTeam]--
							PRINTLN("[RCC MISSION] PROCESS_FMMC_PLAYER_COMMITED_SUICIDE_EVENT - MC_serverBD.iTeamPlayerKills decreased")
						ENDIF
					ELIF igamestage = CLIENT_MISSION_STAGE_KILL_TEAM0
					OR igamestage = CLIENT_MISSION_STAGE_KILL_TEAM1
					OR igamestage = CLIENT_MISSION_STAGE_KILL_TEAM2
					OR igamestage = CLIENT_MISSION_STAGE_KILL_TEAM3
						IF MC_serverBD.iTeamPlayerKills[ObjectiveData.iTeam][ObjectiveData.iTeam] > 0
							MC_serverBD.iTeamPlayerKills[ObjectiveData.iTeam][ObjectiveData.iTeam]--
							PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - MC_serverBD.iTeamPlayerKills of team ",ObjectiveData.iTeam," decreased ") 
						ENDIF
					ENDIF
				ENDIF
			
				INT iVictimRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iDeadPart].iTeam]
				IF iVictimRule < FMMC_MAX_RULES
					IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iDeadPart].iTeam].iDeathPenalty[iVictimRule] > 0
						INT iCurrentTimer
						INT iPenalty = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iDeadPart].iTeam].iDeathPenalty[iVictimRule] * 1000
						IF HAS_NET_TIMER_STARTED(MC_serverBD_3.tdObjectiveLimitTimer[MC_playerBD[iDeadPart].iTeam])
							iCurrentTimer = GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iDeadPart].iTeam].iObjectiveTimeLimitRule[iVictimRule]) - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdObjectiveLimitTimer[MC_playerBD[iDeadPart].iTeam]) - MC_serverBD_3.iTimerPenalty[MC_playerBD[iDeadPart].iTeam]
							IF (iCurrentTimer - iPenalty) > (g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iDeadPart].iTeam].iDeathPenaltyThreshold[iVictimRule] * 1000)
								PRINTLN("[JS] - PROCESS_FMMC_PLAYER_COMMITED_SUICIDE_EVENT - Decrementing team ", MC_playerBD[iDeadPart].iTeam,"'s timer due to player death")
								MC_serverBD_3.iTimerPenalty[MC_playerBD[iDeadPart].iTeam] += iPenalty
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

PROC PROCESS_FMMC_SUMO_PLAYER_KNOCKED_OUT_NOISE_EVENT(INT iCount)
	SCRIPT_EVENT_DATA_FMMC_PLAYER_KNOCKED_OUT ObjectiveData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PLAYER_KNOCKED_OUT_NOISE
			PRINTLN("[RCC MISSION] PROCESS_FMMC_SUMO_PLAYER_KNOCKED_OUT_NOISE_EVENT - Playing sumo vehicle destroyed sound.")
			PLAY_SOUND(-1, "Vehicle_Destroyed", "DLC_LOW2_Sumo_Soundset",FALSE) //B* 2666917	
		ENDIF	
	ENDIF
ENDPROC

PROC PROCESS_BMB_BOMB_PLACED_MESSAGE_EVENT(int iCount)

	SCRIPT_EVENT_DATA_BMB_BOMB_DETAILS ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))

		IF ObjectiveData.Details.Type = SCRIPT_EVENT_BMB_BOMB_PLACED
			
			PED_INDEX pedPlayer
			BOOL bLocalPlayer = FALSE
			pedPlayer = GET_PLAYER_PED(ObjectiveData.Details.FromPlayerIndex)
			
			IF IS_ENTITY_ALIVE(pedPlayer)
				IF ObjectiveData.Details.FromPlayerIndex = PLAYER_ID()
					bLocalPlayer = TRUE
					PLAY_SOUND_FROM_ENTITY(-1,  "Activate_From_Vehicle" , pedPlayer, sVVBombSoundSet) //"DLC_IE_VV_Bomb_Player_Sounds"
				ELSE
					bLocalPlayer  = FALSE
					PLAY_SOUND_FROM_ENTITY(-1,  "Activate_From_Vehicle" , pedPlayer, sVVBombSoundSet) // "DLC_IE_VV_Bomb_Team_Sounds" / "DLC_IE_VV_Bomb_Enemy_Sounds"
				ENDIF
			ENDIF
		
			PRINTLN( "[BMB-NET] PROCESS_BMB_BOMB_PLACED_MESSAGE_EVENT, Native index: ", NATIVE_TO_INT( ObjectiveData.bombID ) )
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciENABLE_BMB_MODE)
				BMB_SET_UP_ARMING_BOMB( ObjectiveData.bombID, ObjectiveData.radius,bLocalPlayer )
			ELSE
				SCRIPT_ASSERT( "Bomber Mode receiving PROCESS_BMB_BOMB_PLACED_MESSAGE_EVENT when the mode is not enabled." )
			ENDIF

		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_BMB_PICKUP_CREATED_MESSAGE_EVENT(int iCount)

	SCRIPT_EVENT_DATA_BMB_PICKUP_DETAILS ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))

		IF ObjectiveData.Details.Type = SCRIPT_EVENT_BMB_PICKUP_CREATED
					
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciENABLE_BMB_MODE)
				BMB_CREATE_PICKUP( ObjectiveData.chance1, 
								   ObjectiveData.chance2, 
								   ObjectiveData.propPos, 
								   ObjectiveData.pickupType )
			ELSE
				SCRIPT_ASSERT( "Bomber Mode receiving PROCESS_BMB_PICKUP_CREATED_MESSAGE_EVENT when the mode is not enabled." )
			ENDIF

		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_BMB_PROP_DESTROY_MESSAGE_EVENT(int iCount)

	SCRIPT_EVENT_DATA_BMB_PROP_DETAILS ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))

		IF ObjectiveData.Details.Type = SCRIPT_EVENT_BMB_PROP_DESTROY
		
			PRINTLN( "[BMB-NET] ID PROCESS_BMB_PROP_DESTROYED_MESSAGE_EVENT: ", ObjectiveData.propIdx )
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciENABLE_BMB_MODE)
				BMB_DESTROY_DESTRUCTIBLE_PROP( ObjectiveData.propIdx )
			ELSE
				SCRIPT_ASSERT( "Bomber Mode receiving PROCESS_BMB_PROP_DESTROYED_MESSAGE_EVENT when the mode is not enabled." )
			ENDIF

		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_PLAYER_CROSSED_LINE_EVENT(int iCount)

	SCRIPT_EVENT_PLAYER_CROSSED_LINE_DETAILS ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))

		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PLAYER_CROSSED_THE_LINE
			IF NOT g_bTriggerCrossTheLineAudio
				IF ObjectiveData.iTeam = GET_PLAYER_TEAM(PLAYER_ID())
				OR DOES_TEAM_LIKE_TEAM(ObjectiveData.iTeam, GET_PLAYER_TEAM(PLAYER_ID()))
					//Currently play the sound for both teams.
					#IF IS_DEBUG_BUILD
						IF DOES_TEAM_LIKE_TEAM(ObjectiveData.iTeam, GET_PLAYER_TEAM(PLAYER_ID()))
							PRINTLN( "[RCC MISSION][AW_MISSION] PROCESS_PLAYER_CROSSED_LINE_EVENT - team likes team, playing cheer")
						ELSE
							PRINTLN( "[RCC MISSION][AW_MISSION] PROCESS_PLAYER_CROSSED_LINE_EVENT - same team, playing cheer")
						ENDIF
					#ENDIF
					
					PLAY_SOUND_FRONTEND(-1, "Cheers", "DLC_TG_Running_Back_Sounds" , FALSE)
				ENDIF
				
				PRINTLN( "[RCC MISSION][AW_MISSION] PROCESS_PLAYER_CROSSED_LINE_EVENT - playing whistle")
				PLAY_SOUND_FRONTEND(-1, "Whistle", "DLC_TG_Running_Back_Sounds" , FALSE)
				g_bTriggerCrossTheLineAudio = TRUE
			ELSE
				PRINTLN("[RCC MISSION] - PROCESS_MISSION_END_STAGE() - We already triggered the cross the line audio")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_PLAYER_IS_SWAN_DIVE_SCORING(int iCount)
	SCRIPT_EVENT_PLAYER_IS_SWAN_DIVE_SCORING Event	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Event, SIZE_OF(Event))
		IF Event.Details.Type = SCRIPT_EVENT_FMMC_PLAYER_IS_SWAN_DIVE_SCORING
			
			#IF IS_DEBUG_BUILD
				PRINTLN("PROCESS_PLAYER_IS_SWAN_DIVE_SCORING - From ", GET_PLAYER_NAME(Event.Details.FromPlayerIndex))
			#ENDIF
		
			IF Event.Details.FromPlayerIndex != LocalPlayer
			AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
			AND NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_IS_DIVE_SCORE_ANIM_IN_PROGRESS)
			
				SET_SWAN_DIVE_CAM_STATE(SDSCS_FindRemoteCamSlot)
				iSwanCamAngleTestSelection = 1
				siSwanDiveCamRay = NULL
				REINIT_NET_TIMER(stDiveCamStallTimer)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_RANDOM_SPAWN_SLOT_FOR_TEAM(int iCount)

	SCRIPT_EVENT_RANDOM_SPAWN_POS_DETAILS ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_RANDOM_SPAWN_SLOT_USED
			IF bIsLocalPlayerHost
				IF NOT IS_BIT_SET(iTeamPropSpawns, ObjectiveData.iTeam)
					SET_BIT(MC_ServerBD.iSpawnPointsUsed[ObjectiveData.iTeam], ObjectiveData.iSpawnSlot)
					SET_BIT(iTeamPropSpawns, ObjectiveData.iTeam)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_REMOTE_VEHICLE_SWAP(int iCount)

	SCRIPT_EVENT_DATA_FMMC_REMOTE_VEHICLE_SWAP ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_REMOTE_VEHICLE_SWAP
			IF NETWORK_IS_PLAYER_ACTIVE(ObjectiveData.playerIndex)
				PED_INDEX tempPed = GET_PLAYER_PED(ObjectiveData.playerIndex)
				IF DOES_ENTITY_EXIST(tempPed)
					IF IS_PED_IN_ANY_VEHICLE(tempPed)
						IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(tempPed))
							SET_ENTITY_MOTION_BLUR(GET_VEHICLE_PED_IS_IN(tempPed), FALSE)
							PRINTLN("[PROCESS_REMOTE_VEHICLE_SWAP] Setting motion blur on entity")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_OVERTIME_SCORED(int iCount)

	SCRIPT_EVENT_DATA_FMMC_OVERTIME_SCORED ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_OVERTIME_SCORED
			IF bIsLocalPlayerHost
				IF g_FMMC_STRUCT.iMissionEndType != ciMISSION_SCORING_TYPE_TIME
					IF ObjectiveData.iTeam < FMMC_MAX_TEAMS
						IF ObjectiveData.iPriority < FMMC_MAX_RULES
						AND ObjectiveData.iPriority = MC_serverBD_4.iCurrentHighestPriority[ObjectiveData.iTeam]
							MC_serverBD.iScoreOnThisRule[ObjectiveData.iTeam] += ObjectiveData.iScore
							PRINTLN("[RCC MISSION] PROCESS_OVERTIME_SCORED - team: ",ObjectiveData.iTeam," priority ",ObjectiveData.iPriority,", increased by ",ObjectiveData.iScore,",  total score ", MC_serverBD.iScoreOnThisRule[ObjectiveData.iTeam] )							
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_AIRSHOOTOUT_SCORED(int iCount)

	SCRIPT_EVENT_DATA_FMMC_AIRSHOOTOUT_SCORED ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_AIRSHOOTOUT_SCORED
			TEXT_LABEL_15 title
			title = "ADV_ASO_SC_"
			BOOL won0 = ObjectiveData.team0
			BOOL won1 = ObjectiveData.team1
			IF won0
				IF won1
					title += "2"
				ELSE
					title += "0"
				ENDIF
			ELSE
				IF won1
					title += "1"
				ELSE
					title += "3"
				ENDIF
			ENDIF
			HUD_COLOURS colour
			colour = HUD_COLOUR_WHITE
			SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, title, "", colour)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_OVERTIME_REMOVE_VEHICLE(int iCount)

	SCRIPT_EVENT_DATA_FMMC_OVERTIME_REMOVE_VEHICLE Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Event, SIZE_OF(Event))
		IF Event.Details.Type = SCRIPT_EVENT_FMMC_OVERTIME_REMOVE_VEHICLE
			PRINTLN("[RCC MISSION] PROCESS_OVERTIME_REMOVE_VEHICLE - Received Event.")
			IF GET_PLAYER_RESPAWN_BACK_AT_START_STATE(iLocalPart) = PRBSS_Prep
				IF NETWORK_DOES_NETWORK_ID_EXIST(Event.netVehicleToDelete)
					
					PRINTLN("[RCC MISSION] PROCESS_OVERTIME_REMOVE_VEHICLE - Vehicle Exists.")
					
					VEHICLE_INDEX vehIndex = NET_TO_VEH(Event.netVehicleToDelete)
					
					IF DOES_ENTITY_EXIST(vehIndex)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(vehIndex)
							PRINTLN("[RCC MISSION] PROCESS_OVERTIME_REMOVE_VEHICLE - We have ownership of the Vehicle. Deleting.")
							DELETE_VEHICLE(vehIndex)
						ELSE
							PRINTLN("[RCC MISSION] PROCESS_OVERTIME_REMOVE_VEHICLE - We do not own this vehicle. Do Nothing.")
						ENDIF
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION] PROCESS_OVERTIME_REMOVE_VEHICLE - No longer in Prep. Old Event, don't need it.")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_EVENT_PRON_PLAYERS_DRAWING_TRAILS(int iCount)

	SCRIPT_EVENT_PRON_PLAYERS_DRAWING_TRAILS_DETAILS ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_PRON_PLAYERS_DRAWING_TRAILS
			IF bIsLocalPlayerHost
				IF ObjectiveData.bIsPlayerDrawing
					IF NOT IS_BIT_SET(MC_serverBD_3.iPronPlayerIsDrawingLines, ObjectiveData.iPlayer)
						SET_BIT(MC_serverBD_3.iPronPlayerIsDrawingLines, ObjectiveData.iPlayer)
					ENDIF
				ELSE
					IF IS_BIT_SET(MC_serverBD_3.iPronPlayerIsDrawingLines, ObjectiveData.iPlayer)
						CLEAR_BIT(MC_serverBD_3.iPronPlayerIsDrawingLines, ObjectiveData.iPlayer)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_EVENT_FMMC_TRIGGERED_BODY_SCANNER(int iCount)

	SCRIPT_EVENT_DATA_FMMC_TRIGGERED_BODY_SCANNER ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_TRIGGERED_BODY_SCANNER
			PRINTLN("[RCC MISSION] starting tdMissionBodyScannerTimer (event)")
			START_NET_TIMER(tdMissionBodyScannerTimer)	
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_FMMC_REQUEST_RESTRICTION_ROCKETS(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_JUST_EVENT ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_REQUEST_RESTRICTION_ROCKETS
			
			IF NOT IS_BIT_SET(iLocalBoolCheck5,LBOOL5_RESTRICTION_ROCKET_LOADED)
				REQUEST_WEAPON_ASSET(WEAPONTYPE_RPG)
				SET_BIT(iLocalBoolCheck5,LBOOL5_RESTRICTION_ROCKET_LOADED)
				PRINTLN("[RCC MISSION] PROCESS_FMMC_REQUEST_RESTRICTION_ROCKETS called")
			ENDIF
			
			SET_BIT(iLocalBoolCheck6,LBOOL6_SOMEONE_NEEDS_RESTRICTION_ROCKETS)
		ENDIF
	ENDIF
	
ENDPROC


PROC PROCESS_FMMC_OBJECTIVE_MID_POINT(INT iCount)

	SCRIPT_EVENT_DATA_FMMC_OBJECTIVE_MID_POINT ObjectiveData
	
	IF bIsLocalPlayerHost
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
			IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_OBJECTIVE_MID_POINT
				IF ObjectiveData.iObjective < FMMC_MAX_RULES
				AND NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[ObjectiveData.iTeam],ObjectiveData.iObjective)
					SET_BIT(MC_serverBD.iObjectiveMidPointBitset[ObjectiveData.iTeam],ObjectiveData.iObjective)
					SET_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
					PRINTLN("[RCC MISSION] TEAM : ",ObjectiveData.iTeam," HAS HIT THE MIDPOINT iObjectiveMidPointBitset OF OBJECTIVE: ",ObjectiveData.iObjective)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: CUTSCENE EVENTS !
//
//************************************************************************************************************************************************************



PROC PROCESS_TEAM_HAS_FINISHED_CUTSCENE( int icount )

	SCRIPT_EVENT_DATA_FMMC_TEAM_HAS_FINISHED_CUTSCENE CutsceneData
	
	IF GET_EVENT_DATA( SCRIPT_EVENT_QUEUE_NETWORK, iCount, CutsceneData, SIZE_OF( CutsceneData ) )

		IF CutsceneData.Details.Type = SCRIPT_EVENT_FMMC_TEAM_HAS_FINISHED_CUTSCENE

			PRINTLN("[RCC MISSION] PROCESS_TEAM_HAS_FINISHED_CUTSCENE - CutsceneData.iTeam = ", CutsceneData.iTeam) 
			
			INT iSpectatorTargetPart = -1
			
			IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
				iSpectatorTargetPart = GET_SPECTATOR_TARGET_PARTICIPANT_OF_PARTICIPANT( iLocalPart )
			ENDIF
			
			IF CutsceneData.iTeam = MC_PlayerBD[ iLocalPart].iTeam
			OR ( iSpectatorTargetPart != -1 AND CutsceneData.iTeam = MC_PlayerBD[ iSpectatorTargetPart ].iTeam )
				CLEAR_BIT( MC_PlayerBD[ iLocalPart].iClientBitset2, PBBOOL2_STARTED_CUTSCENE )
				CLEAR_BIT( MC_PlayerBD[ iLocalPart].iClientBitset2, PBBOOL2_FINISHED_CUTSCENE )
				CLEAR_BIT( MC_PlayerBD[ iLocalPart].iClientBitset2, PBBOOL2_I_AM_SPECTATOR_WATCHING_MOCAP ) 
				PRINTLN("[RCC MISSION] PROCESS_TEAM_HAS_FINISHED_CUTSCENE - clearing bits for  CutsceneData.iTeam = ", CutsceneData.iTeam) 
			ENDIF

		ENDIF
	ENDIF

ENDPROC

//Process the players weapon for a cut scene event
PROC PROCESS_SCRIPT_EVENT_FMMC_MY_WEAPON_DATA( INT iCount )
	SCRIPT_EVENT_DATA_FMMC_MY_WEAPON_DATA sWeaponData	
	IF GET_EVENT_DATA( SCRIPT_EVENT_QUEUE_NETWORK, iCount, sWeaponData, SIZE_OF( sWeaponData ) )
		INT i
		FOR i = 0 TO (ciMAX_CUT_PLAYERS - 1)
			IF NOT sPlayerWeaponDetailsForCutscene[i].bSet
				sPlayerWeaponDetailsForCutscene[i].bHasThisWeapon = sWeaponData.bHasWeapon
				sPlayerWeaponDetailsForCutscene[i].sWeaponInfo	= sWeaponData.sWeaponInfo
				sPlayerWeaponDetailsForCutscene[i].bSet = TRUE
				sPlayerWeaponDetailsForCutscene[i].iParticipant = sWeaponData.iParticipant
				CPRINTLN( DEBUG_CONTROLLER, " PROCESS_SCRIPT_EVENT_FMMC_MY_WEAPON_DATA - sPlayerWeaponDetailsForCutscene[", i, "], bHasWeapon = ", BOOL_TO_STRING( sWeaponData.bHasWeapon ), ", participant = ", sWeaponData.iParticipant )
				i = ciMAX_CUT_PLAYERS // Exit loop
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

PROC PROCESS_FMMC_CUTSCENE_SPAWN_PROGRESS(INT iCount)

	SCRIPT_EVENT_DATA_CUTSCENE_SPAWN_PROGRESS CutsceneData
	
	IF bIsLocalPlayerHost
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, CutsceneData, SIZE_OF(CutsceneData))
			IF CutsceneData.Details.Type = SCRIPT_EVENT_FMMC_CUTSCENE_SPAWN_PROGRESS
				MC_serverBD.iSpawnScene = CutsceneData.iCutscene
				MC_serverBD.iSpawnShot = CutsceneData.iCutsceneCamshot
				PRINTLN("[RCC MISSION] cutscene : ",CutsceneData.iCutscene," SCRIPT_EVENT_DATA_CUTSCENE_SPAWN_PROGRESS cam shot: ",CutsceneData.iCutsceneCamshot)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_MOCAP_PLAYER_ANIMATION(INT iEventID)
	
	EVENT_STRUCT_MOCAP_PLAYER_ANIM Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
	
		// Check if we are in same vote
		PRINTLN("[RCC MISSION]  PROCESS_MOCAP_PLAYER_ANIMATION - ")
		PRINTLN("[RCC MISSION] 			Player: ", NATIVE_TO_INT(Event.Details.FromPlayerIndex))
		PRINTLN("[RCC MISSION] 			Anim Type: ", Event.iAnimationType)
		PRINTLN("[RCC MISSION] 			Anim: ", Event.iAnimation)
		PRINTLN("[RCC MISSION] 			bPlaying: ", Event.bPlaying)
		
		INT iPlayerInt = NATIVE_TO_INT(Event.Details.FromPlayerIndex)
						
		// Update globals so we process new animations
		MPGlobalsInteractions.PedInteraction[iPlayerInt].iInteractionType =  Event.iAnimationType
		MPGlobalsInteractions.PedInteraction[iPlayerInt].iInteractionAnim = Event.iAnimation
		IF Event.bPlaying
			MPGlobalsInteractions.PedInteraction[iPlayerInt].bPlayInteractionAnim = Event.bPlaying
		ENDIF
		MPGlobalsInteractions.PedInteraction[iPlayerInt].bHoldLoop = Event.bPlaying

	ELSE
		SCRIPT_ASSERT("[RCC MISSION] PROCESS_MOCAP_PLAYER_ANIMATION - could not retrieve data")
	ENDIF
	
ENDPROC

PROC PROCESS_START_STREAMING_END_MOCAP_EVENT( INT iEventID )
	EVENT_STRUCT_STREAM_END_MOCAP_DATA Event
	
	IF GET_EVENT_DATA( SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF( Event ) )

		PRINTLN("[RCC MISSION]  PROCESS_START_STREAMING_END_MOCAP_EVENT - Setting bit ")
		SET_BIT( iLocalBoolCheck7, LBOOL7_TOGGLED_RENDERPHASES_FOR_DROPOFF )
		#IF IS_DEBUG_BUILD
		IF IS_NET_PLAYER_OK(event.Details.FromPlayerIndex)
			PRINTLN("[RCC MISSION]  PROCESS_START_STREAMING_END_MOCAP_EVENT recieved from = ",GET_PLAYER_NAME(event.Details.FromPlayerIndex))
		ELSE
			PRINTLN("[RCC MISSION]  PROCESS_START_STREAMING_END_MOCAP_EVENT recieved from = ",NATIVE_TO_INT(event.Details.FromPlayerIndex))
		ENDIF
		#ENDIF
		IF bIsLocalPlayerHost
			mc_serverBD.dropOffAtApartmentPlayer = event.Details.FromPlayerIndex
			#IF IS_DEBUG_BUILD
			IF IS_NET_PLAYER_OK(mc_serverBD.dropOffAtApartmentPlayer)
				PRINTLN("[RCC MISSION]  mc_serverBD.dropOffAtApartmentPlayer = ",GET_PLAYER_NAME(mc_serverBD.dropOffAtApartmentPlayer))
			ELSE
				PRINTLN("[RCC MISSION]  mc_serverBD.dropOffAtApartmentPlayer = ",NATIVE_TO_INT(mc_serverBD.dropOffAtApartmentPlayer))
			ENDIF
			#ENDIF
		ENDIF
	ELSE
		SCRIPT_ASSERT("[RCC MISSION] PROCESS_START_STREAMING_END_MOCAP_EVENT - could not retrieve data")
	ENDIF
ENDPROC

STRUCT EVENT_STRUCT_MOCAP_PLAYER_REQUEST
	STRUCT_EVENT_COMMON_DETAILS	Details
	INT iTeam
	FMMC_CUTSCENE_TYPE eCutType
ENDSTRUCT

PROC BROADCAST_CUTSCENE_PLAYER_REQUEST(INT iTeam, FMMC_CUTSCENE_TYPE eCutType)
	
	EVENT_STRUCT_MOCAP_PLAYER_REQUEST Event
	Event.Details.Type 				= SCRIPT_EVENT_FMMC_CUTSCENE_PLAYER_REQUEST
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	Event.iTeam 					= iTeam
	Event.eCutType					= eCutType
		
	INT iSendTo = ALL_PLAYERS(TRUE)
	IF NOT (iSendTo = 0)		
		
		#IF IS_DEBUG_BUILD
			PRINTLN("[RCC MISSION]  BROADCAST_CUTSCENE_PLAYER_REQUEST - called...")
			PRINTLN("[RCC MISSION] 					From script: ", GET_THIS_SCRIPT_NAME())
			PRINTLN("[RCC MISSION] 					iTeam: ", Event.iTeam )
		#ENDIF

		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iSendTo)
	ELSE
		PRINTLN("[RCC MISSION] BROADCAST_CUTSCENE_PLAYER_REQUEST - playerflags = 0 so not broadcasting") 
	ENDIF	
	
ENDPROC


PROC PROCESS_CUTSCENE_PLAYER_REQUEST(INT iEventID)
	
	EVENT_STRUCT_MOCAP_PLAYER_REQUEST Event
	INT iteam
	
	IF bIsLocalPlayerHost
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
			
			// Check if we are in same vote
			PRINTLN("[RCC MISSION]  PROCESS_CUTSCENE_PLAYER_REQUEST - ")
			PRINTLN("[RCC MISSION] 			Player: ", NATIVE_TO_INT(Event.Details.FromPlayerIndex))
			PRINTLN("[RCC MISSION] 			mocap team: ", Event.iTeam)
			
			SWITCH Event.iTeam
				CASE 0
					SET_BIT(MC_ServerBD.iServerBitSet1, SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_0)
				BREAK
				CASE 1
					SET_BIT(MC_ServerBD.iServerBitSet1, SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_1)
				BREAK
				CASE 2
					SET_BIT(MC_ServerBD.iServerBitSet1, SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_2)
				BREAK
				CASE 3
					SET_BIT(MC_ServerBD.iServerBitSet1, SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_3)
				BREAK
			ENDSWITCH
			
			SWITCH Event.eCutType
				
				CASE FMMCCUT_ENDMOCAP
					
					FOR iteam = 0 TO (FMMC_MAX_TEAMS-1)
						IF DOES_TEAM_LIKE_TEAM(iteam,Event.iTeam)
							IF NOT HAS_NET_TIMER_STARTED(MC_serverBD.tdCutsceneStartTime[iteam])
								REINIT_NET_TIMER(MC_serverBD.tdCutsceneStartTime[iteam],FALSE,TRUE)
								PRINTLN("[RCC MISSION] - MC_serverBD.tdCutsceneStartTime set at time: ",NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()) )
								PRINTLN("[RCC MISSION] -HOST cutscene should start at : ",(NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE())+1000))
							ENDIF	
							SWITCH iteam
								CASE 0
									SET_BIT(MC_ServerBD.iServerBitSet1, SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_0)
								BREAK
								CASE 1
									SET_BIT(MC_ServerBD.iServerBitSet1, SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_1)
								BREAK
								CASE 2
									SET_BIT(MC_ServerBD.iServerBitSet1, SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_2)
								BREAK
								CASE 3
									SET_BIT(MC_ServerBD.iServerBitSet1, SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_3)
								BREAK
							ENDSWITCH
						ENDIF
					ENDFOR
				
				BREAK
				
			ENDSWITCH
		ELSE
			SCRIPT_ASSERT("[RCC MISSION] PROCESS_MOCAP_PLAYER_ANIMATION - could not retrieve data")
		ENDIF
	ENDIF
	
ENDPROC

//caches the delivered vehicle index into cutscene_vehicle[] so it can still be reffereced in order to to set 
//the vehicle visible in the cutscene after CLEANUP_NET_ID has been called on the vehicle.
PROC PROCESS_FMMC_ADD_CUTSCENE_VEH(INT iCount)

	SCRIPT_EVENT_DATA_FMMC_CUTSCENE_VEH CutscenVehData
	//BOOL bTicker = TRUE
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, CutscenVehData, SIZE_OF(CutscenVehData))

		IF CutscenVehData.Details.Type = SCRIPT_EVENT_FMMC_DELIVER_CUTSCENE_VEH
			PRINTLN("[RCC MISSION] CutscenVehData.iVehNetID = ",CutscenVehData.iVehNetID)
			PRINTLN("[RCC MISSION] CutscenVehData.iCSNumVeh = ",CutscenVehData.iCSNumVeh)
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[CutscenVehData.iVehNetID])
				cutscene_vehicle[CutscenVehData.iCSNumVeh] = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[CutscenVehData.iVehNetID])
				
				IF bIsLocalPlayerHost
					PRINTLN("[RCC MISSION] MC_serverBD.cutscene_vehicle_count = ",MC_serverBD.cutscene_vehicle_count)
					MC_serverBD.cutscene_vehicle_count++
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC


//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: SCORE EVENTS !
//
//************************************************************************************************************************************************************



/*FUNC BOOL IS_MISSION_PICKUP(PICKUP_INDEX temp_pickup)

	INT ipickup,iobj
	FOR iobj = 0 TO (FMMC_MAX_NUM_OBJECTS-1)
		FOR ipickup = 0 TO (ciMAX_CRATE_PICKUPS-1)
			IF piCratePickup[iobj][ipickup]  =	temp_pickup
				RETURN TRUE
			ENDIF
		ENDFOR
	ENDFOR
	
	RETURN FALSE

ENDFUNC*/

PROC PROCESS_COLLECTED_PICKUP_EVENT(INT iCount)
	
	STRUCT_PICKUP_EVENT PickupData
	TEXT_LABEL_15 labelTemp
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, PickupData, SIZE_OF(PickupData))
		IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_WeaponPickupTracking)
		AND PickupData.PlayerIndex = LocalPlayer
			INT i
			FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons - 1
				
				IF i >= FMMC_MAX_WEAPONS
					BREAKLOOP
				ENDIF
				
				IF PickupData.PickupIndex != pipickup[i]
					RELOOP
				ENDIF
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iContinuityId != -1
					IF NOT IS_LONG_BIT_SET(sLEGACYMissionLocalContinuityVars.iPickupCollectedTrackingBitset, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iContinuityId)
						SET_LONG_BIT(sLEGACYMissionLocalContinuityVars.iPickupCollectedTrackingBitset, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iContinuityId)
						PRINTLN("[JS][CONTINUITY] - PROCESS_COLLECTED_PICKUP_EVENT - pickup ", i, " with continuity ID ", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iContinuityId, "collected")
					ENDIF
				ENDIF
				
				BREAKLOOP
				
			ENDFOR	
		ENDIF
		
		IF IS_NET_PLAYER_OK(PickupData.PlayerIndex)
    		IF PickupData.PlayerIndex = LocalPlayer
        		IF NETWORK_IS_PLAYER_A_PARTICIPANT(PickupData.PlayerIndex)
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciENABLE_BMB_MODE)
						IF PickupData.PickupType = PICKUP_VEHICLE_CUSTOM_SCRIPT_LOW_GLOW
							IF PickupData.PickupCustomModel = PROP_MP_ROCKET_01
								BMB_INCREMENT_AVAILABLE_BOMBS()	
								PRINT_TICKER("BMB_PU_BOMB") 
								EXIT 
							ELIF PickupData.PickupCustomModel = PROP_MP_BOOST_01
								BMB_INCREMENT_BOMB_SIZE()
								PRINT_TICKER("BMB_PU_SIZE") 
								EXIT
							ELIF PickupData.PickupCustomModel = PROP_MP_REPAIR
								BMB_APPLY_CAR_ARMOUR()
								PRINT_TICKER("BMB_PU_ARMOUR") 
								EXIT
							ENDIF
						ENDIF
					ENDIF
				
                	IF IS_PICKUP_A_WEAPON(PickupData.PickupType)
					OR IS_PICKUP_PARACHUTE(PickupData.PickupType)
					
						labelTemp = GET_CORRECT_TICKER_STRING(PickupData.PickupType)
	              		IF NOT IS_STRING_NULL_OR_EMPTY(labelTemp)
							PRINT_TICKER(labelTemp) 
						ENDIF 
						IF GET_CURRENT_GAMEMODE() = GAMEMODE_CREATOR
							INT i
							REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons i
								IF i < ciMAX_CREATED_WEAPONS
									IF PickupData.PickupIndex = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].id
										IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPlacedBitset, ciFORCE_WEAPON_INTO_HAND)
											SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), GET_WEAPON_TYPE_FROM_PICKUP_TYPE(PickupData.PickupType), TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDREPEAT
						ENDIF
					ELSE
						/*SWITCH PickupData.pickuptype

							CASE PICKUP_MONEY_VARIABLE
							CASE PICKUP_MONEY_MED_BAG
								IF PickupData.PickupAmount > 0
									IF IS_MISSION_PICKUP(PickupData.PickupIndex)
										#IF IS_DEBUG_BUILD
											iDebugCashPickup = iDebugCashPickup + PickupData.PickupAmount
										#ENDIF
										//MC_playerBD[iPartToUse].iCashReward =MC_playerBD[iPartToUse].iCashReward + PickupData.PickupAmount
									ENDIF
								ENDIF
							BREAK
							
						ENDSWITCH*/
					ENDIF
         		ENDIF
      		ENDIF
		ENDIF
	ENDIF
				
ENDPROC

PROC PROCESS_STUNT_RP_EVENT(TrackedStuntType eSTUNT, FLOAT fValue)
	INT iRP
	XPCATEGORY XPCat
	
	BOOL bAwardRP, bBoost

	SWITCH eSTUNT
	    CASE ST_FRONTFLIP
			iRP = g_sMPTunables.iSTUNT_RP_BONUS_FRONTFLIP
			XPCat = XPCATEGORY_SKILL_RACE_STUNT_FFLIP
			bAwardRP = TRUE
			PRINTLN("[RCC MISSION] [STUNTS]  PROCESS_STUNT_RP_EVENT, ST_FRONTFLIP ")
			bBoost = TRUE
			HANDLE_STUNT_RUN_POINTS(500, eStuntTrick_Frontflip)
		BREAK
	    CASE ST_BACKFLIP
			iRP = g_sMPTunables.iSTUNT_RP_BONUS_BACKFLIP
			XPCat = XPCATEGORY_SKILL_RACE_STUNT_BFLIP
			bAwardRP = TRUE
			PRINTLN("[RCC MISSION] [STUNTS] PROCESS_STUNT_RP_EVENT, ST_BACKFLIP ")
			bBoost = TRUE
			HANDLE_STUNT_RUN_POINTS(200, eStuntTrick_Backflip)
		BREAK
	    CASE ST_SPIN
			IF fValue >= g_sMPTunables.fStuntSpinTwoAndHalf
				iRP = g_sMPTunables.iSTUNT_RP_BONUS_900FLIP
				XPCat = XPCATEGORY_SKILL_RACE_STUNT_SPIN900
				bAwardRP = TRUE
				bBoost = TRUE
				HANDLE_STUNT_RUN_POINTS(900, eStuntTrick_Spin900)
				PRINTLN("[RCC MISSION] [STUNTS] PROCESS_STUNT_RP_EVENT, XPCATEGORY_SKILL_RACE_STUNT_SPIN900 ")
			ELIF fValue >= g_sMPTunables.fStuntSpinTwo
				iRP = g_sMPTunables.iSTUNT_RP_BONUS_720FLIP
				XPCat = XPCATEGORY_SKILL_RACE_STUNT_SPIN720	
				bAwardRP = TRUE
				bBoost = TRUE
				HANDLE_STUNT_RUN_POINTS(720, eStuntTrick_Spin720)
				PRINTLN("[RCC MISSION] [STUNTS] PROCESS_STUNT_RP_EVENT, XPCATEGORY_SKILL_RACE_STUNT_SPIN720 ")
			ELIF fValue >= g_sMPTunables.fStuntSpinOneAndHalf
				iRP = g_sMPTunables.iSTUNT_RP_BONUS_540FLIP
				XPCat = XPCATEGORY_SKILL_RACE_STUNT_SPIN540	
				bAwardRP = TRUE
				bBoost = TRUE
				HANDLE_STUNT_RUN_POINTS(540, eStuntTrick_Spin540)
				PRINTLN("[RCC MISSION] [STUNTS] PROCESS_STUNT_RP_EVENT, XPCATEGORY_SKILL_RACE_STUNT_SPIN540 ")
			ELIF fValue >= g_sMPTunables.fStuntSpinOne // url:bugstar:2904309
				iRP = g_sMPTunables.iSTUNT_RP_BONUS_360FLIP
				XPCat = XPCATEGORY_SKILL_RACE_STUNT_SPIN360	
				bAwardRP = TRUE
				bBoost = TRUE
				PRINTLN("[RCC MISSION] [STUNTS] PROCESS_STUNT_RP_EVENT, XPCATEGORY_SKILL_RACE_STUNT_SPIN360 ")
				HANDLE_STUNT_RUN_POINTS(360, eStuntTrick_Spin360)
			ELSE
				PRINTLN("[RCC MISSION] [STUNTS] PROCESS_STUNT_RP_EVENT, XPCATEGORY_SKILL_RACE_STUNT_SPIN but fValue = ", fValue)
				EXIT
			ENDIF
		BREAK
	    CASE ST_WHEELIE
			IF fValue >= 3
				iRP = g_sMPTunables.iSTUNT_RP_BONUS_WHEELIE	
				XPCat = XPCATEGORY_SKILL_RACE_STUNT_WHEELIE
				bAwardRP = TRUE
				bBoost = TRUE
				PRINTLN("[RCC MISSION] [STUNTS] PROCESS_STUNT_RP_EVENT, ST_WHEELIE ")
				HANDLE_STUNT_RUN_POINTS(ROUND(fValue), eStuntTrick_Wheelie)
			ENDIF
		BREAK
	    CASE ST_STOPPIE
			IF fValue >= 1
				iRP = g_sMPTunables.iSTUNT_RP_BONUS_STOPPIE	
				XPCat = XPCATEGORY_SKILL_RACE_STUNT_STOPPIE
				bAwardRP = TRUE
				bBoost = TRUE
				PRINTLN("[RCC MISSION] [STUNTS] PROCESS_STUNT_RP_EVENT, ST_STOPPIE ")
			ENDIF
		BREAK
		CASE ST_BOWLING_PIN
			iRP = g_sMPTunables.iSTUNT_RP_BONUS_PIN	
			XPCat = XPCATEGORY_SKILL_RACE_STUNT_BPIN
			bAwardRP = TRUE
			PRINTLN("[RCC MISSION] [STUNTS] PROCESS_STUNT_RP_EVENT, ST_BOWLING_PIN ")
		BREAK
		CASE ST_FOOTBALL
			iRP = g_sMPTunables.iSTUNT_RP_BONUS_BALL	
			XPCat = XPCATEGORY_SKILL_RACE_STUNT_FBALL
			bAwardRP = TRUE
			PRINTLN("[RCC MISSION] [STUNTS] PROCESS_STUNT_RP_EVENT, ST_FOOTBALL ")
		BREAK
		CASE ST_ROLL			
			XPCat = XPCATEGORY_SKILL_RACE_STUNT_ROLL
			IF fValue >= 0.85
				IF fValue < 1.0
					fValue = 1.0
				ENDIF
				//Add to it so if you do 90% of a roll it is fair
				fValue+=0.15
				iRP = g_sMPTunables.iSTUNT_RP_BONUS_BARRELROLL * FLOOR(fValue)
				bAwardRP = TRUE
				bBoost = TRUE
				HANDLE_STUNT_RUN_POINTS(500, eStuntTrick_Roll)
				PRINTLN("[RCC MISSION] [STUNTS] PROCESS_STUNT_RP_EVENT, ST_ROLL, iRP = g_sMPTunables.iSTUNT_RP_BONUS_BARRELROLL * FLOOR(fValue)  - So ", 
						iRP, "  = ", g_sMPTunables.iSTUNT_RP_BONUS_BARRELROLL, " * FLOOR(", fValue, ")")
			ELSE
				PRINTLN("[RCC MISSION] [STUNTS] PROCESS_STUNT_RP_EVENT, XPCATEGORY_SKILL_RACE_STUNT_ROLL but fValue = ", fValue)
				EXIT
			ENDIF
		BREAK
	ENDSWITCH
	
	IF bAwardRP
	AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciUSE_STUNT_RP_BONUS)
		
		// Store stunt RP
		iStuntRPEarned += ( iRP * ROUND(g_sMPTunables.xpMultiplier) )

		// Cap RP
		IF iStuntRPEarned > g_sMPtunables.imaxrpforstunts
			PRINTLN("[RCC MISSION] [STUNTS] PROCESS_STUNT_RP_EVENT, [CAP] iStuntRPEarned    = ", iStuntRPEarned)
			PRINTLN("[RCC MISSION] [STUNTS] PROCESS_STUNT_RP_EVENT, [CAP] g_sMPtunables.imaxrpforstunts = ", g_sMPtunables.imaxrpforstunts)
			iRP = ( g_sMPtunables.imaxrpforstunts - iStuntRPEarned )
			PRINTLN("[RCC MISSION] [STUNTS] PROCESS_STUNT_RP_EVENT, [CAP] CAPPING RP TO GIVE AT ", iRP)
		ENDIF
		
		IF iRP > 0
		
			// Give RP
			GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "XPT_RCE_STNT", XPTYPE_SKILL, XPCat, iRP, 1, DEFAULT, DEFAULT, DEFAULT, g_sMPtunables.istuntrpduration)
			
			PRINTLN("[RCC MISSION] [STUNTS] PROCESS_STUNT_RP_EVENT, iRP to give = ", iRP)
			PRINTLN("[RCC MISSION] [STUNTS] PROCESS_STUNT_RP_EVENT, RP total now = ", iStuntRPEarned)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciRACE_BACKFLIP_BOOST)
		AND bBoost
			PRINTLN("[RCC MISSION] [STUNTS] PROCESS_STUNT_RP_EVENT - START_VEHICLE_BOOST")
			START_VEHICLE_BOOST(g_FMMC_STRUCT.fStuntBoost)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_FMMC_INCREMENT_REMOTE_PLAYER_SCORE(INT iCount)

	SCRIPT_EVENT_DATA_FMMC_PLAYER_SCORE ScoreData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ScoreData, SIZE_OF(ScoreData))
		
		IF ScoreData.Details.Type = SCRIPT_EVENT_FMMC_PLAYER_SCORE
			IF ScoreData.iServerKey = MC_ServerBD.iSessionScriptEventKey
				PRINTLN("[RCC MISSION] ScoreData.PlayerToIncrease = ",NATIVE_TO_INT(ScoreData.PlayerToIncrease))
				PRINTLN("[RCC MISSION] ScoreData.iamount = ",ScoreData.iamount) 
				PRINTLN("[RCC MISSION] player team: ",MC_playerBD[iPartToUse].iteam)
				PRINTLN("[RCC MISSION] player score: ",MC_Playerbd[iPartToUse].iPlayerScore) 
				IF g_FMMC_STRUCT.iMissionEndType != ciMISSION_SCORING_TYPE_TIME
					PRINTLN("[RCC MISSION] Mission type is points") 
					MC_Playerbd[iPartToUse].iPlayerScore= MC_Playerbd[iPartToUse].iPlayerScore + ScoreData.iamount
				ELSE
					PRINTLN("[RCC MISSION] Mission type is time") 
					MC_Playerbd[iPartToUse].tdMissionTime.Timer =GET_TIME_OFFSET(MC_Playerbd[iPartToUse].tdMissionTime.Timer,(ci_TIME_PER_POINT*1000*ScoreData.iamount))
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_INCREMENTED_LOCAL_PLAYER_SCORE(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_INCREMENTED_LOCAL_PLAYER_SCORE Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Event, SIZE_OF(Event))
		IF Event.Details.Type = SCRIPT_EVENT_FMMC_INCREMENTED_LOCAL_PLAYER_SCORE
			IF bIsLocalPlayerHost
				PRINTLN("[RCC MISSION] PROCESS_FMMC_INCREMENTED_LOCAL_PLAYER_SCORE - Event.iTeam = ",Event.iTeam)
				PRINTLN("[RCC MISSION] PROCESS_FMMC_INCREMENTED_LOCAL_PLAYER_SCORE - Event.iPriority = ",Event.iPriority)
				PRINTLN("[RCC MISSION] PROCESS_FMMC_INCREMENTED_LOCAL_PLAYER_SCORE - Event.iScore = ",Event.iScore)
				PRINTLN("[RCC MISSION] PROCESS_FMMC_INCREMENTED_LOCAL_PLAYER_SCORE - Event.iVictimPart = ",Event.iVictimPart)
				
				IF Event.iTeam < FMMC_MAX_TEAMS
					IF Event.iPriority < FMMC_MAX_RULES
					AND Event.iPriority = MC_serverBD_4.iCurrentHighestPriority[Event.iTeam]
						MC_serverBD.iScoreOnThisRule[Event.iTeam] += Event.iScore
						PRINTLN("[RCC MISSION] PROCESS_FMMC_INCREMENTED_LOCAL_PLAYER_SCORE - total team score on rule = ",MC_serverBD.iScoreOnThisRule[Event.iTeam])
					ENDIF
				ENDIF
				
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
					IF Event.iScore = 0
					AND Event.iVictimPart > -1
						SET_PROGRESS_OBJECTIVE_FOR_TEAM(MC_playerBD[Event.iVictimPart].iTeam, TRUE)
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_TEAM_TURNS)
				AND bIsLocalPlayerHost
				AND NOT IS_THIS_BOMB_FOOTBALL()
					INT killerPart = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(Event.Details.FromPlayerIndex))
					PRINTLN("[RCC MISSION] PROCESS_FMMC_INCREMENTED_LOCAL_PLAYER_SCORE - Event.FromPlayerIndex = ",killerPart)
					MC_serverBD.iPlayerScore[ killerPart ] = MC_serverBD.iPlayerScore[ killerPart ] + Event.iScore
					MC_serverBD.iTeamScore[ Event.iTeam ] = MC_serverBD.iTeamScore[ Event.iTeam ] + Event.iScore
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(iLocalBoolCheck18, LBOOL18_ENABLE_GUNSMITH_AUDIO_SOUNDS)
				PRINTLN("[LM][GUNSMITH][AUDIO] - LBOOL18_ENABLE_GUNSMITH_AUDIO_SOUNDS is set. (Increment score)")
				IF Event.iScore > 0
					PRINTLN("[LM][GUNSMITH][AUDIO] - iScore > 0")
					IF Event.iTeam = MC_PlayerBD[iLocalPart].iteam
						PRINTLN("[LM][GUNSMITH][AUDIO] - Play Sound: Score_Team")
						PLAY_SOUND_FRONTEND(-1, "Score_Team", "DLC_Biker_KQ_Sounds")
					ELSE
						PRINTLN("[LM][GUNSMITH][AUDIO] - Play Sound: Score_Opponent")
						PLAY_SOUND_FRONTEND(-1, "Score_Opponent", "DLC_Biker_KQ_Sounds")
					ENDIF
				ENDIF
			ENDIF
			
			// If it doesn't do it fast enough here, then we need to set a bit here and then when the ui updates (we need to detect a change in the score being showed) we play the sound.
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_AIR_QUOTA_AUDIO)
				PRINTLN("[LM][AIRQUOTA][AUDIO] - ciENABLE_AIR_QUOTA_AUDIO is set. (Increment score)")
				INT iFromPlayerIndex = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(Event.Details.FromPlayerIndex))
				
				IF MC_serverBD.iPlayerScore[ iFromPlayerIndex ] = MC_serverBD.iPlayerScore[ NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(Event.Details.FromPlayerIndex)) ] + Event.iScore
					PRINTLN("[LM][AIRQUOTA][AUDIO][AQSND] - iScore > Target so playing Goal_Reached")
					PLAY_SOUND_FRONTEND(-1, "Goal_Reached" ,"dlc_xm_aqo_sounds", false)
					//SET_BIT(iLocalBoolCHeck25, LBOOL25_SHOULD_WAIT_TO_PLAY_AQ_GOAL_REACHED)
				ELIF Event.iScore > 0
					PRINTLN("[LM][AIRQUOTA][AUDIO][AQSND] - iScore > 0 so setting LBOOL25_SHOULD_WAIT_TO_PLAY_AQ_POINT_GAINED")
					//PLAY_SOUND_FRONTEND(-1, "Gain_Point" ,"dlc_xm_aqo_sounds", false)
					SET_BIT(iLocalBoolCHeck25, LBOOL25_SHOULD_WAIT_TO_PLAY_AQ_POINT_GAINED)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_OVERTIME_PAUSE_RENDERPHASE(INT iCount)
	SCRIPT_EVENT_DATA_FMMC_OVERTIME_PAUSE_RENDERPHASE Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Event, SIZE_OF(Event))
		IF Event.Details.Type = SCRIPT_EVENT_FMMC_OVERTIME_PAUSE_RENDERPHASE
			IF Event.iEventSessionKey = MC_ServerBD.iSessionScriptEventKey
				IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
					
					IF Event.bHidePlayer
						IF Event.Details.FromPlayerIndex != INVALID_PLAYER_INDEX()
							PED_INDEX pedPlayer = GET_PLAYER_PED(Event.Details.FromPlayerIndex)
							
							IF NOT IS_PED_INJURED(pedPlayer)
								IF NETWORK_HAS_CONTROL_OF_ENTITY(pedPlayer)
									SET_ENTITY_VISIBLE(pedPlayer, FALSE)
								ELSE
									SET_ENTITY_ALPHA(pedPlayer, 0, FALSE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT Event.bInstant
						RESET_NET_TIMER(tdOvertimeRenderphasePauseStart)
						START_NET_TIMER(tdOvertimeRenderphasePauseStart)
						PRINTLN("PROCESS_FMMC_OVERTIME_PAUSE_RENDERPHASE - PAUSING THE RENDERPHASE (STARTING TIMER)")
						
						IF DID_I_JOIN_MISSION_AS_SPECTATOR()
							SET_BIT(iLocalBoolCheck24, LBOOL24_SPECTATOR_RENDERPHASE_PAUSED_VIA_EVENT)
							RESET_NET_TIMER(tdOvertimeRenderphaseUnpauseForSpectators)
							START_NET_TIMER(tdOvertimeRenderphaseUnpauseForSpectators)
						ENDIF
					ELSE
						PRINTLN("PROCESS_FMMC_OVERTIME_PAUSE_RENDERPHASE - PAUSING THE RENDERPHASE (ACTUAL INSTANT FORCED)")
						#IF IS_DEBUG_BUILD IF NOT bDoNotPauseRenderphases #ENDIF
							TOGGLE_PAUSED_RENDERPHASES(FALSE)
							TOGGLE_PAUSED_RENDERPHASES(FALSE)
						#IF IS_DEBUG_BUILD ENDIF #ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_FMMC_OVERTIME_SCORED_SHARD(INT iCount)
	SCRIPT_EVENT_DATA_FMMC_OVERTIME_SCORED_SHARD Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Event, SIZE_OF(Event))
		IF Event.Details.Type = SCRIPT_EVENT_FMMC_OVERTIME_SCORED_SHARD
			
			PRINTLN("[LM][PROCESS_FMMC_OVERTIME_SCORED_SHARD] TURNS Play! Called by player: ", GET_PLAYER_NAME(Event.Details.FromPlayerIndex))
			
			PARTICIPANT_INDEX piPart = INT_TO_PARTICIPANTINDEX(Event.iPartToMove)
			PLAYER_INDEX piPlayer
			IF NETWORK_IS_PARTICIPANT_ACTIVE(piPart)
				piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)
				PRINTLN("[LM][PROCESS_FMMC_OVERTIME_SCORED_SHARD] TURNS Play! It is ", GET_PLAYER_NAME(piPlayer), " Turn")
			ENDIF
			
			IF Event.bPlayOvertimeShard
				
				PRINTLN("[LM][PROCESS_FMMC_OVERTIME_SCORED_SHARD] - Event.iPartToMove: ", Event.iPartToMove)				
				PRINTLN("[LM][PROCESS_FMMC_OVERTIME_SCORED_SHARD] - Event.iTeam: ", Event.iTeam)
				PRINTLN("[LM][PROCESS_FMMC_OVERTIME_SCORED_SHARD] - Event.iScore: ", Event.iScore)
				
				IF bIsLocalPlayerHost
					IF IS_THIS_CURRENTLY_OVERTIME_TURNS()												
						INT iCurrentRound = Event.iCurrentRound
						INT iTarget = g_FMMC_STRUCT.iOvertimeRounds
						
						IF IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_OVERTIME_SUDDEN_DEATH_ROUNDS)
						OR IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_SUDDEN_DEATH_OVERTIME_RUMBLE)
							IF g_FMMC_STRUCT.iOvertimeSDRounds != ciOVERTIME_ROUNDS_UNLIMITED
								IF g_FMMC_STRUCT.iOvertimeRoundScale > 0
									iTarget = iTarget * (g_FMMC_STRUCT.iOvertimeRoundScale * GET_LARGEST_STARTING_TEAM())
								ENDIF
								iCurrentRound = iCurrentRound - iTarget
							ENDIF
						ENDIF
						
						PRINTLN("[LM][PROCESS_FMMC_OVERTIME_SCORED_SHARD] - Event.iCurrentRound: ", Event.iCurrentRound)
						
						IF iCurrentRound > -1
						AND iCurrentRound < MAX_PENALTY_PLAYERS
							MC_serverBD.iTeamTurnScore[Event.iTeam][iCurrentRound] = Event.iScore
						ELSE
							PRINTLN("[LM][PROCESS_FMMC_OVERTIME_SCORED_SHARD] - WOULD HAVE OVERRUN (1) iCurrentRound: ", iCurrentRound, " iTarget", iTarget)
						ENDIF
					ENDIF
				ENDIF
				
				CLEAR_ALL_BIG_MESSAGES()
				
				IF piPlayer != INVALID_PLAYER_INDEX()
					IF iLocalPart = Event.iPartToMove
						IF Event.iScore = 0
							SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, "OVT_SC_PLA_TM", "OVT_SC_PLA_0c")
						ELIF Event.iScore > 1
							SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_AND_INT(BIG_MESSAGE_GENERIC_TEXT, Event.iScore, "OVT_SC_PLA_0b", "", "OVT_SC_PLA_TC")
						ELSE
							SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_AND_INT(BIG_MESSAGE_GENERIC_TEXT, Event.iScore, "OVT_SC_PLA_0a", "", "OVT_SC_PLA_TC")
						ENDIF				
					ELSE
						IF Event.iScore = 0
							SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_AND_INT(BIG_MESSAGE_GENERIC_TEXT, Event.iScore, "OVT_SC_PLA_1c", GET_PLAYER_NAME(piPlayer), "OVT_SC_PLA_TM")
						ELIF Event.iScore > 1
							SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_AND_INT(BIG_MESSAGE_GENERIC_TEXT, Event.iScore, "OVT_SC_PLA_1b", GET_PLAYER_NAME(piPlayer), "OVT_SC_PLA_TC")
						ELSE
							SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_AND_INT(BIG_MESSAGE_GENERIC_TEXT, Event.iScore, "OVT_SC_PLA_1a", GET_PLAYER_NAME(piPlayer), "OVT_SC_PLA_TC")
						ENDIF
					ENDIF
				ENDIF
			ELIF Event.bPlayOvertimeRumbleShard
				PRINTLN("[LM][PROCESS_FMMC_OVERTIME_SCORED_SHARD] RUMBLE Play! Called from: ", GET_PLAYER_NAME(Event.Details.FromPlayerIndex))
				//RESTART_LOGIC_TURN_PHONE_RADIO_ON()
				IF bIsLocalPlayerHost
					IF NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_OVERITME_SHOULD_PLAY_DELAYED_SCORE_SHARD_RUMBLE)
						SET_BIT(iLocalBoolCheck23, LBOOL23_OVERITME_SHOULD_PLAY_DELAYED_SCORE_SHARD_RUMBLE)
						PRINTLN("[LM][PROCESS_FMMC_OVERTIME_SCORED_SHARD] - Set Bit: LBOOL23_OVERITME_SHOULD_PLAY_DELAYED_SCORE_SHARD_RUMBLE")
					ELSE
						PRINTLN("[LM][PROCESS_FMMC_OVERTIME_SCORED_SHARD] - LBOOL23_OVERITME_SHOULD_PLAY_DELAYED_SCORE_SHARD_RUMBLE Already Set...")
					ENDIF
				ENDIF
			ENDIF
			IF IS_THIS_CURRENTLY_OVERTIME_TURNS()
				// Set for everyone, just incase weird server migration stuff.
				SET_BIT(iLocalBoolCheck23, LBOOL23_OVERTIME_SCORE_SHARD_ON_SCREEN)
				RESET_NET_TIMER(tdOvertimeScoredShardTimer)
				START_NET_TIMER(tdOvertimeScoredShardTimer)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_FMMC_RUMBLE_TEAM_SCORES_SHARD(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_RUMBLE_TEAM_SCORES_SHARD Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Event, SIZE_OF(Event))
		IF Event.Details.Type = SCRIPT_EVENT_FMMC_RUMBLE_TEAM_SCORES_SHARD
			TEXT_LABEL_15 tlTitle = "OVT_RM_PLA_TC"
			TEXT_LABEL_15 tlStrap = "OVT_RM_SCR"
			TEXT_LABEL_15 tlStrapTeam1 = "OVT_RM_PURP"
			TEXT_LABEL_15 tlStrapTeam2 = "OVT_RM_ORAN"
			
			PRINTLN("[LM][PROCESS_FMMC_RUMBLE_TEAM_SCORES_SHARD] - Displaying the Scores achieved on the previous round...")
			PRINTLN("[LM][PROCESS_FMMC_RUMBLE_TEAM_SCORES_SHARD] - Event Data - Team 1: ", Event.iScores[0], "  Team 2: ", Event.iScores[1])
			PRINTLN("[LM][PROCESS_FMMC_RUMBLE_TEAM_SCORES_SHARD] - MC_PlayerBD[iLocalPart].iMyTurnScore[0]: ", MC_PlayerBD[iLocalPart].iMyTurnScore[0])
			
			IF MC_PlayerBD[iLocalPart].iMyTurnScore[0] != -1
				MC_PlayerBD[iLocalPart].iOvertimePoints += MC_PlayerBD[iLocalPart].iMyTurnScore[0]
				
				PRINTLN("[RCC MISSION] iOvertimePoints += iMyTurnScore... iOvertimePoints: ", MC_PlayerBD[iLocalPart].iOvertimePoints, "  iMyTurnScore: ", MC_PlayerBD[iLocalPart].iMyTurnScore[0])
			ENDIF
						
			SETUP_NEW_BIG_MESSAGE_WITH_TWO_STRINGS_TWO_INTS(BIG_MESSAGE_GENERIC_TEXT, tlTitle, tlStrap, tlStrapTeam1, tlStrapTeam2, DEFAULT, Event.iScores[0], Event.iScores[1], HUD_COLOUR_BLACK, DEFAULT, BIG_MESSAGE_BIT_APPLY_DARKER_BG)
						
			PRINTLN("[LM][OVERTIME][RENDERPHASE_PAUSE_EVENT] - Rumble Scored - Calling to pause renderphases.")
			
			BROADCAST_FMMC_OVERTIME_PAUSE_RENDERPHASE(MC_ServerBD.iSessionScriptEventKey)
			
			SET_BIT(iLocalBoolCheck23, LBOOL23_OVERTIME_MODE_SWITCH_OBJ_BLOCK)
			SET_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
			
			RESET_NET_TIMER(tdOvertimeScoredShardTimer)
			START_NET_TIMER(tdOvertimeScoredShardTimer)			
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_EXIT_MISSION_MOC(INT iCount)
	SCRIPT_EVENT_DATA_FMMC_EXIT_MISSION_MOC Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Event, SIZE_OF(Event))
		IF Event.Details.Type = SCRIPT_EVENT_FMMC_EXIT_MISSION_MOC
		
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciALLOW_MOC_ENTRY_THIS_MISSION)
				IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehCreatorTrailer)
				AND GET_OWNER_OF_CREATOR_TRAILER(MPGlobalsAmbience.vehCreatorTrailer) = PLAYER_ID()
					IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSTwo,  BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_LEAVE_ARMORY_TRUCK_TRAILER_ON_DELIVER)
						SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSTwo,  BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_LEAVE_ARMORY_TRUCK_TRAILER_ON_DELIVER) 
						CDEBUG1LN(DEBUG_SPAWNING, "Clearing BS_SIMPLE_INTERIOR_GLOBAL_DATA_KICK_PLAYERS_OUT_OF_ARMORY_TRUCK_INTERIOR")
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_FMMC_STOLEN_FLAG_SHARD(INT iCount)
	SCRIPT_EVENT_DATA_FMMC_STOLEN_FLAG_SHARD Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Event, SIZE_OF(Event))
		IF Event.Details.Type = SCRIPT_EVENT_FMMC_STOLEN_FLAG_SHARD
			PRINTLN("[RCC MISSION][PROCESS_FMMC_STOLEN_FLAG_SHARD] Calling PROCESS_FMMC_STOLEN_FLAG_SHARD")
			PRINTLN("[RCC MISSION][PROCESS_FMMC_STOLEN_FLAG_SHARD] Team Stolen from: ", Event.iTeamStolenFrom," Player Stolen from: ",Event.iPreviousOwner, " Player Stealing: ", NATIVE_TO_INT(Event.PlayerThatStole), " Team Stealing: ", Event.iTeamThatStole)
			IF Event.iTeamStolenFrom = MC_PlayerBD[iPartToUse].iteam
				IF NETWORK_IS_PARTICIPANT_ACTIVE(NETWORK_GET_PARTICIPANT_INDEX(Event.PlayerThatStole))
					INT iStealPart
					iStealPart = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(Event.PlayerThatStole)) 
					IF iStealPart != -1
					AND MC_PlayerBD[iStealPart].iteam != MC_PlayerBD[iPartToUse].iteam
						PRINTLN("[RCC MISSION][PROCESS_FMMC_STOLEN_FLAG_SHARD] Flag stolen from team: ", Event.iTeamStolenFrom, " Stolen by player: ", NATIVE_TO_INT(Event.PlayerThatStole))
						SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GENERIC_TEXT,Event.PlayerThatStole, DEFAULT,"S_SF_PN", "S_STOLEN",GET_HUD_COLOUR_FOR_FMMC_TEAM(Event.iTeamThatStole, Event.PlayerThatStole))
						IF Event.iPreviousOwner = iLocalPart
							IF MC_PlayerBD[iLocalPart].iPackagesAtHolding > 0
								MC_PlayerBD[iLocalPart].iPackagesAtHolding--
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_FMMC_CAPTURED_FLAG_SHARD(INT iCount)
	SCRIPT_EVENT_DATA_FMMC_CAPTURED_FLAG_SHARD Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Event, SIZE_OF(Event))
		IF Event.Details.Type = SCRIPT_EVENT_FMMC_CAPTURED_FLAG_SHARD
		AND Event.Details.FromPlayerIndex != PLAYER_ID()
			TEXT_LABEL_15 sTagLine
			IF IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
			OR IS_LOCAL_PLAYER_ANY_SPECTATOR()
				sTagLine = "ACTF_STC" //A flag
			ELIF Event.iTeamThatCaptured != MC_PlayerBD[iPartToUse].iteam
				sTagLine = "ACTF_YTC" //Your flag
			ELSE
				sTagLine = "ACTF_ETC" //Enemy flag
			ENDIF
			sTagLine += Event.iTeamThatCaptured
			SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GENERIC_TEXT, Event.Details.FromPlayerIndex, DEFAULT, sTagLine, "ACTF_CAP",GET_HUD_COLOUR_FOR_FMMC_TEAM(Event.iTeamThatCaptured, Event.Details.FromPlayerIndex))
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_FMMC_RETURNED_FLAG_SHARD(INT iCount)
	SCRIPT_EVENT_DATA_FMMC_RETURNED_FLAG_SHARD Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Event, SIZE_OF(Event))
		IF Event.Details.Type = SCRIPT_EVENT_FMMC_RETURNED_FLAG_SHARD
			TEXT_LABEL_15 tlTitle = "ACTF_RTRN"
			IF Event.bUseShuntTitle
				tlTitle = "ACTF_SHNT"
			ENDIF
			TEXT_LABEL_15 tlTag
			IF Event.piPlayerThatReturned = PLAYER_ID()
				tlTag = "ACTF_SF_YS"
				tlTag += Event.iTeamThatReturned
				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, tlTitle, tlTag)
			ELIF Event.iTeamThatReturned = MC_playerBD[iPartToUse].iteam
			AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
				tlTag = "ACTF_SF_SFF"
				tlTag += Event.iTeamThatReturned
				SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GENERIC_TEXT, Event.piPlayerThatReturned, DEFAULT, tlTag, tlTitle, GET_HUD_COLOUR_FOR_FMMC_TEAM(Event.iTeamThatReturned, Event.piPlayerThatReturned)) //Your flag
			ELSE
				tlTag = "ACTF_SF_SFY"
				tlTag += Event.iTeamThatReturned
				SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GENERIC_TEXT, Event.piPlayerThatReturned, DEFAULT, tlTag, tlTitle, GET_HUD_COLOUR_FOR_FMMC_TEAM(Event.iTeamThatReturned, Event.piPlayerThatReturned)) //Their flag
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_FMMC_CCTV_SPOTTED(INT iCount)
	SCRIPT_EVENT_DATA_FMMC_CCTV_SPOTTED Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Event, SIZE_OF(Event))
	AND Event.Details.Type = SCRIPT_EVENT_FMMC_CCTV_SPOTTED
		IF bIsLocalPlayerHost
		
			INT iPlayerTeam = GET_PLAYER_TEAM(Event.PlayerSpotted)
		
			PRINTLN("[RCC MISSION][CCTV] PROCESS_FMMC_CCTV_SPOTTED - Setting SBBOOL6_PLAYER_HAS_BEEN_SPOTTED_CCTV... ", GET_PLAYER_NAME(Event.PlayerSpotted), " was spotted by a camera! Their team is: ", iPlayerTeam)
			SET_BIT(MC_ServerBD.iServerBitSet6, SBBOOL6_PLAYER_HAS_BEEN_SPOTTED_CCTV)
			REINIT_NET_TIMER(CCTVSpottedResetTimer)
			
			IF iPlayerTeam > -1
			AND iPlayerTeam < FMMC_MAX_TEAMS
			
				INT iPlayerPart = NATIVE_TO_INT(Event.PlayerSpotted)
				PRINTLN("[RCC MISSION][CCTV] PROCESS_FMMC_CCTV_SPOTTED iPlayerPart = ", iPlayerPart)
				
				IF iPlayerPart > -1
					PRINTLN("[RCC MISSION][CCTV] PROCESS_FMMC_CCTV_SPOTTED Setting MC_serverBD.iPartCausingFail[",iPlayerTeam,"] to ", iPlayerPart)
					MC_serverBD.iPartCausingFail[iPlayerTeam] = iPlayerPart
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_TriggerCCTVAggroWithoutPeds)
					SET_BIT(MC_serverBD.iServerBitSet1, SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_0 + iPlayerTeam)
					PRINTLN("[RCC MISSION][CCTV] PROCESS_FMMC_CCTV_SPOTTED | Setting SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_", iPlayerTeam)
				ENDIF
			ENDIF
		ENDIF
		
		PRINTLN("[RCC MISSION][CCTV] PROCESS_FMMC_CCTV_SPOTTED iPlayerPart = ", NATIVE_TO_INT(Event.PlayerSpotted), " iCam: ", Event.iCam)
		
		SET_BIT(iCCTVCameraSpottedPlayerBS, Event.iCam)
		
		INT iPartTriggered = -1
		IF Event.PlayerSpotted != INVALID_PLAYER_INDEX()
			iPartTriggered = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(Event.PlayerSpotted))
			iCCTVCameraSpottedPlayer[Event.iCam] = iPartTriggered
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_TriggerAllCCTVCameras)
			PRINTLN("[ML MISSION][CCTV] PROCESS_FMMC_CCTV_SPOTTED Triggering all CCTV cameras, triggered by participant ", iPartTriggered)
			TRIGGER_ALL_CCTV_CAMERAS(iPartTriggered)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_FMMC_TAGGED_ENTITY(INT iCount)
	SCRIPT_EVENT_DATA_FMMC_TAGGED_ENTITY Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Event, SIZE_OF(Event))
		IF Event.Details.Type = SCRIPT_EVENT_FMMC_TAGGED_ENTITY
			IF bIsLocalPlayerHost
				PRINTLN("[RCC MISSION][PROCESS_FMMC_STOLEN_FLAG_SHARD] Calling PROCESS_FMMC_TAGGED_ENTITY")
				SET_BIT(MC_serverBD_3.iTaggedEntityBitset, Event.iTaggedIndex)
				MC_serverBD_3.iTaggedEntityType[Event.iTaggedIndex] = Event.iTaggedEntityType
				MC_serverBD_3.iTaggedEntityIndex[Event.iTaggedIndex] = Event.iTaggedEntityIndex
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_FMMC_STOCKPILE_SOUND(INT iCount)
	SCRIPT_EVENT_DATA_FMMC_STOCKPILE_SOUND Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Event, SIZE_OF(Event))
		IF Event.Details.Type = SCRIPT_EVENT_FMMC_STOCKPILE_SOUND
			PRINTLN("[RCC MISSION][PROCESS_FMMC_STOCKPILE_SOUND] Calling PROCESS_FMMC_STOCKPILE_SOUND with sound type: ", Event.iStockpileSoundEventType, " Event.iPart:", Event.iPart)
			
			IF Event.iPart > -1
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(Event.iPart))
					PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(Event.iPart))
					INT iTeam = GET_PLAYER_TEAM(piPlayer)
					
					IF iTeam > -1
					AND iTeam < FMMC_MAX_TEAMS
						SWITCH Event.iStockpileSoundEventType
							CASE ciStockpile_Sound_Collect
								//Personal sound played on collection with broadcast
								IF DOES_TEAM_LIKE_TEAM(MC_playerBD[iPartToUse].iteam, iTeam)
									PLAY_SOUND_FRONTEND(-1,"Flag_Collected", "DLC_SM_STPI_Player_Sounds")
									PRINTLN("[RCC MISSION][PROCESS_FMMC_STOCKPILE_SOUND] Playing Collect sound for Me")
								ELSE
									PLAY_SOUND_FRONTEND(-1,"Flag_Collected", "DLC_SM_STPI_Enemy_Sounds")
									PRINTLN("[RCC MISSION][PROCESS_FMMC_STOCKPILE_SOUND] Playing Collect sound for Enemy")
								ENDIF
							BREAK
							CASE ciStockpile_Sound_Delivery
								//Personal sound played on delivery with broadcast
								IF DOES_TEAM_LIKE_TEAM(MC_playerBD[iPartToUse].iteam, iTeam)
									PLAY_SOUND_FRONTEND(-1,"Flag_Delivered", "DLC_SM_STPI_Player_Sounds")
									PRINTLN("[RCC MISSION][PROCESS_FMMC_STOCKPILE_SOUND] Playing Deliver sound for Me")
								ELSE
									PLAY_SOUND_FRONTEND(-1,"Flag_Delivered","DLC_SM_STPI_Enemy_Sounds")
									PRINTLN("[RCC MISSION][PROCESS_FMMC_STOCKPILE_SOUND] Playing Deliver sound for Enemy")
								ENDIF
							BREAK
							CASE ciStockpile_Sound_Dropped
								IF DOES_TEAM_LIKE_TEAM(MC_playerBD[iPartToUse].iteam, iTeam)
									PLAY_SOUND_FRONTEND(-1,"Flag_Dropped","DLC_SM_STPI_Player_Sounds")
									PRINTLN("[RCC MISSION][PROCESS_FMMC_STOCKPILE_SOUND] Playing Drop sound for Me")
								ELSE
									PLAY_SOUND_FRONTEND(-1,"Flag_Dropped","DLC_SM_STPI_Enemy_Sounds")
									PRINTLN("[RCC MISSION][PROCESS_FMMC_STOCKPILE_SOUND] Playing Drop sound for Enemy")
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_FMMC_LOCATE_CAPTURE_PLAYER_SCORE(INT iCount)
	SCRIPT_EVENT_DATA_FMMC_LOCATE_CAPTURE_PLAYER_SCORE Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Event, SIZE_OF(Event))
		IF Event.Details.Type = SCRIPT_EVENT_FMMC_LOCATE_CAPTURE_PLAYER_SCORE
			PRINTLN("[RCC MISSION][PROCESS_FMMC_LOCATE_CAPTURE_PLAYER_SCORE] Calling PROCESS_FMMC_LOCATE_CAPTURE_PLAYER_SCORE")
			IF LocalPlayer = event.PlayerThatOwnsLocate
			AND MC_PlayerBD[iLocalPart].iteam = event.iPlayersTeam
				PRINTLN("[RCC MISSION][PROCESS_FMMC_LOCATE_CAPTURE_PLAYER_SCORE] This is the correct player")
				MC_playerBD[iLocalPart].iPersonalLocatePoints++
				PRINTLN("[RCC MISSION][PROCESS_FMMC_LOCATE_CAPTURE_PLAYER_SCORE] Incremented MC_playerBD[iLocalPart].iPersonalLocatePoints to: ", MC_playerBD[iLocalPart].iPersonalLocatePoints)
			ELSE
				PRINTLN("[RCC MISSION][PROCESS_FMMC_LOCATE_CAPTURE_PLAYER_SCORE] This is the incorrect player to process this event. Should be player ", NATIVE_TO_INT(event.PlayerThatOwnsLocate))
			ENDIF
		ENDIF
	ENDIF
ENDPROC


//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: PED EVENTS !
//
//************************************************************************************************************************************************************



PROC PROCESS_FMMC_DIALOGUE_LOOK(INT iCount)

	SCRIPT_EVENT_DATA_FMMC_DIALOGUE_LOOK LookData
	PED_INDEX tempPed
	//BOOL bTicker = TRUE
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, LookData, SIZE_OF(LookData))

		IF LookData.Details.Type = SCRIPT_EVENT_FMMC_DIALOGUE_LOOK
			PRINTLN("[RCC MISSION] LookData.Details.FromPlayerIndex = ",NATIVE_TO_INT(LookData.Details.FromPlayerIndex))
			PRINTLN("[RCC MISSION] LookData.iped = ",LookData.iped)
			tempPed = GET_PLAYER_PED(LookData.Details.FromPlayerIndex)
			IF NOT IS_PED_INJURED(tempPed)
				DialoguePedToLookAt[LookData.iped] = tempPed
				IF DialoguePedToLookAt[LookData.iped] = NULL
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF


ENDPROC


PROC PROCESS_FMMC_PED_RETASK_HANDSHAKE(INT iCount)

	SCRIPT_EVENT_DATA_FMMC_PED_RETASK_HANDSHAKE_EVENT HandshakeData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, HandshakeData, SIZE_OF(HandshakeData))

		IF HandshakeData.Details.Type = SCRIPT_EVENT_FMMC_PED_RETASK_HANDSHAKE
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_RETASK_HANDSHAKE - HandshakeData.Details.FromPlayerIndex = ",NATIVE_TO_INT(HandshakeData.Details.FromPlayerIndex))
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_RETASK_HANDSHAKE - HandshakeData.iped = ",HandshakeData.iped)
			SET_BIT(sRetaskDirtyFlagData.iDirtyFlagPedRetaskHandshake[GET_LONG_BITSET_INDEX(HandshakeData.iped)], GET_LONG_BITSET_BIT(HandshakeData.iped))
		ENDIF
	ENDIF

ENDPROC

PROC PROCESS_FMMC_PED_GIVEN_GUNONRULE(INT iCount)
	
	IF bIsLocalPlayerHost
		
		SCRIPT_EVENT_DATA_FMMC_PED_ID ObjectiveData
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
			IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_GIVEN_GUNONRULE
				PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_GIVEN_GUNONRULE - ObjectiveData.iPedID = ",ObjectiveData.iPedid)
				SET_BIT(MC_serverBD.iPedGunOnRuleGiven[GET_LONG_BITSET_INDEX(ObjectiveData.iPedid)],GET_LONG_BITSET_BIT(ObjectiveData.iPedid))
			ENDIF
		ENDIF

	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_COMBAT_STYLE_CHANGED(INT iCount)
	
	IF bIsLocalPlayerHost
		SCRIPT_EVENT_DATA_FMMC_PED_ID ObjectiveData
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
			IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_COMBAT_STYLE_CHANGED
				PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_COMBAT_STYLE_CHANGED - ObjectiveData.iPedID = ",ObjectiveData.iPedid)
				SET_BIT(MC_serverBD.iPedCombatStyleChanged[GET_LONG_BITSET_INDEX(ObjectiveData.iPedid)],GET_LONG_BITSET_BIT(ObjectiveData.iPedid))
			ENDIF
		ENDIF

	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_WAITING_AFTER_GOTO(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_PED_ID ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_WAITING_AFTER_GOTO
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_WAITING_AFTER_GOTO - ObjectiveData.ipedid = ",ObjectiveData.ipedid)
			SET_BIT(iPedGotoWaitBitset[GET_LONG_BITSET_INDEX(ObjectiveData.ipedid)], GET_LONG_BITSET_BIT(ObjectiveData.ipedid))			
			IF IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[ObjectiveData.ipedid].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[ObjectiveData.ipedid], PED_ASSOCIATED_GOTO_TASK_TYPE_PLAY_IDLE_ANIM_ON_WAIT) 
				PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_WAITING_AFTER_GOTO - ObjectiveData.ipedid = ",ObjectiveData.ipedid, " Starting wait idle anim timer.")
				START_NET_TIMER(tdAssociatedGOTOWaitIdleAnimTimer[ObjectiveData.ipedid])
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_DONE_WAITING_AFTER_GOTO(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_PED_ID ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_DONE_WAITING_AFTER_GOTO
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_DONE_WAITING_AFTER_GOTO - ObjectiveData.ipedid = ",ObjectiveData.ipedid)
			SET_BIT(iPedGotoAssCompletedWaitBitset[GET_LONG_BITSET_INDEX(ObjectiveData.ipedid)], GET_LONG_BITSET_BIT(ObjectiveData.ipedid))			
			IF IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[ObjectiveData.ipedid].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[ObjectiveData.ipedid], PED_ASSOCIATED_GOTO_TASK_TYPE_PLAY_IDLE_ANIM_ON_WAIT) 
				RESET_NET_TIMER(tdAssociatedGOTOWaitIdleAnimTimer[ObjectiveData.ipedid])
				PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_WAITING_AFTER_GOTO - ObjectiveData.ipedid = ",ObjectiveData.ipedid, " Resetting wait idle anim timer.")
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_CLEAR_GOTO_WAITING_STATE(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_PED_ID ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_CLEAR_GOTO_WAITING_STATE
			
			INT iped = ObjectiveData.ipedid
			
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_CLEAR_GOTO_WAITING_STATE - ObjectiveData.ipedid = ",iped)
			CLEAR_BIT(iPedGotoWaitBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			CLEAR_BIT(iPedGotoAssCompletedWaitBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			CLEAR_BIT(iBSTaskAchieveHeadingWait[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_DEFENSIVE_AREA_GOTO_CLEANED_UP(INT iCount)
	
	IF bIsLocalPlayerHost
	
		SCRIPT_EVENT_DATA_FMMC_PED_ID ObjectiveData
		
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
			IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_DEFENSIVE_AREA_GOTO_CLEANED_UP
				
				INT iped = ObjectiveData.ipedid
				
				PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_DEFENSIVE_AREA_GOTO_CLEANED_UP - ObjectiveData.ipedid = ",iped)
				CLEAR_BIT(MC_serverBD.iPedDoneDefensiveAreaGoto[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_DIRTY_FLAG_SET(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_PED_ID ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))

		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_DIRTY_FLAG_SET
			
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_DIRTY_FLAG_SET - ObjectiveData.ipedid = ",ObjectiveData.ipedid)
			SET_BIT(sRetaskDirtyFlagData.iDirtyFlagPedNeedsRetask[GET_LONG_BITSET_INDEX(ObjectiveData.ipedid)], GET_LONG_BITSET_BIT(ObjectiveData.ipedid))
			
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_ANIM_STARTED_EVENT(int icount)

	SCRIPT_EVENT_DATA_FMMC_PED_ANIM_STARTED ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_ANIM_STARTED
			
			#IF IS_DEBUG_BUILD
			IF ObjectiveData.Details.FromPlayerIndex != INVALID_PLAYER_INDEX()
				INT iPartSender = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(ObjectiveData.Details.FromPlayerIndex))			
				PRINTLN("[RCC MISSION] PROCESS_FMMC_ANIM_STARTED_EVENT iPartSender: ", iPartSender, " ObjectiveData.ipedid = ",ObjectiveData.ipedid," - special ",IS_BIT_SET(ObjectiveData.iAnimStarted, 0),", breakout ",IS_BIT_SET(ObjectiveData.iAnimStarted, 1))
			ENDIF
			#ENDIF
			
			SET_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(ObjectiveData.ipedid)], GET_LONG_BITSET_BIT(ObjectiveData.ipedid))
			
			IF IS_BIT_SET(ObjectiveData.iAnimStarted, 0) // Special anim
				SET_BIT(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(ObjectiveData.ipedid)], GET_LONG_BITSET_BIT(ObjectiveData.ipedid))
			ENDIF
			
			IF IS_BIT_SET(ObjectiveData.iAnimStarted, 1) // Breakout anim
				SET_BIT(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(ObjectiveData.ipedid)], GET_LONG_BITSET_BIT(ObjectiveData.ipedid))
			ENDIF
			
		ENDIF
	ENDIF

ENDPROC

PROC PROCESS_FMMC_ANIM_STOPPED_EVENT(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_PED_ANIM_STARTED ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_ANIM_STOPPED
			
			PRINTLN("[RCC MISSION] PROCESS_FMMC_ANIM_STOPPED_EVENT ObjectiveData.ipedid = ",ObjectiveData.ipedid," - special ",IS_BIT_SET(ObjectiveData.iAnimStarted, 0),", breakout ",IS_BIT_SET(ObjectiveData.iAnimStarted, 1))
			
			CLEAR_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(ObjectiveData.ipedid)], GET_LONG_BITSET_BIT(ObjectiveData.ipedid))
			
			IF IS_BIT_SET(ObjectiveData.iAnimStarted, 0) // Special anim
				CLEAR_BIT(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(ObjectiveData.ipedid)], GET_LONG_BITSET_BIT(ObjectiveData.ipedid))
			ENDIF
			
			IF IS_BIT_SET(ObjectiveData.iAnimStarted, 1) // Breakout anim
				CLEAR_BIT(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(ObjectiveData.ipedid)], GET_LONG_BITSET_BIT(ObjectiveData.ipedid))
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_SECONDARY_ANIM_CLEANUP_EVENT(INT iEventID)
	
	SCRIPT_EVENT_DATA_FMMC_SECONDARY_ANIM_CLEANUP Event
	PRINTLN("[RCC MISSION] PROCESS_FMMC_SECONDARY_ANIM_CLEANUP_EVENT Called.")

	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
	
		IF Event.bRunCleanup = TRUE
			SET_BIT(iPedSecondaryAnimCleanupBitset[GET_LONG_BITSET_INDEX(Event.iPedNumber)], GET_LONG_BITSET_BIT(Event.iPedNumber))
			PRINTLN("[RCC MISSION] PROCESS_FMMC_SECONDARY_ANIM_CLEANUP_EVENT Setting bit iPedSecondaryAnimCleanupBitset.")
		ELSE
			CLEAR_BIT(iPedSecondaryAnimCleanupBitset[GET_LONG_BITSET_INDEX(Event.iPedNumber)], GET_LONG_BITSET_BIT(Event.iPedNumber))
			PRINTLN("[RCC MISSION] PROCESS_FMMC_SECONDARY_ANIM_CLEANUP_EVENT Clearing bit iPedSecondaryAnimCleanupBitset.")
		ENDIF

	ENDIF

ENDPROC

PROC PROCESS_FMMC_TREVOR_BODHI_BREAKOUT_SYNCH_SCENE_ID(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_PED_ID ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_TREVOR_BODHI_BREAKOUT_SYNCH_SCENE_ID
			
			PRINTLN("[RCC MISSION] PROCESS_FMMC_TREVOR_BODHI_BREAKOUT_SYNCH_SCENE_ID ObjectiveData.iPedID (iSynchScene) = ",ObjectiveData.iPedID)
			
			iLocalTrevorBodhiBreakoutSynchSceneID = ObjectiveData.iPedID
			
			IF bIsLocalPlayerHost
				MC_serverBD_1.iTrevorBodhiBreakoutSynchSceneID = ObjectiveData.iPedID
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_GO_FOUNDRY_HOSTAGE_SYNCH_SCENE_ID(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_PED_ID ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_GO_FOUNDRY_HOSTAGE_SYNCH_SCENE_ID
			
			PRINTLN("[RCC MISSION] PROCESS_FMMC_GO_FOUNDRY_HOSTAGE_SYNCH_SCENE_ID ObjectiveData.iPedID (iSynchScene) = ",ObjectiveData.iPedID)
			
			iLocalGOFoundryHostageSynchSceneID = ObjectiveData.iPedID
			PRINTLN("[RCC MISSION][HostageSync] PROCESS_FMMC_GO_FOUNDRY_HOSTAGE_SYNCH_SCENE_ID - Set iLocalGOFoundryHostageSynchSceneID as ",ObjectiveData.iPedID)
			
			IF bIsLocalPlayerHost
				MC_serverBD_1.iGOFoundryHostageSynchSceneID = ObjectiveData.iPedID
				PRINTLN("[RCC MISSION][HostageSync] PROCESS_FMMC_GO_FOUNDRY_HOSTAGE_SYNCH_SCENE_ID - Set host iGOFoundryHostageSynchSceneID as ",ObjectiveData.iPedID)
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_INVENTORY_CLEANUP(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_PED_ID ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_INVENTORY_CLEANUP
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_INVENTORY_CLEANUP ObjectiveData.ipedid = ",ObjectiveData.ipedid)
			SET_BIT(iPedInventoryCleanedUpBitset[GET_LONG_BITSET_INDEX(ObjectiveData.ipedid)], GET_LONG_BITSET_BIT(ObjectiveData.ipedid))
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_ADDED_TO_CHARM(INT iCount)

	IF bIsLocalPlayerHost
		SCRIPT_EVENT_DATA_FMMC_PED_ID ObjectiveData
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
			IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_ADDED_TO_CHARM
				PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_ADDED_TO_CHARM ObjectiveData.ipedid = ",ObjectiveData.ipedid)
				SET_BIT(MC_serverBD.iPedInvolvedInMiniGameBitSet[GET_LONG_BITSET_INDEX(ObjectiveData.ipedid)], GET_LONG_BITSET_BIT(ObjectiveData.ipedid))
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_REMOVED_FROM_CHARM(INT iCount)

	IF bIsLocalPlayerHost
		SCRIPT_EVENT_DATA_FMMC_PED_ID ObjectiveData
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
			IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_REMOVED_FROM_CHARM
				PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_REMOVED_FROM_CHARM ObjectiveData.ipedid = ",ObjectiveData.ipedid)
				CLEAR_BIT(MC_serverBD.iPedInvolvedInMiniGameBitSet[GET_LONG_BITSET_INDEX(ObjectiveData.ipedid)], GET_LONG_BITSET_BIT(ObjectiveData.ipedid))
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_ADDED_TO_CC(INT iCount)

	IF bIsLocalPlayerHost
		SCRIPT_EVENT_DATA_FMMC_PED_ADD_TO_CC ObjectiveData
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
			IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_ADDED_TO_CC
				INT iped = ObjectiveData.ipedid
				
				PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_ADDED_TO_CC ObjectiveData.ipedid = ",iped)
				
				//*NO LONGER NEEDED?*
				IF NOT IS_BIT_SET(MC_serverBD_3.sCCServerData.iPedIsNowInvolvedInCrowdControl[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					
					SET_BIT(MC_serverBD_3.sCCServerData.iPedIsNowInvolvedInCrowdControl[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					
					IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[ObjectiveData.iTeam],g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPriority[ObjectiveData.iTeam])
						PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_ADDED_TO_CC - Adding crowd ped to crowd control & setting midpoint iObjectiveMidPointBitset for team ",MC_playerBD[iPartToUse].iTeam,", rule ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPriority[MC_playerBD[iPartToUse].iTeam])
						SET_BIT(MC_serverBD.iObjectiveMidPointBitset[ObjectiveData.iTeam],g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPriority[ObjectiveData.iTeam])
						SET_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_CC_PED_BEATDOWN(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_CC_PED_BEATDOWN EventData
	IF GET_EVENT_DATA( SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF( EventData ) )
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_CC_PED_BEATDOWN
			// bug 2107584 - removed as migration of peds meant this event could be completely missed
			//AND NETWORK_HAS_CONTROL_OF_NETWORK_ID( EventData.niPed )
			PRINTLN("[RCC MISSION] PROCESS_FMMC_CC_PED_BEATDOWN - EventData.iCrowdNum = ", EventData.iCrowdNum)
			SET_BIT_ENUM( sCCLocalPedData[EventData.iCrowdNum].iBitSet, CCLOCALPEDDATA_BeatdownFired )
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_CC_PED_BEATDOWN_CLEAR(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_CC_PED_BEATDOWN EventData
	IF GET_EVENT_DATA( SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF( EventData ) )
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_CC_PED_BEATDOWN_CLEAR
			PRINTLN("[RCC MISSION] PROCESS_FMMC_CC_PED_BEATDOWN_CLEAR - EventData.iCrowdNum = ", EventData.iCrowdNum)
			CLEAR_BIT_ENUM( sCCLocalPedData[EventData.iCrowdNum].iBitSet, CCLOCALPEDDATA_BeatdownFired )
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_CC_PED_BEATDOWN_HIT(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_BEATDOWN_HIT EventData
	IF GET_EVENT_DATA( SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF( EventData ) )
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_CC_PED_BEATDOWN_HIT

			// Decrement number of hits remaining
			IF bIsLocalPlayerHost
				PRINTLN("[RCC MISSION] PROCESS_FMMC_CC_PED_BEATDOWN_HIT iNumHitsRemaining = ", MC_serverBD_3.sCCServerData.iNumHitsRemaining - EventData.iChange, "(", MC_serverBD_3.sCCServerData.iNumHitsRemaining, ") - iChange = ", EventData.iChange )
				MC_serverBD_3.sCCServerData.iNumHitsRemaining += EventData.iChange
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_CC_FAIL(INT iCount)
	
	// Only set if not already set
	IF sCCLocalData.eFailReason = CCFAIL_NONE
	
		SCRIPT_EVENT_DATA_FMMC_CC_FAIL EventData
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
			IF EventData.Details.Type = SCRIPT_EVENT_FMMC_CC_FAIL
				CPRINTLN( DEBUG_MIKE, "[RCC MISSION] PROCESS_FMMC_CC_FAIL - EventData.eFailReason = ", EventData.iFailReason, " EventData.iPedCausedBy = ", EventData.iPedCausedBy, " EventData.timeDelay = ", NATIVE_TO_INT( EventData.timeDelay ) )
				sCCLocalData.eFailReason 		= INT_TO_ENUM( CROWD_CONTROL_FAIL_REASON, EventData.iFailReason )
				sCCLocalData.iFailCausedByPed 	= EventData.iPedCausedBy
				sCCLocalData.timeFailDelay		= EventData.timeDelay
				IF bIsLocalPlayerHost
					SET_BIT_ENUM( MC_serverBD_3.sCCServerData.iBitSet, CCDATA_Failed )
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_CC_DIALOGUE_CLIENT_TO_HOST(INT iCount)
	SCRIPT_EVENT_DATA_FMMC_CC_DIALOGUE_CLIENT_TO_HOST EventData
	IF GET_EVENT_DATA( SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF( EventData ) )
		IF bIsLocalPlayerHost
			IF EventData.Details.Type = SCRIPT_EVENT_FMMC_CC_DIALOGUE_CLIENT_TO_HOST
			
				INT i
				REPEAT FMMC_MAX_CROWD_PEDS i
					CROWD_CONTROL_DIALOGUE eReceivedDialogue = INT_TO_ENUM( CROWD_CONTROL_DIALOGUE, GET_PACKED_BITFIELD_VALUE_FROM_ARRAY( EventData.iConversationsBitField, i, ciCC_DialogueDataBitWidth ) )
					
					// Check we have recieved something for this ped
					// and that we don't have anything already pending (this should never happen but just to be safe)
					IF eReceivedDialogue != CCDIA_NONE
					AND sCCLocalPedData[i].eDialogueClientToHost = CCDIA_NONE
					
						// Update local ped data with new dialogue and the state of the critical flag
						sCCLocalPedData[i].eDialogueClientToHost = eReceivedDialogue
						IF IS_BIT_SET( EventData.iConversationPainFlags, i )
							SET_BIT_ENUM( sCCLocalPedData[i].iBitSet, CCLOCALPEDDATA_DialogueClientToHostForcePain )
						ELSE
							CLEAR_BIT_ENUM( sCCLocalPedData[i].iBitSet, CCLOCALPEDDATA_DialogueClientToHostForcePain )
						ENDIF
						
						IF IS_BIT_SET( EventData.iConversationCriticalFlags, i )
							SET_BIT_ENUM( sCCLocalPedData[i].iBitSet, CCLOCALPEDDATA_DialogueClientToHostForceCritical )
						ELSE
							CLEAR_BIT_ENUM( sCCLocalPedData[i].iBitSet, CCLOCALPEDDATA_DialogueClientToHostForceCritical )
						ENDIF
						
					ENDIF
					
				ENDREPEAT
				
				PRINTLN("[RCC MISSION] PROCESS_FMMC_CC_DIALOGUE_CLIENT_TO_HOST")
				
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_FMMC_CC_DIALOGUE_HOST_TO_CLIENT(INT iCount)
	SCRIPT_EVENT_DATA_FMMC_CC_DIALOGUE_HOST_TO_CLIENT EventData
	IF GET_EVENT_DATA( SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF( EventData ) )
		IF NOT bIsLocalPlayerHost
			IF EventData.Details.Type = SCRIPT_EVENT_FMMC_CC_DIALOGUE_HOST_TO_CLIENTS
			
				INT i
				REPEAT FMMC_MAX_CROWD_PEDS i
					CROWD_CONTROL_DIALOGUE eReceivedDialogue = INT_TO_ENUM( CROWD_CONTROL_DIALOGUE, GET_PACKED_BITFIELD_VALUE_FROM_ARRAY( EventData.iConversationsBitField, i, ciCC_DialogueDataBitWidth ) )
					
					// Check we have recieved something for this ped
					// and that we don't have anything already pending (this should never happen but just to be safe)
					IF eReceivedDialogue != CCDIA_NONE
					AND sCCLocalPedData[i].eDialogueHostToClient = CCDIA_NONE
					
						// Update local ped data with new dialogue and the state of the critical flag
						sCCLocalPedData[i].eDialogueHostToClient = eReceivedDialogue
						// Update ped pain flag
						IF IS_BIT_SET( EventData.iConversationPainAudioFlags, i )
							SET_BIT_ENUM( sCCLocalPedData[i].iBitSet, CCLOCALPEDDATA_DialogueHostToClientPain )
						ELSE
							CLEAR_BIT_ENUM( sCCLocalPedData[i].iBitSet, CCLOCALPEDDATA_DialogueHostToClientPain )
						ENDIF
						
					ENDIF
					
				ENDREPEAT
				
				PRINTLN("[RCC MISSION] PROCESS_FMMC_CC_DIALOGUE_HOST_TO_CLIENT")
				
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_FMMC_CC_AWARD(INT iCount)

	STRUCT_EVENT_COMMON_DETAILS EventData
	IF GET_EVENT_DATA( SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF( EventData ) )
	
		IF EventData.Type = SCRIPT_EVENT_FMMC_CC_AWARD

			IF iSpectatorTarget = -1				
				INCREMENT_MP_INT_CHARACTER_AWARD(MP_AWARD_CONTROL_CROWDS)
				PRINTLN("[RCC MISSION] PROCESS_FMMC_CC_AWARD")
			ELSE
				PRINTLN("[RCC MISSION] Failed to PROCESS_FMMC_CC_AWARD")
			ENDIF
			
		ENDIF
		
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_CC_DEAD(INT iCount)

	SCRIPT_EVENT_DATA_FMMC_CC_DEAD EventData
	IF GET_EVENT_DATA( SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF( EventData ) )
	
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_CC_AWARD

			IF EventData.iCrowdPed >= 0 AND EventData.iCrowdPed < FMMC_MAX_CROWD_PEDS
				sCCLocalPedData[EventData.iCrowdPed].timeDied = GET_NETWORK_TIME()
				SET_BIT_ENUM( sCCLocalPedDecorCopy[EventData.iCrowdPed].iBitSet, CCPEDDATA_Dead )
				PRINTLN("[RCC MISSION] PROCESS_FMMC_CC_DEAD")
			ENDIF
			
		ENDIF
		
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_REMOVE_PED_FROM_GROUP(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_PED_ID ObjectiveData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_REMOVE_PED_FROM_GROUP
			
			INT iped = ObjectiveData.iPedid
			PRINTLN("[RCC MISSION] PROCESS_FMMC_REMOVE_PED_FROM_GROUP - ObjectiveData.ipedid = ",iped)
			
			IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
			AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iped])
				PRINTLN("[RCC MISSION] PROCESS_FMMC_REMOVE_PED_FROM_GROUP - Remote remove ped from group, ped ",iped)
				
				REMOVE_FMMC_PED_FROM_GROUP(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped]), iped)
				
				IF ObjectiveData.Details.FromPlayerIndex != PLAYER_ID() // Unlikely that control will have migrated back, but worth a check
					BROADCAST_FMMC_PED_SUCCESSFULLY_REMOVED_FROM_GROUP(iped, ObjectiveData.Details.FromPlayerIndex)
				ELSE
					CLEAR_BIT(MC_playerBD[iPartToUse].iPedFollowingBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				ENDIF
				
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_SUCCESSFULLY_REMOVED_FROM_GROUP(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_PED_ID ObjectiveData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_SUCCESSFULLY_REMOVED_FROM_GROUP
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_SUCCESSFULLY_REMOVED_FROM_GROUP - ObjectiveData.ipedid = ",ObjectiveData.iPedid)
			CLEAR_BIT(MC_playerBD[iPartToUse].iPedFollowingBitset[GET_LONG_BITSET_INDEX(ObjectiveData.iPedid)], GET_LONG_BITSET_BIT(ObjectiveData.iPedid))
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_HEARING(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_PED_ID ObjectiveData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_HEARING
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_HEARING - ObjectiveData.ipedid = ",ObjectiveData.ipedid)
			
			#IF IS_DEBUG_BUILD
			IF IS_NET_PLAYER_OK(ObjectiveData.Details.FromPlayerIndex, FALSE, FALSE)
				PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_HEARING - Sent by part ",NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(ObjectiveData.Details.FromPlayerIndex)))
			ENDIF
			#ENDIF
			
			SET_BIT(iPedHeardBitset[GET_LONG_BITSET_INDEX(ObjectiveData.ipedid)], GET_LONG_BITSET_BIT(ObjectiveData.ipedid))
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_SPOOKED(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_PED_SPOOKED ObjectiveData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_SPOOKED
			INT iped = ObjectiveData.iPed
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_SPOOKED - ObjectiveData.ipedid = ",iped)
			
			#IF IS_DEBUG_BUILD
			IF IS_NET_PLAYER_OK(ObjectiveData.Details.FromPlayerIndex, FALSE, FALSE)
				PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_SPOOKED - Sent by part ",NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(ObjectiveData.Details.FromPlayerIndex)))
				PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_SPOOKED - Sent by player: ", GET_PLAYER_NAME(ObjectiveData.Details.FromPlayerIndex))
			ENDIF
			#ENDIF
			
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] PROCESS_FMMC_PED_SPOOKED - ObjectiveData.iSpookedReason = ", ObjectiveData.iSpookedReason)
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] PROCESS_FMMC_PED_SPOOKED - ObjectiveData.iPartCausingFail = ", ObjectiveData.iPartCausingFail)
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] PROCESS_FMMC_PED_SPOOKED - MC_serverBD_2.iPedState[iped] = ", MC_serverBD_2.iPedState[iped])
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] PROCESS_FMMC_PED_SPOOKED - g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFlee = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFlee)
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] PROCESS_FMMC_PED_SPOOKED - g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim)
			
			#IF IS_DEBUG_BUILD
				IF ObjectiveData.iPartCausingFail != -1
					PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_SPOOKED - Player causing fail: ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(ObjectiveData.iPartCausingFail)))
				ENDIF
			#ENDIF
			
			#IF IS_DEBUG_BUILD 
			IF NOT IS_BIT_SET(iPedLocalSpookedBitset[GET_LONG_BITSET_INDEX(iPed)],GET_LONG_BITSET_BIT(iPed))
				SET_PED_IS_SPOOKED_DEBUG(iPed, DEFAULT, ObjectiveData.iSpookedReason)
			ENDIF
			#ENDIF
			
			SET_BIT(iPedLocalSpookedBitset[GET_LONG_BITSET_INDEX(iPed)],GET_LONG_BITSET_BIT(iPed))
			
			IF bIsLocalPlayerHost
			AND NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
				PED_INDEX tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPed])				
				
				IF MC_serverBD_2.iPedState[iped] != ciTASK_FLEE
					IF NOT DOES_PED_HAVE_VALID_GOTO(iped)
						IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFlee = ciPED_FLEE_ON
							IF MC_serverBD_2.iPartPedFollows[iped] =-1
								IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
								AND SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_SYNCHRONIZED_SCENE, TRUE)
								OR NOT DOES_ANIMATION_NEED_SPECIAL_BREAKOUT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim)
									SET_PED_STATE(iped,ciTASK_FLEE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//Aggro:
				IF ObjectiveData.iSpookedReason != ciSPOOK_NONE
					IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour,ciPED_BSFour_StayInTasks_On_Aggro)
						IF ObjectiveData.iSpookedReason = ciSPOOK_PLAYER_TOUCHING_ME
						OR ObjectiveData.iSpookedReason = ciSPOOK_PLAYER_HEARD
							TRIGGER_SERVER_AGGRO_HUNTING_ON_PED(tempPed, iped, ObjectiveData.iPartCausingFail)
						ELIF ObjectiveData.iSpookedReason = ciSPOOK_SEEN_BODY
							TRIGGER_SERVER_AGGRO_DEAD_BODY_RESPONSE_ON_PED(tempPed, iped, ObjectiveData.iPartCausingFail)
						ELSE
							TRIGGER_AGGRO_ON_PED(tempPed, iped, ObjectiveData.iPartCausingFail)
						ENDIF
					ELSE
						TRIGGER_AGGRO_ON_PED(tempPed, iped, ObjectiveData.iPartCausingFail)
					ENDIF
				ENDIF
				
			ENDIF
					
			SET_AGGRO_HELP_TEXT_DATA(ObjectiveData.iSpookedReason, ObjectiveData.iPartCausingFail, iPed)
			
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_UNSPOOKED(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_PED_ID ObjectiveData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_UNSPOOKED
			PRINTLN("[RCC MISSION][SpookAggro] PROCESS_FMMC_PED_UNSPOOKED - ObjectiveData.ipedid = ",ObjectiveData.ipedid)
			
			#IF IS_DEBUG_BUILD
			IF IS_NET_PLAYER_OK(ObjectiveData.Details.FromPlayerIndex, FALSE, FALSE)
				PRINTLN("[RCC MISSION][SpookAggro] PROCESS_FMMC_PED_UNSPOOKED - Sent by part ",NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(ObjectiveData.Details.FromPlayerIndex)))
			ENDIF
			#ENDIF
			
			INT iped = ObjectiveData.ipedid
			
			CLEAR_BIT(iPedLocalSpookedBitset[GET_LONG_BITSET_INDEX(iped)],GET_LONG_BITSET_BIT(iped))
			CLEAR_BIT(iPedZoneAggroed[GET_LONG_BITSET_INDEX(iped)],GET_LONG_BITSET_BIT(iped))
			CLEAR_BIT(iPedHeardBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			
			IF bIsLocalPlayerHost
				IF IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					//Unspooked:
					CLEAR_BIT(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)],GET_LONG_BITSET_BIT(iped))
					
					IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
						PROCESS_PED_TASK_NOTHING_BRAIN(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped]),iped)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_SPOOK_HURTORKILLED(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_PED_ID ObjectiveData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_SPOOK_HURTORKILLED
			
			INT iped = ObjectiveData.ipedid
			
			PRINTLN("[RCC MISSION][SpookAggro] PROCESS_FMMC_PED_SPOOK_HURTORKILLED - ObjectiveData.ipedid = ",iped)
			
			#IF IS_DEBUG_BUILD
			IF IS_NET_PLAYER_OK(ObjectiveData.Details.FromPlayerIndex, FALSE, FALSE)
				PRINTLN("[RCC MISSION][SpookAggro] PROCESS_FMMC_PED_SPOOK_HURTORKILLED - Sent by part ",NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(ObjectiveData.Details.FromPlayerIndex)))
			ENDIF
			#ENDIF
			
			SET_BIT(iPedLocalReceivedEvent_HurtOrKilled[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			
			IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
			AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iped])
				
				INT iTeamLoop
				INT iAggroTeam = -1
				
				FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
					IF SHOULD_AGRO_FAIL_FOR_TEAM(iped, iTeamLoop)
						iAggroTeam = iTeamLoop
						iTeamLoop = MC_serverBD.iNumberOfTeams // Break out!
					ENDIF
				ENDFOR
				
				IF iAggroTeam != -1
					PED_INDEX tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
					
					PROCESS_PED_BODY_AGGRO_INIT(tempPed, iped, iAggroTeam, IS_PED_IN_ANY_VEHICLE(tempPed), TRUE)
				ELSE
					SET_BIT(iPedLocalSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					BROADCAST_FMMC_PED_SPOOKED(iped)
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_GIVEN_SPOOKED_GOTO_TASK(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_PED_ID ObjectiveData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
	AND ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_GIVEN_SPOOKED_GOTO_TASK
		
		INT iped = ObjectiveData.ipedid
		
		PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_GIVEN_SPOOKED_GOTO_TASK - ObjectiveData.ipedid = ",iped)
		
		#IF IS_DEBUG_BUILD
		IF IS_NET_PLAYER_OK(ObjectiveData.Details.FromPlayerIndex, FALSE, FALSE)
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_GIVEN_SPOOKED_GOTO_TASK - Sent by part ",NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(ObjectiveData.Details.FromPlayerIndex)))
		ENDIF
		#ENDIF
		
		IF NOT IS_BIT_SET(iPedGivenSpookedGotoTaskBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_GIVEN_SPOOKED_GOTO_TASK - Set iPedGivenSpookedGotoTaskBS for ped ",iped)
			SET_BIT(iPedGivenSpookedGotoTaskBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_GIVEN_SPOOKED_GOTO_TASK - Bit already set for ped ",iped)
		#ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_GIVEN_UNSPOOKED_GOTO_TASK(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_PED_ID ObjectiveData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
	AND ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_GIVEN_UNSPOOKED_GOTO_TASK
		
		INT iped = ObjectiveData.ipedid
		
		PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_GIVEN_UNSPOOKED_GOTO_TASK - ObjectiveData.ipedid = ",iped)
		
		#IF IS_DEBUG_BUILD
		IF IS_NET_PLAYER_OK(ObjectiveData.Details.FromPlayerIndex, FALSE, FALSE)
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_GIVEN_UNSPOOKED_GOTO_TASK - Sent by part ",NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(ObjectiveData.Details.FromPlayerIndex)))
		ENDIF
		#ENDIF
		
		IF IS_BIT_SET(iPedGivenSpookedGotoTaskBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_GIVEN_UNSPOOKED_GOTO_TASK - Clear iPedGivenSpookedGotoTaskBS for ped ",iped)
			CLEAR_BIT(iPedGivenSpookedGotoTaskBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_GIVEN_UNSPOOKED_GOTO_TASK - Bit already not set for ped ",iped)
		#ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_UPDATE_TASKED_GOTO_PROGRESS(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_UPDATE_TASKED_GOTO_PROGRESS ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
	AND ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_UPDATE_TASKED_GOTO_PROGRESS
		
		INT iped = ObjectiveData.ipedid
		
		PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_UPDATE_TASKED_GOTO_PROGRESS - ObjectiveData.ipedid = ",iped)
		
		#IF IS_DEBUG_BUILD
		IF IS_NET_PLAYER_OK(ObjectiveData.Details.FromPlayerIndex, FALSE, FALSE)
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_UPDATE_TASKED_GOTO_PROGRESS - Sent by part ",NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(ObjectiveData.Details.FromPlayerIndex)))
		ENDIF
		#ENDIF
		
		IF iPedTaskedGotoProgress[iped] != ObjectiveData.iGotoProgress
			
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_UPDATE_TASKED_GOTO_PROGRESS - Update ped ",iped," iPedTaskedGotoProgress to ",ObjectiveData.iGotoProgress)
			iPedTaskedGotoProgress[iped] = ObjectiveData.iGotoProgress
			
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_UPDATE_TASKED_GOTO_PROGRESS - Ped ",iped," tasked goto progress already set to ",ObjectiveData.iGotoProgress)
		#ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_AGGROED(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_AGGROED_PED ObjectiveData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_AGGROED
			
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_AGGROED - ObjectiveData.iPedID = ",ObjectiveData.iPedID)
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_AGGROED - ObjectiveData.iPartCausingFail = ",ObjectiveData.iPartCausingFail)
			IF IS_NET_PLAYER_OK(ObjectiveData.Details.FromPlayerIndex, FALSE, FALSE)
				PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_AGGROED - Sent by part ",NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(ObjectiveData.Details.FromPlayerIndex)))
			ENDIF
			
			INT iPed = ObjectiveData.iPedID
			
			IF IS_BIT_SET( iPedHitByStunGun[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
				PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_AGGROED - Blocked by stungun for  ", iped)
				EXIT
			ENDIF
			
			SET_BIT(iPedPerceptionBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			CLEAR_BIT(iPedHeardBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			
			PED_INDEX tempPed
			
			IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
				tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
				
				IF NETWORK_HAS_CONTROL_OF_ENTITY(tempPed)
				AND ObjectiveData.iPartCausingFail = -3 // This means the ped hasn't been tasked yet
					TRIGGER_AGGRO_TASKS_ON_PED(tempPed, iPed, ciSPOOK_NONE, FALSE)
				ENDIF
				
				IF IS_PED_IN_COMBAT(tempPed)
					SET_BIT(iLocalBoolCheck31, LBOOL31_AGGROD_SOMEONE_INTO_COMBAT_AT_SOME_POINT)
					PRINTLN("PROCESS_FMMC_PED_AGGROED - Setting LBOOL31_AGGROD_SOMEONE_INTO_COMBAT_AT_SOME_POINT now due to ped ", iPed)
				ENDIF
			ENDIF
			
			IF bIsLocalPlayerHost
			AND (ObjectiveData.iPartCausingFail >= -1) //-1 and above mean that the server hasn't handled this stuff yet:
			AND (ObjectiveData.iPedID > -1)
				TRIGGER_AGGRO_SERVERDATA_ON_PED(tempPed, ObjectiveData.iPedID, ObjectiveData.iPartCausingFail)
			ENDIF
			
			#IF IS_DEBUG_BUILD 
			IF NOT IS_BIT_SET(iPedLocalAggroedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				SET_PED_IS_AGGROD_DEBUG(iPed)
			ENDIF
			#ENDIF
			
			SET_BIT(iPedLocalAggroedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			
			IF ObjectiveData.bZoneForced
				PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_AGGROED - iPed = ", iPed, " ObjectiveData.bZoneForced: ", ObjectiveData.bZoneForced)
				SET_BIT(iPedZoneAggroed[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_SPEECH_AGGROED(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_AGGROED_PED ObjectiveData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_SPEECH_AGGROED
			
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_AGGROED - ObjectiveData.iPedID = ",ObjectiveData.iPedID)
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_AGGROED - ObjectiveData.iPartCausingFail = ",ObjectiveData.iPartCausingFail)
			IF IS_NET_PLAYER_OK(ObjectiveData.Details.FromPlayerIndex, FALSE, FALSE)
				PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_AGGROED - Sent by part ",NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(ObjectiveData.Details.FromPlayerIndex)))
			ENDIF
			
			INT iPed = ObjectiveData.iPedID
			
			SET_BIT(iPedPerceptionBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			CLEAR_BIT(iPedHeardBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			
			PED_INDEX tempPed
			
			IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
				tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
				
				IF NETWORK_HAS_CONTROL_OF_ENTITY(tempPed)
				AND ObjectiveData.iPartCausingFail = -3 // This means the ped hasn't been tasked yet
					TRIGGER_AGGRO_TASKS_ON_PED(tempPed, iPed, ciSPOOK_NONE, FALSE)
				ENDIF
			ENDIF
			
			IF bIsLocalPlayerHost
			AND (ObjectiveData.iPartCausingFail >= -1) //-1 and above mean that the server hasn't handled this stuff yet:
			AND (ObjectiveData.iPedID > -1)
				TRIGGER_SPEECH_AGGRO_SERVERDATA_ON_PED(tempPed, ObjectiveData.iPedID, ObjectiveData.iPartCausingFail)
			ENDIF
			
			SET_BIT(iPedLocalAggroedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_CANCEL_TASKS(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_PED_ID ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_CANCEL_TASKS
			INT iped = ObjectiveData.ipedid
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_CANCEL_TASKS - ObjectiveData.ipedid = ",iped)
			IF IS_NET_PLAYER_OK(ObjectiveData.Details.FromPlayerIndex, FALSE, FALSE)
				PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_CANCEL_TASKS - Sent by part ",NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(ObjectiveData.Details.FromPlayerIndex)))
			ENDIF
			
			SET_BIT(iPedLocalCancelTasksBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			
			IF bIsLocalPlayerHost
				PROCESS_SERVER_PED_CANCEL_TASKS(iped)
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_SPEECH_AGRO_BITSET(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_PED_ID ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_SPEECH_AGRO_BITSET
			
			INT iped = ObjectiveData.ipedid
			
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_SPEECH_AGRO_BITSET - ObjectiveData.ipedid = ",iped)
			SET_BIT(iPedSpeechAgroBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PRI_STA_IG2_AUDIOSTREAM(INT iCount)
	SCRIPT_EVENT_DATA_FMMC_PED_ID EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_PRI_STA_IG2_AUDIOSTREAM
			IF EventData.iPedid != -1
			AND NETWORK_DOES_NETWORK_ID_EXIST( MC_serverBD_1.sFMMC_SBD.niPed[EventData.iPedid] )
				PRINTLN("[RCC MISSION] PROCESS_FMMC_PRI_STA_IG2_AUDIOSTREAM set ped = ",EventData.iPedid," to play audio stream MPH_PRI_STA_IG2")
				i_Pri_Sta_IG2_AudioStreamPedID = EventData.iPedid
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_TREVOR_COVER_CONVERSTION_TRIGGERED(INT iEventID)

	SCRIPT_EVENT_DATA_FMMC_TREVOR_COVER_CONVERSATION_TRIGGERED Event
	
	PRINTLN("[RCC MISSION] - SCRIPT_EVENT_DATA_FMMC_TREVOR_COVER_CONVERSATION_TRIGGERED - has been received.")

	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
	
		PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_TREVOR_COVER_CONVERSTION_TRIGGERED - Event.iPed ", Event.iPed)
		PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_TREVOR_COVER_CONVERSTION_TRIGGERED - Event.iConversationToPlay ", Event.iConversationToPlay)
		PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_TREVOR_COVER_CONVERSTION_TRIGGERED - Event.iRandomAnimation ", Event.iRandomAnimation)
		
		PED_INDEX tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[Event.iPed])
		
		ADD_PED_FOR_DIALOGUE(speechPedStruct, 2, tempPed, "TREVOR")

		SWITCH Event.iConversationToPlay
		
			CASE 1
				CREATE_CONVERSATION(speechPedStruct, "NAFINAU", "NAFIN_READY2", CONV_PRIORITY_MEDIUM)
			BREAK
		
			CASE 2
				PLAY_SINGLE_LINE_FROM_CONVERSATION_USING_V_CONTENT_IN_DLC(speechPedStruct, "FB3AUD", "F3_WREN2", "F3_WREN2_1", CONV_PRIORITY_MEDIUM)
			BREAK
			
			CASE 3
				CREATE_CONVERSATION_USING_V_CONTENT_IN_DLC(speechPedStruct, "RBH2AUD", "RBH2A_READYT", CONV_PRIORITY_MEDIUM)
			BREAK
			
			CASE 4
				CREATE_CONVERSATION_USING_V_CONTENT_IN_DLC(speechPedStruct, "D2AAUD", "DS2A_COMON2", CONV_PRIORITY_MEDIUM)
			BREAK
			
			CASE 5
				CREATE_CONVERSATION_USING_V_CONTENT_IN_DLC(speechPedStruct, "T1M1AUD", "T1M1_P20", CONV_PRIORITY_MEDIUM)
			BREAK
			
			CASE 6
			DEFAULT
				CREATE_CONVERSATION_USING_V_CONTENT_IN_DLC(speechPedStruct, "FB3AUD", "F3_TTP_GRUNT", CONV_PRIORITY_MEDIUM)
			BREAK
			
		ENDSWITCH

		iTrevorAnimsRandom = Event.iRandomAnimation
		iTrevorAnimsCount++
		
		PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_TREVOR_COVER_CONVERSTION_TRIGGERED - iTrevorAnimsCount ", iTrevorAnimsCount)
	ENDIF

ENDPROC

PROC PROCESS_FMMC_REMOTELY_ADD_RESPAWN_POINTS(INT iCount)

	SCRIPT_EVENT_DATA_FMMC_REMOTELY_ADD_RESPAWN_POINTS EventData

	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_REMOTELY_ADD_RESPAWN_POINTS
			IF EventData.iTeam = MC_PlayerBD[iLocalPart].iTeam
			OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[EventData.iTeam][EventData.iRespawnPoint].iSpawnBitSet, ci_SpawnBS_ActivateAllTeams)
				SET_BIT(iBSRespawnActiveFromRadius[MC_PlayerBD[iLocalPart].iTeam][GET_LONG_BITSET_INDEX(EventData.iRespawnPoint)], GET_LONG_BITSET_BIT(EventData.iRespawnPoint))
				CLEAR_BIT(iBSRespawnDeactivated[MC_PlayerBD[iLocalPart].iTeam][GET_LONG_BITSET_INDEX(EventData.iRespawnPoint)], GET_LONG_BITSET_BIT(EventData.iRespawnPoint))
				SET_BIT(iLocalBoolCheck28, LBOOL28_OTHER_TEAM_ACTIVATED_MY_RESPAWN_POINTS)
				IF EventData.bDeactivateOthers
					iRespawnPointIndexToKeep = EventData.iRespawnPoint
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC


//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: MINIGAME EVENTS !
//
//************************************************************************************************************************************************************



PROC PROCESS_FMMC_START_SYNC_LOCK_HACK_TIMER(INT iCount)

	SCRIPT_EVENT_DATA_FMMC_START_SYNC_LOCK_HACK_TIMER EventData

	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))

		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_START_SYNC_LOCK_HACK_TIMER
			PRINTLN("[RCC MISSION][HumaneSyncLock] EventData.Details.FromPlayerIndex = ",NATIVE_TO_INT(EventData.Details.FromPlayerIndex))
			iSyncLockCount[EventData.iteam]++
			
			IF NOT HAS_NET_TIMER_STARTED(tdSyncTimer[EventData.iteam])
			
				IF EventData.Details.FromPlayerIndex != LocalPlayer
					CLEAR_BIT(iLocalBoolCheck3, LBOOL3_ORIGINAL_SYNC_LOCK_PLAYER)
					PRINTLN("[RCC MISSION][HumaneSyncLock] Clearing LBOOL3_ORIGINAL_SYNC_LOCK_PLAYER because I didn't send the initial event")
				ENDIF
				
				IF MC_playerBD[iPartToUse].iSyncAnimProgress > 0
					REINIT_NET_TIMER(tdSyncTimer[EventData.iteam])
					PRINTLN("[RCC MISSION][HumaneSyncLock] - starting hack timer for syn lock")
				ELSE
					PRINTLN("[RCC MISSION][HumaneSyncLock] - Not starting hack timer for syn lock because I'm not involved")
				ENDIF
			ENDIF
		ENDIF
	ENDIF


ENDPROC

PROC PROCESS_HEIST_BEGIN_HACK_MINIGAME(INT iCount)

	IF bIsLocalPlayerHost
	
		SCRIPT_EVENT_DATA_HEIST_BEGIN_HACK_MINIGAME EventData
		
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
		
			IF EventData.Details.Type = SCRIPT_EVENT_HEIST_BEGIN_HACK_MINIGAME
			
				IF MC_serverBD_1.iPlayerForHackingMG = -1
				AND NOT IS_BIT_SET(MC_serverBD_1.iServerHackMGBitset, SBBOOL_HACK_FIND_PLAYER)
					PROCESS_SERVER_CIRCUIT_HACK_MG_INIT(EventData.iDialogueCausingHack)		
				ENDIF
				PRINTLN("    ----->    PROCESS_HEIST_BEGIN_HACK_MINIGAME - SENT FROM ", GET_PLAYER_NAME(EventData.Details.FromPlayerIndex))
			ENDIF
		ENDIF
	
	ENDIF

ENDPROC

PROC PROCESS_HEIST_END_HACK_MINIGAME(INT iCount)

	IF bIsLocalPlayerHost
	
		SCRIPT_EVENT_DATA_HEIST_END_HACK_MINIGAME EventData
		
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
		
			IF EventData.Details.Type = SCRIPT_EVENT_HEIST_END_HACK_MINIGAME
				
				PROCESS_SERVER_CIRCUIT_HACK_MG_END(EventData.iTeam, EventData.bPass)
				
				PRINTLN("    ----->    PROCESS_HEIST_END_HACK_MINIGAME - SENT FROM ", GET_PLAYER_NAME(EventData.Details.FromPlayerIndex))
			ENDIF
		ENDIF
	
	ENDIF

ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_DRILL_ASSET(INT iEventID)
	IF bIsLocalPlayerHost
		SCRIPT_EVENT_DATA_FMMC_DRILL_ASSET Event
		
		PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_FMMC_DRILL_ASSET - has been received.")

		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
			IF Event.iEventType = ciEVENT_DRILL_ASSET_REQUEST
				SET_BIT(MC_serverBD.iServerBitSet2, SBBOOL2_HEIST_DRILL_ASSET_REQUEST)
			ELSE
				CLEAR_BIT(MC_serverBD.iServerBitSet2, SBBOOL2_HEIST_DRILL_ASSET_REQUEST)
			ENDIF

			PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_FMMC_DRILL_ASSET - iEventType =  ", Event.iEventType)
		ENDIF
	ENDIF
ENDPROC


PROC PROCESS_SCRIPT_EVENT_VAULT_DRILL_ASSET(INT iEventID)
	IF bIsLocalPlayerHost
		SCRIPT_EVENT_DATA_VAULT_DRILL_ASSET Event
		
		PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_VAULT_DRILL_ASSET - has been received.")

		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
			IF Event.iEventType = ciEVENT_VAULT_DRILL_ASSET_REQUEST
			OR Event.iEventType = ciEVENT_VAULT_DRILL_ASSET_REQUEST_LASER
				SET_BIT(MC_serverBD.iServerBitSet8, SBBOOL8_VAULT_DRILL_ASSET_REQUEST)
				
				IF Event.iEventType = ciEVENT_VAULT_DRILL_ASSET_REQUEST_LASER
					SET_BIT(MC_serverBD.iServerBitSet8, SBBOOL8_VAULT_DRILL_ASSET_REQUEST_LASER)
				ENDIF
			ELSE
				CLEAR_BIT(MC_serverBD.iServerBitSet8, SBBOOL8_VAULT_DRILL_ASSET_REQUEST)
				CLEAR_BIT(MC_serverBD.iServerBitSet8, SBBOOL8_VAULT_DRILL_ASSET_REQUEST_LASER)
			ENDIF

			PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_VAULT_DRILL_ASSET - iEventType =  ", Event.iEventType)
		ENDIF
	ENDIF
ENDPROC


PROC PROCESS_SCRIPT_EVENT_VAULT_DRILL_PROGRESS(INT iEventID)
	IF bIsLocalPlayerHost
		SCRIPT_EVENT_DATA_VAULT_DRILL_PROGRESS Event
		
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
			INT i
			
			FOR i = 0 TO FMMC_MAX_VAULT_DRILL_GAMES -1
				IF Event.iObj = MC_serverBD.iObjVaultDrillIDs[i]
					MC_serverBD.iObjVaultDrillDiscs[i] 		= Event.iDiscs
					MC_serverBD.iObjVaultDrillProgress[i]	= Event.iProgress
					MC_serverBD.iObjHackPart[Event.iObj] 			= -1
					BREAKLOOP
				ENDIF			
			ENDFOR
			
		ENDIF
	ENDIF
ENDPROC

//NEW

PROC PROCESS_SCRIPT_EVENT_FMMC_SHOW_REZ_SHARD(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_SHOW_REZ_SHARD EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_SHOW_REZ_SHARD
			IF EventData.iPartResurrected = iPartToUse
				IF NOT HAS_ANY_TEAM_BEEN_GIVEN_ELIMINIATION_SCORE_THIS_RULE()
					IF EventData.iResurrector != iPartToUse
						IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
						AND NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
							PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(EventData.iResurrector)
							IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)							
								SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GENERIC_TEXT, NETWORK_GET_PLAYER_INDEX(tempPart), DEFAULT, "REZ_SSL", "REZ_ST", GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_playerBD[iPartToUse].iteam, PlayerToUse))
							ENDIF
						ELSE
							iCachedResurrector = EventData.iResurrector
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_SHOW_REZ_TICKER(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_SHOW_REZ_TICKER EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_SHOW_REZ_TICKER
			PARTICIPANT_INDEX partTemp = INT_TO_PARTICIPANTINDEX(EventData.iPart)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(partTemp)
				IF EventData.iPart = iLocalPart
					PRINT_TICKER("TK_REZ_Y")
				ELSE
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_RESURRECTION_SOUNDS)
						IF DOES_TEAM_LIKE_TEAM(MC_playerBD[iLocalPart].iTeam, MC_playerBD[EventData.iPart].iTeam)
							PRINTLN("[JS] - PROCESS_SCRIPT_EVENT_FMMC_SHOW_REZ_TICKER - Playing \"Resurrected\" from \"DLC_SR_RS_Team_Sounds\"")
							PLAY_SOUND_FRONTEND(-1, "Resurrected", "DLC_SR_RS_Team_Sounds")
						ELSE
							PRINTLN("[JS] - PROCESS_SCRIPT_EVENT_FMMC_SHOW_REZ_TICKER - Playing \"Resurrected\" from \"DLC_SR_RS_Enemy_Sounds\"")
							PLAY_SOUND_FRONTEND(-1, "Resurrected", "DLC_SR_RS_Enemy_Sounds")
						ENDIF
					ENDIF
					PRINT_TICKER_WITH_PLAYER_NAME_WITH_HUD_COLOUR("TK_REZ", NETWORK_GET_PLAYER_INDEX(partTemp), GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_playerBD[EventData.iPart].iTeam, PlayerToUse)) 
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
ENDPROC
		
//For use in two team game modes - i.e PowerPlay
FUNC INT GET_PLAYERS_ENEMY_TEAM(PLAYER_INDEX PlayerPed)
	IF GET_PLAYER_TEAM(PlayerPed) = 0
		RETURN 1
	ELSE
		RETURN 0
	ENDIF
ENDFUNC

PROC PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_PICKED_UP_POWERPLAY_PICKUP EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_PICKED_UP_POWERPLAY_PICKUP
			IF EventData.iTeam >= 2
				SCRIPT_ASSERT("Attempting to use Power Play pickups in a 3 or 4 team mode, speak to John Scott")
				EXIT
			ENDIF
			INT iRandomPickup = -1
			INT iRandomAttempts, i
			SWITCH(EventData.iPickupType)
				CASE ciCUSTOM_PICKUP_TYPE__BULLSHARK_TEST
					IF bIsLocalPlayerHost
						IF NOT HAS_NET_TIMER_STARTED(MC_serverBD_3.tdBullShark[EventData.iTeam])
							START_NET_TIMER(MC_serverBD_3.tdBullShark[EventData.iTeam])
							PRINTLN("[JS][POWERPLAY] - PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP - Team: ", EventData.iTeam," picked up Bullshark, starting timer")
						ELSE
							RESET_NET_TIMER(MC_serverBD_3.tdBullShark[EventData.iTeam])
							START_NET_TIMER(MC_serverBD_3.tdBullShark[EventData.iTeam])
							PRINTLN("[JS][POWERPLAY] - PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP - Team: ", EventData.iTeam," picked up another Bullshark, restarting timer")
						ENDIF
					ENDIF
					IF EventData.Details.FromPlayerIndex = LocalPlayer
						SET_BIT(MC_playerBD[iLocalPart].iRagePickupBS, ciCUSTOM_PICKUP_TYPE__BULLSHARK_TEST)
						ALLOW_PLAYER_TO_PICK_UP_RAGE_COLLECTABLES(FALSE)
						BROADCAST_PP_PICKUP_PLAY_ANNOUNCER(ciCUSTOM_PICKUP_TYPE__BULLSHARK_TEST, MC_playerBD[iLocalPart].iteam)
						SET_BIT(iPowerPlayEffectBS, ciCUSTOM_PICKUP_TYPE__BULLSHARK_TEST)
						PRINTLN("[JS][POWERPLAY] - PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP - Picked up Bullshark")
//					ELSE
//						IF EventData.iTeam = MC_playerBD[iLocalPart].iteam
//							SET_BIT(iPowerPlayEffectBS, ciCUSTOM_PICKUP_TYPE__BULLSHARK_TEST)
//							//PRINT_TICKER("RAGE_BST_Y_TIK") //Your team are mad
//						ELSE
//							IF GET_HUD_COLOUR_FOR_FMMC_TEAM(GET_PLAYERS_ENEMY_TEAM(EventData.Details.FromPlayerIndex),LocalPlayer) = HUD_COLOUR_ORANGE
//							//	PRINT_TICKER("RAGE_BST_P_TIK") //Pink team are mad
//							ELSE
//							//	PRINT_TICKER("RAGE_BST_O_TIK") //Orange team are mad
//							ENDIF
//						ENDIF
					ENDIF
					iPowerUpJustPickedUp = ciCUSTOM_PICKUP_TYPE__BULLSHARK_TEST
					iThisPlayerPickedItUp = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(EventData.Details.FromPlayerIndex))
				BREAK
				
				CASE ciCUSTOM_PICKUP_TYPE__BEAST_MODE
					IF bIsLocalPlayerHost
						IF NOT HAS_NET_TIMER_STARTED(MC_serverBD_3.tdBeastMode[EventData.iTeam][0])
							START_NET_TIMER(MC_serverBD_3.tdBeastMode[EventData.iTeam][0])
							MC_serverBD_3.iBeastPart[EventData.iTeam][0] = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(EventData.Details.FromPlayerIndex))
							PRINTLN("[JS][POWERPLAY] - PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP - Team: ", EventData.iTeam," picked up Beast 0, starting timer")
						ELIF NOT HAS_NET_TIMER_STARTED(MC_serverBD_3.tdBeastMode[EventData.iTeam][1])
							START_NET_TIMER(MC_serverBD_3.tdBeastMode[EventData.iTeam][1])
							MC_serverBD_3.iBeastPart[EventData.iTeam][1] = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(EventData.Details.FromPlayerIndex))
							PRINTLN("[JS][POWERPLAY] - PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP - Team: ", EventData.iTeam," picked up Beast 1, starting timer")
						ENDIF
					ENDIF
					IF EventData.Details.FromPlayerIndex = LocalPlayer
						START_NET_TIMER(tdBeastModeBackupTimer)
						SET_BIT(MC_playerBD[iLocalPart].iRagePickupBS, ciCUSTOM_PICKUP_TYPE__BEAST_MODE)
						GET_CURRENT_PED_WEAPON(LocalPlayerPed, wtWeaponBeforeBeastmode)
						ALLOW_PLAYER_TO_PICK_UP_RAGE_COLLECTABLES(FALSE)
						BROADCAST_PP_PICKUP_PLAY_ANNOUNCER(ciCUSTOM_PICKUP_TYPE__BEAST_MODE, MC_playerBD[iLocalPart].iteam)
						SET_BIT(iPowerPlayEffectBS, ciCUSTOM_PICKUP_TYPE__BEAST_MODE)
						PRINTLN("[JS][POWERPLAY] - PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP - Picked up Beast Mode")
					ELSE
//						IF EventData.iTeam = MC_playerBD[iLocalPart].iteam
//						//	PRINT_TICKER("RAGE_BM_Y_TIK") //Your team unleashed the Beast
//						ELSE
//							IF GET_HUD_COLOUR_FOR_FMMC_TEAM(GET_PLAYERS_ENEMY_TEAM(EventData.Details.FromPlayerIndex),LocalPlayer) = HUD_COLOUR_ORANGE
//						//		PRINT_TICKER("RAGE_BM_P_TIK") //Pink team unleashed the Beast
//							ELSE
//						//		PRINT_TICKER("RAGE_BM_O_TIK") //Orange team unleashed the Beast
//							ENDIF
//						ENDIF
						SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(EventData.Details.FromPlayerIndex, RADAR_TRACE_BEAST, TRUE)
					ENDIF
					iPowerUpJustPickedUp = ciCUSTOM_PICKUP_TYPE__BEAST_MODE
					iThisPlayerPickedItUp = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(EventData.Details.FromPlayerIndex))
				BREAK
				
				CASE ciCUSTOM_PICKUP_TYPE__THERMAL_VISION
					IF bIsLocalPlayerHost
						IF NOT HAS_NET_TIMER_STARTED(MC_serverBD_3.tdThermalVision[EventData.iTeam])
							START_NET_TIMER(MC_serverBD_3.tdThermalVision[EventData.iTeam])
							PRINTLN("[JS][POWERPLAY] - PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP - Team: ", EventData.iTeam," picked up Thermal, starting timer")
						ELSE
							RESET_NET_TIMER(MC_serverBD_3.tdThermalVision[EventData.iTeam])
							START_NET_TIMER(MC_serverBD_3.tdThermalVision[EventData.iTeam])
							PRINTLN("[JS][POWERPLAY] - PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP - Team: ", EventData.iTeam," picked up another Thermal, restarting timer")
						ENDIF
					ENDIF
					IF EventData.Details.FromPlayerIndex = LocalPlayer
						SET_BIT(MC_playerBD[iLocalPart].iRagePickupBS, ciCUSTOM_PICKUP_TYPE__THERMAL_VISION)
						ALLOW_PLAYER_TO_PICK_UP_RAGE_COLLECTABLES(FALSE)
						SET_BIT(iPowerPlayEffectBS, ciCUSTOM_PICKUP_TYPE__THERMAL_VISION)
						PRINTLN("[JS][POWERPLAY] - PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP - Picked up Thermal")
//					ELSE
//						IF EventData.iTeam = MC_playerBD[iLocalPart].iteam
//							SET_BIT(iPowerPlayEffectBS, ciCUSTOM_PICKUP_TYPE__THERMAL_VISION)
//						ELSE
//
//						ENDIF
					ENDIF
					//PRINT_TICKER_WITH_TEAM_NAME("RAGE_THR_TIK",GET_PLAYER_TEAM(EventData.Details.FromPlayerIndex)) //~a~ has activated Thermal Vision
					iPowerUpJustPickedUp = ciCUSTOM_PICKUP_TYPE__THERMAL_VISION
					iThisPlayerPickedItUp = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(EventData.Details.FromPlayerIndex))
				BREAK
				
				CASE ciCUSTOM_PICKUP_TYPE__SWAP	
					PRINTLN("[JS][POWERPLAY] - PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP - Starting swap for Team: ", EventData.iTeam)
					IF bIsLocalPlayerHost
						IF NOT HAS_NET_TIMER_STARTED(MC_serverBD_3.tdSwap[EventData.iTeam])
							START_NET_TIMER(MC_serverBD_3.tdSwap[EventData.iTeam])
							PRINTLN("[JS][POWERPLAY] - PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP - Team: ", EventData.iTeam," picked up Swap, starting timer")
						ELSE
							RESET_NET_TIMER(MC_serverBD_3.tdSwap[EventData.iTeam])
							START_NET_TIMER(MC_serverBD_3.tdSwap[EventData.iTeam])
							PRINTLN("[JS][POWERPLAY] - PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP - Team: ", EventData.iTeam," picked up another Swap, restarting timer")
						ENDIF
					ENDIF
					IF EventData.Details.FromPlayerIndex = LocalPlayer
						SET_BIT(MC_playerBD[iLocalPart].iRagePickupBS, ciCUSTOM_PICKUP_TYPE__SWAP)
						ALLOW_PLAYER_TO_PICK_UP_RAGE_COLLECTABLES(FALSE)
						BROADCAST_PP_PICKUP_PLAY_ANNOUNCER(ciCUSTOM_PICKUP_TYPE__SWAP, MC_playerBD[iLocalPart].iteam)
						PRINTLN("[JS][POWERPLAY] - PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP - Picked up Swap")
					ELSE
//						IF EventData.iTeam = MC_playerBD[iLocalPart].iteam
////							IF GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_playerBD[iLocalPart].iteam, LocalPlayer) = HUD_COLOUR_ORANGE
////						//		PRINT_TICKER("RAGE_INV_P_TIK") //Pink team’s controls are reversed.
////							ELSE
////						//		PRINT_TICKER("RAGE_INV_O_TIK") //Orange team’s controls are reversed.
////							ENDIF
//						ELSE
//							SET_BIT(iPowerPlayEffectBS, ciCUSTOM_PICKUP_TYPE__SWAP)
//							PRINTLN("[JS][POWERPLAY] - PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP - On the enemy team setting the swap bit")
//						//	PRINT_TICKER("RAGE_INV_Y_TIK") //Your team’s controls are reversed.
//						ENDIF
					ENDIF
					iPowerUpJustPickedUp = ciCUSTOM_PICKUP_TYPE__SWAP
					iThisPlayerPickedItUp = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(EventData.Details.FromPlayerIndex))
				BREAK
				
				CASE ciCUSTOM_PICKUP_TYPE__FILTER
					IF bIsLocalPlayerHost
						IF NOT HAS_NET_TIMER_STARTED(MC_serverBD_3.tdFilter[EventData.iTeam])
							START_NET_TIMER(MC_serverBD_3.tdFilter[EventData.iTeam])
							PRINTLN("[JS][POWERPLAY] - PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP - Team: ", EventData.iTeam," picked up Filter, starting timer")
						ELSE
							RESET_NET_TIMER(MC_serverBD_3.tdFilter[EventData.iTeam])
							START_NET_TIMER(MC_serverBD_3.tdFilter[EventData.iTeam])
							PRINTLN("[JS][POWERPLAY] - PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP - Team: ", EventData.iTeam," picked up another Filter, restarting timer")
						ENDIF
					ENDIF
					IF EventData.Details.FromPlayerIndex = LocalPlayer
						SET_BIT(MC_playerBD[iLocalPart].iRagePickupBS, ciCUSTOM_PICKUP_TYPE__FILTER)
						ALLOW_PLAYER_TO_PICK_UP_RAGE_COLLECTABLES(FALSE)
						BROADCAST_PP_PICKUP_PLAY_ANNOUNCER(ciCUSTOM_PICKUP_TYPE__FILTER, MC_playerBD[iLocalPart].iteam)
						PRINTLN("[JS][POWERPLAY] - PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP - Picked up Filter")
//					ELSE
//						IF EventData.iTeam = MC_playerBD[iLocalPart].iteam
//							IF GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_playerBD[iLocalPart].iteam, LocalPlayer) = HUD_COLOUR_ORANGE
//						//		PRINT_TICKER("RAGE_FIL_P_TIK") //Pink team got doped.
//							ELSE
//						//		PRINT_TICKER("RAGE_FIL_O_TIK") //Orange team got doped.
//							ENDIF
//						ELSE
//							SET_BIT(iPowerPlayEffectBS, ciCUSTOM_PICKUP_TYPE__FILTER)
//						//	PRINT_TICKER("RAGE_FIL_Y_TIK") //Your team got doped.
//						ENDIF
					ENDIF
					iPowerUpJustPickedUp = ciCUSTOM_PICKUP_TYPE__FILTER
					iThisPlayerPickedItUp = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(EventData.Details.FromPlayerIndex))
				BREAK
				
				CASE ciCUSTOM_PICKUP_TYPE__SLOW_DOWN	
					IF bIsLocalPlayerHost
						IF NOT HAS_NET_TIMER_STARTED(MC_serverBD_3.tdSlowDown[EventData.iTeam])
							START_NET_TIMER(MC_serverBD_3.tdSlowDown[EventData.iTeam])
							PRINTLN("[JS][POWERPLAY] - PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP - Team: ", EventData.iTeam," picked up slow down, starting timer")
						ELSE
							RESET_NET_TIMER(MC_serverBD_3.tdSlowDown[EventData.iTeam])
							START_NET_TIMER(MC_serverBD_3.tdSlowDown[EventData.iTeam])
							PRINTLN("[JS][POWERPLAY] - PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP - Team: ", EventData.iTeam," picked up another slow down, restarting timer")
						ENDIF
					ENDIF
					IF EventData.Details.FromPlayerIndex = LocalPlayer
						SET_BIT(MC_playerBD[iLocalPart].iRagePickupBS, ciCUSTOM_PICKUP_TYPE__SLOW_DOWN)
						ALLOW_PLAYER_TO_PICK_UP_RAGE_COLLECTABLES(FALSE)
						PRINTLN("[JS][POWERPLAY] - PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP - Picked up Slow Down")
//					ELSE
//						IF EventData.iTeam = MC_playerBD[iLocalPart].iteam
//							
//						ELSE
//							SET_BIT(iPowerPlayEffectBS, ciCUSTOM_PICKUP_TYPE__SLOW_DOWN)
//						ENDIF
					ENDIF
					//PRINT_TICKER_WITH_TEAM_NAME("RAGE_SLO_TIK",GET_PLAYER_TEAM(EventData.Details.FromPlayerIndex)) //~a~ has activated Slow Down Time
					iPowerUpJustPickedUp = ciCUSTOM_PICKUP_TYPE__SLOW_DOWN
					iThisPlayerPickedItUp = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(EventData.Details.FromPlayerIndex))
				BREAK
				
				CASE ciCUSTOM_PICKUP_TYPE__BULLET_TIME
					IF bIsLocalPlayerHost
						IF NOT HAS_NET_TIMER_STARTED(MC_serverBD_3.tdBulletTime)
							START_NET_TIMER(MC_serverBD_3.tdBulletTime)
							PRINTLN("[JS][POWERPLAY] - PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP - Team: ", EventData.iTeam," picked up Bullet Time, starting timer")
						ELSE
							RESET_NET_TIMER(MC_serverBD_3.tdBulletTime)
							START_NET_TIMER(MC_serverBD_3.tdBulletTime)
							PRINTLN("[JS][POWERPLAY] - PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP - Team: ", EventData.iTeam," picked up another Bullet Time, restarting timer")
						ENDIF
					ENDIF
					IF EventData.Details.FromPlayerIndex = LocalPlayer
						SET_BIT(MC_playerBD[iLocalPart].iRagePickupBS, ciCUSTOM_PICKUP_TYPE__BULLET_TIME)
						ALLOW_PLAYER_TO_PICK_UP_RAGE_COLLECTABLES(FALSE)
						BROADCAST_PP_PICKUP_PLAY_ANNOUNCER(ciCUSTOM_PICKUP_TYPE__BULLET_TIME, MC_playerBD[iLocalPart].iteam)
						PRINTLN("[JS][POWERPLAY] - PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP - Picked up Bullet Time")
					ENDIF
//					SET_BIT(iPowerPlayEffectBS, ciCUSTOM_PICKUP_TYPE__BULLET_TIME)
					iPowerUpJustPickedUp = ciCUSTOM_PICKUP_TYPE__BULLET_TIME
					iThisPlayerPickedItUp = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(EventData.Details.FromPlayerIndex))
				BREAK
				
				CASE ciCUSTOM_PICKUP_TYPE__HIDE_BLIPS	
					IF bIsLocalPlayerHost
						IF NOT HAS_NET_TIMER_STARTED(MC_serverBD_3.tdHideBlips[EventData.iTeam])
							START_NET_TIMER(MC_serverBD_3.tdHideBlips[EventData.iTeam])
							PRINTLN("[JS][POWERPLAY] - PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP - Team: ", EventData.iTeam," picked up Hide Blips, starting timer")
						ELSE
							RESET_NET_TIMER(MC_serverBD_3.tdHideBlips[EventData.iTeam])
							START_NET_TIMER(MC_serverBD_3.tdHideBlips[EventData.iTeam])
							PRINTLN("[JS][POWERPLAY] - PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP - Team: ", EventData.iTeam," picked up another Hide Blips, restarting timer")
						ENDIF
					ENDIF
					IF EventData.Details.FromPlayerIndex = LocalPlayer
						SET_BIT(MC_playerBD[iLocalPart].iRagePickupBS, ciCUSTOM_PICKUP_TYPE__HIDE_BLIPS)
						SET_BIT(iPowerPlayEffectBS, ciCUSTOM_PICKUP_TYPE__HIDE_BLIPS)
						ALLOW_PLAYER_TO_PICK_UP_RAGE_COLLECTABLES(FALSE)
						BROADCAST_PP_PICKUP_PLAY_ANNOUNCER(ciCUSTOM_PICKUP_TYPE__HIDE_BLIPS, MC_playerBD[iLocalPart].iteam)
						PRINTLN("[JS][POWERPLAY] - PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP - Picked up Hidden")
//					ELSE
//						IF EventData.iTeam = MC_playerBD[iLocalPart].iteam
//					//		PRINT_TICKER("RAGE_HID_Y_TIK") //Your team are off the radar. 
//							SET_BIT(iPowerPlayEffectBS, ciCUSTOM_PICKUP_TYPE__HIDE_BLIPS)
//						ELSE
//							IF GET_HUD_COLOUR_FOR_FMMC_TEAM(GET_PLAYERS_ENEMY_TEAM(EventData.Details.FromPlayerIndex),LocalPlayer) = HUD_COLOUR_ORANGE
//					//			PRINT_TICKER("RAGE_HID_P_TIK") //Pink team are off the radar. 
//							ELSE
//					//			PRINT_TICKER("RAGE_HID_O_TIK") //Orange team are off the radar. 
//							ENDIF
//						ENDIF
					ENDIF
					iPowerUpJustPickedUp = ciCUSTOM_PICKUP_TYPE__HIDE_BLIPS
					iThisPlayerPickedItUp = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(EventData.Details.FromPlayerIndex))
				BREAK
				
				CASE ciCUSTOM_PICKUP_TYPE__RANDOM	
					PRINTLN("[JS][RAGEPICKUPS] - PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP - Picked up Random, setting random bit")
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciUSE_CUSTOM_RANDOM_PICKUP)
						PRINTLN("[JS][RAGEPICKUPS] - PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP - Custom Random Pickup Set, BS value: ", g_FMMC_STRUCT.iRATCRandomBS)
						IF g_FMMC_STRUCT.iRATCRandomBS > 0
							WHILE iRandomAttempts < 30
								iRandomPickup = GET_RANDOM_INT_IN_RANGE(ciCUSTOM_PICKUP_TYPE__BULLSHARK_TEST, ciCUSTOM_PICKUP_TYPE__RANDOM)
								PRINTLN("[JS][RAGEPICKUPS] - PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP - Custom Random Pickup = ", iRandomPickup)
								IF IS_BIT_SET(g_FMMC_STRUCT.iRATCRandomBS, iRandomPickup)
									IF NOT ((MC_serverBD_3.iBeastPart[EventData.iTeam][0] > -1 OR MC_serverBD_3.iBeastPart[EventData.iTeam][1] > -1)
									AND iRandomPickup = ciCUSTOM_PICKUP_TYPE__BEAST_MODE)
										BROADCAST_FMMC_PICKED_UP_POWERPLAY_PICKUP(GET_ENTITY_COORDS(LocalPlayerPed), iRandomPickup, GET_PLAYER_TEAM(PLAYER_ID()))
										iRandomAttempts = 99
									ELSE
										PRINTLN("[JS][RAGEPICKUPS] - PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP - Random pickup tried to start beast mode when 2 beasts already exist, trying again")
									ENDIF
								ELSE
									PRINTLN("[JS][RAGEPICKUPS] - PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP - Custom Random Pickup was not in bitset, trying again")
								ENDIF
								iRandomAttempts++
							ENDWHILE
							IF iRandomAttempts = 30
								FOR i = 0 TO ciCUSTOM_PICKUP_TYPE__RANDOM - 1
									IF IS_BIT_SET(g_FMMC_STRUCT.iRATCRandomBS, i)
										PRINTLN("[JS][RAGEPICKUPS] - PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP - Custom Random Pickup was not in bitset 30 times picking first available powerup")
										BROADCAST_FMMC_PICKED_UP_POWERPLAY_PICKUP(GET_ENTITY_COORDS(LocalPlayerPed), i, GET_PLAYER_TEAM(PLAYER_ID()))
									ENDIF
								ENDFOR
							ENDIF
						ELSE
							PRINTLN("[JS][RAGEPICKUPS] - PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP - iRATCRandomBS = 0 doing nothing")
						ENDIF
					ELSE
						SET_BIT(MC_playerBD[iLocalPart].iRagePickupBS, GET_RANDOM_INT_IN_RANGE(ciCUSTOM_PICKUP_TYPE__BULLSHARK_TEST, ciCUSTOM_PICKUP_TYPE__RANDOM))
						iRandomPickup = GET_RANDOM_INT_IN_RANGE(ciCUSTOM_PICKUP_TYPE__BULLSHARK_TEST, ciCUSTOM_PICKUP_TYPE__RANDOM)
						PRINTLN("[JS][RAGEPICKUPS] - PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP - Custom Random Pickup = ", iRandomPickup)
						BROADCAST_FMMC_PICKED_UP_POWERPLAY_PICKUP(GET_ENTITY_COORDS(LocalPlayerPed), iRandomPickup, GET_PLAYER_TEAM(PLAYER_ID()))
					ENDIF
				BREAK
				
				CASE ciCUSTOM_PICKUP_TYPE__WEAPON_BAG
					PRINTLN("[RAGEPICKUPS] - PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP - Custom pickup Weapon Bag collected giving Mid Mission Inventory")
					
					IF EventData.Details.FromPlayerIndex = LocalPlayer
						GIVE_PLAYER_STARTING_MISSION_WEAPONS(WEAPONINHAND_DONTCARE, FALSE, TRUE)
						PRINT_TICKER("CAS_BAG_PKUPU")
					ELSE
						PRINTLN("[RAGEPICKUPS] - PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP - EventData.Details.FromPlayerIndex != LocalPlayer")
						PRINT_TICKER_WITH_PLAYER_NAME("CAS_BAG_PKUP", EventData.Details.FromPlayerIndex)
					ENDIF
				BREAK
				
			ENDSWITCH
		
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_POWERPLAY_PICKUP_SOUNDS)
				VECTOR vSenderPos = EventData.vPlayerCoords
				IF IS_VECTOR_ZERO(vSenderPos)
					IF DOES_ENTITY_EXIST(GET_PLAYER_PED(EventData.Details.FromPlayerIndex))
						PRINTLN("[JS][POWERPLAY] PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP - Passed position was invalid, setting to entity coords of passed player.")
						vSenderPos = GET_ENTITY_COORDS(GET_PLAYER_PED(EventData.Details.FromPlayerIndex))
					ELSE
						PRINTLN("[JS][POWERPLAY] PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP - Passed position was invalid, and passed player does not exist.")
					ENDIF
				ENDIF
				
				IF EventData.iPickupType != ciCUSTOM_PICKUP_TYPE__RANDOM
					PROCESS_POWERPLAY_PICKUP_SOUND(vSenderPos, (EventData.Details.FromPlayerIndex = LocalPlayer) )
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE(INT iEventID)
	PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE] - Processing...")
	
	SCRIPT_EVENT_DATA_FMMC_ALPHA_CHANGE EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_ALPHA_CHANGE

			PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE] - iAlphaPlayer: ", EventData.iAlphaPlayer, " iVehicleAlpha", EventData.iAlphaVehicle, " iPart: ", EventData.iPart, " iNetVehIndex: ", EventData.iNetVehIndex)
			
			IF EventData.iPart > -1
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(EventData.iPart))
					PED_INDEX piPed =  GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(EventData.iPart)))
			
					IF NOT IS_PED_INJURED(piPed)
						IF EventData.iAlphaPlayer > -1
							IF EventData.iAlphaPlayer = 255							
								PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE] - Resetting Alpha ")
								RESET_ENTITY_ALPHA(piPed)
							ELSE
								PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE] - Setting alpha ")
								SET_ENTITY_ALPHA(piPed, EventData.iAlphaPlayer, FALSE)						
							ENDIF
						ENDIF
						
						IF EventData.iAlphaVehicle > -1
							IF EventData.iAlphaVehicle = 255							
								PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE] - Resetting Alpha ")
								IF IS_PED_IN_ANY_VEHICLE(piPed)
								AND DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(piPed))
									PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE] - Resetting Alpha veh")
									RESET_ENTITY_ALPHA(GET_VEHICLE_PED_IS_IN(piPed))
								ENDIF
								IF EventData.iNetVehIndex > 0
									NETWORK_INDEX netVehID = INT_TO_NATIVE(NETWORK_INDEX, EventData.iNetVehIndex)
									IF NETWORK_DOES_NETWORK_ID_EXIST(netVehID)
										VEHICLE_INDEX vehToChange = NET_TO_VEH(netVehID)
										PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE] - Resetting alpha veh (passed in)")
										RESET_ENTITY_ALPHA(vehToChange)
									ELSE
										PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE] - Net id does not exist...")
									ENDIF
								ENDIF
							ELSE
								PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE] - Setting alpha ")
								IF IS_PED_IN_ANY_VEHICLE(piPed)
								AND DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(piPed))
									PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE] - Setting alpha veh")
									SET_ENTITY_ALPHA(GET_VEHICLE_PED_IS_IN(piPed), EventData.iAlphaVehicle, FALSE)
								ENDIF
								IF EventData.iNetVehIndex > 0
									NETWORK_INDEX netVehID = INT_TO_NATIVE(NETWORK_INDEX, EventData.iNetVehIndex)
									IF NETWORK_DOES_NETWORK_ID_EXIST(netVehID)
										VEHICLE_INDEX vehToChange = NET_TO_VEH(netVehID)
										PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE] - Setting alpha veh (passed in)")
										SET_ENTITY_ALPHA(vehToChange, EventData.iAlphaVehicle, FALSE)
									ELSE
										PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE] - Net id does not exist...")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP_ENDED(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_POWERPLAY_PICKUP_ENDED EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP_ENDED
			INT iBeast
			SWITCH(EventData.iPickupType)
				CASE ciCUSTOM_PICKUP_TYPE__BULLSHARK_TEST
					IF bIsLocalPlayerHost
						RESET_NET_TIMER(MC_serverBD_3.tdBullShark[EventData.iTeam])
					ENDIF
					IF IS_BIT_SET(MC_playerBD[iLocalPart].iRagePickupBS, ciCUSTOM_PICKUP_TYPE__BULLSHARK_TEST)
					AND EventData.iTeam = MC_playerBD[iLocalPart].iteam
						ALLOW_PLAYER_TO_PICK_UP_RAGE_COLLECTABLES()
						CLEAR_BIT(MC_playerBD[iLocalPart].iRagePickupBS, ciCUSTOM_PICKUP_TYPE__BULLSHARK_TEST)
						CLEAR_BIT(iPowerPlayEffectBS, ciCUSTOM_PICKUP_TYPE__BULLSHARK_TEST)
						bPowerUpActive = FALSE
					ELSE
						IF EventData.iTeam = MC_playerBD[iLocalPart].iteam
							CLEAR_BIT(iPowerPlayEffectBS, ciCUSTOM_PICKUP_TYPE__BULLSHARK_TEST)
						ELSE

						ENDIF
					ENDIF
				BREAK
				
				CASE ciCUSTOM_PICKUP_TYPE__BEAST_MODE
					IF bIsLocalPlayerHost
						IF EventData.iReason = ciPOWERPLAY_REASON_DEAD
							FOR iBeast = 0 TO 1
								IF MC_serverBD_3.iBeastPart[EventData.iTeam][iBeast] = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(EventData.Details.FromPlayerIndex))
									RESET_NET_TIMER(MC_serverBD_3.tdBeastMode[EventData.iTeam][iBeast])
									MC_serverBD_3.iBeastPart[EventData.iTeam][iBeast] = -1
									IF iBeast = 0
										IF HAS_NET_TIMER_STARTED(MC_serverBD_3.tdBeastMode[EventData.iTeam][1])
											SET_NET_TIMER_TO_SAME_VALUE_AS_TIMER(MC_serverBD_3.tdBeastMode[EventData.iTeam][0], MC_serverBD_3.tdBeastMode[EventData.iTeam][1].Timer)
											RESET_NET_TIMER(MC_serverBD_3.tdBeastMode[EventData.iTeam][1])
											MC_serverBD_3.iBeastPart[EventData.iTeam][0] = MC_serverBD_3.iBeastPart[EventData.iTeam][1]
											MC_serverBD_3.iBeastPart[EventData.iTeam][1] = -1
											PRINTLN("[JS][POWERPLAY] - PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP_ENDED - tdBeastMode[",EventData.iTeam,"][0] was empty, setting it to [1]'s timer and reseting [1]")
										ENDIF
									ENDIF
								ENDIF
							ENDFOR
						ENDIF
					ENDIF
		
					IF EventData.iPart != -1
					AND EventData.iPart = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(LocalPlayer))
						RESET_NET_TIMER(tdBeastModeBackupTimer)
						ALLOW_PLAYER_TO_PICK_UP_RAGE_COLLECTABLES()
						CLEAR_BIT(MC_playerBD[iLocalPart].iRagePickupBS, ciCUSTOM_PICKUP_TYPE__BEAST_MODE)
						CLEAR_BIT(iPowerPlayEffectBS, ciCUSTOM_PICKUP_TYPE__BEAST_MODE)
						bPowerUpActive = FALSE
					ELSE
						SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(EventData.iPart)), RADAR_TRACE_BEAST, FALSE)
					ENDIF
					IF EventData.iTeam = 0 
						RESET_NET_TIMER(tdUIBeastModeT0)
					ELSE
						RESET_NET_TIMER(tdUIBeastModeT1)
					ENDIF
				BREAK
				
				CASE ciCUSTOM_PICKUP_TYPE__THERMAL_VISION
					IF bIsLocalPlayerHost
						RESET_NET_TIMER(MC_serverBD_3.tdThermalVision[EventData.iTeam])
					ENDIF
					IF IS_BIT_SET(MC_playerBD[iLocalPart].iRagePickupBS, ciCUSTOM_PICKUP_TYPE__THERMAL_VISION)
					AND EventData.iTeam = MC_playerBD[iLocalPart].iteam
						ALLOW_PLAYER_TO_PICK_UP_RAGE_COLLECTABLES()
						CLEAR_BIT(MC_playerBD[iLocalPart].iRagePickupBS, ciCUSTOM_PICKUP_TYPE__THERMAL_VISION)
						CLEAR_BIT(iPowerPlayEffectBS, ciCUSTOM_PICKUP_TYPE__THERMAL_VISION)
						bPowerUpActive = FALSE
					ELSE
						IF EventData.iTeam = MC_playerBD[iLocalPart].iteam
							CLEAR_BIT(iPowerPlayEffectBS, ciCUSTOM_PICKUP_TYPE__THERMAL_VISION)	
						ELSE

						ENDIF
					ENDIF
				BREAK
				
				CASE ciCUSTOM_PICKUP_TYPE__SWAP	
					IF bIsLocalPlayerHost
						RESET_NET_TIMER(MC_serverBD_3.tdSwap[EventData.iTeam])
					ENDIF
					IF IS_BIT_SET(MC_playerBD[iLocalPart].iRagePickupBS, ciCUSTOM_PICKUP_TYPE__SWAP)
					AND EventData.iTeam = MC_playerBD[iLocalPart].iteam
						ALLOW_PLAYER_TO_PICK_UP_RAGE_COLLECTABLES()
						CLEAR_BIT(MC_playerBD[iLocalPart].iRagePickupBS, ciCUSTOM_PICKUP_TYPE__SWAP)
						bPowerUpActive = FALSE
					ELSE
						IF EventData.iTeam = MC_playerBD[iLocalPart].iteam
							 
						ELSE
							CLEAR_BIT(iPowerPlayEffectBS, ciCUSTOM_PICKUP_TYPE__SWAP)
						ENDIF
					ENDIF
				BREAK
				
				CASE ciCUSTOM_PICKUP_TYPE__FILTER	
					IF bIsLocalPlayerHost
						RESET_NET_TIMER(MC_serverBD_3.tdFilter[EventData.iTeam])
					ENDIF
					IF IS_BIT_SET(MC_playerBD[iLocalPart].iRagePickupBS, ciCUSTOM_PICKUP_TYPE__FILTER)
					AND EventData.iTeam = MC_playerBD[iLocalPart].iteam
						ALLOW_PLAYER_TO_PICK_UP_RAGE_COLLECTABLES()
						CLEAR_BIT(MC_playerBD[iLocalPart].iRagePickupBS, ciCUSTOM_PICKUP_TYPE__FILTER)
						bPowerUpActive = FALSE
					ELSE
						IF EventData.iTeam = MC_playerBD[iLocalPart].iteam
							
						ELSE
							CLEAR_BIT(iPowerPlayEffectBS, ciCUSTOM_PICKUP_TYPE__FILTER)							
						ENDIF
					ENDIF
				BREAK
				
				CASE ciCUSTOM_PICKUP_TYPE__SLOW_DOWN	
					IF bIsLocalPlayerHost
						RESET_NET_TIMER(MC_serverBD_3.tdSlowDown[EventData.iTeam])
					ENDIF
					IF IS_BIT_SET(MC_playerBD[iLocalPart].iRagePickupBS, ciCUSTOM_PICKUP_TYPE__SLOW_DOWN)
					AND EventData.iTeam = MC_playerBD[iLocalPart].iteam
						ALLOW_PLAYER_TO_PICK_UP_RAGE_COLLECTABLES()
						CLEAR_BIT(MC_playerBD[iLocalPart].iRagePickupBS, ciCUSTOM_PICKUP_TYPE__SLOW_DOWN)
						bPowerUpActive = FALSE
					ELSE
						IF EventData.iTeam = MC_playerBD[iLocalPart].iteam
							
						ELSE
							CLEAR_BIT(iPowerPlayEffectBS, ciCUSTOM_PICKUP_TYPE__SLOW_DOWN)						
						ENDIF
					ENDIF
				BREAK
				
				CASE ciCUSTOM_PICKUP_TYPE__BULLET_TIME
					IF bIsLocalPlayerHost
						RESET_NET_TIMER(MC_serverBD_3.tdBulletTime)
					ENDIF
					IF IS_BIT_SET(MC_playerBD[iLocalPart].iRagePickupBS, ciCUSTOM_PICKUP_TYPE__BULLET_TIME)
					AND EventData.iTeam = MC_playerBD[iLocalPart].iteam
						ALLOW_PLAYER_TO_PICK_UP_RAGE_COLLECTABLES()
						CLEAR_BIT(MC_playerBD[iLocalPart].iRagePickupBS, ciCUSTOM_PICKUP_TYPE__BULLET_TIME)
						bPowerUpActive = FALSE
					ELSE
						IF EventData.iTeam = MC_playerBD[iLocalPart].iteam
							
						ELSE

						ENDIF
					ENDIF
					CLEAR_BIT(iPowerPlayEffectBS, ciCUSTOM_PICKUP_TYPE__BULLET_TIME)	
				BREAK
				
				CASE ciCUSTOM_PICKUP_TYPE__HIDE_BLIPS	
					IF bIsLocalPlayerHost
						RESET_NET_TIMER(MC_serverBD_3.tdHideBlips[EventData.iTeam])
					ENDIF
					IF IS_BIT_SET(MC_playerBD[iLocalPart].iRagePickupBS, ciCUSTOM_PICKUP_TYPE__HIDE_BLIPS)
					AND EventData.iTeam = MC_playerBD[iLocalPart].iteam
						ALLOW_PLAYER_TO_PICK_UP_RAGE_COLLECTABLES()
						CLEAR_BIT(MC_playerBD[iLocalPart].iRagePickupBS, ciCUSTOM_PICKUP_TYPE__HIDE_BLIPS)
						CLEAR_BIT(iPowerPlayEffectBS, ciCUSTOM_PICKUP_TYPE__HIDE_BLIPS)
						bPowerUpActive = FALSE
					ELSE
						IF EventData.iTeam = MC_playerBD[iLocalPart].iteam
							CLEAR_BIT(iPowerPlayEffectBS, ciCUSTOM_PICKUP_TYPE__HIDE_BLIPS)
						ELSE
							//CLEAR_BIT(iPowerPlayEffectBS, ciCUSTOM_PICKUP_TYPE__HIDE_BLIPS)	
						ENDIF
					ENDIF
				BREAK
				
			ENDSWITCH
			IF EventData.iReason = ciPOWERPLAY_REASON_DEAD
				PRINTLN("[JS][POWERPLAY] PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP_ENDED - Pickup type ", EventData.iPickupType ," has ended as player is dead")
			ELIF EventData.iReason = ciPOWERPLAY_REASON_EXPIRED
				PRINTLN("[JS][POWERPLAY] PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP_ENDED - Pickup type ", EventData.iPickupType ," has ended as timer expired")
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

//END NEW

PROC PROCESS_SCRIPT_EVENT_FMMC_PICKED_UP_LIVES_PICKUP(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_PICKED_UP_LIVES_PICKUP EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_PICKED_UP_LIVES_PICKUP
			INT iTeam = EventData.iTeam
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciPLAYER_LIVES_PICKUP_TYPE)
				//Team Lives
				IF bIsLocalPlayerHost
					INT i
					FOR i = 0 TO FMMC_MAX_TEAMS - 1
						MC_serverBD_3.iAdditionalTeamLives[i] = g_FMMC_STRUCT.iLivesToTeam[iTeam][i]
					ENDFOR
				ENDIF
			ELSE
				//PlayerLives
				IF IS_MISSION_COOP_TEAM_LIVES()
					IF bIsLocalPlayerHost
						MC_serverBD_3.iAdditionalTeamLives[0] += g_FMMC_STRUCT.iLivesToTeam[0][0]
					ENDIF
				ELSE
					IF EventData.Details.FromPlayerIndex = LocalPlayer
						MC_playerBD[iLocalPart].iAdditionalPlayerLives += g_FMMC_STRUCT.iLivesToTeam[0][0]
					ENDIF
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_POWERUP(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_PICKED_UP_POWERPLAY_POWERUP EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_PICKED_UP_POWERPLAY_POWERUP
			VECTOR vSenderPos = EventData.vPlayerCoords
			
			IF IS_VECTOR_ZERO(vSenderPos)
				IF DOES_ENTITY_EXIST(GET_PLAYER_PED(EventData.Details.FromPlayerIndex))
					PRINTLN("[RCC MISSION] PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_POWERUP - Passed position was invalid, setting to entity coords of passed player.")
					vSenderPos = GET_ENTITY_COORDS(GET_PLAYER_PED(EventData.Details.FromPlayerIndex))
				ELSE
					PRINTLN("[RCC MISSION] PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_POWERUP - Passed position was invalid, and passed player does not exist.")
				ENDIF
			ENDIF
			PROCESS_POWERPLAY_PICKUP_SOUND(EventData.vPlayerCoords, (EventData.Details.FromPlayerIndex = LocalPlayer) )
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_PP_PICKUP_PLAY_ANNOUNCER(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_PP_PICKUP_PLAY_ANNOUNCER EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_PP_PICKUP_PLAY_ANNOUNCER
			IF IS_SKYSWOOP_AT_GROUND()
				PLAY_RAGE_PP_ANNOUNCER(EventData.iPowerupType, EventData.iTeam, EventData.Details.FromPlayerIndex)
				PLAY_POWERPLAY_SOUND(EventData.iPowerupType, EventData.iTeam, EventData.Details.FromPlayerIndex)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    This function is intended to activate weapons that should effect all players 
PROC PROCESS_SCRIPT_EVENT_FMMC_ACTIVATE_VEHICLE_WEAPON(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_ACTIVATE_VEHICLE_WEAPON EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_ACTIVATE_VEHICLE_WEAPON
			
			IF EventData.Details.FromPlayerIndex = LocalPlayer
				PRINTLN("[KH][VEHICLE WEAPONS] PROCESS_SCRIPT_EVENT_FMMC_ACTIVATE_VEHICLE_WEAPON - Local player used: ", GET_VEHICLE_WEAPON_STRING(EventData.iPickupType))
			ELSE
				PRINTLN("[KH][VEHICLE WEAPONS] PROCESS_SCRIPT_EVENT_FMMC_ACTIVATE_VEHICLE_WEAPON - Player: ", NATIVE_TO_INT(EventData.Details.FromPlayerIndex)," used: ", GET_VEHICLE_WEAPON_STRING(EventData.iPickupType))
			ENDIF
			
			SWITCH(EventData.iPickupType)
				CASE ciVEH_WEP_ROCKETS
					iVehPowerUpJustPickedUp = ciVEH_WEP_ROCKETS
					iVehThisPlayerPickedItUp = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(EventData.Details.FromPlayerIndex))
				BREAK
				
				CASE ciVEH_WEP_SPEED_BOOST
					iVehPowerUpJustPickedUp = ciVEH_WEP_SPEED_BOOST
					iVehThisPlayerPickedItUp = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(EventData.Details.FromPlayerIndex))
				BREAK
				
				CASE ciVEH_WEP_GHOST
					IF EventData.iTeam = 0
						IF NOT HAS_NET_TIMER_STARTED(tdVehGhost0)
							START_NET_TIMER(tdVehGhost0)
						ELSE
							RESET_NET_TIMER(tdVehGhost0)
							START_NET_TIMER(tdVehGhost0)
						ENDIF
					ELSE
						IF NOT HAS_NET_TIMER_STARTED(tdVehGhost1)
							START_NET_TIMER(tdVehGhost1)
						ELSE
							RESET_NET_TIMER(tdVehGhost1)
							START_NET_TIMER(tdVehGhost1)
						ENDIF
					ENDIF
					iVehPowerUpJustPickedUp = ciVEH_WEP_GHOST
					iVehThisPlayerPickedItUp = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(EventData.Details.FromPlayerIndex))
				BREAK
				
				CASE ciVEH_WEP_BEAST
					IF EventData.iTeam = 0
						IF NOT HAS_NET_TIMER_STARTED(tdVehBeast0)
							START_NET_TIMER(tdVehBeast0)
						ELSE
							RESET_NET_TIMER(tdVehBeast0)
							START_NET_TIMER(tdVehBeast0)
						ENDIF
					ELSE
						IF NOT HAS_NET_TIMER_STARTED(tdVehBeast1)
							START_NET_TIMER(tdVehBeast1)
						ELSE
							RESET_NET_TIMER(tdVehBeast1)
							START_NET_TIMER(tdVehBeast1)
						ENDIF
					ENDIF
					iVehPowerUpJustPickedUp = ciVEH_WEP_BEAST
					iVehThisPlayerPickedItUp = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(EventData.Details.FromPlayerIndex))
				BREAK
				
				CASE ciVEH_WEP_PRON
					IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iVehicleWeaponEffectActiveBS, ciVEH_WEP_PRON)
						PRINTLN("[KH][VEHICLE WEAPONS] PROCESS_SCRIPT_EVENT_FMMC_ACTIVATE_VEHICLE_WEAPON - Starting Pron effect on local player")
						SET_BIT(MC_playerBD[iLocalPart].iVehicleWeaponEffectActiveBS, ciVEH_WEP_PRON)
					ELSE
						PRINTLN("[KH][VEHICLE WEAPONS] PROCESS_SCRIPT_EVENT_FMMC_ACTIVATE_VEHICLE_WEAPON - Pron effect already active on local player, resetting timer")
						RESET_NET_TIMER(stVehWepPronEffectTimer)
						START_NET_TIMER(stVehWepPronEffectTimer)
					ENDIF
						
					IF EventData.iTeam = 0
						IF NOT HAS_NET_TIMER_STARTED(tdVehPron0)
							START_NET_TIMER(tdVehPron0)
						ELSE
							RESET_NET_TIMER(tdVehPron0)
							START_NET_TIMER(tdVehPron0)
						ENDIF
					ELIF EventData.iTeam = 1
						IF NOT HAS_NET_TIMER_STARTED(tdVehPron1)
							START_NET_TIMER(tdVehPron1)
						ELSE
							RESET_NET_TIMER(tdVehPron1)
							START_NET_TIMER(tdVehPron1)
						ENDIF
					ELIF EventData.iTeam = 2
						IF NOT HAS_NET_TIMER_STARTED(tdVehPron2)
							START_NET_TIMER(tdVehPron2)
						ELSE
							RESET_NET_TIMER(tdVehPron2)
							START_NET_TIMER(tdVehPron2)
						ENDIF
					ELIF EventData.iTeam = 3	
						IF NOT HAS_NET_TIMER_STARTED(tdVehPron3)
							START_NET_TIMER(tdVehPron3)
						ELSE
							RESET_NET_TIMER(tdVehPron3)
							START_NET_TIMER(tdVehPron3)
						ENDIF
					ENDIF
					iVehPowerUpJustPickedUp = ciVEH_WEP_PRON
					iVehThisPlayerPickedItUp = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(EventData.Details.FromPlayerIndex))
					
				BREAK
				
				CASE ciVEH_WEP_FORCE_ACCELERATE
					IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iVehicleWeaponEffectActiveBS, ciVEH_WEP_FORCE_ACCELERATE)
					AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
						IF MC_playerBD[iLocalPart].iteam != EventData.iTeam
							PRINTLN("[KH][VEHICLE WEAPONS] PROCESS_SCRIPT_EVENT_FMMC_ACTIVATE_VEHICLE_WEAPON - Starting Jammed effect on local player")
							SET_BIT(MC_playerBD[iPartToUse].iVehicleWeaponEffectActiveBS, ciVEH_WEP_FORCE_ACCELERATE)
						ENDIF
					ELSE
						IF MC_playerBD[iLocalPart].iteam != EventData.iTeam
							PRINTLN("[KH][VEHICLE WEAPONS] PROCESS_SCRIPT_EVENT_FMMC_ACTIVATE_VEHICLE_WEAPON - Jammed effect already active on local player, resetting timer")
							RESET_NET_TIMER(stVehWepForceAccelerateTimer)
						ENDIF
					ENDIF
					IF EventData.iTeam = 0
						IF NOT HAS_NET_TIMER_STARTED(tdVehAccelerate0)
							START_NET_TIMER(tdVehAccelerate0)
						ELSE
							RESET_NET_TIMER(tdVehAccelerate0)
							START_NET_TIMER(tdVehAccelerate0)
						ENDIF
					ELSE
						IF NOT HAS_NET_TIMER_STARTED(tdVehAccelerate1)
							START_NET_TIMER(tdVehAccelerate1)
						ELSE
							RESET_NET_TIMER(tdVehAccelerate1)
							START_NET_TIMER(tdVehAccelerate1)
						ENDIF
					ENDIF
					iVehPowerUpJustPickedUp = ciVEH_WEP_FORCE_ACCELERATE
					iVehThisPlayerPickedItUp = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(EventData.Details.FromPlayerIndex))
				BREAK
				
				CASE ciVEH_WEP_FLIPPED_CONTROLS
					IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iVehicleWeaponEffectActiveBS, ciVEH_WEP_FLIPPED_CONTROLS)
					AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
						IF MC_playerBD[iLocalPart].iteam != EventData.iTeam
							PRINTLN("[KH][VEHICLE WEAPONS] PROCESS_SCRIPT_EVENT_FMMC_ACTIVATE_VEHICLE_WEAPON - Starting Flipped Controls effect on local player")
							SET_BIT(MC_playerBD[iPartToUse].iVehicleWeaponEffectActiveBS, ciVEH_WEP_FLIPPED_CONTROLS)
						ENDIF
					ELSE
						IF MC_playerBD[iLocalPart].iteam != EventData.iTeam
							PRINTLN("[KH][VEHICLE WEAPONS] PROCESS_SCRIPT_EVENT_FMMC_ACTIVATE_VEHICLE_WEAPON - Flipped effect already active on local player, resetting timer")
							RESET_NET_TIMER(stVehWepFlippedTimer)
						ENDIF
					ENDIF
					IF EventData.iTeam = 0
						IF NOT HAS_NET_TIMER_STARTED(tdVehFlipped0)
							START_NET_TIMER(tdVehFlipped0)
						ELSE
							RESET_NET_TIMER(tdVehFlipped0)
							START_NET_TIMER(tdVehFlipped0)
						ENDIF
					ELIF EventData.iTeam = 1
						IF NOT HAS_NET_TIMER_STARTED(tdVehFlipped1)
							START_NET_TIMER(tdVehFlipped1)
						ELSE
							RESET_NET_TIMER(tdVehFlipped1)
							START_NET_TIMER(tdVehFlipped1)
						ENDIF
					ELIF EventData.iTeam = 2
						IF NOT HAS_NET_TIMER_STARTED(tdVehFlipped2)
							START_NET_TIMER(tdVehFlipped2)
						ELSE
							RESET_NET_TIMER(tdVehFlipped2)
							START_NET_TIMER(tdVehFlipped2)
						ENDIF
					ELIF EventData.iTeam = 3
						IF NOT HAS_NET_TIMER_STARTED(tdVehFlipped3)
							START_NET_TIMER(tdVehFlipped3)
						ELSE
							RESET_NET_TIMER(tdVehFlipped3)
							START_NET_TIMER(tdVehFlipped3)
						ENDIF
					ENDIF
					iVehPowerUpJustPickedUp = ciVEH_WEP_FLIPPED_CONTROLS
					iVehThisPlayerPickedItUp = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(EventData.Details.FromPlayerIndex))
				BREAK
				
				CASE ciVEH_WEP_ZONED
					IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iVehicleWeaponEffectActiveBS, ciVEH_WEP_ZONED)
						PRINTLN("[KH][VEHICLE WEAPONS] PROCESS_SCRIPT_EVENT_FMMC_ACTIVATE_VEHICLE_WEAPON - Starting Zoned effect on local player")
						SET_BIT(MC_playerBD[iLocalPart].iVehicleWeaponEffectActiveBS, ciVEH_WEP_ZONED)
					ELSE
						PRINTLN("[KH][VEHICLE WEAPONS] PROCESS_SCRIPT_EVENT_FMMC_ACTIVATE_VEHICLE_WEAPON - Zoned effect already active on local player, resetting timer")
						RESET_NET_TIMER(stVehWepZonedTimer)
						START_NET_TIMER(stVehWepZonedTimer)
					ENDIF
					IF NOT HAS_NET_TIMER_STARTED(tdVehZoned)
						START_NET_TIMER(tdVehZoned)
					ELSE
						RESET_NET_TIMER(tdVehZoned)
						START_NET_TIMER(tdVehZoned)
					ENDIF
					iVehPowerUpJustPickedUp = ciVEH_WEP_ZONED
					iVehThisPlayerPickedItUp = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(EventData.Details.FromPlayerIndex))
				BREAK
				
				CASE ciVEH_WEP_BOUNCE
					iVehPowerUpJustPickedUp = ciVEH_WEP_BOUNCE
					iVehThisPlayerPickedItUp = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(EventData.Details.FromPlayerIndex))
				BREAK
				
				CASE ciVEH_WEP_DETONATE
					iVehPowerUpJustPickedUp = ciVEH_WEP_DETONATE
					iVehThisPlayerPickedItUp = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(EventData.Details.FromPlayerIndex))
				BREAK
				
				CASE ciVEH_WEP_BOMB
					iVehPowerUpJustPickedUp = ciVEH_WEP_BOMB
					iVehThisPlayerPickedItUp = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(EventData.Details.FromPlayerIndex))
				BREAK
				
				CASE ciVEH_WEP_REPAIR
					iVehPowerUpJustPickedUp = ciVEH_WEP_REPAIR
					iVehThisPlayerPickedItUp = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(EventData.Details.FromPlayerIndex))
				BREAK
				
				CASE ciVEH_WEP_BOMB_LENGTH
					iVehPowerUpJustPickedUp = ciVEH_WEP_BOMB_LENGTH
					iVehThisPlayerPickedItUp = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(EventData.Details.FromPlayerIndex))
				BREAK
				CASE ciVEH_WEP_BOMB_MAX
					iVehPowerUpJustPickedUp = ciVEH_WEP_BOMB_MAX
					iVehThisPlayerPickedItUp = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(EventData.Details.FromPlayerIndex))
				BREAK
				CASE ciVEH_WEP_EXTRA_LIFE
					iVehPowerUpJustPickedUp = ciVEH_WEP_EXTRA_LIFE
					iVehThisPlayerPickedItUp = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(EventData.Details.FromPlayerIndex))
				BREAK
				
				CASE ciVEH_WEP_RUINER_SPECIAL_VEH
					IF EventData.iTeam = 0
						IF NOT HAS_NET_TIMER_STARTED(tdSwapTimerRuinerT0)
							START_NET_TIMER(tdSwapTimerRuinerT0)
						ELSE
							RESET_NET_TIMER(tdSwapTimerRuinerT0)
							START_NET_TIMER(tdSwapTimerRuinerT0)
						ENDIF
					ELIF EventData.iTeam = 1
						IF NOT HAS_NET_TIMER_STARTED(tdSwapTimerRuinerT1)
							START_NET_TIMER(tdSwapTimerRuinerT1)
						ELSE
							RESET_NET_TIMER(tdSwapTimerRuinerT1)
							START_NET_TIMER(tdSwapTimerRuinerT1)
						ENDIF
					ELIF EventData.iTeam = 2
						IF NOT HAS_NET_TIMER_STARTED(tdSwapTimerRuinerT2)
							START_NET_TIMER(tdSwapTimerRuinerT2)
						ELSE
							RESET_NET_TIMER(tdSwapTimerRuinerT2)
							START_NET_TIMER(tdSwapTimerRuinerT2)
						ENDIF
					ELIF EventData.iTeam = 3
						IF NOT HAS_NET_TIMER_STARTED(tdSwapTimerRuinerT3)
							START_NET_TIMER(tdSwapTimerRuinerT3)
						ELSE
							RESET_NET_TIMER(tdSwapTimerRuinerT3)
							START_NET_TIMER(tdSwapTimerRuinerT3)
						ENDIF
					ENDIF
					
					iVehPowerUpJustPickedUp = ciVEH_WEP_RUINER_SPECIAL_VEH
					iVehThisPlayerPickedItUp = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(EventData.Details.FromPlayerIndex))
				BREAK
				
				CASE ciVEH_WEP_RAMP_SPECIAL_VEH
					IF EventData.iTeam = 0
						IF NOT HAS_NET_TIMER_STARTED(tdSwapTimerRampT0)
							START_NET_TIMER(tdSwapTimerRampT0)
						ELSE
							RESET_NET_TIMER(tdSwapTimerRampT0)
							START_NET_TIMER(tdSwapTimerRampT0)
						ENDIF
					ELIF EventData.iTeam = 1
						IF NOT HAS_NET_TIMER_STARTED(tdSwapTimerRampT1)
							START_NET_TIMER(tdSwapTimerRampT1)
						ELSE
							RESET_NET_TIMER(tdSwapTimerRampT1)
							START_NET_TIMER(tdSwapTimerRampT1)
						ENDIF
					ELIF EventData.iTeam = 2
						IF NOT HAS_NET_TIMER_STARTED(tdSwapTimerRampT2)
							START_NET_TIMER(tdSwapTimerRampT2)
						ELSE
							RESET_NET_TIMER(tdSwapTimerRampT2)
							START_NET_TIMER(tdSwapTimerRampT2)
						ENDIF
					ELIF EventData.iTeam = 3
						IF NOT HAS_NET_TIMER_STARTED(tdSwapTimerRampT3)
							START_NET_TIMER(tdSwapTimerRampT3)
						ELSE
							RESET_NET_TIMER(tdSwapTimerRampT3)
							START_NET_TIMER(tdSwapTimerRampT3)
						ENDIF
					ENDIF
					
					iVehPowerUpJustPickedUp = ciVEH_WEP_RAMP_SPECIAL_VEH
					iVehThisPlayerPickedItUp = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(EventData.Details.FromPlayerIndex))
				BREAK				
			ENDSWITCH
			
			PLAY_VEHICLE_VENDETTA_SOUND(EventData.iPickupType, EventData.iTeam, EventData.Details.FromPlayerIndex)
			
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_REQUEST_EXTRA_LIFE_FROM_PICKUP(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_EXTRA_LIFE_PICKUP EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_REQUEST_EXTRA_LIFE_FROM_PICKUP
			IF bIsLocalPlayerHost
				IF EventData.iTeam != -1
				AND EventData.iTeam < FMMC_MAX_TEAMS
					MC_serverBD.iTeamDeaths[EventData.iTeam]--
					IF MC_serverBD.iTeamDeaths[EventData.iTeam] < 0
						MC_serverBD.iTeamDeaths[EventData.iTeam] = 0
					ENDIF
					PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_REQUEST_EXTRA_LIFE_FROM_PICKUP - Reducing team deaths for team ", EventData.iTeam)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_DETONATE_PROP(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_DETONATE_PROP EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_DETONATE_PROP
			IF DOES_ENTITY_EXIST(oiProps[EventData.iProp])	
				PRINTLN("[KH][VEHICLE WEAPONS][LM] PROCESS_SCRIPT_EVENT_FMMC_DETONATE_PROP - Set Bit")
				// We have to set this to avoid using multiple timers to control fading/flashing of Props
				INT iPropEF = 0
				FOR iPropEF = 0 TO FMMC_MAX_NUM_FADEOUT_PROPS_EVERY_FRAME-1
					IF iPropFadeoutEveryFrameIndex[iPropEF] = -1
						iPropFadeoutEveryFrameIndex[iPropEF] = EventData.iProp
						iFlashToggle[iPropEF] = 1
						iFlashToggleTime[iPropEF] = (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdMissionLengthTimer)/1000)						
						SET_BIT(iPropCleanedupTriggeredBS[GET_LONG_BITSET_INDEX(EventData.iprop)], GET_LONG_BITSET_BIT(EventData.iprop))
						PRINTLN("[LM][PROCESS_PROPS] - ciFMMC_PROP_Cleanup_Triggered has been set on iProp: ", EventData.iProp)
						BREAKLOOP
					ENDIF
				ENDFOR
					
				IF NOT HAS_NET_TIMER_STARTED(tdFlashPropTimer)
					START_NET_TIMER(tdFlashPropTimer)
				ENDIF
				
				SET_BIT(iPropDetonateTriggeredBS[GET_LONG_BITSET_INDEX(EventData.iprop)], GET_LONG_BITSET_BIT(EventData.iprop))
				PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_DETONATE_PROP] - Prop Index: ", EventData.iProp)
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_CONDEMNED_DIED(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_CONDEMNED_DIED EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_CONDEMNED_DIED
			STRING sSoundName = "Condemned_Die"
			STRING sSoundSet = "DLC_SM_CND_Player_Sounds"
			
			IF EventData.Details.FromPlayerIndex != LocalPlayer
				sSoundSet = "DLC_SM_CND_Enemy_Sounds"
			ENDIF
			
			PLAY_SOUND_FROM_COORD(-1, sSoundName, EventData.vPos, sSoundSet)
		ENDIF
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC PROCESS_SCRIPT_EVENT_FMMC_PAUSE_ALL_TIMERS(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_PAUSE_ALL_TIMERS EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_PAUSE_ALL_TIMERS
			IF EventData.Details.FromPlayerIndex != PLAYER_ID()
				bWdPauseGameTimer = EventData.bPauseToggle
			ENDIF
			PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_PAUSE_ALL_TIMERS] - Player ", GET_PLAYER_NAME(EventData.Details.FromPlayerIndex), "  EventData.bPauseToggle: ",  EventData.bPauseToggle)
		ENDIF
	ENDIF
ENDPROC
#ENDIF

#IF IS_DEBUG_BUILD
PROC PROCESS_SCRIPT_EVENT_FMMC_EDIT_ALL_TIMERS(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_EDIT_ALL_TIMERS EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_EDIT_ALL_TIMERS
			IF EventData.bIncrement
				bWdIncrementTimer = TRUE
			ELSE
				bWdDecrementTimer = TRUE
			ENDIF
			
			PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_EDIT_ALL_TIMERS] - Player ", GET_PLAYER_NAME(EventData.Details.FromPlayerIndex), "  EventData.bIncrement: ",  EventData.bIncrement)
		ENDIF
	ENDIF
ENDPROC
#ENDIF

PROC PROCESS_SCRIPT_EVENT_FMMC_DECREMENT_REMAINING_TIME(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_DECREMENT_REMAINING_TIME EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		
		INT iAmountToDecrement
		
		iAmountToDecrement = EventData.iAmountToDecrement
		
		IF bIsLocalPlayerHost
			INT i
			FOR i = 0 TO FMMC_MAX_TEAMS-1
								
				IF EventData.bTeamOnly
					IF i != EventData.iTeam
						PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_DECREMENT_REMAINING_TIME - Skipping team: ", i, " as not the team that sent request")
						RELOOP
					ENDIF
				ENDIF
							
				IF HAS_NET_TIMER_STARTED(MC_serverBD_3.tdMultiObjectiveLimitTimer[i])
					MC_serverBD_3.tdMultiObjectiveLimitTimer[i].Timer = GET_TIME_OFFSET(MC_serverBD_3.tdMultiObjectiveLimitTimer[i].Timer, -EventData.iAmountToDecrement)
					PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_DECREMENT_REMAINING_TIME - Decremented Multirule timer for team: ", i, " by ", EventData.iAmountToDecrement)
					IF GET_TIME_REMAINING_ON_MULTIRULE_TIMER() <= 30000
						SET_BIT(MC_serverBD.iServerBitSet6, SBBOOL6_TIME_DEDUCTED_INTO_30S)
					ENDIF
				ENDIF
				IF HAS_NET_TIMER_STARTED(MC_serverBD_3.tdObjectiveLimitTimer[i])
					
					IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VENETIAN_JOB(g_FMMC_STRUCT.iAdversaryModeType)
					OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_MONSTER_JAM(g_FMMC_STRUCT.iAdversaryModeType)
						IF (GET_REMAINING_TIME_ON_RULE(i, MC_serverBD_4.iCurrentHighestPriority[i]) - EventData.iAmountToDecrement) <= 30000
							iAmountToDecrement = GET_REMAINING_TIME_ON_RULE(i, MC_serverBD_4.iCurrentHighestPriority[i]) - 31000
						ENDIF
					ENDIF
					
					MC_serverBD_3.tdObjectiveLimitTimer[i].Timer = GET_TIME_OFFSET(MC_serverBD_3.tdObjectiveLimitTimer[i].Timer, -iAmountToDecrement)
					PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_DECREMENT_REMAINING_TIME - Decremented Rule timer for team: ", i, " by ", iAmountToDecrement)
					IF MC_serverBD_4.iCurrentHighestPriority[i] < FMMC_MAX_RULES
						IF GET_REMAINING_TIME_ON_RULE(i, MC_serverBD_4.iCurrentHighestPriority[i]) <= 30000
							SET_BIT(MC_serverBD.iServerBitSet6, SBBOOL6_TIME_DEDUCTED_INTO_30S)
						ENDIF
					ENDIF
				ENDIF
			ENDFOR	
			IF HAS_NET_TIMER_STARTED(MC_serverBD.tdMissionLengthTimer)
				MC_serverBD.tdMissionLengthTimer.Timer = GET_TIME_OFFSET(MC_serverBD.tdMissionLengthTimer.Timer, -EventData.iAmountToDecrement)
				PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_DECREMENT_REMAINING_TIME - Decremented Mission length timer by ", EventData.iAmountToDecrement)
			ENDIF
		ELSE
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VENETIAN_JOB(g_FMMC_STRUCT.iAdversaryModeType)
			OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_MONSTER_JAM(g_FMMC_STRUCT.iAdversaryModeType)
				IF (GET_REMAINING_TIME_ON_RULE(EventData.iTeam, MC_serverBD_4.iCurrentHighestPriority[EventData.iTeam]) - EventData.iAmountToDecrement) <= 30000
					iAmountToDecrement = GET_REMAINING_TIME_ON_RULE(EventData.iTeam, MC_serverBD_4.iCurrentHighestPriority[EventData.iTeam]) - 31000
				ENDIF
			ENDIF
		ENDIF

		INT iTimeToDisplay = (iAmountToDecrement / 1000)
		IF NOT EventData.bStealingPoints
			IF EventData.bDestroyed
				PRINT_TICKER_WITH_PLAYER_NAME_AND_INT("DT_DEST", EventData.Details.FromPlayerIndex, iTimeToDisplay)
			ELSE
				PRINT_TICKER_WITH_PLAYER_NAME_AND_INT("VNJOB_MRSPWN", EventData.Details.FromPlayerIndex, iTimeToDisplay)
			ENDIF
		ENDIF
		fRedTimerTime = 5.0
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_INCREMENT_REMAINING_TIME(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_INCREMENT_REMAINING_TIME EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF bIsLocalPlayerHost
			INT i
			FOR i = 0 TO FMMC_MAX_TEAMS-1
			
				IF EventData.bTeamOnly
					IF i != EventData.iTeam
						PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_INCREMENT_REMAINING_TIME - Skipping team: ", i, " as not the team that sent request")
						RELOOP
					ENDIF
				ENDIF
				
				IF HAS_NET_TIMER_STARTED(MC_serverBD_3.tdMultiObjectiveLimitTimer[i])
					MC_serverBD_3.tdMultiObjectiveLimitTimer[i].Timer = GET_TIME_OFFSET(MC_serverBD_3.tdMultiObjectiveLimitTimer[i].Timer, EventData.iAmountToIncrement)
					PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_INCREMENT_REMAINING_TIME - Incremented Multirule timer for team: ", i, " by ", EventData.iAmountToIncrement)
				ENDIF
				IF HAS_NET_TIMER_STARTED(MC_serverBD_3.tdObjectiveLimitTimer[i])
					MC_serverBD_3.tdObjectiveLimitTimer[i].Timer = GET_TIME_OFFSET(MC_serverBD_3.tdObjectiveLimitTimer[i].Timer, EventData.iAmountToIncrement)
					PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_INCREMENT_REMAINING_TIME - Incremented Rule timer for team: ", i, " by ", EventData.iAmountToIncrement)
				ENDIF
			ENDFOR	
			IF HAS_NET_TIMER_STARTED(MC_serverBD.tdMissionLengthTimer)
				MC_serverBD.tdMissionLengthTimer.Timer = GET_TIME_OFFSET(MC_serverBD.tdMissionLengthTimer.Timer, EventData.iAmountToIncrement)
				PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_INCREMENT_REMAINING_TIME - Incremented Mission length timer by ", EventData.iAmountToIncrement)
			ENDIF
		ENDIF
		
		INT iTimeToDisplay = (EventData.iAmountToIncrement / 1000)
		
		IF EventData.bStealingPoints
			//PRINT_TICKER_WITH_PLAYER_NAME_AND_INT("AT_INC_STEAL", EventData.Details.FromPlayerIndex, iTimeToDisplay)
			PRINT_TICKER_WITH_TWO_PLAYER_NAMES_AND_TWO_INTS("AT_INC_STEAL", EventData.Details.FromPlayerIndex, INT_TO_PLAYERINDEX(EventData.iPlayerIndexVictim), iTimeToDisplay)
			
		ELSE
			PRINT_TICKER_WITH_PLAYER_NAME_AND_INT("AT_INCT", NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(EventData.iParticipantIndex)) , iTimeToDisplay)
		ENDIF
		
		// Increment variable tracking total time accrued by this player, for use with the leaderboard in Arena Trials
		IF EventData.iParticipantIndex = iLocalPart	 
			MC_playerBD_1[iLocalPart].iArenaTrialTotalTimeGained += iTimeToDisplay	 // Add the additional seconds to the overall time accrued
			PRINTLN("[ML] Adding time to player's local time accrued value: ", EventData.iAmountToIncrement)
			PRINTLN("[ML] New total time acquired is : ", MC_playerBD_1[iLocalPart].iArenaTrialTotalTimeGained)			
		ENDIF
		
		IF MC_playerBD[iLocalPart].iteam = EventData.iTeam
			VECTOR vPos = GET_POINTS_COORDS(LocalPlayerPed)
			HUD_COLOURS eTextColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(EventData.iTeam, LocalPlayer)
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_ARENA_TRIALS(g_FMMC_STRUCT.iAdversaryModeType)
			AND iLastGoToLocateHit != -1
				eTextColour = MC_GET_HUD_COLOUR_FROM_BLIP_COLOUR(GET_BLIP_COLOUR_FROM_CREATOR(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLastGoToLocateHit].sLocBlipStruct.iBlipColour))
			ENDIF
			TARGET_ADD_FLOATING_SCORE(iTimeToDisplay, vPos,  eTextColour, sFloatingScore)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_START_PRE_COUNTDOWN_STOP(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_START_PRE_COUNTDOWN_STOP EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		PRINTLN("[RCC MISSION][AUDIO] PROCESS_SCRIPT_EVENT_FMMC_START_PRE_COUNTDOWN_STOP - Triggering APT_PRE_COUNTDOWN_STOP")
		TRIGGER_MUSIC_EVENT("APT_PRE_COUNTDOWN_STOP")
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_LOCATE_ALERT_SOUND(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_LOCATE_ALERT_SOUND EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.FromPlayerIndex != PLAYER_ID()
		AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
			PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_LOCATE_ALERT_SOUND - Playing police_notification from DLC_AS_VNT_Sounds")
			PLAY_SOUND_FRONTEND(-1,"police_notification","DLC_AS_VNT_Sounds")
		ENDIF
	ENDIF	
ENDPROC	

//Clears the bit for all other players. So that the player that sends out the event doesn't get to be on the small team twice in a row.
PROC PROCESS_SCRIPT_EVENT_FMMC_CLEAR_HAS_BEEN_ON_SMALLEST_TEAM(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_CLEAR_HAS_BEEN_ON_SMALLEST_TEAM EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF IS_BIT_SET(GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_HAS_JOINED_SMALLEST_TEAM)
			CLEAR_BIT(GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_HAS_JOINED_SMALLEST_TEAM)
			PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_CLEAR_HAS_BEEN_ON_SMALLEST_TEAM - Clearing ciMISSION_DATA_TWO_HAS_JOINED_SMALLEST_TEAM")
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_PROP_DESTROYED(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_PROP_DESTROYED EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF bIsLocalPlayerHost
			IF NOT IS_BIT_SET(MC_serverBD_3.iPropDestroyedBS[GET_LONG_BITSET_INDEX(EventData.iProp)], GET_LONG_BITSET_BIT(EventData.iProp))
				SET_BIT(MC_serverBD_3.iPropDestroyedBS[GET_LONG_BITSET_INDEX(EventData.iProp)], GET_LONG_BITSET_BIT(EventData.iProp))
				PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_PROP_DESTROYED - Prop ", EventData.iProp, " has been destroyed! Setting iPropDestroyedBS[",GET_LONG_BITSET_INDEX(EventData.iProp),"], ",GET_LONG_BITSET_BIT(EventData.iProp),". This prop has been destroyed.")
			ENDIF	
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_RESET_ALL_PROPS(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_RESET_ALL_PROPS EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_RESET_ALL_PROPS
			
			STRING sSoundSetName = "DLC_IE_TW_Player_Sounds"
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_LAND_GRAB_SOUNDS)			
				sSoundSetName = "DLC_SR_LG_Player_Sounds"
			ENDIF
			
			PLAY_SOUND_FRONTEND(-1, "Prop_Reset", sSoundSetName)
			PRINTLN("[LM][TURF WAR][Audio] - PROCESS_SCRIPT_EVENT_FMMC_RESET_ALL_PROPS - Playing Sound: Prop_Reset, DLC_IE_TW_Player_Sounds")
			
			INT iProp = 0
			FOR iProp = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfProps-1
				IF DOES_ENTITY_EXIST(oiProps[iProp])
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_Is_Claimable)
						SET_TURF_WAR_PROP_TO_COLOUR_UNCLAIMED(iProp)
					ENDIF
				ENDIF
				MC_ServerBD_4.iPropOwnedByPart[iProp] = -1
			ENDFOR
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_RESET_INDIVIDUAL_PROP(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_RESET_INDIVIDUAL_PROP EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_RESET_INDIVIDUAL_PROP
			IF DOES_ENTITY_EXIST(oiProps[EventData.iEventProp])
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[EventData.iEventProp].iPropBitset, ciFMMC_PROP_Is_Claimable)
					IF bIsLocalPlayerHost
						CHANGE_PROP_CLAIMING_STATE(ePropClaimingState_Unclaimed, EventData.iEventProp, -1)
					ENDIF
					
					SET_TURF_WAR_PROP_TO_COLOUR_UNCLAIMED(EventData.iEventProp)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_PROP_CLAIMED_SUCCESSFULLY_BY_PART(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_PROP_CLAIMED_SUCCESSFULLY_BY_PART EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_PROP_CLAIMED_SUCCESSFULLY_BY_PART
			IF EventData.iEventPart > -1
				IF EventData.iEventPart = iLocalPart
					MC_PlayerBD[iLocalPart].iPropsClaimedCounter++
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_REQUEST_RIGHTFUL_PROP_COLOUR_FROM_HOST(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_REQUEST_RIGHTFUL_PROP_COLOUR_FROM_HOST EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_REQUEST_RIGHTFUL_PROP_COLOUR_FROM_HOST
			IF bIsLocalPlayerHost
				IF MC_ServerBD_4.ePropClaimState[EventData.iEventProp] = INT_TO_ENUM(ePropClaimingState, EventData.iEventPropState)
				AND GET_OBJECT_TINT_INDEX(oiProps[EventData.iEventProp]) != EventData.iEventPropColour
					PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_RECEIVE_RIGHTFUL_PROP_COLOUR_FROM_HOST] - (HOST) Giving the go ahead for iPart: ", EventData.iEventPart, " to change the colour of iProp: ", EventData.iEventProp, " to state colour ", EventData.iEventPropState)
					BROADCAST_FMMC_RECEIVE_RIGHTFUL_PROP_COLOUR_FROM_HOST(EventData.iEventPart, EventData.iEventProp, EventData.iEventPropState)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_RECEIVE_RIGHTFUL_PROP_COLOUR_FROM_HOST(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_REQUEST_RIGHTFUL_PROP_COLOUR_FROM_HOST EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_REQUEST_RIGHTFUL_PROP_COLOUR_FROM_HOST
			IF EventData.iEventPart = iPartToUse
				PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_RECEIVE_RIGHTFUL_PROP_COLOUR_FROM_HOST] - (Client) Received the go ahead to change the colour of iProp: ", EventData.iEventProp, " to state colour ", EventData.iEventPropState)
				HANDLE_PROP_CLAIMING_CLIENT_PREDICT_PVP(EventData.iEventProp, EventData.iEventPropState)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_PROP_CONTESTED(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_PROP_CONTESTED EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_PROP_CONTESTED
			
			PED_INDEX pedPlayer = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(EventData.iEventPart)))
			
			STRING sSoundSetName
									
			IF EventData.iEventProp = MC_PlayerBD[iPartToUse].iCurrentPropHit[0]
			OR EventData.iEventProp = MC_PlayerBD[iPartToUse].iCurrentPropHit[1]
				sSoundSetName = "DLC_IE_TW_Player_Sounds"
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_LAND_GRAB_SOUNDS)
					sSoundSetName = "DLC_SR_LG_Player_Sounds"
				ENDIF
				PLAY_SOUND_FRONTEND(-1, "Prop_Contested", sSoundSetName)
				PRINTLN("[LM][TURF WAR][Audio] - PROCESS_SCRIPT_EVENT_FMMC_PROP_CONTESTED - Playing Sound: Prop_Contested, ", sSoundSetName)
			ELIF EventData.bEventTeamContesting[MC_PlayerBD[iPartToUse].iTeam]
				sSoundSetName = "DLC_IE_TW_Team_Sounds"
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_LAND_GRAB_SOUNDS)
					sSoundSetName = "DLC_SR_LG_Team_Sounds"
				ENDIF
				PLAY_SOUND_FRONTEND(-1, "Prop_Contested", sSoundSetName)
				PRINTLN("[LM][TURF WAR][Audio] - PROCESS_SCRIPT_EVENT_FMMC_PROP_CONTESTED - Playing Sound: Prop_Contested, ", sSoundSetName)
			ELSE
				sSoundSetName = "DLC_IE_TW_Enemy_Sounds"
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_LAND_GRAB_SOUNDS)
					sSoundSetName = "DLC_SR_LG_Enemy_Sounds"
				ENDIF
				PLAY_SOUND_FRONTEND(-1, "Prop_Contested", sSoundSetName)
				PRINTLN("[LM][TURF WAR][Audio] - PROCESS_SCRIPT_EVENT_FMMC_PROP_CONTESTED - Playing Sound: Prop_Contested, ", sSoundSetName)
			ENDIF
			
			VECTOR vSpawnPos
			VECTOR vSpawnRot
			IF NOT IS_PED_INJURED(pedPlayer)
				VEHICLE_INDEX vehPlayer
				IF IS_PED_IN_ANY_VEHICLE(pedPlayer)
					vehPlayer = GET_VEHICLE_PED_IS_IN(pedPlayer)
					vSpawnPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehPlayer, <<0.0, 2.5, -1.1>>)
					
					vSpawnRot = GET_ENTITY_ROTATION(vehPlayer)
					vSpawnRot += <<0.0, 0.0, 45.0>>
				ELSE
					vSpawnPos = g_FMMC_STRUCT_ENTITIES.sPlacedProp[EventData.iEventProp].vPos
				ENDIF
			ELSE
				vSpawnPos = g_FMMC_STRUCT_ENTITIES.sPlacedProp[EventData.iEventProp].vPos
			ENDIF
			
			FLOAT fScale = 2.0
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[EventData.iEventProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_impexp_bblock_qp3"))
			OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[EventData.iEventProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("bkr_prop_biker_bblock_cor_03"))
			OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[EventData.iEventProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("bkr_prop_biker_bblock_huge_01"))
			OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[EventData.iEventProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("bkr_prop_biker_tube_gap_01"))
			OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[EventData.iEventProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("bkr_prop_biker_tube_gap_01"))
			OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[EventData.iEventProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_crn"))
			OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[EventData.iEventProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("bkr_prop_biker_tube_xxs"))
			OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[EventData.iEventProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("bkr_prop_biker_tube_xxs"))
				fScale = 4.0
			ENDIF
			
			FLOAT fR, fG, fB
			INT iR, iG, iB, iA					
			HUD_COLOURS tempColour
			tempColour = HUD_COLOUR_REDDARK
			GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(tempColour), iR, iG, iB, iA)
			fR = TO_FLOAT(iR)/255.0
			fG = TO_FLOAT(iG)/255.0
			fB = TO_FLOAT(iB)/255.0
			SET_PARTICLE_FX_NON_LOOPED_COLOUR(fR, fG, fB)
			
			STRING strUse = "scr_ie_tw"
			STRING strName = "scr_impexp_tw_take_zone"
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARZONE(g_FMMC_STRUCT.iAdversaryModeType)
				strUse = "scr_sr_adversary"
				strName = "scr_sr_lg_take_zone"
			ENDIF
			
			IF HAS_NAMED_PTFX_ASSET_LOADED(strUse)
				USE_PARTICLE_FX_ASSET(strUse)
				START_PARTICLE_FX_NON_LOOPED_AT_COORD(strName, vSpawnPos, vSpawnRot, fScale)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_PROP_TURF_WARS_TEAM_LEADING_CHANGED(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_PROP_TURF_WARS_TEAM_LEADING_CHANGED EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_TURF_WARS_TEAM_LEADING_CHANGED
			PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_PROP_TURF_WARS_TEAM_LEADING_CHANGED] - Setting Bit: LBOOL20_TURFWAR_ICON_SHOULD_PULSE")
			iPropsHighestTeamForHUD = EventData.iEventTeam
			
			IF EventData.iEventTeam != -1
			AND EventData.iEventTeam != EventData.iEventTeamOld
				SET_BIT(iLocalBoolCheck20, LBOOL20_TURFWAR_ICON_SHOULD_PULSE_0 + EventData.iEventTeam)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_PROP_CLAIMED(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_PROP_CLAIMED EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_PROP_CLAIMED
			IF EventData.iEventPart > -1
				INT iTeamA = MC_PlayerBD[iPartToUse].iTeam
				INT iTeamB = MC_PlayerBD[EventData.iEventPart].iTeam
				
				PED_INDEX pedPlayer = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(EventData.iEventPart)))
				
				STRING sSoundSetName
				
				IF NOT IS_PROP_BEING_HIT_CURRENTLY_BY_ANYONE_ELSE(EventData.iEventProp, EventData.iEventPart)
					IF NOT IS_PED_INJURED(pedPlayer)
						
						PRINTLN("[LM][TURF WAR][Audio] - PROCESS_SCRIPT_EVENT_FMMC_PROP_CLAIMED - iPart, ", iPartToUse)
						PRINTLN("[LM][TURF WAR][Audio] - PROCESS_SCRIPT_EVENT_FMMC_PROP_CLAIMED - iEventPart, ", EventData.iEventPart)
					
						IF EventData.iEventPart = iPartToUse
							sSoundSetName = "DLC_IE_TW_Player_Sounds"
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_LAND_GRAB_SOUNDS)
								sSoundSetName = "DLC_SR_LG_Player_Sounds"
							ENDIF
							
							If bUsingBirdsEyeCam
								PLAY_SOUND_FRONTEND(-1, "Prop_Tagged", sSoundSetName)
							ELSE
								PLAY_SOUND_FROM_ENTITY(-1, "Prop_Tagged", pedPlayer, sSoundSetName)
							ENDIF
							
							PRINTLN("[LM][TURF WAR][Audio] - PROCESS_SCRIPT_EVENT_FMMC_PROP_CLAIMED - Playing Sound: Prop_Tagged, ", sSoundSetName)
						ELIF iTeamA = iTeamB
							sSoundSetName = "DLC_IE_TW_Team_Sounds"
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_LAND_GRAB_SOUNDS)
								sSoundSetName = "DLC_SR_LG_Team_Sounds"
							ENDIF
							PLAY_SOUND_FROM_ENTITY(-1, "Prop_Tagged", pedPlayer, sSoundSetName)
							PRINTLN("[LM][TURF WAR][Audio] - PROCESS_SCRIPT_EVENT_FMMC_PROP_CLAIMED - Playing Sound: Prop_Tagged, ", sSoundSetName)
						ELSE
							sSoundSetName = "DLC_IE_TW_Enemy_Sounds"
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_LAND_GRAB_SOUNDS)
								sSoundSetName = "DLC_SR_LG_Enemy_Sounds"
							ENDIF
							PLAY_SOUND_FROM_ENTITY(-1, "Prop_Tagged", pedPlayer, sSoundSetName)
							PRINTLN("[LM][TURF WAR][Audio] - PROCESS_SCRIPT_EVENT_FMMC_PROP_CLAIMED - Playing Sound: Prop_Tagged, ", sSoundSetName)
						ENDIF
					ENDIF
					
					VECTOR vSpawnPos
					VECTOR vSpawnRot
					IF NOT IS_PED_INJURED(pedPlayer)
						VEHICLE_INDEX vehPlayer
						IF IS_PED_IN_ANY_VEHICLE(pedPlayer)
							vehPlayer = GET_VEHICLE_PED_IS_IN(pedPlayer)
							vSpawnPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehPlayer, <<0.0, 2.5, -1.1>>)
							
							vSpawnRot = GET_ENTITY_ROTATION(vehPlayer)
							vSpawnRot += <<0.0, 0.0, 45.0>>
						ELSE
							vSpawnPos = g_FMMC_STRUCT_ENTITIES.sPlacedProp[EventData.iEventProp].vPos
						ENDIF
					ELSE
						vSpawnPos = g_FMMC_STRUCT_ENTITIES.sPlacedProp[EventData.iEventProp].vPos
					ENDIF
					
					FLOAT fScale = 2.0
					
					IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[EventData.iEventProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_impexp_bblock_qp3"))
					OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[EventData.iEventProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("bkr_prop_biker_bblock_cor_03"))
					OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[EventData.iEventProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("bkr_prop_biker_bblock_huge_01"))
					OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[EventData.iEventProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("bkr_prop_biker_tube_gap_01"))
					OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[EventData.iEventProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("bkr_prop_biker_tube_gap_01"))
					OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[EventData.iEventProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_stunt_tube_crn"))
					OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[EventData.iEventProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("bkr_prop_biker_tube_xxs"))
					OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[EventData.iEventProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("bkr_prop_biker_tube_xxs"))
						fScale = 4.0
					ENDIF
					
					FLOAT fR, fG, fB
					INT iR, iG, iB, iA					
					HUD_COLOURS tempColour
					tempColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_playerBD[EventData.iEventPart].iteam, LocalPlayer)
					GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(tempColour), iR, iG, iB, iA)
					fR = TO_FLOAT(iR)/255.0
					fG = TO_FLOAT(iG)/255.0
					fB = TO_FLOAT(iB)/255.0
					SET_PARTICLE_FX_NON_LOOPED_COLOUR(fR, fG, fB)
					
					STRING strUse = "scr_ie_tw"
					STRING strName = "scr_impexp_tw_take_zone"
					IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARZONE(g_FMMC_STRUCT.iAdversaryModeType)
						strUse = "scr_sr_adversary"
						strName = "scr_sr_lg_take_zone"
					ENDIF
					
					IF HAS_NAMED_PTFX_ASSET_LOADED(strUse)
						USE_PARTICLE_FX_ASSET(strUse)
						START_PARTICLE_FX_NON_LOOPED_AT_COORD(strName, vSpawnPos, vSpawnRot, fScale)
					ENDIF					
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_TOGGLE_PROP_CLAIMING(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_TOGGLE_PROP_CLAIMING EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_TOGGLE_PROP_CLAIMING
			IF EventData.bEventAll
				IF EventData.bEventAllowPropClaiming
					PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_TOGGLE_PROP_CLAIMING] - Setting: LBOOL20_TURF_WAR_DISABLE_PROP_CLAIM_LOCALLY ALL")
					CLEAR_BIT(iLocalBoolCheck20, LBOOL20_TURF_WAR_DISABLE_PROP_CLAIM_LOCALLY)
				ELSE
					PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_TOGGLE_PROP_CLAIMING] - Clearing: LBOOL20_TURF_WAR_DISABLE_PROP_CLAIM_LOCALLY ALL")
					SET_BIT(iLocalBoolCheck20, LBOOL20_TURF_WAR_DISABLE_PROP_CLAIM_LOCALLY)					
				ENDIF
			ELSE
				IF EventData.iEventTeam = MC_PlayerBD[iLocalPart].iteam
					IF EventData.bEventAllowPropClaiming
						PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_TOGGLE_PROP_CLAIMING] - Setting: LBOOL20_TURF_WAR_DISABLE_PROP_CLAIM_LOCALLY (iEventPart: ", EventData.iEventTeam, ")")
						CLEAR_BIT(iLocalBoolCheck20, LBOOL20_TURF_WAR_DISABLE_PROP_CLAIM_LOCALLY)
					ELSE
						PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_TOGGLE_PROP_CLAIMING] - Clearing: LBOOL20_TURF_WAR_DISABLE_PROP_CLAIM_LOCALLY (iEventPart: ", EventData.iEventTeam, ")")
						SET_BIT(iLocalBoolCheck20, LBOOL20_TURF_WAR_DISABLE_PROP_CLAIM_LOCALLY)
					ENDIF
				ENDIF
				
				IF EventData.iEventPart = iLocalPart
					IF EventData.bEventAllowPropClaiming
						PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_TOGGLE_PROP_CLAIMING] - Setting: LBOOL20_TURF_WAR_DISABLE_PROP_CLAIM_LOCALLY (iEventPart: ", EventData.iEventPart, ")")
						CLEAR_BIT(iLocalBoolCheck20, LBOOL20_TURF_WAR_DISABLE_PROP_CLAIM_LOCALLY)
					ELSE
						PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_TOGGLE_PROP_CLAIMING] - Clearing: LBOOL20_TURF_WAR_DISABLE_PROP_CLAIM_LOCALLY (iEventPart: ", EventData.iEventPart, ")")
						SET_BIT(iLocalBoolCheck20, LBOOL20_TURF_WAR_DISABLE_PROP_CLAIM_LOCALLY)
					ENDIF
				ENDIF
			ENDIF			
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_CLAIM_PROP(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_CLAIM_PROP EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_CLAIM_PROP
			IF DOES_ENTITY_EXIST(oiProps[EventData.iEventProp])
				PRINTLN("[LM][TURF WAR] - PROCESS_SCRIPT_EVENT_FMMC_CLAIM_PROP - INFO: _____________________________")
				PRINTLN("[LM][TURF WAR] - PROCESS_SCRIPT_EVENT_FMMC_CLAIM_PROP - EventData.iProp: ", EventData.iEventProp, "	PlayerPart: ", EventData.iEventPart, "	iTeam: ", EventData.iEventTeam)
				
				IF bIsLocalPlayerHost
					BOOL bTeamIsCurrentlyOnProp[FMMC_MAX_TEAMS]					
					INT iTeam
					INT iCountPlayers
					INT iPart = 0
					FOR iPart = 0 TO MAX_NUM_MC_PLAYERS-1
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, iPart))
						AND NOT IS_PARTICIPANT_A_SPECTATOR(iPart)
							iTeam = MC_playerBD[iPart].iteam
							
							IF MC_PlayerBD[iPart].iCurrentPropHit[0] = EventData.iEventProp
							OR MC_PlayerBD[iPart].iCurrentPropHit[1] = EventData.iEventProp
								PRINTLN("[LM][TURF WAR][PROCESS_SCRIPT_EVENT_FMMC_CLAIM_PROP] - iPart: ", iPart, "  We're currently on iProp: ", EventData.iEventProp)
								PRINTLN("[LM][TURF WAR][PROCESS_SCRIPT_EVENT_FMMC_CLAIM_PROP] - MC_PlayerBD[iPart].iCurrentPropHit[0]: ", MC_PlayerBD[iPart].iCurrentPropHit[0])
								PRINTLN("[LM][TURF WAR][PROCESS_SCRIPT_EVENT_FMMC_CLAIM_PROP] - MC_PlayerBD[iPart].iCurrentPropHit[1]: ", MC_PlayerBD[iPart].iCurrentPropHit[1])
								bTeamIsCurrentlyOnProp[iTeam] = TRUE							
							ENDIF
							iCountPlayers++
						ENDIF
						IF iCountPlayers >= (MC_ServerBD.iNumberOfPlayingPlayers[0] + MC_ServerBD.iNumberOfPlayingPlayers[1] + MC_ServerBD.iNumberOfPlayingPlayers[2] + MC_ServerBD.iNumberOfPlayingPlayers[3])
							PRINTLN("[LM][TURF WAR][PROCESS_SCRIPT_EVENT_FMMC_CLAIM_PROP] - iCountPlayers: ", iPart, "  Total Players: ", 
								(MC_ServerBD.iNumberOfPlayingPlayers[0] + MC_ServerBD.iNumberOfPlayingPlayers[1] + MC_ServerBD.iNumberOfPlayingPlayers[2] + MC_ServerBD.iNumberOfPlayingPlayers[3]), " BREAKLOOP")
							BREAKLOOP
						ENDIF
					ENDFOR
					
					INT iContestantsForProp = 0
					FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
						IF bTeamIsCurrentlyOnProp[iTeam]
							iContestantsForProp++
							PRINTLN("[LM][TURF WAR][PROCESS_SCRIPT_EVENT_FMMC_CLAIM_PROP] - iContestantsForProp: ", iContestantsForProp, " for iProp: ", EventData.iEventProp)
						ENDIF
					ENDFOR
				
					INT iNewState
					iNewState = 0
					SWITCH MC_ServerBD_4.ePropClaimState[EventData.iEventProp]
						// If the prop is unclaimed, then set the prop to whoever desires it first. Contested is dealt with on claimed props.
						CASE ePropClaimingState_Unclaimed
							PRINTLN("[LM][TURF WAR][PROCESS_SCRIPT_EVENT_FMMC_CLAIM_PROP] - calling CHANGE_PROP_CLAIMING_STATE")
							iNewState = ENUM_TO_INT(ePropClaimingState_Claimed_Team_0)
							iNewState += EventData.iEventTeam
							CHANGE_PROP_CLAIMING_STATE(INT_TO_ENUM(ePropClaimingState, iNewState), EventData.iEventProp, EventData.iEventTeam, EventData.iEventPart)						
							BROADCAST_FMMC_PROP_CLAIMED(EventData.iEventPart, EventData.iEventProp)
						BREAK
						
						// If the prop is claimed, and we are on the team it is claimed by, then set it to contested.
						CASE ePropClaimingState_Claimed_Team_0
						CASE ePropClaimingState_Claimed_Team_1
						CASE ePropClaimingState_Claimed_Team_2
						CASE ePropClaimingState_Claimed_Team_3
							// Team desires the prop, and if they're the only one who wants it and If they don't own it, then give it to the Team who desires it.
							// More than one team wants this prop, so set it to contested.
							IF iContestantsForProp > 1
								PRINTLN("[LM][TURF WAR][PROCESS_SCRIPT_EVENT_FMMC_CLAIM_PROP] - calling CHANGE_PROP_CLAIMING_STATE")
								CHANGE_PROP_CLAIMING_STATE(ePropClaimingState_Contested, EventData.iEventProp, EventData.iEventTeam, -1)
								
								BROADCAST_FMMC_PROP_CONTESTED(EventData.iEventProp, bTeamIsCurrentlyOnProp, EventData.iEventPart)
							ELSE
								IF iContestantsForProp = 1
									IF NOT DOES_THIS_TEAM_OWN_CLAIMED_PROP(EventData.iEventTeam, EventData.iEventProp)
										PRINTLN("[LM][TURF WAR][PROCESS_SCRIPT_EVENT_FMMC_CLAIM_PROP] - calling CHANGE_PROP_CLAIMING_STATE")
										iNewState = ENUM_TO_INT(ePropClaimingState_Claimed_Team_0)
										iNewState += EventData.iEventTeam
										CHANGE_PROP_CLAIMING_STATE(INT_TO_ENUM(ePropClaimingState, iNewState), EventData.iEventProp, EventData.iEventTeam, EventData.iEventPart)
										
										BROADCAST_FMMC_PROP_CLAIMED(EventData.iEventPart, EventData.iEventProp)
									ENDIF
								ENDIF
							ENDIF
						BREAK
						
						// If the prop is contested then we need to check if anyone still desires it.
						CASE ePropClaimingState_Contested
							IF iContestantsForProp = 0
								PRINTLN("[LM][TURF WAR][PROCESS_SCRIPT_EVENT_FMMC_CLAIM_PROP] - calling CHANGE_PROP_CLAIMING_STATE")
								CHANGE_PROP_CLAIMING_STATE(ePropClaimingState_Unclaimed, EventData.iEventProp, EventData.iEventTeam, -1)
							ELIF iContestantsForProp = 1
								iNewState = ENUM_TO_INT(ePropClaimingState_Claimed_Team_0)
								iNewState += EventData.iEventTeam
								CHANGE_PROP_CLAIMING_STATE(INT_TO_ENUM(ePropClaimingState, iNewState), EventData.iEventProp, EventData.iEventPart)
								
								BROADCAST_FMMC_PROP_CLAIMED(EventData.iEventPart, EventData.iEventProp)
							ENDIF
						BREAK
					ENDSWITCH
				ENDIF
				
				INT iMyTeam = MC_PlayerBD[iPartToUse].iteam
								
				// Predict Claiming...								
				INT iNewState
				iNewState = ENUM_TO_INT(ePropClaimingState_Claimed_Team_0)
				iNewState += EventData.iEventTeam
				
				IF NOT IS_PROP_BEING_HIT_CURRENTLY_BY_ANYONE_ELSE(EventData.iEventProp, EventData.iEventPart)
					IF (MC_PlayerBD[iPartToUse].iCurrentPropHit[0] = EventData.iEventProp OR MC_PlayerBD[iPartToUse].iCurrentPropHit[1] = EventData.iEventProp)
					AND MC_ServerBD_4.ePropClaimState[EventData.iEventProp] != ePropClaimingState_Contested
						HANDLE_PROP_CLAIMING_CLIENT_PREDICT_PVP(EventData.iEventProp, ENUM_TO_INT(ePropClaimingState_Claimed_Team_0)+iMyTeam)
					ELSE
						HANDLE_PROP_CLAIMING_CLIENT_PREDICT_PVP(EventData.iEventProp, iNewState)
					ENDIF
				ENDIF
				
				PRINTLN("[LM][TURF WAR][PROCESS_SCRIPT_EVENT_FMMC_CLAIM_PROP] - EventData.iEventTeam: ", EventData.iEventTeam, "	iNewState: ", iNewState)				
			ENDIF		
		ENDIF	
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_MOVE_FMMC_YACHT(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_MOVE_FMMC_YACHT EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_MOVE_FMMC_YACHT
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				SET_BIT(MC_serverBD.iServerBitSet7, SBBOOL7_YACHT_CUTSCENE_RUNNING)
				PRINTLN("[MYACHT] Starting yacht cutscene!")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_SHARD_BOMBUSHKA_MINI_ROUND_END_EARLY(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_SHARD_BOMBUSHKA_MINI_ROUND_END_EARLY EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_SHARD_BOMBUSHKA_MINI_ROUND_END_EARLY
			TEXT_LABEL_15 tl15 = ""
			
			IF EventData.iTeam = MC_PlayerBD[iPartToUse].iTeam
				tl15 = "BBMUSHK_WN"
			ELSE
				tl15 = "BBMUSHK_LS"
			ENDIF
			
			SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, "BBMUSHK_PT", tl15)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_PLAY_PULSE_SFX(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_PLAY_PULSE_SFX EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_PLAY_PULSE_SFX
			PRINTLN("[RCC MISSION] - Playing Bounds_Timer_Reset. (Client)")			
			PLAY_SOUND_FRONTEND(-1, "Bounds_Timer_Reset", "DLC_SM_VEHWA_Player_Sounds", FALSE)			
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_BOMBUSHKA_NEW_ROUND_SHARD(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_BOMBUSHKA_NEW_ROUND_SHARD EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_BOMBUSHKA_NEW_ROUND_SHARD
			
			INT iTeamInLead
			INT iScoreToShow
			TEXT_LABEL_15 tl15
			HUD_COLOURS hcOverride
			
			IF EventData.iScoreTeam1 > EventData.iScoreTeam2
				tl15 = "MC_ORA_BMB"
				iScoreToShow = EventData.iScoreTeam1
				iTeamInLead = 1
			ELSE
				tl15 = "MC_PURP_BMB"
				iScoreToShow = EventData.iScoreTeam2
				iTeamInLead = 2
			ENDIF
			
			IF MC_PlayerBD[iPartToUse].iTeam = iTeamInLead
				hcOverride = HUD_COLOUR_BLUE
			ELSE
				hcOverride = HUD_COLOUR_RED
			ENDIF
			
			SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING_AND_COLOUR(BIG_MESSAGE_GENERIC_TEXT, iScoreToShow, "BMB_SCR_NRb", "BMB_SCR_NRa", DEFAULT, DEFAULT, DEFAULT, DEFAULT, hcOverride, tl15)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_SHOWDOWN_DAMAGE(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_SHOWDOWN_DAMAGE EventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		
		FLOAT fDamage = 0
	
		SWITCH EventData.iDmgType
			CASE SHOWDOWNDMG_STANDARD
				PRINTLN("[SHOWDOWN] Recieved a STANDARD damage event")
				fDamage = g_FMMC_STRUCT.fShowdown_PointsDamagePerHit
			BREAK
			CASE SHOWDOWNDMG_EXPLOSION
				PRINTLN("[SHOWDOWN] Recieved an EXPLOSION damage event")
				fDamage = g_FMMC_STRUCT.fShowdown_PointsDamagePerHit * 3
			BREAK
			CASE SHOWDOWNDMG_DRAIN
				PRINTLN("[SHOWDOWN] Recieved a DRAIN damage event")
				fDamage = GET_SHOWDOWN_DRAIN_AMOUNT()
			BREAK
		ENDSWITCH
		
		MC_playerBD_1[iLocalPart].iShowdown_LastDamageType = EventData.iDmgType
		
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_SHOWDOWN_DAMAGE
			IF EventData.iVictimPart = iLocalPart
			OR EventData.iVictimPart = -1
				MC_playerBD_1[iLocalPart].fShowdownPoints -= fDamage
				PRINTLN("[SHOWDOWN] I'm the victim of a Showdown Damage event! Removing ", fDamage, " from my points total and I now have: ", MC_playerBD_1[iLocalPart].fShowdownPoints)
			ENDIF
			IF EventData.iAttackerPart = iLocalPart
			AND MC_playerBD_1[iLocalPart].fShowdownPoints > 0.0
				MC_playerBD_1[iLocalPart].fShowdownPoints += fDamage
				MC_playerBD_1[iLocalPart].fShowdown_PointsStolenTotal += fDamage
				PRINTLN("[SHOWDOWN] I'm the attacker of a Showdown Damage event! Adding ", fDamage, " to my points total and I now have: ", MC_playerBD_1[iLocalPart].fShowdownPoints)
			ENDIF
			
			IF MC_playerBD_1[iLocalPart].fShowdownPoints > g_FMMC_STRUCT.fShowdown_MaximumPoints
				MC_playerBD_1[iLocalPart].fShowdownPoints = g_FMMC_STRUCT.fShowdown_MaximumPoints
			ENDIF
		ENDIF
		
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_SHOWDOWN_ELIMINATION(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_SHOWDOWN_ELIMINATION EventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_SHOWDOWN_ELIMINATION
			IF EventData.iAttackerPart = iLocalPart
				MC_playerBD_1[iLocalPart].iShowdownEliminations += 1
				MC_playerBD[iLocalPart].iKillScore += 1
				MC_playerBD[iPartToUse].iNumPlayerKills += 1
				PRINTLN("[SHOWDOWN] I'm the attacker of a Showdown ELIMINATION event! Adding to my kill score and I now have: ", MC_playerBD[iLocalPart].iKillScore)
			ENDIF
			
			IF EventData.iVictimPart > -1
			AND EventData.iAttackerPart > -1
				PARTICIPANT_INDEX partVictim = INT_TO_PARTICIPANTINDEX(EventData.iVictimPart)
				PARTICIPANT_INDEX partKiller = INT_TO_PARTICIPANTINDEX(EventData.iAttackerPart)
				
				PLAYER_INDEX pVictim, pKiller
				
				IF NETWORK_IS_PARTICIPANT_ACTIVE(partVictim)
					pVictim = NETWORK_GET_PLAYER_INDEX(partVictim)
				ENDIF
				IF NETWORK_IS_PARTICIPANT_ACTIVE(partKiller)
					pKiller = NETWORK_GET_PLAYER_INDEX(partKiller)
				ENDIF
			
				PRINT_TICKER_WITH_TWO_PLAYER_NAMES("TICK_ELIM", pKiller, pVictim)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_CCTV_REACHED_DESTINATION(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_CCTV_REACHED_DESTINATION EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_CCTV_REACHED_DESTINATION
			IF NOT IS_BIT_SET(iCCTVReachedDestinationBS, EventData.iCCTVIndex)
				SET_BIT(iCCTVReachedDestinationBS, EventData.iCCTVIndex)
				PRINTLN("[CCTV][CAM ", EventData.iCCTVIndex, "][RCC MISSION] - iCam: ", EventData.iCCTVIndex, " PROCESS_SCRIPT_EVENT_FMMC_CCTV_REACHED_DESTINATION | Setting iCCTVReachedDestinationBS")
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_METAL_DETECTOR_ALERTED(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_METAL_DETECTOR EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_METAL_DETECTOR_ALERTED
			PRINTLN("[MetalDetector] Metal Detector Alerted event received! EventData.iZone: ", EventData.iZone, " / EventData.iDetectedPart: ", EventData.iDetectedPart, " / EventData.bExtremeWeapon: ", EventData.bExtremeWeapon)
			SET_METAL_DETECTOR_AS_ALERTED(EventData.iZone, EventData.vDetectionPos, EventData.bExtremeWeapon)
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_INTERACT_WITH_EVENT(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_INTERACT_WITH_EVENT EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_INTERACT_WITH_EVENT
		
			// Exploding/clearing up all persistent props
			IF EventData.bDeletePersistentProps
				INTERACT_WITH_DESTROY_ALL_PERSISTENT_PROPS(FALSE)
			ENDIF
			
			IF EventData.bExplodePersistentProps
				INTERACT_WITH_DESTROY_ALL_PERSISTENT_PROPS(TRUE)
			ENDIF
			
			// Planting explosives
			IF EventData.bPlantedLeftSideBomb
				IF bIsLocalPlayerHost
					MC_serverBD_1.iCurrentVaultDoorPlantedExplosives_LeftSide++
					PRINTLN("[InteractWith][PlantExplosives] iCurrentVaultDoorPlantedExplosives_LeftSide is now ", MC_serverBD_1.iCurrentVaultDoorPlantedExplosives_LeftSide)
				ENDIF
				
			ELIF EventData.bPlantedRightSideBomb
				IF bIsLocalPlayerHost
					MC_serverBD_1.iCurrentVaultDoorPlantedExplosives_RightSide++
					PRINTLN("[InteractWith][PlantExplosives] iCurrentVaultDoorPlantedExplosives_RightSide is now ", MC_serverBD_1.iCurrentVaultDoorPlantedExplosives_RightSide)
				ENDIF
				
			ENDIF
			
			IF EventData.iBombPlanterIndex > -1
				IF bIsLocalPlayerHost
					MC_serverBD_4.iInteractWith_BombPlantParticipant = EventData.iBombPlanterIndex
					PRINTLN("[InteractWith][CasinoTunnelExplosion] iInteractWith_BombPlantParticipant is now ", MC_serverBD_4.iInteractWith_BombPlantParticipant)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_UPDATE_DOOR_LINKED_ENTITIES(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_UPDATE_DOOR_LINKED_OBJS EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_UPDATE_DOOR_LINKED_ENTITIES
			iDoorHasLinkedEntityBS = EventData.iDoorHasLinkedEntityBS
			PRINTLN("[MCDoors] PROCESS_SCRIPT_EVENT_FMMC_UPDATE_DOOR_LINKED_ENTITIES | iDoorHasLinkedEntityBS is now ", iDoorHasLinkedEntityBS)
		ENDIF
	ENDIF
	
ENDPROC
	
PROC PROCESS_SCRIPT_EVENT_FMMC_ZONE_TIMER(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_ZONE_TIMER EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_ZONE_TIMER
			PRINTLN("[ZONETIMER] Zone Timer event received! EventData.iZone: ", EventData.iZone, " / EventData.bStartTimer: ", EventData.bStartTimer, " / EventData.bStopTimer: ", EventData.bStopTimer)
			
			IF EventData.bStartTimer
				IF NOT HAS_NET_TIMER_STARTED(stZoneTimers[EventData.iZone])
					REINIT_NET_TIMER(stZoneTimers[EventData.iZone])
					PRINTLN("[ZONETIMER] Starting Zone Timer! Zone: ", EventData.iZone)
				ELSE
					PRINTLN("[ZONETIMER] Zone Timer already started! Zone: ", EventData.iZone)
				ENDIF
			ENDIF
			
			IF EventData.bStopTimer
				IF HAS_NET_TIMER_STARTED(stZoneTimers[EventData.iZone])
					RESET_NET_TIMER(stZoneTimers[EventData.iZone])
					PRINTLN("[ZONETIMER] Stopping Zone Timer! Zone: ", EventData.iZone)
				ELSE
					PRINTLN("[ZONETIMER] Requested to stop the zone timer, but this Zone Timer isn't running anyway! Zone: ", EventData.iZone)
				ENDIF
			ENDIF
			
			IF EventData.bMarkTimerAsComplete
				PRINTLN("[ZONETIMER] Marking Zone Timer as complete! Zone: ", EventData.iZone)
				SET_ZONE_TIMER_AS_COMPLETED(EventData.iZone)
			ENDIF
			
			IF EventData.bSkipTimer
				PRINTLN("[ZONETIMER] Skipping Zone Timer! Zone: ", EventData.iZone)
				SET_ZONE_TIMER_AS_COMPLETED(EventData.iZone)
			ENDIF
			
			IF EventData.bHideTimer
				PRINTLN("[ZONETIMER] Hiding Zone Timer! Zone: ", EventData.iZone)
				SET_ZONE_TIMER_AS_HIDDEN(EventData.iZone)
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_POISON_GAS(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_POISON_GAS EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_POISON_GAS
			PRINTLN("[PGAS] Poison Gas event received! EventData.bTriggerGas: ", EventData.bTriggerGas)
			
			IF EventData.bTriggerGas
				SET_BIT(iPoisonGasZoneBS, ciPOISONGASZONEBS_TriggerGas)
				PRINTLN("[PGAS] Setting ciPOISONGASZONEBS_TriggerGas!")
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_DIRECTIONAL_DOOR_EVENT(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_DIRECTIONAL_DOOR_EVENT EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_DIRECTIONAL_DOOR_EVENT
		
			PRINTLN("[DirDoorLock] Directional Door event received! EventData.iDoor: ", EventData.iDoor, " | EventData.bUnlock: ", EventData.bUnlock, " | EventData.bRelock: ", EventData.bRelock)
			
			IF EventData.bUnlock
				//SET_BIT(iDirectionalDoorLocallyUnlockedBS, EventData.iDoor)
				IF bIsLocalPlayerHost
					SET_BIT(MC_serverBD_4.iHostDirectionalDoorUnlockedBS, EventData.iDoor)
					PRINTLN("[DirDoorLock] UNLOCKING - Host setting iHostDirectionalDoorUnlockedBS ", EventData.iDoor)
				ENDIF
				
				SET_BIT(iDirectionalDoorShouldUpdateBS, EventData.iDoor)
				CLEAR_BIT(iDoorUpdatedBS, EventData.iDoor)
				PRINTLN("[DirDoorLock] UNLOCKING - Setting iDirectionalDoorLocallyUnlockedBS ", EventData.iDoor)
			ENDIF
			
			IF EventData.bRelock
				
				IF bIsLocalPlayerHost
					CLEAR_BIT(MC_serverBD_4.iHostDirectionalDoorUnlockedBS, EventData.iDoor)
					PRINTLN("[DirDoorLock] RE-LOCKING - Host clearing iHostDirectionalDoorUnlockedBS ", EventData.iDoor)
				ENDIF
				
				CLEAR_BIT(iDirectionalDoorLocallyUnlockedBS, EventData.iDoor) // Clear this on everyone so it can be re-set if needed
				SET_BIT(iDirectionalDoorShouldUpdateBS, EventData.iDoor)
				CLEAR_BIT(iDoorUpdatedBS, EventData.iDoor)
				PRINTLN("[DirDoorLock] RE-LOCKING - Clearing iDirectionalDoorLocallyUnlockedBS ", EventData.iDoor)
			ENDIF
			
			IF EventData.fAutomaticDistance != -1.0
				fDirectionalDoorAutoDistance[EventData.iDoor] = EventData.fAutomaticDistance
				PRINTLN("[DirDoorLock] Setting fDirectionalDoorAutoDistance[", EventData.iDoor, "] to ", fDirectionalDoorAutoDistance[EventData.iDoor])
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_TRIGGER_AGGRO_FOR_TEAM_LEGACY(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_TRIGGER_AGGRO_FOR_TEAM_LEGACY EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_TRIGGER_AGGRO_FOR_TEAM_LEGACY
			PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_TRIGGER_AGGRO_FOR_TEAM_LEGACY event received! EventData.iTeam: ", EventData.iTeam)
			
			IF bIsLocalPlayerHost
				IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_0 + EventData.iTeam)
					SET_BIT(MC_serverBD.iServerBitSet1, SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_0 + EventData.iTeam)
					PRINTLN("Setting SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_", EventData.iTeam)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_PHONE_EMP_EVENT(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_PHONE_EMP_EVENT EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_PHONE_EMP_EVENT
			PRINTLN("[PhoneEMP] Phone EMP Event Received! EventData.bActivateEMP: ", EventData.bActivateEMP, " | EventData.bDeactivateEMP: ", EventData.bDeactivateEMP)
			
			IF bIsLocalPlayerHost
			AND IS_BIT_SET(g_FMMC_Struct.iContinuityBitset, ciMISSION_CONTINUITY_BS_EMPUsedTracking)
				IF NOT HAS_PHONE_EMP_BEEN_USED()
					SET_BIT(MC_serverBD_1.sLEGACYMissionContinuityVars.iGenericTrackingBitset, ciLEGACY_ContinuityGenericTrackingBS_EMPUsed)
					PRINTLN("[PhoneEMP][CONTINUITY] - PROCESS_SCRIPT_EVENT_FMMC_PHONE_EMP_EVENT - EMP has been used")
				ENDIF
			ENDIF
			
			IF EventData.bActivateEMP
				ACTIVATE_PHONE_EMP()
			ENDIF
			IF EventData.bDeactivateEMP
				DEACTIVATE_PHONE_EMP()
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_BLOCK_WANTED_CONE_RESPONSE_EVENT(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_BLOCK_WANTED_CONE_RESPONSE_EVENT EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_BLOCK_WANTED_CONE_RESPONSE_EVENT
			PRINTLN("[BlockWantedConeResponse] Block Wanted Cone Response Event Received! EventData.bSetBlockWantedConeResponse: ", EventData.bSetBlockWantedConeResponse, "EventData.bClearBlockWantedConeResponse: ", EventData.bClearBlockWantedConeResponse, " EventData.iVeh: ", EventData.iVeh, " EventData.bSeenIllegal: ", EventData.bSeenIllegal)
			
			IF bIsLocalPlayerHost
				IF NOT IS_BIT_SET(MC_serverBD_1.iVehicleBlockedWantedConeResponseBS, EventData.iVeh)
					IF EventData.bSetBlockWantedConeResponse
						SET_BIT(MC_serverBD_1.iVehicleBlockedWantedConeResponseBS, EventData.iVeh)
						PRINTLN("[BlockWantedConeResponse]- PROCESS_SCRIPT_EVENT_FMMC_BLOCK_WANTED_CONE_RESPONSE_EVENT - Set iVehicleBlockedWantedConeResponseBS for vehicle: ", EventData.iVeh)
					ENDIF
				ELSE
					IF EventData.bClearBlockWantedConeResponse
						CLEAR_BIT(MC_serverBD_1.iVehicleBlockedWantedConeResponseBS, EventData.iVeh)
						PRINTLN("[BlockWantedConeResponse]- PROCESS_SCRIPT_EVENT_FMMC_BLOCK_WANTED_CONE_RESPONSE_EVENT - Cleared iVehicleBlockedWantedConeResponseBS for vehicle: ", EventData.iVeh)
					ENDIF
				ENDIF

				IF NOT IS_BIT_SET(MC_serverBD_1.iVehicleBlockedWantedConeResponseSeenIllegalBS, EventData.iVeh)
					IF EventData.bSeenIllegal
						SET_BIT(MC_serverBD_1.iVehicleBlockedWantedConeResponseSeenIllegalBS, EventData.iVeh)
						PRINTLN("[BlockWantedConeResponse]- PROCESS_SCRIPT_EVENT_FMMC_BLOCK_WANTED_CONE_RESPONSE_EVENT - Set iVehicleBlockedWantedConeResponseSeenIllegalBS for vehicle: ", EventData.iVeh)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_UPDATE_DOOR_OFF_RULE_EVENT(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_UPDATE_DOOR_OFF_RULE_EVENT EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_UPDATE_DOOR_OFF_RULE_EVENT
			PRINTLN("[UpdateDoorOffRule] Update Door Off Rule Event Received! EventData.bSetUpdateDoorOffRule: ", EventData.bSetUpdateDoorOffRule, " EventData.iDoor: ", EventData.iDoor)
			
			IF bIsLocalPlayerHost
				IF NOT IS_BIT_SET(MC_serverBD_1.iDoorUpdateOffRuleBS, EventData.iDoor)
					IF EventData.bSetUpdateDoorOffRule
						SET_BIT(MC_serverBD_1.iDoorUpdateOffRuleBS, EventData.iDoor)
						PRINTLN("[UpdateDoorOffRule]- PROCESS_SCRIPT_EVENT_FMMC_UPDATE_DOOR_OFF_RULE_EVENT - Set iDoorUpdateOffRuleBS for door: ", EventData.iDoor)
					ENDIF
				ELIF NOT EventData.bSetUpdateDoorOffRule 
					CLEAR_BIT(MC_serverBD_1.iDoorUpdateOffRuleBS, EventData.iDoor)
					PRINTLN("[UpdateDoorOffRule]- PROCESS_SCRIPT_EVENT_FMMC_UPDATE_DOOR_OFF_RULE_EVENT - Cleared iDoorUpdateOffRuleBS for door: ", EventData.iDoor)
				ENDIF
			ENDIF
			
			IF EventData.iMethodUsed = ciUPDATE_DOOR_OFF_RULE_METHOD__KEYCARD
				SET_BIT(iLocalBoolCheck32, LBOOL32_UNLOCKED_DOOR_WITH_SWIPECARD)
				PRINTLN("[UpdateDoorOffRule][InteractWith][InteractWith_Dialogue] PROCESS_SCRIPT_EVENT_FMMC_UPDATE_DOOR_OFF_RULE_EVENT - Setting LBOOL32_UNLOCKED_DOOR_WITH_SWIPECARD")
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_UPDATE_MINIGAME_OFF_RULE_EVENT(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_UPDATE_MINIGAME_OFF_RULE_EVENT EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_UPDATE_MINIGAME_OFF_RULE_EVENT
			PRINTLN("[UpdateMinigameOffRule] Update Minigame Off Rule Event Received! EventData.bSetHackCompleteOffRule: ", EventData.bSetHackCompleteOffRule, " EventData.iObj: ", EventData.iObj)
			
			IF bIsLocalPlayerHost
				IF NOT IS_BIT_SET(MC_serverBD_1.iOffRuleMinigameCompleted, Eventdata.iObj) 
					IF EventData.bSetHackCompleteOffRule
						SET_BIT(MC_serverBD_1.iOffRuleMinigameCompleted, Eventdata.iObj)
						PRINTLN("[UpdateMinigameOffRule]- PROCESS_SCRIPT_EVENT_FMMC_UPDATE_MINIGAME_OFF_RULE_EVENT - Set iOffRuleMinigameCompleted for object: ", EventData.iObj)
						
						SET_BIT(iLocalOffRuleMinigameCompletedBS, Eventdata.iObj)
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_OffRuleMinigameTracking)
						AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[Eventdata.iObj].iContinuityId > -1
						    SET_LONG_BIT(MC_serverBD_1.sLEGACYMissionContinuityVars.iObjectOffRuleTrackingBitset, g_FMMC_STRUCT_ENTITIES.sPlacedObject[Eventdata.iObj].iContinuityId)
						    PRINTLN("[CONTINUITY] - PROCESS_SCRIPT_EVENT_FMMC_UPDATE_MINIGAME_OFF_RULE_EVENT - offrule minigame on obj ", Eventdata.iObj, " with continuityID ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[Eventdata.iObj].iContinuityId, " was completed")
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF EventData.bSetHackCompleteOffRule
					SET_BIT(iLocalOffRuleMinigameCompletedBS, Eventdata.iObj)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_COP_DECOY_EVENT(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_COP_DECOY_EVENT EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_COP_DECOY_EVENT
			PRINTLN("[CopDecoy] Cop Decoy Event Received! EventData.iPed: ", EventData.iPed, " EventData.bStartedDecoy: ", EventData.bStartedDecoy, " EventData.bDecoyExpired: ", EventData.bDecoyExpired, " EventData.bDecoyShouldFlee: ", EventData.bDecoyShouldFlee)
			
			IF EventData.bStartedDecoy
				SET_DISPATCH_SPAWN_LOCATION(g_FMMC_STRUCT_ENTITIES.sPlacedPed[EventData.iPed].vPos)
			ELIF EventData.bDecoyExpired
			OR EventData.bDecoyShouldFlee
				RESET_DISPATCH_SPAWN_LOCATION()
				IF DOES_BLIP_EXIST(biCopDecoyBlip)
					REMOVE_BLIP(biCopDecoyBlip)
					PRINTLN("[CopDecoy] Removing Decoy Blip via PROCESS_SCRIPT_EVENT_FMMC_COP_DECOY_EVENT.")
				ENDIF
			ENDIF
			
			IF bIsLocalPlayerHost
				IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_COP_DECOY_STARTED)
					IF EventData.bStartedDecoy
					
						INT iCopDecoyStarDeductRandom = GET_RANDOM_INT_IN_RANGE(0, 101) //Get a random number to calculate the 2 star wanted level deduction chance against.
				
						IF iCopDecoyStarDeductRandom <= g_FMMC_STRUCT_ENTITIES.sPlacedPed[EventData.iPed].iPedCopDecoyLose2StarChance
							PRINTLN("[CopDecoy] PROCESS_SCRIPT_EVENT_FMMC_COP_DECOY_EVENT() Decoy will deduct 2 stars!")
							SET_BIT(MC_serverBD.iServerBitSet8, SBBOOL8_COP_DECOY_DEDUCT_2_STAR)
						ELSE
							PRINTLN("[CopDecoy] PROCESS_SCRIPT_EVENT_FMMC_COP_DECOY_EVENT() Decoy will deduct 1 star!")
						ENDIF
						PRINTLN("[CopDecoy] PROCESS_SCRIPT_EVENT_FMMC_COP_DECOY_EVENT() 2 Star chance was: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[EventData.iPed].iPedCopDecoyLose2StarChance, " Random number was: ", iCopDecoyStarDeductRandom)	
						
						REINIT_NET_TIMER(MC_serverBD_1.stCopDecoyActiveTimer)
						SET_BIT(MC_serverBD.iServerBitSet8, SBBOOL8_COP_DECOY_STARTED)
						
						PRINTLN("[CopDecoy]- PROCESS_SCRIPT_EVENT_FMMC_COP_DECOY_EVENT - Set SBBOOL8_COP_DECOY_STARTED and started stCopDecoyActiveTimer.") 
					ENDIF
				ENDIF
				
				IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_COP_DECOY_EXPIRED)
					IF EventData.bDecoyExpired
						SET_BIT(MC_serverBD.iServerBitSet8, SBBOOL8_COP_DECOY_EXPIRED)
						PRINTLN("[CopDecoy]- PROCESS_SCRIPT_EVENT_FMMC_COP_DECOY_EVENT - Set SBBOOL8_COP_DECOY_EXPIRED.") 
					ENDIF
				ENDIF
				
				IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_COP_DECOY_FLEEING)
					IF EventData.bDecoyShouldFlee
						SET_BIT(MC_serverBD.iServerBitSet8, SBBOOL8_COP_DECOY_FLEEING)
						PRINTLN("[CopDecoy]- PROCESS_SCRIPT_EVENT_FMMC_COP_DECOY_EVENT - Set SBBOOL8_COP_DECOY_FLEEING.") 
						
						MC_serverBD_2.iCurrentPedRespawnLives[EventData.iPed] = 0
						PRINTLN("[CopDecoy] ped ",EventData.iPed," getting cleaned up early - set MC_serverBD_2.iCurrentPedRespawnLives[", EventData.iPed, "] = 0. Call 1.")
			
						CLEAR_BIT(MC_serverBD.iPedCleanup_NeedOwnershipBS[GET_LONG_BITSET_INDEX(EventData.iPed)], GET_LONG_BITSET_BIT(EventData.iPed))
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_COP_BEHAVIOUR_OVERRIDE_EVENT(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_COP_BEHAVIOUR_OVERRIDE_EVENT EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_COP_BEHAVIOUR_OVERRIDE_EVENT
			PRINTLN("[CopOverride] Cop Behaviour Override Event Received! EventData.iPed: ", EventData.iPed, " EventData.bForceCopOverride: ", EventData.bForceCopOverride)
			
			IF bIsLocalPlayerHost
				IF EventData.bForceCopOverride
					SET_BIT(MC_serverBD.iCopPed[GET_LONG_BITSET_INDEX(EventData.iPed)], GET_LONG_BITSET_BIT(EventData.iPed))
					PRINTLN("[CopOverride] PROCESS_SCRIPT_EVENT_FMMC_COP_BEHAVIOUR_OVERRIDE_EVENT - MC_serverBD.iCopPed: ",EventData.iPed)
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_ACTIVATE_ELEVATOR(INT iEventID)
	
	IF NOT bIsLocalPlayerHost
		EXIT
	ENDIF
	
	SCRIPT_EVENT_DATA_FMMC_ACTIVATE_ELEVATOR EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_ACTIVATE_ELEVATOR
			PRINTLN("[JS][ELEVATORS] - PROCESS_SCRIPT_EVENT_FMMC_ACTIVATE_ELEVATOR - Event recieved to activate elevator: ", EventData.iElevator)
			
			IF EventData.iElevator < 0 
			OR EventData.iElevator >= FMMC_MAX_ELEVATORS
				EXIT
			ENDIF
			
			IF MC_serverBD_1.eElevatorState[EventData.iElevator] != FMMC_ELEVATOR_SERVER_STATE_READY
				EXIT
			ENDIF
			
			SET_SERVER_ELEVATOR_STATE(EventData.iElevator, FMMC_ELEVATOR_SERVER_STATE_ACTIVATED)
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_CALL_ELEVATOR(INT iEventID)
	
	IF NOT bIsLocalPlayerHost
		EXIT
	ENDIF
	
	SCRIPT_EVENT_DATA_FMMC_CALL_ELEVATOR EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_CALL_ELEVATOR
			PRINTLN("[JS][ELEVATORS] - PROCESS_SCRIPT_EVENT_FMMC_CALL_ELEVATOR - Event recieved to call elevator: ", EventData.iElevator)
			
			IF EventData.iElevator < 0 
			OR EventData.iElevator >= FMMC_MAX_ELEVATORS
				EXIT
			ENDIF
			
			IF MC_serverBD_1.eElevatorState[EventData.iElevator] != FMMC_ELEVATOR_SERVER_STATE_DISABLED
				EXIT
			ENDIF
			
			IF MC_serverBD_1.eElevatorState[g_FMMC_STRUCT.sElevators[EventData.iElevator].iLinkedElevator] != FMMC_ELEVATOR_SERVER_STATE_READY
				EXIT
			ENDIF
			
			
			SET_SERVER_ELEVATOR_STATE(EventData.iElevator, FMMC_ELEVATOR_SERVER_STATE_IDLE)
			SET_SERVER_ELEVATOR_STATE(g_FMMC_STRUCT.sElevators[EventData.iElevator].iLinkedElevator, FMMC_ELEVATOR_SERVER_STATE_DISABLED)
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_THERMITE_EFFECT_EVENT(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_THERMITE_EFFECT_EVENT EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_THERMITE_EFFECT_EVENT
			PRINTLN("[THERMITE] Thermite Effect Event Received! EventData.iObj: ", EventData.iObj, " EventData.bStartThermite: ", EventData.bStartThermite, " EventData.bStartSparkDieOff: ", EventData.bStartSparkDieOff, " EventData.bStartProcessing: ", EventData.bStartProcessing, 
				" EventData.bBreakKeypadObj: ", EventData.bBreakKeypadObj, " EventData.bClearProcessing: ", EventData.bClearProcessing, " EventData.bReduceChargeAmount: ", EventData.bReduceChargeAmount)
			
			INT iOrderedPart = EventData.iOrderedParticipant 
			
			IF EventData.bStartThermite
				oiLocalThermiteTarget[iOrderedPart] = NET_TO_OBJ(EventData.niObj)
				vThermiteLocalDripOffset[iOrderedPart] = EventData.vDripOffset
				vThermiteLocalSparkOffset[iOrderedPart] = EventData.vSparkOffset
				iThermiteLocalSparkTimer[iOrderedPart] = GET_GAME_TIMER()
				
				SET_BIT(iThermiteLocalBitset[iOrderedPart], THERMITE_LOCAL_BITSET_CREATE_LOCAL_THERMITE_PTFX)
				CDEBUG1LN(DEBUG_MISSION, "[THERMITE] PROCESS_SCRIPT_EVENT_FMMC_THERMITE_EFFECT_EVENT Setting THERMITE_LOCAL_BITSET_CREATE_LOCAL_THERMITE_PTFX for Ordered Participant: ", iOrderedPart)
			ENDIF
			
			IF EventData.bReduceChargeAmount
				
				INT iTeam = MC_playerBD[iPartToUse].iteam
				IF MC_serverBD_1.iTeamThermalCharges[iTeam] = 1
					CDEBUG1LN(DEBUG_MISSION, "[THERMITE] PROCESS_SCRIPT_EVENT_FMMC_THERMITE_EFFECT_EVENT Team ", iTeam, " is using their final remaining charge. Setting iThermiteKeypadWasLastTeamCharge for ", EventData.iObj)
					SET_BIT(iThermiteKeypadWasLastTeamCharge[iTeam], EventData.iObj)			
				ENDIF
			
				//If using limited team thermal charges then reduce the total amount here.
				IF bIsLocalPlayerHost
					IF MC_serverBD_1.iTeamThermalCharges[iTeam] > 0
						MC_serverBD_1.iTeamThermalCharges[iTeam] = MC_serverBD_1.iTeamThermalCharges[iTeam] - 1
						CDEBUG1LN(DEBUG_MISSION, "[THERMITE] PROCESS_SCRIPT_EVENT_FMMC_THERMITE_EFFECT_EVENT Team ", iTeam, " is using limited thermal charges and has just used one. ", MC_serverBD_1.iTeamThermalCharges[iTeam], " charges remain.")
					ENDIF
				ENDIF
			
			ENDIF
			
			IF EventData.bStartSparkDieOff
				iThermiteLocalSparkTimer[iOrderedPart] = GET_GAME_TIMER()
				SET_BIT(iThermiteLocalBitset[iOrderedPart], THERMITE_LOCAL_BITSET_START_LOCAL_SPARK_DIE_OFF_EVO)
				CDEBUG1LN(DEBUG_MISSION, "[THERMITE] PROCESS_SCRIPT_EVENT_FMMC_THERMITE_EFFECT_EVENT Setting THERMITE_LOCAL_BITSET_START_LOCAL_SPARK_DIE_OFF_EVO for Ordered Participant: ", iOrderedPart)
			ENDIF
			
			IF EventData.bStartProcessing
				OBJECT_INDEX tempObj = NET_TO_OBJ(EventData.niObj)
				
				//Stop the keypad from being able to be damaged externally once the thermite minigame starts.
				IF NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
					IF GET_ENTITY_CAN_BE_DAMAGED(tempObj)
						SET_ENTITY_CAN_BE_DAMAGED(tempObj, FALSE)
					ENDIF
				ENDIF
				
				//Block other players from processing any other thermite whilst another is ongoing.
				IF EventData.Details.FromPlayerIndex != LocalPlayer
					SET_BIT(iThermiteLocalBitset[iOrderedPart], THERMITE_LOCAL_BITSET_IS_ANY_THERMITE_PROCESSING)
					CDEBUG1LN(DEBUG_MISSION, "[THERMITE] PROCESS_SCRIPT_EVENT_FMMC_THERMITE_EFFECT_EVENT Setting THERMITE_LOCAL_BITSET_IS_ANY_THERMITE_PROCESSING for Ordered Participant: ", iOrderedPart)
				ENDIF
			ENDIF
			
			IF EventData.bBreakKeypadObj
				OBJECT_INDEX tempObj = NET_TO_OBJ(EventData.niObj)
				
				IF NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
					IF GET_IS_ENTITY_A_FRAG(tempObj)
						SET_DISABLE_FRAG_DAMAGE(tempObj, FALSE)
					ENDIF
					
					IF NOT IS_OBJECT_BROKEN_AND_VISIBLE(tempObj)
						DAMAGE_OBJECT_FRAGMENT_CHILD (tempObj, 0, -100)
						CDEBUG1LN(DEBUG_MISSION, "[THERMITE] PROCESS_SCRIPT_EVENT_FMMC_THERMITE_EFFECT_EVENT Damaging fragment for keypad minigame object.")
					ENDIF
				ENDIF
				
			ENDIF
			
			IF EventData.bClearProcessing
				//Force clear other players from blocking processing any other thermite whilst another is ongoing.
				IF EventData.Details.FromPlayerIndex != LocalPlayer
					CLEAR_BIT(iThermiteLocalBitset[iOrderedPart], THERMITE_LOCAL_BITSET_IS_ANY_THERMITE_PROCESSING)
					CDEBUG1LN(DEBUG_MISSION, "[THERMITE] PROCESS_SCRIPT_EVENT_FMMC_THERMITE_EFFECT_EVENT Clearing THERMITE_LOCAL_BITSET_IS_ANY_THERMITE_PROCESSING for Ordered Participant: ", iOrderedPart)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_BMBFB_EXPLODE_BALL(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_BMBFB_EXPLODE_BALL EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_BMBFB_EXPLODE_BALL
		
			IF GET_LAST_PART_TO_HIT_FOOTBALL_ON_TEAM(EventData.iBallToExplode, MC_playerBD[iLocalPart].iTeam) = iLocalPart
			AND MC_playerBD[iLocalPart].iTeam = EventData.iScoringTeam
				
				IF EventData.bIsGoal
					MC_playerBD_1[iLocalPart].iBmbFb_Goals++
					g_sArena_Telemetry_data.m_goalsScored = MC_playerBD_1[iLocalPart].iBmbFb_Goals
					PRINTLN("[BMBFB]", "[BMB", EventData.iBallToExplode, "] PROCESS_SCRIPT_EVENT_FMMC_BMBFB_EXPLODE_BALL - I was the last one to hit ball ", EventData.iBallToExplode, " before it went in the goal! Incrementing my goals to ", MC_playerBD_1[iLocalPart].iBmbFb_Goals)
					PRINTLN("[ARENA][TEL] - Goals scored ", g_sArena_Telemetry_data.m_goalsScored)
				ENDIF
				
				MC_Playerbd[iLocalPart].iPlayerScore++
				PRINTLN("[BMBFB]", "[BMB", EventData.iBallToExplode, "] PROCESS_SCRIPT_EVENT_FMMC_BMBFB_EXPLODE_BALL - I was the last one to hit ball ", EventData.iBallToExplode, " before it exploded! Incrementing my score to ", MC_Playerbd[iLocalPart].iPlayerScore)
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(GET_BOMB_FOOTBALL_BOMB(EventData.iBallToExplode))
				PRINTLN("[BMBFB]", "[BMB", EventData.iBallToExplode, "] PROCESS_SCRIPT_EVENT_FMMC_BMBFB_EXPLODE_BALL - Received explosion event for Football ", EventData.iBallToExplode, ", BUT that ball doesn't exist! Scoring team: ", EventData.iScoringTeam)
				EXIT
			ENDIF
			
			PRINTLN("[BMBFB]", "[BMB", EventData.iBallToExplode, "] PROCESS_SCRIPT_EVENT_FMMC_BMBFB_EXPLODE_BALL - Received explosion event for Football ", EventData.iBallToExplode, "! Scoring team: ", EventData.iScoringTeam)
			
			IF NETWORK_HAS_CONTROL_OF_ENTITY(GET_BOMB_FOOTBALL_BOMB(EventData.iBallToExplode))
				PRINTLN("[BMBFB][BMBFB MAJOR]", "[BMB", EventData.iBallToExplode, "] PROCESS_SCRIPT_EVENT_FMMC_BMBFB_EXPLODE_BALL - I have control of this bomb! About to explode it...")
				EXPLODE_BOMB_FOOTBALL_BOMB(EventData.iBallToExplode)
			ELSE
				PRINTLN("[BMBFB]", "[BMB", EventData.iBallToExplode, "] PROCESS_SCRIPT_EVENT_FMMC_BMBFB_EXPLODE_BALL - I don't have control of this bomb")
			ENDIF
			
			IF bIsLocalPlayerHost
				IF NOT IS_BIT_SET(MC_serverBD_3.iBombFB_ExplodedBS, EventData.iBallToExplode)
					SET_BIT(MC_serverBD_3.iBombFB_ExplodedBS, EventData.iBallToExplode)
					PRINTLN("[BMBFB][BMBFB MAJOR][BMBFB HOST]", "[BMB", EventData.iBallToExplode, "] PROCESS_SCRIPT_EVENT_FMMC_BMBFB_EXPLODE_BALL - iBombFB_ExplodedBS || Setting ", EventData.iBallToExplode)
					
					IF EventData.iScoringTeam > -1
						IF NOT IS_BIT_SET(MC_serverBD_3.iBombFB_GoalProcessedBS, EventData.iBallToExplode)
							MC_serverBD.iTeamScore[EventData.iScoringTeam] += 1
							MC_serverBD_3.iBombFB_ExplosionPointsThisMiniRound[EventData.iScoringTeam]++
							PRINTLN("[BMBFB][BMBFB MAJOR][BMBFB HOST] PROCESS_SCRIPT_EVENT_FMMC_BMBFB_EXPLODE_BALL - Incrementing iTeamScore for team ", EventData.iScoringTeam, " to ", MC_serverBD.iTeamScore[EventData.iScoringTeam])
						ELSE
							PRINTLN("[BMBFB][BMBFB MAJOR][BMBFB HOST] PROCESS_SCRIPT_EVENT_FMMC_BMBFB_EXPLODE_BALL - Not giving points for explosion because this is a goal situation!")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_WAITING_FOR_OBJ_TIMER_AND_FINAL_FOOTBALL_POINTS)
				SET_BIT(iLocalBoolCheck29, LBOOL29_GIVEN_FINAL_FOOTBALL_POINTS)
				PRINTLN("[BMBFB] PROCESS_HOLDING_PACKAGE_SCORING - Setting LBOOL29_GIVEN_FINAL_FOOTBALL_POINTS")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_BMBFB_GOAL(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_BMBFB_GOAL EventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_BMBFB_GOAL
			
			IF IS_BIT_SET(MC_serverBD_3.iBombFB_GoalProcessedBS, EventData.iBallInGoal)
				PRINTLN("[BMBFB]", "[BMB", EventData.iBallInGoal, "] PROCESS_SCRIPT_EVENT_FMMC_BMBFB_GOAL - Football ", EventData.iBallInGoal, " has already had its goal event processed!")
				EXIT
			ENDIF
			
			IF bIsLocalPlayerHost
			
				PRINTLN("[BMBFB]", "[BMB", EventData.iBallInGoal, "] PROCESS_SCRIPT_EVENT_FMMC_BMBFB_GOAL - Football ", EventData.iBallInGoal, " is in the goal and I'm host! Scoring team: ", EventData.iScoringTeam, " and sending out ball explosion event!")
				
				SET_BIT(MC_serverBD_3.iBombFB_GoalProcessedBS, EventData.iBallInGoal)
				
				BROADCAST_FMMC_BMBFB_EXPLODE_BALL(EventData.iBallInGoal, EventData.iScoringTeam, TRUE)
				
				IF EventData.iScoringTeam > -1
				AND EventData.iScoringTeam < FMMC_MAX_TEAMS
					iTeamGoalsThisMiniRound[EventData.iScoringTeam]++
					PRINTLN("[BMBFB]", "[BMB", EventData.iBallInGoal, "] PROCESS_SCRIPT_EVENT_FMMC_BMBFB_GOAL - Setting iTeamGoalsThisMiniRound to ", iTeamGoalsThisMiniRound[EventData.iScoringTeam], " for team ", EventData.iScoringTeam)
					PLAY_ARENA_CROWD_CHEER_AUDIO()
					PRINTLN("[CROWD CHEER] Bomb Ball - Scored a goal")
				ENDIF
				
				INT iTeamToAward = g_FMMC_STRUCT_ENTITIES.sPlacedZones[EventData.iGoalIndex].iGoalTeam
				INT iPointsToGive = g_FMMC_STRUCT_ENTITIES.sPlacedZones[EventData.iGoalIndex].iGoalPoints
				
				INCREMENT_SERVER_TEAM_SCORE(iTeamToAward, MC_serverBD_4.iCurrentHighestPriority[iTeamToAward], iPointsToGive)
				MC_serverBD_3.iBombFB_ExplosionPointsThisMiniRound[iTeamToAward] += iPointsToGive
				
				PLAY_SOUND_FROM_COORD(-1, "Score_Goal", GET_CENTER_OF_AREA(g_FMMC_STRUCT_ENTITIES.sPlacedZones[EventData.iGoalIndex].vPos[0], g_FMMC_STRUCT_ENTITIES.sPlacedZones[EventData.iGoalIndex].vPos[1]), "DLC_AW_BB_Sounds", TRUE, 450)
				
				PRINTLN("[TMS][BMBFB][BMBFB MAJOR] PROCESS_SCRIPT_EVENT_FMMC_BMBFB_GOAL - GOAL! Giving ", iPointsToGive, " points to team ", iTeamToAward, " || They are now at ", MC_serverBD.iTeamScore[iTeamToAward], " points")
			
				//Announcer
				IF iTeamGoalsThisMiniRound[EventData.iScoringTeam] >= 10
					BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_ModeSpecific, g_iAA_PlaySound_Detonation_10Goals)
					PRINTLN("[BMBFBAnnouncer] g_iAA_PlaySound_Detonation_10Goals")
				ELIF iTeamGoalsThisMiniRound[EventData.iScoringTeam] >= 3
					IF EventData.iScoringTeam = 0
						BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_ModeSpecific, g_iAA_PlaySound_Detonation_Team1Hatrick)
						PRINTLN("[BMBFBAnnouncer] g_iAA_PlaySound_Detonation_Team1Hatrick")
					ELSE
						BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_ModeSpecific, g_iAA_PlaySound_Detonation_Team2Hatrick)
						PRINTLN("[BMBFBAnnouncer] g_iAA_PlaySound_Detonation_Team2Hatrick")
					ENDIF
				ELIF GET_FRAME_COUNT() % 4 = 0 // Effectively 25% of the time it'll do this one
					BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_ModeSpecific, g_iAA_PlaySound_Detonation_Goal)
					PRINTLN("[BMBFBAnnouncer] g_iAA_PlaySound_Detonation_Goal")
				ELSE
					IF EventData.iScoringTeam = 0
						BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_ModeSpecific, g_iAA_PlaySound_Detonation_Team1Goal)
						PRINTLN("[BMBFBAnnouncer] g_iAA_PlaySound_Detonation_Team1Goal")
					ELSE
						BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_ModeSpecific, g_iAA_PlaySound_Detonation_Team2Goal)
						PRINTLN("[BMBFBAnnouncer] g_iAA_PlaySound_Detonation_Team2Goal")
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF		
ENDPROC

//END OF ARENA TRAPS//

PROC PROCESS_SCRIPT_EVENT_FMMC_HARD_TARGET_NEW_TARGET_SHARD(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_HARD_TARGET_NEW_TARGET_SHARD EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_HARD_TARGET_NEW_TARGET_SHARD
			TEXT_LABEL_15 tl15 = ""
			TEXT_LABEL_15 tl15_2 = ""
			
			SET_BIT(iLocalBoolCheck26, LBOOL26_FIRST_HARD_TARGET_SHARD_PLAYED)
			
			IF EventData.iOnlyThisPart = -1
				PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_HARD_TARGET_NEW_TARGET_SHARD] MyPart: ", iLocalPart,
																					" Part0Num: ", EventData.iNewTargets[0], 
																					" Part1Num: ", EventData.iNewTargets[1], 
																					" Part2Num: ", EventData.iNewTargets[2],
																					" Part3Num: ", EventData.iNewTargets[3], 
																					" iTeamsActive: ", EventData.iTeamsActive,
																					" iOnlyThisPart: ", EventData.iOnlyThisPart)
				
				tl15 = "HARD_NEW_TRG2"
				
				IF EventData.iTeamsActive > 0
					IF EventData.iNewTargets[0] > -1
					AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(EventData.iNewTargets[0]))
						IF EventData.iNewTargets[0] = iLocalPart
							PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_HARD_TARGET_NEW_TARGET_SHARD] We are the new target (1)")
							tl15 = "HARD_NEW_TRG1"
						ENDIF
					ENDIF
				ENDIF
				
				IF EventData.iTeamsActive > 1
					IF EventData.iNewTargets[1] > -1
					AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(EventData.iNewTargets[1]))
						IF EventData.iNewTargets[1] = iLocalPart
							PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_HARD_TARGET_NEW_TARGET_SHARD] We are the new target (2)")
							tl15 = "HARD_NEW_TRG1"
						ENDIF
					ENDIF
				ENDIF
				
				IF EventData.iTeamsActive > 2
					IF EventData.iNewTargets[2] > -1
					AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(EventData.iNewTargets[2]))
						IF EventData.iNewTargets[2] = iLocalPart
							PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_HARD_TARGET_NEW_TARGET_SHARD] We are the new target (3)")
							tl15 = "HARD_NEW_TRG1"
						ENDIF
					ENDIF
				ENDIF
				
				IF EventData.iTeamsActive > 3
					IF EventData.iNewTargets[3] > -1
					AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(EventData.iNewTargets[3]))
						IF EventData.iNewTargets[3] = iLocalPart
							PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_HARD_TARGET_NEW_TARGET_SHARD] We are the new target (4)")
							tl15 = "HARD_NEW_TRG1"
						ENDIF
					ENDIF
				ENDIF
				
				CLEAR_BIT(MC_PlayerBD[iLocalPart].iClientBitSet3, PBBOOL3_PLAYER_IS_A_HARD_TARGET)
			
				DO_FORCED_UPDATE_OF_ALL_PLAYER_BLIPS_THIS_FRAME()
				
				// If we're a hard target then clear the bit so we can play sound.
				IF IS_BIT_SET(iLocalBoolCHeck26, LBOOL26_HARD_TARGET_CHANGED_SOUND)
					IF EventData.iNewTargets[0] = iPartToUse
					OR EventData.iNewTargets[1] = iPartToUse
					OR EventData.iNewTargets[2] = iPartToUse
					OR EventData.iNewTargets[3] = iPartToUse
						CLEAR_BIT(iLocalBoolCHeck26, LBOOL26_HARD_TARGET_CHANGED_SOUND)
						PRINTLN("[PROCESS_HARD_TARGET_MODE][LM] - Local Player is no longer a Hard target. Clearing bit (audio).")
					ENDIF
				ENDIF				
				
				IF EventData.iTeamsActive = 2
					tl15_2 = "HARD_N_TRG_2"
				ELIF EventData.iTeamsActive = 3
					tl15_2 = "HARD_N_TRG_3"
				ELIF EventData.iTeamsActive = 4
					tl15_2 = "HARD_N_TRG_4"
				ENDIF
					
				PLAYER_INDEX player1 = INVALID_PLAYER_INDEX()
				IF EventData.iNewTargets[0] > -1
				AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(EventData.iNewTargets[0]))
					player1 = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(EventData.iNewTargets[0]))
				ENDIF
				
				PLAYER_INDEX player2 = INVALID_PLAYER_INDEX()
				IF EventData.iNewTargets[1] > -1
				AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(EventData.iNewTargets[1]))
					player2 = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(EventData.iNewTargets[1]))
				ENDIF
				
				PLAYER_INDEX player3 = INVALID_PLAYER_INDEX()
				IF EventData.iNewTargets[2] > -1
				AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(EventData.iNewTargets[2]))
					player3 = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(EventData.iNewTargets[2]))
				ENDIF
				
				PLAYER_INDEX player4 = INVALID_PLAYER_INDEX()
				IF EventData.iNewTargets[3] > -1
				AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(EventData.iNewTargets[3]))
					player4 = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(EventData.iNewTargets[3]))
				ENDIF
				
				// Prints for bug: url:bugstar:4050718
				IF EventData.iNewTargets[0] > -1
				AND EventData.iNewTargets[1] > -1
					PRINTLN("[PROCESS_HARD_TARGET_MODE][LM][url:bugstar:4050718] - iMyTeam: ", MC_PlayerBD[iPartToUse].iteam, " (n)iMyTeam: ", GET_PLAYER_TEAM(PLAYER_ID()), " Player1Team: ", MC_PlayerBD[EventData.iNewTargets[0]].iTeam, " (n)Player1Team: ", GET_PLAYER_TEAM(player1), " Player2Team: ", MC_PlayerBD[EventData.iNewTargets[1]].iTeam, " (n)Player2Team: ", GET_PLAYER_TEAM(player2))
				ENDIF
				
				// Rearrange. ((url:bugstar:4163930))
				INT iEnemyTeam = PICK_INT(MC_playerBD[iPartToUse].iteam = 0, 1, 0)
				INT iFriendlyTeam = PICK_INT(MC_playerBD[iPartToUse].iteam = 0, 0, 1)				
				PLAYER_INDEX PlayerFriendly
				PLAYER_INDEX PlayerEnemy				
				IF iFriendlyTeam = 0
					PlayerFriendly = Player1
				ELSE
					PlayerFriendly = Player2
				ENDIF				
				IF iEnemyTeam = 0
					PlayerEnemy = Player1
				ELSE
					PlayerEnemy = Player2
				ENDIF								
				
				BOOL bPlayShard = TRUE
				IF IS_BIT_SET(iLocalBoolCheck27, LBOOL27_HARD_TARGET_LAST_TIME)
				AND (MC_ServerBD.iNumberOfPlayingPlayers[0] + MC_ServerBD.iNumberOfPlayingPlayers[1]) <= 2
					bPlayShard = FALSE
				ENDIF
				
				IF bPlayShard
					SETUP_NEW_BIG_MESSAGE_WITH_4_PLAYERS_AND_4_COLOURS(BIG_MESSAGE_GENERIC_TEXT, PlayerEnemy, PlayerFriendly, player3, player4, DEFAULT, tl15_2, tl15, DEFAULT, 5000, DEFAULT, GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_PlayerBD[iPartToUse].iteam, PlayerEnemy), GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_PlayerBD[iPartToUse].iteam, PlayerFriendly), GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_PlayerBD[iPartToUse].iteam, player3), GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_PlayerBD[iPartToUse].iteam, player4))
				ENDIF
				
				IF EventData.iNewTargets[0] = iLocalPart
				OR EventData.iNewTargets[1] = iLocalPart
				OR EventData.iNewTargets[2] = iLocalPart
				OR EventData.iNewTargets[3] = iLocalPart
					SET_BIT(iLocalBoolCheck27, LBOOL27_HARD_TARGET_LAST_TIME)
				ELSE
					CLEAR_BIT(iLocalBoolCheck27, LBOOL27_HARD_TARGET_LAST_TIME)	
				ENDIF
			ELSE
				IF iPartToUse = EventData.iOnlyThisPart
					tl15 = "HARD_NEW_TRG1"
										
					IF IS_BIT_SET(iLocalBoolCHeck26, LBOOL26_HARD_TARGET_CHANGED_SOUND)
						CLEAR_BIT(iLocalBoolCHeck26, LBOOL26_HARD_TARGET_CHANGED_SOUND)
						PRINTLN("[PROCESS_HARD_TARGET_MODE][LM] - Local Player is no longer a Hard target. Clearing bit (audio)(iOnlyThisPart).")
					ENDIF
					
					DO_FORCED_UPDATE_OF_ALL_PLAYER_BLIPS_THIS_FRAME()
					CLEAR_ALL_BIG_MESSAGES()
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT,	tl15, "", DEFAULT, 5000)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_TARGET_PROP_SHOT(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_TARGET_PROP_SHOT EventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_TARGET_PROP_SHOT
			IF DOES_ENTITY_EXIST(oiProps[EventData.iEventProp])
				PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_TARGET_PROP_SHOT] - INFO: _____________________________")
				PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_TARGET_PROP_SHOT] - EventData.iProp: ", EventData.iEventProp, "	PlayerPart: ", EventData.iEventPart, "	iTeam: ", EventData.iEventTeam)
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[EventData.iEventProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_spec_target_s_01a"))
				OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[EventData.iEventProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_spec_target_m_01a"))
				OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[EventData.iEventProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_spec_target_b_01a"))
					//[Target Practice][Client] Target has been shot (immediately change the local state if valid as this message arrives before server broadcast data)
					IF NOT HAS_NET_TIMER_STARTED(stTargetPropShot[EventData.iEventProp])
						IF MC_ServerBD_4.ePropClaimState[EventData.iEventProp] != ePropClaimingState_Contested
							START_NET_TIMER(stTargetPropShot[EventData.iEventProp])
							
							TARGET_PRACTICE_TARGET_PROP_SHOT(EventData.iEventProp)
						ENDIF
					ENDIF
					
					//[Target Practice][Server] If valid give time bonus and update the server broadcast data with the target state
					IF bIsLocalPlayerHost
						IF MC_ServerBD_4.ePropClaimState[EventData.iEventProp] != ePropClaimingState_Contested
							MC_ServerBD_4.ePropClaimState[EventData.iEventProp] = ePropClaimingState_Contested
							
							PRINTLN("[MultiRuleTimer] PROCESS_SCRIPT_EVENT_FMMC_TARGET_PROP_SHOT - Offset multi rule timer for team ",EventData.iEventTeam," by ",g_FMMC_STRUCT.iTargetPracticeTimeBonus)
							MC_serverBD_3.tdMultiObjectiveLimitTimer[EventData.iEventTeam].Timer = GET_TIME_OFFSET(MC_serverBD_3.tdMultiObjectiveLimitTimer[EventData.iEventTeam].Timer, g_FMMC_STRUCT.iTargetPracticeTimeBonus)
						ENDIF
					ENDIF
				ENDIF
			ENDIF		
		ENDIF	
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_COLLECTED_WEAPON(INT iEventID)

	SCRIPT_EVENT_DATA_FMMC_COLLECTED_WEAPON EventData
	BOOL bPlayFrontEnd
	PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_COLLECTED_WEAPON] - 1")
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_COLLECTED_WEAPON
			
			STRING sSoundName = ""
			STRING sSoundSetName = ""
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_LAND_GRAB_SOUNDS)
				sSoundName = "Weapon_Collect"
				IF EventData.Details.FromPlayerIndex = PLAYER_ID()					
					sSoundSetName = "DLC_SR_LG_Player_Sounds"			
				ELIF GET_PLAYER_TEAM(EventData.Details.FromPlayerIndex) = GET_PLAYER_TEAM(PLAYER_ID())
					sSoundSetName = "DLC_SR_LG_Team_Sounds"
				ELSE
					sSoundSetName = "DLC_SR_LG_Enemy_Sounds"
				ENDIF	
			ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_RESURRECTION_SOUNDS)
				sSoundName = "Weapon_Collect"
				IF EventData.Details.FromPlayerIndex = PLAYER_ID()					
					sSoundSetName = "DLC_SR_RS_Player_Sounds"			
				ELIF GET_PLAYER_TEAM(EventData.Details.FromPlayerIndex) = GET_PLAYER_TEAM(PLAYER_ID())
					sSoundSetName = "DLC_SR_RS_Team_Sounds"
				ELSE
					sSoundSetName = "DLC_SR_RS_Enemy_Sounds"
				ENDIF	
			ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_CONDEMNED_SOUNDS)
				sSoundName = "Weapon_Collect"				
				IF EventData.Details.FromPlayerIndex = PLAYER_ID()				
					sSoundSetName = "DLC_SM_CND_Player_Sounds"				
				ELIF GET_PLAYER_TEAM(EventData.Details.FromPlayerIndex) = GET_PLAYER_TEAM(PLAYER_ID())
					sSoundSetName = "DLC_SM_CND_Team_Sounds"	
				ELSE
					sSoundSetName = "DLC_SM_CND_Enemy_Sounds"	
				ENDIF	
			ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_VEHICULAR_WARFARE_SOUNDS)
				sSoundName = "Weapon_Collect"
				IF EventData.Details.FromPlayerIndex = PLAYER_ID()
					sSoundName = "Weapon_Collect"
					sSoundSetName = "DLC_SM_VEHWA_Player_Sounds"
				ELIF GET_PLAYER_TEAM(EventData.Details.FromPlayerIndex) = GET_PLAYER_TEAM(PLAYER_ID())					
					sSoundSetName = "DLC_SM_VEHWA_Team_Sounds"
				ELSE
					sSoundName = "Weapon_Collect"
					sSoundSetName = "DLC_SM_VEHWA_Enemy_Sounds"
				ENDIF
			ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_HOSTILE_TAKEOVER_SOUNDS)
				sSoundName = "Weapon_Collect"				
				IF EventData.Details.FromPlayerIndex = PLAYER_ID()				
					sSoundSetName = "dlc_xm_hota_Sounds"
				ELSE
					EXIT
				ENDIF
				bPlayFrontEnd = TRUE
			ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_HARD_TARGET_AUDIO)
				IF EventData.Details.FromPlayerIndex = PLAYER_ID()
					sSoundName = "Weapon_Collect"
					sSoundSetName = "dlc_xm_hata_Sounds"										
				ELSE
					EXIT
				ENDIF
				bPlayFrontEnd = TRUE
			ENDIF
			
			PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_COLLECTED_WEAPON] - sSoundSetName: ", sSoundSetName)
			PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_COLLECTED_WEAPON] - sSoundName: ", sSoundName)
			PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_COLLECTED_WEAPON] - vpos.x: ", EventData.vPos.x, " vpos.y: ", EventData.vPos.y, " vpos.z: ", EventData.vPos.z)
			
			IF NOT IS_STRING_NULL_OR_EMPTY(sSoundSetName)
				IF NOT bPlayFrontEnd
					PLAY_SOUND_FROM_COORD(-1, sSoundName, EventData.vPos, sSoundSetName, FALSE)
				ELSE
					PLAY_SOUND_FRONTEND(-1, sSoundName, sSoundSetName)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_RESPAWNED_WEAPON(INT iEventID)

	SCRIPT_EVENT_DATA_FMMC_RESPAWNED_WEAPON EventData
	BOOL bPlayFrontEnd
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_RESPAWNED_WEAPON

			STRING sSoundName = ""
			STRING sSoundSetName = ""
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_LAND_GRAB_SOUNDS)
				sSoundName = "Weapon_Respawn"				
				sSoundSetName = "DLC_SR_LG_Player_Sounds"
				
			ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_RESURRECTION_SOUNDS)
				sSoundName = "Weapon_Respawn"
				sSoundSetName = "DLC_SR_RS_Player_Sounds"
				
			ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_CONDEMNED_SOUNDS)
				sSoundName = "Weapon_Respawn"
				sSoundSetName = "DLC_SM_CND_Player_Sounds"
				
			ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_VEHICULAR_WARFARE_SOUNDS)
				sSoundName = "Weapon_Respawn"
				sSoundSetName = "DLC_SM_VEHWA_Player_Sounds"
				
			ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_HOSTILE_TAKEOVER_SOUNDS)
				sSoundName = "Weapon_Respawn"
				sSoundSetName = "dlc_xm_hota_Sounds"
				
			ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_HARD_TARGET_AUDIO)
				sSoundName = "Weapon_Respawn"
				sSoundSetName = "dlc_xm_hata_Sounds"
				
			ENDIF
			
			PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_RESPAWNED_WEAPON] - sSoundSetName: ", sSoundSetName)
			PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_RESPAWNED_WEAPON] - sSoundName: ", sSoundName)
			
			IF NOT IS_STRING_NULL_OR_EMPTY(sSoundSetName)
				IF NOT bPlayFrontEnd
					PLAY_SOUND_FROM_COORD(-1, sSoundName, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[EventData.iEventWeapon].vPos, sSoundSetName, FALSE)
				ELSE
					PLAY_SOUND_FRONTEND(-1, sSoundName, sSoundSetName)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_CANT_DECREMENT_SCORE(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_CANT_DECREMENT_SCORE EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_CANT_DECREMENT_SCORE
			PRINTLN("[JS] PROCESS_SCRIPT_EVENT_FMMC_CANT_DECREMENT_SCORE - Killer = ", EventData.iKillingPart)
			IF EventData.iKillingPart = iPartToUse
				PRINT_HELP("TLAD_HELP1")
			ENDIF
		ENDIF
	ENDIF
ENDPROC
/// PURPOSE:
///   Creates a unique ID for hitting a checkpoint to store and check against
///    
FUNC INT SHOULD_INCREASE_CHECKPOINT_SCORE(INT iTeam, INT iLap, INT iCurrentLoc)
	iTeam = iTeam + 1
	iLap = iLap + 1
	
	INT iCheckpointID = ((iTeam * 1000) + (iLap*100) + iCurrentLoc)
	PRINTLN("[JT][CHECKPOINT] SHOULD_INCREASE_CHECKPOINT_SCORE - ID = ", iCheckpointID)
	RETURN iCheckpointID
	
ENDFUNC

PROC PROCESS_SCRIPT_EVENT_FMMC_KILL_PLAYER_NOT_AT_LOCATE(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_KILL_PLAYER_NOT_AT_LOCATE EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_KILL_PLAYER_NOT_AT_LOCATE
			IF EventData.iEventPart > -1
				IF EventData.iEventPart = iLocalPart
					IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
						IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
							PRINTLN("[JT BPKP] BLEEP_TEST_KILL_PLAYERS_NOT_AT_LOCATE - in a vehicle")
							VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
							IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
								IF IS_ENTITY_ALIVE(tempVeh) 
									NETWORK_EXPLODE_VEHICLE(tempVeh, TRUE,TRUE)
								ENDIF
							ELSE
								NETWORK_REQUEST_CONTROL_OF_ENTITY(tempVeh)
							ENDIF
						ELSE
							SET_ENTITY_HEALTH(LocalPlayerPed, 0)	//here for bug 3318679
							PRINTLN("[JT BPKP] Killing participant ", iLocalPart)	
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_TEAM_CHECKPOINT_PASSED(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_TEAM_CHECKPOINT_PASSED EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_TEAM_CHECKPOINT_PASSED
			PRINTLN("[JS] PROCESS_SCRIPT_EVENT_FMMC_TEAM_CHECKPOINT_PASSED - iTeam = ", EventData.iTeam)
			PRINTLN("[JS] PROCESS_SCRIPT_EVENT_FMMC_TEAM_CHECKPOINT_PASSED - iPart = ", EventData.iPart)
			PRINTLN("[JS] PROCESS_SCRIPT_EVENT_FMMC_TEAM_CHECKPOINT_PASSED - iCurrentLoc = ", EventData.iCurrentLoc)
			PRINTLN("[JS] PROCESS_SCRIPT_EVENT_FMMC_TEAM_CHECKPOINT_PASSED - iCurrentLap = ", EventData.iCurrentLap)
			//Increment how many locates have been passed
			INT iCheckpointID = SHOULD_INCREASE_CHECKPOINT_SCORE(EventData.iTeam, EventData.iCurrentLap, EventData.iCurrentLoc)
			IF iCheckpointID != iTeamUniqueLapID[EventData.iTeam]
				PRINTLN("[JT][CHECKPOINT] PROCESS_SCRIPT_EVENT_FMMC_TEAM_CHECKPOINT_PASSED - this ID = ", iCheckpointID, " last ID = ", iTeamUniqueLapID[EventData.iTeam])
				iTeamUniqueLapID[EventData.iTeam] = iCheckpointID
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciENABLE_SPLIT_TIMES)
				AND IS_BIT_SET(iLocalBoolCheck19, LBOOL19_RACE_SPLIT_TIMES_STARTED)
					INT iTime = GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MC_serverBD_3.tdObjectiveLimitTimer[EventData.iTeam].Timer)
					INT iLap = MC_ServerBD_4.iTeamLapsCompleted[EventData.iTeam]
					INT iCheckpoints = iCurrentSplitCheckpoint[EventData.iTeam] - (MC_serverBD.iNumLocCreated * iLap)
					
					IF iCheckpoints >= MC_serverBD.iNumLocCreated
						PRINTLN("[JS] [SPLIT] - SCRIPT_RACE_PLAYER_HIT_CHECKPOINT - iTeamLapsCompleted hasn't updated yet, faking it")
						iLap++
						iCheckpoints = iCurrentSplitCheckpoint[EventData.iTeam] - (MC_serverBD.iNumLocCreated * iLap)
					ELIF iCheckpoints = -1
					AND MC_ServerBD_4.iTeamLapsCompleted[EventData.iTeam] > 0
						PRINTLN("[JS] [SPLIT] - SCRIPT_RACE_PLAYER_HIT_CHECKPOINT - iTeamLapsCompleted updated before event was processed, faking it")
						iLap--
						iCheckpoints = iCurrentSplitCheckpoint[EventData.iTeam] - (MC_serverBD.iNumLocCreated * iLap)
					ENDIF
					PRINTLN("[JS] [SPLIT] - SCRIPT_RACE_PLAYER_HIT_CHECKPOINT - Hit: ", iCheckpoints, " Lap: ", iLap, " Time: ", iTime)
					INT iPartLoop
					INT iTeamPlayersChecked
					PARTICIPANT_INDEX tempPart
					FOR iPartLoop = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
						IF MC_playerBD[iPartLoop].iteam = EventData.iTeam
							tempPart = INT_TO_PARTICIPANTINDEX(iPartLoop)
							IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
								IF (NOT IS_BIT_SET(MC_playerBD[iPartLoop].iClientBitSet,PBBOOL_ANY_SPECTATOR)
								OR IS_BIT_SET(MC_playerBD[iPartLoop].iClientBitSet,PBBOOL_HEIST_SPECTATOR))
									SCRIPT_RACE_PLAYER_HIT_CHECKPOINT(NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX(tempPart)), iCheckpoints, iLap, iTime)	
									iTeamPlayersChecked++
								ENDIF
							ENDIF
						ENDIF
						IF iTeamPlayersChecked >= MC_serverBD.iNumberOfPlayingPlayers[EventData.iteam]
							BREAKLOOP
						ENDIF
					ENDFOR
					iCurrentSplitCheckpoint[EventData.iTeam]++
				ENDIF
						
				IF EventData.iTeam = MC_playerBD[iPartToUse].iTeam
					
					MC_playerBD[iLocalPart].iCheckpointsPassed++
					PRINTLN("[JT][CHECKPOINT] PROCESS_SCRIPT_EVENT_FMMC_TEAM_CHECKPOINT_PASSED - Just passed a checkpoint ", EventData.iCurrentLoc, " New score: ", MC_playerBD[iPartToUse].iCheckpointsPassed)
					
					//Checks if we should record the lap time
					IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[EventData.iCurrentLoc].iJumpToObjectivePass[EventData.iTeam] != ciJumpToObjective_None
					AND g_FMMC_STRUCT_ENTITIES.sGotoLocationData[EventData.iCurrentLoc].iJumpToObjectivePass[EventData.iTeam] = ciJumpToObjective_MissionStart
						INT iCurrentLapTime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_4.tdLapTimer[EventData.iTeam]) - iLastLapTime
						IF (MC_playerBD[iPartToUse].iBestLapTime = 0
						OR iCurrentLapTime < iLastLapTime)
						AND iCurrentLapTime > 10000
							PRINTLN("[KH][LAPS] New fastest lap time: ",iCurrentLapTime)
							MC_playerBD[iPartToUse].iBestLapTime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_4.tdLapTimer[EventData.iTeam]) - iLastLapTime
							
							PRINTLN("[JT LAP] Best lap time: ", MC_playerBD[iPartToUse].iBestLapTime, " Fastest lap: ", MC_playerBD[iPartToUse].iFastestLapOfMission)
						
							IF MC_playerBD[iPartToUse].iFastestLapOfMission = 0
							OR MC_playerBD[iPartToUse].iBestLapTime < MC_playerBD[iPartToUse].iFastestLapOfMission
								MC_playerBD[iPartToUse].iFastestLapOfMission = MC_playerBD[iPartToUse].iBestLapTime
								PRINTLN("[JT LAP] iFastestLapOfMission ", MC_playerBD[iPartToUse].iFastestLapOfMission, " iPart", iPartToUse )
							ELSE
								PRINTLN("[JT LAP] 1 ", MC_playerBD[iPartToUse].iFastestLapOfMission = 0, " iFastestLap", MC_playerBD[iPartToUse].iFastestLapOfMission)
								PRINTLN("[JT LAP] 2 ", MC_playerBD[iPartToUse].iBestLapTime < MC_playerBD[iPartToUse].iFastestLapOfMission) 
							ENDIF
						ELSE
							IF iCurrentLapTime < 10000
								PRINTLN("[KH][LAPS] Current lap time is too low, ignoring ",iCurrentLapTime)
							ENDIF
						ENDIF
						iLastLapTime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_4.tdLapTimer[EventData.iTeam]) - iLastLapTime
						PRINTLN("[KH][LAPS] iBestLapTime: ", MC_playerBD[iPartToUse].iBestLapTime, " - iLastLapTime: ", iLastLapTime)
					ENDIF	
					
				ENDIF
				
			ELSE
				PRINTLN("[JT][CHECKPOINT] New ID: ", SHOULD_INCREASE_CHECKPOINT_SCORE(EventData.iTeam, EventData.iCurrentLap, EventData.iCurrentLoc), " matches current ID: ", iTeamUniqueLapID[EventData.iTeam])
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_VEHICLE_WEAPON_SOUND(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_VEHICLE_WEAPON_SOUND EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_VEHICLE_WEAPON_SOUND
			STRING sSoundSetName
			
			SWITCH EventData.iSound
				CASE ci_BOMB_SOUND_RELEASE
					sSoundSetName = "DLC_SR_TR_Bomb_Player_Sounds"		
					IF EventData.iTeam != MC_playerBD[iLocalPart].iTeam
						sSoundSetName = "DLC_SR_TR_Bomb_Enemy_Sounds"
					ENDIF
					PLAY_SOUND_FROM_COORD(-1, "Bomb_Release", EventData.vCoords, sSoundSetName, FALSE)
				BREAK
				
				/*CASE ci_SPECIAL_VEHICLE_SOUND_ON_BUGGY
					sSoundSetName = "DLC_SR_TR_Ramp_Buggy_Player_Sounds"					
					IF EventData.iTeam != MC_playerBD[iLocalPart].iTeam
						sSoundSetName = "DLC_SR_TR_Ramp_Buggy_Enemy_Sounds"
					ENDIF
					PLAY_SOUND_FROM_COORD(-1, "Activate", EventData.vCoords, sSoundSetName, FALSE)
				BREAK
				
				CASE ci_SPECIAL_VEHICLE_SOUND_OFF_BUGGY
					sSoundSetName = "DLC_SR_TR_Ramp_Buggy_Player_Sounds"					
					IF EventData.iTeam != MC_playerBD[iLocalPart].iTeam
						sSoundSetName = "DLC_SR_TR_Ramp_Buggy_Enemy_Sounds"
					ENDIF
					PLAY_SOUND_FROM_COORD(-1, "Deactivate", EventData.vCoords, sSoundSetName, FALSE)
				BREAK
				
				CASE ci_SPECIAL_VEHICLE_SOUND_ON_RUINER
					sSoundSetName = "DLC_SR_TR_Ruiner2000_Player_Sounds"					
					IF EventData.iTeam != MC_playerBD[iLocalPart].iTeam
						sSoundSetName = "DLC_SR_TR_Ruiner2000_Enemy_Sounds"
					ENDIF
					PLAY_SOUND_FROM_COORD(-1, "Activate", EventData.vCoords, sSoundSetName, FALSE)
				BREAK
				
				CASE ci_SPECIAL_VEHICLE_SOUND_OFF_RUINER
					sSoundSetName = "DLC_SR_TR_Ruiner2000_Player_Sounds"					
					IF EventData.iTeam != MC_playerBD[iLocalPart].iTeam
						sSoundSetName = "DLC_SR_TR_Ruiner2000_Enemy_Sounds"
					ENDIF
					PLAY_SOUND_FROM_COORD(-1, "Deactivate", EventData.vCoords, sSoundSetName, FALSE)
				BREAK*/
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_PICKED_UP_VEHICLE_WEAPON(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_PICKED_UP_VEHICLE_WEAPON EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_PICKED_UP_VEHICLE_WEAPON
			
			IF EventData.Details.FromPlayerIndex = LocalPlayer
				PRINTLN("[KH][VEHICLE WEAPONS] PROCESS_SCRIPT_EVENT_FMMC_PICKED_UP_VEHICLE_WEAPON - Local player collected: ", GET_VEHICLE_WEAPON_STRING(EventData.iPickupType))				
			ELSE
				PRINTLN("[KH][VEHICLE WEAPONS] PROCESS_SCRIPT_EVENT_FMMC_PICKED_UP_VEHICLE_WEAPON - Player: ", NATIVE_TO_INT(EventData.Details.FromPlayerIndex)," collected: ", GET_VEHICLE_WEAPON_STRING(EventData.iPickupType))
			ENDIF
			
			PED_INDEX pedSender = GET_PLAYER_PED(EventData.Details.FromPlayerIndex)
			
			IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
				
				INT iVehWepsThatCanBeSpawned[ciVEH_WEP_RANDOM]
				INT iCountNumOfWepsThatCanBeSpawned
				INT i
				INT iPickupType
				
				IF EventData.iPickupType != - 1
					IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARS(g_FMMC_STRUCT.iAdversaryModeType)
						STRING sSoundSetName = "DLC_IE_VV_General_Sounds"
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TINY_RACERS_SOUNDS)
							sSoundSetName = "DLC_SR_TR_General_Sounds"
						ENDIF
						
						IF EventData.Details.FromPlayerIndex = LocalPlayer
							PLAY_SOUND_FRONTEND(-1,"Powerup_Collect_local", sSoundSetName)
						ELSE						
							IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
								IF GET_DISTANCE_BETWEEN_COORDS(EventData.vPlayerCoords, GET_ENTITY_COORDS(LocalPlayerPed, FALSE)) < 10.0
									PLAY_SOUND_FRONTEND(-1,"Powerup_Collect_Remote", sSoundSetName)
								ENDIF
							ELSE								
								IF NOT IS_PED_INJURED(pedSender)
									PLAY_SOUND_FROM_ENTITY(-1,"Powerup_Collect_Remote", pedSender, sSoundSetName)
								ENDIF					
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF EventData.iPickupType = ciVEH_WEP_BOMB
				AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TINY_RACERS_SOUNDS)
					IF EventData.Details.FromPlayerIndex = LocalPlayer
						PLAY_SOUND_FRONTEND(-1, "Bomb_Collect", "DLC_SR_TR_Bomb_Player_Sounds")						
					ELSE
						IF NOT IS_PED_INJURED(pedSender)
							PLAY_SOUND_FROM_ENTITY(-1, "Bomb_Collect", pedSender, "DLC_SR_TR_Bomb_Enemy_Sounds")
						ENDIF
					ENDIF
				ENDIF
				
				IF EventData.iPickupType = ciVEH_WEP_ROCKETS
				AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TINY_RACERS_SOUNDS)
					IF EventData.Details.FromPlayerIndex = LocalPlayer			
						PLAY_SOUND_FRONTEND(-1, "Rocket_Collect", "DLC_SR_TR_Rocket_Player_Sounds")
					ELSE
						IF NOT IS_PED_INJURED(pedSender)
							PLAY_SOUND_FROM_ENTITY(-1, "Rocket_Collect", pedSender, "DLC_SR_TR_Rocket_Enemy_Sounds")
						ENDIF
					ENDIF
				ENDIF
				
				IF EventData.iPickupType = ciVEH_WEP_MACHINE_GUN
				AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TINY_RACERS_SOUNDS)
					IF EventData.Details.FromPlayerIndex = LocalPlayer
						PLAY_SOUND_FRONTEND(-1, "Gun_Collect", "DLC_SR_TR_Gun_Player_Sounds")
					ELSE
						IF NOT IS_PED_INJURED(pedSender)
							PLAY_SOUND_FROM_ENTITY(-1, "Gun_Collect", pedSender, "DLC_SR_TR_Gun_Enemy_Sounds")
						ENDIF
					ENDIF
				ENDIF				
				
				INT iPartSender = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(EventData.Details.FromPlayerIndex))
				PRINTLN("[KH][VEHICLE WEAPONS] iPartSending: ", iPartSender)
				
				IF EventData.Details.FromPlayerIndex = LocalPlayer
				AND IS_COLLECTION_OF_VEHICLE_WEAPONS_BLOCKED()
					PRINTLN("[KH][VEHICLE WEAPONS] PROCESS_SCRIPT_EVENT_FMMC_PICKED_UP_VEHICLE_WEAPON - We already have a pickup, so bail out of the event.")
					EXIT
				ENDIF
				
				STRING sSoundSetName
				
				SWITCH(EventData.iPickupType)
					CASE ciVEH_WEP_ROCKETS
						IF EventData.Details.FromPlayerIndex = LocalPlayer
						OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARS(g_FMMC_STRUCT.iAdversaryModeType)
							IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERs(g_FMMC_STRUCT.iAdversaryModeType)
								//PLAY_SOUND_FRONTEND(-1,"Powerup_Collect_local","")
							ELIF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARS(g_FMMC_STRUCT.iAdversaryModeType)
								sSoundSetName = "DLC_IE_VV_General_Sounds"
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TINY_RACERS_SOUNDS)
									sSoundSetName = "DLC_SR_TR_General_Sounds"
								ENDIF								
								PLAY_SOUND_FRONTEND(-1,"Powerup_Collect_local", sSoundSetName)
							ELSE
								IF EventData.Details.FromPlayerIndex = LocalPlayer
									PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_PICKED_UP_VEHICLE_WEAPON] Local Player is sender")
									sSoundSetName = "DLC_IE_TW_Player_Sounds"
								ELIF MC_playerBD[iPartSender].iteam = MC_playerBD[iPartToUse].iteam
									PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_PICKED_UP_VEHICLE_WEAPON] Local Player Team mate is sender")
									sSoundSetName = "DLC_IE_TW_Team_Sounds"
								ELSE
									PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_PICKED_UP_VEHICLE_WEAPON] Enemy Player is Sender")
									sSoundSetName = "DLC_IE_TW_Enemy_Sounds"
								ENDIF
								
								IF NOT IS_PED_INJURED(GET_PLAYER_PED(EventData.Details.FromPlayerIndex))
									PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_PICKED_UP_VEHICLE_WEAPON] Playing: ", sSoundSetName, " from: ", iPartSender)
									PLAY_SOUND_FROM_ENTITY(-1, "Collect_Powerup", GET_PLAYER_PED(EventData.Details.FromPlayerIndex), sSoundSetName)
								ENDIF
							ENDIF
							
							IF EventData.Details.FromPlayerIndex = LocalPlayer
								SET_BIT(MC_playerBD[iLocalPart].iVehicleWeaponCollectedBS, ciVEH_WEP_ROCKETS)
							
								REQUEST_WEAPON_ASSET(WEAPONTYPE_VEHICLE_ROCKET)
							
								ALLOW_COLLECTION_OF_VEHICLE_WEAPONS(FALSE)
							ENDIF
						ENDIF
					BREAK
					
					CASE ciVEH_WEP_MACHINE_GUN
						IF EventData.Details.FromPlayerIndex = LocalPlayer
							SET_BIT(MC_playerBD[iLocalPart].iVehicleWeaponCollectedBS, ciVEH_WEP_MACHINE_GUN)
							ALLOW_COLLECTION_OF_VEHICLE_WEAPONS(FALSE)
						ENDIF
					BREAK
					CASE ciVEH_WEP_SPEED_BOOST
						IF EventData.Details.FromPlayerIndex = LocalPlayer
							SET_BIT(MC_playerBD[iLocalPart].iVehicleWeaponCollectedBS, ciVEH_WEP_SPEED_BOOST)
							ALLOW_COLLECTION_OF_VEHICLE_WEAPONS(FALSE)
						ENDIF
					BREAK
					CASE ciVEH_WEP_GHOST
						IF EventData.Details.FromPlayerIndex = LocalPlayer
							SET_BIT(MC_playerBD[iLocalPart].iVehicleWeaponCollectedBS, ciVEH_WEP_GHOST)
							ALLOW_COLLECTION_OF_VEHICLE_WEAPONS(FALSE)
						ENDIF
					BREAK
					CASE ciVEH_WEP_BEAST
						IF EventData.Details.FromPlayerIndex = LocalPlayer
							SET_BIT(MC_playerBD[iLocalPart].iVehicleWeaponCollectedBS, ciVEH_WEP_BEAST)
							
							//Preload the rhino so we can transition quicker
							REQUEST_MODEL(RHINO)
							
							ALLOW_COLLECTION_OF_VEHICLE_WEAPONS(FALSE)
						ENDIF
					BREAK
					CASE ciVEH_WEP_PRON
						IF EventData.Details.FromPlayerIndex = LocalPlayer
							SET_BIT(MC_playerBD[iLocalPart].iVehicleWeaponCollectedBS, ciVEH_WEP_PRON)
						
							REQUEST_MODEL(SHOTARO)
							
							ALLOW_COLLECTION_OF_VEHICLE_WEAPONS(FALSE)
						ENDIF
					BREAK
					CASE ciVEH_WEP_FORCE_ACCELERATE
						IF EventData.Details.FromPlayerIndex = LocalPlayer
							SET_BIT(MC_playerBD[iLocalPart].iVehicleWeaponCollectedBS, ciVEH_WEP_FORCE_ACCELERATE)
							ALLOW_COLLECTION_OF_VEHICLE_WEAPONS(FALSE)
						ENDIF
					BREAK
					CASE ciVEH_WEP_FLIPPED_CONTROLS
						IF EventData.Details.FromPlayerIndex = LocalPlayer
							SET_BIT(MC_playerBD[iLocalPart].iVehicleWeaponCollectedBS, ciVEH_WEP_FLIPPED_CONTROLS)
							ALLOW_COLLECTION_OF_VEHICLE_WEAPONS(FALSE)
						ENDIF
					BREAK
					CASE ciVEH_WEP_ZONED
						IF EventData.Details.FromPlayerIndex = LocalPlayer
							SET_BIT(MC_playerBD[iLocalPart].iVehicleWeaponCollectedBS, ciVEH_WEP_ZONED)
							ALLOW_COLLECTION_OF_VEHICLE_WEAPONS(FALSE)
						ENDIF
					BREAK
					CASE ciVEH_WEP_DETONATE
						IF EventData.Details.FromPlayerIndex = LocalPlayer
							SET_BIT(MC_playerBD[iLocalPart].iVehicleWeaponCollectedBS, ciVEH_WEP_DETONATE)
							
							// [LM] Save the INDEX of the powerup we picked up.
							MC_playerBD[iLocalPart].iWeaponDetonatorIndex = EventData.iPickupID
									
							ALLOW_COLLECTION_OF_VEHICLE_WEAPONS(FALSE)
						ENDIF
					BREAK
					CASE ciVEH_WEP_BOMB
						IF EventData.Details.FromPlayerIndex = LocalPlayer
							SET_BIT(MC_playerBD[iLocalPart].iVehicleWeaponCollectedBS, ciVEH_WEP_BOMB)
							ALLOW_COLLECTION_OF_VEHICLE_WEAPONS(FALSE)
							IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
								MC_playerBD[iPartToUse].iQuantityOfBombs = g_FMMC_STRUCT.iBomberModeTotalBombsPerPlayer
							ELSE
								MC_playerBD[iPartToUse].iQuantityOfBombs = iPickupQuantity_Bombs
							ENDIF
						ENDIF
					BREAK
					CASE ciVEH_WEP_BOUNCE
						IF EventData.Details.FromPlayerIndex = LocalPlayer
							SET_BIT(MC_playerBD[iLocalPart].iVehicleWeaponCollectedBS, ciVEH_WEP_BOUNCE)
							ALLOW_COLLECTION_OF_VEHICLE_WEAPONS(FALSE)
							MC_playerBD[iPartToUse].iQuantityOfBounces = g_FMMC_STRUCT.iNumberOfBounces
						ENDIF
					BREAK
					CASE ciVEH_WEP_REPAIR
						IF EventData.Details.FromPlayerIndex = LocalPlayer
							SET_BIT(MC_playerBD[iLocalPart].iVehicleWeaponCollectedBS, ciVEH_WEP_REPAIR)
							ALLOW_COLLECTION_OF_VEHICLE_WEAPONS(FALSE)
						ENDIF
					BREAK
					CASE ciVEH_WEP_BOMB_LENGTH
						IF EventData.Details.FromPlayerIndex = LocalPlayer
							SET_BIT(MC_playerBD[iLocalPart].iVehicleWeaponCollectedBS, ciVEH_WEP_BOMB_LENGTH)
							ALLOW_COLLECTION_OF_VEHICLE_WEAPONS(TRUE)
							FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons-1
								IF EventData.iPickupID = NATIVE_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].id)
									IF ((MC_playerBD_1[iLocalPart].iBmbCurrentRadius + 1) <= ciDROPTHEBOMB_Max_Bomb_Length)
									AND (MC_playerBD_1[iLocalPart].iBmbCurrentRadius + g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iVehBombLengthIncrease > 0)
										PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_PICKED_UP_VEHICLE_WEAPON - Pre change iBmbCurrentRadius: ", MC_playerBD_1[iLocalPart].iBmbCurrentRadius)
										MC_playerBD_1[iLocalPart].iBmbCurrentRadius += g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iVehBombLengthIncrease
										PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_PICKED_UP_VEHICLE_WEAPON - Setting iBmbExtraLengthFromPickup to: ", MC_playerBD_1[iLocalPart].iBmbCurrentRadius)
									ENDIF
									BREAKLOOP
								ENDIF
							ENDFOR
						ENDIF
					BREAK
					CASE ciVEH_WEP_BOMB_MAX
						IF EventData.Details.FromPlayerIndex = LocalPlayer
							SET_BIT(MC_playerBD[iLocalPart].iVehicleWeaponCollectedBS, ciVEH_WEP_BOMB_MAX)
							ALLOW_COLLECTION_OF_VEHICLE_WEAPONS(TRUE)
							FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons-1
								IF EventData.iPickupID = NATIVE_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].id)
									IF (MC_playerBD_1[iLocalPart].iBmbCurrentAmountOfBombs + g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iVehBombMaxIncrease) <= ciBMB_MAX_TOTAL_BOMBS_PER_PLAYER
									AND (MC_playerBD_1[iLocalPart].iBmbCurrentAmountOfBombs + g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iVehBombMaxIncrease > 0)
										PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_PICKED_UP_VEHICLE_WEAPON - Pre change iBmbCurrentAmountOfBombs: ", MC_playerBD_1[iLocalPart].iBmbCurrentAmountOfBombs)
										PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_PICKED_UP_VEHICLE_WEAPON - Pre change iVehBombMaxIncrease: ", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iVehBombMaxIncrease)
									
										MC_playerBD_1[iLocalPart].iBmbCurrentAmountOfBombs += g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iVehBombMaxIncrease
										MC_playerBD_1[iLocalPart].iBmbAvailableBombs += g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iVehBombMaxIncrease
										PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_PICKED_UP_VEHICLE_WEAPON - Setting iBmbCurrentAmountOfBombs to: ", MC_playerBD_1[iLocalPart].iBmbCurrentAmountOfBombs)
									ENDIF
									BREAKLOOP
								ENDIF
							ENDFOR
						ENDIF
					BREAK
					CASE ciVEH_WEP_EXTRA_LIFE
						IF EventData.Details.FromPlayerIndex = LocalPlayer
							PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_PICKED_UP_VEHICLE_WEAPON - Setting ciVEH_WEP_EXTRA_LIFE")
							SET_BIT(MC_playerBD[iLocalPart].iVehicleWeaponCollectedBS, ciVEH_WEP_EXTRA_LIFE)
							ALLOW_COLLECTION_OF_VEHICLE_WEAPONS(TRUE)
						ENDIF
					BREAK
					CASE ciVEH_WEP_RANDOM
						IF EventData.Details.FromPlayerIndex = LocalPlayer
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciVEHICLE_WEAPONS_RANDOM_PICKUPS)
								IF IS_BIT_SET(MC_serverBD.iServerBitSet4, SBBOOL4_DEADLINE_POWER_UP_ACTIVE_FOR_TEAM_0 + MC_playerBD[iPartToUse].iTeam)
									iPickupType = GET_RANDOM_INT_IN_RANGE(ciVEH_WEP_ROCKETS, ciVEH_WEP_RANDOM - 1)
								ELSE
									iPickupType = GET_RANDOM_INT_IN_RANGE(ciVEH_WEP_ROCKETS, ciVEH_WEP_RANDOM)
								ENDIF
							ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciVEHICLE_WEAPONS_CUSTOM_RANDOM_PICKUPS)
								FOR i = 0 TO ciVEH_WEP_BOUNCE
									IF IS_BIT_SET(g_FMMC_STRUCT.iRandomVehWepBS, i)
										IF i = ciVEH_WEP_PRON
											IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet4, SBBOOL4_DEADLINE_POWER_UP_ACTIVE_FOR_TEAM_0 + MC_playerBD[iPartToUse].iTeam)
												iVehWepsThatCanBeSpawned[iCountNumOfWepsThatCanBeSpawned] = i
												iCountNumOfWepsThatCanBeSpawned++
											ENDIF
										ELSE
											iVehWepsThatCanBeSpawned[iCountNumOfWepsThatCanBeSpawned] = i
											iCountNumOfWepsThatCanBeSpawned++
										ENDIF
									ENDIF
								ENDFOR
								iPickupType = iVehWepsThatCanBeSpawned[GET_RANDOM_INT_IN_RANGE(ciVEH_WEP_ROCKETS, iCountNumOfWepsThatCanBeSpawned)]
								PRINTLN("[KH] PROCESS_SCRIPT_EVENT_FMMC_PICKED_UP_VEHICLE_WEAPON - RANDOM PICKUP - iPickupType:, ", iPickupType)
							ENDIF
							BROADCAST_FMMC_PICKED_UP_VEHICLE_WEAPON(GET_ENTITY_COORDS(PLAYER_PED_ID()), iPickupType, GET_PLAYER_TEAM(PLAYER_ID()), EventData.iPickupID)
							ALLOW_COLLECTION_OF_VEHICLE_WEAPONS(FALSE)
						ENDIF
					BREAK
					CASE ciVEH_WEP_RANDOM_SPECIAL_VEH
						IF EventData.Details.FromPlayerIndex = LocalPlayer
							PRINTLN("[KH] PROCESS_SCRIPT_EVENT_FMMC_PICKED_UP_VEHICLE_WEAPON - ciVEH_WEP_RANDOM_SPECIAL_VEH PICKUP - iPickupType:, ", iPickupType)
							SET_BIT(MC_playerBD[iLocalPart].iVehicleWeaponCollectedBS, ciVEH_WEP_RANDOM_SPECIAL_VEH)
							ALLOW_COLLECTION_OF_VEHICLE_WEAPONS(FALSE)
						ENDIF
					BREAK
					CASE ciVEH_WEP_RUINER_SPECIAL_VEH
						IF EventData.Details.FromPlayerIndex = LocalPlayer
							SET_BIT(MC_playerBD[iLocalPart].iVehicleWeaponCollectedBS, ciVEH_WEP_RUINER_SPECIAL_VEH)
							PRINTLN("[KH] PROCESS_SCRIPT_EVENT_FMMC_PICKED_UP_VEHICLE_WEAPON - ciVEH_WEP_RUINER_SPECIAL_VEH PICKUP - iPickupType:, ", iPickupType)
							ALLOW_COLLECTION_OF_VEHICLE_WEAPONS(FALSE)
						ENDIF
					BREAK
					CASE ciVEH_WEP_RAMP_SPECIAL_VEH
						IF EventData.Details.FromPlayerIndex = LocalPlayer
							PRINTLN("[KH] PROCESS_SCRIPT_EVENT_FMMC_PICKED_UP_VEHICLE_WEAPON - ciVEH_WEP_RAMP_SPECIAL_VEH PICKUP - iPickupType:, ", iPickupType)
							SET_BIT(MC_playerBD[iLocalPart].iVehicleWeaponCollectedBS, ciVEH_WEP_RAMP_SPECIAL_VEH)
							ALLOW_COLLECTION_OF_VEHICLE_WEAPONS(FALSE)
						ENDIF
					BREAK
				ENDSWITCH
				
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_POWER_UP_STOLEN(INT iEventID)
	PRINTLN("[KH][VEHICLE WEAPONS][LM][BROADCAST_VEHICULAR_VENDETTA_PICKUP_STOLEN] - Broadcasting...")
	
	SCRIPT_EVENT_DATA_FMMC_POWER_UP_STOLEN EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_POWER_UP_STOLEN
			
			TEXT_LABEL_63 tl63PickupStolenItem = ""
			TEXT_LABEL_63 tl63PickupStolenShard = ""
			// Find the involved parties iPart number.
			INT iPartStolenFrom = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(EventData.piPlayerStolenFrom))
			INT iPartStolenBy = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(EventData.piPlayerStolenBy))			
			
			CLEAR_BIT(iLocalBoolCheck20, LBOOL20_PLAY_GHOST_AUDIO_EVENT)
						
			PRINTLN("[KH][VEHICLE WEAPONS][LM][BROADCAST_VEHICULAR_VENDETTA_PICKUP_STOLEN] - The player who was stolen from: ", GET_PLAYER_NAME(EventData.piPlayerStolenFrom))
			PRINTLN("[KH][VEHICLE WEAPONS][LM][BROADCAST_VEHICULAR_VENDETTA_PICKUP_STOLEN] - THe player who stole something: ", GET_PLAYER_NAME(EventData.piPlayerStolenBy))
			
			// Change the shard depending on if you are the stealer or the person who had something stolen from them.
			IF localPlayer = EventData.piPlayerStolenFrom
				
				tl63PickupStolenShard = "FMMC_STL_FRM" // I had something stolen from me.
			ELIF localPlayer = EventData.piPlayerStolenBy
				
				tl63PickupStolenShard = "FMMC_STL_BY" // I stole something.
			ENDIF
			
			PRINTLN("[KH][VEHICLE WEAPONS][LM][BROADCAST_FMMC_POWER_UP_STOLEN] - The powerup that was stolen has a value of: ", EventData.iPowerUpStolen)
						
			IF localPlayer = EventData.piPlayerStolenFrom
				
				PRINTLN("[KH][VEHICLE WEAPONS][LM][BROADCAST_FMMC_POWER_UP_STOLEN] - My iPart is: ", iPartToUse, "  I had a powerup stolen from me, clearing bits.")
				CLEAR_BIT(MC_playerBD[iLocalPart].iVehicleWeaponCollectedBS, EventData.iPowerUpStolen)
				CLEANUP_STOLEN_WEAPON()
			ELIF localPlayer = EventData.piPlayerStolenBy
				
				PRINTLN("[KH][VEHICLE WEAPONS][LM][BROADCAST_FMMC_POWER_UP_STOLEN] - My iPart is: ", iPartToUse, "  I stole a powerup, setting bits.")
				SET_BIT(MC_playerBD[iLocalPart].iVehicleWeaponCollectedBS, EventData.iPowerUpStolen)
				//Extra activation logic to save us rebroadcasting the collection event
				SWITCH(EventData.iPowerUpStolen)
					CASE ciVEH_WEP_BOMB
						MC_playerBD[iPartToUse].iQuantityOfBombs = iPickupQuantity_Bombs
					BREAK
				ENDSWITCH
			ENDIF
									
			// Check which item was stolen to assign the text.
			SWITCH(EventData.iPowerUpStolen)
				CASE ciVEH_WEP_ROCKETS
					// Rockets
					tl63PickupStolenItem = "VVHUD_ROCKETS" 
					PRINTLN("[KH][VEHICLE WEAPONS][LM][BROADCAST_VEHICULAR_VENDETTA_PICKUP_STOLEN] - Rockets was stolen")
				BREAK
				
				CASE ciVEH_WEP_SPEED_BOOST
					// Speed Boost	
					tl63PickupStolenItem = "VVHUD_SPEEDB"
					PRINTLN("[KH][VEHICLE WEAPONS][LM][BROADCAST_VEHICULAR_VENDETTA_PICKUP_STOLEN] - Speed Boost was stolen")
				BREAK
				
//				CASE ciVEH_WEP_GHOST
//					// Ghost
//					tl63PickupStolenItem = "VVHUD_GHOST"
//					PRINTLN("[KH][VEHICLE WEAPONS][LM][BROADCAST_VEHICULAR_VENDETTA_PICKUP_STOLEN] - Ghost was stolen")
//				BREAK
				
				CASE ciVEH_WEP_BOMB
					// Bomb
					tl63PickupStolenItem = "VVHUD_BOMB"
					PRINTLN("[KH][VEHICLE WEAPONS][LM][BROADCAST_VEHICULAR_VENDETTA_PICKUP_STOLEN] - Bomb was stolen")
				BREAK
				
				CASE ciVEH_WEP_DETONATE
					// Detonate
					tl63PickupStolenItem = "VVHUD_DET"
					PRINTLN("[KH][VEHICLE WEAPONS][LM][BROADCAST_VEHICULAR_VENDETTA_PICKUP_STOLEN] - Detonate was stolen")
					
					IF localPlayer = EventData.piPlayerStolenBy
						MC_playerBD[iLocalPart].iWeaponDetonatorIndex = MC_playerBD[iPartStolenFrom].iWeaponDetonatorIndex
					ENDIF
				BREAK
				
				CASE ciVEH_WEP_ZONED
					// Zoned
					tl63PickupStolenItem = "VVHUD_ZONE"
					PRINTLN("[KH][VEHICLE WEAPONS][LM][BROADCAST_VEHICULAR_VENDETTA_PICKUP_STOLEN] - Zoned was stolen")
				BREAK
				
				CASE ciVEH_WEP_FLIPPED_CONTROLS
					// Flipped
					tl63PickupStolenItem = "VVHUD_FLIP"
					PRINTLN("[KH][VEHICLE WEAPONS][LM][BROADCAST_VEHICULAR_VENDETTA_PICKUP_STOLEN] - Flipped was stolen")
				BREAK

				CASE ciVEH_WEP_FORCE_ACCELERATE
					// Jammed
					tl63PickupStolenItem = "VVHUD_JAMM"
					PRINTLN("[KH][VEHICLE WEAPONS][LM][BROADCAST_VEHICULAR_VENDETTA_PICKUP_STOLEN] - Jammed was stolen")
				BREAK
				
				CASE ciVEH_WEP_BEAST
					// Beast
					tl63PickupStolenItem = "VVHUD_BEAST"
					PRINTLN("[KH][VEHICLE WEAPONS][LM][BROADCAST_VEHICULAR_VENDETTA_PICKUP_STOLEN] - Armoured was stolen")
				BREAK
				
				CASE ciVEH_WEP_PRON
					// Deadline
					tl63PickupStolenItem = "VVHUD_DEADLINE"
					PRINTLN("[KH][VEHICLE WEAPONS][LM][BROADCAST_VEHICULAR_VENDETTA_PICKUP_STOLEN] - Deadline was stolen")
				BREAK
				
				CASE ciVEH_WEP_REPAIR
					// Repair
					tl63PickupStolenItem = "VVHUD_REPAIR"
					PRINTLN("[KH][VEHICLE WEAPONS][LM][BROADCAST_VEHICULAR_VENDETTA_PICKUP_STOLEN] - Repair was stolen")
				BREAK
				
				CASE ciVEH_WEP_BOUNCE
					// Armoured
					tl63PickupStolenItem = "VVHUD_BOUNCE"
					PRINTLN("[KH][VEHICLE WEAPONS][LM][BROADCAST_VEHICULAR_VENDETTA_PICKUP_STOLEN] - Bunny Hop was stolen")
				BREAK
				
				CASE ciVEH_WEP_RANDOM_SPECIAL_VEH
					// Speical Vehicle
					tl63PickupStolenItem = "VVHUD_RSPECIAL"
					PRINTLN("[KH][VEHICLE WEAPONS][LM][BROADCAST_VEHICULAR_VENDETTA_PICKUP_STOLEN] - Random spec veh was stolen")
				BREAK
				
				CASE ciVEH_WEP_RUINER_SPECIAL_VEH
					// Ruiner
					tl63PickupStolenItem = "VVHUD_RUINER"
					PRINTLN("[KH][VEHICLE WEAPONS][LM][BROADCAST_VEHICULAR_VENDETTA_PICKUP_STOLEN] - Ruiner was stolen")
				BREAK
				
				CASE ciVEH_WEP_RAMP_SPECIAL_VEH
					// Ramp Buggy
					tl63PickupStolenItem = "VVHUD_RAMP"
					PRINTLN("[KH][VEHICLE WEAPONS][LM][BROADCAST_VEHICULAR_VENDETTA_PICKUP_STOLEN] - Ramp Buggy was stolen")
				BREAK
					
			ENDSWITCH 		
			
			IF localPlayer = EventData.piPlayerStolenFrom
				// "<Player_1> Stole <Rockets> From you"
				
				IF NOT IS_BIT_SET(iLocalBoolCheck20, LBOOL20_PLAY_GHOST_AUDIO_EVENT)
					PLAY_SOUND_FRONTEND(-1,"Lose_Powerup" ,"DLC_IE_VV_General_Sounds")
					SET_BIT(iLocalBoolCheck20, LBOOL20_PLAY_GHOST_AUDIO_EVENT)
				ENDIF
				
				HUD_COLOURS hcPlayerTeamColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_playerBD[iPartStolenBy].iteam, EventData.piPlayerStolenBy)
				//PRINT_TICKER_WITH_PLAYER_NAME_AND_LITERAL_STRING_WITH_HUD_COLOUR(tl63PickupStolenShard, EventData.piPlayerStolenBy, tl63PickupStolenItem, hcPlayerTeamColour, DEFAULT)
				PRINT_TICKER_WITH_PLAYER_NAME_AND_STRING_WITH_HUD_COLOUR(tl63PickupStolenShard, EventData.piPlayerStolenBy, tl63PickupStolenItem, hcPlayerTeamColour, DEFAULT)
				PRINTLN("[KH][VEHICLE WEAPONS][LM][BROADCAST_VEHICULAR_VENDETTA_PICKUP_STOLEN] Play Shard for: ", GET_PLAYER_NAME(EventData.piPlayerStolenFrom))
			ELIF localPlayer = EventData.piPlayerStolenBy
				// "You Stole <Rockets>"
				
				IF NOT IS_BIT_SET(iLocalBoolCheck20, LBOOL20_PLAY_GHOST_AUDIO_EVENT)
					PLAY_SOUND_FRONTEND(-1,"Steal_Powerup"  ,"DLC_IE_VV_General_Sounds")
					SET_BIT(iLocalBoolCheck20, LBOOL20_PLAY_GHOST_AUDIO_EVENT)
				ENDIF
				
				//PRINT_TICKER_WITH_ONE_CUSTOM_STRING(tl63PickupStolenShard, tl63PickupStolenItem)
				PRINT_TICKER_WITH_STRING(tl63PickupStolenShard, tl63PickupStolenItem)
				PRINTLN("[KH][VEHICLE WEAPONS][LM][BROADCAST_VEHICULAR_VENDETTA_PICKUP_STOLEN] Play Shard for: ", GET_PLAYER_NAME(EventData.piPlayerStolenBy))
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_FOR_INSTANCED_CONTENT EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT
			PARTICIPANT_INDEX piPart
			PLAYER_INDEX piPlayer
			PED_INDEX pedPlayer
			
			SWITCH EventData.iType
				CASE g_ciInstancedContentEventType_AllPlayersToggleConsiderVehicle
					PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedContentEventType_AllPlayersToggleConsiderVehicle] - iVeh: ", EventData.iVeh)
					NETWORK_INDEX netVeh
					VEHICLE_INDEX viVeh					
					netVeh = MC_serverBD_1.sFMMC_SBD.niVehicle[EventData.iVeh]	
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(netVeh)
						viVeh = NET_TO_VEH(netVeh)
					ELSE
						PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedContentEventType_AllPlayersToggleConsiderVehicle] - NetID does not exist.")
						EXIT
					ENDIF					
					IF DOES_ENTITY_EXIST(viVeh)
					AND IS_VEHICLE_DRIVEABLE(viVeh)
						PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedContentEventType_AllPlayersToggleConsiderVehicle] - Setting vehicle considered with bool: ", EventData.bToggle)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(viVeh, EventData.bToggle)
					ENDIF
						
					IF EventData.bToggle
						IF NETWORK_HAS_CONTROL_OF_ENTITY(viVeh)
							SET_VEHICLE_HANDBRAKE(viVeh, FALSE)
						ENDIF
						SET_BIT(iVehDeliveryStoppedCancelled, EventData.iVeh)
					ELSE
						CLEAR_BIT(iVehDeliveryStoppedCancelled, EventData.iVeh)
					ENDIF
				BREAK
				
				CASE g_ciInstancedContentEventType_HeliCamLookingAtCar
					IF bIsLocalPlayerHost
						IF NOT IS_BIT_SET(MC_ServerBD_1.iPartLookingAtCar, EventData.iPart)
							PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedContentEventType_HeliCamLookingAtCar] - Setting iPart: ", EventData.iPart, " is Looking at Car.")
							SET_BIT(MC_ServerBD_1.iPartLookingAtCar, EventData.iPart)
						ENDIF
					ENDIF
				BREAK
				
				CASE g_ciInstancedContentEventType_HeliCamStoppedLookingAtCar
					IF bIsLocalPlayerHost
						IF IS_BIT_SET(MC_ServerBD_1.iPartLookingAtCar, EventData.iPart)
							PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedContentEventType_HeliCamStoppedLookingAtCar] - Setting iPart: ", EventData.iPart, " Has stopped looking at car")
							CLEAR_BIT(MC_ServerBD_1.iPartLookingAtCar, EventData.iPart)
						ENDIF
					ENDIF
				BREAK
				
				CASE g_ciInstancedContentEventType_CarnageBarKilledPed		
					IF bIsLocalPlayerHost
						PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedContentEventType_CarnageBarKilledPed] - Setting iPart: ", EventData.iPart, " has killed a ped.")
						MC_ServerBD_1.fCarnageBar_PedKilledForFill++
						PRINTLN("[LM][CARNAGE_BAR] - Killed Ped fCarnageBar_PedKilledForFill: ", MC_ServerBD_1.fCarnageBar_PedKilledForFill)
					ENDIF					
				BREAK
				
				CASE g_ciInstancedContentEventType_CarnageBarDestroyedVehicle
					IF bIsLocalPlayerHost
						PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedContentEventType_CarnageBarDestroyedVehicle] - Setting iPart: ", EventData.iPart, " has destroyed a vehicle.")
						MC_ServerBD_1.fCarnageBar_VehDestroyedForFill++
						PRINTLN("[LM][CARNAGE_BAR] - Destroyed Veh fCarnageBar_VehDestroyedForFill: ", MC_ServerBD_1.fCarnageBar_VehDestroyedForFill)
					ENDIF	
				BREAK
				
				CASE g_ciInstancedContentEventType_CarnageBarPerformedStuntAir
					IF bIsLocalPlayerHost
						PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedContentEventType_CarnageBarPerformedStuntAir] - Setting iPart: ", EventData.iPart, " has performed a stunt..")
						MC_ServerBD_1.fCarnageBar_StuntPerformedForFill++
						PRINTLN("[LM][CARNAGE_BAR] - Destroyed Veh fCarnageBar_StuntPerformedForFill: ", MC_ServerBD_1.fCarnageBar_StuntPerformedForFill)
					ENDIF	
				BREAK
				
				CASE g_ciInstancedContentEventType_ActivateZoneRadiusChange		
					SET_BIT(iZoneBS_PlayerPedEntered, EventData.iZone)
				BREAK
				
				CASE g_ciInstancedContentEventType_PedClearedFixationSettings
					PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedContentEventType_PedClearedFixationSettings] - Setting iPart: ", EventData.iPart, " has reset fixation settings for iPed: ", EventData.iPed)
					SET_BIT(iPedFixationStateCleanedUpBS[GET_LONG_BITSET_INDEX(EventData.iPed)], GET_LONG_BITSET_BIT(EventData.iPed))
				BREAK
				
				CASE g_ciInstancedcontentEventType_TrailerDetachedForPed					
					PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_TrailerDetachedForPed] - Setting iPart: ", EventData.iPart, " Trailer Detached for iPed: ", EventData.iPed)
					SET_BIT(iPedTrailerDetachedBitset[GET_LONG_BITSET_INDEX(EventData.iPed)], GET_LONG_BITSET_BIT(EventData.iPed))
				BREAK
				
				CASE g_ciInstancedcontentEventType_HasTrailerattachedForPed
					PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_HasTrailerattachedForPed] - Setting iPart: ", EventData.iPart, " Trailer Detached for iPed: ", EventData.iPed)
					SET_BIT(iPedHasTrailerBitset[GET_LONG_BITSET_INDEX(EventData.iPed)], GET_LONG_BITSET_BIT(EventData.iPed))
				BREAK
				
				CASE g_ciInstancedcontentEventType_WarpedVehicleOnRuleStart
					PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_WarpedVehicleOnRuleStart] - Setting iPart: ", EventData.iPart, " Warped Vehicle on rule start iVeh: ", EventData.iVeh, " Setting iVehicleWarpedOnThisRule.")
					SET_BIT(iVehicleWarpedOnThisRule, EventData.iVeh)
				BREAK
				
				CASE g_ciInstancedcontentEventType_SyncSceneBroadcast
					PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_SyncSceneBroadcast] - Setting iPart: ", EventData.iPart, " assigning iPed: ", EventData.iPed, " to iSyncScene: ", EventData.iGenericInt)
					sMissionPedsLocalVars[EventData.iPed].iSyncScene = EventData.iGenericInt
				BREAK
				
				CASE g_ciInstancedcontentEventType_SetBitset_SyncScene
					PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_SetBitset_SyncScene] - Setting iPart: ", EventData.iPart, " assigning iPed: ", EventData.iPed, " Setting BITSET: ", EventData.iGenericInt)
					SET_BIT(sMissionPedsLocalVars[EventData.iPed].iPedBS, EventData.iGenericInt)
				BREAK
				
				CASE g_ciInstancedcontentEventType_SetPedSyncSceneState
					IF EventData.iPart != iLocalPart // Sender/Host will set this locally. We only care about broadcasting this to sync data in case of a host migration.
						PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_SetBitset_PedFinishedBreakoutAnims][Sync_anims: ", EventData.iPed, "] - Setting iPart: ", EventData.iPart, " assigning iPed: ", EventData.iPed, " Setting ci_MissionPedBS_iSyncSceneAnimationBreakoutFinished.")
						SET_PED_SYNC_SCENE_STATE(EventData.iPed, INT_TO_ENUM(PED_SYNC_SCENE_STATE, EventData.iGenericInt))
					ENDIF
				BREAK
				
				CASE g_ciInstancedcontentEventType_RequestProgressToRule
					IF bIsLocalPlayerHost
						PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_RequestProgressToRule] - Setting iPart: ", EventData.iPart, " Progressing to iRule: ", EventData.iGenericInt)
						SET_PROGRESS_OBJECTIVE_FOR_TEAM(MC_PlayerBD[EventData.iPart].iteam, TRUE, EventData.iGenericInt)
					ENDIF
				BREAK
				
				CASE g_ciInstancedcontentEventType_SyncScenePedOwnerBroadcast
					PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_SyncScenePedOwnerBroadcast][Sync_anims: ", EventData.iPed, "] - Setting iPart: ", EventData.iPart, " as the anim owner for assigning iPed: ", EventData.iPed)
					IF EventData.iPart != iLocalPart
						sMissionPedsLocalVars[EventData.iPed].iSyncPedPartOwner = EventData.iPart
					ENDIF
				BREAK
				
				CASE g_ciInstancedcontentEventType_ClearRoomForPlayerPed
					PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_ClearRoomForPlayerPed] - Setting iPart: ", EventData.iPart, " requesting room key be cleared...")
					
					IF EventData.iPart != iLocalPart
						PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_ClearRoomForPlayerPed] - Performing Validity Checks.")
						
						piPart = INT_TO_PARTICIPANTINDEX(EventData.iPart)
						
						IF NETWORK_IS_PARTICIPANT_ACTIVE(piPart)							
							piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)
							pedPlayer = GET_PLAYER_PED(piPlayer)
							
							IF NOT IS_PED_INJURED(pedPlayer)
							
								PRINTLN("[LM][RCC MISSION]  [MISSION START POS] - CLEAR_ROOM_AFTER_START_POSITION_WARP - Calling CLEAR_ROOM_FOR_ENTITY for Ped.")
								
								IF IS_PED_IN_ANY_VEHICLE(pedPlayer, TRUE)
									VEHICLE_INDEX vehTemp
									vehTemp = GET_VEHICLE_PED_IS_IN(pedPlayer, TRUE)
									
									IF NETWORK_HAS_CONTROL_OF_ENTITY(vehTemp)
									AND IS_VEHICLE_DRIVEABLE(vehTemp)
										PRINTLN("[LM][RCC MISSION]  [MISSION START POS] - CLEAR_ROOM_AFTER_START_POSITION_WARP - Calling CLEAR_ROOM_FOR_ENTITY for Vehicle.")
										CLEAR_ROOM_FOR_ENTITY(vehTemp)
										
										PRINTLN("[LM][RCC MISSION]  [MISSION START POS] - CLEAR_ROOM_AFTER_START_POSITION_WARP - Calling SET_ENTITY_VISIBLE for Vehicle.")
										SET_ENTITY_VISIBLE(vehTemp, TRUE)
									ENDIF
								ENDIF
								
								IF NETWORK_HAS_CONTROL_OF_ENTITY(pedPlayer)
									PRINTLN("[LM][RCC MISSION]  [MISSION START POS] - CLEAR_ROOM_AFTER_START_POSITION_WARP - Calling SET_ENTITY_VISIBLE for Ped.")
									SET_ENTITY_VISIBLE(pedPlayer, TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE g_ciInstancedcontentEventType_SetVehVisible
					PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_SetVehVisible] called with iVeh: ", EventData.iVeh)
					IF EventData.iVeh > -1
						NETWORK_INDEX niVeh
						niVeh = MC_serverBD_1.sFMMC_SBD.niVehicle[EventData.iVeh]
						IF NETWORK_DOES_NETWORK_ID_EXIST(niVeh)
							VEHICLE_INDEX veh
							veh = NET_TO_VEH(niVeh)
							IF IS_VEHICLE_DRIVEABLE(veh)
								IF NETWORK_HAS_CONTROL_OF_ENTITY(veh)
									PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_SetVehVisible] Setting iVeh: ", EventData.iVeh, " as visible")
									SET_ENTITY_VISIBLE(veh, TRUE)
								ELSE
									PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_SetVehVisible] cant set iVeh: ", EventData.iVeh, " as visible as we do not have control")
								ENDIF
							ELSE
								PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_SetVehVisible] iVeh: ", EventData.iVeh, " is not driveable")
							ENDIF
						ELSE
							PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_SetVehVisible] iVeh: ", EventData.iVeh, " Does not exist")
						ENDIF
					ENDIF
				BREAK				
				CASE g_ciInstancedcontentEventType_HideHeistBagForPart
					PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_SetVehVisible] iPart: ", EventData.iPart, " Having their duffel bag hidden")
					
					piPart = INT_TO_PARTICIPANTINDEX(EventData.iPart)					
					IF NETWORK_IS_PARTICIPANT_ACTIVE(piPart)
						piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)
						pedPlayer = GET_PLAYER_PED(piPlayer)
						
						IF NOT IS_PED_INJURED(pedPlayer)
							IF EventData.iPart = iLocalPart
								SET_PED_COMPONENT_VARIATION(pedPlayer, PED_COMP_HAND, 0, 0)
							ELSE
								SET_PED_COMPONENT_VARIATION(pedPlayer, PED_COMP_HAND, 0, 0)
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE g_ciInstancedcontentEventType_ShowHeistBagForPart
					PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_SetVehVisible] iPart: ", EventData.iPart, " Having their duffel bag shown")
					
					piPart = INT_TO_PARTICIPANTINDEX(EventData.iPart)					
					IF NETWORK_IS_PARTICIPANT_ACTIVE(piPart)
						piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)						
						pedPlayer = GET_PLAYER_PED(piPlayer)
						
						IF NOT IS_PED_INJURED(pedPlayer)
							IF EventData.iPart = iLocalPart
								APPLY_DUFFEL_BAG_HEIST_GEAR(pedPlayer, iLocalPart)
							ELSE
								APPLY_DUFFEL_BAG_HEIST_GEAR(pedPlayer, EventData.iPart)
							ENDIF
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_DELIVER_VEHICLE_STOP(INT iEventID)
	PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_DELIVER_VEHICLE_STOP] - Processing...")
	
	SCRIPT_EVENT_DATA_FMMC_DELIVER_VEHICLE_STOP EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_DELIVER_VEHICLE_STOP
			IF bIsLocalPlayerHost
				PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_DELIVER_VEHICLE_STOP] - Host Processing - iVeh: ", EventData.iVehicle, " iTimeToStop: ", EventData.iTimeToStop)
				IF EventData.iVehicle > -1				
					IF EventData.iTimeToStop > 0						
						MC_serverBD_1.iVehStopTimeToWait = EventData.iTimeToStop
						SET_BIT(MC_serverBD_1.iBSVehicleStopDuration, EventData.iVehicle)	
						RESET_NET_TIMER(MC_serverBD_1.timeVehicleStop)
						START_NET_TIMER(MC_serverBD_1.timeVehicleStop)			
					ELSE						
						SET_BIT(MC_serverBD_1.iBSVehicleStopForever, EventData.iVehicle)
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_GHOST_STARTED(INT iEventID)
	PRINTLN("[KH][VEHICLE WEAPONS][LM][PROCESS_SCRIPT_EVENT_FMMC_GHOST_STARTED] - Processing...")
	
	SCRIPT_EVENT_DATA_FMMC_GHOST_STARTED EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_GHOST_STARTED
			VEHICULAR_VENDETTA_SET_GHOSTED_PLAYER_ALPHA(EventData.bActivate)
		ENDIF	
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_SPAWN_PROTECTION_ALPHA(INT iEventID)
	PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ZONE_SPAWN_PROTECTION_ALPHA] - Processing...")
	
	SCRIPT_EVENT_DATA_FMMC_SPAWN_PROTECTION_ALPHA EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_SPAWN_PROTECTION_ALPHA
			
			PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ZONE_SPAWN_PROTECTION_ALPHA] - bActivate: ", EventData.bActivate, " On Participant: ", EventData.iPart)
			
			IF iLocalPart = EventData.iPart
			
			ELSE
				IF EventData.iPart > -1
					IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(EventData.iPart))
						PED_INDEX piPed =  GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(EventData.iPart)))
				
						IF NOT IS_PED_INJURED(piPed)
							IF EventData.bActivate
							AND NOT IS_BIT_SET(MC_PlayerBD[EventData.iPart].iClientBitset4, PBBOOL4_ROAMING_SPECTATOR)
								PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ZONE_SPAWN_PROTECTION_ALPHA] - Setting alpha to 50 ")
								SET_ENTITY_ALPHA(piPed, 50, FALSE)
							ELSE
								PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ZONE_SPAWN_PROTECTION_ALPHA] - Resetting Alpha ")
								RESET_ENTITY_ALPHA(piPed)	
								IF IS_PED_IN_ANY_VEHICLE(piPed)
								AND DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(piPed))
									RESET_ENTITY_ALPHA(GET_VEHICLE_PED_IS_IN(piPed))
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_AUDIO_TRIGGER_ACTIVATED(INT iEventID)
	PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_AUDIO_TRIGGER_ACTIVATED] - Processing...")
	
	SCRIPT_EVENT_DATA_FMMC_AUDIO_TRIGGER_ACTIVATED EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_AUDIO_TRIGGER_ACTIVATED
			IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[EventData.iProp].iSTMSoundIDVar = -1
				g_FMMC_STRUCT_ENTITIES.sPlacedProp[EventData.iProp].iSTMSoundIDVar = GET_SOUND_ID()
			ENDIF
			
			PLAY_SOUND_FROM_COORD(g_FMMC_STRUCT_ENTITIES.sPlacedProp[EventData.iProp].iSTMSoundIDVar, GET_SOUND_STRING_FROM_SOUND_ID(EventData.iSoundID), EventData.vPlayFromPos, "DLC_Stunt_Race_Stinger_Sounds")
			PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_AUDIO_TRIGGER_ACTIVATED_RACES] - Trigger counter a : ", g_FMMC_STRUCT_ENTITIES.sPlacedProp[EventData.iProp].iSTMTriggerCounter)
			g_FMMC_STRUCT_ENTITIES.sPlacedProp[EventData.iProp].iSTMTriggerCounter++
			PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_AUDIO_TRIGGER_ACTIVATED_RACES] - Trigger counter b : ", g_FMMC_STRUCT_ENTITIES.sPlacedProp[EventData.iProp].iSTMTriggerCounter)
		ENDIF	
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_LEFT_LOCATE_DELAY_RECAPTURE(INT iEventID)

	SCRIPT_EVENT_DATA_FMMC_LEFT_LOCATE_DELAY_RECAPTURE EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_LEFT_LOCATE_DELAY_RECAPTURE
			IF MC_PlayerBD[EventData.iPartToBlock].iteam > -1
			AND EventData.iLoc < FMMC_MAX_GO_TO_LOCATIONS
				RESET_NET_TIMER(tdDisableLocateRecaptureTimer[EventData.iPartToBlock])
				START_NET_TIMER(tdDisableLocateRecaptureTimer[EventData.iPartToBlock])
				
				IF bIsLocalPlayerHost
					IF MC_serverBD_1.inumberOfTeamInArea[EventData.iLoc][MC_PlayerBD[EventData.iPartToBlock].iTeam] >= 1
						MC_serverBD_1.inumberOfTeamInArea[EventData.iLoc][MC_PlayerBD[EventData.iPartToBlock].iTeam]--
					ENDIF
					
					IF iPartIterator > EventData.iPartToBlock
					AND tempNumberOfTeamInArea[EventData.iLoc][MC_PlayerBD[EventData.iPartToBlock].iTeam] >= 1
						tempNumberOfTeamInArea[EventData.iLoc][MC_PlayerBD[EventData.iPartToBlock].iTeam]--
					ENDIF
				ENDIF
			ENDIF
		ENDIF		
	ENDIF
ENDPROC


PROC PROCESS_SCRIPT_EVENT_FMMC_INITIATE_SEAT_SWAP_TEAM_VEHICLE(INT iEventID)
	PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_SCRIPT_EVENT_FMMC_INITIATE_SEAT_SWAP_TEAM_VEHICLE][SWAP_SEAT] - Processing...")
	
	SCRIPT_EVENT_DATA_FMMC_INITIATE_SEAT_SWAP_TEAM_VEHICLE EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_INITIATE_SEAT_SWAP_TEAM_VEHICLE
			
			BOOL bShard = FALSE
			
			INT i
			FOR i = 0 TO MAX_VEHICLE_PARTNERS-1
				IF iLocalPart = EventData.iTeamVehiclePartners[i]
				AND LocalPlayer != EventData.Details.FromPlayerIndex
					PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_SCRIPT_EVENT_FMMC_INITIATE_SEAT_SWAP_TEAM_VEHICLE][SWAP_SEAT] - I am a Passenger of this vehicle which wants to swap/cycle Seats. Setting Bit: PBBOOL3_INITIATED_VEHICLE_SEAT_SWAP")
					SET_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_INITIATED_VEHICLE_SEAT_SWAP)
					bShard = TRUE
				ENDIF
			ENDFOR
										
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitSet3, ciBS3_ENABLE_TEAM_VEHICLE_SWAP_REQUIRES_CONSENT)
				IF bShard
					//SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GENERIC_TEXT, EventData.Details.FromPlayerIndex, DEFAULT, "SHD_CMVSS", "SHD_CONSET", GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_playerBD[iPartToUse].iteam, PlayerToUse), ci_VEH_SEAT_SWAP_CONSENT_TIME)
					PRINT_HELP_WITH_PLAYER_NAME("CONSET_HELP", GET_PLAYER_NAME(EventData.Details.FromPlayerIndex), HUD_COLOUR_WHITE, ci_VEH_SEAT_SWAP_CONSENT_TIME)
				ELIF LocalPlayer = EventData.Details.FromPlayerIndex
					PRINT_HELP("CONSET_HELPb", ci_VEH_SEAT_SWAP_CONSENT_TIME)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_TURF_WAR_SCORED(INT iEventID)
	PRINTLN("[LM][TURF WAR][PROCESS_SCRIPT_EVENT_FMMC_TURF_WAR_SCORED] - Processing...")
	
	SCRIPT_EVENT_DATA_FMMC_TURF_WAR_SCORED EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_TURF_WAR_SCORED

			RESET_NET_TIMER(tdPropResetClearBubble)
			START_NET_TIMER(tdPropResetClearBubble)
			
			IF EventData.bShard
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_TURF_WAR_CUSTOM_SCORING)
				AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciENABLE_TURF_WAR_INCREMENT_SCORE_LOGIC)
					PRINTLN("[LM][TURF WAR][PROCESS_SCRIPT_EVENT_FMMC_TURF_WAR_SCORED] - Don't Create Shard because we are using 1 rule variation")
				ELSE
					PRINTLN("[LM][TURF WAR][PROCESS_SCRIPT_EVENT_FMMC_TURF_WAR_SCORED] - Create Shard")
					CREATE_TURF_WAR_BIG_MESSAGE_PROP_RESET(eventData.iTeamTW, EventData.bTeamDrawTW)
				ENDIF
			ELSE
				PRINTLN("[LM][TURF WAR][PROCESS_SCRIPT_EVENT_FMMC_TURF_WAR_SCORED] - Don't create Shard")
			ENDIF
				
			BOOL bPlayPlayerSound = FALSE
			
			// We scored
			IF EventData.iTeamTW > -1
				PRINTLN("[LM][TURF WAR][PROCESS_SCRIPT_EVENT_FMMC_TURF_WAR_SCORED] - Looking at Winning Team: ", EventData.iTeamTW)
				IF MC_PlayerBD[iPartToUse].iTeam = EventData.iTeamTW
					PRINTLN("[LM][TURF WAR][PROCESS_SCRIPT_EVENT_FMMC_TURF_WAR_SCORED] - Our team Scored.")
					bPlayPlayerSound = TRUE
				ELSE					
					PRINTLN("[LM][TURF WAR][PROCESS_SCRIPT_EVENT_FMMC_TURF_WAR_SCORED] - Our team did not Score.")
				ENDIF			
			ELIF EventData.bTeamDrawTW[MC_PlayerBD[iPartToUse].iTeam]
				PRINTLN("[LM][TURF WAR][PROCESS_SCRIPT_EVENT_FMMC_TURF_WAR_SCORED] - Our team Scored Via Drawing.")
				bPlayPlayerSound = TRUE
			ELSE
				PRINTLN("[LM][TURF WAR][PROCESS_SCRIPT_EVENT_FMMC_TURF_WAR_SCORED] - Our team did not Draw with the rest of them.")
			ENDIF
			
			STRING sSoundSetName
			
			IF bPlayPlayerSound
				sSoundSetName = "DLC_IE_TW_Player_Sounds"
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_LAND_GRAB_SOUNDS)
					sSoundSetName = "DLC_SR_LG_Player_Sounds"
				ENDIF
				PLAY_SOUND_FRONTEND(-1, "Reset_Win", sSoundSetName)
				PRINTLN("[LM][TURF WAR][PROCESS_SCRIPT_EVENT_FMMC_TURF_WAR_SCORED] - Playing Scored sound: Reset_Win, DLC_IE_TW_Player_Sounds")
			ELSE
				sSoundSetName = "DLC_IE_TW_Enemy_Sounds"
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_LAND_GRAB_SOUNDS)
					sSoundSetName = "DLC_SR_LG_Enemy_Sounds"
				ENDIF
				PLAY_SOUND_FRONTEND(-1, "Reset_Win", sSoundSetName)
				PRINTLN("[LM][TURF WAR][PROCESS_SCRIPT_EVENT_FMMC_TURF_WAR_SCORED] - Playing Scored sound: Reset_Win, DLC_IE_TW_Enemy_Sounds")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_OVERTIME_ROUND_CHANGE_TURNS(INT iEventID)
	PRINTLN("[LM][OVERTIME][PROCESS_SCRIPT_EVENT_FMMC_OVERTIME_ROUND_CHANGE_TURNS] - Processing...")
	
	SCRIPT_EVENT_DATA_FMMC_OVERTIME_ROUND_CHANGE_TURNS EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_OVERTIME_ROUND_CHANGE_TURNS
			SET_BIT(iLocalBoolCheck22, LBOOL22_OVERTIME_ROUND_START_SHARD)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_OVERTIME_ROUND_CHANGE(INT iEventID)
	PRINTLN("[LM][OVERTIME][PROCESS_SCRIPT_EVENT_FMMC_OVERTIME_ROUND_CHANGE] - Processing...")
	
	SCRIPT_EVENT_DATA_FMMC_OVERTIME_ROUND_CHANGE EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_OVERTIME_ROUND_CHANGE

			CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_LEAVE_AREA)
			CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_OO_BOUNDS)
		
			STRING sSoundSetName
			PRINTLN("[OVERTIME][PROCESS_SCRIPT_EVENT_FMMC_OVERTIME_ROUND_CHANGE] bStartOfNewRoundCycle: ", EventData.bStartOfNewRoundCycle)
			PRINTLN("[OVERTIME][PROCESS_SCRIPT_EVENT_FMMC_OVERTIME_ROUND_CHANGE] iPart: ", EventData.iPart)
			PRINTLN("[OVERTIME][PROCESS_SCRIPT_EVENT_FMMC_OVERTIME_ROUND_CHANGE] iTeam: ", EventData.iTeam)
			IF NOT EventData.bShardOnly
				IF EventData.iPart = iLocalPart 
					sSoundSetName = "DLC_GR_OT_Player_Sounds"
				ELIF EventData.iTeam = MC_PlayerBD[iLocalPart].iTeam
					sSoundSetName = "DLC_GR_OT_Team_Sounds"
				ELSE
					sSoundSetName = "DLC_GR_OT_Enemy_Sounds"
				ENDIF
			ENDIF
			IF EventData.bStartOfNewRoundCycle
				// (When a new round begins)
				PLAY_SOUND_FRONTEND(-1, "Round_End", sSoundSetName)
				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, "OVT_SC_PLA_NR")
			ELSE
				// (When a new turn is set)
				
				PLAYER_INDEX tempPlayer
				PARTICIPANT_INDEX tempPart
				INT iPartForShard = EventData.iPart
				
				IF EventData.bShardOnly AND EventData.iTeam = MC_PlayerBD[iLocalPart].iTeam
					iPartForShard = iLocalPart
				ENDIF
				
				IF iPartForShard > -1
					tempPart = INT_TO_PARTICIPANTINDEX(iPartForShard)
							
					IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
						tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
						HUD_COLOURS hcPlayerTeamColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(EventData.iTeam, tempPlayer)		
						
						PLAY_SOUND_FRONTEND(-1, "Round_Start", sSoundSetName)	
						IF iPartForShard = iLocalPart 
							INT iEnemyTeam = 0
							IF MC_playerBD[iPartToUse].iteam = iEnemyTeam
								iEnemyTeam = 1
							ENDIF
							INT iDifference = (MC_serverBD.iTeamScore[MC_playerBD[iPartToUse].iteam] - MC_serverBD.iTeamScore[iEnemyTeam])
							IF iDifference > -1
								TEXT_LABEL_15 tl15 = ""
								IF IS_THIS_CURRENTLY_OVERTIME_RUMBLE()
									tl15 = "OVT_SC_PLA_NTRb"
								ELSE
									tl15 = "OVT_SC_PLA_NTb"
								ENDIF
								IF iDifference = 0
									tl15 = "OVT_SC_PLA_TIE"
								ELIF iDifference = 1
									IF IS_THIS_CURRENTLY_OVERTIME_RUMBLE()
										tl15 = "OVT_SC_PLA_NTRd"
									ELSE
										tl15 = "OVT_SC_PLA_NTb1"
									ENDIF
								ENDIF
								SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_GENERIC_TEXT, iDifference, tl15, "OVT_SC_PLA_NT", hcPlayerTeamColour)
							ELSE
								TEXT_LABEL_15 tl15 = ""
								IF IS_THIS_CURRENTLY_OVERTIME_RUMBLE()
									tl15 = "OVT_SC_PLA_NTRc"
								ELSE
									iDifference += 1
									tl15 = "OVT_SC_PLA_NTc"
								ENDIF
								
								IF iDifference = -1
									IF IS_THIS_CURRENTLY_OVERTIME_RUMBLE()
										tl15 = "OVT_SC_PLA_NTRe"
									ELSE
										tl15 = "OVT_SC_PLA_NTc1"
									ENDIF
								ENDIF
								
								SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_GENERIC_TEXT, (iDifference * -1), tl15, "OVT_SC_PLA_NT", hcPlayerTeamColour, (BIG_MESSAGE_DISPLAY_TIME + 2000))
							ENDIF
						ELSE
							SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GENERIC_TEXT, tempPlayer, 0, "OVT_SC_PLA_NTa", "OVT_SC_PLA_NT", hcPlayerTeamColour)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_OVERTIME_RESET_TURNS_AND_SCORE(INT iEventID)
	
	SCRIPT_EVENT_DATA_FMMC_OVERTIME_RESET_TURNS_AND_SCORE EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_OVERTIME_RESET_TURNS_AND_SCORE
			IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet3, PBBOOL3_TAKEN_A_TURN)
				PRINTLN("[JS] [TURNS] - PROCESS_SCRIPT_EVENT_FMMC_OVERTIME_RESET_TURNS_AND_SCORE - Processing...")
				CLEAR_BIT(MC_PlayerBD[iLocalPart].iClientBitSet3, PBBOOL3_TAKEN_A_TURN)
				MC_PlayerBD[iLocalPart].iScoreOffset = MC_PlayerBD[iLocalPart].iPlayerScore
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_OVERTIME_LANDING(INT iEventID)
	PRINTLN("[LM][OVERTIME][PROCESS_SCRIPT_EVENT_FMMC_OVERTIME_LANDING] - Processing...")
	
	SCRIPT_EVENT_DATA_FMMC_OVERTIME_LANDING EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_OVERTIME_LANDING
			STRING sSoundSetName
			
			IF EventData.iPart = iLocalPart 
				sSoundSetName = "DLC_GR_OT_Player_Sounds"
			ELIF EventData.iTeam= MC_PlayerBD[iLocalPart].iTeam
				sSoundSetName = "DLC_GR_OT_Team_Sounds"
			ELSE
				sSoundSetName = "DLC_GR_OT_Enemy_Sounds"
			ENDIF
			
			IF EventData.bSuccessful
				PLAY_SOUND_FRONTEND(-1, "Successful_Landing", sSoundSetName)
			ELSE
				PLAY_SOUND_FRONTEND(-1, "Failed_Landing", sSoundSetName)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_OVERTIME_VEHICLE_DESTROYED(INT iEventID)
	PRINTLN("[LM][OVERTIME][PROCESS_SCRIPT_EVENT_FMMC_OVERTIME_VEHICLE_DESTROYED] - Processing...")
	
	SCRIPT_EVENT_DATA_FMMC_OVERTIME_VEHICLE_DESTROYED EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_OVERTIME_VEHICLE_DESTROYED
			STRING sSoundSetName
			
			IF EventData.iPart = iLocalPart 
				sSoundSetName = "DLC_GR_OT_Player_Sounds"
			ELIF EventData.iTeam= MC_PlayerBD[iLocalPart].iTeam
				sSoundSetName = "DLC_GR_OT_Team_Sounds"
			ELSE
				sSoundSetName = "DLC_GR_OT_Enemy_Sounds"
			ENDIF
			
			PLAY_SOUND_FRONTEND(-1, "Vehicle_Destroyed", sSoundSetName)
		ENDIF
	ENDIF
ENDPROC	
PROC PROCESS_SCRIPT_EVENT_FMMC_SUMO_ZONE_TIME_EXTEND_SHARD(INT iEventID)
	PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_SUMO_ZONE_TIME_EXTEND_SHARD]")
	
	SCRIPT_EVENT_DATA_FMMC_SUMO_ZONE_TIME_EXTEND_SHARD EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_SUMO_ZONE_TIME_EXTEND_SHARD
			SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, "SUMOZONE_TESA", "SUMOZONE_TESB")
		ENDIF
	ENDIF
ENDPROC
PROC PROCESS_SCRIPT_EVENT_FMMC_KILLSTREAK_POWERUP_ACTIVATED_FREEZE_POS(INT iEventID)
	PRINTLN("[LM][TURF WAR][PROCESS_SCRIPT_EVENT_FMMC_KILLSTREAK_POWERUP_ACTIVATED_FREEZE_POS] - Broadcasting...")
	
	SCRIPT_EVENT_DATA_FMMC_KILLSTREAK_POWERUP_ACTIVATED_FREEZE_POS EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_KILLSTREAK_POWERUP_ACTIVATED_FREEZE_POS
			INT i = 0
			FOR i = 0 TO FMMC_MAX_TEAMS-1
				IF MC_PlayerBD[iLocalPart].iteam = i
				AND EventData.bTeamAffected[i]
					SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_GENERIC_TEXT, GET_TURFWARS_COLOUR_TEAM_TEXT(EventData.iTeamActivated), "KLSTRK_FRZ_AS1", "KLSTRK_FRZ_A")
					SET_BIT(MC_PlayerBD[iLocalPart].iKSPowerupActiveBS, ci_KILLSTREAK_POWERUP_FREEZE_POS)
				ENDIF
			ENDFOR
			
			IF MC_PlayerBD[iLocalPart].iteam = EventData.iTeamActivated
				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, "KLSTRK_FRZ_A", "KLSTRK_FRZ_AS2")
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_KILLSTREAK_POWERUP_ACTIVATED_TURF_TRADE(INT iEventID)
	PRINTLN("[LM][TURF WAR][PROCESS_SCRIPT_EVENT_FMMC_KILLSTREAK_POWERUP_ACTIVATED_TURF_TRADE] - Broadcasting...")
	
	SCRIPT_EVENT_DATA_FMMC_KILLSTREAK_POWERUP_ACTIVATED_TURF_TRADE EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_KILLSTREAK_POWERUP_ACTIVATED_TURF_TRADE
			
			// Calculate.
			INT iTeamChosen
			INT i = 0
			FOR i = 0 TO 50
				iTeamChosen = GET_RANDOM_INT_IN_RANGE(0, FMMC_MAX_TEAMS)
				
				// Make sure we don't choose ourselves.
				IF iTeamChosen != EventData.iTeamActivated
				AND IS_TEAM_ACTIVE(iTeamChosen)
					PRINTLN("[LM][TURF WAR][PROCESS_SCRIPT_EVENT_FMMC_KILLSTREAK_POWERUP_ACTIVATED_TURF_TRADE] - iTeamChosen: ", iTeamChosen)
					BREAKLOOP
				ENDIF
			ENDFOR
			
			// Swap the Turf
			IF bIsLocalPlayerHost
				INT iProp				
				FOR iProp = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfProps-1
					IF DOES_THIS_TEAM_OWN_CLAIMED_PROP(EventData.iTeamActivated, iProp)
						PRINTLN("[LM][TURF WAR][PROCESS_SCRIPT_EVENT_FMMC_KILLSTREAK_POWERUP_ACTIVATED_TURF_TRADE] - Team: ", EventData.iTeamActivated, " Owns this prop. Broadcast claim for Team: ", iTeamChosen)
						BROADCAST_FMMC_CLAIM_PROP(iProp, iTeamChosen, -1, GET_NETWORK_TIME())
					ELIF DOES_THIS_TEAM_OWN_CLAIMED_PROP(iTeamChosen, iProp)
						PRINTLN("[LM][TURF WAR][PROCESS_SCRIPT_EVENT_FMMC_KILLSTREAK_POWERUP_ACTIVATED_TURF_TRADE] - Team: ", iTeamChosen, " Owns this prop. Broadcast claim for Team: ", EventData.iTeamActivated)
						BROADCAST_FMMC_CLAIM_PROP(iProp, EventData.iTeamActivated, -1, GET_NETWORK_TIME())
					ENDIF
				ENDFOR
			ENDIF
			
			PRINTLN("[LM][TURF WAR][PROCESS_SCRIPT_EVENT_FMMC_KILLSTREAK_POWERUP_ACTIVATED_TURF_TRADE] - TeamChosen: ", iTeamChosen, " TeamActivated: ", EventData.iTeamActivated)
			PRINTLN("[LM][TURF WAR][PROCESS_SCRIPT_EVENT_FMMC_KILLSTREAK_POWERUP_ACTIVATED_TURF_TRADE] - TeamChosen: ", GET_TURFWARS_COLOUR_TEAM_TEXT(iTeamChosen), " TeamActivated: ", GET_TURFWARS_COLOUR_TEAM_TEXT(EventData.iTeamActivated))
			
			// Play the Shards
			IF MC_PlayerBD[iLocalPart].iteam = EventData.iTeamActivated
				SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_GENERIC_TEXT, GET_TURFWARS_COLOUR_TEAM_TEXT(iTeamChosen), "KLSTRK_TRD_A1", "KLSTRK_TRD_A")
			ELIF MC_PlayerBD[iLocalPart].iteam = iTeamChosen
				SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_GENERIC_TEXT, GET_TURFWARS_COLOUR_TEAM_TEXT(EventData.iTeamActivated), "KLSTRK_TRD_A2", "KLSTRK_TRD_A")
			ELSE
				SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_GENERIC_TEXT, GET_TURFWARS_COLOUR_TEAM_TEXT(EventData.iTeamActivated), "KLSTRK_TRD_A3", "KLSTRK_TRD_A", DEFAULT, DEFAULT, DEFAULT, GET_TURFWARS_COLOUR_TEAM_TEXT(iTeamChosen))
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_MANUAL_RESPAWN_TO_PASSENGERS(INT iEventID)
	PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_MANUAL_RESPAWN_TO_PASSENGERS] - Broadcasting...")
	
	SCRIPT_EVENT_DATA_FMMC_MANUAL_RESPAWN_TO_PASSENGERS EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_MANUAL_RESPAWN_TO_PASSENGERS
			IF EventData.iEventLocalPartDriver = iLocalPart
				PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_MANUAL_RESPAWN_TO_PASSENGERS] - I am the Driver")
			ELSE
				VEHICLE_INDEX vehIndex = GET_VEHICLE_PED_IS_IN(localPlayerPed)

				IF EventData.vehEventIndex = VEH_TO_NET(vehIndex)
					SET_BIT(iLocalBoolCheck21, LBOOL21_MY_DRIVER_IS_MANUALLY_RESPAWNING)
					PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_MANUAL_RESPAWN_TO_PASSENGERS] - My Driver is Manually Respawning bitset, set.")
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_PP_SET_TEAM_INTRO_ROOT(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_PP_TEAM_INTRO_ROOT EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_PP_TEAM_INTRO_ROOT
			PRINTLN("[PP-ANNOUNCER] PROCESS_SCRIPT_EVENT_PP_SET_TEAM_INTRO_ROOT - EventData.iRootNum = ", EventData.iRootNum)
			tl15PPTeamIntroRoot = GET_STRING_FOR_PP_ANNOUNCER_ROOT(EventData.iRootNum)
			PRINTLN("[PP-ANNOUNCER] PROCESS_SCRIPT_EVENT_PP_SET_TEAM_INTRO_ROOT - tl15PPTeamIntroRoot = ", tl15PPTeamIntroRoot)
		ENDIF
	ENDIF	
ENDPROC

PROC PROCESS_SCRIPT_EVENT_PP_SET_PRE_INTRO_ROOT(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_PP_TEAM_INTRO_ROOT EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_PP_PRE_INTRO_ROOT
			PRINTLN("[PP-ANNOUNCER] PROCESS_SCRIPT_EVENT_PP_SET_PRE_INTRO_ROOT - EventData.iRootNum = ", EventData.iRootNum)
			tl15PPPreIntroRoot = GET_STRING_FOR_PP_ANNOUNCER_ROOT(EventData.iRootNum)
			PRINTLN("[PP-ANNOUNCER] PROCESS_SCRIPT_EVENT_PP_SET_PRE_INTRO_ROOT - tl15PPPreIntroRoot = ", tl15PPPreIntroRoot)
		ENDIF
	ENDIF	
ENDPROC

PROC PROCESS_SCRIPT_EVENT_PP_SET_SUDDDEATH_ROOT(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_PP_TEAM_INTRO_ROOT EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_PP_SUDDDEATH_ROOT
			PRINTLN("[PP-ANNOUNCER] PROCESS_SCRIPT_EVENT_PP_SET_SUDDDEATH_ROOT - EventData.iRootNum = ", EventData.iRootNum)
			tl15PPSuddendeathRoot = GET_STRING_FOR_PP_ANNOUNCER_ROOT(EventData.iRootNum)
			PRINTLN("[PP-ANNOUNCER] PROCESS_SCRIPT_EVENT_PP_SET_SUDDDEATH_ROOT - tl15PPSuddendeathRoot = ", tl15PPSuddendeathRoot)
		ENDIF
	ENDIF	
ENDPROC

PROC PROCESS_SCRIPT_EVENT_PP_SET_WINNER_ROOT(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_PP_TEAM_INTRO_ROOT EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_PP_WINNER_ROOT
			PRINTLN("[PP-ANNOUNCER] PROCESS_SCRIPT_EVENT_PP_SET_WINNER_ROOT - EventData.iRootNum = ", EventData.iRootNum)
			tl15PPWinnerRoot = GET_STRING_FOR_PP_ANNOUNCER_ROOT(EventData.iRootNum)
			PRINTLN("[PP-ANNOUNCER] PROCESS_SCRIPT_EVENT_PP_SET_WINNER_ROOT - tl15PPWinnerRoot = ", tl15PPWinnerRoot)
		ENDIF
	ENDIF	
ENDPROC

PROC PROCESS_SCRIPT_EVENT_PP_SET_EXIT_ROOT(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_PP_TEAM_INTRO_ROOT EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_PP_EXIT_ROOT
			PRINTLN("[PP-ANNOUNCER] PROCESS_SCRIPT_EVENT_PP_SET_EXIT_ROOT - EventData.iRootNum = ", EventData.iRootNum)
			tl15PPExitRoot = GET_STRING_FOR_PP_ANNOUNCER_ROOT(EventData.iRootNum)
			PRINTLN("[PP-ANNOUNCER] PROCESS_SCRIPT_EVENT_PP_SET_EXIT_ROOT - tl15PPExitRoot = ", tl15PPExitRoot)
		ENDIF
	ENDIF	
ENDPROC

#IF IS_DEBUG_BUILD
PROC PROCESS_SCRIPT_EVENT_PP_MANUALLY_SET_MUSIC(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_PP_MANUAL_MUSIC EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_PP_MANUAL_MUSIC_CHANGE
			PRINTLN("PROCESS_SCRIPT_EVENT_PP_MANUALLY_SET_MUSIC - EventData.iMusic = ", EventData.iMusic)
//			MC_PlayerBD[iPartToUse].iMusicState = MUSIC_ACTION
//			g_FMMC_STRUCT.iAudioScore = EventData.iMusic
//			SET_MISSION_MUSIC(g_FMMC_STRUCT.iAudioScore)
//			TRIGGER_MUSIC_EVENT("MP_MC_ACTION")
			
			CLEAR_BIT(iLocalBoolCheck3,LBOOL3_MUSIC_PLAYING)
			CLEAR_BIT(iLocalBoolCheck,LBOOL_MUSIC_CHANGED)
			MC_PlayerBD[iPartToUse].iMusicState = MUSIC_ACTION
			g_FMMC_STRUCT.iAudioScore = EventData.iMusic
			
		ENDIF
	ENDIF	

ENDPROC
#ENDIF

PROC PROCESS_SCRIPT_EVENT_RUG_PLAY_SOUND(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_RUG_PROCESS_PLAY_SOUND EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_RUGBY_PLAY_SOUND
			PLAY_RUGBY_SOUND(EventData.soundToPlay, EventData.Details.FromPlayerIndex)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_DISTANCE_GAINED_TICKER(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_DISTANCE_GAINED EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_DISTANCE_GAINED
			IF EventData.Details.FromPlayerIndex != PLAYER_ID()
				HUD_COLOURS hcPlayerTeamColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(EventData.iTeam, EventData.Details.FromPlayerIndex)
				IF SHOULD_USE_METRIC_MEASUREMENTS()
					IF EventData.bGainedDistance
						PRINT_TICKER_WITH_PLAYER_NAME_WITH_HUD_COLOUR_AND_INT("IBI_TICK1", EventData.Details.FromPlayerIndex, hcPlayerTeamColour, EventData.iDistanceMeters)
					ELSE
						PRINT_TICKER_WITH_PLAYER_NAME_WITH_HUD_COLOUR_AND_INT("IBI_TICK2", EventData.Details.FromPlayerIndex, hcPlayerTeamColour, EventData.iDistanceMeters)
					ENDIF
				ELSE
					IF EventData.bGainedDistance
						PRINT_TICKER_WITH_PLAYER_NAME_WITH_HUD_COLOUR_AND_INT("IBI_TICK5", EventData.Details.FromPlayerIndex, hcPlayerTeamColour, EventData.iDistanceFeet)
					ELSE
						PRINT_TICKER_WITH_PLAYER_NAME_WITH_HUD_COLOUR_AND_INT("IBI_TICK6", EventData.Details.FromPlayerIndex, hcPlayerTeamColour, EventData.iDistanceFeet)
					ENDIF
				ENDIF
			ELSE
				IF SHOULD_USE_METRIC_MEASUREMENTS()
					IF EventData.bGainedDistance
						PRINT_TICKER_WITH_INT("IBI_TICK3", EventData.iDistanceMeters)
					ELSE
						PRINT_TICKER_WITH_INT("IBI_TICK4", EventData.iDistanceMeters)
					ENDIF
				ELSE
					IF EventData.bGainedDistance
						PRINT_TICKER_WITH_INT("IBI_TICK7", EventData.iDistanceFeet)
					ELSE
						PRINT_TICKER_WITH_INT("IBI_TICK8", EventData.iDistanceFeet)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_IAO_PLAY_SOUND(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_IN_AND_OUT_SFX_DETAILS EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_IAO_SFX_PLAY
			PLAY_IN_AND_OUT_SOUND(EventData.iActionType, EventData.iTeam, EventData.Details.FromPlayerIndex)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_TRADING_PLACES_TRANSFORM_SFX(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_TRADING_PLACES_TRANSFORM_SFX EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_TP_TRANSFORM_SFX
			//PLAY_SOUND_FRONTEND(-1, "Transform_Remote_Player", "DLC_Exec_TP_SoundSet", FALSE)
			IF NOT IS_PED_INJURED(GET_PLAYER_PED(EventData.Details.FromPlayerIndex))
				PLAY_SOUND_FROM_ENTITY(-1, "Transform_Remote_Player", GET_PLAYER_PED(EventData.Details.FromPlayerIndex), "DLC_Exec_TP_SoundSet")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_SUDDEN_DEATH_REASON(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_SUDDEN_DEATH_REASON EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_SUDDEN_DEATH_REASON
		
			STRING sMsgTitle
			STRING tlTeamName = TEXT_LABEL_TO_STRING(g_sMission_TeamName[ EventData.iTeam ])
			HUD_COLOURS hudColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(EventData.iTeam, PlayerToUse)

			IF EventData.iTeam = MC_playerBD[iPartToUse].iteam
				sMsgTitle = "IBI_ST_WIN"
			ELSE
				sMsgTitle = "IBI_ST_LOSE"
			ENDIF
			
			IF IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
			AND NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_WASTED)
				PRINTLN("[KH] PROCESS_SCRIPT_EVENT_SUDDEN_DEATH_REASON - big message is already being displayed, clearing before displaying end reason shard")
				CLEAR_ALL_BIG_MESSAGES()
			ENDIF

			SWITCH(EventData.iReason)
				CASE ciSUDDEN_DEATH_REASON_KILLS		
					SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GENERIC_TEXT, sMsgTitle, "IBI_S_KILLS", tlTeamName, hudColour)
				BREAK
				
				CASE ciSUDDEN_DEATH_REASON_POINTS
					SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GENERIC_TEXT, sMsgTitle, "IBI_S_POINTS", tlTeamName, hudColour)
				BREAK
				
				CASE ciSUDDEN_DEATH_REASON_DEATHS
					SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GENERIC_TEXT, sMsgTitle, "IBI_S_DEATHS", tlTeamName, hudColour)
				BREAK
				
				CASE ciSUDDEN_DEATH_REASON_HSHOTS
					SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GENERIC_TEXT, sMsgTitle, "IBI_S_HSHOTS", tlTeamName, hudColour)
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_GRAB_CASH(INT iEventID)

	//handle updating server cash grab take value
	IF bIsLocalPlayerHost
		SCRIPT_EVENT_DATA_FMMC_CASH_GRAB Event
		
		PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_GRAB_CASH - has been received for host.")

		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
			INT iCashType = FMMC_CASH_ORNATE_BANK
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[Event.iobjIndex].iCashAmount != 0
				iCashType = g_FMMC_STRUCT.iCashReward
			ENDIF
			IF MC_ServerBD.iCashGrabTotalTake + Event.iCashGrabbed > GET_HEIST_MODIFIED_TAKE_FOR_CASH_GRAB(iCashType)
				MC_ServerBD.iCashGrabTotalTake = GET_HEIST_MODIFIED_TAKE_FOR_CASH_GRAB(iCashType)
				PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_GRAB_CASH - Attempting to increase cash over the max (1) - ", GET_HEIST_MODIFIED_TAKE_FOR_CASH_GRAB(iCashType))
			ELSE
				MC_ServerBD.iCashGrabTotalTake += Event.iCashGrabbed
			ENDIF
			IF MC_ServerBD.iCashGrabTotal[Event.iobjIndex] + Event.iCashGrabbed > GET_HEIST_MODIFIED_TAKE_FOR_CASH_GRAB(iCashType)
				MC_ServerBD.iCashGrabTotal[Event.iobjIndex] = GET_HEIST_MODIFIED_TAKE_FOR_CASH_GRAB(iCashType)
				PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_GRAB_CASH - Attempting to increase cash over the max (2) - ", GET_HEIST_MODIFIED_TAKE_FOR_CASH_GRAB(iCashType))
			ELSE
				MC_ServerBD.iCashGrabTotal[Event.iobjIndex] += Event.iCashGrabbed
			ENDIF
			IF ( Event.bIsBonusCash )
				MC_ServerBD.iCashGrabBonusPilesGrabbed[Event.iobjIndex] += 1
			ENDIF
			
			IF Event.bFinalCashPile
				SET_BIT(MC_serverBD_1.iOffRuleMinigameCompleted, Event.iobjIndex)
				PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_GRAB_CASH - Setting bit in iOffRuleMinigameCompleted cash pile: ", Event.iobjIndex)
			ENDIF
			
			IF Event.bDailyCashVault
				IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE()
					MC_ServerBD_1.iDailyCashRoomTake += Event.iCashGrabbed
					PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_GRAB_CASH - Incremented MC_ServerBD_1.iDailyCashRoomTake to: ", MC_ServerBD_1.iDailyCashRoomTake)
				ENDIF
				IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_WeaponPickupTracking)
					MC_serverBD_1.sLEGACYMissionContinuityVars.iDailyVaultCashGrabbed++
					PRINTLN("[RCC MISSION][CONTINUITY] - PROCESS_SCRIPT_EVENT_GRAB_CASH - Incremented MC_serverBD_1.sLEGACYMissionContinuityVars.iDailyVaultCashGrabbed to : ", MC_serverBD_1.sLEGACYMissionContinuityVars.iDailyVaultCashGrabbed)
				ENDIF
			ELSE
				IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE()
					MC_ServerBD_1.iVaultTake += Event.iCashGrabbed
				ENDIF
			ENDIF

			PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_GRAB_CASH - grabbed cash pile ", Event.iCurrentPile)
			IF ( Event.bIsBonusCash )
				PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_GRAB_CASH - bonus pile number ", MC_ServerBD.iCashGrabBonusPilesGrabbed[Event.iobjIndex])
			ELSE
				PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_GRAB_CASH - not a bonus pile ", Event.iCurrentPile)
			ENDIF
			PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_GRAB_CASH - adding cash ", Event.iCashGrabbed, " for object ", Event.iobjIndex)
			PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_GRAB_CASH - new total ", MC_ServerBD.iCashGrabTotal[Event.iobjIndex], " for object ", Event.iobjIndex)
			PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_GRAB_CASH - cash grab total take for mission ", MC_ServerBD.iCashGrabTotalTake)
		ENDIF
	ENDIF
	
	
	//handle playing sound each time take increases	
	SCRIPT_EVENT_DATA_FMMC_CASH_GRAB Event
	
	PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_GRAB_CASH - has been received for all players.")
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
	
		IF Event.Details.FromPlayerIndex = LocalPlayer
			//sounds played on the player who just grabbed some cash
			IF Event.bFinalCashPile
				//when all cash in the cash grab is collected
				PLAY_SOUND_FRONTEND(-1, "LOCAL_PLYR_CASH_COUNTER_COMPLETE", "DLC_HEISTS_GENERAL_FRONTEND_SOUNDS", FALSE)
				PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_GRAB_CASH - Playing sound LOCAL_PLYR_CASH_COUNTER_COMPLETE.")
			ELSE
				//when any cash pile is collected
				PLAY_SOUND_FRONTEND(-1, "LOCAL_PLYR_CASH_COUNTER_INCREASE", "DLC_HEISTS_GENERAL_FRONTEND_SOUNDS", FALSE)
				PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_GRAB_CASH - Playing sound LOCAL_PLYR_CASH_COUNTER_INCREASE.")
			ENDIF
			IF IS_THIS_MISSION_A_CASINO_HEIST_MISSION(g_FMMC_STRUCT.irootContentIDHash)
			AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[Event.iobjIndex].iObjectBitSetThree, cibsOBJ3_UseDailyCashGrabAmount)
				INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_DAICASHCRAB, Event.iCashGrabbed)
				PRINTLN("Incrementing MP_AWARD_DAICASHCRAB by, ", Event.iCashGrabbed, " to ", GET_MP_INT_CHARACTER_AWARD(MP_AWARD_DAICASHCRAB))
			ENDIF	
		ELSE
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_OnlyPlayLocalCashGrabSounds)
				//sounds played on any other player
				IF Event.bFinalCashPile
					//when all cash in the cash grab is collected
					PLAY_SOUND_FRONTEND(-1, "REMOTE_PLYR_CASH_COUNTER_COMPLETE", "DLC_HEISTS_GENERAL_FRONTEND_SOUNDS", FALSE)
					PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_GRAB_CASH - Playing sound REMOTE_PLYR_CASH_COUNTER_COMPLETE.")
				ELSE
					//when any cash pile is collected
					PLAY_SOUND_FRONTEND(-1, "REMOTE_PLYR_CASH_COUNTER_INCREASE", "DLC_HEISTS_GENERAL_FRONTEND_SOUNDS", FALSE)
					PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_GRAB_CASH - Playing sound REMOTE_PLYR_CASH_COUNTER_INCREASE.")
				ENDIF
			ENDIF
		ENDIF
	
		//no longer playing this sound, replaced with new ones, B*2216312
		//play ticker sound for take counter incrementing, B*2084562
		//PLAY_SOUND_FRONTEND(-1, "ROBBERY_MONEY_TOTAL", "HUD_FRONTEND_CUSTOM_SOUNDSET", FALSE)
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_GRAB_CASH_DROP_CASH(INT iEventID)
	
	SCRIPT_EVENT_DATA_FMMC_CASH_GRAB_CASH_DROPPED Event

	INT iCashDropped
	INT iCash = FMMC_CASH_ORNATE_BANK
	
	IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
		iCash = FMMC_CASH_CASINO_HEIST
	ENDIF
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
		//get how much cash to drop
		IF iCash = FMMC_CASH_CASINO_HEIST
			INT iMinDrop, iMaxDrop
			iMinDrop = PICK_INT(Event.bHitInTheBag, g_sMPTunables.iCH_money_grab_cash_drop_threshold_low_bag, g_sMPTunables.iCH_money_grab_cash_drop_threshold_low_body)
			iMaxDrop = PICK_INT(Event.bHitInTheBag, g_sMPTunables.iCH_money_grab_cash_drop_threshold_high_bag, g_sMPTunables.iCH_money_grab_cash_drop_threshold_high_body)
			iCashDropped = GET_RANDOM_INT_IN_RANGE(iMinDrop, iMaxDrop)
		ELSE
			iCashDropped = GET_RANDOM_INT_IN_RANGE(g_sMPTunables.heist_money_grab_cash_drop_threshold_low, g_sMPTunables.heist_money_grab_cash_drop_threshold_high)
			iCashDropped = FLOOR(TO_FLOAT(iCashDropped) * Event.fScale)
		ENDIF
	ENDIF

	IF bIsLocalPlayerHost
		
		PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_GRAB_CASH_DROP_CASH - has been received.")

		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
			//update server data
			MC_ServerBD.iCashGrabTotalDrop += iCashDropped
			
			IF SVM_FLOW_IS_CURRENT_MISSION_SVM_MISSION()
				MC_ServerBD.iCashGrabTotalTake = CLAMP_INT(MC_ServerBD.iCashGrabTotalTake - iCashDropped,
														   0,
														   MC_ServerBD.iCashGrabTotalTake)
			ELSE
				MC_ServerBD.iCashGrabTotalTake = CLAMP_INT(MC_ServerBD.iCashGrabTotalTake - iCashDropped,
														   GET_HEIST_MINIMUM_TAKE_FOR_CASH_DROP(iCash),
														   MC_ServerBD.iCashGrabTotalTake)
			ENDIF
			
			PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_GRAB_CASH_DROP_CASH - dropping cash ", iCashDropped)
			PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_GRAB_CASH_DROP_CASH - total drop ", MC_ServerBD.iCashGrabTotalDrop)
			PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_GRAB_CASH_DROP_CASH - cash grab total take after this drop ", MC_ServerBD.iCashGrabTotalTake)
			PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_GRAB_CASH_DROP_CASH - cash grab minimum take for this heist ", GET_HEIST_MINIMUM_TAKE_FOR_CASH_DROP(iCash))
		ENDIF
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_SVM_PHANTOM2(g_FMMC_STRUCT.iRootContentIDHash)
		PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_GRAB_CASH_DROP_CASH - Has been received on IE mission. Player: ", NATIVE_TO_INT(Event.Details.FromPlayerIndex))
		//Take cash away from your local number
		IF Event.Details.FromPlayerIndex = LocalPlayer
			g_TransitionSessionNonResetVars.iLocalPlayerCashGrabbed -= iCashDropped
			PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_GRAB_CASH_DROP_CASH - g_TransitionSessionNonResetVars.iLocalPlayerCashGrabbed: ", g_TransitionSessionNonResetVars.iLocalPlayerCashGrabbed)
		ENDIF
	ENDIF
	
	bDrawCashGrabTakeRed 		= TRUE
	iDrawCashGrabTakeRedTime	= 0
ENDPROC

PROC PROCESS_SCRIPT_EVENT_GRAB_CASH_DISPLAY_TAKE(INT iEventID)

	SCRIPT_EVENT_DATA_FMMC_JUST_EVENT Event
	
	PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_GRAB_CASH_DISPLAY_TAKE - has been received.")

	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
	
		g_TransitionSessionNonResetVars.bDisplayCashGrabTake = TRUE
		
		PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_DISPLAY_CASH_GRAB_TAKE - setting global g_TransitionSessionNonResetVars.bDisplayCashGrabTake to ", g_TransitionSessionNonResetVars.bDisplayCashGrabTake)
	ENDIF

ENDPROC


PROC PROCESS_SCRIPT_EVENT_FMMC_SAFETY_DEPOSIT_BOX_LOOTED(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_SAFETY_DEPOSIT_BOX_LOOTED Event

	INT iCashDropped

	IF bIsLocalPlayerHost
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
			
			SET_BIT(MC_ServerBD_2.iSafetyDepositBoxDrilledBS[Event.iBox], Event.iCabinet)
			
			SET_RANDOM_SEED(NATIVE_TO_INT(GET_NETWORK_TIME()))
			INT iRewardTier = GET_RANDOM_INT_IN_RANGE(1, 1000)		
			IF iRewardTier < 350
				iCashDropped = g_sMPTunables.iCH_VAULT_SAFETY_DEPOSIT_BOX_VALUE_35Percent_1
			ELIF iRewardTier < 700
				iCashDropped = g_sMPTunables.iCH_VAULT_SAFETY_DEPOSIT_BOX_VALUE_35Percent_2
			ELIF iRewardTier < 800
				iCashDropped = g_sMPTunables.iCH_VAULT_SAFETY_DEPOSIT_BOX_VALUE_10Percent_1
			ELIF iRewardTier < 900
				iCashDropped = g_sMPTunables.iCH_VAULT_SAFETY_DEPOSIT_BOX_VALUE_10Percent_2
			ELIF iRewardTier < 950
				iCashDropped = g_sMPTunables.iCH_VAULT_SAFETY_DEPOSIT_BOX_VALUE_5Percent_1
			ELIF iRewardTier < 1000
				iCashDropped = g_sMPTunables.iCH_VAULT_SAFETY_DEPOSIT_BOX_VALUE_5Percent_2
			ENDIF
			
			PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_FMMC_SAFETY_DEPOSIT_BOX_LOOTED - has been received.")
			PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_FMMC_SAFETY_DEPOSIT_BOX_LOOTED - iCashDropped: ", iCashDropped, " Sent from iPart: ", Event.iPart, " Cabinet: ", Event.iCabinet, " iBox: ", Event.iBox)
			
			MC_ServerBD.iCashGrabTotalTake += iCashDropped		
			IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE()
				MC_ServerBD_1.iDepositBoxTake += iCashDropped	
			ENDIF
		ENDIF
	ENDIF	
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_OCCUPY_SAFETY_DEPOSIT_BOX_CABINET(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_OCCUPY_SAFETY_DEPOSIT_BOX_CABINET Event
	IF bIsLocalPlayerHost
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
			
			IF Event.bUsing
				SET_BIT(MC_ServerBD_2.iSafetyDepositCabinetOccupiedUsedBS, Event.iCabinet)
			ELSE
				CLEAR_BIT(MC_ServerBD_2.iSafetyDepositCabinetOccupiedUsedBS, Event.iCabinet)
			ENDIF
			
			PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_FMMC_OCCUPY_SAFETY_DEPOSIT_BOX_CABINET - has been received.")
			PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_FMMC_OCCUPY_SAFETY_DEPOSIT_BOX_CABINET - iPart: ", Event.iPart, " iCabinet: ", Event.iCabinet, " bUsing: ", Event.bUsing)
		ENDIF
	ENDIF	
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_PAINTING_STOLEN(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_PAINTING_STOLEN Event

	INT iCashDropped
	INT iTotalPaintingValue
	INT iPainting1Value
	INT iPainting2Value
	INT iPainting3Value
	INT iPainting4Value
	INT iPainting5Value
	INT iPainting6Value
	
	IF bIsLocalPlayerHost
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
			
			iTotalPaintingValue = ROUND(GET_CASINO_HEIST_TARGET_VALUE(CASINO_HEIST_TARGET_TYPE__ART) * GET_CASINO_HEIST_TARGET_VALUE_DIFFICULTY_MODIFIER(g_FMMC_STRUCT.iDifficulity))
			iPainting1Value = (iTotalPaintingValue / 100) * 18		// Painting index 0 = 18% of overall painting take
			iPainting2Value = (iTotalPaintingValue / 100) * 10		// Painting index 1 = 10% of overall painting take
			iPainting3Value = (iTotalPaintingValue / 100) * 22		// Painting index 2 = 22% of overall painting take
			iPainting4Value = (iTotalPaintingValue / 100) * 15		// Painting index 3 = 15% of overall painting take
			iPainting5Value = (iTotalPaintingValue / 100) * 17		// Painting index 4 = 17% of overall painting take
			iPainting6Value = iTotalPaintingValue - iPainting1Value - iPainting2Value - iPainting3Value - iPainting4Value - iPainting5Value		// [ML] Calculated differently in case of any rounding errors
			PRINTLN("[ML] Painting values - 1:", iPainting1Value, "  iPainting2Value", iPainting2Value, "  iPainting3Value", iPainting3Value, "  iPainting4Value", iPainting4Value, "  iPainting5Value", iPainting5Value, "  iPainting6Value", iPainting6Value)
			SWITCH Event.iPainting
				CASE 0
					iCashDropped = iPainting1Value
				BREAK
				CASE 1
					iCashDropped = iPainting2Value
				BREAK
				CASE 2
					iCashDropped = iPainting3Value
				BREAK
				CASE 3
					iCashDropped = iPainting4Value
				BREAK
				CASE 4
					iCashDropped = iPainting5Value
				BREAK
				CASE 5
					iCashDropped = iPainting6Value
				BREAK
			ENDSWITCH
			
			PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_FMMC_PAINTING_STOLEN - has been received.")
			PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_FMMC_PAINTING_STOLEN - iCashDropped: ", iCashDropped, " Sent from iPart: ", Event.iPart, " iPainting: ", Event.iPainting)
			
			MC_ServerBD.iCashGrabTotalTake += iCashDropped		
			IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE()
				MC_ServerBD_1.iVaultTake += iCashDropped	
			ENDIF
		ENDIF
	ENDIF	
ENDPROC

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: HEIST SPECIAL CASE EVENTS !
//
//************************************************************************************************************************************************************



PROC PROCESS_HEIST_QUIET_KNOCKING(INT iCount)
	
	SCRIPT_EVENT_DATA_HEIST_QUIET_KNOCKING EventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
	
		IF EventData.Details.Type = SCRIPT_EVENT_HEIST_QUIET_KNOCKING
			
			IF EventData.bQuiet
				START_AUDIO_SCENE("DLC_HEIST_SERIES_A_BIKERS_TRAILERPARK_SCENE")
				PRINTLN("[MJM] START_AUDIO_SCENE(LC_HEIST_SERIES_A_BIKERS_TRAILERPARK_SCENE)")
			ELSE
				STOP_AUDIO_SCENE("DLC_HEIST_SERIES_A_BIKERS_TRAILERPARK_SCENE")
				PRINTLN("[MJM] STOP_AUDIO_SCENE(LC_HEIST_SERIES_A_BIKERS_TRAILERPARK_SCENE)")
				SET_BIT(iLocalBoolCheck9,LBOOL9_KNOCKING_QUIETED)	
			ENDIF
			
			PRINTLN("    ----->    PROCESS_HEIST_QUIET_KNOCKING - SENT FROM ", GET_PLAYER_NAME(EventData.Details.FromPlayerIndex))
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_HEIST_PLAYER_IN_TURRET(INT iCount)
	
	SCRIPT_EVENT_DATA_HEIST_PLAYER_IN_TURRET EventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
	
		IF EventData.Details.Type = SCRIPT_EVENT_HEIST_PLAYER_IN_TURRET
			
			PLAYER_INDEX turretPlayer = EventData.Details.FromPlayerIndex
			PED_INDEX turretPed = GET_PLAYER_PED(turretPlayer)
			
			IF EventData.bInTurret
				iTurretPlayerCounter++
				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(turretPed)
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(turretPed, "DLC_HEIST_REMOTE_PLAYER_ON_TURRET_Group")
				IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_HEIST_VEHICLE_TURRET_BOOST_SCENE")
					START_AUDIO_SCENE("DLC_HEIST_VEHICLE_TURRET_BOOST_SCENE")
					PRINTLN("[MJM] PROCESS_HEIST_PLAYER_IN_TURRET - START_AUDIO_SCENE(DLC_HEIST_VEHICLE_TURRET_BOOST_SCENE)")
				ENDIF
				PRINTLN("[MJM] PROCESS_HEIST_PLAYER_IN_TURRET - bInTurret = TRUE")
			ELSE
				iTurretPlayerCounter--
				IF iTurretPlayerCounter <= 0 //Nobody left on a turret
				AND IS_AUDIO_SCENE_ACTIVE("DLC_HEIST_VEHICLE_TURRET_BOOST_SCENE")
					STOP_AUDIO_SCENE("DLC_HEIST_VEHICLE_TURRET_BOOST_SCENE")
					PRINTLN("[MJM] PROCESS_HEIST_PLAYER_IN_TURRET - STOP_AUDIO_SCENE(DLC_HEIST_VEHICLE_TURRET_BOOST_SCENE)")
				ENDIF
				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(turretPed)
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(turretPed, "DLC_HEIST_CREW_REMOTE_PLAYER_Group")
				PRINTLN("[MJM] PROCESS_HEIST_PLAYER_IN_TURRET - bInTurret = FALSE")	
			ENDIF
			
			PRINTLN("    ----->    PROCESS_HEIST_PLAYER_IN_TURRET - SENT FROM ", GET_PLAYER_NAME(EventData.Details.FromPlayerIndex))
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_NETWORK_INDEX_FOR_LATER_RULE(INT iCount)

	SCRIPT_EVENT_DATA_NETWORK_INDEX_FOR_LATER_RULE LaterObjectiveData
	//BOOL bTicker = TRUE
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, LaterObjectiveData, SIZE_OF(LaterObjectiveData))

		IF LaterObjectiveData.Details.Type = SCRIPT_EVENT_NETWORK_INDEX_FOR_LATER_RULE
			PRINTLN("[RCC MISSION] LaterObjectiveData.NetIdForLaterObjective = ",NATIVE_TO_INT(LaterObjectiveData.NetIdForLaterObjective))
			PRINTLN("[RCC MISSION] LaterObjectiveData.iLaterObjectiveType = ",LaterObjectiveData.iLaterObjectiveType)
			PRINTLN("[RCC MISSION] LaterObjectiveData.iLaterObjectiveEntityIndex = ",LaterObjectiveData.iLaterObjectiveEntityIndex)
			IF bIsLocalPlayerHost
				//IF NETWORK_DOES_NETWORK_ID_EXIST(LaterObjectiveData.NetIdForLaterObjective)
					SWITCH LaterObjectiveData.iLaterObjectiveType
						
						CASE ci_TARGET_PED
						
							MC_serverBD_1.sFMMC_SBD.niPed[LaterObjectiveData.iLaterObjectiveEntityIndex] = LaterObjectiveData.NetIdForLaterObjective
							PRINTLN("[RCC MISSION] PROCESS_NETWORK_INDEX_FOR_LATER_RULE: MC_serverBD_1.sFMMC_SBD.niPed = ",NATIVE_TO_INT(MC_serverBD_1.sFMMC_SBD.niPed[LaterObjectiveData.iLaterObjectiveEntityIndex]))
												

						BREAK
						
						CASE ci_TARGET_VEHICLE

							MC_serverBD_1.sFMMC_SBD.niVehicle[LaterObjectiveData.iLaterObjectiveEntityIndex] = LaterObjectiveData.NetIdForLaterObjective

							SET_BIT(MC_serverBD.iAmbientOverrideVehicle,LaterObjectiveData.iLaterObjectiveEntityIndex)
							PRINTLN("[RCC MISSION] PROCESS_NETWORK_INDEX_FOR_LATER_RULE: MC_serverBD_1.sFMMC_SBD.niVehicle = ",NATIVE_TO_INT(MC_serverBD_1.sFMMC_SBD.niVehicle[LaterObjectiveData.iLaterObjectiveEntityIndex]))
							
						BREAK
						
						CASE ci_TARGET_OBJECT
						
							MC_serverBD_1.sFMMC_SBD.niObject[LaterObjectiveData.iLaterObjectiveEntityIndex] = LaterObjectiveData.NetIdForLaterObjective
							PRINTLN("[RCC MISSION] PROCESS_NETWORK_INDEX_FOR_LATER_RULE: MC_serverBD_1.sFMMC_SBD.niObject = ",NATIVE_TO_INT(MC_serverBD_1.sFMMC_SBD.niObject[LaterObjectiveData.iLaterObjectiveEntityIndex]))
								
						BREAK
						
					
					ENDSWITCH


				//ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC PROCESS_FMMC_HEIST_LEADER_PLAY_COUNT(INT iCount)

	SCRIPT_EVENT_DATA_FMMC_HEIST_PLAYS EventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_HEIST_LEADER_PLAY_COUNT
			IF bIsLocalPlayerHost
			AND MC_serverBD_3.iNumSVMPlays <= 0
			AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet6, SBBOOL6_PLAY_COUNT_SET)
				PRINTLN("[RCC MISSION] PROCESS_FMMC_HEIST_LEADER_PLAY_COUNT = ",EventData.iNumHeistPlays)
				MC_serverBD.iNumHeistPlays = EventData.iNumHeistPlays
				SET_BIT(MC_serverBD.iServerBitSet6, SBBOOL6_PLAY_COUNT_SET)
			ELSE
				PRINTLN("[JS] PROCESS_FMMC_HEIST_LEADER_PLAY_COUNT - Not host or this has already been called, this shouldn't be called")
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_SVM_LEADER_PLAY_COUNT(INT iCount)

	SCRIPT_EVENT_DATA_FMMC_SVM_PLAYS EventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_SVM_LEADER_PLAY_COUNT
			IF bIsLocalPlayerHost
			AND MC_serverBD_3.iNumSVMPlays <= 0
			AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet6, SBBOOL6_PLAY_COUNT_SET)
				PRINTLN("[RCC MISSION] PROCESS_FMMC_SVM_LEADER_PLAY_COUNT = ", EventData.iNumSVMPlays)
				MC_serverBD_3.iNumSVMPlays = EventData.iNumSVMPlays
				SET_BIT(MC_serverBD.iServerBitSet6, SBBOOL6_PLAY_COUNT_SET)
			ELSE
				PRINTLN("[JS] PROCESS_FMMC_SVM_LEADER_PLAY_COUNT - Not host or this has already been called, this shouldn't be called")
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC CLEANUP_PRI_FIN_MCS2_PLANE(NETWORK_INDEX niPlane)
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(niPlane)
		
		INT iveh = IS_ENTITY_A_MISSION_CREATOR_ENTITY(NET_TO_VEH(niPlane))
		
		PRINTLN("[RCC MISSION] CLEANUP_PRI_FIN_MCS2_PLANE - Attempting to clean up vehicle with network index ",NATIVE_TO_INT(niPlane),", iveh = ",iveh)
		
		IF iveh != -1
			
			INT iteam
			FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
				CLEAR_BIT(MC_serverBD.iVehteamFailBitset[iveh],iteam)
				CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iteam], iveh)
			ENDFOR
			
			MC_serverBD_2.iCurrentVehRespawnLives[iveh] = 0
			PRINTLN("[RCC MISSION] CLEANUP_PRI_FIN_MCS2_PLANE - Veh ",iveh," getting cleaned up - set MC_serverBD_2.iCurrentVehRespawnLives[",iveh,"] = 0.")
			
			CLEAR_BIT(MC_serverBD.iVehCleanup_NeedOwnershipBS, iveh)
			
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(niPlane)
				DELETE_NET_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
			ELSE
				NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
				SET_BIT(MC_serverBD.iVehCleanup_NeedOwnershipBS, iveh)
			ENDIF
		ENDIF
	ENDIF
			
ENDPROC

PROC PROCESS_FMMC_PRI_FIN_MCS2_PLANE_NON_CRITICAL(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_PED_ID EventData
	
	IF bIsLocalPlayerHost
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
			IF EventData.Details.Type = SCRIPT_EVENT_FMMC_PRI_FIN_MCS2_PLANE_NON_CRITICAL
				
				INT iveh = EventData.iPedid
				
				PRINTLN("[RCC MISSION] PROCESS_FMMC_PRI_FIN_MCS2_PLANE_NON_CRITICAL - Setting plane as non-critical, vehicle ",iveh)
				
				INT iteam
				FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
					CLEAR_BIT(MC_serverBD.iVehteamFailBitset[iveh],iteam)
					CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iteam], iveh)
				ENDFOR
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PRI_FIN_MCS2_PLANE_CLEANUP(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_NETWORK_INDEX EventData
	
	IF bIsLocalPlayerHost
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
			IF EventData.Details.Type = SCRIPT_EVENT_FMMC_PRI_FIN_MCS2_PLANE_CLEANUP
				PRINTLN("[RCC MISSION] PROCESS_FMMC_PRI_FIN_MCS2_PLANE_CLEANUP - calling CLEANUP_PRI_FIN_MCS2_PLANE with network index ",NATIVE_TO_INT(EventData.vehNetID))
				CLEANUP_PRI_FIN_MCS2_PLANE(EventData.vehNetID)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_VIP_MARKER_POSITION(INT iCount)

	SCRIPT_EVENT_DATA_VIP_MARKER  EventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_VIP_MARKER_POSITION
			IF bIsLocalPlayerHost
				IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))					
					IF NOT IS_VECTOR_ZERO(EventData.vVIPMarkerPosition)
						MC_serverBD_3.vVIPMarker = EventData.vVIPMarkerPosition
						PRINTLN("[RCC MISSION] PROCESS_FMMC_VIP_MARKER_POSITION - MC_serverBD.vVIPMarker" , EventData.vVIPMarkerPosition )
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_CONTAINER_BEING_INVESTIGATED(INT iCount)

	SCRIPT_EVENT_DATA_CONTAINER_INVESTIGATED  EventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_INVESTIGATE_CONTAINER
			IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
				PRINTLN("[RCC MISSION] PROCESS_FMMC_CONTAINER_BEING_INVESTIGATED = ", EventData.iObj, " BY = ", EventData.iPart)
				IF EventData.iObj > -1
					IF iTrackifyTargets[EventData.iObj] > -1
						IF bIsLocalPlayerHost
							IF EventData.bInvestigationInterrupted
								IF IS_BIT_SET(MC_serverBD.iServerObjectBeingInvestigatedBitSet, EventData.iObj)
									PRINTLN("[RCC MISSION] - bInvestigationInterrupted for iObj - ", EventData.iObj)
									CLEAR_BIT(MC_serverBD.iServerObjectBeingInvestigatedBitSet, EventData.iObj)
									
									IF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_OBJ_TARGET_BEING_INVESTIGATED)
										CLEAR_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_OBJ_TARGET_BEING_INVESTIGATED)
									ENDIF
									
									IF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_SHOULD_SHOW_COLLECTABLE)
										CLEAR_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_SHOULD_SHOW_COLLECTABLE)
										PRINTLN("[RCC MISSION] PROCESS_FMMC_CONTAINER_BEING_INVESTIGATED ... Interrupted ... CLEAR_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_SHOULD_SHOW_COLLECTABLE)")
									ENDIF
									
									MC_serverBD.iObjHackPart[iTrackifyTargets[EventData.iObj]] = - 1
									PRINTLN("[RCC MISSION] - MC_serverBD.iObjHackPart[", iTrackifyTargets[EventData.iObj], "] = -1")
								ENDIF
							ELSE
								SET_BIT(MC_serverBD.iServerObjectBeingInvestigatedBitSet, EventData.iObj)
								
								IF MC_serverBD.iObjHackPart[iTrackifyTargets[EventData.iObj]] = -1
									MC_serverBD.iObjHackPart[iTrackifyTargets[EventData.iObj]] = EventData.iPart
								ELSE
									PRINTLN("[RCC MISSION] PROCESS_FMMC_CONTAINER_BEING_INVESTIGATED iObjHackPart is already assigned ... iObjHackPart = ", MC_serverBD.iObjHackPart[iTrackifyTargets[EventData.iObj]])
								ENDIF
								
								IF EventData.bTarget = TRUE
									SET_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_OBJ_TARGET_BEING_INVESTIGATED)
									PRINTLN("[RCC MISSION] PROCESS_FMMC_CONTAINER_BEING_INVESTIGATED is Target")
								ELSE
									//CLEAR_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_OBJ_TARGET_BEING_INVESTIGATED)
									PRINTLN("[RCC MISSION] PROCESS_FMMC_CONTAINER_BEING_INVESTIGATED is Empty")
								ENDIF
								
								IF EventData.bInvestigationFinished = TRUE
									SET_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_OBJ_0_FOUND + EventData.iObj)
									PRINTLN("[RCC MISSION] PROCESS_FMMC_CONTAINER_BEING_INVESTIGATED = ", EventData.iObj, " BY = ", EventData.iPart , " INVESTIGATION FINISHED")
								ENDIF
								
								IF EventData.bShowCollectable = TRUE
									SET_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_SHOULD_SHOW_COLLECTABLE)
									PRINTLN("[RCC MISSION] PROCESS_FMMC_CONTAINER_BEING_INVESTIGATED = ", EventData.iObj, " BY = ", EventData.iPart , " SHOW COLLECTABLE")
								ELSE
									CLEAR_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_SHOULD_SHOW_COLLECTABLE)
									PRINTLN("[RCC MISSION] PROCESS_FMMC_CONTAINER_BEING_INVESTIGATED ... CLEAR_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_SHOULD_SHOW_COLLECTABLE)")
								ENDIF
								
								IF EventData.bTarget = TRUE
								AND EventData.bInvestigationFinished = TRUE
									SET_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_TARGET_FOUND)
									PRINTLN("[RCC MISSION] PROCESS_FMMC_CONTAINER_BEING_INVESTIGATED = TARGET FOUND")
								ENDIF
							ENDIF
						ENDIF
						IF NOT EventData.bInvestigationInterrupted
						AND EventData.bTarget
							g_iDawnRaidPickupCarrierPart = EventData.iPart
							PRINTLN("[RCC MISSION] PROCESS_FMMC_CONTAINER_BEING_INVESTIGATED - g_iDawnRaidPickupCarrierPart = ", EventData.iPart)
						ENDIF
					ELSE
						PRINTLN("[RCC MISSION] - Obj has no read object index - ", EventData.iObj)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC


PROC PROCESS_FMMC_REQUEST_BINBAG( INT iCount )

	SCRIPT_EVENT_DATA_BINBAG_REQUEST BinBagData
	
	IF( bIsLocalPlayerHost )
		IF( GET_EVENT_DATA( SCRIPT_EVENT_QUEUE_NETWORK, iCount, BinBagData, SIZE_OF( BinBagData ) ) )
			IF( BinBagData.Details.Type = SCRIPT_EVENT_FMMC_REQUEST_BINBAG )
				BOOL bPartValid, bObjValid, bPartOwnsBag, bNoPartOwnsBag
				
				bPartValid = BinBagData.iPart > -1 AND BinBagData.iPart < MAX_NUM_MC_PLAYERS
				bObjValid  = BinBagData.iObj > -1 AND BinBagData.iObj < FMMC_MAX_NUM_OBJECTS
				
				IF( bPartValid )
					bPartOwnsBag = MC_serverBD.iBinbagOwners[ BinBagData.iPart ] = BinBagData.iObj
				ENDIF
				IF( bObjValid )
					bNoPartOwnsBag = NOT DOES_ANY_PARTICIPANT_OWN_THIS_BINBAG( BinBagData.iObj )
				ENDIF
				
				IF( bPartOwnsBag OR ( bPartValid AND bNoPartOwnsBag ) )
					IF( BinBagData.bPuttingDown )
						MC_serverBD.iBinbagOwners[ BinBagData.iPart ] = -1
					ELSE
						MC_serverBD.iBinbagOwners[ BinBagData.iPart ] = BinBagData.iObj
					ENDIF
					BROADCAST_BINBAG_OWNER_CHANGED( MC_serverBD.iBinbagOwners[ BinBagData.iPart ], BinBagData.iPart )
				ELSE
					IF( bPartValid )
						// failed to acquire bag
						CPRINTLN( DEBUG_SIMON, "PROCESS_FMMC_REQUEST_BINBAG - iPart:", BinBagData.iPart, " failed to acquire iObj:", BinBagData.iObj, ", bPuttingDown:", BinBagData.bPuttingDown )
						BROADCAST_BINBAG_OWNER_CHANGED( -2, BinBagData.iPart )
					ENDIF
				ENDIF
				
				#IF IS_DEBUG_BUILD
				IF( NOT bPartValid )
					ASSERTLN( "PROCESS_FMMC_REQUEST_BINBAG - iPart = ", BinBagData.iPart, " this is an invalid participant index!" )
				ENDIF
				CPRINTLN( DEBUG_SIMON, "PROCESS_FMMC_REQUEST_BINBAG - received event for iPart:", BinBagData.iPart, ", iObj:", BinBagData.iObj, ", bPuttingDown:", BinBagData.bPuttingDown )
				CPRINTLN( DEBUG_SIMON, " >>> bPartValid:", bPartValid, ", bObjValid:", bObjValid, ", bPartOwnsBag:", bPartOwnsBag, ", bNoPartOwnsBag:", bNoPartOwnsBag )
				CPRINTLN( DEBUG_SIMON, " *** iBinbagOwners ***" )
				INT i
				REPEAT GET_MAXIMUM_PLAYERS_ON_MISSION() i
					CPRINTLN( DEBUG_SIMON, " >>> Part:", i, " owns Obj:", MC_serverBD.iBinbagOwners[ i ] )
				ENDREPEAT
				#ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC PROCESS_FMMC_BINBAG_OWNER_CHANGE( INT iCount )

	SCRIPT_EVENT_DATA_BINBAG_REQUEST BinBagData
	
	IF( GET_EVENT_DATA( SCRIPT_EVENT_QUEUE_NETWORK, iCount, BinBagData, SIZE_OF( BinBagData ) ) )
		IF( BinBagData.Details.Type = SCRIPT_EVENT_FMMC_BINBAG_OWNER_CHANGED )
			CPRINTLN( DEBUG_SIMON, "PROCESS_FMMC_BINBAG_OWNER_CHANGE - received event for iPart:", BinBagData.iPart, ", iObj:", BinBagData.iObj, ", bPuttingDown:", BinBagData.bPuttingDown )
			IF( BinBagData.iPart = iPartToUse )
				IF( sCurrentSpecialPickupState.ePickupState <> eSpecialPickupState_THROWING
				AND sCurrentSpecialPickupState.ePickupState <> eSpecialPickupState_WAIT_FOR_DELIVERY )
					sCurrentSpecialPickupState.iCurrentlyOwnedBinbag = BinBagData.iObj
				ELSE
					CPRINTLN( DEBUG_SIMON, "PROCESS_FMMC_BINBAG_OWNER_CHANGE - Ignoring change because either throwing or waiting for delivery" )
				ENDIF
			ELSE
				IF( sCurrentSpecialPickupState.iCurrentlyOwnedBinbag = BinBagData.iObj AND BinBagData.iObj <> -1 )
					CPRINTLN( DEBUG_SIMON, "PROCESS_FMMC_BINBAG_OWNER_CHANGE - iPart:", iPartToUse, " thought they had control of iObj:", BinBagData.iObj, " so setting iCurrentlyOwnedBinbag to -2" )
					sCurrentSpecialPickupState.iCurrentlyOwnedBinbag = -2
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_TRASH_DRIVE_ENABLE( INT iCount )
	CONST_INT	UNSET	-1

	SCRIPT_EVENT_DATA_TRASH_DRIVE_ENABLE data
	
	IF( GET_EVENT_DATA( SCRIPT_EVENT_QUEUE_NETWORK, iCount, data, SIZE_OF( data ) ) )
		IF( data.Details.Type = SCRIPT_EVENT_FMMC_TRASH_DRIVE_ENABLE )
			CPRINTLN( DEBUG_SIMON, "PROCESS_FMMC_TRASH_DRIVE_ENABLE - Received SCRIPT_EVENT_FMMC_TRASH_DRIVE_ENABLE" )
			BOOL bVehValid = data.iVeh > UNSET AND data.iVeh < FMMC_MAX_VEHICLES
			BOOL bPartValid = data.iPart > UNSET AND data.iPart < MAX_NUM_MC_PLAYERS
		
			IF( bVehValid )
				IF( bPartValid )
					IF( bIsLocalPlayerHost )
						IF( MC_serverBD.iVehRequestPart[ data.iVeh ] = data.iPart )
							IF( NOT data.bEnable )
								CPRINTLN( DEBUG_SIMON, "PROCESS_FMMC_TRASH_DRIVE_ENABLE - Clearing server data for iVeh:", data.iVeh, ", iPart:", data.iPart )
								MC_serverBD.iVehRequestPart[ data.iVeh ] = UNSET
							ENDIF
						ELIF( MC_serverBD.iVehRequestPart[ data.iVeh ] = UNSET )
							IF( data.bEnable )
								CPRINTLN( DEBUG_SIMON, "PROCESS_FMMC_TRASH_DRIVE_ENABLE - Setting server data for iVeh:", data.iVeh, ", iPart:", data.iPart )
								MC_serverBD.iVehRequestPart[ data.iVeh ] = data.iPart
							ENDIF
						ENDIF
					ENDIF
					IF( data.bEnable )
						CPRINTLN( DEBUG_SIMON, "PROCESS_FMMC_TRASH_DRIVE_ENABLE - Clearing local data for iVeh:", data.iVeh, ", iPart:", data.iPart )
						sCurrentSpecialPickupState.iTrashDriveEnable = UNSET
					ELSE
						CPRINTLN( DEBUG_SIMON, "PROCESS_FMMC_TRASH_DRIVE_ENABLE - Setting local data for iVeh:", data.iVeh, ", iPart:", data.iPart )
						sCurrentSpecialPickupState.iTrashDriveEnable = data.iVeh
					ENDIF
				ELSE
					IF( data.bEnable )
						CPRINTLN( DEBUG_SIMON, "PROCESS_FMMC_TRASH_DRIVE_ENABLE - Clearing local data for iVeh:", data.iVeh, ", iPart:", data.iPart )
						sCurrentSpecialPickupState.iTrashDriveEnable = UNSET
					ELSE
						CPRINTLN( DEBUG_SIMON, "PROCESS_FMMC_TRASH_DRIVE_ENABLE - Setting local data for iVeh:", data.iVeh, ", iPart:", data.iPart )
						sCurrentSpecialPickupState.iTrashDriveEnable = data.iVeh
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: MISSION ENDING EVENTS !
//
//************************************************************************************************************************************************************



// The purpose of this event is to make sure the host knows what points everyone has
// if they get an early celebration
PROC PROCESS_FMMC_TEAM_HAS_EARLY_CELEBRATION( INT iCount )
	
	SCRIPT_EVENT_DATA_FMMC_TEAM_HAS_EARLY_CELEBRATION EarlyCelebrationData
	
	IF GET_EVENT_DATA( SCRIPT_EVENT_QUEUE_NETWORK, iCount, EarlyCelebrationData, SIZE_OF( EarlyCelebratioNData ) ) 
		IF EarlyCelebrationData.Details.Type = SCRIPT_EVENT_FMMC_TEAM_HAS_EARLY_CELEBRATION
		
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_FMMC_TEAM_HAS_EARLY_CELEBRATION - EarlyCelebrationData.iTeam = ", EarlyCelebrationData.iTeam) 
			
			END_MISSION_TIMERS()
			iEarlyCelebrationTeamPoints[ EarlyCelebrationData.iTeam ] = EarlyCelebrationData.iWinningPoints
			
			IF HAS_TEAM_PASSED_MISSION( EarlyCelebrationData.iTeam )
				SET_BIT( iLocalBoolCheck11, LBOOL11_STOP_MISSION_FAILING_DUE_TO_EARLY_CELEBRATION )
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_FMMC_TEAM_HAS_EARLY_CELEBRATION - Team ", EarlyCelebrationData.iTeam, " has passed mission - no-one on this team can fail any longer!") 
			ELSE
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_FMMC_TEAM_HAS_EARLY_CELEBRATION - EarlyCelebrationData.iTeam = ", EarlyCelebrationData.iTeam, " hasn't passed the mission. We're gonna fail.") 
			ENDIF
			
		ENDIF	
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_SPECTATOR_POSTFX(INT eventID)
	PRINTLN("[RCC MISSION] PROCESS_FMMC_SPECTATOR_POSTFX")
	
	SCRIPT_EVENT_PLAY_POSTFX_OBJECTIVE_COMPLETE Event
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, eventID, Event, SIZE_OF(Event))
		PLAYER_INDEX playerWhoSent
		PED_INDEX pedSpecTarget
		PLAYER_INDEX playerSpecTarget
		
		playerWhoSent = Event.Details.FromPlayerIndex
		
		
		PRINTLN("[RCC MISSION] PROCESS_FMMC_SPECTATOR_POSTFX - sender = ", NATIVE_TO_INT(playerWhoSent))
		
		IF IS_NET_PLAYER_OK(playerWhoSent, FALSE, FALSE)
			PRINTLN("[RCC MISSION] PROCESS_FMMC_SPECTATOR_POSTFX - Sender player is ok ")
			
			IF IS_PLAYER_SPECTATING(PLAYER_ID())
				pedSpecTarget = GET_SPECTATOR_CURRENT_FOCUS_PED()
				IF IS_PED_A_PLAYER(pedSpecTarget)
					playerSpecTarget = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedSpecTarget)
					IF NETWORK_GET_PLAYER_INDEX_FROM_PED(pedSpecTarget) = playerWhoSent
						PRINTLN("[RCC MISSION] PROCESS_FMMC_SPECTATOR_POSTFX -  Playing PostFX as I'm spectating player", GET_PLAYER_NAME(playerWhoSent))
						PLAY_SOUND_FRONTEND(-1, "CHECKPOINT_NORMAL", "HUD_MINI_GAME_SOUNDSET", FALSE)
						ANIMPOSTFX_PLAY("HeistLocate", 0, FALSE)
					ELIF ARE_PLAYER_IN_SAME_VEHICLE(playerSpecTarget, playerWhoSent)
						PRINTLN("[RCC MISSION] PROCESS_FMMC_SPECTATOR_POSTFX -  Playing PostFX as I'm spectating player", GET_PLAYER_NAME(playerSpecTarget), " who is sharing a vehicle with ", GET_PLAYER_NAME(playerWhoSent))
						PLAY_SOUND_FRONTEND(-1, "CHECKPOINT_NORMAL", "HUD_MINI_GAME_SOUNDSET", FALSE)
						ANIMPOSTFX_PLAY("HeistLocate", 0, FALSE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_BLOCK_SAVE_EOM_CAR_BEEN_DELIVERED(INT iCount)

	SCRIPT_EVENT_DATA_FMMC_NETWORK_INDEX Event
	
	PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_BLOCK_SAVE_EOM_CAR_BEEN_DELIVERED - received event SCRIPT_EVENTBLOCK_SAVE_EOM_CAR_BEEN_DELIVERED.")
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Event, SIZE_OF(Event))
		
		PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_BLOCK_SAVE_EOM_CAR_BEEN_DELIVERED - got event data.")
		
		IF DOES_ENTITY_EXIST(LocalPlayerPed)
			IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(Event.vehNetID)
					IF IS_ENTITY_A_VEHICLE(NET_TO_ENT(Event.vehNetID))
						IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(Event.vehNetID))
							IF IS_PED_IN_VEHICLE(LocalPlayerPed, NET_TO_VEH(Event.vehNetID), TRUE)
								SET_BIT(iLocalBoolCheck6, LBOOL6_BLOCK_SAVE_EOM_CAR_WAS_DELIVERED)
								PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_BLOCK_SAVE_EOM_CAR_BEEN_DELIVERED - I am in delivered vehicle, setting bool LBOOL6_BLOCK_SAVE_EOM_CAR_WAS_DELIVERED.")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF

ENDPROC

PROC PROCESS_EVENT_SYNC_SPECTATOR_CELEBRATION_GLOBALS_1(INT iEventID)

	SCRIPT_EVENT_DATA_SYNC_SPECTATOR_CELEBRATION_GLOBALS_1 Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
		
		PLAYER_INDEX localPlayerID = PLAYER_ID()
		
		IF IS_PLAYER_SCTV(localPlayerID)
		OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator
//		AND GET_SPECTATOR_CURRENT_FOCUS_PED() = GET_PLAYER_PED(Event.Details.FromPlayerIndex)
		
			#IF IS_DEBUG_BUILD
				NET_NL() NET_PRINT("PROCESS_EVENT_SYNC_SPECTATOR_CELEBRATION_GLOBALS_1_COMPLETE - called...") NET_NL()
			#ENDIF
			
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iRpGained = Event.iRpGained
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iRpStarted = Event.iRpStarted
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iCurrentLevelStartPoints = Event.iCurrentLevelStartPoints
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iCurrentLevelEndPoints = Event.iCurrentLevelEndPoints
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iCurrentLevel = Event.iCurrentLevel
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNextLevel = Event.iNextLevel
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMaximumTake = Event.iMaximumTake
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iActualTake = Event.iActualTake
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMyCutPercentage = Event.iMyCutPercentage
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMyTake = Event.iMyTake
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDifficultyHighEnoughForEliteChallenge = Event.bDifficultyHighEnoughForEliteChallenge
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumEliteChallengeComponents = Event.iNumEliteChallengeComponents
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bEliteChallengeComplete = Event.bEliteChallengeComplete
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iEliteChallengeBonusValue = Event.iEliteChallengeBonusValue
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bFirstTimeBonusShouldBeShown = Event.bFirstTimeBonusShouldBeShown
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iFirstTimeBonusValue = Event.iFirstTimeBonusValue
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bOrderBonusShouldBeShown = Event.bOrderBonusShouldBeShown
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iOrderBonusValue = Event.iOrderBonusValue
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bSameTeamBonusShouldBeShown = Event.bSameTeamBonusShouldBeShown
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iSameTeamBonusValue = Event.iSameTeamBonusValue
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bUltimateBonusShouldBeShown = Event.bUltimateBonusShouldBeShown
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iUltimateBonusValue = Event.iUltimateBonusValue
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bMemberBonusShouldBeShown = Event.bMemberBonusShouldBeShown
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMemberBonusValue = Event.iMemberBonusValue
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bIAmHeistLeader = Event.bIAmHeistLeader
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumJobPoints = Event.iNumJobPoints
			
			SET_BIT(iLocalBoolCheck9, LBOOL9_COPIED_CELEBRATION_DATA_EVENT_1)
			
		ENDIF
		
	ELSE	
		SCRIPT_ASSERT("PROCESS_EVENT_SYNC_SPECTATOR_CELEBRATION_GLOBALS_1_COMPLETE - could not retrieve data.")
	ENDIF
	
ENDPROC

PROC PROCESS_EVENT_SYNC_SPECTATOR_CELEBRATION_GLOBALS_2(INT iEventID)
	
	SCRIPT_EVENT_DATA_SYNC_SPECTATOR_CELEBRATION_GLOBALS_2 Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
		
		PLAYER_INDEX localPlayerID = PLAYER_ID()
		
		IF IS_PLAYER_SCTV(localPlayerID)
		OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator
//		AND GET_SPECTATOR_CURRENT_FOCUS_PED() = GET_PLAYER_PED(Event.Details.FromPlayerIndex)
		
			#IF IS_DEBUG_BUILD
				NET_NL() NET_PRINT("PROCESS_EVENT_SYNC_SPECTATOR_CELEBRATION_GLOBALS_2_COMPLETE - called...") NET_NL()
			#ENDIF
			
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentMissionTime = Event.bShowEliteComponentMissionTime
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iTargetMissionTime = Event.iTargetMissionTime
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMissionTime = Event.iMissionTime
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bMissionTimeChallengeComplete = Event.bMissionTimeChallengeComplete
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentVehicleDamage = Event.bShowEliteComponentVehicleDamage
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iVehicleDamagePercentageTarget = Event.iVehicleDamagePercentageTarget
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iVehicleDamagePercentageActual = Event.iVehicleDamagePercentageActual
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bVehicleDamagePercentagePassed = Event.bVehicleDamagePercentagePassed
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentRachDamage = Event.bShowEliteComponentRachDamage
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iRachDamagePercentageTarget = Event.iRachDamagePercentageTarget
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iRachDamagePercentageActual = Event.iRachDamagePercentageActual
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bRachDamagePercentagePassed = Event.bRachDamagePercentagePassed
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentExtractionTime = Event.bShowEliteComponentExtractionTime
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iTargetExtractionTime = Event.iTargetExtractionTime
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iExtractionTime = Event.iExtractionTime
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bExtractionTimeChallengeComplete = Event.bExtractionTimeChallengeComplete
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentNumPedKills = Event.bShowEliteComponentNumPedKills
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumPedKillsTarget = Event.iNumPedKillsTarget
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumPedKills = Event.iNumPedKills
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bNumPedKillsChallengeComplete = Event.bNumPedKillsChallengeComplete
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentNooseCalled  = Event.bShowEliteComponentNooseCalled
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bNooseCalled = Event.bNooseCalled
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentHealthDamage = Event.bShowEliteComponentHealthDamage
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iHealthDamagePercentageTarget = Event.iHealthDamagePercentageTarget
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iHealthDamagePercentageActual = Event.iHealthDamagePercentageActual
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bHealthDamagePercentagePassed = Event.bHealthDamagePercentagePassed
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentLivesLost = Event.bShowEliteComponentLivesLost
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bNumLivesLostChallengeComplete = Event.bNumLivesLostChallengeComplete
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentTripSkip = Event.bShowEliteComponentTripSkip
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentDroppedToDirect = Event.bShowEliteComponentDroppedToDirect
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentDidQuickRestart = Event.bShowEliteComponentDidQuickRestart
			
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentNumPedHeadshots = Event.bShowEliteComponentNumPedHeadshots
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumPedHeadshotsTarget = Event.iNumPedHeadshotsTarget
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumPedHeadshots = Event.iNumPedHeadshots
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bNumPedHeadshotsChallengeComplete = Event.bNumPedHeadshotsChallengeComplete

			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentHackFailed = Event.bShowEliteComponentHackFailed
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumHacksFailedTarget = Event.iNumHacksFailedTarget
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumHacksFailed = Event.iNumHacksFailed
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bNumHackFailedChallengeComplete = Event.bNumHackFailedChallengeComplete
			
			SET_BIT(iLocalBoolCheck9, LBOOL9_COPIED_CELEBRATION_DATA_EVENT_2)
			
		ENDIF	
		
	ELSE	
		SCRIPT_ASSERT("PROCESS_EVENT_SYNC_SPECTATOR_CELEBRATION_GLOBALS_2_COMPLETE - could not retrieve data.")
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_OBJECTIVE_REVALIDATE(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_OBJECTIVE_REVALIDATE EventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_OBJECTIVE_REVALIDATE
			
			PROCESS_CLIENT_OBJECTIVE_REVALIDATION(EventData.iTeam, EventData.iLocResetBS)
			
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_FORCING_EVERYONE_FROM_VEH(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_FORCING_EVERYONE_FROM_VEH EventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_FORCING_EVERYONE_FROM_VEH
			
			IF iDeliveryVehForcingOut = -1
				
				INT iveh = EventData.iveh
				
				IF iveh >= 0 AND iveh < FMMC_MAX_VEHICLES
					
					NETWORK_INDEX niVeh = MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]
					
					IF NETWORK_DOES_NETWORK_ID_EXIST(niVeh)
						
						VEHICLE_INDEX tempVeh = NET_TO_VEH(niVeh)
						
						IF IS_PED_IN_VEHICLE(LocalPlayerPed, tempVeh)
							iDeliveryVehForcingOut = iveh
							iPartSending_DeliveryVehForcingOut = EventData.iPart
							PRINTLN("[RCC MISSION] PROCESS_FMMC_FORCING_EVERYONE_FROM_VEH - Local player is in the same vehicle! Set iDeliveryVehForcingOut = ",iveh)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_INCREMENT_TEAM_SCORE_RESURRECTION(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_INCREMENT_TEAM_SCORE_RESURRECTION EventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_INCREMENT_TEAM_SCORE_RESURRECTION
			INT iMyScore, iEnemyScore
			SWITCH(MC_playerBD[iPartToUse].iTeam)
				CASE 0
					iMyScore = EventData.iTeam0Score
					iEnemyScore = EventData.iTeam1Score
				BREAK
				CASE 1
					iMyScore = EventData.iTeam1Score
					iEnemyScore = EventData.iTeam0Score
				BREAK
			ENDSWITCH
			iCachedResurrector = -1
			CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_WASTED)
			IF EventData.iTeam = MC_playerBD[iPartToUse].iTeam
				SETUP_NEW_BIG_MESSAGE_WITH_2_INTS(BIG_MESSAGE_GENERIC_WINNER, iMyScore, iEnemyScore, "REZ_WS", DEFAULT, DEFAULT, DEFAULT, "REZ_WT")
			ELSE
				SETUP_NEW_BIG_MESSAGE_WITH_2_INTS(BIG_MESSAGE_GENERIC_LOSER, iMyScore, iEnemyScore, "REZ_LS", DEFAULT, DEFAULT, DEFAULT, "REZ_LT")
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_DISPLAY_CUSTOM_OBJECT_PICKUP_SHARD(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_CUSTOM_OBJECT_PICKUP_SHARD EventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_DISPLAY_CUSTOM_OBJECT_PICKUP_SHARD
			TEXT_LABEL_15 tlShardTitle, tlShardStrapline
			SWITCH EventData.iShardIndex
				CASE 1
					tlShardTitle = g_FMMC_STRUCT.tlCustomShardTitle
					tlShardStrapline = g_FMMC_STRUCT.tlCustomShardStrapline
				BREAK
				CASE 2
					tlShardTitle = g_FMMC_STRUCT.tlCustomShardTitle2
					tlShardStrapline = g_FMMC_STRUCT.tlCustomShardStrapline2
				BREAK
				CASE 3
					tlShardTitle = g_FMMC_STRUCT.tlCustomShardTitle3
					tlShardStrapline = g_FMMC_STRUCT.tlCustomShardStrapline3
				BREAK
			ENDSWITCH
			IF bPlayerToUseOK
				SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GENERIC_TEXT, EventData.ObjPlayer, DEFAULT, tlShardStrapline, tlShardTitle, GET_HUD_COLOUR_FOR_FMMC_TEAM(EventData.iTeam, EventData.ObjPlayer))
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: DEBUG EVENTS !
//
//************************************************************************************************************************************************************



#IF IS_DEBUG_BUILD

PROC PROCESS_FMMC_OVERRIDE_LIVES_FAIL(INT iCount)

	SCRIPT_EVENT_DATA_FMMC_OVERRIDE_LIVES_FAIL ObjectiveData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_OVERRIDE_LIVES_FAIL
			
			PRINTLN("[RCC MISSION][FailOnDeath] PROCESS_FMMC_OVERRIDE_LIVES_FAIL - ObjectiveData.bIgnoreOutOfLivesFail = ",ObjectiveData.bIgnoreOutOfLivesFail)
			
			IF ObjectiveData.bIgnoreOutOfLivesFail
				PRINT_TICKER_WITH_PLAYER_NAME_AND_STRING("TICK_CHEATED", ObjectiveData.Details.FromPlayerIndex, "TICK_CHEAT_OOL1")
			ELSE
				PRINT_TICKER_WITH_PLAYER_NAME_AND_STRING("TICK_CHEATED", ObjectiveData.Details.FromPlayerIndex, "TICK_CHEAT_OOL0")
			ENDIF
			
			IF g_bIgnoreOutOfLivesFailure != ObjectiveData.bIgnoreOutOfLivesFail
				g_bIgnoreOutOfLivesFailure = ObjectiveData.bIgnoreOutOfLivesFail
				
				PRINTLN("[RCC MISSION][FailOnDeath] PROCESS_FMMC_OVERRIDE_LIVES_FAIL - g_bIgnoreOutOfLivesFailure = ",g_bIgnoreOutOfLivesFailure)
				
				IF bIsLocalPlayerHost
					IF g_bIgnoreOutOfLivesFailure
						PRINTLN("[RCC MISSION][FailOnDeath] PROCESS_FMMC_OVERRIDE_LIVES_FAIL - Server set bit SBDEBUG_DONTFAILWHENOUTOFLIVES")
						SET_BIT(MC_serverBD.iDebugBitSet, SBDEBUG_DONTFAILWHENOUTOFLIVES)
					ELSE
						PRINTLN("[RCC MISSION][FailOnDeath] PROCESS_FMMC_OVERRIDE_LIVES_FAIL - Server clear bit SBDEBUG_DONTFAILWHENOUTOFLIVES")
						CLEAR_BIT(MC_serverBD.iDebugBitSet, SBDEBUG_DONTFAILWHENOUTOFLIVES)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC PROCESS_DEBUG_OBJECTIVE_SKIP_FOR_TEAM(INT iCount)

	SCRIPT_EVENT_DATA_DEBUG_OBJECTIVE_SKIP_FOR_TEAM EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_DEBUG_OBJECTIVE_SKIP_FOR_TEAM
			
			bUsedDebugMenu = TRUE
			
			IF bIsLocalPlayerHost
				IF EventData.iNewTeam = -1
					INT iObjective = EventData.iSkipObjective
					BOOL bPreviousObjective = FALSE
					
					IF iObjective <= MC_serverBD_4.iCurrentHighestPriority[EventData.iTeam]
						PRINTLN("[RCC MISSION] PROCESS_DEBUG_OBJECTIVE_SKIP_FOR_TEAM - team = ",EventData.iTeam, " objective to skip back to: ",iObjective)
						bPreviousObjective = TRUE
					ELSE
						PRINTLN("[RCC MISSION] PROCESS_DEBUG_OBJECTIVE_SKIP_FOR_TEAM - team = ",EventData.iTeam, " objective to skip to: ",iObjective)
						iObjective--
					ENDIF
					
					INVALIDATE_UP_TO_AND_INCLUDING_OBJECTIVE(EventData.iTeam,iObjective,FALSE,TRUE,bPreviousObjective)
				ELSE
					PRINTLN("[RCC MISSION] PROCESS_DEBUG_OBJECTIVE_SKIP_FOR_TEAM - player on team ",EventData.iTeam," has swapped to team ",EventData.iNewTeam)
					STOP_MISSION_FAILING_AS_TEAM_SWAPS(EventData.iTeam, EventData.iNewTeam)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

#ENDIF



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: APARTMENT WARPING EVENTS !
//
//************************************************************************************************************************************************************



PROC PROCESS_FMMC_APARTMENTWARP_CS_REQUEST(INT iCount)
	
	IF bIsLocalPlayerHost
		SCRIPT_EVENT_DATA_FMMC_PARTICIPANT_ID ObjectiveData
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
			IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_APARTMENTWARP_CS_REQUEST
				PRINTLN("[RCC MISSION] ObjectiveData.iPart = ",ObjectiveData.iPart)
				
				IF MC_serverBD.iPartWarpingIn = -1
					MC_serverBD.iPartWarpingIn = ObjectiveData.iPart
				ELSE
					PRINTLN("[RCC MISSION] Apartment warp CS is already occurring - in use by participant ", MC_serverBD.iPartWarpingIn)
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_APARTMENTWARP_CS_RELEASE(INT iCount)
	
	IF bIsLocalPlayerHost
		SCRIPT_EVENT_DATA_FMMC_PARTICIPANT_ID ObjectiveData
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
			IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_APARTMENTWARP_CS_RELEASE
				PRINTLN("[RCC MISSION] ObjectiveData.iPart = ",ObjectiveData.iPart)
				
				IF MC_serverBD.iPartWarpingIn = ObjectiveData.iPart
					MC_serverBD.iPartWarpingIn = -1
				ELSE
					PRINTLN("[RCC MISSION] Unable to release Apartment Warp CS as not owned by this participant - currently in use by ", MC_serverBD.iPartWarpingIn)
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: DOOR SYSTEM EVENTS !
//
//************************************************************************************************************************************************************



PROC PROCESS_SCRIPT_EVENT_FMMC_OPEN_DOOR(INT iEventID)

	SCRIPT_EVENT_DATA_FMMC_OPEN_DOOR Event
	
	PRINTLN("[RCC MISSION] - SCRIPT_EVENT_DATA_FMMC_OPEN_DOOR - has been received.")

	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
		fHackDoorStartHeading[Event.iDoor] = Event.fHeading
		PRINTLN("[RCC MISSION] - SCRIPT_EVENT_DATA_FMMC_OPEN_DOOR - setting door open: ",Event.iDoor)
		PRINTLN("[RCC MISSION] - SCRIPT_EVENT_DATA_FMMC_OPEN_DOOR - setting door fHackDoorStartHeading: ",Event.fHeading)
	ENDIF


ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_SWAP_DOOR_FOR_DAMAGED_VERSION(INT iEventID)
	IF bIsLocalPlayerHost
		SCRIPT_EVENT_DATA_FMMC_SWAP_DOOR_FOR_DAMAGED_VERSION Event
		
		PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_FMMC_SWAP_DOOR_FOR_DAMAGED_VERSION - has been received.")

		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
			SET_BIT(MC_serverBD.iHackDoorThermiteDamageBitset, Event.iRule)

			PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_FMMC_SWAP_DOOR_FOR_DAMAGED_VERSION - iRule =  ", Event.iRule)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_SMASHED_BY_TOUCH(INT iEventID)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_SMASH_CAR)
		EXIT
	ENDIF
	
	SCRIPT_EVENT_DATA_FMMC_SMASHED_BY_TOUCH EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_SMASHED_BY_TOUCH
			IF HAS_NET_TIMER_STARTED(timerSmashForceHit)
				EXIT
			ENDIF
			IF EventData.details.FromPlayerIndex = LocalPlayer
				PRINTLN("[SMASH_FORCE][PROCESS_SCRIPT_EVENT_FMMC_SMASHED_BY_TOUCH] Can't damage to your self. Exiting.")
				EXIT
			ENDIF
			
			PED_INDEX damagerPed = GET_PLAYER_PED(EventData.details.FromPlayerIndex)
			
			IF NOT IS_PED_IN_ANY_VEHICLE(localPlayerPed)
				PRINTLN("[SMASH_FORCE][PROCESS_SCRIPT_EVENT_FMMC_SMASHED_BY_TOUCH] We are not in a vehicle. Exiting.")
				EXIT
			ENDIF
			IF NOT IS_PED_IN_ANY_VEHICLE(damagerPed)
				PRINTLN("[SMASH_FORCE][PROCESS_SCRIPT_EVENT_FMMC_SMASHED_BY_TOUCH] Damager is not in a vehicle. Exiting.")
				EXIT
			ENDIF
			
			VEHICLE_INDEX vIndex = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)			
			VEHICLE_INDEX damagerVeh = GET_VEHICLE_PED_IS_IN(damagerPed)
			
			VECTOR vDir = GET_ENTITY_COORDS(vIndex) - GET_ENTITY_COORDS(damagerVeh)
			vDir = NORMALISE_VECTOR(vDir)

			FLOAT fFactor = EventData.fSpeedFactor
			FLOAT iForce = g_FMMC_STRUCT.fMaxPushSmash * fFactor
			FLOAT iForceV = g_FMMC_STRUCT.fMaxPushVSmash * fFactor
			IF iForce < g_FMMC_STRUCT.fMinPushSmash
				iForce = g_FMMC_STRUCT.fMinPushSmash
			ENDIF
			IF iForceV < g_FMMC_STRUCT.fMinPushVSmash
				iForceV = g_FMMC_STRUCT.fMinPushVSmash
			ENDIF
			vDir.X = vDir.X*iForce
			vDir.Y = vDir.Y*iForce
			vDir.Z = iForceV
			//FORCE
			SET_ENTITY_INVINCIBLE(vIndex,TRUE)
			APPLY_FORCE_TO_ENTITY(vIndex,APPLY_TYPE_EXTERNAL_IMPULSE,vDir,<<0,0,0>>,0,FALSE,FALSE,TRUE)
			SET_ENTITY_INVINCIBLE(vIndex,FALSE)
			
			//DAMAGE
			FLOAT fDamage = g_FMMC_STRUCT.iMaxDamageSmash*fFactor
			IF fDamage < g_FMMC_STRUCT.iMinDamageSmash
				fDamage = TO_FLOAT(g_FMMC_STRUCT.iMinDamageSmash)
			ENDIF
			
			FLOAT fHealth = GET_VEHICLE_BODY_HEALTH(vIndex) - fDamage
			IF fHealth < 0.0
				fHealth = 0.0
			ENDIF
			
			SET_VEHICLE_BODY_HEALTH(vIndex, fHealth)

			SET_CONTROL_SHAKE(PLAYER_CONTROL, 130, 256)
			SHAKE_GAMEPLAY_CAM("SMALL_EXPLOSION_SHAKE",fFactor + 0.1)
			
			REINIT_NET_TIMER(timerSmashForceHit) 
			PRINTLN("[SMASH_FORCE][PROCESS_SCRIPT_EVENT_FMMC_SMASHED_BY_TOUCH] Smashed in vDir ",vDir, " with a factor of ",fFactor, " and damage ",fDamage)
			
			IF EventData.bDropItem
				IF NOT HAS_NET_TIMER_STARTED(MC_serverBD_3.tdStolenPackageCooldownTimer)
				OR HAS_NET_TIMER_EXPIRED_READ_ONLY(MC_serverBD_3.tdStolenPackageCooldownTimer, (g_FMMC_STRUCT.iObjectStolenCooldownTimer*1000))
				OR g_FMMC_STRUCT.iObjectStolenCooldownTimer = 0	
					
					INT iObjDropped = GET_OBJECT_INDEX_ATTACHED_TO_ENTITY(LocalPlayerPed) //DETACH_PORTABLE_PICKUP_FROM_SPECIFIC_PED(LocalPlayerPed, NOT EventData.bStealItem, EventData.bStealItem, FALSE)
					IF iObjDropped != -1
						OBJECT_INDEX tempObj = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iObjDropped])
						SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM(tempObj,iObjDropped,MC_playerBD[iLocalPart].iteam)
						
						PRINTLN("[SMASH_FORCE][PROCESS_SCRIPT_EVENT_FMMC_STEAL_OBJECT] Dropped object ",iObjDropped, " Steal ", EventData.bStealItem)
						IF EventData.bStealItem
							SET_ENTITY_VISIBLE(tempObj, FALSE)
							ATTACH_PORTABLE_PICKUP_TO_PED(tempObj, GET_PLAYER_PED(eventData.Details.FromPlayerIndex))
							BROADCAST_FMMC_STEAL_OBJECT_EVENT(EventData.details.FromPlayerIndex,iObjDropped)
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF						
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_STEAL_OBJECT(INT iEventID)
		
	SCRIPT_EVENT_DATA_FMMC_STEAL_OBJECT EventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_SMASH_CAR)
		OR (EventData.piToPlayer != LocalPlayer AND NOT bIsLocalPlayerHost)
			EXIT
		ENDIF
	
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_STEAL_OBJECT
	
			PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_STEAL_OBJECT] - piToPlayer: ", GET_PLAYER_NAME(EventData.piToPlayer), " iObjectStolenCooldownTimer: ", g_FMMC_STRUCT.iObjectStolenCooldownTimer, " TimerExp: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdStolenPackageCooldownTimer))
		
			IF NOT HAS_NET_TIMER_STARTED(MC_serverBD_3.tdStolenPackageCooldownTimer)
			OR HAS_NET_TIMER_EXPIRED_READ_ONLY(MC_serverBD_3.tdStolenPackageCooldownTimer, (g_FMMC_STRUCT.iObjectStolenCooldownTimer*1000))
			OR g_FMMC_STRUCT.iObjectStolenCooldownTimer = 0	
//				IF EventData.piToPlayer = LocalPlayer
//					iSmashPickup = EventData.iObjectToDrop
//					PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_STEAL_OBJECT] - iSmashPickup: ", iSmashPickup)
//				ENDIF
				
				IF bIsLocalPlayerHost
				AND g_FMMC_STRUCT.iObjectStolenCooldownTimer > 0
					PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_STEAL_OBJECT] - Restarting Timer for Smash Steal.")
					REINIT_NET_TIMER(MC_serverBD_3.tdStolenPackageCooldownTimer)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC PROCESS_SCRIPT_EVENT_NOTIFY_WANTED_SET_FOR_VEHICLE(INT iEventID)

	IF (bIsLocalPlayerHost)
	
		SCRIPT_EVENT_DATA_NOTIFY_WANTED_SET_FOR_VEHICLE EventData
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
			IF EventData.Details.Type = SCRIPT_EVENT_NOTIFY_WANTED_SET_FOR_VEHICLE
				PRINTLN("[MJL] PROCESS_SCRIPT_EVENT_NOTIFY_WANTED_SET_FOR_VEHICLE")		
				SET_BIT(MC_serverBD_1.iVehicleTheftWantedTriggered, EventData.iVeh)
				PRINTLN("    ----->    MARKING VEHICLE ", EventData.iVeh, " as having been stolen")
			ENDIF
		ENDIF
	
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_CASINO_UPDATED_STEAL_PAINTING_STATE(INT iEventID)

	IF (bIsLocalPlayerHost)
		
		SCRIPT_EVENT_DATA_CASINO_UPDATED_STEAL_PAINTING_STATE EventData
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
				
			// Set a value in serverBD for the cut stage of the specific painting			
			MC_serverBD_1.iCurrentCutPaintingState[EventData.iCurrentPaintingIndex] = EventData.iCurrentCutPaintingState
			PRINTLN("[MJL] PROCESS_SCRIPT_EVENT_CASINO_UPDATED_STEAL_PAINTING_STATE iCurrentCutPaintingState for painting index: ", EventData.iCurrentPaintingIndex, " is now: ", MC_serverBD_1.iCurrentCutPaintingState[EventData.iCurrentPaintingIndex])
		
			IF MC_serverBD_1.iCurrentCutPaintingState[EventData.iCurrentPaintingIndex] = ciCutPaintingComplete
				SET_BIT(MC_serverBD_1.iOffRuleMinigameCompleted, Eventdata.iobj)
				PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_CASINO_UPDATED_STEAL_PAINTING_STATE - Setting bit in iOffRuleMinigame Completed Steal Painting: ", Eventdata.iobj)
			ENDIF
		ENDIF
	
	ENDIF
ENDPROC

//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: MAIN EVENT PROCESSING !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: PROCESS FMMC SCRIPT EVENT - processes network events, giant switch statement to choose how to process them !
//
//************************************************************************************************************************************************************



PROC PROCESS_FMMC_SCRIPT_EVENT(INT iCount)

    STRUCT_EVENT_COMMON_DETAILS Details
                            
    GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Details, SIZE_OF(Details))
    
    SWITCH Details.Type
	
		CASE SCRIPT_EVENT_SYNC_SPECTATOR_CELEBRATION_GLOBALS_1
			IF g_sBlockedEvents.bSCRIPT_EVENT_SYNC_SPECTATOR_CELEBRATION_GLOBALS_1_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_SYNC_SPECTATOR_CELEBRATION_GLOBALS_1, processing event...")
			PROCESS_EVENT_SYNC_SPECTATOR_CELEBRATION_GLOBALS_1(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_SYNC_SPECTATOR_CELEBRATION_GLOBALS_2
			IF g_sBlockedEvents.bSCRIPT_EVENT_SYNC_SPECTATOR_CELEBRATION_GLOBALS_2_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_SYNC_SPECTATOR_CELEBRATION_GLOBALS_2, processing event...")
			PROCESS_EVENT_SYNC_SPECTATOR_CELEBRATION_GLOBALS_2(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_GIVEN_GUNONRULE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_GIVEN_GUNONRULE_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_GIVEN_GUNONRULE, processing event...")
			PROCESS_FMMC_PED_GIVEN_GUNONRULE(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_COMBAT_STYLE_CHANGED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_COMBAT_STYLE_CHANGED_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_COMBAT_STYLE_CHANGED, processing event...")
			PROCESS_FMMC_PED_COMBAT_STYLE_CHANGED(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_RACE_RALLY_ARROWS
			IF g_sBlockedEvents.bSCRIPT_EVENT_RACE_RALLY_ARROWS_BLOCKED
				BREAK
			ENDIF
			PROCESS_ARROW_EVENT(iCount)
		BREAK	
		
		CASE SCRIPT_EVENT_FMMC_PED_WAITING_AFTER_GOTO
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_WAITING_AFTER_GOTO_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_WAITING_AFTER_GOTO, processing event...")
			PROCESS_FMMC_PED_WAITING_AFTER_GOTO(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_DONE_WAITING_AFTER_GOTO
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_DONE_WAITING_AFTER_GOTO_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_DONE_WAITING_AFTER_GOTO, processing event...")
			PROCESS_FMMC_PED_DONE_WAITING_AFTER_GOTO(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_CLEAR_GOTO_WAITING_STATE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_CLEAR_GOTO_WAITING_STATE_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_CLEAR_GOTO_WAITING_STATE, processing event...")
			PROCESS_FMMC_PED_CLEAR_GOTO_WAITING_STATE(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_DEFENSIVE_AREA_GOTO_CLEANED_UP
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_DEFENSIVE_AREA_GOTO_CLEANED_UP_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_DEFENSIVE_AREA_GOTO_CLEANED_UP, processing event...")
			PROCESS_FMMC_PED_DEFENSIVE_AREA_GOTO_CLEANED_UP(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_DIRTY_FLAG_SET
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_DIRTY_FLAG_SET_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_DIRTY_FLAG_SET, processing event...")
			PROCESS_FMMC_PED_DIRTY_FLAG_SET(iCount)
		BREAK
		
        CASE SCRIPT_EVENT_FMMC_ANIM_STARTED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_ANIM_STARTED_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_ANIM_STARTED, processing event...")
            PROCESS_FMMC_ANIM_STARTED_EVENT(iCount)
        BREAK  
		
		CASE SCRIPT_EVENT_FMMC_ANIM_STOPPED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_ANIM_STOPPED_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_ANIM_STOPPED, processing event...")
            PROCESS_FMMC_ANIM_STOPPED_EVENT(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_SECONDARY_ANIM_CLEANUP
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_SECONDARY_ANIM_CLEANUP_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_SECONDARY_ANIM_CLEANUP, processing event...")
            PROCESS_FMMC_SECONDARY_ANIM_CLEANUP_EVENT(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_TREVOR_BODHI_BREAKOUT_SYNCH_SCENE_ID
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_TREVOR_BODHI_BREAKOUT_SYNCH_SCENE_ID_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_TREVOR_BODHI_BREAKOUT_SYNCH_SCENE_ID, processing event...")
            PROCESS_FMMC_TREVOR_BODHI_BREAKOUT_SYNCH_SCENE_ID(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_GO_FOUNDRY_HOSTAGE_SYNCH_SCENE_ID
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_GO_FOUNDRY_HOSTAGE_SYNCH_SCENE_ID_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_GO_FOUNDRY_HOSTAGE_SYNCH_SCENE_ID, processing event...")
            PROCESS_FMMC_GO_FOUNDRY_HOSTAGE_SYNCH_SCENE_ID(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_INVENTORY_CLEANUP
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_INVENTORY_CLEANUP_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FSCRIPT_EVENT_FMMC_PED_INVENTORY_CLEANUPMMC_ANIM_STARTED, processing event...")
            PROCESS_FMMC_PED_INVENTORY_CLEANUP(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_ADDED_TO_CHARM
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_ADDED_TO_CHARM_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_ADDED_TO_CHARM, processing event...")
            PROCESS_FMMC_PED_ADDED_TO_CHARM(iCount)
        BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_REMOVED_FROM_CHARM
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_REMOVED_FROM_CHARM_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_REMOVED_FROM_CHARM, processing event...")
            PROCESS_FMMC_PED_REMOVED_FROM_CHARM(iCount)
        BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_ADDED_TO_CC
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_ADDED_TO_CC_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_ADDED_TO_CC, processing event...")
            PROCESS_FMMC_PED_ADDED_TO_CC(iCount)
        BREAK
		
		CASE SCRIPT_EVENT_FMMC_CC_PED_BEATDOWN
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_CC_PED_BEATDOWN_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_CC_PED_BEATDOWN, processing event...")
			PROCESS_FMMC_CC_PED_BEATDOWN(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_CC_PED_BEATDOWN_CLEAR
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_CC_PED_BEATDOWN_CLEAR_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_CC_PED_BEATDOWN_CLEAR, processing event...")
			PROCESS_FMMC_CC_PED_BEATDOWN_CLEAR(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_CC_PED_BEATDOWN_HIT
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_CC_PED_BEATDOWN_HIT_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_CC_PED_BEATDOWN_HIT, processing event...")
			PROCESS_FMMC_CC_PED_BEATDOWN_HIT(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_CC_FAIL
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_CC_FAIL_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_CC_FAIL, processing event...")
			PROCESS_FMMC_CC_FAIL(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_CC_DIALOGUE_CLIENT_TO_HOST
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_CC_DIALOGUE_CLIENT_TO_HOST_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_CC_DIALOGUE_CLIENT_TO_HOST, processing event...")
			PROCESS_FMMC_CC_DIALOGUE_CLIENT_TO_HOST(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_CC_DIALOGUE_HOST_TO_CLIENTS
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_CC_DIALOGUE_HOST_TO_CLIENTS_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_CC_DIALOGUE_HOST_TO_CLIENTS, processing event...")
			PROCESS_FMMC_CC_DIALOGUE_HOST_TO_CLIENT(iCount)
		BREAK

		CASE SCRIPT_EVENT_FMMC_CC_AWARD
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_CC_AWARD_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_CC_AWARD, processing event...")
			PROCESS_FMMC_CC_AWARD(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_CC_DEAD
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_CC_DEAD_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_CC_DEAD, processing event...")
			PROCESS_FMMC_CC_DEAD(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_REQUEST_RESTRICTION_ROCKETS
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_REQUEST_RESTRICTION_ROCKETS_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_REQUEST_RESTRICTION_ROCKETS, processing event...")
			PROCESS_FMMC_REQUEST_RESTRICTION_ROCKETS(iCount)
		BREAK
		
		#IF IS_DEBUG_BUILD
		CASE SCRIPT_EVENT_FMMC_OVERRIDE_LIVES_FAIL
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_OVERRIDE_LIVES_FAIL_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_OVERRIDE_LIVES_FAIL, processing event...")
			PROCESS_FMMC_OVERRIDE_LIVES_FAIL(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_DEBUG_OBJECTIVE_SKIP_FOR_TEAM
			IF g_sBlockedEvents.bSCRIPT_EVENT_DEBUG_OBJECTIVE_SKIP_FOR_TEAM_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_DEBUG_OBJECTIVE_SKIP_FOR_TEAM, processing event...")
			PROCESS_DEBUG_OBJECTIVE_SKIP_FOR_TEAM(iCount)
		BREAK			
		#ENDIF
		
		CASE SCRIPT_EVENT_FMMC_OBJECTIVE_END_MESSAGE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_OBJECTIVE_END_MESSAGE_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - PROCESS_FMMC_OBJECTIVE_END_MESSAGE, processing event...")
            PROCESS_FMMC_OBJECTIVE_END_MESSAGE_EVENT(iCount)
        BREAK
		
		CASE SCRIPT_EVENT_FMMC_PLAYER_COMMITED_SUICIDE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PLAYER_COMMITED_SUICIDE_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - PROCESS_FMMC_PLAYER_COMMITED_SUICIDE_EVENT, processing event...")
            PROCESS_FMMC_PLAYER_COMMITED_SUICIDE_EVENT(iCount)
        BREAK
		
		CASE SCRIPT_EVENT_FMMC_PLAYER_KNOCKED_OUT
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PLAYER_KNOCKED_OUT_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - PROCESS_FMMC_PLAYER_KNOCKED_OUT_EVENT, processing event...")
            PROCESS_FMMC_PLAYER_KNOCKED_OUT_EVENT(iCount)
        BREAK
		
		CASE SCRIPT_EVENT_FMMC_PLAYER_KNOCKED_OUT_NOISE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PLAYER_KNOCKED_OUT_NOISE_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PLAYER_KNOCKED_OUT_NOISE, processing event...")
            PROCESS_FMMC_SUMO_PLAYER_KNOCKED_OUT_NOISE_EVENT(iCount)
        BREAK
		
		CASE SCRIPT_EVENT_FMMC_PLAYER_SWITCHED_TEAMS
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PLAYER_SWITCHED_TEAMS_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - PROCESS_FMMC_PLAYER_SWITCHED_TEAMS, processing event...")
            PROCESS_FMMC_PLAYER_SWITCHED_TEAMS_EVENT(iCount)
        BREAK
		
		CASE SCRIPT_EVENT_FMMC_PLAYER_TEAM_SWITCH_REQUEST
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PLAYER_TEAM_SWITCH_REQUEST_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PLAYER_TEAM_SWITCH_REQUEST, processing event...")
            PROCESS_FMMC_PLAYER_REQUEST_TEAM_SWITCH_EVENT(iCount)
        BREAK
		
		CASE SCRIPT_EVENT_SERVER_AUTHORISED_TEAM_SWAP
			IF g_sBlockedEvents.bSCRIPT_EVENT_SERVER_AUTHORISED_TEAM_SWAP_BLOCKED
				BREAK
			ENDIF
		 	PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_SERVER_AUTHORISED_TEAM_SWAP, processing event...")
            PROCESS_FMMC_SERVER_AUTHORISED_TEAM_SWAP_EVENT(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_CLIENT_TEAM_SWAP_COMPLETE
			IF g_sBlockedEvents.bSCRIPT_EVENT_CLIENT_TEAM_SWAP_COMPLETE_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_CLIENT_TEAM_SWAP_COMPLETE, processing event...")
            PROCESS_FMMC_CLIENT_TEAM_SWAP_COMPLETE(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_BMB_BOMB_PLACED
			IF g_sBlockedEvents.bSCRIPT_EVENT_BMB_BOMB_PLACED_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - PROCESS_BMB_BOMB_PLACED_MESSAGE_EVENT, processing event...")
            PROCESS_BMB_BOMB_PLACED_MESSAGE_EVENT(iCount)
        BREAK
		CASE SCRIPT_EVENT_BMB_PROP_DESTROY
			IF g_sBlockedEvents.bSCRIPT_EVENT_BMB_PROP_DESTROY_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_BMB_PROP_DESTROYED, processing event...")
            PROCESS_BMB_PROP_DESTROY_MESSAGE_EVENT(iCount)
        BREAK
		CASE SCRIPT_EVENT_BMB_PICKUP_CREATED
			IF g_sBlockedEvents.bSCRIPT_EVENT_BMB_PICKUP_CREATED_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_BMB_PROP_DESTROYED, processing event...")
            PROCESS_BMB_PICKUP_CREATED_MESSAGE_EVENT(iCount)
        BREAK
		
		CASE SCRIPT_EVENT_FMMC_PLAYER_CROSSED_THE_LINE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PLAYER_CROSSED_THE_LINE_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS -  SCRIPT_EVENT_FMMC_PLAYER_CROSSED_THE_LINE, processing event...")
            PROCESS_PLAYER_CROSSED_LINE_EVENT(iCount)
        BREAK
		
		CASE SCRIPT_EVENT_FMMC_PLAYER_IS_SWAN_DIVE_SCORING
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PLAYER_IS_SWAN_DIVE_SCORING_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS -  SCRIPT_EVENT_FMMC_PLAYER_IS_SWAN_DIVE_SCORING, processing event...")
            PROCESS_PLAYER_IS_SWAN_DIVE_SCORING(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_RANDOM_SPAWN_SLOT_USED
			IF g_sBlockedEvents.bSCRIPT_EVENT_RANDOM_SPAWN_SLOT_USED_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS -  SCRIPT_EVENT_RANDOM_SPAWN_SLOT_USED, processing event...")
            PROCESS_RANDOM_SPAWN_SLOT_FOR_TEAM(iCount)
        BREAK
		
		CASE SCRIPT_EVENT_FMMC_REMOTE_VEHICLE_SWAP
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_REMOTE_VEHICLE_SWAP_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS -  SCRIPT_EVENT_FMMC_REMOTE_VEHICLE_SWAP, processing event...")
            PROCESS_REMOTE_VEHICLE_SWAP(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_OVERTIME_SCORED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_OVERTIME_SCORED_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS -  SCRIPT_EVENT_FMMC_OVERTIME_SCORED, processing event...")
            PROCESS_OVERTIME_SCORED(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_OVERTIME_REMOVE_VEHICLE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_OVERTIME_REMOVE_VEHICLE_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS -  SCRIPT_EVENT_FMMC_OVERTIME_REMOVE_VEHICLE, processing event...")
            PROCESS_OVERTIME_REMOVE_VEHICLE(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_AIRSHOOTOUT_SCORED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_AIRSHOOTOUT_SCORED_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS -  SCRIPT_EVENT_FMMC_AIRSHOOTOUT_SCORED, processing event...")
            PROCESS_AIRSHOOTOUT_SCORED(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_PRON_PLAYERS_DRAWING_TRAILS
			IF g_sBlockedEvents.bSCRIPT_EVENT_PRON_PLAYERS_DRAWING_TRAILS_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS -  SCRIPT_EVENT_PRON_PLAYERS_DRAWING_TRAILS, processing event...")
            PROCESS_EVENT_PRON_PLAYERS_DRAWING_TRAILS(iCount)
        BREAK
		
		CASE SCRIPT_EVENT_FMMC_TRIGGERED_BODY_SCANNER
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_TRIGGERED_BODY_SCANNER
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS -  SCRIPT_EVENT_FMMC_TRIGGERED_BODY_SCANNER, processing event...")
            PROCESS_EVENT_FMMC_TRIGGERED_BODY_SCANNER(iCount)
        BREAK
		
		CASE SCRIPT_EVENT_FMMC_TEAM_HAS_EARLY_CELEBRATION
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_TEAM_HAS_EARLY_CELEBRATION_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_TEAM_HAS_EARLY_CELEBRATION, processing event...")
            PROCESS_FMMC_TEAM_HAS_EARLY_CELEBRATION( iCount )
		BREAK
		
        CASE SCRIPT_EVENT_FMMC_OBJECTIVE_COMPLETE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_OBJECTIVE_COMPLETE_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT, processing event...")
            PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT(iCount)
        BREAK
		
		CASE SCRIPT_EVENT_BLOCK_SAVE_EOM_CAR_BEEN_DELIVERED
			IF g_sBlockedEvents.bSCRIPT_EVENT_BLOCK_SAVE_EOM_CAR_BEEN_DELIVERED_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - PROCESS_SCRIPT_EVENT_BLOCK_SAVE_EOM_CAR_BEEN_DELIVERED, processing event...")
			PROCESS_SCRIPT_EVENT_BLOCK_SAVE_EOM_CAR_BEEN_DELIVERED(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_INVESTIGATE_CONTAINER
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_INVESTIGATE_CONTAINER_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_INVESTIGATE_CONTAINER, processing event...")
			PROCESS_FMMC_CONTAINER_BEING_INVESTIGATED( iCount )
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_VIP_MARKER_POSITION
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_VIP_MARKER_POSITION_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_VIP_MARKER_POSITION, processing event...")
			PROCESS_FMMC_VIP_MARKER_POSITION( iCount )
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_REQUEST_BINBAG
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_REQUEST_BINBAG_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_REQUEST_BINBAG, processing event...")
			PROCESS_FMMC_REQUEST_BINBAG( iCount )
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_BINBAG_OWNER_CHANGED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_BINBAG_OWNER_CHANGED_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_BINBAG_OWNER_CHANGED, processing event...")
			PROCESS_FMMC_BINBAG_OWNER_CHANGE( iCount )
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_TRASH_DRIVE_ENABLE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_TRASH_DRIVE_ENABLE_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_TRASH_DRIVE_ENABLE, processing event...")
			PROCESS_FMMC_TRASH_DRIVE_ENABLE( iCount )
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PLAYER_SCORE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PLAYER_SCORE_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - PROCESS_FMMC_INCREMENT_REMOTE_PLAYER_SCORE, processing event...")
            PROCESS_FMMC_INCREMENT_REMOTE_PLAYER_SCORE(iCount)
        BREAK
		
		CASE SCRIPT_EVENT_FMMC_INCREMENTED_LOCAL_PLAYER_SCORE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_INCREMENTED_LOCAL_PLAYER_SCORE_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_INCREMENTED_LOCAL_PLAYER_SCORE, processing event...")
            PROCESS_FMMC_INCREMENTED_LOCAL_PLAYER_SCORE(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_OVERTIME_SCORED_SHARD
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_OVERTIME_SCORED_SHARD_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_OVERTIME_SCORED_SHARD, processing event...")
            PROCESS_FMMC_OVERTIME_SCORED_SHARD(iCount)
		BREAK		
		
		CASE SCRIPT_EVENT_FMMC_OVERTIME_PAUSE_RENDERPHASE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_OVERTIME_PAUSE_RENDERPHASE_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_OVERTIME_PAUSE_RENDERPHASE, processing event...")
            PROCESS_FMMC_OVERTIME_PAUSE_RENDERPHASE(iCount)
		BREAK		
		
		CASE SCRIPT_EVENT_FMMC_RUMBLE_TEAM_SCORES_SHARD
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_RUMBLE_TEAM_SCORES_SHARD_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_RUMBLE_TEAM_SCORES_SHARD, processing event...")
            PROCESS_FMMC_RUMBLE_TEAM_SCORES_SHARD(iCount)
		BREAK		
		
		CASE SCRIPT_EVENT_FMMC_DIALOGUE_LOOK
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_DIALOGUE_LOOK_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_DIALOGUE_LOOK, processing event...")
            PROCESS_FMMC_DIALOGUE_LOOK(iCount)
        BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_RETASK_HANDSHAKE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_RETASK_HANDSHAKE_EVENT_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_RETASK_HANDSHAKE, processing event...")
            PROCESS_FMMC_PED_RETASK_HANDSHAKE(iCount)
        BREAK
		
		CASE SCRIPT_EVENT_FMMC_EXIT_MISSION_MOC
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_EXIT_MISSION_MOC_BLOCKED
				BREAK
			ENDIF
			 PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_EXIT_MISSION_MOC, processing event...")
            PROCESS_FMMC_EXIT_MISSION_MOC(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_CCTV_SPOTTED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_CCTV_SPOTTED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_CCTV_SPOTTED, processing event...")
			PROCESS_FMMC_CCTV_SPOTTED(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_STOLEN_FLAG_SHARD
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_STOLEN_FLAG_SHARD_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_STOLEN_FLAG_SHARD, processing event...")
			PROCESS_FMMC_STOLEN_FLAG_SHARD(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_CAPTURED_FLAG_SHARD
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_CAPTURED_FLAG_SHARD_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - Mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_CAPTURED_FLAG_SHARD, processing event...")
			PROCESS_FMMC_CAPTURED_FLAG_SHARD(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_RETURNED_FLAG_SHARD
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_RETURNED_FLAG_SHARD_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - Mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_RETURNED_FLAG_SHARD, processing event...")
			PROCESS_FMMC_RETURNED_FLAG_SHARD(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_TAGGED_ENTITY
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_TAGGED_ENTITY_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_TAGGED_ENTITY, processing event...")
			PROCESS_FMMC_TAGGED_ENTITY(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_STOCKPILE_SOUND
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_STOCKPILE_SOUND_BLOCKED
				BREAK
			ENDIF
			PROCESS_FMMC_STOCKPILE_SOUND(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_LOCATE_CAPTURE_PLAYER_SCORE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_LOCATE_CAPTURE_PLAYER_SCORE
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_LOCATE_CAPTURE_PLAYER_SCORE, processing event...")
			PROCESS_FMMC_LOCATE_CAPTURE_PLAYER_SCORE(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_START_SYNC_LOCK_HACK_TIMER
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_START_SYNC_LOCK_HACK_TIMER_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_START_SYNC_LOCK_HACK_TIMER, processing event...")
            PROCESS_FMMC_START_SYNC_LOCK_HACK_TIMER(iCount)
        BREAK
		CASE SCRIPT_EVENT_HEIST_BEGIN_HACK_MINIGAME
			IF g_sBlockedEvents.bSCRIPT_EVENT_HEIST_BEGIN_HACK_MINIGAME_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_HEIST_BEGIN_HACK_MINIGAME, processing event...")
			PROCESS_HEIST_BEGIN_HACK_MINIGAME(iCount)
		BREAK
		CASE SCRIPT_EVENT_HEIST_END_HACK_MINIGAME
			IF g_sBlockedEvents.bSCRIPT_EVENT_HEIST_END_HACK_MINIGAME_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_HEIST_END_HACK_MINIGAME, processing event...")
			PROCESS_HEIST_END_HACK_MINIGAME(iCount)
		BREAK
		CASE SCRIPT_EVENT_HEIST_QUIET_KNOCKING
			IF g_sBlockedEvents.bSCRIPT_EVENT_HEIST_QUIET_KNOCKING_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_HEIST_QUIET_KNOCKING, processing event...")
			PROCESS_HEIST_QUIET_KNOCKING(iCount)
		BREAK
		CASE SCRIPT_EVENT_HEIST_PLAYER_IN_TURRET
			IF g_sBlockedEvents.bSCRIPT_EVENT_HEIST_PLAYER_IN_TURRET_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_HEIST_PLAYER_IN_TURRET, processing event...")
			PROCESS_HEIST_PLAYER_IN_TURRET(iCount)
		BREAK
		CASE SCRIPT_EVENT_FMMC_DELIVER_CUTSCENE_VEH
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_DELIVER_CUTSCENE_VEH_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_DELIVER_CUTSCENE_VEH, processing event...")
            PROCESS_FMMC_ADD_CUTSCENE_VEH(iCount)
        BREAK
		CASE SCRIPT_EVENT_NETWORK_INDEX_FOR_LATER_RULE
			IF g_sBlockedEvents.bSCRIPT_EVENT_NETWORK_INDEX_FOR_LATER_RULE_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_NETWORK_INDEX_FOR_LATER_RULE, processing event...")
            PROCESS_NETWORK_INDEX_FOR_LATER_RULE(iCount)
        BREAK		
		CASE SCRIPT_EVENT_FMMC_OBJECTIVE_MID_POINT
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_OBJECTIVE_MID_POINT_BLOCKED
				BREAK
			ENDIF
             PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_OBJECTIVE_MID_POINT, processing event...")
             PROCESS_FMMC_OBJECTIVE_MID_POINT(iCount)
        BREAK
		
		CASE SCRIPT_EVENT_FMMC_CUTSCENE_SPAWN_PROGRESS
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_CUTSCENE_SPAWN_PROGRESS_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - PROCESS_FMMC_CUTSCENE_SPAWN_PROGRESS, processing event...")
            PROCESS_FMMC_CUTSCENE_SPAWN_PROGRESS(iCount)
        BREAK
		
		CASE SCRIPT_EVENT_FMMC_PRI_FIN_MCS2_PLANE_NON_CRITICAL
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PRI_FIN_MCS2_PLANE_NON_CRITICAL_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PRI_FIN_MCS2_PLANE_NON_CRITICAL, processing event...")
            PROCESS_FMMC_PRI_FIN_MCS2_PLANE_NON_CRITICAL(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PRI_FIN_MCS2_PLANE_CLEANUP
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PRI_FIN_MCS2_PLANE_CLEANUP_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PRI_FIN_MCS2_PLANE_CLEANUP, processing event...")
            PROCESS_FMMC_PRI_FIN_MCS2_PLANE_CLEANUP(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_MOCAP_PLAYER_ANIMATION
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_MOCAP_PLAYER_ANIMATION_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - PROCESS_MOCAP_PLAYER_ANIMATION, processing event...")
            PROCESS_MOCAP_PLAYER_ANIMATION(iCount)
        BREAK
		
		CASE SCRIPT_EVENT_FMMC_CUTSCENE_PLAYER_REQUEST
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_CUTSCENE_PLAYER_REQUEST_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - PROCESS_CUTSCENE_PLAYER_REQUEST, processing event...")
            PROCESS_CUTSCENE_PLAYER_REQUEST(iCount)
        BREAK
		
		CASE SCRIPT_EVENT_FMMC_START_STREAMING_END_MOCAP
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_START_STREAMING_END_MOCAP_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - PROCESS_START_STREAMING_END_MOCAP_EVENT, processing event...")
			PROCESS_START_STREAMING_END_MOCAP_EVENT(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_LOWER_LIFT_WARP
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_LOWER_LIFT_WARP_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_LOWER_LIFT_WARP, processing event...")
        	SET_BIT(iBioLiftBitset, BIOLAB_BIT_RECEIVED_LOWER_LIFT_WARP_EVENT)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_UPPER_LIFT_WARP
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_UPPER_LIFT_WARP_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_UPPER_LIFT_WARP, processing event...")
        	SET_BIT(iBioLiftBitset, BIOLAB_BIT_RECEIVED_UPPER_LIFT_WARP_EVENT)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_APARTMENTWARP_CS_REQUEST
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_APARTMENTWARP_CS_REQUEST_BLOCKED
				BREAK
			ENDIF
		  	PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_APARTMENTWARP_CS_REQUEST, processing event...")
            PROCESS_FMMC_APARTMENTWARP_CS_REQUEST(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_APARTMENTWARP_CS_RELEASE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_APARTMENTWARP_CS_RELEASE_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_APARTMENTWARP_CS_RELEASE, processing event...")
            PROCESS_FMMC_APARTMENTWARP_CS_RELEASE(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_REMOVE_PED_FROM_GROUP
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_REMOVE_PED_FROM_GROUP_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_CUTSCENE_PED, processing event...")
			PROCESS_FMMC_REMOVE_PED_FROM_GROUP(iCount)
		BREAK
	ENDSWITCH
    SWITCH Details.Type		
		CASE SCRIPT_EVENT_FMMC_HEIST_LEADER_PLAY_COUNT
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_HEIST_LEADER_PLAY_COUNT_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_HEIST_LEADER_PLAY_COUNT, processing event...")
			PROCESS_FMMC_HEIST_LEADER_PLAY_COUNT(iCount)
		BREAK		
		
		CASE SCRIPT_EVENT_FMMC_SVM_LEADER_PLAY_COUNT
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_SVM_LEADER_PLAY_COUNT_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_SVM_LEADER_PLAY_COUNT, processing event...")
			PROCESS_FMMC_SVM_LEADER_PLAY_COUNT(iCount)
		BREAK		
		
		CASE SCRIPT_EVENT_FMMC_PED_SUCCESSFULLY_REMOVED_FROM_GROUP
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_SUCCESSFULLY_REMOVED_FROM_GROUP_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_SUCCESSFULLY_REMOVED_FROM_GROUP, processing event...")
			PROCESS_FMMC_PED_SUCCESSFULLY_REMOVED_FROM_GROUP(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_HEARING
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_HEARING_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_HEARING, processing event...")
			PROCESS_FMMC_PED_HEARING(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_SPOOKED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_SPOOKED_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_SPOOKED, processing event...")
			PROCESS_FMMC_PED_SPOOKED(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_UNSPOOKED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_UNSPOOKED_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_UNSPOOKED, processing event...")
			PROCESS_FMMC_PED_UNSPOOKED(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_SPOOK_HURTORKILLED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_SPOOK_HURTORKILLED_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_SPOOK_HURTORKILLED, processing event...")
			PROCESS_FMMC_PED_SPOOK_HURTORKILLED(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_GIVEN_SPOOKED_GOTO_TASK
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_GIVEN_SPOOKED_GOTO_TASK
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_GIVEN_SPOOKED_GOTO_TASK, processing event...")
			PROCESS_FMMC_PED_GIVEN_SPOOKED_GOTO_TASK(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_GIVEN_UNSPOOKED_GOTO_TASK
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_GIVEN_UNSPOOKED_GOTO_TASK
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_GIVEN_UNSPOOKED_GOTO_TASK, processing event...")
			PROCESS_FMMC_PED_GIVEN_UNSPOOKED_GOTO_TASK(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_UPDATE_TASKED_GOTO_PROGRESS
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_UPDATE_TASKED_GOTO_PROGRESS
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_UPDATE_TASKED_GOTO_PROGRESS, processing event...")
			PROCESS_FMMC_PED_UPDATE_TASKED_GOTO_PROGRESS(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_AGGROED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_AGGROED_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_AGGROED, processing event...")
			PROCESS_FMMC_PED_AGGROED(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_SPEECH_AGGROED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_SPEECH_AGGROED_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_SPEECH_AGGROED, processing event...")
			PROCESS_FMMC_PED_SPEECH_AGGROED(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_CANCEL_TASKS
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_CANCEL_TASKS_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_CANCEL_TASKS, processing event...")
			PROCESS_FMMC_PED_CANCEL_TASKS(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_SPEECH_AGRO_BITSET
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_SPEECH_AGRO_BITSET_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_SPEECH_AGRO_BITSET, processing event...")
			PROCESS_FMMC_PED_SPEECH_AGRO_BITSET(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_OPEN_DOOR
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_OPEN_DOOR_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_OPEN_DOOR, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_OPEN_DOOR(iCount)
		BREAK	
		
		CASE SCRIPT_EVENT_FMMC_SWAP_DOOR_FOR_DAMAGED_VERSION
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_SWAP_DOOR_FOR_DAMAGED_VERSION_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_SWAP_DOOR_FOR_DAMAGED_VERSION, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_SWAP_DOOR_FOR_DAMAGED_VERSION(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_DRILL_ASSET
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_DRILL_ASSET_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_DRILL_ASSET, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_DRILL_ASSET(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PICKED_UP_LIVES_PICKUP
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PICKED_UP_LIVES_PICKUP_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PICKED_UP_LIVES_PICKUP, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_PICKED_UP_LIVES_PICKUP(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PICKED_UP_POWERPLAY_POWERUP
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PICKED_UP_POWERPLAY_POWERUP_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PICKED_UP_POWERPLAY_POWERUP, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_POWERUP(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PICKED_UP_POWERPLAY_PICKUP
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PICKED_UP_POWERPLAY_PICKUP_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PICKED_UP_POWERPLAY_PICKUP, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_SHOW_REZ_TICKER
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_SHOW_REZ_TICKER_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_SHOW_REZ_TICKER, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_SHOW_REZ_TICKER(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_SHOW_REZ_SHARD
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_SHOW_REZ_SHARD_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_SHOW_REZ_SHARD, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_SHOW_REZ_SHARD(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP_ENDED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_POWERPLAY_PICKUP_ENDED_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP_ENDED, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_POWERPLAY_PICKUP_ENDED(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PP_PICKUP_PLAY_ANNOUNCER
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PP_PICKUP_PLAY_ANNOUNCER_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PP_PICKUP_PLAY_ANNOUNCER, processing event...")
			PROCESS_SCRIPT_EVENT_PP_PICKUP_PLAY_ANNOUNCER(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PICKED_UP_VEHICLE_WEAPON
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PICKED_UP_VEHICLE_WEAPON_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PICKED_UP_VEHICLE_WEAPON, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_PICKED_UP_VEHICLE_WEAPON(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_VEHICLE_WEAPON_SOUND
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_VEHICLE_WEAPON_SOUND_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_VEHICLE_WEAPON_SOUND, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_VEHICLE_WEAPON_SOUND(iCount)
		BREAK
				
		CASE SCRIPT_EVENT_FMMC_ACTIVATE_VEHICLE_WEAPON
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_ACTIVATE_VEHICLE_WEAPON_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_ACTIVATE_VEHICLE_WEAPON, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_ACTIVATE_VEHICLE_WEAPON(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_DETONATE_PROP
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_DETONATE_PROP_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_DETONATE_PROP, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_DETONATE_PROP(iCount)
		BREAK	
		CASE SCRIPT_EVENT_FMMC_CLAIM_PROP
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_CLAIM_PROP_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_CLAIM_PROP, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_CLAIM_PROP(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_TARGET_PROP_SHOT
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_TARGET_PROP_SHOT_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - Mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_TARGET_PROP_SHOT, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_TARGET_PROP_SHOT(iCount)
		BREAK

		CASE SCRIPT_EVENT_FMMC_COLLECTED_WEAPON
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_COLLECTED_WEAPON_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - Mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_COLLECTED_WEAPON, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_COLLECTED_WEAPON(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_RESPAWNED_WEAPON
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_RESPAWNED_WEAPON_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - Mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_RESPAWNED_WEAPON, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_RESPAWNED_WEAPON(iCount)
		BREAK		
		
		CASE SCRIPT_EVENT_FMMC_RESET_INDIVIDUAL_PROP
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_RESET_INDIVIDUAL_PROP_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_RESET_INDIVIDUAL_PROP, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_RESET_INDIVIDUAL_PROP(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PROP_CLAIMED_SUCCESSFULLY_BY_PART
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PROP_CLAIMED_SUCCESSFULLY_BY_PART_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PROP_CLAIMED_SUCCESSFULLY_BY_PART, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_PROP_CLAIMED_SUCCESSFULLY_BY_PART(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PROP_CONTESTED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PROP_CONTESTED_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PROP_CONTESTED, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_PROP_CONTESTED(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PROP_CLAIMED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PROP_CLAIMED_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PROP_CLAIMED, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_PROP_CLAIMED(iCount)
		BREAK

		CASE SCRIPT_EVENT_FMMC_TOGGLE_PROP_CLAIMING
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_TOGGLE_PROP_CLAIMING_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_TOGGLE_PROP_CLAIMING, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_TOGGLE_PROP_CLAIMING(iCount)
		BREAK
				
		CASE SCRIPT_EVENT_FMMC_CONDEMNED_DIED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_CONDEMNED_DIED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_CONDEMNED_DIED, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_CONDEMNED_DIED(iCount)
		BREAK
		
		#IF IS_DEBUG_BUILD
		CASE SCRIPT_EVENT_FMMC_PAUSE_ALL_TIMERS
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PAUSE_ALL_TIMERS_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PAUSE_ALL_TIMER, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_PAUSE_ALL_TIMERS(iCount)
		BREAK 
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		CASE SCRIPT_EVENT_FMMC_EDIT_ALL_TIMERS
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_EDIT_ALL_TIMERS_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_EDIT_ALL_TIMER, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_EDIT_ALL_TIMERS(iCount)
		BREAK 
		#ENDIF
		
		CASE SCRIPT_EVENT_FMMC_DECREMENT_REMAINING_TIME
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_DECREMENT_REMAINING_TIME
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION] - Mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_DECREMENT_REMAINING_TIME")
			PROCESS_SCRIPT_EVENT_FMMC_DECREMENT_REMAINING_TIME(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_START_PRE_COUNTDOWN_STOP
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_START_PRE_COUNTDOWN_STOP
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION] - Mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_START_PRE_COUNTDOWN_STOP")
			PROCESS_SCRIPT_EVENT_FMMC_START_PRE_COUNTDOWN_STOP(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_LOCATE_ALERT_SOUND
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_LOCATE_ALERT_SOUND
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION] - Mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_LOCATE_ALERT_SOUND")
			PROCESS_SCRIPT_EVENT_FMMC_LOCATE_ALERT_SOUND(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_CLEAR_HAS_BEEN_ON_SMALLEST_TEAM
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_DECREMENT_REMAINING_TIME
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION] - Mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_CLEAR_HAS_BEEN_ON_SMALLEST_TEAM")
			PROCESS_SCRIPT_EVENT_FMMC_CLEAR_HAS_BEEN_ON_SMALLEST_TEAM(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PROP_DESTROYED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PROP_DESTROYED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION] - Mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PROP_DESTROYED")
			PROCESS_SCRIPT_EVENT_FMMC_PROP_DESTROYED(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_RESET_ALL_PROPS
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_RESET_ALL_PROPS_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_RESET_ALL_PROPS, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_RESET_ALL_PROPS(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_SHOWDOWN_ELIMINATION
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_SHOWDOWN_ELIMINATION
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_SHOWDOWN_ELIMINATION, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_SHOWDOWN_ELIMINATION(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_SHARD_BOMBUSHKA_MINI_ROUND_END_EARLY
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_SHARD_BOMBUSHKA_MINI_ROUND_END_EARLY_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_SHARD_BOMBUSHKA_MINI_ROUND_END_EARLY, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_SHARD_BOMBUSHKA_MINI_ROUND_END_EARLY(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_TURF_WARS_TEAM_LEADING_CHANGED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_TURF_WARS_TEAM_LEADING_CHANGED_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_TURF_WARS_TEAM_LEADING_CHANGED, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_PROP_TURF_WARS_TEAM_LEADING_CHANGED(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PLAY_PULSE_SFX
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PLAY_PULSE_SFX
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PLAY_PULSE_SFX, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_PLAY_PULSE_SFX(iCount)
		BREAK

		CASE SCRIPT_EVENT_FMMC_BOMBUSHKA_NEW_ROUND_SHARD
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_BOMBUSHKA_NEW_ROUND_SHARD
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_BOMBUSHKA_NEW_ROUND_SHARD, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_BOMBUSHKA_NEW_ROUND_SHARD(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_SHOWDOWN_DAMAGE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_SHOWDOWN_DAMAGE
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_SHOWDOWN_DAMAGE, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_SHOWDOWN_DAMAGE(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_HARD_TARGET_NEW_TARGET_SHARD
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_HARD_TARGET_NEW_TARGET_SHARD
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_HARD_TARGET_NEW_TARGET_SHARD, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_HARD_TARGET_NEW_TARGET_SHARD(iCount)
		BREAK

		CASE SCRIPT_EVENT_FMMC_POWER_UP_STOLEN
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_POWER_UP_STOLEN_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_POWER_UP_STOLEN, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_POWER_UP_STOLEN(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_GHOST_STARTED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_GHOST_STARTED_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_GHOST_STARTED, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_GHOST_STARTED(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_SPAWN_PROTECTION_ALPHA
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_SPAWN_PROTECTION_ALPHA_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_SPAWN_PROTECTION_ALPHA, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_SPAWN_PROTECTION_ALPHA(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_AUDIO_TRIGGER_ACTIVATED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_AUDIO_TRIGGER_ACTIVATED_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_AUDIO_TRIGGER_ACTIVATED, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_AUDIO_TRIGGER_ACTIVATED(iCount)
		BREAK
				
		CASE SCRIPT_EVENT_FMMC_TURF_WAR_SCORED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_TURF_WAR_SCORED_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_TURF_WAR_SCORED, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_TURF_WAR_SCORED(iCount)
		BREAK	
		
		CASE SCRIPT_EVENT_FMMC_INITIATE_SEAT_SWAP_TEAM_VEHICLE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_INITIATE_SEAT_SWAP_TEAM_VEHICLE_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_INITIATE_SEAT_SWAP_TEAM_VEHICLE, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_INITIATE_SEAT_SWAP_TEAM_VEHICLE(iCount)
		BREAK
				
		CASE SCRIPT_EVENT_FMMC_LEFT_LOCATE_DELAY_RECAPTURE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_LEFT_LOCATE_DELAY_RECAPTURE
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_LEFT_LOCATE_DELAY_RECAPTURE, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_LEFT_LOCATE_DELAY_RECAPTURE(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_OVERTIME_ROUND_CHANGE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_OVERTIME_ROUND_CHANGE_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_OVERTIME_ROUND_CHANGE, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_OVERTIME_ROUND_CHANGE(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_OVERTIME_ROUND_CHANGE_TURNS
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_OVERTIME_ROUND_CHANGE_TURNS_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_OVERTIME_ROUND_CHANGE_TURNS, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_OVERTIME_ROUND_CHANGE_TURNS(iCount)
		BREAK	
		
		CASE SCRIPT_EVENT_FMMC_OVERTIME_RESET_TURNS_AND_SCORE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_OVERTIME_RESET_TURNS_AND_SCORE_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_OVERTIME_RESET_TURNS_AND_SCORE, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_OVERTIME_RESET_TURNS_AND_SCORE(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_OVERTIME_LANDING
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_OVERTIME_LANDING_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_OVERTIME_LANDING, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_OVERTIME_LANDING(iCount)
		BREAK	
		
		CASE SCRIPT_EVENT_FMMC_OVERTIME_VEHICLE_DESTROYED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_OVERTIME_VEHICLE_DESTROYED_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_OVERTIME_VEHICLE_DESTROYED, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_OVERTIME_VEHICLE_DESTROYED(iCount)
		BREAK	
		CASE SCRIPT_EVENT_FMMC_SUMO_ZONE_TIME_EXTEND_SHARD
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_SUMO_ZONE_TIME_EXTEND_SHARD_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_SUMO_ZONE_TIME_EXTEND_SHARD, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_SUMO_ZONE_TIME_EXTEND_SHARD(iCount)
		BREAK	
		CASE SCRIPT_EVENT_FMMC_KILLSTREAK_POWERUP_ACTIVATED_FREEZE_POS
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_KILLSTREAK_POWERUP_ACTIVATED_FREEZE_POS_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_KILLSTREAK_POWERUP_ACTIVATED_FREEZE_POS, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_KILLSTREAK_POWERUP_ACTIVATED_FREEZE_POS(iCount)
		BREAK		
		
		CASE SCRIPT_EVENT_FMMC_KILLSTREAK_POWERUP_ACTIVATED_TURF_TRADE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_KILLSTREAK_POWERUP_ACTIVATED_TURF_TRADE_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_KILLSTREAK_POWERUP_ACTIVATED_TURF_TRADE, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_KILLSTREAK_POWERUP_ACTIVATED_TURF_TRADE(iCount)
		BREAK	
		
		CASE SCRIPT_EVENT_FMMC_MANUAL_RESPAWN_TO_PASSENGERS
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_MANUAL_RESPAWN_TO_PASSENGERS_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_MANUAL_RESPAWN_TO_PASSENGERS, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_MANUAL_RESPAWN_TO_PASSENGERS(iCount)
		BREAK		
		
		CASE SCRIPT_EVENT_FMMC_CANT_DECREMENT_SCORE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_CANT_DECREMENT_SCORE_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_CANT_DECREMENT_SCORE, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_CANT_DECREMENT_SCORE(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_KILL_PLAYER_NOT_AT_LOCATE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_KILL_PLAYER_NOT_AT_LOCATE_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_KILL_PLAYER_NOT_AT_LOCATE, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_KILL_PLAYER_NOT_AT_LOCATE(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_TEAM_CHECKPOINT_PASSED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_TEAM_CHECKPOINT_PASSED_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_TEAM_CHECKPOINT_PASSED, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_TEAM_CHECKPOINT_PASSED(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PP_TEAM_INTRO_ROOT
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PP_TEAM_INTRO_ROOT_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PP_TEAM_INTRO_ROOT, processing event...")
			PROCESS_SCRIPT_EVENT_PP_SET_TEAM_INTRO_ROOT(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PP_PRE_INTRO_ROOT
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PP_PRE_INTRO_ROOT_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PP_PRE_INTRO_ROOT, processing event...")
			PROCESS_SCRIPT_EVENT_PP_SET_PRE_INTRO_ROOT(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PP_SUDDDEATH_ROOT
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PP_SUDDDEATH_ROOT_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PP_SUDDDEATH_ROOT, processing event...")
			PROCESS_SCRIPT_EVENT_PP_SET_SUDDDEATH_ROOT(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PP_WINNER_ROOT
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PP_WINNER_ROOT_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PP_WINNER_ROOT, processing event...")
			PROCESS_SCRIPT_EVENT_PP_SET_WINNER_ROOT(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PP_EXIT_ROOT
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PP_EXIT_ROOT_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PP_EXIT_ROOT, processing event...")
			PROCESS_SCRIPT_EVENT_PP_SET_EXIT_ROOT(iCount)
		BREAK
		#IF IS_DEBUG_BUILD
		CASE SCRIPT_EVENT_FMMC_PP_MANUAL_MUSIC_CHANGE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PP_MANUAL_MUSIC_CHANGE_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PP_MANUAL_MUSIC_CHANGE, processing event...")
			PROCESS_SCRIPT_EVENT_PP_MANUALLY_SET_MUSIC(iCount)
		BREAK
		#ENDIF
		
		CASE SCRIPT_EVENT_FMMC_IAO_SFX_PLAY
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_IAO_SFX_PLAY_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_IAO_SFX_PLAY, processing event...")
			PROCESS_SCRIPT_EVENT_IAO_PLAY_SOUND(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_TP_TRANSFORM_SFX
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_TP_TRANSFORM_SFX_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_TP_TRANSFORM_SFX, processing event...")
			PROCESS_SCRIPT_EVENT_TRADING_PLACES_TRANSFORM_SFX(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_SUDDEN_DEATH_REASON
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_SUDDEN_DEATH_REASON_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_SUDDEN_DEATH_REASON, processing event...")
			PROCESS_SCRIPT_EVENT_SUDDEN_DEATH_REASON(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_RUGBY_PLAY_SOUND
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_RUGBY_PLAY_SOUND_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_RUGBY_PLAY_SOUND, processing event...")
			PROCESS_SCRIPT_EVENT_RUG_PLAY_SOUND(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_DISTANCE_GAINED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_DISTANCE_GAINED_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_DISTANCE_GAINED, processing event...")
			PROCESS_SCRIPT_EVENT_DISTANCE_GAINED_TICKER(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_GRAB_CASH_DISPLAY_TAKE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_GRAB_CASH_DISPLAY_TAKE_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_GRAB_CASH_DISPLAY_TAKE, processing event...")
			PROCESS_SCRIPT_EVENT_GRAB_CASH_DISPLAY_TAKE(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_GRAB_CASH
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_GRAB_CASH_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_GRAB_CASH, processing event...")
			PROCESS_SCRIPT_EVENT_GRAB_CASH(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_GRAB_CASH_DROP_CASH
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_GRAB_CASH_DROP_CASH_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_GRAB_CASH_DROP_CASH, processing event...")
			PROCESS_SCRIPT_EVENT_GRAB_CASH_DROP_CASH(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_TREVOR_COVER_CONVERSATION_TRIGGERED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_TREVOR_COVER_CONVERSATION_TRIGGERED_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_TREVOR_COVER_CONVERSATION_TRIGGERED, processing event...")
			PROCESS_SCRIPT_EVENT_TREVOR_COVER_CONVERSTION_TRIGGERED(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_TEAM_HAS_FINISHED_CUTSCENE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_TEAM_HAS_FINISHED_CUTSCENE_BLOCKED
				BREAK
			ENDIF
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_TEAM_HAS_FINISHED_CUTSCENE, processing event...")
			PROCESS_TEAM_HAS_FINISHED_CUTSCENE( iCount )
		BREAK
		
		//Process the players weapon for a cut scene entent
		CASE SCRIPT_EVENT_FMMC_MY_WEAPON_DATA
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_MY_WEAPON_DATA_BLOCKED
				BREAK
			ENDIF
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_MY_WEAPON_DATA, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_MY_WEAPON_DATA( iCount )
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PRI_STA_IG2_AUDIOSTREAM
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PRI_STA_IG2_AUDIOSTREAM_BLOCKED
				BREAK
			ENDIF
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION]  - mission Controller - PROCESS_EVENTS - PROCESS_FMMC_PRI_STA_IG2_AUDIOSTREAM, processing event...")
			PROCESS_FMMC_PRI_STA_IG2_AUDIOSTREAM( iCount )
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_SPECTATOR_PLAY_POSTFX
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_SPECTATOR_PLAY_POSTFX_BLOCKED
				BREAK
			ENDIF
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_SPECTATOR_PLAY_POSTFX, processing event...")
			PROCESS_FMMC_SPECTATOR_POSTFX(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_OBJECTIVE_REVALIDATE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_OBJECTIVE_REVALIDATE_BLOCKED
				BREAK
			ENDIF
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_OBJECTIVE_REVALIDATE, processing event...")
			PROCESS_FMMC_OBJECTIVE_REVALIDATE(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_FORCING_EVERYONE_FROM_VEH
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_FORCING_EVERYONE_FROM_VEH_BLOCKED
				BREAK
			ENDIF
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_FORCING_EVERYONE_FROM_VEH, processing event...")
			PROCESS_FMMC_FORCING_EVERYONE_FROM_VEH(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_INCREMENT_TEAM_SCORE_RESURRECTION
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_INCREMENT_TEAM_SCORE_RESURRECTION_BLOCKED
				BREAK
			ENDIF
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_INCREMENT_TEAM_SCORE_RESURRECTION, processing event...")
			PROCESS_FMMC_INCREMENT_TEAM_SCORE_RESURRECTION(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_DISPLAY_CUSTOM_OBJECT_PICKUP_SHARD
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_DISPLAY_CUSTOM_OBJECT_PICKUP_SHARD_BLOCKED
				BREAK
			ENDIF
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_DISPLAY_CUSTOM_OBJECT_PICKUP_SHARD, processing event...")
			PROCESS_FMMC_DISPLAY_CUSTOM_OBJECT_PICKUP_SHARD(iCount)
		BREAK
		
		#IF IS_DEBUG_BUILD
		CASE SCRIPT_EVENT_JOB_INTRO
			IF g_sBlockedEvents.bSCRIPT_EVENT_JOB_INTRO_BLOCKED
				BREAK
			ENDIF
			PROCESS_JOB_INTRO_EVENT(iCount)
		BREAK
		#ENDIF
		
    ENDSWITCH
    SWITCH Details.Type	
		CASE SCRIPT_EVENT_FMMC_REMOTELY_ADD_RESPAWN_POINTS
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_REMOTELY_ADD_RESPAWN_POINTS
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_REMOTELY_ADD_RESPAWN_POINTS, processing event...")
			PROCESS_FMMC_REMOTELY_ADD_RESPAWN_POINTS(iCount)
		BREAK   
		CASE SCRIPT_EVENT_FMMC_SMASHED_BY_TOUCH
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_SMASHED_BY_TOUCH
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - PROCESS_SCRIPT_EVENT_FMMC_SMASHED_BY_TOUCH, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_SMASHED_BY_TOUCH(iCount)
		BREAK
		CASE SCRIPT_EVENT_FMMC_STEAL_OBJECT
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_STEAL_OBJECT
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - PROCESS_SCRIPT_EVENT_FMMC_STEAL_OBJECT, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_STEAL_OBJECT(iCount)
		BREAK
		CASE SCRIPT_EVENT_FMMC_INCREMENT_REMAINING_TIME
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_INCREMENT_REMAINING_TIME
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION] - Mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_INCREMENT_REMAINING_TIME")
			PROCESS_SCRIPT_EVENT_FMMC_INCREMENT_REMAINING_TIME(iCount)
		BREAK			
		CASE SCRIPT_EVENT_FMMC_BMBFB_EXPLODE_BALL
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_BMBFB_EXPLODE_BALL
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_BMBFB_EXPLODE_BALL, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_BMBFB_EXPLODE_BALL(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_BMBFB_GOAL
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_BMBFB_GOAL
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_BMBFB_GOAL, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_BMBFB_GOAL(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_ARENA_TRAP_ACTIVATED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_ARENA_TRAP_ACTIVATED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_ARENA_TRAP_ACTIVATED, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_ARENA_TRAP_ACTIVATED(iCount, MC_serverBD_2.sTrapInfo_Host, sTrapInfo_Local)
		BREAK
		CASE SCRIPT_EVENT_FMMC_ARENA_TRAP_DEACTIVATED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_ARENA_TRAP_DEACTIVATED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_ARENA_TRAP_DEACTIVATED, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_ARENA_TRAP_DEACTIVATED(iCount, MC_serverBD_2.sTrapInfo_Host, sTrapInfo_Local)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_ARENA_LANDMINE_TOUCHED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_ARENA_LANDMINE_TOUCHED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - bSCRIPT_EVENT_FMMC_ARENA_LANDMINE_TOUCHED, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_ARENA_LANDMINE_TOUCHED(iCount, MC_serverBD_2.sTrapInfo_Host)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_ARENA_ARTICULATED_JOINT
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_ARENA_ARTICULATED_JOINT, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_ARENA_ARTICULATED_JOINT(iCount, MC_serverBD_1.sFMMC_SBD.niDynoProps)
		BREAK
		
		
		CASE SCRIPT_EVENT_FMMC_ALPHA_CHANGE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_CHANGE_ALPHA_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - bSCRIPT_EVENT_FMMC_CHANGE_ALPHA_BLOCKED, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE(iCount)				
		BREAK		
			
		CASE SCRIPT_EVENT_FMMC_DELIVER_VEHICLE_STOP
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_DELIVER_VEHICLE_STOP
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - bSCRIPT_EVENT_FMMC_DELIVER_VEHICLE_STOP, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_DELIVER_VEHICLE_STOP(iCount)				
		BREAK		
						
		CASE SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - bSCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT(iCount)				
		BREAK		
		
		CASE SCRIPT_EVENT_FMMC_MOVE_FMMC_YACHT
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_MOVE_FMMC_YACHT, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_MOVE_FMMC_YACHT(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_NOTIFY_WANTED_SET_FOR_VEHICLE
			IF g_sBlockedEvents.bSCRIPT_EVENT_NOTIFY_WANTED_SET_FOR_VEHICLE_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[MJL][EVENT] - PROCESS_EVENTS - SCRIPT_EVENT_NOTIFY_WANTED_SET_FOR_VEHICLE, processing event...")
			PROCESS_SCRIPT_EVENT_NOTIFY_WANTED_SET_FOR_VEHICLE(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_CCTV_REACHED_DESTINATION
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_CCTV_REACHED_DESTINATION
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_CCTV_REACHED_DESTINATION, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_CCTV_REACHED_DESTINATION(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_METAL_DETECTOR_ALERTED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_METAL_DETECTOR_ALERTED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_METAL_DETECTOR_ALERTED, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_METAL_DETECTOR_ALERTED(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_INTERACT_WITH_EVENT
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_INTERACT_WITH_EVENT 
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_INTERACT_WITH_EVENT, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_INTERACT_WITH_EVENT(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_UPDATE_DOOR_LINKED_ENTITIES
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_UPDATE_DOOR_OBJS
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_UPDATE_DOOR_LINKED_ENTITIES, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_UPDATE_DOOR_LINKED_ENTITIES(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_ZONE_TIMER
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_ZONE_TIMER
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_ZONE_TIMER, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_ZONE_TIMER(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PHONE_EMP_EVENT
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PHONE_EMP_EVENT
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PHONE_EMP_EVENT, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_PHONE_EMP_EVENT(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_BLOCK_WANTED_CONE_RESPONSE_EVENT
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_BLOCK_WANTED_CONE_RESPONSE_EVENT
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_BLOCK_WANTED_CONE_RESPONSE_EVENT, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_BLOCK_WANTED_CONE_RESPONSE_EVENT(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_POISON_GAS
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_POISON_GAS_EVENT
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_POISON_GAS, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_POISON_GAS(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_DIRECTIONAL_DOOR_EVENT
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_DIRECTIONAL_DOOR_EVENT
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_DIRECTIONAL_DOOR_EVENT, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_DIRECTIONAL_DOOR_EVENT(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_TRIGGER_AGGRO_FOR_TEAM_LEGACY
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_TRIGGER_AGGRO_FOR_TEAM_LEGACY
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_TRIGGER_AGGRO_FOR_TEAM_LEGACY, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_TRIGGER_AGGRO_FOR_TEAM_LEGACY(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_UPDATE_DOOR_OFF_RULE_EVENT
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_UPDATE_DOOR_OFF_RULE_EVENT
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_UPDATE_DOOR_OFF_RULE_EVENT, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_UPDATE_DOOR_OFF_RULE_EVENT(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_UPDATE_MINIGAME_OFF_RULE_EVENT
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_UPDATE_MINIGAME_OFF_RULE_EVENT
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_UPDATE_MINIGAME_OFF_RULE_EVENT, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_UPDATE_MINIGAME_OFF_RULE_EVENT(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_COP_DECOY_EVENT
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_COP_DECOY_EVENT
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_COP_DECOY_EVENT, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_COP_DECOY_EVENT(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_COP_BEHAVIOUR_OVERRIDE_EVENT
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_COP_BEHAVIOUR_OVERRIDE_EVENT
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_COP_BEHAVIOUR_OVERRIDE_EVENT, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_COP_BEHAVIOUR_OVERRIDE_EVENT(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_THERMITE_EFFECT_EVENT
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_THERMITE_EFFECT_EVENT
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_THERMITE_EFFECT_EVENT, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_THERMITE_EFFECT_EVENT(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_VAULT_DRILL_EFFECT_EVENT
			IF g_sBlockedEvents.bSCRIPT_EVENT_VAULT_DRILL_EFFECT_EVENT
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_VAULT_DRILL_EFFECT_EVENT, processing event...")
			PROCESS_SCRIPT_EVENT_VAULT_DRILL_EFFECT_EVENT__FMMC(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_ACTIVATE_ELEVATOR
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_ACTIVATE_ELEVATOR
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_ACTIVATE_ELEVATOR, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_ACTIVATE_ELEVATOR(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_CALL_ELEVATOR
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_CALL_ELEVATOR
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_CALL_ELEVATOR, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_CALL_ELEVATOR(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_CASINO_UPDATED_STEAL_PAINTING_STATE
			IF g_sBlockedEvents.bSCRIPT_EVENT_CASINO_UPDATED_STEAL_PAINTING_STATE_BLOCKED
				EXIT
			ENDIF
			PRINTLN("[ML][MINIGAME][EVENT] - PROCESS_EVENTS - SCRIPT_EVENT_CASINO_UPDATED_STEAL_PAINTING_STATE, processing event...")
			PROCESS_SCRIPT_EVENT_CASINO_UPDATED_STEAL_PAINTING_STATE(iCount)
		BREAK
			
		CASE SCRIPT_EVENT_FMMC_SAFETY_DEPOSIT_BOX_LOOTED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_SAFETY_DEPOSIT_BOX_LOOTED_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_SAFETY_DEPOSIT_BOX_LOOTED, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_SAFETY_DEPOSIT_BOX_LOOTED(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_OCCUPY_SAFETY_DEPOSIT_BOX_CABINET
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_OCCUPY_SAFETY_DEPOSIT_BOX_CABINET_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_OCCUPY_SAFETY_DEPOSIT_BOX_CABINET, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_OCCUPY_SAFETY_DEPOSIT_BOX_CABINET(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_VAULT_DRILL_ASSET_REQUEST_EVENT
			IF g_sBlockedEvents.bSCRIPT_EVENT_VAULT_DRILL_ASSET_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_DRILL_ASSET, processing event...")
			PROCESS_SCRIPT_EVENT_VAULT_DRILL_ASSET(iCount)	
		BREAK
		
		CASE SCRIPT_EVENT_VAULT_DRILL_PROGRESS_EVENT
			IF g_sBlockedEvents.bSCRIPT_EVENT_VAULT_DRILL_PROGRESS_EVENT_BLOCKED
				BREAK
			ENDIF
			
			PROCESS_SCRIPT_EVENT_VAULT_DRILL_PROGRESS(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PAINTING_STOLEN
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PAINTING_STOLEN_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PAINTING_STOLEN, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_PAINTING_STOLEN(iCount)
		BREAK
		
	ENDSWITCH
ENDPROC




//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: AI EVENTS - only seeing dead peds right now, could be expanded !
//
//************************************************************************************************************************************************************



PROC PROCESS_FMMC_PED_SEEN_DEAD_PED(INT iCount)
	
	INT iPed
	STRUCT_DEAD_PED_SEEN sDeadPedSeen
		
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_AI,iCount,sDeadPedSeen,SIZE_OF(sDeadPedSeen))
		IF DOES_ENTITY_EXIST(sDeadPedSeen.FindingPedId)
		AND NOT IS_PED_INJURED(sDeadPedSeen.FindingPedId)
			
			iPed = IS_ENTITY_A_MISSION_CREATOR_ENTITY(sDeadPedSeen.FindingPedId)			
			IF iPed >= 0
			AND iPed < FMMC_MAX_PEDS
				INT iDeadPed = IS_ENTITY_A_MISSION_CREATOR_ENTITY(sDeadPedSeen.DeadPedId)
	
				IF iDeadPed >= 0
				AND iDeadPed < FMMC_MAX_PEDS
					PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_SEEN_DEAD_PED - ped ",iped, " has seen a dead ped (",iDeadPed,")! Setting bit.")
					SET_BIT(iPedSeenDeadBodyBitset[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
					iDeadPedIDSeen[iped] = NATIVE_TO_INT(sDeadPedSeen.DeadPedId)
				ELSE
					PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_SEEN_DEAD_PED - ped ",iped, " has seen a dead ped but it is not a creator entity - ", NATIVE_TO_INT(sDeadPedSeen.DeadPedId))
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_ENEMY_FIRED_PROJECTILE(INT iCount)
	
	STRUCT_NETWORK_FIRED_DUMMY_PROJECTILE_EVENT sEventInfo
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sEventInfo, SIZE_OF(sEventInfo))
		
		IF DOES_ENTITY_EXIST(sEventInfo.nFiredProjectileIndex)
			
			eiNearbyRocketsList[iExplodeNearbyRocketsListIndex] = sEventInfo.nFiredProjectileIndex
			iExplodeNearbyRocketsListIndex++
			PRINTLN("[EXPPROJ] PROCESS_ENEMY_FIRED_PROJECTILE - Grabbed missile from event! Adding to slot ", iExplodeNearbyRocketsListIndex, " || Projectile type: ", sEventInfo.nWeaponType)
		ELSE
			PRINTLN("[EXPPROJ] PROCESS_ENEMY_FIRED_PROJECTILE - Got event but entity doesn't exist! || Projectile type: ", sEventInfo.nWeaponType)
		ENDIF
		
		IF iExplodeNearbyRocketsListIndex >= ciNearbyRocketsListSize
			iExplodeNearbyRocketsListIndex = 0
		ENDIF
	ELSE
		PRINTLN("[EXPPROJ] PROCESS_ENEMY_FIRED_PROJECTILE - Couldn't get event data!")
	ENDIF
ENDPROC


//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: PROCESS EVENTS - main function, checks each event and calls the relevant processing system !
//
//************************************************************************************************************************************************************



// PURPOSE : Process all the events received in the events queue
PROC PROCESS_EVENTS()

	INT iCount
	EVENT_NAMES ThisScriptEvent
	STRUCT_STUNT_PERFORMED_EVENT seiStunt
	
	//process the events
	
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount
		
		ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)
		
		SWITCH ThisScriptEvent
			
			CASE EVENT_NETWORK_PLAYER_COLLECTED_PICKUP
				IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
					PROCESS_COLLECTED_PICKUP_EVENT(iCount)
				ENDIF
			BREAK

			CASE EVENT_NETWORK_PICKUP_RESPAWNED
				PROCESS_PICKUP_RESPAWN_SOUND(iCount)	
			BREAK
			
			CASE EVENT_NETWORK_SCRIPT_EVENT 
			
				PROCESS_FMMC_SCRIPT_EVENT(iCount)
				
			BREAK
			
			CASE EVENT_NETWORK_DAMAGE_ENTITY
			
				PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT(iCount)
				
			BREAK
			
			CASE EVENT_NETWORK_STUNT_PERFORMED
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciUSE_STUNT_RP_BONUS)
					GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, seiStunt, SIZE_OF(seiStunt))
					PRINTLN("[RCC MISSION] EVENT_NETWORK_STUNT_PERFORMED ")
					PROCESS_STUNT_RP_EVENT(INT_TO_ENUM(TrackedStuntType, seiStunt.m_StuntType), seiStunt.m_Value)
				ENDIF
			BREAK
			
			CASE EVENT_NETWORK_FIRED_DUMMY_PROJECTILE
				PROCESS_ENEMY_FIRED_PROJECTILE(iCount)
			BREAK
			
		ENDSWITCH
		
	ENDREPEAT
	
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_AI) iCount
		
		ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_AI, iCount)
		
		SWITCH ThisScriptEvent
			
			CASE EVENT_PED_SEEN_DEAD_PED
				
				PROCESS_FMMC_PED_SEEN_DEAD_PED(iCount)
				
			BREAK
			
		ENDSWITCH
		
	ENDREPEAT
	
ENDPROC
