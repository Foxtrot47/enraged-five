
USING "FM_Mission_Controller_USING.sch"
USING "FM_Mission_Controller_Utility.sch"
USING "FM_Mission_Controller_Entities.sch"



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: LOCAL PLAYER OBJECTIVE COMPLETION !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************

// Added for overtime mode.
PROC INCREMENT_LOCAL_PLAYER_SCORE_FLAT_AMOUNT(INT iAmount)
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
	AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet,SBBOOL_MISSION_OVER)
		INT iTeam = MC_playerBD[iLocalPart].iteam
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iteam]
		
		PRINTLN("[RCC MISSION][INCREMENT_LOCAL_PLAYER_SCORE_FLAT_AMOUNT]  current objective: ",							iRule)
		PRINTLN("[RCC MISSION][INCREMENT_LOCAL_PLAYER_SCORE_FLAT_AMOUNT]  player score: ",								MC_PlayerBD[iLocalPart].iPlayerScore)
		PRINTLN("[RCC MISSION][INCREMENT_LOCAL_PLAYER_SCORE_FLAT_AMOUNT]  objective score: ",							iAmount)
		PRINTLN("[RCC MISSION][INCREMENT_LOCAL_PLAYER_SCORE_FLAT_AMOUNT]  player team: ",								iTeam)
		
		MC_Playerbd[iLocalPart].iPlayerScore = MC_Playerbd[iLocalPart].iPlayerScore + iAmount
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciOVERTIME_WAIT_YOUR_TURN)				
			INT iCurrentRound = MC_serverBD_1.iCurrentRound
			INT iTarget = g_FMMC_STRUCT.iOvertimeRounds
			
			IF IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_OVERTIME_SUDDEN_DEATH_ROUNDS)
			OR IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_SUDDEN_DEATH_OVERTIME_RUMBLE)
				IF g_FMMC_STRUCT.iOvertimeSDRounds != ciOVERTIME_ROUNDS_UNLIMITED
					IF g_FMMC_STRUCT.iOvertimeRoundScale > 0
						iTarget = iTarget * (g_FMMC_STRUCT.iOvertimeRoundScale * GET_LARGEST_STARTING_TEAM())
					ENDIF
					iCurrentRound = iCurrentRound - iTarget
				ENDIF
			ENDIF
			
			IF iCurrentRound > -1
			AND iCurrentRound < MAX_PENALTY_PLAYERS
				MC_Playerbd[iLocalPart].iMyTurnScore[iCurrentRound] = iAmount
			ELSE
				PRINTLN("[LM][PROCESS_FMMC_OVERTIME_SCORED_SHARD] - WOULD HAVE OVERRUN (2) iCurrentRound: ", iCurrentRound, " iTarget", iTarget)
			ENDIF
		
			IF IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_OVERTIME_SUDDEN_DEATH_ROUNDS)
			OR IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_SUDDEN_DEATH_OVERTIME_RUMBLE)
				MC_PlayerBD[iLocalPart].iOvertimePoints += iAmount
			ENDIF
		ENDIF
		
		// We want to delay a shard so we need to cache this information.
		iDelayedOvertimeShardScore = iAmount
		iDelayedOvertimeShardTeam = iTeam
		iDelayedOvertimeShardRule = iRule
		iDelayedOvertimePartToMove = iLocalPart
		iDelayedOvertimeCurrentRound = MC_serverBD_1.iCurrentRound
		
		SET_BIT(iLocalBoolCheck22, LBOOL22_OVERTIME_DELAYED_SCORE_SHARD)
		
		IF IS_BIT_SET(iLocalBoolCheck23, LBOOL23_OVERTIME_READY_TO_GHOST)
			CLEAR_BIT(iLocalBoolCheck23, LBOOL23_OVERTIME_READY_TO_GHOST)
			PRINTLN("[LM][OVERTIME] - Clearing : LBOOL23_OVERTIME_READY_TO_GHOST")
		ENDIF
		
		PRINTLN("[LM][OVERTIME][RENDERPHASE_PAUSE_EVENT] - Turns Scored - Calling to pause renderphases.")
		
		BROADCAST_FMMC_OVERTIME_PAUSE_RENDERPHASE(MC_ServerBD.iSessionScriptEventKey)
		
		IF NOT IS_THIS_BOMB_FOOTBALL()
			BROADCAST_FMMC_INCREMENTED_LOCAL_PLAYER_SCORE(iDelayedOvertimeShardTeam, iDelayedOvertimeShardRule, iDelayedOvertimeShardScore, iDelayedOvertimePartToMove)
		ENDIF
	ENDIF
	
ENDPROC

PROC INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(INT imultiplier=1, BOOL bKillPoints = FALSE, INT iVictimPart = -1, INT iIdentifier = -1)
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
	AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet,SBBOOL_MISSION_OVER)
		IF GET_FMMC_POINTS_FOR_TEAM(MC_playerBD[iPartToUse].iteam) > 0
			INT iTeam = MC_playerBD[iPartToUse].iteam
			INT iRule = MC_serverBD_4.iCurrentHighestPriority[iteam]
			
			PRINTLN("[RCC MISSION] current objective: ",						iRule)
			PRINTLN("[RCC MISSION] player score: ",								MC_PlayerBD[iPartToUse].iPlayerScore) 
			PRINTLN("[RCC MISSION] objective score: ",							GET_FMMC_POINTS_FOR_TEAM(iTeam, iRule)) 
			PRINTLN("[RCC MISSION] player team: ",								iTeam) 

			IF CHECK_SUICIDE_DECREMENT_DOES_NOT_EXCEED_PREV_RULE(iRule, iTeam, iMultiplier)
				PRINTLN("[RCC MISSION][CHECK_SUICIDE_DECREMENT_DOES_NOT_EXCEED_PREV_RULE] We should exit the function as we shouldn't decrement score.") 
				EXIT
			ENDIF
			
			IF g_FMMC_STRUCT.iMissionEndType != ciMISSION_SCORING_TYPE_TIME
				PRINTLN("[RCC MISSION] Mission type is score") 			
				MC_Playerbd[iPartToUse].iPlayerScore= MC_Playerbd[iPartToUse].iPlayerScore + (GET_FMMC_POINTS_FOR_TEAM(MC_playerBD[iPartToUse].iteam)*imultiplier)
				IF bKillPoints
					MC_Playerbd[iPartToUse].iKillScore = MC_Playerbd[iPartToUse].iKillScore + (GET_FMMC_POINTS_FOR_TEAM(MC_playerBD[iPartToUse].iteam)*imultiplier)
				ENDIF
				BROADCAST_FMMC_INCREMENTED_LOCAL_PLAYER_SCORE(MC_playerBD[iPartToUse].iteam, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam], (GET_FMMC_POINTS_FOR_TEAM(MC_playerBD[iPartToUse].iteam)*imultiplier), iVictimPart, iIdentifier)
			ELSE
				PRINTLN("[RCC MISSION] Mission type is time") 
				MC_Playerbd[iPartToUse].tdMissionTime.Timer = GET_TIME_OFFSET(MC_Playerbd[iPartToUse].tdMissionTime.Timer, (ci_TIME_PER_POINT * 1000 * imultiplier * GET_FMMC_POINTS_FOR_TEAM(MC_playerBD[iPartToUse].iteam)))
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE: 
///  increments the local players medal objective completed count every time they pass an objective. 
///  Creator designer must set these off if they don't want the objective to count towards the medal calculation 
///  e.g. ghost objectives should not be counted. 
proc increment_medal_objective_completed(int team, int rule)
	IF rule > -1 AND rule < FMMC_MAX_RULES
		if not is_bit_set(g_FMMC_STRUCT.sFMMCEndConditions[team].iRuleBitsetThree[rule], ciBS_RULE3_DONT_COUNT_TOWARDS_SCORE)  
		
			MC_playerBD[iPartToUse].medal_objective_completed_count++

			PRINTLN("[RCC MISSION]increment_medal_objective_completed") 
			
		else 
		
			PRINTLN("[RCC MISSION]increment_medal_objective NOT PROCESSED") 
		
		endif 
	ENDIF
	
	PRINTLN("[RCC MISSION] increment_medal_objective_completed() been called") 
endproc 

PROC INCREMENT_LOCAL_PLAYER_SCORE_BY(INT ichange, BOOL bKill = FALSE, INT iIdentifier = -1)
	
	DEBUG_PRINTCALLSTACK()
	
	IF CHECK_SUICIDE_DECREMENT_DOES_NOT_EXCEED_PREV_RULE(MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam], MC_playerBD[iPartToUse].iteam, default, iChange)
		PRINTLN("[RCC MISSION][CHECK_SUICIDE_DECREMENT_DOES_NOT_EXCEED_PREV_RULE] We should exit the function as we shouldn't decrement score.") 
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT.iMissionEndType != ciMISSION_SCORING_TYPE_TIME
		PRINTLN("[RCC MISSION] Mission type is score- points directly altered by:",ichange) 
		MC_Playerbd[iPartToUse].iPlayerScore = MC_Playerbd[iPartToUse].iPlayerScore + ichange
		IF bKill
			MC_Playerbd[iPartToUse].iKillScore = MC_Playerbd[iPartToUse].iKillScore + ichange
		ENDIF
		BROADCAST_FMMC_INCREMENTED_LOCAL_PLAYER_SCORE(MC_playerBD[iPartToUse].iteam, FMMC_PRIORITY_IGNORE, ichange, DEFAULt, iIdentifier)
		
	ELSE
		PRINTLN("[RCC MISSION] Mission type is time- time directly altered by:",(ci_TIME_PER_POINT*1000*ichange)) 
		MC_Playerbd[iPartToUse].tdMissionTime.Timer =GET_TIME_OFFSET(MC_Playerbd[iPartToUse].tdMissionTime.Timer,(ci_TIME_PER_POINT*1000*ichange))
	ENDIF
	
ENDPROC

PROC INCREMENT_SERVER_REMOTE_PLAYER_SCORE(PLAYER_INDEX tempPlayer, INT iScore, INT iTeam, INT iPriority)
	DEBUG_PRINTCALLSTACK()
	IF g_FMMC_STRUCT.iMissionEndType != ciMISSION_SCORING_TYPE_TIME
		IF iTeam < FMMC_MAX_TEAMS
			IF iPriority < FMMC_MAX_RULES
			AND iPriority = MC_serverBD_4.iCurrentHighestPriority[iTeam]
				MC_serverBD.iScoreOnThisRule[iTeam] += iScore
				PRINTLN("[RCC MISSION] INCREMENT_SERVER_REMOTE_PLAYER_SCORE team: ",iteam," priority ",iPriority,", increased by ",iScore,",  total score ", MC_serverBD.iScoreOnThisRule[iTeam] )
			ENDIF
		ENDIF
	ENDIF
	
	BROADCAST_FMMC_INCREASE_REMOTE_PLAYER_SCORE(tempPlayer, iScore, MC_serverBD.iSessionScriptEventKey, GET_FRAME_COUNT())
	
ENDPROC

PROC SET_MISSION_TEST_COMPLETE()

	IF MC_playerBD[iPartToUse].iteam = 0
		PRINTLN("[RCC MISSION] CHECK_NEEDS_RETESTED - Setting team ", 0, " as tested")
		SET_BIT(g_fmmc_struct.biCTFTestComplete ,CTF_TEAM_1_TEST_COMPLETE)
	ELIF MC_playerBD[iPartToUse].iteam = 1
		PRINTLN("[RCC MISSION] CHECK_NEEDS_RETESTED - Setting team ", 1, " as tested")
		SET_BIT(g_fmmc_struct.biCTFTestComplete ,CTF_TEAM_2_TEST_COMPLETE)
	ELIF MC_playerBD[iPartToUse].iteam = 2
		PRINTLN("[RCC MISSION] CHECK_NEEDS_RETESTED - Setting team ", 2, " as tested")
		SET_BIT(g_fmmc_struct.biCTFTestComplete ,CTF_TEAM_3_TEST_COMPLETE)
	ELIF MC_playerBD[iPartToUse].iteam = 3
		PRINTLN("[RCC MISSION] CHECK_NEEDS_RETESTED - Setting team ", 3, " as tested")
		SET_BIT(g_fmmc_struct.biCTFTestComplete ,CTF_TEAM_4_TEST_COMPLETE)
	ENDIF

ENDPROC

PROC DISABLE_PLAYER_MISSION_VEHICLE_CONTROLS_THIS_FRAME(BOOL bDisableExit = TRUE, BOOL bEnableShooting = FALSE, BOOL bDisableHydraulics = FALSE )
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HANDBRAKE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_UD)
	IF bDisableExit
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
	ENDIF
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
	IF NOT bEnableShooting
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
	ENDIF
	IF bDisableHydraulics
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_HYDRAULICS_CONTROL_TOGGLE )
	ENDIF
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ROLL_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_PITCH_UD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_YAW_LEFT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_YAW_LEFT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_UP)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_DOWN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_UNDERCARRIAGE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_NEXT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_PREV_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_TARGET_LEFT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_TARGET_RIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_VERTICAL_FLIGHT_MODE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ROCKET_BOOST)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_BOOST)
ENDPROC

FUNC BOOL IS_CURRENT_SPECIAL_PICKUP_VALID()
	IF( sCurrentSpecialPickupState.iCurrentlyOwnedBinbag > -1 )
		RETURN IS_THIS_A_SPECIAL_PICKUP( sCurrentSpecialPickupState.iCurrentlyOwnedBinbag )
	ELIF( MC_playerBD[ iPartToUse ].iObjHacking > -1 )
		RETURN IS_THIS_A_SPECIAL_PICKUP( MC_playerBD[ iPartToUse ].iObjHacking )
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_SPECIAL_PICKUP_READY_FOR_DELIVERY( INT iObj = -1 )
	IF( iObj < -1 OR iObj >= FMMC_MAX_NUM_OBJECTS )
		ASSERTLN( "IS_SPECIAL_PICKUP_READY_FOR_DELIVERY - iObj: ", iObj, " is an invalid object index!" )
		DEBUG_PRINTCALLSTACK()
		RETURN FALSE
	ENDIF
	
	IF( iObj = -1 )
		RETURN FALSE
	ENDIF

	INT	iTeam = MC_playerBD[ iPartToUse ].iTeam
	IF( iTeam >= 0 AND iTeam < FMMC_MAX_TEAMS )
		MODEL_NAMES model = g_FMMC_STRUCT_ENTITIES.sPlacedObject[ iObj ].mn
		
		IF( model = HEI_PROP_HEI_TIMETABLE )
			IF( MC_serverBD.iObjHackPart[ iObj ] = iPartToUse )
				IF( MC_serverBD_4.iObjPriority[ iObj ][ iTeam ] <= MC_serverBD_4.iCurrentHighestPriority[ iTeam ] )
					IF( GET_CURRENT_SPECIAL_PICKUP_STATE() = eSpecialPickupState_WAIT_FOR_DELIVERY )
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ELIF( model = HEI_PROP_HEIST_BINBAG )
			IF( iObj = sCurrentSpecialPickupState.iCurrentlyOwnedBinbag )
				IF( GET_CURRENT_SPECIAL_PICKUP_STATE() = eSpecialPickupState_WAIT_FOR_DELIVERY )
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: DRAWING DROP OFFS / LOCATIONS !
//
//************************************************************************************************************************************************************



// The below section is used by all Locations and Drop-Off markers
// to fade in/out when they become valid/invalid
// Author: Owain Davies

ENUM eLocateFadeOutState
	eLocateFadeOutState_BEFORE_FADE_IN,
	eLocateFadeOutState_FADING_IN,
	eLocateFadeOutState_DISPLAYING,
	eLocateFadeOutState_FADING_OUT,
	eLocateFadeOutState_DO_DISTANCE_FADING,
	eLocateFadeOutState_AFTER_FADE_OUT
ENDENUM

// Each locate_fade_out_data struct begins in BEFORE_FADE_IN until they are told to fade in
// then the UPDATE function will fade them in until they are DISPLAYING
// They will stay in the DISPLAYING state until told to FADE OUT
// after which the UPDATE function will fade them out until they are AFTER_FADE_OUT
// 
// This means that locates that have already faded out will no longer be faded in again unless
// requested 

STRUCT locate_fade_out_data
	INT iAlphaValue = -1
	eLocateFadeOutState eState = eLocateFadeOutState_BEFORE_FADE_IN
ENDSTRUCT

locate_fade_out_data sFadeOutLocations[ FMMC_MAX_GO_TO_LOCATIONS ]

/// PURPOSE: This function does all of the updates that are necessary for fading in/out coronas
///    
/// PARAMS:
///    toUpdate - This is the struct of the locate/corona you want to fade in/out
///    iDebugIndex - This is for debug output - the index of the corona/locate
///    iDebugTeam - This is for debug output - the index of the team who triggered this change
PROC UPDATE_LOCATE_FADE_STRUCT( locate_fade_out_data& toUpdate,
								INT iDebugIndex = -1,
								INT iDebugTeam = -1 )
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule < FMMC_MAX_RULES
	
		IF NOT g_bMissionEnding
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciDONT_REMOVE_LOCATES)
			OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iRule].iLocBS2, ciLoc_BS2_DontFadeWhenInside)
				IF toUpdate.eState = eLocateFadeOutState_FADING_OUT
					toUpdate.eState = eLocateFadeOutState_DISPLAYING
					toUpdate.iAlphaValue = 255
					EXIT
				ENDIF
			ENDIF
		ENDIF
		
		
		// Fade the locates in once they appear
		IF toUpdate.eState = eLocateFadeOutState_FADING_IN
			toUpdate.iAlphaValue = FLOOR( toUpdate.iAlphaValue +@ ( 255.0 * 3.5 ) )

			IF toUpdate.iAlphaValue >= 255
				toUpdate.iAlphaValue = 255
				toUpdate.eState = eLocateFadeOutState_DISPLAYING
				
				IF iDebugIndex > -1 AND iDebugTeam > -1
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Locate ", iDebugIndex, " for team ", iDebugTeam, " has fully reappeared." )
				ENDIF
			ENDIF
		ENDIF
		
		// Draw the locates even after we're done with them
		IF toUpdate.eState = eLocateFadeOutState_FADING_OUT
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_TINY_RACERS_CHECKPOINT_LOGIC)
				toUpdate.iAlphaValue = 0
			ELSE
				toUpdate.iAlphaValue = FLOOR( toUpdate.iAlphaValue -@ ( 255.0 * 3.5 ) )
			ENDIF
			
			IF toUpdate.iAlphaValue <= 0
				toUpdate.iAlphaValue = 0
				toUpdate.eState = eLocateFadeOutState_AFTER_FADE_OUT
				
				IF iDebugIndex > -1 AND iDebugTeam > -1
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Locate ", iDebugIndex, " for team ", iDebugTeam, " has fully disappeared" )
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// Tells the locate that we wish it to fade in now
PROC SET_LOCATE_TO_FADE_IN( locate_fade_out_data& toFadeOut,
							INT iDebugIndex = -1,
							INT iDebugTeam = -1)
	IF toFadeOut.eState = eLocateFadeOutState_BEFORE_FADE_IN		
		toFadeOut.eState = eLocateFadeOutState_FADING_IN // Set this locate to fade in
		toFadeOut.iAlphaValue = 0
		
		IF iDebugIndex > -1 AND iDebugTeam > -1
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Locate ", iDebugIndex, " for team ", iDebugTeam, " is now reappearing." )
		ENDIF
	ENDIF
ENDPROC

// Tells the locate that we wish it to fade out now
PROC SET_LOCATE_TO_FADE_OUT( locate_fade_out_data& toFadeOut,
							INT iDebugIndex = -1,
							INT iDebugTeam = -1)
	IF toFadeOut.eState = eLocateFadeOutState_DISPLAYING
		toFadeOut.eState = eLocateFadeOutState_FADING_OUT // Set this locate to fade out
		toFadeOut.iAlphaValue = 255
		
		IF iDebugIndex > -1 AND iDebugTeam > -1
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Locate ", iDebugIndex, " for team ", iDebugTeam, " is now disappearing." )
		ENDIF
	ENDIF

ENDPROC

// Instantly sets this locate to faded out after displaying
PROC SET_LOCATE_TO_NOT_DISPLAY( locate_fade_out_data& toNotDisplay,
								INT iDebugIndex = -1,
								INT iDebugTeam = -1)					
	IF toNotDisplay.eState != eLocateFadeOutState_AFTER_FADE_OUT
		toNotDisplay.eState = eLocateFadeOutState_AFTER_FADE_OUT
		toNotDisplay.iAlphaValue = 0
		
		IF iDebugIndex > -1 AND iDebugTeam > -1
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Locate ", iDebugIndex, " for team ", iDebugTeam, " set to not display." )
		ENDIF
	ENDIF
ENDPROC

// Instantly sets this locate to full opacity and to the displaying state
PROC SET_LOCATE_TO_DISPLAY( locate_fade_out_data& toDisplay,
								INT iDebugIndex = -1,
								INT iDebugTeam = -1)
	IF toDisplay.eState != eLocateFadeOutState_DISPLAYING
		toDisplay.eState = eLocateFadeOutState_DISPLAYING
		toDisplay.iAlphaValue = 255
		
		IF iDebugIndex > -1 AND iDebugTeam > -1
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Locate ", iDebugIndex, " for team ", iDebugTeam, " set to display." )
		ENDIF
	ENDIF
ENDPROC

// Reset the locate_fade_out_data state back to before having faded in
PROC RESET_LOCATE(  INT iloc,
					locate_fade_out_data& toDisplay,
				   	INT iDebugTeam = -1)
	toDisplay.eState = eLocateFadeOutState_BEFORE_FADE_IN
	toDisplay.iAlphaValue = -1
	
	IF IS_BIT_SET(iCheckpointBS, iloc)
		DELETE_CHECKPOINT(ciLocCheckpoint[iloc])
		CLEAR_BIT(iCheckpointBS, iloc)
	ENDIF
	
	IF iloc > -1 AND iDebugTeam > -1
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Locate ", iloc, " for team ", iDebugTeam, " has been reset." )
	ENDIF
ENDPROC

// Tells the locate that we wish it to fade in now
PROC SET_LINE_LOCATE_TO_FADE_IN( locate_fade_out_data& toFadeOut,
							INT iDebugIndex = -1,
							INT iDebugTeam = -1)
	IF toFadeOut.eState = eLocateFadeOutState_BEFORE_FADE_IN		
		toFadeOut.eState = eLocateFadeOutState_FADING_IN // Set this locate to fade in
		
		IF iDebugIndex > -1 AND iDebugTeam > -1
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Line Marker ", iDebugIndex, " for team ", iDebugTeam, " is now fading in." )
		ENDIF
	ENDIF
ENDPROC

// Tells the locate that we wish it to fade out now
PROC SET_LINE_LOCATE_TO_FADE_OUT( locate_fade_out_data& toFadeOut,
							INT iDebugIndex = -1,
							INT iDebugTeam = -1)
	IF toFadeOut.eState = eLocateFadeOutState_DISPLAYING
		toFadeOut.eState = eLocateFadeOutState_FADING_OUT // Set this locate to fade out
		
		IF iDebugIndex > -1 AND iDebugTeam > -1
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Line Marker ", iDebugIndex, " for team ", iDebugTeam, " is now fading out." )
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE: This function does all of the updates that are necessary for fading in/out coronas
///    
/// PARAMS:
///    toUpdate - This is the struct of the locate/corona you want to fade in/out
///    iDebugIndex - This is for debug output - the index of the corona/locate
///    iDebugTeam - This is for debug output - the index of the team who triggered this change
PROC UPDATE_LOCATE_FADE_STRUCT_FOR_LINE_MARKER( locate_fade_out_data& toUpdate,
								INT iDebugIndex = -1,
								INT iDebugTeam = -1, INT iDistance = -1 )
//	IF iDistance <= 10
	toUpdate.eState = eLocateFadeOutState_DO_DISTANCE_FADING
//	ENDIF

	//IF toUpdate.eState = eLocateFadeOutState_DO_DISTANCE_FADING
		toUpdate.iAlphaValue = CEIL(255 - ((iDistance - 50.0) * -4.5))
		PRINTLN( "[RCC MISSION][AW LINE] toUpdate.iAlphaValue:," ,toUpdate.iAlphaValue)
	//ENDIF

	IF toUpdate.iAlphaValue >= 255
		toUpdate.iAlphaValue = 255	
		//toUpdate.eState = eLocateFadeOutState_DISPLAYING
		IF iDebugIndex > -1 AND iDebugTeam > -1
			PRINTLN( "[RCC MISSION][AW LINE] Locate ", iDebugIndex, " for team ", iDebugTeam, " has fully faded in." )
		ENDIF
	ENDIF

	IF toUpdate.iAlphaValue <= 30
		toUpdate.iAlphaValue = 30
		IF iDebugIndex > -1 AND iDebugTeam > -1
			PRINTLN( "[RCC MISSION][AW LINE]] Locate ", iDebugIndex, " for team ", iDebugTeam, " has disappeared" )
		ENDIF
	ENDIF
	
ENDPROC

CONST_INT OUT_OF_RANGE_TIME 750
FUNC BOOL ARE_ALL_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE(INT iTeam)
	
	IF NOT IS_BIT_SET(MC_serverbd.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
		RETURN TRUE
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(tdPassOutOfRangeTimer)
		IF HAS_NET_TIMER_EXPIRED(tdPassOutOfRangeTimer, OUT_OF_RANGE_TIME)
			RESET_NET_TIMER(tdPassOutOfRangeTimer)
		ELSE
		PRINTLN("[RCC MISSION] ARE_ALL_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE - Returning TRUE as out of range for under ", OUT_OF_RANGE_TIME, " ms")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck18, LBOOL18_TEAM_MEMBER_REQDIST_PASSED_THIS_FRAME)
		IF NOT IS_BIT_SET(iLocalBoolCheck18, LBOOL18_TEAM_MEMBER_REQDIST_PASSED_LAST_FRAME)
			SET_BIT(iLocalBoolCheck18, LBOOL18_TEAM_MEMBER_REQDIST_PASSED_LAST_FRAME)
		ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck18, LBOOL18_TEAM_MEMBER_REQDIST_CHECKED_THIS_FRAME)
		IF IS_BIT_SET(iLocalBoolCheck18, LBOOL18_TEAM_MEMBER_REQDIST_PASSED_LAST_FRAME)
			REINIT_NET_TIMER(tdPassOutOfRangeTimer)
			CLEAR_BIT(iLocalBoolCheck18, LBOOL18_TEAM_MEMBER_REQDIST_PASSED_LAST_FRAME)
			RETURN TRUE
		ENDIF
		RETURN FALSE
	ENDIF
	
	INT iPart
	INT iPlayersChecked
	
	PARTICIPANT_INDEX tempPart
	
	
	SET_BIT(iLocalBoolCheck18, LBOOL18_TEAM_MEMBER_REQDIST_CHECKED_THIS_FRAME)
	
	IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitset3, PBBOOL3_ALL_TEAM_MEMBERS_WITHIN_REQDIST)
		PRINTLN("[RCC MISSION] ARE_ALL_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE - Returning FALSE as local part ",iPartToUse," doesn't have all team members near them")
		IF IS_BIT_SET(iLocalBoolCheck18, LBOOL18_TEAM_MEMBER_REQDIST_PASSED_LAST_FRAME)
			REINIT_NET_TIMER(tdPassOutOfRangeTimer)
			CLEAR_BIT(iLocalBoolCheck18, LBOOL18_TEAM_MEMBER_REQDIST_PASSED_LAST_FRAME)
			RETURN TRUE
		ENDIF
		RETURN FALSE
	ENDIF
	
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF iPart != iPartToUse
			IF MC_playerBD[iPart].iteam = iTeam
				
				tempPart = INT_TO_PARTICIPANTINDEX(iPart)
				
				IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
					
					IF (NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_ANY_SPECTATOR)
						OR IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_HEIST_SPECTATOR))
					AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_PLAYER_FAIL)
						
						IF IS_BIT_SET(MC_playerBD[iPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
							IF NOT IS_BIT_SET( MC_playerBD[iPart].iClientBitSet, PBBOOL_FINISHED )
								
								iPlayersChecked++
								
								IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitset3, PBBOOL3_ALL_TEAM_MEMBERS_WITHIN_REQDIST)
									PRINTLN("[RCC MISSION] ARE_ALL_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE - Returning FALSE as part ",iPart," doesn't have all team members near them")
									IF IS_BIT_SET(iLocalBoolCheck18, LBOOL18_TEAM_MEMBER_REQDIST_PASSED_LAST_FRAME)
										REINIT_NET_TIMER(tdPassOutOfRangeTimer)
										CLEAR_BIT(iLocalBoolCheck18, LBOOL18_TEAM_MEMBER_REQDIST_PASSED_LAST_FRAME)
										RETURN TRUE
									ENDIF
									RETURN FALSE
								ENDIF
								
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
				
			ENDIF
			
		ELSE
			iPlayersChecked++
		ENDIF
		
		IF iPlayersChecked >= MC_serverBD.iNumberOfPlayingPlayers[iTeam]
			BREAKLOOP
		ENDIF
	ENDFOR
	
	SET_BIT(iLocalBoolCheck18, LBOOL18_TEAM_MEMBER_REQDIST_PASSED_THIS_FRAME)
	
	IF NOT IS_BIT_SET(iLocalBoolCheck18, LBOOL18_TEAM_MEMBER_REQDIST_PASSED_LAST_FRAME)
		SET_BIT(iLocalBoolCheck18, LBOOL18_TEAM_MEMBER_REQDIST_PASSED_LAST_FRAME)
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE: Draws the aerial checkpoint flag
PROC DRAW_AIR_MARKER( VECTOR vcheckPoint, BOOL bdropoff, INT iloc = -1 ) 
	INT iR, iG, iB, iA
	INT ialpha = 155
	VECTOR vCheckpointRotation = <<0,0,0>>
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
	
	IF bdropoff
		IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitset[ iRule ], ciBS_RULE_HIDE_DROPOFF_CORONA )
			EXIT
		ENDIF
	ENDIF
	
	vCheckpointRotation = NORMALISE_VECTOR( GET_ENTITY_COORDS( LocalPlayerPed ) - vcheckPoint )
	
	IF NOT IS_PED_IN_FLYING_VEHICLE(LocalPlayerPed)
		//Stop checkpoints pointing towards the floor when picked up on foot or in a car.
		vCheckpointRotation.z = 0
	ENDIF
	
	GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)

	// Implementing the ability to have checkpoints face the direction they were placed in
	IF iLoc >= 0
		IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].iLocBS , ciLoc_BS_RotateManual )
			INT iCurrentTeam = MC_playerBD[ iPartToUse ].iTeam
		
			// Unfortunately this function gets called without iLoc being valid sometimes - need to guard against it
			vCheckpointRotation = GET_HEADING_AS_VECTOR( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].fHeading[ iCurrentTeam ] )
		ENDIF

		IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fTeamMembersNeededWithin > 0
		AND NOT ARE_ALL_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE(iTeam)
			GET_HUD_COLOUR(HUD_COLOUR_RED, iR, iG, iB, iA)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS3, ciLoc_BS3_UseBlipColourForCorona)
		AND g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.iBlipColour > 0
			GET_HUD_COLOUR(MC_GET_HUD_COLOUR_FROM_BLIP_COLOUR(GET_BLIP_COLOUR_FROM_CREATOR(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.iBlipColour)), iR, iG, iB, iA)
		ENDIF

		IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS, ciLoc_BS_FlagMarker )			
			DRAW_MARKER(MARKER_RING_FLAG, vcheckPoint, vCheckpointRotation, <<0,0,0>>, <<8,8,8>>,iR, iG, iB,ialpha, FALSE, FALSE)
			DRAW_MARKER(MARKER_RING, vcheckPoint, vCheckpointRotation, <<0,0,0>>, <<8,8,8>>,iR, iG, iB,ialpha, FALSE, FALSE)
		ELIF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS2, ciLoc_BS2_BeastFlagMarker )
			DRAW_MARKER(MARKER_BEAST, vcheckPoint, vCheckpointRotation, <<0,0,0>>, <<8,8,8>>,iR, iG, iB,ialpha, FALSE, FALSE)
			DRAW_MARKER(MARKER_RING, vcheckPoint, vCheckpointRotation, <<0,0,0>>, <<8,8,8>>,iR, iG, iB,ialpha, FALSE, FALSE)
		ELSE
			DRAW_MARKER(MARKER_RING, vcheckPoint, vCheckpointRotation, <<0,0,0>>, <<8,8,8>>,iR, iG, iB,ialpha, FALSE, FALSE)
		ENDIF
	ELSE
		DRAW_MARKER(MARKER_RING, vcheckPoint, vCheckpointRotation, <<0,0,0>>, <<8,8,8>>,iR, iG, iB,ialpha, FALSE, FALSE)
	ENDIF
	
ENDPROC

// KGM_CTF_MARKER: 5/12/13 - Setup the CTF Base Icon to be drawn as a ground projection
PROC SETUP_CTF_DROPOFF_GROUND_PROJECTION_AND_MARKER(VECTOR vPos)

	// Store that the marker is still required on this frame, and store the coords in case this is a new request
	m_sMarkerCTF.mctfKeepActiveThisFrame	= TRUE
	m_sMarkerCTF.mctfPosition				= vPos

ENDPROC

// KGM_CTF_MARKER: 5/12/13 - Maintain the CTF Base Icon to be drawn as a ground projection
// KGM NOTE: 6/12/13 - The ground projections don't appear if script tries to display them over 200m away (somewhere between 200m and 225m is the cutoff).
//						If it doesn't appear and the player then moves within 200m it still won't appear - script needs to try again.
PROC MAINTAIN_CTF_DROPOFF_GROUND_PROJECTION_AND_MARKER()

	// Ignore if nothing to do
	IF NOT (m_sMarkerCTF.mctfKeepActiveThisFrame)
	AND (m_sMarkerCTF.mctfStage = MARKER_NOT_ACTIVE)
		EXIT
	ENDIF
	

	TEXT_LABEL_31			theTXD				= "MPOnMissMarkers"
	TEXT_LABEL_31			theBaseIconCTF		= "Capture_The_Flag_Base_Icon"
	CHECKPOINT_TYPE			theCheckpointType	= CHECKPOINT_MP_LOCATE_1
	DECAL_RENDERSETTING_ID	theDecalID			= DECAL_RSID_MP_LOCATE_1
	FLOAT					theRadius			= 5.0
	HUD_COLOURS				theColour			= GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_playerBD[iPartToUse].iteam, PlayerToUse)
	
	INT	theRed		= 0
	INT	theGreen	= 0
	INT	theBlue		= 0
	INT	theAlpha	= 0
	
	// Record if the marker is still required this frame before the flag gets cleared
	BOOL markerRequired = m_sMarkerCTF.mctfKeepActiveThisFrame
	
	// Clear the required flag so it can be set within DRAW_LOCATE_MARKER() if still required this frame
	//		(if still clear on the next frame then the marker is no longer needed and should be cleaned up)
	m_sMarkerCTF.mctfKeepActiveThisFrame = FALSE
	
	// KEITH 6/13/12: The player needs to be within 200m when the ground projection is activated or it won't appear, even if the player then moves within 200m
	CONST_FLOAT	GROUND_PROJECTION_ON_RANGE_m	180.0
	CONST_FLOAT GROUND_PROJECTION_OFF_RANGE_m	200.0
	
	BOOL playerWithinDisplayRange	= FALSE
	BOOL playerOutsideDisplayRange	= FALSE
	
	IF (markerRequired)
		IF bPedToUseOk
			FLOAT theDist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PlayerPedToUse, m_sMarkerCTF.mctfPosition)
			IF (theDist < GROUND_PROJECTION_ON_RANGE_m)
				playerWithinDisplayRange = TRUE
			ELIF (theDist > GROUND_PROJECTION_OFF_RANGE_m)
				playerOutsideDisplayRange = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	// Maintain the marker based on it's current stage and the latest information
	SWITCH (m_sMarkerCTF.mctfStage)
		CASE MARKER_NOT_ACTIVE
			// Marker stage is 'not active'
			// Is the marker now required?
			
			IF (markerRequired)
			AND (playerWithinDisplayRange)
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGMCTF: Marker Required and Player Within Display Range of ") NET_PRINT_FLOAT(GROUND_PROJECTION_ON_RANGE_m) NET_PRINT("m") NET_NL()
					NET_PRINT("           - Change of CTF Ground Projection from 'Not Active' to 'Request TXD' - Requesting TXD: ") NET_PRINT(theTXD) NET_NL()
				#ENDIF
				
				// Begin requesting the TXD and update the Marker Stage
				REQUEST_STREAMED_TEXTURE_DICT(theTXD)
				
				m_sMarkerCTF.mctfStage = MARKER_REQUEST_TXD
			ENDIF
			BREAK
			
		CASE MARKER_REQUEST_TXD
			// If the TXD is requested then keep requesting it until it is loaded regardless of the current 'required' state
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGMCTF: Requesting TXD: ")
				NET_PRINT(theTXD)
				NET_NL()
			#ENDIF
			
			REQUEST_STREAMED_TEXTURE_DICT(theTXD)
			
			IF (HAS_STREAMED_TEXTURE_DICT_LOADED(theTXD))
				// The TXD has loaded, if it is no longer required then just mark as not needed
				IF (markerRequired)
				AND (playerWithinDisplayRange)
					// Marker is still required and player is still within the display range, so create the checkpoint then patch the decal
					m_sMarkerCTF.mctfPosition.z += 1.0
					GET_GROUND_Z_FOR_3D_COORD(m_sMarkerCTF.mctfPosition, m_sMarkerCTF.mctfPosition.z)
					GET_HUD_COLOUR(theColour, theRed, theGreen, theBlue, theAlpha)
					m_sMarkerCTF.mctfCheckPointIndex = CREATE_CHECKPOINT(theCheckpointType, m_sMarkerCTF.mctfPosition, m_sMarkerCTF.mctfPosition, theRadius, theRed, theGreen, theBlue, theAlpha)
					PATCH_DECAL_DIFFUSE_MAP(theDecalID, theTXD, theBaseIconCTF)
					
					m_sMarkerCTF.mctfStage = MARKER_ON_DISPLAY
					
					#IF IS_DEBUG_BUILD
						NET_PRINT("...KGMCTF: TXD: ")
						NET_PRINT(theTXD)
						NET_PRINT(" has loaded - creating the checkpoint and patching the decal with this texture: ")
						NET_PRINT(theBaseIconCTF)
						NET_PRINT(" - setting stage to 'On Display' at these coords: ")
						NET_PRINT_VECTOR(m_sMarkerCTF.mctfPosition)
						NET_NL()
					#ENDIF
				ELSE
					// Marker is no longer needed, so just mark the TXD as not needed and clear the control variables
					SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(theTXD)
					m_sMarkerCTF.mctfKeepActiveThisFrame	= FALSE
					m_sMarkerCTF.mctfStage					= MARKER_NOT_ACTIVE
					
					#IF IS_DEBUG_BUILD
						NET_PRINT("...KGMCTF: TXD: ")
						NET_PRINT(theTXD)
						NET_PRINT(" has loaded but the TXD is no longer needed")
						IF NOT (markerRequired)
							NET_PRINT(" [MARKER NOT NEEDED]")
						ELSE
							NET_PRINT(" [PLAYER OUT OF RANGE]")
						ENDIF
						NET_PRINT(" - cleaning up and setting stage back to 'not active'")
						NET_NL()
					#ENDIF
				ENDIF
			ENDIF		
			BREAK
			
		CASE MARKER_ON_DISPLAY
			// Nothing to do if the marker is on display and it is still required, otherwise delete the checkpoint, unpatch the decal, and cleanup
			// KEITH UPDATE 6/12/13: The Snow on the Ground for the XMAS Present DLC hides the projection. I'm going to draw a corona if teh weather type is 'Xmas'
			IF (GET_PREV_WEATHER_TYPE_HASH_NAME() = HASH("XMAS"))
				GET_HUD_COLOUR(theColour, theRed, theGreen, theBlue, theAlpha)
				DRAW_MARKER(MARKER_CYLINDER, m_sMarkerCTF.mctfPosition, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>, <<theRadius,theRadius,MATC_CORONA_INACTIVE_HEIGHT_m>>, theRed, theGreen, theBlue, MATC_CORONA_INACTIVE_ALPHA)
			ENDIF
			
			IF NOT (markerRequired)
			OR (playerOutsideDisplayRange)
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGMCTF: TXD: ")
					NET_PRINT(theTXD)
					NET_PRINT(" is no longer required ")
					IF NOT (markerRequired)
						NET_PRINT(" [MARKER NO LONGER NEEDED]")
					ELSE
						NET_PRINT(" [PLAYER MOVED OUT OF RANGE]")
					ENDIF
					NET_NL()
					NET_PRINT("           - deleting checkpoint, unpatching decal, and clearing out control variables - set stage to 'not active'")
					NET_NL()
				#ENDIF
				
				DELETE_CHECKPOINT(m_sMarkerCTF.mctfCheckPointIndex)
				UNPATCH_DECAL_DIFFUSE_MAP(theDecalID)
				SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(theTXD)
				
				m_sMarkerCTF.mctfKeepActiveThisFrame	= FALSE
				m_sMarkerCTF.mctfCheckPointIndex		= NULL
				m_sMarkerCTF.mctfStage					= MARKER_NOT_ACTIVE
			ENDIF
			BREAK
	ENDSWITCH

ENDPROC

//Ideally this would be elsewhere, but it's needed in the function below to get the corona size and height
FUNC BOOL IS_CURRENT_VEHICLE_RULE_OF_TYPE_CARGOBOB_FOR_TEAM(INT iTeam, INT iRule)
	
	IF( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOff_Vehicle[ iRule ] > -1 )
		IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[  iTeam  ].iVehDropOff_HookUpBS,  iRule  )
		AND ( (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOff_Vehicle[ iRule ]].mn = CARGOBOB)
		OR (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOff_Vehicle[ iRule ]].mn = CARGOBOB2)
		OR (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOff_Vehicle[ iRule ]].mn = CARGOBOB3) )
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_PED_IN_REQUIRED_LOCATION_VEHICLE(PED_INDEX tempPed, INT iTeam, INT iLoc = -1)
	
	IF iLoc = -1 AND iTeam != -1
		iLoc = MC_serverBD.iPriorityLocation[iTeam]
	ENDIF
	
	IF (iLoc != -1 AND iTeam != -1)
	AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitset, PBBOOL_HEIST_SPECTATOR)
		IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam] < FMMC_MAX_RULES
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_SUB_TEAM_ROLES)
			AND MC_playerBD[iLocalPart].iCoronaRole != -1
			AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRolesBitset[MC_serverBD_4.iGotoLocationDataPriority[iLoc][iTeam]][MC_playerBD[iLocalPart].iCoronaRole], ciBS_ROLE_Cant_Complete_This_Rule)
				PRINTLN("IS_PED_IN_REQUIRED_LOCATION_VEHICLE - Sub Roles")
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].mnVehicleNeeded[iTeam] != DUMMY_MODEL_FOR_SCRIPT
			IF IS_PED_IN_ANY_VEHICLE(tempPed)
				IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_USING(tempPed)) = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].mnVehicleNeeded[iTeam]
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_NeedsCoronaVehicle)
			AND IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iTeamBitset, ciBS_TEAM_USES_CORONA_VEHICLE_LIST )
				IF IS_PED_IN_ANY_VEHICLE(tempPed)
				AND GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_USING(tempPed)) = g_mnCoronaMissionVehicle
					RETURN TRUE
				ENDIF
			ELSE
				RETURN TRUE
			ENDIF
		ENDIF
		
	ELSE
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitset, PBBOOL_HEIST_SPECTATOR)
			PRINTLN("IS_PED_IN_REQUIRED_LOCATION_VEHICLE - TRUE, PBBOOL_HEIST_SPECTATOR set")
		ENDIF
		RETURN TRUE
	ENDIF
	
	PRINTLN("IS_PED_IN_REQUIRED_LOCATION_VEHICLE - Nothing passed")
	
	RETURN FALSE
	
ENDFUNC

FUNC INT GET_VEHICLE_BEING_DROPPED_OFF(INT iTeam, INT iPartDroppingOff = -1)
	
	INT iveh = -1
	
	IF iPartDroppingOff != -1
		IF MC_playerBD[iPartDroppingOff].iteam = iteam
		AND MC_playerBD[iPartDroppingOff].iVehCarryCount > 0
		AND MC_playerBD[iPartDroppingOff].iVehNear != -1
			iveh = MC_playerBD[iPartDroppingOff].iVehNear
		ENDIF
	ENDIF
	
	IF iveh = -1
		IF MC_serverBD.iNumVehHighestPriority[iteam] = 1
			IF iAHighPriorityVehicle != -1
				iveh = iAHighPriorityVehicle
			ENDIF
		ELIF MC_serverBD.iNumVehHighestPriorityHeld[iteam] = 1
			INT iPart, iPlayersChecked
			INT iPlayersToCheck = MC_serverBD.iNumberOfPlayingPlayers[iteam]
			PARTICIPANT_INDEX tempPart
			
			FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
				IF MC_playerBD[iPart].iteam = iteam
					tempPart = INT_TO_PARTICIPANTINDEX(iPart)
					
					IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
						iPlayersChecked++
						
						IF MC_playerBD[iPart].iVehCarryCount > 0
						AND MC_playerBD[iPart].iVehNear != -1
							iveh = MC_playerBD[iPart].iVehNear
							iPart = MAX_NUM_MC_PLAYERS // Break out!
						ENDIF
					ENDIF
				ENDIF
				
				IF iPlayersChecked >= iPlayersToCheck
					iPart = MAX_NUM_MC_PLAYERS // Break out!
				ENDIF
			ENDFOR
		ELSE
			//Just try the first one we can find:
			iveh = iAHighPriorityVehicle
		ENDIF
	ENDIF
	
	RETURN iveh
	
ENDFUNC

PROC GET_CORONA_SIZE_AND_HEIGHT(FLOAT &fSize, FLOAT &fHeight, FLOAT fRadius, BOOL bdropoff, INT iloc)
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[ iTeam ]
	
	IF NOT bdropoff// locate
		IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iCoronaSize = LOCATION_CORONA_SIZE_SMALL
			fHeight = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fLocateHeight // LOCATE_SIZE_HEIGHT
			fSize = fradius/10
			IF fSize < 0.3
				fSize = 0.3
			ENDIF
			IF fSize > 1.0
				fSize = 1.0
			ENDIF
		ELIF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iCoronaSize = LOCATION_CORONA_SIZE_ACTUAL
			//exact
			fHeight = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fLocateHeight // LOCATE_SIZE_HEIGHT
			fSize = 2*fradius
			IF fSize < 0.3
				fSize = 0.3
			ENDIF
		ELIF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iCoronaSize = LOCATION_CORONA_SIZE_THREE_QUARTER
			fHeight = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fLocateHeight // LOCATE_SIZE_HEIGHT
			fSize = fradius
			IF fSize < 0.3
				fSize = 0.3
			ENDIF
		ELIF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iCoronaSize = LOCATION_CORONA_SIZE_CUSTOM
			fHeight = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fLocateHeight // LOCATE_SIZE_HEIGHT
			fSize = ((1 + g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fLocateCoronaScaler)*fradius)
			IF fSize < 0.5
				fSize = 0.5
			ENDIF
			
		ELIF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].iLocBS, ciLoc_BS_UseStandardSizeLocate )
			fHeight = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fLocateHeight 	// LOCATE_SIZE_HEIGHT
			fSize = 0.6 	// Default SP locate size
		ELIF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iSmallCorona[0] = 1 //small corona
			fHeight = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fLocateHeight // LOCATE_SIZE_HEIGHT
			fSize = fradius/10
			IF fSize < 0.3
				fSize = 0.3
			ENDIF
			IF fSize > 1.0
				fSize = 1.0
			ENDIF
		ELSE  // large corona
			fHeight = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fLocateHeight // LOCATE_SIZE_HEIGHT
			fSize = fradius/5
			IF fSize < 0.5
				fSize = 0.5
			ENDIF
			IF fSize > 5.0
				fSize = 5.0
			ENDIF
		ENDIF
	ELSE // drop off
		
		INT iveh = GET_VEHICLE_BEING_DROPPED_OFF(iteam, iPartToUse)
		IF iveh != -1
		AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fVehDropOffVisualRadius > 0
			fHeight = 1.5
			fSize = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fVehDropOffVisualRadius//*2
			IF fSize < 0.3
				fSize = 0.3
			ENDIF
		ELIF IS_CURRENT_VEHICLE_RULE_OF_TYPE_CARGOBOB_FOR_TEAM(iTeam, iRule)
			fHeight = 1.0
			fSize = fradius
		ELIF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetThree[ iRule ], ciBS_RULE3_UseStandardSizeLocate )
			fHeight = 1.0 	// LOCATE_SIZE_HEIGHT
			fSize = 0.6 	// Default SP locate size
		ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iSmallDropOffBitSet, iRule )
			//small corona
			fHeight = 1.0 // LOCATE_SIZE_HEIGHT
			fSize = fradius/10
			IF fSize < 0.3
				fSize = 0.3
			ENDIF
			IF fSize > 1.0
				fSize = 1.0
			ENDIF
		ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iExactDropOffBitSet, iRule )
			//exact
			fHeight = 1.0 // LOCATE_SIZE_HEIGHT
			fSize = 2*fradius
			IF fSize < 0.3
				fSize = 0.3
			ENDIF
		ELIF Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer) //This only gets hit when the locate is on water
			//exact
			fHeight = 1.0 // LOCATE_SIZE_HEIGHT
			fSize = 2*fradius
			IF fSize < 0.3
				fSize = 0.3
			ENDIF
		ELSE //normal
			fHeight = 1.0 // LOCATE_SIZE_HEIGHT
			fSize = fradius/5
			IF fSize < 0.5
				fSize = 0.5
			ENDIF
			IF fSize > 5.0
				fSize = 5.0
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC FLOAT GET_DIRECTION_FOR_LOCATE_MARKER(INT iLoc)
	FLOAT fThisDirection
	
	fThisDirection = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fDirection
	RETURN fThisDirection
ENDFUNC

FUNC BOOL HAS_CORONA_REACHED_NEW_STATE(INT i)

	IF corona_flare_data[i].fTransitionProgress  >= 1.0
		PRINTLN("AW SMOKE - HAS_CORONA_REACHED_NEW_STATE - TRUE")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC PROCESS_CORONA_TRANSITION(INT i)
	

	//if the current state isn't equal to the new state that has been set
	IF corona_flare_data[i].iFlareState !=  corona_flare_data[i].iNewState
		
		IF HAS_CORONA_REACHED_NEW_STATE(i)
			corona_flare_data[i].iFlareState =  corona_flare_data[i].iNewState
		ELSE
			IF corona_flare_data[i].fRed != corona_flare_data[i].fNextRed
				corona_flare_data[i].fRed   = LERP_FLOAT(corona_flare_data[i].fRed,corona_flare_data[i].fNextRed,corona_flare_data[i].fTransitionProgress)
			ENDIF
			IF corona_flare_data[i].fGreen != corona_flare_data[i].fNextGreen
				corona_flare_data[i].fGreen = LERP_FLOAT(corona_flare_data[i].fGreen,corona_flare_data[i].fNextGreen,corona_flare_data[i].fTransitionProgress)
			ENDIF
			IF corona_flare_data[i].fBlue != flare_data[i].fNextBlue
				corona_flare_data[i].fBlue  = LERP_FLOAT(corona_flare_data[i].fBlue,corona_flare_data[i].fNextBlue,corona_flare_data[i].fTransitionProgress)
			ENDIF
			PRINTLN("AW SMOKE - PROCESS_CORONA_TRANSITION - fTransitionProgress:", corona_flare_data[i].fTransitionProgress)
			corona_flare_data[i].fTransitionProgress = corona_flare_data[i].fTransitionProgress + 0.01
		ENDIF
		
		IF DOES_PARTICLE_FX_LOOPED_EXIST( ptfxFlareIndex[corona_flare_data[i].iPTFXval])
			PRINTLN("AW SMOKE - PROCESS_CORONA_TRANSITION - fRed:", corona_flare_data[i].fRed, " fGreen:", corona_flare_data[i].fGreen," fBlue:", flare_data[i].fBlue)
			SET_PARTICLE_FX_LOOPED_COLOUR(ptfxFlareIndex[corona_flare_data[i].iPTFXval],corona_flare_data[i].fRed, corona_flare_data[i].fGreen, corona_flare_data[i].fBlue ,TRUE)
		ENDIF
	ENDIF


ENDPROC

//iColour 0 - R | 1 - G | 2 - B
FUNC INT GET_CORONA_TRANSITION(INT i, INT iColour)
	
	INT iRGB
	
	IF iColour = 0 
		iRGB = ROUND(corona_flare_data[i].fRed * 255)
		PRINTLN("AW SMOKE - GET_CORONA_TRANSITION =RED ",iRGB )
	ENDIF
	IF iColour = 1 
		iRGB = ROUND(corona_flare_data[i].fGreen * 255)	 
		PRINTLN("AW SMOKE - GET_CORONA_TRANSITION =BLUE ",iRGB )
	ENDIF
	IF iColour = 2 
		iRGB = ROUND(corona_flare_data[i].fBlue * 255)
		PRINTLN("AW SMOKE - GET_CORONA_TRANSITION =GREEN ",iRGB )
	ENDIF
	
	RETURN iRGB
ENDFUNC
/// PURPOSE: This function draws the corona for a dropoff or location
///    If bDropoff is true, iLoc is not used
///    iAlphaOverride overrides the alpha value on this locate and has values of 0-255 or -1 if unused
PROC DRAW_LOCATE_MARKER( VECTOR vPos, FLOAT fradius, BOOL bdropoff, INT iloc, INT iAlphaOverride = -1, BOOL bEnemyMarker = FALSE, INT iTeamWhoOwnThis = -1 , BOOL bShowRugbyOrangeTeam = FALSE)
	
	HUD_COLOURS TeamColour
	
	//LINE MARKER NEEDS TO BE VISIBLE TO THE OTHER TEAM
	//IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].iLocBS, ciLoc_BS2_SetAsTeam2Marker )

	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[ iTeam ]
	FLOAT fDirection
	
	// 2055100 - Don't draw the marker if the objective is blocked
	IF IS_BIT_SET( MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER )
	AND NOT IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS2,ciLoc_BS2_UseLineMarker ) // Rob B - allow Cross the Line lines to display while out of bounds
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Enable_Turn_Rotor_Tag_Team)
		EXIT
	ENDIF
		
	// KGM_CTF_MARKER: 5/12/13 - Forward this to a different function to draw a ground projection and corona if it is a CTF dropoff marker
	IF (Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer))
		IF (bdropoff)
		
			IF iTeam !=-1
				IF( iOldRule != iRule )
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed,vPos,TRUE) <= 200
						
						FLOAT fWaterHeight,fGroundZ
						
						IF NOT GET_WATER_HEIGHT_NO_WAVES(vPos + <<0,0,1>>,fWaterHeight)
							fWaterHeight = -999
						ENDIF
						
						IF NOT GET_GROUND_Z_FOR_3D_COORD(vPos + <<0,0,1>>,fGroundZ)
							fGroundZ = -999
						ENDIF
						
						IF (fWaterHeight > fGroundZ)
							SET_BIT(iLocalBoolCheck3,LBOOL3_CTFLOC_UNDERWATER)
						ELSE
							CLEAR_BIT(iLocalBoolCheck3,LBOOL3_CTFLOC_UNDERWATER)
						ENDIF
						
						iOldRule = iRule
						
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(iLocalBoolCheck3,LBOOL3_CTFLOC_UNDERWATER)
				SETUP_CTF_DROPOFF_GROUND_PROJECTION_AND_MARKER(vPos)
				EXIT
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bdropoff
		IF( IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS,ciLoc_BS_HideMarker ) )
			EXIT
		ENDIF
	ELSE
		IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitset[ iRule ], ciBS_RULE_HIDE_DROPOFF_CORONA )
			EXIT
		ENDIF
	ENDIF
	
	IF NOT IS_PED_IN_REQUIRED_LOCATION_VEHICLE(PlayerPedToUse, iTeam, iLoc)
		EXIT
	ENDIF
	
	IF g_HeistApartmentDropoffPanStarted
	OR g_HeistGarageDropoffPanStarted
	OR NETWORK_IS_PLAYER_IN_MP_CUTSCENE(LocalPlayer)
		EXIT
	ENDIF
	
INT iR, iG, iB, iA
FLOAT fcoronaheight
FLOAT fcoronasize
	
	GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
		
	IF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_CONTROL_AREA
		IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_VFX_CAPTURE_OWNER)
			IF g_iTheWinningTeam = -1
				//Show Yellow = Tied
				GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
			ELSE
				TeamColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(g_iTheWinningTeam, PlayerToUse)
				//Team 0 - Orange
				//Team 1 - Green
				//Team 2 - Pink
				//Team 3 - Purple
				PRINTLN( "[RCC MISSION][AW SMOKE] TEAM ",g_iTheWinningTeam, "is winning")
				GET_HUD_COLOUR(TeamColour, iR, iG, iB, iA)
			ENDIF
		ENDIF
		IF iRule < FMMC_MAX_RULES
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_PROGRESS_VEHICLES_WITH_RESPAWNS)
				IF iTeamWhoOwnThis != -1
					GET_HUD_COLOUR(GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeamWhoOwnThis, PlayerToUse), iR, iG, iB, iA)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fTeamMembersNeededWithin > 0
	AND NOT ARE_ALL_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE(iTeam)
		GET_HUD_COLOUR(HUD_COLOUR_RED, iR, iG, iB, iA)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS3, ciLoc_BS3_UseBlipColourForCorona)
	AND g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.iBlipColour > 0
		GET_HUD_COLOUR(MC_GET_HUD_COLOUR_FROM_BLIP_COLOUR(GET_BLIP_COLOUR_FROM_CREATOR(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.iBlipColour)), iR, iG, iB, iA)
	ENDIF
	
	GET_CORONA_SIZE_AND_HEIGHT(fcoronasize, fcoronaheight,fradius,bdropoff,iloc)	
	
	IF iAlphaOverride > -1
	AND iAlphaOverride < 256
		iA = iAlphaOverride
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocateAlpha != -1
		iA = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocateAlpha
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS2,ciLoc_BS2_MakeMarkerMoreVisible)
		IF iRule < FMMC_MAX_RULES
			IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_TEAM_COLOURED_LOCATES)
				OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_ZONE(g_FMMC_STRUCT.iRootContentIDHash)
					iTeamWhoOwnThis = MC_serverBD.iLocOwner[iloc]
					
					// It doesn't matter if this sets a team twice, because we override that with the contested colour
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_INSTANT_COLOUR_CHANGES_FOR_INSIDE_LOCATE)
						INT i = 0
						FOR i = 0 TO FMMC_MAX_TEAMS-1
							IF MC_serverBD_1.inumberOfTeamInArea[iLoc][i] > 0
								iTeamWhoOwnThis = i
							ENDIF
						ENDFOR
					ENDIF
					
					IF iTeamWhoOwnThis > -1
						GET_HUD_COLOUR(GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeamWhoOwnThis, PlayerToUse), iR, iG, iB, iA)
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_CONTESTED_COLOUR_FOR_LOCATES)
					IF iTeamWhoOwnThis > -1
						INT iHostileTeamsInsideLocate
						INT i = 0
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_INSTANT_COLOUR_CHANGES_FOR_INSIDE_LOCATE)
							FOR i = 0 TO FMMC_MAX_TEAMS-1
								iHostileTeamsInsideLocate = -1
								IF MC_serverBD_1.inumberOfTeamInArea[iLoc][i] > 0
									iHostileTeamsInsideLocate++
								ENDIF
							ENDFOR
						ELSE			
							FOR i = 0 TO FMMC_MAX_TEAMS-1
								IF NOT DOES_TEAM_LIKE_TEAM(iTeamWhoOwnThis, i)
								AND iTeamWhoOwnThis != i
									IF MC_serverBD_1.inumberOfTeamInArea[iLoc][i] > 0
										iHostileTeamsInsideLocate++
									ENDIF
								ENDIF
							ENDFOR
						ENDIF
						
						IF iHostileTeamsInsideLocate > 0
							iR = 255
							iG = 0
							iB = 0
							iA = 200
						ENDIF
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION] Processing Hostile Takeover Location Colours - 1 - Location: ", iLoc)
				iR = iCaptureLocateRed[iLoc]
				iG = iCaptureLocateGreen[iLoc]
				iB = iCaptureLocateBlue[iLoc]
				iA = iCaptureLocateAlpha[iLoc]
			ENDIF
		ENDIF

		DRAW_MARKER(MARKER_SPHERE,
			vPos,
			<<0,0,0>>,
			<<0,0,0>>,
			<<fcoronasize,fcoronasize,fcoronaheight>>,
			iR,
			iG,
			iB,
			iA)
	ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS2,ciLoc_BS2_UseLineMarker) 
	
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS2, ciLoc_BS2_RunningBackMarker)
			
			IF bEnemyMarker
				IF DOES_TEAM_LIKE_TEAM(iTeam,iTeamWhoOwnThis)
					PRINTLN("[RCC MISSION][LOCATES][DRAW_LOCATE_MARKER]Team : ", iTeam, " DOES_TEAM_LIKE_TEAM : " ,iTeamWhoOwnThis ,"HUD colour = HUD_COLOUR_RED") 
					GET_HUD_COLOUR(HUD_COLOUR_RED, iR, iG, iB, iA)	
				ELSE
					GET_HUD_COLOUR(HUD_COLOUR_BLUE, iR, iG, iB, iA)	
				ENDIF
			ELSE
				GET_HUD_COLOUR(HUD_COLOUR_RED, iR, iG, iB, iA)
			ENDIF			
		ELSE
			IF bEnemyMarker
				GET_HUD_COLOUR(HUD_COLOUR_BLUE, iR, iG, iB, iA)
			ELSE
				GET_HUD_COLOUR(HUD_COLOUR_RED, iR, iG, iB, iA)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS2, ciLoc_BS2_RunningBackMarker)
			IF bEnemyMarker
				IF iTeamWhoOwnThis != -1
					IF DOES_TEAM_LIKE_TEAM(iTeam,iTeamWhoOwnThis)
						PRINTLN("[RCC MISSION][LOCATES][DRAW_LOCATE_MARKER]Team : ", iTeam, " DOES_TEAM_LIKE_TEAM : " ,iTeamWhoOwnThis) 
						iA =  iAlphaOverride 
						iR =  255
					ELSE
						iA = 70
					ENDIF
				ELSE
				
				ENDIF
			ELSE
				iA =  iAlphaOverride 
				iR =  255
			ENDIF
		ELSE
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciSET_LINES_AS_RUGBY_LINES)
				IF bShowRugbyOrangeTeam
					GET_HUD_COLOUR(HUD_COLOUR_ORANGE,iR,iG,iB,iA)
					iA = 70
				ELSE
					GET_HUD_COLOUR(HUD_COLOUR_PURPLE,iR,iG,iB,iA)
					iA = 70
				ENDIF
			ENDIF
		ENDIF
	
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS2, ciLoc_BS3_RunningBackMarkerAsRedT1)		
		AND iTeam = 0
			PRINTLN("[RCC MISSION][LOCATES][DRAW_LOCATE_MARKER] iLoc: ", iloc, " setting as red T1")
			GET_HUD_COLOUR(HUD_COLOUR_RED, iR, iG, iB, iA)
		ENDIF		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS3, ciLoc_BS3_RunningBackMarkerAsBlueT1)
		AND iTeam = 0
			PRINTLN("[RCC MISSION][LOCATES][DRAW_LOCATE_MARKER] iLoc: ", iloc, " setting as blue T1")
			GET_HUD_COLOUR(HUD_COLOUR_BLUE, iR, iG, iB, iA)	
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS3, ciLoc_BS3_RunningBackMarkerAsRedT2)
		AND iTeam = 1
			PRINTLN("[RCC MISSION][LOCATES][DRAW_LOCATE_MARKER] iLoc: ", iloc, " setting as Red T2")
			GET_HUD_COLOUR(HUD_COLOUR_RED, iR, iG, iB, iA)
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS3, ciLoc_BS3_RunningBackMarkerAsBlueT2)
		AND iTeam = 1
			PRINTLN("[RCC MISSION][LOCATES][DRAW_LOCATE_MARKER] iLoc: ", iloc, " setting as blue T2")
			GET_HUD_COLOUR(HUD_COLOUR_BLUE, iR, iG, iB, iA)	
		ENDIF
		
		fDirection = GET_DIRECTION_FOR_LOCATE_MARKER(iLoc)
		
		DRAW_MARKER( MARKER_LINES,
			<<vPos.x, vPos.y, vPos.z+g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fGroundClear>>,
			<<0,0,0>>,
			<<0,g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fLocateTilt,fDirection>>,
			<<fradius,1,1>>,
			iR,
			iG,
			iB,
			iA )
	ELSE
		INT iHostileTeamsInsideLocate
		STRING sSoundSet
		
		IF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_CONTROL_AREA
		AND IS_BIT_SET(iLocalBoolCheck14, LBOOL14_VFX_CAPTURE_OWNER)
			IF iRule < FMMC_MAX_RULES
								
				iR = GET_CORONA_TRANSITION(0,0)
				iG = GET_CORONA_TRANSITION(0,1)
				iB = GET_CORONA_TRANSITION(0,2)
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_TEAM_COLOURED_LOCATES)
				OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_ZONE(g_FMMC_STRUCT.iRootContentIDHash)
					IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
						iTeamWhoOwnThis = MC_serverBD.iLocOwner[iloc]
						// It doesn't matter if this sets a team twice, because we override that with the contested colour
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_INSTANT_COLOUR_CHANGES_FOR_INSIDE_LOCATE)
						OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_ZONE(g_FMMC_STRUCT.iRootContentIDHash)
							INT i = 0
							FOR i = 0 TO FMMC_MAX_TEAMS-1
								IF MC_serverBD_1.inumberOfTeamInArea[iLoc][i] > 0
								AND ((IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_ZONE(g_FMMC_STRUCT.iRootContentIDHash) AND (iTeamWhoOwnThis = -1 OR i = MC_PlayerBD[iParttoUse].iteam)) OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_ZONE(g_FMMC_STRUCT.iRootContentIDHash))
									iTeamWhoOwnThis = i
								ENDIF
							ENDFOR
						ENDIF
						
						IF iTeamWhoOwnThis > -1
							GET_HUD_COLOUR(GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeamWhoOwnThis, PlayerToUse), iR, iG, iB, iA)
							IF IS_BIT_SET(iHostileTakeoverContestedBS, iLoc)
								CLEAR_BIT(iHostileTakeoverContestedBS, iLoc)
							ENDIF
						ELSE
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_CONTESTED_COLOUR_FOR_LOCATES)
								iHostileTeamsInsideLocate = 0
								INT i = 0
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_INSTANT_COLOUR_CHANGES_FOR_INSIDE_LOCATE)										
									iHostileTeamsInsideLocate = -1
									FOR i = 0 TO FMMC_MAX_TEAMS-1
										IF MC_serverBD_1.inumberOfTeamInArea[iLoc][i] > 0
											iHostileTeamsInsideLocate++
										ENDIF
									ENDFOR
								ELSE
									FOR i = 0 TO FMMC_MAX_TEAMS-1
										IF NOT DOES_TEAM_LIKE_TEAM(MC_playerBD[iLocalPart].iteam, i)
										AND MC_playerBD[iLocalPart].iteam != i
											IF MC_serverBD_1.inumberOfTeamInArea[iLoc][i] > 0
												iHostileTeamsInsideLocate++
											ENDIF
										ENDIF
									ENDFOR
								ENDIF
								
								IF (iHostileTeamsInsideLocate > 0
								AND MC_serverBD_1.inumberOfTeamInArea[iLoc][iTeam] > 0)
								OR (IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType) AND IS_ZONE_BEING_CONTESTED(iLoc))
									IF iColourTeamCacheGotoLocateOld[iLoc] != -1
										iColourTeamCacheGotoLocateOld[iLoc] = -1
									ENDIF
									iR = 255
									iG = 0
									iB = 0
								ELSE
									iR = 255
									iG = 255
									iB = 0
								ENDIF
							ELSE
								iR = 255
								iG = 255
								iB = 0
							ENDIF
						ENDIF
					ELSE
						PRINTLN("[RCC MISSION] Processing Hostile Takeover Location Colours - 2 - Location: ", iLoc)
						iR = iCaptureLocateRed[iLoc]
						iG = iCaptureLocateGreen[iLoc]
						iB = iCaptureLocateBlue[iLoc]
						iA = iCaptureLocateAlpha[iLoc]
						
						SWITCH eCaptureLocateState[iLoc]
							CASE eCapture_neutral
							CASE eCapture_captured
								IF IS_BIT_SET(iHostileTakeoverContestedBS, iLoc)
									CLEAR_BIT(iHostileTakeoverContestedBS, iLoc)
								ENDIF
							BREAK
							CASE eCapture_contested
								IF iColourTeamCacheGotoLocateOld[iLoc] != -1
									iColourTeamCacheGotoLocateOld[iLoc] = -1
									iLocPreContestOwnerTeam[iLoc] = iTeamWhoOwnThis
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				ENDIF
				
				IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_CONTESTED_COLOUR_FOR_LOCATES)				
						IF iTeamWhoOwnThis > -1
						OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_INSTANT_COLOUR_CHANGES_FOR_INSIDE_LOCATE)			
							iHostileTeamsInsideLocate = 0
							INT i = 0
										
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_INSTANT_COLOUR_CHANGES_FOR_INSIDE_LOCATE)										
								iHostileTeamsInsideLocate = -1
								FOR i = 0 TO FMMC_MAX_TEAMS-1
									IF MC_serverBD_1.inumberOfTeamInArea[iLoc][i] > 0
										iHostileTeamsInsideLocate++
									ENDIF
								ENDFOR
							ELSE
								FOR i = 0 TO FMMC_MAX_TEAMS-1
									IF NOT DOES_TEAM_LIKE_TEAM(iTeamWhoOwnThis, i)
									AND iTeamWhoOwnThis != i
										IF MC_serverBD_1.inumberOfTeamInArea[iLoc][i] > 0
											iHostileTeamsInsideLocate++
										ENDIF
									ENDIF
								ENDFOR
							ENDIF
						
							IF iHostileTeamsInsideLocate > 0
							OR (IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType) AND IS_ZONE_BEING_CONTESTED(iLoc))
								IF iColourTeamCacheGotoLocateOld[iLoc] != -1
									iColourTeamCacheGotoLocateOld[iLoc] = -1
									iLocPreContestOwnerTeam[iLoc] = iTeamWhoOwnThis
								ENDIF
								
								iR = 255
								iG = 0
								iB = 0
								iA = 200
							ELSE
								IF IS_BIT_SET(iHostileTakeoverContestedBS, iLoc)
									CLEAR_BIT(iHostileTakeoverContestedBS, iLoc)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_HOSTILE_TAKEOVER_SOUNDS)
				sSoundSet = "dlc_xm_hota_Sounds"
				
				PRINTLN("[AW_SMOKE][DRAW_LOCATE_MARKER iLoc: ", iLoc, " iColourTeamCacheGotoLocateOld[iLoc]: ", iColourTeamCacheGotoLocateOld[iLoc], " iTeamWhoOwnThis: ", iTeamWhoOwnThis, " iHostileTeamsInsideLocate: ", iHostileTeamsInsideLocate, " (1) ")
								
				IF (iColourTeamCacheGotoLocateOld[iLoc] != iTeamWhoOwnThis
				AND NOT IS_BIT_SET(iHostileTakeoverContestedBS, iLoc))
					PRINTLN("[RCC MISSION] HT SOUNDS - iLoc: ", iLoc, "Prev Owner: ", iLocPreContestOwnerTeam[iLoc], " Prev State: ", ENUM_TO_INT(eCaptureLocateStateLastFrame[iLoc]))
					PLAY_HOSTILE_TAKEOVER_SOUND(eCaptureLocateState[iLoc], iLoc, iTeamWhoOwnThis, sSoundSet)
				ENDIF
			ENDIF			

			PROCESS_CORONA_TRANSITION(0)// might need to remove this.
			PRINTLN("[AW_SMOKE][DRAW_LOCATE_MARKER( ] INVALID TEAM SELECTED") 
			DRAW_MARKER(MARKER_CYLINDER,
			vPos,
			(<<0,0,0>>),
			(<<0,0,0>>),
			(<<fcoronasize,fcoronasize,fcoronaheight>>),
			iR,
			iG,
			iB,
			iA)			
		ELSE
			IF iRule < FMMC_MAX_RULES
				IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
					INT i = 0
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_TEAM_COLOURED_LOCATES)
						iTeamWhoOwnThis = MC_serverBD.iLocOwner[iloc]
						
						// It doesn't matter if this sets a team twice, because we override that with the contested colour
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_INSTANT_COLOUR_CHANGES_FOR_INSIDE_LOCATE)
							FOR i = 0 TO FMMC_MAX_TEAMS-1
								IF MC_serverBD_1.inumberOfTeamInArea[iLoc][i] > 0
									iTeamWhoOwnThis = i
								ENDIF
							ENDFOR
						ENDIF
						
						IF iTeamWhoOwnThis > -1
							GET_HUD_COLOUR(GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeamWhoOwnThis, PlayerToUse), iR, iG, iB, iA)
						ENDIF
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_CONTESTED_COLOUR_FOR_LOCATES)
						IF iTeamWhoOwnThis > -1
						OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_INSTANT_COLOUR_CHANGES_FOR_INSIDE_LOCATE)	
							iHostileTeamsInsideLocate = 0
										
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_INSTANT_COLOUR_CHANGES_FOR_INSIDE_LOCATE)
								iHostileTeamsInsideLocate = -1
								FOR i = 0 TO FMMC_MAX_TEAMS-1
									IF MC_serverBD_1.inumberOfTeamInArea[iLoc][i] > 0
										iHostileTeamsInsideLocate++
									ENDIF
								ENDFOR
							ELSE
								FOR i = 0 TO FMMC_MAX_TEAMS-1
									IF NOT DOES_TEAM_LIKE_TEAM(iTeamWhoOwnThis, i)
									AND iTeamWhoOwnThis != i
										IF MC_serverBD_1.inumberOfTeamInArea[iLoc][i] > 0
											iHostileTeamsInsideLocate++
										ENDIF
									ENDIF
								ENDFOR
							ENDIF
							
							IF iHostileTeamsInsideLocate > 0
								IF iColourTeamCacheGotoLocateOld[iLoc] != -1
									iColourTeamCacheGotoLocateOld[iLoc] = -1
									iLocPreContestOwnerTeam[iLoc] = iTeamWhoOwnThis
								ENDIF
								
								iR = 255
								iG = 0
								iB = 0
								iA = 200
							ELSE
								IF IS_BIT_SET(iHostileTakeoverContestedBS, iLoc)
									CLEAR_BIT(iHostileTakeoverContestedBS, iLoc)
								ENDIF
							ENDIF
						ELSE
							iR = 255
							iG = 255
							iB = 0
							iA = 200
						ENDIF
					ENDIF
					
					IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[iRule] = ciFMMC_DROP_OFF_TYPE_CYLINDER
					AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_CHECK_CYLINDER)
						GET_GROUND_Z_FOR_3D_COORD(vPos, vPos.z)
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION] Processing Hostile Takeover Location Colours - 3 - Location: ", iLoc)
					iR = iCaptureLocateRed[iLoc]
					iG = iCaptureLocateGreen[iLoc]
					iB = iCaptureLocateBlue[iLoc]
					iA = iCaptureLocateAlpha[iLoc]
					
					SWITCH eCaptureLocateState[iLoc]
						CASE eCapture_neutral
						CASE eCapture_captured
							IF IS_BIT_SET(iHostileTakeoverContestedBS, iLoc)
								CLEAR_BIT(iHostileTakeoverContestedBS, iLoc)
							ENDIF
						BREAK
						CASE eCapture_contested
							IF iColourTeamCacheGotoLocateOld[iLoc] != -1
								iColourTeamCacheGotoLocateOld[iLoc] = -1
								iLocPreContestOwnerTeam[iLoc] = iTeamWhoOwnThis
							ENDIF
						BREAK
					ENDSWITCH
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_HOSTILE_TAKEOVER_SOUNDS)
				sSoundSet = "dlc_xm_hota_Sounds"
							
				PRINTLN("[AW_SMOKE][DRAW_LOCATE_MARKER iLoc: ", iLoc, " iColourTeamCacheGotoLocateOld[iLoc]: ", iColourTeamCacheGotoLocateOld[iLoc], " iTeamWhoOwnThis: ", iTeamWhoOwnThis, " iHostileTeamsInsideLocate: ", iHostileTeamsInsideLocate, " (2) ")
				
				IF (iColourTeamCacheGotoLocateOld[iLoc] != iTeamWhoOwnThis
				AND NOT IS_BIT_SET(iHostileTakeoverContestedBS, iLoc))
					PRINTLN("[RCC MISSION] HT SOUNDS - iLoc: ", iLoc, "Prev Owner: ", iLocPreContestOwnerTeam[iLoc], " Prev State: ", ENUM_TO_INT(eCaptureLocateStateLastFrame[iLoc]))
					
					PLAY_HOSTILE_TAKEOVER_SOUND(eCaptureLocateState[iLoc], iLoc, iTeamWhoOwnThis, sSoundSet)
				ENDIF
			ENDIF	
			
			DRAW_MARKER(MARKER_CYLINDER,
			vPos,
			<<0,0,0>>,
			<<0,0,0>>,
			<<fcoronasize,fcoronasize,fcoronaheight>>,
			iR,
			iG,
			iB,
			iA)
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL MAINTAIN_CLIP_CHECK_POINTS(CHECKPOINT_INDEX thisCheckpoint, VECTOR vPos, SHAPETEST_INDEX &stiCheckPointCLip)
	VECTOR vCheckPointGroundPos,vHitNormal, vHitPoint
	ENTITY_INDEX eiHitEntity
	INT iHitCount
	
	vCheckPointGroundPos = vPos
	
	IF stiCheckPointCLip = NULL
		PRINTLN("[MAINTAIN_CLIP_CHECK_POINTS] ANGLED CHECKPOINT - stiCheckPointCLip = START_SHAPE_TEST_LOS_PROBE(", vCheckPointGroundPos, "+<<0.0, 0.0, 1.0>>, ", vCheckPointGroundPos, " - <<0.0, 0.0, 1.0>>, SCRIPT_INCLUDE_OBJECT)")
		stiCheckPointCLip = START_SHAPE_TEST_LOS_PROBE(vCheckPointGroundPos+<<0.0, 0.0, 1.0>>, vCheckPointGroundPos - <<0.0, 0.0, 1.0>>, SCRIPT_INCLUDE_OBJECT , NULL, SCRIPT_SHAPETEST_OPTION_IGNORE_NO_COLLISION | SCRIPT_SHAPETEST_OPTION_IGNORE_GLASS)
		SET_CHECKPOINT_RGBA(thisCheckpoint, 0, 0, 0, 0)
		SET_CHECKPOINT_RGBA2(thisCheckpoint, 0, 0, 0, 0)
	ELIF GET_SHAPE_TEST_RESULT(stiCheckPointCLip, iHitCount, vHitPoint, vHitNormal, eiHitEntity) = SHAPETEST_STATUS_RESULTS_READY
	
		PRINTLN("[MAINTAIN_CLIP_CHECK_POINTS] ANGLED CHECKPOINT -  vHitNormal = ", vHitNormal)
		PRINTLN("[CS_CLY] [2815596] ANGLED CHECKPOINT  SET_CHECKPOINT_RGBA, MAINTAIN_CLIP_CHECK_POINTS, PRIMARY ")
		SET_CHECKPOINT_CLIPPLANE_WITH_POS_NORM(thisCheckpoint, vCheckPointGroundPos - <<0,0,0.05>>,  vHitNormal)
		stiCheckPointCLip = NULL

		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC VECTOR CALCULATE_QUAD_NORMAL(VECTOR &verts[])
      INT i
      VECTOR u, v
      VECTOR norm
      
      REPEAT COUNT_OF(verts) i
            u = verts[i]
            v = verts[(i + 1) % COUNT_OF(verts)]
            
                norm.x += (u.y - v.y) * (u.z + v.z) 
            norm.y += (u.z - v.z) * (u.x + v.x) 
            norm.z += (u.x - v.x) * (u.y + v.y) 

      ENDREPEAT
    
      RETURN NORMALISE_VECTOR(norm)
ENDFUNC

FUNC VECTOR GET_CHECKOINT_OFFSET(INT i)
	CONST_FLOAT ciCHECK_POINT_Z_OFFSET -2.0
      SWITCH i
            CASE 0            RETURN <<0,0,ciCHECK_POINT_Z_OFFSET>>
            CASE 1            RETURN <<FMMC_CHECKPOINT_SIZE/2,0,ciCHECK_POINT_Z_OFFSET>>
            CASE 2            RETURN <<-FMMC_CHECKPOINT_SIZE/2,0,ciCHECK_POINT_Z_OFFSET>>
            CASE 3            RETURN <<0,FMMC_CHECKPOINT_SIZE/2,ciCHECK_POINT_Z_OFFSET>>
            CASE 4            RETURN <<0,-FMMC_CHECKPOINT_SIZE/2,ciCHECK_POINT_Z_OFFSET>>
            CASE 5            RETURN <<FMMC_CHECKPOINT_SIZE/2,FMMC_CHECKPOINT_SIZE/2,ciCHECK_POINT_Z_OFFSET>>
            CASE 6            RETURN <<-FMMC_CHECKPOINT_SIZE/2,-FMMC_CHECKPOINT_SIZE/2,ciCHECK_POINT_Z_OFFSET>>
            CASE 7            RETURN <<FMMC_CHECKPOINT_SIZE/2,-FMMC_CHECKPOINT_SIZE/2,ciCHECK_POINT_Z_OFFSET>>
            CASE 8            RETURN <<-FMMC_CHECKPOINT_SIZE/2,FMMC_CHECKPOINT_SIZE/2,ciCHECK_POINT_Z_OFFSET>>
      ENDSWITCH
      RETURN <<0,0,0>>
ENDFUNC

PROC CLIP_THE_CHECKPOINT(CHECKPOINT_INDEX thisCheckpoint, VECTOR vPos)
	int i
	VECTOR vecArray[8]
	FLOAT fGround
	VECTOR vCheckPointGroundPos = vPos
	
	FOR i = 0 TO 7
		vecArray[i] = vCheckPointGroundPos + GET_CHECKOINT_OFFSET(i)          
		GET_GROUND_Z_FOR_3D_COORD(vecArray[i],fGround)
		IF fGround < vCheckPointGroundPos.z - 2
		OR fGround > vCheckPointGroundPos.z + 2
			vecArray[i].z = vCheckPointGroundPos.z
			PRINTINT(i)PRINTSTRING("vPos z. vecArray[i]  = ")PRINTVECTOR(vecArray[i])PRINTNL()
		ELSE
			IF fGround < 5.0
				vecArray[i].z = fGround
				PRINTINT(i)PRINTSTRING("offset z. vecArray[i]  = ")PRINTVECTOR(vecArray[i])PRINTNL()
			ENDIF
		ENDIF
	ENDFOR

	VECTOR vFinalNormal = CALCULATE_QUAD_NORMAL(vecArray)
	PRINTNL()PRINTSTRING("CLIP_THE_CHECKPOINT, vFinalNormal  = ")PRINTVECTOR(vFinalNormal)PRINTNL()PRINTNL()
	PRINTLN("[CS_CLY] [2815596] SET_CHECKPOINT_CLIPPLANE_WITH_POS_NORM, MAINTAIN_CLIP_CHECK_POINTS, CLIP_THE_CHECKPOINT ")
	SET_CHECKPOINT_CLIPPLANE_WITH_POS_NORM(thisCheckpoint, vCheckPointGroundPos - <<0,0,0.3>>, vFinalNormal)
	
ENDPROC

PROC DRAW_RACE_STYLE_MARKER(VECTOR vLoc, INT iLoc, INT iAlphaOverride = -1)
	
	INT iteam = MC_playerBD[iPartToUse].iteam
	HUD_COLOURS TeamColour
	VECTOR vCreationOffset
	
	IF iTeam = -1 OR iTeam >= FMMC_MAX_TEAMS
		PRINTLN("[MMacK][DRAW_RACE_STYLE_MARKER] INVALID TEAM SELECTED") 
		EXIT
	ENDIF
	
	// 2055100 - Don't draw the marker if the objective is blocked
	IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
		//url:bugstar:2503517
		#IF IS_DEBUG_BUILD
		IF boutputdebugspam
			PRINTLN("[JS] url:bugstar:2503517 - IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER = TRUE")
		ENDIF
		#ENDIF
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS, ciLoc_BS_HideMarker)
		#IF IS_DEBUG_BUILD
		IF boutputdebugspam
			PRINTLN("[JS] url:bugstar:2503517 - IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS, ciLoc_BS_HideMarker) = TRUE")
		ENDIF
		#ENDIF
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_SUB_TEAM_ROLES)
		IF MC_serverBD_4.iGotoLocationDataPriority[iloc][iteam] < FMMC_MAX_RULES
		AND MC_playerBD[iPartToUse].iCoronaRole != -1
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iRolesBitset[MC_serverBD_4.iGotoLocationDataPriority[iloc][iteam]][MC_playerBD[iPartToUse].iCoronaRole], ciBS_ROLE_LAST_RULE_FOR_TEAMMATE_OVERRIDE)
				EXIT			
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_PED_IN_REQUIRED_LOCATION_VEHICLE(PlayerPedToUse, iteam, iLoc) AND MC_playerBD[iLocalPart].iPartIAmSpectating = -1

		#IF IS_DEBUG_BUILD
		IF boutputdebugspam
			PRINTLN("[JS] url:bugstar:2503517 - NOT IS_PED_IN_REQUIRED_LOCATION_VEHICLE(PlayerPedToUse, iteam, iLoc) = TRUE")
		ENDIF
		#ENDIF
		EXIT
	ENDIF
	
	IF g_HeistApartmentDropoffPanStarted
	OR g_HeistGarageDropoffPanStarted
	OR NETWORK_IS_PLAYER_IN_MP_CUTSCENE(LocalPlayer)
		EXIT
	ENDIF
	
	INT iR, iG, iB, iA
	INT iR2, iG2, iB2, iA2
	CHECKPOINT_TYPE checkType
	VECTOR vPointAt
	
	GET_HUD_COLOUR(HUD_COLOUR_YELLOWLIGHT, iR, iG, iB, iA)
	iA = 150
	
	GET_HUD_COLOUR(HUD_COLOUR_NORTH_BLUE, iR2, iG2, iB2, iA2)
	iA2 = 150
	
	IF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_CONTROL_AREA
	AND IS_BIT_SET(iLocalBoolCheck14, LBOOL14_VFX_CAPTURE_OWNER)
		IF g_iTheWinningTeam = -1
			//Show Yellow = Tied
			GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
		ELSE
			TeamColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(g_iTheWinningTeam, PlayerToUse)
			//Team 0 - Orange
			//Team 1 - Green
			//Team 2 - Pink
			//Team 3 - Purple
			PRINTLN( "[RCC MISSION][AW SMOKE] TEAM ",g_iTheWinningTeam, "is winning")
			GET_HUD_COLOUR(TeamColour, iR, iG, iB, iA)
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fTeamMembersNeededWithin > 0
	AND NOT ARE_ALL_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE(iTeam)
		GET_HUD_COLOUR(HUD_COLOUR_RED, iR, iG, iB, iA)
		iA = 150
	ENDIF
	
	INT iTime = GET_CLOCK_HOURS()
	
	IF iTime > 6
	AND iTime < 20
		//The race creator makes these markers more obvious during daytime, 1725777
		
		iA = ROUND(TO_FLOAT(iA) * 1.2)
		iA2 = ROUND(TO_FLOAT(iA2) * 1.4)
	ENDIF
	
	
	IF iAlphaOverride > -1
	AND iAlphaOverride < 256
		FLOAT fOverride = (TO_FLOAT(iAlphaOverride) / 255.0)
		
		iA = ROUND(TO_FLOAT(iA) * fOverride)
		iA2 = ROUND(TO_FLOAT(iA2) * fOverride)
	ENDIF
	
	IF NOT IS_BIT_SET(iCheckpointBS, iloc) // The checkpoint doesn't exist yet, make it:
		
		VECTOR vUnitVec = <<0, 1, 0>>
		
		RotateVec( vUnitVec, <<0, 0, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fHeading[iteam]>> )
		
		vPointAt = vLoc + vUnitVec
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS2, ciLoc_BS2_RaceLocate_Chckpt1)
			IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iCoronaSize = LOCATION_CORONA_SIZE_ACTUAL
				checkType = CHECKPOINT_RACE_GROUND_CHEVRON_1_BASE  
			ELSE
				checkType = CHECKPOINT_RACE_GROUND_CHEVRON_1
			ENDIF
		ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS2, ciLoc_BS2_RaceLocate_Chckpt2)
			checkType = CHECKPOINT_RACE_GROUND_CHEVRON_2
		ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS2, ciLoc_BS2_RaceLocate_Chckpt3)
			checkType = CHECKPOINT_RACE_GROUND_CHEVRON_3
		ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS2, ciLoc_BS2_RaceLocate_Lap)
			checkType = CHECKPOINT_RACE_GROUND_LAP
		ELSE // IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS2, ciLoc_BS2_RaceLocate_Finish)
			checkType = CHECKPOINT_RACE_GROUND_FLAG
		ENDIF

		IF MC_serverBD_4.iGotoLocationDataPriority[iloc][iteam] < FMMC_MAX_RULES
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iRuleBitsetFour[MC_serverBD_4.iGotoLocationDataPriority[iloc][iteam]], ciBS_RULE4_LAP_PASSED_ON_THIS_RULE)
		AND (MC_ServerBD_4.iTeamLapsCompleted[iteam] + 1) >= g_FMMC_STRUCT.sFMMCEndConditions[iteam].iMissionLapLimit
			//Set the final checkpoint of the race to be a checkered flag
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciDISABLE_FINISH_LINE_CHECKERED_SPRITE)
				checkType = CHECKPOINT_RACE_GROUND_FLAG
			ENDIF
		ENDIF
		
		FLOAT fSize = FMMC_CHECKPOINT_SIZE*1*0.66
		
		IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iCoronaSize = LOCATION_CORONA_SIZE_ACTUAL
			fSize = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fRadius[0] * 2
		ELIF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iCoronaSize = LOCATION_CORONA_SIZE_SMALL
			fSize *= 0.5
		ELIF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iCoronaSize = LOCATION_CORONA_SIZE_RACE
			fSize = (8.5 *1.333)
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iCoronaSize = LOCATION_CORONA_SIZE_RACE
			IF checkType != CHECKPOINT_RACE_GROUND_FLAG
				checkType = CHECKPOINT_RACE_GROUND_CHEVRON_1_BASE
			ENDIF
			PRINTLN("[RCC MISSION] DRAW_RACE_STYLE_MARKER Size Race")
		ENDIF

		IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iCoronaSize = LOCATION_CORONA_SIZE_RACE
			IF checkType = CHECKPOINT_RACE_GROUND_FLAG
				vCreationOffset = <<0,0,6>>
			ELSE
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS2, ciLoc_BS2_AngleCheckpoint)
					vCreationOffset = <<0,0,-2.5>> //-2 //<<0,0,2>> 
				ELSE
					vCreationOffset = <<0,0,-2>>
				ENDIF
			ENDIF
		ELIF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iCoronaSize = LOCATION_CORONA_SIZE_ACTUAL
			vCreationOffset = <<0,0,0>>
		ELSE
			vCreationOffset =  <<0,0,4>>
		ENDIF

		IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iCoronaSize = LOCATION_CORONA_SIZE_ACTUAL
			ciLocCheckpoint[iloc] = CREATE_CHECKPOINT(checkType, vLoc + vCreationOffset, vPointAt + <<0,0,5>>, fSize, iR, iG, iB, iA)
		ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS2, ciLoc_BS2_AngleCheckpoint)
			//Create invisible 
			ciLocCheckpoint[iloc] = CREATE_CHECKPOINT(checkType, vLoc + vCreationOffset, vPointAt + <<0,0,5>>, fSize, iR, iG, iB, 0)
			PRINTLN("[RCC MISSION] DRAW_RACE_STYLE_MARKER | ANGLED CHECKPOINT CREATED")
		ELSE
			ciLocCheckpoint[iloc] = CREATE_CHECKPOINT(checkType, vLoc + vCreationOffset, vPointAt + <<0,0,5>>, fSize, iR, iG, iB, iA)
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iCoronaSize = LOCATION_CORONA_SIZE_RACE
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS2, ciLoc_BS2_AngleCheckpoint)
				SET_CHECKPOINT_CYLINDER_HEIGHT(ciLocCheckpoint[iloc],9.5,9.5,100)
				SET_CHECKPOINT_INSIDE_CYLINDER_HEIGHT_SCALE(ciLocCheckpoint[iloc],0.75)
			ELSE
				SET_CHECKPOINT_CYLINDER_HEIGHT(ciLocCheckpoint[iloc],9.5,9.5,100)
				SET_CHECKPOINT_INSIDE_CYLINDER_HEIGHT_SCALE(ciLocCheckpoint[iloc],0.75)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS2, ciLoc_BS2_AngleCheckpoint)
			SET_BIT(bsCheckPointCreated,iloc)
			
			//CLIP_THE_CHECKPOINT(ciLocCheckpoint[iloc],vLoc) //+ vCreationOffset)
		ENDIF
		
		
		IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iCoronaSize = LOCATION_CORONA_SIZE_ACTUAL
			PRINTLN("[RCC MISSION] DRAW_RACE_STYLE_MARKER Overriding height")
			SET_CHECKPOINT_CYLINDER_HEIGHT(ciLocCheckpoint[iloc],9,9,100)
			SET_CHECKPOINT_INSIDE_CYLINDER_HEIGHT_SCALE(ciLocCheckpoint[iloc],0.2)
			SET_CHECKPOINT_INSIDE_CYLINDER_SCALE(ciLocCheckpoint[iloc],0.5)
		ENDIF

		SET_CHECKPOINT_RGBA2(ciLocCheckpoint[iloc], iR2, iG2, iB2, iA2)
		
		SET_BIT(iCheckpointBS, iloc)
		
		PRINTLN("[RCC MISSION] DRAW_RACE_STYLE_MARKER - Creating checkpoint for iloc ",iloc," at ",vLoc)
		
	ELSE // Checkpoint already exists, update the alpha:
		SET_CHECKPOINT_RGBA(ciLocCheckpoint[iloc], iR, iG, iB, iA)
		SET_CHECKPOINT_RGBA2(ciLocCheckpoint[iloc], iR2, iG2, iB2, iA2)
	ENDIF
	
ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: LOCAL PLAYER DROP OFFS !
//
//************************************************************************************************************************************************************



PROC SET_APARTMENT_DROPOFF_WARP_RANGE_BASED_ON_CUTSCENE(INT iteam = -1)

	INT iTeamCheckBit = -1

	IF IS_BIT_SET( iLocalBoolCheck5, LBOOL5_MID_MISSION_MOCAP )
		g_iApartmentDropoffWarpRadius = 120
	ELSE
		IF iteam != -1
			SWITCH iteam
				CASE 0
					iTeamCheckBit = ci_CSBS_Pull_Team0_In
				BREAK
				
				CASE 1
					iTeamCheckBit = ci_CSBS_Pull_Team1_In
				BREAK
				
				CASE 2
					iTeamCheckBit = ci_CSBS_Pull_Team2_In
				BREAK
				
				CASE 3
					iTeamCheckBit = ci_CSBS_Pull_Team3_In
				BREAK
			ENDSWITCH
			
			//PRINTLN("[RCC MISSION] SET_APARTMENT_DROPOFF_WARP_RANGE_BASED_ON_CUTSCENE iteam : ", iteam)
		
			IF iTeamCheckBit != -1
				IF IS_BIT_SET( g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitset, iTeamCheckBit )
				//PRINTLN("[RCC MISSION] SET_APARTMENT_DROPOFF_WARP_RANGE_BASED_ON_CUTSCENE g_iApartmentDropoffWarpRadius = iALWAYS_WARP_TO_APARTMENT_DISTANCE")
					g_iApartmentDropoffWarpRadius = iALWAYS_WARP_TO_APARTMENT_DISTANCE
				ELSE
					//PRINTLN("[RCC MISSION] SET_APARTMENT_DROPOFF_WARP_RANGE_BASED_ON_CUTSCENE g_iApartmentDropoffWarpRadius = 120")
					g_iApartmentDropoffWarpRadius = 120
				ENDIF
			ELSE
				//Set warp radius if team is incorrect value
				g_iApartmentDropoffWarpRadius = iALWAYS_WARP_TO_APARTMENT_DISTANCE
				//PRINTLN("[RCC MISSION] SET_APARTMENT_DROPOFF_WARP_RANGE_BASED_ON_CUTSCENE g_iApartmentDropoffWarpRadius = iALWAYS_WARP_TO_APARTMENT_DISTANCE 2")
			ENDIF
		ELSE
			//Set warp radius if no valid team supplied
			g_iApartmentDropoffWarpRadius = iALWAYS_WARP_TO_APARTMENT_DISTANCE
			//PRINTLN("[RCC MISSION] SET_APARTMENT_DROPOFF_WARP_RANGE_BASED_ON_CUTSCENE g_iApartmentDropoffWarpRadius = iALWAYS_WARP_TO_APARTMENT_DISTANCE 3")
		ENDIF
	ENDIF
ENDPROC

FUNC VECTOR GET_DROP_OFF_CENTER_FOR_TEAM( BOOL bground = FALSE, INT iteam = 0, INT iPartDroppingOff = -1 )

	VECTOR vcenter
	FLOAT fwater

	IF MC_serverBD_4.iCurrentHighestPriority[iteam] < FMMC_MAX_RULES
    	IF g_FMMC_STRUCT.sFMMCEndConditions[iteam].iDropOffType[MC_serverBD_4.iCurrentHighestPriority[iteam]] = ciFMMC_DROP_OFF_TYPE_CYLINDER
        OR 	g_FMMC_STRUCT.sFMMCEndConditions[iteam].iDropOffType[MC_serverBD_4.iCurrentHighestPriority[iteam]] = ciFMMC_DROP_OFF_TYPE_SPHERE
			vcenter = g_FMMC_STRUCT.sFMMCEndConditions[iteam].vDropOff[MC_serverBD_4.iCurrentHighestPriority[iteam]]
			
			IF MC_serverBD.iNumVehHighestPriority[iteam] > 0
				
				INT iveh = GET_VEHICLE_BEING_DROPPED_OFF(iteam, iPartDroppingOff)
				
				IF iveh != -1
					IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].vDropOffOverride)
						vcenter = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].vDropOffOverride
					ELSE
						IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[iteam].vDropOff2[MC_serverBD_4.iCurrentHighestPriority[iteam]])
							IF iveh != iFirstHighPriorityVehicleThisRule[iteam]
								PRINTLN("[RCC MISSION] GET_DROP_OFF_CENTER_FOR_TEAM - Radius drop off, using vDropOff2 as this isn't the first priority vehicle on this rule - vehicle dropping off = ",iveh,", iFirstHighPriorityVehicleThisRule = ",iFirstHighPriorityVehicleThisRule[iteam])
								PRINTLN("[RCC MISSION] GET_DROP_OFF_CENTER_FOR_TEAM - vDropOff2 = ",g_FMMC_STRUCT.sFMMCEndConditions[iteam].vDropOff2[MC_serverBD_4.iCurrentHighestPriority[iteam]])
								vcenter = g_FMMC_STRUCT.sFMMCEndConditions[iteam].vDropOff2[MC_serverBD_4.iCurrentHighestPriority[iteam]]
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF bground
				FLOAT fTempZ
				IF GET_GROUND_Z_FOR_3D_COORD(vcenter, fTempZ)
					vcenter.z = fTempZ
					PRINTLN("[RCC MISSION] GET_DROP_OFF_CENTER_FOR_TEAM - Grounding drop off center for cylinder/sphere")
				ELSE
					PRINTLN("[RCC MISSION] GET_DROP_OFF_CENTER_FOR_TEAM - Tried to ground drop off center for cylinder/sphere, but GET_GROUND_Z_FOR_3D_COORD failed. Sticking with ", vcenter)
				ENDIF
			ENDIF
			
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[iteam].iDropOffType[MC_serverBD_4.iCurrentHighestPriority[iteam]] = ciFMMC_DROP_OFF_TYPE_PROPERTY
			vcenter = GET_PROPERTY_DROP_OFF_COORDS( eApartmentBuzzerEntrance, DEFAULT )

		ELIF g_FMMC_STRUCT.sFMMCEndConditions[iteam].iDropOffType[MC_serverBD_4.iCurrentHighestPriority[iteam]] = ciFMMC_DROP_OFF_TYPE_LEADER_GARAGE
			vcenter = GET_PROPERTY_DROP_OFF_COORDS( eGarageExterior )

   		ELIF g_FMMC_STRUCT.sFMMCEndConditions[iteam].iDropOffType[MC_serverBD_4.iCurrentHighestPriority[iteam]] = ciFMMC_DROP_OFF_TYPE_AREA
        	vcenter = (g_FMMC_STRUCT.sFMMCEndConditions[iteam].vDropOff[MC_serverBD_4.iCurrentHighestPriority[iteam]] + g_FMMC_STRUCT.sFMMCEndConditions[iteam].vDropOff2[MC_serverBD_4.iCurrentHighestPriority[iteam]])
        	vcenter *= << 0.5, 0.5, 0.5 >>
			PRINTLN("[GET_DROP_OFF_CENTER_FOR_TEAM] vcenter = <<", vcenter.x, ", ", vcenter.y, ", ", vcenter.z, ">>")
    		IF bground
           		PRINTLN("[GET_DROP_OFF_CENTER_FOR_TEAM] bground = ", bground)
    			IF GET_WATER_HEIGHT_NO_WAVES(vcenter, fwater)
    				vcenter.z = fwater
                	PRINTLN("[GET_DROP_OFF_CENTER_FOR_TEAM] GET_WATER_HEIGHT_NO_WAVES ... fwater = ", fwater)
             	ELSE
                	GET_GROUND_Z_FOR_3D_COORD(vcenter, vcenter.z)
					PRINTLN("[GET_DROP_OFF_CENTER_FOR_TEAM] vcenter.z = ", vcenter.z)
					IF vCenter.z = 0.0
						PRINTLN("[GET_DROP_OFF_CENTER_FOR_TEAM] vcenter.z was 0. Replacing with bottom of angled area.")
						vCenter.z = g_FMMC_STRUCT.sFMMCEndConditions[iteam].vDropOff2[MC_serverBD_4.iCurrentHighestPriority[iteam]].z
					ENDIF
            	ENDIF
        	ENDIF
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[iteam].iDropOffType[MC_serverBD_4.iCurrentHighestPriority[iteam]] = ciFMMC_DROP_OFF_TYPE_VEHICLE

			IF (g_FMMC_STRUCT.sFMMCEndConditions[iteam].iDropOff_Vehicle[MC_serverBD_4.iCurrentHighestPriority[iteam]]) > -1
				NETWORK_INDEX niVeh = MC_serverBD_1.sFMMC_SBD.niVehicle[ g_FMMC_STRUCT.sFMMCEndConditions[iteam].iDropOff_Vehicle[MC_serverBD_4.iCurrentHighestPriority[iteam]] ]
				IF NETWORK_DOES_NETWORK_ID_EXIST(niVeh)
				AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(niVeh))
					vcenter = GET_ENTITY_COORDS(NET_TO_VEH(niVeh))
				ENDIF
			ENDIF
			
		
		ENDIF
	ENDIF
     
	RETURN vcenter
	
ENDFUNC

FUNC VECTOR GET_DUMMY_OVERRIDE_CENTER(INT iDummyBlip)
	VECTOR vcenter = <<0,0,0>>
	INT iteam = MC_playerBD[iPartToUse].iteam
	INT iveh = GET_VEHICLE_BEING_DROPPED_OFF(iteam, iPartToUse)
	IF iveh != -1
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].vDropOffOverride)
			vcenter = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].vDropOffOverride
		ELSE
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].vPos)
				//IF iveh != iFirstHighPriorityVehicleThisRule[iteam]
					vcenter = g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].vPos
				//ENDIF
			ENDIF
		ENDIF
	ENDIF
	PRINTLN("[QGPS][JR] - DUMMY OVERRIDE CENTER - RETURNING ", vcenter)
	RETURN vcenter
ENDFUNC

FUNC VECTOR GET_DROP_OFF_CENTER( BOOL bground = FALSE )
	
	RETURN GET_DROP_OFF_CENTER_FOR_TEAM( bground, MC_playerBD[iPartToUse].iteam, iPartToUse )
	
ENDFUNC

FUNC VECTOR GET_FINAL_DROP_OFF_CENTER(INT iteam)

	VECTOR vcenter

	IF MC_serverBD.iMaxObjectives[iteam] < FMMC_MAX_RULES
		IF g_FMMC_STRUCT.sFMMCEndConditions[iteam].iDropOffType[MC_serverBD.iMaxObjectives[iteam]] = ciFMMC_DROP_OFF_TYPE_CYLINDER
		OR g_FMMC_STRUCT.sFMMCEndConditions[iteam].iDropOffType[MC_serverBD.iMaxObjectives[iteam]] = ciFMMC_DROP_OFF_TYPE_SPHERE
			vcenter = g_FMMC_STRUCT.sFMMCEndConditions[iteam].vDropOff[MC_serverBD.iMaxObjectives[iteam]]
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[iteam].iDropOffType[MC_serverBD.iMaxObjectives[iteam]] = ciFMMC_DROP_OFF_TYPE_PROPERTY
			vcenter = GET_PROPERTY_DROP_OFF_COORDS( eApartmentBuzzerEntrance )
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[iteam].iDropOffType[MC_serverBD.iMaxObjectives[iteam]] = ciFMMC_DROP_OFF_TYPE_LEADER_GARAGE
			vcenter = GET_PROPERTY_DROP_OFF_COORDS( eGarageExterior )
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[iteam].iDropOffType[MC_serverBD.iMaxObjectives[iteam]] = ciFMMC_DROP_OFF_TYPE_AREA
			vcenter = (g_FMMC_STRUCT.sFMMCEndConditions[iteam].vDropOff[MC_serverBD.iMaxObjectives[iteam]] + g_FMMC_STRUCT.sFMMCEndConditions[iteam].vDropOff2[MC_serverBD.iMaxObjectives[iteam]])
			vcenter *= << 0.5, 0.5, 0.5 >>
		ELSE //It's a vehicle drop off
			IF (g_FMMC_STRUCT.sFMMCEndConditions[iteam].iDropOff_Vehicle[MC_serverBD.iMaxObjectives[iteam]]) > -1
				NETWORK_INDEX niVeh = MC_serverBD_1.sFMMC_SBD.niVehicle[ g_FMMC_STRUCT.sFMMCEndConditions[iteam].iDropOff_Vehicle[MC_serverBD.iMaxObjectives[iteam]] ]
				IF NETWORK_DOES_NETWORK_ID_EXIST(niVeh)
				AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(niVeh))
					vcenter = GET_ENTITY_COORDS(NET_TO_VEH(niVeh))
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN vcenter
	
ENDFUNC

FUNC VECTOR GET_COORDS_FOR_LOCATE_MARKER(FLOAT &fRadius)

	VECTOR vcenter
	FLOAT fwater
	VECTOR vCoords 
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffType[ iRule ] = ciFMMC_DROP_OFF_TYPE_CYLINDER
	OR g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffType[ iRule ] = ciFMMC_DROP_OFF_TYPE_SPHERE
		vCoords = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].vDropOff[ iRule ]
		
		INT iveh = MC_playerBD[iPartToUse].iVehNear
		
		IF iveh = -1 // I'm not in a vehicle, what else can we find:
			GET_VEHICLE_BEING_DROPPED_OFF(iTeam)
		ENDIF
		
		IF iveh != -1
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].vDropOffOverride)
				vCoords = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].vDropOffOverride
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fVehDropOffVisualRadius > 0
					fRadius = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fVehDropOffVisualRadius
				ENDIF
			ELSE
				IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[iteam].vDropOff2[iRule])
					IF iveh != iFirstHighPriorityVehicleThisRule[iteam]
						PRINTLN("[RCC MISSION] GET_COORDS_FOR_LOCATE_MARKER - Radius drop off, using vDropOff2 as this isn't the first priority vehicle on this rule - vehicle dropping off = ",iveh,", iFirstHighPriorityVehicleThisRule = ",iFirstHighPriorityVehicleThisRule[iteam])
						vCoords = g_FMMC_STRUCT.sFMMCEndConditions[iteam].vDropOff2[iRule]
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ELIF g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffType[ iRule ] = ciFMMC_DROP_OFF_TYPE_PROPERTY
		vCoords = GET_PROPERTY_DROP_OFF_COORDS( eApartmentBuzzerEntrance )
	ELIF g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffType[ iRule ] = ciFMMC_DROP_OFF_TYPE_LEADER_GARAGE
		vCoords = GET_PROPERTY_DROP_OFF_COORDS( eGarageExterior )
	ELIF g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffType[ iRule ] = ciFMMC_DROP_OFF_TYPE_AREA
		vcenter = ( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].vDropOff[ iRule ] + g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].vDropOff2[ iRule ] )
		vcenter *= << 0.5, 0.5, 0.5 >>
		IF GET_WATER_HEIGHT_NO_WAVES(vcenter, fwater)
			vcenter.z = fwater
		ELSE
			GET_GROUND_Z_FOR_3D_COORD(vcenter,vcenter.z)
		ENDIF
		
		vCoords = vcenter

	ELIF g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffType[ iRule ] = ciFMMC_DROP_OFF_TYPE_VEHICLE
		
		IF IS_CURRENT_VEHICLE_RULE_OF_TYPE_CARGOBOB_FOR_TEAM(iTeam, iRule)
			INT iveh = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOff_Vehicle[iRule]
			
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
				VEHICLE_INDEX tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
				IF IS_VEHICLE_DRIVEABLE(tempVeh)
					vcenter = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempVeh,<<0,1.8,-6>>)
					
					FLOAT fZ
					IF GET_GROUND_Z_FOR_3D_COORD(vcenter, fZ)
						vcenter.z = fZ
					ENDIF
					
					vCoords = vcenter
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN vCoords
	
ENDFUNC

PROC DRAW_ACTUAL_SIZE_ANGLED_AREA_DROP_OFF(INT iTeam, INT iRule, VECTOR vCoords)
	FLOAT fLength = GET_DISTANCE_BETWEEN_COORDS(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[iRule], g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff2[iRule])
	FLOAT fHeading = GET_HEADING_BETWEEN_VECTORS(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[iRule], g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff2[iRule])
	VECTOR vBoxSize = <<g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffRadius[iRule], fLength, 0.85>>
	
	INT iR, iG, iB, iA
	GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
	
	DRAW_MARKER(MARKER_BOXES,
	vCoords + <<0, 0, -0.25>>,
	<<0, 0, 0>>,
	<<0, 0, fHeading>>,
	vBoxSize,
	iR,
	iG,
	iB,
	iA)
ENDPROC

PROC DRAW_GROUND_DROP_OFF(FLOAT fRadius, INT iAlphaOverride = -1)
	
	VECTOR vcenter
	//FLOAT fwater
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
	VECTOR vCoords = GET_COORDS_FOR_LOCATE_MARKER(fRadius)
	//FLOAT fDirection = GET_DIRECTION_FOR_LOCATE_MARKER()	
	//PRINTLN("[RCC MISSION] DRAW_GROUND_DROP_OFF Called fRadius = ", fRadius, " iAlphaOverride = ",iAlphaOverride)
	
	IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
		PRINTLN("DRAW_GROUND_DROP_OFF - Called - but exitting due to PBBOOL_OBJECTIVE_BLOCKER.")
		EXIT
	ENDIF
	
	SWITCH g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffType[ iRule ]
		
		CASE ciFMMC_DROP_OFF_TYPE_CYLINDER
		CASE ciFMMC_DROP_OFF_TYPE_SPHERE
		CASE ciFMMC_DROP_OFF_TYPE_PROPERTY
		CASE ciFMMC_DROP_OFF_TYPE_LEADER_GARAGE
		CASE ciFMMC_DROP_OFF_TYPE_AREA
		
			IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[iRule] = ciFMMC_DROP_OFF_TYPE_AREA
			AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iExactDropOffBitSet, iRule)
				DRAW_ACTUAL_SIZE_ANGLED_AREA_DROP_OFF(iTeam, iRule, vCoords)
			ELSE
				DRAW_LOCATE_MARKER( vCoords, fradius, TRUE, 0, iAlphaOverride  )
				PRINTLN("DRAW_GROUND_DROP_OFF - Called, vCoords: ", vCoords, " fradius: ", fradius)
			ENDIF
		BREAK
		
		CASE ciFMMC_DROP_OFF_TYPE_VEHICLE
			
			IF IS_CURRENT_VEHICLE_RULE_OF_TYPE_CARGOBOB_FOR_TEAM(iTeam, iRule)
				INT iveh
				iveh = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOff_Vehicle[iRule]
				
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
					VEHICLE_INDEX tempVeh
					tempveh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
					IF IS_VEHICLE_DRIVEABLE(tempVeh)
						vcenter = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempVeh,<<0,1.8,-6>>)
						
						FLOAT fZ
						IF GET_GROUND_Z_FOR_3D_COORD(vcenter, fZ)
							vcenter.z = fZ
						ENDIF
						
						fRadius = 10.0		
						
						DRAW_LOCATE_MARKER(vcenter, fRadius, TRUE, 0, iAlphaOverride)						
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
	ENDSWITCH
	
ENDPROC

PROC DRAW_AIR_DROP_OFF()

	VECTOR vDropOff
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iDropOffType[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] = ciFMMC_DROP_OFF_TYPE_CYLINDER
	OR g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iDropOffType[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] = ciFMMC_DROP_OFF_TYPE_SPHERE
		FLOAT fTemp
		vDropOff = GET_COORDS_FOR_LOCATE_MARKER(fTemp)
	ELIF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iDropOffType[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] =ciFMMC_DROP_OFF_TYPE_PROPERTY
		vDropOff = GET_PROPERTY_DROP_OFF_COORDS( eApartmentBuzzerEntrance )
	ELIF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iDropOffType[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] =ciFMMC_DROP_OFF_TYPE_LEADER_GARAGE
		vDropOff = GET_PROPERTY_DROP_OFF_COORDS( eGarageExterior )
	ELIF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iDropOffType[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] = ciFMMC_DROP_OFF_TYPE_AREA
		vDropOff = (g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].vDropOff[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] + g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].vDropOff2[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]])
		vDropOff *= << 0.5, 0.5, 0.5 >>
	ELSE //It's a vehicle drop off
		//Shouldn't have a drop off corona drawn
		vDropOff = <<0, 0, 0>>
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(vDropOff)
		DRAW_AIR_MARKER(vDropOff, TRUE)
	ENDIF
	
ENDPROC

FUNC INT GET_RULE_GOTO_TEAMS(INT iTeam, INT iRule, BOOL& bCheckOnRule)
	
	INT iTeamLoop
	INT iTeamsToCheck = 0
	bCheckOnRule = TRUE
	
	//Get the setting from the rule of the vehicle
	IF IS_BIT_SET(g_FMMC_STRUCT.iVehRuleWaitForTeamsBS[iRule][iTeam], ciFMMC_VehRuleTeamBS_ALL_TEAM_GOTO)
		SET_BIT(iTeamsToCheck,iTeam)
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iVehRuleWaitForTeamsBS[iRule][iTeam],ciFMMC_VehRuleTeamBS_ALL_FRIENDLY_GOTO)
		FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS-1
			IF DOES_TEAM_LIKE_TEAM(iTeam,iTeamLoop)
				SET_BIT(iTeamsToCheck,iTeamLoop)
			ENDIF
		ENDFOR
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iVehRuleWaitForTeamsBS[iRule][iTeam],ciFMMC_VehRuleTeamBS_EVERYONE_GOTO)
		FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS-1
			SET_BIT(iTeamsToCheck,iTeamLoop)
		ENDFOR
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iVehRuleWaitForTeamsBS[iRule][iTeam],ciFMMC_VehRuleTeamBS_CUSTOMTEAMS_GOTO)
		FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS-1
			IF IS_BIT_SET(g_FMMC_STRUCT.iVehRuleWaitForTeamsBS[iRule][iTeam],ciFMMC_VehRuleTeamBS_CUSTOMTEAMS_GOTO_T0 + iTeamLoop)
				SET_BIT(iTeamsToCheck,iTeamLoop)
			ENDIF
		ENDFOR
	ELSE
		//Only need one player in vehicle
		iTeamsToCheck = 0
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iVehRuleWaitForTeamsBS[iRule][iTeam],ciFMMC_VehRuleTeamBS_IGNORE_CHECK_FOR_ON_VEH_RULE)
		bCheckOnRule = FALSE
	ENDIF
	
	RETURN iTeamsToCheck
	
ENDFUNC

FUNC BOOL IS_WHOLE_GROUP_IN_VEHICLE(GROUP_INDEX giGroup, VEHICLE_INDEX tempVeh, INT& iFollowersInVehCount)
	
	BOOL bInVehicle = FALSE
	
	INT iHasLeader, iFollowers, iFollowersInVeh
	PED_INDEX leaderPed
	PED_INDEX tempPed
	
	IF DOES_GROUP_EXIST(giGroup)
		
		GET_GROUP_SIZE(giGroup, iHasLeader, iFollowers)
		
		IF iHasLeader >= 1
			
			leaderPed = GET_PED_AS_GROUP_LEADER(giGroup)
			
			IF NOT IS_PED_INJURED(leaderPed)
				
				IF IS_PED_IN_VEHICLE(leaderPed, tempVeh)
					
					IF iFollowers > 0
						
						INT i
						
						FOR i = 0 TO (iFollowers-1)
							tempPed = GET_PED_AS_GROUP_MEMBER(giPlayerGroup,i)
							
							IF NOT IS_PED_INJURED(tempPed)
								IF IS_PED_IN_VEHICLE(tempPed,tempVeh)
									iFollowersInVeh++
									iFollowersInVehCount++
								ENDIF
							ELSE
								iFollowersInVeh++ // Dead peds aren't going to get in the car, never mind about those guys
							ENDIF
						ENDFOR
						
						IF iFollowersInVeh >= iFollowers
							bInVehicle = TRUE
						ENDIF
					ELSE
						bInVehicle = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	RETURN bInVehicle
	
ENDFUNC

///PURPOSE: This does all the logic specific to dropping off entities at a vehicle
FUNC BOOL IS_PLAYER_IN_VEHICLE_DROPOFF( INT iTeam, INT iRule, FLOAT fRadius, FLOAT fCoronaHeight, BOOL bOnlyCheckForNear = FALSE )
	
	BOOL bReturn = FALSE

	IF ( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOff_Vehicle[ iRule ] > -1 )
	
		INT iVeh = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOff_Vehicle[ iRule ]
		NETWORK_INDEX niVeh = MC_serverBD_1.sFMMC_SBD.niVehicle[ iVeh ]
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(niVeh)
		AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(niVeh))
			
			VEHICLE_INDEX tempVeh = NET_TO_VEH( niVeh )
			
			IF NOT IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iVehDropOff_GetInBS, iRule )
			OR bOnlyCheckForNear
				
				IF NOT IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iVehDropOff_HookUpBS, iRule )
				OR NOT ( (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = CARGOBOB) OR (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = CARGOBOB2) OR (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = CARGOBOB3) )
				
					IF IS_CURRENT_SPECIAL_PICKUP_VALID()
						IF IS_SPECIAL_PICKUP_READY_FOR_DELIVERY( sCurrentSpecialPickupState.iCurrentlyRequestedObj )
							bReturn = TRUE
						ENDIF
					ELSE
					
						IF bOnlyCheckForNear
						AND IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iVehDropOff_GetInBS, iRule )
							fradius = 15
						ENDIF
						
						IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed, tempVeh) <= fradius
							IF IS_ENTITY_AT_COORD(LocalPlayerPed,GET_ENTITY_COORDS(tempVeh),<<fradius,fradius,fcoronaheight>>,FALSE)	
								bReturn = TRUE
							ENDIF
						ENDIF
					ENDIF
					
				ELSE
				
					//Magnet stuff:
					
					IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
						
						VEHICLE_INDEX playerVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
						
						IF IS_ENTITY_ATTACHED_TO_ENTITY(playerVeh,tempVeh)
							PRINTLN("[RCC MISSION] MAGNET - We're in the drop off (already attached)")
							bReturn = TRUE
						ELSE
							VECTOR vHookLoc = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempVeh,<<0,1.5,-7.2>>)
							VECTOR vMin, vMax
							GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(playerVeh),vMin,vMax)
							VECTOR vRoofLoc = GET_ENTITY_COORDS(playerVeh) + <<0,0,vMax.z>>
							
							//OBJECT_INDEX oiMagnet
							
							IF DOES_CARGOBOB_HAVE_PICKUP_MAGNET(tempVeh)
								
								vHookLoc = GET_ATTACHED_PICK_UP_HOOK_POSITION(tempVeh)
								
								IF IS_VECTOR_ZERO(vHookLoc)
									OBJECT_INDEX oiMagnet = GET_CLOSEST_OBJECT_OF_TYPE(vHookLoc, 10.0, HEI_PROP_HEIST_MAGNET, false, false, false)
									
									IF DOES_ENTITY_EXIST(oiMagnet)
										vHookLoc = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oiMagnet, <<0,0,-1.934200>>)
									ENDIF
								ELSE
									vHookLoc += <<0,0,-1.934200>>
								ENDIF
								
							ELIF DOES_CARGOBOB_HAVE_PICK_UP_ROPE(tempVeh)
								vHookLoc = GET_ATTACHED_PICK_UP_HOOK_POSITION(tempVeh)
							ENDIF
							
							IF NOT bOnlyCheckForNear
								fradius = 15.0
							ELSE
								fradius = 25.0
							ENDIF
							
							IF IS_ENTITY_ATTACHED_TO_ENTITY(playerVeh, tempVeh)
								PRINTLN("[RCC MISSION] MAGNET - We're in the drop off (already attached to cargobob)")
								bReturn = TRUE
							ELSE
								FLOAT fDist = GET_DISTANCE_BETWEEN_COORDS(vHookLoc,vRoofLoc, FALSE)
								
								IF fDist <= 1.5
								//AND ((vHookLoc.z - vRoofLoc.z) > -0.1)
								AND ((vHookLoc.z - vRoofLoc.z) < 3)
									bReturn = TRUE
									bIsInMagnetRange = TRUE
									PRINTLN("[RCC MISSION] MAGNET - We're in the drop off")
								ELSE
								#IF IS_DEBUG_BUILD
									PRINTLN("[RCC MISSION] MAGNET - Distance from drop off = ",GET_DISTANCE_BETWEEN_COORDS(vHookLoc,vRoofLoc))
								#ENDIF
									bIsInMagnetRange = FALSE
								ENDIF
							ENDIF
						ENDIF
					ELSE
						PRINTLN("[RCC MISSION] MAGNET - Player ped isn't in a vehicle")
					ENDIF
				ENDIF
			ELSE
				
				IF IS_VEHICLE_DRIVEABLE(tempVeh)
					IF IS_PED_IN_VEHICLE(LocalPlayerPed,tempVeh)
						
						INT iFollowersInVeh
						
						IF IS_WHOLE_GROUP_IN_VEHICLE(giPlayerGroup, tempVeh, iFollowersInVeh)
							
							BOOL bCheckOnRule
							INT iTeamCheckBS = GET_RULE_GOTO_TEAMS(iTeam, iRule, bCheckOnRule)
							
							IF iTeamCheckBS > 0
							AND ( IS_BIT_SET(g_FMMC_STRUCT.iVehRuleWaitForTeamsBS[iRule][iTeam], ciFMMC_VehRuleTeamBS_DISABLE_TEAMINVEH_PASS_ON_FULL_VEH)
								OR NOT IS_VEHICLE_FULL(tempVeh) )
								
								INT iLoop, iPlayersNeeded, iPlayersChecked, iPlayersInCar
								
								FOR iLoop = 0 TO (FMMC_MAX_TEAMS - 1)
									IF IS_BIT_SET(iTeamCheckBS, iLoop)
										iPlayersNeeded += MC_serverBD.iNumberOfPlayingPlayers[iLoop]
									ENDIF
								ENDFOR
								
								FOR iLoop = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
									
									IF iLoop != iPartToUse
										IF ( NOT IS_BIT_SET(MC_playerBD[iLoop].iClientBitSet,PBBOOL_ANY_SPECTATOR)
											 OR IS_BIT_SET(MC_playerBD[iLoop].iClientBitSet,PBBOOL_HEIST_SPECTATOR) )
										AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iLoop))
										AND IS_BIT_SET(iTeamCheckBS, MC_PlayerBD[iLoop].iteam)
											
											iPlayersChecked++
											
											PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iLoop))
											
											IF IS_NET_PLAYER_OK(tempPlayer)
												IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(tempPlayer), tempVeh)
													IF IS_WHOLE_GROUP_IN_VEHICLE(GET_PLAYER_GROUP(tempPlayer), tempVeh, iFollowersInVeh)
														iPlayersInCar++
													#IF IS_DEBUG_BUILD
													ELSE
														PRINTLN("[RCC MISSION] IS_PLAYER_IN_VEHICLE_DROPOFF - Waiting for particpant ",iLoop,"'s group to enter the vehicle")
													#ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ELSE
										iPlayersChecked++
										iPlayersInCar++
									ENDIF
									
									// If we've checked everyone we need to:
									IF iPlayersChecked >= iPlayersNeeded
										iLoop = MAX_NUM_MC_PLAYERS // Break out!
									ENDIF
								ENDFOR
								
								IF iPlayersInCar >= iPlayersNeeded
									IF iFollowersInVeh >= MC_serverBD.iRequiredDeliveries[iTeam]
										bReturn = TRUE
										PRINTLN("[RCC MISSION] IS_PLAYER_IN_VEHICLE_DROPOFF - All needed / group peds are in the drop off vehicle with the player all players needed (iTeamCheckBS = ",iTeamCheckBS,"), returning true...")
									#IF IS_DEBUG_BUILD
									ELSE
										PRINTLN("[RCC MISSION] IS_PLAYER_IN_VEHICLE_DROPOFF - All players and their groups are in the car, but not enough peds are in! The group data is probably out of sync.")
									#ENDIF
									ENDIF
								ENDIF
								
							ELSE
								bReturn = TRUE
								PRINTLN("[RCC MISSION] IS_PLAYER_IN_VEHICLE_DROPOFF - All group peds are in the drop off vehicle with the player and no extra players needed in car (or vehicle is full), returning true...")
							ENDIF
						#IF IS_DEBUG_BUILD
						ELSE
							PRINTLN("[RCC MISSION] IS_PLAYER_IN_VEHICLE_DROPOFF - Not all of my peds are in the vehicle!")
						#ENDIF
						ENDIF
						
					ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[RCC MISSION] IS_PLAYER_IN_VEHICLE_DROPOFF - Drop off vehicle is no longer driveable!")
				#ENDIF
				ENDIF
			ENDIF
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[RCC MISSION] IS_PLAYER_IN_VEHICLE_DROPOFF - Drop off vehicle doesn't exist!")
		#ENDIF
		ENDIF
	ENDIF
	
	RETURN bReturn
ENDFUNC


FUNC BOOL IS_CURRENT_RULE_AN_APARTMENT_OR_GARAGE_DROPOFF()
	BOOL bReturn = FALSE
	INT iTeam = MC_playerBD[ iPartToUse ].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[ iTeam ]
	
	IF iRule < FMMC_MAX_RULES
		IF g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffType[ iRule ] = ciFMMC_DROP_OFF_TYPE_LEADER_GARAGE
		OR g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffType[ iRule ] = ciFMMC_DROP_OFF_TYPE_PROPERTY
			bReturn = TRUE
		ENDIF
	ENDIF
	
	RETURN bReturn
ENDFUNC

FUNC BOOL IS_PLAYER_IN_DROP_OFF_WHEN_SWAN_DIVING(INT iTeam, INT iRule)
	IF IS_ENTITY_IN_ANGLED_AREA( LocalPlayerPed,
								g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].vDropOff[ iRule ],
								g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].vDropOff2[ iRule ],
								g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].fDropOffRadius[ iRule ] + cfDIVE_SCORE_EXTRA_DROP_OFF_RADIUS,
								FALSE )
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

///PURPOSE: Returns true if the player is at one of the four kinds of dropoffs - 
///    Radius - at a coord
///    Property - at some predefined location, referred to as a "property"
///    Some other thing which is probably never used
///    Vehicle - this will either work if you're near a vehicle or if you (and all the peds (if any) following you) get into it
///    
FUNC BOOL IS_PLAYER_IN_DROP_OFF( FLOAT fradius, FLOAT fcoronaheight )
	
	BOOL bInDropOff = FALSE
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[ iTeam ]
	#IF IS_DEBUG_BUILD
	IF bPlayerInDropOffPrints
		INT iLocalPartId = iLocalPart
		PRINTLN("[RCC MISSION] - IS_PLAYER_IN_DROP_OFF - being called.")
		PRINTLN("[RCC MISSION] - IS_PLAYER_IN_DROP_OFF - my participant ID = ", iLocalPartId)
		PRINTLN("[RCC MISSION] - IS_PLAYER_IN_DROP_OFF - iPartToUse = ", iPartToUse)
		PRINTLN("[RCC MISSION] - IS_PLAYER_IN_DROP_OFF - iTeam = ", iTeam)
		PRINTLN("[RCC MISSION] - IS_PLAYER_IN_DROP_OFF - iRule = ", iRule)
		PRINTLN("[RCC MISSION] - IS_PLAYER_IN_DROP_OFF - fradius = ", fradius)
		PRINTLN("[RCC MISSION] - IS_PLAYER_IN_DROP_OFF - fcoronaheight = ", fcoronaheight)
		PRINTLN("[RCC MISSION] - IS_PLAYER_IN_DROP_OFF - loc = ", g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].vDropOff[ iRule ])
	ENDIF
	#ENDIF
	
	IF iRule < FMMC_MAX_RULES
	
		#IF IS_DEBUG_BUILD
		IF bPlayerInDropOffPrints
		PRINTLN("[RCC MISSION] - IS_PLAYER_IN_DROP_OFF - iRule < FMMC_MAX_RULES (", FMMC_MAX_RULES, ").")
		ENDIF
		#ENDIF
		
		// Radius dropoff
		IF g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffType[ iRule ] = ciFMMC_DROP_OFF_TYPE_CYLINDER
		
			#IF IS_DEBUG_BUILD
			IF bPlayerInDropOffPrints
				PRINTLN("[RCC MISSION] - IS_PLAYER_IN_DROP_OFF - iDropOffType = ciFMMC_DROP_OFF_TYPE_CYLINDER.")
			ENDIF
			#ENDIF
			
			VECTOR vDropOff = GET_DROP_OFF_CENTER()
			VECTOR vPlayerCoords = GET_ENTITY_COORDS(LocalPlayerPed, FALSE)
			
			IF WVM_FLOW_IS_CURRENT_MISSION_WVM_MISSION()
				//LOGIC WAS DIFFERENT FOR THIS PACK!
				IF VDIST2(<<vPlayerCoords.x, vPlayerCoords.y, 0>>, <<vDropOff.x, vDropOff.y, 0>>) <= POW(fradius,2)
					bInDropOff = TRUE
				ENDIF
			ELIF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
			OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_USE_NEW_DROP_OFF_CHECKS)
				FLOAT fZDist = ABSF(vPlayerCoords.Z - vDropOff.Z)
				// B* 4023028 - Added back the height check as the above VDIST2 removed any consideration for height causing this bug
				IF (fZDist >= 0 AND fZDist <= (fcoronaheight / 2)) OR fCoronaHeight = 0
					IF VDIST2(<<vPlayerCoords.x, vPlayerCoords.y, 0>>, <<vDropOff.x, vDropOff.y, 0>>) <= POW(fradius,2)
						bInDropOff = TRUE
					ENDIF
				ENDIF
			ELSE
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed, vDropOff, FALSE) <= fradius
					IF IS_ENTITY_AT_COORD(LocalPlayerPed, vDropOff, <<fradius,fradius,fcoronaheight>>, FALSE)	
						bInDropOff = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
			IF IS_BIT_SET(m_sharedDebugFlags, DBG_LOCATES_ON)
				IF fcoronaheight = 0
					DRAW_WIRE_CYLINDER(vDropOff, vDropoff + <<0,0,0.1>>, fradius, 0, 255, 0, 255)
				ELSE
					DRAW_WIRE_CYLINDER(vDropOff, vDropoff + <<0,0,fcoronaheight>>, fradius, 0, 255, 0, 255)
				ENDIF
			ENDIF
			#ENDIF
			
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffType[ iRule ] = ciFMMC_DROP_OFF_TYPE_SPHERE
			
			#IF IS_DEBUG_BUILD
			IF bPlayerInDropOffPrints
				PRINTLN("[RCC MISSION] - IS_PLAYER_IN_DROP_OFF - iDropOffType = ciFMMC_DROP_OFF_TYPE_SPHERE.")
			ENDIF
			#ENDIF
			
			IF IS_ENTITY_AT_COORD(LocalPlayerPed, GET_DROP_OFF_CENTER(), <<fradius,fradius,fradius>>)
			AND VDIST2(GET_DROP_OFF_CENTER(), GET_ENTITY_COORDS(LocalPlayerPed, FALSE)) <= POW(fradius,2)
				bInDropOff = TRUE
			ENDIF
			
			#IF IS_DEBUG_BUILD
			IF IS_BIT_SET(m_sharedDebugFlags, DBG_LOCATES_ON)
				DRAW_DEBUG_SPHERE(GET_DROP_OFF_CENTER(), fradius, 0, 255, 0, 120)
			ENDIF
			#ENDIF
			
		// Garage dropoff
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffType[ iRule ] = ciFMMC_DROP_OFF_TYPE_LEADER_GARAGE
			
			#IF IS_DEBUG_BUILD
			IF bPlayerInDropOffPrints
			PRINTLN("[RCC MISSION] - IS_PLAYER_IN_DROP_OFF - iDropOffType = ciFMMC_DROP_OFF_TYPE_LEADER_GARAGE.")
			ENDIF
			#ENDIF
			
			IF g_HeistGarageDropoffPanStarted
				bInDropOff = TRUE
			ENDIF
			
			/* Leaving this here just in case we want range-based dropoffs
			ELSE
			 	IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed,GET_PROPERTY_DROP_OFF_COORDS( eGarageExterior ),FALSE) <= fradius
					IF IS_ENTITY_AT_COORD(LocalPlayerPed,GET_PROPERTY_DROP_OFF_COORDS( eGarageExterior ),<< fradius, fradius, fcoronaheight >>,FALSE)	
						bInDropOff = TRUE
					ENDIF
				ENDIF
			*/
			
		//Heist Apartment Dropoff
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffType[ iRule ] = ciFMMC_DROP_OFF_TYPE_PROPERTY
			
			#IF IS_DEBUG_BUILD
			IF bPlayerInDropOffPrints
			PRINTLN("[RCC MISSION] - IS_PLAYER_IN_DROP_OFF - iDropOffType = ciFMMC_DROP_OFF_TYPE_PROPERTY.")
			ENDIF
			#ENDIF

			IF g_HeistApartmentDropoffPanStarted AND g_HeistApartmentReadyForDropOffCut
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] In apartment dropoff cutscene." )
				bInDropOff = TRUE
			ENDIF
			/* Leaving this here just in case we want range-based dropoffs
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed,GET_PROPERTY_DROP_OFF_COORDS( eApartmentBuzzerEntrance ),FALSE) <= fradius
				IF IS_ENTITY_AT_COORD(LocalPlayerPed,GET_PROPERTY_DROP_OFF_COORDS( eApartmentBuzzerEntrance ),<<fradius,fradius,fcoronaheight>>,FALSE)	
					bInDropOff = TRUE
				ENDIF
			ENDIF
			*/
		// Area dropoff
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffType[ iRule ] = ciFMMC_DROP_OFF_TYPE_AREA
			
			#IF IS_DEBUG_BUILD
			IF bPlayerInDropOffPrints
			PRINTLN("[RCC MISSION] - IS_PLAYER_IN_DROP_OFF - iDropOffType = ciFMMC_DROP_OFF_TYPE_AREA.")
			ENDIF
			#ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_DIVE_SCORE_ANIM)
			AND IS_BIT_SET(iLocalBoolCheck16, LBOOL16_IS_DIVE_SCORE_ANIM_IN_PROGRESS)
				bInDropOff = IS_PLAYER_IN_DROP_OFF_WHEN_SWAN_DIVING(iTeam, iRule)
			ELSE
				IF IS_ENTITY_IN_ANGLED_AREA( LocalPlayerPed,
											g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].vDropOff[ iRule ],
											g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].vDropOff2[ iRule ],
											g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].fDropOffRadius[ iRule ],
											FALSE )
					bInDropOff = TRUE
				ENDIF
			ENDIF
		//We are dropping something off to a vehicle
		ELSE
			
			#IF IS_DEBUG_BUILD
			IF bPlayerInDropOffPrints
			PRINTLN("[RCC MISSION] - IS_PLAYER_IN_DROP_OFF - checking IS_PLAYER_IN_VEHICLE_DROPOFF.")
			ENDIF
			#ENDIF
			
			bInDropOff = IS_PLAYER_IN_VEHICLE_DROPOFF( iTeam, iRule, fRadius, fCoronaHeight )
			
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bPlayerInDropOffPrints
	IF iRule < FMMC_MAX_RULES
	AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_GET_DELIVER_LEAVE_AREA)	
		PRINTLN("[RCC MISSION] - IS_PLAYER_IN_DROP_OFF - bInDropOff final value = ", !bInDropOff)
	ELSE
		PRINTLN("[RCC MISSION] - IS_PLAYER_IN_DROP_OFF - bInDropOff final value = ", bInDropOff)
	ENDIF
	ENDIF
	#ENDIF
			
	IF iRule < FMMC_MAX_RULES
	AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_GET_DELIVER_LEAVE_AREA)	
		RETURN !bInDropOff
	ELSE
		RETURN bInDropOff
	ENDIF
ENDFUNC

FUNC FLOAT GET_DROP_OFF_HEIGHT(INT iTeam, INT iRule)

	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffHeight[iRule] != IGNORE_DROP_OFF_HEIGHT
		RETURN g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffHeight[iRule]
	ENDIF
	
	FLOAT fcoronaheight = LOCATE_SIZE_HEIGHT
	
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		IF IS_VEHICLE_DRIVEABLE(tempVeh)
			IF IS_PED_IN_ANY_HELI(LocalPlayerPed)
			OR IS_PED_IN_ANY_PLANE(LocalPlayerPed)
			OR IS_BIG_VEHICLE(tempVeh)
			OR GET_ENTITY_MODEL(tempVeh) = RIOT
			OR GET_ENTITY_MODEL(tempVeh) = RIOT2
			OR IS_PED_IN_ANY_BOAT(LocalPlayerPed)
			OR GET_ENTITY_MODEL(tempVeh) = SUBMERSIBLE
			OR GET_ENTITY_MODEL(tempVeh) = STROMBERG
			OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[iRule], ciBS_RULE8_OVERRIDE_CHECK_VERICALLY)
			OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffBitSet,  iRule )
			OR IS_ENTITY_IN_WATER(LocalPlayerPed)
				fcoronaheight = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffRadius[iRule]
			ENDIF
		ENDIF
	ENDIF
	
	RETURN fCoronaHeight
	
ENDFUNC

FUNC BOOL IS_PLAYER_NEAR_DROP_OFF()
	
	BOOL bInDropOff = FALSE
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	FLOAT fRadius = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffRadius[iRule]
	
	FLOAT fCoronaHeight = GET_DROP_OFF_HEIGHT(iTeam, iRule)
	
	//Near = within 10m
	fradius += 10
	fcoronaheight += 2
	
	IF iRule < FMMC_MAX_RULES
		
		// Cylinder and sphere dropoff
		IF g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffType[ iRule ] = ciFMMC_DROP_OFF_TYPE_CYLINDER
		OR g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffType[ iRule ] = ciFMMC_DROP_OFF_TYPE_SPHERE
			VECTOR vDropOff = GET_DROP_OFF_CENTER()
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed, vDropOff, FALSE) <= fradius
				IF IS_ENTITY_AT_COORD(LocalPlayerPed, vDropOff, <<fradius,fradius,fcoronaheight>>, FALSE)
					bInDropOff = TRUE
				ENDIF
			ENDIF
		// Garage dropoff
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffType[ iRule ] = ciFMMC_DROP_OFF_TYPE_LEADER_GARAGE
			
			IF g_HeistGarageDropoffPanStarted
				bInDropOff = TRUE
			ELSE
			 	IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed,GET_PROPERTY_DROP_OFF_COORDS( eGarageExterior ),FALSE) <= fradius
					IF IS_ENTITY_AT_COORD(LocalPlayerPed,GET_PROPERTY_DROP_OFF_COORDS( eGarageExterior ),<< fradius, fradius, fcoronaheight >>,FALSE)	
						bInDropOff = TRUE
					ENDIF
				ENDIF
			ENDIF
			
		//Heist Apartment Dropoff
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffType[ iRule ] = ciFMMC_DROP_OFF_TYPE_PROPERTY
			
			IF g_HeistApartmentDropoffPanStarted AND g_HeistApartmentReadyForDropOffCut
				bInDropOff = TRUE
			ELSE
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed,GET_PROPERTY_DROP_OFF_COORDS( eApartmentBuzzerEntrance ),FALSE) <= fradius
					IF IS_ENTITY_AT_COORD(LocalPlayerPed,GET_PROPERTY_DROP_OFF_COORDS( eApartmentBuzzerEntrance ),<<fradius,fradius,fcoronaheight>>,FALSE)	
						bInDropOff = TRUE
					ENDIF
				ENDIF
			ENDIF
			
		// Area dropoff
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffType[ iRule ] = ciFMMC_DROP_OFF_TYPE_AREA
			
			IF IS_ENTITY_IN_ANGLED_AREA( LocalPlayerPed,
										g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].vDropOff[ iRule ],
										g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].vDropOff2[ iRule ],
										g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].fDropOffRadius[ iRule ],
										FALSE )
				bInDropOff = TRUE
			ENDIF
		//We are dropping something off to a vehicle
		ELSE
			
			bInDropOff = IS_PLAYER_IN_VEHICLE_DROPOFF( iTeam, iRule, fRadius, fCoronaHeight, TRUE )
			
		ENDIF
	ENDIF
	
	RETURN bInDropOff
	
ENDFUNC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE and related functions !
//
//************************************************************************************************************************************************************



FUNC BOOL SHUT_VEHICLE_DOORS(VEHICLE_INDEX tempveh)
	
	IF IS_VEHICLE_DRIVEABLE(tempveh)
	
		PRINTLN("[RCC MISSION] SHUT_VEHICLE_DOORS called on veh: ",NATIVE_TO_INT(tempveh))
		
		IF NOT IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(tempveh))
			IF NOT HAS_NET_TIMER_STARTED(tddoorsafteytimer)
				START_NET_TIMER(tddoorsafteytimer)
			ELSE
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tddoorsafteytimer) < 2000
					IF GET_VEHICLE_DOOR_ANGLE_RATIO(tempveh, SC_DOOR_FRONT_LEFT) > 0
						SET_VEHICLE_DOOR_CONTROL(tempveh, SC_DOOR_FRONT_LEFT, DT_DOOR_INTACT, 0.0)
						RETURN FALSE
					ENDIF
					IF GET_VEHICLE_DOOR_ANGLE_RATIO(tempveh, SC_DOOR_FRONT_RIGHT) > 0
						SET_VEHICLE_DOOR_CONTROL(tempveh, SC_DOOR_FRONT_RIGHT, DT_DOOR_INTACT, 0.0)
						RETURN FALSE
					ENDIF
					IF GET_VEHICLE_DOOR_ANGLE_RATIO(tempveh, SC_DOOR_REAR_LEFT) > 0
						SET_VEHICLE_DOOR_CONTROL(tempveh, SC_DOOR_REAR_LEFT, DT_DOOR_INTACT, 0.0)
						RETURN FALSE
					ENDIF
					IF GET_VEHICLE_DOOR_ANGLE_RATIO(tempveh, SC_DOOR_REAR_RIGHT) > 0
						SET_VEHICLE_DOOR_CONTROL(tempveh, SC_DOOR_REAR_RIGHT, DT_DOOR_INTACT, 0.0)
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
		RESET_NET_TIMER(tddoorsafteytimer)
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_PROP_LINKED_TO_LOCATE_PRIMARY(INT iTempPart, INT iLoc)
	INT iProp
	FOR iProp = 0 TO ci_PLAYER_CLAIM_PROP_MAX-1
		IF MC_PlayerBD[iTempPart].iCurrentPropHit[iProp] = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocatePropLinkIndex
		AND MC_PlayerBD[iTempPart].iCurrentPropHit[iProp] > -1
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PROP_LINKED_TO_LOCATE_SECONDARY(INT iTempPart, INT iLoc)
	INT iProp
	FOR iProp = 0 TO ci_PLAYER_CLAIM_PROP_MAX-1
		IF MC_PlayerBD[iTempPart].iCurrentPropHit[iProp] = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocatePropLinkIndexSecondary
		AND MC_PlayerBD[iTempPart].iCurrentPropHit[iProp] > -1
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PROP_LINKED_TO_LOCATE_TERTIARY(INT iTempPart, INT iLoc)
	INT iProp
	FOR iProp = 0 TO ci_PLAYER_CLAIM_PROP_MAX-1
		IF MC_PlayerBD[iTempPart].iCurrentPropHit[iProp] = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocatePropLinkIndexTertiary
		AND MC_PlayerBD[iTempPart].iCurrentPropHit[iProp] > -1
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

// 0 = FALSE, 1 = TRUE, 2 = DO NOTHING
FUNC INT IS_PED_IN_LOCATION_AND_ON_PROP_LOGIC(PED_INDEX tempPed, INT iloc, INT iTeam)
	INT iTempPart = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(NETWORK_GET_PLAYER_INDEX_FROM_PED(tempPed)))
	
	VEHICLE_INDEX vehPlayer
	BOOL bVehicleDriveable = TRUE
	
	IF IS_PED_IN_ANY_VEHICLE(tempPed)
		vehPlayer = GET_VEHICLE_PED_IS_IN(tempPed)
		bVehicleDriveable = IS_VEHICLE_DRIVEABLE(vehPlayer)
	ENDIF	
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
	
		// We have already played the score shard.
		IF HAS_NET_TIMER_STARTED(tdOvertimeLocateDelayTimer)
			PRINTLN("[RCC MISSION][OVERTIME] - IS_PED_IN_LOCATION - RETURN TRUE    tdOvertimeLocateDelayTimer")
			RETURN 1 // TRUE
		ENDIF
	
		IF IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_TEAM_0_SHOULD_RESPAWN_BACK_AT_START_NOW + iTeam)
		OR IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)							
		OR IS_BIT_SET(iLocalBoolCheck23, LBOOL23_OVERTIME_MODE_SWITCH_OBJ_BLOCK)
		OR (HAS_NET_TIMER_STARTED(tdOvertimeScoredShardTimer) AND NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(tdOvertimeScoredShardTimer, ci_OVERTIME_SCORED_SHARD_TIME))
			PRINTLN("[RCC MISSION][OVERTIME] - IS_PED_IN_LOCATION - RETURN FALSE    tdOvertimeScoredShardTimer/LBOOL23_OVERTIME_MODE_SWITCH_OBJ_BLOCK/LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME/SBBOOL3_TEAM_0_SHOULD_RESPAWN_BACK_AT_START_NOW")
			RETURN 0 // FALSE
		ENDIF	
		
		IF IS_THIS_CURRENTLY_OVERTIME_RUMBLE()
			IF NOT bVehicleDriveable
			AND IS_ENTITY_UPSIDEDOWN(vehPlayer)
				PRINTLN("[RCC MISSION][OVERTIME] - IS_PED_IN_LOCATION - RETURNING TRUE: Vehicle is no longer driveable and is upside down. For overtime we are now going to act as if we passed the locate.")
				RETURN 1 // TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocatePropLinkIndex > -1
	OR g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocatePropLinkIndexSecondary > -1
	OR g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocatePropLinkIndexTertiary > -1
		PRINTLN("[RCC MISSION][OVERTIME] - IS_PED_IN_LOCATION - iLoc: ", iLoc,  " | Using iLocatePropLinkIndex: ", 			g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocatePropLinkIndex,
																				" | Using iLocatePropLinkIndexSecondary: ",	g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocatePropLinkIndexSecondary,
																				" | Using iLocatePropLinkIndexTertiary: ", 	g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocatePropLinkIndexTertiary)
		
		BOOL bStandingOnProp = FALSE
		
		// This disgusting crap was done because we needed to assign multiple props to a locate, but this was requested after 30 variations of overtime modes had saved data for the unarrayed iLocatePropLinkIndex
		IF IS_PROP_LINKED_TO_LOCATE_PRIMARY(iTempPart, iLoc)
			bStandingOnProp = TRUE
		ENDIF
		
		IF NOT bStandingOnProp
			IF IS_PROP_LINKED_TO_LOCATE_SECONDARY(iTempPart, iLoc)
				bStandingOnProp = TRUE
			ENDIF
		ENDIF
		
		IF NOT bStandingOnProp
			IF IS_PROP_LINKED_TO_LOCATE_TERTIARY(iTempPart, iLoc)
				bStandingOnProp = TRUE
			ENDIF
		ENDIF
		
		IF NOT bStandingOnProp
		AND bVehicleDriveable
			PRINTLN("[RCC MISSION][OVERTIME] - IS_PED_IN_LOCATION - RETURNING FALSE... We are not standing on the prop. Instead we are standing on:  ", MC_PlayerBD[iTempPart].iCurrentPropHit[0], " ", MC_PlayerBD[iTempPart].iCurrentPropHit[1], " ", MC_PlayerBD[iTempPart].iCurrentPropHit[2])			
			RETURN 0 // FALSE
		ELSE
			PRINTLN("[RCC MISSION][OVERTIME] - IS_PED_IN_LOCATION ... ############## WE ARE STANDING ON THE RIGHT PROP(s) FOR THIS LOCATE ##############.")
					
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciOVERTIME_VEHICLE_MUST_BE_STOPPED_TO_PASS_LOCATE)
				IF IS_PED_IN_ANY_VEHICLE(tempPed)
											
					IF IS_ENTITY_ALIVE(vehPlayer)
					AND bVehicleDriveable
						INT bHasStoppedCheck
						
						IF NOT HAS_VEHICLE_STOPPED_VELOCITY(vehPlayer)
						OR IS_ENTITY_IN_AIR(vehPlayer)
						OR IS_ENTITY_UPSIDEDOWN(vehPlayer)
						OR NOT HAS_VEHICLE_STOPPED_VELOCITY_ANGULAR(vehPlayer)
							bHasStoppedCheck = 0 // FALSE
							
							IF HAS_NET_TIMER_STARTED(tdOvertimeStationaryTimer)
								RESET_NET_TIMER(tdOvertimeStationaryTimer)
								PRINTLN("[RCC MISSION][OVERTIME][STOPPED] - IS_PED_IN_LOCATION - Detected movement above the threshhold we have set. Resetting Stopped Timer.")
							ENDIF
						ELSE
							bHasStoppedCheck = 0 // FALSE
							
							IF NOT HAS_NET_TIMER_STARTED(tdOvertimeStationaryTimer)
								START_NET_TIMER(tdOvertimeStationaryTimer)										
								PRINTLN("[RCC MISSION][OVERTIME][STOPPED] - IS_PED_IN_LOCATION - Starting Overtime Stopped Timer.")
							ENDIF
							
							IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdOvertimeStationaryTimer, ciOVERTIME_STATIONARY_TIME)
								bHasStoppedCheck = 1 // TRUE
								PRINTLN("[RCC MISSION][OVERTIME][STOPPED] - IS_PED_IN_LOCATION - tdOvertimeStationaryTimer Expired. We have been 'stopped' for at least: ", ciOVERTIME_STATIONARY_TIME, " Miliseconds.")
							ELSE
								PRINTLN("[RCC MISSION][OVERTIME][STOPPED] - IS_PED_IN_LOCATION - 'Stopped' Timer Progress: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdOvertimeStationaryTimer))
							ENDIF
						ENDIF
						
						IF bHasStoppedCheck = 0
							PRINTLN("[RCC MISSION][OVERTIME] - IS_PED_IN_LOCATION - RETURNING FALSE... Vehicle has not yet stopped. (1)")
							RETURN bHasStoppedCheck
						ENDIF
					ENDIF
				ENDIF
			ENDIF			
		ENDIF
	ENDIF
	
	RETURN 2  // PASS
ENDFUNC

FUNC BOOL IS_PED_IN_LOCATION(PED_INDEX tempPed, INT iloc, INT iTeam)
	
	FLOAT fDist
				
	#IF IS_DEBUG_BUILD
		IF bGotoLocPrints
			PRINTLN("[RCC MISSION] - IS_PED_IN_LOCATION")
		ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		IF bTestOvertimeZones
			PRINTLN("[RCC MISSION] - IS_PED_IN_LOCATION - Bailing out because of bTestOvertimeZones")
			RETURN FALSE
		ENDIF
	#ENDIF
	
	// Only relevant if Overtime Mode or if Prop Links have been set up with the Locate.
	SWITCH IS_PED_IN_LOCATION_AND_ON_PROP_LOGIC(tempPed, iloc, iTeam)
		CASE 0
			RETURN FALSE
		CASE 1
			RETURN TRUE
		CASE 2
			// Do nothing...
		BREAK
	ENDSWITCH	
	
	IF MC_serverBD_4.iGotoLocationDataRule[iloc][iTeam] != FMMC_OBJECTIVE_LOGIC_LEAVE_LOCATION
		IF g_bPropertyDropOff
		AND NOT g_HeistApartmentReadyForDropOffCut
			#IF IS_DEBUG_BUILD
			IF bGotoLocPrints
				PRINTLN("[RCC MISSION] IS_PED_IN_LOCATION ",iloc," = FALSE - g_bPropertyDropOff AND NOT g_HeistApartmentReadyForDropOffCut")
			ENDIF
			#ENDIF
			RETURN FALSE
		ENDIF
		
		IF NOT IS_PED_IN_REQUIRED_LOCATION_VEHICLE(tempPed, iTeam, iLoc)
			#IF IS_DEBUG_BUILD
			IF bGotoLocPrints
				PRINTLN("[RCC MISSION] IS_PED_IN_LOCATION ",iloc," = FALSE - Ped isn't in required location vehicle, model ",GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].mnVehicleNeeded[iTeam]))
			ENDIF
			#ENDIF
			RETURN FALSE
		ENDIF
		
		IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iLoc ].iLocBS, ciLoc_BS_ForceOnFoot )
		AND IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			PRINTLN("[RCC MISSION] IS_PED_IN_LOCATION ",iloc," = FALSE - Ped isn't in on foot but should be")
			RETURN FALSE
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iLoc ].fTeamMembersNeededWithin > 0
			IF NOT ARE_ALL_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE(iTeam)
				#IF IS_DEBUG_BUILD
				IF bGotoLocPrints
					PRINTLN("[RCC MISSION] IS_PED_IN_LOCATION ",iloc," = FALSE - Team members aren't all within ",g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iLoc ].fTeamMembersNeededWithin,"m")
				ENDIF
				#ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].vloc1)
	AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].vloc2)
		IF IS_ENTITY_IN_ANGLED_AREA(tempPed,g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].vloc1,g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].vloc2,g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fWidth)
			PRINTLN("[RCC MISSION] IS_PED_IN_LOCATION - RETURNING TRUE 1")
			RETURN TRUE
		#IF IS_DEBUG_BUILD
		ELIF bGotoLocPrints
			PRINTLN("[RCC MISSION] IS_PED_IN_LOCATION ",iloc," = FALSE - Ped isn't in angled area, v1 = ",g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].vloc1," v2 = ",g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].vloc2," width = ",g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fWidth)
		#ENDIF
		ENDIF
	ELSE
		VECTOR vLoc = GET_LOCATION_VECTOR(iloc)
		
		fDist = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fRadius[0] * g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fRadius[0]
		
		IF VDIST2(GET_ENTITY_COORDS(tempPed),vLoc) < fDist
				PRINTLN("[RCC MISSION] IS_PED_IN_LOCATION - RETURNING TRUE 2")
				RETURN TRUE
			#IF IS_DEBUG_BUILD
			ELIF bGotoLocPrints
				PRINTLN("[RCC MISSION] IS_PED_IN_LOCATION ",iloc," = FALSE - Ped isn't in normal area, loc = ",vLoc," box size/radius = ",g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fRadius[0])
			#ENDIF
		ELSE
			PRINTLN("[RCC MISSION] IS_PED_IN_LOCATION - RETURNING FALSE NOT INSIDE LOC")
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL HAS_ANY_ONE_HIT_LOCATE(INT iLoc, INT iRule)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ALL_TEAMS_SHARE_LOCATES)
		RETURN FALSE
	ENDIF
	
	INT iPart
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF NOT IS_BIT_SET(MC_playerBD_1[iLocalPart].iLocBitset, iLoc)
			IF IS_BIT_SET(MC_playerBD_1[iPart].iLocBitset, iLoc)
			AND iPart != iLocalPart
			AND iRule = MC_serverBD_4.iGotoLocationDataPriority[iloc][MC_playerBD[iLocalPart].iteam]
				PRINTLN("[RCC MISSION] HAS_ANY_ONE_HIT_LOCATE - Returning TRUE for loc ", iLoc, " participant: ", iPart)
				RETURN TRUE
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_OBJ_CLEAN_UP_ON_DELIVERY( INT iObj, INT idelTeam )
	
	IF IS_THIS_ROCKSTAR_MISSION_WVM_APC(g_FMMC_STRUCT.iRootContentIDHash)
		PRINTLN("[RCC MISSION] iObj ", iObj, " shouldn clean up on delivery due to iObj due to us being on the APC mission and it being set up weirdly")
		RETURN TRUE
	ENDIF
	
	//If any teams still have rules involving this obj:
	INT iTeam,iteamtouse
	FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
		IF IS_FAKE_MULTIPLAYER_MODE_SET()
			iteamtouse = MC_playerBD[iPartToUse].iteam
		ELSE
			iteamtouse = iTeam
		ENDIF
		IF iteamtouse != idelTeam
		AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSet, cibsOBJ_InvalidateAllTeams)
			IF MC_serverBD_4.iCurrentHighestPriority[iteamtouse] < MC_serverBD_4.iObjPriority[iObj][iteamtouse]
			AND MC_serverBD_4.iObjPriority[iObj][iteamtouse] < FMMC_MAX_RULES
				PRINTLN("[RCC MISSION] iObj ", iObj, " shouldn't clean up on delivery due to iObj still being required by team ", iteamtouse, " - iObj's primary rule")
				RETURN FALSE
			ENDIF
		ENDIF
		IF (GET_NEXT_PRIORITY_FOR_ENTITY(ciRULE_TYPE_OBJECT, iObj, iteamtouse, -1) < FMMC_MAX_RULES)
			PRINTLN("[RCC MISSION] iObj ", iObj, " shouldn't clean up on delivery due to iObj still being required by team ", iteamtouse, " - iObj's extra rule")
			RETURN FALSE
		ENDIF
	ENDFOR

	PRINTLN("[RCC MISSION] iObj ", iObj, " should clean up on delivery")
	RETURN TRUE
	
ENDFUNC

FUNC BOOL SHOULD_STAY_IN_ON_DELIVERY()
	
	BOOL bStayIn = FALSE
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitset[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE_DONT_STOP_ON_DELIVERY)
		OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetTwo[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE2_STAY_IN_ON_DELIVERY)
			bStayIn = TRUE
		ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetTwo[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE2_ONLY_STAY_IN_ON_LAST_DELIVERY)
			//If we're dropping off the last one:
			IF MC_serverBD.iNumVehHighestPriority[MC_playerBD[iPartToUse].iteam] = 1
			OR MC_serverBD.iNumPedHighestPriority[MC_playerBD[iPartToUse].iteam] = 1
			OR MC_serverBD.iNumObjHighestPriority[MC_playerBD[iPartToUse].iteam] = 1
				bStayIn = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bStayIn
	
ENDFUNC

FUNC BOOL SHOULD_VEH_STOP_ON_DELIVERY()
	
	BOOL bStop = TRUE
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitset[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE_DONT_STOP_ON_DELIVERY)
			bStop = FALSE
		ENDIF
	ENDIF
	
	RETURN bStop
	
ENDFUNC

FUNC BOOL FMMC_FORCE_EVERYONE_FROM_VEHICLE(INT iveh)
	
	IF SHOULD_STAY_IN_ON_DELIVERY() // force doesnt need to happen stay in the car and progress as if it has forced them out
		RETURN TRUE
	ENDIF
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
	AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetTwo[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE2_ONLY_STAY_IN_ON_LAST_DELIVERY)
		
		IF iveh != -1
		AND iDeliveryVehForcingOut = -1
			BROADCAST_FMMC_FORCING_EVERYONE_FROM_VEH(iveh, iLocalPart)
			iDeliveryVehForcingOut = iveh
			iPartSending_DeliveryVehForcingOut = iLocalPart
		ENDIF
		
		//Don't need to wait for everyone to be out, but need to keep calling force_everyone_etc
		// so it cleans up properly (that's what LBOOL5_KEEP_FORCE_EVERYONE_FROM_MY_CAR does)
		FORCE_EVERYONE_FROM_MY_CAR(TRUE,TRUE)
		MPGlobalsAmbience.g_bForceFromVehicle = TRUE
		SET_BIT(iLocalBoolCheck5, LBOOL5_KEEP_FORCE_EVERYONE_FROM_MY_CAR)
		RETURN TRUE
	ELSE
		
		IF iveh != -1
		AND iDeliveryVehForcingOut = -1
			BROADCAST_FMMC_FORCING_EVERYONE_FROM_VEH(iveh, iLocalPart)
			iDeliveryVehForcingOut = iveh
			iPartSending_DeliveryVehForcingOut = iLocalPart
			MPGlobalsAmbience.g_bForceFromVehicle = TRUE
		ENDIF
		
		IF FORCE_EVERYONE_FROM_MY_CAR(TRUE,FALSE)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_MY_VEHICLE_STOPPED(bool bSkipGetDeliveryCheck = false, FLOAT fStoppingDistance = DEFAULT_VEH_STOPPING_DISTANCE, BOOL bDisableExit = TRUE #IF IS_DEBUG_BUILD, BOOL bDoExtraDebug = FALSE #ENDIF)//FLOAT fStoppingDistance = BVTH_STOPPING_DISTANCE, INT iTimeToStopFor = BVTH_TIME_TO_STOP_FOR)
	PRINTLN("[RCC MISSION] [IS_MY_VEHICLE_STOPPED] Called with extra debug")
	#IF IS_DEBUG_BUILD
		IF bDoExtraDebug
			PRINTLN("[RCC MISSION] [IS_MY_VEHICLE_STOPPED] Called with extra debug")
		ENDIF
	#ENDIF
	
	//bool passed in to skip the delivery rule check - mostly used when goto locates seperate from any rules want to stop the vehicle 		
	IF NOT bSkipGetDeliveryCheck
		IF NOT SHOULD_VEH_STOP_ON_DELIVERY()
			CPRINTLN(DEBUG_MISSION,"[RCC MISSION] IS_MY_VEHICLE_STOPPED: not SHOULD_VEH_STOP_ON_DELIVERY return true") 
			RETURN TRUE
		ENDIF
	ENDIF
	
	INT iTeam 
	INT iRule
	BOOL bEnableShooting = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iRule], ciBS_RULE4_ENABLE_WEAPONS_WHEN_STOPPED)
	
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		
		IF IS_PED_IN_ANY_BOAT(LocalPlayerPed)
		OR IS_PED_IN_ANY_AMPHIBIOUS_VEHICLE(LocalPlayerPed) // url:bugstar:3498788
			IF IS_VEHICLE_DRIVEABLE(tempVeh)
				IF GET_PED_IN_VEHICLE_SEAT(tempVeh) = LocalPlayerPed
					IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
						IF NOT IS_ENTITY_IN_AIR(tempVeh)
							FLOAT fSpeed = GET_ENTITY_SPEED(tempVeh)
							if	NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
								if BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(tempVeh, fStoppingDistance, DEFAULT, DEFAULT, DEFAULT, bDisableExit, bEnableShooting)
								or fSpeed < 2.5
									IF IS_PED_IN_ANY_BOAT(LocalPlayerPed)
										SET_BOAT_ANCHOR(tempVeh,TRUE)
										SET_BOAT_REMAINS_ANCHORED_WHILE_PLAYER_IS_DRIVER(tempVeh,TRUE)	//anchor boat if speed is reasonably low
									ELSE
										SET_VEHICLE_HANDBRAKE(tempVeh,TRUE)
									ENDIF
									CPRINTLN(DEBUG_MISSION,"[RCC MISSION] IS_MY_VEHICLE_STOPPED: TRUE - CALLING SET_BOAT_ANCHOR() FOR BOAT")
									SET_BIT(iLocalBoolCheck2, LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
									RETURN TRUE
								ENDIF
							endif
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELIF IS_PED_IN_ANY_HELI(LocalPlayerPed)
			IF IS_VEHICLE_DRIVEABLE(tempVeh)
				IF GET_PED_IN_VEHICLE_SEAT(tempVeh) = LocalPlayerPed
					IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
						IF GET_ENTITY_SPEED(tempVeh) < 4.0
							iTeam = MC_playerBD[iPartToUse].iteam
	 						iRule = MC_serverBD_4.iCurrentHighestPriority[ iTeam ]
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffBitSet, iRule ) // Air drop off
								CPRINTLN(DEBUG_MISSION,"[RCC MISSION] IS_MY_VEHICLE_STOPPED: TRUE - HELICOPTER CASE - Air drop off")
								RETURN TRUE
							ELSE
								IF NOT IS_ENTITY_IN_AIR(tempVeh)
								AND IS_ENTITY_UPRIGHT(tempVeh, 20)
								AND (IS_VEHICLE_ON_ALL_WHEELS(tempVeh)
								OR (GET_VEHICLE_HAS_LANDING_GEAR(tempVeh) AND GET_LANDING_GEAR_STATE(tempVeh) = LGS_BROKEN)
								OR NOT GET_VEHICLE_HAS_LANDING_GEAR(tempVeh))
									CPRINTLN(DEBUG_MISSION,"[RCC MISSION] IS_MY_VEHICLE_STOPPED: TRUE - HELICOPTER CASE - Not in air")
									RETURN TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_VEHICLE_DRIVEABLE(tempVeh)
				IF (GET_ENTITY_MODEL(tempVeh) = INT_TO_ENUM(MODEL_NAMES, HASH("TRAILERLARGE"))
				OR GET_ENTITY_MODEL(tempVeh) = INT_TO_ENUM(MODEL_NAMES, HASH("TRAILERSMALL2")))
				AND NOT IS_ENTITY_DEAD(GET_ENTITY_ATTACHED_TO(tempVeh))
					tempVeh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_ATTACHED_TO(tempVeh))
					CPRINTLN(DEBUG_MISSION,"[RCC MISSION] IS_MY_VEHICLE_STOPPED: Vehicle is a trailer, checking the attached vehicle instead")
				ENDIF
				IF GET_ENTITY_MODEL(tempVeh) = INT_TO_ENUM(MODEL_NAMES, HASH("DELUXO"))
				AND IS_ENTITY_IN_AIR(tempVeh)
					CPRINTLN(DEBUG_MISSION,"[RCC MISSION] IS_MY_VEHICLE_STOPPED: Vehicle is a Deluxo, setting wheels down.")
					SET_SPECIAL_FLIGHT_MODE_TARGET_RATIO(tempVeh, 0)
				ENDIF
				IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
					IF GET_PED_IN_VEHICLE_SEAT(tempVeh) = LocalPlayerPed
					
						IF NOT IS_VEHICLE_STOPPED(tempVeh)	
						
							IF NOT IS_BIT_SET(iLocalBoolCheck2, LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
								//BRING_VEHICLE_TO_HALT(tempVeh, fStoppingDistance, iTimeToStopFor, FALSE)
								SET_VEHICLE_HANDBRAKE(tempVeh,TRUE)
								//NET_SET_PLAYER_CONTROL(LocalPlayer,FALSE,NSPC_ALLOW_PLAYER_DAMAGE|NSPC_PREVENT_EVERYBODY_BACKOFF|NSPC_LEAVE_CAMERA_CONTROL_ON|NSPC_REENABLE_CONTROL_ON_DEATH|NSPC_ALLOW_PAD_SHAKE)
								SET_BIT(iLocalBoolCheck2, LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
								CPRINTLN(DEBUG_MISSION,"[RCC MISSION] IS_MY_VEHICLE_STOPPED: CALLING BRING_VEHICLE_TO_HALT() FOR VEHICLE fStoppingDistance = ", fStoppingDistance, " Time ", GET_CLOUD_TIME_AS_INT())
								
							ELSE
								FLOAT fSpeed = GET_ENTITY_SPEED(tempVeh)
								if	NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
									IF not BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(tempVeh,fStoppingDistance, 1,0.5,False,True, bEnableShooting)
										IF fSpeed > 2
											IF NOT HAS_NET_TIMER_STARTED(tdDeliverBackupTimer)
												REINIT_NET_TIMER(tdDeliverBackupTimer)
											ENDIF
											CPRINTLN(DEBUG_MISSION,"[RCC MISSION] IS_MY_VEHICLE_STOPPED: fSpeed > 2 ",fSpeed)								
										ELSE
											IF NOT HAS_NET_TIMER_STARTED(tdDeliverBackupTimer)
												REINIT_NET_TIMER(tdDeliverBackupTimer)
											ENDIF									
											CPRINTLN(DEBUG_MISSION,"[RCC MISSION] IS_MY_VEHICLE_STOPPED: new fSpeed <2 ",fSpeed)
										ENDIF	
										CPRINTLN(DEBUG_MISSION,"[RCC MISSION] IS_MY_VEHICLE_STOPPED: not stopped ",fSpeed)								
									endif
								endif
							ENDIF
						ENDIF										
					ELSE
						CPRINTLN(DEBUG_MISSION,"[RCC MISSION] IS_MY_VEHICLE_STOPPED: TRUE - PLAYER PED IS NOT THE DRIVER") 
						if SHOULD_STAY_IN_ON_DELIVERY()
						and	NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
							//Disable veh controls for 3 seconds 
						//	BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(tempVeh,fStoppingDistance,3) //-- Causes an assert if it's not the driver. See 2195528
						endif
						RETURN TRUE	
					ENDIF
				ENDIF	
			ENDIF
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(tempVeh)
		AND NOT IS_PED_IN_ANY_HELI(LocalPlayerPed)
			FLOAT fSpeed = GET_ENTITY_SPEED(tempVeh)
			IF (fSpeed < 0.1 AND NOT IS_ENTITY_IN_AIR(tempVeh))
			OR (GET_ENTITY_MODEL(tempVeh) = OPPRESSOR2 AND fSpeed < 1.0)
				CPRINTLN(DEBUG_MISSION,"[RCC MISSION] IS_MY_VEHICLE_STOPPED: TRUE - VEHICLE IS STOPPED AND ON GROUND ",fSpeed) 
				if SHOULD_STAY_IN_ON_DELIVERY()
				and	NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
					//Disable veh controls for 3 seconds 
					BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(tempVeh,fStoppingDistance,3, DEFAULT, DEFAULT,bDisableExit, bEnableShooting)
				endif
				RETURN TRUE
			ELSE
				#IF IS_DEBUG_BUILD
					IF bDoExtraDebug
						PRINTLN("[RCC MISSION] [IS_MY_VEHICLE_STOPPED] Called with extra debug, Going to return false fSpeed = ", fSpeed, " IS_ENTITY_IN_AIR = ", IS_ENTITY_IN_AIR(tempVeh))
					ENDIF
				#ENDIF
			ENDIF
		ENDIF
		
	ELSE
		CPRINTLN(DEBUG_MISSION,"[RCC MISSION] IS_MY_VEHICLE_STOPPED: TRUE - PLAYER PED NOT IN ANY VEHICLE") 
		RETURN TRUE	
	ENDIF
	/*
	IF IS_PED_STOPPED(LocalPlayerPed)
		PRINTLN("[RCC MISSION] IS_MY_VEHICLE_STOPPED: TRUE - PLAYER PED STOPPED") 
		RETURN TRUE
	ENDIF
	*/
	
	#IF IS_DEBUG_BUILD
		IF bDoExtraDebug
			PRINTLN("[RCC MISSION] [IS_MY_VEHICLE_STOPPED] Called with extra debug, returning false")
		ENDIF
	#ENDIF
	
	RETURN FALSE

ENDFUNC

//Get a bitset of teams who need to get in their vehicle for this vehicle's GoTo to complete for this team
FUNC INT GET_VEHICLE_GOTO_TEAMS(INT iTeam, INT iVeh, BOOL& bCheckOnRule)
	
	INT iTeamsToCheck = 0
	INT iTeamLoop
	
	bCheckOnRule = TRUE
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitSet,ciFMMC_VEHICLE_OVERRIDEWRULESETTING_GOTO)
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitSet,ciFMMC_VEHICLE_ALL_TEAM_GOTO)
			SET_BIT(iTeamsToCheck,iTeam)
		ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitSet,ciFMMC_VEHICLE_ALL_FRIENDLY_GOTO)
			FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS-1
				IF DOES_TEAM_LIKE_TEAM(iTeam,iTeamLoop)
					SET_BIT(iTeamsToCheck,iTeamLoop)
				ENDIF
			ENDFOR
		ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitSet,ciFMMC_VEHICLE_EVERYONE_GOTO)
			FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS-1
				SET_BIT(iTeamsToCheck,iTeamLoop)
			ENDFOR
		ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitSet,ciFMMC_VEHICLE_CUSTOMTEAMS_GOTO)
			FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS-1
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitSet,ciFMMC_VEHICLE_CUSTOMTEAMS_GOTO_T0 + iTeamLoop)
					SET_BIT(iTeamsToCheck,iTeamLoop)
				ENDIF
			ENDFOR
		ELSE
			//Only need one player in vehicle
			iTeamsToCheck = 0
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetTwo, ciFMMC_VEHICLE2_IGNORE_CHECK_FOR_ON_VEH_RULE)
			bCheckOnRule = FALSE
		ENDIF
		
	ELSE
		//Get the setting from the rule of the vehicle:
		IF MC_serverBD_4.iVehPriority[iVeh][iTeam] >= FMMC_MAX_RULES
			RETURN 0
		ENDIF
		iTeamsToCheck = GET_RULE_GOTO_TEAMS(iTeam, MC_serverBD_4.iVehPriority[iVeh][iTeam], bCheckOnRule)
	ENDIF
	
	RETURN iTeamsToCheck
	
ENDFUNC

FUNC BOOL AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEHICLE_INTERIOR()
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule < FMMC_MAX_RULES
	AND iTeam > -1
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_WAIT_FOR_REMAINING_IN_MOC_TO_PROGRESS)
			IF NOT ARE_REMAINING_PLAYERS_IN_VEHICLE_INTERIOR(IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen,ciENABLE_AVENGER_INTERIOR_ENTRY))
				PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEHICLE_INTERIOR. RETURNING TRUE. Waiting for remaining players in vehicle interior")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VEHICLE_WAITING_FOR_PLAYERS(INT iVeh, INT iTeam)
	BOOL bCheckOnRule = TRUE
	INT iTeamsToCheck = GET_VEHICLE_GOTO_TEAMS(iTeam, iVeh, bCheckOnRule)
	
	IF iTeamsToCheck = 0 //Only a single player goto
		RETURN FALSE
	ENDIF
	
	INT iTeamLoop, iFreeSeats, iPlayersInVeh, iTotalPlayers
	
	//is everyone in a vehicle
	//or are all the seats taken
	
	FOR iTeamLoop = 0 TO (FMMC_MAX_TEAMS - 1)
		IF IS_BIT_SET(iTeamsToCheck,iTeamLoop)
			IF (NOT bCheckOnRule)
			OR (MC_serverBD.iNumVehHighestPriority[iTeamLoop] > 0
			AND ((MC_serverBD_4.iVehMissionLogic[iTeamLoop] = FMMC_OBJECTIVE_LOGIC_GO_TO) OR (MC_serverBD_4.iVehMissionLogic[iTeamLoop] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER)))
			
				iPlayersInVeh += MC_serverBD.iTeamVehNumPlayers[iTeamLoop]
				iTotalPlayers += MC_serverBD.iNumberOfPlayingPlayers[iTeamLoop]
				iFreeSeats += MC_serverBD.iNumVehSeatsHighestPriority[iTeamLoop] - MC_serverBD.iTeamVehNumPlayers[iTeamLoop]
			ENDIF
		ENDIF
	ENDFOR
	
	IF iFreeSeats = 0
		RETURN FALSE
	ELIF (iPlayersInVeh >= iTotalPlayers)
		RETURN FALSE
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_AVENGER_INTERIOR_ENTRY)
	AND ARE_REMAINING_PLAYERS_IN_VEHICLE_INTERIOR(TRUE, iTotalPlayers, iTeamsToCheck)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL IS_AVENGER_HOLD_TRANSITION_BLOCKED()
	RETURN IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_BLOCKING_AVENGER_HOLD_TRANSITION)
ENDFUNC

FUNC BOOL IS_AVENGER_HOLD_TRANSITION_BLOCKED_AND_INACTIVE(INT iRule, INT iTeam)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[iRule], ciBS_RULE10_PROGRESS_RULE_ALL_DRIVER_ACCEL)
		RETURN TRUE
	ENDIF
	
	INT iTeamLoop
	FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS - 1
		IF MC_serverBD_4.iAvengerHoldTransitionBlockedCount[iTeamLoop] < MC_serverBD.iNumberOfPlayingPlayers[iTeamLoop]
			RETURN FALSE
		ENDIF
	ENDFOR
	
	RETURN TRUE
ENDFUNC

PROC REQUEST_AVENGER_HOLD_TRANSITION_BLOCKED(BOOL bValue)

	IF iSpectatorTarget != -1
	OR IS_LOCAL_PLAYER_ANY_SPECTATOR()
		PRINTLN("REQUEST_AVENGER_HOLD_TRANSITION_BLOCKED - bValue: ", BOOL_TO_STRING(bValue), " - am spectating, bail")
		EXIT
	ENDIF

	IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_REQUEST_AVENGER_HOLD_TRANSITION_BLOCK) = bValue
		PRINTLN("REQUEST_AVENGER_HOLD_TRANSITION_BLOCKED - bValue: ", BOOL_TO_STRING(bValue), " - PBBOOL4_REQUEST_AVENGER_HOLD_TRANSITION_BLOCK = bValue, bail")
		EXIT
	ENDIF

	IF bValue
		SET_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_REQUEST_AVENGER_HOLD_TRANSITION_BLOCK)
	ELSE
		CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_REQUEST_AVENGER_HOLD_TRANSITION_BLOCK)
	ENDIF
	
	PRINTLN("REQUEST_AVENGER_HOLD_TRANSITION_BLOCKED - bValue: ", BOOL_TO_STRING(bValue), " set/cleared PBBOOL4_REQUEST_AVENGER_HOLD_TRANSITION_BLOCK according to bValue")
	
ENDPROC


PROC SET_AVENGER_HOLD_TRANSITION_BLOCKED(BOOL bValue)
	
	// value the same, bail
	IF IS_AVENGER_HOLD_TRANSITION_BLOCKED() = bValue
		PRINTLN("SET_AVENGER_HOLD_TRANSITION_BLOCKED - bValue: ", BOOL_TO_STRING(bValue), " IS_AVENGER_HOLD_TRANSITION_BLOCKED = bValue, bail")
		EXIT
	ENDIF
	
	// cant block, warp in progress
	IF bValue
	AND IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(LocalPlayer)
		PRINTLN("SET_AVENGER_HOLD_TRANSITION_BLOCKED - bValue: ", BOOL_TO_STRING(bValue), " IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR, bail")
		EXIT
	ENDIF
	
	// Transition: Outside/Cockpick -> Hold
	IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
		IF IS_CREATOR_AIRCRAFT_ENTRY_BLOCKED(LocalPlayer) != bValue
			BLOCK_CREATOR_AIRCRAFT_INTERIOR_ENTRY(bValue)
			PRINTLN("SET_AVENGER_HOLD_TRANSITION_BLOCKED - bValue: ", BOOL_TO_STRING(bValue), " called BLOCK_CREATOR_AIRCRAFT_INTERIOR_ENTRY with bValue")
		ENDIF
	ENDIF
	
	// Transition: Hold -> Outside/Cockpick
	IF IS_SIMPLE_INTERIOR_EXIT_BLOCKED() != bValue
		BLOCK_SIMPLE_INTERIOR_EXIT(bValue)
		PRINTLN("SET_AVENGER_HOLD_TRANSITION_BLOCKED - bValue: ", BOOL_TO_STRING(bValue), " called BLOCK_SIMPLE_INTERIOR_EXIT with bValue")
	ENDIF
	
	// Lock the doors too
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		IF GET_ENTITY_MODEL(veh) = AVENGER
			
			IF bValue
				SET_VEHICLE_DOORS_LOCKED(veh, VEHICLELOCK_LOCKED_PLAYER_INSIDE)
			ELSE
				SET_VEHICLE_DOORS_LOCKED(veh, VEHICLELOCK_UNLOCKED)
			ENDIF
			
			PRINTLN("SET_AVENGER_HOLD_TRANSITION_BLOCKED - bValue: ", BOOL_TO_STRING(bValue), " Setting avenger doors lock state")
		ENDIF
	ENDIF

	IF bValue
		SET_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_BLOCKING_AVENGER_HOLD_TRANSITION)
	ELSE
		CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_BLOCKING_AVENGER_HOLD_TRANSITION)
	ENDIF
	
	PRINTLN("SET_AVENGER_HOLD_TRANSITION_BLOCKED - bValue: ", BOOL_TO_STRING(bValue), " set/cleared PBBOOL4_BLOCKING_AVENGER_HOLD_TRANSITION according to bValue")

ENDPROC

FUNC BOOL AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH(INT iPartOverride = -1)
	
	IF iPartOverride = -1
		iPartOverride = iPartToUse
	ENDIF

	#IF IS_DEBUG_BUILD
		IF bInVehicleDebug
			PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH Called with callstack. MC_playerBD[iPartToUse].iVehNear =  ", MC_playerBD[iPartToUse].iVehNear)
			DEBUG_PRINTCALLSTACK()
		ENDIF
	#ENDIF
	
	IF MC_playerBD[iPartOverride].iVehNear = -1 //Not in a vehicle yet	
		#IF IS_DEBUG_BUILD
			IF bInVehicleDebug
				PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH Called. False 1  ")
			ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen,ciENABLE_AVENGER_INTERIOR_ENTRY)
		IF AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEHICLE_INTERIOR()
			PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH - Waiting for players to enter Mobile Operations Center")
			RETURN TRUE
		ENDIF
	ENDIF
	
	BOOL bCheckOnRule = TRUE
	INT iTeamsToCheck = GET_VEHICLE_GOTO_TEAMS(MC_playerBD[iPartOverride].iteam, MC_playerBD[iPartOverride].iVehNear, bCheckOnRule)
	
	#IF IS_DEBUG_BUILD
		IF bInVehicleDebug
			PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH, veh ",MC_playerBD[iPartOverride].iVehNear," - iTeamsToCheck bitset = ",iTeamsToCheck,", bCheckOnRule ",bCheckOnRule)
		ENDIF
	#ENDIF
	
	IF iTeamsToCheck = 0 //Only a single player goto
		#IF IS_DEBUG_BUILD
			IF bInVehicleDebug
				PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH Called. False 2 ")
			ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	INT iTeamLoop, iFreeSeats, iPlayersInVeh, iTotalPlayers
	
	//is everyone in a vehicle
	//or are all the seats taken
	
	FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF IS_BIT_SET(iTeamsToCheck,iTeamLoop)
			IF (NOT bCheckOnRule)
			OR (MC_serverBD.iNumVehHighestPriority[iTeamLoop] > 0
				AND ((MC_serverBD_4.iVehMissionLogic[iTeamLoop] = FMMC_OBJECTIVE_LOGIC_GO_TO)
					 OR (MC_serverBD_4.iVehMissionLogic[iTeamLoop] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER)))
				iPlayersInVeh += MC_serverBD.iTeamVehNumPlayers[iTeamLoop]
				iTotalPlayers += MC_serverBD.iNumberOfPlayingPlayers[iTeamLoop]
				iFreeSeats += MC_serverBD.iNumVehSeatsHighestPriority[iTeamLoop] - MC_serverBD.iTeamVehNumPlayers[iTeamLoop]
			ENDIF
		ENDIF
	ENDFOR
	
	IF MC_serverBD.iTeamUniqueVehHeldNum[MC_playerBD[iPartOverride].iTeam] > 1
	AND MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iPartOverride].iteam] < FMMC_MAX_RULES
	AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartOverride].iTeam].iRuleBitsetEleven[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iPartOverride].iteam]], ciBS_RULE11_FORCE_SAME_VEHICLE_FOR_MID_POINT)
		#IF IS_DEBUG_BUILD
			IF bInVehicleDebug
				PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH, = TRUE because MC_serverBD.iTeamUniqueVehHeldNum[MC_playerBD[iPartOverride].iTeam] = ", MC_serverBD.iTeamUniqueVehHeldNum[MC_playerBD[iPartOverride].iTeam])
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF iFreeSeats = 0
		#IF IS_DEBUG_BUILD
			IF bInVehicleDebug
				PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH, veh ",MC_playerBD[iPartOverride].iVehNear," - Returning FALSE as iFreeSeats = 0 - iTeamsToCheck bitset = ",iTeamsToCheck,", bCheckOnRule ",bCheckOnRule)
			ENDIF
		#ENDIF
		RETURN FALSE
	ELIF (iPlayersInVeh >= iTotalPlayers)
		#IF IS_DEBUG_BUILD
			IF bInVehicleDebug
				PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH, veh ",MC_playerBD[iPartOverride].iVehNear," - Returning FALSE as iPlayersInVeh (",iPlayersInVeh,") >= iTotalPlayers (",iTotalPlayers,") - iTeamsToCheck bitset = ",iTeamsToCheck,", bCheckOnRule ",bCheckOnRule)
			ENDIF
		#ENDIF
		RETURN FALSE
		
//	ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen,ciENABLE_AVENGER_INTERIOR_ENTRY)
//	AND ARE_REMAINING_PLAYERS_IN_VEHICLE_INTERIOR(TRUE, iTotalPlayers, iTeamsToCheck)
//	
//		#IF IS_DEBUG_BUILD
//			IF bInVehicleDebug
//				PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH, veh ",MC_playerBD[iPartOverride].iVehNear," - Returning FALSE as ARE_REMAINING_PLAYERS_IN_VEHICLE_INTERIOR returns true")
//			ENDIF
//		#ENDIF
//		RETURN FALSE

	ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen,ciENABLE_AVENGER_INTERIOR_ENTRY)
	AND ARE_REMAINING_PLAYERS_IN_VEHICLE_INTERIOR(TRUE, iTotalPlayers, iTeamsToCheck)
			
		#IF IS_DEBUG_BUILD
			IF bInVehicleDebug
				PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH - ARE_REMAINING_PLAYERS_IN_VEHICLE_INTERIOR && IS_AVENGER_TRANSITION_BLOCKED_FOR_TEAM")
			ENDIF
		#ENDIF
		RETURN FALSE

	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF bInVehicleDebug
			PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH Called. returning True. iPlayersInVeh = ", iPlayersInVeh, " iTotalPlayers = ", iTotalPlayers, " iFreeSeats = ", iFreeSeats)  
		ENDIF
	#ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC INT GET_NUMBER_OF_FREE_SEATS_IN_HIGH_PRIORITY_VEHICLES_FOR_TEAM(INT iTeam)
	INT iFreeSeats = 0
	
	IF MC_serverBD.iNumVehHighestPriority[iTeam] > 0
	AND ((MC_serverBD_4.iVehMissionLogic[iTeam] = FMMC_OBJECTIVE_LOGIC_GO_TO)
		 OR (MC_serverBD_4.iVehMissionLogic[iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER))
		
		INT iVeh = -1
		
		IF MC_playerBD[iPartToUse].iVehNear != -1
			iVeh = MC_playerBD[iPartToUse].iVehNear
		ELIF iAHighPriorityVehicle != -1
			iVeh = iAHighPriorityVehicle //First vehicle to be checked in DISPLAY_VEHICLE_HUD which is a priority for my team
		ENDIF
		
		IF iVeh != -1
			BOOL bCheckOnRule = TRUE
			INT iTeamsToCheck = GET_VEHICLE_GOTO_TEAMS(iTeam, iVeh, bCheckOnRule)
			
			IF iTeamsToCheck = 0
				iFreeSeats = MC_serverBD.iNumVehSeatsHighestPriority[iTeam] - MC_serverBD.iTeamVehNumPlayers[iTeam]
			ELSE
				
				INT iTeamLoop
				
				FOR iTeamLoop = 0 TO (FMMC_MAX_TEAMS - 1)
					IF IS_BIT_SET(iTeamsToCheck,iTeamLoop)
						IF (NOT bCheckOnRule)
						OR (MC_serverBD.iNumVehHighestPriority[iTeamLoop] > 0
							AND ((MC_serverBD_4.iVehMissionLogic[iTeamLoop] = FMMC_OBJECTIVE_LOGIC_GO_TO)
								 OR (MC_serverBD_4.iVehMissionLogic[iTeamLoop] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER)))
							iFreeSeats += MC_serverBD.iNumVehSeatsHighestPriority[iTeamLoop] - MC_serverBD.iTeamVehNumPlayers[iTeamLoop]
						ENDIF
					ENDIF
				ENDFOR
				
			ENDIF
			
		ELSE
			//Backup:
			iFreeSeats = MC_serverBD.iNumVehSeatsHighestPriority[iTeam] - MC_serverBD.iTeamVehNumPlayers[iTeam]
		ENDIF
		
	ENDIF
	
	RETURN iFreeSeats
	
ENDFUNC

FUNC BOOL SHOULD_PED_ALWAYS_FOLLOW_TEAM(INT iped, INT iteam)
	
	SWITCH iteam
	
		CASE 0
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AlwaysFollowTeam0)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 1
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AlwaysFollowTeam1)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 2
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AlwaysFollowTeam2)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 3
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AlwaysFollowTeam3)
				RETURN TRUE
			ENDIF
		BREAK
	
	ENDSWITCH

RETURN FALSE

ENDFUNC

FUNC BOOL SHOULD_PED_FOLLOW_TEAM_ON_RULE(INT iped, INT iTeam, INT iRule)
	
	BOOL bFollow = FALSE
	
	IF (iRule < FMMC_MAX_RULES)
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFollowTeamOnRuleBS[iTeam], iRule)
		bFollow = TRUE
	ENDIF
	
	RETURN bFollow
	
ENDFUNC

FUNC BOOL SHOULD_PED_FOLLOW_TEAM_ON_CURRENT_RULE(INT iped, INT iTeam)
	
	RETURN SHOULD_PED_FOLLOW_TEAM_ON_RULE(iped, iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam])
	
ENDFUNC

FUNC BOOL IS_THIS_PEDS_LAST_DELIVERY_FOR_TEAM(INT iped,INT iteam)

	INT iextraObjectiveEntity
	INT iextraObjectiveNum
	INT iRule
	
	FOR iextraObjectiveEntity = 0 TO (MAX_NUM_EXTRA_OBJECTIVE_ENTITIES-1)
		IF g_FMMC_STRUCT.iExtraObjectiveEntityType[iextraObjectiveEntity] = ciRULE_TYPE_PED
		AND g_FMMC_STRUCT.iExtraObjectiveEntityId[iextraObjectiveEntity] = iped
			FOR iextraObjectiveNum = 0 TO (MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY-1)
				IF iextraObjectiveNum >= MC_serverBD.iCurrentExtraObjective[iextraObjectiveEntity][iteam]
				OR MC_serverBD.iCurrentExtraObjective[iextraObjectiveEntity][iteam] = 0
					IF g_FMMC_STRUCT.iExtraObjectivePriority[iextraObjectiveEntity][iextraObjectiveNum][iteam] < FMMC_MAX_RULES
					AND g_FMMC_STRUCT.iExtraObjectivePriority[iextraObjectiveEntity][iextraObjectiveNum][iteam] >= 0
						iRule = g_FMMC_STRUCT.iExtraObjectiveRule[iextraObjectiveEntity][iextraObjectiveNum][iteam]
						iRule = GET_FMMC_RULE_FROM_CREATOR_RULE(iRule)
						IF iRule = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
							RETURN FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
	ENDFOR
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL SHOULD_PED_LEAVE_GROUP_ON_DROP_OFF(INT iped)

	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_StopFollowingOnLastDelivery) 
		IF IS_THIS_PEDS_LAST_DELIVERY_FOR_TEAM(iped,MC_playerBD[iPartToUse].iteam)
			RETURN TRUE
		ENDIF
	ELSE
		IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFollow != ciPED_FOLLOW_ON AND NOT SHOULD_PED_ALWAYS_FOLLOW_TEAM(iped,MC_playerBD[iPartToUse].iteam))
		//Will they leave my group on the next rule:
		AND NOT SHOULD_PED_FOLLOW_TEAM_ON_RULE(iped, MC_playerBD[iPartToUse].iteam, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] + 1)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
													
ENDFUNC

///PURPOSE: Returns an int that is broadcast to change what icon is
///    used on the ticker message for pickuing up a special CTF pickup
///    Defaults to -1 (no special type)
FUNC INT GET_CTF_TICKER_ICON_TYPE_TO_DISPLAY()
	INT iToReturn = -1
	
	IF IS_BIT_SET( iLocalBoolCheck3, LBOOL3_CTF_OBJECT_IS_BAG )
		iToReturn = ciTICKER_BAG
	ELIF IS_BIT_SET( iLocalBoolCheck3, LBOOL3_CTF_OBJECT_IS_CASE )
		iToReturn = ciTICKER_CASE
	ELIF IS_BIT_SET( iLocalBoolCheck5, LBOOL5_CTF_OBJECT_IS_FLAG )
		iToReturn = ciTICKER_FLAG
	ELIF IS_BIT_SET( iLocalBoolCheck5, LBOOL5_CTF_OBJECT_IS_TARGET )
		iToReturn = ciTICKER_TARGET
	ELIF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_CTF_OBJECT_IS_DRUGS)
		iToReturn = ciTICKER_DRUGS
	ELIF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_CTF_OBJECT_IS_LAPTOP)
		iToReturn = ciTICKER_LAPTOP
	ENDIF
	
	RETURN iToReturn
ENDFUNC

///PURPOSE: This function is used to bypass certain logic on dropoffs
///    for pickups that aren't explicitly attached to the player
///    as in the binbags
FUNC BOOL HAS_SPECIAL_PICKUP_FINISHED_DELIVERING( INT iObj )
	BOOL bReturn = FALSE
	
	IF IS_SPECIAL_PICKUP_READY_FOR_DELIVERY(iObj)
	OR NOT IS_THIS_A_SPECIAL_PICKUP( iObj )
		bReturn = TRUE
	ENDIF
	
	RETURN bReturn
ENDFUNC

FUNC BOOL IS_OBJECT_A_CONTAINER( OBJECT_INDEX tempObj )
	BOOL bReturn = FALSE
	
	IF DOES_ENTITY_EXIST(tempObj)
		IF GET_ENTITY_MODEL( tempObj ) = PROP_CONTR_03B_LD
		OR GET_ENTITY_MODEL( tempObj ) = prop_container_ld_pu
			bReturn = TRUE
		ENDIF
	ENDIF
	
	RETURN bReturn
ENDFUNC

FUNC BOOL IS_EXCLUSIVE_DELIVERY(INT iObj,INT iTeam, INT iRule)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[iRule], ciBS_RULE12_EXCLUSIVE_DELIVERY_OBJECT)
		RETURN TRUE
	ENDIF
	OBJECT_INDEX tempObj
	INT i
	REPEAT  MC_serverBD.iNumObjCreated i
		IF iObj = i
			RELOOP
		ENDIF
		
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID( MC_serverBD_1.sFMMC_SBD.niObject[i])
			RELOOP
		ENDIF
		
		IF MC_serverBD.iObjCarrier[i] != -1
			PRINTLN("[RCC MISSION] [IS_EXCLUSIVE_DELIVERY] - Can't delivery the object ",iObj," because this ",i," is carried by player ",MC_serverBD.iObjCarrier[i])
			RETURN FALSE
		ENDIF
		
		tempObj = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[i])
		
		IF IS_ENTITY_ATTACHED(tempObj)
			PRINTLN("[RCC MISSION] [IS_EXCLUSIVE_DELIVERY] - Can't delivery the object ",iObj," because this ",i," is attached.")
			RETURN FALSE
		ENDIF
		
	ENDREPEAT
	RETURN TRUE
ENDFUNC

FUNC BOOL HAS_DELIVER_OBJECT_RULE_FINISHED( INT iObj )
	INT iTeam = MC_playerBD[iPartToUse].iteam
	BOOL bReturn = FALSE

	IF MC_serverBD.iObjCarrier[ iObj ] = iPartToUse OR sCurrentSpecialPickupState.iCurrentlyOwnedBinbag = iObj
		IF MC_serverBD_4.iObjRule[ iObj ][ iTeam ] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
			IF MC_serverBD_4.iObjPriority[ iObj ][ iTeam ] < FMMC_MAX_RULES
				IF HAS_SPECIAL_PICKUP_FINISHED_DELIVERING( iObj )
					IF NETWORK_DOES_NETWORK_ID_EXIST( MC_serverBD_1.sFMMC_SBD.niObject[ iObj ] )
						OBJECT_INDEX tempObj = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[ iObj ])
						
						IF IS_THIS_A_SPECIAL_PICKUP(iObj)
						OR IS_OBJECT_A_CONTAINER(tempObj)
						OR IS_ENTITY_ATTACHED_TO_ENTITY(tempObj, LocalPlayerPed) // Normal pickup
							IF IS_EXCLUSIVE_DELIVERY(iObj,iTeam,MC_serverBD_4.iObjPriority[iObj][iTeam])
								bReturn = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bReturn
ENDFUNC

FUNC BOOL IS_DROPOFF_AN_AERIAL_FLAG( INT iLoc )
	RETURN g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iArial[0] = 1
ENDFUNC

PROC CLEAR_COMPLETED_OBJECTIVE_DATA()
	INT iBitsetArrayBlank[1]
	IF IS_BIT_SET(MC_serverBD.iProcessJobCompBitset,iPartToUse)
		IF MC_playerBD[iPartToUse].iObjectiveTypeCompleted =JOB_COMP_ARRIVE_LOC
			MC_playerBD[iPartToUse].iCurrentLoc = -1
		ENDIF
		IF MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_DELIVER_PED
		OR MC_playerBD[iPartToUse].iObjectiveTypeCompleted =JOB_COMP_ARRIVE_PED
			iPedDeliveredBitset[0] = 0
			iPedDeliveredBitset[1] = 0
			iPedDeliveredBitset[2] = 0
			MC_playerBD[iPartToUse].iPedNear = -1
		ENDIF
		IF MC_playerBD[iPartToUse].iObjectiveTypeCompleted =JOB_COMP_ARRIVE_VEH
			MC_playerBD[iPartToUse].iVehNear = -1
			iBroadcastVehNear = -1
		ENDIF
		IF MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_DELIVER_VEH
			CLEAR_BIT(MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_DROPPING_OFF )
			MC_playerBD[iPartToUse].iVehDeliveryId = -1
		ENDIF
		IF MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_DELIVER_OBJ
		OR MC_playerBD[iPartToUse].iObjectiveTypeCompleted =JOB_COMP_ARRIVE_OBJ
			iObjDeliveredBitset = 0
			MC_playerBD[iPartToUse].iObjNear = -1
		ENDIF
		IF MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_PHOTO_LOC
			MC_playerBD[iPartToUse].iLocPhoto = -1
		ENDIF
		IF MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_PHOTO_PED
			MC_playerBD[iPartToUse].iPedPhoto = -1
		ENDIF
		IF MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_CHARM_PED
			MC_playerBD[iPartToUse].iPedCharmed = -1
		ENDIF
		IF MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_PHOTO_VEH
			MC_playerBD[iPartToUse].iVehPhoto = -1
		ENDIF
		IF MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_PHOTO_OBJ
			MC_playerBD[iPartToUse].iObjPhoto = -1
		ENDIF
		IF MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_HACK_OBJ	
			MC_playerBD[iPartToUse].iObjHacked = -1
		ENDIF
		MC_playerBD[iPartToUse].iObjectiveTypeCompleted = -1
		iBroadcastPriority = -1
		iBroadcastRevalidate = -1
		SET_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_CLR_SRV_OBJ_BIT)
		IF HAS_NET_TIMER_STARTED(tdeventsafetytimer)
			RESET_NET_TIMER(tdeventsafetytimer)
		ENDIF
		PRINTLN("[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - iProcessJobCompBitset is set")
	ELSE
		IF HAS_NET_TIMER_STARTED(tdeventsafetytimer)
			IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdeventsafetytimer) > 3000 // Lowered from 5000
				
				INT iJobComp = MC_playerBD[iPartToUse].iObjectiveTypeCompleted
				BOOL bFailure
				
				SWITCH iJobComp
					CASE JOB_COMP_DELIVER_PED
						BROADCAST_FMMC_OBJECTIVE_COMPLETE(iJobComp,iPartToUse,-1,iBroadcastPriority, iBroadcastRevalidate, iPedDeliveredBitset)
					BREAK
					CASE JOB_COMP_DELIVER_VEH
						IF MC_playerBD[iPartToUse].iVehDeliveryId != -1
							BROADCAST_FMMC_OBJECTIVE_COMPLETE(iJobComp,iPartToUse,MC_playerBD[iPartToUse].iVehDeliveryId,iBroadcastPriority, iBroadcastRevalidate, iBitsetArrayBlank)
						ELSE
							CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_DELIVER_VEH, but iVehDeliveryID is -1")
							PRINTLN("[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_DELIVER_VEH, but iVehDeliveryID is -1")
								CLEAR_BIT(MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
								CLEAR_BIT(MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_DROPPING_OFF )
							bFailure = TRUE
						ENDIF
					BREAK
					CASE JOB_COMP_DELIVER_OBJ
						BROADCAST_FMMC_OBJECTIVE_COMPLETE(iJobComp,iPartToUse,iObjDeliveredBitset,iBroadcastPriority, iBroadcastRevalidate, iBitsetArrayBlank)
					BREAK
					CASE JOB_COMP_ARRIVE_LOC
						IF MC_playerBD[iPartToUse].iCurrentLoc != -1
							BROADCAST_FMMC_OBJECTIVE_COMPLETE(iJobComp,iPartToUse,MC_playerBD[iPartToUse].iCurrentLoc,iBroadcastPriority, iBroadcastRevalidate, iBitsetArrayBlank)
						ELSE
							CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_ARRIVE_LOC, but iCurrentLoc is -1")
							PRINTLN("[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_ARRIVE_LOC, but iCurrentLoc is -1")
							bFailure = TRUE
						ENDIF
					BREAK
					CASE JOB_COMP_ARRIVE_PED
						IF MC_playerBD[iPartToUse].iPedNear != -1
							BROADCAST_FMMC_OBJECTIVE_COMPLETE(iJobComp,iPartToUse,MC_playerBD[iPartToUse].iPedNear,iBroadcastPriority, iBroadcastRevalidate, iBitsetArrayBlank)
						ELSE
							CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_ARRIVE_PED, but iPedNear is -1")
							PRINTLN("[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_ARRIVE_PED, but iPedNear is -1")
							iPedDeliveredBitset[0] = 0
							iPedDeliveredBitset[1] = 0
							iPedDeliveredBitset[2] = 0
							bFailure = TRUE
						ENDIF
					BREAK
					CASE JOB_COMP_ARRIVE_VEH
						IF iBroadcastVehNear != -1
							BROADCAST_FMMC_OBJECTIVE_COMPLETE(iJobComp, iPartToUse, iBroadcastVehNear, iBroadcastPriority, iBroadcastRevalidate, iBitsetArrayBlank)
						ELSE
							CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_ARRIVE_VEH, but iBroadcastVehNear is -1")
							PRINTLN("[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_ARRIVE_VEH, but iBroadcastVehNear is -1")
							bFailure = TRUE
						ENDIF
					BREAK
					CASE JOB_COMP_ARRIVE_OBJ
						IF MC_playerBD[iPartToUse].iObjNear != -1
							BROADCAST_FMMC_OBJECTIVE_COMPLETE(iJobComp,iPartToUse,MC_playerBD[iPartToUse].iObjNear,iBroadcastPriority, iBroadcastRevalidate, iBitsetArrayBlank)
						ELSE
							CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_ARRIVE_OBJ, but iObjNear is -1")
							PRINTLN("[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_ARRIVE_OBJ, but iObjNear is -1")
							iObjDeliveredBitset = 0
							bFailure = TRUE
						ENDIF
					BREAK
					CASE JOB_COMP_PHOTO_LOC
						IF MC_playerBD[iPartToUse].iLocPhoto != -1
							BROADCAST_FMMC_OBJECTIVE_COMPLETE(iJobComp,iPartToUse,MC_playerBD[iPartToUse].iLocPhoto,iBroadcastPriority, iBroadcastRevalidate, iBitsetArrayBlank)
						ELSE
							CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_PHOTO_LOC, but iLocPhoto is -1")
							PRINTLN("[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_PHOTO_LOC, but iLocPhoto is -1")
							bFailure = TRUE
						ENDIF
					BREAK
					CASE JOB_COMP_PHOTO_PED
						IF MC_playerBD[iPartToUse].iPedPhoto != -1
							BROADCAST_FMMC_OBJECTIVE_COMPLETE(iJobComp,iPartToUse,MC_playerBD[iPartToUse].iPedPhoto,iBroadcastPriority, iBroadcastRevalidate, iBitsetArrayBlank)
						ELSE
							CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_PHOTO_PED, but iPedPhoto is -1")
							PRINTLN("[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_PHOTO_PED, but iPedPhoto is -1")
							bFailure = TRUE
						ENDIF
					BREAK
					CASE JOB_COMP_CHARM_PED
						IF MC_playerBD[iPartToUse].iPedCharmed != -1
							BROADCAST_FMMC_OBJECTIVE_COMPLETE(iJobComp,iPartToUse,MC_playerBD[iPartToUse].iPedCharmed,iBroadcastPriority, iBroadcastRevalidate, iBitsetArrayBlank)
						ELSE
							CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_CHARM_PED, but iPedCharmed is -1")
							PRINTLN("[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_CHARM_PED, but iPedCharmed is -1")
							bFailure = TRUE
						ENDIF
					BREAK
					CASE JOB_COMP_PHOTO_VEH
						IF MC_playerBD[iPartToUse].iVehPhoto != -1
							BROADCAST_FMMC_OBJECTIVE_COMPLETE(iJobComp,iPartToUse,MC_playerBD[iPartToUse].iVehPhoto,iBroadcastPriority, iBroadcastRevalidate, iBitsetArrayBlank)
						ELSE
							CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_PHOTO_VEH, but iVehPhoto is -1")
							PRINTLN("[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_PHOTO_VEH, but iVehPhoto is -1")
							bFailure = TRUE
						ENDIF
					BREAK
					CASE JOB_COMP_PHOTO_OBJ
						IF MC_playerBD[iPartToUse].iObjPhoto != -1
							BROADCAST_FMMC_OBJECTIVE_COMPLETE(iJobComp,iPartToUse,MC_playerBD[iPartToUse].iObjPhoto,iBroadcastPriority, iBroadcastRevalidate, iBitsetArrayBlank)
						ELSE
							CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_PHOTO_OBJ, but iObjPhoto is -1")
							PRINTLN("[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_PHOTO_OBJ, but iObjPhoto is -1")
							bFailure = TRUE
						ENDIF
					BREAK
					CASE JOB_COMP_HACK_OBJ
						IF MC_playerBD[iPartToUse].iObjHacked != -1
							BROADCAST_FMMC_OBJECTIVE_COMPLETE(iJobComp,iPartToUse,MC_playerBD[iPartToUse].iObjHacked,iBroadcastPriority, iBroadcastRevalidate, iBitsetArrayBlank)
						ELSE
							CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_HACK_OBJ, but iObjHacked is -1")
							PRINTLN("[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_HACK_OBJ, but iObjHacked is -1")
							bFailure = TRUE
						ENDIF
					BREAK
				ENDSWITCH
				
				IF NOT bFailure
					PRINTLN("[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - resending safety event since server not responded for 3 seconds, objective type: ",iJobComp,", priority: ",iBroadcastPriority,", revalidates: ",iBroadcastRevalidate," - my team (",MC_playerBD[iPartToUse].iTeam,") has current priority ",MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam])
					REINIT_NET_TIMER(tdeventsafetytimer)
				ELSE
					PRINTLN("[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - failed to resend safety event! clearing data...")
					RESET_NET_TIMER(tdEventSafetyTimer)
					MC_playerBD[iPartToUse].iObjectiveTypeCompleted = -1
					iBroadcastPriority = -1
					iBroadcastRevalidate = -1
					CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_CLR_SRV_OBJ_BIT)
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - no response from server (been waiting for ",GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdeventsafetytimer),"ms, objective type ",MC_playerBD[iPartToUse].iObjectiveTypeCompleted,")")
			#ENDIF
			ENDIF
		#IF IS_DEBUG_BUILD
		ELSE
			IF MC_playerBD[iPartToUse].iObjectiveTypeCompleted != -1
				PRINTLN("[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - no response from server and tdeventsafetytimer hasn't started! MC_playerBD[iPartToUse].iObjectiveTypeCompleted = ",MC_playerBD[iPartToUse].iObjectiveTypeCompleted)
			ENDIF
		#ENDIF
		ENDIF
		
		IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_CLR_SRV_OBJ_BIT)
			PRINTLN("[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - MC_serverBD.iProcessJobCompBitset is no longer set, clear PBBOOL_CLR_SRV_OBJ_BIT")
			CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_CLR_SRV_OBJ_BIT)
		ENDIF
		
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_VEH_CLEAN_UP_ON_DELIVERY( INT iveh, INT iteamdel,BOOL bCheckDontCleanupFlag = TRUE, BOOL bCheckShockLock = TRUE)
		
	IF bCheckDontCleanupFlag	
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet,ciFMMC_VEHICLE_DONTCLEANUPONDELIVER)
			PRINTLN("[RCC MISSION] Veh ", iveh, " shouldn't clean up on delivery due to creator setting")
			RETURN FALSE
		ENDIF
	ENDIF

	//If any teams still have rules involving this ped:
	INT iTeam,iteamtouse
	
	FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
		IF IS_FAKE_MULTIPLAYER_MODE_SET()
			iteamtouse = MC_playerBD[iPartToUse].iteam
			iTeam = FMMC_MAX_TEAMS // Only need to do this once!
		ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFour, ciFMMC_VEHICLE4_DELIVERY_CLEANUPONLYCHECKSONETEAM)
			iteamtouse = iteamdel
			iTeam = FMMC_MAX_TEAMS // Only need to do this once!
		ELSE
			iteamtouse = iTeam
		ENDIF
		IF iteamtouse != iteamdel
			IF MC_serverBD_4.iVehPriority[iveh][iteamtouse] > MC_serverBD_4.iCurrentHighestPriority[iteamtouse]
			AND MC_serverBD_4.iVehPriority[iveh][iteamtouse] < FMMC_MAX_RULES
				PRINTLN("[RCC MISSION] Veh ", iveh, " shouldn't clean up on delivery due to veh still being required by team ", iteamtouse, " - veh's primary rule")
				RETURN FALSE
			ENDIF
		ENDIF
		IF (GET_NEXT_PRIORITY_FOR_ENTITY(ciRULE_TYPE_VEHICLE, iveh, iteamtouse, MC_serverBD_4.iCurrentHighestPriority[iteamtouse]) < FMMC_MAX_RULES)
			PRINTLN("[RCC MISSION] Veh ", iveh, " shouldn't clean up on delivery due to veh still being required by team ", iteamtouse, " - veh's extra rule")
			RETURN FALSE
		ENDIF
	ENDFOR
	
	IF bCheckShockLock
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive, ciFMMC_VEHICLE5_SHOCKED_IF_LOCKED)
			PRINTLN("[RCC MISSION] Veh ", iveh, " shouldn't clean up on delivery due as it has the shock lock turned on")
			RETURN FALSE
		ENDIF
	ENDIF


	PRINTLN("[RCC MISSION] Veh ", iveh, " should clean up on delivery")
	RETURN TRUE
	
ENDFUNC

FUNC BOOL SHOULD_LOCK_VEHICLE_ON_DELIVERY( INT iVeh, INT iTeam )
	IF( SHOULD_VEH_CLEAN_UP_ON_DELIVERY( iVeh, iTeam, FALSE,FALSE ) )
		IF( IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[ iVeh ].iVehBitsetTwo, ciFMMC_VEHICLE2_LOCK_ON_RULE ) )
			// Lock Vehicle on Final Delivery
			
			PRINTLN("[RCC MISSION] [SHOULD_LOCK_VEHICLE_ON_DELIVERY] True - ciFMMC_VEHICLE2_LOCK_ON_RULE ")
			RETURN TRUE
		ENDIF
		IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[ iVeh ].iVehBitSet, ciFMMC_VEHICLE_DONTCLEANUPONDELIVER ) 
		
			PRINTLN("[RCC MISSION] [SHOULD_LOCK_VEHICLE_ON_DELIVERY] FALSE - ciFMMC_VEHICLE_DONTCLEANUPONDELIVER ")
			RETURN FALSE
		ELSE
			//-- Legacy, return true if nothing is set
			PRINTLN("[RCC MISSION] [SHOULD_LOCK_VEHICLE_ON_DELIVERY] TRUE - NOT ciFMMC_VEHICLE_DONTCLEANUPONDELIVER ")
			RETURN TRUE
		ENDIF 
	ELSE
		IF( g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[ iVeh ].iLockOnDeliveryTeam = iTeam
		AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[ iVeh ].iLockOnDeliveryRule = MC_serverBD_4.iCurrentHighestPriority[ iTeam ] )
			// Lock Vehicle on Delivery Rule
			PRINTLN("[RCC MISSION] [SHOULD_LOCK_VEHICLE_ON_DELIVERY] TRUE and shouldn't clean up..")
			RETURN TRUE
		ENDIF
	ENDIF
	PRINTLN("[RCC MISSION] [SHOULD_LOCK_VEHICLE_ON_DELIVERY] FALSE")
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PED_CLEAN_UP_ON_DELIVERY(INT iped, INT iteamdel,BOOL bOnlyCheckForLastDelivery = FALSE)
	
	IF NOT bOnlyCheckForLastDelivery
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset,ciPED_BS_PedDoesntCleanUpOnDelivery)
			PRINTLN("[RCC MISSION] Ped ", iped, " shouldn't clean up on delivery due to creator setting")
			RETURN FALSE
		ENDIF
	ENDIF
	
	//If any teams still have rules involving this ped:
	INT iTeam
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		iTeam = MC_playerBD[iPartToUse].iteam
		IF iTeam != iteamdel
			IF MC_serverBD_4.iPedPriority[iped][iTeam] > MC_serverBD_4.iCurrentHighestPriority[iTeam]
			AND MC_serverBD_4.iPedPriority[iped][iTeam] < FMMC_MAX_RULES
				PRINTLN("[RCC MISSION] Ped ", iped, " shouldn't clean up on delivery due to ped still being required by team ", iTeam, " - ped's primary rule")
				RETURN FALSE
			ENDIF
		ENDIF
		IF (GET_NEXT_PRIORITY_FOR_ENTITY(ciRULE_TYPE_PED, iped, iTeam, -1) < FMMC_MAX_RULES)
			PRINTLN("[RCC MISSION] Ped ", iped, " shouldn't clean up on delivery due to ped still being required by team ", iTeam, " - ped's extra rule")
			RETURN FALSE
		ENDIF
	ELSE
		FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
		
			IF iTeam != iteamdel
				IF MC_serverBD_4.iPedPriority[iped][iTeam] > MC_serverBD_4.iCurrentHighestPriority[iTeam]
				AND MC_serverBD_4.iPedPriority[iped][iTeam] < FMMC_MAX_RULES
					PRINTLN("[RCC MISSION] Ped ", iped, " shouldn't clean up on delivery due to ped still being required by team ", iTeam, " - ped's primary rule")
					RETURN FALSE
				ENDIF
			ENDIF
			IF (GET_NEXT_PRIORITY_FOR_ENTITY(ciRULE_TYPE_PED, iped, iTeam, -1) < FMMC_MAX_RULES)
				PRINTLN("[RCC MISSION] Ped ", iped, " shouldn't clean up on delivery due to ped still being required by team ", iTeam, " - ped's extra rule")
				RETURN FALSE
			ENDIF
		ENDFOR
	ENDIF
	
	PRINTLN("[RCC MISSION] Ped ", iped, " should clean up on delivery")
	RETURN TRUE

ENDFUNC

FUNC BOOL SHOULD_TURN_PLAYER_CONTROL_ON_AFTER_DELIVERY()
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[ iTeam ]
	
	// Don't ant to turn on plauyer control if it's the last rule (2150083) or if the next rule is a cutscene (2172246)
	
	PRINTLN("[RCC MISSION] [SHOULD_TURN_PLAYER_CONTROL_ON_AFTER_DELIVERY] Called iTeam = ", iTeam, " iRule = ", iRule)
	
	IF MC_ServerBD.iNumVehHighestPriority[ iTeam ] > 1 
		//-- There's more cars to deliver
		
		PRINTLN("[RCC MISSION] [SHOULD_TURN_PLAYER_CONTROL_ON_AFTER_DELIVERY] TRUE -  more cars to deliver")
		RETURN TRUE
	ENDIF
	
	 IF iRule < MC_serverBD.iMaxObjectives[ iTeam ] 
	 	IF NOT IS_NEXT_RULE_A_CUTSCENE()
			//-- This isn't the last rule, and the next rule isn't a cutscene
			PRINTLN("[RCC MISSION] [SHOULD_TURN_PLAYER_CONTROL_ON_AFTER_DELIVERY] TRUE - Not last rule, and next rule isn't a cutscene")
			RETURN TRUE
		ENDIF
	 ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_FMMC_NET_ENTITY_EXIST(INT iEntityType, INT iEntityID)
	
	NETWORK_INDEX niEnt
	
	IF iEntityID >= 0
		SWITCH iEntityType
			CASE ci_TARGET_PED
				IF iEntityID < FMMC_MAX_PEDS
					niEnt = MC_serverBD_1.sFMMC_SBD.niPed[iEntityID]
				ENDIF
			BREAK
			CASE ci_TARGET_VEHICLE
				IF iEntityID < FMMC_MAX_VEHICLES
					niEnt = MC_serverBD_1.sFMMC_SBD.niVehicle[iEntityID]
				ENDIF
			BREAK
			CASE ci_TARGET_OBJECT
				IF iEntityID < FMMC_MAX_NUM_OBJECTS
					niEnt = MC_serverBD_1.sFMMC_SBD.niObject[iEntityID]	
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN NETWORK_DOES_NETWORK_ID_EXIST(niEnt)
	
ENDFUNC

FUNC BOOL IS_GO_TO_LOC_READY_TO_SEND_OUT_COMPLETION_EVENT(INT iTeam, INT iRule)
	
	BOOL bReady = FALSE
	
	INT iEntityType = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].OverwriteEntityType[iRule]
	INT iEntityID = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].OverwriteEntityId[iRule]
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(OverwriteObjectiveNetID)
		
		IF iEntityType > 0
		AND iEntityID != -1
			IF NOT HAS_NET_TIMER_STARTED(tdVehDropoffNetIDRegTimer)
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC / IS_VEHICLE_DROPOFF_READY_TO_SEND_OUT_COMPLETION_EVENT - OverwriteObjectiveNetID exists - iTeam ",iTeam,", iRule ",iRule,", iEntityType ",iEntityType,", iEntityID ",iEntityID," - all valid, broadcast net ID")
				BROADCAST_NETWORK_INDEX_FOR_LATER_RULE(OverwriteObjectiveNetID, iEntityType, iEntityID)
				REINIT_NET_TIMER(tdVehDropoffNetIDRegTimer)
			ELSE
				IF DOES_FMMC_NET_ENTITY_EXIST(iEntityType, iEntityID)
					
					PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC / IS_VEHICLE_DROPOFF_READY_TO_SEND_OUT_COMPLETION_EVENT - Entity w type ",iEntityType," & ID ",iEntityID," exists on the server, ready to move on!")
					OverwriteObjectiveNetID = NULL
					RESET_NET_TIMER(tdVehDropoffNetIDRegTimer)
					bReady = TRUE
					
				ELIF (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdVehDropoffNetIDRegTimer) > 2000)
					
					PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC / IS_VEHICLE_DROPOFF_READY_TO_SEND_OUT_COMPLETION_EVENT - Give up waiting for server to register net ID, let's try again")
					BROADCAST_NETWORK_INDEX_FOR_LATER_RULE(OverwriteObjectiveNetID, iEntityType, iEntityID)
					RESET_NET_TIMER(tdVehDropoffNetIDRegTimer)
					
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC / IS_VEHICLE_DROPOFF_READY_TO_SEND_OUT_COMPLETION_EVENT - Waiting for server to register net ID, been waiting for ",GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdVehDropoffNetIDRegTimer),"ms")
				#ENDIF
				ENDIF
			ENDIF
		ELSE
			OverwriteObjectiveNetID = NULL
			bReady = TRUE
			PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC / IS_VEHICLE_DROPOFF_READY_TO_SEND_OUT_COMPLETION_EVENT - OverwriteObjectiveNetID exists - iTeam ",iTeam,", iRule ",iRule," - iEntityType ",iEntityType," / iEntityID ",iEntityID," not valid, clearing net ID")
		ENDIF
	ELSE
		bReady = TRUE
		
		IF iEntityType > 0
		AND iEntityID != -1
			IF NOT DOES_FMMC_NET_ENTITY_EXIST(iEntityType, iEntityID)
				IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
					IF IS_VEHICLE_DRIVEABLE(tempVeh)
						IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC / IS_VEHICLE_DROPOFF_READY_TO_SEND_OUT_COMPLETION_EVENT - Returning FALSE, OverwriteObjectiveNetID doesn't exist but we're in a vehicle which could be registered and don't have control!")
							bReady = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// We only want to delay the progression if we haven't actually scored yet.
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
		IF bReady		
			bReady = FALSE
			IF HAS_NET_TIMER_STARTED(tdOvertimeLocateDelayTimer)
				IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdOvertimeLocateDelayTimer, ci_OVERTIME_LOCATE_DELAY)
					bReady = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// [LM] IF WE EVER SEE DOUBLE LOCATE COLLECTION AGAIN ADD AN OPTION FOR THE BELOW.
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_EnableGameMastersMode)
		IF bReady		
			bReady = FALSE
			IF NOT HAS_NET_TIMER_STARTED(tdLocalLocateDelayTimer)
			OR HAS_NET_TIMER_EXPIRED_READ_ONLY(tdLocalLocateDelayTimer, 1000)				
				bReady = TRUE				
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bReady
	
ENDFUNC

FUNC BOOL AM_I_IN_SECONDARY_LOCATE_IF_I_NEED_TO_BE(INT iloc)
	
	BOOL bInLocate = FALSE
	FLOAT fSecondaryRadius = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fSecondaryRadius
	
	IF fSecondaryRadius > 0
		IF NOT IS_PED_INJURED(LocalPlayerPed)
			
			VECTOR vMyCoords = GET_ENTITY_COORDS(LocalPlayerPed)
			
			IF VDIST2(vMyCoords, GET_LOCATION_VECTOR(iloc)) <= (fSecondaryRadius * fSecondaryRadius)
				bInLocate = TRUE
			ENDIF
		ENDIF
	ELSE
		bInLocate = TRUE
	ENDIF
	
	RETURN bInLocate
	
ENDFUNC

PROC REGISTER_AMBIENT_VEHICLE_AS_MISSION_ENTITY(INT iTeam, INT iRule)
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(OverwriteObjectiveNetID)
	AND IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		
		DeliveryVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		
		IF IS_VEHICLE_DRIVEABLE(DeliveryVeh)
		AND NETWORK_HAS_CONTROL_OF_ENTITY(DeliveryVeh)
			IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].OverwriteEntityType[iRule] > 0
			AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].OverwriteEntityId[iRule] != -1
				IF NOT IS_ENTITY_A_MISSION_ENTITY(DeliveryVeh)
				AND NOT DECOR_EXIST_ON(DeliveryVeh,"Player_Vehicle")
					PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC / REGISTER_AMBIENT_VEHICLE_AS_MISSION_ENTITY - Calling SET_ENTITY_AS_MISSION_ENTITY with vehicle ",NATIVE_TO_INT(DeliveryVeh))
					SET_ENTITY_AS_MISSION_ENTITY(DeliveryVeh,FALSE,TRUE)
				#IF IS_DEBUG_BUILD
				ELSE
					IF IS_ENTITY_A_MISSION_ENTITY(DeliveryVeh)
						PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC / REGISTER_AMBIENT_VEHICLE_AS_MISSION_ENTITY - Not calling SET_ENTITY_AS_MISSION_ENTITY as vehicle is already a mission entity!")
					ENDIF
					IF DECOR_EXIST_ON(DeliveryVeh,"Player_Vehicle")
						PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC / REGISTER_AMBIENT_VEHICLE_AS_MISSION_ENTITY - Not calling SET_ENTITY_AS_MISSION_ENTITY as vehicle is a personal vehicle!")
					ENDIF
				#ENDIF
				ENDIF
				OverwriteObjectiveNetID= VEH_TO_NET(DeliveryVeh)
				SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(OverwriteObjectiveNetID , TRUE)
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC / REGISTER_AMBIENT_VEHICLE_AS_MISSION_ENTITY - ambient vehicle set as mission entity , net id: ",NATIVE_TO_INT(OverwriteObjectiveNetID))
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL ARE_ALL_PLAYERS_IN_SEPARATE_VEHICLES_AND_LOCATE(INT iRule, INT iTeam)
	IF (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iRule], ciBS_RULE3_ALL_IN_SEPARATE_VEHICLES))
		IF NOT IS_BIT_SET(MC_serverBD.iAllPlayersInSeparateVehiclesBS[iTeam], iRule)
			PRINTLN("[JS] ARE_ALL_PLAYERS_IN_SEPARATE_VEHICLES_AND_LOCATE - Waiting for players to get into separate vehicles at the location, iTeam: ", iTeam, " iRule: ", iRule)
		ELSE
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_ALL_PLAYERS_IN_UNIQUE_VEHICLES_AND_LOCATE(INT iRule, INT iTeam)
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_PROGRESS_GAD_AT_SAME_TIME)
		IF NOT IS_BIT_SET(MC_serverBD.iAllPlayersInUniqueVehiclesBS[iTeam], iRule)
			PRINTLN("[JS] ARE_ALL_PLAYERS_IN_UNIQUE_VEHICLES_AND_LOCATE - Waiting for players to get into unique vehicles at the location")
		ELSE
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_DRIVER_PRESSING_ACCELERATE(INT iRule, INT iTeam)

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[iRule], ciBS_RULE10_PROGRESS_RULE_ALL_DRIVER_ACCEL)
		PRINTLN("ARE_ALL_PLAYERS_IN_VEHICLE - ciBS_RULE10_PROGRESS_RULE_ALL_DRIVER_ACCEL = FALSE, return TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
		PRINTLN("ARE_ALL_PLAYERS_IN_VEHICLE - Pressed INPUT_VEH_ACCELERATE, return TRUE")
		PRINTLN("[JS][UOBJ - VEHICLE] - Requested Objective Update - IS_DRIVER_PRESSING_ACCELERATE")
		SET_BIT(iLocalBoolCheck3, LBOOL3_UPDATE_VEHICLE_OBJECTIVE)
		RETURN TRUE
	ENDIF
	
	PRINTLN("ARE_ALL_PLAYERS_IN_VEHICLE - Waiting for driver to press INPUT_VEH_ACCELERATE")
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_TEAM_IN_VEHICLE_ACCURATE( INT iRule, INT iTeam )
	IF ( IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetThree[ iRule ], ciBS_RULE3_CANT_GET_OUT_NEAR_DELIVERY ) )
		PRINTLN( "[RCC MISSION] IS_TEAM_IN_VEHICLE_ACCURATE - Checking if all required teams in vehicle because ciBS_RULE3_CANT_GET_OUT_NEAR_DELIVERY is set on rule ", iRule )
		
		INT 			iVeh 	= MC_playerBD[ iPartToUse ].iVehNear
		NETWORK_INDEX	tempNet
		VEHICLE_INDEX 	tempVeh
		
		IF( iVeh <> -1 )
			tempNet = MC_serverBD_1.sFMMC_SBD.niVehicle[ iVeh ]
			IF( NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID( tempNet )
			AND MC_serverBD.iNumVehHighestPriority[ iTeam ] = 1 )
				tempVeh = NET_TO_VEH( tempNet )
			ELSE
				RETURN TRUE
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
		
		BOOL bCheckOnRule 	= TRUE
		INT  iTeamsToCheck 	= GET_VEHICLE_GOTO_TEAMS( iTeam, iVeh, bCheckOnRule )
		
		INT iPlayersToCheck, iPlayersChecked, iPlayersInVeh, iTeamLoop
		
		FOR iTeamLoop = 0 TO ( MC_serverBD.iNumberOfTeams - 1 )
			IF( IS_BIT_SET( iTeamsToCheck, iTeamLoop ) )
				iPlayersToCheck += MC_serverBD.iNumberOfPlayingPlayers[ iTeamLoop ]
			ENDIF
		ENDFOR
		
		INT iPart, iPartTeam
		PARTICIPANT_INDEX 	tempPart
		PLAYER_INDEX 		tempPlayer
		PED_INDEX 			tempPed
		
		REPEAT GET_MAXIMUM_PLAYERS_ON_MISSION() iPart
			iPartTeam = MC_playerBD[ iPart ].iTeam
			
			IF( ( NOT IS_BIT_SET( MC_playerBD[ iPart ].iClientBitSet, PBBOOL_ANY_SPECTATOR )
			OR  IS_BIT_SET( MC_playerBD[ iPart ].iClientBitSet, PBBOOL_HEIST_SPECTATOR ) )
			AND IS_BIT_SET( iTeamsToCheck, iPartTeam ) )
				tempPart = INT_TO_PARTICIPANTINDEX( iPart )
				
				IF( NETWORK_IS_PARTICIPANT_ACTIVE( tempPart ) )
					//this player will be included in the server counts
					iPlayersChecked++
					tempPlayer = NETWORK_GET_PLAYER_INDEX( tempPart )
					
					IF( IS_NET_PLAYER_OK( tempPlayer ) )
						tempPed = GET_PLAYER_PED( tempPlayer )
						
						IF( NOT IS_PED_INJURED( tempPed ) )
							IF( IS_PED_IN_VEHICLE( tempPed, tempVeh ) )
								IF( IS_PED_SITTING_IN_VEHICLE( tempPed, tempVeh ) ) // Hopefully this checks that they're not jumping out or whatever
									iPlayersInVeh++

								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF( iPlayersChecked >= iPlayersToCheck )
						iPart = MAX_NUM_MC_PLAYERS //Break out!
					ENDIF
					
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF( iPlayersInVeh >= iPlayersToCheck )
			RETURN TRUE
		ENDIF
		
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

locate_fade_out_data locateDropOff

PROC PROCESS_STOPPING_VEHICLE_FOR_DELIVERY(INT iRule, INT iTeam, BOOL bEnableShooting)
	
	BOOL bForeverStopped, bDurationStopped
	INT iMissionEntity
	VEHICLE_INDEX vehStopped
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)	
		vehStopped = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		iMissionEntity = IS_ENTITY_A_MISSION_CREATOR_ENTITY(vehStopped)
		
		PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - iVeh:  ", iMissionEntity)
		
		IF iMissionEntity > -1				
			IF IS_BIT_SET(iVehDeliveryStoppedCancelled, iMissionEntity)
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - iVeh:  ", iMissionEntity, " Delivery Stopped Cancelled.")
				EXIT
			ENDIF			
			IF IS_BIT_SET(MC_serverBD_1.iBSVehicleStopForever, iMissionEntity)
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - iVeh:  ", iMissionEntity, " iBSVehicleStopForever is set")
				bForeverStopped = TRUE
			ENDIF
			IF IS_BIT_SET(MC_serverBD_1.iBSVehicleStopDuration, iMissionEntity)
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - iVeh:  ", iMissionEntity, " iBSVehicleStopDuration is set")
				bDurationStopped = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF bDurationStopped
	OR bForeverStopped
		PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE Is set!, iVehStopTimeToWait = ", MC_ServerBD_1.iVehStopTimeToWait)
		
		IF IS_VEHICLE_DRIVEABLE(vehStopped)
			IF iRule < FMMC_MAX_RULES
				IF bForeverStopped
					PRINTLN("[RCC MISSION] DISABLE_PLAYER_MISSION_VEHICLE_CONTROLS_THIS_FRAME called as can't move vehicle")
					BOOL bDisableHydraulics = IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetFive[ iRule ], ciBS_RULE5_DISABLE_HYDRAULICS_ON_STOP_VEH )
					DISABLE_PLAYER_MISSION_VEHICLE_CONTROLS_THIS_FRAME(FALSE, bEnableShooting, bDisableHydraulics )	
					SET_BIT(iLocalBoolCheck24, LBOOL24_BLOCK_VEHICLE_KERS_ON_RULE_CHECK)
					PRINTLN("[RCC MISSION] LBOOL24_BLOCK_VEHICLE_KERS_ON_RULE_CHECK setting")
					IF NETWORK_HAS_CONTROL_OF_ENTITY(vehStopped)
						SET_VEHICLE_HANDBRAKE(vehStopped, TRUE)
					ENDIF
				ENDIF
				IF bDurationStopped
					IF NOT HAS_NET_TIMER_STARTED(MC_serverBD_1.timeVehicleStop)
						PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE Waiting for timer to start...")
					ELSE
						IF HAS_NET_TIMER_EXPIRED(MC_serverBD_1.timeVehicleStop, MC_serverBD_1.iVehStopTimeToWait)
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE timeVehicleStop expired")
							IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_STOP_CAR_FOREVER)
								IF NETWORK_HAS_CONTROL_OF_ENTITY(vehStopped)
									SET_VEHICLE_HANDBRAKE(vehStopped,FALSE)
									
									IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciDISABLE_VEHICLE_KERS)
										IF iRule < FMMC_MAX_RULES
											IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_DISABLE_OPPRESSOR_BOOST)
												CLEAR_BIT(iLocalBoolCheck24, LBOOL24_BLOCK_VEHICLE_KERS_ON_RULE_CHECK)
												PRINTLN("[RCC MISSION] LBOOL24_BLOCK_VEHICLE_KERS_ON_RULE_CHECK Clearing")
											ENDIF
										ENDIF
									ENDIF									
									PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE removed handbrake")
								ELSE
									PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE I don't have control!")
								ENDIF
							ELSE
								PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE NOT LBOOL10_STOP_CAR_FOREVER")
							ENDIF
						ELSE
							IF NETWORK_HAS_CONTROL_OF_ENTITY(vehStopped)
								SET_VEHICLE_HANDBRAKE(vehStopped, TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL ARE_ALL_PLAYERS_IN_DROP_OFF_CONDITIONS(INT iTeam, INT iRule, BOOL bIncludeLocalPlayer = TRUE)
	
	IF iRule >= FMMC_MAX_RULES	
		RETURN TRUE
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_DROP_OFF_REQUIRES_ALL_PLAYERS_PRESENCE)
		RETURN TRUE
	ENDIF
	
	BOOL bReturn = TRUE
	
	PARTICIPANT_INDEX tempPart
	INT iPart = 0 
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		tempPart = INT_TO_PARTICIPANTINDEX(iPart)
		
		IF NOT bIncludeLocalPlayer
		AND iPart = iLocalPart
			RELOOP
		ENDIF
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			
			IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR) 
			AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_HEIST_SPECTATOR)
			AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_PLAYER_FAIL)
			AND NOT IS_BIT_SET( MC_playerBD[iPart].iClientBitSet, PBBOOL_FINISHED)
			AND IS_BIT_SET(MC_playerBD[iPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
				IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
					PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE ARE_ALL_PLAYERS_IN_DROP_OFF_CONDITIONS - Not all players are inside drop off. Waiting for iPart: ", iPart)
					bReturn = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN bReturn
ENDFUNC

FUNC BOOL DOES_THIS_PART_HAVE_THE_DROP_OFF_TARGET(INT iPart)
	IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR) 
	AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_HEIST_SPECTATOR)
	AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_PLAYER_FAIL)
	AND NOT IS_BIT_SET( MC_playerBD[iPart].iClientBitSet, PBBOOL_FINISHED)
	AND IS_BIT_SET(MC_playerBD[iPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
		IF (MC_playerBD[iPart].iPedCarryCount > 0
		OR MC_playerBD[iPart].iVehCarryCount > 0
		OR MC_playerBD[iPart].iObjCarryCount > 0
		OR IS_BIT_SET( MC_playerBD[iPart].iClientBitSet, PBBOOL_WITH_CARRIER))
			PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE DOES_THIS_PART_HAVE_THE_DROP_OFF_TARGET iPart: ", iPart, " Has the/a target.")
			RETURN TRUE
		ENDIF
	ENDIF
			
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_ANY_PLAYER_HAVE_THE_DROP_OFF_TARGET()
			
	PARTICIPANT_INDEX tempPart
	INT iPart = 0 
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		tempPart = INT_TO_PARTICIPANTINDEX(iPart)
				
		IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)			
			IF DOES_THIS_PART_HAVE_THE_DROP_OFF_TARGET(iPart)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE	
	
ENDFUNC

FUNC BOOL HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE() 

	INT i
	INT iscoremulti
	BOOL bvalidreward, bInvalidReward, bcollect,bstopped,bdrawAirDropOff,bcargobobdrop,bInAir
	FLOAT fradius
	INT iDropOffEntity
	VEHICLE_INDEX tempVeh,trailerVeh,WinchVeh
	ENTITY_INDEX WinchEnt
	PED_INDEX tempPed
	FLOAT fDelXp 
	INT iDelXp
	FLOAT fStopDist
	INT iTimeToStop

	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[ iTeam ]
	BOOL bEnableShooting = FALSE
	FLOAT fMarkerDist2
	FLOAT fMarkerDisappear  = 3.0
	VECTOR vThisMarkerLoc
	INT iBitsetArrayBlank[1]
	
	BOOL bGroupDropOff
	BOOL bGroupDropOffValid
	
	CLEAR_BIT(MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_IN_DROP_OFF)

	UPDATE_LOCATE_FADE_STRUCT(locateDropOff)
	
	IF NOT bPlayerToUseOK
		CLEAR_BIT(iLocalBoolCheck2,LBOOL2_PED_DROPPED_OFF)
		RETURN FALSE
	ENDIF
	
	// Don't proceed if load scene is active, due to calls that give player control back
	// This used to be gated by this check: Is_Player_Currently_On_MP_Versus_Mission(LocalPlayer).
	// Can be added back if necessary but giving back player control during a load scene isn't allowed.
	IF IS_NEW_LOAD_SCENE_ACTIVE()
		PRINTLN("HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - Returning FALSE due to IS_NEW_LOAD_SCENE_ACTIVE")
		RETURN FALSE
	ENDIF

	IF iRule < FMMC_MAX_RULES
		
		bEnableShooting = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iRule], ciBS_RULE4_ENABLE_WEAPONS_WHEN_STOPPED) 

		bGroupDropOff = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_DROP_OFF_REQUIRES_ALL_PLAYERS_PRESENCE)
		
		IF bGroupDropOff
			bGroupDropOffValid = DOES_ANY_PLAYER_HAVE_THE_DROP_OFF_TARGET()
		ENDIF
		
		IF bGroupDropOff
		AND bGroupDropOffValid
		AND DOES_THIS_PART_HAVE_THE_DROP_OFF_TARGET(iLocalPart)
			SET_BIT(iLocalBoolCheck30, LBOOL30_SHOW_CUSTOM_WAIT_TEXT)
		ELSE
			CLEAR_BIT(iLocalBoolCheck30, LBOOL30_SHOW_CUSTOM_WAIT_TEXT)
		ENDIF
		
		IF ((MC_playerBD[iPartToUse].iPedCarryCount > 0
			OR MC_playerBD[iPartToUse].iVehCarryCount > 0
			OR MC_playerBD[iPartToUse].iObjCarryCount > 0)
			OR IS_BIT_SET( MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_WITH_CARRIER)
			AND NOT IS_BIT_SET(MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_DROPPING_OFF))
			
			OR bGroupDropOffValid
			OR Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer)
			
			PRINTLN("HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE() - Called -1 ")
			//If we have enough entities to drop off:
			fradius = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].fDropOffRadius[ iRule ]
			
			IF ((MC_serverBD.iRequiredDeliveries[iTeam] <= 1)
				OR ((MC_serverBD.iNumPedHighestPriorityHeld[iTeam] + MC_serverBD.iNumVehHighestPriorityHeld[iTeam] + MC_serverBD.iNumObjHighestPriorityHeld[iTeam]) >= MC_serverBD.iRequiredDeliveries[iTeam]))				
				
				PRINTLN("HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE() - Called -2")
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffBitSet, iRule )
					DRAW_AIR_DROP_OFF()
				ELSE
					PRINTLN("HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE() - Called -3")
					PRINTLN("HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE() - locateDropOff.iAlphaValue: ", locateDropOff.iAlphaValue)
					IF iSpectatorTarget !=-1
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iSpectatorTarget))
							PED_INDEX pedSpecPlayer = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iSpectatorTarget)))
							vThisMarkerLoc = GET_COORDS_FOR_LOCATE_MARKER(fradius)
							//fDirection = GET_DIRECTION_FOR_LOCATE_MARKER()
							fMarkerDist2 = VDIST2(GET_ENTITY_COORDS(pedSpecPlayer), vThisMarkerLoc)
							IF fMarkerDist2 <= (fMarkerDisappear * fMarkerDisappear)	
								PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - Fading out locate for spectator fMarkerDist2 = ", fMarkerDist2)
								SET_LOCATE_TO_FADE_OUT(locateDropOff)
							ELSE
								PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - Displaying locate for spectator fMarkerDist2 = ", fMarkerDist2)
								SET_LOCATE_TO_DISPLAY(locateDropOff)
							ENDIF
						ENDIF	
					ENDIF
					
					DRAW_GROUND_DROP_OFF(fradius, locateDropOff.iAlphaValue)					
				ENDIF				
			ENDIF			
		ENDIF		
	ENDIF
	
	IF iSpectatorTarget != -1
	OR IS_LOCAL_PLAYER_ANY_SPECTATOR()
		RETURN FALSE
	ENDIF
	
	CLEAR_COMPLETED_OBJECTIVE_DATA()
	
	PROCESS_STOPPING_VEHICLE_FOR_DELIVERY(iRule, iTeam, bEnableShooting)
		
	IF MC_playerBD[iPartToUse].iObjectiveTypeCompleted = -1
	AND (NOT IS_BIT_SET( MC_serverBD.iProcessJobCompBitset, iPartToUse ))
	AND (NOT IS_BIT_SET( MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER ))
	AND (NOT IS_BIT_SET( MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_FINISHED ))
	AND (iRule < FMMC_MAX_RULES)
	AND (NOT IS_BIT_SET( MC_serverBD_1.iObjectiveProgressionHeldUpBitset[iTeam], iRule))
	AND (NOT (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_SUB_TEAM_ROLES) AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRolesBitset[iRule][MC_playerBD[iLocalPart].iCoronaRole], ciBS_ROLE_Cant_Complete_This_Rule)))
	AND (NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SSBOOL_TEAM0_FAILED + iTeam)) // HAS_TEAM_FAILED
	AND (NOT HAS_TEAM_FINISHED(iTeam))
		
		BOOL bHasVehDropoffChanged // This stores if we've hit a new rule and we're still in the middle of dropping a car off. This is so
								   // that we can stop the delivery from going through and passing a different rule
		
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
		AND MC_playerBD[iPartToUse].iVehDeliveryId != -1
			PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - Vehicle delivery in progress, but this is a new rule (set bHasVehDropoffChanged)")
			bHasVehDropoffChanged = TRUE
		ENDIF
		
		IF iRule  < FMMC_MAX_RULES
		
			IF MC_playerBD[iPartToUse].iPedCarryCount > 0 // for dropping off peds you collect
			AND MC_serverBD.iNumPedHighestPriorityHeld[iTeam] >= MC_serverBD.iRequiredDeliveries[iTeam]
					
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffBitSet,  iRule ) // this is an air drop off
					bdrawAirDropOff = TRUE
				ENDIF
				
				FLOAT fTemp
				vThisMarkerLoc = GET_COORDS_FOR_LOCATE_MARKER(fTemp)
				fMarkerDist2 = VDIST2(GET_ENTITY_COORDS(LocalPlayerPed), vThisMarkerLoc)
			
				IF fMarkerDist2 <= (fMarkerDisappear * fMarkerDisappear)
					IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
						PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - Set  LBOOL10_DROP_OFF_LOC_FADE_OUT - I'm delivering Ped fMarkerDist2 = ", fMarkerDist2, " fMarkerDisappear = ", fMarkerDisappear, " vThisMarkerLoc = ", vThisMarkerLoc)
						SET_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
						CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_NO_DISPLAY)
						CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
					ENDIF
				ELSE
					IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
						IF fMarkerDist2 >= ((fMarkerDisappear + 3.0) * (fMarkerDisappear + 3.0))	
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - Set  LBOOL10_DROP_OFF_LOC_DISPLAY - I'm delivering Ped, marker had been removed but need to display fMarkerDist2 = ", fMarkerDist2, " fMarkerDisappear = ", (fMarkerDisappear + 3.0), " vThisMarkerLoc = ", vThisMarkerLoc)
							CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
							SET_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_PLAYER_IN_DROP_OFF( fradius, GET_DROP_OFF_HEIGHT(iTeam, iRule) )
					
					SET_BIT(MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
					
					BOOL bValid = TRUE
					IF NOT ARE_ALL_PLAYERS_IN_DROP_OFF_CONDITIONS(iTeam, iRule)
						bValid = FALSE
					ENDIF
					
					PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - In dropoff ")
					// Only slow the vehicle down if they're in a vehicle and the dropoff
					// is on the ground
					IF bValid
						IF IS_PED_IN_ANY_VEHICLE( LocalPlayerPed )
						AND SHOULD_VEH_STOP_ON_DELIVERY()
						AND NOT bdrawAirDropOff
													
							IF NOT IS_BIT_SET(iLocalBoolCheck9, LBOOL9_SHOULD_STOP_VEHICLE)
								fStopDist = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffStopDistance[iRule]
								IF fStopDist <= 0.0
									fStopDist = DEFAULT_VEH_STOPPING_DISTANCE
								ELIF fStopDist > 20.0
									fStopDist = 20.0
								ENDIF
								IF NOT IS_MY_VEHICLE_STOPPED(DEFAULT, fStopDist, DEFAULT #IF IS_DEBUG_BUILD, TRUE #ENDIF)
									IF NOT IS_BIT_SET(iLocalBoolCheck11, LBOOL11_SET_SHOULD_STOP_IN_DROP_OFF)
										SET_BIT(iLocalBoolCheck9, LBOOL9_SHOULD_STOP_VEHICLE)
										SET_BIT(iLocalBoolCheck11, LBOOL11_SET_SHOULD_STOP_IN_DROP_OFF)
										PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - Should stop veh on delivery - set LBOOL9_SHOULD_STOP_VEHICLE, set LBOOL11_SET_SHOULD_STOP_IN_DROP_OFF")
									ELSE
										PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - IS_MY_VEHICLE_STOPPED is false but LBOOL11_SET_SHOULD_STOP_IN_DROP_OFF alrready set!")
									ENDIF
								ELSE
									IF NOT IS_BIT_SET(iLocalBoolCheck2,LBOOL2_PED_DROPPED_OFF)
										SET_BIT(iLocalBoolCheck2,LBOOL2_PED_DROPPED_OFF)
										PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - IS_MY_VEHICLE_STOPPED is true but LBOOL9_SHOULD_STOP_VEHICLE Not Set somehow! - Setting LBOOL2_PED_DROPPED_OFF")
									ENDIF
								ENDIF
							ENDIF

						ELSE // Set the ped has been dropped off if the player is not in a vehicle or if they're in the air
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - Don't need to stop a vehicle")
							SET_BIT( iLocalBoolCheck2, LBOOL2_PED_DROPPED_OFF )
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_BIT_SET(iLocalBoolCheck9, LBOOL9_SHOULD_STOP_VEHICLE)
						IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
							IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
								PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED Set LBOOL10_DROP_OFF_LOC_DISPLAY")
								SET_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
								CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_NO_DISPLAY)
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_GOTO_LOC_POSTFX)
						CLEAR_BIT(iLocalBoolCheck10, LBOOL10_GOTO_LOC_POSTFX)
						PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED Cleared LBOOL10_GOTO_LOC_POSTFX - 1")
					ENDIF
					
					IF IS_BIT_SET(iLocalBoolCheck11, LBOOL11_SET_SHOULD_STOP_IN_DROP_OFF)
						CLEAR_BIT(iLocalBoolCheck11, LBOOL11_SET_SHOULD_STOP_IN_DROP_OFF)
						PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED Cleared LBOOL11_SET_SHOULD_STOP_IN_DROP_OFF as no longer in drop off")
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(iLocalBoolCheck9, LBOOL9_SHOULD_STOP_VEHICLE)
					IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
						tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
						IF IS_VEHICLE_DRIVEABLE(tempVeh)	
							IF GET_PED_IN_VEHICLE_SEAT(tempVeh) = LocalPlayerPed // IS_MY_VEHICLE_STOPPED returns true immediately for non-drivers
								IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DRIVING_AND_CARRYING_PED)
									SET_BIT(iLocalBoolCheck10, LBOOL10_DRIVING_AND_CARRYING_PED)
									PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED LBOOL9_SHOULD_STOP_VEHICLE is set, I'm driver, set LBOOL10_DRIVING_AND_CARRYING_PED")
								ENDIF
							ELSE
								//-- I'm not driver - Driver will stop vehicle
								PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED LBOOL9_SHOULD_STOP_VEHICLE is set, but I'm not driver")
								FLOAT fSpeed = GET_ENTITY_SPEED(tempVeh)
								FLOAT fSpeedToCheck = 2.5
								
								IF IS_PED_IN_ANY_BOAT(LocalPlayerPed)
									fSpeedToCheck = 2.5
								ELIF IS_PED_IN_ANY_HELI(LocalPlayerPed)
								OR IS_PED_IN_ANY_PLANE(LocalPlayerPed)
									fSpeedToCheck = 4.0
								ELSE
									fSpeedToCheck = 0.1
								ENDIF
								
								IF fSpeed < fSpeedToCheck
									
									PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED LBOOL9_SHOULD_STOP_VEHICLE is set, but I'm not driver, fSpeed  = ", fSpeed, " fSpeedToCheck = ", fSpeedToCheck)
									SET_BIT(iLocalBoolCheck2,LBOOL2_PED_DROPPED_OFF)
									CLEAR_BIT(iLocalBoolCheck9, LBOOL9_SHOULD_STOP_VEHICLE)
								ELSE
									PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE Trying to set LBOOL2_PED_DROPPED_OFF but fSpeed = ", fSpeed, " fSpeedToCheck = ", fSpeedToCheck)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
				
				IF IS_BIT_SET(iLocalBoolCheck2,LBOOL2_PED_DROPPED_OFF)
					IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					AND NOT bdrawAirDropOff
					AND SHOULD_VEH_STOP_ON_DELIVERY()
						tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
						IF IS_VEHICLE_DRIVEABLE(tempVeh)
							IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
								IF GET_ENTITY_SPEED(tempVeh) > 2
									
									FLOAT fSlow = 0.6
									
									IF (GET_PED_IN_VEHICLE_SEAT(tempveh) != LocalPlayerPed)
										fSlow = 0.35
									ENDIF
									
									VECTOR vSlow = << fSlow, fSlow, 1 >>
									
									VECTOR vVel = GET_ENTITY_VELOCITY(tempVeh)
									vVel *= vSlow
									SET_ENTITY_VELOCITY(tempVeh,vVel)
								ELSE
									SET_ENTITY_VELOCITY(tempVeh,<<0,0,0>>)
								ENDIF
							ELSE
								NETWORK_REQUEST_CONTROL_OF_ENTITY(tempVeh)
							ENDIF
						ENDIF
					ENDIF
					FOR i = 0 TO (MC_serverBD.iNumPedCreated - 1)
						IF IS_BIT_SET(MC_playerBD[iPartToUse].iPedFollowingBitset[GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i))
							IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[i])
								IF MC_serverBD_4.iPedRule[i][ iTeam ] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
									IF MC_serverBD_4.iPedPriority[i][ iTeam ] < FMMC_MAX_RULES
									
										BOOL bNeedControl = FALSE
										
										IF NOT bdrawAirDropOff
											IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFollow != ciPED_FOLLOW_ON
											OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetTwo,ciPED_BSTwo_StopFollowingOnLastDelivery)
												IF SHOULD_PED_CLEAN_UP_ON_DELIVERY(i, iTeam)
													bNeedControl = TRUE
												ELIF SHOULD_PED_LEAVE_GROUP_ON_DROP_OFF(i)
													bNeedControl = TRUE
												ENDIF
											ENDIF
										ENDIF
										
										BOOL bHasControl
										
										IF bNeedControl
											bHasControl = NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[i])
											IF NOT bHasControl
												NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[i])
											ENDIF
										ENDIF
										
										IF (NOT bNeedControl)
										OR bHasControl
											SET_BIT(iPedDeliveredBitset[GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i))
											IF NOT bdrawAirDropOff
												
												tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[i])
												
												IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFollow != ciPED_FOLLOW_ON
												OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetTwo, ciPED_BSTwo_StopFollowingOnLastDelivery))												
													// 
													IF SHOULD_PED_CLEAN_UP_ON_DELIVERY(i, iTeam)
														SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,TRUE)
														
														//SET_PED_CONFIG_FLAG(tempPed,PCF_PreventDraggedOutOfCarThreatResponse,FALSE)
														REMOVE_PED_FROM_GROUP(tempPed)
														SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_ALWAYS_FIGHT, FALSE)
														SET_PED_FLEE_ATTRIBUTES(tempPed, FA_DISABLE_COWER,TRUE)
														SET_PED_FLEE_ATTRIBUTES(tempPed,  FA_COWER_INSTEAD_OF_FLEE,FALSE)
														SET_PED_FLEE_ATTRIBUTES(tempPed, FA_DISABLE_FLEE_FROM_INDIRECT_THREATS,FALSE)
														IF IS_PED_IN_ANY_VEHICLE(tempPed)
															IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetTwelve, ciPed_BSTwelve_StayInVehicleAfterDelivery)
																SET_PED_COMBAT_ATTRIBUTES(tempPed,CA_LEAVE_VEHICLES,TRUE)
																SET_PED_FLEE_ATTRIBUTES(tempPed,  FA_USE_VEHICLE,FALSE)
																SET_PED_FLEE_ATTRIBUTES(tempPed,  FA_FORCE_EXIT_VEHICLE,TRUE)
																
																OPEN_SEQUENCE_TASK(PedDropOffSequence)
															    	TASK_LEAVE_ANY_VEHICLE(NULL)
																    TASK_WANDER_STANDARD(NULL)
																CLOSE_SEQUENCE_TASK(PedDropOffSequence)
																TASK_PERFORM_SEQUENCE(tempPed,PedDropOffSequence)
																CLEAR_SEQUENCE_TASK(PedDropOffSequence)
															ENDIF
														ELSE
															TASK_WANDER_STANDARD(tempPed)
														ENDIF
														
														PLAY_PED_AMBIENT_SPEECH_AND_CLONE_NATIVE(tempPed,"GENERIC_THANKS","SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
														
														PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - remove ped from group on drop off; ped given task leave (should clean up): ", i)
													ELSE
														//If we don't want to keep following after delivery
														IF SHOULD_PED_LEAVE_GROUP_ON_DROP_OFF(i)	
															SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,FALSE)
															
															REMOVE_PED_FROM_GROUP(tempPed)
															
															IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetTwelve, ciPed_BSTwelve_StayInVehicleAfterDelivery)
																IF IS_PED_IN_ANY_VEHICLE(tempPed)
																	TASK_LEAVE_ANY_VEHICLE(tempPed)
																ENDIF
															ENDIF
															
															SET_PED_USING_ACTION_MODE(tempPed, FALSE, -1, "DEFAULT_ACTION")
															SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_PLAY_REACTION_ANIMS, TRUE)
															
															IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetFour, ciPED_BSFour_DisableRagdolling)
																CLEAR_RAGDOLL_BLOCKING_FLAGS(tempPed, RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT)
															ENDIF
															
															CLEAR_BIT(MC_playerBD[iPartToUse].iPedFollowingBitset[GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i))
															
															PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - remove ped from group on drop off; ped doesn't clean up on delivery, but does leave group: ", i)
														ELSE
															PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - ped doesn't clean up or leave group on delivery: ", i)
														ENDIF
													ENDIF
												ENDIF
											ENDIF
											iDropOffEntity = i
											iscoremulti++
											bvalidreward = TRUE
										ELSE
											bInvalidReward = TRUE //Hold off on delivering them all until we've got control of everyone following us
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
					IF bvalidreward AND NOT bInvalidReward
						IF bdrawAirDropOff
							IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
							AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iRule], ciBS_RULE2_DONT_GIVE_PLAYER_XP)
								IF iPedDeliveryRPCap < 20
									fDelXp = (100*MC_playerBD[iPartToUse].iPedCarryCount*g_sMPTunables.fxp_tunable_Deliver_a_package_bonus)
									iDelXp = ROUND(fDelXp)
									GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, LocalPlayerPed,"XPT_MEDIUMT", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_MISSION_DELIVERED_PED, iDelXp,1)
									iPedDeliveryRPCap++
								ENDIF
							ENDIF
							SET_MISSION_TEST_COMPLETE()
							BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_DELIVER_PED, iscoremulti, iTeam ,-1,NULL,ci_TARGET_PED,iDropOffEntity)
							increment_medal_objective_completed(iTeam, iRule)
							MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_DELIVER_PED
							iBroadcastPriority = iRule
							iBroadcastRevalidate = MC_serverBD_4.iTimesRevalidatedObjectives[iTeam]
							BROADCAST_FMMC_OBJECTIVE_COMPLETE(MC_playerBD[iPartToUse].iObjectiveTypeCompleted, iPartToUse, iPedDeliveredBitset[GET_LONG_BITSET_INDEX(iDropOffEntity)], iRule, MC_serverBD_4.iTimesRevalidatedObjectives[iTeam], iPedDeliveredBitset)
							REINIT_NET_TIMER(tdeventsafetytimer)
							INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(iscoremulti)
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - JOB_COMP_DELIVER_PED + bdrawAirDropOff - Setting MC_playerBD[iPartToUse].iPedCarryCount as zero")
							MC_playerBD[iPartToUse].iPedCarryCount = 0
							CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER)
							CLEAR_BIT(iLocalBoolCheck2,LBOOL2_PED_DROPPED_OFF)
							IF IS_BIT_SET(iLocalBoolCheck2,LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
								SET_BIT(iLocalBoolCheck4, LBOOL4_DELIVERY_WAIT)
								REINIT_NET_TIMER(tdDeliverTimer)
							ELSE
								NET_SET_PLAYER_CONTROL(LocalPlayer,TRUE)
							ENDIF
							RESET_NET_TIMER(tdDeliverBackupTimer)
							CLEAR_BIT(iLocalBoolCheck2, LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
							CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - JOB_COMP_DELIVER_PED set locally by part: ",iPartToUse) 
							RETURN TRUE
						ELSE
							IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
							AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iRule], ciBS_RULE2_DONT_GIVE_PLAYER_XP)
								IF iPedDeliveryRPCap < 20
									fDelXp = (100*MC_playerBD[iPartToUse].iPedCarryCount*g_sMPTunables.fxp_tunable_Deliver_a_package_bonus)
									iDelXp = ROUND(fDelXp)
									GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, LocalPlayerPed,"XPT_MEDIUMT", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_MISSION_DELIVERED_PED, iDelXp,1)
									iPedDeliveryRPCap++
								ENDIF
							ENDIF
							SET_MISSION_TEST_COMPLETE()
							BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_DELIVER_PED, iscoremulti, iTeam ,-1,NULL,ci_TARGET_PED,iDropOffEntity)
							MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_DELIVER_PED
							BROADCAST_FMMC_OBJECTIVE_COMPLETE(MC_playerBD[iPartToUse].iObjectiveTypeCompleted, iPartToUse, iPedDeliveredBitset[GET_LONG_BITSET_INDEX(iDropOffEntity)], iRule, MC_serverBD_4.iTimesRevalidatedObjectives[iTeam], iPedDeliveredBitset)
							iBroadcastPriority = iRule
							iBroadcastRevalidate = MC_serverBD_4.iTimesRevalidatedObjectives[iTeam]
							increment_medal_objective_completed(iTeam, iRule)
							REINIT_NET_TIMER(tdeventsafetytimer)
							INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(iscoremulti)
							MC_playerBD[iPartToUse].iPedCarryCount = 0
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - JOB_COMP_DELIVER_PED - Setting MC_playerBD[iPartToUse].iPedCarryCount as zero")
							CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER)
							CLEAR_BIT(iLocalBoolCheck2,LBOOL2_PED_DROPPED_OFF)
							IF IS_BIT_SET(iLocalBoolCheck2,LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
								//SET_VEHICLE_HANDBRAKE(tempVeh,FALSE)
								//NET_SET_PLAYER_CONTROL(LocalPlayer,TRUE)
								SET_BIT(iLocalBoolCheck4, LBOOL4_DELIVERY_WAIT)
								REINIT_NET_TIMER(tdDeliverTimer)
							ELSE
								NET_SET_PLAYER_CONTROL(LocalPlayer,TRUE)
							ENDIF
							RESET_NET_TIMER(tdDeliverBackupTimer)
							CLEAR_BIT(iLocalBoolCheck2, LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
							CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - JOB_COMP_DELIVER_PED set locally by part: ",iPartToUse)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ELIF MC_serverBD.iNumPedHighestPriorityHeld[iTeam] > 0 
			AND MC_serverBD.iNumPedHighestPriorityHeld[iTeam] >= MC_serverBD.iRequiredDeliveries[iTeam]
				
				// My team has a ped to deliver, they're not following me. Have I still driven them into a drop-off?
				IF IS_PED_IN_ANY_VEHICLE( LocalPlayerPed )
				AND NOT IS_PED_IN_ANY_HELI(LocalPlayerPed)
				AND NOT IS_PED_IN_ANY_PLANE(LocalPlayerPed)
				
					VEHICLE_INDEX vehDropOff 	= GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
					MODEL_NAMES mDropOff		= GET_ENTITY_MODEL(vehDropOff)
					
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffBitSet,  iRule ) // this is an air drop off
						bdrawAirDropOff = TRUE
					ENDIF
					
					PED_INDEX pedDriving = GET_PED_IN_VEHICLE_SEAT(vehDropOff)
					PED_INDEX pedTemp
					PLAYER_INDEX playerTemp
					PARTICIPANT_INDEX partTemp
					INT ipart
					
					IF pedDriving = LocalPlayerPed
					
						IF IS_PLAYER_IN_DROP_OFF( fradius, GET_DROP_OFF_HEIGHT(iTeam, iRule) )
						
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - In dropoff - ped not following me")
							// Only slow the vehicle down if they're in a vehicle and the dropoff
							// is on the ground
							IF SHOULD_VEH_STOP_ON_DELIVERY()
							AND NOT bdrawAirDropOff
								INT iNumSeats 		= GET_VEHICLE_MODEL_NUMBER_OF_SEATS(mDropOff)
								INT iNumPassengers	= GET_VEHICLE_NUMBER_OF_PASSENGERS(vehDropOff)
								INT iSeatLoop		= 0
								
								#IF IS_DEBUG_BUILD
									IF IS_MODEL_VALID(mDropOff)
										PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - In dropoff - ped not following me, veh should stop, model = ",mDropOff, " iNumPassengers = ", iNumPassengers, " iNumSeats = ", iNumSeats)
									ENDIF
								#ENDIF
								
								IF iNumPassengers > 0
									FOR iSeatLoop = 0 TO iNumSeats
										IF NOT IS_BIT_SET(iLocalBoolCheck9, LBOOL9_SHOULD_STOP_VEHICLE)
											VEHICLE_SEAT vsTemp = INT_TO_ENUM(VEHICLE_SEAT, iSeatLoop)
											pedTemp = GET_PED_IN_VEHICLE_SEAT(vehDropOff, vsTemp)
											IF pedTemp <> NULL
												IF NOT IS_PED_INJURED(pedTemp)
													IF IS_PED_A_PLAYER(pedTemp)
														playerTemp = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedTemp)
														IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerTemp)
															partTemp = NETWORK_GET_PARTICIPANT_INDEX(playerTemp)
															ipart = NATIVE_TO_INT(partTemp)
															IF MC_playerBD[ipart].iPedCarryCount > 0
																SET_BIT(iLocalBoolCheck9, LBOOL9_SHOULD_STOP_VEHICLE)
																PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - In dropoff - ped not following me - Will stop car - This player is in my car ", GET_PLAYER_NAME(playerTemp), " They are part ", ipart, " MC_playerBD[ipart].iPedCarryCount = ", MC_playerBD[ipart].iPedCarryCount)
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDFOR
								ENDIF
								
							ENDIF
						ELSE
							IF NOT IS_BIT_SET(iLocalBoolCheck9, LBOOL9_SHOULD_STOP_VEHICLE)
								IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
									IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
										PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED Set LBOOL10_DROP_OFF_LOC_DISPLAY (2)")
										SET_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
										CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
										CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_NO_DISPLAY)
									ENDIF
								ENDIF
							ENDIF
							
							FLOAT fTemp
							vThisMarkerLoc = GET_COORDS_FOR_LOCATE_MARKER(fTemp)
							fMarkerDist2 = VDIST2(GET_ENTITY_COORDS(LocalPlayerPed), vThisMarkerLoc)
							
							IF fMarkerDist2 <= (fMarkerDisappear * fMarkerDisappear)
								IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
									PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - Set  LBOOL10_DROP_OFF_LOC_NO_DISPLAY - ped not following me fMarkerDist2 = ", fMarkerDist2, " fMarkerDisappear = ", fMarkerDisappear, " vThisMarkerLoc = ", vThisMarkerLoc)
									SET_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
									CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_NO_DISPLAY)
									CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
								ENDIF
							ELSE
								IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
									IF fMarkerDist2 >= ((fMarkerDisappear + 3.0) * (fMarkerDisappear + 3.0))	
										PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - Set  LBOOL10_DROP_OFF_LOC_DISPLAY - ped not following me, marker had been removed but need to display fMarkerDist2 = ", fMarkerDist2, " fMarkerDisappear = ", (fMarkerDisappear + 3.0), " vThisMarkerLoc = ", vThisMarkerLoc)
										CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
										SET_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
									ENDIF
								ENDIF
							ENDIF
							
							IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_GOTO_LOC_POSTFX)
								CLEAR_BIT(iLocalBoolCheck10, LBOOL10_GOTO_LOC_POSTFX)
								PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED Cleared LBOOL10_GOTO_LOC_POSTFX - 4")
							ENDIF
						ENDIF
					ELSE
						// I'm not driving the vehicle, and the ai ped isn't following me. Check if the player the ai ped is following is driving my car. 
						IF NOT IS_PED_INJURED(pedDriving)
							
							IF IS_PLAYER_IN_DROP_OFF( fradius, GET_DROP_OFF_HEIGHT(iTeam, iRule) )
								
								PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - In dropoff - ped not following me - I'm not driving")
								IF IS_PED_A_PLAYER(pedDriving)
									playerTemp = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedDriving)
									IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerTemp)
										partTemp = NETWORK_GET_PARTICIPANT_INDEX(playerTemp)
										ipart = NATIVE_TO_INT(partTemp)
										IF MC_playerBD[ipart].iPedCarryCount > 0
											PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - In dropoff - ped not following me - I'm not Driving - This player is in Driving ", GET_PLAYER_NAME(playerTemp), " They are part ", ipart, " MC_playerBD[ipart].iPedCarryCount = ", MC_playerBD[ipart].iPedCarryCount)
											IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
												CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
											ENDIF
											IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_NO_DISPLAY)
												IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
													PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED SET_LOCATE_TO_FADE_OUT - 4")
													SET_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
												ENDIF
											ENDIF																													
										ENDIF
									ENDIF
								ENDIF
							ELSE
								
								IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
									IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
										PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED Set LBOOL10_DROP_OFF_LOC_DISPLAY (3)")
										SET_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
										CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_NO_DISPLAY)
									ENDIF
								ENDIF
								
								FLOAT fTemp
								vThisMarkerLoc = GET_COORDS_FOR_LOCATE_MARKER(fTemp)
								fMarkerDist2 = VDIST2(GET_ENTITY_COORDS(LocalPlayerPed), vThisMarkerLoc)
								
								IF fMarkerDist2 <= fMarkerDisappear * fMarkerDisappear
									IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
										PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - Set  LBOOL10_DROP_OFF_LOC_FADE_OUT -ped not following me fMarkerDist2 = ", fMarkerDist2, " fMarkerDisappear = ", fMarkerDisappear, " vThisMarkerLoc = ", vThisMarkerLoc)
										SET_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
										CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_NO_DISPLAY)
										CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
									ENDIF
								ELSE
									IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
										IF fMarkerDist2 >= ((fMarkerDisappear + 3.0) * (fMarkerDisappear + 3.0))
											PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - Set  LBOOL10_DROP_OFF_LOC_DISPLAY -  ped not following me - I'm not driving marker had been removed but need to display fMarkerDist2 = ", fMarkerDist2, " fMarkerDisappear = ", (fMarkerDisappear + 3.0), " vThisMarkerLoc = ", vThisMarkerLoc)
											CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
											SET_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF		
					ENDIF
				ELSE
					IF bGroupDropOff					
						fradius = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].fDropOffRadius[ iRule ]
						IF fradius > 0
							IF IS_PLAYER_IN_DROP_OFF( fradius, GET_DROP_OFF_HEIGHT(iTeam, iRule))
								PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - I am in the drop off area.")
								SET_BIT(MC_playerBD[iPartToUse].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF bGroupDropOff
					fradius = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].fDropOffRadius[ iRule ]
					IF fradius > 0
						IF IS_PLAYER_IN_DROP_OFF( fradius, GET_DROP_OFF_HEIGHT(iTeam, iRule))
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - I am in the drop off area.")
							SET_BIT(MC_playerBD[iPartToUse].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
						ENDIF
					ENDIF
				ENDIF
			ENDIF // End of ped carrying
			
			IF IS_BIT_SET(iLocalBoolCheck9, LBOOL9_SHOULD_STOP_VEHICLE)
			
				IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
					CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
				ENDIF
				
				IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_NO_DISPLAY)
					IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
						IF IS_PLAYER_IN_DROP_OFF( fradius, GET_DROP_OFF_HEIGHT(iTeam, iRule) )
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED SET_LOCATE_TO_FADE_OUT - 2")
							SET_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
					tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
					IF IS_VEHICLE_DRIVEABLE(tempVeh)	
						IF GET_PED_IN_VEHICLE_SEAT(tempVeh) = LocalPlayerPed // IS_MY_VEHICLE_STOPPED returns true immediately for non-drivers
							IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
								NETWORK_REQUEST_CONTROL_OF_ENTITY(tempVeh)
							ELSE
								fStopDist = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffStopDistance[iRule]
								IF fStopDist <= 0.0
									fStopDist = DEFAULT_VEH_STOPPING_DISTANCE
								ELIF fStopDist > 20.0
									fStopDist = 20.0
								ENDIF
								PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE, Trying to stop vehicle, fStopDist = ", fStopDist)
								IF IS_MY_VEHICLE_STOPPED(DEFAULT, fStopDist)
									IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DRIVING_AND_CARRYING_PED)
										PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE,, I've stopped the car, set LBOOL2_PED_DROPPED_OFF")
										SET_BIT(iLocalBoolCheck2,LBOOL2_PED_DROPPED_OFF)
										CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DRIVING_AND_CARRYING_PED)
										
										IF SHOULD_PARTICIPANT_LOOK_AT_CELLPHONE()
											PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - Setting PBBOOL2_TASK_PLAYER_CUTANIM_OUTRO - url:bugstar:6141935")
											SET_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_TASK_PLAYER_CUTANIM_OUTRO)
										ENDIF
									ENDIF
									CLEAR_BIT(iLocalBoolCheck9, LBOOL9_SHOULD_STOP_VEHICLE)
									
									//-- For disengaging the handbrake in the instances where I havcen't actually delivered the ped, but I've stopped the car
									SET_BIT(iLocalBoolCheck4, LBOOL4_DELIVERY_WAIT)
									REINIT_NET_TIMER(tdDeliverTimer)									
									
									PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE,, I've stopped the car, clearing LBOOL9_SHOULD_STOP_VEHICLE, set LBOOL4_DELIVERY_WAIT")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF ( (MC_playerBD[ iPartToUse ].iObjCarryCount > 0)
				AND (MC_serverBD.iNumObjHighestPriorityHeld[iTeam] >= MC_serverBD.iRequiredDeliveries[iTeam]) )
			OR IS_SPECIAL_PICKUP_READY_FOR_DELIVERY( sCurrentSpecialPickupState.iCurrentlyRequestedObj )
			AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciPOINTLESS_MODE_ENABLED)
							
				BOOL bValid = TRUE
				IF NOT ARE_ALL_PLAYERS_IN_DROP_OFF_CONDITIONS(iTeam, iRule)
					bValid = FALSE
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffBitSet,  iRule )
					bdrawAirDropOff = TRUE
				ELSE
					IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
						tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
						IF IS_VEHICLE_DRIVEABLE(tempVeh)
							IF IS_ENTITY_IN_AIR(tempVeh)
								bInAir = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffStopDistance[iRule] <> 1
					IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffPlayerSpeed[iRule] >= 0.0
						IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOff_Vehicle[iRule] = -1 
							PRINTLN("[LH][PLAYERSPEED] Changing player speed to ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffPlayerSpeed[iRule])
							RESET_PLAYER_STAMINA(LocalPlayer)
							SET_PED_MOVE_RATE_OVERRIDE(LocalPlayerPed, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffPlayerSpeed[iRule])
							SET_PED_MOVE_RATE_IN_WATER_OVERRIDE(LocalPlayerPed, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffPlayerSpeed[iRule])
						ENDIF
					ENDIF	
				ENDIF
				
				OBJECT_INDEX tempObj = NULL
				
				IF IS_PLAYER_IN_DROP_OFF( fradius, GET_DROP_OFF_HEIGHT(iTeam, iRule) )
					CERRORLN( DEBUG_OWAIN, "[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE OBJ - In dropoff" )
					SET_BIT(MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
					FOR i = 0 TO ( MC_serverBD.iNumObjCreated - 1 )
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID( MC_serverBD_1.sFMMC_SBD.niObject[ i ] )
							tempObj = NET_TO_OBJ( MC_serverBD_1.sFMMC_SBD.niObject[ i ] )
							
							IF HAS_DELIVER_OBJECT_RULE_FINISHED( i )
								PRINTLN( "[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE OBJ - Finished dropping " )
								
								IF NOT IS_OBJECT_A_CONTAINER( tempObj )
									IF NOT bInAir
										IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitSetSix[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE6_GET_DELIVER_DONT_DROP_ON_DELIVER)
											CLEAR_BIT( iobjcolBoolCheck, i )
										ENDIF
										SET_BIT( iObjDeliveredBitset, i )
										
										IF IS_THIS_A_SPECIAL_PICKUP( i )
											IF GET_ENTITY_MODEL( tempObj ) = HEI_PROP_HEI_TIMETABLE
												DETACH_ENTITY( tempObj, FALSE, TRUE )
											ENDIF
											FREEZE_ENTITY_POSITION( tempObj, TRUE )
											SET_ENTITY_COLLISION( tempObj, FALSE )
											SET_ENTITY_VISIBLE( tempObj, FALSE )
											SET_ENTITY_COORDS( tempObj, <<20.0, 20.0, 0.0>>, FALSE, FALSE, FALSE, FALSE )
											PRINTLN( "[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE OBJ - Resetting special pickup" )
											// tom look here 
											SET_CURRENT_SPECIAL_PICKUP_STATE( eSpecialPickupState_RESET )
										ELSE
											IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitSetSix[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE6_GET_DELIVER_DONT_DROP_ON_DELIVER)
												IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitSet, cibsOBJ_InvisibleOnFirstDelivery)
													IF SHOULD_OBJ_CLEAN_UP_ON_DELIVERY( i,  iTeam  )
														IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niObject[ i ])
															HIDE_PORTABLE_PICKUP_WHEN_DETACHED(tempObj, TRUE ) 
															PRINTLN( "[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE OBJ - HIDE_PORTABLE_PICKUP_WHEN_DETACHED")
														ENDIF
													ENDIF
												ENDIF
	
												IF IS_ENTITY_ATTACHED_TO_ANY_PED(tempObj)//Stop the assertFAILED: DETACH_PORTABLE_PICKUP_FROM_PED - the pickup is not attached
													DETACH_PORTABLE_PICKUP_FROM_PED( tempObj )
												ENDIF
												PRINTLN( "[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE OBJ - Dropped object off - Detached pickup" )
												
												IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitSet, cibsOBJ_InvisibleOnFirstDelivery)
													IF SHOULD_OBJ_CLEAN_UP_ON_DELIVERY( i,  iTeam  )
														SET_ENTITY_ALPHA(tempObj, 0, FALSE)
														PRINTLN( "[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE OBJ - NO ALPHA!")
														SET_ENTITY_VISIBLE( tempObj, FALSE )
														SET_ENTITY_COLLISION( tempObj, FALSE )
														
														PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE OBJ - Dropped object off - Setting invisible; SHOULD_OBJ_CLEAN_UP_ON_DELIVERY ",SHOULD_OBJ_CLEAN_UP_ON_DELIVERY(i, iTeam),", cibsOBJ_InvisibleOnFirstDelivery ",IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitSet, cibsOBJ_InvisibleOnFirstDelivery))
													ENDIF
												ENDIF
												
												IF IS_ARENA_WARS_JOB(TRUE)
												AND IS_THIS_ROCKSTAR_MISSION_NEW_VS_ARENA_CTF(g_FMMC_STRUCT.iAdversaryModeType)
													g_sArena_Telemetry_data.m_flagsDelivered++
													PRINTLN("[ARENA][TEL] - Flags delivered ", g_sArena_Telemetry_data.m_flagsDelivered)
												ENDIF
												
												IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TRANSFORM_ASSETS)
												AND MC_serverBD.iObjCarrier[i] = iLocalPart
												AND SHOULD_CLEANUP_OBJECT_TRANSFORMATION_NOW()
													CLEANUP_OBJECT_TRANSFORMATIONS()
												ENDIF
												IF MC_serverBD.iObjCarrier[i] = iLocalPart
													CLEANUP_OBJECT_INVENTORIES()
												ENDIF
												
												IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_ARENA_CTF(g_FMMC_STRUCT.iAdversaryModeType)
													MC_playerBD[iLocalPart].iNumberOfDeliveries++
													PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - Object ", i," delivered. Number of deliveries: ", MC_playerBD[iLocalPart].iNumberOfDeliveries)
												ENDIF
												
												SET_TEAM_PICKUP_OBJECT( tempObj,  iTeam , FALSE)
											ELSE
												PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE OBJ - ciBS_RULE6_GET_DELIVER_DONT_DROP_ON_DELIVER IS SET - OBJECT WILL NOT BE DROPPED BY THE PLAYER")
											ENDIF
										ENDIF
										
										iDropOffEntity = i
										iscoremulti++
										bvalidreward = TRUE
									ENDIF
								ELSE
									IF IS_VEHICLE_DRIVEABLE(tempVeh)
										CLEAR_BIT( iobjcolBoolCheck, i )
										SET_BIT( iObjDeliveredBitset, i )
										IF GET_ENTITY_MODEL(tempVeh) = CARGOBOB
										OR GET_ENTITY_MODEL(tempVeh) = CARGOBOB2
										OR GET_ENTITY_MODEL(tempVeh) = CARGOBOB3
											DETACH_VEHICLE_FROM_CARGOBOB(tempVeh, tempVeh) //detach anything from this cargobob
											SET_CARGOBOB_PICKUP_MAGNET_ACTIVE(tempVeh, FALSE)
											//DETACH_ENTITY(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[i]))
											PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE OBJ - detach being called for cargobob on crate: ",i) 
										ELIF GET_ENTITY_MODEL(tempVeh) = HANDLER
											DETACH_CONTAINER_FROM_HANDLER_FRAME(tempVeh)
										ELSE
											//DETACH_ENTITY(NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niObject[i]),TRUE,FALSE)
											SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(tempVeh,TRUE)
											
											FORCE_EVERYONE_FROM_MY_CAR(TRUE,TRUE)
										ENDIF
										iDropOffEntity = i
										iscoremulti++
										bvalidreward = TRUE
										SET_BIT(iLocalBoolCheck2, LBOOL2_USE_CRATE_DELIVERY_DELAY)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
					IF bvalidreward 
					AND bValid
						INT iShardOption 
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_RUGBY_SOUNDS)
							// 2712398 shard for when a package is delivered
							iShardOption = ciTICKER_AND_SHARD  
						ELSE
							iShardOption = ciTICKER_ONLY
						ENDIF
						
						INT isublogic = GET_CTF_TICKER_ICON_TYPE_TO_DISPLAY()
						
						// Award, Watch your back when transporting a package or vehicle. The opposition will be after you. Drop off packages in any Capture mode.
						IF Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer)
							IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
								PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE OBJ - AWARD, MP_AWARD_DROPOFF_CAP_PACKAGES ")
								INCREMENT_MP_INT_CHARACTER_AWARD(MP_AWARD_DROPOFF_CAP_PACKAGES)
							ELSE
								PRINTLN("[RCC MISSION] AWARD, MP_AWARD_DROPOFF_CAP_PACKAGES NOT INCREMENTED because in fake MP mode ")
							ENDIF
						ENDIF
						BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_DELIVER_OBJ, iscoremulti, iTeam ,isublogic,NULL,ci_TARGET_OBJECT,iDropOffEntity,TRUE,iShardOption, IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_IN_AND_OUT_SOUNDS))
						IF  iRule  <  MC_serverBD.iMaxObjectives[ iTeam ] 
						OR MC_ServerBD.iNumObjHighestPriority[ iTeam ] > 1
						OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iDropOffEntity].sObjBlipStruct.iBlipStyle = FMMC_OBJECTBLIP_CTF
							IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
							AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iRule], ciBS_RULE2_DONT_GIVE_PLAYER_XP)
								IF iPackageDelXPCount < 10

									fDelXp = (100*MC_playerBD[iPartToUse].iObjCarryCount*g_sMPTunables.fxp_tunable_Deliver_a_package_bonus)
									iDelXp = ROUND(fDelXp)
									GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, LocalPlayerPed,"XPT_MEDIUMT", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_MISSION_DELIVERED_OBJ, iDelXp,1)
									#IF IS_DEBUG_BUILD
										iDebugXPDelObj= iDebugXPDelObj + iDelXp
									#ENDIF
									iPackageDelXPCount = iPackageDelXPCount + MC_playerBD[iPartToUse].iObjCarryCount
								ENDIF

							ENDIF
						ELSE
							IF iPackageDelXPCount < 10
								iDelayedDeliveryXPToAward++
							ENDIF
						ENDIF
						
						MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_DELIVER_OBJ
						IF IS_BIT_SET(iLocalBoolCheck9, LBOOL9_SHOULD_STOP_VEHICLE)
							CLEAR_BIT(iLocalBoolCheck9, LBOOL9_SHOULD_STOP_VEHICLE)
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED LBOOL9_SHOULD_STOP_VEHICLE was set, clearing...")
						ENDIF
						SET_MISSION_TEST_COMPLETE()	
						
						iBroadcastPriority = iRule
						iBroadcastRevalidate = MC_serverBD_4.iTimesRevalidatedObjectives[iTeam]
						BROADCAST_FMMC_OBJECTIVE_COMPLETE(MC_playerBD[iPartToUse].iObjectiveTypeCompleted, iPartToUse, iObjDeliveredBitset, iRule, MC_serverBD_4.iTimesRevalidatedObjectives[iTeam], iBitsetArrayBlank)
						REINIT_NET_TIMER(tdeventsafetytimer)
						INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(iscoremulti)
						increment_medal_objective_completed(iTeam, iRule)
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_CTF_SCREEN_FILTER_ON_HOLDING_FLAG)
						AND NOT IS_BIT_SET(iLocalBoolCheck29, LBOOL29_DO_DELIVER_FX)
							SET_BIT(iLocalBoolCheck29, LBOOL29_DO_DELIVER_FX)
							PRINTLN("HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - Setting LBOOL29_DO_DELIVER_FX")
						ENDIF
						
//						PLAY_SOUND_FRONTEND(-1, "DROP_OFF_PACKAGE","HUD_MINI_GAME_SOUNDSET", FALSE)
//						IF NOT IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
//							SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_MIDSIZED, "PCK_DEL") 
//						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_RUGBY_SOUNDS)
							BROADCAST_RUGBY_PLAY_SOUND(ciRUGBY_SOUND_SCORED)
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_IN_AND_OUT_SOUNDS)
							BROADCAST_IN_AND_OUT_SFX_PLAY(ciIN_AND_OUT_SOUND_DELIVERED, iTeam)
							SET_BIT(iLocalBoolCheck16, LBOOL16_IAO_BLOCK_PACKAGE_SOUNDS)
							REINIT_NET_TIMER(stBlockPackageSoundsTimer)
							
							CLEAR_BIT(iLocalBoolCheck16, LBOOL16_HAS_PLAYED_IAO_PICKUP_SOUND)
						ENDIF
						CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
						CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER)
						MC_playerBD[iPartToUse].iObjCarryCount = 0
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TRANSFORM_ASSETS)
						AND SHOULD_CLEANUP_OBJECT_TRANSFORMATION_NOW()
							CLEANUP_OBJECT_TRANSFORMATIONS()
						ENDIF
						CLEANUP_OBJECT_INVENTORIES()
						PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - JOB_COMP_DELIVER_OBJ set locally by part: ",iPartToUse) 
						RETURN TRUE
					ENDIF
				ELSE
					IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_GOTO_LOC_POSTFX)
						CLEAR_BIT(iLocalBoolCheck10, LBOOL10_GOTO_LOC_POSTFX)
						PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE OBJ Cleared LBOOL10_GOTO_LOC_POSTFX")
					ENDIF
				ENDIF
			ENDIF
			
			IF MC_playerBD[iPartToUse].iVehCarryCount > 0
			AND MC_serverBD.iNumVehHighestPriorityHeld[iTeam] >= MC_serverBD.iRequiredDeliveries[iTeam]
				BOOL bValid = TRUE
				IF NOT ARE_ALL_PLAYERS_IN_DROP_OFF_CONDITIONS(iTeam, iRule)
					bValid = FALSE
				ENDIF
				
				IF MC_playerBD[iPartToUse].iVehDeliveryId = -1
					
					IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					
						tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
						
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffBitSet,  iRule )
							bdrawAirDropOff = TRUE
						ENDIF
						
						IF IS_PLAYER_IN_DROP_OFF(fradius, GET_DROP_OFF_HEIGHT(iTeam, iRule))
						AND IS_TEAM_IN_VEHICLE_ACCURATE(iRule, iTeam)
						
							#IF IS_DEBUG_BUILD
							IF bPlaneDropoffPrints
								PRINTLN("[RCC MISSION][tom] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - 2 - In drop off!")
							ENDIF
							#ENDIF
							
							IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
								CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
							ENDIF
							
							IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_NO_DISPLAY)
								IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
									SET_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
									PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH SET_LOCATE_TO_FADE_OUT - 3, radius = ", g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].fDropOffRadius[ iRule ] )
								
									IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iRule], ciBS_RULE3_ALL_IN_SEPARATE_VEHICLES)
									OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_PROGRESS_GAD_AT_SAME_TIME)
										IF NOT IS_BIT_SET(iLocalBoolCheck2,LBOOL2_REQUEST_UPDATE)
											CLEAR_BIT(iLocalBoolCheck2,LBOOL2_WHOLE_UPDATE_DONE)
											SET_BIT(iLocalBoolCheck2,LBOOL2_REQUEST_UPDATE)
											PRINTLN("[JS] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE requesting update new vehnear(objectivecomp) = ", MC_playerBD[iPartToUse].iVehNear)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
				
							SET_BIT(MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
							
							GET_VEHICLE_TRAILER_VEHICLE(tempVeh,trailerVeh)
							IF GET_ENTITY_MODEL(tempVeh) = CARGOBOB
							OR GET_ENTITY_MODEL(tempVeh) = CARGOBOB2
							OR GET_ENTITY_MODEL(tempVeh) = CARGOBOB3
								Winchent = GET_VEHICLE_ATTACHED_TO_CARGOBOB(tempVeh)
								IF DOES_ENTITY_EXIST(Winchent)
									Winchveh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(Winchent)
								ENDIF
								bcargobobdrop = TRUE
							ELSE
								IF IS_BIT_SET(iLocalBoolCheck,LBOOL_CARGOBOB_DROP)
									CLEAR_BIT(iLocalBoolCheck,LBOOL_CARGOBOB_DROP)
									PRINTLN("HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - Clearing LBOOL_CARGOBOB_DROP because we're not in a cargobob any more")
								ENDIF
							ENDIF
							
							IF bValid
								IF IS_CURRENT_VEHICLE_RULE_OF_TYPE_CARGOBOB_FOR_TEAM(iTeam, iRule)
									//Cargobob drop off:
									
									INT iDeliveryVeh = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOff_Vehicle[ iRule ]
									
									IF IS_VEHICLE_DRIVEABLE(tempVeh)
									AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iDeliveryVeh])
										
										VEHICLE_INDEX cargobobVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iDeliveryVeh])
										
										IF IS_VEHICLE_DRIVEABLE(cargobobVeh)
										AND GET_PED_IN_VEHICLE_SEAT(tempveh) = LocalPlayerPed
											IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
											AND NETWORK_HAS_CONTROL_OF_ENTITY(cargobobVeh)
												IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(tempVeh,cargobobVeh)
													
													IF NOT DOES_CARGOBOB_HAVE_PICKUP_MAGNET(cargobobVeh)
														IF NOT DOES_CARGOBOB_HAVE_PICK_UP_ROPE(cargobobVeh)
															PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH, MAGNET - Cargobob doesn't have pickup rope or magnet, so just attach normally")
															CREATE_PICK_UP_ROPE_FOR_CARGOBOB(cargobobVeh)
															SET_PICKUP_ROPE_LENGTH_FOR_CARGOBOB(cargobobVeh,6,4,TRUE)
															ATTACH_VEHICLE_TO_CARGOBOB(cargobobVeh,tempVeh,-1,<<0,0,1>>)
															SET_CARGOBOB_FORCE_DONT_DETACH_VEHICLE(cargobobVeh, TRUE)
														ELSE
															PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH, MAGNET - Cargobob doesn't have magnet, so just attach normally")
															ATTACH_VEHICLE_TO_CARGOBOB(cargobobVeh,tempVeh,-1,<<0,0,1>>)
															SET_CARGOBOB_FORCE_DONT_DETACH_VEHICLE(cargobobVeh, TRUE)
															SET_BIT(iLocalBoolCheck4, LBOOL4_DROPOFF_TO_CARGOBOB)
															bstopped = TRUE
														ENDIF
													ELSE
														CLEAR_BIT(MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
														PRINTLN("[MMacK][Magnet] - Setting Vehicle as attached entity")

														FLOAT fVehSpeed = GET_ENTITY_SPEED(tempVeh)
														FLOAT fCargobobSpeed = GET_ENTITY_SPEED(cargobobVeh)
														FLOAT fTol = (fCargobobSpeed/100) * 34
														
														SET_CARGOBOB_PICKUP_MAGNET_SET_TARGETED_MODE(cargobobVeh, tempVeh)
														
														IF (fCargobobSpeed >= (fVehSpeed - fTol) 
														AND fCargobobSpeed <= (fVehSpeed + fTol)
														AND bIsInMagnetRange)
														OR (bMegaMagnetOn AND bIsInMagnetRange)
															PRINTLN("[MMacK][Magnet] - Speed Tolerance Is OK - MEGAMAGNET")
															SET_CARGOBOB_PICKUP_MAGNET_ACTIVE(cargobobVeh, TRUE)
															SET_CARGOBOB_PICKUP_MAGNET_PULL_STRENGTH(cargobobVeh, 1)
															SET_CARGOBOB_PICKUP_MAGNET_STRENGTH(cargobobVeh, 1.5)
															SET_CARGOBOB_PICKUP_MAGNET_REDUCED_STRENGTH(cargobobVeh, 0.8)
															SET_ENTITY_PROOFS(tempVeh, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, TRUE)
															SET_CINEMATIC_MODE_ACTIVE(FALSE)
															bMegaMagnetOn = TRUE
														ELSE	
															PRINTLN("[MMacK][Magnet] - fCargobobSpeed : ", fCargobobSpeed)
															PRINTLN("[MMacK][Magnet] - fVehSpeed : ", fVehSpeed)
															SET_CARGOBOB_PICKUP_MAGNET_ACTIVE(cargobobVeh, FALSE)
															SET_CARGOBOB_PICKUP_MAGNET_PULL_STRENGTH(cargobobVeh, 0)
															SET_CARGOBOB_PICKUP_MAGNET_STRENGTH(cargobobVeh, 0.5)
															
															FLOAT fRange	 = 8.0
															FLOAT fMinForce	 = 0.3
															FLOAT fMaxForce  = 1.3
															
															#IF IS_DEBUG_BUILD
															IF( bEnableMagnetTuning )
																fRange = fMagnetRange
																fMinForce = fMagnetMin
																fMaxForce = fMagnetMax
															ENDIF
															#ENDIF
															
															VECTOR vPlayerPos = GET_ENTITY_COORDS( tempVeh, FALSE )
															VECTOR vMagnetPos = GET_ENTITY_COORDS( cargobobVeh, FALSE )
															FLOAT fDist = VDIST( vPlayerPos, vMagnetPos )
															FLOAT fUnit = 1.0 - ( CLAMP( fDist, 0.0, fRange ) / fRange )
															FLOAT fStength = LERP_FLOAT( fMinForce, fMaxForce, fUnit )
															SET_CARGOBOB_PICKUP_MAGNET_REDUCED_STRENGTH(cargobobVeh, fStength)
														ENDIF
													ENDIF
													
												ELSE
													PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH, MAGNET - Vehicle is attached, calling stabilise")
													SET_BIT(iLocalBoolCheck4, LBOOL4_DROPOFF_TO_CARGOBOB)
													//STABILISE_ENTITY_ATTACHED_TO_HELI(cargobobVeh,tempVeh,-6)
													bstopped = TRUE
												ENDIF
											ELSE
												PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH, MAGNET - I don't have control of car / cargobob to deliver and I'm the driver, request control of both")
												NETWORK_REQUEST_CONTROL_OF_ENTITY(tempVeh)
												NETWORK_REQUEST_CONTROL_OF_ENTITY(cargobobVeh)
											ENDIF
										#IF IS_DEBUG_BUILD
										ELSE
											IF NOT IS_VEHICLE_DRIVEABLE(cargobobVeh)
												PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH, MAGNET - Cargobob isn't driveable")
											ENDIF
											IF GET_PED_IN_VEHICLE_SEAT(tempveh) != LocalPlayerPed
												PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH, MAGNET - I'm not the driver of the car")
											ENDIF
										#ENDIF
										ENDIF
									
									#IF IS_DEBUG_BUILD
									ELSE
										IF NOT IS_VEHICLE_DRIVEABLE(tempVeh)
											PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH, MAGNET - Dropoff car is undriveable")
										ENDIF
										IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iDeliveryVeh])
											PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH, MAGNET - Cargobob net ID doesn't exist")
										ENDIF
									#ENDIF
									ENDIF
									
								ELSE
									#IF IS_DEBUG_BUILD
									IF bPlaneDropoffPrints
										PRINTLN("[RCC MISSION][tom] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE -1 - This isn't a cargobob dropoff")
									ENDIF
									#ENDIF
									
									IF NOT bdrawAirDropOff
										
										#IF IS_DEBUG_BUILD
										IF bPlaneDropoffPrints
											PRINTLN("[RCC MISSION][tom] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE 0 - This isn't an air dropoff")
										ENDIF
										#ENDIF
										
										IF IS_VEHICLE_DRIVEABLE(tempVeh)
											
											#IF IS_DEBUG_BUILD
											IF bPlaneDropoffPrints
												PRINTLN("[RCC MISSION][tom] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE 1 - The dropoff vehicle is driveable")
											ENDIF
											#ENDIF
											
											IF IS_PED_IN_ANY_HELI(LocalPlayerPed)
											OR IS_PED_IN_ANY_PLANE(LocalPlayerPed)
												
												#IF IS_DEBUG_BUILD
												IF bPlaneDropoffPrints
													PRINTLN("[RCC MISSION][tom] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE 2 - We're in a helicopter or plane, speed = ",GET_ENTITY_SPEED(tempVeh))
												ENDIF
												#ENDIF
												
												IF (GET_ENTITY_SPEED(tempVeh) < 5.0 
												OR NOT IS_ENTITY_IN_AIR(tempVeh))
													
													#IF IS_DEBUG_BUILD
													IF bPlaneDropoffPrints
														PRINTLN("[RCC MISSION][tom] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE 3 - We're slow enough or not in the air, my landing gear state = ",ENUM_TO_INT(GET_LANDING_GEAR_STATE(tempVeh)),", on all wheels = ",IS_VEHICLE_ON_ALL_WHEELS(tempVeh))
													ENDIF
													#ENDIF
													
													MODEL_NAMES mTempVeh = GET_ENTITY_MODEL(tempVeh)
													
													IF NOT IS_THIS_MODEL_A_PLANE(mTempVeh)
													OR (IS_THIS_MODEL_A_PLANE(mTempVeh) 
														AND ((GET_LANDING_GEAR_STATE(tempVeh) = LGS_BROKEN
														OR NOT IS_PLANE_LANDING_GEAR_INTACT(tempVeh))
														OR((GET_LANDING_GEAR_STATE(tempVeh) = LGS_LOCKED_DOWN) 
															AND IS_VEHICLE_ON_ALL_WHEELS(tempVeh))))
														
														#IF IS_DEBUG_BUILD
														IF bPlaneDropoffPrints
															PRINTLN("[RCC MISSION][tom] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE 4 - We're on all wheels, set bstopped = TRUE")
														ENDIF
														#ENDIF
														
														IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
															
															#IF IS_DEBUG_BUILD
															IF bPlaneDropoffPrints
																PRINTLN("[RCC MISSION][tom] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE 5 - We have control of the vehicle")
															ENDIF
															#ENDIF
															
															SET_VEHICLE_HANDBRAKE(tempVeh,TRUE)
															
															IF NOT IS_BIT_SET(iLocalBoolCheck2, LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
																
																#IF IS_DEBUG_BUILD
																IF bPlaneDropoffPrints
																	PRINTLN("[RCC MISSION][tom] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE 6 - The vehicle hasn't yet been called to a halt")
																ENDIF
																#ENDIF
																
																//BRING_VEHICLE_TO_HALT(tempVeh,BVTH_STOPPING_DISTANCE,BVTH_TIME_TO_STOP_FOR)
																//NET_SET_PLAYER_CONTROL(LocalPlayer,FALSE,SPC_ALLOW_PLAYER_DAMAGE|SPC_PREVENT_EVERYBODY_BACKOFF|SPC_LEAVE_CAMERA_CONTROL_ON|SPC_REENABLE_CONTROL_ON_DEATH|SPC_ALLOW_PAD_SHAKE)
																IF NOT IS_ENTITY_IN_AIR(tempVeh)
																	#IF IS_DEBUG_BUILD
																	IF bPlaneDropoffPrints
																		PRINTLN("[RCC MISSION][tom] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE 7 - We're not in the air, set the 'bring to halt called' bit")
																	ENDIF
																	#ENDIF
																	SET_BIT(iLocalBoolCheck2, LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
																ENDIF
															ENDIF
														ENDIF
														bstopped = TRUE
													ELSE
														// in a plane, but landing gear not down
														CLEAR_BIT(MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
													ENDIF
												ENDIF
											ELSE
												IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
													IF NOT IS_PED_IN_ANY_BOAT(LocalPlayerPed)
													AND GET_ENTITY_MODEL(tempVeh) != SUBMERSIBLE
													AND GET_ENTITY_MODEL(tempVeh) != STROMBERG
														IF NOT IS_BIT_SET(iLocalBoolCheck2, LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
															SET_BIT(iLocalBoolCheck2, LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
														ENDIF
													ENDIF
												ENDIF
												bstopped = TRUE
											ENDIF
										ELSE
											bstopped = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							IF bstopped
							OR bdrawAirDropOff
							OR bcargobobdrop
																
								PRINTLN("[RCC MISSION][tom] A - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - bstopped is true (or this is an air dropoff or cargobob dropoff)")
								
								FOR i = 0 TO (MC_serverBD.iNumVehCreated - 1)
									
									PRINTLN("[RCC MISSION][tom] B - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE -  checking vehicle ",i)
									
									IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
									AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]))
										
										PRINTLN("[RCC MISSION][tom] C - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE -  vehicle ",i," exists and is driveable")
										
										IF NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]) = tempVeh
										OR NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]) = trailerVeh
										OR NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]) = WinchVeh
											
											PRINTLN("[RCC MISSION][tom] D - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE -  vehicle ",i," is the dropoff vehicle (it's tempVeh, trailerVeh or WinchVeh)")
																					
											IF GET_PED_IN_VEHICLE_SEAT(tempVeh) = LocalPlayerPed
											AND IS_PED_SITTING_IN_ANY_VEHICLE(LocalPlayerPed)
											AND GET_SCRIPT_TASK_STATUS(LocalPlayerPed, SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT) != PERFORMING_TASK
											AND GET_SCRIPT_TASK_STATUS(LocalPlayerPed, SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT) != WAITING_TO_START_TASK
											AND GET_SCRIPT_TASK_STATUS(LocalPlayerPed, SCRIPT_TASK_LEAVE_VEHICLE) != PERFORMING_TASK
											AND GET_SCRIPT_TASK_STATUS(LocalPlayerPed, SCRIPT_TASK_LEAVE_VEHICLE) != WAITING_TO_START_TASK
											AND NOT GET_PED_RESET_FLAG(LocalPlayerPed, PRF_IsSeatShuffling) 
											
												PRINTLN("[RCC MISSION][tom] E - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE -  I'm in the driver's seat")
												
												IF (MC_serverBD_4.ivehRule[i][iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
												OR MC_serverBD_4.ivehRule[i][iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD)
												AND ARE_ALL_PLAYERS_IN_SEPARATE_VEHICLES_AND_LOCATE(iRule, iTeam)
												AND ARE_ALL_PLAYERS_IN_UNIQUE_VEHICLES_AND_LOCATE(iRule, iTeam)
												
													// block the transition for avenger
													IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[iRule], ciBS_RULE10_PROGRESS_RULE_ALL_DRIVER_ACCEL)
														IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_REQUEST_AVENGER_HOLD_TRANSITION_BLOCK)
															REQUEST_AVENGER_HOLD_TRANSITION_BLOCKED(TRUE)
														ENDIF
													ENDIF
													
													IF IS_AVENGER_HOLD_TRANSITION_BLOCKED_AND_INACTIVE(iRule, iTeam) // holds up waiting for all machines to block avenger transitions before allowing progression
													AND IS_DRIVER_PRESSING_ACCELERATE(iRule, iTeam)
														PRINTLN("[RCC MISSION][tom] F - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE -  This vehicle is on a get & deliver (or collect and hold)")
														
														IF MC_serverBD_4.iVehPriority[i][ iTeam ] < FMMC_MAX_RULES
															
															PRINTLN("[RCC MISSION][tom] G - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE -  This vehicle has a priority less than max rules")
															
															IF bdrawAirDropOff
															OR bcargobobdrop
															OR IS_BIT_SET(iLocalBoolCheck4, LBOOL4_DROPOFF_TO_CARGOBOB)
															OR (NOT IS_PED_IN_ANY_HELI(LocalPlayerPed))
															OR (NOT IS_ENTITY_IN_AIR(tempVeh))
																
																PRINTLN("[RCC MISSION][tom] H - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE -  This vehicle is either: bdrawAirDropOff, bcargobobdrop, dropoff to cargobob, not a heli, or landed")
																															
																MC_playerBD[iPartToUse].iVehNear = -1
																
																#IF IS_DEBUG_BUILD
																	IF bInVehicleDebug
																		PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH (-1 CHECK) (2) Setting MC_playerBD[iPartToUse].iVehNear = -1 for part ",iPartToUse, " who is player ",  GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartToUse))))
																	ENDIF
																#ENDIF
																DeliveryVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
																
																IF NOT (bdrawAirDropOff OR bcargobobdrop OR IS_BIT_SET(iLocalBoolCheck4, LBOOL4_DROPOFF_TO_CARGOBOB))
																	IF IS_PED_IN_ANY_HELI(LocalPlayerPed)
																		IF NOT IS_ENTITY_IN_AIR(tempVeh)
																			MC_playerBD[iPartToUse].iVehDeliveryId = i
																			SET_BIT(MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_DROPPING_OFF)
																		ENDIF
																	ELSE
																		MC_playerBD[iPartToUse].iVehDeliveryId = i
																		SET_BIT(MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_DROPPING_OFF)
																	ENDIF
																	
																ELSE
																	MC_playerBD[iPartToUse].iVehDeliveryId = i
																ENDIF
																
																IF NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]) = WinchVeh
																	SET_BIT(iLocalBoolCheck,LBOOL_CARGOBOB_DROP)
																ENDIF
																PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - Setting vehicle as ready for deliver: ", MC_playerBD[iPartToUse].iVehDeliveryId) 
																
																IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_PROGRESS_GAD_AT_SAME_TIME)
																	PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - SETTING LBOOL24_BLOCK_OBJECTIVE_TEXT_UNTIL_RULE_CHANGE") 
																	SET_BIT(iLocalBoolCheck24, LBOOL24_BLOCK_OBJECTIVE_TEXT_UNTIL_RULE_CHANGE)
																ENDIF
																
																//Break out!
																i = MC_serverBD.iNumVehCreated
																
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDFOR
							ENDIF
						ELSE
							IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
								IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
									PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH Set LBOOL10_DROP_OFF_LOC_DISPLAY")
									SET_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
									CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
									CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_NO_DISPLAY)
								ENDIF
							ENDIF
							
							FLOAT fTemp
							vThisMarkerLoc = GET_COORDS_FOR_LOCATE_MARKER(fTemp)
							fMarkerDist2 = VDIST2(GET_ENTITY_COORDS(LocalPlayerPed), vThisMarkerLoc)

							IF fMarkerDist2 <= (fMarkerDisappear * fMarkerDisappear)
								IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
									PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - LBOOL10_DROP_OFF_LOC_FADE_OUT -Veh drop off fMarkerDist2 = ", fMarkerDist2, " fMarkerDisappear = ", fMarkerDisappear, " vThisMarkerLoc = ", vThisMarkerLoc)
									SET_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
									CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_NO_DISPLAY)
									CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
								ENDIF
							ELSE
								
								IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
									IF fMarkerDist2 >= ((fMarkerDisappear + 3.0) * (fMarkerDisappear + 3.0))	
										PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - Set  LBOOL10_DROP_OFF_LOC_DISPLAY - Veh drop off- had been removed but need to display fMarkerDist2 = ", fMarkerDist2, " fMarkerDisappear = ", (fMarkerDisappear + 3.0), " vThisMarkerLoc = ", vThisMarkerLoc)
										CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
										SET_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
									ENDIF
								ENDIF
							ENDIF
							
							IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_GOTO_LOC_POSTFX)
								CLEAR_BIT(iLocalBoolCheck10, LBOOL10_GOTO_LOC_POSTFX)
								PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED Cleared LBOOL10_GOTO_LOC_POSTFX - 2")
							ENDIF
							
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF NOT bvalidreward
			IF MC_playerBD[iPartToUse].iVehDeliveryId != -1
				
				IF IS_VEHICLE_DRIVEABLE(DeliveryVeh)
				AND NOT bHasVehDropoffChanged
					
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffBitSet,  iRule )
						bdrawAirDropOff = TRUE
					ENDIF
					
					IF MC_serverBD_4.ivehRule[MC_playerBD[iPartToUse].iVehDeliveryId][ iTeam ] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
						bcollect = TRUE
					ENDIF
					
					IF bdrawAirDropOff
						IF bcollect
							bvalidreward = TRUE
						ELSE
							DeliveryVeh = NULL
							MC_playerBD[iPartToUse].iVehDeliveryId = -1
							MC_playerBD[iPartToUse].iVehCarryCount = 0
						ENDIF
					ELSE
						IF IS_BIT_SET(iLocalBoolCheck,LBOOL_CARGOBOB_DROP)
							IF bcollect
								IF SHOULD_LOCK_VEHICLE_ON_DELIVERY(MC_playerBD[iPartToUse].iVehDeliveryId, iTeam )
									SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS( DeliveryVeh, TRUE )
									IF NETWORK_HAS_CONTROL_OF_ENTITY(DeliveryVeh)
										LOCK_DOORS_WHEN_NO_LONGER_NEEDED(DeliveryVeh)
									ENDIF									
									PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - CARGOBOB DELIVERY -LOCK VEH DOORS CALLED ON VEH: ",MC_playerBD[iPartToUse].iVehDeliveryId) 
								ENDIF
								DETACH_VEHICLE_FROM_ANY_CARGOBOB(DeliveryVeh)
								bvalidreward = TRUE
								CLEAR_BIT(iLocalBoolCheck,LBOOL_CARGOBOB_DROP)
							ELSE
								CLEAR_BIT(iLocalBoolCheck,LBOOL_CARGOBOB_DROP)
								DeliveryVeh = NULL
								MC_playerBD[iPartToUse].iVehDeliveryId =-1
								MC_playerBD[iPartToUse].iVehCarryCount = 0
							ENDIF
						ELIF IS_BIT_SET(iLocalBoolCheck4, LBOOL4_DROPOFF_TO_CARGOBOB)
							IF bcollect
								IF SHOULD_LOCK_VEHICLE_ON_DELIVERY(MC_playerBD[iPartToUse].iVehDeliveryId, iTeam )
									SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS( DeliveryVeh, TRUE )
									LOCK_DOORS_WHEN_NO_LONGER_NEEDED(DeliveryVeh)
									PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - DELIVERY TO CARGOBOB HOOK - LOCK VEH DOORS CALLED ON VEH: ",MC_playerBD[iPartToUse].iVehDeliveryId) 
								ENDIF
								bvalidreward = TRUE
								CLEAR_BIT(iLocalBoolCheck4, LBOOL4_DROPOFF_TO_CARGOBOB)
							ELSE
								CLEAR_BIT(iLocalBoolCheck4, LBOOL4_DROPOFF_TO_CARGOBOB)
								DeliveryVeh = NULL
								MC_playerBD[iPartToUse].iVehDeliveryId =-1
								MC_playerBD[iPartToUse].iVehCarryCount = 0
							ENDIF
						ELSE
							fStopDist = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffStopDistance[iRule]
							IF fStopDist <= 0.0
								fStopDist = DEFAULT_VEH_STOPPING_DISTANCE
							ELIF fStopDist > 20.0
								fStopDist = 20.0
							ENDIF
							
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE, Trying to stop vehicle, fStopDist = ", fStopDist)
							
							IF NOT SHOULD_VEH_STOP_ON_DELIVERY()
							OR IS_MY_VEHICLE_STOPPED(DEFAULT, fStopDist)
								SET_BIT(MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
								IF NOT HAS_NET_TIMER_STARTED(tdDeliverTimer)
									REINIT_NET_TIMER(tdDeliverTimer)
								ELSE
									IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdDeliverTimer) > 500
										IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
											GET_VEHICLE_TRAILER_VEHICLE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed),trailerVeh)
										ENDIF
										IF DeliveryVeh != trailerVeh
											IF FMMC_FORCE_EVERYONE_FROM_VEHICLE(MC_playerBD[iPartToUse].iVehDeliveryId)
												IF IS_VEHICLE_DRIVEABLE(DeliveryVeh)
													IF NETWORK_HAS_CONTROL_OF_ENTITY(DeliveryVeh)
														IF SHOULD_STAY_IN_ON_DELIVERY()
															SET_VEHICLE_HANDBRAKE(DeliveryVeh,FALSE) 
														ENDIF
													ENDIF
												ENDIF
												
												//-- Only turn player control back on if it's not the last rule (2150083)
												//-- Otherwise players can drive around before the mission ends.
												IF SHOULD_STAY_IN_ON_DELIVERY()
													PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - SHOULD_STAY_IN_ON_DELIVERY check veh:", MC_playerBD[iPartToUse].iVehDeliveryId, " iRule = ", iRule , " iMaxObjectives = ", MC_serverBD.iMaxObjectives[iTeam]  ," iNumVehHighestPriority = ",MC_ServerBD.iNumVehHighestPriority[ iTeam ])
													
												//	IF iRule < MC_serverBD.iMaxObjectives[ iTeam ] 
												//	OR MC_ServerBD.iNumVehHighestPriority[ iTeam ] > 1
													IF SHOULD_TURN_PLAYER_CONTROL_ON_AFTER_DELIVERY()
														PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - Turning control back on after veh delivery (1) veh:", MC_playerBD[iPartToUse].iVehDeliveryId)
														NET_SET_PLAYER_CONTROL(LocalPlayer,TRUE)
														
														
													//	IF MC_ServerBD.iNumVehHighestPriority[ iTeam ] <= 1
															iTimeToStop = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iDropOffStopTimer[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]]
															IF iTimeToStop > 0
																PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH Will stop vehicle for duration veh: ",  MC_playerBD[iPartToUse].iVehDeliveryId, " Time: ", iTimeToStop, " Team: ", MC_playerBD[iPartToUse].iteam, " Rule: ", MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
																
																INT iMissionEntity = IS_ENTITY_A_MISSION_CREATOR_ENTITY(DeliveryVeh)
																IF iMissionEntity > -1
																	PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE (1a) Setting iVeh: ", MC_playerBD[iPartToUse].iVehDeliveryId, " BROADCAST_FMMC_DELIVER_VEHICLE_STOP")
																	BROADCAST_FMMC_DELIVER_VEHICLE_STOP(MC_playerBD[iPartToUse].iVehDeliveryId, iLocalPart, iTimeToStop)
																ENDIF
																
																SET_BIT(iLocalBoolCheck24, LBOOL24_BLOCK_VEHICLE_KERS_ON_RULE_CHECK)
																PRINTLN("[RCC MISSION] LBOOL24_BLOCK_VEHICLE_KERS_ON_RULE_CHECK setting")
															ELIF iTimeToStop = -1
																PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH Will stop vehicle forever veh: ",  MC_playerBD[iPartToUse].iVehDeliveryId, " Time: ", iTimeToStop, " Team: ", MC_playerBD[iPartToUse].iteam, " Rule: ", MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
																
																SET_BIT(iLocalBoolCheck10, LBOOL10_STOP_CAR_FOREVER)					
																INT iMissionEntity = IS_ENTITY_A_MISSION_CREATOR_ENTITY(DeliveryVeh)
																IF iMissionEntity > -1
																	PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE (1b) Setting iVeh: ", MC_playerBD[iPartToUse].iVehDeliveryId, " BROADCAST_FMMC_DELIVER_VEHICLE_STOP")
																	BROADCAST_FMMC_DELIVER_VEHICLE_STOP(MC_playerBD[iPartToUse].iVehDeliveryId, iLocalPart, iTimeToStop)
																ENDIF																																
																
																SET_BIT(iLocalBoolCheck24, LBOOL24_BLOCK_VEHICLE_KERS_ON_RULE_CHECK)
																PRINTLN("[RCC MISSION] LBOOL24_BLOCK_VEHICLE_KERS_ON_RULE_CHECK setting")
															ENDIF
													//	ENDIF
													ELSE
														PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - Not turning control back on after veh delivery veh:", MC_playerBD[iPartToUse].iVehDeliveryId)
														IF IS_NEXT_RULE_A_CUTSCENE()
															NET_SET_PLAYER_CONTROL(LocalPlayer,FALSE)
														ENDIF
													ENDIF
												ELSE
													PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - Turning control back on after veh delivery (2) veh:", MC_playerBD[iPartToUse].iVehDeliveryId)
													NET_SET_PLAYER_CONTROL(LocalPlayer,TRUE)
												ENDIF
												
												RESET_NET_TIMER(tdDeliverBackupTimer)
												
												IF SHUT_VEHICLE_DOORS(DeliveryVeh)
													PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - shut door true")
													// 1977126 - Owain
													
													IF bcollect
														bvalidreward = TRUE
														IF IS_VEHICLE_DRIVEABLE(DeliveryVeh)
														AND SHOULD_LOCK_VEHICLE_ON_DELIVERY(MC_playerBD[iPartToUse].iVehDeliveryId, iTeam )
															BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedContentEventType_AllPlayersToggleConsiderVehicle, MC_playerBD[iPartToUse].iVehDeliveryId)
															SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS( DeliveryVeh, TRUE )
															SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(DeliveryVeh, FALSE)
															LOCK_DOORS_WHEN_NO_LONGER_NEEDED( DeliveryVeh )
															CLEAR_LAST_DRIVEN_VEHICLE()
															PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - LOCK VEH DOORS CALLED ON VEH: ",MC_playerBD[iPartToUse].iVehDeliveryId)
														ENDIF
													ELSE
														DeliveryVeh = NULL
														MC_playerBD[iPartToUse].iVehDeliveryId = -1
														MC_playerBD[iPartToUse].iVehCarryCount = 0
													ENDIF
													
													IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUNNING_BACK_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
														IF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_DELIVERED_THIS_RULE)
															bvalidreward = FALSE
														ENDIF
													ENDIF
												ENDIF
											ELSE
												DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
											ENDIF
										ELSE
											IF SHOULD_VEH_STOP_ON_DELIVERY() //Stops trailer from stopping instantly if we want to keep it
												IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
													
													SET_BIT(iLocalBoolCheck4, LBOOL4_DELIVERY_WAIT)
													REINIT_NET_TIMER(tdDeliverTimer)
													
													//-- Only detach the trailer if it's not the last rule (2156538)
													IF iRule < MC_serverBD.iMaxObjectives[ iTeam ] 
													OR MC_ServerBD.iNumVehHighestPriority[ iTeam ] > 1
														
														PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - Detaching trailer ")
														DETACH_VEHICLE_FROM_TRAILER(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
														
													ELSE
														PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - Not detaching trailer as iRule = ", iRule, "MC_serverBD.iMaxObjectives[ iTeam ] = ", MC_serverBD.iMaxObjectives[ iTeam ], " MC_ServerBD.iNumVehHighestPriority[ iTeam ] = ", MC_ServerBD.iNumVehHighestPriority[ iTeam ])
													ENDIF
														
												ELSE
													NET_SET_PLAYER_CONTROL(LocalPlayer,TRUE)
												ENDIF
												RESET_NET_TIMER(tdDeliverBackupTimer)
											
												SET_VEHICLE_DISABLE_TOWING(trailerVeh,TRUE)
												IF GET_ENTITY_BONE_INDEX_BY_NAME(trailerVeh, "attach_female") <> -1
												OR GET_ENTITY_BONE_INDEX_BY_NAME(trailerVeh, "attach_male") <> -1 //my addition
													SET_VEHICLE_AUTOMATICALLY_ATTACHES(trailerVeh, FALSE, FALSE)
													PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - setting no further attachment for veh ") 
												ENDIF
												
												IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciALLOW_MOC_ENTRY_THIS_MISSION)
													BROADCAST_FMMC_EXIT_MISSION_MOC()
												ENDIF
											ENDIF
											
											
											
											IF bcollect
												bvalidreward = TRUE
											ELSE
												DeliveryVeh = NULL
												MC_playerBD[iPartToUse].iVehDeliveryId =-1
												MC_playerBD[iPartToUse].iVehCarryCount = 0
											ENDIF
											
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF IS_BIT_SET(iLocalBoolCheck2, LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
					OR HAS_NET_TIMER_STARTED(tdDeliverBackupTimer)
						NET_SET_PLAYER_CONTROL(LocalPlayer,TRUE)
						
						IF IS_PLAYER_IN_VEH_SEAT(LocalPlayer,VS_DRIVER)
						AND NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
							SET_VEHICLE_HANDBRAKE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed),FALSE)
						ENDIF
						
						RESET_NET_TIMER(tdDeliverBackupTimer)
						CLEAR_BIT(iLocalBoolCheck2,LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
						CLEAR_BIT(iLocalBoolCheck2,LBOOL2_PED_DROPPED_OFF)
						CLEAR_BIT(iLocalBoolCheck,LBOOL_CARGOBOB_DROP)
						CLEAR_BIT(MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
						CLEAR_BIT(MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_DROPPING_OFF)
					ENDIF
					DeliveryVeh = NULL
					MC_playerBD[iPartToUse].iVehDeliveryId =-1
					MC_playerBD[iPartToUse].iVehCarryCount = 0
				ENDIF
			ENDIF
		ENDIF
		IF bvalidreward
			IF MC_playerBD[iPartToUse].iVehDeliveryId != -1
				MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_DELIVER_VEH
				BROADCAST_FMMC_OBJECTIVE_COMPLETE(MC_playerBD[iPartToUse].iObjectiveTypeCompleted,iPartToUse,MC_playerBD[iPartToUse].iVehDeliveryId, iRule, MC_serverBD_4.iTimesRevalidatedObjectives[iTeam], iBitsetArrayBlank)
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUNNING_BACK_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
					SET_BIT(iLocalBoolCheck28, LBOOL28_DELIVERED_THIS_RULE)
				ENDIF
				iBroadcastPriority = iRule
				iBroadcastRevalidate = MC_serverBD_4.iTimesRevalidatedObjectives[iTeam]
				increment_medal_objective_completed(iTeam, iRule)
				REINIT_NET_TIMER(tdeventsafetytimer)
				
				IF iRule < MC_serverBD.iMaxObjectives[ iTeam ]
				OR MC_ServerBD.iNumVehHighestPriority[ iTeam ] > 1
					IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
					AND ( (iRule >= FMMC_MAX_RULES) // This line is just a check so that the line below doesn't array overrun
					OR NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iRule], ciBS_RULE2_DONT_GIVE_PLAYER_XP) )
						IF iPackageDelXPCount < 10
							fDelXp = (100*MC_playerBD[iPartToUse].iVehCarryCount*g_sMPTunables.fxp_tunable_Deliver_a_package_bonus)
							iDelXp = ROUND(fDelXp)
							GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, LocalPlayerPed,"XPT_MEDIUMT", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_MISSION_DELIVERED_VEHICLE, iDelXp,1)
							#IF IS_DEBUG_BUILD
								iDebugXPDelVeh= iDebugXPDelVeh + iDelXp
							#ENDIF
							iPackageDelXPCount = iPackageDelXPCount + MC_playerBD[iPartToUse].iVehCarryCount
						ENDIF
						IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
							PLAY_SOUND_FRONTEND(-1, "ROUND_ENDING_STINGER_CUSTOM","CELEBRATION_SOUNDSET", FALSE)
						ENDIF
					ENDIF
				ELSE
					IF iPackageDelXPCount < 10
						iDelayedDeliveryXPToAward++
					ENDIF
					// Tell everyone not to take this vehicle back to freemode, since it's been delivered. Wouldn't make sense.
					BROADCAST_SCRIPT_EVENT_BLOCK_SAVE_EOM_CAR_BEEN_DELIVERED(MC_serverBD_1.sFMMC_SBD.niVehicle[MC_playerBD[iPartToUse].iVehDeliveryId])

				ENDIF
				
				SET_MISSION_TEST_COMPLETE()
				
				BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_DELIVER_VEH, iscoremulti, iTeam ,-1,NULL,ci_TARGET_VEHICLE,MC_playerBD[iPartToUse].iVehDeliveryId)
				//INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE() url:bugstar:3219654
				MC_playerBD[iPartToUse].iVehDelCount++
				MC_playerBD[iPartToUse].iVehCarryCount = 0
				CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER)
				CLEAR_BIT(iLocalBoolCheck,LBOOL_WITH_CARRIER_TEMP)
				CLEAR_BIT(iLocalBoolCheck2, LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
				CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
				DeliveryVeh = NULL
				RESET_NET_TIMER(tdDeliverTimer)
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - JOB_COMP_DELIVER_VEH set locally by part: ",iPartToUse)
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF MC_playerBD[iPartToUse].iCurrentLoc != iOldLocate
			CLEAR_BIT(iLocalBoolCheck6,LBOOL6_IN_A_LOCATE)
			iOldLocate = MC_playerBD[iPartToUse].iCurrentLoc
		ENDIF
		
		IF MC_playerBD[iPartToUse].iCurrentLoc != -1
			IF MC_serverBD_4.iGotoLocationDataRule[MC_playerBD[iPartToUse].iCurrentLoc][ iTeam ] = FMMC_OBJECTIVE_LOGIC_GO_TO
				IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[MC_playerBD[iPartToUse].iCurrentLoc].iLocBS2 , ciLoc_BS2_DisplayMarkerAsVisualOnly)
					IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[MC_playerBD[iPartToUse].iCurrentLoc].iWholeTeamAtLocation[ iTeam ] = ciGOTO_LOCATION_INDIVIDUAL
					
						SET_BIT(iLocalBoolCheck6,LBOOL6_IN_A_LOCATE)
						
						IF(iLocHit = - 1) //If we aren't currently handling a 'stop after hitting locate'
							IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_GOTO_LOC_POSTFX)
								CLEAR_BIT(iLocalBoolCheck10, LBOOL10_GOTO_LOC_POSTFX)
								PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED Cleared LBOOL10_GOTO_LOC_POSTFX - 5")
							ENDIF
							//Kill chase hint cam, Bug 1921219
							IF (iNearestTarget = MC_playerBD[iPartToUse].iCurrentLoc)
							AND (iNearestTargetType = ci_TARGET_LOCATION)
								SET_BIT(iLocalBoolCheck5, LBOOL5_OBJCOMP_KILLCHASEHINTCAM_1)
							ENDIF
							
							REGISTER_AMBIENT_VEHICLE_AS_MISSION_ENTITY(iTeam, iRule)
							
							// only set iLochit if we are in the correct direction. Or if we're not using ciLoc_BS2_DirectionRestrictPass, then set it anyway so that the fail procedures can take place.
							IF DOES_THIS_LOCATION_HAVE_A_SPECIFIC_ENTRY_HEADING( MC_playerBD[iPartToUse].iCurrentLoc )
							AND IS_PARTICIPANT_FACING_CORRECT_DIRECTION_FOR_LOCATION( MC_playerBD[iPartToUse].iCurrentLoc, iPartToUse )
							OR NOT IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ MC_playerBD[iPartToUse].iCurrentLoc ].iLocBS2, ciLoc_BS2_DirectionRestrictPass )
								IF(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[MC_playerBD[iPartToUse].iCurrentLoc].iLocBS,ciLoc_BS_StopVehicle))
									
									iLocHit = MC_playerBD[iPartToUse].iCurrentLoc //It'll now do the bits below under IF(iLocHit != -1)
									
									PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - Hit a stop vehicle locate, setting iLocHit = ",iLocHit)
								ELSE //Normal locate
									
									IF IS_LOCATION_READY_FOR_PROCESSING(MC_playerBD[iPartToUse].iCurrentLoc)
									AND IS_GO_TO_LOC_READY_TO_SEND_OUT_COMPLETION_EVENT(iTeam, iRule)
									
										IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLocateSwapTeam[iRule] > -1
											IF NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_LOCATE_TEAM_SWAP_REQUESTED)
												PRINTLN("[JS][LOCSWAP] - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - In locate, requesting swap")
												SET_BIT(iLocalBoolCheck23, LBOOL23_LOCATE_TEAM_SWAP_REQUESTED)
											ENDIF
										ENDIF
										
										IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ALL_TEAMS_SHARE_LOCATES)
										AND IS_PED_IN_LOCATION(LocalPlayerPed, MC_playerBD[iPartToUse].iCurrentLoc, iTeam)
										OR NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ALL_TEAMS_SHARE_LOCATES)
											IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[MC_playerBD[iLocalPart].iCurrentLoc].iLocBS3, ciLoc_BS3_IncrementTimeRemaining)
												BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_ARRIVE_LOC, 0, iTeam ,-1,NULL,ci_TARGET_LOCATION,MC_playerBD[iPartToUse].iCurrentLoc,FALSE)
											ENDIF
										ENDIF
										
										MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_ARRIVE_LOC
										BROADCAST_FMMC_OBJECTIVE_COMPLETE(MC_playerBD[iPartToUse].iObjectiveTypeCompleted,iPartToUse,MC_playerBD[iPartToUse].iCurrentLoc, iRule, MC_serverBD_4.iTimesRevalidatedObjectives[iTeam], iBitsetArrayBlank)
										iBroadcastPriority = iRule
										iBroadcastRevalidate = MC_serverBD_4.iTimesRevalidatedObjectives[iTeam]
										increment_medal_objective_completed(iTeam, iRule)
										REINIT_NET_TIMER(tdeventsafetytimer)
										iLastGoToLocateHit = MC_playerBD[iPartToUse].iCurrentLoc
										CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
										
										IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciLBD_SHOW_SLIPSTREAM_TIME)
											BROADCAST_FMMC_TEAM_CHECKPOINT_PASSED(iTeam, iPartToUse, MC_playerBD[iPartToUse].iCurrentLoc, MC_ServerBD_4.iTeamLapsCompleted[iTeam])
										ENDIF
										
										//INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE()
										
										IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFIve, ciBEAST_SFX)
										AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitSetFive[ iRule ], ciBS_RULE5_ENABLE_BEAST_MODE)
											STRING sSoundSet = GET_BEAST_MODE_SOUNDSET()
											PLAY_SOUND_FRONTEND(-1, "Beast_Checkpoint", sSoundSet, FALSE)
										ELSE
											IF IS_DROPOFF_AN_AERIAL_FLAG( MC_playerBD[iPartToUse].iCurrentLoc )
												PLAY_SOUND_FRONTEND(-1, "Player_Collect","DLC_PILOT_MP_HUD_SOUNDS", FALSE)
												START_AUDIO_SCENE( "DLC_PILOT_MP_COLLECT_FLAG_SCENE" )
											ELSE 
												IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TINY_RACERS_SOUNDS)
													PRINTLN("[RCC MISSION][TINY RACERS] Playing Checkpoint_Collect")
													PLAY_SOUND_FROM_COORD(-1,"Checkpoint_Collect", GET_LOCATION_VECTOR(MC_playerBD[iPartToUse].iCurrentLoc), "DLC_SR_TR_General_Sounds", TRUE)
												ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iHitSound[iRule] = GOTO_HIT_SOUND_DEFAULT
													IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TOUR_DE_FORCE (g_FMMC_STRUCT.iAdversaryModeType)
														PLAY_SOUND_FRONTEND(-1, "CHECKPOINT_PERFECT","HUD_MINI_GAME_SOUNDSET", FALSE)
													ENDIF
												ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iHitSound[iRule] = GOTO_HIT_SOUND_ALT
													PLAY_SOUND_FRONTEND(-1, "CHECKPOINT_NORMAL", "HUD_MINI_GAME_SOUNDSET", FALSE)
												ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iHitSound[iRule] = GOTO_HIT_SOUND_BOMBDISARM
													IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
														PLAY_SOUND_FROM_ENTITY(-1, "Bomb_Disarmed", GET_VEHICLE_PED_IS_IN( LocalPlayerPed ), "GTAO_Speed_Convoy_Soundset", TRUE, 0 )
													ELSE
														PLAY_SOUND_FRONTEND(-1,"Bomb_Disarmed", "GTAO_Speed_Convoy_Soundset", FALSE)
													ENDIF
												ENDIF	
											ENDIF
										ENDIF
										IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_EnableGameMastersMode)
											BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_ModeSpecific, g_iAA_PlaySound_GameMasters_CheckpointCollected)
											IF g_sArena_Telemetry_data.m_checkpointsCollected = -1
												g_sArena_Telemetry_data.m_checkpointsCollected = 1
											ELSE
												g_sArena_Telemetry_data.m_checkpointsCollected++
											ENDIF
											PRINTLN("[ARENA][TEL] - Game Masters Checkpoints: ", g_sArena_Telemetry_data.m_checkpointsCollected)
										ENDIF
										RESET_NET_TIMER(tdLocalLocateDelayTimer)
										START_NET_TIMER(tdLocalLocateDelayTimer)
										IF HAS_NET_TIMER_STARTED(tdOvertimeLocateDelayTimer)
											RESET_NET_TIMER(tdOvertimeLocateDelayTimer)
										ENDIF
										PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - MC_playerBD[iPartToUse].iCurrentLoc: ",MC_playerBD[iPartToUse].iCurrentLoc)
										PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - JOB_COMP_ARRIVE_LOC set locally by part: ",iPartToUse) 
										RETURN TRUE
									ELIF IS_THIS_CURRENTLY_OVERTIME_TURNS()
									AND NOT HAS_NET_TIMER_STARTED(tdOvertimeLocateDelayTimer)
										REINIT_NET_TIMER(tdOvertimeLocateDelayTimer)
										PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - starting overtime timer")
										IF iCurrentOTZoneScoreToGive > -1								
										AND NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_OVERTIME_SCORE_INCREMENTED_THIS_RULE)
											INCREMENT_LOCAL_PLAYER_SCORE_FLAT_AMOUNT(iCurrentOTZoneScoreToGive)
											BROADCAST_FMMC_OVERTIME_SCORED(iTeam, iRule, iCurrentOTZoneScoreToGive)
											SET_BIT(iLocalBoolCheck23, LBOOL23_OVERTIME_SCORE_INCREMENTED_THIS_RULE)
										ENDIF
									ENDIF
								ENDIF
							
							ENDIF
							
						#IF IS_DEBUG_BUILD
						ELSE
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - Can't complete current loc ",MC_playerBD[iPartToUse].iCurrentLoc,", as we're running stop car with iLocHit = ",iLocHit)
						#ENDIF
						ENDIF							
						
					ELSE // Not an individual at locate:
						
						//One off check
						IF NOT IS_BIT_SET(iLocalBoolCheck6,LBOOL6_IN_A_LOCATE)

							INT iTeamCheckBS
							INT iPlayersWantedInLoc
							
							IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[MC_playerBD[iPartToUse].iCurrentLoc].iWholeTeamAtLocation[ iTeam ] = ciGOTO_LOCATION_WHOLE_TEAM
								SET_BIT(iTeamCheckBS, iTeam)
								iPlayersWantedInLoc = MC_serverBD.iNumberOfPlayingPlayers[iTeam]
							ELIF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[MC_playerBD[iPartToUse].iCurrentLoc].iWholeTeamAtLocation[ iTeam ] = ciGOTO_LOCATION_ALL_PLAYERS
								SET_BIT(iTeamCheckBS, 0)
								SET_BIT(iTeamCheckBS, 1)
								SET_BIT(iTeamCheckBS, 2)
								SET_BIT(iTeamCheckBS, 3)
								iPlayersWantedInLoc = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]
							ELIF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[MC_playerBD[iPartToUse].iCurrentLoc].iWholeTeamAtLocation[ iTeam ] = ciGOTO_LOCATION_CUSTOM_TEAMS
								
								INT iTeamLoop
								
								FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
									IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[MC_playerBD[iPartToUse].iCurrentLoc].iLocBS,ciLoc_BS_CustomTeams_T0_Needs_T0 + (iTeam * 4) + iTeamLoop)
										SET_BIT(iTeamCheckBS, iTeamLoop)
										iPlayersWantedInLoc += MC_serverBD.iNumberOfPlayingPlayers[iTeamLoop]
									ENDIF
								ENDFOR
								
							ELSE //Custom no. players:
								iTeamCheckBS = -1
								iPlayersWantedInLoc = 2 + (g_FMMC_STRUCT_ENTITIES.sGotoLocationData[MC_playerBD[iPartToUse].iCurrentLoc].iWholeTeamAtLocation[ iTeam ] - ciGOTO_LOCATION_2_PLAYERS)
							ENDIF
							
							BOOL bComplete
							INT iLoc = MC_playerBD[iPartToUse].iCurrentLoc
							
							IF iLoc != - 1
								IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2,ciLoc_BS2_CrossLineDisplayVFX ) 
									IF NOT ANIMPOSTFX_IS_RUNNING("CrossLine")
										PRINTLN("[MMacK][LocVFX] Playing VFX")
										PRINTLN("[CTLSFX] Playing Local")
										ANIMPOSTFX_PLAY("CrossLine", 0, TRUE)
										//WESTHERE
										IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2,ciLoc_BS2_RunningBackMarker ) 
											BROADCAST_FMMC_PLAYER_CROSSED_LINE(iTeam)
											IF g_bTriggerCrossTheLineAudio = FALSE
												PLAY_SOUND_FRONTEND(-1, "Whistle", "DLC_TG_Running_Back_Sounds" , FALSE)
												PLAY_SOUND_FRONTEND(-1, "Cheers", "DLC_TG_Running_Back_Sounds" , FALSE)
												g_bTriggerCrossTheLineAudio = TRUE
											ELSE
												PRINTLN("[RCC MISSION] - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE() - We already triggered the cross the line audio")
											ENDIF
										ENDIF
										PLAY_SOUND_FRONTEND(-1,"Player_Enter_Line","GTAO_FM_Cross_The_Line_Soundset", FALSE)
									ENDIF
								ENDIF
							ENDIF
							
							#IF IS_DEBUG_BUILD
							INT iPlayersInLoc // Defined here in debug so we can keep it for prints later
							#ENDIF
							
							//Don't bother looping if we don't care what locate everyone is in
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_ALL_PLAYERS_IN_ANY_LOCATE)
								IF iPlayersWantedInLoc > 0
									
									#IF NOT IS_DEBUG_BUILD
									INT iPlayersInLoc
									#ENDIF
									
									INT iPartLoop, iPlayersChecked, iRespawnSpectators
									
									FOR iPartLoop = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
										IF iPartLoop != iPartToUse
											IF ((NOT IS_BIT_SET(MC_playerBD[iPartLoop].iClientBitSet,PBBOOL_ANY_SPECTATOR)) OR IS_BIT_SET(MC_playerBD[iPartLoop].iClientBitset, PBBOOL_HEIST_SPECTATOR))
											AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartLoop))
											AND (IS_NET_PLAYER_OK(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartLoop))) OR IS_BIT_SET(MC_playerBD[iPartLoop].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN))
											AND ( (iTeamCheckBS = -1) OR IS_BIT_SET(iTeamCheckBS, MC_PlayerBD[iPartLoop].iteam) )
												iPlayersChecked++
												PRINTLN("[JS][ALLTEAMLOC] checking part ", iPartLoop)
												IF IS_BIT_SET(MC_playerBD[iPartLoop].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
													iRespawnSpectators++
													PRINTLN("[JS][ALLTEAMLOC] part ", iPartLoop," is in spectater. iPlayersChecked ", iPlayersChecked, " iPlayersWantedInLoc ", iPlayersWantedInLoc, " iPlayersInLoc ", iPlayersInLoc, " iRespawnSpectators ", iRespawnSpectators)
												ELSE
													IF MC_playerBD[iPartLoop].iCurrentLoc = iLoc
														iPlayersInLoc++
														PRINTLN("[JS][ALLTEAMLOC] part ", iPartLoop," iCurrentLoc = iLoc. iPlayersChecked ", iPlayersChecked, " iPlayersWantedInLoc ", iPlayersWantedInLoc, " iPlayersInLoc ", iPlayersInLoc)
													ELIF MC_playerBD[iPartLoop].iCurrentLoc = -1
														PED_INDEX PartPed = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartLoop)))
														IF IS_PED_IN_LOCATION(PartPed, iLoc, MC_playerBD[iPartLoop].iteam)
															iPlayersInLoc++
															PRINTLN("[JS][ALLTEAMLOC] part ", iPartLoop," is in loc. iPlayersChecked ", iPlayersChecked, " iPlayersWantedInLoc ", iPlayersWantedInLoc, " iPlayersInLoc ", iPlayersInLoc)
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ELSE
											iPlayersChecked++
											iPlayersInLoc++
											PRINTLN("[JS][ALLTEAMLOC] checking myself, iPlayersChecked ", iPlayersChecked, " iPlayersWantedInLoc ", iPlayersWantedInLoc, " iPlayersInLoc ", iPlayersInLoc)
										ENDIF
										
										IF iTeamCheckBS != -1
										AND iPlayersChecked >= iPlayersWantedInLoc
											//We've checked enough people, break out!
											PRINTLN("[JS][ALLTEAMLOC] Breaking out of loop, iPlayersChecked ", iPlayersChecked, " iPlayersWantedInLoc ", iPlayersWantedInLoc, " iPlayersInLoc ", iPlayersInLoc, " iRespawnSpectators ", iRespawnSpectators)
											BREAKLOOP
										ENDIF
									ENDFOR
									
									IF iPlayersInLoc >= (iPlayersWantedInLoc - iRespawnSpectators)
										bComplete = TRUE
									ENDIF
									
								ELSE
									bComplete = TRUE
								ENDIF
							#IF IS_DEBUG_BUILD
							ELSE
								PRINTLN("[JS][ALLTEAMLOC] - I'm in a locate but ciBS_RULE9_ALL_PLAYERS_IN_ANY_LOCATE is set")
							#ENDIF
							ENDIF
							
							IF (g_FMMC_STRUCT_ENTITIES.sGotoLocationData[MC_playerBD[iPartToUse].iCurrentLoc].iWholeTeamAtLocation[ iTeam ] = ciGOTO_LOCATION_WHOLE_TEAM
							AND g_FMMC_STRUCT_ENTITIES.sGotoLocationData[MC_playerBD[iPartToUse].iCurrentLoc].fSecondaryRadius = 0
							AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[iRule], ciBS_RULE8_RESPAWN_SPECTATORS))
							OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_ALL_PLAYERS_IN_ANY_LOCATE)
								SET_BIT(iLocalBoolCheck6,LBOOL6_IN_A_LOCATE)
							ENDIF
							
							IF bComplete
							AND AM_I_IN_SECONDARY_LOCATE_IF_I_NEED_TO_BE(MC_playerBD[iPartToUse].iCurrentLoc)
							AND IS_LOCATION_READY_FOR_PROCESSING(iLoc)
							
								SET_BIT(iLocalBoolCheck6,LBOOL6_IN_A_LOCATE)
								
								REGISTER_AMBIENT_VEHICLE_AS_MISSION_ENTITY(iTeam, iRule)
								
								IF(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS,ciLoc_BS_StopVehicle))
									iLocHit = iLoc //It'll now do the bits below under IF(iLocHit != -1)
									PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - Hit a stop vehicle locate with WholeTeamAtLocation ",g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iWholeTeamAtLocation[iTeam],", setting iLocHit = ",iLocHit)
								ELSE //Normal locate
									
									IF IS_GO_TO_LOC_READY_TO_SEND_OUT_COMPLETION_EVENT(iTeam, iRule)
									
										
										IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLocateSwapTeam[iRule] > -1
											IF NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_LOCATE_TEAM_SWAP_REQUESTED)
												PRINTLN("[JS][LOCSWAP] - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE 2 - In locate, requesting swap")
												SET_BIT(iLocalBoolCheck23, LBOOL23_LOCATE_TEAM_SWAP_REQUESTED)
											ENDIF
										ENDIF
										
										IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iWholeTeamAtLocation[iTeam] = ciGOTO_LOCATION_WHOLE_TEAM
											BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM_ARV_LOC,0,iTeam,iLoc,NULL,ci_TARGET_LOCATION,iLoc)
										ENDIF
										
										IF IS_DROPOFF_AN_AERIAL_FLAG( MC_playerBD[iPartToUse].iCurrentLoc )
											PLAY_SOUND_FRONTEND(-1, "Player_Collect","DLC_PILOT_MP_HUD_SOUNDS", FALSE)
											START_AUDIO_SCENE( "DLC_PILOT_MP_COLLECT_FLAG_SCENE" )
										ELSE
											IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TINY_RACERS_SOUNDS)
												PRINTLN("[RCC MISSION][TINY RACERS] Playing Checkpoint_Collect")
												PLAY_SOUND_FROM_COORD(-1,"Checkpoint_Collect", GET_LOCATION_VECTOR(iloc), "DLC_SR_TR_General_Sounds", TRUE)
											ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iHitSound[iRule] = GOTO_HIT_SOUND_DEFAULT
												PLAY_SOUND_FRONTEND(-1, "CHECKPOINT_PERFECT","HUD_MINI_GAME_SOUNDSET", FALSE)
											ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iHitSound[iRule] = GOTO_HIT_SOUND_ALT
												PLAY_SOUND_FRONTEND(-1, "CHECKPOINT_NORMAL", "HUD_MINI_GAME_SOUNDSET", FALSE)
											ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iHitSound[iRule] = GOTO_HIT_SOUND_BOMBDISARM
												IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
													PLAY_SOUND_FROM_ENTITY(-1, "Bomb_Disarmed", GET_VEHICLE_PED_IS_IN( LocalPlayerPed ), "GTAO_Speed_Convoy_Soundset", TRUE, 0 )
												ELSE
													PLAY_SOUND_FRONTEND(-1,"Bomb_Disarmed", "GTAO_Speed_Convoy_Soundset", FALSE)
												ENDIF
											ENDIF	
										ENDIF
											
										MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_ARRIVE_LOC
										BROADCAST_FMMC_OBJECTIVE_COMPLETE(MC_playerBD[iPartToUse].iObjectiveTypeCompleted, iPartToUse, iLoc, iRule, MC_serverBD_4.iTimesRevalidatedObjectives[iTeam], iBitsetArrayBlank)
										iBroadcastPriority = iRule
										iBroadcastRevalidate = MC_serverBD_4.iTimesRevalidatedObjectives[iTeam]
										increment_medal_objective_completed(iTeam, iRule)
										REINIT_NET_TIMER(tdeventsafetytimer)
										
										IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciLBD_SHOW_SLIPSTREAM_TIME)
											BROADCAST_FMMC_TEAM_CHECKPOINT_PASSED(iTeam, iPartToUse, MC_playerBD[iPartToUse].iCurrentLoc, MC_ServerBD_4.iTeamLapsCompleted[iTeam])
										ENDIF
										IF HAS_NET_TIMER_STARTED(tdOvertimeLocateDelayTimer)
											RESET_NET_TIMER(tdOvertimeLocateDelayTimer)
										ENDIF
										IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_EnableGameMastersMode)
											BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_ModeSpecific, g_iAA_PlaySound_GameMasters_CheckpointCollected)
										ENDIF
										MC_playerBD_1[iLocalPart].iCheckpointsCollected++
										CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
										PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - MC_playerBD[iPartToUse].iCurrentLoc: ",iLoc)
										PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - JOB_COMP_ARRIVE_LOC with WholeTeamAtLocation ",g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iWholeTeamAtLocation[iTeam]," set locally by part: ",iPartToUse)										
										RETURN TRUE
										
									ELSE
										CLEAR_BIT(iLocalBoolCheck6,LBOOL6_IN_A_LOCATE) // Otherwise we won't keep calling this
									ENDIF
								ENDIF
							ELSE
								PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - not ready for loc ",iLoc," w wholeteamatlocation ",g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iWholeTeamAtLocation[iTeam],", iPlayersInLoc = ",iPlayersInLoc," iPlayersWantedInLoc = ",iPlayersWantedInLoc)
								
								IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLocateSwapTeam[iRule] > -1
									IF NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_LOCATE_TEAM_SWAP_REQUESTED)
										PRINTLN("[JS][LOCSWAP] - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE 3 - In locate, requesting swap")
										SET_BIT(iLocalBoolCheck23, LBOOL23_LOCATE_TEAM_SWAP_REQUESTED)
									ENDIF
								ENDIF
							ENDIF
						ELSE
							INT iLoc = MC_playerBD[iPartToUse].iCurrentLoc
							
							IF iLoc != -1
								IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2,ciLoc_BS2_CrossLineDisplayVFX ) 
									IF NOT ANIMPOSTFX_IS_RUNNING("CrossLine")
										PRINTLN("[MMacK][LocVFX] Playing VFX Fallback")
										PRINTLN("[CTLSFX] Playing Local")
										ANIMPOSTFX_PLAY("CrossLine", 0, TRUE)
										//WESTHERE
										IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2,ciLoc_BS2_RunningBackMarker ) 
											BROADCAST_FMMC_PLAYER_CROSSED_LINE(iTeam)
											IF g_bTriggerCrossTheLineAudio = FALSE
												PLAY_SOUND_FRONTEND(-1, "Whistle", "DLC_TG_Running_Back_Sounds" , FALSE)
												PLAY_SOUND_FRONTEND(-1, "Cheers", "DLC_TG_Running_Back_Sounds" , FALSE)
												g_bTriggerCrossTheLineAudio = TRUE
											ELSE
												PRINTLN("[RCC MISSION] - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE() - We already triggered the cross the line audio")
											ENDIF
										ENDIF
										
										PLAY_SOUND_FRONTEND(-1,"Player_Enter_Line","GTAO_FM_Cross_The_Line_Soundset", FALSE)
									ENDIF
								ENDIF
							ENDIF
							#IF IS_DEBUG_BUILD
								PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - Unable to complete multi-player needed at locate iCurrentLoc ",MC_playerBD[iPartToUse].iCurrentLoc," as LBOOL6_IN_A_LOCATE is already set")
							#ENDIF
						ENDIF
						
					ENDIF
				#IF IS_DEBUG_BUILD
				ELIF bGotoLocPrints
					PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - iCurrentLoc = ",MC_playerBD[iPartToUse].iCurrentLoc,", but it's not a go to - rule type = ",MC_serverBD_4.iGotoLocationDataRule[MC_playerBD[iPartToUse].iCurrentLoc][ iTeam ])
				#ENDIF
				ENDIF
			ENDIF
		ELSE
			IF ANIMPOSTFX_IS_RUNNING("CrossLine")
			AND iRule < FMMC_MAX_RULES
			AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitSetSix[ iRule ], ciBS_RULE6_GET_DELIVER_RED_FILTER)
			AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_ARENA_CTF(g_FMMC_STRUCT.iAdversaryModeType)
				PRINTLN("[MMacK][LocVFX] Uncrossed the Line")
				ANIMPOSTFX_STOP("CrossLine")
				ANIMPOSTFX_PLAY("CrossLineOut", 0, FALSE)
				PRINTLN("[CTLSFX] Stopping Local")
				IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_VFX_CAPTURE_OWNER)
					STRING sSoundSet 
					sSoundSet = "DLC_Apartments_Drop_Zone_Sounds"
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_HOSTILE_TAKEOVER_SOUNDS)
						sSoundSet = "dlc_xm_hota_Sounds"
					ENDIF
					PLAY_SOUND_FRONTEND(-1,"Exit_Capture_Zone",sSoundSet, FALSE)
				ELSE	
					PLAY_SOUND_FRONTEND(-1,"Player_Exit_Line","GTAO_FM_Cross_The_Line_Soundset", FALSE)
				ENDIF
				
				CLEAR_BIT(iLocalBoolCheck27, LBOOL27_RED_FILTER_SOUND_PLAYED)
				
				INT iLoc = MC_playerBD[iPartToUse].iCurrentLoc
				
				//WESTHERE
				IF iLoc != - 1
					//WESTHERE
					IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2,ciLoc_BS2_RunningBackMarker ) 
						BROADCAST_FMMC_PLAYER_CROSSED_LINE(iTeam)
						IF g_bTriggerCrossTheLineAudio = FALSE
							PLAY_SOUND_FRONTEND(-1, "Whistle", "DLC_TG_Running_Back_Sounds" , FALSE)
							PLAY_SOUND_FRONTEND(-1, "Cheers", "DLC_TG_Running_Back_Sounds" , FALSE)
							g_bTriggerCrossTheLineAudio = TRUE
						ELSE
							PRINTLN("[RCC MISSION] - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE() - We already triggered the cross the line audio")
						ENDIF
					ENDIF
				ELSE
					IF g_bIsRunningBackMission = TRUE
						PRINTLN("[RCC MISSION]HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE iLoc = - 1")
						BROADCAST_FMMC_PLAYER_CROSSED_LINE(iTeam)
						IF g_bTriggerCrossTheLineAudio = FALSE
							PLAY_SOUND_FRONTEND(-1, "Whistle", "DLC_TG_Running_Back_Sounds" , FALSE)
							PLAY_SOUND_FRONTEND(-1, "Cheers", "DLC_TG_Running_Back_Sounds" , FALSE)
							g_bTriggerCrossTheLineAudio = TRUE
						ELSE
							PRINTLN("[RCC MISSION] - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE() - We already triggered the cross the line audio")
						ENDIF
					ENDIF
					
				ENDIF
				
			ENDIF
		ENDIF
		
		
		IF iLocHit != -1 //If we've hit a 'stop car' locate
		// OR  MC_playerBD[iPartToUse].iCurrentLoc and option.
			fStopDist = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocHit].fStoppingDist
			IF fStopDist <= 0.0
				fStopDist = DEFAULT_VEH_STOPPING_DISTANCE
			ELIF fStopDist > 20.0
				fStopDist = 20.0
			ENDIF
			
			BOOL bSkipStopping = FALSE
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocHit].iLocBS3, ciLoc_BS3_PassObjectiveBeforeStopping)
				IF NOT HAS_NET_TIMER_STARTED(stStopLocationContinueTimer)
					REINIT_NET_TIMER(stStopLocationContinueTimer)
				ELSE
					IF HAS_NET_TIMER_EXPIRED(stStopLocationContinueTimer, ciStopLocationContinue_Time)
						bSkipStopping = TRUE
						PRINTLN("HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - Skipping the vehicle stop sequence now for location ", iLocHit, " because the timer has expired")
					ENDIF
				ENDIF
			ENDIF
			
			IF bSkipStopping
			OR IS_MY_VEHICLE_STOPPED(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocHit].iLocBS,ciLoc_BS_StopVehicle), fStopDist)			
			
				RESET_NET_TIMER(stStopLocationContinueTimer)
			
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocHit].iLocBS,ciLoc_BS_PedsLeaveVehInLocation)				
					
					IF FORCE_EVERYONE_FROM_MY_CAR(TRUE, FALSE)
						
						IF IS_GO_TO_LOC_READY_TO_SEND_OUT_COMPLETION_EVENT(iTeam, iRule)
							
							IF IS_BIT_SET(iLocalBoolCheck2, LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
							OR HAS_NET_TIMER_STARTED(tdDeliverBackupTimer)
								IF IS_VEHICLE_DRIVEABLE(DeliveryVeh)
									IF NETWORK_HAS_CONTROL_OF_ENTITY(DeliveryVeh)
										PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - Turning handbrake off (1) time ", GET_CLOUD_TIME_AS_INT())
										SET_VEHICLE_HANDBRAKE(DeliveryVeh,FALSE)
									ELSE
										PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - Can't turn  handbrake off NETWORK_HAS_CONTROL_OF_ENTITY (1) time ", GET_CLOUD_TIME_AS_INT())
									ENDIF
								ENDIF
								
								IF IS_PED_IN_ANY_BOAT(LocalPlayerPed)
								OR IS_PED_IN_ANY_AMPHIBIOUS_VEHICLE(LocalPlayerPed) // url:bugstar:3498788
									VEHICLE_INDEX tempBoat = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
									IF IS_VEHICLE_DRIVEABLE(tempBoat)
										IF GET_PED_IN_VEHICLE_SEAT(tempBoat) = LocalPlayerPed
											IF NETWORK_HAS_CONTROL_OF_ENTITY(tempBoat)
												SET_BOAT_ANCHOR(tempBoat,FALSE)
												SET_BOAT_REMAINS_ANCHORED_WHILE_PLAYER_IS_DRIVER(tempBoat,FALSE)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								NET_SET_PLAYER_CONTROL(LocalPlayer,TRUE)
								RESET_NET_TIMER(tdDeliverBackupTimer)
								CLEAR_BIT(iLocalBoolCheck2, LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
								DeliveryVeh = NULL
							ENDIF
							
							//Do 'objective completed' bits:
							BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_ARRIVE_LOC, iscoremulti, iTeam ,-1,NULL,ci_TARGET_LOCATION,iLocHit,FALSE)
							MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_ARRIVE_LOC
							MC_playerBD[iPartToUse].iCurrentLoc = iLocHit
							BROADCAST_FMMC_OBJECTIVE_COMPLETE(MC_playerBD[iPartToUse].iObjectiveTypeCompleted,iPartToUse,iLocHit, iRule, MC_serverBD_4.iTimesRevalidatedObjectives[iTeam], iBitsetArrayBlank)
							iBroadcastPriority = iRule
							iBroadcastRevalidate = MC_serverBD_4.iTimesRevalidatedObjectives[iTeam]
							increment_medal_objective_completed(iTeam, iRule)
							REINIT_NET_TIMER(tdeventsafetytimer)
							//INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE()
							
							IF IS_DROPOFF_AN_AERIAL_FLAG( MC_playerBD[iPartToUse].iCurrentLoc )
								PLAY_SOUND_FRONTEND(-1, "Player_Collect","DLC_PILOT_MP_HUD_SOUNDS", FALSE)
								START_AUDIO_SCENE( "DLC_PILOT_MP_COLLECT_FLAG_SCENE" )
							ELSE
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TINY_RACERS_SOUNDS)
									PRINTLN("[RCC MISSION][TINY RACERS] Playing Checkpoint_Collect")
									PLAY_SOUND_FROM_COORD(-1,"Checkpoint_Collect", GET_LOCATION_VECTOR(MC_playerBD[iPartToUse].iCurrentLoc), "DLC_SR_TR_General_Sounds", TRUE)
								ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iHitSound[iRule] = GOTO_HIT_SOUND_DEFAULT
									PLAY_SOUND_FRONTEND(-1, "CHECKPOINT_PERFECT","HUD_MINI_GAME_SOUNDSET", FALSE)
								ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iHitSound[iRule] = GOTO_HIT_SOUND_ALT
									PLAY_SOUND_FRONTEND(-1, "CHECKPOINT_NORMAL", "HUD_MINI_GAME_SOUNDSET", FALSE)
								ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iHitSound[iRule] = GOTO_HIT_SOUND_BOMBDISARM
									IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
										PLAY_SOUND_FROM_ENTITY(-1, "Bomb_Disarmed", GET_VEHICLE_PED_IS_IN( LocalPlayerPed ), "GTAO_Speed_Convoy_Soundset", TRUE, 0 )
									ELSE
										PLAY_SOUND_FRONTEND(-1,"Bomb_Disarmed", "GTAO_Speed_Convoy_Soundset", FALSE)
									ENDIF
								ENDIF	
							ENDIF
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - Car stopped after hitting location: ",iLocHit)
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - JOB_COMP_ARRIVE_LOC set locally by part: ",iPartToUse)
							RESET_NET_TIMER(tdLocalLocateDelayTimer)
							START_NET_TIMER(tdLocalLocateDelayTimer)
							IF HAS_NET_TIMER_STARTED(tdOvertimeLocateDelayTimer)
								RESET_NET_TIMER(tdOvertimeLocateDelayTimer)
							ENDIF
							CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
							CLEAR_BIT(iLocalBoolCheck10, LBOOL10_GOTO_LOC_POSTFX)
							iLocHit = -1 //Reset iLocHit, so the code above works again
							RETURN TRUE
							
						ELIF IS_THIS_CURRENTLY_OVERTIME_TURNS()
						AND NOT HAS_NET_TIMER_STARTED(tdOvertimeLocateDelayTimer)
							REINIT_NET_TIMER(tdOvertimeLocateDelayTimer)
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - starting overtime timer")
							IF iCurrentOTZoneScoreToGive > -1		
							AND NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_OVERTIME_SCORE_INCREMENTED_THIS_RULE)
								INCREMENT_LOCAL_PLAYER_SCORE_FLAT_AMOUNT(iCurrentOTZoneScoreToGive)
								BROADCAST_FMMC_OVERTIME_SCORED(iTeam, iRule, iCurrentOTZoneScoreToGive)
								SET_BIT(iLocalBoolCheck23, LBOOL23_OVERTIME_SCORE_INCREMENTED_THIS_RULE)
							ENDIF
						ENDIF
						
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - Forcing everyone from my car after hitting location: ",iLocHit)
					#ENDIF
					ENDIF
				else //goto locate not forcing peds out
					
					IF IS_GO_TO_LOC_READY_TO_SEND_OUT_COMPLETION_EVENT(iTeam, iRule)
						
						iTimeToStop =  g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocHit].iStopTimer
						IF iTimeToStop > 0
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC Will stop vehicle for duration iLocHit: ",  iLocHit, " Time: ", iTimeToStop, " Team: ", MC_playerBD[iPartToUse].iteam, " Rule: ", MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
	
							INT iMissionEntity = IS_ENTITY_A_MISSION_CREATOR_ENTITY(DeliveryVeh)
							IF iMissionEntity > -1
								PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE (2a) Setting iVeh: ", MC_playerBD[iPartToUse].iVehDeliveryId, " BROADCAST_FMMC_DELIVER_VEHICLE_STOP")
								BROADCAST_FMMC_DELIVER_VEHICLE_STOP(MC_playerBD[iPartToUse].iVehDeliveryId, iLocalPart, iTimeToStop)
							ENDIF
							SET_BIT(iLocalBoolCheck24, LBOOL24_BLOCK_VEHICLE_KERS_ON_RULE_CHECK)
							PRINTLN("[RCC MISSION] LBOOL24_BLOCK_VEHICLE_KERS_ON_RULE_CHECK setting")
							
						ELIF iTimeToStop = -1
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC Will stop vehicle forever iLocHit: ",  iLocHit, " Time: ", iTimeToStop, " Team: ", MC_playerBD[iPartToUse].iteam, " Rule: ", MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
							SET_BIT(iLocalBoolCheck10, LBOOL10_STOP_CAR_FOREVER)							
														
							INT iMissionEntity = IS_ENTITY_A_MISSION_CREATOR_ENTITY(DeliveryVeh)
							IF iMissionEntity > -1
								PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE (2b) Setting iVeh: ", MC_playerBD[iPartToUse].iVehDeliveryId, " BROADCAST_FMMC_DELIVER_VEHICLE_STOP")
								BROADCAST_FMMC_DELIVER_VEHICLE_STOP(MC_playerBD[iPartToUse].iVehDeliveryId, iLocalPart, iTimeToStop)
							ENDIF														
							SET_BIT(iLocalBoolCheck24, LBOOL24_BLOCK_VEHICLE_KERS_ON_RULE_CHECK)
							PRINTLN("[RCC MISSION] LBOOL24_BLOCK_VEHICLE_KERS_ON_RULE_CHECK setting")
							
						ENDIF
						
						IF IS_BIT_SET(iLocalBoolCheck2, LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
						OR HAS_NET_TIMER_STARTED(tdDeliverBackupTimer)
							IF IS_VEHICLE_DRIVEABLE(DeliveryVeh)
								IF NETWORK_HAS_CONTROL_OF_ENTITY(DeliveryVeh)
									SET_VEHICLE_HANDBRAKE(DeliveryVeh,FALSE)
									PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - Turning handbrake off (2) time ", GET_CLOUD_TIME_AS_INT())
								ELSE
									PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - Can't turn  handbrake off NETWORK_HAS_CONTROL_OF_ENTITY (2) time ", GET_CLOUD_TIME_AS_INT())
								ENDIF
							ENDIF
							IF IS_PED_IN_ANY_BOAT(LocalPlayerPed)
							OR IS_PED_IN_ANY_AMPHIBIOUS_VEHICLE(LocalPlayerPed) // url:bugstar:3498788
								VEHICLE_INDEX tempBoat = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
								IF IS_VEHICLE_DRIVEABLE(tempBoat)
									IF GET_PED_IN_VEHICLE_SEAT(tempBoat) = LocalPlayerPed
										IF NETWORK_HAS_CONTROL_OF_ENTITY(tempBoat)
											SET_BOAT_ANCHOR(tempBoat,FALSE)
											SET_BOAT_REMAINS_ANCHORED_WHILE_PLAYER_IS_DRIVER(tempBoat,FALSE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							NET_SET_PLAYER_CONTROL(LocalPlayer,TRUE)
							RESET_NET_TIMER(tdDeliverBackupTimer)
							CLEAR_BIT(iLocalBoolCheck2, LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
							DeliveryVeh = NULL
						ENDIF
						
						//Do 'objective completed' bits:
						BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_ARRIVE_LOC, iscoremulti, iTeam ,-1,NULL,ci_TARGET_LOCATION,iLocHit,FALSE)
						MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_ARRIVE_LOC
						MC_playerBD[iPartToUse].iCurrentLoc = iLocHit
						BROADCAST_FMMC_OBJECTIVE_COMPLETE(MC_playerBD[iPartToUse].iObjectiveTypeCompleted,iPartToUse,iLocHit, iRule, MC_serverBD_4.iTimesRevalidatedObjectives[iTeam], iBitsetArrayBlank)
						iBroadcastPriority = iRule
						iBroadcastRevalidate = MC_serverBD_4.iTimesRevalidatedObjectives[iTeam]
						increment_medal_objective_completed(iTeam, iRule)
						REINIT_NET_TIMER(tdeventsafetytimer)						
						//INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(iCurrentOTZoneScoreToGive)
						
						IF IS_DROPOFF_AN_AERIAL_FLAG( MC_playerBD[iPartToUse].iCurrentLoc )
							PLAY_SOUND_FRONTEND(-1, "Player_Collect","DLC_PILOT_MP_HUD_SOUNDS", FALSE)
							START_AUDIO_SCENE( "DLC_PILOT_MP_COLLECT_FLAG_SCENE" )
						ELSE
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TINY_RACERS_SOUNDS)
								PRINTLN("[RCC MISSION][TINY RACERS] Playing Checkpoint_Collect")
								PLAY_SOUND_FROM_COORD(-1,"Checkpoint_Collect", GET_LOCATION_VECTOR(MC_playerBD[iPartToUse].iCurrentLoc), "DLC_SR_TR_General_Sounds", TRUE)
							ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iHitSound[iRule] = GOTO_HIT_SOUND_DEFAULT
								PLAY_SOUND_FRONTEND(-1, "CHECKPOINT_PERFECT","HUD_MINI_GAME_SOUNDSET", FALSE)
							ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iHitSound[iRule] = GOTO_HIT_SOUND_ALT
								PLAY_SOUND_FRONTEND(-1, "CHECKPOINT_NORMAL", "HUD_MINI_GAME_SOUNDSET", FALSE)
							ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iHitSound[iRule] = GOTO_HIT_SOUND_BOMBDISARM
								IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
									PLAY_SOUND_FROM_ENTITY(-1, "Bomb_Disarmed", GET_VEHICLE_PED_IS_IN( LocalPlayerPed ), "GTAO_Speed_Convoy_Soundset", TRUE, 0 )
								ELSE
									PLAY_SOUND_FRONTEND(-1,"Bomb_Disarmed", "GTAO_Speed_Convoy_Soundset", FALSE)
								ENDIF
							ENDIF	
						ENDIF
						PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - Car stopped after hitting location: ",iLocHit)
						PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - JOB_COMP_ARRIVE_LOC set locally by part: ",iPartToUse)
						RESET_NET_TIMER(tdLocalLocateDelayTimer)
						START_NET_TIMER(tdLocalLocateDelayTimer)
						IF HAS_NET_TIMER_STARTED(tdOvertimeLocateDelayTimer)
							RESET_NET_TIMER(tdOvertimeLocateDelayTimer)
						ENDIF
						CLEAR_BIT(iLocalBoolCheck10, LBOOL10_GOTO_LOC_POSTFX)
						iLocHit = -1 //Reset iLocHit, so the code above works again	
						CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
						RETURN TRUE
					ELIF IS_THIS_CURRENTLY_OVERTIME_TURNS()
					AND NOT HAS_NET_TIMER_STARTED(tdOvertimeLocateDelayTimer)
						REINIT_NET_TIMER(tdOvertimeLocateDelayTimer)
						PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - starting overtime timer")
						IF iCurrentOTZoneScoreToGive > -1		
						AND NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_OVERTIME_SCORE_INCREMENTED_THIS_RULE)
							INCREMENT_LOCAL_PLAYER_SCORE_FLAT_AMOUNT(iCurrentOTZoneScoreToGive)
							BROADCAST_FMMC_OVERTIME_SCORED(iTeam, iRule, iCurrentOTZoneScoreToGive)
							SET_BIT(iLocalBoolCheck23, LBOOL23_OVERTIME_SCORE_INCREMENTED_THIS_RULE)
						ENDIF
					ENDIF
					
				endif
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - Stopping car after hitting location: ",iLocHit)
			#ENDIF
			ENDIF
		ENDIF
		
		IF MC_playerBD[iPartToUse].iPedNear != -1
			IF MC_serverBD_4.iPedRule[MC_playerBD[iPartToUse].iPedNear][ iTeam ] = FMMC_OBJECTIVE_LOGIC_GO_TO
				//if playing football, check that the player is alive before sending ticker messages
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_FOOTBALL_HUD)
					IF IS_NET_PLAYER_OK(PLAYER_ID())
						BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_ARRIVE_PED, iscoremulti, iTeam ,-1,NULL,ci_TARGET_PED,MC_playerBD[iPartToUse].iPedNear)
					ENDIF
				ELSE
					BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_ARRIVE_PED, iscoremulti, iTeam ,-1,NULL,ci_TARGET_PED,MC_playerBD[iPartToUse].iPedNear)
				ENDIF
				MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_ARRIVE_PED
				BROADCAST_FMMC_OBJECTIVE_COMPLETE(MC_playerBD[iPartToUse].iObjectiveTypeCompleted,iPartToUse,MC_playerBD[iPartToUse].iPedNear, iRule, MC_serverBD_4.iTimesRevalidatedObjectives[iTeam], iBitsetArrayBlank)
				iBroadcastPriority = iRule
				iBroadcastRevalidate = MC_serverBD_4.iTimesRevalidatedObjectives[iTeam]
				increment_medal_objective_completed(iTeam, iRule)
				REINIT_NET_TIMER(tdeventsafetytimer)
				INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE() 
				CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - JOB_COMP_ARRIVE_PED set locally by part: ",iPartToUse) 
				RETURN TRUE
			ENDIF
		ENDIF
			
		IF MC_playerBD[iPartToUse].iVehNear != -1
			IF MC_serverBD_4.ivehRule[MC_playerBD[iPartToUse].iVehNear][ iTeam ] = FMMC_OBJECTIVE_LOGIC_GO_TO
				#IF IS_DEBUG_BUILD
					IF bInVehicleDebug
						PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH Checking if I've complete my goto objective and I'm on a go to")
					ENDIF
				#ENDIF
				
				BOOL bCheckOnRule // To allow GET_VEHICLE_GOTO_TEAMS to be used here
				IF (GET_VEHICLE_GOTO_TEAMS( iTeam ,MC_playerBD[iPartToUse].iVehNear, bCheckOnRule) = 0)
					#IF IS_DEBUG_BUILD
						IF bInVehicleDebug
							PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH Checking if I've complete my goto objective will return TRUE because GET_VEHICLE_GOTO_TEAMS( iTeam ,MC_playerBD[iPartToUse].iVehNear, bCheckOnRule) = 0")
						ENDIF
					#ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_WAIT_FOR_REMAINING_IN_MOC_TO_PROGRESS)
						IF NOT ARE_REMAINING_PLAYERS_IN_VEHICLE_INTERIOR(IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen,ciENABLE_AVENGER_INTERIOR_ENTRY))
							PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH. RETURNING FALSE. Waiting on remaining players MOC")
							RETURN FALSE
						ENDIF
					ENDIF
				
					BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_ARRIVE_VEH, iscoremulti, iTeam ,-1,NULL,ci_TARGET_VEHICLE,MC_playerBD[iPartToUse].iVehNear)
					MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_ARRIVE_VEH
					BROADCAST_FMMC_OBJECTIVE_COMPLETE(MC_playerBD[iPartToUse].iObjectiveTypeCompleted,iPartToUse,MC_playerBD[iPartToUse].iVehNear, iRule, MC_serverBD_4.iTimesRevalidatedObjectives[iTeam], iBitsetArrayBlank)
					iBroadcastPriority = iRule
					iBroadcastRevalidate = MC_serverBD_4.iTimesRevalidatedObjectives[iTeam]
					iBroadcastVehNear = MC_playerBD[iPartToUse].iVehNear
					increment_medal_objective_completed(iTeam, iRule) 
					REINIT_NET_TIMER(tdeventsafetytimer)
					CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
					//INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE() url:bugstar:3219654
					PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - JOB_COMP_ARRIVE_VEH set locally by part: ",iPartToUse) 
					RETURN TRUE
				ELSE
					#IF IS_DEBUG_BUILD
						IF bInVehicleDebug
							PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH Checking if I've complete my goto objective  else GET_VEHICLE_GOTO_TEAMS( iTeam ,MC_playerBD[iPartToUse].iVehNear, bCheckOnRule) = 0")
						ENDIF
					#ENDIF
					
					tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[MC_playerBD[iPartToUse].iVehNear])
					IF IS_VEHICLE_DRIVEABLE(tempVeh)
						#IF IS_DEBUG_BUILD
							IF bInVehicleDebug
								PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH Checking if I've complete my goto objective IS_VEHICLE_DRIVEABLE(tempVeh)")
							ENDIF
						#ENDIF
					
						BOOL bComplete
						
						IF NOT AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH()
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - JOB_COMP_ARRIVE_VEH All needed are in their cars, set by part: ",iPartToUse)
							bComplete = TRUE
						ELSE
							
							BOOL bRuleSetting = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[MC_playerBD[iPartToUse].iVehNear].iVehBitSet,ciFMMC_VEHICLE_OVERRIDEWRULESETTING_GOTO)
							
							IF ( ((NOT bRuleSetting) AND (NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[MC_playerBD[iPartToUse].iVehNear].iVehBitsetTwo,ciFMMC_VEHICLE2_DISABLE_TEAMINVEH_PASS_ON_FULL_VEH)))
								OR (bRuleSetting AND (NOT IS_BIT_SET(g_FMMC_STRUCT.iVehRuleWaitForTeamsBS[MC_serverBD_4.iVehPriority[MC_playerBD[iPartToUse].iVehNear][ iTeam ]][ iTeam ],ciFMMC_VehRuleTeamBS_DISABLE_TEAMINVEH_PASS_ON_FULL_VEH))) )
							AND IS_VEHICLE_FULL(tempVeh)
								PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - JOB_COMP_ARRIVE_VEH All team needed but vehicle full, set by part: ",iPartToUse)
								bComplete = TRUE
							ENDIF
						ENDIF
						
						IF bComplete
							BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_ARRIVE_VEH, iscoremulti, iTeam ,1,NULL,ci_TARGET_VEHICLE,MC_playerBD[iPartToUse].iVehNear)
							MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_ARRIVE_VEH
							BROADCAST_FMMC_OBJECTIVE_COMPLETE(MC_playerBD[iPartToUse].iObjectiveTypeCompleted,iPartToUse,MC_playerBD[iPartToUse].iVehNear, iRule, MC_serverBD_4.iTimesRevalidatedObjectives[iTeam], iBitsetArrayBlank)
							iBroadcastPriority = iRule
							iBroadcastRevalidate = MC_serverBD_4.iTimesRevalidatedObjectives[iTeam]
							iBroadcastVehNear = MC_playerBD[iPartToUse].iVehNear
							increment_medal_objective_completed(iTeam, iRule)
							REINIT_NET_TIMER(tdeventsafetytimer)
							INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE()
							CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					IF bInVehicleDebug
						PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH Checking if I've complete my goto objective but MC_serverBD_4.ivehRule[MC_playerBD[iPartToUse].iVehNear][ iTeam ] = ", MC_serverBD_4.ivehRule[MC_playerBD[iPartToUse].iVehNear][ iTeam ])
					ENDIF
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				IF bInVehicleDebug
					PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH Checking if I've complete my goto objective but MC_playerBD[iPartToUse].iVehNear = -1")
				ENDIF
			#ENDIF
		ENDIF
		
		// If we have a go-to-object rule 
		IF MC_playerBD[iPartToUse].iObjNear != -1
			PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE OBJ - We are near enough to collect iobj: ", MC_playerBD[iPartToUse].iObjNear)
			
			IF MC_serverBD_4.iObjRule[MC_playerBD[iPartToUse].iObjNear][ iTeam ] = FMMC_OBJECTIVE_LOGIC_GO_TO			
				BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_ARRIVE_OBJ, iscoremulti, iTeam ,-1,NULL,ci_TARGET_OBJECT,MC_playerBD[iPartToUse].iObjNear)
				MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_ARRIVE_OBJ
				BROADCAST_FMMC_OBJECTIVE_COMPLETE(MC_playerBD[iPartToUse].iObjectiveTypeCompleted,iPartToUse,MC_playerBD[iPartToUse].iObjNear, iRule, MC_serverBD_4.iTimesRevalidatedObjectives[iTeam], iBitsetArrayBlank)
				iBroadcastPriority = iRule
				iBroadcastRevalidate = MC_serverBD_4.iTimesRevalidatedObjectives[iTeam]
				increment_medal_objective_completed(iTeam, iRule)
				REINIT_NET_TIMER(tdeventsafetytimer)
				INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE()
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_IN_AND_OUT_SOUNDS)
				AND NOT IS_BIT_SET(iobjCollectedBoolCheck, MC_playerBD[iPartToUse].iObjNear)
					PLAY_SOUND_FRONTEND(-1, "Friend_Pick_Up", "HUD_FRONTEND_MP_COLLECTABLE_SOUNDS", FALSE)
				ENDIF
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE OBJ - JOB_COMP_ARRIVE_OBJ set locally by part: ",iPartToUse)
				SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY(TRUE)
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE OBJ - setting SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY true - 7")
				
				SET_BIT(iobjCollectedBoolCheck, MC_playerBD[iPartToUse].iObjNear)
				
				INT iObj = MC_playerBD[iPartToUse].iObjNear
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetTwo, cibsOBJ2_Only_Pickup_Once)
					MC_serverBD_4.iObjPriority[iObj][iteam] = FMMC_MAX_RULES
					PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - Setting iObjPriority on object ", iObj, " to FMMC_MAX_RULES so that it won't be used again on later rules due to cibsOBJ2_Only_Pickup_Once")
				ENDIF
				CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
				MC_playerBD[iPartToUse].iObjNear = -1				
				RETURN TRUE
			ENDIF
		ENDIF


		IF MC_playerBD[iPartToUse].iLocPhoto != -1
			IF MC_serverBD_4.iGotoLocationDataRule[MC_playerBD[iPartToUse].iLocPhoto][ iTeam ] = FMMC_OBJECTIVE_LOGIC_PHOTO
				BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_PHOTO_LOC, iscoremulti, iTeam ,-1,NULL,ci_TARGET_LOCATION,MC_playerBD[iPartToUse].iLocPhoto)
				MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_PHOTO_LOC
				BROADCAST_FMMC_OBJECTIVE_COMPLETE(MC_playerBD[iPartToUse].iObjectiveTypeCompleted,iPartToUse,MC_playerBD[iPartToUse].iLocPhoto, iRule, MC_serverBD_4.iTimesRevalidatedObjectives[iTeam], iBitsetArrayBlank)
				iBroadcastPriority = iRule
				iBroadcastRevalidate = MC_serverBD_4.iTimesRevalidatedObjectives[iTeam]
				increment_medal_objective_completed(iTeam, iRule)
				REINIT_NET_TIMER(tdeventsafetytimer)
				INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE()
				CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
				PLAY_SOUND_FRONTEND(-1, "CHECKPOINT_PERFECT","HUD_MINI_GAME_SOUNDSET", FALSE)
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC PHOTO - MC_playerBD[iPartToUse].iLocPhoto: ",MC_playerBD[iPartToUse].iLocPhoto)
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC PHOTO - JOB_COMP_PHOTO_LOC set locally by part: ",iPartToUse)
				RETURN TRUE
			ELSE
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC PHOTO - iLocPhoto is set to ",MC_playerBD[iPartToUse].iLocPhoto," but we aren't on a photo rule so clear it - for my part ",iPartToUse) 
				MC_playerBD[iPartToUse].iLocPhoto = -1
			ENDIF
		ENDIF
		
		IF MC_playerBD[iPartToUse].iPedCharmed != -1
			IF MC_serverBD_4.iPedRule[MC_playerBD[iPartToUse].iPedCharmed][ iTeam ] = FMMC_OBJECTIVE_LOGIC_CHARM
				BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_CHARM_PED, iscoremulti, iTeam ,-1,NULL,ci_TARGET_PED,MC_playerBD[iPartToUse].iPedCharmed)
				MC_playerBD[iPartToUse].iPedCharming = -1
				MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_CHARM_PED
				BROADCAST_FMMC_OBJECTIVE_COMPLETE(MC_playerBD[iPartToUse].iObjectiveTypeCompleted,iPartToUse,MC_playerBD[iPartToUse].iPedCharmed, iRule, MC_serverBD_4.iTimesRevalidatedObjectives[iTeam], iBitsetArrayBlank)
				iBroadcastPriority = iRule
				iBroadcastRevalidate = MC_serverBD_4.iTimesRevalidatedObjectives[iTeam]
				increment_medal_objective_completed(iTeam, iRule)
				REINIT_NET_TIMER(tdeventsafetytimer)
				INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE() 
				CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED CHARM - JOB_COMP_CHARM_PED set locally by part: ",iPartToUse) 
				RETURN TRUE
			ELSE
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED CHARM - iPedCharmed is set to ",MC_playerBD[iPartToUse].iPedCharmed," but we aren't on a charm rule so clear it - for my part ",iPartToUse) 
				MC_playerBD[iPartToUse].iPedCharmed = -1
			ENDIF
		ENDIF
		
		IF MC_playerBD[iPartToUse].iVehPhoto != -1
			IF MC_serverBD_4.ivehRule[MC_playerBD[iPartToUse].iVehPhoto][ iTeam ] = FMMC_OBJECTIVE_LOGIC_PHOTO
				BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_PHOTO_VEH, iscoremulti, iTeam ,-1,NULL,ci_TARGET_VEHICLE,MC_playerBD[iPartToUse].iVehPhoto)
				RESET_PHOTO_DATA()
				MC_playerBD[iPartToUse].iVehNear = -1
				MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_PHOTO_VEH
				BROADCAST_FMMC_OBJECTIVE_COMPLETE(MC_playerBD[iPartToUse].iObjectiveTypeCompleted,iPartToUse,MC_playerBD[iPartToUse].iVehPhoto, iRule, MC_serverBD_4.iTimesRevalidatedObjectives[iTeam], iBitsetArrayBlank)
				iBroadcastPriority = iRule
				iBroadcastRevalidate = MC_serverBD_4.iTimesRevalidatedObjectives[iTeam]
				increment_medal_objective_completed(iTeam, iRule)
				REINIT_NET_TIMER(tdeventsafetytimer)
				PLAY_SOUND_FRONTEND(-1, "CHECKPOINT_PERFECT","HUD_MINI_GAME_SOUNDSET", FALSE)
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH PHOTO - JOB_COMP_PHOTO_VEH set locally by part: ",iPartToUse) 
				RETURN TRUE
			ELSE
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH PHOTO - iVehPhoto is set to ",MC_playerBD[iPartToUse].iVehPhoto," but we aren't on a photo rule so clear it - for my part ",iPartToUse) 
				MC_playerBD[iPartToUse].iVehPhoto = -1
			ENDIF
		ENDIF
		
		IF MC_playerBD[iPartToUse].iPedPhoto != -1
	        IF MC_serverBD_4.iPedRule[MC_playerBD[iPartToUse].iPedPhoto][ iTeam ] = FMMC_OBJECTIVE_LOGIC_PHOTO
                IF IS_BIT_SET(MC_serverBD.iDeadPedPhotoBitset[ iTeam ][GET_LONG_BITSET_INDEX(MC_playerBD[iPartToUse].iPedPhoto)], GET_LONG_BITSET_BIT(MC_playerBD[iPartToUse].iPedPhoto))
                    BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_PHOTO_PED, iscoremulti, iTeam ,0,NULL,ci_TARGET_PED,MC_playerBD[iPartToUse].iPedPhoto)
                ELSE
                    BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_PHOTO_PED, iscoremulti, iTeam ,-1,NULL,ci_TARGET_PED,MC_playerBD[iPartToUse].iPedPhoto)
                ENDIF
                RESET_PHOTO_DATA()
                MC_playerBD[iPartToUse].iPedNear = -1
                MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_PHOTO_PED
                BROADCAST_FMMC_OBJECTIVE_COMPLETE(MC_playerBD[iPartToUse].iObjectiveTypeCompleted,iPartToUse,MC_playerBD[iPartToUse].iPedPhoto, iRule, MC_serverBD_4.iTimesRevalidatedObjectives[iTeam], iBitsetArrayBlank)
               	iBroadcastPriority = iRule
				iBroadcastRevalidate = MC_serverBD_4.iTimesRevalidatedObjectives[iTeam]
				increment_medal_objective_completed(iTeam, iRule)
				REINIT_NET_TIMER(tdeventsafetytimer)
                INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE() 
				CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
				PLAY_SOUND_FRONTEND(-1, "CHECKPOINT_PERFECT","HUD_MINI_GAME_SOUNDSET", FALSE)
                PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED PHOTO - JOB_COMP_PHOTO_PED set locally by part: ",iPartToUse) 
                RETURN TRUE
	        ELSE
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED PHOTO - iPedPhoto is set to ",MC_playerBD[iPartToUse].iPedPhoto," but we aren't on a photo rule so clear it - for my part ",iPartToUse) 
				MC_playerBD[iPartToUse].iPedPhoto = -1
			ENDIF
		ENDIF
		
		IF MC_playerBD[iPartToUse].iObjPhoto != -1
			IF MC_serverBD_4.iObjRule[MC_playerBD[iPartToUse].iObjPhoto][ iTeam ] = FMMC_OBJECTIVE_LOGIC_PHOTO
				BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_PHOTO_OBJ, iscoremulti, iTeam ,-1,NULL,ci_TARGET_OBJECT,MC_playerBD[iPartToUse].iObjPhoto)
				RESET_PHOTO_DATA()
				MC_playerBD[iPartToUse].iObjNear = -1
				MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_PHOTO_OBJ
				BROADCAST_FMMC_OBJECTIVE_COMPLETE(MC_playerBD[iPartToUse].iObjectiveTypeCompleted,iPartToUse,MC_playerBD[iPartToUse].iObjPhoto, iRule, MC_serverBD_4.iTimesRevalidatedObjectives[iTeam], iBitsetArrayBlank)
				iBroadcastPriority = iRule
				iBroadcastRevalidate = MC_serverBD_4.iTimesRevalidatedObjectives[iTeam]
				increment_medal_objective_completed(iTeam, iRule)
				REINIT_NET_TIMER(tdeventsafetytimer)
				INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE()
				CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
				PLAY_SOUND_FRONTEND(-1, "CHECKPOINT_PERFECT","HUD_MINI_GAME_SOUNDSET", FALSE)
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE OBJ PHOTO - JOB_COMP_PHOTO_OBJ set locally by part: ",iPartToUse) 
				RETURN TRUE
			ELSE
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE OBJ PHOTO - iObjPhoto is set to ",MC_playerBD[iPartToUse].iObjPhoto," but we aren't on a photo rule so clear it - for my part ",iPartToUse) 
				MC_playerBD[iPartToUse].iObjPhoto = -1
			ENDIF
		ENDIF
		
		IF MC_playerBD[iPartToUse].iObjHacked != -1
			IF MC_serverBD_4.iObjRule[MC_playerBD[iPartToUse].iObjHacked][ iTeam ] = FMMC_OBJECTIVE_LOGIC_MINIGAME
				INT iObjHacked = MC_playerBD[iPartToUse].iObjHacked
				BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_HACK_OBJ, iscoremulti, iTeam ,MC_serverBD_4.iObjMissionSubLogic[ iTeam ],NULL,ci_TARGET_OBJECT,MC_playerBD[iPartToUse].iObjHacked)
				MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_HACK_OBJ
				BROADCAST_FMMC_OBJECTIVE_COMPLETE(MC_playerBD[iPartToUse].iObjectiveTypeCompleted,iPartToUse,MC_playerBD[iPartToUse].iObjHacked, iRule, MC_serverBD_4.iTimesRevalidatedObjectives[iTeam], iBitsetArrayBlank)
				iBroadcastPriority = iRule
				iBroadcastRevalidate = MC_serverBD_4.iTimesRevalidatedObjectives[iTeam]
				increment_medal_objective_completed(iTeam, iRule)
				REINIT_NET_TIMER(tdeventsafetytimer)
				INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE()
				CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE OBJ HACK - JOB_COMP_HACK_OBJ set locally by part: ",iPartToUse)
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjHacked].iObjectHackedDialogueTrigger > -1
					PLAY_DIALOGUE_TRIGGER_ASAP(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjHacked].iObjectHackedDialogueTrigger)
					PRINTLN("[TMS] Playing dialogue trigger from hacking obj ")
				ENDIF
				
				RETURN TRUE
			ELSE
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE OBJ HACK - iObjHacked is set to ",MC_playerBD[iPartToUse].iObjHacked," but we aren't on a hack rule so clear it - for my part ",iPartToUse) 
				MC_playerBD[iPartToUse].iObjHacked = -1
			ENDIF
		ENDIF
		
	ELSE
		
		#IF IS_DEBUG_BUILD
		IF MC_playerBD[iPartToUse].iObjectiveTypeCompleted != -1
			PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - Unable to complete objectives, iObjectiveTypeCompleted = ",MC_playerBD[iPartToUse].iObjectiveTypeCompleted)
		ENDIF
		IF IS_BIT_SET( MC_serverBD.iProcessJobCompBitset, iPartToUse )
			PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - Unable to complete objectives, iProcessJobCompBitset is set for me, part ",iPartToUse)
		ENDIF
		IF IS_BIT_SET( MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER )
			PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - Unable to complete objectives, PBBOOL_OBJECTIVE_BLOCKER is set")
		ENDIF
		IF (iRule < FMMC_MAX_RULES)
			IF IS_BIT_SET( MC_serverBD_1.iObjectiveProgressionHeldUpBitset[iTeam], iRule)
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - Unable to complete objectives, iObjectiveProgressionHeldUpBitset is set for team ",iTeam," + rule ",iRule)
			ENDIF
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_SUB_TEAM_ROLES)
			AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRolesBitset[iRule][MC_playerBD[iLocalPart].iCoronaRole], ciBS_ROLE_Cant_Complete_This_Rule)
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - Unable to complete objectives, ciBS_ROLE_Cant_Complete_This_Rule is set for team ",iTeam," + rule ",iRule," + role ",MC_playerBD[iLocalPart].iCoronaRole)
			ENDIF
		ENDIF
		#ENDIF
		
		IF ANIMPOSTFX_IS_RUNNING("CrossLine")
		AND iRule < FMMC_MAX_RULES
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitSetSix[ iRule ], ciBS_RULE6_GET_DELIVER_RED_FILTER)
			PRINTLN("[MMacK][LocVFX] Uncrossed the Line through nefarious means")
			ANIMPOSTFX_STOP("CrossLine")
			ANIMPOSTFX_PLAY("CrossLineOut", 0, FALSE)
			PRINTLN("[CTLSFX] Stopping Local")
			IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_VFX_CAPTURE_OWNER)
				STRING sSoundSet 
				sSoundSet = "DLC_Apartments_Drop_Zone_Sounds"
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_HOSTILE_TAKEOVER_SOUNDS)
					sSoundSet = "dlc_xm_hota_Sounds"
				ENDIF
				PLAY_SOUND_FRONTEND(-1,"Exit_Capture_Zone",sSoundSet, FALSE)
			ELSE	
				PLAY_SOUND_FRONTEND(-1,"Player_Exit_Line","GTAO_FM_Cross_The_Line_Soundset", FALSE)
			ENDIF
			
			CLEAR_BIT(iLocalBoolCheck27, LBOOL27_RED_FILTER_SOUND_PLAYED)
			
			
			INT iLoc = MC_playerBD[iPartToUse].iCurrentLoc
			
			//WESTHERE
			IF iLoc != - 1
				IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2,ciLoc_BS2_RunningBackMarker ) 
					BROADCAST_FMMC_PLAYER_CROSSED_LINE(iTeam)
					PRINTLN("[RCC MISSION]HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE ciLoc_BS2_RunningBackMarker is on - broadcasting for whistle and cheer")
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
				IF iLoc != - 1
					PRINTLN("[RCC MISSION]HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE ciLoc_BS2_RunningBackMarker is: ", IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2,ciLoc_BS2_RunningBackMarker ) )
				ENDIF
			#ENDIF
			
		ENDIF
		
		//Clean up delivery progress - we've swapped out of the ability to deliver here
		IF HAS_NET_TIMER_STARTED(tdDeliverBackupTimer)
		OR IS_BIT_SET(iLocalBoolCheck2, LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
			NET_SET_PLAYER_CONTROL(LocalPlayer,TRUE)
			
			IF IS_PLAYER_IN_VEH_SEAT(LocalPlayer,VS_DRIVER)
			AND NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
				SET_VEHICLE_HANDBRAKE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed),FALSE)
			ENDIF
			
			IF IS_PED_IN_ANY_BOAT(LocalPlayerPed)
			OR IS_PED_IN_ANY_AMPHIBIOUS_VEHICLE(LocalPlayerPed) // url:bugstar:3498788
				VEHICLE_INDEX tempBoat = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
				IF IS_VEHICLE_DRIVEABLE(tempBoat)
					IF GET_PED_IN_VEHICLE_SEAT(tempBoat) = LocalPlayerPed
						IF NETWORK_HAS_CONTROL_OF_ENTITY(tempBoat)
							SET_BOAT_ANCHOR(tempBoat,FALSE)
							SET_BOAT_REMAINS_ANCHORED_WHILE_PLAYER_IS_DRIVER(tempBoat,FALSE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			MC_playerBD[iPartToUse].iVehDeliveryId =-1
			PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - Clearing iVehDeliveryId as tdDeliverBackupTimer or LBOOL2_BRING_VEHICLE_TO_HALT_CALLED")
			DeliveryVeh = NULL
			
			RESET_NET_TIMER(tdDeliverBackupTimer)
			CLEAR_BIT(iLocalBoolCheck2,LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
			CLEAR_BIT(iLocalBoolCheck2,LBOOL2_PED_DROPPED_OFF)
			CLEAR_BIT(iLocalBoolCheck,LBOOL_CARGOBOB_DROP)
			CLEAR_BIT(MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_DROPPING_OFF)
		ENDIF
		
	ENDIF
	
	//-- Fade out the drop-off marker
	IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
		#IF IS_DEBUG_BUILD
			IF bWdDropOffMarker
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LBOOL10_DROP_OFF_LOC_FADE_OUT is set eState = ", ENUM_TO_INT(locateDropOff.eState))
			ENDIF
		#ENDIF
		
		SET_LOCATE_TO_FADE_OUT(locateDropOff)
				
	ENDIF
	
	//-- Turn off the drop-off marker
	IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_NO_DISPLAY)
		#IF IS_DEBUG_BUILD
			IF bWdDropOffMarker
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LBOOL10_DROP_OFF_LOC_NO_DISPLAY is set eState = ", ENUM_TO_INT(locateDropOff.eState))
			ENDIF
		#ENDIF
		SET_LOCATE_TO_NOT_DISPLAY(locateDropOff)
	ENDIF
	
	//-- Turn on the drop-off marker.
	IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
		#IF IS_DEBUG_BUILD
			IF bWdDropOffMarker
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LBOOL10_DROP_OFF_LOC_DISPLAY is set eState = ", ENUM_TO_INT(locateDropOff.eState))
			ENDIF
		#ENDIF
		SET_LOCATE_TO_DISPLAY(locateDropOff)
	ENDIF
		
	RETURN FALSE

ENDFUNC
