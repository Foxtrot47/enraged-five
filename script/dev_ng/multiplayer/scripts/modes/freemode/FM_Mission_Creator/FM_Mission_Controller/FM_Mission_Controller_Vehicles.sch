
USING "FM_Mission_Controller_USING.sch"
USING "FM_Mission_Controller_Utility.sch"
USING "FM_Mission_Controller_Entities.sch"
USING "FM_Mission_Controller_ObjectiveCompletion.sch"
USING "FM_Mission_Controller_SpecialCase.sch"
USING "FM_Mission_Controller_Rules.sch"
USING "FM_Mission_Controller_Doors.sch"
USING "FM_Mission_Controller_Cutscene.sch"
USING "FM_Mission_Controller_Debug.sch"
USING "FM_Mission_Controller_Locations.sch"
USING "net_realty_armory_aircraft.sch"

//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: SERVER VEHICLE PROCESSING / VEH BRAIN !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************



FUNC BOOL IS_TRAILER_STUCK(VEHICLE_INDEX tempVeh)

	IF NOT IS_ENTITY_DEAD(tempVeh)
		IF GET_ENTITY_MODEL(tempVeh) = TRFLAT
		OR GET_ENTITY_MODEL(tempVeh) = TRAILERS
			IF GET_VEHICLE_PETROL_TANK_HEALTH(tempVeh) <= -1000
				RETURN TRUE
			ENDIF
		ENDIF
		IF GET_ENTITY_MODEL(tempVeh) = TANKER
		OR GET_ENTITY_MODEL(tempVeh) = TANKER2
			IF( GET_ENTITY_SUBMERGED_LEVEL(tempVeh) >= 0.5 AND GET_ENTITY_SPEED(tempVeh) < 1.0 )
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE

ENDFUNC

PROC RUN_FINAL_DESTROYED_VEHICLE_CHECK()
	
	BOOL bCheck
	INT iVeh, iTeam
	
	BOOL bHaveAllTeamsFailed = TRUE
	
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF NOT HAS_TEAM_FAILED(iTeam)
			bHaveAllTeamsFailed = FALSE
			iTeam = MC_serverBD.iNumberOfTeams // Break out!
		ENDIF
	ENDFOR
	
	IF NOT bHaveAllTeamsFailed
		
		FOR iveh = 0 TO (MC_serverBD.iNumVehCreated - 1)
			
			IF MC_serverBD.iVehteamFailBitset[iVeh] > 0
			AND GET_VEHICLE_RESPAWNS(iVeh) <= 0
			AND NOT IS_BIT_SET(MC_serverBD_1.sCreatedCount.iSkipVehBitset,iveh)
				
				bCheck = FALSE
				
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
					IF NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]))
					OR IS_TRAILER_STUCK(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]))
						bCheck = TRUE
					ENDIF
				ELSE
					bCheck = TRUE
				ENDIF
				
				IF bCheck
					PRINTLN("[RCC MISSION] RUN_FINAL_DESTROYED_VEHICLE_CHECK - Checking teams for vehicle ",iveh)
					
					bHaveAllTeamsFailed = TRUE
					
					FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
						IF NOT HAS_TEAM_FAILED(iTeam)
							RUN_TEAM_VEH_FAIL_CHECKS(iveh, iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam])
							
							IF NOT HAS_TEAM_FAILED(iTeam)
								bHaveAllTeamsFailed = FALSE
							#IF IS_DEBUG_BUILD
							ELSE
								PRINTLN("[RCC MISSION] RUN_FINAL_DESTROYED_VEHICLE_CHECK - Vehicle ",iveh," has caused team ",iTeam," to fail")
							#ENDIF
							ENDIF
						ENDIF
					ENDFOR
					
					IF bHaveAllTeamsFailed
						PRINTLN("[RCC MISSION] RUN_FINAL_DESTROYED_VEHICLE_CHECK - All teams have failed, break out")
						iveh = MC_serverBD.iNumVehCreated // Break out, everyone's failed already
					ENDIF
				ENDIF
				
			ENDIF
			
		ENDFOR
		
	ENDIF
	
ENDPROC

PROC HANDLE_SETTING_VEHS_ON_FIRE_FOR_RIOTVAN_MISSION_CLIENT(INT iVeh, VEHICLE_INDEX viPassed)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSeven, ciFMMC_VEHICLE7_SPAWN_BURNING)
	AND IS_ENTITY_ALIVE(viPassed)
		IF IS_BIT_SET(MC_serverBD_4.iBurningVehiclePTFXBitset, iVeh)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iBurningLifetime > 0
				PRINTLN("[JR][VEHICLEHUD] - iLocalFireDamageDealt, fLocalFireDamageDealtLastFrame, iVeh - ", fLocalFireDamageDealt[iVeh], " ", fLocalFireDamageDealtLastFrame[iVeh], " ", iVeh)	

				fLocalFireDamageDealtLastFrame[iVeh] = fLocalFireDamageDealt[iVeh]
			
				IF MC_serverBD_4.fBurningVehicleHealth[iVeh] <= 0
				AND MC_serverBD_4.fFireStrength[iVeh] > 0
					SET_VEHICLE_BODY_HEALTH(viPassed, 0.0)
					PRINTLN("[RiotVanFire] Exploding vehicle ", iVeh, " now due to MC_serverBD_4.fBurningVehicleHealth[iVeh] <= 0! MC_serverBD_4.fBurningVehicleHealth[iVeh] is ", MC_serverBD_4.fBurningVehicleHealth[iVeh])
					
					#IF IS_DEBUG_BUILD
					IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
						ASSERTLN("[RiotVanFire] Exploding vehicle ", iVeh, " now due to MC_serverBD_4.fBurningVehicleHealth[iVeh] <= 0! MC_serverBD_4.fBurningVehicleHealth[iVeh] is ", MC_serverBD_4.fBurningVehicleHealth[iVeh])
					ENDIF
					#ENDIF
				ELSE
					SET_ENTITY_PROOFS(viPassed, FALSE, TRUE, FALSE, FALSE, FALSE)
				ENDIF
				
				IF fLocalFireDamageDealt[iVeh] > fCurrentMaxFireDamage[iVeh]
					fCurrentMaxFireDamage[iVeh] += cfMAX_FIRE_DAMAGE_UNTIL_APPLICATION
					MC_playerBD[iLocalPart].fFireDamageSoFar[iVeh] = fLocalFireDamageDealt[iVeh]
					PRINTLN("[JR][RiotVanFire] MC_playerBD[iLocalPart].fFireDamageSoFar[iVeh]: ", MC_playerBD[iLocalPart].fFireDamageSoFar[iVeh])
				ENDIF
				
				IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_PROCESS_BURNING_VANS)
					SET_BIT(iLocalBoolCheck27, LBOOL27_PROCESS_BURNING_VANS)
					PRINTLN("[JR][RiotVanFire] Set bit LBOOL27_PROCESS_BURNING_VANS")
				ENDIF
			ENDIF
			
			SET_ENTITY_RENDER_SCORCHED(viPassed, TRUE)
		ELSE
			
			fCurrentMaxFireDamage[iVeh] = cfMAX_FIRE_DAMAGE_UNTIL_APPLICATION
			PRINTLN("[JR][RiotVanFire] - fCurrentMaxFireDamage = ", fCurrentMaxFireDamage[iVeh])
			IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_PROCESS_BURNING_VANS)
				SET_BIT(iLocalBoolCheck27, LBOOL27_PROCESS_BURNING_VANS)
				PRINTLN("[JR][RiotVanFire] Set bit LBOOL27_PROCESS_BURNING_VANS")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_SETTING_VEHS_ON_FIRE_FOR_RIOTVAN_MISSION_CLIENT_NONHOST(INT iVeh, VEHICLE_INDEX viPassed)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSeven, ciFMMC_VEHICLE7_SPAWN_BURNING)
	AND IS_ENTITY_ALIVE(viPassed)
		IF IS_BIT_SET(MC_serverBD_4.iBurningVehiclePTFXBitset, iVeh)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iBurningLifetime > 0		
				PRINTLN("[JR][VEHICLEHUD] - iLocalFireDamageDealt, fLocalFireDamageDealtLastFrame, iVeh - ", fLocalFireDamageDealt[iVeh], " ", fLocalFireDamageDealtLastFrame[iVeh], " ", iVeh)	
				IF fLocalFireDamageDealt[iVeh] <= fLocalFireDamageDealtLastFrame[iVeh]
					CLEAR_BIT(MC_playerBD[iLocalPart].iBurningVehFlashBS, iVeh)
				ENDIF
			
				fLocalFireDamageDealtLastFrame[iVeh] = fLocalFireDamageDealt[iVeh]
				
				PRINTLN("[JR][RiotVanFire] IS BIT LBOOL27_PROCESS_BURNING_VANS SET - ", IS_BIT_SET(iLocalBoolCheck27, LBOOL27_PROCESS_BURNING_VANS))
				IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_PROCESS_BURNING_VANS)
					SET_BIT(iLocalBoolCheck27, LBOOL27_PROCESS_BURNING_VANS)
					PRINTLN("[JR][RiotVanFire] Set bit LBOOL27_PROCESS_BURNING_VANS")
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_PROCESS_BURNING_VANS)
				SET_BIT(iLocalBoolCheck27, LBOOL27_PROCESS_BURNING_VANS)
				PRINTLN("[JR][RiotVanFire] Set bit LBOOL27_PROCESS_BURNING_VANS")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_SETTING_VEHS_ON_FIRE_FOR_RIOTVAN_MISSION_SERVER(INT iVeh, VEHICLE_INDEX viPassed)

	INT iTeam = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iBurnTeam
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSeven, ciFMMC_VEHICLE7_SPAWN_BURNING)
	AND MC_serverBD_4.iCurrentHighestPriority[iTeam] >= g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iBurnRule
	AND IS_ENTITY_ALIVE(viPassed)
		PRINTLN("[RCC MISSION][PROCESS_VEH_BRAIN][RiotVan] -------------------------- About to handle the burning of mission vehicle ", iVeh, " ---------------------------")
		IF NOT IS_BIT_SET(MC_serverBD_4.iBurningVehiclePTFXBitset, iVeh)
		
			USE_PARTICLE_FX_ASSET("scr_xm_riotvan")			
			MC_serverBD_4.ptfxVehicleFireFX_Front[iVeh] = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY("scr_xm_riotvan_fire_front", viPassed, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>)
			
			USE_PARTICLE_FX_ASSET("scr_xm_riotvan")
			MC_serverBD_4.ptfxVehicleFireFX_Back[iVeh] = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY("scr_xm_riotvan_fire_back", viPassed, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>)
			
			MC_serverBD_4.fFireStrength[iVeh] = 1.5 * g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fFirePutOutTime
			MC_serverBD_4.fBurningVehicleHealth[iVeh] = ciMAX_BURNING_VEHICLE_HEALTH
			
			SET_PARTICLE_FX_LOOPED_EVOLUTION(MC_serverBD_4.ptfxVehicleFireFX_Front[iVeh], "strength", 1, FALSE)
			SET_PARTICLE_FX_LOOPED_EVOLUTION(MC_serverBD_4.ptfxVehicleFireFX_Back[iVeh], "strength", 1, FALSE)
			
			IF DOES_PARTICLE_FX_LOOPED_EXIST(MC_serverBD_4.ptfxVehicleFireFX_Front[iVeh])
				PRINTLN("[RCC MISSION][TMS][RiotVanFire] SET_UP_FMMC_VEH_RC - Setting vehicle on fire due to ciFMMC_VEHICLE7_SPAWN_BURNING")
				SET_BIT(MC_serverBD_4.iBurningVehiclePTFXBitset, iVeh)
				
				PRINTLN("[RCC MISSION][TMS][RiotVanFire] SET_UP_FMMC_VEH_RC - Adding decal in slot ", iVeh)
				VECTOR vVanOffset = -GET_ENTITY_FORWARD_VECTOR(viPassed) * 2.5
				VECTOR vDecalPos = GET_ENTITY_COORDS(viPassed) + vVanOffset
				VECTOR vDecalRot = GET_ENTITY_FORWARD_VECTOR(viPassed)
				
				di_RiotVanDecals[iVeh] = ADD_DECAL(DECAL_RSID_GENERIC_SCORCH, vDecalPos, vDecalRot, vDecalRot, 2.5, 2.5, 1.0, 1.0, 1.0, 1.0, 99000)
			ELSE
				PRINTLN("[RiotVanFire] Trying to spawn the effect again")
			ENDIF
			
			
		ELSE
			FLOAT fStaggeredScalar = TO_FLOAT(MC_serverBD.iMaxLoopSize)
			
			//Damage
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iBurningLifetime > 0
			AND MC_serverBD_4.fFireStrength[iVeh] > 0.0
				
				//DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(viPassed), MC_serverBD_4.fBurningVehicleHealth[iVeh] * 0.2)
				
				IF MC_serverBD_4.fBurningVehicleHealth[iVeh] > ciMAX_BURNING_VEHICLE_HEALTH
					MC_serverBD_4.fBurningVehicleHealth[iVeh] = ciMAX_BURNING_VEHICLE_HEALTH
				ENDIF
				
				FLOAT fDamageToVehicleThisFrame = TO_FLOAT(ciMAX_BURNING_VEHICLE_HEALTH / g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iBurningLifetime) * GET_FRAME_TIME() * fStaggeredScalar
				MC_serverBD_4.fBurningVehicleHealth[iVeh] -= fDamageToVehicleThisFrame
				PRINTLN("[RiotVanFire] Dealing ", fDamageToVehicleThisFrame, " damage to vehicle ", iVeh, ". Health now: ", MC_serverBD_4.fBurningVehicleHealth[iVeh], " / ", ciMAX_BURNING_VEHICLE_HEALTH)
				
				#IF IS_DEBUG_BUILD
				IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
					ASSERTLN("[RiotVanFire] Dealing ", fDamageToVehicleThisFrame, " damage to vehicle ", iVeh, ". Health now: ", MC_serverBD_4.fBurningVehicleHealth[iVeh], " / ", ciMAX_BURNING_VEHICLE_HEALTH)
				ENDIF
				#ENDIF
			ELSE
				PRINTLN("[RiotVanFire] Dealing no damage to veh ", iVeh, " as this vehicle was given a burn lifetime of 0 or less")
			ENDIF			
			
			IF MC_serverBD_4.fTotalBurningVehFireDamage[iVeh] > MC_serverBD_4.fFireStrength[iVeh]
				MC_serverBD_4.fFireStrength[iVeh] = 0
				PRINTLN("[RiotVanFire] Putting out fire now!")
			ELSE							
				IF DOES_PARTICLE_FX_LOOPED_EXIST(MC_serverBD_4.ptfxVehicleFireFX_Front[iVeh])
					SET_PARTICLE_FX_LOOPED_EVOLUTION(MC_serverBD_4.ptfxVehicleFireFX_Front[iVeh], "strength", MC_serverBD_4.fFireStrength[iVeh], FALSE)
				ENDIF
				
				IF DOES_PARTICLE_FX_LOOPED_EXIST(MC_serverBD_4.ptfxVehicleFireFX_Back[iVeh])
					SET_PARTICLE_FX_LOOPED_EVOLUTION(MC_serverBD_4.ptfxVehicleFireFX_Back[iVeh], "strength", MC_serverBD_4.fFireStrength[iVeh], FALSE)
				ENDIF
			ENDIF

			//Making the fire go out when it has lost all strength
			IF MC_serverBD_4.fFireStrength[iVeh] <= 0.0 
				IF DOES_PARTICLE_FX_LOOPED_EXIST(MC_serverBD_4.ptfxVehicleFireFX_Front[iVeh])
					USE_PARTICLE_FX_ASSET("scr_xm_riotvan")
					START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_xm_riotvan_extinguish", viPassed, <<0.0,2.0,0.0>>, <<0.0,0.0,0.0>>)
					STOP_PARTICLE_FX_LOOPED(MC_serverBD_4.ptfxVehicleFireFX_Front[iVeh])
					PRINTLN("[TMS][RiotVanFire] Getting rid of front flames on vehicle ", iVeh)
					CLEAR_BIT(iLocalBoolCheck27, LBOOL27_PROCESS_BURNING_VANS)
					MC_serverBD_4.iNumberOfFiresExtinguished[iTeam]++
					PRINTLN("[TMS][RiotVanFire] Number of fires extinguished is now ", MC_serverBD_4.iNumberOfFiresExtinguished[iTeam])
				ENDIF
				
				IF DOES_PARTICLE_FX_LOOPED_EXIST(MC_serverBD_4.ptfxVehicleFireFX_Back[iVeh])
					USE_PARTICLE_FX_ASSET("scr_xm_riotvan")
					START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_xm_riotvan_extinguish", viPassed, <<0.0,-2.0,0.0>>, <<0.0,0.0,0.0>>)
					STOP_PARTICLE_FX_LOOPED(MC_serverBD_4.ptfxVehicleFireFX_Back[iVeh])
					PRINTLN("[TMS][RiotVanFire] Getting rid of back flames on vehicle ", iVeh)
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSeven, ciFMMC_VEHICLE7_PROGRESS_RULE_IF_FIRE_PUT_OUT) 
					PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE, iVeh, 0, TRUE)
					PRINTLN("[TMS][RiotVanFire] Passing objective for vehicle ", iVeh)
				ENDIF				
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_VEHICLE_CARGO(INT iVeh)
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[iVeh])
		CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[iVeh])
		PRINTLN("[RCC MISSION] CLEANUP_VEHICLE_CARGO - Veh ",iVeh,"'s cargo getting cleaned up (MC_serverBD_1.sFMMC_SBD.niVehicleCrate[", iVeh, "]).")
	ENDIF
ENDPROC

PROC DELETE_VEHICLE_CARGO(INT iVeh)
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[iVeh])
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[iVeh])
			DELETE_NET_ID(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[iVeh])
			PRINTLN("[RCC MISSION] DELETE_VEHICLE_CARGO - Veh ",iVeh,"'s cargo getting deleted (MC_serverBD_1.sFMMC_SBD.niVehicleCrate[", iVeh, "]).")
		ELSE
			CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[iVeh])
			PRINTLN("[RCC MISSION] DELETE_VEHICLE_CARGO - Veh ",iVeh,"'s cargo getting cleaned up as we don't have control (MC_serverBD_1.sFMMC_SBD.niVehicleCrate[", iVeh, "]).")
		ENDIF
	ENDIF
ENDPROC

PROC DELETE_FMMC_VEHICLE(INT iveh)
	
	INT iteam
	FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
		CLEAR_BIT(MC_serverBD.iVehteamFailBitset[iveh],iteam)
		CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iteam], iveh)
	ENDFOR
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iRespawnOnRuleChangeBS = 0
		SET_VEHICLE_RESPAWNS(iVeh, 0)
		PRINTLN("[RCC MISSION] DELETE_FMMC_VEHICLE - Veh ",iveh," getting cleaned up early - set MC_serverBD_2.iCurrentVehRespawnLives[", iveh, "] = 0")
	ENDIF
	
	DELETE_NET_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
	PRINTLN("[RCC MISSION] DELETE_FMMC_VEHICLE - Veh getting cleaned up early - delete, objective: ",g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iCleanupObjective," team: ",g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iCleanupTeam," vehicle: ",iveh)
	DELETE_VEHICLE_CARGO(iveh)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iRespawnOnRuleChangeBS > 0
	AND NOT IS_BIT_SET(MC_ServerBD.iServerBS_PassRuleVehAfterRespawn, iVeh)
		PRINTLN("[RCC MISSION] DELETE_FMMC_VEHICLE - Setting iServerBS_PassRuleVehAfterRespawn on iVeh: ",iveh)
		SET_BIT(MC_ServerBD.iServerBS_PassRuleVehAfterRespawn, iVeh)
	ENDIF
	
	CLEAR_BIT(MC_serverBD.iVehCleanup_NeedOwnershipBS, iveh)
	
ENDPROC

FUNC BOOL CLEANUP_VEH_EARLY(INT iveh, VEHICLE_INDEX tempVeh)

	IF SHOULD_CLEANUP_VEH(iveh,tempVeh)
		
		BOOL bSetAsNoLongerNeeded
		
		// Do not visibly clean up, set as no longer needed
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet,ciFMMC_VEHICLE_IgnoreVisCheckCleanup)
			bSetAsNoLongerNeeded = TRUE
		ENDIF
		
		PRINTLN("[RCC MISSION] CLEANUP_VEH_EARLY - veh ",iveh," Flag ciFMMC_VEHICLE_IgnoreVisCheckCleanup = ", IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet,ciFMMC_VEHICLE_IgnoreVisCheckCleanup), " bSetAsNoLongerNeeded = ", bSetAsNoLongerNeeded )
		
		IF (NOT bSetAsNoLongerNeeded) // If it's already going to clean up, we don't need to check this stuff
		AND (NOT ((g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = VELUM2) AND (iCurrentHeistMissionIndex = HBCA_BS_PRISON_BREAK_FINALE))) // This now happens during the cutscene
			IF DOES_ENTITY_EXIST(tempVeh)
				// Check that any peds in the vehicle are going to clean up too before deleting it (if not, then only mark it as no longer needed / clean it up)
				BOOL bAllPassengersWantToCleanup = TRUE
				INT iSeat
				INT iPassengerSeats = GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(tempVeh)
				PED_INDEX pedInVeh
				INT iPed
				
				FOR iSeat = -1 TO (iPassengerSeats - 1) // All of VEHICLE_INDEX for this car apart from VS_ANY_PASSENGER
					pedInVeh = GET_PED_IN_VEHICLE_SEAT(tempVeh, INT_TO_ENUM(VEHICLE_SEAT, iSeat))
					
					IF NOT IS_PED_INJURED(pedInVeh)
						//Check for players being in passenger seats B*2175272
						// B*2239227 - moved to be the first check so this has to fail first
						IF IS_PED_A_PLAYER(pedInVeh)
							bAllPassengersWantToCleanup = FALSE
							iSeat = iPassengerSeats // Break out
						ELSE
							iPed = IS_ENTITY_A_MISSION_CREATOR_ENTITY(pedInVeh)
							IF iPed != -1
								IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
									IF NOT SHOULD_CLEANUP_PED(iPed, NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPed]))
										bAllPassengersWantToCleanup = FALSE
										iSeat = iPassengerSeats // Break out
									ENDIF
								ENDIF
							ENDIF	
						ENDIF
					ENDIF
				ENDFOR
			
				
				// Only set as no longer needed if at least one of the peds inside doesn't want to clean up
				IF NOT bAllPassengersWantToCleanup
					bSetAsNoLongerNeeded = TRUE
				ENDIF
				
				PRINTLN("[RCC MISSION] CLEANUP_VEH_EARLY - veh ",iveh," IS_VEHICLE_EMPTY = FALSE - bAllPassengersWantToCleanup = ", bAllPassengersWantToCleanup, " - bSetAsNoLongerNeeded = ", bSetAsNoLongerNeeded )
			
			ENDIF
			
			
		ELSE
			PRINTLN("[RCC MISSION] CLEANUP_VEH_EARLY - veh ",iveh," IS_VEHICLE_EMPTY = TRUE - bSetAsNoLongerNeeded = ", bSetAsNoLongerNeeded )
		ENDIF
			
	
		IF bSetAsNoLongerNeeded
		
			INT iteam
			FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
				CLEAR_BIT(MC_serverBD.iVehteamFailBitset[iveh],iteam)
				CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iteam], iveh)
			ENDFOR
			SET_VEHICLE_RESPAWNS(iVeh, 0)
			PRINTLN("[RCC MISSION] CLEANUP_VEH_EARLY - Veh ",iveh," getting cleaned up early - set MC_serverBD_2.iCurrentVehRespawnLives[", iveh, "] = 0.")
			
			IF IS_BIT_SET(MC_serverBD.iVehCleanup_NeedOwnershipBS, iveh) //url:bugstar:4089840
				CLEAR_BIT(MC_serverBD.iVehCleanup_NeedOwnershipBS, iveh)
				PRINTLN("[RCC MISSION] CLEANUP_VEH_EARLY - Veh ",iveh," clearing iVehCleanup_NeedOwnershipBS")
			ENDIF
			
			PRINTLN("[RCC MISSION] CLEANUP_VEH_EARLY - Veh ",iveh," Setting iVehAboutToCleanup_BS")
			SET_BIT(MC_ServerBD.iVehAboutToCleanup_BS, iveh)
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iRespawnOnRuleChangeBS != 0
			AND NOT IS_BIT_SET(MC_ServerBD.iServerBS_PassRuleVehAfterRespawn, iVeh)
				PRINTLN("[RCC MISSION] CLEANUP_VEH_EARLY - Setting iServerBS_PassRuleVehAfterRespawn on iVeh: ",iveh)
				SET_BIT(MC_ServerBD.iServerBS_PassRuleVehAfterRespawn, iVeh)
			ENDIF
			
			CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
			PRINTLN("[RCC MISSION] CLEANUP_VEH_EARLY - Veh getting cleaned up early instead - no longer needed, objective: ",g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iCleanupObjective," team: ",g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iCleanupTeam," vehicle: ",iveh)
			CLEANUP_VEHICLE_CARGO(iveh)
			RETURN TRUE
			
		ELSE
			
			IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
				PRINTLN("[RCC MISSION] CLEANUP_VEH_EARLY - Requesting control of vehicle ",iveh," for deletion; setting MC_serverBD.iVehCleanup_NeedOwnershipBS bit - for delete, objective: ",g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iCleanupObjective," team: ",g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iCleanupTeam," vehicle: ",iveh)
				NETWORK_REQUEST_CONTROL_OF_NETWORK_ID( MC_serverBD_1.sFMMC_SBD.niVehicle[iveh] )
				SET_BIT(MC_serverBD.iVehCleanup_NeedOwnershipBS, iveh)
			ELSE
				PRINTLN("[RCC MISSION] CLEANUP_VEH_EARLY - Deleting vehicle ",iveh)
				DELETE_FMMC_VEHICLE(iveh)
				
				RETURN TRUE
			ENDIF

		ENDIF
	ENDIF
				
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_DESTROYED_VEHICLE_READY_FOR_CLEANUP(INT iVeh)
	
	BOOL bReady = TRUE
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = GBURRITO
	OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = GBURRITO2
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetTwo, ciFMMC_VEHICLE2_EXTRA_MINI_SERVER) //Check if the broken server prop has already been created
		AND NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[iveh])
			
			MODEL_NAMES mnBrokenCrate = HEI_PROP_MINI_SEVER_BROKEN
			
			MODEL_NAMES mnCurrentCrate = GET_ENTITY_MODEL(NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[iveh]))
			
			IF mnCurrentCrate != mnBrokenCrate
				bReady = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bReady
	
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_COUNT_AS_HELD_FOR_TEAM(INT iveh,INT iteam)
	
	IF MC_serverBD_4.ivehRule[iveh][iteam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
	OR MC_serverBD_4.ivehRule[iveh][iteam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
	OR MC_serverBD_4.ivehRule[iveh][iteam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD
		IF MC_serverBD_4.iVehPriority[iveh][iteam] <= MC_serverBD_4.iCurrentHighestPriority[iteam]
		AND ( (MC_serverBD_4.ivehRule[iveh][iteam] != FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER) OR (NOT IS_VEHICLE_WAITING_FOR_PLAYERS(iVeh,iTeam)))
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC DIRTY_SANCHEZ_BIKE_DESTROY_CHECK(NETWORK_INDEX netVeh)
	// [LM] The scorcher on Dirty Sanchez cleans up sometimes when we need to delete it completely.
	IF NETWORK_DOES_NETWORK_ID_EXIST(netVeh)
		IF NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(netVeh))
			IF IS_PED_IN_VEHICLE_OR_WAITING_TO_START_TASK_ENTER_VEHICLE(LocalPlayerPed, NET_TO_VEH(netVeh))
				CLEAR_PED_TASKS_IMMEDIATELY(LocalPlayerPed)							
			ENDIF																		
			IF IS_PED_IN_VEHICLE(LocalPlayerPed, NET_TO_VEH(netVeh), FALSE)
				TASK_LEAVE_VEHICLE(LocalPlayerPed, NET_TO_VEH(netVeh), ECF_WARP_IF_DOOR_IS_BLOCKED)
			ENDIF
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DIRTY_SANCHEZ(g_FMMC_STRUCT.iAdversaryModeType)
				IF IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(NET_TO_VEH(netVeh)))
					PRINTLN("[LM][PROCESS_VEH_BRAIN]")	
					
					IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(NET_TO_VEH(netVeh))
						NETWORK_REQUEST_CONTROL_OF_ENTITY(NET_TO_VEH(netVeh))
					ENDIF
					
					IF NETWORK_HAS_CONTROL_OF_ENTITY(NET_TO_VEH(netVeh))
						PRINTLN("[LM][PROCESS_VEH_BRAIN] - Delete the Scorcher on Dirty Sanchez, it's undriveable and we don't want it's corpse lingering about.")
						PRINTLN("[LM][PROCESS_VEH_BRAIN] - EXIT EARLY ON VEH_BRAIN")
						DELETE_NET_ID(netVeh)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_TIME_SLIPSTREAMING(INT& fSSTime) 
	FLOAT fTime
	IF DOES_ENTITY_EXIST(LocalPlayerPed)
	AND NOT IS_ENTITY_DEAD(LocalPlayerPed)
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		AND GET_ENTITY_SPEED(LocalPlayerPed) > 0
		
			fTime = GET_VEHICLE_CURRENT_TIME_IN_SLIP_STREAM(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)) 
			
			fSSTime += ROUND(fTime)
			PRINTLN("[KH] UPDATE_TIME_SLIPSTREAMING: ", fSSTime)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_DEAD_VEHICLE_CHECK_TEAM_FAILS(INT iVeh)
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMBUSHKA(g_FMMC_STRUCT.iAdversaryModeType)
		IF IS_BIT_SET(iLocalBoolCheck7, LBOOL7_HOST_APPLIED_NEW_PRIORITY_MIDFRAME)
			PRINTLN("[LM][PROCESS_VEH_BRAIN] SHOULD_DEAD_VEHICLE_CHECK_TEAM_FAILS - Vehicle ",iVeh," returning FALSE, bombushka mission has new priority midframe!")
			RETURN FALSE
		ELIF SHOULD_TEAM_RESPAWN_BACK_AT_START(0)
			PRINTLN("[LM][PROCESS_VEH_BRAIN] SHOULD_DEAD_VEHICLE_CHECK_TEAM_FAILS - Vehicle ",iVeh," returning FALSE, bombushka mission will respawn players back at start!")
			RETURN FALSE
		ENDIF
	ENDIF
	
	PRINTLN("[LM][PROCESS_VEH_BRAIN] SHOULD_DEAD_VEHICLE_CHECK_TEAM_FAILS - Vehicle ",iVeh," GET_VEHICLE_RESPAWNS: ", GET_VEHICLE_RESPAWNS(iVeh), " iRespawnOnRuleChangeBS: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iRespawnOnRuleChangeBS)
	
	IF GET_VEHICLE_RESPAWNS(iVeh) <= 0 
		IF NOT IS_BIT_SET(MC_serverBD_1.sCreatedCount.iSkipVehBitset,iveh)
			RETURN TRUE
		ELSE
			PRINTLN("[LM][PROCESS_VEH_BRAIN] SHOULD_DEAD_VEHICLE_CHECK_TEAM_FAILS - Vehicle ",iVeh," iSkipVehBitset skip set...")
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iRespawnOnRuleChangeBS != 0
		IF NOT IS_BIT_SET(MC_ServerBD.iServerBS_PassRuleVehAfterRespawn, iVeh)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

// handles state logic for the mission vehicles
PROC PROCESS_VEH_BRAIN(INT iveh)

	PED_INDEX tempPed
	PLAYER_INDEX tempPlayer
	PARTICIPANT_INDEX tempPart
	VEHICLE_INDEX tempVeh,tempTruck
	INT ivehTeam
	INT iteam	
	INT ipart
	BOOL bcheckteamfail
	INT ipriority[FMMC_MAX_TEAMS]
	BOOL bwasobjective[FMMC_MAX_TEAMS]
	
	IF NOT bIsLocalPlayerHost
		EXIT
	ENDIF
	
	IF MC_serverBD.iVehRequestPart[iveh] != -1
		IF MC_playerBD[MC_serverBD.iVehRequestPart[iveh]].iVehRequest = -1
			PRINTLN("[RCC MISSION][PROCESS_VEH_BRAIN] PROCESS_VEH_BRAIN - Vehicle ",iveh," was being requested by part ",MC_serverBD.iVehRequestPart[iveh]," but no longer, resetting MC_serverBD.iVehRequestPart")
			MC_serverBD.iVehRequestPart[iveh] = -1
		ENDIF
	ENDIF
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
		
		IF NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]))
		OR IS_TRAILER_STUCK(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]))
			RESET_NET_TIMER(MC_serverBD_1.tdControlVehTimer[iveh])
			
			#IF IS_DEBUG_BUILD
			IF NOT bStopVehDeadCleanup
			#ENDIF			
			
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DIRTY_SANCHEZ(g_FMMC_STRUCT.iAdversaryModeType)
			AND IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])))
				DIRTY_SANCHEZ_BIKE_DESTROY_CHECK(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])	
			ELIF IS_DESTROYED_VEHICLE_READY_FOR_CLEANUP(iveh)
				PRINTLN("[RCC MISSION][PROCESS_VEH_BRAIN] Vehicle is undriveable (or it's a trailer and it's stuck) - cleaning up veh ",iveh," & set iPedRespawnVehBitset")
				
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_MINI_ROUNDS_WAIT_FOR_VEH_RESPAWN)
					CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
				ENDIF
				
				CLEANUP_VEHICLE_CARGO(iveh)
				
				SET_BIT(MC_serverBD.iPedRespawnVehBitset, iVeh)
				
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[RCC MISSION][PROCESS_VEH_BRAIN] Vehicle is undriveable (or it's a trailer and it's stuck) - waiting for IS_DESTROYED_VEHICLE_READY_FOR_CLEANUP to return TRUE before cleanup")
			#ENDIF
			ENDIF
			
			IF SHOULD_DEAD_VEHICLE_CHECK_TEAM_FAILS(iVeh)
				bcheckteamfail = TRUE
			ENDIF
			
			#IF IS_DEBUG_BUILD
			ENDIF
			#ENDIF
			
			IF NOT IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_SHOULD_PLAY_RUNNING_BACK_REMIX_BLOCKED)
			AND GET_PLAYER_RESPAWN_BACK_AT_START_STATE(iLocalPart) = PRBSS_PlayingNormally
				PRINTLN("[RCC MISSION][PROCESS_VEH_BRAIN][LM][PROCESS_RUNNING_BACK_SHARDS] - SBBOOL7_SHOULD_PLAY_RUNNING_BACK_REMIX_BLOCKED - Setting bit. (Dead)")
				SET_BIT(MC_ServerBD.iServerBitSet7, SBBOOL7_SHOULD_PLAY_RUNNING_BACK_REMIX_BLOCKED)
				SET_BIT(MC_ServerBD.iServerBitSet7, SBBOOL7_SHOULD_PLAY_RUNNING_BACK_REMIX_DESTROYED)
			ENDIF
		ELSE
			tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
			IF CLEANUP_VEH_EARLY(iveh,tempVeh)
				
				EXIT
			ENDIF
		ENDIF
		
		//IF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_RIOTVAN)
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
		AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
			VEHICLE_INDEX viVehToBurn = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
			
			IF DOES_ENTITY_EXIST(viVehToBurn)
				HANDLE_SETTING_VEHS_ON_FIRE_FOR_RIOTVAN_MISSION_SERVER(iVeh, viVehToBurn)
			ENDIF
		ENDIF
		//ENDIF
	ELSE
		IF IS_BIT_SET(MC_ServerBD.iVehAboutToCleanup_BS, iveh)
			PRINTLN("[RCC MISSION][PROCESS_VEH_BRAIN] iVeh: ", iVeh, " Clearing iVehAboutToCleanup_BS.")
			CLEAR_BIT(MC_ServerBD.iVehAboutToCleanup_BS, iveh)
		ENDIF
		
		IF SHOULD_DEAD_VEHICLE_CHECK_TEAM_FAILS(iVeh)
			bcheckteamfail = TRUE
		ENDIF
	ENDIF
	
	// End the Round Early..
	IF MC_serverBD_4.iVehPriority[iveh][iteam] < FMMC_MAX_RULES
		IF NOT IS_BIT_SET(MC_ServerBD.iServerBitSet6, SBBOOL6_LAP_PASSED)
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciBOMBUSHKA_END_EARLY_ROUND)
			AND NOT IS_BIT_SET(MC_ServerBD.iServerBitSet6, SBBOOL6_LAP_BOMBUSHKA_PROGRESSED)
				IF MC_ServerBD.iTeamScore[1] > MC_ServerBD.iCachedBestRuleScore[0]
				AND MC_ServerBD.iCachedBestRuleScore[0] > 0
					
					PRINTLN("[RCC MISSION][PROCESS_VEH_BRAIN][LM][SBBOOL6_LAP_BOMBUSHKA_PROGRESSED] Calling to Progress Round for MC_ServerBD.iTeamScore[1] : ", MC_ServerBD.iTeamScore[1], MC_ServerBD.iTeamScore[0])
					
					BROADCAST_FMMC_SHARD_BOMBUSHKA_MINI_ROUND_END_EARLY(1)
					INT iTeampLoop
					FOR iTeampLoop = 0 TO FMMC_MAX_TEAMS-1
						IF IS_TEAM_ACTIVE(iTeampLoop)
							IF MC_ServerBD_4.iCurrentHighestPriority[iTeampLoop] < FMMC_MAX_RULES
								PRINTLN("[RCC MISSION][PROCESS_VEH_BRAIN][LM][SBBOOL6_LAP_BOMBUSHKA_PROGRESSED] Calling to Progress Round for iTeam: ", iTeampLoop)
								PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE, iVeh, iTeampLoop, FALSE)
							ENDIF
						ENDIF
					ENDFOR
					SET_BIT(MC_ServerBD.iServerBitSet6, SBBOOL6_LAP_BOMBUSHKA_PROGRESSED)					
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
		IF MC_serverBD_4.iVehPriority[iveh][iteam] < FMMC_MAX_RULES
		OR IS_BIT_SET(MC_serverBD.iMissionCriticalVeh[iTeam], iveh)
			IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet,SBBOOL_MISSION_OVER)
				IF bcheckteamfail
					RUN_TEAM_VEH_FAIL_CHECKS(iveh,iteam,MC_serverBD_4.iVehPriority[iveh][iteam])
				ENDIF
			ENDIF
		ENDIF
		
		PRINTLN("[LM][VEH_PRIORITY] - iVeh: ", iVeh, " iTeam: ", iTeam, " Respawns: ", MC_serverBD_2.iCurrentVehRespawnLives[iVeh], " iRespawnOnRuleChangeBS: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iRespawnOnRuleChangeBS, " iServerBS_PassRuleVehAfterRespawn: ", IS_BIT_SET(MC_ServerBD.iServerBS_PassRuleVehAfterRespawn, iVeh)
			, " iVehPriority: ", MC_serverBD_4.iVehPriority[iveh][iteam], " iCurrentHighestPriority: ", MC_serverBD_4.iCurrentHighestPriority[iteam], " iCurrentHigh: ", iCurrentHighPriority[iteam])
		
		IF MC_serverBD_4.iVehPriority[iveh][iteam] < FMMC_MAX_RULES			
			IF NOT bcheckteamfail
			OR (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iRespawnOnRuleChangeBS != 0 AND NOT IS_BIT_SET(MC_ServerBD.iServerBS_PassRuleVehAfterRespawn, iVeh))
				IF MC_serverBD_4.ivehRule[iveh][iteam] != FMMC_OBJECTIVE_LOGIC_NONE
					IF MC_serverBD_4.iVehPriority[iveh][iteam] <= iCurrentHighPriority[iteam]
						IF MC_serverBD_4.ivehRule[iveh][iteam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD
							IF MC_serverBD_4.iVehPriority[iveh][iteam] <= MC_serverBD_4.iCurrentHighestPriority[iteam]
								
								IF IS_ENTITY_IN_DROP_OFF_AREA(tempVeh, iteam, MC_serverBD_4.iVehPriority[iveh][iteam], ciRULE_TYPE_VEHICLE, iveh)
									IF NOT IS_BIT_SET(MC_serverBD.iVehAtYourHolding[iteam],iveh)
										IF MC_serverBD.iDriverPart[iveh] > -1
											tempPart = INT_TO_PARTICIPANTINDEX(MC_serverBD.iDriverPart[iveh])
											IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
												IF MC_PlayerBD[MC_serverBD.iDriverPart[iveh]].iteam = iteam
													tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
													SET_BIT(MC_serverBD.ivehHoldPartPoints,MC_serverBD.iDriverPart[iveh])
													INCREMENT_SERVER_REMOTE_PLAYER_SCORE(tempPlayer, GET_FMMC_POINTS_FOR_TEAM(iteam,MC_serverBD_4.iVehPriority[iveh][iteam]), iteam, MC_serverBD_4.iVehPriority[iveh][iteam])
													SET_BIT(MC_serverBD.iVehAtYourHolding[iteam],iveh)
												ENDIF
											ENDIF
										ELSE
											SET_BIT(MC_serverBD.iVehAtYourHolding[iteam],iveh)
											INCREMENT_SERVER_TEAM_SCORE(iteam,MC_serverBD_4.iVehPriority[iveh][iteam],GET_FMMC_POINTS_FOR_TEAM(iteam,MC_serverBD_4.iVehPriority[iveh][iteam]))
										ENDIF
									ENDIF
								ELSE
									IF IS_BIT_SET(MC_serverBD.iVehAtYourHolding[iteam],iveh)
										CLEAR_BIT(MC_serverBD.iVehAtYourHolding[iteam],iveh)
										BOOL bRemovedPlayerScore
										FOR ipart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
											IF IS_BIT_SET(MC_serverBD.iVehHoldPartPoints,ipart)
												tempPart = INT_TO_PARTICIPANTINDEX(ipart)
												IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
													tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
													INCREMENT_SERVER_REMOTE_PLAYER_SCORE(tempPlayer, (-1 * GET_FMMC_POINTS_FOR_TEAM(iteam,MC_serverBD_4.iVehPriority[iveh][iteam])), iteam, MC_serverBD_4.iVehPriority[iveh][iteam])
													bRemovedPlayerScore = TRUE
												ENDIF
												CLEAR_BIT(MC_serverBD.iVehHoldPartPoints,ipart)
											ENDIF
										ENDFOR
										IF NOT bRemovedPlayerScore
											INCREMENT_SERVER_TEAM_SCORE(iteam,MC_serverBD_4.iVehPriority[iveh][iteam], -GET_FMMC_POINTS_FOR_TEAM(iteam,MC_serverBD_4.iVehPriority[iveh][iteam]))
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF MC_serverBD_4.iVehPriority[iveh][iteam] <= MC_serverBD_4.iCurrentHighestPriority[iteam]
							iTempNumPriorityRespawnVeh[iteam] = iTempNumPriorityRespawnVeh[iteam] + GET_VEHICLE_RESPAWNS(iVeh)
						ENDIF
						icurrentHighPriority[iteam] = MC_serverBD_4.iVehPriority[iveh][iteam]
						
						itempVehMissionLogic[iteam] = MC_serverBD_4.ivehRule[iveh][iteam]
						IF iOldHighPriority[iteam] > icurrentHighPriority[iteam]
							iNumHighPriorityVeh[iteam] = 0
							iOldHighPriority[iteam] = icurrentHighPriority[iteam]
						ENDIF
						iNumHighPriorityLoc[iteam] = 0
						itempLocMissionLogic[iteam] = FMMC_OBJECTIVE_LOGIC_NONE
						iNumHighPriorityObj[iteam] = 0
						itempObjMissionLogic[iteam] = FMMC_OBJECTIVE_LOGIC_NONE
						itempObjMissionSubLogic[iteam] = ci_HACK_SUBLOGIC_NONE
						iNumHighPriorityPed[iteam] = 0
						iNumHighPriorityDeadPed[iteam] = 0
						itempPedMissionLogic[iteam] = FMMC_OBJECTIVE_LOGIC_NONE
						iNumHighPriorityPlayerRule[iteam] = 0
						itempPlayerRuleMissionLogic[iteam] = FMMC_OBJECTIVE_LOGIC_NONE
						iNumHighPriorityVeh[iteam]++
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
		AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]))
			IF MC_serverBD_4.iVehPriority[iveh][iteam] < FMMC_MAX_RULES
			AND MC_serverBD_4.iVehPriority[iveh][iteam] > -1
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[MC_serverBD_4.iVehPriority[iveh][iteam]], ciBS_RULE10_PASS_RULE_THROUGH_TAGGING)
					IF IS_ENTITY_ALREADY_TAGGED(ENUM_TO_INT(ET_VEHICLE),iVeh)
						PRINTLN("[RCC MISSION][PROCESS_VEH_BRAIN] OBJECT HAS BEEN TAGGED. Calling PROGRESS_SPECIFIC_OBJECTIVE")
						INVALIDATE_SINGLE_OBJECTIVE(iteam, MC_ServerBD_4.iVehPriority[iVeh][iTeam], TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iRespawnOnRuleChangeBS != 0
	AND bcheckteamfail
	AND NOT IS_BIT_SET(MC_ServerBD.iServerBS_PassRuleVehAfterRespawn, iVeh)
		PRINTLN("[RCC MISSION][PROCESS_VEH_BRAIN] - Setting iServerBS_PassRuleVehAfterRespawn on iVeh: ",iveh)
		SET_BIT(MC_ServerBD.iServerBS_PassRuleVehAfterRespawn, iVeh)
	ENDIF
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
	OR NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]))
		MC_serverBD.iDriverPart[iveh] = -1
		MC_serverBD.iVehRequestPart[iveh] = -1
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive, ciFMMC_VEHICLE5_HACKING_USE_DISTANCE_FOR_CAPTURE)
		EXIT
	ENDIF
	
	tempPed = GET_PED_IN_VEHICLE_SEAT(tempVeh)
	IF IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(tempVeh)
		tempTruck = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_ATTACHED_TO(tempVeh))
	ENDIF
	IF IS_VEHICLE_DRIVEABLE(tempTruck)
		tempPed = GET_PED_IN_VEHICLE_SEAT(tempTruck)
	ENDIF
	
	IF NOT IS_PED_INJURED(tempPed)
		IF IS_PED_A_PLAYER(tempPed)
			tempPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(tempPed)
			IF IS_NET_PLAYER_OK(tempPlayer)
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(tempPlayer)
					tempPart = NETWORK_GET_PARTICIPANT_INDEX(tempPlayer)
					IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
						ipart = NATIVE_TO_INT(tempPart)
						ivehTeam = MC_playerBD[ipart].iteam
						IF ivehTeam >= 0

							IF SHOULD_VEHICLE_COUNT_AS_HELD_FOR_TEAM(iveh,ivehTeam)							
								MC_serverBD.iDriverPart[iveh] = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(tempPlayer))
								PRINTLN("LM CHECK MC_serverBD.iDriverPart[iveh]: ", MC_serverBD.iDriverPart[iveh])
							ELSE
								MC_serverBD.iDriverPart[iveh] = -1
							ENDIF
							
							IF IS_BIT_SET(MC_playerBD[NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(tempPlayer))].iClientBitSet,PBBOOL_OBJECTIVE_BLOCKER)
								RESET_NET_TIMER(MC_serverBD_1.tdControlVehTimer[iveh])
								MC_serverBD.iDriverPart[iveh] = -1
								EXIT
							ENDIF
							
							IF MC_serverBD_4.ivehRule[iveh][ivehTeam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
								IF MC_serverBD_4.iVehPriority[iveh][ivehTeam] < FMMC_MAX_RULES
									IF NOT HAS_NET_TIMER_STARTED(MC_serverBD_1.tdControlVehTimer[iveh])
										START_NET_TIMER(MC_serverBD_1.tdControlVehTimer[iveh])
										MC_serverBD_1.tdControlVehTimer[iveh].Timer = GET_TIME_OFFSET(MC_serverBD_1.tdControlVehTimer[iveh].Timer,( -1* iStoredVehCaptureMS[ivehTeam][iVeh]))
										NET_PRINT("[RCC MISSION][PROCESS_VEH_BRAIN]starting control timer for veh: ")  NET_PRINT_INT(iveh) NET_NL()
									ELSE
										IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_RETAIN_CAPTURE_PROGRESS)
											IF iStoredVehCaptureMS[ivehTeam][iVeh] > MC_serverBD.iVehTakeoverTime[ivehTeam][MC_serverBD_4.iVehPriority[iveh][ivehTeam]]
																							
												BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM_CON_VEH,0,ivehTeam,-1,tempplayer,ci_TARGET_VEHICLE,iveh)
												
												PRINTLN("[RCC MISSION][PROCESS_VEH_BRAIN] cleaning up vehicle for control objective, ",iveh)
												
												PRINTLN("[RCC MISSION][PROCESS_VEH_BRAIN][VEHICLE CAPTURE] Number of iRuleReCapture: ",MC_serverBD.iRuleReCapture[ivehTeam][MC_serverBD_4.iVehPriority[iveh][ivehTeam]]) 
												PRINTLN("[RCC MISSION][PROCESS_VEH_BRAIN] GET_VEHICLE_RESPAWNS(",iVeh,"): ",GET_VEHICLE_RESPAWNS(iVeh))
												
												IF MC_serverBD_4.iVehPriority[iveh][ivehTeam] < FMMC_MAX_RULES
													//BROADCAST_GIVE_PLAYER_XP(tempPlayer,200,-1,TRUE)
													INCREMENT_SERVER_REMOTE_PLAYER_SCORE(tempplayer, GET_FMMC_POINTS_FOR_TEAM(ivehTeam,MC_serverBD_4.iVehPriority[iveh][ivehTeam]), ivehTeam, MC_serverBD_4.iVehPriority[iveh][ivehTeam])
													
													IF MC_serverBD.iRuleReCapture[ivehTeam][MC_serverBD_4.iVehPriority[iveh][ivehTeam]] <= 0
														IF SHOULD_VEH_CLEAN_UP_ON_DELIVERY( iVeh, ivehTeam )
															PRINTLN("[RCC MISSION] cleaning up vehicle for control objective, ",iveh)
															CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
															CLEANUP_VEHICLE_CARGO(iveh)
															//MC_serverBD_1.sFMMC_SBD.niVehicle[iveh] = NULL
														ENDIF
													ENDIF
													IF MC_serverBD.iRuleReCapture[ivehTeam][MC_serverBD_4.iVehPriority[iveh][ivehTeam]] <=0	
														
														PRINTLN("[RCC MISSION][PROCESS_VEH_BRAIN][VEHICLE CAPTURE] - In here ")
														
														IF SHOULD_VEH_CLEAN_UP_ON_DELIVERY( iVeh, ivehTeam )
															FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
																ipriority[iTeam] =MC_serverBD_4.iVehPriority[iveh][iTeam]
																IF iteam = ivehTeam
																	IF IS_BIT_SET(MC_serverBD.iVehteamFailBitset[iveh],iteam)
																		CLEAR_BIT(MC_serverBD.iVehteamFailBitset[iveh],iteam)
																		CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iTeam], iveh)
																		NET_PRINT("[RCC MISSION][PROCESS_VEH_BRAIN]OBJECTIVE NOT FAILED FOR TEAM: ")NET_PRINT_INT(ivehTeam) NET_PRINT(" BECAUSE THEY CAPTURED VEH: ") NET_PRINT_INT(iveh) NET_NL()
																		bwasobjective[iTeam] = TRUE
																	ENDIF
																ELSE
																	IF DOES_TEAM_LIKE_TEAM(iteam,ivehTeam)
																		IF IS_BIT_SET(MC_serverBD.iVehteamFailBitset[iveh],iteam)
																			CLEAR_BIT(MC_serverBD.iVehteamFailBitset[iveh],iteam)
																			CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iTeam], iveh)
																			NET_PRINT("[RCC MISSION][PROCESS_VEH_BRAIN]OBJECTIVE NOT FAILED FOR TEAM: ")NET_PRINT_INT(ivehTeam) NET_PRINT(" BECAUSE THEY friend cap veh: ") NET_PRINT_INT(iveh) NET_NL()
																			bwasobjective[iTeam] = TRUE
																		ENDIF
																	ENDIF
																ENDIF
															ENDFOR
														ENDIF
														FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
															PRINTLN("[RCC MISSION][PROCESS_VEH_BRAIN][VEHICLE CAPTURE] - ivehTeam: ", ivehTeam)
															IF iteam!=ivehTeam
																IF SHOULD_VEH_CLEAN_UP_ON_DELIVERY( iVeh, ivehTeam )
																	IF IS_BIT_SET(MC_serverBD.iVehteamFailBitset[iveh],iteam)
																		IF ipriority[iTeam] < FMMC_MAX_RULES
																		OR IS_BIT_SET(MC_serverBD.iMissionCriticalVeh[iTeam], iveh)
																			IF SHOULD_VEH_CAUSE_FAIL(iVeh, iTeam, iPriority[iTeam])
																				MC_serverBD.iEntityCausingFail[iteam] = iveh
																				SET_TEAM_FAILED(iteam,mFail_VEH_CAPTURED)
																				NET_PRINT("[RCC MISSION][PROCESS_VEH_BRAIN] MISSION FAILED FOR TEAM: ") NET_PRINT_INT(iTeam)  NET_PRINT("  BECAUSE VEH CAPTURED: ") NET_PRINT_INT(iveh) NET_NL()
																			ENDIF
																		ENDIF
																		CLEAR_BIT(MC_serverBD.iVehteamFailBitset[iveh],iteam)
																		bwasobjective[iTeam] = TRUE
																	ENDIF
																	IF DOES_TEAM_LIKE_TEAM(iteam,ivehTeam)
																		SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iTeam],TRUE)
																		PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE,iveh,iTeam,TRUE)
																	ELSE
																		SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iTeam],FALSE)
																		PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE,iveh,iTeam,FALSE)
																	ENDIF
																	MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_VEH_CAPTURED
																ENDIF
															ELSE
																PRINTLN("[RCC MISSION][PROCESS_VEH_BRAIN][VEHICLE CAPTURE] - SET_TEAM_OBJECTIVE_OUTCOME ")
																SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iTeam],TRUE)
																PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE,iveh,iTeam,TRUE)
																MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_VEH_CAPTURED
															ENDIF

														ENDFOR
													ENDIF
												ENDIF
												IF MC_serverBD_4.iVehPriority[iveh][ivehTeam] < FMMC_MAX_RULES
													IF MC_serverBD.iRuleReCapture[ivehTeam][MC_serverBD_4.iVehPriority[iveh][ivehTeam]] !=UNLIMITED_CAPTURES_KILLS
														MC_serverBD.iRuleReCapture[ivehTeam][MC_serverBD_4.iVehPriority[iveh][ivehTeam]]--
													ENDIF
												ENDIF
												FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
													IF bwasobjective[iTeam]
														IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_VEHICLE,iteam,ipriority[iTeam])
															SET_LOCAL_OBJECTIVE_END_MESSAGE(iteam,ipriority[iTeam],MC_serverBD.iReasonForObjEnd[iTeam])
															BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iteam],iteam,ipriority[iTeam],DID_TEAM_PASS_OBJECTIVE(iteam,ipriority[iTeam]))
														ENDIF
													ENDIF
												ENDFOR

												NET_PRINT("[RCC MISSION][PROCESS_VEH_BRAIN]Control complete for veh: ")  NET_PRINT_INT(iveh) NET_NL()
												RESET_NET_TIMER(MC_serverBD_1.tdControlVehTimer[iveh])
												iStoredVehCaptureMS[ivehTeam][iVeh] = 0
											ELSE
												//update percentage capture
												//IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_RETAIN_CAPTURE_PROGRESS)
													//IF bIsLocalPlayerHost
														//iStoredVehCaptureMS[ivehTeam][iVeh] =  (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.tdControlVehTimer[iveh]))
														//PRINTLN("[RCC MISSION][VEHICLE CAPTURE] iStoredVehCaptureMS[ivehTeam][iVeh] ",iStoredVehCaptureMS[ivehTeam][iVeh])
												//	ENDIF
											//	ENDIF
											ENDIF
										ELSE
											IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.tdControlVehTimer[iveh]) > MC_serverBD.iVehTakeoverTime[ivehTeam][MC_serverBD_4.iVehPriority[iveh][ivehTeam]]   
											
												BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM_CON_VEH,0,ivehTeam,-1,tempplayer,ci_TARGET_VEHICLE,iveh)
												
												IF MC_serverBD_4.iVehPriority[iveh][ivehTeam] < FMMC_MAX_RULES
													//BROADCAST_GIVE_PLAYER_XP(tempPlayer,200,-1,TRUE)
													INCREMENT_SERVER_REMOTE_PLAYER_SCORE(tempplayer, GET_FMMC_POINTS_FOR_TEAM(ivehTeam,MC_serverBD_4.iVehPriority[iveh][ivehTeam]), ivehTeam, MC_serverBD_4.iVehPriority[iveh][ivehTeam])
													
													IF MC_serverBD.iRuleReCapture[ivehTeam][MC_serverBD_4.iVehPriority[iveh][ivehTeam]] <= 0
														IF SHOULD_VEH_CLEAN_UP_ON_DELIVERY( iVeh, ivehTeam )
															PRINTLN("[RCC MISSION][PROCESS_VEH_BRAIN] cleaning up vehicle for control objective, ",iveh)
															CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
															CLEANUP_VEHICLE_CARGO(iveh)
															//MC_serverBD_1.sFMMC_SBD.niVehicle[iveh] = NULL
														ENDIF
													ENDIF
													IF GET_VEHICLE_RESPAWNS(iVeh) <= 0 	
													AND MC_serverBD.iRuleReCapture[ivehTeam][MC_serverBD_4.iVehPriority[iveh][ivehTeam]] <=0
														
														IF SHOULD_VEH_CLEAN_UP_ON_DELIVERY( iVeh, ivehTeam )
															FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
																ipriority[iTeam] =MC_serverBD_4.iVehPriority[iveh][iTeam]
																IF iteam = ivehTeam
																	IF IS_BIT_SET(MC_serverBD.iVehteamFailBitset[iveh],iteam)
																		CLEAR_BIT(MC_serverBD.iVehteamFailBitset[iveh],iteam)
																		CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iTeam], iveh)
																		NET_PRINT("[RCC MISSION][PROCESS_VEH_BRAIN] OBJECTIVE NOT FAILED FOR TEAM: ")NET_PRINT_INT(ivehTeam) NET_PRINT(" BECAUSE THEY CAPTURED VEH: ") NET_PRINT_INT(iveh) NET_NL()
																		bwasobjective[iTeam] = TRUE
																	ENDIF
																ELSE
																	IF DOES_TEAM_LIKE_TEAM(iteam,ivehTeam)
																		IF IS_BIT_SET(MC_serverBD.iVehteamFailBitset[iveh],iteam)
																			CLEAR_BIT(MC_serverBD.iVehteamFailBitset[iveh],iteam)
																			CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iTeam], iveh)
																			NET_PRINT("[RCC MISSION][PROCESS_VEH_BRAIN] OBJECTIVE NOT FAILED FOR TEAM: ")NET_PRINT_INT(ivehTeam) NET_PRINT(" BECAUSE THEY friend cap veh: ") NET_PRINT_INT(iveh) NET_NL()
																			bwasobjective[iTeam] = TRUE
																		ENDIF
																	ENDIF
																ENDIF
															ENDFOR
														ENDIF
														FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
															IF iteam!=ivehTeam
																IF SHOULD_VEH_CLEAN_UP_ON_DELIVERY( iVeh, ivehTeam )
																	IF IS_BIT_SET(MC_serverBD.iVehteamFailBitset[iveh],iteam)
																		IF ipriority[iTeam] < FMMC_MAX_RULES
																		OR IS_BIT_SET(MC_serverBD.iMissionCriticalVeh[iTeam], iveh)
																			IF SHOULD_VEH_CAUSE_FAIL(iVeh, iTeam, iPriority[iTeam])
																				MC_serverBD.iEntityCausingFail[iteam] = iveh
																				SET_TEAM_FAILED(iteam,mFail_VEH_CAPTURED)
																				NET_PRINT("[RCC MISSION][PROCESS_VEH_BRAIN] MISSION FAILED FOR TEAM: ") NET_PRINT_INT(iTeam)  NET_PRINT("  BECAUSE VEH CAPTURED: ") NET_PRINT_INT(iveh) NET_NL()
																			ENDIF
																		ENDIF
																		CLEAR_BIT(MC_serverBD.iVehteamFailBitset[iveh],iteam)
																		bwasobjective[iTeam] = TRUE
																	ENDIF
																	IF DOES_TEAM_LIKE_TEAM(iteam,ivehTeam)
																		SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iTeam],TRUE)
																		PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE,iveh,iTeam,TRUE)
																	ELSE
																		SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iTeam],FALSE)
																		PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE,iveh,iTeam,FALSE)
																	ENDIF
																	MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_VEH_CAPTURED
																ENDIF
															ELSE
																SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iTeam],TRUE)
																PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE,iveh,iTeam,TRUE)
																MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_VEH_CAPTURED
															ENDIF

														ENDFOR
													ENDIF
												ENDIF
												IF MC_serverBD_4.iVehPriority[iveh][ivehTeam] < FMMC_MAX_RULES
													IF MC_serverBD.iRuleReCapture[ivehTeam][MC_serverBD_4.iVehPriority[iveh][ivehTeam]] !=UNLIMITED_CAPTURES_KILLS
														MC_serverBD.iRuleReCapture[ivehTeam][MC_serverBD_4.iVehPriority[iveh][ivehTeam]]--
													ENDIF
												ENDIF
												FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
													IF bwasobjective[iTeam]
														IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_VEHICLE,iteam,ipriority[iTeam])
															SET_LOCAL_OBJECTIVE_END_MESSAGE(iteam,ipriority[iTeam],MC_serverBD.iReasonForObjEnd[iTeam])
															BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iteam],iteam,ipriority[iTeam],DID_TEAM_PASS_OBJECTIVE(iteam,ipriority[iTeam]))
														ENDIF
													ENDIF
												ENDFOR

												NET_PRINT("[RCC MISSION][PROCESS_VEH_BRAIN] Control complete for veh: ")  NET_PRINT_INT(iveh) NET_NL()
												RESET_NET_TIMER(MC_serverBD_1.tdControlVehTimer[iveh])
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								RESET_NET_TIMER(MC_serverBD_1.tdControlVehTimer[iveh])
							ENDIF
						ELSE
							MC_serverBD.iDriverPart[iveh] = -1
							RESET_NET_TIMER(MC_serverBD_1.tdControlVehTimer[iveh])
						ENDIF
					ELSE //from here on add setting checks and cache capture prog
						MC_serverBD.iDriverPart[iveh] = -1
						RESET_NET_TIMER(MC_serverBD_1.tdControlVehTimer[iveh])
//						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_RETAIN_CAPTURE_PROGRESS)
//							IF bIsLocalPlayerHost
//								MC_serverBD_1.iStoredVehCaptureMS[ivehTeam][iVeh] = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.tdControlVehTimer[iveh]) / MC_serverBD.iVehTakeoverTime[iTeam][iVeh]
//								PRINTLN("[RCC MISSION][VEHICLE CAPTURE] - Host storing vehicle capture percentage -1- MC_serverBD_1.iStoredVehCaptureMS[",iTeam, "][",iveh,"] =",MC_serverBD_1.iStoredVehCaptureMS[iTeam][iVeh] )
//							ENDIF
//						ENDIF
					ENDIF
				ELSE
					MC_serverBD.iDriverPart[iveh] = -1
					RESET_NET_TIMER(MC_serverBD_1.tdControlVehTimer[iveh])
//					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_RETAIN_CAPTURE_PROGRESS)
//						IF bIsLocalPlayerHost
//							MC_serverBD_1.iStoredVehCaptureMS[ivehTeam][iVeh] = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.tdControlVehTimer[iveh]) / MC_serverBD.iVehTakeoverTime[iTeam][iVeh]
//							PRINTLN("[RCC MISSION][VEHICLE CAPTURE] - Host storing vehicle capture percentage -2- MC_serverBD_1.iStoredVehCaptureMS[",iTeam, "][",iveh,"] =",MC_serverBD_1.iStoredVehCaptureMS[iTeam][iVeh] )
//						ENDIF
//					ENDIF
				ENDIF
			ELSE
				MC_serverBD.iDriverPart[iveh] = -1
				RESET_NET_TIMER(MC_serverBD_1.tdControlVehTimer[iveh])
//				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_RETAIN_CAPTURE_PROGRESS)
//					IF bIsLocalPlayerHost
//						MC_serverBD_1.iStoredVehCaptureMS[ivehTeam][iVeh] = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.tdControlVehTimer[iveh]) / MC_serverBD.iVehTakeoverTime[iTeam][iVeh]
//						PRINTLN("[RCC MISSION][VEHICLE CAPTURE] - Host storing vehicle capture percentage -3- MC_serverBD_1.iStoredVehCaptureMS[",iTeam, "][",iveh,"] =",MC_serverBD_1.iStoredVehCaptureMS[iTeam][iVeh] )
//					ENDIF
//				ENDIF
			ENDIF
		ELSE
			MC_serverBD.iDriverPart[iveh] = -1
			RESET_NET_TIMER(MC_serverBD_1.tdControlVehTimer[iveh])
//			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_RETAIN_CAPTURE_PROGRESS)
//				IF bIsLocalPlayerHost
//					MC_serverBD_1.iStoredVehCaptureMS[ivehTeam][iVeh] = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.tdControlVehTimer[iveh]) / MC_serverBD.iVehTakeoverTime[iTeam][iVeh]
//					PRINTLN("[RCC MISSION][VEHICLE CAPTURE] - Host storing vehicle capture percentage -4- MC_serverBD_1.iStoredVehCaptureMS[",iTeam, "][",iveh,"] =",MC_serverBD_1.iStoredVehCaptureMS[iTeam][iVeh] )
//				ENDIF
//			ENDIF
		ENDIF
	ELSE
		MC_serverBD.iDriverPart[iveh] = -1
		RESET_NET_TIMER(MC_serverBD_1.tdControlVehTimer[iveh])
//		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_RETAIN_CAPTURE_PROGRESS)
//			IF bIsLocalPlayerHost
//				MC_serverBD_1.iStoredVehCaptureMS[ivehTeam][iVeh] = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.tdControlVehTimer[iveh]) / MC_serverBD.iVehTakeoverTime[iTeam][iVeh]
//				PRINTLN("[RCC MISSION][VEHICLE CAPTURE] - Host storing vehicle capture percentage -5- MC_serverBD_1.iStoredVehCaptureMS[",iTeam, "][",iveh,"] =",MC_serverBD_1.iStoredVehCaptureMS[iTeam][iVeh] )
//			ENDIF
//		ENDIF
	ENDIF
	
ENDPROC

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: SERVER VEH CAPTURE RANGE PROCESSING !
//
//************************************************************************************************************************************************************

PROC MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME(INT iVeh)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive, ciFMMC_VEHICLE5_HACKING_USE_DISTANCE_FOR_CAPTURE)	
		PARTICIPANT_INDEX tempPart
		PLAYER_INDEX tempPlayer
		INT iCaptureTeam
		INT iTeam
		INT ipriority[FMMC_MAX_TEAMS]
		BOOL bwasobjective[FMMC_MAX_TEAMS]
		INT iPartThatHasControl = -1
		INT iPartsChecked = 0
		INT iTotalPartsToCheck = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]

		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
			IF NOT IS_BIT_SET(MC_serverBD_4.iVehHackedReadyForDeathBS, iVeh)
			AND NOT IS_BIT_SET(MC_serverBD_4.iVehHackeDeathCountedBS, iVeh)
				FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
					IF MC_serverBD_4.iVehPriority[iVeh][iTeam] = MC_ServerbD_4.iCurrentHighestPriority[iTeam]
						iTempHackingTargetsRemaining++
						BREAKLOOP
					ENDIF
				ENDFOR
			ENDIF
			IF NOT IS_ENTITY_DEAD(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]))
				IF NOT IS_BIT_SET(MC_serverBD_4.iVehHackedReadyForDeathBS, iVeh)
					IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive, ciFMMC_VEHICLE5_HACKING_RESET_CAPTURE_OUT_OF_RANGE)
					AND HAS_NET_TIMER_STARTED(MC_serverBD.tdControlVehTimerOffset[iVeh])
						PRINTLN("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME Timer: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.tdControlVehTimer[iVeh]))
						PRINTLN("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME Offset Timer: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdControlVehTimerOffset[iVeh]))
						MC_serverBD_1.tdControlVehTimer[iVeh].Timer = GET_TIME_OFFSET(MC_serverBD_1.tdControlVehTimer[iVeh].Timer, GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdControlVehTimerOffset[iVeh]))
						PRINTLN("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME New Timer: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.tdControlVehTimer[iVeh]))
						RESET_NET_TIMER(MC_serverBD.tdControlVehTimerOffset[iVeh])
					ENDIF
				
					INT iPartToCheck = -1
					IF NOT IS_BIT_SET(MC_serverBD_4.iCaptureVehRequestPartBS, iVeh)
						//Check who asked to hack
						FOR iPartToCheck = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
							IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartToCheck))
							AND NOT (IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet, PBBOOL_ANY_SPECTATOR)
								OR IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
							AND NOT IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet,PBBOOL_PLAYER_FAIL)
							AND IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
							AND NOT IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet, PBBOOL_FINISHED)
								iPartsChecked++
								IF IS_BIT_SET(MC_playerBD[iPartToCheck].iRequestCaptureVehBS, iVeh)
									SET_BIT(MC_serverBD_4.iCaptureVehRequestPartBS, iVeh)
									iCaptureTeam = MC_playerBD[iPartToCheck].iTeam
									PRINTLN("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME - Participant: ", iPartToCheck," has requested to capture the veh: ", iVeh)
									BREAKLOOP
								ENDIF
								IF iPartsChecked >= iTotalPartsToCheck
									BREAKLOOP
								ENDIF
							ENDIF
						ENDFOR
					ELSE
						BOOL bPartHasControl = FALSE
						FOR iPartToCheck = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
							IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartToCheck))
							AND NOT (IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet, PBBOOL_ANY_SPECTATOR)
								OR IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
							AND NOT IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet,PBBOOL_PLAYER_FAIL)
							AND IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
							AND NOT IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet, PBBOOL_FINISHED)
								iPartsChecked++
								
								IF IS_BIT_SET(MC_playerBD[iPartToCheck].iRequestCaptureVehBS, iVeh)
									iPartThatHasControl = iPartToCheck
									bPartHasControl = TRUE
									PRINTLN("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME - Participant: ", iPartToCheck," is still in control - veh: ", iVeh)
									BREAKLOOP
								ENDIF
								
								IF iPartsChecked >= iTotalPartsToCheck
									BREAKLOOP
								ENDIF
							ENDIF
						ENDFOR
						IF !bPartHasControl
							PRINTLN("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME - Participant: ", iPartToCheck, " has gone out of range, clearing bits - veh: ", iVeh)
							CLEAR_BIT(MC_serverBD_4.iCaptureVehRequestPartBS, iVeh)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(MC_serverBD_4.iVehHackedReadyForDeathBS, iVeh)
					PRINTLN("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME - DESTROYED veh: ", iVeh)
					FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
						iPriority[iTeam] = MC_serverBD_4.iVehPriority[iVeh][iTeam]
						IF MC_serverBD_4.ivehRule[iVeh][iTeam] = FMMC_OBJECTIVE_LOGIC_CAPTURE	
							SET_TEAM_OBJECTIVE_OUTCOME(iTeam, iPriority[iTeam], TRUE)
							PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE, iVeh, iTeam, TRUE)
							MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_VEH_CAPTURED
							IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_VEHICLE, iTeam, iPriority[iTeam])
								SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam, iPriority[iTeam], MC_serverBD.iReasonForObjEnd[iTeam])
								BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam], iTeam, iPriority[iTeam], DID_TEAM_PASS_OBJECTIVE(iTeam,iPriority[iTeam]))
							ENDIF
						ENDIF
					ENDFOR
					CLEAR_BIT(MC_serverBD_4.iVehHackedReadyForDeathBS, iVeh)
					SET_BIT(MC_serverBD_4.iVehHackeDeathCountedBS, iVeh)
				ENDIF
				EXIT
			ENDIF
		ENDIF

		IF NOT IS_BIT_SET(MC_serverBD_4.iVehExplodeKillOnCaptureBS, iVeh)
			IF iPartThatHasControl != -1
				tempPart = INT_TO_PARTICIPANTINDEX(iPartThatHasControl)
			
				IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				AND NOT IS_BIT_SET(MC_playerBD[iPartThatHasControl].iClientBitSet, PBBOOL_FINISHED)
					tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
					
					IF IS_NET_PLAYER_OK(tempPlayer)
						iCaptureTeam = MC_playerBD[iPartThatHasControl].iteam
						
						IF MC_serverBD_4.ivehRule[iVeh][iCaptureTeam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
							IF MC_serverBD_4.iVehPriority[iVeh][iCaptureTeam] < FMMC_MAX_RULES
								IF NOT HAS_NET_TIMER_STARTED(MC_serverBD_1.tdControlVehTimer[iVeh])
									START_NET_TIMER(MC_serverBD_1.tdControlVehTimer[iVeh])
									NET_PRINT("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME starting control timer for veh: ")  NET_PRINT_INT(iVeh) NET_NL()
								ELSE
									IF (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.tdControlVehTimer[iVeh]) > MC_serverBD.iVehTakeoverTime[iCaptureTeam][MC_serverBD_4.iVehPriority[iVeh][iCaptureTeam]])
										BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM_CON_VEH, 0, iCaptureTeam, -1, tempPlayer, ci_TARGET_VEHICLE, iVeh)
										
										INCREMENT_SERVER_REMOTE_PLAYER_SCORE(tempPlayer, GET_FMMC_POINTS_FOR_TEAM(iCaptureTeam, MC_serverBD_4.iVehPriority[iVeh][iCaptureTeam]), iCaptureTeam, MC_serverBD_4.iVehPriority[iVeh][iCaptureTeam])
										
										IF MC_serverBD_4.iCurrentHighestPriority[iCaptureTeam] < FMMC_MAX_RULES
										AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iCaptureTeam].iRuleBitsetEleven[MC_serverBD_4.iCurrentHighestPriority[iCaptureTeam]], ciBS_RULE11_SECUROHACK_REQUIRES_KILL)
											PRINTLN("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME - Time to destroy veh: ", iVeh)
											SET_BIT(MC_serverBD_4.iVehHackedReadyForDeathBS, iVeh)
										ENDIF
										
										IF GET_VEHICLE_RESPAWNS(iVeh) <= 0	
										AND MC_serverBD.iRuleReCapture[iCaptureTeam][MC_serverBD_4.iVehPriority[iVeh][iCaptureTeam]] <= 0
											IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive, ciFMMC_VEHICLE5_HACKING_EXPLODE_ON_CAPTURE)
											OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSeven, ciFMMC_VEHICLE7_REMOVE_CRITICAL_ON_HACK)
											OR (MC_serverBD_4.iCurrentHighestPriority[iCaptureTeam] < FMMC_MAX_RULES
											AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iCaptureTeam].iRuleBitsetEleven[MC_serverBD_4.iCurrentHighestPriority[iCaptureTeam]], ciBS_RULE11_SECUROHACK_REQUIRES_KILL))
												FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
													iPriority[iTeam] = MC_serverBD_4.iVehPriority[iVeh][iTeam]
													
													IF iTeam = iCaptureTeam
														IF IS_BIT_SET(MC_serverBD.iVehteamFailBitset[iVeh], iTeam)
															CLEAR_BIT(MC_serverBD.iVehteamFailBitset[iVeh], iTeam)
															CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iTeam], iVeh)
															NET_PRINT("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME OBJECTIVE NOT FAILED FOR TEAM: ")NET_PRINT_INT(iCaptureTeam) NET_PRINT(" BECAUSE THEY CAPTURED VEH: ") NET_PRINT_INT(iVeh) NET_NL()
															bWasObjective[iTeam] = TRUE
														ENDIF
													ELSE
														IF DOES_TEAM_LIKE_TEAM(iTeam, iCaptureTeam)
														AND MC_serverBD_4.ivehRule[iVeh][iTeam] = FMMC_OBJECTIVE_LOGIC_CAPTURE	
															IF IS_BIT_SET(MC_serverBD.iVehteamFailBitset[iVeh], iTeam)
																CLEAR_BIT(MC_serverBD.iVehteamFailBitset[iVeh], iTeam)
																CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iTeam], iVeh)
																bWasObjective[iTeam] = TRUE
															ENDIF
														ENDIF
													ENDIF
												ENDFOR
											ENDIF
											IF MC_serverBD_4.iCurrentHighestPriority[iCaptureTeam] < FMMC_MAX_RULES
												IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iCaptureTeam].iRuleBitsetEleven[MC_serverBD_4.iCurrentHighestPriority[iCaptureTeam]], ciBS_RULE11_SECUROHACK_REQUIRES_KILL)
													FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
														IF iTeam != iCaptureTeam
															IF MC_serverBD_4.ivehRule[iVeh][iTeam] = FMMC_OBJECTIVE_LOGIC_CAPTURE	
																IF DOES_TEAM_LIKE_TEAM(iTeam, iCaptureTeam)
																	SET_TEAM_OBJECTIVE_OUTCOME(iTeam, iPriority[iTeam], TRUE)
																	PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE, iVeh, iTeam, TRUE)
																ELSE
																	SET_TEAM_OBJECTIVE_OUTCOME(iTeam, iPriority[iTeam], FALSE)
																	PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE, iVeh, iTeam, FALSE)
																ENDIF
															ENDIF
														ELSE
															SET_TEAM_OBJECTIVE_OUTCOME(iTeam, iPriority[iTeam], TRUE)
															PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE, iVeh, iTeam, TRUE)
														ENDIF
														
														MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_VEH_CAPTURED
													ENDFOR
												ENDIF
											ENDIF
										ENDIF
										
										IF MC_serverBD_4.iVehPriority[iVeh][iCaptureTeam] < FMMC_MAX_RULES
											IF MC_serverBD.iRuleReCapture[iCaptureTeam][MC_serverBD_4.iVehPriority[iVeh][iCaptureTeam]] != UNLIMITED_CAPTURES_KILLS
												MC_serverBD.iRuleReCapture[iCaptureTeam][MC_serverBD_4.iVehPriority[iVeh][iCaptureTeam]]--
											ENDIF
										ENDIF
										
										IF MC_serverBD_4.iCurrentHighestPriority[iCaptureTeam] < FMMC_MAX_RULES
											IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iCaptureTeam].iRuleBitsetEleven[MC_serverBD_4.iCurrentHighestPriority[iCaptureTeam]], ciBS_RULE11_SECUROHACK_REQUIRES_KILL)
												FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
													IF bWasObjective[iTeam] 
														IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_VEHICLE, iTeam, iPriority[iTeam])
															SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam, iPriority[iTeam], MC_serverBD.iReasonForObjEnd[iTeam])
															BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam], iTeam, iPriority[iTeam], DID_TEAM_PASS_OBJECTIVE(iTeam,iPriority[iTeam]))
														ENDIF
													ENDIF
												ENDFOR
											ENDIF
										ENDIF
										
										IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive, ciFMMC_VEHICLE5_HACKING_EXPLODE_ON_CAPTURE)
											SET_BIT(MC_serverBD_4.iVehExplodeKillOnCaptureBS, iVeh)
										ENDIF
										
										SET_BIT(MC_serverBD_4.iVehUnlockOnCapture, iVeh)
										
										MC_serverBD_4.iCaptureTimestamp = 0

										NET_PRINT("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME Control complete for veh: ")  NET_PRINT_INT(iVeh) NET_NL()
										RESET_NET_TIMER(MC_serverBD_1.tdControlVehTimer[iVeh])
										RESET_NET_TIMER(MC_serverBD.tdControlVehTimerOffset[iVeh])
									ENDIF
								ENDIF
							ELSE
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive,  ciFMMC_VEHICLE5_HACKING_RESET_CAPTURE_OUT_OF_RANGE)
									RESET_NET_TIMER(MC_serverBD_1.tdControlVehTimer[iVeh])
								ELSE
									IF NOT HAS_NET_TIMER_STARTED(MC_serverBD.tdControlVehTimerOffset[iVeh])
									AND HAS_NET_TIMER_STARTED(MC_serverBD_1.tdControlVehTimer[iVeh])
										PRINTLN("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME - veh ", iVeh," starting offset timer 5")
										PRINTLN("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME Timer: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.tdControlVehTimer[iVeh]))
										REINIT_NET_TIMER(MC_serverBD.tdControlVehTimerOffset[iVeh])
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive,  ciFMMC_VEHICLE5_HACKING_RESET_CAPTURE_OUT_OF_RANGE)
								RESET_NET_TIMER(MC_serverBD_1.tdControlVehTimer[iVeh])
							ELSE
								IF NOT HAS_NET_TIMER_STARTED(MC_serverBD.tdControlVehTimerOffset[iVeh])
								AND HAS_NET_TIMER_STARTED(MC_serverBD_1.tdControlVehTimer[iVeh])
									PRINTLN("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME - veh ", iVeh," starting offset timer 4")
									PRINTLN("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME Timer: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.tdControlVehTimer[iVeh]))
									REINIT_NET_TIMER(MC_serverBD.tdControlVehTimerOffset[iVeh])
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive,  ciFMMC_VEHICLE5_HACKING_RESET_CAPTURE_OUT_OF_RANGE)
							RESET_NET_TIMER(MC_serverBD_1.tdControlVehTimer[iVeh])
						ELSE
							IF NOT HAS_NET_TIMER_STARTED(MC_serverBD.tdControlVehTimerOffset[iVeh])
							AND HAS_NET_TIMER_STARTED(MC_serverBD_1.tdControlVehTimer[iVeh])
								PRINTLN("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME - veh ", iVeh," starting offset timer 3")
								PRINTLN("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME Timer: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.tdControlVehTimer[iVeh]))
								REINIT_NET_TIMER(MC_serverBD.tdControlVehTimerOffset[iVeh])
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive,  ciFMMC_VEHICLE5_HACKING_RESET_CAPTURE_OUT_OF_RANGE)
						RESET_NET_TIMER(MC_serverBD_1.tdControlVehTimer[iVeh])
					ELSE
						IF NOT HAS_NET_TIMER_STARTED(MC_serverBD.tdControlVehTimerOffset[iVeh])
						AND HAS_NET_TIMER_STARTED(MC_serverBD_1.tdControlVehTimer[iVeh])
							PRINTLN("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME - veh ", iVeh," starting offset timer 2")
							PRINTLN("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME Timer: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.tdControlVehTimer[iVeh]))
							REINIT_NET_TIMER(MC_serverBD.tdControlVehTimerOffset[iVeh])
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive,  ciFMMC_VEHICLE5_HACKING_RESET_CAPTURE_OUT_OF_RANGE)
					RESET_NET_TIMER(MC_serverBD_1.tdControlVehTimer[iVeh])
				ELSE
					IF NOT HAS_NET_TIMER_STARTED(MC_serverBD.tdControlVehTimerOffset[iVeh])
					AND HAS_NET_TIMER_STARTED(MC_serverBD_1.tdControlVehTimer[iVeh])
						PRINTLN("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME - veh ", iVeh," starting offset timer 1")
						PRINTLN("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME Timer: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.tdControlVehTimer[iVeh]))
						REINIT_NET_TIMER(MC_serverBD.tdControlVehTimerOffset[iVeh])
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: CLIENT VEH CAPTURE RANGE PROCESSING !
//
//************************************************************************************************************************************************************

PROC PROCESS_VEH_CAPTURE_RANGE(INT iVeh)
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
		IF NOT IS_ENTITY_DEAD(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]))
			IF IS_BIT_SET(MC_serverBD_4.iVehHackedReadyForDeathBS, iVeh)
			AND (MC_playerBD[iPartToUse].iVehFollowing = iVeh OR IS_BIT_SET(iHackingVehInRangeBitSet, iVeh))
			AND NOT IS_BIT_SET(MC_PlayerBD[iLocalPart].iVehDestroyBitset, iVeh)	
				PRINTLN("PROCESS_VEH_CAPTURE_RANGE - We need to destroy veh: ", iVeh)
				SET_BIT(MC_PlayerBD[iLocalPart].iVehDestroyBitset, iVeh)
				MC_playerBD[iLocalPart].iVehFollowing = -1
				CLEAR_BIT(MC_playerBD[iLocalPart].iRequestCaptureVehBS, iVeh)
			ENDIF
			
			IF IS_BIT_SET(MC_serverBD_4.iVehHackedReadyForDeathBS, iVeh)
				SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]), TRUE)
			ENDIF
			
			IF IS_BIT_SET(MC_PlayerBD[iPartToUse].iVehDestroyBitset, iVeh)
				IF IS_BIT_SET(iHackingVehInRangeBitSet, iVeh)
					CLEAR_BIT(iHackingVehInRangeBitSet, iVeh)
				ENDIF
				IF DOES_BLIP_EXIST(biVehCaptureRangeBlip[iVeh])
					REMOVE_BLIP(biVehCaptureRangeBlip[iVeh])
				ENDIF
				IF IS_BIT_SET(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
					STOP_SOUND(iHackingLoopSoundID)
					RELEASE_SOUND_ID(iHackingLoopSoundID)
					iHackingLoopSoundID = -1
					CLEAR_BIT(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
				ENDIF
				EXIT
			ENDIF
			
			IF IS_BIT_SET(MC_serverBD_4.iVehUnlockOnCapture, iVeh)
				bShouldStartControl = FALSE
				IF IS_BIT_SET(iHackingVehInRangeBitSet, iVeh)
					CLEAR_BIT(iHackingVehInRangeBitSet, iVeh)
				ENDIF
				IF DOES_BLIP_EXIST(biVehCaptureRangeBlip[iVeh])
					REMOVE_BLIP(biVehCaptureRangeBlip[iVeh])
				ENDIF
				IF NOT IS_BIT_SET(MC_serverBD_4.iVehHackedReadyForDeathBS, iVeh)
					IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive, ciFMMC_VEHICLE5_HACKING_EXPLODE_ON_CAPTURE)
						IF bIsLocalPlayerHost
							VEHICLE_INDEX viTemp = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
							IF NETWORK_HAS_CONTROL_OF_ENTITY(viTemp)
								SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(viTemp, FALSE)
							ELSE
								NETWORK_REQUEST_CONTROL_OF_ENTITY(viTemp)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(MC_serverBD_4.iVehExplodeKillOnCaptureBS, iVeh)
			AND NOT IS_BIT_SET(MC_serverBD_4.iVehUnlockOnCapture, iVeh)
				
				FLOAT fDistanceToHackVeh = VDIST2(GET_ENTITY_COORDS(LocalPlayerPed), GET_ENTITY_COORDS(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])))
				IF fDistanceToHackVeh < POW(TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCaptureRange), 2)
					IF NOT IS_BIT_SET(iHackingVehInRangeBitSet, iVeh)
						SET_BIT(iHackingVehInRangeBitSet, iVeh)
					ENDIF
					IF NOT IS_BIT_SET(MC_serverBD_4.iCaptureVehRequestPartBS, iVeh)
						IF NOT bShouldStartControl
							IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iAlreadyBeenCapturingVehBS, iVeh)
								IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appSecuroHack")) = 0
									IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING)
									AND iHackStage != ciSECUROSERV_HACK_STAGE_HACK_COMPLETE
										IF IS_PHONE_ONSCREEN()
											IF IS_THIS_PRINT_BEING_DISPLAYED("HACK_HELP2") //Should be IS_THIS_HELP_MESSAGE_BEING_DISPLAYED - Change if refactored
												CLEAR_HELP()
											ENDIF
											
											IF NOT IS_THIS_PRINT_BEING_DISPLAYED("HACK_HELP3") //Should be IS_THIS_HELP_MESSAGE_BEING_DISPLAYED - Change if refactored
												PRINT_HELP_FOREVER("HACK_HELP3")
											ENDIF
										ELSE
											IF IS_THIS_PRINT_BEING_DISPLAYED("HACK_HELP3") //Should be IS_THIS_HELP_MESSAGE_BEING_DISPLAYED - Change if refactored
												CLEAR_HELP()
											ENDIF
											
											IF NOT IS_THIS_PRINT_BEING_DISPLAYED("HACK_HELP2") //Should be IS_THIS_HELP_MESSAGE_BEING_DISPLAYED - Change if refactored
											AND NOT IS_PED_INJURED(LocalPlayerPed)
												PRINT_HELP_FOREVER("HACK_HELP2")
											ENDIF
										ENDIF
									ELSE
										IF NOT IS_THIS_PRINT_BEING_DISPLAYED("HACK_HELP") //Should be IS_THIS_HELP_MESSAGE_BEING_DISPLAYED - Change if refactored
											PRINT_HELP_FOREVER("HACK_HELP")
										ENDIF
									ENDIF
								ENDIF
								SET_BIT(iHackingVehHelpBitSet, iVeh)
							ENDIF
						ENDIF

						IF bShouldStartControl
						OR IS_BIT_SET(MC_playerBD[iPartToUse].iAlreadyBeenCapturingVehBS, iVeh)
							PRINTLN("PROCESS_VEH_CAPTURE_RANGE - Setting request capture bit, asking server to let this player capture the veh")
							iHackPercentage = 0
							IF MC_playerBD[iLocalPart].iVehFollowing != iVeh
								MC_playerBD[iLocalPart].iVehFollowing = iVeh
							ENDIF
							SET_BIT(MC_playerBD[iPartToUse].iRequestCaptureVehBS, iVeh)
							SET_BIT(MC_playerBD[iPartToUse].iAlreadyBeenCapturingVehBS, iVeh)
							bShouldStartControl = FALSE
							IF IS_BIT_SET(iHackingVehHelpBitSet, iVeh)
								CLEAR_HELP()
								CLEAR_BIT(iHackingVehHelpBitSet, iVeh)
							ENDIF
						ENDIF
					ELSE
						IF IS_BIT_SET(MC_playerBD[iPartToUse].iRequestCaptureVehBS, iVeh)
						AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciSECUROSERV_DONT_FORCE_ON_SCREEN)	
							IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH("appSecuroHack")) = 0
							AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
							AND NOT	IS_PLAYER_RESPAWNING(LocalPlayer)
							AND NOT IS_PED_DEAD_OR_DYING(LocalPlayerPed)
								PRINTLN("[JS][SECUROHACK] relaunching app")
								LAUNCH_CELLPHONE_APPLICATION( AppDummyApp0, TRUE, TRUE, FALSE )
							ENDIF
						ENDIF
						IF IS_BIT_SET(iHackingVehHelpBitSet, iVeh)
							CLEAR_HELP()
							CLEAR_BIT(iHackingVehHelpBitSet, iVeh)
						ENDIF
						bShouldStartControl = FALSE
						
						IF IS_BIT_SET(MC_playerBD[iPartToUse].iRequestCaptureVehBS, iVeh)
						
							FLOAT fLosingSignalAreaSize = 0.15
							FLOAT fRange = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCaptureRange)
							
							fRange = fRange - (fRange * fLosingSignalAreaSize)
							
							//DIST CHECK
							IF fDistanceToHackVeh > POW(fRange, 2)
								SET_BIT(iLocalBoolCheck26, LBOOL26_SERVUROSERV_LOSING_SIGNAL)
								PRINTLN("[TMS][SECUROHACK] Setting losing signal bit")
							ELSE
								CLEAR_BIT(iLocalBoolCheck26, LBOOL26_SERVUROSERV_LOSING_SIGNAL)
								PRINTLN("[TMS][SECUROHACK] Clearing losing signal bit")
							ENDIF
						ENDIF
					ENDIF
					
					IF DOES_BLIP_EXIST(biVehCaptureRangeBlip[iVeh])
						SET_BLIP_ALPHA(biVehCaptureRangeBlip[iVeh], 50)
					ENDIF
				ELSE
					IF IS_BIT_SET(iHackingVehInRangeBitSet, iVeh)
						CLEAR_BIT(iHackingVehInRangeBitSet, iVeh)
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING)
							PRINTLN("[SECUROHACK] Reseting hack stage")
							
							IF NOT IS_HACK_STOP_SOUND_BLOCKED()
								IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
									PLAY_SOUND_FRONTEND(-1, "Hack_Stop", "dlc_xm_deluxos_hacking_Hacking_Sounds")
								ELSE
									PLAY_SOUND_FRONTEND(-1, "Hack_Stop", "DLC_IE_SVM_Voltic2_Hacking_Sounds")
								ENDIF
							ENDIF
							
							iHackStage = ciSECUROSERV_HACK_STAGE_NO_SIGNAL
						ENDIF
					ENDIF
					
					IF IS_BIT_SET(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
					AND iHackStage = ciSECUROSERV_HACK_STAGE_NO_SIGNAL
						STOP_SOUND(iHackingLoopSoundID)
						RELEASE_SOUND_ID(iHackingLoopSoundID)
						iHackingLoopSoundID = -1
						CLEAR_BIT(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
					ENDIF
					
					IF IS_BIT_SET(iHackingVehHelpBitSet, iVeh)
						CLEAR_HELP()
						CLEAR_BIT(iHackingVehHelpBitSet, iVeh)
					ENDIF
					
					IF IS_BIT_SET(MC_playerBD[iPartToUse].iRequestCaptureVehBS, iVeh)
						PRINTLN("PROCESS_VEH_CAPTURE_RANGE - Clearing the capture request bit for local player as they have gone out of range - range is: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCaptureRange)
						CLEAR_BIT(MC_playerBD[iPartToUse].iRequestCaptureVehBS, iVeh)
						IF MC_playerBD[iLocalPart].iVehFollowing = iVeh
							MC_playerBD[iLocalPart].iVehFollowing = -1
						ENDIF
						bShouldStartControl = FALSE
					ENDIF
					
					IF DOES_BLIP_EXIST(biVehCaptureRangeBlip[iVeh])
						SET_BLIP_ALPHA(biVehCaptureRangeBlip[iVeh], 255)
					ENDIF
				ENDIF
				IF DOES_BLIP_EXIST(biVehBlip[iVeh])
					IF NOT DOES_BLIP_EXIST(biVehCaptureRangeBlip[iVeh])
						biVehCaptureRangeBlip[iVeh] = ADD_BLIP_FOR_RADIUS(GET_ENTITY_COORDS(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]), FALSE), TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCaptureRange))
						SET_BLIP_COLOUR(biVehCaptureRangeBlip[iVeh], BLIP_COLOUR_YELLOW)
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(biVehCaptureRangeBlip[iVeh])
						REMOVE_BLIP(biVehCaptureRangeBlip[iVeh])
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iVehDestroyBitset, iVeh)
				CLEAR_BIT(MC_PlayerBD[iLocalPart].iVehDestroyBitset, iVeh)
			ENDIF
			IF MC_playerBD[iLocalPart].iVehFollowing = iVeh
				PRINTLN("[JS] iVehFollowing cleared (3). Was: ", iVeh)
				MC_playerBD[iLocalPart].iVehFollowing = -1
				IF iHackStage != ciSECUROSERV_HACK_STAGE_HACK_COMPLETE
					iHackStage = ciSECUROSERV_HACK_STAGE_NO_SIGNAL
				ENDIF
			ENDIF
			IF IS_BIT_SET(MC_playerBD[iLocalPart].iRequestCaptureVehBS, iVeh)
				CLEAR_BIT(MC_playerBD[iLocalPart].iRequestCaptureVehBS, iVeh)
			ENDIF
			IF IS_BIT_SET(iHackingVehInRangeBitSet, iVeh)
				CLEAR_BIT(iHackingVehInRangeBitSet, iVeh)
			ENDIF
			IF DOES_BLIP_EXIST(biVehCaptureRangeBlip[iVeh])
				REMOVE_BLIP(biVehCaptureRangeBlip[iVeh])
			ENDIF
			IF IS_BIT_SET(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
				STOP_SOUND(iHackingLoopSoundID)
				RELEASE_SOUND_ID(iHackingLoopSoundID)
				iHackingLoopSoundID = -1
				CLEAR_BIT(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iVehDestroyBitset, iVeh)
			CLEAR_BIT(MC_PlayerBD[iLocalPart].iVehDestroyBitset, iVeh)
		ENDIF
		IF IS_BIT_SET(iHackingVehInRangeBitSet, iVeh)
			CLEAR_BIT(iHackingVehInRangeBitSet, iVeh)
		ENDIF
		IF MC_playerBD[iLocalPart].iVehFollowing = iVeh
			PRINTLN("[JS] iVehFollowing cleared (2). Was: ", iVeh)
			MC_playerBD[iLocalPart].iVehFollowing = -1
			IF iHackStage != ciSECUROSERV_HACK_STAGE_HACK_COMPLETE
				iHackStage = ciSECUROSERV_HACK_STAGE_NO_SIGNAL
			ENDIF
		ENDIF
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iRequestCaptureVehBS, iVeh)
			CLEAR_BIT(MC_playerBD[iLocalPart].iRequestCaptureVehBS, iVeh)
		ENDIF
		IF DOES_BLIP_EXIST(biVehCaptureRangeBlip[iVeh])
			REMOVE_BLIP(biVehCaptureRangeBlip[iVeh])
		ENDIF
		IF IS_BIT_SET(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
			STOP_SOUND(iHackingLoopSoundID)
			RELEASE_SOUND_ID(iHackingLoopSoundID)
			iHackingLoopSoundID = -1
			CLEAR_BIT(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive, ciFMMC_VEHICLE5_HACKING_EXPLODE_ON_CAPTURE)
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
			VEHICLE_INDEX vehToKill = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
			
			IF IS_BIT_SET(MC_serverBD_4.iVehExplodeKillOnCaptureBS, iVeh)
				IF NOT IS_ENTITY_DEAD(vehToKill)
					IF MC_serverBD.iVehteamFailBitset[iVeh] > 0
						IF bIsLocalPlayerHost
							INT iTeam
							
							FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
								CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iTeam], iVeh)
								CLEAR_BIT(MC_serverBD.iVehteamFailBitset[iVeh], iTeam)
							ENDFOR
						ENDIF
					ELSE
						IF bIsLocalPlayerHost
							IF NETWORK_HAS_CONTROL_OF_ENTITY(vehToKill)
								NETWORK_EXPLODE_VEHICLE(vehToKill)
							ELSE
								NETWORK_REQUEST_CONTROL_OF_ENTITY(vehToKill)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF IS_BIT_SET(iHackingVehInRangeBitSet, iVeh)
						CLEAR_BIT(iHackingVehInRangeBitSet, iVeh)
					ENDIF
					
					IF DOES_BLIP_EXIST(biVehCaptureRangeBlip[iVeh])
						REMOVE_BLIP(biVehCaptureRangeBlip[iVeh])
					ENDIF
					
					IF bIsLocalPlayerHost
						CLEAR_BIT(MC_serverBD_4.iVehExplodeKillOnCaptureBS, iVeh)
					ENDIF
				ENDIF
			ELSE
				IF IS_ENTITY_DEAD(vehToKill)
					IF IS_BIT_SET(iHackingVehInRangeBitSet, iVeh)
						CLEAR_BIT(iHackingVehInRangeBitSet, iVeh)
					ENDIF
					
					IF DOES_BLIP_EXIST(biVehCaptureRangeBlip[iVeh])
						REMOVE_BLIP(biVehCaptureRangeBlip[iVeh])
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(biVehCaptureRangeBlip[iVeh])
				REMOVE_BLIP(biVehCaptureRangeBlip[iVeh])
			ENDIF
			
			IF IS_BIT_SET(iHackingVehInRangeBitSet, iVeh)
				CLEAR_BIT(iHackingVehInRangeBitSet, iVeh)
			ENDIF
			
			IF bIsLocalPlayerHost
				CLEAR_BIT(MC_serverBD_4.iVehExplodeKillOnCaptureBS, iVeh)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: CLIENT VEH PROCESSING / VEH BODY !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: VEHICLE BLIPS !
//
//************************************************************************************************************************************************************



FUNC INT GET_TEAM_HOLDING_VEHICLE(INT iVeh)

	INT iteam
	INT ireturnteam = -1
	
	FOR iteam = 0 to (FMMC_MAX_TEAMS-1)
		IF IS_BIT_SET(MC_serverBD.iVehAtYourHolding[iteam],iVeh)
			ireturnteam = iteam
		ENDIF
	ENDFOR
	
	RETURN ireturnteam

ENDFUNC 

FUNC BOOL ARE_ANY_TEAM_PARICIPANTS_IN_VEHICLE(INT iVeh, INT iTeamsBitset)
	INT iLoopIndex
	INT iPlayerCount = 0
	
	//Sum the number of players on all included teams.
	REPEAT FMMC_MAX_TEAMS iLoopIndex
		IF IS_BIT_SET(iTeamsBitset, iLoopIndex)
			iPlayerCount += MC_serverBD.iNumberOfPlayingPlayers[iLoopIndex]
		ENDIF
	ENDREPEAT
	
	INT iPlayersChecked = 0
	REPEAT GET_MAXIMUM_PLAYERS_ON_MISSION() iLoopIndex
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iLoopIndex))
			IF IS_BIT_SET(iTeamsBitset, MC_playerBD[iLoopIndex].iteam)
				//We've found a player in the vehicle. We're done.
				IF MC_playerBD[iLoopIndex].iVehNear = iVeh
					RETURN TRUE
				ENDIF
				iPlayersChecked++
				
				//We've checked as many players as there are on the teams. Early out.
				IF iPlayersChecked >= iPlayerCount
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_ALL_PARTICIPANTS_IN_OTHER_OBJECTIVE_VEHICLE(INT iVeh, VEHICLE_INDEX tempVeh)
	INT iLoopIndex
	IF GET_DIST2_BETWEEN_ENTITIES(tempVeh, LocalPlayerPed) < cfUnblipOtherEntityDistance * cfUnblipOtherEntityDistance
		CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Vehicle ", iveh, " ARE_ALL_PARTICIPANTS_IN_OTHER_OBJECTIVE_VEHICLE - Player is too close. Returning False")
		RETURN FALSE
	ENDIF
	
	INT iVehNear = -1
	REPEAT GET_MAXIMUM_PLAYERS_ON_MISSION() iLoopIndex


		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iLoopIndex))
			RELOOP
		ENDIF
		IF MC_playerBD[iLoopIndex].iVehNear != -1
			IF MC_playerBD[iLoopIndex].iVehNear = iVeh
				CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Vehicle ", iveh, " ARE_ALL_PARTICIPANTS_IN_OTHER_OBJECTIVE_VEHICLE - Player is in this vehicle! Returning False")
				RETURN FALSE
			ENDIF
			IF iVehNear != -1
				IF iVehNear != MC_playerBD[iLoopIndex].iVehNear
					CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Vehicle ", iveh, " ARE_ALL_PARTICIPANTS_IN_OTHER_OBJECTIVE_VEHICLE - All players are not in the same vehicle. Returning False")
					RETURN FALSE
				ENDIF
			ELSE
				iVehNear = MC_playerBD[iLoopIndex].iVehNear
			ENDIF
		ELSE
			CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Vehicle ", iveh, " ARE_ALL_PARTICIPANTS_IN_OTHER_OBJECTIVE_VEHICLE - One player is not in a vehicle. Returning False")
			RETURN FALSE
		ENDIF
	ENDREPEAT
	CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Vehicle ", iveh, " ARE_ALL_PARTICIPANTS_IN_OTHER_OBJECTIVE_VEHICLE - Returning True")
	RETURN TRUE
ENDFUNC

FUNC BOOL ARE_ANY_PARTICIPANTS_IN_VEHICLE(INT iVeh, INT iTeamsBitset, INT &iTeamInVeh)
	INT iLoopIndex
	INT iPlayerCount = 0
	
	iTeamInVeh = -1
	
	REPEAT FMMC_MAX_TEAMS iLoopIndex
		IF IS_BIT_SET(iTeamsBitset, iLoopIndex)
			iPlayerCount += MC_serverBD.iNumberOfPlayingPlayers[iLoopIndex]
		ENDIF
	ENDREPEAT
	
	INT iPlayersChecked = 0
	REPEAT GET_MAXIMUM_PLAYERS_ON_MISSION() iLoopIndex
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iLoopIndex))
		AND NOT (IS_BIT_SET(MC_playerBD[iLoopIndex].iClientBitSet, PBBOOL_ANY_SPECTATOR)
			OR IS_BIT_SET(MC_playerBD[iLoopIndex].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
		AND NOT IS_BIT_SET(MC_playerBD[iLoopIndex].iClientBitSet,PBBOOL_PLAYER_FAIL)
		AND IS_BIT_SET(MC_playerBD[iLoopIndex].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
		AND NOT IS_BIT_SET(MC_playerBD[iLoopIndex].iClientBitSet, PBBOOL_FINISHED)
			PRINTLN("[RCC MISSION] ARE_ANY_PARTICIPANTS_IN_VEHICLE - participant ", iLoopIndex, " is active. OF ", iPlayerCount)
			IF IS_BIT_SET(iTeamsBitset, MC_playerBD[iLoopIndex].iteam)
				PRINTLN("[RCC MISSION] ARE_ANY_PARTICIPANTS_IN_VEHICLE - getting through bit check")
				//We've found a player in the vehicle. We're done.
				
				IF IS_PED_IN_THIS_VEHICLE(GET_PLAYER_PED(INT_TO_PLAYERINDEX(iLoopIndex)), NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]))
				OR (GET_ENTITY_MODEL(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])) = AVENGER
				AND IS_PLAYER_IN_CREATOR_AIRCRAFT(INT_TO_PLAYERINDEX(iLoopIndex)))
					PRINTLN("[RCC MISSION] ARE_ANY_PARTICIPANTS_IN_VEHICLE - RETURNING TRUE for participant: ", iLoopIndex, " vehicle: ", iveh)
					iTeamInVeh = MC_playerBD[iLoopIndex].iteam
					RETURN TRUE
				ENDIF
				iPlayersChecked++
				
				//We've checked as many players as there are on the teams. Early out.
				IF iPlayersChecked >= iPlayerCount
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_HAVE_CUSTOM_BLIP(INT iVeh)

	BOOL bBlip
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].sVehBlipStruct.sTeamBlip[iTeam].iBlipOverrideStartRule != -1)
	OR (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].sVehBlipStruct.sTeamBlip[iTeam].iBlipOverrideEndRule != -1)
		
		INT iBlipFrom = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].sVehBlipStruct.sTeamBlip[iTeam].iBlipOverrideStartRule
		INT iBlipTo = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].sVehBlipStruct.sTeamBlip[iTeam].iBlipOverrideEndRule
		
		BOOL bAtRightPoint1
		BOOL bAtRightPoint2
		
		IF (iBlipFrom = -1)
		OR (iRule > iBlipFrom)
			bAtRightPoint1 = TRUE
		ELIF (iRule = iBlipFrom)
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetThree, ciFMMC_VEHICLE3_CustomBlipStartOnMidpoint_T0 + iTeam )
				IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], iBlipFrom)
					bAtRightPoint1 = TRUE
				ENDIF
			ELSE
				bAtRightPoint1 = TRUE
			ENDIF
		ENDIF
		
		IF (iBlipTo = -1)
		OR (iRule < iBlipTo)
			bAtRightPoint2 = TRUE
		ELIF (iRule = iBlipTo)
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetThree, ciFMMC_VEHICLE3_CustomBlipEndOnMidpoint_T0 + iTeam )
				IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], iBlipTo)
					bAtRightPoint2 = TRUE
				ENDIF
			ELSE
				bAtRightPoint2 = TRUE
			ENDIF
		ENDIF
		
		IF bAtRightPoint1 AND bAtRightPoint2
			bBlip = TRUE
		ENDIF
	ENDIF
	
	IF iRule < FMMC_MAX_RULES
		// #2225760 - Check to see if this vehicle was delivered on this rule.
		// If it is flagged to keep it's blip until the rule ends keep its blip alive.
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iRule], ciBS_RULE3_BLIP_AFTER_DELIVERY)
			STRING strDecorName
			SWITCH iTeam
				CASE 0	strDecorName = "MC_Team0_VehDeliveredRules"		BREAK
				CASE 1	strDecorName = "MC_Team1_VehDeliveredRules"		BREAK
				CASE 2	strDecorName = "MC_Team2_VehDeliveredRules"		BREAK
				CASE 3	strDecorName = "MC_Team3_VehDeliveredRules"		BREAK
			ENDSWITCH
			IF DECOR_IS_REGISTERED_AS_TYPE(strDecorName, DECOR_TYPE_INT)
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
					VEHICLE_INDEX vehID = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
					IF DECOR_EXIST_ON(vehID, strDecorName)
						IF IS_BIT_SET(DECOR_GET_INT(vehID, strDecorName), iRule)
							#IF IS_DEBUG_BUILD
								IF (GET_FRAME_COUNT() % 120) = 0
									CDEBUG3LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Veh ", iVeh, " blip being kept alive by ciBS_RULE3_BLIP_AFTER_DELIVERY setting.")
								ENDIF
							#ENDIF
							bBlip = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iPartToUse].iTeam] < FMMC_MAX_RULES
		INT iBlipVehSetting = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehicleBlipVisibiltyRange[MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iPartToUse].iTeam]]
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
			VEHICLE_INDEX vehID = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
			
			IF iBlipVehSetting = ci_VEHICLE_BLIP_RULE_ALWAYS_OFF
				bBlip = FALSE
			ELIF iBlipVehSetting = ci_VEHICLE_BLIP_RULE_ALWAYS_ON
				bBlip = TRUE
			ELIF iBlipVehSetting > ci_VEHICLE_BLIP_RULE_ALWAYS_ON
				IF DOES_ENTITY_EXIST(vehID)
					VECTOR vVehCoord = GET_ENTITY_COORDS(vehID)
					VECTOR vPlayerCoord = GET_ENTITY_COORDS(LocalPlayerPed)
				
					FLOAT fDist = ABSF(GET_DISTANCE_BETWEEN_COORDS(vVehCoord, vPlayerCoord, FALSE))
					
					IF fDist < iBlipVehSetting
						bBlip = TRUE
					ELSE 
						CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH][RULE] Vehicle ", iVeh, " blip is outside of blip range ", iBlipVehSetting, ", cleaing up blip. (2)")
						bBlip = FALSE
					ENDIF
				ELSE
					bBlip = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	RETURN bBlip
	
ENDFUNC

/// PURPOSE:
///    Checks if we need to clean up the vehicles blip
FUNC BOOL SHOULD_VEHICLE_BLIP_CLEANUP(INT iVeh, VEHICLE_INDEX tempVeh)
	
	BOOL bShouldCleanup
	
	INT iAllTeamsBS = 15 //Checks all teams with 15
	INT iTeamInVehicle
	BOOL bAnyoneInVehicle = ARE_ANY_PARTICIPANTS_IN_VEHICLE(iveh, iAllTeamsBS, iTeamInVehicle)
	
	IF (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sVehBlipStruct.fBlipRange != 0)
	AND NOT (bAnyoneInVehicle AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix,ciFMMC_VEHICLE6_IGNORE_RANGE_IF_OCCUPIED))
	AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix,ciFMMC_VEHICLE6_BLIP_ON_MAP)
		VECTOR vVehCoord = GET_ENTITY_COORDS(tempVeh)
		VECTOR vPlayerCoord = GET_ENTITY_COORDS(LocalPlayerPed)
	
		FLOAT fDist = VDIST2(vVehCoord, vPlayerCoord)
		CPRINTLN(DEBUG_MISSION, "[SHOULD_VEHICLE_HAVE_BLIP]: fDist = ", fDist, ", vVehCoord = ", vVehCoord, ", vPlayerCoord = ", vPlayerCoord)
		IF fDist > POW(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sVehBlipStruct.fBlipRange, 2)
			bShouldCleanup = TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFour, ciFMMC_VEHICLE4_BLIP_OVERRIDE_HIDE_WHEN_IN_VEH_T0 + MC_playerBD[iPartToUse].iteam)
	AND SHOULD_VEHICLE_HAVE_CUSTOM_BLIP(iveh)
		IF IS_PED_IN_VEHICLE(PlayerPedToUse, tempVeh)
		OR IS_PLAYER_IN_CREATOR_AIRCRAFT(PlayerToUse)
			bShouldCleanup = TRUE
		ELIF GET_ENTITY_MODEL(tempVeh) = AVENGER
			IF IS_PLAYER_IN_CREATOR_AIRCRAFT(PlayerToUse)
				bShouldCleanup = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_THIS_MODEL_A_HELI(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn)
		IF IS_VEHICLE_EMPTY_AND_STATIONARY(tempVeh)
		AND (GET_BLIP_SPRITE(biVehBlip[iveh]) = RADAR_TRACE_ENEMY_HELI_SPIN OR GET_BLIP_SPRITE(biVehBlip[iveh]) = RADAR_TRACE_POLICE_HELI_SPIN)
			bShouldCleanup = TRUE
		ENDIF
	ENDIF
	
	IF (GET_ENTITY_MODEL(tempVeh) = TRAILERLARGE
	OR GET_ENTITY_MODEL(tempVeh) = HAULER2
	OR GET_ENTITY_MODEL(tempVeh) = AVENGER)
	AND IS_BIT_SET(iLocalBoolCheck24, LBOOL24_IN_CREATOR_TRAILER)
		bShouldCleanup = TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_DONT_BLIP_WITH_TRAILER)
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			VEHICLE_INDEX viTempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
			IF DOES_ENTITY_EXIST(viTempVeh)
				VEHICLE_INDEX viTrailer
				IF GET_VEHICLE_TRAILER_VEHICLE(viTempVeh, viTrailer)
				OR IS_VEHICLE_A_TRAILER(viTempVeh)
					bShouldCleanup = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iPartToUse].iTeam]
	IF iRule < FMMC_MAX_RULES
		INT iBlipVehSetting = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehicleBlipVisibiltyRange[MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iPartToUse].iTeam]]
		
		IF iBlipVehSetting = ci_VEHICLE_BLIP_RULE_ALWAYS_OFF
			bShouldCleanup = TRUE
		ELIF iBlipVehSetting = ci_VEHICLE_BLIP_RULE_ALWAYS_ON
			bShouldCleanup = FALSE
		ELIF iBlipVehSetting > ci_VEHICLE_BLIP_RULE_ALWAYS_ON
			VECTOR vVehCoord = GET_ENTITY_COORDS(tempVeh)
			VECTOR vPlayerCoord = GET_ENTITY_COORDS(LocalPlayerPed)
		
			FLOAT fDist = ABSF(GET_DISTANCE_BETWEEN_COORDS(vVehCoord, vPlayerCoord, FALSE))
			
			IF fDist < iBlipVehSetting
				bShouldCleanup = TRUE
			ELSE 
				CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH][RULE] Vehicle ", iVeh, " blip is outside of blip range ", iBlipVehSetting, ", cleaing up blip.")
				bShouldCleanup = FALSE
			ENDIF
		ENDIF

		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive, ciFMMC_VEHICLE5_HACKING_USE_DISTANCE_FOR_CAPTURE)	
		AND MC_serverBD_4.ivehRule[iVeh][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetEleven[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE11_SECUROHACK_REQUIRES_KILL)
			IF MC_serverBD_4.iHackingTargetsRemaining > 0
				IF MC_PlayerBD[iPartToUse].iVehDestroyBitset > 0
					IF NOT IS_BIT_SET(MC_PlayerBD[iPartToUse].iVehDestroyBitset, iVeh)
						bShouldCleanup = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF (NOT bShouldCleanup)
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetEleven[iRule], ciBS_RULE11_UNBLIP_OTHER_ENTITY)
		AND ARE_ALL_PARTICIPANTS_IN_OTHER_OBJECTIVE_VEHICLE(iveh, tempVeh)
			bShouldCleanup = TRUE
			CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH][RULE] Vehicle ", iVeh, " ciBS_RULE11_UNBLIP_OTHER_ENTITY unblipping")
		ENDIF
	ENDIF
	
	RETURN bShouldCleanup
	
ENDFUNC

PROC SET_SECONDARY_COLOUR_FOR_VEHICLE_BLIP_BASED_ON_TEAM(INT& iSColourTeam, INT iSetToValue, INT iCallNumber)
	PRINTLN("[RCC MISSION] SET_SECONDARY_COLOUR_FOR_VEHICLE_BLIP_BASED_ON_TEAM - Call number: ",iCallNumber,". Setting secondary blip colour to: ", iSetToValue)
	iSColourTeam = iSetToValue
ENDPROC

FUNC BOOL SHOULD_VEHICLE_HAVE_BLIP(INT iVeh, VEHICLE_INDEX tempVeh, INT& iPColour, INT& iSColourTeam)//, BOOL& bFlashing)
	
	BOOL bBlip = FALSE
	
	iPColour = BLIP_COLOUR_BLUEDARK
	SET_SECONDARY_COLOUR_FOR_VEHICLE_BLIP_BASED_ON_TEAM(iSColourTeam, -1, 1)
	//bFlashing = FALSE
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	
	INT iAllTeamsBS = 15 //Checks all teams with 15
	INT iTeamInVehicle
	BOOL bAnyoneInVehicle = ARE_ANY_PARTICIPANTS_IN_VEHICLE(iveh, iAllTeamsBS, iTeamInVehicle)
	
	IF iRule < FMMC_MAX_RULES
		IF MC_serverBD_4.iVehPriority[iveh][iTeam] <= iRule
		AND MC_serverBD_4.iVehPriority[iveh][iTeam] < FMMC_MAX_RULES
		
			IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_OBJECTIVE_BLOCKER)
			AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet, ciFMMC_VEHICLE_BLIP_OFF)
			AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iRule],ciBS_RULE_ENABLETRACKIFY)
			
				//CDEBUG3LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Vehicle ", iveh, " is on an active rule and hasn't had its blip disabled by settings.")
				BOOL bBlip2 = TRUE
				IF MC_serverBD_4.ivehRule[iveh][iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD
				AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iRule] ,ciBS_RULE4_ALWAYS_BLIP_DELIVERY_POINT)
					
					PRINTLN("[RCC MISSION][VEH] SHOULD_VEHICLE_HAVE_BLIP - ciBS_RULE4_ALWAYS_BLIP_DELIVERY_POINT is set using custom blip logic")
					IF GET_TEAM_HOLDING_VEHICLE( iveh ) > -1
						SET_SECONDARY_COLOUR_FOR_VEHICLE_BLIP_BASED_ON_TEAM(iSColourTeam, GET_TEAM_HOLDING_VEHICLE(iveh), 2)
					ELSE
						IF MC_serverBD.iDriverPart[ iveh ] != -1
							SET_SECONDARY_COLOUR_FOR_VEHICLE_BLIP_BASED_ON_TEAM(iSColourTeam, MC_playerBD[MC_serverBD.iDriverPart[iveh]].iteam, 3)
						ENDIF
					ENDIF
					
					bBlip = TRUE
					bBlip2 = TRUE
					
					iPColour = BLIP_COLOUR_BLUEDARK

					IF bBlip
						IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sVehBlipStruct.fBlipRange != 0
							VECTOR vVehCoord = GET_ENTITY_COORDS(tempVeh)
							VECTOR vPlayerCoord = GET_ENTITY_COORDS(LocalPlayerPed)
						
							FLOAT fDist = ABSF(GET_DISTANCE_BETWEEN_COORDS(vVehCoord, vPlayerCoord, FALSE))
							
							IF fDist > g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sVehBlipStruct.fBlipRange
								bBlip = FALSE
								CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] SHOULD_VEHICLE_HAVE_BLIP - Vehicle ", iVeh, " blip is outside of blip range ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sVehBlipStruct.fBlipRange, ", not blipping.")
							ENDIF
						ENDIF
					ENDIF
					
				ELSE
					IF ((MC_serverBD_4.ivehRule[iveh][iTeam] = FMMC_OBJECTIVE_LOGIC_GO_TO
					OR MC_serverBD_4.ivehRule[iveh][iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER)
					AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iRule], ciBS_RULE3_ALL_IN_SEPARATE_VEHICLES))
					OR SHOULD_VEHICLE_HAVE_CUSTOM_BLIP(iveh)
						BOOL bCheckOnRule // To get it compiling
						INT iTeamsBS = GET_VEHICLE_GOTO_TEAMS(iTeam, iveh, bCheckOnRule)
						IF SHOULD_VEHICLE_HAVE_CUSTOM_BLIP(iveh)
							iTeamsBS = 15
							INT iTeamInVeh
							IF NOT ARE_ANY_PARTICIPANTS_IN_VEHICLE(iveh, iTeamsBS, iTeamInVeh)
								CDEBUG3LN(DEBUG_MC_BLIP, "[RCC MISSION] SHOULD_VEHICLE_HAVE_BLIP - There are no players in vehicle ", iveh, ". Cancelling blip.")
								bBlip2 = FALSE
							ENDIF
						ELSE
							// If any participant on a team involved in the rule is in this vehicle
							// we don't want to blip it.
							IF ARE_ANY_TEAM_PARICIPANTS_IN_VEHICLE(iveh, iTeamsBS)
								CDEBUG3LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] SHOULD_VEHICLE_HAVE_BLIP - A participant on the rule is in vehicle ", iveh, ". Cancelling blip.")
								bBlip2 = FALSE
							ENDIF
						ENDIF
					ENDIF
					
					
					IF bBlip2
					AND NOT IS_PED_IN_VEHICLE(LocalPlayerPed, tempVeh)
						IF MC_serverBD_4.ivehRule[iveh][iTeam] = FMMC_OBJECTIVE_LOGIC_KILL					
						
							CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] SHOULD_VEHICLE_HAVE_BLIP - Vehicle ", iveh, " adding blip for KILL rule.")
							bBlip = TRUE
							iPColour = BLIP_COLOUR_RED
							
						ELSE
							IF (MC_serverBD_4.ivehRule[iveh][iTeam] != FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
							OR iDeliveryVehForcingOut != iveh // If we're doing a get & deliver (COLLECT) rule, then don't re-blip this vehicle if we're just getting forced out of it before dropping off
							OR SHOULD_VEHICLE_HAVE_CUSTOM_BLIP(iveh))
							AND NOT IS_PLAYER_IN_CREATOR_AIRCRAFT(PlayerToUse)
								CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] SHOULD_VEHICLE_HAVE_BLIP - Vehicle ", iveh, " adding blip for COLLECT&HOLD rule. MC_serverBD.iDriverPart: ", MC_serverBD.iDriverPart[iveh])
								
								bBlip = TRUE
								iPColour = BLIP_COLOUR_BLUEDARK

								IF GET_TEAM_HOLDING_VEHICLE(iveh) > -1
									SET_SECONDARY_COLOUR_FOR_VEHICLE_BLIP_BASED_ON_TEAM(iSColourTeam, GET_TEAM_HOLDING_VEHICLE(iveh), 4)
									CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] SHOULD_VEHICLE_HAVE_BLIP - Setting iSColourTeam to ", iSColourTeam)
								ELSE
									IF MC_serverBD.iDriverPart[ iveh ] != -1
									AND NOT IS_VEHICLE_SEAT_FREE(tempVeh)
									AND bAnyoneInVehicle
										SET_SECONDARY_COLOUR_FOR_VEHICLE_BLIP_BASED_ON_TEAM(iSColourTeam, MC_playerBD[MC_serverBD.iDriverPart[iveh]].iteam, 5)
										CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] SHOULD_VEHICLE_HAVE_BLIP - Set flashing and 2nd colour of team (", iSColourTeam, ") MC_serverBD.iDriverPart[ iveh ] : ", MC_serverBD.iDriverPart[ iveh ])
										//bFlashing = iveh
									ELIF (SHOULD_VEHICLE_HAVE_CUSTOM_BLIP(iveh) OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].sVehBlipStruct.iBlipSpriteOverride != -1)
									AND bAnyoneInVehicle
									AND NOT IS_VEHICLE_SEAT_FREE(tempVeh)
										PLAYER_INDEX piInVeh = GET_DRIVER_OF_CAR_AS_PLAYER(tempVeh)
										IF NETWORK_IS_PLAYER_A_PARTICIPANT(piInVeh)
											SET_SECONDARY_COLOUR_FOR_VEHICLE_BLIP_BASED_ON_TEAM(iSColourTeam, MC_playerBD[NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(piInVeh))].iteam, 6)
										ENDIF
									ELIF IS_VEHICLE_A_TRAILER(tempVeh)
									AND NOT IS_ENTITY_DEAD(GET_ENTITY_ATTACHED_TO(tempVeh))
										VEHICLE_INDEX truckVeh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_ATTACHED_TO(tempVeh))
										PLAYER_INDEX piInVeh = GET_DRIVER_OF_CAR_AS_PLAYER(truckVeh)
										IF NETWORK_IS_PLAYER_A_PARTICIPANT(piInVeh)
											SET_SECONDARY_COLOUR_FOR_VEHICLE_BLIP_BASED_ON_TEAM(iSColourTeam, MC_playerBD[NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(piInVeh))].iteam, 7)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
													
					IF bBlip
						IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sVehBlipStruct.fBlipRange != 0
							VECTOR vVehCoord = GET_ENTITY_COORDS(tempVeh)
							VECTOR vPlayerCoord = GET_ENTITY_COORDS(LocalPlayerPed)
						
							FLOAT fDist = ABSF(GET_DISTANCE_BETWEEN_COORDS(vVehCoord, vPlayerCoord, FALSE))
							
							IF fDist > g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sVehBlipStruct.fBlipRange
								bBlip = FALSE
								CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] SHOULD_VEHICLE_HAVE_BLIP - Vehicle ", iVeh, " blip is outside of blip range ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sVehBlipStruct.fBlipRange, ", not blipping.")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF NOT bBlip
		IF SHOULD_VEHICLE_HAVE_CUSTOM_BLIP(iVeh)
		AND NOT Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer)
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
				VEHICLE_INDEX vehID = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
				IF IS_VEHICLE_DRIVEABLE(vehID)
					BOOL bBlip2 = TRUE
					IF NOT bAnyoneInVehicle
						PRINTLN("[JS] SHOULD_VEHICLE_HAVE_BLIP - Don't add secondary blip colour - no one is in vehicle ", iVeh)
						bBlip2 = FALSE
						IF DOES_BLIP_EXIST(biVehBlip[iVeh])
							CLEAR_SECONDARY_COLOUR_FROM_BLIP(biVehBlip[iVeh])
						ENDIF
					ENDIF
						
					IF bBlip2
					AND NOT IS_PED_IN_VEHICLE(LocalPlayerPed, tempVeh)
						IF iDeliveryVehForcingOut != iveh 
							IF NOT IS_VEHICLE_SEAT_FREE(tempVeh)
								PLAYER_INDEX piInVeh = GET_DRIVER_OF_CAR_AS_PLAYER(tempVeh)
								IF NETWORK_IS_PLAYER_A_PARTICIPANT(piInVeh)
									SET_SECONDARY_COLOUR_FOR_VEHICLE_BLIP_BASED_ON_TEAM(iSColourTeam, MC_playerBD[NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(piInVeh))].iteam, 8)
								ENDIF
							ENDIF
						ENDIF
					ENDIF

					IF (NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFour, ciFMMC_VEHICLE4_BLIP_OVERRIDE_HIDE_WHEN_IN_VEH_T0 + iTeam))
					OR (NOT IS_PED_IN_VEHICLE(PlayerPedToUse, tempVeh))
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFour, ciFMMC_VEHICLE4_BLIP_OVERRIDE_HIDE_WHEN_IN_VEH_T0 + iTeam)
						AND IS_PLAYER_IN_CREATOR_AIRCRAFT(PlayerToUse)
							bBlip = FALSE
							CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] SHOULD_VEHICLE_HAVE_BLIP - Veh ", iVeh, " Avenger shouldn't be blipped as inside hold.")
						ELSE
							bBlip = TRUE
							iPColour = BLIP_COLOUR_BLUEDARK
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix, ciFMMC_VEHICLE6_TEAM_COLOURED_BLIP)
								IF iTeamInVehicle != -1
									iPColour = GET_BLIP_COLOUR_FROM_HUD_COLOUR(GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeamInVehicle, PlayerToUse))
								ENDIF
							ENDIF
							CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] SHOULD_VEHICLE_HAVE_BLIP - Veh ", iVeh, " being blipped by custom blip settings.")
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF			
		ENDIF
	ENDIF
	
	IF bBlip
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sVehBlipStruct.fBlipRange != 0
		AND NOT (bAnyoneInVehicle AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix,ciFMMC_VEHICLE6_IGNORE_RANGE_IF_OCCUPIED))
		AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix,ciFMMC_VEHICLE6_BLIP_ON_MAP)
			VECTOR vVehCoord = GET_ENTITY_COORDS(tempVeh)
			VECTOR vPlayerCoord = GET_ENTITY_COORDS(LocalPlayerPed)
		
			FLOAT fDist = VDIST2(vVehCoord, vPlayerCoord)
				
			IF fDist > POW(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sVehBlipStruct.fBlipRange, 2)
				bBlip = FALSE
				CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] SHOULD_VEHICLE_HAVE_BLIP - Vehicle ", iVeh, " blip is outside of blip range ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sVehBlipStruct.fBlipRange, ", not blipping.")
			ENDIF
		ENDIF
	ENDIF
	
	IF bBlip
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_DONT_BLIP_WITH_TRAILER)
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			VEHICLE_INDEX viTempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
			IF DOES_ENTITY_EXIST(viTempVeh)
				VEHICLE_INDEX viTrailer
				IF GET_VEHICLE_TRAILER_VEHICLE(viTempVeh, viTrailer)
				OR IS_VEHICLE_A_TRAILER(viTempVeh)
					bBlip = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bBlip
		IF (GET_ENTITY_MODEL(tempVeh) = TRAILERLARGE
		OR GET_ENTITY_MODEL(tempVeh) = HAULER2)
		AND IS_BIT_SET(iLocalBoolCheck24, LBOOL24_IN_CREATOR_TRAILER)
			bBlip = FALSE
		ENDIF
	ENDIF
	
	IF iRule < FMMC_MAX_RULES
		INT iBlipVehSetting = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehicleBlipVisibiltyRange[iRule]
		
		IF iBlipVehSetting = ci_VEHICLE_BLIP_RULE_ALWAYS_OFF
			bBlip = FALSE
		ELIF iBlipVehSetting = ci_VEHICLE_BLIP_RULE_ALWAYS_ON
			bBlip = TRUE
		ELIF iBlipVehSetting > ci_VEHICLE_BLIP_RULE_ALWAYS_ON
			VECTOR vVehCoord = GET_ENTITY_COORDS(tempVeh)
			VECTOR vPlayerCoord = GET_ENTITY_COORDS(LocalPlayerPed)
		
			FLOAT fDist = ABSF(GET_DISTANCE_BETWEEN_COORDS(vVehCoord, vPlayerCoord, FALSE))
			
			IF fDist < iBlipVehSetting
				bBlip = TRUE
			ELSE 
				CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH][RULE] SHOULD_VEHICLE_HAVE_BLIP - Vehicle ", iVeh, " blip is outside of blip range ", iBlipVehSetting, ", not blipping.")
				bBlip = FALSE
			ENDIF
		ENDIF
	
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive, ciFMMC_VEHICLE5_HACKING_USE_DISTANCE_FOR_CAPTURE)	
		AND MC_serverBD_4.ivehRule[iVeh][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetEleven[iRule], ciBS_RULE11_SECUROHACK_REQUIRES_KILL)
			IF MC_serverBD_4.iHackingTargetsRemaining > 0
				IF MC_PlayerBD[iPartToUse].iVehDestroyBitset > 0
					IF NOT IS_BIT_SET(MC_PlayerBD[iPartToUse].iVehDestroyBitset, iVeh)
						bBlip = FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF bBlip
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetEleven[iRule], ciBS_RULE11_UNBLIP_OTHER_ENTITY)
			CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH][RULE] SHOULD_VEHICLE_HAVE_BLIP - Vehicle ", iVeh, " checking for ciBS_RULE11_UNBLIP_OTHER_ENTITY.")
			IF ARE_ALL_PARTICIPANTS_IN_OTHER_OBJECTIVE_VEHICLE(iveh, tempVeh)
				CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH][RULE] SHOULD_VEHICLE_HAVE_BLIP - Vehicle ", iVeh, " unblipping because of ciBS_RULE11_UNBLIP_OTHER_ENTITY.")
				bBlip = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	// NOTE: See where this print is... CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION] Removing blip for vehicle ", iveh, ". The local player has entered the vehicle.")
	IF IS_PED_IN_VEHICLE(localPlayerPed, tempVeh)
		IF bBlip
			CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH][RULE] SHOULD_VEHICLE_HAVE_BLIP - Vehicle ", iVeh, " Blip was set up to show but we are in the vehicle so normal script functionality would delete it every frame. Seting bBlip to False.")
			bBlip = FALSE
		ENDIF
	ENDIF
	
	DEBUG_PRINTCALLSTACK()
		
	RETURN bBlip
ENDFUNC

PROC SET_VEHICLE_BLIP_SETTINGS(INT iVehIndex, ENTITY_INDEX veh, INT iPrimaryBlipColour)

	// Override the inputted blip colour if we have an override set
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[ iVehIndex ].sVehBlipStruct.iBlipColour != BLIP_COLOUR_DEFAULT
		iPrimaryBlipColour = GET_BLIP_COLOUR_FROM_CREATOR(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[ iVehIndex ].sVehBlipStruct.iBlipColour)
		CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Vehicle ", iVehIndex, " blip setup: Using custom colour from creator.")
	ENDIF
	
	INT iTeam = MC_PlayerBD[iLocalPart].iteam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	
	INT iTeamsBS = 15
	INT iTeamInVeh
	BOOL bOccupied = ARE_ANY_PARTICIPANTS_IN_VEHICLE(iVehIndex, iTeamsBS, iTeamInVeh)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehIndex].iVehBitsetSix, ciFMMC_VEHICLE6_TEAM_COLOURED_BLIP)
		IF bOccupied
			IF iTeamInVeh != -1
				INT iTempBlipColour = GET_BLIP_COLOUR_FROM_HUD_COLOUR(GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeamInVeh, PlayerToUse))
				IF iPrimaryBlipColour != iTempBlipColour
					CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Vehicle ", iVehIndex, " blip setup: Using team colour", iTeamInVeh)
					iPrimaryBlipColour = iTempBlipColour
				ENDIF
			ENDIF
		ELSE
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[ iVehIndex ].sVehBlipStruct.iBlipColour = BLIP_COLOUR_DEFAULT
				iPrimaryBlipColour = BLIP_COLOUR_BLUEDARK
			ENDIF
		ENDIF
	ENDIF
	
	CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Vehicle ", iVehIndex, " blip setup: Primary colour is ", iPrimaryBlipColour, ".")
	
	// should we show the height indicator?
	BOOL bUseExtendedHeightThreshold
	BOOL bShowBlipHeight
	
	bShowBlipHeight = SHOULD_SHOW_HEIGHT_ON_BLIP(biVehBlip[ iVehIndex ], bUseExtendedHeightThreshold)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehIndex].iVehBitsetThree, ciFMMC_VEHICLE3_ShowBlipHeightIndicator)
		CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Vehicle ", iVehIndex, " blip setup: Show height overriden by ShowBlipHeightIndicator setting.")
		bShowBlipHeight = TRUE
	ENDIF
	
	CDEBUG2LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Vehicle ", iVehIndex, " blip setup: Show height on blip ", PICK_STRING(bShowBlipHeight, "TRUE", "FALSE"), ".")
	
	// should this blip be rotated?
	IF SHOULD_BLIP_BE_MANUALLY_ROTATED(biVehBlip[ iVehIndex ])
		SET_BLIP_ROTATION(biVehBlip[ iVehIndex ], ROUND(GET_ENTITY_HEADING(veh)))
	ENDIF
	
	// url:bugstar:2185210 url:bugstar:2187866
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehIndex].iVehBitsetThree, ciFMMC_VEHICLE3_AutoGPSVehicle)
		CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Vehicle ", iVehIndex, " blip setup: Enabling GPS route for vehicle blip.")
		SET_BLIP_ROUTE(biVehBlip[ iVehIndex ], TRUE)
		SET_BLIP_ROUTE_COLOUR(biVehBlip[ iVehIndex ], iPrimaryBlipColour)
	ENDIF
	
	// url:bugstar:2192742
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehIndex].iVehBitsetThree, ciFMMC_VEHICLE3_IgnoreNoGPSFlag)
		CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Vehicle ", iVehIndex, " blip setup: Ignoring \"No GPS\" flag.")
		SET_IGNORE_NO_GPS_FLAG(TRUE)
	ENDIF
	
	SHOW_HEIGHT_ON_BLIP(biVehBlip[ iVehIndex ], bShowBlipHeight)
	SET_BLIP_EXTENDED_HEIGHT_THRESHOLD(biVehBlip[ iVehIndex ], bUseExtendedHeightThreshold)
	
	// url:bugstar:5627228
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehIndex].iVehBitsetEight, ciFMMC_VEHICLE8_FORCE_BLIP_HEIGHT_INDICATOR)
		SHOW_HEIGHT_ON_BLIP(biVehBlip[ iVehIndex ], TRUE)
		PRINTLN("[ML][SET_VEHICLE_BLIP_SETTINGS] Vehicle ", iVehIndex, " ciFMMC_VEHICLE8_FORCE_BLIP_HEIGHT_INDICATOR is SET, so forcing SHOW_HEIGHT_ON_BLIP")
	ENDIF
	
	SET_BLIP_COLOUR( biVehBlip[ iVehIndex ], iPrimaryBlipColour )
	SET_BLIP_SCALE( biVehBlip[ iVehIndex ], g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[ iVehIndex ].sVehBlipStruct.fBlipScale )
	SET_BLIP_ALPHA( biVehBlip[ iVehIndex ], 255 ) 
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehIndex].iVehBitsetFive, ciFMMC_VEHICLE5_USE_DEFAULT_BLIP_NAME)
		SET_BLIP_CUSTOM_NAME( biVehBlip[ iVehIndex ], MC_playerBD[ iPartToUse ].iteam, MC_serverBD_4.iVehPriority[ iVehIndex ][ MC_playerBD[ iPartToUse ].iteam ], iVehBlipCachedNamePriority[ iVehIndex ] )
	ENDIF

	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehIndex].iVehBitsetEight, ciFMMC_VEHICLE8_UseCustomFailNameForBlip)
		CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][CACHE] iVehIndex: ", iVehIndex, " Using Fail Name for Blip.")
		
		INT iName = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehIndex].iCustomVehName
		IF iName > -1
			CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][CACHE] iVehIndex: ", iVehIndex, " iName: ", iName, " ActualString: ", g_FMMC_STRUCT_ENTITIES.tlCustomVehName[iName], ".")
			IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomVehName[iName])
				//SET_BLIP_NAME_FROM_TEXT_FILE(biVehBlip[iVehIndex], g_FMMC_STRUCT_ENTITIES.tlCustomVehName[iName])
				BEGIN_TEXT_COMMAND_SET_BLIP_NAME("MCUSTBLIP")
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_FMMC_STRUCT_ENTITIES.tlCustomVehName[iName])
				END_TEXT_COMMAND_SET_BLIP_NAME(biVehBlip[iVehIndex])		
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][CACHE] iVehIndex: ", iVehIndex, " iName = -1 for some reason. Improper setup?")
		ENDIF
	ENDIF

	IF IS_SPECIAL_VEHICLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVehIndex]))	
		SET_BLIP_PRIORITY( biVehBlip[ iVehIndex ], BLIPPRIORITY_HIGH_HIGHEST ) 
	ELSE
		SET_BLIP_PRIORITY( biVehBlip[ iVehIndex ], BLIPPRIORITY_HIGHEST ) 
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_WARFARE(g_FMMC_STRUCT.iAdversaryModeType)
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
		SET_BLIP_PRIORITY( biVehBlip[ iVehIndex ], BLIPPRIORITY_MED ) 
	ENDIF
	
	IF iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehIndex].iVehBitsetSix,ciFMMC_VEHICLE6_BLIP_ON_MAP)
			BLIP_DISPLAY eBlipDisplay = DISPLAY_BOTH
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehIndex].sVehBlipStruct.fBlipRange != 0
			AND NOT (bOccupied AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehIndex].iVehBitsetSix,ciFMMC_VEHICLE6_IGNORE_RANGE_IF_OCCUPIED))
			AND IS_ENTITY_ALIVE(veh)
			AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehIndex].iVehicleBlipVisibiltyRange[iRule] != ci_VEHICLE_BLIP_RULE_ALWAYS_ON
				VECTOR vVehCoord = GET_ENTITY_COORDS(veh)
				VECTOR vPlayerCoord = GET_ENTITY_COORDS(LocalPlayerPed)
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetSeventeen, ciIGNORE_Z_FOR_VEH_BLIP_RANGE)
					vPlayerCoord.z = vVehCoord.z //url:bugstar:3796180
				ENDIF
				
				FLOAT fDist = VDIST2(vVehCoord, vPlayerCoord)
					
				IF fDist > POW(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehIndex].sVehBlipStruct.fBlipRange, 2)
					eBlipDisplay = DISPLAY_MAP
				ENDIF
			ENDIF
			SET_BLIP_DISPLAY(biVehBlip[iVehIndex], eBlipDisplay)
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehIndex].iVehBitsetSix, ciFMMC_VEHICLE6_TEAM_COLOURED_BLIP)
	AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_STUNTING_PACK(g_FMMC_STRUCT.iAdversaryModeType)
		INT iPColour
		INT iSColourTeam
		BOOL bShouldUseSecondaryColour
		INT iTeamForSecondaryColour
		SHOULD_VEHICLE_HAVE_BLIP(iVehIndex, GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(veh), iPColour, iSColourTeam)
		bShouldUseSecondaryColour = (iSColourTeam != -1)
		iTeamForSecondaryColour = iSColourTeam
		IF bShouldUseSecondaryColour
			CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Vehicle ", iVehIndex, " blip setup: Overriding colour to secondary colour ", iTeamForSecondaryColour, ".")
			IF ( iTeamForSecondaryColour = -1 )
				CASSERTLN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Vehicle ", iVehIndex, " blip setup: Invalid secondary colour of ", iTeamForSecondaryColour, " passed.") 
			ENDIF
			SET_BLIP_TEAM_SECONDARY_COLOUR( biVehBlip[ iVehIndex ], TRUE, iTeamForSecondaryColour )
		ENDIF
	ENDIF
	
	CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Vehicle ", iVehIndex, " blip setup: Done.")
ENDPROC

PROC SET_VEHICLE_BOUNDS_BLIP_SETTINGS(INT iVehIndex, ENTITY_INDEX veh, INT iPrimaryBlipColour, BOOL bSecondary = FALSE)

	// Override the inputted blip colour if we have an override set
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[ iVehIndex ].sVehBlipStruct.iBlipColour != BLIP_COLOUR_DEFAULT
		iPrimaryBlipColour = GET_BLIP_COLOUR_FROM_CREATOR(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[ iVehIndex ].sVehBlipStruct.iBlipColour)
		CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Vehicle ", iVehIndex, " bounds blip setup: Using custom colour from creator.")
	ENDIF
	
	CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Vehicle ", iVehIndex, " bounds blip setup: Primary colour is ", iPrimaryBlipColour, ".")
	
	// should we show the height indicator?
	BOOL bUseExtendedHeightThreshold
	BOOL bShowBlipHeight
	
	IF NOT bSecondary
		bShowBlipHeight = SHOULD_SHOW_HEIGHT_ON_BLIP(bounds_blip,bUseExtendedHeightThreshold)
	ELSE
		bShowBlipHeight = SHOULD_SHOW_HEIGHT_ON_BLIP(sec_bounds_blip,bUseExtendedHeightThreshold)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehIndex].iVehBitsetThree, ciFMMC_VEHICLE3_ShowBlipHeightIndicator)
		bShowBlipHeight = TRUE
		CDEBUG2LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Vehicle ", iVehIndex, " bounds blip setup: Show height overriden by ShowBlipHeightIndicator setting.")
	ENDIF
	
	CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Vehicle ", iVehIndex, " bounds blip setup: Show height on blip ", PICK_STRING(bShowBlipHeight, "TRUE", "FALSE"), ".")
	
	IF NOT bSecondary
		CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Vehicle ", iVehIndex, " bounds blip setup: Not secondary.")
	
		IF SHOULD_BLIP_BE_MANUALLY_ROTATED(bounds_blip)
			SET_BLIP_ROTATION(bounds_blip, ROUND(GET_ENTITY_HEADING(veh)))
		ENDIF
		
		SHOW_HEIGHT_ON_BLIP(bounds_blip, bShowBlipHeight)
		SET_BLIP_EXTENDED_HEIGHT_THRESHOLD(bounds_blip, bUseExtendedHeightThreshold)
		
		SET_BLIP_COLOUR( bounds_blip, iPrimaryBlipColour )
		SET_BLIP_SCALE( bounds_blip, 1.2 )
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehIndex].iVehBitsetFive, ciFMMC_VEHICLE5_USE_DEFAULT_BLIP_NAME)
			SET_BLIP_CUSTOM_NAME( bounds_blip, MC_playerBD[ iPartToUse ].iteam, MC_serverBD_4.iVehPriority[ iVehIndex ][ MC_playerBD[ iPartToUse ].iteam ], iVehBlipCachedNamePriority[ iVehIndex ] )
		ENDIF

		IF IS_SPECIAL_VEHICLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVehIndex]))
			SET_BLIP_PRIORITY( bounds_blip, BLIPPRIORITY_HIGH_HIGHEST ) 
		ELSE
			SET_BLIP_PRIORITY( bounds_blip, BLIPPRIORITY_HIGHEST ) 
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Vehicle ", iVehIndex, " bounds blip setup: Is secondary.")
		
		IF SHOULD_BLIP_BE_MANUALLY_ROTATED(sec_bounds_blip)
			SET_BLIP_ROTATION(sec_bounds_blip, ROUND(GET_ENTITY_HEADING(veh)))
		ENDIF
		
		SHOW_HEIGHT_ON_BLIP(sec_bounds_blip, bShowBlipHeight)
		SET_BLIP_EXTENDED_HEIGHT_THRESHOLD(sec_bounds_blip, bUseExtendedHeightThreshold)
		
		SET_BLIP_COLOUR( sec_bounds_blip, iPrimaryBlipColour )
		SET_BLIP_SCALE( sec_bounds_blip, 1.2 )
		IF IS_SPECIAL_VEHICLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVehIndex]))
			SET_BLIP_PRIORITY( sec_bounds_blip, BLIPPRIORITY_HIGH_HIGHEST ) 
		ELSE
			SET_BLIP_PRIORITY( sec_bounds_blip, BLIPPRIORITY_HIGHEST ) 
		ENDIF
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehIndex].iVehBitsetFive, ciFMMC_VEHICLE5_USE_DEFAULT_BLIP_NAME)
			SET_BLIP_CUSTOM_NAME( sec_bounds_blip, MC_playerBD[ iPartToUse ].iteam, MC_serverBD_4.iVehPriority[ iVehIndex ][ MC_playerBD[ iPartToUse ].iteam ], iVehBlipCachedNamePriority[ iVehIndex ] )
		ENDIF
		
	ENDIF
	
	CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Vehicle ", iVehIndex, " bounds blip setup: Done.")
ENDPROC

PROC SET_SPAWN_VEHICLE_BLIP_SETTINGS(VEHICLE_INDEX veh, INT iPrimaryBlipColour)
	
	// Should this blip be rotated?
	IF SHOULD_BLIP_BE_MANUALLY_ROTATED(biSpawnVehicle)
		CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][SPAWN] Detected blip should be manually rotated.")
		SET_BLIP_ROTATION(biSpawnVehicle, ROUND(GET_ENTITY_HEADING(veh)))
	ENDIF	
	
	// should we show the height indicator?
	BOOL bUseExtendedHeightThreshold
	SHOW_HEIGHT_ON_BLIP(biSpawnVehicle, SHOULD_SHOW_HEIGHT_ON_BLIP(biSpawnVehicle, bUseExtendedHeightThreshold))
	SET_BLIP_EXTENDED_HEIGHT_THRESHOLD(biSpawnVehicle, bUseExtendedHeightThreshold)
	
	CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][SPAWN] Setting primary colour to ", iPrimaryBlipColour)
	SET_BLIP_COLOUR(biSpawnVehicle, iPrimaryBlipColour )
	SET_BLIP_SCALE(biSpawnVehicle, BLIP_SIZE_NETWORK_VEHICLE )
	
	IF IS_SPECIAL_VEHICLE(veh)
		SET_BLIP_PRIORITY( biSpawnVehicle, BLIPPRIORITY_HIGH_HIGHEST ) 
	ELSE
		SET_BLIP_PRIORITY( biSpawnVehicle, BLIPPRIORITY_HIGHEST ) 
	ENDIF	
	CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][SPAWN] Finished configuring spawn blip.")
ENDPROC

PROC RUN_UPDATE_ON_VEHICLE_BLIP(INT iVeh, VEHICLE_INDEX veh)
	
	INT iPrimaryBlipColour
	
	IF DOES_BLIP_EXIST(biVehBlip[iVeh])
		// should this blip be rotated?
		IF SHOULD_BLIP_BE_MANUALLY_ROTATED(biVehBlip[ iVeh ])
			IF NOT IS_ENTITY_DEAD(veh)
				#IF IS_DEBUG_BUILD
					IF (GET_FRAME_COUNT() % 30) = 0
						CDEBUG3LN(DEBUG_MC_BLIP, "[RCC MISSION] Veh ", iVeh, " blip manually rotated to vehicle heading ", GET_ENTITY_HEADING(veh), ".")
					ENDIF
				#ENDIF
				SET_BLIP_ROTATION(biVehBlip[ iVeh ], ROUND(GET_ENTITY_HEADING(veh)))
			ENDIF
		ENDIF	
		
		BOOL bSetBlipSettings = FALSE
		// should we change the sprite?
		iPrimaryBlipColour = GET_BLIP_COLOUR(biVehBlip[iVeh])
		IF SHOULD_BLIP_ATLER_DEPENDING_ON_OCCUPANCY(biVehBlip[ iVeh ])
			IF UPDATE_VEHILCE_BLIP_BASED_ON_OCCUPANCY(veh, biVehBlip[ iVeh ])
				// if we've change the blip sprite we need to reapply all the settings as code effectively considers this a new blip.
				CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] Veh ", iVeh, " blip sprite was changed. Refreshing blip settings.")
				bSetBlipSettings = TRUE
			ENDIF
		ENDIF
		
		IF IS_THIS_ROCKSTAR_MISSION_SVM_DUNE4(g_FMMC_STRUCT.iRootContentIDHash)
			IF IS_BIT_SET(MC_serverBD.iMissionCriticalVeh[MC_playerBD[iPartToUse].iteam], iveh)
				CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] Veh ", iVeh, " allowing updating blips for mission critical vehicles in this mission")
				bSetBlipSettings = TRUE
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_TEAM_COLOURED_BLIP)
			bSetBlipSettings = TRUE
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetEight, ciFMMC_VEHICLE8_RunBlipUpdates)
			PRINTLN("[RCC MISSION] Veh ", iVeh, " allowing updating blip as iVehBitsetEight, ciFMMC_VEHICLE8_RunBlipUpdates is set.")
			bSetBlipSettings = TRUE
		ENDIF
		
		IF bSetBlipSettings
			SET_VEHICLE_BLIP_SETTINGS(iVeh, veh, iPrimaryBlipColour)
		ENDIF
		
		// should we show the height indicator?
		BOOL bUseExtendedHeightThreshold
		BOOL bShowBlipHeight
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetThree, ciFMMC_VEHICLE3_ShowBlipHeightIndicator)
			bShowBlipHeight = SHOULD_SHOW_HEIGHT_ON_BLIP(biVehBlip[ iVeh ],bUseExtendedHeightThreshold)
		ENDIF
		
		SHOW_HEIGHT_ON_BLIP(biVehBlip[ iVeh ], bShowBlipHeight)
		SET_BLIP_EXTENDED_HEIGHT_THRESHOLD(biVehBlip[ iVeh ], bUseExtendedHeightThreshold)
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetEight, ciFMMC_VEHICLE8_FORCE_BLIP_HEIGHT_INDICATOR)
			SHOW_HEIGHT_ON_BLIP(biVehBlip[ iVeh ], TRUE)
			PRINTLN("[MJL][RUN_UPDATE_ON_VEHICLE_BLIP] Vehicle ", iVeh, " ciFMMC_VEHICLE8_FORCE_BLIP_HEIGHT_INDICATOR is SET, so forcing SHOW_HEIGHT_ON_BLIP")
		ENDIF
		
	ENDIF
ENDPROC

PROC CREATE_BLIP_FOR_MP_VEHICLE( INT iVehIndex, ENTITY_INDEX veh, INT iPrimaryBlipColour )
	
	CDEBUG2LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Removing any existing blips on vehicle ", iVehIndex, " ahead of vehicle blip creation.")
	REMOVE_ANY_EXISTING_BLIP_FROM_ENTITY(veh)

	//CDEBUG2LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH]  Removing any AI blips on vehicle ", iVehIndex, " ahead of vehicle blip creation.")
	//CLEANUP_AI_VEHICLE_BLIPS_FOR_VEHICLE(iVehIndex)

	biVehBlip[ iVehIndex ] = ADD_BLIP_FOR_ENTITY(veh)
	
	//Blocks another kind of vehicle blip attempting to be made on this vehicle this frame.
	CDEBUG3LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Flagging to block any more new blips being created on veh ", iVehIndex, " this frame (", GET_FRAME_COUNT(), ").")
	SET_BIT(iVehBlipCreatedThisFrameBitset, iVehIndex)
	
	// What blip sprite should we be dealing with?
	BLIP_SPRITE BlipSprite = GET_CORRECT_PED_BLIP_SPRITE_FOR_VEHICLE_MODEL(GET_ENTITY_MODEL(veh), FALSE, IS_VEHICLE_EMPTY_AND_STATIONARY(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(veh)))
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehIndex].sVehBlipStruct.iBlipSpriteOverride != 0
		BlipSprite = GET_BLIP_SPRITE_FROM_BLIP_SPRITE_OVERRIDE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehIndex].sVehBlipStruct.iBlipSpriteOverride)
	ENDIF
	
	// Set the sprite.
	IF NOT (BlipSprite = GET_STANDARD_BLIP_ENUM_ID())
	AND BlipSprite != RADAR_TRACE_INVALID
		CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Setting vehicle ", iVehIndex, " blip sprite to ", GET_BLIP_SPRITE_DEBUG_STRING(BlipSprite), ".")
		SET_BLIP_SPRITE(biVehBlip[ iVehIndex ], BlipSprite)
		SET_CUSTOM_BLIP_NAME_FROM_MODEL(biVehBlip[ iVehIndex ], GET_ENTITY_MODEL(veh))
	ENDIF
	
	SET_BLIP_OVERRIDE_SPRITE_USE_CUSTOM_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehIndex].sVehBlipStruct.iBlipSpriteOverride, biVehBlip[iVehIndex])

	SET_VEHICLE_BLIP_SETTINGS(iVehIndex, veh, iPrimaryBlipColour)
ENDPROC

PROC CREATE_BLIP_FOR_MP_VEHICLE_BOUNDS(INT iVehIndex, ENTITY_INDEX veh, INT iPrimaryBlipColour, BOOL bSecondary = FALSE)
	
	IF DOES_BLIP_EXIST(biVehBlip[iVehIndex])
		EXIT
	ENDIF
	
	CDEBUG2LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Removing any existing blips on vehicle ", iVehIndex, " ahead of vehicle bounds blip creation.")
	REMOVE_ANY_EXISTING_BLIP_FROM_ENTITY(veh)

	//CDEBUG2LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH]  Removing any AI blips on vehicle ", iVehIndex, " ahead of vehicle bounds blip creation.")
	//CLEANUP_AI_VEHICLE_BLIPS_FOR_VEHICLE(iVehIndex)

	BLIP_INDEX tempBlip
	tempBlip = ADD_BLIP_FOR_ENTITY(veh)
	
	//Blocks another kind of vehicle blip attempting to be made on this vehicle this frame.
	CDEBUG3LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Flagging to block any more new blips being created on veh ", iVehIndex, " this frame (", GET_FRAME_COUNT(), ").")
	SET_BIT(iVehBlipCreatedThisFrameBitset, iVehIndex)

	BLIP_SPRITE BlipSprite
	// what blip sprite should we be dealing with?
	BlipSprite = GET_CORRECT_PED_BLIP_SPRITE_FOR_VEHICLE_MODEL(GET_ENTITY_MODEL(veh), FALSE, IS_VEHICLE_EMPTY_AND_STATIONARY(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(veh)))
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehIndex].sVehBlipStruct.iBlipSpriteOverride != 0
		BlipSprite = GET_BLIP_SPRITE_FROM_BLIP_SPRITE_OVERRIDE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehIndex].sVehBlipStruct.iBlipSpriteOverride)
	ENDIF
	
	// set the correct blip sprite
	IF NOT (BlipSprite = GET_STANDARD_BLIP_ENUM_ID())
		CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Custom sprite override: ", GET_BLIP_SPRITE_DEBUG_STRING(BlipSprite))
		SET_BLIP_SPRITE(tempBlip, BlipSprite)
		SET_CUSTOM_BLIP_NAME_FROM_MODEL(tempBlip, GET_ENTITY_MODEL(veh))
	ENDIF
	
	IF NOT bSecondary
		bounds_blip = tempBlip
	ELSE
		sec_bounds_blip = tempBlip
	ENDIF
	
	SET_VEHICLE_BOUNDS_BLIP_SETTINGS(iVehIndex, veh, iPrimaryBlipColour, bSecondary)
	
ENDPROC

// Custom procedure for the removal of a vehicle blip.
PROC REMOVE_VEHICLE_BLIP( INT iVeh )
	IF IS_BIT_SET(iVehBlipFlashingBitset, iVeh)
		CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Clearing flashing vehicle blip for vehicle ", iVeh, ".")
		CLEAR_BIT(iVehBlipFlashingBitset, iVeh)
	ENDIF

	IF DOES_BLIP_EXIST(biVehBlip[iVeh])
		// url:bugstar:2192742
		IF DOES_BLIP_HAVE_GPS_ROUTE(biVehBlip[iVeh])
		AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetThree, ciFMMC_VEHICLE3_IgnoreNoGPSFlag)
			SET_IGNORE_NO_GPS_FLAG(FALSE)
			CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Calling SET_IGNORE_NO_GPS_FLAG FALSE for vehicle ", iVeh, ".")
		ENDIF
		
		CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION][VEH]  Removing vehicle blip for vehicle ", iVeh, ".")
		REMOVE_BLIP(biVehBlip[iVeh])
	ENDIF
ENDPROC

// *** ADDED BY NEIL *** see 1707129
// Every time a new blip is added and set to flash, resync all the currently flashing blips to ensure they flash in unison. 
PROC INITIALISE_FLASHING_VEHICLE_BLIPS()
	CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Initialising flashing vehicle blip settings.")
	iVehBlipFlashingBitset = 0
	iVehBlipFlashingState = 0
ENDPROC

PROC RESYNC_FLASHING_VEHICLE_BLIPS()
	IF IS_BIT_SET(iVehBlipFlashingState, BIT_FLASH_VEH_BLIP_RESYNC_REQUESTED)
		CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Handling flashing veh blip resync request this frame.")
		INT i
		
		IF NOT IS_BIT_SET(iVehBlipFlashingState, BIT_FLASH_VEH_BLIP_FLASHING_STOPPED)
			REPEAT FMMC_MAX_VEHICLES i
				IF DOES_BLIP_EXIST(biVehBlip[i])
					IF IS_BIT_SET(iVehBlipFlashingBitset, i)
						SET_BLIP_FLASHES(biVehBlip[i], FALSE)
					ENDIF
				ENDIF
			ENDREPEAT
			SET_BIT(iVehBlipFlashingState, BIT_FLASH_VEH_BLIP_FLASHING_STOPPED)
			CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Stopped all flashing blips this frame.")
			
		ELSE
			REPEAT FMMC_MAX_VEHICLES i
				IF DOES_BLIP_EXIST(biVehBlip[i])
					IF IS_BIT_SET(iVehBlipFlashingBitset, i)
						SET_BLIP_FLASHES(biVehBlip[i], TRUE)
					ENDIF
				ENDIF
			ENDREPEAT
			CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Restarted all flashing blips this frame.")
			CLEAR_BIT(iVehBlipFlashingState, BIT_FLASH_VEH_BLIP_FLASHING_STOPPED)
			CLEAR_BIT(iVehBlipFlashingState, BIT_FLASH_VEH_BLIP_RESYNC_REQUESTED)
			CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Finished flashing veh blip resync.")
		ENDIF
		
	ENDIF
ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: PROCESS VEH BODY !
//
//************************************************************************************************************************************************************



PROC CLEANUP_COLLISION_ON_VEH(VEHICLE_INDEX tempVeh,INT iVeh)

	IF DOES_ENTITY_EXIST(tempVeh)
		IF NOT IS_ENTITY_DEAD(tempVeh)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
				SET_ENTITY_LOAD_COLLISION_FLAG(tempVeh, FALSE)
			ENDIF
		ENDIF
	ENDIF
	IF IS_BIT_SET(iVehCollisionBitset,iVeh)
		IF iCollisionStreamingLimit > 0
			iCollisionStreamingLimit--
		ENDIF
		CLEAR_BIT(iVehCollisionBitset,iVeh)
		PRINTLN("[LOAD_COLLISION_FLAG] Cleanup for veh: ",iVeh, " current requests: ",iCollisionStreamingLimit)
	ENDIF

ENDPROC

PROC LOAD_COLLISION_ON_VEH_IF_POSSIBLE(VEHICLE_INDEX tempVeh,INT iVeh,BOOL bForce = FALSE)

	INT iCreatorVehID = iVeh
	IF iCollisionStreamingLimit <= ciCOLLISION_STREAMING_MAX
		IF IS_VEHICLE_DRIVEABLE(tempVeh)
			IF bForce
			OR IS_ENTITY_WAITING_FOR_WORLD_COLLISION(tempVeh)
				IF iCreatorVehID = -1
					iCreatorVehID = IS_VEH_A_MISSION_CREATOR_VEH(tempVeh)
				ENDIF
				IF iCreatorVehID > -1
					IF NOT IS_BIT_SET(iVehCollisionBitset,iCreatorVehID)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
							SET_ENTITY_LOAD_COLLISION_FLAG(tempVeh, TRUE)
							iCollisionStreamingLimit++
							SET_BIT(iVehCollisionBitset,iCreatorVehID)
							PRINTLN("[LOAD_COLLISION_FLAG] Loading for veh: ",iCreatorVehID, " current requests: ",iCollisionStreamingLimit)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		PRINTLN("[LOAD_COLLISION_FLAG] iCollisionStreamingLimit reached! Shelved CL 13920263 has a possible solution. veh: ",iVeh)
		SCRIPT_ASSERT("[LOAD_COLLISION_FLAG] iCollisionStreamingLimit reached!")
	ENDIF

ENDPROC

//PURPOSE: Removes the load collision settings for planes/helis that are in the air and no longer need it for Take Off.
PROC MAINTAIN_VEH_COLLISION_CLEANUP(INT iveh, VEHICLE_INDEX tempVeh)
	
	IF IS_BIT_SET(iVehCollisionBitset, iVeh)
		IF IS_ENTITY_IN_AIR(tempVeh)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(tempVeh, FALSE)
			ENDIF
			
			CLEANUP_COLLISION_ON_VEH(tempVeh,iveh)
			PRINTLN("VEHICLE IN AIR - CLEANUP COLLISION AND SET CLEANUP_COLLISION_ON_VEH FALSE -  veh: ",iVeh)
		ENDIF
	ENDIF
	
ENDPROC

PROC INIT_BOAT(VEHICLE_INDEX tempVeh,INT iveh,BOOL bGoto = FALSE)

	IF bGoto
		SET_ENTITY_REQUIRES_MORE_EXPENSIVE_RIVER_CHECK(tempVeh, TRUE)
	ENDIF
	
	LOAD_COLLISION_ON_VEH_IF_POSSIBLE(tempVeh,iveh,bGoto)
	
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(tempVeh, TRUE)
	SET_BOAT_LOW_LOD_ANCHOR_DISTANCE(tempVeh,99999.0)
	SET_BOAT_ANCHOR(tempVeh,FALSE)
	SET_ENTITY_DYNAMIC(tempVeh, TRUE)
	ACTIVATE_PHYSICS(tempVeh)
	SET_VEHICLE_ENGINE_ON(tempVeh, TRUE,TRUE)
	PRINTLN("init boat called")
				
ENDPROC

PROC INIT_PLANE(VEHICLE_INDEX tempVeh,INT iveh, BOOL bGoto= FALSE)
	IF NOT IS_ENTITY_IN_AIR(tempVeh)
		LOAD_COLLISION_ON_VEH_IF_POSSIBLE(tempVeh,iveh,bGoto)
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(tempVeh, TRUE)
	ELSE
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(tempVeh, FALSE)
	ENDIF
	SET_ENTITY_DYNAMIC(tempVeh, TRUE)
	ACTIVATE_PHYSICS(tempVeh)
	SET_VEHICLE_ENGINE_ON(tempVeh, TRUE,TRUE)
	SET_HELI_BLADES_FULL_SPEED(tempVeh)
	PRINTLN("init plane called")

ENDPROC

PROC BIKE_RESPAWNED_TICKER_MESSAGE()
	// Ticker: The Scorcher bike got stuck and has been respawned.											
	SCRIPT_EVENT_DATA_TICKER_CUSTOM_MESSAGE TickerMessage											
	TickerMessage.TickerEvent = TICKER_EVENT_DSANCHEZ_BIKE_RESPAWNED					
	BROADCAST_CUSTOM_TICKER_EVENT(TickerMessage, ALL_PLAYERS_ON_SCRIPT(TRUE))
ENDPROC
PROC HANDLE_STUCK_VEH_DELETION(VEHICLE_INDEX veh, NETWORK_INDEX vehIdToDelete)
	IF IS_PED_IN_VEHICLE_OR_WAITING_TO_START_TASK_ENTER_VEHICLE(LocalPlayerPed, veh)
		CLEAR_PED_TASKS_IMMEDIATELY(LocalPlayerPed)							
	ENDIF																		
	IF IS_PED_IN_VEHICLE(LocalPlayerPed, Veh, FALSE)
		TASK_LEAVE_VEHICLE(LocalPlayerPed, veh, ECF_WARP_IF_DOOR_IS_BLOCKED)
	ENDIF	
	IF NETWORK_DOES_NETWORK_ID_EXIST(vehIdToDelete)								
		IF NETWORK_HAS_CONTROL_OF_ENTITY(veh)
			// Scorcher/Dirty Sanchez Only.
			PRINTLN("[LM][RCC MISSION] CHECK_VEHICLE_STUCK_TIMERS - Deleting the old Bike(Via; Force DELETE_NET_ID)")
			IF IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(veh))
			AND IS_THIS_ROCKSTAR_MISSION_NEW_VS_DIRTY_SANCHEZ(g_FMMC_STRUCT.iAdversaryModeType)
				BIKE_RESPAWNED_TICKER_MESSAGE()
			ENDIF
			
			DELETE_NET_ID(vehIdToDelete)		
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_ANY_PLAYER_TRYING_TO_OPEN_LOCKED_VEHICLE()
	INT i
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			
			PED_INDEX playerPed = GET_PLAYER_PED(piPlayer)
			
			IF DOES_ENTITY_EXIST(playerPed)
				IF IS_ENTITY_ALIVE(playerPed)
					IF IS_PED_TRYING_TO_ENTER_A_LOCKED_VEHICLE(playerPed)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

PROC SET_DECORATOR_ON_MOC_TRAILER(INT iveh, VEHICLE_INDEX ThisVeh)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = TRAILERLARGE
		IF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciALLOW_MOC_ENTRY_THIS_MISSION)
			AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix, ciFMMC_VEHICLE6_ALLOW_MOC_ENTRY))
		AND NOT DECOR_EXIST_ON(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]), "Creator_Trailer")
			IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
				IF NETWORK_HAS_CONTROL_OF_ENTITY(ThisVeh)
					IF DECOR_IS_REGISTERED_AS_TYPE("Creator_Trailer", DECOR_TYPE_INT)
						IF NOT DECOR_EXIST_ON(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]), "Creator_Trailer")
							IF GB_GET_LOCAL_PLAYER_GANG_BOSS() != INVALID_PLAYER_INDEX()
								DECOR_SET_INT(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]), "Creator_Trailer", NETWORK_HASH_FROM_PLAYER_HANDLE(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
								PRINTLN("[RCC MISSION] SET_DECORATOR_ON_MOC_TRAILER - setting ID decorator on created trailer to Creator_Trailer")
							ENDIF
						ENDIF
					ENDIF
				ELSE
					NETWORK_REQUEST_CONTROL_OF_ENTITY(ThisVeh)
					PRINTLN("[RCC MISSION] SET_DECORATOR_ON_MOC_TRAILER - Requesting control of MOC Trailer")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SET_DECORATOR_ON_AVENGER(INT iveh, VEHICLE_INDEX ThisVeh)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = INT_TO_ENUM(MODEL_NAMES, HASH("avenger"))
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_AVENGER_INTERIOR_ENTRY)
		AND NOT DECOR_EXIST_ON(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]), "Creator_Trailer")
			IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
				IF NETWORK_HAS_CONTROL_OF_ENTITY(ThisVeh)
					IF DECOR_IS_REGISTERED_AS_TYPE("Creator_Trailer", DECOR_TYPE_INT)
						IF NOT DECOR_EXIST_ON(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]), "Creator_Trailer")
							IF GB_GET_LOCAL_PLAYER_GANG_BOSS() != INVALID_PLAYER_INDEX()
								DECOR_SET_INT(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]), "Creator_Trailer", NETWORK_HASH_FROM_PLAYER_HANDLE(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
								PRINTLN("[RCC MISSION] SET_DECORATOR_ON_MOC_TRAILER - setting ID decorator on created Avenger to Creator_Trailer")
							ENDIF
						ENDIF
					ENDIF
				ELSE
					NETWORK_REQUEST_CONTROL_OF_ENTITY(ThisVeh)
					PRINTLN("[RCC MISSION] SET_DECORATOR_ON_MOC_TRAILER - Requesting control of Avenger")
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION] - SET_DECORATOR_ON_AVENGER Not a gang boss.")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC INT EXTRA_STUCK_TIME(VEHICLE_INDEX vehIndex, INT iVehFMMCIndex = -1)
	MODEL_NAMES model = GET_ENTITY_MODEL(vehIndex)
	
	IF model = RUINER2
	OR model = DUNE3
		RETURN 3000
	ENDIF
	
	IF IS_THIS_MODEL_A_BIKE(model)
		RETURN 13000
	ENDIF
	
	IF iVehFMMCIndex > -1
	AND iVehFMMCIndex < FMMC_MAX_VEHICLES
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehFMMCIndex].iVehBitsetSeven, ciFMMC_VEHICLE7_EXTENDED_STUCK_TIMER)
			RETURN 25000
		ENDIF
	ENDIF
	
	RETURN 0
ENDFUNC

CONST_INT ciSTUCK_TIMEOUT 20000

PROC CHECK_VEHICLE_STUCK_TIMERS( VEHICLE_INDEX veh, INT iVeh, BOOL bHaveControl )
	
	IF iVeh > -1 AND iVeh < FMMC_MAX_VEHICLES
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetEight, ciFMMC_VEHICLE8_IGNORE_STUCK_TIMERS)
		EXIT
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_DisableAllStuckTimers)
		EXIT
	ENDIF
	IF DOES_ENTITY_EXIST(veh)
		MODEL_NAMES model = GET_ENTITY_MODEL( veh )
		IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_EVERYONE_RUNNING)		
			INT iStuckTimer = ciSTUCK_TIMEOUT + EXTRA_STUCK_TIME(veh, iVeh) 
			INT iRoofTimer = ROOF_TIME + EXTRA_STUCK_TIME(veh, iVeh) 
			
			IF g_FMMC_STRUCT.iStuckTimeOut > 0
				iStuckTimer = g_FMMC_STRUCT.iStuckTimeOut
				iRoofTimer = g_FMMC_STRUCT.iStuckTimeOut
				PRINTLN("[RCC MISSION] CHECK_VEHICLE_STUCK_TIMERS - Using override Stuck TimeOut ",g_FMMC_STRUCT.iStuckTimeOut )
			ENDIF
			
			IF (IS_VEHICLE_STUCK_TIMER_UP( veh, VEH_STUCK_ON_ROOF, iRoofTimer)
			OR IS_VEHICLE_STUCK_TIMER_UP( veh, VEH_STUCK_ON_SIDE, iStuckTimer)
			OR IS_VEHICLE_STUCK_TIMER_UP( veh, VEH_STUCK_HUNG_UP, iStuckTimer)
			OR IS_VEHICLE_STUCK_TIMER_UP( veh, VEH_STUCK_JAMMED,iStuckTimer))
			AND NOT IS_ENTITY_ATTACHED_TO_ANY_OBJECT(veh)
			AND NOT IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(veh)
				IF NOT IS_THIS_MODEL_A_BOAT( model ) 
				OR ( HAS_COLLISION_LOADED_AROUND_ENTITY(veh) )
				AND NOT IS_ENTITY_IN_WATER( veh ) 
					IF bHaveControl					
						#IF IS_DEBUG_BUILD
							IF IS_VEHICLE_STUCK_TIMER_UP( veh, VEH_STUCK_ON_ROOF, iRoofTimer)
								PRINTLN("[RCC MISSION] CHECK_VEHICLE_STUCK_TIMERS - VEH_STUCK_ON_ROOF ")
							ENDIF
							IF IS_VEHICLE_STUCK_TIMER_UP( veh, VEH_STUCK_ON_SIDE, iStuckTimer)
								PRINTLN("[RCC MISSION] CHECK_VEHICLE_STUCK_TIMERS - VEH_STUCK_ON_SIDE ")
							ENDIF
							IF IS_VEHICLE_STUCK_TIMER_UP( veh, VEH_STUCK_HUNG_UP, iStuckTimer)
								PRINTLN("[RCC MISSION] CHECK_VEHICLE_STUCK_TIMERS - VEH_STUCK_HUNG_UP ")
							ENDIF
							IF IS_VEHICLE_STUCK_TIMER_UP( veh, VEH_STUCK_JAMMED, iStuckTimer)
								PRINTLN("[RCC MISSION] CHECK_VEHICLE_STUCK_TIMERS - VEH_STUCK_JAMMED ")
							ENDIF
						#ENDIF
						
						// So We're not Spamming..
						IF IS_VEHICLE_DRIVEABLE(veh)
							PRINTLN("[RCC MISSION] CHECK_VEHICLE_STUCK_TIMERS Vehicle is still driveable")						
							
							IF model = TRFLAT
							OR model = TRAILERS
								SET_ENTITY_HEALTH( veh, 0 )
								SET_VEHICLE_ENGINE_HEALTH( veh, -1000 )
								SET_VEHICLE_PETROL_TANK_HEALTH( veh, -1000 )
								PRINTLN("[RCC MISSION] CHECK_VEHICLE_STUCK_TIMERS - TRAILER IS STUCK SETTING PETROL TANK HEALTH -1000: ", iveh )
							ENDIF									
								
							IF GET_VEHICLE_ENGINE_HEALTH( veh ) > -1000
								SET_VEHICLE_ENGINE_HEALTH( veh, -1000 )
								IF GET_ENTITY_HEALTH( veh ) > 100
									SET_ENTITY_HEALTH( veh, 100 )
								ENDIF
								SET_VEHICLE_PETROL_TANK_HEALTH( veh, -1000 )
								PRINTLN("[LM][RCC MISSION] CHECK_VEHICLE_STUCK_TIMERS - VEHICLE IS STUCK SETTING HEALTH -1000: ", iveh )
								
								IF CONTENT_IS_USING_ARENA()
								AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_Disable_Arena_Mode)
									PRINTLN("[RCC MISSION] CHECK_VEHICLE_STUCK_TIMERS - ARENA MODE IS SET - VEHICLE IS STUCK SETTING BODY HEALTH -1000: ", iveh )
									SET_VEHICLE_BODY_HEALTH(veh, -1000)
								ENDIF
							ENDIF
							SET_VEHICLE_UNDRIVEABLE(veh, TRUE)
							SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(veh, FALSE)								
						ENDIF
						
						NETWORK_INDEX vehIdToDelete
						vehIdToDelete = VEH_TO_NET(veh)
						
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Roaming_Spectator_Force_Delete_Old_Veh)	
						AND NOT CONTENT_IS_USING_ARENA()
							// If we're playing Dirty Sanchez and the bike gets stuck
							// NOTE: Currently ciDELETE_VEHICLE_ON_STUCK was set as a Mission Details options so it affects all vehicles.
							// We only really want to affect the Scorcher but for now it will be hardcoded to delete scorcher and clcean up everthing else. 'Delete Vehicle When Stuck'
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciDELETE_VEHICLE_ON_STUCK)
								PRINTLN("[LM][RCC MISSION] CHECK_VEHICLE_STUCK_TIMERS - FORCE DELETE THE STUCK VEHICLE")	
								IF IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(veh))
								AND IS_THIS_ROCKSTAR_MISSION_NEW_VS_DIRTY_SANCHEZ(g_FMMC_STRUCT.iAdversaryModeType)
									HANDLE_STUCK_VEH_DELETION(veh, vehIdToDelete)		
								ELSE
									// Hardcoded Solution..
									IF NETWORK_DOES_NETWORK_ID_EXIST(vehIdToDelete)
										CLEANUP_NET_ID(vehIdToDelete)
									ENDIF
								ENDIF
							ELSE
								PRINTLN("[LM][RCC MISSION] CHECK_VEHICLE_STUCK_TIMERS - cleaning up the old Veh (Via; CLEANUP_NET_ID)")
								IF NETWORK_DOES_NETWORK_ID_EXIST(vehIdToDelete)
									CLEANUP_NET_ID(vehIdToDelete)
								ENDIF
							ENDIF						
													
							IF iVeh = 666
								IF NETWORK_DOES_NETWORK_ID_EXIST(netRespawnVehicle)
									PRINTLN("[RCC MISSION] CHECK_VEHICLE_STUCK_TIMERS - Cleaning up old respawn vehicle Other via (IF iVeh = 666)")
									CLEANUP_NET_ID(netRespawnVehicle)
									//CLEANUP_VEHICLE_CARGO(iVeh) this array overruns!!
								ENDIF
							ENDIF
						ENDIF
					ELSE
						NETWORK_REQUEST_CONTROL_OF_ENTITY(veh)
					ENDIF
				ENDIF
			ENDIF
			
			IF iVeh < FMMC_MAX_VEHICLES
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetEight, ciFMMC_VEHICLE8_RIGHT_SELF_WHEN_OUT_OF_SIGHT)
					
					INT iTimeToRightSelf = ROUND(ROOF_TIME * 0.25)
					
					IF (IS_VEHICLE_STUCK_TIMER_UP(veh, VEH_STUCK_ON_ROOF, iTimeToRightSelf)
					OR IS_VEHICLE_STUCK_TIMER_UP(veh, VEH_STUCK_ON_SIDE, iTimeToRightSelf))
					AND NOT CAN_ANY_PLAYER_SEE_POINT(GET_ENTITY_COORDS(veh), 3.25)
					
						PRINTLN("CHECK_VEHICLE_STUCK_TIMERS - Flipping vehicle ", iVeh, " now!")
						
						FLOAT fZRot = GET_ENTITY_HEADING(veh)
						VECTOR vTargetRot = <<0, 0, fZRot>>

						INT iAttemptsPerFrame = 10
						INT iAttempt
						
						FOR iAttempt = 0 TO iAttemptsPerFrame
							
							IF IS_VECTOR_ZERO(vBarragePlacementVector)
								vBarragePlacementVector = GET_ENTITY_COORDS(veh)
								vBarragePlacementVector.z += 0.5
							ELSE
								DRAW_DEBUG_LINE(vBarragePlacementVector, GET_ENTITY_COORDS(veh), 255, 0, 100, 255)
							ENDIF
						
							IF GET_CAN_VEHICLE_BE_PLACED_HERE(veh, vBarragePlacementVector, vTargetRot)
								
								PRINTLN("CHECK_VEHICLE_STUCK_TIMERS - Space IS free! ", vBarragePlacementVector, " After attempts: ", iAttempt)
								
								SET_ENTITY_COORDS(veh, vBarragePlacementVector)
								SET_ENTITY_ROTATION(veh, vTargetRot)
								SET_VEHICLE_ON_GROUND_PROPERLY(veh)
								
								vBarragePlacementVector = <<0,0,0>>
								BREAKLOOP
							ELSE
								PRINTLN("CHECK_VEHICLE_STUCK_TIMERS - Space is not free! ", vBarragePlacementVector, " Attempt: ", iAttempt)
								
								FLOAT fRandomMoveAmount = 2.5
								vBarragePlacementVector += <<GET_RANDOM_FLOAT_IN_RANGE(-fRandomMoveAmount, fRandomMoveAmount), GET_RANDOM_FLOAT_IN_RANGE(-fRandomMoveAmount, fRandomMoveAmount), 0.05>>	
							ENDIF
						ENDFOR
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_OBJECTIVE_VEHICLE_ON_PLAYERS_CARGOBOB(INT iVeh, VEHICLE_INDEX tempVeh)
	
	BOOL bPrintDebug = FALSE
	
	#IF IS_DEBUG_BUILD
	bPrintDebug = bobjectiveVehicleOnCargobobDebug
	#ENDIF
	
	IF NOT DOES_ENTITY_EXIST(tempVeh)
		IF bPrintDebug
			PRINTLN("IS_OBJECTIVE_VEHICLE_ON_PLAYERS_CARGOBOB - Veh ", iVeh, " || tempVeh doesn't exist")
		ENDIF
		RETURN FALSE
	ENDIF
		
	VEHICLE_INDEX viPlayerVeh
	
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				
		viPlayerVeh = GET_VEHICLE_PED_IS_USING(LocalPlayerPed)
		
		IF GET_ENTITY_MODEL(viPlayerVeh) != CARGOBOB
		AND GET_ENTITY_MODEL(viPlayerVeh) != CARGOBOB2
		AND GET_ENTITY_MODEL(viPlayerVeh) != CARGOBOB3
		AND GET_ENTITY_MODEL(viPlayerVeh) != CARGOBOB4
			IF bPrintDebug
				PRINTLN("IS_OBJECTIVE_VEHICLE_ON_PLAYERS_CARGOBOB - Veh ", iVeh, " || Player isn't in cargobob")
			ENDIF
			RETURN FALSE
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(viPlayerVeh)
			IF bPrintDebug
				PRINTLN("IS_OBJECTIVE_VEHICLE_ON_PLAYERS_CARGOBOB - Veh ", iVeh, " || viPlayerVeh doesn't exist")
			ENDIF
			RETURN FALSE
		ENDIF
	ELSE
		IF bPrintDebug
		PRINTLN("IS_OBJECTIVE_VEHICLE_ON_PLAYERS_CARGOBOB - Veh ", iVeh, " || Player isn't in any vehicle")
		ENDIF
		RETURN FALSE
	ENDIF
	
	IF MC_serverBD_4.iVehPriority[iveh][MC_playerBD[iPartToUse].iteam] > MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
	OR MC_serverBD_4.iVehPriority[iveh][MC_playerBD[iPartToUse].iteam] = -1
		IF bPrintDebug
			PRINTLN("IS_OBJECTIVE_VEHICLE_ON_PLAYERS_CARGOBOB - Veh ", iVeh, " || Not an objective veh")
		ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_VEHICLE_ATTACHED_TO_CARGOBOB(viPlayerVeh, tempVeh)
		IF bPrintDebug
			PRINTLN("IS_OBJECTIVE_VEHICLE_ON_PLAYERS_CARGOBOB - Veh ", iVeh, " || Is attached to viPlayerVeh!!!")
		ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SPECIALCASE_IGNORE_VEHICLE_AS_TARGET(VEHICLE_INDEX tempVeh, INT iVehIndex)
	IF (IS_THIS_ROCKSTAR_MISSION_WVM_TRAILERSMALL(g_FMMC_STRUCT.iRootContentIDHash) AND GET_ENTITY_MODEL(tempVeh) = NIGHTSHARK) 
	OR (IS_THIS_ROCKSTAR_MISSION_WVM_TAMPA(g_FMMC_STRUCT.iRootContentIDHash) AND GET_ENTITY_MODEL(tempVeh) = BUZZARD)
	OR (IS_THIS_ROCKSTAR_MISSION_WVM_OPPRESSOR_2(g_FMMC_STRUCT.iRootContentIDHash) AND GET_ENTITY_MODEL(tempVeh) = INSURGENT)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehIndex].iVehBitsetSix, ciFMMC_VEHICLE6_BLOCK_VEHICLE_AS_HINT_TARGET)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC KEEP_VEHICLE_IN_BOUNDS(VEHICLE_INDEX vehIndex, INT iVehToUse, BOOL bCheckOverhang, BOOL bForceToPlacedPos = FALSE)
	PRINTLN("[KEEP_VEHICLE_IN_BOUNDS]")
	INT iTeam = MC_playerBD[iPartToUse].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule < FMMC_MAX_RULES
		VECTOR vVehCoords = GET_ENTITY_COORDS(vehIndex)
		PRINTLN("[KEEP_VEHICLE_IN_BOUNDS] vVehCoords = <<", vVehCoords.X, ", ", vVehCoords.Y, ", ", vVehCoords.Z, ">>")
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iType = 1
			PRINTLN("[KEEP_VEHICLE_IN_BOUNDS] g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iType = 1")
			IF NOT IS_ENTITY_IN_ANGLED_AREA(vehIndex, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos1, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos2, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fWidth)
				vVehCoords = GET_CLOSEST_POINT_AT_EDGE_OF_BOUNDS(vVehCoords, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos1, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos2, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fWidth)
				PRINTLN("[KEEP_VEHICLE_IN_BOUNDS] - vVehCoords at edge: ", vVehCoords)
				FLOAT newZ
				IF GET_GROUND_Z_FOR_3D_COORD(vVehCoords, newZ)
				AND NOT bForceToPlacedPos
					IF NOT bCheckOverhang
						vVehCoords.z = newZ
					ELSE
						INT i
						vVehCoords.z = newZ
						REPEAT 2 i
							vVehCoords.z = CHECK_Z_FOR_OVERHANG(vVehCoords)
						ENDREPEAT
					ENDIF
					PRINTLN("[KEEP_VEHICLE_IN_BOUNDS] GET_GROUND_Z_FOR_3D_COORD returned true")
				ELSE
					PRINTLN("[KEEP_VEHICLE_IN_BOUNDS] GET_GROUND_Z_FOR_3D_COORD returned false")
					vVehCoords = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehToUse].vPos
					
					IF GET_GROUND_Z_FOR_3D_COORD(vVehCoords, newZ)
						vVehCoords.z = newZ
						PRINTLN("[KEEP_VEHICLE_IN_BOUNDS] - BACKUP PLACING AT CENTER -  FOUND A VALID GROUND Z = ", newZ)
					ELSE
						PRINTLN("[KEEP_VEHICLE_IN_BOUNDS] - BACKUP PLACING AT CENTER -  NO VALID Z FOUND, LIKELY SOME STRANGE PLACEMENT")
					ENDIF
					PRINTLN("[KEEP_VEHICLE_IN_BOUNDS] BACKUP PLACING AT CENTER - new coords: ", vVehCoords)
				ENDIF
				SET_ENTITY_COORDS(vehIndex, vVehCoords)
				SET_VEHICLE_ON_GROUND_PROPERLY(vehIndex)
				PRINTLN("[KEEP_VEHICLE_IN_BOUNDS] NOT IS_ENTITY_IN_ANGLED_AREA")

			ENDIF
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iType = 0
		AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos)
			PRINTLN("[KEEP_VEHICLE_IN_BOUNDS] g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iType = 0")
			IF VDIST2(vVehCoords, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos) < POW(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fRadius, 2)
				VECTOR vDir = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos - vVehCoords
				
				vDir = NORMALISE_VECTOR(vDir)
				
				vVehCoords = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos + (vDir * g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fRadius)

				GET_GROUND_Z_FOR_3D_COORD(vVehCoords, vVehCoords.Z)
				SET_ENTITY_COORDS(vehIndex, vVehCoords)
				PRINTLN("[KEEP_VEHICLE_IN_BOUNDS] Moving to ", vVehCoords)
			ENDIF

		ENDIF
	ENDIF
ENDPROC

//PURPOSE: When the mission is ending this checks for cases where we should be setting the heli blades to full speed
PROC HANDLE_PROPELLORS_WHEN_MISSION_ENDS()

	IF IS_PED_IN_ANY_HELI(LocalPlayerPed)
	
		VEHICLE_INDEX viPlayerVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		
		IF (IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		OR MC_serverBD.iServerGameState = GAME_STATE_END)	
			BOOL bShouldSetHeliBladesToFullSpeed
			
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRQUOTA(g_FMMC_STRUCT.iAdversaryModeType)
				bShouldSetHeliBladesToFullSpeed = TRUE
				PRINTLN("HANDLE_PROPELLORS_WHEN_MISSION_ENDS - Setting heli blades to full speed because Air Quota is ending and we don't want it to look like the engine has turned off (Bug 4002941)")
			ENDIF
			
			IF bShouldSetHeliBladesToFullSpeed
				SET_HELI_BLADES_FULL_SPEED(viPlayerVeh)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_VEHICLE_BLIP_BE_REMOVED(VEHICLE_INDEX tempVeh)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_PerformVehicleBlipRemovalChecks)
		RETURN TRUE
	ENDIF
	
	INT iPlacedPedInVeh = GET_PLACED_PED_IN_VEHICLE(tempVeh)
	
	IF iPlacedPedInVeh != -1
		RETURN IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPlacedPedInVeh].iPedBitset, ciPED_BS_BlipOff)
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

PROC HANDLE_VEHICLE_BLIP_CREATION(INT iVeh, VEHICLE_INDEX tempVeh, INT& iPColour, INT& iSColourTeam)

	IF NOT DOES_BLIP_EXIST(biVehBlip[iveh])
		IF SHOULD_VEHICLE_HAVE_BLIP(iVeh, tempVeh, iPColour, iSColourTeam)
			IF NOT IS_BIT_SET(iVehBlipCreatedThisFrameBitset, iveh)
				CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Creating blip for vehicle ", iveh, ". SHOULD_VEHICLE_HAVE_BLIP returned TRUE.")
				CREATE_BLIP_FOR_MP_VEHICLE(iVeh, tempVeh, iPColour)
			ELSE
				CDEBUG3LN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Block vehicle blip creation this frame as another blip has just been made on the vehicle.")
			ENDIF
		ELSE
			CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Deleting blip from vehicle ", iveh)
			IF SHOULD_VEHICLE_BLIP_BE_REMOVED(tempVeh)
				REMOVE_VEHICLE_BLIP(iveh)
				REMOVE_ANY_EXISTING_BLIP_FROM_ENTITY(tempVeh)
			ENDIF
		ENDIF
	ELSE
		// We know a blip exists, monitor any conditions that require it to be cleaned up
		IF SHOULD_VEHICLE_BLIP_CLEANUP(iVeh, tempVeh)
			CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Removing blip for vehicle ", iveh, ". SHOULD_VEHICLE_BLIP_CLEANUP returned TRUE.")
			REMOVE_VEHICLE_BLIP(iveh)
		ENDIF
	ENDIF
ENDPROC

PROC EXT2_CREATE_CLOUD_PTFX(VEHICLE_INDEX vehPlaneSkydive)
	IF IS_ENTITY_ALIVE(vehPlaneSkydive)
	
		REQUEST_NAMED_PTFX_ASSET("scr_rcextreme2")
		IF HAS_NAMED_PTFX_ASSET_LOADED("scr_rcextreme2")
			IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_cargoplane_moving_clouds)
				FLOAT fCloudsCurrentAlpha = 0.4
				USE_PARTICLE_FX_ASSET("scr_rcextreme2")
				ptfx_cargoplane_moving_clouds = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_extrm2_moving_cloud", vehPlaneSkydive, <<0,-20,-13>>, <<0,0,0>>)
				SET_PARTICLE_FX_LOOPED_ALPHA(ptfx_cargoplane_moving_clouds, fCloudsCurrentAlpha)
				CPRINTLN(DEBUG_MISSION, "EXT2_CREATE_CLOUD_PTFX make clouds ptfx")
			ELSE
				//SET_PARTICLE_FX_LOOPED_ALPHA(ptfx_moving_clouds, fCloudsCurrentAlpha)
			ENDIF
			IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_cargoplane_wind_and_smoke)
				USE_PARTICLE_FX_ASSET("scr_rcextreme2")
				ptfx_cargoplane_wind_and_smoke = START_PARTICLE_FX_LOOPED_AT_COORD("scr_rcext2_cargo_smoke", <<417.0, 3920.0, 1449.0>>, <<0,0,0>>)
				CPRINTLN(DEBUG_MISSION, "EXT2_CREATE_CLOUD_PTFX make winds and smoke")
			ENDIF
		ENDIF
	ELSE
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_cargoplane_moving_clouds)
		OR DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_cargoplane_wind_and_smoke)
			REMOVE_PARTICLE_FX(ptfx_cargoplane_moving_clouds)
			REMOVE_PARTICLE_FX(ptfx_cargoplane_wind_and_smoke)
			REMOVE_NAMED_PTFX_ASSET("scr_rcextreme2")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_VALID_PLAYERS_MOVED_TO_PROCESS_SHOTS()	
	INT iPart
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		PARTICIPANT_INDEX participantIndex = INT_TO_PARTICIPANTINDEX(iPart)
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(participantIndex)
			IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)
			AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_HEIST_SPECTATOR)
			
				IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet4, PBBOOL4_REACHED_CUT_PROCESS_SHOTS)
					PRINTLN("[LM][HAS_VALID_PLAYERS_MOVED_TO_PROCESS_SHOTS] Waiting for ", iPart, " so returning FALSE") 
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	PRINTLN("[LM][HAS_VALID_PLAYERS_MOVED_TO_PROCESS_SHOTS] All players reached process shots, returning TRUE")
	RETURN TRUE
ENDFUNC

PROC PROCESS_VEHICLE_ON_RULE_WARP(INT iVehicle, VEHICLE_INDEX tempVeh)
	PRINTLN("LM SPAM IS_THIS_RULE_A_CUTSCENE: ", IS_THIS_RULE_A_CUTSCENE())

	IF IS_BIT_SET(iVehicleShouldWarpThisRuleStart, iVehicle)	
	AND NOT IS_BIT_SET(iVehicleWarpedOnThisRule, iVehicle)
				
		IF NOT DOES_ENTITY_EXIST(tempVeh)
		OR NOT IS_VEHICLE_DRIVEABLE(tempVeh)
			PRINTLN("[LM][Veh_Rule_warp] - iVehicle: ", iVehicle, " Has been set to warp on this rule but the vehicle does not exist or is destroyed...")
			EXIT
		ENDIF
		
		IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
			PRINTLN("[LM][Veh_Rule_warp] - iVehicle: ", iVehicle, " Has been set to warp on this rule but we don't have control over it.")
			EXIT
		ENDIF
		
		INT iTeam = MC_playerBD[iPartToUse].iteam
		INT iRule =	MC_serverBD_4.iCurrentHighestPriority[iTeam] 
		
		IF iRule >= FMMC_MAX_RULES
			EXIT	
		ENDIF
		
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
			PRINTLN("[LM][Veh_Rule_warp] - iVehicle: ", iVehicle, " Exitting - On frame new priority.")
			EXIT
		ENDIF
		
		IF IS_THIS_RULE_A_CUTSCENE()
		OR g_bInMissionControllerCutscene
			IF NOT HAS_VALID_PLAYERS_MOVED_TO_PROCESS_SHOTS()
				PRINTLN("[LM][Veh_Rule_warp] - iVehicle: ", iVehicle, " Cutscene rule. Bailing until we are processing the shots...")
				EXIT
			ELSE
				PRINTLN("[LM][Veh_Rule_warp] - iVehicle: ", iVehicle, " All Players are processing the shots...")
			ENDIF
		ELSE
			PRINTLN("[LM][Veh_Rule_warp] - iVehicle: ", iVehicle, " Not a cutscene.")
		ENDIF
		
		FLOAT fVehWarpHead
		VECTOR vVehWarpPos
		INT iWarpPos = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleVehicleWarp[iRule][iVehicle]
		IF iWarpPos > -1
			vVehWarpPos = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehicle].sWarpLocationSettings.vPosition[iWarpPos]
			fVehWarpHead = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehicle].sWarpLocationSettings.fHeading[iWarpPos]
		ELSE
			PRINTLN("[LM][Veh_Rule_warp] - iVehicle: ", iVehicle, " iWarpPos = -1... Something is set up wrong.")
		ENDIF
		
		IF NOT IS_VECTOR_ZERO(vVehWarpPos)		
			PRINTLN("[LM][Veh_Rule_warp] - iVehicle: ", iVehicle, " Warping this vehicle with COORD: ", vVehWarpPos, " and Heading: ", fVehWarpHead, " which is using iWarpPos: ", iWarpPos, " Setting iVehicleWarpedOnThisRule.")
			
			SET_ENTITY_COORDS(tempVeh, vVehWarpPos, FALSE, TRUE)
			SET_ENTITY_HEADING(tempVeh, fVehWarpHead)
			SET_BIT(iVehicleWarpedOnThisRule, iVehicle)		
			BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_WarpedVehicleOnRuleStart, iVehicle, DEFAULT, DEFAULT, DEFAULT, DEFAULt, DEFAULT, TRUE)
		ENDIF
	ENDIF
ENDPROC

INT iDebugVehicleDelayedDoorOpeningIndex = -1
PROC PROCESS_VEH_BODY(INT iveh)

	VEHICLE_INDEX tempVeh,tempPlayerVeh,tempTrailer
	INT iPColour
	INT iSColourTeam = -1

	//Request from last update to resync all flashing veh blips.
	RESYNC_FLASHING_VEHICLE_BLIPS()
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])		
			
		IF NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]))
			IF DOES_BLIP_EXIST(biVehBlip[iveh])
				CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION] Removing blip for vehicle ", iveh, ". The vehicle is no longer driveable.")
				REMOVE_VEHICLE_BLIP(iveh)
			ENDIF		
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_TAGGING_TARGETS)
				REMOVE_TAGGED_BLIP(2, iveh)
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetEight, ciFMMC_VEHICLE8_OffRuleQuickGPS)
				MPGlobalsAmbience.vQuickGPS2 = <<0,0,0>>
			ENDIF
							
			CLEANUP_COLLISION_ON_VEH(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]),iveh)
			EXIT
		ELSE
			tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
			
			IF IS_PED_IN_VEHICLE(LocalPlayerPed, tempVeh)		
				iPlacedVehicleIAmIn = iVeh
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFive, ciFMMC_VEHICLE5_ENABLE_REAR_MINES_DUNE3)
					IF g_bDisableVehicleMines
						PRINTLN("[LM][PROCESS_RESET_DUNE_REAR_MINES][MINES] - g_bDisableVehicleMines is not set Setting to false so we can use the mines.")
						DISABLE_VEHICLE_MINES(FALSE)
					ENDIF
				ENDIF
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSeven, ciFMMC_VEHICLE7_VEHICLE_BLOCK_MANUAL_RESPAWN)
					IF NOT IS_BIT_SET(iLocalBoolCheck25, LBOOL25_BLOCK_MANUAL_RESPAWN_DUE_TO_VEHICLE)
						PRINTLN("[LM][MANUAL RESPAWN] - We are inside iVeh: ", iveh, " Which has LBOOL25_BLOCK_MANUAL_RESPAWN_DUE_TO_VEHICLE set.")
						SET_BIT(iLocalBoolCheck25, LBOOL25_BLOCK_MANUAL_RESPAWN_DUE_TO_VEHICLE)
					ENDIF
				ENDIF
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSeven, ciFMMC_VEHICLE7_PLAYER_INVINCIBLE_IN_VEHICLE)
				AND NOT (IS_ENTITY_IN_WATER(localPlayerPed) AND IS_ENTITY_IN_DEEP_WATER(tempVeh))
					IF GET_ENTITY_CAN_BE_DAMAGED(LocalPlayerPed)
						SET_ENTITY_CAN_BE_DAMAGED(LocalPlayerPed, FALSE)
						PRINTLN("[LM][PROCESS_VEH_BODY] - Player is in vehicle with ciFMMC_VEHICLE7_PLAYER_INVINCIBLE_IN_VEHICLE. Setting SET_ENTITY_CAN_BE_DAMAGED to false - iVeh: ", iVeh)				
						SET_BIT(iLocalboolCheck25, LBOOL25_CANNOT_BE_DAMAGED_DUE_TO_VEHICLE)
						iVehLastInsideGrantedInvulnability = iVeh
					ENDIF
				ELSE
					IF NOT GET_ENTITY_CAN_BE_DAMAGED(LocalPlayerPed)
					AND IS_BIT_SET(iLocalBoolCheck25, LBOOL25_CANNOT_BE_DAMAGED_DUE_TO_VEHICLE)
						SET_ENTITY_CAN_BE_DAMAGED(LocalPlayerPed, TRUE)
						PRINTLN("[LM][PROCESS_VEH_BODY] - Player in water with ciFMMC_VEHICLE7_PLAYER_INVINCIBLE_IN_VEHICLE. Setting SET_ENTITY_CAN_BE_DAMAGED to true - iVeh: ", iVeh)		
						CLEAR_BIT(iLocalBoolCheck25, LBOOL25_CANNOT_BE_DAMAGED_DUE_TO_VEHICLE)
						iVehLastInsideGrantedInvulnability = -1
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSeven, ciFMMC_VEHICLE7_PLAYER_INVINCIBLE_IN_VEHICLE)
					IF NOT GET_ENTITY_CAN_BE_DAMAGED(LocalPlayerPed)
					AND IS_BIT_SET(iLocalBoolCheck25, LBOOL25_CANNOT_BE_DAMAGED_DUE_TO_VEHICLE)
						SET_ENTITY_CAN_BE_DAMAGED(LocalPlayerPed, TRUE)
						PRINTLN("[LM][PROCESS_VEH_BODY] - Player left a vehicle with ciFMMC_VEHICLE7_PLAYER_INVINCIBLE_IN_VEHICLE. Setting SET_ENTITY_CAN_BE_DAMAGED to true - iVeh: ", iVeh)		
						CLEAR_BIT(iLocalBoolCheck25, LBOOL25_CANNOT_BE_DAMAGED_DUE_TO_VEHICLE)
						iVehLastInsideGrantedInvulnability = -1
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetEight, ciFMMC_VEHICLE8_OffRuleQuickGPS)
				MPGlobalsAmbience.vQuickGPS2 = GET_ENTITY_COORDS(tempVeh)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCustomVehName > -1
					MPGlobalsAmbience.tlQuickGPS_Offrule = g_FMMC_STRUCT_ENTITIES.tlCustomVehName[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCustomVehName]
				ENDIF
			ENDIF
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehicleIndexForColImmunity > -1
				IF NOT IS_BIT_SET(iShouldProcessVehiclColdmgBitset, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehicleIndexForColImmunity)
					SET_BIT(iShouldProcessVehiclColdmgBitset, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehicleIndexForColImmunity)
					PRINTLN("[LM][PROCESS_VEH_BODY] - Setting iShouldProcessVehiclColdmgBitset from ", iVeh, " for iVeh: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehicleIndexForColImmunity)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(iShouldProcessVehiclColdmgBitset, iVeh)
				INT iVehB = 0
				FOR iVehB = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles-1
					IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehB].iVehicleIndexForColImmunity > -1
						IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVehB])
							VEHICLE_INDEX vehTarget = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVehB])
						
							IF NOT IS_BIT_SET(iDisableVehiclColdmgBitset, iVehB)
								IF IS_ENTITY_ALIVE(vehTarget)
								AND IS_ENTITY_ALIVE(TempVeh)
									SET_ENTITY_CANT_CAUSE_COLLISION_DAMAGED_ENTITY(TempVeh, vehTarget)
									SET_BIT(iDisableVehiclColdmgBitset, iVehB)
									
									PRINTLN("[LM][PROCESS_VEH_BODY] - Setting iDisableVehiclColdmgBitset for iVehB: ", iVehB)
								ENDIF						
							ELSE
								IF IS_ENTITY_ALIVE(vehTarget)
								AND IS_ENTITY_ALIVE(TempVeh)
									CLEAR_BIT(iDisableVehiclColdmgBitset, iVehB)
									PRINTLN("[LM][PROCESS_VEH_BODY] - (1) Clearing iDisableVehiclColdmgBitset for iVehB: ", iVehB)
								ENDIF
							ENDIF						
						ELSE
							IF IS_BIT_SET(iDisableVehiclColdmgBitset, iVehB)
								PRINTLN("[LM][PROCESS_VEH_BODY] - (2) Clearing iDisableVehiclColdmgBitset for iVehB: ", iVehB)
								CLEAR_BIT(iDisableVehiclColdmgBitset, iVehB)
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
			
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_WARFARE(g_FMMC_STRUCT.iAdversaryModeType)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
					IF GET_ENTITY_MODEL(tempVeh) = CARGOBOB
					OR GET_ENTITY_MODEL(tempVeh) = CARGOBOB2
					OR GET_ENTITY_MODEL(tempVeh) = CARGOBOB4
						IF IS_VEHICLE_DRIVEABLE(tempVeh)
						AND GET_VEHICLE_DOOR_ANGLE_RATIO(tempVeh, SC_DOOR_REAR_LEFT) < 1
							SET_VEHICLE_DOOR_CONTROL(tempVeh, SC_DOOR_REAR_LEFT, DT_DOOR_SWINGING_FREE, 1)
							PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - MOTOR WARS BACKUP HELI DOOR OPEN")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			// Very specific for Heists or Special Missions with 4 players.
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSeven, ciFMMC_VEHICLE7_SPECIAL_VOLATOL_SEAT_PREF_OVERRIDE)
				IF iVehSeatPrefOverrideVeh != iVeh
					PRINTLN("[LM][PROCESS_VEH_BODY] - Setting iVehSeatPrefOverrideVeh to iVeh: ", iVeh)
					iVehSeatPrefOverrideVeh = iVeh
				ENDIF
			ENDIF
			
			PROCESS_VEHICLE_ON_RULE_WARP(iVeh, tempVeh)
			
			IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)		
				// This has to be done after the vehicle is no longer fixed / migrated. There needs to be a frame gap.
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehicleDoorSettingsOnRule = -1 	// [ML] Only set up the vehicle doors like this from spawn if there is not a rule set where these settings should be applied
				
					IF NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_FIRST_STAGGERED_LOOP_OF_VEH_BODY)
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet, enum_to_int(SC_DOOR_FRONT_LEFT))
							SET_VEHICLE_DOOR_OPEN(tempVeh,SC_DOOR_FRONT_LEFT,FALSE,TRUE)
							PRINTLN("[RCC MISSION] PROCESS_VEH_BODY (physics) - opening FL DOOR for vehicle: ",iveh) 
						ENDIF
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet, enum_to_int(SC_DOOR_FRONT_RIGHT))
							SET_VEHICLE_DOOR_OPEN(tempVeh,SC_DOOR_FRONT_RIGHT,FALSE,TRUE)
							PRINTLN("[RCC MISSION] PROCESS_VEH_BODY (physics) - opening FR DOOR for vehicle: ",iveh) 
						ENDIF
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet, enum_to_int(SC_DOOR_REAR_LEFT))
							SET_VEHICLE_DOOR_OPEN(tempVeh,SC_DOOR_REAR_LEFT,FALSE,TRUE)
							PRINTLN("[RCC MISSION] PROCESS_VEH_BODY (physics) - opening RL DOOR for vehicle: ",iveh) 
						ENDIF
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet, enum_to_int(SC_DOOR_REAR_RIGHT))
							SET_VEHICLE_DOOR_OPEN(tempVeh,SC_DOOR_REAR_RIGHT,FALSE,TRUE)
							PRINTLN("[RCC MISSION] PROCESS_VEH_BODY (physics) - opening RR DOOR for vehicle: ",iveh) 
						ENDIF
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet, enum_to_int(SC_DOOR_BONNET))
							SET_VEHICLE_DOOR_OPEN(tempVeh,SC_DOOR_BONNET,FALSE,TRUE)
							PRINTLN("[RCC MISSION] PROCESS_VEH_BODY (physics) - opening BONNET for vehicle: ",iveh) 
						ENDIF
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet, enum_to_int(SC_DOOR_BOOT))
							SET_VEHICLE_DOOR_OPEN(tempVeh,SC_DOOR_BOOT,FALSE,TRUE)
							PRINTLN("[RCC MISSION] PROCESS_VEH_BODY (physics) - opening BOOT for vehicle: ",iveh) 
						ENDIF
					ENDIF
				ENDIF
				
				IF MC_PlayerBD[iLocalPart].iteam > -1
					
					INT iTeamVeh = MC_PlayerBD[iLocalPart].iteam
					INT iRuleVeh = MC_ServerBD_4.iCurrentHighestPriority[iTeamVeh]	
					
					BOOL bPlayerIsDrivingTempVeh = FALSE
					
					IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
						IF GET_VEHICLE_PED_IS_IN(LocalPlayerPed) = tempVeh
							bPlayerIsDrivingTempVeh = TRUE
						ENDIF
					ENDIF
					
					IF GET_ENTITY_MODEL(tempVeh) = DELUXO
					AND g_FMMC_STRUCT.iMissionSubType != FMMC_MISSION_TYPE_CTF
					AND	g_FMMC_STRUCT.iMissionSubType != FMMC_MISSION_TYPE_LTS
						IF iRuleVeh < FMMC_MAX_RULES
						
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeamVeh].iRuleBitsetEleven[iRuleVeh], ciBS_RULE11_DISABLE_DELUXO_DRIVE)
							AND bPlayerIsDrivingTempVeh
								SET_SPECIAL_FLIGHT_MODE_TARGET_RATIO(tempVeh, 1)
								DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_TRANSFORM)
							ENDIF
							
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeamVeh].iRuleBitsetTen[iRuleVeh], ciBS_RULE10_DISABLE_DELUXO_FLIGHT_MODE)
								IF NOT IS_BIT_SET(iFlightModeEnabledForVeh, iVeh)
									SET_BIT(iFlightModeEnabledForVeh, iVeh)
									PRINTLN("[RCC MISSION][PROCESS_VEH_BODY] - Vehicle ", iveh," Special flight mode activated this rule. iRule: ", iRuleVeh)
									
									SET_SPECIAL_FLIGHT_MODE_ALLOWED(tempVeh, TRUE)
									SET_DISABLE_HOVER_MODE_FLIGHT(tempVeh, FALSE)
									
									/*IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_DELUXO_SOUND_UNLOCK_FLIGHT_PLAYED)
									AND IS_PED_IN_VEHICLE(localPlayerPed, tempVeh, FALSE)
										SET_BIT(iLocalboolCheck27, LBOOL27_DELUXO_SOUND_UNLOCK_FLIGHT_PLAYED)
										PLAY_SOUND_FRONTEND(-1, "Flight_Unlock", "DLC_XM17_IAA_Deluxos_Sounds")
									ENDIF*/
								ENDIF
							ELSE
								PRINTLN("[RCC MISSION][PROCESS_VEH_BODY] - Vehicle ", iveh," Disabling hover flight mode iRule: ", iRuleVeh)
								
								IF IS_BIT_SET(iLocalBoolCheck27, LBOOL27_DELUXO_SOUND_UNLOCK_FLIGHT_PLAYED)
									CLEAR_BIT(iLocalboolCheck27, LBOOL27_DELUXO_SOUND_UNLOCK_FLIGHT_PLAYED)
									PRINTLN("[RCC MISSION][PROCESS_VEH_BODY] - Vehicle ", iveh," Special flight mode Deactivated Clearing Sound Bit")
								ENDIF
								
								//ALlow hover if it's not disabled
								IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeamVeh].iRuleBitsetTen[iRuleVeh], ciBS_RULE10_DISABLE_DELUXO_HOVER_MODE)
									PRINTLN("[RCC MISSION][PROCESS_VEH_BODY] - Vehicle ", iveh," Re-enabling hover flight mode iRule: ", iRuleVeh)
									
									SET_SPECIAL_FLIGHT_MODE_ALLOWED(tempVeh, TRUE)
									SET_DISABLE_HOVER_MODE_FLIGHT(tempVeh, TRUE)
									
									/*IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_DELUXO_SOUND_UNLOCK_HOVER_PLAYED)
									AND IS_PED_IN_VEHICLE(localPlayerPed, tempVeh, FALSE)
										SET_BIT(iLocalboolCheck27, LBOOL27_DELUXO_SOUND_UNLOCK_HOVER_PLAYED)
										PLAY_SOUND_FRONTEND(-1, "Hover_Unlock", "DLC_XM17_IAA_Deluxos_Sounds")
									ENDIF*/
								ELSE
									PRINTLN("[RCC MISSION][PROCESS_VEH_BODY] - Vehicle ", iveh," Re-disabling hover flight mode iRule: ", iRuleVeh)
									SET_SPECIAL_FLIGHT_MODE_ALLOWED(tempVeh, FALSE)
									SET_DISABLE_HOVER_MODE_FLIGHT(tempVeh, FALSE)
								ENDIF
							ENDIF
							
						ENDIF
					ENDIF
				ENDIF
				
				HANDLE_SETTING_VEHS_ON_FIRE_FOR_RIOTVAN_MISSION_CLIENT(iveh, tempVeh)
								
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix,  ciFMMC_VEHICLE6_VEHICLE_IS_INVINCIBLE_WHEN_EMPTY)
					IF IS_VEHICLE_EMPTY(tempVeh)
						IF GET_ENTITY_CAN_BE_DAMAGED(tempVeh)
							SET_ENTITY_CAN_BE_DAMAGED(tempVeh, FALSE)
							PRINTLN("[LM][PROCESS_VEH_BODY] - Vehicle Is Empty. Setting SET_ENTITY_CAN_BE_DAMAGED to false for iVeh: ", iVeh)
						ENDIF
					ELSE
						IF NOT GET_ENTITY_CAN_BE_DAMAGED(tempVeh)
							SET_ENTITY_CAN_BE_DAMAGED(tempVeh, TRUE)
							PRINTLN("[LM][PROCESS_VEH_BODY] - Vehicle no longer Empty. Setting SET_ENTITY_CAN_BE_DAMAGED to true for iVeh: ", iVeh)
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_PED_INJURED(localPlayerPed)
					IF IS_PED_IN_VEHICLE(LocalPlayerPed, tempVeh, FALSE)
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix,  ciFMMC_VEHICLE6_DISABLE_HOMING_MISSILES)
							IF IS_VEHICLE_DRIVEABLE(tempVeh)
								IF DOES_VEHICLE_HAVE_WEAPONS(tempVeh)
									IF NOT IS_BIT_SET(iLocalBoolCheck25, LBOOL25_VEHICLE_WEAPONS_HOMING_DISABLED)
										SET_PLAYER_HOMING_DISABLED_FOR_ALL_VEHICLE_WEAPONS(localPlayer, TRUE)
										SET_BIT(iLocalBoolCheck25, LBOOL25_VEHICLE_WEAPONS_HOMING_DISABLED)
										
										IF DOES_ENTITY_EXIST(tempVeh)
											IF GET_ENTITY_MODEL(tempVeh) = HUNTER
												DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_HUNTER_MISSILE, tempVeh, LocalPlayerPed)
											ENDIF
										ENDIF
										
										PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - iVeh ", iveh, " Blocking Homing Missiles.")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELIF NOT IS_PED_IN_ANY_VEHICLE(localPlayerPed)
						IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_VEHICLE_WEAPONS_HOMING_DISABLED)
							SET_PLAYER_HOMING_DISABLED_FOR_ALL_VEHICLE_WEAPONS(localPlayer, FALSE)
							CLEAR_BIT(iLocalBoolCheck25, LBOOL25_VEHICLE_WEAPONS_HOMING_DISABLED)
							
							IF DOES_ENTITY_EXIST(tempVeh)
								IF GET_ENTITY_MODEL(tempVeh) = HUNTER
									DISABLE_VEHICLE_WEAPON(FALSE, WEAPONTYPE_DLC_VEHICLE_HUNTER_MISSILE, tempVeh, LocalPlayerPed)
								ENDIF
							ENDIF
							
							PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - iVeh ", iveh, " Clearing Blocked Homing Missiles.")
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFive, ciFMMC_VEHICLE5_EXTINGUISH_FIRE_ON_VEH_HIGH_HEALTH)
					IF GET_ENTITY_HEALTH(tempVeh) > GET_ENTITY_MAX_HEALTH(tempVeh) * 0.2
						IF IS_ENTITY_ON_FIRE(tempVeh)
							PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - Veh ", iveh, " was on fire. Extinguishing. ", GET_ENTITY_HEALTH(tempVeh), " / ", GET_ENTITY_MAX_HEALTH(tempVeh))
							STOP_ENTITY_FIRE(tempVeh)
						ENDIF
					ENDIF
				ENDIF
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive, ciFMMC_VEHICLE5_PREVENT_HELI_ROTOR_DAMAGE)
					IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(tempVeh))
						IF NOT IS_BIT_SET(iHeliRotorHealthCacheBitset, iVeh)
							fHeliMainRotorHealthCache[iVeh] = GET_HELI_MAIN_ROTOR_HEALTH(tempVeh)
							fHeliTailRotorHealthCache[iVeh] = GET_HELI_TAIL_ROTOR_HEALTH(tempVeh)
							
							SET_HELI_TAIL_BOOM_CAN_BREAK_OFF(tempVeh, FALSE)
							
							SET_BIT(iHeliRotorHealthCacheBitset, iVeh)
						ELSE
							IF GET_HELI_MAIN_ROTOR_HEALTH(tempVeh) != fHeliMainRotorHealthCache[iVeh]
								SET_HELI_MAIN_ROTOR_HEALTH(tempVeh, fHeliMainRotorHealthCache[iVeh])
							ENDIF
							
							IF GET_HELI_TAIL_ROTOR_HEALTH(tempVeh) != fHeliTailRotorHealthCache[iVeh]
								SET_HELI_TAIL_ROTOR_HEALTH(tempVeh, fHeliTailRotorHealthCache[iVeh])
							ENDIF
						ENDIF
					ELIF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(tempVeh))
						SET_PLANE_PROPELLER_HEALTH(tempVeh, 1000.0)
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSeven, ciFMMC_VEHICLE7_KEEP_IN_BOUNDS)
				
					IF IS_ENTITY_OUT_OF_BOUNDS(tempVeh, 0)
					AND IS_VEHICLE_EMPTY(tempVeh, TRUE) AND (NOT IS_VEHICLE_A_TRAILER(tempVeh) OR (IS_VEHICLE_A_TRAILER(tempVeh) AND NOT IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(tempVeh)))
					AND NOT IS_PED_IN_VEHICLE(LocalPlayerPed, tempVeh, TRUE)
						IF NOT HAS_NET_TIMER_STARTED(tdKeepVehicleInBounds[iveh])
							REINIT_NET_TIMER(tdKeepVehicleInBounds[iVeh])
						ELSE
							IF HAS_NET_TIMER_EXPIRED(tdKeepVehicleInBounds[iveh], ciKeepVehicleInBoundsTimer)
								NETWORK_EXPLODE_VEHICLE(tempVeh, TRUE)
								RESET_NET_TIMER(tdKeepVehicleInBounds[iVeh])
							ENDIF
						ENDIF
					ELSE
						IF HAS_NET_TIMER_STARTED(tdKeepVehicleInBounds[iveh])
							RESET_NET_TIMER(tdKeepVehicleInBounds[iveh])	
						ENDIF
					ENDIF
				ENDIF
				//url:bugstar:4154453 
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = AVENGER
				AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen,ciENABLE_AVENGER_INTERIOR_ENTRY)
					IF IS_ENTITY_ALIVE(tempVeh)
					AND IS_ENTITY_IN_AIR(tempVeh)
						IF IS_VEHICLE_EMPTY(tempVeh)
						AND NOT IS_ANY_PLAYER_IN_VEHICLE_INTERIOR()
							IF NOT HAS_NET_TIMER_STARTED(tdEmptyAvengerTimer)
								REINIT_NET_TIMER(tdEmptyAvengerTimer)
								PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - Starting crash timer for Avenger. Vehicle: ", iVeh)
							ELSE
								IF HAS_NET_TIMER_EXPIRED(tdEmptyAvengerTimer, ciForceAvengerCrash)
									CLEAR_PRIMARY_VEHICLE_TASK(tempVeh)
									BROADCAST_EVENT_AVENGER_AUTOPILOT_ACTIVATED(GET_OWNER_OF_CREATOR_AIRCRAFT(tempVeh), FALSE)
									PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - Crashing Avenger as it has been empty and in the air for 5 seconds")
								ENDIF
							ENDIF
						ELSE
							IF HAS_NET_TIMER_STARTED(tdEmptyAvengerTimer)
								PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - Resetting Avenger Timer 1")
								RESET_NET_TIMER(tdEmptyAvengerTimer)
							ENDIF
						ENDIF
					ELSE
						IF HAS_NET_TIMER_STARTED(tdEmptyAvengerTimer)
							PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - Resetting Avenger Timer 2")
							RESET_NET_TIMER(tdEmptyAvengerTimer)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				HANDLE_SETTING_VEHS_ON_FIRE_FOR_RIOTVAN_MISSION_CLIENT_NONHOST(iveh, tempVeh)
			ENDIF
						
			IF DOES_ENTITY_EXIST(tempVeh)
			AND NOT IS_PLAYER_SPECTATING(localPlayer)
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSeven, ciFMMC_VEHICLE7_OPEN_BOMB_BAY_DOORS_HELP_TEXT)
				AND IS_ENTITY_IN_AIR(TempVeh)
				AND IS_PED_IN_ANY_VEHICLE(localPlayerPed, FALSE)
				AND IS_PED_IN_VEHICLE(localPlayerPed, TempVeh)	
					BOOL bHasControl = FALSE					
					IF GET_SEAT_PED_IS_IN(localPlayerPed) = VS_FRONT_RIGHT
					AND MPGlobalsAmbience.bEnablePassengerBombing
						bHasControl = TRUE
					ELIF GET_SEAT_PED_IS_IN(localPlayerPed) = VS_DRIVER
					AND NOT MPGlobalsAmbience.bEnablePassengerBombing
						bHasControl = TRUE
					ENDIF
					
					IF bHasControl
						IF NOT GET_ARE_BOMB_BAY_DOORS_OPEN(tempVeh)
							IF IS_BIT_SET(iLocalBoolCheck27, LBOOL27_BOMB_BAY_HELP_TEXT_PLAYED_OPEN)
								CLEAR_BIT(iLocalBoolCheck27, LBOOL27_BOMB_BAY_HELP_TEXT_PLAYED_OPEN)
								PRINTLN("[LM][PROCESS_VEH_BODY] - bomb bay doors are closed, clearing bit to re-allow help text (Use Controls).")
							ENDIF
							IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_BOMB_BAY_HELP_TEXT_PLAYED)
								SET_BIT(iLocalBoolCheck27, LBOOL27_BOMB_BAY_HELP_TEXT_PLAYED)
								IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BOMB_CAM_HELP")
									PRINT_HELP("BOMB_CAM_HELP", 10000)
									PRINTLN("[LM][PROCESS_VEH_BODY] - bomb bay doors are closed, Playing help text. (Open Doors)")
								ENDIF
							ENDIF
						ELIF GET_ARE_BOMB_BAY_DOORS_OPEN(tempVeh)
							IF IS_BIT_SET(iLocalBoolCheck27, LBOOL27_BOMB_BAY_HELP_TEXT_PLAYED) 
								CLEAR_BIT(iLocalBoolCheck27, LBOOL27_BOMB_BAY_HELP_TEXT_PLAYED)
								PRINTLN("[LM][PROCESS_VEH_BODY] - bomb bay doors are open, clearing bit to re-allow help text (Open Doors).")
							ENDIF
							IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_BOMB_BAY_HELP_TEXT_PLAYED_OPEN)
								SET_BIT(iLocalBoolCheck27, LBOOL27_BOMB_BAY_HELP_TEXT_PLAYED_OPEN)
								IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BOMB_HELP")
									PRINT_HELP("BOMB_HELP", 10000)
									PRINTLN("[LM][PROCESS_VEH_BODY] - bomb bay doors are open, Playing help text. (Use Controls)")
								ENDIF
							ENDIF
						ENDIF					
					ENDIF
				ENDIF
			ENDIF
			
			//Detecting Objective Vehicles Connected to Cargobob
			IF IS_OBJECTIVE_VEHICLE_ON_PLAYERS_CARGOBOB(iVeh, tempVeh)
				SET_BIT(iObjectiveVehicleAttachedToCargobobBS, iVeh)
				PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - Setting iObjectiveVehicleAttachedToCargobobBS for veh ", iVeh)
			ELSE
				IF IS_BIT_SET(iObjectiveVehicleAttachedToCargobobBS, iVeh)
					CLEAR_BIT(iObjectiveVehicleAttachedToCargobobBS, iVeh)
					PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - Clearing iObjectiveVehicleAttachedToCargobobBS for veh ", iVeh)
				ENDIF
			ENDIF
			
			//Prevents the player from manually respawning
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_PREVENT_MANUAL_RESPAWN)
				IF IS_PED_IN_VEHICLE(LocalPlayerPed, tempVeh)
					IF NOT IS_BIT_SET(iLocalBoolCheck17, LBOOL17_PREVENT_VEHICLE_MANUAL_RESPAWN)
						PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - Preventing manual respawn in this vehicle")
						SET_BIT(iLocalBoolCheck17, LBOOL17_PREVENT_VEHICLE_MANUAL_RESPAWN)
					ENDIF
				ELSE
					IF IS_BIT_SET(iLocalBoolCheck17, LBOOL17_PREVENT_VEHICLE_MANUAL_RESPAWN)
						PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - Reenabling manual respawning as this player has just exited a restricted vehicle")
						CLEAR_BIT(iLocalBoolCheck17, LBOOL17_PREVENT_VEHICLE_MANUAL_RESPAWN)
					ENDIF
				ENDIF
			ENDIF
						
			IF DOES_BLIP_EXIST(biVehBlip[iveh]) 
				IF NOT DOES_BLIP_EXIST(GET_BLIP_FROM_ENTITY(tempVeh))
					CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION] Removing blip for vehicle ", iveh, ". The blip was on an old vehicle.")
					REMOVE_VEHICLE_BLIP(iveh)
				ENDIF
			ENDIF
			
			IF IS_PED_IN_VEHICLE(LocalPlayerPed, tempVeh)
				IF iLastUsedPlacedVeh != iVeh
					IF (IS_THIS_ROCKSTAR_MISSION_WVM_OPPRESSOR_1(g_FMMC_STRUCT.iRootContentIDHash) AND GET_ENTITY_MODEL(tempveh) = OPPRESSOR)
					OR NOT IS_THIS_ROCKSTAR_MISSION_WVM_OPPRESSOR_1(g_FMMC_STRUCT.iRootContentIDHash)
						PRINTLN("[JS] - Setting last used placed vehicle as ", iveh)
						iLastUsedPlacedVeh = iVeh
					ENDIF
				ENDIF
				
				IF MC_playerBD[iLocalPart].iTeam > -1
				AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iRespawnInLastMissionVehicleFromBS, iVeh)
				AND iLastOccupiedRespawnListVehicle != iVeh
					PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - Setting iLastOccupiedRespawnListVehicle as vehicle ",iVeh)
					iLastOccupiedRespawnListVehicle = iVeh
				ENDIF
				
				IF MC_playerBD[iLocalPart].iTeam > -1
				AND MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam] < FMMC_MAX_RULES
				AND g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iRespawnInMissionVehicle[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]] = iVeh
					vsLastOccupiedVehicleSeat = GET_SEAT_PED_IS_IN(LocalPlayerPed)
					PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - Setting vsLastOccupiedVehicleSeat as ",ENUM_TO_INT(vsLastOccupiedVehicleSeat))
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFive, ciFMMC_VEHICLE5_OCCUPANTS_EXPLOSION_PROOF)
				AND NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_VEHICLE_SET_EXPLOSION_PROOF)
					PRINTLN("[JS] - Making player explosion proof as in vehicle ", iveh)
					RUN_LOCAL_PLAYER_PROOF_SETTING(TRUE, TRUE)
					SET_BIT(iLocalBoolCheck23, LBOOL23_VEHICLE_SET_EXPLOSION_PROOF)
				ELSE
					#IF IS_DEBUG_BUILD
						IF (GET_FRAME_COUNT() % 15) = 0 
							PRINTLN("[RCC MISSION] ciFMMC_VEHICLE5_OCCUPANTS_EXPLOSION_PROOF option on? ", BOOL_TO_STRING(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFive, ciFMMC_VEHICLE5_OCCUPANTS_EXPLOSION_PROOF)))
							PRINTLN("[RCC MISSION] IS_BIT_SET(iLocalBoolCheck23, LBOOL23_VEHICLE_SET_EXPLOSION_PROOF): ", BOOL_TO_STRING(IS_BIT_SET(iLocalBoolCheck23, LBOOL23_VEHICLE_SET_EXPLOSION_PROOF)))
						ENDIF
					#ENDIF
				ENDIF
				IF NOT g_EnableKersHelp
				AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = LECTRO
					IF HAS_NET_TIMER_STARTED(MC_serverBD.tdMissionLengthTimer) 
					AND GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdMissionLengthTimer) > 30000 //url:bugstar:4867662
						PRINTLN("[RCC MISSION] [PROCESS_VEH_BODY] - Vehicle is LECTRO ( KERS ). Enabling Kers help text")
						g_EnableKersHelp = TRUE
					ELSE
						g_EnableKersHelp = FALSE
						PRINTLN("[RCC MISSION] [PROCESS_VEH_BODY] - Vehicle is LECTRO ( KERS ). Disabling Kers help text")
					ENDIF
				ENDIF
			ENDIF
			
			// Print help text about entering the gunner seat on the Technical and Insurgent
			// B*2211177 
			IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
			OR Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
				IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_HEIST_HELP_TEXT_INSURGENT_TECHNICAL)
				AND NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)		// url:bugstar:2221253 - ensurng message doesn't show if already in a vehicle. ST.
				AND NOT SHOULD_VEHICLE_BE_LOCKED_FOR_TEAM(iveh, MC_playerBD[iPartToUse].iteam)
					IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = INSURGENT
						// Check if the gunner seat is free
						IF IS_VEHICLE_SEAT_FREE(tempVeh, VS_EXTRA_LEFT_3)
						OR IS_PED_INJURED(GET_PED_IN_VEHICLE_SEAT(tempVeh, VS_EXTRA_LEFT_3))// url:bugstar:2224707 - Making sure the help text appears if the ped in the gunner seat is dead. RS.
							PRINTLN("[RCC MISSION]- Checking if player is near INSURGENT")
							//check if player is near vehicle
							IF IS_ENTITY_AT_ENTITY(LocalPlayerPed, tempVeh, <<4, 4, 4>>)
								PRINTLN("[RCC MISSION]- PLAYER IS AT INSURGENT")
								PRINT_HELP("HEIST_HELP_63", 10000)
								SET_BIT(iLocalBoolCheck10, LBOOL10_HEIST_HELP_TEXT_INSURGENT_TECHNICAL)
							ENDIF
						ENDIF
					ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = TECHNICAL
						// Check if the gunner seat is free
						IF IS_VEHICLE_SEAT_FREE(tempVeh, VS_BACK_LEFT)
						OR IS_PED_INJURED(GET_PED_IN_VEHICLE_SEAT(tempVeh, VS_BACK_LEFT))	// url:bugstar:2224707 - Making sure the help text appears if the ped in the gunner seat is dead. RS.
							PRINTLN("[RCC MISSION] - Checking if player is near TECHNICAL")
							//check if player is near vehicle
							IF IS_ENTITY_AT_ENTITY(LocalPlayerPed, tempVeh, <<4, 4, 4>>)
								PRINTLN("[RCC MISSION]- PLAYER IS AT TECHNICAL")
								PRINT_HELP("HEIST_HELP_63", 10000)
								SET_BIT(iLocalBoolCheck10, LBOOL10_HEIST_HELP_TEXT_INSURGENT_TECHNICAL)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					// Clear gunner help text when in the vehicle or if a ped is in the gunner seat
					// B*2220476
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_HELP_63")
						IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = INSURGENT
							IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
							OR NOT IS_VEHICLE_SEAT_FREE(tempVeh, VS_EXTRA_LEFT_3)
							OR NOT IS_ENTITY_AT_ENTITY(LocalPlayerPed, tempVeh, <<5, 5, 5>>)
								PRINTLN("[RCC MISSION] - CLEARING GUNNER HELP TEXT")
								CLEAR_HELP(TRUE)
							ENDIF
						ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = TECHNICAL
							IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
							OR NOT IS_VEHICLE_SEAT_FREE(tempVeh, VS_BACK_LEFT)
							OR NOT IS_ENTITY_AT_ENTITY(LocalPlayerPed, tempVeh, <<5, 5, 5>>)
								PRINTLN("[RCC MISSION] - CLEARING GUNNER HELP TEXT")
								CLEAR_HELP(TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_THIS_ROCKSTAR_MISSION_SVM_RUINER2(g_FMMC_STRUCT.iRootContentIDHash)
					IF IS_PED_IN_VEHICLE(LocalPlayerPed, tempVeh, TRUE)
					AND GET_ENTITY_MODEL(tempVeh) = RUINER2
						IF NOT IS_BIT_SET(iLocalBoolCheck21, LBOOL21_RUINER_2000_WEAPON_SET)
							SET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_VEHICLE_RUINER_ROCKET)
							SET_BIT(iLocalBoolCheck21, LBOOL21_RUINER_2000_WEAPON_SET)
							PRINTLN("[JT RUINER] Setting weapon. WEAPONTYPE_DLC_VEHICLE_RUINER_ROCKET")
						ENDIF
					ELSE
						IF NOT IS_PED_IN_ANY_VEHICLE(localPlayerPed)
							IF IS_BIT_SET(iLocalBoolCheck21, LBOOL21_RUINER_2000_WEAPON_SET)
								CLEAR_BIT(iLocalBoolCheck21, LBOOL21_RUINER_2000_WEAPON_SET)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				
				IF IS_PED_IN_VEHICLE(LocalPlayerPed, tempVeh, TRUE)				
					IF GET_ENTITY_MODEL(tempVeh) = DUNE4
						IF NOT IS_BIT_SET(iLocalBoolCheck21, LBOOL21_DUNE4_RAMP_BUGGY_SIDE_IMPULSE_ACTIVE)
							PRINTLN("[RCC MISSION] SETTING VEHICLE_SET_ENABLE_RAMP_CAR_SIDE_IMPULSE for vehicle: ", iveh)
							VEHICLE_SET_ENABLE_RAMP_CAR_SIDE_IMPULSE(tempVeh, TRUE)
							SET_BIT(iLocalBoolCheck21, LBOOL21_DUNE4_RAMP_BUGGY_SIDE_IMPULSE_ACTIVE)
						ENDIF
					ENDIF
				ELIF NOT IS_PED_IN_ANY_VEHICLE(localPlayerPed)
					IF IS_BIT_SET(iLocalBoolCheck21, LBOOL21_DUNE4_RAMP_BUGGY_SIDE_IMPULSE_ACTIVE)
						CLEAR_BIT(iLocalBoolCheck21, LBOOL21_DUNE4_RAMP_BUGGY_SIDE_IMPULSE_ACTIVE)
					ENDIF
				ENDIF
				
				// Insurgent Gun Seat Help
				IF NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_INSURGENT_GUN_SEAT_HELP)
				AND NOT IS_BIT_SET(g_iFmNmhBitSet8, BI_FM_NMH8_TURRET_HELP)
					IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = INSURGENT
						IF IS_PED_IN_VEHICLE(LocalPlayerPed, tempVeh)
							IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
								IF IS_VEHICLE_SEAT_FREE(tempVeh, VS_EXTRA_LEFT_3)
									VEHICLE_SEAT vehSeat = GET_SEAT_PED_IS_IN(LocalPlayerPed)
									
									IF vehSeat = VS_BACK_LEFT OR vehSeat = VS_BACK_RIGHT
										PRINT_HELP("FMMC_GUN_SEAT_HELP")
										PRINTLN("[RCC MISSION] - PRINT_HELP FMMC_GUN_SEAT_HELP")
										SET_BIT(iLocalBoolCheck14, LBOOL14_INSURGENT_GUN_SEAT_HELP)
										SET_BIT(g_iFmNmhBitSet8, BI_FM_NMH8_TURRET_HELP)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FMMC_GUN_SEAT_HELP")
						IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = INSURGENT
							IF IS_PED_IN_VEHICLE(LocalPlayerPed, tempVeh)
								VEHICLE_SEAT vehSeat = GET_SEAT_PED_IS_IN(LocalPlayerPed)
								
								IF vehSeat = VS_EXTRA_LEFT_3
								//OR NOT IS_VEHICLE_SEAT_FREE(tempVeh, VS_EXTRA_LEFT_3)
									PRINTLN("[RCC MISSION] - CLEAR_HELP FMMC_GUN_SEAT_HELP")
									CLEAR_HELP(TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(biVehBlip[iveh]) 
			CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION] Removing blip for vehicle ", iveh, ". The vehicle netID doesn't exist.")
			REMOVE_VEHICLE_BLIP(iveh)
		ENDIF
		IF IS_BIT_SET(iTitanRotorParticlesBitset, iVeh)
			CLEAR_BIT(iTitanRotorParticlesBitset, iVeh)
			STOP_PARTICLE_FX_LOOPED(ptfxTitanRotorParticles[iVeh][0])
			STOP_PARTICLE_FX_LOOPED(ptfxTitanRotorParticles[iVeh][1])
			STOP_PARTICLE_FX_LOOPED(ptfxTitanRotorParticles[iVeh][2])
			STOP_PARTICLE_FX_LOOPED(ptfxTitanRotorParticles[iVeh][3])
			IF iTitanRotorParticlesBitset = 0
				REMOVE_NAMED_PTFX_ASSET("scr_gr_def")
			ENDIF
		ENDIF
		
		CLEAR_BIT(iLocalBoolCheck26, LBOOL26_OVERRIDING_SEAT_PREF_SPECIAL_VEH)
	
		IF IS_BIT_SET(iDisableVehiclColdmgBitset, iVeh)	
			PRINTLN("[LM][PROCESS_VEH_BODY] - (1) Clearing iDisableVehiclColdmgBitset for iVeh: ", iVeh)
			CLEAR_BIT(iDisableVehiclColdmgBitset, iVeh)	
		ENDIF
	
		CLEANUP_COLLISION_ON_VEH(NULL,iveh)
		
		EXIT
	ENDIF
	
	
		
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehTeamHorn > -1
		IF g_FMMC_STRUCT.iTeamHorn[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehTeamHorn] > -1
			IF g_FMMC_STRUCT.iTeamHorn[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehTeamHorn] != GET_VEHICLE_MOD(tempVeh, MOD_HORN)
				PRINTLN("[JS] Applying horn: ", GET_HORN_LABEL_FROM_MOD_INDEX(g_FMMC_STRUCT.iTeamHorn[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehTeamHorn]))
				SET_VEHICLE_MOD(tempVeh, MOD_HORN, g_FMMC_STRUCT.iTeamHorn[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehTeamHorn], FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bFMMCVehBodyNetVehIDprints
		INT iNI = NATIVE_TO_INT(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
		INT iNTVI = NATIVE_TO_INT(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]))
		INT iVI = NATIVE_TO_INT(tempVeh)
		INT iVTNI = NATIVE_TO_INT(VEH_TO_NET(tempVeh))
		
		PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - bFMMCVehBodyNetVehIDprints 1 - vehicle ",iveh," has network index ",iNI,", NET_TO_VEH on that = ",iNTVI," / has tempVeh ",iVi,", VEH_TO_NET on that = ",iVTNI)
	ENDIF
	#ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSeven, ciFMMC_VEHICLE7_ENGINE_KNOCKBACK)		
	AND GET_ENTITY_MODEL(tempVeh) = CARGOPLANE
		EXT2_CREATE_CLOUD_PTFX(tempVeh)
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalVehInit,iveh)
	//	SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(tempVeh, FALSE) // This line is causing 2063965
		
		//lock vehicle doors if vehicle has crates
		//-- Dave W: I've moved this to SET_UP_FMMC_VEH_RC() as SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED is now sync'd
		
		
//		IF GET_VEHICLE_CRATE_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet,g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetTwo,tempVeh) != DUMMY_MODEL_FOR_SCRIPT
//			IF NOT IS_ENTITY_DEAD(tempVeh)
//				SWITCH GET_VEHICLE_CRATE_DOORS_COUNT(tempVeh)
//					CASE 4
//						SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(tempVeh, ENUM_TO_INT(SC_DOOR_REAR_LEFT), VEHICLELOCK_LOCKED)
//						SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(tempVeh, ENUM_TO_INT(SC_DOOR_REAR_RIGHT), VEHICLELOCK_LOCKED)
//						NET_PRINT("LOCKING VEHICLE SC_DOOR_REAR_LEFT & SC_DOOR_REAR_RIGHT FOR VEHICLE CRATES") NET_NL()
//					BREAK
//					CASE 5
//						SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(tempVeh, ENUM_TO_INT(SC_DOOR_BOOT), VEHICLELOCK_LOCKED)
//						NET_PRINT("LOCKING VEHICLE SC_DOOR_BOOT FOR VEHICLE CRATES") NET_NL()
//					BREAK
//				ENDSWITCH
//			ENDIF
//		ENDIF
		
		SET_BIT(iLocalVehInit,iveh)
	ENDIF
	

	MAINTAIN_VEH_COLLISION_CLEANUP(iveh, tempVeh)
	
	IF iLocalDriverPart[iveh] != MC_serverBD.iDriverPart[iveh]
		IF MC_serverBD.iDriverPart[iveh] !=-1
			IF DOES_BLIP_EXIST(biVehBlip[iveh])
				SET_BLIP_TEAM_SECONDARY_COLOUR(biVehBlip[iveh],TRUE,MC_playerBD[MC_serverBD.iDriverPart[iveh]].iteam)
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(biVehBlip[iveh])
				SET_BLIP_TEAM_SECONDARY_COLOUR(biVehBlip[iveh],FALSE,-1)
			ENDIF
		ENDIF
		IF DOES_BLIP_EXIST(biVehBlip[iveh])
			CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Creating blip for vehicle ", iveh, ". iLocalDriverPart[iveh]: ", iLocalDriverPart[iveh], " != MC_serverBD.iDriverPart[iveh]: ", MC_serverBD.iDriverPart[iveh])
			REMOVE_VEHICLE_BLIP(iveh)
		ENDIF
		
		iLocalDriverPart[iveh] = MC_serverBD.iDriverPart[iveh]
	ENDIF
	
	HANDLE_VEHICLE_BLIP_CREATION(iVeh, tempVeh, iPColour, iSColourTeam)
	
	INT iPlayerTeam = MC_playerBD[ iPartToUse ].iteam
	//QUICK GPS
	IF SHOULD_VEHICLE_HAVE_CUSTOM_BLIP(iVeh)
	AND NOT (IS_SVM_VEHICLE(tempVeh) OR IS_WVM_VEHICLE(tempVeh) OR SPECIALCASE_IGNORE_VEHICLE_AS_TARGET(tempVeh, iVeh))
	AND NOT IS_PED_IN_VEHICLE(LocalPlayerPed, tempVeh)
	AND MC_serverBD_4.iVehPriority[iVeh][iPlayerTeam] = MC_ServerBD_4.iCurrentHighestPriority[iPlayerTeam] 
	AND MC_serverBD_4.iVehPriority[iVeh][iPlayerTeam] < FMMC_MAX_RULES
		IF NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_DUMMY_BLIP_OVERRIDE)
			FLOAT fClosestDistance = GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), GET_ENTITY_COORDS(tempVeh))
			IF fClosestDistance < fNearestTargetDistTemp
				iNearestTargetTemp = iveh
				iNearestTargetTypeTemp = ci_TARGET_VEHICLE
				fNearestTargetDistTemp = fClosestDistance
				PRINTLN("[RCC MISSION] CUSTOM VEH BLIP - UPDATED ")
				PRINTLN("[RCC MISSION] CUSTOM VEH BLIP - iNearestTargetTemp ", iNearestTargetTemp)
				PRINTLN("[RCC MISSION] CUSTOM VEH BLIP - iNearestTargetTypeTemp ", iNearestTargetTypeTemp)
				PRINTLN("[RCC MISSION] CUSTOM VEH BLIP - fNearestTargetDistTemp ", fNearestTargetDistTemp)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix, ciFMMC_VEHICLE6_TITAN_DAMAGED_PARTICLES)
	AND GET_ENTITY_MODEL(tempVeh) = TITAN
		
		REQUEST_NAMED_PTFX_ASSET("scr_gr_def")
		
		IF HAS_NAMED_PTFX_ASSET_LOADED("scr_gr_def")
			IF NOT IS_BIT_SET(iTitanRotorParticlesBitset, iVeh)
			AND GET_VEHICLE_ENGINE_HEALTH(tempVeh) / GET_FMMC_VEHICLE_MAX_ENGINE_HEALTH(iVeh) < 0.70 
				UNUSED_PARAMETER(ptfxTitanRotorParticles)
				SET_BIT(iTitanRotorParticlesBitset, iVeh)
				USE_PARTICLE_FX_ASSET("scr_gr_def")
				ptfxTitanRotorParticles[iVeh][0] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_gr_sw_engine_smoke", tempVeh, <<-10.5,-2.1,2.9>>, <<0,0,0>>)
				USE_PARTICLE_FX_ASSET("scr_gr_def")
				ptfxTitanRotorParticles[iVeh][1] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_gr_sw_engine_smoke", tempVeh, <<-4.5,-2.1,2.9>>, <<0,0,0>>)
				USE_PARTICLE_FX_ASSET("scr_gr_def")
				ptfxTitanRotorParticles[iVeh][2] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_gr_sw_engine_smoke", tempVeh, <<4.5,-2.1,2.9>>, <<0,0,0>>)
				USE_PARTICLE_FX_ASSET("scr_gr_def")
				ptfxTitanRotorParticles[iVeh][3] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_gr_sw_engine_smoke", tempVeh, <<10.5,-2.1,2.9>>, <<0,0,0>>)
			ENDIF	
		ENDIF
	ENDIF
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
		IF MC_serverBD_4.iVehPriority[iveh][MC_playerBD[iPartToUse].iteam] > MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
		OR (MC_serverBD_4.ivehRule[iveh][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD AND IS_BIT_SET(MC_serverBD.iVehAtYourHolding[MC_playerBD[iPartToUse].iteam],iveh))
		OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitset[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]],ciBS_RULE_ENABLETRACKIFY)
			IF DOES_BLIP_EXIST(biVehBlip[iveh])
				IF NOT SHOULD_VEHICLE_HAVE_CUSTOM_BLIP(iVeh)
					CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION] Removing blip for vehicle ", iveh, ". The vehicle is in a Collect and Hold drop-off.")
					REMOVE_VEHICLE_BLIP(iveh)
				ENDIF
			ENDIF
		ELSE
			IF IS_VEHICLE_DRIVEABLE(tempVeh)
				
				IF (MC_serverBD_4.ivehRule[iveh][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_GO_TO
				OR MC_serverBD_4.ivehRule[iveh][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER)
				AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetThree[MC_serverBD_4.iVehPriority[iveh][MC_playerBD[iPartToUse].iteam]], ciBS_RULE3_ALL_IN_SEPARATE_VEHICLES)
				AND DOES_BLIP_EXIST(biVehBlip[iveh])
					
					BOOL bCheckOnRule // To get it compiling
					INT iTeamsBS = GET_VEHICLE_GOTO_TEAMS(MC_playerBD[iPartToUse].iteam, iveh, bCheckOnRule)
										
					// If any participant on a team involved in the rule is in this vehicle
					// we don't want to blip it.
					IF NOT SHOULD_VEHICLE_HAVE_CUSTOM_BLIP(iveh)
						IF ARE_ANY_TEAM_PARICIPANTS_IN_VEHICLE(iveh, iTeamsBS)
							CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION] Removing blip for vehicle ", iveh, ". A participant on the vehicle's rule has entered the vehicle.")
							REMOVE_VEHICLE_BLIP(iveh)
						ENDIF
					ELSE
						iTeamsBS = 15
						INT iTeamInVeh
						IF ARE_ANY_PARTICIPANTS_IN_VEHICLE(iveh,iTeamsBS,iTeamInVeh)
							RUN_UPDATE_ON_VEHICLE_BLIP(iveh, tempveh)
						ENDIF
					ENDIF
				ENDIF
				
				IF MC_serverBD_4.ivehRule[iVeh][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetEleven[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE11_SECUROHACK_REQUIRES_KILL)
						IF IS_BIT_SET(MC_serverBD_4.iVehHackedReadyForDeathBS, iVeh)
							IF DOES_BLIP_EXIST(biVehBlip[iVeh])
								IF GET_BLIP_COLOUR(biVehBlip[iVeh]) != BLIP_COLOUR_RED
									SET_BLIP_COLOUR(biVehBlip[iVeh], BLIP_COLOUR_RED)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_PED_IN_VEHICLE(LocalPlayerPed,tempVeh)
					IF DOES_BLIP_EXIST(biVehBlip[iveh])
						CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION] Removing blip for vehicle ", iveh, ". The local player has entered the vehicle.")
						REMOVE_VEHICLE_BLIP(iveh)
					ENDIF
				ELSE
					IF MC_serverBD_4.ivehRule[iveh][MC_playerBD[iPartToUse].iteam] != FMMC_OBJECTIVE_LOGIC_NONE
					AND NOT (IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER)	
					OR MC_playerBD[iPartToUse].iVehNear = iveh)
						IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet, ciFMMC_VEHICLE_BLIP_OFF)
							
							// url:bugstar:5627549 - [ML] If the vehicle has another player driving it, don't allow it to be set as a Quick GPS target
							BOOL bIsVehicleEmpty = TRUE
							BOOL bIsDriverAI = TRUE	 
							IF GET_NUM_PEDS_IN_VEHICLE(tempVeh) > 0 
								bIsVehicleEmpty = FALSE
								IF NOT IS_VEHICLE_SEAT_FREE(tempVeh, VS_DRIVER)
									IF IS_PED_A_PLAYER(GET_PED_IN_VEHICLE_SEAT(tempVeh, VS_DRIVER))
										bIsDriverAI = FALSE
									ENDIF
								ENDIF
							ENDIF

							IF bIsVehicleEmpty OR bIsDriverAI 
								FLOAT fVehDist = GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed,tempVeh)
								IF fVehDist < fNearestTargetDistTemp
									BOOL bDontTargetVehicle = FALSE
									IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
										tempPlayerVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
										IF IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(tempPlayerVeh)
											tempTrailer = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_ATTACHED_TO(tempPlayerVeh))
										ENDIF
										IF (IS_THIS_ROCKSTAR_MISSION_WVM_TRAILERSMALL(g_FMMC_STRUCT.iRootContentIDHash) AND GET_ENTITY_MODEL(tempPlayerVeh) = NIGHTSHARK)
										AND GET_ENTITY_MODEL(tempVeh) = NIGHTSHARK
											bDontTargetVehicle = TRUE
										ENDIF
									ENDIF
									IF tempTrailer != tempVeh
									AND !bDontTargetVehicle
									AND MC_serverBD_4.iVehPriority[iVeh][iPlayerTeam] = MC_ServerBD_4.iCurrentHighestPriority[iPlayerTeam] 
									AND MC_serverBD_4.iVehPriority[iVeh][iPlayerTeam] < FMMC_MAX_RULES
										iNearestTargetTemp = iveh
										iNearestTargetTypeTemp = ci_TARGET_VEHICLE
										fNearestTargetDistTemp = fVehDist
										PRINTLN("[RCC MISSION] QUICK GPS VEH UPDATED")
										PRINTLN("[RCC MISSION] QUICK GPS VEH iNearestTargetTemp ", iNearestTargetTemp)
										PRINTLN("[RCC MISSION] QUICK GPS VEH iNearestTargetTypeTemp ", iNearestTargetTypeTemp)
										PRINTLN("[RCC MISSION] QUICK GPS VEH fNearestTargetDistTemp ", fNearestTargetDistTemp)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFive, ciFMMC_VEHICLE5_RESTART_WITH_TRAILER)
	AND IS_VEHICLE_DRIVEABLE(tempVeh)
		INT prevTrailer = g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[iveh]
		VEHICLE_INDEX trailer
		IF GET_VEHICLE_TRAILER_VEHICLE(tempVeh, trailer)
			NETWORK_INDEX netId
			IF prevTrailer != -1
				netId = MC_serverBD_1.sFMMC_SBD.niVehicle[prevTrailer]
			ENDIF
			IF prevTrailer = -1
			OR (NETWORK_DOES_NETWORK_ID_EXIST(netId)
			AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(netId))
			AND NET_TO_VEH(netId) != trailer)
				PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - RESPAWN_WITH_TRAILER - (not trailer) Vehicle number: ", iveh)
				PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - RESPAWN_WITH_TRAILER - New trailer was detected, ent id: ", NATIVE_TO_INT(trailer), ", old value: ", prevTrailer)
				g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[iveh] = -1
				INT i
				FOR i = 0 TO (FMMC_MAX_VEHICLES-1)
					IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
					AND NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]) = trailer
						PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - RESPAWN_WITH_TRAILER - New trailer saved, vehicle number ", i)
						g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[iveh] = i
						BREAKLOOP
					ENDIF
				ENDFOR
			ENDIF
		ELSE
			IF prevTrailer != 0
			AND NOT IS_THIS_ROCKSTAR_MISSION_WVM_TRAILERSMALL(g_FMMC_STRUCT.iRootContentIDHash) //Will keep the last attached trailer stored, even if it becomes detached during this mission
				PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - RESPAWN_WITH_TRAILER - Trailer was detached, saving, old value: ", prevTrailer)
				g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[iveh] = -1
			ENDIF
		ENDIF
	ENDIF
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[iveh])
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[iveh])
			OBJECT_INDEX tempVehCrate = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[iveh])
			IF NOT IS_ENTITY_VISIBLE(tempVehCrate)
				SET_ENTITY_VISIBLE(tempVehCrate,TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iOpenVehicleDoorsBitset, iveh)
	AND IS_THIS_ROCKSTAR_MISSION_WVM_OPPRESSOR_2(g_FMMC_STRUCT.iRootContentIDHash)
	AND GET_ENTITY_MODEL(tempVeh) = CARGOPLANE
		IF iDoorAlarmSoundID = -1
			iDoorAlarmSoundID = GET_SOUND_ID()
			PLAY_SOUND_FROM_COORD(-1, "Plane_Door_Open", GET_ENTITY_COORDS(tempVeh) + <<24.0, 0.0, 6.0>>, "DLC_GR_WVM_Oppressor2_Sounds")
		ENDIF
		IF HAS_SOUND_FINISHED(iDoorAlarmSoundID)
		OR iDoorAlarmSoundID = -1
			IF IS_VEHICLE_DOOR_FULLY_OPEN(tempVeh, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].eVehicleDelayedDoorWhichDoor)
				
				IF iDoorAlarmSoundID = -1
					iDoorAlarmSoundID = GET_SOUND_ID()
				ENDIF
				
				PLAY_SOUND_FROM_COORD(iDoorAlarmSoundID, "Plane_Alarm_Loop", GET_ENTITY_COORDS(tempVeh) + <<24.0, 0.0, 6.0>>, "DLC_GR_WVM_Oppressor2_Sounds")
			ENDIF
		ENDIF
	ENDIF
	IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
	
		//Allows the player driving the vehicle from receiving damage from explosives.
		//Particularly useful for when the vehicle is invulnerable.			
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_OCCUPANT_EXPLOSION_DMG)
			IF NOT IS_BIT_SET(iLocalVehicleExplosiveBitset, iveh)	
				SET_BIT(iLocalVehicleExplosiveBitset, iveh)
				PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - VEHICLE_OCCUPANT_EXPLOSION_DMG - Allowing explosive damage to affect occupants of this vehicle: ", iveh)
				SET_VEHICLE_OCCUPANTS_TAKE_EXPLOSIVE_DAMAGE(tempVeh, TRUE)
			ENDIF
		ENDIF
		MODEL_NAMES tempModel = GET_ENTITY_MODEL(tempVeh)
		
		PROCESS_SPECIAL_CASE_DUNE3_TITAN_HEALTHBAR(tempVeh, iVeh, tempModel)
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix, ciFMMC_VEHICLE6_FORCE_COLLISION_LOADING)
			LOAD_COLLISION_ON_VEH_IF_POSSIBLE(tempVeh, iveh, TRUE)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF bFMMCVehBodyNetVehIDprints
			INT iNI = NATIVE_TO_INT(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
			INT iNTVI = NATIVE_TO_INT(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]))
			INT iVI = NATIVE_TO_INT(tempVeh)
			INT iVTNI = NATIVE_TO_INT(VEH_TO_NET(tempVeh))
			
			PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - bFMMCVehBodyNetVehIDprints 2 - vehicle ",iveh," has network index ",iNI,", NET_TO_VEH on that = ",iNTVI," / has tempVeh ",iVi,", VEH_TO_NET on that = ",iVTNI)
		ENDIF
		#ENDIF
		
		IF tempModel = TANKER
		OR tempModel = ARMYTANKER
		OR tempModel = TANKER2
			FLOAT fPercentage = GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(tempVeh, iVeh, MC_serverBD.iTotalNumStartingPlayers)
			
			IF fPercentage <= 1
				NETWORK_EXPLODE_VEHICLE(tempVeh, DEFAULT, TRUE)
				PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - tanker health <= 100, exploding w NETWORK_EXPLODE_VEHICLE: ",iveh)
			ENDIF
		ENDIF	
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet, ciFMMC_VEHICLE_EASYEXPLODER)
			IF GET_VEHICLE_ENGINE_HEALTH(tempVeh) <= -3650
				PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - Calling NETWORK_EXPLODE_VEHICLE on veh ",iveh," as it's an easy exploder and engine health <= -3650")
				NETWORK_EXPLODE_VEHICLE(tempVeh, DEFAULT, TRUE)
			ELSE
				IF (GET_VEHICLE_ENGINE_HEALTH(tempVeh) > -3500)
					IF (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fEngineHealth = 1001.0) //Default values
						IF GET_VEHICLE_ENGINE_HEALTH(tempVeh) <= (100)
							SET_VEHICLE_ENGINE_HEALTH(tempVeh,-3500) //Set it on fire
							PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - Vehicle ",iveh," is an easy exploder and taken enough engine damage 1, set engine health to -3500 (set on fire)")
						ENDIF
					ELSE
						//Need to do a fifth of the engine health in damage (engine health is from -1000 to +1000)
						IF GET_VEHICLE_ENGINE_HEALTH(tempVeh) <= ( g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fEngineHealth - ((g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fEngineHealth + 1000) / 5 ))
							SET_VEHICLE_ENGINE_HEALTH(tempVeh,-3500) //Set it on fire
							PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - Vehicle ",iveh," is an easy exploder and taken enough engine damage 2, set engine health to -3500 (set on fire)")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = INT_TO_ENUM(MODEL_NAMES, HASH("dune4"))
		OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = INT_TO_ENUM(MODEL_NAMES, HASH("dune5"))
		OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = INT_TO_ENUM(MODEL_NAMES, HASH("phantom2"))
		OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = INT_TO_ENUM(MODEL_NAMES, HASH("boxville5"))
			PRINTLN("[PROCESS_VEH_BODY] - Setting VEHICLE_SET_RAMP_AND_RAMMING_CARS_TAKE_DAMAGE FALSE ... iVeh = ", iVeh, " ... Model = ", GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn))
			VEHICLE_SET_RAMP_AND_RAMMING_CARS_TAKE_DAMAGE(tempVeh, FALSE)
		ENDIF
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(VEH_TO_NET(tempVeh))			
			CHECK_VEHICLE_STUCK_TIMERS(tempVeh, iVeh, TRUE)
			
			// For preventing helis and planes freezing in the air. toggle freeze waiting on collision
			IF NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(tempVeh)
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(VEH_TO_NET(tempVeh))
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(VEH_TO_NET(tempVeh))
						IF NOT IS_ENTITY_VISIBLE(tempVeh)
							SET_ENTITY_VISIBLE(tempVeh,TRUE)
						ENDIF
					ENDIF
					
					MODEL_NAMES eVehicleModel = GET_ENTITY_MODEL(tempVeh)	
					PED_INDEX vehiclePed
					
					IF IS_THIS_MODEL_A_HELI(eVehicleModel)
					OR IS_THIS_MODEL_A_PLANE(eVehicleModel)
						PRINTLN("[MMacK][HeliFall] We have a heli ", iVeh)
						IF NOT IS_ANY_PLAYER_IN_VEHICLE(tempVeh)
							IF IS_ENTITY_IN_AIR(tempVeh) AND IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_EVERYONE_RUNNING)
								PRINTLN("[MMacK][HeliFall] Entity is airborne and we are running according to server")
								IF NOT IS_VEHICLE_SEAT_FREE(tempVeh)
									vehiclePed = GET_PED_IN_VEHICLE_SEAT(tempVeh)
									IF IS_ENTITY_DEAD(vehiclePed)
										SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(tempVeh, FALSE)
										PRINTLN("[MMacK][HeliFall] FALSE A")
									ENDIF
								ELSE
									SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(tempVeh, FALSE)
									PRINTLN("[MMacK][HeliFall] FALSE B")
								ENDIF
							ELSE
								IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_EVERYONE_RUNNING)
									PRINTLN("[MMacK][HeliFall] SBBOOL_EVERYONE_RUNNING = TRUE")
								ELSE
									PRINTLN("[MMacK][HeliFall] SBBOOL_EVERYONE_RUNNING = FALSE")
								ENDIF
								IF IS_ENTITY_IN_AIR(tempVeh)
									PRINTLN("[MMacK][HeliFall] IS_ENTITY_IN_AIR = TRUE")
								ELSE
									PRINTLN("[MMacK][HeliFall] IS_ENTITY_IN_AIR = FALSE")
								ENDIF
								IF NOT IS_VEHICLE_SEAT_FREE(tempVeh)
									vehiclePed = GET_PED_IN_VEHICLE_SEAT(tempVeh)
									IF IS_ENTITY_DEAD(vehiclePed)
										SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(tempVeh, TRUE)
										PRINTLN("[MMacK][HeliFall] TRUE A")
									ENDIF
								ELSE
									SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(tempVeh, TRUE)
									PRINTLN("[MMacK][HeliFall] TRUE B")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
		
		IF IS_THIS_MODEL_A_BOAT(tempModel)
		OR tempModel = SUBMERSIBLE
			IF NOT IS_ENTITY_WAITING_FOR_WORLD_COLLISION(tempVeh)
				IF IS_VEHICLE_EMPTY(tempVeh, TRUE)
					ACTIVATE_PHYSICS(tempVeh)
				ENDIF
			ENDIF
		ENDIF

		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet, enum_to_int(SC_DOOR_BOOT))
			IF NOT IS_BIT_SET(ibootbitset,iveh)
				IF DOES_ENTITY_EXIST(GET_PED_IN_VEHICLE_SEAT(tempVeh, VS_DRIVER))
					IF NOT IS_VEHICLE_STOPPED(tempVeh)
						SET_VEHICLE_DOOR_OPEN(tempVeh,SC_DOOR_BOOT,TRUE,FALSE)
						PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - Calling SET_VEHICLE_DOOR_OPEN(TRUE, FALSE) for SC_DOOR_BOOT for vehicle: ") NET_PRINT_INT(iveh) NET_NL()
					ENDIF
				ENDIF
				SET_BIT(ibootbitset,iveh)
			ENDIF
		ENDIF

		IF NOT IS_BIT_SET(iDoorUnlockBitset,iveh)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iDoorUnlockRule != -1
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iAssociatedTeam !=-1
			 	OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iSecondAssociatedTeam !=-1
				OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iThirdAssociatedTeam !=-1
				OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iFourthAssociatedTeam !=-1
					IF NOT IS_ANY_PLAYER_TRYING_TO_OPEN_LOCKED_VEHICLE()
					OR IS_THIS_ROCKSTAR_MISSION_WVM_HALFTRACK(g_FMMC_STRUCT.iRootContentIDHash)
						
						BOOL bUnlockVeh = FALSE
						INT iteam
						IF DOES_ENTITY_EXIST(tempVeh)
							IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iAssociatedTeam !=-1
							AND MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iAssociatedTeam] < FMMC_MAX_RULES
							AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iDoorUnlockRule <= MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iAssociatedTeam]
							AND NOT (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetTwo, ciFMMC_VEHICLE2_LOCK_ON_RULE) AND DOES_VEHICLE_HAVE_DELIVERED_DECOR_FOR_TEAM(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iAssociatedTeam,iveh,MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iAssociatedTeam]))
								
								bUnlockVeh = TRUE
								
							ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iSecondAssociatedTeam !=-1 
							AND MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iSecondAssociatedTeam] < FMMC_MAX_RULES
							AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iDoorUnlockRule <= MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iSecondAssociatedTeam]
								
								bUnlockVeh = TRUE
								
							ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iThirdAssociatedTeam != -1 
							AND MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iThirdAssociatedTeam] < FMMC_MAX_RULES
							AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iDoorUnlockRule <= MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iThirdAssociatedTeam]
								
								bUnlockVeh = TRUE
								
							ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iFourthAssociatedTeam !=-1
							AND MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iFourthAssociatedTeam] < FMMC_MAX_RULES
							AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iDoorUnlockRule <= MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iFourthAssociatedTeam]
								
								bUnlockVeh = TRUE
								
							ENDIF
							
							IF bUnlockVeh
							
								FOR iteam = 0 TO (FMMC_MAX_TEAMS-1)
									SET_VEHICLE_DOORS_LOCKED_FOR_TEAM(tempVeh,iteam,FALSE)
								ENDFOR
								
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetEight, ciFMMC_VEHICLE8_REQUIRES_HOTWIRE) // [ML] url:bugstar:5833072 - Locking then unlocking on associated rule was overriding this bitset.
									SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(tempVeh, FALSE)
									SET_VEHICLE_DOORS_LOCKED(tempVeh,VEHICLELOCK_LOCKED_BUT_CAN_BE_DAMAGED)
									SET_VEHICLE_NEEDS_TO_BE_HOTWIRED(tempVeh,TRUE)		
									PRINTLN("[RCC MISSION] PROCESS_VEH_BODY [ML] - vehicle: ",iveh," Unlocking on this rule, but set as requiring Hotwire")
								ELSE
									SET_VEHICLE_DOORS_LOCKED(tempVeh, VEHICLELOCK_UNLOCKED)
									SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(tempVeh, FALSE)
									SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(tempVeh,FALSE)
									PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - Unlocking the doors for vehicle: ",iveh," g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iDoorUnlockRule : ",g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iDoorUnlockRule, " iAssociatedTeam = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iAssociatedTeam, " Model = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(tempVeh)))			
								ENDIF
								//-- Don't want any rear doors being unlocked if there's crates in the back.
								LOCK_VEHICLE_DOORS_FOR_CRATES(tempVeh, iveh)
								SET_BIT(iDoorUnlockBitset,iveh)
							
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_IMMUNE_HOMING_LAUNCHER)
			IF DOES_ENTITY_EXIST(tempVeh)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iRemoveHomingLockRule != -1
					IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iRemoveHomingLockRule
						PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - Allowing homing missile lock from vehicle: ", iveh)
						SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(tempVeh, TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	IF IS_BIT_SET(iLocalBoolCheck3, LBOOL3_EXPLOSIVE_ZONE_ACTIVE)
		IF NOT IS_BIT_SET(iVehDamageTrackerBS, iVeh)
			IF DOES_ENTITY_EXIST(tempVeh)
				
				NETWORK_INDEX niVeh = VEH_TO_NET(tempVeh)
				
				IF NOT IS_DAMAGE_TRACKER_ACTIVE_ON_NETWORK_ID(niVeh)
					ACTIVATE_DAMAGE_TRACKER_ON_NETWORK_ID(niVeh, TRUE)
				ENDIF
				
				CPRINTLN(DEBUG_SIMON, "ACTIVATE_DAMAGE_TRACKER_ON_NETWORK_ID enabled on iVeh: ", iVeh)
				SET_BIT(iVehDamageTrackerBS, iVeh)
			ENDIF
		ENDIF
	ENDIF
	
	// B* 4030061
	// Must be set on gang boss
	IF DOES_ENTITY_EXIST(tempVeh)
	AND GET_ENTITY_MODEL(tempVeh) = AVENGER
	AND GB_GET_LOCAL_PLAYER_GANG_BOSS() = LocalPlayer
		
		LOCK_STATE vehLockState = GET_VEHICLE_DOOR_LOCK_STATUS(tempVeh)
		IF vehLockState = VEHICLELOCK_LOCKED
		OR vehLockState = VEHICLELOCK_LOCKED_BUT_CAN_BE_DAMAGED
		OR vehLockState = VEHICLELOCK_LOCKED_BUT_BOOT_UNLOCKED
		OR vehLockState = VEHICLELOCK_LOCKED_NO_PASSENGERS
			
			IF NOT IS_CREATOR_AIRCRAFT_ENTRY_BLOCKED(PLAYER_ID())
				PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - vehLockState: ", vehLockState, " - BLOCK_CREATOR_AIRCRAFT_INTERIOR_ENTRY = TRUE")
				BLOCK_CREATOR_AIRCRAFT_INTERIOR_ENTRY(TRUE)
			ENDIF
			
		ELSE
		
			IF IS_CREATOR_AIRCRAFT_ENTRY_BLOCKED(PLAYER_ID())
			AND NOT IS_AVENGER_HOLD_TRANSITION_BLOCKED()
				PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - vehLockState: ", vehLockState, " - BLOCK_CREATOR_AIRCRAFT_INTERIOR_ENTRY = FALSE")
				BLOCK_CREATOR_AIRCRAFT_INTERIOR_ENTRY(FALSE)
			ENDIF

		ENDIF
	ENDIF

	// We need to do some creation functionality when the vehicle has loaded and has collision / has migrated.
	IF iStaggeredVehLoopCounterPostCreationCalls >= FMMC_MAX_VEHICLES-1
	OR iStaggeredVehLoopCounterPostCreationCalls >= MC_serverBD.iNumVehCreated-1
		IF NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_FIRST_STAGGERED_LOOP_OF_VEH_BODY)
			PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - Setting LBOOL26_FIRST_STAGGERED_LOOP_OF_VEH_BODY")
			SET_BIT(iLocalBoolCheck26, LBOOL26_FIRST_STAGGERED_LOOP_OF_VEH_BODY)			
		ENDIF	
	ELSE
		iStaggeredVehLoopCounterPostCreationCalls++
	ENDIF
ENDPROC



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: EVERY FRAME VEHICLE CHECKS / VEHICLE HUD !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************



FUNC INT GET_PARTICIPANT_VEHICLE_EXCLUSIVE_DRIVER_NUMBER(INT iPart, INT iVeh)
	
	INT iPartLoop
	INT iDriverNum = 0
	
	FOR iPartLoop = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF iPartLoop != iPart
			IF (MC_playerBD[iPartLoop].iTeam != -1)
			AND (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iTeamSeatPreference[MC_playerBD[iPartLoop].iTeam] = ciFMMC_SeatPreference_Driver)
			AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartLoop))
				PRINTLN("[RCC MISSION] GET_PARTICIPANT_VEHICLE_EXCLUSIVE_DRIVER_NUMBER iPartLoop = ",iPartLoop," iTeam = ",MC_playerBD[iPartLoop].iTeam," iDriverNum = ",iDriverNum)
				iDriverNum++
			ENDIF
		ELSE
			//It's me - break out!
			iPartLoop = MAX_NUM_MC_PLAYERS
		ENDIF
	ENDFOR
	
	RETURN iDriverNum
	
ENDFUNC

PROC CHECK_SEATING_PREFERENCES(INT iveh,INT iPart)
	
	#IF IS_DEBUG_BUILD
		MODEL_NAMES mSeat
		IF bWdSeatPreferenceDebug
			mSeat = GET_ENTITY_MODEL(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]))
			PRINTLN("[RCC MISSION] CHECK_SEATING_PREFERENCES - Called for veh ", iveh, " model = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mSeat), " team set pref = ",g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iTeamSeatPreference[MC_playerBD[iPart].iTeam] )
		ENDIF
	#ENDIF

	
	IF NOT IS_BIT_SET(iVehSeatPreferenceCheckBS,iveh)
		IF (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iTeamSeatPreference[MC_playerBD[iPart].iTeam] != ciFMMC_SeatPreference_None)
			//IF MC_serverBD_4.iVehPriority[iVeh][MC_playerBD[iPart].iteam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPart].iteam] 
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
			AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]))
				IF iSeatPreferenceSlot < 3
					IF (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iTeamSeatPreference[MC_playerBD[iPart].iTeam] = ciFMMC_SeatPreference_Driver)
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
						AND bLocalPlayerPedOk
							
							INT iDriver = GET_PARTICIPANT_VEHICLE_EXCLUSIVE_DRIVER_NUMBER(iPart, iveh)
							
							SET_VEHICLE_EXCLUSIVE_DRIVER(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]), LocalPlayerPed, iDriver)
							//FORCE_PED_TO_USE_FRONT_SEATS_IN_VEHICLE(LocalPlayerPed,iveh,TRUE)
							SET_PED_VEHICLE_FORCED_SEAT_USAGE(LocalPlayerPed,NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]),iSeatPreferenceSlot,ENUM_TO_INT(VC_FORCE_USE_FRONT_SEATS))
							iSeatPreferenceSlot++
							
							#IF IS_DEBUG_BUILD
								IF bWdSeatPreferenceDebug
									PRINTLN("[RCC MISSION] CHECK_SEATING_PREFERENCES - Setting as exclusive driver ",iDriver," on vehicle ", iveh, " Model = ",  GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mSeat))
								ENDIF
							#ENDIF
							
							PRINTLN("[RCC MISSION] CHECK_SEATING_PREFERENCES - Setting as exclusive driver ",iDriver," on vehicle ", iveh)
						
						ENDIF
					ELIF (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iTeamSeatPreference[MC_playerBD[iPart].iTeam] = ciFMMC_SeatPreference_Rear_left)
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
						AND bLocalPlayerPedOk
							//SET_PLAYER_RESET_FLAG_PREFER_REAR_SEATS(LocalPlayer, NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]))
							//FORCE_PED_TO_USE_REAR_SEATS_IN_VEHICLE(LocalPlayerPed,iveh,TRUE)
							SET_PED_VEHICLE_FORCED_SEAT_USAGE(LocalPlayerPed,NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]),iSeatPreferenceSlot,ENUM_TO_INT(VC_FORCE_USE_REAR_SEATS))
							iSeatPreferenceSlot++
							
	//						IF IS_BIT_SET(iVehSeatClearedPrefBS,iveh)
	//							CLEAR_BIT(iVehSeatClearedPrefBS,iveh)
	//							PRINTLN("[RCC MISSION] CHECK_SEATING_PREFERENCES - REAR LEFT - iVehSeatClearedPrefBS had been set for this vehicle! veh ", iveh)
	//						ENDIF
							
							#IF IS_DEBUG_BUILD
								IF bWdSeatPreferenceDebug
									PRINTLN("[RCC MISSION] CHECK_SEATING_PREFERENCES - Setting as prefer rear left seat on vehicle ", iveh, " Model = ",  GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mSeat))
								ENDIF
							#ENDIF
							
							PRINTLN("[RCC MISSION] CHECK_SEATING_PREFERENCES - Setting as prefer rear left seat on vehicle ", iveh)
							
						ENDIF
					ELIF (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iTeamSeatPreference[MC_playerBD[iPart].iTeam] = ciFMMC_SeatPreference_Rear_right)
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
						AND bLocalPlayerPedOk
							//SET_PLAYER_RESET_FLAG_PREFER_REAR_SEATS(LocalPlayer, NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]))
							//FORCE_PED_TO_USE_REAR_SEATS_IN_VEHICLE(LocalPlayerPed,iveh,TRUE)
							SET_PED_VEHICLE_FORCED_SEAT_USAGE(LocalPlayerPed,NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]),iSeatPreferenceSlot,ENUM_TO_INT(VC_FORCE_USE_REAR_SEATS))
							iSeatPreferenceSlot++
							
	//						IF IS_BIT_SET(iVehSeatClearedPrefBS,iveh)
	//							CLEAR_BIT(iVehSeatClearedPrefBS,iveh)
	//							PRINTLN("[RCC MISSION] CHECK_SEATING_PREFERENCES - REAR RIGHT - iVehSeatClearedPrefBS had been set for this vehicle! veh ", iveh)
	//						ENDIF

							
							#IF IS_DEBUG_BUILD
								IF bWdSeatPreferenceDebug
									PRINTLN("[RCC MISSION] CHECK_SEATING_PREFERENCES - Setting as prefer rear right seat on vehicle ", iveh, " Model = ",  GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mSeat))
								ENDIF
							#ENDIF
							
							PRINTLN("[RCC MISSION] CHECK_SEATING_PREFERENCES - Setting as prefer rear right seat on vehicle ", iveh)
						ENDIF
					ELIF (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iTeamSeatPreference[MC_playerBD[iPart].iTeam] = ciFMMC_SeatPreference_Passenger)
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
						AND bLocalPlayerPedOk
							//SET_PLAYER_RESET_FLAG_PREFER_FRONT_PASSENGER_SEAT(LocalPlayer ,NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]))
							//FORCE_PED_TO_USE_FRONT_SEATS_IN_VEHICLE(LocalPlayerPed,iveh,TRUE)
							SET_PED_VEHICLE_FORCED_SEAT_USAGE(LocalPlayerPed,NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]),iSeatPreferenceSlot,ENUM_TO_INT(VC_FORCE_USE_FRONT_SEATS))
							iSeatPreferenceSlot++
							
	//						IF IS_BIT_SET(iVehSeatClearedPrefBS,iveh)
	//							CLEAR_BIT(iVehSeatClearedPrefBS,iveh)
	//							PRINTLN("[RCC MISSION] CHECK_SEATING_PREFERENCES - FRONT PASS - iVehSeatClearedPrefBS had been set for this vehicle! veh ", iveh)
	//						ENDIF
							
							
							#IF IS_DEBUG_BUILD
								IF bWdSeatPreferenceDebug
									PRINTLN("[RCC MISSION] CHECK_SEATING_PREFERENCES - Setting as prefer passenger seats on vehicle ", iveh, " Model = ",  GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mSeat))
								ENDIF
							#ENDIF
							
							PRINTLN("[RCC MISSION] CHECK_SEATING_PREFERENCES - Setting as prefer passenger seats on vehicle ", iveh)
						ENDIF
					ELIF (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iTeamSeatPreference[MC_playerBD[iPart].iTeam] = ciFMMC_SeatPreference_Front_Seats)
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
						AND bLocalPlayerPedOk
							//See AI code bug 2058559 / controller bug 2058565)
							//FORCE_PED_TO_USE_FRONT_SEATS_IN_VEHICLE(LocalPlayerPed,iveh,TRUE)
							SET_PED_VEHICLE_FORCED_SEAT_USAGE(LocalPlayerPed,NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]),iSeatPreferenceSlot,ENUM_TO_INT(VC_FORCE_USE_FRONT_SEATS))
							iSeatPreferenceSlot++
							
	//						IF IS_BIT_SET(iVehSeatClearedPrefBS,iveh)
	//							CLEAR_BIT(iVehSeatClearedPrefBS,iveh)
	//							PRINTLN("[RCC MISSION] CHECK_SEATING_PREFERENCES - FRONT SEATS - iVehSeatClearedPrefBS had been set for this vehicle! veh ", iveh)
	//						ENDIF
							
							#IF IS_DEBUG_BUILD
								IF bWdSeatPreferenceDebug
									PRINTLN("[RCC MISSION] CHECK_SEATING_PREFERENCES - Setting as prefer front seats on vehicle ", iveh, " Model = ",  GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mSeat))
								ENDIF
							#ENDIF
							
							PRINTLN("[RCC MISSION] CHECK_SEATING_PREFERENCES - Setting as prefer front seats on vehicle ", iveh)
						ENDIF
					ELSE
						PRINTLN("[RCC MISSION] CHECK_SEATING_PREFERENCES - No seat preference on vehicle ", iveh)
					ENDIF
					SET_BIT(iVehSeatPreferenceCheckBS, iveh)
					PRINTLN("[RCC MISSION] CHECK_SEATING_PREFERENCES - iSeatPreferenceSlot = ", iSeatPreferenceSlot)
				ELSE
					PRINTLN("[RCC MISSION] CHECK_SEATING_PREFERENCES - Player trying to set more than the max allowed seating prefs iSeatPreferenceSlot: ", iSeatPreferenceSlot)
				ENDIF
			ENDIF
		ELSE
			SET_BIT(iVehSeatPreferenceCheckBS, iveh)
		ENDIF
	ENDIF
	
ENDPROC

PROC CHECK_VEHICLE_TARGETING(INT iveh)
	BOOL bNoLockOn = FALSE
	INT iteamrepeat
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
	AND IS_ENTITY_A_MISSION_ENTITY(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]))		
		IF NOT IS_BIT_SET(iVehTargetingCheckedThisRuleBS,iveh)
			FOR iteamrepeat = 0 TO (MC_serverBD.iNumberOfTeams-1)
				IF NOT bNoLockOn
					IF iteamrepeat = MC_playerBD[iPartToUse].iteam
					OR DOES_TEAM_LIKE_TEAM(MC_playerBD[iPartToUse].iteam,iteamrepeat)
						IF( (MC_serverBD_4.ivehRule[iveh][iteamrepeat] = FMMC_OBJECTIVE_LOGIC_PROTECT)
						OR (MC_serverBD_4.ivehRule[iveh][iteamrepeat] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER)
						OR (MC_serverBD_4.ivehRule[iveh][iteamrepeat] = FMMC_OBJECTIVE_LOGIC_GO_TO)
						OR (MC_serverBD_4.ivehRule[iveh][iteamrepeat] = FMMC_OBJECTIVE_LOGIC_PHOTO)
						OR (MC_serverBD_4.ivehRule[iveh][iteamrepeat] = FMMC_OBJECTIVE_LOGIC_CAPTURE)
						OR (MC_serverBD_4.ivehRule[iveh][iteamrepeat] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD) )
						AND (MC_serverBD_4.iVehPriority[iveh][iteamrepeat] < FMMC_MAX_RULES)
						AND NOT (MC_serverBD_4.iVehMissionLogic[MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL) AND (GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION() OR IS_FAKE_MULTIPLAYER_MODE_SET(TRUE))
							PRINTLN("[RCC MISSION] CHECK_VEHICLE_TARGETING, veh ",iveh, " Shouldn't lock on,  My team  = ", MC_playerBD[iPartToUse].iteam , " iteamrepeat = ", iteamrepeat, " MC_serverBD_4.ivehRule[iveh][iteamrepeat] = ", MC_serverBD_4.ivehRule[iveh][iteamrepeat] )
							bNoLockOn = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
			
			IF bNoLockOn			
				SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]),FALSE)
				PRINTLN("[RCC MISSION] CHECK_VEHICLE_TARGETING, veh ",iveh," - Vehicle cannot be locked on, on a protect rule")
			ELSE	
				SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]),TRUE)
				PRINTLN("[RCC MISSION] CHECK_VEHICLE_TARGETING, veh ",iveh," - Vehicle can be locked on")			
			ENDIF
			
			SET_BIT(iVehTargetingCheckedThisRuleBS,iveh)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_VEHICLE_PHOTO_LOGIC(INT iveh, VEHICLE_INDEX ThisVeh)

	FLOAT fPhotoRange
	VECTOR vVehCoords, vPlateCoords1, vPlateCoords2
								
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitset[MC_serverBD_4.iVehPriority[iveh][MC_playerBD[iPartToUse].iteam]], ciBS_RULE_VEHPHOTO_NUMPLATES)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].ivehPhotoRange > 0
			fPhotoRange = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].ivehPhotoRange)
		ELSE
			fPhotoRange = ci_PHOTO_RANGE_VEH
		ENDIF
	ELSE
		fPhotoRange = ci_PHOTO_RANGE_VEH_NUMPLATE
	ENDIF
	
	IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed,ThisVeh) < fPhotoRange
	
		PRINTLN("[RCC MISSION] [AW PHOTO]GET_DISTANCE_BETWEEN_ENTITIES < fPhotoRange : ", fPhotoRange) 
		
		IF MC_playerBD[iPartToUse].iVehNear = -1
			MC_playerBD[iPartToUse].iVehNear = iveh
			
			#IF IS_DEBUG_BUILD
				IF bInVehicleDebug
					PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH (1) Setting MC_playerBD[iPartToUse].iVehNear =  ", MC_playerBD[iPartToUse].iVehNear, " for part ",iPartToUse, " who is player ",  GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartToUse))))
				ENDIF
			#ENDIF
		ENDIF
		
		IF HAS_CELLPHONE_CAM_JUST_TAKEN_PIC()
			
			SET_BIT(iVehPhotoChecksBS, iveh)
			
			BOOL bPlates = FALSE
			VECTOR vPlayer
			
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitset[MC_serverBD_4.iVehPriority[iveh][MC_playerBD[iPartToUse].iteam]], ciBS_RULE_VEHPHOTO_NUMPLATES)
				vVehCoords = GET_ENTITY_COORDS(ThisVeh)
				
				VECTOR vMax, vMin
				GET_MODEL_DIMENSIONS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn, vMin, vMax)
				FLOAT fAddZ = FMAX(vMin.z, vMax.z)
				
				PRINTLN("[RCC MISSION] [AW PHOTO]ciBS_RULE_VEHPHOTO_NUMPLATES is not set. entity coords ", vVehCoords,", add on ",fAddZ," in z to get final vVehCoords ",vVehCoords + <<0, 0, fAddZ>>)
				vVehCoords.Z = vVehCoords.Z + fAddZ
			ELSE
				
				VEHICLE_PLATE_TYPE vpt = GET_VEHICLE_PLATE_TYPE(ThisVeh)
				
				IF vpt = VPT_BACK_PLATES
					INT iNumPlateBone = GET_ENTITY_BONE_INDEX_BY_NAME(ThisVeh,"platelight")
					
					vPlayer = GET_PLAYER_COORDS(LocalPlayer)
					
					IF iNumPlateBone != -1
						vVehCoords = GET_WORLD_POSITION_OF_ENTITY_BONE(ThisVeh,iNumPlateBone)
						vVehCoords.z = (vVehCoords.z - 0.075)
					ELSE
						VECTOR vMin,vMax
						GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(ThisVeh),vMin,vMax)
						vVehCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ThisVeh,<<0,vMin.y,0>>)
						vVehCoords.z = (vVehCoords.z - 0.15)
					ENDIF
					
					bPlates = TRUE
					
				ELIF vpt = VPT_FRONT_PLATES
					
					vPlayer = GET_PLAYER_COORDS(LocalPlayer)
					
					VECTOR vMin,vMax
					GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(ThisVeh),vMin,vMax)
					
					vVehCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ThisVeh,<<0,vMax.y,0>>)
					vVehCoords.z = (vVehCoords.z - 0.15)
					
					bPlates = TRUE
					
				ELIF vpt = VPT_FRONT_AND_BACK_PLATES
					
					INT iNumPlateBone = GET_ENTITY_BONE_INDEX_BY_NAME(ThisVeh,"platelight")
					
					vPlayer = GET_PLAYER_COORDS(LocalPlayer)
					
					VECTOR vMin,vMax
					GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(ThisVeh),vMin,vMax)
					
					VECTOR vMinCoords,vMaxCoords
					
					vMinCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ThisVeh,<<0,vMin.y,0>>)
					vMaxCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ThisVeh,<<0,vMax.y,0>>)
					
					FLOAT fPlayerFromMin, fPlayerFromMax
					
					fPlayerFromMin = GET_DISTANCE_BETWEEN_COORDS(vPlayer,vMinCoords)
					fPlayerFromMax = GET_DISTANCE_BETWEEN_COORDS(vPlayer,vMaxCoords)
					
					IF fPlayerFromMin < fPlayerFromMax
						IF iNumPlateBone != -1
							vVehCoords = GET_WORLD_POSITION_OF_ENTITY_BONE(ThisVeh,iNumPlateBone)
						ELSE
							vVehCoords = vMinCoords
							vVehCoords.z = (vVehCoords.z - 0.15)
						ENDIF
					ELSE
						vVehCoords = vMaxCoords
						vVehCoords.z = (vVehCoords.z - 0.15)
					ENDIF
					
					bPlates = TRUE
					
				ELSE //no numplates:
					vVehCoords = GET_ENTITY_COORDS(ThisVeh)
				ENDIF
				
			ENDIF
			
			IF bPlates
				
				VECTOR v1 = vVehCoords - GET_ENTITY_COORDS(ThisVeh)
				VECTOR v2 = vPlayer - vVehCoords
							
				FLOAT fAngle = GET_ANGLE_BETWEEN_2D_VECTORS(v1.x,v1.y,v2.x,v2.y)

				IF fAngle <= cf_PHOTO_NUMPLATE_MAXANGLE
				
					//for license plates use 2 tracked points that encompass the plates
					//this should allow for more precise check when photographing plates
					//and also stop plates being photographed when they are obscured, see B*2090614
					vPlateCoords1 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vVehCoords, GET_ENTITY_HEADING(ThisVeh), << -0.08, 0.0, 0.0 >>)
					vPlateCoords2 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vVehCoords, GET_ENTITY_HEADING(ThisVeh), << 0.08, 0.0, 0.0 >>)
					
					//DRAW_DEBUG_SPHERE(vPlateCoords1, 0.10, 255, 0, 0, 64)
					//DRAW_DEBUG_SPHERE(vPlateCoords2, 0.10, 0, 255, 0, 64)
					
					IF NOT DOES_PHOTO_TRACKED_POINT_EXIST(iVehPhotoTrackedPoint[iveh])
						//create new tracked point for current target coords
						CREATE_PHOTO_TRACKED_POINT_FOR_COORD(iVehPhotoTrackedPoint[iveh], vPlateCoords1, 0.10)
					ELSE
						SET_TRACKED_POINT_INFO(iVehPhotoTrackedPoint[iveh], vPlateCoords1, 0.10)
					ENDIF
					
					IF NOT DOES_PHOTO_TRACKED_POINT_EXIST(iVehPhotoTrackedPoint2[iveh])
						//create new tracked point for current target coords
						CREATE_PHOTO_TRACKED_POINT_FOR_COORD(iVehPhotoTrackedPoint2[iveh], vPlateCoords2, 0.10)
					ELSE
						SET_TRACKED_POINT_INFO(iVehPhotoTrackedPoint2[iveh], vPlateCoords2, 0.10)
					ENDIF
					
					IF DOES_PHOTO_TRACKED_POINT_EXIST(iVehPhotoTrackedPoint[iveh]) AND IS_PHOTO_TRACKED_POINT_VISIBLE(iVehPhotoTrackedPoint[iveh])
					AND DOES_PHOTO_TRACKED_POINT_EXIST(iVehPhotoTrackedPoint2[iveh]) AND IS_PHOTO_TRACKED_POINT_VISIBLE(iVehPhotoTrackedPoint2[iveh])
						IF IS_POINT_IN_CELLPHONE_CAMERA_VIEW(vVehCoords, 0.4)
							IF HAS_CELLPHONE_CAM_JUST_TAKEN_PIC()
								RESET_PHOTO_DATA(iveh)
								//CLEANUP_PHOTO_TRACKED_POINT(iVehPhotoTrackedPoint[iveh])
								IF MC_playerBD[iPartToUse].iVehNear = iveh
									MC_playerBD[iPartToUse].iVehNear = -1
								ENDIF
								MC_playerBD[iPartToUse].ivehPhoto = iveh
								CLEAR_BIT(iVehPhotoChecksBS, iveh)
								
								IF SHOULD_PUT_AWAY_PHONE_AFTER_PHOTO(MC_serverBD_4.iVehPriority[iveh][MC_playerBD[iPartToUse].iTeam], ci_TARGET_VEHICLE)
									SET_BIT(iLocalBoolCheck3,LBOOL3_PHOTOTAKEN)
									REINIT_NET_TIMER(tdPhotoTakenTimer)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
				
			ELSE
				
				PRINTLN("[RCC MISSION] [AW PHOTO] bPlates: ", bPlates) 
				
				IF NOT DOES_PHOTO_TRACKED_POINT_EXIST(iVehPhotoTrackedPoint[iveh])
					//create new tracked point for current target coords
					CREATE_PHOTO_TRACKED_POINT_FOR_COORD(iVehPhotoTrackedPoint[iveh], vVehCoords, 2.5)
					vVehPhotoCoords[iveh] = vVehCoords
					PRINTLN("[RCC MISSION] [AW PHOTO] Creating a tracked point for the vehicle")
					PRINTLN("[RCC MISSION] [AW PHOTO] vVehPhotoCoords[iveh]: ", vVehPhotoCoords[iveh]) 
				ELSE
					IF NOT ARE_VECTORS_ALMOST_EQUAL(vVehCoords, vVehPhotoCoords[iveh], 0.05) //If the car's moved
						vVehPhotoCoords[iveh] = vVehCoords
						SET_TRACKED_POINT_INFO(iVehPhotoTrackedPoint[iveh],vVehPhotoCoords[iveh],2.5)
						PRINTLN("[RCC MISSION] [AW PHOTO] Tracked point has moved")
						PRINTLN("[RCC MISSION] [AW PHOTO] vVehPhotoCoords[iveh]: ", vVehPhotoCoords[iveh]) 
					ENDIF
				ENDIF
				
				IF DOES_PHOTO_TRACKED_POINT_EXIST(iVehPhotoTrackedPoint[iveh]) AND IS_PHOTO_TRACKED_POINT_VISIBLE(iVehPhotoTrackedPoint[iveh])
					PRINTLN("[RCC MISSION] [AW PHOTO] Tracked point exists and is visible")
					IF IS_POINT_IN_CELLPHONE_CAMERA_VIEW(vVehCoords, 0.5)
						PRINTLN("[RCC MISSION] [AW PHOTO] Point is in cellphone view")

						IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY(PLAYER_PED_ID(),ThisVeh,SCRIPT_INCLUDE_FOLIAGE)
							PRINTLN("[RCC MISSION] [AW PHOTO] Entity has clear LOS")
							RESET_PHOTO_DATA(iveh)
							//CLEANUP_PHOTO_TRACKED_POINT(iVehPhotoTrackedPoint[iveh])
							IF MC_playerBD[iPartToUse].iVehNear = iveh
								MC_playerBD[iPartToUse].iVehNear = -1
							ENDIF
							MC_playerBD[iPartToUse].ivehPhoto = iveh
							CLEAR_BIT(iVehPhotoChecksBS, iveh)
							
							IF SHOULD_PUT_AWAY_PHONE_AFTER_PHOTO(MC_serverBD_4.iVehPriority[iveh][MC_playerBD[iPartToUse].iTeam], ci_TARGET_VEHICLE)
								PRINTLN("[RCC MISSION] [AW PHOTO] LBOOL3_PHOTOTAKEN : TRUE")
								SET_BIT(iLocalBoolCheck3,LBOOL3_PHOTOTAKEN)
								REINIT_NET_TIMER(tdPhotoTakenTimer)
							ENDIF
						ENDIF							
					ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[RCC MISSION] [AW PHOTO] DOES_PHOTO_TRACKED_POINT_EXIST ",DOES_PHOTO_TRACKED_POINT_EXIST(iVehPhotoTrackedPoint[iveh]),", IS_PHOTO_TRACKED_POINT_VISIBLE ",IS_PHOTO_TRACKED_POINT_VISIBLE(iVehPhotoTrackedPoint[iveh]))
				#ENDIF
				ENDIF
				
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION] [AW PHOTO] We haven't taken a photo any time recently, don't bother processing position things")
			IF IS_BIT_SET(iVehPhotoChecksBS, iveh)
				RESET_PHOTO_DATA(iveh)
				CLEAR_BIT(iVehPhotoChecksBS, iveh)
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(iVehPhotoChecksBS, iveh)
			RESET_PHOTO_DATA(iveh)
			CLEAR_BIT(iVehPhotoChecksBS, iveh)
			
			IF MC_playerBD[iPartToUse].iVehNear = iveh
				MC_playerBD[iPartToUse].iVehNear = -1
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC CONVERT_AMBIENT_VEHICLE_TO_MISSION_ENTITY(INT iVeh)

	IF IS_BIT_SET(MC_serverBD.iAmbientOverrideVehicle,iVeh)
		IF NOT IS_BIT_SET(MC_serverBD.iAmbientOverrideVehicleSetup,iVeh)
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])

					VEHICLE_INDEX tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
					
					CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
					
					SET_ENTITY_AS_MISSION_ENTITY(tempVeh,TRUE,TRUE)
					
					NETWORK_INDEX tempNet = VEH_TO_NET(tempVeh)
					
					MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh] = tempNet
					
					SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh] , TRUE)	
					SET_BIT(MC_serverBD.iAmbientOverrideVehicleSetup,iVeh)
					
					SET_VEHICLE_HANDBRAKE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]), FALSE)
					PRINTLN("[RCC MISSION] CONVERT_AMBIENT_VEHICLE_TO_MISSION_ENTITY - setting up ambient vehicle: ", iveh, " turned off handbrake, time ", GET_CLOUD_TIME_AS_INT())
				ELSE
					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
				ENDIF
			ENDIF
		ENDIF
	ENDIF


ENDPROC

FUNC BOOL HAS_VEH_RESPAWN_DELAY_EXPIRED(INT iveh)
	
	BOOL bExpired

	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iRespawnDelay != 0
	AND IS_BIT_SET(MC_serverBD_2.iVehSpawnedOnce, iveh)
		IF NOT HAS_NET_TIMER_STARTED(MC_serverBD_3.tdVehSpawnDelay[iveh])
			REINIT_NET_TIMER(MC_serverBD_3.tdVehSpawnDelay[iveh])
		ELSE
			INT iTimeScale = 1
			IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
				iTimeScale = 1000
			ENDIF
			IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdVehSpawnDelay[iveh]) >= g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iRespawnDelay*iTimeScale
				bExpired = TRUE
			ENDIF
		ENDIF
	ELSE
		bExpired = TRUE
	ENDIF
	
	RETURN bExpired
	
ENDFUNC

FUNC BOOL IS_VEHICLE_OUT_OF_BOUNDS(VEHICLE_INDEX thisVeh, INT iVeh)
	
	INT iTeam
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		BOOL bValidBounds = FALSE
		BOOL bInBounds = FALSE
		
		IF iRule < FMMC_MAX_RULES
			IF DOES_ENTITY_EXIST(thisVeh)
				IF IS_VEHICLE_DRIVEABLE(thisVeh)
					IF g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct[ iRule ].fRadius > 0
						bValidBounds = TRUE
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(thisVeh),g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos,TRUE) < g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fRadius
							bInBounds = TRUE
						ELSE
							PRINTLN("[JS] IS_VEHICLE_OUT_OF_BOUNDS - OUT OF BOUNDS - RADIUS 1")
						ENDIF
					ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fWidth > 0
						bValidBounds = TRUE
						IF IS_ENTITY_IN_ANGLED_AREA(thisVeh,g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos1,g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos2,g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fWidth)
							bInBounds = TRUE
						ELSE
							PRINTLN("[JS] IS_VEHICLE_OUT_OF_BOUNDS - OUT OF BOUNDS - ANGLED AREA 1")
						ENDIF
					ENDIF
					
					IF g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct2[ iRule ].fRadius > 0
						bValidBounds = TRUE
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(thisVeh),g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].vPos,TRUE) < g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].fRadius
							bInBounds = TRUE
						ELSE
							PRINTLN("[JS] IS_VEHICLE_OUT_OF_BOUNDS - OUT OF BOUNDS - RADIUS 2")
						ENDIF
					ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].fWidth > 0
						bValidBounds = TRUE
						IF IS_ENTITY_IN_ANGLED_AREA(thisVeh,g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].vPos1,g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].vPos2,g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].fWidth)
							bInBounds = TRUE
						ELSE
							PRINTLN("[JS] IS_VEHICLE_OUT_OF_BOUNDS - OUT OF BOUNDS - ANGLED AREA 2")
						ENDIF
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[KH] IS_VEHICLE_OUT_OF_BOUNDS - VEHICLE DOES NOT EXIST!")
			ENDIF
			
			IF bValidBounds
				IF bInBounds
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLeaveAreaBitset, iRule)
						PRINTLN("[JS] IS_VEHICLE_OUT_OF_BOUNDS - iLeaveAreaBitset - bOutOfBounds TRUE")
						RETURN TRUE
					ENDIF
				ELSE
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPlayAreaBitset, iRule)
					OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_RESET_IF_OOB)
						PRINTLN("[JS] IS_VEHICLE_OUT_OF_BOUNDS - iPlayAreaBitset, ciFMMC_VEHICLE4_VEHICLE_RETAIN_CAPTURE_PROGRESS - bOutOfBounds TRUE")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF

	ENDFOR
	
	RETURN FALSE

ENDFUNC

INT iTeamVehCurrentHealth[MAX_NUM_MC_PLAYERS]
TEXT_LABEL_63 tl63_VehicleHealthString[MAX_NUM_MC_PLAYERS]

PROC GENERATE_AND_DISPLAY_TEAM_VEH_DATA(INT iThisTeam = 1)

	INT iPartLoop
	PLAYER_INDEX playerThisPlayer
	PED_INDEX pedThisPed
	PARTICIPANT_INDEX partThisParticipant
	VEHICLE_INDEX vehThisVeh
	TEXT_LABEL_23 tl23_Temp
	BOOL isLiteralString=FALSE
	
	PRINTLN( "[AW] GENERATE_AND_DISPLAY_TEAM_VEH_DATA - 0" )
	
	FOR iPartLoop = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		partThisParticipant = INT_TO_PARTICIPANTINDEX(iPartLoop)
		IF( NETWORK_IS_PARTICIPANT_ACTIVE( partThisParticipant ) )
			PRINTLN( "[AW] GENERATE_AND_DISPLAY_TEAM_VEH_DATA - 1" )
			playerThisPlayer = INT_TO_PLAYERINDEX(iPartLoop)
			pedThisPed = GET_PLAYER_PED(playerThisPlayer)
			IF NOT IS_PED_INJURED(pedThisPed)
				PRINTLN( "[AW] GENERATE_AND_DISPLAY_TEAM_VEH_DATA - 2" )
				IF MC_playerBD[iPartLoop].iteam = iThisTeam
					PRINTLN( "[AW] GENERATE_AND_DISPLAY_TEAM_VEH_DATA - 3" )
					IF IS_PED_IN_ANY_VEHICLE(pedThisPed)
						PRINTLN( "[AW] GENERATE_AND_DISPLAY_TEAM_VEH_DATA - 4" )
						vehThisVeh = GET_VEHICLE_PED_IS_IN(pedThisPed)
						IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[ iThisTeam  ].tl23ObjBlip[MC_ServerBD_4.iCurrentHighestPriority[ iThisTeam  ]])
							IF IS_PED_A_PLAYER(pedThisPed)
								PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedThisPed)
								tl63_VehicleHealthString[iPartLoop] = GET_PLAYER_NAME(tempplayer)
								//tl63_VehicleHealthString[iPartLoop] += " - "
							ENDIF
							tl23_Temp = g_FMMC_STRUCT.sFMMCEndConditions[ iThisTeam].tl23ObjBlip[MC_ServerBD_4.iCurrentHighestPriority[ iThisTeam ]]
							tl23_Temp = CONVERT_STRING_TO_UPPERCASE(tl23_Temp)
							tl63_VehicleHealthString[iPartLoop] += tl23_Temp
							isLiteralString=TRUE
						ELSE
							tl63_VehicleHealthString[iPartLoop] = "VEH_HEALTH"
						ENDIF
						
						PRINTLN( "[AW] GENERATE_AND_DISPLAY_TEAM_VEH_DATA - 5" )
						
						IF IS_VEHICLE_DRIVEABLE(vehThisVeh)
							iTeamVehCurrentHealth[iPartLoop] = GET_ENTITY_HEALTH(vehThisVeh)
						ELSE
							iTeamVehCurrentHealth[iPartLoop] = 0
						ENDIF
						DRAW_GENERIC_METER(iTeamVehCurrentHealth[iPartLoop], GET_FMMC_PLAYER_VEHICLE_HEALTH_FOR_TEAM(iThisTeam), tl63_VehicleHealthString[iPartLoop], HUD_COLOUR_WHITE, -1, HUDORDER_DONTCARE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, isLiteralString)
					ENDIF
				ENDIF
			ENDIF
		ENDIF

	ENDFOR
ENDPROC


//Team with tanks is team 1 for use in David and Goliath
PROC DISPLAY_TEAM_VEHICLE_HUD(INT iTeamToShow = 1)
	
	IF MC_ServerBD_4.iCurrentHighestPriority[ iTeamToShow ] < FMMC_MAX_RULES
	AND NOT IS_ON_SCREEN_WHEEL_SPIN_MINIGAME_ACTIVE()
		PRINTLN( "[AW] DISPLAY_TEAM_VEHICLE_HUD - 0" )
		//IF NOT g_bMissionEnding
			GENERATE_AND_DISPLAY_TEAM_VEH_DATA(iTeamToShow)
		//ENDIF
	ENDIF
ENDPROC

//FUNC BOOL HAS_TEAM_GOT_CREATOR_NAME(INT iteam)
//
//    IF g_FMMC_STRUCT.iTeamNames[iTeam] != 0
//         RETURN TRUE
//    ENDIF
//                
//    RETURN FALSE
//
//ENDFUNC


FUNC STRING GET_CREATOR_NAME_STRING_FOR_TEAM(INT iteam, BOOL bSingular = FALSE, BOOL bGenericName = FALSE)

STRING steamname
TEXT_LABEL_63 tl63TeamName 

	IF NOT bGenericName
		IF bSingular
			tl63TeamName = "FMMC_TEAM_S_"
		ELSE 
			tl63TeamName = "FMMC_TEAM_"
		ENDIF
		
		tl63TeamName += g_FMMC_STRUCT.iTeamNames[iTeam]
		
		IF g_FMMC_STRUCT.iTeamNames[iTeam] = ciTEAM_NAME_MAX
			TEXT_LABEL_63 originalString
			TEXT_LABEL_15 tl15 = "FMMC_MN"			
			IF DOES_TEXT_LABEL_EXIST(g_FMMC_STRUCT.tlCustomName[iTeam])
				originalString = GET_FILENAME_FOR_AUDIO_CONVERSATION(g_FMMC_STRUCT.tlCustomName[iTeam])
			ELSE
				originalString = g_FMMC_STRUCT.tlCustomName[iTeam]
			ENDIF
			RETURN GET_CROPPED_LITERAL_STRING_FOR_MENU(tl15, originalString)
		ENDIF
	ELSE
		SWITCH iTeam
			CASE 0
				tl63TeamName = "MC_GT_NAME_1"
			BREAK
			CASE 1
				tl63TeamName = "MC_GT_NAME_2"
			BREAK
			CASE 2
				tl63TeamName = "MC_GT_NAME_3"
			BREAK
			CASE 3
				tl63TeamName = "MC_GT_NAME_4"
			BREAK
		ENDSWITCH
	ENDIF
	
	steamname = GET_FILENAME_FOR_AUDIO_CONVERSATION(tl63TeamName)
	
	RETURN steamname
	
ENDFUNC

FUNC BOOL IS_VEHICLE_TOUCHING_THIS_VEHICLE(VEHICLE_INDEX veh1, VEHICLE_INDEX veh2)
	VECTOR vMin
	VECTOR vMax
	GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(veh1), vMin, vMax)
	VECTOR vMinOffset = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh1, <<0.0, vMin.y - 2.8, vMin.z - 1.2>>)
	VECTOR vMaxOffset = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh1, <<0.0, vMax.y + 2.8, vMax.z + 1.2>>)
	
	IF IS_ENTITY_IN_ANGLED_AREA(veh2, vMinOffset, vMaxOffset, ((vMax.x  + 2.1) - (vMin.x - 2.1)))
		PRINTLN("[LM][IS_VEHICLE_TOUCHING_THIS_VEHICLE] - Vehicles Touching.")
		RETURN TRUE
	ENDIF
	PRINTLN("[LM][IS_VEHICLE_TOUCHING_THIS_VEHICLE] - Vehicles Not Touching.")
	RETURN FALSE
ENDFUNC


// Debug only Function.
#IF IS_DEBUG_BUILD
FUNC INT GET_VEHICLE_INT_FROM_INDEX(VEHICLE_INDEX vehToCheck)
	PRINTLN("[LM][GET_VEHICLE_INT_FROM_INDEX] - starting check")
	INT i = 0
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles i
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn != DUMMY_MODEL_FOR_SCRIPT
		AND NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]) = vehToCheck
			PRINTLN("[LM][GET_VEHICLE_INT_FROM_INDEX] - Returning i: ", i)
			RETURN i
		ENDIF
	ENDREPEAT
	PRINTLN("[LM][GET_VEHICLE_INT_FROM_INDEX] - Returning -1 ... Failed? ")
	RETURN -1
ENDFUNC
#ENDIF

FUNC BOOL DOES_VEHICLE_RESIST_DUNE_FLIP(VEHICLE_INDEX VictimVeh)
	INT i
	FOR i = 0 TO MC_serverBD.iNumVehCreated - 1
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
			IF VictimVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_RESIST_EASY_EXPLODE)
					PRINTLN("[JT EXPLODE] RESISTING")
					RETURN TRUE
				ELSE
					PRINTLN("[JT EXPLODE] FALSE NATIVE_TO_INT(VictimVeh): ", NATIVE_TO_INT(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i])), " BIT SET?: ", IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_RESIST_EASY_EXPLODE))
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

PROC PROCESS_VEHICLE_COLLISION_SHOULD_EXPLODE(VEHICLE_INDEX vehToCheck, INT iIndex)
	IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(vehToCheck)
	AND iIndex != g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iIndex].iEntityIndexToCheck

		PRINTLN("[LM][PROCESS_VEHICLE_COLLISION_SHOULD_EXPLODE] - Collision Detected for iVehicle: ", iIndex)	
		PRINTLN("[LM][PROCESS_VEHICLE_COLLISION_SHOULD_EXPLODE] - We will explode if we are touched by: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iIndex].iEntityIndexToCheck)
		PRINTLN("[LM][PROCESS_VEHICLE_COLLISION_SHOULD_EXPLODE] - At this set speed: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iIndex].fVelocityReqForExplosion)
		
		VEHICLE_INDEX vehTarget		
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iIndex].iEntityIndexToCheck > -1
			PRINTLN("[LM][PROCESS_VEHICLE_COLLISION_SHOULD_EXPLODE] - Assigning from iEntityIndexToCheck")
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iIndex].iEntityIndexToCheck])
				vehTarget = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iIndex].iEntityIndexToCheck])
			ENDIF
			
		ELSE
			PRINTLN("[LM][PROCESS_VEHICLE_COLLISION_SHOULD_EXPLODE] - Assigning from Player Vehicle")
			PLAYER_INDEX piPlayer = GET_PLAYER_INDEX()
			IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(piPlayer)
			AND NOT IS_PLAYER_SPECTATOR_ONLY(piPlayer)
				PRINTLN("[LM][PROCESS_VEHICLE_COLLISION_SHOULD_EXPLODE] - Player Exists and is not a spectator")
			
				PED_INDEX pedPlayer = GET_PLAYER_PED(piPlayer)
			
				IF NOT IS_PED_INJURED(pedPlayer)
					PRINTLN("[LM][PROCESS_VEHICLE_COLLISION_SHOULD_EXPLODE] - Player is not injured")
					
					IF IS_PED_IN_ANY_VEHICLE(pedPlayer, FALSE)
						PRINTLN("[LM][PROCESS_VEHICLE_COLLISION_SHOULD_EXPLODE] - Player is in a Vehicle.")			
						vehTarget = GET_VEHICLE_PED_IS_IN(pedPlayer)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehTarget)	
			// This is needed for debugging only....
			#IF IS_DEBUG_BUILD
				PRINTLN("[LM][PROCESS_VEHICLE_COLLISION_SHOULD_EXPLODE] - Vehicle i: ", GET_VEHICLE_INT_FROM_INDEX(vehTarget))
			#ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehTarget)
				IF IS_VEHICLE_TOUCHING_THIS_VEHICLE(vehTarget, vehToCheck)
					PRINTLN("[LM][PROCESS_VEHICLE_COLLISION_SHOULD_EXPLODE] - Player Vehicle and this vehicle is touching.")
					
					FLOAT fVelReq = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iIndex].fVelocityReqForExplosion
					FLOAT fVelIncoming = GET_ENTITY_SPEED(vehTarget)
					
					PRINTLN("[LM][PROCESS_VEHICLE_COLLISION_SHOULD_EXPLODE] - fVelReq: ", fVelReq)
					PRINTLN("[LM][PROCESS_VEHICLE_COLLISION_SHOULD_EXPLODE] - fVelIncoming: ", fVelIncoming)
					
					IF fVelIncoming > fVelReq
						PRINTLN("[LM][PROCESS_VEHICLE_COLLISION_SHOULD_EXPLODE] - Explode Vehicle")
						NETWORK_EXPLODE_VEHICLE(veHToCheck, TRUE, TRUE)
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
ENDPROC

PROC DISABLE_VEHICLE_WEAPONS(VEHICLE_INDEX vehIndex)
	MODEL_NAMES mnToCheck = GET_ENTITY_MODEL(vehIndex)
	
	INT iWeaponSlotBitset = 0
	
	//Copied from weapon_enums.sch - DLC Vehicle Weapons - Gunrunning
	SWITCH mnToCheck
		CASE APC
			iWeaponSlotBitset = BIT0 | BIT1 | BIT2 | BIT3
		
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_APC_CANNON, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_APC_CANNON")
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_APC_MISSILE, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_APC_MISSILE")
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_APC_MG, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_APC_MG")
		BREAK
		CASE ARDENT
			iWeaponSlotBitset = BIT0
			
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_ARDENT_MG, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_ARDENT_MG")
		BREAK
		CASE DUNE3
			iWeaponSlotBitset = BIT0 | BIT1 | BIT2
			
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_DUNE_MG, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_DUNE_MG")
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_DUNE_GRENADELAUNCHER, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_DUNE_GRENADELAUNCHER")
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_DUNE_MINIGUN, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_DUNE_MINIGUN")
		BREAK
		CASE HALFTRACK
			iWeaponSlotBitset = BIT0 | BIT1
			
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_HALFTRACK_DUALMG, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_HALFTRACK_DUALMG")
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_HALFTRACK_QUADMG, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_HALFTRACK_QUADMG")
		BREAK
		CASE INSURGENT3
			iWeaponSlotBitset = BIT1
			
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_INSURGENT_MINIGUN, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_INSURGENT_MINIGUN")
		BREAK
		CASE NIGHTSHARK
			iWeaponSlotBitset = BIT0
			
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_NIGHTSHARK_MG, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_NIGHTSHARK_MG")
		BREAK
		CASE OPPRESSOR
			iWeaponSlotBitset = BIT0 | BIT1
			
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_OPPRESSOR_MG, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_OPPRESSOR_MG")
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_OPPRESSOR_MISSILE, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_OPPRESSOR_MISSILE")
		BREAK
		CASE TAMPA3
			iWeaponSlotBitset = BIT0 | BIT1 | BIT2 | BIT3
		
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_TAMPA_MISSILE, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_TAMPA_MISSILE")
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_TAMPA_MORTAR, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_TAMPA_MORTAR")
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_TAMPA_FIXEDMINIGUN, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_TAMPA_FIXEDMINIGUN")
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_TAMPA_DUALMINIGUN, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_TAMPA_DUALMINIGUN")
		BREAK
		CASE TECHNICAL3
			iWeaponSlotBitset = BIT1
			
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_TECHNICAL_MINIGUN, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_TECHNICAL_MINIGUN")
		BREAK
		CASE TRAILERSMALL2
			iWeaponSlotBitset = BIT0 | BIT1 | BIT2
			
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_TRAILER_QUADMG, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_TRAILER_QUADMG")
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_TRAILER_DUALAA, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_TRAILER_DUALAA")
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_TRAILER_MISSILE, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_TRAILER_MISSILE")
		BREAK
		DEFAULT
			PRINTLN("[DISABLE_VEHICLE_WEAPON] Not a configured vehicle model!")
		BREAK
	ENDSWITCH
	
	INT iWeaponSlot
	
	REPEAT 4 iWeaponSlot	//Max Vehicle Weapon Slots
		IF IS_BIT_SET(iWeaponSlotBitset, iWeaponSlot)
			SET_VEHICLE_WEAPON_RESTRICTED_AMMO(vehIndex, iWeaponSlot, 0)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] SET_VEHICLE_WEAPON_RESTRICTED_AMMO - iWeaponSlot = ", iWeaponSlot)
		ENDIF
	ENDREPEAT
ENDPROC

PROC EXPLODE_VEHICLE_AT_ZERO_HEALTH(INT iVeh, VEHICLE_INDEX vehToCheck)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_EXPLODE_AT_ZERO)
		
		FLOAT fPercentage = GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(vehToCheck, iVeh, MC_serverBD.iTotalNumStartingPlayers, IS_VEHICLE_A_TRAILER(vehToCheck))
		
		IF fPercentage <= 0
		OR NOT IS_VEHICLE_DRIVEABLE(vehToCheck)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(vehToCheck)
				PRINTLN("[RCC MISSION] EXPLODE_VEHICLE_AT_ZERO_HEALTH health: ", GET_ENTITY_HEALTH(vehToCheck))
				PRINTLN("[RCC MISSION] EXPLODE_VEHICLE_AT_ZERO_HEALTH health percentage: ", fPercentage)
				IF IS_ENTITY_ALIVE(vehToCheck) 
					NETWORK_EXPLODE_VEHICLE(vehToCheck, TRUE,TRUE)
				ENDIF
				PRINTLN("[RCC MISSION] EXPLODE_VEHICLE_AT_ZERO_HEALTH - Vehicle: ", iVeh, " ID: ", NATIVE_TO_INT(vehToCheck))
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC EXPLODE_VEHICLE_AT_ZERO_BODY_HEALTH(INT iVeh, VEHICLE_INDEX vehToCheck)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_VEHICLE_EXPLODE_AT_ZERO_BODY)
		
		IF IS_ENTITY_ALIVE(vehToCheck) 
		AND GET_VEHICLE_BODY_HEALTH(vehToCheck) <= 0.0						
			IF NETWORK_HAS_CONTROL_OF_ENTITY(vehToCheck)
				PRINTLN("[RCC MISSION] EXPLODE_VEHICLE_AT_ZERO_BODY_HEALTH health: ", GET_ENTITY_HEALTH(vehToCheck))
				PRINTLN("[RCC MISSION] EXPLODE_VEHICLE_AT_ZERO_BODY_HEALTH body health: ", GET_VEHICLE_BODY_HEALTH(vehToCheck))
				NETWORK_EXPLODE_VEHICLE(vehToCheck, TRUE, TRUE)
				PRINTLN("[RCC MISSION] EXPLODE_VEHICLE_AT_ZERO_BODY_HEALTH - Vehicle: ", iVeh, " ID: ", NATIVE_TO_INT(vehToCheck))
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DISABLE_SPECIAL_VEHICLE_WEAPONS(VEHICLE_INDEX vehToUse)
	IF GET_ENTITY_MODEL(vehToUse) = BLAZER5
		UPDATE_BLAZER_GTA_RACE(eLastVehiclePickup)
	ELIF GET_ENTITY_MODEL(vehToUse) = RUINER2
		UPDATE_RUINER_GTA_RACE(eLastVehiclePickup)
	ELSE
		eLastVehiclePickup = VEHICLE_PICKUP_INIT
	ENDIF
ENDPROC

PROC PROCESS_VEHICLE_ANCHOR_WHILE_STATIONARY(INT iVeh, VEHICLE_INDEX thisVeh)
	UNUSED_PARAMETER(iVeh)
	
	MODEL_NAMES vehModel = GET_ENTITY_MODEL(thisVeh)
	
	IF IS_THIS_MODEL_A_BOAT(vehModel)
	OR IS_THIS_MODEL_A_JETSKI(vehModel)
	OR IS_THIS_MODEL_AN_AMPHIBIOUS_CAR(vehModel)
	OR IS_THIS_MODEL_AN_AMPHIBIOUS_QUADBIKE(vehModel)
	OR vehModel = SUBMERSIBLE 
	OR vehModel = SUBMERSIBLE2
	OR vehModel = DODO
	OR vehModel = TULA
		IF IS_ENTITY_IN_WATER(ThisVeh)
			IF IS_VEHICLE_SEAT_FREE(ThisVeh)
				IF CAN_ANCHOR_BOAT_HERE(ThisVeh) // stops an assert..
					IF NOT IS_BOAT_ANCHORED(ThisVeh)
						PRINTLN("[LM][DISPLAY_VEHICLE_HUD] - ciFMMC_VEHICLE4_VEHICLE_ANCHORS_WHEN_EMPTY: Anchoring Vehicle: ", iVeh)
						SET_BOAT_ANCHOR(thisVeh, TRUE)
						SET_BOAT_REMAINS_ANCHORED_WHILE_PLAYER_IS_DRIVER(ThisVeh, TRUE)
					ENDIF
				ENDIF
			ELSE
				IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_VEH_ACCELERATE)
				OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_VEH_BRAKE)
					IF GET_SEAT_PED_IS_IN(localPlayerPed) = VS_DRIVER
						IF IS_BOAT_ANCHORED(ThisVeh)
							PRINTLN("[LM][DISPLAY_VEHICLE_HUD] - ciFMMC_VEHICLE4_VEHICLE_ANCHORS_WHEN_EMPTY: Releasing Anchor from Vehicle: ", iVeh)
							SET_BOAT_ANCHOR(thisVeh, FALSE)
							SET_BOAT_REMAINS_ANCHORED_WHILE_PLAYER_IS_DRIVER(ThisVeh, FALSE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_ENTITY_IN_AIR(thisVeh)
		AND NOT IS_VEHICLE_IN_WATER(ThisVeh)
		OR (IS_VEHICLE_ON_ALL_WHEELS(ThisVeh)
		AND NOT IS_VEHICLE_IN_WATER(ThisVeh))
			IF IS_BOAT_ANCHORED(ThisVeh)
				PRINTLN("[LM][DISPLAY_VEHICLE_HUD] - ciFMMC_VEHICLE4_VEHICLE_ANCHORS_WHEN_EMPTY: Releasing Anchor from Vehicle (failsafe): ", iVeh)
				SET_BOAT_ANCHOR(thisVeh, FALSE)
				SET_BOAT_REMAINS_ANCHORED_WHILE_PLAYER_IS_DRIVER(ThisVeh, FALSE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_DISABLE_VEHICLE_ON_FRAME(INT iVeh, VEHICLE_INDEX thisVeh)
	INT iVehDisableOnRule = g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iVehDisableOnRule[MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]]
	IF iVehDisableOnRule > -1
		PRINTLN("[LM][DISPLAY_VEHICLE_HUD] - iVehDisableOnRule: ", iVehDisableOnRule, " Vehicle: ", iVeh, " Rule: ", MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam])
		IF iVehDisableOnRule = iVeh
			IF NOT IS_PED_INJURED(LocalPlayerPed)
				IF IS_PED_IN_VEHICLE(localPlayerPed, ThisVeh)
					PRINTLN("[LM][DISPLAY_VEHICLE_HUD] - Local Player Ped is in the vehicle. Disable Movement Controls.")
					DISABLE_VEHICLE_MOVEMENT_CONTROLS_THIS_FRAME()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC
SCRIPT_TIMER cargobobDetachTimer
FUNC BOOL WAIT_FORCARGOBOB_DETACH(PED_INDEX ThisPed, INT iPed, INT iVeh)
	// If this function is returning false you'd see it in the logs
	IF IS_PED_IN_ANY_HELI(ThisPed)
		INT iIndex = MC_serverBD_2.iPedGotoProgress[iped]
		IF iIndex = GET_ASSOCIATED_GOTO_TASK_DATA__DETACH_CARGOBOB_ON_GOTO_INDEX(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData)
			IF NOT IS_BIT_SET(MC_serverBD.iCargobobDettachedBitset, iVeh)
				PRINTLN("[RCC MISSION] HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - Ped ",iped," arrived, waiting for cargobob detach: ", iVeh)
				IF HAS_NET_TIMER_STARTED(cargobobDetachTimer)
					IF HAS_NET_TIMER_EXPIRED(cargobobDetachTimer, 2000)
						SET_BIT(MC_serverBD.iCargobobShouldDetachBitset, iVeh)
						PRINTLN("[RCC MISSION] HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - Ped ",iped," cargobob detached! Should continue next frame: ", iVeh)
					ENDIF
				ELSE
					START_NET_TIMER(cargobobDetachTimer)
				ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC ENTITY_INDEX GET_WHATEVER_ATTACHED_TO_CARGOBOB(VEHICLE_INDEX thisVeh, INT entityType)
	IF entityType = ciRULE_TYPE_VEHICLE
		RETURN GET_VEHICLE_ATTACHED_TO_CARGOBOB(thisVeh) 
	ELSE
		RETURN GET_ENTITY_ATTACHED_TO_CARGOBOB(thisVeh)
	ENDIF
ENDFUNC

PROC CARGOBOB_EXPLODE_FORCE(VEHICLE_INDEX thisVeh, VEHICLE_INDEX viCarriedVeh)
	VECTOR vFwrd, vSide, vUp, v4, vOffset
	GET_ENTITY_MATRIX(thisVeh, vFwrd, vSide, vUp, v4)
	vOffset = vSide
	vOffset = vOffset * <<10,10,10>>	
	vSide = vSide * <<50,50,50>>
	vSide.z = 25
	KEEP_FORCE_UNDER_RECOMMENDED_LIMIT(vSide)
	vUp = vUp * <<-10, -10, -10>>
	PRINTLN("[RCC MISSION] CARGOBOB_EXPLODE_FORCE - Adding force to cargobob: ", vSide)
	APPLY_FORCE_TO_ENTITY(thisVeh, APPLY_TYPE_FORCE, vSide, vOffset, 0, TRUE, TRUE, TRUE )
	PRINTLN("[RCC MISSION] CARGOBOB_EXPLODE_FORCE - Adding force to viCarriedVeh: ", vUp)
	APPLY_FORCE_TO_ENTITY(viCarriedVeh, APPLY_TYPE_FORCE, vUp, v4, 0, TRUE, TRUE, TRUE )
ENDPROC

PROC PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY(INT iVeh, VEHICLE_INDEX thisVeh)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCargobobEntitySelectID > -1
		
		PRINTLN("[RCC MISSION] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Processing veh: ", iVeh)
		MODEL_NAMES mn = GET_ENTITY_MODEL(thisVeh)
		IF mn = CARGOBOB
		OR mn = CARGOBOB2
		OR mn = CARGOBOB3
			IF (NOT IS_ENTITY_DEAD(ThisVeh))
			AND IS_VEHICLE_DRIVEABLE(ThisVeh)
				IF NOT IS_BIT_SET(MC_serverBD.iCargobobDettachedBitset, iVeh)
					BOOL bInControl = NETWORK_HAS_CONTROL_OF_ENTITY(ThisVeh)
					BOOL bHost = bIsLocalPlayerHost
					IF IS_BIT_SET(MC_serverBD.iCargobobShouldDetachBitset, iVeh)
						//Needs called on all clones else they constantly reattach
						SET_CARGOBOB_FORCE_DONT_DETACH_VEHICLE(thisVeh, FALSE)
					ELIF IS_BIT_SET(MC_serverBD.iCargobobAttachedBitset, iVeh)
						SET_CARGOBOB_FORCE_DONT_DETACH_VEHICLE(thisVeh, TRUE)
					ENDIF
					PRINTLN("[RCC MISSION] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - bInControl: ", bInControl, ", bHost: ", bHost)
					IF bInControl
					OR bHost
						NETWORK_INDEX netEntityToAttach
						INT iIndexToAttach = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCargobobEntitySelectID
						INT iEntityType = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCargobobEntitySelectType
						PRINTLN("[RCC MISSION] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - iEntityType: ", iEntityType, ", iIndexToAttach: ", iIndexToAttach)
						SWITCH iEntityType
							CASE ciRULE_TYPE_PED
								netEntityToAttach = MC_serverBD_1.sFMMC_SBD.niPed[iIndexToAttach]
							BREAK
							CASE ciRULE_TYPE_VEHICLE
								netEntityToAttach = MC_serverBD_1.sFMMC_SBD.niVehicle[iIndexToAttach]
							BREAK
							CASE ciRULE_TYPE_OBJECT
								netEntityToAttach = MC_serverBD_1.sFMMC_SBD.niObject[iIndexToAttach] 
							BREAK
							DEFAULT
								EXIT
						ENDSWITCH
						BOOL useMagnet = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive, ciFMMC_VEHICLE5_CARGOBOB_PICKUP_USE_MAGNET)
						PICKUP_GADGET_TYPE pickupType = PICKUP_HOOK
						FLOAT pickupRopeLength = 4.0
						FLOAT pickupPositionOffset = -0.5
						IF useMagnet
							pickupType = PICKUP_MAGNET
							pickupRopeLength = 10.0
							//pickupPositionOffset = -2.0
						ENDIF
						IF NETWORK_DOES_NETWORK_ID_EXIST(netEntityToAttach)
						
							ENTITY_INDEX entToAttach = NET_TO_ENT(netEntityToAttach)
							VEHICLE_INDEX vehToAttach
							IF iEntityType = ciRULE_TYPE_VEHICLE
								vehToAttach = NET_TO_VEH(netEntityToAttach)
							ENDIF
							IF DOES_ENTITY_EXIST(entToAttach)
								//Player in control stuff - detaching and attaching objects
								IF bInControl
									IF (useMagnet AND DOES_CARGOBOB_HAVE_PICKUP_MAGNET(thisVeh))
									OR (NOT useMagnet AND DOES_CARGOBOB_HAVE_PICK_UP_ROPE(thisVeh))
										IF NETWORK_HAS_CONTROL_OF_ENTITY(entToAttach)
											IF IS_ENTITY_ALIVE(entToAttach)
												OBJECT_INDEX oiMagnet
												IF useMagnet
													VECTOR tempLoc = GET_ENTITY_COORDS(ThisVeh)
													oiMagnet = GET_CLOSEST_OBJECT_OF_TYPE(tempLoc, 10.0, HEI_PROP_HEIST_MAGNET, false, false, false)
													SET_CARGOBOB_PICKUP_MAGNET_ACTIVE(ThisVeh, FALSE)
													SET_CARGOBOB_PICKUP_MAGNET_REDUCED_STRENGTH(ThisVeh, 0.0)
													SET_CARGOBOB_PICKUP_MAGNET_PULL_STRENGTH(ThisVeh, 0.0)
												ELSE
													VECTOR tempLoc = GET_ATTACHED_PICK_UP_HOOK_POSITION(thisVeh)
													oiMagnet = GET_CLOSEST_OBJECT_OF_TYPE(tempLoc, 10.0, PROP_V_HOOK_S, false, false, false)
												ENDIF

												IF NOT IS_BIT_SET(MC_serverBD.iCargobobShouldDetachBitset, iVeh)
													IF NOT IS_BIT_SET(MC_serverBD.iCargobobAttachedBitset, iVeh)
														PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Attaching...")
														IF DOES_ENTITY_EXIST(oiMagnet)
															IF GET_WHATEVER_ATTACHED_TO_CARGOBOB(thisVeh, iEntityType) != entToAttach
																IF GET_ENTITY_HEIGHT_ABOVE_GROUND(thisVeh) > 8.0
																	PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Attaching attachment")
																	SET_CARGOBOB_PICKUP_MAGNET_ENSURE_PICKUP_ENTITY_UPRIGHT(thisVeh, TRUE)
																	SET_ENTITY_NO_COLLISION_ENTITY(oiMagnet, entToAttach, FALSE)
																	SET_ENTITY_NO_COLLISION_ENTITY(thisVeh, entToAttach, FALSE)
																	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(entToAttach, FALSE)
																	SET_CARGOBOB_FORCE_DONT_DETACH_VEHICLE(thisVeh, TRUE)
																	IF iEntityType = ciRULE_TYPE_VEHICLE
																		ATTACH_VEHICLE_TO_CARGOBOB(thisVeh, vehToAttach, -1, <<0,0,pickupPositionOffset>>)
																	ELSE
																		ATTACH_ENTITY_TO_CARGOBOB(thisVeh, entToAttach, -1, <<0,0,pickupPositionOffset>>)
																	ENDIF
																	FREEZE_ENTITY_POSITION(entToAttach, FALSE)
																ELSE
																	PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Else 1")
																ENDIF
															ELSE
																PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Else 2")
															ENDIF
														ELSE
															PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Else 3")
														ENDIF
													ELSE
														PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Else 4")
													ENDIF
												ELSE
													IF GET_WHATEVER_ATTACHED_TO_CARGOBOB(thisVeh, iEntityType) = entToAttach
														PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Detaching attachment")
														IF iEntityType = ciRULE_TYPE_VEHICLE
															DETACH_VEHICLE_FROM_CARGOBOB(thisVeh, vehToAttach)
														ELSE
															DETACH_ENTITY_FROM_CARGOBOB(thisVeh, entToAttach)
														ENDIF
													ELSE
														PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Attached entity isn't the same")
													ENDIF
												ENDIF
											ELSE
												PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Attachment entity isn't alive")
											ENDIF
										ELSE
											PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Requesting control of attachment")
											NETWORK_REQUEST_CONTROL_OF_ENTITY(entToAttach)
										ENDIF
									ELSE
										IF mn = CARGOBOB
										OR mn = CARGOBOB2
										OR mn = CARGOBOB3
											PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Creating rope")
											CREATE_PICK_UP_ROPE_FOR_CARGOBOB(thisVeh, pickupType)
											SET_CARGOBOB_PICKUP_ROPE_TYPE(thisVeh, pickupType)
											SET_PICKUP_ROPE_LENGTH_FOR_CARGOBOB(thisVeh, pickupRopeLength, pickupRopeLength)
											SET_CARGOBOB_PICKUP_ROPE_DAMPING_MULTIPLIER(thisVeh, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fCargobobDamp)
										ENDIF
									ENDIF
								ENDIF
								//Host stuff - checking and updating ServerBD
								IF bHost
									IF IS_BIT_SET(MC_serverBD.iCargobobShouldDetachBitset, iVeh)
										IF NOT IS_BIT_SET(MC_serverBD.iCargobobDettachedBitset, iVeh)
											IF GET_WHATEVER_ATTACHED_TO_CARGOBOB(thisVeh, iEntityType) != entToAttach
												PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY (SERVER) - Entity has been detached")
												SET_BIT(MC_serverBD.iCargobobDettachedBitset, iVeh)
											ENDIF
										ENDIF
									ELSE
										IF NOT IS_BIT_SET(MC_serverBD.iCargobobAttachedBitset, iVeh)
											IF GET_WHATEVER_ATTACHED_TO_CARGOBOB(thisVeh, iEntityType) = entToAttach
												PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY (SERVER) - Entity has been attached")
												SET_BIT(MC_serverBD.iCargobobAttachedBitset, iVeh)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Attachment entity doesn't exist")
							ENDIF
						ELSE
							PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Attachment netId doesn't exist")
						ENDIF
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Dettached")
					SET_CARGOBOB_FORCE_DONT_DETACH_VEHICLE(thisVeh, FALSE)
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Dead")
				ENTITY_INDEX eiTemp = GET_WHATEVER_ATTACHED_TO_CARGOBOB(thisVeh, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCargobobEntitySelectType)
				IF DOES_ENTITY_EXIST(eiTemp)
					SET_CARGOBOB_FORCE_DONT_DETACH_VEHICLE(thisVeh, FALSE)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(ThisVeh)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(eiTemp)
							DETACH_ENTITY_FROM_CARGOBOB(thisVeh, eiTemp)
							SET_ENTITY_NO_COLLISION_ENTITY(thisVeh, eiTemp, TRUE)
							SET_ENTITY_LOAD_COLLISION_FLAG(eiTemp, TRUE)
							IF IS_ENTITY_DEAD(ThisVeh)
							AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCargobobEntitySelectType = ciRULE_TYPE_VEHICLE
								VEHICLE_INDEX tempCarriedVeh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(eiTemp)
								CARGOBOB_EXPLODE_FORCE(ThisVeh, tempCarriedVeh)
							ENDIF
							PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Force detach as cargobob is dead")
						ELSE
							NETWORK_REQUEST_CONTROL_OF_ENTITY(eiTemp)
							PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Requesting control of attachment (cargobob dead)")
						ENDIF
					ENDIF
				ELSE
					NETWORK_INDEX netEntityToAttach
					INT iIndexToAttach = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCargobobEntitySelectID
					INT iEntityType = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCargobobEntitySelectType
					SWITCH iEntityType
						CASE ciRULE_TYPE_PED
							netEntityToAttach = MC_serverBD_1.sFMMC_SBD.niPed[iIndexToAttach]
						BREAK
						CASE ciRULE_TYPE_VEHICLE
							netEntityToAttach = MC_serverBD_1.sFMMC_SBD.niVehicle[iIndexToAttach]
						BREAK
						CASE ciRULE_TYPE_OBJECT
							netEntityToAttach = MC_serverBD_1.sFMMC_SBD.niObject[iIndexToAttach] 
						BREAK
					ENDSWITCH
					IF NETWORK_DOES_NETWORK_ID_EXIST(netEntityToAttach)
						eiTemp = NET_TO_ENT(netEntityToAttach)
					ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(eiTemp)
					IF IS_BIT_SET(MC_serverBD.iCargobobAttachedBitset, iVeh)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(eiTemp)
							IF NOT IS_BIT_SET(iVehCargoCollisionDoneBS, iVeh)
								IF IS_ENTITY_STATIC(eiTemp)
									IF NOT IS_ENTITY_WAITING_FOR_WORLD_COLLISION(eiTemp)
									AND NOT HAS_COLLISION_LOADED_AROUND_ENTITY(eiTemp)
										PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Force loading collision around cargo")
										SET_ENTITY_LOAD_COLLISION_FLAG(eiTemp, TRUE)
										SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(eiTemp, TRUE)
									ELSE
										ACTIVATE_PHYSICS(eiTemp)
										SET_ENTITY_DYNAMIC(eiTemp, TRUE)
										PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Activating physics")
									ENDIF
								ELSE
									IF HAS_COLLISION_LOADED_AROUND_ENTITY(eiTemp)
										IF NOT IS_ENTITY_IN_AIR(eiTemp)
											PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - clear load collision around cargo")
											SET_ENTITY_LOAD_COLLISION_FLAG(eiTemp, FALSE)
											SET_BIT(iVehCargoCollisionDoneBS, iVeh)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC DEBUG_PRINT_PLACED_VEHICLE_HEALTH_INFO(INT iVeh)
	IF bPlacedMissionVehiclesDebugHealth
	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
		VEHICLE_INDEX tempV = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
		IF DOES_ENTITY_EXIST(tempV)
		AND IS_ENTITY_ALIVE(tempV)
			PRINTLN("[LM][DEBUG_PRINT_PLACED_VEHICLE_HEALTH_INFO]   iVeh: ", iVeh,
			"     SCRIPT_AUTOMOBILE_", NETWORK_ENTITY_GET_OBJECT_ID(tempV),
			"     Model: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(tempV)),
			"     Body Health: ", GET_VEHICLE_BODY_HEALTH(tempV),
			"     Body Health [Creator]: ", GET_FMMC_VEHICLE_MAX_BODY_HEALTH(iveh, MC_serverBD.iTotalNumStartingPlayers),
			"     UI Health %: ", GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(tempV, iVeh, MC_serverBD.iTotalNumStartingPlayers,IS_VEHICLE_A_TRAILER(tempV)))
			
			PRINTLN("[LM][DEBUG_PRINT_PLACED_VEHICLE_HEALTH_INFO]   iVeh: ", iVeh,
			"     (Continued) - Engine Health: ", GET_VEHICLE_ENGINE_HEALTH(tempV),
			"     Engine Health [Creator]: ", GET_FMMC_VEHICLE_MAX_ENGINE_HEALTH(iVeh, MC_serverBD.iTotalNumStartingPlayers),
			"     Petrol Tank: ", GET_VEHICLE_PETROL_TANK_HEALTH(tempV),
			"     Petrol Tank [Creator]: ", GET_FMMC_VEHICLE_MAX_PETROL_TANK_HEALTH(iveh, MC_serverBD.iTotalNumStartingPlayers))
			
			IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(tempV))
				PRINTLN("[LM][DEBUG_PRINT_PLACED_VEHICLE_HEALTH_INFO]   iVeh: ", iVeh,
				"     (Continued HELI) - Rotor Tail Health: ", GET_HELI_TAIL_ROTOR_HEALTH(tempV),
				"     Rotor Tail Health [Creator]: ", GET_FMMC_HELI_MAX_TAIL_ROTOR_HEALTH(iVeh, MC_serverBD.iTotalNumStartingPlayers),
				"     Heli Rotor Main: ", GET_HELI_MAIN_ROTOR_HEALTH(tempV),
				"     Heli Rotor Main [Creator]: ", GET_FMMC_HELI_MAX_MAIN_ROTOR_HEALTH(iveh, MC_serverBD.iTotalNumStartingPlayers))
			ENDIF
			
			PRINTLN("[LM][DEBUG_PRINT_PLACED_VEHICLE_HEALTH_INFO]   (Continued) Using ciFMMC_VEHICLE6_USE_UNIFIED_ARCADE_HEALTH_BAR Override: ", IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix, ciFMMC_VEHICLE6_USE_UNIFIED_ARCADE_HEALTH_BAR))
		ELSE
			PRINTLN("[LM][DEBUG_PRINT_PLACED_VEHICLE_HEALTH_INFO] iVeh: ", iVeh, " Does not exist or is dead")
		ENDIF
	ENDIF
ENDPROC
#ENDIF

FUNC BOOL IN_RANGE_OF_VEHICLE_FOR_HEALTH_BAR(INT iVeh, VEHICLE_INDEX viVeh)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iHealthBarInRange > 0
		IF DOES_ENTITY_EXIST(viVeh)
		AND NOT IS_ENTITY_DEAD(viVeh)
			VECTOR vVehicleLoc = GET_ENTITY_COORDS(viVeh)
			IF VDIST2(vVehicleLoc, GET_PLAYER_COORDS(PlayerToUse)) < POW(TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iHealthBarInRange), 2)
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_VEH_BAR_FLASH(INT iVeh)
	
	INT iPart
	
	INT iNumberOfPlayers = (MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]) 
	INT iPlayersChecked = 0
	
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF ((NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)) OR IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
		AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_PLAYER_FAIL)
		AND IS_BIT_SET(MC_playerBD[iPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
		AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_FINISHED)
			
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
				IF IS_BIT_SET(MC_playerBD[iPart].iBurningVehFlashBS, iVeh)
					PRINTLN("[VehHud] Player ", iPart, " says it should be flashing so we're flashing the bar for veh ", iVeh)
					RETURN TRUE
				ENDIF
				
				iPlayersChecked++
			ENDIF
			
			IF iPlayersChecked >= iNumberOfPlayers
				BREAKLOOP
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_LETTER_FROM_CUSTOM_BLIP(INT iCustomBlip)

	STRING sReturn = ""
		
	SWITCH iCustomBlip
		CASE 1	RETURN " A"
		CASE 2	RETURN " B"
		CASE 3	RETURN " C"
		CASE 4	RETURN " D"
		CASE 5	RETURN " E"
		CASE 6	RETURN " F"
		CASE 7	RETURN " G"
		CASE 8	RETURN " H"
		CASE 9	RETURN " I"
		CASE 10	RETURN " J"
		CASE 11	RETURN " K"
		CASE 12	RETURN " L"
		CASE 13	RETURN " M"
		CASE 14	RETURN " N"
		CASE 15	RETURN " O"
		CASE 16	RETURN " P"
		CASE 17	RETURN " Q"
		CASE 18	RETURN " R"
		CASE 19	RETURN " S"
		CASE 20	RETURN " T"
		CASE 21	RETURN " U"
		CASE 22	RETURN " V"
		CASE 23	RETURN " W"
		CASE 24	RETURN " X"
		CASE 25	RETURN " Y"
		CASE 26	RETURN " Z"
	ENDSWITCH
	
	RETURN sReturn
ENDFUNC

PROC PROCESS_UNLOCK_VEHICLE_ON_RULE(INT iVeh, VEHICLE_INDEX viVeh)
	
	INT iUnstuckRule = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBecomesUnstuckOnRule
	
	IF iUnstuckRule = -1
	OR IS_BIT_SET(iVehicleHasBeenUnlockedBS, iVeh)
		EXIT
	ENDIF
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule < FMMC_MAX_RULES
 		IF iRule = iUnstuckRule
			PRINTLN("[PROCESS_UNLOCK_VEHICLE_ON_RULE] iVeh: ", iVeh, " unlocking as we are now on rule: ", iRule, " Vehicle should now also be considered by local player - iUnstuckRule: ", iUnstuckRule)
			BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedContentEventType_AllPlayersToggleConsiderVehicle, iVeh, DEFAULT, DEFAULT, DEFAULT, DEFAULt, DEFAULT, TRUE)
			IF DOES_ENTITY_EXIST(viVeh)
			AND IS_ENTITY_ALIVE(viVeh)
				SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(viVeh, FALSE)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(viVeh, TRUE)
			ENDIF
			SET_BIT(iVehicleHasBeenUnlockedBS, iVeh)
		ENDIF
	ENDIF

ENDPROC

PROC PROCESS_LOCK_VEHICLE_ON_RULE(INT iVeh, VEHICLE_INDEX viVeh)
	
	INT iLockRule = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBecomesLockedOnRule
	
	IF iLockRule = -1
	OR IS_BIT_SET(iVehicleHasBeenLockedBS, iVeh)
		EXIT
	ENDIF
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule < FMMC_MAX_RULES
 		IF iRule = iLockRule
			PRINTLN("[PROCESS_LOCK_VEHICLE_ON_RULE] iVeh: ", iVeh, " Locking as we are now on rule: ", iRule, " - LockRule: ", iLockRule)
			BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedContentEventType_AllPlayersToggleConsiderVehicle, iVeh, DEFAULT, DEFAULT, DEFAULT, DEFAULt, DEFAULT, FALSE)
			IF DOES_ENTITY_EXIST(viVeh)
			AND IS_ENTITY_ALIVE(viVeh)
				SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(viVeh, FALSE)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(viVeh, TRUE)
			ENDIF
			SET_BIT(iVehicleHasBeenLockedBS, iVeh)
		ENDIF
	ENDIF

ENDPROC

PROC PROCESS_FORCE_HELI_BLADES_AT_FULL_SPEED(INT iVeh, VEHICLE_INDEX ThisVeh)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_FREEZE_POS)
	AND IS_THIS_MODEL_A_PLANE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn)
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetThree, ciFMMC_VEHICLE3_VehicleStartsAirborne)
	AND NETWORK_HAS_CONTROL_OF_ENTITY(ThisVeh)
	AND NOT (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = TITAN AND IS_THIS_ROCKSTAR_MISSION_WVM_DUNE3(g_FMMC_STRUCT.iRootContentIDHash))
		SET_HELI_BLADES_FULL_SPEED(ThisVeh)
	ENDIF
ENDPROC

FUNC BOOL HAS_PLAYER_VEHICLE_COLLIDED_WITH_COP_VEHICLE(VEHICLE_INDEX viPlayerVehicle)
	
	VECTOR vMin
	VECTOR vMax
	GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(viPlayerVehicle), vMin, vMax)
	VECTOR vMinOffset = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viPlayerVehicle, <<0.0, vMin.y - 2.8, vMin.z - 1.2>>)
	VECTOR vMaxOffset = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viPlayerVehicle, <<0.0, vMax.y + 2.8, vMax.z + 1.2>>)
	
	IF IS_PED_IN_VEHICLE(LocalPlayerPed, viPlayerVehicle, TRUE)
	AND GET_ENTITY_SPEED(viPlayerVehicle) > 1.0
	AND HAS_ENTITY_COLLIDED_WITH_ANYTHING(viPlayerVehicle)
	AND IS_COP_PED_IN_AREA_3D(vMinOffset, vMaxOffset)
		PRINTLN("HAS_PLAYER_VEHICLE_COLLIDED_WITH_COP_VEHICLE - Return TRUE")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_ENTERED_WANTED_BOUNDS_IN_VEHICLE(VEHICLE_INDEX viPlayerVehicle)

	IF IS_PED_IN_VEHICLE(LocalPlayerPed, viPlayerVehicle, TRUE)
		IF (IS_BIT_SET(iLocalBoolCheck3,LBOOL3_IN_BOUNDS1) AND IS_BIT_SET(ilocalboolcheck3,LBOOL3_WANTED_BOUNDS1_TOGGLE))
		OR (IS_BIT_SET(iLocalBoolCheck3,LBOOL3_IN_BOUNDS2) AND IS_BIT_SET(ilocalboolcheck3,LBOOL3_WANTED_BOUNDS2_TOGGLE))
		AND NOT bSafeFromWantedZone
			PRINTLN("HAS_PLAYER_ENTERED_WANTED_BOUNDS_IN_VEHICLE - Return TRUE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC 

FUNC BOOL HAS_PLAYER_DONE_SOMETHING_ILLEGAL_NEAR_COP_AND_VEHICLE(VEHICLE_INDEX ThisVeh)
	
	FLOAT fCopAreaRadius = 70.0
	FLOAT fVehicleAreaRadius = 80.0
	
	IF bLocalPlayerPedOk
		FLOAT fDistFromVehicle = GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed, ThisVeh)
		
		IF fDistFromVehicle <= fVehicleAreaRadius	
		AND IS_COP_PED_IN_AREA_3D(GET_ENTITY_COORDS(LocalPlayerPed) - << fCopAreaRadius, fCopAreaRadius, fCopAreaRadius >>, GET_ENTITY_COORDS(LocalPlayerPed) + << fCopAreaRadius, fCopAreaRadius, fCopAreaRadius >>)
			IF ((fDistFromVehicle <= 10.0) AND IS_WANTED_AND_HAS_BEEN_SEEN_BY_COPS(LocalPlayer)) //First check if the player is already wanted, seen, and near/in the vehicle.
			OR IS_PED_SHOOTING(LocalPlayerPed)
			OR HAS_PLAYER_VEHICLE_COLLIDED_WITH_COP_VEHICLE(ThisVeh)
			OR IS_BIT_SET(iCopDecoyBS, ci_COP_DECOY_ENTERED_AREA)
				PRINTLN("HAS_PLAYER_DONE_SOMETHING_ILLEGAL_NEAR_COP_AND_VEHICLE - Return TRUE")
				RETURN TRUE
			ENDIF
			
			//If the player remains stationary near cops then clear the vision block after a time so they can see them. url:bugstar:6158680
			IF NOT HAS_NET_TIMER_STARTED(tdBlockWantedConeResponseStationaryTimer)
				IF IS_PED_IN_VEHICLE(LocalPlayerPed, ThisVeh, FALSE)
				AND GET_ENTITY_SPEED(ThisVeh) < 22.0
					REINIT_NET_TIMER(tdBlockWantedConeResponseStationaryTimer)
					PRINTLN("HAS_PLAYER_DONE_SOMETHING_ILLEGAL_NEAR_COP_AND_VEHICLE - Vehicle is stationary near cops, starting timer!") 
				ENDIF
			ELIF HAS_NET_TIMER_EXPIRED(tdBlockWantedConeResponseStationaryTimer, 25000)
				PRINTLN("HAS_PLAYER_DONE_SOMETHING_ILLEGAL_NEAR_COP_AND_VEHICLE - Vehicle was stationary near cops for too long! Return TRUE")
				RETURN TRUE
			ELSE 
				IF NOT IS_PED_IN_VEHICLE(LocalPlayerPed, ThisVeh, FALSE)
				OR GET_ENTITY_SPEED(ThisVeh) >= 22.0
					PRINTLN("HAS_PLAYER_DONE_SOMETHING_ILLEGAL_NEAR_COP_AND_VEHICLE - Vehicle is no longer stationary near cops, clearing timer!")
					RESET_NET_TIMER(tdBlockWantedConeResponseStationaryTimer)
				ENDIF
			ENDIF
		ELSE
			IF HAS_NET_TIMER_STARTED(tdBlockWantedConeResponseStationaryTimer)
				PRINTLN("HAS_PLAYER_DONE_SOMETHING_ILLEGAL_NEAR_COP_AND_VEHICLE - Vehicle is no longer stationary near cops, clearing timer!")
				RESET_NET_TIMER(tdBlockWantedConeResponseStationaryTimer)
			ENDIF
		ENDIF
	ENDIF
	
	IF HAS_PLAYER_ENTERED_WANTED_BOUNDS_IN_VEHICLE(ThisVeh)
		PRINTLN("HAS_PLAYER_DONE_SOMETHING_ILLEGAL_NEAR_COP_AND_VEHICLE - Return TRUE")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_PLAYER_BLIP_IN_VEHICLE_WITH_BLOCK_WANTED_CONE_RESPONSE(INT iVeh, VEHICLE_INDEX ThisVeh)
	
	BLIP_INDEX bPlayerBlip = GET_MAIN_PLAYER_BLIP_ID()
	
	IF NOT IS_BIT_SET(MC_serverBD_1.iVehicleBlockedWantedConeResponseSeenIllegalBS, iVeh)
		IF IS_PED_IN_VEHICLE(LocalPlayerPed, ThisVeh)
			IF NOT IS_BIT_SET(sLEGACYMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciLEGACY_ContinuityGenericTrackingBS_SwapVehicleUsed)
				PRINTLN("[JS][CONTINUITY] - SWAP VEHICLE - Setting swap vehicle as used")
				SET_BIT(sLEGACYMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciLEGACY_ContinuityGenericTrackingBS_SwapVehicleUsed)
			ENDIF
			IF NOT IS_BIT_SET(iLocalBoolCheck29, LBOOL29_SET_BLIP_IN_BLOCK_WANTED_CONE_RESPONSE_VEHICLE)
				IF GET_PLAYER_WANTED_LEVEL(LocalPlayer) > 0
					FORCE_START_HIDDEN_EVASION(LocalPlayer)
				ENDIF
				SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(LocalPlayer, BLIP_COLOUR_STEALTH_GREY, TRUE)
				SET_BIT(iLocalBoolCheck29, LBOOL29_SET_BLIP_IN_BLOCK_WANTED_CONE_RESPONSE_VEHICLE)
			ENDIF
			SET_PLAYER_BLIP_COLOUR(LocalPlayer, bPlayerBlip)
		ELSE
			IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_SET_BLIP_IN_BLOCK_WANTED_CONE_RESPONSE_VEHICLE)
				SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(LocalPlayer, BLIP_COLOUR_STEALTH_GREY, FALSE)
				SET_PLAYER_BLIP_COLOUR(LocalPlayer, bPlayerBlip)
				CLEAR_BIT(iLocalBoolCheck29, LBOOL29_SET_BLIP_IN_BLOCK_WANTED_CONE_RESPONSE_VEHICLE)
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_SET_BLIP_IN_BLOCK_WANTED_CONE_RESPONSE_VEHICLE)
			SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(LocalPlayer, BLIP_COLOUR_STEALTH_GREY, FALSE)
			SET_PLAYER_BLIP_COLOUR(LocalPlayer, bPlayerBlip)
			CLEAR_BIT(iLocalBoolCheck29, LBOOL29_SET_BLIP_IN_BLOCK_WANTED_CONE_RESPONSE_VEHICLE)
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_PLAYER_IN_VEHICLE_WANTED_CONE_RESPONSE(INT iVeh, VEHICLE_INDEX ThisVeh)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBlockWantedConeResponse = ci_VEHICLE_BLOCK_WANTED_CONE_RESPONSE_On
	OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBlockWantedConeResponse = ci_VEHICLE_BLOCK_WANTED_CONE_RESPONSE_UntilIllegal
		
		MAINTAIN_PLAYER_BLIP_IN_VEHICLE_WITH_BLOCK_WANTED_CONE_RESPONSE(iVeh, ThisVeh)
		
		IF NOT IS_BIT_SET(MC_serverBD_1.iVehicleBlockedWantedConeResponseSeenIllegalBS, iVeh)
			IF NOT IS_BIT_SET(MC_serverBD_1.iVehicleBlockedWantedConeResponseBS, iVeh)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
				AND IS_ENTITY_ALIVE(NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]))
					SET_DISABLE_WANTED_CONES_RESPONSE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]), TRUE)
					BROADCAST_FMMC_BLOCK_WANTED_CONE_RESPONSE_EVENT(TRUE, FALSE, iVeh, FALSE)
					PRINTLN("PROCESS_PLAYER_IN_VEHICLE_WANTED_CONE_RESPONSE - Setting vehicle: ", iVeh, " to Block wanted cone response.")
				ENDIF
			ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBlockWantedConeResponse = ci_VEHICLE_BLOCK_WANTED_CONE_RESPONSE_UntilIllegal
				IF HAS_PLAYER_DONE_SOMETHING_ILLEGAL_NEAR_COP_AND_VEHICLE(ThisVeh)
					//Switch off the blocking so the owner can unblock it below. 
					BROADCAST_FMMC_BLOCK_WANTED_CONE_RESPONSE_EVENT(FALSE, FALSE, iVeh, TRUE)
				ENDIF
			ENDIF
		ELSE //Set Off for Seen Illegal
			IF IS_BIT_SET(MC_serverBD_1.iVehicleBlockedWantedConeResponseBS, iVeh)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
				AND IS_ENTITY_ALIVE(NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]))
					SET_DISABLE_WANTED_CONES_RESPONSE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]), FALSE)
					BROADCAST_FMMC_BLOCK_WANTED_CONE_RESPONSE_EVENT(FALSE, TRUE, iVeh, FALSE)
					PRINTLN("PROCESS_PLAYER_IN_VEHICLE_WANTED_CONE_RESPONSE - Setting vehicle: ", iVeh, " to CLEAR Block wanted cone response.")
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_RESET_VEHICLE_WHEN_OOB(INT iVeh, VEHICLE_INDEX ThisVeh)
		
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_RESET_IF_OOB)
		EXIT
	ENDIF
	
	IF IS_VEHICLE_OUT_OF_BOUNDS(ThisVeh, iVeh)
	AND NOT IS_ANY_PLAYER_IN_VEHICLE(ThisVeh)	
		IF NETWORK_HAS_CONTROL_OF_ENTITY(ThisVeh)
			BOOL bForceAreaCheck = FALSE
			VECTOR vValidSpawnPoint
			FLOAT fValidHeading
			VEHICLE_SPAWN_LOCATION_PARAMS spawnParams
			spawnParams.fMaxDistance = 10.0
			INT iSpawnInterior
			IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(GET_VEHICLE_FMMC_SPAWN_LOCATION(iveh, bForceAreaCheck, iSpawnInterior, MC_ServerBD_4.iFacilityPosVehSpawned), <<0,0,0>>, GET_ENTITY_MODEL(ThisVeh), FALSE, vValidSpawnPoint, fValidHeading, spawnParams)
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DIRTY_SANCHEZ(g_FMMC_STRUCT.iAdversaryModeType)
				AND IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(ThisVeh))		
					BIKE_RESPAWNED_TICKER_MESSAGE()
				ENDIF
										
				SET_ENTITY_COORDS(ThisVeh, vValidSpawnPoint)
				INT iNumPlayers = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]
				SET_UP_FMMC_VEH_RC(ThisVeh, iveh,MC_ServerBD_1.sFMMC_SBD.niVehicle, vValidSpawnPoint, FALSE, iSpawnInterior, DEFAULT, iNumPlayers)
				PRINTLN("[JS] DISPLAY_VEHICLE_HUD IS_VEHICLE_OUT_OF_BOUNDS - VALID SPAWN POINT FOUND - VEHICLE RESPAWNED")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_HACKING_DISTANCE_CAPTURE(INT iVeh)
		
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive, ciFMMC_VEHICLE5_HACKING_USE_DISTANCE_FOR_CAPTURE)
		EXIT
	ENDIF
	
	IF MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
		IF MC_serverBD_4.ivehRule[iveh][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
		AND MC_serverBD_4.iVehPriority[iVeh][MC_playerBD[iPartToUse].iteam] = MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
			PROCESS_VEH_CAPTURE_RANGE(iVeh)
		ELSE
			IF MC_playerBD[iLocalPart].iVehFollowing = iVeh
				PRINTLN("[JS] iVehFollowing cleared (1). Was: ", iVeh)
				MC_playerBD[iLocalPart].iVehFollowing = -1
				IF iHackStage != ciSECUROSERV_HACK_STAGE_HACK_COMPLETE
					iHackStage = ciSECUROSERV_HACK_STAGE_NO_SIGNAL
				ENDIF
			ENDIF
			IF IS_BIT_SET(MC_playerBD[iLocalPart].iRequestCaptureVehBS, iVeh)
				CLEAR_BIT(MC_playerBD[iLocalPart].iRequestCaptureVehBS, iVeh)
			ENDIF
			IF DOES_BLIP_EXIST(biVehCaptureRangeBlip[iVeh])
				REMOVE_BLIP(biVehCaptureRangeBlip[iVeh])
			ENDIF
			IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iVehDestroyBitset, iVeh)
				CLEAR_BIT(MC_PlayerBD[iLocalPart].iVehDestroyBitset, iVeh)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_HELICOPTER_WEAPON_SMOKE_EFFECT(VEHICLE_INDEX viVeh, FLOAT fHealth, FLOAT fMaxBodyHealth)
	
	FLOAT fHealthThreshold = fMaxBodyHealth * 0.2	// [ML] Start engine smoke when at 20% health 
	IF fHealth < fHealthThreshold
		
		FLOAT fHealthAdjusted = (fHealthThreshold - fHealth) / fHealthThreshold
		PRINTLN("[ML][SMOKE] PROCESS_HELICOPTER_WEAPON_SMOKE_EFFECT - fHealthAdjusted: ", fHealthAdjusted, "     fHealth: ", fHealth, "   fHealthThreshold: ", fHealthThreshold)
		
		STRING sBone1 = "exhaust"
		STRING sBone2 = "exhaust_2"
		
		INT iBone1 = GET_ENTITY_BONE_INDEX_BY_NAME(viVeh, sBone1)
		INT iBone2 = GET_ENTITY_BONE_INDEX_BY_NAME(viVeh, sBone2)
		
		IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxHeliDamageSmoke1)
			PRINTLN("[ML][SMOKE] PROCESS_HELICOPTER_WEAPON_SMOKE_EFFECT - Starting helicopter damage smoke fx 1")
			USE_PARTICLE_FX_ASSET("scr_vw_finale")
			ptfxHeliDamageSmoke1 = START_PARTICLE_FX_LOOPED_ON_ENTITY_BONE("scr_vw_finale_heli_smoke", viVeh, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, iBone1)
		ELSE
			SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxHeliDamageSmoke1, "damage", fHealthAdjusted)
		ENDIF
		IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxHeliDamageSmoke2)
			PRINTLN("[ML][SMOKE] PROCESS_HELICOPTER_WEAPON_SMOKE_EFFECT - Starting helicopter damage smoke fx 2")
			USE_PARTICLE_FX_ASSET("scr_vw_finale")
			ptfxHeliDamageSmoke2 = START_PARTICLE_FX_LOOPED_ON_ENTITY_BONE("scr_vw_finale_heli_smoke", viVeh, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, iBone2)
		ELSE
			SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxHeliDamageSmoke2, "damage", fHealthAdjusted)
		ENDIF
		
	ENDIF
ENDPROC

FUNC BOOL HAS_VEHICLE_RADIO_STATION_SEQUENCE_DELAY_FINISHED(INT iVeh)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehRadioSequenceDelay = 0
		RETURN TRUE
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(tdVehicleRadioSequenceDelayTimer[iVeh])
		PRINTLN("[LM][RadioSequence] PROCESS_VEHICLE_RADIO_STATION_SEQUENCE - Start Delay")	
		START_NET_TIMER(tdVehicleRadioSequenceDelayTimer[iVeh])
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(tdVehicleRadioSequenceDelayTimer[iVeh])
	AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdVehicleRadioSequenceDelayTimer[iVeh], g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehRadioSequenceDelay*1000)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_VEHICLE_RADIO_STATION_SEQUENCE(INT iVeh, VEHICLE_INDEX tempVeh)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].eVehRadioSequenceType = VEHICLE_RADIO_SEQUENCE_TYPE_NONE
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iVehicleRadioSequenceBitset, iVeh)
		EXIT
	ENDIF
	
	IF NOT IS_PED_IN_VEHICLE(localPlayerped, tempVeh, FALSE)
		EXIT
	ENDIF
	
	IF IS_PED_GETTING_INTO_A_VEHICLE(localPlayerped)
		EXIT
	ENDIF
	
	IF NOT HAS_VEHICLE_RADIO_STATION_SEQUENCE_DELAY_FINISHED(iVeh)
		PRINTLN("[LM][RadioSequence] PROCESS_VEHICLE_RADIO_STATION_SEQUENCE - Delay has not finished.")
		EXIT
	ENDIF
	
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].eVehRadioSequenceType
		CASE VEHICLE_RADIO_SEQUENCE_TYPE_CASINO_CELEBRITY
			PRINTLN("[LM][RadioSequence] PROCESS_VEHICLE_RADIO_STATION_SEQUENCE - Trying to play special radio sequence... VEHICLE_RADIO_SEQUENCE_TYPE_CASINO_CELEBRITY")	
			
			FREEZE_RADIO_STATION("RADIO_23_DLC_XM19_RADIO")
			SET_RADIO_TRACK_WITH_START_OFFSET("RADIO_23_DLC_XM19_RADIO", "DLC_HEIST3_CRIME_PAYS", 120000)
			UNFREEZE_RADIO_STATION("RADIO_23_DLC_XM19_RADIO")
			SET_NEXT_RADIO_TRACK("RADIO_23_DLC_XM19_RADIO", "RADIO_TRACK_CAT_DJSOLO", "", "dlc_ch_finale_radio_djsolo")
			
			SET_RADIO_TO_STATION_NAME("RADIO_23_DLC_XM19_RADIO")
			
			IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
				SET_VEH_RADIO_STATION(tempVeh, "RADIO_23_DLC_XM19_RADIO")
			ENDIF
			
			//SET_MOBILE_PHONE_RADIO_STATE(FALSE)
			//SET_AUDIO_FLAG("MobileRadioInGame", FALSE)
			
			SET_BIT(iVehicleRadioSequenceBitset, iVeh)
			
			PRINTLN("[LM][RadioSequence] PROCESS_VEHICLE_RADIO_STATION_SEQUENCE - (SET_RADIO_TRACK_WITH_START_OFFSET) - RadioStationName: RADIO_23_DLC_XM19_RADIO, TrackName: RADIO_TRACK_CAT_DJSOLO, Time: 120000")
			PRINTLN("[LM][RadioSequence] PROCESS_VEHICLE_RADIO_STATION_SEQUENCE 			 - (SET_NEXT_RADIO_TRACK) - RadioStationName: RADIO_23_DLC_XM19_RADIO, category: RADIO_TRACK_CAT_DJSOLO, TrackIndex: dlc_heist_finale_custom_radio_djsolo ")
		BREAK		
	ENDSWITCH	
			
ENDPROC

PROC DISPLAY_VEHICLE_HUD(INT iveh) 

	PED_INDEX tempPed
	VEHICLE_INDEX tempVeh, trailerVeh,WinchVeh,ThisVeh
	ENTITY_INDEX Winchent
		
	BOOL bHostOfScript = bIsLocalPlayerHost
	INT iMyTeam = MC_playerBD[iPartToUse].iteam
	INT iTeam // Used for loops
	BOOL bNotDriveable
	HUD_COLOURS eTeamColour
	IF bHostOfScript
	AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		IF IS_BIT_SET(MC_serverBD.iVehCleanup_NeedOwnershipBS, iveh)
		
			PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - iVehCleanup_NeedOwnershipBS set for veh ",iveh)
		
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
				ThisVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
				
				PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - NETWORK_DOES_NETWORK_ID_EXIST exists for veh ",iveh)
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iRespawnOnRuleChangeBS > 0
					IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(ThisVeh)
						IF IS_VEHICLE_EMPTY(ThisVeh)
						AND NOT IS_ANY_PLAYER_IN_VEHICLE(ThisVeh, TRUE)
						AND IS_VEHICLE_SEAT_FREE(ThisVeh, VS_DRIVER)
							PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - Requesting control of veh ",iveh," for cleanup - for delete, objective: ",g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iCleanupObjective," team: ",g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iCleanupTeam," vehicle: ",iveh)
							NETWORK_REQUEST_CONTROL_OF_ENTITY(ThisVeh)
						ENDIF
					ELSE
						IF (IS_ANY_TEAM_BEING_WARPED_TO_START_POINT()
						AND IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_ALL_PLAYERS_RENDERPHASES_PAUSED))
						OR (NOT IS_ANY_TEAM_BEING_WARPED_TO_START_POINT() AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_MINI_ROUNDS_WAIT_FOR_VEH_RESPAWN))
							IF IS_VEHICLE_EMPTY(ThisVeh)
							AND NOT IS_ANY_PLAYER_IN_VEHICLE(ThisVeh, TRUE)
							AND IS_VEHICLE_SEAT_FREE(ThisVeh, VS_DRIVER)
								PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - Deleting vehicle ",iveh)
								DELETE_FMMC_VEHICLE(iveh)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	PROCESS_HACKING_DISTANCE_CAPTURE(iVeh)
		
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
		
		IF bHostOfScript
			IF IS_BIT_SET(MC_serverBD.iVehCleanup_NeedOwnershipBS, iveh)
				// Mini-rounds missions we don't care about this assert for. (Placed Vehicles)
				IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMBUSHKA(g_FMMC_STRUCT.iAdversaryModeType)
				AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUNNING_BACK_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
					CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION] DISPLAY_VEHICLE_HUD - Veh ",iveh," we were trying to request ownership of for cleanup no longer exists!")
				ENDIF
				PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - Veh ",iveh," we were trying to request ownership of for cleanup no longer exists!")
				CLEAR_BIT(MC_serverBD.iVehCleanup_NeedOwnershipBS, iveh)
			ENDIF
	
			IF IS_BIT_SET(MC_serverBD.iVehSpawnBitset, iveh)
				PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - Clear iVehSpawnBitset for veh ",iveh,", net ID doesn't exist")
				CLEAR_BIT(MC_serverBD.iVehSpawnBitset, iveh)
			ENDIF
			CLEAR_BIT(iDisableVehicleWeaponBitset, iVeh)
		
			IF GET_VEHICLE_RESPAWNS(iVeh) > 0
				IF SHOULD_VEH_RESPAWN_NOW(iveh)
				AND HAS_VEH_RESPAWN_DELAY_EXPIRED(iveh)
					IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iAttachParent > -1
					AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iAttachParentType = CREATION_TYPE_VEHICLES

						IF NOT IS_NET_VEHICLE_DRIVEABLE(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iAttachParent])
						AND SHOULD_VEH_RESPAWN_NOW(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iAttachParent)
							PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - Waiting for veh ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iAttachParent," to spawn")
							EXIT
						ENDIF
					
					ENDIF
					IF RESPAWN_MISSION_VEHICLE(iveh)
						
						SET_BIT(MC_serverBD_2.iVehSpawnedOnce, iveh)
						IF IS_BIT_SET(MC_serverBD_2.iVehIsDestroyed, iveh)
							CLEAR_BIT(MC_serverBD_2.iVehIsDestroyed, iveh)
						ENDIF
						
						IF IS_BIT_SET(MC_ServerBD.iServerBS_PassRuleVehAfterRespawn, iVeh)
							PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - Clearing iServerBS_PassRuleVehAfterRespawn on iVeh, ", iVeh)
							CLEAR_BIT(MC_ServerBD.iServerBS_PassRuleVehAfterRespawn, iVeh)
						ENDIF
						
						IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iRespawnOnRuleChangeBS > 0
							FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
								IF MC_serverBD_4.iVehPriority[iveh][iTeam] < FMMC_MAX_RULES
									SET_BIT(MC_serverBD.iVehteamFailBitset[iVeh],iteam)
								ENDIF
							ENDFOR
						ENDIF
						
						RESET_NET_TIMER(MC_serverBD_3.tdVehSpawnDelay[iveh])
												
						IF DOES_VEH_HAVE_PLAYER_VARIABLE_SETTING(iveh)
							IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iAssociatedObjective > -1
							AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iAssociatedTeam > -1
								IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iAssociatedTeam] < FMMC_MAX_RULES
									MC_serverBD_1.sCreatedCount.iNumVehCreated[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iAssociatedObjective]++
								ENDIF
							ENDIF
						ENDIF
						IF GET_VEHICLE_RESPAWNS(iVeh) != UNLIMITED_RESPAWNS
							DECREMENT_VEHICLE_RESPAWNS(iVeh)
						ENDIF
					ENDIF
				ELSE
					IF MC_serverBD.isearchingforVeh = iveh
						IF NETWORK_ENTITY_AREA_DOES_EXIST(MC_serverBD.iSpawnArea)
							NETWORK_REMOVE_ENTITY_AREA(MC_serverBD.iSpawnArea)
							PRINTLN("[JS] DISPLAY_VEHICLE_HUD MC_serverBD.iSpawnArea Reset veh: ", iveh)
						ENDIF
						RESET_NET_TIMER(MC_serverBD.tdSpawnAreaTimeout)
						MC_serverBD.iSpawnArea = -1
						MC_serverBD.isearchingforVeh = -1						
					ENDIF
					
					
					IF DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME()
					AND (NOT CAN_VEH_STILL_SPAWN_IN_MISSION(iveh))
						
						MC_serverBD.iVehteamFailBitset[iveh] = 0
						
						FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
							MC_serverBD_4.iVehPriority[iveh][iTeam] = FMMC_PRIORITY_IGNORE
							MC_serverBD_4.ivehRule[iveh][iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
							CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iTeam], iveh)
						ENDFOR
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCargobobEntitySelectID > -1
			IF IS_BIT_SET(MC_serverBD.iCargobobAttachedBitset, iVeh)
				NETWORK_INDEX netEntityToAttach
				INT iIndexToAttach = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCargobobEntitySelectID
				INT iEntityType = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCargobobEntitySelectType
				SWITCH iEntityType
					CASE ciRULE_TYPE_PED
						netEntityToAttach = MC_serverBD_1.sFMMC_SBD.niPed[iIndexToAttach]
					BREAK
					CASE ciRULE_TYPE_VEHICLE
						netEntityToAttach = MC_serverBD_1.sFMMC_SBD.niVehicle[iIndexToAttach]
					BREAK
					CASE ciRULE_TYPE_OBJECT
						netEntityToAttach = MC_serverBD_1.sFMMC_SBD.niObject[iIndexToAttach] 
					BREAK
				ENDSWITCH
				IF NETWORK_DOES_NETWORK_ID_EXIST(netEntityToAttach)
					ENTITY_INDEX eiTemp = NET_TO_ENT(netEntityToAttach)
				
					IF IS_ENTITY_ALIVE(eiTemp)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(eiTemp)
							IF NOT IS_BIT_SET(iVehCargoCollisionDoneBS, iVeh)
								IF IS_ENTITY_STATIC(eiTemp)
									IF NOT IS_ENTITY_WAITING_FOR_WORLD_COLLISION(eiTemp)
									AND NOT HAS_COLLISION_LOADED_AROUND_ENTITY(eiTemp)
										PRINTLN("[JS] DISPLAY_VEHICLE_HUD PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Force loading collision around cargo 2")
										SET_ENTITY_LOAD_COLLISION_FLAG(eiTemp, TRUE)
										SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(eiTemp, TRUE)
									ELSE
										ACTIVATE_PHYSICS(eiTemp)
										SET_ENTITY_DYNAMIC(eiTemp, TRUE)
										PRINTLN("[JS] DISPLAY_VEHICLE_HUD PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Activating physics 2")
									ENDIF
								ELSE
									IF HAS_COLLISION_LOADED_AROUND_ENTITY(eiTemp)
										IF NOT IS_ENTITY_IN_AIR(eiTemp)
											PRINTLN("[JS] DISPLAY_VEHICLE_HUD PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - clear load collision around cargo 2")
											SET_ENTITY_LOAD_COLLISION_FLAG(eiTemp, FALSE)
											SET_BIT(iVehCargoCollisionDoneBS, iVeh)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		EXIT
		
	ELSE
		ThisVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
		
		// At the start so we get the print before script modifications.	 
		#IF IS_DEBUG_BUILD
			DEBUG_PRINT_PLACED_VEHICLE_HEALTH_INFO(iVeh)
		#ENDIF
		
		IF bHostOfScript		
			CONVERT_AMBIENT_VEHICLE_TO_MISSION_ENTITY(iveh)
			IF IS_VEHICLE_DRIVEABLE(ThisVeh)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = OPPRESSOR
					IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVModTurret != GET_VEHICLE_MOD(ThisVeh, MOD_ROOF)			
						PRELOAD_VEHICLE_MOD(ThisVeh, MOD_ROOF, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVModTurret)
						IF HAS_PRELOAD_MODS_FINISHED(ThisVeh)
							SET_VEHICLE_MOD(ThisVeh, MOD_ROOF, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVModTurret)
						ENDIF
					ENDIF
				ENDIF
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix, ciFMMC_VEHICLE6_GHOST_PLAYER_VEHICLES)
					MC_serverBD_3.iGhostingPlayerVehiclesVeh = iveh
					PRINTLN("[RCC MISSION][DISPLAY_VEHICLE_HUD] Setting vehicle ", iveh," GHOST PLAYER VEHICLES")
				ENDIF
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFive, ciFMMC_VEHICLE5_DELAYED_DOOR_OPEN)
				AND NOT IS_BIT_SET(MC_serverBD.iOpenVehicleDoorsBitset, iveh)
					IF HAS_NET_TIMER_STARTED(MC_serverBD.stVehicleDoorsOpenTimer)
						IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(ThisVeh)
							NETWORK_REQUEST_CONTROL_OF_ENTITY(ThisVeh)
						ENDIF
						IF iDebugVehicleDelayedDoorOpeningIndex != -1
						AND iDebugVehicleDelayedDoorOpeningIndex != iVeh
							SCRIPT_ASSERT("[RCC MISSION] DISPLAY_VEHICLE_HUD - DELAYED OPEN DOOR - Only one door can be opening at once")
							iDebugVehicleDelayedDoorOpeningIndex = iVeh
						ENDIF
						IF HAS_NET_TIMER_EXPIRED(MC_serverBD.stVehicleDoorsOpenTimer, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehicleDelayedDoorTimer * 1000)
							IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFive, ciFMMC_VEHICLE5_DELAYED_DOOR_OPEN_SECOND_CHECK)
								PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - DELAYED OPEN DOOR - Timer expired, setting doors open")
								SET_BIT(MC_serverBD.iOpenVehicleDoorsBitset, iveh)
								RESET_NET_TIMER(MC_serverBD.stVehicleDoorsOpenTimer)
								iDebugVehicleDelayedDoorOpeningIndex = -1
							ELSE
								INT iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY(ThisVeh)
								PLAYER_INDEX ClosestPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
								IF IS_NET_PLAYER_OK(ClosestPlayer)
									PED_INDEX ClosestPlayerPed = GET_PLAYER_PED(ClosestPlayer)
									IF GET_DISTANCE_BETWEEN_ENTITIES(ThisVeh,ClosestPlayerPed, FALSE) < g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fVehicleDelayedDoorRange
										PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - DELAYED OPEN DOOR - Timer expired and second range check satisfied, setting doors open")
										SET_BIT(MC_serverBD.iOpenVehicleDoorsBitset, iveh)
										RESET_NET_TIMER(MC_serverBD.stVehicleDoorsOpenTimer)
										iDebugVehicleDelayedDoorOpeningIndex = -1
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehicleDelayedDoorRule = -1
						OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehicleDelayedDoorRule = MC_serverBD_4.iCurrentHighestPriority[0]
							IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(ThisVeh)
								NETWORK_REQUEST_CONTROL_OF_ENTITY(ThisVeh)
							ENDIF
							IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fVehicleDelayedDoorRange < 0.0
								IF iDebugVehicleDelayedDoorOpeningIndex != -1
									SCRIPT_ASSERT("[RCC MISSION] DISPLAY_VEHICLE_HUD - DELAYED OPEN DOOR - Only one door can be opening at once")
								ENDIF
								START_NET_TIMER(MC_serverBD.stVehicleDoorsOpenTimer)
								iDebugVehicleDelayedDoorOpeningIndex = iVeh
								PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - DELAYED OPEN DOOR - No range check, starting timer")
							ELSE
								INT iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY(ThisVeh)
								PLAYER_INDEX ClosestPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
								IF IS_NET_PLAYER_OK(ClosestPlayer)
									PED_INDEX ClosestPlayerPed = GET_PLAYER_PED(ClosestPlayer)
									IF GET_DISTANCE_BETWEEN_ENTITIES(ThisVeh,ClosestPlayerPed, FALSE) < g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fVehicleDelayedDoorRange
										START_NET_TIMER(MC_serverBD.stVehicleDoorsOpenTimer)
										IF iDebugVehicleDelayedDoorOpeningIndex != -1
											SCRIPT_ASSERT("[RCC MISSION] DISPLAY_VEHICLE_HUD - DELAYED OPEN DOOR - Only one door can be opening at once")
										ENDIF
										iDebugVehicleDelayedDoorOpeningIndex = iVeh
										PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - DELAYED OPEN DOOR - Range check succeeded, starting timer ")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				CLEAR_BIT(MC_serverBD.iOpenVehicleDoorsBitset, iveh)
				RESET_NET_TIMER(MC_serverBD.stVehicleDoorsOpenTimer)
				iDebugVehicleDelayedDoorOpeningIndex = -1
			ENDIF
		ENDIF
				
		PROCESS_VEHICLE_RADIO_STATION_SEQUENCE(iVeh, ThisVeh)
		
		PROCESS_FORCE_HELI_BLADES_AT_FULL_SPEED(iVeh, ThisVeh)
		
		PROCESS_PLAYER_IN_VEHICLE_WANTED_CONE_RESPONSE(iVeh, ThisVeh)
		
		PROCESS_LOCK_VEHICLE_ON_RULE(iVeh, ThisVeh)
		
		PROCESS_UNLOCK_VEHICLE_ON_RULE(iVeh, ThisVeh)
		
		PROCESS_RESET_VEHICLE_WHEN_OOB(iVeh, ThisVeh)
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix, ciFMMC_VEHICLE6_BLOCK_MOVING_EXIT)
			IF IS_ENTITY_ALIVE(ThisVeh)
				IF IS_PED_IN_VEHICLE(LocalPlayerPed, ThisVeh, FALSE)
					IF IS_ENTITY_IN_AIR(ThisVeh)
					OR VMAG2(GET_ENTITY_VELOCITY(ThisVeh)) > POW(cfMIN_MOVING_VEH_SPEED, 2.0)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Setting to flash the player's vehicle if they are not currently in it
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix, ciFMMC_VEHICLE6_FLASH_WHEN_NO_PLAYER_IN)
			IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, TRUE)
			AND NOT IS_ANY_PLAYER_IN_VEHICLE(ThisVeh)
				INT mAlpha = GET_ENTITY_ALPHA(ThisVeh)
				IF mAlpha <= 70
					mAlpha = 255
				ELSE
					mAlpha = mAlpha-10
				ENDIF
				SET_ENTITY_ALPHA(ThisVeh, mAlpha, TRUE)
				SET_BLIP_FLASHES(biVehBlip[iveh], TRUE)
			ELSE
				SET_ENTITY_ALPHA(ThisVeh, 255, TRUE)
				SET_BLIP_FLASHES(biVehBlip[iveh], FALSE)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetEight, ciFMMC_VEHICLE8_ENGINE_SMOKE_ON_LOW_BODY_HEALTH)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(VEH_TO_NET(ThisVeh))
				IF IS_ENTITY_ALIVE(ThisVeh)
					FLOAT fEngineHealthForSmoke = GET_VEHICLE_BODY_HEALTH(ThisVeh)
					
					IF NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(ThisVeh))
						SET_VEHICLE_ENGINE_HEALTH(ThisVeh, fEngineHealthForSmoke)
						PRINTLN("[ML] Vehicle body health is ", GET_VEHICLE_BODY_HEALTH(ThisVeh), "   Vehicle engine health is ", GET_VEHICLE_ENGINE_HEALTH(ThisVeh), " | Setting engine health to ", fEngineHealthForSmoke)
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_ENTITY_ALIVE(ThisVeh)
				FLOAT fEngineHealthForSmoke = GET_VEHICLE_BODY_HEALTH(ThisVeh)
				FLOAT fVehicleMaxBodyHealth = GET_FMMC_VEHICLE_MAX_BODY_HEALTH(iveh)
				
				IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(ThisVeh))
					PROCESS_HELICOPTER_WEAPON_SMOKE_EFFECT(ThisVeh, fEngineHealthForSmoke, fVehicleMaxBodyHealth)
				ENDIF
			ELSE
				IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxHeliDamageSmoke1)
					STOP_PARTICLE_FX_LOOPED(ptfxHeliDamageSmoke1)
				ENDIF
				IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxHeliDamageSmoke2)
					STOP_PARTICLE_FX_LOOPED(ptfxHeliDamageSmoke2)
				ENDIF				
			ENDIF		
		ENDIF
		
		//Sets the player's vehicle blip to flash when they are no longer on it
		IF IS_THIS_ROCKSTAR_MISSION_WVM_OPPRESSOR_2(g_FMMC_STRUCT.iRootContentIDHash)
			//Set the index of the vehicle that the player is on
			IF iIndexOfVehPlayerIsIn = -1
				IF IS_PED_IN_VEHICLE(LocalPlayerPed, ThisVeh, TRUE)
					iIndexOfVehPlayerIsIn = iVeh
				ENDIF
			ELSE
				//If we're on the vehicle we're currently looking at and the index of the one we're on is the same as this then we stop the blip flashing
				IF IS_PED_IN_VEHICLE(LocalPlayerPed, ThisVeh, TRUE)
				AND iVeh = iIndexOfVehPlayerIsIn
					SET_BLIP_FLASHES(biVehBlip[iIndexOfVehPlayerIsIn], FALSE)
				ELSE
					SET_BLIP_FLASHES(biVehBlip[iIndexOfVehPlayerIsIn], TRUE)
				ENDIF
			ENDIF
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehicleFlashBlipRule != -1
		AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehicleFlashBlipRule < FMMC_MAX_RULES
		AND NOT IS_BIT_SET(iVehBlipFlashExpiredBS, iveh)
			IF NOT IS_BIT_SET(iVehBlipFlashActiveBS, iveh)
				FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
					IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
					AND MC_serverBD_4.iCurrentHighestPriority[iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehicleFlashBlipRule
						IF DOES_BLIP_EXIST(biVehBlip[iVeh])
							SET_BLIP_FLASHES(biVehBlip[iVeh], TRUE)
							SET_BIT(iVehBlipFlashActiveBS, iveh)
							PRINTLN("[RCC MISSION][DISPLAY_VEHICLE_HUD] Starting flashing blip. Rule: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehicleFlashBlipRule, " Duration: ", (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehicleFlashBlipDuration * 1000))
						ENDIF
					ENDIF
				ENDFOR
			ELSE
				IF HAS_NET_TIMER_STARTED(tdVehicleBlipFlash[iVeh])
					IF HAS_NET_TIMER_EXPIRED(tdVehicleBlipFlash[iVeh], (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehicleFlashBlipDuration * 1000))
						IF DOES_BLIP_EXIST(biVehBlip[iVeh])
							SET_BLIP_FLASHES(biVehBlip[iVeh], FALSE)
							RESET_NET_TIMER(tdVehicleBlipFlash[iVeh])
							SET_BIT(iVehBlipFlashExpiredBS, iveh)
							PRINTLN("[RCC MISSION][DISPLAY_VEHICLE_HUD] Ending flashing blip")
						ELSE
							RESET_NET_TIMER(tdVehicleBlipFlash[iVeh])
							SET_BIT(iVehBlipFlashExpiredBS, iveh)
							PRINTLN("[RCC MISSION][DISPLAY_VEHICLE_HUD] Ending flashing blip")
						ENDIF
					ENDIF
				ELSE
					REINIT_NET_TIMER(tdVehicleBlipFlash[iVeh])
				ENDIF
			ENDIF
		ENDIF
		
		//Setting to place a marker above the vehicle when the player is not in the vehicle we're currently looking at
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_PLACE_CHEVRON)
			IF NOT IS_ENTITY_DEAD(ThisVeh) AND NOT IS_PED_IN_VEHICLE(LocalPlayerPed, ThisVeh, TRUE)
				FM_EVENTDRAW_MARKER_ABOVE_VEHICLE(ThisVeh,
				g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iChevronColours[0],
				g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iChevronColours[1],
				g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iChevronColours[2])
			ENDIF			
		ENDIF
		
		//Setting to completely ignore a vehicle - this will mean the player cannot enter or will even recognise there's a vehicle there
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix, ciFMMC_VEHICLE6_VEHICLE_NOT_CONSIDERED_BY_PLAYERS)
			IF NOT IS_BIT_SET(iVehConsideredBS, iVeh)
				IF NOT IS_ENTITY_DEAD(ThisVeh)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(ThisVeh, FALSE)
					SET_BIT(iVehConsideredBS, iVeh)
					PRINTLN("[JS] DISPLAY_VEHICLE_HUD Setting not considered by player for veh: ", iVeh)
				ENDIF
			ENDIF
		ENDIF			
		
		IF IS_BIT_SET( g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_BlockModShopOptions)
			IF IS_ENTITY_ALIVE(ThisVeh)
				IF NOT IS_BIT_SET(iVehicleModShopOptionsBlockedBS, iVeh)
					INT iModsLoop = 0
					FOR iModsLoop = 0 TO (NUMBER_OF_CAR_MODS - 1)
						IF iModsLoop = ENUM_TO_INT(CMM_MAIN)
						OR iModsLoop = ENUM_TO_INT(CMM_BUY)
						OR iModsLoop = ENUM_TO_INT(CMM_MOD)
						OR iModsLoop = ENUM_TO_INT(CMM_MAINTAIN)
							PRINTLN("[ML] Vehicle: ", iVeh, "    Not blocking car mod ", iModsLoop)
						ELSE
							PRINTLN("[ML] Vehicle: ", iVeh, "    Blocking car mod ", iModsLoop)
							BLOCK_CARMOD_MENU_OPTION_FOR_CREATOR_MISSION(ThisVeh, INT_TO_ENUM(CARMOD_MENU_ENUM, iModsLoop) , TRUE)
						ENDIF
					ENDFOR
					SET_BIT(iVehicleModShopOptionsBlockedBS, iVeh)
				ENDIF
			ENDIF
		ENDIF
				
		IF NETWORK_HAS_CONTROL_OF_ENTITY(ThisVeh)
			IF IS_VEHICLE_DRIVEABLE(ThisVeh)
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFive, ciFMMC_VEHICLE5_DELAYED_DOOR_OPEN)
				AND IS_BIT_SET(MC_serverBD.iOpenVehicleDoorsBitset, iveh)
					IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
						IF IS_ENTITY_CARGOBOB(ThisVeh) // Might need for some planes?
							PRINTLN("[LM] DISPLAY_VEHICLE_HUD - Preventing NetID from migrating as it is a cargobob and door opening ratios are not net synced.")
							SET_NETWORK_ID_CAN_MIGRATE(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh], FALSE)
						ENDIF
					ENDIF
					SET_VEHICLE_DOOR_OPEN(ThisVeh,g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].eVehicleDelayedDoorWhichDoor)
					SET_OVERRIDE_VEHICLE_DOOR_TORQUE(ThisVeh, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].eVehicleDelayedDoorWhichDoor, TRUE)
				ENDIF
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iLinkedDestroyVeh != -1
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iLinkedDestroyVeh])
						IF IS_ENTITY_DEAD(NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iLinkedDestroyVeh]))
							NETWORK_EXPLODE_VEHICLE(ThisVeh, TRUE, FALSE)
							PRINTLN("[JS] DISPLAY_VEHICLE_HUD Linked Vehicle: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iLinkedDestroyVeh, " is dead exploding vehicle: ", iVeh)
						ENDIF
					ENDIF
				ENDIF
				
				// Velocity dependent collision explosions
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fVelocityReqForExplosion > -1.0
					PROCESS_VEHICLE_COLLISION_SHOULD_EXPLODE(ThisVeh, iVeh)
				ENDIF
				
				// Anchor Vehicle Bitset
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_ANCHORS_WHEN_EMPTY)
					PROCESS_VEHICLE_ANCHOR_WHILE_STATIONARY(iVeh, ThisVeh)					
				ENDIF

				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iSpawnNearType = ciSPAWN_NEAR_ENTITY_TYPE_LAST_PLAYER
				AND MC_PlayerBD[iLocalPart].iLastVeh != iVeh
					IF IS_PED_IN_VEHICLE(LocalPlayerPed, ThisVeh, TRUE)
						MC_PlayerBD[iLocalPart].iLastVeh = iVeh
						PRINTLN("[JS] DISPLAY_VEHICLE_HUD Setting iLastVeh to veh: ", iVeh)
					ENDIF
				ENDIF
				
				IF MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] < FMMC_MAX_RULES
					PROCESS_DISABLE_VEHICLE_ON_FRAME(iVeh, ThisVeh)	
					
					IF GET_ENTITY_MODEL(ThisVeh) = OPPRESSOR
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]], ciBS_RULE9_DISABLE_OPPRESSOR_BOOST)
						OR IS_BIT_SET(iLocalBoolCheck24, LBOOL24_BLOCK_VEHICLE_KERS_ON_RULE_CHECK)
							SET_VEHICLE_KERS_ALLOWED(ThisVeh, FALSE)
						ELSE							
							SET_VEHICLE_KERS_ALLOWED(ThisVeh, TRUE)							
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive,  ciFMMC_VEHICLE5_PERMANENT_BRAKES_ON)
					SET_VEHICLE_BRAKE(ThisVeh, 5.0)
					SET_VEHICLE_HANDBRAKE(ThisVeh, TRUE)
					SET_VEHICLE_BRAKE_LIGHTS(ThisVeh, FALSE)
					
					#IF IS_DEBUG_BUILD
						IF (GET_FRAME_COUNT() % 15) = 0
							PRINTLN("[ciFMMC_VEHICLE5_PERMANENT_BRAKES_ON] DISPLAY_VEHICLE_HUD Maintaining Permanent Hand and Regular Brakes for iVeh: ", iVeh)
						ENDIF
					#ENDIF
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive,  ciFMMC_VEHICLE5_DISABLE_VEHICLE_WEAPONS)
				OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciDISABLE_SPECIAL_VEHICLE_WEAPONS)
					DISABLE_SPECIAL_VEHICLE_WEAPONS(ThisVeh)
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_DISABLE_VEHICLE_WEAPONS)
					IF IS_PED_IN_VEHICLE(LocalPlayerPed, ThisVeh)
						IF NOT IS_BIT_SET(iDisableVehicleWeaponBitset, iVeh)
							DISABLE_VEHICLE_WEAPONS(ThisVeh)
							
							SET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED)
							SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED, TRUE)
							
							SET_BIT(iDisableVehicleWeaponBitset, iVeh)
						ENDIF
					ELSE
						CLEAR_BIT(iDisableVehicleWeaponBitset, iVeh)
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_DISABLE_SPACE_ROCKETS)
					IF NOT IS_PED_INJURED(localPlayerPed)
						IF IS_PED_IN_VEHICLE(LocalPlayerPed, ThisVeh, TRUE)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_NEXT_WEAPON)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_PREV_WEAPON)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_NEXT_WEAPON)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_PREV_WEAPON)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_UD)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_LR)

							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_IHELP_BUZ")
								HIDE_HELP_TEXT_THIS_FRAME()
							ENDIF
							
							WEAPON_TYPE weap		
							GET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, weap)
							
							IF weap = WEAPONTYPE_VEHICLE_SPACE_ROCKET
							OR weap != WEAPONTYPE_VEHICLE_PLAYER_BUZZARD
							OR NOT IS_BIT_SET(iDisableVehicleWeaponBitset, iVeh)
								DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_VEHICLE_SPACE_ROCKET, ThisVeh, LocalPlayerPed)
								SET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, WEAPONTYPE_VEHICLE_PLAYER_BUZZARD)
								SET_BIT(iDisableVehicleWeaponBitset, iVeh)
							ENDIF
						ELSE
							IF IS_BIT_SET(iDisableVehicleWeaponBitset, iVeh)
								CLEAR_BIT(iDisableVehicleWeaponBitset, iVeh)
							ENDIF
						ENDIF
					ELSE
						IF IS_BIT_SET(iDisableVehicleWeaponBitset, iVeh)
							CLEAR_BIT(iDisableVehicleWeaponBitset, iVeh)
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSeven, ciFMMC_VEHICLE7_KEEP_DOORS_OPEN)
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet, enum_to_int(SC_DOOR_FRONT_LEFT))
						SET_VEHICLE_DOOR_CONTROL(ThisVeh, SC_DOOR_FRONT_LEFT, DT_DOOR_NO_RESET, 1.0)
					ENDIF
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet, enum_to_int(SC_DOOR_FRONT_RIGHT))
						SET_VEHICLE_DOOR_CONTROL(ThisVeh, SC_DOOR_FRONT_RIGHT, DT_DOOR_NO_RESET, 1.0)
					ENDIF
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet, enum_to_int(SC_DOOR_REAR_LEFT))
						SET_VEHICLE_DOOR_CONTROL(ThisVeh, SC_DOOR_REAR_LEFT, DT_DOOR_NO_RESET, 1.0)
					ENDIF
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet, enum_to_int(SC_DOOR_REAR_RIGHT))
						SET_VEHICLE_DOOR_CONTROL(ThisVeh, SC_DOOR_REAR_RIGHT, DT_DOOR_NO_RESET, 1.0)
					ENDIF
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet, enum_to_int(SC_DOOR_BONNET))
						SET_VEHICLE_DOOR_CONTROL(ThisVeh, SC_DOOR_BONNET, DT_DOOR_NO_RESET, 1.0)
					ENDIF
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet, enum_to_int(SC_DOOR_BOOT))
						SET_VEHICLE_DOOR_CONTROL(ThisVeh, SC_DOOR_BOOT, DT_DOOR_NO_RESET, 1.0)
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetEight, ciFMMC_VEHICLE8_EXPLODE_IN_WATER)
					IF IS_ENTITY_ALIVE(ThisVeh)
					AND IS_ENTITY_SUBMERGED_IN_WATER(ThisVeh, DEFAULT, 0.3)
						NETWORK_EXPLODE_VEHICLE(ThisVeh, TRUE, FALSE)
						PRINTLN("DISPLAY_VEHICLE_HUD - Exploding vehicle as it is submerged in water with option set.")
					ENDIF	
				ENDIF
			ELSE
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix, ciFMMC_VEHICLE6_DISABLE_BREAKING)
					IF NOT IS_ENTITY_DEAD(ThisVeh)
						SET_VEHICLE_CAN_BREAK(ThisVeh, TRUE)
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetEight, ciFMMC_VEHICLE8_EXPLODE_IN_WATER)
					IF IS_ENTITY_ALIVE(ThisVeh)
					AND IS_ENTITY_IN_WATER(ThisVeh)
						NETWORK_EXPLODE_VEHICLE(ThisVeh, TRUE, TRUE)
						PRINTLN("DISPLAY_VEHICLE_HUD - Exploding vehicle as it is in water with option set.")
					ENDIF	
				ENDIF
				
				CLEAR_BIT(iDisableVehicleWeaponBitset, iVeh)
			ENDIF
		ENDIF
		
		EXPLODE_VEHICLE_AT_ZERO_HEALTH(iVeh,ThisVeh)
		
		EXPLODE_VEHICLE_AT_ZERO_BODY_HEALTH(iVeh,ThisVeh)
				
		PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY(iVeh, ThisVeh)
	
		RUN_UPDATE_ON_VEHICLE_BLIP(iveh, ThisVeh)
		
		SET_DECORATOR_ON_MOC_TRAILER(iveh, ThisVeh)
		
		SET_DECORATOR_ON_AVENGER(iveh, ThisVeh)
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_TAGGING_TARGETS)
			IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam] < FMMC_MAX_RULES
				IF IS_IT_SAFE_TO_ADD_TAG_BLIP(MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam])	
					INT iTagged
					FOR iTagged = 0 TO FMMC_MAX_TAGGED_ENTITIES-1
						IF MC_serverBD_3.iTaggedEntityType[iTagged] = ENUM_TO_INT(GET_ENTITY_TYPE(ThisVeh))
							IF MC_serverBD_3.iTaggedEntityIndex[iTagged] = iveh
								IF IS_ENTITY_ALIVE(ThisVeh)
									VECTOR vEntityCoords = GET_ENTITY_COORDS(ThisVeh)
									vEntityCoords.z = (vEntityCoords.z + 1.0)
									DRAW_TAGGED_MARKER(vEntityCoords, ThisVeh, ENUM_TO_INT(GET_ENTITY_TYPE(ThisVeh)), iveh)
									
									IF NOT DOES_BLIP_EXIST(biTaggedEntity[iTagged])
									AND NOT DOES_BLIP_EXIST(GET_BLIP_FROM_ENTITY(ThisVeh))
										ADD_TAGGED_BLIP(ThisVeh, ENUM_TO_INT(GET_ENTITY_TYPE(ThisVeh)), iveh, iTagged)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
		ENDIF
							
		//EMP Light/Sound Effects
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetTwo, ciFMMC_VEHICLE2_EXTRA_EMP)
			UPDATE_LIGHTS_ON_ENTITY(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[iveh]))
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetTwo, ciFMMC_VEHICLE2_TriggerAudioOnVehicle) 
				IF NOT IS_ENTITY_DEAD(ThisVeh)
					PLAY_SOUND_FROM_ENTITY(-1, "EMP_Vehicle_Hum", ThisVeh, "DLC_HEIST_BIOLAB_DELIVER_EMP_SOUNDS")
					SET_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetTwo, ciFMMC_VEHICLE2_TriggerAudioOnVehicle) 
				ENDIF
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF bEnableDamageTrackerOnNetworkID
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
					IF NOT IS_DAMAGE_TRACKER_ACTIVE_ON_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
						ACTIVATE_DAMAGE_TRACKER_ON_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh], TRUE)
						PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD ACTIVATE_DAMAGE_TRACKER_ON_NETWORK_ID on vehicle: ", iveh)
					ENDIF
				ENDIF
			ENDIF
		#ENDIF
		
		IF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_SUBMARINE_FINALE)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = AVENGER AND IS_ENTITY_ALIVE(ThisVeh)
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen,ciENABLE_AVENGER_INTERIOR_ENTRY)
					IF IS_PLAYER_IN_CREATOR_AIRCRAFT(LocalPlayer)
						IF IS_VEHICLE_ON_ALL_WHEELS(ThisVeh)
						AND ARE_VECTORS_ALMOST_EQUAL(NETWORK_GET_LAST_VEL_RECEIVED_OVER_NETWORK(ThisVeh),  <<0,0,0>>)
							IF IS_AVENGER_NON_COCKPIT_EXIT_BLOCKED()
								BLOCK_AVENGER_NON_COCKPIT_EXIT(FALSE)
								
								CLEAR_BIT(iLocalBoolCheck27, LBOOL27_AVENGER_EXIT_BLOCKED)
								PRINTLN("[RCC MISSION] Avenger is on the ground/not moving. Enabling exit.")
							ENDIF
						ELSE
							IF NOT IS_AVENGER_NON_COCKPIT_EXIT_BLOCKED()
								BLOCK_AVENGER_NON_COCKPIT_EXIT(TRUE)
								SET_BIT(iLocalBoolCheck27, LBOOL27_AVENGER_EXIT_BLOCKED)
								PRINTLN("[RCC MISSION] Avenger is off the ground/moving. Blocking exit.")
							ENDIF 
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, TRUE)
			IF GET_VEHICLE_PED_IS_IN(LocalPlayerPed, TRUE) = ThisVeh
			AND GET_INTERIOR_FROM_ENTITY(LocalPlayerPed) = GET_INTERIOR_AT_COORDS(vHangarInteriorCoords)
				SET_THIRD_PERSON_CAM_ORBIT_DISTANCE_LIMITS_THIS_UPDATE(10,15)
				SET_THIRD_PERSON_CAM_RELATIVE_PITCH_LIMITS_THIS_UPDATE(-13, 16)
				PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - In an Avenger inside the hangar. Setting third person camera limits.")
			ENDIF
		ENDIF
		
		IF NOT IS_VEHICLE_DRIVEABLE(ThisVeh)
			
			IF IS_BIT_SET(MC_serverBD.iVehSpawnBitset, iveh)
			AND bHostOfScript
				PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - Clear iVehSpawnBitset for veh ",iveh,", undriveable")
				CLEAR_BIT(MC_serverBD.iVehSpawnBitset, iveh)
			ENDIF
			
			bNotDriveable = TRUE
			
			IF bHostOfScript
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = GBURRITO
				OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = GBURRITO2
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetTwo, ciFMMC_VEHICLE2_EXTRA_MINI_SERVER) //Check if the broken server prop has already been created
					AND NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[iveh])
						
						MODEL_NAMES mnBrokenCrate = HEI_PROP_MINI_SEVER_BROKEN
						
						MODEL_NAMES mnCurrentCrate = GET_ENTITY_MODEL(NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[iveh]))
						
						IF mnCurrentCrate != mnBrokenCrate
							
							IF CAN_REGISTER_MISSION_OBJECTS( 1 )
								
								REQUEST_MODEL(mnBrokenCrate)
								
								//Need control of the vehicle to attach the new crate to it:
								IF NETWORK_HAS_CONTROL_OF_ENTITY(ThisVeh)
									
									//Need control of the old crate to delete it:
									IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[iveh])
										
										IF HAS_MODEL_LOADED(mnBrokenCrate)
											
											MC_RESERVE_NETWORK_MISSION_OBJECTS(1)
											
											NETWORK_INDEX niBrokenCrate
											STRING sBone = GET_VEHICLE_CRATE_BONE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetTwo, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSeven, ThisVeh)
											
											PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - Creating new object to replace ",iveh,"'s crate in back with broken server - bone index ",GET_ENTITY_BONE_INDEX_BY_NAME(ThisVeh, sBone))
											
											INT iBone = GET_ENTITY_BONE_INDEX_BY_NAME(ThisVeh, sBone)
											VECTOR vBone = GET_WORLD_POSITION_OF_ENTITY_BONE(ThisVeh, iBone)
											
											PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - Creating new object to replace ",iveh,"'s crate in back with broken server - bone index ",iBone,", bone loc ",vBone)
											
											IF CREATE_NET_OBJ(niBrokenCrate, mnBrokenCrate, vBone)
												
												PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - Removing ",iveh,"'s old crate, as new broken server has been created!")
												
												//Get rid of the old crate:
												DELETE_NET_ID(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[iveh])
												
												OBJECT_INDEX oiBrokenCrate = NET_TO_OBJ(niBrokenCrate)
												
												ATTACH_ENTITY_TO_ENTITY(oiBrokenCrate, ThisVeh, iBone, GET_CRATE_OFFSET_FOR_VEHICLE(ThisVeh, oiBrokenCrate), GET_CRATE_ROTATION_FOR_VEHICLE(ThisVeh, oiBrokenCrate), TRUE, FALSE, SHOULD_CRATE_COLLISION_BE_ENABLED(ThisVeh))
												SET_ENTITY_LOD_DIST(oiBrokenCrate, 100)
												SET_ENTITY_VISIBLE(oiBrokenCrate, TRUE)
												SET_MODEL_AS_NO_LONGER_NEEDED(mnBrokenCrate)
												
												//Update server variable so we can clean it up normally:
												MC_serverBD_1.sFMMC_SBD.niVehicleCrate[iveh] = niBrokenCrate
												
											ENDIF
										#IF IS_DEBUG_BUILD
										ELSE
											PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - Waiting for model of ",iveh,"'s crate to replace crate in back with broken server")
										#ENDIF
										ENDIF
									ELSE
										PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - Requesting control of vehicle ",iveh,"'s crate to replace crate in back with broken server")
										NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[iveh])
									ENDIF
								ELSE
									PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - Requesting control of vehicle ",iveh," to replace crate in back with broken server")
									NETWORK_REQUEST_CONTROL_OF_ENTITY(ThisVeh)
								ENDIF
							#IF IS_DEBUG_BUILD
							ELSE
								PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - Unable to register 1 more mission object to replace crate in back of vehicle ",iveh," with broken server")
							#ENDIF
							ENDIF
							
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		else 
		
		
			//calculate tracked vehicle healths as a percentage
			//*****make a function out of this
			//track_specific_vehicle_damage_and_time
					
			//track_vehicle_damage_health_percentage_and_time(int iveh)			
			
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iHeistCSVehDmgTrackBitSet, iveh)
			
				if does_entity_exist(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]))
					
					if is_vehicle_driveable(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]))
					
						if is_ped_sitting_in_vehicle(player_ped_id(), NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]))
						
							if get_ped_in_vehicle_seat(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])) = player_ped_id()
							//or is_turret_seat(mission_vehicle, get_seat_ped_is_in(player_ped_id()))
							
								FLOAT fPercentage = GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]), iVeh, MC_serverBD.iTotalNumStartingPlayers)
								
								float current_vehicle_health
								
								current_vehicle_health = fPercentage
					
								if not is_bit_set(vehicle_safe_damage_bit_set, iveh)
									
									vehicle_previous_damage_percentage[iveh] = current_vehicle_health
									
									set_bit(vehicle_safe_damage_bit_set, iveh)
								endif 

								//cache the time the player has been in the vehicle so when we do the 
								//calculation we favour the player who has been in the car the longest if both
								//players have the same damage percentage. 
								if get_entity_speed(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])) > 1.0
									vehicle_damage_time[iveh] += get_frame_time()
								endif 
								
								//update the player broadcast data every 10s
								if not has_net_timer_started(safe_drive_stat_time)
									start_net_timer(safe_drive_stat_time)
								else 
									if get_net_timer_difference_with_current_time(safe_drive_stat_time) > 5000
									
										PRINTLN("[LK MISSION] DISPLAY_VEHICLE_HUD vehicle_damage_time[", iveh, "] = ", vehicle_damage_time[iveh])

										MC_playerBD[iLocalPart].veh_damage_time += vehicle_damage_time[iveh]
									
										vehicle_damage_time[iveh] = 0  
									
										reset_net_timer(safe_drive_stat_time)
										
										PRINTLN("[LK MISSION] DISPLAY_VEHICLE_HUD MC_playerBD[", iLocalPart, "].veh_damage_time = ", MC_playerBD[iLocalPart].veh_damage_time)
									endif 
								endif
								
								
								//might add into 5s MC_playerBD if it is causing issues where it gets broad cast
								//to many times and asserts - machine gun
								if (current_vehicle_health < vehicle_previous_damage_percentage[iveh])
								
									MC_playerBD[iLocalPart].veh_damage_percentage += (vehicle_previous_damage_percentage[iveh] - current_vehicle_health)

									vehicle_previous_damage_percentage[iveh] = current_vehicle_health
									
									PRINTLN("[LK MISSION] DISPLAY_VEHICLE_HUD MC_playerBD[", iLocalPart, "].veh_damage_percentage = ", MC_playerBD[iLocalPart].veh_damage_percentage)
									
									PRINTLN("[LK MISSION] DISPLAY_VEHICLE_HUD vehicle_previous_damage_percentage[", iveh, "] = ", vehicle_previous_damage_percentage[iveh])
									
								endif 

							else 
							
								if is_bit_set(vehicle_safe_damage_bit_set, iveh)
									clear_bit(vehicle_safe_damage_bit_set, iveh)
								endif 
							
							endif
							
						else 
						
							if is_bit_set(vehicle_safe_damage_bit_set, iveh)
								clear_bit(vehicle_safe_damage_bit_set, iveh)
							endif 
								
						endif 
					
					else
					
						
						//if is_bit_set(vehicle_safe_damage_bit_set, iveh) then the player must have been in the
						//vehicle before it became undriveable. So if the health percentage is 2% then it will be added to MC_playerBD[iLocalPart].veh_damage_percentage
						if is_bit_set(vehicle_safe_damage_bit_set, iveh)
						
							//increment the last small health value of the car
							FLOAT fPercentage = GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]), iVeh, MC_serverBD.iTotalNumStartingPlayers)
							
							MC_playerBD[iLocalPart].veh_damage_percentage += fPercentage
							
							CLEAR_BIT(vehicle_safe_damage_bit_set, iveh)
							
							PRINTLN("[LK MISSION] DISPLAY_VEHICLE_HUD GET_VEHICLE_HEALTH_PERCENTAGE = ", fPercentage)
							
							PRINTLN("[LK MISSION] DISPLAY_VEHICLE_HUD MC_playerBD[iLocalPart].veh_damage_percentage[", iveh, "] = ", MC_playerBD[iLocalPart].veh_damage_time)
							
						endif 
					
					endif 
				
				endif 

			endif
		
		ENDIF
	ENDIF
	
	//Update all blips
	INT i
	
	FOR i = 0 TO FMMC_MAX_VEHICLES - 1
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
			IF DOES_BLIP_EXIST(biVehCaptureRangeBlip[i])
				SET_BLIP_COORDS(biVehCaptureRangeBlip[i], GET_ENTITY_COORDS(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]), FALSE))
			ENDIF
		ENDIF
	ENDFOR
	

	
	IF bHostOfScript
		IF IS_BIT_SET(MC_serverBD.iVehCleanup_NeedOwnershipBS, iveh)
			
			IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(ThisVeh)
				IF IS_VEHICLE_EMPTY(ThisVeh)
				AND NOT IS_ANY_PLAYER_IN_VEHICLE(ThisVeh, TRUE)
				AND IS_VEHICLE_SEAT_FREE(ThisVeh, VS_DRIVER)
					PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - Requesting control of veh ",iveh," for cleanup - for delete, objective: ",g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iCleanupObjective," team: ",g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iCleanupTeam," vehicle: ",iveh)
					NETWORK_REQUEST_CONTROL_OF_ENTITY(ThisVeh)
				ENDIF
			ELSE
				IF (IS_ANY_TEAM_BEING_WARPED_TO_START_POINT()
				AND IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_ALL_PLAYERS_RENDERPHASES_PAUSED))
				OR NOT IS_ANY_TEAM_BEING_WARPED_TO_START_POINT()
					IF IS_VEHICLE_EMPTY(ThisVeh)
					AND NOT IS_ANY_PLAYER_IN_VEHICLE(ThisVeh, TRUE)
					AND IS_VEHICLE_SEAT_FREE(ThisVeh, VS_DRIVER)
						PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD -  (2) Deleting vehicle ",iveh)
						DELETE_FMMC_VEHICLE(iveh)
					ELSE
						// Adding this hasn't seemed to cause any issue, but may need to be removed... We shall see. 
						// Fixes asserts related to deleting vehicles with players in them.
						PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD -  (2) Cannot delete vehicle. Waiting for a player to exit iVeh: ",iveh)
					ENDIF					 
				ENDIF
				EXIT
			ENDIF
			
		ELIF DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME(TRUE)
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_MINI_ROUNDS_WAIT_FOR_VEH_RESPAWN)	
			IF MC_serverBD.iVehteamFailBitset[iveh] > 0
				FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
					IF IS_BIT_SET(MC_serverBD.iVehteamFailBitset[iveh], iTeam)
					AND (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iMissionCriticalRemovalRule[iTeam] > 0)
					AND MC_serverBD_4.iCurrentHighestPriority[iTeam] >= g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iMissionCriticalRemovalRule[iTeam]
						CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iTeam], iveh)
						CLEAR_BIT(MC_serverBD.iVehteamFailBitset[iveh], iTeam)
						PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - Veh ",iveh," set as no longer critical for team ",iteam," as they have reached iMissionCriticalRemovalRule ",g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iMissionCriticalRemovalRule[iTeam])
					ENDIF
				ENDFOR
			ENDIF
			
			IF CLEANUP_VEH_EARLY(iveh, ThisVeh)
				PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - Veh ",iveh," has cleaned up early, new priority/midpoint check")
				EXIT
			ENDIF
			
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iMissionCriticalLinkedVeh > -1
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
			AND NOT bNotDriveable
				FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
					IF IS_BIT_SET(MC_serverBD.iVehteamFailBitset[iveh], iTeam)
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iMissionCriticalLinkedVeh])
							IF NOT IS_ENTITY_DEAD(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iMissionCriticalLinkedVeh]))
								PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - Veh ",iveh," set as no longer critical for team ",iteam," as linked veh is ok: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iMissionCriticalLinkedVeh)
								CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iTeam], iVeh)
								CLEAR_BIT(MC_serverBD.iVehteamFailBitset[iVeh], iTeam)
							ENDIF
						ENDIF
					ELSE
						IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iMissionCriticalLinkedVeh])
						OR IS_ENTITY_DEAD(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iMissionCriticalLinkedVeh]))
							PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - Veh ",iveh," set as  critical for team ",iteam," as linked veh is NOT ok: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iMissionCriticalLinkedVeh)
							SET_BIT(MC_serverBD.iMissionCriticalVeh[iTeam], iVeh)
							SET_BIT(MC_serverBD.iVehteamFailBitset[iVeh], iTeam)
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
	ENDIF
	
	IF bNotDriveable
		EXIT
	ENDIF
	
	IF NETWORK_HAS_CONTROL_OF_ENTITY(ThisVeh)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iInvincibleOnRules >= 0
			INT currentRule = MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iInvincibleOnRulesTeam]
			IF currentRule+1 < FMMC_MAX_RULES
				BOOL bInvincible = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iInvincibleOnRules, currentRule+1)
				SET_ENTITY_INVINCIBLE(ThisVeh, bInvincible)
				SET_ENTITY_PROOFS(ThisVeh,bInvincible,bInvincible,bInvincible,bInvincible,bInvincible,bInvincible)
			ENDIF
		ENDIF
	ENDIF
	IF iSpectatorTarget = -1
		CHECK_SEATING_PREFERENCES(iveh,iPartToUse)
		CHECK_VEHICLE_TARGETING(iveh)
	ENDIF
	
	// disable being able to move vehicle when another player has reserved the vehicle
	IF( MC_serverBD.iVehRequestPart[ iveh ] <> -1 AND MC_serverBD.iVehRequestPart[ iveh ] <> iPartToUse )
		IF( IS_PED_IN_VEHICLE( LocalPlayerPed, ThisVeh, TRUE ) )
			DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_ACCELERATE )
			DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_BRAKE )
			CPRINTLN( DEBUG_SIMON, "DISPLAY_VEHICLE_HUD - Disabling ped ability to move veh because of server data" )
		ENDIF
	ENDIF
	IF( sCurrentSpecialPickupState.iTrashDriveEnable = iVeh )
		IF( IS_PED_IN_VEHICLE( LocalPlayerPed, ThisVeh, TRUE ) )
			DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_ACCELERATE )
			DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_BRAKE )
			CPRINTLN( DEBUG_SIMON, "DISPLAY_VEHICLE_HUD - Disabling ped ability to move veh because of local data" )
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet, ciFMMC_VEHICLE_ChangeBootWhenDropOff)
	AND NETWORK_HAS_CONTROL_OF_ENTITY(ThisVeh)
		
		BOOL bVehBootOpen = FALSE
		INT iTeamLoop
		
		FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
			IF MC_serverBD_4.iCurrentHighestPriority[iTeamLoop] < FMMC_MAX_RULES
			AND (g_FMMC_STRUCT.sFMMCEndConditions[iTeamLoop].iDropOffType[MC_serverBD_4.iCurrentHighestPriority[iTeamLoop]] = ciFMMC_DROP_OFF_TYPE_VEHICLE)
			AND (g_FMMC_STRUCT.sFMMCEndConditions[iTeamLoop].iDropOff_Vehicle[MC_serverBD_4.iCurrentHighestPriority[iTeamLoop]] = iveh)
			AND IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeamLoop],MC_serverBD_4.iCurrentHighestPriority[iTeamLoop]) //At the objective midpoint (ie the package/whatever has been collected and is ready for delivery)
				bVehBootOpen = TRUE
				iTeamLoop = MC_serverBD.iNumberOfTeams // Break out!
			ENDIF
		ENDFOR
		
		IF(bVehBootOpen)
			IF GET_VEHICLE_DOOR_ANGLE_RATIO(ThisVeh,SC_DOOR_BOOT) < 0.95
				SET_VEHICLE_DOOR_OPEN(ThisVeh,SC_DOOR_BOOT)
			ENDIF
		ELSE
			IF GET_VEHICLE_DOOR_ANGLE_RATIO(ThisVeh,SC_DOOR_BOOT) > 0.05
				SET_VEHICLE_DOOR_SHUT(ThisVeh,SC_DOOR_BOOT,FALSE)
			ENDIF
		ENDIF
		
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciDISABLE_SPECIAL_VEHICLE_WEAPONS)
		IF NOT IS_PED_INJURED(LocalPlayerPed)
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				IF IS_ENTITY_ALIVE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
					DISABLE_SPECIAL_VEHICLE_WEAPONS(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
				ELSE
					eLastVehiclePickup = VEHICLE_PICKUP_INIT
				ENDIF
			ELSE
				eLastVehiclePickup = VEHICLE_PICKUP_INIT
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciCARGO_SUFFERS_COLLISION_DAMAGE)
		FLOAT fVehHealth, fParentHealth 
		VEHICLE_INDEX parentVeh
			
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAttachParent >= 0
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAttachParent])
				parentVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAttachParent])
			ENDIF
			fVehHealth = GET_VEHICLE_BODY_HEALTH(ThisVeh)
			fParentHealth = GET_VEHICLE_BODY_HEALTH(parentVeh)
			
			IF fVehHealth != fParentHealth
				IF bIsLocalPlayerHost
					IF NETWORK_HAS_CONTROL_OF_ENTITY(ThisVeh)
						PRINTLN("[RCC MISSION] [DISPLAY_VEHICLE_HUD][RUIN] setting Vehicle health to ", fParentHealth, " from ", fVehHealth)
						SET_VEHICLE_BODY_HEALTH(ThisVeh,fParentHealth)
						SET_ENTITY_HEALTH(ThisVeh,CEIL(fParentHealth))
						
						PRINTLN("[RCC MISSION] [DISPLAY_VEHICLE_HUD][RUIN] Cargo vehicle health: ", GET_VEHICLE_BODY_HEALTH(ThisVeh))
					ELSE
						PRINTLN("[RCC MISSION] [DISPLAY_VEHICLE_HUD][RUIN] Requesting control")
						NETWORK_REQUEST_CONTROL_OF_ENTITY(ThisVeh)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		IF (GET_FRAME_COUNT() % 120) = 0
			PRINTLN("[RCC MISSION] [DISPLAY_VEHICLE_HUD][RUIN] BIT IS NOT SET")
		ENDIF
		#ENDIF
	ENDIF
	
	IF GET_ENTITY_MODEL(ThisVeh) = STROMBERG
		IF IS_PED_IN_VEHICLE(LocalPlayerPed, ThisVeh)
		AND IS_ENTITY_IN_WATER(ThisVeh)
			SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisableAutoForceOutWhenBlowingUpCar, TRUE)
		ENDIF
	ENDIF
	
	
	IF MC_ServerBD_4.iCurrentHighestPriority[ iMyTeam ] < FMMC_MAX_RULES
		// If the vehicle has been set to show a health bar for this rule, then do it. Need to account for three different systems of vehicle health, and trigger values for undrivebale being -1000 on the engine and petrol tank. 
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iMyTeam ].iRuleBitset[MC_ServerBD_4.iCurrentHighestPriority[ iMyTeam ]], ciBS_RULE_ENABLE_HEALTH_BAR) 
			IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetTwo, ciFMMC_VEHICLE2_ENABLE_HEALTH_BAR )
			AND (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iHealthBarRule = -1 OR (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iHealthBarRule > -1 AND MC_ServerBD_4.iCurrentHighestPriority[iMyTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iHealthBarRule))
				IF NOT g_bMissionEnding
				AND NOT IS_PED_INJURED(localPlayerPed)
				AND NOT IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_TEAM_0_SHOULD_RESPAWN_BACK_AT_START_NOW + iMyTeam)
					IF IN_RANGE_OF_VEHICLE_FOR_HEALTH_BAR(iVeh, ThisVeh)
						TEXT_LABEL_63 tl63_Temp
						BOOL isLiteralString=FALSE
						BOOL isPlayerString=FALSE
						TEXT_LABEL_23 tl23_Temp
						INT iTeamForName = iMyTeam
						IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
							IF MC_serverBD_4.iVehPriority[iveh][iMyTeam] != MC_ServerBD_4.iCurrentHighestPriority[iMyTeam]
								//Not our team's priority, check if it is someone else's
								INT iTeamLoop
								FOR iTeamLoop = 0 TO MC_ServerBD.iNumberOfTeams - 1
									IF MC_serverBD_4.iVehPriority[iveh][iTeamLoop] = MC_ServerBD_4.iCurrentHighestPriority[iTeamLoop]
										iTeamForName = iTeamLoop
										BREAKLOOP
									ENDIF
								ENDFOR
							ENDIF
						ENDIF
						
						IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[iTeamForName].tl63ObjectiveEntityHUDMessage[MC_ServerBD_4.iCurrentHighestPriority[iTeamForName]])
							tl23_Temp = g_FMMC_STRUCT.sFMMCEndConditions[iTeamForName].tl63ObjectiveEntityHUDMessage[MC_ServerBD_4.iCurrentHighestPriority[iTeamForName]]
						ELIF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl23CustomTargetString[iTeamForName])
							tl23_Temp = g_FMMC_STRUCT.tl23CustomTargetString[iTeamForName]
						ELIF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[ iTeamForName ].tl23ObjBlip[MC_ServerBD_4.iCurrentHighestPriority[ iTeamForName ]])
							tl23_Temp = g_FMMC_STRUCT.sFMMCEndConditions[ iTeamForName ].tl23ObjBlip[MC_ServerBD_4.iCurrentHighestPriority[ iTeamForName ]]
						ENDIF
						
						IF NOT IS_STRING_NULL_OR_EMPTY(tl23_Temp)
							isLiteralString=TRUE
							BOOL bShowPlayerName = TRUE
							
							IF IS_THIS_ROCKSTAR_MISSION_WVM_APC(g_FMMC_STRUCT.iRootContentIDHash)
							OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSeven, ciFMMC_VEHICLE7_HIDE_PLAYER_NAME_ON_HEALTHBAR)
								PRINTLN("[VehicleHud] DISPLAY_VEHICLE_HUD Setting bShowPlayerName to FALSE")
								bShowPlayerName = FALSE
							ENDIF
							
							IF bShowPlayerName
								PED_INDEX driverPed= GET_PED_IN_VEHICLE_SEAT(ThisVeh)
								IF NOT IS_PED_INJURED(driverPed)
									IF IS_PED_A_PLAYER(driverPed)
										//isPlayerString=TRUE
										PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(driverPed)
										tl63_Temp = GET_PLAYER_NAME(tempplayer)
										tl63_Temp += " - "
									ENDIF
								ENDIF
							ENDIF
							
							tl23_Temp = CONVERT_STRING_TO_UPPERCASE(tl23_Temp)
							tl63_Temp += tl23_Temp
						ELSE
							 tl63_Temp = "VEH_HEALTH"
						ENDIF
						
						IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sVehBlipStruct.iBlipSpriteOverride > 0
						AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sVehBlipStruct.iBlipSpriteOverride < 9
							isLiteralString = FALSE
							isPlayerString = FALSE
							tl63_Temp = "TGT_HEALTH_" 
							tl63_Temp += g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sVehBlipStruct.iBlipSpriteOverride
							//tl63_Temp += GET_LETTER_FROM_CUSTOM_BLIP(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sVehBlipStruct.iBlipSpriteOverride)
							PRINTLN("[VehicleHud] DISPLAY_VEHICLE_HUD Adding letter to the end of the healthbar text because the blip override of veh ", iVeh, " is ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sVehBlipStruct.iBlipSpriteOverride)
						ELSE
							PRINTLN("[VehicleHud] DISPLAY_VEHICLE_HUD Not using blip override to set healthbar text for veh ", iVeh, " because the blip override isn't a letter: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sVehBlipStruct.iBlipSpriteOverride)
						ENDIF
						
						FLOAT fPercentage = GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(ThisVeh, iVeh, MC_serverBD.iTotalNumStartingPlayers,IS_VEHICLE_A_TRAILER(ThisVeh))
												
						HUDORDER hudOrderVeh = HUDORDER_DONTCARE
						
						IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMBUSHKA(g_FMMC_STRUCT.iAdversaryModeType)
							hudOrderVeh = HUDORDER_BOTTOM
						ENDIF
						
						//IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSeven, ciFMMC_VEHICLE7_SPAWN_BURNING)
						IF IS_BIT_SET(MC_serverBD_4.iBurningVehiclePTFXBitset, iVeh) 
							//Special behaviour for burning vehicles that need to be put out
							IF MC_serverBD_4.fFireStrength[iVeh] > 0.0
								INT iFlashTime = -1		
								HUD_COLOURS hcBarColour = HUD_COLOUR_WHITE								
								
								IF SHOULD_VEH_BAR_FLASH(iVeh)
									iFlashTime = SCRIPT_MAX_INT32
									hcBarColour = HUD_COLOUR_BLUELIGHT
								ENDIF
								
								IF fLocalFireDamageDealt[iVeh] <= fLocalFireDamageDealtLastFrame[iVeh]
								AND IS_BIT_SET(MC_playerBD[iLocalPart].iBurningVehFlashBS, iVeh)
									CLEAR_BIT(MC_playerBD[iLocalPart].iBurningVehFlashBS, iVeh)
									PRINTLN("[VehicleHud] Clearing MC_playerBD[iLocalPart].iBurningVehFlashBS now for Veh ", iVeh)
								ENDIF
								
								PRINTLN("[RCV][JR] - DISPLAY_VEHICLE_HUD Veh ", iVeh, ", ", FLOOR(MC_serverBD_4.fBurningVehicleHealth[iVeh]), " / ", ciMAX_BURNING_VEHICLE_HEALTH)
								
								IF MC_serverBD_4.fBurningVehicleHealth[iVeh] > -1
									DRAW_GENERIC_METER(ROUND(MC_serverBD_4.fBurningVehicleHealth[iVeh]), ciMAX_BURNING_VEHICLE_HEALTH, tl63_Temp, hcBarColour, iFlashTime, hudOrderVeh, DEFAULT, DEFAULT, isPlayerString, DEFAULT, HUDFLASHING_FLASHRED, -1, DEFAULT, DEFAULT, isLiteralString, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE, DEFAULT, HUD_COLOUR_RED, DEFAULT)
								ENDIF
							ELSE
								PRINTLN("[TMS][VehicleHud] DISPLAY_VEHICLE_HUD Not drawing healthbar for vehicle ", iVeh, " because the fire on this one has been put out")
							ENDIF
						ELSE
							//Standard vehicle healthbar
							DRAW_GENERIC_METER(FLOOR(fPercentage), 100, tl63_Temp, HUD_COLOUR_WHITE, -1, hudOrderVeh, DEFAULT, DEFAULT, isPlayerString, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, isLiteralString)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING)
			IF HAS_NET_TIMER_STARTED(MC_serverBD_1.tdControlVehTimer[iVeh])
				IF MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam] < FMMC_MAX_RULES	
					IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iShowTeamHackingProgress[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]] > -2
						INT iHackingPart = -1
						
						IF MC_playerBD[iPartToUse].iVehFollowing != iVeh
							INT iPartsChecked
							INT iPlayersToCheck = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]
							
							FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
								IF ((NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_ANY_SPECTATOR)) OR IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
								AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet,PBBOOL_PLAYER_FAIL)
								AND IS_BIT_SET(MC_playerBD[i].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
								AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_FINISHED)
									IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
										iPartsChecked++
										IF i != iPartToUse
										AND DOES_TEAM_LIKE_TEAM(MC_playerBD[i].iTeam, MC_playerBD[iPartToUse].iTeam)
											IF MC_playerBD[i].iVehFollowing = iVeh
												iHackingPart = i
												BREAKLOOP
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								IF iPartsChecked >= iPlayersToCheck
									BREAKLOOP
								ENDIF
							ENDFOR
						ENDIF
						
						IF iHackingPart > -1
							INT iTeamLoop = 0
							FOR iTeamLoop = 0 TO MC_serverBD.iNumberOfTeams - 1
								IF MC_serverBD_4.iVehPriority[iveh][iTeamLoop] <= MC_serverBD_4.iCurrentHighestPriority[ iMyTeam ]
								AND MC_serverBD_4.iVehPriority[iveh][iTeamLoop] < FMMC_MAX_RULES
								AND MC_serverBD_4.ivehRule[iveh][iTeamLoop] = FMMC_OBJECTIVE_LOGIC_CAPTURE
								AND iveh != MC_playerBD[iLocalPart].iVehFollowing
									IF iTeamLoop = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iShowTeamHackingProgress[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]]
									OR g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iShowTeamHackingProgress[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]] = -1	
										PRINTLN("[SECUROHACK] We are iPart: ", iPartToUse, " on Team: ", MC_PlayerBD[iParttoUse].iTeam, " and we want to see the bar for team: ", g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iShowTeamHackingProgress[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]])
										TEXT_LABEL_15 tl15 = "HACK_PROG"
										INT iProgressMax = MC_serverBD.iVehTakeoverTime[iTeamLoop][MC_serverBD_4.iVehPriority[iVeh][iTeamLoop]]
										INT iProgress = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.tdControlVehTimer[iVeh])
										DRAW_GENERIC_METER(iProgress, iProgressMax, tl15, HUD_COLOUR_WHITE, -1, INT_TO_ENUM(HUDORDER, ENUM_TO_INT(HUDORDER_SEVENTHBOTTOM)+iTeamLoop))
										
										//Don't keep looping
										BREAKLOOP
									ENDIF
								ENDIF
							ENDFOR
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		IF MC_serverBD_4.iVehPriority[iveh][ iMyTeam ] <= MC_serverBD_4.iCurrentHighestPriority[ iMyTeam ]
		AND MC_serverBD_4.iVehPriority[iveh][ iMyTeam ] < FMMC_MAX_RULES
		AND MC_serverBD_4.ivehRule[iveh][ iMyTeam ] != FMMC_OBJECTIVE_LOGIC_NONE
			
			IF iAHighPriorityVehicle = -1
				iAHighPriorityVehicle = iveh
			ENDIF
			
			BOOL bSingular
			BOOL isLiteralString
			
			IF MC_serverBD.iNumStartingPlayers[iMyTeam] > 1
				bSingular = FALSE
			ELSE
				bSingular = TRUE
			ENDIF
			
			IF MC_serverBD_4.ivehRule[iveh][ iMyTeam ]= FMMC_OBJECTIVE_LOGIC_CAPTURE
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_SHOW_CAPTURE_FOR_ALL_TEAMS)
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_ALWAYS_SHOW_CAPTURE_BAR)
						IF bPlayerToUseOK
							IF NOT IS_SPECTATOR_HUD_HIDDEN()
							AND NOT IS_SPECTATOR_SHOWING_NEWS_HUD()
							//AND NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_OBJECTIVE_BLOCKER) - B* 2747787
							AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iMyTeam ].iRuleBitset[MC_serverBD_4.iVehPriority[iveh][ iMyTeam ]], ciBS_RULE_HIDE_HUD)
								// Show all teams vehicle capture bars
								FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
									eTeamColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam,PLAYER_ID())
									
									HUDORDER eHudOrder = INT_TO_ENUM(HUDORDER, ENUM_TO_INT(HUDORDER_SIXTHBOTTOM) - iTeam)
									
									IF iTeam = MC_playerBD[iPartToUse].iTeam
										eHudOrder = HUDORDER_SEVENTHBOTTOM
									ENDIF
									
									IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_RETAIN_CAPTURE_PROGRESS)
										
										IF g_FMMC_STRUCT.iTeamNames[iTeam] != 0
											g_sMission_TeamName[iTeam] = GET_CREATOR_NAME_STRING_FOR_TEAM( iTeam , bSingular ) 
											isLiteralString=TRUE
											PRINTLN("[RCC MISSION] [VEHICLE CAPTURE] - g_sMission_TeamName[iTeam] assigned") 
										ELSE
											SCRIPT_ASSERT("[RCC MISSION] - [VEHICLE CAPTURE] on heist or heist planning mission but team does not have creator name. Give an A class bug to *default online content creation*!, team: ")
										ENDIF
										IF HAS_NET_TIMER_STARTED(MC_serverBD_1.tdControlVehTimer[iveh])
											IF MC_serverBD.iDriverPart[iveh] != -1
											AND iTeam = MC_playerBD[MC_serverBD.iDriverPart[iveh]].iTeam
												iStoredVehCaptureMS[iTeam][iVeh] = (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.tdControlVehTimer[iveh]))
											ENDIF
										ENDIF
										IF g_FMMC_STRUCT.iTeamNames[iTeam] != 0
											DRAW_GENERIC_METER(iStoredVehCaptureMS[iTeam][iVeh] ,MC_serverBD.iVehTakeoverTime[ iTeam ][MC_serverBD_4.iVehPriority[iveh][ iTeam ]],g_sMission_TeamName[iTeam],eTeamColour,-1, eHudOrder,-1,-1,FALSE,TRUE,HUDFLASHING_NONE,0,FALSE,0,isLiteralString)
										ELSE 
											DRAW_GENERIC_METER(iStoredVehCaptureMS[iTeam][iVeh] ,MC_serverBD.iVehTakeoverTime[ iTeam ][MC_serverBD_4.iVehPriority[iveh][ iTeam ]],"CON_TIME",eTeamColour,-1, eHudOrder)
										ENDIF
									ELSE
										DRAW_GENERIC_METER(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.tdControlVehTimer[iveh]),MC_serverBD.iVehTakeoverTime[ iTeam ][MC_serverBD_4.iVehPriority[iveh][ iTeam ]],"CON_TIME",eTeamColour,-1, eHudOrder)
									ENDIF
								ENDFOR
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_ALWAYS_SHOW_CAPTURE_BAR)
						IF NOT IS_SPECTATOR_HUD_HIDDEN()
						AND NOT IS_SPECTATOR_SHOWING_NEWS_HUD()
						AND NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_OBJECTIVE_BLOCKER)	
						AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iMyTeam ].iRuleBitset[MC_serverBD_4.iVehPriority[iveh][ iMyTeam ]], ciBS_RULE_HIDE_HUD)
						AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive,  ciFMMC_VEHICLE5_HACKING_DISABLE_CAPTURE_BAR)
							//Just show for your own team
							DRAW_GENERIC_METER(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.tdControlVehTimer[iveh]),MC_serverBD.iVehTakeoverTime[ iMyTeam ][MC_serverBD_4.iVehPriority[iveh][ iMyTeam ]],"CON_TIME",HUD_COLOUR_GREEN,-1)
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING)
					
					IF HAS_NET_TIMER_STARTED(MC_serverBD_1.tdControlVehTimer[iVeh])
						
						INT iHackingPart = -1
						
						IF MC_playerBD[iPartToUse].iVehFollowing = iVeh
							//We are the hacker
							iHackingPart = iPartToUse
							
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciSECUROSERV_DONT_FORCE_ON_SCREEN)
								DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_PHONE)
								DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CELLPHONE_DOWN)
								DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CELLPHONE_CANCEL)
							ENDIF
						ELSE
							IF IS_BIT_SET(iHackingVehInRangeBitSet, iVeh)
								//In range of the veh
								INT iPartsChecked
								INT iPlayersToCheck = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]
								
								FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
									IF ((NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_ANY_SPECTATOR)) OR IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
									AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet,PBBOOL_PLAYER_FAIL)
									AND IS_BIT_SET(MC_playerBD[i].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
									AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_FINISHED)
										IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
											iPartsChecked++
											IF i != iPartToUse
											AND DOES_TEAM_LIKE_TEAM(MC_playerBD[i].iTeam, MC_playerBD[iPartToUse].iTeam)
												IF MC_playerBD[i].iVehFollowing = iVeh
													iHackingPart = i
													PRINTLN("[SECUROHACK] DISPLAY_VEHICLE_HUD Hacker is, ", iHackingPart)
													BREAKLOOP
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									IF iPartsChecked >= iPlayersToCheck
										BREAKLOOP
									ENDIF
								ENDFOR
							ENDIF
						ENDIF
						
						IF iHackingPart != -1
							IF iSpectatorTarget = -1
								IF NOT HAS_NET_TIMER_STARTED(tdTimeHackingTimer)
									PRINTLN("[JS][SECUROHACK] - DISPLAY_VEHICLE_HUD - Starting hacking tracking timer")
									REINIT_NET_TIMER(tdTimeHackingTimer)
								ENDIF
							ENDIF
							
							SET_BIT(iLocalBoolCheck23, LBOOL23_SOMEONE_IS_HACKING_A_VEHICLE)
							
							IF MC_serverBD_4.iVehPriority[iVeh][MC_playerBD[iHackingPart].iTeam] > -1
							AND MC_serverBD_4.iVehPriority[iVeh][MC_playerBD[iHackingPart].iTeam] < FMMC_MAX_RULES
								IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appSecuroHack")) > 0
								AND IS_PHONE_ONSCREEN(TRUE)
									IF NOT IS_BIT_SET(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
										IF iHackingLoopSoundID = -1
											iHackingLoopSoundID = GET_SOUND_ID()
										ENDIF
										
										IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
											PLAY_SOUND_FRONTEND(iHackingLoopSoundID, "Hack_Loop", "dlc_xm_deluxos_hacking_Hacking_Sounds")
										ELSE
											PLAY_SOUND_FRONTEND(iHackingLoopSoundID, "Hack_Loop", "DLC_IE_SVM_Voltic2_Hacking_Sounds")
										ENDIF
										
										SET_BIT(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
									ENDIF
									
									SET_VARIABLE_ON_SOUND(iHackingLoopSoundID, "percentageComplete", TO_FLOAT(iHackPercentage) / 100)
									
									IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED ("FHHUD_DELUXOHACK")
										IF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE ( ciGANGOPS_FLOW_MISSION_DELOREAN )
										AND iHackPercentage < 100
											DISPLAY_HELP_TEXT_THIS_FRAME("FHHUD_DELUXOHACK", TRUE)
										ENDIF
									ENDIF
								ENDIF
								
								INT iTempHackPercentage = ROUND(TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.tdControlVehTimer[iVeh])) / MC_serverBD.iVehTakeoverTime[MC_playerBD[iHackingPart].iTeam][MC_serverBD_4.iVehPriority[iVeh][MC_playerBD[iHackingPart].iTeam]] * 100)
								
								IF iTempHackPercentage >= iHackPercentage
								OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive, ciFMMC_VEHICLE5_HACKING_RESET_CAPTURE_OUT_OF_RANGE)
								OR iPreviousVehHack != iVeh
									iHackPercentage = iTempHackPercentage
								ELSE
									PRINTLN("[SECUROHACK] DISPLAY_VEHICLE_HUD iTempHackPercentage is less that we think it should be ", iTempHackPercentage, " vs ", iHackPercentage, " keeping higher value until we catch up")
								ENDIF
								
								IF (IS_THIS_ROCKSTAR_MISSION_WVM_HALFTRACK(g_FMMC_STRUCT.iRootContentIDHash)
								AND (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH("appSecuroHack")) = 0)
									OR NOT IS_PHONE_ONSCREEN())
								OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciDISPLAY_HACK_PROGRESS_BAR)
									IF MC_playerBD[iLocalPart].iVehFollowing = iVeh
										DRAW_GENERIC_METER(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.tdControlVehTimer[iVeh]), MC_serverBD.iVehTakeoverTime[MC_playerBD[iHackingPart].iTeam][MC_serverBD_4.iVehPriority[iVeh][MC_playerBD[iHackingPart].iTeam]], "HACK_PROG", DEFAULT, DEFAULT, HUDORDER_TOP, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT)
									ENDIF
								ENDIF
																
								IF iHackPercentage < 100
									IF iHackStage != ciSECUROSERV_HACK_STAGE_HACKING
									AND iHackStage != ciSECUROSERV_HACK_STAGE_LOSING_SIGNAL
										IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciDISPLAY_HACK_PROGRESS_BAR)
											SET_BIT(iHackingVehInRangeBitSet, iVeh)	//Fix for bug 3515537 (forcing the player to be in range because you can always see the progress bar)
										ENDIF
										
										IF NOT IS_HACK_START_SOUND_BLOCKED()
											IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
												PLAY_SOUND_FRONTEND(-1, "Hack_Start", "dlc_xm_deluxos_hacking_Hacking_Sounds")
											ELSE
												PLAY_SOUND_FRONTEND(-1, "Hack_Start", "DLC_IE_SVM_Voltic2_Hacking_Sounds")
											ENDIF
										ENDIF
										
										iHackStage = ciSECUROSERV_HACK_STAGE_HACKING
									ENDIF
									
									IF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_SERVUROSERV_LOSING_SIGNAL)
										IF iHackStage = ciSECUROSERV_HACK_STAGE_HACKING
											iHackStage = ciSECUROSERV_HACK_STAGE_LOSING_SIGNAL
											PRINTLN("[TMS][SECUROHACK] DISPLAY_VEHICLE_HUD Losing signal now!")
										ENDIF
									ELSE
										IF iHackStage = ciSECUROSERV_HACK_STAGE_LOSING_SIGNAL
											iHackStage = ciSECUROSERV_HACK_STAGE_HACKING
											PRINTLN("[TMS][SECUROHACK] DISPLAY_VEHICLE_HUD Signal back to normal now!")
										ENDIF
									ENDIF
								ENDIF
								iPreviousVehHack = iVeh
							ENDIF
						ELSE
							IF iSpectatorTarget = -1
								IF HAS_NET_TIMER_STARTED(tdTimeHackingTimer)
									MC_playerBD_1[iLocalPart].iTimeSpentPhoneHacking += GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdTimeHackingTimer)
									PRINTLN("[JS][SECUROHACK] - DISPLAY_VEHICLE_HUD - Stopping hacking tracking timer. Time hacking: ", MC_playerBD_1[iLocalPart].iTimeSpentPhoneHacking)
									RESET_NET_TIMER(tdTimeHackingTimer)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF bPlayerToUseOK
				tempPed = PlayerPedToUse
				INT iMultiRuleNo
				IF NOT IS_PED_INJURED(tempPed)
					IF ((MC_serverBD_4.ivehRule[iveh][iMyTeam] = FMMC_OBJECTIVE_LOGIC_PHOTO) OR (MC_playerBD[iPartToUse].iVehNear = iveh OR MC_playerBD[iPartToUse].iVehNear = -1))
					AND iSpectatorTarget = -1
						IF MC_serverBD_4.ivehRule[iveh][iMyTeam] != FMMC_OBJECTIVE_LOGIC_PHOTO
							IF MC_playerBD[iPartToUse].iVehNear = iveh
								CLEAR_BIT(iVehPhotoChecksBS, iveh)
								RESET_PHOTO_DATA(iveh)
								MC_playerBD[iLocalPart].iVehNear = -1
							ENDIF
						ENDIF
						
						SWITCH MC_serverBD_4.ivehRule[iveh][iMyTeam]
						
							CASE FMMC_OBJECTIVE_LOGIC_PHOTO
							
								IF MC_playerBD[iLocalPart].iVehPhoto = -1
									PROCESS_VEHICLE_PHOTO_LOGIC(iveh,ThisVeh)
								#IF IS_DEBUG_BUILD
								ELSE
									PRINTLN("[RCC MISSION] [AW PHOTO] DISPLAY_VEHICLE_HUD Unable to process photo logic, MC_playerBD[iLocalPart].iVehPhoto = ",MC_playerBD[iLocalPart].iVehPhoto)
								#ENDIF
								ENDIF
							
							BREAK
							
							CASE FMMC_OBJECTIVE_LOGIC_GO_TO
								IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iGotoRange != 0
									iMultiRuleNo = GET_ENTITY_MULTIRULE_NUMBER(iveh,MC_playerBD[iLocalPart].iteam,ci_TARGET_VEHICLE)
									
									IF (iMultiRuleNo != -1)
									AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetTwo, ciFMMC_VEHICLE2_IgnoreProximityPrimaryRule + iMultiRuleNo)
										IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iMyTeam].iRuleBitsetFive[MC_ServerBD_4.iCurrentHighestPriority[iMyTeam]], ciBS_RULE5_IGNORE_GOTO_RADIUS)
											PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - ciBS_RULE5_IGNORE_GOTO_RADIUS is set - 3")
											IF IS_VEHICLE_DRIVEABLE(ThisVeh)
												IF IS_PED_IN_VEHICLE(LocalPlayerPed,ThisVeh)
													PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - ciBS_RULE5_IGNORE_GOTO_RADIUS - IN VEHICLE")
													MC_playerBD[iLocalPart].iVehNear = iveh
												ENDIF
											ENDIF
										ELSE
											IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed,ThisVeh) <= g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iGotoRange
												MC_playerBD[iLocalPart].iVehNear = iveh
												
												#IF IS_DEBUG_BUILD
													IF bInVehicleDebug
														PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH (2) Setting MC_playerBD[iPartToUse].iVehNear =  ", MC_playerBD[iPartToUse].iVehNear, " for part ",iPartToUse, " who is player ",  GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartToUse))))
													ENDIF
												#ENDIF
											ENDIF
										ENDIF
									ELSE
										#IF IS_DEBUG_BUILD
											IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iMyTeam].iRuleBitsetFive[MC_ServerBD_4.iCurrentHighestPriority[iMyTeam]], ciBS_RULE5_IGNORE_GOTO_RADIUS)
												PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - ciBS_RULE5_IGNORE_GOTO_RADIUS is set - 2")
											ENDIF
										#ENDIF
									ENDIF
								ENDIF
							
							BREAK
						
						ENDSWITCH

					ENDIF
					
					IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, IS_THIS_ROCKSTAR_MISSION_SVM_BOXVILLE5(g_Fmmc_Struct.iRootContentIDHash))
						#IF IS_DEBUG_BUILD
							IF bInVehicleDebug
								PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH local player is in a vehicle... ")
							ENDIF
						#ENDIF
						tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed, IS_THIS_ROCKSTAR_MISSION_SVM_BOXVILLE5(g_Fmmc_Struct.iRootContentIDHash))
						GET_VEHICLE_TRAILER_VEHICLE(tempVeh,trailerVeh)
						IF GET_ENTITY_MODEL(tempVeh) = CARGOBOB
						OR GET_ENTITY_MODEL(tempVeh) = CARGOBOB2
						OR GET_ENTITY_MODEL(tempVeh) = CARGOBOB3
							Winchent = GET_VEHICLE_ATTACHED_TO_CARGOBOB(tempVeh)
							IF DOES_ENTITY_EXIST(Winchent)
								IF IS_ENTITY_A_VEHICLE(Winchent)
									Winchveh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(Winchent)
								ENDIF
							ENDIF
						ELSE
							Winchent = GET_ENTITY_ATTACHED_TO(tempVeh)
							IF DOES_ENTITY_EXIST(Winchent)
								IF IS_ENTITY_A_VEHICLE(Winchent)
									Winchveh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(Winchent)
								ENDIF
							ENDIF
						ENDIF
						
						IF tempVeh = ThisVeh
						OR trailerVeh = ThisVeh
						OR WinchVeh = ThisVeh
							#IF IS_DEBUG_BUILD
								IF bInVehicleDebug
									PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH vehicle matches ")
								ENDIF
							#ENDIF
							
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iMyTeam ].iWantedAtMidBitset,MC_serverBD_4.iVehPriority[iveh][ iMyTeam ])
								IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[ iMyTeam ],MC_serverBD_4.iVehPriority[iveh][ iMyTeam ])
									IF NOT IS_BIT_SET(iWantedVehBitset,iveh)
										iLocalWantedHighestPriority = -1
										SET_BIT(iWantedVehBitset,iveh)
									ENDIF
								ENDIF
							ENDIF
							
							IF MC_serverBD_4.ivehRule[iveh][ iMyTeam ] = FMMC_OBJECTIVE_LOGIC_GO_TO
								#IF IS_DEBUG_BUILD
									IF bInVehicleDebug
										PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH I'm on a goto rule ")
									ENDIF
								#ENDIF
								
								IF iSpectatorTarget = -1
								OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_HEIST_SPECTATOR)
									#IF IS_DEBUG_BUILD
										IF bInVehicleDebug
											PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH Not a spectator ")
										ENDIF
									#ENDIF
								
									IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iMyTeam].iRuleBitsetThree[MC_serverBD_4.iVehPriority[iveh][iMyTeam]], ciBS_RULE3_ALL_IN_SEPARATE_VEHICLES)
										IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iMyTeam].iRuleBitsetFive[MC_ServerBD_4.iCurrentHighestPriority[iMyTeam]], ciBS_RULE5_IGNORE_GOTO_RADIUS)
											MC_playerBD[iLocalPart].iVehNear = iveh
										ELSE
											PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - ciBS_RULE5_IGNORE_GOTO_RADIUS is set - 1")
										ENDIF
										
										#IF IS_DEBUG_BUILD
											IF bInVehicleDebug
												PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH (3) Setting MC_playerBD[iPartToUse].iVehNear =  ", MC_playerBD[iPartToUse].iVehNear, " for part ",iPartToUse, " who is player ",  GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartToUse))))
											ENDIF
										#ENDIF
									ELSE
										#IF IS_DEBUG_BUILD
											IF bInVehicleDebug
												PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH ciBS_RULE3_ALL_IN_SEPARATE_VEHICLES is set! ")
											ENDIF
										#ENDIF
									
										BOOL bCheckOnRule // To get it compiling
										INT iTeamsBS = GET_VEHICLE_GOTO_TEAMS(iMyTeam, iveh, bCheckOnRule)
										
										INT iCombinedBS = 0
										
										INT iTeamLoop
										FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
											IF IS_BIT_SET(iTeamsBS, iTeamLoop)
												iCombinedBS |= iVehHeld_NotMe_BS[iTeamLoop] 
											ENDIF
										ENDFOR
										
										IF NOT IS_BIT_SET(iCombinedBS, iveh)
										AND IS_PED_SITTING_IN_VEHICLE_SEAT(LocalPlayerPed, ThisVeh, VS_DRIVER)
											MC_playerBD[iLocalPart].iVehNear = iveh
										ENDIF
									ENDIF
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
									IF bInVehicleDebug
										PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH I'm not on a goto rule! MC_serverBD_4.ivehRule[iveh][ iMyTeam ] = ", MC_serverBD_4.ivehRule[iveh][ iMyTeam ])
									ENDIF
								#ENDIF
										
								IF MC_serverBD_4.ivehRule[iveh][ iMyTeam ]= FMMC_OBJECTIVE_LOGIC_CAPTURE
									IF HAS_NET_TIMER_STARTED(MC_serverBD_1.tdControlVehTimer[iveh])
										IF bPlayerToUseOK
											IF NOT IS_SPECTATOR_HUD_HIDDEN()
											AND NOT IS_SPECTATOR_SHOWING_NEWS_HUD()
											AND NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_OBJECTIVE_BLOCKER)	
											AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iMyTeam ].iRuleBitset[MC_serverBD_4.iVehPriority[iveh][ iMyTeam ]], ciBS_RULE_HIDE_HUD)
												// Don't draw this one if 'always show is on as we draw this elsewhere all the time.
												IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_ALWAYS_SHOW_CAPTURE_BAR)
													DRAW_GENERIC_METER(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.tdControlVehTimer[iveh]),MC_serverBD.iVehTakeoverTime[ iMyTeam ][MC_serverBD_4.iVehPriority[iveh][ iMyTeam ]],"CON_TIME",HUD_COLOUR_GREEN,-1)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								IF MC_serverBD_4.ivehRule[iveh][ iMyTeam ] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
								OR MC_serverBD_4.ivehRule[iveh][ iMyTeam ] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD
								OR MC_serverBD_4.ivehRule[iveh][ iMyTeam ] = FMMC_OBJECTIVE_LOGIC_CAPTURE
								OR MC_serverBD_4.ivehRule[iveh][ iMyTeam ] = FMMC_OBJECTIVE_LOGIC_PROTECT
									IF iSpectatorTarget = -1
									OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_HEIST_SPECTATOR)
										IF MC_serverBD_4.ivehRule[iveh][MC_playerBD[iLocalPart].iteam] != FMMC_OBJECTIVE_LOGIC_PROTECT
											IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iMyTeam].iRuleBitsetThree[MC_serverBD_4.iVehPriority[iveh][iMyTeam]], ciBS_RULE3_ALL_IN_SEPARATE_VEHICLES)
												MC_playerBD[iLocalPart].iVehNear = iveh
												
												#IF IS_DEBUG_BUILD
													IF bInVehicleDebug
														PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH (4) Setting MC_playerBD[iPartToUse].iVehNear =  ", MC_playerBD[iPartToUse].iVehNear, " for part ",iPartToUse, " who is player ",  GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartToUse))))
													ENDIF
												#ENDIF
												
											ELSE
												BOOL bCheckOnRule // To get it compiling
												INT iTeamsBS = GET_VEHICLE_GOTO_TEAMS(iMyTeam, iveh, bCheckOnRule)
												
												INT iCombinedBS = 0
												
												INT iTeamLoop
												FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
													IF IS_BIT_SET(iTeamsBS, iTeamLoop)
														iCombinedBS |= iVehHeld_NotMe_BS[iTeamLoop]
													ENDIF
												ENDFOR
												
												IF NOT IS_BIT_SET(iCombinedBS, iveh)
												AND IS_PED_SITTING_IN_VEHICLE_SEAT(LocalPlayerPed, ThisVeh, VS_DRIVER)
													MC_playerBD[iLocalPart].iVehNear = iveh
													
													#IF IS_DEBUG_BUILD
														IF bInVehicleDebug
															PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH (5) Setting MC_playerBD[iPartToUse].iVehNear =  ", MC_playerBD[iPartToUse].iVehNear, " for part ",iPartToUse, " who is player ",  GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartToUse))))
														ENDIF
													#ENDIF
												ENDIF
											ENDIF
										ENDIF
										
										IF MC_serverBD_4.ivehRule[iveh][MC_playerBD[iLocalPart].iteam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
											IF (NOT AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH())
												IF GET_PED_IN_VEHICLE_SEAT(tempVeh) = LocalPlayerPed
													IF NOT IS_BIT_SET(ivehcolBoolCheck,iveh)
														BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_COLLECT_VEH,0,MC_playerBD[iLocalPart].iteam,-1,NULL,ci_TARGET_VEHICLE,iveh)
														
														// Award, In order to win at Capture, someone has to get the goods. Pick up a package or vehicle in any Capture mode.
														IF MC_playerBD[iLocalPart].iClientLogicStage <> CLIENT_MISSION_STAGE_DELIVER_OBJ
															IF Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer)
																IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
																	PRINTLN("[RCC MISSION] AWARD, MP_AWARD_PICKUP_CAP_PACKAGES - VEH ")
																	INCREMENT_MP_INT_CHARACTER_AWARD(MP_AWARD_PICKUP_CAP_PACKAGES)
																ELSE
																	PRINTLN("[RCC MISSION] AWARD, MP_AWARD_PICKUP_CAP_PACKAGES NOT INCREMENTED because in fake MP mode ")
																ENDIF
															ENDIF
														ENDIF
														SET_BIT(ivehcolBoolCheck,iveh)
													ENDIF
												ENDIF
												imyVehicleCarryCount++
												PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - imyVehicleCarryCount = ", imyVehicleCarryCount)
											ENDIF
										ELSE
											IF GET_PED_IN_VEHICLE_SEAT(tempVeh) = LocalPlayerPed
												IF NOT IS_BIT_SET(ivehcolBoolCheck,iveh)
													BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_COLLECT_VEH,0,MC_playerBD[iLocalPart].iteam,-1,NULL,ci_TARGET_VEHICLE,iveh)
													// Award, In order to win at Capture, someone has to get the goods. Pick up a package or vehicle in any Capture mode.
													IF MC_playerBD[iLocalPart].iClientLogicStage <> CLIENT_MISSION_STAGE_DELIVER_OBJ
														IF Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer)
															IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
																PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD AWARD, MP_AWARD_PICKUP_CAP_PACKAGES - VEH ")
																INCREMENT_MP_INT_CHARACTER_AWARD(MP_AWARD_PICKUP_CAP_PACKAGES)
															ELSE
																PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD AWARD, MP_AWARD_PICKUP_CAP_PACKAGES NOT INCREMENTED because in fake MP mode ")
															ENDIF
														ENDIF
													ENDIF
													
													SET_BIT(ivehcolBoolCheck,iveh)
												ENDIF
											ENDIF
											imyVehicleCarryCount++
											PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - imyVehicleCarryCount = ", imyVehicleCarryCount)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							IF bInVehicleDebug
								PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH vehicle doesn't match ")
							ENDIF
						#ENDIF
						
						IF MC_playerBD[iPartToUse].iVehCarryCount < 1
							IF IS_BIT_SET(ivehcolBoolCheck,iveh)
								CLEAR_BIT(ivehcolBoolCheck,iveh)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF // If the current rule is valid

	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF iFirstHighPriorityVehicleThisRule[iTeam] = -1
			IF MC_serverBD_4.iVehPriority[iveh][iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam]
				iFirstHighPriorityVehicleThisRule[iTeam] = iveh
			ENDIF
		ENDIF
		
		IF bHostOfScript
			IF MC_serverBD_4.iVehPriority[iveh][iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam]
				MC_serverBD.iNumVehSeatsHighestPriority[iTeam] += GET_VEHICLE_MODEL_NUMBER_OF_SEATS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn)
			ENDIF
		ENDIF
	ENDFOR
	
	
ENDPROC

//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//***********************CHOOFUCKINGCHOO**********************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************

FUNC INT GET_CARRIAGE_CONFIG()
	RETURN 0
ENDFUNC

FUNC BOOL CHECK_FOR_BOARDING_OF_TRAIN(BOOL bCheckAllPlayers)
	IF bCheckAllPlayers
		INT iPartLoop

		FOR iPartLoop = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
			IF iPartLoop != iPartToUse
				IF (MC_playerBD[iPartLoop].iTeam != -1)
				AND (MC_playerBD[iPartLoop].iTeam = MC_playerBD[iPartToUse].iTeam)
				AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartLoop))
					PLAYER_INDEX player = NETWORK_GET_PLAYER_INDEX( INT_TO_PARTICIPANTINDEX(iPartLoop) )
					IF( IS_NET_PLAYER_OK( player ) )
						PED_INDEX ped = GET_PLAYER_PED( player )
						IF NOT IS_PED_IN_ANY_TRAIN(ped)
							PRINTLN("[MMacK][Train] Not all team players are on train")
							RETURN FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ELSE
		IF NOT IS_PLAYER_RIDING_TRAIN(LocalPlayer)
			PRINTLN("[MMacK][Train] Player is on the train")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC HANDLE_TRAIN_SPEED(INT iRule)
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCTrainStruct[iRule].iTrainBS, ciTRAINBS_FORCE_TRAIN_TO_SLOW_DOWN)
	//or some flag set when hacking game is complete?
		PRINTLN("[MMacK][Train] SET_TRAIN_FORCED_TO_SLOW_DOWN")
		SET_TRAIN_FORCED_TO_SLOW_DOWN(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niTrain), TRUE)
	ELSE
		SET_TRAIN_CRUISE_SPEED(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niTrain), g_FMMC_STRUCT.sFMMCTrainStruct[iRule].fSpeed)
		PRINTLN("[MMacK][Train] SET_TRAIN_CRUISE_SPEED - ", g_FMMC_STRUCT.sFMMCTrainStruct[iRule].fSpeed)
	ENDIF
ENDPROC

PROC HANDLE_TRAIN_DEBUG()
	#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_PRESSED(KEY_8)
		ELIF IS_KEYBOARD_KEY_PRESSED(KEY_9)	
		ENDIF
	#ENDIF
ENDPROC

PROC HANDLE_TRAIN_SPAWNING()
	//TODO CREATE A TRAIN
	PRINTLN("[MMacK][Train] HANDLE_TRAIN_SPAWNING - COMPLETED")
	g_FMMC_STRUCT.bHasTrainSpawned = TRUE
ENDPROC

FUNC BOOL HAS_TRAIN_MODELS_LOADED()
	BOOL bHasLoadedModels = TRUE
	
	REQUEST_MODEL(FREIGHT)
	REQUEST_MODEL(FREIGHTGRAIN)
	REQUEST_MODEL(FREIGHTCONT1)
	REQUEST_MODEL(FREIGHTCONT2)
	REQUEST_MODEL(TANKERCAR)
	
	IF NOT HAS_MODEL_LOADED(FREIGHT)
	AND NOT HAS_MODEL_LOADED(FREIGHTGRAIN)
	AND NOT HAS_MODEL_LOADED(FREIGHTCONT1)
	AND NOT HAS_MODEL_LOADED(FREIGHTCONT2)
	AND NOT HAS_MODEL_LOADED(TANKERCAR)
		bHasLoadedModels = FALSE
		PRINTLN("[MMacK][Train] HAS_TRAIN_MODELS_LOADED = FALSE")
	ENDIF
	
	RETURN bHasLoadedModels
ENDFUNC

FUNC BOOL SHOULD_WE_SPAWN_A_TRAIN(INT iRule)
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCTrainStruct[iRule].iTrainBS, ciTRAINBS_SPAWN_TRAIN)
		SET_RANDOM_TRAINS(FALSE) //turn off random trains spawning to not cause issues
		PRINTLN("[MMacK][Train] SHOULD_WE_SPAWN_A_TRAIN - YES")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_SERVER_HAVE_TRAIN_CONTROL()
	BOOL bSpawnOK = FALSE
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niTrain)
	AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niTrain)
		bSpawnOK = TRUE
	ELSE
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niTrain)
			NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niTrain)
		ELSE
			IF HAS_TRAIN_MODELS_LOADED()
				HANDLE_TRAIN_SPAWNING() //have to make the train, as it doesn't exist
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bSpawnOK
ENDFUNC

PROC HANDLE_ATTACHED_MINIGAMES_TO_TRAIN(INT iTeam, INT iRule)
	IF iTeam = iRule + 300
		PRINTLN("IGNORE")
	ENDIF
ENDPROC

PROC HANDLE_ATTACHED_ENTITIES_TO_TRAIN(INT iTeam, INT iRule)
	IF iTeam = iRule + 300
		PRINTLN("IGNORE")
	ENDIF
ENDPROC

PROC HANDLE_BOARDING_OF_TRAIN(INT iRule)
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCTrainStruct[iRule].iTrainBS, ciTRAINBS_CHECK_FOR_BOARDING)
		IF CHECK_FOR_BOARDING_OF_TRAIN(IS_BIT_SET(g_FMMC_STRUCT.sFMMCTrainStruct[iRule].iTrainBS, ciTRAINBS_ALL_PLAYERS_REQUIRED_TO_BOARD))
			//TODO objective logic when on train, pass a rule?
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_TRAIN()
	IF bIsLocalPlayerHost
		INT iTeam = MC_playerBD[iLocalPart].iteam
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		
		IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
			IF g_FMMC_STRUCT.bHasTrainSpawned
			OR SHOULD_WE_SPAWN_A_TRAIN(iRule)
				PRINTLN("[MMacK][Train] PROCESS_TRAIN - is host")
				IF DOES_SERVER_HAVE_TRAIN_CONTROL()					
					HANDLE_ATTACHED_MINIGAMES_TO_TRAIN(iTeam, iRule)
					
					HANDLE_ATTACHED_ENTITIES_TO_TRAIN(iTeam, iRule)
					
					HANDLE_BOARDING_OF_TRAIN(iRule)

					HANDLE_TRAIN_DEBUG()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_TRAIN()
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF bIsLocalPlayerHost
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niTrain)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niTrain)
					PRINTLN("[MMacK][Train] CLEANUP_TRAIN")
					
					CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niTrain)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	g_FMMC_STRUCT.bHasTrainSpawned = FALSE
ENDPROC

PROC CLEANUP_SVM_BJXL()
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF bIsLocalPlayerHost
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_4.niBJXL)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_4.niBJXL)
					PRINTLN("[JS][BJXL] CLEANUP_SVM_BJXL")
					CLEANUP_NET_ID(MC_serverBD_4.niBJXL)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//Anything that needs done with the BJXL after spawning it
PROC PROCESS_SVM_BJXL()
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_4.niBJXL)
		INT iTeam = MC_playerBD[iLocalPart].iteam
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		
		VEHICLE_INDEX viBJXL = NET_TO_VEH(MC_serverBD_4.niBJXL)
		
		IF IS_ENTITY_ALIVE(viBJXL)
		AND IS_VEHICLE_DRIVEABLE(viBJXL)
		AND iRule < g_FMMC_STRUCT.iSVMRemoveCorpVehicleBlipRule
			IF NOT DOES_BLIP_EXIST(biBJXL)
				IF NOT IS_PED_IN_VEHICLE(LocalPlayerPed, viBJXL)
					biBJXL = CREATE_BLIP_FOR_VEHICLE(viBJXL)
					SET_BLIP_COLOUR(biBJXL, GET_INT_FROM_HUD_COLOUR(GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_playerBD[iPartToUse].iteam, PlayerToUse)))
					SET_BLIP_NAME_FROM_TEXT_FILE(biBJXL, "BJXL")
				ENDIF
			ELSE
				IF IS_PED_IN_VEHICLE(LocalPlayerPed, viBJXL)
					PRINTLN("[JS][BJXL] - PROCESS_SVM_BJXL - In the BJXL, cleaning up blip")
					REMOVE_BLIP(biBJXL)
				ENDIF
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(biBJXL)
				PRINTLN("[JS][BJXL] - PROCESS_SVM_BJXL - BJXL is dead, cleaning up blip")
				REMOVE_BLIP(biBJXL)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_VEHICLE_LOCK_SHOCK(INT iVeh)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive, ciFMMC_VEHICLE5_SHOCKED_IF_LOCKED)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
			VEHICLE_INDEX ThisVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
			SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(thisVeh, FALSE)
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(thisVeh, TRUE)
			IF NOT IS_BIT_SET(MC_serverBD_4.iVehUnlockOnCapture, iVeh)
				IF IS_PED_TRYING_TO_ENTER_A_LOCKED_VEHICLE(LocalPlayerPed)
					IF IS_NET_PLAYER_OK(LocalPlayer, TRUE)
					AND IS_PED_GETTING_INTO_A_VEHICLE(LocalPlayerPed)
						VEHICLE_INDEX vehEntering = GET_VEHICLE_PED_IS_USING(LocalPlayerPed)
						IF ThisVeh = vehEntering
							IF IS_VEHICLE_DRIVEABLE(ThisVeh)
								IF iOpenAnimStarted = -1
									iOpenAnimStarted = GET_GAME_TIMER()
								ELSE
									IF HAS_ANIM_EVENT_FIRED(LocalPlayerPed, HASH("HandContactWithDoor"))
									OR (GET_GAME_TIMER() - iOpenAnimStarted) > waittimezap
										PRINTLN("PROCESS_VEHICLE_LOCK_SHOCK - Zapping player for attempting to enter vehicle with a shock lock turned on.")
										VECTOR vOffset
										SHOOT_SINGLE_BULLET_BETWEEN_COORDS(GET_PED_BONE_COORDS(LocalPlayerPed, BONETAG_PELVIS, vOffset), GET_PED_BONE_COORDS(LocalPlayerPed, BONETAG_NECK, vOffset), 1, TRUE, WEAPONTYPE_STUNGUN, NULL, TRUE, TRUE, -1)
										iOpenAnimStarted = -1
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_CAR_ALARM(INT iVeh)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetEight, ciFMMC_VEHICLE8_ALARMED)
		EXIT
	ENDIF

	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
		VEHICLE_INDEX vehId = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
		
		IF NOT IS_VEHICLE_ALARM_ACTIVATED(vehId)
			IF GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID()) = vehId
			AND IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), vehId)
				SET_VEHICLE_ALARM(vehId, TRUE)
				START_VEHICLE_ALARM(vehId)
				PRINTLN("[ML][MAINTAIN_CAR_ALARM] - MAINTAIN_CAR_ALARM - Triggering vehicle car alarm for attempting to enter locked vehicle #", iVeh)
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

/// PURPOSE:
///    Applies a wanted level to the local player on stealing this specific vehicle. Value set on placing the vehicle in the creator.
PROC STEALING_SETS_LOCAL_PLAYER_WANTED(INT iVeh)
	
	INT iWantedLevel = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iWantedLevelOnTheft
	
	IF iWantedLevel = 0
		EXIT
	ENDIF
	
	IF GET_PLAYER_WANTED_LEVEL(LocalPlayer) < iWantedLevel	// [ML] Less than to avoid the player gaming this by stealing a car to reduce their wanted level
	AND NOT IS_BIT_SET(MC_serverBD_1.iVehicleTheftWantedTriggered, iVeh)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
			VEHICLE_INDEX vehId = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])

			IF GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID()) = vehId
			AND IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), vehId)	
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehicleSpecificTheftWantedDelay = 0
					PRINTLN("[ML][STEALING_SETS_LOCAL_PLAYER_WANTED] - Current Player wanted level = ", GET_PLAYER_WANTED_LEVEL(LocalPlayer))
					SET_PLAYER_WANTED_LEVEL(LocalPlayer, iWantedLevel)
					SET_PLAYER_WANTED_LEVEL_NOW(LocalPlayer)	
					BROADCAST_NOTIFY_WANTED_SET_FOR_VEHICLE(iVeh)		
					PRINTLN("[ML][STEALING_SETS_LOCAL_PLAYER_WANTED] - Set Player wanted level = ", iWantedLevel)
				ELSE
					IF NOT HAS_NET_TIMER_STARTED(tdVehicleWantedDelayTimer)
						REINIT_NET_TIMER(tdVehicleWantedDelayTimer)
					ENDIF
				ENDIF
			ENDIF
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehicleSpecificTheftWantedDelay > 0
				IF HAS_NET_TIMER_STARTED(tdVehicleWantedDelayTimer)
					IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdVehicleWantedDelayTimer) < (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehicleSpecificTheftWantedDelay * 1000)
						EXIT					
					ELSE
						PRINTLN("[ML][STEALING_SETS_LOCAL_PLAYER_WANTED] - Current Player wanted level = ", GET_PLAYER_WANTED_LEVEL(LocalPlayer))
						SET_PLAYER_WANTED_LEVEL(LocalPlayer, iWantedLevel)
						SET_PLAYER_WANTED_LEVEL_NOW(LocalPlayer)	
						BROADCAST_NOTIFY_WANTED_SET_FOR_VEHICLE(iVeh)		
						PRINTLN("[ML][STEALING_SETS_LOCAL_PLAYER_WANTED] - Set Player wanted level = ", iWantedLevel)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC GET_CARGOPLANE_ENGINE_KNOCKBACK(VEHICLE_INDEX veh, VEHICLE_INDEX playerVeh)
	
	VECTOR vMin, vMax
	GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(veh), vMin, vMax)
	VECTOR playerPos = GET_ENTITY_COORDS(playerVeh)
	VECTOR localOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(veh, playerPos)
	FLOAT height = ABSF(localOffset.z)
	IF height < 8.0
	AND localOffset.x > vMin.x AND localOffset.x < vMax.x
	AND localOffset.y > vMin.y AND localOffset.y < vMax.y
		VECTOR direction = GET_ENTITY_FORWARD_VECTOR(veh)
		APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(playerVeh, APPLY_TYPE_FORCE, direction* -10.0, 0, FALSE, TRUE)
	ENDIF
ENDPROC

PROC PROCESS_VEHICLE_ENGINE_KNOCKBACK(INT iVeh)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSeven, ciFMMC_VEHICLE7_ENGINE_KNOCKBACK)
	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
	AND IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		VEHICLE_INDEX veh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
		//IF GET_IS_VEHICLE_ENGINE_RUNNING(veh)
			MODEL_NAMES vehModel = GET_ENTITY_MODEL(veh)
			VEHICLE_INDEX playerVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
			IF vehModel = CARGOPLANE
				IF GET_DIST2_BETWEEN_ENTITIES(veh, playerVeh) < 75.0*75.0
					GET_CARGOPLANE_ENGINE_KNOCKBACK(veh, playerVeh)
				ENDIF
			ENDIF
		//ENDIF
	ENDIF
ENDPROC

PROC PROCESS_LOCK_PEGASUS_VEHICLE()
	IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iPegasusLockRule > -1
	AND g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iPegasusLockRule <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].netID_PegV)
			VEHICLE_INDEX viPegasus = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].netID_PegV)
			IF DOES_ENTITY_EXIST(viPegasus)
				IF IS_VEHICLE_EMPTY(viPegasus, TRUE, TRUE)
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].netID_PegV)
						SET_VEHICLE_DOORS_SHUT(viPegasus, TRUE)
						SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(viPegasus, TRUE)
						SET_VEHICLE_UNDRIVEABLE(viPegasus, TRUE)
					ELSE
						NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].netID_PegV)
					ENDIF
				ENDIF
			ENDIF
		ENDIF 
	ENDIF
ENDPROC

PROC PROCESS_RESPAWN_DELUXO_VEHICLE()
	INT iPlayerTeam = MC_playerBD[ iLocalPart ].iteam
	
	IF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_CTF
	OR	g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_LTS
		PRINTLN("[PROCESS_RESPAWN_DELUXO_VEHICLE]  FMMC_MISSION_TYPE_CTF OR FMMC_MISSION_TYPE_LTS EXIT")
		EXIT
	ENDIF
	IF NOT IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iPlayerTeam ].iTeamBitset, ciBS_TEAM_USES_CORONA_VEHICLE_LIST )
		PRINTLN("[PROCESS_RESPAWN_DELUXO_VEHICLE] NOT ciBS_TEAM_USES_CORONA_VEHICLE_LIST EXIT")
		EXIT
	ENDIF
	
	VEHICLE_INDEX vehPlayer
	IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		PRINTLN("[PROCESS_RESPAWN_DELUXO_VEHICLE] NOT IS_PED_IN_ANY_VEHICLE EXIT")
		EXIT
	ENDIF
	
	vehPlayer = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
	
	IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(vehPlayer)
		PRINTLN("[PROCESS_RESPAWN_DELUXO_VEHICLE] NOT NETWORK_HAS_CONTROL_OF_ENTITY EXIT")
		EXIT
	ENDIF
	
	IF GET_ENTITY_MODEL(vehPlayer) != DELUXO
		PRINTLN("[PROCESS_RESPAWN_DELUXO_VEHICLE] GET_ENTITY_MODEL NOT DELUXO EXIT")
		EXIT
	ENDIF
	
	INT iTeamVeh = MC_PlayerBD[iLocalPart].iteam
	INT iRuleVeh = MC_ServerBD_4.iCurrentHighestPriority[iTeamVeh]	
	
	BOOL bDriveMode = NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeamVeh].iRuleBitsetEleven[iRuleVeh], ciBS_RULE11_DISABLE_DELUXO_DRIVE)
	BOOL bHoverMode = NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeamVeh].iRuleBitsetTen[iRuleVeh], ciBS_RULE10_DISABLE_DELUXO_HOVER_MODE) AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeamVeh].iTeamBitSet3, ciBS3_TEAM_VEH_DELUXO_HOVER_MODE)
	BOOL bFlightMode = NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeamVeh].iRuleBitsetTen[iRuleVeh], ciBS_RULE10_DISABLE_DELUXO_FLIGHT_MODE)
	
	PRINTLN("[PROCESS_RESPAWN_DELUXO_VEHICLE] bFlightMode: ", bFlightMode," bHoverMode: ",bHoverMode, " bDriveMode: ",bDriveMode)
	
	IF bFlightMode
		SET_DISABLE_HOVER_MODE_FLIGHT(vehPlayer, FALSE)
	ELSE
		SET_DISABLE_HOVER_MODE_FLIGHT(vehPlayer, TRUE)
	ENDIF

	IF bHoverMode
		SET_SPECIAL_FLIGHT_MODE_ALLOWED(vehPlayer, TRUE)
	ELSE
		SET_SPECIAL_FLIGHT_MODE_ALLOWED(vehPlayer, FALSE)
	ENDIF
	
	IF bDriveMode
	ELSE
		SET_SPECIAL_FLIGHT_MODE_TARGET_RATIO(vehPlayer, 1)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_TRANSFORM)
	ENDIF
ENDPROC

//Catch Up (Slows Down Top 3 Racers)
PROC PROCESS_SERVER_DRAG_AMOUNTS()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_VEHICLE_CATCH_UP)
		IF bIsLocalPlayerHost
			SWITCH GET_NUMBER_OF_PLAYING_PLAYERS()
				CASE 2
					IF MC_serverBD_3.eDragState <> DRAG_STATE_TWO
						MC_serverBD_3.f1stDragAmount = LOW_DRAG
						MC_serverBD_3.f2ndDragAmount = ZERO_DRAG
						MC_serverBD_3.f3rdDragAmount = ZERO_DRAG
						PRINTLN("[PROCESS_SERVER_DRAG_AMOUNTS] RACER_DRAG_STATE_TWO")
						MC_serverBD_3.eDragState = DRAG_STATE_TWO
					ENDIF
				BREAK
				CASE 3
					IF MC_serverBD_3.eDragState <> DRAG_STATE_THREE
						MC_serverBD_3.f1stDragAmount = MED_DRAG
						MC_serverBD_3.f2ndDragAmount = LOW_DRAG
						MC_serverBD_3.f3rdDragAmount = ZERO_DRAG
						PRINTLN("[PROCESS_SERVER_DRAG_AMOUNTS] RACER_DRAG_STATE_THREE")
						MC_serverBD_3.eDragState = DRAG_STATE_THREE
					ENDIF
				BREAK
				DEFAULT
					IF MC_serverBD_3.eDragState <> DRAG_STATE_DEFAULT
						MC_serverBD_3.f1stDragAmount = HIGH_DRAG
						MC_serverBD_3.f2ndDragAmount = MED_DRAG
						MC_serverBD_3.f3rdDragAmount = LOW_DRAG
						PRINTLN("[PROCESS_SERVER_DRAG_AMOUNTS] RACER_DRAG_STATE_DEFAULT")
						MC_serverBD_3.eDragState = DRAG_STATE_DEFAULT
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

//Catch Up (Slows Down Top 3 Racers)
PROC PROCESS_AIR_DRAG_CATCH_UP()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_VEHICLE_CATCH_UP)
		INT iCurrentPosition = GET_RACE_FINISH_POSITION(MC_playerBD[iLocalPart].iTeam)
		
		IF iDragPosition != iCurrentPosition
			SWITCH iCurrentPosition
				CASE FIRST_PLACE_DRAG
					SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(LocalPlayer, FMAX(MC_serverBD_3.f1stDragAmount, 1.0))
					PRINTLN("[MAINTAIN_AIR_DRAG_CATCH_UP] FIRST_PLACE_DRAG", FMAX(MC_serverBD_3.f1stDragAmount, 1.0))
				BREAK
				CASE SECOND_PLACE_DRAG
					SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(LocalPlayer, FMAX(MC_serverBD_3.f2ndDragAmount, 1.0))
					PRINTLN("[MAINTAIN_AIR_DRAG_CATCH_UP] SECOND_PLACE_DRAG", FMAX(MC_serverBD_3.f2ndDragAmount, 1.0))
				BREAK
				CASE THIRD_PLACE_DRAG
					SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(LocalPlayer, FMAX(MC_serverBD_3.f3rdDragAmount, 1.0))
					PRINTLN("[MAINTAIN_AIR_DRAG_CATCH_UP] THIRD_PLACE_DRAG", FMAX(MC_serverBD_3.f3rdDragAmount, 1.0))
				BREAK
				DEFAULT
					SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(LocalPlayer, DRAG_RESET)
					PRINTLN("[MAINTAIN_AIR_DRAG_CATCH_UP] DRAG_RESET", DRAG_RESET)
				BREAK
			ENDSWITCH
			
			iDragPosition = iCurrentPosition
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_LOCAL_PED_DRIVING_VEHICLE_UNDERWATER(BOOL bSubMode = FALSE)	
	IF NOT IS_PED_INJURED(LocalPlayerPed)
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_USING(LocalPlayerPed)			
			IF DOES_ENTITY_EXIST(tempVeh)	
				IF IS_ENTITY_IN_WATER(tempVeh)
					IF IS_PED_SITTING_IN_VEHICLE_SEAT(LocalPlayerPed,tempVeh,VS_DRIVER)
						IF bSubMode
							RETURN IS_VEHICLE_IN_SUBMARINE_MODE(tempVeh)
						ELSE
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_AKULA_STEALTH_MODE_IN_MISSION()
	BOOL bInStealthMode = IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iBSVehStealthMode, GLOBAL_BS_VEH_STEALTH_MODE_ON)
	PRINTLN("PROCESS_AKULA_STEALTH_MODE_IN_MISSION - bInStealthMode: ", bInStealthMode)
	
	VEHICLE_INDEX viAkula
	
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
	
		viAkula = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		
		IF GET_ENTITY_MODEL(viAkula) = AKULA
			IF IS_FAKE_MULTIPLAYER_MODE_SET()
				bInStealthMode =  NOT ARE_FOLDING_WINGS_DEPLOYED(viAkula)
				PRINTLN("PROCESS_AKULA_STEALTH_MODE_IN_MISSION - IS_FAKE_MULTIPLAYER_MODE_SET - bInStealthMode: ", bInStealthMode)
			ENDIF
			
			IF GET_PLAYER_WANTED_LEVEL(LocalPlayer) > 0
				INT	ivehID = IS_VEH_A_MISSION_CREATOR_VEH(viAkula)
				IF iVehID > -1 
				AND iVehID < FMMC_MAX_VEHICLES
				AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[ivehID].iAkulaTimeToResetWantedLvl > 0
					IF ARE_PLAYER_STARS_GREYED_OUT(LocalPlayer)
					OR  ARE_PLAYER_FLASHING_STARS_ABOUT_TO_DROP(LocalPlayer)
						IF HAS_NET_TIMER_STARTED(timerResetWantedLevelInStealth)
							IF HAS_NET_TIMER_EXPIRED(timerResetWantedLevelInStealth, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[ivehID].iAkulaTimeToResetWantedLvl)
								RESET_NET_TIMER(timerResetWantedLevelInStealth)
								IF bInStealthMode
									//Reset Wanted Level
									CLEAR_PLAYER_WANTED_LEVEL(LocalPlayer)
									PRINTLN("PROCESS_AKULA_STEALTH_MODE_IN_MISSION - Reset Wanted Level")
								ENDIF
							ELSE 
								SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(LocalPlayer)
								IF bInStealthMode
									PRINTLN("PROCESS_AKULA_STEALTH_MODE_IN_MISSION - ALLOW_EVASION_HUD_IF_DISABLING_HIDDEN_EVASION_THIS_FRAME")
									ALLOW_EVASION_HUD_IF_DISABLING_HIDDEN_EVASION_THIS_FRAME(LocalPlayer, 1)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						//Reset and Start Timer
						IF HAS_NET_TIMER_STARTED(timerResetWantedLevelInStealth)
							RESET_NET_TIMER(timerResetWantedLevelInStealth)
						ENDIF
						START_NET_TIMER(timerResetWantedLevelInStealth)
						IF bInStealthMode
							PRINTLN("PROCESS_AKULA_STEALTH_MODE_IN_MISSION - Start Timer to Reset the Wanted Level: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[ivehID].iAkulaTimeToResetWantedLvl)
							FORCE_START_HIDDEN_EVASION(LocalPlayer)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF HAS_NET_TIMER_STARTED(timerResetWantedLevelInStealth)
					RESET_NET_TIMER(timerResetWantedLevelInStealth)
				ENDIF
			ENDIF
			
			IF bInStealthMode
			AND NOT IS_PED_INJURED(LocalPlayerPed)
				SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(viAkula, FALSE, TRUE) // Disables lock-on
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_KEEP_STEALTH_VEHICLE_BLIPS_VISIBLE)
					SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].iBSVehStealthMode, GLOBAL_BS_VEH_STEALTH_DISABLE_HIDDEN_BLIP_THIS_FRAME) // Keeps blip visible
				ENDIF
				
				PRINTLN("PROCESS_AKULA_STEALTH_MODE_IN_MISSION - Disabling lock on for local player's Akula!")
				SET_BIT(iVehStealthMode_MC_BS, ci_VehStealthMode_HasDisabledLockOn)		
			ELSE
				IF IS_BIT_SET(iVehStealthMode_MC_BS, ci_VehStealthMode_HasDisabledLockOn)
					SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(viAkula, TRUE, TRUE)
					CLEAR_BIT(iVehStealthMode_MC_BS, ci_VehStealthMode_HasDisabledLockOn)
					PRINTLN("PROCESS_AKULA_STEALTH_MODE_IN_MISSION - Re-enabling lock on for local player's Akula!")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_VEHICLES_EVERY_FRAME()
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING)
			IF IS_BIT_SET(iLocalBoolCheck23, LBOOL23_SOMEONE_IS_HACKING_A_VEHICLE)
				CLEAR_BIT(iLocalBoolCheck23, LBOOL23_SOMEONE_IS_HACKING_A_VEHICLE)
			ENDIF
			IF bIsLocalPlayerHost
				iTempHackingTargetsRemaining = 0
			ENDIF
		ENDIF
		IF MC_serverBD.iNumVehCreated > 0
		AND MC_serverBD.iNumVehCreated <= FMMC_MAX_VEHICLES
			INT i = 0
			FOR i = 0 TO (MC_serverBD.iNumVehCreated-1)
				DISPLAY_VEHICLE_HUD(i)
				PROCESS_VEHICLE_LOCK_SHOCK(i)
				MAINTAIN_CAR_ALARM(i)
				STEALING_SETS_LOCAL_PLAYER_WANTED(i)
				PROCESS_VEHICLE_ENGINE_KNOCKBACK(i)
				IF bIsLocalPlayerHost
					MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME(i)
					IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMBUSHKA(g_FMMC_STRUCT.iAdversaryModeType)
					OR (IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMBUSHKA(g_FMMC_STRUCT.iAdversaryModeType) AND ARE_ALL_PLAYER_RENDERHASES_PAUSED_PRIVATE())
						IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iRespawnOnRuleChangeBS > 0
							INT iTeam
							FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
								IF (IS_BIT_SET(iLocalBoolCheck16, LBOOL16_TEAM_0_IS_PAST_THEIR_FIRST_RULE + iTeam)
								AND IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam))
								OR IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_RETRY_CURRENT_RULE_TEAM_0 + iTeam)
									IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iRespawnOnRuleChangeBS, iTeam)
										
										IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
										AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
											SET_BIT(MC_serverBD.iVehCleanup_NeedOwnershipBS, i)
										ENDIF
										
										CLEAR_BIT(iCountThisVehAsDestroyedBS, i)
										IF NOT IS_VEHICLE_USING_RESPAWN_POOL(i)											
											PRINTLN("[JS] GET_VEHICLE_RESPAWNS: ", GET_VEHICLE_RESPAWNS(i), " iveh:  ", i , " iTeam = ", iTeam)											
											IF GET_VEHICLE_RESPAWNS(i) < 1
												SET_VEHICLE_RESPAWNS(i, 1)
											ENDIF
										ELSE										
											PRINTLN("[JS] IS_VEHICLE_USING_RESPAWN_POOL true for iveh:  ", i , " iTeam = ", iTeam)
										ENDIF
										
										PRINTLN("[JS] Setting to Clean up veh to respawn on rule change ", i , " iTeam = ", iTeam)
										BREAKLOOP
									ENDIF
								ENDIF
							ENDFOR
						ENDIF
					ENDIF
				ELSE
					IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iRespawnOnRuleChangeBS > 0
						INT iTeam
						FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
							IF (MC_serverBD_4.iCurrentHighestPriority[iTeam] > 0
							AND IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam))
							OR IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_RETRY_CURRENT_RULE_TEAM_0 + iTeam)
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iRespawnOnRuleChangeBS, iTeam)
									CLEAR_BIT(iCountThisVehAsDestroyedBS, i)
									PRINTLN("[JS] Cleaning up iCountThisVehAsDestroyedBS  ", i)
									BREAKLOOP
								ENDIF
							ENDIF
						ENDFOR
					ENDIF
				ENDIF
				
				VEHICLE_INDEX viVeh
				INT iDoor
				INT iTeam
				
				FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
					IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
					AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
						viVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
									
						PRINTLN("[ML][PROCESS_VEHICLES_EVERY_FRAME] Current rule: ", MC_ServerBD_4.iCurrentHighestPriority[iTeam], "  Unlock on associated rule: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iDoorUnlockRule, "   Lock on specific rule: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehicleDoorSettingsOnRule)
			
						// [ML] Only execute the following logic if there is a rule set for setting specific car door states
						IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehicleDoorSettingsOnRule != -1 		
							
							IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iDoorUnlockRule
							
								IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetEight, ciFMMC_VEHICLE8_VehicleDoorSettingsMidpoint)
								OR(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetEight, ciFMMC_VEHICLE8_VehicleDoorSettingsMidpoint)
								AND IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], MC_ServerBD_4.iCurrentHighestPriority[iTeam]))
															
									IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehicleDoorSettingsOnRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
										IF NOT IS_BIT_SET(iVehicleDoorSetupSetToCreatorSettings, i)
											FOR iDoor = VEHICLE_DOORS_FRONT_LEFT TO VEHICLE_DOORS_TRUNC
												IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_DOOR_FRONT_LEFT_LOCKED + iDoor)
													PRINTLN("[ML][PROCESS_VEHICLES_EVERY_FRAME] On the specified rule. Locking door ", iDoor, " on vehicle - ", i) 
													SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(viVeh, iDoor, VEHICLELOCK_LOCKED)
												ELSE
													PRINTLN("[ML][PROCESS_VEHICLES_EVERY_FRAME] On the specified rule. Unlocking door ", iDoor, " on vehicle - ", i) 
													SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(viVeh, iDoor, VEHICLELOCK_UNLOCKED)
													
													IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitset, ciFMMC_VEHICLE_DOORS_FRONT_LEFT_OPEN + iDoor)
														SET_VEHICLE_DOOR_OPEN(viVeh, INT_TO_ENUM(SC_DOOR_LIST, ciFMMC_VEHICLE_DOORS_FRONT_LEFT_OPEN + iDoor), TRUE)													
													ENDIF
													
												ENDIF
											ENDFOR
											SET_BIT(iVehicleDoorSetupSetToCreatorSettings, i)
										ENDIF
					
									ENDIF
								ENDIF
											
							ELSE
								PRINTLN("[ML][PROCESS_VEHICLES_EVERY_FRAME] Past the unlock associated rule for sPlacedVehicle[", i, "]")										
							ENDIF

						ENDIF				
					ELSE
						PRINTLN("[ML][PROCESS_VEHICLES_EVERY_FRAME] sPlacedVehicle[", i, "].iVehicleDoorSettingsOnRule is OFF, so doing nothing")
					ENDIF
				ENDFOR
				
			ENDFOR
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING)
				IF bIsLocalPlayerHost
					IF MC_serverBD_4.iHackingTargetsRemaining != iTempHackingTargetsRemaining
						MC_serverBD_4.iHackingTargetsRemaining = iTempHackingTargetsRemaining 
						SET_BIT(MC_serverBD.iServerBitSet6, SBBOOL6_COUNTED_VEH_HACK_TARGETS)
						PRINTLN("[TMS][SecuroHack] Setting SBBOOL6_COUNTED_VEH_HACK_TARGETS")
						PRINTLN("[JS][SECUROHACK] iHackingTargetsRemaining updated to  ", MC_serverBD_4.iHackingTargetsRemaining)
					ENDIF
				ENDIF
				IF NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_SOMEONE_IS_HACKING_A_VEHICLE)
				AND iHackStage != ciSECUROSERV_HACK_STAGE_HACK_COMPLETE
				AND bShouldStartControl
					iHackingVehInRangeBitSet = 0
					PRINTLN("[SECUROHACK] Reseting hack stage as not LBOOL23_SOMEONE_IS_HACKING_A_VEHICLE")
					iHackStage = ciSECUROSERV_HACK_STAGE_NO_SIGNAL
					CLEAR_BIT(iLocalBoolCheck26, LBOOL26_SERVUROSERV_LOSING_SIGNAL)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF SVM_FLOW_IS_CURRENT_MISSION_SVM_MISSION()
		PROCESS_SVM_BJXL()
	ENDIF	
	IF IS_BIT_SET( g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciDISPLAY_HEALTH_ON_TEAM_VEHICLE)
		//For use in David and Goliath - team 1 = tank team, expand functionality to show other / more teams
		PRINTLN( "[AW] ciDISPLAY_HEALTH_ON_TEAM_VEHICLE" )
		DISPLAY_TEAM_VEHICLE_HUD(1)
	ENDIF
ENDPROC
