
USING "FM_Mission_Controller_USING.sch"
USING "FM_Mission_Controller_Utility.sch"
USING "FM_Mission_Controller_Entities.sch"
USING "FM_Mission_Controller_ObjectiveCompletion.sch"
USING "FM_Mission_Controller_SpecialCase.sch"
USING "FM_Mission_Controller_Rules.sch"
USING "FM_Mission_Controller_Doors.sch"
USING "FM_Mission_Controller_Cutscene.sch"
USING "FM_Mission_Controller_Debug.sch"
USING "FM_Mission_Controller_Locations.sch"
USING "FM_Mission_Controller_Vehicles.sch"



/// PURPOSE: Set the Peds's state
//PARAMS: Use iOption if there are multiple places this state change can be called from and you want to debug which is happening
PROC SET_PED_STATE(INT iPedID, INT iPedState #IF IS_DEBUG_BUILD, INT iOption = -1 #ENDIF )

	#IF IS_DEBUG_BUILD
		IF iPedState != MC_serverBD_2.iPedState[iPedID]
			TEXT_LABEL_63 l_sDebug
			TEXT_LABEL_63 l_sState
			
			INT iOldState = MC_serverBD_2.iPedState[iPedID]
			
			l_sDebug +="[RCC MISSION]SET_PED_STATE-ped"
			l_sDebug += GET_STRING_FROM_INT(iPedID)
			l_sDebug += " "
			l_sState = GET_PED_STATE_NAME(iOldState)
			l_sDebug += l_sState
			l_sDebug += ">"
			l_sState = GET_PED_STATE_NAME(iPedState)
			l_sDebug += l_sState
			
			IF iOption != -1
				l_sDebug += " nr"
				l_sDebug += GET_STRING_FROM_INT(iOption)
			ENDIF
			
			NET_PRINT(l_sDebug) NET_NL()
		ENDIF
	#ENDIF
	
	MC_serverBD_2.iPedState[iPedID] = iPedState

ENDPROC

FUNC BOOL IS_PED_A_COP_MC(INT iped)

	IF IS_BIT_SET(MC_serverBD.iCopPed[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL SHOULD_STAY_IN_FIXATION_STATE(INT iPed)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedFixationTarget > -1
	AND IS_BIT_SET(MC_serverBD_1.iPedFixationTaskStarted[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))	
	AND NOT IS_BIT_SET(iPedFixationStateCleanedUpBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))	
	AND IS_RULE_VALID_FOR_PED_FIXATION_STATE(iPed)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(INT iPed, PED_INDEX piPed, ENTITY_INDEX eiEntity, INT iLOSFlags = SCRIPT_INCLUDE_ALL)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetFourteen, ciPED_BSFourteen_IgnoreCoverForLOS)
	OR NOT IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
		RETURN HAS_ENTITY_CLEAR_LOS_TO_ENTITY(piPed, eiEntity, iLOSFlags)
	ELSE
		RETURN HAS_ENTITY_CLEAR_LOS_TO_ENTITY_ADJUST_FOR_COVER(piPed, eiEntity, iLOSFlags)
	ENDIF
	
	RETURN FALSE
ENDFUNC

//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: PED ANIMATION CHECKS (what type of animation does the ped have, how should the script handle it) !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************


FUNC BOOL IS_ANIMATION_INTERRUPTIBLE(INT iAnimation)
	
	BOOL bInterruptible = TRUE
	
	//Not for:
	SWITCH iAnimation
		CASE ciPED_IDLE_ANIM__BRIEFCASE
		CASE ciPED_IDLE_ANIM__PRISONER_BREAKOUT_COWER
		CASE ciPED_IDLE_ANIM__PRISONER_BREAKOUT_FREED
		CASE ciPED_IDLE_ANIM__KARENCODES_KARENIDLE
		CASE ciPED_IDLE_ANIM__KARENCODES_DRIVERIDLE
		CASE ciPED_IDLE_ANIM__PRISONBREAK_RASHKOVSKY_IDLE
		CASE ciPED_IDLE_ANIM__PRISONPLANE_AGENT_A_IDLE
		CASE ciPED_IDLE_ANIM__PRISONPLANE_AGENT_A_REACT
		CASE ciPED_IDLE_ANIM__PRISONPLANE_AGENT_B_IDLE
		CASE ciPED_IDLE_ANIM__PRISONPLANE_AGENT_B_REACT
		CASE ciPED_IDLE_ANIM__WITSEC_CHAIR_SNOOZE
		CASE ciPED_IDLE_ANIM__WITSEC_CHAIR_STAND_UP
		CASE ciPED_IDLE_ANIM__WITSEC_CHAIR_FALL
		CASE ciPED_IDLE_ANIM__UNFSHDBIZ_JUDGE_IDLE
		CASE ciPED_IDLE_ANIM__UNFSHDBIZ_JUDGE_BREAKOUT
		CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_BODHI_LOOP
		CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_BODHI_OUTRO
		CASE ciPED_IDLE_ANIM__SIT_ON_FLOOR
		CASE ciPED_IDLE_ANIM__FLOOR_STAND_REACT
		CASE ciPED_IDLE_ANIM__HOSTAGE_1
		CASE ciPED_IDLE_ANIM__HOSTAGE_2
		CASE ciPED_IDLE_ANIM__HOSTAGE_3
		CASE ciPED_IDLE_ANIM__HOSTAGE_4
		CASE ciPED_IDLE_ANIM__HEIST_TUT_LESTER_PHONE_INTRO
		CASE ciPED_IDLE_ANIM__HEIST_TUT_LESTER_PHONE_OUTRO
		CASE ciPED_IDLE_ANIM__STATION_DRUNK_LADY_REACT
		CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_1
		CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_2
		CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_3
		CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_4
		CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_5
		CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_6
		CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_7
		CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_8
		CASE ciPED_IDLE_ANIM__FUNERAL_REACTION_1
		CASE ciPED_IDLE_ANIM__FUNERAL_REACTION_2
		CASE ciPED_IDLE_ANIM__FUNERAL_REACTION_3
		CASE ciPED_IDLE_ANIM__FUNERAL_REACTION_4
		CASE ciPED_IDLE_ANIM__FUNERAL_REACTION_5
		CASE ciPED_IDLE_ANIM__FUNERAL_REACTION_6
		CASE ciPED_IDLE_ANIM__FUNERAL_REACTION_7
		CASE ciPED_IDLE_ANIM__FUNERAL_REACTION_8
		CASE ciPED_IDLE_ANIM__HANG_OUT_STREET
		CASE ciPED_IDLE_ANIM__GO_FOUNDRY_HOSTAGE_PERP_IDLE
		CASE ciPED_IDLE_ANIM__GO_FOUNDRY_HOSTAGE_VICTIM_IDLE
		CASE ciPED_IDLE_ANIM__GO_FOUNDRY_HOSTAGE_PERP_EXECUTE
		CASE ciPED_IDLE_ANIM__GO_FOUNDRY_HOSTAGE_VICTIM_FREED		
			bInterruptible = FALSE
		BREAK
	ENDSWITCH
	
	RETURN bInterruptible
	
ENDFUNC

FUNC BOOL DOES_ANIMATION_NEED_SPECIAL_BREAKOUT(INT iAnimation)
	
	BOOL bSpecialBreakout
	
	SWITCH iAnimation
		CASE ciPED_IDLE_ANIM__WITSEC_CHAIR_SNOOZE
		CASE ciPED_IDLE_ANIM__UNFSHDBIZ_JUDGE_IDLE
		CASE ciPED_IDLE_ANIM__SIT_ON_FLOOR
		CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_BODHI_LOOP
		CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_1
		CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_2
		CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_3
		CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_4
		CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_5
		CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_6
		CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_7
		CASE ciPED_IDLE_ANIM__FUNERAL_IDLE_8
		CASE ciPED_IDLE_ANIM__GO_FOUNDRY_HOSTAGE_PERP_IDLE
		CASE ciPED_IDLE_ANIM__GO_FOUNDRY_HOSTAGE_VICTIM_IDLE
			bSpecialBreakout = TRUE
		BREAK
	ENDSWITCH
	
	RETURN bSpecialBreakout
	
ENDFUNC

FUNC BOOL DOES_SPECIAL_ANIM_LOOP(INT iAnimation)
	
	BOOL bLooping = TRUE
	
	SWITCH iAnimation
		CASE ciPED_IDLE_ANIM__WITSEC_CHAIR_STAND_UP
		CASE ciPED_IDLE_ANIM__WITSEC_CHAIR_FALL
		CASE ciPED_IDLE_ANIM__UNFSHDBIZ_JUDGE_BREAKOUT
		CASE ciPED_IDLE_ANIM__FLOOR_STAND_REACT
		CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_BODHI_OUTRO
		CASE ciPED_IDLE_ANIM__HEIST_TUT_LESTER_PHONE_OUTRO
		CASE ciPED_IDLE_ANIM__PRISONER_BREAKOUT_FREED
		CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_SHOUT_A
		CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_SHOUT_B
		CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_SHOUT_C
		CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_A
		CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_B
		CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_C
		CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_D
		//CASE ciPED_IDLE_ANIM__STAND_AIM
			bLooping = FALSE
		BREAK
	ENDSWITCH
	
	RETURN bLooping
	
ENDFUNC

FUNC BOOL CAN_ANIMATION_PLAY_WHILE_GROUPED(INT iAnimation)
	
	BOOL bPlay
	
	SWITCH iAnimation
		CASE ciPED_IDLE_ANIM__HEIST_TUT_LESTER_PHONE_INTRO
		CASE ciPED_IDLE_ANIM__HEIST_TUT_LESTER_PHONE_OUTRO
		CASE ciPED_IDLE_ANIM__PRISONER_BREAKOUT_FREED
		CASE ciPED_IDLE_ANIM__SHOW_OFF_VEHICLE_HYDRAULICS
			bPlay = TRUE
		BREAK
	ENDSWITCH
	
	RETURN bPlay
	
ENDFUNC

FUNC BOOL CAN_ANIMATION_PLAY_IN_CAR(INT iAnimation)
	
	BOOL bPlay
	
	SWITCH iAnimation
		CASE ciPED_IDLE_ANIM__HEIST_TUT_LESTER_PHONE_INTRO
		CASE ciPED_IDLE_ANIM__HEIST_TUT_LESTER_PHONE_OUTRO
		CASE ciPED_IDLE_ANIM__SHOW_OFF_VEHICLE_HYDRAULICS
			bPlay = TRUE
		BREAK
	ENDSWITCH
	
	RETURN bPlay
	
ENDFUNC

FUNC BOOL IS_ANIMATION_A_COWER(INT iAnimation)
	
	BOOL bCower
	
	SWITCH iAnimation
		CASE ciPED_IDLE_ANIM__HOSTAGE_1
		CASE ciPED_IDLE_ANIM__HOSTAGE_2
		CASE ciPED_IDLE_ANIM__HOSTAGE_3
		CASE ciPED_IDLE_ANIM__HOSTAGE_4
			bCower = TRUE
		BREAK
	ENDSWITCH
	
	RETURN bCower
	
ENDFUNC

FUNC INT GET_GO_FOUNDRY_VICTIM_PED(INT iPerpPed)
	
	INT iPedLoop
	
	INT iClosestPed = -1
	FLOAT fClosestDist2 = 100 // Must be within 10m, don't want to steal anybody else's hostage
	FLOAT fTempDist2
	VECTOR vPerpCoords = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPerpPed].vPos
	
	FOR iPedLoop = 0 TO (MC_serverBD.iNumPedCreated - 1)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedLoop].iIdleAnim = ciPED_IDLE_ANIM__GO_FOUNDRY_HOSTAGE_VICTIM_IDLE
		AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedLoop].vPos)
			
			fTempDist2 = VDIST2(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedLoop].vPos, vPerpCoords)
			
			IF fTempDist2 < fClosestDist2
				iClosestPed = iPedLoop
				fClosestDist2 = fTempDist2
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN iClosestPed
	
ENDFUNC

FUNC INT GET_GO_FOUNDRY_PERP_PED(INT iVictimPed)
	
	INT iPedLoop
	
	INT iClosestPed = -1
	FLOAT fClosestDist2 = 100 // Must be within 10m, don't want to steal anybody else's perp
	FLOAT fTempDist2
	VECTOR vVictimCoords = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iVictimPed].vPos
	
	FOR iPedLoop = 0 TO (MC_serverBD.iNumPedCreated - 1)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedLoop].iIdleAnim = ciPED_IDLE_ANIM__GO_FOUNDRY_HOSTAGE_PERP_IDLE
		AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedLoop].vPos)
			
			fTempDist2 = VDIST2(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedLoop].vPos, vVictimCoords)
			
			IF fTempDist2 < fClosestDist2
				iClosestPed = iPedLoop
				fClosestDist2 = fTempDist2
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN iClosestPed
	
ENDFUNC

FUNC BOOL IS_PED_AT_RIGHT_POINT_FOR_SPECIAL_ANIM(INT iped, PED_INDEX tempPed, BOOL bInVehicle = FALSE, BOOL bAtMidPoint = FALSE)
	
	BOOL bAtRightPoint = FALSE
	
	IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimRule
	OR (DOES_ANIMATION_NEED_SPECIAL_BREAKOUT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim) AND (MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimTeam] >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimRule))
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetTwo,ciPED_BSTwo_SpecialAnim_On_Midpoint)
			IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimTeam],g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimRule)
			OR bAtMidPoint
			OR (MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimTeam] > g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimRule)
				bAtRightPoint = TRUE
			ELSE
				//Hack to get the prisoner breakout animation triggering properly (the VIP can be picked up by either of two teams)
				IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnim = ciPED_IDLE_ANIM__PRISONER_BREAKOUT_FREED)
				AND IS_PED_IN_GROUP(tempPed)
					bAtRightPoint = TRUE
				ENDIF
			ENDIF
		ELSE
			bAtRightPoint = TRUE
		ENDIF
	ENDIF
	
	IF bInVehicle
	AND NOT CAN_ANIMATION_PLAY_IN_CAR(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnim)
		bAtRightPoint = FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSpecialAnim = ciPED_IDLE_ANIM__GO_FOUNDRY_HOSTAGE_VICTIM_FREED
		
		// Only play this once the perp holding this ped hostage is dead
		
		INT iPerp = GET_GO_FOUNDRY_PERP_PED(iped)
		
		IF iPerp != -1
		AND NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iPerp])			
			PED_INDEX PerpPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPerp])
			
			IF IS_PED_INJURED(PerpPed)
				// The perp has been killed
				PRINTLN("[RCC MISSION] IS_PED_AT_RIGHT_POINT_FOR_SPECIAL_ANIM - GO Foundry Hostage ",iped,"'s perp ",iPerp," is dead, return TRUE")
				bAtRightPoint = TRUE
			ELIF IS_BIT_SET(ipedAnimBitset[GET_LONG_BITSET_INDEX(iPerp)], GET_LONG_BITSET_BIT(iPerp))
			AND SHOULD_PED_BE_GIVEN_TASK(PerpPed, iPerp, SCRIPT_TASK_SYNCHRONIZED_SCENE, TRUE)
				// The perp has broken out somehow - possibly set on fire on something
				PRINTLN("[RCC MISSION] IS_PED_AT_RIGHT_POINT_FOR_SPECIAL_ANIM - GO Foundry Hostage ",iped,"'s perp ",iPerp," is no longer animating, return TRUE")
				bAtRightPoint = TRUE
			ENDIF
		ELSE
			IF iPerp != -1
			AND IS_BIT_SET(MC_serverBD_4.iPedKilledBS[GET_LONG_BITSET_INDEX(iPerp)], GET_LONG_BITSET_BIT(iPerp))
				PRINTLN("[RCC MISSION] IS_PED_AT_RIGHT_POINT_FOR_SPECIAL_ANIM - GO Foundry Hostage ",iped,"'s perp ",iPerp," has been killed, return TRUE")
				bAtRightPoint = TRUE
			ELSE
				PRINTLN("[RCC MISSION] IS_PED_AT_RIGHT_POINT_FOR_SPECIAL_ANIM - GO Foundry Hostage ",iped,"'s perp ",iPerp," does not exist, return TRUE")
				bAtRightPoint = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bAtRightPoint
	
ENDFUNC

//Checks if ped tasks shouldn't clear up due to a smooth animation transition
//bAtMidpoint is a bool that overrides the midpoint check - as sometimes the midpoint bitset gets set late
FUNC BOOL IS_PED_DOING_ANIMATION_TRANSITION(INT iped, PED_INDEX tempPed, BOOL bAtMidpoint = FALSE, BOOL bBusyReaction = FALSE)
	
	BOOL bIsTransition = FALSE
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnim != ciPED_IDLE_ANIM__NONE
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnim != ciPED_IDLE_ANIM__NONE
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimTeam != -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimRule != -1
		
		INT iIdleAnim = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnim
		INT iSpecialAnim = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnim
		
		//Are these animations that should transition smoothly?
		IF ( (iIdleAnim = ciPED_IDLE_ANIM__PRISONER_BREAKOUT_COWER) AND (iSpecialAnim = ciPED_IDLE_ANIM__PRISONER_BREAKOUT_FREED) )
		OR ( (iIdleAnim = ciPED_IDLE_ANIM__GUARD_WAVING_IDLE) AND ( (iSpecialAnim = ciPED_IDLE_ANIM__GUARD_WAVING_WAVE) OR (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnim = ciPED_IDLE_ANIM__GUARD_WAVING_REACTINBOOTH) ) )
		OR ( (iIdleAnim = ciPED_IDLE_ANIM__PRISONPLANE_AGENT_A_IDLE) AND (iSpecialAnim = ciPED_IDLE_ANIM__PRISONPLANE_AGENT_A_REACT) )
		OR ( (iIdleAnim = ciPED_IDLE_ANIM__PRISONPLANE_AGENT_B_IDLE) AND (iSpecialAnim = ciPED_IDLE_ANIM__PRISONPLANE_AGENT_B_REACT) )
		OR ( (iIdleAnim = ciPED_IDLE_ANIM__PRISONBREAK_BUS_GUARD_A_IDLE_CHAT) AND (iSpecialAnim = ciPED_IDLE_ANIM__PRISONBREAK_BUS_GUARD_A_REACT_TO_BUS) )
		OR ( (iIdleAnim = ciPED_IDLE_ANIM__WITSEC_CHAIR_SNOOZE) AND ((iSpecialAnim = ciPED_IDLE_ANIM__WITSEC_CHAIR_FALL) OR (iSpecialAnim = ciPED_IDLE_ANIM__WITSEC_CHAIR_STAND_UP)) )
		OR ( (iIdleAnim = ciPED_IDLE_ANIM__UNFSHDBIZ_JUDGE_IDLE) AND (iSpecialAnim = ciPED_IDLE_ANIM__UNFSHDBIZ_JUDGE_BREAKOUT) )
		OR ( (iIdleAnim = ciPED_IDLE_ANIM__SIT_ON_FLOOR) AND (iSpecialAnim = ciPED_IDLE_ANIM__FLOOR_STAND_REACT) )
		OR ( (iIdleAnim = ciPED_IDLE_ANIM__TREVOR_NARCFIN_BODHI_LOOP) AND (iSpecialAnim = ciPED_IDLE_ANIM__TREVOR_NARCFIN_BODHI_OUTRO) )
		OR ( (iIdleAnim = ciPED_IDLE_ANIM__STATION_COP_BEHIND_DESK_IDLE) AND (iSpecialAnim = ciPED_IDLE_ANIM__STATION_COP_BEHIND_DESK_REACT ) )
		OR ( (iIdleAnim = ciPED_IDLE_ANIM__STATION_COP_BESIDE_LADY_IDLE) AND (iSpecialAnim = ciPED_IDLE_ANIM__STATION_COP_BESIDE_LADY_REACT ) )
		OR ( (iIdleAnim = ciPED_IDLE_ANIM__VALKYRIE_AGENT_IDLE) AND (iSpecialAnim = ciPED_IDLE_ANIM__VALKYRIE_AGENT_HANDOVER ) )
		OR ( (iIdleAnim = ciPED_IDLE_ANIM__GO_FOUNDRY_HOSTAGE_PERP_IDLE) AND (iSpecialAnim = ciPED_IDLE_ANIM__GO_FOUNDRY_HOSTAGE_PERP_EXECUTE) )
		OR ( (iIdleAnim = ciPED_IDLE_ANIM__GO_FOUNDRY_HOSTAGE_VICTIM_IDLE) AND (iSpecialAnim = ciPED_IDLE_ANIM__GO_FOUNDRY_HOSTAGE_VICTIM_FREED) )
		OR ( (iIdleAnim = ciPED_IDLE_ANIM__CROUCH_AIM) AND (iSpecialAnim = ciPED_IDLE_ANIM__STAND_AIM) )
		
			//Check if team is just about to trigger the special
			IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimRule
				
				//If we haven't yet launched the special animation:
				IF NOT IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					
					IF IS_PED_AT_RIGHT_POINT_FOR_SPECIAL_ANIM(iped,tempPed,DEFAULT,bAtMidPoint)
						
						//Are we just coming out of the idle:
						IF NOT ( IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_IdleAnimOnJustThisRule) OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_IdleAnimBeforeAndInclThisRule) OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_IdleAnimAfterAndInclThisRule) )
							//Idle anim always running
							bIsTransition = TRUE
						ELSE
							INT iRuleToCheck
							
							IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetTwo,ciPED_BSTwo_SpecialAnim_On_Midpoint)
								IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].fSpecialAnimTriggerRange = cfMAX_SPECIALANIM_TRIGGER_DIST
									//if not triggering on midpoint, was the prev rule idle
									iRuleToCheck = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimRule - 1
								ELSE
									//Triggering on range, check this rule:
									iRuleToCheck = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimRule
								ENDIF
							ELSE
								//if triggering on midpoint, was this rule idle
								iRuleToCheck = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimRule
							ENDIF
							
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_IdleAnimOnJustThisRule)
								IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnimRule = iRuleToCheck
								AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnimTeam = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSpecialAnimTeam
									bIsTransition = TRUE
								ENDIF
							ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_IdleAnimBeforeAndInclThisRule)
								IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnimRule >= iRuleToCheck
								AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnimTeam = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSpecialAnimTeam
									bIsTransition = TRUE
								ENDIF
							ELSE
								//ciPED_BSTwo_IdleAnimBeforeAndInclThisRule:
								IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnimRule <= iRuleToCheck
								AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnimTeam = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSpecialAnimTeam
									bIsTransition = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELIF ( (iIdleAnim = ciPED_IDLE_ANIM__HEIST_TUT_LESTER_PHONE_INTRO) AND (iSpecialAnim = ciPED_IDLE_ANIM__HEIST_TUT_LESTER_PHONE_OUTRO) )
			//Special case, the idle task needs to last for a while after it gets called:
			
			//If we haven't yet launched the special animation:
			IF NOT IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				INT iIdleTeam = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnimTeam
				INT iIdleRule = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnimRule
				INT iSpecialTeam = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimTeam
				INT iSpecialRule = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimRule
				
				IF iIdleTeam != -1
				AND iIdleRule != -1
				AND iSpecialTeam != -1
				AND iSpecialRule != -1
					IF MC_serverBD_4.iCurrentHighestPriority[iIdleTeam] >= iIdleRule
					AND MC_serverBD_4.iCurrentHighestPriority[iSpecialTeam] <= iSpecialRule
						//Should probably be more rigorous checks for animation order here, but not needed for this case (yet)
						bIsTransition = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT bIsTransition
			// Special case, trevor at the back of the bhodi, don't interrupt his special anim
			IF iIdleAnim = ciPED_IDLE_ANIM__TREVOR_NARCFIN_BODHI_LOOP AND iSpecialAnim = ciPED_IDLE_ANIM__TREVOR_NARCFIN_BODHI_OUTRO
				IF NOT SHOULD_PED_BE_GIVEN_TASK( tempPed, iPed, SCRIPT_TASK_SYNCHRONIZED_SCENE, TRUE )
				AND IS_ENTITY_PLAYING_ANIM( tempPed, "ANIM@HEISTS@NARCOTICS@FINALE@IG_4_CAR_INTRO", "TREVOR_OUTRO", ANIM_SYNCED_SCENE )
					bIsTransition = TRUE
				ENDIF
			ELSE
				IF ( NOT ( SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
						   AND SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_SYNCHRONIZED_SCENE, TRUE) )
					AND DOES_ANIMATION_NEED_SPECIAL_BREAKOUT(iIdleAnim) )
					
					IF (iIdleAnim != ciPED_IDLE_ANIM__GO_FOUNDRY_HOSTAGE_PERP_IDLE) // Special case: we don't want the hostage taker to go through the execution breakout anim if the victim has gone
					OR (NOT IS_BIT_SET(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
						
						bIsTransition = TRUE
						
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	IF bBusyReaction
		bIsTransition = TRUE
	ENDIF
	
	RETURN bIsTransition
	
ENDFUNC

/// PURPOSE: This function was used before Mocap cutscenes existed, and checked if the ped was doing an animation while in a Scripted Cutscene,
///    ie: Karen/The Driver in what is now the mid-mission Humane Labs Key Codes cutscene, the Bank Manager in Booty Call, Rashkovsky breaking
///    out of his cowering animation in The Prison Break heist finale
FUNC BOOL IS_PED_IN_A_CUTSCENE(INT iped)
	
	BOOL bInCutscene
	
	//If we're running a special anim
	IF IS_BIT_SET(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		//If this is one we don't want interrupting
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSpecialAnim = ciPED_IDLE_ANIM__PRISONER_BREAKOUT_FREED
			bInCutscene = TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iPedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
		//If this is one we don't want interrupting
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim = ciPED_IDLE_ANIM__PRISONER_BREAKOUT_COWER
			bInCutscene = TRUE
		ENDIF
	ENDIF
	
	RETURN bInCutscene
	
ENDFUNC

FUNC BOOL IS_PED_ANIMATION_PLAYING(PED_INDEX tempPed, INT iped)
	
	BOOL bPlaying = FALSE

	IF (NOT SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE))
	OR (NOT SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_SYNCHRONIZED_SCENE, TRUE))
		bPlaying = TRUE
		
		IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim = ciPED_IDLE_ANIM__TREVOR_NARCFIN_BODHI_LOOP)
		AND IS_BIT_SET(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed)) // Ped has started the bodhi breakout anim
			IF IS_ENTITY_PLAYING_ANIM(tempPed,"ANIM@HEISTS@NARCOTICS@FINALE@IG_4_CAR_INTRO","TREVOR_OUTRO")
			AND MC_serverBD_1.iTrevorBodhiBreakoutSynchSceneID != -1
				
				INT iSynchScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_serverBD_1.iTrevorBodhiBreakoutSynchSceneID)
				
				//check if Trevor's animation area is blocked, B*2230724
				//BOOL bAreaOccupied = IS_AREA_OCCUPIED(<< 635.851990 - 2.0, -435.981476 - 3.5, 25.051590 - 1.5>>, << 635.851990 + 2.0, -435.981476 + 3.5, 25.051590 + 1.5>>, FALSE, TRUE, FALSE, FALSE, FALSE, NULL, FALSE)
				//check for a smaller area, B*2237781
				BOOL bAreaOccupied = IS_AREA_OCCUPIED(<< 636.078674 - 1.5, -436.732452 - 3.0, 24.973911 - 1.5>>, << 636.078674 + 1.5, -436.732452 + 3.0, 24.973911 + 1.5>>, FALSE, TRUE, FALSE, FALSE, FALSE, NULL, FALSE)

				IF iSynchScene != -1
				AND IS_SYNCHRONIZED_SCENE_RUNNING(iSynchScene)
					
					//blendout earlier from synched scene if area is blocked, B*2230724
					IF bAreaOccupied
						IF (GET_SYNCHRONIZED_SCENE_PHASE(iSynchScene) >= 0.65)
							bPlaying = FALSE
						ENDIF
					ENDIF
				
					IF (GET_SYNCHRONIZED_SCENE_PHASE(iSynchScene) >= 0.9)
						bPlaying = FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim = ciPED_IDLE_ANIM__UNFSHDBIZ_JUDGE_IDLE)
		AND IS_BIT_SET(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed)) // Ped has started the breakout anim
			IF IS_ENTITY_PLAYING_ANIM(tempPed,"anim@heists@prison_heistunfinished_biz@popov_react","popov_react")
				IF GET_ENTITY_ANIM_CURRENT_TIME(tempPed, "anim@heists@prison_heistunfinished_biz@popov_react","popov_react") >= 0.925
					bPlaying = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bPlaying
	
ENDFUNC



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: SERVER PED SPOOK / AGGRO CHECKS !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************

FUNC BOOL HAVE_CLEAR_LOS_TO_ANY_PLAYER_NEARBY(PED_INDEX tempPed)	
	INT i, iPlayerCount, iPlayersMax
	iPlayersMax = MC_serverBD.iNumberOfPlayingPlayers[0]+MC_serverBD.iNumberOfPlayingPlayers[1]+MC_serverBD.iNumberOfPlayingPlayers[2]+MC_serverBD.iNumberOfPlayingPlayers[3]
	
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF iPlayerCount < iPlayersMax
			PARTICIPANT_INDEX piPart = INT_TO_PARTICIPANTINDEX(i)
			
			IF NETWORK_IS_PARTICIPANT_ACTIVE(piPart)
				PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)
				
				IF NETWORK_IS_PLAYER_ACTIVE(piPlayer)
					IF NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(piPlayer)
					AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_ANY_SPECTATOR)
						iPlayerCount++
						
						PED_INDEX pedPlayer = GET_PLAYER_PED(piPlayer)
						
						IF NOT IS_PED_INJURED(pedPlayer)
						AND HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(tempPed, pedPlayer)
							RETURN TRUE			
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			BREAKLOOP
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC



FUNC BOOL IS_PED_AN_AGGRO_PED(INT iped)
	
	BOOL bAggroPed = FALSE //Will this ped be an aggro ped at some point in future?
	
	INT iTeam
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAgroFailPriority[iTeam] > -1
			IF NOT ( IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroAfterAndInclRuleT0 + iTeam * 2) OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroBeforeAndInclRuleT0 + iTeam * 2) )
				IF MC_serverBD_4.iCurrentHighestPriority[iTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAgroFailPriority[iTeam]
					bAggroPed = TRUE
				ELIF MC_serverBD_4.iCurrentHighestPriority[iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAgroFailPriority[iTeam]
					
					IF (NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPED_BSEight_Aggro_SwitchesOnMidpoint_T0 + iTeam))
					OR (NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAgroFailPriority[iTeam])) // If we haven't reached the midpoint yet
						//It's still going on
						bAggroPed = TRUE
					ENDIF
					
				ENDIF
			ELSE
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroAfterAndInclRuleT0 + iTeam * 2)
					//If the rule is before the current one, they will still be an aggro ped
					//If after, they are going to be an aggro ped
					//So no matter what, it's true!
					bAggroPed = TRUE
				ELSE //Before and incl:
					IF MC_serverBD_4.iCurrentHighestPriority[iTeam] > g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAgroFailPriority[iTeam]
						//If we've passed it already, we're done with the Aggro
					ELSE
						IF (NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPED_BSEight_Aggro_SwitchesOnMidpoint_T0 + iTeam))
						OR (NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAgroFailPriority[iTeam])) // If we haven't reached the midpoint yet
							//It's still going on
							bAggroPed = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF bAggroPed
				//Break out, as we already know this is an aggro ped:
				iTeam = FMMC_MAX_TEAMS
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN bAggroPed
	
ENDFUNC

/// PURPOSE: This runs on the frame the mission finishes and checks if any live aggro peds remain, and if so it fails the mission (because the fail countdown timer 
///    would normally kick in, but the mission is finishing right now)
PROC RUN_FINAL_AGGRO_PED_CHECK()
	
	INT iTeamCheckBS
	INT iTeam, iPed
	
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF (NOT HAS_TEAM_FAILED(iTeam))
		AND HAS_NET_TIMER_STARTED(MC_serverBD.tdAggroCountdownTimer[0])
			PRINTLN("[RCC MISSION][SpookAggro] RUN_FINAL_AGGRO_PED_CHECK - Team ",iTeam," hasn't failed and has an aggro timer still running!")
			SET_BIT(iTeamCheckBS, iTeam)
		ENDIF
	ENDFOR
	
	IF iTeamCheckBS > 0
		IF MC_serverBD.iPedAggroedBitset[0] > 0
		OR MC_serverBD.iPedAggroedBitset[1] > 0
			
			INT iTempBS[FMMC_MAX_PEDS_BITSET]
			iTempBS[0] = MC_serverBD.iPedAggroedBitset[0]
			iTempBS[1] = MC_serverBD.iPedAggroedBitset[1]
			iTempBS[2] = MC_serverBD.iPedAggroedBitset[2]
			
			FOR iped = 0 TO (MC_serverBD.iNumPedCreated - 1)
				IF IS_BIT_SET(MC_serverBD.iPedAggroedBitset[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
					
					PRINTLN("[RCC MISSION][SpookAggro] RUN_FINAL_AGGRO_PED_CHECK - Ped ",iped," is still aggroed (but not necessarily alive)")
					
					CLEAR_BIT(iTempBS[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
					
					IF (NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroDoesntFail))
					AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree, ciPED_BSThree_AggroFailHasDelay)
						
						IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
							
							PRINTLN("[RCC MISSION][SpookAggro] RUN_FINAL_AGGRO_PED_CHECK - Ped ",iped," is still aggroed and alive!")
							
							FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
								IF IS_BIT_SET(iTeamCheckBS, iTeam)
									
									IF NOT IS_BIT_SET(MC_serverBD.iPedSpookedByDeadBodyBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
										MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_PED_AGRO
										SET_TEAM_FAILED(iTeam,int_to_enum(eFailMissionEnum,enum_to_int(mFail_PED_AGRO_T0)+iTeam),false,MC_serverBD.iPartCausingFail[iTeam])
									ELSE
										MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_PED_FOUND_BODY
										SET_TEAM_FAILED(iTeam,mFail_PED_FOUND_BODY)
									ENDIF
									
									MC_serverBD.iEntityCausingFail[iTeam] = iped
									PRINTLN("[RCC MISSION][SpookAggro] RUN_FINAL_AGGRO_PED_CHECK - MISSION FAILED FOR TEAM:",iTeam," BECAUSE PED AGGROED: ",iped)
									
									CLEAR_BIT(iTeamCheckBS, iTeam)
									
								ENDIF
							ENDFOR
							
						ENDIF
						
					ENDIF
				ENDIF
				
				IF (iTeamCheckBS = 0)
				AND (iTempBS[0] = 0 AND iTempBS[1] = 0 AND iTempBS[2] = 0)
					iped = MC_serverBD.iNumPedCreated // Break out, we've checked everyone we need to (or failed all the teams we can)
				ENDIF
			ENDFOR
			
		ENDIF
	ENDIF
	
ENDPROC

FUNC INT GET_PART_PED_IS_IN_COMBAT_WITH(PED_INDEX ThisPed)
	
	INT iPartCausingFail = -1
	
	VECTOR vPed = GET_ENTITY_COORDS(ThisPed)
	
	FLOAT fClosestDist2 = 90000 //Players will need to be closer than 300m
	INT iPlayersToCheck = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]
	INT iPlayersChecked
	
	INT iPart
	FLOAT fTempDist2
	PARTICIPANT_INDEX tempPart
	PLAYER_INDEX tempPlayer
	PED_INDEX tempPed
	
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		
		tempPart = INT_TO_PARTICIPANTINDEX(iPart)
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			
			IF (NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR))
			OR IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_HEIST_SPECTATOR)
				
				iPlayersChecked++
				
				tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
				
				IF IS_NET_PLAYER_OK(tempPlayer)
					
					tempPed = GET_PLAYER_PED(tempPlayer)
					
					fTempDist2 = VDIST2(GET_ENTITY_COORDS(tempPed), vPed)
					
					IF fTempDist2 < fClosestDist2
					AND IS_PED_IN_COMBAT(ThisPed, tempPed)
						fClosestDist2 = fTempDist2
						iPartCausingFail = iPart
					ENDIF
				ENDIF
				
				IF iPlayersChecked >= iPlayersToCheck
					iPart = MAX_NUM_MC_PLAYERS //Break out!
				ENDIF
				
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN iPartCausingFail
	
ENDFUNC

FUNC INT GET_PART_CLOSEST_TO_ALERTED_PED(PED_INDEX ThisPed)
	
	INT iPartCausingFail = -1
	
	VECTOR vPed = GET_ENTITY_COORDS(ThisPed)
	
	FLOAT fClosestDist2 = 90000 //Players will need to be closer than 300m
	INT iPlayersToCheck = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]
	INT iPlayersChecked
	
	INT iPart
	FLOAT fTempDist2
	PARTICIPANT_INDEX tempPart
	PLAYER_INDEX tempPlayer
	PED_INDEX tempPed
	
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		
		tempPart = INT_TO_PARTICIPANTINDEX(iPart)
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			
			IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)
			AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_HEIST_SPECTATOR)
				
				iPlayersChecked++
				
				tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
				
				IF IS_NET_PLAYER_OK(tempPlayer)
					
					tempPed = GET_PLAYER_PED(tempPlayer)
					
					fTempDist2 = VDIST2(GET_ENTITY_COORDS(tempPed), vPed)
					
					IF fTempDist2 < fClosestDist2
						fClosestDist2 = fTempDist2
						iPartCausingFail = iPart
					ENDIF
				ENDIF
				
				IF iPlayersChecked >= iPlayersToCheck
					iPart = MAX_NUM_MC_PLAYERS //Break out!
				ENDIF
				
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN iPartCausingFail
	
ENDFUNC

FUNC BOOL SHOULD_TEAM_AGRO_FAIL_FOR_TOUCHING_PED(INT iped, INT iTeam)
	
	BOOL bFail = FALSE
	
	IF DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_PED(iPed, iTeam)
		RETURN FALSE
	ENDIF
	
	
	SWITCH iTeam
		CASE 0
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIgnoreTouchingT0)
				bFail = TRUE
			ENDIF
		BREAK
		
		CASE 1
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIgnoreTouchingT1)
				bFail = TRUE
			ENDIF
		BREAK
		
		CASE 2
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIgnoreTouchingT2)
				bFail = TRUE
			ENDIF
		BREAK
		
		CASE 3
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIgnoreTouchingT3)
				bFail = TRUE
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN bFail
	
ENDFUNC

FUNC INT GET_NEAREST_AGGRO_PART_TO_PED(PED_INDEX pedToCheck, INT iped, FLOAT& fDistance2, INT& iTeamOfPart)

	INT iClosestAggroPart = -1
	FLOAT fClosestDist2 = 999999999 //Furthest distance apart on map is O(10,000)
	FLOAT fTempDist2
	
	INT iTeam
	FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
		
		IF SHOULD_AGRO_FAIL_FOR_TEAM(iped,iTeam)
			INT iClosestPlyrOnTeam = GET_NEAREST_PLAYER_TO_ENTITY_ON_TEAM(pedToCheck,iTeam)
			PLAYER_INDEX tempPlayer = INT_TO_PLAYERINDEX(iClosestPlyrOnTeam)
			IF (tempPlayer != INVALID_PLAYER_INDEX())
			AND IS_NET_PLAYER_OK(tempPlayer)
			AND NETWORK_IS_PLAYER_A_PARTICIPANT(tempPlayer)
				fTempDist2 = VDIST2(GET_PLAYER_COORDS(tempPlayer),GET_ENTITY_COORDS(pedToCheck))
				IF (fTempDist2 < fClosestDist2)
					fClosestDist2 = fTempDist2
					iClosestAggroPart = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(tempPlayer))
					iTeamOfPart = iTeam
				ENDIF
			ENDIF
		ENDIF
		
	ENDFOR
	
	fDistance2 = fClosestDist2
	
	RETURN iClosestAggroPart

ENDFUNC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: CLIENT PED AGGRO INIT (playing Audio and changing ped flags) !
//
//************************************************************************************************************************************************************



// These functions are run by the client who owns the ped (rather than the server), but they are in the Brain header as they are needed to be run by some of
// 	the server initialisation functions below (to speed up the aggro process)

PROC PLAY_PED_SPOOKED_AUDIO(INT iped)

	INT iscream
	
	PED_INDEX tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
	
	IF NOT IS_ENTITY_DEAD(tempPed)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFlee = ciPED_FLEE_ON
			iscream = GET_RANDOM_INT_IN_RANGE(0, 10)
		ELSE
			iscream = GET_RANDOM_INT_IN_RANGE(4, 8)
		ENDIF
		
		SWITCH iscream
			CASE 0
				PLAY_PAIN(tempPed, AUD_DAMAGE_REASON_SCREAM_PANIC)
				PRINTLN("[RCC][SpookAggro] PLAY_PED_SPOOKED_AUDIO - Case 0 for ped: ", iped)
			BREAK
			CASE 1
				PLAY_PAIN(tempPed, AUD_DAMAGE_REASON_SCREAM_SCARED)
				PRINTLN("[RCC][SpookAggro] PLAY_PED_SPOOKED_AUDIO - Case 1 for ped: ", iped)
			BREAK
			CASE 2
				PLAY_PAIN(tempPed, AUD_DAMAGE_REASON_SCREAM_TERROR)
				PRINTLN("[RCC][SpookAggro] PLAY_PED_SPOOKED_AUDIO - Case 2 for ped: ", iped)
			BREAK
			CASE 3
				PLAY_PAIN(tempPed, AUD_DAMAGE_REASON_SCREAM_PANIC_SHORT)
				PRINTLN("[RCC][SpookAggro] PLAY_PED_SPOOKED_AUDIO - Case 3 for ped: ", iped)
			BREAK
			CASE 4
				PLAY_PED_AMBIENT_SPEECH_AND_CLONE_NATIVE(tempPed, "GENERIC_CURSE_MED", "SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
				PRINTLN("[RCC][SpookAggro] PLAY_PED_SPOOKED_AUDIO - Case 4 for ped: ", iped)
			BREAK
			CASE 5
				PLAY_PED_AMBIENT_SPEECH_AND_CLONE_NATIVE(tempPed, "GENERIC_CURSE_HIGH", "SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
				PRINTLN("[RCC][SpookAggro] PLAY_PED_SPOOKED_AUDIO - Case 5 for ped: ", iped)
			BREAK
			CASE 6
				PLAY_PED_AMBIENT_SPEECH_AND_CLONE_NATIVE(tempPed, "GENERIC_SHOCKED_MED", "SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
				PRINTLN("[RCC][SpookAggro] PLAY_PED_SPOOKED_AUDIO - Case 6 for ped: ", iped)
			BREAK
			CASE 7
				PLAY_PED_AMBIENT_SPEECH_AND_CLONE_NATIVE(tempPed, "GENERIC_SHOCKED_HIGH", "SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
				PRINTLN("[RCC][SpookAggro] PLAY_PED_SPOOKED_AUDIO - Case 7 for ped: ", iped)
			BREAK
			CASE 8
				PLAY_PED_AMBIENT_SPEECH_AND_CLONE_NATIVE(tempPed, "GENERIC_FRIGHTENED_MED", "SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
				PRINTLN("[RCC][SpookAggro] PLAY_PED_SPOOKED_AUDIO - Case 8 for ped: ", iped)
			BREAK
			CASE 9
				PLAY_PED_AMBIENT_SPEECH_AND_CLONE_NATIVE(tempPed, "GENERIC_FRIGHTENED_HIGH", "SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
				PRINTLN("[RCC][SpookAggro] PLAY_PED_SPOOKED_AUDIO - Case 9 for ped: ", iped)
			BREAK
		ENDSWITCH
		
		#IF IS_DEBUG_BUILD
			PRINTLN("[RCC][SpookAggro] PLAY_PED_SPOOKED_AUDIO - Ped: ", iped, " model is ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(tempPed)))
		#ENDIF
	
	ENDIF
	
ENDPROC

PROC PLAY_PED_AGGROED_AUDIO(INT iped, INT iSpookedReason)
	
	PED_INDEX tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
	
	IF NOT IS_BIT_SET(iPedLocalSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
	
		IF NOT IS_ENTITY_DEAD(tempPed)
			
			IF iSpookedReason = ciSPOOK_SEEN_BODY
				PLAY_PED_AMBIENT_SPEECH_AND_CLONE_NATIVE(tempPed,"GENERIC_CURSE_MED","SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
			ELIF (iSpookedReason = ciSPOOK_PLAYER_HEARD
				  OR iSpookedReason = ciSPOOK_PLAYER_TOUCHING_ME)
				PLAY_PED_AMBIENT_SPEECH_AND_CLONE_NATIVE(tempPed, "GENERIC_FRIGHTENED_MED", "SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
				println("[MJM][SpookAggro] PLAY_PED_AGGROED_AUDIO - PLAY_PED_AMBIENT_SPEECH")
			ELSE
				
				INT iSound = GET_RANDOM_INT_IN_RANGE(0, 9)
				
				IF (iSound < 6) //66% chance
					//decent
					PLAY_PAIN(tempPed,AUD_DAMAGE_REASON_SCREAM_SHOCKED)
				ELSE //33% chance
					//a bit gentle, whimpery
					PLAY_PAIN(tempPed, AUD_DAMAGE_REASON_SCREAM_PANIC)

				ENDIF
				
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE: Run by the owner of the ped (not necessarily the server) to trigger the ped Aggro audio shout, and change various ped flags/properties
PROC TRIGGER_AGGRO_TASKS_ON_PED(PED_INDEX tempPed, INT iped, INT iSpookedReason, BOOL bPlayAudio = TRUE)
	
	IF bPlayAudio
		PLAY_PED_AGGROED_AUDIO(iped, iSpookedReason)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_TriggerTasksOnAggro)
		PRINTLN("[RCC MISSION][SpookAggro] TRIGGER_AGGRO_TASKS_ON_PED - Aggro Task Activation Triggered for ped: ",iped) 
		IF MC_serverBD_2.iPedState[iped] != ciTASK_CHOOSE_NEW_TASK
			IF NOT IS_PED_IN_COMBAT(tempPed)
			AND CAN_PED_TASKS_BE_CLEARED(tempPed)
				//CLEAR_PED_TASKS(tempPed)
				SET_PED_RETASK_DIRTY_FLAG(iPed)
			ENDIF
			PRINTLN("[RCC MISSION][SpookAggro] TRIGGER_AGGRO_TASKS_ON_PED - Aggro triggered, clearing tasks",iped)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iPedZoneAggroed[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
		PRINTLN("[RCC MISSION][SpookAggro] TRIGGER_AGGRO_TASKS_ON_PED - SET_BLOCKING_OF_NON_TEMPORARY_EVENTS Called with false: ",iped)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, FALSE)
	ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour,ciPED_BSFour_StayInTasks_On_Aggro)
		PRINTLN("[RCC MISSION][SpookAggro] TRIGGER_AGGRO_TASKS_ON_PED - SET_BLOCKING_OF_NON_TEMPORARY_EVENTS Called with true: ",iped)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, TRUE)
	ENDIF
	
	IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedPerceptionSettings != ciPED_PERCEPTION_DEFAULT)
		FLOAT fRange = GET_FMMC_PED_PERCEPTION_RANGE(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedPerceptionSettings, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fPedPerceptionCentreRange)
		
		IF fRange < 60 //Default combat range
			fRange = CLAMP(fRange*1.5,0,60)
		ENDIF
		
		SET_PED_VISUAL_FIELD_PROPERTIES(tempPed,fRange)
	ENDIF
	
ENDPROC


//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: SERVER PED AGGRO INIT !
//
//************************************************************************************************************************************************************

PROC PROCESS_SERVER_PED_SPEECH_AGGRO(PED_INDEX tempPed, INT iPed)
	BOOL bDead = IS_PED_DEAD_OR_DYING(tempPed)
	
	IF NOT bDead
		MC_ServerBD_2.iAssociatedAction[iped] = ciACTION_NOTHING
		MC_serverBD.niTargetID[iped] = NULL
		MC_serverBD_2.iTargetID[iped] = -1
		SET_BIT(MC_serverBD.iPedAtGotoLocBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		
		IF NOT IS_BIT_SET(MC_serverBD.iPedSpookedByDeadBodyBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			//Don't throw the ped straight into combat if they need to breakout of their anim
			IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
			AND SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_SYNCHRONIZED_SCENE, TRUE)
			OR NOT DOES_ANIMATION_NEED_SPECIAL_BREAKOUT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim)
				SET_PED_STATE(iped,ciTASK_GENERAL_COMBAT #IF IS_DEBUG_BUILD , 2 #ENDIF )
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SERVER_PED_AGGRO_TASK_CHOOSING(PED_INDEX tempPed, INT iPed)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_TriggerTasksOnAggro)
		SET_BIT(MC_serverBD.iPedTaskActive[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		PRINTLN("[RCC MISSION][SpookAggro] MANAGE_PED_TASK_ACTIVATION - (PROCESS_SERVER_PED_AGGRO_TASK_CHOOSING) Task Activation Triggered for ped: ",iped)
	ELSE
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour,ciPED_BSFour_StayInTasks_On_Aggro)
			//Finish task progress:
			MC_ServerBD_2.iAssociatedAction[iped] = ciACTION_NOTHING
			MC_serverBD.niTargetID[iped] = NULL
			MC_serverBD_2.iTargetID[iped] = -1
			SET_BIT(MC_serverBD.iPedAtGotoLocBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			
			IF NOT IS_BIT_SET(MC_serverBD.iPedSpookedByDeadBodyBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				//Don't throw the ped straight into combat if they need to breakout of their anim
				IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
				AND SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_SYNCHRONIZED_SCENE, TRUE)
				OR NOT DOES_ANIMATION_NEED_SPECIAL_BREAKOUT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim)
					SET_PED_STATE(iped,ciTASK_GENERAL_COMBAT #IF IS_DEBUG_BUILD , 2 #ENDIF )
				ENDIF
			//ELSE
				//Spooked by dead body response - already in this state
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL WILL_ANY_AGGRO_PED_FAIL_THE_MISSION()
	
	INT iPed
	FOR iPed = 0 TO FMMC_MAX_PEDS - 1
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
		AND NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroDoesntFail)
				PRINTLN("[SpookAggro] WILL_ANY_AGGRO_PED_FAIL_THE_MISSION - Ped ", iPed, " will fail the mission!")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	
	PRINTLN("[SpookAggro] WILL_ANY_AGGRO_PED_FAIL_THE_MISSION - No valid peds will fail the mission!")
	RETURN FALSE
ENDFUNC

PROC PROCESS_SERVER_PED_AGGRO_SETTING_DATA(INT iPed, INT iTeam, INT iPartCausingFail = -1)
	
	IF IS_BIT_SET( iPedHitByStunGun[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
		PRINTLN("[RCC MISSION][SpookAggro] PROCESS_SERVER_PED_AGGRO_SETTING_DATA - Don't set iObjectivePedAggroedBitset bit due to stunned ",iped )
		EXIT 
	ENDIF
	
	PRINTLN("[RCC MISSION][SpookAggro] PROCESS_SERVER_PED_AGGRO_SETTING_DATA - setting iObjectivePedAggroedBitset bit due to ped ",iped," - for team ",iTeam,", priority ",MC_serverBD_4.iCurrentHighestPriority[iTeam],
			", iPartCausingFail: ", iPartCausingFail)
	
	#IF IS_DEBUG_BUILD
		IF iPartCausingFail != -1
			PRINTLN("[RCC MISSION][SpookAggro] PROCESS_SERVER_PED_AGGRO_SETTING_DATA - Player causing fail: ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iPartCausingFail)))
		ENDIF
		
		DEBUG_PRINTCALLSTACK()
	#ENDIF
			
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEleven, ciPed_BSEleven_FailDelayForWanted)
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree, ciPED_BSThree_AggroFailHasDelay)			
			SET_BIT(MC_serverBD.iObjectivePedAggroedBitset[iTeam],MC_serverBD_4.iCurrentHighestPriority[iTeam])
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroDoesntFail)
	OR (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_AggroAlertsAllOtherAggroPeds) AND WILL_ANY_AGGRO_PED_FAIL_THE_MISSION())
		PRINTLN("[RCC MISSION][SpookAggro] PROCESS_SERVER_PED_AGGRO_SETTING_DATA - ciPED_BSTwo_AggroDoesntFail Not set. MC_serverBD.iPartCausingFail[",iTeam, "] = ", MC_serverBD.iPartCausingFail[iTeam])
		IF (iPartCausingFail >= 0)
		AND (MC_serverBD.iPartCausingFail[iTeam] = -1)
			MC_serverBD.iPartCausingFail[iTeam] = iPartCausingFail
		ENDIF
	ELSE
		PRINTLN("[RCC MISSION][SpookAggro] PROCESS_SERVER_PED_AGGRO_SETTING_DATA ciPED_BSTwo_AggroDoesntFail is set on ped ", iped)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,ciPED_BSThree_AggroFailHasDelay)
		IF NOT HAS_NET_TIMER_STARTED(MC_serverBD.tdAggroCountdownTimer[iTeam])
			REINIT_NET_TIMER(MC_serverBD.tdAggroCountdownTimer[iTeam])
		ENDIF
	ELSE
		SET_BIT(MC_serverBD.iServerBitSet1,SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_0 + iTeam)
		
		IF iTeam > -1 AND iTeam < FMMC_MAX_TEAMS
		AND MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE13_DISABLE_CCTV_CAMERAS_TRIGGERING)			
				PRINTLN("[RCC MISSION][SpookAggro] PROCESS_SERVER_PED_AGGRO_SETTING_DATA ped ", iped, " Setting SBBOOL7_CCTV_TEAM_AGGROED_PEDS")
				SET_BIT(MC_serverBD.iServerBitSet7, SBBOOL7_CCTV_TEAM_AGGROED_PEDS)
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroDoesntFail)
			//Fail!
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree, ciPED_BSThree_AggroFailHasDelay)
				IF MC_serverBD.iNumTeamRuleAttempts[iTeam] > g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamRuleAttempts
				OR g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamRuleAttempts = 1	
					IF NOT IS_BIT_SET(MC_serverBD.iPedSpookedByDeadBodyBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
						MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_PED_AGRO
						SET_TEAM_FAILED(iTeam,int_to_enum(eFailMissionEnum,enum_to_int(mFail_PED_AGRO_T0)+iTeam),false,MC_serverBD.iPartCausingFail[iTeam],iped)
					ELSE
						MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_PED_FOUND_BODY
						SET_TEAM_FAILED(iTeam,mFail_PED_FOUND_BODY)
					ENDIF
					
					SET_TEAM_OBJECTIVE_OUTCOME(iTeam,MC_serverBD_4.iCurrentHighestPriority[iTeam],FALSE)
					SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam,MC_serverBD_4.iCurrentHighestPriority[iTeam],MC_serverBD.iReasonForObjEnd[iTeam],TRUE)
					BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam],iTeam,mc_serverBD_4.iCurrentHighestPriority[iTeam],DID_TEAM_PASS_OBJECTIVE(iTeam,mc_serverBD_4.iCurrentHighestPriority[iTeam]),TRUE)
					MC_serverBD.iEntityCausingFail[iTeam] = iped
					PRINTLN("[RCC MISSION][SpookAggro] PROCESS_SERVER_PED_AGGRO_SETTING_DATA - MISSION FAILED FOR TEAM:",iTeam," BECAUSE PED AGGROED: ",iped)
				ELSE
					IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_RETRY_CURRENT_RULE_TEAM_0 + iTeam)
						MC_serverBD.iNumTeamRuleAttempts[iTeam]++
						IF MC_serverBD.iNumTeamRuleAttempts[iTeam] <= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamRuleAttempts
							CLEAR_BIT(MC_serverBD.iServerBitSet1,SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_0 + iTeam)
							CLEAR_BIT(MC_serverBD.iServerBitSet1,SBBOOL1_AGGRO_WILL_FAIL_FOR_TEAM_0 + iTeam)
							RESET_NET_TIMER(MC_serverBD.tdAggroCountdownTimer[iTeam])
							SET_BIT(MC_serverBD.iServerBitSet5, SBBOOL5_RETRY_CURRENT_RULE_TEAM_0 + iTeam)
							MC_serverBD.iAggroCountdownTimerSlack[iTeam] = 0
							MC_serverBD.iPartCausingFail[iTeam] = -1
							MC_serverBD.iEntityCausingFail[iteam] = -1
							PRINTLN("[RCC MISSION][SpookAggro] SETTING TEAM TO RESTART RULE. CURRENT ATTEMPTS - ", MC_serverBD.iNumTeamRuleAttempts[iTeam])
						ENDIF
					ENDIF
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC ALERT_ALL_AGGRO_PEDS(INT iTeam)
	
	INT iPed, iTeamLoop
	PED_INDEX tempPed
	
	FOR iPed = 0 TO (MC_serverBD.iNumPedCreated - 1)
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive,ciPED_BSFive_BlockAggroAlertBroadcast)
		AND NOT IS_BIT_SET(MC_serverBD.iPedAggroedBitset[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
		AND SHOULD_AGRO_FAIL_FOR_TEAM(iPed, iTeam)
		AND NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
		AND NOT  IS_BIT_SET( iPedHitByStunGun[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
		
			PRINTLN("[RCC MISSION][SpookAggro] ALERT_ALL_AGGRO_PEDS - Setting ped ",iPed," as spooked/aggroed")
			
			tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
			
			#IF IS_DEBUG_BUILD 
			IF NOT IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
				SET_PED_IS_SPOOKED_DEBUG(iPed, TRUE)
			ENDIF
			#ENDIF
			
			SET_BIT(iPedLocalSpookedBitset[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
			SET_BIT(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))			
			SET_BIT(MC_serverBD.iPedAggroedFromEventBitset[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
			BROADCAST_FMMC_PED_SPOOKED(iPed)
			
			//Trigger ped aggro stuff here to avoid recursion (don't want to call ALERT_ALL_AGGRO_PEDS again and end up in an endless loop)
			
			PROCESS_SERVER_PED_AGGRO_TASK_CHOOSING(tempPed, iped)
			
			FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
				IF (MC_serverBD_4.iCurrentHighestPriority[iTeamLoop] < FMMC_MAX_RULES)
				AND SHOULD_AGRO_FAIL_FOR_TEAM(iped,iTeamLoop)
					PROCESS_SERVER_PED_AGGRO_SETTING_DATA(iped, iTeamLoop)
				ENDIF
			ENDFOR
			
			IF NETWORK_HAS_CONTROL_OF_ENTITY(tempPed)
				TRIGGER_AGGRO_TASKS_ON_PED(tempPed, iPed, ciSPOOK_NONE, FALSE)
				IF NOT IS_BIT_SET(iPedLocalAggroedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					BROADCAST_FMMC_PED_AGGROED(iped, -2)
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(iPedLocalAggroedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					BROADCAST_FMMC_PED_AGGROED(iped, -3)
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD 
			IF NOT IS_BIT_SET(MC_serverBD.iPedAggroedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				SET_PED_IS_AGGROD_DEBUG(iPed, TRUE)
			ENDIF
			#ENDIF
			
			SET_BIT(iPedPerceptionBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			CLEAR_BIT(iPedHeardBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			SET_BIT(iPedLocalAggroedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			SET_BIT(MC_serverBD.iPedAggroedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))			
		ENDIF
	ENDFOR
	
ENDPROC

PROC SPOOK_LINKED_PED(INT iPed, INT iPartCausingFail)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSpookLinkedPed > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSpookLinkedPed < FMMC_MAX_PEDS
		INT iLinkedPed = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSpookLinkedPed
		IF NOT IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iLinkedPed)], GET_LONG_BITSET_BIT(iLinkedPed))
			IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iLinkedPed])
				#IF IS_DEBUG_BUILD 
				SET_PED_IS_SPOOKED_DEBUG(iPed, TRUE, ciSPOOK_PLAYER_CAR_CRASH)
				#ENDIF
				SET_BIT(iPedLocalSpookedBitset[GET_LONG_BITSET_INDEX(iLinkedPed)], GET_LONG_BITSET_BIT(iLinkedPed))
				SET_BIT(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iLinkedPed)], GET_LONG_BITSET_BIT(iLinkedPed))
				BROADCAST_FMMC_PED_SPOOKED(iLinkedPed, ciSPOOK_PLAYER_CAR_CRASH, iPartCausingFail)
				PRINTLN("[JS][SpookAggro] SPOOK_LINKED_PED - SPOOKING LINKED PED ", iLinkedPed, " from ped: ", iPed)
			ELSE
				PRINTLN("[JS][SpookAggro] SPOOK_LINKED_PED - LINKED PED IS DEAD OR DOESN'T EXIST ", iLinkedPed, " from ped: ", iPed)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC TRIGGER_AGGRO_SERVERDATA_ON_PED(PED_INDEX tempPed, INT iped, INT iPartCausingFail = -1)

	BOOL bInjured = IS_PED_INJURED(tempPed)
	
	IF bInjured
	AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_AggroWhenHurtOrKilled)
		EXIT
	ENDIF
	
	IF NOT bInjured
		PROCESS_SERVER_PED_AGGRO_TASK_CHOOSING(tempPed, iped)
	ENDIF

	#IF IS_DEBUG_BUILD 
	SET_PED_IS_AGGROD_DEBUG(iPed)
	#ENDIF
	SET_BIT(MC_serverBD.iPedAggroedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
	
	INT iTeamLoop
	FOR iTeamLoop = 0 TO (FMMC_MAX_TEAMS-1)
		IF (MC_serverBD_4.iCurrentHighestPriority[iTeamLoop] < FMMC_MAX_RULES)
		AND SHOULD_AGRO_FAIL_FOR_TEAM(iped,iTeamLoop)
			PROCESS_SERVER_PED_AGGRO_SETTING_DATA(iped, iTeamLoop, iPartCausingFail)
			SPOOK_LINKED_PED(iped, iPartCausingFail)
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,ciPED_BSThree_AggroFailHasDelay)
				//Alert all other aggro peds if creator setting tells us to:
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_AggroAlertsAllOtherAggroPeds)
					PRINTLN("[RCC MISSION][SpookAggro] Ped ",iped," aggroed, now alert all other aggro peds!")
					ALERT_ALL_AGGRO_PEDS(iTeamLoop)
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

PROC TRIGGER_SPEECH_AGGRO_SERVERDATA_ON_PED(PED_INDEX tempPed, INT iped, INT iPartCausingFail = -1)
	
	BOOL bInjured = IS_PED_INJURED(tempPed)
	
	IF bInjured
	AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_AggroWhenHurtOrKilled)
		EXIT
	ENDIF
	
	IF NOT bInjured
		PROCESS_SERVER_PED_SPEECH_AGGRO(tempPed, iped)
	ENDIF

	#IF IS_DEBUG_BUILD 
	SET_PED_IS_AGGROD_DEBUG(iPed)
	#ENDIF
	SET_BIT(MC_serverBD.iPedAggroedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))	
	
	INT iTeamLoop
	FOR iTeamLoop = 0 TO (FMMC_MAX_TEAMS-1)
		IF (MC_serverBD_4.iCurrentHighestPriority[iTeamLoop] < FMMC_MAX_RULES)
		AND SHOULD_AGRO_FAIL_FOR_TEAM(iped,iTeamLoop)
		AND NOT IS_BIT_SET(iPedHitByStunGun[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
			PROCESS_SERVER_PED_AGGRO_SETTING_DATA(iped, iTeamLoop, iPartCausingFail)
			
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,ciPED_BSThree_AggroFailHasDelay)
				//Alert all other aggro peds if creator setting tells us to:
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_AggroAlertsAllOtherAggroPeds)
					PRINTLN("[RCC MISSION][SpookAggro] Ped ",iped," aggroed, now alert all other aggro peds!")
					ALERT_ALL_AGGRO_PEDS(iTeamLoop)
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: CLIENT PED AGGRO INIT 1 (broadcasting the Event to the server) !
//
//************************************************************************************************************************************************************



/// PURPOSE: When the ped owner realises that their ped has been aggroed, they will run this function which will send out an Event to the server (or just
///    run the server function if the ped owner is also already the server)
PROC TRIGGER_AGGRO_ON_PED(PED_INDEX tempPed, INT iped, INT iPartCausingFail = -1)
	
	BOOL bServer = bIsLocalPlayerHost
	
	INT iBroadcastNum = iPartCausingFail
	
	PRINTLN("[RCC MISSION][SpookAggro] TRIGGER_AGGRO_ON_PED (bServer ",bServer,") - ped ",iped," has been aggroed")
	
	IF bServer
		
		TRIGGER_AGGRO_SERVERDATA_ON_PED(tempPed, iped, iPartCausingFail)
		
		IF iBroadcastNum != (-3)
			iBroadcastNum = -2 //-2 is a value which means that the server has already taken care of the stuff above, and the ped has already been tasked
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(iPedLocalAggroedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		BROADCAST_FMMC_PED_AGGROED(iped, iBroadcastNum)
	ENDIF
	
	SET_BIT(iPedPerceptionBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
	CLEAR_BIT(iPedHeardBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))	
	SET_BIT(iPedLocalAggroedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
	
ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: SERVER PED HUNTING / DEAD BODY RESPONSE INIT !
//
//************************************************************************************************************************************************************



PROC TRIGGER_SERVER_AGGRO_HUNTING_ON_PED(PED_INDEX ThisPed, INT iped, INT iPartCausingFail = -1)
	
	IF SHOULD_PED_BE_GIVEN_TASK(ThisPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
	OR NOT DOES_ANIMATION_NEED_SPECIAL_BREAKOUT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim)
		SET_PED_STATE(iped,ciTASK_HUNT_FOR_PLAYER)
	ELSE
		//Can't do the 'hunt for player' stuff if this ped needs to breakout of an anim first, just head straight into aggro:
		TRIGGER_AGGRO_ON_PED(ThisPed,iped,iPartCausingFail)
	ENDIF
	
ENDPROC

PROC TRIGGER_SERVER_AGGRO_DEAD_BODY_RESPONSE_ON_PED(PED_INDEX ThisPed, INT iped, INT iPartCausingFail = -1)
	
	SET_BIT(MC_serverBD.iPedSpookedByDeadBodyBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
	
	IF (SHOULD_PED_BE_GIVEN_TASK(ThisPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
	OR NOT DOES_ANIMATION_NEED_SPECIAL_BREAKOUT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim))
	AND NOT IS_PED_IN_ANY_VEHICLE(ThisPed)
		SET_PED_STATE(iped,ciTASK_FOUND_BODY_RESPONSE)
		
		//Finish task progress:
		MC_ServerBD_2.iAssociatedAction[iped] = ciACTION_NOTHING
		MC_serverBD.niTargetID[iped] = NULL
		MC_serverBD_2.iTargetID[iped] = -1
		SET_BIT(MC_serverBD.iPedAtGotoLocBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
	ELSE
		//Can't do the 'find a body' stuff if this ped needs to breakout of an anim first, just head straight into aggro:
		TRIGGER_AGGRO_ON_PED(ThisPed,iped,iPartCausingFail)
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_PED_SHUFFLE_TO_TURRET_SEAT_FROM_BEING_ALERTED(INT iPed, PED_INDEX ThisPed)
	RETURN ((IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetNine, ciPed_BSNine_Shuffle_To_Turret_On_Alert))
	AND (IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed)) OR IS_PED_IN_COMBAT(ThisPed)))	
ENDFUNC
	
FUNC BOOL SHOULD_PED_SPEED_UP_FROM_BEING_ALERTED(INT iPed, PED_INDEX ThisPed) 
	RETURN ((IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetNine, ciPed_BSNine_Speed_Override_On_Spooked_Alerted))
	AND (IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed)) OR IS_PED_IN_COMBAT(ThisPed)))
ENDFUNC

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: HAS PED BEEN SPOOKED / HAS PED BEEN SPOOKED BY AGGRO PLAYER (the actual checks for whether a ped has been spooked / aggroed) !
//
//************************************************************************************************************************************************************

FUNC BOOL ARE_ANY_PEDS_AGGROED()
	INT iPed = 0
	FOR iPed = 0 TO (MC_serverBD.iNumPedCreated - 1)
		IF IS_BIT_SET(MC_serverBD.iPedAggroedFromEventBitset[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
			PRINTLN("[RCC MISSION][SpookAggro] ARE_ANY_PEDS_AGGROED - iped: ", iped, " was aggro'd from an event. Setting to spawn aggro'd.")
			RETURN TRUE
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
FUNC STRING GET_SPOOK_AT_COORD_AND_RADIUS_DEBUG_NAME(INT iSpookID)
	SWITCH iSpookID
		CASE ciSpookCoordID_GENERIC					RETURN "GENERIC"
		CASE ciSpookCoordID_THERMITE_PLACED			RETURN "THERMITE_PLACED"
		CASE ciSpookCoordID_OBJECT_INTERACT_WITH	RETURN "OBJECT_INTERACT_WITH"
	ENDSWITCH

	RETURN "INVALID"
ENDFUNC
#ENDIF

PROC SPOOK_PEDS_AT_COORD_IN_RADIUS(VECTOR vCoord, FLOAT fRadius, INT iSpookID = ciSpookCoordID_GENERIC, ENTITY_INDEX entTarget = NULL)
	BOOL bAssigned
	INT iSpookIndex
	FOR iSpookIndex = 0 TO ciMAX_PED_SPOOK_COORDS-1
		// Stop special spooks from being assigned in this array twice.
		IF iSpookID = sPedSpookAtCoord[iSpookIndex].iPedSpookCoordID
		AND iSpookID != ciSpookCoordID_GENERIC
			PRINTLN("[LM][RCC MISSION][SpookAggro] SPOOK_PEDS_AT_COORD_IN_RADIUS - set to spook at range and coord with ID: ", GET_SPOOK_AT_COORD_AND_RADIUS_DEBUG_NAME(iSpookID), " but it already exists. Bailing.")
			BREAKLOOP
		ENDIF
		
		IF IS_VECTOR_ZERO(sPedSpookAtCoord[iSpookIndex].vPedSpookCoord)
			PRINTLN("[LM][RCC MISSION][SpookAggro] SPOOK_PEDS_AT_COORD_IN_RADIUS - set to spook at range and coord with ID: ", GET_SPOOK_AT_COORD_AND_RADIUS_DEBUG_NAME(iSpookID), " vCoord: ", vCoord, " fRadius: ", fRadius, " being assigned at index: ", iSpookIndex)
			sPedSpookAtCoord[iSpookIndex].vPedSpookCoord = vCoord
			sPedSpookAtCoord[iSpookIndex].fPedSpookRadius = fRadius
			sPedSpookAtCoord[iSpookIndex].iPedSpookCoordID = iSpookID
			sPedSpookAtCoord[iSpookIndex].entPedSpookTarget = entTarget
			START_NET_TIMER(sPedSpookAtCoord[iSpookIndex].tdPedSpookAtCoordTimer)
			bAssigned = TRUE
			BREAKLOOP
		ENDIF
	ENDFOR
	IF NOT bAssigned
		PRINTLN("[LM][RCC MISSION][SpookAggro] SPOOK_PEDS_AT_COORD_IN_RADIUS - with iSpookID: ", GET_SPOOK_AT_COORD_AND_RADIUS_DEBUG_NAME(iSpookID), " vCoord: ", vCoord, " fRadius: ", fRadius, " has not been assigned for some reason. Array Might be full.")
	ENDIF
ENDPROC

FUNC BOOL IS_PED_SPOOKED_AT_COORD_IN_RADIUS(INT iPed)
	PED_INDEX tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
	VEcTOR vPed = GET_ENTITY_COORDS(tempPed)
	FLOAT fDist
	IF NOT IS_PED_INJURED(tempPed)
		INT iSpookIndex
		FOR iSpookIndex = 0 TO ciMAX_PED_SPOOK_COORDS-1		
			IF NOT IS_VECTOR_ZERO(sPedSpookAtCoord[iSpookIndex].vPedSpookCoord)
				fDist = VDIST2(vPed, sPedSpookAtCoord[iSpookIndex].vPedSpookCoord)
				PRINTLN("[LM][RCC MISSION][SpookAggro] IS_PED_SPOOKED_AT_COORD_IN_RADIUS - iSpookIndex: ", iSpookIndex, " with iSpookID: ", GET_SPOOK_AT_COORD_AND_RADIUS_DEBUG_NAME(sPedSpookAtCoord[iSpookIndex].iPedSpookCoordID), " vCoord: ", sPedSpookAtCoord[iSpookIndex].vPedSpookCoord, " fRadius: ", sPedSpookAtCoord[iSpookIndex].fPedSpookRadius, " fDist: ", fDist)
				IF fDist < POW(sPedSpookAtCoord[iSpookIndex].fPedSpookRadius, 2.0)
					IF DOES_ENTITY_EXIST(sPedSpookAtCoord[iSpookIndex].entPedSpookTarget)
						PRINTLN("[LM][RCC MISSION][SpookAggro] IS_PED_SPOOKED_AT_COORD_IN_RADIUS - iSpookIndex: ", iSpookIndex, " Has an entity associated with it")
						IF NOT HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(iPed, tempPed, sPedSpookAtCoord[iSpookIndex].entPedSpookTarget, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)
							PRINTLN("[LM][RCC MISSION][SpookAggro] IS_PED_SPOOKED_AT_COORD_IN_RADIUS - iSpookIndex: ", iSpookIndex, " No clear LOS, returning FALSE.")
							RETURN FALSE
						ENDIF
					ENDIF
					
					RETURN TRUE
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC RESET_SPOOK_AT_COORD_IN_RADIUS()
	INT iSpookIndex
	FOR iSpookIndex = 0 TO ciMAX_PED_SPOOK_COORDS-1	
		IF HAS_NET_TIMER_STARTED(sPedSpookAtCoord[iSpookIndex].tdPedSpookAtCoordTimer)
			IF HAS_NET_TIMER_EXPIRED_READ_ONLY(sPedSpookAtCoord[iSpookIndex].tdPedSpookAtCoordTimer, 5000)
				sPedSpookAtCoord[iSpookIndex].vPedSpookCoord = <<0.0, 0.0, 0.0>>
				sPedSpookAtCoord[iSpookIndex].fPedSpookRadius = 0.0
				sPedSpookAtCoord[iSpookIndex].iPedSpookCoordID = ciSpookCoordID_GENERIC
				RESET_NET_TIMER(sPedSpookAtCoord[iSpookIndex].tdPedSpookAtCoordTimer)
				sPedSpookAtCoord[iSpookIndex].entPedSpookTarget = NULL				
				PRINTLN("[LM][RCC MISSION][SpookAggro] RESET_SPOOK_AT_COORD_IN_RADIUS - with iSpookID: ", GET_SPOOK_AT_COORD_AND_RADIUS_DEBUG_NAME(sPedSpookAtCoord[iSpookIndex].iPedSpookCoordID), " vCoord: ", sPedSpookAtCoord[iSpookIndex].vPedSpookCoord, " fRadius: ", sPedSpookAtCoord[iSpookIndex].fPedSpookRadius, " Removing from the Array.")
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

/// PURPOSE: This function checks if the current ped has been 'spooked' - bAggroCheat includes the aggro checks without the ped actually being an
///    aggro ped (this is used for the Cancel Tasks when Alarmed check)
FUNC BOOL HAS_PED_BEEN_SPOOKED(INT iped, BOOL bIgnoreGrouping = FALSE, BOOL bAggroCheat = FALSE, BOOL bVehicle = FALSE)
	
	PED_INDEX tempPed 			= NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
	INT iClosestPlayer 			= GET_NEAREST_PLAYER_TO_ENTITY(tempPed)
	PLAYER_INDEX ClosestPlayer 	= INT_TO_PLAYERINDEX(iClosestPlayer)
	PED_INDEX ClosestPlayerPed 	= GET_PLAYER_PED(ClosestPlayer)
	
	#IF IS_DEBUG_BUILD
		IF bHandcuff
			PRINTLN("[RCC MISSION][SpookAggro] [sc_handcuff]")
			PRINTLN("[RCC MISSION][SpookAggro] [sc_handcuff] HAS_PED_BEEN_SPOOKED - Called for  ped ",iped)
		ENDIF
	#ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwelve, ciPed_BSTwelve_AggroOnSpawnIfAggroAlertEventWasSentOut)
		IF IS_BIT_SET(iPedLocalCheckedSpawnedAggroed[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
		OR ARE_ANY_PEDS_AGGROED()
			PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - iPed: ", iPed, " Spawning into aggro'd iPedLocalSpawnedAggroed")		
			SET_BIT(iPedLocalCheckedSpawnedAggroed[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF (IS_BIT_SET(MC_ServerBD.iServerBitSet6, SBBOOL6_PLAYER_HAS_BEEN_SPOTTED_CCTV) OR IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_CCTV_SPOTTED_DEAD_PED_BODY))
	AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwelve, ciPed_BSTwelve_BlockThreatenEvents)
		PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - iped: ", iped, " a player was spotted by a CCTV camera")
		RETURN TRUE
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwelve, ciPed_BSTwelve_BlockThreatenEvents)
	AND IS_PED_SPOOKED_AT_COORD_IN_RADIUS(iPed)
		PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - iped: ", iped, " is spooked by special script event at Coord and Radius.")
		RETURN TRUE
	ENDIF
	
	IF IS_PED_INJURED(ClosestPlayerPed)
	OR (NOT NETWORK_IS_PLAYER_A_PARTICIPANT(ClosestPlayer))
		RETURN FALSE
	ENDIF
	
	PARTICIPANT_INDEX tempPart = NETWORK_GET_PARTICIPANT_INDEX(ClosestPlayer)
	
	IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
		RETURN FALSE
	ENDIF
	
	INT iClosestPart = NATIVE_TO_INT(tempPart)
	
	IF IS_ENTITY_DEAD(tempPed)
		RETURN FALSE
	ENDIF
	
	IF IS_PED_IN_A_CUTSCENE(iped)
		RETURN FALSE
	ENDIF
	
	VECTOR vPed = GET_ENTITY_COORDS( tempPed )
	VECTOR vClosestPlayer = GET_ENTITY_COORDS( ClosestPlayerPed, FALSE )
	
	IF IS_PED_A_COP_MC(iped)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_AggroOnWanted)
		IF MC_playerBD[iClosestPart].iWanted > 0
		OR MC_playerBD[iClosestPart].iFakeWanted > 0
			IF VDIST2( vPed, vClosestPlayer ) < 2500 //(50 * 50)
				IF IS_TARGET_PED_IN_PERCEPTION_AREA( tempPed, ClosestPlayerPed ) AND HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(iPed, tempPed, ClosestPlayerPed,SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)
					PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped spooked by closest player having a wanted rating: ",iped) 
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bVehicle
		
		VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(tempPed)
		
		IF NOT IS_VEHICLE_DRIVEABLE(tempVeh)
			PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped spooked by vehicle being undrivable: ",iped) 
			RETURN TRUE
		ENDIF
		
		IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(tempVeh, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON)
		OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(tempVeh,ClosestPlayerPed)
		OR IS_BIT_SET(iPedLocalReceivedEvent_VehicleDamageFromPlayer[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
		OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_VEHICLE_DAMAGE_WEAPON) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_VEHICLE_DAMAGE_WEAPON)
			PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped spooked by vehicle damage: ",iped)
			#IF IS_DEBUG_BUILD
			IF IS_BIT_SET(iPedLocalReceivedEvent_VehicleDamageFromPlayer[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped spooked by iPedLocalReceivedEvent_VehicleDamageFromPlayer set: ",iped)
				//Lukasz: Fix for B*2193459, need a faster way of detecting player and ped vehicle collision by checking if they touch
				//Don't clear this bit set here, it will be cleared in HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER()
				//CLEAR_BIT(iPedLocalReceivedEvent_VehicleDamageFromPlayer[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			ENDIF
			#ENDIF
			IF IS_BIT_SET(iPedLocalReceivedEvent_VehicleDamageFromPlayer[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_VEHICLE_DAMAGE_WEAPON) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_VEHICLE_DAMAGE_WEAPON)
				//Lukasz: Fix for B*2193873, stop friendly peds from being spooked by these events  
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iTeam[MC_playerBD[iClosestPart].iTeam] = ciPED_RELATION_SHIP_LIKE
					PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped in vehicle with ciPED_RELATION_SHIP_LIKE has iPedLocalReceivedEvent_VehicleDamageFromPlayer set, not spooking ped: ",iped)
					RETURN FALSE
				ENDIF
			ENDIF
			
			RETURN TRUE
		ENDIF
		
		IF IS_PED_ON_SPECIFIC_VEHICLE(ClosestPlayerPed, tempVeh)
			PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped spooked by player on vehicle: ",iped) 
			RETURN TRUE
		ENDIF
		
		IF HAS_PED_RECEIVED_EVENT(tempPed, EVENT_DRAGGED_OUT_CAR) 
		OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_PED_ON_CAR_ROOF)
		OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_PED_ENTERED_MY_VEHICLE) 
		OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_PED_JACKING_MY_VEHICLE)
		OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_DRAGGED_OUT_CAR)
		OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_PED_ON_CAR_ROOF)
		OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_PED_ENTERED_MY_VEHICLE)
		OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_PED_JACKING_MY_VEHICLE)
			PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped spooked by car jacking: ",iped)
			RETURN TRUE
		ENDIF
		
		IF IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(tempVeh)
			PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped spooked by car being attached to something: ",iped)
			RETURN TRUE
		ENDIF
		
//		IF IS_PED_EVASIVE_DIVING(tempPed, tempEntity) 
//			NET_PRINT("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped spooked by evasive dive: ") NET_PRINT_INT(iped) NET_NL()
//			RETURN TRUE
//		ENDIF
		
		//If we need to do any of the vehicle stopped checks:
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIncludeWeaponEquipped)
		OR (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedPerceptionSettings != ciPED_PERCEPTION_TOTALLY_BLIND)
		OR (NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIgnoreBlockingVehicleMovemt) )
			
			IF IS_VEHICLE_STOPPED(tempVeh)
				
				IF IS_ENTITY_IN_ANGLED_AREA(ClosestPlayerPed, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempVeh, <<0.0, 0.0, -1.5>>),
							 		  GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempVeh, <<0.0, 10.0, 3.0>>), 6.0)
					
					//Lukasz: Fix for B*2193873, stop friendly peds from being spooked by touching player and being blocked by player	  
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iTeam[MC_playerBD[iClosestPart].iTeam] = ciPED_RELATION_SHIP_LIKE
						PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped in vehicle with ciPED_RELATION_SHIP_LIKE is blocked by player, not spooking ped: ",iped)
						RETURN FALSE
					ENDIF
					
					//check if closest player has weapon equipped in front of the stopped ped vehicle
					WEAPON_TYPE eClosestPlayerWeapon
					
					IF IS_PED_SHOOTING(ClosestPlayerPed)
					OR ((g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedPerceptionSettings != ciPED_PERCEPTION_TOTALLY_BLIND)
						AND (IS_PLAYER_TARGETTING_ENTITY(ClosestPlayer, tempPed)
							OR IS_PLAYER_FREE_AIMING_AT_ENTITY(ClosestPlayer, tempPed) ) )
					OR ( IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIncludeWeaponEquipped)
						AND GET_CURRENT_PED_WEAPON(ClosestPlayerPed, eClosestPlayerWeapon) )
						
						IF NOT IS_CELLPHONE_CAMERA_IN_USE()
							
							#IF IS_DEBUG_BUILD
							IF IS_PED_SHOOTING(ClosestPlayerPed)
								PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped spooked by player shooting & vehicle stopped: ",iped)
							ENDIF
							IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedPerceptionSettings != ciPED_PERCEPTION_TOTALLY_BLIND)
								IF IS_PLAYER_TARGETTING_ENTITY(ClosestPlayer, tempPed)
									PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped spooked by player targetting them: ",iped)
								ENDIF
								IF IS_PLAYER_FREE_AIMING_AT_ENTITY(ClosestPlayer, tempPed)
									PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped spooked by player free aiming at them: ",iped)
								ENDIF
							ENDIF
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIncludeWeaponEquipped)
							AND GET_CURRENT_PED_WEAPON(ClosestPlayerPed, eClosestPlayerWeapon)
								PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped spooked by player having a weapon equipped: ",iped)
							ENDIF
							#ENDIF
							RETURN TRUE
						ENDIF
					ENDIF
					
					
					IF NOT IS_BIT_SET(iPedBlockedBitset[GET_LONG_BITSET_INDEX(iped)],GET_LONG_BITSET_BIT(iped))
						IF iPedVehStoppedTimer[iped] >= 5000
							PLAY_PED_AMBIENT_SPEECH_AND_CLONE_NATIVE(tempPed,"BLOCKED_GENERIC","SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
							SET_BIT(iPedBlockedBitset[GET_LONG_BITSET_INDEX(iped)],GET_LONG_BITSET_BIT(iped))
						ENDIF
					ENDIF
					
					IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIgnoreBlockingVehicleMovemt)
						//check if closest player is blocking the stopped ped car for some time
						iPedVehStoppedTimer[iped] += MC_serverBD.iMaxLoopSize * ROUND(fLastFrameTime * 1000)
						
						IF iPedVehStoppedTimer[iped] >= 12000
							PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped spooked by blocked time: ",iped) 
							RETURN TRUE
						ENDIF
					ENDIF
					
				ENDIF
				
			ELSE
				IF iPedVehStoppedTimer[iped] != 0
				AND GET_ENTITY_SPEED(tempVeh) > 5.0
					iPedVehStoppedTimer[iped] = 0
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF // End of vehicle checks
	
	//Only check for these if grouped up with the player, otherwise they'll be handled by combat stuff - bug 1881474
	BOOL bGrouped = (GET_PED_GROUP_INDEX(tempPed) = GET_PLAYER_GROUP(ClosestPlayer))
	IF (bIgnoreGrouping OR bGrouped)
	AND (bAggroCheat OR IS_PED_AN_AGGRO_PED(iped))
	AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwelve, ciPed_BSTwelve_BlockThreatenEvents)
		IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedPerceptionSettings != ciPED_PERCEPTION_TOTALLY_BLIND)
		AND (IS_PLAYER_FREE_AIMING_AT_ENTITY(ClosestPlayer,tempPed) OR IS_PLAYER_TARGETTING_ENTITY(ClosestPlayer,tempPed))
			IF (NOT IS_CELLPHONE_CAMERA_IN_USE())
			AND ((NOT bIgnoreGrouping) OR (IS_TARGET_PED_IN_PERCEPTION_AREA(tempPed,ClosestPlayerPed) AND HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(iPed, tempPed, ClosestPlayerPed,SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)))
				PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped ",iped," spooked by being aimed at by grouped player")
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF (IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST) AND bGrouped)
		OR NOT IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
			IF IS_PED_SHOOTING(ClosestPlayerPed)
				PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped ",iped," spooked by shots fired by grouped player")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF MC_serverBD_2.iCamObjDestroyed > 0
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedAggroWhenCCTVCamsDestroyed > 0
		IF MC_serverBD_2.iCamObjDestroyed >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedAggroWhenCCTVCamsDestroyed
			PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped ",iped," iCamObjDestroyed: ", MC_serverBD_2.iCamObjDestroyed, " is greater than equal to iPedAggroWhenCCTVCamsDestroyed: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedAggroWhenCCTVCamsDestroyed)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedPerceptionSettings != ciPED_PERCEPTION_TOTALLY_BLIND)
	AND HAS_PED_RECEIVED_EVENT(tempPed, EVENT_GUN_AIMED_AT) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_GUN_AIMED_AT)
		IF (NOT IS_CELLPHONE_CAMERA_IN_USE())
		AND ((NOT bIgnoreGrouping) OR (IS_TARGET_PED_IN_PERCEPTION_AREA(tempPed,ClosestPlayerPed) AND HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(iPed, tempPed, ClosestPlayerPed,SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)) )
			PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped spooked by gun aimed at: ",iped)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF ( NOT IS_ANIMATION_INTERRUPTIBLE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim) AND DOES_ANIMATION_NEED_SPECIAL_BREAKOUT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim) )
	OR ( MC_serverBD_2.iPedState[iped] = ciTASK_GOTO_COORDS AND NOT IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)) )
		IF NOT IS_BIT_SET(iPedLocalReceivedEvent_SeenMeleeActionOrPedKilled[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			IF HAS_PED_RECEIVED_EVENT(tempPed, EVENT_RESPONDED_TO_THREAT) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_RESPONDED_TO_THREAT)
			OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_INJURED_CRY_FOR_HELP) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_INJURED_CRY_FOR_HELP)
			OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_SHOCKING_SEEN_PED_KILLED) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_SHOCKING_SEEN_PED_KILLED)
			OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_SHOCKING_SEEN_MELEE_ACTION) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_SHOCKING_SEEN_MELEE_ACTION)
				IF NOT IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
				OR HAVE_CLEAR_LOS_TO_ANY_PLAYER_NEARBY(tempPed)
					PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped with non interruptible idle animation or active go to spooked by melee action seen: ",iped)
					RETURN TRUE
				ELSE
					PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped has received a code even for melee attacks but does not have Line of Sight of the melee action iPed: ",iped)
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped spooked by iPedLocalReceivedEvent_SeenMeleeActionOrPedKilled set: ",iped)
			CLEAR_BIT(iPedLocalReceivedEvent_SeenMeleeActionOrPedKilled[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwelve, ciPed_BSTwelve_BlockThreatenEvents)
		IF NOT IS_BIT_SET(iPedLocalReceivedEvent_ShotFiredORThreatResponse[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,ciPED_BSThree_StopShotFiredEventsAggroing)
				#IF IS_DEBUG_BUILD
					EVENT_NAMES eventReceived
					
					IF HAS_PED_RECEIVED_EVENT(tempPed, EVENT_SHOT_FIRED) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_SHOT_FIRED)
						eventReceived = EVENT_SHOT_FIRED
						PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped spooked by EVENT_SHOT_FIRED: ",iped)
					ELIF HAS_PED_RECEIVED_EVENT(tempPed, EVENT_SHOT_FIRED_WHIZZED_BY) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_SHOT_FIRED_WHIZZED_BY)
						eventReceived = EVENT_SHOT_FIRED_WHIZZED_BY
						PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped spooked by EVENT_SHOT_FIRED_WHIZZED_BY: ",iped)
					ELIF HAS_PED_RECEIVED_EVENT(tempPed, EVENT_SHOT_FIRED_BULLET_IMPACT) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_SHOT_FIRED_BULLET_IMPACT)
						eventReceived = EVENT_SHOT_FIRED_BULLET_IMPACT
						PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped spooked by EVENT_SHOT_FIRED_BULLET_IMPACT: ",iped)
					ENDIF
				#ENDIF
				
				IF HAS_PED_RECEIVED_EVENT(tempPed, EVENT_SHOT_FIRED) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_SHOT_FIRED)
				OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_SHOT_FIRED_WHIZZED_BY) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_SHOT_FIRED_WHIZZED_BY)
				OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_SHOT_FIRED_BULLET_IMPACT) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_SHOT_FIRED_BULLET_IMPACT)
					PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped spooked by shot fired: ",iped)
					#IF IS_DEBUG_BUILD
						VECTOR vGunshot
						IF GET_POS_FROM_FIRED_EVENT(tempPed, eventReceived, vGunshot)
							PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - gunshot from coord ", vGunshot)
						ELSE
							PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - GET_POS_FROM_FIRED_EVENT returned false, unable to find where shot came from")
						ENDIF
						PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - GET_PED_INDEX_FROM_FIRED_EVENT = ",GET_PED_INDEX_FROM_FIRED_EVENT(tempPed, eventReceived))
					#ENDIF
					RETURN TRUE
				ENDIF
			ELSE
				IF HAS_PED_RECEIVED_EVENT(tempPed, EVENT_RESPONDED_TO_THREAT) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_RESPONDED_TO_THREAT)
					PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped spooked by receiving event_responded_to_threat: ",iped)
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped spooked by iPedLocalReceivedEvent_ShotFiredORThreatResponse set: ",iped)
			CLEAR_BIT(iPedLocalReceivedEvent_ShotFiredORThreatResponse[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, vPed, 32.0)
	OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_EXPLOSION)	OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_EXPLOSION)
	OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_EXPLOSION_HEARD) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_EXPLOSION_HEARD)
	OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_POTENTIAL_BLAST) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_POTENTIAL_BLAST)
	OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_SHOCKING_EXPLOSION) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_SHOCKING_EXPLOSION)
		PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped spooked by explosion: ",iped) 
		RETURN TRUE
	ENDIF
	
	IF IS_PROJECTILE_TYPE_WITHIN_DISTANCE(vPed, WEAPONTYPE_DLC_FLAREGUN, 20.0)
		PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped spooked by flare: ",iped)
		RETURN TRUE
	ENDIF
		
	IF HAS_PED_RECEIVED_EVENT(tempPed, EVENT_POTENTIAL_GET_RUN_OVER) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_POTENTIAL_GET_RUN_OVER)
		INT iSearchFlags = VEHICLE_SEARCH_FLAG_RETURN_LAW_ENFORCER_VEHICLES + VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES + VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES + VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_GROUP_MEMBERS + VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_PLAYER + VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_DEAD_OR_DYING_PED + VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_SCRIPTED_TASK + VEHICLE_SEARCH_FLAG_ALLOW_LAW_ENFORCER_VEHICLES_WITH_WANTED_LEVEL
		VEHICLE_INDEX hitVeh = GET_CLOSEST_VEHICLE(vPed,20,DUMMY_MODEL_FOR_SCRIPT,iSearchFlags)
		IF DOES_ENTITY_EXIST(hitVeh)
			PED_INDEX hitPed = GET_PED_IN_VEHICLE_SEAT(hitVeh)
			PLAYER_INDEX playerInVeh
			
			IF NOT IS_PED_INJURED(hitPed)
				playerInVeh = NETWORK_GET_PLAYER_INDEX_FROM_PED(hitPed)
			ENDIF
			
			IF playerInVeh = INVALID_PLAYER_INDEX() //If there isn't a player in the driver's seat
				PED_INDEX lastPedInVeh = GET_LAST_PED_IN_VEHICLE_SEAT(hitVeh)
				IF NOT IS_PED_INJURED(lastPedInVeh)
					playerInVeh = NETWORK_GET_PLAYER_INDEX_FROM_PED(lastPedInVeh)
				ENDIF
			ENDIF
			
			IF playerInVeh != INVALID_PLAYER_INDEX()
			AND NETWORK_IS_PLAYER_ACTIVE(playerInVeh)
				
				PARTICIPANT_INDEX tempParticipant = NETWORK_GET_PARTICIPANT_INDEX(playerInVeh)
				
				IF NETWORK_IS_PARTICIPANT_ACTIVE(tempParticipant)
					
					INT iPart = NATIVE_TO_INT(tempParticipant)
					
					IF SHOULD_AGRO_FAIL_FOR_TEAM(iped,MC_playerBD[iPart].iteam)
					AND ( NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,(ciPED_BSThree_AggroIgnorePotentialRunoverT0 + MC_playerBD[iPart].iteam)) )
						PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped ",iped," spooked by evasive dive due to part ",iPart)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_ENTITY_TOUCHING_ENTITY(tempPed, ClosestPlayerPed)
		
		//Lukasz: Fix for B*2193873, stop friendly peds from being spooked by touching player and being blocked by player
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iTeam[MC_playerBD[iClosestPart].iTeam] = ciPED_RELATION_SHIP_LIKE
			IF IS_PED_IN_ANY_VEHICLE(tempPed, TRUE) OR IS_PED_IN_ANY_VEHICLE(ClosestPlayerPed, TRUE)
				//PRINTLN("[RCC MISSION] HAS_PED_BEEN_SPOOKED - player is touching ped in vehicle with ciPED_RELATION_SHIP_LIKE, not spooking ped: ",iped)
				RETURN FALSE
			ENDIF
		ENDIF	
	
		PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped spooked by player touching: ",iped)
		RETURN TRUE
	ELSE
		IF bVehicle
			IF iPedScratchVehTimer[iped] > 0
				IF iPedScratchVehTimer[iped] > 0
					iPedScratchVehTimer[iped] = CLAMP_INT(iPedScratchVehTimer[iped] - CEIL(5 * (fLastFrameTime*30) * MC_serverBD.iMaxLoopSize),0,1000)
				ENDIF
			ENDIF
		ELSE
			iPedScratchVehTimer[iped] = 0
		ENDIF
	ENDIF
	
	IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(tempPed, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON)
	OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(tempPed,ClosestPlayerPed)
		PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped spooked by player damage: ",iped) 
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroWithSound)
		
		FLOAT fDistance = GET_DISTANCE_BETWEEN_ENTITIES(ClosestPlayerPed,tempPed)
		
		IF (GET_PLAYER_CURRENT_STEALTH_NOISE(ClosestPlayer) >= (fDistance / 1.5))
			PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped ", iped, ", spooked by hearing player")
			RETURN TRUE
		ENDIF
		
		IF fDistance <= 50
		AND IS_PED_IN_ANY_VEHICLE(ClosestPlayerPed)
			VEHICLE_INDEX closestPlayerVeh = GET_VEHICLE_PED_IS_IN(ClosestPlayerPed)
			IF IS_HORN_ACTIVE(closestPlayerVeh)
			OR IS_VEHICLE_IN_BURNOUT(closestPlayerVeh)
				PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped ", iped, ", spooked by hearing car")
				RETURN TRUE
			ENDIF
		ENDIF
		
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,ciPED_BSThree_AggroOnSeeingDeadBody)
		IF IS_BIT_SET(iPedSeenDeadBodyBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped ", iped, ", spooked by seeing dead body")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF iClosestPart > -1
		#IF IS_DEBUG_BUILD
			IF bHandcuff
				PRINTLN("[RCC MISSION][SpookAggro] [sc_handcuff]")
				PRINTLN("[RCC MISSION][SpookAggro] [sc_handcuff] HAS_PED_BEEN_SPOOKED - Checking ped ",iped, " Closest part ", iClosestPart, " Which is player ", GET_PLAYER_NAME(ClosestPlayer))
			ENDIF
		#ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour, ciPED_BSFour_AggroIf_T0_DroppedOutofSecondaryAnim + MC_playerBD[iClosestPart].iteam)
			#IF IS_DEBUG_BUILD
				IF bHandcuff
					PRINTLN("[RCC MISSION][SpookAggro] [sc_handcuff]")
					PRINTLN("[RCC MISSION][SpookAggro] [sc_handcuff] HAS_PED_BEEN_SPOOKED - ciPED_BSFour_AggroIf_T0_DroppedOutofSecondaryAnim is set ped ",iped)
				ENDIF
			#ENDIF
			IF IS_BIT_SET(MC_playerBD[iClosestPart].iClientBitSet,PBBOOL_DROPPED_OUT_OF_SECONDARY_ANIM)
			AND IS_TARGET_PED_IN_PERCEPTION_AREA(tempPed,ClosestPlayerPed)
			AND HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(iPed, tempPed,ClosestPlayerPed, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)
				PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped spooked by closest player dropped out of secondary anim: ",iped)
				RETURN TRUE
			ELSE
				#IF IS_DEBUG_BUILD
					IF bHandcuff
						PRINTLN("[RCC MISSION][SpookAggro] [sc_handcuff] HAS_PED_BEEN_SPOOKED - Not spooked because... ",iped)
						
						IF NOT IS_BIT_SET(MC_playerBD[iClosestPart].iClientBitSet,PBBOOL_DROPPED_OUT_OF_SECONDARY_ANIM)
							PRINTLN("[RCC MISSION][SpookAggro] [sc_handcuff] HAS_PED_BEEN_SPOOKED - PBBOOL_DROPPED_OUT_OF_SECONDARY_ANIM not set!",iped)
						ENDIF
						
						IF NOT IS_TARGET_PED_IN_PERCEPTION_AREA(tempPed,ClosestPlayerPed)
							PRINTLN("[RCC MISSION][SpookAggro] [sc_handcuff] HAS_PED_BEEN_SPOOKED - IS_TARGET_PED_IN_PERCEPTION_AREA ",iped)
						ENDIF
						
						IF NOT HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(iPed, tempPed,ClosestPlayerPed, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)
							PRINTLN("[RCC MISSION][SpookAggro] [sc_handcuff] HAS_PED_BEEN_SPOOKED - HAS_ENTITY_CLEAR_LOS_TO_ENTITY ",iped)
						ENDIF
					ENDIF
				#ENDIF
			ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			IF bHandcuff
				PRINTLN("[RCC MISSION][SpookAggro] [sc_handcuff] HAS_PED_BEEN_SPOOKED - Checking ped ",iped, " Closest part is -1!")
			ENDIF
		#ENDIF
	ENDIF
		
	IF ((NOT bVehicle) AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIncludeWeaponEquipped))
		IF (NOT IS_PED_INJURED(ClosestPlayerPed))
		AND IS_TARGET_PED_IN_PERCEPTION_AREA(tempPed,ClosestPlayerPed)
		AND HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(iPed, tempPed, ClosestPlayerPed, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)
			//check if player has weapon equipped in front of the ped
			WEAPON_TYPE eWeapon
			IF GET_CURRENT_PED_WEAPON(ClosestPlayerPed, eWeapon)
				IF NOT IS_PED_IN_ANY_VEHICLE(ClosestPlayerPed)
					PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped spooked by closest player with weapon out: ",iped)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Part loop:
	IF (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour,ciPED_BSFour_AggroIf_T0_DroppedOutofSecondaryAnim)
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour,ciPED_BSFour_AggroIf_T1_DroppedOutofSecondaryAnim)
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour,ciPED_BSFour_AggroIf_T2_DroppedOutofSecondaryAnim)
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour,ciPED_BSFour_AggroIf_T3_DroppedOutofSecondaryAnim))
	OR ((NOT bVehicle) AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIncludeWeaponEquipped))
		
		IF iCurrentHeistMissionIndex = HBCA_BS_PRISON_BREAK_FINALE
			
			PLAYER_INDEX prisonerPlayer = INT_TO_PLAYERINDEX(GET_NEAREST_PLAYER_TO_ENTITY_ON_TEAM(tempPed, 0))
			
			IF IS_NET_PLAYER_OK(prisonerPlayer)
				
				PED_INDEX prisonerPed = GET_PLAYER_PED(prisonerPlayer)
				
				IF IS_TARGET_PED_IN_PERCEPTION_AREA(tempPed,prisonerPed)
				
					PLAYER_INDEX officerPlayer = INT_TO_PLAYERINDEX(GET_NEAREST_PLAYER_TO_ENTITY_ON_TEAM(tempPed, 2))
					
					IF IS_NET_PLAYER_OK(officerPlayer)
					AND (VDIST2(GET_ENTITY_COORDS(prisonerPed), GET_ENTITY_COORDS(GET_PLAYER_PED(officerPlayer))) > 64) // 8 metres
					AND HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(iPed, tempPed, prisonerPed, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)
						PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped spooked by prisoner getting too far away from officer: ",iped)
						RETURN TRUE
					ENDIF
					
				ENDIF
				
			ENDIF
		ENDIF
		
		WEAPON_TYPE eWeapon //Placed here rather than in FOR loop below to avoid 'value will persist across iterations' info annoyance when compiling
		INT iPartLoop
		
		INT iCount
		
		FOR iPartLoop = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartLoop))
				//Don't check closest player again
				IF (iPartLoop != iClosestPart)
				AND (bAggroCheat OR SHOULD_AGRO_FAIL_FOR_TEAM(iped,MC_playerBD[iPartLoop].iteam))
					PLAYER_INDEX TempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartLoop))
					PED_INDEX PlayerPed = GET_PLAYER_PED(TempPlayer)
					IF (NOT IS_PED_INJURED(PlayerPed))
					AND IS_TARGET_PED_IN_PERCEPTION_AREA(tempPed,PlayerPed)
					AND HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(iPed, tempPed, ClosestPlayerPed, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)
						
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour,ciPED_BSFour_AggroIf_T0_DroppedOutofSecondaryAnim + MC_playerBD[iPartLoop].iteam)
							IF IS_BIT_SET(MC_playerBD[iPartLoop].iClientBitSet,PBBOOL_DROPPED_OUT_OF_SECONDARY_ANIM)
								PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped ",iped," spooked by seeing part ",iPartLoop," not in secondary anim")
								RETURN TRUE
							ENDIF
						ENDIF
						
						IF (NOT bVehicle AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIncludeWeaponEquipped))
							//check if player has weapon equipped in front of the ped
							IF GET_CURRENT_PED_WEAPON(PlayerPed, eWeapon)
								IF NOT IS_PED_IN_ANY_VEHICLE(PlayerPed)
									PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped ",iped," spooked by seeing part ",iPartLoop," with weapon")
									RETURN TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				iCount++
			ENDIF
			
			IF iCount >= (MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3])
				iPartLoop = MAX_NUM_MC_PLAYERS
			ENDIF
		ENDFOR
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedAggroZone > -1
		IF IS_BIT_SET(iAlertPedsZoneBitSet, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedAggroZone)
			PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - iped: ", iped, " Spooked by entering Alert Zone: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedAggroZone)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iHackFailsForAggro != -1
		IF MC_ServerBD_4.iHackingFails >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iHackFailsForAggro
			PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED - ped ",iped," spooked by iHackingFails ",MC_ServerBD_4.iHackingFails," >= iHackFailsForAggro ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iHackFailsForAggro)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE: This function checks if the current ped has been 'aggroed' - iPartCausingFail returns the participant we guess to be doing the Aggro,
///    and iSpookedReason returns the reason the ped has been aggroed (an INT in the list of ciSPOOK_etc spook reasons)
FUNC BOOL HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER(INT iped, INT& iPartCausingFail, INT& iSpookedReason, BOOL bVehicle, INT iTeam)
	
	iPartCausingFail = -1
	iSpookedReason = ciSPOOK_NONE
	
	PED_INDEX tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
	FLOAT fDistance2
	INT iTeamOfPart
	INT iClosestAggroPart = GET_NEAREST_AGGRO_PART_TO_PED(tempPed, iped, fDistance2, iTeamOfPart)
		
	IF iClosestAggroPart = -1 //No players on an aggro rule on this ped!
		PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - no aggro players for ped ",iped)
		RETURN FALSE
	ELSE
		IF (fDistance2 >= 90000) //(300 * 300)
			PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - closest aggro player is over 300m away (don't bother checking) for ped ",iped)
			RETURN FALSE
		ENDIF
	ENDIF
	
	PLAYER_INDEX ClosestPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iClosestAggroPart))
	PED_INDEX ClosestPlayerPed 	= GET_PLAYER_PED(ClosestPlayer)
	
	IF IS_ENTITY_DEAD(tempPed)
		PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - ped ",iped," is dead")
		RETURN FALSE
	ENDIF
	
	VECTOR vPed = GET_ENTITY_COORDS(tempPed)
	
	IF IS_PED_A_COP_MC(iped)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_AggroOnWanted)
		IF MC_playerBD[iClosestAggroPart].iWanted > 0
		OR MC_playerBD[iClosestAggroPart].iFakeWanted > 0
			IF (fDistance2 < 2500) //(50 * 50)
				PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - ped ",iped," spooked by closest aggro part having a wanted rating")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF bVehicle
		
		VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(tempPed)
		
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(tempVeh,ClosestPlayerPed)
		OR IS_BIT_SET(iPedLocalReceivedEvent_VehicleDamageFromPlayer[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
		OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_VEHICLE_DAMAGE_WEAPON) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_VEHICLE_DAMAGE_WEAPON)
			CLEAR_BIT(iPedLocalReceivedEvent_VehicleDamageFromPlayer[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - ped ",iped," spooked by car crash with player via iPedLocalReceivedEvent_VehicleDamageFromPlayer set")
			iSpookedReason = ciSPOOK_PLAYER_CAR_CRASH
			RETURN TRUE
		ENDIF
		//Car jacking and ped on roof is handled in the IS_ENTITY_TOUCHING_ENTITY stuff below
		
		IF IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(tempVeh)
			ENTITY_INDEX attachEnt = GET_ENTITY_ATTACHED_TO(tempVeh)
			IF DOES_ENTITY_EXIST(attachEnt)
			AND IS_ENTITY_A_VEHICLE(attachEnt)
				VEHICLE_INDEX attachVeh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(attachEnt)
				
				PED_INDEX driverPed = GET_PED_IN_VEHICLE_SEAT(attachVeh)
				PLAYER_INDEX playerInVeh
				
				IF NOT IS_PED_INJURED(driverPed)
					playerInVeh = NETWORK_GET_PLAYER_INDEX_FROM_PED(driverPed)
				ENDIF
				
				IF playerInVeh = INVALID_PLAYER_INDEX() //If there isn't a player in the driver's seat
					PED_INDEX lastPedInSeat = GET_LAST_PED_IN_VEHICLE_SEAT(attachVeh)
					IF NOT IS_PED_INJURED(lastPedInSeat)
						playerInVeh = NETWORK_GET_PLAYER_INDEX_FROM_PED(lastPedInSeat)
					ENDIF
				ENDIF
				
				IF playerInVeh != INVALID_PLAYER_INDEX()
				AND NETWORK_IS_PLAYER_ACTIVE(playerInVeh)
					
					PARTICIPANT_INDEX tempPart = NETWORK_GET_PARTICIPANT_INDEX(playerInVeh)
					
					IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
						
						INT iPart = NATIVE_TO_INT(tempPart)
						
						IF SHOULD_AGRO_FAIL_FOR_TEAM(iped,MC_playerBD[iPart].iteam)
						AND SHOULD_TEAM_AGRO_FAIL_FOR_TOUCHING_PED(iped,MC_playerBD[iPart].iteam)
							PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - ped ",iped," spooked by vehicle attached to vehicle of part ",iPart)
							iPartCausingFail = iPart
							iSpookedReason = ciSPOOK_VEHICLE_ATTACHED
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
		
		//If we need to do any of the vehicle stopped checks:
		IF (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIncludeWeaponEquipped) )
		OR (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedPerceptionSettings != ciPED_PERCEPTION_TOTALLY_BLIND)
		OR (NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIgnoreBlockingVehicleMovemt) )
			IF IS_VEHICLE_STOPPED(tempVeh)
				
				IF IS_ENTITY_IN_ANGLED_AREA(ClosestPlayerPed, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempVeh, <<0.0, 0.0, -1.5>>),
													 		  GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempVeh, <<0.0, 10.0, 3.0>>), 6.0)
					
					//check if closest player has weapon equipped in front of the stopped ped vehicle
					WEAPON_TYPE eClosestPlayerWeapon
					
					IF IS_PED_SHOOTING(ClosestPlayerPed)
					OR ((g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedPerceptionSettings != ciPED_PERCEPTION_TOTALLY_BLIND)
						AND (IS_PLAYER_TARGETTING_ENTITY(ClosestPlayer, tempPed)
							OR IS_PLAYER_FREE_AIMING_AT_ENTITY(ClosestPlayer, tempPed) ) )
					OR ( IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIncludeWeaponEquipped)
						AND GET_CURRENT_PED_WEAPON(ClosestPlayerPed, eClosestPlayerWeapon) )
						
						IF NOT IS_CELLPHONE_CAMERA_IN_USE()
							
							#IF IS_DEBUG_BUILD
							IF IS_PED_SHOOTING(ClosestPlayerPed)
								PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - ped ",iped," spooked by part ",iClosestAggroPart," shooting & vehicle stopped")
							ENDIF
							IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedPerceptionSettings != ciPED_PERCEPTION_TOTALLY_BLIND)
								IF IS_PLAYER_TARGETTING_ENTITY(ClosestPlayer, tempPed)
									PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - ped ",iped," spooked by part ",iClosestAggroPart," targetting them")
								ENDIF
								IF IS_PLAYER_FREE_AIMING_AT_ENTITY(ClosestPlayer, tempPed)
									PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - ped ",iped," spooked by part ",iClosestAggroPart," free aiming at them")
								ENDIF
							ENDIF
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIncludeWeaponEquipped)
							AND GET_CURRENT_PED_WEAPON(ClosestPlayerPed, eClosestPlayerWeapon)
								PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - ped ",iped," spooked by part ",iClosestAggroPart," having a weapon equipped")
							ENDIF
							#ENDIF
							iPartCausingFail = iClosestAggroPart
							iSpookedReason = ciSPOOK_PLAYER_WITH_WEAPON
							RETURN TRUE
						ENDIF
					ENDIF
					
					IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIgnoreBlockingVehicleMovemt)
						//Check if closest player is blocking the stopped ped car for some time (this stuff is already incremented / reset in HAS_PED_BEEN_SPOOKED_BY_PLAYER)
						IF iPedVehStoppedTimer[iped] >= 12000
							PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - ped ",iped, " spooked by blocked time") 
							iPartCausingFail = iClosestAggroPart
							iSpookedReason = ciSPOOK_PLAYER_BLOCKED_VEHICLE
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	FLOAT fExplosionPerceptionRadius = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fExplosionPerceptionRadius	
	IF fExplosionPerceptionRadius > 0.0
		IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, vPed, fExplosionPerceptionRadius)
		OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_EXPLOSION)	OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_EXPLOSION)
		OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_EXPLOSION_HEARD) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_EXPLOSION_HEARD)
		OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_POTENTIAL_BLAST) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_POTENTIAL_BLAST)
		OR HAS_PED_RECEIVED_EVENT(tempPed, EVENT_SHOCKING_EXPLOSION) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_SHOCKING_EXPLOSION)
			
			ENTITY_INDEX expOwner = GET_OWNER_OF_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE,vPed + <<fExplosionPerceptionRadius,0,-fExplosionPerceptionRadius>>,vPed + <<-fExplosionPerceptionRadius,0,fExplosionPerceptionRadius>>,64)
			
			IF IS_ENTITY_A_PED(expOwner)
				PED_INDEX expPed = GET_PED_INDEX_FROM_ENTITY_INDEX(expOwner)
				IF IS_PED_A_PLAYER(expPed)
					
					PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(expPed)
					
					IF NETWORK_IS_PLAYER_ACTIVE(tempPlayer)
						
						PARTICIPANT_INDEX tempPart = NETWORK_GET_PARTICIPANT_INDEX(tempPlayer)
						
						IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
							
							INT iPart = NATIVE_TO_INT(tempPart)
							
							IF SHOULD_AGRO_FAIL_FOR_TEAM(iped,MC_playerBD[iPart].iteam)
								PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - ped ",iped," spooked by explosion due to part ", iPart, " fExplosionPerceptionRadius: ", fExplosionPerceptionRadius)
								iPartCausingFail = iPart
								iSpookedReason = ciSPOOK_PLAYER_CAUSED_EXPLOSION
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_PROJECTILE_TYPE_WITHIN_DISTANCE(vPed, WEAPONTYPE_DLC_FLAREGUN, 20.0)
		PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - ped ",iped," spooked by flare")
		iSpookedReason = ciSPOOK_SHOTS_FIRED
		RETURN TRUE
	ENDIF
	
	IF IS_PHONE_EMP_CURRENTLY_ACTIVE()
		PRINTLN("[RCC MISSION] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - ped ",iped," spooked by EMP")
		iSpookedReason = ciSPOOK_NONE
		RETURN TRUE
	ENDIF
	
	IF (HAS_PED_RECEIVED_EVENT(tempPed, EVENT_POTENTIAL_GET_RUN_OVER) OR IS_PED_RESPONDING_TO_EVENT(tempPed, EVENT_POTENTIAL_GET_RUN_OVER))
		INT iSearchFlags = VEHICLE_SEARCH_FLAG_RETURN_LAW_ENFORCER_VEHICLES + VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES + VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES + VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_GROUP_MEMBERS + VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_PLAYER + VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_DEAD_OR_DYING_PED + VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_SCRIPTED_TASK + VEHICLE_SEARCH_FLAG_ALLOW_LAW_ENFORCER_VEHICLES_WITH_WANTED_LEVEL
		VEHICLE_INDEX hitVeh = GET_CLOSEST_VEHICLE(vPed,20,DUMMY_MODEL_FOR_SCRIPT,iSearchFlags)
		IF DOES_ENTITY_EXIST(hitVeh)
			PED_INDEX hitPed = GET_PED_IN_VEHICLE_SEAT(hitVeh)
			PLAYER_INDEX playerInVeh
			
			IF NOT IS_PED_INJURED(hitPed)
				playerInVeh = NETWORK_GET_PLAYER_INDEX_FROM_PED(hitPed)
			ENDIF
			
			IF playerInVeh = INVALID_PLAYER_INDEX() //If there isn't a player in the driver's seat
				PED_INDEX lastPedInVeh = GET_LAST_PED_IN_VEHICLE_SEAT(hitVeh)
				IF NOT IS_PED_INJURED(lastPedInVeh)
					playerInVeh = NETWORK_GET_PLAYER_INDEX_FROM_PED(lastPedInVeh)
				ENDIF
			ENDIF
			
			IF playerInVeh != INVALID_PLAYER_INDEX()
			AND NETWORK_IS_PLAYER_ACTIVE(playerInVeh)
				
				PARTICIPANT_INDEX tempPart = NETWORK_GET_PARTICIPANT_INDEX(playerInVeh)
				
				IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
					
					INT iPart = NATIVE_TO_INT(tempPart)
					
					IF SHOULD_AGRO_FAIL_FOR_TEAM(iped,MC_playerBD[iPart].iteam)
					AND ( NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,(ciPED_BSThree_AggroIgnorePotentialRunoverT0 + MC_playerBD[iPart].iteam)) )
						PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - ped ",iped," spooked by evasive dive due to part ",iPart)
						iPartCausingFail = iPart
						iSpookedReason = ciSPOOK_PLAYER_POTENTIAL_RUN_OVER
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Only check for these if grouped up with the player, otherwise they'll be handled by combat stuff - bug 1881474
	IF (GET_PED_GROUP_INDEX(tempPed) = GET_PLAYER_GROUP(ClosestPlayer))
		IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedPerceptionSettings != ciPED_PERCEPTION_TOTALLY_BLIND)
		AND (IS_PLAYER_FREE_AIMING_AT_ENTITY(ClosestPlayer,tempPed) OR IS_PLAYER_TARGETTING_ENTITY(ClosestPlayer,tempPed))
			IF (NOT IS_CELLPHONE_CAMERA_IN_USE())
				PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - ped ",iped," spooked by being aimed at by grouped player ",iClosestAggroPart)
				iPartCausingFail = iClosestAggroPart
				iSpookedReason = ciSPOOK_PLAYER_AIMED_AT
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF IS_PED_SHOOTING(ClosestPlayerPed)
			PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - ped ",iped," spooked by shots fired by grouped player ",iClosestAggroPart)
			iPartCausingFail = iClosestAggroPart
			iSpookedReason = ciSPOOK_SHOTS_FIRED
			RETURN TRUE
		ENDIF
		
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(tempPed,ClosestPlayerPed)
			PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - ped ",iped," spooked by damage from grouped player ",iClosestAggroPart)
			iPartCausingFail = iClosestAggroPart
			iSpookedReason = ciSPOOK_DAMAGED
			RETURN TRUE
		ENDIF
	ENDIF
	
	//IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIgnoreTouching)
		IF NOT bVehicle
			IF IS_ENTITY_TOUCHING_ENTITY(tempPed, ClosestPlayerPed)
				IF SHOULD_AGRO_FAIL_FOR_TEAM(iped,iTeamOfPart)
				AND SHOULD_TEAM_AGRO_FAIL_FOR_TOUCHING_PED(iped,iTeamOfPart)
					PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - ped ",iped," spooked by part ",iClosestAggroPart," touching")
					iPartCausingFail = iClosestAggroPart
					iSpookedReason = ciSPOOK_PLAYER_TOUCHING_ME
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			
			IF IS_PED_IN_ANY_VEHICLE(ClosestPlayerPed)
				
				VEHICLE_INDEX hitVeh = GET_VEHICLE_PED_IS_IN(ClosestPlayerPed)
				
				PED_INDEX pedInSeat = GET_PED_IN_VEHICLE_SEAT(hitVeh)
				PLAYER_INDEX playerInVeh = INVALID_PLAYER_INDEX()
				IF NOT IS_PED_INJURED(pedInSeat)
					playerInVeh = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedInSeat)
				ENDIF
				
				IF playerInVeh = INVALID_PLAYER_INDEX() //If there isn't a player in the driver's seat
					PED_INDEX lastPedInSeat = GET_LAST_PED_IN_VEHICLE_SEAT(hitVeh)
					IF NOT IS_PED_INJURED(lastPedInSeat)
						playerInVeh = NETWORK_GET_PLAYER_INDEX_FROM_PED(lastPedInSeat)
					ENDIF
				ENDIF
				
				IF playerInVeh != INVALID_PLAYER_INDEX()
				AND NETWORK_IS_PLAYER_ACTIVE(playerInVeh)
					
					PARTICIPANT_INDEX tempPart = NETWORK_GET_PARTICIPANT_INDEX(playerInVeh)
					
					IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
					
						INT iPart = NATIVE_TO_INT(tempPart)
						
						IF SHOULD_AGRO_FAIL_FOR_TEAM(iped,MC_playerBD[iPart].iteam)
						AND SHOULD_TEAM_AGRO_FAIL_FOR_TOUCHING_PED(iped,MC_playerBD[iPart].iteam)

							IF IS_ENTITY_TOUCHING_ENTITY(hitVeh,GET_VEHICLE_PED_IS_IN(tempPed))
								VECTOR vPlayerVel = GET_ENTITY_VELOCITY(hitVeh)
								VECTOR vPedVel = GET_ENTITY_VELOCITY(GET_VEHICLE_PED_IS_IN(tempPed))
								
								//Speed diff in the direction of the ped's car
								FLOAT fSpeedDifference = ABSF(DOT_PRODUCT((vPlayerVel - vPedVel),vPedVel) / VMAG(vPedVel))
								
								IF fSpeedDifference >= 4
								OR (VMAG2(vPlayerVel - vPedVel) >= 25)
									PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - ped ",iped," spooked by car crash due to aggro player",iPart)
									iPartCausingFail = iPart
									iSpookedReason = ciSPOOK_PLAYER_CAR_CRASH
									RETURN TRUE
								ELSE
									// fLastFrameTime * 30 scales the thing by FPS, scaling it by 1 if we're running at 30 FPS
									iPedScratchVehTimer[iped] += FLOOR(((fSpeedDifference * 5) + 40) * (fLastFrameTime*30) ) * MC_serverBD.iMaxLoopSize
									
									IF iPedScratchVehTimer[iped] >= 150
										PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - ped ",iped," spooked by car scratch due to aggro player",iPart)
										iPartCausingFail = iPart
										iSpookedReason = ciSPOOK_PLAYER_CAR_CRASH
										RETURN TRUE
									ENDIF
								ENDIF
							ENDIF
							
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_ENTITY_TOUCHING_ENTITY(tempPed, ClosestPlayerPed)
				AND SHOULD_AGRO_FAIL_FOR_TEAM(iped,iTeamOfPart)
				AND SHOULD_TEAM_AGRO_FAIL_FOR_TOUCHING_PED(iped,iTeamOfPart)
					PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - ped ",iped," spooked by part ",iClosestAggroPart," touching vehicle")
					iPartCausingFail = iClosestAggroPart
					iSpookedReason = ciSPOOK_PLAYER_TOUCHING_MY_VEHICLE
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	//ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroWithSound)
		IF (GET_PLAYER_CURRENT_STEALTH_NOISE(ClosestPlayer) >= (SQRT(fDistance2) / 1.5))
			PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - ped ",iped," spooked by hearing part ",iClosestAggroPart)
			iPartCausingFail = iClosestAggroPart
			iSpookedReason = ciSPOOK_PLAYER_HEARD
			RETURN TRUE
		ENDIF
		
		IF fDistance2 <= 2500 // 50 * 50
		AND IS_PED_IN_ANY_VEHICLE(ClosestPlayerPed)
			VEHICLE_INDEX closestPlayerVeh = GET_VEHICLE_PED_IS_IN(ClosestPlayerPed)
			IF IS_HORN_ACTIVE(closestPlayerVeh)
			OR IS_VEHICLE_IN_BURNOUT(closestPlayerVeh)
				PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - ped ",iped," spooked by hearing car of part ",iClosestAggroPart)
				iPartCausingFail = iClosestAggroPart
				iSpookedReason = ciSPOOK_PLAYER_HEARD
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedAggroZone > -1
		IF IS_BIT_SET(iAlertPedsZoneBitSet, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedAggroZone)
			PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - Alert Zone: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedAggroZone, " returning TRUE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(MC_ServerBD.iServerBitSet6, SBBOOL6_PLAYER_HAS_BEEN_SPOTTED_CCTV)
	OR IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_CCTV_SPOTTED_DEAD_PED_BODY)
		PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - Spotted by CCTV Camera. MC_serverBD.iPartCausingFail[",iTeam,"] = ", MC_serverBD.iPartCausingFail[iTeam])
		
		IF MC_serverBD.iPartCausingFail[iTeam] != -1
			PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - Player spotted was ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(MC_serverBD.iPartCausingFail[iTeam])))
			iPartCausingFail = MC_serverBD.iPartCausingFail[iTeam]
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iHackFailsForAggro != -1
		IF MC_ServerBD_4.iHackingFails >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iHackFailsForAggro
			PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - ped ",iped," spooked by iHackingFails ",MC_ServerBD_4.iHackingFails," >= iHackFailsForAggro ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iHackFailsForAggro)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetNine, ciPed_BSNine_Check_For_Aggro_On_Spawn)
	AND IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed)))	
		PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - Spawning into aggro'd world state")
		RETURN TRUE
	ENDIF
	
	/*
	IF we wanted to check for torch shining at the ped, possible future implementation:
		IF IS_FLASH_LIGHT_ON(ClosestPlayerPed)
		AND ( GET_CLOCK_HOURS() >= 19 or GET_CLOCK_HOURS() < 6)
		AND fDistance2 <= 625 // 25 * 25
		AND (IS_PLAYER_TARGETTING_ENTITY(ClosestPlayer,tempPed) 
		  OR IS_PLAYER_FREE_AIMING_AT_ENTITY(ClosestPlayer,tempPed))
			PRINTLN("[RCC MISSION] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - ped ",iped," spooked by seeing torchlight of part ",iClosestAggroPart)
			iPartCausingFail = iClosestAggroPart
			iSpookedReason = ciSPOOK_TORCH_SEEN
			RETURN TRUE
		ENDIF
	ENDIF
	*/
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,ciPED_BSThree_AggroOnSeeingDeadBody)
		IF IS_BIT_SET(iPedSeenDeadBodyBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - ped ",iped," spooked by seeing dead body")
			iSpookedReason = ciSPOOK_SEEN_BODY
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour, ciPED_BSFour_AggroIf_T0_DroppedOutofSecondaryAnim + MC_playerBD[iClosestAggroPart].iteam)
		IF IS_BIT_SET(MC_playerBD[iClosestAggroPart].iClientBitSet,PBBOOL_DROPPED_OUT_OF_SECONDARY_ANIM)
		AND IS_TARGET_PED_IN_PERCEPTION_AREA(tempPed,ClosestPlayerPed)
		AND HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(iPed, tempPed, ClosestPlayerPed, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)
			PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - ped ",iped," spooked by seeing part ",iClosestAggroPart," not performing secondary anim")
			iPartCausingFail = iClosestAggroPart
			iSpookedReason = ciSPOOK_SEEN_PLAYER_NOT_IN_SECONDARY_ANIM
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF ((NOT bVehicle) AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIncludeWeaponEquipped))
		IF (NOT IS_PED_INJURED(ClosestPlayerPed))
		AND IS_TARGET_PED_IN_PERCEPTION_AREA(tempPed,ClosestPlayerPed)
		AND HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(iPed, tempPed, ClosestPlayerPed, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)
			//check if player has weapon equipped in front of the ped
			WEAPON_TYPE eWeapon
			IF GET_CURRENT_PED_WEAPON(ClosestPlayerPed, eWeapon)
				IF NOT IS_PED_IN_ANY_VEHICLE(ClosestPlayerPed)
					PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - ped ",iped," spooked by seeing part ",iClosestAggroPart," with weapon ", GET_WEAPON_NAME(eWeapon))
					iPartCausingFail = iClosestAggroPart
					iSpookedReason = ciSPOOK_PLAYER_WITH_WEAPON
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Part loop:
	IF (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour,ciPED_BSFour_AggroIf_T0_DroppedOutofSecondaryAnim)
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour,ciPED_BSFour_AggroIf_T1_DroppedOutofSecondaryAnim)
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour,ciPED_BSFour_AggroIf_T2_DroppedOutofSecondaryAnim)
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour,ciPED_BSFour_AggroIf_T3_DroppedOutofSecondaryAnim))
	OR ((NOT bVehicle) AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIncludeWeaponEquipped))
		
		IF iCurrentHeistMissionIndex = HBCA_BS_PRISON_BREAK_FINALE
			
			PLAYER_INDEX prisonerPlayer = INT_TO_PLAYERINDEX(GET_NEAREST_PLAYER_TO_ENTITY_ON_TEAM(tempPed, 0))
			
			IF IS_NET_PLAYER_OK(prisonerPlayer)
				
				PED_INDEX prisonerPed = GET_PLAYER_PED(prisonerPlayer)
				
				IF IS_TARGET_PED_IN_PERCEPTION_AREA(tempPed,prisonerPed)
					
					PLAYER_INDEX officerPlayer = INT_TO_PLAYERINDEX(GET_NEAREST_PLAYER_TO_ENTITY_ON_TEAM(tempPed, 2))
					
					IF IS_NET_PLAYER_OK(officerPlayer)
					AND (VDIST2(GET_ENTITY_COORDS(prisonerPed), GET_ENTITY_COORDS(GET_PLAYER_PED(officerPlayer))) > 64) // 8 metres
					AND HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(iPed, tempPed, prisonerPed, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)
						PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - ped ",iped," spooked by prisoner getting too far away from officer")
						iSpookedReason = ciSPOOK_SEEN_PLAYER_NOT_IN_SECONDARY_ANIM
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		WEAPON_TYPE eWeapon //Placed here rather than in FOR loop below to avoid 'value will persist across iterations' info annoyance when compiling
		INT iPartLoop
		
		INT iCount
		
		FOR iPartLoop = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartLoop))
				//Don't check closest player again
				IF (iPartLoop != iClosestAggroPart)
				AND SHOULD_AGRO_FAIL_FOR_TEAM(iped,MC_playerBD[iPartLoop].iteam)
					PLAYER_INDEX TempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartLoop))
					PED_INDEX PlayerPed = GET_PLAYER_PED(TempPlayer)
					IF (NOT IS_PED_INJURED(PlayerPed))
					AND IS_TARGET_PED_IN_PERCEPTION_AREA(tempPed,PlayerPed)
					AND HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(iPed, tempPed, PlayerPed, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)
						
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour,ciPED_BSFour_AggroIf_T0_DroppedOutofSecondaryAnim + MC_playerBD[iPartLoop].iteam)
							IF IS_BIT_SET(MC_playerBD[iPartLoop].iClientBitSet,PBBOOL_DROPPED_OUT_OF_SECONDARY_ANIM)
								PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - ped ",iped," spooked by seeing part ",iPartLoop," not performing secondary anim")
								iPartCausingFail = iPartLoop
								iSpookedReason = ciSPOOK_SEEN_PLAYER_NOT_IN_SECONDARY_ANIM
								RETURN TRUE
							ENDIF
						ENDIF
						
						IF ((NOT bVehicle) AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroIncludeWeaponEquipped))
							//check if player has weapon equipped in front of the ped
							IF GET_CURRENT_PED_WEAPON(PlayerPed, eWeapon)
								IF NOT IS_PED_IN_ANY_VEHICLE(PlayerPed)
									PRINTLN("[RCC MISSION][SpookAggro] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - ped ",iped," spooked by seeing part ",iPartLoop," with weapon ", GET_WEAPON_NAME(eWeapon))
									iPartCausingFail = iPartLoop
									iSpookedReason = ciSPOOK_PLAYER_WITH_WEAPON
									RETURN TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				iCount++
			ENDIF
			
			IF iCount >= (MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3])
				iPartLoop = MAX_NUM_MC_PLAYERS
			ENDIF
			
		ENDFOR
	ENDIF
	
	PRINTLN("[RCC MISSION] HAS_PED_BEEN_SPOOKED_BY_AGGRO_PLAYER - FALSE on ped ",iped)
	RETURN FALSE
	
ENDFUNC



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: SERVER PED TASKING / STATE CONTROL !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
FUNC BOOL HAVE_ALL_PEDS_TRIGGERED_PASS_RULE_CONSQ(INT iTeam, INT iPed)
	INT i
	FOR i = 0 TO MC_serverBD.iNumPedCreated-1
		IF i != iPed
			IF IS_BIT_SET(g_FMMC_STRUCT.iGoToPedCompletesRuleDoubleBS[iTeam][GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i))
				IF IS_TEAM_ON_RIGHT_RULE_TO_DO_PED_ARRIVAL_RESULTS(iTeam, i)
					IF NOT IS_BIT_SET(MC_serverBD.iPedAtGotoLocBitset[GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i))
					OR NOT IS_BIT_SET(MC_serverBD.iPedTasksArrivalResultsBS[GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i))	
						PRINTLN("[JS] HAVE_ALL_PEDS_TRIGGERED_PASS_RULE_CONSQ - Team ", iTeam, " is waiting on ped ", i)
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	PRINTLN("[JS] HAVE_ALL_PEDS_TRIGGERED_PASS_RULE_CONSQ - Team ", iTeam, " can progress")
	RETURN TRUE
ENDFUNC

PROC PROCESS_PED_FINISHED_GOTO_INTERRUPT(INT iPed)

	INT iTeam = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondaryTeam
	INT iRule = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSecondaryObjective // MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	PRINTLN("[RCC MISSION] PROCESS_PED_FINISHED_GOTO_INTERRUPT - Ped ", iPed, " has finished their goto interrupt!|| Rule: ", iRule, " for team ", iTeam)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwelve, ciPed_BSTwelve_PassRuleWhenFinishingSecondaryGoto)
		
		PRINTLN("[RCC MISSION] PROCESS_PED_FINISHED_GOTO_INTERRUPT - Passing rule ", iRule, " for team ", iTeam)
		
		IF MC_serverBD_4.iCurrentHighestPriority[iTeam] != iRule
			//Invalidate a future rule:
			PRINTLN("[RCC MISSION] PROCESS_PED_FINISHED_GOTO_INTERRUPT - Progress future ped rule objective for team ",iteam," because ped ",iped," finished GoTo, MC_serverBD_4.iCurrentHighestPriority[iTeam] ",MC_serverBD_4.iCurrentHighestPriority[iTeam],", iRule ", iRule)
			INVALIDATE_SINGLE_OBJECTIVE(iteam, iRule, TRUE)
			SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam, iRule, OBJ_END_REASON_PED_ARRIVED)
			BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(OBJ_END_REASON_PED_ARRIVED, iTeam, iRule, DID_TEAM_PASS_OBJECTIVE(iteam, iRule))
			RECALCULATE_OBJECTIVE_LOGIC(iteam)
		ELSE
			PRINTLN("[RCC MISSION] PROCESS_PED_FINISHED_GOTO_INTERRUPT - Progressing current rule")
			
			MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_PED_ARRIVED
			SET_TEAM_OBJECTIVE_OUTCOME(iTeam, iRule, TRUE)
			
			IF iRule < FMMC_MAX_RULES
				INCREMENT_SERVER_TEAM_SCORE(iteam, iRule,GET_FMMC_POINTS_FOR_TEAM(iteam, iRule))
			ENDIF
			SET_PROGRESS_OBJECTIVE_FOR_TEAM(iteam,TRUE)
			SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam, iRule, MC_serverBD.iReasonForObjEnd[iTeam])
			BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam],iTeam, iRule, TRUE) //DID_TEAM_PASS_OBJECTIVE(iteam, iRule))
			RECALCULATE_OBJECTIVE_LOGIC(iteam)
		ENDIF	
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwelve, ciPed_BSTwelve_FailMissionWhenFinishingSecondaryGoto)
	
		PRINTLN("[RCC MISSION] PROCESS_PED_FINISHED_GOTO_INTERRUPT - Failing mission on rule ", iRule, " for team ", iTeam)
		
		MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_PED_ARRIVED
		SET_TEAM_FAILED(iteam, mFail_PED_ARRIVED)
		MC_serverBD.iEntityCausingFail[iTeam] = iped
		SET_TEAM_OBJECTIVE_OUTCOME(iTeam, iRule, FALSE)
		SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam, iRule, MC_serverBD.iReasonForObjEnd[iTeam],TRUE)
		BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam], iTeam, iRule, DID_TEAM_PASS_OBJECTIVE(iteam, iRule), TRUE)
	ENDIF
	
ENDPROC


PROC MANAGE_PED_ARRIVAL_RESULTS(INT iped, BOOL bFinishingGoto)
	
	INT iteam
	INT iPedRule
	
	BOOL bCheckResults = FALSE
	
	BOOL bTriggerOnTaskCompletion = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetSix, ciPED_BSSix_PedArrivalResultsTriggerOnTaskCompletion)
	BOOL bPedTaskArrivalResult = IS_BIT_SET(MC_serverBD.iPedTasksArrivalResultsBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed)) // so we don't run this more than once
	
	IF bFinishingGoto	
		IF NOT bTriggerOnTaskCompletion
			bCheckResults = TRUE
		ENDIF
	ELSE
		IF bTriggerOnTaskCompletion
		AND NOT bPedTaskArrivalResult
			bCheckResults = TRUE
		ENDIF
	ENDIF
	
	PRINTLN("[RCC MISSION] MANAGE_PED_ARRIVAL_RESULTS - Processing for Ped: ", iPed)
	PRINTLN("[RCC MISSION] MANAGE_PED_ARRIVAL_RESULTS - 		bCheckResults: ", bCheckResults)
	PRINTLN("[RCC MISSION] MANAGE_PED_ARRIVAL_RESULTS - 		bFinishingGoTo: ", bFinishingGoTo)
	PRINTLN("[RCC MISSION] MANAGE_PED_ARRIVAL_RESULTS - 		bPedTaskArrivalResult: ", bPedTaskArrivalResult)
	PRINTLN("[RCC MISSION] MANAGE_PED_ARRIVAL_RESULTS - 		bTriggerOnTaskCompletion: ", bTriggerOnTaskCompletion)
	
	IF bCheckResults
	
		FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
			
			iPedRule = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iGotoProgressesRule[iteam]
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iCriticalGoToPedDoubleBS[iteam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			AND NOT IS_BIT_SET(MC_serverBD_4.iPedClearFailOnGoTo[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			
				PRINTLN("[RCC MISSION] MANAGE_PED_ARRIVAL_RESULTS - Looking for fail on go to complete, iTeam: ", iTeam, ", iped: ", iped)
				
				IF IS_TEAM_ON_RIGHT_RULE_TO_DO_PED_ARRIVAL_RESULTS(iTeam, iped, TRUE)
					MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_PED_ARRIVED
					SET_TEAM_FAILED(iteam,mFail_PED_ARRIVED)
					MC_serverBD.iEntityCausingFail[iTeam] = iped
					SET_TEAM_OBJECTIVE_OUTCOME(iTeam,MC_serverBD_4.iCurrentHighestPriority[iTeam],FALSE)
					SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam,MC_serverBD_4.iCurrentHighestPriority[iTeam],MC_serverBD.iReasonForObjEnd[iTeam],TRUE)
					BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam],iTeam,mc_serverBD_4.iCurrentHighestPriority[iTeam],DID_TEAM_PASS_OBJECTIVE(iteam,mc_serverBD_4.iCurrentHighestPriority[iTeam]),TRUE)
					
					PRINTLN("[RCC MISSION] MANAGE_PED_ARRIVAL_RESULTS - MISSION FAILED FOR TEAM:",iteam," because ped ",iped," arrived at goto loc, mc_serverBD_4.iCurrentHighestPriority[iTeam] ",mc_serverBD_4.iCurrentHighestPriority[iTeam],", iPedRule ",iPedRule)
				ENDIF
				
			ELIF IS_BIT_SET(g_FMMC_STRUCT.iGoToPedCompletesRuleDoubleBS[iTeam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			
				PRINTLN("[RCC MISSION] MANAGE_PED_ARRIVAL_RESULTS - Looking for pass on go to complete, iTeam: ", iTeam, ", iped: ", iped)
				
				IF IS_TEAM_ON_RIGHT_RULE_TO_DO_PED_ARRIVAL_RESULTS(iTeam, iped)
					IF MC_serverBD_4.iCurrentHighestPriority[iteam] < FMMC_MAX_RULES
					AND ((IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE8_WAIT_FOR_ALL_PEDS_CONSQ) 
						AND HAVE_ALL_PEDS_TRIGGERED_PASS_RULE_CONSQ(iTeam, iPed))
					OR NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE8_WAIT_FOR_ALL_PEDS_CONSQ))
						IF iPedRule != -1
						AND MC_serverBD_4.iCurrentHighestPriority[iTeam] != iPedRule
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetSix, ciPED_BSSix_GotoProgressesRule_InvalidateFutureRule_T0 + iTeam)
								//Invalidate a future rule:
								PRINTLN("[RCC MISSION] MANAGE_PED_ARRIVAL_RESULTS - Progress future ped rule objective for team ",iteam," because ped ",iped," finished GoTo, MC_serverBD_4.iCurrentHighestPriority[iTeam] ",MC_serverBD_4.iCurrentHighestPriority[iTeam],", iPedRule ",iPedRule)
								INVALIDATE_SINGLE_OBJECTIVE(iteam, iPedRule, TRUE)
								SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam, iPedRule, OBJ_END_REASON_PED_ARRIVED)
								BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(OBJ_END_REASON_PED_ARRIVED, iTeam, iPedRule,DID_TEAM_PASS_OBJECTIVE(iteam, iPedRule))
								RECALCULATE_OBJECTIVE_LOGIC(iteam)
							ELSE //IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetSix, ciPED_BSSix_GotoProgressesRule_InvalidateAllRulesUpToAndIncl_T0 + iTeam)
								//Invalidate all rules up to & including:
								PRINTLN("[RCC MISSION] MANAGE_PED_ARRIVAL_RESULTS - Progress objectives up to & incl ped rule for team ",iteam," because ped ",iped," finished GoTo, MC_serverBD_4.iCurrentHighestPriority[iTeam] ",MC_serverBD_4.iCurrentHighestPriority[iTeam],", iPedRule ",iPedRule)
								INVALIDATE_UP_TO_AND_INCLUDING_OBJECTIVE(iteam, iPedRule, TRUE)
								MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_PED_ARRIVED
								SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam, iPedRule, MC_serverBD.iReasonForObjEnd[iTeam])
								BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam], iTeam, iPedRule,DID_TEAM_PASS_OBJECTIVE(iteam, iPedRule))
								RECALCULATE_OBJECTIVE_LOGIC(iteam)
							ENDIF
						ELSE
							// Only on rule:
							PRINTLN("[RCC MISSION] MANAGE_PED_ARRIVAL_RESULTS - Progress current objective for team ",iteam," because ped ",iped," finished GoTo, MC_serverBD_4.iCurrentHighestPriority[iTeam] ",MC_serverBD_4.iCurrentHighestPriority[iTeam],", iPedRule ",iPedRule)
							MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_PED_ARRIVED
							SET_TEAM_OBJECTIVE_OUTCOME(iTeam,MC_serverBD_4.iCurrentHighestPriority[iTeam],TRUE)
							IF mc_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
								INCREMENT_SERVER_TEAM_SCORE(iteam,MC_serverBD_4.iCurrentHighestPriority[iTeam],GET_FMMC_POINTS_FOR_TEAM(iteam,MC_serverBD_4.iCurrentHighestPriority[iTeam]))
							ENDIF
							SET_PROGRESS_OBJECTIVE_FOR_TEAM(iteam,TRUE)
							SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam,MC_serverBD_4.iCurrentHighestPriority[iTeam],MC_serverBD.iReasonForObjEnd[iTeam])
							BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam],iTeam,mc_serverBD_4.iCurrentHighestPriority[iTeam],DID_TEAM_PASS_OBJECTIVE(iteam,mc_serverBD_4.iCurrentHighestPriority[iTeam]))
							RECALCULATE_OBJECTIVE_LOGIC(iteam)
						ENDIF
					ENDIF
					
				ENDIF
				
			ELSE
				PRINTLN("[RCC MISSION] MANAGE_PED_ARRIVAL_RESULTS - No pass/fail on go to complete for iTeam: ", iTeam, ", iped: ", iped)
				PRINTLN("[RCC MISSION] MANAGE_PED_ARRIVAL_RESULTS - 		GoTo complete Fail: ", IS_BIT_SET(g_FMMC_STRUCT.iCriticalGoToPedDoubleBS[iteam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
				PRINTLN("[RCC MISSION] MANAGE_PED_ARRIVAL_RESULTS - 		GoTo complete Clear Fail: ", IS_BIT_SET(MC_serverBD_4.iPedClearFailOnGoTo[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
				PRINTLN("[RCC MISSION] MANAGE_PED_ARRIVAL_RESULTS - 		GoTo complete Pass Rule: ", IS_BIT_SET(g_FMMC_STRUCT.iGoToPedCompletesRuleDoubleBS[iTeam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
			ENDIF
		ENDFOR	
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iPedSecondaryTaskActive[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		IF NOT IS_BIT_SET(iPedCompletedSecondaryGotoBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			PROCESS_PED_FINISHED_GOTO_INTERRUPT(iPed)
			SET_BIT(iPedCompletedSecondaryGotoBS[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iPedSecondaryTaskActive[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPed_BSEight_DeactivateSecondaryTaskWhenCompleted)
		SET_BIT(MC_serverBD.iPedSecondaryTaskCompleted[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		CLEAR_BIT(MC_serverBD.iPedSecondaryTaskActive[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		MC_serverBD.TargetPlayerID[iped] = INVALID_PLAYER_INDEX()
		MC_serverBD.niTargetID[iped] = NULL
		SET_PED_STATE(iped,ciTASK_CHOOSE_NEW_TASK)
		PRINTLN("[RCC MISSION] MANAGE_PED_ARRIVAL_RESULTS - Deactivating secondary task, ped: ", iPed)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThirteen, ciPED_BSThirteen_AllowRappellingOnGoToArrival)
		SET_BIT(MC_serverBD.iServerBitSet8, SBBOOL8_ALLOW_RAPPELLING_FROM_HELICOPTERS)
		PRINTLN("[RCC MISSION] MANAGE_PED_ARRIVAL_RESULTS - Setting MC_serverBD.iServerBitSet8, SBBOOL8_ALLOW_RAPPELLING_FROM_HELICOPTERS")
	ENDIF
	
	IF bFinishingGoto
		SET_BIT(MC_serverBD.iPedAtGotoLocBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
	ELSE
		SET_BIT(MC_serverBD.iPedTasksArrivalResultsBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
	ENDIF
	
ENDPROC

FUNC VEHICLE_SEAT GET_PED_PREFERRED_SEAT_FROM_CREATOR( INT iPedIndex )
	VEHICLE_SEAT vsToReturn = VS_ANY_PASSENGER
	
	SWITCH( g_FMMC_STRUCT_ENTITIES.sPlacedPed[ iPedIndex ]. iVehicleSeatPreference )
		CASE ciFMMC_SeatPreference_None
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] GET_PED_PREFERRED_SEAT_FROM_CREATOR - Ped preferred seat is any passneger" )
		BREAK
		
		CASE ciFMMC_SeatPreference_Driver
			vsToReturn = VS_DRIVER
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] GET_PED_PREFERRED_SEAT_FROM_CREATOR - Ped preferred seat is driver" )
		BREAK
		
		CASE ciFMMC_SeatPreference_Passenger
		CASE ciFMMC_SeatPreference_Front_Seats
			vsToReturn = VS_FRONT_RIGHT
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] GET_PED_PREFERRED_SEAT_FROM_CREATOR - Ped preferred seat is front passenger" )
		BREAK
		
		CASE ciFMMC_SeatPreference_Rear_Left
			vsToReturn = VS_BACK_LEFT
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] GET_PED_PREFERRED_SEAT_FROM_CREATOR - Ped preferred seat is back left" )
		BREAK
		
		CASE ciFMMC_SeatPreference_Rear_Right
			vsToReturn = VS_BACK_RIGHT
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] GET_PED_PREFERRED_SEAT_FROM_CREATOR - Ped preferred seat is back right" )
		BREAK
	ENDSWITCH
	
	RETURN vsToReturn
ENDFUNC

FUNC VEHICLE_SEAT GET_VEHICLE_SEAT_PED_SHOULD_ENTER(VEHICLE_INDEX tempVeh, INT iped #IF IS_DEBUG_BUILD , BOOL bPrints = FALSE #ENDIF )
	
	VEHICLE_SEAT vsSeatToUse = VS_ANY_PASSENGER
	BOOL bUseSeatPreference
	
	IF ( g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicleSeatPreference > ciFMMC_SeatPreference_Driver )
		bUseSeatPreference = TRUE
		vsSeatToUse = GET_PED_PREFERRED_SEAT_FROM_CREATOR(iPed)
		
		#IF IS_DEBUG_BUILD
		IF bPrints
			PRINTLN("[RCC MISSION] GET_VEHICLE_SEAT_PED_SHOULD_ENTER - Seat preference for ped ",iped," from the creator as ",ENUM_TO_INT(vsSeatToUse))
		ENDIF
		#ENDIF
		
		IF NOT IS_VEHICLE_SEAT_FREE(tempveh, vsSeatToUse)
			PED_INDEX VehPed = GET_PED_IN_VEHICLE_SEAT(tempveh, vsSeatToUse)
			IF NOT IS_PED_INJURED(VehPed)
				#IF IS_DEBUG_BUILD
				IF bPrints
					PRINTLN("[RCC MISSION] GET_VEHICLE_SEAT_PED_SHOULD_ENTER - Creator seat preference is already taken, find first free seat...")
				ENDIF
				#ENDIF
				
				bUseSeatPreference = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bUseSeatPreference
		vsSeatToUse = GET_FIRST_FREE_VEHICLE_SEAT_RC(tempVeh, TRUE, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicleSeatPreference != ciFMMC_SeatPreference_Driver)
		
		#IF IS_DEBUG_BUILD
		IF bPrints
			PRINTLN("[RCC MISSION] GET_VEHICLE_SEAT_PED_SHOULD_ENTER - First free vehicle seat for ped ",iped," found as ",ENUM_TO_INT(targetseat[iped]))
		ENDIF
		#ENDIF
	ENDIF
	
	RETURN vsSeatToUse
	
ENDFUNC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: PED TARGET CHOOSING (chooses what other entity the ped should focus on) !
//
//************************************************************************************************************************************************************



FUNC BOOL IS_ENTITY_WITHIN_DROP_OFF_FLEE_RANGE_FOR_PED(ENTITY_INDEX tempEnt, INT iPedChecking, INT iFleeDistance = 150, INT iEntityType = ciRULE_TYPE_NONE, INT iEntityID = -1 #IF IS_DEBUG_BUILD, BOOL bDoDebug = FALSE #ENDIF)
	
	BOOL bInDropOff = FALSE
	INT iTeamLoop
	BOOL bDropOffOverride
	VECTOR vDropOff
	
	#IF IS_DEBUG_BUILD
		BOOL bDebugInDropOff 
		FLOAT fDist
		VECTOR vEntCoords
		IF bDoDebug
			PRINTLN("[RCC MISSION] IS_ENTITY_WITHIN_DROP_OFF_FLEE_RANGE_FOR_PED called on entity type ",iEntityType," and ID ",iEntityID," with iPedChecking = ", iPedChecking, " iFleeDistance = ", iFleeDistance, "; Callstack:")
			DEBUG_PRINTCALLSTACK()
		ENDIF
	#ENDIF
	
	SWITCH iEntityType
		CASE ciRULE_TYPE_VEHICLE
			IF iEntityID >= 0
			AND iEntityID < FMMC_MAX_VEHICLES
				IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntityID].vDropOffOverride)
					vDropOff = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntityID].vDropOffOverride
					bDropOffOverride = TRUE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedChecking].iTeam[iTeamLoop] = ciPED_RELATION_SHIP_DISLIKE
		OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedChecking].iTeam[iTeamLoop] = ciPED_RELATION_SHIP_HATE
			
			IF NOT bDropOffOverride
				vDropOff = GET_DROP_OFF_CENTER_FOR_TEAM(DEFAULT, iTeamLoop)
			ENDIF
			
			IF (MC_serverBD_4.iCurrentHighestPriority[iTeamLoop] < FMMC_MAX_RULES)
			AND ( IS_ENTITY_IN_DROP_OFF_AREA(tempEnt, iTeamLoop, MC_serverBD_4.iCurrentHighestPriority[iTeamLoop], iEntityType, iEntityID)
				OR (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(tempEnt, vDropOff) < iFleeDistance) )
				
				#IF IS_DEBUG_BUILD
					IF bDoDebug
						IF (MC_serverBD_4.iCurrentHighestPriority[iTeamLoop] < FMMC_MAX_RULES)
							bDebugInDropOff = IS_ENTITY_IN_DROP_OFF_AREA(tempEnt, iTeamLoop, MC_serverBD_4.iCurrentHighestPriority[iTeamLoop], iEntityType, iEntityID)
							fDist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(tempEnt, vDropOff) 
							vEntCoords = GET_ENTITY_COORDS(tempEnt)
							PRINTLN("[RCC MISSION] IS_ENTITY_WITHIN_DROP_OFF_FLEE_RANGE_FOR_PED returning TRUE for iPedChecking = ", iPedChecking, " iTeamLoop = ", iTeamLoop, " IS_ENTITY_IN_DROP_OFF_AREA = ", bDebugInDropOff, " Dist from drop off = ", fDist, " Drop off centre = ", vDropOff, " Entity coords = ", vEntCoords)
						ENDIF
					ENDIF
				#ENDIF
				
				bInDropOff = TRUE
				iTeamLoop = MC_serverBD.iNumberOfTeams // Break out!
				
			ELSE
				#IF IS_DEBUG_BUILD
					IF bDoDebug
						IF (MC_serverBD_4.iCurrentHighestPriority[iTeamLoop] < FMMC_MAX_RULES)
							bDebugInDropOff = IS_ENTITY_IN_DROP_OFF_AREA(tempEnt, iTeamLoop, MC_serverBD_4.iCurrentHighestPriority[iTeamLoop], iEntityType, iEntityID)
							fDist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(tempEnt, vDropOff) 
							vEntCoords = GET_ENTITY_COORDS(tempEnt)
							PRINTLN("[RCC MISSION] IS_ENTITY_WITHIN_DROP_OFF_FLEE_RANGE_FOR_PED failing drop off check for iPedChecking = ", iPedChecking, " iTeamLoop = ", iTeamLoop, " IS_ENTITY_IN_DROP_OFF_AREA = ", bDebugInDropOff, " Dist from drop off = ", fDist, " Drop off centre = ", vDropOff, " Entity coords = ", vEntCoords)
						ENDIF
					ENDIF
				#ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	#IF IS_DEBUG_BUILD
		IF bDoDebug
			PRINTLN("[RCC MISSION] IS_ENTITY_WITHIN_DROP_OFF_FLEE_RANGE_FOR_PED iPedChecking = ", iPedChecking, " returning ", bInDropOff)
		ENDIF
	#ENDIF
	
	RETURN bInDropOff
	
ENDFUNC

FUNC NETWORK_INDEX GET_PED_CUSTOM_TARGET_NETWORK_ID(INT iped, INT iType, INT iID, INT iAction)
	
	INT iFleeFromDropOffDist = 150
	PED_INDEX ThisPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
	NETWORK_INDEX niTarget = NULL
	
	IF IS_PED_IN_ANY_HELI(ThisPed) OR IS_PED_IN_ANY_PLANE(ThisPed)
		iFleeFromDropOffDist = 350
	ENDIF
	
	PRINTLN("[RCC MISSION] GET_PED_CUSTOM_TARGET_NETWORK_ID - Ped ",iped,", iType", iType,", iID", iID,", iAction", iAction)
	
	SWITCH iType
		CASE ciRULE_TYPE_PED
			IF iID < FMMC_MAX_PEDS
				niTarget = MC_serverBD_1.sFMMC_SBD.niPed[iID]
								
				IF NETWORK_DOES_NETWORK_ID_EXIST(niTarget)
					PED_INDEX tempPed
					tempPed = NET_TO_PED(niTarget)
					
					IF NOT IS_PED_INJURED(tempPed)
					AND ( IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_DontFleeDropOffs) OR (NOT IS_ENTITY_WITHIN_DROP_OFF_FLEE_RANGE_FOR_PED(tempPed, iped, iFleeFromDropOffDist, ciRULE_TYPE_PED, iID)) )
						RETURN niTarget
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ciRULE_TYPE_VEHICLE
			IF iID < FMMC_MAX_VEHICLES
				niTarget = MC_serverBD_1.sFMMC_SBD.niVehicle[iID]
				
				IF NETWORK_DOES_NETWORK_ID_EXIST(niTarget)
					
					VEHICLE_INDEX tempVeh
					tempVeh = NET_TO_VEH(niTarget)
					
					IF IS_VEHICLE_DRIVEABLE(tempVeh)
					AND ( IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_DontFleeDropOffs) OR (NOT IS_ENTITY_WITHIN_DROP_OFF_FLEE_RANGE_FOR_PED(tempVeh, iped, iFleeFromDropOffDist, ciRULE_TYPE_VEHICLE, iID)) )
						
						BOOL bVehicleUsed
						
						IF iAction = ciACTION_GOTO
						AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedEscortRange = 0	AND IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vPedHeliEscortOffset))
						AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwelve, ciPed_BSTwelve_DoNotTryToEnterGoToTargetVehicle)
							VEHICLE_SEAT vsSeat
							vsSeat = GET_VEHICLE_SEAT_PED_SHOULD_ENTER(tempVeh, iped)
							PRINTLN("[RCC MISSION] GET_PED_CUSTOM_TARGET_NETWORK_ID - Ped ",iped,", vsSeat", vsSeat)
							IF vsSeat = VS_ANY_PASSENGER
								bVehicleUsed = TRUE // No free seats
							ENDIF
						ENDIF
						
						IF NOT bVehicleUsed // If the ped is able to get inside the vehicle
							RETURN niTarget
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ciRULE_TYPE_OBJECT
			IF iID < FMMC_MAX_NUM_OBJECTS
				niTarget = MC_serverBD_1.sFMMC_SBD.niObject[iID]
				
				IF NETWORK_DOES_NETWORK_ID_EXIST(niTarget)
					
					OBJECT_INDEX tempObj
					tempObj = NET_TO_OBJ(niTarget)
					
					IF NOT IS_ENTITY_DEAD(tempObj)
					AND ( IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_DontFleeDropOffs) OR (NOT IS_ENTITY_WITHIN_DROP_OFF_FLEE_RANGE_FOR_PED(tempObj, iped, iFleeFromDropOffDist, ciRULE_TYPE_OBJECT, iID)) )
						RETURN niTarget
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
	ENDSWITCH
	
	RETURN NULL
	
ENDFUNC

FUNC BOOL HAS_PLAYER_GOT_CONTROL_OF_CUSTOM_TARGET_ENTITY(INT iEntityType, INT iEntityID)
	
	IF iEntityType = ciRULE_TYPE_NONE
	OR iEntityID = -1
		RETURN FALSE
	ENDIF
	
	SWITCH iEntityType
		
		CASE ciRULE_TYPE_PED
			PRINTLN("[MMacK][TargetEntity] HAS_PLAYER_GOT_CONTROL_OF_CUSTOM_TARGET_ENTITY - ciRULE_TYPE_PED")
			IF (iEntityID >= 0) AND (iEntityID < FMMC_MAX_PEDS)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEntityID].iFollow = ciPED_FOLLOW_ON
				AND MC_serverBD_2.iPartPedFollows[iEntityID] != -1
					PRINTLN("[MMacK][TargetEntity] HAS_PLAYER_GOT_CONTROL_OF_CUSTOM_TARGET_ENTITY - Returning TRUE, MC_serverBD_2.iPartPedFollows[iEntityID] = ",MC_serverBD_2.iPartPedFollows[iEntityID])
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE ciRULE_TYPE_VEHICLE
			PRINTLN("[MMacK][TargetEntity] HAS_PLAYER_GOT_CONTROL_OF_CUSTOM_TARGET_ENTITY - ciRULE_TYPE_VEHICLE")
			IF (iEntityID >= 0) AND (iEntityID < FMMC_MAX_VEHICLES)
				
				NETWORK_INDEX niEntity
				niEntity = MC_serverBD_1.sFMMC_SBD.niVehicle[iEntityID]
				
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niEntity)
					
					PED_INDEX tempDriver
					tempDriver = GET_PED_IN_VEHICLE_SEAT(NET_TO_VEH(niEntity))
					
					IF NOT IS_PED_INJURED(tempDriver)
						IF IS_PED_A_PLAYER(tempDriver)
							PRINTLN("[MMacK][TargetEntity] HAS_PLAYER_GOT_CONTROL_OF_CUSTOM_TARGET_ENTITY - A player is in the driver's seat!")
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ciRULE_TYPE_OBJECT
			PRINTLN("[MMacK][TargetEntity] HAS_PLAYER_GOT_CONTROL_OF_CUSTOM_TARGET_ENTITY - ciRULE_TYPE_OBJECT")
			IF MC_serverBD.iObjCarrier[iEntityID] != -1
				PRINTLN("[MMacK][TargetEntity] HAS_PLAYER_GOT_CONTROL_OF_CUSTOM_TARGET_ENTITY - MC_serverBD.iObjCarrier[iEntityID] = ",MC_serverBD.iObjCarrier[iEntityID])
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE ciRULE_TYPE_GOTO
			PRINTLN("[MMacK][TargetEntity] HAS_PLAYER_GOT_CONTROL_OF_CUSTOM_TARGET_ENTITY - ciRULE_TYPE_GOTO")
			
			BOOL bSomeoneInArea
			INT iTeam
			
			FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
				IF MC_serverBD_1.inumberOfTeamInArea[iEntityID][iTeam] > 0
					bSomeoneInArea = TRUE
					iTeam = MC_serverBD.iNumberOfTeams
				ENDIF
			ENDFOR
			
			IF bSomeoneInArea
				PRINTLN("[MMacK][TargetEntity] HAS_PLAYER_GOT_CONTROL_OF_CUSTOM_TARGET_ENTITY - Someone is in the area!")
				RETURN TRUE
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_GOT_CONTROL_OF_PRIMARY_CUSTOM_TARGET_ENTITY(INT iped)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEleven, ciPED_BSEleven_DeactivatePrimaryTaskWhenPlayerControlsTarget)
		RETURN HAS_PLAYER_GOT_CONTROL_OF_CUSTOM_TARGET_ENTITY(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPrimaryTargetOverrideType, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPrimaryTargetOverrideID)
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL HAS_PLAYER_GOT_CONTROL_OF_SECONDARY_CUSTOM_TARGET_ENTITY(INT iped)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetSeven, ciPED_BSSeven_DeactivateSecondaryTaskWhenPlayerControlsTarget)
		RETURN HAS_PLAYER_GOT_CONTROL_OF_CUSTOM_TARGET_ENTITY(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSecondaryTargetOverrideType, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSecondaryTargetOverrideID)
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC INT GET_PED_OBJECTIVE_TARGET_LOCATION(INT iped, INT iRule, INT iTeam)
	
	INT i
	FLOAT fmaxdist = 999999
	INT ilocation = -1
	
	IF MC_serverBD.iNumLocCreated > 0
	AND MC_serverBD.iNumLocCreated <= FMMC_MAX_GO_TO_LOCATIONS
		FOR i = 0 TO (MC_serverBD.iNumLocCreated-1)
			IF IS_ENTITY_INVOLVED_IN_THIS_TEAMS_RULE(ciRULE_TYPE_GOTO,i,iTeam,iRule)
				IF NOT IS_VECTOR_ZERO(GET_LOCATION_VECTOR(i))
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped]),GET_LOCATION_VECTOR(i)) < fmaxdist
						ilocation = i
						fmaxdist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped]),GET_LOCATION_VECTOR(i))
					ENDIF
				ELSE
					NET_PRINT("vector is 0") NET_NL()
				ENDIF
			ELSE
				NET_PRINT(" location is not valid priority: ") NET_PRINT_INT(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iPriority[iTeam]) NET_PRINT(" associated objective : ") NET_PRINT_INT(iRule) NET_PRINT(" associated team : ") NET_PRINT_INT(iTeam) NET_NL()
			ENDIF
		ENDFOR
	ENDIF
	
	RETURN ilocation
	
ENDFUNC

FUNC PLAYER_INDEX GET_PED_OBJECTIVE_TARGET_PLAYER(INT iped, INT iTeam, INT iPriority, INT iCustomTargetTeam = -1)

	PLAYER_INDEX TargetPlayerID = INVALID_PLAYER_INDEX()
	INT iTargetTeam = -1
	
	IF iCustomTargetTeam = -1
		NET_PRINT("iPriority is: ") NET_PRINT_INT(iPriority) NET_PRINT("for ped: ") NET_PRINT_INT(iped)  NET_NL()
		NET_PRINT("iTeam is: ") NET_PRINT_INT(iTeam) NET_PRINT("for ped: ") NET_PRINT_INT(iped)  NET_NL()
		NET_PRINT("iRule selected is: ") NET_PRINT_INT(g_FMMC_STRUCT.sPlayerRuleData[iPriority].iRule[iTeam]) NET_PRINT("for ped: ") NET_PRINT_INT(iped)  NET_NL()
		
		IF g_FMMC_STRUCT.sPlayerRuleData[iPriority].iRule[iTeam] = FMMC_OBJECTIVE_LOGIC_KILL_PLAYERS
			iTargetTeam = -1
		ELIF g_FMMC_STRUCT.sPlayerRuleData[iPriority].iRule[iTeam] = FMMC_OBJECTIVE_LOGIC_KILL_TEAM0
			iTargetTeam = 0
		ELIF g_FMMC_STRUCT.sPlayerRuleData[iPriority].iRule[iTeam] = FMMC_OBJECTIVE_LOGIC_KILL_TEAM1
			iTargetTeam = 1
		ELIF g_FMMC_STRUCT.sPlayerRuleData[iPriority].iRule[iTeam] = FMMC_OBJECTIVE_LOGIC_KILL_TEAM2
			iTargetTeam = 2
		ELIF g_FMMC_STRUCT.sPlayerRuleData[iPriority].iRule[iTeam] = FMMC_OBJECTIVE_LOGIC_KILL_TEAM3
			iTargetTeam = 3
		ENDIF
	ELSE
		iTargetTeam = iCustomTargetTeam
	ENDIF
	
	NET_PRINT("target team selected is: ") NET_PRINT_INT(iTargetTeam) NET_PRINT("for ped: ") NET_PRINT_INT(iped)  NET_NL()	
	
	ENTITY_INDEX entPed = NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niped[iped])
	
	IF iTargetTeam != -1
		TargetPlayerID = INT_TO_PLAYERINDEX(GET_NEAREST_PLAYER_TO_ENTITY_ON_TEAM(entPed, iTargetTeam))
	ELSE
		TargetPlayerID = INT_TO_PLAYERINDEX(GET_NEAREST_PLAYER_TO_ENTITY(entPed))
	ENDIF
	
	NET_PRINT("target player selected is: ") NET_PRINT_INT(NATIVE_TO_INT(TargetPlayerID)) NET_PRINT("for ped: ") NET_PRINT_INT(iped)  NET_NL()	
	
	RETURN TargetPlayerID
			
ENDFUNC

FUNC BOOL ARE_ALL_TARGETS_DEAD(INT iped)
	
	INT i
	
	SWITCH MC_serverBD.iTargetType[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedObjective][g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedTeam]

		CASE ci_TARGET_PED
		
			IF MC_serverBD.iNumPedCreated > 0
			AND MC_serverBD.iNumPedCreated <= FMMC_MAX_PEDS
				FOR i = 0 TO (MC_serverBD.iNumPedCreated-1)
					IF i != iped
						IF IS_ENTITY_INVOLVED_IN_THIS_TEAMS_RULE(ciRULE_TYPE_PED,i,g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedTeam,g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedObjective)
							IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[i])
							OR ( (MC_serverBD_2.iCurrentPedRespawnLives[i] > 0) AND SHOULD_PED_RESPAWN_NOW(i) )
								RETURN FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
			
		BREAK
		
		CASE ci_TARGET_VEHICLE
			IF MC_serverBD.iNumVehCreated > 0
			AND MC_serverBD.iNumVehCreated <= FMMC_MAX_VEHICLES
				FOR i = 0 TO (MC_serverBD.iNumVehCreated-1)
					IF IS_ENTITY_INVOLVED_IN_THIS_TEAMS_RULE(ciRULE_TYPE_VEHICLE,i,g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedTeam,g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedObjective)
						IF ( NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
							AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i])) )
						OR ( (GET_VEHICLE_RESPAWNS(i) > 0) AND SHOULD_VEH_RESPAWN_NOW(i) )
							RETURN FALSE
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
		BREAK
		
		CASE ci_TARGET_OBJECT
			IF MC_serverBD.iNumObjCreated > 0
			AND MC_serverBD.iNumObjCreated <= FMMC_MAX_NUM_OBJECTS
				FOR i = 0 TO (MC_serverBD.iNumObjCreated-1)
					IF IS_ENTITY_INVOLVED_IN_THIS_TEAMS_RULE(ciRULE_TYPE_OBJECT,i,g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedTeam,g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedObjective)
						IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[i])
						OR ( (MC_serverBD_2.iCurrentObjRespawnLives[i] > 0) AND SHOULD_OBJ_RESPAWN_NOW(i) )
							RETURN FALSE
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN TRUE


ENDFUNC

FUNC NETWORK_INDEX GET_PED_OBJECTIVE_TARGET_NET_ID(INT iped, INT iRule, INT iTeam)

	INT i
	FLOAT fTempDist2
	FLOAT fmaxdist2 = 999999999
	NETWORK_INDEX TargetNetID= NULL
	PED_INDEX ThisPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
	VECTOR vPed = GET_ENTITY_COORDS(ThisPed)
	
	PED_INDEX tempPed
	VEHICLE_INDEX tempVeh
	OBJECT_INDEX tempObj
	
	BOOL bVehicleUsed
	BOOL bDontFleeDropOffs = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_DontFleeDropOffs)
	INT iFleeFromDropOffDist = 150
	
	IF IS_PED_IN_ANY_HELI(ThisPed) OR IS_PED_IN_ANY_PLANE(ThisPed)
		iFleeFromDropOffDist = 350
	ENDIF
	
	PRINTLN("[RCC MISSION] GET_PED_OBJECTIVE_TARGET_NET_ID - iPed: ", iPed, " MC_serverBD.iTargetType: ", MC_serverBD.iTargetType[iRule][iTeam])
	
	SWITCH MC_serverBD.iTargetType[iRule][iTeam]
		
		CASE ci_TARGET_PED
			
			IF MC_serverBD.iNumPedCreated > 0
			AND MC_serverBD.iNumPedCreated <= FMMC_MAX_PEDS
				FOR i = 0 TO (MC_serverBD.iNumPedCreated-1)
					IF i != iped
						IF IS_ENTITY_INVOLVED_IN_THIS_TEAMS_RULE(ciRULE_TYPE_PED,i,iTeam,iRule)
							IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[i])
								
								tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[i])
								
								fTempDist2 = VDIST2(vPed, GET_ENTITY_COORDS(tempPed))
								
								IF fTempDist2 < fmaxdist2
								AND ( bDontFleeDropOffs
									 OR (NOT IS_ENTITY_WITHIN_DROP_OFF_FLEE_RANGE_FOR_PED(tempPed, iped, iFleeFromDropOffDist, ciRULE_TYPE_PED, i)))
									TargetNetID = MC_serverBD_1.sFMMC_SBD.niPed[i]
									fmaxdist2 = fTempDist2
								ENDIF
								
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
			
		BREAK
		
		CASE ci_TARGET_VEHICLE
			IF MC_serverBD.iNumVehCreated > 0
			AND MC_serverBD.iNumVehCreated <= FMMC_MAX_VEHICLES
				FOR i = 0 TO (MC_serverBD.iNumVehCreated-1)
					IF IS_ENTITY_INVOLVED_IN_THIS_TEAMS_RULE(ciRULE_TYPE_VEHICLE, i, iTeam, iRule)
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
							
							tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
							
							IF IS_VEHICLE_DRIVEABLE(tempVeh)
								
								bVehicleUsed = FALSE
								
								IF MC_serverBD_2.iAssociatedAction[iped] = ciACTION_GOTO
								AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedEscortRange = 0
								AND IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vPedHeliEscortOffset)
									
									IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicleSeatPreference = ciFMMC_SeatPreference_None
										//Still use this checking the driver seat here for legacy missions
										tempPed = GET_PED_IN_VEHICLE_SEAT(tempVeh)
										
										IF NOT IS_PED_INJURED(tempPed)
											IF NOT IS_PED_A_PLAYER(tempPed)
												bVehicleUsed = TRUE
											ENDIF
										ENDIF
									ELSE
										VEHICLE_SEAT vsSeat
										vsSeat = GET_VEHICLE_SEAT_PED_SHOULD_ENTER(tempVeh, iped)
										
										IF vsSeat = VS_ANY_PASSENGER
											bVehicleUsed = TRUE // No free seats
										ENDIF
									ENDIF
								ENDIF
								
								NET_PRINT("[RCC MISSION] GET_PED_OBJECTIVE_TARGET_NET_ID iAssociatedAction: ") NET_PRINT_INT(MC_serverBD_2.iAssociatedAction[iped]) NET_NL()
								PRINTLN("[RCC MISSION] GET_PED_OBJECTIVE_TARGET_NET_ID Checking niVehicle i = ", i, " bVehicleUsed = ", bVehicleUsed, " iFleeFromDropOffDist = ", iFleeFromDropOffDist)
								
								IF NOT bVehicleUsed
									
									fTempDist2 = VDIST2(vPed, GET_ENTITY_COORDS(tempVeh))
									
									IF fTempDist2 < fmaxdist2
									AND ( bDontFleeDropOffs
										 OR (NOT IS_ENTITY_WITHIN_DROP_OFF_FLEE_RANGE_FOR_PED(tempVeh, iped, iFleeFromDropOffDist, ciRULE_TYPE_VEHICLE, i #IF IS_DEBUG_BUILD, TRUE #ENDIF)))
										TargetNetID = MC_serverBD_1.sFMMC_SBD.niVehicle[i]
										
										fmaxdist2 = fTempDist2
										
										PRINTLN("[RCC MISSION] GET_PED_OBJECTIVE_TARGET_NET_ID assigning target vehicle number: ", i, " Don't flee bit set = ",  IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_DontFleeDropOffs), " new fmaxdist2 = ", fmaxdist2)
									#IF IS_DEBUG_BUILD
									ELSE
										IF fTempDist2 >= fmaxdist2
											PRINTLN("[RCC MISSION] GET_PED_OBJECTIVE_TARGET_NET_ID Rejecting niVehicle i = ", i, " because fTempDist2 = ", fTempDist2, " >= fmaxdist2 = ", fmaxdist2 )
										ENDIF
									#ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						PRINTLN("[RCC MISSION] GET_PED_OBJECTIVE_TARGET_NET_ID veh ", i, " priority is not equal for iTeam: ", iTeam)
					ENDIF
				ENDFOR
			ELSE
				NET_PRINT("[RCC MISSION] GET_PED_OBJECTIVE_TARGET_NET_ID veh created is 0") NET_NL()
			ENDIF
		BREAK
		
		CASE ci_TARGET_OBJECT
			IF MC_serverBD.iNumObjCreated > 0
			AND MC_serverBD.iNumObjCreated <= FMMC_MAX_NUM_OBJECTS
				FOR i = 0 TO (MC_serverBD.iNumObjCreated - 1)
					IF IS_ENTITY_INVOLVED_IN_THIS_TEAMS_RULE(ciRULE_TYPE_OBJECT,i,iTeam,iRule)
						IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[i])
							
							tempObj = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[i])
							
							IF NOT IS_ENTITY_DEAD(tempObj)
								
								fTempDist2 = VDIST2(vPed, GET_ENTITY_COORDS(tempObj))
								
								IF fTempDist2 < fmaxdist2
								AND ( bDontFleeDropOffs
									 OR (NOT IS_ENTITY_WITHIN_DROP_OFF_FLEE_RANGE_FOR_PED(tempObj, iped, iFleeFromDropOffDist, ciRULE_TYPE_OBJECT, i)))
									TargetNetID = MC_serverBD_1.sFMMC_SBD.niObject[i]
									fmaxdist2 = fTempDist2
								ENDIF
								
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN TargetNetID
	
ENDFUNC

FUNC BOOL DOES_PED_HAVE_VALID_CUSTOM_TARGET(INT iped, INT iCustomType, INT iCustomID, BOOL bForceNewTarget, BOOL& bWillSpawn)
	
	BOOL bHasTarget
	
	bWillSpawn = FALSE
	
	PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_CUSTOM_TARGET - Ped ",iped,", iCustomType ", iCustomType)
	SWITCH iCustomType
		
		CASE ciRULE_TYPE_GOTO
			IF MC_serverBD_2.iTargetID[iped] = -1
			OR bForceNewTarget
				IF iCustomID < FMMC_MAX_GO_TO_LOCATIONS
					MC_serverBD_2.iTargetID[iped] = iCustomID
					bHasTarget = TRUE
				ENDIF
			ELSE
				bHasTarget = TRUE
			ENDIF
			
			IF bHasTarget
				SET_PED_STATE(iped,ciTASK_GOTO_COORDS)
			ENDIF
		BREAK
		
		CASE ciRULE_TYPE_PLAYER
			IF MC_serverBD.TargetPlayerID[iped] = INVALID_PLAYER_INDEX()
			OR bForceNewTarget
				MC_serverBD.TargetPlayerID[iped] = GET_PED_OBJECTIVE_TARGET_PLAYER(iped, -1, -1, iCustomID)
				
				IF MC_serverBD.TargetPlayerID[iped] != INVALID_PLAYER_INDEX()
					bHasTarget = TRUE
				ENDIF
			ELSE
				bHasTarget = TRUE
			ENDIF
			
			IF bHasTarget
				IF MC_serverBD_2.iAssociatedAction[iped] = ciACTION_GOTO
					SET_PED_STATE(iped,ciTASK_GOTO_PLAYER)
				ELIF MC_serverBD_2.iAssociatedAction[iped] = ciACTION_DEFEND
					SET_PED_STATE(iped,ciTASK_DEFEND_PLAYER)
				ELIF MC_serverBD_2.iAssociatedAction[iped] = ciACTION_ATTACK
					SET_PED_STATE(iped,ciTASK_ATTACK_PLAYER)
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_CUSTOM_TARGET - Ped ",iped," has a valid target, but no MC_serverBD_2.iAssociatedAction[iped]")
				#ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ciRULE_TYPE_PED
		CASE ciRULE_TYPE_VEHICLE
		CASE ciRULE_TYPE_OBJECT
			
			IF MC_serverBD.niTargetID[iped] = NULL
			OR bForceNewTarget
				
				MC_serverBD.niTargetID[iped] = GET_PED_CUSTOM_TARGET_NETWORK_ID(iped, iCustomType, iCustomID, MC_serverBD_2.iAssociatedAction[iped])
				
				IF MC_serverBD.niTargetID[iped] != NULL
					bHasTarget = TRUE
				ELSE
					
					INT iRespawnCount, iSpawnType, iSpawnTeam, iSpawnRule
					
					SWITCH iCustomType
						CASE ciRULE_TYPE_PED
							iRespawnCount = MC_serverBD_2.iCurrentPedRespawnLives[iCustomID]
							iSpawnTeam = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iCustomID].iAssociatedTeam
							iSpawnRule = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iCustomID].iAssociatedTeam
							iSpawnType = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iCustomID].iAssociatedSpawn
						BREAK
						CASE ciRULE_TYPE_VEHICLE
							iRespawnCount = GET_VEHICLE_RESPAWNS(iCustomID)
							iSpawnTeam = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iCustomID].iAssociatedTeam
							iSpawnRule = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iCustomID].iAssociatedObjective
							iSpawnType = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iCustomID].iAssociatedSpawn
						BREAK
						CASE ciRULE_TYPE_OBJECT
							iRespawnCount = MC_serverBD_2.iCurrentObjRespawnLives[iCustomID]
							iSpawnTeam = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCustomID].iAssociatedTeam
							iSpawnRule = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCustomID].iAssociatedObjective
							iSpawnType = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCustomID].iAssociatedSpawn
						BREAK
					ENDSWITCH
					
					IF iRespawnCount > 0
						IF iSpawnTeam = -1
						OR iSpawnRule = -1
						OR (iSpawnType != ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY)
						OR (MC_serverBD_4.iCurrentHighestPriority[iSpawnTeam] <= iSpawnRule) // If the entity will only spawn during one rule, check that we haven't passed that rule yet
							bWillSpawn = TRUE
						ENDIF
					ENDIF
				ENDIF
			ELSE
				bHasTarget = TRUE
			ENDIF
			
			IF bHasTarget
				IF MC_serverBD_2.iAssociatedAction[iped] = ciACTION_GOTO
					SET_PED_STATE(iped,ciTASK_GOTO_ENTITY)
					PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_CUSTOM_TARGET - ped ",iped," got valid target - goto")
				ELIF MC_serverBD_2.iAssociatedAction[iped] = ciACTION_DEFEND
					SET_PED_STATE(iped,ciTASK_DEFEND_ENTITY)
					PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_CUSTOM_TARGET - ped ",iped," got valid target - defend")
				ELIF MC_serverBD_2.iAssociatedAction[iped] = ciACTION_ATTACK
					SET_PED_STATE(iped,ciTASK_ATTACK_ENTITY)
					PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_CUSTOM_TARGET - ped ",iped," got valid target - attack")
				ELIF MC_serverBD_2.iAssociatedAction[iped] = ciACTION_AIM_AT
					SET_PED_STATE(iped,ciTASK_AIM_AT_ENTITY)
					PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_CUSTOM_TARGET - ped ",iped," got valid target - aim at")
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_CUSTOM_TARGET - Ped ",iped," has a valid target, but no MC_serverBD_2.iAssociatedAction[iped]")
				#ENDIF
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_CUSTOM_TARGET - Ped ",iped,", has no valid target")
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN bHasTarget
	
ENDFUNC

/// PURPOSE: This function is the main target choosing function, it will find a target for the ped if it can - bForceNewTarget forces
///    the ped to refresh its current target (whether or not the current target is still active or not); bPrimaryOnly / bSecondaryOnly
///    choose whether the function should check for targets regarding the ped's primary / secondary Actions respectively
FUNC BOOL DOES_PED_HAVE_VALID_TARGET(INT iped, BOOL bForceNewTarget = FALSE, BOOL bPrimaryOnly = FALSE, BOOL bSecondaryOnly = FALSE, BOOL bIncludeRespawningTarget = FALSE)
		
	IF SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK(iped)
		RETURN FALSE
	ENDIF
	
	INT iRule = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedObjective
	INT iTeam = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedTeam
	INT iAction = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedAction
	
	INT iTargetOverrideType = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPrimaryTargetOverrideType
	INT iTargetOverrideID = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPrimaryTargetOverrideID
	
	IF NOT bPrimaryOnly
	AND ( ( IS_BIT_SET(MC_serverBD.iPedSecondaryTaskActive[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		    AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSecondaryAction != -1 )
		 OR bSecondaryOnly )
		
		iRule = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSecondaryObjective
		iTeam = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSecondaryTeam
		iAction = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSecondaryAction
		
		iTargetOverrideType = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSecondaryTargetOverrideType
		iTargetOverrideID = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSecondaryTargetOverrideID
	ENDIF
	
	PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_TARGET - Ped ", iped, ", iTeam: ", iTeam, " irule: ", iRule, " MC_serverBD_2.iTargetID[iped]: ", MC_serverBD_2.iTargetID[iped], 
			" iTargetOverrideType:, ", iTargetOverrideType,", iTargetOverrideID: ", iTargetOverrideID,", iAction: ",iAction)
	
	PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_TARGET - bForceNewTarget: ", bForceNewTarget, ", bPrimaryOnly: ", bPrimaryOnly, ", bSecondaryOnly: ", bSecondaryOnly, ", bIncludeRespawningTarget: ", bIncludeRespawningTarget)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedFixationTarget > -1
	AND IS_RULE_VALID_FOR_PED_FIXATION_STATE(iPed)
		IF NOT IS_BIT_SET(MC_serverBD_1.iPedFixationTaskEnded[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))		
			PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_TARGET - [LM][PED_FIXATE] - iPed: ", iPed, " iPed Fixate Target: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedFixationTarget, " Checking Target")
			MC_serverBD.niTargetID[iped] = MC_serverBD_1.sFMMC_SBD.niPed[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedFixationTarget]
			
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD.niTargetID[iped])
				PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_TARGET - [LM][PED_FIXATE] - iPed: ", iPed, " iPed Fixate Target: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedFixationTarget, " Setting ciTASK_ATTACK_ENTITY")
				SET_PED_STATE(iped, ciTASK_ATTACK_ENTITY)
				RETURN TRUE
			ELSE
				MC_serverBD.niTargetID[iped] = NULL
				PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_TARGET - [LM][PED_FIXATE] - iPed: ", iPed, " iPed Fixate Target: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedFixationTarget, " Target is NULL")
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_TARGET - [LM][PED_FIXATE] - iPed: ", iPed, " iPed Fixate Target: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedFixationTarget, " Fixation has ended though!")
		ENDIF
	ENDIF
	
	IF iRule != -1
	AND iRule < FMMC_MAX_RULES
	AND iTeam > -1
	AND iTeam < FMMC_MAX_TEAMS
		PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_TARGET MC_serverBD.iTargetType: ", MC_serverBD.iTargetType[iRule][iTeam])
	ENDIF
	
	IF iTargetOverrideType != ciRULE_TYPE_NONE
	AND iTargetOverrideID != -1
	AND iAction != ciACTION_NOTHING
		
		PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_TARGET - Overrides ---")
		
		IF iAction = ciACTION_COMBAT_MODE
			SET_PED_STATE(iped,ciTASK_GENERAL_COMBAT #IF IS_DEBUG_BUILD , 0 #ENDIF )
			RETURN TRUE
		ENDIF
		
		BOOL bWillSpawn
		BOOL bValidTarget = DOES_PED_HAVE_VALID_CUSTOM_TARGET(iped, iTargetOverrideType, iTargetOverrideID, bForceNewTarget, bWillSpawn)
		
		IF (NOT bValidTarget) // If the target doesn't exist right now
		AND (NOT bWillSpawn) // and if the target won't be valid later
		AND iAction = ciACTION_ATTACK
			MANAGE_PED_ARRIVAL_RESULTS(iped, FALSE)
		ENDIF
		
		IF bIncludeRespawningTarget
		AND NOT bValidTarget
			RETURN bWillSpawn
		ENDIF
		
		RETURN bValidTarget
		
	ENDIF
	
	
	IF iRule != -1
	AND iTeam != -1
	AND iAction != ciACTION_NOTHING		
		IF iAction = ciACTION_COMBAT_MODE
			SET_PED_STATE(iped,ciTASK_GENERAL_COMBAT #IF IS_DEBUG_BUILD , 1 #ENDIF )
			PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_TARGET - Ped ", iped, " Returninbg True (1)")
			RETURN TRUE
		ENDIF
		IF iAction = ciACTION_THROW_PROJECTILES
			SET_PED_STATE(iped,ciTASK_THROW_PROJECTILES #IF IS_DEBUG_BUILD , 1 #ENDIF )
			PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_TARGET - Ped ", iped, " Returninbg True (2)")
			RETURN TRUE
		ENDIF
		
		IF MC_serverBD.iTargetType[iRule][iTeam] != ci_TARGET_NONE
			IF MC_serverBD.iTargetType[iRule][iTeam] = ci_TARGET_LOCATION
				PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_TARGET - Ped ", iped, " MC_serverBD.iTargetType[iRule][iTeam] = ci_TARGET_LOCATION")
				
				IF MC_serverBD_2.iTargetID[iped] = -1
				OR bForceNewTarget
					MC_serverBD_2.iTargetID[iped] = GET_PED_OBJECTIVE_TARGET_LOCATION(iped, iRule, iTeam)
					IF MC_serverBD_2.iTargetID[iped] = -1
						MC_serverBD.iTargetType[iRule][iTeam] = ci_TARGET_NONE
						PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_TARGET - Ped ", iped, " MC_serverBD.iTargetType[iRule][iTeam] = ci_TARGET_NONE")
					ELSE
						SET_PED_STATE(iped,ciTASK_GOTO_COORDS)
						PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_TARGET - Ped ", iped, " Returninbg True (3)")
						RETURN TRUE
					ENDIF
				ELSE
					SET_PED_STATE(iped,ciTASK_GOTO_COORDS)
					PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_TARGET - Ped ", iped, " Returninbg True (4)")
					RETURN TRUE
				ENDIF
			ELIF MC_serverBD.iTargetType[iRule][iTeam] = ci_TARGET_PLAYER
				PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_TARGET - Ped ", iped, " MC_serverBD.iTargetType[iRule][iTeam] = ci_TARGET_PLAYER")
				
				IF MC_serverBD.TargetPlayerID[iped] = INVALID_PLAYER_INDEX()
				OR bForceNewTarget
					MC_serverBD.TargetPlayerID[iped] = GET_PED_OBJECTIVE_TARGET_PLAYER(iped, iTeam, iRule)
					IF MC_serverBD.TargetPlayerID[iped] = INVALID_PLAYER_INDEX()
						PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_TARGET - Ped ", iped, " Target = INVALID_PLAYER_INDEX")
						MC_serverBD.iTargetType[iRule][iTeam] = ci_TARGET_NONE
					ELSE
						IF MC_serverBD_2.iAssociatedAction[iped] = ciACTION_GOTO
							SET_PED_STATE(iped,ciTASK_GOTO_PLAYER)
						ELIF MC_serverBD_2.iAssociatedAction[iped] =ciACTION_DEFEND
							SET_PED_STATE(iped,ciTASK_DEFEND_PLAYER)
						ELIF MC_serverBD_2.iAssociatedAction[iped] =ciACTION_ATTACK
							SET_PED_STATE(iped,ciTASK_ATTACK_PLAYER)
						#IF IS_DEBUG_BUILD
						ELSE
							PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_TARGET - Ped ",iped," has a valid target, but no MC_serverBD_2.iAssociatedAction[iped]")
						#ENDIF
						ENDIF
						PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_TARGET - Ped ", iped, " Returninbg True (5)")
						RETURN TRUE
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_TARGET - Ped ", iped, " Other...")
					
					IF MC_serverBD_2.iAssociatedAction[iped] =ciACTION_GOTO
						SET_PED_STATE(iped,ciTASK_GOTO_PLAYER)
					ELIF MC_serverBD_2.iAssociatedAction[iped] =ciACTION_DEFEND
						SET_PED_STATE(iped,ciTASK_DEFEND_PLAYER)
					ELIF MC_serverBD_2.iAssociatedAction[iped] =ciACTION_ATTACK
						SET_PED_STATE(iped,ciTASK_ATTACK_PLAYER)
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_TARGET - Ped ",iped," has a valid target, but no MC_serverBD_2.iAssociatedAction[iped]")
					#ENDIF
					ENDIF
					
					PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_TARGET - Ped ", iped, " Returninbg True (6)")
					RETURN TRUE
				ENDIF
			ELSE
				IF MC_serverBD.niTargetID[iped] = NULL
				OR bForceNewTarget
					PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_TARGET - Ped ", iped, " MC_serverBD.niTargetID[iped] = NULL or bForceNewTarget")
					
					MC_serverBD.niTargetID[iped] = GET_PED_OBJECTIVE_TARGET_NET_ID(iped, iRule, iTeam)
					IF MC_serverBD.niTargetID[iped] = NULL
						PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_TARGET - Ped ", iped, " MC_serverBD.niTargetID[iped] = NULL")
						
						IF ARE_ALL_TARGETS_DEAD(iped)
							
							IF MC_serverBD_2.iAssociatedAction[iped] = ciACTION_ATTACK
								MANAGE_PED_ARRIVAL_RESULTS(iped, FALSE)
							ENDIF
							
							MC_serverBD.iTargetType[iRule][iTeam] = ci_TARGET_NONE
							PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_TARGET - ped ",iped," all targets dead for team ",iTeam,"'s rule ",iRule,", setting type to none")
						ELSE
							IF bIncludeRespawningTarget
								PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_TARGET - ped ",iped," targets respawning for team ",iTeam,"'s rule ",iRule,", return TRUE")
								PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_TARGET - Ped ", iped, " Returning True (7)")	
								RETURN TRUE
							ENDIF
						ENDIF
					ELSE
						IF MC_serverBD_2.iAssociatedAction[iped] =ciACTION_GOTO
							SET_PED_STATE(iped,ciTASK_GOTO_ENTITY)
							PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_TARGET - ped ",iped," got valid target - goto")
						ELIF MC_serverBD_2.iAssociatedAction[iped] =ciACTION_DEFEND
							SET_PED_STATE(iped,ciTASK_DEFEND_ENTITY)
							PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_TARGET - ped ",iped," got valid target - defend")
						ELIF MC_serverBD_2.iAssociatedAction[iped] =ciACTION_ATTACK
							SET_PED_STATE(iped,ciTASK_ATTACK_ENTITY)
							PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_TARGET - ped ",iped," got valid target - attack")
						ELIF MC_serverBD_2.iAssociatedAction[iped] =ciACTION_AIM_AT
							SET_PED_STATE(iped,ciTASK_AIM_AT_ENTITY)
						#IF IS_DEBUG_BUILD
						ELSE
							PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_TARGET - Ped ",iped," has a valid target, but no MC_serverBD_2.iAssociatedAction[iped]")
						#ENDIF
						ENDIF
						
						PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_TARGET - Ped ", iped, " Returning True (8)")	
						RETURN TRUE
					ENDIF
				ELSE
					IF MC_serverBD_2.iAssociatedAction[iped] =ciACTION_GOTO
						SET_PED_STATE(iped,ciTASK_GOTO_ENTITY)
						PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_TARGET - ped ",iped," got valid target - goto")
					ELIF MC_serverBD_2.iAssociatedAction[iped] =ciACTION_DEFEND
						SET_PED_STATE(iped,ciTASK_DEFEND_ENTITY)
						PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_TARGET - ped ",iped," got valid target - defend")
					ELIF MC_serverBD_2.iAssociatedAction[iped] =ciACTION_ATTACK
						SET_PED_STATE(iped,ciTASK_ATTACK_ENTITY)
						PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_TARGET - ped ",iped," got valid target - attack")
					ELIF MC_serverBD_2.iAssociatedAction[iped] =ciACTION_AIM_AT
						SET_PED_STATE(iped,ciTASK_AIM_AT_ENTITY)
						PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_TARGET - ped ",iped," got valid target - aim at")
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_TARGET - Ped ",iped," has a valid target, but no MC_serverBD_2.iAssociatedAction[iped]")
					#ENDIF
					ENDIF
					
					PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_TARGET - Ped ", iped, " Returning True (9)")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	PRINTLN("[RCC MISSION] DOES_PED_HAVE_VALID_TARGET - Ped ", iped, " Returning FALSE")
	
	RETURN FALSE

ENDFUNC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: PED STATE CHOOSING (chooses what state the ped should be in / what action the ped should be performing) !
//
//************************************************************************************************************************************************************



FUNC BOOL SHOULD_PED_START_COMBAT(PED_INDEX tempPed, INT iped)
	
	IF SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK(iped, TRUE)
		RETURN FALSE
	ENDIF
	
	IF IS_PED_IN_COMBAT(tempPed)
	OR IS_PED_FLEEING(tempPed)
	OR (GET_PED_ALERTNESS(tempPed) != AS_NOT_ALERT AND GET_PED_ALERTNESS(tempPed) != INT_TO_ENUM(ALERTNESS_STATE, -1))
		
		SWITCH GET_PED_COMBAT_STYLE(iped)
			CASE ciPED_AGRESSIVE
			CASE ciPED_BERSERK
				RETURN TRUE
			
			CASE ciPED_DEFENSIVE
				IF IS_PED_ARMED_BY_FMMC(iped)
					RETURN TRUE
				ENDIF
			BREAK
		ENDSWITCH
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_ANY_FMMC_PLAYER_WITHIN_AREA(VECTOR vCentre, INT iRadius)
	
	BOOL bPlayerInArea
	
	INT iPart
	INT iChecked
	INT iToCheck = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]
	
	PARTICIPANT_INDEX tempPart
	PLAYER_INDEX tempPlayer
	VECTOR vTempCoords
	
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		
		tempPart = INT_TO_PARTICIPANTINDEX(iPart)
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			
			tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
			
			IF IS_NET_PLAYER_OK(tempPlayer)
			AND NOT IS_PLAYER_SPECTATOR_ONLY(tempPlayer)
				
				vTempCoords = GET_PLAYER_COORDS(tempPlayer)
				
				IF VDIST2(vTempCoords, vCentre) <= (iRadius * iRadius)
					PRINTLN("[RCC MISSION] IS_ANY_FMMC_PLAYER_WITHIN_AREA - Part ",iPart," w coords ",vTempCoords," is in area centered at ",vCentre," w radius ",iRadius)
					bPlayerInArea = TRUE
					iPart = MAX_NUM_MC_PLAYERS // Break out!
				ELSE
					iChecked++
					
					IF iChecked >= iToCheck
						iPart = MAX_NUM_MC_PLAYERS // Break out!
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
		
	ENDFOR
	
	RETURN bPlayerInArea
	
ENDFUNC

FUNC BOOL SET_PED_HAS_VALID_GOTO(INT iped)

	IF SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK( iped)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(MC_serverBD.iPedAtGotoLocBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
	
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData.sPointData[0].vPosition)
		
			INT iAssociatedRule = GET_ASSOCIATED_GOTO_TASK_DATA__ASSOCIATED_RULE(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed])
			VECTOR vPlayerActivatedPosition = GET_ASSOCIATED_GOTO_TASK_DATA__PLAYER_ACTIIVATED_POSITION(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed])
			INT iPlayerActivatedRadius = GET_ASSOCIATED_GOTO_TASK_DATA__PLAYER_ACTIIVATED_RADIUS(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed])
			
			BOOL bDoGoto			
			BOOL bDependsOnRule = (iAssociatedRule != -1) AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedTeam != -1)
			BOOL bDependsOnArea = (NOT IS_VECTOR_ZERO(vPlayerActivatedPosition)) AND (iPlayerActivatedRadius > 0)
			
			IF (MC_serverBD_2.iPedGotoProgress[iped] < (MAX_ASSOCIATED_GOTO_TASKS))
			AND (bDependsOnRule OR bDependsOnArea)
				
				IF bDependsOnRule
					IF (MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedTeam] >= iAssociatedRule)
						IF bDependsOnArea 
							IF IS_ANY_FMMC_PLAYER_WITHIN_AREA(vPlayerActivatedPosition, iPlayerActivatedRadius)
								bDoGoto = TRUE
							ENDIF
						ELSE
							bDoGoto = TRUE
						ENDIF
					ENDIF
				ELIF bDependsOnArea
					IF IS_ANY_FMMC_PLAYER_WITHIN_AREA(vPlayerActivatedPosition, iPlayerActivatedRadius)
						bDoGoto = TRUE
					ENDIF
				ENDIF
				
			ELSE
				bDoGoto = TRUE
			ENDIF
			
			
			IF bDoGoto
				IF MC_serverBD_2.iPedState[iped] != ciTASK_GOTO_COORDS
					SET_PED_STATE(iped,ciTASK_GOTO_COORDS)
				ENDIF
				MC_serverBD_2.iTargetID[iped] = ciINITIAL_GOTO
				RETURN TRUE
			ENDIF
			
		ELSE
			SET_BIT(MC_serverBD.iPedAtGotoLocBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL DOES_PED_HAVE_VALID_GOTO(INT iped)
 	
	IF NOT IS_BIT_SET(MC_serverBD.iPedAtGotoLocBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData.sPointData[0].vPosition)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SET_PED_HAS_VALID_SECONDARY_GOTO(INT iped)
	
	IF SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK(iped)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(MC_serverBD.iPedAtSecondaryGotoLocBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vSecondaryVector)
			
			IF MC_serverBD_2.iPedState[iped] != ciTASK_GOTO_COORDS
				SET_PED_STATE(iped,ciTASK_GOTO_COORDS)
			ENDIF
			
			PRINTLN("[RCC MISSION] SET_PED_HAS_VALID_SECONDARY_GOTO - ped ",iped," set ped to do secondary goto and cancel primary goto by setting iPedAtGotoLocBitset")
			SET_BIT(MC_serverBD.iPedAtGotoLocBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			MC_serverBD_2.iTargetID[iped] = ciSECONDARY_GOTO
			
			RETURN TRUE
		ELSE
			SET_BIT(MC_serverBD.iPedAtSecondaryGotoLocBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL DOES_PED_HAVE_VALID_SECONDARY_GOTO(INT iped)
	
	IF NOT IS_BIT_SET(MC_serverBD.iPedAtSecondaryGotoLocBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vSecondaryVector)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_PED_GO_TO_COVER_STATE(PED_INDEX ThisPed, INT iped)
	
	IF SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK(iped)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour, ciPED_BSFour_TakeCover_AlwaysTakeCover)
	AND NOT (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn = IG_RASHCOSVKI) // Want Rashkovsky to get into cover at the start, and then let normal AI take over
	AND SHOULD_PED_START_COMBAT(ThisPed, iped)
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedAction = ciACTION_TAKE_COVER
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_COMBAT_VEHICLE(VEHICLE_INDEX tempVeh)
	
	IF NOT DOES_ENTITY_EXIST(tempVeh)
		PRINTLN("IS_COMBAT_VEHICLE - This vehicle doesn't exist! Returning FALSE")
		DEBUG_PRINTCALLSTACK()
		RETURN FALSE
	ENDIF
	
	SWITCH GET_ENTITY_MODEL(tempVeh)
		
		CASE RHINO
		CASE LAZER
		CASE BUZZARD
		CASE ANNIHILATOR
		CASE SAVAGE
		CASE TAMPA3
		CASE HUNTER
		CASE VALKYRIE
		CASE THRUSTER
			RETURN TRUE
			
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

FUNC BOOL SHOULD_PED_GO_FROM_NOTHING_STATE_TO_COMBAT_STATE(PED_INDEX ThisPed, INT iPed)
	
	IF SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK(iped)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEleven, ciPED_BSEleven_CanHaveDefensiveAreaInVehicle)
		RETURN FALSE
	ENDIF
	
	VEHICLE_INDEX tempVeh
	
	IF SHOULD_PED_START_COMBAT(ThisPed, iped)
	AND NOT (
			( (NOT SHOULD_PED_BE_GIVEN_TASK(ThisPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE))
			OR (NOT SHOULD_PED_BE_GIVEN_TASK(ThisPed, iped, SCRIPT_TASK_SYNCHRONIZED_SCENE, TRUE)) )
			AND DOES_ANIMATION_NEED_SPECIAL_BREAKOUT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim)
			)
		
		IF IS_BIT_SET( iPedHitByStunGun[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed) )
			PRINTLN("[RCC MISSION] SHOULD_PED_GO_FROM_NOTHING_STATE_TO_COMBAT_STATE - Returning FALSE as ped ",iped," is stunned!")
			RETURN FALSE
		ENDIF
		
		
		#IF IS_DEBUG_BUILD
		IF IS_PED_IN_COMBAT(ThisPed)
			PRINTLN("[RCC MISSION] SHOULD_PED_GO_FROM_NOTHING_STATE_TO_COMBAT_STATE - Returning TRUE as ped ",iped," is in combat!")
		ENDIF
		IF IS_PED_FLEEING(ThisPed)
			PRINTLN("[RCC MISSION] SHOULD_PED_GO_FROM_NOTHING_STATE_TO_COMBAT_STATE - Returning TRUE as ped ",iped," is fleeing!")
		ENDIF
		IF (GET_PED_ALERTNESS(ThisPed) != AS_NOT_ALERT AND GET_PED_ALERTNESS(ThisPed) != INT_TO_ENUM(ALERTNESS_STATE, -1))
			PRINTLN("[RCC MISSION] SHOULD_PED_GO_FROM_NOTHING_STATE_TO_COMBAT_STATE - Returning TRUE as ped ",iped," is alerted!")
		ENDIF
		#ENDIF
		
		RETURN TRUE
	ENDIF
		
	IF NOT IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetThirteen, ciPED_BSThirteen_CombatAutomaticallyWhenInCombatVehicle)
		IF IS_PED_IN_ANY_VEHICLE(ThisPed)
			tempVeh = GET_VEHICLE_PED_IS_USING(ThisPed)
			IF IS_VEHICLE_DRIVEABLE(tempVeh)
				IF IS_COMBAT_VEHICLE(tempVeh)
					
					#IF IS_DEBUG_BUILD
					PRINTLN("[RCC MISSION] SHOULD_PED_GO_FROM_NOTHING_STATE_TO_COMBAT_STATE - Returning TRUE as ped ",iped," in a driveable combat vehicle!")
					#ENDIF
					
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_PED_AT_ANY_TEAM_HOLDING(INT iped)

	IF NOT IS_BIT_SET(MC_serverBD.iPedAtYourHolding[0][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
	AND NOT IS_BIT_SET(MC_serverBD.iPedAtYourHolding[1][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
	AND NOT IS_BIT_SET(MC_serverBD.iPedAtYourHolding[2][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
	AND NOT IS_BIT_SET(MC_serverBD.iPedAtYourHolding[3][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
		RETURN FALSE
	ENDIF

	RETURN TRUE

ENDFUNC

/// PURPOSE: This function is the main Ped State choosing function, it will find what state the ped should be doing right now by
///    checking through the ped's potential Actions and checking for any potential Targets
PROC PROCESS_PED_TASK_NOTHING_BRAIN(PED_INDEX ThisPed, INT iPed)
	
	BOOL bIsThisPacificStandardConvoyHACK = (GET_CURRENT_HEIST_MISSION() = HBCA_BS_PACIFIC_STANDARD_CONVOY)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedFixationTarget > -1
	AND IS_BIT_SET(MC_ServerBD_1.iPedFixationTaskEnded[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
	AND NOT IS_BIT_SET(iPedFixationStateCleanedUpBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
		PRINTLN("[RCC MISSION] PROCESS_PED_TASK_NOTHING_BRAIN - Ped ",iped," Exitting. Waiting for the Body to cleanup fixation settings. Likely stuck in combat...")
		EXIT	
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF bIsThisPacificStandardConvoyHACK
			PRINTLN("[RCC MISSION] PROCESS_PED_TASK_NOTHING_BRAIN - Ped ",iped," bIsThisPacificStandardConvoyHACK = TRUE, this is the convoy setup mission.")
		ENDIF
	#ENDIF
	
	IF NOT IS_BIT_SET(MC_serverBD.iPedTaskActive[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
	AND NOT IS_BIT_SET(MC_serverBD.iPedSecondaryTaskActive[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		PRINTLN("[RCC MISSION] PROCESS_PED_TASK_NOTHING_BRAIN - Ped ",iped," shouldn't start any tasks because none have been activated")
		EXIT
	ENDIF
	
	IF DOES_ANIMATION_NEED_SPECIAL_BREAKOUT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim)
	AND IS_PED_ANIMATION_PLAYING(ThisPed, iped)
		//We're animating and we need to do a special break out, don't start any tasks!
		PRINTLN("[RCC MISSION] PROCESS_PED_TASK_NOTHING_BRAIN - Ped ",iped," shouldn't start any tasks because performing an animation that requires a breakout")
		EXIT
	ENDIF
	
	IF MC_serverBD_2.iPartPedFollows[iped] != -1
	AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetSix, ciPED_BSSix_AllowTaskingWhileGrouped)
		PRINTLN("[RCC MISSION] PROCESS_PED_TASK_NOTHING_BRAIN - Ped ",iped," shouldn't start any tasks because MC_serverBD_2.iPartPedFollows[iped] = ",MC_serverBD_2.iPartPedFollows[iped])
		EXIT
	ENDIF
	
	IF IS_PED_AT_ANY_TEAM_HOLDING(iped)
		PRINTLN("[RCC MISSION] PROCESS_PED_TASK_NOTHING_BRAIN - Ped ",iped," shouldn't start any tasks because IS_PED_AT_ANY_TEAM_HOLDING returns TRUE")
		EXIT
	ENDIF
	
	//Secondary tasks:
	IF IS_BIT_SET(MC_serverBD.iPedSecondaryTaskActive[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		
		IF SET_PED_HAS_VALID_SECONDARY_GOTO(iped)
			PRINTLN("[RCC MISSION] PROCESS_PED_TASK_NOTHING_BRAIN - Ped ",iped," has a valid secondary goto")
			EXIT
		ENDIF
		
		IF DOES_PED_HAVE_VALID_TARGET(iped, FALSE, FALSE, TRUE, (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEleven, ciPED_BSEleven_WaitForTaskTargetToSpawn) OR bIsThisPacificStandardConvoyHACK))
			PRINTLN("[RCC MISSION] PROCESS_PED_TASK_NOTHING_BRAIN - Ped ",iped," has a valid secondary target; ciPED_BSEleven_WaitForTaskTargetToSpawn = ",IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEleven, ciPED_BSEleven_WaitForTaskTargetToSpawn))
			EXIT
		ENDIF
		
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedFixationTarget > -1
		IF NOT IS_BIT_SET(MC_serverBD_1.iPedFixationTaskStarted[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))	
		AND NOT IS_BIT_SET(iPedFixationStateCleanedUpBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))	
		AND IS_RULE_VALID_FOR_PED_FIXATION_STATE(iPed)
		AND NOT SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK(iped)
			SET_PED_STATE(iped, ciTASK_ATTACK_ENTITY)
			PRINTLN("[RCC MISSION] PROCESS_PED_TASK_NOTHING_BRAIN - Ped ",iped," should go into ciTASK_ATTACK_ENTITY iPedFixationTarget: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedFixationTarget)
			EXIT
		ENDIF
	ENDIF
			
	IF IS_BIT_SET(MC_serverBD.iPedTaskActive[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		
		IF NOT IS_BIT_SET(MC_serverBD.iPedTaskStopped[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			
			IF SET_PED_HAS_VALID_GOTO(iped)
				PRINTLN("[RCC MISSION] PROCESS_PED_TASK_NOTHING_BRAIN - Ped ",iped," has a valid primary goto")
				EXIT
			ENDIF
			
			IF SHOULD_PED_GO_TO_COVER_STATE(ThisPed,iped)
				SET_PED_STATE(iped,ciTASK_TAKE_COVER)
				PRINTLN("[RCC MISSION] PROCESS_PED_TASK_NOTHING_BRAIN - Ped ",iped," should go into cover state")
				EXIT
			ENDIF
			
			IF DOES_PED_HAVE_VALID_TARGET(iped, FALSE, TRUE, FALSE, (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEleven, ciPED_BSEleven_WaitForTaskTargetToSpawn) OR bIsThisPacificStandardConvoyHACK))
				PRINTLN("[RCC MISSION] PROCESS_PED_TASK_NOTHING_BRAIN - Ped ",iped," has a valid primary target; ciPED_BSEleven_WaitForTaskTargetToSpawn = ",IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEleven, ciPED_BSEleven_WaitForTaskTargetToSpawn))
				EXIT
			ENDIF
			
		ENDIF
		
		IF SHOULD_PED_GO_FROM_NOTHING_STATE_TO_COMBAT_STATE(ThisPed, iPed)
			SET_PED_STATE(iped,ciTASK_GENERAL_COMBAT #IF IS_DEBUG_BUILD , 3 #ENDIF )
			PRINTLN("[RCC MISSION] PROCESS_PED_TASK_NOTHING_BRAIN - Ped ",iped," should go into combat state")
			EXIT
		ENDIF
		
		IF NOT IS_BIT_SET(MC_serverBD.iPedTaskStopped[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fRange = cfMAX_PATROL_RANGE
				IF SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK(iped)
					PRINTLN("[RCC MISSION] PROCESS_PED_TASK_NOTHING_BRAIN - Ped ",iped," should not go to wander state as they should prioritise their anim")
					EXIT
				ENDIF
				SET_PED_STATE(iped,ciTASK_WANDER)
				PRINTLN("[RCC MISSION] PROCESS_PED_TASK_NOTHING_BRAIN - Ped ",iped," should go into wander state")
				EXIT
			ENDIF
			
		ENDIF
		
		IF NOT IS_PED_IN_ANY_VEHICLE(ThisPed)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iIdleAnim = ciPED_IDLE_ANIM__NONE
			AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnim = ciPED_IDLE_ANIM__NONE
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitset,ciPED_BS_CanMoveBeforeCombat)
					IF SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK(iped)
						PRINTLN("[RCC MISSION] PROCESS_PED_TASK_NOTHING_BRAIN - Ped ",iped," should not go to defend state as they should prioritise their anim")
						EXIT
					ENDIF
					SET_PED_STATE(iped,ciTASK_DEFEND_AREA)
					PRINTLN("[RCC MISSION] PROCESS_PED_TASK_NOTHING_BRAIN - Ped ",iped," should go into defend state")
					EXIT
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEleven, ciPED_BSEleven_CanHaveDefensiveAreaInVehicle)
				SET_PED_STATE(iped,ciTASK_DEFEND_AREA_IN_VEHICLE)
				PRINTLN("[RCC MISSION] PROCESS_PED_TASK_NOTHING_BRAIN - Ped ",iped," should go into defend (vehicle version) state")
				EXIT
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: PED TASK ACTIVATION & CANCELLING/STOPPING !
//
//************************************************************************************************************************************************************



FUNC BOOL HAS_ANY_TEAM_HIT_PED_TASK_ACTIVATION(INT iped)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCSTaskActivationSceneIndex > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCSTaskActivationShotIndex > -1
		// If the ped's tasks are supposed to start on a cutscene, then that takes priority over the rest of the associated objective stuff
		IF MC_serverBD.iSpawnScene = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCSTaskActivationSceneIndex
		AND MC_serverBD.iSpawnShot >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCSTaskActivationShotIndex
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedTeam != -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedObjective != -1
		//The logic here is the same as for spawning, so use the same function:
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedActionStart, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedObjective, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedAlwaysForceSpawnOnRule)
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSecondAssociatedTeam != -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSecondAssociatedObjective != -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSecondActionStart, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSecondAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSecondAssociatedObjective, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSecondScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSecondAlwaysForceSpawnOnRule)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iThirdAssociatedTeam != -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iThirdAssociatedObjective != -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iThirdActionStart, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iThirdAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iThirdAssociatedObjective, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iThirdScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iThirdAlwaysForceSpawnOnRule)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFourthAssociatedTeam != -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFourthAssociatedObjective != -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFourthActionStart, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFourthAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFourthAssociatedObjective, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFourthScoreRequired, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFourthAlwaysForceSpawnOnRule)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL HAS_ANY_TEAM_HIT_PED_SECONDARY_TASK_ACTIVATION(INT iped)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSecondaryTeam != -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSecondaryObjective != -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSecondaryActionStart, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSecondaryTeam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSecondaryObjective)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Checks if the ped is going in/out of range to be tasked with Actions focusing on this target entity
/// PARAMS:
///    tempPed - The ped index of the ped doing the targetting
///    iped - The creator index of the ped doing the targetting
///    Don't stop bLeaving - Hold on to that feeling. Set this to TRUE if you're trying to see if the ped is going to leave this radius and should be un-tasked again.
///    iTriggerDistance - The distance a ped needs to be near to their custom target to be able to do their tasks.
///    iCustomType - The rule type of their target (choose from ciRULE_TYPE_NONE/PED/VEHICLE/etc)
///    iCustomID - The creator ID of their target.
/// RETURNS:
///    If the ped is within range to be tasked with Actions focusing on this target entity
FUNC BOOL IS_PED_WITHIN_RANGE_OF_CUSTOM_TARGET_ENTITY(PED_INDEX tempPed, INT iped, BOOL bLeaving, INT iTriggerDistance, INT iCustomType, INT iCustomID)
	
	BOOL bInRange
	UNUSED_PARAMETER(iped)
	
	IF iTriggerDistance != -1
	AND iCustomType != ciRULE_TYPE_NONE
	AND iCustomID != -1
		VECTOR vPed = GET_ENTITY_COORDS(tempPed)
		
		VECTOR vEntity = GET_FMMC_ENTITY_LOCATION(vPed, iCustomType, iCustomID, tempPed)
		
		IF NOT IS_VECTOR_ZERO(vEntity)
			
			FLOAT fDist2
			
			IF bLeaving
				FLOAT fDistToAdd = iTriggerDistance * 0.25
				
				IF fDistToAdd < 5
					fDistToAdd = 5
				ENDIF
				
				fDist2 = (iTriggerDistance + fDistToAdd) * (iTriggerDistance + fDistToAdd)
			ELSE
				fDist2 = TO_FLOAT(iTriggerDistance * iTriggerDistance)
			ENDIF
			
			IF VDIST2(vPed, vEntity) <= fDist2
				PRINTLN("[RCC MISSION] IS_PED_WITHIN_RANGE_OF_CUSTOM_TARGET_ENTITY - Returning TRUE for ped ",iped," & iCustomType ",iCustomType,", iCustomID ",iCustomID,", bLeaving ",bLeaving)
				bInRange = TRUE
			ELSE
				PRINTLN("[RCC MISSION] IS_PED_WITHIN_RANGE_OF_CUSTOM_TARGET_ENTITY - Returning FALSE for ped ",iped," & iCustomType ",iCustomType,", iCustomID ",iCustomID,", bLeaving ",bLeaving)
			ENDIF
		ENDIF
	ELSE
		bInRange = TRUE
	ENDIF
	
	RETURN bInRange
	
ENDFUNC

FUNC BOOL IS_PED_WITHIN_RANGE_OF_PRIMARY_CUSTOM_TARGET_ENTITY(PED_INDEX tempPed, INT iped, BOOL bLeaving)
	
	RETURN IS_PED_WITHIN_RANGE_OF_CUSTOM_TARGET_ENTITY(tempPed, iped, bLeaving, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPrimaryActionTriggerDistanceFromTarget, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPrimaryTargetOverrideType, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPrimaryTargetOverrideID)
	
ENDFUNC

FUNC BOOL IS_PED_WITHIN_RANGE_OF_SECONDARY_CUSTOM_TARGET_ENTITY(PED_INDEX tempPed, INT iped, BOOL bLeaving)
	
	RETURN IS_PED_WITHIN_RANGE_OF_CUSTOM_TARGET_ENTITY(tempPed, iped, bLeaving, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSecondaryActionTriggerDistanceFromTarget, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSecondaryTargetOverrideType, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSecondaryTargetOverrideID)
	
ENDFUNC

FUNC BOOL IS_CUSTOM_TARGET_ENTITY_DEAD(PED_INDEX tempPed, INT iCustomType, INT iCustomID)
	
	SWITCH iCustomType
		
		CASE ciRULE_TYPE_GOTO
			IF iCustomID < FMMC_MAX_GO_TO_LOCATIONS
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE ciRULE_TYPE_PLAYER
			IF iCustomID < FMMC_MAX_TEAMS
				IF MC_serverBD.iNumberOfPlayingPlayers[iCustomID] > 0
					
					INT iPlayer
					iPlayer = GET_NEAREST_PLAYER_TO_ENTITY_ON_TEAM(tempPed, iCustomID)
					
					IF iPlayer != -1
						
						PLAYER_INDEX tempPlayer
						tempPlayer = INT_TO_PLAYERINDEX(iPlayer)
						
						IF NOT IS_PLAYER_DEAD(tempPlayer)
							RETURN FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ciRULE_TYPE_OBJECT
			IF iCustomID < FMMC_MAX_NUM_OBJECTS
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iCustomID])
					
					OBJECT_INDEX tempObj
					tempObj = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iCustomID])
					
					IF IS_ENTITY_ALIVE(tempObj)
						IF IS_BIT_SET(MC_serverBD.iFragableCrate, iCustomID)
						OR IS_THIS_MODEL_A_CCTV_CAMERA(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCustomID].mn)
							IF NOT IS_OBJECT_BROKEN_AND_VISIBLE(tempObj)
								RETURN FALSE
							ENDIF
						ELSE
							RETURN FALSE
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_BIT_SET(MC_serverBD_2.iObjSpawnedOnce, iCustomID)
						// If the object hasn't spawned yet, it can't have died
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ciRULE_TYPE_PED
			IF iCustomID < FMMC_MAX_PEDS
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iCustomID])
					
					PED_INDEX targetPed
					targetPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iCustomID])
					
					IF NOT IS_PED_INJURED(targetPed)
						RETURN FALSE
					ENDIF
				ELSE
					IF NOT IS_BIT_SET(MC_serverBD.iObjSpawnPedBitset[GET_LONG_BITSET_INDEX(iCustomID)], GET_LONG_BITSET_BIT(iCustomID))
						// If the ped hasn't spawned yet, it can't have died
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ciRULE_TYPE_VEHICLE
			IF iCustomID < FMMC_MAX_VEHICLES
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iCustomID])
					
					VEHICLE_INDEX tempVeh
					tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iCustomID])
					
					IF IS_VEHICLE_DRIVEABLE(tempVeh)
						RETURN FALSE
					ENDIF
				ELSE
					IF NOT IS_BIT_SET(MC_serverBD_2.iVehSpawnedOnce, iCustomID)
						// If the vehicle hasn't spawned yet, it can't have died
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL IS_PRIMARY_CUSTOM_TARGET_ENTITY_IN_CORRECT_ALIVE_STATE_FOR_TASK(PED_INDEX ThisPed, INT iped, BOOL bLeaving)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPrimaryTargetOverrideType = ciRULE_TYPE_NONE
	OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPrimaryTargetOverrideID = -1
		RETURN TRUE
	ENDIF
	
	IF NOT bLeaving
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEleven, ciPED_BSEleven_ActivatePrimaryTaskWhenCustomTargetDead)
			IF NOT IS_CUSTOM_TARGET_ENTITY_DEAD(ThisPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPrimaryTargetOverrideType, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPrimaryTargetOverrideID)
				RETURN FALSE
			ENDIF
		ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEleven, ciPED_BSEleven_DeactivatePrimaryTaskWhenCustomTargetDead)
			IF IS_CUSTOM_TARGET_ENTITY_DEAD(ThisPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPrimaryTargetOverrideType, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPrimaryTargetOverrideID)
				RETURN FALSE
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEleven, ciPED_BSEleven_DeactivatePrimaryTaskWhenCustomTargetDead)
			IF IS_CUSTOM_TARGET_ENTITY_DEAD(ThisPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPrimaryTargetOverrideType, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPrimaryTargetOverrideID)
				PRINTLN("[RCC MISSION] IS_PRIMARY_CUSTOM_TARGET_ENTITY_IN_CORRECT_ALIVE_STATE_FOR_TASK - Returning FALSE for ped ",iped," w bLeaving TRUE, target is dead")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL IS_PRIMARY_CUSTOM_TARGET_ENTITY_IN_CORRECT_STATE_FOR_TASK(INT iped, PED_INDEX ThisPed, BOOL bLeaving)
	
	IF NOT bLeaving
		IF NOT IS_PED_WITHIN_RANGE_OF_PRIMARY_CUSTOM_TARGET_ENTITY(ThisPed, iped, FALSE)
			RETURN FALSE
		ENDIF
		
		IF HAS_PLAYER_GOT_CONTROL_OF_PRIMARY_CUSTOM_TARGET_ENTITY(iped)
			RETURN FALSE
		ENDIF
		
		IF NOT IS_PRIMARY_CUSTOM_TARGET_ENTITY_IN_CORRECT_ALIVE_STATE_FOR_TASK(ThisPed, iped, FALSE)
			RETURN FALSE
		ENDIF
	ELSE
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEleven, ciPED_BSEleven_DeactivatePrimaryTaskWhenOutOfTriggerDistance)
			IF NOT IS_PED_WITHIN_RANGE_OF_PRIMARY_CUSTOM_TARGET_ENTITY(ThisPed, iped, TRUE)
				PRINTLN("[RCC MISSION] IS_PRIMARY_CUSTOM_TARGET_ENTITY_IN_CORRECT_STATE_FOR_TASK - Returning FALSE for ped ",iped," w bLeaving TRUE, out of range of target")
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF HAS_PLAYER_GOT_CONTROL_OF_PRIMARY_CUSTOM_TARGET_ENTITY(iped) // Checks if the option to do this is set before doing anything silly
			PRINTLN("[RCC MISSION] IS_PRIMARY_CUSTOM_TARGET_ENTITY_IN_CORRECT_STATE_FOR_TASK - Returning FALSE for ped ",iped," w bLeaving TRUE, player has control of target")
			RETURN FALSE
		ENDIF
		
		IF NOT IS_PRIMARY_CUSTOM_TARGET_ENTITY_IN_CORRECT_ALIVE_STATE_FOR_TASK(ThisPed, iped, TRUE)
			PRINTLN("[RCC MISSION] IS_PRIMARY_CUSTOM_TARGET_ENTITY_IN_CORRECT_STATE_FOR_TASK - Returning FALSE for ped ",iped," w bLeaving TRUE, target is not in correct alive state")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL SHOULD_PED_STOP_TASKS(INT iped)
	
	BOOL bStopTasks = FALSE
	
	INT iTeam = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iStopTasksTeam
	INT iRule = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iStopTasksRule
	
	IF iTeam != -1
	AND iRule != -1
		IF MC_serverBD_4.iCurrentHighestPriority[iTeam] >= iRule			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwelve, ciPed_BSTwelve_ClearPedTasksWhenTrailerDetached)
				IF IS_BIT_SET(iPedTrailerDetachedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					bStopTasks = TRUE
				ENDIF
			ELSE
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour, ciPED_BSFour_StopTasks_OnMidpoint)
					IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], iRule)
						bStopTasks = TRUE
					ENDIF
				ELSE
					bStopTasks = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bStopTasks
	
ENDFUNC

PROC MANAGE_PED_TASK_ACTIVATION(INT iped,PED_INDEX ThisPed)

	BOOL bActivateTask,bSightCheck
	INT itempPlayer
	PLAYER_INDEX tempPlayer
	PED_INDEX PlayerPed
	
	IF SHOULD_STAY_IN_FIXATION_STATE(iped)
		PRINTLN("[RCC MISSION] MANAGE_PED_TASK_ACTIVATION - PED: ",iped, " EXITTING - We are currently FIXATED.")
		EXIT
	ENDIF
	
	PRINTLN("[RCC MISSION] MANAGE_PED_TASK_ACTIVATION - PED: ", iped, " Checking if we should activate tasks.")
	
	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vActivationAAPos1)
	AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vActivationAAPos2)
	AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fActivationAAWidth != 0)
		
		itempPlayer = GET_NEAREST_PLAYER_TO_ENTITY(ThisPed)
		tempPlayer = INT_TO_PLAYERINDEX(itempPlayer)
		IF tempPlayer != INVALID_PLAYER_INDEX()
			IF IS_NET_PLAYER_OK(tempPlayer)
				PlayerPed = GET_PLAYER_PED(tempPlayer)
				IF IS_ENTITY_IN_ANGLED_AREA(PlayerPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vActivationAAPos1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vActivationAAPos2, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fActivationAAWidth)
				OR SHOULD_PED_START_COMBAT(ThisPed, iped)
					
					#IF IS_DEBUG_BUILD
					IF NOT IS_ENTITY_IN_ANGLED_AREA(PlayerPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vActivationAAPos1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vActivationAAPos2, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fActivationAAWidth)
						PRINTLN("[RCC MISSION] MANAGE_PED_TASK_ACTIVATION - ped ",iped,"; nearest player isn't in activation area , but ped should start combat!")
					ENDIF
					#ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_ActivationSight)
						IF IS_TARGET_PED_IN_PERCEPTION_AREA(ThisPed,PlayerPed)
						AND HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(iPed, ThisPed, PlayerPed, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)
							bSightCheck = TRUE
						ENDIF
					ELSE
						bSightCheck = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iActivationRange != 0
		
		itempPlayer = GET_NEAREST_PLAYER_TO_ENTITY(ThisPed)
		tempPlayer = INT_TO_PLAYERINDEX(itempPlayer)
		IF tempPlayer != INVALID_PLAYER_INDEX()
			IF IS_NET_PLAYER_OK(tempPlayer)
				PlayerPed = GET_PLAYER_PED(tempPlayer)
				
				FLOAT fActivateSquared = TO_SQUARED_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iActivationRange)
				FLOAT fDist2 
				
				IF IS_LOCAL_PLAYER_USING_DRONE()
					OBJECT_INDEX oiDrone = GET_LOCAL_DRONE_PROP_OBJ_INDEX()
					fDist2 = VDIST2(GET_ENTITY_COORDS(ThisPed, FALSE), GET_ENTITY_COORDS(oiDrone))
					
					IF fDist2 <= fActivateSquared
					OR SHOULD_PED_START_COMBAT(ThisPed, iped)
						
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_ActivationSight)
							IF HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(iPed, ThisPed, oiDrone, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)
								bSightCheck = TRUE
							ENDIF
						ELSE
							bSightCheck = TRUE
						ENDIF
					ENDIF
				
				ENDIF
				
				
				//Awareness checks will not trigger if already triggered from the drone.
				IF NOT bSightCheck
					fDist2 = VDIST2(GET_ENTITY_COORDS(ThisPed, FALSE), GET_ENTITY_COORDS(PlayerPed))
				
					IF (fDist2 <= fActivateSquared)
					OR SHOULD_PED_START_COMBAT(ThisPed, iped)
						
						#IF IS_DEBUG_BUILD
						IF (GET_DISTANCE_BETWEEN_PEDS(ThisPed,PlayerPed) > g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iActivationRange)
							PRINTLN("[RCC MISSION] MANAGE_PED_TASK_ACTIVATION - ped ",iped,"; nearest player isn't close enough (should be within ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iActivationRange,"m), but ped should start combat!")
						ENDIF
						#ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_ActivationSight)
							IF IS_TARGET_PED_IN_PERCEPTION_AREA(ThisPed,PlayerPed)
							AND HAS_PED_GOT_CLEAR_LINE_OF_SIGHT_TO_ENTITY(iPed, ThisPed, PlayerPed, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)
								bSightCheck = TRUE
							ENDIF
						ELSE
							bSightCheck = TRUE
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
		
	ELSE
		PRINTLN("[RCC MISSION] MANAGE_PED_TASK_ACTIVATION - PED: ", iped, " default case bSightCheck = TRUE.")
		bSightCheck = TRUE
	ENDIF
	
	
	IF bSightCheck
		IF HAS_ANY_TEAM_HIT_PED_TASK_ACTIVATION(iped)
			PRINTLN("[RCC MISSION] MANAGE_PED_TASK_ACTIVATION - PED: ", iped)
			PRINTLN("[RCC MISSION] MANAGE_PED_TASK_ACTIVATION - g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedTeam ", 		 		g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedTeam)
			PRINTLN("[RCC MISSION] MANAGE_PED_TASK_ACTIVATION - g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedObjective ", 			g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedObjective)
			PRINTLN("[RCC MISSION] MANAGE_PED_TASK_ACTIVATION - g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedActionStart ", 		g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedActionStart)
			PRINTLN("[RCC MISSION] MANAGE_PED_TASK_ACTIVATION - g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedScoreRequired ",		g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedScoreRequired)
			PRINTLN("[RCC MISSION] MANAGE_PED_TASK_ACTIVATION - g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSecondAssociatedTeam ", 		g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSecondAssociatedTeam)
			PRINTLN("[RCC MISSION] MANAGE_PED_TASK_ACTIVATION - g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSecondAssociatedObjective ", 	g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSecondAssociatedObjective)
			PRINTLN("[RCC MISSION] MANAGE_PED_TASK_ACTIVATION - g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iThirdAssociatedTeam ", 			g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iThirdAssociatedTeam)
			PRINTLN("[RCC MISSION] MANAGE_PED_TASK_ACTIVATION - g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iThirdAssociatedObjective ", 	g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iThirdAssociatedObjective)
			PRINTLN("[RCC MISSION] MANAGE_PED_TASK_ACTIVATION - g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFourthAssociatedTeam ", 		g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFourthAssociatedTeam)
			PRINTLN("[RCC MISSION] MANAGE_PED_TASK_ACTIVATION - g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFourthAssociatedObjective ", 	g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFourthAssociatedObjective)
			
			bActivateTask = TRUE
		ELSE
			PRINTLN("[RCC MISSION] MANAGE_PED_TASK_ACTIVATION - PED: ", iped, " HAS_ANY_TEAM_HIT_PED_TASK_ACTIVATION = FALSE.")
		ENDIF
	ENDIF
	
	IF bActivateTask
	AND IS_PRIMARY_CUSTOM_TARGET_ENTITY_IN_CORRECT_STATE_FOR_TASK(iped, ThisPed, FALSE)
		
		SET_BIT(MC_serverBD.iPedTaskActive[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		PRINTLN("[RCC MISSION] MANAGE_PED_TASK_ACTIVATION - Task Activation Triggered for ped: ",iped)
		
		MC_serverBD_2.iAssociatedAction[iped] = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedAction
		
		IF SHOULD_PED_STOP_TASKS(iped)
			PRINTLN("[RCC MISSION] MANAGE_PED_TASK_ACTIVATION - Tasks should stop immediately!! for ped: ",iped)
			SET_BIT(MC_serverBD.iPedTaskStopped[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			SET_BIT(MC_serverBD.iPedAtGotoLocBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		ELSE
			IF MC_serverBD_2.iPedState[iped] != ciTASK_CHOOSE_NEW_TASK
				IF NOT IS_PED_IN_COMBAT(ThisPed)
				AND CAN_PED_TASKS_BE_CLEARED(ThisPed)
					CLEAR_PED_TASKS(ThisPed)
				ENDIF
				PRINTLN("[RCC MISSION] MANAGE_PED_TASK_ACTIVATION - clearing ped task, Task Activation Triggered: ",iped) 
				SET_PED_STATE(iped,ciTASK_CHOOSE_NEW_TASK)
			ENDIF
		ENDIF
		
	ENDIF

ENDPROC

FUNC BOOL DOES_VEHICLE_PASS_SECONDARY_TASK_ACTIVATION_CONDITIONS(INT iPed, PED_INDEX ThisPed)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFourteen, ciPED_BSFourteen_SecondaryGoto_WaitForVehToBeEmptyOfPlayers)
		IF IS_PED_IN_ANY_VEHICLE(thisPed)
			VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(thisPed)
			IF IS_VEHICLE_DRIVEABLE(tempVeh)
				PED_INDEX pedInVeh
				INT iSeat
				INT iPassengerSeats = GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(tempVeh)
				
				FOR iSeat = -1 TO (iPassengerSeats - 1)
					pedInVeh = GET_PED_IN_VEHICLE_SEAT(tempVeh, INT_TO_ENUM(VEHICLE_SEAT, iSeat))
					
					IF NOT IS_PED_INJURED(pedInVeh)
						IF IS_PED_A_PLAYER(pedInVeh)
							RETURN FALSE
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC MANAGE_PED_SECONDARY_TASK_ACTIVATION(INT iped, PED_INDEX ThisPed)
	
	IF HAS_ANY_TEAM_HIT_PED_SECONDARY_TASK_ACTIVATION(iped)
	AND IS_PED_WITHIN_RANGE_OF_SECONDARY_CUSTOM_TARGET_ENTITY(ThisPed, iped, FALSE)
	AND (NOT HAS_PLAYER_GOT_CONTROL_OF_SECONDARY_CUSTOM_TARGET_ENTITY(iped))
	AND DOES_VEHICLE_PASS_SECONDARY_TASK_ACTIVATION_CONDITIONS(iPed, ThisPed)
		SET_BIT(MC_serverBD.iPedSecondaryTaskActive[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		PRINTLN("[RCC MISSION] Secondary Task Activation Triggered for ped: ",iped)
		
		MC_ServerBD_2.iAssociatedAction[iped] = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSecondaryAction
		MC_serverBD.niTargetID[iped] = NULL
		MC_serverBD_2.iTargetID[iped] = -1
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].mn = PLAYER_TWO
			CLEAR_BIT(ipedAnimBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			CLEAR_BIT(iPedPerformingSpecialAnimBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			CLEAR_BIT(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			
			BROADCAST_FMMC_ANIM_STOPPED(iped, TRUE, TRUE)
		ENDIF
		
		IF MC_serverBD_2.iPedState[iped] != ciTASK_CHOOSE_NEW_TASK
			
			IF NOT IS_PED_IN_COMBAT(ThisPed)
			AND CAN_PED_TASKS_BE_CLEARED(ThisPed)
				CLEAR_PED_TASKS(ThisPed)
			ENDIF
			
			PRINTLN("[RCC MISSION] clearing ped task, Secondary Task Activation Triggered: ",iped)
			SET_PED_STATE(iped,ciTASK_CHOOSE_NEW_TASK)
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_SERVER_PED_CANCEL_TASKS(INT iped)
	
	IF NOT IS_BIT_SET(MC_serverBD.iPedCancelTasksBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		SET_BIT(MC_serverBD.iPedCancelTasksBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		
		PRINTLN("[RCC MISSION] PROCESS_SERVER_PED_CANCEL_TASKS - Ped ",iped," cancelling tasks to get into combat...")
		MC_ServerBD_2.iAssociatedAction[iped] = ciACTION_NOTHING
		MC_serverBD.niTargetID[iped] = NULL
		MC_serverBD_2.iTargetID[iped] = -1
		SET_PED_STATE(iped,ciTASK_GENERAL_COMBAT #IF IS_DEBUG_BUILD , 4 #ENDIF )
		SET_BIT(MC_serverBD.iPedAtGotoLocBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
	ENDIF
	
ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: PED GOTO COMPLETION CHECKS (also check tasks they must perform at their destination: rappelling, waiting, etc) !
//
//************************************************************************************************************************************************************



FUNC BOOL SHOULD_PED_GETOUT(INT iped)
	
	BOOL bShouldGetOut = FALSE
	
	PED_INDEX tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
	
	IF IS_PED_IN_ANY_VEHICLE(tempPed)
		IF MC_serverBD_2.iTargetID[iped] != ciSECONDARY_GOTO
			IF (NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset,ciPED_BS_DontGetOutAtEndOfGoto) )
			AND (MC_serverBD_2.iPedGotoProgress[iped] >= (GET_ASSOCIATED_GOTO_TASK_DATA__NUMBER_OF_GOTOS(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, FALSE) - 1)) //If the ped is reaching the end of their gotos
			AND NOT (IS_ASSOCIATED_GOTO_TASK_DATA__ENTIRE_BIT_SET(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData, ciENTIRE_TASK_BITSET_LOOP_GOTO)) //If they shouldn't continue looping through them
			AND NOT ( (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fRange = cfMAX_PATROL_RANGE) OR (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFlee = ciPED_FLEE_ON) ) //If they aren't set to wander/flee at the end
				MODEL_NAMES mnVeh = GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_USING(tempPed))
				IF NOT (IS_THIS_MODEL_A_BOAT(mnVeh) OR (mnVeh = SUBMERSIBLE) ) //If they're not in a boat/sub
					bShouldGetOut = TRUE //They should get out!
				ENDIF
			ENDIF
		ELSE
			IF (NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset,ciPED_BS_DontGetOutAtEndOfGoto) )
			AND NOT (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetNine, ciPed_BSNine_DontExitOnSecondaryArrival) AND IS_BIT_SET(MC_serverBD.iPedSecondaryTaskActive[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed)))
				MODEL_NAMES mnVeh = GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_USING(tempPed))
				IF NOT (IS_THIS_MODEL_A_BOAT(mnVeh) OR (mnVeh = SUBMERSIBLE) ) //If they're not in a boat/sub
					bShouldGetOut = TRUE //They should get out!
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPed_BSEight_NeverLeaveVehicle)
		PRINTLN("[RCC MISSION] SHOULD_PED_GETOUT - iPed: ", iPed, " Forcing bShouldGetOut to FALSE due to ciPed_BSEight_NeverLeaveVehicle")
		bShouldGetOut = FALSE
	ENDIF
	
	PRINTLN("[RCC MISSION] SHOULD_PED_GETOUT - Returning: ", BOOL_TO_STRING(bShouldGetOut))
	RETURN bShouldGetOut
	
ENDFUNC

FUNC BOOL IS_RAPPEL_SEAT_FREE(VEHICLE_INDEX tempVeh, VEHICLE_SEAT eSeat, INT iPed)
	
	INT iSeat = ENUM_TO_INT(eSeat)
	iSeat += 1 // VEHICLE_SEAT enum begins at -1, but iVehicleRappelSeatsBitset begins at 0
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle > -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle < (FMMC_MAX_VEHICLES - 1)
			
			IF NOT IS_BIT_SET(MC_serverBD.iVehicleRappelSeatsBitset[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle], iSeat)
				PRINTLN("[RCC MISSION] ped ", iPed, " IS_RAPPEL_SEAT_FREE - bit not set.")
				RETURN TRUE
			ENDIF
			
			IF IS_VEHICLE_SEAT_FREE(tempVeh, eSeat)
				PRINTLN("[RCC MISSION] ped ", iPed, " IS_RAPPEL_SEAT_FREE - seat is free.")
				RETURN TRUE
			ENDIF
			
		ENDIF
	ENDIF
	
	PRINTLN("[RCC MISSION] ped ", iPed, " IS_RAPPEL_SEAT_FREE - bit set but seat not free.")
	RETURN FALSE
	
ENDFUNC

FUNC BOOL HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION(PED_INDEX ThisPed, INT iPed)
	
	BOOL bArrived = FALSE
	VECTOR vGotoCoords
//	INT iGotoSize
	FLOAT fGotoSize
	FLOAT fCustomGotoSize
	VEHICLE_INDEX tempVeh
 
	IF MC_serverBD_2.iTargetID[iped] = ciINITIAL_GOTO
		
		vGotoCoords = GET_ASSOCIATED_GOTO_TASK_DATA__POSITION(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed])
		
		//-- Each goto can now have it's own arrived-at-destination radius
		//-- Will use this value if it's non-zero, otherwise use sAssociatedGotoTaskData.iArrivalRadius
		fCustomGotoSize = GET_ASSOCIATED_GOTO_TASK_DATA__CUSTOM_ARRIVAL_RADIUS(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed])
		
		PRINTLN("[RCC MISSION] HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - Ped ",iped," Creator setting fCustomGotoSize: ", fCustomGotoSize)
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData.iArrivalRadius != 0
		OR fCustomGotoSize != 0
			IF fCustomGotoSize != 0
				IF (fCustomGotoSize >= 3.0)
				OR (NOT IS_PED_A_HIGH_PRIORITY_PED(iPed))
					fGotoSize = fCustomGotoSize + 3.0
					IF IS_PED_IN_ANY_PLANE(ThisPed)
						fGotoSize += 17.0
					ENDIF
				ELSE
					fGotoSize = fCustomGotoSize + 1.2
					vGotoCoords.z += 1.0 // Add 1m, otherwise it'll be down at their feet and they might not reach it!
				ENDIF
			ELSE
				fGotoSize = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData.iArrivalRadius + 3)
				IF IS_PED_IN_ANY_PLANE(ThisPed)
					fGotoSize += 17.0
				ENDIF
			ENDIF
			
			//Clamp that value at 1/4 of default:
			IF(IS_PED_IN_ANY_VEHICLE(ThisPed))
				
				fGotoSize = MAX_VALUE(fGotoSize, 5.0)
				IF IS_PED_IN_ANY_HELI(ThisPed)
					tempVeh = GET_VEHICLE_PED_IS_IN(ThisPed)
					fGotoSize = MAX_VALUE(fGotoSize, 20.0)
				ENDIF
				
				IF IS_PED_IN_ANY_PLANE(ThisPed)
					fGotoSize = MAX_VALUE(fGotoSize, 38.0)
				ENDIF
				
			ENDIF
			
		ELSE
			
			fGotoSize = 5.0
			IF(IS_PED_IN_ANY_VEHICLE(ThisPed))
				
				fGotoSize = 20.0	
				IF IS_PED_IN_ANY_HELI(ThisPed)
					tempVeh = GET_VEHICLE_PED_IS_IN(ThisPed)
					fGotoSize = 80.0
				ENDIF
				
				IF IS_PED_IN_ANY_PLANE(ThisPed)
				AND NOT IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed], PED_ASSOCIATED_GOTO_TASK_TYPE_PLANETAXI)
					fGotoSize = 150.0
				ENDIF
				
			ENDIF
		ENDIF
		
		VECTOR vPed = GET_ENTITY_COORDS(ThisPed)
		FLOAT fDist = GET_DISTANCE_BETWEEN_COORDS(vPed,vGotoCoords)
		
		PRINTLN("[RCC MISSION] HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - Ped ",iped," at ",vPed," going to ",vGotoCoords,", dist ",fDist," vs fGotoSize ",fGotoSize)
		
		IF fDist < fGotoSize //iGotoSize
		AND WAIT_FORCARGOBOB_DETACH(ThisPed, iped, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle)
			PRINTLN("[RCC MISSION] HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - Ped ",iped," arrived for primary goto; coords ",vGotoCoords)
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_HoverAtGoToDest) // If pilot is supposed to hover, continue doing so until everyone has left the heli.
				IF DOES_ENTITY_EXIST(tempVeh)
				AND IS_RAPPEL_SEAT_FREE(tempVeh, VS_BACK_LEFT, iPed)
				AND IS_RAPPEL_SEAT_FREE(tempVeh, VS_BACK_RIGHT, iPed)
				AND NOT IS_ANY_PED_RAPPELLING_FROM_HELI(tempVeh)
					PRINTLN("[RCC MISSION] HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - Ped ",iped," returning TRUE, hovering at go to & back_left/right seats are empty and nobody is rappelling")
					bArrived = TRUE
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[RCC MISSION] HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - Ped ",iped," returning FALSE, hovering at go to & still waiting for back_left/right seats empty or for peds to finish rappelling")
				#ENDIF
				ENDIF
			ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_RappelAtGoToDest)
				IF NOT IS_PED_IN_ANY_VEHICLE(ThisPed)
				AND SHOULD_PED_BE_GIVEN_TASK(ThisPed, iped, SCRIPT_TASK_RAPPEL_FROM_HELI, TRUE)
					PRINTLN("[RCC MISSION] HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - Ped ",iped," returning TRUE, rappelling at goto & no longer in a vehicle and not performing rappel task")
					bArrived = TRUE
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[RCC MISSION] HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - Ped ",iped," returning FALSE, rappelling at goto & either still in vehicle, or still performing rappel task")
				#ENDIF
				ENDIF
			ELIF IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iped], PED_ASSOCIATED_GOTO_TASK_TYPE_LANDVEH) 
				IF DOES_ENTITY_EXIST(tempVeh)
				AND IS_VEHICLE_ON_ALL_WHEELS(tempVeh)
					PRINTLN("[RCC MISSION] HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - Ped ",iped," returning TRUE, heli is on ground")
					bArrived = TRUE
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[RCC MISSION] HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - Ped ",iped," returning FALSE, heli not on ground")
				#ENDIF
				ENDIF
			ELIF (IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iped], PED_ASSOCIATED_GOTO_TASK_TYPE_EXIT_VEHICLE)
				  OR SHOULD_PED_GETOUT(iped))
				
				IF NOT IS_PED_IN_ANY_VEHICLE(ThisPed)
					PRINTLN("[RCC MISSION] HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - Ped ",iped," returning TRUE, should get out of vehicle & no longer in vehicle")
					bArrived = TRUE
				ELSE
					//If we're leaving due to completion of gotos					
					IF NOT IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iped], PED_ASSOCIATED_GOTO_TASK_TYPE_EXIT_VEHICLE)
						IF IS_COMBAT_VEHICLE(GET_VEHICLE_PED_IS_IN(ThisPed))
						OR IS_PED_IN_ANY_HELI(ThisPed)
						OR IS_PED_IN_ANY_PLANE(ThisPed)
							PRINTLN("[RCC MISSION] HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - Ped ",iped," returning TRUE, done with GoTos and in a vehicle we don't need to get out of")
							bArrived = TRUE
						#IF IS_DEBUG_BUILD
						ELSE
							PRINTLN("[RCC MISSION] HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - Ped ",iped," returning TRUE, done with GoTos but still need to get out of this vehicle")
						#ENDIF
						ENDIF
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("[RCC MISSION] HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - Ped ",iped," returning FALSE, exit bitset is true and still need to get out of this vehicle")
					#ENDIF
					ENDIF
				ENDIF
				
			ELIF (MC_serverBD_2.iPedGotoProgress[iped] < MAX_ASSOCIATED_GOTO_TASKS)
			AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData.sPointData[MC_serverBD_2.iPedGotoProgress[iped]].iWaitTime > 0)
				IF IS_BIT_SET(iPedGotoAssCompletedWaitBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					PRINTLN("[RCC MISSION] HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - Ped ",iped," returning TRUE, done with wait task")
					bArrived = TRUE
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[RCC MISSION] HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - Ped ",iped," returning FALSE, still performing wait task")
				#ENDIF
				ENDIF
			ELIF (MC_serverBD_2.iPedGotoProgress[iPed] < MAX_ASSOCIATED_GOTO_TASKS)
			AND IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed], PED_ASSOCIATED_GOTO_TASK_TYPE_ACHIEVEHEADING) 
				FLOAT fTargetHeadingCheck = GET_ASSOCIATED_GOTO_TASK_DATA__ACHIEVE_FINAL_HEADING(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed]) % 360
				FLOAT fTargetHeadingCheckLower, fTargetHeadingCheckUpper
				FLOAT fCurrentHeadingCheck = GET_ENTITY_HEADING(ThisPed) % 360
				FLOAT fThreshold = 15
				
				IF fTargetHeadingCheck < 0
					fTargetHeadingCheck += 360
				ENDIF
				
				IF fCurrentHeadingCheck < 0
					fCurrentHeadingCheck += 360
				ENDIF
				
				fTargetHeadingCheckLower = fTargetHeadingCheck - fThreshold
				
				fTargetHeadingCheckUpper = fTargetHeadingCheck + fThreshold
				
				IF fCurrentHeadingCheck - 360 > fTargetHeadingCheckLower
					fCurrentHeadingCheck -= 360
				ENDIF
				
				IF fCurrentHeadingCheck + 360 < fTargetHeadingCheckUpper
					fCurrentHeadingCheck += 360
				ENDIF
				
				PRINTLN("[RCC MISSION] HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - Current Heading = ", fCurrentHeadingCheck)
				PRINTLN("[RCC MISSION] HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - Target Heading = ", fTargetHeadingCheck, ", Lower = ", fTargetHeadingCheckLower, ", Upper = ", fTargetHeadingCheckUpper)
				IF (fCurrentHeadingCheck > fTargetHeadingCheckLower AND fCurrentHeadingCheck < fTargetHeadingCheckUpper)
					PRINTLN("[RCC MISSION] HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - Ped ",iped," returning TRUE, done with achieve heading task")
					bArrived = TRUE
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[RCC MISSION] HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - Ped ",iped," returning FALSE, still performing achieve heading task")
				#ENDIF
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION] HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - Ped ",iped," returning TRUE, no special GoTo completion logic")
				bArrived = TRUE
			ENDIF
			
		ENDIF
		
	ELIF MC_serverBD_2.iTargetID[iped] = ciSECONDARY_GOTO // Go To Interrupt
		
		fGotoSize = 5.0
		
		IF(IS_PED_IN_ANY_VEHICLE(ThisPed))
			
			fGotoSize = 20.0	
			IF IS_PED_IN_ANY_HELI(ThisPed)
				tempVeh = GET_VEHICLE_PED_IS_IN(ThisPed)
				fGotoSize = 80.0
			ENDIF
			
			IF IS_PED_IN_ANY_PLANE(ThisPed)
				fGotoSize = 150.0
			ENDIF
			
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData.iArrivalRadius != 0
			fGotoSize = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData.iArrivalRadius + 3)
		ENDIF
		
		vGotoCoords = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vSecondaryVector
		
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(ThisPed),vGotoCoords) < fGotoSize //iGotoSize
			PRINTLN("[RCC MISSION] HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION - Ped ",iped," returning TRUE for secondary goto; vGotoCoords ",vGotoCoords,", distance to coords ",GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(ThisPed),vGotoCoords),", goto size ",fGotoSize)
			bArrived = TRUE
		ENDIF
				
	ENDIF
	
	RETURN bArrived
	
ENDFUNC

///Works out if a ped has finished all their gotos - returns false if not yet, true if they have but have no loop,
/// and false with bGoToIsLooped as true if they have but they're supposed to loop around
FUNC BOOL SHOULD_PED_MOVE_ON_FROM_GOTO_COORDS_STATE(INT iPed, BOOL &bGoToIsLooped, BOOL bProgressServerData = FALSE)
	
	BOOL bShouldMoveOn = TRUE
	INT iTemp
	
	IF IS_BIT_SET(MC_serverBD.iPedAtGotoLocBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		bShouldMoveOn = FALSE
	ENDIF
	
	PRINTLN("[RCC MISSION] SHOULD_PED_MOVE_ON_FROM_GOTO_COORDS_STATE - Ped ",iped," arrived at goto loc (just completed MC_serverBD_2.iPedGotoProgress[iped] = ",MC_serverBD_2.iPedGotoProgress[iped],", bProgressServerData = ",bProgressServerData,")")
	
	INT iLoop
	
	FOR iLoop = 0 TO (MAX_ASSOCIATED_GOTO_TASKS - 1) // Max number of times we might need to do this
		
		IF bProgressServerData
		AND bIsLocalPlayerHost
			MC_serverBD_2.iPedGotoProgress[iped] += 1
			iTemp = MC_serverBD_2.iPedGotoProgress[iped]
		ELSE
			iTemp = MC_serverBD_2.iPedGotoProgress[iped] + 1
		ENDIF
		
		IF iTemp < GET_ASSOCIATED_GOTO_TASK_DATA__NUMBER_OF_GOTOS(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, FALSE)
			
			IF NOT IS_VECTOR_ZERO(GET_ASSOCIATED_GOTO_TASK_DATA__POSITION(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, iTemp))
				PRINTLN("[RCC MISSION] SHOULD_PED_MOVE_ON_FROM_GOTO_COORDS_STATE - Ped ",iped," next goto found (MC_serverBD_2.iPedGotoProgress[iped] = ",iTemp,") is non-zero & valid")
				bShouldMoveOn = FALSE
				iLoop = MAX_ASSOCIATED_GOTO_TASKS // Break out, this vector is non-zero (use it)
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[RCC MISSION] SHOULD_PED_MOVE_ON_FROM_GOTO_COORDS_STATE - Ped ",iped," next goto found (MC_serverBD_2.iPedGotoProgress[iped] = ",iTemp,") had a zero vector!")
				CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION] SHOULD_PED_MOVE_ON_FROM_GOTO_COORDS_STATE - Ped ",iped," next goto found (MC_serverBD_2.iPedGotoProgress[iped] = ",iTemp,") had a zero vector!")
			#ENDIF
			ENDIF
			
		ELSE
			PRINTLN("[RCC MISSION] SHOULD_PED_MOVE_ON_FROM_GOTO_COORDS_STATE - Ped ",iped," has reached placed number of gotos - MC_serverBD_2.iPedGotoProgress[iped] = ",iTemp,", number of placed gotos = ", GET_ASSOCIATED_GOTO_TASK_DATA__NUMBER_OF_GOTOS(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, FALSE))
			iLoop = MAX_ASSOCIATED_GOTO_TASKS // Break out, we're out of vectors
		ENDIF
		
	ENDFOR
	
	IF IS_ASSOCIATED_GOTO_TASK_DATA__ENTIRE_BIT_SET(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData, ciENTIRE_TASK_BITSET_LOOP_GOTO)
	AND bShouldMoveOn
		bGoToIsLooped = TRUE
		bShouldMoveOn = FALSE
	ENDIF
	
	RETURN bShouldMoveOn
			
ENDFUNC



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: PROCESS PED BRAIN !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************



/// PURPOSE: This function runs as a backup if spooked/aggro/cancel tasks event was lost
PROC RUN_SERVER_PED_BACKUP_SPOOK_CANCEL_TASKS_LOGIC(PED_INDEX tempPed, INT iped)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetSix, ciPED_BSSix_SpookOnSpawn)
	AND NOT IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
	AND NOT IS_BIT_SET( iPedHitByStunGun[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
			
		#IF IS_DEBUG_BUILD 
		SET_PED_IS_SPOOKED_DEBUG(iPed)
		#ENDIF
		SET_BIT(iPedLocalSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		SET_BIT(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		PRINTLN("[RCC MISSION][SpookAggro] RUN_SERVER_PED_BACKUP_SPOOK_CANCEL_TASKS_LOGIC - Ped ",iped," spooked as ped is set to spook on spawn!")
		
		BROADCAST_FMMC_PED_SPOOKED(iped)
	ENDIF
	
	//SPOOKED/CANCEL TASKS/AGGRO CHECKS - all this stuff should just be a backup, it should hopefully just run when the event is received
	
	IF IS_BIT_SET(iPedLocalSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		IF NOT IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			//Spooked:
			PRINTLN("[RCC MISSION][SpookAggro] RUN_SERVER_PED_BACKUP_SPOOK_CANCEL_TASKS_LOGIC - Ped ",iped," spooked as iPedLocalSpookedBitset is set")
			#IF IS_DEBUG_BUILD 
			SET_PED_IS_SPOOKED_DEBUG(iPed)
			#ENDIF
			SET_BIT(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		ENDIF
		
		IF MC_serverBD_2.iPedState[iped] != ciTASK_FLEE
			IF NOT DOES_PED_HAVE_VALID_GOTO(iped)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFlee = ciPED_FLEE_ON
					IF MC_serverBD_2.iPartPedFollows[iped] =-1
						IF SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
						AND SHOULD_PED_BE_GIVEN_TASK(tempPed, iped, SCRIPT_TASK_SYNCHRONIZED_SCENE, TRUE)
						OR NOT DOES_ANIMATION_NEED_SPECIAL_BREAKOUT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim)
							SET_PED_STATE(iped,ciTASK_FLEE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ELSE
		IF IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			//Unspooked:
			CLEAR_BIT(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)],GET_LONG_BITSET_BIT(iped))
			
			PRINTLN("[RCC MISSION][SpookAggro] RUN_SERVER_PED_BACKUP_SPOOK_CANCEL_TASKS_LOGIC - Ped ",iped," was spooked on Server, but not locally and we ARE the server so clearing it.")
			
			IF NOT IS_PED_INJURED(tempPed)
				PROCESS_PED_TASK_NOTHING_BRAIN(tempPed,iped)
			ENDIF
			
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,ciPED_BSThree_CancelTasksToCombatPlayers) 
	AND IS_BIT_SET(iPedLocalCancelTasksBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
	AND NOT IS_BIT_SET(MC_serverBD.iPedCancelTasksBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		PROCESS_SERVER_PED_CANCEL_TASKS(iped)
	ENDIF
	
	IF IS_BIT_SET(iPedLocalAggroedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
	AND NOT IS_BIT_SET(MC_serverBD.iPedAggroedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		//Aggro:
		TRIGGER_AGGRO_SERVERDATA_ON_PED(tempPed, iped)
	ENDIF
	
ENDPROC

FUNC BOOL DOES_ANY_TEAM_HAVE_PHOTO_DEAD_RULE(INT iped)

INT iteam

	FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
		IF IS_BIT_SET(MC_serverBD.iDeadPedPhotoBitset[iteam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			RETURN TRUE
		ENDIF
	ENDFOR

	RETURN FALSE

ENDFUNC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: SERVER PED GROUPING CHECKS !
//
//************************************************************************************************************************************************************



FUNC BOOL SHOULD_PED_COUNT_AS_HELD_FOR_TEAM(INT iped, INT iteam)

	IF MC_serverBD_4.iPedRule[iped][iteam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
	OR MC_serverBD_4.iPedRule[iped][iteam] = FMMC_OBJECTIVE_LOGIC_CAPTURE	
	OR MC_serverBD_4.iPedRule[iped][iteam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD	
		IF MC_serverBD_4.iPedPriority[iped][iteam]  <= MC_serverBD_4.iCurrentHighestPriority[iteam]
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC MANAGE_SERVER_CAPTURE_BY_DISTANCE_EVERY_FRAME(INT iPed)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPED_BSEight_UseDistanceToCapture)	 	
		PARTICIPANT_INDEX tempPart
		PLAYER_INDEX tempPlayer
		INT iteam
		INT ipart
		INT igroupteam
		INT ipriority[FMMC_MAX_TEAMS]
		BOOL bwasobjective[FMMC_MAX_TEAMS]
		
		INT iPartsChecked = 0
		INT iTotalPartsToCheck = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]
		#IF IS_DEBUG_BUILD
		IF bJS3226522
			PRINTLN("[JS] MANAGE_SERVER_CAPTURE_BY_DISTANCE_EVERY_FRAME - iTotalPartsToCheck: ", iTotalPartsToCheck)
		ENDIF
		#ENDIF
		INT iPartThatHasControl = -1

		IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
			
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPED_BSEight_ResetCaptureOnOutOfRange)
			AND HAS_NET_TIMER_STARTED(MC_serverBD.tdControlPedTimerOffset[iped])
				PRINTLN("[JS] MANAGE_SERVER_CAPTURE_BY_DISTANCE_EVERY_FRAME Timer: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.tdControlPedTimer[iped]))
				PRINTLN("[JS] MANAGE_SERVER_CAPTURE_BY_DISTANCE_EVERY_FRAME Offset Timer: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdControlPedTimerOffset[iped]))
				MC_serverBD_1.tdControlPedTimer[iped].Timer = GET_TIME_OFFSET(MC_serverBD_1.tdControlPedTimer[iped].Timer, GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdControlPedTimerOffset[iped]))
				PRINTLN("[JS] MANAGE_SERVER_CAPTURE_BY_DISTANCE_EVERY_FRAME New Timer: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.tdControlPedTimer[iped]))
				RESET_NET_TIMER(MC_serverBD.tdControlPedTimerOffset[iped])
			ENDIF
			
			INT iPartToCheck = -1
			IF NOT IS_BIT_SET(MC_serverBD_4.iCapturePedRequestPartBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				//Check who asked to hack
				FOR iPartToCheck = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
					IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartToCheck))
					AND NOT (IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet, PBBOOL_ANY_SPECTATOR)
						OR IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
					AND NOT IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet,PBBOOL_PLAYER_FAIL)
					AND IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
					AND NOT IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet, PBBOOL_FINISHED)
						iPartsChecked++
						IF IS_BIT_SET(MC_playerBD[iPartToCheck].iRequestCapturePedBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							SET_BIT(MC_serverBD_4.iCapturePedRequestPartBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							igroupteam = MC_playerBD[iPartToCheck].iteam
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPed_BSEight_SpookOnHack)
							AND NOT IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
								PRINTLN("[JS] MANAGE_SERVER_CAPTURE_BY_DISTANCE_EVERY_FRAME - Spooking ped ", iped)
								SET_BIT(iPedLocalSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
								BROADCAST_FMMC_PED_SPOOKED(iped, ciSPOOK_PLAYER_CAR_CRASH, iPartToCheck)
							ENDIF
							PRINTLN("[JS] MANAGE_SERVER_CAPTURE_BY_DISTANCE_EVERY_FRAME - Participant: ", iPartToCheck," has requested to capture the ped: ", iped)
							BREAKLOOP
						ENDIF
						IF iPartsChecked >= iTotalPartsToCheck
							#IF IS_DEBUG_BUILD
							IF bJS3226522
								PRINTLN("[JS] MANAGE_SERVER_CAPTURE_BY_DISTANCE_EVERY_FRAME - Parts Checked ", iPartsChecked)
							ENDIF
							#ENDIF
							BREAKLOOP
						ENDIF
					ELSE
						IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartToCheck))
							#IF IS_DEBUG_BUILD
							IF bJS3226522
								PRINTLN("[JS] MANAGE_SERVER_CAPTURE_BY_DISTANCE_EVERY_FRAME - Participant: ", iPartToCheck," NOT ACTIVE ped: ", iped)
							ENDIF
							#ENDIF
						ENDIF
						IF (IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet, PBBOOL_ANY_SPECTATOR)
						OR IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
							#IF IS_DEBUG_BUILD
							IF bJS3226522
								PRINTLN("[JS] MANAGE_SERVER_CAPTURE_BY_DISTANCE_EVERY_FRAME - Participant: ", iPartToCheck," SPECTATING ped: ", iped)
							ENDIF
							#ENDIF
						ENDIF
					ENDIF
				ENDFOR
			ELSE
				BOOL bPartHasControl = FALSE
				FOR iPartToCheck = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
					IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartToCheck))
					AND NOT (IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet, PBBOOL_ANY_SPECTATOR)
						OR IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
					AND NOT IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet,PBBOOL_PLAYER_FAIL)
					AND IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
					AND NOT IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet, PBBOOL_FINISHED)
						iPartsChecked++
						IF IS_BIT_SET(MC_playerBD[iPartToCheck].iRequestCapturePedBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
							iPartThatHasControl = iPartToCheck
							bPartHasControl = TRUE
							PRINTLN("[JS] MANAGE_SERVER_CAPTURE_BY_DISTANCE_EVERY_FRAME - Participant: ", iPartToCheck," is still in control - ped: ", iped)
							BREAKLOOP
						ENDIF
						IF iPartsChecked >= iTotalPartsToCheck
							#IF IS_DEBUG_BUILD
							IF bJS3226522
								PRINTLN("[JS] MANAGE_SERVER_CAPTURE_BY_DISTANCE_EVERY_FRAME - Parts Checked ", iPartsChecked)
							ENDIF
							#ENDIF
							BREAKLOOP
						ENDIF
					ELSE
						IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartToCheck))
							#IF IS_DEBUG_BUILD
							IF bJS3226522
								PRINTLN("[JS] MANAGE_SERVER_CAPTURE_BY_DISTANCE_EVERY_FRAME - Participant: ", iPartToCheck," NOT ACTIVE ped: ", iped)
							ENDIF
							#ENDIF
						ENDIF
						IF (IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet, PBBOOL_ANY_SPECTATOR)
						OR IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
							#IF IS_DEBUG_BUILD
							IF bJS3226522
								PRINTLN("[JS] MANAGE_SERVER_CAPTURE_BY_DISTANCE_EVERY_FRAME - Participant: ", iPartToCheck," SPECTATING ped: ", iped)
							ENDIF
							#ENDIF
						ENDIF
					ENDIF
				ENDFOR
				IF !bPartHasControl
					PRINTLN("[JS] MANAGE_SERVER_CAPTURE_BY_DISTANCE_EVERY_FRAME - Participant: ", iPartToCheck," has gone out of range, clearing bits - ped: ", iped)
					CLEAR_BIT(MC_serverBD_4.iCapturePedRequestPartBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				ENDIF
			ENDIF

			IF iPartThatHasControl > -1
			
				tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartThatHasControl))
			
				IF NATIVE_TO_INT(tempPlayer) !=-1
					IF IS_NET_PLAYER_OK(tempPlayer)
						
						tempPart = NETWORK_GET_PARTICIPANT_INDEX(tempPlayer)
						
						IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
							
							ipart = NATIVE_TO_INT(tempPart)
							
							IF MC_serverBD_2.iPartPedFollows[iped] != ipart
								IF IS_BIT_SET(MC_playerBD[ipart].iPedFollowingBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
								AND NOT IS_BIT_SET(MC_playerBD[ipart].iClientBitSet,PBBOOL_FINISHED)
								AND NOT IS_BIT_SET(MC_playerBD[ipart].iClientBitSet,PBBOOL_START_SPECTATOR)
									PRINTLN("[JS] MANAGE_SERVER_CAPTURE_BY_DISTANCE_EVERY_FRAME - Setting ped ",iped," with MC_serverBD_2.iPartPedFollows[iped] = ",ipart,", old iPartPedFollows = ",MC_serverBD_2.iPartPedFollows[iped])
									MC_serverBD_2.iPartPedFollows[iped] = ipart
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			EXIT
		ENDIF
		
		IF NOT IS_BIT_SET(MC_serverBD_4.iPedExplodeKillOnCaptureBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)) 
			IF IS_PED_IN_GROUP(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped]))
			OR iPartThatHasControl != -1
				IF MC_serverBD_2.iPartPedFollows[iped] != -1
					
					tempPart = INT_TO_PARTICIPANTINDEX(MC_serverBD_2.iPartPedFollows[iped])
					
					IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
					AND NOT IS_BIT_SET(MC_playerBD[MC_serverBD_2.iPartPedFollows[iped]].iClientBitSet,PBBOOL_FINISHED)
						
						tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
						
						IF IS_NET_PLAYER_OK(tempPlayer)
							
							igroupteam = MC_playerBD[MC_serverBD_2.iPartPedFollows[iped]].iteam
							
							IF SHOULD_PED_COUNT_AS_HELD_FOR_TEAM(iped,igroupteam)
							OR iPartThatHasControl != -1
								iNumHighPriorityPedHeld[igroupteam]++
								
								FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
									IF iteam != igroupteam
										IF DOES_TEAM_LIKE_TEAM(iteam,igroupteam)
											IF SHOULD_PED_COUNT_AS_HELD_FOR_TEAM(iped,iteam)
											OR iPartThatHasControl != -1
												iNumHighPriorityPedHeld[iteam]++
											ENDIF
										ENDIF
									ENDIF
								ENDFOR
							ENDIF
							
							IF MC_serverBD_4.iPedRule[iped][igroupTeam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
								IF MC_serverBD_2.iOldPartPedFollows[iped] = MC_serverBD_2.iPartPedFollows[iped] 
									IF MC_serverBD_4.iPedPriority[iped][igroupTeam] < FMMC_MAX_RULES
										IF NOT HAS_NET_TIMER_STARTED(MC_serverBD_1.tdControlPedTimer[iped])
											START_NET_TIMER(MC_serverBD_1.tdControlPedTimer[iped])
											NET_PRINT("[JS] MANAGE_SERVER_CAPTURE_BY_DISTANCE_EVERY_FRAME starting control timer for ped: ")  NET_PRINT_INT(iped) NET_NL()
										ELSE
											IF (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.tdControlPedTimer[iped]) > MC_serverBD.iPedTakeoverTime[igroupTeam][MC_serverBD_4.iPedPriority[iped][igroupTeam]])
												BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM_CON_PED,0,igroupTeam,-1,tempplayer,ci_TARGET_PED,iped)
												//BROADCAST_GIVE_PLAYER_XP(tempPlayer,200,-1,TRUE)
												INCREMENT_SERVER_REMOTE_PLAYER_SCORE(tempPlayer, GET_FMMC_POINTS_FOR_TEAM(igroupTeam,MC_serverBD_4.iPedPriority[iped][igroupTeam]), igroupTeam, MC_serverBD_4.iPedPriority[iped][igroupTeam])

												IF MC_serverBD.iRuleReCapture[igroupTeam][MC_serverBD_4.iPedPriority[iped][igroupTeam]] <= 0
													IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFollow = ciPED_FOLLOW_OFF
														PRINTLN("[JS] MANAGE_SERVER_CAPTURE_BY_DISTANCE_EVERY_FRAME Control ped ",iped," getting cleaned up as iRuleReCapture is zero for team ",igroupTeam,", resetting MC_serverBD_2.iPartPedFollows[iped] to -1")
														CLEAR_BIT(MC_serverBD.iPedCleanup_NeedOwnershipBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
														IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPED_BSEight_UseDistanceToCapture)
															CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niPed[iped])
														ENDIF
														//MC_serverBD_1.sFMMC_SBD.niPed[iped] = NULL
														MC_serverBD_2.iPartPedFollows[iped] = -1
														MC_serverBD_2.iOldPartPedFollows[iped]  = -1
													ENDIF
												ENDIF
												
												IF MC_serverBD_2.iCurrentPedRespawnLives[iped] <= 0	
												AND MC_serverBD.iRuleReCapture[igroupTeam][MC_serverBD_4.iPedPriority[iped][igroupTeam]] <= 0
													FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
														ipriority[iteam] = MC_serverBD_4.iPedPriority[iped][iteam]
														
														IF iteam=igroupTeam
															IF IS_BIT_SET(MC_serverBD.iPedteamFailBitset[iped],iteam)
																CLEAR_BIT(MC_serverBD.iPedteamFailBitset[iped],iteam)
																CLEAR_BIT(MC_serverBD.iMissionCriticalPed[iteam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
																NET_PRINT("[JS] MANAGE_SERVER_CAPTURE_BY_DISTANCE_EVERY_FRAME OBJECTIVE NOT FAILED FOR TEAM: ")NET_PRINT_INT(igroupTeam) NET_PRINT(" BECAUSE THEY CAPTURED PED: ") NET_PRINT_INT(iped) NET_NL()
																bwasobjective[iTeam] = TRUE
															ENDIF
														ELSE
															IF DOES_TEAM_LIKE_TEAM(iteam,igroupTeam)
																IF IS_BIT_SET(MC_serverBD.iPedteamFailBitset[iped],iteam)
																	CLEAR_BIT(MC_serverBD.iPedteamFailBitset[iped],iteam)
																	CLEAR_BIT(MC_serverBD.iMissionCriticalPed[iteam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
																	bwasobjective[iTeam] = TRUE
																ENDIF
															ENDIF
														ENDIF
													ENDFOR
													
													FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
														IF iteam!=igroupTeam
															IF IS_BIT_SET(MC_serverBD.iPedteamFailBitset[iped],iteam)
																IF ipriority[iteam]  < FMMC_MAX_RULES
																OR IS_BIT_SET(MC_serverBD.iMissionCriticalPed[iteam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
																	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iTeamFailBitset,ipriority[iteam])
																	OR IS_BIT_SET(MC_serverBD.iMissionCriticalPed[iteam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
																		SET_TEAM_FAILED(iteam,mFail_PED_CAPTURED)
																		NET_PRINT("[JS] MANAGE_SERVER_CAPTURE_BY_DISTANCE_EVERY_FRAME MISSION FAILED FOR TEAM:")NET_PRINT_INT(iteam) NET_PRINT(" BECAUSE PED CAPTURED: ") NET_PRINT_INT(iped) NET_NL()
																		MC_serverBD.iEntityCausingFail[iteam] = iped
																	ENDIF
																ENDIF
																
																bwasobjective[iTeam] = TRUE
																CLEAR_BIT(MC_serverBD.iPedteamFailBitset[iped],iteam)
															ENDIF
															
															IF DOES_TEAM_LIKE_TEAM(iteam,igroupTeam)
																SET_TEAM_OBJECTIVE_OUTCOME(iteam,ipriority[iteam],TRUE)
																PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED,iped,iteam,TRUE)
															ELSE
																SET_TEAM_OBJECTIVE_OUTCOME(iteam,ipriority[iteam],FALSE)
																PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED,iped,iteam,FALSE)
															ENDIF
														ELSE
															SET_TEAM_OBJECTIVE_OUTCOME(iteam,ipriority[iteam],TRUE)
															PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED,iped,iteam,TRUE)
														ENDIF
														MC_serverBD.iReasonForObjEnd[iteam] = OBJ_END_REASON_PED_CAPTURED
														
													ENDFOR
												ENDIF
												
												IF MC_serverBD_4.iPedPriority[iped][igroupTeam] < FMMC_MAX_RULES
													IF MC_serverBD.iRuleReCapture[igroupTeam][MC_serverBD_4.iPedPriority[iped][igroupTeam]] <= 0
														IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFollow = ciPED_FOLLOW_ON
															FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
																PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED,iped,iteam,FALSE)
															ENDFOR
														ENDIF
													ENDIF
												ENDIF
												
												IF MC_serverBD_4.iPedPriority[iped][igroupTeam] < FMMC_MAX_RULES
													IF MC_serverBD.iRuleReCapture[igroupTeam][MC_serverBD_4.iPedPriority[iped][igroupTeam]] != UNLIMITED_CAPTURES_KILLS
														MC_serverBD.iRuleReCapture[igroupTeam][MC_serverBD_4.iPedPriority[iped][igroupTeam]]--
													ENDIF
												ENDIF
												
												FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
													IF bwasobjective[iTeam] 
														IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_PED,iteam,ipriority[iteam])
															SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam,ipriority[iteam],MC_serverBD.iReasonForObjEnd[iteam])
															BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iteam],iteam,ipriority[iteam],DID_TEAM_PASS_OBJECTIVE(iteam,ipriority[iteam]))
														ENDIF
													ENDIF
												ENDFOR
												
												IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPED_BSEight_KillExplodeOnCapture)
													SET_BIT(MC_serverBD_4.iPedExplodeKillOnCaptureBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
												ELSE
													PED_INDEX tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
													IF NOT IS_PED_INJURED(tempPed)
														VEHICLE_INDEX vehPedIsIn
														INT iVehPedIsIn = -1
														IF IS_PED_IN_ANY_VEHICLE(tempPed)
															vehPedIsIn = GET_VEHICLE_PED_IS_IN(tempPed)
															iVehPedIsIn = IS_ENTITY_A_MISSION_CREATOR_ENTITY(vehPedIsIn)
														ENDIF
														IF MC_serverBD.iPedteamFailBitset[iPed] > 0
														OR (iVehPedIsIn != -1 AND MC_serverBD.iVehteamFailBitset[iVehPedIsIn] > 0)
															FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
																CLEAR_BIT(MC_serverBD.iMissionCriticalPed[iTeam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
																CLEAR_BIT(MC_serverBD.iPedteamFailBitset[iPed],iTeam)
																IF iVehPedIsIn != -1
																	CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iTeam], iVehPedIsIn)
																	CLEAR_BIT(MC_serverBD.iVehteamFailBitset[iVehPedIsIn], iTeam)
																ENDIF
															ENDFOR
														ENDIF
													ENDIF
												ENDIF
												
												SET_BIT(MC_serverBD_4.iPedClearFailOnGoTo[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
												
												MC_serverBD_4.iCaptureTimestamp = 0

												NET_PRINT("[JS] MANAGE_SERVER_CAPTURE_BY_DISTANCE_EVERY_FRAME Control complete for ped: ")  NET_PRINT_INT(iped) NET_NL()
												RESET_NET_TIMER(MC_serverBD_1.tdControlPedTimer[iped])
												RESET_NET_TIMER(MC_serverBD.tdControlPedTimerOffset[iped])
											ENDIF
										ENDIF
									ENDIF
								ELSE
									IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPED_BSEight_ResetCaptureOnOutOfRange)
										RESET_NET_TIMER(MC_serverBD_1.tdControlPedTimer[iped])
									ELSE
										IF NOT HAS_NET_TIMER_STARTED(MC_serverBD.tdControlPedTimerOffset[iped])
										AND HAS_NET_TIMER_STARTED(MC_serverBD_1.tdControlPedTimer[iped])
											PRINTLN("[JS] MANAGE_SERVER_CAPTURE_BY_DISTANCE_EVERY_FRAME - ped ",iped," starting offset timer 6")
											PRINTLN("[JS] MANAGE_SERVER_CAPTURE_BY_DISTANCE_EVERY_FRAME Timer: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.tdControlPedTimer[iped]))
											REINIT_NET_TIMER(MC_serverBD.tdControlPedTimerOffset[iped])
										ENDIF
									ENDIF
									MC_serverBD_2.iOldPartPedFollows[iped] = MC_serverBD_2.iPartPedFollows[iped]
								ENDIF
							ELSE
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPED_BSEight_ResetCaptureOnOutOfRange)
									RESET_NET_TIMER(MC_serverBD_1.tdControlPedTimer[iped])
								ELSE
									IF NOT HAS_NET_TIMER_STARTED(MC_serverBD.tdControlPedTimerOffset[iped])
									AND HAS_NET_TIMER_STARTED(MC_serverBD_1.tdControlPedTimer[iped])
										PRINTLN("[JS] MANAGE_SERVER_CAPTURE_BY_DISTANCE_EVERY_FRAME - ped ",iped," starting offset timer 5")
										PRINTLN("[JS] MANAGE_SERVER_CAPTURE_BY_DISTANCE_EVERY_FRAME Timer: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.tdControlPedTimer[iped]))
										REINIT_NET_TIMER(MC_serverBD.tdControlPedTimerOffset[iped])
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF MC_serverBD_2.iPartPedFollows[iped] != -1
								PRINTLN("[JS] MANAGE_SERVER_CAPTURE_BY_DISTANCE_EVERY_FRAME - ped ",iped," gets MC_serverBD_2.iPartPedFollows[iped] reset to -1 as leader player is not okay")
							ENDIF
							
							MC_serverBD_2.iPartPedFollows[iped] = -1
							MC_serverBD_2.iOldPartPedFollows[iped]  = -1
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPED_BSEight_ResetCaptureOnOutOfRange)
								RESET_NET_TIMER(MC_serverBD_1.tdControlPedTimer[iped])
							ELSE
								IF NOT HAS_NET_TIMER_STARTED(MC_serverBD.tdControlPedTimerOffset[iped])
								AND HAS_NET_TIMER_STARTED(MC_serverBD_1.tdControlPedTimer[iped])
									PRINTLN("[JS] MANAGE_SERVER_CAPTURE_BY_DISTANCE_EVERY_FRAME - ped ",iped," starting offset timer 4")
									PRINTLN("[JS] MANAGE_SERVER_CAPTURE_BY_DISTANCE_EVERY_FRAME Timer: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.tdControlPedTimer[iped]))
									REINIT_NET_TIMER(MC_serverBD.tdControlPedTimerOffset[iped])
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF MC_serverBD_2.iPartPedFollows[iped] != -1
							PRINTLN("[JS] MANAGE_SERVER_CAPTURE_BY_DISTANCE_EVERY_FRAME - ped ",iped," gets MC_serverBD_2.iPartPedFollows[iped] reset to -1 as leader isn't active/is finished")
						ENDIF
						
						MC_serverBD_2.iPartPedFollows[iped] = -1
						MC_serverBD_2.iOldPartPedFollows[iped]  = -1
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPED_BSEight_ResetCaptureOnOutOfRange)
							RESET_NET_TIMER(MC_serverBD_1.tdControlPedTimer[iped])
						ELSE
							IF NOT HAS_NET_TIMER_STARTED(MC_serverBD.tdControlPedTimerOffset[iped])
							AND HAS_NET_TIMER_STARTED(MC_serverBD_1.tdControlPedTimer[iped])
								PRINTLN("[JS] MANAGE_SERVER_CAPTURE_BY_DISTANCE_EVERY_FRAME - ped ",iped," starting offset timer 3")
								PRINTLN("[JS] MANAGE_SERVER_CAPTURE_BY_DISTANCE_EVERY_FRAME Timer: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.tdControlPedTimer[iped]))
								REINIT_NET_TIMER(MC_serverBD.tdControlPedTimerOffset[iped])
							ENDIF
						ENDIF
					ENDIF
				ELSE
					MC_serverBD_2.iOldPartPedFollows[iped]  = -1
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPED_BSEight_ResetCaptureOnOutOfRange)
						RESET_NET_TIMER(MC_serverBD_1.tdControlPedTimer[iped])
					ELSE
						IF NOT HAS_NET_TIMER_STARTED(MC_serverBD.tdControlPedTimerOffset[iped])
						AND HAS_NET_TIMER_STARTED(MC_serverBD_1.tdControlPedTimer[iped])
							PRINTLN("[JS] MANAGE_SERVER_CAPTURE_BY_DISTANCE_EVERY_FRAME - ped ",iped," starting offset timer 2")
							PRINTLN("[JS] MANAGE_SERVER_CAPTURE_BY_DISTANCE_EVERY_FRAME Timer: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.tdControlPedTimer[iped]))
							REINIT_NET_TIMER(MC_serverBD.tdControlPedTimerOffset[iped])
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF MC_serverBD_2.iPartPedFollows[iped] != -1
					PRINTLN("[JS] MANAGE_SERVER_CAPTURE_BY_DISTANCE_EVERY_FRAME - ped ",iped," gets MC_serverBD_2.iPartPedFollows[iped] reset to -1 as is no longer in a group")
				ENDIF
				
				MC_serverBD_2.iPartPedFollows[iped] = -1
				MC_serverBD_2.iOldPartPedFollows[iped]  = -1
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPED_BSEight_ResetCaptureOnOutOfRange)
					RESET_NET_TIMER(MC_serverBD_1.tdControlPedTimer[iped])
				ELSE
					IF NOT HAS_NET_TIMER_STARTED(MC_serverBD.tdControlPedTimerOffset[iped])
					AND HAS_NET_TIMER_STARTED(MC_serverBD_1.tdControlPedTimer[iped])
						PRINTLN("[JS] MANAGE_SERVER_CAPTURE_BY_DISTANCE_EVERY_FRAME - ped ",iped," starting offset timer 1")
						PRINTLN("[JS] MANAGE_SERVER_CAPTURE_BY_DISTANCE_EVERY_FRAME Timer: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.tdControlPedTimer[iped]))
						REINIT_NET_TIMER(MC_serverBD.tdControlPedTimerOffset[iped])
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC MANAGE_SERVER_GROUPING_DISTANCE_AND_CONTROL_DATA(INT iped, BOOL bEveryFrame = FALSE)

	PARTICIPANT_INDEX tempPart
	PLAYER_INDEX tempPlayer
	INT iteam
	INT ipart
	INT igroupteam
	INT ipriority[FMMC_MAX_TEAMS]
	BOOL bwasobjective[FMMC_MAX_TEAMS]

	INT iPartThatHasControl = -1

	IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
		
		//Handle distance to capture logic
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPED_BSEight_UseDistanceToCapture)	 	
			EXIT //Done every frame
		ELSE
			tempPlayer = GET_PLAYER_PED_IS_FOLLOWING(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped]))
		
			IF tempPlayer != INVALID_PLAYER_INDEX()
				IF IS_NET_PLAYER_OK(tempPlayer)
					
					tempPart = NETWORK_GET_PARTICIPANT_INDEX(tempPlayer)
					
					IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
						
						ipart = NATIVE_TO_INT(tempPart)
						
						IF MC_serverBD_2.iPartPedFollows[iped] != ipart
							IF IS_BIT_SET(MC_playerBD[ipart].iPedFollowingBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
							AND NOT IS_BIT_SET(MC_playerBD[ipart].iClientBitSet,PBBOOL_FINISHED)
							AND NOT IS_BIT_SET(MC_playerBD[ipart].iClientBitSet,PBBOOL_START_SPECTATOR)
								PRINTLN("[RCC MISSION] MANAGE_SERVER_GROUPING_DISTANCE_AND_CONTROL_DATA - Setting ped ",iped," with MC_serverBD_2.iPartPedFollows[iped] = ",ipart,", old iPartPedFollows = ",MC_serverBD_2.iPartPedFollows[iped])
								MC_serverBD_2.iPartPedFollows[iped] = ipart
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		EXIT
	ENDIF
	
	IF IS_PED_IN_GROUP(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped]))
	OR iPartThatHasControl != -1
		IF MC_serverBD_2.iPartPedFollows[iped] != -1
			
			tempPart = INT_TO_PARTICIPANTINDEX(MC_serverBD_2.iPartPedFollows[iped])
			
			IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			AND NOT IS_BIT_SET(MC_playerBD[MC_serverBD_2.iPartPedFollows[iped]].iClientBitSet,PBBOOL_FINISHED)
				
				tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
				
				IF IS_NET_PLAYER_OK(tempPlayer)
					
					igroupteam = MC_playerBD[MC_serverBD_2.iPartPedFollows[iped]].iteam
					
					IF NOT bEveryFrame
					AND SHOULD_PED_COUNT_AS_HELD_FOR_TEAM(iped,igroupteam)
					OR iPartThatHasControl != -1
						iNumHighPriorityPedHeld[igroupteam]++
						
						FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
							IF iteam != igroupteam
								IF DOES_TEAM_LIKE_TEAM(iteam,igroupteam)
									IF SHOULD_PED_COUNT_AS_HELD_FOR_TEAM(iped,iteam)
									OR iPartThatHasControl != -1
										iNumHighPriorityPedHeld[iteam]++
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
					ENDIF
					
					IF MC_serverBD_4.iPedRule[iped][igroupTeam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
						IF MC_serverBD_2.iOldPartPedFollows[iped] = MC_serverBD_2.iPartPedFollows[iped] 
							IF MC_serverBD_4.iPedPriority[iped][igroupTeam] < FMMC_MAX_RULES
								IF NOT HAS_NET_TIMER_STARTED(MC_serverBD_1.tdControlPedTimer[iped])
									START_NET_TIMER(MC_serverBD_1.tdControlPedTimer[iped])
									NET_PRINT("starting control timer for ped: ")  NET_PRINT_INT(iped) NET_NL()
								ELSE
									IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.tdControlPedTimer[iped]) > MC_serverBD.iPedTakeoverTime[igroupTeam][MC_serverBD_4.iPedPriority[iped][igroupTeam]] 
										BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM_CON_PED,0,igroupTeam,-1,tempplayer,ci_TARGET_PED,iped)
										//BROADCAST_GIVE_PLAYER_XP(tempPlayer,200,-1,TRUE)
										INCREMENT_SERVER_REMOTE_PLAYER_SCORE(tempPlayer, GET_FMMC_POINTS_FOR_TEAM(igroupTeam,MC_serverBD_4.iPedPriority[iped][igroupTeam]), igroupTeam, MC_serverBD_4.iPedPriority[iped][igroupTeam])

										IF MC_serverBD.iRuleReCapture[igroupTeam][MC_serverBD_4.iPedPriority[iped][igroupTeam]] <= 0
											IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFollow = ciPED_FOLLOW_OFF
												PRINTLN("[RCC MISSION] Control ped ",iped," getting cleaned up as iRuleReCapture is zero for team ",igroupTeam,", resetting MC_serverBD_2.iPartPedFollows[iped] to -1")
												CLEAR_BIT(MC_serverBD.iPedCleanup_NeedOwnershipBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
												IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPED_BSEight_UseDistanceToCapture)
													CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niPed[iped])
												ENDIF
												//MC_serverBD_1.sFMMC_SBD.niPed[iped] = NULL
												MC_serverBD_2.iPartPedFollows[iped] = -1
												MC_serverBD_2.iOldPartPedFollows[iped]  = -1
											ENDIF
										ENDIF
										
										IF MC_serverBD_2.iCurrentPedRespawnLives[iped] <= 0	
										AND MC_serverBD.iRuleReCapture[igroupTeam][MC_serverBD_4.iPedPriority[iped][igroupTeam]] <= 0
											FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
												ipriority[iteam] = MC_serverBD_4.iPedPriority[iped][iteam]
												
												IF iteam=igroupTeam
													IF IS_BIT_SET(MC_serverBD.iPedteamFailBitset[iped],iteam)
														CLEAR_BIT(MC_serverBD.iPedteamFailBitset[iped],iteam)
														CLEAR_BIT(MC_serverBD.iMissionCriticalPed[iteam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
														NET_PRINT("OBJECTIVE NOT FAILED FOR TEAM: ")NET_PRINT_INT(igroupTeam) NET_PRINT(" BECAUSE THEY CAPTURED PED: ") NET_PRINT_INT(iped) NET_NL()
														bwasobjective[iTeam] = TRUE
													ENDIF
												ELSE
													IF DOES_TEAM_LIKE_TEAM(iteam,igroupTeam)
														IF IS_BIT_SET(MC_serverBD.iPedteamFailBitset[iped],iteam)
															CLEAR_BIT(MC_serverBD.iPedteamFailBitset[iped],iteam)
															CLEAR_BIT(MC_serverBD.iMissionCriticalPed[iteam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
															bwasobjective[iTeam] = TRUE
														ENDIF
													ENDIF
												ENDIF
											ENDFOR
											
											FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
												IF iteam!=igroupTeam
													IF IS_BIT_SET(MC_serverBD.iPedteamFailBitset[iped],iteam)
														IF ipriority[iteam]  < FMMC_MAX_RULES
														OR IS_BIT_SET(MC_serverBD.iMissionCriticalPed[iteam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
															IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iTeamFailBitset,ipriority[iteam])
															OR IS_BIT_SET(MC_serverBD.iMissionCriticalPed[iteam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
																SET_TEAM_FAILED(iteam,mFail_PED_CAPTURED)
																NET_PRINT(" MISSION FAILED FOR TEAM:")NET_PRINT_INT(iteam) NET_PRINT(" BECAUSE PED CAPTURED: ") NET_PRINT_INT(iped) NET_NL()
																MC_serverBD.iEntityCausingFail[iteam] = iped
															ENDIF
														ENDIF
														
														bwasobjective[iTeam] = TRUE
														CLEAR_BIT(MC_serverBD.iPedteamFailBitset[iped],iteam)
													ENDIF
													
													IF DOES_TEAM_LIKE_TEAM(iteam,igroupTeam)
														SET_TEAM_OBJECTIVE_OUTCOME(iteam,ipriority[iteam],TRUE)
														PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED,iped,iteam,TRUE)
													ELSE
														SET_TEAM_OBJECTIVE_OUTCOME(iteam,ipriority[iteam],FALSE)
														PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED,iped,iteam,FALSE)
													ENDIF
												ELSE
													SET_TEAM_OBJECTIVE_OUTCOME(iteam,ipriority[iteam],TRUE)
													PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED,iped,iteam,TRUE)
												ENDIF
												MC_serverBD.iReasonForObjEnd[iteam] = OBJ_END_REASON_PED_CAPTURED
												
											ENDFOR
										ENDIF
										
										IF MC_serverBD_4.iPedPriority[iped][igroupTeam] < FMMC_MAX_RULES
											IF MC_serverBD.iRuleReCapture[igroupTeam][MC_serverBD_4.iPedPriority[iped][igroupTeam]] <= 0
												IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFollow = ciPED_FOLLOW_ON
													FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
														PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED,iped,iteam,FALSE)
													ENDFOR
												ENDIF
											ENDIF
										ENDIF
										
										IF MC_serverBD_4.iPedPriority[iped][igroupTeam] < FMMC_MAX_RULES
											IF MC_serverBD.iRuleReCapture[igroupTeam][MC_serverBD_4.iPedPriority[iped][igroupTeam]] != UNLIMITED_CAPTURES_KILLS
												MC_serverBD.iRuleReCapture[igroupTeam][MC_serverBD_4.iPedPriority[iped][igroupTeam]]--
											ENDIF
										ENDIF
										
										FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
											IF bwasobjective[iTeam] 
												IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_PED,iteam,ipriority[iteam])
													SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam,ipriority[iteam],MC_serverBD.iReasonForObjEnd[iteam])
													BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iteam],iteam,ipriority[iteam],DID_TEAM_PASS_OBJECTIVE(iteam,ipriority[iteam]))
												ENDIF
											ENDIF
										ENDFOR
										
										IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPED_BSEight_KillExplodeOnCapture)
											SET_BIT(MC_serverBD_4.iPedExplodeKillOnCaptureBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
										ENDIF
								
										MC_serverBD_4.iCaptureTimestamp = 0

										NET_PRINT("Control complete for ped: ")  NET_PRINT_INT(iped) NET_NL()
										RESET_NET_TIMER(MC_serverBD_1.tdControlPedTimer[iped])
									ENDIF
								ENDIF
							ENDIF
						ELSE
							RESET_NET_TIMER(MC_serverBD_1.tdControlPedTimer[iped])
							MC_serverBD_2.iOldPartPedFollows[iped] = MC_serverBD_2.iPartPedFollows[iped]
						ENDIF
					ELSE
						RESET_NET_TIMER(MC_serverBD_1.tdControlPedTimer[iped])
					ENDIF
				ELSE
					IF MC_serverBD_2.iPartPedFollows[iped] != -1
						PRINTLN("[RCC MISSION] MANAGE_SERVER_GROUPING_DISTANCE_AND_CONTROL_DATA - ped ",iped," gets MC_serverBD_2.iPartPedFollows[iped] reset to -1 as leader player is not okay")
					ENDIF
					
					MC_serverBD_2.iPartPedFollows[iped] = -1
					MC_serverBD_2.iOldPartPedFollows[iped]  = -1
					RESET_NET_TIMER(MC_serverBD_1.tdControlPedTimer[iped])
				ENDIF
			ELSE
				IF MC_serverBD_2.iPartPedFollows[iped] != -1
					PRINTLN("[RCC MISSION] MANAGE_SERVER_GROUPING_DISTANCE_AND_CONTROL_DATA - ped ",iped," gets MC_serverBD_2.iPartPedFollows[iped] reset to -1 as leader isn't active/is finished")
				ENDIF
				
				MC_serverBD_2.iPartPedFollows[iped] = -1
				MC_serverBD_2.iOldPartPedFollows[iped]  = -1
				RESET_NET_TIMER(MC_serverBD_1.tdControlPedTimer[iped])
			ENDIF
		ELSE
			MC_serverBD_2.iOldPartPedFollows[iped]  = -1
			RESET_NET_TIMER(MC_serverBD_1.tdControlPedTimer[iped])
		ENDIF
	ELSE
		IF MC_serverBD_2.iPartPedFollows[iped] != -1
			PRINTLN("[RCC MISSION] MANAGE_SERVER_GROUPING_DISTANCE_AND_CONTROL_DATA - ped ",iped," gets MC_serverBD_2.iPartPedFollows[iped] reset to -1 as is no longer in a group")
		ENDIF
		
		MC_serverBD_2.iPartPedFollows[iped] = -1
		MC_serverBD_2.iOldPartPedFollows[iped]  = -1
		RESET_NET_TIMER(MC_serverBD_1.tdControlPedTimer[iped])
	ENDIF

ENDPROC

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: PED CLEANUP / DELETION !
//
//************************************************************************************************************************************************************



PROC DELETE_FMMC_PED(INT iped)
	
	INT iteam
	
	FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
		CLEAR_BIT(MC_serverBD.iPedteamFailBitset[iped],iteam)
		CLEAR_BIT(MC_serverBD.iMissionCriticalPed[iteam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
	ENDFOR	
	
	CLEAR_BIT(MC_serverBD.iPedCleanup_NeedOwnershipBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
	
	DELETE_NET_ID(MC_serverBD_1.sFMMC_SBD.niPed[iped])
	
	#IF IS_DEBUG_BUILD
		IF biHostilePedBlip[iped].PedID != NULL
			CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][AI] DELETE_FMMC_PED - Ped ", iped, " AI blip cleaning up due to CLEANUP_PED_EARLY call.")
		ENDIF
	#ENDIF
	CLEANUP_AI_PED_BLIP(biHostilePedBlip[iped])
	
	PRINTLN("[RCC MISSION] DELETE_FMMC_PED - ped getting cleaned up early - delete, objective: ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCleanupObjective," team: ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCleanupTeam," ped: ",iped)
	
ENDPROC

FUNC BOOL CLEANUP_PED_EARLY(INT iped, PED_INDEX tempPed)

	IF SHOULD_CLEANUP_PED(iped,tempPed)
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset,ciPED_BS_IgnoreVisCheckCleanup)
			IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iped])
				PRINTLN("[RCC MISSION] CLEANUP_PED_EARLY - Requesting control of ped ",iped," for deletion; setting MC_serverBD.iPedCleanup_NeedOwnershipBS bit")
				NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iped])
				SET_BIT(MC_serverBD.iPedCleanup_NeedOwnershipBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			ELSE
				PRINTLN("[RCC MISSION] CLEANUP_PED_EARLY - Deleting ped ",iped)
				DELETE_FMMC_PED(iped)
				
				RETURN TRUE
			ENDIF
		ELSE
			INT iteam
			FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
				CLEAR_BIT(MC_serverBD.iPedteamFailBitset[iped],iteam)
				CLEAR_BIT(MC_serverBD.iMissionCriticalPed[iteam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			ENDFOR
			
			// If a ped is already fleeing, or is set to flee on cleanup
			IF NOT IS_PED_INJURED(tempPed)
				IF IS_PED_FLEEING(tempPed)
				OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetFive, ciPED_BSFive_FleeOnCleanup)
					// Force ped to always flee (Added to fix url:bugstar:2117976)
					SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_ALWAYS_FIGHT, FALSE)
					SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_DISABLE_FLEE_FROM_COMBAT, FALSE)
					SET_PED_FLEE_ATTRIBUTES(tempPed, FA_NEVER_FLEE, FALSE)
					SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_ALWAYS_FLEE, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_FLEE_WHILST_IN_VEHICLE, TRUE)
					SET_PED_FLEE_ATTRIBUTES(tempPed, FA_COWER_INSTEAD_OF_FLEE, FALSE)
					SET_PED_FLEE_ATTRIBUTES(tempPed, FA_DISABLE_COWER, TRUE)
					PRINTLN("[RCC MISSION] Ped ", iped, " setup to flee (w CA_ALWAYS_FLEE) on cleanup.")
					IF IS_PED_IN_ANY_HELI(tempPed)
					OR IS_PED_IN_ANY_PLANE(tempPed)
						PRINTLN("[RCC MISSION] [JS] Ped ", iped, " setup to flee far away in heli")
						CLEAR_PED_TASKS(tempPed)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, TRUE)
						VECTOR fleeDirection = GET_ENTITY_FORWARD_VECTOR(tempPed)
						fleeDirection.z = 0
						fleeDirection = fleeDirection / VMAG(fleeDirection)
						fleeDirection.z = 0.2
						fleeDirection = fleeDirection / VMAG(fleeDirection)
						TASK_SMART_FLEE_COORD(tempPed, GET_ENTITY_COORDS(tempPed) - (fleeDirection*100.0), 10000.0, 999999)
						//TASK_PLANE_MISSION(tempPed, GET_VEHICLE_PED_IS_IN(tempPed), NULL, NULL, GET_ENTITY_COORDS(tempPed) + (fleeDirection*1000.0), MISSION_GOTO, 100.0, -1.0, -1.0, 30, 30)
					ENDIF
				ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFlee != ciPED_FLEE_ON
					//This should already be set, but make sure it is anyway
					PRINTLN("[RCC MISSION] Ped ", iped, " setup to NOT flee Making sure ped doesn't flee on cleanup.")
					SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_DISABLE_FLEE_FROM_COMBAT, TRUE)
					SET_PED_FLEE_ATTRIBUTES(tempPed, FA_NEVER_FLEE, TRUE)
					SET_PED_FLEE_ATTRIBUTES(tempPed, FA_DISABLE_COWER, TRUE)
				ENDIF
				IF NOT IS_PED_IN_ANY_HELI(tempPed)
				AND NOT IS_PED_IN_ANY_PLANE(tempPed)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,FALSE)
				ENDIF
			ENDIF
			
			MC_serverBD_2.iCurrentPedRespawnLives[iped] = 0
			PRINTLN("[RCC MISSION] ped ",iped," getting cleaned up early - set MC_serverBD_2.iCurrentPedRespawnLives[", iPed, "] = 0. Call 1.")
			
			CLEAR_BIT(MC_serverBD.iPedCleanup_NeedOwnershipBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			
			CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niPed[iped])
			
//			#IF IS_DEBUG_BUILD
//				IF biHostilePedBlip[iped].PedID != NULL
//					CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION][AI] Ped ", iped, " AI blip cleaning up due to CLEANUP_PED_EARLY call.")
//				ENDIF
//			#ENDIF
//			CLEANUP_AI_PED_BLIP(biHostilePedBlip[iped])
			
			PRINTLN("[RCC MISSION] ped getting cleaned up early - no longer needed, objective: ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCleanupObjective," team: ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iCleanupTeam," ped: ",iped)
			RETURN TRUE
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetSeven, ciPED_BSSeven_LoudPropellorPlanes)
			STOP_AUDIO_SCENE("Speed_Race_Desert_Airport_Scene")
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_PED_CLEANUP_ON_DEATH(INT iped)
	
	BOOL bShouldCleanup = TRUE
	PED_INDEX tempPed
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iped])
		tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,ciPED_BSThree_DontCleanupOnDeath)
	OR DOES_ANY_TEAM_HAVE_PHOTO_DEAD_RULE(iped)
	OR IS_PED_INVOLVED_IN_CHARM_OR_CROWD_CONTROL(iped)
		bShouldCleanup = FALSE
	ELSE
		IF DOES_ENTITY_EXIST(tempPed)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iInventoryBS > 0
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iInventoryBS,ciPED_INV_Briefcase_prop_ld_case_01)
					OBJECT_INDEX oiBrief = GET_CLOSEST_OBJECT_OF_TYPE(GET_ENTITY_COORDS(NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niPed[iped]),FALSE),10,PROP_LD_CASE_01, false, false, false)
					
					IF DOES_ENTITY_EXIST(oiBrief)
					AND IS_ENTITY_ATTACHED_TO_ENTITY(oiBrief,tempPed)
						bShouldCleanup = FALSE
					ENDIF
				ENDIF
			ENDIF
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim = ciPED_IDLE_ANIM__UNFSHDBIZ_JUDGE_IDLE
				VECTOR vCoords = <<-1806.5146, 427.71, 131.810>>
				OBJECT_INDEX oiTablet
				oiTablet = GET_CLOSEST_OBJECT_OF_TYPE(vCoords,5,PROP_CS_TABLET, false, false, false)
				
				IF DOES_ENTITY_EXIST(oiTablet)
				AND IS_ENTITY_ATTACHED_TO_ENTITY(oiTablet,tempPed)
					bShouldCleanup = FALSE
				ENDIF
			ELIF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim = ciPED_IDLE_ANIM__WITSEC_CHAIR_SNOOZE)
				VECTOR vCoords = GET_ENTITY_COORDS(tempPed, FALSE)
				
				OBJECT_INDEX oiChair
				oiChair = GET_CLOSEST_OBJECT_OF_TYPE(vCoords,5,HEI_PROP_HEI_SKID_CHAIR, false, false, false)
				
				IF DOES_ENTITY_EXIST(oiChair)
				AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(oiChair, FALSE)
					bShouldCleanup = FALSE
				ENDIF
			ELIF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim = ciPED_IDLE_ANIM__GO_FOUNDRY_HOSTAGE_PERP_IDLE)
				
				IF NOT IS_BIT_SET(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					
					INT iVictimPed = GET_GO_FOUNDRY_VICTIM_PED(iped)
					
					IF iVictimPed != -1
						
						IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iVictimPed])
							
							PED_INDEX VictimPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iVictimPed])
							
							IF NOT IS_PED_INJURED(VictimPed)
								IF NOT IS_BIT_SET(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iVictimPed)], GET_LONG_BITSET_BIT(iVictimPed))
									IF (NOT SHOULD_PED_BE_GIVEN_TASK(VictimPed, iVictimPed, SCRIPT_TASK_SYNCHRONIZED_SCENE, TRUE))
									AND IS_ENTITY_PLAYING_ANIM(VictimPed, "anim@GangOps@Hostage@", "victim_idle")
										PRINTLN("[RCC MISSION] SHOULD_PED_CLEANUP_ON_DEATH - Ped ",iped," w GO Foundry Perp anim waiting for victim ",iVictimPed," to start breakout, don't clean up")
										bShouldCleanup = FALSE
									ENDIF
								ELSE
									IF (NOT SHOULD_PED_BE_GIVEN_TASK(VictimPed, iVictimPed, SCRIPT_TASK_SYNCHRONIZED_SCENE, TRUE))
									AND IS_ENTITY_PLAYING_ANIM(VictimPed, "anim@GangOps@Hostage@", "victim_success")
										
										INT iSynchScene = iLocalGOFoundryHostageSynchSceneID
										
										IF iSynchScene = -1
											iSynchScene = MC_serverBD_1.iGOFoundryHostageSynchSceneID
										ENDIF
										
										IF iSynchScene != -1
											INT iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iSynchScene)
											IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
												IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene) < 0.5 // Wait for the perp ped to be down on the floor
													PRINTLN("[RCC MISSION] SHOULD_PED_CLEANUP_ON_DEATH - Ped ",iped," w GO Foundry Perp anim waiting for synced scene ",iSynchScene," to pass 0.5 phase (currently at ",GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene),"), don't clean up")
													bShouldCleanup = FALSE
												ENDIF
											ELSE
												PRINTLN("[RCC MISSION] SHOULD_PED_CLEANUP_ON_DEATH - Ped ",iped," w GO Foundry Perp anim waiting for synced scene ",iSynchScene," to start, don't clean up")
												bShouldCleanup = FALSE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			ELIF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim = ciPED_IDLE_ANIM__GO_FOUNDRY_HOSTAGE_VICTIM_IDLE)
				
				IF NOT IS_BIT_SET(g_iFMMCPedAnimBreakoutBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
					
					INT iPerpPed = GET_GO_FOUNDRY_PERP_PED(iped)
					
					IF iPerpPed != -1
						
						IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iPerpPed])
							
							PED_INDEX perpPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPerpPed])
							
							IF NOT IS_PED_INJURED(perpPed)
								
								// If the perp is doing their breakout, we should wait until this ped (the victim) is dead on the ground before cleaning them up
								IF (NOT SHOULD_PED_BE_GIVEN_TASK(perpPed, iPerpPed, SCRIPT_TASK_SYNCHRONIZED_SCENE, TRUE))
								AND IS_ENTITY_PLAYING_ANIM(perpPed, "anim@GangOps@Hostage@", "perp_fail")
									
									INT iSynchScene = iLocalGOFoundryHostageSynchSceneID
									
									IF iSynchScene = -1
										iSynchScene = MC_serverBD_1.iGOFoundryHostageSynchSceneID
									ENDIF
									
									IF iSynchScene != -1
										INT iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iSynchScene)
										IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
											IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene) < 0.8 // Wait for the victim ped to be down on the floor
												PRINTLN("[RCC MISSION] SHOULD_PED_CLEANUP_ON_DEATH - Ped ",iped," w GO Foundry Victim anim waiting for synced scene ",iSynchScene," to pass 0.8 phase (currently at ",GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene),"), don't clean up")
												bShouldCleanup = FALSE
											ELSE
												IF NETWORK_HAS_CONTROL_OF_ENTITY(tempPed)
													PRINTLN("[RCC MISSION] SHOULD_PED_CLEANUP_ON_DEATH - Ped ",iped," w GO Foundry Victim anim synced scene ",iSynchScene," has passe 0.8 phase, clean up & set unfrozen")
													FREEZE_ENTITY_POSITION(tempPed, FALSE)
												ELSE
													PRINTLN("[RCC MISSION] SHOULD_PED_CLEANUP_ON_DEATH - Ped ",iped," w GO Foundry Victim anim synced scene ",iSynchScene,", requesting control to unfreeze")
													NETWORK_REQUEST_CONTROL_OF_ENTITY(tempPed)
													bShouldCleanup = FALSE
												ENDIF
											ENDIF
										ELSE
											PRINTLN("[RCC MISSION] SHOULD_PED_CLEANUP_ON_DEATH - Ped ",iped," w GO Foundry Victim anim waiting for synced scene ",iSynchScene," to start, don't clean up")
											bShouldCleanup = FALSE
										ENDIF
									ENDIF
									
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bShouldCleanup
	
ENDFUNC

FUNC BOOL CHECK_PLAYERS_BEING_HOSTILE_IN_RANGE_OF_PED(INT iPed, FLOAT fRange)
	INT iPlayersToCheck, iPlayersChecked, i

	iPlayersToCheck = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]
	
	PED_INDEX pedToCheck
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iped])
		pedToCheck = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
	ELSE
		RETURN FALSE
	ENDIF

	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF NOT (IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_ANY_SPECTATOR)
		OR IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
			
			PRINTLN("[LM] CHECK_PLAYERS_BEING_HOSTILE_IN_RANGE_OF_PED - Checking iPed: ", iPed, " against iPlayer: ", i)
			
			PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(i)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				
				PRINTLN("[LM] CHECK_PLAYERS_BEING_HOSTILE_IN_RANGE_OF_PED - active")
				
				iPlayersChecked++
				PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX( tempPart )
				
				IF IS_NET_PLAYER_OK(tempPlayer)
					
					PRINTLN("[LM] CHECK_PLAYERS_BEING_HOSTILE_IN_RANGE_OF_PED - ok")
					
					PED_INDEX tempPed = GET_PLAYER_PED(tempPlayer)
					IF NOT IS_PED_INJURED(tempPed) 

						PRINTLN("[LM] CHECK_PLAYERS_BEING_HOSTILE_IN_RANGE_OF_PED - not injured")
						
						IF VDIST2(GET_ENTITY_COORDS(tempPed), GET_ENTITY_COORDS(pedToCheck)) < POW(fRange,2)							
							PRINTLN("[LM] CHECK_PLAYERS_BEING_HOSTILE_IN_RANGE_OF_PED - iPed: ", iPed, " iPlayer is now in range: ", i)
							IF IS_PED_SHOOTING(tempPed)
							OR IS_PED_IN_COMBAT(tempPed)
							OR IS_PED_PERFORMING_MELEE_ACTION(tempPed)
								PRINTLN("[LM] CHECK_PLAYERS_BEING_HOSTILE_IN_RANGE_OF_PED - iPed: ", iPed, " iPlayer IS BEING HOSTILE ", i)
								RETURN TRUE
							ELSE
								PRINTLN("[LM] CHECK_PLAYERS_BEING_HOSTILE_IN_RANGE_OF_PED - iPed: ", iPed, " iPlayer is not doing anything hostile: ", i)
							ENDIF
						ELSE
							PRINTLN("[LM] CHECK_PLAYERS_BEING_HOSTILE_IN_RANGE_OF_PED - iPed: ", iPed, " iPlayer is Out of Range: ", i)
						ENDIF
					ENDIF
				ENDIF
				IF iPlayersChecked >= iPlayersToCheck 
					BREAKLOOP
				ENDIF
			ENDIF
		ENDIF
	ENDFOR

	RETURN FALSE
ENDFUNC

FUNC BOOL CHECK_PLAYERS_IN_RANGE_OF_PED(INT iPed, INT iRange)
	INT iPlayersToCheck, iPlayersChecked, i

	iPlayersToCheck = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]
	
	PED_INDEX pedToCheck
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iped])
		pedToCheck = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
	ELSE
		RETURN FALSE
	ENDIF

	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF NOT (IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_ANY_SPECTATOR)
		OR IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
			PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(i)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				iPlayersChecked++
				PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX( tempPart )
				
				IF IS_NET_PLAYER_OK(tempPlayer)
					PED_INDEX tempPed = GET_PLAYER_PED(tempPlayer)
					IF NOT IS_PED_INJURED(tempPed) 
						IF VDIST2(GET_ENTITY_COORDS(tempPed), GET_ENTITY_COORDS(pedToCheck)) < POW(TO_FLOAT(iRange),2)
							PRINTLN("[JS] CHECK_PLAYERS_IN_RANGE_OF_PED - part ",i," is in range of ped ", iPed)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
				IF iPlayersChecked >= iPlayersToCheck 
					BREAKLOOP
				ENDIF
			ENDIF
		ENDIF
	ENDFOR

	RETURN FALSE
ENDFUNC

PROC PROCESS_SPECIAL_FREEZE_VEHICLE_FOR_ATTACHED_PEDS_FIX_4384208(INT iPed)
	IF NOT IS_BIT_SET(MC_serverBD_3.iMissionSpecialFixBS, SBBOOL_SPECIAL_FIX_UNFREEZE_VEH_16)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle = 16
		AND DOES_ENTITY_EXIST(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[12]))
		AND DOES_ENTITY_EXIST(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[13]))
		AND DOES_ENTITY_EXIST(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[16]))			
			PRINTLN("[RCC MISSION][url:bugstar:4384208]  iVeh = 16 Calling FREEZE_ENTITY_POSITION Special Fix.")
			FREEZE_ENTITY_POSITION(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[16]), FALSE)
			SET_BIT(MC_serverBD_3.iMissionSpecialFixBS, SBBOOL_SPECIAL_FIX_UNFREEZE_VEH_16)
		ENDIF
	ENDIF		
	IF NOT IS_BIT_SET(MC_serverBD_3.iMissionSpecialFixBS, SBBOOL_SPECIAL_FIX_UNFREEZE_VEH_18)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle = 18
		AND DOES_ENTITY_EXIST(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[30]))
		AND DOES_ENTITY_EXIST(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[29]))
		AND DOES_ENTITY_EXIST(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[18]))
			PRINTLN("[RCC MISSION][url:bugstar:4384208]  iVeh = 18 Calling FREEZE_ENTITY_POSITION Special fix.")
			FREEZE_ENTITY_POSITION(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[18]), FALSE)
			SET_BIT(MC_serverBD_3.iMissionSpecialFixBS, SBBOOL_SPECIAL_FIX_UNFREEZE_VEH_18)
		ENDIF
	ENDIF		
	IF NOT IS_BIT_SET(MC_serverBD_3.iMissionSpecialFixBS, SBBOOL_SPECIAL_FIX_UNFREEZE_VEH_4)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle = 4
		AND DOES_ENTITY_EXIST(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[16]))
		AND DOES_ENTITY_EXIST(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[4]))
			PRINTLN("[RCC MISSION][url:bugstar:4384208]  iVeh = 4 Calling FREEZE_ENTITY_POSITION Special fix.")
			FREEZE_ENTITY_POSITION(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[4]), FALSE)
			SET_BIT(MC_serverBD_3.iMissionSpecialFixBS, SBBOOL_SPECIAL_FIX_UNFREEZE_VEH_4)
		ENDIF
	ENDIF		
	IF NOT IS_BIT_SET(MC_serverBD_3.iMissionSpecialFixBS, SBBOOL_SPECIAL_FIX_UNFREEZE_VEH_3)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle = 3
		AND DOES_ENTITY_EXIST(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[19]))
		AND DOES_ENTITY_EXIST(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[3]))
			PRINTLN("[RCC MISSION][url:bugstar:4384208]  iVeh = 3 Calling FREEZE_ENTITY_POSITION Special fix.")
			FREEZE_ENTITY_POSITION(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[3]), FALSE)
			SET_BIT(MC_serverBD_3.iMissionSpecialFixBS, SBBOOL_SPECIAL_FIX_UNFREEZE_VEH_3)
		ENDIF
	ENDIF
	IF NOT IS_BIT_SET(MC_serverBD_3.iMissionSpecialFixBS, SBBOOL_SPECIAL_FIX_UNFREEZE_VEH_2)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle = 2
		AND DOES_ENTITY_EXIST(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[20]))
		AND DOES_ENTITY_EXIST(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[2]))
			PRINTLN("[RCC MISSION][url:bugstar:4384208]  iVeh = 2 Calling FREEZE_ENTITY_POSITION Special fix.")
			FREEZE_ENTITY_POSITION(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[2]), FALSE)
			SET_BIT(MC_serverBD_3.iMissionSpecialFixBS, SBBOOL_SPECIAL_FIX_UNFREEZE_VEH_2)
		ENDIF
	ENDIF
ENDPROC

// Peds set up to have an "AggroRule" from the creator used to use IF NOT SHOULD_AGRO_FAIL_FOR_TEAM(iped, iTeam) which made them unable to use the skiprule logic.
// This function fixes that issue while maintaining old content/old setups.
FUNC BOOL IS_PED_RULE_SKIP_VALID_ON_THIS_RULE(INT iPed, INT iTeam)
	IF NOT IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
		IF NOT SHOULD_AGRO_FAIL_FOR_TEAM(iped, iTeam)
			RETURN TRUE
		ENDIF
	ELSE
		IF SHOULD_AGRO_FAIL_FOR_TEAM(iped, iTeam)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: PROCESS PED BRAIN !
//
//************************************************************************************************************************************************************


/// PURPOSE: Handles Ped State and other server logic for the mission peds
PROC PROCESS_PED_BRAIN(INT iped, BOOL bEveryFrame = FALSE)

INT iteam,iPart
BOOL bcheckteamfail
PED_INDEX ThisPed
BOOL bVehicleUsed,btargetOK,bArrived, bGoToIsLooped
ENTITY_INDEX tempTarget
VEHICLE_INDEX tempveh
PARTICIPANT_INDEX tempPart
PLAYER_INDEX tempPlayer

IF NOT bIsLocalPlayerHost
	EXIT
ENDIF
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iped])
		IF IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
			
			//This is a backup, it should already have been handled during the damage event
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_AggroWhenHurtOrKilled)
			AND NOT IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			AND NOT IS_BIT_SET( iPedHitByStunGun[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed)) 
				PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN - Ped ",iped," SPOOKED because found dead and has ciPED_BSFive_AggroWhenHurtOrKilled set (damage event was probably missed)")
				
				#IF IS_DEBUG_BUILD 
				SET_PED_IS_SPOOKED_DEBUG(iPed)
				#ENDIF
				SET_BIT(iPedLocalSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed)) // broadcast
				SET_BIT(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				BROADCAST_FMMC_PED_SPOOKED(iped)
				
				FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
					IF SHOULD_AGRO_FAIL_FOR_TEAM(iped, iTeam)
						SET_BIT(iPedLocalAggroedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
						TRIGGER_AGGRO_ON_PED(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped]),iped)
						iTeam = MC_serverBD.iNumberOfTeams // Break out, we found an aggro team!
					ENDIF
				ENDFOR
				
				SET_BIT(iPedLocalReceivedEvent_HurtOrKilled[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			ENDIF
			
			IF MC_serverBD_2.iPartPedFollows[iped] != -1
				PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN - ped ",iped," getting MC_serverBD_2.iPartPedFollows[iped] reset to -1 as ped is injured")
			ENDIF
			
			MC_serverBD_2.iPartPedFollows[iped] = -1
			MC_serverBD_2.iOldPartPedFollows[iped] = -1
			RESET_NET_TIMER(MC_serverBD_1.tdControlPedTimer[iped])
			
			BOOL bCleanupOnDeath = SHOULD_PED_CLEANUP_ON_DEATH(iped)
			
			IF bCleanupOnDeath
				
				INT iCrowdID
				FOR iCrowdID = 0 TO (MC_serverBD_3.sCCServerData.iNumCrowdControlPeds - 1)
					IF MC_serverBD_3.sCCServerData.iCrowdControlCloudIndex[iCrowdID] = iped
					
						IF NOT IS_BIT_SET_ENUM( sCCLocalPedDecorCopy[iCrowdID].iBitset, CCPEDDATA_Dead )
						AND NOT IS_BIT_SET_ENUM( sCCLocalPedDecorCopy[iCrowdID].iBitset, CCPEDDATA_KnockedOut )
						
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID( MC_serverBD_1.sFMMC_SBD.niPed[iped] )
								sCCLocalPedData[iCrowdID].timeDied = GET_NETWORK_TIME()
								SET_BIT_ENUM( sCCLocalPedDecorCopy[iCrowdID].iBitset, CCPEDDATA_Dead )
							ELSE
								BROADCAST_FMMC_CC_DEAD(iCrowdID)
							ENDIF
							
						ENDIF
						
						iCrowdID = MC_serverBD_3.sCCServerData.iNumCrowdControlPeds // break loop
					ENDIF
				ENDFOR

				PRINTLN( "[RCC MISSION] PROCESS_PED_BRAIN dead ped getting cleaned up: ",iped)
				
				CLEAR_BIT(MC_serverBD.iPedCleanup_NeedOwnershipBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
				
				CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niPed[iped])
				
				IF MC_serverBD_2.iCurrentPedRespawnLives[iped] <= 0
				AND NOT IS_BIT_SET(MC_serverBD_1.sCreatedCount.iSkipPedBitset[GET_LONG_BITSET_INDEX(iped)],GET_LONG_BITSET_BIT(iped))
					PRINTLN( "[RCC MISSION] PROCESS_PED_BRAIN iped: ", iped, " - bcheckteamfail = TRUE - ped dead & cleanup on death")
					bcheckteamfail = TRUE
				ELSE
					//PRINTLN( "[RCC MISSION] PROCESS_PED_BRAIN iped: ", iped, " - bcheckteamfail = FALSE - MC_serverBD_2.iCurrentPedRespawnLives[iped] = ", MC_serverBD_2.iCurrentPedRespawnLives[iped], ", MC_serverBD_1.sCreatedCount.iSkipPedBitset = ", IS_BIT_SET(MC_serverBD_1.sCreatedCount.iSkipPedBitset[GET_LONG_BITSET_INDEX(iped)],GET_LONG_BITSET_BIT(iped)))
				ENDIF
			ELSE
				FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
					IF IS_BIT_SET(MC_serverBD.iDeadPedPhotoBitset[iteam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					AND NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam],g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPriority[iTeam])
						SET_BIT(MC_serverBD.iObjectiveMidPointBitset[iTeam],g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPriority[iTeam])
						SET_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
						PRINTLN( "[RCC MISSION] PROCESS_PED_BRAIN ped dead photo rule so setting midpoint iObjectiveMidPointBitset for team ",iTeam,", rule ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPriority[iTeam])
					ENDIF
				ENDFOR
				
				PRINTLN( "[RCC MISSION] PROCESS_PED_BRAIN iped: ", iped, " - bcheckteamfail = TRUE - ped dead & DONT cleanup on death")
				bcheckteamfail = TRUE
			ENDIF
			
			//Aggro countdown (grace period) clearing:
			IF IS_BIT_SET(MC_serverBD.iPedAggroedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				CLEAR_BIT(MC_serverBD.iPedAggroedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				
				FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
					//If it's not too late already:
					IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_AGGRO_WILL_FAIL_FOR_TEAM_0 + iTeam)
						IF HAS_NET_TIMER_STARTED(MC_serverBD.tdAggroCountdownTimer[iTeam])
						AND SHOULD_AGRO_FAIL_FOR_TEAM(iped,iTeam)
							
							BOOL bResetTimer = TRUE
							
							//is there anyone else this team cares about?
							IF (MC_serverBD.iPedAggroedBitset[0] > 0)
							OR (MC_serverBD.iPedAggroedBitset[1] > 0)
								
								INT iPedLoop
								FOR iPedLoop = 0 TO (MC_serverBD.iNumPedCreated - 1)
									IF IS_BIT_SET(MC_serverBD.iPedAggroedBitset[GET_LONG_BITSET_INDEX(iPedLoop)], GET_LONG_BITSET_BIT(iPedLoop))
										IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedLoop].iPedBitsetThree,ciPED_BSThree_AggroFailHasDelay)
										AND SHOULD_AGRO_FAIL_FOR_TEAM(iPedLoop,iTeam)
											PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN Checking for leftover aggro countdown peds - found ped ",iPedLoop," for team ",iTeam)
											bResetTimer = FALSE
											iPedLoop = MC_serverBD.iNumPedCreated //Break out into a special value which gets caught below
										ENDIF
									ENDIF
								ENDFOR
							ENDIF
							
							IF bResetTimer
								//Therefore we have nobody left who needs a timer!
								PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN No more aggro countdown peds found for team ",iTeam,", clearing timer...")
								MC_serverBD.iPartCausingFail[iTeam] = -1
								MC_serverBD.iEntityCausingFail[iteam] = -1
								RESET_NET_TIMER(MC_serverBD.tdAggroCountdownTimer[iTeam])
								MC_serverBD.iAggroCountdownTimerSlack[iTeam] = 0
							ELSE
								PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN More aggro countdown peds found for team ",iTeam,", adding a bit of slack")
								MC_serverBD.iAggroCountdownTimerSlack[iTeam] += 500
							ENDIF
							
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
			
			// B* 4076335 - commented out
//			IF MC_serverBD_2.iCurrentPedRespawnLives[iped] <= 0
//			AND bCleanupOnDeath
//				EXIT
//			ENDIF

		ELSE // Else if this ped isn't injured
			ThisPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
			IF CLEANUP_PED_EARLY(iped,ThisPed)
				EXIT
			ENDIF
		ENDIF
	ELSE
		IF MC_serverBD_2.iCurrentPedRespawnLives[iped] <= 0
		AND NOT IS_BIT_SET(MC_serverBD_1.sCreatedCount.iSkipPedBitset[GET_LONG_BITSET_INDEX(iped)],GET_LONG_BITSET_BIT(iped))
			bcheckteamfail = TRUE
			PRINTLN( "[RCC MISSION] PROCESS_PED_BRAIN iped: ", iped, " - bcheckteamfail = TRUE - ped doen't exist")
		ENDIF
	ENDIF
	
	FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
		IF MC_serverBD_4.iCurrentHighestPriority[iteam] < FMMC_MAX_RULES
		AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_DontFleeDropOffs)
		//	PRINTLN("[RCC MISSION] FMMC_MAX_RULES near drop off for ped: ",iped)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iTeam[iteam] = ciPED_RELATION_SHIP_DISLIKE
			OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iTeam[iteam] = ciPED_RELATION_SHIP_HATE
		//		PRINTLN("[RCC MISSION] Rel group near drop off for ped: ",iped)
				IF NOT IS_VECTOR_ZERO(GET_DROP_OFF_CENTER_FOR_TEAM(DEFAULT,iteam))
			//		PRINTLN("[RCC MISSION] Drop off ok near drop off for ped: ",iped)
					IF NOT IS_PED_INJURED(ThisPed)
				//		PRINTLN("[RCC MISSION] Not injured near drop off for ped: ",iped)
						VECTOR vDropOff = GET_DROP_OFF_CENTER_FOR_TEAM(DEFAULT,iteam)
						FLOAT fDist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(ThisPed,vDropOff)
						BOOL bInaHeliOrPlane = IS_PED_IN_ANY_HELI(ThisPed) OR IS_PED_IN_ANY_PLANE(ThisPed) 
						
						IF ( bInaHeliOrPlane AND fDist < 350)
						OR ( NOT bInaHeliOrPlane  AND fDist < 150)
					//		PRINTLN("[RCC MISSION] Dist ok near drop off for ped: ",iped)
							IF NOT IS_BIT_SET(MC_serverBD.iPedNearDropOff[iteam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
								PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN iPedNearDropOff not set near drop off for ped: ",iped, " fDist = ", fDist, " vDropOff = ", vDropOff, " bInaHeliOrPlane = ", bInaHeliOrPlane)
								//If there isn't some other target I can focus on (only for helicopters/planes), flee!
								
								NETWORK_INDEX niOldTarget = MC_serverBD.niTargetID[iped]
								
								IF ( IS_PED_IN_ANY_HELI(ThisPed) OR IS_PED_IN_ANY_PLANE(ThisPed) )
								AND DOES_PED_HAVE_VALID_TARGET(iped,TRUE)
									PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN in a heli near drop off for ped: ",iped)
									IF niOldTarget != MC_serverBD.niTargetID[iped]
										PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN niOldTarget != MC_serverBD.niTargetID[iped] near drop off for ped: ",iped)
										//New target, retask the ped:
										SET_PED_RETASK_DIRTY_FLAG(iped)
									ENDIF
								ELSE		// url:bugstar:2214442 - Heli's now take off when a player is within activation range. ST 29/01/15.
									IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_DontFleeDropOffs)
										SET_BIT(MC_serverBD.iPedNearDropOff[iteam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))									
										SET_BIT(MC_serverBD.iPedShouldFleeDropOff[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
										PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN Setting near drop off for ped: ",iped)
									ENDIF
								ENDIF
							ENDIF
						ELSE
							//PRINTLN("[RCC MISSION] Dist NOT ok near drop off for ped: ",iped, " fDist =  ", fDist)
							IF IS_BIT_SET(MC_serverBD.iPedNearDropOff[iteam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
								CLEAR_BIT(MC_serverBD.iPedNearDropOff[iteam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
								PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN  Clearing near drop off for ped: ",iped)
							ENDIF
						ENDIF
					ELSE
						IF IS_BIT_SET(MC_serverBD.iPedNearDropOff[iteam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
							CLEAR_BIT(MC_serverBD.iPedNearDropOff[iteam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
							CLEAR_BIT(MC_serverBD.iPedShouldFleeDropOff[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
							PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN  Clearing near drop off for ped: ",iped)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF bcheckteamfail
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_Ped_Arrived_On_Death)
				IF NOT IS_BIT_SET(MC_serverBD.iPedAtGotoLocBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					MANAGE_PED_ARRIVAL_RESULTS(iped, TRUE)
				ENDIF
			ENDIF
			
			IF (MC_serverBD_4.iPedPriority[iped][iteam] < FMMC_MAX_RULES
			OR IS_BIT_SET(MC_serverBD.iMissionCriticalPed[iTeam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped)))
			AND NOT HAS_TEAM_FAILED(iTeam)
				PRINTLN( "[RCC MISSION] PROCESS_PED_BRAIN iped: ", iped, " - bcheckteamfail is TRUE, Running RUN_TEAM_PED_FAIL_CHECKS, PedCritical: ", BOOL_TO_STRING(IS_BIT_SET(MC_serverBD.iMissionCriticalPed[iTeam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))))
				RUN_TEAM_PED_FAIL_CHECKS(iped,iteam,MC_serverBD_4.iPedPriority[iped][iteam])
			ELSE
				PRINTLN( "[RCC MISSION] PROCESS_PED_BRAIN iped: ", iped, " - bcheckteamfail is TRUE, skipping fail checks - ",
				" PedCritical: ", BOOL_TO_STRING(IS_BIT_SET(MC_serverBD.iMissionCriticalPed[iTeam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))), 
				", MC_serverBD_4.iPedPriority[iped][iteam]: ", MC_serverBD_4.iPedPriority[iped][iteam],
				", HAS_TEAM_FAILED(iTeam) = ", BOOL_TO_STRING(HAS_TEAM_FAILED(iTeam)))
			ENDIF
			
		ENDIF
		
		INT iRuleToSkipTo = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedSkipToRuleWhenAggro[iTeam]		
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iped])
			
			PRINTLN("[LM][url:bugstar:3966118] PROCESS_PED_BRAIN Ped Exists iPed, ", iPed, "  - iRuleToSkipTo: ", iRuleToSkipTo)
			
			IF iRuleToSkipTo > -1 AND iRuleToSkipTo > MC_serverBD_4.iCurrentHighestPriority[iTeam] AND MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
			
				
				PRINTLN("[LM][url:bugstar:3966118] PROCESS_PED_BRAIN iPedAggroedBitset" , IS_BIT_SET(MC_serverBD.iPedAggroedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed)))
				PRINTLN("[LM][url:bugstar:3966118] PROCESS_PED_BRAIN iObjectivePedAggroedBitset", IS_BIT_SET(MC_serverBD.iObjectivePedAggroedBitset[iTeam],MC_serverBD_4.iCurrentHighestPriority[iTeam]))
				
				
				IF (IS_BIT_SET(MC_serverBD.iPedAggroedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				OR IS_BIT_SET(MC_serverBD.iObjectivePedAggroedBitset[iTeam],MC_serverBD_4.iCurrentHighestPriority[iTeam]))
				AND IS_PED_RULE_SKIP_VALID_ON_THIS_RULE(iPed, iTeam)
				
					PRINTLN("[LM][url:bugstar:3966118] PROCESS_PED_BRAIN In here... ")
					
					BOOL bSkipNow = TRUE
				
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree,ciPED_BSThree_AggroFailHasDelay)
						IF HAS_NET_TIMER_STARTED(MC_ServerBD_4.tdPedRuleSkipTimer)
							IF NOT HAS_NET_TIMER_EXPIRED(MC_ServerBD_4.tdPedRuleSkipTimer, ciPED_AGGRO_RULE_SKIP_DELAY)
								PRINTLN("[LM][url:bugstar:3966118] PROCESS_PED_BRAIN In here... FALSE 1 ")
								bSkipNow = FALSE
							ENDIF
						ELSE
							REINIT_NET_TIMER(MC_ServerBD_4.tdPedRuleSkipTimer)
							PRINTLN("[TMS] PROCESS_PED_BRAIN starting aggro skip timer for Ped ", iped)
							
							PRINTLN("[LM][url:bugstar:3966118] PROCESS_PED_BRAIN In here... FALSE 2 ")
							bSkipNow = FALSE
						ENDIF
					ENDIF
					
					IF bSkipNow	
						PRINTLN("[LM][url:bugstar:3966118] PROCESS_PED_BRAIN In here... TRUE.. GO ")
						//Past content always forced passed so leaving as it was
						BOOL bForcePass = NOT IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
						INVALIDATE_UP_TO_AND_INCLUDING_OBJECTIVE(iTeam, iRuleToSkipTo - 1, FALSE, bForcePass, FALSE)
						RECALCULATE_OBJECTIVE_LOGIC(iTeam)
						PRINTLN("[TMS] PROCESS_PED_BRAIN Ped ", iped, " Is aggro'd so we have skipped to rule ", MC_serverBD_4.iCurrentHighestPriority[iTeam], " for team ", iTeam)
						RESET_NET_TIMER(MC_ServerBD_4.tdPedRuleSkipTimer)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Aggro countdown (grace period) fail logic:
		IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_AGGRO_WILL_FAIL_FOR_TEAM_0 + iTeam)
			IF IS_BIT_SET(MC_serverBD.iPedAggroedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree, ciPED_BSThree_AggroFailHasDelay)
				IF NOT IS_PED_INJURED(ThisPed)
				AND HAS_NET_TIMER_STARTED(MC_serverBD.tdAggroCountdownTimer[iTeam])
				AND SHOULD_AGRO_FAIL_FOR_TEAM(iped,iTeam)
					IF (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdAggroCountdownTimer[iTeam]) > 3500 + MC_serverBD.iAggroCountdownTimerSlack[iTeam]) //3.5 seconds
						
						SET_BIT(MC_serverBD.iServerBitSet1, SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_0 + iTeam)
						
						IF iTeam > -1 AND iTeam < FMMC_MAX_TEAMS
						AND MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE13_DISABLE_CCTV_CAMERAS_TRIGGERING)			
							AND NOT IS_BIT_SET(iPedHitByStunGun[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
								PRINTLN("[RCC MISSION] PROCESS_SERVER_PED_AGGRO_SETTING_DATA ped ", iped, " Setting SBBOOL7_CCTV_TEAM_AGGROED_PEDS")
								SET_BIT(MC_serverBD.iServerBitSet7, SBBOOL7_CCTV_TEAM_AGGROED_PEDS)
							ENDIF
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThree, ciPED_BSThree_AggroFailHasDelay)
							IF NOT IS_PED_INJURED(ThisPed)
								PRINTLN("[RCC MISSION] PROCESS_SERVER_PED_AGGRO_SETTING_DATA ped ", iped, " is setting iObjectivePedAggroedBitset with ruke: ", MC_serverBD_4.iCurrentHighestPriority[iTeam])
								SET_BIT(MC_serverBD.iObjectivePedAggroedBitset[iTeam],MC_serverBD_4.iCurrentHighestPriority[iTeam])
							ENDIF
						ENDIF
						
						//Alert all other aggro peds if creator setting tells us to:
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_AggroAlertsAllOtherAggroPeds)
							PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN Ped ",iped," aggro timer run out, now alert all other aggro peds!")
							ALERT_ALL_AGGRO_PEDS(iTeam)
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEleven, ciPed_BSEleven_FailDelayForWanted)
							IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
								PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN Ped ",iped," aggro timer run out, set iObjectivePedAggroedBitset for team: ", iTeam," rule: ", MC_serverBD_4.iCurrentHighestPriority[iTeam])
								SET_BIT(MC_serverBD.iObjectivePedAggroedBitset[iTeam], MC_serverBD_4.iCurrentHighestPriority[iTeam])
							ENDIF
						ENDIF
						
						IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroDoesntFail)
							
							IF MC_serverBD.iPartCausingFail[iTeam] = -1
								MC_serverBD.iPartCausingFail[iTeam] = GET_PART_PED_IS_IN_COMBAT_WITH(ThisPed)
								PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN GET_PART_PED_IS_IN_COMBAT_WITH - Ped ",iped," is in combat with part ", MC_serverBD.iPartCausingFail[iTeam])
								
								IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO)
									IF MC_serverBD.iPartCausingFail[iTeam] = -1
										MC_serverBD.iPartCausingFail[iTeam] =  GET_PART_CLOSEST_TO_ALERTED_PED(ThisPed)
										PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN GET_PART_CLOSEST_TO_ALERTED_PED - Getting part ", MC_serverBD.iPartCausingFail[iTeam], " as a backup")
									ENDIF
								ENDIF
							ENDIF
							
							MC_serverBD.iEntityCausingFail[iteam] = iped
							
							IF NOT IS_BIT_SET(MC_serverBD.iPedSpookedByDeadBodyBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
								MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_PED_AGRO
//								SET_TEAM_FAIL_REASON(iteam,int_to_enum(eFailMissionEnum,enum_to_int(mFail_PED_AGRO_T0)+iteam),false,MC_serverBD.iPartCausingFail[iTeam])
							ELSE
								MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_PED_FOUND_BODY
								SET_BIT(MC_serverBD.iServerBitSet2,SBBOOL2_AGGRO_FAIL_DUE_TO_BODY_FOUND_TEAM_0 + iTeam)
//								SET_TEAM_FAIL_REASON(iteam,mFail_PED_FOUND_BODY)
							ENDIF
							
							SET_TEAM_OBJECTIVE_OUTCOME(iTeam,MC_serverBD_4.iCurrentHighestPriority[iTeam],FALSE)
							SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam,MC_serverBD_4.iCurrentHighestPriority[iTeam],MC_serverBD.iReasonForObjEnd[iTeam],TRUE)
							BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam],iTeam,mc_serverBD_4.iCurrentHighestPriority[iTeam],DID_TEAM_PASS_OBJECTIVE(iTeam,mc_serverBD_4.iCurrentHighestPriority[iTeam]),TRUE)
							
							REINIT_NET_TIMER(MC_serverBD.tdAggroCountdownTimer[iTeam])
							SET_BIT(MC_serverBD.iServerBitSet1,SBBOOL1_AGGRO_WILL_FAIL_FOR_TEAM_0 + iTeam)
							
							PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN MISSION IS GOING TO FAIL FOR TEAM:",iTeam," BECAUSE PED AGGRO COUNTDOWN RAN OUT: ",iped)
						ELSE
							RESET_NET_TIMER(MC_serverBD.tdAggroCountdownTimer[iTeam])
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF MC_serverBD_4.iPedPriority[iped][iteam] < FMMC_MAX_RULES
			
			#IF IS_DEBUG_BUILD
			IF bDoPedCountPrints
			PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN - iNumHighPriorityPed - iped = ", iped, ", iteam = ", iteam, " ped priority = ", MC_serverBD_4.iPedPriority[iped][iteam])
			ENDIF
			#ENDIF
			
			IF NOT bcheckteamfail OR (bcheckteamfail AND IS_BIT_SET(MC_serverBD.iDeadPedPhotoBitset[iteam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed)))
				
				#IF IS_DEBUG_BUILD
				IF bDoPedCountPrints
				BOOL bDeadPhotoBitSet = IS_BIT_SET(MC_serverBD.iDeadPedPhotoBitset[iteam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN - iNumHighPriorityPed - iped = ", iped, ", iteam = ", iteam, " bcheckteamfail = ", bcheckteamfail, ", iDeadPedPhotoBitset = ", bDeadPhotoBitSet)
				ENDIF
				#ENDIF
				
				IF MC_serverBD_4.iPedPriority[iped][iteam] <= iCurrentHighPriority[iteam]
					
					#IF IS_DEBUG_BUILD
					IF bDoPedCountPrints
					PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN - iNumHighPriorityPed - iped = ", iped, ", iteam = ", iteam, " iCurrentHighPriority = ", iCurrentHighPriority[iteam])
					ENDIF
					#ENDIF
					
					IF MC_serverBD_4.iPedRule[iped][iteam] != FMMC_OBJECTIVE_LOGIC_NONE
						
						#IF IS_DEBUG_BUILD
						IF bDoPedCountPrints
						PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN - iNumHighPriorityPed - iped = ", iped, ", iteam = ", iteam, " iPedRule = ", MC_serverBD_4.iPedRule[iped][iteam])
						ENDIF
						#ENDIF
						
						IF MC_serverBD_4.iPedRule[iped][iteam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD
						
							IF MC_serverBD_4.iPedPriority[iped][iteam] <= MC_serverBD_4.iCurrentHighestPriority[iteam]
								IF NOT IS_PED_INJURED(ThisPed)
									IF IS_ENTITY_IN_DROP_OFF_AREA(ThisPed, iteam, MC_serverBD_4.iPedPriority[iped][iteam], ciRULE_TYPE_PED, iped)
										IF NOT IS_BIT_SET(MC_serverBD.iPedAtYourHolding[iteam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
											SET_BIT(MC_serverBD.iPedAtYourHolding[iteam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
											IF MC_serverBD_2.iPartPedFollows[iped] > -1
												tempPart = INT_TO_PARTICIPANTINDEX(MC_serverBD_2.iPartPedFollows[iped])
												IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
													IF MC_PlayerBD[MC_serverBD_2.iPartPedFollows[iped]].iteam = iteam
														tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
														SET_BIT(MC_serverBD.iPedHoldPartPoints,MC_serverBD_2.iPartPedFollows[iped])
														INCREMENT_SERVER_REMOTE_PLAYER_SCORE(tempPlayer, GET_FMMC_POINTS_FOR_TEAM(iteam,MC_serverBD_4.iPedPriority[iped][iteam]), iteam, MC_serverBD_4.iPedPriority[iped][iteam])
													ENDIF
												ENDIF
											ENDIF
											PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN - Setting at your holding (MC_serverBD.iPedAtYourHolding) for team ",iteam,", ped ",iped," as in drop off")
										ENDIF
									ELSE
										IF IS_BIT_SET(MC_serverBD.iPedAtYourHolding[iteam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
											CLEAR_BIT(MC_serverBD.iPedAtYourHolding[iteam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
											FOR ipart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
												IF IS_BIT_SET(MC_serverBD.iPedHoldPartPoints,ipart)
													tempPart = INT_TO_PARTICIPANTINDEX(ipart)
													IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
														tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
														INCREMENT_SERVER_REMOTE_PLAYER_SCORE(tempPlayer, (-1 * GET_FMMC_POINTS_FOR_TEAM(iteam,MC_serverBD_4.iPedPriority[iped][iteam])), iteam, MC_serverBD_4.iPedPriority[iped][iteam])
													ENDIF
													CLEAR_BIT(MC_serverBD.iPedHoldPartPoints,ipart)
												ENDIF
											ENDFOR
											PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN - Clearing at your holding (MC_serverBD.iPedAtYourHolding) for team ",iteam,", ped ",iped," as no longer in drop off")
										ENDIF
									ENDIF
								ELSE
									IF IS_BIT_SET(MC_serverBD.iPedAtYourHolding[iteam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
										INCREMENT_SERVER_TEAM_SCORE(iteam,MC_serverBD_4.iPedPriority[iped][iteam], -GET_FMMC_POINTS_FOR_TEAM(iteam,MC_serverBD_4.iPedPriority[iped][iteam]))
										CLEAR_BIT(MC_serverBD.iPedAtYourHolding[iteam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
										PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN - Clearing at your holding (MC_serverBD.iPedAtYourHolding) for team ",iteam,", ped ",iped," as ped is injured")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT bEveryFrame // Only run this stuff in sync with the staggered loop:
							IF MC_serverBD_4.iPedPriority[iped][iteam] <= MC_serverBD_4.iCurrentHighestPriority[iteam]
								iTempNumPriorityRespawnPed[iteam] = iTempNumPriorityRespawnPed[iteam] + MC_serverBD_2.iCurrentPedRespawnLives[iped]
							ENDIF
							icurrentHighPriority[iteam] = MC_serverBD_4.iPedPriority[iped][iteam]
							itempPedMissionLogic[iteam] = MC_serverBD_4.iPedRule[iped][iteam]
							IF iOldHighPriority[iteam] > icurrentHighPriority[iteam]
								iNumHighPriorityPed[iteam] = 0
								iNumHighPriorityDeadPed[iteam] = 0
								iOldHighPriority[iteam] = icurrentHighPriority[iteam]
							ENDIF
							iNumHighPriorityLoc[iteam] = 0
							itempLocMissionLogic[iteam] = FMMC_OBJECTIVE_LOGIC_NONE
							iNumHighPriorityObj[iteam] = 0
							itempObjMissionLogic[iteam] = FMMC_OBJECTIVE_LOGIC_NONE
							itempObjMissionSubLogic[iteam] = ci_HACK_SUBLOGIC_NONE
							iNumHighPriorityVeh[iteam] = 0
							itempVehMissionLogic[iteam] = FMMC_OBJECTIVE_LOGIC_NONE
							iNumHighPriorityPlayerRule[iteam] = 0
							itempPlayerRuleMissionLogic[iteam] = FMMC_OBJECTIVE_LOGIC_NONE
							iNumHighPriorityPed[iteam]++
							IF IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
								IF IS_BIT_SET(MC_serverBD.iDeadPedPhotoBitset[iteam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
									iNumHighPriorityDeadPed[iteam]++
								ENDIF
							ENDIF
							//NET_PRINT("iNumHighPriorityPed: ") NET_PRINT_INT(iNumHighPriorityPed) NET_NL()
						ENDIF
						
					ELSE
						#IF IS_DEBUG_BUILD
						IF bDoPedCountPrints
						PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN - iNumHighPriorityPed* - iped = ", iped, ", iteam = ", iteam, " iPedRule = ", MC_serverBD_4.iPedRule[iped][iteam], " = FMMC_OBJECTIVE_LOGIC_NONE")
						ENDIF
						#ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					IF bDoPedCountPrints
					PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN - iNumHighPriorityPed* - iped = ", iped, ", iteam = ", iteam, " iCurrentHighPriority = ", iCurrentHighPriority[iteam], " > high priority.")
					ENDIF
					#ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				IF bDoPedCountPrints
				BOOL bDeadPhotoBitSet = IS_BIT_SET(MC_serverBD.iDeadPedPhotoBitset[iteam][GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN - iNumHighPriorityPed* - iped = ", iped, ", iteam = ", iteam, " bcheckteamfail = ", bcheckteamfail, ", iDeadPedPhotoBitset = ", bDeadPhotoBitSet)
				ENDIF
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			IF bDoPedCountPrints
			PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN - iNumHighPriorityPed* - iped = ", iped, ", iteam = ", iteam, " ped priority = ", MC_serverBD_4.iPedPriority[iped][iteam])
			ENDIF
			#ENDIF
		ENDIF
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
			IF MC_serverBD_4.iPedPriority[iPed][iteam] < FMMC_MAX_RULES
			AND MC_serverBD_4.iPedPriority[iPed][iteam] > -1
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[MC_serverBD_4.iPedPriority[iPed][iteam]], ciBS_RULE10_PASS_RULE_THROUGH_TAGGING)
					IF IS_ENTITY_ALREADY_TAGGED(ENUM_TO_INT(ET_PED),iPed)
						PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN PED HAS BEEN TAGGED. Calling PROGRESS_SPECIFIC_OBJECTIVE")
						INVALIDATE_SINGLE_OBJECTIVE(iteam, MC_ServerBD_4.iPedPriority[iPed][iTeam], TRUE)
					ENDIF
				ENDIF
				
				IF MC_serverBD_4.iPedRule[iPed][iteam] = FMMC_OBJECTIVE_LOGIC_PROTECT
				AND MC_serverBD_4.iPedPriority[iPed][iteam] <= MC_serverBD_4.iCurrentHighestPriority[iteam]
				AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[MC_serverBD_4.iPedPriority[iPed][iteam]], ciBS_RULE10_PROGRESS_PROTECT_WHEN_AIMING_AT_PED)
					INT i, iPartsChecked = 0
					FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
						IF MC_playerBD[i].iteam = iteam
							IF ((NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet,PBBOOL_ANY_SPECTATOR)) OR IS_BIT_SET(MC_playerBD[i].iClientBitSet,PBBOOL_HEIST_SPECTATOR))
							AND (NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet,PBBOOL_PLAYER_FAIL))
							AND IS_BIT_SET(MC_playerBD[i].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
							AND (NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_FINISHED))
								IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
									
									iPartsChecked++
									
									IF IS_BIT_SET(MC_PlayerBD[i].iClientBitSet4, PBBOOL4_PLAYER_IS_AIMING_AT_PROTECT_PED)
										PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN PED is being aimed at by part ",i," on team ",iteam,", calling PROGRESS_SPECIFIC_OBJECTIVE")
										PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED, iped, iteam, TRUE)
										RECALCULATE_OBJECTIVE_LOGIC(iteam)
										BREAKLOOP
									ENDIF
									
									IF iPartsChecked > MC_serverBD.iNumberOfPlayingPlayers[iteam]
										BREAKLOOP
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
		ENDIF
		
	ENDFOR
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPED_BSEight_UseDistanceToCapture)
		MANAGE_SERVER_GROUPING_DISTANCE_AND_CONTROL_DATA(iped, bEveryFrame)
	ELSE
		MANAGE_SERVER_GROUPING_DISTANCE_AND_CONTROL_DATA(iped, bEveryFrame)
	ENDIF
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iped])
		IF DOES_ENTITY_EXIST(ThisPed)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedSpeedRange != -1
			OR SHOULD_PED_SPEED_UP_FROM_BEING_ALERTED(iPed, ThisPed)
				IF NOT IS_BIT_SET(MC_serverBD_4.iPedSpeedOverride[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					PRINTLN("[JS] PROCESS_PED_BRAIN Ped ",iped,"'s speed is to be overridden if a player is within ",g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedSpeedRange, " ciPed_BSNine_Speed_Override_On_Spooked_Alerted): ", IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetNine, ciPed_BSNine_Speed_Override_On_Spooked_Alerted))
					IF CHECK_PLAYERS_IN_RANGE_OF_PED(iped, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedSpeedRange)
					OR SHOULD_PED_SPEED_UP_FROM_BEING_ALERTED(iPed, ThisPed)
						SET_BIT(MC_serverBD_4.iPedSpeedOverride[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					ENDIF
				ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetNine, ciPed_BSNine_SpeedOverrideAutoDeactivate)
					IF NOT CHECK_PLAYERS_IN_RANGE_OF_PED(iped, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedSpeedRange)
					AND NOT SHOULD_PED_SPEED_UP_FROM_BEING_ALERTED(iPed, ThisPed)
						PRINTLN("[JS] PROCESS_PED_BRAIN Ped ",iped,"'s speed override deactivated (out of range) - ciPed_BSNine_Speed_Override_On_Spooked_Alerted): ", IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetNine, ciPed_BSNine_Speed_Override_On_Spooked_Alerted))
						CLEAR_BIT(MC_serverBD_4.iPedSpeedOverride[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
	OR IS_BIT_SET(MC_serverBD.iPedInvolvedInMiniGameBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(MC_serverBD.iPedTaskActive[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_TriggerTasksOnAggro)
			MANAGE_PED_TASK_ACTIVATION(iped,ThisPed)
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(MC_serverBD.iPedTaskStopped[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
	
			IF NOT SHOULD_PED_STOP_TASKS(iped)
				IF NOT IS_PRIMARY_CUSTOM_TARGET_ENTITY_IN_CORRECT_STATE_FOR_TASK(iped, ThisPed, TRUE)
					
					PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN Ped ",iped," dropping out of primary task")
					
					CLEAR_BIT(MC_serverBD.iPedTaskActive[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					MC_ServerBD_2.iAssociatedAction[iped] = ciACTION_NOTHING
					MC_serverBD.niTargetID[iped] = NULL
					MC_serverBD_2.iTargetID[iped] = -1
					
					IF MC_serverBD_2.iPedState[iped] != ciTASK_CHOOSE_NEW_TASK
						IF NOT IS_PED_IN_COMBAT(ThisPed)
						AND CAN_PED_TASKS_BE_CLEARED(ThisPed)
							CLEAR_PED_TASKS(ThisPed)
						ENDIF
						PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN clearing ped task, dropped out of primary task: ",iped)
						SET_PED_STATE(iped,ciTASK_CHOOSE_NEW_TASK)
					ENDIF
					
				ELSE
					IF NOT (MC_serverBD_2.iPedState[iped] = ciTASK_CHOOSE_NEW_TASK OR MC_serverBD_2.iPedState[iped] = ciTASK_GOTO_COORDS)
					AND NOT IS_BIT_SET(MC_serverBD.iPedAtGotoLocBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
						//Shouldn't drop out of these states:
						IF NOT (MC_serverBD_2.iPedState[iped] = ciTASK_HUNT_FOR_PLAYER OR MC_serverBD_2.iPedState[iped] = ciTASK_FOUND_BODY_RESPONSE)
							//Ped hasn't finished their Go To yet, but isn't running it - check if they need to get back on that:
							PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN hasn't yet finished GoTo, checking to see if they should resume it: ",iped)
							PROCESS_PED_TASK_NOTHING_BRAIN(ThisPed,iped)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				SET_BIT(MC_serverBD.iPedTaskStopped[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN Task Stopped reached for ped: ",iped)
				
				IF MC_serverBD_2.iPedState[iped] = ciTASK_GOTO_COORDS
				AND SHOULD_PED_DO_DEFENSIVE_AREA_GOTO(ThisPed, iped)
					SET_BIT(MC_serverBD.iPedDoneDefensiveAreaGoto[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				ENDIF
				
				MC_ServerBD_2.iAssociatedAction[iped] = ciACTION_NOTHING
				MC_serverBD.niTargetID[iped] = NULL
				MC_serverBD_2.iTargetID[iped] = -1
				SET_BIT(MC_serverBD.iPedAtGotoLocBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				
				IF MC_serverBD_2.iPedState[iped] != ciTASK_CHOOSE_NEW_TASK
					IF NOT IS_PED_IN_COMBAT(ThisPed)
					AND CAN_PED_TASKS_BE_CLEARED(ThisPed)
						CLEAR_PED_TASKS(ThisPed)
					ENDIF
					PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN clearing ped task, Task Stopped trigger hit: ",iped)
					SET_PED_STATE(iped,ciTASK_CHOOSE_NEW_TASK)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(MC_serverBD.iPedSecondaryTaskCompleted[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		IF NOT IS_BIT_SET(MC_serverBD.iPedSecondaryTaskActive[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			MANAGE_PED_SECONDARY_TASK_ACTIVATION(iped, ThisPed)
		ELSE
			IF (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetSix, ciPED_BSSix_DeactivateSecondaryTaskWhenOutOfTriggerDistance) AND (NOT IS_PED_WITHIN_RANGE_OF_SECONDARY_CUSTOM_TARGET_ENTITY(ThisPed, iped, TRUE)))
			OR HAS_PLAYER_GOT_CONTROL_OF_SECONDARY_CUSTOM_TARGET_ENTITY(iped) // Checks the bit for this setting being on before doing anything silly
				
				PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN Ped ",iped," dropping out of secondary task")
				
				CLEAR_BIT(MC_serverBD.iPedSecondaryTaskActive[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				MC_ServerBD_2.iAssociatedAction[iped] = ciACTION_NOTHING
				MC_serverBD.niTargetID[iped] = NULL
				MC_serverBD_2.iTargetID[iped] = -1
				
				IF MC_serverBD_2.iPedState[iped] != ciTASK_CHOOSE_NEW_TASK
					IF NOT IS_PED_IN_COMBAT(ThisPed)
					AND CAN_PED_TASKS_BE_CLEARED(ThisPed)
						CLEAR_PED_TASKS(ThisPed)
					ENDIF
					PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN clearing ped task, dropped out of secondary task: ",iped)
					SET_PED_STATE(iped,ciTASK_CHOOSE_NEW_TASK)
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
	
	IF MC_serverBD_2.iOldPedState[iped] != MC_serverBD_2.iPedState[iped]
		
		IF NOT IS_BIT_SET(MC_serverBD.iPedDelivered[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		AND NOT IS_BIT_SET(MC_serverBD.iPedInvolvedInMiniGameBitSet[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iped))
			IF 	NOT IS_PED_IN_COMBAT(ThisPed)
			AND CAN_PED_TASKS_BE_CLEARED(ThisPed)
			AND (NOT IS_BIT_SET( iPedHitByStunGun[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed)) AND HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(ThisPed, WEAPONTYPE_STUNGUN))

//				CLEAR_PED_TASKS(ThisPed)
				SET_PED_RETASK_DIRTY_FLAG(iPed)
				PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN clearing ped task new state: ",iped)
			ENDIF
		ENDIF
		MC_serverBD_2.iOldPedState[iped] = MC_serverBD_2.iPedState[iped]
	ENDIF
	
	IF MC_serverBD_2.iPedState[iped] != ciTASK_CHOOSE_NEW_TASK
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetSix, ciPED_BSSix_AllowTaskingWhileGrouped)
		AND MC_serverBD_2.iPartPedFollows[iped] != -1
			IF NOT IS_BIT_SET(MC_serverBD.iPedDelivered[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				IF CAN_PED_TASKS_BE_CLEARED(ThisPed)
//					CLEAR_PED_TASKS(ThisPed)
					SET_PED_RETASK_DIRTY_FLAG(iPed)
				ENDIF
			ENDIF
			PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN clearing ped task, following: ",iped)
			SET_PED_STATE(iped,ciTASK_CHOOSE_NEW_TASK)
		ENDIF
	ENDIF
	
	IF ( MC_serverBD_2.iPedOldGoto[iped] != MC_serverBD_2.iPedGotoProgress[iped] )
	AND ( IS_PED_IN_ANY_HELI(ThisPed) OR IS_PED_IN_ANY_PLANE(ThisPed) )
		IF CAN_PED_TASKS_BE_CLEARED(ThisPed)
//			CLEAR_PED_TASKS(ThisPed)
			PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN Heli or plane ped double task (why?): ",iped)
			SET_PED_RETASK_DIRTY_FLAG(iPed)
		ENDIF
		MC_serverBD_2.iPedOldGoto[iped] = MC_serverBD_2.iPedGotoProgress[iped]
	ENDIF
	
	IF MC_serverBD.niTargetID[iped] != NULL
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD.niTargetID[iped])
			tempTarget = NET_TO_ENT(MC_serverBD.niTargetID[iped])
			IF IS_ENTITY_DEAD(tempTarget)
				IF CAN_PED_TASKS_BE_CLEARED(ThisPed)
//					CLEAR_PED_TASKS(ThisPed)
					SET_PED_RETASK_DIRTY_FLAG(iPed)
				ENDIF
				PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN clearing ped task, target ent dead: ",iped) 
				SET_PED_STATE(iped,ciTASK_CHOOSE_NEW_TASK)
				MC_serverBD.niTargetID[iped] = NULL
			ELSE
				btargetOK = TRUE
			ENDIF
		ELSE
			IF CAN_PED_TASKS_BE_CLEARED(ThisPed)
//				CLEAR_PED_TASKS(ThisPed)
				SET_PED_RETASK_DIRTY_FLAG(iPed)
			ENDIF
			PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN clearing ped task, target ent not exist: ",iped) 
			SET_PED_STATE(iped,ciTASK_CHOOSE_NEW_TASK)
			MC_serverBD.niTargetID[iped] = NULL
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(ThisPed)
		IF IS_PED_IN_ANY_VEHICLE(ThisPed)
			VEHICLE_INDEX VehPed = GET_VEHICLE_PED_IS_IN(ThisPed)
								
			IF IS_ENTITY_ALIVE(VehPed)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedGotoHeightOffset != -101
				AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedGoToHeightOffsetAtHealth != -1
					FLOAT fPercentageThreshhold = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedGoToHeightOffsetAtHealth)
					
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle > -1
						PRINTLN("[LM][TASK_GO_TO_COORD_ANY_MEANS_etc][iPedGotoHeightOffset] PROCESS_PED_BRAIN - GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE: ", GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(VehPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle, MC_serverBD.iTotalNumStartingPlayers), " fPercentageThreshhold: ", fPercentageThreshhold, " iVehicle: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle)
						
						IF GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(VehPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iVehicle, MC_serverBD.iTotalNumStartingPlayers) < fPercentageThreshhold
							IF NOT IS_BIT_SET(MC_serverBD_4.iPedHeightOverride[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))				
								PRINTLN("[LM][TASK_GO_TO_COORD_ANY_MEANS_etc][iPedGotoHeightOffset] PROCESS_PED_BRAIN - SETTING VEHICLE TO BE RE-TASKED - GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE")
								SET_BIT(MC_serverBD_4.iPedHeightOverride[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
								SET_PED_RETASK_DIRTY_FLAG(iPed)
								
								#IF IS_DEBUG_BUILD
								TEXT_LABEL_15 pedStateName2 = GET_PED_STATE_NAME(MC_serverBD_2.iPedState[iped])								
								PRINTLN("[LM][RCC MISSION][iPedGotoHeightOffset] PROCESS_PED_BRAIN - iped: ", iPed, " GET_PED_STATE_NAME: ", pedStateName2, " Before")
								#ENDIF
							
								SET_PED_STATE(iPed, ciTASK_CHOOSE_NEW_TASK)
								
								#IF IS_DEBUG_BUILD
								pedStateName2 = GET_PED_STATE_NAME(MC_serverBD_2.iPedState[iped])								
								PRINTLN("[LM][RCC MISSION][iPedGotoHeightOffset] PROCESS_PED_BRAIN - iped: ", iPed, " GET_PED_STATE_NAME: ", pedStateName2, " After")
								#ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				// If the ped isn't a driver, and there is a turret on the vehicle, and they have the option set, then they will switch to it.
				IF SHOULD_PED_SHUFFLE_TO_TURRET_SEAT_FROM_BEING_ALERTED(iPed, ThisPed)
				AND GET_PED_IN_VEHICLE_SEAT(VehPed, VS_DRIVER) != ThisPed
					VEHICLE_SEAT seatTurret = INT_TO_ENUM(VEHICLE_SEAT, GetFirstFreeSeatOfVehicle(VehPed, TRUE, FALSE, TRUE))
					
					PRINTLN("[LM][ShuffleTurretSeatAlerted] PROCESS_PED_BRAIN - Seat Selected: ", ENUM_TO_INT(seatTurret), " Option is set that we want to shuffle to an available turret seat when alerted.")
					PRINTLN("[LM][ShuffleTurretSeatAlerted] PROCESS_PED_BRAIN - IS_TURRET_SEAT (desired): ", IS_TURRET_SEAT(VehPed, seatTurret))
					PRINTLN("[LM][ShuffleTurretSeatAlerted] PROCESS_PED_BRAIN - IS_TURRET_SEAT (we're in): ", IS_TURRET_SEAT(VehPed, GET_SEAT_PED_IS_IN(ThisPed, TRUE)))
					PRINTLN("[LM][ShuffleTurretSeatAlerted] PROCESS_PED_BRAIN - GET_PED_IN_VEHICLE_SEAT: ", GET_PED_IN_VEHICLE_SEAT(VehPed, seatTurret) != ThisPed)
					
					IF IS_TURRET_SEAT(VehPed, seatTurret)
					AND IS_VEHICLE_SEAT_FREE(VehPed, seatTurret, TRUE)
					AND NOT IS_TURRET_SEAT(VehPed, GET_SEAT_PED_IS_IN(ThisPed, TRUE))
					AND GET_PED_IN_VEHICLE_SEAT(VehPed, seatTurret) != ThisPed
						SCRIPTTASKSTATUS TaskStatus = GET_SCRIPT_TASK_STATUS(ThisPed, SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT)
						
						IF (TaskStatus != WAITING_TO_START_TASK)
						AND (TaskStatus != PERFORMING_TASK)
							PRINTLN("[LM][ShuffleTurretSeatAlerted] PROCESS_PED_BRAIN - SETTING VEHICLE TO BE RE-TASKED - SHOULD_PED_SHUFFLE_TO_TURRET_SEAT_FROM_BEING_ALERTED")
							SET_PED_STATE(iPed, ciTASK_SHUFFLE_VEHICLE_SEAT_TO_TURRET)
						ELSE
							PRINTLN("[LM][RCC MISSION][ShuffleTurretSeatAlerted] PROCESS_PED_BRAIN - iped: ", iPed, " Already Performing Task.")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RUN_SERVER_PED_BACKUP_SPOOK_CANCEL_TASKS_LOGIC(ThisPed, iped)
	
#IF IS_DEBUG_BUILD
	TEXT_LABEL_15 pedStateName = GET_PED_STATE_NAME(MC_serverBD_2.iPedState[iped])
	PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN - Ped ",iped, "(SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(ThisPed), " State: ", pedStateName)
#ENDIF
	SWITCH MC_serverBD_2.iPedState[iped]
				
		CASE ciTASK_CHOOSE_NEW_TASK
			
			PROCESS_PED_TASK_NOTHING_BRAIN(ThisPed, iped)
			
		BREAK

		CASE ciTASK_GOTO_COORDS
			
			IF (MC_serverBD_2.iTargetID[iped] = -1
			OR MC_serverBD_2.iPartPedFollows[iped] !=-1	
			OR (IS_ASSOCIATED_GOTO_TASK_DATA__ENTIRE_BIT_SET(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData, ciENTIRE_TASK_BITSET_LOOP_GOTO) AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour,ciPED_BSFour_StayInTasks_On_Aggro) AND SHOULD_PED_START_COMBAT(ThisPed,iped)))
			AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPED_BSEight_UseDistanceToCapture)
				IF MC_serverBD_2.iTargetID[iped] != ciSECONDARY_GOTO
					SET_BIT(MC_serverBD.iPedAtGotoLocBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				ELSE
					SET_BIT(MC_serverBD.iPedAtSecondaryGotoLocBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				ENDIF
				SET_PED_STATE(iped,ciTASK_CHOOSE_NEW_TASK)
			ELSE
				bArrived = HAS_PED_ARRIVED_AT_GOTO_COORDS_DESTINATION(ThisPed, iped)
				
				IF bArrived
					IF MC_serverBD_2.iTargetID[iped] != ciSECONDARY_GOTO
						
						BOOL bWasOnDefensiveAreaGoto
						
						IF SHOULD_PED_DO_DEFENSIVE_AREA_GOTO(ThisPed, iped)
							bWasOnDefensiveAreaGoto = TRUE
						ENDIF
						
						IF SHOULD_PED_MOVE_ON_FROM_GOTO_COORDS_STATE(iped, bGoToIsLooped, TRUE)
							MANAGE_PED_ARRIVAL_RESULTS(iped, TRUE)
							
							MC_serverBD_2.iTargetID[iped] = -1
							PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN PED finished all gotos: ",iped)
							
							IF bWasOnDefensiveAreaGoto
								SET_BIT(MC_serverBD.iPedDoneDefensiveAreaGoto[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
							ENDIF
							
							PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN PED calling PROCESS_PED_TASK_NOTHING_BRAIN to see what state we should go to now: ",iped)
							PROCESS_PED_TASK_NOTHING_BRAIN(ThisPed, iped)
						ELSE
							IF bGoToIsLooped
								MC_serverBD_2.iPedGotoProgress[iped] = 0
								PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN PED ",iped," finished all gotos but set to loop (setting MC_serverBD_2.iPedGotoProgress[iped] back to 0)")
							ENDIF
							
							//Do we need to wait for the next goto? Set their state to nothing, and if they need to get back on their Go To
							// then the PROCESS_PED_TASK_NOTHING_BRAIN check will get them back onto the GoTo. Otherwise the function will
							// shunt them into any other task they might want to perform.
							SET_PED_STATE(iped,ciTASK_CHOOSE_NEW_TASK)
							PROCESS_PED_TASK_NOTHING_BRAIN(ThisPed,iped)
							
							IF MC_serverBD_2.iPedState[iped] != ciTASK_GOTO_COORDS
								
								PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN PED finished goto, need to wait before starting next stage: ",iped)
								
								IF bWasOnDefensiveAreaGoto
									SET_BIT(MC_serverBD.iPedDoneDefensiveAreaGoto[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
								ENDIF
								
							ELSE
								IF bWasOnDefensiveAreaGoto
								AND NOT SHOULD_PED_DO_DEFENSIVE_AREA_GOTO(ThisPed, iped) // Done with defensive area goto
									SET_BIT(MC_serverBD.iPedDoneDefensiveAreaGoto[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
								ENDIF
							ENDIF
							
						ENDIF
					ELSE
						IF NOT IS_BIT_SET(MC_serverBD.iPedAtSecondaryGotoLocBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
							SET_BIT(MC_serverBD.iPedAtSecondaryGotoLocBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
							MC_serverBD_2.iTargetID[iped] = -1
							PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN PED finished secondary goto: ",iped)
							
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetNine, ciPed_BSNine_CleanupOnSecondaryTaskCompleted)
								PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN | PROCESS_PED_FINISHED_GOTO_INTERRUPT -Cleaning up ped early because ciPed_BSNine_CleanupOnSecondaryTaskCompleted: ",iped)
								IF CLEANUP_PED_EARLY(iped, ThisPed)
									SET_PED_STATE(iped, ciTASK_CHOOSE_NEW_TASK)
									EXIT
								ELSE
									SCRIPT_ASSERT("PROCESS_PED_BRAIN | PROCESS_PED_FINISHED_GOTO_INTERRUPT - I was expecting CLEANUP_PED_EARLY to return TRUE, because ciPed_BSNine_CleanupOnSecondaryTaskCompleted")
									PROCESS_PED_TASK_NOTHING_BRAIN(ThisPed, iped)
								ENDIF
							ELSE
								PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN | PROCESS_PED_FINISHED_GOTO_INTERRUPT - PED calling PROCESS_PED_TASK_NOTHING_BRAIN to see what state we should go to now: ",iped)
								PROCESS_PED_TASK_NOTHING_BRAIN(ThisPed, iped)
							ENDIF
							
						ENDIF
					ENDIF
				ELSE
					// url:bugstar:2105607
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_AbortGoToAndFleeIfVehicleIsStuck)
					AND IS_PED_IN_ANY_VEHICLE(ThisPed)
						
						MODEL_NAMES mnVeh 
						mnVeh = GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(ThisPed))
						
						IF NOT (IS_THIS_MODEL_A_BOAT(mnVeh) OR IS_THIS_MODEL_A_PLANE(mnVeh) OR IS_THIS_MODEL_A_HELI(mnVeh))
						AND IS_CONTROLLED_VEHICLE_UNABLE_TO_GET_TO_ROAD(ThisPed)
							MC_ServerBD_2.iAssociatedAction[iped] = ciACTION_NOTHING 
							MC_serverBD.niTargetID[iped] = NULL 
							MC_serverBD_2.iTargetID[iped] = -1 
							SET_PED_STATE(iped,ciTASK_ABORT_GOTO_AND_FLEE)
							SET_BIT(MC_serverBD.iPedAtGotoLocBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
							PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN Vehicle unable to get to road, ped ", iped, " set to ciTASK_ABORT_GOTO_AND_FLEE")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
							
		BREAK
		
		CASE ciTASK_SHUFFLE_VEHICLE_SEAT_TO_TURRET			
			IF NOT IS_PED_INJURED(ThisPed)
				IF IS_PED_IN_ANY_VEHICLE(ThisPed)
					SCRIPTTASKSTATUS TaskStatus 
					TaskStatus = GET_SCRIPT_TASK_STATUS(ThisPed, SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT)
					
					IF (TaskStatus != WAITING_TO_START_TASK)
					AND (TaskStatus != PERFORMING_TASK)
						
					ELSE	
						SET_PED_RETASK_DIRTY_FLAG(iPed)
						SET_PED_STATE(iPed, ciTASK_CHOOSE_NEW_TASK)
						PRINTLN("[LM][RCC MISSION][ShuffleTurretSeatAlerted] PROCESS_PED_BRAIN - iped: ", iPed, " Finished task. Choose new Task.")
					ENDIF
				ELSE
					SET_PED_RETASK_DIRTY_FLAG(iPed)
					SET_PED_STATE(iPed, ciTASK_CHOOSE_NEW_TASK)
					PRINTLN("[LM][RCC MISSION][ShuffleTurretSeatAlerted] PROCESS_PED_BRAIN - iped: ", iPed, " Not in a vehicle. Choose new task.")
				ENDIF
			ENDIF
		BREAK
		
		CASE ciTASK_ATTACK_ENTITY
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedFixationTarget > -1
			AND NOT IS_BIT_SET(MC_serverBD_1.iPedFixationTaskEnded[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				SET_BIT(MC_serverBD_1.iPedFixationTaskStarted[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				
				MC_serverBD.niTargetID[iped] = MC_serverBD_1.sFMMC_SBD.niPed[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedFixationTarget]
				
				BOOL bFinished
				PED_INDEX piTarget
				
				PRINTLN("[LM][RCC MISSION][PED_FIXATE] - PROCESS_PED_BRAIN - iped: ", iPed, " Checking if ped should still be fixated... Range: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fPedFixationInterruptPlayerRange, " On Target: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedFixationTarget)
				
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD.niTargetID[iped])
					piTarget = NET_TO_PED(MC_serverBD.niTargetID[iped])
					IF IS_PED_INJURED(piTarget)
						PRINTLN("[LM][RCC MISSION][PED_FIXATE] - PROCESS_PED_BRAIN - iped: ", iPed, " Fixate Ped Dead Clearing Task. Choose new task.")
						bFinished = TRUE
					ENDIF
				ELSE
					PRINTLN("[LM][RCC MISSION][PED_FIXATE] - PROCESS_PED_BRAIN - iped: ", iPed, " Fixate Ped: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedFixationTarget, " No Longer Exists. Clearing Task. Choose new task.")
					bFinished = TRUE
				ENDIF
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fPedFixationInterruptPlayerRange > -1.0
					IF CHECK_PLAYERS_BEING_HOSTILE_IN_RANGE_OF_PED(iPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fPedFixationInterruptPlayerRange)
						PRINTLN("[LM][RCC MISSION][PED_FIXATE] - PROCESS_PED_BRAIN - iped: ", iPed, " Fixate Ped Player Interrupted Clearing Task. Choose new task.")
						bFinished = TRUE
					ENDIF
				ENDIF
				
				IF bFinished
					//CLEAR_BIT(MC_serverBD.iPedTaskActive[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					//CLEAR_BIT(MC_serverBD.iPedSecondaryTaskActive[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					MC_ServerBD_2.iAssociatedAction[iped] = ciACTION_NOTHING
					MC_serverBD.niTargetID[iped] = NULL
					MC_serverBD_2.iTargetID[iped] = -1
					SET_PED_STATE(iped,ciTASK_CHOOSE_NEW_TASK)
					SET_PED_RETASK_DIRTY_FLAG(iPed)
					SET_BIT(MC_serverBD_1.iPedFixationTaskEnded[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				ENDIF
			ENDIF
		BREAK
		
		CASE ciTASK_GOTO_ENTITY
			
			IF btargetOK
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedEscortRange = 0
				AND IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vPedHeliEscortOffset)
					IF IS_ENTITY_A_VEHICLE(tempTarget)
						
						tempveh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(tempTarget)
						
						IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwelve, ciPed_BSTwelve_DoNotTryToEnterGoToTargetVehicle)
							IF IS_PED_IN_VEHICLE(ThisPed,tempveh)
								
								MANAGE_PED_ARRIVAL_RESULTS(iped, FALSE)
								
								IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fRange = cfMAX_PATROL_RANGE
									SET_PED_STATE(iped,ciTASK_WANDER)
								ELSE
									IF 	NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_PreventCombatTaskingWhileDoingGoto)
									AND SHOULD_PED_START_COMBAT(ThisPed, iped)
										PRINTLN("[RCC MISSION][SpookAggro] PROCESS_PED_BRAIN Ped ", iped, " Cancelling ciTASK_GOTO_ENTITY Tasks.")
										SET_PED_STATE(iped,ciTASK_GENERAL_COMBAT #IF IS_DEBUG_BUILD ,5 #ENDIF )
									ENDIF
								ENDIF
							ELSE
								
								VEHICLE_SEAT vsSeat
								vsSeat = GET_VEHICLE_SEAT_PED_SHOULD_ENTER(tempVeh, iped)
								
								IF vsSeat = VS_ANY_PASSENGER
									bVehicleUsed = TRUE // No free seats
								ENDIF
								
								IF bVehicleUsed
									PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN Ped ",iped," trying to enter a vehicle, but the vehicle is full - return to choose new task state")
									MC_serverBD.niTargetID[iped] = NULL
									SET_PED_STATE(iped,ciTASK_CHOOSE_NEW_TASK)
								ENDIF
							ENDIF
						ELSE
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetSix, ciPED_BSSix_PedArrivalResultsTriggerOnTaskCompletion)
							AND NOT IS_BIT_SET(MC_serverBD.iPedTasksArrivalResultsBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
								VECTOR vTarget
								VEHICLE_INDEX vehTemp
								
								IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD.niTargetID[iped])
									IF IS_ENTITY_A_VEHICLE(NET_TO_ENT(MC_serverBD.niTargetID[iped]))
										vehTemp =  NET_TO_VEH(MC_serverBD.niTargetID[iped])
									ENDIF
								ENDIF
								
								IF IS_VEHICLE_DRIVEABLE(vehTemp)
									vTarget = GET_ENTITY_COORDS(vehTemp)
								ENDIF
																
								IF NOT IS_VECTOR_ZERO(vTarget)
									FLOAT fTargetDist
									fTargetDist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(ThisPed, vTarget)
									FLOAT fGotoRange
									fGotoRange = 10
									
									IF IS_PED_IN_ANY_VEHICLE(ThisPed)
										IF IS_PED_IN_ANY_PLANE(ThisPed)
											fGotoRange = 130
										ELIF IS_PED_IN_ANY_HELI(ThisPed)
											fGotoRange = 60
										ENDIF
									ENDIF
									
									IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedEscortRange != 0
										fGotoRange = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedEscortRange)
									ENDIF
									
									IF fTargetDist <= FMAX(fGotoRange, 15.0)
										MANAGE_PED_ARRIVAL_RESULTS(iped, FALSE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELIF IS_ENTITY_AN_OBJECT(tempTarget)
						IF IS_ENTITY_ATTACHED_TO_ENTITY(tempTarget,ThisPed)
							
							MANAGE_PED_ARRIVAL_RESULTS(iped, FALSE)
							
							IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fRange = cfMAX_PATROL_RANGE
								SET_PED_STATE(iped,ciTASK_WANDER)
							ENDIF
						ENDIF
					ELSE
						//Peds and locations:
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetSix, ciPED_BSSix_PedArrivalResultsTriggerOnTaskCompletion)
						AND NOT IS_BIT_SET(MC_serverBD.iPedTasksArrivalResultsBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
							VECTOR vTarget
							
							IF IS_ENTITY_A_PED(tempTarget)
								vTarget = GET_ENTITY_COORDS(tempTarget)
							ELIF MC_serverBD_2.iTargetID[iped] != -1
								vTarget = GET_LOCATION_VECTOR(MC_serverBD_2.iTargetID[iped])
							ENDIF
							
							IF NOT IS_VECTOR_ZERO(vTarget)
								FLOAT fTargetDist
								fTargetDist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(ThisPed, vTarget)
								FLOAT fGotoRange
								fGotoRange = 10
								
								IF IS_PED_IN_ANY_VEHICLE(ThisPed)
									IF IS_PED_IN_ANY_PLANE(ThisPed)
										fGotoRange = 130
									ELIF IS_PED_IN_ANY_HELI(ThisPed)
										fGotoRange = 60
									ENDIF
								ENDIF
								
								IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedEscortRange != 0
									fGotoRange = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedEscortRange)
								ENDIF
								
								IF fTargetDist <= FMAX(fGotoRange, 15.0)
									MANAGE_PED_ARRIVAL_RESULTS(iped, FALSE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				SET_PED_STATE(iped,ciTASK_CHOOSE_NEW_TASK)
			ENDIF
							
		BREAK
		
		CASE ciTASK_GOTO_PLAYER
		CASE ciTASK_DEFEND_PLAYER
		CASE ciTASK_ATTACK_PLAYER
			
			IF IS_NET_PLAYER_OK(MC_serverBD.TargetPlayerID[iped])
				IF NOT NETWORK_IS_PLAYER_A_PARTICIPANT(MC_serverBD.TargetPlayerID[iped])
					MC_serverBD.TargetPlayerID[iped] = NULL
					SET_PED_STATE(iped,ciTASK_CHOOSE_NEW_TASK)
				ELIF MC_serverBD_2.iPedState[iped] = ciTASK_GOTO_PLAYER
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetSix, ciPED_BSSix_PedArrivalResultsTriggerOnTaskCompletion)
					AND NOT IS_BIT_SET(MC_serverBD.iPedTasksArrivalResultsBS[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
						
						PED_INDEX playerPed
						playerPed = GET_PLAYER_PED(MC_serverBD.TargetPlayerID[iped])
						
						IF NOT IS_PED_IN_ANY_VEHICLE(playerPed)
							
							FLOAT fGotoRange
							fGotoRange = ciPED_ARRIVE_GOTO_RANGE
							
							IF IS_PED_IN_ANY_VEHICLE(ThisPed)
								IF IS_PED_IN_ANY_PLANE(ThisPed)
									fGotoRange = ciPED_PLANE_ARRIVE_GOTO_RANGE
								ELIF (IS_PED_IN_ANY_BOAT(ThisPed) OR IS_PED_IN_ANY_HELI(ThisPed))
									fGotoRange = ciPED_BOAT_HELI_ARRIVE_GOTO_RANGE
								ENDIF
							ENDIF
							
							FLOAT fTargetDist
							fTargetDist = GET_DISTANCE_BETWEEN_ENTITIES(ThisPed, playerPed)
							
							IF fTargetDist <= fGotoRange
								bArrived = TRUE
							ENDIF
						ELSE
							IF IS_PED_IN_VEHICLE(ThisPed, GET_VEHICLE_PED_IS_IN(playerPed))
								bArrived = TRUE
							ENDIF
						ENDIF
						
						IF bArrived
							MANAGE_PED_ARRIVAL_RESULTS(iped, FALSE)
						ENDIF
						
					ENDIF
				ENDIF
			ELSE
				MC_serverBD.TargetPlayerID[iped] = NULL
				SET_PED_STATE(iped,ciTASK_CHOOSE_NEW_TASK)
			ENDIF
			
		BREAK
		
		CASE ciTASK_GENERAL_COMBAT
			IF SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK(iped, TRUE)
				PRINTLN("[RCC MISSION] PROCESS_PED_BRAIN Ped ",iped," Cancelling Combat Tasks for Animation.")
				CLEAR_BIT(MC_serverBD.iPedSecondaryTaskActive[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				MC_ServerBD_2.iAssociatedAction[iped] = ciACTION_NOTHING
				MC_serverBD.niTargetID[iped] = NULL
				MC_serverBD_2.iTargetID[iped] = -1
				IF IS_PED_IN_COMBAT(ThisPed)
				AND CAN_PED_TASKS_BE_CLEARED(ThisPed)
					CLEAR_PED_TASKS(ThisPed)
				ENDIF
				SET_PED_STATE(iped,ciTASK_CHOOSE_NEW_TASK)
			ENDIF
		BREAK
		
		CASE ciTASK_DEFEND_AREA	

			IF IS_PED_IN_ANY_VEHICLE(ThisPed)
				SET_PED_STATE(iped,ciTASK_CHOOSE_NEW_TASK)
			ELSE
				IF IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				OR SHOULD_PED_START_COMBAT(ThisPed, iped)
				OR HAS_PED_BEEN_SPOOKED(iped)
					PRINTLN("[RCC MISSION][SpookAggro] PROCESS_PED_BRAIN Ped ", iped, " Cancelling ciTASK_DEFEND_AREA Tasks.")
					SET_PED_STATE(iped,ciTASK_GENERAL_COMBAT #IF IS_DEBUG_BUILD , 6 #ENDIF )
				ENDIF
			ENDIF

		BREAK
		
		CASE ciTASK_DEFEND_AREA_IN_VEHICLE
			IF NOT IS_PED_IN_ANY_VEHICLE(ThisPed)
				SET_PED_STATE(iped, ciTASK_CHOOSE_NEW_TASK)
			ENDIF
		BREAK
		
		CASE ciTASK_ABORT_GOTO_AND_FLEE
			
			IF NOT IS_PED_IN_ANY_VEHICLE(ThisPed)
				SET_PED_STATE(iped,ciTASK_FLEE)
			ENDIF
			
		BREAK
		
		CASE ciTASK_WANDER
		
			IF NOT IS_PED_IN_ANY_VEHICLE(ThisPed)
				IF IS_BIT_SET(MC_serverBD.iPedSpookedBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					PRINTLN("[RCC MISSION][SpookAggro] PROCESS_PED_BRAIN Ped ", iped, " Cancelling ciTASK_WANDER Tasks.")
					SET_PED_STATE(iped,ciTASK_CHOOSE_NEW_TASK)
				ENDIF
			ENDIF
			
		BREAK
		
	ENDSWITCH
		
	// Retroactive Fix, in the future content need to set up the vehicles to be stationary until the peds have attached.
	// WE will need to provide something that links the vehicles and peds, so we know to freeze the vehicle and not to unfreeze it until the linked entities are attached.
	// Right now we'd have to loop through all peds here to determine whether we need to unfreeze.
	IF IS_THIS_ROCKSTAR_MISSION_WVM_DUNE3(g_FMMC_STRUCT.iRootContentIDHash)
		PROCESS_SPECIAL_FREEZE_VEHICLE_FOR_ATTACHED_PEDS_FIX_4384208(iPed)	
	ENDIF	
ENDPROC

