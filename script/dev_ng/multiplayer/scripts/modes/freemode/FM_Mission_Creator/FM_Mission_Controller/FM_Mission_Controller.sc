
// ___________________________________________
//
//	Game: GTAV - MISSION CREATOR.
//  Script: FM_Mission_controller.sc
//  Author: Rowan Cockcroft: 11/7/2012
//	Description: mission creator controller	
// ___________________________________________


USING "FM_Mission_Controller_USING.sch"
USING "FM_Mission_Controller_Utility.sch"
USING "FM_Mission_Controller_Entities.sch"
USING "FM_Mission_Controller_ObjectiveCompletion.sch"
USING "FM_Mission_Controller_SpecialCase.sch"
USING "FM_Mission_Controller_Rules.sch"
USING "FM_Mission_Controller_Doors.sch"
USING "FM_Mission_Controller_Cutscene.sch"
USING "FM_Mission_Controller_Debug.sch"
USING "FM_Mission_Controller_Locations.sch"
USING "FM_Mission_Controller_Vehicles.sch"
USING "FM_Mission_Controller_Peds_Brain.sch"
USING "FM_Mission_Controller_Peds_Body.sch"
USING "FM_Mission_Controller_Peds.sch"
USING "FM_Mission_Controller_GangChase.sch"
USING "FM_Mission_Controller_Minigame.sch"
USING "FM_Mission_Controller_Objects.sch"
USING "FM_Mission_Controller_Props.sch"
USING "FM_Mission_Controller_Audio.sch"
USING "FM_Mission_Controller_HUD.sch"
USING "FM_Mission_Controller_Players.sch"
USING "FM_Mission_Controller_Zones.sch"
USING "FM_Mission_Controller_Bounds.sch"
USING "FM_Mission_Controller_Events.sch"
USING "FM_Mission_Controller_Cutscene_2.sch"
USING "FM_Mission_Controller_Main.sch"
USING "FM_Mission_Controller_Ending.sch"
USING "FM_Mission_Controller_Ending_2.sch"
USING "FM_Mission_Controller_Init.sch"
USING "FM_Mission_Controller_Tutorial.sch"
USING "select_mission_stage.sch"

/// PURPOSE:
///    This function does some server-side processing, which includes
///    * creating networked objects
///    * deciding when each participant has started, is running, and has finished, a particular rule or precondition
///    * progressing the mission when appropriate
PROC PROCESS_SERVER()

	INT iTeam
	INT iParticipant
	PARTICIPANT_INDEX tempPart
	
	BOOL bEveryoneOnLB
	
	SWITCH GET_MC_SERVER_GAME_STATE()
					
		CASE GAME_STATE_INI
			
			IF IS_MISSION_USING_ANY_MISSION_VARIATION()
				IF NOT IS_CORONA_READY_TO_START_WITH_JOB()
					PRINTLN("[LM][MissionVariation][PROCESS_SERVER] - Corona Not Ready to start the job - Waiting before initializing mission variations vars.")
					EXIT
				ELIF NOT HAVE_ALL_CORONA_PLAYERS_JOINED(tdSafeJoinVariations)
					PRINTLN("[LM][MissionVariation][PROCESS_SERVER] - Waiting for all 'Corona Players' to have joined.")
					EXIT
				ELIF NOT HAS_MISSION_VARIATIONS_SERVER_DATA_BEEN_SET_YET()
					PRINTLN("[LM][MissionVariation][PROCESS_SERVER] - Initializing Mission Variations Vars.")
					PROCESS_DEFINE_MISSION_VARIATION_SETTINGS_SERVER_DATA()
				ELSE
					PRINTLN("[LM][MissionVariation][PROCESS_SERVER] - Mission Variation Vars already Initialized!")
				ENDIF
			ENDIF
			
			IF MC_serverBD.iTotalNumStartingPlayers = 0
			AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
			AND NOT IS_BIT_SET(MC_serverBD.iServerBitset2, SBBOOL2_SERVER_INITIALISATION_TIMED_OUT)
				IF IS_CORONA_READY_TO_START_WITH_JOB()
				OR IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
				OR SHOULD_LOCAL_PLAYER_LAUNCH_TUTORIAL_MISSION_FROM_SKYSWOOP()
					IF NOT HAS_NET_TIMER_STARTED(MC_serverBD.tdInitialisationTimeout)
						PRINTLN("[RCC MISSION][PROCESS_SERVER] Starting MC_serverBD.tdInitialisationTimeout timer")
						REINIT_NET_TIMER(MC_serverBD.tdInitialisationTimeout)
					ELIF (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdInitialisationTimeout) >= CORONA_FAILSAFE_TIMEOUT)
						PRINTLN("[RCC MISSION][PROCESS_SERVER] MC_serverBD.tdInitialisationTimeout has passed 30 seconds!")
						RESET_NET_TIMER(MC_serverBD.tdInitialisationTimeout)
						SET_BIT(MC_serverBD.iServerBitset2, SBBOOL2_SERVER_INITIALISATION_TIMED_OUT)
					ENDIF

					HOST_SET_ARENA_AUDIO_SCORE()
					
					IF g_TransitionSessionNonResetVars.iHeistPresistantPropertyID = - 1
						IF GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].propertyDetails.iCurrentlyInsideProperty > 0
						AND GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].propertyDetails.iCurrentlyInsideProperty < MAX_MP_PROPERTIES
							MC_ServerBD.iHeistPresistantPropertyID = GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].propertyDetails.iCurrentlyInsideProperty
						ELSE
							MC_ServerBD.iHeistPresistantPropertyID = g_TransitionSessionNonResetVars.iLastPropertyIndex
						ENDIF
						PRINTLN("[RCC MISSION][PROCESS_SERVER] MC_ServerBD.iHeistPresistantPropertyID = ", MC_ServerBD.iHeistPresistantPropertyID)
					ELSE
						MC_ServerBD.iHeistPresistantPropertyID = g_TransitionSessionNonResetVars.iHeistPresistantPropertyID
						PRINTLN("[RCC MISSION][PROCESS_SERVER] MC_ServerBD.iHeistPresistantPropertyID = g_TransitionSessionNonResetVars.iHeistPresistantPropertyID = ", MC_ServerBD.iHeistPresistantPropertyID)
					ENDIF
					
					FOR iteam = 0 TO (FMMC_MAX_TEAMS-1)
						MC_serverBD.iNumStartingPlayers[iteam] = GET_NUMBER_PLAYERS_JOINING_MISSION(iteam)
						PRINTLN("[RCC MISSION][PROCESS_SERVER] team = ",iteam," MC_serverBD.iNumStartingPlayers = ",MC_serverBD.iNumStartingPlayers[iteam])
					ENDFOR
					
					MC_serverBD.iTotalNumStartingPlayers = MC_serverBD.iNumStartingPlayers[0] + MC_serverBD.iNumStartingPlayers[1] + MC_serverBD.iNumStartingPlayers[2] + MC_serverBD.iNumStartingPlayers[3] 
					PRINTLN("[RCC MISSION][PROCESS_SERVER] MC_serverBD.iTotalNumStartingPlayers = ",MC_serverBD.iTotalNumStartingPlayers) 
					
					IF IS_A_STRAND_MISSION_BEING_INITIALISED()
						PRINTLN("[RCC MISSION][PROCESS_SERVER] RUN_ALL_STAGGERED_MC_HOST_FUNCTIONS being called") 
						RUN_ALL_STAGGERED_MC_HOST_FUNCTIONS( iCurrentRequestStaggerForStrandMission, iRelGroupStaggerVars, iNumTimesCalledRelGroupFunc )
					ENDIF
				ENDIF
			ELSE
				
				#IF IS_DEBUG_BUILD
				IF IS_BIT_SET(MC_serverBD.iServerBitset2, SBBOOL2_SERVER_INITIALISATION_TIMED_OUT)
					PRINTLN("[RCC MISSION][PROCESS_SERVER] MC_serverBD.iTotalNumStartingPlayers = ",MC_serverBD.iTotalNumStartingPlayers," but MC_serverBD.tdInitialisationTimeout has passed 30 seconds, move on")
				ENDIF
				#ENDIF
				
				IF NETWORK_IS_IN_MP_CUTSCENE()
					//Any entities created during a cutscene should be networked
					SET_NETWORK_CUTSCENE_ENTITIES(TRUE)
				ENDIF
				
				IF NOT IS_BIT_SET(MC_serverBD.iServerBitset8, SBBOOL8_APPLIED_SCRIPT_CLOUD_CONTENT_FIXES)
				AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
					SET_BIT(MC_serverBD.iServerBitset8, SBBOOL8_APPLIED_SCRIPT_CLOUD_CONTENT_FIXES)
					SET_SCRIPT_CLOUD_CONTENT_FIXES()
				ENDIF
				
				IF IS_MISSION_USING_ANY_MISSION_VARIATION()
				AND NOT IS_BIT_SET(MC_serverBD.iServerBitset8, SBBOOL8_HOST_MISSION_VARIATION_DATA_APPLIED)
					PRINTLN("[RCC MISSION][PROCESS_SERVER] Calling PROCESS_APPLY_MISSION_VARIATION_SETTINGS - I AM THE ONE WHO HOSTS.")
					PROCESS_APPLY_MISSION_VARIATION_SETTINGS()
					SET_BIT(MC_serverBD.iServerBitset8, SBBOOL8_HOST_MISSION_VARIATION_DATA_APPLIED)
				ENDIF
				
				IF NOT IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_RSGDATA_INITIALISED)
					PRINTLN("[RCC MISSION][PROCESS_SERVER] Calling SEED_SPAWN_GROUPS - Setting SBBOOL3_RSGDATA_INITIALISED.")
					SET_BIT(MC_serverBD.iServerBitset3, SBBOOL3_RSGDATA_INITIALISED)
					SEED_SPAWN_GROUPS()					
					INITIALIZE_SPAWN_GROUP_DEPENDENT_SERVER_DATA()
					PRINTLN("[RCC MISSION][PROCESS_SERVER] MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS: ", MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS)
				ENDIF
				
				IF NOT IS_BIT_SET(iLocalBoolCheck8, LBOOL8_REQUEST_LOAD_FMMC_MODELS_STAGGERED_DONE)
				AND IS_A_STRAND_MISSION_BEING_INITIALISED()
					PRINTLN("[RCC MISSION][PROCESS_SERVER] REQUEST_LOAD_FMMC_MODELS_STAGGERED being called") 
					IF RUN_ALL_STAGGERED_MC_HOST_FUNCTIONS( iCurrentRequestStaggerForStrandMission, iRelGroupStaggerVars, iNumTimesCalledRelGroupFunc )
						CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][PROCESS_SERVER] RUN_ALL_STAGGERED_MC_HOST_FUNCTIONS RETURNING TRUE") 
						SET_BIT(iLocalBoolCheck8, LBOOL8_REQUEST_LOAD_FMMC_MODELS_STAGGERED_DONE)
					ENDIF
					
					PRINTLN("[RCC MISSION][PROCESS_SERVER] NOT LBOOL8_REQUEST_LOAD_FMMC_MODELS_STAGGERED_DONE AND IS_A_STRAND_MISSION_BEING_INITIALISED") 
				ELSE
					IF IS_BIT_SET(iLocalBoolCheck8, LBOOL8_INITIAL_IPL_SET_UP_COMPLETE)
					AND IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_RSGDATA_INITIALISED)
						IF HAVE_ALL_PLAYERS_PROCESSED_PRE_GAME()
							IF HAVE_INTERIORS_LOADED()
								#IF IS_DEBUG_BUILD
									IF NOT HAS_NET_TIMER_STARTED(tdLoadAndCreateTimer)
										REINIT_NET_TIMER(tdLoadAndCreateTimer)
									ENDIF
								#ENDIF
								IF LOAD_AND_CREATE_ALL_FMMC_ENTITIES_RC( MC_serverBD_1.sFMMC_SBD,
																		MC_serverBD_1.sCreatedCount,
																		MC_serverBD.iNumStartingPlayers, 
																		MC_serverBD_1.niMinigameCrateDoors,
																		MC_serverBD.iIsHackContainer,
																		MC_ServerBD.iHeistPresistantPropertyID,
																		MC_serverBD_4.rsgSpawnSeed,
																		MC_serverBD_1.niDZSpawnVehicle,
																		MC_serverBD_1.niDZSpawnVehPed,
																		MC_ServerBD_4.niBJXL,
																		MC_ServerBD.iServerBitSet5,
																		MC_ServerBD.iVehicleNotSpawningDueToRandomBS,
																		MC_ServerBD_4.iFacilityPosVehSpawned,
																		MC_serverBD_1.sLEGACYMissionContinuityVars)
									
									SET_BIT(MC_ServerBD.iServerBitset7, SBBOOL7_LOAD_AND_CREATE_ALL_FMMC_ENTITIES_RC)
									
									START_PLACED_PEDS_IN_COVER()
									
									// Missions where we are placed in placed vehicles at the start need this to be called earlier.
									IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUNNING_BACK_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
										SET_ALL_LEGACY_MISSION_ENTITIES_UNFROZEN_AND_ABLE_TO_MIGRATE(MC_serverBD_1.sFMMC_SBD, iPedStartedInCoverBitset)
									ENDIF
									
										#IF IS_DEBUG_BUILD
											IF HAS_NET_TIMER_STARTED(tdLoadAndCreateTimer)
											AND NOT bPrintedLoadTime
												PRINTLN("[RCC MISSION][PROCESS_SERVER] LOAD_AND_CREATE_ALL_FMMC_ENTITIES_RC took ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdLoadAndCreateTimer), "ms to complete")
												RESET_NET_TIMER(tdLoadAndCreateTimer)
												bPrintedLoadTime = TRUE
											ENDIF
										#ENDIF
									IF CREATE_APARTMENTWARP_DOOR()
										IF IS_CORONA_READY_TO_START_WITH_JOB()
										OR IS_FAKE_MULTIPLAYER_MODE_SET()
										OR NOT IS_THIS_AN_ADVERSARY_MODE_MISSION(g_FMMC_STRUCT.iRootContentIDHash, g_FMMC_STRUCT.iAdversaryModeType)
											IF INIT_SERVER_DATA()
												SET_FMMC_MODELS_AS_NOT_NEEDED()
												REINIT_NET_TIMER(MC_serverBD.tdJoinTimer)
												/*IF MC_serverBD.fPedDensity = 0.0 
												AND MC_serverBD.fVehicleDensity = 0.0
													SERVER_BLOCK_ALL_SCENARIOS(MC_serverBD_3.scenBlockServer) 
												ENDIF*/
												IF IS_A_STRAND_MISSION_BEING_INITIALISED()
													SET_ALL_MISSION_ENTITIES_VISIBLITY(MC_serverBD_1.sFMMC_SBD, oiProps, oiPropsChildren, TRUE)
												ENDIF
												SET_SERVER_GAME_STATE(GAME_STATE_RUNNING)
											ELSE
												PRINTLN("[RCC MISSION][PROCESS_SERVER] INIT_SERVER_DATA - FALSE") 
											ENDIF
										ELSE
											PRINTLN("[RCC MISSION][PROCESS_SERVER] IS_CORONA_READY_TO_START_WITH_JOB - FALSE")
										ENDIF
									ELSE
										PRINTLN("[RCC MISSION][PROCESS_SERVER] CREATE_APARTMENTWARP_DOOR - FALSE")
									ENDIF
								ELSE
									PRINTLN("[RCC MISSION][PROCESS_SERVER] LOAD_AND_CREATE_ALL_FMMC_ENTITIES_RC - FALSE") 
								ENDIF
							ELSE
								PRINTLN("[RCC MISSION][PROCESS_SERVER] HAVE_INTERIORS_LOADED - FALSE || Clearing LBOOL8_INITIAL_IPL_SET_UP_COMPLETE to try again") 
								CLEAR_BIT(iLocalBoolCheck8, LBOOL8_INITIAL_IPL_SET_UP_COMPLETE)
							ENDIF
						ELSE
							PRINTLN("[RCC MISSION][PROCESS_SERVER] HAVE_ALL_PLAYER_PREGAMED - FALSE") 
						ENDIF
					ELSE
						IF NOT IS_BIT_SET(iLocalBoolCheck8, LBOOL8_INITIAL_IPL_SET_UP_COMPLETE)
							PRINTLN("[RCC MISSION][PROCESS_SERVER] LBOOL8_INITIAL_IPL_SET_UP_COMPLETE is FALSE") 
						ENDIF
						IF NOT IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_RSGDATA_INITIALISED)
							PRINTLN("[RCC MISSION][PROCESS_SERVER] SBBOOL3_RSGDATA_INITIALISED is FALSE") 
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE GAME_STATE_RUNNING		
			
			IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_ALL_JOINED_START)
				IF NOT IS_BIT_SET(MC_serverbd.iServerBitSet, SSBOOL_ALL_READY_INTRO_CUT)
					IF SERVER_CHECK_EVERYONE_REACHED_INTRO_CUTSCENE()
						SERVER_CALCULATE_CENTRE_POINT_FOR_TEAMS(MC_serverBD,MC_playerBD)
						SET_BIT(MC_serverbd.iServerBitSet, SSBOOL_ALL_READY_INTRO_CUT)
						SET_ALL_LEGACY_MISSION_ENTITIES_UNFROZEN_AND_ABLE_TO_MIGRATE(MC_serverBD_1.sFMMC_SBD, iPedStartedInCoverBitset)
						SET_BIT(iLocalBoolCheck8, LBOOL8_ENTITIES_UNFROZEN_AT_START_OF_MISSION)
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciPOINTLESS_MODE_ENABLED)
				AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_POINTLESS_OBJECTS_LOADED)
					IF CREATE_POINTLESS_PORTABLE_PICKUPS()
						PRINTLN("[RCC MISSION][PROCESS_SERVER] - CREATE_POINTLESS_PORTABLE_PICKUPS() = true.")
						SET_BIT(MC_serverBD.iServerBitSet, SBBOOL_POINTLESS_OBJECTS_LOADED)
					ELSE
						PRINTLN("[RCC MISSION][PROCESS_SERVER] - CREATE_POINTLESS_PORTABLE_PICKUPS() = false.")
					ENDIF
				ENDIF
				
			ENDIF
			
			IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_ALL_JOINED_START)
				IF HAVE_ALL_CORONA_PLAYERS_JOINED(tdsafejointimer)
				OR IS_FAKE_MULTIPLAYER_MODE_SET()
				OR IS_BIT_SET(MC_serverBD.iServerBitset2, SBBOOL2_SERVER_INITIALISATION_TIMED_OUT)
					
					#IF IS_DEBUG_BUILD
					IF NOT IS_BIT_SET(MC_serverBD.iServerBitset2, SBBOOL2_SERVER_INITIALISATION_TIMED_OUT)
						PRINTLN("[RCC MISSION][PROCESS_SERVER] HAVE_ALL_CORONA_PLAYERS_JOINED returning TRUE")
					ELSE
						PRINTLN("[RCC MISSION][PROCESS_SERVER] Check HAVE_ALL_CORONA_PLAYERS_JOINED, but server timed out while initialising...")
					ENDIF
					#ENDIF
					
					FOR iteam = 0 TO (FMMC_MAX_TEAMS-1)
						MC_serverBD.iNumStartingPlayers[iteam] = GET_NUMBER_PLAYERS_JOINING_MISSION(iteam)
						PRINTLN("[RCC MISSION][PROCESS_SERVER] team = ",iteam," MC_serverBD.iNumStartingPlayers second grab = ",MC_serverBD.iNumStartingPlayers[iteam])
					ENDFOR
					
					MC_serverBD.iTotalNumStartingPlayers = MC_serverBD.iNumStartingPlayers[0] + MC_serverBD.iNumStartingPlayers[1] + MC_serverBD.iNumStartingPlayers[2] + MC_serverBD.iNumStartingPlayers[3] 
					PRINTLN("[RCC MISSION][PROCESS_SERVER] MC_serverBD.iTotalNumStartingPlayers second grab  = ",MC_serverBD.iTotalNumStartingPlayers) 
					
					SET_BIT(MC_serverBD.iServerBitSet, SBBOOL_ALL_JOINED_START)
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_EVERYONE_RUNNING)
					
					SERVER_INIT_TAG_TEAM_TURN_ROTOR()
										
					// Look for server eFnd conditions
					IF IS_EVERYONE_RUNNING(TRUE)
						PRINTLN("[RCC MISSION][PROCESS_SERVER] IS_EVERYONE_RUNNING RETURNING TRUE")
						
						START_MISSION_TIMERS()
						
						CLEAR_BIT(MC_serverBD.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
						CLEAR_BIT(MC_serverBD.iServerBitSet2, SBBOOL2_FIRST_UPDATE_STARTED) 
						SET_BIT(MC_serverBD.iServerBitSet, SBBOOL_EVERYONE_RUNNING)
						SET_BIT(MC_serverbd.iServerBitSet, SSBOOL_ALL_READY_INTRO_CUT)
						
						IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
							IF MC_ServerBD_4.piGangBossID = INVALID_PLAYER_INDEX()
								MC_ServerBD_4.piGangBossID = GB_GET_LOCAL_PLAYER_GANG_BOSS()
								PRINTLN("[RCC MISSION][PROCESS_SERVER] - Everyone running - Setting Gang Boss ID as: ", NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
							ENDIF
						ENDIF
						
						FOR iteam = 0 TO (FMMC_MAX_TEAMS-1)
							MC_serverBD.iNumStartingPlayers[iteam] = GET_STARTING_PLAYERS_FOR_TEAM(iteam)
							PRINTLN("[RCC MISSION][PROCESS_SERVER] team = ",iteam," MC_serverBD.iNumStartingPlayers third grab = ",MC_serverBD.iNumStartingPlayers[iteam])
						ENDFOR
						
						MC_serverBD.iTotalNumStartingPlayers = MC_serverBD.iNumStartingPlayers[0] + MC_serverBD.iNumStartingPlayers[1] + MC_serverBD.iNumStartingPlayers[2] + MC_serverBD.iNumStartingPlayers[3] 
						PRINTLN("[RCC MISSION][PROCESS_SERVER] MC_serverBD.iTotalNumStartingPlayers third grab  = ",MC_serverBD.iTotalNumStartingPlayers) 
						IF HAS_FIRST_STRAND_MISSION_BEEN_PASSED() OR HAS_FIRST_STRAND_MISSION_BEEN_PASSED_NO_RESET()
							IF g_TransitionSessionNonResetVars.iStrandTotalStartingPlayers > 0
								MC_serverBD.iTotalNumStartingPlayers = g_TransitionSessionNonResetVars.iStrandTotalStartingPlayers
								PRINTLN("[RCC MISSION][PROCESS_SERVER] Grabbing old starting players from previous strand part  = ",MC_serverBD.iTotalNumStartingPlayers) 
								FOR iteam = 0 TO (FMMC_MAX_TEAMS-1)
									MC_serverBD.iNumStartingPlayers[iteam] = g_TransitionSessionNonResetVars.iStrandStartingPlayers[iteam]
									PRINTLN("[RCC MISSION][PROCESS_SERVER] team = ",iteam," MC_serverBD.iNumStartingPlayers third grab from previous strand part = ",MC_serverBD.iNumStartingPlayers[iteam])
								ENDFOR
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)				
						
						// Look for server end conditions
						IF HAVE_MC_END_CONDITIONS_BEEN_MET()
						#IF IS_DEBUG_BUILD
						OR g_debugDisplayState = DEBUG_DISP_REPLAY
						#ENDIF
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciTRADING_PLACES_TIME_BAR_ENABLED) AND g_FMMC_STRUCT.iTradingPlacesTimeBarDuration != -1
								#IF IS_DEBUG_BUILD
								PRINTLN("[PROCESS_SERVER][BROADCAST_FMMC_TRADING_PLACES_TEAM_SWAP]")
								FOR iParticipant = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
									tempPart = INT_TO_PARTICIPANTINDEX(iParticipant)
									
									IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
										IF MC_playerBD[iParticipant].iTeam = 0 //Losers
											PRINTLN("PLAYER=", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(tempPart)), "   TEAM=", MC_playerBD[iParticipant].iTeam, "   SCORE=", CLAMP_INT(MC_playerBD[iParticipant].iTradingPlacesTimeBarCurrent - GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MC_playerBD[iParticipant].iTradingPlacesTimeBarTimer.Timer), 0, (g_FMMC_STRUCT.iTradingPlacesTimeBarDuration * 1000)))
										ELSE
											PRINTLN("PLAYER=", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(tempPart)), "   TEAM=", MC_playerBD[iParticipant].iTeam, "   SCORE=", CLAMP_INT(MC_playerBD[iParticipant].iTradingPlacesTimeBarCurrent + GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MC_playerBD[iParticipant].iTradingPlacesTimeBarTimer.Timer), 0, (g_FMMC_STRUCT.iTradingPlacesTimeBarDuration * 1000)))
										ENDIF
									ENDIF
								ENDFOR
								#ENDIF
								PROCESS_TRADING_PLACES_TIME_BAR_HUD(TRUE) 
								INT i
								#IF IS_DEBUG_BUILD
								PRINTLN("iWinningPlayerHUD = ", iWinningPlayerHUD)
								REPEAT COUNT_OF(sTimeBarSorted) i
									IF sTimeBarSorted[i].playerIndex != INVALID_PLAYER_INDEX()
										PRINTLN("[PROCESS_SERVER][sTimeBarSorted] PLAYER=", GET_PLAYER_NAME(sTimeBarSorted[i].playerIndex), "   SCORE=", sTimeBarSorted[i].iTimeBarCurrent)
									ELSE
										PRINTLN("[PROCESS_SERVER][sTimeBarSorted] INVALID PLAYER INDEX i = ", i)
									ENDIF
								ENDREPEAT
								#ENDIF
								REPEAT iWinningPlayerHUD i
									IF sTimeBarSorted[i].playerIndex != INVALID_PLAYER_INDEX()
										MC_serverBD_1.piTimeBarWinners[i] = sTimeBarSorted[i].playerIndex
									#IF IS_DEBUG_BUILD
										PRINTLN("[PROCESS_SERVER]WINNING PLAYER=", GET_PLAYER_NAME(sTimeBarSorted[i].playerIndex))
									ELSE
										PRINTLN("[PROCESS_SERVER]WINNING PLAYER=INVALID PLAYER INDEX i = ", i)
									#ENDIF
									ENDIF
								ENDREPEAT
							ENDIF
							
							IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
								PRINTLN("[LM][OVERTIME][RENDERPHASE_PAUSE_EVENT][PROCESS_SERVER] - MISSION OVER - Calling to pause renderphases.")
								BROADCAST_FMMC_OVERTIME_PAUSE_RENDERPHASE(MC_ServerBD.iSessionScriptEventKey, FALSE, TRUE)
							ENDIF
													
							RUN_FINAL_DESTROYED_VEHICLE_CHECK()
							RUN_FINAL_AGGRO_PED_CHECK()
							RUN_FINAL_CRITICAL_PLAYER_NUMBERS_CHECK()
							
							PRINTLN("[RCC MISSION][PROCESS_SERVER] !!!!MISSION OVER!!!!")
							
							END_MISSION_TIMERS()
							
							SET_BIT(MC_serverBD.iServerBitSet,SBBOOL_MISSION_OVER)
							
							IF NOT SHOULD_SPAWN_OUTSIDE_APARTMENT_FOR_QUICK_RESTART()
								PRINTLN("[MMacK][Clean][PROCESS_SERVER] YES")
								CLEAN_APARTMENT_INFO_FOR_WALKOUT()
							ELSE
								PRINTLN("[MMacK][Clean][PROCESS_SERVER] NO")
							ENDIF
							
						ENDIF
						
						#IF IS_DEBUG_BUILD
							IF iPlayerPressedF != -1
							OR iPlayerPressedS != -1
								END_MISSION_TIMERS()
								PRINTLN("[RCC MISSION][PROCESS_SERVER] MISSION OVER SETTING TIME TO: ",MC_serverBD.iTotalMissionEndTime )
								SET_BIT(MC_serverBD.iServerBitSet,SBBOOL_MISSION_OVER)
								#IF IS_DEBUG_BUILD
								IF iPlayerPressedF != -1
								AND bAutoForceRestartNoCorona
									PRINTLN("[PROCESS_SERVER] FMMC EOM - sc_AutoForceRestartNoCorona - FMMC_EOM_SERVER_END_OF_FRAME_PROCESSING - VOTE PASSED FOR QUICK RESTART")
									MC_serverBD.sServerFMMC_EOM.bVotePassed = TRUE
									MC_serverBD.sServerFMMC_EOM.vWarpPos 	= g_vFMMC_MissionStartPos
									MC_serverBD.sServerFMMC_EOM.contentID	= ""	// Added by Keith for 'Random' 2/7/13 - wasn't used for 'Quick Restart' so NULLifying to prevent confusion
									MC_serverBD.sServerFMMC_EOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART			
									PRINTLN("[PROCESS_SERVER] FMMC EOM - sc_AutoForceRestartNoCorona - MC_serverBD.sServerFMMC_EOM.vWarpPos  = ", MC_serverBD.sServerFMMC_EOM.vWarpPos)
									PRINTLN("[PROCESS_SERVER] FMMC EOM - sc_AutoForceRestartNoCorona - MC_serverBD.sServerFMMC_EOM.contentID = ", MC_serverBD.sServerFMMC_EOM.contentID)
								ENDIF
								#ENDIF 
							ENDIF
						#ENDIF
						
					ELSE
						
						IF HAVE_ALL_PLAYERS_FINISHED()
							IF NOT HAS_NET_TIMER_STARTED(tdeomwaittimer)
								START_NET_TIMER(tdeomwaittimer)
							ENDIF
							
							IF NOT PROCESS_PLAYER_TEAM_SWAP()
							OR (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdeomwaittimer) >= ciTIMESTAMP_FOR_TEAM_SWAP)
								PRINTLN("[RCC MISSION][PROCESS_SERVER] HAVE_ALL_PLAYERS_FINISHED returning TRUE")
								REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iparticipant
									COUNT_PLAYER_SCORES(iparticipant)
								ENDREPEAT
								IF g_FMMC_STRUCT.iMissionEndType = ciMISSION_SCORING_TYPE_ALL_VS_1
									SORT_ALL_VS_1_TEAMS()
								ELSE
									SORT_MISSION_WINNING_TEAMS()
								ENDIF
								
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciSUMO_ZONE_CAPTURE_BAR_WINNER)
									PRINTLN("[RCC MISSION][PROCESS_SERVER] ciSUMO_ZONE_CAPTURE_BAR_WINNER set, call GIVE_TEAM_ENOUGH_POINTS_TO_PASS for winning team ", MC_serverBD.iWinningTeam)
									GIVE_TEAM_ENOUGH_POINTS_TO_PASS(MC_serverBD.iWinningTeam)
								ENDIF
								
								ARE_TEAM_NUMBERS_TOO_LOW_FOR_NEXT_ROUND()
								
								SET_SERVER_GAME_STATE(GAME_STATE_END)
							ELSE
								PRINTLN("[RCC MISSION][PROCESS_SERVER] MISSION WAITING TO SWAP TEAMS!")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF

		BREAK
		  
		CASE GAME_STATE_END	
			//Set the data for rounds mission if we're on a round
			SERVER_SET_WINING_TEAM_BROAD_CAST_DATA()
			
			bEveryoneOnLB = TRUE
			FMMC_EOM_SERVER_RESET_PLAYER_COUNTS(sPlayerCounts)
			REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iparticipant
				FMMC_EOM_SERVER_PLAYER_PROCESSING(sPlayerCounts, iparticipant) 
				tempPart = INT_TO_PARTICIPANTINDEX(iparticipant)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
					IF NOT IS_BIT_SET(MC_PlayerBD[iparticipant].iClientBitSet,PBBOOL_ON_LEADERBOARD)
						bEveryoneOnLB = FALSE
					ENDIF
				ENDIF
			ENDREPEAT
			IF bEveryoneOnLB
				MC_serverBD.sServerFMMC_EOM.bAllowedToVote = TRUE
				PRINTLN("[RCC MISSION][PROCESS_SERVER] MC_serverBD.sServerFMMC_EOM.bAllowedToVote = TRUE")
			ELSE
				PRINTLN("[RCC MISSION][PROCESS_SERVER] IF bEveryoneOnLB ELSE")
			ENDIF
			
			//For the pass ratio
			FLOAT fPassRatio
			
			//If we're on a heist then make it more than 50%
			IF (Is_Player_Currently_On_MP_Heist(LocalPlayer) 
			OR Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer))
			AND g_FMMC_STRUCT.iNumParticipants > 2
				fPassRatio = 0.51
			ELSE
				fPassRatio = 0.5
			ENDIF
			
			FMMC_EOM_SERVER_END_OF_FRAME_PROCESSING(sPlayerCounts, MC_serverBD.sServerFMMC_EOM, FMMC_TYPE_MISSION, 0, fPassRatio)
			
			IF SHOULD_RUN_NEW_VOTING_SYSTEM(sEndOfMission.iQuitProgress, MC_serverBD.sServerFMMC_EOM.iVoteStatus)
				// 2292909 don't do prematurely during Rounds missions 
				IF NOT IS_THIS_A_ROUNDS_MISSION()
				OR SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
					SERVER_JOB_VOTE_PROCESSING(g_sMC_serverBDEndJob)
				ENDIF
			ENDIF
		BREAK	

	ENDSWITCH
ENDPROC

FUNC BOOL SET_VEHICLE_HANDBRAKE_STATE(BOOL bOn)
	VEHICLE_INDEX vehPlayer

	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())

		IF NETWORK_HAS_CONTROL_OF_ENTITY(vehPlayer)
			IF bOn 
				PRINTLN("LIMIT RACE CONROLS - [SET_VEHICLE_HANDBRAKE_ON] VEHICLE_HANDBRAKE, ON")
			ELSE
				PRINTLN("LIMIT RACE CONROLS - [SET_VEHICLE_HANDBRAKE_ON] VEHICLE_HANDBRAKE, OFF")
			ENDIF

			SET_VEHICLE_HANDBRAKE(vehPlayer, bOn)
			RETURN TRUE
		ELSE
			PRINTLN("LIMIT RACE CONROLS - [SET_VEHICLE_HANDBRAKE_ON] VEHICLE_HANDBRAKE, NETWORK_HAS_CONTROL_OF_ENTITY")
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL OK_TO_DO_VEHICLE_BOOST()

	IF IS_BIT_SET(g_FMMC_STRUCT.bsRaceMission, ciDidSpeedBoost)
		RETURN FALSE
	ENDIF

	IF IS_RACE_PASSENGER()
		RETURN FALSE
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(g_FMMC_STRUCT.stBoostTimer)
	AND HAS_NET_TIMER_EXPIRED(g_FMMC_STRUCT.stBoostTimer, 1000)
		RESET_NET_TIMER(g_FMMC_STRUCT.stBoostTimer)
		RETURN FALSE
	ENDIF
	
	//RETURN TRUE
	// 2540514 - Always return false - stripping out this functionality for Lowrider 
	RETURN TRUE
ENDFUNC

PROC DO_VEH_BOOST()
	IF HAS_NET_TIMER_STARTED(g_FMMC_STRUCT.stBoostTimer)
		IF OK_TO_DO_VEHICLE_BOOST()
			PRINTLN("[JS] DO_VEH_BOOST - Waiting for input")
			IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE) 
				PRINTLN(">>>>>>>>>>>> OK_TO_DO_VEHICLE_BOOST, DO_VEH_BOOST, START_VEHICLE_BOOST")
				START_VEHICLE_BOOST(0.5)
				IF NOT IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetSix[0],  ciBS_RULE6_FILTERS_FORCE_FILTER)
					ANIMPOSTFX_PLAY("RaceTurbo", 0, FALSE)
				ENDIF
				SET_BIT(g_FMMC_STRUCT.bsRaceMission, ciDidSpeedBoost)
				INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_TURBO_STARTS_IN_RACE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_PROOFS()

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_ProofsInvalidatedOnFoot)
		EXIT
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF NOT g_bProofsSet 
			//SET_PLAYER_PROOFS_FOR_TEAM(GET_PLAYER_TEAM(GET_PLAYER_INDEX()))
			RUN_LOCAL_PLAYER_PROOF_SETTING(TRUE)
			g_bProofsSet = TRUE
			PRINTLN("[MJL][PROCESS_PROOFS] - MissionController - Proofs are SET")
		ENDIF
	ELSE
		IF g_bProofsSet
			SET_ENTITY_PROOFS(PLAYER_PED_ID(),FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,DEFAULT,FALSE)
			g_bProofsSet = FALSE
			PRINTLN("[MJL][PROCESS_PROOFS] - MissionController - Proofs are all set to FALSE")	
		ENDIF
	ENDIF
	
ENDPROC

PROC DISABLE_CONTROL_THIS_FRAME_ENABLE_HORN_AND_HOOD(BOOL bBlockCam = TRUE, BOOL bAllowRev = FALSE, BOOL bAllowRadio = FALSE , BOOL bHandBrakeOn = TRUE, BOOL bAllowHydro = FALSE)
	DISABLE_ALL_CONTROL_ACTIONS(PLAYER_CONTROL)
	IF bBlockCam
		DISABLE_ALL_CONTROL_ACTIONS(CAMERA_CONTROL)
	    PRINTLN("LIMIT RACE CONROLS - DISABLE_CONTROL_THIS_FRAME_ENABLE_HORN_AND_HOOD - DISABLE_CONTROL_THIS_FRAME_ENABLE_HORN_AND_HOOD - DISABLE_ALL_CONTROL_ACTIONS(CAMERA_CONTROL)")
	ELSE
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_LOOK_BEHIND)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_LOOK_LEFT)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_LOOK_RIGHT)
	ENDIF
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SPECIAL)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ROOF)	
	IF bAllowRadio
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
		PRINTLN("LIMIT RACE CONROLS - bAllowRadio")
	ENDIF
	IF bAllowRev
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)	
		PRINTLN("LIMIT RACE CONROLS - bAllowRev")
	ENDIF
	IF bAllowHydro
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HYDRAULICS_CONTROL_TOGGLE)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HYDRAULICS_CONTROL_DOWN)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HYDRAULICS_CONTROL_LEFT)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HYDRAULICS_CONTROL_RIGHT)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HYDRAULICS_CONTROL_UP)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HYDRAULICS_CONTROL_UD)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HYDRAULICS_CONTROL_LR)
		PRINTLN("LIMIT RACE CONROLS - bAllowHydro")
	ENDIF
	
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)

	SET_VEHICLE_HANDBRAKE_STATE(bHandBrakeOn)

ENDPROC

PROC PROCESS_BEFORE_GAME_STATE_RUNNING()
	RENDER_SPAWN_PROTECTION_SPHERES_EARLY()
	
	IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_PLACED_PEDS_PERFORMED_ROLLING_START)
	AND (MC_playerBD[iLocalPart].iGameState = GAME_STATE_RUNNING
	OR MC_playerBD[iLocalPart].iGameState = GAME_STATE_CAMERA_BLEND)
		INT iPed
		IF MC_serverBD.iNumPedCreated > 0
		AND MC_serverBD.iNumPedCreated <= FMMC_MAX_PEDS
			FOR iPed = 0 TO (MC_serverBD.iNumPedCreated-1)
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iped])
					PED_INDEX tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
					PROCESS_PLACED_PED_ROLLING_START(iPed, tempPed)
				ENDIF
			ENDFOR
		ENDIF
		
		SET_BIT(iLocalBoolCheck31, LBOOL31_PLACED_PEDS_PERFORMED_ROLLING_START)
	ENDIF
	
	IF MC_playerBD[iLocalPart].iGameState != GAME_STATE_RUNNING
	AND MC_playerBD[iLocalPart].iGameState != GAME_STATE_MISSION_OVER
	AND MC_playerBD[iLocalPart].iGameState != GAME_STATE_LEAVE
	AND MC_playerBD[iLocalPart].iGameState != GAME_STATE_END
	AND NOT IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitset, PBBOOL_ON_LEADERBOARD)
	AND NOT IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciDISABLE_SPECIAL_VEHICLE_WEAPONS)
			OR IS_THIS_CURRENTLY_OVERTIME_RUMBLE()
				DISABLE_RUINER_WEAPONS_TO_SHOW_COVER_PANEL()
			ENDIF
		
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SPECIAL, TRUE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_JUMP, TRUE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK, TRUE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2, TRUE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN, TRUE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_LOOK_BEHIND, TRUE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CAR_JUMP, TRUE)
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitsetTen[0], ciBS_RULE10_DISABLE_DELUXO_HOVER_MODE)
				PRINTLN("[PROCESS_BEFORE_GAME_STATE_RUNNING][RH] disabling Deluxo hover as the first turn is set")
				VEHICLE_INDEX viPedCar = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
				IF GET_ENTITY_MODEL(viPedCar) = DELUXO
					IF DOES_ENTITY_EXIST(viPedCar)
						SET_SPECIAL_FLIGHT_MODE_ALLOWED(viPedCar, FALSE)
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitsetTen[0], ciBS_RULE10_DISABLE_DELUXO_FLIGHT_MODE)
				PRINTLN("[PROCESS_BEFORE_GAME_STATE_RUNNING][RH] disabling Deluxo flight mode as the first turn is set")
				VEHICLE_INDEX viPedCar = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
				IF GET_ENTITY_MODEL(viPedCar) = DELUXO
					IF DOES_ENTITY_EXIST(viPedCar)
						SET_DISABLE_HOVER_MODE_FLIGHT(viPedCar, FALSE)
					ENDIF
				ENDIF
			ENDIF
			
			SET_BIT(iLocalBoolCheck30, LBOOL30_SHOULD_RESET_VEHICLE_MINES_BLOCK_1)
			
			DISABLE_VEHICLE_MINES(TRUE)
			PRINTLN("[RCC MISSION] DISABLE_VEHICLE_MINES(TRUE)")
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck30, LBOOL30_SHOULD_RESET_VEHICLE_MINES_BLOCK_1)
			IF g_bDisableVehicleMines
			AND IS_BITMASK_AS_ENUM_SET(cduiIntro.iBitFlags, CNTDWN_UI2_Played_Go)
				DISABLE_VEHICLE_MINES(FALSE)
				PRINTLN("[RCC MISSION] DISABLE_VEHICLE_MINES(FALSE)")
			ENDIF
			CLEAR_BIT(iLocalBoolCheck30, LBOOL30_SHOULD_RESET_VEHICLE_MINES_BLOCK_1)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[MC_playerBD[iLocalPart].iTeam][0].iSpawnBitSet, ci_SpawnBS_AirSpawn)
		VECTOR vTempPos
		vTempPos = GET_ENTITY_COORDS(LocalPlayerPed)
		
		IF VDIST(vTempPos, vDZPlayerSpawnLoc) < 100
		AND NOT IS_PED_IN_ANY_VEHICLE(localPlayerPed)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PARACHUTE_DEPLOY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
		ENDIF
	ENDIF
	
ENDPROC

// May need to add additional conditions to stop this from clearing text where we might expect to not be in PlayingNormally and still see the UI.
// Spectators for example will be in _INVALID state, so we don't want to clear their UI for not being in PlayingNormally. We may want to also allow spectators to do this. We'll have to see.
PROC PROCESS_CLEAR_HUD_FOR_SPECIAL_REASONS()
	IF GET_PLAYER_RESPAWN_BACK_AT_START_STATE(iPartToUse) != PRBSS_PlayingNormally
	AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)
	AND NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
	AND NOT g_bCelebrationScreenIsActive
	AND NOT g_bMissionEnding
		PRINTLN("[LM][Restart] PROCESS_PLAYER_RESPAWN_BACK_AT_START [PROCESS_CLEAR_HUD_FOR_SPECIAL_REASONS] - Disabling the HUD this frame - Restarting Mini-Round!")
		DISABLE_HUD()
		DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_ALL_OVERHEADS)
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		DISABLE_ALL_MP_HUD_THIS_FRAME()				
		IF IS_BROWSER_OPEN()
			MP_FORCE_TERMINATE_INTERNET()
		ELSE
			MP_FORCE_TERMINATE_INTERNET_CLEAR()
		ENDIF
		IF Is_MP_Objective_Text_On_Display()
			Clear_Any_Objective_Text()
		ENDIF
		IF IS_HELP_MESSAGE_BEING_DISPLAYED()
			CLEAR_HELP(TRUE)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_PLAYER_FAIL_QUIT_TEST_MODE_FOR_CREATOR()
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET() 
		EXIT
	ENDIF
	
	IF Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)
		INT iTime 
		iTime = GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[0].iObjectiveTimeLimitRule[0]) 
		INT iDiff
		iDiff = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdObjectiveLimitTimer[0])
		
		IF NOT IS_PAUSE_MENU_ACTIVE()
			IF (NOT IS_SCREEN_FADED_OUT() AND NOT IS_SCREEN_FADING_OUT())
				IF iDiff > iTime
				OR IS_BIT_SET(MC_playerBD[0].iClientBitSet, PBBOOL_PLAYER_FAIL)
					g_FMMC_STRUCT.g_b_QuitTest = TRUE
					IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
						SCRIPT_TRIGGER_MUSIC_EVENT_STOP()
						PRINTLN("[RCC MISSION][PROCESS_PLAYER_FAIL_QUIT_TEST_MODE_FOR_CREATOR] - MUSIC - TRIGGER_MUSIC_EVENT(MP_MC_STOP) - L")
					ENDIF
				ENDIF 
			ELSE
				RESET_NET_TIMER(MC_serverBD_3.tdObjectiveLimitTimer[0])
			ENDIF
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT.g_b_QuitTest
		IF NOT IS_PAUSE_MENU_ACTIVE()		
			IF IS_SCREEN_FADED_OUT()
				SET_MC_CLIENT_GAME_STATE(GAME_STATE_MISSION_OVER)
			ELSE
				IF NOT IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_OUT(500)
					ALLOW_MISSION_CREATOR_WARP(TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SET_UP_PERSONAL_VEHICLE_CREATION_FOR_MISSION()
			
	IF IS_PV_DISABLED()
		DISABLE_PERSONAL_VEHICLE_CREATION_FOR_MISSION( TRUE ) 
		PRINTLN("[PROCESS_CLIENT][GAME_STATE_INI][MMacK][SET_UP_PERSONAL_VEHICLE_CREATION_FOR_MISSION] PV Is Disabled")
	ELSE 
		IF NOT g_TransitionSessionNonResetVars.bHasQuickRestarted
			PRINTLN("[PROCESS_CLIENT][GAME_STATE_INI][MMacK][SET_UP_PERSONAL_VEHICLE_CREATION_FOR_MISSION] No Quick Restart")
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciDISABLE_PERSONAL_VEH_INITIAL_SPAWN)
				PRINTLN("[PROCESS_CLIENT][GAME_STATE_INI][MMacK][SET_UP_PERSONAL_VEHICLE_CREATION_FOR_MISSION] Disabled On Initial Spawn 1")
				DISABLE_PERSONAL_VEHICLE_CREATION_FOR_MISSION( TRUE ) 
			ELSE
				PRINTLN("[PROCESS_CLIENT][GAME_STATE_INI][MMacK][SET_UP_PERSONAL_VEHICLE_CREATION_FOR_MISSION] Not Disabled On Initial Spawn 1")
			ENDIF
		ELSE
			IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(3)
				PRINTLN("[PROCESS_CLIENT][GAME_STATE_INI][MMacK][SET_UP_PERSONAL_VEHICLE_CREATION_FOR_MISSION] Checkpoint 4 Active")
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_DisablePersonalVehOnCheckpoint_3)
					PRINTLN("[PROCESS_CLIENT][GAME_STATE_INI][MMacK][SET_UP_PERSONAL_VEHICLE_CREATION_FOR_MISSION] Disabled On Checkpoint 4")
					DISABLE_PERSONAL_VEHICLE_CREATION_FOR_MISSION( TRUE ) 
				ELSE	
					PRINTLN("[PROCESS_CLIENT][GAME_STATE_INI][MMacK][SET_UP_PERSONAL_VEHICLE_CREATION_FOR_MISSION] Not Disabled On Checkpoint 4")
				ENDIF
			ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(2)
				PRINTLN("[PROCESS_CLIENT][GAME_STATE_INI][MMacK][SET_UP_PERSONAL_VEHICLE_CREATION_FOR_MISSION] Checkpoint 3 Active")
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciOptionsBS17_DisablePersonalVehOnCheckpoint_2)
					PRINTLN("[PROCESS_CLIENT][GAME_STATE_INI][MMacK][SET_UP_PERSONAL_VEHICLE_CREATION_FOR_MISSION] Disabled On Checkpoint 3")
					DISABLE_PERSONAL_VEHICLE_CREATION_FOR_MISSION( TRUE ) 
				ELSE	
					PRINTLN("[PROCESS_CLIENT][GAME_STATE_INI][MMacK][SET_UP_PERSONAL_VEHICLE_CREATION_FOR_MISSION] Not Disabled On Checkpoint 3")
				ENDIF
			ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(1)
				PRINTLN("[PROCESS_CLIENT][GAME_STATE_INI][MMacK][SET_UP_PERSONAL_VEHICLE_CREATION_FOR_MISSION] Checkpoint 2 Active")
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciDISABLE_PERSONAL_VEH_CHECKPOINT_1)
					PRINTLN("[PROCESS_CLIENT][GAME_STATE_INI][MMacK][SET_UP_PERSONAL_VEHICLE_CREATION_FOR_MISSION] Disabled On Checkpoint 2")
					DISABLE_PERSONAL_VEHICLE_CREATION_FOR_MISSION( TRUE ) 
				ELSE	
					PRINTLN("[PROCESS_CLIENT][GAME_STATE_INI][MMacK][SET_UP_PERSONAL_VEHICLE_CREATION_FOR_MISSION] Not Disabled On Checkpoint 2")
				ENDIF
			ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(0)
				PRINTLN("[PROCESS_CLIENT][GAME_STATE_INI][MMacK][SET_UP_PERSONAL_VEHICLE_CREATION_FOR_MISSION] Checkpoint 1 Active")
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciDISABLE_PERSONAL_VEH_CHECKPOINT_0)
					PRINTLN("[PROCESS_CLIENT][GAME_STATE_INI][MMacK][SET_UP_PERSONAL_VEHICLE_CREATION_FOR_MISSION] Disabled On Checkpoint 1")
					DISABLE_PERSONAL_VEHICLE_CREATION_FOR_MISSION( TRUE ) 
				ELSE	
					PRINTLN("[PROCESS_CLIENT][GAME_STATE_INI][MMacK][SET_UP_PERSONAL_VEHICLE_CREATION_FOR_MISSION] Not Disabled On Checkpoint 1")
				ENDIF
			ELSE
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciDISABLE_PERSONAL_VEH_INITIAL_SPAWN)
					PRINTLN("[PROCESS_CLIENT][GAME_STATE_INI][MMacK][SET_UP_PERSONAL_VEHICLE_CREATION_FOR_MISSION] Disabled On Initial Spawn 2")
					DISABLE_PERSONAL_VEHICLE_CREATION_FOR_MISSION( TRUE ) 
				ELSE
					PRINTLN("[PROCESS_CLIENT][GAME_STATE_INI][MMacK][SET_UP_PERSONAL_VEHICLE_CREATION_FOR_MISSION] Not Disabled On Initial Spawn 2")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_TRIP_SKIP_EVERY_FRAME()
	IF AM_I_ON_A_HEIST()
	OR IS_FAKE_MULTIPLAYER_MODE_SET()
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciALLOW_NON_HEIST_REBREATHERS)
	OR GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_EnableTripSkipForMission)
		VECTOR vTripSkipDestination
		
		IF MC_playerBD[iLocalPart].TSPlayerData.iStage > TSS_VOTE
		AND MC_playerBD[iLocalPart].TSPlayerData.iStage <= TSS_INTERP_CAM
			PRINTLN("TRIP SKIP - Disabling Player controls to avoid leaving the vehicle. (TSPlayerData) We are between TSS_VOTE/TSS_INTERP_CAM")
			PROCESS_DISABLING_CONTROLS_WHILE_ACTIVE_TRIP_SKIP()
		ENDIF
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciALLOW_NON_HEIST_REBREATHERS)
		OR GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_EnableTripSkipForMission)
			//Don't Do Trip Skip after Checkpoint or on Restart
			
			PRINTLN("TRIP SKIP - IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciTRIP_SKIP_ALL): ", IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciTRIP_SKIP_ALL))
			//Whole Crew Trip Skip
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciTRIP_SKIP_ALL)						
				IF NOT IS_BIT_SET(MC_serverBD_3.TSServerData[0].iBitSet, TSS_BI_BLOCKED)
					IF (IS_THIS_A_QUICK_RESTART_JOB() OR IS_CORONA_INITIALISING_A_QUICK_RESTART())
					AND (FMMC_CHECKPOINTS_IS_ANY_RETRY_START_POINT_SET())
						SET_BIT(MC_serverBD_3.TSServerData[0].iBitSet, TSS_BI_BLOCKED)
						PRINTLN("TRIP SKIP - BLOCKED - TSS_BI_BLOCKED")
					ENDIF
				ENDIF
				
				IF MC_serverBD_4.iCurrentHighestPriority[0] < FMMC_MAX_RULES
				AND NOT IS_BIT_SET(MC_serverBD_3.TSServerData[0].iBitSet, TSS_BI_BLOCKED)
					IF (IS_TEAM_ON_GET_AND_DELIVER_RULE(0)
					AND GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION())
						vTripSkipDestination = GET_DROP_OFF_CENTER_FOR_TEAM(DEFAULT, 0)
						PRINTLN("(1) TRIP SKIP [RCC MISSION] - G&D - vTripSkipDestination: ", vTripSkipDestination)
					ELSE
						vTripSkipDestination = MPGlobalsAmbience.vQuickGPS
						PRINTLN("(1) TRIP SKIP [RCC MISSION] - vTripSkipDestination: ", vTripSkipDestination)
					ENDIF
					
					PRINTLN("TRIP SKIP - TYPE - WHOLE CREW TRIP SKIP")
					IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
						MAINTAIN_TRIP_SKIP_CLIENT(MC_serverBD_3.TSServerData[0], MC_playerBD[iLocalPart].TSPlayerData, TSLocalData, vTripSkipDestination, IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[0].iRuleBitsetTwo[MC_serverBD_4.iCurrentHighestPriority[0]], ciBS_RULE2_USE_TRIP_SKIP), (GET_MP_INT_CHARACTER_STAT(GET_HEIST_MISSION_COMPLETION_STAT(GET_CURRENT_HEIST_MISSION())) > 0), bHasTripSkipJustFinished, TRUE)
					ENDIF
					IF bIsLocalPlayerHost
						MAINTAIN_TRIP_SKIP_SERVER(MC_serverBD_3.TSServerData[0], (MC_playerBD[iLocalPart].bPlayedFirstDialogue OR IS_THIS_A_QUICK_RESTART_JOB()), 0)
					ENDIF
				ENDIF
					
			//Separate Team Trip Skip
			ELSE
				PRINTLN("____________ (2) TRIP SKIP - CLIENT - (SEPARATE TEAM) for Team: ", MC_playerBD[iPartToUse].iteam, "_______________")
				BOOL bTeamOnGDRule
				bTeamOnGDRule = IS_TEAM_ON_GET_AND_DELIVER_RULE(MC_playerBD[iPartToUse].iteam)
				IF NOT IS_BIT_SET(MC_serverBD_3.TSServerData[MC_playerBD[iPartToUse].iteam].iBitSet, TSS_BI_BLOCKED)
					IF (IS_THIS_A_QUICK_RESTART_JOB() OR IS_CORONA_INITIALISING_A_QUICK_RESTART())
					AND (FMMC_CHECKPOINTS_IS_ANY_RETRY_START_POINT_SET())
						SET_BIT(MC_serverBD_3.TSServerData[MC_playerBD[iPartToUse].iteam].iBitSet, TSS_BI_BLOCKED)
						PRINTLN("TRIP SKIP - BLOCKED - TSS_BI_BLOCKED")
					ENDIF
				ENDIF
				
				IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
				AND NOT IS_BIT_SET(MC_serverBD_3.TSServerData[MC_playerBD[iPartToUse].iteam].iBitSet, TSS_BI_BLOCKED)
					IF (bTeamOnGDRule
					AND GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION())
						vTripSkipDestination = GET_DROP_OFF_CENTER_FOR_TEAM(DEFAULT, MC_playerBD[iPartToUse].iteam)
						PRINTLN("(2) TRIP SKIP - CLIENT - - G&D - vTripSkipDestination: ", vTripSkipDestination)
					ELSE
						vTripSkipDestination = MPGlobalsAmbience.vQuickGPS
						PRINTLN("(2) TRIP SKIP - CLIENT - vTripSkipDestination: ", vTripSkipDestination)
					ENDIF
					
					PRINTLN("(2) TRIP SKIP - CLIENT - TSServerBD.vCarPos.x: ", MC_serverBD_3.TSServerData[MC_playerBD[iPartToUse].iteam].vCarPos.x, " TSServerBD.vCarPos.y: ", MC_serverBD_3.TSServerData[MC_playerBD[iPartToUse].iteam].vCarPos.y, " TSServerBD.vCarPos.z: ", MC_serverBD_3.TSServerData[MC_playerBD[iPartToUse].iteam].vCarPos.z)
					
					IF (bTeamOnGDRule
					AND GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_DELIVER_VEH
					AND GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION())
					OR NOT bTeamOnGDRule
					OR NOT GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
						IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
							MAINTAIN_TRIP_SKIP_CLIENT(MC_serverBD_3.TSServerData[MC_playerBD[iPartToUse].iteam], MC_playerBD[iLocalPart].TSPlayerData, TSLocalData, vTripSkipDestination, IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetTwo[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE2_USE_TRIP_SKIP), IS_FAKE_MULTIPLAYER_MODE_SET() OR (GET_MP_INT_CHARACTER_STAT(GET_HEIST_MISSION_COMPLETION_STAT(GET_CURRENT_HEIST_MISSION())) > 0), bHasTripSkipJustFinished)
						ENDIF
					ELSE
						IF MC_playerBD[iLocalPart].TSPlayerData.iStage > TSS_VOTE
						AND MC_playerBD[iLocalPart].TSPlayerData.iStage <= TSS_INTERP_CAM
							IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
								PRINTLN("TRIP SKIP - CLIENT - (SEPARATE TEAM)- NOT ON DELIVER STAGE but already started - CLEANUP")
								MC_playerBD[iLocalPart].TSPlayerData.iStage = TSS_CLEANUP
								MAINTAIN_TRIP_SKIP_CLIENT(MC_serverBD_3.TSServerData[MC_playerBD[iLocalPart].iteam], MC_playerBD[iLocalPart].TSPlayerData, TSLocalData, vTripSkipDestination, IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetTwo[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE2_USE_TRIP_SKIP), IS_FAKE_MULTIPLAYER_MODE_SET() OR (GET_MP_INT_CHARACTER_STAT(GET_HEIST_MISSION_COMPLETION_STAT(GET_CURRENT_HEIST_MISSION())) > 0), bHasTripSkipJustFinished)
							ENDIF
						#IF IS_DEBUG_BUILD
						ELSE
							PRINTLN("TRIP SKIP - CLIENT - (SEPARATE TEAM)- NOT ON DELIVER STAGE")
						#ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF bIsLocalPlayerHost
					INT iTripSkipTeam
					FOR iTripSkipTeam = 0 TO (FMMC_MAX_TEAMS-1)
						IF IS_TEAM_ACTIVE(iTripSkipTeam)
							PRINTLN("________________ TRIP SKIP - SERVER - (SEPARATE TEAM) - NOT ON DELIVER STAGE iTripSkipTeam: ", iTripSkipTeam, "_________________")
							MAINTAIN_TRIP_SKIP_SERVER(MC_serverBD_3.TSServerData[iTripSkipTeam], (MC_playerBD[iLocalPart].bPlayedFirstDialogue OR IS_THIS_A_QUICK_RESTART_JOB()), iTripSkipTeam)
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_QUICK_EQUIP_MASK()	
	IF AM_I_ON_A_HEIST()
	OR IS_FAKE_MULTIPLAYER_MODE_SET()
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciALLOW_NON_HEIST_REBREATHERS)
	OR GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
	OR IS_BIT_SET( g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_HeistOptionAllowMaskAfterTeamAggro)
	OR g_bAutoMaskEquip
	
		IF IS_BIT_SET( g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_HeistOptionAllowMaskAfterTeamAggro)
			IF NOT IS_BIT_SET(iLocalQuickMaskBitSet, LBS_MASK_QUICK_EQUIP_AVAILABLE)
				IF HAS_ANY_TEAM_TRIGGERED_AGGRO()
					SET_BIT(iLocalQuickMaskBitSet, LBS_MASK_QUICK_EQUIP_AVAILABLE)
				ENDIF
			ENDIF
		ENDIF
		
		//Quick Equip Mask (also Rebreather, and Quick Remove)
		IF MC_serverBD_4.iCurrentHighestPriority[ MC_playerBD[iPartToUse].iteam ] < FMMC_MAX_RULES
		AND iSpectatorTarget = -1
			BOOL bAvailable
			BOOL bUseRebreather
			BOOL bRemoveMask
			//REBREATHER
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetThree[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE3_QUICK_EQUIP_REBREATHER)
				bAvailable = TRUE
				bUseRebreather = TRUE
				#IF IS_DEBUG_BUILD IF GET_FRAME_COUNT() % 60 = 0 PRINTLN("MASK - SERVER - ciBS_RULE3_QUICK_EQUIP_REBREATHER = TRUE") ENDIF #ENDIF
			//REMOVE MASK
			ELIF IS_BIT_SET(iLocalQuickMaskBitSet, LBS_MASK_QUICK_UNEQUIP_AVAILABLE)
				bAvailable = TRUE
				bRemoveMask = TRUE
				#IF IS_DEBUG_BUILD IF GET_FRAME_COUNT() % 60 = 0 PRINTLN("MASK - SERVER - LBS_MASK_QUICK_UNEQUIP_AVAILABLE = TRUE - REMOVE MASK") ENDIF #ENDIF
			//EQUIP MASK
			ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetTwo[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE2_QUICK_EQUIP_MASK)
			OR IS_BIT_SET(iLocalQuickMaskBitSet, LBS_MASK_QUICK_EQUIP_AVAILABLE)
				bAvailable = TRUE
				g_bMaskEquipReached = TRUE
				#IF IS_DEBUG_BUILD IF GET_FRAME_COUNT() % 60 = 0 PRINTLN("MASK - SERVER - ciBS_RULE2_QUICK_EQUIP_MASK = TRUE") ENDIF #ENDIF
			ELIF g_bAutoMaskEquip
				bAvailable = TRUE
				g_bMaskEquipReached = TRUE
				#IF IS_DEBUG_BUILD IF GET_FRAME_COUNT() % 60 = 0 PRINTLN("MASK - SERVER - g_bAutoMaskEquip = TRUE") ENDIF #ENDIF
			ENDIF
			
			MAINTAIN_QUICK_EQUIP_MASK(maskAnimData, bAnimationPlaying, MC_playerBD[iPartToUse].iteam, bAvailable, iLocalQuickMaskBitSet, bUseRebreather, bRemoveMask)
			MAINTAIN_PLAYER_MASK_STATUS() //handle this for re-equip on quick restart
			IF g_bMaskEquipReached AND g_iEquippedMask != -1 //if we've done a quick equip restart, disable help
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HQE_MASK")
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HQE_MASK1")
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HQE_MASK2")
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HQE_MASK3")
					CLEAR_HELP()
				ENDIF
			ENDIF			
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_CLIENT_PRE_RUNNING()
	// Stop enemies attacking the player until in gameplay. B* 2048103. 
	IF MC_playerBD[iLocalPart].iGameState < GAME_STATE_RUNNING
		SET_EVERYONE_IGNORE_PLAYER(LocalPlayer, TRUE)
		SET_PED_RESET_FLAG(LocalPlayerPed, PRF_CannotBeTargetedByAI, TRUE)
	ENDIF
ENDPROC

PROC UNFREEZE_ENTITIES_AT_THE_START_OF_MISSION_IF_NECESSARY()
	IF NOT IS_BIT_SET(iLocalBoolCheck8, LBOOL8_ENTITIES_UNFROZEN_AT_START_OF_MISSION)
	AND MC_playerBD[iLocalPart].iGameState >= GAME_STATE_RUNNING
		IF IS_BIT_SET(MC_serverbd.iServerBitSet, SSBOOL_ALL_READY_INTRO_CUT)
			
			IF NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(PLAYER_ID())
			AND NOT IS_PLAYER_SCTV(PLAYER_ID())
				SET_ALL_LEGACY_MISSION_ENTITIES_UNFROZEN_AND_ABLE_TO_MIGRATE(MC_serverBD_1.sFMMC_SBD, iPedStartedInCoverBitset)
			ENDIF
			
			SET_BIT(iLocalBoolCheck8, LBOOL8_ENTITIES_UNFROZEN_AT_START_OF_MISSION)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_CLIENT_BLIMP_SIGNS()
	BOOL bDoBlimpSigns	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
	AND MC_playerBD[iLocalPart].iGameState = GAME_STATE_RUNNING
		bDoBlimpSigns = TRUE
	ELIF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
		bDoBlimpSigns = TRUE
	ENDIF	
	IF bDoBlimpsigns
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
			DO_MISSION_BLIMP_SIGNS(g_bMissionOver)
		ELSE
			DO_BLIMP_SIGNS(sBlimpSign, g_bMissionOver)
		ENDIF
	ENDIF
ENDPROC

PROC APPLY_INTRO_GAIT()
	IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_APPLIED_INTRO_GAIT)
		SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), 0.5, 3500)
		SET_BIT(iLocalBoolCheck31, LBOOL31_APPLIED_INTRO_GAIT)
		PRINTLN("[RCC MISSION] - Doing SIMULATE_PLAYER_INPUT_GAIT")
	ENDIF
ENDPROC

PROC PROCESS_START_MISSION_WALKING()
	IF MC_playerBD[iLocalPart].iGameState >= GAME_STATE_INTRO_CUTSCENE
		IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_APPLIED_INTRO_GAIT)
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitset4, ciBS4_TEAM_START_MISSION_WALKING)
			AND (IS_SKYSWOOP_AT_GROUND())
			AND NOT IS_SCREEN_FADED_OUT()
			AND GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()
			AND NOT IS_CUTSCENE_ACTIVE()
				APPLY_INTRO_GAIT()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_RESPAWN_VEHICLES_EACH_RULE(INT iRule)	
	IF iRule > -1 AND iRule < FMMC_MAX_RULES		
		INT iTeam
		FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
			IF bIsLocalPlayerHost
			AND IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule], ciBS_RULE6_RESPAWN_VEHICLES_EACH_RULE)
					PRINTLN("[MMacK][PROCESS_RESPAWN_VEHICLES_EACH_RULE] 3")
					INT iVeh
					FOR iVeh = 0 TO (MC_serverBD.iNumVehCreated-1)
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_RESPAWNS_AT_END_OF_RULE)
							PRINTLN("[MMacK][PROCESS_RESPAWN_VEHICLES_EACH_RULE] 4")
							IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
							AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
								PRINTLN("[MMacK][PROCESS_RESPAWN_VEHICLES_EACH_RULE] 5")
								IF NOT IS_VEHICLE_FUCKED_MP(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]))
									PRINTLN("[MMacK][PROCESS_RESPAWN_VEHICLES_EACH_RULE] Vehicle is respawning, fixed and heading")
									VEHICLE_INDEX viTemp 
									viTemp = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
									SET_ENTITY_COORDS((viTemp), g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vPos)
									SET_ENTITY_HEADING((viTemp), g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fHead)
									SET_VEHICLE_FIXED(viTemp)
								ELSE
									PRINTLN("[MMacK][PROCESS_RESPAWN_VEHICLES_EACH_RULE] Vehicle is not fit to respawn")
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

PROC PROCESS_OVERTIME_START_AS_SPECTATOR()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_OT_START_SPECTATING)
	AND NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_OVERTIME_INITIAL_SPECTATOR_DONE)
	AND iOTPartToMove > -1
		PARTICIPANT_INDEX tempPartToMove
		tempPartToMove = INT_TO_PARTICIPANTINDEX(iOTPartToMove)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPartToMove)
			PLAYER_INDEX tempPlayer 
			tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPartToMove)
			IF IS_NET_PLAYER_OK(tempPlayer)
				SET_BIT(iLocalBoolCheck23, LBOOL23_OVERTIME_INITIAL_SPECTATOR_DONE)
				IF iOTPartToMove != iLocalPart
					DO_SCREEN_FADE_OUT(500)
					BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("") 
					END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_LOADING)) 
					PRINTLN("[RCC MISSION] ciENABLE_OT_START_SPECTATING - Going to heist spectate thing, DO SCREEN FADE OUT")
					SET_BIT(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iSpecInfoBitset, SPEC_INFO_BS_USING_HEIST_SPECTATE)
					SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_DO_HEIST_SPECTATE)
					MPGlobalsAmbience.piHeistSpectateTarget = tempPlayer
					NETWORK_SET_OVERRIDE_SPECTATOR_MODE(TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_RACE_BOOST_START()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciRACE_BOOST_START) 
	AND NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_EXCLUDE_FROM_3_2_1_GO_COUNTDOWN)
		IF HAS_NET_TIMER_STARTED(g_FMMC_STRUCT.stBoostTimer)
			DO_VEH_BOOST()
		ENDIF
		UPDATE_VEHICLE_BOOSTING()
	ENDIF
ENDPROC

PROC SET_MISSION_START_RACE_TIME()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciENABLE_SPLIT_TIMES)
	AND NOT IS_BIT_SET(iLocalBoolCheck19, LBOOL19_RACE_SPLIT_TIMES_STARTED)
	AND IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
		SCRIPT_RACE_INIT(MC_serverBD.iNumLocCreated, g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iMissionLapLimit + 1, (MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]), NATIVE_TO_INT(PlayerToUse))
		PRINTLN("[JS] [SPLIT] - SCRIPT_RACE_INIT - Checkpoints: ", MC_serverBD.iNumLocCreated, " Laps: ", g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iMissionLapLimit + 1," Racers: ", (MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]))
		SET_BIT(iLocalBoolCheck19, LBOOL19_RACE_SPLIT_TIMES_STARTED)
	ENDIF
ENDPROC

PROC PROCESS_PRE_GAME_EMP_AFTER_INTRO()
	//If player joins after the intro then it needs to do this. 
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciPRE_GAME_EMP)
			SET_ARTIFICIAL_LIGHTS_STATE(TRUE)  
			SET_TIMECYCLE_MODIFIER("NoPedLight")
			SET_TIMECYCLE_MODIFIER_STRENGTH(1.0)
			IF NOT g_bFMMCLightsTurnedOff
				STRING sSoundSet 
				sSoundSet = "DLC_HALLOWEEN_FVJ_Sounds"
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_SLASHERS_AUDIO)
					sSoundSet = "dlc_xm_sls_Sounds"
				ENDIF
				
				PLAY_SOUND_FRONTEND(-1, "Breaker_02", sSoundSet, FALSE)
				PRINTLN("[RCC MISSION] Playing DLC_HALLOWEEN_FVJ_Sounds - 02 ")
			ENDIF
			g_bFMMCLightsTurnedOff = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_CLIENT_TREAT_AS_WANTED()
	IF IS_PLAYER_DEAD(LocalPlayer)
		IF GET_PLAYER_WANTED_LEVEL(LocalPlayer) > 0
			PRINTLN("PBBOOL4_TREAT_AS_WANTED - Setting PBBOOL4_TREAT_AS_WANTED")
			SET_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_TREAT_AS_WANTED)
		ENDIF
		
		IF HAS_NET_TIMER_STARTED(stWantedDeathCooldownTimer)
			RESET_NET_TIMER(stWantedDeathCooldownTimer)
		ENDIF
	ELSE
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_TREAT_AS_WANTED)
			IF NOT HAS_NET_TIMER_STARTED(stWantedDeathCooldownTimer)
				REINIT_NET_TIMER(stWantedDeathCooldownTimer)
				PRINTLN("PBBOOL4_TREAT_AS_WANTED - Starting timer")
			ELSE
				IF HAS_NET_TIMER_EXPIRED(stWantedDeathCooldownTimer, ciWantedDeathCooldownTimer_Length)
					PRINTLN("PBBOOL4_TREAT_AS_WANTED - Clearing PBBOOL4_TREAT_AS_WANTED")
					CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_TREAT_AS_WANTED)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_HOLD_UP_LOGIC_BETWEEN_ROUNDS()
	IF IS_EVERYONE_READY_FOR_GAMEPLAY() = FALSE
		PRINTLN("[RCC MISSION][PROCESS_CLIENT][ROUNDCAM][PROCESS_HOLD_UP_LOGIC_BETWEEN_ROUNDS] g_b_HoldupCutsceneBetweenRounds = TRUE GAME_STATE_TEAM_PLAY IS_EVERYONE_READY_FOR_GAMEPLAY() = FALSE ")	
		g_b_HoldupCutsceneBetweenRounds = TRUE
	ELSE
		PRINTLN("[RCC MISSION][PROCESS_CLIENT][ROUNDCAM][PROCESS_HOLD_UP_LOGIC_BETWEEN_ROUNDS] g_b_HoldupCutsceneBetweenRounds = FALSE GAME_STATE_TEAM_PLAY IS_EVERYONE_READY_FOR_GAMEPLAY() = TRUE ")
		g_b_HoldupCutsceneBetweenRounds = FALSE
	ENDIF
	
	IF GET_SKYSWOOP_STAGE() != SKYSWOOP_NONE
		PRINTLN("[RCC MISSION][PROCESS_CLIENT][ROUNDCAM][PROCESS_HOLD_UP_LOGIC_BETWEEN_ROUNDS] g_b_HoldupSkycamBetweenRounds = TRUE GET_SKYSWOOP_STAGE() = ", GET_SKYSWOOP_STRING(GET_SKYSWOOP_STAGE()))
		g_b_HoldupSkycamBetweenRounds = TRUE
	ELSE
		PRINTLN("[RCC MISSION][PROCESS_CLIENT][ROUNDCAM][PROCESS_HOLD_UP_LOGIC_BETWEEN_ROUNDS] g_b_HoldupSkycamBetweenRounds = FALSE GET_SKYSWOOP_STAGE() = ", GET_SKYSWOOP_STRING(GET_SKYSWOOP_STAGE()))
		g_b_HoldupSkycamBetweenRounds = FALSE
	ENDIF
ENDPROC

PROC PROCESS_TEAM_SWAPPING_ADVERSARY_MODES()
	IF HAS_NET_TIMER_STARTED(tdTeamSwapPassiveModeTimer)
	AND g_MultiplayerSettings.g_bTempPassiveModeSetting
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdTeamSwapPassiveModeTimer) >= 5000
			CLEANUP_TEMP_PASSIVE_MODE()
			TURN_OFF_PASSIVE_MODE_OPTION(FALSE)
			
			SET_ENTITY_ALPHA(LocalPlayerPed, 255, FALSE)
						
			SET_PED_RESET_FLAG(LocalPlayerPed, PRF_BlockWeaponFire, FALSE)
			
			NETWORK_SET_FRIENDLY_FIRE_OPTION(FALSE)
			
			RESET_NET_TIMER(tdTeamSwapPassiveModeTimer)
			
			PRINTLN("[MMacK][TeamSwaps] Cleared Passive")
		ELSE
			SET_PED_RESET_FLAG(LocalPlayerPed, PRF_BlockWeaponFire, TRUE)
			SET_PED_RESET_FLAG(LocalPlayerPed, PRF_SuspendInitiatedMeleeActions, TRUE)
		ENDIF
	ENDIF
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_GRASS_GREENER(g_FMMC_STRUCT.iAdversaryModeType)
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
		IF NOT IS_BIT_SET(MC_serverBD_4.iTeamSwapLockFlag, iLocalPart)
			IF MC_PlayerBD[iLocalPart].bSwapTeamFinished
				MC_PlayerBD[iLocalPart].bSwapTeamStarted = FALSE
				MC_PlayerBD[iLocalPart].bSwapTeamFinished = FALSE
				
				PROCESS_IMMEDIATE_TEAM_SWAP_LOGIC()
				
				PRINTLN("[MMacK][TeamSwaps] SWAP COMPLETE")
			ELSE
				IF MC_PlayerBD[iLocalPart].bSwapTeamStarted
					IF HAS_NET_TIMER_STARTED(tdTeamSwapFailSafe)
						IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdTeamSwapFailSafe) >= ciTIMESTAMP_FOR_TEAM_SWAP
							MC_PlayerBD[iLocalPart].bSwapTeamStarted = FALSE
							MC_PlayerBD[iLocalPart].bSwapTeamFinished = FALSE
							PRINTLN("[MMacK][TeamSwaps] SAFETY BAIL, normally because we killed whilst waiting for a server response for another kill which was rejected")
						ENDIF
					ELSE
						REINIT_NET_TIMER(tdTeamSwapFailSafe)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF HAS_NET_TIMER_STARTED(tdTeamSwapFailSafe)
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdTeamSwapFailSafe) >= ciTIMESTAMP_FOR_TEAM_SWAP
					MC_PlayerBD[iLocalPart].bSwapTeamStarted = TRUE
					MC_PlayerBD[iLocalPart].bSwapTeamFinished = TRUE
					PRINTLN("[MMacK][TeamSwaps] SAFETY CLEANUP, the server thinks we are swapping, but we dont. Lets pretend we are clear to cleanup server")
				ENDIF
			ELSE
				REINIT_NET_TIMER(tdTeamSwapFailSafe)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SET_SPAWN_VEHICLE_STARTING_VELOCITY_BACKUP()
	IF NOT IS_BIT_SET(iLocalBoolCheck5, LBOOL5_GIVEN_SPAWN_VEHICLE_STARTING_VELOCITY) // This is here as a backup, so players aren't left frozen in their spawn vehicles:
		IF IS_BIT_SET(iLocalBoolCheck5, LBOOL5_SPAWN_TEAM_IN_VEHICLE)
		AND NOT (IS_PLAYER_SCTV(LocalPlayer) OR DID_I_JOIN_MISSION_AS_SPECTATOR())
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] GAME_STATE_RUNNING call to UNFREEZE_AND_PUNT_PLAYER_SPAWN_VEHICLE_FORWARDS!")
			IF IS_PED_IN_ANY_HELI(localPlayerPed)
				UNFREEZE_AND_PUNT_PLAYER_SPAWN_VEHICLE_FORWARDS(1.0)
				PRINTLN("[RCC MISSION] GAME_STATE_RUNNING - Calling SET_VEHICLE_FORWARD_SPEED with 1.0")
			ELSE
				UNFREEZE_AND_PUNT_PLAYER_SPAWN_VEHICLE_FORWARDS(60.0)
			ENDIF
		ENDIF
		SET_BIT( iLocalBoolCheck5, LBOOL5_GIVEN_SPAWN_VEHICLE_STARTING_VELOCITY )
	ENDIF
ENDPROC

FUNC BOOL PROCESS_MISSION_COUNTDOWN_TIMER()
	IF NOT IS_BIT_SET(iLocalBoolCheck21, LBOOL21_PREVENT_COUNTDOWN_FROM_STARTING)
		IF NOT (IS_PLAYER_SCTV(LocalPlayer) OR DID_I_JOIN_MISSION_AS_SPECTATOR())
			
			INT iTeam = MC_playerBD[iPartToUse].iteam
			INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
			
			IF NOT IS_COUNTDOWN_OK_TO_PROCEED()				
				IF DOES_THIS_MODE_HAVE_NO_TIME_LIMIT() // The radar is meant to be hidden until timers have started in VS modes, but if there is no timer it gets confused
				AND NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_EXCLUDE_FROM_3_2_1_GO_COUNTDOWN)
					PRINTLN("[TMS][PROCESS_MISSION_COUNTDOWN_TIMER] Blocking HUD and radar during countdown to DOES_THIS_MODE_HAVE_NO_TIME_LIMIT()")
					HIDE_HUD_AND_RADAR_THIS_FRAME()							
				ENDIF
				
				IF NOT g_bDisableVehicleMines
				AND NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_EXCLUDE_FROM_3_2_1_GO_COUNTDOWN)
				AND NOT IS_BITMASK_AS_ENUM_SET(cduiIntro.iBitFlags, CNTDWN_UI2_Played_Go)
					DISABLE_VEHICLE_MINES(TRUE)
					PRINTLN("[LM][PROCESS_MISSION_COUNTDOWN_TIMER] - LIMIT RACE CONROLS - Disabling Vehicle Mines.")
				ENDIF
				
				IF (Is_Player_Currently_On_MP_Versus_Mission(LocalPlayer) OR IS_FAKE_MULTIPLAYER_MODE_SET())
				AND iRule < FMMC_MAX_RULES
				AND  (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRolesBitset[iRule][MC_playerBD[iLocalPart].iCoronaRole], ciBS_ROLE_SHOW_STOP_START)
				AND  (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRolesBitset[iRule][MC_playerBD[iLocalPart].iCoronaRole], ciBS_ROLE_DISABLE_CONTROL) 
				OR    IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRolesBitset[iRule][MC_playerBD[iLocalPart].iCoronaRole], ciBS_ROLE_SET_AS_GHOST)))
					IF NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_EXCLUDE_FROM_3_2_1_GO_COUNTDOWN)
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63VsShardMessage[0], DEFAULT, DEFAULT, ROUND(ciFM_EVENTS_END_UI_SHARD_TIME * 2.5))
					ENDIF
				ELSE
					PRINTLN("[PROCESS_MISSION_COUNTDOWN_TIMER] LIMIT RACE CONROLS - Processing Countdown.")
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_SUB_TEAM_ROLES)
						PROCESS_VERSUS_ROLES_GHOSTS(iTeam, iRule, MC_playerBD[iLocalPart].iCoronaRole)
					ENDIF
					
					IF NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_EXCLUDE_FROM_3_2_1_GO_COUNTDOWN)
						DISPLAY_INTRO_COUNTDOWN(cduiIntro)
						RENDER_SPAWN_PROTECTION_SPHERES_EARLY()
						IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
						AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
							IF bTRCamStarted
								PROCESS_SCRIPT_CAMS()
							ENDIF
						ENDIF
					ENDIF
					
					//Allow for some player control during the race countdown
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciRACE_STYLE_INTRO)	
					AND NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_EXCLUDE_FROM_3_2_1_GO_COUNTDOWN)
						IF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_STARTED_COUNTDOWN)
							IF NOT IS_BITMASK_AS_ENUM_SET(cduiIntro.iBitFlags, CNTDWN_UI2_Played_Go)
								IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
									
									// Due to Hover Mode issues.
									IF NOT IS_PED_INJURED(localPlayerPed)
									AND IS_PED_IN_ANY_VEHICLE(localPlayerPed)
									AND GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(localPlayerPed)) = DELUXO
										NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE)
									ENDIF
									
									PRINTLN("[PROCESS_MISSION_COUNTDOWN_TIMER] LIMIT RACE CONROLS - DISABLE_CONTROL_THIS_FRAME_ENABLE_HORN_AND_HOOD")
									DISABLE_CONTROL_THIS_FRAME_ENABLE_HORN_AND_HOOD(FALSE,IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciMISSION_ALLOW_REV_ON_COUNTDOWN),FALSE,TRUE,IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciLBD_ALLOW_HYDRO_FOR_COUNTDOWN))
									bLocalHandBrakeOnFlag = TRUE
								ENDIF
								
								IF IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
									PRINTLN("[PROCESS_MISSION_COUNTDOWN_TIMER] LIMIT RACE CONROLS - Preventing Spectator Powerups This Frame.")
								ENDIF
							ELSE										
								PRINTLN("[PROCESS_MISSION_COUNTDOWN_TIMER] LIMIT RACE CONROLS - SBBOOL2_FINISHED_COUNTDOWN")
								SET_VEHICLE_HANDBRAKE_STATE(FALSE)
								bLocalHandBrakeOnFlag = FALSE
							ENDIF
						ELSE
							PRINTLN("[PROCESS_MISSION_COUNTDOWN_TIMER] LIMIT RACE CONROLS - SBBOOL2_STARTED_COUNTDOWN not set")
						ENDIF
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciRACE_BOOST_START) 
							IF HAS_NET_TIMER_STARTED(g_FMMC_STRUCT.stBoostTimer)
								DO_VEH_BOOST()
							ENDIF
							UPDATE_VEHICLE_BOOSTING()
						ENDIF
					ENDIF

				ENDIF
				
				IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
				OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType) AND NOT IS_BIT_SET(iLocalBoolCheck21, LBOOL21_TINY_RACERS_INITIAL_COUNTDOWN_OVER)
					RETURN FALSE
				ENDIF
			ELSE
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciRACE_STYLE_INTRO)	
					IF NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_DISPLAYED_INTRO_SHARD)
						IF IS_BITMASK_AS_ENUM_SET(cduiIntro.iBitFlags, CNTDWN_UI2_Played_Go) 
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetTwelve, ciENABLE_TINY_RACERS_CHECKPOINT_LOGIC)
								IF NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_EXCLUDE_FROM_3_2_1_GO_COUNTDOWN)
								AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_Reset_321GO_OnRoundRestart)
								AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_GAMES_MASTERS(g_FMMC_STRUCT.iAdversaryModeType) 
									RELEASE_COUNTDOWN_UI(cduiIntro)
									CLEAN_COUNTDOWN(cduiIntro)
								ENDIF
								SET_BIT(iLocalBoolCheck13, LBOOL13_DISPLAYED_INTRO_SHARD)
							ENDIF
						ENDIF
						
						IF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_STARTED_COUNTDOWN)
						AND cduiIntro.iBitFlags != 0
							//if we have a lag issue on countdown, this might not cleanup properly
							
							IF NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_EXCLUDE_FROM_3_2_1_GO_COUNTDOWN)
								IF bLocalHandBrakeOnFlag = TRUE
									SET_VEHICLE_HANDBRAKE_STATE(FALSE)
									PRINTLN("[PROCESS_MISSION_COUNTDOWN_TIMER] LIMIT RACE CONROLS - SET_VEHICLE_HANDBRAKE_STATE - set to false")
									bLocalHandBrakeOnFlag = FALSE
								ENDIF
								IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
									//NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
								ENDIF
							ENDIF
							
							IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
								SET_BIT(iLocalBoolCheck21, LBOOL21_TINY_RACERS_INITIAL_COUNTDOWN_OVER)
							ENDIF
							
							IF bIsLocalPlayerHost
								RESET_NET_TIMER(MC_serverBD.timeServerCountdown)
								SET_BIT(MC_serverBD.iServerBitSet2, SBBOOL2_FINISHED_COUNTDOWN)
							ENDIF
						ENDIF
						
						IF NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_DISPLAYED_INTRO_SHARD)
						AND NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_EXCLUDE_FROM_3_2_1_GO_COUNTDOWN)
							SHOW_GO_SPLASH(cduiIntro)
							IF g_bDisableVehicleMines
								DISABLE_VEHICLE_MINES(FALSE)
								PRINTLN("[LM][PROCESS_MISSION_COUNTDOWN_TIMER] - LIMIT RACE CONROLS - Re-Enabling Vehicle Mines.")
							ENDIF
						ENDIF
						
						IF (iRule < FMMC_MAX_RULES)
						AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRolesBitset[iRule][MC_playerBD[iLocalPart].iCoronaRole], ciBS_ROLE_SHOW_STOP_START)
						AND NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_SHOWN_TEAM_SHARD)
						AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciDISABLE_TEAM_VERSUS_SHARD)
							IF NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_EXCLUDE_FROM_3_2_1_GO_COUNTDOWN)
								SHOW_TEAM_SHARD_AT_START_OF_VS_MISSION()
							ENDIF
							SET_BIT(iLocalBoolCheck13, LBOOL13_SHOWN_TEAM_SHARD) //if there's a massive lag on GO, this can happen twice
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bLocalHandBrakeOnFlag = TRUE
		IF SET_VEHICLE_HANDBRAKE_STATE(FALSE)
			PRINTLN("[PROCESS_MISSION_COUNTDOWN_TIMER] LIMIT RACE CONROLS - SET_VEHICLE_HANDBRAKE_STATE - set to false - bLocalHandBrakeOnFlag = FALSE")
			bLocalHandBrakeOnFlag = FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC 

PROC PROCESS_YACHT_FOR_CUTSCENE()
	IF IS_BIT_SET(MC_serverBD.iServerBitSet7, SBBOOL7_YACHT_CUTSCENE_RUNNING)
	OR GlobalplayerBD[iPartToUse].PrivateYachtDetails.iWarpState > 0
		UPDATE_FMMC_YACHT_WARPING(iYachtScene, &PLAY_YACHT_SCENE)
	ELSE
		#IF IS_DEBUG_BUILD
		IF bMissionYachtDebug
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD4)
			AND IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD5)
				BROADCAST_FMMC_MOVE_FMMC_YACHT(iLocalPart)
			ENDIF
		ENDIF
		#ENDIF
	ENDIF
ENDPROC

PROC PROCESS_INIT_TRIP_SKIP_DATA_FOR_LOCAL_PLAYER()
	
//	// -1 means unassigned by MC yet. Any other value is assigned.
//	IF GET_CURRENT_HEIST_COMPLETION_STAT_FOR_LEADER() = -1
//		EXIT
//	ENDIF
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		INIT_TRIP_SKIP(MC_playerBD[iLocalPart].TSPlayerData, IS_FAKE_MULTIPLAYER_MODE_SET())			
	
	ELIF AM_I_ON_A_HEIST()
	
		INIT_TRIP_SKIP(MC_playerBD[iLocalPart].TSPlayerData, GET_MP_INT_CHARACTER_STAT(GET_HEIST_MISSION_COMPLETION_STAT(GET_CURRENT_HEIST_MISSION())) > 0)
		PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_INI] TRIP SKIP - INIT_TRIP_SKIP - GET_MP_INT_CHARACTER_STAT(GET_HEIST_MISSION_COMPLETION_STAT(GET_CURRENT_HEIST_MISSION())) = ", GET_MP_INT_CHARACTER_STAT(GET_HEIST_MISSION_COMPLETION_STAT(GET_CURRENT_HEIST_MISSION())))
		PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_INI] TRIP SKIP - INIT_TRIP_SKIP - GET_CURRENT_HEIST_MISSION() = ", GET_CURRENT_HEIST_MISSION())
		
	ELIF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()	
		BOOL bCompletedBefore
		bCompletedBefore = IS_BIT_SET(GET_MP_INT_CHARACTER_STAT(MP_STAT_GANGOPS_FLOW_PASSED_BITSET), GANGOPS_FLOW_GET_GANG_OPS_MISSION_CONST_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash))
		INIT_TRIP_SKIP(MC_playerBD[iLocalPart].TSPlayerData, bCompletedBefore)
		PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_INI] TRIP SKIP - INIT_TRIP_SKIP - Gang Ops Heist - bCompletedBefore = ", GET_STRING_FROM_BOOL(bCompletedBefore))
		PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_INI] TRIP SKIP - INIT_TRIP_SKIP - Gang Ops Heist - GANGOPS_FLOW_GET_GANG_OPS_MISSION_CONST_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash) = ", GANGOPS_FLOW_GET_GANG_OPS_MISSION_CONST_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash))
		
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_EnableTripSkipForMission)		
		INIT_TRIP_SKIP(MC_playerBD[iLocalPart].TSPlayerData, GET_MP_INT_CHARACTER_STAT(GET_HEIST_MISSION_COMPLETION_STAT(GET_CURRENT_HEIST_MISSION())) > 0)
		PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_INI] TRIP SKIP - INIT_TRIP_SKIP - bCompletedBefore = ", GET_MP_INT_CHARACTER_STAT(GET_HEIST_MISSION_COMPLETION_STAT(GET_CURRENT_HEIST_MISSION())) > 0)
		
	ENDIF
ENDPROC

PROC PROCESS_CLIENT()

	INT iParticipant
	PARTICIPANT_INDEX tempPart
	BOOL bAnimFinished
	
	g_bMissionClientGameStateRunning = FALSE
	
	// This must be called every frame (and before main state) in order to
	// disallow people from leaving vehicles during the intro cutscene
	PROCESS_DISABLED_CONTROLS()
	#IF IS_DEBUG_BUILD
    #IF SCRIPT_PROFILER_ACTIVE 
        ADD_SCRIPT_PROFILE_MARKER("PROCESS_DISABLED_CONTROLS")
    #ENDIF
    #ENDIF
	
	ADD_MISSION_JOB_POINTS_ROUNDS()
	#IF IS_DEBUG_BUILD
    #IF SCRIPT_PROFILER_ACTIVE 
        ADD_SCRIPT_PROFILE_MARKER("ADD_MISSION_JOB_POINTS_ROUNDS")
    #ENDIF
    #ENDIF

	BLOCK_ACTION_MODE_OPTION(jobIntroData)
	#IF IS_DEBUG_BUILD
    #IF SCRIPT_PROFILER_ACTIVE 
        ADD_SCRIPT_PROFILE_MARKER("BLOCK_ACTION_MODE_OPTION")
    #ENDIF
    #ENDIF
		
	FREEZE_OPPRESSOR_FOR_DATA_BREACH()
	
	PROCESS_CLIENT_PRE_RUNNING()
	
	UNFREEZE_ENTITIES_AT_THE_START_OF_MISSION_IF_NECESSARY()
		
	PROCESS_CLIENT_BLIMP_SIGNS()
	
	// let pv system know when its safe to create vehicle.
	IF (MC_ServerBD.iHeistPresistantPropertyID > 0)
	AND NOT GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		SET_PERSONAL_VEHICLE_HEIST_PROPERTY_SPAWN(MC_ServerBD.iHeistPresistantPropertyID, IS_THIS_A_QUICK_RESTART_JOB())
	ENDIF
	
	IF CONTENT_IS_USING_ARENA()
		PROCESS_UGC_ARENA(sArenaSoundsInfo, !g_bMissionEnding, MC_serverBD_3.iArena_Lighting)
	ENDIF
	
	PROCESS_START_MISSION_WALKING()
	
	PROCESS_YACHT_FOR_CUTSCENE()
		
	PROCESS_TRADING_PLACES_END_TEAM_SWAP()
	
	MAINTAIN_PRISON_AMBIENCE_STATE()
		
	DISABLE_RUINER_ROCKETS()
	
 	PROCESS_BEFORE_GAME_STATE_RUNNING()	
	
	PROCESS_RENDERPHASE_UNPAUSE_WITH_DELAY()
	
	PROCESS_ARENA_CROWD_CHEERING_FOR_MISSION()
	
	PROCESS_CASINO_HEIST_FUNCTIONALITY()
	
	PROCESS_PLAYER_STOLEN_GOODS_SLOW_DOWN()
	
	PROCESS_SHOWING_AIRLOCK_OBJECTIVE()
	
	PROCESS_RESET_AIRLOCK_HUD_VARS()
	
	PROCESS_INIT_TRIP_SKIP_DATA_FOR_LOCAL_PLAYER()
	
	PROCESS_DISABLE_REALTIME_MULTIPLAYER()
	
	SWITCH MC_playerBD[iLocalPart].iGameState
		
		// Wait until the server gives the all go before moving on.
		CASE GAME_STATE_INI				
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("start game state ini")
			#ENDIF
			#ENDIF			
									
			LOAD_MC_TEXT_BLOCK()
			
			LOAD_PARTICLE_FX()
			
			LOAD_ACTION_ANIMS()
			
			SET_UP_PERSONAL_VEHICLE_CREATION_FOR_MISSION()
			
			IF IS_PLAYER_SCTV(LocalPlayer)
				IF CONTENT_IS_USING_ARENA()
					DISABLE_SPECTATOR_FILTER_OPTION(TRUE)
				ENDIF
			ENDIF
						
			IF IS_PED_SWIMMING(GET_PLAYER_PED(LocalPlayer))
				CLEAR_PED_TASKS(GET_PLAYER_PED(LocalPlayer))
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciDISABLE_TRAINS_AND_TRAMS)
				SET_RANDOM_TRAINS( FALSE )
				CPRINTLN( DEBUG_SIMON, "[PROCESS_CLIENT][GAME_STATE_INI] - Disabling SET_RANDOM_TRAINS" )
			ENDIF

			g_bEndOfMissionCleanUpHUD = FALSE
			g_bCelebrationScreenIsActive = FALSE
			g_bIgnoreCelebrationScreenPV = FALSE
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciRACE_STYLE_INTRO)
				REQUEST_COUNTDOWN_UI(cduiIntro)
			ENDIF
			
			// AMEC HEISTS
			// Maintain scripted cutscene.
			IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
			OR Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
				IF NOT IS_THIS_A_QUICK_RESTART_JOB()
				AND NOT IS_CORONA_INITIALISING_A_QUICK_RESTART()
					IF NOT HAS_FIRST_STRAND_MISSION_BEEN_PASSED()
						IF NOT IS_PLAYER_SCTV(PLAYER_ID())
						
							MAINTAIN_HEIST_EXIT_SCENE(sPedVariations,sApplyOutfitData,bTriggerSuspense)
						
							IF NOT IS_BIT_SET(iLocalBoolCheck3,LBOOL3_CLEANED_UP_CAM)
								SET_BIT(iLocalBoolCheck3,LBOOL3_CLEANED_UP_CAM)
								CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[PROCESS_CLIENT][GAME_STATE_INI] - Setting LBOOL3_CLEANED_UP_CAM.")
							ENDIF
							
							IF NOT IS_BIT_SET(iLocalBoolCheck3,LBOOL3_MUSIC_PLAYING)
								SET_BIT(iLocalBoolCheck3,LBOOL3_MUSIC_PLAYING)
								CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[PROCESS_CLIENT][GAME_STATE_INI] - Setting LBOOL3_MUSIC_PLAYING.")
							ENDIF
							
							IF SHOULD_MOVE_TO_NON_APARTMENT_INTRO()
							AND GET_HEIST_EXIT_CUTS_FLOW() = HEIST_CUTS_FLOW_STATE_FINISHED
								MAINTAIN_HEIST_EXIT_SCENE(sPedVariations,sApplyOutfitData,bTriggerSuspense)
								g_TransitionSessionNonResetVars.bCanWeHandToMissionControl = TRUE
							ENDIF
							
						ENDIF
					ELSE
						CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[PROCESS_CLIENT][GAME_STATE_INI] = TRUE, not doing cutscene.")
					ENDIF
												
				ELSE
					CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[PROCESS_CLIENT][GAME_STATE_INI] - IS_THIS_A_QUICK_RESTART_JOB() = TRUE, don't play cutscene.")
				ENDIF
			ENDIF
			
			//Hide every thing
			HIDE_ALL_MISSION_ENTITIES_LOCALLY_FOR_STRAND_MISSION(MC_serverBD_1.sFMMC_SBD, oiProps, oiPropsChildren)
			
			//Wait till the flags are ready for the script to progress			
			IF NOT IS_BIT_SET(iLocalBoolCheck8, LBOOL8_REQUEST_LOAD_FMMC_MODELS_STAGGERED_DONE)
			AND IS_A_STRAND_MISSION_BEING_INITIALISED()
				IF NOT bIsLocalPlayerHost // If we're the server, this stuff will happen in the server version of this down below (RUN_ALL_STAGGERED_MC_HOST_FUNCTIONS)
					PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_INI] REQUEST_LOAD_FMMC_MODELS_STAGGERED being called")
					IF RUN_ALL_STAGGERED_MC_CLIENT_FUNCTIONS( iRelGroupStaggerVars, iNumTimesCalledRelGroupFunc )
						CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][PROCESS_CLIENT][GAME_STATE_INI] RUN_ALL_STAGGERED_MC_CLIENT_FUNCTIONS RETURNING TRUE") 
						SET_BIT(iLocalBoolCheck8, LBOOL8_REQUEST_LOAD_FMMC_MODELS_STAGGERED_DONE)
					ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][PROCESS_CLIENT][GAME_STATE_INI] Not calling RUN_ALL_STAGGERED_MC_CLIENT_FUNCTIONS RETURNING, we're the server") 
				#ENDIF
				ENDIF
			ELSE
				
				IF NOT IS_PLAYER_SCTV(LocalPlayer)
				AND NOT DID_I_JOIN_MISSION_AS_SPECTATOR()					
					IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
						g_sFMMCEOM.vWarp = <<364.9413,278.7449,102.2490>> //g_FMMC_STRUCT.vStartPos
						PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_INI][dsw] Set g_sFMMCEOM.vWarp = ", g_sFMMCEOM.vWarp)
					ENDIF
					IF IS_SKYSWOOP_AT_GROUND()
						IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
							CLEAN_UP_INTRO_STAGE_CAM(g_sTransitionSessionData.ciCam,-1,TRUE)
							IF NOT IS_CORONA_FREEZE_AND_START_FX_SET()
								SETUP_FAKE_CAMERA_PAUSE(jobIntroData)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
		
				IF bLocalPlayerPedOK
					SET_PED_RESET_FLAG(LocalPlayerPed, PRF_CannotBeTargetedByAI, TRUE)
				ENDIF
				
				THEFEED_HIDE_THIS_FRAME()
				PROCESS_FREEZE_MICROPHONE_AT_PLAYER_FOR_MISSION()
				PROCESS_SCRIPT_INIT()
				SET_BIT(MC_playerBD[iLocalPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_INI)

			ENDIF
						
			INITIALISE_ARENA_WAR_TELEMETRY()
			
			IF IS_AN_AIRLOCK_TRANSITION_ENDING_OR_STARTING()
				PROCESS_AIRLOCK_BOTTOM_RIGHT_HUD()
			ENDIF
				
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("end game state ini")
			#ENDIF
			#ENDIF		
		BREAK
		
		CASE GAME_STATE_TUTORIAL_CUTSCENE
			IF DO_TUTORIAL_MISSION_INTRO_MOCAP()
			AND DO_TUTORIAL_MISSION_CUTSCENE()
				NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
				IF SHOULD_TRANSITION_SESSIONS_HIDE_RADAR_UNTILL_LAUNCHED()
					CLEAR_TRANSITION_SESSIONS_HIDE_RADAR_UNTILL_LAUNCHED()
				ENDIF
				DO_CORONA_POLICE_SETTINGS()
				SET_HIDOF_OVERRIDE(FALSE,FALSE,0,0,0,0)
				SET_MC_CLIENT_GAME_STATE(GAME_STATE_RUNNING)
			ENDIF
			IF IS_AN_AIRLOCK_TRANSITION_ENDING_OR_STARTING()
				PROCESS_AIRLOCK_BOTTOM_RIGHT_HUD()
			ENDIF
		BREAK
		
		CASE GAME_STATE_INTRO_CUTSCENE
			PROCESS_FREEZE_MICROPHONE_AT_PLAYER_FOR_MISSION()
			IF IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
				IF IS_PLAYER_CONTROL_ON(LocalPlayer)
					PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_INTRO_CUTSCENE] - ENTERED PROCESS_SCRIPTED_INTRO_CUTSCENE - Turning off control.")
					NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE)
				ENDIF
			ENDIF
			
			PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_INTRO_CUTSCENE][MMacK][IntroSync] GAME_STATE_INTRO_CUTSCENE")
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			SET_ALL_MISSION_ENTITIES_VISIBLITY(MC_serverBD_1.sFMMC_SBD, oiProps, oiPropsChildren, TRUE)			
			SET_PLAYER_INTO_COVER_AFTER_INITIAL_SPAWNING()			
			CORRECT_CAMERA_FROM_STARTING_IN_COVER()
			PRE_PROCESS_WORLD_PROPS()
				
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_START_IN_CUTSCENE)
				PROCESS_SCRIPTED_INTRO_CUTSCENE()
			ELSE
				IF IS_BIT_SET(iLocalBoolCheck3,LBOOL3_TEAM_INTRO)
					PROCESS_TEAM_CUTSCENE()
				ELSE
					PROCESS_INTRO_CUTSCENE()
				ENDIF
			ENDIF
			IF IS_AN_AIRLOCK_TRANSITION_ENDING_OR_STARTING()
				PROCESS_AIRLOCK_BOTTOM_RIGHT_HUD()
			ENDIF
		BREAK
		
		CASE GAME_STATE_APARTMENT_WALKOUT
			IF bApartmentIntroReady
				IF NOT HAS_NET_TIMER_STARTED(tdIntroTimer)
					REINIT_NET_TIMER(tdIntroTimer)
				ENDIF
				
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( tdIntroTimer ) > 2000
					IF SET_SKYSWOOP_DOWN()
						IF NOT IS_BIT_SET( GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_EXITING_PROPERTY) 
							
							CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][PROCESS_CLIENT][GAME_STATE_APARTMENT_WALKOUT] Moving to Camera Blend from Apartment Walkout" )
							SET_GAMEPLAY_CAM_RELATIVE_HEADING()
							DO_CORONA_POLICE_SETTINGS()
							CHECK_EMERGENCY_CLEANUP()
							SET_MC_CLIENT_GAME_STATE( GAME_STATE_CAMERA_BLEND )
							bApartmentIntroReady = FALSE
						ELSE
							CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][PROCESS_CLIENT][GAME_STATE_APARTMENT_WALKOUT] Conor's flag is already set - we've somehow already walked out" )
						ENDIF
					ELSE
						CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][PROCESS_CLIENT][GAME_STATE_APARTMENT_WALKOUT] Waiting for the skycam down to finish on a QR that has an apartment walkout" )
					ENDIF
				ELSE
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][PROCESS_CLIENT][GAME_STATE_APARTMENT_WALKOUT] Waiting for 2 seconds to pass before pulling skycam down" )
				ENDIF
			ELSE
				IF NOT g_b_ReadyToWalkOutApartmentForQuickRestart
					IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlaceInApartment
							PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_APARTMENT_WALKOUT][MMacK] UNPACK_INVITED_DATA_INT")
							g_b_ReadyToWalkOutApartmentForQuickRestart = TRUE
							g_b_TriggerLaunchInteriorQuickRestart = TRUE
							UNPACK_INVITED_DATA_INT(g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPackedInt, 
													g_TransitionSessionNonResetVars.sPostMissionCleanupData.iInvitedPropertyNum, 
													g_TransitionSessionNonResetVars.sPostMissionCleanupData.iInvitedPropertyInstance, 
													g_TransitionSessionNonResetVars.sPostMissionCleanupData.iInvitedPropertyEntrance 
													 ,g_TransitionSessionNonResetVars.sPostMissionCleanupData.iApartmentCustomVariation )
													
					ELSE
						PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_APARTMENT_WALKOUT][MMacK] Interior Is Ready - Seeing Me Means There Is A Serious Problem")
					ENDIF
				ELSE
					PLAYER_INDEX tempPlayer
					bApartmentIntroReady = TRUE
					
					REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iparticipant
						tempPart = INT_TO_PARTICIPANTINDEX(iparticipant)
						PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_APARTMENT_WALKOUT][MMacK] Checking Participant ", iparticipant)
						IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
							PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_APARTMENT_WALKOUT][MMacK] Participant Active")
							tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
							
							IF NOT IS_PLAYER_SCTV(tempPlayer)
								PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_APARTMENT_WALKOUT][MMacK] Participant not SCTV")
								IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(tempPlayer)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_USING_PROPERTY)//IS_PLAYER_IN_MP_PROPERTY(tempPlayer, FALSE)
									PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_APARTMENT_WALKOUT][MMacK] Participant Not In Property")
									bApartmentIntroReady = FALSE 
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
					
					IF bApartmentIntroReady //we are ready, lets come down from the sky. 
						RESET_NET_TIMER(tdIntroTimer)
						PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_APARTMENT_WALKOUT][MMacK] Setting Skyswoop Down")
						SET_SKYSWOOP_DOWN(DEFAULT, TRUE, TRUE, TRUE, FALSE, TRUE, FALSE, TRUE)
					ENDIF
					
					IF (IS_SKYSWOOP_MOVING() OR IS_SKYSWOOP_AT_GROUND())
					AND bApartmentIntroReady
						g_b_AllPlayersReadyForApartmentQuickRestart = bApartmentIntroReady
					ELSE
						bApartmentIntroReady = FALSE
					ENDIF
				ENDIF
			ENDIF
			IF IS_AN_AIRLOCK_TRANSITION_ENDING_OR_STARTING()
				PROCESS_AIRLOCK_BOTTOM_RIGHT_HUD()
			ENDIF
		BREAK
		
		CASE GAME_STATE_FORCED_TRIP_SKIP
			//HOST ONLY
			IF bIsLocalPlayerHost
				//SET CAR
				IF MC_serverBD_3.FTSServerData.viCar = NULL
					IF MC_serverBD_3.FTSServerData.iStage > TSS_INIT
						IF DOES_ENTITY_EXIST(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[MC_serverBD_3.FTSServerData.iCar]))
							MC_serverBD_3.FTSServerData.viCar = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[MC_serverBD_3.FTSServerData.iCar])
							PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_FORCED_TRIP_SKIP] FORCED TRIP SKIP - viCar SET TO VEHICLE ", MC_serverBD_3.FTSServerData.iCar, " - CarID = ", NATIVE_TO_INT(MC_serverBD_3.FTSServerData.viCar))
						ELSE
							PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_FORCED_TRIP_SKIP] FORCED TRIP SKIP - CAR DOESN'T YET EXIST - iCar = ", MC_serverBD_3.FTSServerData.iCar)
						ENDIF
					ENDIF
				ENDIF
				
				//PRE LOOP
				PRE_TRIP_SKIP_SERVER_LOOP(MC_serverBD_3.FTSServerData, MC_serverBD.iNumberOfPart[0] + MC_serverBD.iNumberOfPart[1] + MC_serverBD.iNumberOfPart[2] + MC_serverBD.iNumberOfPart[3])
				//LOOP
				INT iTripSkipPart
				REPEAT NUM_NETWORK_PLAYERS iTripSkipPart
					IF NETWORK_IS_PARTICIPANT_ACTIVE(temppart)
						MAINTAIN_TRIP_SKIP_SERVER_LOOP(MC_serverBD_3.FTSServerData, MC_playerBD[iTripSkipPart].FTSPlayerData, iTripSkipPart, IS_BIT_SET(MC_playerBD[iTripSkipPart].iClientBitSet, PBBOOL_HEIST_HOST))
					ENDIF
				ENDREPEAT
				POST_TRIP_SKIP_SERVER_LOOP(MC_serverBD_3.FTSServerData, TRUE, TRUE)
			ENDIF
			
			//MAIN
			MAINTAIN_FORCED_TRIP_SKIP_CLIENT(MC_serverBD_3.FTSServerData, MC_playerBD[PARTICIPANT_ID_TO_INT()].FTSPlayerData, FTSLocalData, bHasTripSkipJustFinished)
			IF bIsLocalPlayerHost
				MAINTAIN_FORCED_TRIP_SKIP_SERVER(MC_serverBD_3.FTSServerData)
			ENDIF
			
			//DONE CHECK
			IF MC_playerBD[PARTICIPANT_ID_TO_INT()].FTSPlayerData.iStage >= TSS_FINISHED
				PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_FORCED_TRIP_SKIP] FORCED TRIP SKIP - DONE - GO TO GAME STATE RUNNING")
				SET_MC_CLIENT_GAME_STATE(GAME_STATE_RUNNING)
			ENDIF
			IF IS_AN_AIRLOCK_TRANSITION_ENDING_OR_STARTING()
				PROCESS_AIRLOCK_BOTTOM_RIGHT_HUD()
			ENDIF
		BREAK
						
		CASE GAME_STATE_HEIST_INTRO
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			CLEAR_AREA_OF_PEDS(g_vTeamStartPos, 50)
			IF SHOULD_MOVE_TO_NON_APARTMENT_INTRO()
				REQUEST_NEW_INTRO_ANIMATION_DATA(jobIntroData)
			ELSE
				REQUEST_ANIMATIONS_FOR_HEIST_INTRO(jobIntroData, MC_playerBD[iPartToUse].iTeam)
			ENDIF
			
			REQUEST_PHONE_MOD()
		
			HIDE_HUD_AND_RADAR_THIS_FRAME()
				
			IF HAS_SERVER_CHECKED_ALL_CLIENTS_READY_FOR_INTRO()
									
				MC_playerBD[PARTICIPANT_ID_TO_INT()].iSyncRoundCutsceneStage = jobIntroData.sNewAnimData.eStage 
				MC_playerBD[PARTICIPANT_ID_TO_INT()].iSyncRoundIntroCutsceneStage = jobIntroData.jobIntroStage
				MC_playerBD[PARTICIPANT_ID_TO_INT()].iSyncRoundTeamCutsceneStage = jobIntroData.jobTeamCutStage						
				MC_playerBD[PARTICIPANT_ID_TO_INT()].iSyncRoundPostCutsceneStage = GET_SKYSWOOP_STAGE()
				MC_playerBD[PARTICIPANT_ID_TO_INT()].bSyncCutsceneDone = jobIntroData.bReadyToMoveIntoGameplay
			
				IF GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
	
					IF IS_EVERYONE_READY_FOR_NEXT_ROUND()
						ARE_ALL_PLAYERS_READY_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION()
					ENDIF	
			
					IF DOES_CAM_EXIST(jobIntroData.jobIntroCam)
					AND IS_EVERYONE_READY_FOR_NEXT_ROUND()
					AND HAS_SERVER_TIME_PASSED_ROUND(6000)
						IF ARE_ALL_PLAYERS_READY_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION()
							SET_SKYSWOOP_DOWN(jobIntroData.jobIntroCam, DEFAULT, TRUE, DEFAULT, DEFAULT, DEFAULT, FALSE)
						ELSE
							PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_HEIST_INTRO][ROUNDCAM] GAME_STATE_HEIST_INTRO ARE_ALL_PLAYERS_READY_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION = FALSE, wait ")
						ENDIF
					ENDIF

					IF HAS_NET_TIMER_EXPIRED(RoundCutsceneTimeOut, 35000, TRUE)
						PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_HEIST_INTRO][ROUNDCAM] GAME_STATE_HEIST_INTRO timed out, running skycamdown ")
						
						SET_SKYSWOOP_DOWN(jobIntroData.jobIntroCam, TRUE, TRUE, TRUE, FALSE, TRUE, TRUE)
					ENDIF
					
					IF iSyncRoundLastFrame != GET_SKYSWOOP_STAGE()
						IF IS_SKYCAM_ON_LAST_CUT()
						AND GET_SKYSWOOP_STAGE() != SKYSWOOP_NONE
							HAS_SERVER_TIME_PASSED_ROUND()
						ENDIF
					ENDIF
					iSyncRoundLastFrame = GET_SKYSWOOP_STAGE()

					PROCESS_HOLD_UP_LOGIC_BETWEEN_ROUNDS()
				ELSE
					PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_HEIST_INTRO][ROUNDCAM] g_b_HoldupSkycamBetweenRounds = FALSE & g_b_HoldupCutsceneBetweenRounds = FALSE GAME_STATE_HEIST_INTRO GAME_STATE_TEAM_PLAY GET_MC_SERVER_GAME_STATE() = GAME_STATE_END ")
					g_b_HoldupSkycamBetweenRounds = FALSE
					g_b_HoldupCutsceneBetweenRounds = FALSE
				ENDIF
				
				IF BUSYSPINNER_IS_ON()
					IF IS_SKYSWOOP_AT_GROUND()
						BUSYSPINNER_OFF()
					ENDIF
				ENDIF

				g_vTeamStartPos = <<0,0,0>>
				g_vTeamStartPos = CALCULATE_CENTRE_POINT_FOR_RESTART_ANIMS()
				PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_HEIST_INTRO][ROUNDCAM] GAME_STATE_HEIST_INTRO, g_vTeamStartPos = ", g_vTeamStartPos) 

				IF SHOULD_MOVE_TO_NON_APARTMENT_INTRO()
					g_vTeamStartPos = <<2455.7639, 4058.7334, 37.0647>> //need to make this NOT hard coded
					BOOL bAllReady
					IF IS_EVERYONE_READY_FOR_NEXT_ROUND()
						bAllReady = ARE_ALL_PLAYERS_READY_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION()
					ENDIF	
					
					MANAGE_NON_APARTMENT_INTRO_ANIMS(iIntroState,bAnimFinished, jobIntroData, IS_LOCAL_PLAYER_ANY_SPECTATOR(), bAllReady, bIsSmoker, niNetworkObj)
					
					IF bAnimFinished
						SET_HEIST_EXIT_CUTS_FLOW(HEIST_CUTS_FLOW_STATE_NONE)
						SET_ON_JOB_INTRO(TRUE)
						SET_PAUSE_INTRO_DROP_AWAY(FALSE)
						SET_MC_CLIENT_GAME_STATE(GAME_STATE_CAMERA_BLEND)
					ENDIF
				ELSE
					IF HAS_HEIST_STARTING_ANIM_FINISHED(jobIntroData, MC_playerBD[iPartToUse].iTeam)
					AND NOT ARE_HEIST_CAMS_INTERPOLATING(jobIntroData)
						SET_ON_JOB_INTRO(TRUE)
						SET_PAUSE_INTRO_DROP_AWAY(FALSE)
						SET_MC_CLIENT_GAME_STATE(GAME_STATE_CAMERA_BLEND)
						PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_HEIST_INTRO][CS_ANIM][ROUNDCAM] GAME_STATE_HEIST_INTRO, DONE ") 
					ELSE
						PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_HEIST_INTRO][ROUNDCAM] GAME_STATE_HEIST_INTRO, HAS_HEIST_STARTING_ANIM_FINISHED(FALSE), iQuickRestartAnim = ", g_FMMC_STRUCT.iQuickRestartAnim[MC_PlayerBD[iLocalPart].iTeam]) 
					ENDIF
				ENDIF
			ELSE
				MC_playerBD[PARTICIPANT_ID_TO_INT()].iSyncRoundPostCutsceneStage = SKYSWOOP_INSKYSTATIC
				PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_HEIST_INTRO][CS_ANIM][ROUNDCAM] GAME_STATE_HEIST_INTRO,, HAS_SERVER_CHECKED_ALL_CLIENTS_READY_FOR_INTRO = false ") 
			ENDIF
			IF IS_AN_AIRLOCK_TRANSITION_ENDING_OR_STARTING()
				PROCESS_AIRLOCK_BOTTOM_RIGHT_HUD()
			ENDIF
		BREAK
		
		CASE GAME_STATE_SOLO_PLAY
			IF IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
				IF IS_PLAYER_CONTROL_ON(LocalPlayer)
					PRINTLN("[PROCESS_CLIENT][GAME_STATE_SOLO_PLAY] - ENTERED GAME_STATE_SOLO_PLAY - Turning off control.")
					NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE)
				ENDIF
			ENDIF
			
			PROCESS_DISPLACEMENT_ARENA_INTERIOR_MC()
			
			CLEAR_PLAYER_WANTED_LEVEL(LocalPlayer)
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciPLAY_POWERPLAY_INTRO_ANNOUNCER)
				IF NOT IS_BIT_SET(iLocalBoolCheck17, LBOOL17_HAS_PLAYED_PP_INTRO_SFX)
					IF HAS_INTRO_SHARD_BEGUN()
						IF NOT PREPARE_MUSIC_EVENT("EXEC1_POWER_PLAY_OS")
							PRINTLN("[PROCESS_CLIENT][GAME_STATE_SOLO_PLAY] - POWERPLAY - HAS_INTRO_SHARD_BEGUN = EXEC1_POWER_PLAY_OS Loading...")
						ENDIF
						
						IF PREPARE_MUSIC_EVENT("EXEC1_POWER_PLAY_OS")
							SET_BIT(iLocalBoolCheck17, LBOOL17_HAS_PLAYED_PP_INTRO_SFX)
							TRIGGER_MUSIC_EVENT("EXEC1_POWER_PLAY_OS")
						ENDIF
					ELSE
						PRINTLN("[PROCESS_CLIENT][GAME_STATE_SOLO_PLAY] - HAS_INTRO_SHARD_BEGUN = FALSE")
						IF NOT PREPARE_MUSIC_EVENT("EXEC1_POWER_PLAY_OS")
							PRINTLN("[PROCESS_CLIENT][GAME_STATE_SOLO_PLAY] - POWERPLAY - HAS_INTRO_SHARD_BEGUN = EXEC1_POWER_PLAY_OS Loading...")
						ENDIF
					ENDIF
				ENDIF
			
				IF bIsLocalPlayerHost
					IF NOT IS_BIT_SET(iLocalBoolCheck17, LBOOL17_HAS_PICKED_PP_INTRO_ANNOUNCER)
						PICK_POWERPLAY_INTRO_ANNOUNCER()
					ENDIF
					IF NOT IS_BIT_SET(iLocalBoolCheck17, LBOOL17_HAS_PICKED_PP_PRE_INTRO_ANNOUNCER)
						PICK_POWERPLAY_PRE_INTRO_ANNOUNCER()
					ENDIF
					IF NOT IS_BIT_SET(iLocalBoolCheck17, LBOOL17_HAS_PICKED_PP_SUDDDEATH_ANNOUNCER)
						PICK_POWERPLAY_SUDDENDEATH_ANNOUNCER()
					ENDIF
				ENDIF
			ENDIF
			
			IF HAS_SERVER_CHECKED_ALL_CLIENTS_READY_FOR_INTRO()
				SET_ALL_MISSION_ENTITIES_VISIBLITY(MC_serverBD_1.sFMMC_SBD, oiProps, oiPropsChildren, TRUE)
				PRE_PROCESS_WORLD_PROPS()
				
				MC_playerBD[PARTICIPANT_ID_TO_INT()].iSyncRoundCutsceneStage = jobIntroData.sNewAnimData.eStage 
				MC_playerBD[PARTICIPANT_ID_TO_INT()].iSyncRoundIntroCutsceneStage = jobIntroData.jobIntroStage
				MC_playerBD[PARTICIPANT_ID_TO_INT()].iSyncRoundTeamCutsceneStage = jobIntroData.jobTeamCutStage						
				MC_playerBD[PARTICIPANT_ID_TO_INT()].iSyncRoundPostCutsceneStage = GET_SKYSWOOP_STAGE()
				MC_playerBD[PARTICIPANT_ID_TO_INT()].bSyncCutsceneDone = jobIntroData.bReadyToMoveIntoGameplay
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciPRE_GAME_EMP)
					g_bFMMCLightsTurnedOff = FALSE
				ENDIF
				
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
					IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
						PROCESS_CARGO_BOMBS()
					ENDIF
					
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
					DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_VEH_CIN_CAM)
					DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_VEH_CIN_CAM)
					SET_CINEMATIC_MODE_ACTIVE(FALSE)
				ENDIF
				
				IF GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
					
					IF IS_EVERYONE_READY_FOR_NEXT_ROUND()
						ARE_ALL_PLAYERS_READY_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION()
					ENDIF
			
					IF DOES_CAM_EXIST(jobIntroData.jobIntroCam)
					AND (IS_EVERYONE_READY_FOR_NEXT_ROUND() OR (DID_I_JOIN_MISSION_AS_SPECTATOR() AND CONTENT_IS_USING_ARENA()))
					AND HAS_SERVER_TIME_PASSED_ROUND(6000)
						IF ARE_ALL_PLAYERS_READY_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION()
							SET_SKYSWOOP_DOWN(jobIntroData.jobIntroCam)
							PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_SOLO_PLAY][ROUNDCAM] GAME_STATE_SOLO_PLAY calling SET_SKYSWOOP_DOWN ")
						ELSE
							PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_SOLO_PLAY][ROUNDCAM] GAME_STATE_SOLO_PLAY ARE_ALL_PLAYERS_READY_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION = FALSE, wait ")
						ENDIF
					ENDIF
					
					INT iServerTimeOut 
					iServerTimeOut = 25000
					IF IS_ARENA_WARS_JOB()
					AND IS_BIT_SET(MC_serverBD.iServerBitSet7, SBBOOL7_THIS_IS_NOT_ROUND_ONE)
						iServerTimeOut = 2500
					ENDIF
					IF HAS_NET_TIMER_EXPIRED(RoundCutsceneTimeOut, iServerTimeOut, TRUE)
						PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_SOLO_PLAY][ROUNDCAM] GAME_STATE_SOLO_PLAY timed out, running skycamdown iServerTimeOut: ", iServerTimeOut)
					
						SET_SKYSWOOP_DOWN(jobIntroData.jobIntroCam)
					ENDIF
					
					IF iSyncRoundLastFrame != GET_SKYSWOOP_STAGE()
						IF IS_SKYCAM_ON_LAST_CUT()
						AND GET_SKYSWOOP_STAGE() != SKYSWOOP_NONE
							HAS_SERVER_TIME_PASSED_ROUND()
						ENDIF
					ENDIF
					iSyncRoundLastFrame = GET_SKYSWOOP_STAGE()
					
					PROCESS_HOLD_UP_LOGIC_BETWEEN_ROUNDS()					
					IF IS_EVERYONE_READY_FOR_GAMEPLAY()
						PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_TEAM_PLAY][ROUNDCAM] IS_EVERYONE_READY_FOR_GAMEPLAY() = TRUE - Calling LOWER_CARGOBOB_RAMP_FOR_INTRO ")
						LOWER_CARGOBOB_RAMP_FOR_INTRO()
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_SOLO_PLAY][ROUNDCAM] g_b_HoldupSkycamBetweenRounds = FALSE GAME_STATE_SOLO_PLAY GAME_STATE_TEAM_PLAY GET_MC_SERVER_GAME_STATE() = GAME_STATE_END ")
					g_b_HoldupSkycamBetweenRounds = FALSE
					SET_MC_CLIENT_GAME_STATE(GAME_STATE_CAMERA_BLEND)
				ENDIF
				
				IF BUSYSPINNER_IS_ON()
					IF IS_SKYSWOOP_AT_GROUND()
						BUSYSPINNER_OFF()
					ENDIF
				ENDIF
				
				IF IS_ARENA_WARS_JOB()
					SET_RADIO_TO_STATION_NAME("OFF") 
				ENDIF
				IF CONTENT_IS_USING_ARENA()
					IF PROCESS_ARENA_SPAWN_FIX()
						IF NOT IS_LOCAL_PLAYER_SPAWNING_IN_ARENA_BOX_SEAT()
						AND NOT IS_PLAYER_IN_ARENA_BOX_SEAT()
						AND NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
							IF NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_SETUP_NEW_INTRO_COORDS)
								SET_BIT(iLocalBoolCheck30, LBOOL30_SETUP_NEW_INTRO_COORDS)
							ENDIF
						ENDIF
					ELSE
						PRINTLN("[PROCESS_CLIENT][GAME_STATE_SOLO_PLAY] - PROCESS_ARENA_SPAWN_FIX - Exiting")
						g_b_HoldupCutsceneBetweenRounds = TRUE
						EXIT
					ENDIF
					STOP_AUDIO_SCENE_FOR_ARENA_VIP_TRANSITION()
				ENDIF
							
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciPLAY_POWERPLAY_INTRO_ANNOUNCER)				
					IF NOT IS_BIT_SET(iLocalBoolCheck17, LBOOL17_HAS_PLAYED_PP_PRE_INTRO_ANNOUNCER)
						IF IS_INTRO_SHARD_AT_BOTTOM_OF_SCREEN(TRUE)
							PLAY_POWERPLAY_PRE_INTRO_ANNOUNCER()
						ELSE
							PRINTLN("[PROCESS_CLIENT][GAME_STATE_SOLO_PLAY] waiting on IS_INTRO_SHARD_AT_BOTTOM_OF_SCREEN")
						ENDIF
					ELSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						AND NOT IS_BIT_SET(iLocalBoolCheck17, LBOOL17_HAS_PLAYED_PP_INTRO_ANNOUNCER)
							PLAY_POWERPLAY_INTRO_ANNOUNCER()
						#IF IS_DEBUG_BUILD
						ELIF NOT IS_BIT_SET(iLocalBoolCheck17, LBOOL17_HAS_PLAYED_PP_INTRO_ANNOUNCER)
							PRINTLN("[PROCESS_CLIENT][GAME_STATE_SOLO_PLAY] PP ANNOUNCER - Waiting on pre-intro to stop before playing team intro")
						#ENDIF
						ENDIF
					ENDIF
				ENDIF
			
				IF PROCESS_DEATHMATCH_STARTING_CUT(jobIntroData) //PROCESS_JOB_OVERVIEW_CUT(jobIntroData,FALSE,FALSE,MC_playerBD[iLocalPart].iteam)
				OR (DID_I_JOIN_MISSION_AS_SPECTATOR() AND CONTENT_IS_USING_ARENA() AND HAS_SERVER_TIME_PASSED_ROUND(6000))
					PRINTLN("[RCC MISSION][ROUNDCAM] JOB INTRO DONE - MOVE TO RUNNING")
					IF SHOULD_TRANSITION_SESSIONS_HIDE_RADAR_UNTILL_LAUNCHED()
						CLEAR_TRANSITION_SESSIONS_HIDE_RADAR_UNTILL_LAUNCHED()
					ENDIF
					g_b_HoldupSkycamBetweenRounds = FALSE	// [NG-INTEGRATE] Next-gen only.
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciRACE_STYLE_INTRO)
					AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciDISABLE_TEAM_VERSUS_SHARD)
						SHOW_TEAM_SHARD_AT_START_OF_VS_MISSION()
					ENDIF
					SET_PLAYER_BLIPS_DISPLAY_AT_START_OF_VS_MISSION()
					RESET_NET_TIMER( RoundCutsceneTimeOut )	// [NG-INTEGRATE] Next-gen only.
					CLEANUP_CELEBRATION_SCALEFORMS( jobIntroData )
					
					IF IS_THIS_THE_FIRST_ROUND_OR_NOT_A_ROUNDS_MISSION()
						APPLY_CORONA_ALCOHOL_AND_SMOKE_EFFECTS( FALSE, FALSE, TRUE )
						CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][PROCESS_CLIENT][GAME_STATE_SOLO_PLAY] Applying corona drunk effects" )
					ELSE
						CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][PROCESS_CLIENT][GAME_STATE_SOLO_PLAY] Not applying corona drunk effects as it's a rounds mission and past the first round." )
					ENDIF
					
					IF CONTENT_IS_USING_ARENA()
						IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
						AND DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
							FREEZE_ENTITY_POSITION(GET_VEHICLE_PED_IS_IN(LocalPlayerPed), FALSE)
							PRINTLN("[PROCESS_CLIENT][GAME_STATE_SOLO_PLAY] - ARENA_SPAWN_FIX - Placed on ground. Time to UN-freeze.")
						ENDIF
						
						IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TAG_TEAM(g_FMMC_STRUCT.iAdversaryModeType)
							DELETE_ANY_EMPTY_NEARBY_VEHICLES()
						ENDIF
					ENDIF
					
					SET_MC_CLIENT_GAME_STATE(GAME_STATE_CAMERA_BLEND)
				ELSE
					PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_SOLO_PLAY][CS_ANIM][ROUNDCAM] GAME_STATE_SOLO_PLAY,, PROCESS_DEATHMATCH_STARTING_CUT = false ") 
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_SOLO_PLAY][CS_ANIM][ROUNDCAM] GAME_STATE_SOLO_PLAY,, HAS_SERVER_CHECKED_ALL_CLIENTS_READY_FOR_INTRO = false ") 

			ENDIF
			IF IS_AN_AIRLOCK_TRANSITION_ENDING_OR_STARTING()
				PROCESS_AIRLOCK_BOTTOM_RIGHT_HUD()
			ENDIF
		BREAK
		
		CASE GAME_STATE_TEAM_PLAY
			IF IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
				IF IS_PLAYER_CONTROL_ON(LocalPlayer)
					PRINTLN("[PROCESS_CLIENT][GAME_STATE_TEAM_PLAY] - ENTERED GAME_STATE_TEAM_PLAY - Turning off control.")
					NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE)
				ENDIF
			ENDIF
			
			PROCESS_DISPLACEMENT_ARENA_INTERIOR_MC()
			
			CLEAR_PLAYER_WANTED_LEVEL(LocalPlayer)
			HIDE_HUD_AND_RADAR_THIS_FRAME()

			IF HAS_SERVER_CHECKED_ALL_CLIENTS_READY_FOR_INTRO()
			
				SET_ALL_MISSION_ENTITIES_VISIBLITY(MC_serverBD_1.sFMMC_SBD, oiProps, oiPropsChildren, TRUE)
				PRE_PROCESS_WORLD_PROPS()
				
				MC_playerBD[PARTICIPANT_ID_TO_INT()].iSyncRoundCutsceneStage = jobIntroData.sNewAnimData.eStage 
				MC_playerBD[PARTICIPANT_ID_TO_INT()].iSyncRoundIntroCutsceneStage = jobIntroData.jobIntroStage
				MC_playerBD[PARTICIPANT_ID_TO_INT()].iSyncRoundTeamCutsceneStage = jobIntroData.jobTeamCutStage						
				MC_playerBD[PARTICIPANT_ID_TO_INT()].iSyncRoundPostCutsceneStage = GET_SKYSWOOP_STAGE()
				MC_playerBD[PARTICIPANT_ID_TO_INT()].bSyncCutsceneDone = jobIntroData.bReadyToMoveIntoGameplay
				
			
				IF GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
			
					IF IS_EVERYONE_READY_FOR_NEXT_ROUND()
						ARE_ALL_PLAYERS_READY_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION()
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciPRE_GAME_EMP)
						g_bFMMCLightsTurnedOff = FALSE
					ENDIF
					
					IF DOES_CAM_EXIST(jobIntroData.jobIntroCam)
					AND IS_EVERYONE_READY_FOR_NEXT_ROUND()
					AND HAS_SERVER_TIME_PASSED_ROUND(6000)
						IF ARE_ALL_PLAYERS_READY_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION()
							SET_SKYSWOOP_DOWN(jobIntroData.jobIntroCam)
						ELSE
							PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_TEAM_PLAY][ROUNDCAM] GAME_STATE_TEAM_PLAY ARE_ALL_PLAYERS_READY_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION = FALSE, wait ")
						ENDIF
					ENDIF
					
					IF HAS_NET_TIMER_EXPIRED(RoundCutsceneTimeOut, 25000, TRUE)
						PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_TEAM_PLAY][ROUNDCAM] GAME_STATE_TEAM_PLAY timed out, running skycamdown ")
					
						SET_SKYSWOOP_DOWN(jobIntroData.jobIntroCam)
					ENDIF
					
					IF iSyncRoundLastFrame != GET_SKYSWOOP_STAGE()
						IF IS_SKYCAM_ON_LAST_CUT()
						AND GET_SKYSWOOP_STAGE() != SKYSWOOP_NONE
							HAS_SERVER_TIME_PASSED_ROUND()
						ENDIF
					ENDIF
					iSyncRoundLastFrame = GET_SKYSWOOP_STAGE()
					
					PROCESS_HOLD_UP_LOGIC_BETWEEN_ROUNDS()					
					IF IS_EVERYONE_READY_FOR_GAMEPLAY()
						PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_TEAM_PLAY][ROUNDCAM] IS_EVERYONE_READY_FOR_GAMEPLAY() = TRUE - Calling LOWER_CARGOBOB_RAMP_FOR_INTRO ")
						LOWER_CARGOBOB_RAMP_FOR_INTRO()
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_TEAM_PLAY][ROUNDCAM] g_b_HoldupSkycamBetweenRounds = FALSE GAME_STATE_TEAM_PLAY GAME_STATE_TEAM_PLAY GET_MC_SERVER_GAME_STATE() = GAME_STATE_END ")
					g_b_HoldupSkycamBetweenRounds = FALSE
					SET_MC_CLIENT_GAME_STATE(GAME_STATE_CAMERA_BLEND)
				ENDIF
			
				IF BUSYSPINNER_IS_ON()
					IF IS_SKYSWOOP_AT_GROUND()
						BUSYSPINNER_OFF()
					ENDIF
				ENDIF
				
				STOP_AUDIO_SCENE_FOR_ARENA_VIP_TRANSITION()
			
				IF IS_ARENA_WARS_JOB()
					SET_RADIO_TO_STATION_NAME("OFF") 
				ENDIF
			
				IF PROCESS_NEW_TEAM_OVERVIEW_CUT(jobIntroData,MC_serverBD.vTeamCentreStartPoint[MC_PlayerBD[iLocalPart].iTeam])
					PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_TEAM_PLAY][ROUNDCAM] MOVING TO JOB INTRO") 
					RESET_NET_TIMER(tdSafetyTimer)
					RESET_NET_TIMER(RoundCutsceneTimeOut)	// [NG-INTEGRATE] Next-gen only.
					IF SHOULD_TRANSITION_SESSIONS_HIDE_RADAR_UNTILL_LAUNCHED()
						CLEAR_TRANSITION_SESSIONS_HIDE_RADAR_UNTILL_LAUNCHED()
					ENDIF
					g_b_HoldupSkycamBetweenRounds = FALSE	// [NG-INTEGRATE] Next-gen only.
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciRACE_STYLE_INTRO)
					AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciDISABLE_TEAM_VERSUS_SHARD)
						SHOW_TEAM_SHARD_AT_START_OF_VS_MISSION()
					ENDIF
					SET_PLAYER_BLIPS_DISPLAY_AT_START_OF_VS_MISSION()
					
					CLEANUP_CELEBRATION_SCALEFORMS(jobIntroData)
					IF IS_THIS_THE_FIRST_ROUND_OR_NOT_A_ROUNDS_MISSION()
						APPLY_CORONA_ALCOHOL_AND_SMOKE_EFFECTS( FALSE, FALSE, TRUE )
						CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][PROCESS_CLIENT][GAME_STATE_TEAM_PLAY] Applying corona drunk effects" )
					ELSE
						CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][PROCESS_CLIENT][GAME_STATE_TEAM_PLAY] Not applying corona drunk effects as it's a rounds mission and past the first round." )
					ENDIF
					
					CLEANUP_JOB_INTRO_CUTSCENE(jobIntroData) 
					CLEANUP_NEW_ANIMS(jobIntroData)
					
					SET_MC_CLIENT_GAME_STATE(GAME_STATE_CAMERA_BLEND)
				ELSE
				
					PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_TEAM_PLAY][CS_ANIM][ROUNDCAM] GAME_STATE_TEAM_PLAY,, PROCESS_NEW_TEAM_OVERVIEW_CUT = false ") 
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_TEAM_PLAY][CS_ANIM][ROUNDCAM] GAME_STATE_TEAM_PLAY,, HAS_SERVER_CHECKED_ALL_CLIENTS_READY_FOR_INTRO = false ") 

			ENDIF
			IF IS_AN_AIRLOCK_TRANSITION_ENDING_OR_STARTING()
				PROCESS_AIRLOCK_BOTTOM_RIGHT_HUD()
			ENDIF
		BREAK
		
		CASE GAME_STATE_CAMERA_BLEND
			PRINTLN("[PROCESS_CLIENT][GAME_STATE_CAMERA_BLEND] - ENTERED GAME_STATE_CAMERA_BLEND")
			
			IF IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
				IF IS_PLAYER_CONTROL_ON(LocalPlayer)
					PRINTLN("[PROCESS_CLIENT][GAME_STATE_CAMERA_BLEND] - ENTERED GAME_STATE_CAMERA_BLEND - Turning off control.")
					NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE)
				ENDIF
			ENDIF
						
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
				IF NOT GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()
					PRINTLN("[LM][PROCESS_CLIENT][GAME_STATE_CAMERA_BLEND] - UNPAUSING THE RENDERPHASE - This shouldn't 'NEED' be called. This is a Continengcy incase something on the transition from rounds went wrong!")
					TOGGLE_PAUSED_RENDERPHASES(TRUE)
				ENDIF
			ENDIF
			
			CLEAR_PLAYER_WANTED_LEVEL(LocalPlayer)
			CLEAR_AREA_OF_PEDS(g_vTeamStartPos, 50)
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			bLocalHandBrakeOnFlag = FALSE

			SET_ALL_MISSION_ENTITIES_VISIBLITY(MC_serverBD_1.sFMMC_SBD, oiProps, oiPropsChildren, TRUE)
			PRE_PROCESS_WORLD_PROPS()
			
			IF g_bIntroJobShardPlayed
			AND IS_SKYSWOOP_AT_GROUND()
				IF IS_AUDIO_SCENE_ACTIVE("DLC_AW_Arena_Lobby_Veh_Select_To_VIP_Transition_Scene")
					STOP_AUDIO_SCENE("DLC_AW_Arena_Lobby_Veh_Select_To_VIP_Transition_Scene")
				ENDIF
				IF IS_AUDIO_SCENE_ACTIVE("DLC_AW_Arena_Lobby_Veh_Select_To_Arena_Transition_Scene")
					STOP_AUDIO_SCENE("DLC_AW_Arena_Lobby_Veh_Select_To_Arena_Transition_Scene")
				ENDIF
			ENDIF
			
			IF SHOULD_MOVE_TO_NON_APARTMENT_INTRO() AND g_bRunHeistIntroShard
			AND IS_SCREEN_FADED_IN()
				DRAW_JOB_TEXT( jobIntroData, -1, SHOULD_LOCAL_PLAYER_SEE_TAKE_IN_CELEBRATION_SCREENS(), DEFAULT, DEFAULT, DEFAULT, DEFAULT, GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION())
				DISABLE_HUD()
				RENDER_SCRIPT_CAMS(FALSE, FALSE, DEFAULT, FALSE)
			ENDIF
						
			CPRINTLN( DEBUG_OWAIN, "[PROCESS_CLIENT][GAME_STATE_CAMERA_BLEND] - Value of Conor's flag is ", IS_BIT_SET( GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_EXITING_PROPERTY) )
			PROCESS_DISPLACEMENT_ARENA_INTERIOR_MC()
			IF GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
				IF PROCESS_DEATHMATCH_BLEND_OUT(jobIntroData)
					IF IS_BLEND_OK_TO_PROCEED(LocalPlayer)
						IF NOT IS_BIT_SET( GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_EXITING_PROPERTY)
							IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
								PLAY_SOUND_FRONTEND(-1,"Hit","RESPAWN_SOUNDSET", FALSE)
								IF NOT IS_PLAYER_PLAYING_OR_PLANNING_HEIST(LocalPlayer)
									PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_CAMERA_BLEND] ANIMPOSTFX_PLAY, GAME_STATE_CAMERA_BLEND ")
									ANIMPOSTFX_PLAY("MinigameEndNeutral", 0, FALSE)
								ENDIF
								CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][PROCESS_CLIENT][GAME_STATE_CAMERA_BLEND] Moving from GAME_STATE_CAMERA_BLEND to GAME_STATE_RUNNING" )
								
								CLEANUP_CELEBRATION_SCALEFORMS( jobIntroData )
								CLEAN_UP_INTRO_STAGE_CAM(g_sTransitionSessionData.ciCam,FMMC_TYPE_MISSION,TRUE) // Also called in DO_TUTORIAL_MISSION_CUTSCENE()
								DO_CORONA_POLICE_SETTINGS()
								IF IS_COUNTDOWN_OK_TO_PROCEED()
									IF IS_SKYSWOOP_AT_GROUND()
										NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
									ENDIF
								ENDIF
								IF USE_RACE_VEHICLE_CAMS()
									SET_PAUSE_INTRO_DROP_AWAY(FALSE)
									THEFEED_RESUME()
								ENDIF
								CLEAR_HEIST_QUICK_RESTART_IN_PROGRESS()
								SET_ON_JOB_INTRO(FALSE)
								SET_HIDOF_OVERRIDE(FALSE,FALSE,0,0,0,0)
								CHECK_EMERGENCY_CLEANUP()								
								HEIST_STOP_ANIM_TASK(jobIntroData, MC_playerBD[iPartToUse].iTeam)

								// NOTE(Owain): B* 2237750 - only reset the cam if we're on a contact mission
								IF Is_Player_Currently_On_MP_Contact_Mission(LocalPlayer)
								OR Is_Player_Currently_On_MP_Coop_Mission(LocalPlayer)
									SET_GAMEPLAY_CAM_RELATIVE_HEADING()
								ENDIF

								CLEANUP_JOB_INTRO_CUTSCENE(jobIntroData)
								
								CLEANUP_NEW_ANIMS(jobIntroData)
								
								START_CAMERA_BLEND_AUDIO_SCENES_AND_SOUNDS()
								
								LOWER_CARGOBOB_RAMP_FOR_INTRO_BACKUP()
								
								IF IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
									IF NOT IS_PLAYER_CONTROL_ON(LocalPlayer)
										PRINTLN("[PROCESS_CLIENT][GAME_STATE_CAMERA_BLEND] - ENTERED GAME_STATE_CAMERA_BLEND - Turning On control.")
										NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
									ENDIF
								ENDIF
								SET_MC_CLIENT_GAME_STATE(GAME_STATE_RUNNING)
							ENDIF
						ELSE
							CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][PROCESS_CLIENT][GAME_STATE_CAMERA_BLEND] PROPERTY_BROADCAST_BS_PLAYER_EXITING_PROPERTY - IS SET" )
						ENDIF
					ELSE
						CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][PROCESS_CLIENT][GAME_STATE_CAMERA_BLEND] IS_BLEND_OK_TO_PROCEED - FALSE" )
					ENDIF
				ELSE
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][PROCESS_CLIENT][GAME_STATE_CAMERA_BLEND] PROCESS_DEATHMATCH_BLEND_OUT - FALSE" )
				ENDIF
			ELSE
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][PROCESS_CLIENT][GAME_STATE_CAMERA_BLEND] Dirty server data, this mission will be broken." )
				SCRIPT_ASSERT("This mission is broken, there is some dirty server data.")
				IF IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
					IF NOT IS_PLAYER_CONTROL_ON(LocalPlayer)
						PRINTLN("[PROCESS_CLIENT][GAME_STATE_CAMERA_BLEND] - ENTERED GAME_STATE_CAMERA_BLEND - Turning On control.")
						NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
					ENDIF
				ENDIF
				SET_MC_CLIENT_GAME_STATE(GAME_STATE_RUNNING)
			ENDIF
			IF IS_AN_AIRLOCK_TRANSITION_ENDING_OR_STARTING()
				PROCESS_AIRLOCK_BOTTOM_RIGHT_HUD()
			ENDIF
		BREAK
		
		// Main gameplay state.
		CASE GAME_STATE_RUNNING
			INT iRule
			iRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam]
			g_bMissionClientGameStateRunning = TRUE
			g_bMission321Done = FALSE
			
			CORRECT_CAMERA_FROM_STARTING_IN_COVER()
			
			RESET_MICROPHONE_POSITION_FROM_START_TRANSITION()
			
			RESET_ROAMING_SPECTATOR_CAMERA_POSITION_FOR_START()	
			
			IF IS_SKYSWOOP_AT_GROUND() AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
				PROCESS_RESPAWN_BACK_AT_START_LOGIC(MC_playerBD[iLocalPart].iteam, iRule)
			ELSE
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Preventing PROCESS_RESPAWN_BACK_AT_START_LOGIC from being called because the mission is over and the sky camera is in the air or transitioning..." )
			ENDIF
			
			PROCESS_GRAB_POINTLESS_PACKAGE_AT_START()
						
			PROCESS_RESPAWN_VEHICLES_EACH_RULE(iRule)
			
			PROCESS_OVERTIME_START_AS_SPECTATOR()
						
			PROCESS_TRADING_PLACES_TIME_BAR()
			
			PROCESS_ARENA_OPTIONS()
			
			IF IS_BIT_SET(iLocalBoolCheck16, LBOOL16_CHANGE_PLAYER_TEAM_ALIVE_VARS_SETTER)
				SET_PLAYER_TEAM_SWAP_DATA(MC_playerBD[iLocalPart].iTeam)
				IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam] < FMMC_MAX_RULES
					PROCESS_MY_PLAYER_BLIP_VISIBILITY(MC_playerBD[iLocalPart].iTeam, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam])
				ENDIF
			ENDIF
			
			IF NOT PROCESS_MISSION_COUNTDOWN_TIMER()
				EXIT
			ENDIF
			
			g_bMission321Done = TRUE
			
			START_TAG_TEAM_MODE_TIMERS()
			
			PROCESS_GAME_MASTERS_MODE()
			
			PROCESS_PLAY_ARENA_AUDIO_SCORE()
						
			PROCESS_RACE_BOOST_START()
			
			SET_MISSION_START_RACE_TIME()
			
			IF Is_Player_Currently_On_MP_Versus_Mission(LocalPlayer)
			OR IS_FAKE_MULTIPLAYER_MODE_SET()
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_SUB_TEAM_ROLES)
					PROCESS_VERSUS_ROLES(MC_playerBD[iPartToUse].iTeam, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam], MC_playerBD[iLocalPart].iCoronaRole)
				ENDIF
			ENDIF
			
			PROCESS_PRE_GAME_EMP_AFTER_INTRO()			
			
			SET_SPAWN_VEHICLE_STARTING_VELOCITY_BACKUP()			
			
			IF SHOULD_MOVE_TO_NON_APARTMENT_INTRO() AND g_bRunHeistIntroShard
			AND IS_SCREEN_FADED_IN()
				DRAW_JOB_TEXT( jobIntroData, -1, SHOULD_LOCAL_PLAYER_SEE_TAKE_IN_CELEBRATION_SCREENS(), DEFAULT, DEFAULT, DEFAULT, DEFAULT, GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION() )
				DISABLE_HUD()
			ENDIF
			
			PROCESS_TUTORIAL_CUSTSCENE_CLEANUP()
			
			PROCESS_HEIST_SPECTATE(g_BossSpecData)
			
			IF IS_THIS_A_QUICK_RESTART_JOB()
				IF GET_HEIST_QUICK_RESTART_IN_PROGRESS()
					SET_HEIST_QUICK_RESTART_IN_PROGRESS(FALSE)
				ENDIF
			ENDIF
			
			//Leave this above PROCESS_MAIN_STATE!
			PROCESS_SPECTATOR_MODES_AND_LOCAL_PLAYER_VARIABLES()
			
			BLOCK_VEHICLE_REWARD_WEAPONS_DURING_LOCKED_CAPTURE()
			
			PROCESS_MAIN_STATE()

			MAINTAIN_BIOLAB_LIFT()
			
			MAINTAIN_SETTING_MAP_OBJECT_DOOR_RATIOS()
			
			PROCESS_CLIENT_TREAT_AS_WANTED()
			
			PROCESS_TEAM_SWAPPING_ADVERSARY_MODES()			
			
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciHIDE_DRIVING_ARROW_HELP)
				MAINTAIN_DRIVING_ARROWS()
			ENDIF
			
			MANAGE_CAM_PHONE_LEGEND()
								
			MAINTAIN_SPAWN_VEHICLE_BLIP()
			
			IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet,SBBOOL_MISSION_OVER)
				IF AM_I_ON_A_HEIST()
					//PRINTLN("[MMacK][Walkout] Getting Info")
					IF PLAYER_NEEDS_APARTMENT_INFO()
						COPY_APARTMENT_INFO_FOR_WALKOUT()
					ELSE
						IF IS_BIT_SET(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_1)
						OR IS_BIT_SET(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_2)
						OR IS_BIT_SET(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_STRAND_1) 
						OR HAS_FIRST_STRAND_MISSION_BEEN_PASSED_NO_RESET()
							CLEAN_APARTMENT_INFO_FOR_WALKOUT()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			PROCESS_TRIP_SKIP_EVERY_FRAME()
			
			PROCESS_QUICK_EQUIP_MASK()
			
			GET_SNACKS_STATUS(MC_playerBD[iPartToUse].iteam)
			
			PROCESS_CLEAR_HUD_FOR_SPECIAL_REASONS()
			
			PROCESS_MISSION_END_STAGE()
						
			IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
				DISPLAY_ROUNDS_LBD()
			ENDIF
			
			PROCESS_PLAYER_FAIL_QUIT_TEST_MODE_FOR_CREATOR()			
						
			PROCESS_PROOFS()
			
			IF GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
			OR g_bMissionOver
				IF IS_AN_AIRLOCK_TRANSITION_ENDING_OR_STARTING()
					PROCESS_AIRLOCK_BOTTOM_RIGHT_HUD(GET_MC_SERVER_GAME_STATE() = GAME_STATE_END)
				ENDIF
			ENDIF
			
		BREAK
		
		CASE GAME_STATE_MISSION_OVER
			
			IF IS_AN_AIRLOCK_TRANSITION_ENDING_OR_STARTING()
				PROCESS_AIRLOCK_BOTTOM_RIGHT_HUD()
			ENDIF
			
			IF IS_SCREEN_FADED_OUT()
			OR IS_BIT_SET(iLocalBoolCheck,LBOOL_LB_FINISHED)
			OR (g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration AND g_TransitionSessionNonResetVars.sGlobalCelebrationData.bInTestMode)
				//2081710 - Humane Labs | Keycodes: Music during Karen arrival cutscene needs updating
				IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
					IF NOT g_bCelebrationScreenIsActive
					AND NOT IS_THIS_AN_ADVERSARY_MODE_MISSION(g_FMMC_STRUCT.iRootContentIDHash, g_FMMC_STRUCT.iAdversaryModeType)
						SCRIPT_TRIGGER_MUSIC_EVENT_STOP()
						PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_MISSION_OVER] MUSIC - TRIGGER_MUSIC_EVENT(MP_MC_STOP) - M")
					ELSE
						PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_MISSION_OVER] MUSIC - not calling TRIGGER_MUSIC_EVENT(MP_MC_STOP) - M, g_bCelebrationScreenIsActive = TRUE, celebration screen will handle this.")
					ENDIF
				ENDIF
				
				MC_SCRIPT_CLEANUP()
				SET_MC_CLIENT_GAME_STATE(GAME_STATE_END)
			ENDIF
		BREAK

		CASE GAME_STATE_LEAVE
			DISABLE_SPECTATOR_FADES_DURING_THIS_EVENT()
			IF NOT IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData)				//DaveyG 04/01/2013 to avoid fade in
				
				IF NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC)
					SCRIPT_TRIGGER_MUSIC_EVENT_FAIL()
					PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_LEAVE] - TRIGGER_MUSIC_EVENT(\"MP_MC_FAIL\") - m1")
				ENDIF
				
				DoSkySwoopUpImmediateFadeOutIfNecessary()
				MC_SCRIPT_CLEANUP(FALSE)
				SET_MC_CLIENT_GAME_STATE(GAME_STATE_END)
			ENDIF

			
		FALLTHRU

		CASE GAME_STATE_END			

		BREAK

	ENDSWITCH
	
ENDPROC

// Check to see if we should leave the mission now, for various reasons
// If so, we terminate the script thread here in MC_SCRIPT_CLEANUP
PROC CHECK_MISSION_BAIL_CONDITIONS()

#IF FEATURE_GEN9_STANDALONE
	//If we're on a heist finale or setup mission and bailing we need to abandon the relevant UDS activities
	IF AM_I_ON_A_HEIST() AND GET_HEIST_AM_I_HEIST_LEADER_GLOBAL()
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		OR SHOULD_PLAYER_LEAVE_MP_MISSION(GET_CORONA_STATUS() = CORONA_STATUS_IDLE)
			HEIST_UDS_ACTIVITY_MISSION_BAIL()
		ENDIF
	ENDIF
#ENDIF // FEATURE_GEN9_STANDALONE

	// If we have a match end event, bail.
	IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			IF Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)
				IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bIAmHeistLeader
					CPRINTLN(DEBUG_MISSION,"[RCC MISSION] - [PMC] - setting should warp into apartment since quitting from a heist prep mission.")
					SET_POST_MISSION_PLACE_IN_APARTMENT_FLAG(TRUE)
					SET_APARTMENT_LOCATION()
				ENDIF
			ENDIF

		IF NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC)
			SCRIPT_TRIGGER_MUSIC_EVENT_FAIL()
			PRINTLN("[RCC MISSION] TRIGGER_MUSIC_EVENT(\"MP_MC_FAIL\") Cmbc1")
		ENDIF
		MC_SCRIPT_CLEANUP(FALSE)
	ENDIF
	
	IF SHOULD_PLAYER_LEAVE_MP_MISSION(GET_CORONA_STATUS() = CORONA_STATUS_IDLE) 
		CPRINTLN(DEBUG_MISSION,"[RCC MISSION] SHOULD_PLAYER_LEAVE_MP_MISSION true ")
		SET_BIT(sSaveOutVars.iBitSet,ciRATINGS_QUIT)
		iMissionResult = ciFMMC_END_OF_MISSION_STATUS_CANCELLED
		SCRIPT_TRIGGER_MUSIC_EVENT_FAIL()
		PRINTLN("[RCC MISSION] TRIGGER_MUSIC_EVENT(\"MP_MC_FAIL\") Cmbc2")
		IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
			TempFMMC_EOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_RESTART
			TempFMMC_EOM.vWarpPos = <<364.9413,278.7449,102.2490>> //g_FMMC_STRUCT.vStartPos
			TempFMMC_EOM.bVotePassed = TRUE
			MAINTAIN_SETTING_VARS_ON_VOTE_PASSING(sEndOfMission, TempFMMC_EOM)
		ENDIF
		IF g_bNeedToCallMatchEndOnQuit
		AND MC_playerBD[iLocalPart].iGameState >= GAME_STATE_RUNNING
			DEAL_WITH_FM_MATCH_END(FMMC_TYPE_MISSION, g_MissionControllerserverBD_LB.iMatchTimeID, DEFAULT, iMissionResult, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, IS_THIS_A_ROUNDS_MISSION(), MC_serverBD.iDifficulty, g_sArena_Telemetry_data.m_matchduration)
		ENDIF
		DoSkySwoopUpImmediateFadeOutIfNecessary()
		MC_SCRIPT_CLEANUP(FALSE)
	ENDIF	

ENDPROC

PROC PROCESS_MISSION_TIME_OF_DAY_EVERY_FRAME()
	IF MC_serverBD.iTimeOfDay > -1
		IF IS_CORONA_READY_TO_START_WITH_JOB()
		OR DID_I_JOIN_MISSION_AS_SPECTATOR()
		OR IS_FAKE_MULTIPLAYER_MODE_SET()
		OR IS_PLAYER_SCTV(LocalPlayer)
		OR AM_I_ON_A_HEIST() 
		OR SVM_FLOW_IS_CURRENT_MISSION_SVM_FLOW()
		OR GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
			IF NOT IS_BIT_SET(iLocalBoolCheck7, LBOOL7_BLOCK_TOD_EVERY_FRAME)
			AND NOT DELAY_SET_TOD()
			AND IS_BIT_SET(MC_serverbd.iServerBitSet6, SBBOOL6_UPDATED_WEATHER_AND_TIME_FROM_CORONA)
				IF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciPROGRESS_TIME_OF_DAY) AND NOT IS_BIT_SET(iLocalBoolCheck9,LBOOL9_TIME_SET))
				OR NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciPROGRESS_TIME_OF_DAY)
					IF MC_serverBD.iNumHeistPlays != -1 AND MC_serverBD_3.iNumSVMPlays != -1
						IF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciLOCK_TOD_FIRST_PLAY_ONLY) AND (MC_serverBD.iNumHeistPlays = 0 AND MC_serverBD_3.iNumSVMPlays = 0))
						OR NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciLOCK_TOD_FIRST_PLAY_ONLY)
							
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciOPTION_VEHICLE_WEAPON_ENABLED)
								IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iVehicleWeaponEffectActiveBS, ciVEH_WEP_PRON)
								AND NOT g_bArenaBigScreenBinkPlaying
									IF NOT CONTENT_IS_USING_ARENA()
										IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciFORCE_SPECIFIC_TOD)
											SET_TIME_OF_DAY(MC_serverBD.iTimeOfDay)
										ELSE
											NETWORK_OVERRIDE_CLOCK_TIME(g_FMMC_STRUCT.iTODOverrideHours, g_FMMC_STRUCT.iTODOverrideMinutes, 0)
										ENDIF
										
										IF CONTENT_IS_USING_ARENA()
											SET_BIT(iLocalBoolCheck30, LBOOL30_TIME_SET_FOR_ARENA)
											CASCADE_SHADOWS_ENABLE_FREEZER(TRUE)
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF NOT g_bArenaBigScreenBinkPlaying
									IF NOT CONTENT_IS_USING_ARENA()
										IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciFORCE_SPECIFIC_TOD)
											SET_TIME_OF_DAY(MC_serverBD.iTimeOfDay)
										ELSE
											NETWORK_OVERRIDE_CLOCK_TIME(g_FMMC_STRUCT.iTODOverrideHours, g_FMMC_STRUCT.iTODOverrideMinutes, 0)
										ENDIF
										IF CONTENT_IS_USING_ARENA()
											SET_BIT(iLocalBoolCheck30, LBOOL30_TIME_SET_FOR_ARENA)
											CASCADE_SHADOWS_ENABLE_FREEZER(TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT IS_BIT_SET(iLocalBoolCheck9, LBOOL9_TIME_SET)
							SET_BIT(iLocalBoolCheck9, LBOOL9_TIME_SET)
						ENDIF
					ELSE
						CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][TIME] MC_serverBD.iNumHeistPlays = -1 !!!!!")
					ENDIF
				ENDIF
			ENDIF
			IF NOT IS_BIT_SET(iLocalBoolCheck2, LBOOL2_WEATHER_SET)
				IF MC_serverBD.iWeather > -1 AND IS_BIT_SET(MC_serverbd.iServerBitSet6, SBBOOL6_UPDATED_WEATHER_AND_TIME_FROM_CORONA)
					IF MC_serverBD_3.iNumSVMPlays != -1
						IF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciLOCK_WEATHER_FIRST_PLAY_ONLY) AND MC_serverBD_3.iNumSVMPlays = 0)
						OR NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciLOCK_WEATHER_FIRST_PLAY_ONLY)
							SET_WEATHER_FOR_FMMC_MISSION(MC_serverBD.iWeather)
							SET_BIT(iLocalBoolCheck2, LBOOL2_WEATHER_SET)
						ENDIF
					ENDIF
				ENDIF
			ENDIF		
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_DISABLED_RADIO()
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciDISABLE_RADIO)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO_TRACK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO_TRACK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_RADIO_WHEEL)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_RADIO_WHEEL_LR)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_RADIO_WHEEL_UD)
			
			IF GET_PLAYER_RADIO_STATION_INDEX() != 255
				SET_RADIO_TO_STATION_INDEX(255)
				IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
				AND NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(localPlayerPed))
					PRINTLN("[RCC MISSION] Disabling Radio")
					SET_VEHICLE_RADIO_ENABLED(GET_VEHICLE_PED_IS_IN(localPlayerPed), FALSE)
				ENDIF
			ENDIF
		ENDIF
ENDPROC

PROC RESET_LOCAL_BOOL_BITSET()
	iLocalBoolResetCheck1 = 0
	
	INT iPedLoop
	FOR iPedLoop = 0 TO ciFMMC_PED_BITSET_SIZE-1
		iSPHBBS_PedHasHadBlipCheckedBS[iPedLoop] = 0
		iSPHBBS_PedShouldHaveBlipBS[iPedLoop] = 0
	ENDFOR
	
ENDPROC

// -----------------------------------	MAIN LOOP ----------------------------------------------------------------------
SCRIPT(MP_MISSION_DATA fmmcMissionData)
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_AutoProfileMissionController") //Intentionally not cached!
		SET_PROFILING_OF_THIS_SCRIPT(TRUE)
	ENDIF
	#ENDIF
	
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	
	RESET_GLOBAL_SPECTATOR_STRUCT()
	// Carry out all the initial game starting duties.
	IF NOT PROCESS_PRE_GAME(fmmcMissionData)
		PRINTLN("[RCC MISSION] fm_mission_creator: Failed to receive and initial network broadcast. Cleaning up") NET_NL() 
		MC_SCRIPT_CLEANUP(FALSE)
	ENDIF
	
	IF IS_A_STRAND_MISSION_BEING_INITIALISED()
		SET_BIT(GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_ON_SECOND_PART_OF_STRAND)
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_ON_SECOND_PART_OF_STRAND)
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bHeistHashedMac
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			SEND_EVENT_HEIST_POSIX_AND_HASHED_MAC()
		ENDIF
	ENDIF
	#ENDIF
	#IF IS_DEBUG_BUILD
		CREATE_MC_WIDGETS()
		
		#IF SCRIPT_PROFILER_ACTIVE
			CREATE_SCRIPT_PROFILER_WIDGET()
		#ENDIF
	#ENDIF
	LOAD_MC_TEXT_BLOCK()
	LOAD_PARTICLE_FX()
	LOAD_ACTION_ANIMS()
	
	INITIALISE_CACHED_BLIP_NAME_INDEXES()
	INITIALISE_FLASHING_VEHICLE_BLIPS()		
	
	WHILE TRUE
		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
		
		#IF IS_DEBUG_BUILD
			iPrintNum = 0
		#ENDIF
		
		// Always check to see if we should bail first, else we'll array overrun
		// as we're no longer active on the script any more
		CHECK_MISSION_BAIL_CONDITIONS()
		
		PROCESS_PRE_CONTROLLER_LOGIC_FIXES()
		
		INIT_LOCAL_PLAYER_DATA()
		
		RESET_LOCAL_BOOL_BITSET()
		
		PROCESS_EVENTS()
		
		// Heist anti-cheat
		MAINTAIN_HEIST_LEADER_ANTI_CHEAT()
		MAINTAIN_GANGOPS_LEADER_ANTI_CHEAT()
		MAINTAIN_CASINO_HEIST_LEADER_ANTI_CHEAT()
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_ARENA)
			ENABLE_STADIUM_PROBES_THIS_FRAME(TRUE)
		ENDIF
		
		SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER()
		
		PROCESS_MISSION_FACTORY_INTERIORS(sMissionFactories, iInteriorInitializingExtrasBS)
		
		PROCESS_INTERIOR_COLLISION_FIXES(sInteriorFixStruct)
		
		FM_MAINTAIN_MISSION_DENSITYS_THIS_FRAME( MC_serverBD.fPedDensity,
												MC_serverBD.fPedDensity, 
												MC_serverBD.fPedDensity,
												MC_serverBD.fVehicleDensity,
												MC_serverBD.fVehicleDensity,
												MC_serverBD.fVehicleDensity,
												MC_serverBD.fVehicleDensity,
												MC_serverBD.fVehicleDensity = 0.0 )	
		IF IS_CELLPHONE_TRACKIFY_IN_USE()
			SET_PHONE_UNDER_HUD_THIS_FRAME()
		ENDIF
	
		
		MAINTAIN_DRAW_BLACK_RECTANGLE_FOR_FADED_MOCAP()
		MAINTAIN_PLAYER_CONTROL_AT_END_OF_STRAND_CUTSCENE()
		
		
		IF IS_BIT_SET(MC_serverBD.iOptionsMenuBitSet, ciGANGS_DISABLED) 
			SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(LocalPlayer)
		ENDIF

		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetTwelve, ciDISABLE_CPO_OVERRIDE)
		OR IS_PHONE_EMP_CURRENTLY_ACTIVE()
			IF g_FMMC_STRUCT.fCopPerceptionOverrideSeeingRange <> 60.0	//Default values
			OR g_FMMC_STRUCT.fCopPerceptionOverridePeripheralRange <> 5.0
			OR g_FMMC_STRUCT.fCopPerceptionOverrideHearingRange <> 60.0
			OR g_FMMC_STRUCT.fCopPerceptionOverrideRearViewSeeingRange <> -1.0
			OR IS_PHONE_EMP_CURRENTLY_ACTIVE()
				
				FLOAT fSeeingRangeToUse = g_FMMC_STRUCT.fCopPerceptionOverrideSeeingRange
				IF IS_PHONE_EMP_CURRENTLY_ACTIVE()
					fSeeingRangeToUse *= 0.01
					fSeeingRangeToUse *= g_FMMC_STRUCT.sPhoneEMPSettings.iPhoneEMP_PerceptionPercentage
				ENDIF
				
				SET_COP_PERCEPTION_OVERRIDES(fSeeingRangeToUse, 
											 g_FMMC_STRUCT.fCopPerceptionOverridePeripheralRange, 
											 g_FMMC_STRUCT.fCopPerceptionOverrideHearingRange,
											 DEFAULT, 
											 DEFAULT, 
											 DEFAULT, 
											 g_FMMC_STRUCT.fCopPerceptionOverrideRearViewSeeingRange)
			ENDIF
		ENDIF
		
		IF g_FMMC_STRUCT.fCopDelayedReportTime <> -1.0	//Default value
			ALLOW_EVASION_HUD_IF_DISABLING_HIDDEN_EVASION_THIS_FRAME(PLAYER_ID(), g_FMMC_STRUCT.fCopDelayedReportTime)
		ENDIF
		
		PROCESS_DISABLED_RADIO()
			
		PROCESS_MISSION_TIME_OF_DAY_EVERY_FRAME()

		#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
				SCRIPT_PROFILER_START_OF_FRAME()
			#ENDIF
		#ENDIF	
		
		#IF IS_DEBUG_BUILD
		MAINTAIN_MISSION_RESTART_DEBUG()
		#ENDIF	
		
		IF IS_FAKE_MULTIPLAYER_MODE_SET()
			IF NOT IS_PAUSE_MENU_ACTIVE()
				IF g_FMMC_STRUCT.g_b_QuitTest
					IF IS_SCREEN_FADED_OUT()
						PRINTLN("[RCC MISSION] - g_FMMC_STRUCT.g_b_QuitTest is true so quit")
						MC_SCRIPT_CLEANUP()
					ELSE
						IF NOT IS_SCREEN_FADING_OUT()
							DO_SCREEN_FADE_OUT(500)
							SCRIPT_TRIGGER_MUSIC_EVENT_STOP()
							PRINTLN("[RCC MISSION] MUSIC - TRIGGER_MUSIC_EVENT(MP_MC_STOP) - K")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(iLocalBoolCheck3,LBOOL3_RADAR_INIT)
				INIT_RADAR_ZOOM_LEVEL()
				CONTROL_RADAR_ZOOM()
			ELSE
				CONTROL_RADAR_ZOOM()
			ENDIF
		ENDIF
		
		DO_OUTROS()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("DO_OUTROS")
		#ENDIF
		#ENDIF
		
		IF GET_MC_CLIENT_GAME_STATE(iLocalPart) = GAME_STATE_INI
			IF NOT IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING()
			AND NOT g_bStartedInAirlockTransition
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				THEFEED_HIDE_THIS_FRAME() 
				PRINTLN("radar being hidden: ") 
			ENDIF
		ENDIF
		
		IF IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING()
		OR g_bStartedInAirlockTransition
			//Hide Area names
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MC_SCRIPT_CLEANUP")
		#ENDIF
		#ENDIF
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
		
			IF bIsLocalPlayerHost
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_POWER_PLAY(g_FMMC_STRUCT.iAdversaryModeType)
					IF g_iVersusOutfitStyleSetup > -2
						IF MC_serverBD_3.iVersusOutfitStyleSetup != g_iVersusOutfitStyleSetup
							PRINTLN("[RCC MISSION] DATA CHANGED Updating - g_iVersusOutfitStyleSetup=",g_iVersusOutfitStyleSetup)
							PRINTLN("[RCC MISSION] DATA CHANGED Updating - MC_serverBD_3.iVersusOutfitStyleSetup=",MC_serverBD_3.iVersusOutfitStyleSetup)
							MC_serverBD_3.iVersusOutfitStyleSetup = g_iVersusOutfitStyleSetup
						ENDIF
					ENDIF
					IF g_iVersusOutfitStyleChoice > -2
						IF MC_serverBD_3.iVersusOutfitStyleChoice != g_iVersusOutfitStyleChoice
							PRINTLN("[RCC MISSION] DATA CHANGED Updating - g_iVersusOutfitStyleChoice=",g_iVersusOutfitStyleChoice)
							PRINTLN("[RCC MISSION] DATA CHANGED Updating - MC_serverBD_3.iVersusOutfitStyleChoice=",MC_serverBD_3.iVersusOutfitStyleChoice)
							MC_serverBD_3.iVersusOutfitStyleChoice = g_iVersusOutfitStyleChoice
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//Deal with making sure that a spectator is not host
			MAINTAIN_SPEC_NOT_HOST(iManageSpechost, iDesiredHost)
			 
			IF NOT IS_PLAYER_SCTV(LocalPlayer)						//This MAINTAIN_SPECTATOR is just for player specating, not SCTV
				MAINTAIN_SPECTATOR(g_BossSpecData)
				#IF IS_DEBUG_BUILD
		        #IF SCRIPT_PROFILER_ACTIVE 
		            ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SPECTATOR")
		        #ENDIF
		        #ENDIF
			ENDIF
			
			PROCESS_SPECTATOR_PLAYER_LISTS_FOR_MISSION()			
			
			// For phone warning message.
			g_iNumPlayersOnMyHeistTeam = MC_serverBD.iNumberOfPlayingPlayers[MC_playerBD[iLocalPart].iteam]			
			
			SET_MISSION_INIT_COMPLETE_GLOBAL()			
			
			// For data dump when mission is ending.
			#IF IS_DEBUG_BUILD
			MAINTAIN_EOM_DATA_DUMP()
	        #IF SCRIPT_PROFILER_ACTIVE 
	            ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_EOM_DATA_DUMP")
	        #ENDIF
	        #ENDIF
			
			IF Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer)
			AND NOT IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_MISSION_IS_CTF_RAID) 
				IF iNumObjsChecked < 12
					IF NOT MOVE_ANY_ILLEGALLY_PLACED_CAPTURE_OBJECTS()	
						PRINTLN("[LH][ILLEGALOBJS] - Illegally placed objects were found and still have not yet been moved! Mission can be exploited!")
					ELSE
						PRINTLN("[LH][ILLEGALOBJS] - All illegally placed objects were found and have now been moved! Mission can no longer be exploited!")
					ENDIF
				ENDIF
			ENDIF
		
// -----------------------------------	CLIENT -----------------------------------	
			PROCESS_CLIENT()
// -------------------------------------------------------------------------------
			
		ELSE
			IF IS_AN_AIRLOCK_TRANSITION_ENDING_OR_STARTING()
				PROCESS_AIRLOCK_BOTTOM_RIGHT_HUD()
			ENDIF
			#IF IS_DEBUG_BUILD
				SCRIPT_ASSERT("NETWORK_IS_GAME_IN_PROGRESS is returning false") 
			#ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("process main game state")
		#ENDIF
		#ENDIF
		
		MAINTAIN_HIDING_HUD_FOR_VERSUS()
				
		MAINTAIN_THE_CURRENT_HUD()
		
		MAINTAIN_CELEB_PRE_LOAD_FX(sCelebrationData.bShownPreloadFX, sCelebrationData.timerCelebPreLoadPostFxBeenPlaying)
		
		MAINTAIN_INTERNAL_CELEBRATION_SCREEN()
		MAINTAIN_STAND_ALONE_CELEBRATION_SCREEN()
				
// -----------------------------------	SERVER  -----------------------------------
		IF bIsLocalPlayerHost
			PROCESS_SERVER()
		ENDIF
//---------------------------------------------------------------------------------
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("server processing")
		#ENDIF
		#ENDIF
		
		MAINTAIN_BG_GLOBALS_PASS_OVER()	
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_BG_GLOBALS_PASS_OVER")
		#ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD		
			PROCESS_CONTENT_OVERVIEW_DEBUG_WINDOW(&PROCESS_CONTENT_OVERVIEW_DEBUG_WINDOW_MAIN_FOR_SPECIFIC_CONTENT, &PROCESS_CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT)	
			PROCESS_MAIN_MISSION_CONTROLLER_DEBUG()			
		#ENDIF
		
		PROCESS_INSTANCED_CONTENT_FADE_SCREEN_OUT_FOR_PERIOD_OF_TIME()
		
		IF CONTENT_IS_USING_ARENA()
			PROCESS_TRAP_CAM_TRAP_BLIPS(biTrapCamBlip)
			PROCESS_SPECIAL_SPECTATOR_MODES_FOR_INSTANCED_CONTENT()
			PROCESS_MINIROUND_TRANSITIONS_FOR_SPECIAL_SPECTATORS()
		ENDIF
		
		IF CONTENT_IS_USING_ARENA()
		AND IS_SKYSWOOP_AT_GROUND()
			PROCESS_ARENA_ANNOUNCER_EVERY_FRAME()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_DEBUG")
		#ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_END_OF_FRAME()
		#ENDIF
		#ENDIF	
		
		#IF IS_DEBUG_BUILD
		MC_UPDATE_PLAYER_RESPAWN_STATE()
		#ENDIF
		
	ENDWHILE
	
ENDSCRIPT
